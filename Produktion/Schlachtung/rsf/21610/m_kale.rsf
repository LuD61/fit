/*
-----------------------------------------------------------------------------
-
-      (C)OPYRIGHT 1991 by       
-
-	BSA Bizerba Software- und Automationssysteme GmbH
-	Geisental 12, 4630 Bochum 1, Tel: 0234/5065-250
-
-----------------------------------------------------------------------------
- 
-  Rechtsklausel:
-  Fuer diese Dokumentation/Programmbeschreibung behalten wir uns alle Rechte
-  auch fuer den Fall der Patenterteilung und der Eintragung eines anderen
-  gewerblichen Schutzrechtes vor.
-  Missbraeuchliche Verwendung, wie insbesondere Vervielfaeltigung und 
-  Weitergabe an Dritte, ist nicht gestattet; 
-  sie kann zivil- und strafrechtlich geahndet werden.
-
- ------------------------------------------------------------------------------
-
-	Modulname		:	
-
-	Autor			:	
-	Erstellungsdatum	:	
-
-	Projekt			:	BWS
-	Laendervariante		:	BRD
-
-	Rechner			:	Targon 31 
-	Sprache			:       ROSI-SQL 3.1
-
- 
------------------------------------------------------------------------------
-
-  Modulbeschreibung	:  Datendefinitionen als Constanten
-
- -----------------+------------------------------------------------------------
-  Titel            : m_kale.rsf
-  -----------------+------------------------------------------------------------
-  Version          : @(#)m_kale.rsf	1.1   1/10/92   19:17:13
-  -----------------+------------------------------------------------------------
-  Aenderungsjournal:
-
-  ---------------+------------------------------------------------------------
-  @(#) Datum     lfd.     Autor       Bemerkungen
-  @(#) --------  -------  ----------  -----------
-  @(#) 17.09.91  #1     
------------------------------------------------------------------------------
*/
/***---------------------------------------------------------------------***/
module m_kale
export  procedure datum_comp
	procedure kal_woche
end

import module mo_meld end
/***
--------------------------------------------------------------------------------
-
-       BIZERBA         System-Engineering         TE-IS-W
-       Postfach 10 01 64, 7460 Balingen, Tel.: 07433/12-0
-
--------------------------------------------------------------------------------
-
-       Modulname               :       m_kale
-
-       Interne Prozeduren      :       kal_woche 
-
-       Externe Prozeduren      :       ----
-
-       Autor                   :       Norbert
-       Abgekupfert von         :       Bruno
-       Erstellungsdatum        :       14.08.90
-
-       Projekt                 :       BWS
-       Version                 :       1.01
-       Laendervariante         :       BRD
-
-       Rechner                 :       UNISYS 386
-       Betriebssystem          :       XENIX  386
-       Sprache                 :       ROSI-SQL
-
-       Aenderungsjournal       :
-
-       lfd.    Version Datum           Name       Beschreibung
-       ------------------------------------------------------------------------
-       1       1.01    17.08.90        Norbert    Erweitert fuer zusaetzliche
-                                                  Rueckgabe des Jahres
-       2       1.10    17.08.90        Norbert    Erweitert: proc datum_comp
-       3       1.11    20.08.90        Norbert    proc datum_comp erweitert
-                                                  fuer konst. Feiertage
-       4       1.12    03.09.90        Norbert    var. Rueckgabe: SA oder MO
-	5               01.11.90        Juergen    datum_comp neu, als modul
-		Feiertage abgeschaltet
--------------------------------------------------------------------------------
-
-       -----------------       :
-
--------------------------------------------------------------------------------
***/



/*-------------------------------------------------------------------*/
/* 	Programm Kalenderwoche                                       */
/*-------------------------------------------------------------------*/
/*
	Diese Routine berechnet die Kalenderwoche/Jahr aus dem Datum     

        Aufruf:
                   perform kal_woche (datum) returning (kalwoch , kal_year)
*/

proc kal_woche
parameter  
field    datum          date      /* Uebergabe des aktuellen Datums   */
end

field    jahrtage       date      /* Umwandlung des Jahres in Tage    */
field    vjahrtage      date      /* Umwandlung des Vorjahres in Tage */
field    wochtag        smallint  /* Wochentag des 01.01. im Jahr     */
                                  /*So=0,Mo=1,Di=2,Mi=3,Do=4,Fr=5,Sa=6*/
field    vwochtag       smallint  /* Wochentag des 01.01. im Vorjahr  */
field    nwochtag       smallint  /* Wochentag des 01.01. im Nach.jahr*/
field    tage           smallint  /* Tage fuer Kalenderwoche          */
field    kalwoch        smallint  /* Kalenderwoche                    */
field    kal_year       char(4)   /* Jahr der Kalenderwoche           */

	if "" = "@(#)m_kale.rsf	1.1   1/10/92   19:17:13" end
	if 
	   "" = "@(#) #1   Norbert (Kraushaar)   Ursprungsversion SCCS"
	end
let jahrtage  = "01.01."#year (datum)
let vjahrtage = "01.01."#(year (datum) - 1)
let wochtag  =  weekday (jahrtage)
let vwochtag =  weekday (vjahrtage)
let nwochtag =  weekday ("01.01."#(year (datum) + 1))
if  wochtag = 0  let  wochtag  = 7 end
if vwochtag = 0  let vwochtag  = 7 end
                                  /*Mo=1,Di=2,Mi=3,Do=4,Fr=5,Sa=6,So=7*/
if wochtag > 4                    /* Woche gehoert zum Vorjahr */
   if datum < (jahrtage + 8 - wochtag) /* Liegt Datum vor KW 1 */
       if vwochtag > 4            /* Wochentag des 01.01. vom Vorjahr */
                                  /* KW = 53 */
          let tage = datum - vjahrtage + vwochtag - 7 - 1
       else
                                  /* KW = 52 */
          let tage = datum - vjahrtage + vwochtag - 1
       end
       let kal_year = (year (datum) - 1)
   else                           /* KW = 1 bis 52 oder 1 von naechste*/
       let tage = datum - jahrtage + wochtag - 7
       let kal_year = (year (datum) )
       if tage > 364 and nwochtag < 5 /* 52 * 7 = 364 */
          let tage = 1            /* KW = 1 von naechstem Jahr */
          let kal_year = (year (datum) + 1 )
       end
   end
else                              /* Woche ist bereits in KW = 1 */
   let tage = datum - jahrtage + wochtag /* KW = 1 bis KW = 53 */
   let kal_year = (year (datum) )
   if tage > 364 and nwochtag < 5
      let tage = 1                /* KW = 1 von naechstem Jahr */
      let kal_year = (year (datum) + 1 )
   end
end
let kalwoch = (tage + 6) / 7      /* KW berechnen, ohne 0 */
return (kalwoch , kal_year)
end /* kal_woche */

/*-------------------------------------------------------------------*/
/* 	Programm DATUM COMPute                                       */
/*-------------------------------------------------------------------*/
/*
	Diese Routine berechnet das Datum des Montags aus Kalenderwoche
        und Jahreszahl

        Aufruf:
                   perform datum_comp (k_woch , k_jahr , day_flag)
                   returning (k_datum)

	/** die Behandlung von Feiertagenm rausgeschmissen JG **/
*/

proc datum_comp

parameter
      field k_woch         smallint     /*Uebergabe der eingeg. Kalenderwoche*/
      field k_jahr         smallint     /*Uebergabe der eingeg. Jahreszahl ohne
                                          Angabe d. Jahrhunderts (Default:1900*/
      field day_flag       smallint     /*Uebergabe des gewuensch. Wochentages*/
end

field dauswerten     smallint	/*	1 = alles ignorieren */
field first_weekday  smallint   /* Wochentag des 1. Julis                    */
field week_1         smallint   /* KalWoch von fr_jun                        */
field year_1         smallint   /* Jahreszahl von fr_jun                     */
field fr_dat         date       /* Datumsmerkzelle                           */
field dat_char       char(8)    /* Umladevariable fuer fr_dat                */
field dd_mm          char(5)    /* Tag und Monat von fr_dat                  */
field error_day      smallint   /* Flag fuer korrekten Arbeitstag            */
/*       day_flag:  1 Mo
                    2 Di
                    3 Mi
                    4 Do
                    5 Fr
                    6 Sa
                    7 So
*/

let dauswerten = 1	/* Feiertage ignorieren */
let fr_dat = "01.01."#k_jahr
let first_weekday = weekday (fr_dat)
if first_weekday > 4
	let fr_dat = fr_dat - first_weekday + 8 /* 1.woche beginnt spaeter */
   else let fr_dat = fr_dat - first_weekday + 1 /* 1.woche teilw. im Vorjahr */
end
let fr_dat = fr_dat + day_flag - 1 + (7 * (k_woch - 1))
perform kal_woche (fr_dat) returning (week_1, year_1)

if dauswerten = 1
	let error_day = 0
else
	let error_day = 1
end

while error_day
      let dat_char = fr_dat
      getstr ( dat_char , dd_mm , 1 , 5 )
      switch dd_mm
            case "01.01"    /* feste Feiertage! */
            case "06.01"
            case "25.12"
            case "26.12"
                 let error_day = 1
                 let fr_dat = fr_dat - 1
                 break

            case "01.05"    /* feste Feiertage; nicht in allen Laendern! */
            case "03.10"
            case "24.12"
            case "31.12"
                 let error_day = 0
                 perform disp_msg ( 614 , 1 )
                 break

            otherwise
                 let error_day = 0
                 break

      end /*switch*/
      if weekday(fr_dat) = 0 /* sonntag */
            let error_day = 1
            let fr_dat = fr_dat - 1
      end /* if */
end

return (fr_dat)

end /* procedure datum_comp */
