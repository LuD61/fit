#pragma once
#include "CtrlGrid.h"
#include "afxwin.h"
#include "afxcmn.h"
#include "MaTreeCtrl.h"
#include "ZerlTreeData.h"
#include "UpdateEvent.h"
#include "PageUpdate.h"
#include "Page1.h"
#include "a_bas.h"
#include "a_mat.h"
#include "zerm.h"
#include "schnitt.h"
#include "WorkDB.h"
#include "WorkPrint.h"



// CMaTree-Dialogfeld

class CMaTree : public CDialog, public CUpdateEvent
{
	DECLARE_DYNAMIC(CMaTree)

public:
	CMaTree(CWnd* pParent = NULL);   // Standardkonstruktor
	CImageList          *pImageList;
	virtual ~CMaTree();

// Dialogfelddaten
	enum { IDD = IDD_MATREE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();    // DDX/DDV-Unterstützung
    virtual BOOL PreTranslateMessage(MSG* pMsg);
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 
	virtual void OnSize (UINT nType, int cx, int cy);

	DECLARE_MESSAGE_MAP()
	BOOL ItemToZerm (void);
public:
	HTREEITEM OldHItem ;

	CMaTreeCtrl m_MaTree;
	CPage1 *Page1;
//	CZerlTreeData ZerlTreeData;
	CPageUpdate *PageUpdate;

	CFont Font;
	CFont LFont;
	CCtrlGrid CtrlGrid;
	COLORREF DlgBkColor;
	HBRUSH DlgBrush;
	CStatic m_LMaTree;
	A_BAS_CLASS *A_bas;
	A_MAT_CLASS *A_mat;
	ZERM_CLASS *Zerm;
	SCHNITT_CLASS *Schnitt;
	WORKDB_CLASS *Work;
	WORKPRINT_CLASS WorkPrint;
	BOOL PasteOK; 
	BOOL CopyOK; 
	virtual BOOL Print (HWND);

//    std::vector<CZerlTreeData *> ZerlTreeData;

	virtual void Update (short);
	virtual void Read ();
	virtual BOOL Write (BOOL);
	virtual void Delete ();
	virtual void OnCopy ();
	virtual void DeleteRow ();
	afx_msg void OnTvnSelchangedMaTree(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkZtree(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMRclkZtree(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMClickZtree(NMHDR *pNMHDR, LRESULT *pResult);
	BOOL HasFocus ();
	BOOL IstItemZerlegt ();
	short ItemToA_typ ();
	void FixVarSwitchen ();


};
