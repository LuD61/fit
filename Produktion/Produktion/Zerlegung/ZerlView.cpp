// Art1.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "Zerlegung.h"
#include "ZerlView.h"
#include "resource.h"

#define CXPLUS 38
#define CYPLUS 50

// CZerlView

IMPLEMENT_DYNCREATE(CZerlView, DbFormView)

CZerlView::CZerlView()
	: DbFormView(CZerlView::IDD)
{
	tabx = 0;
	taby = 0;
	StartSize = START_NORMAL;
	Cfg.SetProgName( _T("Zerlegung"));
	ReadCfg ();
	DlgBkColor = NULL;
	DlgBrush = NULL;
	ArchiveName = _T("Form.prp");
	Load ();
	dlg.DlgBkColor = DlgBkColor;
}

CZerlView::~CZerlView()
{
	Save ();
}

void CZerlView::DoDataExchange(CDataExchange* pDX)
{
	DbFormView::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CZerlView, DbFormView)
	ON_WM_SIZE ()
	ON_WM_CTLCOLOR ()
	ON_COMMAND(ID_FILE_SAVE, OnFileSave)
	ON_COMMAND(ID_BACK, OnBack)
//	ON_WM_DRAWITEM()
ON_COMMAND(ID_DELETE, OnDelete)
//ON_COMMAND(ID_INSERT, OnInsert)
ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
ON_COMMAND(ID_PRINT, OnFilePrint)
ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
//ON_COMMAND(ID_DELETEALL, OnDeleteall)
ON_COMMAND(ID_FILE_PRINT, OnFilePrint)
//ON_COMMAND(ID_PRINT_ALL, OnPrintAll)
ON_COMMAND(ID_FIRST, OnFirst)
ON_UPDATE_COMMAND_UI(ID_FIRST, OnUpdateFirst)
ON_COMMAND(ID_PRIOR, OnPrior)
ON_UPDATE_COMMAND_UI(ID_PRIOR, OnUpdatePrior)
ON_COMMAND(ID_NEXT, OnNext)
ON_UPDATE_COMMAND_UI(ID_NEXT, OnUpdateNext)
ON_COMMAND(ID_LAST, OnLast)
ON_UPDATE_COMMAND_UI(ID_LAST, OnUpdateLast)
//ON_COMMAND(ID_DLG_BACKGROUND, OnDlgBackground)
//ON_COMMAND(ID_CHOICEBACKGROUND, OnChoicebackground)
//ON_UPDATE_COMMAND_UI(ID_CHOICEBACKGROUND, OnUpdateChoicebackground)
//ON_COMMAND(ID_CHOICEA_DEFAULT, OnChoiceaDefault)
//ON_UPDATE_COMMAND_UI(ID_CHOICEA_DEFAULT, OnUpdateChoiceaDefault)
//ON_COMMAND(ID_FLAT_LAYOUT, OnFlatLayout)
//ON_UPDATE_COMMAND_UI(ID_FLAT_LAYOUT, OnUpdateFlatLayout)
//ON_COMMAND(ID_DLGFRAME_BACKGROUND, OnDlgframeBackground)
//ON_COMMAND(ID_CHOICE_OK, OnChoiceOk)
//ON_UPDATE_COMMAND_UI(ID_CHOICE_OK, OnUpdateChoiceOk)
ON_UPDATE_COMMAND_UI(ID_FILE_SAVE, OnUpdateFileSave)
ON_UPDATE_COMMAND_UI(ID_DELETE, OnUpdateDateiL)
ON_UPDATE_COMMAND_UI(ID_EDIT_PASTE, OnUpdatePaste)
ON_UPDATE_COMMAND_UI(ID_EDIT_COPY, OnUpdateCopy)
ON_UPDATE_COMMAND_UI(ID_PRINT, OnUpdateFileSave)
ON_COMMAND(ID_INFO, OnInfo)
ON_UPDATE_COMMAND_UI(ID_INFO, OnUpdateInfo)
END_MESSAGE_MAP()


// CZerlView-Diagnose

#ifdef _DEBUG
void CZerlView::AssertValid() const
{
	DbFormView::AssertValid();
}

void CZerlView::Dump(CDumpContext& dc) const
{
	DbFormView::Dump(dc);
}
#endif //_DEBUG


// CZerlView-Meldungshandler

void CZerlView::OnInitialUpdate()
{
	DbFormView::OnInitialUpdate();


	dlg.Construct (_T(""), this);
	Page1.Construct (IDD_PAGE1);
	Page1.Frame = this;
	Page1.Update = this;
//	Page1.HideButtons = TRUE;
	dlg.AddPage (&Page1);

	Page2.Construct (IDD_PAGE2);
	Page2.Frame = this;
	Page2.Update = this;
	Page2.ZerlZerllist.m_Zerl.Page1 = &Page1;
	Page2.ZerlZerllist.m_ZerlList.Page1 = &Page1;
	Page2.MaTree.Page1 = &Page1;
//	Page2.HideButtons = TRUE;
	dlg.AddPage (&Page2);

	Page3.Construct (IDD_PAGE3);
	Page3.Frame = this;
	Page3.Basis = &Page1;
	Page3.Update = this;
	dlg.AddPage (&Page3);

	Page4.Construct (IDD_PAGE4);
	Page4.Frame = this;
	Page4.Basis = &Page1;
	Page4.Update = this;
	dlg.AddPage (&Page4);


    dlg.m_psh.dwSize = sizeof (dlg.m_psh);
	dlg.Create (this, WS_CHILD | WS_VISIBLE);
    Page1.SetFocus (); 

	CRect cRect;
    dlg.GetClientRect (&cRect);
    dlg.MoveWindow (0, 0, cRect.right, cRect.bottom);
	CSize Size = GetTotalSize ();
	if (Size.cy < (cRect.bottom) + 10)
	{
		Size.cy = cRect.bottom + 10;
	}
	if (Size.cx < (cRect.right) + 10)
	{
		Size.cx = cRect.right + 10;
	}
	SetScrollSizes (MM_TEXT, Size);
	CRect pRect;
	CRect pcRect;
	CWnd *ChildFrame = GetParent ();
	CWnd *MainFrame = ChildFrame->GetParent ();
	ChildFrame->GetWindowRect (&pRect);
	MainFrame->ScreenToClient (&pRect);
	MainFrame->GetClientRect (&pcRect);
	BOOL Resize = FALSE;
	if (pcRect.right > pRect.left + cRect.right + CXPLUS)
	{
		pRect.right  = pRect.left + cRect.right + CXPLUS;
		Resize = TRUE;
	}
	if (pcRect.bottom > pRect.top + cRect.bottom + CYPLUS)
	{
		pRect.bottom = pRect.top + cRect.bottom + CYPLUS;
		Resize = TRUE;
	}
	if (Resize)
	{
		GetParent ()->MoveWindow (&pRect, TRUE);
	}
}


HBRUSH CZerlView::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}

	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return DbFormView::OnCtlColor (pDC, pWnd,nCtlColor);
}


void CZerlView::OnSize(UINT nType, int cx, int cy)
{
	if (m_hWnd == NULL || !IsWindow (m_hWnd))
	{
		return;
	}
	if (!IsWindow (dlg.m_hWnd))
	{
		return;
	}
	CRect frame;
	CRect pRect;
	dlg.GetWindowRect (&pRect);
	dlg.GetParent ()->ScreenToClient (&pRect);
	dlg.MoveWindow (tabx, taby, cx, cy);
	CTabCtrl *tab = dlg.GetTabControl ();
	Page1.Frame = this;
	Page1.GetWindowRect (&pRect);
	dlg.ScreenToClient (&pRect);

	frame = pRect;
	frame.top = 10;
	frame.left = 10;
    frame.right = cx - 10 - tabx;
	frame.bottom = cy - 10 - taby;
	tab->MoveWindow (&frame);
	frame.top += 20;
	frame.left += 2;
    frame.right -= 10;
	frame.bottom -= 10;

	int page = dlg.GetActiveIndex ();
	if (page == 0)
	{
		Page1.MoveWindow (&frame);
	}
	else if (page == 1)
	{
		Page2.MoveWindow (&frame);
	}
}

void CZerlView::ReadCfg ()
{
    char cfg_v [256];

    if (Cfg.GetCfgValue ("StartSize", cfg_v) == TRUE)
    {
			StartSize = atoi (cfg_v);
    }
	Cfg.CloseCfg ();
}

void CZerlView::OnFileSave()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
     dlg.Write ();
//	 GetParent()-> SetWindowText (_T("Artikelstamm: gespeichert"));
}

void CZerlView::OnBack()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
     dlg.StepBack ();
//	 GetParent()-> SetWindowText (_T("Artikelstamm: Aktion r�ckg�ngig"));
}

void CZerlView::OnDelete()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	dlg.OnDelete ();
}

void CZerlView::OnInsert()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	dlg.Insert ();
}

void CZerlView::OnEditCopy()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	dlg.OnCopy ();
}

void CZerlView::OnEditPaste()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	dlg.OnPaste ();
}

void CZerlView::OnDeleteall()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	dlg.DeleteAll ();
}

void CZerlView::OnFilePrint()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	dlg.OnPrint ();
}

void CZerlView::OnPrintAll()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	dlg.PrintAll ();
}

void CZerlView::OnFirst()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
   CPage1 *Page = (CPage1 *) dlg.GetPage (0);
//   Page->FirstRec ();
}

void CZerlView::OnUpdateFirst(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
   CPage1 *Page = (CPage1 *) dlg.GetPage (0);
   /**
   if (!Page->ChoiceHasElements ())
   {
	   pCmdUI->Enable (FALSE);
   }
   else
   {
	   pCmdUI->Enable ();
   }
   ****/
}

void CZerlView::OnPrior()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
   CPage1 *Page = (CPage1 *) dlg.GetPage (0);
//   Page->PriorRec ();
}

void CZerlView::OnUpdatePrior(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
   CPage1 *Page = (CPage1 *) dlg.GetPage (0);
   /*
   if (!Page->ChoiceHasElements ())
   {
	   pCmdUI->Enable (FALSE);
   }
   else
   {
	   pCmdUI->Enable ();
   }
   */
}

void CZerlView::OnNext()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
   CPage1 *Page = (CPage1 *) dlg.GetPage (0);
//   Page->NextRec ();
}

void CZerlView::OnUpdateNext(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
   CPage1 *Page = (CPage1 *) dlg.GetPage (0);
   /**
   if (!Page->ChoiceHasElements ())
   {
	   pCmdUI->Enable (FALSE);
   }
   else
   {
	   pCmdUI->Enable ();
   }
   */
}

void CZerlView::OnLast()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
   CPage1 *Page = (CPage1 *) dlg.GetPage (0);
//   Page->LastRec ();
}

void CZerlView::OnUpdateLast(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
   CPage1 *Page = (CPage1 *) dlg.GetPage (0);
   /*
   if (!Page->ChoiceHasElements ())
   {
	   pCmdUI->Enable (FALSE);
   }
   else
   {
	   pCmdUI->Enable ();
   }
   */
}


void CZerlView::OnDlgBackground()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
/*
   CPage1 *Page = (CPage1 *) dlg.GetPage (0);
   if (Page != NULL)
   {
		Page->OnDlgBackground ();
   }
*/

	CColorDialog cdlg;
	cdlg.m_cc.Flags |= CC_FULLOPEN | CC_RGBINIT;
	COLORREF Color = GetSysColor (COLOR_3DFACE);
	cdlg.m_cc.rgbResult = Color;
	if (cdlg.DoModal() == IDOK)
	{
		COLORREF DlgBkColor = cdlg.GetColor();

		for (int i = 0; i < dlg.GetPageCount (); i ++)
		{
			CPage1 *Page = (CPage1 *) dlg.GetPage (i);
			Page->DlgBkColor = DlgBkColor;
	  	    DeleteObject (Page->DlgBrush);
		    Page->DlgBrush = NULL;
			if (IsWindow (Page->m_hWnd))
			{
				Page->InvalidateRect (NULL);
			}
		}
	}

}

void CZerlView::OnChoicebackground()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
   CPage1 *Page = (CPage1 *) dlg.GetPage (0);
   if (Page != NULL)
   {
		Page->OnChoicebackground ();
   }
}

void CZerlView::OnUpdateChoicebackground(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
/*
   CPage1 *Page = (CPage1 *) dlg.GetPage (0);
   if (Page->Choice == NULL)
   {
	   pCmdUI->Enable (FALSE);
   }
   else
   {
	   pCmdUI->Enable ();
   }
*/
}

void CZerlView::OnChoiceaDefault()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
   CPage1 *Page = (CPage1 *) dlg.GetPage (0);
   if (Page != NULL)
   {
//		Page->OnChoiceaDefault();
   }
}

void CZerlView::OnUpdateChoiceaDefault(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
   CPage1 *Page = (CPage1 *) dlg.GetPage (0);
   /*
   if (!Page->ChoiceHasElements ())
   {
	   pCmdUI->Enable (FALSE);
   }
   else
   {
	   pCmdUI->Enable ();
   }
   */
}


void CZerlView::OnFlatLayout()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.

   CPage1 *Page = (CPage1 *) dlg.GetPage (0);
   if (Page != NULL)
   {
//		Page->OnFlatLayout();
   }
}

void CZerlView::OnUpdateFlatLayout(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
    CPage1 *Page = (CPage1 *) dlg.GetPage (0);
	if (Page->FlatLayout)
	{
		pCmdUI->SetCheck (1);
	}
	else
	{
		pCmdUI->SetCheck (0);
	}
}


void CZerlView::OnDlgframeBackground()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	CColorDialog dlg;
	dlg.m_cc.Flags |= CC_FULLOPEN | CC_RGBINIT;
//	dlg.m_cc.rgbResult = KprParams.LeftColor;
	if (dlg.DoModal() == IDOK)
	{
		DlgBkColor = dlg.GetColor();
		DeleteObject (DlgBrush);
		DlgBrush = NULL;
		this->dlg.DlgBkColor = DlgBkColor;
		this->dlg.DlgBrush = NULL;
		InvalidateRect (NULL);
		this->dlg.Invalidate ();
	}
}

void CZerlView::OnChoiceOk()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
   CPage1 *Page = (CPage1 *) dlg.GetPage (0);
   if (Page != NULL)
   {
	    if (Page->HideOK)
		{
			Page->HideOK = FALSE;
		}
		else
		{
			Page->HideOK = TRUE;
		}
   }
}

void CZerlView::OnUpdateChoiceOk(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
    CPage1 *Page = (CPage1 *) dlg.GetPage (0);
	if (Page->HideOK)
	{
		pCmdUI->SetCheck (1);
	}
	else
	{
		pCmdUI->SetCheck (0);
	}
}

// Abgeleitete Methoden von PageUpdate

void CZerlView::Show ()
{
   for (int i = 1; i < dlg.GetPageCount (); i ++)
   {
		CPage1 *Page = (CPage1 *) dlg.GetPage (i);
		if (IsWindow (Page->m_hWnd))
		{
			Page->UpdatePage ();
		}
   }
}

void CZerlView::Get ()
{
   for (int i = 1; i < 2; i ++)
   {
		CPage1 *Page = (CPage1 *) dlg.GetPage (i);
		if (IsWindow (Page->m_hWnd))
		{
			Page->GetPage ();
		}
   }
}

void CZerlView::Back ()
{
     dlg.StepBack ();
}

BOOL CZerlView::Write ()
{
     return dlg.Write ();
}

void CZerlView::Page2Write ()
{
     dlg.Page2Write ();
}

void CZerlView::Delete ()
{
	dlg.Delete ();
}

void CZerlView::OnUpdateFileSave(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
   CPage1 *Page = (CPage1 *) dlg.GetPage (0);
   if (Page->m_Mdn.IsWindowEnabled ())
   {
	   pCmdUI->Enable (FALSE);
   }
   else
   {
	   pCmdUI->Enable ();
   }
}

void CZerlView::OnUpdateDateiL(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
   CPage1 *Page = (CPage1 *) dlg.GetPage (0);
   if (Page->m_Mdn.IsWindowEnabled ())
   {
	   pCmdUI->Enable (FALSE);
   }
   else
   {
	   pCmdUI->Enable ();
   }
}
void CZerlView::OnUpdatePaste(CCmdUI *pCmdUI)
{

   CPage2 *Page = (CPage2 *) dlg.GetPage (1);
   pCmdUI->Enable (FALSE);
   if (Page->MaTree.PasteOK == TRUE)
   {
		CString Message = _T("");
		CStrFuncs::FromClipboard (Message, CF_TEXT);
		CToken t (Message, _T("="));
		if (t.GetAnzToken () > 3)
		{
			CString c = t.GetToken (0);
			if (c == _T("SchnittNr"))
			{
			   pCmdUI->Enable ();
			}
		}
   }
   
}
void CZerlView::OnUpdateCopy(CCmdUI *pCmdUI)
{
   CPage2 *Page = (CPage2 *) dlg.GetPage (1);
   if (Page->MaTree.CopyOK == FALSE)
   {
	   pCmdUI->Enable (FALSE);
   }
   else
   {
	   pCmdUI->Enable ();
   }
}

void CZerlView::UpdateAll ()
{
	GetDocument ()->UpdateAllViews (this);
}
void CZerlView::OnInfo()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	 CPage1 *Page = (CPage1 *) dlg.GetPage (0);
//	 Page->OnInfo ();
 }

void CZerlView::OnUpdateInfo(CCmdUI *pCmdUI) 
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
   CPage1 *Page = (CPage1 *) dlg.GetPage (0);
   if (Page->m_Mdn.IsWindowEnabled ())
   {
	   pCmdUI->Enable (FALSE);
   }
   else
   {
	   pCmdUI->Enable ();
   }
}

