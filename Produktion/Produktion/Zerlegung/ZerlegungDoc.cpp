// ZerlegungDoc.cpp : Implementierung der Klasse CZerlegungDoc
//

#include "stdafx.h"
#include "Zerlegung.h"

#include "ZerlegungDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CZerlegungDoc

IMPLEMENT_DYNCREATE(CZerlegungDoc, CDocument)

BEGIN_MESSAGE_MAP(CZerlegungDoc, CDocument)
END_MESSAGE_MAP()


// CZerlegungDoc Erstellung/Zerst�rung

CZerlegungDoc::CZerlegungDoc()
{
	// TODO: Hier Code f�r One-Time-Konstruktion einf�gen

}

CZerlegungDoc::~CZerlegungDoc()
{
}

BOOL CZerlegungDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: Hier Code zur Reinitialisierung einf�gen
	// (SDI-Dokumente verwenden dieses Dokument)

	return TRUE;
}




// CZerlegungDoc Serialisierung

void CZerlegungDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: Hier Code zum Speichern einf�gen
	}
	else
	{
		// TODO: Hier Code zum Laden einf�gen
	}
}


// CZerlegungDoc Diagnose

#ifdef _DEBUG
void CZerlegungDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CZerlegungDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CZerlegungDoc-Befehle
