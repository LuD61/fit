#ifndef _CHOICEKOSTART_DEF
#define _CHOICEKOSTART_DEF

#include "ChoiceX.h"
#include "KostArtList.h"
#include <vector>

class CChoiceKostArt : public CChoiceX
{
    private :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
      
    public :
		CString Where;
	    std::vector<CKostArtList *> KostArtList;
      	CChoiceKostArt(CWnd* pParent = NULL);   // Standardkonstruktor
      	~CChoiceKostArt(); 
        virtual void BezLabel (CListCtrl *);
        virtual void NumLabel (CListCtrl *);
        virtual void FillList (void);
        void SearchNum (CListCtrl *,  LPTSTR);
        void SearchKostArtBz (CListCtrl *, LPTSTR);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
		CKostArtList *GetSelectedText ();
        int GetPtBez (LPTSTR, LPTSTR);
		void DestroyList ();
};
#endif
