// Zerlegung.h : Hauptheaderdatei f�r die Zerlegung-Anwendung
//
#pragma once

#ifndef __AFXWIN_H__
	#error 'stdafx.h' muss vor dieser Datei in PCH eingeschlossen werden.
#endif

#include "resource.h"       // Hauptsymbole


#define CLICKED_KOST_DETAILS 90
#define CLICKED_VK_DETAILS 91
#define CLICKED_SINGLE_EXPAND 92


// CZerlegungApp:
// Siehe Zerlegung.cpp f�r die Implementierung dieser Klasse
//

class CZerlegungApp : public CWinApp
{
public:
	CZerlegungApp();


// �berschreibungen
public:
	virtual BOOL InitInstance();

// Implementierung
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
	afx_msg void OnZerlegung();
};

extern CZerlegungApp theApp;
