#ifndef _ZERL_TREE_DATA_DEF
#define _ZERL_TREE_DATA_DEF
#pragma once

class CZerlTreeData
{
public:
    HTREEITEM Item;
	short mdn;
	long schnitt;
	long zerm_mat;
	long zerm_aus_mat;
	short zerm_aus_a_typ;
	double zerm_gew;
	double zerm_gew_ant;
	double mat_o_b;
	double dm_100;
	double fwp;
	short a_typ;
	CString fix;
   CString          a_kz;
   CString          zerm_teil;
   double         hk_vollk;
   double         hk_teilk;
   short          delstatus;
   double         vollkosten_abs;
   double         vollkosten_proz;
   double         teilkosten_abs;
   double         teilkosten_proz;
   long           a_z_f;
   double         vk_pr;
   double         kosten_zutat;
   double         kosten_vpk;

   double         kosten;
   double         pr_zutat;
   double         pr_vpk;
   double         pr_kosten;
   double         pr_vk_netto;
   double         kosten_fix;
   double         pr_vk_eh;
	CString zerm_mat_bz1;
	CString zerm_mat_bz2;
	double zerm_gew_ant_parent;
	double kosten_gh;
	double kosten_eh;
	double fwp_org;
   double         sk_vollk;
   double         sk_teilk;
   double         fil_ek_vollk;
   double         fil_ek_teilk;
   double         fil_vk_vollk;
   double         fil_vk_teilk;
   double         gh1_vollk;
   double         gh1_teilk;
   double         gh2_vollk;
   double         gh2_teilk;

	CZerlTreeData(void);
	CZerlTreeData(HTREEITEM,
		short,
		long,
		long,
		long,
		short,
		double,
		double,
		double,
		double,
		double,
		LPTSTR,

		LPTSTR,  
		LPTSTR,  
		double,
		double,
		short ,
		double,
		double,
		double,
		double,
		long  ,
		double,
		double,
		double,
		short,
	    double ,
	    double ,
		double ,
	    double ,
	    double ,
	    double ,
	    double ,

		LPTSTR,
		LPTSTR,
		double,
		double,
		double,
	    double ,
	    double ,
		double ,
	    double ,
	    double ,
	    double ,
	    double ,
	    double ,
	    double ,
	    double ,
		double
		);
	~CZerlTreeData(void);
};


class CZerlTreeGt
{
public:
    HTREEITEM Item;
	long zerm_mat;
	long zerm_aus_mat;
	short ebene;
	CString fix;

	CZerlTreeGt(void);
	CZerlTreeGt(HTREEITEM,
		long,
		long,
		short,
		LPTSTR
		);
	~CZerlTreeGt(void);

};
class CZerlTreeGt2
{
public:
    HTREEITEM Item;
	long zerm_mat;
	long zerm_aus_mat;
	short ebene;
	CString fix;

	CZerlTreeGt2(void);
	CZerlTreeGt2(HTREEITEM,
		long,
		long,
		short,
		LPTSTR
		);
	~CZerlTreeGt2(void);

};
class CZerlTreeMatKost
{
public:
    HTREEITEM Item;
	long zerm_mat;
	short ebene;

	CZerlTreeMatKost(void);
	CZerlTreeMatKost(HTREEITEM,
		long,
		short
		);
	~CZerlTreeMatKost(void);

};
class CZerlTreeVkArtikel
{
public:
    HTREEITEM Item;
    HTREEITEM PItem;
    HTREEITEM CItem;

	CZerlTreeVkArtikel(void);
	CZerlTreeVkArtikel(HTREEITEM,
		HTREEITEM,
		HTREEITEM
		);
	~CZerlTreeVkArtikel(void);

};
#endif
