#ifndef _CHOICEKALK_DEF
#define _CHOICEKALK_DEF

#include "ChoiceX.h"
#include "KalkList.h"
#include <vector>

class CChoiceKalk : public CChoiceX
{
    private :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
      
    public :
	    std::vector<CKalkList *> KalkList;
      	CChoiceKalk(CWnd* pParent = NULL);   // Standardkonstruktor
      	~CChoiceKalk(); 
        virtual void BezLabel (CListCtrl *);
        virtual void NumLabel (CListCtrl *);
        virtual void FillList (void);
        void SearchNum (CListCtrl *,  LPTSTR);
        void SearchName (CListCtrl *, LPTSTR);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
		CKalkList *GetSelectedText ();
//        int GetPtBez (LPTSTR, LPTSTR);
		void DestroyList ();
};
#endif
