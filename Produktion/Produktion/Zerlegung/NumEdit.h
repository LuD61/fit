#pragma once
#include "afxwin.h"

class CNumEdit :
	public CEdit
{
	DECLARE_DYNAMIC(CNumEdit)
public:
	CNumEdit(void);
	~CNumEdit(void);
protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnSetFocus (CWnd *);
public:
	afx_msg void OnNMDblclkZtree(NMHDR *pNMHDR, LRESULT *pResult);
};
