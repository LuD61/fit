#pragma once
#include "DbPropertyPage.h"
#include "MaTree.h"
#include "Zerl.h"
#include "ZerlZerllist.h"
#include "SplitPane.h"



// CPage2-Dialogfeld

class CPage2 : public CDbPropertyPage
{
	DECLARE_DYNAMIC(CPage2)

public:
	CPage2();
	virtual ~CPage2();

// Dialogfelddaten
	enum { IDD = IDD_PAGE2 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog();
	virtual void OnSize (UINT, int, int);
    virtual BOOL PreTranslateMessage(MSG* pMsg);
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 
	virtual BOOL Write (BOOL);
	virtual BOOL Back ();
	virtual void OnCopy ();
	virtual void OnPaste ();
	virtual BOOL Print (HWND);



	DECLARE_MESSAGE_MAP()
public:
	CWnd *Frame;
	CMaTree MaTree;
	CZerl Zerl;
	CZerlZerllist ZerlZerllist;
	CSplitPane SplitPane;
	virtual void UpdatePage ();
	virtual BOOL OnSetActive ();
	virtual void OnDelete ();


};
