#ifndef _LINE_DEF
#define _LINE_DEF
#pragma once
#include "afxwin.h"

class CLine :
	public CStatic
{
public:
	CLine(void);
	~CLine(void);
protected:
	virtual void DrawItem (LPDRAWITEMSTRUCT lpDrawItemStruct);
};
#endif
