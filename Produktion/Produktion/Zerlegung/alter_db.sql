alter table zerm add (kosten_zutat decimal(7,3),
kosten_vpk decimal(7,3));
alter table schnitt add (schlachtkalk integer,
schlachtart decimal(13,0),ek decimal(8,4));
alter table schnitt modify schnitt_hin char(200);


alter table zerm add a_typ smallint;
update zerm set a_typ = 5 where 1 = 1;
drop index i01zerm;
create unique index i01zerm on zerm (mdn,schnitt,zerm_aus_mat,zerm_mat,a_typ);

alter table zerm add kost_bz char(24);

{neu ab 24.07.07}
alter table zerm add kosten decimal(9,3);
alter table zerm add pr_zutat decimal(9,4);
alter table zerm add pr_vpk decimal(9,4);
alter table zerm add pr_kosten decimal(9,4);
alter table zerm add pr_vk_netto decimal(9,4);
alter table zerm add kosten_fix decimal(8,3);
