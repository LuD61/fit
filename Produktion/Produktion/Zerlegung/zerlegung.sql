alter table zerm add (kosten_zutat decimal(7,3),
kosten_vpk decimal(7,3));
alter table schnitt add (schlachtkalk integer,
schlachtart decimal(13,0),ek decimal(8,4));
alter table schnitt modify schnitt_hin char(200);

create table "fit".zerm_mat_v 
  (
    mdn smallint,
    schnitt smallint,
    zerm_aus_mat integer,
    zerm_mat integer,
    fwp decimal(5,2),
    zerm_gew_ant decimal(5,2),
    mat_o_b decimal(7,3),
    hk_vollk decimal(7,3),
    hk_teilk decimal(7,3),
    dm_100 decimal(9,3),
    kosten_zutat decimal(7,3),
    kosten_vpk decimal(7,3)
  ) with crcols  in fit_dat extent size 32 next size 256 lock mode row;
revoke all on "fit".zerm_mat_v from "public";
create unique index "fit".i01zerm_mat_v on "fit".zerm_mat_v (mdn,schnitt,zerm_mat,zerm_aus_mat);

alter table zerm add a_typ smallint;
update zerm set a_typ = 5 where 1 = 1;
drop index i01zerm;
create unique index i01zerm on zerm (mdn,schnitt,zerm_aus_mat,zerm_mat,a_typ);
alter table zerm add kost_bz char(24);

{ am 24.7.07 ge�ndert: }
	alter table zerm add kosten decimal(9,3);
	alter table zerm add pr_zutat decimal(9,4);
	alter table zerm add pr_vpk decimal(9,4);
	alter table zerm add pr_kosten decimal(9,4);
	alter table zerm add pr_vk_netto decimal(9,4);
	alter table zerm add kosten_fix decimal(8,3);
	alter table zerm add pr_vk_eh decimal(9,4);

drop table zerm_mat_v;
create table "fit".zerm_mat_v 
  (
    mdn smallint,
    schnitt smallint,
    zerm_aus_mat integer,
    zerm_mat integer,
    fwp decimal(8,2),
    zerm_gew decimal(8,3),
    zerm_gew_ant decimal(8,2),
    mat_o_b decimal(9,3),
    hk_vollk decimal(9,3),
    hk_teilk decimal(9,3),
    dm_100 decimal(11,3),
    kosten_zutat decimal(9,3),
    kosten_vpk decimal(9,3),
    teilkosten_abs decimal(9,3),
    vk_pr decimal(9,3),
    kost_mat integer,
    kost_mat_bz char(24),
    a_typ smallint
  ) with crcols  in fit_dat extent size 32 next size 256 lock mode row;
revoke all on "fit".zerm_mat_v from "public";

create unique index "fit".i01zerm_mat_v on "fit".zerm_mat_v (mdn,
    schnitt,zerm_mat,zerm_aus_mat,a_typ);


{ am 18.8.07 ge�ndert: }
	alter table zerm add kosten_gh decimal(9,3);
	alter table zerm add kosten_eh decimal(9,3);
	update zerm set kosten_gh = 0 where kosten_gh is null;
	update zerm set kosten_eh = 0 where kosten_eh is null;
	
{ am 24.8.07 ge�ndert: }
alter table zerm_mat_v add (pr_vk_eh decimal(9,4)); 
{ am 24.10.07 ge�ndert: }
alter table zerm add (lfd smallint);
alter table zerm add (list_lfd smallint);
alter table zerm add (list_ebene smallint);

{ am 05.11.07 ge�ndert: }
alter table kost_art_zerl add vkost_fix decimal(8,2);

{ am 06.11.07 ge�ndert: }
alter table schnitt modify schnitt integer;
alter table schnitt modify vergl_schnitt integer;
alter table zerm modify schnitt integer;
alter table zerm_mat_v modify schnitt integer;

{ am 17.12.07 ge�ndert: }
alter table zerm add zerm_aus_a_typ smallint;

{ am 23.04.08 ge�ndert: }
alter table zerm add fwp_org decimal(5,2);
alter table schnitt add (deckung_proz decimal(8,4),
	summe_wert decimal(8,2), --menge*Preis
	menge_vt decimal(8,2), --menge Verkaufsteile
	menge_vm decimal(8,2), --menge Verarbeitungsmat.
	menge_kk decimal(8,2), --menge keine Kostentr�ger
	wert_vt decimal(8,2), --wert Verkaufsteile
	wert_vm decimal(8,2), --wert Verarbeitungsmat.
	wert_kk decimal(8,2));  --wert keine Kostentr�ger
update schnitt set deckung_proz = 0 where deckung_proz is null;	

{ am 20.05.08 ge�ndert: }
create table "fit".schnittkost_g   
  (
    mdn smallint,
    schnitt integer,
    grundkost_art integer,  -- Kostenart f�r Zerlegegrundkosten
    grundkostenwert decimal(8,2),   
    grobkost_art integer,  -- Kostenart f�r Grobteilzerlegung 
    grobkostenwert decimal(8,2)
 
  ) in fit_dat extent size 32 next size 256 lock mode row;
create unique index "fit".i01schnittkost_g on "fit".schnittkost_g (mdn,schnitt);

create table "fit".schnittkost_f    
  (                      --Kosten Feinzerlegung (korrespondiert mit ptab feinzerlegung)
  			 -- wenn ptab nicht da, wird stattdessen a_kz genommen
    mdn smallint,
    schnitt integer,
    fein_ptlfnr integer,
    fein_ptwert char(3),
    fein_kost_art integer,
    fein_wert decimal(8,2)
    ) in fit_dat extent size 32 next size 256 lock mode row;   
    
create unique index "fit".i01schnittkost_f on "fit".schnittkost_f (mdn,schnitt,fein_ptwert);
    

{ am 23.05.09 ge�ndert: }
alter table zerm 
	add (sk_vollk decimal(7,3),
		 sk_teilk decimal (7,3),
		 fil_ek_vollk decimal (7,3),
		 fil_ek_teilk decimal (7,3),
		 fil_vk_vollk decimal (7,3),
		 fil_vk_teilk decimal (7,3),
		 gh1_vollk decimal (7,3),
		 gh1_teilk decimal (7,3),
		 gh2_vollk decimal (7,3),
		 gh2_teilk decimal (7,3))
;		 
alter table zerm_mat_v 
	add (sk_vollk decimal(7,3),
		 sk_teilk decimal (7,3),
		 fil_ek_vollk decimal (7,3),
		 fil_ek_teilk decimal (7,3),
		 fil_vk_vollk decimal (7,3),
		 fil_vk_teilk decimal (7,3),
		 gh1_vollk decimal (7,3),
		 gh1_teilk decimal (7,3),
		 gh2_vollk decimal (7,3),
		 gh2_teilk decimal (7,3))
;	
alter table schnitt
	add (koart_grund smallint,
		koart_grob smallint,
		koart_vm smallint,
		koart_vt smallint,
		koart_za smallint,
		koart_min smallint
	)
;
	 
		 



