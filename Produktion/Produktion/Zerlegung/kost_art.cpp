#include "stdafx.h"
#include "kost_art.h"

struct KOST_ART kost_art, kost_art_null;

void KOST_ART_CLASS::prepare (void)
{
            char *sqltext;

            sqlin ((short *)   &kost_art.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &kost_art.kost_art,  SQLSHORT, 0);
    sqlout ((short *) &kost_art.mdn,SQLSHORT,0);
    sqlout ((short *) &kost_art.fil,SQLSHORT,0);
    sqlout ((short *) &kost_art.kost_art,SQLSHORT,0);
    sqlout ((TCHAR *) kost_art.kost_art_bz,SQLCHAR,25);
    sqlout ((short *) &kost_art.delstatus,SQLSHORT,0);
    sqlout ((long *) &kost_art.erl_kto,SQLLONG,0);
    sqlout ((TCHAR *) kost_art.vkost_rechb,SQLCHAR,4);
    sqlout ((double *) &kost_art.vkost_wt,SQLDOUBLE,0);
    sqlout ((double *) &kost_art.faktor,SQLDOUBLE,0);
    sqlout ((DATE_STRUCT *) &kost_art.dat,SQLDATE,0);
    sqlout ((TCHAR *) kost_art.pers_nam,SQLCHAR,9);
    sqlout ((short *) &kost_art.mwst_schl,SQLSHORT,0);
    sqlout ((long *) &kost_art.ag,SQLLONG,0);
    sqlout ((double *) &kost_art.vkost_fix,SQLDOUBLE,0);
            cursor = sqlcursor (_T("select kost_art.mdn,  ")
_T("kost_art.fil,  kost_art.kost_art,  kost_art.kost_art_bz,  ")
_T("kost_art.delstatus,  kost_art.erl_kto,  kost_art.vkost_rechb,  ")
_T("kost_art.vkost_wt,  kost_art.faktor,  kost_art.dat,  ")
_T("kost_art.pers_nam,  kost_art.mwst_schl,  kost_art.ag,  ")
_T("kost_art.vkost_fix from kost_art ")

#line 13 "kost_art.rpp"
                                  _T("where mdn = ? and kost_art = ? "));
    sqlin ((short *) &kost_art.mdn,SQLSHORT,0);
    sqlin ((short *) &kost_art.fil,SQLSHORT,0);
    sqlin ((short *) &kost_art.kost_art,SQLSHORT,0);
    sqlin ((TCHAR *) kost_art.kost_art_bz,SQLCHAR,25);
    sqlin ((short *) &kost_art.delstatus,SQLSHORT,0);
    sqlin ((long *) &kost_art.erl_kto,SQLLONG,0);
    sqlin ((TCHAR *) kost_art.vkost_rechb,SQLCHAR,4);
    sqlin ((double *) &kost_art.vkost_wt,SQLDOUBLE,0);
    sqlin ((double *) &kost_art.faktor,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &kost_art.dat,SQLDATE,0);
    sqlin ((TCHAR *) kost_art.pers_nam,SQLCHAR,9);
    sqlin ((short *) &kost_art.mwst_schl,SQLSHORT,0);
    sqlin ((long *) &kost_art.ag,SQLLONG,0);
    sqlin ((double *) &kost_art.vkost_fix,SQLDOUBLE,0);
            sqltext = _T("update kost_art set ")
_T("kost_art.mdn = ?,  kost_art.fil = ?,  kost_art.kost_art = ?,  ")
_T("kost_art.kost_art_bz = ?,  kost_art.delstatus = ?,  ")
_T("kost_art.erl_kto = ?,  kost_art.vkost_rechb = ?,  ")
_T("kost_art.vkost_wt = ?,  kost_art.faktor = ?,  kost_art.dat = ?,  ")
_T("kost_art.pers_nam = ?,  kost_art.mwst_schl = ?,  kost_art.ag = ?,  ")
_T("kost_art.vkost_fix = ? ")

#line 15 "kost_art.rpp"
                                  _T("where mdn = ? and kost_art = ? ");
            sqlin ((short *)   &kost_art.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &kost_art.kost_art,  SQLSHORT, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &kost_art.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &kost_art.kost_art,  SQLSHORT, 0);
            test_upd_cursor = sqlcursor (_T("select kost_art from kost_art ")
                                  _T("where mdn = ? and kost_art = ? "));
            sqlin ((short *)   &kost_art.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &kost_art.kost_art,  SQLSHORT, 0);
            del_cursor = sqlcursor (_T("delete from kost_art ")
                                  _T("where mdn = ? and kost_art = ? "));
    sqlin ((short *) &kost_art.mdn,SQLSHORT,0);
    sqlin ((short *) &kost_art.fil,SQLSHORT,0);
    sqlin ((short *) &kost_art.kost_art,SQLSHORT,0);
    sqlin ((TCHAR *) kost_art.kost_art_bz,SQLCHAR,25);
    sqlin ((short *) &kost_art.delstatus,SQLSHORT,0);
    sqlin ((long *) &kost_art.erl_kto,SQLLONG,0);
    sqlin ((TCHAR *) kost_art.vkost_rechb,SQLCHAR,4);
    sqlin ((double *) &kost_art.vkost_wt,SQLDOUBLE,0);
    sqlin ((double *) &kost_art.faktor,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &kost_art.dat,SQLDATE,0);
    sqlin ((TCHAR *) kost_art.pers_nam,SQLCHAR,9);
    sqlin ((short *) &kost_art.mwst_schl,SQLSHORT,0);
    sqlin ((long *) &kost_art.ag,SQLLONG,0);
    sqlin ((double *) &kost_art.vkost_fix,SQLDOUBLE,0);
            ins_cursor = sqlcursor (_T("insert into kost_art (")
_T("mdn,  fil,  kost_art,  kost_art_bz,  delstatus,  erl_kto,  vkost_rechb,  vkost_wt,  ")
_T("faktor,  dat,  pers_nam,  mwst_schl,  ag,  vkost_fix) ")

#line 29 "kost_art.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?)")); 

#line 31 "kost_art.rpp"
}
