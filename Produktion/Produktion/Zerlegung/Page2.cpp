// Page2.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "Zerlegung.h"
#include "Page2.h"



// CPage2-Dialogfeld

IMPLEMENT_DYNAMIC(CPage2, CDbPropertyPage)
CPage2::CPage2()
	: CDbPropertyPage(CPage2::IDD)
{
}

CPage2::~CPage2()
{
}

void CPage2::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
}



BEGIN_MESSAGE_MAP(CPage2, CDbPropertyPage)
	ON_WM_SIZE ()
END_MESSAGE_MAP()


// CPage2-Meldungshandler
BOOL CPage2::OnInitDialog()
{
	ZerlZerllist.PageUpdate = Update;
	MaTree.PageUpdate = Update;
	CPropertyPage::OnInitDialog();
//	EanEmbListDlg.SetBasis (Basis);
	MaTree.DlgBkColor = DlgBkColor;
	MaTree.Create (IDD_MATREE, this);
	MaTree.ShowWindow (SW_SHOWNORMAL);
	MaTree.UpdateWindow ();


	ZerlZerllist.m_Zerl.DlgBkColor = DlgBkColor;
//	EanEmb.m_Emb.DlgBkColor = DlgBkColor;
//	EanEmb.SetBasis (Basis);
	ZerlZerllist.Create (IDD_ZERL_ZERLLIST, this);
	ZerlZerllist.ShowWindow (SW_SHOWNORMAL);
	ZerlZerllist.UpdateWindow ();
    ZerlZerllist.SetFocus ();  


	ZerlZerllist.m_Zerl.AddUpdateEvent (&MaTree);
	MaTree.AddUpdateEvent (&ZerlZerllist.m_Zerl);

	ZerlZerllist.m_ZerlList.AddUpdateEvent (&MaTree);
	ZerlZerllist.m_ZerlList.AddUpdateEvent (&ZerlZerllist.m_Zerl);
	ZerlZerllist.m_Zerl.AddUpdateEvent (&ZerlZerllist.m_ZerlList);
	MaTree.AddUpdateEvent (&ZerlZerllist.m_ZerlList);

//	EanEmb.m_Ean.AddUpdateEvent (&EanEmbListDlg.m_EanListDlg);
//	EanEmb.m_Ean.AddUpdateEvent (&EanEmbListDlg.m_EmbListDlg);
//	EanEmb.m_Emb.AddUpdateEvent (&EanEmbListDlg.m_EmbListDlg);
//	EanEmbListDlg.m_EanListDlg.AddUpdateEvent (&EanEmb.m_Ean);
//	EanEmbListDlg.m_EmbListDlg.AddUpdateEvent (&EanEmb.m_Emb);
	CRect pRect;
    GetParent ()->GetParent ()->GetWindowRect (&pRect);
    GetParent ()->GetParent ()->GetParent ()->ScreenToClient (&pRect);
    GetParent ()->MoveWindow (&pRect);

	SplitPane.Create (this, 0, 0, &MaTree, &ZerlZerllist, 31, CSplitPane::Horizontal);
	return TRUE;
}
void CPage2::OnSize (UINT nType, int cx, int cy) //PROBLEM OnSize wird nicht aufgerufen warum ??? (aus ZerlView)
{
	if (IsWindow (MaTree.m_hWnd)) 
	{
		CRect rect;
		MaTree.GetClientRect (&rect);
		MaTree.MoveWindow (0, 0, rect.right, cy);
		SplitPane.SetLength (cy);

		ZerlZerllist.GetWindowRect (&rect);
		ScreenToClient (&rect);
		rect.right = cx;
		rect.bottom = cy;
		ZerlZerllist.MoveWindow (&rect);
	}
}
HBRUSH CPage2::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}

	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return CDialog::OnCtlColor (pDC, pWnd,nCtlColor);
}

BOOL CPage2::PreTranslateMessage(MSG* pMsg)
{

	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :

			if (pMsg->wParam == VK_F5)
			{
				Update->Back ();
				return TRUE;
			}
	}

	return FALSE;
}
void CPage2::UpdatePage ()
{
	MaTree.Read ();
	ZerlZerllist.Update (0);
}

BOOL CPage2::OnSetActive ()
{
	UpdatePage ();
	return TRUE;
}


BOOL CPage2::Write (BOOL YesNo)
{
	int di = 0;
	return MaTree.Write (YesNo);
}
BOOL CPage2::Back ()
{
	int di = 0;
	MaTree.Write (TRUE);
	return TRUE;
}
void CPage2::OnDelete ()
{
	MaTree.Delete() ;

}
void CPage2::OnCopy ()
{
	MaTree.OnCopy() ;
}
void CPage2::OnPaste ()
{
	// nur Test 
	int di = 0;
//	CWinApp::OnFileNew();
	POSITION pos;
	CWinApp* pApp = AfxGetApp();
	pos = pApp->GetFirstDocTemplatePosition ();
	CDocTemplate *ZerlInstance = NULL; 
	CDocTemplate *dt = pApp->GetNextDocTemplate (pos);
	ZerlInstance = dt;
	CDocument *dc = dt->CreateNewDocument ();
	if (dc !=NULL)
	{
		dc->SetTitle (_T("Kopie Zerlegung"));
	}
	CFrameWnd *Frame = dt->CreateNewFrame (dc, NULL);
	if (Frame != NULL)
	{
		dt->InitialUpdateFrame (Frame, NULL, TRUE);
	}
}
BOOL CPage2::Print (HWND m_hwnd)
{
	MaTree.Print(m_hwnd) ;
	return TRUE;
}
