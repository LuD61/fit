#include "stdafx.h"
#include "schnittkostg.h"

struct SCHNITTKOST_G schnittkost_g, schnittkost_g_null;

void SCHNITTKOST_G_CLASS::prepare (void)
{
            char *sqltext;

            sqlin ((short *)   &schnittkost_g.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &schnittkost_g.schnitt,  SQLLONG, 0);
    sqlout ((short *) &schnittkost_g.mdn,SQLSHORT,0);
    sqlout ((long *) &schnittkost_g.schnitt,SQLLONG,0);
    sqlout ((long *) &schnittkost_g.grundkost_art,SQLLONG,0);
    sqlout ((double *) &schnittkost_g.grundkostenwert,SQLDOUBLE,0);
    sqlout ((long *) &schnittkost_g.grobkost_art,SQLLONG,0);
    sqlout ((double *) &schnittkost_g.grobkostenwert,SQLDOUBLE,0);
            cursor = sqlcursor (_T("select schnittkost_g.mdn,  ")
_T("schnittkost_g.schnitt,  schnittkost_g.grundkost_art,  ")
_T("schnittkost_g.grundkostenwert,  schnittkost_g.grobkost_art,  ")
_T("schnittkost_g.grobkostenwert from schnittkost_g ")

#line 13 "schnittkostg.rpp"
                                _T("where mdn = ? ")
			        _T("and schnitt = ?"));
    sqlin ((short *) &schnittkost_g.mdn,SQLSHORT,0);
    sqlin ((long *) &schnittkost_g.schnitt,SQLLONG,0);
    sqlin ((long *) &schnittkost_g.grundkost_art,SQLLONG,0);
    sqlin ((double *) &schnittkost_g.grundkostenwert,SQLDOUBLE,0);
    sqlin ((long *) &schnittkost_g.grobkost_art,SQLLONG,0);
    sqlin ((double *) &schnittkost_g.grobkostenwert,SQLDOUBLE,0);
            sqltext = _T("update schnittkost_g set ")
_T("schnittkost_g.mdn = ?,  schnittkost_g.schnitt = ?,  ")
_T("schnittkost_g.grundkost_art = ?,  ")
_T("schnittkost_g.grundkostenwert = ?,  ")
_T("schnittkost_g.grobkost_art = ?,  ")
_T("schnittkost_g.grobkostenwert = ? ")

#line 16 "schnittkostg.rpp"
                                _T("where mdn = ? ")
			        _T("and schnitt = ?");
            sqlin ((short *)   &schnittkost_g.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &schnittkost_g.schnitt,  SQLLONG, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &schnittkost_g.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &schnittkost_g.schnitt,  SQLLONG, 0);
            test_upd_cursor = sqlcursor (_T("select schnitt from schnittkost_g ")
                                _T("where mdn = ? ")
			        _T("and schnitt = ?"));
            sqlin ((short *)   &schnittkost_g.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &schnittkost_g.schnitt,  SQLLONG, 0);
            del_cursor = sqlcursor (_T("delete from schnittkost_g ")
                                _T("where mdn = ? ")
			        _T("and schnitt = ?"));
    sqlin ((short *) &schnittkost_g.mdn,SQLSHORT,0);
    sqlin ((long *) &schnittkost_g.schnitt,SQLLONG,0);
    sqlin ((long *) &schnittkost_g.grundkost_art,SQLLONG,0);
    sqlin ((double *) &schnittkost_g.grundkostenwert,SQLDOUBLE,0);
    sqlin ((long *) &schnittkost_g.grobkost_art,SQLLONG,0);
    sqlin ((double *) &schnittkost_g.grobkostenwert,SQLDOUBLE,0);
            ins_cursor = sqlcursor (_T("insert into schnittkost_g (")
_T("mdn,  schnitt,  grundkost_art,  grundkostenwert,  grobkost_art,  ")
_T("grobkostenwert) ")

#line 33 "schnittkostg.rpp"
                                    _T("values ")
                                    _T("(?,?,?,?,?,?)")); 

#line 35 "schnittkostg.rpp"
}
