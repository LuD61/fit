#ifndef _SCHLAKLKK_DEF
#define _SCHLAKLKK_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct SCHLAKLKK {
   short          mdn;
   long           schlklknr;
   TCHAR          schklk_bez[37];
   short          kalkart;
   long           anzahl;
   double         lebendgew;
   double         schlachtgew;
   double         kaltgew;
   double         preisek;
   double         nettowert;
   double         nettoprkg;
   double         zuabkg;
   double         ausbeute;
   double         verlust;
};
extern struct SCHLAKLKK schlaklkk, schlaklkk_null;

#line 8 "schlaklkk.rh"

class SCHLAKLKK_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               SCHLAKLKK schlaklkk;  
               SCHLAKLKK_CLASS () : DB_CLASS ()
               {
               }
};
#endif
