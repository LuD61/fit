// ZerlegungView.cpp : Implementierung der Klasse CZerlegungView
//

#include "stdafx.h"
#include "Zerlegung.h"

#include "ZerlegungDoc.h"
#include "ZerlegungView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define CXPLUS 38
#define CYPLUS 50


// CZerlegungView

IMPLEMENT_DYNCREATE(CZerlegungView, DbFormView)

BEGIN_MESSAGE_MAP(CZerlegungView, DbFormView)
		ON_WM_SIZE ()
// Standarddruckbefehle
//	ON_COMMAND(ID_FILE_PRINT, ONLLPrintt)
//	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
//	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
	ON_COMMAND(ID_FILE_SAVE, OnFileSave)
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVE, OnUpdateFileSave)
	ON_COMMAND(ID_BACK, OnBack)
END_MESSAGE_MAP()

// CZerlegungView Erstellung/Zerst�rung

CZerlegungView::CZerlegungView()
			   : DbFormView(CZerlegungView::IDD)
{
	tabx = 0;
	taby = 0;
	StartSize = START_NORMAL;
	Cfg.SetProgName( _T("Zerlegung"));
	ReadCfg ();
	DlgBkColor = NULL;
	DlgBrush = NULL;
	ArchiveName = _T("Form.prp");
	Load ();

}

CZerlegungView::~CZerlegungView()
{
}

void CZerlegungView::DoDataExchange(CDataExchange* pDX)
{
	DbFormView::DoDataExchange(pDX);
}

// CZerlegungView Diagnose

#ifdef _DEBUG
void CZerlegungView::AssertValid() const
{
	DbFormView::AssertValid();
}

void CZerlegungView::Dump(CDumpContext& dc) const
{
	DbFormView::Dump(dc);
}

#endif //_DEBUG


// CZerlegungView Meldungshandler

void CZerlegungView::OnInitialUpdate()
{
	DbFormView::OnInitialUpdate();

	dlg.Construct (_T(""), this);
	Page1.Construct (IDD_PAGE1);
	Page1.Frame = this;
	Page1.Update = this;
//	Page1.HideButtons = TRUE;
	dlg.AddPage (&Page1);

	Page2.Construct (IDD_PAGE2);
	Page2.Frame = this;
	Page2.Update = this;
	Page2.ZerlZerllist.Page1 = &Page1;
	Page2.ZerlZerllist.m_Zerl.Page1 = &Page1;
	Page2.MaTree.Page1 = &Page1;
//	Page2.HideButtons = TRUE;
	dlg.AddPage (&Page2);

	Page3.Construct (IDD_PAGE3);
	Page3.Frame = this;
	Page3.Update = this;
	dlg.AddPage (&Page3);

    dlg.m_psh.pszCaption = _T("Simple");
    dlg.m_psh.nStartPage = 0;


//    dlg.m_psh.dwFlags |= PSH_NOAPPLYNOW | PSH_PROPTITLE;
    dlg.m_psh.dwSize = sizeof (dlg.m_psh);
//	dlg.Create (this, WS_CHILD | WS_VISIBLE);
	dlg.Create (this, WS_CHILD | WS_VISIBLE );
	Page1.SetFocus (); 

	CRect cRect;
    dlg.GetClientRect (&cRect);
    dlg.MoveWindow (0, 0, cRect.right, cRect.bottom);
	CSize Size = GetTotalSize ();
	if (Size.cy < (cRect.bottom) + 10)
	{
		Size.cy = cRect.bottom + 10;
	}
	if (Size.cx < (cRect.right) + 10)
	{
		Size.cx = cRect.right + 10;
	}
	SetScrollSizes (MM_TEXT, Size);
	CRect pRect;
	CRect pcRect;
	CWnd *ChildFrame = GetParent ();
	CWnd *MainFrame = ChildFrame->GetParent ();
	ChildFrame->GetWindowRect (&pRect);
	MainFrame->ScreenToClient (&pRect);
	MainFrame->GetClientRect (&pcRect);
	BOOL Resize = FALSE;
	if (pcRect.right > pRect.left + cRect.right + CXPLUS)
	{
		pRect.right  = pRect.left + cRect.right + CXPLUS;
		Resize = TRUE;
	}
	if (pcRect.bottom > pRect.top + cRect.bottom + CYPLUS)
	{
		pRect.bottom = pRect.top + cRect.bottom + CYPLUS;
		Resize = TRUE;
	}
	if (Resize)
	{
		GetParent ()->MoveWindow (&pRect, TRUE);
	}
	GetParent ()->ShowWindow (SW_SHOWMAXIMIZED);  

}

void CZerlegungView::OnFileSave()
{
}

void CZerlegungView::OnUpdateFileSave(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
   CPage1 *Page = (CPage1 *) dlg.GetPage (0);
   if (Page->m_Mdn.IsWindowEnabled ())
   {
	   pCmdUI->Enable (FALSE);
   }
   else
   {
	   pCmdUI->Enable ();
   }
}

void CZerlegungView::OnBack()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
     CDbPropertyPage *Page = (CDbPropertyPage *) dlg.GetActivePage ();
     Page->StepBack ();
	 if (Page != &Page1)
	 {
		 dlg.SetActivePage (0);
	 }
}

void CZerlegungView::OnSize(UINT nType, int cx, int cy)
{
	if (m_hWnd == NULL || !IsWindow (m_hWnd))
	{
		return;
	}
	if (!IsWindow (dlg.m_hWnd))
	{
		return;
	}
	CRect frame;
	CRect pRect;
	dlg.GetWindowRect (&pRect);
	dlg.GetParent ()->ScreenToClient (&pRect);
	dlg.MoveWindow (tabx, taby, cx, cy);
	CTabCtrl *tab = dlg.GetTabControl ();
	Page1.Frame = this;
	Page1.GetWindowRect (&pRect);
	dlg.ScreenToClient (&pRect);

	frame = pRect;
	frame.top = 10;
	frame.left = 10;
    frame.right = cx - 10 - tabx;
	frame.bottom = cy - 10 - taby;
	tab->MoveWindow (&frame);
	frame.top += 20;
	frame.left += 2;
    frame.right -= 10;
	frame.bottom -= 10;

	int page = dlg.GetActiveIndex ();
	if (page == 0)
	{
		Page1.MoveWindow (&frame);
	}
	else if (page == 1)
	{
		Page2.MoveWindow (&frame);
	}
}

void CZerlegungView::ReadCfg ()
{
    char cfg_v [256];

    if (Cfg.GetCfgValue ("StartSize", cfg_v) == TRUE)
    {
			StartSize = atoi (cfg_v);
    }
	Cfg.CloseCfg ();
}
