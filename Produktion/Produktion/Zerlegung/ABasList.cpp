#include "StdAfx.h"
#include "ABasList.h"

CABasList::CABasList(void)
{
	a = 0.0;
	a_bz1 = _T("");
	a_bz2 = _T("");
	a_typ = _T("");
	ag = _T("");
	wg = _T("");
	hwg = _T("");
	teil_smt = _T("");
	me_einh = _T("");
	a_bz3 = _T("");
}

CABasList::CABasList(double a, LPTSTR a_bz1, LPTSTR a_bz2, LPTSTR a_typ, LPTSTR ag,
					 LPTSTR wg, LPTSTR hwg, LPTSTR teil_smt, LPTSTR me_einh, LPTSTR a_bz3)
{
	this->a        = a;
	this->a_bz1    = a_bz1;
	this->a_bz2    = a_bz2;
	this->a_typ    = a_typ;
	this->ag       = ag;
	this->wg       = wg;
	this->hwg      = hwg;
	this->teil_smt = teil_smt;
	this->me_einh  = me_einh;
	this->a_bz3    = a_bz3;
}

CABasList::~CABasList(void)
{
}
