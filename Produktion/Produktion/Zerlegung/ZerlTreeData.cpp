#include "StdAfx.h"
#include "ZerlTreeData.h"

CZerlTreeData::CZerlTreeData(void)
{
	Item = NULL;
	mdn = 0;
	schnitt = 0;
	zerm_mat = 0;
	zerm_aus_mat = 0;
	zerm_aus_a_typ = 0;
	zerm_gew = 0.0;
	zerm_gew_ant = 0.0;
	mat_o_b = 0.0;
	dm_100 = 0.0;
	fwp = 0.0;
	a_typ = 0;
	fix = _T("");

    a_kz = _T("");
    zerm_teil = _T("0");
    hk_vollk = 0;
    hk_teilk = 0;
    delstatus = 0;
    vollkosten_abs = 0;
    vollkosten_proz = 0;
    teilkosten_abs = 0;
    teilkosten_proz = 0;
    a_z_f = 0;
    vk_pr = 0;
    kosten_zutat = 0;
    kosten_vpk = 0;

    kosten = 0.0;
    pr_zutat = 0.0;
    pr_vpk = 0.0;
    pr_kosten = 0.0;
    pr_vk_netto = 0.0;
    kosten_fix = 0.0;
    pr_vk_eh = 0.0;

	zerm_mat_bz1 = _T("");
	zerm_mat_bz2 = _T("");
	zerm_gew_ant_parent = 0;
	kosten_gh = 0.0;
	kosten_eh = 0.0;
}

CZerlTreeData::CZerlTreeData(HTREEITEM Item,
							 short mdn,
							 long schnitt,
							 long zerm_mat,
							 long zerm_aus_mat,
							 short zerm_aus_a_typ,
							 double zerm_gew,
							 double zerm_gew_ant,
							 double mat_o_b,
							double dm_100,
							double fwp,
							LPTSTR fix,

							LPTSTR          a_kz,
							LPTSTR          zerm_teil,
							double         hk_vollk,
							double         hk_teilk,
							short          delstatus,
							double         vollkosten_abs,
							double         vollkosten_proz,
							double         teilkosten_abs,
							double         teilkosten_proz,
							long           a_z_f,
							double         vk_pr,
							double         kosten_zutat,
							double         kosten_vpk,
							short a_typ,
						    double         kosten,
							double         pr_zutat,
							double         pr_vpk,
							double         pr_kosten,
							double         pr_vk_netto,
							double         kosten_fix,
							double         pr_vk_eh,

							 LPTSTR zerm_mat_bz1,
							 LPTSTR zerm_mat_bz2,
							 double		zerm_gew_ant_parent,
							 double		kosten_gh,
							 double		kosten_eh,
							 double		fwp_org,
							 double         sk_vollk,
							double         sk_teilk,
							double         fil_ek_vollk,
							double         fil_ek_teilk,
							double         fil_vk_vollk,
							double         fil_vk_teilk,
							double         gh1_vollk,
							double         gh1_teilk,
							double         gh2_vollk,
							double         gh2_teilk
					 )
{
	this->Item		 = Item;
	this->mdn		 = mdn;
	this->schnitt		 = schnitt;
	this->zerm_mat        = zerm_mat;
	this->zerm_aus_mat    = zerm_aus_mat;
	this->zerm_aus_a_typ    = zerm_aus_a_typ;
	this->zerm_gew        = zerm_gew;
	this->zerm_gew_ant    = zerm_gew_ant;
	this->mat_o_b    = mat_o_b;
	this->dm_100 = dm_100;
	this->fwp        = fwp;
	this->a_typ      = a_typ;
	this->fix		 = fix;

    this->a_kz = a_kz;
    this->zerm_teil = zerm_teil;
    this->hk_vollk = hk_vollk;
    this->hk_teilk = hk_teilk;
    this->delstatus = delstatus;
    this->vollkosten_abs = vollkosten_abs;
    this->vollkosten_proz = vollkosten_proz;
    this->teilkosten_abs = teilkosten_abs;
    this->teilkosten_proz = teilkosten_proz;
    this->a_z_f = a_z_f;
    this->vk_pr = vk_pr;
    this->kosten_zutat = kosten_zutat;
    this->kosten_vpk = kosten_vpk;
   this->kosten = kosten;
   this->pr_zutat = pr_zutat;
   this->pr_vpk = pr_vpk;
   this->pr_kosten = pr_kosten;
   this->pr_vk_netto = pr_vk_netto;
   this->kosten_fix = kosten_fix;
   this->pr_vk_eh = pr_vk_eh;


	this->zerm_mat_bz1      = zerm_mat_bz1;
	this->zerm_mat_bz2      = zerm_mat_bz2;
	this->zerm_gew_ant_parent    = zerm_gew_ant_parent;
	this->kosten_gh = kosten_gh;
	this->kosten_eh = kosten_eh;
	this->fwp_org   = fwp_org;
	this->sk_vollk   = sk_vollk;
	this->sk_teilk   = sk_teilk;
	this->fil_ek_vollk   = fil_ek_vollk;
	this->fil_ek_teilk   = fil_ek_teilk;
	this->fil_vk_vollk   = fil_vk_vollk;
	this->fil_vk_teilk   = fil_vk_teilk;
	this->gh1_vollk   = gh1_vollk;
	this->gh1_teilk   = gh1_teilk;
	this->gh2_vollk   = gh2_vollk;
	this->gh2_teilk   = gh2_teilk;
}

CZerlTreeData::~CZerlTreeData(void)
{
}

CZerlTreeGt::CZerlTreeGt(void)
{
	Item = NULL;
	zerm_mat = 0;
	zerm_aus_mat = 0;
	ebene = 0;
	fix = _T("");
}

CZerlTreeGt::CZerlTreeGt(HTREEITEM Item,
							 long zerm_mat,
							 long zerm_aus_mat,
							 short ebene,
							 LPTSTR fix
					 )
{
	this->Item		 = Item;
	this->zerm_mat        = zerm_mat;
	this->zerm_aus_mat    = zerm_aus_mat;
	this->ebene        = ebene;
	this->fix = fix;
}

CZerlTreeGt::~CZerlTreeGt(void)
{
}

CZerlTreeGt2::CZerlTreeGt2(void)
{
	Item = NULL;
	zerm_mat = 0;
	zerm_aus_mat = 0;
	ebene = 0;
	fix = _T("");
}

CZerlTreeGt2::CZerlTreeGt2(HTREEITEM Item,
							 long zerm_mat,
							 long zerm_aus_mat,
							 short ebene,
							 LPTSTR fix
					 )
{
	this->Item		 = Item;
	this->zerm_mat        = zerm_mat;
	this->zerm_aus_mat    = zerm_aus_mat;
	this->ebene        = ebene;
	this->fix = fix;
}

CZerlTreeGt2::~CZerlTreeGt2(void)
{
}

CZerlTreeMatKost::CZerlTreeMatKost(void)
{
	Item = NULL;
	zerm_mat = 0;
	ebene = 0;
}

CZerlTreeMatKost::CZerlTreeMatKost(HTREEITEM Item,
							 long zerm_mat,
							 short ebene
					 )
{
	this->Item		 = Item;
	this->zerm_mat        = zerm_mat;
	this->ebene        = ebene;
}

CZerlTreeMatKost::~CZerlTreeMatKost(void)
{
}
CZerlTreeVkArtikel::CZerlTreeVkArtikel(void)
{
	Item = NULL;
	PItem = NULL;
	CItem = NULL;
}

CZerlTreeVkArtikel::CZerlTreeVkArtikel(HTREEITEM Item,
							 HTREEITEM PItem,
							 HTREEITEM CItem
					 )
{
	this->Item		 = Item;
	this->PItem		 = PItem;
	this->CItem		 = CItem;
}

CZerlTreeVkArtikel::~CZerlTreeVkArtikel(void)
{
}
