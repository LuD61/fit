#pragma once
#include "afxwin.h"
#include "ListEdit.h"

class CSearchListCtrl :
	public CObject
{
protected :
	DECLARE_DYNAMIC(CSearchListCtrl)
public:
	enum ALIGN
	{
		LEFT = 1,
		RIGHT = 2,
	};
	CListEdit Edit;
	CButton Button;
	CSearchListCtrl(void);
	~CSearchListCtrl(void);
	void Create (RECT &rect, CWnd *pParent, UINT nIDEdit, UINT nIDButton, int Align);
	void DestroyWindow ();
};
