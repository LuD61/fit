// ChildFrm.cpp : Implementierung der Klasse CChildFrame
//
#include "stdafx.h"
#include "Zerlegung.h"

#include "MainFrm.h"
#include "ChildFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CVector CChildFrame::ZerlWnd;

// CChildFrame

IMPLEMENT_DYNCREATE(CChildFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CChildFrame, CMDIChildWnd)
	ON_WM_SIZE ()
	ON_WM_DESTROY ()
END_MESSAGE_MAP()


// CChildFrame Erstellung/Zerst�rung

CChildFrame::CChildFrame()
{
	// TODO: Hier Code f�r die Memberinitialisierung einf�gen
	InitSize = FALSE;
}

CChildFrame::~CChildFrame()
{
}


BOOL CChildFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: �ndern Sie die Fensterklasse oder die Stile hier, indem Sie CREATESTRUCT �ndern
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	return TRUE;
}


// CChildFrame Diagnose

#ifdef _DEBUG
void CChildFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CChildFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG



// CChildFrame Meldungshandler

BOOL CChildFrame::OnCreateClient(LPCREATESTRUCT lpcs,   CCreateContext* pContext)
{
	lpcs->cy = 500;
	BOOL ret = CMDIChildWnd::OnCreateClient(lpcs,   pContext);	

	CStringA ClassName = pContext->m_pNewViewClass->m_lpszClassName;
	if (ClassName == "CZerlView")
	{

		CMainFrame *MainFrm = (CMainFrame *) GetParent ()->GetParent ();
		if (MainFrm->ZerlWnd == NULL)
		{
			MainFrm->ZerlWnd = this;
		}
		else
		{
			ZerlWnd.Add (this);
		}

	}
//	MDIMaximize ();
	
	return ret;
}

void CChildFrame::OnSize(UINT nType, int cx, int cy)
{
	static BOOL InitPatView;

	CMDIChildWnd::OnSize (nType, cx, cy);
	if (!InitSize)
	{
		    CRect pRect;
			GetParent ()->GetClientRect (pRect);
			CRect rect;
			GetWindowRect (&rect);
			GetParent ()->ScreenToClient (&rect);
			rect.bottom = pRect.bottom;
			rect.right += 55;
			rect.top = 0;
			InitSize = TRUE;
			MoveWindow (&rect, TRUE);
	}
}


void CChildFrame::OnDestroy()
{

	InitSize = FALSE;

	CMainFrame *MainFrm = (CMainFrame *) GetParent ()->GetParent ();
	if (MainFrm->ZerlWnd == this)
	{
		MainFrm->ZerlWnd = NULL;
		if (ZerlWnd.GetCount () != 0)
		{
			MainFrm->ZerlWnd = (CWnd *) ZerlWnd.Get (0);
			ZerlWnd.Drop (0);
		}
		InitSize = FALSE;
		return;
	}
/*
	else if (MainFrm->MatWnd == this)
	{
		MainFrm->MatWnd = NULL;
		if (MatWnd.GetCount () != 0)
		{
			MainFrm->MatWnd = (CWnd *) MatWnd.Get (0);
			MatWnd.Drop (0);
		}
		InitSize = FALSE;
		return;
	}
	else if (MainFrm->APrWnd == this)
	{
		MainFrm->APrWnd = NULL;
		if (APrWnd.GetCount () != 0)
		{
			MainFrm->APrWnd = (CWnd *) APrWnd.Get (0);
			APrWnd.Drop (0);
		}
		InitSize = FALSE;
		return;
	}
	else if (MainFrm->StdPrWnd == this)
	{
		MainFrm->StdPrWnd = NULL;
		if (StdPrWnd.GetCount () != 0)
		{
			MainFrm->StdPrWnd = (CWnd *) StdPrWnd.Get (0);
			StdPrWnd.Drop (0);
		}
		InitSize = FALSE;
		return;
	}
	int i = -1;
	if ((i = ArtWnd.Find (this)) != -1)
	{
		ArtWnd.Drop (i);
		InitSize = FALSE;
	}
	else if ((i = MatWnd.Find (this)) != -1)
	{
		MatWnd.Drop (i);
		InitSize = FALSE;
	}
	else if ((i = APrWnd.Find (this)) != -1)
	{
		APrWnd.Drop (i);
		InitSize = FALSE;
	}
	else if ((i = StdPrWnd.Find (this)) != -1)
	{
		StdPrWnd.Drop (i);
		InitSize = FALSE;
	}
*/
}
