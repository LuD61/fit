// ZerlZerllist.cpp : Implementierungsdatei  d
//

#include "stdafx.h"
#include "Zerlegung.h"
#include "ZerlZerllist.h"


// CZerlZerllist-Dialogfeld

IMPLEMENT_DYNAMIC(CZerlZerllist, CDialog)
CZerlZerllist::CZerlZerllist(CWnd* pParent /*=NULL*/)
	: CDialog(CZerlZerllist::IDD, pParent)
{
	PageUpdate = NULL;
}

CZerlZerllist::~CZerlZerllist()
{
}

void CZerlZerllist::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CZerlZerllist, CDialog)
	ON_WM_SIZE ()
END_MESSAGE_MAP()


// CZerlZerllist-Meldungshandler
BOOL CZerlZerllist::OnInitDialog()
{
	Zerm  = &Page1->Zerm;

	m_Zerl.PageUpdate = PageUpdate;
	m_ZerlList.PageUpdate = PageUpdate;

	CDialog::OnInitDialog();
	m_ZerlList.Create (IDD_ZERLLIST, this);
	m_ZerlList.ShowWindow (SW_SHOWNORMAL);
	m_ZerlList.UpdateWindow ();
//	m_ZerlList.Basis = Basis;

	m_Zerl.Create (IDD_ZERL, this);
	m_Zerl.ShowWindow (SW_SHOWNORMAL);
	m_Zerl.UpdateWindow ();
	SplitPane.Create (this, 0, 0, &m_ZerlList, &m_Zerl, 24/*Zerm->SPLIT_HORIZONTAL*/, CSplitPane::Vertical);
	m_Zerl.SetFocus(); 

	return FALSE;
}
void CZerlZerllist::OnSize (UINT nType, int cx, int cy)
{
	if (IsWindow (SplitPane.m_hWnd))
	{
		CRect rect;
		m_ZerlList.GetWindowRect (&rect);
		ScreenToClient (&rect);
		rect.right = cx;
        m_ZerlList.MoveWindow (&rect);

		m_Zerl.GetWindowRect (&rect);
		ScreenToClient (&rect);
		rect.right = cx;
		rect.bottom = cy - 2;
        m_Zerl.MoveWindow (&rect);

		SplitPane.SetLength (cx);
	}
}

void CZerlZerllist::AddUpdateEvent (CUpdateEvent *UpdateEvent)
{
	UpdateTab.push_back (UpdateEvent);
}
void CZerlZerllist::Update (short flgsave)
{
	int di = 0;
	m_Zerl.Read();
	m_ZerlList.Read();


}
