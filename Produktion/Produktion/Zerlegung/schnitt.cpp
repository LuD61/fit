#include "stdafx.h"
#include "schnitt.h"

struct SCHNITT schnitt, schnitt_null;

void SCHNITT_CLASS::prepare (void)
{
            char *sqltext;

            sqlin ((short *)   &schnitt.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &schnitt.schnitt,  SQLLONG, 0);
    sqlout ((short *) &schnitt.mdn,SQLSHORT,0);
    sqlout ((long *) &schnitt.schnitt,SQLLONG,0);
    sqlout ((TCHAR *) schnitt.lief_txt,SQLCHAR,49);
    sqlout ((short *) &schnitt.kalk_art,SQLSHORT,0);
    sqlout ((short *) &schnitt.kalk_bas,SQLSHORT,0);
    sqlout ((TCHAR *) schnitt.schnitt_txt,SQLCHAR,81);
    sqlout ((TCHAR *) schnitt.schnitt_hin,SQLCHAR,201);
    sqlout ((long *) &schnitt.schnitt_mat,SQLLONG,0);
    sqlout ((TCHAR *) schnitt.schnitt_stk,SQLCHAR,6);
    sqlout ((double *) &schnitt.ek_pr,SQLDOUBLE,0);
    sqlout ((double *) &schnitt.ek_gew,SQLDOUBLE,0);
    sqlout ((short *) &schnitt.bearb_weg,SQLSHORT,0);
    sqlout ((DATE_STRUCT *) &schnitt.zer_dat,SQLDATE,0);
    sqlout ((DATE_STRUCT *) &schnitt.kalk_dat,SQLDATE,0);
    sqlout ((TCHAR *) schnitt.kalk_usr,SQLCHAR,9);
    sqlout ((short *) &schnitt.stat,SQLSHORT,0);
    sqlout ((long *) &schnitt.vergl_schnitt,SQLLONG,0);
    sqlout ((short *) &schnitt.delstatus,SQLSHORT,0);
    sqlout ((TCHAR *) schnitt.hdkl,SQLCHAR,5);
    sqlout ((double *) &schnitt.fix_zeit,SQLDOUBLE,0);
    sqlout ((double *) &schnitt.soll_zeit,SQLDOUBLE,0);
    sqlout ((double *) &schnitt.vk_pr,SQLDOUBLE,0);
    sqlout ((long *) &schnitt.schlachtkalk,SQLLONG,0);
    sqlout ((double *) &schnitt.schlachtart,SQLDOUBLE,0);
    sqlout ((double *) &schnitt.ek,SQLDOUBLE,0);
    sqlout ((double *) &schnitt.deckung_proz,SQLDOUBLE,0);
    sqlout ((double *) &schnitt.summe_wert,SQLDOUBLE,0);
    sqlout ((double *) &schnitt.menge_vt,SQLDOUBLE,0);
    sqlout ((double *) &schnitt.menge_vm,SQLDOUBLE,0);
    sqlout ((double *) &schnitt.menge_kk,SQLDOUBLE,0);
    sqlout ((double *) &schnitt.wert_vt,SQLDOUBLE,0);
    sqlout ((double *) &schnitt.wert_vm,SQLDOUBLE,0);
    sqlout ((double *) &schnitt.wert_kk,SQLDOUBLE,0);
    sqlout ((short *) &schnitt.koart_grund,SQLSHORT,0);
    sqlout ((short *) &schnitt.koart_grob,SQLSHORT,0);
    sqlout ((short *) &schnitt.koart_vm,SQLSHORT,0);
    sqlout ((short *) &schnitt.koart_vt,SQLSHORT,0);
    sqlout ((short *) &schnitt.koart_za,SQLSHORT,0);
    sqlout ((short *) &schnitt.koart_min,SQLSHORT,0);
            cursor = sqlcursor (_T("select schnitt.mdn,  ")
_T("schnitt.schnitt,  schnitt.lief_txt,  schnitt.kalk_art,  ")
_T("schnitt.kalk_bas,  schnitt.schnitt_txt,  schnitt.schnitt_hin,  ")
_T("schnitt.schnitt_mat,  schnitt.schnitt_stk,  schnitt.ek_pr,  ")
_T("schnitt.ek_gew,  schnitt.bearb_weg,  schnitt.zer_dat,  ")
_T("schnitt.kalk_dat,  schnitt.kalk_usr,  schnitt.stat,  ")
_T("schnitt.vergl_schnitt,  schnitt.delstatus,  schnitt.hdkl,  ")
_T("schnitt.fix_zeit,  schnitt.soll_zeit,  schnitt.vk_pr,  ")
_T("schnitt.schlachtkalk,  schnitt.schlachtart,  schnitt.ek,  ")
_T("schnitt.deckung_proz,  schnitt.summe_wert,  schnitt.menge_vt,  ")
_T("schnitt.menge_vm,  schnitt.menge_kk,  schnitt.wert_vt,  ")
_T("schnitt.wert_vm,  schnitt.wert_kk,  schnitt.koart_grund,  ")
_T("schnitt.koart_grob,  schnitt.koart_vm,  schnitt.koart_vt,  ")
_T("schnitt.koart_za,  schnitt.koart_min from schnitt ")

#line 13 "schnitt.rpp"
                                _T("where mdn = ? ")
			        _T("and schnitt = ?"));
    sqlin ((short *) &schnitt.mdn,SQLSHORT,0);
    sqlin ((long *) &schnitt.schnitt,SQLLONG,0);
    sqlin ((TCHAR *) schnitt.lief_txt,SQLCHAR,49);
    sqlin ((short *) &schnitt.kalk_art,SQLSHORT,0);
    sqlin ((short *) &schnitt.kalk_bas,SQLSHORT,0);
    sqlin ((TCHAR *) schnitt.schnitt_txt,SQLCHAR,81);
    sqlin ((TCHAR *) schnitt.schnitt_hin,SQLCHAR,201);
    sqlin ((long *) &schnitt.schnitt_mat,SQLLONG,0);
    sqlin ((TCHAR *) schnitt.schnitt_stk,SQLCHAR,6);
    sqlin ((double *) &schnitt.ek_pr,SQLDOUBLE,0);
    sqlin ((double *) &schnitt.ek_gew,SQLDOUBLE,0);
    sqlin ((short *) &schnitt.bearb_weg,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &schnitt.zer_dat,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &schnitt.kalk_dat,SQLDATE,0);
    sqlin ((TCHAR *) schnitt.kalk_usr,SQLCHAR,9);
    sqlin ((short *) &schnitt.stat,SQLSHORT,0);
    sqlin ((long *) &schnitt.vergl_schnitt,SQLLONG,0);
    sqlin ((short *) &schnitt.delstatus,SQLSHORT,0);
    sqlin ((TCHAR *) schnitt.hdkl,SQLCHAR,5);
    sqlin ((double *) &schnitt.fix_zeit,SQLDOUBLE,0);
    sqlin ((double *) &schnitt.soll_zeit,SQLDOUBLE,0);
    sqlin ((double *) &schnitt.vk_pr,SQLDOUBLE,0);
    sqlin ((long *) &schnitt.schlachtkalk,SQLLONG,0);
    sqlin ((double *) &schnitt.schlachtart,SQLDOUBLE,0);
    sqlin ((double *) &schnitt.ek,SQLDOUBLE,0);
    sqlin ((double *) &schnitt.deckung_proz,SQLDOUBLE,0);
    sqlin ((double *) &schnitt.summe_wert,SQLDOUBLE,0);
    sqlin ((double *) &schnitt.menge_vt,SQLDOUBLE,0);
    sqlin ((double *) &schnitt.menge_vm,SQLDOUBLE,0);
    sqlin ((double *) &schnitt.menge_kk,SQLDOUBLE,0);
    sqlin ((double *) &schnitt.wert_vt,SQLDOUBLE,0);
    sqlin ((double *) &schnitt.wert_vm,SQLDOUBLE,0);
    sqlin ((double *) &schnitt.wert_kk,SQLDOUBLE,0);
    sqlin ((short *) &schnitt.koart_grund,SQLSHORT,0);
    sqlin ((short *) &schnitt.koart_grob,SQLSHORT,0);
    sqlin ((short *) &schnitt.koart_vm,SQLSHORT,0);
    sqlin ((short *) &schnitt.koart_vt,SQLSHORT,0);
    sqlin ((short *) &schnitt.koart_za,SQLSHORT,0);
    sqlin ((short *) &schnitt.koart_min,SQLSHORT,0);
            sqltext = _T("update schnitt set schnitt.mdn = ?,  ")
_T("schnitt.schnitt = ?,  schnitt.lief_txt = ?,  schnitt.kalk_art = ?,  ")
_T("schnitt.kalk_bas = ?,  schnitt.schnitt_txt = ?,  ")
_T("schnitt.schnitt_hin = ?,  schnitt.schnitt_mat = ?,  ")
_T("schnitt.schnitt_stk = ?,  schnitt.ek_pr = ?,  schnitt.ek_gew = ?,  ")
_T("schnitt.bearb_weg = ?,  schnitt.zer_dat = ?,  schnitt.kalk_dat = ?,  ")
_T("schnitt.kalk_usr = ?,  schnitt.stat = ?,  ")
_T("schnitt.vergl_schnitt = ?,  schnitt.delstatus = ?,  ")
_T("schnitt.hdkl = ?,  schnitt.fix_zeit = ?,  schnitt.soll_zeit = ?,  ")
_T("schnitt.vk_pr = ?,  schnitt.schlachtkalk = ?,  ")
_T("schnitt.schlachtart = ?,  schnitt.ek = ?,  ")
_T("schnitt.deckung_proz = ?,  schnitt.summe_wert = ?,  ")
_T("schnitt.menge_vt = ?,  schnitt.menge_vm = ?,  schnitt.menge_kk = ?,  ")
_T("schnitt.wert_vt = ?,  schnitt.wert_vm = ?,  schnitt.wert_kk = ?,  ")
_T("schnitt.koart_grund = ?,  schnitt.koart_grob = ?,  ")
_T("schnitt.koart_vm = ?,  schnitt.koart_vt = ?,  schnitt.koart_za = ?,  ")
_T("schnitt.koart_min = ? ")

#line 16 "schnitt.rpp"
                                _T("where mdn = ? ")
			        _T("and schnitt = ?");
            sqlin ((short *)   &schnitt.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &schnitt.schnitt,  SQLLONG, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &schnitt.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &schnitt.schnitt,  SQLLONG, 0);
            test_upd_cursor = sqlcursor (_T("select schnitt from schnitt ")
                                _T("where mdn = ? ")
			        _T("and schnitt = ?"));
            sqlin ((short *)   &schnitt.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &schnitt.schnitt,  SQLLONG, 0);
            del_cursor = sqlcursor (_T("delete from schnitt ")
                                _T("where mdn = ? ")
			        _T("and schnitt = ?"));
    sqlin ((short *) &schnitt.mdn,SQLSHORT,0);
    sqlin ((long *) &schnitt.schnitt,SQLLONG,0);
    sqlin ((TCHAR *) schnitt.lief_txt,SQLCHAR,49);
    sqlin ((short *) &schnitt.kalk_art,SQLSHORT,0);
    sqlin ((short *) &schnitt.kalk_bas,SQLSHORT,0);
    sqlin ((TCHAR *) schnitt.schnitt_txt,SQLCHAR,81);
    sqlin ((TCHAR *) schnitt.schnitt_hin,SQLCHAR,201);
    sqlin ((long *) &schnitt.schnitt_mat,SQLLONG,0);
    sqlin ((TCHAR *) schnitt.schnitt_stk,SQLCHAR,6);
    sqlin ((double *) &schnitt.ek_pr,SQLDOUBLE,0);
    sqlin ((double *) &schnitt.ek_gew,SQLDOUBLE,0);
    sqlin ((short *) &schnitt.bearb_weg,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &schnitt.zer_dat,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &schnitt.kalk_dat,SQLDATE,0);
    sqlin ((TCHAR *) schnitt.kalk_usr,SQLCHAR,9);
    sqlin ((short *) &schnitt.stat,SQLSHORT,0);
    sqlin ((long *) &schnitt.vergl_schnitt,SQLLONG,0);
    sqlin ((short *) &schnitt.delstatus,SQLSHORT,0);
    sqlin ((TCHAR *) schnitt.hdkl,SQLCHAR,5);
    sqlin ((double *) &schnitt.fix_zeit,SQLDOUBLE,0);
    sqlin ((double *) &schnitt.soll_zeit,SQLDOUBLE,0);
    sqlin ((double *) &schnitt.vk_pr,SQLDOUBLE,0);
    sqlin ((long *) &schnitt.schlachtkalk,SQLLONG,0);
    sqlin ((double *) &schnitt.schlachtart,SQLDOUBLE,0);
    sqlin ((double *) &schnitt.ek,SQLDOUBLE,0);
    sqlin ((double *) &schnitt.deckung_proz,SQLDOUBLE,0);
    sqlin ((double *) &schnitt.summe_wert,SQLDOUBLE,0);
    sqlin ((double *) &schnitt.menge_vt,SQLDOUBLE,0);
    sqlin ((double *) &schnitt.menge_vm,SQLDOUBLE,0);
    sqlin ((double *) &schnitt.menge_kk,SQLDOUBLE,0);
    sqlin ((double *) &schnitt.wert_vt,SQLDOUBLE,0);
    sqlin ((double *) &schnitt.wert_vm,SQLDOUBLE,0);
    sqlin ((double *) &schnitt.wert_kk,SQLDOUBLE,0);
    sqlin ((short *) &schnitt.koart_grund,SQLSHORT,0);
    sqlin ((short *) &schnitt.koart_grob,SQLSHORT,0);
    sqlin ((short *) &schnitt.koart_vm,SQLSHORT,0);
    sqlin ((short *) &schnitt.koart_vt,SQLSHORT,0);
    sqlin ((short *) &schnitt.koart_za,SQLSHORT,0);
    sqlin ((short *) &schnitt.koart_min,SQLSHORT,0);
            ins_cursor = sqlcursor (_T("insert into schnitt (")
_T("mdn,  schnitt,  lief_txt,  kalk_art,  kalk_bas,  schnitt_txt,  schnitt_hin,  ")
_T("schnitt_mat,  schnitt_stk,  ek_pr,  ek_gew,  bearb_weg,  zer_dat,  kalk_dat,  ")
_T("kalk_usr,  stat,  vergl_schnitt,  delstatus,  hdkl,  fix_zeit,  soll_zeit,  vk_pr,  ")
_T("schlachtkalk,  schlachtart,  ek,  deckung_proz,  summe_wert,  menge_vt,  ")
_T("menge_vm,  menge_kk,  wert_vt,  wert_vm,  wert_kk,  koart_grund,  koart_grob,  ")
_T("koart_vm,  koart_vt,  koart_za,  koart_min) ")

#line 33 "schnitt.rpp"
                                    _T("values ")
                                    _T("(?,?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")); 

#line 35 "schnitt.rpp"
}
