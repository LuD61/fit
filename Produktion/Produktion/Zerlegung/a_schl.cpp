#include "stdafx.h"
#include "a_schl.h"

struct A_SCHL a_schl, a_schl_null;

void A_SCHL_CLASS::prepare (void)
{
            char *sqltext;

            sqlin ((double *)   &a_schl.a,  SQLDOUBLE, 0);
    sqlout ((short *) &a_schl.mdn,SQLSHORT,0);
    sqlout ((short *) &a_schl.fil,SQLSHORT,0);
    sqlout ((double *) &a_schl.a,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_schl.a_k_bez,SQLCHAR,13);
    sqlout ((TCHAR *) a_schl.mo_kz,SQLCHAR,11);
    sqlout ((TCHAR *) a_schl.gatt_kz,SQLCHAR,2);
    sqlout ((double *) &a_schl.grund_pr,SQLDOUBLE,0);
    sqlout ((long *) &a_schl.kost_st,SQLLONG,0);
    sqlout ((TCHAR *) a_schl.hdkl,SQLCHAR,4);
    sqlout ((TCHAR *) a_schl.tier_kz,SQLCHAR,3);
    sqlout ((short *) &a_schl.ag,SQLSHORT,0);
    sqlout ((double *) &a_schl.grossvieheinh,SQLDOUBLE,0);
            cursor = sqlcursor (_T("select a_schl.mdn,  ")
_T("a_schl.fil,  a_schl.a,  a_schl.a_k_bez,  a_schl.mo_kz,  a_schl.gatt_kz,  ")
_T("a_schl.grund_pr,  a_schl.kost_st,  a_schl.hdkl,  a_schl.tier_kz,  ")
_T("a_schl.ag,  a_schl.grossvieheinh from a_schl ")

#line 12 "a_schl.rpp"
			        _T("where a = ?"));
    sqlin ((short *) &a_schl.mdn,SQLSHORT,0);
    sqlin ((short *) &a_schl.fil,SQLSHORT,0);
    sqlin ((double *) &a_schl.a,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_schl.a_k_bez,SQLCHAR,13);
    sqlin ((TCHAR *) a_schl.mo_kz,SQLCHAR,11);
    sqlin ((TCHAR *) a_schl.gatt_kz,SQLCHAR,2);
    sqlin ((double *) &a_schl.grund_pr,SQLDOUBLE,0);
    sqlin ((long *) &a_schl.kost_st,SQLLONG,0);
    sqlin ((TCHAR *) a_schl.hdkl,SQLCHAR,4);
    sqlin ((TCHAR *) a_schl.tier_kz,SQLCHAR,3);
    sqlin ((short *) &a_schl.ag,SQLSHORT,0);
    sqlin ((double *) &a_schl.grossvieheinh,SQLDOUBLE,0);
            sqltext = _T("update a_schl set a_schl.mdn = ?,  ")
_T("a_schl.fil = ?,  a_schl.a = ?,  a_schl.a_k_bez = ?,  a_schl.mo_kz = ?,  ")
_T("a_schl.gatt_kz = ?,  a_schl.grund_pr = ?,  a_schl.kost_st = ?,  ")
_T("a_schl.hdkl = ?,  a_schl.tier_kz = ?,  a_schl.ag = ?,  ")
_T("a_schl.grossvieheinh = ? ")

#line 14 "a_schl.rpp"
                                _T("where a = ? ");
            sqlin ((double *)   &a_schl.a,  SQLDOUBLE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((double *)   &a_schl.a,  SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor (_T("select a from a_schl ")
			        _T("where a = ?"));
            sqlin ((double *)   &a_schl.a,  SQLDOUBLE, 0);
            del_cursor = sqlcursor (_T("delete from a_schl ")
                                _T("where a = ? "));
    sqlin ((short *) &a_schl.mdn,SQLSHORT,0);
    sqlin ((short *) &a_schl.fil,SQLSHORT,0);
    sqlin ((double *) &a_schl.a,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_schl.a_k_bez,SQLCHAR,13);
    sqlin ((TCHAR *) a_schl.mo_kz,SQLCHAR,11);
    sqlin ((TCHAR *) a_schl.gatt_kz,SQLCHAR,2);
    sqlin ((double *) &a_schl.grund_pr,SQLDOUBLE,0);
    sqlin ((long *) &a_schl.kost_st,SQLLONG,0);
    sqlin ((TCHAR *) a_schl.hdkl,SQLCHAR,4);
    sqlin ((TCHAR *) a_schl.tier_kz,SQLCHAR,3);
    sqlin ((short *) &a_schl.ag,SQLSHORT,0);
    sqlin ((double *) &a_schl.grossvieheinh,SQLDOUBLE,0);
            ins_cursor = sqlcursor (_T("insert into a_schl (")
_T("mdn,  fil,  a,  a_k_bez,  mo_kz,  gatt_kz,  grund_pr,  kost_st,  hdkl,  tier_kz,  ag,  ")
_T("grossvieheinh) ")

#line 25 "a_schl.rpp"
                                    _T("values ")
                                    _T("(?,?,?,?,?,?,?,?,?,")
_T("?,?,?)")); 

#line 27 "a_schl.rpp"
}
