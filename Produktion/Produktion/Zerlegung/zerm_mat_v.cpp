#include "stdafx.h"
#include "zerm_mat_v.h"

struct ZERM_MAT_V zerm_mat_v, zerm_mat_v_null;

void ZERM_MAT_V_CLASS::prepare (void)
{
            char *sqltext;

            sqlin ((short *)   &zerm_mat_v.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &zerm_mat_v.schnitt,  SQLLONG, 0);
            sqlin ((long *)   &zerm_mat_v.zerm_mat,  SQLLONG, 0);
	    sqlin ((long *) &zerm_mat_v.kost_mat,SQLLONG,0);
    	    sqlin ((short *) &zerm_mat_v.a_typ,SQLSHORT,0);
    sqlout ((short *) &zerm_mat_v.mdn,SQLSHORT,0);
    sqlout ((long *) &zerm_mat_v.schnitt,SQLLONG,0);
    sqlout ((long *) &zerm_mat_v.zerm_mat,SQLLONG,0);
    sqlout ((double *) &zerm_mat_v.fwp,SQLDOUBLE,0);
    sqlout ((double *) &zerm_mat_v.zerm_gew_ant,SQLDOUBLE,0);
    sqlout ((double *) &zerm_mat_v.mat_o_b,SQLDOUBLE,0);
    sqlout ((double *) &zerm_mat_v.hk_vollk,SQLDOUBLE,0);
    sqlout ((double *) &zerm_mat_v.hk_teilk,SQLDOUBLE,0);
    sqlout ((TCHAR *) zerm_mat_v.kost_mat_bz,SQLCHAR,25);
    sqlout ((double *) &zerm_mat_v.dm_100,SQLDOUBLE,0);
    sqlout ((double *) &zerm_mat_v.kosten_zutat,SQLDOUBLE,0);
    sqlout ((double *) &zerm_mat_v.kosten_vpk,SQLDOUBLE,0);
    sqlout ((double *) &zerm_mat_v.teilkosten_abs,SQLDOUBLE,0);
    sqlout ((double *) &zerm_mat_v.vk_pr,SQLDOUBLE,0);
    sqlout ((double *) &zerm_mat_v.zerm_gew,SQLDOUBLE,0);
    sqlout ((double *) &zerm_mat_v.pr_vk_eh,SQLDOUBLE,0);
            cursor = sqlcursor (_T("select  ")
_T("zerm_mat_v.mdn,  zerm_mat_v.schnitt,  zerm_mat_v.zerm_mat, ")
_T("zerm_mat_v.fwp,  sum(zerm_mat_v.zerm_gew_ant),  zerm_mat_v.mat_o_b,  ")
_T("zerm_mat_v.hk_vollk,  zerm_mat_v.hk_teilk, zerm_mat_v.kost_mat_bz, sum(zerm_mat_v.dm_100),  ")
_T("sum(zerm_mat_v.kosten_zutat),  sum(zerm_mat_v.kosten_vpk), ")
_T("sum(zerm_mat_v.teilkosten_abs),  sum(zerm_mat_v.vk_pr),sum(zerm_mat_v.zerm_gew), ")
_T("sum(zerm_mat_v.pr_vk_eh) from zerm_mat_v ")

                                _T("where zerm_mat_v.mdn = ? ")
			        _T("and zerm_mat_v.schnitt = ? ")
			        _T("and zerm_mat_v.zerm_mat = ? ")
			        _T("and zerm_mat_v.kost_mat = ? ")
			        _T("and zerm_mat_v.a_typ = ? ")
                                _T("group by 1,2,3,4,6,7,8,9 ")
			        );
    sqlin ((short *) &zerm_mat_v.mdn,SQLSHORT,0);
    sqlin ((long *) &zerm_mat_v.schnitt,SQLLONG,0);
    sqlin ((long *) &zerm_mat_v.zerm_aus_mat,SQLLONG,0);
    sqlin ((long *) &zerm_mat_v.zerm_mat,SQLLONG,0);
    sqlin ((double *) &zerm_mat_v.fwp,SQLDOUBLE,0);
    sqlin ((double *) &zerm_mat_v.zerm_gew,SQLDOUBLE,0);
    sqlin ((double *) &zerm_mat_v.zerm_gew_ant,SQLDOUBLE,0);
    sqlin ((double *) &zerm_mat_v.mat_o_b,SQLDOUBLE,0);
    sqlin ((double *) &zerm_mat_v.hk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &zerm_mat_v.hk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &zerm_mat_v.dm_100,SQLDOUBLE,0);
    sqlin ((double *) &zerm_mat_v.kosten_zutat,SQLDOUBLE,0);
    sqlin ((double *) &zerm_mat_v.kosten_vpk,SQLDOUBLE,0);
    sqlin ((double *) &zerm_mat_v.teilkosten_abs,SQLDOUBLE,0);
    sqlin ((double *) &zerm_mat_v.vk_pr,SQLDOUBLE,0);
    sqlin ((long *) &zerm_mat_v.kost_mat,SQLLONG,0);
    sqlin ((TCHAR *) zerm_mat_v.kost_mat_bz,SQLCHAR,25);
    sqlin ((short *) &zerm_mat_v.a_typ,SQLSHORT,0);
    sqlin ((double *) &zerm_mat_v.pr_vk_eh,SQLDOUBLE,0);
    sqlin ((double *) &zerm_mat_v.sk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &zerm_mat_v.sk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &zerm_mat_v.fil_ek_vollk,SQLDOUBLE,0);
    sqlin ((double *) &zerm_mat_v.fil_ek_teilk,SQLDOUBLE,0);
    sqlin ((double *) &zerm_mat_v.fil_vk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &zerm_mat_v.fil_vk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &zerm_mat_v.gh1_vollk,SQLDOUBLE,0);
    sqlin ((double *) &zerm_mat_v.gh1_teilk,SQLDOUBLE,0);
    sqlin ((double *) &zerm_mat_v.gh2_vollk,SQLDOUBLE,0);
    sqlin ((double *) &zerm_mat_v.gh2_teilk,SQLDOUBLE,0);
            sqltext = _T("update zerm_mat_v set ")
_T("zerm_mat_v.mdn = ?,  zerm_mat_v.schnitt = ?,  ")
_T("zerm_mat_v.zerm_aus_mat = ?,  zerm_mat_v.zerm_mat = ?,  ")
_T("zerm_mat_v.fwp = ?,  zerm_mat_v.zerm_gew = ?,  ")
_T("zerm_mat_v.zerm_gew_ant = ?,  zerm_mat_v.mat_o_b = ?,  ")
_T("zerm_mat_v.hk_vollk = ?,  zerm_mat_v.hk_teilk = ?,  ")
_T("zerm_mat_v.dm_100 = ?,  zerm_mat_v.kosten_zutat = ?,  ")
_T("zerm_mat_v.kosten_vpk = ?,  zerm_mat_v.teilkosten_abs = ?,  ")
_T("zerm_mat_v.vk_pr = ?,  zerm_mat_v.kost_mat = ?,  ")
_T("zerm_mat_v.kost_mat_bz = ?,  zerm_mat_v.a_typ = ?,  ")
_T("zerm_mat_v.pr_vk_eh = ?,  zerm_mat_v.sk_vollk = ?,  ")
_T("zerm_mat_v.sk_teilk = ?,  zerm_mat_v.fil_ek_vollk = ?,  ")
_T("zerm_mat_v.fil_ek_teilk = ?,  zerm_mat_v.fil_vk_vollk = ?,  ")
_T("zerm_mat_v.fil_vk_teilk = ?,  zerm_mat_v.gh1_vollk = ?,  ")
_T("zerm_mat_v.gh1_teilk = ?,  zerm_mat_v.gh2_vollk = ?,  ")
_T("zerm_mat_v.gh2_teilk = ? ")

#line 47 "zerm_mat_v.rpp"
                                _T("where mdn = ? ")
			        _T("and schnitt = ? ")
			        _T("and zerm_mat = ? ")
			        _T("and zerm_aus_mat = ?")
			        _T("and zerm_mat_v.a_typ = ? ");
			        
            sqlin ((short *)   &zerm_mat_v.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &zerm_mat_v.schnitt,  SQLLONG, 0);
            sqlin ((long *)   &zerm_mat_v.zerm_mat,  SQLLONG, 0);
            sqlin ((long *)   &zerm_mat_v.zerm_aus_mat,  SQLLONG, 0);
    	    sqlin ((short *) &zerm_mat_v.a_typ,SQLSHORT,0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &zerm_mat_v.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &zerm_mat_v.schnitt,  SQLLONG, 0);
            sqlin ((long *)   &zerm_mat_v.zerm_mat,  SQLLONG, 0);
            sqlin ((long *)   &zerm_mat_v.zerm_aus_mat,  SQLLONG, 0);
    	    sqlin ((short *) &zerm_mat_v.a_typ,SQLSHORT,0);
            test_upd_cursor = sqlcursor (_T("select schnitt from zerm_mat_v ")
                                _T("where mdn = ? ")
			        _T("and schnitt = ? ")
			        _T("and zerm_mat = ? ")
			        _T("and zerm_aus_mat = ?")
			        _T("and zerm_mat_v.a_typ = ? ")
			        );
			        
            sqlin ((short *)   &zerm_mat_v.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &zerm_mat_v.schnitt,  SQLLONG, 0);
            sqlin ((long *)   &zerm_mat_v.zerm_mat,  SQLLONG, 0);
            sqlin ((long *)   &zerm_mat_v.zerm_aus_mat,  SQLLONG, 0);
    	    sqlin ((short *) &zerm_mat_v.a_typ,SQLSHORT,0);
            del_cursor = sqlcursor (_T("delete from zerm_mat_v ")
                                _T("where mdn = ? ")
			        _T("and schnitt = ?")
			        _T("and zerm_mat = ? ")
			        _T("and zerm_aus_mat = ?")
			        _T("and zerm_mat_v.a_typ = ? ")
			        );
			        
    sqlin ((short *) &zerm_mat_v.mdn,SQLSHORT,0);
    sqlin ((long *) &zerm_mat_v.schnitt,SQLLONG,0);
    sqlin ((long *) &zerm_mat_v.zerm_aus_mat,SQLLONG,0);
    sqlin ((long *) &zerm_mat_v.zerm_mat,SQLLONG,0);
    sqlin ((double *) &zerm_mat_v.fwp,SQLDOUBLE,0);
    sqlin ((double *) &zerm_mat_v.zerm_gew,SQLDOUBLE,0);
    sqlin ((double *) &zerm_mat_v.zerm_gew_ant,SQLDOUBLE,0);
    sqlin ((double *) &zerm_mat_v.mat_o_b,SQLDOUBLE,0);
    sqlin ((double *) &zerm_mat_v.hk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &zerm_mat_v.hk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &zerm_mat_v.dm_100,SQLDOUBLE,0);
    sqlin ((double *) &zerm_mat_v.kosten_zutat,SQLDOUBLE,0);
    sqlin ((double *) &zerm_mat_v.kosten_vpk,SQLDOUBLE,0);
    sqlin ((double *) &zerm_mat_v.teilkosten_abs,SQLDOUBLE,0);
    sqlin ((double *) &zerm_mat_v.vk_pr,SQLDOUBLE,0);
    sqlin ((long *) &zerm_mat_v.kost_mat,SQLLONG,0);
    sqlin ((TCHAR *) zerm_mat_v.kost_mat_bz,SQLCHAR,25);
    sqlin ((short *) &zerm_mat_v.a_typ,SQLSHORT,0);
    sqlin ((double *) &zerm_mat_v.pr_vk_eh,SQLDOUBLE,0);
    sqlin ((double *) &zerm_mat_v.sk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &zerm_mat_v.sk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &zerm_mat_v.fil_ek_vollk,SQLDOUBLE,0);
    sqlin ((double *) &zerm_mat_v.fil_ek_teilk,SQLDOUBLE,0);
    sqlin ((double *) &zerm_mat_v.fil_vk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &zerm_mat_v.fil_vk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &zerm_mat_v.gh1_vollk,SQLDOUBLE,0);
    sqlin ((double *) &zerm_mat_v.gh1_teilk,SQLDOUBLE,0);
    sqlin ((double *) &zerm_mat_v.gh2_vollk,SQLDOUBLE,0);
    sqlin ((double *) &zerm_mat_v.gh2_teilk,SQLDOUBLE,0);
            ins_cursor = sqlcursor (_T("insert into zerm_mat_v (")
_T("mdn,  schnitt,  zerm_aus_mat,  zerm_mat,  fwp,  zerm_gew,  zerm_gew_ant,  mat_o_b,  ")
_T("hk_vollk,  hk_teilk,  dm_100,  kosten_zutat,  kosten_vpk,  teilkosten_abs,  ")
_T("vk_pr,  kost_mat,  kost_mat_bz,  a_typ,  pr_vk_eh,  sk_vollk,  sk_teilk,  ")
_T("fil_ek_vollk,  fil_ek_teilk,  fil_vk_vollk,  fil_vk_teilk,  gh1_vollk,  ")
_T("gh1_teilk,  gh2_vollk,  gh2_teilk) ")

#line 87 "zerm_mat_v.rpp"
                                    _T("values ")
                                    _T("(?,?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")); 

#line 89 "zerm_mat_v.rpp"
}
