// ZerlListDlg.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "Zerlegung.h"
#include "ZerlListDlg.h"
#include "StrFuncs.h"


// CZerlListDlg-Dialogfeld

IMPLEMENT_DYNAMIC(CZerlListDlg, CDialog)
CZerlListDlg::CZerlListDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CZerlListDlg::IDD, pParent)
{
	A_bas = NULL;
	Zerm = NULL;
	Zerm_mat_v = NULL;
	Schnitt = NULL;
	Page1 = NULL;
	PageUpdate = NULL;
	DlgBkColor = GetSysColor (COLOR_3DFACE);
	DlgBrush = NULL;
	dcs_a_par = -1;
	waa_a_par = -1;
}

CZerlListDlg::~CZerlListDlg()
{
}

void CZerlListDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_ZERLLIST, m_ZerlList);
	DDX_Control(pDX, IDC_ZERLLIST_TEXT, m_ZerlListText);
}


BEGIN_MESSAGE_MAP(CZerlListDlg, CDialog)
	ON_WM_SIZE ()
	ON_WM_CTLCOLOR ()
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_ZERLLIST, OnLvnItemchangedEanList)
END_MESSAGE_MAP()


// CZerlListDlg-Meldungshandler

BOOL CZerlListDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	A_bas   = &Page1->A_bas;
	A_mat   = &Page1->A_mat;
	Zerm   = &Page1->Zerm;
	Zerm_mat_v   = &Page1->Zerm_mat_v;
	Schnitt   = &Page1->Schnitt;
	Sys_par = &Page1->Sys_par;
	m_ZerlList.A_bas = A_bas;
	m_ZerlList.A_mat = A_mat;
	m_ZerlList.Zerm = Zerm;
	m_ZerlList.Zerm_mat_v = Zerm_mat_v;
	m_ZerlList.Schnitt = Schnitt;

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
	}
	m_ZerlList.Init ();
    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (0, 0, 0, 0);
    CtrlGrid.SetCellHeight (15);
//    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetFontCellHeight (this, &Font);
    CtrlGrid.SetGridSpace (5, 0);  //Spaltenabstand und Zeilenabstand

	CCtrlInfo *c_LEanText = new CCtrlInfo (&m_ZerlListText, 1, 1, 1, 1);
	CtrlGrid.Add (c_LEanText);

	CCtrlInfo *c_EanList = new CCtrlInfo (&m_ZerlList, 0, 2, DOCKRIGHT, DOCKBOTTOM);
	CtrlGrid.Add (c_EanList);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);

	CtrlGrid.Display ();

	return FALSE;
}

HBRUSH CZerlListDlg::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}

	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return CDialog::OnCtlColor (pDC, pWnd,nCtlColor);
}

void CZerlListDlg::OnSize (UINT nType, int cx, int cy)
{
		CRect rect (0, 0, cx, cy);
		CtrlGrid.pcx = 0;
		CtrlGrid.pcy = 0;
		CtrlGrid.DlgSize = &rect;
		CtrlGrid.Move (0, 0);
		CtrlGrid.DlgSize = NULL;
}

BOOL CZerlListDlg::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{
		case WM_KEYDOWN :
			if (pMsg->wParam == VK_F3)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
//					Page1->NextRec ();
				}
				else
				{
//					Page1->LastRec ();
				}
				return TRUE;

			}

			else if (pMsg->wParam == VK_F2)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
//					Page1->PriorRec ();
				}
				else
				{
//					Page1->FirstRec ();
				}
				return TRUE;

			}

			if (pMsg->wParam == VK_F5)
			{
				Zerm->ZerlTreeErlauben = Zerm->TYP_MATERIAL; 
				PageUpdate->Back ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F7)
			{
				Delete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Zerm->ZerlTreeErlauben = Zerm->TYP_MATERIAL; 
				PageUpdate->Write (); 
				return TRUE;
			}
/*
			else if (pMsg->wParam == VK_F12)
			{
				Update->Write ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
				if (Page1->Choice != NULL)
				{
					if (Page1->Choice->IsWindowVisible ())
					{
						Page1->Choice->ShowWindow (SW_HIDE);
					}
					else
					{
						Page1->Choice->ShowWindow (SW_SHOWNORMAL);
					}
				}
			}
			else if (pMsg->wParam == VK_F9)
			{
				return TRUE;
			}
*/
	}
	return FALSE;
}


void CZerlListDlg::SetBasis (CPage1 *Page1)
{
	this->Page1 = Page1;
}

void CZerlListDlg::Update (short flgSave)
{
	if (flgSave < 0) return;
	if (flgSave > 100) return;
	memcpy (&Zerm_mat_v->zerm_mat_v, &zerm_mat_v_null, sizeof (ZERM_MAT_V)); 
	m_ZerlList.Read ();
}

void CZerlListDlg::Read ()
{
	memcpy (&Zerm_mat_v->zerm_mat_v, &zerm_mat_v_null, sizeof (ZERM_MAT_V)); 
	m_ZerlList.Read ();
	m_ZerlList.SetItemState (0, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
	SetSelectedItem (0);

	//Das folgende hat nur den Sinn, den Tree aus das Ausgangsmaterial zu selektieren
	Zerm->zerm.mdn = Schnitt->schnitt.mdn;
	Zerm->zerm.schnitt = Schnitt->schnitt.schnitt;
	/* TESTTEST
	Zerm->zerm.zerm_aus_mat = Schnitt->schnitt.schnitt_mat;
	Zerm->zerm.zerm_mat = Schnitt->schnitt.schnitt_mat;
	Zerm->zerm.a_typ = Zerm->TYP_MATERIAL;
	Zerm->dbreadfirst(); 

	for (std::vector<CUpdateEvent *>::iterator e = UpdateTab.begin ();
			                                       e != UpdateTab.end (); ++ e)
	{
			CUpdateEvent *event = *e;
			event->Update (0);
	}
	*/
}

void CZerlListDlg::OnLvnItemchangedEanList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	*pResult = 0;
	int idx = m_ZerlList.GetNextItem (-1, LVNI_SELECTED);
	if (idx != -1)
	{
		SetSelectedItem (idx);
		/**
		for (std::vector<CUpdateEvent *>::iterator e = UpdateTab.begin ();
			                                       e != UpdateTab.end (); ++ e)
		{
			CUpdateEvent *event = *e;
			event->Update (0);
		}
		***/
	}
}

void CZerlListDlg::SetSelectedItem (int idx)
{
	if (idx != -1)
	{
		CString Ean = m_ZerlList.GetItemText (idx, 1);
		/*
		CString EanBz = m_ZerlList.GetItemText (idx, 2);
		CString HEan = m_ZerlList.GetItemText (idx, 3);
		CString AKrz = m_ZerlList.GetItemText (idx, 4);

		A_ean->a_ean.ean = CStrFuncs::StrToDouble (Ean);
		_tcscpy (A_ean->a_ean.ean_bz, EanBz.GetBuffer ()); 
		if (HEan == _T("X"))
		{
			_tcscpy (A_ean->a_ean.h_ean_kz, _T("1"));
		}
		else
		{
			_tcscpy (A_ean->a_ean.h_ean_kz, _T("0"));
		}
		*/
	}
}

BOOL CZerlListDlg::HasFocus ()
{
	CWnd *Control = GetFocus ();
	if (Control != &m_ZerlList) return FALSE;
	return TRUE;
}

void CZerlListDlg::Delete ()
{
	if (!HasFocus ()) return;
	DeleteRow ();
}

void CZerlListDlg::DeleteRow ()
{
	int idx = m_ZerlList.GetNextItem (-1, LVNI_SELECTED);
	if (idx == -1) return;
	/*
	CString Ean = m_ZerlList.GetItemText (idx, 1);
	A_ean->a_ean.ean = CStrFuncs::StrToDouble (Ean);
	A_ean->dbdelete ();
	m_ZerlList.DeleteItem (idx);
	if (idx >= m_ZerlList.GetItemCount ())
	{
		idx = m_ZerlList.GetItemCount () - 1;
		SetSelectedItem (idx);
	}
	if (m_ZerlList.GetItemCount () > 0)
	{
		Ean = m_ZerlList.GetItemText (idx, 1);
		CString EanBz = m_ZerlList.GetItemText (idx, 2);
		CString HEan = m_ZerlList.GetItemText (idx, 3);

		A_ean->a_ean.ean = CStrFuncs::StrToDouble (Ean);
		_tcscpy (A_ean->a_ean.ean_bz, EanBz.GetBuffer ()); 
		if (HEan == _T("X"))
		{
			_tcscpy (A_ean->a_ean.h_ean_kz, _T("1"));
		}
		else
		{
			_tcscpy (A_ean->a_ean.h_ean_kz, _T("0"));
		}
	}
	else
	{
		memcpy (&A_ean->a_ean, &a_ean_null, sizeof (A_EAN));
	}
	for (std::vector<CUpdateEvent *>::iterator e = UpdateTab.begin ();
			                                       e != UpdateTab.end (); ++ e)
	{
		CUpdateEvent *event = *e;
		event->Update (0);
	}
	*/
}

void CZerlListDlg::SetSysPar()
{
	/******
	if (dcs_a_par == -1)
	{
		strcpy ((LPSTR) sys_par.sys_par_nam, "dcs_a_par");
		if (Sys_par->dbreadfirst () == 0)
		{
			LPSTR p = (LPSTR) sys_par.sys_par_wrt;
			CDbUniCode::DbToUniCode (sys_par.sys_par_wrt, p);
			dcs_a_par = _tstoi (sys_par.sys_par_wrt);
		}
	}
	******/
}
