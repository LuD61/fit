#ifndef _PTABN_DEF
#define _PTABN_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct PTABN {
   TCHAR          ptitem[19];
   long           ptlfnr;
   TCHAR          ptwert[4];
   TCHAR          ptbez[33];
   TCHAR          ptbezk[9];
   TCHAR          ptwer1[9];
   TCHAR          ptwer2[9];
   short          delstatus;
   DATE_STRUCT    akv;
   DATE_STRUCT    bearb;
   TCHAR          pers_nam[9];
};
extern struct PTABN ptabn, ptabn_null;

#line 8 "ptabn.rh"

class PTABN_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               PTABN ptabn;
               PTABN_CLASS () : DB_CLASS ()
               {
               }
};
#endif
