#pragma once
#include "vector.h"
#include "propertyitem.h"

class CProperties :
	public CVector
{
public:
	CProperties(void);
	~CProperties(void);
    CString GetValue (CString Name);
    CString GetName (CString Value);
	int Find (CString Name);
	int FindValue (CString Value);
	void Set (CString Name, CString Value, int idx);
};
