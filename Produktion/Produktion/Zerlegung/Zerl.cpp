// Zerl.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Zerlegung.h"
#include "Zerl.h"
#include "StrFuncs.h"

// CZerl-Dialogfeld

IMPLEMENT_DYNAMIC(CZerl, CDialog) 

CZerl::CZerl(CWnd* pParent /*=NULL*/)
	: CDialog(CZerl::IDD, pParent)
{
	ChoiceMat = NULL;
	ChoiceKostArt = NULL;
	PageUpdate = NULL;

}

CZerl::~CZerl()
{
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}

	Mat1Form.FirstPosition ();
	while ((f = (CFormField *) Mat1Form.GetNext ()) != NULL)
	{
		delete f;
	}
	Mat2Form.FirstPosition ();
	while ((f = (CFormField *) Mat2Form.GetNext ()) != NULL)
	{
		delete f;
	}
	Mat3Form.FirstPosition ();
	while ((f = (CFormField *) Mat3Form.GetNext ()) != NULL)
	{
		delete f;
	}
	Mat4Form.FirstPosition ();
	while ((f = (CFormField *) Mat4Form.GetNext ()) != NULL)
	{
		delete f;
	}
	Mat5Form.FirstPosition ();
	while ((f = (CFormField *) Mat5Form.GetNext ()) != NULL)
	{
		delete f;
	}

	ZutForm.FirstPosition ();
	while ((f = (CFormField *) ZutForm.GetNext ()) != NULL)
	{
		delete f;
	}
	KostForm.FirstPosition ();
	while ((f = (CFormField *) KostForm.GetNext ()) != NULL)
	{
		delete f;
	}
	VkForm.FirstPosition ();
	while ((f = (CFormField *) VkForm.GetNext ()) != NULL)
	{
		delete f;
	}
}

void CZerl::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LAUS_MAT, m_LAusMat);
	DDX_Control(pDX, IDC_AUS_MAT, m_AusMat);
	DDX_Control(pDX, IDC_AUS_MAT_NAME, m_AusMatName);
	DDX_Control(pDX, IDC_MAT, m_Mat);
	DDX_Control(pDX, IDC_MAT_NAME, m_MatName);
	DDX_Control(pDX, IDC_LMAT, m_LMat);
	DDX_Control(pDX, IDC_MAT_GROUP, m_MatGroup);
	DDX_Control(pDX, IDC_LGEW, m_LGew);
	DDX_Control(pDX, IDC_GEW, m_Gew);
	DDX_Control(pDX, IDC_GEW_ANT, m_GewAnt);
	DDX_Control(pDX, IDC_LGEW_ANT, m_LGewAnt);
	DDX_Control(pDX, IDC_FWP, m_Fwp);
	DDX_Control(pDX, IDC_WERT_KG, m_MatoB);
	DDX_Control(pDX, IDC_LWERT_KG, m_LMatoB);
	DDX_Control(pDX, IDC_WERT_100KG, m_Wert100Kg);
	DDX_Control(pDX, IDC_LWERT_100KG, m_LWert100Kg);
	DDX_Control(pDX, IDC_LFWP, m_LFwp);
	DDX_Control(pDX, IDC_CHK_GEW, m_ChkGew);
	DDX_Control(pDX, IDC_CHK_GEW_ANT, m_ChkGewAnt);
	DDX_Control(pDX, IDC_CHK_WERT_KG, m_ChkMatoB);
	DDX_Control(pDX, IDC_CHK_WERT_100KG, m_ChkWert100Kg);
	DDX_Control(pDX, IDC_CHK_FWP, m_ChkFwp);
	DDX_Control(pDX, IDC_KENNZEICHEN, m_Kennzeichen);
	DDX_Control(pDX, IDC_CHK_KENNZEICHEN, m_ChkKennzeichen);
	DDX_Control(pDX, IDC_ZEITFAKTOR, m_Zeitfaktor);
	DDX_Control(pDX, IDC_LZeitfaktor, m_LZeitfaktor);
	DDX_Control(pDX, IDC_CHK_ZEITFAKTOR, m_ChkZeitfaktor);

	DDX_Control(pDX, IDC_MATERIAL_GROUP, m_MaterialGroup);
	//	DDX_Control(pDX, IDC_FWP, m_Fwp);
	DDX_Control(pDX, IDC_MAT_COMBO, m_MatCombo);
	DDX_Control(pDX, IDC_PREIS_COMBO, m_PreisCombo);
	DDX_Control(pDX, IDC_CHK_VARIABEL, m_Chk_Variabel);
	DDX_Control(pDX, IDC_CHK_FV, m_Chk_FV);
	DDX_Control(pDX, IDC_BUTTONZERLEGEN, m_ButtonZerlegen);
	DDX_Control(pDX, IDC_ENTERERGEBNIS, m_EnterErgebnis);

	DDX_Control(pDX, IDC_ZUTAT_GROUP, m_ZutatGroup);
	DDX_Control(pDX, IDC_LZUT_ZUGABE, m_LZutZugabe);
	DDX_Control(pDX, IDC_CHK_ZUT_ZUGABE, m_ChkZutZugabe);
	DDX_Control(pDX, IDC_ZUT_ZUGABE, m_ZutZugabe);
	DDX_Control(pDX, IDC_CHK_ZUT_PREIS, m_ChkZutPreis);
	DDX_Control(pDX, IDC_LZUT_PREIS, m_LZutPreis);
	DDX_Control(pDX, IDC_ZUT_PREIS, m_ZutPreis);
	DDX_Control(pDX, IDC_CHK_ZUT_WERT100kg, m_ChkZutWert100kg);
	DDX_Control(pDX, IDC_LZUT_WERT100kg, m_LZutWert100kg);
	DDX_Control(pDX, IDC_ZUT_WERT100kg, m_ZutWert100kg);
	DDX_Control(pDX, IDC_KOSTEN_GROUP, m_KostenGroup);
	DDX_Control(pDX, IDC_LKOST_BEZEICHNUNG, m_LKostBezeichnung);
	DDX_Control(pDX, IDC_KOST_BEZEICHNUNG, m_KostBezeichnung);
	DDX_Control(pDX, IDC_LKOST_AUFSCHLAG, m_LKostAufschlag);
	DDX_Control(pDX, IDC_KOST_AUFSCHLAG, m_KostAufschlag);
	DDX_Control(pDX, IDC_LKOST_AUFSCHLAG_GES, m_LKostWert);
	DDX_Control(pDX, IDC_KOST_AUFSCHLAG_GES, m_KostWert);
	DDX_Control(pDX, IDC_CHK_ZUT_BOLD, m_ChkZutBold);
	DDX_Control(pDX, IDC_CHK_WERT, m_ChkWert);
	DDX_Control(pDX, IDC_LWERT, m_LWert);
	DDX_Control(pDX, IDC_WERT, m_Wert);
	DDX_Control(pDX, IDC_CHK_VK, m_ChkVk);
	DDX_Control(pDX, IDC_LVK, m_LVk);
	DDX_Control(pDX, IDC_VK, m_Vk);
	DDX_Control(pDX, IDC_CHK_ZUT_WERT, m_ChkZutWert);
	DDX_Control(pDX, IDC_LZUT_WERT, m_LZutWert);
	DDX_Control(pDX, IDC_ZUT_WERT, m_ZutWert);
	DDX_Control(pDX, IDC_CHK_ZUT_GEW, m_ChkZutGew);
	DDX_Control(pDX, IDC_LZUT_GEW, m_LZutGew);
	DDX_Control(pDX, IDC_ZUT_GEW, m_ZutGew);
	DDX_Control(pDX, IDC_LKOST_FIX, m_LKostFix);
	DDX_Control(pDX, IDC_KOST_FIX, m_KostFix);
	DDX_Control(pDX, IDC_VKARTIKEL_GROUP, m_VkartikelGroup);
	DDX_Control(pDX, IDC_LMATOB, m_LMatob);
	DDX_Control(pDX, IDC_LPR_ZUTAT, m_LPrZutat);
	DDX_Control(pDX, IDC_LPR_VPK, m_LPrVpk);
	DDX_Control(pDX, IDC_LPR_KOSTEN, m_LPrKosten);
	DDX_Control(pDX, IDC_LPR_GH, m_LPrGh);
	DDX_Control(pDX, IDC_LPR_EH, m_LPrEh);
	DDX_Control(pDX, IDC_MATOB, m_Matob);
	DDX_Control(pDX, IDC_PR_ZUTAT, m_PrZutat);
	DDX_Control(pDX, IDC_PR_VPK, m_PrVpk);
	DDX_Control(pDX, IDC_PR_KOSTEN, m_PrKosten);
	DDX_Control(pDX, IDC_PR_GH, m_PrGh);
	DDX_Control(pDX, IDC_PR_EH, m_PrEh);
	DDX_Control(pDX, IDC_CHK_SINGLE_EXPAND, m_ChkSingleExpand);
	DDX_Control(pDX, IDC_CHK_VK_BOLD, m_ChkVkBold);
	DDX_Control(pDX, IDC_LGESWERT, m_LGesWert);
	DDX_Control(pDX, IDC_LDECKUNGA, m_LDeckungA);
	DDX_Control(pDX, IDC_LDECKUNGP, m_LDeckungP);
	DDX_Control(pDX, IDC_LGESEURO, m_LGesEuro);
	DDX_Control(pDX, IDC_LDECKUNGEURO, m_LDeckungEuro);
	DDX_Control(pDX, IDC_LDECKUNGPROZ, m_LDeckungProz);
	DDX_Control(pDX, IDC_GESWERT, m_GesWert);
	DDX_Control(pDX, IDC_DECKUNGA, m_DeckungA);
	DDX_Control(pDX, IDC_DECKUNGP, m_DeckungP);
	DDX_Control(pDX, IDC_ERGEBNIS_GROUP, m_ErgebnisGroup);
	DDX_Control(pDX, IDC_GRUNDPREIS, m_Grundpreis);
	DDX_Control(pDX, IDC_LGRUNDPREIS, m_LGrundpreis);
}


BEGIN_MESSAGE_MAP(CZerl, CDialog)
	ON_BN_CLICKED(IDC_ZERLSAVE, OnSave)
	ON_BN_CLICKED(IDC_MATERIALCHOICE , OnMatchoice)

	ON_BN_CLICKED(IDC_CHK_GEW, &CZerl::OnBnClickedChkGew)
	ON_BN_CLICKED(IDC_CHK_GEW_ANT, &CZerl::OnBnClickedChkGewAnt)
	ON_BN_CLICKED(IDC_CHK_WERT_KG, &CZerl::OnBnClickedChkMatoB)
	ON_BN_CLICKED(IDC_CHK_WERT_100KG, &CZerl::OnBnClickedChkWert100kg)
	ON_BN_CLICKED(IDC_CHK_KENNZEICHEN, &CZerl::OnBnClickedChkKennzeichen)
	ON_BN_CLICKED(IDC_CHK_ZEITFAKTOR, &CZerl::OnBnClickedChkZeitfaktor)
	ON_BN_CLICKED(IDC_CHK_FWP, &CZerl::OnBnClickedChkFwp)
	ON_BN_CLICKED(IDC_CHK_VARIABEL, &CZerl::OnBnClickedChkVariabel)
	ON_EN_CHANGE(IDC_GEW, &CZerl::OnEnChangeGew)
	ON_EN_SETFOCUS(IDC_GEW, &CZerl::OnEnSetfocusGew)
	ON_EN_KILLFOCUS(IDC_GEW, &CZerl::OnEnKillfocusGew)
	ON_EN_SETFOCUS(IDC_AUS_MAT, &CZerl::OnEnSetfocusAusMat)
	ON_EN_KILLFOCUS(IDC_AUS_MAT, &CZerl::OnEnKillfocusAusMat)
	ON_EN_SETFOCUS(IDC_MAT, &CZerl::OnEnSetfocusMat)
	ON_EN_KILLFOCUS(IDC_MAT, &CZerl::OnEnKillfocusMat)
	ON_BN_CLICKED(IDC_CHK_FV, &CZerl::OnBnClickedChkFv)
	ON_BN_CLICKED(IDC_BUTTONZERLEGEN, &CZerl::OnBnClickedButtonzerlegen)
	ON_EN_KILLFOCUS(IDC_GEW_ANT, &CZerl::OnEnKillfocusGewAnt)
	ON_EN_KILLFOCUS(IDC_WERT_KG, &CZerl::OnEnKillfocusWertKg)
	ON_EN_KILLFOCUS(IDC_WERT_100KG, &CZerl::OnEnKillfocusWert100kg)
	ON_EN_KILLFOCUS(IDC_FWP, &CZerl::OnEnKillfocusFwp)
	ON_CBN_SELCHANGE(IDC_MAT_COMBO, &CZerl::OnCbnSelchangeMatCombo)
	ON_EN_KILLFOCUS(IDC_ZUT_ZUGABE, &CZerl::OnEnKillfocusZutZugabe)
	ON_EN_KILLFOCUS(IDC_ZUT_PREIS, &CZerl::OnEnKillfocusZutPreis)
	ON_EN_KILLFOCUS(IDC_ZUT_WERT100kg, &CZerl::OnEnKillfocusZutWert100kg)
	ON_BN_CLICKED(IDC_CHK_ZUT_BOLD, &CZerl::OnBnClickedChkZutBold)
	ON_EN_SETFOCUS(IDC_GEW_ANT, &CZerl::OnEnSetfocusGewAnt)
	ON_EN_SETFOCUS(IDC_WERT_KG, &CZerl::OnEnSetfocusWertKg)
	ON_EN_SETFOCUS(IDC_WERT_100KG, &CZerl::OnEnSetfocusWert100kg)
	ON_EN_SETFOCUS(IDC_FWP, &CZerl::OnEnSetfocusFwp)
	ON_EN_SETFOCUS(IDC_ZUT_ZUGABE, &CZerl::OnEnSetfocusZutZugabe)
	ON_EN_SETFOCUS(IDC_ZUT_PREIS, &CZerl::OnEnSetfocusZutPreis)
	ON_EN_SETFOCUS(IDC_ZUT_WERT100kg, &CZerl::OnEnSetfocusZutWert100kg)
	ON_EN_SETFOCUS(IDC_KOST_BEZEICHNUNG, &CZerl::OnEnSetfocusKostBezeichnung)
	ON_EN_SETFOCUS(IDC_KOST_AUFSCHLAG, &CZerl::OnEnSetfocusKostAufschlag)
	ON_EN_SETFOCUS(IDC_KOST_AUFSCHLAG_GES, &CZerl::OnEnSetfocusKostAufschlagGes)
	ON_BN_CLICKED(IDC_CHK_ZUT_ZUGABE, &CZerl::OnBnClickedChkZutZugabe)
	ON_BN_CLICKED(IDC_CHK_ZUT_PREIS, &CZerl::OnBnClickedChkZutPreis)
	ON_BN_CLICKED(IDC_CHK_ZUT_WERT100kg, &CZerl::OnBnClickedChkZutWert100kg)
	ON_BN_CLICKED(IDC_CHK_WERT, &CZerl::OnBnClickedChkWert)
	ON_EN_SETFOCUS(IDC_WERT, &CZerl::OnEnSetfocusWert)
	ON_BN_CLICKED(IDC_CHK_VK, &CZerl::OnBnClickedChkVk)
	ON_EN_SETFOCUS(IDC_VK, &CZerl::OnEnSetfocusVk)
	ON_BN_CLICKED(IDC_CHK_ZUT_WERT, &CZerl::OnBnClickedChkZutWert)
	ON_BN_CLICKED(IDC_CHK_ZUT_GEW, &CZerl::OnBnClickedChkZutGew)
	ON_EN_SETFOCUS(IDC_ZUT_WERT, &CZerl::OnEnSetfocusZutWert)
	ON_EN_SETFOCUS(IDC_ZUT_GEW, &CZerl::OnEnSetfocusZutGew)
	ON_EN_SETFOCUS(IDC_KOST_FIX, &CZerl::OnEnSetfocusKostFix)
	ON_EN_SETFOCUS(IDC_MATOB, &CZerl::OnEnSetfocusMatob)
	ON_EN_SETFOCUS(IDC_PR_ZUTAT, &CZerl::OnEnSetfocusPrZutat)
	ON_EN_SETFOCUS(IDC_PR_VPK, &CZerl::OnEnSetfocusPrVpk)
	ON_EN_SETFOCUS(IDC_PR_KOSTEN, &CZerl::OnEnSetfocusPrKosten)
	ON_EN_SETFOCUS(IDC_PR_GH, &CZerl::OnEnSetfocusPrGh)
	ON_EN_SETFOCUS(IDC_PR_EH, &CZerl::OnEnSetfocusPrEh)
	ON_BN_CLICKED(IDC_CHK_SINGLE_EXPAND, &CZerl::OnBnClickedChkSingleExpand)
	ON_BN_CLICKED(IDC_CHK_VK_BOLD, &CZerl::OnBnClickedChkVkBold)
	ON_CBN_SELCHANGE(IDC_PREIS_COMBO, &CZerl::OnCbnSelchangePreisCombo)
	ON_EN_SETFOCUS(IDC_ZEITFAKTOR, &CZerl::OnEnSetfocusZeitfaktor)
	ON_EN_KILLFOCUS(IDC_GRUNDPREIS, &CZerl::OnEnKillfocusGrundpreis)
END_MESSAGE_MAP()

BOOL CZerl::Write ()
{
	if (!TestMat ()) return FALSE;
    UpdateZermMatV();
	for (std::vector<CUpdateEvent *>::iterator e =  UpdateTab.begin (); e != UpdateTab.end (); ++e)
	{
		CUpdateEvent *event = *e;
		event->Update (10);  // 10 = Speichern in DB
	}
	m_Mat.SetFocus();
	return TRUE;
}
void CZerl::OnSave ()
{
//	Umlasten (21) ; //mit FWPNeuAufbauen
	if (Schnitt->cfg_FWP_berechnen && Schnitt->schnitt.kalk_bas == 1)
	{
		Umlasten (22) ; //mit FWP_aus_AVarb
	}
	else
	{
		Umlasten (21) ; //mit FWPNeuAufbauen
	}

}

// CZerl-Meldungshandler
BOOL CZerl::OnInitDialog()
{

	CDialog::OnInitDialog();

    m_Save.Create (_T("Speichern"), WS_CHILD | WS_VISIBLE | SS_BLACKRECT, 
		                          CRect (0, 0, 40, 32),
		                          this, IDC_ZERLSAVE);
//	m_Save.LoadBitmap (IDB_SAVE);IDB_UNCHECK
	m_Save.LoadBitmap (IDB_Folderopen);

	Zerm  = &Page1->Zerm;
	Schnitt = &Page1->Schnitt;
	A_mat = &Page1->A_mat;
	A_bas = &Page1->A_bas;
	Work = &Page1->Work;
	if (Schnitt->schnitt.kalk_bas == 1) // fwp
	{
		if (Schnitt->cfg_FWP_berechnen)
		{
			m_Save.SetToolTip (_T("alle FWP aus dem Artikelstamm holen und neu berechnen "));
		}
		else
		{
			m_Save.SetToolTip (_T("Preise aus dem Artikelstamm neu laden"));
		}
	}
	else if (Schnitt->schnitt.kalk_bas == 2)
	{
			m_Save.SetToolTip (_T("Marktpreise aus Preisgruppenstufe neu laden"));
	}

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
		FontBig.CreatePointFont (135, _T("Dlg"));
	}
	Form.Add (new CFormField    (&m_ChkZutBold,CHECKBOX,     (short *) &Zerm->ZEIGE_KOSTEN, VSHORT));
	Form.Add (new CFormField    (&m_ChkVkBold,CHECKBOX,     (short *) &Zerm->ZEIGE_VK, VSHORT));
	Form.Add (new CFormField    (&m_ChkSingleExpand,CHECKBOX,     (short *) &Zerm->SINGLE_EXPAND, VSHORT));
	Form.Add (new CFormField    (&m_AusMat,EDIT,     (long *) &Zerm->zerm.zerm_aus_mat, VLONG));
	Form.Add (new CFormField    (&m_MatCombo,COMBOBOX, (short *) &Zerm->zerm.a_typ, VSHORT));
	Form.Add (new CFormField    (&m_PreisCombo,COMBOBOX, (short *) &Schnitt->schnitt._PreisArt, VSHORT));
	Form.Add (new CFormField    (&m_Mat,EDIT,     (long *) &Zerm->zerm.zerm_mat, VLONG));
    Form.Add (new CUniFormField (&m_AusMatName,EDIT,      (LPTSTR)   Zerm->zerm.zerm_aus_mat_bz, VCHAR));
	Form.Add (new CUniFormField (&m_MatName,EDIT,      (LPTSTR)   Zerm->zerm.kost_bz, VCHAR));
    Form.Add (new CUniFormField (&m_Grundpreis,EDIT, (double *) &Schnitt->schnitt.ek_pr, VDOUBLE,8,4));


//Materialien 1 MaterialPreis 
	Mat1Form.Add (new CFormField    (&m_GesWert,EDIT,     (double *) &Schnitt->schnitt._SumPreis, VDOUBLE,9,3));
	Mat1Form.Add (new CFormField    (&m_DeckungA,EDIT,     (double *) &Schnitt->schnitt._DeckungA, VDOUBLE,9,2));
	Mat1Form.Add (new CFormField    (&m_DeckungP,EDIT,     (double *) &Schnitt->schnitt._DeckungP, VDOUBLE,9,2));

	Mat1Form.Add (new CFormField    (&m_ChkGew,CHECKBOX,     (short *) &Zerm->zerm.chk_gew, VSHORT));
	Mat1Form.Add (new CFormField    (&m_ChkGewAnt,CHECKBOX,     (short *) &Zerm->zerm.chk_gew_ant, VSHORT));
	Mat1Form.Add (new CFormField    (&m_ChkMatoB,CHECKBOX,     (short *) &Zerm->zerm.chk_mat_o_b, VSHORT));
	Mat1Form.Add (new CFormField    (&m_ChkWert,CHECKBOX,     (short *) &Zerm->zerm.chk_wert, VSHORT));
	Mat1Form.Add (new CFormField    (&m_ChkVk,CHECKBOX,     (short *) &Zerm->zerm.chk_vk, VSHORT));
	Mat1Form.Add (new CFormField    (&m_ChkWert100Kg,CHECKBOX,     (short *) &Zerm->zerm.chk_wert_100kg, VSHORT));
	Mat1Form.Add (new CFormField    (&m_ChkFwp,CHECKBOX,     (short *) &Zerm->zerm.chk_fwp, VSHORT));

	Mat1Form.Add (new CFormField    (&m_Gew,EDIT,     (double *) &Zerm->zerm.zerm_gew, VDOUBLE,8,3));
	Mat1Form.Add (new CFormField    (&m_GewAnt,EDIT,     (double *) &Zerm->zerm.zerm_gew_ant, VDOUBLE,5,2));
	Mat1Form.Add (new CFormField    (&m_Fwp,EDIT,     (double *) &Zerm->zerm.fwp, VDOUBLE,5,2));
	Mat1Form.Add (new CFormField    (&m_MatoB,EDIT,     (double *) &Zerm->zerm.mat_o_b, VDOUBLE,7,3));
	Mat1Form.Add (new CFormField    (&m_Wert,EDIT,     (double *) &Zerm->zerm.teilkosten_abs, VDOUBLE,7,3));
	Mat1Form.Add (new CFormField    (&m_Vk,EDIT,     (double *) &Zerm->zerm.vk_pr, VDOUBLE,8,3));
	Mat1Form.Add (new CFormField    (&m_Wert100Kg,EDIT,     (double *) &Zerm->zerm.dm_100, VDOUBLE,9,3));

//Materialien 2 HK-Kosten  
	Mat2Form.Add (new CFormField    (&m_GesWert,EDIT,     (double *) &Schnitt->schnitt._SumPreisHK, VDOUBLE,9,3));
	Mat2Form.Add (new CFormField    (&m_DeckungA,EDIT,     (double *) &Schnitt->schnitt._DeckungAHK, VDOUBLE,9,2));
	Mat2Form.Add (new CFormField    (&m_DeckungP,EDIT,     (double *) &Schnitt->schnitt._DeckungPHK, VDOUBLE,9,2));

	Mat2Form.Add (new CFormField    (&m_ChkGew,CHECKBOX,     (short *) &Zerm->zerm.chk_gew, VSHORT));
	Mat2Form.Add (new CFormField    (&m_ChkGewAnt,CHECKBOX,     (short *) &Zerm->zerm.chk_gew_ant, VSHORT));
	Mat2Form.Add (new CFormField    (&m_ChkMatoB,CHECKBOX,     (short *) &Zerm->zerm.chk_hk_vollk_preis, VSHORT));
	Mat2Form.Add (new CFormField    (&m_ChkWert,CHECKBOX,     (short *) &Zerm->zerm.chk_hk_vollk_wert, VSHORT));
	Mat2Form.Add (new CFormField    (&m_ChkVk,CHECKBOX,     (short *) &Zerm->zerm.chk_hk_vollk, VSHORT));
	Mat2Form.Add (new CFormField    (&m_ChkWert100Kg,CHECKBOX,     (short *) &Zerm->zerm.chk_wert_100kg, VSHORT));
	Mat2Form.Add (new CFormField    (&m_ChkFwp,CHECKBOX,     (short *) &Zerm->zerm.chk_fwp, VSHORT));

	Mat2Form.Add (new CFormField    (&m_Gew,EDIT,     (double *) &Zerm->zerm.zerm_gew, VDOUBLE,8,3));
	Mat2Form.Add (new CFormField    (&m_GewAnt,EDIT,     (double *) &Zerm->zerm.zerm_gew_ant, VDOUBLE,5,2));
	Mat2Form.Add (new CFormField    (&m_Fwp,EDIT,     (double *) &Zerm->zerm.fwp, VDOUBLE,5,2));
	Mat2Form.Add (new CFormField    (&m_MatoB,EDIT,     (double *) &Zerm->hk_vollk_preis, VDOUBLE,7,3));
	Mat2Form.Add (new CFormField    (&m_Wert,EDIT,     (double *) &Zerm->hk_vollk_wert, VDOUBLE,7,3));
	Mat2Form.Add (new CFormField    (&m_Vk,EDIT,     (double *) &Zerm->hk_vollk_kosten, VDOUBLE,8,3));
//	Mat2Form.Add (new CFormField    (&m_Wert100Kg,EDIT,     (double *) &Zerm->zerm.zerm_gew, VDOUBLE,9,3));

	Mat2Form.Add (new CUniFormField (&m_Kennzeichen,EDIT,      (LPTSTR)   Zerm->zerm.a_kz, VCHAR));
	Mat2Form.Add (new CFormField    (&m_ChkKennzeichen,CHECKBOX,     (short *) &Zerm->zerm.chk_kennzeichen, VSHORT));
	Mat2Form.Add (new CFormField	(&m_Zeitfaktor,EDIT,      (long *)   &Zerm->zerm.a_z_f, VLONG));
	Mat2Form.Add (new CFormField    (&m_ChkZeitfaktor,CHECKBOX,     (short *) &Zerm->zerm.chk_zeitfaktor, VSHORT));

//Materialien 3 SK_Preis  
	Mat3Form.Add (new CFormField    (&m_GesWert,EDIT,     (double *) &Schnitt->schnitt._SumPreisSK, VDOUBLE,9,3));
	Mat3Form.Add (new CFormField    (&m_DeckungA,EDIT,     (double *) &Schnitt->schnitt._DeckungASK, VDOUBLE,9,2));
	Mat3Form.Add (new CFormField    (&m_DeckungP,EDIT,     (double *) &Schnitt->schnitt._DeckungPSK, VDOUBLE,9,2));

	Mat3Form.Add (new CFormField    (&m_ChkGew,CHECKBOX,     (short *) &Zerm->zerm.chk_gew, VSHORT));
	Mat3Form.Add (new CFormField    (&m_ChkGewAnt,CHECKBOX,     (short *) &Zerm->zerm.chk_gew_ant, VSHORT));
	Mat3Form.Add (new CFormField    (&m_ChkMatoB,CHECKBOX,     (short *) &Zerm->zerm.chk_mat_o_b, VSHORT));
	Mat3Form.Add (new CFormField    (&m_ChkWert,CHECKBOX,     (short *) &Zerm->zerm.chk_wert, VSHORT));
	Mat3Form.Add (new CFormField    (&m_ChkVk,CHECKBOX,     (short *) &Zerm->zerm.chk_vk, VSHORT));
	Mat3Form.Add (new CFormField    (&m_ChkWert100Kg,CHECKBOX,     (short *) &Zerm->zerm.chk_wert_100kg, VSHORT));
	Mat3Form.Add (new CFormField    (&m_ChkFwp,CHECKBOX,     (short *) &Zerm->zerm.chk_fwp, VSHORT));

	Mat3Form.Add (new CFormField    (&m_Gew,EDIT,     (double *) &Zerm->zerm.zerm_gew, VDOUBLE,8,3));
	Mat3Form.Add (new CFormField    (&m_GewAnt,EDIT,     (double *) &Zerm->zerm.zerm_gew_ant, VDOUBLE,5,2));
	Mat3Form.Add (new CFormField    (&m_Fwp,EDIT,     (double *) &Zerm->zerm.fwp, VDOUBLE,5,2));
	Mat3Form.Add (new CFormField    (&m_MatoB,EDIT,     (double *) &Zerm->zerm.mat_o_b, VDOUBLE,7,3));
	Mat3Form.Add (new CFormField    (&m_Wert,EDIT,     (double *) &Zerm->zerm.teilkosten_abs, VDOUBLE,7,3));
	Mat3Form.Add (new CFormField    (&m_Vk,EDIT,     (double *) &Zerm->zerm.vk_pr, VDOUBLE,8,3));
	Mat3Form.Add (new CFormField    (&m_Wert100Kg,EDIT,     (double *) &Zerm->zerm.dm_100, VDOUBLE,9,3));

//Materialien 4 Fil_Ek_Preis  
	Mat4Form.Add (new CFormField    (&m_GesWert,EDIT,     (double *) &Schnitt->schnitt._SumPreisFIL_EK, VDOUBLE,9,3));
	Mat4Form.Add (new CFormField    (&m_DeckungA,EDIT,     (double *) &Schnitt->schnitt._DeckungAFIL_EK, VDOUBLE,9,2));
	Mat4Form.Add (new CFormField    (&m_DeckungP,EDIT,     (double *) &Schnitt->schnitt._DeckungPFIL_EK, VDOUBLE,9,2));

	Mat4Form.Add (new CFormField    (&m_ChkGew,CHECKBOX,     (short *) &Zerm->zerm.chk_gew, VSHORT));
	Mat4Form.Add (new CFormField    (&m_ChkGewAnt,CHECKBOX,     (short *) &Zerm->zerm.chk_gew_ant, VSHORT));
	Mat4Form.Add (new CFormField    (&m_ChkMatoB,CHECKBOX,     (short *) &Zerm->zerm.chk_mat_o_b, VSHORT));
	Mat4Form.Add (new CFormField    (&m_ChkWert,CHECKBOX,     (short *) &Zerm->zerm.chk_wert, VSHORT));
	Mat4Form.Add (new CFormField    (&m_ChkVk,CHECKBOX,     (short *) &Zerm->zerm.chk_vk, VSHORT));
	Mat4Form.Add (new CFormField    (&m_ChkWert100Kg,CHECKBOX,     (short *) &Zerm->zerm.chk_wert_100kg, VSHORT));
	Mat4Form.Add (new CFormField    (&m_ChkFwp,CHECKBOX,     (short *) &Zerm->zerm.chk_fwp, VSHORT));

	Mat4Form.Add (new CFormField    (&m_Gew,EDIT,     (double *) &Zerm->zerm.zerm_gew, VDOUBLE,8,3));
	Mat4Form.Add (new CFormField    (&m_GewAnt,EDIT,     (double *) &Zerm->zerm.zerm_gew_ant, VDOUBLE,5,2));
	Mat4Form.Add (new CFormField    (&m_Fwp,EDIT,     (double *) &Zerm->zerm.fwp, VDOUBLE,5,2));
	Mat4Form.Add (new CFormField    (&m_MatoB,EDIT,     (double *) &Zerm->zerm.mat_o_b, VDOUBLE,7,3));
	Mat4Form.Add (new CFormField    (&m_Wert,EDIT,     (double *) &Zerm->zerm.teilkosten_abs, VDOUBLE,7,3));
	Mat4Form.Add (new CFormField    (&m_Vk,EDIT,     (double *) &Zerm->zerm.vk_pr, VDOUBLE,8,3));
	Mat4Form.Add (new CFormField    (&m_Wert100Kg,EDIT,     (double *) &Zerm->zerm.dm_100, VDOUBLE,9,3));

//Materialien 4 Fil_Vk_Preis  
	Mat5Form.Add (new CFormField    (&m_GesWert,EDIT,     (double *) &Schnitt->schnitt._SumPreisFIL_VK, VDOUBLE,9,3));
	Mat5Form.Add (new CFormField    (&m_DeckungA,EDIT,     (double *) &Schnitt->schnitt._DeckungAFIL_VK, VDOUBLE,9,2));
	Mat5Form.Add (new CFormField    (&m_DeckungP,EDIT,     (double *) &Schnitt->schnitt._DeckungPFIL_VK, VDOUBLE,9,2));

	Mat5Form.Add (new CFormField    (&m_ChkGew,CHECKBOX,     (short *) &Zerm->zerm.chk_gew, VSHORT));
	Mat5Form.Add (new CFormField    (&m_ChkGewAnt,CHECKBOX,     (short *) &Zerm->zerm.chk_gew_ant, VSHORT));
	Mat5Form.Add (new CFormField    (&m_ChkMatoB,CHECKBOX,     (short *) &Zerm->zerm.chk_mat_o_b, VSHORT));
	Mat5Form.Add (new CFormField    (&m_ChkWert,CHECKBOX,     (short *) &Zerm->zerm.chk_wert, VSHORT));
	Mat5Form.Add (new CFormField    (&m_ChkVk,CHECKBOX,     (short *) &Zerm->zerm.chk_vk, VSHORT));
	Mat5Form.Add (new CFormField    (&m_ChkWert100Kg,CHECKBOX,     (short *) &Zerm->zerm.chk_wert_100kg, VSHORT));
	Mat5Form.Add (new CFormField    (&m_ChkFwp,CHECKBOX,     (short *) &Zerm->zerm.chk_fwp, VSHORT));

	Mat5Form.Add (new CFormField    (&m_Gew,EDIT,     (double *) &Zerm->zerm.zerm_gew, VDOUBLE,8,3));
	Mat5Form.Add (new CFormField    (&m_GewAnt,EDIT,     (double *) &Zerm->zerm.zerm_gew_ant, VDOUBLE,5,2));
	Mat5Form.Add (new CFormField    (&m_Fwp,EDIT,     (double *) &Zerm->zerm.fwp, VDOUBLE,5,2));
	Mat5Form.Add (new CFormField    (&m_MatoB,EDIT,     (double *) &Zerm->zerm.mat_o_b, VDOUBLE,7,3));
	Mat5Form.Add (new CFormField    (&m_Wert,EDIT,     (double *) &Zerm->zerm.teilkosten_abs, VDOUBLE,7,3));
	Mat5Form.Add (new CFormField    (&m_Vk,EDIT,     (double *) &Zerm->zerm.vk_pr, VDOUBLE,8,3));
	Mat5Form.Add (new CFormField    (&m_Wert100Kg,EDIT,     (double *) &Zerm->zerm.dm_100, VDOUBLE,9,3));

    Form.Add (new CFormField    (&m_Chk_Variabel,CHECKBOX,   (char *) Zerm->zerm.variabel, VCHAR));
//Zutaten	
	ZutForm.Add (new CFormField    (&m_ZutZugabe,EDIT,     (double *) &Zerm->zerm.zerm_gew_ant, VDOUBLE,8,3));
//	ZutForm.Add (new CFormField    (&m_ZutGew,EDIT,     (double *) &Zerm->zerm.zerm_gew, VDOUBLE,8,3));
	ZutForm.Add (new CFormField    (&m_ZutGew,EDIT,     (double *) &Zerm->zerm.zerm_gew, VDOUBLE,8,3));
	ZutForm.Add (new CFormField    (&m_ZutPreis,EDIT,     (double *) &Zerm->zerm.mat_o_b, VDOUBLE,5,2));
	ZutForm.Add (new CFormField    (&m_ZutWert,EDIT,     (double *) &Zerm->zerm.teilkosten_abs, VDOUBLE,9,3));
	ZutForm.Add (new CFormField    (&m_ZutWert100kg,EDIT,     (double *) &Zerm->zerm.dm_100, VDOUBLE,9,3));
//Kosten
	KostForm.Add (new CUniFormField (&m_KostBezeichnung,EDIT,      (LPTSTR)   Zerm->zerm.kost_bz, VCHAR));
	KostForm.Add (new CFormField    (&m_KostAufschlag,EDIT,     (double *) &Zerm->zerm.mat_o_b, VDOUBLE,9,3));
	KostForm.Add (new CFormField    (&m_KostFix,EDIT,     (double *) &Zerm->zerm.vollkosten_abs, VDOUBLE,9,3));
	KostForm.Add (new CFormField    (&m_KostWert,EDIT,     (double *) &Zerm->zerm.teilkosten_abs, VDOUBLE,9,3));

//VK-Artikel
	VkForm.Add (new CFormField    (&m_Matob,EDIT,     (double *) &Zerm->zerm.mat_o_b, VDOUBLE,9,4));
	VkForm.Add (new CFormField    (&m_PrZutat,EDIT,     (double *) &Zerm->zerm.pr_zutat, VDOUBLE,9,4));
	VkForm.Add (new CFormField    (&m_PrVpk,EDIT,     (double *) &Zerm->zerm.pr_vpk, VDOUBLE,9,4));
	VkForm.Add (new CFormField    (&m_PrKosten,EDIT,     (double *) &Zerm->zerm.pr_kosten, VDOUBLE,9,4));
	VkForm.Add (new CFormField    (&m_PrGh,EDIT,     (double *) &Zerm->zerm.pr_vk_netto, VDOUBLE,9,4));
	VkForm.Add (new CFormField    (&m_PrEh,EDIT,     (double *) &Zerm->zerm.pr_vk_eh, VDOUBLE,9,4));

	m_EnterErgebnis.nID = IDC_ENTERERGEBNIS;
//	m_EnterErgebnis.MoveWindow (CRect (0, 0, 60, 20));
	m_EnterErgebnis.SetWindowText (_T("Ergebnis:   Mat.Preis"));
//	m_EnterErgebnis.SetToolTip (_T("Details anzeigen (in dieser Version noch nicht aktiv) "));

//	m_LWert100Kg.SetWindowText (_T("Kostenanteil in %"));

    CtrlGrid.Create (this, 22, 22);
    CtrlGrid.SetBorder (10, 10);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this, &Font);
	CtrlGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

//	m_AusMat.SetReadOnly ();
	CCtrlInfo *c_MatGroup = new CCtrlInfo (&m_MatGroup, 0, 0, 8, 99);
	CtrlGrid.Add (c_MatGroup);

	CCtrlInfo *c_LAusMat = new CCtrlInfo (&m_LAusMat, 1, 1, 1, 1);
	CtrlGrid.Add (c_LAusMat);

	CCtrlInfo *c_AusMat = new CCtrlInfo (&m_AusMat, 2, 1, 1, 1);
	CtrlGrid.Add (c_AusMat);

	CCtrlInfo *c_AusMatName = new CCtrlInfo (&m_AusMatName, 3, 1, 2, 2);
	CtrlGrid.Add (c_AusMatName);

	CCtrlInfo *c_Save = new CCtrlInfo (&m_Save, -1, -1, 1, 1);
//	c_Save->SetCellPos (30, 0, 0, 0);
	CtrlGrid.Add (c_Save);

	CCtrlInfo *c_MatCombo = new CCtrlInfo (&m_MatCombo, 1, 2, 1, 1);
	CtrlGrid.Add (c_MatCombo);

//Grid Material
			MaterialGrid.Create (this, 2, 2);
		    MaterialGrid.SetBorder (0, 0);
		    MaterialGrid.SetGridSpace (0, 0);
			CCtrlInfo *c_Material = new CCtrlInfo (&m_Mat, 0, 0, 1, 1);
			MaterialGrid.Add (c_Material);
			CtrlGrid.CreateChoiceButton (m_MatChoice, IDC_MATERIALCHOICE, this);
			CCtrlInfo *c_MatChoice = new CCtrlInfo (&m_MatChoice, 1, 0, 1, 1);
			MaterialGrid.Add (c_MatChoice);



	CCtrlInfo *c_Mat = new CCtrlInfo (&MaterialGrid, 2, 2, 1, 1);
	CtrlGrid.Add (c_Mat);

	CCtrlInfo *c_MatName = new CCtrlInfo (&m_MatName, 3, 2, 6, 6);
	CtrlGrid.Add (c_MatName);

//Grid Zerlegen 
			ZerlegenGrid.Create (this, 2, 2);
		    ZerlegenGrid.SetBorder (0, 0);
		    ZerlegenGrid.SetGridSpace (0, 0);
		    ZerlegenGrid.SetCellHeight (8);
		    ZerlegenGrid.SetFontCellHeight (this, &Font);
			ZerlegenGrid.SetFont (&Font);
			CCtrlInfo *c_ButtonZerlegen = new CCtrlInfo (&m_ButtonZerlegen, 0, 0, 2, 1);
			ZerlegenGrid.Add (c_ButtonZerlegen);
	CCtrlInfo *c_ZerlegenGrid = new CCtrlInfo (&ZerlegenGrid, 2, 3, 2, 1);
	CtrlGrid.Add (c_ZerlegenGrid);

//=====================================================
//Grid MaterialDaten
//=====================================================
	 MatGrid.Create (this, 8, 8);
     MatGrid.SetBorder (0, 0);
     MatGrid.SetCellHeight (15);
     MatGrid.SetFontCellHeight (this);
     MatGrid.SetFontCellHeight (this, &Font);
     MatGrid.SetGridSpace (5, 6);
// Rahmen Material
	CCtrlInfo *c_MaterialGroup = new CCtrlInfo (&m_MaterialGroup, 0, 0, 8, 8);
	MatGrid.Add (c_MaterialGroup);
	
	CCtrlInfo *c_PreisCombo = new CCtrlInfo (&m_PreisCombo, 0, 0, 3, 3);
	MatGrid.Add (c_PreisCombo);
    //Gewicht
	CCtrlInfo *c_ChkGew = new CCtrlInfo (&m_ChkGew, 1, 1, 1, 1);
	MatGrid.Add (c_ChkGew);
	CCtrlInfo *c_LGew = new CCtrlInfo (&m_LGew, 2, 1, 1, 1);
	MatGrid.Add (c_LGew);
	CCtrlInfo *c_Gew = new CCtrlInfo (&m_Gew, 3, 1, 1, 1);
	MatGrid.Add (c_Gew);
    //Gewichtsanteil
	CCtrlInfo *c_ChkGewAnt = new CCtrlInfo (&m_ChkGewAnt, 1, 2, 1, 1);
	MatGrid.Add (c_ChkGewAnt);
	CCtrlInfo *c_LGewAnt = new CCtrlInfo (&m_LGewAnt, 2, 2, 1, 1);
	MatGrid.Add (c_LGewAnt);
	CCtrlInfo *c_GewAnt = new CCtrlInfo (&m_GewAnt, 3, 2, 1, 1);
	MatGrid.Add (c_GewAnt);
    //FWP
	CCtrlInfo *c_ChkFwp = new CCtrlInfo (&m_ChkFwp, 1, 3, 1, 1);
	MatGrid.Add (c_ChkFwp);
	CCtrlInfo *c_LFwp = new CCtrlInfo (&m_LFwp, 2, 3, 1, 1);
	MatGrid.Add (c_LFwp);
	CCtrlInfo *c_Fwp = new CCtrlInfo (&m_Fwp, 3, 3, 1, 1);
	MatGrid.Add (c_Fwp);
    //Preis mat_o_b
	CCtrlInfo *c_ChkMatoB = new CCtrlInfo (&m_ChkMatoB, 1, 4, 1, 1);
	MatGrid.Add (c_ChkMatoB);

	CCtrlInfo *c_LMatoB = new CCtrlInfo (&m_LMatoB, 2, 4, 1, 1);
	MatGrid.Add (c_LMatoB);
//	CCtrlInfo *c_PreisCombo = new CCtrlInfo (&m_PreisCombo, 2, 4, 1, 1);
//	MatGrid.Add (c_PreisCombo);
	CCtrlInfo *c_MatoB = new CCtrlInfo (&m_MatoB, 3, 4, 1, 1);
	MatGrid.Add (c_MatoB);
    //Wert (Preis * Gewicht)
	CCtrlInfo *c_ChkWert = new CCtrlInfo (&m_ChkWert, 1, 5, 1, 1);
	MatGrid.Add (c_ChkWert);
	CCtrlInfo *c_LWert = new CCtrlInfo (&m_LWert, 2, 5, 1, 1);
	MatGrid.Add (c_LWert);
	CCtrlInfo *c_Wert = new CCtrlInfo (&m_Wert, 3, 5, 1, 1);
	MatGrid.Add (c_Wert);
    //VK Netto
	CCtrlInfo *c_ChkVk = new CCtrlInfo (&m_ChkVk, 1, 6, 1, 1);
	MatGrid.Add (c_ChkVk);
	CCtrlInfo *c_LVk = new CCtrlInfo (&m_LVk, 2, 6, 1, 1);
	MatGrid.Add (c_LVk);
	CCtrlInfo *c_Vk = new CCtrlInfo (&m_Vk, 3, 6, 1, 1);
	MatGrid.Add (c_Vk);

    //Wert /100 Kg
	CCtrlInfo *c_ChkWert100Kg = new CCtrlInfo (&m_ChkWert100Kg, 1, 7, 1, 1);
	MatGrid.Add (c_ChkWert100Kg);
	CCtrlInfo *c_LWert100Kg = new CCtrlInfo (&m_LWert100Kg, 2, 7, 1, 1);
	MatGrid.Add (c_LWert100Kg);
	CCtrlInfo *c_Wert100Kg = new CCtrlInfo (&m_Wert100Kg, 3, 7, 1, 1);
	MatGrid.Add (c_Wert100Kg);

    //Kennzeichen
	CCtrlInfo *c_ChkKennzeichen = new CCtrlInfo (&m_ChkKennzeichen, 1, 7, 1, 1);
	MatGrid.Add (c_ChkKennzeichen);
	CCtrlInfo *c_Kennzeichen = new CCtrlInfo (&m_Kennzeichen, 3, 7, 1, 1);
	MatGrid.Add (c_Kennzeichen);
    //Zeitfaktor
	CCtrlInfo *c_ChkZeitfaktor = new CCtrlInfo (&m_ChkZeitfaktor, 1, 8, 1, 1);
	MatGrid.Add (c_ChkZeitfaktor);
	CCtrlInfo *c_LZeitfaktor = new CCtrlInfo (&m_LZeitfaktor, 2, 8, 1, 1);
	MatGrid.Add (c_LZeitfaktor);
	CCtrlInfo *c_Zeitfaktor = new CCtrlInfo (&m_Zeitfaktor, 3, 8, 1, 1);
	MatGrid.Add (c_Zeitfaktor);

    //Fix-Variabel
//	CCtrlInfo *c_Chk_Variabel = new CCtrlInfo (&m_Chk_Variabel, 2, 8, 1, 1);
//	MatGrid.Add (c_Chk_Variabel);

	/***
	CCtrlInfo *c_ChkFV = new CCtrlInfo (&m_Chk_FV, 1, 8, 1, 1);
	MatGrid.Add (c_ChkFV);
	***/

//=====================================================
//Grid Zutaten Daten 
//=====================================================
	 ZutGrid.Create (this, 20, 20);
     ZutGrid.SetBorder (0, 0);
     ZutGrid.SetCellHeight (15);
     ZutGrid.SetFontCellHeight (this);
     ZutGrid.SetFontCellHeight (this, &Font);
     ZutGrid.SetGridSpace (5, 6);
// Rahmen Zutaten
	CCtrlInfo *c_ZutatGroup = new CCtrlInfo (&m_ZutatGroup, 0, 0, 4, 8);
	ZutGrid.Add (c_ZutatGroup);
	
    //Zugabe g/kg
//	CCtrlInfo *c_ChkZutZugabe = new CCtrlInfo (&m_ChkZutZugabe, 1, 1, 1, 1);
//	ZutGrid.Add (c_ChkZutZugabe);
	CCtrlInfo *c_LZutZugabe = new CCtrlInfo (&m_LZutZugabe, 2, 1, 1, 1);
	ZutGrid.Add (c_LZutZugabe);
	CCtrlInfo *c_ZutZugabe = new CCtrlInfo (&m_ZutZugabe, 3, 1, 1, 1);
	ZutGrid.Add (c_ZutZugabe);
    //Menge
//	CCtrlInfo *c_ChkZutGew = new CCtrlInfo (&m_ChkZutGew, 1, 2, 1, 1);
//	ZutGrid.Add (c_ChkZutGew);
	CCtrlInfo *c_LZutGew = new CCtrlInfo (&m_LZutGew, 2, 2, 1, 1);
	ZutGrid.Add (c_LZutGew);
	CCtrlInfo *c_ZutGew = new CCtrlInfo (&m_ZutGew, 3, 2, 1, 1);
	ZutGrid.Add (c_ZutGew);
    //EK Preis
//	CCtrlInfo *c_ChkZutPreis = new CCtrlInfo (&m_ChkZutPreis, 1, 3, 1, 1);
//	ZutGrid.Add (c_ChkZutPreis);
	CCtrlInfo *c_LZutPreis = new CCtrlInfo (&m_LZutPreis, 2, 3, 1, 1);
	ZutGrid.Add (c_LZutPreis);
	CCtrlInfo *c_ZutPreis = new CCtrlInfo (&m_ZutPreis, 3, 3, 1, 1);
	ZutGrid.Add (c_ZutPreis);
    //Wert
//	CCtrlInfo *c_ChkZutWert = new CCtrlInfo (&m_ChkZutWert, 1, 4, 1, 1);
//	ZutGrid.Add (c_ChkZutWert);
	CCtrlInfo *c_LZutWert = new CCtrlInfo (&m_LZutWert, 2, 4, 1, 1);
	ZutGrid.Add (c_LZutWert);
	CCtrlInfo *c_ZutWert = new CCtrlInfo (&m_ZutWert, 3, 4, 1, 1);
	ZutGrid.Add (c_ZutWert);
    //Wert / 100Kg
//	CCtrlInfo *c_ChkZutWert100kg = new CCtrlInfo (&m_ChkZutWert100kg, 1, 5, 1, 1);
//	ZutGrid.Add (c_ChkZutWert100kg);
	CCtrlInfo *c_LZutWert100kg = new CCtrlInfo (&m_LZutWert100kg, 2, 5, 1, 1);
	ZutGrid.Add (c_LZutWert100kg);
	CCtrlInfo *c_ZutWert100kg = new CCtrlInfo (&m_ZutWert100kg, 3, 5, 1, 1);
	ZutGrid.Add (c_ZutWert100kg);

//=====================================================
//Grid Kosten Daten 
//=====================================================
	 KostGrid.Create (this, 7, 7);
     KostGrid.SetBorder (0, 0);
     KostGrid.SetCellHeight (15);
     KostGrid.SetFontCellHeight (this);
     KostGrid.SetFontCellHeight (this, &Font);
     KostGrid.SetGridSpace (5, 6);
// Rahmen Kosten
	CCtrlInfo *c_KostenGroup = new CCtrlInfo (&m_KostenGroup, 0, 0, 4, 8);
	KostGrid.Add (c_KostenGroup);
	
    //Bezeichnung
	CCtrlInfo *c_LKostBezeichnung = new CCtrlInfo (&m_LKostBezeichnung, 1, 1, 1, 1);
	KostGrid.Add (c_LKostBezeichnung);
	CCtrlInfo *c_KostBezeichnung = new CCtrlInfo (&m_KostBezeichnung, 2, 1, 1, 1);
	KostGrid.Add (c_KostBezeichnung);
    //Aufschlag
	CCtrlInfo *c_LKostAufschlag = new CCtrlInfo (&m_LKostAufschlag, 1, 2, 1, 1);
	KostGrid.Add (c_LKostAufschlag);
	CCtrlInfo *c_KostAufschlag = new CCtrlInfo (&m_KostAufschlag, 2, 2, 1, 1);
	KostGrid.Add (c_KostAufschlag);
    //Kosten Fix
	CCtrlInfo *c_LKostFix = new CCtrlInfo (&m_LKostFix, 1, 3, 1, 1);
	KostGrid.Add (c_LKostFix);
	CCtrlInfo *c_KostFix = new CCtrlInfo (&m_KostFix, 2, 3, 1, 1);
	KostGrid.Add (c_KostFix);
    //Aufschlag Gesamt
	CCtrlInfo *c_LKostAufschlagGes = new CCtrlInfo (&m_LKostWert, 1, 4, 1, 1);
	KostGrid.Add (c_LKostAufschlagGes);
	CCtrlInfo *c_KostAufschlagGes = new CCtrlInfo (&m_KostWert, 2, 4, 1, 1);
	KostGrid.Add (c_KostAufschlagGes);


//=====================================================
//Grid VK-Artikel Daten 
//=====================================================
	 VkGrid.Create (this, 9, 9);
     VkGrid.SetBorder (0, 0);
     VkGrid.SetCellHeight (15);
     VkGrid.SetFontCellHeight (this);
     VkGrid.SetFontCellHeight (this, &Font);
     VkGrid.SetGridSpace (5, 6);
// Rahmen Vk-Artikel
	CCtrlInfo *c_VkartikelGroup = new CCtrlInfo (&m_VkartikelGroup, 0, 0, 4, 8);
	VkGrid.Add (c_VkartikelGroup);
	
    //mat_o_b
	CCtrlInfo *c_LMatob = new CCtrlInfo (&m_LMatob, 1, 1, 1, 1);
	VkGrid.Add (c_LMatob);
	CCtrlInfo *c_Matob = new CCtrlInfo (&m_Matob, 2, 1, 1, 1);
	VkGrid.Add (c_Matob);
	//pr_zutat
	CCtrlInfo *c_LPrZutat = new CCtrlInfo (&m_LPrZutat, 1, 2, 1, 1);
	VkGrid.Add (c_LPrZutat);
	CCtrlInfo *c_PrZutat = new CCtrlInfo (&m_PrZutat, 2, 2, 1, 1);
	VkGrid.Add (c_PrZutat);
	//pr_vpk
	CCtrlInfo *c_LPrVpk = new CCtrlInfo (&m_LPrVpk, 1, 3, 1, 1);
	VkGrid.Add (c_LPrVpk);
	CCtrlInfo *c_PrVpk = new CCtrlInfo (&m_PrVpk, 2, 3, 1, 1);
	VkGrid.Add (c_PrVpk);
	//pr_kosten
	CCtrlInfo *c_LPrKosten = new CCtrlInfo (&m_LPrKosten, 1, 4, 1, 1);
	VkGrid.Add (c_LPrKosten);
	CCtrlInfo *c_PrKosten = new CCtrlInfo (&m_PrKosten, 2, 4, 1, 1);
	VkGrid.Add (c_PrKosten);
	//pr_vk_netto (GH)
	CCtrlInfo *c_LPrGh = new CCtrlInfo (&m_LPrGh, 1, 5, 1, 1);
	VkGrid.Add (c_LPrGh);
	CCtrlInfo *c_PrGh = new CCtrlInfo (&m_PrGh, 2, 5, 1, 1);
	VkGrid.Add (c_PrGh);
	//pr_vk_eh (EH)
	CCtrlInfo *c_LPrEh = new CCtrlInfo (&m_LPrEh, 1, 6, 1, 1);
	VkGrid.Add (c_LPrEh);
	CCtrlInfo *c_PrEh = new CCtrlInfo (&m_PrEh, 2, 6, 1, 1);
	VkGrid.Add (c_PrEh);



//Grid Material hinzuf�gen
	CCtrlInfo *c_MatGrid = new CCtrlInfo (&MatGrid, 1, 4, 3, 3);
	CtrlGrid.Add (c_MatGrid);
//Grid Zutat hinzuf�gen
	CCtrlInfo *c_ZutGrid = new CCtrlInfo (&ZutGrid, 1, 4, 99, 99);
	CtrlGrid.Add (c_ZutGrid);
//Grid Kosten hinzuf�gen
	CCtrlInfo *c_KostGrid = new CCtrlInfo (&KostGrid, 1, 4, 99, 99);
	CtrlGrid.Add (c_KostGrid);
//Grid Vk-Artikel hinzuf�gen
	CCtrlInfo *c_VkGrid = new CCtrlInfo (&VkGrid, 1, 4, 99, 99);
	CtrlGrid.Add (c_VkGrid);

	// KostenDatails
	CCtrlInfo *c_ChkZutBold = new CCtrlInfo (&m_ChkZutBold, 4, 4, 99, 99);
	CtrlGrid.Add (c_ChkZutBold);
	// VK Datails
	CCtrlInfo *c_ChkVkBold = new CCtrlInfo (&m_ChkVkBold, 4, 5, 99, 99);
	CtrlGrid.Add (c_ChkVkBold);
	// 1. Ebene .... 
	CCtrlInfo *c_ChkSingleExpand = new CCtrlInfo (&m_ChkSingleExpand, 4, 6, 99, 99);
	CtrlGrid.Add (c_ChkSingleExpand);

    //Fix-Variabel
	CCtrlInfo *c_Chk_Variabel = new CCtrlInfo (&m_Chk_Variabel, 1, 13, 1, 1);
	CtrlGrid.Add (c_Chk_Variabel);

//GrundPreis
		CCtrlInfo *c_LGrundpreis   = new CCtrlInfo (&m_LGrundpreis, 1, 14, 1, 1); 
		CtrlGrid.Add (c_LGrundpreis);
		CCtrlInfo *c_Grundpreis   = new CCtrlInfo (&m_Grundpreis, 2, 14, 1, 1); 
		CtrlGrid.Add (c_Grundpreis);


//	CCtrlInfo *c_PreisCombo = new CCtrlInfo (&m_PreisCombo, 1, 13, 99, 99);
//	CtrlGrid.Add (c_PreisCombo);

	// Ergebnis 
	 ErgebnisGrid.Create (this, 6, 6);
     ErgebnisGrid.SetBorder (4, 4);
//     ErgebnisGrid.SetCellHeight (15);
//     ErgebnisGrid.SetFontCellHeight (this);
//     ErgebnisGrid.SetFontCellHeight (this, &FontBig);
     ErgebnisGrid.SetGridSpace (5, 9);
// Rahmen Ergenis
	CCtrlInfo *c_ErgebnisGroup = new CCtrlInfo (&m_ErgebnisGroup, 0, 0, 6, 8);
	ErgebnisGrid.Add (c_ErgebnisGroup);

	CCtrlInfo *c_LGesWert = new CCtrlInfo (&m_LGesWert, 1, 1, 1, 1);
	ErgebnisGrid.Add (c_LGesWert);
	CCtrlInfo *c_GesWert = new CCtrlInfo (&m_GesWert, 2, 1, 1, 1);
	ErgebnisGrid.Add (c_GesWert);
	CCtrlInfo *c_LGesEuro = new CCtrlInfo (&m_LGesEuro, 3, 1, 1, 1);
	ErgebnisGrid.Add (c_LGesEuro);
	CCtrlInfo *c_LDeckungA = new CCtrlInfo (&m_LDeckungA, 1, 2, 1, 1);
	ErgebnisGrid.Add (c_LDeckungA);
	CCtrlInfo *c_DeckungA = new CCtrlInfo (&m_DeckungA, 2, 2, 1, 1);
	ErgebnisGrid.Add (c_DeckungA);
	CCtrlInfo *c_LDeckungEuro = new CCtrlInfo (&m_LDeckungEuro, 3, 2, 1, 1);
	ErgebnisGrid.Add (c_LDeckungEuro);
	CCtrlInfo *c_LDeckungP = new CCtrlInfo (&m_LDeckungP, 1, 3, 1, 1);
	ErgebnisGrid.Add (c_LDeckungP);
	CCtrlInfo *c_DeckungP = new CCtrlInfo (&m_DeckungP, 2, 3, 1, 1);
	ErgebnisGrid.Add (c_DeckungP);
	CCtrlInfo *c_LDeckungProz = new CCtrlInfo (&m_LDeckungProz, 3, 3, 1, 1);
	ErgebnisGrid.Add (c_LDeckungProz);

//Grid Ergebnis hinzuf�gen
	CCtrlInfo *c_EnterErgebnis = new CCtrlInfo (&m_EnterErgebnis, 4, 9, 2, 1);
	CtrlGrid.Add (c_EnterErgebnis);
	CCtrlInfo *c_ErgebnisGrid = new CCtrlInfo (&ErgebnisGrid, 4, 9, 99, 99);
	CtrlGrid.Add (c_ErgebnisGrid);



	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);
	m_EnterErgebnis.SetFont (&FontBig);

//	071009 rausgenommen ,f�hrete zu einem Fehler !!!??? m_Save.Tooltip.SetFont (&Font); 
	m_Save.Tooltip.SetSize ();

	CtrlGrid.Display ();

//erste zerm_mat nach der Wurzel suchen 
    Zerm->dbreadfirst_AusMat();
//Bezeichnung von zerm_mat holen 
	A_mat->a_mat.mat = Zerm->zerm.zerm_mat;
	if (A_mat->dbreadfirst() == 0)	A_bas->a_bas.a = A_mat->a_mat.a;
	if (A_bas->dbreadfirst()== 0) 
	{
		_tcscpy_s (Zerm->zerm.kost_bz, A_bas->a_bas.a_bz1);
		_tcscpy_s (Zerm->zerm.zerm_mat_bz1, A_bas->a_bas.a_bz1);
		_tcscpy_s (Zerm->zerm.zerm_mat_bz2, A_bas->a_bas.a_bz2);
	}


	FillMatCombo ();
	FillPreisCombo ();

	FormShow ();
    m_Mat.SetFocus ();


	for (std::vector<CUpdateEvent *>::iterator e =  UpdateTab.begin (); e != UpdateTab.end (); ++e)
	{
		CUpdateEvent *event = *e;
		event->Update (-1); // < 0 : kein Update bei ZerlList n�tig
	}


	return FALSE;
}

HBRUSH CZerl::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			m_Save.SetBkColor(DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}

	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return CDialog::OnCtlColor (pDC, pWnd,nCtlColor);
}
void CZerl::OnSize (UINT nType, int cx, int cy)
{
		CRect rect (0, 0, cx, cy);
		CtrlGrid.pcx = 0;
		CtrlGrid.pcy = 0;
		CtrlGrid.DlgSize = &rect;
		CtrlGrid.Move (0, 0);
		CtrlGrid.DlgSize = NULL;
}

BOOL CZerl::PreTranslateMessage(MSG* pMsg)
{
	CWnd *Control;

	Control = GetFocus ();
	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
                if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F3)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
					//Page1->NextRec ();
				}
				else
				{
					//Page1->LastRec ();
				}
				return TRUE;

			}

			else if (pMsg->wParam == VK_F2)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
					//Page1->PriorRec ();
				}
				else
				{
					//Page1->FirstRec ();
				}
				return TRUE;

			}

			else if (pMsg->wParam == VK_DOWN)
			{
				Control = GetNextDlgTabItem (Control, FALSE);
				if (Control != NULL)
				{
					Control->SetFocus ();
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_NEXT)
			{
				TreeDown();
				if (Control != NULL)
				{
					Control->SetFocus ();
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_PRIOR)
			{
				TreeUp();
				if (Control != NULL)
				{
					Control->SetFocus ();
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				Control = GetNextDlgTabItem (Control, TRUE);
				if (Control != NULL)
				{
					Control->SetFocus ();
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F5)
			{
				Zerm->ZerlTreeErlauben = Zerm->TYP_MATERIAL; 
				PageUpdate->Back (); 
				return TRUE;

			}
			else if (pMsg->wParam == VK_F7)
			{
// so l�ufts nicht, weil ich den Focus nicht kriege	Matree.Delete();
				PageUpdate->Write ();  //testtest soll wieder raus!!!
				DeleteRow ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Zerm->ZerlTreeErlauben = Zerm->TYP_MATERIAL; 
				return PageUpdate->Write (); 
//				Write ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
//ZuTun				if (Basis->Choice != NULL)
//				{
//					if (Basis->Choice->IsWindowVisible ())
//					{
//						Basis->Choice->ShowWindow (SW_HIDE);
//					}
//					else
//					{
//						Basis->Choice->ShowWindow (SW_SHOWNORMAL);
//					}
//				}
			}
			else if (pMsg->wParam == VK_F9)
			{
				if (GetFocus () == &m_Mat)
				{
					OnMatchoice ();
					return TRUE;
				}
				return TRUE;
			}
	}
	return FALSE;
}
void CZerl::Read ()
{

	Zerm->zerm.zerm_aus_mat = Schnitt->schnitt.schnitt_mat;
	Zerm->zerm.zerm_mat = Schnitt->schnitt.schnitt_mat;
	Zerm->dbreadfirst ();
	_tcscpy_s (Zerm->zerm.variabel, "N");
	_tcscpy_s (Zerm->zerm.fix, "F");

	Zerm->zerm.chk_gew = FALSE;
	if (m_ChkGew.GetCheck () == BST_CHECKED)
	{
		Zerm->zerm.chk_gew = TRUE;
	}
	Zerm->zerm.chk_gew_ant = FALSE;
	if (m_ChkGewAnt.GetCheck () == BST_CHECKED)
	{
		Zerm->zerm.chk_gew_ant = TRUE;
	}
	Zerm->zerm.chk_mat_o_b = FALSE;
	if (m_ChkMatoB.GetCheck () == BST_CHECKED)
	{
		Zerm->zerm.chk_mat_o_b = TRUE;
	}
	Zerm->zerm.chk_wert = FALSE;
	if (m_ChkWert.GetCheck () == BST_CHECKED)
	{
		Zerm->zerm.chk_wert = TRUE;
	}
	Zerm->zerm.chk_wert_100kg = FALSE;
	if (m_ChkWert100Kg.GetCheck () == BST_CHECKED)
	{
		Zerm->zerm.chk_wert_100kg = TRUE;
	}
	Zerm->zerm.chk_kennzeichen = FALSE;
	if (m_ChkKennzeichen.GetCheck () == BST_CHECKED)
	{
		Zerm->zerm.chk_kennzeichen = TRUE;
	}
	Zerm->zerm.chk_zeitfaktor = FALSE;
	if (m_ChkZeitfaktor.GetCheck () == BST_CHECKED)
	{
		Zerm->zerm.chk_zeitfaktor = TRUE;
	}
	Zerm->zerm.chk_fwp = FALSE;
	if (m_ChkFwp.GetCheck () == BST_CHECKED)
	{
		Zerm->zerm.chk_fwp = TRUE;
	}

	Update(0);
}
void CZerl::Update (short flgSave)
{
	CString CFix;

//	Zerm->dbreadfirst (); hier nicht �ber die DB, sondern ZerlTreeDataToZerm !!!
	if (flgSave == 105)  //Zerm.variabel im Tree ge�ndert
	{
		CFix.Format (_T("%s"),Zerm->zerm.variabel);
		if (CFix == "J") CFix = "V";
		if (CFix == "N") CFix = "F";
		_tcscpy_s (Zerm->zerm.fix, CFix.GetBuffer());
		Umlasten (20);  //Hier wird auch wieder update-event aufgerufen 
	}
	else
	{
		CFix.Format (_T("%s"),Zerm->zerm.fix);
		if (CFix == "V") CFix = "J";
		if (CFix == "F") CFix = "N";
		_tcscpy_s (Zerm->zerm.variabel, CFix.GetBuffer());
	}

	if (_tstoi(Zerm->zerm.zerm_teil) == 1)
	{
//		m_ButtonZerlegen.EnableWindow (FALSE);
		if (Zerm->zerm.zerm_aus_mat == Zerm->zerm.zerm_mat && Zerm->zerm.a_typ == Zerm->TYP_MATERIAL) //nur f�r den Einstieg bei neuer Schnittf�hrung! 
		{
			m_ButtonZerlegen.ShowWindow(SW_NORMAL);
		}
		else
		{
			m_ButtonZerlegen.ShowWindow(SW_HIDE);
		}
	}
	else
	{
		CString CStr;
		CStr = Zerm->zerm.a_kz;
		if (CStr == "GT" || Schnitt->cfg_NurGTZerlegen == 0)
		{
			if (CStr == "GT" 
				|| CStr == "VT"
				|| CStr == "VM")
			{
				m_ButtonZerlegen.ShowWindow(SW_SHOWNORMAL);
			}
			else
			{
				m_ButtonZerlegen.ShowWindow(SW_HIDE);
			}
		}
		else
		{
//			m_ButtonZerlegen.EnableWindow (FALSE);
			m_ButtonZerlegen.ShowWindow(SW_HIDE);
		}
	}


	A_mat->a_mat.mat = Zerm->zerm.zerm_aus_mat;
	A_mat->dbreadfirst ();
	A_bas->a_bas.a = A_mat->a_mat.a;
	A_bas->dbreadfirst ();
	Zerm->zerm.zerm_aus_mat_bz; 
	_tcscpy_s (Zerm->zerm.zerm_aus_mat_bz, A_bas->a_bas.a_bz1);
	Work->ReadMaterial(Zerm->zerm.mdn,Zerm->zerm.zerm_mat,Zerm->zerm.a_typ,Schnitt->cfg_PreiseAus_a_kalkpreis);
	_tcscpy_s (Zerm->zerm.kost_bz, Work->ABas.a_bas.a_bz1);
	_tcscpy_s (Zerm->zerm.zerm_mat_bz1, Work->ABas.a_bas.a_bz1);

	DisableFelder();

	FormShow ();
}
BOOL CZerl::OnReturn ()
{
	CWnd * Control = GetFocus ();
	if (Control == &m_Grundpreis)
	{
		FormGet();
			Schnitt->schnitt._DeckungA = Schnitt->schnitt._SumPreis - (Schnitt->schnitt.ek_pr * Schnitt->schnitt.ek_gew);
			Schnitt->schnitt._DeckungP = Schnitt->schnitt._DeckungA * 100 / (Schnitt->schnitt.ek_pr * Schnitt->schnitt.ek_gew);
			Schnitt->schnitt.deckung_proz = Schnitt->schnitt._DeckungP;
		FormShow();

			m_Grundpreis.SetFocus ();
			m_Grundpreis.SetSel (0, -1);

			return TRUE;
	}
	if (Control == &m_Mat)
	{
		Zerm->ZerlTreeErlauben = Zerm->TYP_MATERIAL; 
		if (Zerm->zerm.a_typ == Zerm->TYP_MATERIAL) Zerm->ZerlTreeErlauben = Zerm->TYP_MATERIAL; 
		if (Zerm->zerm.a_typ == Zerm->TYP_ZUTAT) Zerm->ZerlTreeErlauben = Zerm->TYP_ZUTAT; 
		if (Zerm->zerm.a_typ == Zerm->TYP_VERPACKUNG) Zerm->ZerlTreeErlauben = Zerm->TYP_ZUTAT; 
		if (Zerm->zerm.a_typ == Zerm->TYP_KOSTEN) Zerm->ZerlTreeErlauben = Zerm->TYP_KOSTEN; 
		if (Zerm->zerm.a_typ == Zerm->TYP_AUFSCHLAG) Zerm->ZerlTreeErlauben = Zerm->TYP_KOSTEN; 
		if (Zerm->zerm.a_typ == Zerm->TYP_AUFSCHLAG_EH) Zerm->ZerlTreeErlauben = Zerm->TYP_KOSTEN; 
		if (Zerm->zerm.a_typ == Zerm->TYP_VKARTIKEL) Zerm->ZerlTreeErlauben = Zerm->TYP_VKARTIKEL; 
		FormGet ();
		if (!TestMat ()) return FALSE;        
		DisableFelder ();

		if (OldMat != Zerm->zerm.zerm_mat || OldA_typ != Zerm->zerm.a_typ)
		{
			flgMatGeaendert = TRUE;
			OldMat =  Zerm->zerm.zerm_mat;
			OldA_typ =  Zerm->zerm.a_typ;
//			UpdateList ();
		}
		flgMatGeaendert = TRUE; // TESTTEST
		CString cMat;
		m_Mat.GetWindowText (cMat);

		if (ReadMaterial(_tstol(cMat.GetBuffer())) == TRUE)
		{
			UpdateList ();
		}

	}

	if (Control == &m_Gew)
	{
		Zerm->PAGE2_WRITE = TRUE;
		Zerm->ZerlTreeErlauben = Zerm->TYP_MATERIAL; 
		double gew = Zerm->zerm.zerm_gew;
		FormGet();
		if (!CStrFuncs::IsDoubleEqual (gew, Zerm->zerm.zerm_gew,2))
		{
			if (!CStrFuncs::IsDoubleEqual (Schnitt->schnitt.ek_gew, (double) 0,2))
			{
				Zerm->zerm.zerm_gew_ant = 100 * Zerm->zerm.zerm_gew / Schnitt->schnitt.ek_gew;
				Zerm->zerm.dm_100 = Zerm->zerm.mat_o_b * Zerm->zerm.zerm_gew_ant;
				Zerm->zerm.teilkosten_abs = Zerm->zerm.mat_o_b * Zerm->zerm.zerm_gew;
				Zerm->zerm.kosten_zutat = Zerm->zerm.pr_zutat * Zerm->zerm.zerm_gew;
				//Zerm->zerm.kosten_vpk  bleibt fix
				Zerm->zerm.kosten = Zerm->zerm.pr_kosten * Zerm->zerm.zerm_gew 
					+ Zerm->zerm.kosten_fix;
				Zerm->zerm.vk_pr = Zerm->zerm.mat_o_b * Zerm->zerm.zerm_gew 
					+ Zerm->zerm.pr_zutat * Zerm->zerm.zerm_gew 
					+ Zerm->zerm.pr_vpk  ;
				FormShow();
			}
			UpdateList();
			Umlasten (20);
		}
	}
	if (Control == &m_GewAnt)
	{
		Zerm->PAGE2_WRITE = TRUE;
		Zerm->ZerlTreeErlauben = Zerm->TYP_MATERIAL; 
		double gew_ant = Zerm->zerm.zerm_gew_ant;
		FormGet();
		if (!CStrFuncs::IsDoubleEqual (gew_ant, Zerm->zerm.zerm_gew_ant,2))
		{
			Zerm->zerm.zerm_gew  = Zerm->zerm.zerm_gew_ant * Schnitt->schnitt.ek_gew / 100;
			Zerm->zerm.dm_100 = Zerm->zerm.mat_o_b * Zerm->zerm.zerm_gew_ant;
				Zerm->zerm.teilkosten_abs = Zerm->zerm.mat_o_b * Zerm->zerm.zerm_gew;
				Zerm->zerm.kosten_zutat = Zerm->zerm.pr_zutat * Zerm->zerm.zerm_gew;
				//Zerm->zerm.kosten_vpk  bleibt fix
				Zerm->zerm.kosten = Zerm->zerm.pr_kosten * Zerm->zerm.zerm_gew 
					+ Zerm->zerm.kosten_fix;
				Zerm->zerm.vk_pr = Zerm->zerm.mat_o_b * Zerm->zerm.zerm_gew 
					+ Zerm->zerm.pr_zutat * Zerm->zerm.zerm_gew 
					+ Zerm->zerm.pr_vpk  ;
			FormShow();
			UpdateList();
			Umlasten (20);
		}
		if (!m_Fwp.IsWindowEnabled ())
		{
			m_Mat.SetFocus ();
			return TRUE;
		}
	}
	if (Control == &m_Fwp)
	{
		Zerm->PAGE2_WRITE = TRUE;
		Zerm->ZerlTreeErlauben = Zerm->TYP_MATERIAL; 
		double fwp = Zerm->zerm.fwp;
		FormGet();
		if (!CStrFuncs::IsDoubleEqual (fwp, Zerm->zerm.fwp,2))
		{
			Zerm->zerm.mat_o_b =  Zerm->zerm.fwp * Schnitt->schnitt._wert_fwp; 
			Zerm->zerm.hk_vollk = Schnitt->schnitt._grund_kosten;
			Zerm->zerm.dm_100 = Zerm->zerm.mat_o_b * Zerm->zerm.zerm_gew_ant;
				Zerm->zerm.teilkosten_abs = Zerm->zerm.mat_o_b * Zerm->zerm.zerm_gew;
				Zerm->zerm.kosten_zutat = Zerm->zerm.pr_zutat * Zerm->zerm.zerm_gew;
				//Zerm->zerm.kosten_vpk  bleibt fix
				Zerm->zerm.kosten = Zerm->zerm.pr_kosten * Zerm->zerm.zerm_gew 
					+ Zerm->zerm.kosten_fix;
				Zerm->zerm.vk_pr = Zerm->zerm.mat_o_b * Zerm->zerm.zerm_gew 
					+ Zerm->zerm.pr_zutat * Zerm->zerm.zerm_gew 
					+ Zerm->zerm.pr_vpk  ;
				Zerm->fwp_neu = TRUE;
			FormShow();
			UpdateList();
			Umlasten (20);
		}
	}
	if (Control == &m_MatoB)
	{
		Zerm->PAGE2_WRITE = TRUE;
		Zerm->ZerlTreeErlauben = Zerm->TYP_MATERIAL; 
		double mat_o_b = Zerm->zerm.mat_o_b;
		FormGet();
		if (!CStrFuncs::IsDoubleEqual (mat_o_b, Zerm->zerm.mat_o_b,2))
		{
			if (!CStrFuncs::IsDoubleEqual (Schnitt->schnitt._wert_fwp, (double) 0,2))
			{
				Zerm->zerm.fwp =  Zerm->zerm.mat_o_b / Schnitt->schnitt._wert_fwp; 
				Zerm->fwp_neu = TRUE;
				Zerm->zerm.dm_100 = Zerm->zerm.mat_o_b * Zerm->zerm.zerm_gew_ant;
				Zerm->zerm.teilkosten_abs = Zerm->zerm.mat_o_b * Zerm->zerm.zerm_gew;
				Zerm->zerm.kosten_zutat = Zerm->zerm.pr_zutat * Zerm->zerm.zerm_gew;
				//Zerm->zerm.kosten_vpk  bleibt fix
				Zerm->zerm.kosten = Zerm->zerm.pr_kosten * Zerm->zerm.zerm_gew 
					+ Zerm->zerm.kosten_fix;
				Zerm->zerm.vk_pr = Zerm->zerm.mat_o_b * Zerm->zerm.zerm_gew 
					+ Zerm->zerm.pr_zutat * Zerm->zerm.zerm_gew 
					+ Zerm->zerm.pr_vpk  ;
				FormShow();
			}
			UpdateList();
			Umlasten (20);
		}		
		if (Schnitt->schnitt._PreisArt == 2)
		{
			m_Zeitfaktor.SetFocus ();
			return TRUE;
		}
		if (Schnitt->schnitt._PreisArt > 2)
		{
			m_Mat.SetFocus ();
			return TRUE;
		}

/**** funkt. nicht arum ??????
		if (!m_Wert100Kg.IsWindowEnabled ())
		{
			if (!m_Zeitfaktor.IsWindowEnabled ())
			{
				m_Mat.SetFocus ();
				return TRUE;
			}
			else
			{
				m_Zeitfaktor.SetFocus ();
				return TRUE;
			}
		}
**********/
	}
	if (Control == &m_Wert100Kg)
	{
		Zerm->PAGE2_WRITE = TRUE;
		Zerm->ZerlTreeErlauben = Zerm->TYP_MATERIAL; 
		double dm_100 = Zerm->zerm.dm_100;
		FormGet();
		if (!CStrFuncs::IsDoubleEqual (dm_100, Zerm->zerm.dm_100,2))
		{
			if (!CStrFuncs::IsDoubleEqual (Schnitt->schnitt._wert_fwp, (double) 0,2))
			{
				if (!CStrFuncs::IsDoubleEqual (Zerm->zerm.zerm_gew_ant, (double) 0,2))
				{
					Zerm->zerm.mat_o_b  = Zerm->zerm.dm_100 / Zerm->zerm.zerm_gew_ant;
					Zerm->zerm.fwp =  Zerm->zerm.mat_o_b / Schnitt->schnitt._wert_fwp; 
					Zerm->fwp_neu = TRUE;
					Zerm->zerm.teilkosten_abs = Zerm->zerm.mat_o_b * Zerm->zerm.zerm_gew;
					Zerm->zerm.kosten_zutat = Zerm->zerm.pr_zutat * Zerm->zerm.zerm_gew;
					//Zerm->zerm.kosten_vpk  bleibt fix
					Zerm->zerm.kosten = Zerm->zerm.pr_kosten * Zerm->zerm.zerm_gew 
						+ Zerm->zerm.kosten_fix;
					Zerm->zerm.vk_pr = Zerm->zerm.mat_o_b * Zerm->zerm.zerm_gew 
						+ Zerm->zerm.pr_zutat * Zerm->zerm.zerm_gew 
						+ Zerm->zerm.pr_vpk  ;
					FormShow();
				}
			}
			UpdateList();
			Umlasten (20);
		}
		if (Schnitt->schnitt._PreisArt == 1)
		{
			m_Mat.SetFocus ();
			return TRUE;
		}


/*** funkt. nicht ... warum ?????
		if (!m_Zeitfaktor.IsWindowEnabled ())
		{
			m_Mat.SetFocus ();
			return TRUE;
		}
****************/
	}
	if (Control == &m_Zeitfaktor)
	{
		Zerm->PAGE2_WRITE = TRUE;
		Zerm->ZerlTreeErlauben = Zerm->TYP_MATERIAL; 
		FormGet();
		UpdateList();
//		Umlasten (20);
		m_Mat.SetFocus ();
		return TRUE;
	}
	if (Control == &m_ZutZugabe)
	{
		Zerm->PAGE2_WRITE = TRUE;
		double gew = Zerm->zerm.zerm_gew_ant;
		Zerm->ZerlTreeErlauben = Zerm->TYP_ZUTAT; 
		FormGet ();
		if (!CStrFuncs::IsDoubleEqual (gew, Zerm->zerm.zerm_gew_ant,2))
		{
			Umlasten (20); // Zerm.gew wird neu berechnet aus dem Gewicht des Ausgangsmat. (in ZermToZerlTreeData)
			Zerm->zerm.teilkosten_abs = Zerm->zerm.mat_o_b * Zerm->zerm.zerm_gew;
			Umlasten (20); 
			FormShow();
		}
	}
	if (Control == &m_ZutGew)
	{
		Zerm->PAGE2_WRITE = TRUE;
		double gew = Zerm->zerm.zerm_gew;
		double gew_ant = Zerm->zerm.zerm_gew_ant;
		Zerm->ZerlTreeErlauben = Zerm->TYP_ZUTAT; 
		FormGet ();
		double gew_neu = Zerm->zerm.zerm_gew;
		if (!CStrFuncs::IsDoubleEqual (gew, gew_neu,2))
		{
			if (Zerm->zerm.a_typ == Zerm->TYP_ZUTAT)
			{
				if (CStrFuncs::IsDoubleEqual (Zerm->zerm.zerm_gew_ant, 0.0,3))
				{
					Zerm->zerm.zerm_gew_ant = 1.0;   //mit 0 l�sst sich hier nicht rechnen, deshalb der Umweg
					Umlasten(20);
					gew = Zerm->zerm.zerm_gew;
				}
				Zerm->zerm.zerm_gew = gew;
				Umlasten (20); // Zerm.gew wird neu berechnet aus dem Gewicht des Ausgangsmat. (in ZermToZerlTreeData)
				Zerm->zerm.zerm_gew = gew_neu;
				double PGew = 0.0;
//				if (!CStrFuncs::IsDoubleEqual (Zerm->zerm.zerm_gew_ant, 0.0)) PGew = gew * 1000 /Zerm->zerm.zerm_gew_ant; // Gewicht des Ausgangsmat. zur�ckrechnen
				if (!CStrFuncs::IsDoubleEqual (Zerm->zerm.zerm_gew_ant, 0.0,3)) PGew = gew * 1000 /gew_ant; // Gewicht des Ausgangsmat. zur�ckrechnen
				Zerm->zerm.zerm_gew_ant = 0.0;
				if (!CStrFuncs::IsDoubleEqual (PGew, 0.0,3)) Zerm->zerm.zerm_gew_ant = Zerm->zerm.zerm_gew / PGew * 1000;
				Zerm->zerm.teilkosten_abs = Zerm->zerm.mat_o_b * Zerm->zerm.zerm_gew;
				Umlasten (20); 
				int di = 0;
			}
			if (Zerm->zerm.a_typ == Zerm->TYP_VERPACKUNG)
			{
				Zerm->zerm.teilkosten_abs = Zerm->zerm.mat_o_b * Zerm->zerm.zerm_gew;
				Umlasten (20); 
			}
			FormShow();
		}
		m_Mat.SetFocus ();
		return TRUE;
	}
	if (Control == &m_ZutPreis)
	{
		Zerm->PAGE2_WRITE = TRUE;
		double mat_o_b = Zerm->zerm.mat_o_b;
		Zerm->ZerlTreeErlauben = Zerm->TYP_ZUTAT; 
		FormGet ();
		if (!CStrFuncs::IsDoubleEqual (mat_o_b, Zerm->zerm.mat_o_b,2))
		{
			Umlasten (20); // Zerm.gew wird neu berechnet aus dem Gewicht des Ausgangsmat. (in ZermToZerlTreeData)
			Zerm->zerm.teilkosten_abs = Zerm->zerm.mat_o_b * Zerm->zerm.zerm_gew;
			Umlasten (20); 
			FormShow();
		}
	}
	if (Control == &m_ZutWert100kg)
	{
		Zerm->PAGE2_WRITE = TRUE;
		double dm_100 = Zerm->zerm.dm_100;
		Zerm->ZerlTreeErlauben = Zerm->TYP_ZUTAT; 
		FormGet ();
		if (!CStrFuncs::IsDoubleEqual (dm_100, Zerm->zerm.dm_100,2))
		{
			FormShow();
			UpdateList();
			Umlasten (20);
		}
	}
	if (Control == &m_KostBezeichnung)
	{
		Zerm->PAGE2_WRITE = TRUE;
		Zerm->ZerlTreeErlauben = Zerm->TYP_KOSTEN; 
		FormGet ();
		FormShow();
//		UpdateList();
		Umlasten (20);
	}
	if (Control == &m_KostAufschlag)
	{
		Zerm->PAGE2_WRITE = TRUE;
		Zerm->ZerlTreeErlauben = Zerm->TYP_KOSTEN; 
		double mat_o_b = Zerm->zerm.mat_o_b;
		FormGet ();
		KostArtSpeichern();
		if (!CStrFuncs::IsDoubleEqual (mat_o_b, Zerm->zerm.mat_o_b,2))
		{
			if (Zerm->zerm.a_typ == Zerm->TYP_KOSTEN)
			{
				Umlasten (20); // Zerm.gew wird neu berechnet aus dem Gewicht des Ausgangsmat. (in ZermToZerlTreeData)
				Zerm->zerm.teilkosten_abs = kost_art_zerl.kost_art_zerl.vkost_wt * Zerm->zerm.zerm_gew;
				Zerm->zerm.teilkosten_abs += kost_art_zerl.kost_art_zerl.vkost_fix; //FixKosten
				Umlasten (20); 
			}
			if (Zerm->zerm.a_typ == Zerm->TYP_AUFSCHLAG)
			{
				Zerm->zerm.vollkosten_abs = (double) 0;
				Umlasten (20); // Zerm.teilkosten_abs wird neu berechnet aus dem hk_vollk des Ausgangsmat. (in ZermToZerlTreeData)
//				Zerm->zerm.teilkosten_abs = Zerm->zerm.mat_o_b * Zerm->zerm.zerm_gew;
				Umlasten (20); 
			}
			if (Zerm->zerm.a_typ == Zerm->TYP_AUFSCHLAG_EH)
			{
				Zerm->zerm.vollkosten_abs = (double) 0;
				Umlasten (20); // Zerm.teilkosten_abs wird neu berechnet aus dem hk_vollk des Ausgangsmat. (in ZermToZerlTreeData)
				Umlasten (20); 
				FormShow();
			}
			FormShow();
		}
		if (Zerm->zerm.a_typ == Zerm->TYP_AUFSCHLAG_EH || Zerm->zerm.a_typ == Zerm->TYP_AUFSCHLAG)
		{
			m_Mat.SetFocus ();
			return TRUE;
		}
	}
	if (Control == &m_KostFix)
	{
		Zerm->PAGE2_WRITE = TRUE;
		Zerm->ZerlTreeErlauben = Zerm->TYP_KOSTEN; 
		double vollkosten_abs = Zerm->zerm.vollkosten_abs;
		FormGet ();
		KostArtSpeichern();
		if (!CStrFuncs::IsDoubleEqual (vollkosten_abs, Zerm->zerm.vollkosten_abs,2))
		{
			if (Zerm->zerm.a_typ == Zerm->TYP_KOSTEN)
			{

				vollkosten_abs = Zerm->zerm.vollkosten_abs;
				Umlasten (20); // Zerm.gew wird neu berechnet aus dem Gewicht des Ausgangsmat. (in ZermToZerlTreeData)
				Zerm->zerm.teilkosten_abs = Zerm->zerm.mat_o_b * Zerm->zerm.zerm_gew;
				Zerm->zerm.teilkosten_abs += vollkosten_abs; //FixKosten
				Umlasten (20); 
			}
			FormShow();
		}
		m_Mat.SetFocus ();
		return TRUE;
	}
	if (Control == &m_KostWert)
	{
		Zerm->PAGE2_WRITE = TRUE;
		Zerm->ZerlTreeErlauben = Zerm->TYP_KOSTEN; 
		FormGet ();
		FormShow();
		UpdateList();
		Umlasten (20);
	}

//============================= Ergebnis-Block ===================================
	if (Control == &m_GesWert)
	{
		Zerm->PAGE2_WRITE = TRUE;
		double ges_wert = Schnitt->schnitt._SumPreis;
		FormGet();
		if (!CStrFuncs::IsDoubleEqual (ges_wert, Schnitt->schnitt._SumPreis,2))
		{
			Schnitt->schnitt._DeckungA = Schnitt->schnitt._SumPreis - (Schnitt->schnitt.ek_pr * Schnitt->schnitt.ek_gew);
			Schnitt->schnitt._DeckungP = Schnitt->schnitt._DeckungA * 100 / (Schnitt->schnitt.ek_pr * Schnitt->schnitt.ek_gew);
			Schnitt->schnitt.deckung_proz = Schnitt->schnitt._DeckungP;


			FormShow();
			UpdateList();
			Umlasten (20);
		}
	}
	if (Control == &m_DeckungA)
	{
		Zerm->PAGE2_WRITE = TRUE;
		double deckung_a = Schnitt->schnitt._DeckungA;
		FormGet();
		if (!CStrFuncs::IsDoubleEqual (deckung_a, Schnitt->schnitt._DeckungA,2))
		{
			Schnitt->schnitt._SumPreis  = (Schnitt->schnitt.ek_pr * Schnitt->schnitt.ek_gew) + Schnitt->schnitt._DeckungA;
			Schnitt->schnitt._DeckungP = Schnitt->schnitt._DeckungA * 100 / (Schnitt->schnitt.ek_pr * Schnitt->schnitt.ek_gew);
			Schnitt->schnitt.deckung_proz = Schnitt->schnitt._DeckungP;

			FormShow();
			UpdateList();
			Umlasten (20);
		}
	}
	if (Control == &m_DeckungP)
	{
		Zerm->PAGE2_WRITE = TRUE;
		double deckung_p = Schnitt->schnitt._DeckungP;
		FormGet();
		if (!CStrFuncs::IsDoubleEqual (deckung_p, Schnitt->schnitt._DeckungP,2))
		{
			Schnitt->schnitt._DeckungA = Schnitt->schnitt._DeckungP * (Schnitt->schnitt.ek_pr * Schnitt->schnitt.ek_gew) / 100;
			Schnitt->schnitt._SumPreis  = (Schnitt->schnitt.ek_pr * Schnitt->schnitt.ek_gew) + Schnitt->schnitt._DeckungA;
			Schnitt->schnitt.deckung_proz = Schnitt->schnitt._DeckungP;

			FormShow();
			UpdateList();
			Umlasten (20);
		}
		if (m_GesWert.IsWindowEnabled ())
		{
			m_GesWert.SetFocus ();
			return TRUE;
		}
	}





	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
		double dzerm_gew = Zerm->zerm.zerm_gew;
		double dzerm_gew_ant = Zerm->zerm.zerm_gew_ant;
		Control->SetFocus ();   // neg. Werte werden hier pl�tzlich positiv !!!!???? daher diese Umschaufelei
		Zerm->zerm.zerm_gew = dzerm_gew;
		Zerm->zerm.zerm_gew_ant = dzerm_gew_ant;
		return TRUE;
	}
	return TRUE;
}
BOOL CZerl::TestMat ()
{
	CString Msg;
	FormGet ();
	if (Zerm->zerm.zerm_mat == 0) return TRUE; 
	int dstatus = Work->ReadMaterial(Zerm->zerm.mdn,Zerm->zerm.zerm_mat,Zerm->zerm.a_typ,Schnitt->cfg_PreiseAus_a_kalkpreis);
	if (dstatus == 0) 
	{
		_tcscpy_s (Zerm->zerm.a_kz, Work->AVarb.a_varb.a_kz);
		if (Zerm->zerm.a_typ  == Zerm->TYP_ZUTAT) _tcscpy_s (Zerm->zerm.a_kz, "ZT");
		if (Zerm->zerm.a_typ != Zerm->TYP_MATERIAL && Zerm->zerm.a_typ != Zerm->TYP_VKARTIKEL)
		{
//			_tcscpy_s (Zerm->zerm.zerm_teil, _T("2"));
			_tcscpy_s (Zerm->zerm.fix, _T("F"));
		}
		return TRUE;
	}

    if (dstatus == 100)
	{
		if (Zerm->zerm.a_typ == Zerm->TYP_MATERIAL || Zerm->zerm.a_typ == Zerm->TYP_VKARTIKEL)
		{
			MessageBox (Msg.GetBuffer ());
			return FALSE;
		}
		else
		{
			Msg.Format (_T("Kostenart %d ist nicht vorhanden, neu anlegen ?"),
		        Zerm->zerm.zerm_mat);
			int ret = MessageBox (Msg.GetBuffer(), NULL, MB_YESNO | MB_ICONQUESTION);
			if  (ret != IDYES) return FALSE;
			if (Zerm->zerm.a_typ  == Zerm->TYP_KOSTEN) _tcscpy_s (Zerm->zerm.a_kz, "KO");
			if (Zerm->zerm.a_typ  == Zerm->TYP_AUFSCHLAG) _tcscpy_s (Zerm->zerm.a_kz, "AU");
			if (Zerm->zerm.a_typ  == Zerm->TYP_AUFSCHLAG_EH) _tcscpy_s (Zerm->zerm.a_kz, "AU");
		}
		return TRUE;
	}
	if (dstatus == 50 && Zerm->zerm.a_typ == Zerm->TYP_MATERIAL)
	{
		Msg.Format (_T("Materialnummer %d ist keine Verarbeitungsmaterial (falscher Artikeltyp)"),
		        Zerm->zerm.zerm_mat);
		MessageBox (Msg.GetBuffer ());
		return FALSE;
	}
	if (dstatus == 50 && Zerm->zerm.a_typ == Zerm->TYP_ZUTAT)
	{
		Msg.Format (_T("Materialnummer %d ist keine Zutat oder Verarb.Material (falscher Artikeltyp)"),
		        Zerm->zerm.zerm_mat);
		MessageBox (Msg.GetBuffer ());
		return FALSE;
	}
	Msg.Format (_T("Kein Zugriff auf Materialnummer %d (Datenbankproblem!)"),
	        Zerm->zerm.zerm_mat);
	MessageBox (Msg.GetBuffer ());
	return FALSE;
}
void CZerl::UpdateList ()
{
	short flgSave = 0;
	FormGet ();
	if (Zerm->zerm.zerm_mat == 0)
	{
		return;
	}
	if (flgMatGeaendert == TRUE)
	{
		flgSave = 1;
		flgMatGeaendert = FALSE;
	}
	FormGet ();
	UpdateZermMatV();
	for (std::vector<CUpdateEvent *>::iterator e =  UpdateTab.begin (); e != UpdateTab.end (); ++e)
	{
		CUpdateEvent *event = *e;
		event->Update (flgSave);
	}
}
void CZerl::DeleteRow ()
{
	for (std::vector<CUpdateEvent *>::iterator e =  UpdateTab.begin (); e != UpdateTab.end (); ++e)
	{
		CUpdateEvent *event = *e;
		event->Update (7);  // 7 : L�schen
	}
	m_Mat.SetFocus ();
}


void CZerl::OnBnClickedChkGew()
{
	Zerm->ZerlTreeErlauben = Zerm->TYP_MATERIAL; 
	Zerm->zerm.chk_gew = FALSE;
	if (m_ChkGew.GetCheck () == BST_CHECKED)
	{
		Zerm->zerm.chk_gew = TRUE;
	}
	for (std::vector<CUpdateEvent *>::iterator e =  UpdateTab.begin (); e != UpdateTab.end (); ++e)
	{
		CUpdateEvent *event = *e;
		event->Update (-1); // < 0 : kein Update bei ZerlList n�tig
	}

}

void CZerl::OnBnClickedChkGewAnt()
{
	Zerm->ZerlTreeErlauben = Zerm->TYP_MATERIAL; 
	Zerm->zerm.chk_gew_ant = FALSE;
	if (m_ChkGewAnt.GetCheck () == BST_CHECKED)
	{
		Zerm->zerm.chk_gew_ant = TRUE;
	}
	for (std::vector<CUpdateEvent *>::iterator e =  UpdateTab.begin (); e != UpdateTab.end (); ++e)
	{
		CUpdateEvent *event = *e;
		event->Update (-1); // < 0 : kein Update bei ZerlList n�tig
	}
}

void CZerl::OnBnClickedChkMatoB()
{
	Zerm->ZerlTreeErlauben = Zerm->TYP_MATERIAL; 
	Zerm->zerm.chk_mat_o_b = FALSE;
	Zerm->zerm.chk_hk_vollk_preis = FALSE;
	if (m_ChkMatoB.GetCheck () == BST_CHECKED)
	{
		switch (Schnitt->schnitt._PreisArt)
		{
		case 1 :
			Zerm->zerm.chk_mat_o_b = TRUE;
			break;
		case 2 :
			Zerm->zerm.chk_hk_vollk_preis = TRUE;
			break;
		default :
			Zerm->zerm.chk_mat_o_b = TRUE;
			break;
		}
	}


	for (std::vector<CUpdateEvent *>::iterator e =  UpdateTab.begin (); e != UpdateTab.end (); ++e)
	{
		CUpdateEvent *event = *e;
		event->Update (-1); // < 0 : kein Update bei ZerlList n�tig
	}
}

void CZerl::OnBnClickedChkWert100kg()
{
	Zerm->ZerlTreeErlauben = Zerm->TYP_MATERIAL; 
	Zerm->zerm.chk_wert_100kg = FALSE;
	Zerm->zerm.chk_kennzeichen = FALSE;
	if (m_ChkWert100Kg.GetCheck () == BST_CHECKED)
	{
		Zerm->zerm.chk_wert_100kg = TRUE;
	}
	for (std::vector<CUpdateEvent *>::iterator e =  UpdateTab.begin (); e != UpdateTab.end (); ++e)
	{
		CUpdateEvent *event = *e;
		event->Update (-1); // < 0 : kein Update bei ZerlList n�tig
	}
}
void CZerl::OnBnClickedChkKennzeichen()
{
	Zerm->ZerlTreeErlauben = Zerm->TYP_MATERIAL; 
	Zerm->zerm.chk_wert_100kg = FALSE;
	Zerm->zerm.chk_kennzeichen = FALSE;
	if (m_ChkKennzeichen.GetCheck () == BST_CHECKED)
	{
		Zerm->zerm.chk_kennzeichen = TRUE;
	}
	for (std::vector<CUpdateEvent *>::iterator e =  UpdateTab.begin (); e != UpdateTab.end (); ++e)
	{
		CUpdateEvent *event = *e;
		event->Update (-1); // < 0 : kein Update bei ZerlList n�tig
	}
}
void CZerl::OnBnClickedChkZeitfaktor()
{
	Zerm->ZerlTreeErlauben = Zerm->TYP_MATERIAL; 
	Zerm->zerm.chk_zeitfaktor = FALSE;
	if (m_ChkZeitfaktor.GetCheck () == BST_CHECKED)
	{
		Zerm->zerm.chk_zeitfaktor = TRUE;
	}
	for (std::vector<CUpdateEvent *>::iterator e =  UpdateTab.begin (); e != UpdateTab.end (); ++e)
	{
		CUpdateEvent *event = *e;
		event->Update (-1); // < 0 : kein Update bei ZerlList n�tig
	}
}

void CZerl::OnBnClickedChkFwp()
{
	Zerm->ZerlTreeErlauben = Zerm->TYP_MATERIAL; 
	Zerm->zerm.chk_fwp = FALSE;
	if (m_ChkFwp.GetCheck () == BST_CHECKED)
	{
		Zerm->zerm.chk_fwp = TRUE;
	}
	for (std::vector<CUpdateEvent *>::iterator e =  UpdateTab.begin (); e != UpdateTab.end (); ++e)
	{
		CUpdateEvent *event = *e;
		event->Update (-1); // < 0 : kein Update bei ZerlList n�tig
	}
}
void CZerl::OnBnClickedChkVariabel()
{
	/** 190509
	if (Zerm->zerm.zerm_mat == Zerm->zerm.zerm_aus_mat)
	{
		return;
	}
	*/
	Zerm->ZerlTreeErlauben = Zerm->TYP_MATERIAL; 
	_tcscpy_s (Zerm->zerm.fix, "F");
	_tcscpy_s (Zerm->zerm.variabel, "N");
	if (m_Chk_Variabel.GetCheck () == BST_CHECKED)
	{
		_tcscpy_s (Zerm->zerm.fix, "V");
		_tcscpy_s (Zerm->zerm.variabel, "J");
	}
	UpdateList ();
	Umlasten (20);
    DisableFelder(); 
}

void CZerl::OnBnClickedChkFv()
{
	short chk_fv = 2;
	if (m_Chk_FV.GetCheck () == BST_CHECKED)
	{
		chk_fv = 3;
	}
	for (std::vector<CUpdateEvent *>::iterator e =  UpdateTab.begin (); e != UpdateTab.end (); ++e)
	{
		CUpdateEvent *event = *e;
		event->Update (chk_fv);
	}
}

void CZerl::OnEnChangeGew()
{
}

void CZerl::OnEnSetfocusGew()
{
	Zerm->ZerlTreeErlauben = Zerm->TYP_MATERIAL; 
}

void CZerl::OnEnSetfocusAusMat()
{
	OldAusMat = Zerm->zerm.zerm_aus_mat;
}

void CZerl::OnEnKillfocusAusMat()
{
	//Bringt Hier nichts, wird zu sp�t aufgerufen 
	/*
	if (OldAusMat != Zerm->zerm.zerm_aus_mat)
	{
		flgMatGeaendert = TRUE;
	}
	*/

}

void CZerl::OnEnSetfocusMat()
{
	FormGet();
	OldMat = Zerm->zerm.zerm_mat;
}


void CZerl::OnMatchoice ()
{
	if (Zerm->zerm.a_typ == Zerm->TYP_KOSTEN || 
	    Zerm->zerm.a_typ == Zerm->TYP_AUFSCHLAG || 
	    Zerm->zerm.a_typ == Zerm->TYP_AUFSCHLAG_EH)
	{
		OnKostArtChoice ();
		return;
	}

	ModalChoice = TRUE;
	AChoiceStatM = TRUE;
	FormGet ();
	long ZermMatOld = Zerm->zerm.zerm_mat;
	if (Zerm->zerm.a_typ == Zerm->TYP_MATERIAL)
	{
		Where.Format (_T(" and (a_bas.a_typ = 5 or a_bas.a_typ2 = 5 or a_bas.a_typ = 2 or a_bas.a_typ2 = 2) and a_bas.a in (select a from a_mat) "));
	} 
	else if (Zerm->zerm.a_typ == Zerm->TYP_ZUTAT) 
	{
		Where.Format (_T(" and (a_bas.a_typ = 6 or a_bas.a_typ2 = 6 or a_bas.a_typ = 5 or a_bas.a_typ2 = 5 or a_bas.a_typ = 9 or a_bas.a_typ2 = 9) and a_bas.a in (select a from a_mat) "));
	} 
	else if (Zerm->zerm.a_typ == Zerm->TYP_VKARTIKEL) 
	{
		Where.Format (_T(" and (a_bas.a_typ in (1,2) or a_bas.a_typ2 in (1,2)) "));
	} 
	else if (Zerm->zerm.a_typ == Zerm->TYP_VERPACKUNG) 
	{
		Where.Format (_T(" and (a_bas.a_typ in (8,9) or a_bas.a_typ2 in (8,9))  "));
	} 
	else if (Zerm->zerm.a_typ == Zerm->TYP_KOSTEN) //  (gibts noch nicht)
	{
		Where.Format (_T(" and (a_bas.a_typ = 99 or a_bas.a_typ2 = 99) and a_bas.a in (select a from a_mat) "));
	} 
	else if (Zerm->zerm.a_typ == Zerm->TYP_AUFSCHLAG) //  (gibts noch nicht)
	{
		Where.Format (_T(" and (a_bas.a_typ = 99 or a_bas.a_typ2 = 99) and a_bas.a in (select a from a_mat) "));
	} 
	else if (Zerm->zerm.a_typ == Zerm->TYP_AUFSCHLAG_EH) //  (gibts noch nicht)
	{
		Where.Format (_T(" and (a_bas.a_typ = 99 or a_bas.a_typ2 = 99) and a_bas.a in (select a from a_mat) "));
	} 
	if (ChoiceMat != NULL) // immer neu aufbauen, 
	{
		delete ChoiceMat;
		ChoiceMat = NULL;
	}

	if (ChoiceMat == NULL)
	{
		ChoiceMat = new CChoiceA (this);
	    ChoiceMat->IsModal = ModalChoice;
		ChoiceMat->Where = Where;
		ChoiceMat->IdArrDown = IDI_HARROWDOWN;
		ChoiceMat->IdArrUp   = IDI_HARROWUP;
		ChoiceMat->IdArrNo   = IDI_HARROWNO;
		ChoiceMat->HideEnter = TRUE;  // Bearbeiten Knopf raus mit TRUE
//		ChoiceMat->HideOK    = HideOK;

		ChoiceMat->CreateDlg ();
	}
	CString cA;
	m_Mat.GetWindowText (cA);
	if (!CStrFuncs::IsDecimal (cA))
	{
		ChoiceMat->SortRow   = 2;
		m_Mat.SetWindowText ("");
	}

    ChoiceMat->SetDbClass (A_bas);
	ChoiceMat->SearchText = SearchA;
	ChoiceMat->DoModal();
    if (ChoiceMat->GetState ())
    {
		  CABasList *abl = ChoiceMat->GetSelectedText (); 
		  if (abl == NULL) return;
          memcpy (&A_bas->a_bas, &a_bas_null, sizeof (A_BAS));
          A_bas->a_bas.a = abl->a;

		  if (Zerm->zerm.a_typ == Zerm->TYP_MATERIAL || 
			  Zerm->zerm.a_typ == Zerm->TYP_ZUTAT ||
			  Zerm->zerm.a_typ == Zerm->TYP_VKARTIKEL ||
			  Zerm->zerm.a_typ == Zerm->TYP_VERPACKUNG)
		  {
			if (A_bas->dbreadfirst () == 0)
			{
              if (Zerm->zerm.a_typ == Zerm->TYP_VERPACKUNG ||
				  Zerm->zerm.a_typ == Zerm->TYP_VKARTIKEL)
			  {
				  A_mat->a_mat.mat = (long) A_bas->a_bas.a;
				  Zerm->zerm.zerm_mat = (long) A_bas->a_bas.a;
				  ReadMaterial(A_mat->a_mat.mat);
			  }
			  else
			  {
				A_mat->a_mat.a = abl->a;
				if (A_mat->dbreadfirsta () == 0)
				{
					ReadMaterial(A_mat->a_mat.mat);
				}
			  }
			}
		  }
		  FormShow ();
		  m_Mat.EnableWindow (TRUE);
		  m_Mat.SetSel (0, -1, TRUE);
		  m_Mat.SetFocus ();
		  if (ZermMatOld != Zerm->zerm.zerm_mat)
		  {
				flgMatGeaendert = TRUE;
				UpdateList();
				OldMat =  Zerm->zerm.zerm_mat;
		  }
		  if (SearchA == "")
		  {
			PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
		  }
    }
	else
	{
		AChoiceStatM = FALSE;
	}
}

void CZerl::OnKostArtChoice ()
{
	ModalChoice = TRUE;
	FormGet ();
	long ZermMatOld = Zerm->zerm.zerm_mat;
	if (Zerm->zerm.a_typ == Zerm->TYP_KOSTEN) //  (gibts noch nicht)
	{
		Where.Format (_T(" where kost_art_zerl.mdn =  \"%hd\" and vkost_rechb = \"1\" "), Schnitt->schnitt.mdn);
	} 
	else if (Zerm->zerm.a_typ == Zerm->TYP_AUFSCHLAG) //  (gibts noch nicht)
	{
		Where.Format (_T(" where kost_art_zerl.mdn =  \"%hd\"  and vkost_rechb = \"4\" "), Schnitt->schnitt.mdn);
	} 
	else if (Zerm->zerm.a_typ == Zerm->TYP_AUFSCHLAG_EH) //  (gibts noch nicht)
	{
		Where.Format (_T(" where kost_art_zerl.mdn =  \"%hd\"  and vkost_rechb = \"4\" "), Schnitt->schnitt.mdn);
	} 
	if (ChoiceKostArt != NULL) // immer neu aufbauen, 
	{
		delete ChoiceKostArt;
		ChoiceKostArt = NULL;
	}

	if (ChoiceKostArt == NULL)
	{
		ChoiceKostArt = new CChoiceKostArt (this);
	    ChoiceKostArt->IsModal = ModalChoice;
		ChoiceKostArt->Where = Where;
		ChoiceKostArt->IdArrDown = IDI_HARROWDOWN;
		ChoiceKostArt->IdArrUp   = IDI_HARROWUP;
		ChoiceKostArt->IdArrNo   = IDI_HARROWNO;
		ChoiceKostArt->HideEnter = TRUE;  // Bearbeiten Knopf raus mit TRUE
//		ChoiceKostArt->HideOK    = HideOK;

		ChoiceKostArt->CreateDlg ();
	}
	CString cA;
	m_Mat.GetWindowText (cA);
	if (!CStrFuncs::IsDecimal (cA))
	{
		ChoiceKostArt->SortRow   = 2;
		m_Mat.SetWindowText ("");
	}

    ChoiceKostArt->SetDbClass (A_bas);
	ChoiceKostArt->SearchText = SearchA;
	ChoiceKostArt->DoModal();
    if (ChoiceKostArt->GetState ())
    {
		  CKostArtList *abl = ChoiceKostArt->GetSelectedText (); 
		  if (abl == NULL) return;
          memcpy_s (&kost_art_zerl.kost_art_zerl,sizeof (kost_art_zerl.kost_art_zerl), &kost_art_zerl_null, sizeof (kost_art_zerl.kost_art_zerl));
          kost_art_zerl.kost_art_zerl.kost_art_zerl = abl->kost_art_zerl;
		  kost_art_zerl.kost_art_zerl.mdn = Schnitt->schnitt.mdn;

		  if (Zerm->zerm.a_typ == Zerm->TYP_KOSTEN || 
			  Zerm->zerm.a_typ == Zerm->TYP_AUFSCHLAG ||
			  Zerm->zerm.a_typ == Zerm->TYP_AUFSCHLAG_EH )
		  {
			if (kost_art_zerl.dbreadfirst () == 0)
			{
				Zerm->zerm.zerm_mat = kost_art_zerl.kost_art_zerl.kost_art_zerl;
				ReadMaterial(Zerm->zerm.zerm_mat);
				DisableFelder();
			}
		  }
		  FormShow ();
		  m_Mat.EnableWindow (TRUE);
		  m_Mat.SetSel (0, -1, TRUE);
		  m_Mat.SetFocus ();
		  if (ZermMatOld != Zerm->zerm.zerm_mat)
		  {
				flgMatGeaendert = TRUE;
				UpdateList();
				OldMat =  Zerm->zerm.zerm_mat;
		  }
	  	  Umlasten (20);
		  if (SearchA == "")
		  {
			PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
		  }
    }
	else
	{
//		AChoiceStatM = FALSE;
	}
}


BOOL CZerl::ReadMaterial(long mat)
{
	ModalChoice = TRUE;
	if (ModalChoice)
	{
		CString cA;
		m_Mat.GetWindowText (cA);
		if (!CStrFuncs::IsDecimal (cA))
		{
			SearchA = cA;
			OnMatchoice ();
			SearchA = "";
			if (!AChoiceStatM)
			{
				m_Mat.SetFocus ();
				m_Mat.SetSel (0, -1);
				ModalChoice = FALSE;
				return FALSE;
			}
			else
			{
				return TRUE; // testtest
			}
		}
	}

	CString CStr;
		if (Work->ReadMaterial(Zerm->zerm.mdn,mat,Zerm->zerm.a_typ,Schnitt->cfg_PreiseAus_a_kalkpreis) != 0)
		{
			if (Zerm->zerm.a_typ == Zerm->TYP_MATERIAL || Zerm->zerm.a_typ == Zerm->TYP_VKARTIKEL)
			{
				return FALSE;
			}
			_tcscpy_s (Zerm->zerm.zerm_mat_bz1, Work->ABas.a_bas.a_bz1);
			_tcscpy_s (Zerm->zerm.kost_bz, Work->ABas.a_bas.a_bz1);

			Zerm->zerm.zerm_mat = mat;
			FormShow ();

			DisableFelder();
			return TRUE;

		}
		if (OldMat != mat)
		{
		}

		_tcscpy_s (Zerm->zerm.zerm_mat_bz1, Work->ABas.a_bas.a_bz1);
		_tcscpy_s (Zerm->zerm.kost_bz, Work->ABas.a_bas.a_bz1);

		Zerm->zerm.zerm_mat = mat;
		FormShow ();

		DisableFelder();
		return TRUE;
}

void CZerl::OnBnClickedButtonzerlegen()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	_tcscpy_s (Zerm->zerm.zerm_teil, "1");
	for (std::vector<CUpdateEvent *>::iterator e =  UpdateTab.begin (); e != UpdateTab.end (); ++e)
	{
		CUpdateEvent *event = *e;
		event->Update (0);  
	}


	Zerm->zerm.zerm_aus_mat = Zerm->zerm.zerm_mat;
	Zerm->zerm.zerm_aus_a_typ = Zerm->zerm.a_typ;
	Zerm->zerm.zerm_mat = Zerm->Zerlegeverlust;
	_tcscpy_s (Zerm->zerm.kost_bz, "Zerlegeverlust");
	_tcscpy_s (Zerm->zerm.zerm_mat_bz1, "Zerlegeverlust");
	_tcscpy_s (Zerm->zerm.a_kz, "ZV");
	Zerm->zerm.a_typ = Zerm->TYP_MATERIAL;
	short a_typ = Zerm->zerm.a_typ;
	short aus_a_typ = Zerm->zerm.zerm_aus_a_typ;
	long mat = Zerm->zerm.zerm_mat;
	long aus_mat = Zerm->zerm.zerm_aus_mat;
	for (std::vector<CUpdateEvent *>::iterator e =  UpdateTab.begin (); e != UpdateTab.end (); ++e)
	{
		CUpdateEvent *event = *e;
		event->Update (1);  // 1 = AddChild
	}
	m_ButtonZerlegen.ShowWindow(SW_HIDE);
	Zerm->zerm.zerm_mat = mat;
	Zerm->zerm.zerm_aus_mat = aus_mat;
	Zerm->zerm.a_typ = a_typ;
	Zerm->zerm.zerm_aus_a_typ = aus_a_typ;
	Form.Show();
}
BOOL CZerl::UpdateZermMatV ()
{
		return    Work->UpdateZermMatV(Schnitt->schnitt.mdn,
								   Schnitt->schnitt.schnitt,
								   Zerm->zerm.zerm_aus_mat,
								   Zerm->zerm.zerm_mat,
								   Zerm->zerm.fwp,
								   Zerm->zerm.zerm_gew,
								   Zerm->zerm.zerm_gew_ant,
								   Zerm->zerm.mat_o_b,
								   Zerm->zerm.hk_vollk,
								   Zerm->zerm.hk_teilk,
								   Zerm->zerm.sk_vollk,
								   Zerm->zerm.sk_teilk,
								   Zerm->zerm.fil_ek_vollk,
								   Zerm->zerm.fil_ek_teilk,
								   Zerm->zerm.fil_vk_vollk,
								   Zerm->zerm.fil_vk_teilk,
								   Zerm->zerm.gh1_vollk,
								   Zerm->zerm.gh1_teilk,
								   Zerm->zerm.gh2_vollk,
								   Zerm->zerm.gh2_teilk,
								   Zerm->zerm.dm_100,
								   Zerm->zerm.kosten_zutat,
								   Zerm->zerm.kosten_vpk,
								   Zerm->zerm.fix,
								   Zerm->zerm.a_typ,
								   Zerm->zerm.teilkosten_abs,
								   Zerm->zerm.vk_pr,
								   Zerm->zerm.zerm_teil,
								   Zerm->zerm.zerm_mat, // in kost_mat
								   Zerm->zerm.kost_bz, // in kost_mat_bz
								   Zerm->zerm.pr_vk_eh
								   );
}
void CZerl::TreeDown ()
{
	for (std::vector<CUpdateEvent *>::iterator e =  UpdateTab.begin (); e != UpdateTab.end (); ++e)
	{
		CUpdateEvent *event = *e;
		event->Update (101);  //101; nextitem
	}
}
void CZerl::TreeUp ()
{
	for (std::vector<CUpdateEvent *>::iterator e =  UpdateTab.begin (); e != UpdateTab.end (); ++e)
	{
		CUpdateEvent *event = *e;
		event->Update (102);  //102; Previtem
	}
}
void CZerl::DisableFelder ()
{
	CWnd * Control = GetFocus ();
	if (Control == NULL) return;
//	FormShow();
//	Form.GetFormField (&m_MatCombo)->Get ();
	/***
	if (Schnitt->schnitt._AnzVariabel == 0)
	{
			m_GesWert.EnableWindow (FALSE);
			m_DeckungA.EnableWindow (FALSE);
			m_DeckungP.EnableWindow (FALSE);
	}
	else
	{
			m_GesWert.EnableWindow (TRUE);
			m_DeckungA.EnableWindow (TRUE);
			m_DeckungP.EnableWindow (TRUE);
	}
	******/
	if (Zerm->zerm.a_typ == Zerm->TYP_MATERIAL)  
	{
		ShowMaterialGrid (SW_NORMAL);
		ShowZutatenGrid (SW_HIDE);
		ShowKostenGrid (SW_HIDE);
		ShowGridVk (SW_HIDE);

		if (Zerm->zerm.zerm_mat == Zerm->Zerlegeverlust || ZermA_KzToA_typ() != Zerm->TYP_MATERIAL)
		{
			m_Gew.EnableWindow (FALSE);
			m_GewAnt.EnableWindow (FALSE);
			m_MatoB.EnableWindow (FALSE);
			m_Wert100Kg.EnableWindow (FALSE);
			m_Fwp.EnableWindow (FALSE);
			m_Chk_Variabel.EnableWindow (FALSE);
			m_MatCombo.EnableWindow (FALSE);
			m_Zeitfaktor.EnableWindow (FALSE);
			return;
		}
		else
		{
			m_Gew.EnableWindow (TRUE);
			m_GewAnt.EnableWindow (TRUE);
			m_MatoB.EnableWindow (TRUE);
			m_Wert100Kg.EnableWindow (TRUE);
			m_Fwp.EnableWindow (TRUE);
			m_Chk_Variabel.EnableWindow (TRUE);
			m_MatCombo.EnableWindow (TRUE);
			m_Mat.EnableWindow (TRUE);
			m_Zeitfaktor.EnableWindow (TRUE);
		}
		CString CFix = Zerm->zerm.fix;
		if (_tstoi(Zerm->zerm.zerm_teil) == 1 || CFix == "V" ) // zerlegte Grobteile oder variabel
		{
			if (Control == &m_MatoB || Control == &m_Wert100Kg || Control == &m_Fwp)
			{
				m_Gew.SetFocus ();
			}
			else
			{
				Control->SetFocus ();
			}
		    m_MatoB.EnableWindow (FALSE);
		    m_Wert100Kg.EnableWindow (FALSE);
		    m_Fwp.EnableWindow (FALSE);
			if (Zerm->zerm.zerm_aus_mat == Zerm->zerm.zerm_mat)
			{
				if (Control == &m_Gew || Control == &m_GewAnt)
				{
					m_Mat.SetFocus ();
				}
				m_Gew.EnableWindow (FALSE);
				m_GewAnt.EnableWindow (FALSE);
			}
			else
			{
				m_Gew.EnableWindow (TRUE);
				m_GewAnt.EnableWindow (TRUE);
			}
//			m_MatCombo.EnableWindow (FALSE);
		    return;
		}
		else
		{
			m_MatoB.EnableWindow (TRUE);
			m_Wert100Kg.EnableWindow (TRUE);
			m_Fwp.EnableWindow (TRUE);
			m_Chk_Variabel.EnableWindow (TRUE);
		}
	}
	else if (Zerm->zerm.a_typ == Zerm->TYP_ZUTAT)   
	{
		ShowMaterialGrid (SW_HIDE);
		ShowZutatenGrid (SW_NORMAL);
		ShowKostenGrid (SW_HIDE);
		ShowGridVk (SW_HIDE);
		m_MatCombo.EnableWindow (TRUE);
		m_Mat.EnableWindow (TRUE);

		if (ZermA_KzToA_typ() != Zerm->TYP_ZUTAT)
		{
			m_ZutZugabe.EnableWindow (FALSE);
			m_ZutGew.EnableWindow (FALSE);
			m_ZutWert.EnableWindow (FALSE);
			m_ZutPreis.EnableWindow (FALSE);
			m_ZutWert100kg.EnableWindow (FALSE);
			return;
		}
		else
		{
			m_ZutZugabe.EnableWindow (TRUE);
			m_ZutPreis.EnableWindow (FALSE);     //Preis immer disabled , wird geholt
			m_ZutWert100kg.EnableWindow (FALSE);
			m_ZutGew.EnableWindow (TRUE);
			m_ZutWert.EnableWindow (FALSE);
		}
	}
	else if (Zerm->zerm.a_typ == Zerm->TYP_VERPACKUNG)   
	{
		ShowMaterialGrid (SW_HIDE);
		ShowVerpackungGrid (SW_NORMAL);
		ShowKostenGrid (SW_HIDE);
		ShowGridVk (SW_HIDE);
		m_MatCombo.EnableWindow (TRUE);
		m_Mat.EnableWindow (TRUE);

		if (ZermA_KzToA_typ() != Zerm->TYP_VERPACKUNG)
		{
			m_ZutZugabe.EnableWindow (FALSE);
			m_ZutGew.EnableWindow (FALSE);
			m_ZutWert.EnableWindow (FALSE);
			m_ZutPreis.EnableWindow (FALSE);
			m_ZutWert100kg.EnableWindow (FALSE);
			return;
		}
		else
		{
			m_ZutZugabe.EnableWindow (TRUE);
			m_ZutPreis.EnableWindow (FALSE);//Preis immer disabled , wird geholt
			m_ZutWert100kg.EnableWindow (FALSE);
			m_ZutGew.EnableWindow (TRUE);
			m_ZutWert.EnableWindow (FALSE);
		}
	}
	else if (Zerm->zerm.a_typ == Zerm->TYP_KOSTEN)  
	{
		ShowMaterialGrid (SW_HIDE);
		ShowZutatenGrid (SW_HIDE);
		ShowKostenGrid (SW_NORMAL);
		ShowGridVk (SW_HIDE);
		m_MatCombo.EnableWindow (TRUE);
		if (ZermA_KzToA_typ() != Zerm->TYP_KOSTEN)
		{
			m_KostBezeichnung.EnableWindow (FALSE);
			m_KostAufschlag.EnableWindow (FALSE);
			m_KostWert.EnableWindow (FALSE);
			return;
		}
		else
		{
			m_KostBezeichnung.EnableWindow (TRUE);
			m_KostAufschlag.EnableWindow (TRUE);
			m_KostWert.EnableWindow (FALSE);
		}
	}
	else if (Zerm->zerm.a_typ == Zerm->TYP_AUFSCHLAG || Zerm->zerm.a_typ == Zerm->TYP_AUFSCHLAG_EH)  
	{
		ShowMaterialGrid (SW_HIDE);
		ShowZutatenGrid (SW_HIDE);
		ShowAufschlagGrid (SW_NORMAL);
		ShowGridVk (SW_HIDE);
		m_MatCombo.EnableWindow (TRUE);
		if (ZermA_KzToA_typ() != Zerm->TYP_AUFSCHLAG)
		{
			m_KostBezeichnung.EnableWindow (FALSE);
			m_KostAufschlag.EnableWindow (FALSE);
			m_KostWert.EnableWindow (FALSE);
			return;
		}
		else
		{
			m_KostBezeichnung.EnableWindow (TRUE);
			m_KostAufschlag.EnableWindow (TRUE);
			m_KostWert.EnableWindow (FALSE);
		}
	}
	else if (Zerm->zerm.a_typ == Zerm->TYP_VKARTIKEL)  
	{
		ShowMaterialGrid (SW_HIDE);
		ShowZutatenGrid (SW_HIDE);
		ShowAufschlagGrid (SW_HIDE);
		ShowGridVk (SW_NORMAL);
		m_MatCombo.EnableWindow (TRUE);
	}
	Control->SetFocus ();
}
void CZerl::OnEnKillfocusMat()
{
	/**
		CString cMat;
		m_Mat.GetWindowText (cMat);

		if (ReadMaterial(_tstol(cMat.GetBuffer())) == TRUE)
		{
			UpdateList ();
		}
		**/

}

void CZerl::OnEnKillfocusGew()
{
	/*** Gibt hier nur Probleme durch setzen von ShowWindow HIDE daher in OnReturn
	double gew = Zerm->zerm.zerm_gew;
	FormGet();
	if (!CStrFuncs::IsDoubleEqual (gew, Zerm->zerm.zerm_gew))
	{
		if (!CStrFuncs::IsDoubleEqual (Schnitt->schnitt.ek_gew, (double) 0))
		{
			Zerm->zerm.zerm_gew_ant = 100 * Zerm->zerm.zerm_gew / Schnitt->schnitt.ek_gew;
			Zerm->zerm.dm_100 = Zerm->zerm.mat_o_b * Zerm->zerm.zerm_gew_ant;
			FormShow();
		}
		UpdateList();
		Umlasten ();
	}
	**************/
}
void CZerl::OnEnKillfocusGewAnt()
{
	/*** Gibt hier nur Probleme durch setzen von ShowWindow HIDE daher in OnReturn
	double gew_ant = Zerm->zerm.zerm_gew_ant;
	FormGet();
	if (!CStrFuncs::IsDoubleEqual (gew_ant, Zerm->zerm.zerm_gew_ant))
	{
		Zerm->zerm.zerm_gew  = Zerm->zerm.zerm_gew_ant * Schnitt->schnitt.ek_gew / 100;
		Zerm->zerm.dm_100 = Zerm->zerm.mat_o_b * Zerm->zerm.zerm_gew_ant;
		FormShow();
		UpdateList();
		Umlasten ();
	}
	**************/
}

void CZerl::OnEnKillfocusWertKg()
{
	/*** Gibt hier nur Probleme durch setzen von ShowWindow HIDE daher in OnReturn
	double mat_o_b = Zerm->zerm.mat_o_b;
	FormGet();
	if (!CStrFuncs::IsDoubleEqual (mat_o_b, Zerm->zerm.mat_o_b))
	{
		if (!CStrFuncs::IsDoubleEqual (Schnitt->schnitt._wert_fwp, (double) 0))
		{
			Zerm->zerm.fwp =  Zerm->zerm.mat_o_b / Schnitt->schnitt._wert_fwp; 
			Zerm->zerm.dm_100 = Zerm->zerm.mat_o_b * Zerm->zerm.zerm_gew_ant;
			FormShow();
		}
		UpdateList();
		Umlasten ();
	}
	**************/
}

void CZerl::OnEnKillfocusWert100kg()
{
	/*** Gibt hier nur Probleme durch setzen von ShowWindow HIDE daher in OnReturn
	double dm_100 = Zerm->zerm.dm_100;
	FormGet();
	if (!CStrFuncs::IsDoubleEqual (dm_100, Zerm->zerm.dm_100))
	{
		if (!CStrFuncs::IsDoubleEqual (Schnitt->schnitt._wert_fwp, (double) 0))
		{
			if (!CStrFuncs::IsDoubleEqual (Zerm->zerm.zerm_gew_ant, (double) 0))
			{
				Zerm->zerm.mat_o_b  = Zerm->zerm.dm_100 / Zerm->zerm.zerm_gew_ant;
				Zerm->zerm.fwp =  Zerm->zerm.mat_o_b / Schnitt->schnitt._wert_fwp; 
				FormShow();
			}
		}
		UpdateList();
		Umlasten ();
	}
	**************/
}

void CZerl::OnEnKillfocusFwp()
{
	/*** Gibt hier nur Probleme durch setzen von ShowWindow HIDE daher in OnReturn
	**************/
}
void CZerl::Umlasten (short dflg)
{
	for (std::vector<CUpdateEvent *>::iterator e =  UpdateTab.begin (); e != UpdateTab.end (); ++e)
	{
		CUpdateEvent *event = *e;
		event->Update (dflg);  // 20 = Umlasten  21 = vorher FWPNeuAufbauen()
	}
	CString CFix;
	CFix.Format (_T("%s"),Zerm->zerm.fix);
	if (CFix == "V")
	{
		_tcscpy_s (Zerm->zerm.variabel, "J");
	}
	else if (CFix == "F") 
	{
		_tcscpy_s (Zerm->zerm.variabel, "N");
	}

	FormShow () ; //Daten k�nnen nach Umlasten ver�ndert sein
}

void CZerl::FillMatCombo ()
{
	int di = 1;
	CFormField *f = Form.GetFormField (&m_MatCombo);
	if (f != NULL)
	{
		f->ComboValues.clear ();
        while (di > 0)
		{
			if (di == 1)
			{
				CString *ComboValue = new CString ();
				ComboValue->Format (_T("%d %s"), Zerm->TYP_MATERIAL,"Material");
				f->ComboValues.push_back (ComboValue); 
				di ++;
			}
			if (di == 2)
			{
				CString *ComboValue = new CString ();
				ComboValue->Format (_T("%d %s"), Zerm->TYP_VKARTIKEL,"VK-Artikel");
				f->ComboValues.push_back (ComboValue); 
				di ++;
			}
			if (di == 3)
			{
				CString *ComboValue = new CString ();
				ComboValue->Format (_T("%d %s"), Zerm->TYP_ZUTAT,"Zugabe (Fleisch und Gew�rze)");
				f->ComboValues.push_back (ComboValue); 
				di ++;
			}
			if (di == 4)
			{
				CString *ComboValue = new CString ();
				ComboValue->Format (_T("%d %s"), Zerm->TYP_VERPACKUNG,"Verpackung");
				f->ComboValues.push_back (ComboValue); 
				di ++;
			}
			if (di == 5)
			{
				CString *ComboValue = new CString ();
				ComboValue->Format (_T("%d %s"), Zerm->TYP_KOSTEN,"Kosten");
				f->ComboValues.push_back (ComboValue); 
				di ++;
			}
			if (di == 6)
			{
				CString *ComboValue = new CString ();
				ComboValue->Format (_T("%d %s"), Zerm->TYP_AUFSCHLAG,"Aufschlag zum VK-Netto");
				f->ComboValues.push_back (ComboValue); 
				di ++;
			}
			if (di == 7)
			{
				CString *ComboValue = new CString ();
				ComboValue->Format (_T("%d %s"), Zerm->TYP_AUFSCHLAG_EH,"Aufschlag zum EH-Preis");
				f->ComboValues.push_back (ComboValue); 
				di ++;
			}
			di = 0;
		}


		f->FillComboBox ();
		m_MatCombo.SetDroppedWidth (280);
		f->SetSel (0);
		f->Get ();
	}
}
void CZerl::FillPreisCombo ()
{

	int di = 1;
	CFormField *f = Form.GetFormField (&m_PreisCombo);
	if (f != NULL)
	{
		f->ComboValues.clear ();
        while (di > 0)
		{
			if (di == 1)
			{
				CString *ComboValue = new CString ();
				ComboValue->Format (_T("%d %s"), Schnitt->TYP_MAT_O_B,"MaterialPreis");
				f->ComboValues.push_back (ComboValue); 
				di ++;
			}
			if (di == 2)
			{
				CString *ComboValue = new CString ();
				ComboValue->Format (_T("%d %s"), Schnitt->TYP_HK_PR,"HK-Kosten/Aufschl�ge");
				f->ComboValues.push_back (ComboValue); 
				di ++;
			}
			/**************** erst mal raus TODO
			if (di == 3)
			{
				CString *ComboValue = new CString ();
				ComboValue->Format (_T("%d %s"), Schnitt->TYP_SK_PR,"SK-Preis");
				f->ComboValues.push_back (ComboValue); 
				di ++;
			}
			if (di == 4)
			{
				CString *ComboValue = new CString ();
				ComboValue->Format (_T("%d %s"), Schnitt->TYP_FIL_EK,"Fil-EK-Preis");
				f->ComboValues.push_back (ComboValue); 
				di ++;
			}
			if (di == 5)
			{
				CString *ComboValue = new CString ();
				ComboValue->Format (_T("%d %s"), Schnitt->TYP_FIL_VK,"Fil-VK-Preis");
				f->ComboValues.push_back (ComboValue); 
				di ++;
			}
			************/
			di = 0;
		}


		f->FillComboBox ();
		m_PreisCombo.SetDroppedWidth (280);
		f->SetSel (0);
		f->Get ();
	}
}

/*****
void CZerl::FillMatCombo ()
{
	CFormField *f = Form.GetFormField (&m_MatCombo);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
		Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 
		int cursor = -1;
			cursor = Ptabn.sqlcursor (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			                          _T("where ptitem = \"a_typ\" ")
									  _T("order by ptlfnr"));
		while (Ptabn.sqlfetch (cursor) == 0)
		{
			Ptabn.dbreadfirst ();

			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbez;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbez, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn.ptabn.ptwert,
				                             Ptabn.ptabn.ptbez);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
		m_MatCombo.SetDroppedWidth (300);
		f->SetSel (0);
		f->Get ();
	}
}
******/

void CZerl::ShowMaterialGrid(BOOL fShow)
{
	Form.GetFormField (&m_PreisCombo)->Get ();
	short di = Schnitt->schnitt._PreisArt;

	m_Chk_Variabel.ShowWindow (fShow);
	m_MaterialGroup.ShowWindow (fShow);

	m_PreisCombo.ShowWindow (fShow);

	m_Gew.ShowWindow (fShow);
	m_LGew.ShowWindow (fShow);
	m_ChkGew.ShowWindow (fShow);

	m_GewAnt.ShowWindow (fShow);
	m_LGewAnt.ShowWindow (fShow);
	m_ChkGewAnt.ShowWindow (fShow);

	if (Schnitt->schnitt.kalk_bas == 1)
	{
		m_LFwp.ShowWindow (fShow);
		m_Fwp.ShowWindow (fShow);
		m_ChkFwp.ShowWindow (fShow);
	}
	else
	{
		m_LFwp.ShowWindow (SW_HIDE);
		m_Fwp.ShowWindow (SW_HIDE);
		m_ChkFwp.ShowWindow (SW_HIDE);
	}

	switch (di)
    {
        case 1 :
			m_MatoB.ShowWindow (fShow);
			m_LMatoB.ShowWindow (fShow);
			m_ChkMatoB.ShowWindow (fShow);

			m_Wert.ShowWindow (fShow);
			m_LWert.ShowWindow (fShow);
			m_ChkWert.ShowWindow (fShow);

			m_Vk.ShowWindow (FALSE);
			m_LVk.ShowWindow (FALSE);
			m_ChkVk.ShowWindow (FALSE);

			m_Wert100Kg.ShowWindow (fShow);
			m_LWert100Kg.ShowWindow (fShow);
			m_ChkWert100Kg.ShowWindow (fShow);

			m_Kennzeichen.ShowWindow (FALSE);
			m_ChkKennzeichen.ShowWindow (FALSE);
			m_Zeitfaktor.ShowWindow (FALSE);
			m_ChkZeitfaktor.ShowWindow (FALSE);
			m_LZeitfaktor.ShowWindow (FALSE);
			break;
        case 2 :
			m_MatoB.ShowWindow (fShow);
			m_LMatoB.ShowWindow (fShow);
			m_ChkMatoB.ShowWindow (fShow);

			m_Wert.ShowWindow (fShow);
			m_LWert.ShowWindow (fShow);
			m_ChkWert.ShowWindow (fShow);

			m_Vk.ShowWindow (fShow);
			m_LVk.ShowWindow (fShow);
			m_ChkVk.ShowWindow (fShow);

			m_Wert100Kg.ShowWindow (FALSE);
			m_LWert100Kg.ShowWindow (fShow);
			m_ChkWert100Kg.ShowWindow (FALSE);

			m_Kennzeichen.ShowWindow (fShow);
			m_ChkKennzeichen.ShowWindow (fShow);
			m_Zeitfaktor.ShowWindow (fShow);
			m_LZeitfaktor.ShowWindow (fShow);
			m_ChkZeitfaktor.ShowWindow (fShow);
			break;
        default :
			m_MatoB.ShowWindow (fShow);
			m_LMatoB.ShowWindow (fShow);
			m_ChkMatoB.ShowWindow (fShow);

			m_Wert.ShowWindow (fShow);
			m_LWert.ShowWindow (fShow);
			m_ChkWert.ShowWindow (fShow);

			m_Vk.ShowWindow (fShow);
			m_LVk.ShowWindow (fShow);
			m_ChkVk.ShowWindow (fShow);

			m_Wert100Kg.ShowWindow (FALSE);
			m_LWert100Kg.ShowWindow (FALSE);
			m_ChkWert100Kg.ShowWindow (FALSE);
			m_Kennzeichen.ShowWindow (FALSE);
			m_ChkKennzeichen.ShowWindow (FALSE);
			m_Zeitfaktor.ShowWindow (FALSE);
			m_LZeitfaktor.ShowWindow (FALSE);
			m_ChkZeitfaktor.ShowWindow (FALSE);
			break;
	}


}
void CZerl::ShowZutatenGrid(BOOL fShow)
{
	    ShowGridZutat(fShow);
		if (fShow != SW_HIDE)
		{
			m_ZutatGroup.SetWindowText ("Zutaten");
			m_LZutZugabe.SetWindowText ("Zugabe in g / kg");
			m_LZutGew.SetWindowText ("Menge");
			m_LZutPreis.SetWindowText ("EK-Preis");
			m_LZutWert.SetWindowText ("Wert");
			m_LZutWert100kg.SetWindowText ("Wert / 100 kg");
			m_ChkZutWert100kg.ShowWindow (SW_HIDE);
			m_LZutWert100kg.ShowWindow (SW_HIDE);
			m_ZutWert100kg.ShowWindow (SW_HIDE);
		}
}
void CZerl::ShowVerpackungGrid(BOOL fShow)
{
	    ShowGridZutat(fShow);
		if (fShow != SW_HIDE)
		{
			m_ZutatGroup.SetWindowText ("Verpackungen");
			m_LZutZugabe.ShowWindow (SW_HIDE);
			m_ChkZutZugabe.ShowWindow (SW_HIDE);
			m_ZutZugabe.ShowWindow (SW_HIDE);
			m_LZutGew.SetWindowText ("Anzahl");
			m_LZutPreis.SetWindowText ("EK-Preis");
			m_LZutWert.SetWindowText ("Wert");
			m_ChkZutWert100kg.ShowWindow (SW_HIDE);
			m_LZutWert100kg.ShowWindow (SW_HIDE);
			m_ZutWert100kg.ShowWindow (SW_HIDE);
		}
}
void CZerl::ShowGridZutat(BOOL fShow)
{
		m_ZutatGroup.ShowWindow (fShow);
		m_LZutZugabe.ShowWindow (fShow);
//		m_ChkZutZugabe.ShowWindow (fShow);
		m_ZutZugabe.ShowWindow (fShow);
		m_LZutGew.ShowWindow (fShow);
//		m_ChkZutGew.ShowWindow (fShow);
		m_ZutGew.ShowWindow (fShow);
//		m_ChkZutPreis.ShowWindow (fShow);
		m_LZutPreis.ShowWindow (fShow);
		m_ZutPreis.ShowWindow (fShow);
		m_LZutWert.ShowWindow (fShow);
//		m_ChkZutWert.ShowWindow (fShow);
		m_ZutWert.ShowWindow (fShow);
//		m_ChkZutWert100kg.ShowWindow (fShow);
		m_LZutWert100kg.ShowWindow (fShow);
		m_ZutWert100kg.ShowWindow (fShow);
}
void CZerl::ShowKostenGrid(BOOL fShow)
{
		ShowGridKost(fShow);
		m_KostenGroup.SetWindowText ("Kosten");
		m_LKostBezeichnung.SetWindowText ("Bezeichnung");
		m_LKostAufschlag.SetWindowText ("Kosten / kg");
		m_LKostFix.SetWindowText ("Kosten Fix");
		m_LKostWert.SetWindowText ("Wert");
}
void CZerl::ShowAufschlagGrid(BOOL fShow)
{
		ShowGridKost(fShow);
		m_KostFix.ShowWindow (SW_HIDE);
		m_KostenGroup.SetWindowText ("Aufschlag zum VK Netto");
		m_LKostBezeichnung.SetWindowText ("Bezeichnung");
		m_LKostAufschlag.SetWindowText ("Aufschlag in %");
		m_LKostFix.SetWindowText ("");
		m_LKostWert.SetWindowText ("Wert / kg");
}
void CZerl::ShowGridKost(BOOL fShow)
{
		m_KostenGroup.ShowWindow (fShow);
		m_LKostBezeichnung.ShowWindow (fShow);
		m_LKostAufschlag.ShowWindow (fShow);
		m_LKostFix.ShowWindow (fShow);
		m_LKostWert.ShowWindow (fShow);
		m_KostBezeichnung.ShowWindow (fShow);
		m_KostAufschlag.ShowWindow (fShow);
		m_KostFix.ShowWindow (fShow);
		m_KostWert.ShowWindow (fShow);
}
void CZerl::ShowGridVk(BOOL fShow)
{
		m_VkartikelGroup.ShowWindow (fShow);
		m_LMatob.ShowWindow (fShow);
		m_LPrZutat.ShowWindow (fShow);
		m_LPrVpk.ShowWindow (fShow);
		m_LPrKosten.ShowWindow (fShow);
		m_LPrGh.ShowWindow (fShow);
		m_LPrEh.ShowWindow (fShow);
		m_Matob.ShowWindow (fShow);
		m_PrZutat.ShowWindow (fShow);
		m_PrVpk.ShowWindow (fShow);
		m_PrKosten.ShowWindow (fShow);
		m_PrGh.ShowWindow (fShow);
		m_PrEh.ShowWindow (fShow);
}


short CZerl::ZermA_KzToA_typ()
{
	CString A_Kz;
	A_Kz = Zerm->zerm.a_kz;
	if (A_Kz == "GT" || A_Kz == "VM" || A_Kz == "VT" || A_Kz == "ZA" || A_Kz == "ZV")
	{
		return Zerm->TYP_MATERIAL; 
	}
	if (A_Kz == "ZT")
	{
		return Zerm->TYP_ZUTAT;
	}
	if (A_Kz == "VK")
	{
		return Zerm->TYP_VKARTIKEL;
	}
	if (A_Kz == "VP")
	{
		return Zerm->TYP_VERPACKUNG;
	}
	if (A_Kz == "KO")
	{
		return Zerm->TYP_KOSTEN;
	}
	if (A_Kz == "AU")
	{
		return Zerm->TYP_AUFSCHLAG;
	}
	return 0;
}
void CZerl::OnEnKillfocusZutZugabe()
{
	/*** Gibt hier nur Probleme durch setzen von ShowWindow HIDE daher in OnReturn
	double gew = Zerm->zerm.zerm_gew_ant;
	FormGet ();
	if (!CStrFuncs::IsDoubleEqual (gew, Zerm->zerm.zerm_gew_ant))
	{
		Zerm->zerm.hk_teilk = Zerm->zerm.mat_o_b * Zerm->zerm.zerm_gew_ant / 1000;
		FormShow();
		UpdateList();
		Umlasten ();
	}
	**********/
}

void CZerl::OnEnKillfocusZutPreis()
{
	/*** Gibt hier nur Probleme durch setzen von ShowWindow HIDE daher in OnReturn
	double mat_o_b = Zerm->zerm.mat_o_b;
	FormGet ();
	if (!CStrFuncs::IsDoubleEqual (mat_o_b, Zerm->zerm.mat_o_b))
	{
		Zerm->zerm.hk_teilk = Zerm->zerm.mat_o_b * Zerm->zerm.zerm_gew_ant / 1000;
		FormShow();
		UpdateList();
		Umlasten ();
	}
	**********/
}

void CZerl::OnEnKillfocusZutWert100kg()
{
	/*** Gibt hier nur Probleme durch setzen von ShowWindow HIDE daher in OnReturn
	double dm_100 = Zerm->zerm.dm_100;
	FormGet ();
	if (!CStrFuncs::IsDoubleEqual (dm_100, Zerm->zerm.dm_100))
	{
		FormShow();
		UpdateList();
		Umlasten ();
	}
	**********/
}
void CZerl::FormGet()
{
	Form.Get ();

	Form.GetFormField (&m_PreisCombo)->Get ();
	short di = Schnitt->schnitt._PreisArt;
	switch (di)
    {
        case 1 :
			Mat1Form.Get ();
			break;
        case 2 :
			Mat2Form.Get ();
			break;
        case 3 :
			Mat3Form.Get ();
			break;
        case 4 :
			Mat4Form.Get ();
			break;
        case 5 :
			Mat5Form.Get ();
			break;
	}


	if (Zerm->zerm.a_typ == Zerm->TYP_ZUTAT ||Zerm->zerm.a_typ == Zerm->TYP_VERPACKUNG)
	{
		ZutForm.Get ();
	}
	if (Zerm->zerm.a_typ == Zerm->TYP_KOSTEN || Zerm->zerm.a_typ == Zerm->TYP_AUFSCHLAG
											 || Zerm->zerm.a_typ == Zerm->TYP_AUFSCHLAG_EH)
	{
		KostForm.Get ();
	}
	if (Zerm->zerm.a_typ == Zerm->TYP_VKARTIKEL)
	{
		VkForm.Get ();
	}
}
void CZerl::FormShow()
{
	Form.Show ();

	Form.GetFormField (&m_PreisCombo)->Get ();
	short di = Schnitt->schnitt._PreisArt;
	switch (di)
    {
        case 1 :
			Mat1Form.Show ();
			break;
        case 2 :
			Zerm->hk_vollk_preis = Zerm->zerm.hk_vollk ;
			Zerm->hk_vollk_kosten = Zerm->zerm.hk_vollk - Zerm->zerm.mat_o_b ;
			Zerm->hk_vollk_wert = Zerm->zerm.hk_vollk * Zerm->zerm.zerm_gew ;
			Mat2Form.Show ();
			break;
        case 3 :
			Mat3Form.Show ();
			break;
        case 4 :
			Mat4Form.Show ();
			break;
        case 5 :
			Mat5Form.Show ();
			break;
	}

	if (Zerm->zerm.a_typ == Zerm->TYP_ZUTAT ||Zerm->zerm.a_typ == Zerm->TYP_VERPACKUNG)
	{
		ZutForm.Show ();
	}
	if (Zerm->zerm.a_typ == Zerm->TYP_KOSTEN || Zerm->zerm.a_typ == Zerm->TYP_AUFSCHLAG
											 || Zerm->zerm.a_typ == Zerm->TYP_AUFSCHLAG_EH)
	{
		KostForm.Show ();
	}
	if (Zerm->zerm.a_typ == Zerm->TYP_VKARTIKEL)
	{
		VkForm.Show ();
	}
}


void CZerl::OnEnSetfocusGewAnt()
{
		Zerm->ZerlTreeErlauben = Zerm->TYP_MATERIAL; 
}

void CZerl::OnEnSetfocusWertKg()
{
		Zerm->ZerlTreeErlauben = Zerm->TYP_MATERIAL; 
}

void CZerl::OnEnSetfocusWert100kg()
{
		Zerm->ZerlTreeErlauben = Zerm->TYP_MATERIAL; 
}

void CZerl::OnEnSetfocusFwp()
{
		Zerm->ZerlTreeErlauben = Zerm->TYP_MATERIAL; 
}

void CZerl::OnEnSetfocusZutZugabe()
{
	/**
		CString Text = "Test";
		m_LZutZugabe.SetWindowText (Text);   //TESTTEST
**/
		Zerm->ZerlTreeErlauben = Zerm->TYP_ZUTAT; 
}

void CZerl::OnEnSetfocusZutPreis()
{
		Zerm->ZerlTreeErlauben = Zerm->TYP_ZUTAT; 
}

void CZerl::OnEnSetfocusZutWert100kg()
{
		Zerm->ZerlTreeErlauben = Zerm->TYP_ZUTAT; 
}

void CZerl::OnEnSetfocusKostBezeichnung()
{
		Zerm->ZerlTreeErlauben = Zerm->TYP_KOSTEN; 
}

void CZerl::OnEnSetfocusKostAufschlag()
{
		Zerm->ZerlTreeErlauben = Zerm->TYP_KOSTEN; 
}

void CZerl::OnEnSetfocusKostFix()
{
		Zerm->ZerlTreeErlauben = Zerm->TYP_KOSTEN; 
}

void CZerl::OnEnSetfocusKostAufschlagGes()
{
		Zerm->ZerlTreeErlauben = Zerm->TYP_KOSTEN; 
}


void CZerl::OnBnClickedChkZutZugabe()
{
		Zerm->ZerlTreeErlauben = Zerm->TYP_ZUTAT; 
}

void CZerl::OnBnClickedChkZutPreis()
{
		Zerm->ZerlTreeErlauben = Zerm->TYP_ZUTAT; 
}

void CZerl::OnBnClickedChkZutWert100kg()
{
		Zerm->ZerlTreeErlauben = Zerm->TYP_ZUTAT; 
}

void CZerl::OnStnClickedLwertKg()
{
}

void CZerl::OnBnClickedChkWert()
{
	Zerm->ZerlTreeErlauben = Zerm->TYP_MATERIAL; 
		switch (Schnitt->schnitt._PreisArt)
		{
		case 1 :
			Zerm->zerm.chk_vk = FALSE; 
			break;
		case 2 :
			Zerm->zerm.chk_hk_vollk_wert = FALSE;
			break;
		default :
			Zerm->zerm.chk_vk = FALSE;
			break;
		}
	if (m_ChkWert.GetCheck () == BST_CHECKED)
	{
		switch (Schnitt->schnitt._PreisArt)
		{
		case 1 :
			Zerm->zerm.chk_vk = TRUE; 
			break;
		case 2 :
			Zerm->zerm.chk_hk_vollk_wert = TRUE;
			break;
		default :
			Zerm->zerm.chk_vk = TRUE;
			break;
		}
	}
	for (std::vector<CUpdateEvent *>::iterator e =  UpdateTab.begin (); e != UpdateTab.end (); ++e)
	{
		CUpdateEvent *event = *e;
		event->Update (-1); // < 0 : kein Update bei ZerlList n�tig
	}
}

void CZerl::OnEnSetfocusWert()
{
		Zerm->ZerlTreeErlauben = Zerm->TYP_MATERIAL; 
}

void CZerl::OnBnClickedChkVk()
{
	Zerm->ZerlTreeErlauben = Zerm->TYP_MATERIAL; 
	Zerm->zerm.chk_vk = FALSE;
	Zerm->zerm.chk_hk_vollk = FALSE;
	if (m_ChkVk.GetCheck () == BST_CHECKED)
	{
		switch (Schnitt->schnitt._PreisArt)
		{
		case 1 :
			Zerm->zerm.chk_vk = TRUE;
			break;
		case 2 :
			Zerm->zerm.chk_hk_vollk = TRUE;
			break;
		default :
			Zerm->zerm.chk_vk = TRUE;
			break;
		}
	}
	for (std::vector<CUpdateEvent *>::iterator e =  UpdateTab.begin (); e != UpdateTab.end (); ++e)
	{
		CUpdateEvent *event = *e;
		event->Update (-1); // < 0 : kein Update bei ZerlList n�tig
	}
}

void CZerl::OnEnSetfocusVk()
{
		Zerm->ZerlTreeErlauben = Zerm->TYP_MATERIAL; 
}

void CZerl::OnBnClickedChkZutWert()
{
		Zerm->ZerlTreeErlauben = Zerm->TYP_ZUTAT; 
}

void CZerl::OnBnClickedChkZutGew()
{
		Zerm->ZerlTreeErlauben = Zerm->TYP_ZUTAT; 
}

void CZerl::OnEnSetfocusZutWert()
{
		Zerm->ZerlTreeErlauben = Zerm->TYP_ZUTAT; 
}

void CZerl::OnEnSetfocusZutGew()
{
		Zerm->ZerlTreeErlauben = Zerm->TYP_ZUTAT; 
}



void CZerl::OnEnSetfocusMatob()
{
		Zerm->ZerlTreeErlauben = Zerm->TYP_VKARTIKEL; 
}

void CZerl::OnEnSetfocusPrZutat()
{
		Zerm->ZerlTreeErlauben = Zerm->TYP_VKARTIKEL; 
}

void CZerl::OnEnSetfocusPrVpk()
{
		Zerm->ZerlTreeErlauben = Zerm->TYP_VKARTIKEL; 
}

void CZerl::OnEnSetfocusPrKosten()
{
		Zerm->ZerlTreeErlauben = Zerm->TYP_VKARTIKEL; 
}

void CZerl::OnEnSetfocusPrGh()
{
		Zerm->ZerlTreeErlauben = Zerm->TYP_VKARTIKEL; 
}

void CZerl::OnEnSetfocusPrEh()
{
		Zerm->ZerlTreeErlauben = Zerm->TYP_VKARTIKEL; 
}

void CZerl::OnBnClickedChkSingleExpand()
{
	Zerm->SINGLE_EXPAND = FALSE;
	if (m_ChkSingleExpand.GetCheck () == BST_CHECKED)
	{
		Zerm->SINGLE_EXPAND = TRUE;
	}
	for (std::vector<CUpdateEvent *>::iterator e =  UpdateTab.begin (); e != UpdateTab.end (); ++e)
	{
		CUpdateEvent *event = *e;
		event->Update (CLICKED_SINGLE_EXPAND);  
	}
}

void CZerl::OnBnClickedChkZutBold()
{
	Zerm->ZEIGE_KOSTEN = FALSE;
	if (m_ChkZutBold.GetCheck () == BST_CHECKED)
	{
	   	 Zerm->ZEIGE_KOSTEN = TRUE;  
	}
	for (std::vector<CUpdateEvent *>::iterator e =  UpdateTab.begin (); e != UpdateTab.end (); ++e)
	{
		CUpdateEvent *event = *e;
		event->Update (CLICKED_KOST_DETAILS);  
	}
}

void CZerl::OnBnClickedChkVkBold()
{
	Zerm->ZEIGE_VK = FALSE;
	if (m_ChkVkBold.GetCheck () == BST_CHECKED)
	{
	   	 Zerm->ZEIGE_VK = TRUE;  
	}
	CString zerm_mat_bz1 = Zerm->zerm.kost_bz;
	for (std::vector<CUpdateEvent *>::iterator e =  UpdateTab.begin (); e != UpdateTab.end (); ++e)
	{
		CUpdateEvent *event = *e;
		event->Update (CLICKED_VK_DETAILS);  // Hier geht kost_bz verloren !!!!!!! 
	}
//	Umlasten (); // Hier geht kost_bz verloren !!!!!!!  warum hier Umlasten ??? jetzt raus
//	strcpy_s(Zerm->zerm.kost_bz,zerm_mat_bz1);
//	Form.Get();
//	Form.Show();
}
void CZerl::KostArtSpeichern()
{
		memcpy_s (&kost_art_zerl.kost_art_zerl,sizeof (kost_art_zerl.kost_art_zerl), &kost_art_zerl_null, sizeof (kost_art_zerl.kost_art_zerl));
		kost_art_zerl.kost_art_zerl.mdn = Zerm->zerm.mdn;
		kost_art_zerl.kost_art_zerl.kost_art_zerl = (short) Zerm->zerm.zerm_mat;
		if (Zerm->zerm.a_typ == Zerm->TYP_KOSTEN)
		{
			_tcscpy (kost_art_zerl.kost_art_zerl.vkost_rechb,"1");//�/kg
		}
		else
		{
			_tcscpy (kost_art_zerl.kost_art_zerl.vkost_rechb,"4");//%Aufschlag
		}
		kost_art_zerl.kost_art_zerl.faktor = 1;
		_tcscpy (kost_art_zerl.kost_art_zerl.kost_art_zerl_bz, Zerm->zerm.kost_bz);
		kost_art_zerl.kost_art_zerl.vkost_wt = Zerm->zerm.mat_o_b;
		kost_art_zerl.kost_art_zerl.vkost_fix = Zerm->zerm.vollkosten_abs;
		CString Date;
		CStrFuncs::SysDate (Date);
		kost_art_zerl.ToDbDate (Date, &kost_art_zerl.kost_art_zerl.dat);

		kost_art_zerl.dbupdate();
}


void CZerl::OnCbnSelchangeMatCombo()
{
	if (Zerm->zerm.a_typ == Zerm->TYP_MATERIAL)
	{
		OldAusMat = Zerm->zerm.zerm_aus_mat;
	}
	OldA_typ = Zerm->zerm.a_typ;
	Form.GetFormField (&m_MatCombo)->Get ();

	if (Zerm->zerm.a_typ == Zerm->TYP_MATERIAL)
	{
		Zerm->zerm.zerm_aus_mat = OldAusMat;
	}

	DisableFelder () ;
	if (Zerm->zerm.a_typ == Zerm->TYP_ZUTAT && (ZermA_KzToA_typ () == Zerm->TYP_MATERIAL || ZermA_KzToA_typ () == Zerm->TYP_VKARTIKEL))
	{
		Zerm->zerm.zerm_aus_mat = Zerm->zerm.zerm_mat;
		Zerm->zerm.zerm_aus_a_typ = ZermA_KzToA_typ ();
	}
	if (Zerm->zerm.a_typ == Zerm->TYP_VERPACKUNG && (ZermA_KzToA_typ () == Zerm->TYP_MATERIAL || ZermA_KzToA_typ () == Zerm->TYP_VKARTIKEL))
	{
		Zerm->zerm.zerm_aus_mat = Zerm->zerm.zerm_mat;
		Zerm->zerm.zerm_aus_a_typ = ZermA_KzToA_typ ();
	}
	if (Zerm->zerm.a_typ == Zerm->TYP_KOSTEN && (ZermA_KzToA_typ () == Zerm->TYP_MATERIAL || ZermA_KzToA_typ () == Zerm->TYP_VKARTIKEL))
	{
		Zerm->zerm.zerm_aus_mat = Zerm->zerm.zerm_mat;
		Zerm->zerm.zerm_aus_a_typ = ZermA_KzToA_typ ();
	}
	if (Zerm->zerm.a_typ == Zerm->TYP_AUFSCHLAG && (ZermA_KzToA_typ () == Zerm->TYP_MATERIAL || ZermA_KzToA_typ () == Zerm->TYP_VKARTIKEL))
	{
		Zerm->zerm.zerm_aus_mat = Zerm->zerm.zerm_mat;
		Zerm->zerm.zerm_aus_a_typ = ZermA_KzToA_typ ();
	}
	if (Zerm->zerm.a_typ == Zerm->TYP_AUFSCHLAG_EH && (ZermA_KzToA_typ () == Zerm->TYP_MATERIAL || ZermA_KzToA_typ () == Zerm->TYP_VKARTIKEL))
	{
		Zerm->zerm.zerm_aus_mat = Zerm->zerm.zerm_mat;
		Zerm->zerm.zerm_aus_a_typ = ZermA_KzToA_typ ();
	}
	if (Zerm->zerm.a_typ == Zerm->TYP_VKARTIKEL && ZermA_KzToA_typ () == Zerm->TYP_MATERIAL)
	{
		Zerm->zerm.zerm_aus_mat = Zerm->zerm.zerm_mat;
		Zerm->zerm.zerm_aus_a_typ = ZermA_KzToA_typ ();
	}
	FormShow ();
}

void CZerl::OnCbnSelchangePreisCombo()
{
	Form.GetFormField (&m_PreisCombo)->Get ();
	short di = Schnitt->schnitt._PreisArt;
	ShowMaterialGrid (SW_NORMAL);
   Zerm->zerm.chk_mat_o_b = FALSE;
   Zerm->zerm.chk_wert_100kg = FALSE;
   Zerm->zerm.chk_wert = FALSE;
   Zerm->zerm.chk_vk = FALSE;
   Zerm->zerm.chk_fwp = FALSE;
   Zerm->zerm.chk_kennzeichen = FALSE;
   Zerm->zerm.chk_zeitfaktor = FALSE;
   Zerm->zerm.chk_hk_vollk_preis = FALSE;
   Zerm->zerm.chk_hk_vollk_wert = FALSE;
   Zerm->zerm.chk_hk_vollk = FALSE;
	switch (di)
    {
        case 1 :
			m_EnterErgebnis.SetWindowText (_T("Ergebnis:   Mat.Preis"));
			m_LMatoB.SetWindowText (_T("Materialpreis"));
			m_LWert.SetWindowText (_T("Wert (Preis*Gewicht)"));
			m_LWert100Kg.SetWindowText (_T("Wert / 100 Kg"));
			break;
        case 2 :
			m_EnterErgebnis.SetWindowText (_T("Ergebnis:   HK-Preis"));
			m_LMatoB.SetWindowText (_T("HK-Preis"));
			m_LWert.SetWindowText (_T("HK-Wert"));
			m_LVk.SetWindowText (_T("HK-Kosten"));
			m_LWert100Kg.SetWindowText (_T("Kennzeichen"));
             break;
        case 3 :
			m_EnterErgebnis.SetWindowText (_T("Ergebnis:   SK-Preis"));
			m_LMatoB.SetWindowText (_T("SK-Preis"));
			m_LWert.SetWindowText (_T("SK-Wert"));
			m_LVk.SetWindowText (_T("SK-Kosten"));
			m_LWert100Kg.SetWindowText (_T("Kennzeichen"));
             break;
        case 4 :
			m_EnterErgebnis.SetWindowText (_T("Ergebnis:   Fil-EK"));
			m_LMatoB.SetWindowText (_T("Fil-EK-Preis"));
			m_LWert.SetWindowText (_T("Fil-EK-Wert"));
			m_LVk.SetWindowText (_T("Fil-EK-Kosten"));
			m_LWert100Kg.SetWindowText (_T("Kennzeichen"));
             break;
        default :
			m_EnterErgebnis.SetWindowText (_T("Ergebnis:   Fil-VK"));
			m_LMatoB.SetWindowText (_T("Fil-VK-Preis"));
			m_LWert.SetWindowText (_T("Fil-VK-Wert"));
			m_LVk.SetWindowText (_T("Fil-VK-Kosten"));
			m_LWert100Kg.SetWindowText (_T("Kennzeichen"));
             break;
    }
//	Form.Show();
 
	/**
		    if (Schnitt->schnitt._DeckungP < 0.01) Schnitt->schnitt._DeckungP = 10; 
			Schnitt->schnitt._DeckungA = Schnitt->schnitt._DeckungP * (Schnitt->schnitt.ek_pr * Schnitt->schnitt.ek_gew) / 100;
			Schnitt->schnitt._SumPreis  = (Schnitt->schnitt.ek_pr * Schnitt->schnitt.ek_gew) + Schnitt->schnitt._DeckungA;
			Schnitt->schnitt.deckung_proz = Schnitt->schnitt._DeckungP;
**/

			FormShow();
			UpdateList();
			Umlasten (20);
			m_Gew.SetFocus ();


//	Umlasten (23) ; // Vk_Laden aufrufen 
}

void CZerl::OnEnSetfocusZeitfaktor()
{
	Zerm->ZerlTreeErlauben = Zerm->TYP_MATERIAL; 
}

void CZerl::OnEnKillfocusGrundpreis()
{
	FormGet();
			Schnitt->schnitt._DeckungA = Schnitt->schnitt._SumPreis - (Schnitt->schnitt.ek_pr * Schnitt->schnitt.ek_gew);
			Schnitt->schnitt._DeckungP = Schnitt->schnitt._DeckungA * 100 / (Schnitt->schnitt.ek_pr * Schnitt->schnitt.ek_gew);
			Schnitt->schnitt.deckung_proz = Schnitt->schnitt._DeckungP;
	FormShow();
}
