#include "StdAfx.h"
#include "Zerlsheet.h"
#include "DbPropertyPage.h"

IMPLEMENT_DYNAMIC(CZerlSheet, CDbPropertySheet)

CZerlSheet::CZerlSheet(void)
{
}

CZerlSheet::~CZerlSheet(void)
{
}
BOOL CZerlSheet::Page2Write ()
{
	CDbPropertyPage *Page2 = (CDbPropertyPage *) GetPage (1);
	if (Page2 == NULL) return FALSE;

	return Page2->Write (TRUE);   
}


BOOL CZerlSheet::Write ()
{
//	CDbPropertyPage *Page = (CDbPropertyPage *) GetActivePage ();
	CDbPropertyPage *Page = (CDbPropertyPage *) GetPage (0);
	if (Page == NULL) return FALSE;
	BOOL ret = Page->Write (TRUE);
	if (ret == FALSE) return ret;


	CDbPropertyPage *Page2 = (CDbPropertyPage *) GetActivePage ();
	if (Page2 == NULL) return FALSE;
	if (Page == Page2) return TRUE;
	ret = Page2->Write (TRUE);
	if (ret == FALSE) return ret;

    SetActivePage (0);
	if (ret )
	{
		Page->AfterWrite ();
	}
	return ret;
}

void CZerlSheet::StepBack ()
{

//	CDbPropertyPage *Page = (CDbPropertyPage *) GetActivePage ();
	CDbPropertyPage *Page = (CDbPropertyPage *) GetPage (0);
	if (Page == NULL) return;
	CDbPropertyPage *Page2 = (CDbPropertyPage *) GetActivePage ();
	if (Page2 != NULL) 
	{
		if (Page != Page2)
		{

			Page2->Write (TRUE);
//			int ret = MessageBox (_T("Speichern ? "), NULL, MB_YESNOCANCEL | MB_ICONQUESTION);
//			if  (ret == IDYES)
//			{
//				Page2->Write (TRUE);
//			}
//			else
//			{
//				Page2->Write (FALSE);
//			}
//			if (ret == IDCANCEL) return ;
		}
	}



    SetActivePage (0);
	Page->StepBack ();
}

BOOL CZerlSheet::Delete ()
{
//    CDbPropertyPage *ActPage = (CDbPropertyPage *) GetActivePage ();
	CDbPropertyPage *Page = (CDbPropertyPage *) GetPage (0);
	if (Page == NULL) return FALSE;
//	SetActivePage (0);
	BOOL ret = Page->Delete ();
	if (ret)
	{
		SetActivePage (0);
//		SetActivePage (ActPage);
	}
	return ret;
}

void CZerlSheet::OnCopy ()
{

	CDbPropertyPage *Page = (CDbPropertyPage *) GetActivePage ();
	if (Page != NULL) 
	{
		Page->OnCopy ();
	}
}

void CZerlSheet::OnPrint ()
{

	CDbPropertyPage *Page = (CDbPropertyPage *) GetActivePage ();
	if (Page != NULL) 
	{
		Page->Print (Page->m_hWnd);
	}
}
void CZerlSheet::OnPaste ()
{

	CDbPropertyPage *Page = (CDbPropertyPage *) GetActivePage ();
	if (Page != NULL) 
	{
		Page->OnPaste ();
	}
}
