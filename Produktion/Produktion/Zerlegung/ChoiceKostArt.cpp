#include "stdafx.h"
#include "ChoiceKostArt.h"
#include "DbUniCode.h"

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

int CChoiceKostArt::Sort1 = -1;
int CChoiceKostArt::Sort2 = -1;
int CChoiceKostArt::Sort3 = -1;

CChoiceKostArt::CChoiceKostArt(CWnd* pParent) 
        : CChoiceX(pParent)
{
	Where = "";
}

CChoiceKostArt::~CChoiceKostArt() 
{
	DestroyList ();
}

void CChoiceKostArt::DestroyList() 
{
	for (std::vector<CKostArtList *>::iterator pabl = KostArtList.begin (); pabl != KostArtList.end (); ++pabl)
	{
		CKostArtList *abl = *pabl;
		delete abl;
	}
    KostArtList.clear ();
}

void CChoiceKostArt::FillList () 
{
    short  kost_art_zerl;
    TCHAR kost_art_zerl_bz [34];

	DestroyList ();
	CListCtrl *listView = GetListView ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

    SetWindowText (_T("Auswahl Kostenarten"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
    SetCol (_T("Kostenart"),  1, 50, LVCFMT_RIGHT);
    SetCol (_T("Bezeichnung"),  2, 250);

	if (KostArtList.size () == 0)
	{
		DbClass->sqlout ((short *)&kost_art_zerl,      SQLSHORT, 0);
		DbClass->sqlout ((LPTSTR)  kost_art_zerl_bz,   SQLCHAR, sizeof (kost_art_zerl_bz));
		CString Sql  = (_T("select kost_art_zerl.kost_art_zerl, kost_art_zerl.kost_art_zerl_bz ")
			                             _T("from kost_art_zerl ")
										 _T(" "));
		Sql += " ";
		Sql += Where;
		int cursor = DbClass->sqlcursor (Sql.GetBuffer ());
		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) kost_art_zerl_bz;
			CDbUniCode::DbToUniCode (kost_art_zerl_bz, pos);
			CKostArtList *abl = new CKostArtList (kost_art_zerl, kost_art_zerl_bz);
			KostArtList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
	}

	for (std::vector<CKostArtList *>::iterator pabl = KostArtList.begin (); pabl != KostArtList.end (); ++pabl)
	{
		CKostArtList *abl = *pabl;
		CString kost_art_zerl;
		kost_art_zerl.Format (_T("%hd"), abl->kost_art_zerl); 
		CString Num;
		CString Bez;
		_tcscpy (kost_art_zerl_bz, abl->kost_art_zerl_bz.GetBuffer ());

		CString LText;
		LText.Format (_T("%hd %s"), abl->kost_art_zerl, 
									abl->kost_art_zerl_bz.GetBuffer ());
		if (Style == LVS_REPORT)
		{
                Num = kost_art_zerl;
        }
        else
        {
                Num = LText;
        }
        int ret = InsertItem (i, -1);
        ret = SetItemText (Num.GetBuffer (), i, 1);
        ret = SetItemText (kost_art_zerl_bz, i, 2);
        i ++;
    }

	SortRow = 1;
    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort (listView);
}


void CChoiceKostArt::NumLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        LPTSTR p = aText.GetBuffer ();
        p = _tcstok (p, _T(" "));
        aText.Format (_T("%s"), p);
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceKostArt::BezLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        aText.TrimRight ();
        CString iText = ListBox->GetItemText (i, 1);
        iText.TrimRight ();
        aText.Format (_T("%s %s"), aText.GetBuffer (), iText.GetBuffer ());
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceKostArt::SearchNum (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 1);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceKostArt::SearchKostArtBz (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 2);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceKostArt::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
    switch (SortRow)
    {
        case 0 :
//             SearchNum (ListBox, EditText.GetBuffer (16));
             break;
        case 1 :
             SearchNum (ListBox, EditText.GetBuffer (8));
             break;
        case 2 :
             EditText.MakeUpper ();
             SearchKostArtBz (ListBox, EditText.GetBuffer ());
             break;
    }
}

int CChoiceKostArt::GetPtBez (LPTSTR ptwert, LPTSTR ptbez)
{
	_tcscpy (ptbez, _T(""));
	DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
	DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
	return DbClass->sqlcomm (_T("select ptbez from ptabn where ptitem = \"peri_typ\" ")
							_T("and ptwert = ?"));
}



int CALLBACK CChoiceKostArt::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   int SortRow = CmpRow;
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   if (SortRow == 0)
   {
//	return strcmp(strItem2.GetBuffer (0), strItem1.GetBuffer (0)) * Sort1;
   }
   else if (SortRow == 1)
   {

	   int li1 = _tstoi (strItem1.GetBuffer ());
	   int li2 = _tstoi (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort2;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort2);
	   }
	   return 0;
   }
   else if (SortRow == 2)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort3;
   }
   return 0;
}


void CChoiceKostArt::Sort (CListCtrl *ListBox)
{
    CmpRow = SortRow;
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);

    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
              break;
        case 2:
              Sort3 *= -1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CKostArtList *abl = KostArtList [i];
		   
		   abl->kost_art_zerl     = (short) _tstoi (ListBox->GetItemText (i, 1));
		   abl->kost_art_zerl_bz = ListBox->GetItemText (i, 2);
	}
}

void CChoiceKostArt::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = KostArtList [idx];
}

CKostArtList *CChoiceKostArt::GetSelectedText ()
{
	CKostArtList *abl = (CKostArtList *) SelectedRow;
	return abl;
}

