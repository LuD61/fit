#include "stdafx.h"
#include "ChoiceKalk.h"
#include "DbUniCode.h"

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

int CChoiceKalk::Sort1 = -1;
int CChoiceKalk::Sort2 = -1;
int CChoiceKalk::Sort3 = -1;

CChoiceKalk::CChoiceKalk(CWnd* pParent) 
        : CChoiceX(pParent)
{
}

CChoiceKalk::~CChoiceKalk() 
{
	DestroyList ();
}

void CChoiceKalk::DestroyList() 
{
	for (std::vector<CKalkList *>::iterator pabl = KalkList.begin (); pabl != KalkList.end (); ++pabl)
	{
		CKalkList *abl = *pabl;
		delete abl;
	}
    KalkList.clear ();
}

void CChoiceKalk::FillList () 
{
    short  mdn;
    long   schlklknr = 0;
    TCHAR schklk_bez [37];

	DestroyList ();
	CListCtrl *listView = GetListView ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

    SetWindowText (_T("Auswahl Schlachtkalkulation"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
    SetCol (_T("Nummer"),  1, 50, LVCFMT_RIGHT);
    SetCol (_T("Bezeichnung"),  2, 250);

	if (KalkList.size () == 0)
	{
		DbClass->sqlout ((short *)&mdn,      SQLSHORT, 0);
		DbClass->sqlout ((long *)&schlklknr,      SQLLONG, 0);
		DbClass->sqlout ((LPTSTR)  schklk_bez,   SQLCHAR, sizeof (schklk_bez));
		int cursor = DbClass->sqlcursor (_T("select schlaklkk.mdn,schlaklkk.schlklknr,schlaklkk.schklk_bez ")
			                             _T("from schlaklkk  "));
		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) schklk_bez;
			CDbUniCode::DbToUniCode (schklk_bez, pos);
			CKalkList *abl = new CKalkList (mdn, schlklknr, schklk_bez);
			KalkList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
	}

	for (std::vector<CKalkList *>::iterator pabl = KalkList.begin (); pabl != KalkList.end (); ++pabl)
	{
		CKalkList *abl = *pabl;
		CString KalkNr;
		KalkNr.Format (_T("%ld"), abl->schlklknr); 
		CString Num;
		CString Bez;
		_tcscpy (schklk_bez, abl->schklk_bez.GetBuffer ());

		CString LText;
		LText.Format (_T("%ld %s"), abl->schlklknr, 
									abl->schklk_bez.GetBuffer ());
		if (Style == LVS_REPORT)
		{
                Num = KalkNr;
        }
        else
        {
                Num = LText;
        }
        int ret = InsertItem (i, -1);
        ret = SetItemText (Num.GetBuffer (), i, 1);
        ret = SetItemText (schklk_bez, i, 2);
        i ++;
    }

	SortRow = 1;
    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort (listView);
}


void CChoiceKalk::NumLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        LPTSTR p = aText.GetBuffer ();
        p = _tcstok (p, _T(" "));
        aText.Format (_T("%s"), p);
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceKalk::BezLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        aText.TrimRight ();
        CString iText = ListBox->GetItemText (i, 1);
        iText.TrimRight ();
        aText.Format (_T("%s %s"), aText.GetBuffer (), iText.GetBuffer ());
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceKalk::SearchNum (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 1);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceKalk::SearchName (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 2);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceKalk::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
    switch (SortRow)
    {
        case 0 :
//             SearchNum (ListBox, EditText.GetBuffer (16));
             break;
        case 1 :
             SearchNum (ListBox, EditText.GetBuffer (8));
             break;
        case 2 :
             EditText.MakeUpper ();
             SearchName (ListBox, EditText.GetBuffer ());
             break;
    }
}


int CALLBACK CChoiceKalk::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.


	int SortRow = CmpRow;
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   if (SortRow == 0)
   {
//	return strcmp(strItem2.GetBuffer (0), strItem1.GetBuffer (0)) * Sort1;
   }
   else if (SortRow == 1)
   {

	   int li1 = _tstoi (strItem1.GetBuffer ());
	   int li2 = _tstoi (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort2;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort2);
	   }
	   return 0;
   }
   else if (SortRow == 2)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort3;
   }
   return 0;
}


void CChoiceKalk::Sort (CListCtrl *ListBox)
{
    CmpRow = SortRow;
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);

    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
              break;
        case 2:
              Sort3 *= -1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CKalkList *abl = KalkList [i];
		   
		   abl->schlklknr     = (long) _tstoi (ListBox->GetItemText (i, 1));
		   abl->schklk_bez = ListBox->GetItemText (i, 2);
	}
}

void CChoiceKalk::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = KalkList [idx];
}

CKalkList *CChoiceKalk::GetSelectedText ()
{
	CKalkList *abl = (CKalkList *) SelectedRow;
	return abl;
}

