//200810  Problem trat bei Richter auf Ausgangsmat. "1/2 Schwein" (Bezeichnung mit einem Leerzeichen)
// MaTree.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "Zerlegung.h"
#include "MaTree.h"
#include "Token.h"
#include "StrFuncs.h"
#include <vector>

CString A_Kz;
// CMaTree-Dialogfeld

IMPLEMENT_DYNAMIC(CMaTree, CDialog)
CMaTree::CMaTree(CWnd* pParent /*=NULL*/)
	: CDialog(CMaTree::IDD, pParent)
{
	DlgBkColor = GetSysColor (COLOR_3DFACE);
	DlgBrush = NULL;
	PasteOK = FALSE;
	CopyOK = FALSE;
	pImageList = NULL;
}

CMaTree::~CMaTree()
{
		delete pImageList;
		pImageList = NULL;
}

void CMaTree::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_ZTREE, m_MaTree);
	DDX_Control(pDX, IDC_LMATREE, m_LMaTree);

}


BEGIN_MESSAGE_MAP(CMaTree, CDialog)
	ON_WM_SIZE ()
	ON_WM_CTLCOLOR ()
	ON_NOTIFY(TVN_SELCHANGED, IDC_ZTREE, OnTvnSelchangedMaTree)
	ON_NOTIFY(NM_DBLCLK, IDC_ZTREE, OnNMDblclkZtree)
	ON_NOTIFY(NM_RCLICK, IDC_ZTREE, OnNMRclkZtree)
	ON_NOTIFY(NM_CLICK, IDC_ZTREE, OnNMClickZtree)

END_MESSAGE_MAP()


// CMaTree-Meldungshandler
BOOL CMaTree::OnInitDialog()
{
	A_bas   = &Page1->A_bas;
	A_mat   = &Page1->A_mat;
	Zerm   = &Page1->Zerm;
	Schnitt   = &Page1->Schnitt;
	Work   = &Page1->Work;

	m_MaTree.A_bas = A_bas;
	m_MaTree.Zerm = Zerm;
	m_MaTree.Schnitt = Schnitt;
	m_MaTree.A_mat = A_mat;
	m_MaTree.Work = Work;

	HTREEITEM OldHItem = NULL;

	CDialog::OnInitDialog();
	CBitmap             bitmap;
	UINT                nID;

	pImageList = new CImageList();
	short AnzBmp = IDB_BMTREELAST - IDB_BMTREEFIRST;
	pImageList->Create(16, 16, ILC_MASK, AnzBmp, 2);
	for (nID = IDB_BMTREEFIRST; nID <= IDB_BMTREELAST; nID++)  
	{
		bitmap.LoadBitmap(nID);
		pImageList->Add(&bitmap, (COLORREF)0xFFFFFF);
		bitmap.DeleteObject();
	}

	m_MaTree.SetImageList(pImageList, TVSIL_NORMAL);

	pImageList = new CImageList();
	AnzBmp = IDB_BMTREELAST - IDB_BMTREEFIRST;
	pImageList->Create(16, 16, ILC_MASK, AnzBmp, 2);
	for (nID = IDB_BMTREEFIRST; nID <= IDB_BMTREELAST; nID++)  
	{
		bitmap.LoadBitmap(nID);
		pImageList->Add(&bitmap, (COLORREF)0xFFFFFF);
		bitmap.DeleteObject();
	}

	m_MaTree.SetImageList(pImageList, TVSIL_NORMAL);

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
		LFont.CreatePointFont (85, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
		LFont.CreatePointFont (95, _T("Dlg"));
	}
	int di = 1;
	m_MaTree.Init ();
    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (0, 0, 0, 0);
    CtrlGrid.SetCellHeight (15);
//    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetFontCellHeight (this, &Font);
    CtrlGrid.SetGridSpace (5, 0);  //Spaltenabstand und Zeilenabstand

	CCtrlInfo *c_LMaTree = new CCtrlInfo (&m_LMaTree, 1, 1, 1, 1);
	CtrlGrid.Add (c_LMaTree);

	CCtrlInfo *c_MaTree = new CCtrlInfo (&m_MaTree, 0, 2, DOCKRIGHT, DOCKBOTTOM);
	CtrlGrid.Add (c_MaTree);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);
	m_MaTree.SetFont (&LFont);

	CtrlGrid.Display ();
//	m_MaTree.Read (0);// wird in CPage2.PageUpdate aufgerufen

	return FALSE;
}
HBRUSH CMaTree::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}

	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return CDialog::OnCtlColor (pDC, pWnd,nCtlColor);
}


void CMaTree::OnSize (UINT nType, int cx, int cy)
{
		CRect rect (0, 0, cx, cy);
		CtrlGrid.pcx = 0;
		CtrlGrid.pcy = 0;
		CtrlGrid.DlgSize = &rect;
		CtrlGrid.Move (0, 0);
		CtrlGrid.DlgSize = NULL;
}

BOOL CMaTree::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{
		case WM_KEYDOWN :

			if (pMsg->wParam == VK_SPACE)
			{
				Zerm->PAGE2_WRITE = TRUE;
				FixVarSwitchen ();
				return TRUE;
			}
			if (pMsg->wParam == VK_F3)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
					//Page1->NextRec ();
				}
				else
				{
					//Page1->LastRec ();
				}
				return TRUE;

			}

			else if (pMsg->wParam == VK_F2)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
					//Page1->PriorRec ();
				}
				else
				{
					//Page1->FirstRec ();
				}
				return TRUE;

			}

			if (pMsg->wParam == VK_F5)
			{
				PageUpdate->Back ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F7)
			{
				Delete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Zerm->ZerlTreeErlauben = Zerm->TYP_MATERIAL; 
				PageUpdate->Write (); 
				return TRUE;
			}

/*
			else if (pMsg->wParam == VK_F12)
			{
				Update->Write ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
				if (Basis->Choice != NULL)
				{
					if (Basis->Choice->IsWindowVisible ())
					{
						Basis->Choice->ShowWindow (SW_HIDE);
					}
					else
					{
						Basis->Choice->ShowWindow (SW_SHOWNORMAL);
					}
				}
			}
			else if (pMsg->wParam == VK_F9)
			{
				return TRUE;
			}
*/
	}
	return FALSE;
}


BOOL CMaTree::Write (BOOL YesNo)
{
	BOOL ret = TRUE;
	Zerm->PAGE2_WRITE = FALSE;
	if (YesNo == TRUE)
	{
		ret = m_MaTree.ZerlTreeDataToDB ();
		CString Date;
		CStrFuncs::SysDate (Date);
		Schnitt->ToDbDate (Date, &Schnitt->schnitt.zer_dat);
		Schnitt->ToDbDate (Date, &Schnitt->schnitt.kalk_dat);
		Schnitt->dbupdate();
	}
	return ret;
}

void CMaTree::Update (short flgSave)
{
	if (flgSave == 7) //L�schen
	{
		Delete ();
	}
	if (flgSave == 22 || flgSave == 21 || flgSave == 20 || flgSave == 7 || flgSave == 23) //Umlasten (auch nach dem l�schen)
	{
		short a_typ = Zerm->zerm.a_typ;
		long mat = Zerm->zerm.zerm_mat;
		long aus_mat = Zerm->zerm.zerm_aus_mat;
		short aus_a_typ = Zerm->zerm.zerm_aus_a_typ;
		m_MaTree.ZermToZerlTreeData();  

		if (flgSave == 21)
		{
			m_MaTree.FWPNeuAufbauen();
		}
		if (flgSave == 22)
		{
			m_MaTree.FWP_aus_AVarb();
		}
		if (flgSave == 23)
		{
			m_MaTree.Vk_Laden();
		}
		m_MaTree.Umlasten();  

		m_MaTree.UpdateTree (flgSave);
		Zerm->zerm.zerm_mat = mat;
		Zerm->zerm.zerm_aus_mat = aus_mat;
		Zerm->zerm.zerm_aus_a_typ = aus_a_typ;
		Zerm->zerm.a_typ = a_typ;
		m_MaTree.ZerlTreeDataToZerm(1);  
		return;
	}
	if (flgSave == 101) //N�chstes Item holen
	{
		m_MaTree.NextItem();
	}
	if (flgSave == 102) //Vorheriges Item holen
	{
		m_MaTree.PrevItem();
	}
	if (flgSave == CLICKED_KOST_DETAILS) //Kosten  hervorheben
	{
		m_MaTree.TreeHervorheben(CLICKED_KOST_DETAILS,m_MaTree.m_hWnd);
	}
	if (flgSave == CLICKED_VK_DETAILS) //VK-Artikel  hervorheben
	{
		m_MaTree.TreeHervorheben(CLICKED_VK_DETAILS,m_MaTree.m_hWnd);
	}
	if (flgSave == CLICKED_SINGLE_EXPAND) // nur 1.Ebene und single expand
	{
		m_MaTree.TreeHervorheben(CLICKED_SINGLE_EXPAND,m_MaTree.m_hWnd);
	}
	if (flgSave == 10)
	{
		m_MaTree.ZerlTreeDataToDB ();
	}
	else if (flgSave == 2 || flgSave == 3)
	{
//		m_MaTree.ModifyStyle( TVS_CHECKBOXES, 0 );
//		if (flgSave == 3) m_MaTree.ModifyStyle( 0, TVS_CHECKBOXES );
//		UINT i, uCount = m_MaTree.GetVisibleCount();
//		HTREEITEM hItem = m_MaTree.GetFirstVisibleItem();

		/***
		for (i=0;i < uCount;i++)
		{
			ASSERT(hItem != NULL);
			m_MaTree.SetCheck(hItem, !m_MaTree.GetCheck(hItem));
			hItem = m_MaTree.GetNextVisibleItem(hItem);
		}
		*****/
		m_MaTree.UpdateTree (flgSave);
	}

	else if (flgSave == 1) //evt. neuesChildItem
	{
		HTREEITEM hItem = NULL;
//		hItem = m_MaTree.ZermToZerlTreeData();
		hItem = m_MaTree.ZermToItem();
		if (hItem == NULL)
		{
			if (Work->ReadMaterial(Zerm->zerm.mdn,Zerm->zerm.zerm_mat,Zerm->zerm.a_typ,Schnitt->cfg_PreiseAus_a_kalkpreis) == 0)
			{
				Zerm->zerm.zerm_gew = (double) 0;
				Zerm->zerm.zerm_gew_ant = (double) 0;
				Zerm->zerm.mat_o_b = (double) 0;
				Zerm->zerm.fwp =  0;
				Zerm->zerm.dm_100 = (double) 0;
				Zerm->zerm.teilkosten_abs = (double) 0;
				Zerm->zerm.vollkosten_abs = (double) 0;
				Zerm->zerm.hk_teilk = (double) 0;
				Zerm->zerm.hk_vollk = (double) 0;
				Zerm->zerm.sk_teilk = (double) 0;
				Zerm->zerm.sk_vollk = (double) 0;
				Zerm->zerm.fil_ek_teilk = (double) 0;
				Zerm->zerm.fil_ek_vollk = (double) 0;
				Zerm->zerm.fil_vk_teilk = (double) 0;
				Zerm->zerm.fil_vk_vollk = (double) 0;
				Zerm->zerm.gh1_teilk = (double) 0;
				Zerm->zerm.gh1_vollk = (double) 0;
				Zerm->zerm.gh2_teilk = (double) 0;
				Zerm->zerm.gh2_vollk = (double) 0;
				_tcscpy_s (Zerm->zerm.fix, "F");
				_tcscpy_s (Zerm->zerm.a_kz, " ");
				_tcscpy_s (Zerm->zerm.a_kz, Work->AVarb.a_varb.a_kz);

				if (Zerm->zerm.a_typ == Zerm->TYP_ZUTAT || 
					Zerm->zerm.a_typ == Zerm->TYP_VERPACKUNG ||
					Zerm->zerm.a_typ == Zerm->TYP_KOSTEN ||
					Zerm->zerm.a_typ == Zerm->TYP_AUFSCHLAG ||
					Zerm->zerm.a_typ == Zerm->TYP_AUFSCHLAG_EH)
				{
					//wenn noch kein VK-Artikel da, jetzt generieren
					Zerm->zerm.fwp =  (double) 0;
					Zerm->zerm.mat_o_b =  (double) 0;
					if (m_MaTree.VKVorhanden(Zerm->zerm.zerm_aus_mat) == FALSE)
					{
						if (Work->ReadMaterial(Zerm->zerm.mdn,Zerm->zerm.zerm_aus_mat,Zerm->TYP_VKARTIKEL,Schnitt->cfg_PreiseAus_a_kalkpreis) == 0)
						{
							short Zerm_a_typ = Zerm->zerm.a_typ;
							long Zerm_mat = Zerm->zerm.zerm_mat;
							Zerm->zerm.a_typ = Zerm->TYP_VKARTIKEL;
							Zerm->zerm.zerm_mat = Zerm->zerm.zerm_aus_mat;
							Zerm->zerm.zerm_aus_a_typ = Zerm->TYP_MATERIAL;
							_tcscpy_s (Zerm->zerm.kost_bz, Work->ABas.a_bas.a_bz1);
							m_MaTree.AddChild();
							hItem = m_MaTree.ZermToZerlTreeData();
							Zerm->zerm.a_typ = Zerm_a_typ;
							Zerm->zerm.zerm_mat = Zerm_mat;
							Zerm->zerm.zerm_aus_a_typ = Zerm->TYP_VKARTIKEL;
	   					    Work->ReadMaterial(Zerm->zerm.mdn,Zerm->zerm.zerm_mat,Zerm->zerm.a_typ,Schnitt->cfg_PreiseAus_a_kalkpreis);
						}
					}
				}


				if (Zerm->zerm.a_typ == Zerm->TYP_MATERIAL)
				{
					Zerm->zerm.fwp =  Work->AVarb.a_varb.fwp;
//					Work->AKalkPreis.a_kalkpreis;
					//ZuTun Hier Kosten Laden  vorher Lesen 
					Zerm->zerm.mat_o_b = Zerm->zerm.fwp * Schnitt->schnitt._wert_fwp;
					Zerm->zerm.hk_vollk = m_MaTree.HoleHKKosten(0.0,Zerm->zerm.a_kz,Zerm->zerm.list_ebene,Zerm->zerm.a_z_f,Zerm->zerm.zerm_teil);
					Zerm->zerm.hk_vollk += Zerm->zerm.mat_o_b;
					if (Zerm->zerm.zerm_mat == Zerm->zerm.zerm_aus_mat && Zerm->zerm.a_typ == Zerm->TYP_MATERIAL) //ausgangsmaterial //301009
					{
						Zerm->zerm.hk_vollk = Zerm->zerm.mat_o_b;
						Zerm->zerm.hk_teilk = Zerm->zerm.mat_o_b;
					}
					A_Kz = Zerm->zerm.a_kz;
					if (A_Kz == "ZV") Zerm->zerm.hk_vollk = (double) 0;
				}
				else if (Zerm->zerm.a_typ == Zerm->TYP_ZUTAT)
				{

					Zerm->zerm.fwp =  (double) 0;
					Zerm->zerm.mat_o_b =  Work->AKalkMat.a_kalk_mat.mat_o_b;
					Zerm->zerm.zerm_aus_a_typ = Zerm->TYP_VKARTIKEL; //301009 
				}
				else if (Zerm->zerm.a_typ == Zerm->TYP_VERPACKUNG)
				{
					Zerm->zerm.fwp =  (double) 0;
					Zerm->zerm.mat_o_b =  Work->AKalkMat.a_kalk_mat.mat_o_b;
					Zerm->zerm.zerm_aus_a_typ = Zerm->TYP_VKARTIKEL; //301009 
				}
				else if (Zerm->zerm.a_typ == Zerm->TYP_VKARTIKEL)
				{
					Zerm->zerm.fwp =  (double) 0;
					Zerm->zerm.mat_o_b =  (double) 0;
				}

				if (Zerm->zerm.a_typ == Zerm->TYP_KOSTEN || Zerm->zerm.a_typ == Zerm->TYP_AUFSCHLAG
														 || Zerm->zerm.a_typ == Zerm->TYP_AUFSCHLAG_EH)
				{
					Zerm->zerm.mat_o_b = Work->ABas.a_bas.sk_vollk;
					_tcscpy_s (Zerm->zerm.kost_bz, Work->ABas.a_bas.a_bz1);
					Zerm->zerm.zerm_aus_a_typ = Zerm->TYP_VKARTIKEL; //301009 
				}
			}



			m_MaTree.AddChild();
			hItem = m_MaTree.ZermToZerlTreeData();
		    if (Zerm->zerm.a_typ == Zerm->TYP_VKARTIKEL)
			{
				m_MaTree.Zerm_aus_a_typ_NeuSetzen (Zerm->zerm.zerm_mat);
			}
		}
		if (hItem != NULL)
		{
			if (m_MaTree.SelectItem(hItem))
			{
				m_MaTree.EnsureVisible(hItem);
				m_MaTree.ZerlTreeDataToZerm(1);
			}

		}
		m_MaTree.UpdateTree (flgSave);
// nicht aus DB!!!		m_MaTree.Read (flgSave);
	}
	else
	{
		/***
		if (Zerm->zerm.zerm_aus_mat == Zerm->zerm.zerm_mat)
		{
			Zerm->zerm.a_typ = Zerm->TYP_MATERIAL;  //Nicht richtig belegt, wenn von Seite 1
		}
		***/
		HTREEITEM hItem = m_MaTree.ZermToZerlTreeData();  
		m_MaTree.UpdateTree (flgSave);
		if (hItem != NULL)
		{
			if (m_MaTree.SelectItem(hItem))       // zum gleichschalten von Zerl und Tree wenn durch Seite 1 Zerm sich ge�ndert hat
			{
				m_MaTree.EnsureVisible(hItem);
				m_MaTree.ZerlTreeDataToZerm(1);
			}
		}


	}
}

void CMaTree::Read ()
{
	m_MaTree.Read (0);
	// 190509 nicht mehr m_MaTree.FWPNeuAufbauen();  //neu aus dem Artikelstamm berechnen aus den aktuellen Preisen 
	m_MaTree.Umlasten ();
	m_MaTree.TreeHervorheben(CLICKED_KOST_DETAILS,m_MaTree.m_hWnd);
	Zerm->SINGLE_EXPAND = FALSE;
	m_MaTree.TreeHervorheben(CLICKED_SINGLE_EXPAND,m_MaTree.m_hWnd);
	HTREEITEM RItem = m_MaTree.GetRootItem();
	if (RItem != NULL)
	{
		TreeView_Expand(m_MaTree.m_hWnd,RItem,TVE_EXPAND);

		if (m_MaTree.SelectItem(RItem))
		{
			m_MaTree.EnsureVisible(RItem);
			m_MaTree.ZerlTreeDataToZerm(1);
		}
	}


}



void CMaTree::OnTvnSelchangedMaTree(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
	*pResult = 0;

	TVITEM Item = pNMTreeView->itemNew;
	if (Zerm->ZerlTreeErlauben > 0)
	{
		if (ItemToA_typ() != Zerm->ZerlTreeErlauben)
		{
			m_MaTree.SelectItem(OldHItem);
			m_MaTree.EnsureVisible(OldHItem);
			return;
		}
		Zerm->ZerlTreeErlauben = 0;
	}

    ItemToZerm();
	m_MaTree.ZerlTreeDataToZerm(1);
    CopyOK = FALSE;
    PasteOK = FALSE;
	if (m_MaTree.GetChildItem (m_MaTree.GetSelectedItem ()) != NULL) CopyOK = TRUE;
	if (Zerm->zerm.a_typ == Zerm->TYP_MATERIAL) PasteOK = TRUE;
	for (std::vector<CUpdateEvent *>::iterator e = UpdateTab.begin ();
			                                       e != UpdateTab.end (); ++ e)
	{
		CUpdateEvent *event = *e;
		event->Update (104);  //> 100 nur f�r Zerl.cpp
	}
}
BOOL CMaTree::ItemToZerm ()
{
	HTREEITEM ht = m_MaTree.GetSelectedItem ();
	if (ht == NULL) return FALSE;
	_tcscpy_s (Zerm->zerm.variabel, "N");
	_tcscpy_s (Zerm->zerm.fix, "F");
	if (m_MaTree.GetCheck(ht))
	{
		_tcscpy_s (Zerm->zerm.variabel, "J"); 
		_tcscpy_s (Zerm->zerm.fix, "V"); 
	}

	CString Text = m_MaTree.GetItemText (ht);
	CToken t (Text, " ");
	int count = t.GetAnzToken ();
	if (count == 0) return FALSE;
	if (count == 1)
	{
		Zerm->zerm.zerm_mat = (long)CStrFuncs::StrToDouble (Text);
	}
	else
	{
		/***
		Zerm->zerm.zerm_mat = (long)CStrFuncs::StrToDouble (t.GetToken (1));
		if (Zerm->zerm.zerm_mat == 0.0)
		{
			Zerm->zerm.zerm_mat = (long)CStrFuncs::StrToDouble (t.GetToken (0));
		}
		***/
		Zerm->zerm.zerm_mat = (long)CStrFuncs::StrToDouble (t.GetToken (0));
		Zerm->zerm.a_typ = Zerm->TYP_MATERIAL;
		if (Zerm->zerm.zerm_mat == 0.0)
		{
			CString CTyp = t.GetToken (0);
			if (CTyp == "Z") Zerm->zerm.a_typ = Zerm->TYP_ZUTAT;
			if (CTyp == "P") Zerm->zerm.a_typ = Zerm->TYP_VERPACKUNG;
			if (CTyp == "K") Zerm->zerm.a_typ = Zerm->TYP_KOSTEN;
			if (CTyp == "A-GH") Zerm->zerm.a_typ = Zerm->TYP_AUFSCHLAG;
			if (CTyp == "A-EH") Zerm->zerm.a_typ = Zerm->TYP_AUFSCHLAG_EH;
			if (CTyp == "Verkauf:") Zerm->zerm.a_typ = Zerm->TYP_VKARTIKEL;
			Zerm->zerm.zerm_mat = (long)CStrFuncs::StrToDouble (t.GetToken (1));
		}
	}      
//	Hier brauche ich noch das Ausgangsmaterial !!!
	HTREEITEM htp = m_MaTree.GetParentItem (ht);
	Text = m_MaTree.GetItemText (htp);
	CToken tp (Text, " ");
	count = tp.GetAnzToken ();
	if (count == 0) //ist selber das Ausgangsmaterial
	{
		if (Schnitt->schnitt.schnitt_mat == Zerm->zerm.zerm_mat)
		{
			Zerm->zerm.zerm_aus_mat = Zerm->zerm.zerm_mat;
			Zerm->zerm.zerm_aus_a_typ = Zerm->zerm.a_typ;
			return TRUE;
		}
		return TRUE;
	}
	if (count == 1)
	{
		Zerm->zerm.zerm_aus_mat = (long)CStrFuncs::StrToDouble (Text);
		Zerm->zerm.zerm_aus_a_typ = Zerm->TYP_MATERIAL;
	}
	else
	{
//200810		Zerm->zerm.zerm_aus_a_typ = m_MaTree.TypKennzeichen2ATyp (tp.GetToken (0));
//200810		Zerm->zerm.zerm_aus_mat = (long)CStrFuncs::StrToDouble (tp.GetToken (1));
//200810		if (Zerm->zerm.zerm_aus_mat == 0.0)
//200810		{
//200810			Zerm->zerm.zerm_aus_mat = (long)CStrFuncs::StrToDouble (tp.GetToken (0));
//200810			Zerm->zerm.zerm_aus_a_typ = Zerm->TYP_MATERIAL;
//200810		}
		//200810 A
		Zerm->zerm.zerm_aus_a_typ = Zerm->TYP_MATERIAL;
		Zerm->zerm.zerm_aus_mat = (long)CStrFuncs::StrToDouble (tp.GetToken (0));    
		if (Zerm->zerm.zerm_aus_mat == 0.0)
		{
			Zerm->zerm.zerm_aus_mat = (long)CStrFuncs::StrToDouble (tp.GetToken (1));
			Zerm->zerm.zerm_aus_a_typ = m_MaTree.TypKennzeichen2ATyp (tp.GetToken (0));
		}
		//200810 E
	}
	return TRUE;

}

BOOL CMaTree::HasFocus ()
{
	CWnd *Control = GetFocus ();
	if (Control != &m_MaTree) return FALSE;
	return TRUE;
}

void CMaTree::OnCopy ()
{
	short SchnittNeu = -1;
	if (!HasFocus ()) return;
	HTREEITEM ht = m_MaTree.GetSelectedItem ();
 	if (ht == NULL) return;
	if (m_MaTree.GetChildItem (ht) == NULL) return;
	if (ItemToZerm() == TRUE)
	{
		CString Message;
		Message.Format (_T("SchnittNr=%d=%d=%d=%d=%d"), SchnittNeu,
				Schnitt->schnitt.mdn,
				Zerm->zerm.zerm_mat,
				Zerm->zerm.a_typ,
				Zerm->zerm.zerm_aus_mat);
		CStrFuncs::ToClipboard (Message, CF_TEXT);	
		Work->CopySchnitt(Schnitt->schnitt.mdn,Schnitt->schnitt.schnitt);
		


		CString Text;
/*
		Text.Format("Element : %d %s :\n\nkann jetzt an einer anderen Stelle, (Men� Datei->Einf�gen)\n"
				"in einer anderen Schnittf�hrung (Vor dem Kopieren diese Schnittf�hrung aufrufen und an entsprechender Stelle im Zerlegebaum mit Menue Datei->Einf�gen),\n"
				"oder als neue Schnittf�hrung benutzt werden (Men�: Datei->Neu, dann neue SchnittNr eintragen) " 
				,Zerm->zerm.zerm_mat,Zerm->zerm.kost_bz);
			MessageBox (Text.GetBuffer(), 
		        NULL, MB_ICONINFORMATION);
*/
		Text.Format("Element : %d %s :\n\nkann jetzt an einer anderen Stelle, (Men� Datei->Einf�gen)\n"
				"in einer anderen Schnittf�hrung (Vor dem Kopieren diese Schnittf�hrung aufrufen und an entsprechender Stelle im Zerlegebaum mit Menue Datei->Einf�gen),\n"
				"oder als neue Schnittf�hrung benutzt werden\n\n  " 
				"Element in eine neue Schnittf�hrung einsetzen ?"
				,Zerm->zerm.zerm_mat,Zerm->zerm.kost_bz);
		int ret = MessageBox (Text.GetBuffer(),NULL, MB_YESNO | MB_ICONQUESTION); 
		if  (ret == IDYES) 
		{
			POSITION pos;
			CWinApp* pApp = AfxGetApp();
			pos = pApp->GetFirstDocTemplatePosition ();
			CDocTemplate *ZerlInstance = NULL; 
			CDocTemplate *dt = pApp->GetNextDocTemplate (pos);
			ZerlInstance = dt;
			CDocument *dc = dt->CreateNewDocument ();
			if (dc !=NULL)
			{
				dc->SetTitle (_T("Kopie Zerlegung"));
			}
			CFrameWnd *Frame = dt->CreateNewFrame (dc, NULL);
			if (Frame != NULL)
			{
				dt->InitialUpdateFrame (Frame, NULL, TRUE);
			}
		}
	}
}
void CMaTree::Delete ()
{
	if (!HasFocus ()) return;
	Zerm->PAGE2_WRITE = TRUE;
	DeleteRow ();
}

void CMaTree::DeleteRow ()
{
	HTREEITEM ht = m_MaTree.GetSelectedItem ();
 	if (ht == NULL) return;
	if (m_MaTree.GetChildItem (ht) != NULL) return;

	if (ItemToZerm() == TRUE)
	{
		if (Zerm->zerm.zerm_mat == Zerm->Zerlegeverlust && Zerm->zerm.a_typ == Zerm->TYP_MATERIAL) return;
		if (m_MaTree.DestroyTreeData (ht) == TRUE)
		{
			Work->DelZermMatV(Schnitt->schnitt.mdn,Schnitt->schnitt.schnitt,Zerm->zerm.zerm_aus_mat,Zerm->zerm.zerm_mat,Zerm->zerm.a_typ);
			m_MaTree.DeleteItem (ht);
			m_MaTree.GrobteilBer ();
// 			m_MaTree.Umlasten(); geht hier nicht 
// 			m_MaTree.ZerlVerlBer ();  geht hier nicht , deshalb  event->update(105), hier wird umlasten aus zerl aufgerufen

		}
	}
	ItemToZerm();
	m_MaTree.ZerlTreeDataToZerm(1);
	for (std::vector<CUpdateEvent *>::iterator e = UpdateTab.begin ();
			                                       e != UpdateTab.end (); ++ e)
	{
		CUpdateEvent *event = *e;
		event->Update (105); // > 100 nur event f�r Zerl.cpp
	}

}
BOOL CMaTree::IstItemZerlegt ()
{
	HTREEITEM ht = m_MaTree.GetSelectedItem ();
	if (ht == NULL) return FALSE;
	if (m_MaTree.ItemHasChildren(ht)) return TRUE;
	return FALSE;
}
short CMaTree::ItemToA_typ ()
{
	short a_typ = 0;
	ItemToZerm ();
	a_typ = Zerm->zerm.a_typ;
	if (a_typ == Zerm->TYP_VERPACKUNG)
	{
		a_typ = Zerm->TYP_ZUTAT;
	}
	if (a_typ == Zerm->TYP_AUFSCHLAG)
	{
		a_typ = Zerm->TYP_KOSTEN;
	}
	if (a_typ == Zerm->TYP_AUFSCHLAG_EH)
	{
		a_typ = Zerm->TYP_KOSTEN;
	}

	return a_typ;
}
void CMaTree::FixVarSwitchen ()
{
//1.Zerm holen
    ItemToZerm();
	m_MaTree.ZerlTreeDataToZerm(0);

//	if (Zerm->zerm.a_typ != Zerm->TYP_MATERIAL && Zerm->zerm.a_typ != Zerm->TYP_VKARTIKEL) return ;
	if (Zerm->zerm.a_typ != Zerm->TYP_MATERIAL ) return ;
	/* 190509
	if (Zerm->zerm.zerm_mat == Zerm->zerm.zerm_aus_mat && Zerm->zerm.a_typ == Zerm->TYP_MATERIAL)
	{
		return;
	}
	*/

 //2.Fix-Kennz. �ndern
	CString CFix;
	CFix.Format (_T("%s"),Zerm->zerm.fix);
	if (CFix == "V")
	{
		CFix = "F"; 
		_tcscpy_s (Zerm->zerm.variabel, "N");
	}
	else if (CFix == "F") 
	{
		CFix = "V"; 
		_tcscpy_s (Zerm->zerm.variabel, "J");
	}

	_tcscpy_s (Zerm->zerm.fix, CFix.GetBuffer ());
//3. Zerm in Tree zur�ckschreiben
		m_MaTree.ZermToZerlTreeData();  
		m_MaTree.ZerlTreeDataToZerm(1); //fwp zur�ckschreiben
		if (CFix == "V")
		{
			_tcscpy_s (Zerm->zerm.variabel, "J");
		}
		else if (CFix == "F") 
		{
			_tcscpy_s (Zerm->zerm.variabel, "N");
		}

//		m_MaTree.Umlasten();  //geht hier nicht , daher update->event()  
		for (std::vector<CUpdateEvent *>::iterator e = UpdateTab.begin ();
			                                       e != UpdateTab.end (); ++ e)
		{
			CUpdateEvent *event = *e;
			event->Update (105); // > 100 nur event f�r Zerl.cpp
		}


		m_MaTree.UpdateTree (0);

// 4. �nderungen bekanntmachen


	for (std::vector<CUpdateEvent *>::iterator e = UpdateTab.begin ();
			                                       e != UpdateTab.end (); ++ e)
	{
		CUpdateEvent *event = *e;
		event->Update (0);
	}
}
void CMaTree::OnNMDblclkZtree(NMHDR *pNMHDR, LRESULT *pResult)
{
	// FixVarSwitchen ();
 	*pResult = 0;
}
void CMaTree::OnNMRclkZtree(NMHDR *pNMHDR, LRESULT *pResult)
{
 
	*pResult = 0;
}
void CMaTree::OnNMClickZtree(NMHDR *pNMHDR, LRESULT *pResult)
{
	int di = 0;
	OldHItem = m_MaTree.GetSelectedItem ();
	CString Text = m_MaTree.GetItemText (OldHItem);
	if (OldHItem == NULL) 
	{

	}
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	*pResult = 0;
}
BOOL CMaTree::Print (HWND m_hwnd)
{
	WorkPrint.PrintZerl(m_hwnd,Schnitt->schnitt.mdn,Schnitt->schnitt.schnitt);
	return TRUE;
}
