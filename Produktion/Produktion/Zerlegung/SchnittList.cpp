#include "StdAfx.h"
#include "SchnittList.h"

CSchnittList::CSchnittList(void)
{
	schnitt = 0;
	schnitt_txt = _T("");
	schnitt_mat = _T("");
}

CSchnittList::CSchnittList(long schnitt, LPTSTR schnitt_txt, LPTSTR schnitt_mat)
{
	this->schnitt  = schnitt;
	this->schnitt_txt    = schnitt_txt;
	this->schnitt_mat    = schnitt_mat;
}

CSchnittList::~CSchnittList(void)
{
}
