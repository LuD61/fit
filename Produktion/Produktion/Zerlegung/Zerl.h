#pragma once
#include "StaticButton.h"
#include "afxwin.h"
#include "CtrlGrid.h"
#include "PageUpdate.h"
#include "UpdateEvent.h"
#include "UniFormField.h"
#include "FormTab.h"
#include "NumEdit.h"
#include "TextEdit.h"
#include "Page1.h"
#include "zerm.h"
#include "schnitt.h"
#include "a_mat.h"
#include "a_bas.h"
#include "kost_art_zerl.h"
#include "ptabn.h"
#include "MaTree.h"
#include "ChoiceA.h"
#include "ChoiceKostArt.h"
#include "WorkDB.h"


// CZerl-Dialogfeld
#define IDC_ZERLSAVE 3011
#define IDC_MATCHOICE 3012

class CZerl : public CDialog, public CUpdateEvent
{
	DECLARE_DYNAMIC(CZerl)

public:
	CZerl(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CZerl();
	CCtrlGrid CtrlGrid;
	CCtrlGrid MatGrid;
	CCtrlGrid MaterialGrid;
	CCtrlGrid ZerlegenGrid;
	CCtrlGrid ZutGrid;
	CCtrlGrid KostGrid;
	CCtrlGrid VkGrid;
	CCtrlGrid ErgebnisGrid;

	CMaTree Matree;
	CFont Font;
	CFont FontBig;
	CFormTab Form;
	CFormTab ZutForm;
	CFormTab Mat1Form;
	CFormTab Mat2Form;
	CFormTab Mat3Form;
	CFormTab Mat4Form;
	CFormTab Mat5Form;
	CFormTab KostForm;
	CFormTab VkForm;
	COLORREF DlgBkColor;
	HBRUSH DlgBrush;
	virtual void Update (short);
	virtual void Read (void);
	virtual BOOL OnReturn ();
	BOOL TestMat ();
	BOOL UpdateZermMatV ();
	void TreeDown ();
	void TreeUp ();
	void DisableFelder ();
	void UpdateList ();
	void DeleteRow ();

	CButton m_MatChoice;
	WORKDB_CLASS *Work;

	long OldMat; 
	long OldAusMat; 
	short OldA_typ;
	BOOL flgMatGeaendert;






// Dialogfelddaten
	enum { IDD = IDD_ZERL };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();    // DDX/DDV-Unterstützung
    virtual BOOL PreTranslateMessage(MSG* pMsg);
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 
	virtual void OnSize (UINT nType, int cx, int cy);



	DECLARE_MESSAGE_MAP()
public:
	void KostArtSpeichern (void);
	CPage1 *Page1;
	CPageUpdate *PageUpdate;

	ZERM_CLASS *Zerm;
	SCHNITT_CLASS *Schnitt;
	A_MAT_CLASS *A_mat;
	A_BAS_CLASS *A_bas;
	KOST_ART_ZERL_CLASS kost_art_zerl;
	PTABN_CLASS Ptabn;
    CStaticButton m_Save;
	afx_msg void OnSave ();
	virtual BOOL Write ();
	CStatic m_LAusMat;
	CNumEdit m_AusMat;
	CTextEdit m_AusMatName;
	CNumEdit m_Mat;
	CTextEdit m_MatName;
	CStatic m_LMat;
	CStatic m_MatGroup;
	CStatic m_LGew;
	CNumEdit m_Gew;
	CNumEdit m_GewAnt;
	CStatic m_LGewAnt;
	CNumEdit m_MatoB;
	CTextEdit m_Kennzeichen;
	CNumEdit m_Zeitfaktor;
	CStatic m_LMatoB;
	CNumEdit m_Wert100Kg;
	CStatic m_LWert100Kg;
	CStatic m_LFwp;
	CStatic m_LZeitfaktor;
	CButton m_ChkGew;
	CButton m_ChkGewAnt;
	CButton m_ChkMatoB;
	CButton m_ChkKennzeichen;
	CButton m_ChkZeitfaktor;
	CButton m_ChkWert100Kg;
	CButton m_ChkFwp;
	CStatic m_MaterialGroup;
	CNumEdit m_Fwp;
	CComboBox m_MatCombo;
	CComboBox m_PreisCombo;
	CButton m_Chk_Variabel;
	CButton m_Chk_FV;
	CStatic m_ZutatGroup;
	CStatic m_LZutZugabe;
	CButton m_ChkZutZugabe;
	CNumEdit m_ZutZugabe;
	CButton m_ChkZutPreis;
	CStatic m_LZutPreis;
	CNumEdit m_ZutPreis;
	CButton m_ChkZutWert100kg;
	CStatic m_LZutWert100kg;
	CNumEdit m_ZutWert100kg;
	CStatic m_KostenGroup;
	CStatic m_LKostBezeichnung;
	CTextEdit m_KostBezeichnung;
	CStatic m_LKostAufschlag;
	CNumEdit m_KostAufschlag;
	CStatic m_LKostWert;
	CNumEdit m_KostWert;




	CString Where;
	CChoiceA *ChoiceMat;
	CChoiceKostArt *ChoiceKostArt;
	BOOL ModalChoice;
	BOOL AChoiceStatM;
	CString SearchA;
	void OnMatchoice();
    void OnKostArtChoice(); 
	BOOL ReadMaterial(long);
	void Umlasten(short dflg);
	void FormGet();
	void FormShow();
	void FillMatCombo ();
	void FillPreisCombo ();
	void ShowMaterialGrid(BOOL fShow);
	void ShowZutatenGrid(BOOL fShow);
	void ShowVerpackungGrid(BOOL fShow);
	void ShowGridZutat(BOOL fShow);
	void ShowKostenGrid(BOOL fShow);
	void ShowAufschlagGrid(BOOL fShow);
	void ShowGridKost(BOOL fShow);
	void ShowGridVk(BOOL fShow);
	short ZermA_KzToA_typ(void);
public:
	afx_msg void OnBnClickedChkGew();
	afx_msg void OnBnClickedChkGewAnt();
	afx_msg void OnBnClickedChkMatoB();
	afx_msg void OnBnClickedChkWert100kg();
	afx_msg void OnBnClickedChkKennzeichen();
	afx_msg void OnBnClickedChkZeitfaktor();
	afx_msg void OnBnClickedChkFwp();
	afx_msg void OnBnClickedChkVariabel();
	afx_msg void OnEnChangeGew();
	afx_msg void OnEnSetfocusGew();
	afx_msg void OnEnKillfocusGew();
	afx_msg void OnEnSetfocusAusMat();
	afx_msg void OnEnKillfocusAusMat();
	afx_msg void OnEnSetfocusMat();
	afx_msg void OnEnKillfocusMat();
public:
	afx_msg void OnBnClickedChkFv();
	CMaTreeCtrl m_MaTree;
	CButton m_ChkZutBold;

	CButton m_ButtonZerlegen;
	CStaticButton m_EnterErgebnis;
	afx_msg void OnBnClickedButtonzerlegen();
	afx_msg void OnEnKillfocusGewAnt();
	afx_msg void OnEnKillfocusWertKg();
	afx_msg void OnEnKillfocusWert100kg();
	afx_msg void OnEnKillfocusFwp();
	afx_msg void OnCbnSelchangeMatCombo();
	afx_msg void OnEnKillfocusZutZugabe();
	afx_msg void OnEnKillfocusZutPreis();
	afx_msg void OnEnKillfocusZutWert100kg();
	afx_msg void OnStnClickedLzutWert100kg();
	afx_msg void OnBnClickedChkZutBold();
	afx_msg void OnEnSetfocusGewAnt();
	afx_msg void OnEnSetfocusWertKg();
	afx_msg void OnEnSetfocusWert100kg();
	afx_msg void OnEnSetfocusFwp();
	afx_msg void OnEnSetfocusZutZugabe();
	afx_msg void OnEnSetfocusZutPreis();
	afx_msg void OnEnSetfocusZutWert100kg();
	afx_msg void OnEnSetfocusKostBezeichnung();
	afx_msg void OnEnSetfocusKostAufschlag();
	afx_msg void OnEnSetfocusKostAufschlagGes();
	afx_msg void OnBnHotItemChangeChkWert100kg(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedChkZutZugabe();
	afx_msg void OnBnClickedChkZutPreis();
	afx_msg void OnBnClickedChkZutWert100kg();
public:
	afx_msg void OnStnClickedLwertKg();
public:
	CButton m_ChkWert;
public:
	afx_msg void OnBnClickedChkWert();
public:
	CStatic m_LWert;
public:
	CNumEdit m_Wert;
public:
	afx_msg void OnEnSetfocusWert();
public:
	CButton m_ChkVk;
public:
	afx_msg void OnBnClickedChkVk();
public:
	CStatic m_LVk;
public:
	CNumEdit m_Vk;
public:
	afx_msg void OnEnSetfocusVk();
public:
	CButton m_ChkZutWert;
public:
	afx_msg void OnBnClickedChkZutWert();
public:
	CStatic m_LZutWert;
public:
	CNumEdit m_ZutWert;
public:
	CButton m_ChkZutGew;
public:
	afx_msg void OnBnClickedChkZutGew();
public:
	afx_msg void OnEnSetfocusZutWert();
public:
	CStatic m_LZutGew;
public:
	CNumEdit m_ZutGew;
public:
	afx_msg void OnEnSetfocusZutGew();
public:
	CStatic m_LKostFix;
public:
	CNumEdit m_KostFix;
public:
	afx_msg void OnEnSetfocusKostFix();
public:
	CStatic m_VkartikelGroup;
public:
	CStatic m_ErgebnisGroup;
public:
	CStatic m_LMatob;
public:
	CStatic m_LPrZutat;
public:
	CStatic m_LPrVpk;
public:
	CStatic m_LPrKosten;
public:
	CStatic m_LPrGh;
public:
	CStatic m_LPrEh;
public:
	CEdit m_Matob;
public:
	CEdit m_PrZutat;
public:
	CEdit m_PrVpk;
public:
	CEdit m_PrKosten;
public:
	CEdit m_PrGh;
public:
	CEdit m_PrEh;
public:
	CEdit m_LGesWert;
	CNumEdit m_GesWert;
	CEdit m_LDeckungA;
	CEdit m_LDeckungP;
	CNumEdit m_DeckungA;
	CNumEdit m_DeckungP;
	CEdit m_LGesEuro;
	CEdit m_LDeckungEuro;
	CEdit m_LDeckungProz;
public:
	afx_msg void OnEnSetfocusMatob();
public:
	afx_msg void OnEnSetfocusPrZutat();
public:
	afx_msg void OnEnSetfocusPrVpk();
public:
	afx_msg void OnEnSetfocusPrKosten();
public:
	afx_msg void OnEnSetfocusPrGh();
public:
	afx_msg void OnEnSetfocusPrEh();
public:
	CButton m_ChkSingleExpand;
public:
	afx_msg void OnBnClickedChkSingleExpand();
public:
	CButton m_ChkVkBold;
public:
	afx_msg void OnBnClickedChkVkBold();
public:
	afx_msg void OnCbnSelchangePreisCombo();
public:
	afx_msg void OnEnSetfocusZeitfaktor();
public:
	CEdit m_Grundpreis;
public:
	CStatic m_LGrundpreis;
public:
	afx_msg void OnEnKillfocusGrundpreis();
};
