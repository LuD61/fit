#pragma once

class CPageUpdate
{
public:
	CPageUpdate(void);
	~CPageUpdate(void);
	virtual void Show ();
	virtual void Get ();
	virtual void Back ();
	virtual BOOL Write ();
	virtual void Page2Write ();
	virtual void Delete ();
	virtual void UpdateAll ();
};
