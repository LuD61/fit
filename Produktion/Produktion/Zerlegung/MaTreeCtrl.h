#pragma once
#include "afxcmn.h"
#include "a_bas.h"
#include "a_mat.h"
#include "a_varb.h"
#include "a_kalk_mat.h"
#include "a_kalkhndw.h"
#include "zerm.h"
#include "k.h"
#include "schnitt.h"
#include "ZerlTreeData.h"
#include "WorkDB.h"
#include "Zerlegung.h"
#include "ipr.h"
#include <vector>

class CMaTreeCtrl :
	public CTreeCtrl
{
public:

//	int AusMatCursor;
	HTREEITEM ArtItem;
	HTREEITEM selectedItem;
	A_BAS_CLASS *A_bas;
	A_MAT_CLASS *A_mat;
	ZERM_CLASS *Zerm;
	K k;
	WORKDB_CLASS *Work;
	SCHNITT_CLASS *Schnitt;
//	CZerlTreeData *ZerlTreeData;
    std::vector<CZerlTreeData *> ZerlTreeData;
    std::vector<CZerlTreeGt *> ZerlTreeGt;
    std::vector<CZerlTreeGt2 *> ZerlTreeGt2;
    std::vector<CZerlTreeMatKost *> ZerlTreeMatKost;
    std::vector<CZerlTreeVkArtikel *> ZerlTreeVkArtikel;
	CMaTreeCtrl(void);
	~CMaTreeCtrl(void);
	void Init ();
    HTREEITEM AddRootItem (LPSTR, int, int);
    HTREEITEM AddChildItem (HTREEITEM, LPSTR, int, int, int , DWORD);
    BOOL UpdateItem (HTREEITEM, LPSTR, int, int, int , DWORD);
    void SetTreeStyle (DWORD);
	BOOL Read (short);
	BOOL ReadATree ();
	BOOL ReadZermTree (HTREEITEM ChildItem);
	void SetZerlTree (HTREEITEM hItem, int idx);
	CZerlTreeData *GetZerlTree (HTREEITEM hItem);
	void DestroyTreeData(void); 
	BOOL TreeDataVorhanden(void); 
	void DestroyZerlTreeGt(void); 
	void DestroyZerlTreeGt2(void); 
	void DestroyZerlTreeMatKost(void); 
	void DestroyZerlTreeVkArtikel(void); 
	BOOL DestroyTreeData(HTREEITEM hItem); 
	BOOL GetZerlTreeData (HTREEITEM);
	BOOL UpdateTree (short);
	BOOL TreeHervorheben (short Aktion,HWND HandleTreeView);
	void SetNewStyle(long , BOOL);
	HTREEITEM ZermToZerlTreeData (void);
	void FWPNeuAufbauen (void);
	void FWP_aus_AVarb (void);
	void Vk_Laden (void);
void Zerm_aus_a_typ_NeuSetzen (double Zerm_aus_mat);
	BOOL VKVorhanden (long aus_mat);
	BOOL KorrigiereATypVK(void);
	HTREEITEM ZermToItem (void);
	BOOL ZerlTreeDataToZerm (short flgMitFwp);
	BOOL ZerlVerlBer (HTREEITEM hItem);
	BOOL ZerlVerlBer (void);
	void ZutKostBer (HTREEITEM hItem, double mat_gew, double mat_hk_vollk);
	BOOL GrobteilBer (HTREEITEM hItem);
	BOOL GrobteilBer (void);
	BOOL MatKostenBer (HTREEITEM hItem);
	BOOL VkArtikelBer (HTREEITEM hItem,HTREEITEM ParentItem,HTREEITEM ChildItem);
	BOOL ZerlegeverlustBmpSetzen(void);
	BOOL ItemToZerm (HTREEITEM ht);
	double ItemToGew (HTREEITEM ht);
	double ItemToGewAnt (HTREEITEM ht);
	double ItemTo_k_ausZerlTreeData (HTREEITEM ht);
	double SummeDm100 (void);
	void Umlasten (void);
	BOOL IsZerlVerl (HTREEITEM ht);
	BOOL ZerlTreeDataToDB (void);
	BOOL PruefeZerm (void);
	void AddChild (void);
	void NextItem (void);
	void PrevItem (void);
	BOOL UpdateZermMatV (void);
	HTREEITEM ZerlegtAus (HTREEITEM hItem);
	CString BestimmeZerm_teil (HTREEITEM hItem);
	short ItemToZermTeil (HTREEITEM ht);
	CString ATyp2TypKennzeichen (short);
	short TypKennzeichen2ATyp (CString CTyp);
	CString HoleBezeichnung ();
	double HoleHKKosten(double kosten ,char* a_kz, short list_ebene, long a_z_f, char* zerm_teil);



	CImageList          *pImageList;


public:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnNMClickZtree(NMHDR *pNMHDR, LRESULT *pResult);
public:
	afx_msg void OnTvnKeydownZtree(NMHDR *pNMHDR, LRESULT *pResult);
public:
	afx_msg void OnNMDblclkZtree(NMHDR *pNMHDR, LRESULT *pResult);
protected:
	afx_msg void OnDestroy();

	//Indexe der ImageList
    int IID_Image ;
    int IID_GT ;
	int IID_Mat ;
    int IID_ZV ;
	int IID_Folderok ;
	int IID_ZVMinus;
	int IID_ZVLeer;
	int IID_ZV1;
	int IID_ZV2;
	int IID_ZV3;
	int IID_ZV4;
	int IID_ZV5;
	int IID_ZV6;
	int IID_ZV7;
	int IID_ZV8;
	int IID_ZV9;
	int IID_ZV10;
};
