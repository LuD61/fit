#include "StdAfx.h"
#include "zerllistctrl.h"
#include "DbUniCode.h"


CZerlListCtrl::CZerlListCtrl(void)
{
	A_bas = NULL;
	A_mat = NULL;
	Zerm = NULL;
	Zerm_mat_v = NULL;
	Schnitt = NULL;
}

CZerlListCtrl::~CZerlListCtrl(void)
{
}

void CZerlListCtrl::Init ()
{

	CBitmap bmp;
	bmp.CreateBitmap (1,20, 1, 0, NULL);
	BITMAP bm;
	int ret = bmp.GetBitmap (&bm);
	if (ret != 0)
	{
		image.Create (bm.bmWidth, bm.bmHeight, ILC_COLOR8, 0, 4);
		image.Add (&bmp, RGB (0,0,0));
	}
	SetImageList (&image, LVSIL_SMALL);   

	FillList = this;

	FillList.SetStyle (LVS_REPORT);
	FillList.SetExtendedStyle (LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_HEADERDRAGDROP);
//	m_PosList.VLines = 1;

	FillList.SetCol ("", 0, 0);
	/*
	FillList.SetCol ("Material", LMat, 70, LVCFMT_RIGHT);
	FillList.SetCol ("Bezeichnung",  2, 150);
	FillList.SetCol ("Preis",  3, 72, LVCFMT_RIGHT);
	FillList.SetCol ("Wert",  4, 72, LVCFMT_RIGHT);
	FillList.SetCol ("HK-Wert",  5, 72, LVCFMT_RIGHT);
	FillList.SetCol ("VK-Wert",  6, 72, LVCFMT_RIGHT);
	FillList.SetCol ("VK-Netto",  7, 72, LVCFMT_RIGHT);
	FillList.SetCol ("Gew.Anteil(%)  ",  8, 105, LVCFMT_RIGHT);
	FillList.SetCol ("Preis/100kg  ",  9, 100, LVCFMT_RIGHT);
	*/
//	ColType.Add (new CColType (3, CheckBox));
	EnableEdit = FALSE;
	Read ();
}

BOOL CZerlListCtrl::Read ()
{
	short A_typ = 0;
	/*
		FillList.DelCol (1);
		FillList.DelCol (2);
		FillList.DelCol (3);
		FillList.DelCol (4);
		FillList.DelCol (5);
		FillList.DelCol (6);
		FillList.DelCol (7);
		FillList.DelCol (8);
		FillList.DelCol (9);
		FillList.DelCol (10);
		*/




	if (Zerm->ZEIGE_KOSTEN == FALSE && Zerm->ZEIGE_VK == FALSE)
	{
		A_typ = Zerm->TYP_MATERIAL;  //Zeige nur Materialien, keine VK-Artikel
		LMat = 1;
		LBez = 2;
		LPreis = 3;
		LWert = 4;
		LHK_Wert = 5;
		LGewAnt = 6;
		LPr100 = 7;
		FillList.DelCol (1);
		FillList.SetCol ("Material", LMat, 70, LVCFMT_RIGHT);
		FillList.DelCol (2);
		FillList.SetCol ("Bezeichnung",  LBez, 150);
		FillList.DelCol (3);
		FillList.SetCol ("Preis",  LPreis, 72, LVCFMT_RIGHT);
		FillList.DelCol (4);
		FillList.SetCol ("Wert",  LWert, 72, LVCFMT_RIGHT);
		FillList.DelCol (5);
		FillList.SetCol ("HK-Wert",  LHK_Wert, 72, LVCFMT_RIGHT);
		FillList.DelCol (6);
		FillList.SetCol ("Gew.Anteil(%)  ",  LGewAnt, 105, LVCFMT_RIGHT);
		FillList.DelCol (7);
		FillList.SetCol ("Preis/100kg  ",  LPr100, 100, LVCFMT_RIGHT);
		FillList.DelCol (8);
		FillList.DelCol (9);
		FillList.DelCol (10);
	}
	else
	{
		A_typ = Zerm->TYP_VKARTIKEL;  //Zeige nur Vk-Artikel, keine Materialien
		LMat = 1;
		LBez = 2;
		LPreis = 3;
		LWert = 4;
		LHK_Wert = 5;
		LVK_Wert = 6;
		LGH_Preis = 7;
		LEH_Preis = 8;
		LGewAnt = 9;
		LPr100 = 10;
		FillList.DelCol (1);
		FillList.SetCol ("Artikel", LMat, 70, LVCFMT_RIGHT);
		FillList.DelCol (2);
		FillList.SetCol ("Bezeichnung",  LBez, 150);
		FillList.DelCol (3);
		FillList.SetCol ("Preis",  LPreis, 72, LVCFMT_RIGHT);
		FillList.DelCol (4);
		FillList.SetCol ("Wert",  LWert, 72, LVCFMT_RIGHT);
		FillList.DelCol (5);
		FillList.SetCol ("HK-Wert",  LHK_Wert, 72, LVCFMT_RIGHT);
		FillList.DelCol (6);
		FillList.SetCol ("VK-Wert",  LVK_Wert, 72, LVCFMT_RIGHT);
		FillList.DelCol (7);
		FillList.SetCol ("GH-Preis",  LGH_Preis, 72, LVCFMT_RIGHT);
		FillList.DelCol (8);
		FillList.SetCol ("EH-Preis",  LEH_Preis, 72, LVCFMT_RIGHT);
		FillList.DelCol (9);
//		FillList.SetCol ("Gew.Anteil(%)  ",  LGewAnt, 105, LVCFMT_RIGHT);
		FillList.DelCol (10);
//		FillList.SetCol ("Preis/100kg  ",  LPr100, 100, LVCFMT_RIGHT);
	}

		Zerm_mat_v->sqlin ((short *) &Schnitt->schnitt.mdn, SQLSHORT, 0);
		Zerm_mat_v->sqlin ((long *) &Schnitt->schnitt.schnitt, SQLLONG, 0);
		Zerm_mat_v->sqlin ((short *) &A_typ, SQLSHORT, 0);
		Zerm_mat_v->sqlout ((long *) &Zerm_mat_v->zerm_mat_v.zerm_mat, SQLLONG, 0);
//		Zerm_mat_v->sqlout ((long *) &Zerm_mat_v->zerm_mat_v.zerm_aus_mat, SQLLONG, 0);
		Zerm_mat_v->sqlout ((short *) &Zerm_mat_v->zerm_mat_v.a_typ, SQLSHORT, 0);
		Zerm_mat_v->sqlout ((long *) &Zerm_mat_v->zerm_mat_v.kost_mat, SQLLONG, 0);
	    Zerm_mat_v->sqlout ((TCHAR *) Zerm_mat_v->zerm_mat_v.kost_mat_bz,SQLCHAR,25);
		Zerm_mat_v->sqlout ((double *) &Zerm_mat_v->zerm_mat_v.dm_100, SQLDOUBLE, 0);
		Zerm_mat_v->sqlout ((double *) &Zerm_mat_v->zerm_mat_v.teilkosten_abs, SQLDOUBLE, 0);
		int cursor = Zerm_mat_v->sqlcursor (_T("select unique(zerm_mat),a_typ,kost_mat, ")
			_T("kost_mat_bz,sum(dm_100),sum(teilkosten_abs) from zerm_mat_v ")
			_T("where mdn = ? and schnitt = ? and a_typ = ? group by 1,2,3,4 order by 2,1 desc"));


 		Zerm_mat_v->sqlin ((short *) &Schnitt->schnitt.mdn, SQLSHORT, 0);
		Zerm_mat_v->sqlin ((long *) &Schnitt->schnitt.schnitt, SQLLONG, 0);
		Zerm_mat_v->sqlin ((long *) &Zerm_mat_v->zerm_mat_v.zerm_mat, SQLLONG, 0);
		Zerm_mat_v->sqlout ((long *) &Zerm_mat_v->zerm_mat_v.zerm_mat, SQLLONG, 0);
		Zerm_mat_v->sqlout ((short *) &Zerm_mat_v->zerm_mat_v.a_typ, SQLSHORT, 0);
		Zerm_mat_v->sqlout ((long *) &Zerm_mat_v->zerm_mat_v.kost_mat, SQLLONG, 0);
	    Zerm_mat_v->sqlout ((TCHAR *) Zerm_mat_v->zerm_mat_v.kost_mat_bz,SQLCHAR,25);
		Zerm_mat_v->sqlout ((double *) &Zerm_mat_v->zerm_mat_v.dm_100, SQLDOUBLE, 0);
		Zerm_mat_v->sqlout ((double *) &Zerm_mat_v->zerm_mat_v.teilkosten_abs, SQLDOUBLE, 0);
		int kost_cursor = Zerm_mat_v->sqlcursor (_T("select unique(zerm_mat),a_typ,kost_mat, ")
			_T("kost_mat_bz,sum(dm_100),sum(teilkosten_abs) from zerm_mat_v ")
			_T("where mdn = ? and schnitt = ?  and zerm_aus_mat = ? and a_typ in (2,3,4,5,6) group by 1,2,3,4 order by 2 "));


	if (cursor == -1) return FALSE;
	if (kost_cursor == -1) return FALSE;

	DeleteAllItems ();
	memcpy (&Zerm_mat_v->zerm_mat_v, &zerm_mat_v_null, sizeof (ZERM_MAT_V));

	Zerm_mat_v->sqlopen (cursor);
	int idx = 0;
	double dteilkosten_abs = (double) 0;
	double dkosten = (double) 0;
	double dzerm_gew = (double) 0;
	double dVkWert = (double) 0;

	while (Zerm_mat_v->sqlfetch (cursor) == 0)
	{
		dzerm_gew = (double) 0;
		Zerm_mat_v->zerm_mat_v.mdn = Schnitt->schnitt.mdn;
		Zerm_mat_v->zerm_mat_v.schnitt = Schnitt->schnitt.schnitt;
		CString CZerm;
		Zerm_mat_v->dbreadfirst ();
		if (Zerm->ZEIGE_VK == TRUE && Zerm_mat_v->zerm_mat_v.pr_vk_eh < 0.001) continue; 
		if (Zerm_mat_v->zerm_mat_v.a_typ == Zerm->TYP_MATERIAL || Zerm_mat_v->zerm_mat_v.a_typ == Zerm->TYP_ZUTAT)
		{
			A_mat->a_mat.mat = Zerm_mat_v->zerm_mat_v.zerm_mat;
			A_mat->dbreadfirst();
			A_bas->a_bas.a = A_mat->a_mat.a;
		}
		else
		{
			A_bas->a_bas.a = Zerm_mat_v->zerm_mat_v.zerm_mat;
		}
		A_bas->dbreadfirst();
		_tcscpy_s (Zerm_mat_v->zerm_mat_v._zerm_mat_bz1, A_bas->a_bas.a_bz1);

		FillList.InsertItem (idx, 0);
		CZerm.Format (_T("%ld"), Zerm_mat_v->zerm_mat_v.zerm_mat); 
		FillList.SetItemText (CZerm.GetBuffer (), idx, LMat);

		CString ZermBez = Zerm_mat_v->zerm_mat_v._zerm_mat_bz1;
		LPTSTR text = new TCHAR [(ZermBez.GetLength () + 1) * 2];
		_tcscpy (text, ZermBez.GetBuffer ());
		LPSTR pos = (LPSTR) text;
		CDbUniCode::DbToUniCode (text, pos);
		ZermBez = text;
		delete text;
		FillList.SetItemText (ZermBez.GetBuffer (), idx, LBez);


		CZerm.Format (_T("%.2f"), Zerm_mat_v->zerm_mat_v.mat_o_b); 
	  FillList.SetItemText (CZerm.GetBuffer (), idx, LPreis);
		CZerm.Format (_T("%.2f"), Zerm_mat_v->zerm_mat_v.teilkosten_abs); 
	  FillList.SetItemText (CZerm.GetBuffer (), idx, LWert);
	  dteilkosten_abs = Zerm_mat_v->zerm_mat_v.teilkosten_abs;
	  dkosten = (double) 0;

	  if (Zerm_mat_v->zerm_mat_v.hk_vollk == (double) 0)
	  {
			FillList.SetItemText ("", idx, 5);
	  }
	  else
	  {
			CZerm.Format (_T("%.2f"), Zerm_mat_v->zerm_mat_v.hk_vollk); 
			FillList.SetItemText (CZerm.GetBuffer (), idx, LHK_Wert);
	  }
	  if (Zerm_mat_v->zerm_mat_v.vk_pr == (double) 0)
	  {
			FillList.SetItemText ("", idx, LVK_Wert);
			FillList.SetItemText ("", idx,  LGH_Preis );
			FillList.SetItemText ("", idx,  LEH_Preis );
	  }
	  else
	  {
			CZerm.Format (_T("%.2f"), Zerm_mat_v->zerm_mat_v.vk_pr); 
			FillList.SetItemText (CZerm.GetBuffer (), idx, LVK_Wert);
			if (Zerm_mat_v->zerm_mat_v.zerm_gew == (double) 0) 
			{
				CZerm.Format (_T("%.2f"), (double) 0); 
			}
			else
			{
				CZerm.Format (_T("%.2f"), Zerm_mat_v->zerm_mat_v.vk_pr / Zerm_mat_v->zerm_mat_v.zerm_gew); 
				dzerm_gew = Zerm_mat_v->zerm_mat_v.zerm_gew;
			}
			FillList.SetItemText (CZerm.GetBuffer (), idx, LGH_Preis);
			CZerm.Format (_T("%.2f"), Zerm_mat_v->zerm_mat_v.pr_vk_eh); 
		    FillList.SetItemText (CZerm.GetBuffer (), idx, LEH_Preis);
	  }

		CZerm.Format (_T("%.2f"), Zerm_mat_v->zerm_mat_v.zerm_gew_ant); 
	  FillList.SetItemText (CZerm.GetBuffer (), idx, LGewAnt);
		CZerm.Format (_T("%.2f"), Zerm_mat_v->zerm_mat_v.dm_100); 
	  FillList.SetItemText (CZerm.GetBuffer (), idx, LPr100);
		idx ++; 

		if (Zerm->ZEIGE_KOSTEN == TRUE)
		{
		  Zerm_mat_v->sqlopen (kost_cursor);
		  while (Zerm_mat_v->sqlfetch (kost_cursor) == 0)
		  {
			FillList.InsertItem (idx, 0);
			FillList.SetItemText ("->", idx, LMat);

			CString ZermBez = Zerm_mat_v->zerm_mat_v.kost_mat_bz;
			LPTSTR text = new TCHAR [(ZermBez.GetLength () + 1) * 2];
			_tcscpy (text, ZermBez.GetBuffer ());
			LPSTR pos = (LPSTR) text;
			CDbUniCode::DbToUniCode (text, pos);
			ZermBez = text;
			delete text;
			FillList.SetItemText (ZermBez.GetBuffer (), idx, LBez);
	  	    FillList.SetItemText ("", idx, LPreis);
			CZerm.Format (_T("%.2f"), Zerm_mat_v->zerm_mat_v.teilkosten_abs); 
			FillList.SetItemText (CZerm.GetBuffer (), idx, LWert);
	  	    FillList.SetItemText ("", idx, LGH_Preis);
	  	    FillList.SetItemText ("", idx, LGewAnt);
	  	    FillList.SetItemText ("", idx, LPr100);
			if (Zerm_mat_v->zerm_mat_v.a_typ == Zerm->TYP_ZUTAT || 
				Zerm_mat_v->zerm_mat_v.a_typ == Zerm->TYP_VERPACKUNG || 
				Zerm_mat_v->zerm_mat_v.a_typ == Zerm->TYP_KOSTEN)
			{

				dkosten += Zerm_mat_v->zerm_mat_v.teilkosten_abs;
				CZerm.Format (_T("%.2f"), dteilkosten_abs + dkosten); 
	  		    FillList.SetItemText ("", idx, LHK_Wert);
	  		    FillList.SetItemText (CZerm.GetBuffer (), idx, LHK_Wert);
			}
			else if (Zerm_mat_v->zerm_mat_v.a_typ == Zerm->TYP_AUFSCHLAG)
			{
				dVkWert =dteilkosten_abs + dkosten + Zerm_mat_v->zerm_mat_v.teilkosten_abs;
				CZerm.Format (_T("%.2f"), dVkWert); 
	  		    FillList.SetItemText ("", idx, LHK_Wert);
	  		    FillList.SetItemText (CZerm.GetBuffer (), idx, LVK_Wert);
				CZerm.Format (_T("%.2f"), dVkWert / dzerm_gew); 
	  		    FillList.SetItemText (CZerm.GetBuffer (), idx, LGH_Preis);

			}
			else if (Zerm_mat_v->zerm_mat_v.a_typ == Zerm->TYP_AUFSCHLAG_EH)
			{
				dVkWert =dteilkosten_abs + dkosten + Zerm_mat_v->zerm_mat_v.teilkosten_abs;
				CZerm.Format (_T("%.2f"), dVkWert); 
	  		    FillList.SetItemText ("", idx, LHK_Wert);
	  		    FillList.SetItemText (CZerm.GetBuffer (), idx, LVK_Wert);
				CZerm.Format (_T("%.2f"), dVkWert / dzerm_gew); 
	  		    FillList.SetItemText (CZerm.GetBuffer (), idx, LEH_Preis);
			}
			else
			{
	  		    FillList.SetItemText (CZerm.GetBuffer (), idx, LHK_Wert);
	  		    FillList.SetItemText ("", idx, LVK_Wert);
			}
			idx ++;
		  }
		}

	}
	Zerm_mat_v->sqlclose (cursor);
	if (idx > 0) return TRUE;
	return FALSE;
}

