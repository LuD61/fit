#include "stdafx.h"
#include "ChoiceSchnitt.h"
#include "DbUniCode.h"
#include "Process.h"
#include "StrFuncs.h"

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

int CChoiceSchnitt::Sort1 = -1;
int CChoiceSchnitt::Sort2 = -1;
int CChoiceSchnitt::Sort3 = -1;
int CChoiceSchnitt::Sort4 = -1;
int CChoiceSchnitt::Sort5 = -1;
int CChoiceSchnitt::Sort6 = -1;
int CChoiceSchnitt::Sort7 = -1;
int CChoiceSchnitt::Sort8 = -1;
int CChoiceSchnitt::Sort9 = -1;
int CChoiceSchnitt::Sort10 = -1;
int CChoiceSchnitt::Sort11 = -1;

CChoiceSchnitt::CChoiceSchnitt(CWnd* pParent) 
        : CChoiceX(pParent)
{
	ptcursor = -1;
	schnittmatcursor = -1;
	Where = "";
//	Bean.ArchiveName = _T("AList.prp");
}

CChoiceSchnitt::~CChoiceSchnitt() 
{
	DestroyList ();
	if (ptcursor == -1) DbClass->sqlclose (ptcursor);
	if (schnittmatcursor == -1) DbClass->sqlclose (schnittmatcursor);
	ptcursor = -1;
	schnittmatcursor = -1;
}

void CChoiceSchnitt::DestroyList() 
{
	for (std::vector<CSchnittList *>::iterator pabl = SchnittList.begin (); pabl != SchnittList.end (); ++pabl)
	{
		CSchnittList *abl = *pabl;
		delete abl;
	}
    SchnittList.clear ();
}

void CChoiceSchnitt::FillList () 
{
    long  schnitt;
    TCHAR schnitt_txt [81];
    TCHAR schnitt_hin [201];
    long schnitt_mat;

	DestroyList ();
	CListCtrl *listView = GetListView ();
	ClearList (); 

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT  | LVS_EX_HEADERDRAGDROP);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

    SetWindowText (_T("Auswahl Schnittführungen"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
    SetCol (_T("Schnitt"),           1, 150, LVCFMT_RIGHT);
    SetCol (_T("Bezeichnung"),     2, 250);
    SetCol (_T("Ausg.Material"),     3, 250);
	SortRow = 1;

	if (SchnittList.size () == 0)
	{
		DbClass->sqlout ((long *)&schnitt,        SQLLONG, 0);
		DbClass->sqlout ((LPTSTR)  schnitt_txt,     SQLCHAR, sizeof (schnitt_txt));
		DbClass->sqlout ((LPTSTR)  schnitt_hin,     SQLCHAR, sizeof (schnitt_hin));
		DbClass->sqlout ((long *) &schnitt_mat,    SQLLONG, 0);
		CString Sql  = (_T("select schnitt, schnitt_txt, schnitt_hin,schnitt_mat "
						   "from schnitt where schnitt > 0 "));
		Sql += " ";
		Sql += Where;
		int cursor = DbClass->sqlcursor (Sql.GetBuffer ());
//		memset (a_bz3, 0, sizeof (a_bz3));
		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) schnitt_txt;
			CDbUniCode::DbToUniCode (schnitt_txt, pos);
			pos = (LPSTR) schnitt_hin;
			CDbUniCode::DbToUniCode (schnitt_hin, pos);

			CString SchnittMats;
            this->schnitt_mat = schnitt_mat;
            GetSchnittMatBez ();
			SchnittMats = schnitt_mat_bz;

			CSchnittList *abl = new CSchnittList (schnitt, 
			                                schnitt_txt,
			                                SchnittMats.GetBuffer ());
			SchnittList.push_back (abl);
//			memset (a_bz3, 0, sizeof (a_bz3));
		}
		DbClass->sqlclose (cursor);
		Load ();
	}

	for (std::vector<CSchnittList *>::iterator pabl = SchnittList.begin (); pabl != SchnittList.end (); ++pabl)
	{
		CSchnittList *abl = *pabl;
		CString Schnitt;
		Schnitt.Format (_T("%ld"), abl->schnitt); 

        int ret = InsertItem (i, -1);
        ret = SetItemText (Schnitt.GetBuffer (), i, 1);
        ret = SetItemText (abl->schnitt_txt.GetBuffer (), i, 2);
        ret = SetItemText (abl->schnitt_mat.GetBuffer (), i, 3);
        i ++;
    }

    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort4 = -1;
    Sort5 = -1;
    Sort6 = -1;
    Sort7 = -1;
    Sort8 = -1;
    Sort9 = -1;
    Sort10 = -1;
    Sort11 = -1;
    Sort (listView);
}


void CChoiceSchnitt::NumLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        LPTSTR p = aText.GetBuffer ();
        p = _tcstok (p, _T(" "));
        aText.Format (_T("%s"), p);
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceSchnitt::BezLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        aText.TrimRight ();
        CString iText = ListBox->GetItemText (i, 1);
        iText.TrimRight ();
        aText.Format (_T("%s %s"), aText.GetBuffer (), iText.GetBuffer ());
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceSchnitt::SearchNum (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 1);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}


void CChoiceSchnitt::SearchCol (CListCtrl *ListBox, LPTSTR Search, int col)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, col);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceSchnitt::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
	if ((EditText.Find (_T("*")) != -1) || EditText.Find (_T("?")) != -1)
	{
		CChoiceX::SearchMatch (EditText);
		return;
	}

    switch (SortRow)
    {
        case 0 :
//             SearchNum (ListBox, EditText.GetBuffer (16));
             break;
        case 1 :
             SearchNum (ListBox, EditText.GetBuffer (8));
             break;
        default :
             EditText.MakeUpper ();
             SearchCol (ListBox, EditText.GetBuffer (), SortRow);
             break;
    }
}

int CChoiceSchnitt::GetPtBez (LPSTR ptitem, LPTSTR ptwert, LPTSTR ptbez)
{
	if (ptcursor == -1)
	{
		DbClass->sqlout ((LPTSTR) this->ptbez,  SQLCHAR, sizeof (this->ptbez));
		DbClass->sqlout ((LPTSTR) this->ptbezk, SQLCHAR, sizeof (this->ptbezk));
		DbClass->sqlin ((LPTSTR) this->ptitem, SQLCHAR, sizeof (this->ptitem));
		DbClass->sqlin  ((LPTSTR) this->ptwert, SQLCHAR, sizeof (this->ptwert));
	    ptcursor = DbClass->sqlcursor (_T("select ptbez,ptbezk from ptabn where ptitem = ? ")
							           _T("and ptwert = ?"));
	}
	_tcscpy (this->ptbez, _T(""));
	_tcscpy (this->ptbezk, _T(""));
	_tcscpy (this->ptitem, ptitem);
	_tcscpy (this->ptwert, ptwert);
	DbClass->sqlopen (ptcursor);
	int dsqlstatus = DbClass->sqlfetch (ptcursor);

	_tcscpy (ptbez, this->ptbez);
	LPSTR pos = (LPSTR) ptbez;
    CDbUniCode::DbToUniCode (ptbez, pos);
	return dsqlstatus;
}


int CChoiceSchnitt::GetSchnittMatBez ()
{
	if (schnittmatcursor == -1)
	{
		DbClass->sqlout ((LPTSTR) this->schnitt_mat_bz, SQLCHAR, sizeof (this->schnitt_mat_bz));
		DbClass->sqlin  ((long *) &this->schnitt_mat, SQLLONG, 0);
	    schnittmatcursor = DbClass->sqlcursor (_T("select a_bz1 from a_bas,a_mat where a_mat.mat = ? and a_mat.a = a_bas.a "));
	}
	_tcscpy (this->schnitt_mat_bz, _T(""));
	DbClass->sqlopen (schnittmatcursor);
	int dsqlstatus = DbClass->sqlfetch (schnittmatcursor);

	LPSTR pos = (LPSTR) schnitt_mat_bz;
    CDbUniCode::DbToUniCode (schnitt_mat_bz, pos);
	return dsqlstatus;
}


int CALLBACK CChoiceSchnitt::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   int SortRow = CmpRow;
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   if (SortRow == 0)
   {
//	return strcmp(strItem2.GetBuffer (0), strItem1.GetBuffer (0)) * Sort1;
   }
   else if (SortRow == 1)
   {

	   double li1 = _tstof (strItem1.GetBuffer ());
	   double li2 = _tstof (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort2;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort2);
	   }
	   return 0;
   }
   else if (SortRow == 2)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort3;
   }
   else if (SortRow == 3)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort4;
   }
   else if (SortRow == 4)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort5;
   }
   else if (SortRow == 5)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort6;
   }
   else if (SortRow == 6)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort7;
   }
   else if (SortRow == 7)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort8;
   }
   else if (SortRow == 8)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort9;
   }
   else if (SortRow == 9)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort10;
   }
   else if (SortRow == 10)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort11;
   }
   return 0;
}

void CChoiceSchnitt::Sort (CListCtrl *ListBox)
{
    CmpRow = SortRow;
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
	int SortPos = 0;
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
			  if (Sort2 < 0) SortPos = 1;
              break;
        case 2:
              Sort3 *= -1;
			  if (Sort3 < 0) SortPos = 1;
              break;
        case 3:
              Sort4 *= -1;
			  if (Sort4 < 0) SortPos = 1;
              break;
        case 4:
              Sort5 *= -1;
			  if (Sort5 < 0) SortPos = 1;
              break;
        case 5:
              Sort6 *= -1;
			  if (Sort6 < 0) SortPos = 1;
              break;
        case 6:
              Sort7 *= -1;
			  if (Sort7 < 0) SortPos = 1;
              break;
        case 7:
              Sort8 *= -1;
			  if (Sort8 < 0) SortPos = 1;
              break;
        case 8:
              Sort9 *= -1;
			  if (Sort9 < 0) SortPos = 1;
              break;
        case 9:
              Sort10 *= -1;
			  if (Sort10 < 0) SortPos = 1;
              break;
        case 10:
              Sort11 *= -1;
			  if (Sort11 < 0) SortPos = 1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CSchnittList *abl = SchnittList [i];
		   
		   abl->schnitt        = (long) _tstoi (ListBox->GetItemText (i, 1));
		   abl->schnitt_txt       = ListBox->GetItemText (i, 5);
		   abl->schnitt_mat       = ListBox->GetItemText (i, 6);
	}


	for (int i = 1; i <= 3; i ++)
	{
		SetItemSort (i, 2);
	}

	SetItemSort (SortRow, SortPos);

}

void CChoiceSchnitt::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = SchnittList [idx];
}

CSchnittList *CChoiceSchnitt::GetSelectedText ()
{
	CSchnittList *abl = (CSchnittList *) SelectedRow;
	return abl;
}

void CChoiceSchnitt::SaveSelection (CListCtrl *ListBox)
{
	SelectList.clear ();
    int idx   = ListBox->GetNextItem (-1, LVNI_SELECTED);
	while (idx != -1)
	{
		SelectList.push_back (SchnittList[idx]);
	    idx   = ListBox->GetNextItem (idx, LVNI_SELECTED);
	}
}

void CChoiceSchnitt::SetDefault ()
{
	CChoiceX::SetDefault ();
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    ListBox->SetColumnWidth (0, 0);
    ListBox->SetColumnWidth (1, 75);
//	SetColFmt (1, LVCFMT_RIGHT);
    ListBox->SetColumnWidth (2, 150);
    ListBox->SetColumnWidth (3, 150);
    ListBox->SetColumnWidth (4, 150);
    ListBox->SetColumnWidth (5, 150);
    ListBox->SetColumnWidth (6, 150);
    ListBox->SetColumnWidth (7, 150);
    ListBox->SetColumnWidth (8, 150);
    ListBox->SetColumnWidth (9, 150);
    ListBox->SetColumnWidth (10,250);

}

void CChoiceSchnitt::OnEnter ()
{
	CProcess p;

    int idx   = m_List.GetNextItem (-1, LVNI_SELECTED);
	if (idx >= 0)
	{
		CString Message;
		CString cA = m_List.GetItemText (idx, 1);  
		double a = CStrFuncs::StrToDouble (cA.GetBuffer ());
		Message.Format (_T("SchnittNr=%.0lf"), a);
		ToClipboard (Message);
	}
	p.SetCommand (_T("Zerlegung"));
	HANDLE Pid = p.Start ();
}