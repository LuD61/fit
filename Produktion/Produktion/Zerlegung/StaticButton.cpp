#include "StdAfx.h"
#include "Resource.h"
#include "staticbutton.h"
#include "Bmap.h"

#define IDC_HAND 32649

IMPLEMENT_DYNCREATE(CStaticButton, CStatic)
// IMPLEMENT_DYNCREATE(CStaticButton, CButton)

CStaticButton::CStaticButton(void)
{
	ButtonCursor = FALSE;
	nID = 0;
	TextColor = RGB (0, 0, 255); //StaticButtons jetzt alle Blau!
	ColorSet = FALSE;
	NoUpdate = FALSE;
	Orientation = Center;
	Hand = LoadCursor (NULL,  MAKEINTRESOURCE (IDC_HAND));
}

CStaticButton::~CStaticButton(void)
{
}

BEGIN_MESSAGE_MAP(CStaticButton, CStatic)
// BEGIN_MESSAGE_MAP(CStaticButton, CButton)
	ON_WM_MOUSEMOVE ()
	ON_WM_LBUTTONDOWN ()
	ON_WM_SETCURSOR ()
	ON_WM_KEYDOWN ()
	ON_WM_SETFOCUS ()
	ON_WM_KILLFOCUS ()
END_MESSAGE_MAP()

BOOL CStaticButton::Create(LPCTSTR lpszText, DWORD dwStyle, const RECT& rect,
                          CWnd* pParentWnd,   UINT nID)
{
	    this->nID = nID;
	    Style = dwStyle;
        dwStyle |= SS_OWNERDRAW | SS_NOTIFY;
//        dwStyle |= BS_OWNERDRAW | BS_NOTIFY;
		Arrow = LoadCursor (NULL,  IDC_ARROW);
//		Hand  = LoadCursor (NULL,  IDC_HAND);
//		Hand = LoadCursor (NULL,  MAKEINTRESOURCE (IDC_HAND));


		BOOL ret = CStatic::Create (lpszText, dwStyle, rect, pParentWnd, nID);
//		BOOL ret = CButton::Create (lpszText, dwStyle, rect, pParentWnd, nID);
		return ret;
}

void CStaticButton::SetToolTip (LPTSTR Text)
{
	BOOL ret = FALSE;

	if (!IsWindow (Tooltip.m_hWnd))
	{
		RECT rect = {0, 0, 330, 20};
		Tooltip.Create (Text, this, 0, rect, GetParent ());
	}
}

void CStaticButton::LoadBitmap (UINT Id)
{
	Bitmap.LoadBitmap (Id);
}


/*
BOOL CStaticButton::PreTranslateMessage(MSG* pMsg )
{
	switch (pMsg->message)
	{
	case WM_MOUSEMOVE :
		CPoint p;
		p.x = LOWORD (pMsg->lParam);
		p.y = HIWORD (pMsg->lParam);
		OnMouseMove ((UINT) pMsg->wParam, p);
        return TRUE;
	}
	return FALSE;
}
*/

void CStaticButton::SetBkColor (COLORREF BkColor)
{
	this->BkColor = BkColor;
	ColorSet = TRUE;
}

void CStaticButton::Draw (CDC& cDC)
{
	CString Text;
	GetWindowText (Text);
	CRect rect;
	CRect fillRect;
	CSize Size;
//	if (NoUpdate) return;

	NoUpdate = TRUE;
	GetClientRect (&rect);
	COLORREF Background;
	fillRect = rect;

	if (ColorSet)
	{
		Background = BkColor; 
		cDC.FillRect (&rect, &CBrush (Background));
		if (GetFocus () == this)
		{
			CPen Pen (PS_DOT, 1, RGB (0, 0, 0));
			CPen *oldPen = cDC.SelectObject (&Pen);
			cDC.Rectangle (&rect);
			cDC.SelectObject (oldPen);
			fillRect = rect;
			fillRect.left ++;
			fillRect.top ++;
			fillRect.right --;
			fillRect.bottom --;
			cDC.FillRect (&fillRect, &CBrush (Background));
		}
	}
	else
	{
		Background = BkColor; 
		cDC.FillRect (&rect, &CBrush (GetSysColor (COLOR_3DFACE)));
		ClientToScreen (&fillRect);
		GetParent ()-> ScreenToClient (&fillRect);
		if (GetFocus () == this)
		{
			CPen Pen (PS_DOT, 1, RGB (0, 0, 0));
			CPen *oldPen = cDC.SelectObject (&Pen);
			cDC.Rectangle (&rect);
			cDC.SelectObject (oldPen);
			fillRect.left ++;
			fillRect.top ++;
			fillRect.right --;
			fillRect.bottom --;
		}
	}

	if ((HBITMAP) Bitmap != NULL)
	{
		if (ButtonCursor)
		{
			cDC.FillRect (&rect, &CBrush (RGB (200, 200, 200)));
		}
		POINT Size;
		BMAP Bmap;
		Bmap.BitmapSize (cDC, (HBITMAP) Bitmap, &Size);
		int x = max (0, (rect.right - Size.x) / 2); 
		int y = max (0, (rect.bottom - Size.y) / 2); 

		Bmap.DrawBitmap (cDC, (HBITMAP) Bitmap, x, y);
		if (ButtonCursor)
		{
			CBrush Brush;
			Brush.CreateSolidBrush (RGB (0, 0, 0));
			cDC.FrameRect (&rect, &Brush);
		}
        return;
	}

	LPTSTR t = strtok (Text.GetBuffer (), "\n");
	int y = 0;
	int row = 0;
	while (t != NULL)
	{
		CString T = t;
		Size = cDC.GetTextExtent (T);
		y = row * Size.cy;
		row ++;
	    t = strtok (NULL, "\n");
	}
	if (row > 0)
	{
		y = row * Size.cy;
	}
    
	int start = max (0, (rect.bottom - y) / 2);

	t = strtok (Text.GetBuffer (), "\n");
	row = 0;
	while (t != NULL)
	{
		CString T = t;
		Size = cDC.GetTextExtent (T);
		int x = 0;
		if (Orientation == Center)
		{
			x = max (0, (rect.right - Size.cx) / 2);
		}
		else if (Orientation == Right)
		{
			x = max (0, (rect.right - Size.cx));
		}
		y = row * Size.cy + start;
		cDC.SetBkMode (TRANSPARENT);
		if (IsWindowEnabled ())
		{
			cDC.SetTextColor (TextColor);
			cDC.TextOut (x, y, t, (int) _tcslen (t));
		}
		else
		{
			cDC.SetTextColor (RGB (192, 192, 192));
			cDC.TextOut (x, y, t, (int) _tcslen (t));
			cDC.SetTextColor (RGB (255, 255, 255));
			cDC.TextOut (x + 1, y + 1, t, (int) _tcslen (t));
		}
		row ++;
	    t = strtok (NULL, "\n");
	}
	NoUpdate = FALSE;
}

void CStaticButton::DrawItem (LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	CDC cDC;
	cDC.Attach (lpDrawItemStruct->hDC);
	Draw (cDC);
}

void CStaticButton::SetButtonCursor (BOOL b)
{
	if (b)
	{
		if (ButtonCursor) return;
		ButtonCursor = TRUE;

        NMHDR hdr;
		hdr.hwndFrom = m_hWnd;
		hdr.idFrom = (UINT_PTR) m_hWnd;
		SetCapture ();
		::SetCursor (Hand);
		Invalidate ();
		Tooltip.Show ();
	}
	else
	{
		if (!ButtonCursor) return;
		ButtonCursor = FALSE;
		ReleaseCapture ();
		Invalidate ();
		Tooltip.Hide ();
	}
}

BOOL CStaticButton::InClient (CPoint p)
{
	RECT rect;

	if (!IsWindowEnabled ())
	{
		return FALSE;
	}
	ClientToScreen (&p);
	GetWindowRect (&rect);
	if ((p.x >= rect.left) && (p.x <= rect.right) &&
		(p.y >= rect.top) && (p.y <= rect.bottom))
	{
			return TRUE;
	}
	return FALSE;
}


void CStaticButton::SetButtonCursor (CPoint p)
{
	if (InClient (p))
	{
		SetButtonCursor (TRUE);
		return;
	}
	SetButtonCursor (FALSE);
}


void CStaticButton::OnMouseMove(UINT nFlags,  CPoint p)
{
	SetButtonCursor (p);
}

void CStaticButton::OnLButtonDown(UINT nFlags,  CPoint p)
{
	if (InClient (p))
	{
		GetParent ()->SendMessage (WM_COMMAND, MAKELONG (nID, 0), (LPARAM) m_hWnd);
		SetFocus ();
		if (IsWindow (Tooltip.m_hWnd))
		{
			Tooltip.Hide ();
		}
	}
}

BOOL CStaticButton::OnSetCursor(CWnd* pWnd, UINT nHitTest,  UINT message )
{
	if (ButtonCursor)
	{
		::SetCursor (Hand);
		return TRUE;
	}
	return FALSE;
}

void CStaticButton::OnKeyDown(UINT nChar,  UINT nRepCnt,  UINT nFlags)
{
	if (nChar == VK_SPACE)
	{
		GetParent ()->SendMessage (WM_COMMAND, MAKELONG (nID, 0), (LPARAM) m_hWnd);
	}
	if (IsWindow (Tooltip.m_hWnd))
	{
		Tooltip.ShowWindow (SW_HIDE);
	}
}

void CStaticButton::OnSetFocus(CWnd *cWnd)
{
	Invalidate ();
}

void CStaticButton::OnKillFocus(CWnd *cWnd)
{
	SetButtonCursor (FALSE);
	Invalidate ();
}
