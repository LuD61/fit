#ifndef _SCHNITTKOST_G_DEF
#define _SCHNITTKOST_G_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct SCHNITTKOST_G {
   short          mdn;
   long           schnitt;
   long           grundkost_art;
   double         grundkostenwert;
   long           grobkost_art;
   double         grobkostenwert;
};
extern struct SCHNITTKOST_G schnittkost_g, schnittkost_g_null;

#line 8 "schnittkostg.rh"

class SCHNITTKOST_G_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               SCHNITTKOST_G schnittkost_g; 
   			   char GrundKoartBez[25];
   			   char GrobKoartBez[25];
    	   SCHNITTKOST_G_CLASS () : DB_CLASS ()
               {
				   strcpy_s(GrundKoartBez,"");
				   strcpy_s(GrobKoartBez,"");
               }
};
#endif