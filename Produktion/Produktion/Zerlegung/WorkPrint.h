#ifndef _WORKPRINT_DEF
#define _WORKPRINT_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"
#include "DbUniCode.h"
#include "zerm_mat_v.h"
#include "a_bas.h"
#include "a_mat.h"
#include "a_varb.h"
#include "a_zut.h"
#include "a_kalk_mat.h"
#include "a_kalkn.h"
#include "a_kalkhndw.h"
#include "a_schl.h"
#include "zerm.h"
#include "mdn.h"
#include "adr.h"
#include "schnitt.h"
#include "StrFuncs.h"
#include "token.h"
//#include "cmbtl9.h"
#include "cmbtll11.h"


class WORKPRINT_CLASS : public DB_CLASS 
{
       private :
			void Felduebergabe(short kennung_feld,HJOB hJob,LPCSTR Feldname,LPCSTR Formatstring,void *Variable);
			void Variablendefinition (HJOB); 
			void Felderdefinition (HJOB);
			void VariablenUebergabe ( HJOB hJob, char szTemp2[], int nRecno  );
			void FelderUebergabe ( HJOB hJob, char szTemp2[], int nRecno  );
			void HoleFilename (HJOB);
			int GetRecCount (short mdn, long schnitt); 
			int LeseZerm (int); 
			int LeseFirstZerm (short mdn, long schnitt); 
       public :
               WORKPRINT_CLASS () : DB_CLASS ()
               {
//					MatCursor = -1;	
               }
               ~WORKPRINT_CLASS ()
               {
//				   if (MatCursor != -1)
//			       {
  //                         sqlclose (MatCursor);
	//			   }		
               }
       public :
		    void PrintZerl (HWND,short,long);
		    void EditZerl (HWND);
			TCHAR mat_bz [25];
			A_BAS_CLASS AusMatABas;
			A_BAS_CLASS ABas;
			A_BAS_CLASS SchlArtABas;
			A_VARB_CLASS AusMatAVarb;
			A_VARB_CLASS AVarb;
			A_SCHL_CLASS A_schl;
			A_ZUT_CLASS AZut;
			A_MAT_CLASS AMat;
			A_KALK_MAT_CLASS AKalkMat;
			A_KALKN_CLASS AKalkn;
			A_KALKHNDW_CLASS AKalkHndw;
			ZERM_CLASS Zerm;
			ZERM_MAT_V_CLASS Zerm_mat_v;
			SCHNITT_CLASS Schnitt;
			MDN_CLASS Mdn;
			ADR_CLASS Adr;


               
};
#endif
