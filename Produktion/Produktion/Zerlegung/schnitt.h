#ifndef _SCHNITT_DEF
#define _SCHNITT_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct SCHNITT {
   short          mdn;
   long           schnitt;
   TCHAR          lief_txt[49];
   short          kalk_art;
   short          kalk_bas;
   TCHAR          schnitt_txt[81];
   TCHAR          schnitt_hin[201];
   long           schnitt_mat;
   TCHAR          schnitt_stk[6];
   double         ek_pr;
   double         ek_gew;
   short          bearb_weg;
   DATE_STRUCT    zer_dat;
   DATE_STRUCT    kalk_dat;
   TCHAR          kalk_usr[9];
   short          stat;
   long           vergl_schnitt;
   short          delstatus;
   TCHAR          hdkl[5];
   double         fix_zeit;
   double         soll_zeit;
   double         vk_pr;
   long           schlachtkalk;
   double         schlachtart;
   double         ek;
   double         deckung_proz;
   double         summe_wert;
   double         menge_vt;
   double         menge_vm;
   double         menge_kk;
   double         wert_vt;
   double         wert_vm;
   double         wert_kk;
   short          koart_grund;
   short          koart_grob;
   short          koart_vm;
   short          koart_vt;
   short          koart_za;
   short          koart_min;
//Zusätzlich in die Schnitt-struktur (ohne DB)
   TCHAR          _material_bz[25];
   TCHAR          _schlachtkalk_bz[25];
   TCHAR          _schlachtart_bz[25];
   double         _basis_fwp;
   double         _wert_fwp;
   double		_SumPreis;
   double		_DeckungA;
   double		_DeckungP;
   long		    _AnzVariabel;
   short		_PreisArt;
   double          _grund_kosten;
   double          _grob_kosten;
   double          _vm_kosten;
   double          _vt_kosten;
   double          _za_kosten;
   double          _min_kosten;

   double		_SumPreisHK;
   double		_DeckungAHK;
   double		_DeckungPHK;
   double		_SumHK;

   double		_SumPreisSK;
   double		_DeckungASK;
   double		_DeckungPSK;
   double		_SumSK;

   double		_SumPreisFIL_EK;
   double		_DeckungAFIL_EK;
   double		_DeckungPFIL_EK;
   double		_SumFIL_EK;

   double		_SumPreisFIL_VK;
   double		_DeckungAFIL_VK;
   double		_DeckungPFIL_VK;
   double		_SumFIL_VK;

};
extern struct SCHNITT schnitt, schnitt_null;

#line 8 "schnitt.rh"


class SCHNITT_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               SCHNITT schnitt;  
			   long PrGrStuf;
			   short kalk_bas;
	   	       short TYP_MAT_O_B;
	   	       short TYP_HK_PR;
	   	       short TYP_SK_PR;
	   	       short TYP_FIL_EK;
	   	       short TYP_FIL_VK;
	   	       short TYP_VK_PRGR_0;
	   	       short TYP_VK_PRGR_1;
	   	       short TYP_VK_PRGR_2;
			   short cfg_NurGTZerlegen;
			   short cfg_FWP_berechnen;
			   short cfg_ek_aus_schnitt;
			   short cfg_NurVariableMaterialienSpeichern;
			   short cfg_GTKostenAddieren;
			   short cfg_PreiseAus_a_kalkpreis;
			   short cfg_Multimandant;
			   double SollDeckung;
   			   char GrundKoartBez[25];
   			   char GrobKoartBez[25];
   			   char VMKoartBez[25];
   			   char VTKoartBez[25];
   			   char ZAKoartBez[25];
               SCHNITT_CLASS () : DB_CLASS () 
               {
		   	       TYP_MAT_O_B = 1;
				   TYP_HK_PR = 2;
				   TYP_SK_PR = 3;
		   	       TYP_FIL_EK = 4;
		   	       TYP_FIL_VK = 5;
				   PrGrStuf = 9999;
				   kalk_bas = 1;
				   cfg_NurGTZerlegen = 0;
				   cfg_FWP_berechnen = 0;
				   cfg_ek_aus_schnitt = 0;
				   cfg_NurVariableMaterialienSpeichern = 1;
				   cfg_GTKostenAddieren = 0;
				   cfg_PreiseAus_a_kalkpreis = 0; 
				   cfg_Multimandant = 1; 
				   SollDeckung = 0.0;
				   strcpy_s(GrundKoartBez,"");
				   strcpy_s(GrobKoartBez,"");

               }
};
#endif
