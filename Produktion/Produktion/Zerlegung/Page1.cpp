// Page1.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "Zerlegung.h"
#include "Page1.h"
#include "UniFormField.h"
#include "StrFuncs.h"
#include "Util.h"
#include "Token.h"
#include ".\page1.h"


// CPage1-Dialogfeld 

IMPLEMENT_DYNAMIC(CPage1, CDbPropertyPage)
CPage1::CPage1()
	: CDbPropertyPage(CPage1::IDD)
{

	Choice = NULL;
	ChoiceMat = NULL;
	ChoiceSchlA = NULL;
	ModalChoice = FALSE;


	Cfg.SetProgName( _T("Zerlegung"));
	DlgBrush = NULL;
	ChoiceMdn = NULL;
	ChoiceKalk = NULL;
	Frame = NULL;
	HeadControls.Add (&m_Mdn);
	HeadControls.Add (&m_MdnChoice);
	HeadControls.Add (&m_Schnitt); 
//	HeadControls.Add (&m_SchnittChoice); 
	PosControls.Add (&m_SchnittTxt); 

	PosControls.Add (&m_Material); 
	PosControls.Add (&m_MaterialChoice); 
	PosControls.Add (&m_Gewicht); 
	PosControls.Add (&m_Ek); 
	PosControls.Add (&m_Schlachtkalk); 
	PosControls.Add (&m_SchlachtKalkChoice); 
	PosControls.Add (&m_Schlachtart); 
	PosControls.Add (&m_SchlachtArtChoice); 
	PosControls.Add (&m_Preis); 
	PosControls.Add (&m_HDKL); 
	PosControls.Add (&m_Anzahl); 
	PosControls.Add (&m_Hinweis); 
}

CPage1::~CPage1()
{
	Font.DeleteObject ();
	Mdn.dbclose ();
	if (ChoiceMdn != NULL)
	{
		delete ChoiceMdn;
		ChoiceMdn = NULL;
	}
	if (Choice != NULL)
	{
		delete Choice;
		Choice = NULL;
	}
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
	Keys.FirstPosition ();
	while ((f = (CFormField *) Keys.GetNext ()) != NULL)
	{
		delete f;
	}
	Zerm.dbclose ();
	Schnitt.dbclose ();
}

void CPage1::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LMDN, m_LMdn);
	DDX_Control(pDX, IDC_MDN, m_Mdn);
	DDX_Control(pDX, IDC_MDN_NAME, m_MdnName);
	DDX_Control(pDX, IDC_LSCHNITT, m_LSchnitt);
	DDX_Control(pDX, IDC_SCHNITT, m_Schnitt);
	DDX_Control(pDX, IDC_SCHNITT_TXT, m_SchnittTxt);
	DDX_Control(pDX, IDC_LHINWEIS, m_LHinweis);
	DDX_Control(pDX, IDC_HINWEIS, m_Hinweis);
	DDX_Control(pDX, IDC_AUSMAT_GROUP, m_AusmatGroup);
	DDX_Control(pDX, IDC_LMATERIAL, m_LMaterial);
	DDX_Control(pDX, IDC_LGEWICHT, m_LGewicht);
	DDX_Control(pDX, IDC_LEK, m_LEk);
	DDX_Control(pDX, IDC_LSCHLACHTKALK, m_LSchlachtkalk);
	DDX_Control(pDX, IDC_LSCHLACHTART, m_LSchlachtart);
	DDX_Control(pDX, IDC_LPREIS, m_LPreis);
	DDX_Control(pDX, IDC_INFO_GROUP, m_InfoGroup);
	DDX_Control(pDX, IDC_LHDKL, m_LHDKL);
	DDX_Control(pDX, IDC_LANZAHL, m_LAnzahl);
	DDX_Control(pDX, IDC_LBASISFWP, m_LBasisFWP);
	DDX_Control(pDX, IDC_LAM, m_LAm);
	DDX_Control(pDX, IDC_LVON, m_LVon);
	DDX_Control(pDX, IDC_GEWICHT, m_Gewicht);
	DDX_Control(pDX, IDC_EK, m_Ek);
	DDX_Control(pDX, IDC_SCHLACHTKALK, m_Schlachtkalk);
	DDX_Control(pDX, IDC_SCHLACHTART, m_Schlachtart);
	DDX_Control(pDX, IDC_PREIS, m_Preis);
	DDX_Control(pDX, IDC_HDKL, m_HDKL);
	DDX_Control(pDX, IDC_BASISFWP, m_BasisFWP);
	DDX_Control(pDX, IDC_BEARBAM, m_BearbAm);
	DDX_Control(pDX, IDC_BEARBVON, m_BearbVon);
	DDX_Control(pDX, IDC_MATERIAL_TXT, m_MaterialTxt);
	DDX_Control(pDX, IDC_SCHLACHTKALK_TXT, m_SchlachtkalkTxt);
	DDX_Control(pDX, IDC_MATERIAL, m_Material);
	DDX_Control(pDX, IDC_SCHLACHTART_TXT, m_SchlachtartTxt);
	DDX_Control(pDX, IDC_ANZAHL, m_Anzahl);
	DDX_Control(pDX, IDC_BEARB_GROUP, m_BearbGroup);
}


BEGIN_MESSAGE_MAP(CPage1, CPropertyPage)
	ON_BN_CLICKED(IDC_MDNCHOICE , OnMdnchoice)
	ON_BN_CLICKED(IDC_SCHNITTCHOICE , OnSchnittchoice)
	ON_BN_CLICKED(IDC_MATERIALCHOICE , OnMatchoice)
	ON_BN_CLICKED(IDC_SCHLACHTARTCHOICE , OnSchlArtchoice)
	ON_BN_CLICKED(IDC_SCHLACHTKALKCHOICE , OnSchlKalkchoice)
	ON_COMMAND (SELECTED, OnSchnittSelected)
	ON_COMMAND (CANCELED, OnSchnittCanceled)
	ON_COMMAND(ID_FILE_SAVE, OnFileSave)

	ON_EN_KILLFOCUS(IDC_MATERIAL, &CPage1::OnEnKillfocusMaterial)
	ON_EN_KILLFOCUS(IDC_SCHLACHTKALK, &CPage1::OnEnKillfocusSchlachtkalk)
	ON_EN_KILLFOCUS(IDC_SCHLACHTART, &CPage1::OnEnKillfocusSchlachtart)
	ON_EN_UPDATE(IDC_GEWICHT, &CPage1::OnEnUpdateGewicht)
	ON_EN_KILLFOCUS(IDC_GEWICHT, &CPage1::OnEnKillfocusGewicht)
	ON_EN_KILLFOCUS(IDC_EK, &CPage1::OnEnKillfocusEk)
	ON_EN_KILLFOCUS(IDC_PREIS, &CPage1::OnEnKillfocusPreis)
	ON_EN_SETFOCUS(IDC_EK, &CPage1::OnEnSetfocusEk)
	ON_EN_SETFOCUS(IDC_SCHNITT_TXT, &CPage1::OnEnSetfocusSchnittTxt)
	ON_EN_CHANGE(IDC_SCHNITT, &CPage1::OnEnChangeSchnitt)
	ON_EN_SETFOCUS(IDC_SCHNITT, &CPage1::OnEnSetfocusSchnitt)
END_MESSAGE_MAP()


// CPage1-Meldungshandler

BOOL CPage1::OnInitDialog()
{
	CPropertyPage::OnInitDialog();

	Mdn.opendbase (_T("bws"));

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
		lFont.CreatePointFont (85, _T("Courier"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
		lFont.CreatePointFont (95, _T("Courier New"));
	}

	CUtil::GetPersName (PersName);

	ReadCfg ();
	Zerm.Zerlegeverlust = Work.ZerlMat (); 

    Form.Add (new CFormField (&m_Mdn,EDIT,        (short *) &Mdn.mdn.mdn, VSHORT));
    Form.Add (new CUniFormField (&m_MdnName,EDIT, (LPTSTR) MdnAdr.adr.adr_krz, VCHAR));
    Form.Add (new CFormField (&m_Schnitt,EDIT,    (long *) &Schnitt.schnitt.schnitt, VLONG));
    Form.Add (new CUniFormField (&m_SchnittTxt,EDIT, (LPTSTR) Schnitt.schnitt.schnitt_txt, VCHAR));
    Form.Add (new CFormField (&m_Material,EDIT, (long *) &Schnitt.schnitt.schnitt_mat, VLONG));
    Form.Add (new CUniFormField (&m_MaterialTxt,EDIT, (LPTSTR) Schnitt.schnitt._material_bz, VCHAR));
    Form.Add (new CUniFormField (&m_Gewicht,EDIT, (double *) &Schnitt.schnitt.ek_gew, VDOUBLE,8,3));
    Form.Add (new CUniFormField (&m_Ek,EDIT, (double *) &Schnitt.schnitt.ek, VDOUBLE,6,2));
    Form.Add (new CUniFormField (&m_Schlachtkalk,EDIT, (long *) &Schnitt.schnitt.schlachtkalk, VLONG));
	Form.Add (new CUniFormField (&m_SchlachtkalkTxt,EDIT, (LPTSTR) Schnitt.schnitt._schlachtkalk_bz, VCHAR));
    Form.Add (new CUniFormField (&m_Schlachtart,EDIT, (double *) &Schnitt.schnitt.schlachtart, VDOUBLE,13,0));
	Form.Add (new CUniFormField (&m_SchlachtartTxt,EDIT, (LPTSTR) Schnitt.schnitt._schlachtart_bz, VCHAR));
    Form.Add (new CUniFormField (&m_Preis,EDIT, (double *) &Schnitt.schnitt.ek_pr, VDOUBLE,8,4));
    Form.Add (new CUniFormField (&m_HDKL,EDIT, (LPTSTR) Schnitt.schnitt.hdkl, VCHAR));
	Form.Add (new CUniFormField (&m_Anzahl,EDIT, (LPTSTR) Schnitt.schnitt.schnitt_stk, VCHAR));
	Form.Add (new CUniFormField (&m_BasisFWP,EDIT, (double *) &Schnitt.schnitt._basis_fwp, VDOUBLE,5,2));
	Form.Add (new CFormField (&m_BearbAm,EDIT, (DATE_STRUCT *) &Schnitt.schnitt.kalk_dat, VDATE));

	Form.Add (new CUniFormField (&m_BearbVon,EDIT, (LPTSTR) Schnitt.schnitt.kalk_usr, VCHAR));
	Form.Add (new CUniFormField (&m_Hinweis,EDIT, (LPTSTR) Schnitt.schnitt.schnitt_hin, VCHAR));

    Keys.Add (new CFormField (&m_Mdn,EDIT,        (short *) &Mdn.mdn.mdn, VSHORT));
    Keys.Add (new CFormField (&m_Schnitt,EDIT,    (long *) &Schnitt.schnitt.schnitt, VLONG));

    CtrlGrid.Create (this, 20, 20);
	CtrlGrid.SetFont (&Font);
    CtrlGrid.SetBorder (12, 20);
//    CtrlGrid.SetCellHeight (StdCellHeight);
    CtrlGrid.SetFontCellHeight (this, &Font);
//    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand
//Grid Mdn
	MdnGrid.Create (this, 2, 2);
    MdnGrid.SetBorder (0, 0);
    MdnGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Mdn = new CCtrlInfo (&m_Mdn, 0, 0, 1, 1);
	MdnGrid.Add (c_Mdn);
	CtrlGrid.CreateChoiceButton (m_MdnChoice, IDC_MDNCHOICE, this);
	CCtrlInfo *c_MdnChoice = new CCtrlInfo (&m_MdnChoice, 1, 0, 1, 1);
	MdnGrid.Add (c_MdnChoice);

//Grid Schnitt
	SchnittGrid.Create (this, 2, 2);
    SchnittGrid.SetBorder (0, 0);
    SchnittGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Schnitt = new CCtrlInfo (&m_Schnitt, 0, 0, 1, 1);
	SchnittGrid.Add (c_Schnitt);
	CtrlGrid.CreateChoiceButton (m_SchnittChoice, IDC_SCHNITTCHOICE, this);
	CCtrlInfo *c_SchnittChoice = new CCtrlInfo (&m_SchnittChoice, 1, 0, 1, 1);
	SchnittGrid.Add (c_SchnittChoice);

//Mandant
	CCtrlInfo *c_LMdn     = new CCtrlInfo (&m_LMdn, 1, 0, 1, 1); 
	CtrlGrid.Add (c_LMdn);
	CCtrlInfo *c_MdnGrid   = new CCtrlInfo (&MdnGrid, 2, 0, 1, 1); 
	CtrlGrid.Add (c_MdnGrid);
	CCtrlInfo *c_MdnName     = new CCtrlInfo (&m_MdnName, 3, 0, 1, 1); 
	CtrlGrid.Add (c_MdnName);

//Schnitt
	CCtrlInfo *c_LSchnitt     = new CCtrlInfo (&m_LSchnitt, 1, 1, 1, 1); 
	CtrlGrid.Add (c_LSchnitt);
	CCtrlInfo *c_SchnittGrid   = new CCtrlInfo (&SchnittGrid, 2, 1, 1, 1); 
	CtrlGrid.Add (c_SchnittGrid);
	CCtrlInfo *c_SchnittTxt     = new CCtrlInfo (&m_SchnittTxt, 3, 1, 1, 1); 
	CtrlGrid.Add (c_SchnittTxt);

//Grid AusMat
		AusMatGrid.Create (this, 12, 12);
		AusMatGrid.SetBorder (0, 0);
		AusMatGrid.SetCellHeight (15);
		AusMatGrid.SetFontCellHeight (this);
		AusMatGrid.SetFontCellHeight (this, &Font);
		AusMatGrid.SetGridSpace (12, 12);
// Rahmen AusMat
		CCtrlInfo *c_AusmatGroup = new CCtrlInfo (&m_AusmatGroup, 0, 0, 4, 8);
		AusMatGrid.Add (c_AusmatGroup);
//Grid Material
			MaterialGrid.Create (this, 2, 2);
		    MaterialGrid.SetBorder (0, 0);
		    MaterialGrid.SetGridSpace (0, 0);
			CCtrlInfo *c_Material = new CCtrlInfo (&m_Material, 0, 0, 1, 1);
			MaterialGrid.Add (c_Material);
			CtrlGrid.CreateChoiceButton (m_MaterialChoice, IDC_MATERIALCHOICE, this);
			CCtrlInfo *c_MaterialChoice = new CCtrlInfo (&m_MaterialChoice, 1, 0, 1, 1);
			MaterialGrid.Add (c_MaterialChoice);
//Material
		CCtrlInfo *c_LMaterial   = new CCtrlInfo (&m_LMaterial, 1, 1, 1, 1); 
		AusMatGrid.Add (c_LMaterial);
		CCtrlInfo *c_MaterialGrid   = new CCtrlInfo (&MaterialGrid, 2, 1, 1, 1); 
		AusMatGrid.Add (c_MaterialGrid);
		CCtrlInfo *c_MaterialTxt   = new CCtrlInfo (&m_MaterialTxt, 3, 1, 1, 1); 
		AusMatGrid.Add (c_MaterialTxt);
//Gewicht
		CCtrlInfo *c_LGewicht   = new CCtrlInfo (&m_LGewicht, 1, 2, 1, 1); 
		AusMatGrid.Add (c_LGewicht);
		CCtrlInfo *c_Gewicht   = new CCtrlInfo (&m_Gewicht, 2, 2, 1, 1); 
		AusMatGrid.Add (c_Gewicht);
//EK
		CCtrlInfo *c_LEk   = new CCtrlInfo (&m_LEk, 1, 3, 1, 1); 
		AusMatGrid.Add (c_LEk);
		CCtrlInfo *c_Ek   = new CCtrlInfo (&m_Ek, 2, 3, 1, 1); 
		AusMatGrid.Add (c_Ek);
//Grid Schlachtkalk
			SchlachtKalkGrid.Create (this, 2, 2);
		    SchlachtKalkGrid.SetBorder (0, 0);
		    SchlachtKalkGrid.SetGridSpace (0, 0);
			CCtrlInfo *c_Schlachtkalk = new CCtrlInfo (&m_Schlachtkalk, 0, 0, 1, 1);
			SchlachtKalkGrid.Add (c_Schlachtkalk);
			CtrlGrid.CreateChoiceButton (m_SchlachtKalkChoice, IDC_SCHLACHTKALKCHOICE, this);
			CCtrlInfo *c_SchlachtkalkChoice = new CCtrlInfo (&m_SchlachtKalkChoice, 1, 0, 1, 1);
			SchlachtKalkGrid.Add (c_SchlachtkalkChoice);
//Schlachtkalk
		CCtrlInfo *c_LSchlachtKalk   = new CCtrlInfo (&m_LSchlachtkalk, 1, 4, 1, 1); 
		AusMatGrid.Add (c_LSchlachtKalk);
		CCtrlInfo *c_SchlachtkalkGrid   = new CCtrlInfo (&SchlachtKalkGrid, 2, 4, 1, 1); 
		AusMatGrid.Add (c_SchlachtkalkGrid);
		CCtrlInfo *c_SchlachtkalkTxt   = new CCtrlInfo (&m_SchlachtkalkTxt, 3, 4, 1, 1); 
		AusMatGrid.Add (c_SchlachtkalkTxt);
//Grid SchlachtArt
			SchlachtArtGrid.Create (this, 2, 2);
		    SchlachtArtGrid.SetBorder (0, 0);
		    SchlachtArtGrid.SetGridSpace (0, 0);
			CCtrlInfo *c_Schlachtart = new CCtrlInfo (&m_Schlachtart, 0, 0, 1, 1);
			SchlachtArtGrid.Add (c_Schlachtart);
			CtrlGrid.CreateChoiceButton (m_SchlachtArtChoice, IDC_SCHLACHTARTCHOICE, this);
			CCtrlInfo *c_SchlachtartChoice = new CCtrlInfo (&m_SchlachtArtChoice, 1, 0, 1, 1);
			SchlachtArtGrid.Add (c_SchlachtartChoice);
//Schlachtkalk
		CCtrlInfo *c_LSchlachtArt   = new CCtrlInfo (&m_LSchlachtart, 1, 5, 1, 1); 
		AusMatGrid.Add (c_LSchlachtArt);
		CCtrlInfo *c_SchlachtartGrid   = new CCtrlInfo (&SchlachtArtGrid, 2, 5, 1, 1); 
		AusMatGrid.Add (c_SchlachtartGrid);
		CCtrlInfo *c_SchlachtartTxt   = new CCtrlInfo (&m_SchlachtartTxt, 3, 5, 1, 1); 
		AusMatGrid.Add (c_SchlachtartTxt);
//Preis
		CCtrlInfo *c_LPreis   = new CCtrlInfo (&m_LPreis, 1, 6, 1, 1); 
		AusMatGrid.Add (c_LPreis);
		CCtrlInfo *c_Preis   = new CCtrlInfo (&m_Preis, 2, 6, 1, 1); 
		AusMatGrid.Add (c_Preis);


//Grid AusMat hinzuf�gen
	CCtrlInfo *c_AusMatGrid = new CCtrlInfo (&AusMatGrid, 1, 3, 99, 99);
	CtrlGrid.Add (c_AusMatGrid);

//Grid Info
		InfoGrid.Create (this, 12, 12);
		InfoGrid.SetBorder (0, 0);
		InfoGrid.SetCellHeight (15);
		InfoGrid.SetFontCellHeight (this);
		InfoGrid.SetFontCellHeight (this, &Font);
		InfoGrid.SetGridSpace (12, 12);
//Rahmen Info
		CCtrlInfo *c_InfoGroup = new CCtrlInfo (&m_InfoGroup, 0, 0, 4, 8);
		InfoGrid.Add (c_InfoGroup);
//HDKL
		CCtrlInfo *c_LHDKL = new CCtrlInfo (&m_LHDKL, 1, 1, 1, 1);
		InfoGrid.Add (c_LHDKL);
		CCtrlInfo *c_HDKL = new CCtrlInfo (&m_HDKL, 2, 1, 1, 1);
		InfoGrid.Add (c_HDKL);
//Anzahl
		CCtrlInfo *c_LAnzahl = new CCtrlInfo (&m_LAnzahl, 1, 2, 1, 1);
		InfoGrid.Add (c_LAnzahl);
		CCtrlInfo *c_Anzahl = new CCtrlInfo (&m_Anzahl, 2, 2, 1, 1);
		InfoGrid.Add (c_Anzahl);
//Basis FWP
		CCtrlInfo *c_LBasisFWP = new CCtrlInfo (&m_LBasisFWP, 1, 3, 1, 1);
		InfoGrid.Add (c_LBasisFWP);
		CCtrlInfo *c_BasisFWP = new CCtrlInfo (&m_BasisFWP, 2, 3, 1, 1);
		InfoGrid.Add (c_BasisFWP);
//Grid Bearb
			BearbGrid.Create (this, 3, 3);
			BearbGrid.SetBorder (0, 0);
			BearbGrid.SetCellHeight (15);
			BearbGrid.SetFontCellHeight (this);
			BearbGrid.SetFontCellHeight (this, &Font);
			BearbGrid.SetGridSpace (12, 12);
//Rahmen Bearb
			CCtrlInfo *c_BearbGroup = new CCtrlInfo (&m_BearbGroup, 0, 0, 4, 8);
			BearbGrid.Add (c_BearbGroup);
//Bearb am
			CCtrlInfo *c_LAm = new CCtrlInfo (&m_LAm, 1, 1, 1, 1);
			BearbGrid.Add (c_LAm);
			CCtrlInfo *c_BearbAm = new CCtrlInfo (&m_BearbAm, 2, 1, 1, 1);
			BearbGrid.Add (c_BearbAm);
//Bearb von
			CCtrlInfo *c_LVon = new CCtrlInfo (&m_LVon, 1, 2, 1, 1);
			BearbGrid.Add (c_LVon);
			CCtrlInfo *c_BearbVon = new CCtrlInfo (&m_BearbVon, 2, 2, 1, 1);
			BearbGrid.Add (c_BearbVon);


//Grid Bearb hinzuf�gen
		CCtrlInfo *c_BearbGrid = new CCtrlInfo (&BearbGrid, 3, 1, 99, 99);
		InfoGrid.Add (c_BearbGrid);


//Grid Info hinzuf�gen
	CCtrlInfo *c_InfoGrid = new CCtrlInfo (&InfoGrid, 1, 12, 99, 99);
	CtrlGrid.Add (c_InfoGrid);

//Grid Hinweis
		HinweisGrid.Create (this, 3, 3);
		HinweisGrid.SetBorder (0, 0);
		HinweisGrid.SetCellHeight (15);
		HinweisGrid.SetFontCellHeight (this);
		HinweisGrid.SetFontCellHeight (this, &Font);
		HinweisGrid.SetGridSpace (4, 4);
//Hinweis
	CCtrlInfo *c_LHinweis     = new CCtrlInfo (&m_LHinweis, 0, 0, 1, 1); 
	HinweisGrid.Add (c_LHinweis);
	CCtrlInfo *c_Hinweis     = new CCtrlInfo (&m_Hinweis, 0, 1, 99, 99); 
	HinweisGrid.Add (c_Hinweis);
//Grid Info hinzuf�gen
	CCtrlInfo *c_HinweisGrid = new CCtrlInfo (&HinweisGrid, 1, 18, 99, 99);
	CtrlGrid.Add (c_HinweisGrid);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);

	memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
	memcpy (&Schnitt.schnitt, &schnitt_null, sizeof (SCHNITT));

	Form.Show ();
	DisableTabControl (TRUE);
	EnableFields (FALSE);

	CtrlGrid.Display ();


	CString Message = _T("");
	CStrFuncs::FromClipboard (Message, CF_TEXT);
	CToken t (Message, _T("="));
	if (t.GetAnzToken () > 1)
	{
		CString c = t.GetToken (0);
		if (c == _T("SchnittNr"))
		{
			CStrFuncs::ToClipboard (CString (""), CF_TEXT);
			Schnitt.schnitt.schnitt = (long) CStrFuncs::StrToDouble (t.GetToken (1));
			Schnitt.schnitt.mdn = (short) CStrFuncs::StrToDouble (t.GetToken (2));
			Mdn.mdn.mdn = (short) CStrFuncs::StrToDouble (t.GetToken (2));
//			Schnitt.schnitt.schnitt = 1234; //290509
			Form.Show ();
//			PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
			m_Schnitt.SetFocus ();
			Read ();
//			DisableTabControl (FALSE); //290509

			return FALSE;
		}
	}

	return TRUE;
}


BOOL CPage1::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	/**** funkt. nicht 
	CWnd *Control = GetFocus ();

	if (pMsg->wParam == VK_RETURN && Control == &m_Hinweis)
	{
		return TRUE;
	}
	******/

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Write ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{
				if (GetKeyState (VK_SHIFT) >= 0)
				{
					if (OnReturn ())
					{
						return TRUE;
					}
				}
				else
				{
					if (OnKeyup ())
					{
						return TRUE;
					}
				}
				break;
			}

/*
			else if (pMsg->wParam == VK_F3)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
					NextRec ();
				}
				else
				{
					LastRec ();
				}
				return TRUE;

			}

			else if (pMsg->wParam == VK_F2)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
					PriorRec ();
				}
				else
				{
					FirstRec ();
				}
				return TRUE;

			}
*/

			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F5)
			{
				StepBack ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F7)
			{
				OnDelete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Write ();
				return TRUE;
			}
/*
			else if (pMsg->wParam == VK_F8)
			{
				if (Choice != NULL)
				{
					if (Choice->IsWindowVisible ())
					{
						Choice->ShowWindow (SW_HIDE);
					}
					else
					{
						Choice->ShowWindow (SW_SHOWNORMAL);
					}
				}
				else
				{
				}
			}
*/
			else if (pMsg->wParam == VK_F9)
			{
				if (GetFocus () == &m_Mdn)
				{
					OnMdnchoice ();
					return TRUE;
				}
				if (GetFocus () == &m_Schnitt)
				{
					OnSchnittchoice ();
					return TRUE;
				}
				if (GetFocus () == &m_Material)
				{
					OnMatchoice ();
					return TRUE;
				}
				if (GetFocus () == &m_Schlachtart)
				{
					OnSchlArtchoice ();
					return TRUE;
				}
				if (GetFocus () == &m_Schlachtkalk)
				{
					OnSchlKalkchoice ();
					return TRUE;
				}
			return TRUE;
			}
	}
	return FALSE;
}


BOOL  CPage1::OnReturn ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_Mdn)
	{
		if (!ReadMdn ())
		{
			m_Mdn.SetFocus ();
			return FALSE;
		}
	}

	if (Control == &m_Schnitt)
	{
		if (flgCopy == TRUE)
		{
			if (Schnitt.schnitt.schnitt == -1)
			{
				Keys.Get ();
				if (Schnitt.dbreadfirst () != 0)
				{
					if (Schnitt.schnitt.kalk_bas == 0) Schnitt.schnitt.kalk_bas = Schnitt.kalk_bas;
					Work.RenameSchnitt(Mdn.mdn.mdn,-1,Schnitt.schnitt.schnitt);
				}
				else
				{
					if (Schnitt.schnitt.schnitt != -1) MessageBox("Die Nummer ist bereits vergeben",NULL,MB_ICONEXCLAMATION);
					Schnitt.schnitt.schnitt = -1;
					return FALSE;
				}
			}
			flgCopy = FALSE;
		}
		if (!Read ())
		{
//			m_Schnitt.SetFocus ();
			return FALSE;
		}
	}

	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
		Control->SetFocus ();
	}
	return TRUE;
}

BOOL  CPage1::OnKeyup ()
{
	CWnd *Control = GetFocus ();
	Control = GetNextDlgTabItem (Control, TRUE);
	if (Control != NULL)
	{
		Control->SetFocus ();
	}
	return TRUE;
}


BOOL  CPage1::ReadMdn ()
{
	memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
	Form.Get ();
	
	if (Mdn.dbreadfirst () == 0)
	{
		MdnAdr.adr.adr = Mdn.mdn.adr;
		MdnAdr.dbreadfirst ();
		Form.Show ();
		return TRUE;
	}
	else
	{
		CString Error;
		Error.Format (_T("Mandant %hd nicht gefunden"),Mdn.mdn.mdn);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONERROR);
		memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		Form.Show ();
		m_Mdn.SetFocus ();
		m_Mdn.SetSel (0, -1);
		return FALSE;
	}
	return FALSE;
}


void CPage1::OnMdnchoice ()
{

	if (ChoiceMdn == NULL)
	{
		ChoiceMdn = new CChoiceMdn (this);
	    ChoiceMdn->IsModal = TRUE;
		ChoiceMdn->CreateDlg ();
	}

//	Choice->IsModal = ModalChoice;
    ChoiceMdn->SetDbClass (&Mdn);
	ChoiceMdn->DoModal();
    if (ChoiceMdn->GetState ())
    {
		  CMdnList *abl = ChoiceMdn->GetSelectedText (); 
		  if (abl == NULL) return;
//		  a_kun_gx.mdn = abl->mdn;
          memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		  memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		  Mdn.mdn.mdn = abl->mdn;
		  if (Mdn.dbreadfirst () == 0)
		  {
			  MdnAdr.adr.adr = Mdn.mdn.adr;
			  MdnAdr.dbreadfirst ();
		  }
		  if (Mdn.dbreadfirst () == 0)
		  {
		  }
		  Form.Show ();
		  m_Mdn.SetSel (0, -1, TRUE);
		  m_Mdn.SetFocus ();
          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
}
void CPage1::OnSchlKalkchoice ()
{

	if (ChoiceKalk == NULL)
	{
		ChoiceKalk = new CChoiceKalk (this);
	    ChoiceKalk->IsModal = TRUE;
		ChoiceKalk->CreateDlg ();
	}

//	Choice->IsModal = ModalChoice;
    ChoiceKalk->SetDbClass (&Schlaklkk);
	ChoiceKalk->DoModal();
    if (ChoiceKalk->GetState ())
    {
		  CKalkList *abl = ChoiceKalk->GetSelectedText (); 
		  if (abl == NULL) return;
//		  a_kun_gx.mdn = abl->mdn;
          memcpy (&Schlaklkk.schlaklkk, &schlaklkk_null, sizeof (SCHLAKLKK));
		  Schlaklkk.schlaklkk.mdn = abl->mdn;
		  Schlaklkk.schlaklkk.schlklknr = abl->schlklknr;
		  if (Schlaklkk.dbreadfirst () == 0)
		  {
		  }
		  Schnitt.schnitt.schlachtkalk = abl->schlklknr;
 	 	 _tcscpy (Schnitt.schnitt._schlachtkalk_bz, Schlaklkk.schlaklkk.schklk_bez);

		  Form.Show ();
		  m_Schlachtkalk.SetSel (0, -1, TRUE);
		  m_Schlachtkalk.SetFocus ();
          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
}
void CPage1::OnSchnittchoice ()
{
	ModalChoice = FALSE;
	AChoiceStat = TRUE;
	Form.Get ();
	Where.Format (_T(" and schnitt.mdn =  \"%hd\" "), Mdn.mdn.mdn);
	if (Choice != NULL) // immer neu aufbauen, da sich mdn ge�ndert haben kann
					    // (so viel Schnittf�hrungen sinds ja nicht, geht schnell)
	{
		delete Choice;
		Choice = NULL;
	}

	if (Choice != NULL)
	{
		Choice->ShowWindow (SW_SHOWNORMAL);
		if (SearchA.Trim () != "")
		{
			Choice->SearchText = SearchA;
			Choice->SetSearchText ();
		}
		return;
	}
	if (Choice == NULL)
	{
		Choice = new CChoiceSchnitt (this);
	    Choice->IsModal = FALSE;
		Choice->DlgBkColor = ListBkColor;
		Choice->Where = Where;
		Choice->IdArrDown = IDI_HARROWDOWN;
		Choice->IdArrUp   = IDI_HARROWUP;
		Choice->IdArrNo   = IDI_HARROWNO;
		Choice->HideOK    = HideOK;
		Choice->CreateDlg ();
		OnChoiceaDefault();
	}

    Choice->SetDbClass (&A_bas);
	if (SearchA.Trim () != "")
	{
		Choice->SearchText = SearchA;
		Choice->SetSearchText ();
	}
	CRect mrect;
	GetParent ()->GetWindowRect (&mrect);
	CRect rect;
	Choice->GetWindowRect (&rect);
	int scx = GetSystemMetrics (SM_CXSCREEN);
	int scy = GetSystemMetrics (SM_CYSCREEN);
	rect.top = 50;
	rect.right = scx - 2;
	rect.left = rect.right - 300;
	rect.bottom = scy - 50;

	Choice->MoveWindow (&rect);
	Choice->SetListFocus ();

	return;
}


void CPage1::ReadCfg ()
{
    char cfg_v [256];

/*
    if (Cfg.GetCfgValue ("UseOdbc", cfg_v) == TRUE)
    {
			DB_CLASS::UseOdbc = atoi (cfg_v);
    }
*/
    if (Cfg.GetCfgValue ("cfg_Multimandant", cfg_v) == TRUE)
    {
		Schnitt.cfg_Multimandant = atoi(cfg_v); 
	}
    if (Cfg.GetCfgValue ("FWP_berechnen", cfg_v) == TRUE)
    {
		Schnitt.cfg_FWP_berechnen = atoi(cfg_v);
	}
    if (Cfg.GetCfgValue ("PrGrStuf", cfg_v) == TRUE)
    {
		Schnitt.PrGrStuf = atoi(cfg_v);
	}
    if (Cfg.GetCfgValue ("KalkBas", cfg_v) == TRUE)
    {
		Schnitt.kalk_bas = atoi(cfg_v);
	}
    if (Cfg.GetCfgValue ("PreiseAus_a_kalkpreis", cfg_v) == TRUE)
    {
		Schnitt.cfg_PreiseAus_a_kalkpreis = atoi(cfg_v);
	}
    if (Cfg.GetCfgValue ("ek_aus_schnitt", cfg_v) == TRUE)
    {
		Schnitt.cfg_ek_aus_schnitt = atoi(cfg_v); //Ek nicht aus Stammdaten, kann hier ver�ndert werden (Richter)
	}
    if (Cfg.GetCfgValue ("NurGTZerlegen", cfg_v) == TRUE)
    {
		Schnitt.cfg_NurGTZerlegen = atoi(cfg_v);
	}
    if (Cfg.GetCfgValue ("NurVariableMaterialienSpeichern", cfg_v) == TRUE)
    {
		Schnitt.cfg_NurVariableMaterialienSpeichern = atoi(cfg_v);
	}
    if (Cfg.GetCfgValue ("GTKostenAddieren", cfg_v) == TRUE)
    {
		Schnitt.cfg_GTKostenAddieren = atoi(cfg_v);
	}
    if (Cfg.GetCfgValue ("MaxComboEntries", cfg_v) == TRUE)
    {
//			m_List.MaxComboEntries = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("ModalChoice", cfg_v) == TRUE)
    {
//			ModalChoice = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("CloseChoice", cfg_v) == TRUE)
    {
//			CloseChoice = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("GridLines", cfg_v) == TRUE)
    {
//			m_List.GridLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("HLines", cfg_v) == TRUE)
    {
//			m_List.HLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("VLines", cfg_v) == TRUE)
    {
//			m_List.VLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("CellHeight", cfg_v) == TRUE)
    {
//			CellHeight = atoi (cfg_v);
    }
	Cfg.CloseCfg ();
}

BOOL CPage1::StepBack ()
{
	if (m_Mdn.IsWindowEnabled ())
	{
		if (Frame != NULL)
		{
			if (Frame->IsKindOf (RUNTIME_CLASS (CDialog)))
			{
				((CDialog *) Frame)->EndDialog (0);
			}
			else
			{
					Frame->GetParent ()->DestroyWindow ();
					return FALSE;
			}
		}
	}
	else
	{
		DisableTabControl (TRUE);
		EnableFields (FALSE);
		m_Schnitt.SetFocus ();
		Schnitt.rollbackwork ();
	}
	return TRUE;
}

void CPage1::EnableFields (BOOL b)
{
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
			f->Control->EnableWindow (b);
	}
	HeadControls.Enable (!b);
	PosControls.Enable (b);
	Update->UpdateAll ();
}

void CPage1::DisableTabControl (BOOL disable)
{
	CPropertySheet *p = (CPropertySheet *) GetParent ();
	CTabCtrl *tab = p->GetTabControl ();
	if (disable)
	{
		tab->ShowWindow (SW_HIDE);
	}
	else
	{
		tab->ShowWindow (SW_SHOWNORMAL);
	}
}

BOOL CPage1::Read ()
{
	extern short sql_mode;
	BOOL FMat = FALSE;
/*
	if (ModalChoice)
	{
		CString cA;
		m_A.GetWindowText (cA);
		if (!CStrFuncs::IsDecimal (cA))
		{
			SearchA = cA;
			OnAchoice ();
			SearchA = "";
			return TRUE;
		}
	}
*/
	LockWindowUpdate ();
	memcpy (&Schnitt.schnitt, &schnitt_null, sizeof (SCHNITT));
	
	Keys.Get ();
	if (Schnitt.schnitt.schnitt == 0)
	{
			MessageBox (_T("Schnitt-Nr 0 kann nicht angelegt werden"), NULL, 
						MB_OK | MB_ICONERROR);
			m_Schnitt.SetFocus ();
			m_Schnitt.SetSel (0, -1);
  			UnlockWindowUpdate ();
            return FALSE;
	}
	Schnitt.schnitt.mdn = Mdn.mdn.mdn;
	if (Schnitt.dbreadfirst () == 0)
	{
		if (Schnitt.schnitt.kalk_bas == 0) Schnitt.schnitt.kalk_bas = Schnitt.kalk_bas;
/*
		if (ArtikelGesperrt)
		{
			MessageBox (_T("Der Artikel wird von einem anderen Benitzer bearbeitet"), NULL, 
						MB_OK | MB_ICONERROR);
			m_A.SetFocus ();
			m_A.SetSel (0, -1);
  			UnlockWindowUpdate ();
            return FALSE;
		}
*/
		if (Work.ReadAusMaterial(Schnitt.schnitt.mdn,Schnitt.schnitt.schnitt_mat) != 0)
		{
			CString Error;
			Error.Format (_T("Ausgangsmaterial %d nicht vorhanden"),Schnitt.schnitt.schnitt_mat);
			MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONINFORMATION);
			m_Schnitt.SetFocus ();
			m_Schnitt.SetSel (0, -1);
  			UnlockWindowUpdate ();
			return FALSE;
		}
		_tcscpy (Schnitt.schnitt._material_bz, Work.AusMatABas.a_bas.a_bz1);
//		Schnitt.schnitt.ek_pr = Work.AKalkMat.a_kalk_mat.mat_o_b;
//		Schnitt.schnitt.ek = Work.AKalkMat.a_kalk_mat.mat_o_b;      //erst mal beide best�cken , sp�ter Schlachtkalk einbeziehen
		ReadSchlachtKalk(Schnitt.schnitt.schlachtkalk);
		if (Work.ReadSchlachtArt(Schnitt.schnitt.schlachtart) == 0)
		{
			_tcscpy (Schnitt.schnitt._schlachtart_bz, Work.SchlArtABas.a_bas.a_bz1);
		}
		EkPreiseHolen();
		Schnitt.schnitt._basis_fwp = Work.AusMatAVarb.a_varb.fwp;
		Schnitt.schnitt._wert_fwp = Schnitt.schnitt.ek_pr / Schnitt.schnitt._basis_fwp;

		KostArtZerl.kost_art_zerl.mdn = Schnitt.schnitt.mdn;
		KostArtZerl.kost_art_zerl.kost_art_zerl = Schnitt.schnitt.koart_grund;
		if (KostArtZerl.dbreadfirst() == 0)	Schnitt.schnitt._grund_kosten = KostArtZerl.kost_art_zerl.vkost_wt;

		KostArtZerl.kost_art_zerl.mdn = Schnitt.schnitt.mdn;
		KostArtZerl.kost_art_zerl.kost_art_zerl = Schnitt.schnitt.koart_grob;
		if (KostArtZerl.dbreadfirst() == 0)	Schnitt.schnitt._grob_kosten = KostArtZerl.kost_art_zerl.vkost_wt;

		KostArtZerl.kost_art_zerl.mdn = Schnitt.schnitt.mdn;
		KostArtZerl.kost_art_zerl.kost_art_zerl = Schnitt.schnitt.koart_vm;
		if (KostArtZerl.dbreadfirst() == 0)	Schnitt.schnitt._vm_kosten = KostArtZerl.kost_art_zerl.vkost_wt;

		KostArtZerl.kost_art_zerl.mdn = Schnitt.schnitt.mdn;
		KostArtZerl.kost_art_zerl.kost_art_zerl = Schnitt.schnitt.koart_vt;
		if (KostArtZerl.dbreadfirst() == 0)	Schnitt.schnitt._vt_kosten = KostArtZerl.kost_art_zerl.vkost_wt;

		KostArtZerl.kost_art_zerl.mdn = Schnitt.schnitt.mdn;
		KostArtZerl.kost_art_zerl.kost_art_zerl = Schnitt.schnitt.koart_za;
		if (KostArtZerl.dbreadfirst() == 0)	Schnitt.schnitt._za_kosten = KostArtZerl.kost_art_zerl.vkost_wt;

		KostArtZerl.kost_art_zerl.mdn = Schnitt.schnitt.mdn;
		KostArtZerl.kost_art_zerl.kost_art_zerl = Schnitt.schnitt.koart_min;
		if (KostArtZerl.dbreadfirst() == 0)	Schnitt.schnitt._min_kosten = KostArtZerl.kost_art_zerl.vkost_wt;

	memcpy (&Zerm.zerm, &zerm_null, sizeof (ZERM)); 

	WriteFirstZerm ();

	A_mat.a_mat.mat = Schnitt.schnitt.schnitt_mat;
	if (A_mat.a_mat.mat > 0)
	{
		if (A_mat.dbreadfirst () != 0)
		{
			MessageBox (_T("Materialnummer nicht vorhanden"), NULL, 
						MB_OK | MB_ICONERROR);
			FMat = TRUE;
		}
		else
		{
			A_bas.a_bas.a = A_mat.a_mat.a;
			if (A_bas.a_bas.a > 0.0)
			{
				if (A_bas.dbreadfirst () != 0 )
				{
					MessageBox (_T("Artikelnummer nicht vorhanden"), NULL, 
								MB_OK | MB_ICONERROR);
					m_Schnitt.SetFocus ();
					m_Schnitt.SetSel (0, -1);
  					UnlockWindowUpdate ();
					return FALSE;
				}
				ReadMaterial(A_mat.a_mat.mat);
			}
		}
	}
	else
	{
		A_bas.a_bas.a = 0.0;
	}
	_tcscpy (Zerm.zerm.zerm_aus_mat_bz, A_bas.a_bas.a_bz1);

		Form.Show ();
  	    Update->Show (); 
		flgCopy = FALSE;
		if (Schnitt.schnitt.schnitt == -1)
		{
			flgCopy = TRUE;
			EnableFields (FALSE);
			m_Mdn.SetFocus ();
			m_Mdn.SetSel (0, -1);
	  	    UnlockWindowUpdate ();
	        PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
			return TRUE;
		}
		else
		{
			m_Schnitt.SetFocus ();
			m_Schnitt.SetSel (0, -1);
			EnableFields (TRUE);
		}
		DisableTabControl (FALSE);
  	    UnlockWindowUpdate ();
//		m_SchnittTxt.SetFocus ();
//		if (FMat) m_Material.SetFocus();
		return TRUE;
	}
	else
	{
		CString Error;
		Error.Format (_T("Schnitt-Nr %ld wird neu angelegt"),Schnitt.schnitt.schnitt);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONINFORMATION);
	    DisableTabControl (FALSE);
		EnableFields (TRUE);
		Schnitt.schnitt.kalk_bas = Schnitt.kalk_bas;  //defaultwert aus cfg holen
 	    Form.Show ();
		m_Schnitt.SetFocus ();
		m_Schnitt.SetSel (0, -1);
  	    UnlockWindowUpdate ();
		return TRUE;
	}
	return FALSE;
}

void CPage1::OnFileSave () //kommt hier nicht zum tragen !!??
{
	Write ();
}
BOOL CPage1::Write ()
{
	extern short sql_mode;
	short sql_s;
	sql_s = sql_mode;
//	sql_mode = 1;
	Form.Get ();
	CString Date;
	CStrFuncs::SysDate (Date);
	Schnitt.ToDbDate (Date, &Schnitt.schnitt.zer_dat);
	Schnitt.ToDbDate (Date, &Schnitt.schnitt.kalk_dat);
	Schnitt.dbupdate();

	WriteFirstZerm ();
	Schnitt.commitwork();
	DisableTabControl (TRUE);
	EnableFields (FALSE);
	m_Schnitt.SetFocus ();

	return TRUE;
}

void CPage1::OnChoicebackground()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.

	CColorDialog dlg;
	dlg.m_cc.Flags |= CC_FULLOPEN | CC_RGBINIT;
	COLORREF Color = GetSysColor (COLOR_3DFACE);
	dlg.m_cc.rgbResult = Color;
	if (dlg.DoModal() == IDOK)
	{
		ListBkColor = dlg.GetColor();
		if (Choice != NULL)
		{
			Choice->DlgBkColor = dlg.GetColor();
			DeleteObject (Choice->DlgBrush);
			Choice->DlgBrush = NULL;
			Choice->InvalidateRect (NULL);
		}
	}
}

void CPage1::OnChoiceaDefault()
{
	if (Choice == NULL) return;
	Choice->SetDefault ();

	CColorDialog dlg;
	dlg.m_cc.Flags |= CC_FULLOPEN | CC_RGBINIT;
	COLORREF Color = GetSysColor (COLOR_3DFACE);
	dlg.m_cc.rgbResult = Color;

			Choice->DlgBkColor = dlg.GetColor();
			DeleteObject (Choice->DlgBrush);
			Choice->DlgBrush = NULL;
			Choice->InvalidateRect (NULL);

}
void CPage1::OnSchnittSelected ()
{
	if (Choice == NULL) return;
    CSchnittList *abl = Choice->GetSelectedText (); 
    if (abl == NULL) return;
    Schnitt.schnitt.schnitt = abl->schnitt;
	Form.GetFormField (&m_Schnitt)->Show ();
	Read ();
    Form.Show ();
	if (Choice->FocusBack)
	{
		Choice->SetListFocus ();
	}
}
void CPage1::OnSchnittCanceled ()
{
	Choice->ShowWindow (SW_HIDE);
}

void CPage1::OnEnKillfocusMaterial()
{
		CString cMat;
		m_Material.GetWindowText (cMat);

		ReadMaterial(_tstol(cMat.GetBuffer()));
}

void CPage1::OnEnKillfocusSchlachtkalk()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
		CString cKalk;
		m_Schlachtkalk.GetWindowText (cKalk);
		ReadSchlachtKalk(_tstol(cKalk.GetBuffer()));
		EkPreiseHolen();

		Schnitt.schnitt._wert_fwp = Schnitt.schnitt.ek_pr / Schnitt.schnitt._basis_fwp;
}

void CPage1::OnEnKillfocusSchlachtart()
{
		CString cArt;
		m_Schlachtart.GetWindowText (cArt);

		ReadSchlachtArt(_tstol(cArt.GetBuffer()));
		EkPreiseHolen();

		Schnitt.schnitt._wert_fwp = Schnitt.schnitt.ek_pr / Schnitt.schnitt._basis_fwp;

}
BOOL CPage1::ReadMaterial(long mat)
{

	ModalChoice = TRUE;
	if (ModalChoice)
	{
		CString cA;
		m_Material.GetWindowText (cA);
		if (!CStrFuncs::IsDecimal (cA))
		{
			SearchA = cA;
			OnMatchoice ();
			SearchA = "";
			if (!AChoiceStatM)
			{
				m_Material.SetFocus ();
				m_Material.SetSel (0, -1);
				ModalChoice = FALSE;
				return FALSE;
			}
		}
	}

	CString CStr;
		if (Work.ReadAusMaterial(Schnitt.schnitt.mdn,mat) != 0)
		{
			m_Material.SetFocus ();
			return FALSE;
		}
		_tcscpy (Schnitt.schnitt._material_bz, Work.AusMatABas.a_bas.a_bz1);

//		Schnitt.schnitt.ek_pr = Work.AKalkMat.a_kalk_mat.mat_o_b;
//		Schnitt.schnitt.ek = Work.AKalkMat.a_kalk_mat.mat_o_b;      //erst mal beide best�cken , sp�ter Schlachtkalk einbeziehen
		EkPreiseHolen();
		Schnitt.schnitt._basis_fwp = Work.AusMatAVarb.a_varb.fwp;
		Schnitt.schnitt._wert_fwp = Schnitt.schnitt.ek_pr / Schnitt.schnitt._basis_fwp;
		Schnitt.schnitt.schnitt_mat = mat;
		CStr.Format (_T("%d"),Schnitt.schnitt.schnitt_mat);
		m_Material.SetWindowText (CStr);
		m_MaterialTxt.SetWindowText (Schnitt.schnitt._material_bz);
		CStr.Format (_T("%5.2f"),Schnitt.schnitt._basis_fwp);
		m_BasisFWP.SetWindowText (CStr);
		return TRUE;
}
BOOL CPage1::ReadSchlachtArt(double a)
{
	ModalChoice = TRUE;
	if (ModalChoice)
	{
		CString cA;
		m_Schlachtart.GetWindowText (cA);
		if (!CStrFuncs::IsDecimal (cA))
		{
			SearchA = cA;
			OnSchlArtchoice ();
			SearchA = "";
			if (!AChoiceStatM)
			{
				m_Schlachtart.SetFocus ();
				m_Schlachtart.SetSel (0, -1);
				ModalChoice = FALSE;
				return FALSE;
			}
		}
	}

	CString CStr;
	    if (a == (double) 0) return TRUE;
		if (Work.ReadSchlachtArt(a) != 0)
		{
			m_Schlachtart.SetFocus ();
			m_Schlachtart.SetSel (0, -1);
			return FALSE;
		}
		_tcscpy (Schnitt.schnitt._schlachtart_bz, Work.SchlArtABas.a_bas.a_bz1);
		EkPreiseHolen();
//		Schnitt.schnitt.ek = Work.A_schl.a_schl.grund_pr;
//		Schnitt.schnitt.ek_pr = Work.A_schl.a_schl.grund_pr; //ZUTUN sp�ter errechnen
		m_SchlachtartTxt.SetWindowText (Schnitt.schnitt._schlachtart_bz);
		CStr.Format (_T("%.0f"),a);
		m_Schlachtart.SetWindowText (CStr);
		Schnitt.schnitt.schlachtart = a;

		return TRUE;
}
BOOL CPage1::ReadSchlachtKalk(int kalk)
{
	ModalChoice = TRUE;
	if (ModalChoice)
	{
		CString cKalk;
		m_Schlachtkalk.GetWindowText (cKalk);
		if (!CStrFuncs::IsDecimal (cKalk))
		{
			OnSchlKalkchoice ();
		}
	}

	CString CStr;
	    if (kalk ==  0) return TRUE;
		if (Work.ReadSchlachtKalk(kalk,Mdn.mdn.mdn) != 0)
		{
			m_Schlachtkalk.SetFocus ();
			m_Schlachtkalk.SetSel (0, -1);
			return FALSE;
		}
		else
		{
    	  Schnitt.schnitt.schlachtkalk = Work.Schlaklkk.schlaklkk.schlklknr;
 	 	 _tcscpy (Schnitt.schnitt._schlachtkalk_bz, Work.Schlaklkk.schlaklkk.schklk_bez);
     		m_SchlachtkalkTxt.SetWindowText (Schnitt.schnitt._schlachtkalk_bz);
		}
		return TRUE;
}
void CPage1::EkPreiseHolen (void)
{
	CString CStr;
	double ek = 0.0;
	if (Schnitt.cfg_ek_aus_schnitt) ek = Schnitt.schnitt.ek;
	Work.EkPreiseHolen (Mdn.mdn.mdn,
						Schnitt.schnitt.schnitt_mat,
						Schnitt.schnitt.schlachtart,
						Schnitt.schnitt.schlachtkalk,
						Schnitt.cfg_PreiseAus_a_kalkpreis,
						ek);
	Schnitt.schnitt.ek_pr = Work.Schnitt.schnitt.ek_pr;
	Schnitt.schnitt.ek = Work.Schnitt.schnitt.ek;
	CStr.Format (_T("%6.2f"),Schnitt.schnitt.ek);
	m_Ek.SetWindowText (CStr);
	CStr.Format (_T("%8.4f"),Schnitt.schnitt.ek_pr);
	m_Preis.SetWindowText (CStr);


}
void CPage1::OnMatchoice ()
{
	ModalChoice = TRUE;
	AChoiceStatM = TRUE;
	Form.Get ();
	Where.Format (_T(" and (a_bas.a_typ = 5 or a_bas.a_typ2 = 5) and a_bas.a in (select a from a_mat) "));
	if (ChoiceMat == NULL)
	{
		ChoiceMat = new CChoiceA (this);
	    ChoiceMat->IsModal = ModalChoice;
		ChoiceMat->Where = Where;
		ChoiceMat->IdArrDown = IDI_HARROWDOWN;
		ChoiceMat->IdArrUp   = IDI_HARROWUP;
		ChoiceMat->IdArrNo   = IDI_HARROWNO;
		ChoiceMat->HideOK    = HideOK;

		ChoiceMat->CreateDlg ();
	}
	CString cA;
	m_Material.GetWindowText (cA);
	if (!CStrFuncs::IsDecimal (cA))
	{
		m_Material.SetWindowText ("");
	}

    ChoiceMat->SetDbClass (&A_bas);
	ChoiceMat->SearchText = SearchA;
	ChoiceMat->DoModal();
    if (ChoiceMat->GetState ())
    {
		  CABasList *abl = ChoiceMat->GetSelectedText (); 
		  if (abl == NULL) return;
          memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
          A_bas.a_bas.a = abl->a;
		  if (A_bas.dbreadfirst () == 0)
		  {
	          A_mat.a_mat.a = abl->a;
			  if (A_mat.dbreadfirsta () == 0)
			  {
				  ReadMaterial(A_mat.a_mat.mat);
			  }
		  }
		  Form.Show ();
		  m_Material.EnableWindow (TRUE);
		  m_Material.SetSel (0, -1, TRUE);
		  m_Material.SetFocus ();
		  if (SearchA == "")
		  {
			PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
		  }
    }
	else
	{
		AChoiceStatM = FALSE;
	}
}

void CPage1::OnSchlArtchoice ()
{
	ModalChoice = TRUE;
	AChoiceStatM = TRUE;
	Form.Get ();
	Where.Format (_T(" and a_bas.a_typ = 14 "));
	if (ChoiceSchlA == NULL)
	{
		ChoiceSchlA = new CChoiceA (this);
	    ChoiceSchlA->IsModal = ModalChoice;
		ChoiceSchlA->Where = Where;
		ChoiceSchlA->IdArrDown = IDI_HARROWDOWN;
		ChoiceSchlA->IdArrUp   = IDI_HARROWUP;
		ChoiceSchlA->IdArrNo   = IDI_HARROWNO;
		ChoiceSchlA->HideOK    = HideOK;

		ChoiceSchlA->CreateDlg ();
	}
	CString cA;
	m_Schlachtart.GetWindowText (cA);
	if (!CStrFuncs::IsDecimal (cA))
	{
		m_Schlachtart.SetWindowText ("");
	}

    ChoiceSchlA->SetDbClass (&A_bas);
	ChoiceSchlA->SearchText = SearchA;
	ChoiceSchlA->DoModal();
    if (ChoiceSchlA->GetState ())
    {
		  CABasList *abl = ChoiceSchlA->GetSelectedText (); 
		  if (abl == NULL) return;
          memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
          A_bas.a_bas.a = abl->a;
		  if (A_bas.dbreadfirst () == 0)
		  {
				  ReadSchlachtArt(A_bas.a_bas.a);
		  }
		  Form.Show ();
		  m_Schlachtart.EnableWindow (TRUE);
		  m_Schlachtart.SetSel (0, -1, TRUE);
		  m_Schlachtart.SetFocus ();
		  if (SearchA == "")
		  {
			PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
		  }
    }
	else
	{
		AChoiceStatM = FALSE;
	}
}
void CPage1::OnDelete ()
{
    extern short sql_mode;
	short sql_s = sql_mode;
    sql_mode = 1;
	DelOK = FALSE;
	if (m_Mdn.IsWindowEnabled ()) return;
	int ret = MessageBox (_T("Schnittf�hrung l�schen"), NULL, MB_YESNO | MB_ICONQUESTION);
	if (ret != IDYES) return;
	ret = Work.DelZerm(Schnitt.schnitt.mdn,Schnitt.schnitt.schnitt);
	if (ret < 0) return ;
	ret = Schnitt.dbdelete();

	DisableTabControl (TRUE);
	EnableFields (FALSE);
	m_Schnitt.SetFocus ();
	Schnitt.commitwork ();
	sql_mode = sql_s;
	DelOK = TRUE;
	return;
}

void CPage1::OnEnUpdateGewicht()
{
}

void CPage1::OnEnKillfocusGewicht()
{
	CString cGew;
	m_Gewicht.GetWindowText (cGew);
	Work.UpdateGew(Schnitt.schnitt.mdn,
			Schnitt.schnitt.schnitt,
			Schnitt.schnitt.schnitt_mat,
			Schnitt.schnitt.schnitt_mat,
			CStrFuncs::StrToDouble (cGew.GetBuffer()));
    Form.Get ();
	Form.Show();
	OnEnKillfocusPreis();

}

void CPage1::OnEnKillfocusEk()
{
	Schnitt.schnitt._wert_fwp = Schnitt.schnitt.ek_pr / Schnitt.schnitt._basis_fwp;
    Form.Get ();
	Form.Show();

}

void CPage1::OnEnKillfocusPreis()
{
	memcpy (&Zerm.zerm, &zerm_null, sizeof (ZERM));
	Zerm.zerm.mdn = Schnitt.schnitt.mdn;
	Zerm.zerm.schnitt = Schnitt.schnitt.schnitt;
	Zerm.zerm.zerm_aus_mat = Schnitt.schnitt.schnitt_mat;
	Zerm.zerm.zerm_mat = Schnitt.schnitt.schnitt_mat;
	Zerm.zerm.a_typ = Zerm.TYP_MATERIAL;
	Zerm.dbreadfirst();
	CString cPr;
	m_Preis.GetWindowText (cPr);

	Zerm.zerm.mat_o_b = CStrFuncs::StrToDouble (cPr.GetBuffer() );
	Zerm.zerm.hk_vollk = Zerm.zerm.mat_o_b;
	Zerm.zerm.hk_teilk = Zerm.zerm.mat_o_b;
	Zerm.zerm.dm_100 = Zerm.zerm.mat_o_b * 100;
	Zerm.dbupdate();
	Schnitt.schnitt._wert_fwp = Zerm.zerm.mat_o_b / Schnitt.schnitt._basis_fwp;
    Form.Get ();
	WriteFirstZerm ();
	Form.Show();


}
BOOL CPage1::WriteFirstZerm ()
{

	Zerm.zerm.mdn = Schnitt.schnitt.mdn;
	Zerm.zerm.schnitt = Schnitt.schnitt.schnitt;
	Zerm.zerm.zerm_aus_mat = Schnitt.schnitt.schnitt_mat;
	Zerm.zerm.zerm_mat = Schnitt.schnitt.schnitt_mat;

	_tcscpy (Zerm.zerm.a_kz, "GT");
	_tcscpy (Zerm.zerm.fix, "V");
	Zerm.zerm.fwp = Schnitt.schnitt._basis_fwp;
	Zerm.zerm.fwp_org = Schnitt.schnitt._basis_fwp; //180509
	Zerm.zerm.zerm_gew = Schnitt.schnitt.ek_gew;
	Zerm.zerm.zerm_gew_ant = 100;
	_tcscpy (Zerm.zerm.zerm_teil, "1");
	Zerm.zerm.mat_o_b = Schnitt.schnitt.ek_pr;
	Zerm.zerm.hk_vollk = Schnitt.schnitt.ek_pr;
	Zerm.zerm.hk_teilk = Schnitt.schnitt.ek_pr;
	Zerm.zerm.delstatus =  0;
	Zerm.zerm.vollkosten_abs = (double) 0;
	Zerm.zerm.vollkosten_proz = (double) 0;
	Zerm.zerm.teilkosten_abs = Zerm.zerm.mat_o_b * Zerm.zerm.zerm_gew;
	Zerm.zerm.teilkosten_proz = (double) 0;
	Zerm.zerm.dm_100 = Schnitt.schnitt.ek_pr * 100;
	Zerm.zerm.dm_100 = (double) 0;
	Zerm.zerm.a_z_f =  0;
	Zerm.zerm.vk_pr = (double) 0;
	Zerm.zerm.kosten_zutat = (double) 0; //erst mal
	Zerm.zerm.kosten_vpk = (double) 0;//erst mal
	Zerm.zerm.a_typ = Zerm.TYP_MATERIAL;
	Zerm.zerm.zerm_aus_a_typ = Zerm.TYP_MATERIAL;
	if (Zerm.zerm.teilkosten_abs < 9999.9)
	{
		Zerm.dbupdate();
	}

	Zerm.zerm.zerm_mat = Zerm.Zerlegeverlust;
	if (Zerm.dbreadfirst() == 0) return TRUE; //ZV schon vorhanden, nix tun
	_tcscpy (Zerm.zerm.a_kz, "ZV");
	_tcscpy (Zerm.zerm.fix, "F");
	Zerm.zerm.fwp = 0;
	Zerm.zerm.zerm_gew = Schnitt.schnitt.ek_gew;
	Zerm.zerm.zerm_gew_ant = 100;
	_tcscpy (Zerm.zerm.zerm_teil, "0");
	Zerm.zerm.mat_o_b = (double) 0;
	Zerm.zerm.hk_vollk = (double) 0;
	Zerm.zerm.hk_teilk = (double) 0;
	Zerm.zerm.delstatus =  0;
	Zerm.zerm.vollkosten_abs = (double) 0;
	Zerm.zerm.vollkosten_proz = (double) 0;
	Zerm.zerm.teilkosten_abs = (double) 0;
	Zerm.zerm.teilkosten_proz = (double) 0;
	Zerm.zerm.dm_100 = (double) 0;
	Zerm.zerm.a_typ = Zerm.TYP_MATERIAL;
	Zerm.dbupdate();


	return TRUE;
}

void CPage1::OnEnSetfocusEk()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
}

void CPage1::OnEnSetfocusSchnittTxt()
{
	if (Zerm.PAGE2_WRITE == TRUE)
	{
		Zerm.ZerlTreeErlauben = Zerm.TYP_MATERIAL; 
		Update->Page2Write ();
	}
}

void CPage1::OnCopy ()
{
	if (m_Mdn.IsWindowEnabled ())
	{
		if (Frame != NULL)
		{
			if (Frame->IsKindOf (RUNTIME_CLASS (CDialog)))
			{
//				((CDialog *) Frame)->EndDialog (0);
			}
			else
			{
//					Frame->GetParent ()->DestroyWindow ();
//					return FALSE;
			}
		}
	}
	else
	{
//		DisableTabControl (TRUE);
//		EnableFields (FALSE);
//		m_Schnitt.SetFocus ();
//		Schnitt.rollbackwork ();
	}
//	return TRUE;
}
BOOL CPage1::Print (HWND m_hwnd)
{
	if (m_Mdn.IsWindowEnabled ())
	{
		WorkPrint.EditZerl(m_hwnd);
	}
	else
	{
		WorkPrint.PrintZerl(m_hwnd,Schnitt.schnitt.mdn,Schnitt.schnitt.schnitt);
	}
	return TRUE;
}
void CPage1::OnPaste ()
{
	if (m_Mdn.IsWindowEnabled ())
	{
		if (Frame != NULL)
		{
			if (Frame->IsKindOf (RUNTIME_CLASS (CDialog)))
			{
//				((CDialog *) Frame)->EndDialog (0);
			}
			else
			{
//					Frame->GetParent ()->DestroyWindow ();
//					return FALSE;
			}
		}
	}
	else
	{
	}
}

void CPage1::OnEnChangeSchnitt()
{
	CString Text;
	/*
	if (NeuerSchnitt == TRUE)
	{
		sprintf(ctext,"Schnitt Neuanlage");
		GetParentOwner ()->SetWindowText (_T(ctext));
		GetParent()->GetParent()->GetParent()-> SetWindowText (_T(ctext));
	}
	else
	{
	*/
	Text.Format("Schnitt %ld",Schnitt.schnitt.schnitt);
		GetParentOwner ()->SetWindowText (Text);

	Text.Format("%s",Schnitt.schnitt.schnitt_txt);
		GetParent()->GetParent()->GetParent()-> SetWindowText (Text);
//	}
}

void CPage1::OnEnSetfocusSchnitt()
{
	if (Schnitt.schnitt.schnitt == -1)
	{  
		CString Text;
		Text.Format("Schnitt %ld    === Hier kann jetzt eine neue Schnittf�hrungsnummer vergeben werden === ",Schnitt.schnitt.schnitt);
		GetParentOwner ()->SetWindowText (Text);

		Text.Format("%s    === Hier kann jetzt eine neue Schnittf�hrungsnummer vergeben werden === ",Schnitt.schnitt.schnitt_txt);
		GetParent()->GetParent()->GetParent()-> SetWindowText (Text);
	}
}

BOOL CPage1::OnSetActive ()
{
	Form.Show();
	return TRUE;
}
