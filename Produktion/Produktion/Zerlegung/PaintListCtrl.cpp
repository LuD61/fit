#include "StdAfx.h"
#include "paintlistctrl.h"


int CAttributes::GetAttribute (int col)
{
	FirstPosition ();
	CAttribute *att;
	while ((att = (CAttribute *) GetNext ()) != NULL)
	{
		if (att->col == col)
		{
			return att->attribute;
		}
	}
	return -1;
}

CPaintListCtrl::CPaintListCtrl(void)
{
	Color = RGB (255,255,255);
	AlternateColor = RGB (255,255,128);
	SelectedColor = RGB (210, 210, 255);
	IsAlternateColor = FALSE;
	VLines = 0;
	HLines = 0;
	Attributes.Init ();
}

CPaintListCtrl::~CPaintListCtrl(void)
{
	Attributes.DestroyAll ();
}

BEGIN_MESSAGE_MAP(CPaintListCtrl, CListCtrl)
	ON_WM_PAINT ()
	ON_WM_LBUTTONDOWN ()
	ON_WM_KEYDOWN ()
END_MESSAGE_MAP()

void CPaintListCtrl::OnPaint ()
{

	CRect cRect;
	GetClientRect (&cRect);
	int count = GetItemCount ();
//	CListCtrl::OnPaint ();
//	return;
	PAINTSTRUCT ps;
	CDC *cDC = BeginPaint (&ps);
//	CDC *cDC = GetDC ();
	CFont *oldfont = cDC->SelectObject (GetFont ());
	for (int i = 0; i < count; i ++)
	{
		CRect ItemRect;
		GetItemRect (i, &ItemRect, LVIR_BOUNDS);
		if (ItemRect.bottom < cRect.top) continue;
		if (ItemRect.top > cRect.bottom) break;
	    DWORD status = GetItemState (i, LVIS_SELECTED);
	    if ((status & LVIS_SELECTED) != 0)
		{
			 cDC->FillSolidRect (&ItemRect, SelectedColor);
		}
		else if ((i % 2) == 0)
		{
			cDC->FillSolidRect (&ItemRect, Color);
		}
		else
		{
			cDC->FillSolidRect (&ItemRect, AlternateColor);
		}
		HDITEM hdi;
		CHeaderCtrl *hCtrl = GetHeaderCtrl ();
		int colcount = hCtrl->GetItemCount ();
		CRect rect;
		for (int c = 0; c < colcount; c ++)
		{
			hdi.mask = HDI_FORMAT;
			hCtrl->GetItem (c, &hdi);
			GetSubItemRect (i, c, LVIR_LABEL, rect);  
			if ((rect.right - rect.left) <= 0) continue;
			CString Text = GetItemText (i, c);
			if (Attributes.GetAttribute (c) == CAttribute::CheckBox)
			{
				PrintCheckBox (cDC, &rect, Text);
				continue;
			}
			CSize tsize = cDC->GetTextExtent (Text);
			int cy = rect.bottom - rect.top;
			int y = rect.top + (cy - tsize.cy) / 2;
			if (y < rect.top) y = rect.top;
			rect.left += 4;
			rect.right -= 4;
            int x = rect.left;
			if ((hdi.fmt & HDF_RIGHT) != 0)
			{
				x = rect.right - tsize.cx;
				if (x < rect.left) x = rect.left;
			}
			else if ((hdi.fmt & HDF_CENTER) != 0)
			{
				int cx = rect.right - rect.left;
				x = rect.left + (cx - tsize.cx) / 2;
				if (x < rect.left) x = rect.left;
			}

			CSize psize (0,0);
			if ((x + tsize.cx) > rect.right)
			{
			    psize = cDC->GetTextExtent ("...");
				tsize.cx += psize.cx;
			}

			while ((tsize.cx > psize.cx) && (x + tsize.cx) > rect.right)
			{
				int len = Text.GetLength () - 1;
				Text = Text.Left (len);
			    tsize = cDC->GetTextExtent (Text);
				tsize.cx += psize.cx;
			}
			if (psize.cx > 0)
			{
				Text += "...";
			}
			cDC->TextOut (x, y, Text);
		}
	}
	cDC->SelectObject (oldfont);
    DWORD style = GetExtendedStyle ();
	if (VLines != 0 || HLines != 0)
	{
		PrintGridLines (cDC, VLines, HLines);
	}
	else if ((style & LVS_EX_GRIDLINES) != 0)
	{
		PrintGridLines (cDC, 1, 1);
	}
	EndPaint (&ps);
}

void CPaintListCtrl::OnLButtonDown (UINT cFlags, CPoint point)
{
	CListCtrl::OnLButtonDown (cFlags, point);
	InvalidateRect (NULL, FALSE);
	UpdateWindow ();
}

void CPaintListCtrl::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	CListCtrl::OnKeyDown(nChar, nRepCnt, nFlags);
	InvalidateRect (NULL, FALSE);
	UpdateWindow ();
}

void CPaintListCtrl::PrintGridLines (CDC *cDC, int VLines, int HLines)
{
	CRect listRect;
	CRect colRect;
	CRect hRect;

	GetClientRect (&listRect);
	CHeaderCtrl *Header = GetHeaderCtrl (); 
    Header->GetClientRect (&hRect);
	int Items = Header->GetItemCount ();

	CPen Pen;
	Pen.CreatePen (PS_SOLID, 1, RGB (195, 195, 195));
//	Pen.CreatePen (PS_SOLID, 1, RGB (120, 120, 120));
	CPen *oldPen = cDC->SelectObject (&Pen);

	if (VLines)
	{
		for (int i = 0; i < Items; i ++)
		{
			GetSubItemRect (0, i, LVIR_BOUNDS, colRect);
			if (colRect.left > listRect.left &&
				colRect.left < listRect.right)
			{
				cDC->MoveTo (colRect.left, hRect.bottom + 2);
				cDC->LineTo (colRect.left, listRect.bottom);
			}
			if (colRect.right > listRect.left &&
				colRect.right < listRect.right)
			{
				cDC->MoveTo (colRect.right, hRect.bottom + 2);
				cDC->LineTo (colRect.right, listRect.bottom);
			}
		}
	}

    if (HLines == 1)
	{
		GetSubItemRect (0, 1, LVIR_LABEL, colRect);
		int ydiff = colRect.bottom - colRect.top;
		int ItemCount = GetItemCount ();
//		int y = ydiff + hRect.bottom;
		int y = colRect.top;
		for (int i = 0; i < ItemCount; y += ydiff, i ++)
		{
			cDC->MoveTo (0, y);
			cDC->LineTo (listRect.right, y);
		}
		for (; y < listRect.bottom; y += ydiff)
		{
			cDC->MoveTo (0, y);
			cDC->LineTo (listRect.right, y);
		}
	}

    if (HLines == 2)
	{
		GetSubItemRect (0, 1, LVIR_LABEL, colRect);
		int ydiff = colRect.bottom - colRect.top;
		int ItemCount = GetItemCount ();
		int y = hRect.bottom;
		for (int i = 0; i < ItemCount; y += ydiff, i ++)
		{
			if (i == 0) 
			{
				continue;
			}
		    CString KunNr = GetItemText (i, 1);
			if (KunNr.Trim () == "") continue;
			cDC->MoveTo (0, y);
			cDC->LineTo (listRect.right, y);
		}
		cDC->MoveTo (0, y);
		cDC->LineTo (listRect.right, y);
	}
    cDC->SelectObject (oldPen);
}

void CPaintListCtrl::PrintCheckBox (CDC *cDC, CRect *ItemRect, CString& Text)
{
	int cx = 12;
	int cy = 12;

	int rx = ItemRect->right  - ItemRect->left;
	int ry = ItemRect->bottom - ItemRect->top;

	int x = (rx - cx) / 2;
	x = (x < 0) ? 0 : x;
	int y = (ry - cy) / 2;
	y = (y < 0) ? 0 : y;
	x += ItemRect->left;
	y += ItemRect->top;

	CPen Pen;
	CPen RedPen;
	Pen.CreatePen (PS_SOLID, 1, RGB (120, 120, 120));
	RedPen.CreatePen (PS_SOLID, 1, RGB (255, 0, 0));
	CPen *oldPen = cDC->SelectObject (&Pen);
	cDC->MoveTo (x, y);
	cDC->LineTo (x + cx, y);

	cDC->MoveTo (x, y);
	cDC->LineTo (x, y + cy);

	cDC->MoveTo (x + cx, y);
	cDC->LineTo (x + cx, y + cy);

	cDC->MoveTo (x + cx, y + cy);
	cDC->LineTo (x, y + cy);

	Text.Trim ();
	if (Text == "1")
	{
		cDC->SelectObject (&RedPen);
		cDC->MoveTo (x + 3, y + 5);
		cDC->LineTo (x + 3, y + cy - 1);
		cDC->MoveTo (x + 3, y + 5);
		cDC->LineTo (x + 4, y + cy - 1);
		cDC->MoveTo (x + 4, y + 5);
		cDC->LineTo (x + 5, y + cy - 1);

		cDC->MoveTo (x + 5, y + cy - 1);
		cDC->LineTo (x + cx - 1, y + 2);
		cDC->MoveTo (x + 5, y + cy - 2);
		cDC->LineTo (x + cx - 2, y + 2);
	}

    cDC->SelectObject (oldPen);
}

void CPaintListCtrl::AddAttribute (int col, int attribute)
{
	Attributes.Add (new CAttribute (col, attribute));
}