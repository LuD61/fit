// Page4.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "Zerlegung.h"
#include "UniFormField.h"
#include "StrFuncs.h"
#include "Util.h"
#include "Token.h"
#include "Page4.h"



// CPage4-Dialogfeld

IMPLEMENT_DYNAMIC(CPage4, CDbPropertyPage)
CPage4::CPage4()
	: CDbPropertyPage(CPage4::IDD)
{
	Basis = NULL;
	Mdn = NULL;
	Schnitt = NULL;
	Zerm = NULL;
	MdnAdr = NULL;
	HeadControls.Add (&m_Mdn);
	HeadControls.Add (&m_Schnitt); 
	PosControls.Add (&m_SchnittTxt); 
}

CPage4::~CPage4()
{
	Font.DeleteObject ();
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
}

void CPage4::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_MDN, m_Mdn);
	DDX_Control(pDX, IDC_MDN_NAME, m_MdnName);
	DDX_Control(pDX, IDC_SCHNITT, m_Schnitt);
	DDX_Control(pDX, IDC_SCHNITT_TXT, m_SchnittTxt);
	DDX_Control(pDX, IDC_LMDN, m_LMdn);
	DDX_Control(pDX, IDC_LSCHNITT, m_LSchnitt);
	DDX_Control(pDX, IDC_ZERLKOST_GROUP, m_ZerlkostGroup);
	DDX_Control(pDX, IDC_MARKTPREIS, m_Marktpreis);
	DDX_Control(pDX, IDC_KALKBASIS_COMBO, m_KalkbasisCombo);
	DDX_Control(pDX, IDC_LMarktPreis, m_LMarktpreis);
}



BEGIN_MESSAGE_MAP(CPage4, CDbPropertyPage)
	ON_WM_SIZE ()
	ON_BN_CLICKED(IDC_SCHNITTCHOICE , OnSchnittchoice)
	ON_CBN_SELCHANGE(IDC_KALKBASIS_COMBO, &CPage4::OnCbnSelchangeKalkbasisCombo)
	ON_EN_KILLFOCUS(IDC_MARKTPREIS, &CPage4::OnEnKillfocusMarktpreis)
	ON_EN_SETFOCUS(IDC_SCHNITT_TXT, &CPage4::OnEnSetfocusSchnittTxt)
END_MESSAGE_MAP()


// CPage4-Meldungshandler
BOOL CPage4::OnInitDialog()
{

	CPropertyPage::OnInitDialog();
	Mdn   = &Basis->Mdn;
	Schnitt = &Basis->Schnitt;
	Zerm = &Basis->Zerm;
	MdnAdr = &Basis->MdnAdr;


	CRect pRect;
	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
		lFont.CreatePointFont (85, _T("Courier"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
		lFont.CreatePointFont (95, _T("Courier New"));
	}
    GetParent ()->GetParent ()->GetWindowRect (&pRect);
    GetParent ()->GetParent ()->GetParent ()->ScreenToClient (&pRect);
    GetParent ()->MoveWindow (&pRect);
    Form.Add (new CFormField (&m_Mdn,EDIT,        (short *) &Mdn->mdn.mdn, VSHORT));
    Form.Add (new CUniFormField (&m_MdnName,EDIT, (LPTSTR) MdnAdr->adr.adr_krz, VCHAR));
    Form.Add (new CFormField (&m_Schnitt,EDIT,    (long *) &Schnitt->schnitt.schnitt, VLONG));
    Form.Add (new CUniFormField (&m_SchnittTxt,EDIT, (LPTSTR) Schnitt->schnitt.schnitt_txt, VCHAR));
	Form.Add (new CFormField    (&m_KalkbasisCombo,COMBOBOX, (short *) &kalk_bas, VSHORT));
    Form.Add (new CUniFormField (&m_Marktpreis,EDIT, (long *) &Schnitt->PrGrStuf, VLONG));

    CtrlGrid.Create (this, 20, 20);
	CtrlGrid.SetFont (&Font);
    CtrlGrid.SetBorder (12, 20);
    CtrlGrid.SetFontCellHeight (this, &Font);
    CtrlGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand
	//Grid Mdn
		MdnGrid.Create (this, 2, 2);
		MdnGrid.SetBorder (0, 0);
		MdnGrid.SetGridSpace (0, 0);
		CCtrlInfo *c_Mdn = new CCtrlInfo (&m_Mdn, 0, 0, 1, 1);
		MdnGrid.Add (c_Mdn);
		CtrlGrid.CreateChoiceButton (m_MdnChoice, IDC_MDNCHOICE, this);
		CCtrlInfo *c_MdnChoice = new CCtrlInfo (&m_MdnChoice, 1, 0, 1, 1);
		MdnGrid.Add (c_MdnChoice);

	//Grid Schnitt
		SchnittGrid.Create (this, 2, 2);
	    SchnittGrid.SetBorder (0, 0);
	    SchnittGrid.SetGridSpace (0, 0);
		CCtrlInfo *c_Schnitt = new CCtrlInfo (&m_Schnitt, 0, 0, 1, 1);
		SchnittGrid.Add (c_Schnitt);
		CtrlGrid.CreateChoiceButton (m_SchnittChoice, IDC_SCHNITTCHOICE, this);
		CCtrlInfo *c_SchnittChoice = new CCtrlInfo (&m_SchnittChoice, 1, 0, 1, 1);
		SchnittGrid.Add (c_SchnittChoice);


//Mandant
	CCtrlInfo *c_LMdn     = new CCtrlInfo (&m_LMdn, 1, 0, 1, 1); 
	CtrlGrid.Add (c_LMdn);
	CCtrlInfo *c_MdnGrid   = new CCtrlInfo (&MdnGrid, 2, 0, 1, 1); 
	CtrlGrid.Add (c_MdnGrid);
	CCtrlInfo *c_MdnName     = new CCtrlInfo (&m_MdnName, 3, 0, 3, 1); 
	CtrlGrid.Add (c_MdnName);

//Schnitt
	CCtrlInfo *c_LSchnitt     = new CCtrlInfo (&m_LSchnitt, 1, 1, 1, 1); 
	CtrlGrid.Add (c_LSchnitt);
	CCtrlInfo *c_SchnittGrid   = new CCtrlInfo (&SchnittGrid, 2, 1, 1, 1); 
	CtrlGrid.Add (c_SchnittGrid);
	CCtrlInfo *c_SchnittTxt     = new CCtrlInfo (&m_SchnittTxt, 3, 1, 3, 1); 
	CtrlGrid.Add (c_SchnittTxt);


	//Grid ZerlKosten
		ZerlKostenGrid.Create (this, 16, 16);
		ZerlKostenGrid.SetBorder (0, 0);
		ZerlKostenGrid.SetCellHeight (15);
		ZerlKostenGrid.SetFontCellHeight (this);
		ZerlKostenGrid.SetFontCellHeight (this, &Font);
		ZerlKostenGrid.SetGridSpace (12, 12);

        int danf = 2;
		CCtrlInfo *c_ZerlkostGroup     = new CCtrlInfo (&m_ZerlkostGroup, 0, 0, 99, 99); 
		ZerlKostenGrid.Add (c_ZerlkostGroup);

//ZerlKostenGrid hinzufügen
	CCtrlInfo *c_ZerlKostenGrid     = new CCtrlInfo (&ZerlKostenGrid, 1, 3, 99, 99); 
	CtrlGrid.Add (c_ZerlKostenGrid);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);

	FillKalkbasisCombo ();

	Form.Show ();

	CtrlGrid.Display ();



	return TRUE;
}
void CPage4::OnSize (UINT nType, int cx, int cy) 
{
}
HBRUSH CPage4::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}

	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return CDialog::OnCtlColor (pDC, pWnd,nCtlColor);
}

BOOL CPage4::PreTranslateMessage(MSG* pMsg)
{

	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{
		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
//				Write (1);
//				Update->Back();
				Update->Write();
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{
				if (GetKeyState (VK_SHIFT) >= 0)
				{
					if (OnReturn ())
					{
						return TRUE;
					}
				}
				else
				{
					if (OnKeyup ())
					{
						return TRUE;
					}
				}
				break;
			}


			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F5)
			{
				Update->Back ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F7)
			{
				OnDelete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
			Write (1);
				Update->Back ();
//				Update->Write ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F9)
			{
//				if (GetFocus () == &m_Mdn)
//				{
//					OnMdnchoice ();
//					return TRUE;
//				}
			return TRUE;
			}
	}
	return FALSE;
}

void CPage4::UpdatePage ()
{
//	MaTree.Read ();
//	ZerlZerllist.Update (0);
	kalk_bas = Schnitt->schnitt.kalk_bas;
	Form.Show ();

}
void CPage4::GetPage ()
{
	Form.Get ();
}

BOOL CPage4::OnSetActive ()
{
	UpdatePage ();
	DisableFelder ();
	return TRUE;
}


BOOL CPage4::Write (BOOL YesNo)
{
	GetPage();
//	Basis->Write();
	return TRUE;
}
BOOL CPage4::Back ()
{
	int di = 0;
//	MaTree.Write (TRUE);
	return TRUE;
}
void CPage4::OnDelete ()
{
//	MaTree.Delete() ;

}
BOOL CPage4::Print (HWND m_hwnd)
{
//	MaTree.Print(m_hwnd) ;
	return TRUE;
}
BOOL  CPage4::OnReturn ()
{
	CWnd *Control = GetFocus ();


	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
		Control->SetFocus ();
	}
	return TRUE;
}
BOOL  CPage4::OnKeyup ()
{
	CWnd *Control = GetFocus ();
	Control = GetNextDlgTabItem (Control, TRUE);
	if (Control != NULL)
	{
		Control->SetFocus ();
	}
	return TRUE;
}
void CPage4::OnSchnittchoice ()
{
	Basis->OnSchnittchoice ();
}

void CPage4::FillKalkbasisCombo ()
{
	CFormField *f = Form.GetFormField (&m_KalkbasisCombo);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
		Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 
		int cursor = -1;
		cursor = Ptabn.sqlcursor (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			                          _T("where ptitem = \"kalk_bas\" ")
									  _T("order by ptlfnr"));
		while (Ptabn.sqlfetch (cursor) == 0)
		{
			Ptabn.dbreadfirst ();

			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbez;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbez, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn.ptabn.ptwert,
				                             Ptabn.ptabn.ptbez);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
		m_KalkbasisCombo.SetDroppedWidth (280);
		f->SetSel (0);
		f->Get ();
		kalk_bas = Schnitt->schnitt.kalk_bas;
	}
}



void CPage4::OnCbnSelchangeKalkbasisCombo()
{
	int di = 0;
	Form.Get();
	Form.Show();
	Schnitt->schnitt.kalk_bas = kalk_bas;
	DisableFelder ();


}

void CPage4::OnEnKillfocusMarktpreis()
{
	Form.Get();
}
void CPage4::DisableFelder ()
{
	if (Schnitt->schnitt.kalk_bas == 2)
	{
			m_Marktpreis.ShowWindow (SW_SHOWNORMAL);
			m_LMarktpreis.ShowWindow (SW_SHOWNORMAL);
//			m_Marktpreis.EnableWindow (TRUE);
//			m_LMarktpreis.EnableWindow (TRUE);
	}
	else
	{
			m_Marktpreis.ShowWindow (SW_HIDE);
			m_LMarktpreis.ShowWindow (SW_HIDE);
//			m_Marktpreis.EnableWindow (FALSE);
//			m_LMarktpreis.EnableWindow (FALSE);
	}
}

void CPage4::OnEnSetfocusSchnittTxt()
{
	if (Zerm->PAGE2_WRITE == TRUE)
	{
		Zerm->ZerlTreeErlauben = Zerm->TYP_MATERIAL; 
		Update->Page2Write ();
	}

}
