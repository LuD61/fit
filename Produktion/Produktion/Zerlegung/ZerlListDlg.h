#pragma once
#include "afxcmn.h"
#include "CtrlGrid.h"
#include "FormTab.h"
#include "afxwin.h"
#include "ZerlListCtrl.h"
#include "zerm.h"
#include "zerm_mat_v.h"
#include "A_bas.h"
#include "a_mat.h"
#include "Page1.h"
#include "UpdateEvent.h"
#include "PageUpdate.h"
#include "vector"
#include "Sys_par.h"
#include "schnitt.h"


// CZerlListDlg-Dialogfeld

class CZerlListDlg : public CDialog, public CUpdateEvent
{
	DECLARE_DYNAMIC(CZerlListDlg)

public:
	CZerlListDlg(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CZerlListDlg();

// Dialogfelddaten
	enum { IDD = IDD_ZERLLIST };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();    // DDX/DDV-Unterstützung
    virtual BOOL PreTranslateMessage(MSG* pMsg);
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 
	virtual void OnSize (UINT nType, int cx, int cy);

	DECLARE_MESSAGE_MAP()
public:
	int dcs_a_par;
	int waa_a_par;
	CCtrlGrid CtrlGrid;
	COLORREF DlgBkColor;
	HBRUSH DlgBrush;
	CFormTab Form;
	CFont Font;
	A_BAS_CLASS *A_bas;
	A_MAT_CLASS *A_mat;
	ZERM_CLASS *Zerm;
	ZERM_MAT_V_CLASS *Zerm_mat_v;
	SCHNITT_CLASS *Schnitt;
	SYS_PAR_CLASS *Sys_par;
	CPage1 *Page1;
	CPageUpdate *PageUpdate;

	void SetBasis (CPage1 *Basis);
	void SetSelectedItem (int idx);
	virtual void Update (short);
	virtual void Read ();
	virtual void Delete ();
	BOOL HasFocus ();
	afx_msg void OnLvnItemchangedEanList(NMHDR *pNMHDR, LRESULT *pResult);
	void SetSysPar();
	virtual void DeleteRow ();
public:
	CZerlListCtrl m_ZerlList;
public:
	CStatic m_ZerlListText;
};
