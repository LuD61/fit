#include "stdafx.h"
#include "WorkPrint.h" 
int Listenauswahl = 1;   // wenn 1 : Auswahl mehrerer Reports
int Hauptmenue = 0;      // wenn 1 : Auswahl edit oder print �ber Menue
static char Nutzer[65];
static char LNutzer[65];
CHAR szFilename[128+1] = "*.lst";
static char Listenname[512] = "Zerlegung";
const int LUL_LANGUAGE = CMBTLANG_GERMAN;
const int CTEXTMAX = 512;
const int LEERMATDIM = 21 ;	// 041206 : 20 Felder

static char FILENAME_DEFAULT[] = "d:\\user\\fit\\format\\LL\\Zerlegung.lst ........................................................" ; 

void WORKPRINT_CLASS::Variablendefinition (HJOB hJob)
{
    LlDefineVariableExt(hJob, "Schnitt.mdn",        "1", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "Schnitt.schnitt_txt","Schwein Beispiel", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "Schnitt.schnitt_hin","Hinweistext", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "Schnitt.schnitt_mat",      "123456", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "Schnitt.schnitt_stk",        "100", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "Schnitt.ek",        "2.5", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "Schnitt.ek_pr",        "3", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "Schnitt.ek_gew",        "510", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "Schnitt.zer_dat",        "24.10.2007", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "Schnitt.kalk_dat",        "24.10.2007", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "Schnitt.hdkl",        "R", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "Schnitt.BasisFWP",        "100", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "Schnitt_mat.a_bz1",        "S-H�lften", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "Mdn.mdn",        "1", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "Mdn.adr.adr",        "1", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "Mdn.adr.adr_krz",        "Mdn 1", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "Mdn.adr.adr_nam1",        "Mandant 1", LL_TEXT, NULL);
}

void WORKPRINT_CLASS::VariablenUebergabe ( HJOB hJob, char szTemp2[], int nRecno )
{
	Felduebergabe(0,hJob,"schnitt.mdn","%hd",&Schnitt.schnitt.mdn);
	Felduebergabe(0,hJob,"schnitt.schnitt_mat","%hd",&Schnitt.schnitt.schnitt_mat);
	Felduebergabe(0,hJob,"schnitt.schnitt_stk","%s",&Schnitt.schnitt.schnitt_stk);
	Felduebergabe(0,hJob,"schnitt.ek","%.2lf",&Schnitt.schnitt.ek);
	Felduebergabe(0,hJob,"schnitt.ek_pr","%.2lf",&Schnitt.schnitt.ek_pr);
	Felduebergabe(0,hJob,"schnitt.ek_gew","%.2lf",&Schnitt.schnitt.ek_gew);
	Felduebergabe(0,hJob,"schnitt.schnitt_txt","%s",&Schnitt.schnitt.schnitt_txt);
	Felduebergabe(0,hJob,"schnitt.schnitt_hin","%s",&Schnitt.schnitt.schnitt_hin);
	Felduebergabe(0,hJob,"schnitt.hdkl","%s",&Schnitt.schnitt.hdkl);
	Felduebergabe(0,hJob,"Schnitt_mat.BasisFWP","%s",&Schnitt.schnitt._basis_fwp);
	Felduebergabe(0,hJob,"Schnitt_mat.a_bz1","%s",&ABas.a_bas.a_bz1);

	Felduebergabe(0,hJob,"Mdn.mdn","%hd",&Mdn.mdn.mdn);
	Felduebergabe(0,hJob,"Mdn.adr.adr","%s",&Mdn.mdn.adr);
	Felduebergabe(0,hJob,"Mdn.adr.adr_krz","%s",&Adr.adr.adr_krz);
	Felduebergabe(0,hJob,"Mdn.adr.adr_nam1","%s",&Adr.adr.adr_nam1);
}

void WORKPRINT_CLASS::Felduebergabe(short kennung_feld,HJOB hJob,LPCSTR Feldname,LPCSTR Formatstring,void *Variable)
{
	char szTemp2[256];
	long *plongvar;
	double *pdoublevar;

	if (kennung_feld == 1)
	{
		if (LlPrintIsFieldUsed(hJob,Feldname)==0)
		{
			return;
		}
	}
	else
	{
		if (LlPrintIsVariableUsed(hJob,Feldname)==0)
		{
			return;
		}
	}
	if (strlen (Formatstring) != strcspn( Formatstring, "s"))  
	{
		if (kennung_feld == 1)
		{
			LlDefineFieldExt(hJob, Feldname,(char*)Variable , LL_TEXT, NULL);
		}
		else
		{
			LlDefineVariableExt(hJob, Feldname,(char*)Variable , LL_TEXT, NULL);
		}
	}
	else if (strlen (Formatstring) != strcspn( Formatstring, "d"))  
	{
		plongvar = (long*) Variable;
		sprintf(szTemp2, Formatstring, *plongvar );
		if (kennung_feld == 1)
		{
			LlDefineFieldExt(hJob, Feldname,        szTemp2, LL_NUMERIC, NULL);
		}
		else
		{
			LlDefineVariableExt(hJob, Feldname,        szTemp2, LL_NUMERIC, NULL);
		}
	}
	else if (strlen (Formatstring) != strcspn( Formatstring, "f"))  
	{
		pdoublevar = (double*) Variable;
		sprintf(szTemp2, Formatstring, *pdoublevar  );
		if (kennung_feld == 1)
		{
			LlDefineFieldExt(hJob, Feldname,        szTemp2, LL_NUMERIC, NULL);
		}
		else
		{
			LlDefineVariableExt(hJob, Feldname,        szTemp2, LL_NUMERIC, NULL);
		}
	}
}

void WORKPRINT_CLASS::Felderdefinition (HJOB hJob)
{
    LlDefineFieldExt(hJob, "zerm.zerm_aus_mat",        "123456", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "zerm.zerm_mat",        "123456", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "zerm.a_kz",        "GT", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "zerm.fix",        "V", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "zerm.fwp",        "192.65", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "zerm.zerm_gew",        "71", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "zerm.zerm_gew_ant",        "13.92", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "zerm.dm_100",        "13.92", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "zerm.zerm_teil",        "0", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "zerm.mat_o_b",        "1", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "zerm.hk_vollk",        "1", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "zerm.hk_teilk",        "1", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "zerm.vollkosten_abs",        "1", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "zerm.vollkosten_proz",        "1", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "zerm.teilkosten_abs",        "1", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "zerm.teilkosten_proz",        "1", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "zerm.a_z_f",        "1", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "zerm.vk_pr",        "1", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "zerm.kosten_zutat",        "1", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "zerm.kosten_vpk",        "1", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "zerm.a_typ",        "1", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "zerm.kost_bz",        "S R�ckensteak mit Kette", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "zerm.kosten",        "1", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "zerm.pr_zutat",        "1", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "zerm.pr_vpk",        "1", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "zerm.pr_kosten",        "1", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "zerm.pr_vk_netto",        "1", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "zerm.kosten_fix",        "1", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "zerm.pr_vk_eh",        "1", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "zerm.kosten_gh",        "1", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "zerm.kosten_eh",        "1", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "zerm.lfd",        "1", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "zerm.list_lfd",        "1", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "zerm.list_ebene",        "1", LL_NUMERIC, NULL);
}
void WORKPRINT_CLASS::FelderUebergabe ( HJOB hJob, char szTemp2[], int nRecno )
{
	Felduebergabe(1,hJob,"zerm.zerm_aus_mat","%ld",&Zerm.zerm.zerm_aus_mat);
	Felduebergabe(1,hJob,"zerm.zerm_mat","%ld",&Zerm.zerm.zerm_mat);
	/**
	CString Message;
	Message.Format (_T("zerm.zerm_mat=%ld"), Zerm.zerm.zerm_mat);         //testtesttest
     AfxMessageBox (_T(Message));
	 **/
	Felduebergabe(1,hJob,"zerm.zerm_gew_ant","%.3lf",&Zerm.zerm.zerm_gew_ant);
	Felduebergabe(1,hJob,"zerm.a_kz","%s",&Zerm.zerm.a_kz);
	Felduebergabe(1,hJob,"zerm.fix","%s",&Zerm.zerm.fix);
	Felduebergabe(1,hJob,"zerm.fwp","%.2lf",&Zerm.zerm.fwp);
	Felduebergabe(1,hJob,"zerm.zerm_gew","%.3lf",&Zerm.zerm.zerm_gew);
	Felduebergabe(1,hJob,"zerm.zerm_gew_ant","%.3lf",&Zerm.zerm.zerm_gew_ant);
	Felduebergabe(1,hJob,"zerm.dm_100","%.3lf",&Zerm.zerm.dm_100);
	short dzerm_teil = atoi(Zerm.zerm.zerm_teil);
	Felduebergabe(1,hJob,"zerm.zerm_teil","%hd",&dzerm_teil);
	Felduebergabe(1,hJob,"zerm.mat_o_b","%.3lf",&Zerm.zerm.mat_o_b);
	Felduebergabe(1,hJob,"zerm.hk_vollk","%.3lf",&Zerm.zerm.hk_vollk);
	Felduebergabe(1,hJob,"zerm.hk_teilk","%.3lf",&Zerm.zerm.hk_teilk);
	Felduebergabe(1,hJob,"zerm.vollkosten_abs","%.3lf",&Zerm.zerm.vollkosten_abs);
	Felduebergabe(1,hJob,"zerm.vollkosten_proz","%.3lf",&Zerm.zerm.vollkosten_proz);
	Felduebergabe(1,hJob,"zerm.teilkosten_abs","%.3lf",&Zerm.zerm.teilkosten_abs);
	Felduebergabe(1,hJob,"zerm.teilkosten_proz","%.3lf",&Zerm.zerm.teilkosten_proz);
	Felduebergabe(1,hJob,"zerm.a_z_f","%ld",&Zerm.zerm.a_z_f);
	Felduebergabe(1,hJob,"zerm.vk_pr","%.3lf",&Zerm.zerm.vk_pr);
	Felduebergabe(1,hJob,"zerm.kosten_zutat","%.3lf",&Zerm.zerm.kosten_zutat);
	Felduebergabe(1,hJob,"zerm.kosten_vpk","%.3lf",&Zerm.zerm.kosten_vpk);
	Felduebergabe(1,hJob,"zerm.a_typ","%hd",&Zerm.zerm.a_typ);
	Felduebergabe(1,hJob,"zerm.kost_bz","%s",&Zerm.zerm.kost_bz);
	Felduebergabe(1,hJob,"zerm.kosten","%.3lf",&Zerm.zerm.kosten);
	Felduebergabe(1,hJob,"zerm.pr_zutat","%.3lf",&Zerm.zerm.pr_zutat);
	Felduebergabe(1,hJob,"zerm.pr_vpk","%.3lf",&Zerm.zerm.pr_vpk);
	Felduebergabe(1,hJob,"zerm.pr_kosten","%.3lf",&Zerm.zerm.pr_kosten);
	Felduebergabe(1,hJob,"zerm.pr_vk_netto","%.3lf",&Zerm.zerm.pr_vk_netto);
	Felduebergabe(1,hJob,"zerm.pr_vk_eh","%.3lf",&Zerm.zerm.pr_vk_eh);
	Felduebergabe(1,hJob,"zerm.kosten_fix","%.3lf",&Zerm.zerm.kosten_fix);
	Felduebergabe(1,hJob,"zerm.kosten_gh","%.3lf",&Zerm.zerm.kosten_gh);
	Felduebergabe(1,hJob,"zerm.kosten_eh","%.3lf",&Zerm.zerm.kosten_eh);
	Felduebergabe(1,hJob,"zerm.lfd","%hd",&Zerm.zerm.lfd);
	Felduebergabe(1,hJob,"zerm.list_lfd","%hd",&Zerm.zerm.list_lfd);
	Felduebergabe(1,hJob,"zerm.list_ebene","%hd",&Zerm.zerm.list_ebene);
}

int WORKPRINT_CLASS::GetRecCount(short dmdn,long dschnitt)
{
	int danz = 0;
	Zerm.zerm.mdn = dmdn;
	Zerm.zerm.schnitt = dschnitt;
    int dsqlstatus = Zerm.dbreadfirst_Schnitt();
	while (dsqlstatus == 0)
	{
		danz++;
		dsqlstatus = Zerm.dbread_Schnitt();
	}
	return danz;
}

void WORKPRINT_CLASS::HoleFilename (HJOB hJob)
{
	char envchar[512] ;
    char *etc;
	char filename[512];
	FILE *fp ;
	etc = getenv ("BWS");
	if ( Nutzer[0] == '\0' )
		sprintf (filename, "%s\\format\\LL\\Zerlegung\\%s.lst", etc, Listenname);
	else
        sprintf (filename, "%s\\format\\LL\\%s\\Zerlegung\\%s.lst", etc, Nutzer, Listenname);


	sprintf ( LNutzer , "%s", Nutzer ) ;	// 060905
	fp = fopen ( filename, "r" ); 
	if ( fp== NULL )
	{
		Nutzer[0] = '\0' ;
		sprintf (filename, "%s\\format\\LL\\Zerlegung\\%s.lst", etc, Listenname);
	}
	else fclose ( fp) ;


	sprintf ( FILENAME_DEFAULT , filename) ;
	char * penvchar = getenv ("BWS");


	if ( Nutzer[0] == '\0' )	// 240304 
		sprintf ( envchar ,"%s\\format\\LL\\Zerlegung\\" , penvchar);
	else
		sprintf ( envchar ,"%s\\format\\LL\\Zerlegung\\%s\\" , penvchar, Nutzer);


	LlSetPrinterDefaultsDir(hJob, envchar) ;	// GrJ Printer-Datei
	penvchar = getenv ("TMPPATH");

	LlPreviewSetTempPath(hJob,penvchar) ;	// GrJ Vorschau-Datei
   	sprintf ( szFilename, FILENAME_DEFAULT );
}

void WORKPRINT_CLASS::PrintZerl (HWND m_hWnd, short dmdn, long dschnitt)
{
    CHAR szTemp[50], szTemp2[60], szBoxText[200];
	CHAR dmess[128] = "";
	HJOB hJob;
	int  nRet = 0;
	int  nRet1 = -1;
	strcpy(szTemp, " ");


	// GR: Initialisieren von List & Label.
	//     Es wird ein Job geoeffnet
	hJob = LlJobOpen(LUL_LANGUAGE);
	if (hJob==LL_ERR_BAD_JOBHANDLE)
	{
//		MessageBox("Job can't be initialized!", "List & Label Sample App", MB_OK|MB_ICONSTOP);
		return;
    }
	else if (hJob==LL_ERR_NO_LANG_DLL)
	{
//		MessageBox("Language file not found!\nEnsure that *.lng files can be found in your LuL DLL directory.",
//					"List & Label Sample App",
//					MB_OK|MB_ICONSTOP);
		return;
    }

 // Ich muss wohl noch meine Lizenz setzen , damit Lauff�hig beim Kunden !?(Siehe redist.txt)
//	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "q6GLOw");
	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "pn4SOw");


	//GR: Exporter aktivieren
//	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cm32l9ex.llx");
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll11ex.llx");

	// GR: Setzen der List & Label Optionen
	LlSetOption(hJob, LL_OPTION_NEWEXPRESSIONS, TRUE);

	// GR: Setze den Modus multiple datalines fuer Tabellen (wird nur fuer SAMPLE3.LST benoetigt)
	//     Der Benutzer hat nun mehrere Zeilendefinitionen in Tabellen zu verfuegung.
	LlSetOption(hJob, LL_OPTION_MULTIPLETABLELINES, TRUE);

	LlSetOption(hJob, LL_OPTION_WIZ_FILENEW, TRUE);

	// GR: Auswahl der Projekt-Datei �ber Datei-Auswahl-Dialog
    HoleFilename (hJob);
	if (Listenauswahl == 1)  //#LuD
    {
		nRet = LlSelectFileDlgTitleEx(hJob, m_hWnd, "", LL_PROJECT_LIST
								, szFilename,sizeof(szFilename), NULL);
		if (nRet < 0)
	   	{
    		if (nRet != LL_ERR_USER_ABORTED)
//				MessageBox("Error While Printing", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
			LlJobClose(hJob);
			return;
		}
		nRet1 = -1;
		nRet1 = (int) strcspn( szFilename, "Zerl" );
		if (strlen (szFilename) == (unsigned) nRet1) nRet1 = -1;
		while (nRet1 == -1 && nRet != LL_ERR_USER_ABORTED) 
        {
//			MessageBox("keine Berechtigung f�r diese Liste !", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
			nRet = LlSelectFileDlgTitleEx(hJob, m_hWnd, "", LL_PROJECT_LIST
								, szFilename,sizeof(szFilename), NULL);
			if (nRet < 0)
		   	{
	    		if (nRet != LL_ERR_USER_ABORTED)
//					MessageBox("Error While Printing", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
				LlJobClose(hJob);
				return;
			}
   		    nRet1 = -1;
		    nRet1 = (int) strcspn( szFilename, "_" );
   		    if (strlen (szFilename) == (unsigned) nRet1) nRet1 = -1; 
			 
        }
	}

    // GR: Zur�cksetzen der internen Variablen-Puffer
    LlDefineVariableStart(hJob); 

    Variablendefinition (hJob);
    Felderdefinition (hJob);

    // GR: Druck starten
	if (LlPrintWithBoxStart(hJob, LL_PROJECT_LIST, szFilename,
							LL_PRINT_EXPORT,
							LL_BOXTYPE_NORMALMETER, m_hWnd, "Printing...") < 0)
    {
//        MessageBox("Error While Printing", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
        LlJobClose(hJob);
        return;
    }

    // GR: Einstellungen bzw. optionen f�r den Drucker-Optionen-Dialog
	LlPrintSetOption(hJob, LL_PRNOPT_COPIES, 	LL_COPIES_HIDE);
    LlPrintSetOption(hJob, LL_PRNOPT_STARTPAGE, 1);
	if (LlPrintOptionsDialog(hJob, m_hWnd, "Auswahl der Druckoptionen") < 0)
    {
        LlPrintEnd(hJob,0);
        LlJobClose(hJob);
        return;
    }

	//GR: Druckziel abfragen
	CString sMedia;
	LlPrintGetOptionString(hJob, LL_PRNOPTSTR_EXPORT, sMedia.GetBuffer(256), 256);
	sMedia.ReleaseBuffer();

    int  nRecCount = 1, nErrorValue = 0, nPrintFehler = 0, nLastPage, nRecno;
	char szPrinter[80], szPort[20];

    LlPrintGetPrinterInfo(hJob, szPrinter, sizeof(szPrinter), szPort, sizeof(szPort));

	nLastPage = LlPrintGetOption(hJob, LL_OPTION_LASTPAGE);
    nRecno = 1;

    // GR: Druckschleife
    //     Diese wird so lange wiederholt, bis saemtliche Daten abgearbeitet wurden, oder
    //     ein Fehler auftritt.
	nRecCount = WORKPRINT_CLASS::GetRecCount (dmdn,dschnitt) ;
	int di = WORKPRINT_CLASS::LeseFirstZerm (dmdn,dschnitt);   //       1.DATENSATZ lesen f�r Variablen�bergabe 
	if (di != 0) 
    {
		sprintf (dmess, "Datensatz nicht gefunden SQL:%hd",di);
		MessageBox (m_hWnd, dmess, "", MB_ICONERROR);
    			LlPrintEnd(hJob,0);
    			LlJobClose(hJob);
    			return;

	}

	WORKPRINT_CLASS::LeseFirstZerm (dmdn,dschnitt);   
	while (nRecno < nRecCount
			&& (LlPrintGetCurrentPage(hJob) <= nLastPage)
			&& (nErrorValue == 0 || nErrorValue == LL_WRN_REPEAT_DATA))
   	{
	    VariablenUebergabe ( hJob, szTemp2, nRecno );

		nErrorValue = LlPrint(hJob);
		while (nRecno <= nRecCount 
			&& (nErrorValue == 0) 
			&& (LlPrintGetCurrentPage(hJob) <= nLastPage)
			&& (WORKPRINT_CLASS::LeseZerm (nPrintFehler) == 0))  //       DATENSATZ lesen

		{

		    FelderUebergabe ( hJob, szTemp2, nRecno );
    		// GR: Prozentbalken in der Fortschritts-Anzeige setzen
    		sprintf(szBoxText, "printing on %s %s", szPrinter, szPort);
    		nErrorValue = LlPrintSetBoxText(hJob, szBoxText, (100 * nRecno / nRecCount));
        	if (nErrorValue == LL_ERR_USER_ABORTED)
   			{
    			LlPrintEnd(hJob,0);
    			LlJobClose(hJob);
    			return;
   			}

    		// GR: Drucken der aktuellen Tabellenzeile
    		nErrorValue = LlPrintFields(hJob);
			nPrintFehler = nErrorValue;
			if (nErrorValue == LL_WRN_REPEAT_DATA)
			{
					// GR: Aktualisieren der seitenabhaengigen Variablen
			    WORKPRINT_CLASS::VariablenUebergabe ( hJob, szTemp2, nRecno );
				nErrorValue = LlPrint(hJob);
//			    FelderUebergabe ( hJob, szTemp2, nRecno );
//	    		nErrorValue = LlPrintFields(hJob);
			}


			if (nErrorValue == 0)	// US: everything fine, record could have been printed...
	    	{
				// US: ...but probably the user defined a filter condition!
				//     So before updating time dependent variables we have to check if the record really has been printed:
				if (LlPrintDidMatchFilter(hJob))
				{
					// GR: Aktualisieren der zeitabhaengigen Variable 'FixedVariable2'
					// US: update the time dependent variable 'FixedVariable2'
					//     coming after printig the list(s)
		    		sprintf(szTemp2, "FixedVariable2, record %d", nRecno);
		    		LlDefineVariable(hJob, "FixedVariable2", szTemp2);
		    	}

	    		if (nPrintFehler == 0) nRecno++;	// GR: gehe zum naechsten Datensatz
			}
		}
  	}

	// US: all records have been printed, now flush the table
	//     If footer doesn't fit to this page try again for the next page:

	nErrorValue = LlPrintFieldsEnd(hJob);
	if (nErrorValue == LL_WRN_REPEAT_DATA)
	{
		// GR: Aktualisieren der seitenabhaengigen Variablen
	    WORKPRINT_CLASS::VariablenUebergabe ( hJob, szTemp2, nRecno );
	    WORKPRINT_CLASS::FelderUebergabe ( hJob, szTemp2, nRecno );

		// US: ... and then try again:
		nErrorValue = LlPrintFieldsEnd(hJob);
	}

	if (nErrorValue == LL_WRN_REPEAT_DATA)	// US: footer still doesn't fit!
		MessageBox(m_hWnd,"Error because table is too small for footer!", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);

	//GR: Druck beenden
	LlPrintEnd(hJob,0);
	
	//GR: Druckziel = Preview? 
	if (sMedia=="PRV")
    {
        // GR: Beim Preview-Druck Preview anzeigen und dann Preview-Datei (.LL) loeschen
		char *penvchar = getenv ("TMPPATH");
        LlPreviewDisplay(hJob, szFilename, penvchar, m_hWnd);

        LlPreviewDeleteFiles(hJob, szFilename, penvchar);
    }
	// GR: Beenden des List & Label Jobs
	LlJobClose(hJob);
}

void WORKPRINT_CLASS::EditZerl (HWND m_hWnd)
{
	HJOB hJob;
	

	// GR: Initialisieren von List & Label.
	//     Es wird ein Job geoeffnet
	hJob = LlJobOpen(LUL_LANGUAGE);
	if (hJob==LL_ERR_BAD_JOBHANDLE)
	{
//		MessageBox("Job can't be initialized!", "List & Label Sample App", MB_OK|MB_ICONSTOP);
		return;
	}
	else if (hJob==LL_ERR_NO_LANG_DLL)
	{
//		MessageBox("Language file not found!\nEnsure that *.lng files can be found in your LuL DLL directory.",
//					"List & Label Sample App",
//					MB_OK|MB_ICONSTOP);
		return;
	}
	// Ich muss wohl noch meine Lizenz setzen , damit Lauff�hig beim Kunden (Siehe redist.txt)
// 	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "q6GLOw");
			LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "pn4SOw");

 
    HoleFilename (hJob);



	// GR: Setzen der List & Label Optionen
	LlSetOption(hJob, LL_OPTION_NEWEXPRESSIONS, TRUE);

	// GR: Setze den Modus multiple datalines fuer Tabellen (wird nur fuer SAMPLE3.LST benoetigt)
	//     Der Benutzer hat nun mehrere Zeilendefinitionen in Tabellen zu verfuegung.
	LlSetOption(hJob, LL_OPTION_MULTIPLETABLELINES, TRUE);

	LlSetOption(hJob, LL_OPTION_WIZ_FILENEW, TRUE);

	//GR: Exporter aktivieren
//	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cm32l9ex.llx");
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll11ex.llx");


	// GR: Auswahl der Projekt-Datei �ber Datei-Auswahl-Dialog
//	if (Listenauswahl == 1)  
//    {
		if (LlSelectFileDlgTitleEx(hJob, m_hWnd, "", LL_PROJECT_LIST|LL_FILE_ALSONEW
								, szFilename, sizeof(szFilename), NULL) < 0)
		{
	        LlJobClose(hJob);
//			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
			return;
		}
//	}

	// GR: Zur�cksetzen der internen Variablen-Puffer
    LlDefineVariableStart(hJob);

    // GR: Zur�cksetzen der internen Feld-Puffer
 

//	dbClass.opendbase ("bws");
    Variablendefinition (hJob);
    Felderdefinition (hJob);

	if (Listenauswahl != 1)  
    {
	   	sprintf ( szFilename, FILENAME_DEFAULT );
	}
	// GR: Aufruf des Designers
    if (LlDefineLayout(hJob, m_hWnd, "Design Rechnung", LL_PROJECT_LIST, szFilename) < 0)
    {
//        MessageBox("Error by calling LlDefineLayout", "Design Rechnung", MB_OK|MB_ICONEXCLAMATION);
        LlJobClose(hJob);
//		PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
        return;
    }
    // GR: Beenden des List & Label Jobs
    LlJobClose(hJob);

//	PostMessage(WM_COMMAND,ID_APP_EXIT,0l);


}

int WORKPRINT_CLASS::LeseZerm (int fehlercode)
{
	int di = 0;
	  if (fehlercode == LL_WRN_REPEAT_DATA)
	  {
         return 0;    // Hier kein fetch , da DS nicht mehr auf Seite passt , und nochmal 
					  // ausegeben werden mu�
	  }
	  di = Zerm.dbread_Schnitt();
	  return di;
}
int WORKPRINT_CLASS::LeseFirstZerm (short dmdn, long dschnitt)
{
	int dret = -1;
	Mdn.mdn.mdn = dmdn;
	dret = Mdn.dbreadfirst();
	Adr.adr.adr = Mdn.mdn.adr;
	dret = Adr.dbreadfirst();
	AMat.a_mat.mat = Schnitt.schnitt.schnitt_mat;
	dret = AMat.dbreadfirst();
	ABas.a_bas.a = AMat.a_mat.a;
	dret = ABas.dbreadfirst();
		

	Zerm.zerm.mdn = dmdn;
	Zerm.zerm.schnitt = dschnitt;
	Schnitt.schnitt.mdn = dmdn;
	Schnitt.schnitt.schnitt = dschnitt;
	dret = Schnitt.dbreadfirst();
	if (dret < 0) return dret;
    return Zerm.dbreadfirst_Schnitt();
}


