// Page3.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "Zerlegung.h"
#include "UniFormField.h"
#include "StrFuncs.h"
#include "Util.h"
#include "Token.h"
#include "Page3.h"



// CPage3-Dialogfeld

IMPLEMENT_DYNAMIC(CPage3, CDbPropertyPage)
CPage3::CPage3()
	: CDbPropertyPage(CPage3::IDD)
{
	Basis = NULL;
	Mdn = NULL;
	Schnitt = NULL;
	Zerm = NULL;
	MdnAdr = NULL;
	HeadControls.Add (&m_Mdn);
	HeadControls.Add (&m_Schnitt); 
	PosControls.Add (&m_SchnittTxt); 
	ChoiceKostArt = NULL;
	PageUpdate = NULL;
}

CPage3::~CPage3()
{
	Font.DeleteObject ();
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
}

void CPage3::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_MDN, m_Mdn);
	DDX_Control(pDX, IDC_MDN_NAME, m_MdnName);
	DDX_Control(pDX, IDC_SCHNITT, m_Schnitt);
	DDX_Control(pDX, IDC_SCHNITT_TXT, m_SchnittTxt);
	DDX_Control(pDX, IDC_LMDN, m_LMdn);
	DDX_Control(pDX, IDC_LSCHNITT, m_LSchnitt);
	DDX_Control(pDX, IDC_ZERLKOST_GROUP, m_ZerlkostGroup);
	DDX_Control(pDX, IDC_LGRUND, m_LGrund);
	DDX_Control(pDX, IDC_GRUNDKOART, m_GrundKoart);
	DDX_Control(pDX, IDC_GRUNDKOART_BEZ, m_GrundKoartBez);
	DDX_Control(pDX, IDC_GRUNDKOART_WERT, m_GrundKoartWert);
	DDX_Control(pDX, IDC_LKOART, m_LKoart);
	DDX_Control(pDX, IDC_LWERT, m_LWert);
	DDX_Control(pDX, IDC_LGROB, m_LGrob);
	DDX_Control(pDX, IDC_GROBKOART, m_GrobKoart);
	DDX_Control(pDX, IDC_GROBKOART_BEZ, m_GrobKoartBez);
	DDX_Control(pDX, IDC_GROBKOART_WERT, m_GrobKoartWert);
	DDX_Control(pDX, IDC_FEINZERL_GROUP, m_FeinzerlGroup);
	DDX_Control(pDX, IDC_VM, m_VM);
	DDX_Control(pDX, IDC_VM_BEZ, m_VMBez);
	DDX_Control(pDX, IDC_VM_WERT, m_VMWert);
	DDX_Control(pDX, IDC_FEINKOART2, m_VT);
	DDX_Control(pDX, IDC_FEINKOART2_BEZ, m_VTBez);
	DDX_Control(pDX, IDC_FEINKOART2_WERT, m_VTWert);
	DDX_Control(pDX, IDC_FEINKOART3, m_ZA);
	DDX_Control(pDX, IDC_FEINKOART3_BEZ, m_ZABez);
	DDX_Control(pDX, IDC_FEINKOART3_WERT, m_ZAWert);
	DDX_Control(pDX, IDC_LVM, m_LVM);
	DDX_Control(pDX, IDC_LVT, m_LVT);
	DDX_Control(pDX, IDC_LZA, m_LZA);
}



BEGIN_MESSAGE_MAP(CPage3, CDbPropertyPage)
	ON_WM_SIZE ()
	ON_BN_CLICKED(IDC_SCHNITTCHOICE , OnSchnittchoice)
	ON_BN_CLICKED(IDC_GRUNDKOARTCHOICE , OnKostArtChoiceGrund)
	ON_BN_CLICKED(IDC_GROBKOARTCHOICE , OnKostArtChoiceGrob)
	ON_BN_CLICKED(IDC_VMCHOICE , OnKostArtChoiceVM)
	ON_BN_CLICKED(IDC_VTCHOICE , OnKostArtChoiceVT)
	ON_BN_CLICKED(IDC_ZACHOICE , OnKostArtChoiceZA)
	ON_EN_KILLFOCUS(IDC_GRUNDKOART_WERT, &CPage3::OnEnKillfocusGrundkoartWert)
	ON_EN_KILLFOCUS(IDC_GROBKOART_WERT, &CPage3::OnEnKillfocusGrobkoartWert)
	ON_EN_KILLFOCUS(IDC_GRUNDKOART, &CPage3::OnEnKillfocusGrundkoart)
	ON_EN_KILLFOCUS(IDC_GROBKOART, &CPage3::OnEnKillfocusGrobkoart)
	ON_EN_KILLFOCUS(IDC_VM_WERT, &CPage3::OnEnKillfocusVmWert)
	ON_EN_KILLFOCUS(IDC_FEINKOART2_WERT, &CPage3::OnEnKillfocusVtWert)
	ON_EN_KILLFOCUS(IDC_FEINKOART3_WERT, &CPage3::OnEnKillfocusZaWert)
	ON_EN_SETFOCUS(IDC_SCHNITT_TXT, &CPage3::OnEnSetfocusSchnittTxt)
END_MESSAGE_MAP()


// CPage3-Meldungshandler
BOOL CPage3::OnInitDialog()
{

	CPropertyPage::OnInitDialog();
	Mdn   = &Basis->Mdn;
	Schnitt = &Basis->Schnitt;
	Zerm = &Basis->Zerm;
	MdnAdr = &Basis->MdnAdr;


	CRect pRect;
	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
		lFont.CreatePointFont (85, _T("Courier"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
		lFont.CreatePointFont (95, _T("Courier New"));
	}
    GetParent ()->GetParent ()->GetWindowRect (&pRect);
    GetParent ()->GetParent ()->GetParent ()->ScreenToClient (&pRect);
    GetParent ()->MoveWindow (&pRect);
    Form.Add (new CFormField (&m_Mdn,EDIT,        (short *) &Mdn->mdn.mdn, VSHORT));
    Form.Add (new CUniFormField (&m_MdnName,EDIT, (LPTSTR) MdnAdr->adr.adr_krz, VCHAR));
    Form.Add (new CFormField (&m_Schnitt,EDIT,    (long *) &Schnitt->schnitt.schnitt, VLONG));
    Form.Add (new CUniFormField (&m_SchnittTxt,EDIT, (LPTSTR) Schnitt->schnitt.schnitt_txt, VCHAR));
    Form.Add (new CFormField (&m_GrundKoart,EDIT,    (long *) &Schnitt->schnitt.koart_grund, VSHORT));
    Form.Add (new CUniFormField (&m_GrundKoartBez,EDIT, (LPTSTR) Schnitt->GrundKoartBez, VCHAR));
    Form.Add (new CFormField (&m_GrundKoartWert,EDIT,    (double *) &Schnitt->schnitt._grund_kosten, VDOUBLE,6,2));
    Form.Add (new CFormField (&m_GrobKoart,EDIT,    (long *) &Schnitt->schnitt.koart_grob, VSHORT));
    Form.Add (new CUniFormField (&m_GrobKoartBez,EDIT, (LPTSTR) Schnitt->GrobKoartBez, VCHAR));
    Form.Add (new CFormField (&m_GrobKoartWert,EDIT,    (double *) &Schnitt->schnitt._grob_kosten, VDOUBLE,5,2));

    Form.Add (new CFormField (&m_VM,EDIT,    (long *) &Schnitt->schnitt.koart_vm, VSHORT));
    Form.Add (new CUniFormField (&m_VMBez,EDIT, (LPTSTR) Schnitt->VMKoartBez, VCHAR));
    Form.Add (new CFormField (&m_VMWert,EDIT,    (double *) &Schnitt->schnitt._vm_kosten, VDOUBLE,5,2));
    Form.Add (new CFormField (&m_VT,EDIT,    (long *) &Schnitt->schnitt.koart_vt, VSHORT));
    Form.Add (new CUniFormField (&m_VTBez,EDIT, (LPTSTR) Schnitt->VTKoartBez, VCHAR));
    Form.Add (new CFormField (&m_VTWert,EDIT,    (double *) &Schnitt->schnitt._vt_kosten, VDOUBLE,5,2));
    Form.Add (new CFormField (&m_ZA,EDIT,    (long *) &Schnitt->schnitt.koart_za, VSHORT));
    Form.Add (new CUniFormField (&m_ZABez,EDIT, (LPTSTR) Schnitt->ZAKoartBez, VCHAR));
    Form.Add (new CFormField (&m_ZAWert,EDIT,    (double *) &Schnitt->schnitt._za_kosten, VDOUBLE,5,2));

//	Form.Add (new CFormField (&m_FeinCombo1,COMBOBOX, (char *) SchnittKost_f.fein1_ptwert, VCHAR));
//	Form.Add (new CFormField (&m_VM,EDIT,    (double *) &SchnittKost_f.fein1_kost_art, VDOUBLE));

    CtrlGrid.Create (this, 20, 20);
	CtrlGrid.SetFont (&Font);
    CtrlGrid.SetBorder (12, 20);
    CtrlGrid.SetFontCellHeight (this, &Font);
    CtrlGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand
	//Grid Mdn
		MdnGrid.Create (this, 2, 2);
		MdnGrid.SetBorder (0, 0);
		MdnGrid.SetGridSpace (0, 0);
		CCtrlInfo *c_Mdn = new CCtrlInfo (&m_Mdn, 0, 0, 1, 1);
		MdnGrid.Add (c_Mdn);
		CtrlGrid.CreateChoiceButton (m_MdnChoice, IDC_MDNCHOICE, this);
		CCtrlInfo *c_MdnChoice = new CCtrlInfo (&m_MdnChoice, 1, 0, 1, 1);
		MdnGrid.Add (c_MdnChoice);

	//Grid Schnitt
		SchnittGrid.Create (this, 2, 2);
	    SchnittGrid.SetBorder (0, 0);
	    SchnittGrid.SetGridSpace (0, 0);
		CCtrlInfo *c_Schnitt = new CCtrlInfo (&m_Schnitt, 0, 0, 1, 1);
		SchnittGrid.Add (c_Schnitt);
		CtrlGrid.CreateChoiceButton (m_SchnittChoice, IDC_SCHNITTCHOICE, this);
		CCtrlInfo *c_SchnittChoice = new CCtrlInfo (&m_SchnittChoice, 1, 0, 1, 1);
		SchnittGrid.Add (c_SchnittChoice);


//Mandant
	CCtrlInfo *c_LMdn     = new CCtrlInfo (&m_LMdn, 1, 0, 1, 1); 
	CtrlGrid.Add (c_LMdn);
	CCtrlInfo *c_MdnGrid   = new CCtrlInfo (&MdnGrid, 2, 0, 1, 1); 
	CtrlGrid.Add (c_MdnGrid);
	CCtrlInfo *c_MdnName     = new CCtrlInfo (&m_MdnName, 3, 0, 3, 1); 
	CtrlGrid.Add (c_MdnName);

//Schnitt
	CCtrlInfo *c_LSchnitt     = new CCtrlInfo (&m_LSchnitt, 1, 1, 1, 1); 
	CtrlGrid.Add (c_LSchnitt);
	CCtrlInfo *c_SchnittGrid   = new CCtrlInfo (&SchnittGrid, 2, 1, 1, 1); 
	CtrlGrid.Add (c_SchnittGrid);
	CCtrlInfo *c_SchnittTxt     = new CCtrlInfo (&m_SchnittTxt, 3, 1, 3, 1); 
	CtrlGrid.Add (c_SchnittTxt);


	//Grid ZerlKosten
		ZerlKostenGrid.Create (this, 16, 16);
		ZerlKostenGrid.SetBorder (0, 0);
		ZerlKostenGrid.SetCellHeight (15);
		ZerlKostenGrid.SetFontCellHeight (this);
		ZerlKostenGrid.SetFontCellHeight (this, &Font);
		ZerlKostenGrid.SetGridSpace (12, 12);

        int danf = 2;
		CCtrlInfo *c_ZerlkostGroup     = new CCtrlInfo (&m_ZerlkostGroup, 0, 0, 99, 99); 
		ZerlKostenGrid.Add (c_ZerlkostGroup);
		CCtrlInfo *c_m_LKoart     = new CCtrlInfo (&m_LKoart, 2+danf, 1, 1, 1); 
		ZerlKostenGrid.Add (c_m_LKoart);
		CCtrlInfo *c_m_LWert     = new CCtrlInfo (&m_LWert, 4+danf, 1, 1, 1); 
		ZerlKostenGrid.Add (c_m_LWert);
		CCtrlInfo *c_m_LGrund      = new CCtrlInfo (&m_LGrund , 1, 2, danf+1, 3); 
		ZerlKostenGrid.Add (c_m_LGrund );
//Grid GrundKoart
			GrundKoartGrid.Create (this, 2, 2);
		    GrundKoartGrid.SetBorder (0, 0);
		    GrundKoartGrid.SetGridSpace (0, 0);
			CCtrlInfo *c_GrundKoart = new CCtrlInfo (&m_GrundKoart, 0, 0, 1, 1);
			GrundKoartGrid.Add (c_GrundKoart);
			CtrlGrid.CreateChoiceButton (m_GrundKoartChoice, IDC_GRUNDKOARTCHOICE, this);
			CCtrlInfo *c_GrundKoartChoice = new CCtrlInfo (&m_GrundKoartChoice, 1, 0, 1, 1);
			GrundKoartGrid.Add (c_GrundKoartChoice);

		CCtrlInfo *c_m_GrundKoart      = new CCtrlInfo (&GrundKoartGrid , 2+danf, 2, 1, 1); 
		ZerlKostenGrid.Add (c_m_GrundKoart );
		CCtrlInfo *c_m_GrundKoartBez      = new CCtrlInfo (&m_GrundKoartBez , 3+danf, 2, 1, 1); 
		ZerlKostenGrid.Add (c_m_GrundKoartBez );
		CCtrlInfo *c_m_GrundKoartWert      = new CCtrlInfo (&m_GrundKoartWert , 4+danf, 2, 1, 1); 
		ZerlKostenGrid.Add (c_m_GrundKoartWert );
		CCtrlInfo *c_m_LGrob      = new CCtrlInfo (&m_LGrob , 1, 3, danf+1, 3); 
		ZerlKostenGrid.Add (c_m_LGrob );

//Grid GrobKoart
			GrobKoartGrid.Create (this, 2, 2);
		    GrobKoartGrid.SetBorder (0, 0);
		    GrobKoartGrid.SetGridSpace (0, 0);
			CCtrlInfo *c_GrobKoart = new CCtrlInfo (&m_GrobKoart, 0, 0, 1, 1);
			GrobKoartGrid.Add (c_GrobKoart);
			CtrlGrid.CreateChoiceButton (m_GrobKoartChoice, IDC_GROBKOARTCHOICE, this);
			CCtrlInfo *c_GrobKoartChoice = new CCtrlInfo (&m_GrobKoartChoice, 1, 0, 1, 1);
			GrobKoartGrid.Add (c_GrobKoartChoice);
		CCtrlInfo *c_m_GrobKoart      = new CCtrlInfo (&GrobKoartGrid , 2+danf, 3, 1, 1); 
		ZerlKostenGrid.Add (c_m_GrobKoart );
		CCtrlInfo *c_m_GrobKoartBez      = new CCtrlInfo (&m_GrobKoartBez , 3+danf, 3, 1, 1); 
		ZerlKostenGrid.Add (c_m_GrobKoartBez );
		CCtrlInfo *c_m_GrobKoartWert      = new CCtrlInfo (&m_GrobKoartWert , 4+danf, 3, 1, 1); 
		ZerlKostenGrid.Add (c_m_GrobKoartWert );

		//Grid ZerlKostenFein
			ZerlKostenFeinGrid.Create (this, 12, 12);
			ZerlKostenFeinGrid.SetBorder (0, 0);
			ZerlKostenFeinGrid.SetCellHeight (15);
			ZerlKostenFeinGrid.SetFontCellHeight (this);
			ZerlKostenFeinGrid.SetFontCellHeight (this, &Font);
			ZerlKostenFeinGrid.SetGridSpace (12, 12);

			CCtrlInfo *c_FeinzerlGroup     = new CCtrlInfo (&m_FeinzerlGroup, 1, 0, 99, 99); 
			ZerlKostenFeinGrid.Add (c_FeinzerlGroup);
//Grid VM
			VMGrid.Create (this, 2, 2);
		    VMGrid.SetBorder (0, 0);
		    VMGrid.SetGridSpace (0, 0);
			CCtrlInfo *c_VM = new CCtrlInfo (&m_VM, 0, 0, 1, 1);
			VMGrid.Add (c_VM);
			CtrlGrid.CreateChoiceButton (m_VMChoice, IDC_VMCHOICE, this);
			CCtrlInfo *c_VMChoice = new CCtrlInfo (&m_VMChoice, 1, 0, 1, 1);
			VMGrid.Add (c_VMChoice);


			CCtrlInfo *c_m_LVM      = new CCtrlInfo (&m_LVM , 1, 1, 1, 1); 
			ZerlKostenFeinGrid.Add (c_m_LVM );
			CCtrlInfo *c_m_VM      = new CCtrlInfo (&VMGrid , 2, 1, 1, 1); 
			ZerlKostenFeinGrid.Add (c_m_VM );
			CCtrlInfo *c_m_VMBez      = new CCtrlInfo (&m_VMBez , 3, 1, 1, 1); 
			ZerlKostenFeinGrid.Add (c_m_VMBez );
			CCtrlInfo *c_m_VMWert      = new CCtrlInfo (&m_VMWert , 4, 1, 1, 1); 
			ZerlKostenFeinGrid.Add (c_m_VMWert );
//Grid VT
			VTGrid.Create (this, 2, 2);
		    VTGrid.SetBorder (0, 0);
		    VTGrid.SetGridSpace (0, 0);
			CCtrlInfo *c_VT = new CCtrlInfo (&m_VT, 0, 0, 1, 1);
			VTGrid.Add (c_VT);
			CtrlGrid.CreateChoiceButton (m_VTChoice, IDC_VTCHOICE, this);
			CCtrlInfo *c_VTChoice = new CCtrlInfo (&m_VTChoice, 1, 0, 1, 1);
			VTGrid.Add (c_VTChoice);


			CCtrlInfo *c_m_LVT      = new CCtrlInfo (&m_LVT , 1, 2, 1, 1); 
			ZerlKostenFeinGrid.Add (c_m_LVT );
			CCtrlInfo *c_m_VT      = new CCtrlInfo (&VTGrid , 2, 2, 1, 1); 
			ZerlKostenFeinGrid.Add (c_m_VT );
			CCtrlInfo *c_m_VTBez      = new CCtrlInfo (&m_VTBez , 3, 2, 1, 1); 
			ZerlKostenFeinGrid.Add (c_m_VTBez );
			CCtrlInfo *c_m_VTWert      = new CCtrlInfo (&m_VTWert , 4, 2, 1, 1); 
			ZerlKostenFeinGrid.Add (c_m_VTWert );
//Grid ZA
			ZAGrid.Create (this, 2, 2);
		    ZAGrid.SetBorder (0, 0);
		    ZAGrid.SetGridSpace (0, 0);
			CCtrlInfo *c_ZA = new CCtrlInfo (&m_ZA, 0, 0, 1, 1);
			ZAGrid.Add (c_ZA);
			CtrlGrid.CreateChoiceButton (m_ZAChoice, IDC_ZACHOICE, this);
			CCtrlInfo *c_ZAChoice = new CCtrlInfo (&m_ZAChoice, 1, 0, 1, 1);
			ZAGrid.Add (c_ZAChoice);


			CCtrlInfo *c_m_LZA      = new CCtrlInfo (&m_LZA , 1, 3, 1, 1); 
			ZerlKostenFeinGrid.Add (c_m_LZA );
			CCtrlInfo *c_m_ZA      = new CCtrlInfo (&ZAGrid , 2, 3, 1, 1); 
			ZerlKostenFeinGrid.Add (c_m_ZA );
			CCtrlInfo *c_m_ZABez      = new CCtrlInfo (&m_ZABez , 3, 3, 1, 1); 
			ZerlKostenFeinGrid.Add (c_m_ZABez );
			CCtrlInfo *c_m_ZAWert      = new CCtrlInfo (&m_ZAWert , 4, 3, 1, 1); 
			ZerlKostenFeinGrid.Add (c_m_ZAWert );


	//ZerlKostenFeinGrid hinzuf�gen
		CCtrlInfo *c_ZerlKostenFeinGrid     = new CCtrlInfo (&ZerlKostenFeinGrid, 2, 5, 99, 99); 
		ZerlKostenGrid.Add (c_ZerlKostenFeinGrid);

//ZerlKostenGrid hinzuf�gen
	CCtrlInfo *c_ZerlKostenGrid     = new CCtrlInfo (&ZerlKostenGrid, 1, 3, 99, 99); 
	CtrlGrid.Add (c_ZerlKostenGrid);


	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);

	Form.Show ();
	CtrlGrid.Display ();



	return TRUE;
}
void CPage3::OnSize (UINT nType, int cx, int cy) 
{
}
HBRUSH CPage3::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}

	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return CDialog::OnCtlColor (pDC, pWnd,nCtlColor);
}

BOOL CPage3::PreTranslateMessage(MSG* pMsg)
{

	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{
		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
//				Write (1);
//				Update->Back();
				Update->Write();
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{
				if (GetKeyState (VK_SHIFT) >= 0)
				{
					if (OnReturn ())
					{
						return TRUE;
					}
				}
				else
				{
					if (OnKeyup ())
					{
						return TRUE;
					}
				}
				break;
			}


			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F5)
			{
				Update->Back ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F7)
			{
				OnDelete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
			Write (1);
				Update->Back ();
//				Update->Write ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F9)
			{
				if (GetFocus () == &m_GrundKoart)
				{
					OnKostArtChoiceGrund ();
					return TRUE;
				}
				if (GetFocus () == &m_GrobKoart)
				{
					OnKostArtChoiceGrob ();
					return TRUE;
				}
				if (GetFocus () == &m_VM)
				{
					OnKostArtChoiceVM ();
					return TRUE;
				}
				if (GetFocus () == &m_VT)
				{
					OnKostArtChoiceVT ();
					return TRUE;
				}
				if (GetFocus () == &m_ZA)
				{
					OnKostArtChoiceZA ();
					return TRUE;
				}
			return TRUE;
			}
	}
	return FALSE;
}

void CPage3::UpdatePage ()
{
	GrundKostArtLesen();
	GrobKostArtLesen();
	VMKostArtLesen();
	VTKostArtLesen();
	ZAKostArtLesen();
	Form.Show ();
}
void CPage3::GetPage ()
{
	Form.Get ();
}

BOOL CPage3::OnSetActive ()
{
	UpdatePage ();
	return TRUE;
}


BOOL CPage3::Write (BOOL YesNo)
{
	GetPage();
//	Basis->Write();
	return TRUE;
}
BOOL CPage3::Back ()
{
	int di = 0;
//	MaTree.Write (TRUE);
	return TRUE;
}
void CPage3::OnDelete ()
{
	int ret = MessageBox (_T("Kostenart l�schen ? "), NULL, MB_YESNO | MB_ICONQUESTION);
	if  (ret != IDYES) return ;
	CWnd *Control = GetFocus ();
	if (Control == &m_GrundKoart)
	{
		kost_art_zerl.kost_art_zerl.mdn = Schnitt->schnitt.mdn;
		kost_art_zerl.kost_art_zerl.kost_art_zerl = Schnitt->schnitt.koart_grund;
		kost_art_zerl.dbdelete();
		Schnitt->schnitt.koart_grund = 0;
		strcpy_s(Schnitt->GrundKoartBez,"");
		Schnitt->schnitt._grund_kosten = (double) 0;
	}
	if (Control == &m_GrobKoart)
	{
		kost_art_zerl.kost_art_zerl.mdn = Schnitt->schnitt.mdn;
		kost_art_zerl.kost_art_zerl.kost_art_zerl = Schnitt->schnitt.koart_grob;
		kost_art_zerl.dbdelete();
		Schnitt->schnitt.koart_grob = 0;
		strcpy_s(Schnitt->GrobKoartBez,"");
		Schnitt->schnitt._grob_kosten = (double) 0;
	}
	if (Control == &m_VM)
	{
		kost_art_zerl.kost_art_zerl.mdn = Schnitt->schnitt.mdn;
		kost_art_zerl.kost_art_zerl.kost_art_zerl = Schnitt->schnitt.koart_vm;
		kost_art_zerl.dbdelete();
		Schnitt->schnitt.koart_vm = 0;
		strcpy_s(Schnitt->VMKoartBez,"");
		Schnitt->schnitt._vm_kosten = (double) 0;
	}
	if (Control == &m_VT)
	{
		kost_art_zerl.kost_art_zerl.mdn = Schnitt->schnitt.mdn;
		kost_art_zerl.kost_art_zerl.kost_art_zerl = Schnitt->schnitt.koart_vt;
		kost_art_zerl.dbdelete();
		Schnitt->schnitt.koart_vt = 0;
		strcpy_s(Schnitt->VTKoartBez,"");
		Schnitt->schnitt._vt_kosten = (double) 0;
	}
	if (Control == &m_ZA)
	{
		kost_art_zerl.kost_art_zerl.mdn = Schnitt->schnitt.mdn;
		kost_art_zerl.kost_art_zerl.kost_art_zerl = Schnitt->schnitt.koart_za;
		kost_art_zerl.dbdelete();
		Schnitt->schnitt.koart_za = 0;
		strcpy_s(Schnitt->ZAKoartBez,"");
		Schnitt->schnitt._za_kosten = (double) 0;
	}
	Form.Show();
}
BOOL CPage3::Print (HWND m_hwnd)
{
//	MaTree.Print(m_hwnd) ;
	return TRUE;
}
BOOL  CPage3::OnReturn ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_GrundKoartWert)
	{
//		GrundKostArtSpeichern();
	}
	if (Control == &m_GrundKoart)
	{
		Form.Get();
		GrundKostArtLesen();
		Form.Show();
		  m_GrobKoart.EnableWindow (TRUE);
		  m_GrobKoart.SetSel (0, -1, TRUE);
		  m_GrobKoart.SetFocus ();

	}
	if (Control == &m_GrobKoart)
	{
		Form.Get();
		GrobKostArtLesen();
		Form.Show();
	}
	if (Control == &m_VM)
	{
		Form.Get();
		VMKostArtLesen();
		Form.Show();
	}
	if (Control == &m_VT)
	{
		Form.Get();
		VTKostArtLesen();
		Form.Show();
	}
	if (Control == &m_ZA)
	{
		Form.Get();
		ZAKostArtLesen();
		Form.Show();
	}

	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
		Control->SetFocus ();
	}
	return TRUE;
}
BOOL  CPage3::OnKeyup ()
{
	CWnd *Control = GetFocus ();
	Control = GetNextDlgTabItem (Control, TRUE);
	if (Control != NULL)
	{
		Control->SetFocus ();
	}
	return TRUE;
}
void CPage3::OnSchnittchoice ()
{
	Basis->OnSchnittchoice ();
}

void CPage3::OnKostArtChoiceGrund ()
{
	OnKostArtChoice();
	if (kost_art_zerl.dbreadfirst () == 0)
	{
		Schnitt->schnitt.koart_grund = kost_art_zerl.kost_art_zerl.kost_art_zerl;
		strcpy_s(Schnitt->GrundKoartBez,kost_art_zerl.kost_art_zerl.kost_art_zerl_bz);
		Schnitt->schnitt._grund_kosten = kost_art_zerl.kost_art_zerl.vkost_wt;
	}
    Form.Show ();
		  m_GrundKoart.EnableWindow (TRUE);
		  m_GrundKoart.SetSel (0, -1, TRUE);
		  m_GrundKoart.SetFocus ();
}
void CPage3::OnKostArtChoiceGrob ()
{
	OnKostArtChoice();
	if (kost_art_zerl.dbreadfirst () == 0)
	{
		Schnitt->schnitt.koart_grob = kost_art_zerl.kost_art_zerl.kost_art_zerl;
		strcpy_s(Schnitt->GrobKoartBez,kost_art_zerl.kost_art_zerl.kost_art_zerl_bz);
		Schnitt->schnitt._grob_kosten = kost_art_zerl.kost_art_zerl.vkost_wt;
	}
    Form.Show ();
		  m_GrobKoart.EnableWindow (TRUE);
		  m_GrobKoart.SetSel (0, -1, TRUE);
		  m_GrobKoart.SetFocus ();
}
void CPage3::OnKostArtChoiceVM () 
{
	OnKostArtChoice();
	if (kost_art_zerl.dbreadfirst () == 0)
	{
		Schnitt->schnitt.koart_vm = kost_art_zerl.kost_art_zerl.kost_art_zerl;
		strcpy_s(Schnitt->VMKoartBez,kost_art_zerl.kost_art_zerl.kost_art_zerl_bz);
		Schnitt->schnitt._vm_kosten = kost_art_zerl.kost_art_zerl.vkost_wt;
	}
    Form.Show ();
		  m_VM.EnableWindow (TRUE);
		  m_VM.SetSel (0, -1, TRUE);
		  m_VM.SetFocus ();
}
void CPage3::OnKostArtChoiceVT () 
{
	OnKostArtChoice();
	if (kost_art_zerl.dbreadfirst () == 0)
	{
		Schnitt->schnitt.koart_vt = kost_art_zerl.kost_art_zerl.kost_art_zerl;
		strcpy_s(Schnitt->VTKoartBez,kost_art_zerl.kost_art_zerl.kost_art_zerl_bz);
		Schnitt->schnitt._vt_kosten = kost_art_zerl.kost_art_zerl.vkost_wt;
	}
    Form.Show ();
		  m_VT.EnableWindow (TRUE);
		  m_VT.SetSel (0, -1, TRUE);
		  m_VT.SetFocus ();
}
void CPage3::OnKostArtChoiceZA () 
{
	OnKostArtChoice();
	if (kost_art_zerl.dbreadfirst () == 0)
	{
		Schnitt->schnitt.koart_za = kost_art_zerl.kost_art_zerl.kost_art_zerl;
		strcpy_s(Schnitt->ZAKoartBez,kost_art_zerl.kost_art_zerl.kost_art_zerl_bz);
		Schnitt->schnitt._za_kosten = kost_art_zerl.kost_art_zerl.vkost_wt;
	}
    Form.Show ();
		  m_ZA.EnableWindow (TRUE);
		  m_ZA.SetSel (0, -1, TRUE);
		  m_ZA.SetFocus ();
}

void CPage3::OnKostArtChoice ()
{
	ModalChoice = TRUE;
	Form.Get ();
	Form.Show ();
	if (ChoiceKostArt != NULL) // immer neu aufbauen, 
	{
		delete ChoiceKostArt;
		ChoiceKostArt = NULL;
	}

	if (ChoiceKostArt == NULL)
	{
		ChoiceKostArt = new CChoiceKostArt (this);
	    ChoiceKostArt->IsModal = ModalChoice;
		ChoiceKostArt->Where = Where;
		ChoiceKostArt->IdArrDown = IDI_HARROWDOWN;
		ChoiceKostArt->IdArrUp   = IDI_HARROWUP;
		ChoiceKostArt->IdArrNo   = IDI_HARROWNO;
		ChoiceKostArt->HideEnter = TRUE;  // Bearbeiten Knopf raus mit TRUE
//		ChoiceKostArt->HideOK    = HideOK;

		ChoiceKostArt->CreateDlg ();
	}
	CString cA;
	m_GrundKoart.GetWindowText (cA);
	if (!CStrFuncs::IsDecimal (cA))
	{
		ChoiceKostArt->SortRow   = 2;
		m_GrundKoart.SetWindowText ("");
	}

//    ChoiceKostArt->SetDbClass (Schnitt);
//	ChoiceKostArt->SearchText = "TEST";
	ChoiceKostArt->DoModal();
    if (ChoiceKostArt->GetState ())
    {
		  CKostArtList *abl = ChoiceKostArt->GetSelectedText (); 
		  if (abl == NULL) return;
          memcpy_s (&kost_art_zerl.kost_art_zerl,sizeof (kost_art_zerl.kost_art_zerl), &kost_art_zerl_null, sizeof (kost_art_zerl.kost_art_zerl));
          kost_art_zerl.kost_art_zerl.kost_art_zerl = abl->kost_art_zerl;
		  kost_art_zerl.kost_art_zerl.mdn = Schnitt->schnitt.mdn;

    }
	else
	{
//		AChoiceStatM = FALSE;
	}
}

void CPage3::GrundKostArtSpeichern()
{
		memcpy_s (&kost_art_zerl.kost_art_zerl,sizeof (kost_art_zerl.kost_art_zerl), &kost_art_zerl_null, sizeof (kost_art_zerl.kost_art_zerl));
		kost_art_zerl.kost_art_zerl.mdn = Schnitt->schnitt.mdn;
		kost_art_zerl.kost_art_zerl.kost_art_zerl = Schnitt->schnitt.koart_grund;
		_tcscpy (kost_art_zerl.kost_art_zerl.vkost_rechb,"1");//�/kg
		kost_art_zerl.kost_art_zerl.faktor = 1;
		_tcscpy (kost_art_zerl.kost_art_zerl.kost_art_zerl_bz, Schnitt->GrundKoartBez);
		kost_art_zerl.kost_art_zerl.vkost_wt = Schnitt->schnitt._grund_kosten;
		kost_art_zerl.kost_art_zerl.vkost_fix = 0.0;
		CString Date;
		CStrFuncs::SysDate (Date);
		kost_art_zerl.ToDbDate (Date, &kost_art_zerl.kost_art_zerl.dat);

		if (kost_art_zerl.kost_art_zerl.kost_art_zerl > 0) kost_art_zerl.dbupdate();
}
void CPage3::GrobKostArtSpeichern()
{
		memcpy_s (&kost_art_zerl.kost_art_zerl,sizeof (kost_art_zerl.kost_art_zerl), &kost_art_zerl_null, sizeof (kost_art_zerl.kost_art_zerl));
		kost_art_zerl.kost_art_zerl.mdn = Schnitt->schnitt.mdn;
		kost_art_zerl.kost_art_zerl.kost_art_zerl = Schnitt->schnitt.koart_grob;
		_tcscpy (kost_art_zerl.kost_art_zerl.vkost_rechb,"1");//�/kg
		kost_art_zerl.kost_art_zerl.faktor = 1;
		_tcscpy (kost_art_zerl.kost_art_zerl.kost_art_zerl_bz, Schnitt->GrobKoartBez);
		kost_art_zerl.kost_art_zerl.vkost_wt = Schnitt->schnitt._grob_kosten;
		kost_art_zerl.kost_art_zerl.vkost_fix = 0.0;
		CString Date;
		CStrFuncs::SysDate (Date);
		kost_art_zerl.ToDbDate (Date, &kost_art_zerl.kost_art_zerl.dat);
		if (kost_art_zerl.kost_art_zerl.kost_art_zerl > 0) kost_art_zerl.dbupdate();
}
void CPage3::VMKostArtSpeichern()
{
		memcpy_s (&kost_art_zerl.kost_art_zerl,sizeof (kost_art_zerl.kost_art_zerl), &kost_art_zerl_null, sizeof (kost_art_zerl.kost_art_zerl));
		kost_art_zerl.kost_art_zerl.mdn = Schnitt->schnitt.mdn;
		kost_art_zerl.kost_art_zerl.kost_art_zerl = Schnitt->schnitt.koart_vm;
		_tcscpy (kost_art_zerl.kost_art_zerl.vkost_rechb,"1");//�/kg
		kost_art_zerl.kost_art_zerl.faktor = 1;
		_tcscpy (kost_art_zerl.kost_art_zerl.kost_art_zerl_bz, Schnitt->VMKoartBez);
		kost_art_zerl.kost_art_zerl.vkost_wt = Schnitt->schnitt._vm_kosten;
		kost_art_zerl.kost_art_zerl.vkost_fix = 0.0;
		CString Date;
		CStrFuncs::SysDate (Date);
		kost_art_zerl.ToDbDate (Date, &kost_art_zerl.kost_art_zerl.dat);
		if (kost_art_zerl.kost_art_zerl.kost_art_zerl > 0) kost_art_zerl.dbupdate();
}
void CPage3::VTKostArtSpeichern()
{
		memcpy_s (&kost_art_zerl.kost_art_zerl,sizeof (kost_art_zerl.kost_art_zerl), &kost_art_zerl_null, sizeof (kost_art_zerl.kost_art_zerl));
		kost_art_zerl.kost_art_zerl.mdn = Schnitt->schnitt.mdn;
		kost_art_zerl.kost_art_zerl.kost_art_zerl = Schnitt->schnitt.koart_vt;
		_tcscpy (kost_art_zerl.kost_art_zerl.vkost_rechb,"1");//�/kg
		kost_art_zerl.kost_art_zerl.faktor = 1;
		_tcscpy (kost_art_zerl.kost_art_zerl.kost_art_zerl_bz, Schnitt->VMKoartBez);
		kost_art_zerl.kost_art_zerl.vkost_wt = Schnitt->schnitt._vt_kosten;
		kost_art_zerl.kost_art_zerl.vkost_fix = 0.0;
		CString Date;
		CStrFuncs::SysDate (Date);
		kost_art_zerl.ToDbDate (Date, &kost_art_zerl.kost_art_zerl.dat);
		if (kost_art_zerl.kost_art_zerl.kost_art_zerl > 0) kost_art_zerl.dbupdate();
}
void CPage3::ZAKostArtSpeichern()
{
		memcpy_s (&kost_art_zerl.kost_art_zerl,sizeof (kost_art_zerl.kost_art_zerl), &kost_art_zerl_null, sizeof (kost_art_zerl.kost_art_zerl));
		kost_art_zerl.kost_art_zerl.mdn = Schnitt->schnitt.mdn;
		kost_art_zerl.kost_art_zerl.kost_art_zerl = Schnitt->schnitt.koart_za;
		_tcscpy (kost_art_zerl.kost_art_zerl.vkost_rechb,"1");//�/kg
		kost_art_zerl.kost_art_zerl.faktor = 1;
		_tcscpy (kost_art_zerl.kost_art_zerl.kost_art_zerl_bz, Schnitt->ZAKoartBez);
		kost_art_zerl.kost_art_zerl.vkost_wt = Schnitt->schnitt._za_kosten;
		kost_art_zerl.kost_art_zerl.vkost_fix = 0.0;
		CString Date;
		CStrFuncs::SysDate (Date);
		kost_art_zerl.ToDbDate (Date, &kost_art_zerl.kost_art_zerl.dat);
		if (kost_art_zerl.kost_art_zerl.kost_art_zerl > 0) kost_art_zerl.dbupdate();
}

void CPage3::GrundKostArtLesen()
{
          memcpy_s (&kost_art_zerl.kost_art_zerl,sizeof (kost_art_zerl.kost_art_zerl), &kost_art_zerl_null, sizeof (kost_art_zerl.kost_art_zerl));
		  kost_art_zerl.kost_art_zerl.kost_art_zerl = Schnitt->schnitt.koart_grund;
		  kost_art_zerl.kost_art_zerl.mdn = Schnitt->schnitt.mdn;

			if (kost_art_zerl.dbreadfirst () == 0)
			{
				Schnitt->schnitt.koart_grund = kost_art_zerl.kost_art_zerl.kost_art_zerl;
				strcpy_s(Schnitt->GrundKoartBez,kost_art_zerl.kost_art_zerl.kost_art_zerl_bz);
				Schnitt->schnitt._grund_kosten = kost_art_zerl.kost_art_zerl.vkost_wt;
			}
			else
			{
				strcpy_s(Schnitt->GrundKoartBez,"");
				Schnitt->schnitt._grund_kosten = (double) 0;;
			}
}
void CPage3::GrobKostArtLesen()
{
          memcpy_s (&kost_art_zerl.kost_art_zerl,sizeof (kost_art_zerl.kost_art_zerl), &kost_art_zerl_null, sizeof (kost_art_zerl.kost_art_zerl));
		  kost_art_zerl.kost_art_zerl.kost_art_zerl = Schnitt->schnitt.koart_grob;
		  kost_art_zerl.kost_art_zerl.mdn = Schnitt->schnitt.mdn;

			if (kost_art_zerl.dbreadfirst () == 0)
			{
				Schnitt->schnitt.koart_grob = kost_art_zerl.kost_art_zerl.kost_art_zerl;
				strcpy_s(Schnitt->GrobKoartBez,kost_art_zerl.kost_art_zerl.kost_art_zerl_bz);
				Schnitt->schnitt._grob_kosten = kost_art_zerl.kost_art_zerl.vkost_wt;
			}
			else
			{
				strcpy_s(Schnitt->GrobKoartBez,"");
				Schnitt->schnitt._grob_kosten = (double) 0;;
			}
}
void CPage3::VMKostArtLesen()
{
          memcpy_s (&kost_art_zerl.kost_art_zerl,sizeof (kost_art_zerl.kost_art_zerl), &kost_art_zerl_null, sizeof (kost_art_zerl.kost_art_zerl));
		  kost_art_zerl.kost_art_zerl.kost_art_zerl = Schnitt->schnitt.koart_vm;
		  kost_art_zerl.kost_art_zerl.mdn = Schnitt->schnitt.mdn;

			if (kost_art_zerl.dbreadfirst () == 0)
			{
				Schnitt->schnitt.koart_vm = kost_art_zerl.kost_art_zerl.kost_art_zerl;
				strcpy_s(Schnitt->VMKoartBez,kost_art_zerl.kost_art_zerl.kost_art_zerl_bz);
				Schnitt->schnitt._vm_kosten = kost_art_zerl.kost_art_zerl.vkost_wt;
			}
			else
			{
				strcpy_s(Schnitt->VMKoartBez,"");
				Schnitt->schnitt._vm_kosten = (double) 0;;
			}
}
void CPage3::VTKostArtLesen()
{
          memcpy_s (&kost_art_zerl.kost_art_zerl,sizeof (kost_art_zerl.kost_art_zerl), &kost_art_zerl_null, sizeof (kost_art_zerl.kost_art_zerl));
		  kost_art_zerl.kost_art_zerl.kost_art_zerl = Schnitt->schnitt.koart_vt;
		  kost_art_zerl.kost_art_zerl.mdn = Schnitt->schnitt.mdn;

			if (kost_art_zerl.dbreadfirst () == 0)
			{
				Schnitt->schnitt.koart_vt = kost_art_zerl.kost_art_zerl.kost_art_zerl;
				strcpy_s(Schnitt->VTKoartBez,kost_art_zerl.kost_art_zerl.kost_art_zerl_bz);
				Schnitt->schnitt._vt_kosten = kost_art_zerl.kost_art_zerl.vkost_wt;
			}
			else
			{
				strcpy_s(Schnitt->VTKoartBez,"");
				Schnitt->schnitt._vt_kosten = (double) 0;;
			}
}
void CPage3::ZAKostArtLesen()
{
          memcpy_s (&kost_art_zerl.kost_art_zerl,sizeof (kost_art_zerl.kost_art_zerl), &kost_art_zerl_null, sizeof (kost_art_zerl.kost_art_zerl));
		  kost_art_zerl.kost_art_zerl.kost_art_zerl = Schnitt->schnitt.koart_za;
		  kost_art_zerl.kost_art_zerl.mdn = Schnitt->schnitt.mdn;

			if (kost_art_zerl.dbreadfirst () == 0)
			{
				Schnitt->schnitt.koart_za = kost_art_zerl.kost_art_zerl.kost_art_zerl;
				strcpy_s(Schnitt->ZAKoartBez,kost_art_zerl.kost_art_zerl.kost_art_zerl_bz);
				Schnitt->schnitt._za_kosten = kost_art_zerl.kost_art_zerl.vkost_wt;
			}
			else
			{
				strcpy_s(Schnitt->ZAKoartBez,"");
				Schnitt->schnitt._za_kosten = (double) 0;;
			}
}

void CPage3::OnEnKillfocusGrundkoartWert()
{
	Form.Get();
	Form.Show();
	GrundKostArtSpeichern();
}

void CPage3::OnEnKillfocusGrobkoartWert()
{
	Form.Get();
	Form.Show();
	GrobKostArtSpeichern();
}

void CPage3::OnEnKillfocusGrundkoart()
{
//	Form.Get ();
//	GrundKostArtLesen ();
//	Form.Get();
//	Form.Show();
}

void CPage3::OnEnKillfocusGrobkoart()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
}

void CPage3::OnEnKillfocusVmWert()
{
	Form.Get();
	Form.Show();
	VMKostArtSpeichern();
}

void CPage3::OnEnKillfocusVtWert()
{
	Form.Get();
	Form.Show();
	VTKostArtSpeichern();
}

void CPage3::OnEnKillfocusZaWert()
{
	Form.Get();
	Form.Show();
	ZAKostArtSpeichern();
}

void CPage3::OnEnSetfocusSchnittTxt()
{
	if (Zerm->PAGE2_WRITE == TRUE)
	{
		Zerm->ZerlTreeErlauben = Zerm->TYP_MATERIAL; 
		Update->Page2Write ();
	}
}
