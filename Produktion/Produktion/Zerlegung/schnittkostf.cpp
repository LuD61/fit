#include "stdafx.h"
#include "schnittkostf.h"

struct SCHNITTKOST_F schnittkost_f, schnittkost_f_null;

void SCHNITTKOST_F_CLASS::prepare (void)
{
            char *sqltext;

            sqlin ((short *)   &schnittkost_f.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &schnittkost_f.schnitt,  SQLLONG, 0);
            sqlin ((long *)   &schnittkost_f.fein_ptlfnr,  SQLLONG, 0);
    sqlout ((short *) &schnittkost_f.mdn,SQLSHORT,0);
    sqlout ((long *) &schnittkost_f.schnitt,SQLLONG,0);
    sqlout ((long *) &schnittkost_f.fein_ptlfnr,SQLLONG,0);
    sqlout ((TCHAR *) schnittkost_f.fein_ptwert,SQLCHAR,4);
    sqlout ((long *) &schnittkost_f.fein_kost_art,SQLLONG,0);
    sqlout ((double *) &schnittkost_f.fein_wert,SQLDOUBLE,0);
            cursor = sqlcursor (_T("select schnittkost_f.mdn,  ")
_T("schnittkost_f.schnitt,  schnittkost_f.fein_ptlfnr,  ")
_T("schnittkost_f.fein_ptwert,  schnittkost_f.fein_kost_art,  ")
_T("schnittkost_f.fein_wert from schnittkost_f ")

#line 14 "schnittkostf.rpp"
                                _T("where mdn = ? ")
			        _T("and schnitt = ? ")
			        _T("and fein_ptlfnr = ?")
			        );
    sqlin ((short *) &schnittkost_f.mdn,SQLSHORT,0);
    sqlin ((long *) &schnittkost_f.schnitt,SQLLONG,0);
    sqlin ((long *) &schnittkost_f.fein_ptlfnr,SQLLONG,0);
    sqlin ((TCHAR *) schnittkost_f.fein_ptwert,SQLCHAR,4);
    sqlin ((long *) &schnittkost_f.fein_kost_art,SQLLONG,0);
    sqlin ((double *) &schnittkost_f.fein_wert,SQLDOUBLE,0);
            sqltext = _T("update schnittkost_f set ")
_T("schnittkost_f.mdn = ?,  schnittkost_f.schnitt = ?,  ")
_T("schnittkost_f.fein_ptlfnr = ?,  schnittkost_f.fein_ptwert = ?,  ")
_T("schnittkost_f.fein_kost_art = ?,  schnittkost_f.fein_wert = ? ")

#line 19 "schnittkostf.rpp"
                                _T("where mdn = ? ")
			        _T("and schnitt = ? ")
			        _T("and fein_ptlfnr = ?");
            sqlin ((short *)   &schnittkost_f.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &schnittkost_f.schnitt,  SQLLONG, 0);
            sqlin ((long *)   &schnittkost_f.fein_ptlfnr,  SQLLONG, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &schnittkost_f.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &schnittkost_f.schnitt,  SQLLONG, 0);
            sqlin ((long *)   &schnittkost_f.fein_ptlfnr,  SQLLONG, 0);
            test_upd_cursor = sqlcursor (_T("select schnitt from schnittkost_f ")
                                _T("where mdn = ? ")
			        _T("and schnitt = ? ")
			        _T("and fein_ptlfnr = ?")
			        );
            sqlin ((short *)   &schnittkost_f.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &schnittkost_f.schnitt,  SQLLONG, 0);
            sqlin ((long *)   &schnittkost_f.fein_ptlfnr,  SQLLONG, 0);
            del_cursor = sqlcursor (_T("delete from schnittkost_f ")
                                _T("where mdn = ? ")
			        _T("and schnitt = ? ")
			        _T("and fein_ptlfnr = ?")
			        );
    sqlin ((short *) &schnittkost_f.mdn,SQLSHORT,0);
    sqlin ((long *) &schnittkost_f.schnitt,SQLLONG,0);
    sqlin ((long *) &schnittkost_f.fein_ptlfnr,SQLLONG,0);
    sqlin ((TCHAR *) schnittkost_f.fein_ptwert,SQLCHAR,4);
    sqlin ((long *) &schnittkost_f.fein_kost_art,SQLLONG,0);
    sqlin ((double *) &schnittkost_f.fein_wert,SQLDOUBLE,0);
            ins_cursor = sqlcursor (_T("insert into schnittkost_f (")
_T("mdn,  schnitt,  fein_ptlfnr,  fein_ptwert,  fein_kost_art,  fein_wert) ")

#line 44 "schnittkostf.rpp"
                                    _T("values ")
                                    _T("(?,?,?,?,?,?)")); 

#line 46 "schnittkostf.rpp"
}
