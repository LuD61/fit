//190111  Berechnung der Grobteile, wenn fix
//170111 
//020510  Berechnung der Kosten
//010510  Berechnung der Kosten
//160408
#include "StdAfx.h"
#include ".\Matreectrl.h"
#include "zerm.h"
#include "Resource.h"
#include "Token.h"
#include "StrFuncs.h"
#include <vector>


char* clipped (char *str)
/**
Blank am Ende eines Strings abschneiden.
**/
{
          size_t i;

          i = strlen (str);
          if (i == 0) return (str);

          for (i = i - 1; i > 0; i --)
          {
                    if ((unsigned char) str[i] > (unsigned char) ' ')
                    {
                              str [i + 1] = (char) 0;
                              break;
                    }
          }
          return (str);
}

class MaTreeEntry
{
public :
	long mat;
	long aus_mat;
	short aus_a_typ;
	CString a_bz1;
	CString a_bz2;
	double gew;
	double gew_ant;
	double mat_o_b;
	double wert_100kg;
	double fwp;
	short typ;
    HTREEITEM Item;
	MaTreeEntry (HTREEITEM Item,long mat,long aus_mat,short aus_a_typ, LPTSTR a_bz1, LPTSTR a_bz2, 
					double gew, double gew_ant,
					double mat_o_b, double wert_100kg, double fwp, short typ)
	{
		this->Item = Item;
		this->mat        = mat;
		this->aus_mat    = aus_mat;
		this->aus_a_typ    = aus_a_typ;
		this->a_bz1      = a_bz1;
		this->a_bz2      = a_bz2;
		this->gew        = gew;
		this->gew_ant    = gew_ant;
		this->mat_o_b    = mat_o_b;
		this->wert_100kg = wert_100kg;
		this->fwp        = fwp;
		this->typ        = typ;
	}
};


CMaTreeCtrl::CMaTreeCtrl(void)
{
	A_bas = NULL;
	A_mat = NULL;
	Schnitt = NULL;
	Work = NULL;
	Zerm = NULL;

//	AusMatCursor = -1;
}

CMaTreeCtrl::~CMaTreeCtrl(void)
{

}
void CMaTreeCtrl::OnDestroy()
{
	CImageList  *pimagelist;

	pimagelist = GetImageList(TVSIL_NORMAL);
	pimagelist->DeleteImageList();
	delete pimagelist;
}

void CMaTreeCtrl::Init ()
{
    IID_GT = 0;
	IID_Image = 0;
	IID_Mat = 1;
	IID_Folderok = 2;
	IID_ZVMinus = 3;
	IID_ZVLeer = 4;
    IID_ZV1 = 5;
    IID_ZV2 = 6;
    IID_ZV3 = 7;
    IID_ZV4 = 8;
    IID_ZV5 = 9;
    IID_ZV6 = 10;
    IID_ZV7 = 11;
    IID_ZV8 = 12;
    IID_ZV9 = 13;
    IID_ZV10 = 14;
    IID_ZV = 15;

    SetTreeStyle (TVS_HASLINES | TVS_LINESATROOT | TVS_HASBUTTONS );

	CString Text;
//	Text.Format (_T("Ausgangsmaterial %d"), Schnitt->schnitt.schnitt_mat);
	Text.Format (_T("%d %s"), Schnitt->schnitt.schnitt_mat, Schnitt->schnitt._material_bz);

	HTREEITEM RootItem = AddRootItem (Text.GetBuffer (), 0, 5);

/*****
    Zerm->sqlin ((short *)   &Zerm->zerm.mdn,  SQLSHORT, 0);
    Zerm->sqlin ((short *)   &Zerm->zerm.schnitt,  SQLSHORT, 0);
    Zerm->sqlin ((long *)   &Zerm->zerm.zerm_aus_mat,  SQLLONG, 0);
    Zerm->sqlout ((long *)   &Zerm->zerm.zerm_mat,  SQLLONG, 0);
	AusMatCursor = Zerm->sqlcursor (
			_T("select zerm_mat from zerm where mdn = ? ")
			_T("and schnitt = ? and zerm_aus_mat = ? ")
			_T("and zerm_mat <> zerm_aus_mat and a_kz <> \"ZV\" ")
									);
*****/

}

HTREEITEM CMaTreeCtrl::AddRootItem (LPSTR Text, int idx, int Childs)
{
	CBitmap             bitmap;
   TV_INSERTSTRUCT TvItem;

   TvItem.hParent     = TVI_ROOT;
   TvItem.hInsertAfter = TVI_LAST;

   TvItem.item.mask = TVIF_CHILDREN |  TVIF_CHILDREN | TVIF_HANDLE |
                      TVIF_IMAGE | TVIF_PARAM | TVIF_SELECTEDIMAGE |
                      TVIF_STATE | TVIF_TEXT;
   TvItem.item.hItem = NULL;
   TvItem.item.state      = TVIS_STATEIMAGEMASK | TVIS_EXPANDED; 
   TvItem.item.stateMask  = TVIS_STATEIMAGEMASK | TVIS_EXPANDED; 
   TvItem.item.pszText    = Text;
   TvItem.item.cchTextMax = (int) strlen (Text);      
//   TvItem.item.iImage     =  IDB_Folderclose;          
   TvItem.item.iImage     =  0;          
   TvItem.item.iSelectedImage =  0;  
   TvItem.item.cChildren = Childs;       
   TvItem.item.lParam     = (LPARAM) idx;
  TvItem.item.mask = TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_TEXT;


   return InsertItem (&TvItem);
}

HTREEITEM CMaTreeCtrl::AddChildItem (HTREEITEM hParent,  LPSTR Text, int idx, int Childs, 
                                   int iImage, DWORD state)
{
   TV_INSERTSTRUCT TvItem;

   TvItem.hParent     = hParent;
   TvItem.hInsertAfter = TVI_LAST;

   TvItem.item.mask = TVIF_CHILDREN | TVIF_HANDLE |
                      TVIF_IMAGE | TVIF_PARAM | TVIF_SELECTEDIMAGE |
                      TVIF_STATE | TVIF_TEXT | TVS_CHECKBOXES;
   TvItem.item.hItem = NULL;
   TvItem.item.state      = state; 
   TvItem.item.stateMask  = state; 
   TvItem.item.pszText    = Text;
   TvItem.item.cchTextMax = (int) strlen (Text);      
   TvItem.item.iImage     = iImage;
   TvItem.item.iSelectedImage = iImage;  
   TvItem.item.cChildren = Childs;       
   TvItem.item.lParam     = (LPARAM) idx;

   return InsertItem (&TvItem);
}
BOOL CMaTreeCtrl::UpdateItem (HTREEITEM hItem,  LPSTR Text, int idx, int Childs, 
                                   int iImage, DWORD state)
{
   TVITEMA TvItem;


   TvItem.mask = TVIF_CHILDREN | TVIF_HANDLE |
                      TVIF_IMAGE | TVIF_PARAM | TVIF_SELECTEDIMAGE |
                      TVIF_STATE | TVIF_TEXT | TVS_CHECKBOXES | TVE_COLLAPSE;
//	TvItem.mask = state;
   TvItem.hItem = hItem;
   TvItem.state      = state; 
   TvItem.stateMask  = state |TVIS_SELECTED | TVIS_BOLD ; 
   TvItem.pszText    = Text;
   TvItem.cchTextMax = (int) strlen (Text);      
   TvItem.iImage     = iImage;
   TvItem.iSelectedImage = iImage;  
   TvItem.cChildren = Childs;       
   TvItem.lParam     = (LPARAM) idx;
   return SetItem (&TvItem);
//   return SetItemText (TvItem.hItem,TvItem.pszText);
}

void CMaTreeCtrl::SetTreeStyle (DWORD style)
{
    DWORD oldStyle = GetWindowLong (m_hWnd, GWL_STYLE);

    SetWindowLong (m_hWnd, GWL_STYLE, oldStyle | style);
}

BOOL CMaTreeCtrl::Read (short flgSave)
{
//  if (!TreeDataVorhanden ())  Doch immer neu aufbauen, es wurde vorher gesichert !
//  {
	Work->PreiseToZerm(Schnitt->schnitt.mdn,Schnitt->schnitt.schnitt,Schnitt->cfg_PreiseAus_a_kalkpreis);
	DeleteAllItems ();
	DestroyTreeData ();
	SetTreeStyle (TVS_HASLINES | TVS_LINESATROOT | TVS_HASBUTTONS );
//	ModifyStyle( TVS_CHECKBOXES, 0 );
//	SetNewStyle(TVS_CHECKBOXES, TRUE);
// erst mal nicht schaltbar	if (flgSave == 3) SetNewStyle(TVS_CHECKBOXES, TRUE);

	Work->DelZermMatV(Zerm->zerm.mdn,Zerm->zerm.schnitt);
	KorrigiereATypVK ();
	ReadATree ();
	Zerm->ZerlTreeErlauben = TRUE; //301009
	memcpy_s (&Zerm->zerm,sizeof(ZERM), &k, sizeof (K));
	ZerlegeverlustBmpSetzen();
	UpdateTree(0);
	/*
    SelectItem (selectedItem); //TODO Test mit selectedItem hat keine Auswirkung warum?
    Select (selectedItem,TVGN_FIRSTVISIBLE      );//TODO Test mit selectedItem hat keine Auswirkung warum?
	SetNewStyle(TVS_CHECKBOXES, FALSE);
	*/
//  }
    return TRUE;
}
BOOL CMaTreeCtrl::TreeHervorheben (short Aktion, HWND hWndTreeView)
{
	HTREEITEM pItem;
	DestroyZerlTreeGt ();


		// alle Materialien werden hier gespeichert um Fix oder Variabel zu ermitteln
		for (std::vector<CZerlTreeData *>::iterator e = ZerlTreeData.begin (); e != ZerlTreeData.end (); ++e)
		{
			CZerlTreeData *ete = *e;
			if (ete->zerm_mat == 0) continue;
			if (ete->zerm_mat == ete->zerm_aus_mat && ete->a_typ == Zerm->TYP_MATERIAL) continue;
			if (ete->a_typ == Zerm->TYP_MATERIAL)
			{
				CZerlTreeGt *abl = new CZerlTreeGt (ete->Item,
											ete->zerm_mat,
											ete->zerm_mat,
			                                0,
											ete->fix.GetBuffer()
											);
				ZerlTreeGt.push_back (abl);
			}
		}


	CString Text;  
	if (Aktion == CLICKED_SINGLE_EXPAND)
	{
		SetNewStyle(TVS_SINGLEEXPAND, Zerm->SINGLE_EXPAND); 
		for (std::vector<CZerlTreeData *>::iterator e = ZerlTreeData.begin (); e != ZerlTreeData.end (); ++e)
		{
			CZerlTreeData *ete = *e;
			if (ete->zerm_mat == 0) continue;
			if (ete->zerm_mat == ete->zerm_aus_mat && ete->a_typ == Zerm->TYP_MATERIAL) continue;
			pItem = GetParentItem(ete->Item);
			if (pItem == NULL) continue;
			if (pItem == GetRootItem())
			{
				if (Zerm->SINGLE_EXPAND == FALSE)
				{
					TreeView_Expand(hWndTreeView,ete->Item,TVE_EXPAND);
				}
				else
				{
					TreeView_Expand(hWndTreeView,ete->Item,TVE_COLLAPSE);
				}	
			}
		}

	}
	if (Aktion == CLICKED_KOST_DETAILS || Aktion == CLICKED_VK_DETAILS)
	{
		for (std::vector<CZerlTreeData *>::iterator e = ZerlTreeData.begin (); e != ZerlTreeData.end (); ++e)
		{
			CZerlTreeData *ete = *e;
			if (ete->zerm_mat == 0) continue;
			pItem = GetParentItem(ete->Item);
			if (Zerm->ZEIGE_KOSTEN == FALSE)
			{
				if (ete->a_typ != Zerm->TYP_MATERIAL && ete->a_typ != Zerm->TYP_VKARTIKEL) 
				{
					UpdateItem (ete->Item,  GetItemText (ete->Item).GetBuffer(), 
							0, 1, 
							0, TVIS_STATEIMAGEMASK | TVIS_EXPANDED  );
				}	
				/*
				if (ete->a_typ != Zerm->TYP_MATERIAL ) 
				{
					if (ete->a_typ == Zerm->TYP_VKARTIKEL && Zerm->ZEIGE_VK == TRUE)
					{
					}
					else
					{
						TreeView_Expand(hWndTreeView,pItem,TVE_COLLAPSE);
					}
				}
				*/
			}
			if (Zerm->ZEIGE_VK == FALSE)
			{
				if (ete->a_typ == Zerm->TYP_VKARTIKEL) 
				{
					ete->fix = "F";
					UpdateItem (ete->Item,  GetItemText (ete->Item).GetBuffer(), 
							0, 1, 
							0, TVIS_STATEIMAGEMASK | TVIS_EXPANDED  );
					if (Zerm->ZEIGE_KOSTEN == FALSE) TreeView_Expand(hWndTreeView,pItem,TVE_COLLAPSE);
				}	
			}
			if (Zerm->ZEIGE_KOSTEN == TRUE)
			{
				if (ete->a_typ != Zerm->TYP_MATERIAL && ete->a_typ != Zerm->TYP_VKARTIKEL) 
				{
					UpdateItem (ete->Item,  GetItemText (ete->Item).GetBuffer(), 
							0, 1, 
							0, TVIS_STATEIMAGEMASK | TVIS_EXPANDED | TVIS_BOLD );
				}
				if (ete->a_typ != Zerm->TYP_MATERIAL ) 
				{
					TreeView_Expand(hWndTreeView,pItem,TVE_EXPAND);
				}
			}
			if (Zerm->ZEIGE_VK == TRUE)
			{
				if (ete->a_typ == Zerm->TYP_VKARTIKEL) 
				{
					for (std::vector<CZerlTreeGt *>::iterator ge = ZerlTreeGt.begin (); ge != ZerlTreeGt.end (); ++ge)
					{
						CZerlTreeGt *gete = *ge;
						if (gete->Item == pItem)
						{
							if (gete->fix == "V") ete->fix = "V";
							if (gete->fix == "F") ete->fix = "F";
						}
					}
					if (Zerm->ZEIGE_KOSTEN == FALSE)
					{
						UpdateItem (ete->Item,  GetItemText (ete->Item).GetBuffer(), 
								0, 1, 
								0, TVIS_STATEIMAGEMASK | TVIS_EXPANDED | TVIS_BOLD );
					}
					if (Zerm->ZEIGE_KOSTEN == FALSE) TreeView_Expand(hWndTreeView,GetParentItem(ete->Item),TVE_EXPAND);
					if (Zerm->ZEIGE_KOSTEN == FALSE) TreeView_Expand(hWndTreeView,ete->Item,TVE_COLLAPSE);
				}
			}
		}
	}
    return TRUE;
}



BOOL CMaTreeCtrl::UpdateTree (short flgSave)
{
	CString Text;
	CString CTyp, CGew, CGewAnt,CWertKg,CWert100Kg,CWert,CVk,CFwp,CFix,CZeitfaktor,CVollkWert;
//erst mal nicht 	if (flgSave == 2)SetNewStyle(TVS_CHECKBOXES, FALSE);
//erst mal nicht 	if (flgSave == 3) SetNewStyle(TVS_CHECKBOXES, TRUE);
//	SetNewStyle(TVS_CHECKBOXES, FALSE);
	for (std::vector<CZerlTreeData *>::iterator e = ZerlTreeData.begin (); e != ZerlTreeData.end (); ++e)
	{
		CZerlTreeData *ete = *e;
		if (ete->zerm_mat == 0) continue;
		/*** 190509
		if (ete->zerm_mat == ete->zerm_aus_mat && ete->a_typ == Zerm->TYP_MATERIAL)
		{
			ete->fix = "F";
		}
		****/

    	_tcscpy_s (Zerm->zerm.zerm_mat_bz1, ete->zerm_mat_bz1.GetBuffer ());
		_tcscpy_s (Zerm->zerm.kost_bz, ete->zerm_mat_bz1.GetBuffer ());

		if (Zerm->zerm.chk_gew) CGew.Format (_T(" (%.3lf kg)"),ete->zerm_gew);
		if (Zerm->zerm.chk_gew_ant) CGewAnt.Format (_T("   (%.3lf %s)"),ete->zerm_gew_ant,"%");
		if (Zerm->zerm.chk_mat_o_b) CWertKg.Format (_T("   (%.2lf �)"),ete->mat_o_b);
		if (Zerm->zerm.chk_wert) CWert.Format (_T("   (%.2lf �)"),ete->teilkosten_abs);

		switch (Schnitt->schnitt._PreisArt)
		{
		case 1 :
			if (Zerm->zerm.chk_vk) CVk.Format (_T("   (%.2lf �)"), ete->mat_o_b * ete->zerm_gew);
			break;
		case 2 :
			if (Zerm->zerm.chk_vk) CVk.Format (_T("   (%.2lf �)"),ete->vk_pr);
			break;
		default :
			if (Zerm->zerm.chk_vk) CVk.Format (_T("   (%.2lf �)"), ete->mat_o_b * ete->zerm_gew);
			break;
		}

		if (Zerm->zerm.chk_wert_100kg) CWert100Kg.Format (_T("   (%.2lf �)"),ete->dm_100);
		if (Zerm->zerm.chk_kennzeichen) CWert100Kg.Format (_T("   (%s)"),ete->a_kz);
		if (Zerm->zerm.chk_zeitfaktor) CZeitfaktor.Format (_T("   (%ld)"),ete->a_z_f);
//		if (Zerm->zerm.chk_hk_vollk_preis) CVk.Format (_T("   (%.2lf �)"),ete->mat_o_b + ete->hk_vollk );
		if (Zerm->zerm.chk_hk_vollk_preis) CVk.Format (_T("   (%.2lf �)"), ete->hk_vollk );
		if (Zerm->zerm.chk_hk_vollk_wert) CVollkWert.Format (_T("   (%.2lf �)"), ete->hk_vollk * ete->zerm_gew );
		if (Zerm->zerm.chk_hk_vollk) CWertKg.Format (_T("   (%.2lf �)"),ete->hk_vollk - ete->mat_o_b);
		if (ete->fix == "F")
		{
			if (Zerm->zerm.chk_fwp) CFwp.Format (_T("   (%.0lf)"),ete->fwp_org);
		}
		else
		{
			if (Zerm->zerm.chk_fwp) CFwp.Format (_T("   (%.0lf)"),ete->fwp);
		}
		CTyp = ATyp2TypKennzeichen(ete->a_typ);

		Text.Format (_T("%s %d %s%s%s%s%s%s%s%s%s%s"),CTyp.GetBuffer(), ete->zerm_mat, ete->zerm_mat_bz1,
									CGew.GetBuffer(),
									CGewAnt.GetBuffer(),
									CWertKg.GetBuffer(),
									CWert.GetBuffer(),
									CVk.GetBuffer(),
									CVollkWert.GetBuffer(),
									CWert100Kg.GetBuffer(),
									CFwp.GetBuffer(),
									CZeitfaktor.GetBuffer());
		if (ete->zerm_mat == Zerm->Zerlegeverlust)
		{
			CGew.Format (_T(" (%.3lf kg)"),ete->zerm_gew);
			Text.Format (_T("%s %d %s%s"), CTyp.GetBuffer(), ete->zerm_mat, clipped(ete->zerm_mat_bz1.GetBuffer()),
									CGew.GetBuffer());
			CGew = "";
		}
		if (ete->a_typ != Zerm->TYP_MATERIAL)
		{
			CGew.Format (_T(" (%.2lf �)"),ete->teilkosten_abs);
			Text.Format (_T("%s %d %s%s"),CTyp.GetBuffer(),  ete->zerm_mat, clipped(ete->zerm_mat_bz1.GetBuffer()),
									CGew.GetBuffer());
			CGew = "";
		}
		if (ete->a_typ == Zerm->TYP_VKARTIKEL)
		{
			CGew.Format (_T(" (%.2lf �)"),ete->pr_vk_netto);
			Text.Format (_T("%s %d %s%s"),CTyp.GetBuffer(),  ete->zerm_mat, clipped(ete->zerm_mat_bz1.GetBuffer()),
									CGew.GetBuffer());
			CGew = "";
		}
		if (ete->a_typ == Zerm->TYP_ZUTAT)
		{
			CGew.Format (_T(" (%.2lf �)"),ete->teilkosten_abs);   
			Text.Format (_T("%s %d %s%s"),CTyp.GetBuffer(),  ete->zerm_mat, clipped(ete->zerm_mat_bz1.GetBuffer()),
									CGew.GetBuffer());
			CGew = "";
		}
//		UpdateItem (ete->Item,  Text.GetBuffer(), 
//	                          0, 1, 
//	 					      0, NULL);

		/******************* hier nicht 
		if (ete->a_typ == Zerm->zerm.a_typ
			&& ete->a_typ == Zerm->TYP_ZUTAT)
		{
			UpdateItem (ete->Item,  GetItemText (ete->Item).GetBuffer(), 
                         0, 1, 
					      0, TVIS_STATEIMAGEMASK | TVIS_EXPANDED | TVIS_BOLD );
		}
		else
		{
			UpdateItem (ete->Item,  GetItemText (ete->Item).GetBuffer(), 
                         0, 1, 
					      0, TVIS_STATEIMAGEMASK | TVIS_EXPANDED  );
		}
		***********************/


		SetItemText (ete->Item,Text.GetBuffer());

		CFix.Format (_T("%s"),ete->fix);
		SetCheck(ete->Item, CFix == "V"); 
		int nID;
		if (ete->zerm_mat == Zerm->Zerlegeverlust)
		{
			if (ete->zerm_gew_ant_parent < -0.01)
			{
				IID_Image = IID_ZVMinus;
			}
			if (ete->zerm_gew_ant_parent > -0.01)
			{
				IID_Image = IID_ZVLeer;
			}
			double dgew_ant = (double) -0.2;
			for (nID = IID_ZV1; nID <= IID_ZV; nID++)  
			{
				dgew_ant += 0.25;
				if (ete->zerm_gew_ant_parent > dgew_ant)
				{
					IID_Image = nID;
				}
			}
		}
		else
		{
			if (CFix == "V")
			{
				IID_Image = IID_Folderok;
			}
			else
			{
				if (ete->zerm_teil == "1")
				{
					IID_Image = IID_GT;
				}
				else
				{
					IID_Image = IID_Mat;
				}
			}
		}
		SetItemImage(ete->Item,IID_Image,IID_Image);

	}
    return TRUE;
}



BOOL CMaTreeCtrl::ReadATree ()
{
	CString A_Kz;
	std::vector<MaTreeEntry *> MatTab;
	CString Text;
	CString CTyp, CGew, CGewAnt,CWertKg,CWert100Kg,CFwp,CFix,CA_kz,CZeitfaktor,CWert;
	int iImage = 0;

	Zerm->zerm.mdn = Schnitt->schnitt.mdn;
	Zerm->zerm.schnitt = Schnitt->schnitt.schnitt;
	Zerm->zerm.zerm_aus_mat = Schnitt->schnitt.schnitt_mat;
	Zerm->zerm.zerm_mat = Schnitt->schnitt.schnitt_mat;
	Zerm->zerm.a_typ = Zerm->TYP_MATERIAL;
	Zerm->zerm.zerm_aus_a_typ = Zerm->TYP_MATERIAL;
	if (Zerm->dbreadfirst() != 0) return FALSE;
	A_Kz = Work->HoleA_Kz(Zerm->zerm.zerm_mat,Zerm->zerm.a_typ,A_Kz.GetBuffer());  //071009 hier jetzt immer a_kz aus den Stammdaten holen
	_tcscpy (Zerm->zerm.a_kz, A_Kz.GetBuffer());
	A_mat->a_mat.mat = Zerm->zerm.zerm_mat;
	_tcscpy_s (Zerm->zerm.kost_bz, HoleBezeichnung ().GetBuffer());

	if (Zerm->zerm.a_typ == Zerm->TYP_MATERIAL) //010510 nur dann neu berechnen, sonst l�ufts bei Zutaten usw. falsch
	{
	    Zerm->zerm.zerm_gew_ant = 100 * Zerm->zerm.zerm_gew / Schnitt->schnitt.ek_gew; //besser neu berechnen 
	}

	Zerm->zerm.mat_o_b = Zerm->zerm.fwp * Schnitt->schnitt._wert_fwp;
	Zerm->zerm.hk_vollk = HoleHKKosten(0.0,Zerm->zerm.a_kz,Zerm->zerm.list_ebene,Zerm->zerm.a_z_f,Zerm->zerm.zerm_teil);
	Zerm->zerm.hk_vollk += Zerm->zerm.mat_o_b;
	if (Zerm->zerm.zerm_mat == Zerm->zerm.zerm_aus_mat && Zerm->zerm.a_typ == Zerm->TYP_MATERIAL) //ausgangsmaterial //301009
	{
		Zerm->zerm.hk_vollk = Zerm->zerm.mat_o_b;
		Zerm->zerm.hk_teilk = Zerm->zerm.mat_o_b;
	}


	A_Kz = Zerm->zerm.a_kz;
	if (A_Kz == "ZV") Zerm->zerm.hk_vollk = (double) 0;

	Zerm->zerm.dm_100 = Zerm->zerm.fwp * Schnitt->schnitt._wert_fwp * Zerm->zerm.zerm_gew_ant;
	Zerm->zerm.a_typ = Zerm->TYP_MATERIAL;
	Zerm->zerm.teilkosten_abs = Zerm->zerm.zerm_gew * Zerm->zerm.mat_o_b;
   	_tcscpy_s (Zerm->zerm.fix, "F");
	memcpy_s (&k,sizeof (K), &Zerm->zerm,sizeof (ZERM)); 

//	Text.Format (_T("Ausgangsmaterial %d"), Schnitt->schnitt.schnitt_mat);
	Text.Format (_T("%d %s"), Schnitt->schnitt.schnitt_mat, Schnitt->schnitt._material_bz);
	ArtItem = AddRootItem (Text.GetBuffer (), 0, 5);
	UpdateItem (ArtItem,  GetItemText (ArtItem).GetBuffer(), 
	                          0, 1, 
	 					      0, TVIS_STATEIMAGEMASK | TVIS_EXPANDED | TVIS_BOLD );
		CFix.Format (_T("%s"),Zerm->zerm.fix);
		SetCheck(ArtItem, CFix == "V"); 

//	HTREEITEM selectedItem = ArtItem;
	HTREEITEM ChildItem = ArtItem;
		MaTreeEntry *ete = new MaTreeEntry (ArtItem,
											Schnitt->schnitt.schnitt_mat,
											Schnitt->schnitt.schnitt_mat,
											Zerm->TYP_MATERIAL,
//											"Ausgangsmaterial",
											Schnitt->schnitt._material_bz,
											"",
			                                Zerm->zerm.zerm_gew ,
			                                Zerm->zerm.zerm_gew_ant,
			                                Zerm->zerm.mat_o_b,
			                                Zerm->zerm.dm_100,
											Zerm->zerm.fwp,
											Zerm->zerm.a_typ
											);
		MatTab.push_back (ete);
		CZerlTreeData *abl = new CZerlTreeData (ArtItem,
											Schnitt->schnitt.mdn,
											Schnitt->schnitt.schnitt,
											Schnitt->schnitt.schnitt_mat,
											Schnitt->schnitt.schnitt_mat,
											Zerm->TYP_MATERIAL,
			                                Zerm->zerm.zerm_gew ,
			                                Zerm->zerm.zerm_gew_ant,
			                                Zerm->zerm.mat_o_b,
			                                Zerm->zerm.dm_100,
											Zerm->zerm.fwp,
											Zerm->zerm.fix,

											Zerm->zerm.a_kz,
											Zerm->zerm.zerm_teil,
											Zerm->zerm.hk_vollk, //301009
											Zerm->zerm.hk_teilk, //301009
											//301009 0.0,//Zerm->zerm.hk_vollk,
											//301009 0.0,//Zerm->zerm.hk_teilk,
											Zerm->zerm.delstatus,
											Zerm->zerm.vollkosten_abs,
											Zerm->zerm.vollkosten_proz,
											Zerm->zerm.teilkosten_abs,
											Zerm->zerm.teilkosten_proz,
											Zerm->zerm.a_z_f ,
											Zerm->zerm.vk_pr ,
											Zerm->zerm.kosten_zutat,
											Zerm->zerm.kosten_vpk ,
											Zerm->zerm.a_typ ,
										    Zerm->zerm.kosten,
											Zerm->zerm.pr_zutat,
											Zerm->zerm.pr_vpk,
										    Zerm->zerm.pr_kosten,
											Zerm->zerm.pr_vk_netto,
											Zerm->zerm.kosten_fix,
											Zerm->zerm.pr_vk_eh,

//											"Ausgangsmaterial",
											Schnitt->schnitt._material_bz,
											"",
											(double) 0,  //zerm_gew_ant_parent
											Zerm->zerm.kosten_gh,
											Zerm->zerm.kosten_eh,
											Zerm->zerm.fwp_org,     //230408
											0.0,//Zerm->zerm.sk_vollk,
											0.0,//Zerm->zerm.sk_teilk,
											0.0,//Zerm->zerm.fil_ek_vollk,
											0.0,//Zerm->zerm.fil_ek_teilk,
											0.0,//Zerm->zerm.fil_vk_vollk,
											0.0,//Zerm->zerm.fil_vk_teilk,
											0.0,//Zerm->zerm.gh1_vollk,
											0.0,//Zerm->zerm.gh1_teilk,
											0.0,//Zerm->zerm.gh2_vollk,
											0.0//Zerm->zerm.gh2_teilk,
											);
			ZerlTreeData.push_back (abl);
		    UpdateZermMatV();
						
	Zerm->zerm.mdn = Schnitt->schnitt.mdn;
	Zerm->zerm.schnitt = Schnitt->schnitt.schnitt;
	Zerm->zerm.zerm_aus_mat = Schnitt->schnitt.schnitt_mat;
	Zerm->zerm.zerm_aus_a_typ = Zerm->TYP_MATERIAL;
	Zerm->sqlopen (Zerm->AusMatCursor);
	while (Zerm->sqlfetch (Zerm->AusMatCursor) == 0)
	{
		A_Kz = Work->HoleA_Kz(Zerm->zerm.zerm_mat,Zerm->zerm.a_typ,A_Kz.GetBuffer());  //071009 hier jetzt immer a_kz aus den Stammdaten holen
		_tcscpy (Zerm->zerm.a_kz, A_Kz.GetBuffer());
		if (Zerm->zerm.a_typ == Zerm->TYP_MATERIAL) //010510 nur dann neu berechnen, sonst l�ufts bei Zutaten usw. falsch
		{
		     Zerm->zerm.zerm_gew_ant = 100 * Zerm->zerm.zerm_gew / Schnitt->schnitt.ek_gew; //besser neu berechnen 
		}
		_tcscpy_s (Zerm->zerm.kost_bz, HoleBezeichnung ().GetBuffer());
// nich mehr n�tig		if (Zerm->dbreadfirst () != 0) continue;
		if (Zerm->zerm.a_typ == Zerm->TYP_MATERIAL)
		{
			Zerm->zerm.mat_o_b = Zerm->zerm.fwp * Schnitt->schnitt._wert_fwp;
			Zerm->zerm.hk_vollk = HoleHKKosten(0.0,Zerm->zerm.a_kz,Zerm->zerm.list_ebene,Zerm->zerm.a_z_f,Zerm->zerm.zerm_teil);
			Zerm->zerm.hk_vollk += Zerm->zerm.mat_o_b;

			A_Kz = Zerm->zerm.a_kz;
			if (A_Kz == "ZV") Zerm->zerm.hk_vollk = (double) 0;
			Zerm->zerm.dm_100 = Zerm->zerm.fwp * Schnitt->schnitt._wert_fwp * Zerm->zerm.zerm_gew_ant;
			Zerm->zerm.teilkosten_abs = Zerm->zerm.zerm_gew * Zerm->zerm.mat_o_b;
		}
		if (Zerm->zerm.chk_gew) CGew.Format (_T(" (%.2lf kg)"),Zerm->zerm.zerm_gew);
		if (Zerm->zerm.chk_gew_ant) CGewAnt.Format (_T("   (%.2lf %s)"),Zerm->zerm.zerm_gew_ant,"%");
		if (Zerm->zerm.chk_mat_o_b) CWertKg.Format (_T("   (%.2lf �)"),Zerm->zerm.mat_o_b);
		if (Zerm->zerm.chk_wert_100kg) CWert100Kg.Format (_T("   (%.2lf �)"),Zerm->zerm.dm_100);
		if (Zerm->zerm.chk_wert) CWert.Format (_T("   (%.2lf �)"),Zerm->zerm.teilkosten_abs); //190111
		if (Zerm->zerm.chk_kennzeichen) CWert100Kg.Format (_T("   (%s)"),Zerm->zerm.a_kz);
		if (Zerm->zerm.chk_zeitfaktor) CZeitfaktor.Format (_T("   (%ld)"),Zerm->zerm.a_z_f);
		if (Zerm->zerm.chk_fwp) CFwp.Format (_T("   (%.0lf)"),Zerm->zerm.fwp);
		CTyp = ATyp2TypKennzeichen(Zerm->zerm.a_typ);
		Text.Format (_T("%s %d %s%s%s%s%s%s%s%s"),CTyp.GetBuffer(), Zerm->zerm.zerm_mat, Zerm->zerm.zerm_mat_bz1,
									CGew.GetBuffer(),
									CGewAnt.GetBuffer(),
									CWertKg.GetBuffer(),
									CWert.GetBuffer(),
									CWert100Kg.GetBuffer(),
									CFwp.GetBuffer(),
									CZeitfaktor.GetBuffer());
		CA_kz.Format (_T("%s"),Zerm->zerm.a_kz);
		iImage = 0;
		ChildItem = AddChildItem (ArtItem,  Text.GetBuffer (), 
	                          0, 1, 
	 					      iImage, TVIS_STATEIMAGEMASK | TVIS_EXPANDED );

		CFix.Format (_T("%s"),Zerm->zerm.fix);
		SetCheck(ChildItem, CFix == "V"); 
		MaTreeEntry *ete = new MaTreeEntry (ChildItem,
											Zerm->zerm.zerm_mat,
											Zerm->zerm.zerm_aus_mat,
											Zerm->zerm.zerm_aus_a_typ,
											Zerm->zerm.kost_bz,
											Zerm->zerm.zerm_mat_bz2,
			                                Zerm->zerm.zerm_gew ,
			                                Zerm->zerm.zerm_gew_ant,
			                                Zerm->zerm.mat_o_b,
			                                Zerm->zerm.dm_100,
											Zerm->zerm.fwp,
											Zerm->zerm.a_typ
											);
		MatTab.push_back (ete);
		HTREEITEM selectedItem = ChildItem;
		CZerlTreeData *abl = new CZerlTreeData (ChildItem,
											Schnitt->schnitt.mdn,
											Schnitt->schnitt.schnitt,
											Zerm->zerm.zerm_mat,
											Zerm->zerm.zerm_aus_mat,
											Zerm->zerm.zerm_aus_a_typ,
			                                Zerm->zerm.zerm_gew ,
			                                Zerm->zerm.zerm_gew_ant,
			                                Zerm->zerm.mat_o_b,
			                                Zerm->zerm.dm_100,
											Zerm->zerm.fwp,
											Zerm->zerm.fix,

											Zerm->zerm.a_kz,
											Zerm->zerm.zerm_teil,
											Zerm->zerm.hk_vollk,
											Zerm->zerm.hk_teilk,
											Zerm->zerm.delstatus,
											Zerm->zerm.vollkosten_abs,
											Zerm->zerm.vollkosten_proz,
											Zerm->zerm.teilkosten_abs,
											Zerm->zerm.teilkosten_proz,
											Zerm->zerm.a_z_f ,
											Zerm->zerm.vk_pr ,
											Zerm->zerm.kosten_zutat,
											Zerm->zerm.kosten_vpk ,
											Zerm->zerm.a_typ ,
										    Zerm->zerm.kosten,
											Zerm->zerm.pr_zutat,
											Zerm->zerm.pr_vpk,
										    Zerm->zerm.pr_kosten,
											Zerm->zerm.pr_vk_netto,
											Zerm->zerm.kosten_fix,
											Zerm->zerm.pr_vk_eh,

											Zerm->zerm.kost_bz,
											Zerm->zerm.zerm_mat_bz2,
											(double) 0,  //zerm_gew_ant_parent
											Zerm->zerm.kosten_gh,
											Zerm->zerm.kosten_eh,
											Zerm->zerm.fwp_org,
											Zerm->zerm.sk_vollk,
											Zerm->zerm.sk_teilk,
											Zerm->zerm.fil_ek_vollk,
											Zerm->zerm.fil_ek_teilk,
											Zerm->zerm.fil_vk_vollk,
											Zerm->zerm.fil_vk_teilk,
											Zerm->zerm.gh1_vollk,
											Zerm->zerm.gh1_teilk,
											Zerm->zerm.gh2_vollk,
											Zerm->zerm.gh2_teilk
											);
			ZerlTreeData.push_back (abl);
		    UpdateZermMatV();
	}
	for (std::vector<MaTreeEntry *>::iterator e = MatTab.begin (); e != MatTab.end (); ++e)
	{
		MaTreeEntry *ete = *e;
		if (ete->mat != Schnitt->schnitt.schnitt_mat)
		{
			Zerm->zerm.zerm_aus_mat = ete->mat;
			Zerm->zerm.a_typ = ete->typ;
			Zerm->zerm.zerm_aus_a_typ = ete->aus_a_typ;
			if (ReadZermTree (ete->Item)== TRUE) 
			{
				if (ete->typ == Zerm->TYP_MATERIAL)
				{
					UpdateItem (ete->Item,  GetItemText (ete->Item).GetBuffer(), 
	                          0, 1, 
	 					      0, TVIS_STATEIMAGEMASK | TVIS_EXPANDED | TVIS_BOLD );
				}
			SetCheck(ete->Item, TRUE); //TODO greift noch nicht
			}
		}
		delete ete;
	}
	MatTab.clear ();
	return TRUE; 
}




BOOL CMaTreeCtrl::ReadZermTree (HTREEITEM ArtItem)
{
	BOOL flgReturn = FALSE;
	std::vector<MaTreeEntry *> MatTab;
	CString Text, CBez;
	CString CTyp,CGew, CGewAnt,CWertKg,CWert100Kg,CFwp,CFix,CA_kz,CZeitfaktor,CWert;
	int iImage;
	Zerm->sqlopen (Zerm->AusMatCursor);
	while (Zerm->sqlfetch (Zerm->AusMatCursor) == 0)
	{
		CA_kz = Work->HoleA_Kz(Zerm->zerm.zerm_mat,Zerm->zerm.a_typ,CA_kz.GetBuffer());  //071009 hier jetzt immer a_kz aus den Stammdaten holen
		_tcscpy (Zerm->zerm.a_kz, CA_kz.GetBuffer());
		if (Zerm->zerm.a_typ == Zerm->TYP_MATERIAL) //010510 nur dann neu berechnen, sonst l�ufts bei Zutaten usw. falsch
		{
			Zerm->zerm.zerm_gew_ant = 100 * Zerm->zerm.zerm_gew / Schnitt->schnitt.ek_gew; //besser neu berechnen 
		}
		_tcscpy_s (Zerm->zerm.kost_bz, HoleBezeichnung ().GetBuffer());

// nich mehr n�tig		if (Zerm->dbreadfirst () != 0) continue;
		if (Zerm->zerm.a_typ == Zerm->TYP_MATERIAL)
		{
			Zerm->zerm.mat_o_b = Zerm->zerm.fwp * Schnitt->schnitt._wert_fwp;
			Zerm->zerm.hk_vollk = HoleHKKosten(0.0,Zerm->zerm.a_kz,Zerm->zerm.list_ebene,Zerm->zerm.a_z_f,Zerm->zerm.zerm_teil);
			Zerm->zerm.hk_vollk += Zerm->zerm.mat_o_b;

			Zerm->zerm.dm_100 = Zerm->zerm.fwp * Schnitt->schnitt._wert_fwp * Zerm->zerm.zerm_gew_ant;
			Zerm->zerm.teilkosten_abs = Zerm->zerm.zerm_gew * Zerm->zerm.mat_o_b;
		}
		if (Zerm->zerm.chk_gew) CGew.Format (_T(" (%.2lf kg)"),Zerm->zerm.zerm_gew);
		if (Zerm->zerm.chk_gew_ant) CGewAnt.Format (_T("   (%.2lf %s)"),Zerm->zerm.zerm_gew_ant,"%");
		if (Zerm->zerm.chk_mat_o_b) CWertKg.Format (_T("   (%.2lf �)"),Zerm->zerm.mat_o_b);
		if (Zerm->zerm.chk_wert_100kg) CWert100Kg.Format (_T("   (%.2lf �)"),Zerm->zerm.dm_100);
		if (Zerm->zerm.chk_wert) CWert.Format (_T("   (%.2lf �)"),Zerm->zerm.teilkosten_abs); //190111
		if (Zerm->zerm.chk_kennzeichen) CWert100Kg.Format (_T("   (%s)"),Zerm->zerm.a_kz);
		if (Zerm->zerm.chk_zeitfaktor) CZeitfaktor.Format (_T("   (%ld)"),Zerm->zerm.a_z_f);
		if (Zerm->zerm.chk_fwp) CFwp.Format (_T("   (%.0lf)"),Zerm->zerm.fwp);
		CTyp = ATyp2TypKennzeichen(Zerm->zerm.a_typ);

		Text.Format (_T("%s %d %s%s%s%s%s%s%s%s"),CTyp.GetBuffer(), Zerm->zerm.zerm_mat, Zerm->zerm.zerm_mat_bz1,
									CGew.GetBuffer(),
									CGewAnt.GetBuffer(),
									CWertKg.GetBuffer(),
									CWert.GetBuffer(),
									CWert100Kg.GetBuffer(),
									CFwp.GetBuffer(),
									CZeitfaktor.GetBuffer());

		CA_kz.Format (_T("%s"),Zerm->zerm.a_kz);
		iImage = 0;
		HTREEITEM ChildItem = AddChildItem (ArtItem,  Text.GetBuffer (), 
	                          0, 1, 
	 					      iImage, TVIS_STATEIMAGEMASK | TVIS_EXPANDED);
		CFix.Format (_T("%s"),Zerm->zerm.fix);
		SetCheck(ChildItem, CFix == "V"); //TODO greift noch nicht
//			SetCheck(ChildItem, TRUE); //TODO greift noch nicht
		if (Zerm->zerm.a_typ == Zerm->TYP_MATERIAL)
		{
			flgReturn = TRUE;
		}


		MaTreeEntry *ete = new MaTreeEntry (ChildItem,
											Zerm->zerm.zerm_mat,
											Zerm->zerm.zerm_aus_mat,
											Zerm->zerm.zerm_aus_a_typ,
											Zerm->zerm.kost_bz,
											Zerm->zerm.zerm_mat_bz2,
			                                Zerm->zerm.zerm_gew ,
			                                Zerm->zerm.zerm_gew_ant,
			                                Zerm->zerm.mat_o_b,
			                                Zerm->zerm.dm_100,
											Zerm->zerm.fwp,
											Zerm->zerm.a_typ
											);
		MatTab.push_back (ete);

		CZerlTreeData *abl = new CZerlTreeData (ChildItem,
											Schnitt->schnitt.mdn,
											Schnitt->schnitt.schnitt,
											Zerm->zerm.zerm_mat,
											Zerm->zerm.zerm_aus_mat,
											Zerm->zerm.zerm_aus_a_typ,
			                                Zerm->zerm.zerm_gew ,
			                                Zerm->zerm.zerm_gew_ant,
			                                Zerm->zerm.mat_o_b,
			                                Zerm->zerm.dm_100,
											Zerm->zerm.fwp,
											Zerm->zerm.fix,

											Zerm->zerm.a_kz,
											Zerm->zerm.zerm_teil,
											Zerm->zerm.hk_vollk,
											Zerm->zerm.hk_teilk,
											Zerm->zerm.delstatus,
											Zerm->zerm.vollkosten_abs,
											Zerm->zerm.vollkosten_proz,
											Zerm->zerm.teilkosten_abs,
											Zerm->zerm.teilkosten_proz,
											Zerm->zerm.a_z_f ,
											Zerm->zerm.vk_pr ,
											Zerm->zerm.kosten_zutat,
											Zerm->zerm.kosten_vpk ,
											Zerm->zerm.a_typ ,
										    Zerm->zerm.kosten,
											Zerm->zerm.pr_zutat,
											Zerm->zerm.pr_vpk,
										    Zerm->zerm.pr_kosten,
											Zerm->zerm.pr_vk_netto,
											Zerm->zerm.kosten_fix,
											Zerm->zerm.pr_vk_eh,


											Zerm->zerm.kost_bz,
											Zerm->zerm.zerm_mat_bz2,
											(double) 0,  //zerm_gew_ant_parent
											Zerm->zerm.kosten_gh,
											Zerm->zerm.kosten_eh,
											Zerm->zerm.fwp_org, 
											Zerm->zerm.sk_vollk,
											Zerm->zerm.sk_teilk,
											Zerm->zerm.fil_ek_vollk,
											Zerm->zerm.fil_ek_teilk,
											Zerm->zerm.fil_vk_vollk,
											Zerm->zerm.fil_vk_teilk,
											Zerm->zerm.gh1_vollk,
											Zerm->zerm.gh1_teilk,
											Zerm->zerm.gh2_vollk,
											Zerm->zerm.gh2_teilk
											);
			ZerlTreeData.push_back (abl);
		    UpdateZermMatV();
     // SetItemData(ChildItem,  ZerlTreeData[0]);

	}
	for (std::vector<MaTreeEntry *>::iterator e = MatTab.begin (); e != MatTab.end (); ++e)
	{
		MaTreeEntry *ete = *e;
		if (ete->mat != Schnitt->schnitt.schnitt_mat)
		{
			Zerm->zerm.zerm_aus_mat = ete->mat;
			Zerm->zerm.a_typ = ete->typ;
			Zerm->zerm.zerm_aus_a_typ = ete->typ;
			if (ReadZermTree (ete->Item)== TRUE) 
			{
				if (ete->typ == Zerm->TYP_MATERIAL)
				{
					UpdateItem (ete->Item,  GetItemText (ete->Item).GetBuffer(), 
	                          0, 1, 
	 					      0, TVIS_STATEIMAGEMASK | TVIS_EXPANDED | TVIS_BOLD );
				}
			SetCheck(ete->Item, TRUE); //TODO greift noch nicht
			}
		}
		delete ete;
	}
	MatTab.clear ();
	return flgReturn;
}

void CMaTreeCtrl::SetZerlTree (HTREEITEM hItem, int idx)
{
//Geht nicht , nicht benutzt
	if (hItem != NULL)
   {
      SetItemData(hItem, (DWORD_PTR) &ZerlTreeData [idx]);
   }

}
CZerlTreeData *CMaTreeCtrl::GetZerlTree (HTREEITEM hItem)
{
//Geht nicht , nicht benutzt
	CZerlTreeData *treedata = (CZerlTreeData *) GetItemData(hItem);
	return treedata;
}


void CMaTreeCtrl::DestroyTreeData() 
{
	for (std::vector<CZerlTreeData *>::iterator ptreedata = ZerlTreeData.begin (); ptreedata != ZerlTreeData.end (); ++ptreedata)
	{
		CZerlTreeData *treedata = *ptreedata;
		delete treedata;
	}
    ZerlTreeData.clear ();
}
BOOL CMaTreeCtrl::TreeDataVorhanden() 
{
	for (std::vector<CZerlTreeData *>::iterator ptreedata = ZerlTreeData.begin (); ptreedata != ZerlTreeData.end (); ++ptreedata)
	{
		CZerlTreeData *treedata = *ptreedata;
		if (treedata->mdn == Schnitt->schnitt.mdn && treedata->schnitt == Schnitt->schnitt.schnitt)
		{
			return TRUE;
		} else return FALSE;
	}
	return FALSE;
}
void CMaTreeCtrl::DestroyZerlTreeGt() 
{
	for (std::vector<CZerlTreeGt *>::iterator ptreedata = ZerlTreeGt.begin (); ptreedata != ZerlTreeGt.end (); ++ptreedata)
	{
		CZerlTreeGt *treedata = *ptreedata;
		delete treedata;
	}
    ZerlTreeGt.clear ();
}
void CMaTreeCtrl::DestroyZerlTreeGt2() 
{
	for (std::vector<CZerlTreeGt2 *>::iterator ptreedata = ZerlTreeGt2.begin (); ptreedata != ZerlTreeGt2.end (); ++ptreedata)
	{
		CZerlTreeGt2 *treedata = *ptreedata;
		delete treedata;
	}
    ZerlTreeGt2.clear ();
}
void CMaTreeCtrl::DestroyZerlTreeMatKost() 
{
	for (std::vector<CZerlTreeMatKost *>::iterator ptreedata = ZerlTreeMatKost.begin (); ptreedata != ZerlTreeMatKost.end (); ++ptreedata)
	{
		CZerlTreeMatKost *treedata = *ptreedata;
		delete treedata;
	}
    ZerlTreeMatKost.clear ();
}
void CMaTreeCtrl::DestroyZerlTreeVkArtikel() 
{
	for (std::vector<CZerlTreeVkArtikel *>::iterator ptreedata = ZerlTreeVkArtikel.begin (); ptreedata != ZerlTreeVkArtikel.end (); ++ptreedata)
	{
		CZerlTreeVkArtikel *treedata = *ptreedata;
		delete treedata;
	}
    ZerlTreeVkArtikel.clear ();
}
BOOL CMaTreeCtrl::DestroyTreeData(HTREEITEM hItem)
{
	for (std::vector<CZerlTreeData *>::iterator e = ZerlTreeData.begin (); e != ZerlTreeData.end (); ++e)
	{
		CZerlTreeData *ete = *e;
		if (hItem == ete->Item)
		{
//			delete ete;
			ete->zerm_mat = 0;
			return TRUE;
		}
	}
    ZerlTreeData.clear ();
	return FALSE;
}
BOOL CMaTreeCtrl::GetZerlTreeData (HTREEITEM ht)
{
//	Geht nicht , nicht benutzt
	CZerlTreeData *treedata = (CZerlTreeData *) GetItemData(ht);
	return FALSE;
}

void CMaTreeCtrl::SetNewStyle(long lStyleMask, BOOL bSetBits)
{
	long        lStyleOld;

	lStyleOld = GetWindowLong(m_hWnd, GWL_STYLE);
	lStyleOld &= ~lStyleMask;
	if (bSetBits)
		lStyleOld |= lStyleMask;

	SetWindowLong(m_hWnd, GWL_STYLE, lStyleOld);
	SetWindowPos(NULL, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER);
}
BEGIN_MESSAGE_MAP(CMaTreeCtrl, CTreeCtrl)
	ON_NOTIFY(NM_CLICK, IDC_ZTREE, &CMaTreeCtrl::OnNMClickZtree)
	ON_NOTIFY(TVN_KEYDOWN, IDC_ZTREE, &CMaTreeCtrl::OnTvnKeydownZtree)
	ON_NOTIFY(NM_DBLCLK, IDC_ZTREE, &CMaTreeCtrl::OnNMDblclkZtree)
END_MESSAGE_MAP()

void CMaTreeCtrl::OnNMClickZtree(NMHDR *pNMHDR, LRESULT *pResult)
{
	// Hier kommt er nie hin nicht benutzt
	int di = 0;
	*pResult = 0;
}

void CMaTreeCtrl::OnTvnKeydownZtree(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTVKEYDOWN pTVKeyDown = reinterpret_cast<LPNMTVKEYDOWN>(pNMHDR);
	*pResult = 0;
}


HTREEITEM CMaTreeCtrl::ZermToZerlTreeData ()
{
	CString CFix;
	// bei : FWP,mat_o_b, pr100kg, fix (hier �ber alle gleichen mat)
	// bei : gew, gew_ant (nur �ber 1 Item und ZV!)(mat,aus_mat)
	HTREEITEM hItem = NULL;
	HTREEITEM PItem = NULL;
	DestroyZerlTreeGt (); //190509
    double fwp = 0.0;
	double mat_gew = (double) 0;
	double mat_gew_ant = (double) 0;
	double mat_hk_vollk = (double) 0;
	for (std::vector<CZerlTreeData *>::iterator e = ZerlTreeData.begin (); e != ZerlTreeData.end (); ++e)
	{
		CZerlTreeData *ete = *e;
		if (ete->zerm_mat == 0) continue;
		if (Zerm->zerm.zerm_mat == ete->zerm_mat && Zerm->zerm.a_typ == ete->a_typ)
		{
			mat_gew += Zerm->zerm.zerm_gew;
//			mat_gew_ant += Zerm->zerm.zerm_gew; 010510
			mat_gew_ant += Zerm->zerm.zerm_gew_ant; //010510

			ete->fix = Zerm->zerm.fix;
			CFix.Format (_T("%s"),Zerm->zerm.fix);
			if (CFix == "V")
			{
				ete->fwp = Zerm->zerm.fwp;
			}
			else
			{
				ete->fwp = Zerm->zerm.fwp_org; //2304008
 				//170111 if (Zerm->fwp_neu > 0.0) ete->fwp_org = Zerm->zerm.fwp;
 				if (Zerm->fwp_neu == TRUE) ete->fwp_org = Zerm->zerm.fwp; //190111
 				// 190111 ete->fwp_org = Zerm->zerm.fwp; //170111
			}
			ete->a_typ = Zerm->zerm.a_typ;
			if (Zerm->zerm.a_typ == Zerm->TYP_MATERIAL)
			{
				ete->mat_o_b = Zerm->zerm.mat_o_b;
				ete->hk_teilk = Zerm->zerm.hk_teilk;
				ete->hk_vollk = Zerm->zerm.hk_vollk;
				ete->sk_teilk = Zerm->zerm.sk_teilk;
				ete->sk_vollk = Zerm->zerm.sk_vollk;
				ete->fil_ek_teilk = Zerm->zerm.fil_ek_teilk;
				ete->fil_ek_vollk = Zerm->zerm.fil_ek_vollk;
				ete->fil_vk_teilk = Zerm->zerm.fil_vk_teilk;
				ete->fil_vk_vollk = Zerm->zerm.fil_vk_vollk;
				ete->gh1_teilk = Zerm->zerm.gh1_teilk;
				ete->gh1_vollk = Zerm->zerm.gh1_vollk;
				ete->gh2_teilk = Zerm->zerm.gh2_teilk;
				ete->gh2_vollk = Zerm->zerm.gh2_vollk;
			}

			if (Zerm->zerm.zerm_aus_mat == ete->zerm_aus_mat 
				&& Zerm->zerm.zerm_aus_a_typ == ete->zerm_aus_a_typ
				&& (ete->a_typ  == Zerm->TYP_MATERIAL || ete->a_typ  == Zerm->TYP_VKARTIKEL 
				|| ete->a_typ == Zerm->TYP_VERPACKUNG || ete->a_typ == Zerm->TYP_AUFSCHLAG  || ete->a_typ == Zerm->TYP_AUFSCHLAG_EH))
			{
				ete->zerm_gew = Zerm->zerm.zerm_gew;
//				ete->zerm_gew_ant = Zerm->zerm.zerm_gew_ant;
				ete->zerm_gew_ant = 100 * ete->zerm_gew / Schnitt->schnitt.ek_gew; //besser neu berechnen 
				ete->zerm_teil = Zerm->zerm.zerm_teil;
				ete->mat_o_b = Zerm->zerm.mat_o_b;
				ete->hk_teilk = Zerm->zerm.hk_teilk;
				ete->hk_vollk = Zerm->zerm.hk_vollk;
				mat_hk_vollk = ete->hk_vollk;
		        ete->vollkosten_abs = Zerm->zerm.vollkosten_abs;
			    ete->vollkosten_proz = Zerm->zerm.vollkosten_proz;
				ete->teilkosten_abs = Zerm->zerm.teilkosten_abs;
				ete->teilkosten_proz = Zerm->zerm.teilkosten_proz;
				ete->a_z_f = Zerm->zerm.a_z_f;
				ete->vk_pr = Zerm->zerm.vk_pr;
				ete->kosten_zutat = Zerm->zerm.kosten_zutat;
				ete->kosten_vpk = Zerm->zerm.kosten_vpk;
			    
				ete->kosten = Zerm->zerm.kosten;
				ete->pr_zutat = Zerm->zerm.pr_zutat;
				ete->pr_vpk = Zerm->zerm.pr_vpk;
				ete->pr_kosten = Zerm->zerm.pr_kosten;
				ete->pr_vk_netto = Zerm->zerm.pr_vk_netto;
				ete->kosten_fix = Zerm->zerm.kosten_fix;
				ete->pr_vk_eh = Zerm->zerm.pr_vk_eh;
				ete->kosten_gh = Zerm->zerm.kosten_gh;
				ete->kosten_eh = Zerm->zerm.kosten_eh;


				ZerlVerlBer(ete->Item);
		    	_tcscpy_s (Zerm->zerm.fix, ete->fix.GetBuffer ()); //wurde von ZerlverBer �berschrieben!!!
				hItem = ete->Item;
			}

			if (Zerm->zerm.a_typ == Zerm->TYP_MATERIAL)
			{
				ete->dm_100 = ete->mat_o_b * ete->zerm_gew_ant;
				ete->teilkosten_abs = ete->mat_o_b * ete->zerm_gew; //190111
			}
			if (ete->zerm_mat == Zerm->Zerlegeverlust)
			{
				ete->dm_100 = 0.0;
				ete->fwp = 0.0;
				ete->fwp_org = 0.0;
				ete->teilkosten_abs = 0.0;
				ete->mat_o_b = 0.0;
				ete->hk_vollk = 0.0;
				ete->hk_teilk = 0.0;
				ete->sk_vollk = 0.0;
				ete->sk_teilk = 0.0;
			}

			if (Zerm->zerm.a_typ == Zerm->TYP_ZUTAT)
			{
 				ete->dm_100 = (double) 0;
				PItem = GetParentItem(ete->Item);
				if (PItem != NULL) 
				{
//					ete->zerm_gew = ete->zerm_gew_ant * ItemToGew(PItem) / 1000;   //ZUTUN 010510 zerm_gew_ant ist 0
					ete->zerm_gew = mat_gew_ant * ItemToGew(PItem) / 1000;  //010510
					ete->zerm_gew_ant = mat_gew_ant ; //010510
				} 
				else
				{
					ete->zerm_gew = (double) 0;
				}
				mat_gew = ete->zerm_gew;
				ete->mat_o_b = Zerm->zerm.mat_o_b; // 250509 
				ete->vollkosten_abs = Zerm->zerm.vollkosten_abs;  //250509 
				ete->teilkosten_abs = Zerm->zerm.teilkosten_abs;  //250509 

			}
			if (Zerm->zerm.a_typ == Zerm->TYP_KOSTEN )
			{
				ete->zerm_mat_bz1 = Zerm->zerm.kost_bz;
				ete->mat_o_b = Zerm->zerm.mat_o_b; // 250509 Kosten/kg
				ete->vollkosten_abs = Zerm->zerm.vollkosten_abs;  //250509 Fix-Kosten
				ete->dm_100 = (double) 0;
				PItem = GetParentItem(ete->Item);
				if (PItem != NULL) 
				{
					ete->zerm_gew = ItemToGew(PItem) ;
				} 
				else
				{
					ete->zerm_gew = (double) 0;
				}
				mat_gew = ete->zerm_gew;
			}
			if (Zerm->zerm.a_typ == Zerm->TYP_AUFSCHLAG)
			{
				ete->zerm_mat_bz1 = Zerm->zerm.kost_bz;
				PItem = GetParentItem(ete->Item);
				if (PItem != NULL) 
				{
					ItemTo_k_ausZerlTreeData(PItem) ;
					ete->teilkosten_abs = k.hk_vollk * ete->mat_o_b / 100;

				} 
				else
				{
					ete->teilkosten_abs = (double) 0;
				}
			}
			if (Zerm->zerm.a_typ == Zerm->TYP_AUFSCHLAG_EH)
			{
				ete->zerm_mat_bz1 = Zerm->zerm.kost_bz;
				PItem = GetParentItem(ete->Item);
				if (PItem != NULL) 
				{
					ItemTo_k_ausZerlTreeData(PItem) ;
					ete->teilkosten_abs = k.hk_vollk * ete->mat_o_b / 100;

				} 
				else
				{
					ete->teilkosten_abs = (double) 0;
				}
			}
			if (Zerm->zerm.a_typ == Zerm->TYP_VKARTIKEL)
			{
				PItem = GetParentItem(ete->Item);
				if (PItem != NULL) 
				{
					ItemTo_k_ausZerlTreeData(PItem) ;
					ete->zerm_gew = k.zerm_gew;
					ete->zerm_gew_ant = k.zerm_gew_ant;
					ete->dm_100 = k.dm_100;
					ete->mat_o_b = k.mat_o_b;
					ete->fwp = k.fwp; //080510 auch fwp aus dem material �bernehmen, sonst geht int. Kalkulation nicht
					ete->fwp_org = k.fwp_org; //080510 auch fwp aus dem material �bernehmen, sonst geht int. Kalkulation nicht
				} 
				else
				{
					ete->zerm_gew = (double) 0;
				}
				mat_gew = ete->zerm_gew;
			}
			if (ete->fix == "V") fwp = ete->fwp;
			if (ete->fix == "F") fwp = ete->fwp_org;
			if (ete->mat_o_b > -9999 && ete->mat_o_b < 99999 && ete->dm_100 > -9999 && ete->dm_100 < 99999)
			{
		    Work->UpdateZermMatV(Schnitt->schnitt.mdn,
								   Schnitt->schnitt.schnitt,
								   ete->zerm_aus_mat,
								   ete->zerm_mat,
								   fwp,
								   ete->zerm_gew,
								   ete->zerm_gew_ant,
								   ete->mat_o_b,
								   ete->hk_vollk,
								   ete->hk_teilk,
								   ete->sk_vollk,
								   ete->sk_teilk,
								   ete->fil_ek_vollk,
								   ete->fil_ek_teilk,
								   ete->fil_vk_vollk,
								   ete->fil_vk_teilk,
								   ete->gh1_vollk,
								   ete->gh1_teilk,
								   ete->gh2_vollk,
								   ete->gh2_teilk,
								   ete->dm_100,
								   ete->kosten_zutat,
								   ete->kosten_vpk,
								   ete->fix.GetBuffer(),
								   ete->a_typ,
								   ete->teilkosten_abs,
								   ete->vk_pr,
								   ete->zerm_teil.GetBuffer(),
								   ete->zerm_mat, // in kost_mat
								   ete->zerm_mat_bz1.GetBuffer(), // in kost_mat_bz
								   ete->pr_vk_eh
								   );
			}
			if (ete->zerm_teil == "1") GrobteilBer(ete->Item);
		}
		else
		{
			if (ete->zerm_aus_mat == Zerm->zerm.zerm_mat && ete->a_typ == Zerm->TYP_MATERIAL)  //190509 alles unterhalb des Grobteils fix/Variabel setzen
			{
				if (ete->zerm_teil == "1" && ete->a_typ == Zerm->TYP_MATERIAL)  // hier f�llen, um die n�chste Ebene abzuhandeln
				{
					CZerlTreeGt *abl = new CZerlTreeGt (ete->Item,
											ete->zerm_mat,
											ete->zerm_aus_mat,
			                                0,
											Zerm->zerm.fix
											);
					ZerlTreeGt.push_back (abl);
				}

				ete->fix = Zerm->zerm.fix;
				if (ete->a_kz == "ZV") ete->fix = "F";
				CFix.Format (_T("%s"),Zerm->zerm.fix);
				if (ete->fix == "V") fwp = ete->fwp;
				if (ete->fix == "F") fwp = ete->fwp_org;
				if (ete->mat_o_b > -9999 && ete->mat_o_b < 99999 && ete->dm_100 > -9999 && ete->dm_100 < 99999)
				{
					Work->UpdateZermMatV(Schnitt->schnitt.mdn,
								   Schnitt->schnitt.schnitt,
								   ete->zerm_aus_mat,
								   ete->zerm_mat,
								   fwp,
								   ete->zerm_gew,
								   ete->zerm_gew_ant,
								   ete->mat_o_b,
								   ete->hk_vollk,
								   ete->hk_teilk,
								   ete->sk_vollk,
								   ete->sk_teilk,
								   ete->fil_ek_vollk,
								   ete->fil_ek_teilk,
								   ete->fil_vk_vollk,
								   ete->fil_vk_teilk,
								   ete->gh1_vollk,
								   ete->gh1_teilk,
								   ete->gh2_vollk,
								   ete->gh2_teilk,
								   ete->dm_100,
								   ete->kosten_zutat,
								   ete->kosten_vpk,
								   ete->fix.GetBuffer(),
								   ete->a_typ,
								   ete->teilkosten_abs,
								   ete->vk_pr,
								   ete->zerm_teil.GetBuffer(),
								   ete->zerm_mat, // in kost_mat
								   ete->zerm_mat_bz1.GetBuffer(), // in kost_mat_bz
								   ete->pr_vk_eh
								   );
				}
			}
		}
	}

	//190509 auch die Untergeordneten auf fix/variable setzten
   BOOL flg_continue = TRUE;
   while (flg_continue == TRUE)
   {
	flg_continue = FALSE;
	DestroyZerlTreeGt2 (); 
	for (std::vector<CZerlTreeGt *>::iterator e = ZerlTreeGt.begin (); e != ZerlTreeGt.end (); ++e)
	{
		CZerlTreeGt *eteGt = *e;
		for (std::vector<CZerlTreeData *>::iterator e = ZerlTreeData.begin (); e != ZerlTreeData.end (); ++e)
		{
			CZerlTreeData *ete = *e;
			if (ete->zerm_mat == 0) continue;
			if (ete->zerm_aus_mat == eteGt->zerm_mat) 
			{
				if (ete->zerm_teil == "1" && ete->a_typ == Zerm->TYP_MATERIAL) //hier gehts noch eine Ebene tiefer  
				{
					flg_continue = TRUE;
					CZerlTreeGt2 *abl = new CZerlTreeGt2 (ete->Item,
											ete->zerm_mat,
											ete->zerm_aus_mat,
			                                0,
											Zerm->zerm.fix
											);
					ZerlTreeGt2.push_back (abl);
				}
				ete->fix = eteGt->fix;
				if (ete->a_kz == "ZV") ete->fix = "F";
				CFix.Format (_T("%s"),Zerm->zerm.fix);
				if (ete->fix == "V") fwp = ete->fwp;
				if (ete->fix == "F") fwp = ete->fwp_org;
				if (ete->mat_o_b > -9999 && ete->mat_o_b < 99999 && ete->dm_100 > -9999 && ete->dm_100 < 99999)
				{
					Work->UpdateZermMatV(Schnitt->schnitt.mdn,
								   Schnitt->schnitt.schnitt,
								   ete->zerm_aus_mat,
								   ete->zerm_mat,
								   fwp,
								   ete->zerm_gew,
								   ete->zerm_gew_ant,
								   ete->mat_o_b,
								   ete->hk_vollk,
								   ete->hk_teilk,
								   ete->sk_vollk,
								   ete->sk_teilk,
								   ete->fil_ek_vollk,
								   ete->fil_ek_teilk,
								   ete->fil_vk_vollk,
								   ete->fil_vk_teilk,
								   ete->gh1_vollk,
								   ete->gh1_teilk,
								   ete->gh2_vollk,
								   ete->gh2_teilk,
								   ete->dm_100,
								   ete->kosten_zutat,
								   ete->kosten_vpk,
								   ete->fix.GetBuffer(),
								   ete->a_typ,
								   ete->teilkosten_abs,
								   ete->vk_pr,
								   ete->zerm_teil.GetBuffer(),
								   ete->zerm_mat, // in kost_mat
								   ete->zerm_mat_bz1.GetBuffer(), // in kost_mat_bz
								   ete->pr_vk_eh
								   );
				}
			}
		}
	}
    if (flg_continue == TRUE)
	{
		// 1:1 wieder inb cZerlTreeGT schaufeln und oben wieder in die schleife rein, bis alle Ebenen durch sind
		DestroyZerlTreeGt (); 
		for (std::vector<CZerlTreeGt2 *>::iterator e = ZerlTreeGt2.begin (); e != ZerlTreeGt2.end (); ++e)
		{
			CZerlTreeGt2 *eteGt = *e;
				CZerlTreeGt *abl = new CZerlTreeGt (eteGt->Item,
											eteGt->zerm_mat,
											eteGt->zerm_aus_mat,
			                                0,
											eteGt->fix.GetBuffer ()
											);
				ZerlTreeGt.push_back (abl);
		}
	}

   }//while flg_continue




	ZutKostBer(hItem,mat_gew,mat_hk_vollk);
	ItemToZerm(hItem);
	Zerm->fwp_neu = FALSE;
    return hItem;
}
BOOL CMaTreeCtrl::ZerlTreeDataToZerm (short flgMitFwp)
{
	HTREEITEM AktItem = NULL;
	for (std::vector<CZerlTreeData *>::iterator e = ZerlTreeData.begin (); e != ZerlTreeData.end (); ++e)
	{
		CZerlTreeData *ete = *e;
		if (ete->zerm_mat == 0) continue;
		if (Zerm->zerm.zerm_mat == ete->zerm_mat && 
			Zerm->zerm.zerm_aus_mat == ete->zerm_aus_mat &&
			Zerm->zerm.a_typ == ete->a_typ)
		{

	    	_tcscpy_s (Zerm->zerm.a_kz, ete->a_kz.GetBuffer ());
	    	_tcscpy_s (Zerm->zerm.fix, ete->fix.GetBuffer ());
			if (ete->a_typ == Zerm->TYP_VKARTIKEL )
			{
				int dii = 1;
			}

			if (flgMitFwp == 1)
			{
				if (ete->fix == "F")
				{
					Zerm->zerm.fwp = ete->fwp_org;
				}
				else
				{
					Zerm->zerm.fwp = ete->fwp;
				}
				Zerm->zerm.fwp_org = ete->fwp_org;
			}
			Zerm->zerm.zerm_gew = ete->zerm_gew;
			Zerm->zerm.zerm_gew_ant = ete->zerm_gew_ant;
    		_tcscpy_s (Zerm->zerm.zerm_teil, ete->zerm_teil.GetBuffer ());
			Zerm->zerm.mat_o_b = ete->mat_o_b;
			Zerm->zerm.hk_vollk = ete->hk_vollk;
			Zerm->zerm.hk_teilk = ete->hk_teilk;
			Zerm->zerm.delstatus = ete->delstatus;
			Zerm->zerm.vollkosten_abs = ete->vollkosten_abs;
			Zerm->zerm.vollkosten_proz = ete->vollkosten_proz;
			Zerm->zerm.teilkosten_abs = ete->teilkosten_abs;
			Zerm->zerm.teilkosten_proz = ete->teilkosten_proz;
			Zerm->zerm.dm_100 = ete->dm_100;
			Zerm->zerm.a_z_f = ete->a_z_f;
			Zerm->zerm.vk_pr = ete->vk_pr;
			Zerm->zerm.a_typ = ete->a_typ;

	        Zerm->zerm.vollkosten_abs = ete->vollkosten_abs;
		    Zerm->zerm.vollkosten_proz = ete->vollkosten_proz;
		    Zerm->zerm.teilkosten_abs = ete->teilkosten_abs;
		    Zerm->zerm.teilkosten_proz = ete->teilkosten_proz;
		    Zerm->zerm.a_z_f = ete->a_z_f;
		    Zerm->zerm.vk_pr = ete->vk_pr;
		    Zerm->zerm.kosten_zutat = ete->kosten_zutat;
		    Zerm->zerm.kosten_vpk = ete->kosten_vpk;

			Zerm->zerm.kosten = ete->kosten;
			Zerm->zerm.pr_zutat = ete->pr_zutat;
			Zerm->zerm.pr_vpk = ete->pr_vpk;
			Zerm->zerm.pr_kosten = ete->pr_kosten;
			Zerm->zerm.pr_vk_netto = ete->pr_vk_netto;
			Zerm->zerm.kosten_fix = ete->kosten_fix;
			Zerm->zerm.pr_vk_eh = ete->pr_vk_eh;
			Zerm->zerm.kosten_gh = ete->kosten_gh;
			Zerm->zerm.kosten_eh = ete->kosten_eh;

			if (Zerm->zerm.a_typ == Zerm->TYP_KOSTEN || Zerm->zerm.a_typ == Zerm->TYP_AUFSCHLAG
													 || Zerm->zerm.a_typ == Zerm->TYP_AUFSCHLAG_EH) 
			{
				_tcscpy_s (Zerm->zerm.kost_bz, ete->zerm_mat_bz1.GetBuffer ());
			}
//			if (ItemHasChildren(ete->Item))  funkt. nicht !!!
    		_tcscpy_s (Zerm->zerm.zerm_teil, BestimmeZerm_teil(ete->Item));
			if (Zerm->zerm.a_typ != Zerm->TYP_MATERIAL && Zerm->zerm.a_typ != Zerm->TYP_VKARTIKEL )
			{
	    			_tcscpy_s (Zerm->zerm.fix, "F");
			}
			if (ete->zerm_mat == Zerm->Zerlegeverlust)
			{
				Zerm->zerm.dm_100 = 0.0;
				Zerm->zerm.fwp = 0.0;
				Zerm->zerm.teilkosten_abs = 0.0;
				Zerm->zerm.mat_o_b = 0.0;
			}

			AktItem = ete->Item;
		}
	}
    return TRUE;
}
BOOL CMaTreeCtrl::ZerlVerlBer ()
{
	HTREEITEM hItem = NULL;
	hItem = GetSelectedItem ();
	if (hItem == NULL) return FALSE;
	ZerlVerlBer (hItem);
	return TRUE;
}


BOOL CMaTreeCtrl::ZerlVerlBer (HTREEITEM hItem)
{
//===== Zerlegeverlust der akt. Ebene errechnen ==================
	HTREEITEM hChildItem;
	HTREEITEM PItem = GetParentItem(hItem);
	if (PItem == NULL) 
	{
		//================ Wenn akt. Item ein zerlegtes GT ist (zerm_teil = 1), dann ZV 
		//================ der Child-Ebene neu errechnen =====================================
		hChildItem = ZerlegtAus(hItem);
		if (hChildItem != NULL)
		{
			ZerlVerlBer(hChildItem);
		}
		ItemToZerm(hItem);
		return TRUE;
	}
	HTREEITEM ZVItem = NULL;
	double CGew = (double) 0;
	double PGew = ItemToGew(PItem);
	hChildItem = GetChildItem(PItem);
	while (hChildItem != NULL)
	{
		if (IsZerlVerl(hChildItem))
		{
			ZVItem = hChildItem; 
			hChildItem = GetNextItem(hChildItem, TVGN_NEXT);
			continue;
		}
		//CGew += ItemToGew(hChildItem);
		ItemTo_k_ausZerlTreeData(hChildItem);
		if (k.a_typ == Zerm->TYP_MATERIAL)
		{
			CGew += k.zerm_gew;
		}
		hChildItem = GetNextItem(hChildItem, TVGN_NEXT);
	}
	if (ZVItem == NULL)
	{
		ItemToZerm(hItem);
		return FALSE;
	}
	ItemToZerm(ZVItem);
	for (std::vector<CZerlTreeData *>::iterator e = ZerlTreeData.begin (); e != ZerlTreeData.end (); ++e)
	{
		CZerlTreeData *ete = *e;
		if (ete->zerm_mat == 0) continue;
		if (Zerm->zerm.zerm_mat == ete->zerm_mat && 
			Zerm->zerm.zerm_aus_mat == ete->zerm_aus_mat &&
			Zerm->zerm.a_typ == ete->a_typ && 
			ete->a_typ == Zerm->TYP_MATERIAL)    //010510
		{
			ete->zerm_gew = PGew - CGew;
			if (!CStrFuncs::IsDoubleEqual (Schnitt->schnitt.ek_gew, (double) 0))
			{
				ete->zerm_gew_ant = 100 * ete->zerm_gew / Schnitt->schnitt.ek_gew;
				ete->zerm_gew_ant_parent = 100 * ete->zerm_gew / PGew;
/*** nicht mehr n�tig
				if (ete->zerm_gew < -0.01)
				{
					UpdateItem (ete->Item,  GetItemText (ete->Item).GetBuffer(), 
	                          0, 1, 
	 					      0, TVIS_STATEIMAGEMASK | TVIS_EXPANDED | TVIS_SELECTED );
				}
				else
				{
					UpdateItem (ete->Item,  GetItemText (ete->Item).GetBuffer(), 
	                          0, 1, 
	 					      0, TVIS_STATEIMAGEMASK | TVIS_EXPANDED  );
				}
*************/
			}
		}
	}
	ItemToZerm(hItem);


//================ Wenn akt. Item ein zerlegtes GT ist (zerm_teil = 1), dann ZV 
//================ der Child-Ebene neu errechnen =====================================
	hChildItem = ZerlegtAus(hItem);
	if (hChildItem != NULL)
	{
		ZerlVerlBer(hChildItem);
	}
	ItemToZerm(hItem);

	return TRUE;
}
BOOL CMaTreeCtrl::GrobteilBer ()
{
	HTREEITEM hItem = NULL;
	hItem = GetSelectedItem ();
	if (hItem == NULL) return FALSE;
	GrobteilBer (hItem);
	return TRUE;
}
BOOL CMaTreeCtrl::GrobteilBer (HTREEITEM hItem)
{
//===== �bergeordnetes Grobteil neu berechnen ==================
	long mat = Zerm->zerm.zerm_mat;
	long aus_mat = Zerm->zerm.zerm_aus_mat;
	short aus_a_typ = Zerm->zerm.zerm_aus_a_typ;
	short a_typ = Zerm->zerm.a_typ;
	CString Cfix = Zerm->zerm.fix;
	HTREEITEM PItem = GetParentItem(hItem);
	if (PItem == NULL) return FALSE;
	ItemToZerm(PItem);
	if (Zerm->zerm.zerm_mat == Schnitt->schnitt.schnitt_mat) 
	{
		Zerm->zerm.zerm_mat = mat;
		Zerm->zerm.zerm_aus_mat = aus_mat;
		Zerm->zerm.zerm_aus_a_typ = aus_a_typ;
		Zerm->zerm.a_typ = a_typ;
		_tcscpy_s (Zerm->zerm.fix, Cfix.GetBuffer());
		return TRUE ; //Root nicht neu ber!!
	}
	double SumDm100 = (double) 0;
	HTREEITEM hChildItem = GetChildItem(PItem);
	while (hChildItem != NULL)
	{
//		SumDm100 += ItemTo_k_ausZerlTreeData(hChildItem);
		ItemTo_k_ausZerlTreeData(hChildItem);
		SumDm100 += k.fwp * Schnitt->schnitt._wert_fwp * k.zerm_gew_ant;

		hChildItem = GetNextItem(hChildItem, TVGN_NEXT);
	}
	ItemToZerm(PItem);
	for (std::vector<CZerlTreeData *>::iterator e = ZerlTreeData.begin (); e != ZerlTreeData.end (); ++e)
	{
		CZerlTreeData *ete = *e;
		if (ete->zerm_mat == 0) continue;
		if (Zerm->zerm.zerm_mat == ete->zerm_mat && 
			Zerm->zerm.zerm_aus_mat == ete->zerm_aus_mat &&
			Zerm->zerm.a_typ == ete->a_typ)
		{
			if (!CStrFuncs::IsDoubleEqual (Schnitt->schnitt._wert_fwp, (double) 0))
			{
				if (!CStrFuncs::IsDoubleEqual (ete->zerm_gew_ant, (double) 0))
				{
					ete->dm_100 = SumDm100;
					ete->mat_o_b  = ete->dm_100 / ete->zerm_gew_ant;
					ete->fwp =  ete->mat_o_b / Schnitt->schnitt._wert_fwp; 
					ete->fwp_org =  ete->fwp; //190111 
					ete->teilkosten_abs = ete->mat_o_b * ete->zerm_gew; //190111
					ete->hk_vollk = HoleHKKosten(ete->mat_o_b,"GT",1,ete->a_z_f,ete->zerm_teil.GetBuffer());										if (ete->zerm_mat == Zerm->Zerlegeverlust)
					if (ete->zerm_mat == Zerm->Zerlegeverlust)
					{
						ete->dm_100 = 0.0;
						ete->fwp = 0.0;
						ete->fwp_org = 0.0;
						ete->teilkosten_abs = 0.0;
						ete->mat_o_b = 0.0;
						ete->hk_vollk = 0.0;
					}

					if (ete->mat_o_b > -9999 && ete->mat_o_b < 99999 && ete->dm_100 > -9999 && ete->dm_100 < 99999)
					{
				    Work->UpdateZermMatV(Schnitt->schnitt.mdn,
								   Schnitt->schnitt.schnitt,
								   ete->zerm_aus_mat,
								   ete->zerm_mat,
								   ete->fwp,
								   ete->zerm_gew,
								   ete->zerm_gew_ant,
								   ete->mat_o_b,
								   ete->hk_vollk,
								   ete->hk_teilk,
								   ete->sk_vollk,
								   ete->sk_teilk,
								   ete->fil_ek_vollk,
								   ete->fil_ek_teilk,
								   ete->fil_vk_vollk,
								   ete->fil_vk_teilk,
								   ete->gh1_vollk,
								   ete->gh1_teilk,
								   ete->gh2_vollk,
								   ete->gh2_teilk,
								   ete->dm_100,
								   ete->kosten_zutat,
								   ete->kosten_vpk,
								   ete->fix.GetBuffer (),
								   ete->a_typ,
								   ete->teilkosten_abs,
								   ete->vk_pr,
								   ete->zerm_teil.GetBuffer(),
								   ete->zerm_mat, // in kost_mat
								   ete->zerm_mat_bz1.GetBuffer(), // in kost_mat_bz
								   ete->pr_vk_eh
								   );
					}
				}
			}
		}
	}
	Zerm->zerm.zerm_mat = mat;
	Zerm->zerm.zerm_aus_mat = aus_mat;
	Zerm->zerm.zerm_aus_a_typ = aus_a_typ;
	Zerm->zerm.a_typ = a_typ;
	_tcscpy_s (Zerm->zerm.fix, Cfix.GetBuffer());
	return TRUE;
}

BOOL CMaTreeCtrl::MatKostenBer (HTREEITEM hItem)
{
//===== �bergeordnetes Material aus dessen Kosten neu berechnen ==================
	long mat = Zerm->zerm.zerm_mat;
	long aus_mat = Zerm->zerm.zerm_aus_mat;
	short aus_a_typ = Zerm->zerm.zerm_aus_a_typ;
	short a_typ = Zerm->zerm.a_typ;
	CString Cfix = Zerm->zerm.fix;
	HTREEITEM PItem = GetParentItem(hItem);
	if (PItem == NULL) return FALSE;
	ItemToZerm(PItem);
	if (Zerm->zerm.zerm_mat == Schnitt->schnitt.schnitt_mat) 
	{
		Zerm->zerm.zerm_mat = mat;
		Zerm->zerm.zerm_aus_mat = aus_mat;
		Zerm->zerm.zerm_aus_a_typ = aus_a_typ;
		Zerm->zerm.a_typ = a_typ;
		_tcscpy_s (Zerm->zerm.fix, Cfix.GetBuffer());
		return TRUE ; //Root nicht neu ber!!
	}
	double SumDm100 = (double) 0;
	double 	Sum_kosten_zutat = (double) 0;
	double 	Sum_gewicht_zutat = (double) 0;
	double 	Sum_kosten_vpk = (double) 0;
	double 	Sum_kosten = (double) 0;
	double  Sum_kosten_gh = (double) 0;
	double  Sum_kosten_eh = (double) 0;
	double 	ZusatzKosten = (double) 0;
	HTREEITEM hChildItem = GetChildItem(PItem);
	while (hChildItem != NULL)
	{
		ItemTo_k_ausZerlTreeData(hChildItem);
		if (k.a_typ == Zerm->TYP_VKARTIKEL && k.zerm_aus_mat == k.zerm_mat)
		{
			ZusatzKosten = k.pr_kosten; 
		}
		else
		{
			SumDm100 += k.dm_100;
			Sum_kosten_zutat += k.kosten_zutat ;
			Sum_kosten_vpk += k.kosten_vpk ;
			Sum_kosten += k.kosten ;
			if (k.a_typ == Zerm->TYP_ZUTAT)
			{
				Sum_gewicht_zutat += k.zerm_gew;
			}

			if (k.a_typ == Zerm->TYP_AUFSCHLAG)
			{
				Sum_kosten_gh += k.mat_o_b ;
			}
			if (k.a_typ == Zerm->TYP_AUFSCHLAG_EH)
			{
				Sum_kosten_eh += k.mat_o_b ;
			}
		}

		hChildItem = GetNextItem(hChildItem, TVGN_NEXT);
	}
	ItemToZerm(PItem);
	for (std::vector<CZerlTreeData *>::iterator e = ZerlTreeData.begin (); e != ZerlTreeData.end (); ++e)
	{
		CZerlTreeData *ete = *e;
		if (ete->zerm_mat == 0) continue;
		if (Zerm->zerm.zerm_mat == ete->zerm_mat && 
			Zerm->zerm.zerm_aus_mat == ete->zerm_aus_mat &&
			Zerm->zerm.a_typ == ete->a_typ)
		{
			if (!CStrFuncs::IsDoubleEqual (Schnitt->schnitt._wert_fwp, (double) 0))
			{
				if (!CStrFuncs::IsDoubleEqual (ete->zerm_gew_ant, (double) 0))
				{
					double sum_gewicht = ete->zerm_gew + Sum_gewicht_zutat;
					ete->kosten_zutat = Sum_kosten_zutat;
					ete->pr_zutat = ete->mat_o_b + (Sum_kosten_zutat / sum_gewicht);
					ete->kosten_vpk = Sum_kosten_vpk;
					ete->pr_vpk = ete->pr_zutat + (Sum_kosten_vpk / sum_gewicht);
					ete->kosten = Sum_kosten;
					ete->pr_kosten = ete->pr_vpk + (Sum_kosten / sum_gewicht);
					ete->hk_vollk = ete->pr_kosten; //020510

					ete->kosten_gh = Sum_kosten_gh; //020510
					ete->pr_vk_netto = ete->pr_kosten + (Sum_kosten_gh / sum_gewicht); //020510
					ete->kosten_eh = Sum_kosten_eh; //020510
					ete->pr_vk_eh = ete->pr_kosten + (Sum_kosten_eh / sum_gewicht); //020510

//020510					ete->hk_vollk = ete->teilkosten_abs + ete->kosten_zutat + ete->kosten_vpk + ete->kosten;
//020510					ete->hk_vollk /= sum_gewicht;
					/**200410 
					if (ZusatzKosten > (double) 0)
					{
						ete->hk_vollk = ZusatzKosten;
					}
					*/
					//ZuTun : 1 ersetzten mit ebene (ist noch nicht in ete-> drin 
					if (ete->a_typ == Zerm->TYP_MATERIAL)
					{
						// bei Materialien nur! Kosten aus der 3. Tabseite holen
						if (ZusatzKosten > ete->mat_o_b)
						{	
							ete->hk_vollk = HoleHKKosten(ete->mat_o_b + (ZusatzKosten - ete->mat_o_b),ete->a_kz.GetBuffer(),1,ete->a_z_f,ete->zerm_teil.GetBuffer());
						}
						else
						{
							ete->hk_vollk = HoleHKKosten(ete->mat_o_b ,ete->a_kz.GetBuffer(),1,ete->a_z_f,ete->zerm_teil.GetBuffer());
						}
					}

					if (ete->a_kz == "ZV")	ete->hk_vollk = (double) 0;
//020510					ete->kosten_gh = ete->hk_vollk * Sum_kosten_gh / 100;
//020510					ete->vk_pr = ete->hk_vollk + ete->kosten_gh;
//020510					ete->pr_vk_netto = ete->vk_pr / sum_gewicht;

//020510					ete->kosten_eh = ete->hk_vollk * Sum_kosten_eh / 100;
//020510					ete->pr_vk_eh = (ete->hk_vollk + ete->kosten_eh) / sum_gewicht;
					if (ete->a_typ == 5)
					{
						int di = 0;
					}

					double fwp = ete->fwp;
					if (ete->fix == "F") fwp = ete->fwp_org; 
					if (ete->mat_o_b > -9999 && ete->mat_o_b < 99999 && ete->dm_100 > -9999 && ete->dm_100 < 99999)
					{
				    Work->UpdateZermMatV(Schnitt->schnitt.mdn,
								   Schnitt->schnitt.schnitt,
								   ete->zerm_aus_mat,
								   ete->zerm_mat,
								   fwp,
								   ete->zerm_gew,
								   ete->zerm_gew_ant,
								   ete->mat_o_b,
								   ete->hk_vollk,
								   ete->hk_teilk,
								   ete->sk_vollk,
								   ete->sk_teilk,
								   ete->fil_ek_vollk,
								   ete->fil_ek_teilk,
								   ete->fil_vk_vollk,
								   ete->fil_vk_teilk,
								   ete->gh1_vollk,
								   ete->gh1_teilk,
								   ete->gh2_vollk,
								   ete->gh2_teilk,
								   ete->dm_100,
								   ete->kosten_zutat,
								   ete->kosten_vpk,
								   ete->fix.GetBuffer (),
								   ete->a_typ,
								   ete->teilkosten_abs,
								   ete->vk_pr,
								   ete->zerm_teil.GetBuffer(),
								   ete->zerm_mat, // in kost_mat
								   ete->zerm_mat_bz1.GetBuffer(), // in kost_mat_bz
								   ete->pr_vk_eh
								   );
					}
				}
			}
		}
	}
	Zerm->zerm.zerm_mat = mat;
	Zerm->zerm.zerm_aus_mat = aus_mat;
	Zerm->zerm.zerm_aus_a_typ = aus_a_typ;
	Zerm->zerm.a_typ = a_typ;
	_tcscpy_s (Zerm->zerm.fix, Cfix.GetBuffer());
	return TRUE;
}

void CMaTreeCtrl::ZutKostBer (HTREEITEM hItem, double mat_gew, double mat_hk_vollk)
{
	HTREEITEM hChildItem = GetChildItem(hItem);

	while (hChildItem != NULL)
	{
		ItemToZerm(hChildItem);
		for (std::vector<CZerlTreeData *>::iterator e = ZerlTreeData.begin (); e != ZerlTreeData.end (); ++e)
		{
			CZerlTreeData *ete = *e;
			if (ete->zerm_mat == 0) continue;
			if (Zerm->zerm.zerm_mat == ete->zerm_mat && 
				Zerm->zerm.zerm_aus_mat == ete->zerm_aus_mat &&
				Zerm->zerm.a_typ == ete->a_typ)
			{
				if (ete->a_typ == Zerm->TYP_ZUTAT)
				{
					ete->dm_100 = 0;
					ete->zerm_gew = ete->zerm_gew_ant * mat_gew / 1000;
					ete->teilkosten_abs = ete->mat_o_b * ete->zerm_gew;
					ete->kosten_zutat = ete->teilkosten_abs;
				}
				if (ete->a_typ == Zerm->TYP_VERPACKUNG)
				{
					ete->dm_100 = 0;
					ete->teilkosten_abs = ete->mat_o_b * ete->zerm_gew;// zerm_gew = Anzahl
					ete->kosten_vpk = ete->teilkosten_abs;
				}
				if (ete->a_typ == Zerm->TYP_KOSTEN)
				{
					ete->dm_100 = 0;
					ete->zerm_gew =  mat_gew ;
					ete->teilkosten_abs = ete->mat_o_b * mat_gew;
					ete->teilkosten_abs += ete->vollkosten_abs; //Fix-Kosten
					ete->kosten = ete->teilkosten_abs;
				}
				if (ete->a_typ == Zerm->TYP_AUFSCHLAG)
				{
					ete->dm_100 = 0;
					ete->zerm_gew = 0;
					ete->teilkosten_abs = mat_hk_vollk * ete->mat_o_b / 100;
					ete->vk_pr = ete->teilkosten_abs;
					ete->kosten_gh = ete->teilkosten_abs;
				}
				if (ete->a_typ == Zerm->TYP_AUFSCHLAG_EH)
				{
					ete->dm_100 = 0;
					ete->zerm_gew = 0;
					ete->teilkosten_abs = mat_hk_vollk * ete->mat_o_b / 100;
					ete->vk_pr = ete->teilkosten_abs;
					ete->kosten_eh = ete->teilkosten_abs;
				}
				double fwp = ete->fwp;
				if (ete->fix == "F") fwp = ete->fwp_org; 
				if (ete->mat_o_b > -9999 && ete->mat_o_b < 99999 && ete->dm_100 > -9999 && ete->dm_100 < 99999)
				{
			    Work->UpdateZermMatV(Schnitt->schnitt.mdn,
								   Schnitt->schnitt.schnitt,
								   ete->zerm_aus_mat,
								   ete->zerm_mat,
								   fwp,
								   ete->zerm_gew,
								   ete->zerm_gew_ant,
								   ete->mat_o_b,
								   ete->hk_vollk,
								   ete->hk_teilk,
								   ete->sk_vollk,
								   ete->sk_teilk,
								   ete->fil_ek_vollk,
								   ete->fil_ek_teilk,
								   ete->fil_vk_vollk,
								   ete->fil_vk_teilk,
								   ete->gh1_vollk,
								   ete->gh1_teilk,
								   ete->gh2_vollk,
								   ete->gh2_teilk,
								   ete->dm_100,
								   ete->kosten_zutat,
								   ete->kosten_vpk,
								   ete->fix.GetBuffer (),
								   ete->a_typ,
								   ete->teilkosten_abs,
								   ete->vk_pr,
								   ete->zerm_teil.GetBuffer(),
								   ete->zerm_mat, // in kost_mat
								   ete->zerm_mat_bz1.GetBuffer(), // in kost_mat_bz
								   ete->pr_vk_eh
								   );
				}

			}
		}
		hChildItem = GetNextItem(hChildItem, TVGN_NEXT);
	}
}


double CMaTreeCtrl::ItemToGew (HTREEITEM ht)
{
	long mat = Zerm->zerm.zerm_mat;
	long aus_mat = Zerm->zerm.zerm_aus_mat;
	short aus_a_typ = Zerm->zerm.zerm_aus_a_typ;
	short a_typ = Zerm->zerm.a_typ;
	CString Cfix = Zerm->zerm.fix;
	ItemToZerm(ht);
	for (std::vector<CZerlTreeData *>::iterator e = ZerlTreeData.begin (); e != ZerlTreeData.end (); ++e)
	{
		CZerlTreeData *ete = *e;
		if (ete->zerm_mat == 0) continue;
		if (Zerm->zerm.zerm_mat == ete->zerm_mat && 
			Zerm->zerm.zerm_aus_mat == ete->zerm_aus_mat &&
			Zerm->zerm.a_typ == ete->a_typ)
		{
			Zerm->zerm.zerm_mat = mat;
			Zerm->zerm.zerm_aus_mat = aus_mat;
			Zerm->zerm.a_typ = a_typ;
			Zerm->zerm.zerm_aus_a_typ = aus_a_typ;
			_tcscpy_s (Zerm->zerm.fix, Cfix.GetBuffer());
			return ete->zerm_gew;
		}
	}
	Zerm->zerm.zerm_mat = mat;
	Zerm->zerm.zerm_aus_mat = aus_mat;
	Zerm->zerm.a_typ = a_typ;
	Zerm->zerm.zerm_aus_a_typ = aus_a_typ;
	_tcscpy_s (Zerm->zerm.fix, Cfix.GetBuffer());
	return (double) 0;
}
double CMaTreeCtrl::ItemToGewAnt (HTREEITEM ht)
{
	long mat = Zerm->zerm.zerm_mat;
	long aus_mat = Zerm->zerm.zerm_aus_mat;
	short a_typ = Zerm->zerm.a_typ;
	short aus_a_typ = Zerm->zerm.zerm_aus_a_typ;
	CString Cfix = Zerm->zerm.fix;
	ItemToZerm(ht);
	for (std::vector<CZerlTreeData *>::iterator e = ZerlTreeData.begin (); e != ZerlTreeData.end (); ++e)
	{
		CZerlTreeData *ete = *e;
		if (ete->zerm_mat == 0) continue;
		if (Zerm->zerm.zerm_mat == ete->zerm_mat && 
			Zerm->zerm.zerm_aus_mat == ete->zerm_aus_mat &&
			Zerm->zerm.a_typ == ete->a_typ)
		{
			Zerm->zerm.zerm_mat = mat;
			Zerm->zerm.zerm_aus_mat = aus_mat;
			Zerm->zerm.a_typ = a_typ;
			Zerm->zerm.zerm_aus_a_typ = aus_a_typ;
			_tcscpy_s (Zerm->zerm.fix, Cfix.GetBuffer());
			return ete->zerm_gew_ant;
		}
	}
	Zerm->zerm.zerm_mat = mat;
	Zerm->zerm.zerm_aus_mat = aus_mat;
	Zerm->zerm.a_typ = a_typ;
	Zerm->zerm.zerm_aus_a_typ = aus_a_typ;
	_tcscpy_s (Zerm->zerm.fix, Cfix.GetBuffer());
	return (double) 0;
}
double CMaTreeCtrl::ItemTo_k_ausZerlTreeData (HTREEITEM ht)
{
	long mat = Zerm->zerm.zerm_mat;
	long aus_mat = Zerm->zerm.zerm_aus_mat;
	short a_typ = Zerm->zerm.a_typ;
	short aus_a_typ = Zerm->zerm.zerm_aus_a_typ;
	CString Cfix = Zerm->zerm.fix;
	if (ht == NULL) return (double) 0;
	ItemToZerm(ht);
	memcpy_s (&k,sizeof(K), &zerm_null, sizeof (ZERM)); 
	for (std::vector<CZerlTreeData *>::iterator e = ZerlTreeData.begin (); e != ZerlTreeData.end (); ++e)
	{
		CZerlTreeData *ete = *e;
		if (ete->zerm_mat == 0) continue;
		if (Zerm->zerm.zerm_mat == ete->zerm_mat && 
			Zerm->zerm.zerm_aus_mat == ete->zerm_aus_mat &&
			Zerm->zerm.a_typ == ete->a_typ)
		{
			Zerm->zerm.zerm_mat = mat;
			Zerm->zerm.zerm_aus_mat = aus_mat;
			Zerm->zerm.a_typ = a_typ;
			Zerm->zerm.zerm_aus_a_typ = aus_a_typ;

			k.a_typ = ete->a_typ;
			k.hk_teilk = ete->hk_teilk;
			k.hk_vollk = ete->hk_vollk;
			k.dm_100 = ete->dm_100;
			strcpy_s(k.fix,ete->fix);
			if (ete->fix == "F")
			{
				k.fwp = ete->fwp_org;
			}
			else
			{
				k.fwp = ete->fwp;
			}
			k.mat_o_b = ete->mat_o_b;

			k.kosten = (double) 0;
			k.pr_kosten =  (double) 0;
			k.kosten_zutat =  (double) 0;
			k.pr_zutat =  (double) 0;
			k.kosten_vpk =  (double) 0;
			k.pr_vpk = (double) 0;
			k.vk_pr = (double) 0;
			k.pr_vk_netto = (double) 0;
			k.pr_vk_eh = (double) 0;
			k.kosten_fix = (double) 0;
			k.kosten_gh =  (double) 0;
			k.kosten_eh =  (double) 0;
			if (k.a_typ == Zerm->TYP_MATERIAL)
			{
				k.kosten = ete->kosten;
				k.pr_kosten = ete->pr_kosten;
				k.kosten_zutat = ete->kosten_zutat;
				k.pr_zutat = ete->pr_zutat;
				k.kosten_vpk = ete->kosten_vpk;
				k.pr_vpk = ete->pr_vpk;
				k.vk_pr = ete->vk_pr;
				k.pr_vk_netto = ete->pr_vk_netto;
				k.pr_vk_eh = ete->pr_vk_eh;
				k.kosten_fix = ete->kosten_fix;
				k.kosten_gh = ete->kosten_gh;
				k.kosten_eh = ete->kosten_eh;
			}
			else if (k.a_typ == Zerm->TYP_VKARTIKEL) //211009
			{
				k.kosten = ete->kosten;
				k.pr_kosten = ete->pr_kosten;
				k.kosten_zutat = ete->kosten_zutat;
				k.pr_zutat = ete->pr_zutat;
				k.kosten_vpk = ete->kosten_vpk;
				k.pr_vpk = ete->pr_vpk;
				k.vk_pr = ete->vk_pr;
				k.pr_vk_netto = ete->pr_vk_netto;
				k.pr_vk_eh = ete->pr_vk_eh;
				k.kosten_fix = ete->kosten_fix;
				k.kosten_gh = ete->kosten_gh;
				k.kosten_eh = ete->kosten_eh;
			}
			else if (k.a_typ == Zerm->TYP_KOSTEN)
			{
				k.kosten = ete->teilkosten_abs ; 
 				ete->kosten = k.kosten;
				k.kosten_fix = ete->vollkosten_abs;
				ete->kosten_fix = k.kosten_fix;
				k.pr_kosten = ete->mat_o_b;
				ete->pr_kosten = k.pr_kosten;
			}
			else if (k.a_typ == Zerm->TYP_VERPACKUNG)
			{
				k.kosten_vpk = ete->teilkosten_abs;
				ete->kosten_vpk = k.kosten_vpk;
				k.pr_vpk = ete->mat_o_b;
				ete->pr_vpk = k.pr_vpk;
			}
			else if (k.a_typ == Zerm->TYP_ZUTAT)
			{
				k.kosten_zutat = ete->teilkosten_abs;
				ete->kosten_zutat = k.kosten_zutat;
				k.pr_zutat = ete->mat_o_b;
				ete->pr_zutat = k.pr_zutat;
			}
			else if (k.a_typ == Zerm->TYP_AUFSCHLAG)
			{
				k.vk_pr = ete->teilkosten_abs;
				ete->vk_pr = k.vk_pr;
				if (ete->mat_o_b != (double) 0)
				{
					k.pr_vk_netto = ete->teilkosten_abs + (ete->teilkosten_abs * 100 / ete->mat_o_b);
				}
				else
				{
					k.pr_vk_netto = (double) 0;
				}
				ete->pr_vk_netto = k.pr_vk_netto;
			}
			else if (k.a_typ == Zerm->TYP_AUFSCHLAG_EH)
			{
				if (ete->mat_o_b != (double) 0)
				{
					k.pr_vk_eh = ete->teilkosten_abs + (ete->teilkosten_abs * 100 / ete->mat_o_b);
				}
				else
				{
					k.pr_vk_eh = (double) 0;
				}
				ete->pr_vk_eh = k.pr_vk_eh;
			}




			k.teilkosten_abs = ete->teilkosten_abs;
			k.teilkosten_proz = ete->teilkosten_proz;
			k.vollkosten_abs = ete->vollkosten_abs;
			k.vollkosten_proz = ete->vollkosten_proz;
			k.zerm_mat = ete->zerm_mat;
			k.zerm_aus_mat = ete->zerm_aus_mat;
			k.zerm_aus_a_typ = ete->zerm_aus_a_typ;
			k.zerm_gew = ete->zerm_gew;
			k.zerm_gew_ant = ete->zerm_gew_ant;
			_tcscpy_s (k.zerm_teil, ete->zerm_teil.GetBuffer());
			return ete->dm_100;
		}
	}
	Zerm->zerm.zerm_mat = mat;
	Zerm->zerm.zerm_aus_mat = aus_mat;
	Zerm->zerm.zerm_aus_a_typ = aus_a_typ;
	Zerm->zerm.a_typ = a_typ;
	_tcscpy_s (Zerm->zerm.fix, Cfix.GetBuffer());
	return (double) 0;
}
//double CMaTreeCtrl::SummeDm100 ()
//{
//	double Sum_dm_100 = (double) 0;
//	for (std::vector<CZerlTreeData *>::iterator e = ZerlTreeData.begin (); e != ZerlTreeData.end (); ++e)
//	{
//		CZerlTreeData *ete = *e;
//		if (ete->zerm_mat == 0) continue;
//		if (_tstoi (ete->zerm_teil.GetBuffer ()) == 0) 
//		{
//			Sum_dm_100 += ete->dm_100;   
//		}
//	}
//	return Sum_dm_100;
//}
void CMaTreeCtrl::Umlasten ()
{
	HTREEITEM hpItem;
	HTREEITEM PhItemGeloescht;
	DestroyZerlTreeGt ();
	DestroyZerlTreeMatKost ();
	DestroyZerlTreeVkArtikel ();
	short ebene = 0;
	double SumDm100 = (double) 0;
	double IstSumPreis = (double) 0;
	double IstSumPreisHk = (double) 0;
	double IstSumPreisSk = (double) 0;
	double IstSumPreisFil_Ek = (double) 0;
	double IstSumPreisFil_Vk = (double) 0;
	double SumDm100Kosten = (double) 0;  //Zutaten und Kosten 
	double SumDm100Var =  (double) 0;
	short AnzVar =  0;
	PhItemGeloescht = NULL;

	// 1. Schleife : Summen holen 
	for (std::vector<CZerlTreeData *>::iterator e = ZerlTreeData.begin (); e != ZerlTreeData.end (); ++e)
	{
		CZerlTreeData *ete = *e;
		if (ete->zerm_mat == 0) continue;
						if (ete->zerm_mat == Zerm->Zerlegeverlust)
						{
							ete->dm_100 = 0.0;
							ete->fwp = 0.0;
							ete->fwp_org = 0.0;
							ete->teilkosten_abs = 0.0;
							ete->mat_o_b = 0.0;
						}
		// alle VK-Artikel  werden hier gespeichert zur Nachtr�glichen Berechnung
		if (ete->a_typ == Zerm->TYP_VKARTIKEL)
		{
			hpItem = GetParentItem (ete->Item);
			CZerlTreeVkArtikel *abl = new CZerlTreeVkArtikel (ete->Item,
										GetParentItem(ete->Item),
		                                GetChildItem(ete->Item) 
										);
			ZerlTreeVkArtikel.push_back (abl);

				hpItem = GetParentItem (ete->Item);
				ebene = 0;
				while (hpItem != NULL)
				{
					ebene ++;
					hpItem = GetParentItem (hpItem);
				}
				CZerlTreeMatKost *abl1 = new CZerlTreeMatKost (ete->Item,
											ete->zerm_aus_mat,
			                                ebene 
											);
				ZerlTreeMatKost.push_back (abl1);


			continue;
		}


		if (_tstoi (ete->zerm_teil.GetBuffer ()) != 1) 
		{
			if (ete->a_typ != Zerm->TYP_MATERIAL)  
			{
				SumDm100Kosten += ete->dm_100;   
			}
			else
			{
				if (ete->fix == "V") SumDm100 += ete->fwp_org * Schnitt->schnitt._wert_fwp * ete->zerm_gew_ant;
				if (ete->fix == "F") SumDm100 += ete->fwp_org * Schnitt->schnitt._wert_fwp * ete->zerm_gew_ant;
				if (ete->fix == "V") 
				{
					SumDm100Var += ete->fwp_org * Schnitt->schnitt._wert_fwp * ete->zerm_gew_ant;
					AnzVar ++;
				}
				 
				/*************
				switch (Schnitt->schnitt._PreisArt)
				{
				case 1 :
					if (ete->fix == "V") SumDm100 += ete->fwp_org * Schnitt->schnitt._wert_fwp * ete->zerm_gew_ant;
					if (ete->fix == "F") SumDm100 += ete->fwp_org * Schnitt->schnitt._wert_fwp * ete->zerm_gew_ant;
					if (ete->fix == "V") 
					{
						SumDm100Var += ete->fwp_org * Schnitt->schnitt._wert_fwp * ete->zerm_gew_ant;
						AnzVar ++;
					}
					break;
				case 2 :
					if (ete->fix == "V") SumDm100 += (ete->fwp_org * Schnitt->schnitt._wert_fwp + ete->hk_vollk) * ete->zerm_gew_ant;
					if (ete->fix == "F") SumDm100 += (ete->fwp_org * Schnitt->schnitt._wert_fwp + ete->hk_vollk) * ete->zerm_gew_ant;
					if (ete->fix == "V") 
					{
						if (ete->a_kz != "ZV")
						{
							SumDm100Var += (ete->fwp_org * Schnitt->schnitt._wert_fwp + ete->hk_vollk) * ete->zerm_gew_ant;
						}

						AnzVar ++;
					}
					break;
				case 3 :
					if (ete->fix == "V") SumDm100 += (ete->fwp_org * Schnitt->schnitt._wert_fwp + ete->sk_vollk) * ete->zerm_gew_ant;
					if (ete->fix == "F") SumDm100 += (ete->fwp_org * Schnitt->schnitt._wert_fwp + ete->sk_vollk) * ete->zerm_gew_ant;
					if (ete->fix == "V") 
					{
						SumDm100Var += (ete->fwp_org * Schnitt->schnitt._wert_fwp + ete->sk_vollk) * ete->zerm_gew_ant;
						AnzVar ++;
					}
					break;
				case 4 :
					if (ete->fix == "V") SumDm100 += (ete->fwp_org * Schnitt->schnitt._wert_fwp + ete->fil_ek_vollk) * ete->zerm_gew_ant;
					if (ete->fix == "F") SumDm100 += (ete->fwp_org * Schnitt->schnitt._wert_fwp + ete->fil_ek_vollk) * ete->zerm_gew_ant;
					if (ete->fix == "V") 
					{
						SumDm100Var += (ete->fwp_org * Schnitt->schnitt._wert_fwp + ete->fil_ek_vollk) * ete->zerm_gew_ant;
						AnzVar ++;
					}
					break;
				case 5 :
					if (ete->fix == "V") SumDm100 += (ete->fwp_org * Schnitt->schnitt._wert_fwp + ete->fil_vk_vollk) * ete->zerm_gew_ant;
					if (ete->fix == "F") SumDm100 += (ete->fwp_org * Schnitt->schnitt._wert_fwp + ete->fil_vk_vollk) * ete->zerm_gew_ant;
					if (ete->fix == "V") 
					{
						SumDm100Var += (ete->fwp_org * Schnitt->schnitt._wert_fwp + ete->fil_vk_vollk) * ete->zerm_gew_ant;
						AnzVar ++;
					}
					break;
				}
				********/
			}
		}
		// alle GT werden hier gespeichert mit angabe der Ebene des Baumes um die 
		// GTs nach der Umlastung neu zu berechnen
		if (ete->zerm_mat == Zerm->Zerlegeverlust && ete->a_typ == Zerm->TYP_MATERIAL) 
		{
			if (ete->zerm_gew_ant_parent == 100.0)   //Hier Zerlegeverlust wieder rausschmeissen, wenn sonst nix da
			{
				ete->zerm_mat = 0;  
				ete->zerm_aus_mat = 0;
				PhItemGeloescht = GetParentItem (ete->Item);
				DeleteItem(ete->Item);
			}
			else
			{
				hpItem = GetParentItem (ete->Item);
				ebene = 0;
				while (hpItem != NULL)
				{
					ebene ++;
					hpItem = GetParentItem (hpItem);
				}
				CZerlTreeGt *abl = new CZerlTreeGt (ete->Item,
											ete->zerm_aus_mat,
											ete->zerm_aus_mat,
			                                ebene,
											ete->fix.GetBuffer()
											);
				ZerlTreeGt.push_back (abl);
			}
		}
		// alle Mat. mit Kosten ...  werden hier gespeichert mit angabe der Ebene des Baumes um die 
		// Mats nach der Umlastung neu zu berechnen
		long OldAusMat = 0;
		if (ete->a_typ != Zerm->TYP_MATERIAL)
		{
			if (OldAusMat != Zerm->zerm.zerm_aus_mat)
			{
				hpItem = GetParentItem (ete->Item);
				ebene = 0;
				while (hpItem != NULL)
				{
					ebene ++;
					hpItem = GetParentItem (hpItem);
				}
				CZerlTreeMatKost *abl = new CZerlTreeMatKost (ete->Item,
											ete->zerm_aus_mat,
			                                ebene 
											);
				ZerlTreeMatKost.push_back (abl);
			}
		}
	}

		if (CStrFuncs::IsDoubleEqual (SumDm100, (double) 0)) return ;
		double DeckungA = Schnitt->schnitt.deckung_proz * (Schnitt->schnitt.ek_pr * 100) / 100;
		double SumPreis  = (Schnitt->schnitt.ek_pr * 100) + DeckungA;
		double SollVarDm100 = SumPreis - (SumDm100 - SumDm100Var);
		double Faktor = 1.0 ;
		if (CStrFuncs::IsDoubleEqual (SumDm100Var, (double) 0))
		{
			Faktor = 1.0;
		}
		else
		{
			Faktor = SollVarDm100 / SumDm100Var ;
		}
		if (!CStrFuncs::IsDoubleEqual (Faktor, (double) 0)) 
		{
		  // 2. Schleife Werte neu rechnen und speichern 
		for (std::vector<CZerlTreeData *>::iterator e = ZerlTreeData.begin (); e != ZerlTreeData.end (); ++e)
		{
			CZerlTreeData *ete = *e;
			if (ete->zerm_mat == 0) continue;
			if (_tstoi (ete->zerm_teil.GetBuffer ()) != 1) // && ete->fix == "V")
			{
				if (!CStrFuncs::IsDoubleEqual (Schnitt->schnitt._wert_fwp, (double) 0))
				{
					if (!CStrFuncs::IsDoubleEqual (ete->zerm_gew_ant, (double) 0))
					{
						if (AnzVar == 1)
						{
							ete->dm_100 = SollVarDm100 ;
						}
						else
						{
							ete->dm_100 = Faktor * ete->fwp_org * Schnitt->schnitt._wert_fwp * ete->zerm_gew_ant;
/*************
							if (!CStrFuncs::IsDoubleEqual (ete->dm_100, (double) 0))
							{
								switch (Schnitt->schnitt._PreisArt)
								{
								case 2 :
									ete->dm_100 -= ete->hk_vollk;
									break;
								case 3 :
									ete->dm_100 -= ete->sk_vollk;
									break;
								}
							}
***/




						}
						if (ete->fix == "F") //alter Zustand wieder herstellen
						{
							ete->dm_100 = ete->fwp_org * Schnitt->schnitt._wert_fwp * ete->zerm_gew_ant;
						}
						if (ete->a_typ == Zerm->TYP_MATERIAL)
						{
							ete->mat_o_b  = ete->dm_100 / ete->zerm_gew_ant;
							ete->fwp =  ete->mat_o_b / Schnitt->schnitt._wert_fwp; 
							ete->teilkosten_abs  =  ete->mat_o_b * ete->zerm_gew; 
						}
						if (ete->zerm_mat == Zerm->Zerlegeverlust)
						{
							ete->dm_100 = 0.0;
							ete->fwp = 0.0;
							ete->fwp_org = 0.0;
							ete->teilkosten_abs = 0.0;
							ete->mat_o_b = 0.0;
						}
//						ete->hk_teilk = (double) 0;
//						ete->hk_vollk = (double) 0;
						ete->kosten = (double) 0;
						ete->kosten = (double) 0;
						ete->kosten_vpk = (double) 0;
						ete->kosten_zutat = (double) 0;
						ete->pr_kosten = (double) 0;
						ete->pr_vk_eh = (double) 0;
						ete->pr_vk_netto = (double) 0;
						ete->pr_vpk = (double) 0;
						ete->pr_zutat = (double) 0;
//160408						ete->vk_pr = (double) 0;
//150708						ete->vollkosten_abs = (double) 0; 
						ete->vollkosten_proz = (double) 0;
						ete->teilkosten_proz = (double) 0;
						ete->kosten_gh = (double) 0;
						ete->kosten_eh = (double) 0;
	
						if (ete->mat_o_b > -9999 && ete->mat_o_b < 99999 && ete->dm_100 > -9999 && ete->dm_100 < 99999)
						{
						Work->UpdateZermMatV(Schnitt->schnitt.mdn,
								   Schnitt->schnitt.schnitt,
								   ete->zerm_aus_mat,
								   ete->zerm_mat,
								   ete->fwp,
								   ete->zerm_gew,
								   ete->zerm_gew_ant,
								   ete->mat_o_b,
								   ete->hk_vollk,
								   ete->hk_teilk,
								   ete->sk_vollk,
								   ete->sk_teilk,
								   ete->fil_ek_vollk,
								   ete->fil_ek_teilk,
								   ete->fil_vk_vollk,
								   ete->fil_vk_teilk,
								   ete->gh1_vollk,
								   ete->gh1_teilk,
								   ete->gh2_vollk,
								   ete->gh2_teilk,
								   ete->dm_100,
								   ete->kosten_zutat,
								   ete->kosten_vpk,
								   ete->fix.GetBuffer (),
								   ete->a_typ,
								   ete->teilkosten_abs,
								   ete->vk_pr,
								   ete->zerm_teil.GetBuffer(),
								   ete->zerm_mat, // in kost_mat
								   ete->zerm_mat_bz1.GetBuffer(), // in kost_mat_bz
								   ete->pr_vk_eh
								   );
						}
						}
					}
				}
			}
		}
//	}

  if (PhItemGeloescht != NULL)
  {
	ItemToZerm(PhItemGeloescht);
	for (std::vector<CZerlTreeData *>::iterator e = ZerlTreeData.begin (); e != ZerlTreeData.end (); ++e)
	{
		CZerlTreeData *ete = *e;
		if (ete->zerm_mat == 0) continue;
		if (Zerm->zerm.zerm_mat == ete->zerm_mat && 
			Zerm->zerm.zerm_aus_mat == ete->zerm_aus_mat &&
			Zerm->zerm.a_typ == ete->a_typ)
		{
			ete->zerm_teil = "0";
		}
	}
  }

	// 3. Grobteile neu berechnen
	short max_ebene = 0;
	for (std::vector<CZerlTreeGt *>::iterator e = ZerlTreeGt.begin (); e != ZerlTreeGt.end (); ++e)
	{
		CZerlTreeGt *ete = *e;
		if (ete->ebene > max_ebene) max_ebene = ete->ebene;
	}
	for (ebene = max_ebene;ebene > 0;ebene --)
	{
		for (std::vector<CZerlTreeGt *>::iterator e = ZerlTreeGt.begin (); e != ZerlTreeGt.end (); ++e)
		{
			CZerlTreeGt *ete = *e;
			if (ete->ebene == ebene)
			{
				GrobteilBer(ete->Item);
			}
		}
	}
	// 4. Materialien mit Kosten neu berechnen
	max_ebene = 0;
	for (std::vector<CZerlTreeMatKost *>::iterator e = ZerlTreeMatKost.begin (); e != ZerlTreeMatKost.end (); ++e)
	{
		CZerlTreeMatKost *ete = *e;
		if (ete->ebene > max_ebene) max_ebene = ete->ebene;
	}
	for (ebene = max_ebene;ebene > 0;ebene --)
	{
		for (std::vector<CZerlTreeMatKost *>::iterator e = ZerlTreeMatKost.begin (); e != ZerlTreeMatKost.end (); ++e)
		{
			CZerlTreeMatKost *ete = *e;
			if (ete->ebene == ebene)
			{
				MatKostenBer(ete->Item);
				HTREEITEM hP = GetParentItem(ete->Item);
				if (hP != NULL)
				{
					ItemTo_k_ausZerlTreeData (hP);
					ZutKostBer(hP,k.zerm_gew,k.hk_vollk);
				}
			}
		}
	}
	// 5. Vk-Artikel neu berechnen
	for (std::vector<CZerlTreeVkArtikel *>::iterator e = ZerlTreeVkArtikel.begin (); e != ZerlTreeVkArtikel.end (); ++e)
	{
		CZerlTreeVkArtikel *ete = *e;
		if (ete->PItem != NULL) 
		{
			VkArtikelBer(ete->Item,ete->PItem,ete->CItem);
		}

	}
	// 6. Materialien mit Kosten noch einmal neu berechnen
	max_ebene = 0;
	for (std::vector<CZerlTreeMatKost *>::iterator e = ZerlTreeMatKost.begin (); e != ZerlTreeMatKost.end (); ++e)
	{
		CZerlTreeMatKost *ete = *e;
		if (ete->ebene > max_ebene) max_ebene = ete->ebene;
	}
	for (ebene = max_ebene;ebene > 0;ebene --)
	{
		for (std::vector<CZerlTreeMatKost *>::iterator e = ZerlTreeMatKost.begin (); e != ZerlTreeMatKost.end (); ++e)
		{
			CZerlTreeMatKost *ete = *e;
			if (ete->ebene == ebene)
			{
				MatKostenBer(ete->Item);
				HTREEITEM hP = GetParentItem(ete->Item);
				if (hP != NULL)
				{
					ItemTo_k_ausZerlTreeData (hP);
					ZutKostBer(hP,k.zerm_gew,k.hk_vollk);
				}
			}
		}
	// 7. Vk-Artikel noch mal neu berechnen
		for (std::vector<CZerlTreeVkArtikel *>::iterator e = ZerlTreeVkArtikel.begin (); e != ZerlTreeVkArtikel.end (); ++e)
		{
			CZerlTreeVkArtikel *ete = *e;
			if (ete->PItem != NULL) 
			{
				VkArtikelBer(ete->Item,ete->PItem,ete->CItem);
			}
		}
	}


	// 8. Schleife : IstSumDm100 holen 
	for (std::vector<CZerlTreeData *>::iterator e = ZerlTreeData.begin (); e != ZerlTreeData.end (); ++e)
	{
		CZerlTreeData *ete = *e;
		if (ete->zerm_mat == 0) continue;
		if (_tstoi (ete->zerm_teil.GetBuffer ()) != 1) 
		{
			if (ete->a_typ == Zerm->TYP_MATERIAL)  
			{
//170111				IstSumPreis += ete->fwp * Schnitt->schnitt._wert_fwp * ete->zerm_gew_ant;
				IstSumPreis += ete->mat_o_b * ete->zerm_gew_ant; //170111
//				IstSumPreisHk += (ete->fwp * Schnitt->schnitt._wert_fwp + ete->hk_vollk)  * ete->zerm_gew_ant;
//				IstSumPreisSk += (ete->fwp * Schnitt->schnitt._wert_fwp + ete->sk_vollk)  * ete->zerm_gew_ant;
//				IstSumPreisFil_Ek += (ete->fwp * Schnitt->schnitt._wert_fwp + ete->fil_ek_vollk)  * ete->zerm_gew_ant;
//				IstSumPreisFil_Vk += (ete->fwp * Schnitt->schnitt._wert_fwp + ete->fil_vk_vollk)  * ete->zerm_gew_ant;
				IstSumPreisHk += (ete->hk_vollk)  * ete->zerm_gew_ant;
				IstSumPreisSk += (ete->sk_vollk)  * ete->zerm_gew_ant;
				IstSumPreisFil_Ek += (ete->fil_ek_vollk)  * ete->zerm_gew_ant;
				IstSumPreisFil_Vk += (ete->fil_vk_vollk)  * ete->zerm_gew_ant;
			}
		}
	}
	Schnitt->schnitt._SumPreis = 0.0;
	Schnitt->schnitt._DeckungA = 0.0;
	Schnitt->schnitt._DeckungP = 0.0;
	Schnitt->schnitt._AnzVariabel = AnzVar;
	if (!CStrFuncs::IsDoubleEqual (Schnitt->schnitt.ek_gew, 0.0,2))
	{
		if (!CStrFuncs::IsDoubleEqual (Schnitt->schnitt.ek_pr, 0.0,2))
		{
		Schnitt->schnitt._SumPreis = IstSumPreis /100 * Schnitt->schnitt.ek_gew;
		Schnitt->schnitt._DeckungA = Schnitt->schnitt._SumPreis - (Schnitt->schnitt.ek_pr * Schnitt->schnitt.ek_gew);
		Schnitt->schnitt._DeckungP = Schnitt->schnitt._DeckungA * 100 / (Schnitt->schnitt.ek_pr * Schnitt->schnitt.ek_gew);
		switch (Schnitt->schnitt._PreisArt)
		{
			case 2 :
				Schnitt->schnitt._SumPreisHK = IstSumPreisHk /100 * Schnitt->schnitt.ek_gew;
				Schnitt->schnitt._DeckungAHK = Schnitt->schnitt._SumPreisHK - (Schnitt->schnitt.ek_pr * Schnitt->schnitt.ek_gew);
				Schnitt->schnitt._DeckungPHK = Schnitt->schnitt._DeckungAHK * 100 / (Schnitt->schnitt.ek_pr * Schnitt->schnitt.ek_gew);
				Schnitt->schnitt._SumHK = Schnitt->schnitt._SumPreisHK - Schnitt->schnitt._SumPreis;
				break;
			case 3 :
				Schnitt->schnitt._SumPreisSK = IstSumPreisSk /100 * Schnitt->schnitt.ek_gew;
				Schnitt->schnitt._DeckungASK = Schnitt->schnitt._SumPreisSK - (Schnitt->schnitt.ek_pr * Schnitt->schnitt.ek_gew);
				Schnitt->schnitt._DeckungPSK = Schnitt->schnitt._DeckungASK * 100 / (Schnitt->schnitt.ek_pr * Schnitt->schnitt.ek_gew);
				break;
			case 4 :
				Schnitt->schnitt._SumPreisFIL_EK = IstSumPreisFil_Ek /100 * Schnitt->schnitt.ek_gew;
				Schnitt->schnitt._DeckungAFIL_EK = Schnitt->schnitt._SumPreisFIL_EK - (Schnitt->schnitt.ek_pr * Schnitt->schnitt.ek_gew);
				Schnitt->schnitt._DeckungPFIL_EK = Schnitt->schnitt._DeckungAFIL_EK * 100 / (Schnitt->schnitt.ek_pr * Schnitt->schnitt.ek_gew);
				break;
			case 5 :
				Schnitt->schnitt._SumPreisFIL_VK = IstSumPreisFil_Vk /100 * Schnitt->schnitt.ek_gew;
				Schnitt->schnitt._DeckungAFIL_VK = Schnitt->schnitt._SumPreisFIL_VK - (Schnitt->schnitt.ek_pr * Schnitt->schnitt.ek_gew);
				Schnitt->schnitt._DeckungPFIL_VK = Schnitt->schnitt._DeckungAFIL_VK * 100 / (Schnitt->schnitt.ek_pr * Schnitt->schnitt.ek_gew);
				break;
			default :
				Schnitt->schnitt._SumPreis = IstSumPreis /100 * Schnitt->schnitt.ek_gew;
				Schnitt->schnitt._DeckungA = Schnitt->schnitt._SumPreis - (Schnitt->schnitt.ek_pr * Schnitt->schnitt.ek_gew);
				Schnitt->schnitt._DeckungP = Schnitt->schnitt._DeckungA * 100 / (Schnitt->schnitt.ek_pr * Schnitt->schnitt.ek_gew);
				break;
		}

		}
	}


}

HTREEITEM CMaTreeCtrl::ZermToItem ()
{
	for (std::vector<CZerlTreeData *>::iterator e = ZerlTreeData.begin (); e != ZerlTreeData.end (); ++e)
	{
		CZerlTreeData *ete = *e;
		if (ete->zerm_mat == 0) continue;
		if (Zerm->zerm.zerm_mat == ete->zerm_mat && 
			Zerm->zerm.zerm_aus_mat == ete->zerm_aus_mat &&
			Zerm->zerm.a_typ == ete->a_typ)
		{
			return ete->Item;
		}
	}
	return NULL;
}

BOOL CMaTreeCtrl::IsZerlVerl (HTREEITEM ht)
{
	long mat = Zerm->zerm.zerm_mat;
	long aus_mat = Zerm->zerm.zerm_aus_mat;
	short aus_a_typ = Zerm->zerm.zerm_aus_a_typ;
	short a_typ = Zerm->zerm.a_typ;
	CString Cfix = Zerm->zerm.fix;
	ItemToZerm(ht);
	if (Zerm->zerm.zerm_mat == Zerm->Zerlegeverlust && Zerm->zerm.a_typ == Zerm->TYP_MATERIAL) 
	{
		Zerm->zerm.zerm_mat = mat;
		Zerm->zerm.zerm_aus_mat = aus_mat;
		Zerm->zerm.a_typ = a_typ;
		Zerm->zerm.zerm_aus_a_typ = aus_a_typ;
		_tcscpy_s (Zerm->zerm.fix, Cfix.GetBuffer());
		return TRUE;
	}
	Zerm->zerm.zerm_mat = mat;
	Zerm->zerm.zerm_aus_mat = aus_mat;
	Zerm->zerm.a_typ = a_typ;
	Zerm->zerm.zerm_aus_a_typ = aus_a_typ;
	_tcscpy_s (Zerm->zerm.fix, Cfix.GetBuffer());
	return FALSE;
}


BOOL CMaTreeCtrl::ItemToZerm (HTREEITEM ht)
{
	if (ht == NULL) return FALSE;

	CString Text = GetItemText (ht);
	CToken t (Text, " ");
	int count = t.GetAnzToken ();
	if (count < 2) return FALSE;

	_tcscpy_s (Zerm->zerm.variabel, "N");
	_tcscpy_s (Zerm->zerm.fix, "F");
	if (GetCheck(ht))
	{
		_tcscpy_s (Zerm->zerm.variabel, "J"); 
		_tcscpy_s (Zerm->zerm.fix, "V");
	}
	Zerm->zerm.zerm_mat = (long)CStrFuncs::StrToDouble (t.GetToken (0));
	Zerm->zerm.a_typ = Zerm->TYP_MATERIAL;
	if (Zerm->zerm.zerm_mat == 0.0)
	{
		CString CTyp = t.GetToken (0);
		Zerm->zerm.a_typ = TypKennzeichen2ATyp (CTyp);
		Zerm->zerm.zerm_mat = (long)CStrFuncs::StrToDouble (t.GetToken (1));
	}
//	Hier brauche ich noch das Ausgangsmaterial !!!
	HTREEITEM htp = GetParentItem (ht);
	Text = GetItemText (htp);
	CToken tp (Text, " ");
	count = tp.GetAnzToken ();
	if (count == 0) return FALSE;
	if (count == 1)
	{
		Zerm->zerm.zerm_aus_mat = (long)CStrFuncs::StrToDouble (Text);
		Zerm->zerm.zerm_aus_a_typ = Zerm->TYP_MATERIAL; 
	}
	else
	{
		Zerm->zerm.zerm_aus_mat = (long)CStrFuncs::StrToDouble (tp.GetToken (0));
		if (Zerm->zerm.zerm_aus_mat == 0.0)
		{
			CString CTyp = t.GetToken (0);
			Zerm->zerm.zerm_aus_a_typ = TypKennzeichen2ATyp (CTyp);
			Zerm->zerm.zerm_aus_mat = (long)CStrFuncs::StrToDouble (tp.GetToken (1));
		}
		else
		{
			Zerm->zerm.zerm_aus_a_typ = Zerm->TYP_MATERIAL;
		}
	}
	return TRUE;

}
BOOL CMaTreeCtrl::ZerlTreeDataToDB ()
{
		int ret = MessageBox (_T("Speichern ? "), NULL, MB_YESNO | MB_ICONQUESTION);
		if  (ret != IDYES) return TRUE;

    short lfd = 0;
	int KalkSave;
	CString Date;
	CStrFuncs::SysDate (Date);
	Work->beginwork();
	ret = Work->DelZerm(Schnitt->schnitt.mdn,Schnitt->schnitt.schnitt);
	if (ret < 0) return FALSE;
    if (Schnitt->schnitt.kalk_bas == 2) //Marktpreise
    {
		KalkSave = MessageBox (_T("ge�nderte Preise in die Preisgruppenstufe zur�ckschreiben  "), NULL, MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2);
	}
	else
	{
		KalkSave = MessageBox (_T("Kalkulationsergebnisse in den Artikelstamm zur�ckschreiben "), NULL, MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2);
	}
	for (std::vector<CZerlTreeData *>::iterator e = ZerlTreeData.begin (); e != ZerlTreeData.end (); ++e)
	{
		CZerlTreeData *ete = *e;
		int KalkOk = 1;
		if (ete->zerm_mat == 0) continue;
		if (ete->a_typ == Zerm->TYP_KOSTEN) KalkOk = 0;
		if (ete->a_typ == Zerm->TYP_ZUTAT) KalkOk = 0;
		if (ete->a_typ == Zerm->TYP_VERPACKUNG) KalkOk = 0;
		if (ete->a_typ == Zerm->TYP_AUFSCHLAG) KalkOk = 0;
		if (ete->a_typ == Zerm->TYP_AUFSCHLAG_EH) KalkOk = 0;
		if (ete->zerm_mat == 1133)
		{
			int di = 0;
		}
		Zerm->zerm.mdn = Schnitt->schnitt.mdn;
		Zerm->zerm.schnitt = Schnitt->schnitt.schnitt;
		Zerm->zerm.zerm_aus_mat = ete->zerm_aus_mat;
		Zerm->zerm.zerm_aus_a_typ = ete->zerm_aus_a_typ;
		Zerm->zerm.zerm_mat = ete->zerm_mat;
    	_tcscpy_s (Zerm->zerm.a_kz, ete->a_kz.GetBuffer ());
    	_tcscpy_s (Zerm->zerm.fix, ete->fix.GetBuffer ());
		if (ete->fix == "F")
		{
			Zerm->zerm.fwp = ete->fwp_org;
		}
		else
		{
			Zerm->zerm.fwp = ete->fwp;
		}
		Zerm->zerm.fwp_org = ete->fwp_org;
		if (KalkSave == IDYES )
		{
			Zerm->zerm.fwp_org = ete->fwp; // beim Zur�ckspeichern in a_varb, wird auch fwp_org neu gesetzt!
		}
		if (ete->a_typ == 0) ete->a_typ = 1;
		Zerm->zerm.a_typ = ete->a_typ;
		Zerm->zerm.zerm_gew = ete->zerm_gew;
		Zerm->zerm.zerm_gew_ant = ete->zerm_gew_ant;
    	_tcscpy_s (Zerm->zerm.zerm_teil, ete->zerm_teil.GetBuffer ());
		Zerm->zerm.mat_o_b = ete->mat_o_b;
		Zerm->zerm.hk_vollk = ete->hk_vollk;
		Zerm->zerm.hk_teilk = ete->hk_teilk;
		Zerm->zerm.delstatus = ete->delstatus;
		Zerm->zerm.vollkosten_abs = ete->vollkosten_abs;
		Zerm->zerm.vollkosten_proz = ete->vollkosten_proz;
		Zerm->zerm.teilkosten_abs = ete->teilkosten_abs;
		Zerm->zerm.teilkosten_proz = ete->teilkosten_proz;
		Zerm->zerm.dm_100 = ete->dm_100;
		Zerm->zerm.a_z_f = ete->a_z_f;
		Zerm->zerm.vk_pr = ete->vk_pr;
		Zerm->zerm.pr_zutat = ete->pr_zutat;
		Zerm->zerm.pr_vpk = ete->pr_vpk;
		Zerm->zerm.pr_vk_netto = ete->pr_vk_netto;
		Zerm->zerm.pr_vk_eh = ete->pr_vk_eh;
		Zerm->zerm.pr_kosten = ete->pr_kosten;
	    Zerm->zerm.kosten = ete->kosten; 
	    Zerm->zerm.kosten_vpk = ete->kosten_vpk;
	    Zerm->zerm.kosten_zutat = ete->kosten_zutat;
		Zerm->zerm.kosten_fix = ete->kosten_fix;
		Zerm->zerm.kosten_gh = ete->kosten_gh;
		Zerm->zerm.kosten_eh = ete->kosten_eh;
		Zerm->zerm.lfd = lfd;
		lfd++;

//			if (ItemHasChildren(ete->Item)) funkt. nicht !!!
    		_tcscpy_s (Zerm->zerm.zerm_teil, BestimmeZerm_teil(ete->Item));
			if (Zerm->zerm.a_typ != Zerm->TYP_MATERIAL && Zerm->zerm.a_typ != Zerm->TYP_VKARTIKEL)
			{
    			_tcscpy_s (Zerm->zerm.fix, "F");
			}
// jetzt bei allen !!		if (Zerm->zerm.a_typ == Zerm->TYP_KOSTEN || Zerm->zerm.a_typ == Zerm->TYP_AUFSCHLAG)
//		{
			_tcscpy_s (Zerm->zerm.kost_bz, ete->zerm_mat_bz1.GetBuffer());
//		}


        if (PruefeZerm() == FALSE)
		{
			Work->rollbackwork();
			return FALSE;
		}

		ret = Zerm->dbupdate();
		if (ret != 0)
		{
			Work->rollbackwork();
			return FALSE;
		}
		if (Zerm->zerm.zerm_mat == Zerm->zerm.zerm_aus_mat && Zerm->zerm.a_typ == Zerm->TYP_VKARTIKEL)
		{
			KalkOk = 0;  //290410 Wenn der Vk-Artikel keine eigene Artikelnummer hat, darf er hier nicht nochmal gespeichert werden!
		}
		if (KalkSave == IDYES && KalkOk == 1)
		{
		  if (Schnitt->schnitt.kalk_bas == 2) //Marktpreise
		  {
			A_mat->a_mat.mat = Zerm->zerm.zerm_mat;
			A_mat->dbreadfirst();
			memcpy (&Work->Ipr.ipr, &ipr_null, sizeof (IPR));
			Work->Ipr.ipr.mdn = Zerm->zerm.mdn;;
			Work->Ipr.ipr.a = A_mat->a_mat.a;
			Work->Ipr.ipr.pr_gr_stuf = Schnitt->PrGrStuf;
			Work->Ipr.ipr.kun_pr =  0;
			Work->Ipr.ipr.kun =  0;
			Work->Ipr.dbreadfirst();
			Work->Ipr.ipr.vk_pr_i = Zerm->zerm.mat_o_b;
			Work->Ipr.ipr.vk_pr_eu = Zerm->zerm.mat_o_b;
			Work->Ipr.dbupdate();
		  }
		  else
		  {
			if (strcmp (Zerm->zerm.fix, "V") == 0 || Schnitt->cfg_NurVariableMaterialienSpeichern == 0 )
			{
				A_mat->a_mat.mat = Zerm->zerm.zerm_mat;
				A_mat->dbreadfirst();
				Work->ABas.a_bas.a = A_mat->a_mat.a;
				Work->ABas.dbreadfirst();
				if (Work->ABas.a_bas.a_typ == 2 || Work->ABas.a_bas.a_typ2 == 2) 
				{
					//===== a_kalkpreis ===========
					memcpy (&Work->AKalkEig.a_kalk_eig, &a_kalk_eig_null, sizeof (A_KALK_EIG));
					Work->AKalkEig.a_kalk_eig.mdn = Zerm->zerm.mdn;
					Work->AKalkEig.a_kalk_eig.a = A_mat->a_mat.a;
					Work->AKalkEig.dbreadfirst();

					Work->AKalkEig.a_kalk_eig.mat_o_b = Zerm->zerm.mat_o_b;
					Work->AKalkEig.a_kalk_eig.hk_teilk = Zerm->zerm.mat_o_b;
					Work->AKalkEig.a_kalk_eig.hk_vollk = Zerm->zerm.mat_o_b;
					Work->AKalkEig.a_kalk_eig.kost = 0.0;
					if (Zerm->zerm.hk_vollk > (double) 0)
					{
						Work->AKalkEig.a_kalk_eig.hk_vollk = Zerm->zerm.hk_vollk;
						Work->AKalkEig.a_kalk_eig.kost = Zerm->zerm.hk_vollk - Zerm->zerm.mat_o_b;
					}
					Zerm->ToDbDate (Date, &Work->AKalkEig.a_kalk_eig.dat);
					Zerm->ToDbDate (Date, &Work->AKalkEig.a_kalk_eig.aend_dat);
					Work->AKalkEig.dbupdate();
					if (Schnitt->cfg_Multimandant == 1 && Zerm->zerm.mdn != 0)  //14.12.09 dann auch in Mandant 0 speichern  
					{
						memcpy (&Work->AKalkEig.a_kalk_eig, &a_kalk_eig_null, sizeof (A_KALK_EIG));
						Work->AKalkEig.a_kalk_eig.mdn = 0;   // ToDo  alle Mandanten aus tab mdn
						Work->AKalkEig.a_kalk_eig.a = A_mat->a_mat.a;
						Work->AKalkEig.dbreadfirst();
	
						Work->AKalkEig.a_kalk_eig.mat_o_b = Zerm->zerm.mat_o_b;
						Work->AKalkEig.a_kalk_eig.hk_teilk = Zerm->zerm.mat_o_b;
						Work->AKalkEig.a_kalk_eig.hk_vollk = Zerm->zerm.mat_o_b;
						Work->AKalkEig.a_kalk_eig.kost = 0.0;
						if (Zerm->zerm.hk_vollk > (double) 0)
						{
							Work->AKalkEig.a_kalk_eig.hk_vollk = Zerm->zerm.hk_vollk;
							Work->AKalkEig.a_kalk_eig.kost = Zerm->zerm.hk_vollk - Zerm->zerm.mat_o_b;
						}
						Zerm->ToDbDate (Date, &Work->AKalkEig.a_kalk_eig.dat);
						Zerm->ToDbDate (Date, &Work->AKalkEig.a_kalk_eig.aend_dat);
						Work->AKalkEig.dbupdate();
					}
				}
				if (Work->ABas.a_bas.a_typ == 1 || Work->ABas.a_bas.a_typ2 == 1) 
				{
					//===== a_kalkpreis ===========
					memcpy (&Work->AKalkHndw.a_kalkhndw, &a_kalkhndw_null, sizeof (A_KALKHNDW));
					Work->AKalkHndw.a_kalkhndw.mdn = Zerm->zerm.mdn;
					Work->AKalkHndw.a_kalkhndw.a = A_mat->a_mat.a;
					Work->AKalkHndw.dbreadfirst();

					Work->AKalkHndw.a_kalkhndw.pr_ek1 = Zerm->zerm.mat_o_b;
					Work->AKalkHndw.a_kalkhndw.sk_teilk = Zerm->zerm.mat_o_b;
					Work->AKalkHndw.a_kalkhndw.sk_vollk = Zerm->zerm.mat_o_b;
					if (Zerm->zerm.hk_vollk > (double) 0)
					{
						Work->AKalkHndw.a_kalkhndw.sk_vollk = Zerm->zerm.hk_vollk;
					}
					Zerm->ToDbDate (Date, &Work->AKalkHndw.a_kalkhndw.dat);
					Zerm->ToDbDate (Date, &Work->AKalkHndw.a_kalkhndw.aend_dat);
					Work->AKalkHndw.dbupdate();
					if (Schnitt->cfg_Multimandant == 1 && Zerm->zerm.mdn != 0)  //14.12.09 dann auch in Mandant 0 speichern  
					{
						memcpy (&Work->AKalkHndw.a_kalkhndw, &a_kalkhndw_null, sizeof (A_KALKHNDW));
						Work->AKalkHndw.a_kalkhndw.mdn = 0;   // ToDo  alle Mandanten aus tab mdn
						Work->AKalkHndw.a_kalkhndw.a = A_mat->a_mat.a;
						Work->AKalkHndw.dbreadfirst();
	
					Work->AKalkHndw.a_kalkhndw.pr_ek1 = Zerm->zerm.mat_o_b;
					Work->AKalkHndw.a_kalkhndw.sk_teilk = Zerm->zerm.mat_o_b;
					Work->AKalkHndw.a_kalkhndw.sk_vollk = Zerm->zerm.mat_o_b;
						if (Zerm->zerm.hk_vollk > (double) 0)
						{
							Work->AKalkHndw.a_kalkhndw.sk_vollk = Zerm->zerm.hk_vollk;
						}
						Zerm->ToDbDate (Date, &Work->AKalkHndw.a_kalkhndw.dat);
						Zerm->ToDbDate (Date, &Work->AKalkHndw.a_kalkhndw.aend_dat);
						Work->AKalkHndw.dbupdate();
					}
				}
				if (Schnitt->cfg_PreiseAus_a_kalkpreis == 1)
				{
					//===== a_kalkpreis ===========
					memcpy (&Work->AKalkPreis.a_kalkpreis, &a_kalkpreis_null, sizeof (A_KALKPREIS));
					Work->AKalkPreis.a_kalkpreis.mdn = Zerm->zerm.mdn;
					Work->AKalkPreis.a_kalkpreis.a = A_mat->a_mat.a;
					Work->AKalkPreis.dbreadfirst();

					Work->AKalkPreis.a_kalkpreis.mat_o_b = Zerm->zerm.mat_o_b;
					Work->AKalkPreis.a_kalkpreis.hk_teilk = Zerm->zerm.mat_o_b;
					Work->AKalkPreis.a_kalkpreis.hk_vollk = Zerm->zerm.mat_o_b;
					if (Zerm->zerm.hk_vollk > (double) 0)
					{
						Work->AKalkPreis.a_kalkpreis.hk_vollk = Zerm->zerm.hk_vollk;
					}
					Zerm->ToDbDate (Date, &Work->AKalkPreis.a_kalkpreis.dat);
					strcpy_s(Work->AKalkPreis.a_kalkpreis.programm,"Zerlegung");
					strcpy_s(Work->AKalkPreis.a_kalkpreis.version,"29.04.2010");
					Work->AKalkPreis.dbupdate();
					if (Schnitt->cfg_Multimandant == 1 && Zerm->zerm.mdn != 0)  //14.12.09 dann auch in Mandant 0 speichern  
					{
						memcpy (&Work->AKalkPreis.a_kalkpreis, &a_kalkpreis_null, sizeof (A_KALKPREIS));
						Work->AKalkPreis.a_kalkpreis.mdn = 0;   // ToDo  alle Mandanten aus tab mdn
						Work->AKalkPreis.a_kalkpreis.a = A_mat->a_mat.a;
						Work->AKalkPreis.dbreadfirst();
	
						Work->AKalkPreis.a_kalkpreis.mat_o_b = Zerm->zerm.mat_o_b;
						Work->AKalkPreis.a_kalkpreis.hk_teilk = Zerm->zerm.mat_o_b;
						Work->AKalkPreis.a_kalkpreis.hk_vollk = Zerm->zerm.mat_o_b;
						if (Zerm->zerm.hk_vollk > (double) 0)
						{
							Work->AKalkPreis.a_kalkpreis.hk_vollk = Zerm->zerm.hk_vollk;
						}
						Zerm->ToDbDate (Date, &Work->AKalkPreis.a_kalkpreis.dat);
						strcpy_s(Work->AKalkPreis.a_kalkpreis.programm,"Zerlegung");
						strcpy_s(Work->AKalkPreis.a_kalkpreis.version,"14.12.2009");
						Work->AKalkPreis.dbupdate();
					}

				}
				if (Zerm->zerm.a_typ == Zerm->TYP_MATERIAL)
				{
					//===== a_kalk_mat ===========
					memcpy (&Work->AKalkMat.a_kalk_mat, &a_kalk_mat_null, sizeof (A_KALK_MAT));
					Work->AKalkMat.a_kalk_mat.mdn = Zerm->zerm.mdn;
					Work->AKalkMat.a_kalk_mat.a = A_mat->a_mat.a;
					Work->AKalkMat.dbreadfirst();
		
					Work->AKalkMat.a_kalk_mat.mat_o_b = Zerm->zerm.mat_o_b;
					Work->AKalkMat.a_kalk_mat.hk_teilk = Zerm->zerm.mat_o_b;
					Work->AKalkMat.a_kalk_mat.hk_vollk = Zerm->zerm.mat_o_b;
					Work->AKalkMat.a_kalk_mat.kost = 0.0;
					if (Zerm->zerm.hk_vollk > (double) 0)
					{
						Work->AKalkMat.a_kalk_mat.hk_vollk = Zerm->zerm.hk_vollk;
						Work->AKalkMat.a_kalk_mat.kost = Zerm->zerm.hk_vollk - Zerm->zerm.mat_o_b;
					}
					Zerm->ToDbDate (Date, &Work->AKalkMat.a_kalk_mat.dat);
					Work->AKalkMat.dbupdate();
					if (Schnitt->cfg_Multimandant == 1 && Zerm->zerm.mdn != 0)  //14.12.09 dann auch in Mandant 0 speichern  
					{
						memcpy (&Work->AKalkMat.a_kalk_mat, &a_kalk_mat_null, sizeof (A_KALK_MAT));
						Work->AKalkMat.a_kalk_mat.mdn = 0; 
						Work->AKalkMat.a_kalk_mat.a = A_mat->a_mat.a;
						Work->AKalkMat.dbreadfirst();
		
						Work->AKalkMat.a_kalk_mat.mat_o_b = Zerm->zerm.mat_o_b;
						Work->AKalkMat.a_kalk_mat.hk_teilk = Zerm->zerm.mat_o_b;
						Work->AKalkMat.a_kalk_mat.hk_vollk = Zerm->zerm.mat_o_b;
						Work->AKalkMat.a_kalk_mat.kost = 0.0;
						if (Zerm->zerm.hk_vollk > (double) 0)
						{
							Work->AKalkMat.a_kalk_mat.hk_vollk = Zerm->zerm.hk_vollk;
							Work->AKalkMat.a_kalk_mat.kost = Zerm->zerm.hk_vollk - Zerm->zerm.mat_o_b;
						}
						Zerm->ToDbDate (Date, &Work->AKalkMat.a_kalk_mat.dat);
						Work->AKalkMat.dbupdate();
					}


					//===== a_varb ===========
					memcpy (&Work->AVarb.a_varb, &a_varb_null, sizeof (A_VARB));
					Work->AVarb.a_varb.a = A_mat->a_mat.a;
					Work->AVarb.dbreadfirst();
					Work->AVarb.a_varb.mat = Zerm->zerm.zerm_mat;
					Work->AVarb.a_varb.fwp = Zerm->zerm.fwp;
					Work->AVarb.dbupdate();
				}
				if (Zerm->zerm.a_typ == Zerm->TYP_VKARTIKEL)
				{
					//===== a_kalkhndw ===========
					memcpy (&Work->AKalkHndw.a_kalkhndw, &a_kalkhndw_null, sizeof (A_KALKHNDW));
					Work->AKalkHndw.a_kalkhndw.mdn = Zerm->zerm.mdn;
					Work->AKalkHndw.a_kalkhndw.fil = 0;
					Work->AKalkHndw.a_kalkhndw.a = Zerm->zerm.zerm_mat;
					Work->AKalkHndw.dbreadfirst();
					Work->AKalkHndw.a_kalkhndw.sk_vollk = Zerm->zerm.pr_kosten;
					Work->AKalkHndw.a_kalkhndw.sk_teilk = Zerm->zerm.pr_kosten;
					Work->AKalkHndw.a_kalkhndw.fil_ek_vollk = Zerm->zerm.pr_vk_netto;
					Work->AKalkHndw.a_kalkhndw.fil_ek_teilk = Zerm->zerm.pr_vk_netto;
					Work->AKalkHndw.a_kalkhndw.lad_vk_vollk = Zerm->zerm.pr_vk_eh;
					Work->AKalkHndw.a_kalkhndw.lad_vk_teilk = Zerm->zerm.pr_vk_eh;
					Zerm->ToDbDate (Date, &Work->AKalkHndw.a_kalkhndw.aend_dat);
					Zerm->ToDbDate (Date, &Work->AKalkHndw.a_kalkhndw.dat);
					Work->AKalkHndw.dbupdate();
				}
			  }
			}

		}
	}
	Work->UpdateZermList(Schnitt->schnitt.mdn,Schnitt->schnitt.schnitt);
	Work->commitwork();
return TRUE;
}

BOOL CMaTreeCtrl::PruefeZerm ()
{
	CString ErrorText;
	if (Zerm->zerm.fwp > 999.99 || Zerm->zerm.fwp < -999.99)//5,2
	{
		ErrorText.Format("Speicherung zur�ckgewiesen :\n\nFWP %.2f bei Material %d",Zerm->zerm.fwp,Zerm->zerm.zerm_mat);
			MessageBox (ErrorText.GetBuffer(), 
		        NULL, MB_ICONERROR);
		return FALSE;
	}
	if (Zerm->zerm.zerm_gew > 99999.999 || Zerm->zerm.zerm_gew < -9999.999) //8,3
	{
		ErrorText.Format("Speicherung zur�ckgewiesen :\n\nGewicht %.3f bei Material %d",Zerm->zerm.zerm_gew,Zerm->zerm.zerm_mat);
			MessageBox (ErrorText.GetBuffer(), 
		        NULL, MB_ICONERROR);
		return FALSE;
	}
	if (Zerm->zerm.zerm_gew_ant > 999.99 || Zerm->zerm.zerm_gew_ant < -99.99) //5,2
	{
		ErrorText.Format("Speicherung zur�ckgewiesen :\n\nGewichtsanteil %.3f bei Material %d",Zerm->zerm.zerm_gew_ant,Zerm->zerm.zerm_mat);
			MessageBox (ErrorText.GetBuffer(), 
		        NULL, MB_ICONERROR);
		return FALSE;
	}
	if (Zerm->zerm.mat_o_b > 9999.999 || Zerm->zerm.mat_o_b < -999.999) //7,3
	{
		ErrorText.Format("Speicherung zur�ckgewiesen :\n\nPreis(ohneKosten) %.3f bei Material %d",Zerm->zerm.mat_o_b,Zerm->zerm.zerm_mat);
			MessageBox (ErrorText.GetBuffer(), 
		        NULL, MB_ICONERROR);
		return FALSE;
	}
	if (Zerm->zerm.hk_vollk > 9999.999 || Zerm->zerm.hk_vollk < -999.999) //7,3
	{
		ErrorText.Format("Speicherung zur�ckgewiesen :\n\nPreis(hk_vollk) %.3f bei Material %d",Zerm->zerm.hk_vollk,Zerm->zerm.zerm_mat);
			MessageBox (ErrorText.GetBuffer(), 
		        NULL, MB_ICONERROR);
		return FALSE;
	}
	if (Zerm->zerm.hk_teilk > 9999.999 || Zerm->zerm.hk_teilk < -999.999) //7,3
	{
		ErrorText.Format("Speicherung zur�ckgewiesen :\n\nPreis(hk_teilk) %.3f bei Material %d",Zerm->zerm.hk_teilk,Zerm->zerm.zerm_mat);
			MessageBox (ErrorText.GetBuffer(), 
		        NULL, MB_ICONERROR);
		return FALSE;
	}
	if (Zerm->zerm.vollkosten_abs > 9999.999 || Zerm->zerm.vollkosten_abs < -999.999) //7,3
	{
		ErrorText.Format("Speicherung zur�ckgewiesen :\n\nPreis(vollkosten_abs) %.3f bei Material %d",Zerm->zerm.vollkosten_abs,Zerm->zerm.zerm_mat);
			MessageBox (ErrorText.GetBuffer(), 
		        NULL, MB_ICONERROR);
		return FALSE;
	}
	if (Zerm->zerm.vollkosten_proz > 99.999 || Zerm->zerm.vollkosten_proz < -99.999) //5,3
	{
		ErrorText.Format("Speicherung zur�ckgewiesen :\n\nPreis(vollkosten_proz) %.3f bei Material %d",Zerm->zerm.vollkosten_proz,Zerm->zerm.zerm_mat);
			MessageBox (ErrorText.GetBuffer(), 
		        NULL, MB_ICONERROR);
		return FALSE;
	}
	if (Zerm->zerm.teilkosten_abs > 9999.999 || Zerm->zerm.teilkosten_abs < -999.999) //7,3
	{
		ErrorText.Format("Speicherung zur�ckgewiesen :\n\nPreis(teilkosten_abs) %.3f bei Material %d",Zerm->zerm.teilkosten_abs,Zerm->zerm.zerm_mat);
			MessageBox (ErrorText.GetBuffer(), 
		        NULL, MB_ICONERROR);
		return FALSE;
	}
	if (Zerm->zerm.teilkosten_proz > 99.999 || Zerm->zerm.teilkosten_proz < -99.999) //5,3
	{
		ErrorText.Format("Speicherung zur�ckgewiesen :\n\nPreis(teilkosten_proz) %.3f bei Material %d",Zerm->zerm.teilkosten_proz,Zerm->zerm.zerm_mat);
			MessageBox (ErrorText.GetBuffer(), 
		        NULL, MB_ICONERROR);
		return FALSE;
	}
	if (Zerm->zerm.dm_100 > 999999.999 || Zerm->zerm.dm_100 < -99999.999) //9,3
	{
		ErrorText.Format("Speicherung zur�ckgewiesen :\n\nWert/100kg %.3f bei Material %d",Zerm->zerm.dm_100,Zerm->zerm.zerm_mat);
			MessageBox (ErrorText.GetBuffer(), 
		        NULL, MB_ICONERROR);
		return FALSE;
	}
	if (Zerm->zerm.vk_pr > 99999.999 || Zerm->zerm.vk_pr < -9999.999) //8,3
	{
		ErrorText.Format("Speicherung zur�ckgewiesen :\n\nVK Wert %.3f bei Material %d",Zerm->zerm.vk_pr,Zerm->zerm.zerm_mat);
			MessageBox (ErrorText.GetBuffer(), 
		        NULL, MB_ICONERROR);
		return FALSE;
	}
	if (Zerm->zerm.kosten_zutat > 9999.999 || Zerm->zerm.kosten_zutat < -999.999) //7,3
	{
		ErrorText.Format("Speicherung zur�ckgewiesen :\n\nkosten_zutat %.3f bei Material %d",Zerm->zerm.kosten_zutat,Zerm->zerm.zerm_mat);
			MessageBox (ErrorText.GetBuffer(), 
		        NULL, MB_ICONERROR);
		return FALSE;
	}
	if (Zerm->zerm.kosten_vpk > 9999.999 || Zerm->zerm.kosten_vpk < -999.999) //7,3
	{
		ErrorText.Format("Speicherung zur�ckgewiesen :\n\nkosten_vpk %.3f bei Material %d",Zerm->zerm.kosten_vpk,Zerm->zerm.zerm_mat);
			MessageBox (ErrorText.GetBuffer(), 
		        NULL, MB_ICONERROR);
		return FALSE;
	}
	if (Zerm->zerm.kosten > 999999.999 || Zerm->zerm.kosten < -99999.999) //9,3
	{
		ErrorText.Format("Speicherung zur�ckgewiesen :\n\nkosten %.3f bei Material %d",Zerm->zerm.kosten,Zerm->zerm.zerm_mat);
			MessageBox (ErrorText.GetBuffer(), 
		        NULL, MB_ICONERROR);
		return FALSE;
	}
	if (Zerm->zerm.pr_zutat > 99999.999 || Zerm->zerm.pr_zutat < -9999.999) //9,4
	{
		ErrorText.Format("Speicherung zur�ckgewiesen :\n\npr_zutat %.3f bei Material %d",Zerm->zerm.pr_zutat,Zerm->zerm.zerm_mat);
			MessageBox (ErrorText.GetBuffer(), 
		        NULL, MB_ICONERROR);
		return FALSE;
	}
	if (Zerm->zerm.pr_vpk > 99999.999 || Zerm->zerm.pr_vpk < -9999.999) //9,4
	{
		ErrorText.Format("Speicherung zur�ckgewiesen :\n\npr_vpk %.3f bei Material %d",Zerm->zerm.pr_vpk,Zerm->zerm.zerm_mat);
			MessageBox (ErrorText.GetBuffer(), 
		        NULL, MB_ICONERROR);
		return FALSE;
	}
	if (Zerm->zerm.pr_kosten > 99999.999 || Zerm->zerm.pr_kosten < -9999.999) //9,4
	{
		ErrorText.Format("Speicherung zur�ckgewiesen :\n\npr_kosten %.3f bei Material %d",Zerm->zerm.pr_kosten,Zerm->zerm.zerm_mat);
			MessageBox (ErrorText.GetBuffer(), 
		        NULL, MB_ICONERROR);
		return FALSE;
	}
	if (Zerm->zerm.pr_vk_netto > 99999.999 || Zerm->zerm.pr_vk_netto < -9999.999) //9,4
	{
		ErrorText.Format("Speicherung zur�ckgewiesen :\n\nVk_netto %.3f bei Material %d",Zerm->zerm.pr_vk_netto,Zerm->zerm.zerm_mat);
			MessageBox (ErrorText.GetBuffer(), 
		        NULL, MB_ICONERROR);
		return FALSE;
	}
	if (Zerm->zerm.kosten_fix > 99999.999 || Zerm->zerm.kosten_fix < -9999.999) //8,3
	{
		ErrorText.Format("Speicherung zur�ckgewiesen :\n\nkosten_fix %.3f bei Material %d",Zerm->zerm.kosten_fix,Zerm->zerm.zerm_mat);
			MessageBox (ErrorText.GetBuffer(), 
		        NULL, MB_ICONERROR);
		return FALSE;
	}
	if (Zerm->zerm.pr_vk_eh > 99999.999 || Zerm->zerm.pr_vk_eh < -9999.999) //8,3
	{
		ErrorText.Format("Speicherung zur�ckgewiesen :\n\nVK Einzelh.(pr_vk_eh) %.3f bei Material %d",Zerm->zerm.pr_vk_eh,Zerm->zerm.zerm_mat);
			MessageBox (ErrorText.GetBuffer(), 
		        NULL, MB_ICONERROR);
		return FALSE;
	}
	if (Zerm->zerm.kosten_gh > 999999.999 || Zerm->zerm.kosten_gh < -99999.999) //9,3
	{
		ErrorText.Format("Speicherung zur�ckgewiesen :\n\nkosten_gh %.3f bei Material %d",Zerm->zerm.kosten_gh,Zerm->zerm.zerm_mat);
			MessageBox (ErrorText.GetBuffer(), 
		        NULL, MB_ICONERROR);
		return FALSE;
	}
	if (Zerm->zerm.kosten_eh > 999999.999 || Zerm->zerm.kosten_eh < -99999.999) //9,3
	{
		ErrorText.Format("Speicherung zur�ckgewiesen :\n\nkosten_eh %.3f bei Material %d",Zerm->zerm.kosten_eh,Zerm->zerm.zerm_mat);
			MessageBox (ErrorText.GetBuffer(), 
		        NULL, MB_ICONERROR);
		return FALSE;
	}
	return TRUE;
}

void CMaTreeCtrl::AddChild ()
{
	HTREEITEM ChildItem;
	int iImage = 0;
	CString Text;
	CString CTyp, CGew, CGewAnt,CWertKg,CWert100Kg,CFwp,CFix,CA_kz,CZeitfaktor,CWert;

	for (std::vector<CZerlTreeData *>::iterator e = ZerlTreeData.begin (); e != ZerlTreeData.end (); ++e)
	{
		CZerlTreeData *ete = *e;
		if (ete->zerm_mat == 0) continue;
		if (Zerm->zerm.zerm_aus_mat == ete->zerm_mat && Zerm->zerm.zerm_aus_a_typ == ete->a_typ)
		{

			if (Zerm->zerm.chk_gew) CGew.Format (_T(" (%.2lf kg)"),Zerm->zerm.zerm_gew);
			if (Zerm->zerm.chk_gew_ant) CGewAnt.Format (_T("   (%.2lf %s)"),Zerm->zerm.zerm_gew_ant,"%");
			if (Zerm->zerm.chk_mat_o_b) CWertKg.Format (_T("   (%.2lf �)"),Zerm->zerm.mat_o_b);
			if (Zerm->zerm.chk_wert_100kg) CWert100Kg.Format (_T("   (%.2lf �)"),Zerm->zerm.dm_100);
			if (Zerm->zerm.chk_wert) CWert.Format (_T("   (%.2lf �)"),Zerm->zerm.teilkosten_abs); //190111
			if (Zerm->zerm.chk_kennzeichen) CWert100Kg.Format (_T("   (%s)"),Zerm->zerm.a_kz);
			if (Zerm->zerm.chk_zeitfaktor) CZeitfaktor.Format (_T("   (%ld)"),Zerm->zerm.a_z_f);
			if (Zerm->zerm.chk_fwp) CFwp.Format (_T("   (%.0lf)"),Zerm->zerm.fwp);
			CTyp = ATyp2TypKennzeichen (Zerm->zerm.a_typ);
			Text.Format (_T("%s %d %s%s%s%s%s%s%s%s"),CTyp.GetBuffer(), Zerm->zerm.zerm_mat, Zerm->zerm.zerm_mat_bz1,
									CGew.GetBuffer(),
									CGewAnt.GetBuffer(),
									CWertKg.GetBuffer(),
									CWert.GetBuffer(),
									CWert100Kg.GetBuffer(),
									CFwp.GetBuffer(),
									CZeitfaktor.GetBuffer());
			CA_kz.Format (_T("%s"),Zerm->zerm.a_kz);
			iImage = 0;
			ChildItem = AddChildItem (ete->Item,  Text.GetBuffer (), 
	                          0, 1, 
	 					      iImage, TVIS_STATEIMAGEMASK | TVIS_EXPANDED );
			CFix.Format (_T("%s"),Zerm->zerm.fix);
			SetCheck(ChildItem, CFix == "V"); 
			HTREEITEM selectedItem = ChildItem;
			CZerlTreeData *abl = new CZerlTreeData (ChildItem,
											Schnitt->schnitt.mdn,
											Schnitt->schnitt.schnitt,
											Zerm->zerm.zerm_mat,
											Zerm->zerm.zerm_aus_mat,
											Zerm->zerm.zerm_aus_a_typ,
			                                Zerm->zerm.zerm_gew ,
			                                Zerm->zerm.zerm_gew_ant,
			                                Zerm->zerm.mat_o_b,
			                                Zerm->zerm.dm_100,
											Zerm->zerm.fwp,
											Zerm->zerm.fix,

											Zerm->zerm.a_kz,
											Zerm->zerm.zerm_teil,
											Zerm->zerm.hk_vollk,
											Zerm->zerm.hk_teilk,
											Zerm->zerm.delstatus,
											Zerm->zerm.vollkosten_abs,
											Zerm->zerm.vollkosten_proz,
											Zerm->zerm.teilkosten_abs,
											Zerm->zerm.teilkosten_proz,
											Zerm->zerm.a_z_f ,
											Zerm->zerm.vk_pr ,
											Zerm->zerm.kosten_zutat,
											Zerm->zerm.kosten_vpk ,
											Zerm->zerm.a_typ ,
										    Zerm->zerm.kosten,
											Zerm->zerm.pr_zutat,
											Zerm->zerm.pr_vpk,
										    Zerm->zerm.pr_kosten,
											Zerm->zerm.pr_vk_netto,
											Zerm->zerm.kosten_fix,
											Zerm->zerm.pr_vk_eh,

											Zerm->zerm.kost_bz,
											Zerm->zerm.zerm_mat_bz2,
											(double) 0,  //zerm_gew_ant_parent
											Zerm->zerm.kosten_gh,
											Zerm->zerm.kosten_eh,
											Zerm->zerm.fwp,
											Zerm->zerm.sk_vollk,
											Zerm->zerm.sk_teilk,
											Zerm->zerm.fil_ek_vollk,
											Zerm->zerm.fil_ek_teilk,
											Zerm->zerm.fil_vk_vollk,
											Zerm->zerm.fil_vk_teilk,
											Zerm->zerm.gh1_vollk,
											Zerm->zerm.gh1_teilk,
											Zerm->zerm.gh2_vollk,
											Zerm->zerm.gh2_teilk

											);
			ZerlTreeData.push_back (abl);
		    UpdateZermMatV();
			return;
		}


	}

}

void CMaTreeCtrl::NextItem()
{
	HTREEITEM hItem = NULL;
	HTREEITEM hItemOK = NULL;
	hItem = GetSelectedItem ();
	if (hItem == NULL) return ;
	hItemOK = hItem;
	ItemToZerm(hItem);
	ZerlTreeDataToZerm(1);
	short AktA_Typ = Zerm->zerm.a_typ;

	hItem = GetNextVisibleItem(hItem);
	if (IsZerlVerl(hItem)) hItem = GetNextVisibleItem(hItem);
	if (hItem == NULL) return ;
	ItemToZerm(hItem);
	ZerlTreeDataToZerm(1);
	if (AktA_Typ == Zerm->zerm.a_typ) hItemOK = hItem;
	while (AktA_Typ != Zerm->zerm.a_typ)
	{
		hItem = GetNextVisibleItem(hItem);
		if (IsZerlVerl(hItem)) hItem = GetNextVisibleItem(hItem);
		if (hItem == NULL)
		{
			break ;
		}
		ItemToZerm(hItem);
		ZerlTreeDataToZerm(1);
		if (AktA_Typ == Zerm->zerm.a_typ) hItemOK = hItem;
	}
	ItemToZerm(hItemOK);
	ZerlTreeDataToZerm(1);
	SelectItem(hItemOK);
	EnsureVisible(hItemOK);
}
void CMaTreeCtrl::PrevItem()
{
	HTREEITEM hItem = NULL;
	HTREEITEM hItemOK = NULL;
	hItem = GetSelectedItem ();
	if (hItem == NULL) return ;
	hItemOK = hItem;
	ItemToZerm(hItem);
	ZerlTreeDataToZerm(1);
	short AktA_Typ = Zerm->zerm.a_typ;

	hItem = GetPrevVisibleItem(hItem);
	if (IsZerlVerl(hItem)) hItem = GetPrevVisibleItem(hItem);
	if (hItem == NULL) return ;
	ItemToZerm(hItem);
	if (Zerm->zerm.zerm_mat == Zerm->zerm.zerm_aus_mat)
	{
		ItemToZerm(hItemOK);
		return;
	}
	ZerlTreeDataToZerm(1);
	if (AktA_Typ == Zerm->zerm.a_typ) hItemOK = hItem;
	while (AktA_Typ != Zerm->zerm.a_typ)
	{
		hItem = GetPrevVisibleItem(hItem);
		ItemToZerm(hItem);
		if (Zerm->zerm.zerm_mat == Zerm->zerm.zerm_aus_mat) break;
		if (IsZerlVerl(hItem)) hItem = GetPrevVisibleItem(hItem);
		if (hItem == NULL) break ;
		ItemToZerm(hItem);
		ItemToZerm(hItem);
		ZerlTreeDataToZerm(1);
		if (Zerm->zerm.zerm_mat == Zerm->zerm.zerm_aus_mat) break;
		if (AktA_Typ == Zerm->zerm.a_typ) hItemOK = hItem;
	}
	ItemToZerm(hItemOK);
	ZerlTreeDataToZerm(1);
	SelectItem(hItemOK);
	EnsureVisible(hItemOK);
}

void CMaTreeCtrl::OnNMDblclkZtree(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	//Hier geht er nicht rein warum ????
	*pResult = 0;
}

BOOL CMaTreeCtrl::UpdateZermMatV ()
{
		return    Work->UpdateZermMatV(Schnitt->schnitt.mdn,
								   Schnitt->schnitt.schnitt,
								   Zerm->zerm.zerm_aus_mat,
								   Zerm->zerm.zerm_mat,
								   Zerm->zerm.fwp,
								   Zerm->zerm.zerm_gew,
								   Zerm->zerm.zerm_gew_ant,
								   Zerm->zerm.mat_o_b,
								   Zerm->zerm.hk_vollk,
								   Zerm->zerm.hk_teilk,
								   Zerm->zerm.sk_vollk,
								   Zerm->zerm.sk_teilk,
								   Zerm->zerm.fil_ek_vollk,
								   Zerm->zerm.fil_ek_teilk,
								   Zerm->zerm.fil_vk_vollk,
								   Zerm->zerm.fil_vk_teilk,
								   Zerm->zerm.gh1_vollk,
								   Zerm->zerm.gh1_teilk,
								   Zerm->zerm.gh2_vollk,
								   Zerm->zerm.gh2_teilk,
								   Zerm->zerm.dm_100,
								   Zerm->zerm.kosten_zutat,
								   Zerm->zerm.kosten_vpk,
								   Zerm->zerm.fix,
								   Zerm->zerm.a_typ,
								   Zerm->zerm.teilkosten_abs,
								   Zerm->zerm.vk_pr,
								   Zerm->zerm.zerm_teil,
								   Zerm->zerm.zerm_mat, // in kost_mat
								   Zerm->zerm.kost_bz, // in kost_mat_bz
								   Zerm->zerm.pr_vk_eh
								   );
}
short CMaTreeCtrl::ItemToZermTeil (HTREEITEM ht)
{
	long mat = Zerm->zerm.zerm_mat;
	long aus_mat = Zerm->zerm.zerm_aus_mat;
	short a_typ = Zerm->zerm.a_typ;
	short aus_a_typ = Zerm->zerm.zerm_aus_a_typ;
	CString Cfix = Zerm->zerm.fix;
	ItemToZerm(ht);
	for (std::vector<CZerlTreeData *>::iterator e = ZerlTreeData.begin (); e != ZerlTreeData.end (); ++e)
	{
		CZerlTreeData *ete = *e;
		if (ete->zerm_mat == 0) continue;
		if (Zerm->zerm.zerm_mat == ete->zerm_mat && 
			Zerm->zerm.zerm_aus_mat == ete->zerm_aus_mat &&
			Zerm->zerm.a_typ == ete->a_typ)
		{
			Zerm->zerm.zerm_mat = mat;
			Zerm->zerm.zerm_aus_mat = aus_mat;
			Zerm->zerm.a_typ = a_typ;
			Zerm->zerm.zerm_aus_a_typ = aus_a_typ;
			return _tstoi (ete->zerm_teil.GetBuffer ());
		}
	}
	Zerm->zerm.zerm_mat = mat;
	Zerm->zerm.zerm_aus_mat = aus_mat;
	Zerm->zerm.a_typ = a_typ;
	Zerm->zerm.zerm_aus_a_typ = aus_a_typ;
	_tcscpy_s (Zerm->zerm.fix, Cfix.GetBuffer());
	return 0;
}

HTREEITEM CMaTreeCtrl::ZerlegtAus (HTREEITEM hItem)
{
	//GetChildItem reicht nicht mehr, da auch nicht Zerlegtes 
	//Childs haben kann (Zutaten und Kosten)
	// Diese Methode gibt nur ein Item zur�ck, wenn dieses ein Material ist 
//	return GetChildItem (hItem)  
	long mat = Zerm->zerm.zerm_mat;
	long aus_mat = Zerm->zerm.zerm_aus_mat;
	short a_typ = Zerm->zerm.a_typ;
	short aus_a_typ = Zerm->zerm.zerm_aus_a_typ;
	CString Cfix = Zerm->zerm.fix;
	CString Variabel = Zerm->zerm.variabel;
	HTREEITEM hChildItem = NULL;
	hChildItem = GetChildItem (hItem);  
	if (hChildItem == NULL) return NULL;
	ItemTo_k_ausZerlTreeData (hChildItem);
	if (k.a_typ == Zerm->TYP_MATERIAL) // Es reicht, das erste Child zu pr�fen, da order by �ber a_typ
//	if (ItemToZermTeil(hItem) == 1)   // zerlegtes Grobteil 
	{
		Zerm->zerm.zerm_mat = mat;
		Zerm->zerm.zerm_aus_mat = aus_mat;
		Zerm->zerm.a_typ = a_typ;
		Zerm->zerm.zerm_aus_a_typ = aus_a_typ;
		_tcscpy_s (Zerm->zerm.variabel, Variabel.GetBuffer ());
		_tcscpy_s (Zerm->zerm.fix, Cfix.GetBuffer());
		return hChildItem;
	}
	Zerm->zerm.zerm_mat = mat;
	Zerm->zerm.zerm_aus_mat = aus_mat;
	Zerm->zerm.a_typ = a_typ;
	Zerm->zerm.zerm_aus_a_typ = aus_a_typ;
	_tcscpy_s (Zerm->zerm.variabel, Variabel.GetBuffer ());
	_tcscpy_s (Zerm->zerm.fix, Cfix.GetBuffer());
	return NULL;
}

CString CMaTreeCtrl::BestimmeZerm_teil (HTREEITEM hItem)
{
	long mat = Zerm->zerm.zerm_mat;
	long aus_mat = Zerm->zerm.zerm_aus_mat;
	short a_typ = Zerm->zerm.a_typ;
	short aus_a_typ = Zerm->zerm.zerm_aus_a_typ;
	CString Cfix = Zerm->zerm.fix;
	CString Variabel = Zerm->zerm.variabel;
	HTREEITEM hChildItem = NULL;
	hChildItem = GetChildItem (hItem);  
	if (hChildItem == NULL) return "0";
	ItemTo_k_ausZerlTreeData (hChildItem);

	Zerm->zerm.zerm_mat = mat;
	Zerm->zerm.zerm_aus_mat = aus_mat;
	Zerm->zerm.a_typ = a_typ;
	Zerm->zerm.zerm_aus_a_typ = aus_a_typ;
    _tcscpy_s (Zerm->zerm.variabel, Variabel.GetBuffer ());
	_tcscpy_s (Zerm->zerm.fix, Cfix.GetBuffer());

	// Es reicht, das erste Child zu pr�fen, da order by �ber a_typ
	if (k.a_typ == Zerm->TYP_MATERIAL) 
	{
		return "1";  //Child ist ein anderes material , also zerlegt zerm_teil des GT = 1 
	}
	if (k.a_typ != Zerm->TYP_MATERIAL) 
	{
		return "2";  //Child ist Zutat, Kosten..  , also nicht zerlegt zerm_teil des GT = 2 
	}
	return "0";  //nicht def.
}

CString CMaTreeCtrl::ATyp2TypKennzeichen (short a_typ)
{
	CString CTyp;
		if (a_typ == Zerm->TYP_MATERIAL) CTyp.Format (_T(""));
		if (a_typ == Zerm->TYP_ZUTAT) CTyp.Format (_T("Z"));
		if (a_typ == Zerm->TYP_VERPACKUNG) CTyp.Format (_T("P"));
		if (a_typ == Zerm->TYP_KOSTEN) CTyp.Format (_T("K"));
		if (a_typ == Zerm->TYP_AUFSCHLAG) CTyp.Format (_T("A-GH"));
		if (a_typ == Zerm->TYP_AUFSCHLAG_EH) CTyp.Format (_T("A-EH"));
		if (a_typ == Zerm->TYP_VKARTIKEL) CTyp.Format (_T("Verkauf:"));
		return CTyp;
}
short CMaTreeCtrl::TypKennzeichen2ATyp (CString CTyp)
{
	short a_typ;
	a_typ = Zerm->TYP_MATERIAL;
		if (CTyp == "Z") a_typ = Zerm->TYP_ZUTAT;
		if (CTyp == "P") a_typ = Zerm->TYP_VERPACKUNG;
		if (CTyp == "K") a_typ = Zerm->TYP_KOSTEN;
		if (CTyp == "A-GH") a_typ = Zerm->TYP_AUFSCHLAG;
		if (CTyp == "A-EH") a_typ = Zerm->TYP_AUFSCHLAG_EH;
		if (CTyp == "Verkauf:") a_typ = Zerm->TYP_VKARTIKEL;
		return a_typ;
}
CString CMaTreeCtrl::HoleBezeichnung ()
{
	CString CBez;
		if (Zerm->zerm.a_typ == Zerm->TYP_KOSTEN || Zerm->zerm.a_typ == Zerm->TYP_AUFSCHLAG
												 || Zerm->zerm.a_typ == Zerm->TYP_AUFSCHLAG_EH)
		{
			_tcscpy_s (Zerm->zerm.zerm_mat_bz1, Zerm->zerm.kost_bz);
		}
		else
		{
			if (Zerm->zerm.a_typ == Zerm->TYP_MATERIAL || Zerm->zerm.a_typ == Zerm->TYP_ZUTAT)	
			{
				A_mat->a_mat.mat = Zerm->zerm.zerm_mat;
				if (A_mat->dbreadfirst() == 0)
				{
					A_bas->a_bas.a = A_mat->a_mat.a;
				}
			}
			if (Zerm->zerm.a_typ == Zerm->TYP_VERPACKUNG || Zerm->zerm.a_typ == Zerm->TYP_VKARTIKEL) 
			{
				A_bas->a_bas.a = (long) Zerm->zerm.zerm_mat;
			}
			A_bas->dbreadfirst() ;
			_tcscpy_s (Zerm->zerm.zerm_mat_bz1, A_bas->a_bas.a_bz1);
			_tcscpy_s (Zerm->zerm.kost_bz, A_bas->a_bas.a_bz1);
		}
		CBez = Zerm->zerm.kost_bz;

	return CBez;
}

BOOL CMaTreeCtrl::VkArtikelBer (HTREEITEM hItem,HTREEITEM PItem,HTREEITEM CItem)
{
	long mat = Zerm->zerm.zerm_mat;
	long aus_mat = Zerm->zerm.zerm_aus_mat;
	short a_typ = Zerm->zerm.a_typ;
	short aus_a_typ = Zerm->zerm.zerm_aus_a_typ;

	CString Cfix = Zerm->zerm.fix;
	if (PItem == NULL) return FALSE;
	if (Zerm->zerm.zerm_mat == Schnitt->schnitt.schnitt_mat) 
	{
		Zerm->zerm.zerm_mat = mat;
		Zerm->zerm.zerm_aus_mat = aus_mat;
		Zerm->zerm.a_typ = a_typ;
		Zerm->zerm.zerm_aus_a_typ = aus_a_typ;
		_tcscpy_s (Zerm->zerm.fix, Cfix.GetBuffer());
		return TRUE ; //Root nicht neu ber!!
	}
	// Werte aus dem Ausgangsmaterial holen (auch Kosten.... wenn diese direkt dem 
	// Ausgangsmaterial zugeordnet sind  (Beim Umlasten schon berechnet (MatKostenBer))
	ItemTo_k_ausZerlTreeData(PItem);
					if (k.zerm_mat == 1132)
					{
						int cccccc = 1;
					}
//211009	double 	Sum_kosten_zutat = k.kosten_zutat;
//211009	double 	Sum_kosten_vpk = k.kosten_vpk;
//211009	double 	Sum_kosten = k.kosten;
	double P_mat_o_b = k.mat_o_b;
	double P_fwp = k.fwp;
	double 	Sum_kosten_zutat = 0; //211009
	double 	Sum_kosten_vpk = 0;//211009
	double 	Sum_kosten = 0;//211009
	CString PFix = k.fix;
// Das Gewicht muss addiert werden von allen gleichen Materialien des Ausgangsmat. 
	ItemToZerm(PItem);
	double Sum_gew = 0.0;
	double Sum_gew_zutat = 0.0;
	double Sum_gew_ant = 0.0;
	double Sum_teilkosten_abs = 0.0;
	double SumDm100 = 0.0;
	double 	Sum_vk_pr = 0.0;
	double 	Sum_kosten_gh = 0.0;
	double 	Sum_kosten_eh = 0.0;
	for (std::vector<CZerlTreeData *>::iterator e = ZerlTreeData.begin (); e != ZerlTreeData.end (); ++e)
	{
		CZerlTreeData *ete = *e;
		if (ete->zerm_mat == 0) continue;
		if (Zerm->zerm.zerm_mat == ete->zerm_mat && 
			Zerm->zerm.a_typ == ete->a_typ)
		{
			Sum_gew += ete->zerm_gew;
			Sum_gew_ant += ete->zerm_gew_ant;
			Sum_teilkosten_abs += ete->teilkosten_abs;
			SumDm100 += ete->dm_100;
			Sum_kosten_gh += ete->kosten_gh;
			Sum_kosten_eh += ete->kosten_eh;
//			Sum_vk_pr += ete->vk_pr;
		}
	}


    //Werte aus den Childs dazupacken , (Kosten, Aufschl�ge, die direkt dem Vk-Artikel zugeordnet sind)
	while (CItem != NULL)
	{
		ItemTo_k_ausZerlTreeData(CItem);
		if (k.a_typ == Zerm->TYP_ZUTAT) Sum_gew_zutat += k.zerm_gew;
		Sum_kosten_zutat += k.kosten_zutat ;
		Sum_kosten_vpk += k.kosten_vpk ;
		Sum_kosten += k.kosten ;

//020510		Sum_kosten_gh += k.kosten_gh;
//020510		Sum_kosten_eh += k.kosten_eh;

		//020510 A
			if (k.a_typ == Zerm->TYP_AUFSCHLAG)
			{
				Sum_kosten_gh += k.mat_o_b ;
			}
			if (k.a_typ == Zerm->TYP_AUFSCHLAG_EH)
			{
				Sum_kosten_eh += k.mat_o_b ;
			}
		//020510 E


		CItem = GetNextItem(CItem, TVGN_NEXT);
	}
	ItemToZerm(hItem);
	Sum_gew_zutat = Sum_gew + Sum_gew_zutat;
	for (std::vector<CZerlTreeData *>::iterator e = ZerlTreeData.begin (); e != ZerlTreeData.end (); ++e)
	{
		CZerlTreeData *ete = *e;
		if (ete->zerm_mat == 0) continue;
		if (Zerm->zerm.zerm_mat == ete->zerm_mat && 
			Zerm->zerm.zerm_aus_mat == ete->zerm_aus_mat &&
			Zerm->zerm.a_typ == ete->a_typ)
		{
			ete->fix = PFix.GetBuffer();  // VK -Artikel dann variabel , wenn Parent auch
			if (!CStrFuncs::IsDoubleEqual (Schnitt->schnitt._wert_fwp, (double) 0))
			{
				if (!CStrFuncs::IsDoubleEqual (Sum_gew, (double) 0))
				{
					ete->zerm_gew = Sum_gew;
					ete->zerm_gew_ant = Sum_gew_ant;
					ete->mat_o_b = P_mat_o_b;
					ete->fwp = P_fwp; //080510
					ete->fwp_org = P_fwp; //080510 
					if (ete->zerm_mat == 1133)
					{
						int ccc = 1;
					}
					ete->teilkosten_abs = Sum_teilkosten_abs;
					ete->kosten_zutat = Sum_kosten_zutat;
					ete->pr_zutat = P_mat_o_b + (Sum_kosten_zutat / Sum_gew_zutat);
//					ete->pr_zutat = (Sum_teilkosten_abs + Sum_kosten_zutat) / Sum_gew_zutat;
					ete->kosten_vpk = Sum_kosten_vpk;
					ete->pr_vpk = ete->pr_zutat + (Sum_kosten_vpk / Sum_gew_zutat);
					ete->kosten = Sum_kosten;
					ete->pr_kosten = ete->pr_vpk + (Sum_kosten / Sum_gew_zutat);
					ete->hk_vollk = ete->pr_kosten; //020510

					ete->kosten_gh = Sum_kosten_gh; //020510
					ete->pr_vk_netto = ete->pr_kosten + (Sum_kosten_gh / Sum_gew_zutat); //020510
					ete->kosten_eh = Sum_kosten_eh; //020510
					ete->pr_vk_eh = ete->pr_kosten + (Sum_kosten_eh / Sum_gew_zutat); //020510


//020510					ete->hk_vollk = Sum_teilkosten_abs + ete->kosten_zutat + ete->kosten_vpk + ete->kosten;
//020510					ete->hk_vollk /= ete->zerm_gew;
					//ZuTun : 1 ersetzten mit ebene (ist noch nicht in ete-> drin 
					if (ete->a_typ == Zerm->TYP_MATERIAL)
					{
						// bei Materialien nur! Kosten aus der 3. Tabseite holen
						//hier sollte man nie reinkommen (ist ja nur vk-artikel mit typ= TYP_VKARTIKEL
						ete->hk_vollk = HoleHKKosten(ete->mat_o_b,ete->a_kz.GetBuffer(),1,ete->a_z_f,ete->zerm_teil.GetBuffer());
					}
					if (ete->a_kz == "ZV") ete->hk_vollk = (double) 0;

					ete->vk_pr = ete->hk_vollk + ete->kosten_gh;
//020510					ete->pr_vk_netto = ete->hk_vollk + ete->kosten_gh;
//020510					ete->pr_vk_eh = ete->hk_vollk + ete->kosten_eh;
					if (ete->a_typ == 5)
					{
						int di = 0;
					}

					double fwp = ete->fwp;
					if (ete->fix == "F") fwp = ete->fwp_org; 
					if (ete->zerm_mat == Zerm->Zerlegeverlust)
					{
						ete->dm_100 = 0.0;
						ete->fwp = 0.0;
						ete->fwp_org = 0.0;
						ete->teilkosten_abs = 0.0;
						ete->mat_o_b = 0.0;
					}
					if (ete->mat_o_b > -9999 && ete->mat_o_b < 99999 && ete->dm_100 > -9999 && ete->dm_100 < 99999)
					{
				    Work->UpdateZermMatV(Schnitt->schnitt.mdn,
								   Schnitt->schnitt.schnitt,
								   ete->zerm_aus_mat,
								   ete->zerm_mat,
								   fwp,
//								   ete->zerm_gew,
								   Sum_gew_zutat,
								   ete->zerm_gew_ant,
								   ete->mat_o_b,
								   ete->hk_vollk,
								   ete->hk_teilk,
								   ete->sk_vollk,
								   ete->sk_teilk,
								   ete->fil_ek_vollk,
								   ete->fil_ek_teilk,
								   ete->fil_vk_vollk,
								   ete->fil_vk_teilk,
								   ete->gh1_vollk,
								   ete->gh1_teilk,
								   ete->gh2_vollk,
								   ete->gh2_teilk,
								   ete->dm_100,
								   ete->kosten_zutat,
								   ete->kosten_vpk,
								   ete->fix.GetBuffer (),
								   ete->a_typ,
								   ete->teilkosten_abs,
								   ete->vk_pr,
								   ete->zerm_teil.GetBuffer(),
								   ete->zerm_mat, // in kost_mat
								   ete->zerm_mat_bz1.GetBuffer(), // in kost_mat_bz
								   ete->pr_vk_eh
								   );
					}
				}
			}
		}
	}
	Zerm->zerm.zerm_mat = mat;
	Zerm->zerm.zerm_aus_mat = aus_mat;
	Zerm->zerm.a_typ = a_typ;
	Zerm->zerm.zerm_aus_a_typ = aus_a_typ;
	_tcscpy_s (Zerm->zerm.fix, Cfix.GetBuffer());
	return TRUE;
}



BOOL CMaTreeCtrl::ZerlegeverlustBmpSetzen ()
{
	HTREEITEM hItem;

	if (Zerm->dbreadfirst_GT() == 0)
	{
		hItem = ZermToItem();
		if (hItem != NULL)
		{
			ZerlVerlBer(hItem);
		}
	} else return FALSE;
	while (Zerm->dbread_GT() == 0)
	{
		hItem = ZermToItem();
		if (hItem != NULL)
		{
			ZerlVerlBer(hItem);
		}
	}
	return TRUE;
}
void CMaTreeCtrl::FWPNeuAufbauen (void)
{
	CString CFix;
	// bei : FWP,mat_o_b, pr100kg, fix (hier �ber alle gleichen mat)
	// bei : gew, gew_ant (nur �ber 1 Item und ZV!)(mat,aus_mat)
	HTREEITEM hItem = NULL;
	HTREEITEM PItem = NULL;
	double mat_gew = (double) 0;
	double mat_gew_ant = (double) 0;
	double mat_hk_vollk = (double) 0;
	for (std::vector<CZerlTreeData *>::iterator e = ZerlTreeData.begin (); e != ZerlTreeData.end (); ++e)
	{
		CZerlTreeData *ete = *e;
		if (ete->zerm_mat == 0) continue;
		if (ete->a_typ != Zerm->TYP_MATERIAL) continue;
		ete->a_kz =  Work->HoleA_Kz(ete->zerm_mat,ete->a_typ,ete->a_kz.GetBuffer());  //hier jetzt immer a_kz aus den Stammdaten holen
		if (ete->zerm_teil == "1") continue;
		if (ete->fix == "V") continue;
		if (Schnitt->schnitt._wert_fwp < 0.00001) 
		{
			ete->fwp = 0.0;
		}
		else
		{
			if (Schnitt->cfg_FWP_berechnen && Schnitt->schnitt.kalk_bas == 1)
			{
				ete->fwp_org =  Work->HoleFWP(ete->zerm_mat,ete->fwp_org);
			}
			else
			{
				ete->fwp_org =  Work->HoleMatoB(Schnitt->schnitt.mdn,ete->zerm_mat,Schnitt->cfg_PreiseAus_a_kalkpreis, Schnitt->schnitt.kalk_bas, Schnitt->PrGrStuf) / Schnitt->schnitt._wert_fwp;
			}
		}
	}

}
void CMaTreeCtrl::FWP_aus_AVarb (void)
{
	CString CFix;
	HTREEITEM hItem = NULL;
	HTREEITEM PItem = NULL;
	double mat_gew = (double) 0;
	double mat_gew_ant = (double) 0;
	double mat_hk_vollk = (double) 0;
	for (std::vector<CZerlTreeData *>::iterator e = ZerlTreeData.begin (); e != ZerlTreeData.end (); ++e)
	{
		CZerlTreeData *ete = *e;

		if (ete->zerm_mat == 0) continue;
		if (ete->a_typ != Zerm->TYP_MATERIAL) continue;
		ete->a_kz =  Work->HoleA_Kz(ete->zerm_mat,ete->a_typ,ete->a_kz.GetBuffer());  //hier jetzt immer a_kz aus den Stammdaten holen
		if (ete->zerm_teil == "1") continue;
//		if (ete->fix == "V") continue;
		if (Schnitt->schnitt._wert_fwp < 0.00001) 
		{
			ete->fwp = 0.0;
		}
		else
		{
			ete->fwp_org =  Work->HoleFWP(ete->zerm_mat,ete->fwp_org);
			ete->fwp = ete->fwp_org;
		}
	}

}
void CMaTreeCtrl::Vk_Laden (void)
{
	CString CFix;
	// bei : FWP,mat_o_b, pr100kg, fix (hier �ber alle gleichen mat)
	// bei : gew, gew_ant (nur �ber 1 Item und ZV!)(mat,aus_mat)
	HTREEITEM hItem = NULL;
	HTREEITEM PItem = NULL;
	double mat_gew = (double) 0;
	double mat_gew_ant = (double) 0;
	double mat_hk_vollk = (double) 0;
	for (std::vector<CZerlTreeData *>::iterator e = ZerlTreeData.begin (); e != ZerlTreeData.end (); ++e)
	{
		CZerlTreeData *ete = *e;
		if (ete->zerm_mat == 0) continue;
		if (ete->a_typ != Zerm->TYP_MATERIAL) continue;
		if (ete->zerm_teil == "1") continue;
		if (ete->fix == "V") continue;
		if (Schnitt->schnitt._wert_fwp < 0.00001) 
		{
			ete->fwp = 0.0;
		}
		else
		{
			ete->fwp_org =  Work->HoleMatoB(Schnitt->schnitt.mdn,ete->zerm_mat,Schnitt->cfg_PreiseAus_a_kalkpreis, Schnitt->schnitt.kalk_bas, Schnitt->PrGrStuf) * 1.2 / Schnitt->schnitt._wert_fwp;
		}
//230408		ete->fwp_org = ete->fwp;
	}
}

void CMaTreeCtrl::Zerm_aus_a_typ_NeuSetzen (double Zerm_aus_mat)
{
	for (std::vector<CZerlTreeData *>::iterator e = ZerlTreeData.begin (); e != ZerlTreeData.end (); ++e)
	{
		CZerlTreeData *ete = *e;
		if (ete->zerm_mat == 0) continue;
		if (ete->a_typ == Zerm->TYP_MATERIAL || ete->a_typ == Zerm->TYP_VKARTIKEL)
		{
			ete->zerm_aus_a_typ = Zerm->TYP_MATERIAL;
		}
		if (ete->zerm_aus_mat == Zerm_aus_mat && ete->a_typ != Zerm->TYP_MATERIAL && ete->a_typ != Zerm->TYP_VKARTIKEL)
		{
			ete->zerm_aus_a_typ = Zerm->TYP_VKARTIKEL;
		}
	}
}

BOOL CMaTreeCtrl::KorrigiereATypVK ()
{
	Work->SetzeDefault_aus_a_typ (Zerm->zerm.mdn,Zerm->zerm.schnitt );
	Zerm->zerm.mdn = Schnitt->schnitt.mdn;
	Zerm->zerm.schnitt = Schnitt->schnitt.schnitt;
	Zerm->sqlopen (Zerm->ATypVKCursor);
	while (Zerm->sqlfetch (Zerm->ATypVKCursor) == 0)
	{
		Zerm->zerm.zerm_aus_a_typ = Zerm->TYP_VKARTIKEL;
		Zerm->dbupdate();
	}
	return TRUE;
}

BOOL CMaTreeCtrl::VKVorhanden (long aus_mat)
{
	for (std::vector<CZerlTreeData *>::iterator e = ZerlTreeData.begin (); e != ZerlTreeData.end (); ++e)
	{
		CZerlTreeData *ete = *e;
		if (ete->zerm_mat == 0) continue;
		if (ete->a_typ == Zerm->TYP_VKARTIKEL)
		{
			if (ete->zerm_mat == aus_mat)
			{
				return TRUE;
			}
		}
	}
	return FALSE;
}
double CMaTreeCtrl::HoleHKKosten (double kosten,char* a_kz, short list_ebene, long a_z_f,char* zerm_teil)
{
	CString A_Kz;
	CString Zerm_teil;
	A_Kz = a_kz;
	Zerm_teil = zerm_teil;
	if (A_Kz != "ZV" && A_Kz != "GT" && A_Kz != "VM" && A_Kz != "VT" && A_Kz != "ZA") return kosten;
	if (A_Kz == "ZV") return (double) 0;
	kosten += Schnitt->schnitt._grund_kosten;
	if (Zerm_teil == "1") 
	{
		kosten += Schnitt->schnitt._grob_kosten;
		return kosten;
	}
	if (A_Kz == "GT") 
	{
		kosten += Schnitt->schnitt._grob_kosten;
		if (list_ebene > 1) kosten += (Schnitt->schnitt._grob_kosten * (list_ebene -1));
		return kosten;
	}
	if (A_Kz == "VM")
	{
		kosten += Schnitt->schnitt._vm_kosten;
		if (Schnitt->cfg_GTKostenAddieren == 1)
		{
	 		if (list_ebene > 1) kosten += (Schnitt->schnitt._grob_kosten * (list_ebene -1));
		}
		return kosten;
	}
	if (A_Kz == "VT")
	{
		kosten += Schnitt->schnitt._vt_kosten;
		if (Schnitt->cfg_GTKostenAddieren == 1)
		{
	 		if (list_ebene > 1) kosten += (Schnitt->schnitt._grob_kosten * (list_ebene -1));
		}
		return kosten;
	}
	if (A_Kz == "ZA")
	{
		kosten += Schnitt->schnitt._za_kosten;
		if (Schnitt->cfg_GTKostenAddieren == 1)
		{
	 		if (list_ebene > 1) kosten += (Schnitt->schnitt._grob_kosten * (list_ebene -1));
		}
		return kosten;
	}
	return kosten;
}
