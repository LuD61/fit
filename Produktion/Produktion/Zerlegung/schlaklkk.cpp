#include "stdafx.h"
#include "schlaklkk.h"

struct SCHLAKLKK schlaklkk, schlaklkk_null;

void SCHLAKLKK_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &schlaklkk.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &schlaklkk.schlklknr,  SQLLONG, 0);

    sqlout ((short *) &schlaklkk.mdn,SQLSHORT,0);
    sqlout ((long *) &schlaklkk.schlklknr,SQLLONG,0);
    sqlout ((TCHAR *) schlaklkk.schklk_bez,SQLCHAR,37);
    sqlout ((short *) &schlaklkk.kalkart,SQLSHORT,0);
    sqlout ((long *) &schlaklkk.anzahl,SQLLONG,0);
    sqlout ((double *) &schlaklkk.lebendgew,SQLDOUBLE,0);
    sqlout ((double *) &schlaklkk.schlachtgew,SQLDOUBLE,0);
    sqlout ((double *) &schlaklkk.kaltgew,SQLDOUBLE,0);
    sqlout ((double *) &schlaklkk.preisek,SQLDOUBLE,0);
    sqlout ((double *) &schlaklkk.nettowert,SQLDOUBLE,0);
    sqlout ((double *) &schlaklkk.nettoprkg,SQLDOUBLE,0);
    sqlout ((double *) &schlaklkk.zuabkg,SQLDOUBLE,0);
    sqlout ((double *) &schlaklkk.ausbeute,SQLDOUBLE,0);
    sqlout ((double *) &schlaklkk.verlust,SQLDOUBLE,0);
            cursor = sqlcursor (_T("select schlaklkk.mdn,  ")
_T("schlaklkk.schlklknr,  schlaklkk.schklk_bez,  schlaklkk.kalkart,  ")
_T("schlaklkk.anzahl,  schlaklkk.lebendgew,  schlaklkk.schlachtgew,  ")
_T("schlaklkk.kaltgew,  schlaklkk.preisek,  schlaklkk.nettowert,  ")
_T("schlaklkk.nettoprkg,  schlaklkk.zuabkg,  schlaklkk.ausbeute,  ")
_T("schlaklkk.verlust from schlaklkk ")

                                  _T(" where mdn = ? ")
                                  _T(" and schlklknr  = ?"));
 
    sqlin ((short *) &schlaklkk.mdn,SQLSHORT,0);
    sqlin ((long *) &schlaklkk.schlklknr,SQLLONG,0);
    sqlin ((TCHAR *) schlaklkk.schklk_bez,SQLCHAR,37);
    sqlin ((short *) &schlaklkk.kalkart,SQLSHORT,0);
    sqlin ((long *) &schlaklkk.anzahl,SQLLONG,0);
    sqlin ((double *) &schlaklkk.lebendgew,SQLDOUBLE,0);
    sqlin ((double *) &schlaklkk.schlachtgew,SQLDOUBLE,0);
    sqlin ((double *) &schlaklkk.kaltgew,SQLDOUBLE,0);
    sqlin ((double *) &schlaklkk.preisek,SQLDOUBLE,0);
    sqlin ((double *) &schlaklkk.nettowert,SQLDOUBLE,0);
    sqlin ((double *) &schlaklkk.nettoprkg,SQLDOUBLE,0);
    sqlin ((double *) &schlaklkk.zuabkg,SQLDOUBLE,0);
    sqlin ((double *) &schlaklkk.ausbeute,SQLDOUBLE,0);
    sqlin ((double *) &schlaklkk.verlust,SQLDOUBLE,0);
            sqltext = _T("update schlaklkk set ")
_T("schlaklkk.mdn = ?,  schlaklkk.schlklknr = ?,  ")
_T("schlaklkk.schklk_bez = ?,  schlaklkk.kalkart = ?,  ")
_T("schlaklkk.anzahl = ?,  schlaklkk.lebendgew = ?,  ")
_T("schlaklkk.schlachtgew = ?,  schlaklkk.kaltgew = ?,  ")
_T("schlaklkk.preisek = ?,  schlaklkk.nettowert = ?,  ")
_T("schlaklkk.nettoprkg = ?,  schlaklkk.zuabkg = ?,  ")
_T("schlaklkk.ausbeute = ?,  schlaklkk.verlust = ? ")

                                  _T(" where mdn = ?")
                                  _T(" and schlklknr = ?");

            sqlin ((short *)   &schlaklkk.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &schlaklkk.schlklknr,  SQLLONG, 0);

            upd_cursor = sqlcursor (sqltext);


            sqlin ((short *)   &schlaklkk.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &schlaklkk.schlklknr,  SQLLONG, 0);
            test_upd_cursor = sqlcursor (_T("select schlklknr from schlaklkk ")
                                 _T(" where mdn = ? ")
                                  _T(" and schlklknr  = ?"));

            sqlin ((short *)   &schlaklkk.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &schlaklkk.schlklknr,  SQLLONG, 0);
            del_cursor = sqlcursor (_T("delete from schlaklkk ")
                                 _T(" where mdn = ? ")
                                  _T(" and schlklknr  = ?"));

    sqlin ((short *) &schlaklkk.mdn,SQLSHORT,0);
    sqlin ((long *) &schlaklkk.schlklknr,SQLLONG,0);
    sqlin ((TCHAR *) schlaklkk.schklk_bez,SQLCHAR,37);
    sqlin ((short *) &schlaklkk.kalkart,SQLSHORT,0);
    sqlin ((long *) &schlaklkk.anzahl,SQLLONG,0);
    sqlin ((double *) &schlaklkk.lebendgew,SQLDOUBLE,0);
    sqlin ((double *) &schlaklkk.schlachtgew,SQLDOUBLE,0);
    sqlin ((double *) &schlaklkk.kaltgew,SQLDOUBLE,0);
    sqlin ((double *) &schlaklkk.preisek,SQLDOUBLE,0);
    sqlin ((double *) &schlaklkk.nettowert,SQLDOUBLE,0);
    sqlin ((double *) &schlaklkk.nettoprkg,SQLDOUBLE,0);
    sqlin ((double *) &schlaklkk.zuabkg,SQLDOUBLE,0);
    sqlin ((double *) &schlaklkk.ausbeute,SQLDOUBLE,0);
    sqlin ((double *) &schlaklkk.verlust,SQLDOUBLE,0);
            ins_cursor = sqlcursor (_T("insert into schlaklkk (")
_T("mdn,  schlklknr,  schklk_bez,  kalkart,  anzahl,  lebendgew,  schlachtgew,  ")
_T("kaltgew,  preisek,  nettowert,  nettoprkg,  zuabkg,  ausbeute,  verlust) ")

                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?)")); 

}
