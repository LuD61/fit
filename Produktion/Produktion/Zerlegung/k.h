#ifndef _K_DEF
#define _K_DEF
//#include <odbcinst.h>
//#include <sqlext.h>
//Sturktur k muss immer Identisch sein zur Struktur Zerm !!!
struct K {
   short          mdn;
   short          schnitt;
   long           zerm_aus_mat;
   long           zerm_mat;
   TCHAR          a_kz[3];
   TCHAR          fix[2];
   double         fwp;
   double         zerm_gew;
   double         zerm_gew_ant;
   TCHAR          zerm_teil[2];
   double         mat_o_b;
   double         hk_vollk;
   double         hk_teilk;
   short          delstatus;
   double         vollkosten_abs;
   double         vollkosten_proz;
   double         teilkosten_abs;
   double         teilkosten_proz;
   double         dm_100;
   long           a_z_f;
   double         vk_pr;
   double         kosten_zutat;
   double         kosten_vpk;
   short          a_typ;
   TCHAR          kost_bz[25];
   double         kosten;
   double         pr_zutat;
   double         pr_vpk;
   double         pr_kosten;
   double         pr_vk_netto;
   double         kosten_fix;
   double         pr_vk_eh;
   double         kosten_gh;
   double         kosten_eh;
   short          lfd;
   short          list_lfd;
   short          list_ebene;
   short		  zerm_aus_a_typ;
   double    fwp_org;
   double         sk_vollk;
   double         sk_teilk;
   double         fil_ek_vollk;
   double         fil_ek_teilk;
   double         fil_vk_vollk;
   double         fil_vk_teilk;
   double         gh1_vollk;
   double         gh1_teilk;
   double         gh2_vollk;
   double         gh2_teilk;
//Zusätzlich in die zerm-struktur (ohne DB)
   TCHAR          zerm_aus_mat_bz[25];
   TCHAR          zerm_mat_bz1[25];
   TCHAR          zerm_mat_bz2[25];
   BOOL			  chk_gew;
   BOOL			  chk_gew_ant;
   BOOL			  chk_mat_o_b;
   BOOL			  chk_wert_100kg;
   BOOL			  chk_wert;
   BOOL			  chk_vk;
   BOOL			  chk_fwp;
   BOOL			  chk_kennzeichen;
   BOOL			  chk_zeitfaktor;
   TCHAR          variabel[2];
   BOOL			  chk_hk_vollk_preis;
   BOOL			  chk_hk_vollk_wert;
   BOOL			  chk_hk_vollk;
};
extern struct K k, k_null;

#line 7 "k.rh"
// ZERM durch k ersetzen --->extern struct K k;
  	   

#endif
