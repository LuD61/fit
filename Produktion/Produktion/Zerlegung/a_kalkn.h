#ifndef _A_KALKN_DEF
#define _A_KALKN_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct A_KALKN {
   short          mdn;
   short          fil;
   double         a;
   DATE_STRUCT    gue_ab;
   double         ek;
   double         sk;
   double         fil_ek;
};
extern struct A_KALKN a_kalkn, a_kalkn_null;

#line 8 "a_kalkn.rh"


class A_KALKN_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               A_KALKN a_kalkn;  
               A_KALKN_CLASS () : DB_CLASS ()
               {
               }
};
#endif
