#pragma once
#include "DbPropertyPage.h"
#include "NumEdit.h"
#include "TextEdit.h"
#include "TextEdit.h"
#include "CtrlGrid.h"
#include "sys_par.h"
#include "mdn.h"
#include "kost_art.h"
#include "schlaklkk.h"
#include "adr.h"
#include "Schnitt.h"
#include "zerm.h"
#include "zerm_mat_v.h"
#include "a_mat.h"
#include "a_bas.h"
#include "a_varb.h"
#include "a_kalk_mat.h"
#include "a_kalkhndw.h"
#include "DbUniCode.h"
#include "mo_progcfg.h"
#include "FormTab.h"
#include "afxwin.h"
#include "ChoiceMdn.h"
#include "ChoiceKalk.h"
#include "ChoiceSchnitt.h"
#include "ChoiceA.h"
#include "Controls.h"
#include "WorkDB.h"
#include "WorkPrint.h"
#include "ZerlTreeData.h"

// CPage1-Dialogfeld

#define IDC_SCHNITTCHOICE 3000
#define IDC_MDNCHOICE 3001
#define IDC_MATERIALCHOICE 3002
#define IDC_SCHLACHTKALKCHOICE 3003
#define IDC_SCHLACHTARTCHOICE 3004

class CPage1 : public CDbPropertyPage
{
	DECLARE_DYNAMIC(CPage1)

public:
	CPage1();
	virtual ~CPage1();

// Dialogfelddaten
	enum { IDD = IDD_PAGE1 };

	PROG_CFG Cfg;
	CCtrlGrid CtrlGrid;
	CCtrlGrid MdnGrid;
	CCtrlGrid SchnittGrid;
	CCtrlGrid AusMatGrid;
	CCtrlGrid MaterialGrid;
	CCtrlGrid SchlachtKalkGrid;
	CCtrlGrid SchlachtArtGrid;
	CCtrlGrid InfoGrid;
	CCtrlGrid BearbGrid;
	CCtrlGrid HinweisGrid;

	CFormTab Form;
	CFormTab Keys;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog();
    virtual BOOL PreTranslateMessage(MSG* pMsg);
    virtual BOOL OnReturn ();
    virtual BOOL OnKeyup ();
	virtual BOOL StepBack ();
	virtual void OnCopy ();
	virtual BOOL Print (HWND);
	virtual void OnPaste ();
//	virtual void OnFileSave ();
	afx_msg void OnFileSave();
	virtual BOOL OnSetActive ();
	BOOL flgCopy;

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL Write ();
	CStatic m_LMdn;
	CNumEdit m_Mdn;
	CButton m_MdnChoice;
	CChoiceMdn *ChoiceMdn;
	CChoiceKalk *ChoiceKalk;
	CEdit m_MdnName;

	CStatic m_LSchnitt;
	CNumEdit m_Schnitt;
	CButton m_SchnittChoice;
	CTextEdit m_SchnittTxt;
	CButton m_MaterialChoice;
	CButton m_SchlachtKalkChoice;
	CButton m_SchlachtArtChoice;

	CFont Font;
	CFont lFont;

	KOST_ART_CLASS KostArt;
	KOST_ART_ZERL_CLASS KostArtZerl;
	MDN_CLASS Mdn;
	SCHLAKLKK_CLASS Schlaklkk;
	ADR_CLASS MdnAdr;
	SCHNITT_CLASS Schnitt;
	ZERM_CLASS Zerm;
	ZERM_MAT_V_CLASS Zerm_mat_v;
	A_MAT_CLASS A_mat;
	A_BAS_CLASS A_bas;
	SYS_PAR_CLASS Sys_par;
	CZerlTreeData ZerlTreeData;
	WORKDB_CLASS Work;
	WORKPRINT_CLASS WorkPrint;

	CString PersName;
	CWnd *Frame;
	CControls HeadControls;
	CControls PosControls;


	BOOL ReadMdn ();
	BOOL ReadMaterial(long);
	BOOL ReadSchlachtArt(double);
	BOOL ReadSchlachtKalk(int);
    void OnMdnchoice(); 
    void OnSchlKalkchoice(); 
    void OnSchnittchoice(); 
    void OnMatchoice(); 
    void EkPreiseHolen(); 
    void OnSchlArtchoice(); 
    void OnDelete(); 
	void ReadCfg ();
    void EnableFields (BOOL b);
    void DisableTabControl (BOOL disable);
	virtual BOOL Read ();
public:
	CStatic m_LHinweis;
	CTextEdit m_Hinweis;
	CStatic m_AusmatGroup;
	CStatic m_LMaterial;
	CStatic m_LGewicht;
	CStatic m_LEk;
	CStatic m_LSchlachtkalk;
	CStatic m_LSchlachtart;
	CStatic m_LPreis;
	CStatic m_InfoGroup;
	CStatic m_LHDKL;
	CStatic m_LAnzahl;
	CStatic m_LBasisFWP;
	CStatic m_LAm;
	CStatic m_LVon;
	CNumEdit m_Gewicht;
	CNumEdit m_Ek;
	CNumEdit m_Schlachtkalk;
	CNumEdit m_Schlachtart;
	CNumEdit m_Preis;
	CTextEdit m_HDKL;
	CNumEdit m_BasisFWP;
	CEdit m_BearbAm;
	CEdit m_BearbVon;
	CEdit m_MaterialTxt;
	CEdit m_SchlachtkalkTxt;
	CNumEdit m_Material;
	CEdit m_SchlachtartTxt;
	CTextEdit m_Anzahl;
	CStatic m_BearbGroup;


	CString Where;
	CString SearchA;
	CChoiceSchnitt *Choice;
	CChoiceA *ChoiceMat;
	CChoiceA *ChoiceSchlA;
	BOOL ModalChoice;
	BOOL AChoiceStat;
	BOOL AChoiceStatM;
	BOOL DelOK;
	void OnChoicebackground();
	void OnChoiceaDefault();
	void OnSchnittSelected();
	void OnSchnittCanceled();
	BOOL WriteFirstZerm ();

public:
	afx_msg void OnEnKillfocusMaterial();
public:
	afx_msg void OnEnKillfocusSchlachtkalk();
public:
	afx_msg void OnEnKillfocusSchlachtart();
public:
	afx_msg void OnEnUpdateGewicht();
public:
	afx_msg void OnEnKillfocusGewicht();
public:
	afx_msg void OnEnKillfocusEk();
public:
	afx_msg void OnEnKillfocusPreis();
public:
	afx_msg void OnEnSetfocusEk();
public:
	afx_msg void OnEnSetfocusSchnittTxt();
public:
	afx_msg void OnEnChangeSchnitt();
public:
	afx_msg void OnEnSetfocusSchnitt();
};

