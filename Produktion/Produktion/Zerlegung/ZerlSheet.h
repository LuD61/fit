#pragma once
#include "dbpropertysheet.h"

class CZerlSheet :
	public CDbPropertySheet
{
	DECLARE_DYNAMIC(CZerlSheet)
public:
	CZerlSheet(void);
	~CZerlSheet(void);
	virtual BOOL Write ();
	virtual BOOL Page2Write ();
    virtual void StepBack ();
    virtual void OnCopy ();
	virtual void OnPrint ();
    virtual void OnPaste ();
	virtual BOOL Delete ();
//	virtual BOOL Print ();
};
