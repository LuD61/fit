#ifndef _KOST_ART_DEF
#define _KOST_ART_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct KOST_ART {
   short          mdn;
   short          fil;
   short          kost_art;
   TCHAR          kost_art_bz[25];
   short          delstatus;
   long           erl_kto;
   TCHAR          vkost_rechb[4];
   double         vkost_wt;
   double         faktor;
   DATE_STRUCT    dat;
   TCHAR          pers_nam[9];
   short          mwst_schl;
   long           ag;
   double         vkost_fix;
};
extern struct KOST_ART kost_art, kost_art_null;

#line 8 "kost_art.rh"

class KOST_ART_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               KOST_ART kost_art;  
               KOST_ART_CLASS () : DB_CLASS ()
               {
               }
};
#endif
