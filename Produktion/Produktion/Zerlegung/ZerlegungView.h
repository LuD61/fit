// ZerlegungView.h : Schnittstelle der Klasse CZerlegungView
//


#pragma once

#include "DbFormView.h"
#include "DbPropertySheet.h"
#include "Page1.h"
#include "Page2.h"
#include "Page3.h"
#include "mo_progcfg.h"
#include "PageUpdate.h"

class CZerlegungView : public DbFormView, 
	                 CPageUpdate
{
protected: // Nur aus Serialisierung erstellen
	CZerlegungView();


	DECLARE_DYNCREATE(CZerlegungView)

// Attribute
public:
	enum { 
		IDD = IDD_ZERLEGUNG ,
		START_NORMAL = 0,
		START_MAXIMIZED = 1,
		START_FITSIZE = 2,
		};
	void ReadCfg ();


// Operationen
public:

// Überschreibungen
protected:
	void OnInitialUpdate();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

// Implementierung
public:
	virtual ~CZerlegungView();

	CPropertySheet dlg;
	CPage1 Page1;
	CPage2 Page2;
	CPage3 Page3;
	int tabx;
	int taby;
	int StartSize;
	PROG_CFG Cfg;


#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generierte Funktionen für die Meldungstabellen
protected:
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 
    afx_msg void OnSize(UINT, int, int);

	DECLARE_MESSAGE_MAP()
	afx_msg void OnFileSave();
	afx_msg void OnUpdateFileSave(CCmdUI *pCmdUI);
	afx_msg void OnBack();
};
