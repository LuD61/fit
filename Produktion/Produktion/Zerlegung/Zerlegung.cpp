// Zerlegung.cpp : Definiert das Klassenverhalten f�r die Anwendung.
//

#include "stdafx.h"
#include "Zerlegung.h"
#include "MainFrm.h"

#include "ChildFrm.h"
#include "ZerlegungDoc.h"
#include "ZerlView.h"
#include "afxwin.h"
#include "EditListCtrl.h"
#include "afxcmn.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CDocTemplate *ZerlInstance = NULL; 

// CZerlegungApp

BEGIN_MESSAGE_MAP(CZerlegungApp, CWinApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	// Dateibasierte Standarddokumentbefehle
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	// Standarddruckbefehl "Seite einrichten"
	ON_COMMAND(ID_FILE_PRINT_SETUP, CWinApp::OnFilePrintSetup)
	ON_COMMAND(ID_ZERLEGUNG, OnZerlegung)
END_MESSAGE_MAP()


// CZerlegungApp-Erstellung

CZerlegungApp::CZerlegungApp()
{
	// TODO: Hier Code zur Konstruktion einf�gen
	// Alle wichtigen Initialisierungen in InitInstance positionieren
}


// Das einzige CZerlegungApp-Objekt

CZerlegungApp theApp;

// CZerlegungApp Initialisierung

BOOL CZerlegungApp::InitInstance()
{
	// InitCommonControls() ist f�r Windows XP erforderlich, wenn ein Anwendungsmanifest
	// die Verwendung von ComCtl32.dll Version 6 oder h�her zum Aktivieren
	// von visuellen Stilen angibt. Ansonsten treten beim Erstellen von Fenstern Fehler auf.
	InitCommonControls();

	CWinApp::InitInstance();

	// OLE-Bibliotheken initialisieren
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}
	AfxEnableControlContainer();
	// Standardinitialisierung
	// Wenn Sie diese Features nicht verwenden und die Gr��e
	// der ausf�hrbaren Datei verringern m�chten, entfernen Sie
	// die nicht erforderlichen Initialisierungsroutinen.
	// �ndern Sie den Registrierungsschl�ssel unter dem Ihre Einstellungen gespeichert sind.
	// TODO: �ndern Sie diese Zeichenfolge entsprechend,
	// z.B. zum Namen Ihrer Firma oder Organisation.
	SetRegistryKey(_T("Vom lokalen Anwendungs-Assistenten generierte Anwendungen"));
	LoadStdProfileSettings(4);  // Standard INI-Dateioptionen laden (einschlie�lich MRU)
	// Dokumentvorlagen der Anwendung registrieren. Dokumentvorlagen
	//  dienen als Verbindung zwischen Dokumenten, Rahmenfenstern und Ansichten.
	CMultiDocTemplate* pDocTemplate;
	pDocTemplate = new CMultiDocTemplate(IDR_ZerlegungTYPE,
		RUNTIME_CLASS(CZerlegungDoc),
		RUNTIME_CLASS(CChildFrame), // Benutzerspezifischer MDI-Child-Rahmen
		RUNTIME_CLASS(CZerlView));
	if (!pDocTemplate)
		return FALSE;
	AddDocTemplate(pDocTemplate);
	// Haupt-MDI-Rahmenfenster erstellen
	CMainFrame* pMainFrame = new CMainFrame;
	if (!pMainFrame || !pMainFrame->LoadFrame(IDR_MAINFRAME))
		return FALSE;
	m_pMainWnd = pMainFrame;
	// Rufen Sie DragAcceptFiles nur auf, wenn eine Suffix vorhanden ist.
	//  In einer MDI-Anwendung ist dies unmittelbar nach dem Festlegen von m_pMainWnd erforderlich
	// Befehlszeile parsen, um zu pr�fen auf Standardumgebungsbefehle DDE, Datei offen
	CCommandLineInfo cmdInfo;
//	ParseCommandLine(cmdInfo);
	// Verteilung der in der Befehlszeile angegebenen Befehle. Es wird FALSE zur�ckgegeben, wenn
	// die Anwendung mit /RegServer, /Register, /Unregserver oder /Unregister gestartet wurde.
//	if (!ProcessShellCommand(cmdInfo))
//		return FALSE;
	// Das Hauptfenster ist initialisiert und kann jetzt angezeigt und aktualisiert werden.
	pMainFrame->ShowWindow(SW_SHOWMAXIMIZED);
	pMainFrame->UpdateWindow();
	OnZerlegung();
	return TRUE;
}



// CAboutDlg-Dialogfeld f�r Anwendungsbefehl 'Info'

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialogfelddaten
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterst�tzung
	void FuelleListInfo();
	void FuelleListHistory();
	void TextInfo(CString,CString);
	void TextHist(CString,CString);
	int Info_idx, Hist_idx;

// Implementierung
protected:
	DECLARE_MESSAGE_MAP()
public:
	CEditListCtrl m_ListInfo;
public:
	CEditListCtrl m_ListHistory;
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_INFO, m_ListInfo);
	DDX_Control(pDX, IDC_LIST_HISTORY, m_ListHistory);
	CFillList FillList;
    FuelleListInfo ();
    FuelleListHistory ();

}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()

// Anwendungsbefehl zum Ausf�hren des Dialogfelds
void CZerlegungApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}


// CZerlegungApp Meldungshandler


void CZerlegungApp::OnZerlegung()
{
	POSITION pos;
	pos = GetFirstDocTemplatePosition ();
	CDocTemplate *dt = GetNextDocTemplate (pos);
	if (dt != NULL)
	{
		if (dt == ZerlInstance)
		{
            CMainFrame *MainFrame =  (CMainFrame *) m_pMainWnd;
			if (MainFrame->ZerlWnd != NULL)
			{
				MainFrame->MDIActivate (MainFrame->ZerlWnd);
				return;
			}
		}

		ZerlInstance = dt;
		CDocument *dc = dt->CreateNewDocument ();
		if (dc !=NULL)
		{
//			dt->SetDefaultTitle (dc);
			dc->SetTitle (_T("Zerlegung"));
		}
		CFrameWnd *Frame = dt->CreateNewFrame (dc, NULL);
		if (Frame != NULL)
		{
			dt->InitialUpdateFrame (Frame, NULL, TRUE);
		}
	}
}

void CAboutDlg::TextInfo(CString t1, CString t2)
{
	m_ListInfo.FillList.InsertItem (Info_idx, 0);
	m_ListInfo.FillList.SetItemText (t1.GetBuffer(), Info_idx, 1);
	m_ListInfo.FillList.SetItemText (t2.GetBuffer(), Info_idx, 2);
	Info_idx++;
}
void CAboutDlg::TextHist(CString t1, CString t2)
{
	m_ListHistory.FillList.InsertItem (Hist_idx, 0);
	m_ListHistory.FillList.SetItemText (t1.GetBuffer(), Hist_idx, 1);
	m_ListHistory.FillList.SetItemText (t2.GetBuffer(), Hist_idx, 2);
	Hist_idx++;
}
void CAboutDlg::FuelleListInfo()
{
	m_ListInfo.FillList.SetStyle (LVS_REPORT);
//	m_ListInfo.FillList.SetExtendedStyle (LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);
	m_ListInfo.SetExtendedStyle (LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT  | LVS_EX_HEADERDRAGDROP);
	m_ListInfo.DeleteAllItems ();
	Info_idx = 0;
//	m_ListInfo.ListEdit.SetReadOnly (TRUE);

	m_ListInfo.FillList.SetCol ("", 0, 0);
	m_ListInfo.FillList.SetCol ("Kontext", 1, 130, LVCFMT_LEFT);
	m_ListInfo.FillList.SetCol ("Beschreibung", 2, 800, LVCFMT_LEFT);

	TextInfo("Programmeinstieg","Die Preise der Zutaten und Verpackungen werden geholt");
	TextInfo("",                "Zutaten : aus a_kalk_mat.hk_vollk");
	TextInfo("",                "Verpackungen : aus a_kalkhndw.pr_ek1 bei Artikeltyp = 8 (Verpackungen)");
	TextInfo("",                "             : aus a_kalk_mat.hk_vollk bei Artikeltyp = 9 (H�llen,D�rme..)");
	TextInfo("Speichern der Materialien","");
	TextInfo("",         "a_kalk_mat.mat_o_b , a_kalk_mat.hk_teilk aus Preis(ohneBearb.)");
	TextInfo("",         "a_kalk_mat.hk_vollk aus HK-Preis (incl. Kosten ohne Aufschl�ge)");
	TextInfo("Speichern der VK-Artikel","Preis incl. Kosten : in a_kalkhndw.sk_vollk (und sk_teilk)");
	TextInfo("           ","Preis incl. GH-Aufschlag : in a_kalkhndw.fil_ek_vollk (und fil_ek_teilk)");
	TextInfo("           ","Preis incl. EH-Aufschlag : in a_kalkhndw.lad_vk_vollk (und lad_vk_teilk)");
	TextInfo("Kopierfunktion :","Auf Seite 2 im Zerlegebaum ein Grobteil selektieren");
	TextInfo("",         "und dann im Men� auf Bearbeiten->Kopieren (oder Button Kopieren)");
	TextInfo("Einf�gen  :","nach dem Kopieren :  Men� Datei->Neu (oder Button Neu)");
	TextInfo("",         "es erscheint eine neue Seite mit Schnitt = -1. Diese Schnittnummer muss mit einer neuen Nummer");
	TextInfo("",         "�berschrieben werden. Das kopierte Grobteil mit allen seinen untergeordneten Teilen wurde jetzt in diese neue Schnittf�hrung eingef�gt");
}
void CAboutDlg::FuelleListHistory()
{
	m_ListHistory.FillList.SetStyle (LVS_REPORT);
	m_ListHistory.FillList.SetExtendedStyle (LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);
	m_ListHistory.DeleteAllItems ();
	Hist_idx = 0;

	m_ListHistory.FillList.SetCol ("", 0, 0);
	m_ListHistory.FillList.SetCol ("Datum", 1, 70, LVCFMT_LEFT);
	m_ListHistory.FillList.SetCol ("Info", 2, 800, LVCFMT_LEFT);

	TextHist("","");
	TextHist("07.10.2009","Zerlegungen unter Mandant 0 schreiben Preise auch in Mandant 1 (H�nnger)");
	TextHist("19.05.2009","wenn ein Grobteil auf Fix/Variabel gesetzt wird, werden alle untergeordneten Materialien auch umgesetzt");
	TextHist("      2009","Deckung kann ver�ndert werden");
	TextHist("23.04.2008","Wert der zerlegten Teile, Deckung in � und % wird angezeigt");
	TextHist("          ","           sobald ein Material variabel ist und somit umgelastet wurde, k�nnen die Werte Summe ,Deckung � und % editiert werden. ");
	TextHist("          ","           es wird dann mit diesen Soll-Vorgaben umgelastet. Deckung in % wird gespeichert und beim wiedereinstieg in den Schnitt wieder verwendet");
	TextHist("03.04.2008","Variable NurVariableMaterialienSpeichern in cfg-Datei einfgf�hrt (default ist 1)");
	TextHist("03.04.2008","Schlachtkalkulation (Programm schlaka) integriert, Auswahl und Berechnung bei den Basisdaten");
	TextHist("10.09.2007","Zerlegeverlust kann nicht mehr gel�scht werden, wird automatisch entfernt");
	TextHist("10.09.2007","VK-Artikel automatisch selektiert, wenn Ausgagsmaterial auf Variabel gesetzt wird ");
	TextHist("10.09.2007","Fenster beliebig in ihrer Gr��e ver�nderbar ");
	TextHist("24.08.2007","VK-Artikel werden in a_kalkhndw zur�ckgeschrieben");
	TextHist("22.08.2007","Checkbox : VK-Artikel Details  ");
	TextHist("19.08.2007","Kalkulation umgestellt : Beim Fix-Setzen wird der FWP wieder auf den Ursprungswert vor dem Umlasten (Kalkuklieren) zur�ckgestellt");
	TextHist("          ","           Dieser Wert wird auch f�r die Kalkulation weiter benutzt bis er durch eine manuelle Preis�nderung �berschrieben wird");
	TextHist("          ","           oder durch Speichern der Schnittf�hrung fixiert wird.");
	TextHist("          ","           So kann beliebig oft das Kennzeichen bei verschiedenen Materialien umgesetzt werden, ohne das sich die Werte f�r das weitere Umlasten direkt �ndern");
	TextHist("16.08.2007","10 verschiedene Bitmaps f�r Zerlegeverlust,je nach H�he des Verlustes  ");
	TextHist("15.08.2007","Checkboxes im Treeview ersetzt durch unterschiedl. Bitmaps, f�r GT, Mat..,Variabel, Zerlegeverlust (< 0, wenig, viel)");
	TextHist("09.08.2007","Neu :Kopierfunktion : Menue : Bearbeiten->Kopieren (Vorerst nur zum Kopieren in eine neue Schnittf�hrung) ");
	TextHist("07.08.2007","Neu :Checkbox : 1. Ebene Anzeigen, einzeln erweitern");
	TextHist("07.08.2007","Checkbox : Kosten Details -> TreeView wird erweitert ");
	TextHist("07.08.2007","Beim Speichern Datenbankfehlermeldungen abgefangen");
	TextHist("06.08.2007","L�schen aus dem Tree heraus : Speicherte nicht wenn auf Seite 1 gewechselt wurde , behoben ");
	TextHist("06.08.2007","L�schen aus dem Tree heraus : Zerlegeverlust u. Preise  wird direkt neu berechnet und in den Tree gestellt ");
	TextHist("06.08.2007","Preise holen vereinheitlicht ");
	TextHist("03.08.2007","Preise f�r Zutaten und Verpackungen erst in a_kalkn suchen");
	TextHist("","Seite Materialien : Listview: Spalten jetzt verschiebbar");
	TextHist("02.08.2007","Fertigstellung der Grundversion");
}
