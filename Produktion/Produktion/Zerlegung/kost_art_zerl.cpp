#include "stdafx.h"
#include "kost_art_zerl.h"

struct KOST_ART_ZERL kost_art_zerl, kost_art_zerl_null;

void KOST_ART_ZERL_CLASS::prepare (void)
{
            char *sqltext;

            sqlin ((short *)   &kost_art_zerl.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &kost_art_zerl.kost_art_zerl,  SQLSHORT, 0);
    sqlout ((short *) &kost_art_zerl.mdn,SQLSHORT,0);
    sqlout ((short *) &kost_art_zerl.fil,SQLSHORT,0);
    sqlout ((short *) &kost_art_zerl.kost_art_zerl,SQLSHORT,0);
    sqlout ((TCHAR *) kost_art_zerl.kost_art_zerl_bz,SQLCHAR,25);
    sqlout ((short *) &kost_art_zerl.delstatus,SQLSHORT,0);
    sqlout ((long *) &kost_art_zerl.erl_kto,SQLLONG,0);
    sqlout ((TCHAR *) kost_art_zerl.vkost_rechb,SQLCHAR,4);
    sqlout ((double *) &kost_art_zerl.vkost_wt,SQLDOUBLE,0);
    sqlout ((double *) &kost_art_zerl.faktor,SQLDOUBLE,0);
    sqlout ((DATE_STRUCT *) &kost_art_zerl.dat,SQLDATE,0);
    sqlout ((TCHAR *) kost_art_zerl.pers_nam,SQLCHAR,9);
    sqlout ((double *) &kost_art_zerl.vkost_fix,SQLDOUBLE,0);
            cursor = sqlcursor (_T("select kost_art_zerl.mdn,  ")
_T("kost_art_zerl.fil,  kost_art_zerl.kost_art_zerl,  ")
_T("kost_art_zerl.kost_art_zerl_bz,  kost_art_zerl.delstatus,  ")
_T("kost_art_zerl.erl_kto,  kost_art_zerl.vkost_rechb,  ")
_T("kost_art_zerl.vkost_wt,  kost_art_zerl.faktor,  kost_art_zerl.dat,  ")
_T("kost_art_zerl.pers_nam,  kost_art_zerl.vkost_fix from kost_art_zerl ")

#line 13 "kost_art_zerl.rpp"
                                  _T("where mdn = ? and kost_art_zerl = ? "));
    sqlin ((short *) &kost_art_zerl.mdn,SQLSHORT,0);
    sqlin ((short *) &kost_art_zerl.fil,SQLSHORT,0);
    sqlin ((short *) &kost_art_zerl.kost_art_zerl,SQLSHORT,0);
    sqlin ((TCHAR *) kost_art_zerl.kost_art_zerl_bz,SQLCHAR,25);
    sqlin ((short *) &kost_art_zerl.delstatus,SQLSHORT,0);
    sqlin ((long *) &kost_art_zerl.erl_kto,SQLLONG,0);
    sqlin ((TCHAR *) kost_art_zerl.vkost_rechb,SQLCHAR,4);
    sqlin ((double *) &kost_art_zerl.vkost_wt,SQLDOUBLE,0);
    sqlin ((double *) &kost_art_zerl.faktor,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &kost_art_zerl.dat,SQLDATE,0);
    sqlin ((TCHAR *) kost_art_zerl.pers_nam,SQLCHAR,9);
    sqlin ((double *) &kost_art_zerl.vkost_fix,SQLDOUBLE,0);
            sqltext = _T("update kost_art_zerl set ")
_T("kost_art_zerl.mdn = ?,  kost_art_zerl.fil = ?,  ")
_T("kost_art_zerl.kost_art_zerl = ?,  ")
_T("kost_art_zerl.kost_art_zerl_bz = ?,  ")
_T("kost_art_zerl.delstatus = ?,  kost_art_zerl.erl_kto = ?,  ")
_T("kost_art_zerl.vkost_rechb = ?,  kost_art_zerl.vkost_wt = ?,  ")
_T("kost_art_zerl.faktor = ?,  kost_art_zerl.dat = ?,  ")
_T("kost_art_zerl.pers_nam = ?,  kost_art_zerl.vkost_fix = ? ")

#line 15 "kost_art_zerl.rpp"
                                  _T("where mdn = ? and kost_art_zerl = ? ");
            sqlin ((short *)   &kost_art_zerl.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &kost_art_zerl.kost_art_zerl,  SQLSHORT, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &kost_art_zerl.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &kost_art_zerl.kost_art_zerl,  SQLSHORT, 0);
            test_upd_cursor = sqlcursor (_T("select kost_art_zerl from kost_art_zerl ")
                                  _T("where mdn = ? and kost_art_zerl = ? "));
            sqlin ((short *)   &kost_art_zerl.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &kost_art_zerl.kost_art_zerl,  SQLSHORT, 0);
            del_cursor = sqlcursor (_T("delete from kost_art_zerl ")
                                  _T("where mdn = ? and kost_art_zerl = ? "));
    sqlin ((short *) &kost_art_zerl.mdn,SQLSHORT,0);
    sqlin ((short *) &kost_art_zerl.fil,SQLSHORT,0);
    sqlin ((short *) &kost_art_zerl.kost_art_zerl,SQLSHORT,0);
    sqlin ((TCHAR *) kost_art_zerl.kost_art_zerl_bz,SQLCHAR,25);
    sqlin ((short *) &kost_art_zerl.delstatus,SQLSHORT,0);
    sqlin ((long *) &kost_art_zerl.erl_kto,SQLLONG,0);
    sqlin ((TCHAR *) kost_art_zerl.vkost_rechb,SQLCHAR,4);
    sqlin ((double *) &kost_art_zerl.vkost_wt,SQLDOUBLE,0);
    sqlin ((double *) &kost_art_zerl.faktor,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &kost_art_zerl.dat,SQLDATE,0);
    sqlin ((TCHAR *) kost_art_zerl.pers_nam,SQLCHAR,9);
    sqlin ((double *) &kost_art_zerl.vkost_fix,SQLDOUBLE,0);
            ins_cursor = sqlcursor (_T("insert into kost_art_zerl (")
_T("mdn,  fil,  kost_art_zerl,  kost_art_zerl_bz,  delstatus,  erl_kto,  ")
_T("vkost_rechb,  vkost_wt,  faktor,  dat,  pers_nam,  vkost_fix) ")

#line 29 "kost_art_zerl.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?)")); 

#line 31 "kost_art_zerl.rpp"
}
