#include "stdafx.h"
#include "a_kalk_mat.h"

struct A_KALK_MAT a_kalk_mat, a_kalk_mat_null;

void A_KALK_MAT_CLASS::prepare (void)
{
            char *sqltext;

            sqlin ((short *)   &a_kalk_mat.mdn,  SQLSHORT, 0);
            sqlin ((double *)   &a_kalk_mat.a,  SQLDOUBLE, 0);
    sqlout ((double *) &a_kalk_mat.a,SQLDOUBLE,0);
    sqlout ((short *) &a_kalk_mat.bearb_sk,SQLSHORT,0);
    sqlout ((short *) &a_kalk_mat.delstatus,SQLSHORT,0);
    sqlout ((short *) &a_kalk_mat.fil,SQLSHORT,0);
    sqlout ((double *) &a_kalk_mat.hk_teilk,SQLDOUBLE,0);
    sqlout ((double *) &a_kalk_mat.hk_vollk,SQLDOUBLE,0);
    sqlout ((double *) &a_kalk_mat.kost,SQLDOUBLE,0);
    sqlout ((double *) &a_kalk_mat.mat_o_b,SQLDOUBLE,0);
    sqlout ((short *) &a_kalk_mat.mdn,SQLSHORT,0);
    sqlout ((double *) &a_kalk_mat.sp_hk,SQLDOUBLE,0);
    sqlout ((DATE_STRUCT *) &a_kalk_mat.dat,SQLDATE,0);
            cursor = sqlcursor (_T("select a_kalk_mat.a,  ")
_T("a_kalk_mat.bearb_sk,  a_kalk_mat.delstatus,  a_kalk_mat.fil,  ")
_T("a_kalk_mat.hk_teilk,  a_kalk_mat.hk_vollk,  a_kalk_mat.kost,  ")
_T("a_kalk_mat.mat_o_b,  a_kalk_mat.mdn,  a_kalk_mat.sp_hk,  ")
_T("a_kalk_mat.dat from a_kalk_mat ")

#line 13 "a_kalk_mat.rpp"
			        _T("where mdn = ? and a = ? and fil = 0 "));
    sqlin ((double *) &a_kalk_mat.a,SQLDOUBLE,0);
    sqlin ((short *) &a_kalk_mat.bearb_sk,SQLSHORT,0);
    sqlin ((short *) &a_kalk_mat.delstatus,SQLSHORT,0);
    sqlin ((short *) &a_kalk_mat.fil,SQLSHORT,0);
    sqlin ((double *) &a_kalk_mat.hk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalk_mat.hk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalk_mat.kost,SQLDOUBLE,0);
    sqlin ((double *) &a_kalk_mat.mat_o_b,SQLDOUBLE,0);
    sqlin ((short *) &a_kalk_mat.mdn,SQLSHORT,0);
    sqlin ((double *) &a_kalk_mat.sp_hk,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &a_kalk_mat.dat,SQLDATE,0);
            sqltext = _T("update a_kalk_mat set ")
_T("a_kalk_mat.a = ?,  a_kalk_mat.bearb_sk = ?,  ")
_T("a_kalk_mat.delstatus = ?,  a_kalk_mat.fil = ?,  ")
_T("a_kalk_mat.hk_teilk = ?,  a_kalk_mat.hk_vollk = ?,  ")
_T("a_kalk_mat.kost = ?,  a_kalk_mat.mat_o_b = ?,  a_kalk_mat.mdn = ?,  ")
_T("a_kalk_mat.sp_hk = ?,  a_kalk_mat.dat = ? ")

#line 15 "a_kalk_mat.rpp"
                                _T("where mdn = ? and a = ?  and fil = 0 ");
            sqlin ((short *)   &a_kalk_mat.mdn,  SQLSHORT, 0);
            sqlin ((double *)   &a_kalk_mat.a,  SQLDOUBLE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &a_kalk_mat.mdn,  SQLSHORT, 0);
            sqlin ((double *)   &a_kalk_mat.a,  SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor (_T("select a from a_kalk_mat ")
			        _T("where mdn = ? and a = ? and fil = 0 "));
            sqlin ((short *)   &a_kalk_mat.mdn,  SQLSHORT, 0);
            sqlin ((double *)   &a_kalk_mat.a,  SQLDOUBLE, 0);
            del_cursor = sqlcursor (_T("delete from a_kalk_mat ")
                                _T("where mdn = ? and a = ?  and fil = 0 "));
    sqlin ((double *) &a_kalk_mat.a,SQLDOUBLE,0);
    sqlin ((short *) &a_kalk_mat.bearb_sk,SQLSHORT,0);
    sqlin ((short *) &a_kalk_mat.delstatus,SQLSHORT,0);
    sqlin ((short *) &a_kalk_mat.fil,SQLSHORT,0);
    sqlin ((double *) &a_kalk_mat.hk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalk_mat.hk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalk_mat.kost,SQLDOUBLE,0);
    sqlin ((double *) &a_kalk_mat.mat_o_b,SQLDOUBLE,0);
    sqlin ((short *) &a_kalk_mat.mdn,SQLSHORT,0);
    sqlin ((double *) &a_kalk_mat.sp_hk,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &a_kalk_mat.dat,SQLDATE,0);
            ins_cursor = sqlcursor (_T("insert into a_kalk_mat (")
_T("a,  bearb_sk,  delstatus,  fil,  hk_teilk,  hk_vollk,  kost,  mat_o_b,  mdn,  sp_hk,  dat) ")

#line 29 "a_kalk_mat.rpp"
                                    _T("values ")
                                    _T("(?,?,?,?,?,?,?,?,?,")
_T("?,?)")); 

#line 31 "a_kalk_mat.rpp"
}
