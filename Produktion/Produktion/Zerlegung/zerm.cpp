#include "stdafx.h"
#include "zerm.h"

struct ZERM zerm, zerm_null;

void ZERM_CLASS::prepare (void)
{
            char *sqltext;

            sqlin ((short *)   &zerm.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &zerm.schnitt,  SQLLONG, 0);
            sqlin ((long *)   &zerm.zerm_aus_mat,  SQLLONG, 0);
            sqlin ((long *)   &zerm.zerm_mat,  SQLLONG, 0);
            sqlin ((short *)   &zerm.a_typ,  SQLSHORT, 0);
            
    sqlout ((short *) &zerm.mdn,SQLSHORT,0);
    sqlout ((long *) &zerm.schnitt,SQLLONG,0);
    sqlout ((long *) &zerm.zerm_aus_mat,SQLLONG,0);
    sqlout ((long *) &zerm.zerm_mat,SQLLONG,0);
    sqlout ((TCHAR *) zerm.a_kz,SQLCHAR,3);
    sqlout ((TCHAR *) zerm.fix,SQLCHAR,2);
    sqlout ((double *) &zerm.fwp,SQLDOUBLE,0);
    sqlout ((double *) &zerm.zerm_gew,SQLDOUBLE,0);
    sqlout ((double *) &zerm.zerm_gew_ant,SQLDOUBLE,0);
    sqlout ((TCHAR *) zerm.zerm_teil,SQLCHAR,2);
    sqlout ((double *) &zerm.mat_o_b,SQLDOUBLE,0);
    sqlout ((double *) &zerm.hk_vollk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.hk_teilk,SQLDOUBLE,0);
    sqlout ((short *) &zerm.delstatus,SQLSHORT,0);
    sqlout ((double *) &zerm.vollkosten_abs,SQLDOUBLE,0);
    sqlout ((double *) &zerm.vollkosten_proz,SQLDOUBLE,0);
    sqlout ((double *) &zerm.teilkosten_abs,SQLDOUBLE,0);
    sqlout ((double *) &zerm.teilkosten_proz,SQLDOUBLE,0);
    sqlout ((double *) &zerm.dm_100,SQLDOUBLE,0);
    sqlout ((long *) &zerm.a_z_f,SQLLONG,0);
    sqlout ((double *) &zerm.vk_pr,SQLDOUBLE,0);
    sqlout ((double *) &zerm.kosten_zutat,SQLDOUBLE,0);
    sqlout ((double *) &zerm.kosten_vpk,SQLDOUBLE,0);
    sqlout ((short *) &zerm.a_typ,SQLSHORT,0);
    sqlout ((TCHAR *) zerm.kost_bz,SQLCHAR,25);
    sqlout ((double *) &zerm.kosten,SQLDOUBLE,0);
    sqlout ((double *) &zerm.pr_zutat,SQLDOUBLE,0);
    sqlout ((double *) &zerm.pr_vpk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.pr_kosten,SQLDOUBLE,0);
    sqlout ((double *) &zerm.pr_vk_netto,SQLDOUBLE,0);
    sqlout ((double *) &zerm.kosten_fix,SQLDOUBLE,0);
    sqlout ((double *) &zerm.pr_vk_eh,SQLDOUBLE,0);
    sqlout ((double *) &zerm.kosten_gh,SQLDOUBLE,0);
    sqlout ((double *) &zerm.kosten_eh,SQLDOUBLE,0);
    sqlout ((short *) &zerm.lfd,SQLSHORT,0);
    sqlout ((short *) &zerm.list_lfd,SQLSHORT,0);
    sqlout ((short *) &zerm.list_ebene,SQLSHORT,0);
    sqlout ((short *) &zerm.zerm_aus_a_typ,SQLSHORT,0);
    sqlout ((double *) &zerm.fwp_org,SQLDOUBLE,0);
    sqlout ((double *) &zerm.sk_vollk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.sk_teilk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.fil_ek_vollk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.fil_ek_teilk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.fil_vk_vollk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.fil_vk_teilk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.gh1_vollk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.gh1_teilk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.gh2_vollk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.gh2_teilk,SQLDOUBLE,0);
            cursor = sqlcursor (_T("select  zerm.mdn,  ")
_T("zerm.schnitt,  zerm.zerm_aus_mat,  zerm.zerm_mat,  zerm.a_kz,  zerm.fix,  ")
_T("zerm.fwp,  zerm.zerm_gew,  zerm.zerm_gew_ant,  zerm.zerm_teil,  ")
_T("zerm.mat_o_b,  zerm.hk_vollk,  zerm.hk_teilk,  zerm.delstatus,  ")
_T("zerm.vollkosten_abs,  zerm.vollkosten_proz,  zerm.teilkosten_abs,  ")
_T("zerm.teilkosten_proz,  zerm.dm_100,  zerm.a_z_f,  zerm.vk_pr,  ")
_T("zerm.kosten_zutat,  zerm.kosten_vpk,  zerm.a_typ,  zerm.kost_bz,  ")
_T("zerm.kosten,  zerm.pr_zutat,  zerm.pr_vpk,  zerm.pr_kosten,  ")
_T("zerm.pr_vk_netto,  zerm.kosten_fix,  zerm.pr_vk_eh,  zerm.kosten_gh,  ")
_T("zerm.kosten_eh,  zerm.lfd,  zerm.list_lfd,  zerm.list_ebene,  ")
_T("zerm.zerm_aus_a_typ,  zerm.fwp_org,  zerm.sk_vollk,  zerm.sk_teilk,  ")
_T("zerm.fil_ek_vollk,  zerm.fil_ek_teilk,  zerm.fil_vk_vollk,  ")
_T("zerm.fil_vk_teilk,  zerm.gh1_vollk,  zerm.gh1_teilk,  zerm.gh2_vollk,  ")
_T("zerm.gh2_teilk from zerm ")

#line 17 "zerm.rpp"
                                _T("where zerm.mdn = ? ")
                                _T("and zerm.schnitt = ? ")
                                _T("and zerm.zerm_aus_mat = ? ")
			        _T("and zerm.zerm_mat = ? ")
			        _T("and zerm.a_typ = ?")
                                );
    sqlin ((short *) &zerm.mdn,SQLSHORT,0);
    sqlin ((long *) &zerm.schnitt,SQLLONG,0);
    sqlin ((long *) &zerm.zerm_aus_mat,SQLLONG,0);
    sqlin ((long *) &zerm.zerm_mat,SQLLONG,0);
    sqlin ((TCHAR *) zerm.a_kz,SQLCHAR,3);
    sqlin ((TCHAR *) zerm.fix,SQLCHAR,2);
    sqlin ((double *) &zerm.fwp,SQLDOUBLE,0);
    sqlin ((double *) &zerm.zerm_gew,SQLDOUBLE,0);
    sqlin ((double *) &zerm.zerm_gew_ant,SQLDOUBLE,0);
    sqlin ((TCHAR *) zerm.zerm_teil,SQLCHAR,2);
    sqlin ((double *) &zerm.mat_o_b,SQLDOUBLE,0);
    sqlin ((double *) &zerm.hk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &zerm.hk_teilk,SQLDOUBLE,0);
    sqlin ((short *) &zerm.delstatus,SQLSHORT,0);
    sqlin ((double *) &zerm.vollkosten_abs,SQLDOUBLE,0);
    sqlin ((double *) &zerm.vollkosten_proz,SQLDOUBLE,0);
    sqlin ((double *) &zerm.teilkosten_abs,SQLDOUBLE,0);
    sqlin ((double *) &zerm.teilkosten_proz,SQLDOUBLE,0);
    sqlin ((double *) &zerm.dm_100,SQLDOUBLE,0);
    sqlin ((long *) &zerm.a_z_f,SQLLONG,0);
    sqlin ((double *) &zerm.vk_pr,SQLDOUBLE,0);
    sqlin ((double *) &zerm.kosten_zutat,SQLDOUBLE,0);
    sqlin ((double *) &zerm.kosten_vpk,SQLDOUBLE,0);
    sqlin ((short *) &zerm.a_typ,SQLSHORT,0);
    sqlin ((TCHAR *) zerm.kost_bz,SQLCHAR,25);
    sqlin ((double *) &zerm.kosten,SQLDOUBLE,0);
    sqlin ((double *) &zerm.pr_zutat,SQLDOUBLE,0);
    sqlin ((double *) &zerm.pr_vpk,SQLDOUBLE,0);
    sqlin ((double *) &zerm.pr_kosten,SQLDOUBLE,0);
    sqlin ((double *) &zerm.pr_vk_netto,SQLDOUBLE,0);
    sqlin ((double *) &zerm.kosten_fix,SQLDOUBLE,0);
    sqlin ((double *) &zerm.pr_vk_eh,SQLDOUBLE,0);
    sqlin ((double *) &zerm.kosten_gh,SQLDOUBLE,0);
    sqlin ((double *) &zerm.kosten_eh,SQLDOUBLE,0);
    sqlin ((short *) &zerm.lfd,SQLSHORT,0);
    sqlin ((short *) &zerm.list_lfd,SQLSHORT,0);
    sqlin ((short *) &zerm.list_ebene,SQLSHORT,0);
    sqlin ((short *) &zerm.zerm_aus_a_typ,SQLSHORT,0);
    sqlin ((double *) &zerm.fwp_org,SQLDOUBLE,0);
    sqlin ((double *) &zerm.sk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &zerm.sk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &zerm.fil_ek_vollk,SQLDOUBLE,0);
    sqlin ((double *) &zerm.fil_ek_teilk,SQLDOUBLE,0);
    sqlin ((double *) &zerm.fil_vk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &zerm.fil_vk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &zerm.gh1_vollk,SQLDOUBLE,0);
    sqlin ((double *) &zerm.gh1_teilk,SQLDOUBLE,0);
    sqlin ((double *) &zerm.gh2_vollk,SQLDOUBLE,0);
    sqlin ((double *) &zerm.gh2_teilk,SQLDOUBLE,0);
            sqltext = _T("update zerm set zerm.mdn = ?,  ")
_T("zerm.schnitt = ?,  zerm.zerm_aus_mat = ?,  zerm.zerm_mat = ?,  ")
_T("zerm.a_kz = ?,  zerm.fix = ?,  zerm.fwp = ?,  zerm.zerm_gew = ?,  ")
_T("zerm.zerm_gew_ant = ?,  zerm.zerm_teil = ?,  zerm.mat_o_b = ?,  ")
_T("zerm.hk_vollk = ?,  zerm.hk_teilk = ?,  zerm.delstatus = ?,  ")
_T("zerm.vollkosten_abs = ?,  zerm.vollkosten_proz = ?,  ")
_T("zerm.teilkosten_abs = ?,  zerm.teilkosten_proz = ?,  ")
_T("zerm.dm_100 = ?,  zerm.a_z_f = ?,  zerm.vk_pr = ?,  ")
_T("zerm.kosten_zutat = ?,  zerm.kosten_vpk = ?,  zerm.a_typ = ?,  ")
_T("zerm.kost_bz = ?,  zerm.kosten = ?,  zerm.pr_zutat = ?,  ")
_T("zerm.pr_vpk = ?,  zerm.pr_kosten = ?,  zerm.pr_vk_netto = ?,  ")
_T("zerm.kosten_fix = ?,  zerm.pr_vk_eh = ?,  zerm.kosten_gh = ?,  ")
_T("zerm.kosten_eh = ?,  zerm.lfd = ?,  zerm.list_lfd = ?,  ")
_T("zerm.list_ebene = ?,  zerm.zerm_aus_a_typ = ?,  zerm.fwp_org = ?,  ")
_T("zerm.sk_vollk = ?,  zerm.sk_teilk = ?,  zerm.fil_ek_vollk = ?,  ")
_T("zerm.fil_ek_teilk = ?,  zerm.fil_vk_vollk = ?,  ")
_T("zerm.fil_vk_teilk = ?,  zerm.gh1_vollk = ?,  zerm.gh1_teilk = ?,  ")
_T("zerm.gh2_vollk = ?,  zerm.gh2_teilk = ? ")

#line 24 "zerm.rpp"
                      _T("where mdn = ? ")
			          _T("and schnitt = ? ")
                      _T("and zerm_aus_mat = ? ")
			          _T("and zerm_mat = ? ")
			          _T("and a_typ = ?");
            sqlin ((short *)   &zerm.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &zerm.schnitt,  SQLLONG, 0);
            sqlin ((long *)   &zerm.zerm_aus_mat,  SQLLONG, 0);
            sqlin ((long *)   &zerm.zerm_mat,  SQLLONG, 0);
            sqlin ((short *)   &zerm.a_typ,  SQLSHORT, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &zerm.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &zerm.schnitt,  SQLLONG, 0);
            sqlin ((long *)   &zerm.zerm_aus_mat,  SQLLONG, 0);
            sqlin ((long *)   &zerm.zerm_mat,  SQLLONG, 0);
            sqlin ((short *)   &zerm.a_typ,  SQLSHORT, 0);
            test_upd_cursor = sqlcursor (_T("select schnitt from zerm ")
                                _T("where mdn = ? ")
			        _T("and schnitt = ? ")
                                _T("and zerm_aus_mat = ? ")
			        _T("and zerm_mat = ? ")
			        _T("and a_typ = ?"));
            sqlin ((short *)   &zerm.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &zerm.schnitt,  SQLLONG, 0);
            sqlin ((long *)   &zerm.zerm_aus_mat,  SQLLONG, 0);
            sqlin ((long *)   &zerm.zerm_mat,  SQLLONG, 0);
            sqlin ((short *)   &zerm.a_typ,  SQLSHORT, 0);
            del_cursor = sqlcursor (_T("delete from zerm ")
                                _T("where mdn = ? ")
			        _T("and schnitt = ? ")
                                _T("and zerm_aus_mat = ? ")
			        _T("and zerm_mat = ? ")
			        _T("and a_typ = ?"));
    sqlin ((short *) &zerm.mdn,SQLSHORT,0);
    sqlin ((long *) &zerm.schnitt,SQLLONG,0);
    sqlin ((long *) &zerm.zerm_aus_mat,SQLLONG,0);
    sqlin ((long *) &zerm.zerm_mat,SQLLONG,0);
    sqlin ((TCHAR *) zerm.a_kz,SQLCHAR,3);
    sqlin ((TCHAR *) zerm.fix,SQLCHAR,2);
    sqlin ((double *) &zerm.fwp,SQLDOUBLE,0);
    sqlin ((double *) &zerm.zerm_gew,SQLDOUBLE,0);
    sqlin ((double *) &zerm.zerm_gew_ant,SQLDOUBLE,0);
    sqlin ((TCHAR *) zerm.zerm_teil,SQLCHAR,2);
    sqlin ((double *) &zerm.mat_o_b,SQLDOUBLE,0);
    sqlin ((double *) &zerm.hk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &zerm.hk_teilk,SQLDOUBLE,0);
    sqlin ((short *) &zerm.delstatus,SQLSHORT,0);
    sqlin ((double *) &zerm.vollkosten_abs,SQLDOUBLE,0);
    sqlin ((double *) &zerm.vollkosten_proz,SQLDOUBLE,0);
    sqlin ((double *) &zerm.teilkosten_abs,SQLDOUBLE,0);
    sqlin ((double *) &zerm.teilkosten_proz,SQLDOUBLE,0);
    sqlin ((double *) &zerm.dm_100,SQLDOUBLE,0);
    sqlin ((long *) &zerm.a_z_f,SQLLONG,0);
    sqlin ((double *) &zerm.vk_pr,SQLDOUBLE,0);
    sqlin ((double *) &zerm.kosten_zutat,SQLDOUBLE,0);
    sqlin ((double *) &zerm.kosten_vpk,SQLDOUBLE,0);
    sqlin ((short *) &zerm.a_typ,SQLSHORT,0);
    sqlin ((TCHAR *) zerm.kost_bz,SQLCHAR,25);
    sqlin ((double *) &zerm.kosten,SQLDOUBLE,0);
    sqlin ((double *) &zerm.pr_zutat,SQLDOUBLE,0);
    sqlin ((double *) &zerm.pr_vpk,SQLDOUBLE,0);
    sqlin ((double *) &zerm.pr_kosten,SQLDOUBLE,0);
    sqlin ((double *) &zerm.pr_vk_netto,SQLDOUBLE,0);
    sqlin ((double *) &zerm.kosten_fix,SQLDOUBLE,0);
    sqlin ((double *) &zerm.pr_vk_eh,SQLDOUBLE,0);
    sqlin ((double *) &zerm.kosten_gh,SQLDOUBLE,0);
    sqlin ((double *) &zerm.kosten_eh,SQLDOUBLE,0);
    sqlin ((short *) &zerm.lfd,SQLSHORT,0);
    sqlin ((short *) &zerm.list_lfd,SQLSHORT,0);
    sqlin ((short *) &zerm.list_ebene,SQLSHORT,0);
    sqlin ((short *) &zerm.zerm_aus_a_typ,SQLSHORT,0);
    sqlin ((double *) &zerm.fwp_org,SQLDOUBLE,0);
    sqlin ((double *) &zerm.sk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &zerm.sk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &zerm.fil_ek_vollk,SQLDOUBLE,0);
    sqlin ((double *) &zerm.fil_ek_teilk,SQLDOUBLE,0);
    sqlin ((double *) &zerm.fil_vk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &zerm.fil_vk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &zerm.gh1_vollk,SQLDOUBLE,0);
    sqlin ((double *) &zerm.gh1_teilk,SQLDOUBLE,0);
    sqlin ((double *) &zerm.gh2_vollk,SQLDOUBLE,0);
    sqlin ((double *) &zerm.gh2_teilk,SQLDOUBLE,0);
            ins_cursor = sqlcursor (_T("insert into zerm (")
_T("mdn,  schnitt,  zerm_aus_mat,  zerm_mat,  a_kz,  fix,  fwp,  zerm_gew,  zerm_gew_ant,  ")
_T("zerm_teil,  mat_o_b,  hk_vollk,  hk_teilk,  delstatus,  vollkosten_abs,  ")
_T("vollkosten_proz,  teilkosten_abs,  teilkosten_proz,  dm_100,  a_z_f,  vk_pr,  ")
_T("kosten_zutat,  kosten_vpk,  a_typ,  kost_bz,  kosten,  pr_zutat,  pr_vpk,  ")
_T("pr_kosten,  pr_vk_netto,  kosten_fix,  pr_vk_eh,  kosten_gh,  kosten_eh,  lfd,  ")
_T("list_lfd,  list_ebene,  zerm_aus_a_typ,  fwp_org,  sk_vollk,  sk_teilk,  ")
_T("fil_ek_vollk,  fil_ek_teilk,  fil_vk_vollk,  fil_vk_teilk,  gh1_vollk,  ")
_T("gh1_teilk,  gh2_vollk,  gh2_teilk) ")

#line 59 "zerm.rpp"
                                    _T("values ")
                                    _T("(?,?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?)")); 

#line 61 "zerm.rpp"
            sqlin ((short *)   &zerm.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &zerm.schnitt,  SQLLONG, 0);
            sqlin ((long *)   &zerm.zerm_aus_mat,  SQLLONG, 0);
            sqlin ((short *)   &zerm.zerm_aus_a_typ,  SQLSHORT, 0);
            sqlin ((short *)   &zerm.zerm_aus_a_typ,  SQLSHORT, 0);
    sqlout ((short *) &zerm.mdn,SQLSHORT,0);
    sqlout ((long *) &zerm.schnitt,SQLLONG,0);
    sqlout ((long *) &zerm.zerm_aus_mat,SQLLONG,0);
    sqlout ((long *) &zerm.zerm_mat,SQLLONG,0);
    sqlout ((TCHAR *) zerm.a_kz,SQLCHAR,3);
    sqlout ((TCHAR *) zerm.fix,SQLCHAR,2);
    sqlout ((double *) &zerm.fwp,SQLDOUBLE,0);
    sqlout ((double *) &zerm.zerm_gew,SQLDOUBLE,0);
    sqlout ((double *) &zerm.zerm_gew_ant,SQLDOUBLE,0);
    sqlout ((TCHAR *) zerm.zerm_teil,SQLCHAR,2);
    sqlout ((double *) &zerm.mat_o_b,SQLDOUBLE,0);
    sqlout ((double *) &zerm.hk_vollk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.hk_teilk,SQLDOUBLE,0);
    sqlout ((short *) &zerm.delstatus,SQLSHORT,0);
    sqlout ((double *) &zerm.vollkosten_abs,SQLDOUBLE,0);
    sqlout ((double *) &zerm.vollkosten_proz,SQLDOUBLE,0);
    sqlout ((double *) &zerm.teilkosten_abs,SQLDOUBLE,0);
    sqlout ((double *) &zerm.teilkosten_proz,SQLDOUBLE,0);
    sqlout ((double *) &zerm.dm_100,SQLDOUBLE,0);
    sqlout ((long *) &zerm.a_z_f,SQLLONG,0);
    sqlout ((double *) &zerm.vk_pr,SQLDOUBLE,0);
    sqlout ((double *) &zerm.kosten_zutat,SQLDOUBLE,0);
    sqlout ((double *) &zerm.kosten_vpk,SQLDOUBLE,0);
    sqlout ((short *) &zerm.a_typ,SQLSHORT,0);
    sqlout ((TCHAR *) zerm.kost_bz,SQLCHAR,25);
    sqlout ((double *) &zerm.kosten,SQLDOUBLE,0);
    sqlout ((double *) &zerm.pr_zutat,SQLDOUBLE,0);
    sqlout ((double *) &zerm.pr_vpk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.pr_kosten,SQLDOUBLE,0);
    sqlout ((double *) &zerm.pr_vk_netto,SQLDOUBLE,0);
    sqlout ((double *) &zerm.kosten_fix,SQLDOUBLE,0);
    sqlout ((double *) &zerm.pr_vk_eh,SQLDOUBLE,0);
    sqlout ((double *) &zerm.kosten_gh,SQLDOUBLE,0);
    sqlout ((double *) &zerm.kosten_eh,SQLDOUBLE,0);
    sqlout ((short *) &zerm.lfd,SQLSHORT,0);
    sqlout ((short *) &zerm.list_lfd,SQLSHORT,0);
    sqlout ((short *) &zerm.list_ebene,SQLSHORT,0);
    sqlout ((short *) &zerm.zerm_aus_a_typ,SQLSHORT,0);
    sqlout ((double *) &zerm.fwp_org,SQLDOUBLE,0);
    sqlout ((double *) &zerm.sk_vollk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.sk_teilk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.fil_ek_vollk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.fil_ek_teilk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.fil_vk_vollk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.fil_vk_teilk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.gh1_vollk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.gh1_teilk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.gh2_vollk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.gh2_teilk,SQLDOUBLE,0);
            AusMatCursor = sqlcursor (_T("select zerm.mdn,  ")
_T("zerm.schnitt,  zerm.zerm_aus_mat,  zerm.zerm_mat,  zerm.a_kz,  zerm.fix,  ")
_T("zerm.fwp,  zerm.zerm_gew,  zerm.zerm_gew_ant,  zerm.zerm_teil,  ")
_T("zerm.mat_o_b,  zerm.hk_vollk,  zerm.hk_teilk,  zerm.delstatus,  ")
_T("zerm.vollkosten_abs,  zerm.vollkosten_proz,  zerm.teilkosten_abs,  ")
_T("zerm.teilkosten_proz,  zerm.dm_100,  zerm.a_z_f,  zerm.vk_pr,  ")
_T("zerm.kosten_zutat,  zerm.kosten_vpk,  zerm.a_typ,  zerm.kost_bz,  ")
_T("zerm.kosten,  zerm.pr_zutat,  zerm.pr_vpk,  zerm.pr_kosten,  ")
_T("zerm.pr_vk_netto,  zerm.kosten_fix,  zerm.pr_vk_eh,  zerm.kosten_gh,  ")
_T("zerm.kosten_eh,  zerm.lfd,  zerm.list_lfd,  zerm.list_ebene,  ")
_T("zerm.zerm_aus_a_typ,  zerm.fwp_org,  zerm.sk_vollk,  zerm.sk_teilk,  ")
_T("zerm.fil_ek_vollk,  zerm.fil_ek_teilk,  zerm.fil_vk_vollk,  ")
_T("zerm.fil_vk_teilk,  zerm.gh1_vollk,  zerm.gh1_teilk,  zerm.gh2_vollk,  ")
_T("zerm.gh2_teilk from zerm ")

#line 67 "zerm.rpp"
                                _T("where zerm.mdn = ? ")
			        _T("and zerm.schnitt = ? ")
			        _T("and not (zerm.zerm_aus_mat = zerm.zerm_mat and (zerm.a_typ = 1 or zerm.zerm_aus_a_typ = 0)) ")
                                _T("and zerm.zerm_aus_mat = ? ")
                                _T("and (zerm.zerm_aus_a_typ = ? or zerm.zerm_aus_a_typ = 0 or ? = 0) ")
                                _T("order by a_typ, zerm_mat ")
                                );
            sqlin ((short *)   &zerm.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &zerm.schnitt,  SQLLONG, 0);
            
    sqlout ((short *) &zerm.mdn,SQLSHORT,0);
    sqlout ((long *) &zerm.schnitt,SQLLONG,0);
    sqlout ((long *) &zerm.zerm_aus_mat,SQLLONG,0);
    sqlout ((long *) &zerm.zerm_mat,SQLLONG,0);
    sqlout ((TCHAR *) zerm.a_kz,SQLCHAR,3);
    sqlout ((TCHAR *) zerm.fix,SQLCHAR,2);
    sqlout ((double *) &zerm.fwp,SQLDOUBLE,0);
    sqlout ((double *) &zerm.zerm_gew,SQLDOUBLE,0);
    sqlout ((double *) &zerm.zerm_gew_ant,SQLDOUBLE,0);
    sqlout ((TCHAR *) zerm.zerm_teil,SQLCHAR,2);
    sqlout ((double *) &zerm.mat_o_b,SQLDOUBLE,0);
    sqlout ((double *) &zerm.hk_vollk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.hk_teilk,SQLDOUBLE,0);
    sqlout ((short *) &zerm.delstatus,SQLSHORT,0);
    sqlout ((double *) &zerm.vollkosten_abs,SQLDOUBLE,0);
    sqlout ((double *) &zerm.vollkosten_proz,SQLDOUBLE,0);
    sqlout ((double *) &zerm.teilkosten_abs,SQLDOUBLE,0);
    sqlout ((double *) &zerm.teilkosten_proz,SQLDOUBLE,0);
    sqlout ((double *) &zerm.dm_100,SQLDOUBLE,0);
    sqlout ((long *) &zerm.a_z_f,SQLLONG,0);
    sqlout ((double *) &zerm.vk_pr,SQLDOUBLE,0);
    sqlout ((double *) &zerm.kosten_zutat,SQLDOUBLE,0);
    sqlout ((double *) &zerm.kosten_vpk,SQLDOUBLE,0);
    sqlout ((short *) &zerm.a_typ,SQLSHORT,0);
    sqlout ((TCHAR *) zerm.kost_bz,SQLCHAR,25);
    sqlout ((double *) &zerm.kosten,SQLDOUBLE,0);
    sqlout ((double *) &zerm.pr_zutat,SQLDOUBLE,0);
    sqlout ((double *) &zerm.pr_vpk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.pr_kosten,SQLDOUBLE,0);
    sqlout ((double *) &zerm.pr_vk_netto,SQLDOUBLE,0);
    sqlout ((double *) &zerm.kosten_fix,SQLDOUBLE,0);
    sqlout ((double *) &zerm.pr_vk_eh,SQLDOUBLE,0);
    sqlout ((double *) &zerm.kosten_gh,SQLDOUBLE,0);
    sqlout ((double *) &zerm.kosten_eh,SQLDOUBLE,0);
    sqlout ((short *) &zerm.lfd,SQLSHORT,0);
    sqlout ((short *) &zerm.list_lfd,SQLSHORT,0);
    sqlout ((short *) &zerm.list_ebene,SQLSHORT,0);
    sqlout ((short *) &zerm.zerm_aus_a_typ,SQLSHORT,0);
    sqlout ((double *) &zerm.fwp_org,SQLDOUBLE,0);
    sqlout ((double *) &zerm.sk_vollk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.sk_teilk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.fil_ek_vollk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.fil_ek_teilk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.fil_vk_vollk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.fil_vk_teilk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.gh1_vollk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.gh1_teilk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.gh2_vollk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.gh2_teilk,SQLDOUBLE,0);
            SchnittCursor = sqlcursor (_T("select zerm.mdn,  ")
_T("zerm.schnitt,  zerm.zerm_aus_mat,  zerm.zerm_mat,  zerm.a_kz,  zerm.fix,  ")
_T("zerm.fwp,  zerm.zerm_gew,  zerm.zerm_gew_ant,  zerm.zerm_teil,  ")
_T("zerm.mat_o_b,  zerm.hk_vollk,  zerm.hk_teilk,  zerm.delstatus,  ")
_T("zerm.vollkosten_abs,  zerm.vollkosten_proz,  zerm.teilkosten_abs,  ")
_T("zerm.teilkosten_proz,  zerm.dm_100,  zerm.a_z_f,  zerm.vk_pr,  ")
_T("zerm.kosten_zutat,  zerm.kosten_vpk,  zerm.a_typ,  zerm.kost_bz,  ")
_T("zerm.kosten,  zerm.pr_zutat,  zerm.pr_vpk,  zerm.pr_kosten,  ")
_T("zerm.pr_vk_netto,  zerm.kosten_fix,  zerm.pr_vk_eh,  zerm.kosten_gh,  ")
_T("zerm.kosten_eh,  zerm.lfd,  zerm.list_lfd,  zerm.list_ebene,  ")
_T("zerm.zerm_aus_a_typ,  zerm.fwp_org,  zerm.sk_vollk,  zerm.sk_teilk,  ")
_T("zerm.fil_ek_vollk,  zerm.fil_ek_teilk,  zerm.fil_vk_vollk,  ")
_T("zerm.fil_vk_teilk,  zerm.gh1_vollk,  zerm.gh1_teilk,  zerm.gh2_vollk,  ")
_T("zerm.gh2_teilk from zerm ")

#line 78 "zerm.rpp"
                                _T("where zerm.mdn = ? ")
                                _T("and zerm.schnitt = ? order by list_lfd"));
                                
            sqlin ((short *)   &zerm.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &zerm.schnitt,  SQLLONG, 0);
            
    sqlout ((short *) &zerm.mdn,SQLSHORT,0);
    sqlout ((long *) &zerm.schnitt,SQLLONG,0);
    sqlout ((long *) &zerm.zerm_aus_mat,SQLLONG,0);
    sqlout ((long *) &zerm.zerm_mat,SQLLONG,0);
    sqlout ((TCHAR *) zerm.a_kz,SQLCHAR,3);
    sqlout ((TCHAR *) zerm.fix,SQLCHAR,2);
    sqlout ((double *) &zerm.fwp,SQLDOUBLE,0);
    sqlout ((double *) &zerm.zerm_gew,SQLDOUBLE,0);
    sqlout ((double *) &zerm.zerm_gew_ant,SQLDOUBLE,0);
    sqlout ((TCHAR *) zerm.zerm_teil,SQLCHAR,2);
    sqlout ((double *) &zerm.mat_o_b,SQLDOUBLE,0);
    sqlout ((double *) &zerm.hk_vollk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.hk_teilk,SQLDOUBLE,0);
    sqlout ((short *) &zerm.delstatus,SQLSHORT,0);
    sqlout ((double *) &zerm.vollkosten_abs,SQLDOUBLE,0);
    sqlout ((double *) &zerm.vollkosten_proz,SQLDOUBLE,0);
    sqlout ((double *) &zerm.teilkosten_abs,SQLDOUBLE,0);
    sqlout ((double *) &zerm.teilkosten_proz,SQLDOUBLE,0);
    sqlout ((double *) &zerm.dm_100,SQLDOUBLE,0);
    sqlout ((long *) &zerm.a_z_f,SQLLONG,0);
    sqlout ((double *) &zerm.vk_pr,SQLDOUBLE,0);
    sqlout ((double *) &zerm.kosten_zutat,SQLDOUBLE,0);
    sqlout ((double *) &zerm.kosten_vpk,SQLDOUBLE,0);
    sqlout ((short *) &zerm.a_typ,SQLSHORT,0);
    sqlout ((TCHAR *) zerm.kost_bz,SQLCHAR,25);
    sqlout ((double *) &zerm.kosten,SQLDOUBLE,0);
    sqlout ((double *) &zerm.pr_zutat,SQLDOUBLE,0);
    sqlout ((double *) &zerm.pr_vpk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.pr_kosten,SQLDOUBLE,0);
    sqlout ((double *) &zerm.pr_vk_netto,SQLDOUBLE,0);
    sqlout ((double *) &zerm.kosten_fix,SQLDOUBLE,0);
    sqlout ((double *) &zerm.pr_vk_eh,SQLDOUBLE,0);
    sqlout ((double *) &zerm.kosten_gh,SQLDOUBLE,0);
    sqlout ((double *) &zerm.kosten_eh,SQLDOUBLE,0);
    sqlout ((short *) &zerm.lfd,SQLSHORT,0);
    sqlout ((short *) &zerm.list_lfd,SQLSHORT,0);
    sqlout ((short *) &zerm.list_ebene,SQLSHORT,0);
    sqlout ((short *) &zerm.zerm_aus_a_typ,SQLSHORT,0);
    sqlout ((double *) &zerm.fwp_org,SQLDOUBLE,0);
    sqlout ((double *) &zerm.sk_vollk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.sk_teilk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.fil_ek_vollk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.fil_ek_teilk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.fil_vk_vollk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.fil_vk_teilk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.gh1_vollk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.gh1_teilk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.gh2_vollk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.gh2_teilk,SQLDOUBLE,0);
            GTCursor = sqlcursor (_T("select zerm.mdn,  ")
_T("zerm.schnitt,  zerm.zerm_aus_mat,  zerm.zerm_mat,  zerm.a_kz,  zerm.fix,  ")
_T("zerm.fwp,  zerm.zerm_gew,  zerm.zerm_gew_ant,  zerm.zerm_teil,  ")
_T("zerm.mat_o_b,  zerm.hk_vollk,  zerm.hk_teilk,  zerm.delstatus,  ")
_T("zerm.vollkosten_abs,  zerm.vollkosten_proz,  zerm.teilkosten_abs,  ")
_T("zerm.teilkosten_proz,  zerm.dm_100,  zerm.a_z_f,  zerm.vk_pr,  ")
_T("zerm.kosten_zutat,  zerm.kosten_vpk,  zerm.a_typ,  zerm.kost_bz,  ")
_T("zerm.kosten,  zerm.pr_zutat,  zerm.pr_vpk,  zerm.pr_kosten,  ")
_T("zerm.pr_vk_netto,  zerm.kosten_fix,  zerm.pr_vk_eh,  zerm.kosten_gh,  ")
_T("zerm.kosten_eh,  zerm.lfd,  zerm.list_lfd,  zerm.list_ebene,  ")
_T("zerm.zerm_aus_a_typ,  zerm.fwp_org,  zerm.sk_vollk,  zerm.sk_teilk,  ")
_T("zerm.fil_ek_vollk,  zerm.fil_ek_teilk,  zerm.fil_vk_vollk,  ")
_T("zerm.fil_vk_teilk,  zerm.gh1_vollk,  zerm.gh1_teilk,  zerm.gh2_vollk,  ")
_T("zerm.gh2_teilk from zerm ")

#line 85 "zerm.rpp"
                                _T("where zerm.mdn = ? ")
                                _T("and zerm.schnitt = ? and zerm_teil = \"1\" "));
                                
// ATypVKCursor:  selektiert alle DS, bei denen zerm_aus_a_typ auf VK_ARTIKEL (10) gesetzt werden muss ,damit alle Kosten etc unterhalb von dem VK-Artikel kommen                               
            sqlin ((short *)   &zerm.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &zerm.schnitt,  SQLLONG, 0);
            sqlin ((short *)   &zerm.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &zerm.schnitt,  SQLLONG, 0);
    sqlout ((short *) &zerm.mdn,SQLSHORT,0);
    sqlout ((long *) &zerm.schnitt,SQLLONG,0);
    sqlout ((long *) &zerm.zerm_aus_mat,SQLLONG,0);
    sqlout ((long *) &zerm.zerm_mat,SQLLONG,0);
    sqlout ((TCHAR *) zerm.a_kz,SQLCHAR,3);
    sqlout ((TCHAR *) zerm.fix,SQLCHAR,2);
    sqlout ((double *) &zerm.fwp,SQLDOUBLE,0);
    sqlout ((double *) &zerm.zerm_gew,SQLDOUBLE,0);
    sqlout ((double *) &zerm.zerm_gew_ant,SQLDOUBLE,0);
    sqlout ((TCHAR *) zerm.zerm_teil,SQLCHAR,2);
    sqlout ((double *) &zerm.mat_o_b,SQLDOUBLE,0);
    sqlout ((double *) &zerm.hk_vollk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.hk_teilk,SQLDOUBLE,0);
    sqlout ((short *) &zerm.delstatus,SQLSHORT,0);
    sqlout ((double *) &zerm.vollkosten_abs,SQLDOUBLE,0);
    sqlout ((double *) &zerm.vollkosten_proz,SQLDOUBLE,0);
    sqlout ((double *) &zerm.teilkosten_abs,SQLDOUBLE,0);
    sqlout ((double *) &zerm.teilkosten_proz,SQLDOUBLE,0);
    sqlout ((double *) &zerm.dm_100,SQLDOUBLE,0);
    sqlout ((long *) &zerm.a_z_f,SQLLONG,0);
    sqlout ((double *) &zerm.vk_pr,SQLDOUBLE,0);
    sqlout ((double *) &zerm.kosten_zutat,SQLDOUBLE,0);
    sqlout ((double *) &zerm.kosten_vpk,SQLDOUBLE,0);
    sqlout ((short *) &zerm.a_typ,SQLSHORT,0);
    sqlout ((TCHAR *) zerm.kost_bz,SQLCHAR,25);
    sqlout ((double *) &zerm.kosten,SQLDOUBLE,0);
    sqlout ((double *) &zerm.pr_zutat,SQLDOUBLE,0);
    sqlout ((double *) &zerm.pr_vpk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.pr_kosten,SQLDOUBLE,0);
    sqlout ((double *) &zerm.pr_vk_netto,SQLDOUBLE,0);
    sqlout ((double *) &zerm.kosten_fix,SQLDOUBLE,0);
    sqlout ((double *) &zerm.pr_vk_eh,SQLDOUBLE,0);
    sqlout ((double *) &zerm.kosten_gh,SQLDOUBLE,0);
    sqlout ((double *) &zerm.kosten_eh,SQLDOUBLE,0);
    sqlout ((short *) &zerm.lfd,SQLSHORT,0);
    sqlout ((short *) &zerm.list_lfd,SQLSHORT,0);
    sqlout ((short *) &zerm.list_ebene,SQLSHORT,0);
    sqlout ((short *) &zerm.zerm_aus_a_typ,SQLSHORT,0);
    sqlout ((double *) &zerm.fwp_org,SQLDOUBLE,0);
    sqlout ((double *) &zerm.sk_vollk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.sk_teilk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.fil_ek_vollk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.fil_ek_teilk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.fil_vk_vollk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.fil_vk_teilk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.gh1_vollk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.gh1_teilk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.gh2_vollk,SQLDOUBLE,0);
    sqlout ((double *) &zerm.gh2_teilk,SQLDOUBLE,0);
            ATypVKCursor = sqlcursor (_T("select zerm.mdn,  ")
_T("zerm.schnitt,  zerm.zerm_aus_mat,  zerm.zerm_mat,  zerm.a_kz,  zerm.fix,  ")
_T("zerm.fwp,  zerm.zerm_gew,  zerm.zerm_gew_ant,  zerm.zerm_teil,  ")
_T("zerm.mat_o_b,  zerm.hk_vollk,  zerm.hk_teilk,  zerm.delstatus,  ")
_T("zerm.vollkosten_abs,  zerm.vollkosten_proz,  zerm.teilkosten_abs,  ")
_T("zerm.teilkosten_proz,  zerm.dm_100,  zerm.a_z_f,  zerm.vk_pr,  ")
_T("zerm.kosten_zutat,  zerm.kosten_vpk,  zerm.a_typ,  zerm.kost_bz,  ")
_T("zerm.kosten,  zerm.pr_zutat,  zerm.pr_vpk,  zerm.pr_kosten,  ")
_T("zerm.pr_vk_netto,  zerm.kosten_fix,  zerm.pr_vk_eh,  zerm.kosten_gh,  ")
_T("zerm.kosten_eh,  zerm.lfd,  zerm.list_lfd,  zerm.list_ebene,  ")
_T("zerm.zerm_aus_a_typ,  zerm.fwp_org,  zerm.sk_vollk,  zerm.sk_teilk,  ")
_T("zerm.fil_ek_vollk,  zerm.fil_ek_teilk,  zerm.fil_vk_vollk,  ")
_T("zerm.fil_vk_teilk,  zerm.gh1_vollk,  zerm.gh1_teilk,  zerm.gh2_vollk,  ")
_T("zerm.gh2_teilk from zerm where zerm_aus_mat in")

#line 94 "zerm.rpp"
                                _T("(select zerm_mat from zerm where mdn = ? and schnitt = ? and a_typ = 10) ")
			        _T("and a_typ != 1 and a_typ != 10 and zerm_aus_a_typ != 10 and mdn = ? and schnitt = ?")
                                );
                                
}

int ZERM_CLASS::dbreadfirst_AusMat ()
{
	   if (AusMatCursor == -1)
           {
		prepare ();	
           }
	   if (AusMatCursor == -1) return -1;
           int dsqlstatus = sqlopen (AusMatCursor);
           if (dsqlstatus != 0) return dsqlstatus;
           return sqlfetch (AusMatCursor); 
}

int ZERM_CLASS::dbread_AusMat ()
{
	   if (AusMatCursor == -1) return -1;
           return sqlfetch (AusMatCursor); 
}
int ZERM_CLASS::dbreadfirst_ATypVK ()
{
	   if (ATypVKCursor == -1)
           {
		prepare ();	
           }
	   if (ATypVKCursor == -1) return -1;
           int dsqlstatus = sqlopen (ATypVKCursor);
           if (dsqlstatus != 0) return dsqlstatus;
           return sqlfetch (ATypVKCursor); 
}

int ZERM_CLASS::dbread_ATypVK ()
{
	   if (ATypVKCursor == -1) return -1;
           return sqlfetch (ATypVKCursor); 
}
int ZERM_CLASS::dbreadfirst_Schnitt ()
{
	   if (SchnittCursor == -1)
           {
		prepare ();	
           }
	   if (SchnittCursor == -1) return -1;
           int dsqlstatus = sqlopen (SchnittCursor);
           if (dsqlstatus != 0) return dsqlstatus;
           return sqlfetch (SchnittCursor); 
}

int ZERM_CLASS::dbread_Schnitt ()
{
	   if (SchnittCursor == -1) return -1;
           return sqlfetch (SchnittCursor); 
}
int ZERM_CLASS::dbreadfirst_GT ()
{
	   if (GTCursor == -1)
           {
		prepare ();	
           }
	   if (GTCursor == -1) return -1;
           int dsqlstatus = sqlopen (GTCursor);
           if (dsqlstatus != 0) return dsqlstatus;
           return sqlfetch (GTCursor); 
}

int ZERM_CLASS::dbread_GT ()
{
	   if (GTCursor == -1) return -1;
           return sqlfetch (GTCursor); 
}

/**
int ZERM_CLASS::dbupdate ()
{
	CString CFix;
	CFix.Format (_T("%s"),zerm.fix);
	if (CFix == "N") _tcscpy (zerm.fix, "F");
	if (CFix == "J") _tcscpy (zerm.fix, "V");
	 
	return DB_CLASS::dbupdate();
}
****/
