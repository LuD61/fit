#ifndef _WORKDB_DEF
#define _WORKDB_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"
#include "DbUniCode.h"
#include "zerm_mat_v.h"
#include "a_bas.h"
#include "a_mat.h"
#include "a_varb.h"
#include "a_zut.h"
#include "a_kalkpreis.h"
#include "a_kalk_mat.h"
#include "a_kalk_eig.h"
#include "a_kalkn.h"
#include "a_kalkhndw.h"
#include "a_schl.h"
#include "schlaklkk.h"
#include "zerm.h"
#include "kost_art_zerl.h"
#include "schnitt.h"
#include "StrFuncs.h"
#include "token.h"
#include "ipr.h"

class WORKDB_CLASS : public DB_CLASS  
{
       private :
		   int MatCursor;
		   int HoleAKalkHndw (short mdn, double a, int PreiseAus_a_kalkpreis);
		   int HoleAKalkMat (short mdn, long mat,int PreiseAus_a_kalkpreis);
		   BOOL InsZerm (long aus_mat);
		   double HoleStammDatenPreis (short a_typ,short mdn, long mat, int PreiseAus_a_kalkpreis);
       public :
               WORKDB_CLASS () : DB_CLASS ()
               {
//					MatCursor = -1;	
				   Schnitt.schnitt.ek = 0.0;
				   Schnitt.schnitt.ek_pr = 0.0;
               }
               ~WORKDB_CLASS ()
               {
//				   if (MatCursor != -1)
//			       {
  //                         sqlclose (MatCursor);
	//			   }		
               }
			   double HoleMatoB (short mdn, long mat,int PreiseAus_a_kalkpreis, int kalk_bas, int PrGrStuf);
			   double HoleFWP (long mat,double FWP);
			   char* HoleA_Kz (long mat,short a_typ,char* a_kz);
			   int UpdateZermList (short dmdn,long dschnitt);
               BOOL CopySchnitt (short mdn_alt,long schnitt_alt);  
               BOOL RenameSchnitt (short mdn,long schnitt_alt, long schnitt_neu);  
               int DelZerm (short mdn,long schnitt);  
               int DelZermMatV (short mdn,long schnitt);  
               long ZerlMat (void);  
			   BOOL DelZermMatV (short mdn,
								   long schnitt,
								   long zerm_aus_mat,
								   long zerm_mat,
								   short a_typ);
			   BOOL DelZermMatV (short mdn,
								   long schnitt,
								   long zerm_mat);
			   int ReadMaterial (short mdn, long mat, short a_typ, int PreiseAus_a_kalkpreis);
			   int VKVorhanden (short mdn, long schnitt, long aus_mat);
			   int SetzeDefault_aus_a_typ (short mdn,long schnitt );
			   int ReadAusMaterial (short mdn,long mat);
			   int ReadSchlachtArt (double a);
			   int ReadSchlachtKalk (int kalk, short mdn);
			   double EkPreiseHolen (short mdn,long schnitt_mat,double schlachtart,long schlachtkalk, int PreiseAus_a_kalkpreis, double ek_schnitt);
			   BOOL UpdateGew (short mdn, long schnitt, long mat, long aus_mat, double gew_neu);
			   int PreiseToZerm (short mdn, long schnitt, int PreiseAus_a_kalkpreis);

			   BOOL UpdateZermMatV (short mdn,
								   long schnitt,
								   long zerm_aus_mat,
								   long zerm_mat,
								   double fwp,
								   double zerm_gew,
								   double zerm_gew_ant,
								   double mat_o_b,
								   double hk_vollk,
								   double hk_teilk,
								   double sk_vollk,
								   double sk_teilk,
								   double fil_ek_vollk,
								   double fil_ek_teilk,
								   double fil_vk_vollk,
								   double fil_vk_teilk,
								   double gh1_vollk,
								   double gh1_teilk,
								   double gh2_vollk,
								   double gh2_teilk,
								   double dm_100,
								   double kosten_zutat,
								   double kosten_vpk,
								   LPTSTR fix,
								   short a_typ,
								   double teilkosten_abs,
								   double vk_pr,
								   LPTSTR zerm_teil,
								   long kost_mat,
								   LPTSTR kost_mat_bz,
								   double pr_vk_eh
								   );
       public :
			TCHAR mat_bz [25];
			A_BAS_CLASS AusMatABas;
			A_BAS_CLASS ABas;
			A_BAS_CLASS SchlArtABas;
			SCHLAKLKK_CLASS Schlaklkk;
			A_VARB_CLASS AusMatAVarb;
			A_VARB_CLASS AVarb;
			A_SCHL_CLASS A_schl;
			A_ZUT_CLASS AZut;
			A_MAT_CLASS AMat;
			A_KALKPREIS_CLASS AKalkPreis;
			A_KALK_MAT_CLASS AKalkMat;
			A_KALK_EIG_CLASS AKalkEig;
			A_KALKN_CLASS AKalkn;
			A_KALKHNDW_CLASS AKalkHndw;
			ZERM_CLASS Zerm;
			ZERM_MAT_V_CLASS Zerm_mat_v;
			SCHNITT_CLASS Schnitt;
			KOST_ART_ZERL_CLASS kost_art_zerl;
			IPR_CLASS Ipr;


               
};
#endif
