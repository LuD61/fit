#ifndef _CHOICESCHNITT_DEF
#define _CHOICESCHNITT_DEF

#include "ChoiceX.h"
#include "SchnittList.h"
#include "Ag.h"
#include <vector>

class CChoiceSchnitt : public CChoiceX
{
    protected :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
        static int Sort4;
        static int Sort5;
        static int Sort6;
        static int Sort7;
        static int Sort8;
        static int Sort9;
        static int Sort10;
        static int Sort11;
		int ptcursor;
		int schnittmatcursor;
		TCHAR ptitem [19];
		TCHAR ptwert [4];
		TCHAR ptbez [37];
		TCHAR ptbezk [17];
		short wg;
		short hwg;
		TCHAR wg_bz1 [25];
		TCHAR hwg_bz1 [25];
		long schnitt_mat;
		TCHAR schnitt_mat_bz [25];

      
    public :
		CString Where;
	    std::vector<CSchnittList *> SchnittList;
	    std::vector<CSchnittList *> SelectList;
      	CChoiceSchnitt(CWnd* pParent = NULL);   // Standardkonstruktor
      	~CChoiceSchnitt(); 
		AG_CLASS Ag;
        virtual void BezLabel (CListCtrl *);
        virtual void NumLabel (CListCtrl *);
        virtual void FillList (void);
        void SearchNum (CListCtrl *,  LPTSTR);
        void SearchCol (CListCtrl *, LPTSTR, int);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
	    virtual void SaveSelection (CListCtrl *);
		CSchnittList *GetSelectedText ();
        int GetPtBez (LPTSTR, LPTSTR, LPSTR);
        int GetSchnittMatBez ();
		void DestroyList ();
		virtual void SetDefault ();
		virtual void OnEnter ();
};
#endif
