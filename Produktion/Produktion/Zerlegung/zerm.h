#ifndef _ZERM_DEF
#define _ZERM_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct ZERM {
   short          mdn;
   long           schnitt;
   long           zerm_aus_mat;
   long           zerm_mat;
   TCHAR          a_kz[3];
   TCHAR          fix[2];
   double         fwp;
   double         zerm_gew;
   double         zerm_gew_ant;
   TCHAR          zerm_teil[2];
   double         mat_o_b;
   double         hk_vollk;
   double         hk_teilk;
   short          delstatus;
   double         vollkosten_abs;
   double         vollkosten_proz;
   double         teilkosten_abs;
   double         teilkosten_proz;
   double         dm_100;
   long           a_z_f;
   double         vk_pr;
   double         kosten_zutat;
   double         kosten_vpk;
   short          a_typ;
   TCHAR          kost_bz[25];
   double         kosten;
   double         pr_zutat;
   double         pr_vpk;
   double         pr_kosten;
   double         pr_vk_netto;
   double         kosten_fix;
   double         pr_vk_eh;
   double         kosten_gh;
   double         kosten_eh;
   short          lfd;
   short          list_lfd;
   short          list_ebene;
   short          zerm_aus_a_typ;
   double         fwp_org;
   double         sk_vollk;
   double         sk_teilk;
   double         fil_ek_vollk;
   double         fil_ek_teilk;
   double         fil_vk_vollk;
   double         fil_vk_teilk;
   double         gh1_vollk;
   double         gh1_teilk;
   double         gh2_vollk;
   double         gh2_teilk;
//Zusätzlich in die zerm-struktur (ohne DB)
   TCHAR          zerm_aus_mat_bz[25];
   TCHAR          zerm_mat_bz1[25];
   TCHAR          zerm_mat_bz2[25];
   BOOL			  chk_gew;
   BOOL			  chk_gew_ant;
   BOOL			  chk_mat_o_b;
   BOOL			  chk_wert_100kg;
   BOOL			  chk_wert;
   BOOL			  chk_vk;
   BOOL			  chk_fwp;
   BOOL			  chk_kennzeichen;
   BOOL			  chk_zeitfaktor;
   TCHAR          variabel[2];
   BOOL			  chk_hk_vollk;
   BOOL			  chk_hk_vollk_preis;
   BOOL			  chk_hk_vollk_wert;
};
extern struct ZERM zerm, zerm_null;

#line 8 "zerm.rh"
  
	   

class ZERM_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               int AusMatCursor;
               int ATypVKCursor;
               int SchnittCursor;
               int GTCursor;
   	       int Zerlegeverlust ;
   	       short ZerlTreeErlauben;
   	       short TYP_MATERIAL;
   	       short TYP_ZUTAT;
   	       short TYP_KOSTEN;
   	       BOOL fwp_neu;
   	       double hk_vollk_preis;
   	       double hk_vollk_kosten;
   	       double hk_vollk_wert;
   		   short TYP_VERPACKUNG;
   		   short TYP_VKARTIKEL;
       	   short TYP_AUFSCHLAG;
       	   short TYP_AUFSCHLAG_EH;
		   BOOL PAGE2_WRITE;
		   BOOL ZEIGE_KOSTEN;
		   BOOL ZEIGE_VK;
		   BOOL SINGLE_EXPAND;
		   short SPLIT_HORIZONTAL;
		   short SPLIT_VERTIKAL;
		   
               ZERM zerm;  
               ZERM_CLASS () : DB_CLASS ()
               {
		    AusMatCursor = -1;		
		    ATypVKCursor = -1;		
		    SchnittCursor = -1;		
		    GTCursor = -1;		
	   	    TYP_MATERIAL = 1;
   		    TYP_ZUTAT = 2;
   		    TYP_VERPACKUNG = 3;
   		    TYP_VKARTIKEL = 10;
   		    fwp_neu = FALSE;
			hk_vollk_preis = 0.0;
			hk_vollk_kosten = 0.0;
			hk_vollk_wert = 0.0;
 	        TYP_KOSTEN = 4;
		   PAGE2_WRITE = FALSE;
       	    TYP_AUFSCHLAG = 5;
       	    TYP_AUFSCHLAG_EH = 6;
			ZEIGE_KOSTEN = FALSE;
			ZEIGE_VK = FALSE;
			SINGLE_EXPAND = FALSE;
			SPLIT_HORIZONTAL = 24;
			SPLIT_VERTIKAL = 24;
       	    ZerlTreeErlauben = 0;
               }
               ~ZERM_CLASS ()
               {
		    if (AusMatCursor != -1)
                    {
                           sqlclose (AusMatCursor);
                    }		
		    if (ATypVKCursor != -1)
                    {
                           sqlclose (ATypVKCursor);
                    }		
		    if (SchnittCursor != -1)
                    {
                           sqlclose (SchnittCursor);
                    }		
		    if (GTCursor != -1)
                    {
                           sqlclose (GTCursor);
                    }		
               }

               int dbreadfirst_AusMat ();  
               int dbread_AusMat ();  
               int dbreadfirst_ATypVK ();  
               int dbread_ATypVK ();  
               int dbreadfirst_Schnitt ();  
               int dbread_Schnitt ();  
               int dbreadfirst_GT ();  
               int dbread_GT ();  
               
};
#endif
