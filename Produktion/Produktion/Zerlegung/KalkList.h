#ifndef _KALK_LIST_DEF
#define _KALK_LIST_DEF
#pragma once

class CKalkList
{
public:
	short mdn;
	long schlklknr;
	CString schklk_bez;
	CKalkList(void);
	CKalkList(short,long, LPTSTR);
	~CKalkList(void);
};
#endif
