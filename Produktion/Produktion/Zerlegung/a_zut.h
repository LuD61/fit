#ifndef _A_ZUT_DEF
#define _A_ZUT_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct A_ZUT {
   double         a;
   long           bem_offs;
   short          delstatus;
   TCHAR          dkl_kz[2];
   TCHAR          ewg[7];
   short          fil;
   TCHAR          lgr_bdg[49];
   short          mdn;
   TCHAR          verwend[49];
   TCHAR          zug_men_max[49];
};
extern struct A_ZUT a_zut, a_zut_null;

#line 8 "a_zut.rh"

class A_ZUT_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               A_ZUT a_zut;  
               A_ZUT_CLASS () : DB_CLASS ()
               {
               }
};
#endif
