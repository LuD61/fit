#pragma once
#include "paintlistctrl.h"
//#include "FillList.h"
#include "EditListCtrl.h"
#include "zerm.h"
#include "zerm_mat_v.h"
#include "A_bas.h"
#include "A_mat.h"
#include "Schnitt.h"
#include "Page1.h"

class CZerlListCtrl :
//	public CPaintListCtrl
	public CEditListCtrl
{
public:
    CImageList image; 
	A_BAS_CLASS *A_bas;
	A_MAT_CLASS *A_mat;
	ZERM_CLASS *Zerm;
	ZERM_MAT_V_CLASS *Zerm_mat_v;
	SCHNITT_CLASS *Schnitt;
	CPage1 *Page1;

	CZerlListCtrl(void);
	~CZerlListCtrl(void);

	CFillList FillList;

	void Init ();

	BOOL Read ();
	short LMat;
	short LBez;
	short LPreis;
	short LWert;
	short LHK_Wert;
	short LVK_Wert;
	short LGH_Preis;
	short LEH_Preis;
	short LGewAnt;
	short LPr100;
};
