#pragma once
#include "DbPropertyPage.h"
#include "NumEdit.h"
#include "TextEdit.h"
#include "TextEdit.h"
#include "CtrlGrid.h"
#include "sys_par.h"
#include "mdn.h"
#include "schlaklkk.h"
#include "adr.h"
#include "Schnitt.h"
#include "zerm.h"
#include "zerm_mat_v.h"
#include "a_mat.h"
#include "a_bas.h"
#include "a_varb.h"
#include "a_kalk_mat.h"
#include "a_kalkhndw.h"
#include "DbUniCode.h"
#include "mo_progcfg.h"
#include "FormTab.h"
#include "afxwin.h"
#include "ChoiceMdn.h"
#include "ChoiceKalk.h"
#include "ChoiceSchnitt.h"
#include "ChoiceKostArt.h"
#include "ChoiceA.h"
#include "Controls.h"
#include "WorkDB.h"
#include "WorkPrint.h"
#include "ZerlTreeData.h"
#include "Page1.h"
#include "Schnittkostg.h"
#include "Schnittkostf.h"
#include "ptabn.h"


// CPage4-Dialogfeld

class CPage4 : public CDbPropertyPage
{
	DECLARE_DYNAMIC(CPage4)

public:
	CPage4();
	virtual ~CPage4();

// Dialogfelddaten
	enum { IDD = IDD_PAGE4 };
	PROG_CFG Cfg;
	CCtrlGrid CtrlGrid;
	CCtrlGrid MdnGrid;
	CCtrlGrid SchnittGrid;
	CCtrlGrid ZerlKostenGrid;
	CFormTab Form;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog();
	virtual void OnSize (UINT, int, int);
     virtual BOOL OnReturn ();
       virtual BOOL OnKeyup ();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 
	virtual BOOL Write (BOOL);
	virtual BOOL Back ();
	virtual BOOL Print (HWND);



	DECLARE_MESSAGE_MAP()
public:
	CWnd *Frame;
	virtual void UpdatePage ();
	virtual void GetPage ();
	virtual BOOL OnSetActive ();
	virtual void OnDelete ();
	CControls HeadControls;
	CControls PosControls;
	MDN_CLASS *Mdn;
	SCHNITT_CLASS *Schnitt;
	ZERM_CLASS *Zerm;
	ADR_CLASS *MdnAdr;
	CFont Font;
	CFont lFont;
	CPage1 *Basis;


public:
    void OnSchnittchoice(); 
    void OnKostArtchoice(); 
	void DisableFelder();
	void FillKalkbasisCombo ();
	CNumEdit m_Mdn;
	CButton m_MdnChoice;
	short kalk_bas; 
public:
	CEdit m_MdnName;
	CNumEdit m_Schnitt;
	CButton m_SchnittChoice;

	CEdit m_SchnittTxt;
	CStatic m_LMdn;
	CStatic m_LSchnitt;
	CStatic m_ZerlkostGroup;
	BOOL ModalChoice;
	CComboBox m_KalkbasisCombo;
	PTABN_CLASS Ptabn;
public:
	CEdit m_Marktpreis;
public:
	afx_msg void OnCbnSelchangeKalkbasisCombo();
public:
	afx_msg void OnEnKillfocusMarktpreis();
public:
	CStatic m_LMarktpreis;
public:
	afx_msg void OnEnSetfocusSchnittTxt();
};
