#include "stdafx.h"
#include "WorkDB.h"

	long schnitt_neu = 0;
	short mdn_neu = 0;
	long cpy_schnitt = 0;
	short cpy_mdn = 0;
	long cpy_mat = 0; 
	long cpy_aus_mat = 0; 
	short cpy_a_typ = 0;

int WORKDB_CLASS::DelZerm (short mdn,long schnitt )
{

    sqlin ((short *) &mdn,SQLSHORT,0);
    sqlin ((long *) &schnitt,SQLLONG,0);

	return sqlcomm (_T("delete from zerm where mdn = ? ")
							_T("and schnitt = ?"));
}
int WORKDB_CLASS::ReadAusMaterial (short mdn,long mat)
{
	double a = (double) 0;
	CString Error;
	sqlin  ((long *) &mat, SQLLONG, 0);
	sqlout ((double *) &a, SQLDOUBLE, 0);
	int MatCursor = sqlcursor (_T("select a from a_mat where mat = ? "));
	sqlopen (MatCursor);
	int dsqlstatus = sqlfetch (MatCursor);
	if (dsqlstatus)
	{
		return dsqlstatus;
	}
	AusMatABas.a_bas.a = a;
	dsqlstatus = AusMatABas.dbreadfirst () ;
	if (dsqlstatus)
	{
		return dsqlstatus;
	}
	AusMatAVarb.a_varb.a = a;
	dsqlstatus = AusMatAVarb.dbreadfirst () ;
	if (AusMatAVarb.a_varb.fwp < 0.001) AusMatAVarb.a_varb.fwp = 100; 
	if (dsqlstatus)
	{
		return dsqlstatus;
	}
	AKalkMat.a_kalk_mat.mdn = mdn;
	AKalkMat.a_kalk_mat.fil = 0;
	AKalkMat.a_kalk_mat.a = a;
	AKalkMat.dbreadfirst () ;


	return dsqlstatus;
}

int WORKDB_CLASS::ReadMaterial (short mdn, long mat,short a_typ,int PreiseAus_a_kalkpreis)
{
	double a = (double) 0;
	int dsqlstatus = 0;
	CString Error;
	memcpy (&AVarb.a_varb, &a_varb_null, sizeof (A_VARB));
	memcpy (&AZut.a_zut, &a_zut_null, sizeof (A_ZUT));
	memcpy (&AKalkMat.a_kalk_mat, &a_kalk_mat_null, sizeof (A_KALK_MAT));

	if (a_typ == Zerm.TYP_KOSTEN || a_typ == Zerm.TYP_AUFSCHLAG
								 || a_typ == Zerm.TYP_AUFSCHLAG_EH)
	{
		if (a_typ == Zerm.TYP_AUFSCHLAG) _tcscpy_s (AVarb.a_varb.a_kz, _T("AU"));
		if (a_typ == Zerm.TYP_AUFSCHLAG_EH) _tcscpy_s (AVarb.a_varb.a_kz, _T("AU"));
		if (a_typ == Zerm.TYP_KOSTEN) _tcscpy_s (AVarb.a_varb.a_kz, _T("KO"));

		/***
		sqlin  ((long *) &mat, SQLLONG, 0);
		sqlin  ((short *) &a_typ, SQLSHORT, 0);
		sqlout ((TCHAR *) ABas.a_bas.a_bz1, SQLCHAR, 25);
		sqlout ((double *) &ABas.a_bas.sk_vollk, SQLDOUBLE, 0);
		int KostCursor = sqlcursor (_T("select unique(kost_bz),mat_o_b from zerm where zerm_mat = ? and a_typ = ? "));
		sqlopen (KostCursor);
		dsqlstatus = sqlfetch (KostCursor);
		****/
		kost_art_zerl.kost_art_zerl.mdn = mdn;
		kost_art_zerl.kost_art_zerl.kost_art_zerl = (short) mat;
		dsqlstatus = kost_art_zerl.dbreadfirst();
	   _tcscpy_s (ABas.a_bas.a_bz1, _T(" "));
	   ABas.a_bas.sk_vollk = 0;
		if (dsqlstatus == 0)
		{
			_tcscpy_s (ABas.a_bas.a_bz1, kost_art_zerl.kost_art_zerl.kost_art_zerl_bz);
			ABas.a_bas.sk_vollk = kost_art_zerl.kost_art_zerl.vkost_wt;
			return 0;
		}
		return dsqlstatus;
	}


	if (a_typ == Zerm.TYP_VERPACKUNG || a_typ == Zerm.TYP_VKARTIKEL)
	{
		a = mat;
	}
	else
	{
		AMat.a_mat.mat = mat;
		dsqlstatus = AMat.dbreadfirst();
		if (dsqlstatus)
		{
			return dsqlstatus;
		}
		a = AMat.a_mat.a;

		/***
		sqlin  ((long *) &mat, SQLLONG, 0);
		sqlout ((double *) &a, SQLDOUBLE, 0);
		int MatCursor = sqlcursor (_T("select a from a_mat where mat = ? "));
		sqlopen (MatCursor);
		dsqlstatus = sqlfetch (MatCursor);
		if (dsqlstatus)
		{
			return dsqlstatus;
		}
		********/
	}
	ABas.a_bas.a = a;
	dsqlstatus = ABas.dbreadfirst ();
	if (dsqlstatus)
	{
		return dsqlstatus;
	}

	if (a_typ == Zerm.TYP_MATERIAL)
	{
		AVarb.a_varb.a = a;
		dsqlstatus = AVarb.dbreadfirst ();
		if (dsqlstatus == 100) 
		{
			if (ABas.a_bas.a_typ == ABas.A_TYP_EIGENPRODUKT || ABas.a_bas.a_typ2 == ABas.A_TYP_EIGENPRODUKT) 
			{
				memcpy (&AVarb.a_varb, &a_varb_null, sizeof (A_VARB));
				AVarb.a_varb.a = ABas.a_bas.a;
				AVarb.a_varb.mdn = ABas.a_bas.mdn;
				AVarb.a_varb.mat = AMat.a_mat.mat;
				AVarb.a_varb.fwp = 100;
				strcpy(AVarb.a_varb.a_kz ,"GT");
				dsqlstatus = AVarb.dbupdate();
				// ZUTUN  Eigenprodukt zulassen , fwp muss noch errechnet werden  
			}
			if (dsqlstatus == 100)
			{
				dsqlstatus = 50;
			}
		}
	} 
	else if (a_typ == Zerm.TYP_ZUTAT)
	{
		AZut.a_zut.a = a;
		dsqlstatus = AZut.dbreadfirst () ;
		if (dsqlstatus == 0)
		{
		   _tcscpy_s (AVarb.a_varb.a_kz, _T("ZT"));
		}
		if (dsqlstatus == 100)
		{
			AVarb.a_varb.a = a;
			dsqlstatus = AVarb.dbreadfirst ();
		    if (dsqlstatus == 0) _tcscpy_s (AVarb.a_varb.a_kz, _T("ZT"));
		}
		if (dsqlstatus == 100) dsqlstatus = 50;
	} 
	else if (a_typ == Zerm.TYP_VERPACKUNG)
	{
		   _tcscpy_s (AVarb.a_varb.a_kz, _T("VP"));
	} 
	else if (a_typ == Zerm.TYP_VKARTIKEL)
	{
		   _tcscpy_s (AVarb.a_varb.a_kz, _T("VK"));
	} 
	AKalkMat.a_kalk_mat.mat_o_b = HoleStammDatenPreis(a_typ, mdn,mat,PreiseAus_a_kalkpreis);
//	AKalkMat.a_kalk_mat.mdn = mdn;
//	AKalkMat.a_kalk_mat.a = a;
//	AKalkMat.dbreadfirst ();
	return dsqlstatus;
}

int WORKDB_CLASS::ReadSchlachtArt (double a)
{
	if (a == (double) 0) return 100;
	CString Error;
	SchlArtABas.a_bas.a = a;
	int dsqlstatus = SchlArtABas.dbreadfirst () != 0;
	if (dsqlstatus)
	{
		return dsqlstatus;
	}
	A_schl.a_schl.a = a;
	dsqlstatus = A_schl.dbreadfirst () != 0;
	if (dsqlstatus)
	{
		SchlArtABas.a_bas.a = (double) 0;
		return dsqlstatus;
	}

	return dsqlstatus;
}
int WORKDB_CLASS::ReadSchlachtKalk (int kalk,short mdn)
{
	if (kalk ==  0) return 0;
	CString Error;
	Schlaklkk.schlaklkk.schlklknr = kalk;
	Schlaklkk.schlaklkk.mdn = mdn;
	int dsqlstatus = Schlaklkk.dbreadfirst () != 0;
	if (dsqlstatus)
	{
		return dsqlstatus;
	}

	return dsqlstatus;
}
double WORKDB_CLASS::EkPreiseHolen (short mdn,long schnitt_mat,double schlachtart,long schlachtkalk,int PreiseAus_a_kalkpreis, double ek_schnitt)
{
	double ek_pr = 0.0;
	double a = 0.0;
//                ================
//=============== schnitt_mat holen===========================
//                ================
	sqlin  ((long *) &schnitt_mat, SQLLONG, 0);
	sqlout ((double *) &a, SQLDOUBLE, 0);
	int MatCursor = sqlcursor (_T("select a from a_mat where mat = ? "));
	sqlopen (MatCursor);
	int dsqlstatus = sqlfetch (MatCursor);
	if (dsqlstatus)
	{
		return -1;
	}
	if (AusMatABas.a_bas.a != a)
	{
		AusMatABas.a_bas.a = a;
		dsqlstatus = AusMatABas.dbreadfirst () ;
		if (dsqlstatus)
		{
			return -2;
		}
	}
	if (ek_schnitt == 0.0)
	{
		if (AusMatAVarb.a_varb.a != a)
		{
			AusMatAVarb.a_varb.a = a;
			dsqlstatus = AusMatAVarb.dbreadfirst () ;
			if (AusMatAVarb.a_varb.fwp < 0.001) AusMatAVarb.a_varb.fwp = 100; 
			if (dsqlstatus)
			{
				return -3;
			}
		}
		if (AKalkMat.a_kalk_mat.a != a)
		{
			AKalkMat.a_kalk_mat.mdn = mdn;
			AKalkMat.a_kalk_mat.fil = 0;
			AKalkMat.a_kalk_mat.a = a;
			AKalkMat.dbreadfirst () ;
		}
		Schnitt.schnitt.ek_pr = AKalkMat.a_kalk_mat.mat_o_b;
		Schnitt.schnitt.ek = AKalkMat.a_kalk_mat.mat_o_b;   
	
	//                ================
	//=============== schlacht_artikel holen===========================
	//                ================
	
		if (SchlArtABas.a_bas.a != schlachtart && schlachtart > 0.0)
		{
			SchlArtABas.a_bas.a = a;
			int dsqlstatus = SchlArtABas.dbreadfirst () != 0;
			if (dsqlstatus)
			{
				return -4;
			}
			A_schl.a_schl.a = a;
			dsqlstatus = A_schl.dbreadfirst () != 0;
			if (dsqlstatus)
			{
				SchlArtABas.a_bas.a = (double) 0;
				return -5;
			}
		}
		if (schlachtart > 0.0)
		{
			Schnitt.schnitt.ek = A_schl.a_schl.grund_pr;
			Schnitt.schnitt.ek_pr = A_schl.a_schl.grund_pr; 
		}
	}
	else
	{
		Schnitt.schnitt.ek = ek_schnitt;
		Schnitt.schnitt.ek_pr = ek_schnitt;
	}

//                ================
//=============== schlacht_kalk holen===========================
//                ================
	if (Schlaklkk.schlaklkk.schlklknr != schlachtkalk && schlachtkalk > 0)
	{
		Schlaklkk.schlaklkk.schlklknr = schlachtkalk;
		Schlaklkk.schlaklkk.mdn = mdn;
		int dsqlstatus = Schlaklkk.dbreadfirst () != 0;
		if (dsqlstatus)
		{
			return -6;
		}
	}
	if (schlachtkalk > 0)
	{
		if (Schlaklkk.schlaklkk.kaltgew > 0.01)
		{
			if (Schlaklkk.schlaklkk.kalkart == 0)
			{
				Schnitt.schnitt.ek_pr = Schnitt.schnitt.ek * (Schlaklkk.schlaklkk.schlachtgew / Schlaklkk.schlaklkk.kaltgew);
			}
			else
			{
				Schnitt.schnitt.ek_pr = Schnitt.schnitt.ek * (Schlaklkk.schlaklkk.lebendgew / Schlaklkk.schlaklkk.kaltgew);
			}
		}
		else
		{
			Schnitt.schnitt.ek_pr = Schnitt.schnitt.ek;
		}
		Schnitt.schnitt.ek_pr += Schlaklkk.schlaklkk.zuabkg;
	}
	return Schnitt.schnitt.ek_pr;
}


BOOL WORKDB_CLASS::UpdateGew (short mdn, long schnitt, long mat, long aus_mat, double gew_neu)


{

	Zerm.zerm.mdn = mdn;
	Zerm.zerm.schnitt = schnitt;
	Zerm.zerm.zerm_aus_mat = aus_mat;
	Zerm.zerm.zerm_mat = mat;
	Zerm.zerm.a_typ = Zerm.TYP_MATERIAL;
	if (Zerm.dbreadfirst() != 0) return FALSE;
	if (Zerm.zerm.zerm_gew == (double) 0) return FALSE;
	double GewFaktor = gew_neu / Zerm.zerm.zerm_gew;
	if (CStrFuncs::IsDoubleEqual (GewFaktor,(double) 0)) return TRUE;
	if (CStrFuncs::IsDoubleEqual (GewFaktor,(double) 1)) return TRUE;
//	if (GewFaktor > -0.000001 && GewFaktor < 0.000001) return TRUE;
	if (Zerm.dbreadfirst_Schnitt() == 0)
	{
		if (Zerm.zerm.a_typ == Zerm.TYP_MATERIAL)
		{
			Zerm.zerm.zerm_gew = Zerm.zerm.zerm_gew * GewFaktor;
			Zerm.dbupdate();
		}
	} else return FALSE;
	while (Zerm.dbread_Schnitt() == 0)
	{
		if (Zerm.zerm.a_typ == Zerm.TYP_MATERIAL)
		{
			Zerm.zerm.zerm_gew = Zerm.zerm.zerm_gew * GewFaktor;
			Zerm.dbupdate();
		}
	}
	return TRUE;
}
BOOL WORKDB_CLASS::DelZermMatV (short mdn,
								   long schnitt,
								   long zerm_aus_mat,
								   long zerm_mat,
								   short a_typ)
{
	Zerm_mat_v.zerm_mat_v.mdn = mdn;
	Zerm_mat_v.zerm_mat_v.schnitt = schnitt;
	Zerm_mat_v.zerm_mat_v.zerm_aus_mat = zerm_aus_mat;
	Zerm_mat_v.zerm_mat_v.zerm_mat = zerm_mat;
	Zerm_mat_v.zerm_mat_v.a_typ = a_typ;
	return Zerm_mat_v.dbdelete();

	/************* dies geht nicht immer .. WARUM ???? 
    sqlin ((short *) &mdn,SQLSHORT,0);
    sqlin ((short *) &schnitt,SQLSHORT,0);
    sqlin ((long *) &zerm_aus_mat,SQLLONG,0);
    sqlin ((long *) &zerm_mat,SQLLONG,0);

	return sqlcomm (_T("delete from zerm_mat_v where mdn = ? ")
							_T("and schnitt = ? and zerm_aus_mat = ? and zerm_mat = ? "));
*********************/
}
BOOL WORKDB_CLASS::DelZermMatV (short mdn,
								   long schnitt,
								   long zerm_mat)
{

	/***
    sqlin ((short *) &mdn,SQLSHORT,0);
    sqlin ((short *) &schnitt,SQLSHORT,0);
    sqlin ((long *) &zerm_mat,SQLLONG,0);
	sqlin ((short *) &Zerm.TYP_MATERIAL,SQLSHORT,0);

	sqlcomm (_T("delete from zerm_mat_v where mdn = ? ")
							_T("and schnitt = ? and zerm_aus_mat = ? and a_typ <> ? "));

	*****/
    sqlin ((short *) &mdn,SQLSHORT,0);
    sqlin ((long *) &schnitt,SQLLONG,0);
    sqlin ((long *) &zerm_mat,SQLLONG,0);

	return sqlcomm (_T("delete from zerm_mat_v where mdn = ? ")
							_T("and schnitt = ? and zerm_mat = ? "));
}
int WORKDB_CLASS::DelZermMatV (short mdn,long schnitt )
{

    sqlin ((short *) &mdn,SQLSHORT,0);
    sqlin ((long *) &schnitt,SQLLONG,0);

	return sqlcomm (_T("delete from zerm_mat_v where mdn = ? ")
							_T("and schnitt = ?"));
}
long WORKDB_CLASS::ZerlMat ()
{
	long ZerlMat = 1;
    sqlout ((long *) &ZerlMat,SQLLONG,0);
    sqlcomm (_T("select a_mat.mat from a_varb,a_mat where a_varb.a = a_mat.a ")
							_T("and a_varb.a_kz = \"ZV\" "));
	return ZerlMat;
}
BOOL WORKDB_CLASS::UpdateZermMatV (short mdn,
								   long schnitt,
								   long zerm_aus_mat,
								   long zerm_mat,
								   double fwp,
								   double zerm_gew,
								   double zerm_gew_ant,
								   double mat_o_b,
								   double hk_vollk,
								   double hk_teilk,
								   double sk_vollk,
								   double sk_teilk,
								   double fil_ek_vollk,
								   double fil_ek_teilk,
								   double fil_vk_vollk,
								   double fil_vk_teilk,
								   double gh1_vollk,
								   double gh1_teilk,
								   double gh2_vollk,
								   double gh2_teilk,
								   double dm_100,
								   double kosten_zutat,
								   double kosten_vpk,
								   LPTSTR fix,
								   short a_typ,
								   double teilkosten_abs,
								   double vk_pr,
								   LPTSTR zerm_teil,
								   long kost_mat,
								   LPTSTR kost_mat_bz,
								   double pr_vk_eh
								   )
{
	if (a_typ != Zerm.TYP_MATERIAL)
	{
		int di = 0;
//		return TRUE;   
	}
	CString CFix;
	CFix.Format (_T("%s"),fix);
	if (CFix == "F" && (a_typ == Zerm.TYP_MATERIAL || a_typ == Zerm.TYP_VKARTIKEL)) 
	{
		return DelZermMatV ( mdn, schnitt,zerm_mat);
	}
	if (zerm_mat == zerm_aus_mat && a_typ == Zerm.TYP_MATERIAL)
	{
		return DelZermMatV ( mdn, schnitt,zerm_mat);
	}

	memcpy (&Zerm_mat_v.zerm_mat_v, &zerm_mat_v_null, sizeof (ZERM_MAT_V));
	Zerm_mat_v.zerm_mat_v.mdn = mdn;
	Zerm_mat_v.zerm_mat_v.schnitt = schnitt;
	Zerm_mat_v.zerm_mat_v.zerm_aus_mat = zerm_aus_mat;
	Zerm_mat_v.zerm_mat_v.zerm_mat = zerm_mat;

	Zerm_mat_v.zerm_mat_v.zerm_gew = zerm_gew;
	Zerm_mat_v.zerm_mat_v.zerm_gew_ant = zerm_gew_ant;
	Zerm_mat_v.zerm_mat_v.fwp = fwp;
	Zerm_mat_v.zerm_mat_v.mat_o_b = mat_o_b;
	Zerm_mat_v.zerm_mat_v.dm_100 = dm_100;
	Zerm_mat_v.zerm_mat_v.a_typ = a_typ;
	Zerm_mat_v.zerm_mat_v.kost_mat = kost_mat;
	_tcscpy_s (Zerm_mat_v.zerm_mat_v.kost_mat_bz, _T(kost_mat_bz));
	Zerm_mat_v.zerm_mat_v.teilkosten_abs = teilkosten_abs;
	if (_tstoi (zerm_teil) == 2 || a_typ == Zerm.TYP_VKARTIKEL)   // Mat hat Kosten usw..
	{
		Zerm_mat_v.zerm_mat_v.hk_vollk = hk_vollk;
		Zerm_mat_v.zerm_mat_v.hk_teilk = hk_teilk;
		Zerm_mat_v.zerm_mat_v.kosten_zutat = kosten_zutat;
		Zerm_mat_v.zerm_mat_v.kosten_vpk = kosten_vpk;
		Zerm_mat_v.zerm_mat_v.vk_pr = vk_pr;
		Zerm_mat_v.zerm_mat_v.pr_vk_eh = pr_vk_eh;
	}
	Zerm_mat_v.dbupdate();
	return TRUE;
}

int WORKDB_CLASS::PreiseToZerm (short mdn,long schnitt, int PreiseAus_a_kalkpreis)
{
	int dsqlstatus = 0;
	Zerm.zerm.mdn = mdn;
	Zerm.zerm.schnitt = schnitt;
	if (Zerm.dbreadfirst_Schnitt() == 0)
	{
		if (Zerm.zerm.a_typ == Zerm.TYP_ZUTAT || Zerm.zerm.a_typ == Zerm.TYP_VERPACKUNG)
		{
			Zerm.zerm.mat_o_b = HoleStammDatenPreis(Zerm.zerm.a_typ, mdn,Zerm.zerm.zerm_mat,PreiseAus_a_kalkpreis);
			Zerm.dbupdate();
		}
	} else return FALSE;
	while (Zerm.dbread_Schnitt() == 0)
	{
		if (Zerm.zerm.a_typ == Zerm.TYP_ZUTAT || Zerm.zerm.a_typ == Zerm.TYP_VERPACKUNG)
		{
			Zerm.zerm.mat_o_b = HoleStammDatenPreis(Zerm.zerm.a_typ, mdn,Zerm.zerm.zerm_mat,PreiseAus_a_kalkpreis);
			Zerm.dbupdate();
		}
	}
	return dsqlstatus;
}

int WORKDB_CLASS::HoleAKalkMat (short mdn,long mat,int PreiseAus_a_kalkpreis)
{
	int dsqlstatus = 0;
		AMat.a_mat.mat = mat;
		dsqlstatus = AMat.dbreadfirst();
		if (dsqlstatus)
		{
			return dsqlstatus;
		}
		if (PreiseAus_a_kalkpreis == 1)
		{
				AKalkPreis.a_kalkpreis.mdn = mdn;
				AKalkPreis.a_kalkpreis.fil = 0;
				AKalkPreis.a_kalkpreis.a = AMat.a_mat.a;
				dsqlstatus = AKalkPreis.dbreadfirst ();
				if (dsqlstatus == 100)
				{
					AKalkPreis.a_kalkpreis.mdn = 0;
					AKalkPreis.a_kalkpreis.fil = 0;
					AKalkPreis.a_kalkpreis.a = AMat.a_mat.a;
					dsqlstatus = AKalkPreis.dbreadfirst ();
				}
				AKalkMat.a_kalk_mat.mat_o_b = AKalkPreis.a_kalkpreis.mat_o_b;
				return dsqlstatus;
		}

		AKalkMat.a_kalk_mat.mdn = mdn;
		AKalkMat.a_kalk_mat.fil = 0;
		AKalkMat.a_kalk_mat.a = AMat.a_mat.a;
		dsqlstatus = AKalkMat.dbreadfirst ();
		if (dsqlstatus == 100)
		{
			AKalkMat.a_kalk_mat.mdn = 0;
			AKalkMat.a_kalk_mat.fil = 0;
			AKalkMat.a_kalk_mat.a = AMat.a_mat.a;
			dsqlstatus = AKalkMat.dbreadfirst ();
		}

	return dsqlstatus;
}

double WORKDB_CLASS::HoleMatoB (short mdn,long mat, int PreiseAus_a_kalkpreis, int kalk_bas, int PrGrStuf)
{
	int dsqlstatus = 0;

		AMat.a_mat.mat = mat;
		dsqlstatus = AMat.dbreadfirst();
		if (dsqlstatus)
		{
			return 0.0;
		}
		ABas.a_bas.a = AMat.a_mat.a;
		dsqlstatus = ABas.dbreadfirst ();
		if (kalk_bas == 2) 
		{
				Ipr.ipr.mdn = mdn;
				Ipr.ipr.a = AMat.a_mat.a;
				Ipr.ipr.pr_gr_stuf = PrGrStuf;
				Ipr.ipr.kun_pr =  0;
				Ipr.ipr.kun =  0;
				dsqlstatus = Ipr.dbreadfirst ();
				if (dsqlstatus == 0) return Ipr.ipr.vk_pr_eu;
				return (double) 0;
		}
		if (PreiseAus_a_kalkpreis == 1)
		{
				AKalkPreis.a_kalkpreis.mdn = mdn;
				AKalkPreis.a_kalkpreis.fil = 0;
				AKalkPreis.a_kalkpreis.a = AMat.a_mat.a;
				dsqlstatus = AKalkPreis.dbreadfirst ();
				if (dsqlstatus == 100)
				{
					AKalkPreis.a_kalkpreis.mdn = 0;
					AKalkPreis.a_kalkpreis.fil = 0;
					AKalkPreis.a_kalkpreis.a = AMat.a_mat.a;
					dsqlstatus = AKalkPreis.dbreadfirst ();
				}
				return AKalkPreis.a_kalkpreis.mat_o_b;
		}
		else
		{
			if (ABas.a_bas.a_typ == ABas.A_TYP_MATERIAL || ABas.a_bas.a_typ2 == ABas.A_TYP_MATERIAL) 
			{
				AKalkMat.a_kalk_mat.mdn = mdn;
				AKalkMat.a_kalk_mat.fil = 0;
				AKalkMat.a_kalk_mat.a = AMat.a_mat.a;
				dsqlstatus = AKalkMat.dbreadfirst ();
				if (dsqlstatus == 100)
				{
					AKalkMat.a_kalk_mat.mdn = 0;
					AKalkMat.a_kalk_mat.fil = 0;
					AKalkMat.a_kalk_mat.a = AMat.a_mat.a;
					dsqlstatus = AKalkMat.dbreadfirst ();
				}
				return AKalkMat.a_kalk_mat.mat_o_b;
			}
			if (ABas.a_bas.a_typ == ABas.A_TYP_EIGENPRODUKT || ABas.a_bas.a_typ2 == ABas.A_TYP_EIGENPRODUKT) 
			{
				AKalkEig.a_kalk_eig.mdn = mdn;
				AKalkEig.a_kalk_eig.fil = 0;
				AKalkEig.a_kalk_eig.a = AMat.a_mat.a;
				dsqlstatus = AKalkEig.dbreadfirst ();
				if (dsqlstatus == 100)
				{
					AKalkEig.a_kalk_eig.mdn = 0;
					AKalkEig.a_kalk_eig.fil = 0;
					AKalkEig.a_kalk_eig.a = AMat.a_mat.a;
					dsqlstatus = AKalkEig.dbreadfirst ();
				}
				return AKalkEig.a_kalk_eig.mat_o_b;
			}
		}

	return 0.0;
}
double WORKDB_CLASS::HoleFWP (long mat,double FWP)
{
	int dsqlstatus = 0;

		AMat.a_mat.mat = mat;
		dsqlstatus = AMat.dbreadfirst();
		if (dsqlstatus)
		{
			return 0.0;
		}
		ABas.a_bas.a = AMat.a_mat.a;
		dsqlstatus = ABas.dbreadfirst ();
		if (ABas.a_bas.a_typ == ABas.A_TYP_MATERIAL || ABas.a_bas.a_typ2 == ABas.A_TYP_MATERIAL || 
			ABas.a_bas.a_typ == ABas.A_TYP_EIGENPRODUKT || ABas.a_bas.a_typ2 == ABas.A_TYP_EIGENPRODUKT) 
		{
			AVarb.a_varb.a = ABas.a_bas.a;
			dsqlstatus = AVarb.dbreadfirst ();
			if (dsqlstatus == 0)
			{
				return AVarb.a_varb.fwp;
			}
		}
		return FWP;
}
char* WORKDB_CLASS::HoleA_Kz (long mat,short a_typ,char* a_kz)
{
	int dsqlstatus = 0;
		AMat.a_mat.mat = mat;
		if (a_typ == Zerm.TYP_KOSTEN) return "KO";
		if (a_typ == Zerm.TYP_ZUTAT) return "ZT";
		if (a_typ == Zerm.TYP_VKARTIKEL) return "VK";
		if (a_typ == Zerm.TYP_VERPACKUNG) return "VP";
		if (a_typ == Zerm.TYP_AUFSCHLAG) return "AU";
		if (a_typ == Zerm.TYP_AUFSCHLAG_EH) return "AU";
		dsqlstatus = AMat.dbreadfirst();
		if (dsqlstatus)
		{
			return a_kz;
		}
		ABas.a_bas.a = AMat.a_mat.a;
		dsqlstatus = ABas.dbreadfirst ();
		if (ABas.a_bas.a_typ == ABas.A_TYP_MATERIAL || ABas.a_bas.a_typ2 == ABas.A_TYP_MATERIAL || 
			ABas.a_bas.a_typ == ABas.A_TYP_EIGENPRODUKT || ABas.a_bas.a_typ2 == ABas.A_TYP_EIGENPRODUKT) 
		{
			AVarb.a_varb.a = ABas.a_bas.a;
			dsqlstatus = AVarb.dbreadfirst ();
			if (dsqlstatus == 0)
			{
				return AVarb.a_varb.a_kz;
			}
		}
		return a_kz;
}

int WORKDB_CLASS::HoleAKalkHndw (short mdn,double a, int PreiseAus_a_kalkpreis)
{
	int dsqlstatus = 0;

		if (PreiseAus_a_kalkpreis == 1)
		{
				AKalkPreis.a_kalkpreis.mdn = mdn;
				AKalkPreis.a_kalkpreis.fil = 0;
				AKalkPreis.a_kalkpreis.a = a;
				dsqlstatus = AKalkPreis.dbreadfirst ();
				if (dsqlstatus == 100)
				{
					AKalkPreis.a_kalkpreis.mdn = 0;
					AKalkPreis.a_kalkpreis.fil = 0;
					AKalkPreis.a_kalkpreis.a = a;
					dsqlstatus = AKalkPreis.dbreadfirst ();
				}
				AKalkHndw.a_kalkhndw.pr_ek1 = AKalkPreis.a_kalkpreis.mat_o_b;
				return dsqlstatus;
		}

		AKalkHndw.a_kalkhndw.mdn = mdn;
		AKalkHndw.a_kalkhndw.fil = 0;
		AKalkHndw.a_kalkhndw.a = a;
		dsqlstatus = AKalkHndw.dbreadfirst ();
		if (dsqlstatus == 100)
		{
			AKalkHndw.a_kalkhndw.mdn = 0;
			AKalkHndw.a_kalkhndw.fil = 0;
			AKalkHndw.a_kalkhndw.a = a;
			dsqlstatus = AKalkHndw.dbreadfirst ();
		}
	return dsqlstatus;
}

double WORKDB_CLASS::HoleStammDatenPreis(short a_typ,short mdn,long mat, int PreiseAus_a_kalkpreis)
{
		double Artikel = (double) mat;
		AMat.a_mat.mat = mat;
		int dsqlstatus = AMat.dbreadfirst();
		if (dsqlstatus == 0) Artikel = AMat.a_mat.a;

		if (a_typ == Zerm.TYP_ZUTAT)
		{
			AKalkn.a_kalkn.mdn = mdn;
			AKalkn.a_kalkn.fil = 0;
			AKalkn.a_kalkn.a = Artikel;
			CString Date;
			CStrFuncs::SysDate (Date);
			Zerm.ToDbDate (Date, &AKalkn.a_kalkn.gue_ab);
				if (HoleAKalkMat(mdn,mat,PreiseAus_a_kalkpreis) == 0)
				{
					return AKalkMat.a_kalk_mat.mat_o_b;
				}
		}



		if (a_typ == Zerm.TYP_VERPACKUNG)
		{
			AKalkn.a_kalkn.mdn = mdn;
			AKalkn.a_kalkn.fil = 0;
			AKalkn.a_kalkn.a = Artikel;
			CString Date;
			CStrFuncs::SysDate (Date);
			Zerm.ToDbDate (Date, &AKalkn.a_kalkn.gue_ab);
				ABas.a_bas.a = Artikel;
				dsqlstatus = ABas.dbreadfirst ();
				if (ABas.a_bas.a_typ == 8) // Artikeltyp Verpackungen
				{
					if (HoleAKalkHndw(mdn,ABas.a_bas.a,PreiseAus_a_kalkpreis) == 0)
					{
						return AKalkHndw.a_kalkhndw.pr_ek1;
					}
				}
				else
				{
					if (HoleAKalkMat(mdn,mat,PreiseAus_a_kalkpreis) == 0)
					{
						return  AKalkMat.a_kalk_mat.mat_o_b;
					}
				}
		}
		return (double) 0;
}
BOOL WORKDB_CLASS::RenameSchnitt (short mdn,long schnitt_alt, long schnitt_neu)
{
	sqlin  ((long *) &schnitt_neu, SQLLONG, 0);
	sqlin  ((short *) &mdn, SQLSHORT, 0);
	sqlcomm (_T("update schnitt set schnitt = ? where mdn = ? and schnitt = -1 "));

	sqlin  ((long *) &schnitt_neu, SQLLONG, 0);
	sqlin  ((short *) &mdn, SQLSHORT, 0);
	sqlcomm (_T("update zerm set schnitt = ? where mdn = ? and schnitt = -1 "));

	return TRUE;
}

BOOL WORKDB_CLASS::CopySchnitt (short mdn_alt,long schnitt_alt)
{
	CString Message = _T("");
	CStrFuncs::FromClipboard (Message, CF_TEXT);
	CToken t (Message, _T("="));
	if (t.GetAnzToken () > 4)
	{
		CString c = t.GetToken (0);
		if (c == _T("SchnittNr"))
		{
			schnitt_neu = (short) CStrFuncs::StrToDouble (t.GetToken (1));
			mdn_neu = (short) CStrFuncs::StrToDouble (t.GetToken (2));
			cpy_mat = (long) CStrFuncs::StrToDouble (t.GetToken (3));
			cpy_a_typ = (short) CStrFuncs::StrToDouble (t.GetToken (4));
			cpy_aus_mat = (long) CStrFuncs::StrToDouble (t.GetToken (5));
			cpy_mdn = mdn_alt;
			cpy_schnitt = schnitt_alt;
		}
	} else return FALSE;

	Schnitt.schnitt.mdn = mdn_neu;
	Schnitt.schnitt.schnitt = schnitt_neu;
	Schnitt.dbdelete();

	Zerm.zerm.mdn = mdn_neu;
	Zerm.zerm.schnitt = schnitt_neu;
	if (Zerm.dbreadfirst_Schnitt() == 0) Zerm.dbdelete();
	while (Zerm.dbread_Schnitt() == 0)
	{
		Zerm.dbdelete();
	}

    Schnitt.schnitt.mdn = cpy_mdn;
    Schnitt.schnitt.schnitt = cpy_schnitt;
	if (Schnitt.dbreadfirst() != 0)
	{
		if (Schnitt.schnitt.kalk_bas == 0) Schnitt.schnitt.kalk_bas = Schnitt.kalk_bas;
		return FALSE;
	}
	// Gewicht des cpy_mat holen 
	Zerm.zerm.mdn = cpy_mdn;
	Zerm.zerm.schnitt = cpy_schnitt;
	Zerm.zerm.zerm_aus_mat = cpy_aus_mat;
	Zerm.zerm.zerm_mat = cpy_mat;
	Zerm.zerm.a_typ = cpy_a_typ;
	if (Zerm.dbreadfirst() != 0)
	{
		return FALSE;
	}


    Schnitt.schnitt.mdn = mdn_neu;
    Schnitt.schnitt.schnitt = schnitt_neu;
	Schnitt.schnitt.schnitt_mat = cpy_mat;
	Schnitt.schnitt.ek_gew = Zerm.zerm.zerm_gew;
	Schnitt.schnitt.ek_pr = Zerm.zerm.mat_o_b;
	CString Text;
	Text.Format (_T("Kopie aus Schnitt %d"),cpy_schnitt);
	_tcscpy_s (Schnitt.schnitt.schnitt_txt, Text);
	if (Schnitt.dbupdate() != 0)
	{
		return FALSE;
	}

	InsZerm(cpy_mat);
	return TRUE;
}

BOOL WORKDB_CLASS::InsZerm (long aus_mat)
{
	long mat = 0;
	short a_typ = 0;
	sqlin  ((short *) &cpy_mdn, SQLSHORT, 0);
	sqlin  ((long *) &cpy_schnitt, SQLLONG, 0);
	sqlin  ((long *) &aus_mat, SQLLONG, 0);
	sqlout ((long *) &mat, SQLLONG, 0);
	sqlout ((short *) &a_typ, SQLSHORT, 0);
	int ZermCursor = sqlcursor (_T("select zerm_mat,a_typ from zerm where mdn = ? and schnitt = ? and zerm_aus_mat = ? "));
	sqlopen (ZermCursor);
	int dsqlstatus = sqlfetch (ZermCursor);
	if (dsqlstatus != 0) 
	{
		sqlclose (ZermCursor);
		return FALSE;
	}
	Zerm.zerm.mdn = cpy_mdn;
	Zerm.zerm.schnitt = cpy_schnitt;
	Zerm.zerm.zerm_aus_mat = aus_mat;
	Zerm.zerm.zerm_mat = mat;
	Zerm.zerm.a_typ = a_typ;
	Zerm.dbreadfirst();
	Zerm.zerm.mdn = Schnitt.schnitt.mdn;
	Zerm.zerm.schnitt = Schnitt.schnitt.schnitt;
	_tcscpy_s (Zerm.zerm.fix, _T("F"));
	Zerm.dbupdate();
	if (Zerm.zerm.zerm_mat != Zerm.zerm.zerm_aus_mat)
	{
		InsZerm(Zerm.zerm.zerm_mat); //Rekursiv weiter hangeln
	}
	dsqlstatus = 0;
	while (dsqlstatus == 0)
	{
		dsqlstatus = sqlfetch (ZermCursor);
		if (dsqlstatus == 0)
		{
			Zerm.zerm.mdn = cpy_mdn;
			Zerm.zerm.schnitt = cpy_schnitt;
			Zerm.zerm.zerm_aus_mat = aus_mat;
			Zerm.zerm.zerm_mat = mat;
			Zerm.zerm.a_typ = a_typ;
			Zerm.dbreadfirst();
			Zerm.zerm.mdn = Schnitt.schnitt.mdn;
			Zerm.zerm.schnitt = Schnitt.schnitt.schnitt;
			_tcscpy_s (Zerm.zerm.fix, _T("F"));
			Zerm.dbupdate();
			if (Zerm.zerm.zerm_mat != Zerm.zerm.zerm_aus_mat)
			{
				InsZerm(Zerm.zerm.zerm_mat); //Rekursiv weiter hangeln
			}
		}
	}
	sqlclose (ZermCursor);
	return TRUE;
}

int WORKDB_CLASS::UpdateZermList (short dmdn,long dschnitt)
{
	    int i = 0;
		int z = 0;
	    int i1 = 0;
	    int i2 = 0;
	    int i3 = 0;
	    int i4 = 0;
	    int i5 = 0;
	    int i6 = 0;
		int iCount = 100; //count auf zerm 
		sqlin ((short *)   &dmdn,  SQLSHORT, 0);
		sqlin ((long *)   &dschnitt,  SQLLONG, 0);
		sqlout ((short *)   &iCount,  SQLLONG, 0);
         int ZermCountCursor = sqlcursor (_T("select max(lfd) from zerm   ")
                                _T("where mdn = ? and schnitt = ?  "));
		
         sqlopen(ZermCountCursor);
		 sqlfetch(ZermCountCursor);
		 iCount++;
		int *Zerm_mat = new int [iCount];
		int *Zerm_aus_mat = new int [iCount];
		int *Lfd = new int [iCount];
		int *LfdN = new int [iCount];
		int *Zerm_teil = new int [iCount];
		int *Ebene = new int [iCount];
//Initialisieren
		for (i = 0; i < iCount; i ++)
		{
			Zerm_mat[i] = -1;
			Zerm_aus_mat[i] = -1;
			Lfd[i] = -1;
			LfdN[i] = -1;
			Zerm_teil[i] = -1;
			Ebene[i] = -1;
		}
//Arrays f�llen: zerm_teil,zerm_mat,zerm_aus_mat,lfd
        short dZerm_teil = -1;
		long dZerm_mat = -1;
		long dZerm_aus_mat = -1;
		short dLfd = -1;
		short dListLfd = -1;
		short dEbene = -1;
		sqlin ((short *)   &dmdn,  SQLSHORT, 0);
		sqlin ((long *)   &dschnitt,  SQLLONG, 0);
		sqlout ((short *)   &dZerm_teil,  SQLSHORT, 0);
		sqlout ((long *)   &dZerm_mat,  SQLLONG, 0);
		sqlout ((long *)   &dZerm_aus_mat,  SQLLONG, 0);
		sqlout ((short *)   &dLfd,  SQLSHORT, 0);
         int ZermCursor = sqlcursor (_T("select zerm_teil,zerm_mat,zerm_aus_mat,lfd from zerm   ")
                                _T("where mdn = ? and schnitt = ? order by lfd "));
         sqlopen(ZermCursor);
		 int dsqlstatus = sqlfetch(ZermCursor);
		 while (dsqlstatus == 0)
		 {
			Zerm_mat[dLfd] = dZerm_mat;
			Zerm_aus_mat[dLfd] = dZerm_aus_mat;
			Lfd[dLfd] = dLfd;
			Zerm_teil[dLfd] = dZerm_teil;
			 dsqlstatus = sqlfetch(ZermCursor);
		 }

//abarbeiten alle mit zerm_teil > 0
		for (i = 0, z = 0; i < iCount; i++)
		{
			if (LfdN[i] == -1) 
			{
				LfdN[i] = z++;
				Ebene[i] = 0;
//1. Ebene
				if (Zerm_teil[i] > 0)
				{
					for (i1 = i+1;  i1 < iCount; i1++)
					{
						if (Zerm_aus_mat[i1] == Zerm_mat[i])
						{
							LfdN[i1] = z++;
							Ebene[i1] = 1;
//2. Ebene
				if (Zerm_teil[i1] > 0)
				{
					for (i2 = i1+1;  i2 < iCount; i2++)
					{
						if (Zerm_aus_mat[i2] == Zerm_mat[i1])
						{
							LfdN[i2] = z++;
							Ebene[i2] = 2;
//3. Ebene
				if (Zerm_teil[i2] > 0)
				{
					for (i3 = i2+1;  i3 < iCount; i3++)
					{
						if (Zerm_aus_mat[i3] == Zerm_mat[i2])
						{
							LfdN[i3] = z++;
							Ebene[i3] = 3;
//4. Ebene
				if (Zerm_teil[i3] > 0)
				{
					for (i4 = i3+1;  i4 < iCount; i4++)
					{
						if (Zerm_aus_mat[i4] == Zerm_mat[i3])
						{
							LfdN[i4] = z++;
							Ebene[i4] = 4;
//5. Ebene
				if (Zerm_teil[i4] > 0)
				{
					for (i5 = i4+1;  i5 < iCount; i5++)
					{
						if (Zerm_aus_mat[i5] == Zerm_mat[i4])
						{
							LfdN[i5] = z++;
							Ebene[i5] = 5;
//6. Ebene
				if (Zerm_teil[i5] > 0)
				{
					for (i6 = i5+1;  i6 < iCount; i6++)
					{
						if (Zerm_aus_mat[i6] == Zerm_mat[i5])
						{
							LfdN[i6] = z++;
							Ebene[i6] = 6;
//6. Ebene
						}
					}
				}
//5. Ebene
						}
					}
				}
//4. Ebene
						}
					}
				}
//3. Ebene
						}
					}
				}
//2. Ebene
						}
					}
				}
//1. Ebene
						}
					}
				}
						


				}
			}

		for (i = 0; i < iCount; i ++)
		{
			if (Zerm_mat[i] > 0)
			{
				dZerm_mat = Zerm_mat[i];
				dZerm_aus_mat = Zerm_aus_mat[i];
				dLfd = Lfd[i];
				dListLfd = LfdN[i];
				dEbene = Ebene[i];

				sqlin ((short *)   &dListLfd,  SQLSHORT, 0);
				sqlin ((short *)   &dEbene,  SQLSHORT, 0);

				sqlin ((short *)   &dmdn,  SQLSHORT, 0);
				sqlin ((long *)   &dschnitt,  SQLLONG, 0);
				sqlin ((long *)   &dZerm_aus_mat,  SQLLONG, 0);
				sqlin ((long *)   &dZerm_mat,  SQLLONG, 0);
				sqlin ((short *)   &dLfd,  SQLSHORT, 0);
				dsqlstatus = sqlcomm (_T("update zerm set list_lfd = ?, list_ebene = ? where mdn = ? ")
							_T("and schnitt = ? and zerm_aus_mat = ? and zerm_mat = ? and lfd = ? "));

			}
		}
		delete Zerm_mat;
		delete Zerm_aus_mat;
		delete Lfd;
		delete LfdN;
		delete Zerm_teil;
		delete Ebene;
	return 0;
}

int WORKDB_CLASS::VKVorhanden (short mdn, long schnitt,long aus_mat)
{
	//z.Z nicht benutzt
				sqlin ((short *)   &mdn,  SQLSHORT, 0);
				sqlin ((long *)   &schnitt,  SQLLONG, 0);
				sqlin ((long *)   &aus_mat,  SQLLONG, 0);
				return sqlcomm (_T("select mdn from zerm where mdn = ? ")
							_T("and schnitt = ? and zerm_aus_mat = ? and a_typ = 10 "));

}

int WORKDB_CLASS::SetzeDefault_aus_a_typ (short mdn,long schnitt )
{

    sqlin ((short *) &mdn,SQLSHORT,0);
    sqlin ((long *) &schnitt,SQLLONG,0);

	return sqlcomm (_T("update zerm set zerm_aus_a_typ = 1 where mdn = ? ")
							_T("and schnitt = ? and (zerm_aus_a_typ is null or zerm_aus_a_typ = 0) "));
}



