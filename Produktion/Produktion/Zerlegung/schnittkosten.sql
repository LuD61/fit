create table "fit".schnittkost_g   
  (
    mdn smallint,
    schnitt integer,
    grundkost_art integer,  -- Kostenart f�r Zerlegegrundkosten
    grundkostenwert decimal(8,2),   
    grobkost_art integer,  -- Kostenart f�r Grobteilzerlegung 
    grobkostenwert decimal(8,2)
 
  ) in fit_dat extent size 32 next size 256 lock mode row;
create unique index "fit".i01schnittkost_g on "fit".schnittkost_g (mdn,schnitt);

create table "fit".schnittkost_f    
  (                      --Kosten Feinzerlegung (korrespondiert mit ptab feinzerlegung)
  			 -- wenn ptab nicht da, wird stattdessen a_kz genommen
    mdn smallint,
    schnitt integer,
    fein_ptlfnr integer,
    fein_ptwert char(3),
    fein_kost_art integer,
    fein_wert decimal(8,2)
    ) in fit_dat extent size 32 next size 256 lock mode row;   
    
create unique index "fit".i01schnittkost_f on "fit".schnittkost_f (mdn,schnitt,fein_ptwert);
    




