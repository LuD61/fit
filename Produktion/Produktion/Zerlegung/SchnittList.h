#ifndef _SCHNITT_LIST_DEF
#define _SCHNITT_LIST_DEF
#pragma once

class CSchnittList
{
public:
	long schnitt;
	CString schnitt_txt;
	CString schnitt_mat;

	CSchnittList(void);
	CSchnittList(long, LPTSTR, LPTSTR);
	~CSchnittList(void);
};
#endif
