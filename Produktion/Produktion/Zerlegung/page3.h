#pragma once
#include "DbPropertyPage.h"
#include "NumEdit.h"
#include "TextEdit.h"
#include "TextEdit.h"
#include "CtrlGrid.h"
#include "sys_par.h"
#include "mdn.h"
#include "schlaklkk.h"
#include "adr.h"
#include "Schnitt.h"
#include "zerm.h"
#include "page1.h"
#include "zerm_mat_v.h"
#include "a_mat.h"
#include "a_bas.h"
#include "a_varb.h"
#include "a_kalk_mat.h"
#include "a_kalkhndw.h"
#include "DbUniCode.h"
#include "mo_progcfg.h"
#include "FormTab.h"
#include "afxwin.h"
#include "ChoiceMdn.h"
#include "ChoiceKalk.h"
#include "ChoiceSchnitt.h"
#include "ChoiceKostArt.h"
#include "kost_art_zerl.h"
#include "ChoiceA.h"
#include "Controls.h"
#include "WorkDB.h"
#include "WorkPrint.h"
#include "ZerlTreeData.h"
#include "Page1.h"
#include "Schnittkostg.h"
#include "Schnittkostf.h"
#include "PageUpdate.h"
#include "UpdateEvent.h"


// CPage3-Dialogfeld
#define IDC_GRUNDKOARTCHOICE 3002
#define IDC_GROBKOARTCHOICE 3003
#define IDC_VMCHOICE 3004
#define IDC_VTCHOICE 3005
#define IDC_ZACHOICE 3006

class CPage3 : public CDbPropertyPage
{
	DECLARE_DYNAMIC(CPage3)

public:
	CPage3();
	CPageUpdate *PageUpdate;
	virtual ~CPage3();

// Dialogfelddaten
	enum { IDD = IDD_PAGE3 };
	PROG_CFG Cfg;
	CCtrlGrid CtrlGrid;
	CCtrlGrid MdnGrid;
	CCtrlGrid SchnittGrid;
	CCtrlGrid ZerlKostenGrid;
	CCtrlGrid GrundKoartGrid;
	CCtrlGrid GrobKoartGrid;
	CCtrlGrid ZerlKostenFeinGrid;
	CCtrlGrid VMGrid;
	CCtrlGrid VTGrid;
	CCtrlGrid ZAGrid;
	CFormTab Form;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog();
	virtual void OnSize (UINT, int, int);
     virtual BOOL OnReturn ();
       virtual BOOL OnKeyup ();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 
	virtual BOOL Write (BOOL);
	virtual BOOL Back ();
	virtual BOOL Print (HWND);



	DECLARE_MESSAGE_MAP()
public:
	CWnd *Frame;
	virtual void UpdatePage ();
	virtual void GetPage ();
	virtual BOOL OnSetActive ();
	virtual void OnDelete ();
	void GrundKostArtSpeichern();
	void GrobKostArtSpeichern();
	void VMKostArtSpeichern();
	void VTKostArtSpeichern();
	void ZAKostArtSpeichern();
	void GrundKostArtLesen();
	void GrobKostArtLesen();
	void VMKostArtLesen();
	void VTKostArtLesen();
	void ZAKostArtLesen();
	CControls HeadControls;
	CControls PosControls;
	MDN_CLASS *Mdn;
	SCHNITT_CLASS *Schnitt;
	ADR_CLASS *MdnAdr;
	CFont Font;
	CFont lFont;
	CPage1 *Basis;
	SCHNITTKOST_G_CLASS SchnittKost_g;
	SCHNITTKOST_F_CLASS SchnittKost_f;


public:

	ZERM_CLASS *Zerm;

    void OnSchnittchoice(); 
    void OnKostArtchoice(); 
	KOST_ART_ZERL_CLASS kost_art_zerl;
	CChoiceKostArt *ChoiceKostArt;
	CString Where;
	CNumEdit m_Mdn;
	CButton m_MdnChoice;
public:
	CEdit m_MdnName;
	CNumEdit m_Schnitt;
	CButton m_SchnittChoice;

	CEdit m_SchnittTxt;
	CStatic m_LMdn;
	CStatic m_LSchnitt;
	CStatic m_ZerlkostGroup;
	CStatic m_LGrund;
	CNumEdit m_GrundKoart;
	CEdit m_GrundKoartBez;
	CNumEdit m_GrundKoartWert;
	CButton m_GrundKoartChoice;
	CButton m_GrobKoartChoice;
	CButton m_VMChoice;
	CButton m_VTChoice;
	CButton m_ZAChoice;
	CStatic m_LKoart;
	CStatic m_LWert;
	CStatic m_LGrob;
	CNumEdit m_GrobKoart;
	CEdit m_GrobKoartBez;
	CNumEdit m_GrobKoartWert;
	CStatic m_FeinzerlGroup;
	CComboBox m_FeinCombo1;
	CNumEdit m_VM;
	CEdit m_VMBez;
	CNumEdit m_VMWert;
	CComboBox m_FeinCombo2;
	CNumEdit m_VT;
	CEdit m_VTBez;
	CNumEdit m_VTWert;
	CComboBox m_FeinCombo3;
	CNumEdit m_ZA;
	CEdit m_ZABez;
	CNumEdit m_ZAWert;
	CComboBox m_FeinCombo4;
	CNumEdit m_FeinKoart4;
	CEdit m_FeinKoart4Bez;
	CNumEdit m_FeinKoart4Wert;
    void OnKostArtChoice(); 
    void OnKostArtChoiceGrund(); 
    void OnKostArtChoiceGrob(); 
    void OnKostArtChoiceVM(); 
    void OnKostArtChoiceVT(); 
    void OnKostArtChoiceZA(); 
	BOOL ModalChoice;
public:
	afx_msg void OnEnKillfocusGrundkoartWert();
public:
	afx_msg void OnEnKillfocusGrobkoartWert();
public:
	afx_msg void OnEnKillfocusGrundkoart();
public:
	afx_msg void OnEnKillfocusGrobkoart();
public:
	CStatic m_LVM;
public:
	CStatic m_LVT;
public:
	CStatic m_LZA;
public:
	afx_msg void OnEnKillfocusVmWert();
public:
	afx_msg void OnEnKillfocusVtWert();
public:
	afx_msg void OnEnKillfocusZaWert();
public:
	afx_msg void OnEnSetfocusSchnittTxt();
};
