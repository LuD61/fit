#ifndef _A_SCHL_DEF
#define _A_SCHL_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct A_SCHL {
   short          mdn;
   short          fil;
   double         a;
   TCHAR          a_k_bez[13];
   TCHAR          mo_kz[11];
   TCHAR          gatt_kz[2];
   double         grund_pr;
   long           kost_st;
   TCHAR          hdkl[4];
   TCHAR          tier_kz[3];
   short          ag;
   double         grossvieheinh;
};
extern struct A_SCHL a_schl, a_schl_null;

#line 8 "a_schl.rh"


class A_SCHL_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               A_SCHL a_schl;  
               A_SCHL_CLASS () : DB_CLASS ()
               {
               }
};
#endif
