#ifndef _KOST_ART_ZERL_DEF
#define _KOST_ART_ZERL_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct KOST_ART_ZERL {
   short          mdn;
   short          fil;
   short          kost_art_zerl;
   TCHAR          kost_art_zerl_bz[25];
   short          delstatus;
   long           erl_kto;
   TCHAR          vkost_rechb[4];
   double         vkost_wt;
   double         faktor;
   DATE_STRUCT    dat;
   TCHAR          pers_nam[9];
   double         vkost_fix;
};
extern struct KOST_ART_ZERL kost_art_zerl, kost_art_zerl_null;

#line 8 "kost_art_zerl.rh"

class KOST_ART_ZERL_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               KOST_ART_ZERL kost_art_zerl;  
               KOST_ART_ZERL_CLASS () : DB_CLASS ()
               {
               }
};
#endif
