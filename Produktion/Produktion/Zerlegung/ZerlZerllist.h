#pragma once
#include "SplitPane.h"
#include "ZerlListDlg.h"
#include "Zerl.h"
#include "Zerm.h"
#include "Page1.h"
#include "UpdateEvent.h"
#include "PageUpdate.h"



// CZerlZerllist-Dialogfeld

class CZerlZerllist : public CDialog, public CUpdateEvent
{
	DECLARE_DYNAMIC(CZerlZerllist)

public:
	CZerlZerllist(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CZerlZerllist();

// Dialogfelddaten
	enum { IDD = IDD_ZERL_ZERLLIST };
	CZerlListDlg m_ZerlList;
	CZerl m_Zerl;
	CPage1 *Page1;
	ZERM_CLASS *Zerm;
	CSplitPane SplitPane;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();
	virtual void OnSize (UINT, int, int);


	DECLARE_MESSAGE_MAP()
public:
	CPageUpdate *PageUpdate;
	void AddUpdateEvent (CUpdateEvent *UpdateEvent);
	void Update(short);

};
