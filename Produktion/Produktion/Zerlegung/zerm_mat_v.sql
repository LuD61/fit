{ TABLE "fit".zerm_mat_v row size = 109 number of columns = 17 index size = 24 }
create table "fit".zerm_mat_v 
  (
    mdn smallint,
    schnitt smallint,
    zerm_aus_mat integer,
    zerm_mat integer,
    fwp decimal(8,2),
    zerm_gew_ant decimal(8,2),
    mat_o_b decimal(9,3),
    hk_vollk decimal(9,3),
    hk_teilk decimal(9,3),
    dm_100 decimal(11,3),
    kosten_zutat decimal(9,3),
    kosten_vpk decimal(9,3),
    teilkosten_abs decimal(9,3),
    vk_pr decimal(9,3),
    kost_mat integer,
    kost_mat_bz char(24),
    a_typ smallint,
    pr_vk_eh decimal(9,4) 

  ) with crcols  in fit_dat extent size 32 next size 256 lock mode row;
revoke all on "fit".zerm_mat_v from "public";

create unique index "fit".i01zerm_mat_v on "fit".zerm_mat_v (mdn,
    schnitt,zerm_mat,zerm_aus_mat,a_typ);




