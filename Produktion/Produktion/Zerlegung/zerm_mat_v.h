#ifndef _ZERM_MAT_V_DEF
#define _ZERM_MAT_V_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct ZERM_MAT_V {
   short          mdn;
   long           schnitt;
   long           zerm_aus_mat;
   long           zerm_mat;
   double         fwp;
   double         zerm_gew;
   double         zerm_gew_ant;
   double         mat_o_b;
   double         hk_vollk;
   double         hk_teilk;
   double         dm_100;
   double         kosten_zutat;
   double         kosten_vpk;
   double         teilkosten_abs;
   double         vk_pr;
   long           kost_mat;
   char           kost_mat_bz[25];
   short          a_typ;
   double         pr_vk_eh;
   double         sk_vollk;
   double         sk_teilk;
   double         fil_ek_vollk;
   double         fil_ek_teilk;
   double         fil_vk_vollk;
   double         fil_vk_teilk;
   double         gh1_vollk;
   double         gh1_teilk;
   double         gh2_vollk;
   double         gh2_teilk;
//Zusätzlich in die struktur (ohne DB)
   TCHAR          _zerm_mat_bz1[25];
   TCHAR          _zerm_mat_bz2[25];
};
extern struct ZERM_MAT_V zerm_mat_v, zerm_mat_v_null;

#line 8 "zerm_mat_v.rh"

class ZERM_MAT_V_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               ZERM_MAT_V zerm_mat_v;  
               ZERM_MAT_V_CLASS () : DB_CLASS ()
               {
               }
};
#endif
