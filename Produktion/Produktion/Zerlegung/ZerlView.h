#pragma once
#include "DbFormView.h"
#include "DbPropertySheet.h"
#include "ZerlSheet.h"
#include "Page1.h"
#include "Page2.h"
#include "Page3.h"
#include "Page4.h"
#include "PageUpdate.h"
#include "mo_progcfg.h"



// CZerlView-Formularansicht 

class CZerlView : public DbFormView, 
	                 CPageUpdate
{
	DECLARE_DYNCREATE(CZerlView)

protected:
	CZerlView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CZerlView();

public:
	enum { IDD = IDD_ZERLEGUNG };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	virtual void OnInitialUpdate(); // Erster Aufruf nach Erstellung
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 
    afx_msg void OnSize(UINT, int, int);

	DECLARE_MESSAGE_MAP()
public:
    enum
	{
		START_NORMAL = 0,
		START_MAXIMIZED = 1,
		START_FITSIZE = 2,
	};

	PROG_CFG Cfg;
	int StartSize;
	int tabx;
	int taby;
	CZerlSheet dlg;
	CPage1 Page1;
	CPage2 Page2;
	CPage3 Page3;
	CPage4 Page4;
	HBRUSH hBrush;
	HBRUSH staticBrush;
	void DeletePropertySheet ();
	afx_msg void OnLanguage();
	afx_msg void OnFileSave();
	afx_msg void OnBack();
	void ReadCfg ();

	afx_msg void OnDelete();
	afx_msg void OnInsert();
	afx_msg void OnEditCopy();
	afx_msg void OnEditPaste();
	afx_msg void OnDeleteall();
	afx_msg void OnFilePrint();
	afx_msg void OnPrintAll();
	afx_msg void OnFirst();
	afx_msg void OnUpdateFirst(CCmdUI *pCmdUI);
	afx_msg void OnPrior();
	afx_msg void OnUpdatePrior(CCmdUI *pCmdUI);
	afx_msg void OnNext();
	afx_msg void OnUpdateNext(CCmdUI *pCmdUI);
	afx_msg void OnLast();
	afx_msg void OnUpdateLast(CCmdUI *pCmdUI);
	afx_msg void OnEanChoice();
	afx_msg void OnUpdateEanChoice(CCmdUI *pCmdUI);
	afx_msg void OnPlu();
	afx_msg void OnUpdatePlu(CCmdUI *pCmdUI);
	afx_msg void OnDlgBackground();
	afx_msg void OnChoicebackground();
	afx_msg void OnUpdateChoicebackground(CCmdUI *pCmdUI);
	afx_msg void OnChoiceaDefault();
	afx_msg void OnUpdateChoiceaDefault(CCmdUI *pCmdUI);
	afx_msg void OnChoiceeanDefault();
	afx_msg void OnUpdateChoiceeanDefault(CCmdUI *pCmdUI);
	afx_msg void OnChoiceakrzDefault();
	afx_msg void OnUpdateChoiceakrzDefault(CCmdUI *pCmdUI);
	afx_msg void OnFlatLayout();
	afx_msg void OnUpdateFlatLayout(CCmdUI *pCmdUI);
	afx_msg void OnLiefbest();
	afx_msg void OnUpdateLiefbest(CCmdUI *pCmdUI);
	afx_msg void OnChoiceliefbestDefault();
	afx_msg void OnUpdateChoiceliefbestDefault(CCmdUI *pCmdUI);
	afx_msg void OnDlgframeBackground();
	afx_msg void OnChoiceOk();
	afx_msg void OnUpdateChoiceOk(CCmdUI *pCmdUI);

// Abgeleitete Methoden von PageUpdate
	virtual void Show ();
	virtual void Get ();
	virtual void Back ();
	virtual BOOL Write ();
	virtual void Page2Write ();
	virtual void Delete ();
	virtual void UpdateAll ();
	afx_msg void OnUpdateFileSave(CCmdUI *pCmdUI);
	afx_msg void OnUpdateDateiL(CCmdUI *pCmdUI);
	afx_msg void OnUpdatePaste(CCmdUI *pCmdUI);
	afx_msg void OnUpdateCopy(CCmdUI *pCmdUI);
	afx_msg void OnInfo();
	afx_msg void OnUpdateInfo(CCmdUI *pCmdUI);
};


