#ifndef _SCHNITTKOST_F_DEF
#define _SCHNITTKOST_F_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct SCHNITTKOST_F {
   short          mdn;
   long           schnitt;
   long           fein_ptlfnr;
   TCHAR          fein_ptwert[4];
   long           fein_kost_art;
   double         fein_wert;
};
extern struct SCHNITTKOST_F schnittkost_f, schnittkost_f_null;

#line 8 "schnittkostf.rh"

class SCHNITTKOST_F_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               SCHNITTKOST_F schnittkost_f;  
				TCHAR          fein1_ptwert[4];
				long           fein1_kost_art;
				TCHAR          fein1_ptbez[32];
				double         fein1_wert;
				TCHAR          fein2_ptwert[4];
				long           fein2_kost_art;
				TCHAR          fein2_ptbez[32];
				double         fein2_wert;
				TCHAR          fein3_ptwert[4];
				long           fein3_kost_art;
				TCHAR          fein3_ptbez[32];
				double         fein3_wert;
				TCHAR          fein4_ptwert[4];
				long           fein4_kost_art;
				TCHAR          fein4_ptbez[32];
				double         fein4_wert;

    	   SCHNITTKOST_F_CLASS () : DB_CLASS ()
               {
				   strcpy_s(fein1_ptwert,"");
				   fein1_kost_art = 0;
				   strcpy_s(fein1_ptbez,"");
				   fein1_wert = 0.0;

				   strcpy_s(fein2_ptwert,"");
				   fein2_kost_art = 0;
				   strcpy_s(fein2_ptbez,"");
				   fein2_wert = 0.0;

				   strcpy_s(fein3_ptwert,"");
				   fein3_kost_art = 0;
				   strcpy_s(fein3_ptbez,"");
				   fein3_wert = 0.0;

				   strcpy_s(fein4_ptwert,"");
				   fein4_kost_art = 0;
				   strcpy_s(fein4_ptbez,"");
				   fein4_wert = 0.0;
               }
};
#endif
