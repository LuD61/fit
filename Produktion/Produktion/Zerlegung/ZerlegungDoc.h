// ZerlegungDoc.h : Schnittstelle der Klasse CZerlegungDoc
//


#pragma once

class CZerlegungDoc : public CDocument
{
protected: // Nur aus Serialisierung erstellen
	CZerlegungDoc();
	DECLARE_DYNCREATE(CZerlegungDoc)

// Attribute
public:

// Operationen
public:

// Überschreibungen
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// Implementierung
public:
	virtual ~CZerlegungDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generierte Funktionen für die Meldungstabellen
protected:
	DECLARE_MESSAGE_MAP()
};


