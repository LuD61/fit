#include <windows.h>
#include <string.h>
#include "strfkt.h"
#include "rdonly.h"


extern HFONT EzCreateFontT (HDC hdc, char * szFaceN, int iDeciPtH,
                    int iDeciPtW, int iAttrib, BOOL fLogRes);
extern BOOL DeleteObjectT (HGDIOBJ hObject);

#define EzCreateFont EzCreateFontT
#define DeleteObject DeleteObjectT

#define MAXWINCOLS 0x1000

#define WHITECOL  RGB (255,255,255)
#define BLACKCOL  RGB (0,0,0)
#define LTGRAYCOL RGB (192,192,192)
#define GRAYCOL RGB (128,128,128)
#define BLUECOL RGB (0, 0, 255)
#define GREENCOL RGB (0, 255, 0)
#define REDCOL RGB (255, 0, 0)
#define YELLOWCOL RGB (255, 255, 0)
#define DKYELLOWCOL RGB (175, 175, 0)

LONG FAR PASCAL RdOnlyProcEx(HWND,UINT,WPARAM,LPARAM);

static void RegisterColWinR (char *, COLORREF, WNDPROC);

static COLORREF wincolors [MAXWINCOLS];
static WNDPROC colorprocs [MAXWINCOLS];
static int wincolanz;
static HINSTANCE MainInst;
static TEXTMETRIC tm;
static int lmargin = 6;
static int rmargin = 5;
static BOOL MarginSet = FALSE;


WPARAM GetRDOMargins ()
{
	return MAKEWPARAM (lmargin, rmargin);
}

void UnSetRdoMargins ()
{
	MarginSet = FALSE;
}

void SetRDOMargins (LRESULT m)
{
	if (MarginSet)
	{
		return;
	}
	lmargin = LOWORD (m);
	rmargin = HIWORD (m);
	MarginSet = TRUE;
}

void SetRDONewMargins (LRESULT m)
{
	lmargin = LOWORD (m);
	rmargin = HIWORD (m);
	MarginSet = TRUE;
}

static int BkMode = TRANSPARENT;


static void RegisterColWinR (char *ColClass, COLORREF col, WNDPROC wproc)
/**
Farbwindow regostrieren.
**/
{
         int i;
         WNDCLASS wc;

         if (wproc == NULL)
         {
                     wproc = RdOnlyProcEx;
         }
         for (i = 0; i < wincolanz; i ++)
         {
                     if ((COLORREF) col == (COLORREF) wincolors [i])
                     {
                               sprintf (ColClass, "winR%d", i);
                               if (wproc == colorprocs[i]) break;
                     }
         }
     //     i = wincolanz;
         sprintf (ColClass, "winR%d", i);
         wincolors[i]  = col;
         colorprocs[i] = wproc;
         if (i < wincolanz) return;

         if (i == wincolanz && wincolanz < MAXWINCOLS - 1) wincolanz ++;

         wc.style         =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                            | CS_OWNDC;
         wc.lpfnWndProc   =  wproc;
         wc.cbClsExtra    =  0;
         wc.cbWndExtra    =  0;
         wc.hInstance     =  MainInst;
         wc.hIcon         =  LoadIcon (MainInst, "ROSIICON");
         wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
         wc.hbrBackground =  CreateSolidBrush (col);
         wc.lpszMenuName  =  "";
         wc.lpszClassName =  ColClass;

        RegisterClass(&wc);
}

struct RDO
{
	HWND hWnd;
	char *Text;
	HFONT hFont;
	DWORD BuId;
      COLORREF Color;
      COLORREF BkColor;
    DWORD Style;
    DWORD ExStyle;
};

typedef struct RDO Rdo;

static Rdo **rdo = NULL;
static int rdoanz = 0;

static COLORREF aktback = LTGRAYCOL;
static COLORREF aktvor  = BLACKCOL;

static COLORREF BkColor = LTGRAYCOL;

void ROSetBkColor (COLORREF color)
{
	BkColor = color;
}

void ROSetAktBack (COLORREF color)
{
	aktback = color;
}

void ROSetAktVor (COLORREF color)
{
	aktvor = color;
}


static void SetFont (HWND hWnd, HFONT hFont)
{
	int i;

	for (i = 0; i < rdoanz; i ++)
	{
		if (hWnd == rdo[i]->hWnd) break;
	}
	if (i >= rdoanz) return;
	rdo[i]->hFont = hFont;
}


static HFONT GetFont (HWND hWnd)
{
	int i;

	for (i = 0; i < rdoanz; i ++)
	{
		if (hWnd == rdo[i]->hWnd) break;
	}
	if (i >= rdoanz) return NULL;
	return rdo[i]->hFont;
}


static DWORD GetStyle (HWND hWnd)
{
	int i;

	for (i = 0; i < rdoanz; i ++)
	{
		if (hWnd == rdo[i]->hWnd) break;
	}
	if (i >= rdoanz) return NULL;
	return rdo[i]->Style;
}

static DWORD GetExStyle (HWND hWnd)
{
	int i;

	for (i = 0; i < rdoanz; i ++)
	{
		if (hWnd == rdo[i]->hWnd) break;
	}
	if (i >= rdoanz) return NULL;
	return rdo[i]->ExStyle;
}


HWND CreateRdOnly (HWND hMain, char *Text, int x, int y, int cx, int cy, HFONT hFont, 
			   HINSTANCE hMainInst, DWORD ID)
{
	      HWND hWnd;
          char ColClass [20];

	      MainInst = hMainInst;
          RegisterColWinR (ColClass, BkColor, RdOnlyProcEx);

          hWnd = CreateWindowEx (
			                     WS_EX_CLIENTEDGE, 
                                 ColClass,
                                 Text,     
                                 WS_CHILD | WS_VISIBLE,
                                 x, y,
                                 cx, cy,
                                 hMain,
                                 NULL,
                                 hMainInst,
                                 NULL);
           SendMessage (hWnd,
                         WM_SETFONT, (WPARAM) hFont, 0);
	       if (rdo == NULL)
		   {
			   rdo = new Rdo *[0x1000];
			   if (rdo == NULL) return hWnd;
		   }
		   rdo[rdoanz] = new Rdo; 
		   if (rdo[rdoanz] == NULL) return hWnd;
		   rdo[rdoanz]->hWnd    = hWnd;
		   rdo[rdoanz]->Text    = Text;
		   rdo[rdoanz]->hFont   = hFont;
		   rdo[rdoanz]->BuId    = 0;
		   rdo[rdoanz]->Color   = aktvor;
		   rdo[rdoanz]->BkColor = aktback;

		   if (rdoanz < 0x1000 - 1) rdoanz ++;
		   InvalidateRect (hWnd, NULL, TRUE);
		   UpdateWindow (hWnd);
           return hWnd;
}

HWND CreateRdOnlyEx (HWND hMain, char *Text, int x, int y, int cx, int cy, HFONT hFont, 
			   HINSTANCE hMainInst, DWORD ID, DWORD ExStyle, DWORD Style)
{
	      HWND hWnd;
          char ColClass [20];

	      MainInst = hMainInst;
          RegisterColWinR (ColClass, BkColor, RdOnlyProcEx);

		  cx += lmargin;
		  cx += rmargin;
          hWnd = CreateWindowEx (
			                     ExStyle, 
                                 ColClass,
                                 Text,     
                                 WS_CHILD | WS_VISIBLE | Style,
                                 x, y,
                                 cx, cy,
                                 hMain,
                                 NULL,
                                 hMainInst,
                                 NULL);
           SendMessage (hWnd,
                         WM_SETFONT, (WPARAM) hFont, 0);
	       if (rdo == NULL)
		   {
			   rdo = new Rdo *[0x1000];
			   if (rdo == NULL) return hWnd;
		   }
		   rdo[rdoanz] = new Rdo; 
		   if (rdo[rdoanz] == NULL) return hWnd;
		   rdo[rdoanz]->hWnd    = hWnd;
		   rdo[rdoanz]->Text    = Text;
		   rdo[rdoanz]->hFont   = hFont;
		   rdo[rdoanz]->BuId    = 0;
		   rdo[rdoanz]->Color   = aktvor;
		   rdo[rdoanz]->BkColor = aktback;
		   rdo[rdoanz]->Style   = Style;
		   rdo[rdoanz]->ExStyle = ExStyle;

		   if (rdoanz < 0x1000 - 1) rdoanz ++;
		   InvalidateRect (hWnd, NULL, TRUE);
		   UpdateWindow (hWnd);
           return hWnd;
}

static void print_rdonlyEx (char *text, HWND hWnd, int zeile, int spalte,
                            HFONT hFont)
/**
Text in Fenster schreiben.
**/
{
        HDC      hdc;
        PAINTSTRUCT ps;
        int x, y;
        int xchar;
        int ychar;
        DWORD Style;
        DWORD ExStyle;
        RECT rect;
        SIZE size;

        GetClientRect (hWnd, &rect);
        Style = GetStyle (hWnd); 
        ExStyle = GetExStyle (hWnd); 
        hdc = BeginPaint (hWnd, &ps);
        if (hFont)
        {
               SelectObject (hdc, hFont);
               GetTextMetrics (hdc, &tm);
               GetTextExtentPoint32 (hdc, "X", 1 , &size); 
               tm.tmAveCharWidth = size.cx;
               GetTextExtentPoint32 (hdc, text, strlen (text) , &size); 
        }
        xchar = tm.tmAveCharWidth;
        ychar = tm.tmHeight;


        if (Style & ES_RIGHT)
        {
            x = rect.right - size.cx - rmargin;
            if (Style & WS_BORDER) 
			{
				x -= (GetSystemMetrics (SM_CXBORDER) * 2);
			}
			else if (ExStyle & WS_EX_CLIENTEDGE)
			{
				x -= GetSystemMetrics (SM_CXBORDER);
			}
        }
        else
        {
            x = spalte * xchar + lmargin + rmargin;
//            x = spalte * xchar;
        }
        y = zeile * ychar;
		y += ((rect.bottom - size.cy) / 2);
        SetBkMode (hdc, BkMode);
        SetTextColor (hdc, aktvor);
        SetBkColor (hdc, aktback);
        TextOut (hdc, x, y, text, strlen (text));
        EndPaint (hWnd, &ps);
        return;
}

static void disp_rdonlyEx (HWND hWnd)
/**
Bei WM_PAINT-Meldung RDONLY-Felder neu zeichnen.
**/
{
    int i;

	for (i = 0; i < rdoanz; i ++)
	{
		if (hWnd == rdo[i]->hWnd) break;
	}
	if (i >= rdoanz) return;


	aktvor  =  rdo[i]->Color;
	aktback =  rdo[i]->BkColor;
    print_rdonlyEx (rdo[i]->Text, 
 		            rdo[i]->hWnd,
                    0, 0,
		            rdo[i]->hFont);
}

static void SetText (HWND hWnd, char *Text)
/**
Bei WM_PAINT-Meldung RDONLY-Felder neu zeichnen.
**/
{
    int i;

	for (i = 0; i < rdoanz; i ++)
	{
		if (hWnd == rdo[i]->hWnd) break;
	}
	if (i >= rdoanz) return;

    rdo[i]->Text = Text; 
}


LONG FAR PASCAL RdOnlyProcEx (HWND hWnd,UINT msg, WPARAM wParam,LPARAM lParam)
{
        static HFONT hFont = 0;

        switch(msg)
        {
              case WM_PAINT :
                       disp_rdonlyEx (hWnd);
                       break;
              case WM_SETFONT :
                       hFont = (HFONT) wParam;
                       SetFont (hWnd, hFont);
                       return 0;
              case WM_SETTEXT :
                       SetText (hWnd, (char *) lParam);
                       InvalidateRect (hWnd, NULL, TRUE);
                       break;
               case WM_DESTROY :
                       break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}



