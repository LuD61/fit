#ifndef _SPEZDLG_DEF
#define _SPEZDLG_DEF
#include "dlg.h"
#include "itprog.h"
#include "itfont.h"
#include "formfield.h"
#include "listdlg.h"
#include "maschlst.h"
#include "phaselst.h"
#include "perslst.h"
#include "erfassunglst.h"
//#include "mo_txt.h"
#include "Work.h"
#include "cmask.h"

#define MDN_CTL 1801
#define FIL_CTL 1802

#define ABT_CTL 1803
#define STND_CTL 1804
#define KW_CTL 1805
#define JR_CTL 1806
#define BEARB_CTL 1807
#define CHECK_MO_CTL 1808
#define CHECK_DI_CTL 1809
#define CHECK_MI_CTL 1810
#define CHECK_DO_CTL 1811
#define CHECK_FR_CTL 1812
#define CHECK_SA_CTL 1813
#define CHECK_SO_CTL 1814
#define DATVON_CTL 1815
#define DATBIS_CTL 1816

#define IDM_CHOISE 2002
#define IDM_QUIK   2003

#define HEADCTL 700
#define POSCTL 701

#define RAISEDBORDER 0
#define HIGHBORDER 1
#define LOWBORDER 2
#define HIGHCOLBORDER 3
#define LOWCOLBORDER 4
#define LINEBORDER 5



#define IDM_LIST 2001
#define IDM_DOCKLIST 2003
#define IDM_INSERT 2004
#define IDM_DELALL 5000
#define IDM_CHOISEPRINTER 2016
#define IDM_MALIEF 2017
#define IDM_MAA 2018
#define IDM_SCRPRINT 2019
#define IDM_ERFMODE 2020
#define IDM_ARTSCROLL 2021
#define IDM_MASCHENTER 2022

#define ENTERHEAD 0

class SpezDlg : virtual public DLG 
{
          private :
             static mfont *Font;
             static CFIELD *_fHead [];
             static CFIELD *_fHeadM [];
             static CFIELD *_fPos [];
             static CFORM fHead;
             static CFORM fHeadM;
             static CFORM fPos;
             static CFIELD *_fPosM [];
             static CFORM fPosM;
             static CFIELD *_fPosE [];
             static CFORM fPosE;
             static CFIELD *_fPosP [];
             static CFORM fPosP;
             static CFIELD *_fPosPers [];
             static CFORM fPosPers;
             static CFIELD *_fcField [];
             static CFORM fcForm;
             static CFIELD *_fcField0 [];
             static CFORM fcForm0;
             static CFIELD *_fcFieldM [];
             static CFORM fcFormM;
             static CFIELD *_fcField0M [];
             static CFORM fcForm0M;
             static CFIELD *_fcFieldP [];
             static CFORM fcFormP;
             static CFIELD *_fcField0P [];
             static CFORM fcForm0P;

             static CFIELD *_fcFieldE [];
             static CFORM fcFormE;
             static CFIELD *_fcField0E [];
             static CFORM fcForm0E;

             static CFIELD *_fcFieldPers [];
             static CFORM fcFormPers;
             static CFIELD *_fcField0Pers [];
             static CFORM fcForm0Pers;


             static ItProg *After [];
             static ItProg *Before [];
             static ItFont *itFont [];
             static ItFont *RdoFont [];
             static ItFont *InfoFont [];
             static FORMFIELD *dbfields [];
             static FORMFIELD *dbheadfields [];
             static char *EnableHead[];
             static char *EnableHeadFil[];
             static char *EnablePos [];
 			 static BitImage ImageDelete;
			 static BitImage ImageInsert;
             int    BorderType;
             static Work work;
             static BOOL WriteOK;
             static char *HelpName;
             static BOOL NewRec;
             static HBITMAP ListBitmap;
             static HBITMAP ListMask;
             static HBITMAP ListBitmapi;
			 static BitImage ImageList;
			 static COLORREF Colors[];
			 static COLORREF BkColors[];
             CFORM *Toolbar2;

             static SpezDlg *ActiveSpezDlg;
             static int pos;
             static char ptwert[];
             static BOOL LockPage;
             LISTDLG *ListDlg;
             MASCHLST *MaschLst;
             PHASELST *PhaseLst;
             PERSLST *PersLst;
             ERFLST *ErfLst;
             BOOL ColorSet;
             BOOL LineSet;
             HWND hwndCombo1;
             HWND hwndCombo2;
             char **Combo1;
             char **Combo2;
             HWND *hwndList;
             BOOL DockList;
             BOOL DockMode;
             char AktDatS [12];
             long AktDat;
             short AktPhase;
             char AktPhaseBz [20];
             long AktMaschNr;
             long AktPersNr;
             BOOL MaschSel;
             BOOL PersSel;

          public :

            static int ListLines;
            static COLORREF ListColor;
            static COLORREF ListBkColor;
            static BOOL ListButtons;
            static int ButtonSize;
            static HBITMAP SelBmp;
            static int MeKzDefault;
            static COLORREF SysBkColor;

            static char *ErfInfoMe;
            static char *ErfInfoChg;
            static char *ErfInfoAuf;
            static char ErfInfoBu [];

            void SethwndList (HWND *hwndList)
            {
                this->hwndList = hwndList;
            }

            HWND *GethwndList (void)
            {
                return hwndList;
            }

            void SetToolbar2 (CFORM *Toolbar2)
            {
                this->Toolbar2 = Toolbar2;
            }

            CFORM *GetToolbar2 (void)
            {
                return Toolbar2;
            }

            void SetDockList (BOOL DockList)
            {
                this->DockList = DockList;
            }

            BOOL GetDockList (void)
            {
                return DockList;
            }

            void SetDockMode (BOOL DockMode)
            {
                this->DockMode = DockMode;
            }

            BOOL GetDockMode (void)
            {
                return DockMode;
            }

 	        SpezDlg (int, int, int, int, char *, int, BOOL);
 	        SpezDlg (int, int, int, int, char *, int, BOOL, int);
 	        SpezDlg (int, int, int, int, char *, int, BOOL, int, int);
 	        void Init (int, int, int, int, char *, int, BOOL);
            BOOL NextPage (void);
            BOOL PriorPage (void);
            BOOL OnPreTranslateMessage (MSG *);
            BOOL OnKeyDown (void);
            BOOL OnKeyUp (void);
            BOOL OnKey1 (void);
            BOOL OnKey2 (void);
            BOOL OnKey3 (void);
            BOOL OnKey4 (void);
            BOOL OnKey5 (void);
            BOOL OnKey6 (void);
            BOOL OnKey7 (void);
            BOOL OnKey9 (void);
            BOOL OnKey10 (void);
            BOOL OnKey11 (void);
            BOOL OnKey12 (void);
            BOOL OnKeyDelete (void);
            BOOL OnKeyInsert (void);
            BOOL OnKeyPrior (void);
            BOOL OnKeyNext (void);
            BOOL OnKeyHome (void);
            BOOL OnKeyEnd (void);
            BOOL OnKeyReturn (void);
            BOOL OnPaint (HWND,HDC,UINT,WPARAM,LPARAM);
            BOOL OnActivate (HWND,UINT,WPARAM,LPARAM);
            BOOL OnCommand (HWND,UINT,WPARAM,LPARAM);
            BOOL OnSysCommand (HWND,UINT,WPARAM,LPARAM);
            BOOL OnTcnSelChange (HWND,UINT,WPARAM,LPARAM);
            BOOL OnDestroy (HWND,UINT,WPARAM,LPARAM);
            BOOL OnMove (HWND,UINT,WPARAM,LPARAM);
            BOOL OnCloseUp (HWND,UINT,WPARAM,LPARAM);
            BOOL ChangeDockList (void);
            BOOL ChangeMatrix (void);
            BOOL ChangeScrPrint (void);
            BOOL ChangeArtScroll (void);
            void PrintComment (char *);
            BOOL EnterList (void);
            BOOL EnterMaschLst (void);
            BOOL EnterPhaseLst (void);
            BOOL EnterPersLst (void);
            BOOL EnterErfLst (void);
            HWND OpenWindow (HANDLE, HWND);
            HWND OpenScrollWindow (HANDLE, HWND);
            HWND OpenTabWindow (HANDLE, HWND, char *);
            HWND OpenScrollTabWindow (HANDLE, HWND, char *);
            void SetWinBackground (COLORREF);
            void ToClipboard (void);
            void FromClipboard (void);
            void Help (void);
            BOOL ShowMdn (void);
            BOOL ShowFil (void);
            BOOL ShowAbt (void);
            void CallInfo (void);
            void SetListColors (COLORREF, COLORREF);
            static int ReadMdn (void);
            static int ReadFil (void);
            static int ReadAbt (void);
            static int setzeSperre (void);
            static int EnterPos (void);
            static int GetPtab (void);
            HWND SetToolCombo (int, int, int, int);
            void SetComboTxt (HWND, char **, int, int);
			void ChoiseCombo1 (void);
			void ChoiseCombo2 (void);
            void SetListLines (int);
            void ChangeF6 (DWORD);
            void DestroyPos (void);
            void SetPos (void);
            void SetListMeMode (int);
            void SwitchListMeMode (void);
            void ShowList (void);
            void ShowMaschLst (void);
            void ShowPhaseLst (void);
            void ShowPersLst (void);
            void ShowErfLst (void);
            void SetChargen (double, double, double);
            void SetMaschPers (char*, char*,char*,char*,double, long, long);
            short SetSperre (char*, double, short);
            short SetSperreErf (char*, double, short);
            int SchreibeZusatz (char*, double, short, double);
            void SetBearb (BOOL);
            void SetBearbR (BOOL);
			int GetCheckMo(void);
			int GetCheckDi(void);
			int GetCheckMi(void);
			int GetCheckDo(void);
			int GetCheckFr(void);
			int GetCheckSa(void);
            void ProcessMessages (void);
            void HoleStnd (void);
			void SetTabStack(short,short,short,short);
};
#endif
