#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include <string.h>
#include "comcthlp.h"
#include "itemc.h"
#include "wmaskc.h"
#include "mo_meld.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_intp.h"
#include "listclex.h"
#include "inflib.h"
#include "listenter.h"
#include "dlg.h"

#ifdef _DEBUG
#include <crtdbg.h>
extern _CrtMemState memory;
#endif

#define MAXLEN 40
#define MAXPOS 5000
#define LPLUS 1

//BOOL LISTENTER::cfgOK = FALSE;
BOOL LISTENTER::NoRecNr = FALSE;

HWND LISTENTER::MessWindow = NULL;
char LISTENTER::MessText [256];
BOOL LISTENTER::NoRecNrSet = FALSE;

void LISTENTER::SetMessWindow (HWND MessW)
{
     MessWindow = MessW;
}

HWND LISTENTER::GetMessWindow (void)
{
     return MessWindow;
}


void LISTENTER::addField (form * frm, field * feld, int pos, int len, int space)
{
	int i;
	int plus;

	frm->fieldanz ++;
	plus = len + space;

	feld->pos[1] = frm->mask[pos].pos[1];
	for (i = frm->fieldanz - 1; i > pos; i --)
	{
		memcpy (&frm->mask[i], &frm->mask[i - 1], sizeof (field));
		frm->mask[i].pos[1] += plus;
	}
	memcpy (&frm->mask[i], feld, sizeof (field));
}


int LISTENTER::addFieldName (form * frm, field * feld, char *name, int space)
{
	int pos;
	int len;

	len = feld->length;
	pos = GetItemPos (frm, name);
	if (pos == -1)
	{
		return pos;
	}
	addField (frm, feld, pos, len, space);
	return pos;
}


void LISTENTER::DelFormField (form *frm, int pos)
/**
Feld aus Dataform herausnehmen.
**/
{
	         int i;
			 int diff;
			 int pos1, pos2;

			 pos1 = frm->mask[pos].pos[1];
			 if (pos < frm->fieldanz - 1)
			 {
				 pos2 = frm->mask[pos + 1].pos[1];
				 diff = max (0, pos2 - pos1);
			 }
			 else
			 {
				 diff = frm->mask[pos].length;
				 if (frm != dataform) diff ++;
			 }
			 if (diff == 0)
			 {
				 diff = frm->mask[pos].length;
				 if (frm != dataform) diff ++;
			 }

			 frm->fieldanz --;

			 for (i = pos; i < frm->fieldanz; i ++)
			 {
				 memcpy ((char *) &frm->mask[i], (char *) &frm->mask[i + 1], sizeof (field));
			 }

			 for (i = 0; i < frm->fieldanz; i ++)
			 {
				 if (frm->mask[i].pos[1] > pos1)
				 {
					 frm->mask[i].pos[1] -= diff;
				 }
			 }
}


void LISTENTER::DelFormFieldEx (form *frm, int pos, int diff)
/**
Feld aus Dataform herausnehmen.
**/
{
	         int i;
			 int pos1;

			 pos1 = frm->mask[pos].pos[1];
			 frm->fieldanz --;

			 for (i = pos; i < frm->fieldanz; i ++)
			 {
				 memcpy ((char *) &frm->mask[i], (char *) &frm->mask[i + 1], sizeof (field));
			 }

			 for (i = 0; i < frm->fieldanz; i ++)
			 {
				 if (frm->mask[i].pos[1] > pos1)
				 {
					 frm->mask[i].pos[1] -= diff;
				 }
			 }
}


void LISTENTER::DelListField (char *Item)
{
	         int pos, diff;

		     diff = 0;
		     pos = GetItemPos (ubform, Item);
			 if (pos > -1 && pos < ubform->fieldanz - 1)
			 {
					 diff = ubform->mask[pos + 1].pos[1] -
						    ubform->mask[pos].pos[1];
			 }
	         if (pos > -1) DelFormFieldEx (ubform, pos, diff);
	         if (pos > -1) DelFormFieldEx (lineform, pos - 1, diff);
		     pos = GetItemPos (dataform, Item);
	         if (pos > -1) DelFormFieldEx (dataform, pos, diff);
}


void LISTENTER::DelEListField (char *Item)
{
	         int pos, diff;

             if (eListe.GetDataForm () == NULL)
             {
                 return;
             }

		     diff = 0;
		     pos = GetItemPos (eListe.GetUbForm (), Item);
			 if (pos > -1 && pos < eListe.GetUbForm ()->fieldanz - 1)
			 {
					 diff = eListe.GetUbForm ()->mask[pos + 1].pos[1] -
						    eListe.GetUbForm ()->mask[pos].pos[1];
			 }
	         if (pos > -1) DelFormFieldEx (eListe.GetUbForm (), pos, diff);
	         if (pos > -1) DelFormFieldEx (eListe.GetLineForm (), pos - 1, diff);
		     pos = GetItemPos (GetDataForm (), Item);
	         if (pos > -1) DelFormFieldEx (eListe.GetDataForm (), pos, diff);
}


void LISTENTER::SetFieldLen (char *Item, int length)
{
	         int pos, diff;

		     diff = 0;
		     pos = GetItemPos (ubform, Item);
			 if (pos > -1 && pos < ubform->fieldanz - 1)
			 {
					 diff = ubform->mask[pos].length  - length;
			 }

             SetItemLen (ubform, Item, length);
             if (pos < 0) return;

             ChangeItemPos (lineform, pos, diff); 
		     pos = GetItemPos (dataform, Item);
			 if (pos > -1 && pos < dataform->fieldanz - 1)
			 {
                 length = dataform->mask[pos].length - diff;
                 SetItemLen (dataform, Item, length);
			 }
}


void LISTENTER::SetUbHeight (void)
/**
Hoehe der Listueberschrift setzen.
**/
{
	int i;

    if (ubform == NULL) return;

	for (i = 0; i < ubform->fieldanz; i ++)
	{
		ubform->mask[i].rows = UbHeight;
	}
}


void LISTENTER::SethMainWindow (HWND hMainWindow)
{
    this->hMainWindow = hMainWindow;
	hMainWin = hMainWindow;
    hMainInst = (HINSTANCE) GetWindowLong (hMainWindow, GWL_HINSTANCE);
}

LISTENTER:: LISTENTER ()
{

    ProgCfg = new PROG_CFG ("stdlst");
    cfgOK = FALSE;
    ListFocus = 4;
    FormOK = FALSE;

    RowHeight = 1.5;
    UbHeight = 0;

    mamain1 = NULL;
    mamainmax = 0;
    mamainmin = 0; 
    eingabesatz = NULL;
    ausgabesatz = ausgabebuffer;
    zlen = 0;
    feld_anz = 0;
    banz = 0;
    PageView = 0;
    LstZiel = NULL;
    LstSatz   = NULL;
    Lstbanz = 0;
    SwRecs = 0;
    AktRow = 0;
    AktColumn = 0;
    scrollpos = 0;
    InfoCaption[0] = (char) 0; 
    WithMenue   = 1;
    WithToolBar = 1;
	liney = 0;
    ListAktiv = 0;
    this->hMainWindow = NULL;
	hMainWin = NULL;
    ubform   = NULL;
    dataform = NULL;
    lineform = NULL;
    WinStyle = WS_THICKFRAME | WS_POPUP;
    DockWin  = TRUE;
    DockY    = 0;
    DockDiff = -1;
    winX     = -1;
    winY     = -1;
    winCX    = 600;;
    winCY    = 400;
//    GetCfgValues ();
//    NoRecNrSet = FALSE; 
//    if (NoRecNr)
//    {
//	    SetNoRecNr ();
//    }
    Modal = FALSE;
    ITEM::SetPrintMess (DLG::PrintMess);
    initlist = 0;
    DockButtonSpace  = 0;
    DockButtonSpaceP = 0;
}

LISTENTER::~LISTENTER ()
{
    if (ProgCfg != NULL)
    {
        ProgCfg->CloseCfg ();
        delete ProgCfg;
        ProgCfg = NULL;
    }
}


void LISTENTER::SetChAttr (int ca)
{
}

void LISTENTER::SetFieldAttr (char *fname, int attr)
{
         int i;
         char *feldname;

         for (i = 0; i < dataform->fieldanz; i ++)
         {
             feldname = dataform->mask[i].item->GetItemName ();
             if (feldname && strcmp (feldname, fname) == 0)
             {
                   break;
             }
         }
         if (i == dataform->fieldanz) return;

         dataform->mask[i].attribut = attr;
}

void LISTENTER::OrFieldAttr (char *fname, int attr)
{
         int i;
         char *feldname;

         for (i = 0; i < dataform->fieldanz; i ++)
         {
             feldname = dataform->mask[i].item->GetItemName ();
             if (feldname && strcmp (feldname, fname) == 0)
             {
                   break;
             }
         }
         if (i == dataform->fieldanz) return;

         dataform->mask[i].attribut |= attr;
}

int LISTENTER::GetFieldAttr (char *fname)
{
         int i;
         char *feldname;

         for (i = 0; i < dataform->fieldanz; i ++)
         {
             feldname = dataform->mask[i].item->GetItemName ();
             if (feldname && strcmp (feldname, fname) == 0)
             {
                   break;
             }
         }
         if (i == dataform->fieldanz) return -1;

         return (dataform->mask[i].attribut);
}



void LISTENTER::TestMessage (void)
{
	MSG msg;

    if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
    {
          TranslateMessage(&msg);
          DispatchMessage(&msg);
    }
}


void LISTENTER::InitSwSaetze (void)
{
       int i;

       for (i = 0; i < MAXPOS; i ++)
       {
               SwSaetze [i] = (char *) GetListTab (i);
       }
}

void LISTENTER::SetRecHeight (void)
/**
Anzahl Zeilen pro Listzeile ermitteln.
**/
{
       int height;
       int i;

       height = 0;
       for (i = 0; i < dataform->fieldanz; i ++)
       {
           if (dataform->mask[i].attribut & REMOVED) continue;
           if (dataform->mask[i].pos[0] > height)
           {
               height = dataform->mask[i].pos[0];
           }
       }
       height ++; 
       eListe.SetRecHeight (height);
}


int LISTENTER::FromMemory (int pos)
/**
SW-Satz in Ascii-Format Speichern.
**/
{

       memcpy (ausgabesatz, SwSaetze[pos], zlen);
//       memcpy (ausgabesatz, eListe.GetSwSatz(pos), zlen);
       return 0;
}


int LISTENTER::ToMemory (int pos)
/**
SW-Satz in Ascii-Format Speichern.
**/
{

       memcpy (SwSaetze[pos], ausgabesatz, zlen);
       eListe.SetRecanz (pos + 1);
       return 0;
}


void LISTENTER::SetStringEnd (char *feldname, int len)
/**
Stringende Setzen.
**/
{

       return;
}


void LISTENTER::uebertragen (void)
/**
Eingabesatz in ASCII-Struktur uebertragen.
**/
{
        
}

void LISTENTER::InitRow (void)
/**
Eingabesatz in ASCII-Struktur uebertragen.
**/
{
        int i;
//        ListButton **lb;
        ListButton *lb;

        if (dataform == NULL) return;
        
        for (i = 0; i < dataform->fieldanz; i ++)
        {
            if ((dataform->mask[i].attribut & BUTTON) == BUTTON)
            {
                    if (dataform->mask[i].item->GetFeldPtr () != NULL)
                    {
//                        lb = (ListButton **) dataform->mask[i].item->GetFeldPtr ();
//                        lb[0] = new ListButton ();
//                        lb[0]->SetFeld ("..");
                        lb = (ListButton *) dataform->mask[i].item->GetFeldPtr ();
                        lb->SetFeld ("..");
                    }
            }
            else if (eListe.IsInsButton(dataform->mask[i].item->GetItemName ()))
            {
                    if (dataform->mask[i].item->GetFeldPtr () != NULL)
                    {

/*
                        lb = (ListButton **) dataform->mask[i].item->GetFeldPtr ();
                        lb[0] = new ListButton ();
                        lb[0]->SetFeld ("..");
*/
                        lb = (ListButton *) dataform->mask[i].item->GetFeldPtr ();
                        lb->SetFeld ("..");
                    }
            }
            else if ((dataform->mask[i].attribut & CHECKBUTTON) == CHECKBUTTON)
            {
                    if (dataform->mask[i].item->GetFeldPtr () != NULL)
                    {
                        HANDLE hInstance = (HANDLE) GetWindowLong (mamain1, GWL_HINSTANCE); 
/*
                        lb = (ListButton **) dataform->mask[i].item->GetFeldPtr ();
                        lb[0] = new ListButton ();
*/
/*
                        lb[0]->SetBitMap (BMAP::LoadBitmap (hInstance, "SEL", "MSK", WHITECOL));
                        lb[0]->SetFeld (" ");
                        lb = (ListButton *) dataform->mask[i].item->GetFeldPtr ();
                        lb->SetFeld ("..");
*/
                        lb->SetBitMap (BMAP::LoadBitmap (hInstance, "SEL", "MSK", WHITECOL));
                        lb->SetFeld (" ");
                    }
            }
            else
            {
                     strcpy (dataform->mask[i].item->GetFeldPtr (), "");
            }

        }
}

void LISTENTER::CalcSum (void)
{
}

void LISTENTER::FillDB (void)
/**
Auftragspositionen lesen und anzeigen.
**/
{
        int i;
        int dsqlstatus;


        ausgabesatz = (unsigned char *) GetDataRec ();
        zlen = GetDataRecLen ();

        PageView = 0;
        eListe.SetPageView (0);
        feld_anz = dataform->fieldanz;
        Lstzlen = zlen;
        LstSatz = ausgabesatz;

        Lstbanz = feld_anz;

        eListe.SetPos (AktRow, AktColumn);
        eListe.Setbanz (feld_anz);
        eListe.Setzlen (zlen);
        eListe.Initscrollpos ();
        eListe.SetAusgabeSatz (ausgabesatz);

        InitSwSaetze ();
        eListe.SetRecanz (0);
        i = eListe.GetRecanz ();
		Prepare ();
        dsqlstatus = Fetch ();
        while (dsqlstatus == 0)
        {
                     uebertragen ();
                     if (ToMemory (i) != 0) break;
                     i = eListe.GetRecanz ();
                     if (i >= MAXPOS) break; 
                     dsqlstatus = Fetch ();
        }

        Close ();
        i = eListe.GetRecanz ();

        eListe.SetRecanz (i);
        SwRecs = eListe.GetRecanz ();
        CalcSum ();
}

void LISTENTER::StartList (void)
{
}

void LISTENTER::ShowDB (void)
/**
Auftragspositionen lesen und anzeigen.
**/
{
        int i;
        HCURSOR oldcursor;
        extern short do_exit;
        int dsqlstatus;

        InitSwSaetze ();
        eListe.SetRecanz (0);
        i = eListe.GetRecanz ();
//        eListe.SetUbForm (ubform);
        oldcursor = SetCursor (LoadCursor (NULL, IDC_WAIT));
		Prepare ();
        dsqlstatus = Fetch ();
        while (dsqlstatus == 0)
        {
                     uebertragen ();
                     if (ToMemory (i) != 0) break;
                     i = eListe.GetRecanz ();
                     if (i >= MAXPOS) break; 
                     dsqlstatus = Fetch ();
        }

        Close ();
        SetRecHeight ();
        SetCursor (oldcursor);
        i = eListe.GetRecanz ();

        eListe.SetRecanz (i);
        SwRecs = eListe.GetRecanz ();

        eListe.SetDataForm0 (dataform, lineform);
        eListe.SetSaetze (SwSaetze);
//        eListe.SetChAttr (ChAttr); 
//        eListe.SetUbRows (ubrows); 
        StartList ();
        if (i == 0)
        {
			     Posanz = 0;
                 eListe.AppendLine ();
                 InitRow ();
                 ToMemory (0);
				 AktRow = AktColumn = 0;
                 i = eListe.GetRecanz ();
        }
		else
		{
			     Posanz = i;
		}
        SendMessage (eListe.Getmamain2 (), WM_SIZE, NULL, NULL);
        InvalidateRect (eListe.Getmamain3 (), 0, FALSE);
        UpdateWindow (eListe.Getmamain3 ());
		FromTab (0);
}


void LISTENTER::ReadDB (void)
/**
Auftragspositionen lesen.
**/
{
        ausgabesatz = (unsigned char *) GetDataRec ();
        zlen = GetDataRecLen ();

        PageView = 0;
        eListe.SetPageView (0);
        feld_anz = dataform->fieldanz;
        Lstzlen = zlen;
        LstSatz = ausgabesatz;

        Lstbanz = feld_anz;

        eListe.SetPos (AktRow, AktColumn);
        eListe.Setbanz (feld_anz);
        eListe.Setzlen (zlen);
        eListe.Initscrollpos ();
        eListe.SetAusgabeSatz (ausgabesatz);
}

void LISTENTER::DestroyWindows (void)
{
       eListe.DestroyListWindow ();
       SetAktivWindow (NULL);
       DestroyMainWindow ();
       InitMax ();
       InitMin ();
       MoveMamain1 ();
       ListAktiv = 0;
/*
       SetFkt (11, leer, NULL);
       set_fkt (NULL, 11);
*/
       initlist = 0;
}

void LISTENTER::SetSchirm (void)
{
}


void LISTENTER::SetNoRecNr (void)
{
	   int i;

	   eListe.SetNoRecNr (TRUE);
	   if (NoRecNrSet) return;
       if (dataform == NULL) return;

	   for (i = 0; i < dataform->fieldanz; i ++)
	   {
		   dataform->mask[i].pos[1] -= 6;
	   }
	   for (i = 0; i < ubform->fieldanz; i ++)
	   {
		   ubform->mask[i].pos[1] -= 6;
	   }
	   for (i = 0; i < lineform->fieldanz; i ++)
	   {
		   lineform->mask[i].pos[1] -= 6;
	   }
       NoRecNrSet = TRUE;
}


void LISTENTER::ShowList (void)
/**
Auftragsliste bearbeiten.
**/

{
//	   int pos;

       if (ListAktiv) return; 

       LockWindowUpdate (hMainWindow);
  	   SetUbHeight ();
       SetSchirm ();
//       eListe.SetInfoProc (InfoProc);
       mamain1 = CreateMainWindow ();
       eListe.InitListWindow (mamain1);
       ReadDB ();

       eListe.SetListFocus (0);
       eListe.Initscrollpos ();
       AktRow = 0;
       AktColumn = 0;
       scrollpos = 0;
       SetAktivWindow (eListe.Getmamain2 ());
       eListe.SetUbForm (ubform);
       ShowDB ();

       ListAktiv = 1;
       LockWindowUpdate (NULL);
       initlist = 1; 
       return;
}


void LISTENTER::SetWorkMode (BOOL work)
{
    if (work)
    {
       eListe.FirstPos ();
       eListe.SetListFocus (ListFocus);
       SetAktFocus ();
    }
    else
    {
       eListe.SetListFocus (0);
    }
}


int LISTENTER::GetListLine (char *cfg_v)
{
       static char *lineflag [] = {"WT" ,
                                   "HT",
                                   "ST",
                                   "GT",
                                   "VW",
                                   "VH",
                                   "VS", 
                                   "VG",
                                   "HW",
                                   "HH",
                                   "HS", 
                                   "HG",
                                   "NO",
                                   NULL};
       int i;

       for (i = 0; lineflag[i] != NULL; i ++)
       {
           if (strcmp (lineflag[i], cfg_v) == 0) return i;
       }
       return 2;
}


int LISTENTER::GetListColor (COLORREF *color, char *cfg_v)
/**
Farbunterlegung fuer Listen uebertragen.
**/
{
	static char *ColTxt[] = {"BLACKCOL",
		                     "WHITECOL",
							 "BLUECOL",
							 "REDCOL",
							 "GRAYCOL",
							 "LTGRAYCOL",
							 "GREENCOL",
							 "YELLOWCOL",
							 "DKYELLOWCOL",
							 NULL};
	
	static COLORREF ColVal[] = {BLACKCOL,
		                        WHITECOL,
					  		    BLUECOL,
							    REDCOL,
							    GRAYCOL,
							    LTGRAYCOL,
							    GREENCOL,
							    YELLOWCOL,
								DKYELLOWCOL};
	int i;
	int red, blue, green;
	char *ColR;
	int Cols = 0;

	for (i = 0; ColTxt[i]; i ++)
	{
		if (strcmp (ColTxt[i], cfg_v) == 0)
		{
			*color = ColVal[i];
			return i;
		}
	}


	ColR = strstr (cfg_v, "red");
	if (ColR)
	{
		red = atoi (&ColR[3]);
		Cols ++;
	}
	ColR = strstr (cfg_v, "blue");
	if (ColR)
	{
		blue = atoi (&ColR[4]);
		Cols ++;
	}
	ColR = strstr (cfg_v, "green");
	if (ColR)
	{
		green = atoi (&ColR[5]);
		Cols ++;
	}
	if (Cols == 3)
	{
		*color = RGB (red, green, blue);
	}
    return 0;
}


void LISTENTER::GetCfgValues (void)
{
       char cfg_v [512];
       COLORREF Color;

//	   if (cfgOK) return;

       if (ProgCfg == NULL) return;

       cfgOK = TRUE;
       if (ProgCfg->GetCfgValue ("3DList", cfg_v) == TRUE)
       {
                    eListe.Set3D (atoi (cfg_v));
	   }
       if (ProgCfg->GetCfgValue ("3DSize", cfg_v) == TRUE)
       {
                    eListe.SetPlus3D (atoi (cfg_v));
	   }
       if (ProgCfg->GetCfgValue ("ListFocus", cfg_v) == TRUE)
       {
                    ListFocus = atoi (cfg_v);
	   }
        if (ProgCfg->GetCfgValue ("rowheight", cfg_v) == TRUE)
		{
			        RowHeight = ratod (cfg_v);
					if (RowHeight < (double) 1.0)
					{
						RowHeight = (double) 1.0;
					}
		}
        if (ProgCfg->GetCfgValue ("ubheight", cfg_v) == TRUE)
		{
			        UbHeight = atoi (cfg_v);
					SetUbHeight ();
		}
		eListe.SetRowHeight (RowHeight);


        if (ProgCfg->GetCfgValue ("NoRecNr", cfg_v) == TRUE)
        {
			        NoRecNr = atoi (cfg_v);
		            if (atoi (cfg_v))
					{
	                        SetNoRecNr ();
					}
		}


        if (ProgCfg->GetCfgValue ("ListColor", cfg_v) == TRUE)
        {
                     GetListColor (&Color, cfg_v);
		             eListe.SetColor (Color);
        }
        if (ProgCfg->GetCfgValue ("ListBkColor", cfg_v) == TRUE)
        {
                     GetListColor (&Color, cfg_v);
		             eListe.SetBkColor (Color);
        }
        if (ProgCfg->GetCfgValue ("ListLine", cfg_v) == TRUE)
        {
		             eListe.SetLines (GetListLine (cfg_v));
        }
}


void LISTENTER::SetNewRow (char *item, int row)
{
        int lenu;
        int len;
        int diff;
        int i;
		int datapos;
		int ipos;


        ipos = GetItemPos (ubform, item);
		if (ipos <= 0) return;

		for (i = ipos; i < dataform->fieldanz; i ++)
		{
		    datapos = dataform->mask[i].pos[1];
			if (datapos >= ubform->mask[ipos].pos[1]) break;
		}

        diff = row - ubform->mask[ipos].pos[1]; 

		if (i == dataform->fieldanz)
		{
	        datapos = dataform->mask[i].pos[1];
            len  = dataform->mask[i].length + diff;
		}
		else
		{
	        datapos = dataform->mask[i - 1].pos[1];
            len  = dataform->mask[i- 1].length + diff;
		}

        lenu = ubform->mask[ipos - 1].length + diff;

        if (ipos < ubform->fieldanz - 1)
        {
             ubform->mask[ipos].pos[1] = row;
             lineform->mask[ipos - 1].pos[1] = row;

             for ( i = ipos + 1; i < ubform->fieldanz; i ++)
             {
                  ubform->mask[i].pos[1]   += diff;
                  lineform->mask[i - 1].pos[1] += diff;
             }
        }

		for (i = 0; i < dataform->fieldanz; i ++)
		{

			   if (dataform->mask[i].pos[1] == datapos)
			   {
                         dataform->mask[i].length = len;
			   }

			   if (dataform->mask[i].pos[1] > datapos)
			   {
                  dataform->mask[i].pos[1] += diff;
			   }
		}
        ubform->mask[ipos - 1].length = lenu;
}

void LISTENTER::SetNewPicture (char *item, char *picture)
{
		int ipos;
		char *pic;
        ipos = GetItemPos (dataform, item);
        if (ipos == -1) return;

		pic = new char (strlen (picture + 2));
		if (pic == NULL) return;

		dataform->mask[ipos].picture = pic;
}


void LISTENTER::SetNewLen (char *item, int len)
{
        int lenu;
        int diff;
        int i;
		int datapos;
		int ipos;


		if (len == 0) return;
        ipos = GetItemPos (ubform, item);
        if (ipos == -1) return;

        diff = len - ubform->mask[ipos].length + 2; 
		for (i = ipos; i < dataform->fieldanz; i ++)
		{
		    datapos = dataform->mask[i].pos[1];
			if (datapos >= ubform->mask[ipos].pos[1]) break;
		}

		if (i < dataform->fieldanz)
		{
                 datapos = dataform->mask[i].pos[1];
		}

        lenu = ubform->mask[ipos].length + diff;

        if (ipos < ubform->fieldanz - 1)
        {
             for ( i = ipos + 1; i < ubform->fieldanz; i ++)
             {
                  ubform->mask[i].pos[1]   += diff;
                  lineform->mask[i - 1].pos[1] += diff;
             }
        }

		for (i = 0; i < dataform->fieldanz; i ++)
		{

			   if (dataform->mask[i].pos[1] == datapos)
			   {
                         dataform->mask[i].length = len;
			   }

			   if (dataform->mask[i].pos[1] > datapos)
			   {
                  dataform->mask[i].pos[1] += diff;
			   }
		}
        ubform->mask[ipos].length = lenu;
}


void LISTENTER::EnterList ()
/**
Liste bearbeiten.
**/
{
//       static int initlist = 0;

       LockWindowUpdate (hMainWindow);
  	   SetUbHeight ();
       AktRow = 0;
       AktColumn = 0;
       if (initlist == 0)
       {
//                 eListe.SetInfoProc (InfoProc);
                 mamain1 = CreateMainWindow ();
                 eListe.InitListWindow (mamain1);
                 ReadDB ();
       }
       else
       {
                 eListe.GetTextMetric (&tm);
       }

       eListe.SetListFocus (ListFocus);
       eListe.Initscrollpos ();
       scrollpos = 0;
       SetAktivWindow (eListe.Getmamain2 ());
       if (initlist == 0)
       {
                  eListe.SetUbForm (ubform);
       }
       initlist = 1;
       ShowDB ();
       LockWindowUpdate (NULL);

       ListAktiv = 1;
       eListe.TestBeforeRow ();
       eListe.ProcessMessages ();

       InitMax ();
       InitMin ();
       MoveMamain1 ();
       ListAktiv = 0;
       eListe.DestroyListWindow ();
       DestroyMainWindow ();
       initlist = 0;
       return;
}

void LISTENTER::SetAktFocus (void)
{
       eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                             eListe.FirstColumn ());
}

void LISTENTER::WorkList ()
/**
Auftragsliste bearbeiten.
**/

{

       eListe.SetDataForm0 (dataform, lineform);
//       eListe.SetChAttr (ChAttr); 
//       eListe.SetUbRows (ubrows); 
       eListe.SetListFocus (ListFocus);
       eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                             eListe.GetAktColumn ());
       AktRow = 0;
       AktColumn = 0;
       scrollpos = 0;

       SetAktivWindow (eListe.Getmamain2 ());

       ShowDB ();
       ListAktiv = 1;
       eListe.TestBeforeRow ();
       eListe.ProcessMessages ();
       eListe.DestroyFocusWindow ();
       eListe.SetListFocus (0);

       if (syskey == KEY5)
       {
                  eListe.Initscrollpos ();
                  ShowDB ();
       }
       ListAktiv = 0;
/*
       SetFkt (6, leer, NULL);
       SetFkt (7, leer, NULL);
       SetFkt (8, leer, NULL);
       SetFkt (11, leer, NULL);
       set_fkt (NULL, 11);
*/
       return;
}


void LISTENTER::DestroyMainWindow (void)
/**
Hauptfenster fuer Liste loeschen.
**/
{
        if (mamain1 == NULL) return;
        DestroyWindow (mamain1);
        mamain1 = NULL;
}


HWND LISTENTER::CreateMainWindow (void)
/**
Hauptfenster fuer Liste erzeugen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;
        int xfull, yfull;

        if (hMainWindow == NULL) return NULL;    
           
        if (mamain1) return mamain1;

        eListe.Register (hMainInst);
        eListe.GetTextMetric (&tm);

        GetClientRect (hMainWindow, &rect);
        GetWindowRect (hMainWindow, &wrect);
        xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
        yfull = GetSystemMetrics (SM_CYFULLSCREEN);        


        if (DockWin)
        {
		        if (DockY > 0)
                {
			            y = DockY;
                        DockDiff = y - wrect.top;
                }
		        else
                {
                        y = (wrect.bottom - 12 * tm.tmHeight);
                }
                x = wrect.left;
                cy = wrect.bottom - y;
                if (DockButtonSpace > 0)
                {
                        cy -= DockButtonSpace * tm.tmHeight;
                }
                if (DockButtonSpaceP > 0)
                {
                        cy -= DockButtonSpaceP;
                }
                cx = rect.right;
                if (WinStyle & WS_CHILD)
                {
                        y -= wrect.top;
                        x -= wrect.left;
                }
        }
        else
        {
                x  = this->winX;
                y  = this->winY;
                cx = this->winCX;
                cy = this->winCY;
                if (this->winX == -1)
                {
                    x = max (0, (xfull - this->winCX) / 2);
                }
                if (this->winY == -1)
                {
                    y = max (0, (yfull - this->winCY) / 2);
                }
                eListe.SetCYDiff (tm.tmHeight * 3);
        }
        

        mamain1 = CreateWindow ("ListMain",
                                    InfoCaption,
                                    WinStyle,
                                    x, y,
                                    cx, cy,
                                    hMainWindow,
                                    NULL,
                                    hMainInst,
                                    NULL);
        ShowWindow (mamain1, SW_SHOWNORMAL);
        UpdateWindow (mamain1);
        return mamain1;
}

static int IsMax = 0;
static int IsMin = 0;

void LISTENTER::SetMax0 (int val)
{
    IsMax = val;
    if (IsMax)
    {
//             SetFkt (11, fenster, KEY11);
    }
    else
    {
//             SetFkt (11, vollbild, KEY11);
    }
}

void LISTENTER::SetMin0 (int val)
{
    IsMin = val;
}


int LISTENTER::Schirm (void)
{
         if (IsMin) return 0;
         
         if (IsMax)
         {
             SendMessage (mamain1, WM_SYSCOMMAND, SC_RESTORE, 0l);
             IsMax = 0;
         }
         else
         {
             SendMessage (mamain1, WM_SYSCOMMAND, SC_MAXIMIZE, 0l);
             IsMax = 1;
         }
         return 0;
}


void LISTENTER::MoveMamain1 ()
{
        RECT rect;
        RECT wrect;
        int x, y, cx, cy;
        TEXTMETRIC tm;

        if (mamainmin) return;
        eListe.GetTextMetric (&tm);
        GetClientRect (hMainWindow, &rect);
        GetWindowRect (hMainWindow, &wrect);
        if (mamainmax)
        {
                 x = wrect.left + 2;
                 y = wrect.top + 2;
                 cy = wrect.bottom - y - 2;
                 cx = rect.right;
        }
        else
        {
                if (DockWin)
                {
                      if (DockDiff > -1)
                      {
                         y = wrect.top + DockDiff;
                      }
		              else if (DockY > 0)
                      {
			             y = DockY;
                      }
		              else
                      {
                        y = (wrect.bottom - 12 * tm.tmHeight);
                      }
                      x = wrect.left;
                      cy = wrect.bottom - y;
                      cx = rect.right;
                      if (WinStyle & WS_CHILD)
                      {
                                 y -= wrect.top;
                                 x -= wrect.left;
                      }
                      if (DockButtonSpace > 0)
                      {
                                 cy -= DockButtonSpace * tm.tmHeight;
                      }
                      if (DockButtonSpaceP > 0)
                      {
                                 cy -= DockButtonSpaceP;
                      }
                }
        }
        MoveWindow (mamain1, x,y, cx, cy, TRUE);
}


void LISTENTER::MaximizeMamain1 ()
{
        RECT rect;
        RECT wrect;
        int x, y, cx, cy;

        GetClientRect (hMainWindow, &rect);
        GetWindowRect (hMainWindow, &wrect);
        x = wrect.left + 2;
        y = wrect.top + 2;
        cy = wrect.bottom - y - 2;
        cx = rect.right;
        MoveWindow (mamain1, x,y, cx, cy, TRUE);
}


HWND LISTENTER::GetMamain1 (void)
{
       return (mamain1);
}

void LISTENTER::SethwndTB (HWND hwndTB)
{
         this->hwndTB = hwndTB; 
         eListe.SethwndTB (hwndTB);
}

void LISTENTER::SethMenu(HMENU hMenue)
{
         eListe.SethMenu (hMenue);
}

void LISTENTER::SetTextMetric (TEXTMETRIC *tm)
{
         memcpy (&textm, tm, sizeof (TEXTMETRIC));
         eListe.SetTextMetric (tm);
}


void LISTENTER::SetLineRow (int LineRow)
{
         eListe.SetLineRow (0);
}

void LISTENTER::SetListLines (int i)
{ 
         eListe.SetListLines (i);
}

void LISTENTER::OnPaint (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	    HDC hdc;

        eListe.OnPaint (hWnd, msg, wParam, lParam);
        if (hWnd == eWindow)  
        {
                    hdc = BeginPaint (eWindow, &aktpaint);
                    EndPaint (eWindow, &aktpaint);
        }
}


void LISTENTER::MoveListWindow (void)
{
        eListe.MoveListWindow ();
}


void LISTENTER::BreakList (void)
{
        eListe.BreakList ();
}


void LISTENTER::OnHScroll (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
        eListe.OnHScroll (hWnd, msg,wParam, lParam);
}

void LISTENTER::OnVScroll (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
        eListe.OnVScroll (hWnd, msg,wParam, lParam);
}

void LISTENTER::OnSize (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
        if (hWnd == GetMamain1 ())
        {
                    MoveListWindow ();
        }
}


void LISTENTER::FunkKeys (WPARAM wParam, LPARAM lParam)
{
        eListe.FunkKeys (wParam, lParam);
}


int LISTENTER::GetRecanz (void)
{
          return eListe.GetRecanz ();
}


void LISTENTER::SwitchPage0 (int rows)
{
           eListe.SwitchPage0 (rows);
}


HWND LISTENTER::Getmamain2 (void)
{
           return eListe.Getmamain2 ();
}


HWND LISTENTER::Getmamain3 (void)
{
           return eListe.Getmamain3 ();
}


void LISTENTER::ChoiseFont (mfont *lfont)
{
           eListe.ChoiseFont (lfont);
}

void LISTENTER::SetFont (mfont *lfont)
{
           eListe.SetFont (lfont);
}

void LISTENTER::SetListFont (mfont *lfont)
{
           eListe.SetListFont (lfont);
}

void LISTENTER::FindString (void)
{
           eListe.FindString ();
}


void LISTENTER::SetLines (int Lines)
{
           eListe.SetLines (Lines);
}


int LISTENTER::GetAktRow (void)
{
                 return eListe.GetAktRow ();
}

int LISTENTER::GetAktRowS (void)
{
                 return eListe.GetAktRowS ();
}

void LISTENTER::SetColors (COLORREF Color, COLORREF BkColor)
{
                 eListe.SetColors (Color, BkColor); 
}

void LISTENTER::SetListFocus (void)
{
                 eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                                       eListe.GetAktColumn ()); 
}

void LISTENTER::PaintUb (void)
{
                 eListe.PaintUb (); 
}


int LISTENTER::DeleteLine (void)
/**
Zeile aus Liste loeschen.
**/
{
        return 0;
}

int LISTENTER::InsertLine (void)
/**
Zeile aus Liste loeschen.
**/
{
        eListe.SetPos (eListe.GetAktRow (), eListe.FirstColumn ());
        eListe.InsertLine ();
        return 0;
}


int LISTENTER::AppendLine (void)
/**
Zeile aus Liste loeschen.
**/
{
        eListe.AppendLine ();
        return 0;
}

void LISTENTER::Prepare (void)
{
}

int LISTENTER::Fetch (void)
{
	    return 100;
}

void LISTENTER::Close (void)
{
}

char * LISTENTER::GetListTab (int i)
{
	    return (char *) " ";
}

void LISTENTER::FromTab (int i)
{
}

void LISTENTER::ToTab (int i)
{
}

unsigned char *LISTENTER::GetDataRec (void)
{
	    return (unsigned char *) " ";
}

int LISTENTER::GetDataRecLen (void)
{
	    return 1;
}

int LISTENTER::SetRowItem (void)
{
	    return 0;
}

void LISTENTER::DoBreak (void)
{
}

void LISTENTER::PrintMess (char *Text)
{
        if (MessWindow == NULL) return; 
        if (Text[0] == 0) return;
        strcpy (MessText, Text);
        SetWindowText (MessWindow, MessText);
}

int LISTENTER::GetUblen (char *Item)
{
        int fpos;
        fpos = GetItemPos (ubform, Item);
        if (fpos == -1) return - 1;
        return ubform->mask[fpos].length;
}

void LISTENTER::DestroyFormField (form *frm, char *Item, int flen)
{
        int i;

        int xpos;
        int fpos;

        fpos = GetItemPos (frm, Item);
        if (fpos == -1) return;

        xpos = frm->mask[fpos].pos[1];

        frm->fieldanz --;
        for (i = fpos; i < frm->fieldanz; i ++)
        {
            memcpy (&frm->mask[i], &frm->mask[i + 1], sizeof (field));
        }

        for (i = 0; i < frm->fieldanz; i ++)
        {
            if (frm->mask[i].pos[1] > xpos)
            {
                frm->mask[i].pos[1] -= flen;
            }
        }
}

void LISTENTER::DestroyLine (char *Item, int flen)
{
        int i;
        int xpos;
        int fpos;

        fpos = GetItemPos (ubform, Item);
        if (fpos == -1) return;

        xpos = ubform->mask[fpos].pos[1];

        lineform->fieldanz --;
        for (i = fpos; i < lineform->fieldanz; i ++)
        {
            memcpy (&lineform->mask[i], &lineform->mask[i + 1], sizeof (field));
        }
        for (i = 0; i < lineform->fieldanz; i ++)
        {
            if (lineform->mask[i].pos[1] > xpos)
            {
                lineform->mask[i].pos[1] -= flen;
            }
        }
}

void LISTENTER::ScrollUbRows (char *Item)
{
        int *UbRows;
        int fpos;
        int i;

        fpos = GetItemPos (ubform, Item);
        if (fpos == -1) return;

        UbRows = eListe.GetUbRows ();
        if (UbRows == NULL) return;

        for (i = 0; i < dataform->fieldanz; i ++)
        {
            if (UbRows[i] == -1) break;
            if (UbRows[i] >= fpos)
            {
                UbRows[i] --;
            }
        }
}

void LISTENTER::DestroyListField (char *Item)
{
        int len;

        if (ubform == NULL) return;
        if (dataform == NULL) return;
        if (lineform == NULL) return;
        if ((len = GetUblen (Item)) == -1) return;

        DestroyLine (Item, len);
        DestroyFormField (ubform, Item, len); 
        DestroyFormField (dataform, Item, len); 
}







