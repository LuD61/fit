#ifndef _PRDK_ZUT_DEF
#define _PRDK_ZUT_DEF

#include "windows.h"
#include "mo_draw.h"
#include "wmask.h"
#include "dbclass.h"

struct PRDK_ZUT {
   short     mdn;
   char      rez[9];
   long      mat;
   short     kutf;
   char      bearb_info[81];
   char      zug_bas[3];
   char      zug_stnd[2];
   double    zug_me;
   double    zut_me;
   short     me_einh;
   double    gew;
   short     variante;
   short     typ;
};
extern struct PRDK_ZUT prdk_zut, prdk_zut_null;

#line 10 "prdk_zut.rh"

class PRDK_ZUT_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               PRDK_ZUT_CLASS () : DB_CLASS ()
               {
               }
};
#endif
