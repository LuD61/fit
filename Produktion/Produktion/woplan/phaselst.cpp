#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include <string.h>
#include "comcthlp.h"
#include "itemc.h"
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_menu.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_intp.h"
#include "inflib.h"
#include "spezdlg.h"
#include "enterfunc.h"
#include "phaselst.h"
#include "spezdlg.h"
#include "searchmasch.h"
#include "Text.h"
// #include "searchvarb.h"
// #include "dlg.h"

#define IDM_CANCEL 3001
#ifndef IDM_WRITE
#define IDM_WRITE  3002
#endif

static int StdSize = STDSIZE;

static int dlgsize    = STDSIZE;
static int ltgraysize = STDSIZE;
static int dlgpossize = STDSIZE;
static int cpsize     = 200;


static mfont dlgfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         GRAYCOL,
                         0,
                         NULL};


static ColButton Ctxt    = { "Text",  10, -1, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, -1, -1,
                             NULL,  0, 0,
                             BLACKCOL,
                             LTGRAYCOL,
                             0, 
};


struct PHASELSTS  PHASELST::Lsts;
struct PHASELSTS *PHASELST::LstTab;
char *PHASELST::InfoItem = "a";
Work *PHASELST::work = NULL;
short PHASELST::kw;
short PHASELST::jr;


CFIELD *PHASELST::_fChoise0 [] = {
    new CFIELD ("cancel", " abbrechen ", 10, 0, 0, 0, NULL, "",
                CBUTTON, 
                IDM_CANCEL, &dlgfont, 0, 0),
    new CFIELD ("write", " speichern ", 10, 0,12, 0, NULL, "",
                CBUTTON, 
                IDM_WRITE, &dlgfont, 0, 0),
    new CFIELD ("show",  "  ansehen  ", 10, 0,24, 0, NULL, "",
                CBUTTON, 
                IDM_CHOISE, &dlgfont, 0, 0),
    new CFIELD ("delete","  l�schen  ", 10, 0,36, 0, NULL, "",
                CBUTTON, 
                IDM_DELETE, &dlgfont, 0, 0),
    new CFIELD ("insert"," einf�gen  ", 10, 0,48, 0, NULL, "",
                CBUTTON, 
                IDM_INSERT, &dlgfont, 0, 0),
    NULL,
};

CFORM PHASELST::fChoise0 (5, _fChoise0);

CFIELD *PHASELST::_fChoise [] = {
    new CFIELD ("fChoise0", (char *) &fChoise0, 0, 0, -1 , -2, NULL, "",
                CFFORM, 
                500, &dlgfont, 0, 0),
    NULL,
};

CFORM PHASELST::fChoise (1, _fChoise);


ITEM PHASELST::ua                  ("a",                  "Artikel",          "", 0);
ITEM PHASELST::ua_bz1              ("a_bz1",              "Bezeichnung",      "", 0);  
ITEM PHASELST::uchg                ("chgsize",            "ChgGr��e.",        "", 0);
ITEM PHASELST::ume                 ("me",                 "Menge",            "", 0);
ITEM PHASELST::ufolge              ("folge",              "Reihenfolge",      "", 0);
ITEM PHASELST::ume2                ("me2",                "Menge 2",          "", 0);
ITEM PHASELST::umasch_nr2          ("masch_nr2",          "MaschinenNr",      "", 0);
ITEM PHASELST::ume3                ("me3",                "Menge 3",          "", 0);
ITEM PHASELST::umasch_nr3          ("masch_nr3",          "MaschinenNr",      "", 0);
ITEM PHASELST::ufiller             ("",                   " ",                "", 0);

ITEM PHASELST::iline ("", "1", "", 0);

ITEM PHASELST::ia                  ("a",                  Lsts.a,                   "", 0);
ITEM PHASELST::ia_bz1              ("a_bz1",              Lsts.a_bz1,               "", 0);  
ITEM PHASELST::ichg                ("chgsize",            Lsts.chg,                 "", 0);
ITEM PHASELST::ime                 ("me",                 Lsts.me,                  "", 0);
ITEM PHASELST::ifolge              ("folge",              Lsts.folge,               "", 0);
ITEM PHASELST::ime2                ("me2",                Lsts.me2,                 "", 0);
ITEM PHASELST::imasch_nr2          ("masch_nr2",          Lsts.masch_nr2,           "", 0);
ITEM PHASELST::ichmasch_nr         ("chmasch_nr",         (char *) &Lsts.chmasch_nr, "", 0);
ITEM PHASELST::ime3                ("me3",                Lsts.me3,                 "", 0);
ITEM PHASELST::imasch_nr3          ("masch_nr3",          Lsts.masch_nr3,           "", 0);
ITEM PHASELST::ichmasch_nr3        ("chmasch_nr3",        (char *) &Lsts.chmasch_nr3,"", 0);

field PHASELST::_UbForm [] = {
&ua,         17, 0, 0,  6,  NULL, "", BUTTON, 0, 0, 0,     
&ua_bz1,     21, 0, 0, 23,  NULL, "", BUTTON, 0, 0, 0, 
&uchg,       15, 0, 0, 44,  NULL, "", BUTTON, 0, 0, 0, 
&ume,        15, 0, 0, 59,  NULL, "", BUTTON, 0, 0, 0, 
&ufolge,     15, 0, 0, 74,  NULL, "", BUTTON, 0, 0, 0, 
&ume2,       15, 0, 0, 89,  NULL, "", BUTTON, 0, 0, 0, 
&umasch_nr2, 15, 0, 0,104,  NULL, "", BUTTON, 0, 0, 0, 
&ume3,       15, 0, 0,119,  NULL, "", BUTTON, 0, 0, 0, 
&umasch_nr3, 15, 0, 0,134,  NULL, "", BUTTON, 0, 0, 0, 
&ufiller, 120, 0, 0 , 149,  NULL, "", BUTTON, 0, 0, 0,
};


form PHASELST::UbForm = {10, 0, 0, _UbForm, 0, 0, 0, 0, NULL};

field PHASELST::_LineForm [] = {
&iline,   1, 0, 0, 23,  NULL, "",  DISPLAYONLY, 0, 0, 0, 
&iline,   1, 0, 0, 44,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0, 59,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0, 74,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0, 89,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,104,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,119,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,134,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,149,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
};

form PHASELST::LineForm = {9, 0, 0, _LineForm,0, 0, 0, 0, NULL};

field PHASELST::_DataForm [] = {
&ia,         15, 0, 0,  7,  NULL, "%13.0f", DISPLAYONLY, 0, 0, 0,     
&ia_bz1,     19, 0, 0, 24,  NULL, "",       DISPLAYONLY, 0, 0, 0, 
&ichg,       13, 0, 0, 45,  NULL, "%12.3f", DISPLAYONLY, 0, 0, 0, 
&ime,        14, 0, 0, 60,  NULL, "%13.3f", EDIT,        0, 0, 0, 
&ifolge,      5, 0, 0, 80,  NULL, "%3d",    EDIT,        0, 0, 0, 
&ime2,       14, 0, 0, 90,  NULL, "%13.3fn", DISPLAYONLY, 0, 0, 0, 
&imasch_nr2,  9, 0, 0,105,  NULL, "%8dn",    DISPLAYONLY, 0, 0, 0, 
&ichmasch_nr, 2, 1, 0,115,  NULL, "",       BUTTON,      0, 0, 0,
&ime3,       14, 0, 0,120,  NULL, "%13.3fn", DISPLAYONLY, 0, 0, 0, 
&imasch_nr3,  9, 0, 0,135,  NULL, "%8dn",    DISPLAYONLY, 0, 0, 0, 
&ichmasch_nr3,2, 1, 0,145,  NULL, "",       BUTTON,      0, 0, 0,
};


form PHASELST::DataForm = {11, 0, 0, _DataForm,0, 0, 0, 0, NULL};

int PHASELST::ubrows [] = {0, 
                           1,
                           2,
                           3, 
                           4,
                           5,
                           6,6,
                           7,
                           8,8 
                         -1,
};


struct CHATTR PHASELST::ChAttra [] = {
//                                      "chmasch_nr",  REMOVED,BUTTON,                
//                                      "nr",          DISPLAYONLY,COMBOBOX,                
                                      NULL,  0,           0};

struct CHATTR *PHASELST::ChAttr = ChAttra;
int PHASELST::NoRecNrSet = FALSE;



/*
BOOL PHASELST::CallInfo (HWND hWnd, RECT *rect, char *Item, char *ItemValue, DWORD WinMode)
/?*
Einstieg in Infosystem mit Artikel. Abgeleitet von Klasse Info.
*?/
{
 		 char buffer [1024];
         BOOL ret;

         sprintf (buffer, "where a = 2");
         ret = Info::CallInfo (hWnd, rect, "a", buffer, WinMode); 
         SetListFocus ();
         return ret;
}


int PHASELST::InfoProc (char **Item, char *Value, char *where)
/?**
Spezielle Procedure fuer Info-System. Alter Ablauf, ist nicht mehr Aktiv 
*?/
{

   *Item = "a";


   sprintf (where, "where a = %.0lf", Lsts.a);
   return 1;
}
*/

    
void PHASELST::CallInfoEx (void)
/*
Info wird aus Dialog aufgerufen.
*/

{
   char where [512];
   
   sprintf (where, "where a = %.0lf", Lsts.a);
   _CallInfoEx (hMainWindow, NULL, InfoItem, where, 0l); 
}

void PHASELST::SetButtonSize (int Size)
{
    int fpos;

    fpos = GetItemPos (&DataForm, "chmasch_nr");
    if (fpos < 0) return;

    if (Size == SMALL)
    {
         DataForm.mask [fpos].length = 2;
         DataForm.mask [fpos].rows = 1;
    }
    else 
    {
         DataForm.mask [fpos].length = 3;
         DataForm.mask [fpos].rows = 0;
    }

    fpos = GetItemPos (&DataForm, "chmasch_nr3");
    if (fpos < 0) return;

    if (Size == SMALL)
    {
         DataForm.mask [fpos].length = 2;
         DataForm.mask [fpos].rows = 1;
    }
    else 
    {
         DataForm.mask [fpos].length = 3;
         DataForm.mask [fpos].rows = 0;
    }
}


void PHASELST::GetCfgValues (void)
{
       char cfg_v [512];
       COLORREF Color;

       if (ProgCfg == NULL) return;
	   if (cfgOK) return;

       cfgOK = TRUE;
       if (ProgCfg->GetCfgValue ("3DList", cfg_v) == TRUE)
       {
                    eListe.Set3D (atoi (cfg_v));
	   }
       if (ProgCfg->GetCfgValue ("3DSize", cfg_v) == TRUE)
       {
                    eListe.SetPlus3D (atoi (cfg_v));
	   }
       if (ProgCfg->GetCfgValue ("ListFocus", cfg_v) == TRUE)
       {
                    ListFocus = atoi (cfg_v);
	   }
        if (ProgCfg->GetCfgValue ("rowheight", cfg_v) == TRUE)
		{
			        RowHeight = ratod (cfg_v);
					if (RowHeight < (double) 1.0)
					{
						RowHeight = (double) 1.0;
					}
		}
        if (ProgCfg->GetCfgValue ("ubheight", cfg_v) == TRUE)
		{
			        UbHeight = atoi (cfg_v);
					SetUbHeight ();
		}
		eListe.SetRowHeight (RowHeight);


        if (ProgCfg->GetCfgValue ("NoRecNr", cfg_v) == TRUE)
        {
			        NoRecNr = atoi (cfg_v);
		            if (atoi (cfg_v))
					{
	                        SetNoRecNr ();
					}
		}
        if (ProgCfg->GetCfgValue ("ListColor", cfg_v) == TRUE)
        {
                     GetListColor (&Color, cfg_v);
		             eListe.SetColor (Color);
        }
        if (ProgCfg->GetCfgValue ("ListBkColor", cfg_v) == TRUE)
        {
                     GetListColor (&Color, cfg_v);
		             eListe.SetBkColor (Color);
        }
        if (ProgCfg->GetCfgValue ("ListLine", cfg_v) == TRUE)
        {
		             eListe.SetLines (GetListLine (cfg_v));
        }
        if (ProgCfg->GetCfgValue ("VarbInfo", cfg_v) == TRUE)
        {
                     InfoItem = new char [80];
                     strcpy (InfoItem, cfg_v);
        }
}


void PHASELST::TestRemoves (void)
{
/*
        int fpos;

        fpos = GetItemPos (&DataForm, "feld3");
        if (fpos >= 0)
        {
            if (DataForm.mask[fpos].attribut & REMOVED)
            {
                DestroyListField ("feld3");
            }
        }
*/
}

void PHASELST::SetNoRecNr (void)
{
	   int i;

	   eListe.SetNoRecNr (TRUE);
	   if (NoRecNrSet) return;

	   for (i = 0; i < dataform->fieldanz; i ++)
	   {
		   dataform->mask[i].pos[1] -= 6;
	   }
	   for (i = 0; i < ubform->fieldanz; i ++)
	   {
		   ubform->mask[i].pos[1] -= 6;
	   }
	   for (i = 0; i < lineform->fieldanz; i ++)
	   {
		   lineform->mask[i].pos[1] -= 6;
	   }
       NoRecNrSet = TRUE;
}


PHASELST::PHASELST (short mdn, long dat) : KEYS (), LISTENTER () 
{

    ProgCfg = new PROG_CFG ("woplan");
	Dlg = NULL;
    cfgOK = FALSE;
	cursor = -1;
    prdk_k.mdn = mdn;
    prstatatag.mdn = mdn;
    AktDat = dat;

    LstTab = new PHASELSTS [0x10000];

    dataform  = &DataForm;
    ubform    = &UbForm;
    lineform  = &LineForm;
//    DelListField ("kosten");
//    SetFieldLen ("a", 9);
    eListe.SetUbRows (ubrows); 
	eListe.SetKeys (this);

    if ((work.ArtScroll == FALSE))
    {

           eListe.SetHscrollStart (3);
           UbForm.ScrollStart = 3;
    }

    LISTENTER::GetCfgValues ();
    GetCfgValues ();
    TestRemoves ();
    SetNoRecNr ();
	fChoise.Setchpos (0, 2);
    work = NULL;
    ChAttr = ChAttra;
    eListe.SetChAttr (ChAttr); 
    Toolbar2 = NULL;
    eListe.SetTestAppend (TestAppend);
//    eListe.SetInfoProc (InfoProc);
    eListe.SetInfo (this);
    ArrDown = NULL;
    ArrDown = LoadBitmap (DLG::hInstance, "ARRDOWN");
    eListe.SetListclipp (TRUE);
    eListe.SetNoScrollBkColor (GetSysColor (COLOR_3DFACE));
//    Ctxt.BkColor = GetSysColor (COLOR_3DFACE);
}


PHASELST::~PHASELST ()
{
    int i, anz;
	if (work != NULL)
	{
          work->SetListDlg (NULL);
	}
    fChoise.destroy ();
	if (LstTab != NULL)
	{

        anz =   eListe.GetRecanz ()	;
        for (i = 0; i < anz; i ++)
        {
        }

  	    delete []LstTab;
		LstTab = NULL;
	}
    if (ProgCfg != NULL)
    {
        ProgCfg->CloseCfg ();
        delete ProgCfg;
        ProgCfg = NULL;
    }
    if (work != NULL)
    {
        work->SetPhaseLsts (NULL);
    }

}


// Funktionen f�r den Zugriff auf die Daten. Sind als abstrakte Schnittstellen in
// listenter definiert.

unsigned char *PHASELST::GetDataRec (void)
{
    return (unsigned char *) &Lsts;
}

int PHASELST::GetDataRecLen (void)
{
    return sizeof (struct PHASELSTS);
}

char *PHASELST::GetListTab (int i)
{
    return (char *) &LstTab[i];
}

// Ende der Schnittstellen.

/*
void PHASELST::Setfeld2Removed (BOOL removed)
{
    if (removed)
    {
            SetFieldAttr ("feld2", REMOVED);
    }
    else
    {
            SetFieldAttr ("feld2", EDIT);
    }
}
*/

void PHASELST::Prepare (void)
{
    if (work != NULL)
    {
        cursor = work->PreparePhase ();
    }
}

void PHASELST::Open (void)
{
	if (cursor == -1)
	{
		Prepare ();
		if (cursor == -1) return;
	}
    if (work != NULL)
    {
        work->OpenPhase ();
    }
}

int PHASELST::Fetch (void)
{
	if (cursor == -1)
	{
		Prepare ();
		if (cursor == -1) return 100;
	}
    if (work != NULL)
    {
        return work->FetchPhase ();
    }
	return 100;
}


void PHASELST::Close (void)
{
	if (cursor != -1)
	{
		cursor = -1;
	}
    if (work != NULL)
    {
        work->ClosePhase ();
    }
}



BOOL PHASELST::BeforeCol (char *colname)
{
         double a = ratod (Lsts.a);
         ((SpezDlg *) Dlg)->SetChargen (0, 0, a);
         if (strcmp (colname, "me2") == 0)
         {
             oldme2 = ratod (Lsts.me2);
         }
         return FALSE;
}
         

BOOL PHASELST::AfterCol (char *colname)
{
         
         if (strcmp (colname, "me") == 0)
         {
              if (ratod (Lsts.me) > Lsts.PrStatMe)
              {
                  sprintf (Lsts.me,  "%.3lf", Lsts.PrStatMe);
                  sprintf (Lsts.me2, "%.3lf", 0.0);
                  sprintf (Lsts.me3, "%.3lf", 0.0);
              }
              if (ratod (Lsts.me3) > 0.0 &&
                       (ratod (Lsts.me) + ratod (Lsts.me2) + ratod (Lsts.me3) > Lsts.PrStatMe))
              {
                  sprintf (Lsts.me3, "%.3lf", Lsts.PrStatMe - 
                                              (ratod (Lsts.me) + ratod (Lsts.me2)));
                  if (ratod (Lsts.me3) < 0.0)
                  {
                      sprintf (Lsts.me3, "%.3lf", 0.0);
                  }
              }
              if (ratod (Lsts.me) + ratod (Lsts.me2) > Lsts.PrStatMe)
              {
                  sprintf (Lsts.me2, "%.3lf", Lsts.PrStatMe - ratod (Lsts.me));
                  if (ratod (Lsts.me2) < 0.0)
                  {
                      sprintf (Lsts.me2, "%.3lf", 0.0);
                  }
              }
              if (ratod (Lsts.me3) == 0.0)
              {
                   sprintf (Lsts.me2, "%.3lf", Lsts.PrStatMe - ratod (Lsts.me));

              }
              if (ratod (Lsts.me) + ratod (Lsts.me2) + ratod (Lsts.me3) < Lsts.PrStatMe)
              {
                  sprintf (Lsts.me2, "%.3lf", Lsts.PrStatMe - 
                                              (ratod (Lsts.me) + ratod (Lsts.me3)));
              }
              if (ratod (Lsts.me2) > 0.0)
              {
                  SetItemAttr (&eListe.DataForm, "me2", EDIT);
                  SetItemAttr (&eListe.DataForm, "masch_nr2", EDIT);
              }
              memcpy (&LstTab[eListe.GetAktRow ()], &Lsts, sizeof (Lsts));
              eListe.ShowAktRow ();
              return FALSE;
         }
         else if (strcmp (colname, "me2") == 0)
         {
              if (ratod (Lsts.me2) > Lsts.PrStatMe)
              {
                  sprintf (Lsts.me2,  "%.3lf", Lsts.PrStatMe);
                  sprintf (Lsts.me, "%.3lf", 0.0);
                  sprintf (Lsts.me3, "%.3lf", 0.0);
              }
              if (ratod (Lsts.me3) > 0.0 &&
                       (ratod (Lsts.me) + ratod (Lsts.me2) + ratod (Lsts.me3) > Lsts.PrStatMe))
              {
                  sprintf (Lsts.me3, "%.3lf", Lsts.PrStatMe - (ratod (Lsts.me) + ratod (Lsts.me2)));
                  if (ratod (Lsts.me3) < 0.0)
                  {
                      sprintf (Lsts.me3, "%.3lf", 0.0);
                  }
              }
              if (ratod (Lsts.me) + ratod (Lsts.me2) > Lsts.PrStatMe)
              {
                  sprintf (Lsts.me2, "%.3lf", Lsts.PrStatMe - ratod (Lsts.me));
                  if (ratod (Lsts.me2) < 0.0)
                  {
                      sprintf (Lsts.me2, "%.3lf", 0.0);
                  }
              }
              if (ratod (Lsts.me) + ratod (Lsts.me2) + ratod (Lsts.me3) < Lsts.PrStatMe)
              {
                  sprintf (Lsts.me3, "%.3lf", Lsts.PrStatMe - 
                                              (ratod (Lsts.me) + ratod (Lsts.me2)));
              }
              if (ratod (Lsts.me) + ratod (Lsts.me2) < Lsts.PrStatMe)
              {
                  if (ratod (Lsts.me3) == 0.0)
                  {
                         sprintf (Lsts.me3, "%.3lf", Lsts.PrStatMe - (ratod (Lsts.me) +
                                                                      ratod (Lsts.me2)));
                  }
                  SetItemAttr (&eListe.DataForm, "me3", EDIT);
                  SetItemAttr (&eListe.DataForm, "masch_nr3", EDIT);
              }
              else
              {
                  SetItemAttr (&eListe.DataForm, "me3", DISPLAYONLY);
                  SetItemAttr (&eListe.DataForm, "masch_nr3", DISPLAYONLY);
              }
              memcpy (&LstTab[eListe.GetAktRow ()], &Lsts, sizeof (Lsts));
              eListe.ShowAktRow ();
              return FALSE;
         }
         else if (strcmp (colname, "me3") == 0)
         {
              if (ratod (Lsts.me3) > Lsts.PrStatMe)
              {
                  sprintf (Lsts.me3,  "%.3lf", Lsts.PrStatMe);
                  sprintf (Lsts.me, "%.3lf", 0.0);
                  sprintf (Lsts.me2, "%.3lf", 0.0);
              }
              if (ratod (Lsts.me2) > 0.0 &&
                       (ratod (Lsts.me) + ratod (Lsts.me2) + ratod (Lsts.me3) > Lsts.PrStatMe))
              {
                  sprintf (Lsts.me2, "%.3lf", Lsts.PrStatMe - (ratod (Lsts.me) + ratod (Lsts.me3)));
                  if (ratod (Lsts.me2) < 0.0)
                  {
                      sprintf (Lsts.me2, "%.3lf", 0.0);
                  }
              }
              if (ratod (Lsts.me) + ratod (Lsts.me3) > Lsts.PrStatMe)
              {
                  sprintf (Lsts.me3, "%.3lf", Lsts.PrStatMe - ratod (Lsts.me));
                  if (ratod (Lsts.me3) < 0.0)
                  {
                      sprintf (Lsts.me3, "%.3lf", 0.0);
                  }
              }
              if (ratod (Lsts.me) + ratod (Lsts.me2) + ratod (Lsts.me3) < Lsts.PrStatMe)
              {
                  sprintf (Lsts.me2, "%.3lf", Lsts.PrStatMe - 
                                             (ratod (Lsts.me) + ratod (Lsts.me3)));
              }
              if (ratod (Lsts.me) + ratod (Lsts.me3) < Lsts.PrStatMe)
              {
                  if (ratod (Lsts.me2) == 0.0)
                  {
                         sprintf (Lsts.me2, "%.3lf", Lsts.PrStatMe - (ratod (Lsts.me) +
                                                                      ratod (Lsts.me3)));
                  }
                  SetItemAttr (&eListe.DataForm, "me2", EDIT);
                  SetItemAttr (&eListe.DataForm, "masch_nr2", EDIT);
              }
              else
              {
                  SetItemAttr (&eListe.DataForm, "me2", DISPLAYONLY);
                  SetItemAttr (&eListe.DataForm, "masch_nr2", DISPLAYONLY);
              }
              memcpy (&LstTab[eListe.GetAktRow ()], &Lsts, sizeof (Lsts));
              eListe.ShowAktRow ();
              return FALSE;
         }
         else if (strcmp (colname, "chmasch_nr") == 0)
         {
              if ((eListe.DataForm.mask [GetItemPos (&eListe.DataForm, "me2")].attribut &
                   DISPLAYONLY) == 0)
              {
                   ShowMaschNr ("masch_nr2");
              }
         }
         else if (strcmp (colname, "chmasch_nr3") == 0)
         {
              if ((eListe.DataForm.mask [GetItemPos (&eListe.DataForm, "me3")].attribut &
                   DISPLAYONLY) == 0)
              {
                   ShowMaschNr ("masch_nr3");
              }
         }
         return FALSE;
}

int PHASELST::AfterRow (int Row)
{
//         form * DataForm; 

/*
         if (syskey == KEYUP && ratod (Lsts.nr) == 0.0)
         {
                return OnKey7 ();
         }
*/
         memcpy (&LstTab [Row], &Lsts, sizeof (Lsts));
         work->SetPhaseLsts (&Lsts);
         return 1;
}

int PHASELST::BeforeRow (int Row)
{
         memcpy (&LstTab [Row], &Lsts, sizeof (Lsts));
         double a = ratod (Lsts.a);
         ((SpezDlg *) Dlg)->SetChargen (0, 0, a);
         if (ratod (Lsts.me2) > 0.0)
         {
                  SetItemAttr (&eListe.DataForm, "me2", EDIT);
                  SetItemAttr (&eListe.DataForm, "masch_nr2", EDIT);
         }
         else
         {
                  SetItemAttr (&eListe.DataForm, "me2",       DISPLAYONLY);
                  SetItemAttr (&eListe.DataForm, "masch_nr2", DISPLAYONLY);
         }
         if (ratod (Lsts.me3) > 0.0)
         {
                  SetItemAttr (&eListe.DataForm, "me3", EDIT);
                  SetItemAttr (&eListe.DataForm, "masch_nr3", EDIT);
         }
         else
         {
                  SetItemAttr (&eListe.DataForm, "me3",       DISPLAYONLY);
                  SetItemAttr (&eListe.DataForm, "masch_nr3", DISPLAYONLY);
         }

         return 1;
}


int PHASELST::ShowMaschNr (void)
{

        struct SMASCH *smasch;
        SEARCHMASCH SearchMasch;


        SearchMasch.SetWindowStyle (WS_POPUP | WS_THICKFRAME);
        SearchMasch.SetParams (DLG::hInstance, GetParent (hMainWindow));
        if (Dlg != NULL)
        {
               SearchMasch.Setawin (((SpezDlg *) Dlg)->GethMainWindow ());
        }
        SearchMasch.Search ();
        if (syskey == KEY5)
        {
            syskey = 0;
            SetListFocus ();
            return 0;
        }

        smasch = SearchMasch.GetSMasch ();
        if (smasch == NULL)
        {
            syskey = 0;
            SetListFocus ();
            return 0;
        }

        int chpos = eListe.GetAktColumn ();
        if (chpos != -1)
        {
              strcpy (DataForm.mask[chpos].item->GetFeldPtr (),
                      smasch->masch_nr);
              memcpy (&LstTab [eListe.GetAktRow ()], &Lsts, sizeof (Lsts));
              eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
              eListe.ShowAktRow ();
        }
        SetListFocus ();
        return 0;
}

int PHASELST::ShowMaschNr (char *colname)
{

        struct SMASCH *smasch;
        SEARCHMASCH SearchMasch;


        SearchMasch.SetWindowStyle (WS_POPUP | WS_THICKFRAME);
        SearchMasch.SetParams (DLG::hInstance, GetParent (hMainWindow));
        if (Dlg != NULL)
        {
               SearchMasch.Setawin (((SpezDlg *) Dlg)->GethMainWindow ());
        }
        SearchMasch.Search ();
        if (syskey == KEY5)
        {
            syskey = 0;
            SetListFocus ();
            return 0;
        }

        smasch = SearchMasch.GetSMasch ();
        if (smasch == NULL)
        {
            syskey = 0;
            SetListFocus ();
            return 0;
        }

        int chpos = GetItemPos (&DataForm, colname);
        if (chpos != -1)
        {
              strcpy (DataForm.mask[chpos].item->GetFeldPtr (),
                      smasch->masch_nr);
              memcpy (&LstTab [eListe.GetAktRow ()], &Lsts, sizeof (Lsts));
              eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
              eListe.ShowAktRow ();
        }
        SetListFocus ();
        return 0;
}


BOOL PHASELST::NewMaschNr (long MnrTab[], long masch_nr, int NrPos)
{
    if (masch_nr == 0)
    {
        return FALSE;
    }

    for (int i = 0; i < NrPos; i ++)
    {
        if (MnrTab[i] == masch_nr)
        {
            return FALSE;
        }
    }
    return TRUE;
}


BOOL PHASELST::StopList (void)
{
    int i;
    int recanz;
    double chg_gew = 0.0;
    Text WText;
    long MnrTab[256];
    int NrPos = 0;     
    
    if (AfterRow (eListe.GetAktRow ()) == -1)
    {
        return TRUE;
    }

    eListe.GetFocusText ();
    recanz = eListe.GetRecanz ();

    for (i = 0; i < recanz; i ++)
    {
        if (LstTab[i].MaschNr2 != atol (LstTab[i].masch_nr2))
        {
            if (NrPos < 256 && NewMaschNr (MnrTab, atol (LstTab[i].masch_nr2), NrPos))
            {
                   MnrTab[NrPos] = atol (LstTab[i].masch_nr2);
                   NrPos ++;
            }
        }
        if (LstTab[i].MaschNr3 != atol (LstTab[i].masch_nr3))
        {
            if (NrPos < 256 && NewMaschNr (MnrTab, atol (LstTab[i].masch_nr3), NrPos))
            {
                   MnrTab[NrPos] = atol (LstTab[i].masch_nr3);
                   NrPos ++;
            }
        }
        WriteRec (i);
    }
    if (NrPos > 0)
    {
        WText = "Die Reihenfolge f�r folgende Maschinen muss bearbeitet werden\nMaschinen : ";
        for (i = 0; i < NrPos; i ++)
        {
            if (i > 0)
            {
                WText + ",";
            }
            WText.Format ("%s%ld ", WText.GetBuffer (), MnrTab[i]);
        }
        disp_mess (WText.GetBuffer (), 1);
    }
	return TRUE;
}

BOOL PHASELST::OnKeyNext (void)
{
        if (GetKeyState (VK_CONTROL) >= 0)
        {
              if (Dlg != NULL)
              {
                  ((SpezDlg *) Dlg)->NextPage ();
                  return TRUE;
              }
        }
        return FALSE;
}


BOOL PHASELST::OnKeyPrior (void)
{
        if (GetKeyState (VK_CONTROL) >= 0)
        {
              if (Dlg != NULL)
              {
                  ((SpezDlg *) Dlg)->PriorPage ();
                  return TRUE;
              }
        }
        return FALSE;
}


BOOL PHASELST::OnKey5 (void)
{

//    StopList ();
    syskey = KEY5;
    if (work != NULL)
    {
        work->RollbackWork ();
    }
	return TRUE;

}

int PHASELST::TestAppend (void)
{
    switch (syskey)
    {
         case KEY6 :
           return 0;
         case KEY7 :
           return 0;
         case KEY8 :
           return 0;
    }
    return 1;
}

BOOL PHASELST::OnKey6 (void)
{
//    eListe.InsertLine ();
    return TRUE;
}

BOOL PHASELST::OnKeyInsert (void)
{
    return FALSE;
}

BOOL PHASELST::OnKey7 (void)
{
//    eListe.DeleteLine ();
    return TRUE;
}

BOOL PHASELST::OnKeyDel (void)
{
    return FALSE;
}

BOOL PHASELST::OnKey8 (void)
{
         return FALSE;
}


BOOL PHASELST::OnKey9 (void)
{
    char * Item = eListe.GetAktItem ();
    if (strcmp (Item, "masch_nr2") == 0)
    {
         ShowMaschNr ();
    }
    else if (strcmp (Item, "masch_nr3") == 0)
    {
         ShowMaschNr ();
    }
    return TRUE;
}


BOOL PHASELST::OnKey10 (void)
{
    if (Dlg != NULL)
    {
         work->SetPhaseLsts (&Lsts);
// Hier Workfunktion einsetzen
         SetListFocus ();
         return TRUE;
	}
    return FALSE;
}

BOOL PHASELST::OnKey11 (void)
{

    if (Dlg != NULL)
    {
        ((SpezDlg *) Dlg)->OnKey11 ();
        SetAktFocus ();
    }
    return FALSE;
}


int PHASELST::ToMemory (int i)
{

    if (atol (Lsts.masch_nr) != work->GetAktMaschNr ())
    {
        return 0;
    }
    LISTENTER::ToMemory (i);
    return 0;
}


void PHASELST::InitRow (void)
{

    LISTENTER::InitRow ();


    work->SetPhaseLsts (&Lsts);
    work->InitMaschRow ();

    if (ArrDown != NULL)
    {
        Lsts.chmasch_nr.SetBitMap (ArrDown);
    }
    if (ArrDown != NULL)
    {
        Lsts.chmasch_nr3.SetBitMap (ArrDown);
    }


}

void PHASELST::uebertragen (void)
{
    work->SetPhaseLsts (&Lsts);
    work->FillPhaseRow ();

    if (ArrDown != NULL)
    {
        Lsts.chmasch_nr.SetBitMap (ArrDown);
    }
    if (ArrDown != NULL)
    {
        Lsts.chmasch_nr3.SetBitMap (ArrDown);
    }
}

void PHASELST::WriteRec (int i)
{
    memcpy (&Lsts, &LstTab[i], sizeof (Lsts));
    work->SetPhaseLsts (&LstTab[i]);
    work->WritePhaseRec ();
}

BOOL PHASELST::OnKey12 (void)
{
    StopList ();
    syskey = KEY12;
    if (work != NULL)
    {
        work->CommitWork ();
    }
    return TRUE;
}


BOOL PHASELST::OnKeyUp (void)
{
    return FALSE;
}

BOOL PHASELST::OnKeyDown (void)
{

    return FALSE;
}


void PHASELST::CreateListQuikInfos (HWND hWnd)
{
       if (Toolbar2 == NULL) return;

       Toolbar2->GetCfield ("f5")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("f12")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("f9")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("del")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("insert")->CreateQuikInfos (hWnd);
}


BOOL PHASELST::OnCommand (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    if (LOWORD (wParam) == IDM_CHOISE)
    {
        return OnKey9 ();
    }
    if (LOWORD (wParam) == IDM_CANCEL)
    {
        return OnKey5 ();
    }
    if (LOWORD (wParam) == IDM_WRITE)
    {
        return OnKey12 ();
    }
    if (LOWORD (wParam) == VK_DELETE)
    {
        return OnKeyDel ();
    }
    if (LOWORD (wParam) == IDM_DELETE)
    {
        return OnKey7 ();
    }
    if (LOWORD (wParam) == VK_INSERT)
    {
        return OnKeyInsert ();
    }
    if (LOWORD (wParam) == IDM_INSERT)
    {
        return OnKey6 ();
    }
    if (HIWORD (wParam)  == CBN_CLOSEUP)
	{
        if (Dlg != NULL)
        {
	           return ((SpezDlg *) Dlg)->OnCloseUp (hWnd, msg, wParam, lParam);
        }
	}
    if (HIWORD (wParam)  == CBN_DROPDOWN)
	{
/*
        if (strcmp (DataForm.mask[eListe.GetAktColumn ()].item->GetItemName (),
            "nr") == 0)
        {
             ShowNr ();
             eListe.CloseDropDown ();
             return TRUE;
        }
*/
	}
    return FALSE;
}

BOOL PHASELST::OnPaint (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    HDC hdc;

    if (Modal == FALSE) return FALSE;
    if (this->mamain1 != hWnd) return FALSE; 
    
    hdc = BeginPaint (hWnd, &ps);
    fChoise.display (hWnd, hdc);
    EndPaint (hWnd,&ps);
    return FALSE;
}

BOOL PHASELST::OnMove (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{

    if (Modal == FALSE) return FALSE;
    if (this->mamain1 != hWnd) return FALSE; 

    fChoise.MoveWindow ();
    return FALSE;
}








