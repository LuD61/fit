#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "wmaskc.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "prstatmasch.h"

struct PRSTATMASCH prstatmasch, prstatmasch_null;

void PRSTATMASCH_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *)   &prstatmasch.mdn, 1, 0);
            ins_quest ((char *)   &prstatmasch.a,  3, 0);
            ins_quest ((char *)   &prstatmasch.dat,  2, 0);
            ins_quest ((char *)   &prstatmasch.posi,  1, 0);
            ins_quest ((char *)   &prstatmasch.phase,  1, 0);
            ins_quest ((char *)   &prstatmasch.masch_nr,  2, 0);
    out_quest ((char *) &prstatmasch.mdn,1,0);
    out_quest ((char *) &prstatmasch.dat,2,0);
    out_quest ((char *) &prstatmasch.a,3,0);
    out_quest ((char *) &prstatmasch.phase,1,0);
    out_quest ((char *) &prstatmasch.masch_nr,2,0);
    out_quest ((char *) &prstatmasch.me,3,0);
    out_quest ((char *) &prstatmasch.folge,1,0);
    out_quest ((char *) &prstatmasch.status,1,0);
    out_quest ((char *) &prstatmasch.posi,1,0);
            cursor = prepare_sql ("select prstatmasch.mdn,  "
"prstatmasch.dat,  prstatmasch.a,  prstatmasch.phase,  "
"prstatmasch.masch_nr,  prstatmasch.me,  prstatmasch.folge,  "
"prstatmasch.status,  prstatmasch.posi from prstatmasch "

#line 25 "prstatmasch.rpp"
                                  "where mdn = ? "
                                  "and   a   = ? "
                                  "and   dat = ? "
                                  "and   posi = ? "
                                  "and   phase = ? "
                                  "and   masch_nr = ?");
    ins_quest ((char *) &prstatmasch.mdn,1,0);
    ins_quest ((char *) &prstatmasch.dat,2,0);
    ins_quest ((char *) &prstatmasch.a,3,0);
    ins_quest ((char *) &prstatmasch.phase,1,0);
    ins_quest ((char *) &prstatmasch.masch_nr,2,0);
    ins_quest ((char *) &prstatmasch.me,3,0);
    ins_quest ((char *) &prstatmasch.folge,1,0);
    ins_quest ((char *) &prstatmasch.status,1,0);
    ins_quest ((char *) &prstatmasch.posi,1,0);
            sqltext = "update prstatmasch set "
"prstatmasch.mdn = ?,  prstatmasch.dat = ?,  prstatmasch.a = ?,  "
"prstatmasch.phase = ?,  prstatmasch.masch_nr = ?,  "
"prstatmasch.me = ?,  prstatmasch.folge = ?,  "
"prstatmasch.status = ?,  prstatmasch.posi = ? "

#line 32 "prstatmasch.rpp"
                                  "where mdn = ? "
                                  "and   a   = ? "
                                  "and   dat = ? "
                                  "and   posi = ? "
                                  "and   phase = ? "
                                  "and   masch_nr = ?";
            ins_quest ((char *)   &prstatmasch.mdn, 1, 0);
            ins_quest ((char *)   &prstatmasch.a,  3, 0);
            ins_quest ((char *)   &prstatmasch.dat,  2, 0);
            ins_quest ((char *)   &prstatmasch.posi,  1, 0);
            ins_quest ((char *)   &prstatmasch.phase,  1, 0);
            ins_quest ((char *)   &prstatmasch.masch_nr,  2, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *)   &prstatmasch.mdn, 1, 0);
            ins_quest ((char *)   &prstatmasch.a,  3, 0);
            ins_quest ((char *)   &prstatmasch.dat,  2, 0);
            ins_quest ((char *)   &prstatmasch.posi,  1, 0);
            ins_quest ((char *)   &prstatmasch.phase,  1, 0);
            ins_quest ((char *)   &prstatmasch.masch_nr,  2, 0);
            test_upd_cursor = prepare_sql ("select a from prstatmasch "
                                  "where mdn = ? "
                                  "and   a   = ? "
                                  "and   dat = ? "
                                  "and   posi = ? "
                                  "and   phase = ? "
                                  "and   masch_nr = ? "
                                  "for update");

            ins_quest ((char *)   &prstatmasch.mdn, 1, 0);
            ins_quest ((char *)   &prstatmasch.a,  3, 0);
            ins_quest ((char *)   &prstatmasch.dat,  2, 0);
            ins_quest ((char *)   &prstatmasch.posi,  1, 0);
            ins_quest ((char *)   &prstatmasch.phase,  1, 0);
            ins_quest ((char *)   &prstatmasch.masch_nr,  2, 0);
            del_cursor = prepare_sql ("delete from prstatmasch "
                                  "where mdn = ? "
                                  "and   a   = ? "
                                  "and   dat = ? "
                                  "and   posi = ? "
                                  "and   phase = ? "
                                  "and   masch_nr = ?");
    ins_quest ((char *) &prstatmasch.mdn,1,0);
    ins_quest ((char *) &prstatmasch.dat,2,0);
    ins_quest ((char *) &prstatmasch.a,3,0);
    ins_quest ((char *) &prstatmasch.phase,1,0);
    ins_quest ((char *) &prstatmasch.masch_nr,2,0);
    ins_quest ((char *) &prstatmasch.me,3,0);
    ins_quest ((char *) &prstatmasch.folge,1,0);
    ins_quest ((char *) &prstatmasch.status,1,0);
    ins_quest ((char *) &prstatmasch.posi,1,0);
            ins_cursor = prepare_sql ("insert into prstatmasch ("
"mdn,  dat,  a,  phase,  masch_nr,  me,  folge,  status,  posi) "

#line 75 "prstatmasch.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?)");

#line 77 "prstatmasch.rpp"
}
