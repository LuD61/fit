#ifndef _VERBR_TAG_DEF
#define _VERBR_TAG_DEF

#include "windows.h"
#include "mo_draw.h"
#include "wmask.h"
#include "dbclass.h"

struct VERBR_TAG {
   short     mdn;
   long      dat;
   double    a;
   double    me_ist;
   double    anz_einh;
   double    zerl_me;
   short     me_einh;
   double    mat_o_b;
   double    hk_vollk;
   double    hk_teilk;
   short     typ;
   short     prdk_stufe;
   double    artikel;
   char      bearb_info[81];
   short	 prodabt;
};
extern struct VERBR_TAG verbr_tag, verbr_tag_null;

#line 10 "verbr_tag.rh"

class VERBR_TAG_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               VERBR_TAG_CLASS () : DB_CLASS ()
               {
               }
};
#endif
