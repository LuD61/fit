#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "wmaskc.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "maschinen.h"

struct MASCHINEN maschinen, maschinen_null;

void MASCHINEN_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *)   &maschinen.masch_nr,  2, 0);
    out_quest ((char *) &maschinen.masch_nr,2,0);
    out_quest ((char *) &maschinen.prod_abt,1,0);
    out_quest ((char *) maschinen.masch_bz,0,25);
    out_quest ((char *) &maschinen.masch_typ,1,0);
    out_quest ((char *) &maschinen.masch_kap,2,0);
    out_quest ((char *) &maschinen.prod_abt2,1,0);
    out_quest ((char *) &maschinen.prod_abt3,1,0);
    out_quest ((char *) &maschinen.prod_abt4,1,0);
    out_quest ((char *) maschinen.pers1,0,13);
    out_quest ((char *) maschinen.pers2,0,13);
    out_quest ((char *) maschinen.pers3,0,13);
    out_quest ((char *) maschinen.pers4,0,13);
            cursor = prepare_sql ("select maschinen.masch_nr,  "
"maschinen.prod_abt,  maschinen.masch_bz,  maschinen.masch_typ,  "
"maschinen.masch_kap,  maschinen.prod_abt2,  maschinen.prod_abt3,  "
"maschinen.prod_abt4,  maschinen.pers1,  maschinen.pers2,  "
"maschinen.pers3,  maschinen.pers4 from maschinen "

#line 20 "maschinen.rpp"
                                  "where masch_nr = ? ");

    ins_quest ((char *) &maschinen.masch_nr,2,0);
    ins_quest ((char *) &maschinen.prod_abt,1,0);
    ins_quest ((char *) maschinen.masch_bz,0,25);
    ins_quest ((char *) &maschinen.masch_typ,1,0);
    ins_quest ((char *) &maschinen.masch_kap,2,0);
    ins_quest ((char *) &maschinen.prod_abt2,1,0);
    ins_quest ((char *) &maschinen.prod_abt3,1,0);
    ins_quest ((char *) &maschinen.prod_abt4,1,0);
    ins_quest ((char *) maschinen.pers1,0,13);
    ins_quest ((char *) maschinen.pers2,0,13);
    ins_quest ((char *) maschinen.pers3,0,13);
    ins_quest ((char *) maschinen.pers4,0,13);
            sqltext = "update maschinen set "
"maschinen.masch_nr = ?,  maschinen.prod_abt = ?,  "
"maschinen.masch_bz = ?,  maschinen.masch_typ = ?,  "
"maschinen.masch_kap = ?,  maschinen.prod_abt2 = ?,  "
"maschinen.prod_abt3 = ?,  maschinen.prod_abt4 = ?,  "
"maschinen.pers1 = ?,  maschinen.pers2 = ?,  maschinen.pers3 = ?,  "
"maschinen.pers4 = ? "

#line 23 "maschinen.rpp"
                                  "where masch_nr = ? ";
            ins_quest ((char *)   &maschinen.masch_nr,  2, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *)   &maschinen.masch_nr,  2, 0);
            test_upd_cursor = prepare_sql ("select masch_nr from maschinen "
                                  "where masch_nr = ? "
                                  "for update");

            ins_quest ((char *)   &maschinen.masch_nr,  2, 0);
            del_cursor = prepare_sql ("delete from maschinen "
                                  "where masch_nr = ? ");
    ins_quest ((char *) &maschinen.masch_nr,2,0);
    ins_quest ((char *) &maschinen.prod_abt,1,0);
    ins_quest ((char *) maschinen.masch_bz,0,25);
    ins_quest ((char *) &maschinen.masch_typ,1,0);
    ins_quest ((char *) &maschinen.masch_kap,2,0);
    ins_quest ((char *) &maschinen.prod_abt2,1,0);
    ins_quest ((char *) &maschinen.prod_abt3,1,0);
    ins_quest ((char *) &maschinen.prod_abt4,1,0);
    ins_quest ((char *) maschinen.pers1,0,13);
    ins_quest ((char *) maschinen.pers2,0,13);
    ins_quest ((char *) maschinen.pers3,0,13);
    ins_quest ((char *) maschinen.pers4,0,13);
            ins_cursor = prepare_sql ("insert into maschinen ("
"masch_nr,  prod_abt,  masch_bz,  masch_typ,  masch_kap,  prod_abt2,  prod_abt3,  "
"prod_abt4,  pers1,  pers2,  pers3,  pers4) "

#line 36 "maschinen.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?)");

#line 38 "maschinen.rpp"
}
