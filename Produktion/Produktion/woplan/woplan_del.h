#ifndef _WOPLAN_DEL_DEF
#define _WOPLAN_DEL_DEF

#include "windows.h"
#include "mo_draw.h"
#include "wmask.h"
#include "dbclass.h"

struct WOPLAN_DEL {
   short     mdn;
   short     kw;
   short     jr;
   double    a;
};
extern struct WOPLAN_DEL woplan_del, woplan_del_null;

#line 10 "woplan_del.rh"

class WOPLAN_DEL_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               WOPLAN_DEL_CLASS () : DB_CLASS ()
               {
               }
};
#endif
