#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "wmaskc.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "woplan_del.h"

struct WOPLAN_DEL woplan_del, woplan_del_null;

void WOPLAN_DEL_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *)   &woplan_del.mdn,  1, 0);
            ins_quest ((char *)   &woplan_del.jr,  1, 0);
            ins_quest ((char *)   &woplan_del.kw,  1, 0);
            ins_quest ((char *)   &woplan_del.a,  3, 0);
    out_quest ((char *) &woplan_del.mdn,1,0);
    out_quest ((char *) &woplan_del.kw,1,0);
    out_quest ((char *) &woplan_del.jr,1,0);
    out_quest ((char *) &woplan_del.a,3,0);
            cursor = prepare_sql ("select woplan_del.mdn,  "
"woplan_del.kw,  woplan_del.jr,  woplan_del.a from woplan_del "

#line 23 "woplan_del.rpp"
                                  "where mdn = ? and jr = ? and kw = ? and a = ? ");

    ins_quest ((char *) &woplan_del.mdn,1,0);
    ins_quest ((char *) &woplan_del.kw,1,0);
    ins_quest ((char *) &woplan_del.jr,1,0);
    ins_quest ((char *) &woplan_del.a,3,0);
            sqltext = "update woplan_del set "
"woplan_del.mdn = ?,  woplan_del.kw = ?,  woplan_del.jr = ?,  "
"woplan_del.a = ? "

#line 26 "woplan_del.rpp"
                                  "where mdn = ? and jr = ? and kw = ? and a = ? ";
            ins_quest ((char *)   &woplan_del.mdn,  1, 0);
            ins_quest ((char *)   &woplan_del.jr,  1, 0);
            ins_quest ((char *)   &woplan_del.kw,  1, 0);
            ins_quest ((char *)   &woplan_del.a,  3, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *)   &woplan_del.mdn,  1, 0);
            ins_quest ((char *)   &woplan_del.jr,  1, 0);
            ins_quest ((char *)   &woplan_del.kw,  1, 0);
            ins_quest ((char *)   &woplan_del.a,  3, 0);
            test_upd_cursor = prepare_sql ("select a from woplan_del "
                                  "where mdn = ? and jr = ? and kw = ? and a = ? "
                                  "for update");

            ins_quest ((char *)   &woplan_del.mdn,  1, 0);
            ins_quest ((char *)   &woplan_del.jr,  1, 0);
            ins_quest ((char *)   &woplan_del.kw,  1, 0);
            ins_quest ((char *)   &woplan_del.a,  3, 0);
            del_cursor = prepare_sql ("delete from woplan_del "
                                  "where mdn = ? and jr = ? and kw = ? and a = ? ");
    ins_quest ((char *) &woplan_del.mdn,1,0);
    ins_quest ((char *) &woplan_del.kw,1,0);
    ins_quest ((char *) &woplan_del.jr,1,0);
    ins_quest ((char *) &woplan_del.a,3,0);
            ins_cursor = prepare_sql ("insert into woplan_del ("
"mdn,  kw,  jr,  a) "

#line 48 "woplan_del.rpp"
                                      "values "
                                      "(?,?,?,?)");

#line 50 "woplan_del.rpp"
}
