#ifndef SINIX
#ident  "@(#) datum.c	2.001g	17.01.96	by RO"
#endif

/*** 
--------------------------------------------------------------------------------
-
-       BSA   Bizerba Software- und Automationssysteme GmbH
-       Vorstadtstr. 59
-       7465 Geislingen
-
-       Tel.: 07433/96800     Fax: 07433/968030
-
--------------------------------------------------------------------------------
-
-       Modulname               :       
-
-       Autor                   :       F.Folmer
-       Erstellungsdatum        :       05.10.93
-       Modifikationsdatum      :       TT.MM.JJ
-
-       Projekt                 :       BWS
-       Version                 :       1.00
-       Laendervariante         :       BRD
-
-       Rechner                 :       UNISYS 386
-       Betriebssystem          :       SCO-UNIX  386
-       Sprache                 :       C
-
-       Aenderungsjournal       :
-
-       lfd.    Version Datum           Name       Beschreibung
-       ------------------------------------------------------------------------
-       1       1.00    04.09.91	A.Folmer  Grundmodul
--------------------------------------------------------------------------------
-
-       Modulbeschreibung       :	HILFSFUNKTIONEN FUER DATUM.
-				
-
-					
--------------------------------------------------------------------------------
##D2A
Verwendete Tabellen:

##D2E
--------------------------------------------------------------------------------
##D3A
Logische Tabellenverknuepfung

##D3E
--------------------------------------------------------------------------------
##D4A
	Tabellenname		Felder		Zugriff(r=read,w=write,u=update)
-       ------------------------------------------------------------------------

##D4E
--------------------------------------------------------------------------------
##D5A
	Verwendete itm		ptm			itm
-       ------------------------------------------------------------------------

##D5E
--------------------------------------------------------------------------------
##D6A
	Verwendete Modulen			mit Proceduren
-       ------------------------------------------------------------------------

	Verwendeter Includes			mit Procedurnamen
-       ------------------------------------------------------------------------

##D6E
--------------------------------------------------------------------------------
##D7A
	Programmspezifische Module		Include
-       ------------------------------------------------------------------------

##D7E
--------------------------------------------------------------------------------
***/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

extern long dasc_to_long (char *);
extern int dlong_to_asc (long, char *);

/* prototyp */
short get_jahr(char *);
short get_monat(char *);
short get_woche(char *);
short get_tag(char *);
short get_lauf_tag(char *);
short get_wochentag(char *);
short get_schalt_jahr(short);
double zeit_yo_dec(char *);
static short woche_korr(short,short);
static short woche_pre(char *);

#define   NEU   (short)1
#define   ALT   (short)2

short mon_tab[12]={0,31,28,31,30,31,30,31,31,30,31,30}; /* in get_lauf_tag()*/


/************* Funktionen **************/

/***************************************/
/*    Funkton liefert jahr (short)     */
/***************************************/
short get_jahr(char *datum)
{
char  dat[16];
strcpy(dat,datum);
if(strlen(dat) == 8)
      return((short)(atoi(&dat[6])));
else
      return((short)(atoi(&dat[8])));
}

/***************************************/
/*    Funkton liefert monat(short)     */
/***************************************/
short get_monat(char *datum)
{
char  dat[16];
strcpy(dat,datum);
dat[5]='\0';
return((short)(atoi(&dat[3])));
}



/***************************************/
/*    Funkton liefert tag (short)      */
/***************************************/
short get_tag(char *datum)
{
char  dat[16];
strcpy(dat,datum);
dat[2]='\0';
return((short)(atoi(dat)));
}

/********************************************/
/*    Funkton liefert laufenden Tag im Jahr */
/********************************************/
short get_lauf_tag(char *datum)
{
short i,l_tag=0,tag,mon;
mon=get_monat(datum);
tag=get_tag(datum);
for(i=1;i<mon;i++)
    {
    l_tag += mon_tab[i];
    }
if(mon > 2)
     l_tag += tag + get_schalt_jahr(get_jahr(datum));
else
     l_tag += tag;

return(l_tag);
}

/********************************************/
/*    Funkton liefert 1/0 wenn Schalt-Jahr  */
/********************************************/
short get_schalt_jahr(short jahr)   /* jahr 2-Stellig */
{
short i;
i=jahr%4;
if(i == 0) 
    return(1);
else
    return(0);
}

/********************************************/
/*    Funkton liefert Wochentag             */
/*          Basis  01.01.1990               */
/*    1 = Montag .. 7 = Sonntag             */
/********************************************/
short get_wochentag(char *datum)
{
short jahr;
short wochentag;
short i;
int   rest=0;
int   tage=0;

jahr = get_jahr(datum);
if(jahr < 90 )
	jahr += 100;
for(i=90;i<jahr;i++)
	{
	rest = (int)(i % 4);
	if(rest != 0)
		tage += 365;
	else
		tage += 366;
	}
tage += (int)get_lauf_tag(datum);

wochentag = (short)(tage % 7);
if(wochentag == 0)
	return(7);
return(wochentag);
}/*end of get_wochentag() */
/**********************************************/
/*    Funkton liefert Kalenderwoche im Jahr   */
/*           52(53)     vom Vorjahr           */
/*           1 ..52(53) im Jahr               */
/*           1          fuer Neujahr(im Dez)  */
/**********************************************/
short get_woche(char *datum)
{
short woche;
short jahr;
char  buff[16];

woche = woche_pre(datum);
if(woche == 0) /* Korrektur bei Anfang des Jahres */
	{
	jahr = get_jahr(datum);
	if(jahr == 0)
		jahr = 99;
	else
		jahr--;
	sprintf(buff,"31.12.%02d",jahr);
        woche = woche_pre(buff);
	}
return(woche);
}/* end of get_woche() */

/****static Funktionen fuer get_woche********/
/****static get_woche_pre********************/
static short woche_pre(char *datum)
{
short woche,jahr,rest;
short korr_neu;
short l_tag;

l_tag = get_lauf_tag(datum);
jahr = get_jahr(datum);
korr_neu = woche_korr(jahr,NEU);
woche = (short)(l_tag / 7);
rest  = (short)(l_tag % 7);

if(rest == 0)
	{
	woche--;
	rest = 7;
	}
if(korr_neu <= 4)
	woche++;
if(korr_neu > 1)
	if(rest >= ((short)9 - korr_neu))
		woche++;
if(woche == 53)
	{
	if(woche_korr(jahr,ALT) <= 3)
		woche = 1;
	}
if(woche == 0)
	{
	}
return(woche);
} /* end of static woche_pre() */

/***********static woche_korr***************/
/*          Basis = 01.01.90               */

static short woche_korr(short jahr_ein,short par)
{
short jahr;
short wochentag;
short i;
int   rest=0;
int   tage=0;

jahr = jahr_ein;
if(par == ALT) /* Ende des Jahres */
	jahr++;
if(jahr < 90 )
	jahr += 100;

for(i=90;i<jahr;i++)
	{
	rest = (int)(i % 4);
	if(rest != 0)
		tage += 365;
	else
		tage += 366;
	}
if(par == NEU) /* Anfang des Jahres */
	tage++;

wochentag = (short)(tage % 7);
if(wochentag == 0)
	return(7);

return(wochentag);
}/* end of static woche_korr() */

/***************** zeit_to_dec ********************/
/*    min in decimal Darstellung                  */

double zeit_to_dec(char *zeit)
{
char zwi[8];
double arb_double;

strcpy(zwi,zeit);

zwi[2] = '\0';
arb_double = (double)((double)atol(&zwi[3]) / 60.0) + (double)atol(zwi);
return (arb_double);
} /* end of zeit_to_dec */


/**************************************************************************/
/*              int tages_datum_zeit(long zeit,short par,char * ausgabe)  */
/*------------------------------------------------------------------------*/
/*  Prozedure rechnet Datum und Uhrzeit aus der Anzahl Sekunden           */
/*  seit 01.01.1970, die in der Variable zeit �bergeben sind.          */
/*          wenn par=0  liefert Datum: tt.mm.yyyy  im ausgabe             */
/*          wenn par=1  liefert Datum: tt.mm.yy    im ausgabe             */
/*          wenn par=2  liefert Zeit : hh:mm:ss    im ausgabe             */
/*          wenn par=3  liefert Zeit : hh:mm       im ausgabe             */
/*          wenn par=4  liefert letzte Ziffer des  im return und ausgabe  */
/*          wenn par=5  liefert Zeit : hhmmss      im ausgabe             */
/**************************************************************************/

int tages_datum_zeit(long zeit, short par, char *ausgabe)
{
long int arb;
int  i,jahr,mon,schal,tag,std,min,sek;

arb = (long int)(zeit - 725846400);
for(i=1;;i++)
   {
   if((i % 4) == 0)
      {
      if(arb < 31622400)
        {
        schal = 1;
        break;
        }
      arb = arb - 31622400;
      continue;
      }
   schal = 0;
   if(arb < 31536000)
        break;
   arb = arb - 31536000;
   }
jahr = 1992 + i;
for(i=1;i<=12;i++)
   {
   if(i == 1 || i == 3 || i == 5 || i == 7 || i == 8 || i == 10 || i == 12)
      {
      if(arb < 2678400)
        break;
      arb = arb - 2678400;
      continue;
      }
   if(i == 2)
      {
      if(arb < (2419200 + schal * 86400))
        break;
      arb = arb - (2419200 + schal * 86400);
      continue;
      }
   if(arb < 2592000)
        break;
   arb = arb - 2592000;
   }
mon = i;
for(i=1;;i++)
   {
   if(arb < 86400)
        break;
   arb = arb - 86400;
   }
tag = i;

/****** return letzte Ziffer tages_datum *****/
if (par == 4)
   {
   sprintf(ausgabe,"%1d",tag % 10);
   return (tag % 10);
   }
/****** tages_datum *****/
if (par == 0)
   {
   sprintf(ausgabe,"%02d.%02d.%4d",tag,mon,jahr);
   return (0);
   }
if (par == 1)
   {
   sprintf(ausgabe,"%02d.%02d.%2d",tag,mon,jahr%100);
   return (0);
   }

for(i=0;;i++)
   {
   if(arb < 3600)
        break;
   arb = arb - 3600;
   }
std = i;
for(i=0;;i++)
   {
   if(arb < 60)
        break;
   arb = arb - 60;
   }
min = i;
sek = (int)arb;
if(par == 2)
   {
   sprintf(ausgabe,"%02d:%02d:%02d",std,min,sek);
   return (0);
   }
if(par == 3)
   {
   sprintf(ausgabe,"%02d:%02d",std,min);
   return (0);
   }
if(par == 5)
   {
   sprintf(ausgabe,"%02d%02d%02d",std,min,sek);
   return (0);
   }
return (-1);
} /* end of teges_datum_zeit() */

long first_kw_day (short kw, short jr)
/**
1.Tag von Kalenderwoche holen.
**/
{
     char datum [11];
     long date;
     long daplus;
     short wo;


     if (kw == 1)
     {
               sprintf (datum, "31.12.%hd", jr - 1);
               date = dasc_to_long (datum);// + 1;
     }
     else
     {
               sprintf (datum, "01.01.%hd", jr);
               date = dasc_to_long (datum);
               daplus = (long) (kw - 1) * 7;
               date = date + daplus - (long) 10;
     }
     dlong_to_asc (date, datum); 
     wo = get_woche (datum); 
     while (wo != kw)
     {
               date ++;
               dlong_to_asc (date, datum); 
               wo = get_woche (datum); 
     }
     return (date);
}
	 
long last_kw_day (short kw, short jr)
/**
letzten.Tag von Kalenderwoche holen.
**/
{
     long date;

     date = first_kw_day (kw, jr) + 6;
     return (date);
}

int first_kw_dayc (short kw, short jr, char *datum)
/**
1.Tag von Kalenderwoche holen. Datum als String
**/
{
     long date;

     date = first_kw_day (kw, jr);
     dlong_to_asc (date, datum);
     return (0);
}
        
int last_kw_dayc (short kw, short jr, char *datum)
/**
1.Tag von Kalenderwoche holen. Datum als String
**/
{
     long date;

     date = last_kw_day (kw, jr);
     dlong_to_asc (date, datum);
     return (0);
}
        

	/*****************Ende**********************/
