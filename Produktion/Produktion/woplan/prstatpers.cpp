#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "wmaskc.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "prstatpers.h"

struct PRSTATPERS prstatpers, prstatpers_null;

void PRSTATPERS_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *)   &prstatpers.mdn, 1, 0);
            ins_quest ((char *)   &prstatpers.dat,  2, 0);
            ins_quest ((char *)   &prstatpers.masch_nr,  2, 0);
            ins_quest ((char *)   &prstatpers.prodabt, 1, 0);
            ins_quest ((char *)   prstatpers.pers,  0, 13);
    out_quest ((char *) &prstatpers.mdn,1,0);
    out_quest ((char *) &prstatpers.dat,2,0);
    out_quest ((char *) &prstatpers.masch_nr,2,0);
    out_quest ((char *) &prstatpers.prodabt,1,0);
    out_quest ((char *) prstatpers.pers,0,13);
    out_quest ((char *) &prstatpers.zeit_min,2,0);
    out_quest ((char *) &prstatpers.status,1,0);
    out_quest ((char *) &prstatpers.menge,3,0);
            cursor = prepare_sql ("select prstatpers.mdn,  "
"prstatpers.dat,  prstatpers.masch_nr,  prstatpers.prodabt,  "
"prstatpers.pers,  prstatpers.zeit_min,  prstatpers.status,  "
"prstatpers.menge from prstatpers "

#line 24 "prstatpers.rpp"
                                  "where mdn = ? "
                                  "and   dat = ? "
                                  "and   masch_nr = ? "
                                  "and prodabt = ? "
                                   "and pers = ?" );
    ins_quest ((char *) &prstatpers.mdn,1,0);
    ins_quest ((char *) &prstatpers.dat,2,0);
    ins_quest ((char *) &prstatpers.masch_nr,2,0);
    ins_quest ((char *) &prstatpers.prodabt,1,0);
    ins_quest ((char *) prstatpers.pers,0,13);
    ins_quest ((char *) &prstatpers.zeit_min,2,0);
    ins_quest ((char *) &prstatpers.status,1,0);
    ins_quest ((char *) &prstatpers.menge,3,0);
            sqltext = "update prstatpers set "
"prstatpers.mdn = ?,  prstatpers.dat = ?,  prstatpers.masch_nr = ?,  "
"prstatpers.prodabt = ?,  prstatpers.pers = ?,  "
"prstatpers.zeit_min = ?,  prstatpers.status = ?,  "
"prstatpers.menge = ? "

#line 30 "prstatpers.rpp"
                                  "where mdn = ? "
                                  "and   dat = ? "
                                  "and   masch_nr = ? "
                                  "and prodabt = ? "
                                   "and pers = ?" ;
            ins_quest ((char *)   &prstatpers.mdn, 1, 0);
            ins_quest ((char *)   &prstatpers.dat,  2, 0);
            ins_quest ((char *)   &prstatpers.masch_nr,  2, 0);
            ins_quest ((char *)   &prstatpers.prodabt, 1, 0);
            ins_quest ((char *)   prstatpers.pers,  0, 13);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *)   &prstatpers.mdn, 1, 0);
            ins_quest ((char *)   &prstatpers.dat,  2, 0);
            ins_quest ((char *)   &prstatpers.masch_nr,  2, 0);
            ins_quest ((char *)   &prstatpers.prodabt, 1, 0);
            ins_quest ((char *)   prstatpers.pers,  0, 13);
            test_upd_cursor = prepare_sql ("select masch_nr from prstatpers "
                                  "where mdn = ? "
                                  "and   dat = ? "
                                  "and   masch_nr = ? "
                                  "and prodabt = ? "
                                   "and pers = ? "
                                  "for update");

            ins_quest ((char *)   &prstatpers.mdn, 1, 0);
            ins_quest ((char *)   &prstatpers.dat,  2, 0);
            ins_quest ((char *)   &prstatpers.masch_nr,  2, 0);
            ins_quest ((char *)   &prstatpers.prodabt, 1, 0);
            ins_quest ((char *)   prstatpers.pers,  0, 13);
            del_cursor = prepare_sql ("delete from prstatpers "
                                  "where mdn = ? "
                                  "and   dat = ? "
                                  "and   masch_nr = ? "
                                  "and prodabt = ? "
                                   "and pers = ?" );
    ins_quest ((char *) &prstatpers.mdn,1,0);
    ins_quest ((char *) &prstatpers.dat,2,0);
    ins_quest ((char *) &prstatpers.masch_nr,2,0);
    ins_quest ((char *) &prstatpers.prodabt,1,0);
    ins_quest ((char *) prstatpers.pers,0,13);
    ins_quest ((char *) &prstatpers.zeit_min,2,0);
    ins_quest ((char *) &prstatpers.status,1,0);
    ins_quest ((char *) &prstatpers.menge,3,0);
            ins_cursor = prepare_sql ("insert into prstatpers ("
"mdn,  dat,  masch_nr,  prodabt,  pers,  zeit_min,  status,  menge) "

#line 67 "prstatpers.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?)");

#line 69 "prstatpers.rpp"
}
