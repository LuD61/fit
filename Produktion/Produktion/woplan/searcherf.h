#ifndef _SEARCHERF_DEF
#define _SEARCHERF_DEF
#include "wmaskc.h"
#include "cmask.h"
#include "mo_chqex.h"
#include "dbclass.h"

#define ERF 8

struct SERF
{
      char a [15];
	  char mat [10];
	  char a_bz1 [26];
};

class SEARCHERF
{
       private :

           static ITEM ua;
           static ITEM umat;
           static ITEM ua_bz1;

           static ITEM ia;
           static ITEM imat;
           static ITEM ia_bz1;


           static ITEM iline;

           static field _UbForm[];
           static form UbForm;

           static field _DataForm[];
           static form DataForm;

           static field _LineForm[];
           static form LineForm;

           static HINSTANCE hMainInst;
           static HWND hMainWindow;
           static DB_CLASS DbClass; 
           static HWND awin;
           static struct SERF *serftab;
           static struct SERF serf;
           static int idx;
           static long erfanz;
           static CHQEX *Query;
           static int SearchField;
           static char *query;
           static int soa; 
           static int somat; 
           static int soa_bz1; 
           static short mdn;

           int SearchPos;
           int OKPos;
           int CAPos;
           DWORD WindowStyle;
           BOOL SetStyle;
		   char ProdAbt [25];
		   short abt;
        public :

           SEARCHERF ()
           {
                  SearchPos = 8;
                  OKPos = 9;
                  CAPos = 10;
                  SetStyle = FALSE;
                  mdn = 0;
                  WindowStyle = WS_POPUP | WS_THICKFRAME;
           }

           ~SEARCHERF ()
           {
                  if (serftab != NULL)
                  {
                      delete serftab;
                      serftab = NULL;
                  }
                  if (Query != NULL)
                  {
                      delete Query;
                      Query = NULL;
                  }
           }

           void SetQuery (char *q)
           {
               if (query == NULL || (strcmp (query, q) != 0))
               {
                      query = q;
                      if (Query != NULL)
                      {
                             delete Query;
                      }
                      if (serftab != NULL)
                      {
                             delete serftab;
                      }
               }   
           }

           void SetAbt (short a, char *PrdAbt)
		   {
				abt = a;
				strcpy(ProdAbt,PrdAbt);
		   }

           char *GetQuery (void)
           {
                return query;
           }

           SERF *GetSerf (void)
           {
               if (idx == -1) return NULL;
               return &serf;
           }

           void Setawin (HWND awin)
           {
               this->awin = awin;
           }

           void SetWindowStyle (DWORD style)
           {
               WindowStyle = style;
               SetStyle = TRUE;
           }


           void FillBox (void);
           static void FillFormat (char *, int);
           static void FillVlines (char *, int);
           static void FillCaption (char *, int);
           static void FillRec (char *, int, int);
           static int sortmat (const void *, const void *);
           static void SortMat (HWND);
           static int sorta (const void *, const void *);
           static void SortA (HWND);
           static int sorta_bz1 (const void *, const void *);
           static void SortABz1 (HWND);
           static void SortLst (int, HWND);
           static void UpdateList (void);

           static int SearchLst (char *);
           static int Read (char *);
           void SetParams (HINSTANCE, HWND);
           void Search (void);
};  
#endif