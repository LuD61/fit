//100905 me_old 
//010905
//310805
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include <string.h>
#include "comcthlp.h"
#include "itemc.h"
#include "wmaskc.h"
#include "mo_meld.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_intp.h"
#include "listdlg.h"
#include "inflib.h"
#include "spezdlg.h"
#include "enterfunc.h"
#include "ListDlg.h"
#include "dlg.h"
#include "mo_wmess.h"
#include "gprintl.h"
#include "mo_gdruck.h"
#include "datum.h"
#include "spezdlg.h"
#include <sqlproto.h>

#define IDM_CANCEL 3001
#ifndef IDM_WRITE
#define IDM_WRITE  3002
#endif


static int StdSize = STDSIZE;

static int dlgsize    = STDSIZE;
static int ltgraysize = STDSIZE;
static int dlgpossize = STDSIZE;
static int cpsize     = 200;
extern int SperreAktiv ;
extern int ohneSamstag ;

static int row;
static char *pline;
static unsigned char ESC = 27;
static unsigned char NORM   = 70;
static unsigned char FETT1  = 80;
static unsigned char FETT2  = 81;
static unsigned char ENG  = 0;
static unsigned int FETT = 0;


short SperreGesetzt;
double AktMe;

char *LISTDLG::PrintCommand = "rezaufloesung";

/*
         int Datumsperre [] = {FALSE,
                                  FALSE,
                                  FALSE,
                                  FALSE,
                                  FALSE,
                                  FALSE,
                                  FALSE,
                                  FALSE,
                                  FALSE,
                                   FALSE,
                                   FALSE,
                                   FALSE,
        };
		*/


static mfont dlgfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         GRAYCOL,
                         0,
                         NULL};

static ColButton DefCol = {NULL,    -1, 0, 
                           NULL,    -1, 1,
                           NULL,  0,  0,
                           NULL,  0,  0,
                           NULL,  0,  0,
                           BLACKCOL,
                           LTGRAYCOL,
                           2,
                           NULL,
                           NULL,
                           TRUE,
};

static mfont printfont   = {"Arial", 80, 0, 0, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};

static mfont printfettfont   = {"Arial", 100, 0, 1, RGB (255, 0, 0),
                                         RGB (0, 255, 255),
                                         1,
                                         NULL};

char *LISTDLG::Rec;
int LISTDLG::RecLen;
CFELD **LISTDLG::Lsts;
char *LISTDLG::LstTab[0x10000];
Work *LISTDLG::work = NULL;
BOOL LISTDLG::NoRecNrOK = FALSE;


CFIELD *LISTDLG::_fChoise0 [] = {
    new CFIELD ("cancel", " abbrechen ", 10, 0, 0, 0, NULL, "",
                CBUTTON, 
                IDM_CANCEL, &dlgfont, 0, 0),
    new CFIELD ("write", " speichern ", 10, 0,12, 0, NULL, "",
                CBUTTON, 
                IDM_WRITE, &dlgfont, 0, 0),
    new CFIELD ("show",  "  ansehen  ", 10, 0,24, 0, NULL, "",
                CBUTTON, 
                IDM_CHOISE, &dlgfont, 0, 0),
    new CFIELD ("delete","  l�schen  ", 10, 0,36, 0, NULL, "",
                CBUTTON, 
                IDM_DELETE, &dlgfont, 0, 0),
    new CFIELD ("ins"," einf�gen  ", 10, 0,48, 0, NULL, "",
                CBUTTON, 
                IDM_INSERT, &dlgfont, 0, 0),
    NULL,
};

CFORM LISTDLG::fChoise0 (5, _fChoise0);

CFIELD *LISTDLG::_fChoise [] = {
    new CFIELD ("fChoise0", (char *) &fChoise0, 0, 0, -1 , -2, NULL, "",
                CFFORM, 
                500, &dlgfont, 0, 0),
    NULL,
};

CFORM LISTDLG::fChoise (1, _fChoise);


ITEM **LISTDLG::ufelder;
ITEM LISTDLG::ufiller         ("",               " ",               "",0);

ITEM LISTDLG::iline ("", "1", "", 0);

field *LISTDLG::_UbForm;


form LISTDLG::UbForm = {6, 0, 0, _UbForm, 0, 0, 0, 0, NULL};

field *LISTDLG::_LineForm;

form LISTDLG::LineForm = {4, 0, 0, _LineForm,0, 0, 0, 0, NULL};

field *LISTDLG::_DataForm;

form LISTDLG::DataForm = {5, 0, 0, _DataForm,0, 0, 0, 0, NULL};


int *LISTDLG::ubrows;


struct CHATTR LISTDLG::ChAttra [] = {"feld1a",     DISPLAYONLY, EDIT,
                                      NULL,  0,           0};

struct CHATTR *LISTDLG::ChAttr = ChAttra;

BOOL LISTDLG::ScrPrint = TRUE;
short LISTDLG::kw;
short LISTDLG::jr;
int LISTDLG::GewLen = 15;

BOOL LISTDLG::CallInfo (HWND hWnd, RECT *rect, char *Item, char *ItemValue, DWORD WinMode)
/**
Einstieg in Infosystem mit Artikel. Abgeleitet von Klasse Info.
**/
{
 		 char buffer [1024];
         BOOL ret;

         sprintf (buffer, "where a = 2");
         ret = Info::CallInfo (hWnd, rect, "a", buffer, WinMode); 
         SetListFocus ();
         return ret;
}


int LISTDLG::InfoProc (char **Item, char *Value, char *where)
/**
Spezielle Procedure fuer Info-System. Alter Ablauf, ist nicht mehr Aktiv 
**/
{

   *Item = "a";

   sprintf (where, "where a = 2");
   return 1;
}

void LISTDLG::SetButtonSize (int Size)
{
    int fpos;

    return;
    fpos = GetItemPos (&DataForm, "choise");
    if (fpos < 0) return;

    if (Size == SMALL)
    {
         DataForm.mask [fpos].length = 2;
         DataForm.mask [fpos].rows = 1;
    }
    else 
    {
         DataForm.mask [fpos].length = 3;
         DataForm.mask [fpos].rows = 0;
    }
}


void LISTDLG::GetCfgValues (void)
{
       char cfg_v [512];
       COLORREF Color;

       if (ProgCfg == NULL) return;
	   if (cfgOK) return;

       cfgOK = TRUE;
       if (ProgCfg->GetCfgValue ("3DList", cfg_v) == TRUE)
       {
                    eListe.Set3D (atoi (cfg_v));
	   }
       if (ProgCfg->GetCfgValue ("3DSize", cfg_v) == TRUE)
       {
                    eListe.SetPlus3D (atoi (cfg_v));
	   }
       if (ProgCfg->GetCfgValue ("ListFocus", cfg_v) == TRUE)
       {
                    ListFocus = atoi (cfg_v);
	   }
        if (ProgCfg->GetCfgValue ("rowheight", cfg_v) == TRUE)
		{
			        RowHeight = ratod (cfg_v);
					if (RowHeight < (double) 1.0)
					{
						RowHeight = (double) 1.0;
					}
		}
        if (ProgCfg->GetCfgValue ("ubheight", cfg_v) == TRUE)
		{
			        UbHeight = atoi (cfg_v);
					SetUbHeight ();
		}
		eListe.SetRowHeight (RowHeight);


        if (ProgCfg->GetCfgValue ("NoRecNr", cfg_v) == TRUE)
        {
			        NoRecNr = atoi (cfg_v);
		            if (atoi (cfg_v))
					{
	                        SetNoRecNr ();
					}
		}
        if (ProgCfg->GetCfgValue ("ListColor", cfg_v) == TRUE)
        {
                     GetListColor (&Color, cfg_v);
		             eListe.SetColor (Color);
        }
        if (ProgCfg->GetCfgValue ("ListBkColor", cfg_v) == TRUE)
        {
                     GetListColor (&Color, cfg_v);
		             eListe.SetBkColor (Color);
        }
        if (ProgCfg->GetCfgValue ("ListLine", cfg_v) == TRUE)
        {
		             eListe.SetLines (GetListLine (cfg_v));
        }
        if (ProgCfg->GetCfgValue ("PrintBackground", cfg_v) == TRUE)
        {
		             PrintBackground = atoi (cfg_v);
        }
}


void LISTDLG::TestRemoves (void)
{
        int fpos;

        fpos = GetItemPos (&DataForm, "feld3");
        if (fpos >= 0)
        {
            if (DataForm.mask[fpos].attribut & REMOVED)
            {
                DestroyListField ("feld3");
            }
        }
}



/*
void LISTDLG::GenWoForms (short abt)
{
        char iname [80];
        long lday;
        char day [12];
        char datum [12];
        static char *KwDays [] = {"",
                                  "Montag",
                                  "Dienstag",
                                  "Mittwoch",
                                  "Donnerstag",
                                  "Freitag",
                                  "Samstag",
                                  "Sonntag",
                                   NULL,
                                   NULL,
                                   NULL,
        };

        sysdate (datum);
        lday = first_kw_day ((short) kw, (short) jr);
        StartDay = lday;
 	    DefCol.BkColor = GetSysColor (COLOR_3DFACE);
        eListe.SetNoScrollBkColor (DefCol.BkColor);
        work->SetAbt (abt);
        UbForm.fieldanz = 9;
        _UbForm = new field [UbForm.fieldanz];
        DLG::ExitError ((_UbForm == NULL), "Fehler bei der Speicherzuordnung");
        _LineForm = new field [UbForm.fieldanz];
        DLG::ExitError ((_LineForm == NULL), "Fehler bei der Speicherzuordnung");

        UbForm.mask = _UbForm;
        UbForm.frmstart  = 0;
        UbForm.frmscroll = 0;
        UbForm.caption   = NULL;
        UbForm.before    = NULL;
        UbForm.after     = NULL;
        UbForm.cbfield   = NULL;
        UbForm.font      = NULL;

        memcpy (&LineForm, &UbForm, sizeof (form));
        LineForm.mask    = _LineForm;

        int i = 0;
        int pos = 6;

        sprintf (iname, "a");
        char *ibez = new char [80]; 
        DLG::ExitError ((ibez == NULL), "Fehler bei der Speicherzuordnung");
        ColButton *Cub = new ColButton;
        DLG::ExitError ((Cub == NULL), "Fehler bei der Speicherzuordnung");
        memcpy (Cub, &DefCol, sizeof (ColButton));
        sprintf (ibez, "Artikel");
        Cub->text1 = ibez;
        Cub->ty1 = -1;

// Form �berschrift definieren

        _UbForm[i].item = new ITEM (iname, (char *) Cub, "", NULL);
        _UbForm[i].length    = 20;
        _UbForm[i].rows      = 0;
        _UbForm[i].pos[0]    = 0;
        _UbForm[i].pos[1]    = pos;
        _UbForm[i].feldid    = NULL;
        _UbForm[i].picture   = "";
        _UbForm[i].attribut  = COLBUTTON;
        _UbForm[i].before    = NULL;
        _UbForm[i].after     = NULL;
        _UbForm[i].BuId      = 0;

// �berschrift f�r Artikelspalte

        pos += _UbForm[i].length;
        memcpy (&_LineForm[i], &_UbForm[0], sizeof (field));
        _LineForm[i].item = &iline;
        _LineForm[i].length    = 1;
        _LineForm[i].pos[1]    = pos;
        _LineForm[i].attribut  = DISPLAYONLY;
        i ++;

// �berschrift f�r Wochentage

        for (; i < 8; i ++)
        {
            if (i >= UbForm.fieldanz - 1) break;
            dlong_to_asc (lday, day);

            ibez = new char [80]; 
            DLG::ExitError ((ibez == NULL), "Fehler bei der Speicherzuordnung");
            sprintf (ibez, "%s", day);
            ColButton *Cub = new ColButton;
            DLG::ExitError ((Cub == NULL), "Fehler bei der Speicherzuordnung");
            memcpy (Cub, &DefCol, sizeof (ColButton));
            Cub->text1 = ibez;
            Cub->text2 = new char [25];
            DLG::ExitError ((Cub->text2 == NULL), "Fehler bei der Speicherzuordnung");
            strcpy (Cub->text2, KwDays[i]);

            memcpy (&_UbForm[i], &_UbForm[0], sizeof (field));
            _UbForm[i].item = new ITEM (iname, (char *) Cub, "", NULL);
            _UbForm[i].length    = GewLen;
            _UbForm[i].pos[1]    = pos;
            pos += _UbForm[i].length;

            memcpy (&_LineForm[i], &_UbForm[0], sizeof (field));
            _LineForm[i].item = &iline;
            _LineForm[i].length    = 1;
            _LineForm[i].pos[1]    = pos;
            _LineForm[i].attribut  = DISPLAYONLY;
            lday ++;
        }

// F�llbutton f�r �berschrifr am Zeilenende

        _UbForm[i].item = &ufiller;
        _UbForm[i].length    = 100;
        _UbForm[i].rows      = 0;
        _UbForm[i].pos[0]    = 0;
        _UbForm[i].pos[1]    = pos;
        _UbForm[i].feldid    = NULL;
        _UbForm[i].picture   = "";
        _UbForm[i].attribut  = BUTTON;
        _UbForm[i].before    = NULL;
        _UbForm[i].after     = NULL;
        _UbForm[i].BuId      = 0;

        DataForm.fieldanz = UbForm.fieldanz;
        _DataForm = new field [UbForm.fieldanz + 1];
        DLG::ExitError ((_DataForm == NULL), "Fehler bei der Speicherzuordnung");
        DataForm.mask = _DataForm;
        Lsts = new CFELD * [DataForm.fieldanz + 8];
        DLG::ExitError ((Lsts == NULL), "Fehler bei der Speicherzuordnung");

        RecLen = 19 + 13 * DataForm.fieldanz + 
                 20 +                         // a_bz1
                 8 +                          // Variante 
                 12 +                         // Rezeptur
				 8 +						  // posi 020905	
                 14 +                         // chg_gew
                 14 +                         // mat_o_b
                 14 +                         // hk_vollk
                 14 +                         // hk_teilk
                  8 +                         // planstatus
                 10;                          // dummy

        Rec = new char [RecLen];
        DLG::ExitError ((Rec == NULL), "Fehler bei der Speicherzuordnung");
        memset (Rec, ' ', RecLen);

        char *Rpos = Rec;

        pos = 7;
        i = 0;

// Artikelspalte f�r Daten 

        strcpy (iname, "a");
        Lsts [i] = new CFELD (iname, Rpos, 19);
        DLG::ExitError ((Lsts[i] == NULL), "Fehler bei der Speicherzuordnung");
        Lsts[i]->Init ();
        memcpy (&_DataForm[i], &_UbForm[0], sizeof (field));

        _DataForm[i].item = new ITEM (iname, Lsts[i]->GetFeld (), "", NULL);
        _DataForm[i].length    = 18;
        _DataForm[i].pos[1]    = pos + 1;
        _DataForm[i].picture   = "%13.0f";
        _DataForm[i].attribut    = DISPLAYONLY;
        pos  += 20;
        Rpos += _DataForm[i].length;

// Datenzeile f�r Mengeneingabe definieren
        for (i = 1; i < DataForm.fieldanz - 1; i ++)
        {
                 sprintf (iname, "me%d", i);

                 Lsts [i] = new CFELD (iname, Rpos, 12);
                 DLG::ExitError ((Lsts[i] == NULL), "Fehler bei der Speicherzuordnung");
                 Lsts[i]->Init ();
                 memcpy (&_DataForm[i], &_UbForm[0], sizeof (field));

                 _DataForm[i].item = new ITEM (iname, Lsts[i]->GetFeld (), "", NULL);
                 _DataForm[i].length    = 12;
                 _DataForm[i].pos[1]    = pos + 1;
                 _DataForm[i].picture   = "%9.3fn";
                 _DataForm[i].attribut  = EDIT;
                 pos += GewLen;
                 Rpos += 13;

                 i ++;
                 sprintf (iname, "planstatus%d", i);
                 Lsts [i] = new CFELD (iname, Rpos, 6);
                 DLG::ExitError ((Lsts[i] == NULL), "Fehler bei der Speicherzuordnung");
                 Lsts[i]->Init ();
                 Rpos += 8;
        }



        strcpy (iname, "a_bz1");
        Lsts [i] = new CFELD (iname, Rpos, 19);
        DLG::ExitError ((Lsts[i] == NULL), "Fehler bei der Speicherzuordnung");
        Lsts[i]->Init ();
        memcpy (&_DataForm[i], &_UbForm[0], sizeof (field));

        _DataForm[i].item = new ITEM (iname, Lsts[i]->GetFeld (), "", NULL);
        _DataForm[i].length      = 18;
        _DataForm[i].pos[0]      = 1;
        _DataForm[i].pos[1]      = 7;
        _DataForm[i].attribut    = DISPLAYONLY;

// Speicher f�r Variante

        Rpos += 20;
        i ++;
        strcpy (iname, "variante");
        Lsts [i] = new CFELD (iname, Rpos, 6);
        DLG::ExitError ((Lsts[i] == NULL), "Fehler bei der Speicherzuordnung");
        Lsts[i]->Init ();

// Speicher f�r Rezepturnummer

        Rpos += 8;
        i ++;
        strcpy (iname, "rez");
        Lsts [i] = new CFELD (iname, Rpos, 10);
        DLG::ExitError ((Lsts[i] == NULL), "Fehler bei der Speicherzuordnung");
        Lsts[i]->Init ();

// Speicher f�r posi 020905

        Rpos += 12;
        i ++;
        strcpy (iname, "posi");
        Lsts [i] = new CFELD (iname, Rpos, 10);
        DLG::ExitError ((Lsts[i] == NULL), "Fehler bei der Speicherzuordnung");
        Lsts[i]->Init ();


// Speicher f�r chg_gew

        Rpos += 8;
        i ++;
        strcpy (iname, "chg_gew");
        Lsts [i] = new CFELD (iname, Rpos, 12);
        DLG::ExitError ((Lsts[i] == NULL), "Fehler bei der Speicherzuordnung");
        Lsts[i]->Init ();

// Speicher f�r mat_o_b

        Rpos += 14;
        i ++;
        strcpy (iname, "mat_o_b");
        Lsts [i] = new CFELD (iname, Rpos, 12);
        DLG::ExitError ((Lsts[i] == NULL), "Fehler bei der Speicherzuordnung");
        Lsts[i]->Init ();

// Speicher f�r hk_vollk

        Rpos += 14;
        i ++;
        strcpy (iname, "hk_vollk");
        Lsts [i] = new CFELD (iname, Rpos, 12);
        DLG::ExitError ((Lsts[i] == NULL), "Fehler bei der Speicherzuordnung");
        Lsts[i]->Init ();

// Speicher f�r hk_teilk

        Rpos += 14;
        i ++;
        strcpy (iname, "hk_teilk");
        Lsts [i] = new CFELD (iname, Rpos, 12);
        DLG::ExitError ((Lsts[i] == NULL), "Fehler bei der Speicherzuordnung");
        Lsts[i]->Init ();

        Lsts[i + 1] = NULL;

        ubrows = new int [DataForm.fieldanz + 1];
        DLG::ExitError ((ubrows == NULL), "Fehler bei der Speicherzuordnung");
        for (i = 0; i < DataForm.fieldanz; i ++)
        {
            ubrows[i] = i;
        }
        ubrows[i] = 0;
}

*/

void LISTDLG::GenWoFormsOne (short abt)
{
        char iname [80];
        long lday;
        long ldatum;
        char day [12];
        char datum [12];
        static char *KwDays [] = {"",
                                  "",
                                  "Montag",
                                  "Dienstag",
                                  "Mittwoch",
                                  "Donnerstag",
                                  "Freitag",
                                  "Samstag",
                                  "Sonntag",
                                   NULL,
                                   NULL,
        };
        static char *KwDays1 [] = {"",
                                  "",
                                  "((Montag))",
                                  "((Dienstag))",
                                  "((Mittwoch))",
                                  "((Donnerstag))",
                                  "((Freitag))",
                                  "((Samstag))",
                                  "((Sonntag))",
                                   NULL,
                                   NULL,
        };

        sysdate (datum);
        ldatum = dasc_to_long (datum) + 1;
        lday = first_kw_day ((short) kw, (short) jr);
        StartDay = lday;
 	    DefCol.BkColor = GetSysColor (COLOR_3DFACE);
        eListe.SetNoScrollBkColor (DefCol.BkColor);
        work->SetAbt (abt);
        UbForm.fieldanz = 9;
        _UbForm = new field [UbForm.fieldanz];
        DLG::ExitError ((_UbForm == NULL), "Fehler bei der Speicherzuordnung");
        _LineForm = new field [UbForm.fieldanz];
        DLG::ExitError ((_LineForm == NULL), "Fehler bei der Speicherzuordnung");

        UbForm.mask = _UbForm;
        UbForm.frmstart  = 0;
        UbForm.frmscroll = 0;
        UbForm.caption   = NULL;
        UbForm.before    = NULL;
        UbForm.after     = NULL;
        UbForm.cbfield   = NULL;
        UbForm.font      = NULL;

        memcpy (&LineForm, &UbForm, sizeof (form));
        LineForm.mask    = _LineForm;

        int i = 0;
        int pos = 6;

        sprintf (iname, "a");
        char *ibez = new char [80]; 
        DLG::ExitError ((ibez == NULL), "Fehler bei der Speicherzuordnung");
        ColButton *Cub = new ColButton;
        DLG::ExitError ((Cub == NULL), "Fehler bei der Speicherzuordnung");
        memcpy (Cub, &DefCol, sizeof (ColButton));
        sprintf (ibez, "Artikel");
        Cub->text1 = ibez;
        Cub->ty1 = -1;

// Form �berschrift definieren

        _UbForm[i].item = new ITEM (iname, (char *) Cub, "", NULL);
        _UbForm[i].length    = 16;
        _UbForm[i].rows      = 0;
        _UbForm[i].pos[0]    = 0;
        _UbForm[i].pos[1]    = pos;
        _UbForm[i].feldid    = NULL;
        _UbForm[i].picture   = "";
        _UbForm[i].attribut  = COLBUTTON;
        _UbForm[i].before    = NULL;
        _UbForm[i].after     = NULL;
        _UbForm[i].BuId      = 0;

// �berschrift f�r Artikelspalte


        pos += _UbForm[i].length;
        memcpy (&_LineForm[i], &_UbForm[0], sizeof (field));
        _LineForm[i].item = &iline;
        _LineForm[i].length    = 1;
        _LineForm[i].pos[1]    = pos;
        _LineForm[i].attribut  = DISPLAYONLY;
        i ++;


// �berschrift f�r Bezeichnung

        sprintf (iname, "a_bz1");
        ibez = new char [80]; 
        DLG::ExitError ((ibez == NULL), "Fehler bei der Speicherzuordnung");
        Cub = new ColButton;
        DLG::ExitError ((Cub == NULL), "Fehler bei der Speicherzuordnung");
        memcpy (Cub, &DefCol, sizeof (ColButton));
        sprintf (ibez, "Bezeichnung");
        Cub->text1 = ibez;
        Cub->ty1 = -1;

        _UbForm[i].item = new ITEM (iname, (char *) Cub, "", NULL);
        _UbForm[i].length    = 25;
        _UbForm[i].rows      = 0;
        _UbForm[i].pos[0]    = 0;
        _UbForm[i].pos[1]    = pos;
        _UbForm[i].feldid    = NULL;
        _UbForm[i].picture   = "";
        _UbForm[i].attribut  = COLBUTTON;
        _UbForm[i].before    = NULL;
        _UbForm[i].after     = NULL;
        _UbForm[i].BuId      = 0;

        pos += _UbForm[i].length;
        memcpy (&_LineForm[i], &_UbForm[0], sizeof (field));
        _LineForm[i].item = &iline;
        _LineForm[i].length    = 1;
        _LineForm[i].pos[1]    = pos;
        _LineForm[i].attribut  = DISPLAYONLY;
        i ++;

// �berschrift f�r Averkaufsmengen

/***** 300709
        sprintf (iname, "mengen");
        ibez = new char [80]; 
        DLG::ExitError ((ibez == NULL), "Fehler bei der Speicherzuordnung");
        Cub = new ColButton;
        DLG::ExitError ((Cub == NULL), "Fehler bei der Speicherzuordnung");
        memcpy (Cub, &DefCol, sizeof (ColButton));
        sprintf (ibez, "Liefermengen");
		strcpy (datvon,work.datvon);
		strcpy (datbis,work.datbis);
		rfmtdate (dasc_to_long(datvon),"dd.mm",datvon); 
		rfmtdate (dasc_to_long(datbis),"dd.mm",datbis); 
		sprintf(cmedat,"%s-%s",clipped(datvon),clipped(datbis));
		Cub->text1 = ibez;
        Cub->text2 = new char [25];
        DLG::ExitError ((Cub->text2 == NULL), "Fehler bei der Speicherzuordnung");
        strcpy (Cub->text2, cmedat);
//        Cub->ty1 = -1;

        _UbForm[i].item = new ITEM (iname, (char *) Cub, "", NULL);
        _UbForm[i].length    = 15;
        _UbForm[i].rows      = 0;
        _UbForm[i].pos[0]    = 0;
        _UbForm[i].pos[1]    = pos;
        _UbForm[i].feldid    = NULL;
        _UbForm[i].picture   = "";
        _UbForm[i].attribut  = COLBUTTON;
        _UbForm[i].before    = NULL;
        _UbForm[i].after     = NULL;
        _UbForm[i].BuId      = 0;

        pos += _UbForm[i].length;
        memcpy (&_LineForm[i], &_UbForm[0], sizeof (field));
        _LineForm[i].item = &iline;
        _LineForm[i].length    = 1;
        _LineForm[i].pos[1]    = pos;
        _LineForm[i].attribut  = DISPLAYONLY;
        i ++;

  ********/
// �berschrift f�r Wochentage

        for (; i < UbForm.fieldanz - 1; i ++)
        {
            dlong_to_asc (lday, day);

            ibez = new char [80]; 
            DLG::ExitError ((ibez == NULL), "Fehler bei der Speicherzuordnung");
            sprintf (ibez, "%s", day);
            ColButton *Cub = new ColButton;
            DLG::ExitError ((Cub == NULL), "Fehler bei der Speicherzuordnung");
            memcpy (Cub, &DefCol, sizeof (ColButton));
            Cub->text1 = ibez;
            Cub->text2 = new char [25];
            DLG::ExitError ((Cub->text2 == NULL), "Fehler bei der Speicherzuordnung");
            strcpy (Cub->text2, KwDays[i]);
			if (((SpezDlg *) Dlg)->GetCheckMo() == 1)
			{
				if (strcmp ("Montag", KwDays[i]) == 0) Work::Datumsperre[i] = FALSE;
			}
			else
			{
				if (strcmp ("Montag", KwDays[i]) == 0) Work::Datumsperre[i] = TRUE;
			}

			if (((SpezDlg *) Dlg)->GetCheckDi() == 1)
			{
				if (strcmp ("Dienstag", KwDays[i]) == 0) Work::Datumsperre[i] = FALSE;
			}
			else
			{
				if (strcmp ("Dienstag", KwDays[i]) == 0) Work::Datumsperre[i] = TRUE;
			}

			if (((SpezDlg *) Dlg)->GetCheckMi() == 1)
			{
				if (strcmp ("Mittwoch", KwDays[i]) == 0) Work::Datumsperre[i] = FALSE;
			}
			else
			{
				if (strcmp ("Mittwoch", KwDays[i]) == 0) Work::Datumsperre[i] = TRUE;
			}

			if (((SpezDlg *) Dlg)->GetCheckDo() == 1)
			{
				if (strcmp ("Donnerstag", KwDays[i]) == 0) Work::Datumsperre[i] = FALSE;
			}
			else
			{
				if (strcmp ("Donnerstag", KwDays[i]) == 0) Work::Datumsperre[i] = TRUE;
			}

			if (((SpezDlg *) Dlg)->GetCheckFr() == 1)
			{
				if (strcmp ("Freitag", KwDays[i]) == 0) Work::Datumsperre[i] = FALSE;
			}
			else
			{
				if (strcmp ("Freitag", KwDays[i]) == 0) Work::Datumsperre[i] = TRUE;
			}

			if (((SpezDlg *) Dlg)->GetCheckSa() == 1)
			{
				if (strcmp ("Samstag", KwDays[i]) == 0) Work::Datumsperre[i] = FALSE;
			}
			else
			{
				if (strcmp ("Samstag", KwDays[i]) == 0) Work::Datumsperre[i] = TRUE;
			}


			if (ldatum  > lday + 1  && SperreAktiv == 1)
            {
				Work::Datumsperre[i] = TRUE;  //020204
                strcpy (Cub->text2, KwDays1[i]);
			}
		    else
            {
//				Datumsperre[i] = FALSE;  //020204
	            strcpy (Cub->text2, KwDays[i]);
			}
			if (ohneSamstag == 1)
			{
				if (strcmp ("Samstag", KwDays[i]) == 0) Work::Datumsperre[i] = TRUE; //050705
			}
            memcpy (&_UbForm[i], &_UbForm[0], sizeof (field));
            _UbForm[i].item = new ITEM (iname, (char *) Cub, "", NULL);
            _UbForm[i].length    = GewLen;
            _UbForm[i].pos[1]    = pos;
            pos += _UbForm[i].length;

            memcpy (&_LineForm[i], &_UbForm[0], sizeof (field));
            _LineForm[i].item = &iline;
            _LineForm[i].length    = 1;
            _LineForm[i].pos[1]    = pos;
            _LineForm[i].attribut  = DISPLAYONLY;
            lday ++;
        }

// F�llbutton f�r �berschrifr am Zeilenende

        _UbForm[i].item = &ufiller;
        _UbForm[i].length    = 100;
        _UbForm[i].rows      = 0;
        _UbForm[i].pos[0]    = 0;
        _UbForm[i].pos[1]    = pos;
        _UbForm[i].feldid    = NULL;
        _UbForm[i].picture   = "";
        _UbForm[i].attribut  = BUTTON;
        _UbForm[i].before    = NULL;
        _UbForm[i].after     = NULL;
        _UbForm[i].BuId      = 0;

        DataForm.fieldanz = UbForm.fieldanz - 1;
        _DataForm = new field [UbForm.fieldanz];
        DLG::ExitError ((_DataForm == NULL), "Fehler bei der Speicherzuordnung");
        DataForm.mask = _DataForm;
        Lsts = new CFELD * [DataForm.fieldanz + 8];
        DLG::ExitError ((Lsts == NULL), "Fehler bei der Speicherzuordnung");

        RecLen = 19 + 
                 25 +                         // a_bz1
//                 15 +                         // menge
                 13 * DataForm.fieldanz + 
                 8 +                          // Variante 
                 12 +                         // Rezeptur
				 8 +						  // posi 020905	
				 8 +						  // flg_me_old
                 14 +                         // chg_gew
                 8 +                          // planstatus
                 10;                          // dummy

        Rec = new char [RecLen];
        DLG::ExitError ((Rec == NULL), "Fehler bei der Speicherzuordnung");
        memset (Rec, ' ', RecLen);

        char *Rpos = Rec;

        pos = 7;
        i = 0;

// Artikelspalte f�r Daten 

        strcpy (iname, "a");
        Lsts [i] = new CFELD (iname, Rpos, 19);
        DLG::ExitError ((Lsts[i] == NULL), "Fehler bei der Speicherzuordnung");
        Lsts[i]->Init ();
        memcpy (&_DataForm[i], &_UbForm[0], sizeof (field));

        _DataForm[i].item = new ITEM (iname, Lsts[i]->GetFeld (), "", NULL);
        _DataForm[i].length    = 14;
        _DataForm[i].pos[1]    = pos + 1;
        _DataForm[i].picture   = "%13.0f";
        _DataForm[i].attribut    = DISPLAYONLY;
        pos  += 16;
        Rpos += _DataForm[i].length;
        i ++;

        strcpy (iname, "a_bz1");
        Lsts [i] = new CFELD (iname, Rpos, 25);
        DLG::ExitError ((Lsts[i] == NULL), "Fehler bei der Speicherzuordnung");
        Lsts[i]->Init ();
        memcpy (&_DataForm[i], &_UbForm[0], sizeof (field));

        _DataForm[i].item = new ITEM (iname, Lsts[i]->GetFeld (), "", NULL);
        _DataForm[i].length      = 23;
        _DataForm[i].pos[0]      = 0;
        _DataForm[i].pos[1]      = pos + 1;
        _DataForm[i].attribut    = DISPLAYONLY;
        pos  += 25;
        Rpos += 25;
        i ++;

		/** 300709
        strcpy (iname, "mengen");
        Lsts [i] = new CFELD (iname, Rpos, 19);
        DLG::ExitError ((Lsts[i] == NULL), "Fehler bei der Speicherzuordnung");
        Lsts[i]->Init ();
        memcpy (&_DataForm[i], &_UbForm[0], sizeof (field));

        _DataForm[i].item = new ITEM (iname, Lsts[i]->GetFeld (), "", NULL);
        _DataForm[i].length      = 13;
        _DataForm[i].pos[0]      = 0;
        _DataForm[i].pos[1]      = pos + 1;
        _DataForm[i].picture   = "%9.3fn";
        _DataForm[i].attribut    = DISPLAYONLY;
        pos  += 15;
        Rpos += 15;
        i ++;
		***/
// Datenzeile f�r Mengeneingabe definieren

        for (; i < DataForm.fieldanz ; i ++)
        {
                 sprintf (iname, "me%d", i - 1);
//                 sprintf (iname, "me%d", i - 2);     //testtest

                 Lsts [i] = new CFELD (iname, Rpos, 12);
                 DLG::ExitError ((Lsts[i] == NULL), "Fehler bei der Speicherzuordnung");
                 Lsts[i]->Init ();
                 memcpy (&_DataForm[i], &_UbForm[0], sizeof (field));

                 _DataForm[i].item = new ITEM (iname, Lsts[i]->GetFeld (), "", NULL);
                 _DataForm[i].length    = 12;
                 _DataForm[i].pos[1]    = pos + 1;
                 _DataForm[i].picture   = "%9.1fn";
                 _DataForm[i].attribut  = EDIT;
                 pos += GewLen;
                 Rpos += 13;
        }


// Speicher f�r Variante

        strcpy (iname, "variante");
        Lsts [i] = new CFELD (iname, Rpos, 6);
        DLG::ExitError ((Lsts[i] == NULL), "Fehler bei der Speicherzuordnung");
        Lsts[i]->Init ();

// Speicher f�r Rezepturnummer

        Rpos += 8;
        i ++;
        strcpy (iname, "rez");
        Lsts [i] = new CFELD (iname, Rpos, 10);
        DLG::ExitError ((Lsts[i] == NULL), "Fehler bei der Speicherzuordnung");
        Lsts[i]->Init ();


// Speicher f�r posi 020905

        Rpos += 12;
        i ++;
        strcpy (iname, "posi");
        Lsts [i] = new CFELD (iname, Rpos, 10);
        DLG::ExitError ((Lsts[i] == NULL), "Fehler bei der Speicherzuordnung");
        Lsts[i]->Init ();

// Speicher f�r flg_me_old 100905

        Rpos += 8;
        i ++;
        strcpy (iname, "flg_me_old");
        Lsts [i] = new CFELD (iname, Rpos, 10);
        DLG::ExitError ((Lsts[i] == NULL), "Fehler bei der Speicherzuordnung");
        Lsts[i]->Init ();
// Speicher f�r chg_gew

        Rpos += 8;
        i ++;
        strcpy (iname, "chg_gew");
        Lsts [i] = new CFELD (iname, Rpos, 12);
        DLG::ExitError ((Lsts[i] == NULL), "Fehler bei der Speicherzuordnung");
        Lsts[i]->Init ();

        Lsts[i + 1] = NULL;

        ubrows = new int [DataForm.fieldanz + 1];
        DLG::ExitError ((ubrows == NULL), "Fehler bei der Speicherzuordnung");
        for (i = 0; i < DataForm.fieldanz; i ++)
        {
            ubrows[i] = i;
        }
        ubrows[i] = 0; 
}

void LISTDLG::GenWoFormsOneMengen (short abt)
{
        char iname [80];
        long lday;
        long ldatum;
        char day [12];
        char datum [12];
        char datvon [12];
        char datbis [12];
        char cmedat [20];
        static char *KwDays [] = {"",
                                  "",
                                  "",
                                  "Montag",
                                  "Dienstag",
                                  "Mittwoch",
                                  "Donnerstag",
                                  "Freitag",
                                  "Samstag",
                                  "Sonntag",
                                   NULL,
                                   NULL,
        };
        static char *KwDays1 [] = {"",
                                  "",
                                  "",
                                  "((Montag))",
                                  "((Dienstag))",
                                  "((Mittwoch))",
                                  "((Donnerstag))",
                                  "((Freitag))",
                                  "((Samstag))",
                                  "((Sonntag))",
                                   NULL,
                                   NULL,
        };

        sysdate (datum);
        ldatum = dasc_to_long (datum) + 1;
        lday = first_kw_day ((short) kw, (short) jr);
        StartDay = lday;
 	    DefCol.BkColor = GetSysColor (COLOR_3DFACE);
        eListe.SetNoScrollBkColor (DefCol.BkColor);
        work->SetAbt (abt);
        UbForm.fieldanz = 10;
        _UbForm = new field [UbForm.fieldanz];
        DLG::ExitError ((_UbForm == NULL), "Fehler bei der Speicherzuordnung");
        _LineForm = new field [UbForm.fieldanz];
        DLG::ExitError ((_LineForm == NULL), "Fehler bei der Speicherzuordnung");

        UbForm.mask = _UbForm;
        UbForm.frmstart  = 0;
        UbForm.frmscroll = 0;
        UbForm.caption   = NULL;
        UbForm.before    = NULL;
        UbForm.after     = NULL;
        UbForm.cbfield   = NULL;
        UbForm.font      = NULL;

        memcpy (&LineForm, &UbForm, sizeof (form));
        LineForm.mask    = _LineForm;

        int i = 0;
        int pos = 6;

        sprintf (iname, "a");
        char *ibez = new char [80]; 
        DLG::ExitError ((ibez == NULL), "Fehler bei der Speicherzuordnung");
        ColButton *Cub = new ColButton;
        DLG::ExitError ((Cub == NULL), "Fehler bei der Speicherzuordnung");
        memcpy (Cub, &DefCol, sizeof (ColButton));
        sprintf (ibez, "Artikel");
        Cub->text1 = ibez;
        Cub->ty1 = -1;

// Form �berschrift definieren

        _UbForm[i].item = new ITEM (iname, (char *) Cub, "", NULL);
        _UbForm[i].length    = 16;
        _UbForm[i].rows      = 0;
        _UbForm[i].pos[0]    = 0;
        _UbForm[i].pos[1]    = pos;
        _UbForm[i].feldid    = NULL;
        _UbForm[i].picture   = "";
        _UbForm[i].attribut  = COLBUTTON;
        _UbForm[i].before    = NULL;
        _UbForm[i].after     = NULL;
        _UbForm[i].BuId      = 0;

// �berschrift f�r Artikelspalte


        pos += _UbForm[i].length;
        memcpy (&_LineForm[i], &_UbForm[0], sizeof (field));
        _LineForm[i].item = &iline;
        _LineForm[i].length    = 1;
        _LineForm[i].pos[1]    = pos;
        _LineForm[i].attribut  = DISPLAYONLY;
        i ++;


// �berschrift f�r Bezeichnung

        sprintf (iname, "a_bz1");
        ibez = new char [80]; 
        DLG::ExitError ((ibez == NULL), "Fehler bei der Speicherzuordnung");
        Cub = new ColButton;
        DLG::ExitError ((Cub == NULL), "Fehler bei der Speicherzuordnung");
        memcpy (Cub, &DefCol, sizeof (ColButton));
        sprintf (ibez, "Bezeichnung");
        Cub->text1 = ibez;
        Cub->ty1 = -1;

        _UbForm[i].item = new ITEM (iname, (char *) Cub, "", NULL);
        _UbForm[i].length    = 25;
        _UbForm[i].rows      = 0;
        _UbForm[i].pos[0]    = 0;
        _UbForm[i].pos[1]    = pos;
        _UbForm[i].feldid    = NULL;
        _UbForm[i].picture   = "";
        _UbForm[i].attribut  = COLBUTTON;
        _UbForm[i].before    = NULL;
        _UbForm[i].after     = NULL;
        _UbForm[i].BuId      = 0;

        pos += _UbForm[i].length;
        memcpy (&_LineForm[i], &_UbForm[0], sizeof (field));
        _LineForm[i].item = &iline;
        _LineForm[i].length    = 1;
        _LineForm[i].pos[1]    = pos;
        _LineForm[i].attribut  = DISPLAYONLY;
        i ++;

// �berschrift f�r Averkaufsmengen

        sprintf (iname, "mengen");
        ibez = new char [80]; 
        DLG::ExitError ((ibez == NULL), "Fehler bei der Speicherzuordnung");
        Cub = new ColButton;
        DLG::ExitError ((Cub == NULL), "Fehler bei der Speicherzuordnung");
        memcpy (Cub, &DefCol, sizeof (ColButton));
        sprintf (ibez, "Liefermengen");
		strcpy (datvon,work.datvon);
		strcpy (datbis,work.datbis);
		rfmtdate (dasc_to_long(datvon),"dd.mm",datvon); 
		rfmtdate (dasc_to_long(datbis),"dd.mm",datbis); 
		sprintf(cmedat,"%s-%s",clipped(datvon),clipped(datbis));
		Cub->text1 = ibez;
        Cub->text2 = new char [25];
        DLG::ExitError ((Cub->text2 == NULL), "Fehler bei der Speicherzuordnung");
        strcpy (Cub->text2, cmedat);
//        Cub->ty1 = -1;

        _UbForm[i].item = new ITEM (iname, (char *) Cub, "", NULL);
        _UbForm[i].length    = 15;
        _UbForm[i].rows      = 0;
        _UbForm[i].pos[0]    = 0;
        _UbForm[i].pos[1]    = pos;
        _UbForm[i].feldid    = NULL;
        _UbForm[i].picture   = "";
        _UbForm[i].attribut  = COLBUTTON;
        _UbForm[i].before    = NULL;
        _UbForm[i].after     = NULL;
        _UbForm[i].BuId      = 0;

        pos += _UbForm[i].length;
        memcpy (&_LineForm[i], &_UbForm[0], sizeof (field));
        _LineForm[i].item = &iline;
        _LineForm[i].length    = 1;
        _LineForm[i].pos[1]    = pos;
        _LineForm[i].attribut  = DISPLAYONLY;
        i ++;

// �berschrift f�r Wochentage

        for (; i < UbForm.fieldanz - 1; i ++)
        {
            dlong_to_asc (lday, day);

            ibez = new char [80]; 
            DLG::ExitError ((ibez == NULL), "Fehler bei der Speicherzuordnung");
            sprintf (ibez, "%s", day);
            ColButton *Cub = new ColButton;
            DLG::ExitError ((Cub == NULL), "Fehler bei der Speicherzuordnung");
            memcpy (Cub, &DefCol, sizeof (ColButton));
            Cub->text1 = ibez;
            Cub->text2 = new char [25];
            DLG::ExitError ((Cub->text2 == NULL), "Fehler bei der Speicherzuordnung");
            strcpy (Cub->text2, KwDays[i]);
			if (((SpezDlg *) Dlg)->GetCheckMo() == 1)
			{
				if (strcmp ("Montag", KwDays[i]) == 0) Work::Datumsperre[i] = FALSE;
			}
			else
			{
				if (strcmp ("Montag", KwDays[i]) == 0) Work::Datumsperre[i] = TRUE;
			}

			if (((SpezDlg *) Dlg)->GetCheckDi() == 1)
			{
				if (strcmp ("Dienstag", KwDays[i]) == 0) Work::Datumsperre[i] = FALSE;
			}
			else
			{
				if (strcmp ("Dienstag", KwDays[i]) == 0) Work::Datumsperre[i] = TRUE;
			}

			if (((SpezDlg *) Dlg)->GetCheckMi() == 1)
			{
				if (strcmp ("Mittwoch", KwDays[i]) == 0) Work::Datumsperre[i] = FALSE;
			}
			else
			{
				if (strcmp ("Mittwoch", KwDays[i]) == 0) Work::Datumsperre[i] = TRUE;
			}

			if (((SpezDlg *) Dlg)->GetCheckDo() == 1)
			{
				if (strcmp ("Donnerstag", KwDays[i]) == 0) Work::Datumsperre[i] = FALSE;
			}
			else
			{
				if (strcmp ("Donnerstag", KwDays[i]) == 0) Work::Datumsperre[i] = TRUE;
			}

			if (((SpezDlg *) Dlg)->GetCheckFr() == 1)
			{
				if (strcmp ("Freitag", KwDays[i]) == 0) Work::Datumsperre[i] = FALSE;
			}
			else
			{
				if (strcmp ("Freitag", KwDays[i]) == 0) Work::Datumsperre[i] = TRUE;
			}

			if (((SpezDlg *) Dlg)->GetCheckSa() == 1)
			{
				if (strcmp ("Samstag", KwDays[i]) == 0) Work::Datumsperre[i] = FALSE;
			}
			else
			{
				if (strcmp ("Samstag", KwDays[i]) == 0) Work::Datumsperre[i] = TRUE;
			}


			if (ldatum  > lday + 1  && SperreAktiv == 1)
            {
				Work::Datumsperre[i] = TRUE;  //020204
                strcpy (Cub->text2, KwDays1[i]);
			}
		    else
            {
//				Datumsperre[i] = FALSE;  //020204
	            strcpy (Cub->text2, KwDays[i]);
			}
			if (ohneSamstag == 1)
			{
				if (strcmp ("Samstag", KwDays[i]) == 0) Work::Datumsperre[i] = TRUE; //050705
			}
            memcpy (&_UbForm[i], &_UbForm[0], sizeof (field));
            _UbForm[i].item = new ITEM (iname, (char *) Cub, "", NULL);
            _UbForm[i].length    = GewLen;
            _UbForm[i].pos[1]    = pos;
            pos += _UbForm[i].length;

            memcpy (&_LineForm[i], &_UbForm[0], sizeof (field));
            _LineForm[i].item = &iline;
            _LineForm[i].length    = 1;
            _LineForm[i].pos[1]    = pos;
            _LineForm[i].attribut  = DISPLAYONLY;
            lday ++;
        }

// F�llbutton f�r �berschrifr am Zeilenende

        _UbForm[i].item = &ufiller;
        _UbForm[i].length    = 100;
        _UbForm[i].rows      = 0;
        _UbForm[i].pos[0]    = 0;
        _UbForm[i].pos[1]    = pos;
        _UbForm[i].feldid    = NULL;
        _UbForm[i].picture   = "";
        _UbForm[i].attribut  = BUTTON;
        _UbForm[i].before    = NULL;
        _UbForm[i].after     = NULL;
        _UbForm[i].BuId      = 0;

        DataForm.fieldanz = UbForm.fieldanz - 1;
        _DataForm = new field [UbForm.fieldanz];
        DLG::ExitError ((_DataForm == NULL), "Fehler bei der Speicherzuordnung");
        DataForm.mask = _DataForm;
        Lsts = new CFELD * [DataForm.fieldanz + 8];
        DLG::ExitError ((Lsts == NULL), "Fehler bei der Speicherzuordnung");

        RecLen = 19 + 
                 25 +                         // a_bz1
                 15 +                         // menge
                 13 * DataForm.fieldanz + 
                 8 +                          // Variante 
                 12 +                         // Rezeptur
				 8 +						  // posi 020905	
				 8 +						  // flg_me_old
                 14 +                         // chg_gew
                 8 +                          // planstatus
                 10;                          // dummy

        Rec = new char [RecLen];
        DLG::ExitError ((Rec == NULL), "Fehler bei der Speicherzuordnung");
        memset (Rec, ' ', RecLen);

        char *Rpos = Rec;

        pos = 7;
        i = 0;

// Artikelspalte f�r Daten 

        strcpy (iname, "a");
        Lsts [i] = new CFELD (iname, Rpos, 19);
        DLG::ExitError ((Lsts[i] == NULL), "Fehler bei der Speicherzuordnung");
        Lsts[i]->Init ();
        memcpy (&_DataForm[i], &_UbForm[0], sizeof (field));

        _DataForm[i].item = new ITEM (iname, Lsts[i]->GetFeld (), "", NULL);
        _DataForm[i].length    = 14;
        _DataForm[i].pos[1]    = pos + 1;
        _DataForm[i].picture   = "%13.0f";
        _DataForm[i].attribut    = DISPLAYONLY;
        pos  += 16;
        Rpos += _DataForm[i].length;
        i ++;

        strcpy (iname, "a_bz1");
        Lsts [i] = new CFELD (iname, Rpos, 25);
        DLG::ExitError ((Lsts[i] == NULL), "Fehler bei der Speicherzuordnung");
        Lsts[i]->Init ();
        memcpy (&_DataForm[i], &_UbForm[0], sizeof (field));

        _DataForm[i].item = new ITEM (iname, Lsts[i]->GetFeld (), "", NULL);
        _DataForm[i].length      = 23;
        _DataForm[i].pos[0]      = 0;
        _DataForm[i].pos[1]      = pos + 1;
        _DataForm[i].attribut    = DISPLAYONLY;
        pos  += 25;
        Rpos += 25;
        i ++;

        strcpy (iname, "mengen");
        Lsts [i] = new CFELD (iname, Rpos, 19);
        DLG::ExitError ((Lsts[i] == NULL), "Fehler bei der Speicherzuordnung");
        Lsts[i]->Init ();
        memcpy (&_DataForm[i], &_UbForm[0], sizeof (field));

        _DataForm[i].item = new ITEM (iname, Lsts[i]->GetFeld (), "", NULL);
        _DataForm[i].length      = 13;
        _DataForm[i].pos[0]      = 0;
        _DataForm[i].pos[1]      = pos + 1;
        _DataForm[i].picture   = "%9.3fn";
        _DataForm[i].attribut    = DISPLAYONLY;
        pos  += 15;
        Rpos += 15;
        i ++;
// Datenzeile f�r Mengeneingabe definieren

        for (; i < DataForm.fieldanz ; i ++)
        {
                 sprintf (iname, "me%d", i - 1);
//                 sprintf (iname, "me%d", i - 2);     //testtest

                 Lsts [i] = new CFELD (iname, Rpos, 12);
                 DLG::ExitError ((Lsts[i] == NULL), "Fehler bei der Speicherzuordnung");
                 Lsts[i]->Init ();
                 memcpy (&_DataForm[i], &_UbForm[0], sizeof (field));

                 _DataForm[i].item = new ITEM (iname, Lsts[i]->GetFeld (), "", NULL);
                 _DataForm[i].length    = 12;
                 _DataForm[i].pos[1]    = pos + 1;
                 _DataForm[i].picture   = "%9.1fn";
                 _DataForm[i].attribut  = EDIT;
                 pos += GewLen;
                 Rpos += 13;
        }


// Speicher f�r Variante

        strcpy (iname, "variante");
        Lsts [i] = new CFELD (iname, Rpos, 6);
        DLG::ExitError ((Lsts[i] == NULL), "Fehler bei der Speicherzuordnung");
        Lsts[i]->Init ();

// Speicher f�r Rezepturnummer

        Rpos += 8;
        i ++;
        strcpy (iname, "rez");
        Lsts [i] = new CFELD (iname, Rpos, 10);
        DLG::ExitError ((Lsts[i] == NULL), "Fehler bei der Speicherzuordnung");
        Lsts[i]->Init ();


// Speicher f�r posi 020905

        Rpos += 12;
        i ++;
        strcpy (iname, "posi");
        Lsts [i] = new CFELD (iname, Rpos, 10);
        DLG::ExitError ((Lsts[i] == NULL), "Fehler bei der Speicherzuordnung");
        Lsts[i]->Init ();

// Speicher f�r flg_me_old 100905

        Rpos += 8;
        i ++;
        strcpy (iname, "flg_me_old");
        Lsts [i] = new CFELD (iname, Rpos, 10);
        DLG::ExitError ((Lsts[i] == NULL), "Fehler bei der Speicherzuordnung");
        Lsts[i]->Init ();
// Speicher f�r chg_gew

        Rpos += 8;
        i ++;
        strcpy (iname, "chg_gew");
        Lsts [i] = new CFELD (iname, Rpos, 12);
        DLG::ExitError ((Lsts[i] == NULL), "Fehler bei der Speicherzuordnung");
        Lsts[i]->Init ();

        Lsts[i + 1] = NULL;

        ubrows = new int [DataForm.fieldanz + 1];
        DLG::ExitError ((ubrows == NULL), "Fehler bei der Speicherzuordnung");
        for (i = 0; i < DataForm.fieldanz; i ++)
        {
            ubrows[i] = i;
        }
        ubrows[i] = 0; 
}
     

void LISTDLG::EnterZusatz (char* datum, double a, short mdn, double menge, int i)
{
    EnterF Enter;
  char *text = "             ";
	int AktRow;
	if (menge < (double) 0 || SperreGesetzt == 10)
	{
		print_mess (1, "Planmenge kann nicht mehr verkleinert werden");
		sprintf(Lsts[i]->GetFeld (),"%.2f", AktMe);
        memcpy (LstTab [eListe.GetAktRow ()], Rec, RecLen);
		syskey = KEYCR;
		return;
	}
    

    sprintf (text,"%.3f",menge);
	sprintf(Lsts[i]->GetFeld (),"%.2f", AktMe); //010905
    memcpy (LstTab [eListe.GetAktRow ()], Rec, RecLen);
    Enter.EnterText (hMainInst, NULL, "Zusatzmenge", text, 12, "7.3f", "Zusatzmenge");
    if (syskey == KEY5)
    {
		syskey = KEYCR;
		return;
    }
	menge = ratod (text);
	// 010905 A
	//Insert nicht immer notwendig, was ist wenn Artikel scon mehrfach in der Liste ???
	//TODO sollte noch �berpr�ft werden
	syskey = 999; //dadurch wird TestAppend auf 1 gesetzt 
    eListe.InsertLine (); 
	AktRow = eListe.GetAktRow();
    //nicht n�tig : memcpy (LstTab [AktRow], LstTab [AktRow + 1], RecLen);
    memcpy (Rec , LstTab [AktRow + 1], RecLen);
	syskey = KEYCR;

	//Mengen der anderen Spalten raus!
	     short p = 0;
         if (work->GetMatrix () == BIG)
         {
             p = 0;
         }
         else if (work->GetMatrix () == SMALL)
         {
             p = 1;
         }

         for (int y = 1 + p; y < DataForm.fieldanz - 1; y ++)
         {
			sprintf(Lsts[y]->GetFeld (),"%.2f", 0.0);
		 }




	sprintf(Lsts[i]->GetFeld (),"%.2f", menge);
    short neuposi = HoleMaxPosi(a) + 1;
	sprintf(text, "%d", neuposi);
    int fpos = CFELD::GetFeldpos (Lsts, "posi");
    if (fpos != -1)
    {
           strcpy (Lsts[fpos]->GetFeld (), clipped(text)); 
    }
	// 010905 E
   //010905 nicht mehr n�tig, wenn 2.Datensatz ((SpezDlg *) Dlg)->SchreibeZusatz (datum, a, mdn, menge);

}



void LISTDLG::DeleteForms (void)
{
        int i;

        if (Lsts != NULL)
        {
            for (i = 0; Lsts[i] != NULL; i ++)
            {
                if (Lsts[i] != NULL)
                {
                    delete Lsts[i];
                }
            }
            delete Lsts;
            Lsts = NULL;
        }

        if (_UbForm != NULL)
        {
          for (i = 0; i < UbForm.fieldanz; i++)
          {
            if (UbForm.mask[i].item != NULL &&
                UbForm.mask[i].item != &ufiller)
            {
                if (UbForm.mask[i].attribut & COLBUTTON)
                {
                      ColButton *Cub = (ColButton *) UbForm.mask[i].item->GetFeldPtr ();
                      if (Cub != NULL)
                      {
                          if (Cub->text1 != NULL) delete Cub->text1;
                          if (Cub->text2 != NULL) delete Cub->text2;
                      }
                      delete Cub;
                }
                else if (UbForm.mask[i].item->GetFeldPtr () != NULL)
                {
                      delete UbForm.mask[i].item->GetFeldPtr ();
                }
                delete UbForm.mask[i].item;
            }
          }
          delete _UbForm; 
          _UbForm = NULL;
        }
        UbForm.fieldanz = 0; 

        if (_LineForm != NULL)
        {
          delete _LineForm; 
        }
        LineForm.fieldanz = 0;

        if (_DataForm != NULL)
        {
          for (i = 0; i < DataForm.fieldanz; i++)
          {
            if (DataForm.mask[i].item != NULL)
            {
                delete DataForm.mask[i].item;
            }
          }
          delete _DataForm;
          _DataForm = NULL;
        }

        DataForm.fieldanz = 0; 

        if (Rec != NULL)
        {
            delete Rec;
            Rec = NULL;
        }

        if (ubrows != NULL)
        {
             delete ubrows;
        }
}

char *LISTDLG::GetAktDat (char *dat)
{
    int Col = eListe.GetAktColumn ();
    ColButton *Cub = (ColButton *) UbForm.mask[Col].item->GetFeldPtr ();
    strcpy (dat, Cub->text1);
    return dat;
}
    
char *LISTDLG::GetLastDat (char *dat)
{
    int Col = UbForm.fieldanz -2;
    ColButton *Cub = (ColButton *) UbForm.mask[Col].item->GetFeldPtr ();
    strcpy (dat, Cub->text1);
    return dat;
}

LISTDLG::LISTDLG (short mdn, short abt, short k, short j) : KEYS (), LISTENTER () 
{

    if (work == NULL) return;
    ProgCfg = new PROG_CFG ("woplan");
	Dlg = NULL;
    cfgOK = FALSE;
	cursor = -1;
    prstatatag.mdn = mdn;
    kw = k;
    jr = j;
	/*
    if (work->GetMatrix () == BIG)
    {
//            GenWoForms (abt);
    }
    else if (work->GetMatrix () == SMALL)
    {
            GenWoFormsOne (abt);
    }
    else
    {
            GenWoFormsOne (abt);
    }
*/
	if (Work::LiefMeHolen == 1)
	{
		GenWoFormsOneMengen (abt);
	}
	else
	{
		GenWoFormsOne (abt);
	}




//    LstTab = new char * [0x10000];
//    for (int i = 0; i < 0x10000; i ++)

    dataform  = &DataForm;
    ubform    = &UbForm;
    lineform  = &LineForm;
    LISTENTER::GetCfgValues ();
    GetCfgValues ();
    eListe.SetUbRows (ubrows); 
	eListe.SetKeys (this);

    if ((work.ArtScroll == FALSE) && (work->GetMatrix () == BIG))
    {
           eListe.SetHscrollStart (1);
           UbForm.ScrollStart = 1;
    }
    else if ((work.ArtScroll == FALSE)  && (work->GetMatrix () == SMALL))
    {

           eListe.SetHscrollStart (2);
           UbForm.ScrollStart = 2;
    }
    else if (work.ArtScroll == FALSE)
    {
           eListe.SetHscrollStart (1);
           UbForm.ScrollStart = 1;
    }


//    GetSysPar ();
    UbHeight = 3;
	SetUbHeight ();

    TestRemoves ();

//    SetNoRecNr ();
	fChoise.Setchpos (0, 2);
    ChAttr = ChAttra;
    Toolbar2 = NULL;
    eListe.SetInfoProc (InfoProc);
    eListe.SetInfo (this);
    ArrDown = NULL;
    ArrDown = LoadBitmap (DLG::hInstance, "ARRDOWN");
    eListe.SetTestAppend (TestAppend);
//    eListe.SetNoMove (TRUE);
    pagelen = 66;
    pagelen0 = 59;
    ublen = 4;
    PrintFont = 0;
    RowPrintFont = 0;
    BOOL UsePrintForm = FALSE;
    BOOL QuerPrint = FALSE;
    DrkDeviceFile = NULL;
    PrintBackground = FALSE;
}


LISTDLG::~LISTDLG ()
{
    int i, anz;
	if (work != NULL)
	{
          work->SetListDlg (NULL);
	}
    fChoise.destroy ();
	if (LstTab != NULL)
	{

        anz =   eListe.GetRecanz ()	;
        for (i = 0; i < anz; i ++)
        {
/*
            if (LstTab[i].choise[0] != NULL)
            {
                delete LstTab[i].choise[0];
            }
*/
        }

//  	    delete LstTab;
//		LstTab = NULL;
	}
    if (ProgCfg != NULL)
    {
        ProgCfg->CloseCfg ();
        delete ProgCfg;
        ProgCfg = NULL;
    }
    DeleteForms ();
    NoRecNrOK = FALSE;
}


int LISTDLG::TestAppend (void)
{
    switch (syskey)
    {
         case KEY6 :
           return 0;  
         case KEY7 :
           return 1; //211105 
         case KEY8 :
           return 0;
         case 999  :
           return 1;  //010905 manuell ein Zeileneinf�gen erzwingen
    }
    return 1;
}

// Funktionen f�r den Zugriff auf die Daten. Sind als abstrakte Schnittstellen in
// listenter definiert.

unsigned char *LISTDLG::GetDataRec (void)
{
    return (unsigned char *) Rec;
}

int LISTDLG::GetDataRecLen (void)
{
    return RecLen;
}

char *LISTDLG::GetListTab (int i)
{
	if (i == 0)
	{
		int di = 0;
	}
    LstTab[i] = new char [RecLen];
    DLG::ExitError ((LstTab[i] == NULL), "Fehler bei der Speicherzuordnung");
    memset (LstTab[i], ' ' , RecLen);
    return (char *) LstTab[i];
}

// Ende der Schnittstellen.


void LISTDLG::Setfeld2Removed (BOOL removed)
{
    if (removed)
    {
            SetFieldAttr ("feld2", REMOVED);
    }
    else
    {
            SetFieldAttr ("feld2", EDIT);
    }
}

void LISTDLG::Prepare (void)
{
     cursor = work->Prepare ();
}

void LISTDLG::Open (void)
{
    work->Open ();
}

double LISTDLG::GetA (field *feld)
{
    if ((feld->attribut & COLBUTTON) == 0)
    {
        return ratod (feld->item->GetFeldPtr ());
    }

    ColButton *Cub = (ColButton *) feld->item->GetFeldPtr ();
    if (Cub != NULL)
    {
        return ratod (Cub->text1);
    }
    return 0.0;
}


char * LISTDLG::GetLief (field *feld)
{
    if ((feld->attribut & COLBUTTON) == 0)
    {
        return clipped (feld->item->GetFeldPtr ());
    }

    ColButton *Cub = (ColButton *) feld->item->GetFeldPtr ();
    if (Cub != NULL)
    {
        return clipped (Cub->text1);
    }
    return NULL;
}

int LISTDLG::Fetch (void)
{
    work->SetLsts (Lsts);

    int dsqlstatus =work->Fetch ();
    if (dsqlstatus == 100)
    {
          return dsqlstatus;
    }

    return FetchA ();
}


int LISTDLG::FetchA (void)
{

    sprintf (Lsts[0]->GetFeld (), "%.0lf", _a_bas.a);
    clipped (Lsts[0]->GetFeld ());
    int fpos = CFELD::GetFeldpos (Lsts, "a_bz1");
    if (fpos != -1)
    {
           strcpy (Lsts[fpos]->GetFeld (), clipped (_a_bas.a_bz1));
    }

    for (int i = 1; i < UbForm.fieldanz - 1; i ++)
    {
               work->FetchPlanGew ();
    }

    return 0;
}


void LISTDLG::Close (void)
{
    work->Close ();
	if (cursor != -1)
	{
		cursor = -1;
	}
}


BOOL LISTDLG::BeforeCol (char *colname)
{
         char aktcol [5];
         int p;
         char date [12];

         if (work->GetMatrix () == BIG)
         {
             p = 0;
         }
         else if (work->GetMatrix () == SMALL)
         {
             p = 1;
         }
         GetAktDat (date);
         if (strcmp (date, "Artikel") == 0) return FALSE;
         if (strcmp (date, "Liefermengen") == 0) return FALSE;
         for (int i = 1 + p; i < DataForm.fieldanz; i ++)
         {
             sprintf (aktcol, "me%d", i - p); 

             if (strcmp (colname, aktcol) == 0)
             {
                    double a       = ratod (CFELD::GetFeldWrt (Lsts, "a"));
//ist teilw. 0 , warum ???                    double chg_gew = ratod (CFELD::GetFeldWrt (Lsts, "chg_gew"));
					double chg_gew = prdk_k.kutter_gew;  
                    double me      = ratod (Lsts[i]->GetFeld ());
                    short posi = atoi (CFELD::GetFeldWrt (Lsts, "posi"));

                    SperreGesetzt = ((SpezDlg *) Dlg)->SetSperre (date, a,posi);
					AktMe = me;

                    if (work->GetListMeMode () == MENGE || work->GetListMeMode () == AUFTRAEGE)
                    {
                          ((SpezDlg *) Dlg)->SetChargen (me / chg_gew, chg_gew, a);
                    }
                    else if (work->GetListMeMode () == CHARGEN)
                    {
                          ((SpezDlg *) Dlg)->SetChargen (me * chg_gew, chg_gew, a);
                    }
                    if (me != 0.0)
                    {
                          ((SpezDlg *) Dlg)->SetBearb (work->GetPlanStatus (a,posi, date));
                    }
                    return FALSE;
             }
         }
         return FALSE;
}


BOOL LISTDLG::AfterCol (char *colname)
{
	     char date[12];
         char aktcol [5];
		 long ldat;
         int p;

         if (work->GetMatrix () == BIG)
         {
             p = 0;
         }
         else if (work->GetMatrix () == SMALL)
         {
             p = 1; //300709 (von 1 auf 0 (nur wenn ohne Liefermengen)
         }

        GetAktDat (date);
        ldat = dasc_to_long (date);


//         for (int i = 1 + p; i < DataForm.fieldanz - 1; i ++)
         for (int i = 1 + p; i < DataForm.fieldanz; i ++)  //testtest
         {
             sprintf (aktcol, "me%d", i - p);

             if (strcmp (colname, aktcol) == 0)
             {
                    double a       = ratod (CFELD::GetFeldWrt (Lsts, "a"));
                    double chg_gew = ratod (CFELD::GetFeldWrt (Lsts, "chg_gew"));
                    double me      = ratod (Lsts[i]->GetFeld ());
					if (AktMe != me) 
					{
						CFELD::SetFeldWrt (Lsts, "flg_me_old", "1"); //100905
						if (work->ldat1 != ldat && work->ldat2 != ldat && work->ldat3 != ldat && work->ldat4 != ldat
							&& work->ldat5 != ldat && work->ldat6 != ldat && work->ldat7 != ldat)
						{
							if (work->ldat1 == 0) work->ldat1 = ldat;
							else if (work->ldat2 == 0) work->ldat2 = ldat;
							else if (work->ldat3 == 0) work->ldat3 = ldat;
							else if (work->ldat4 == 0) work->ldat4 = ldat;
							else if (work->ldat5 == 0) work->ldat5 = ldat;
							else if (work->ldat6 == 0) work->ldat6 = ldat;
							else if (work->ldat7 == 0) work->ldat7 = ldat;

						}
					}
                    if (work->GetListMeMode () == MENGE || work->GetListMeMode () == AUFTRAEGE)
                    {
                          ((SpezDlg *) Dlg)->SetChargen (me / chg_gew, chg_gew, a);
                    }
                    else if (work->GetListMeMode () == CHARGEN)
                    {
                          ((SpezDlg *) Dlg)->SetChargen (me * chg_gew, chg_gew, a);
                    }
					if (AktMe != me) //141105 bgut1   prstatmasch status zur�cksetzen
					{
						work->MaschStatusZurueck(date,a,prstatatag.mdn);
					}
					if (SperreGesetzt >= 1) //100905 jetzt auch neue Zeile bei Grundbr�t in bearb.
					{
						if (AktMe != me) 
						{
							EnterZusatz(date, a, prstatatag.mdn, me - AktMe,i);
							//Menge zur�cksetzen
//010905							sprintf(Lsts[i]->GetFeld (),"%.2f", AktMe);
//			                memcpy (&LstTab[eListe.GetAktRow ()], &Lsts, sizeof (Lsts));
				            memcpy (LstTab [eListe.GetAktRow ()], Rec, RecLen);
						    eListe.ShowAktRow ();
							return TRUE; //auf dem Feld stehenbleiben
						}
					}
//310805					if (SperreGesetzt == 2 & (me - AktMe) < 0.0)  //Grundbr�t in Bearb.
					if (SperreGesetzt == 2 && (me - AktMe) < 0.0)  //Grundbr�t in Bearb.
					{
						print_mess (1, "Planmenge kann nicht mehr verkleinert werden");
						syskey = KEYCR;
							sprintf(Lsts[i]->GetFeld (),"%.2f", AktMe);
				            memcpy (LstTab [eListe.GetAktRow ()], Rec, RecLen);
						    eListe.ShowAktRow ();
						return TRUE;
					}


                    return FALSE;
             }
         }
         return FALSE;
}

int LISTDLG::AfterRow (int Row)
{
//         form * DataForm; 

/*
         if (syskey == KEYUP && strcmp (clipped (Lsts.feld1), " ") <= 0)
         {
                return OnKeyDel ();
         }
*/
         memcpy (LstTab [Row], Rec, RecLen);
         work->SetLsts (Lsts);
         return 1;
}

int LISTDLG::BeforeRow (int Row)
{
//041105 bgut1         BeforeCol ("me1"); 
         memcpy (LstTab [Row], Rec, RecLen);
         return 1;
}

/*
int LISTDLG::ShowA ()
{

        struct SA *sa;

        if (Modal)
        {  
               SearchA.SetWindowStyle (WS_POPUP | 
                                       WS_THICKFRAME |
                                       WS_CAPTION |
                                       WS_SYSMENU |
                                       WS_MINIMIZEBOX |
                                       WS_MAXIMIZEBOX);
               SearchA.SetParams (DLG::hInstance, hMainWindow);
        }
        else
        {  
               SearchA.SetWindowStyle (WS_POPUP | WS_THICKFRAME);
               SearchA.SetParams (DLG::hInstance, hMainWindow);
        }
        SearchA.Setawin (eListe.GethMainWindow ());
        SearchA.SearchA ();
        if (Modal)
        {  
               EnableWindows (GetParent (eListe.GethMainWindow ()), FALSE);
        }
        if (syskey == KEY5)
        {
            SetListFocus ();
            return 0;
        }
        sa = SearchA.GetSa ();
        if (sa == NULL)
        {
            return 0;
        }

        strcpy (LstTab [eListe.GetAktRow ()].a, sa->a);
        eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
        eListe.ShowAktRow ();
        SetListFocus ();
        return 0;
}
*/

BOOL LISTDLG::OnKey5 (void) 
{
    syskey = KEY5;
    if (work != NULL)
    {
        work->RollbackWork ();
    }
	return TRUE;
}


BOOL LISTDLG::OnKey6 (void)
{
	syskey = KEY6;
    eListe.InsertLine ();
    work->loesch_woplan_del();
    UpdateList ();
    return TRUE;
}

BOOL LISTDLG::OnKeyInsert (void)
{
    eListe.InsertLine ();
    work->loesch_woplan_del();
    UpdateList ();
    return TRUE;
}

BOOL LISTDLG::OnKey7 (void)
{
    if (PruefLoesch()) eListe.DeleteLine ();
    return TRUE;
}

BOOL LISTDLG::OnKeyDel (void)
{
    if (PruefLoesch()) eListe.DeleteLine ();
    return TRUE;
}

BOOL LISTDLG::OnKey8 (void)
{
         return FALSE;
}


BOOL LISTDLG::OnKey9 (void)
{
    char * Item = eListe.GetAktItem ();
    if (strcmp (Item, "feld1") == 0)
    {
//        ShowA ();
        return TRUE;
    }
    return FALSE;
}


BOOL LISTDLG::OnKey10 (void)
{

    if (Dlg != NULL)
    {
         work->SetLsts (Lsts);
//         SetListFocus ();
         return ((SpezDlg *) Dlg)->OnKey10 ();
	}
    return FALSE;
}

BOOL LISTDLG::OnKey11 (void)
{

    return FALSE;
}


BOOL LISTDLG::OnKeyNext (void)
{
        if (GetKeyState (VK_CONTROL) >= 0)
        {
              if (Dlg != NULL)
              {
                  ((SpezDlg *) Dlg)->NextPage ();
                  return TRUE;
              }
        }
        return FALSE;
}


BOOL LISTDLG::OnKeyPrior (void)
{
        if (GetKeyState (VK_CONTROL) >= 0)
        {
              if (Dlg != NULL)
              {
                  ((SpezDlg *) Dlg)->PriorPage ();
                  return TRUE;
              }
        }
        return FALSE;
}

BOOL LISTDLG::OnKeyReturn (void)
{
	syskey = KEYDOWN;
        return FALSE;
}


int LISTDLG::ToMemory (int i)
{

    LISTENTER::ToMemory (i);
    return 0;
}


void LISTDLG::InitRow (void)
{

    LISTENTER::InitRow ();


    work->SetLsts (Lsts);

//    if (ArrDown != NULL && Lsts.choise[0] != NULL)
    {
//        Lsts.choise[0]->SetBitMap (ArrDown);
    }


}


BOOL LISTDLG::WriteList (void)
{
    extern short sql_mode;

    if (AfterRow (eListe.GetAktRow ()) == -1)
    {
        return FALSE;
    }

    int recanz = eListe.GetRecanz ();

    short sql_s = sql_mode;
    sql_mode = 1;

    for (int i = 0; i < recanz; i ++)
    {
        int dsqlstatus = WriteRec (i);
        if (dsqlstatus != 0)
        {
            disp_mess ("Fehler beim Schreiben der Preise\n"
                       "Die Datens�tze werden eventuell an einem anderen Arbeitsplatz\n"
                       "bearbeitet", 2);
            sql_mode = sql_s;
		    eListe.SetFeldFocus0 (eListe.GetAktRow (), eListe.GetAktColumn ());
            return FALSE;
        }
    }

    sql_mode = sql_s;
    eListe.SetFeldFocus0 (eListe.GetAktRow (), eListe.GetAktColumn ());
    return TRUE;
}


void LISTDLG::UpdateList (void)
{

    eListe.DestroyFocusWindow ();
    ShowDB ();
    eListe.SetFeldFocus0 (eListe.GetAktRow (), eListe.GetAktColumn ());
    BeforeCol (DataForm.mask [eListe.GetAktColumn ()].item->GetItemName ());
}


void LISTDLG::uebertragen (void)
{
    work->SetLsts (Lsts);
	int noedit = 1;
//	if (SperreAktiv == 1) noedit = 1;


// Datenzeile f�r Mengeneingabe definieren

        for (int i = 3; i < DataForm.fieldanz ; i ++)
        {
                 if (work->GetListMeMode () == MENGE)
                 {
                     _DataForm[i].picture   = "%9.3fn";
                 }
                 if (work->GetListMeMode () == AUFTRAEGE)
                 {
                     _DataForm[i].picture   = "%9.0fnx"; //DDDDDDDDDDDDDDDD
                 }
                 else if (work->GetListMeMode () == CHARGEN)
                 {
                     _DataForm[i].picture   = "%9.2fn";
                 }
				 if (Work::Datumsperre[i] == TRUE)  //#020204
                 {
//					 if (SperreAktiv == 1) _DataForm[i].attribut  = DISPLAYONLY;
					 _DataForm[i].attribut  = DISPLAYONLY;
                 }
				 else
   				 {
	                 _DataForm[i].attribut  = EDIT;
					 noedit = 0;
                 }
        }
		if (noedit == 1) _DataForm[0].attribut  = EDIT;
		if (noedit == 0) _DataForm[0].attribut  = DISPLAYONLY;
		if (noedit == 0) _DataForm[1].attribut  = DISPLAYONLY;
		if (noedit == 0) _DataForm[2].attribut  = DISPLAYONLY;



    work->FillRow (StartDay, DataForm.fieldanz);
//    if (ArrDown != NULL && Lsts.choise[0] != NULL)
    {
//        Lsts.choise[0]->SetBitMap (ArrDown);
    }
}

int LISTDLG::WriteRec (int i)
{
	memcpy (Rec, LstTab [i], RecLen);
	work->SetLsts (Lsts);
//	return work->WriteRec (&UbForm, DataForm.fieldanz - 1, StartDay, i);
	return work->WriteRec (&UbForm, DataForm.fieldanz , StartDay, i); //testtest
}

double LISTDLG::RechneSumme (void)
{
	int i = 0;
	int x = 0;
	double me = (double) 0;
    long day, aktdat;
	char* Recsav;
	char AktDat [12];
	int Feldanz = DataForm.fieldanz - 1;
    Recsav = new char[RecLen + 1];
    GetAktDat (AktDat);
    aktdat = dasc_to_long (AktDat);

	memcpy (Recsav, Rec, RecLen);
    int recanz = eListe.GetRecanz ();
	for (i = 0; i < recanz; i ++, day++)
    {
	    if (work.MatrixTyp == BIG)
	    {
			x = 1;
		}
		else if (work.MatrixTyp == SMALL)
		{
	        x = 3;
			if (Work::LiefMeHolen == 0) x = 2; //300709
	    }
		memcpy (Rec, LstTab [i], RecLen);
	    for (day = StartDay; x < Feldanz; x ++, day ++)
	    {
			if (day != aktdat) continue;
			me = me + ratod (Lsts[x]->GetFeld ());
		}
	}
	memcpy (Rec, Recsav, RecLen);
	delete Recsav;
	return me;
}

short LISTDLG::HoleMaxPosi (double artikel)
{
	int i = 0;
    long day;
	short posi = 0;
	short maxposi = 0;
	char* Recsav;
	int Feldanz = DataForm.fieldanz - 1;
    Recsav = new char[RecLen + 1];

 	memcpy (Recsav, Rec, RecLen); 
    int recanz = eListe.GetRecanz ();
	for (i = 0; i < recanz; i ++, day++)
    {
		memcpy (Rec, LstTab [i], RecLen);
        if (ratod (CFELD::GetFeldWrt (Lsts, "a")) == artikel)
		{
		  posi = atoi (CFELD::GetFeldWrt (Lsts, "posi"));
		  if (posi > maxposi) maxposi = posi;
		}
	}
	memcpy (Rec, Recsav, RecLen);
	delete Recsav;
	return maxposi;
}


BOOL LISTDLG::OnKey12 (void)
{
	/*
    WMESS Wmess;
    Wmess.Message (hMainInst, hMainWindow, " \nBitte warten.....\n"
                                            "Die Planmengen werden geschrieben.\n ");  
*/
    StopList ();
    syskey = KEY12;
    if (work != NULL)
    {
        work->CommitWork ();
    }
	work->ldat1 = 0;work->ldat2 = 0;work->ldat3 = 0;work->ldat4 = 0;
	work->ldat5 = 0;work->ldat6 = 0;work->ldat7 = 0;
//    Wmess.Destroy ();
    return TRUE;
}


BOOL LISTDLG::StopList (void)
{
    WMESS Wmess;
    int i;
    int recanz;
	bool dmess = FALSE;
    short sql_s;
    extern short sql_mode;
	char cMeld[120];

    if (AfterRow (eListe.GetAktRow ()) == -1)
    {
        return TRUE;
    }

    eListe.GetFocusText ();
    recanz = eListe.GetRecanz ();

    sql_s = sql_mode;
    sql_mode = 1;
	int y = 9;
	for (i = 0; i < recanz; i ++)
    {
		y++;
		if (y == 10)
		{
			sprintf (cMeld, "Bitte warten ...%d \n"
                        "Die Planmengen werden geschrieben.\n "  
			            ,recanz - i);
		    if (dmess == TRUE) Wmess.Destroy ();
			Wmess.Message (hMainInst, hMainWindow,cMeld);  
			dmess = TRUE;
			y = 0;
		}
        int dsqlstatus = WriteRec (i);
        if (dsqlstatus != 0)
        {
            disp_mess ("Fehler beim Schreiben der Daten\n"
                       "Die Datens�tze werden eventuell an einem anderen Arbeitsplatz\n"
                       "bearbeitet", 2);
            sql_mode = sql_s;
		    eListe.SetFeldFocus0 (eListe.GetAktRow (), eListe.GetAktColumn ());
		    Wmess.Destroy ();
            return TRUE;
        }
    }
    sql_mode = sql_s;
			sprintf (cMeld, "Bitte warten ... \n"
                        "Grundbr�te werden geschrieben.\n "  
			            );
		    if (dmess == TRUE) Wmess.Destroy ();
			Wmess.Message (hMainInst, hMainWindow,cMeld);  
			dmess = TRUE;
	work->Upd99();

    eListe.SetFeldFocus0 (eListe.GetAktRow (), eListe.GetAktColumn ());

    if (dmess == TRUE)  Wmess.Destroy ();
    return TRUE;
}


BOOL LISTDLG::OnKeyUp (void)
{
//    form *DataForm;
//    int fpos;
/*
    if (Leere Zeile)
    {
        return OnKeyDel ();
    }
*/

    return FALSE;
}

BOOL LISTDLG::OnKeyDown (void)
{
//    form *DataForm;
//    int fpos;

/*
    if (Leere Zeile)
    {
        return TRUE;
    }
*/

    return FALSE;
}


void LISTDLG::PrintCaption (FILE *fp, char *buffer)
{
        char datum [12];
        char b[80];
        int len;

        Page ++;
        fprintf (fp, "\n");
        fprintf (fp, "%c%c", ESC, FETT1);

        sysdate (datum);
        memset (buffer, ' ', 78);
        buffer [78] = 0;
        memcpy (&buffer[1], "Wochenplan vom  ", strlen ("Wochenplan vom  "));
        memcpy (&buffer[16], datum, strlen (datum));
        memcpy (&buffer[30], "Mandant", strlen ("Mandant"));
        sprintf (b, "%2d", _mdn.mdn);
        memcpy (&buffer[38], b, strlen (b));
        work->GetMdnName (b, _mdn.mdn);
        b[16] = 0;
        memcpy (&buffer[41], b, strlen (b));
        memcpy (&buffer[69], "Seite", strlen ("Seite"));
        sprintf (&buffer[75], "%d", Page);
        fprintf (fp, "%s\n", buffer);

        memset (buffer, ' ', 78);
        buffer [78] = 0;
        memcpy (&buffer[1], "Kalenderwoche  ", strlen ("Kalenderwoche  "));
        sprintf (b, "%2d", kw);
        memcpy (&buffer[16], b, strlen (b));
        memcpy (&buffer[30], "ABT", strlen ("ABT"));
        sprintf (b, "%2d", work->GetAbt ());
        memcpy (&buffer[38], b, strlen (b));
        work->GetAbtBez (b, work->GetAbt ());
        sprintf (&buffer[41], "%s", b);
        b[16] = 0;
        fprintf (fp, "%s\n", buffer);

        fprintf (fp, "%c%c", ESC, FETT2);
        fprintf (fp, "\n");


        if (ENG > 0)
        {
                fprintf (fp, "%c%c", ESC, ENG);
        }
        if (FETT)
        {
                fprintf (fp, "%c%c", ESC, FETT1);
        }

		eListe.FillCubPrintUb (buffer, 0);
   	    fprintf (fp, "%s\n", buffer);
		eListe.FillCubPrintUb (buffer, 1);
   	    fprintf (fp, "%s\n", buffer);
		len = strlen (buffer);
        sprintf (buffer, "#LINE%d", len);
   	    fprintf (fp, "%s\n", buffer);

/*
        pline = new char [len + 10];
		memset (pline, '-', len);
		pline [len] = 0;
   	    fprintf (fp, "%s\n", pline);
        delete pline;
*/
        row = 7;
}


int LISTDLG::GetPageRows (char *Printer, mfont *mFont)
/**
Seitenlaenge fuer gewaehlten Drucker holen.
**/
{
     HDC hdc;
     int RowHeight;
     int PageRows;
     int cxPage;
     int cyPage;
     SIZE size;
     HFONT oldfont;
     TEXTMETRIC tm;


     hdc = GetPrinterbyName (Printer);

     if (hdc == NULL) return 0;
     cxPage = GetDeviceCaps (hdc, HORZRES);
     cyPage = GetDeviceCaps (hdc, VERTRES);
	 HFONT hFont = SetDeviceFont (hdc, mFont, &tm);
     oldfont = SelectObject (hdc, hFont);
     GetTextExtentPoint32 (hdc, "X", 1, &size);
	 RowHeight = (int) (double) ((double) tm.tmHeight * 1.0); 
     PageRows = (int) (double) ((double) cyPage / RowHeight);
     SelectObject (hdc, (HFONT) DeleteObject (hFont));
/*
	 hFont = SetDeviceFont (hdc, &mFont, &tm);
     oldfont = SelectObject (hdc, hFont);
     GetTextExtentPoint32 (hdc, "X", 1, &size);
     SelectObject (hdc, oldfont);
     PageCols = (int) (double) ((double) cxPage / size.cx);
     SelectObject (hdc, (HFONT) DeleteObject (hFont));
*/
     return PageRows;
}


void LISTDLG::ScrPrintCaption (HDC hdc, TEXTMETRIC *printtm, int page)
{
     TEXTMETRIC capttm;
     char buffer [80];
     char datum [12];
     SIZE size;
     int x, y;

     HFONT hFont   = SetDeviceFont (hdc, &printfettfont, &capttm);
     HFONT oldfont = SelectObject (hdc, hFont);
     GetTextMetrics (hdc, &capttm); 
     GetTextExtentPoint32 (hdc, "X", 1, &size);
     capttm.tmAveCharWidth = size.cx;
     SetTextColor (hdc,BLACKCOL);
     ::SetBkColor (hdc,WHITECOL);
     y = capttm.tmHeight;
     x = capttm.tmAveCharWidth;

     sysdate (datum);
     sprintf (buffer, "Wochenplan vom ");
     TextOut (hdc, x, y, buffer, strlen (buffer));
     x = 15 * capttm.tmAveCharWidth;
     sprintf (buffer, "%s", datum);
     TextOut (hdc, x, y, buffer, strlen (buffer));

     x = 40 * capttm.tmAveCharWidth;
     sprintf (buffer, "Mandant");
     TextOut (hdc, x, y, buffer, strlen (buffer));

     x = 56 * capttm.tmAveCharWidth;
     sprintf (buffer, "%hd", _mdn.mdn);
     TextOut (hdc, x, y, buffer, strlen (buffer));

     x = 60 * capttm.tmAveCharWidth;
     work->GetMdnName ( buffer, _mdn.mdn);
     TextOut (hdc, x, y, buffer, strlen (buffer));

     sprintf (buffer, "Seite %d", page);
     GetTextExtentPoint32 (hdc, buffer, strlen (buffer), &size);
     x = GetDeviceCaps (hdc, HORZRES) - size.cx - 5;
     TextOut (hdc, x, y, buffer, strlen (buffer));

     y += (int) (double) ((double) capttm.tmHeight * 1.5);
     x = capttm.tmAveCharWidth;
     sprintf (buffer, "Kalenderwoche");
     TextOut (hdc, x, y, buffer, strlen (buffer));

     x = 15 * capttm.tmAveCharWidth;
     sprintf (buffer, "%hd", kw);
     TextOut (hdc, x, y, buffer, strlen (buffer));

     x = 40 * capttm.tmAveCharWidth;
     sprintf (buffer, "Abteilung");
     TextOut (hdc, x, y, buffer, strlen (buffer));

     x = 56 * capttm.tmAveCharWidth;
     sprintf (buffer, "%hd", work->GetAbt ());
     TextOut (hdc, x, y, buffer, strlen (buffer));

     x = 60 * capttm.tmAveCharWidth;
     work->GetAbtBez ( buffer, work->GetAbt ());
     TextOut (hdc, x, y, buffer, strlen (buffer));
     DeleteObject (SelectObject (hdc, oldfont));   
}

      
void LISTDLG::ScrPrintList (void)
{
        TEXTMETRIC printtm;
        TEXTMETRIC scrtm;
        SIZE size;
        int scrRowHeightP;
        int scrUbHeight;
        RECT rect;
        COLORREF BkCol;
        int page = 0;
        DOCINFO di = {sizeof (DOCINFO), "dbinfo", NULL};
        
/*
        if (QuerPrint)
        {
              SetQuerDefault (TRUE);
        }
*/


        if (SelectPrinter () == FALSE)
        {
            return;
        }


/*
        if (::SetPage () == FALSE)
        {
            return;
        }
*/

        BkCol = eListe.GetBkColor ();
        if (PrintBackground == FALSE)
        {
            eListe.SetBkColor (WHITECOL);
        }

        char *Printer = GetPrinterName ();
        if (Printer == NULL)
        {
            Printer = GetPrinterDevName ();
        }
        if (Printer == NULL)
        {
            return;
        }
//        memcpy (&printfont, eListe.GetAktFont (), sizeof (mfont));
//        memcpy (&printfettfont, eListe.GetAktFont (), sizeof (mfont));

        printfont.FontBkColor = WHITECOL;
        printfettfont.FontBkColor = WHITECOL;
        printfettfont.FontAttribute = 1;

        HDC hdc = GetPrinterbyName (Printer);
        if (hdc == NULL) return;
        eListe.PrintHdc = TRUE;
        eListe.ublen = 6;
        if (StartDoc (hdc, &di) == 0) return; 

   	    HFONT hFont   = SetDeviceFont (hdc, &printfont, &printtm);
        HFONT oldfont = SelectObject (hdc, hFont);

        rect.top = 0;
        rect.left = 0;
        rect.bottom = GetDeviceCaps (hdc, VERTRES);
        rect.right  = GetDeviceCaps (hdc, HORZRES);
        eListe.pagelen = GetPageRows (Printer, &printfont) - 2;

        eListe.pagelen0 = (int) (double) ((double) 
                    eListe.pagelen / RowHeight - ublen);
        eListe.pagelen0 /= eListe.GetRecHeight ();
        eListe.pagelen0 --;
        eListe.PageStdCols = GetPageStdCols (Printer);


	    GetTextExtentPoint32 (hdc, "X", 1, &size);
        printtm.tmAveCharWidth = size.cx;

        if (rect.bottom > (eListe.pagelen + 1) * printtm.tmHeight)
        {
            rect.bottom = (eListe.pagelen + 1) * printtm.tmHeight;
        }

        memcpy (&scrtm, eListe.GetListTextMetrics (),   sizeof (TEXTMETRIC)); 
        memcpy (eListe.GetListTextMetrics (), &printtm, sizeof (TEXTMETRIC)); 
        scrUbHeight = eListe.GetUbHeight ();
        eListe.SetUbHeight (scrUbHeight * printtm.tmHeight / scrtm.tmHeight);

        scrRowHeightP = eListe.GetRowHeightP (); 
		eListe.SetRowHeightP ((int) (double) ((double) printtm.tmHeight * RowHeight));

        eListe.printscrollpos = 0;
        eListe.PageStart = 0;
        while (eListe.printscrollpos < eListe.GetRecanz ())
        {
              eListe.PageEnd = eListe.PageStart + eListe.pagelen * printtm.tmHeight;
              if (StartPage (hdc) == 0) return;
              if (PrintBackground && (eListe.GetBkColor () != WHITECOL))
              {
                    HBRUSH hBrush = CreateSolidBrush (LTGRAYCOL);
                    SelectObject (hdc, hBrush);
                    SelectObject (hdc, GetStockObject (NULL_PEN));
                    Rectangle (hdc,0, 0, GetDeviceCaps (hdc, HORZRES), 
                                         GetDeviceCaps (hdc, VERTRES));
                    DeleteObject (hBrush); 
              }
              page ++;
              ScrPrintCaption (hdc, &printtm, page);
              eListe.FillUbPFrmRow (hdc, eListe.Getfrmrow (), &UbForm, 0);
              eListe.ShowPrintForms (hdc, 2);
              eListe.PrintPVlines (hdc, &rect);
              eListe.PrintPHlines (hdc, &rect);
              EndPage (hdc);
              eListe.printscrollpos += eListe.pagelen0;
              eListe.PageStart = eListe.PageEnd;
        }
        DeleteObject (SelectObject (hdc, oldfont));
    	EndDoc (hdc);
        memcpy (eListe.GetListTextMetrics (), &scrtm,   sizeof (TEXTMETRIC)); 
        eListe.SetRowHeightP (scrRowHeightP); 
        eListe.SetUbHeight (scrUbHeight);
        eListe.PrintHdc = FALSE;
        eListe.SetBkColor (BkCol);
        SetListFocus ();
}


void LISTDLG::PrintRezaufloesung (void)
//Liste drucken.
{
   char date[12];
   char sabt[12];
        GetAktDat (date);
		short abt = work->GetAbt();
        sprintf(sabt,"%hd",abt);
		strcpy(PrintCommand, "rezaufloesung ");
		strcat(PrintCommand, " -datvon ");
		strcat(PrintCommand, date);
		strcat(PrintCommand, " -datbis ");
		strcat(PrintCommand, date);
		strcat(PrintCommand, " -prodabt ");
		strcat(PrintCommand, sabt);
		ProcExec (PrintCommand, SW_SHOWNORMAL, -1, 0, -1, 0);  
}

void LISTDLG::PrintRezeinzel (void)
//Liste drucken.
{
   char date[12];
   char sabt[12];
        GetAktDat (date);
		short abt = work->GetAbt();
        sprintf(sabt,"%hd",abt);
		strcpy(PrintCommand, "rezaufloesung ");
		strcat(PrintCommand, " -datvon ");
		strcat(PrintCommand, date);
		strcat(PrintCommand, " -datbis ");
		strcat(PrintCommand, date);
		strcat(PrintCommand, " -prodabt ");
		strcat(PrintCommand, sabt);
		strcat(PrintCommand, " einzellisten");
		ProcExec (PrintCommand, SW_SHOWNORMAL, -1, 0, -1, 0);  
//   print_mess (1,"Der Einzelausdruck ist in dieser Version noch nicht realisiert");
}

void LISTDLG::PrintLabel (void)
//Liste drucken.
{
   char date[12];
   char sabt[12];
        GetAktDat (date);
		short abt = work->GetAbt();
        sprintf(sabt,"%hd",abt);
		strcpy(PrintCommand, "rezaufloesung ");
		strcat(PrintCommand, " -datvon ");
		strcat(PrintCommand, date);
		strcat(PrintCommand, " -datbis ");
		strcat(PrintCommand, date);
		strcat(PrintCommand, " -prodabt ");
		strcat(PrintCommand, sabt);
		strcat(PrintCommand, " labeldruck");
		ProcExec (PrintCommand, SW_SHOWNORMAL, -1, 0, -1, 0);  
}

void LISTDLG::PrintWoplan (void)
//Liste drucken.
{
   char buffer [50];
   char datvon[12];
   char datbis[12];
   char datTag[12];
   GetAktDat (datTag);
   work->GetAbtBez (buffer, work->GetAbt ());
   if (strlen (clipped(buffer) ) == 0) strcpy (buffer,".");

   dlong_to_asc(StartDay,datvon);
   GetLastDat (datbis) ;

//		strcpy(PrintCommand, "woplandruck ");
        sprintf (PrintCommand, "woplandruck "
			                    "-mdn %2d "
								"-prodabt %2d "
								"-abtbez %s "
								"-dat_von %s "
								"-dat_bis %s "
								"-datum %s "
								, _mdn.mdn
								, work->GetAbt ()
								, buffer
								, datvon
								, datbis
								, datTag
								 );
//        work->GetAbtBez (b, work->GetAbt ());
/*
        dlong_to_asc(StartDay,date);
		strcat(PrintCommand, " -datvon ");
		strcat(PrintCommand, date);
        GetLastDat (date);
		strcat(PrintCommand, " -datbis ");
		strcat(PrintCommand, date);
*/
		ProcExec (PrintCommand, SW_SHOWNORMAL, -1, 0, -1, 0);  
}



void LISTDLG::PrintList (void)
//Liste drucken.
{

	    char *tmp;
		char buffer [0x1000];
		char listname [256];
		char command  [512];
		char *p;
		FILE *fp;
		int anz;
		int i;
		int len;
		gPrintl Gprint;
//        DWORD ExitCode;


        if (ScrPrint)
        {
            ScrPrintList ();
            return;
        }

        pagelen = 60;
        PageStdCols = 80;
        Page = 0;

//
 //       if (QuerPrint)
//        {
//              SetQuerDefault (TRUE);
//        }
//        if (DrkDeviceFile != NULL)
//        {
//             SetDrkDeviceFile (DrkDeviceFile);
//        }
//        else
//        {
//             SetDrkDeviceFile (NULL);
//        }
//        if (RowPrintFont != 0)
//        {
//            SetRowPrintFont (RowPrintFont);
//        }
//

        if (SelectPrinter ())
        {
             pagelen  = ::GetPageRows (NULL) - 3;
             pagelen0 = pagelen;
             PageStdCols = GetPageStdCols (NULL);
//
//             if (BeforePrint != NULL)
//             {
//                 ChangePrintParams ();
//             }
//
        }
        else
        {
             return;
        }

        len = eListe.GetUbFrmlen (UbForm.fieldanz - 1);
        frmlen = len;
        ENG = GetRowWidthStz (len);
        if (PrintFont != 0)
        {
            ENG = PrintFont;
        }

  	    strcpy (listname, "lipreise.lst");
		p = strchr (listname, '.');
		if (p) *p = 0;

		tmp = getenv ("TMPPATH");
        if (tmp == NULL) 
		{
			sprintf (buffer, "%s.lst", listname);
		}
		else
		{
			sprintf (buffer, "%s\\%s.lst", tmp,listname);
		}

		fp = fopen (buffer, "w");
		if (fp == NULL)
		{
			print_mess (2, "Druckdatei kann nicht ge�ffnet werden");
			return;
		}

        PrintCaption (fp, buffer);
        anz = eListe.GetRecanz ();

        if (ENG > 0)
        {
                fprintf (fp, "%c%c", ESC, NORM);
        }
        if (FETT)
        {
                fprintf (fp, "%c%c", ESC, FETT2);
        }

        for (i = 0; i < anz; i ++)
		{
			if (row + 1 >= pagelen0)
			{
                    for (;row < pagelen0; row ++) 
                    {
                        fprintf (fp, "\n");
                    }

                    fprintf (fp, "%c%c", ESC, NORM);
		            PrintCaption (fp, buffer);
			}
            FromMemory (i);
            if (ENG > 0)
            {
                fprintf (fp, "%c%c", ESC, ENG);
            }
            if (FETT)
            {
                fprintf (fp, "%c%c", ESC, FETT1);
            }
            eListe.FillPrintRow (buffer, 0);
 			fprintf (fp, "%s\n", buffer);
            row ++;
            eListe.FillPrintRow (buffer, 1);
 			fprintf (fp, "%s\n", buffer);

            if (FETT)
            {
                fprintf (fp, "%c%c", ESC, FETT2);
            }

            if (ENG > 0)
            {
                fprintf (fp, "%c%c", ESC, NORM);
            }

			row ++;
		}
		fclose (fp);
//		GlobalFree (pline);

        if (tmp == NULL) 
		{
			sprintf (buffer, "%s.lst", listname);
		}
		else
		{
			sprintf (buffer, "%s\\%s.lst", tmp,listname);
		}

//
//        if (PrintFile != NULL)
//        {
//            strcpy (buffer, PrintFile);
//        }
//
	    sprintf (command, "-p%d %s", 1, buffer); 
		Gprint.Print (command, SW_SHOWNORMAL);
//        SetQuerDefault (FALSE);
        eListe.SetFeldFocus0 (eListe.GetAktRow (),eListe.GetAktColumn ());
}

void LISTDLG::CreateListQuikInfos (HWND hWnd)
{
       if (Toolbar2 == NULL) return;

       Toolbar2->GetCfield ("f5")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("f12")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("f9")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("del")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("ins")->CreateQuikInfos (hWnd);
}


BOOL LISTDLG::OnCommand (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    if (LOWORD (wParam) == IDM_CHOISE)
    {
        return OnKey9 ();
    }
    if (LOWORD (wParam) == IDM_CANCEL)
    {
        return OnKey5 ();
    }
    if (LOWORD (wParam) == IDM_WRITE)
    {
        return OnKey12 ();
    }
    if (LOWORD (wParam) == VK_DELETE)
    {
        return OnKeyDel ();
    }
    if (LOWORD (wParam) == IDM_DELETE)
    {
        return OnKeyDel ();
    }
    if (LOWORD (wParam) == IDM_PRINT)
    {

        WriteList (); //020204
		work->Upd99 ();
        if (work != NULL)
		{
				work->CommitWork ();
				work->BeginWork ();
		}
        int drk = WasWirdGedruckt (mamain1);
		if (drk == 0) PrintWoplan();
		else if (drk == 1) PrintRezaufloesung();
		else if (drk == 2) PrintRezeinzel();
		else if (drk == 3) PrintLabel();


/***
	    if (abfragejn (mamain1, "Rezepturaufl�sung drucken ?", "J"))
		{
			PrintRezaufloesung ();
        }
		else
		{
//	        PrintList ();
	        PrintWoplan ();
        }
*********/
	

        return TRUE;
    }
    if (LOWORD (wParam) == IDM_CHPRINT)
    {
		PrintRezaufloesung ();
        return TRUE;
    }

    if (LOWORD (wParam) == VK_INSERT)
    {
        return OnKeyInsert ();
    }
    if (LOWORD (wParam) == IDM_INSERT)
    {
        return OnKey6 ();
    }
    if (HIWORD (wParam)  == CBN_CLOSEUP)
	{
       if (Dlg != NULL)
	   {
	           return ((SpezDlg *) Dlg)->OnCloseUp (hWnd, msg, wParam, lParam);
	   }
	}
    return FALSE;
}

BOOL LISTDLG::OnPaint (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    HDC hdc;

    if (Modal == FALSE) return FALSE;
    if (this->mamain1 != hWnd) return FALSE; 
    
    hdc = BeginPaint (hWnd, &ps);
    fChoise.display (hWnd, hdc);
    EndPaint (hWnd,&ps);
    return FALSE;
}

BOOL LISTDLG::OnMove (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{

    if (Modal == FALSE) return FALSE;
    if (this->mamain1 != hWnd) return FALSE; 

    fChoise.MoveWindow ();
    return FALSE;
}


void LISTDLG::SetNoRecNr (void)
{
	   int i;

	   eListe.SetNoRecNr (TRUE);

       if (NoRecNrOK) return;
       if (dataform == NULL) return;   

	   for (i = 0; i < dataform->fieldanz; i ++)
	   {
		   dataform->mask[i].pos[1] -= 6;
	   }
	   for (i = 0; i < ubform->fieldanz; i ++)
	   {
		   ubform->mask[i].pos[1] -= 6;
	   }
	   for (i = 0; i < lineform->fieldanz; i ++)
	   {
		   lineform->mask[i].pos[1] -= 6;
	   }
       NoRecNrOK = TRUE;
}





BOOL LISTDLG::PruefLoesch (void)
{
         int p;

         if (work->GetMatrix () == BIG)
         {
             p = 0;
         }
         else if (work->GetMatrix () == SMALL)
         {
             p = 1;
         }

         for (int i = 1 + p; i < DataForm.fieldanz - 1; i ++)
         {

                    double me      = ratod (Lsts[i]->GetFeld ());

                    if (me > 0.0) return FALSE;
         }
		 work->ins_woplan_del(ratod (Lsts[0]->GetFeld () ));
         return TRUE;
}




//=========================================== 
//===========Auswahl Box zum Drucken ======== 
//=========================================== 
static int woplandruck       = 0;
static int rezaufl       = 0;
static int rezeinzel       = 0;
static int labeldruck       = 0;

static int *fakbuttons [] = {&woplandruck, &rezaufl, &rezeinzel, &labeldruck, NULL};

static int SetWoplandruck (void);
static int SetRezaufl (void);
static int SetRezeinzel (void);
static int SetLabel (void);
static int BreakBox (void);
static int BoxBreak (void);

static int IsFakChoise;

static ITEM iWoplandruck      ("",  "Ausdruck des Wochenplans",           "", 0);
static ITEM iRezaufl    ("",  "Rezepturaufl�sung",  "", 0);
static ITEM iRezEinzel   ("",  "Rezepturaufl�sung Einzel", "", 0);
static ITEM iLabeldruck   ("",  "Etiketten", "", 0);
static ITEM  iOK      ("", "    OK      ",  "", 0);
static ITEM  iCancel  ("", "  Abbruch   ",  "", 0);

static int SetFakDef (void);

static field _fakbox [] = {
&iWoplandruck,                   36, 2, 1,  6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             SetWoplandruck, 101,
&iRezaufl,                   36, 2, 3,  6, 0, "", BUTTON | CHECKBUTTON, 0,
                                              SetRezaufl, 102,
&iRezEinzel,                   36, 2, 5,  6, 0, "", BUTTON | CHECKBUTTON, 0,
                                              SetRezeinzel, 102,
&iLabeldruck,                   36, 2, 7,  6, 0, "", BUTTON | CHECKBUTTON, 0,
                                              SetLabel, 102,
&iOK,                    15, 0, 9,  6,  0, "", BUTTON, 0, BreakBox, 107,
&iCancel,                15, 0, 9, 23, 0, "",  BUTTON, 0, BoxBreak, KEY5};

form fakbox = {6, 0, 0, _fakbox, 0, SetFakDef, 0, 0, NULL};

int BreakBox (void)
/**
EnterButton abbrechen.
**/
{
          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;
          syskey = KEYCR;
          break_enter ();
          return 1;
}

int BoxBreak (void)
/**
EnterButton abbrechen.
**/
{
	      int savecurrent;
//		  HWND hWndA;
//		  HWND hWndB;

		  savecurrent = currentfield;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;


          switch (syskey)
		  {
                case KEY5 :
                       break_enter ();
                       return 1;
                case KEY11 :
                case KEY12 :
                       syskey = KEY12;
                       break_enter ();
                       return 1;
				case KEY7 :
/*****
                       hWndA = GetActiveWindow ();
					   hWndB = AktivWindow;
  					   SetCurrentField (0);
					   KompAuf::ChoisePrinter0 ();
					   AktivWindow = hWndB;
					   SetActiveWindow (hWndA);
					   EnableWindow (GetParent (AktivWindow), FALSE);
 		               SetButtonTab (TRUE);
					   SetCurrentField (savecurrent);
					   SetCurrentFocus (savecurrent);
********/
					   return 1;
		  }

          if (current_form->mask[currentfield].BuId == KEY7)
		  {
			  /*******
					syskey = KEY7;
                    hWndA = GetActiveWindow ();
			        hWndB = AktivWindow;
 					SetCurrentField (0);
					KompAuf::ChoisePrinter0 ();
				    AktivWindow = hWndB;
					SetActiveWindow (hWndA);
					EnableWindow (GetParent (AktivWindow), FALSE);
	                SetButtonTab (TRUE);
 					SetCurrentField (savecurrent);
					SetCurrentFocus (savecurrent);
					***********/
					return 1;
		  }
		  else if (current_form->mask[currentfield].BuId == KEY5)
		  {
					syskey = KEY5;
					break_enter ();
					return 1;
		  }
		  else if (current_form->mask[currentfield].BuId == 107)
		  {
					syskey = KEY12;
					break_enter ();
					return 1;
		  }

          return 1;
}


void UnsetBox (int pos)
/**
Button auf selectierte setzen. (ausser pos);
**/
{
/****
        int i;
        for (i = 0; aufbuttons [i]; i ++)
        {
                  if (i == pos) continue;

                  SendMessage (cbox.mask[i].feldid, BM_SETCHECK, 0, 0l);
                  *aufbuttons [i] = 0;
        }
		********/
}
int SetFakDef (void)

/**
Uebergabe an Lieferschein setzen.
**/
{
          woplandruck = 1;
          SendMessage (fakbox.mask[0].feldid, BM_SETCHECK,
                       woplandruck, 0l);
          SetFocus (fakbox.mask[0].feldid);
          SetCurrentField (0);
          UnsetBox (0);
          return 1;
}


int FakEnd (void)
/**
EnterButton abbrechen.
**/
{
           if (syskey == KEY5)
           {
                    break_enter ();
           }
           return 1;
}

void UnsetFakBox (int pos)
/**
Button auf selectierte setzen. (ausser pos);
**/
{
        int i;

        for (i = 0; fakbuttons [i]; i ++)
        {
                  if (i == pos) continue;

                  SendMessage (fakbox.mask[i].feldid, BM_SETCHECK, 0, 0l);
                  *fakbuttons [i] = 0;
        }
}


int SetWoplandruck (void)
/**
K-Liste setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (fakbox.mask[0].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       woplandruck = 0;
          }
          else
          {
                       woplandruck = 1;
          }
          if (syskey == KEYCR)
          {
                       woplandruck = 1;
                       SendMessage (fakbox.mask[0].feldid, BM_SETCHECK,
                       woplandruck, 0l);
                       break_enter ();
          }
          UnsetFakBox (0);
          return 1;
}

int SetRezaufl (void)
/**
Uebergabe und Druck setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (fakbox.mask[1].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                        rezaufl = 0;
          }
          else
          {
                        rezaufl  = 1;
          }
          if (syskey == KEYCR)
          {
                       rezaufl  = 1;
                       SendMessage (fakbox.mask[1].feldid, BM_SETCHECK,
                       rezaufl, 0l);
                       break_enter ();
          }
          UnsetFakBox (1);
          return 1;
}

int SetRezeinzel (void)
/**
Uebergabe und Druck setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (fakbox.mask[2].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                        rezeinzel = 0;
          }
          else
          {
                        rezeinzel  = 1;
          }
          if (syskey == KEYCR)
          {
                       rezeinzel  = 1;
                       SendMessage (fakbox.mask[2].feldid, BM_SETCHECK,
                       rezeinzel, 0l);
                       break_enter ();
          }
          UnsetFakBox (2);
          return 1;
}

int SetLabel (void)
/**
Uebergabe und Druck setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (fakbox.mask[3].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                        labeldruck = 0;
          }
          else
          {
                        labeldruck  = 1;
          }
          if (syskey == KEYCR)
          {
                       labeldruck  = 1;
                       SendMessage (fakbox.mask[3].feldid, BM_SETCHECK,
                       labeldruck, 0l);
                       break_enter ();
          }
          UnsetFakBox (3);
          return 1;
}

void SelectFakCtr (void)
/**
Aktiven Button in EnterButton selectieren.
**/
{
        syskey = KEYCR;
        if (fakbox.mask[currentfield].after)
        {
                       (*fakbox.mask[currentfield].after) ();
        }
        syskey = KEYCR;
}


int KomplettEnd (void)
/**
EnterButton abbrechen.
**/
{
           if (syskey == KEY5)
           {
                    break_enter ();
           }
           return 1;
}

int LISTDLG::WasWirdGedruckt (HWND hWnd)
/**
Komplett setzen.
**/
{

         save_fkt (5);
         save_fkt (7);
		 set_fkt (NULL, 7);
         set_fkt (KomplettEnd, 5);
		 int savecurrent;
		 int saveSyskey = syskey;


		 savecurrent = currentfield;
         woplandruck = rezaufl = rezeinzel = labeldruck = 0;
         woplandruck = 1;
         EnableWindows (hWnd, FALSE);
         SetBorder (WS_POPUP | WS_VISIBLE | WS_CAPTION | WS_DLGFRAME);
         SetCaption ("Druck-Auswahl");
         if (EnterButtonWindow (hWnd, &fakbox, 14, 10, 11, 62) == -1)
         {
			            SetActiveWindow (hWnd);
                        SetAktivWindow (hWnd);
                        EnableWindows (hWnd, TRUE);
                        restore_fkt (5);
		                currentfield = savecurrent;
						syskey = saveSyskey;
                        return -1;
         }

         SetActiveWindow (hWnd);
         SetAktivWindow (hWnd);
         EnableWindows (hWnd, TRUE);
         restore_fkt (5);
         restore_fkt (7);
		 currentfield = savecurrent;
		 if (woplandruck)
		 {
                return 0;
		 }
		 else if (rezaufl)
		 {
                return 1;
		 }
		 else if (rezeinzel)
		 {
                return 2;
		 }
		 else if (labeldruck)
		 {
                return 3;
		 }
		 return 0;
}


