#ifndef _LISTDLG_DEF
#define _LISTDLG_DEF
#include "listenter.h"
#include "listclex.h"
#include "work.h"
#include "mo_progcfg.h"

 
class LISTDLG : public virtual LISTENTER, 
                public virtual KEYS,
                public virtual Info
{
         private :
                      static CFIELD *_fChoise0[];
                      static CFORM fChoise0;
                      static CFIELD *_fChoise[];
                      static CFORM fChoise;

                      static struct CHATTR ChAttra [];
                      static struct CHATTR *ChAttr;
                      int cursor;
                      CFORM *Toolbar2;
                      BOOL cfgOK;
                      PROG_CFG *ProgCfg;
                      HBITMAP ArrDown;
                      static BOOL NoRecNrOK;
                      int pagelen;
                      int pagelen0;
                      int ublen;
                      int frmlen;
                      int PageStdCols;
                      int Page;
                      int PrintFont;
                      int RowPrintFont;
                      BOOL UsePrintForm;
                      BOOL QuerPrint;
                      char *DrkDeviceFile;
                      BOOL PrintBackground;
                      long StartDay;
                      static short kw;
                      static short jr;

         public :
				     int  WasWirdGedruckt (HWND hWnd);
	                 static Work *work;
                     static char *Rec;
                     static int RecLen;
                     static CFELD **Lsts;
                     static char *LstTab[];
                     static ITEM **ufelder;
                     static ITEM ufiller;
                     static field *_UbForm;
                     static form UbForm;

                     static ITEM iline;
                     static field *_LineForm;
                     static form LineForm;

                     static field *_DataForm;
                     static form DataForm;
                     static int *ubrows;
                     void SetButtonSize (int);
                     static BOOL ScrPrint; 
                     static int GewLen;
                     static void ScrPrintCaption (HDC, TEXTMETRIC *, int);
		 			static char *PrintCommand;

                     void SetToolbar2 (CFORM *Toolbar2)
                     {
                               this->Toolbar2 = Toolbar2;
                     }
 
                     CFORM *GetToolbar2 (void)
                     {
                               return Toolbar2;
                     }

                     void SetWork (Work *w)
                     {
                         work = w;
                         work->SetListDlg (this);
                     }

                     Work *GetWork (void)
                     {
                         return work;
                     }

                     LISTDLG (short, short, short, short);
					 ~LISTDLG ();
                     char *GetAktDat (char *);
                     char *GetLastDat (char *);
//                     void GenWoForms (short);
                     void GenWoFormsOne (short);
                     void GenWoFormsOneMengen (short);
                     void DeleteForms (void);
                     void GetSysPar (void);
                     void GetCfgValues (void);
                     void Setfeld2Removed (BOOL);
                     void Prepare (void);
                     void Open (void);
                     int Fetch (void);
                     int FetchA (void);
                     void Close (void);
                     double GetA (field *);
                     char * GetLief (field *);
                     
 				     unsigned char *GetDataRec (void);
				     int GetDataRecLen (void);
 				     char *GetListTab (int);
                     BOOL WriteList (void);
                     int WriteRec (int);
                     double RechneSumme (void);
                     short HoleMaxPosi (double);
                     void InitRow (void);
                     int ToMemory (int);
                     void uebertragen (void);
                     void PrintCaption (FILE *, char *);
                     void PrintList (void);
                     void PrintRezaufloesung (void);
                     void PrintRezeinzel (void);
                     void PrintLabel (void);
                     void PrintWoplan (void);
                     int  GetPageRows (char *, mfont *);
                     void ScrPrintList (void);
                     void UpdateList ();

                     BOOL OnKey5 (void);
                     BOOL OnKey6 (void);
                     BOOL OnKey7 (void);
                     BOOL OnKey8 (void);
                     BOOL OnKey9 (void);
                     BOOL OnKey10 (void);
                     BOOL OnKey11 (void);
                     BOOL OnKey12 (void);
                     BOOL OnKeyDel (void);
                     BOOL OnKeyInsert (void);
                     BOOL OnKeyUp (void);
                     BOOL OnKeyDown (void);
                     BOOL OnKeyNext (void);
                     BOOL OnKeyPrior (void);
		             BOOL OnKeyReturn (void);

                     BOOL OnCommand (HWND, UINT, WPARAM, LPARAM);
                     BOOL OnPaint (HWND, UINT, WPARAM, LPARAM);
                     BOOL OnMove (HWND, UINT, WPARAM, LPARAM);
                     int AfterRow (int);
                     int AfterCol (char *);
                     int BeforeCol (char *);
                     BOOL PruefLoesch (void);
                     int BeforeRow (int);
                     void CreateListQuikInfos (HWND);
                     void TestRemoves (void);
                     BOOL StopList (void);
                     void EnterZusatz (char*, double, short, double, int);
                     void SetNoRecNr (void);
                     static int InfoProc (char **, char *, char *);
                     static int TestAppend (void);

                     BOOL CallInfo (HWND, RECT *, char *, char *, DWORD);
};
#endif