#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "wmaskc.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "prdk_huel.h"

struct PRDK_HUEL prdk_huel, prdk_huel_null;

void PRDK_HUEL_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *)   &prdk_huel.mdn, 1, 0);
            ins_quest ((char *)   &prdk_huel.a,  3, 0);
            ins_quest ((char *)   &prdk_huel.mat,  2, 0);
            ins_quest ((char *)   &prdk_huel.variante,  1, 0);
    out_quest ((char *) &prdk_huel.mdn,1,0);
    out_quest ((char *) &prdk_huel.a,3,0);
    out_quest ((char *) &prdk_huel.mat,2,0);
    out_quest ((char *) &prdk_huel.bearb_flg,1,0);
    out_quest ((char *) prdk_huel.bearb_info,0,49);
    out_quest ((char *) &prdk_huel.me,3,0);
    out_quest ((char *) &prdk_huel.me_einh,1,0);
    out_quest ((char *) &prdk_huel.fuel_gew,3,0);
    out_quest ((char *) &prdk_huel.gew,3,0);
    out_quest ((char *) &prdk_huel.variante,1,0);
    out_quest ((char *) &prdk_huel.typ,1,0);
    out_quest ((char *) prdk_huel.me_buch,0,2);
            cursor = prepare_sql ("select prdk_huel.mdn,  "
"prdk_huel.a,  prdk_huel.mat,  prdk_huel.bearb_flg,  "
"prdk_huel.bearb_info,  prdk_huel.me,  prdk_huel.me_einh,  "
"prdk_huel.fuel_gew,  prdk_huel.gew,  prdk_huel.variante,  "
"prdk_huel.typ,  prdk_huel.me_buch from prdk_huel "

#line 23 "prdk_huel.rpp"
                                  "where mdn = ? "
                                  "and   a   = ? "
                                  "and   mat = ? "
                                  "and variante = ?");
    ins_quest ((char *) &prdk_huel.mdn,1,0);
    ins_quest ((char *) &prdk_huel.a,3,0);
    ins_quest ((char *) &prdk_huel.mat,2,0);
    ins_quest ((char *) &prdk_huel.bearb_flg,1,0);
    ins_quest ((char *) prdk_huel.bearb_info,0,49);
    ins_quest ((char *) &prdk_huel.me,3,0);
    ins_quest ((char *) &prdk_huel.me_einh,1,0);
    ins_quest ((char *) &prdk_huel.fuel_gew,3,0);
    ins_quest ((char *) &prdk_huel.gew,3,0);
    ins_quest ((char *) &prdk_huel.variante,1,0);
    ins_quest ((char *) &prdk_huel.typ,1,0);
    ins_quest ((char *) prdk_huel.me_buch,0,2);
            sqltext = "update prdk_huel set "
"prdk_huel.mdn = ?,  prdk_huel.a = ?,  prdk_huel.mat = ?,  "
"prdk_huel.bearb_flg = ?,  prdk_huel.bearb_info = ?,  "
"prdk_huel.me = ?,  prdk_huel.me_einh = ?,  prdk_huel.fuel_gew = ?,  "
"prdk_huel.gew = ?,  prdk_huel.variante = ?,  prdk_huel.typ = ?,  "
"prdk_huel.me_buch = ? "

#line 28 "prdk_huel.rpp"
                                  "where mdn = ? "
                                  "and   a   = ? "
                                  "and mat = ? "
                                  "and variante = ?";
            ins_quest ((char *)   &prdk_huel.mdn, 1, 0);
            ins_quest ((char *)   &prdk_huel.a,  3, 0);
            ins_quest ((char *)   &prdk_huel.mat,  2, 0);
            ins_quest ((char *)   &prdk_huel.variante,  1, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &prdk_huel.mdn, 1, 0);
            ins_quest ((char *)   &prdk_huel.a,  3, 0);
            ins_quest ((char *)   &prdk_huel.mat,  2, 0);
            ins_quest ((char *)   &prdk_huel.variante,  1, 0);
            test_upd_cursor = prepare_sql ("select a from prdk_huel "
                                  "where mdn = ? "
                                  "and   a = ? "
                                  "and mat = ? "
                                  "and variante = ? "
                                  "for update");

            ins_quest ((char *) &prdk_huel.mdn, 1, 0);
            ins_quest ((char *)   &prdk_huel.a,  3, 0);
            ins_quest ((char *)   &prdk_huel.mat,  2, 0);
            ins_quest ((char *)   &prdk_huel.variante,  1, 0);
            del_cursor = prepare_sql ("delete from prdk_huel "
                                  "where mdn = ? "
                                  "and   a = ? "
                                  "and mat = ? "
                                  "and variante = ?");
    ins_quest ((char *) &prdk_huel.mdn,1,0);
    ins_quest ((char *) &prdk_huel.a,3,0);
    ins_quest ((char *) &prdk_huel.mat,2,0);
    ins_quest ((char *) &prdk_huel.bearb_flg,1,0);
    ins_quest ((char *) prdk_huel.bearb_info,0,49);
    ins_quest ((char *) &prdk_huel.me,3,0);
    ins_quest ((char *) &prdk_huel.me_einh,1,0);
    ins_quest ((char *) &prdk_huel.fuel_gew,3,0);
    ins_quest ((char *) &prdk_huel.gew,3,0);
    ins_quest ((char *) &prdk_huel.variante,1,0);
    ins_quest ((char *) &prdk_huel.typ,1,0);
    ins_quest ((char *) prdk_huel.me_buch,0,2);
            ins_cursor = prepare_sql ("insert into prdk_huel ("
"mdn,  a,  mat,  bearb_flg,  bearb_info,  me,  me_einh,  fuel_gew,  gew,  variante,  typ,  "
"me_buch) "

#line 59 "prdk_huel.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?)"); 

#line 61 "prdk_huel.rpp"
}
