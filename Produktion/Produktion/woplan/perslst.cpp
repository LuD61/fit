#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include <string.h>
#include "comcthlp.h"
#include "itemc.h"
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_menu.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_intp.h"
#include "inflib.h"
#include "spezdlg.h"
#include "enterfunc.h"
#include "PersLst.h"
#include "spezdlg.h"
#include "maschinen.h"
#include "searchmasch.h"
#include "datum.h"

#define IDM_CANCEL 3001
#ifndef IDM_WRITE
#define IDM_WRITE  3002
#endif

int Kw,Jr,MDN;
char AktPersBz[26];
char AktMaschNr[26];
char AktMaschBz[26];
double akt_masch_me = 0.0;
static int StdSize = STDSIZE;

static int dlgsize    = STDSIZE;
static int ltgraysize = STDSIZE;
static int dlgpossize = STDSIZE;
static int cpsize     = 200;


static mfont dlgfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         GRAYCOL,
                         0,
                         NULL};


static ColButton Ctxt    = { "Text",  10, -1, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, -1, -1,
                             NULL,  0, 0,
                             BLACKCOL,
                             LTGRAYCOL,
                             0, 
};

static ColButton DefCol = {NULL,    -1, 0, 
                           NULL,    -1, 1,
                           NULL,  0,  0,
                           NULL,  0,  0,
                           NULL,  0,  0,
                           BLACKCOL,
                           LTGRAYCOL,
                           2,
                           NULL,
                           NULL,
                           TRUE,
};

char *PERSLST::Rec;
int PERSLST::RecLen;


//struct PERSLSTS  PERSLST::Lsts;
//struct PERSLSTS *PERSLST::LstTab;
CFELD **PERSLST::PersLsts;
char *PERSLST::LstTabPers[0x10000];
char *PERSLST::InfoItem = "a";
Work *PERSLST::work = NULL;
int PERSLST::PersLen = 18;


ITEM **PERSLST::ufelder;
ITEM PERSLST::ufiller         ("",               " ",               "",0);

ITEM PERSLST::iline ("", "1", "", 0);

field *PERSLST::_UbForm;


form PERSLST::UbForm = {6, 0, 0, _UbForm, 0, 0, 0, 0, NULL};

field *PERSLST::_LineForm;

form PERSLST::LineForm = {4, 0, 0, _LineForm,0, 0, 0, 0, NULL};

field *PERSLST::_DataForm;

form PERSLST::DataForm = {5, 0, 0, _DataForm,0, 0, 0, 0, NULL};


int *PERSLST::ubrows;





CFIELD *PERSLST::_fChoise0 [] = {
    new CFIELD ("cancel", " abbrechen ", 10, 0, 0, 0, NULL, "",
                CBUTTON, 
                IDM_CANCEL, &dlgfont, 0, 0),
    new CFIELD ("write", " speichern ", 10, 0,12, 0, NULL, "",
                CBUTTON, 
                IDM_WRITE, &dlgfont, 0, 0),
    new CFIELD ("show",  "  ansehen  ", 10, 0,24, 0, NULL, "",
                CBUTTON, 
                IDM_CHOISE, &dlgfont, 0, 0),
    new CFIELD ("delete","  l�schen  ", 10, 0,36, 0, NULL, "",
                CBUTTON, 
                IDM_DELETE, &dlgfont, 0, 0),
    new CFIELD ("insert"," einf�gen  ", 10, 0,48, 0, NULL, "",
                CBUTTON, 
                IDM_INSERT, &dlgfont, 0, 0),
    NULL,
};

CFORM PERSLST::fChoise0 (5, _fChoise0);

CFIELD *PERSLST::_fChoise [] = {
    new CFIELD ("fChoise0", (char *) &fChoise0, 0, 0, -1 , -2, NULL, "",
                CFFORM, 
                500, &dlgfont, 0, 0),
    NULL,
};

CFORM PERSLST::fChoise (1, _fChoise);

/*

ITEM PERSLST::umasch             ("masch",             "Maschine",          "", 0);
ITEM PERSLST::uprodabt           ("prodabt",           "Prod.Abt.",          "", 0);
ITEM PERSLST::umontag            ("montag",            "Montag",          "", 0);
ITEM PERSLST::udienstag          ("dienstag",          "Dienstag",          "", 0);
ITEM PERSLST::umittwoch          ("mittwoch",          "Mittwoch",          "", 0);
ITEM PERSLST::udonnerstag        ("donnerstag",        "Donnerstag",         "", 0);
ITEM PERSLST::ufreitag           ("freitag",           "Freitag",          "", 0);
ITEM PERSLST::usamstag           ("samstag",           "Samstag",          "", 0);
ITEM PERSLST::ufiller             ("",                   " ",                "",0);

ITEM *PERSLST::PersTab[] = {&umontag,
                              &udienstag,
                              &umittwoch,
                              &udonnerstag,
                              &ufreitag,
                              &usamstag,
                              NULL,
};

ITEM PERSLST::iline ("", "1", "", 0);

ITEM PERSLST::imasch             ("masch_nr",      "Lsts.masch_nr",          "", 0);
ITEM PERSLST::iprodabt           ("prodabt",       "Lsts.prodabt.",          "", 0);
ITEM PERSLST::imo_1              ("mo_1",          "Lsts.mo_1",          "", 0);
ITEM PERSLST::imo_2              ("mo_2",          "Lsts.mo_2",          "", 0);
ITEM PERSLST::imo_3              ("mo_3",          "Lsts.mo_3",          "", 0);
ITEM PERSLST::imo_4              ("mo_4",          "Lsts.mo_4",          "", 0);

ITEM PERSLST::idi_1              ("di_1",          "Lsts.di_1",          "", 0);
ITEM PERSLST::idi_2              ("di_2",          "Lsts.di_2",          "", 0);
ITEM PERSLST::idi_3              ("di_3",          "Lsts.di_3",          "", 0);
ITEM PERSLST::idi_4              ("di_4",          "Lsts.di_4",          "", 0);

ITEM PERSLST::imi_1              ("mi_1",          "Lsts.mi_1",          "", 0);
ITEM PERSLST::imi_2              ("mi_2",          "Lsts.mi_2",          "", 0);
ITEM PERSLST::imi_3              ("mi_3",          "Lsts.mi_3",          "", 0);
ITEM PERSLST::imi_4              ("mi_4",          "Lsts.mi_4",          "", 0);

ITEM PERSLST::ido_1              ("do_1",          "Lsts.do_1",          "", 0);
ITEM PERSLST::ido_2              ("do_2",          "Lsts.do_2",          "", 0);
ITEM PERSLST::ido_3              ("do_3",          "Lsts.do_3",          "", 0);
ITEM PERSLST::ido_4              ("do_4",          "Lsts.do_4",          "", 0);

ITEM PERSLST::ifr_1              ("fr_1",          "Lsts.fr_1",          "", 0);
ITEM PERSLST::ifr_2              ("fr_2",          "Lsts.fr_2",          "", 0);
ITEM PERSLST::ifr_3              ("fr_3",          "Lsts.fr_3",          "", 0);
ITEM PERSLST::ifr_4              ("fr_4",          "Lsts.fr_4",          "", 0);

ITEM PERSLST::isa_1              ("sa_1",          "Lsts.sa_1",          "", 0);
ITEM PERSLST::isa_2              ("sa_2",          "Lsts.sa_2",          "", 0);
ITEM PERSLST::isa_3              ("sa_3",          "Lsts.sa_3",          "", 0);
ITEM PERSLST::isa_4              ("sa_4",          "Lsts.sa_4",          "", 0);

*/





/*
field PERSLST::_UbForm [] = {
&umasch,       17, 0, 0,  6,  NULL, "", BUTTON, 0, 0, 0,     
&uprodabt,   21, 0, 0, 23,  NULL, "", BUTTON, 0, 0, 0, 
&umontag,  10, 0, 0, 74,  NULL, "", BUTTON, 0, 0, 0, 
&udienstag,  10, 0, 0, 84,  NULL, "", BUTTON, 0, 0, 0, 
&umittwoch,  10, 0, 0, 94,  NULL, "", BUTTON, 0, 0, 0, 
&udonnerstag,  10, 0, 0,104,  NULL, "", BUTTON, 0, 0, 0, 
&ufreitag,  10, 0, 0,114,  NULL, "", BUTTON, 0, 0, 0, 
&usamstag,  10, 0, 0,124,  NULL, "", BUTTON, 0, 0, 0, 
&ufiller, 120, 0, 0,174,  NULL, "", BUTTON, 0, 0, 0,
};


form PERSLST::UbForm = {9, 0, 0, _UbForm, 0, 0, 0, 0, NULL};
int PERSLST::UbAnz = 9;

field PERSLST::_LineForm [] = {
&iline,   1, 0, 0, 23,  NULL, "",  DISPLAYONLY, 0, 0, 0, 
&iline,   1, 0, 0, 44,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0, 59,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0, 74,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0, 84,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0, 94,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,104,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,114,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,124,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,134,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
};

form PERSLST::LineForm = {10, 0, 0, _LineForm,0, 0, 0, 0, NULL};
int PERSLST::LineAnz = 10;

field PERSLST::_DataForm [] = {
&imasch,       15, 0, 0,  7,  NULL, "%13.0f", DISPLAYONLY, 0, 0, 0,     
&iprodabt,   19, 0, 0, 24,  NULL, "",       DISPLAYONLY, 0, 0, 0, 
&imo_1,   9, 0, 0, 75,  NULL, "%8dn",    EDIT,        0, 0, 0, 
&imo_2,   9, 0, 1, 75,  NULL, "%8dn",    EDIT,        0, 0, 0, 
&imo_3,   9, 0, 2, 75,  NULL, "%8dn",    EDIT,        0, 0, 0, 
&imo_4,   9, 0, 3, 75,  NULL, "%8dn",    EDIT,        0, 0, 0, 
&idi_1,   9, 0, 0, 85,  NULL, "%8dn",    EDIT,        0, 0, 0, 
&imi_1,   9, 0, 0, 95,  NULL, "%8dn",    EDIT,        0, 0, 0, 
&ido_1,   9, 0, 0,105,  NULL, "%8dn",    EDIT,        0, 0, 0, 
&ifr_1,   9, 0, 0,115,  NULL, "%8dn",    EDIT,        0, 0, 0, 
&isa_1,   9, 0, 0,125,  NULL, "%8dn",    EDIT,        0, 0, 0, 
};


form PERSLST::DataForm = {11, 0, 0, _DataForm,0, 0, 0, 0, NULL};
int PERSLST::DataAnz = 11;

int PERSLST::FirstPhasePos = 4;

int PERSLST::ubrows [] = {0, 
                          1,
                          2,
                          3, 
                          4,
                          5,
                          6,
                          7,
                          8,
                         -1,
};

  *****/

struct CHATTR PERSLST::ChAttra [] = {
                                      "chmasch_nr",   REMOVED,BUTTON,                
//                                      "nr",     DISPLAYONLY,COMBOBOX,                
                                      NULL,  0,           0};

struct CHATTR *PERSLST::ChAttr = ChAttra;
int PERSLST::NoRecNrSet = FALSE;




void PERSLST::ReadProdPhasen (void)
{
	/*
    int prpanz = work->GetProdPhasen (MaschTab);
	prpanz --;
	if (prpanz <= 0)
	{
		prpanz = 4;
	}
    int diff = max (0, 10 - prpanz);
    ubform->fieldanz   = UbAnz - diff;
    lineform->fieldanz = LineAnz - (diff - 1);
    dataform->fieldanz = DataAnz - (diff - 1);
	*/
}

    
void PERSLST::CallInfoEx (void)
/*
Info wird aus Dialog aufgerufen.
*/ 

{
//   char where [512];
   
//   sprintf (where, "where masch_nr = %.0lf", Lsts.masch_nr);
//   _CallInfoEx (hMainWindow, NULL, InfoItem, where, 0l); 
}

void PERSLST::SetButtonSize (int Size)
{
    int fpos;

    fpos = GetItemPos (&DataForm, "chmasch_nr");
    if (fpos < 0) return;

    if (Size == SMALL)
    {
         DataForm.mask [fpos].length = 2;
         DataForm.mask [fpos].rows = 1;
    }
    else 
    {
         DataForm.mask [fpos].length = 3;
         DataForm.mask [fpos].rows = 0;
    }
}


void PERSLST::GetCfgValues (void)
{
       char cfg_v [512];
       COLORREF Color;

       if (ProgCfg == NULL) return;
	   if (cfgOK) return;

       cfgOK = TRUE;
       if (ProgCfg->GetCfgValue ("3DList", cfg_v) == TRUE)
       {
                    eListe.Set3D (atoi (cfg_v));
	   }
       if (ProgCfg->GetCfgValue ("3DSize", cfg_v) == TRUE)
       {
                    eListe.SetPlus3D (atoi (cfg_v));
	   }
       if (ProgCfg->GetCfgValue ("ListFocus", cfg_v) == TRUE)
       {
                    ListFocus = atoi (cfg_v);
	   }
        if (ProgCfg->GetCfgValue ("rowheight", cfg_v) == TRUE)
		{
			        RowHeight = ratod (cfg_v);
					if (RowHeight < (double) 1.0)
					{
						RowHeight = (double) 1.0;
					}
		}
        if (ProgCfg->GetCfgValue ("ubheight", cfg_v) == TRUE)
		{
			        UbHeight = atoi (cfg_v);
					SetUbHeight ();
		}
		eListe.SetRowHeight (RowHeight);


        if (ProgCfg->GetCfgValue ("NoRecNr", cfg_v) == TRUE)
        {
			        NoRecNr = atoi (cfg_v);
		            if (atoi (cfg_v))
					{
	                        SetNoRecNr ();
					}
		}
        if (ProgCfg->GetCfgValue ("ListColor", cfg_v) == TRUE)
        {
                     GetListColor (&Color, cfg_v);
		             eListe.SetColor (Color);
        }
        if (ProgCfg->GetCfgValue ("ListBkColor", cfg_v) == TRUE)
        {
                     GetListColor (&Color, cfg_v);
		             eListe.SetBkColor (Color);
        }
        if (ProgCfg->GetCfgValue ("ListLine", cfg_v) == TRUE)
        {
		             eListe.SetLines (GetListLine (cfg_v));
        }
        if (ProgCfg->GetCfgValue ("VarbInfo", cfg_v) == TRUE)
        {
                     InfoItem = new char [80];
                     strcpy (InfoItem, cfg_v);
        }
}




void PERSLST::SetNoRecNr (void)
{
	   int i;


	   eListe.SetNoRecNr (TRUE);
	   if (NoRecNrSet) return;

	   for (i = 0; i < dataform->fieldanz; i ++)
	   {
		   dataform->mask[i].pos[1] -= 6;
	   }
	   for (i = 0; i < ubform->fieldanz; i ++)
	   {
		   ubform->mask[i].pos[1] -= 6;
	   }
	   for (i = 0; i < lineform->fieldanz; i ++)
	   {
		   lineform->mask[i].pos[1] -= 6;
	   }
       NoRecNrSet = TRUE;
}


long PERSLST::GetAktPersNr (void)
{
    int Col = eListe.GetAktColumn ();
    return atol (DataForm.mask[Col].item->GetFeldPtr ());
}
    

    
PERSLST::PERSLST (short mdn, short kw, short jr) : KEYS (), LISTENTER () 
{

    ProgCfg = new PROG_CFG ("woplan");
	Dlg = NULL;
    cfgOK = FALSE;
	cursor = -1;
    prdk_k.mdn = mdn;
    prstatatag.mdn = mdn;
	Kw = kw;
	MDN = mdn;
	Jr = jr;

//    LstTab = new PERSLSTS [0x10000];
    GenWoFormsPers (mdn,kw,jr);

    dataform  = &DataForm;
    ubform    = &UbForm;
    lineform  = &LineForm;
    ReadProdPhasen ();
    eListe.SetUbRows (ubrows); 
	eListe.SetKeys (this);

    if ((work.ArtScroll == FALSE))
    {

           eListe.SetHscrollStart (4);
           UbForm.ScrollStart = 4;
    }

    LISTENTER::GetCfgValues ();
    GetCfgValues ();
//    TestRemoves ();
    SetNoRecNr ();
	fChoise.Setchpos (0, 2);
    work = NULL;
    ChAttr = ChAttra;
    eListe.SetChAttr (ChAttr); 
    Toolbar2 = NULL;
    eListe.SetTestAppend (TestAppend);
//    eListe.SetInfoProc (InfoProc);
    eListe.SetInfo (this);
    ArrDown = NULL;
    ArrDown = LoadBitmap (DLG::hInstance, "ARRDOWN");
    eListe.SetListclipp (TRUE);
    eListe.SetNoScrollBkColor (GetSysColor (COLOR_3DFACE));
//    Ctxt.BkColor = GetSysColor (COLOR_3DFACE);
}


PERSLST::~PERSLST ()
{
    int i, anz;
	if (work != NULL)
	{
          work->SetListDlg (NULL);
	}
    fChoise.destroy ();
	if (LstTabPers != NULL)
	{

        anz =   eListe.GetRecanz ()	;
        for (i = 0; i < anz; i ++)
        {
        }

//  	    delete []LstTab;
//		LstTab = NULL;
	}
    if (ProgCfg != NULL)
    {
        ProgCfg->CloseCfg ();
        delete ProgCfg;
        ProgCfg = NULL;
    }
    if (work != NULL)
    {
        work->SetPersLsts (NULL);
    }

}


// Funktionen f�r den Zugriff auf die Daten. Sind als abstrakte Schnittstellen in
// listenter definiert.

unsigned char *PERSLST::GetDataRec (void)
{
    return (unsigned char *) Rec;
//    return (unsigned char *) &Lsts;
}

int PERSLST::GetDataRecLen (void)
{
    return RecLen;
//    return sizeof (struct PERSLSTS);
}

char *PERSLST::GetListTab (int i)
{
	if (i == 0)
	{
		int di = 0;
	}
    LstTabPers[i] = new char [RecLen];
    DLG::ExitError ((LstTabPers[i] == NULL), "Fehler bei der Speicherzuordnung");
    memset (LstTabPers[i], ' ' , RecLen);
    return (char *) LstTabPers[i];
}

// Ende der Schnittstellen.


void PERSLST::Prepare (void)
{
    if (work != NULL)
    {
        cursor = work->PreparePers ();
    }
}

void PERSLST::Open (void)
{
	if (cursor == -1)
	{
		Prepare ();
		if (cursor == -1) return;
	}
    if (work != NULL)
    {
        work->OpenPers ();
    }
}

/*
int PERSLST::Fetch (void)
{
	if (cursor == -1)
	{
		Prepare ();
		if (cursor == -1) return 100;
	}
    if (work != NULL)
    {
        return work->FetchPers ();
    }
	return 100;
}
*/



int PERSLST::Fetch (void)
{
    work->SetPersLsts (PersLsts);
    int dsqlstatus =work->FetchPers ();
    if (dsqlstatus == 100)
    {
          return dsqlstatus;
    }

    return FetchM ();
}


int PERSLST::FetchM (void)
{

    sprintf (PersLsts[0]->GetFeld (), "%ld", prstatmasch.masch_nr);
    clipped (PersLsts[0]->GetFeld ());
    int fpos = CFELD::GetFeldpos (PersLsts, "masch_bz");
    if (fpos != -1)
    {
          strcpy (PersLsts[fpos]->GetFeld (), clipped (maschinen.masch_bz));
    }

	/*
    for (int i = 1; i < UbForm.fieldanz - 1; i ++)
    {
               work->FetchPlanGew ();
    }
	*/

    return 0;
}




void PERSLST::Close (void)
{
	if (cursor != -1)
	{
		cursor = -1;
	}
    if (work != NULL)
    {
        work->ClosePers ();
    }
}



BOOL PERSLST::BeforeCol (char *colname)
{
         char aktdat [12];
//         char dat [12];
         char aktcol [10];
         GetAktDat (aktdat);
		 strcpy (AktMaschNr , PersLsts[0]->GetFeld ());
		 strcpy (AktMaschBz , PersLsts[1]->GetFeld ());
//		 akt_masch_me = work->GetMaschMe(aktdat,atoi(AktMaschNr));
		 int i = Work::BeginPersCol;
        for (int z = i; i < DataForm.fieldanz ; i += Work::AnzPers,z++)
        {
	        for (int y = 0; y < Work::AnzPers ; y ++)
		    {
				if ((i+y) >= DataForm.fieldanz) break;


                sprintf (aktcol, "pers%d_%d", z - Work::BeginPersCol+1,y+1); 
	             if (strcmp (colname, aktcol) == 0)
		         {
					 strcpy (AktPersBz , PersLsts[i+y]->GetFeld ());
					 strcpy(prstatpers.pers,"");
					 work->GetPersBz(AktPersBz, AktPersBz, MDN);

//					 strcpy (AktMaschNr , PersLsts[0]->GetFeld ());
//					 strcpy (AktMaschBz , PersLsts[1]->GetFeld ());
//					 akt_masch_me = work->GetMaschMe(aktdat,atoi(AktMaschNr));

				 }
			}
		}
		RechneSumme();
         return FALSE;
}
         

BOOL PERSLST::AfterCol (char *colname)
{
         
	char eingabe [25];
	char Pers_bz [17];
         if (strcmp (colname, "chmasch_nr") == 0)
         {
              return TRUE;
         }
         memcpy (LstTabPers [eListe.GetAktRow ()], Rec, RecLen);
  	     eListe.ShowAktRow ();

         char date [12];
         char aktcol [10];
         GetAktDat (date);
		 int i = Work::BeginPersCol;
        for (int z = i; i < DataForm.fieldanz ; i += Work::AnzPers,z++)
        {
	        for (int y = 0; y < Work::AnzPers ; y ++)
		    {
//				if ((i+y) >= DataForm.fieldanz) break;
	             sprintf (aktcol, "pers%d_%d", z - Work::BeginPersCol+1,y+1); 
	             if (strcmp (clipped(colname), clipped(aktcol)) == 0)
		         {
					 strcpy (eingabe , PersLsts[i+y]->GetFeld ());
					 if (work->GetPersBz(Pers_bz, eingabe, MDN) == 0)
					 {
						 sprintf(PersLsts[i+y]->GetFeld (),"%s", clipped(Pers_bz));
					 }
					 else
					 {
						 memset (PersLsts[i+y]->GetFeld (), ' ', 16);
						 PersLsts[i+y]->GetFeld () [16] = (char) 0;
					 }
 	     		     memcpy (LstTabPers [eListe.GetAktRow ()], Rec, RecLen);
					 eListe.ShowAktRow (); 

				 }
			}
		}

         return FALSE;
}

int PERSLST::AfterRow (int Row)
{
//         form * DataForm; 

/*
         if (syskey == KEYUP && ratod (Lsts.nr) == 0.0)
         {
                return OnKey7 ();
         }
*/
         memcpy (LstTabPers [Row], Rec, RecLen);
         work->SetLsts (PersLsts);
         return 1;
}

int PERSLST::BeforeRow (int Row)
{
         BeforeCol ("me1_1"); 
         memcpy (LstTabPers [Row], Rec, RecLen);
		 /*
         double a = ratod (Lsts.a);
         ((SpezDlg *) Dlg)->SetChargen (0, 0, a);
         ((SpezDlg *) Dlg)->SetBearbR (Lsts.planstatus);
		 */
         return 1;
}


int PERSLST::ShowPersNr (void)
{
	/*

        struct SMASCH *smasch;
        SEARCHMASCH SearchMasch;


        SearchMasch.SetWindowStyle (WS_POPUP | WS_THICKFRAME);
        SearchMasch.SetParams (DLG::hInstance, GetParent (hMainWindow));
        if (Dlg != NULL)
        {
               SearchMasch.Setawin (((SpezDlg *) Dlg)->GethMainWindow ());
        }
        SearchMasch.Search ();
        if (syskey == KEY5)
        {
            syskey = 0;
            SetListFocus ();
            return 0;
        }

        smasch = SearchMasch.GetSMasch ();
        if (smasch == NULL)
        {
            syskey = 0;
            SetListFocus ();
            return 0;
        }

        strcpy (DataForm.mask[eListe.GetAktColumn ()].item->GetFeldPtr (),
                smasch->masch_nr);
        memcpy (&LstTab [eListe.GetAktRow ()], &Lsts, sizeof (Lsts));
        eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
		*/
        eListe.ShowAktRow ();
        SetListFocus ();
        return 0;
}


BOOL PERSLST::StopList (void)
{
    int i;
    int recanz;
    double chg_gew = 0.0;

    if (AfterRow (eListe.GetAktRow ()) == -1)
    {
        return TRUE;
    }

    eListe.GetFocusText ();
    recanz = eListe.GetRecanz ();

    for (i = 0; i < recanz; i ++)
    {
        WriteRec (i);
    }
	return TRUE;
}

BOOL PERSLST::OnKeyNext (void)
{
        if (GetKeyState (VK_CONTROL) >= 0)
        {
              if (Dlg != NULL)
              {
                  ((SpezDlg *) Dlg)->NextPage ();
                  return TRUE;
              }
        }
        return FALSE;
}


BOOL PERSLST::OnKeyPrior (void)
{
        if (GetKeyState (VK_CONTROL) >= 0)
        {
              if (Dlg != NULL)
              {
                  ((SpezDlg *) Dlg)->PriorPage ();
                  return TRUE;
              }
        }
        return FALSE;
}


BOOL PERSLST::OnKey5 (void)
{

//    StopList ();
    syskey = KEY5;
    if (work != NULL)
    {
        work->RollbackWork ();
    }
	return TRUE;

}

int PERSLST::TestAppend (void)
{
    switch (syskey)
    {
         case KEY6 :
           return 0;
         case KEY7 :
           return 0;
         case KEY8 :
           return 0;
    }
    return 1;
}

BOOL PERSLST::OnKey6 (void)
{
//    eListe.InsertLine ();
    return TRUE;
}

BOOL PERSLST::OnKeyInsert (void)
{
    return FALSE;
}

BOOL PERSLST::OnKey7 (void)
{
//    eListe.DeleteLine ();
    return TRUE;
}

BOOL PERSLST::OnKeyDel (void)
{
    return FALSE;
}

BOOL PERSLST::OnKey8 (void)
{
         return FALSE;
}


BOOL PERSLST::OnKey9 (void)
{
    char * Item = eListe.GetAktItem ();
    ShowPersNr ();
    return TRUE;
}


BOOL PERSLST::OnKey10 (void)
{
    if (Dlg != NULL)
    {
         return ((SpezDlg *) Dlg)->OnKey10 ();

         work->SetPersLsts (PersLsts);
// Hier Workfunktion einsetzen
         SetListFocus ();
         return TRUE;
	}
    return FALSE;
}

BOOL PERSLST::OnKey11 (void)
{

    if (Dlg != NULL)
    {
        ((SpezDlg *) Dlg)->OnKey11 ();
        SetAktFocus ();
    }
    return FALSE;
}


int PERSLST::ToMemory (int i)
{

	/*
    if (ratod (Lsts.masch_nr) <= 0.0)
    {
        return 0;
    }
	*/
    LISTENTER::ToMemory (i);
    return 0;
}


void PERSLST::InitRow (void)
{

    LISTENTER::InitRow ();


    work->SetPersLsts (PersLsts);
    work->InitPersRow ();

	/*
    if (ArrDown != NULL)
    {
        Lsts.chmasch_nr.SetBitMap (ArrDown);
    }
	*/


}

void PERSLST::uebertragen (void)
{
    work->SetPersLsts (PersLsts);
    work->FillPersRow (StartDay,DataForm.fieldanz - 1);
	    int y = 3;
        for (int i = Work::BeginPersCol; i < DataForm.fieldanz ; i += Work::AnzPers, y++)
        {
			for (int anz = 0; anz < Work::AnzPers ; anz++)
			{
				 if (Work::Datumsperre[y] == TRUE) 
                 {
					 _DataForm[i+anz].attribut  = DISPLAYONLY;
                 }
				 else
   				 {
	                 _DataForm[i+anz].attribut  = EDIT;
                 }
			}
        }

	/*
    if (ArrDown != NULL)
    {
        Lsts.chmasch_nr.SetBitMap (ArrDown);
    }
	*/
}

int PERSLST::WriteRec (int i)
{
	/*
    memcpy (&Lsts, &LstTab[i], sizeof (Lsts));
    work->SetPersLsts (&LstTab[i]);
    work->WritePersRec ();
	*/


	memcpy (Rec, LstTabPers [i], RecLen);
	work->SetPersLsts (PersLsts);
	return work->WritePersRec (&UbForm, DataForm.fieldanz, StartDay, i);

}

BOOL PERSLST::OnKey12 (void)
{
    StopList ();
    syskey = KEY12;
    if (work != NULL)
    {
        work->CommitWork ();
    }
    return TRUE;
}


BOOL PERSLST::OnKeyUp (void)
{
    return FALSE;
}

BOOL PERSLST::OnKeyDown (void)
{

    return FALSE;
}


void PERSLST::CreateListQuikInfos (HWND hWnd)
{
       if (Toolbar2 == NULL) return;

       Toolbar2->GetCfield ("f5")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("f12")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("f9")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("del")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("insert")->CreateQuikInfos (hWnd);
}


BOOL PERSLST::OnCommand (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    if (LOWORD (wParam) == IDM_CHOISE)
    {
        return OnKey9 ();
    }
    if (LOWORD (wParam) == IDM_CANCEL)
    {
        return OnKey5 ();
    }
    if (LOWORD (wParam) == IDM_WRITE)
    {
        return OnKey12 ();
    }
    if (LOWORD (wParam) == VK_DELETE)
    {
        return OnKeyDel ();
    }
    if (LOWORD (wParam) == IDM_DELETE)
    {
        return OnKey7 ();
    }
    if (LOWORD (wParam) == VK_INSERT)
    {
        return OnKeyInsert ();
    }
    if (LOWORD (wParam) == IDM_INSERT)
    {
        return OnKey6 ();
    }
    if (HIWORD (wParam)  == CBN_CLOSEUP)
	{
        if (Dlg != NULL)
        {
	           return ((SpezDlg *) Dlg)->OnCloseUp (hWnd, msg, wParam, lParam);
        }
	}
    if (HIWORD (wParam)  == CBN_DROPDOWN)
	{
/*
        if (strcmp (DataForm.mask[eListe.GetAktColumn ()].item->GetItemName (),
            "nr") == 0)
        {
             ShowNr ();
             eListe.CloseDropDown ();
             return TRUE;
        }
*/
	}
    return FALSE;
}

BOOL PERSLST::OnPaint (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    HDC hdc;

    if (Modal == FALSE) return FALSE;
    if (this->mamain1 != hWnd) return FALSE; 
    
    hdc = BeginPaint (hWnd, &ps);
    fChoise.display (hWnd, hdc);
    EndPaint (hWnd,&ps);
    return FALSE;
}

BOOL PERSLST::OnMove (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{

    if (Modal == FALSE) return FALSE;
    if (this->mamain1 != hWnd) return FALSE; 

    fChoise.MoveWindow ();
    return FALSE;
}



void PERSLST::GenWoFormsPers (short mdn, short kw, short jr)
{
        char iname [80];
        long lday;
        long ldatum;
        char day [12];
        char datum [12];
		Work::BeginPersCol = 0;
//        char datvon [12];
//        char datbis [12];
//        char cmedat [20];
        static char *KwDays [] = {"",
                                  "",
                                  "",
                                  "Montag",
                                  "Dienstag",
                                  "Mittwoch",
                                  "Donnerstag",
                                  "Freitag",
                                  "Samstag",
                                  "Sonntag",
                                   NULL,
                                   NULL,
        };
        static char *KwDays1 [] = {"",
                                  "",
                                  "",
                                  "((Montag))",
                                  "((Dienstag))",
                                  "((Mittwoch))",
                                  "((Donnerstag))",
                                  "((Freitag))",
                                  "((Samstag))",
                                  "((Sonntag))",
                                   NULL,
                                   NULL,
        };

        sysdate (datum);
        ldatum = dasc_to_long (datum) + 1;
        lday = first_kw_day ((short) kw, (short) jr);
        StartDay = lday;
 	    DefCol.BkColor = GetSysColor (COLOR_3DFACE);
        eListe.SetNoScrollBkColor (DefCol.BkColor);
//        work->SetAbt (abt);
        UbForm.fieldanz = 9;
        _UbForm = new field [UbForm.fieldanz];
        DLG::ExitError ((_UbForm == NULL), "Fehler bei der Speicherzuordnung");
        _LineForm = new field [UbForm.fieldanz];
        DLG::ExitError ((_LineForm == NULL), "Fehler bei der Speicherzuordnung");

        UbForm.mask = _UbForm;
        UbForm.frmstart  = 0;
        UbForm.frmscroll = 0;
        UbForm.caption   = NULL;
        UbForm.before    = NULL;
        UbForm.after     = NULL;
        UbForm.cbfield   = NULL;
        UbForm.font      = NULL;

        memcpy (&LineForm, &UbForm, sizeof (form));
        LineForm.mask    = _LineForm;

        int i = 0;
        int pos = 6;

        sprintf (iname, "masch_nr");
        char *ibez = new char [80]; 
        DLG::ExitError ((ibez == NULL), "Fehler bei der Speicherzuordnung");
        ColButton *Cub = new ColButton;
        DLG::ExitError ((Cub == NULL), "Fehler bei der Speicherzuordnung");
        memcpy (Cub, &DefCol, sizeof (ColButton));
        sprintf (ibez, "Maschine");
        Cub->text1 = ibez;
        Cub->ty1 = -1;

// Form �berschrift definieren

        _UbForm[i].item = new ITEM (iname, (char *) Cub, "", NULL);
        _UbForm[i].length    = 10;
        _UbForm[i].rows      = 0;
        _UbForm[i].pos[0]    = 0;
        _UbForm[i].pos[1]    = pos;
        _UbForm[i].feldid    = NULL;
        _UbForm[i].picture   = "";
        _UbForm[i].attribut  = COLBUTTON;
        _UbForm[i].before    = NULL;
        _UbForm[i].after     = NULL;
        _UbForm[i].BuId      = 0;

// �berschrift f�r Artikelspalte


        pos += _UbForm[i].length;
        memcpy (&_LineForm[i], &_UbForm[0], sizeof (field));
        _LineForm[i].item = &iline;
        _LineForm[i].length    = 1;
        _LineForm[i].pos[1]    = pos;
        _LineForm[i].attribut  = DISPLAYONLY;
        i ++;


// �berschrift f�r Bezeichnung

        sprintf (iname, "masch_bz");
        ibez = new char [80]; 
        DLG::ExitError ((ibez == NULL), "Fehler bei der Speicherzuordnung");
        Cub = new ColButton;
        DLG::ExitError ((Cub == NULL), "Fehler bei der Speicherzuordnung");
        memcpy (Cub, &DefCol, sizeof (ColButton));
        sprintf (ibez, "Bez./Abteilung");
        Cub->text1 = ibez;
        Cub->ty1 = -1;

        _UbForm[i].item = new ITEM (iname, (char *) Cub, "", NULL);
        _UbForm[i].length    = 25;
        _UbForm[i].rows      = 0;
        _UbForm[i].pos[0]    = 0;
        _UbForm[i].pos[1]    = pos;
        _UbForm[i].feldid    = NULL;
        _UbForm[i].picture   = "";
        _UbForm[i].attribut  = COLBUTTON;
        _UbForm[i].before    = NULL;
        _UbForm[i].after     = NULL;
        _UbForm[i].BuId      = 0;

        pos += _UbForm[i].length;
        memcpy (&_LineForm[i], &_UbForm[0], sizeof (field));
        _LineForm[i].item = &iline;
        _LineForm[i].length    = 1;
        _LineForm[i].pos[1]    = pos;
        _LineForm[i].attribut  = DISPLAYONLY;
        i ++;

// �berschrift f�r Averkaufsmengen

		/*
        sprintf (iname, "mengen");
        ibez = new char [80]; 
        DLG::ExitError ((ibez == NULL), "Fehler bei der Speicherzuordnung");
        Cub = new ColButton;
        DLG::ExitError ((Cub == NULL), "Fehler bei der Speicherzuordnung");
        memcpy (Cub, &DefCol, sizeof (ColButton));
        sprintf (ibez, "Liefermengen");
		rfmtdate (ldatum - 1 - work.LiefTagebis,"dd.mm",datbis); //todo
		rfmtdate (ldatum - 1 - work.LiefTagevon,"dd.mm",datvon);
		sprintf(cmedat,"%s-%s",clipped(datvon),clipped(datbis));
		Cub->text1 = ibez;
        Cub->text2 = new char [25];
        DLG::ExitError ((Cub->text2 == NULL), "Fehler bei der Speicherzuordnung");
        strcpy (Cub->text2, cmedat);
//        Cub->ty1 = -1;

        _UbForm[i].item = new ITEM (iname, (char *) Cub, "", NULL);
        _UbForm[i].length    = 15;
        _UbForm[i].rows      = 0;
        _UbForm[i].pos[0]    = 0;
        _UbForm[i].pos[1]    = pos;
        _UbForm[i].feldid    = NULL;
        _UbForm[i].picture   = "";
        _UbForm[i].attribut  = COLBUTTON;
        _UbForm[i].before    = NULL;
        _UbForm[i].after     = NULL;
        _UbForm[i].BuId      = 0;
        pos += _UbForm[i].length;
        memcpy (&_LineForm[i], &_UbForm[0], sizeof (field));
        _LineForm[i].item = &iline;
        _LineForm[i].length    = 1;
        _LineForm[i].pos[1]    = pos;
        _LineForm[i].attribut  = DISPLAYONLY;
        i ++;
*/
// �berschrift f�r Wochentage

        for (; i < UbForm.fieldanz - 1; i ++)
        {
            dlong_to_asc (lday, day);

            ibez = new char [80]; 
            DLG::ExitError ((ibez == NULL), "Fehler bei der Speicherzuordnung");
            sprintf (ibez, "%s", day);
            ColButton *Cub = new ColButton;
            DLG::ExitError ((Cub == NULL), "Fehler bei der Speicherzuordnung");
            memcpy (Cub, &DefCol, sizeof (ColButton));
            Cub->text1 = ibez;
            Cub->text2 = new char [25];
            DLG::ExitError ((Cub->text2 == NULL), "Fehler bei der Speicherzuordnung");
            strcpy (Cub->text2, KwDays[i]);
            strcpy (Cub->text2, KwDays[i]);


            memcpy (&_UbForm[i], &_UbForm[0], sizeof (field));
            _UbForm[i].item = new ITEM (iname, (char *) Cub, "", NULL);
            _UbForm[i].length    = PersLen;
            _UbForm[i].pos[1]    = pos;
            pos += _UbForm[i].length;

            memcpy (&_LineForm[i], &_UbForm[0], sizeof (field));
            _LineForm[i].item = &iline;
            _LineForm[i].length    = 1;
            _LineForm[i].pos[1]    = pos;
            _LineForm[i].attribut  = DISPLAYONLY;
            lday ++;
        }

// F�llbutton f�r �berschrifr am Zeilenende

        _UbForm[i].item = &ufiller;
        _UbForm[i].length    = 100;
        _UbForm[i].rows      = 0;
        _UbForm[i].pos[0]    = 0;
        _UbForm[i].pos[1]    = pos;
        _UbForm[i].feldid    = NULL;
        _UbForm[i].picture   = "";
        _UbForm[i].attribut  = BUTTON;
        _UbForm[i].before    = NULL;
        _UbForm[i].after     = NULL;
        _UbForm[i].BuId      = 0;

//        DataForm.fieldanz = (UbForm.fieldanz - 1) ;
//        _DataForm = new field [UbForm.fieldanz];
//        DataForm.fieldanz = UbForm.fieldanz + (6 * (Work::AnzPers - 1)) + 1; //+ 1 wegen "mengen" 
        DataForm.fieldanz = UbForm.fieldanz + (6 * (Work::AnzPers - 1)) ;  
//        _DataForm = new field [UbForm.fieldanz + (6 * (Work::AnzPers - 1))];
//        DataForm.fieldanz = (UbForm.fieldanz - 2  ) + (5 * (Work::AnzPers)) ;
        _DataForm = new field [DataForm.fieldanz + 1];
//        DataForm.fieldanz = 27 ;
//        _DataForm = new field [28];
        DLG::ExitError ((_DataForm == NULL), "Fehler bei der Speicherzuordnung");
        DataForm.mask = _DataForm;
        PersLsts = new CFELD * [DataForm.fieldanz + 8];
        DLG::ExitError ((PersLsts == NULL), "Fehler bei der Speicherzuordnung");

        RecLen = 19 + 
                 25 +                         // a_bz1
                 15 +                         // menge
                 18 * (DataForm.fieldanz + 1)  + 
                 10;                          // dummy

        Rec = new char [RecLen];
        DLG::ExitError ((Rec == NULL), "Fehler bei der Speicherzuordnung");
        memset (Rec, ' ', RecLen);

        char *Rpos = Rec;

        pos = 7;
        i = 0;


// Artikelspalte f�r Daten 

        strcpy (iname, "masch_nr");
        PersLsts [i] = new CFELD (iname, Rpos, 19);
        DLG::ExitError ((PersLsts[i] == NULL), "Fehler bei der Speicherzuordnung");
        PersLsts[i]->Init ();
        memcpy (&_DataForm[i], &_UbForm[0], sizeof (field));

        _DataForm[i].item = new ITEM (iname, PersLsts[i]->GetFeld (), "", NULL);
        _DataForm[i].length    = 8;
        _DataForm[i].pos[1]    = pos + 1;
        _DataForm[i].picture   = "%ld";
        _DataForm[i].attribut    = DISPLAYONLY;
        pos  += 10;
        Rpos += _DataForm[i].length;
		Work::BeginPersCol ++;

        i ++;

        strcpy (iname, "masch_bz");
        PersLsts [i] = new CFELD (iname, Rpos, 25);
        DLG::ExitError ((PersLsts[i] == NULL), "Fehler bei der Speicherzuordnung");
        PersLsts[i]->Init ();
        memcpy (&_DataForm[i], &_UbForm[0], sizeof (field));

        _DataForm[i].item = new ITEM (iname, PersLsts[i]->GetFeld (), "", NULL);
        _DataForm[i].length      = 23;
        _DataForm[i].pos[0]      = 0;
        _DataForm[i].pos[1]      = pos + 1;
        _DataForm[i].attribut    = DISPLAYONLY;
        Rpos += 25;
		Work::BeginPersCol ++;
        i ++;

        strcpy (iname, "prodabt");
        PersLsts [i] = new CFELD (iname, Rpos, 25);
        DLG::ExitError ((PersLsts[i] == NULL), "Fehler bei der Speicherzuordnung");
        PersLsts[i]->Init ();
        memcpy (&_DataForm[i], &_UbForm[0], sizeof (field));

        _DataForm[i].item = new ITEM (iname, PersLsts[i]->GetFeld (), "", NULL);
        _DataForm[i].length      = 23;
        _DataForm[i].pos[0]      = 1;
        _DataForm[i].pos[1]      = pos + 1;
        _DataForm[i].attribut    = DISPLAYONLY;
        pos  += 25;
        Rpos += 25;
		Work::BeginPersCol ++;
        i ++;

		/*
        strcpy (iname, "mengen");
        PersLsts [i] = new CFELD (iname, Rpos, 19);
        DLG::ExitError ((PersLsts[i] == NULL), "Fehler bei der Speicherzuordnung");
        PersLsts[i]->Init ();
        memcpy (&_DataForm[i], &_UbForm[0], sizeof (field));

        _DataForm[i].item = new ITEM (iname, PersLsts[i]->GetFeld (), "", NULL);
        _DataForm[i].length      = 13;
        _DataForm[i].pos[0]      = 0;
        _DataForm[i].pos[1]      = pos + 1;
        _DataForm[i].picture   = "%9.3fn";
        _DataForm[i].attribut    = DISPLAYONLY;
//        pos  += 15;
        Rpos += 15;
		Work::BeginPersCol ++;
        i ++;
		*/
// Datenzeile f�r Mengeneingabe definieren
        for (int z = i; i < DataForm.fieldanz ; i += Work::AnzPers,z++)
        {
	        for (int y = 0; y < Work::AnzPers ; y ++)
		    {
				if ((i+y) >= DataForm.fieldanz) break;
				sprintf (iname, "pers%d_%d", z - Work::BeginPersCol+1,y+1);

                 PersLsts [i+y] = new CFELD (iname, Rpos, 16);
                 DLG::ExitError ((PersLsts[i+y] == NULL), "Fehler bei der Speicherzuordnung");
                 PersLsts[i+y]->Init ();
                 memcpy (&_DataForm[i+y], &_UbForm[0], sizeof (field));

                 _DataForm[i+y].item = new ITEM (iname, PersLsts[i+y]->GetFeld (), "", NULL);
                 _DataForm[i+y].length    = 16;
                 _DataForm[i+y].pos[0]    = y;
                 _DataForm[i+y].pos[1]    = pos + 1;
//                 _DataForm[i+y].picture   = "%s";
                 _DataForm[i+y].attribut  = EDIT;
	             Rpos += _DataForm[i+y].length + 2;
			}
            pos += PersLen;
        }

// Speicher f�r Maschinenbezeichnung

		/*
        strcpy (iname, "masch_bz");
        PersLsts [i] = new CFELD (iname, Rpos, 13);
        DLG::ExitError ((PersLsts[i] == NULL), "Fehler bei der Speicherzuordnung");
        PersLsts[i]->Init ();
		*/


        PersLsts[i + 1] = NULL;

        ubrows = new int [DataForm.fieldanz + 1];
        DLG::ExitError ((ubrows == NULL), "Fehler bei der Speicherzuordnung");
        for (i = 0; i < DataForm.fieldanz; i ++)
        {
            ubrows[i] = i;
        }
        ubrows[i] = 0; 
}



char *PERSLST::GetAktDat (char *dat)
{
    int Col = eListe.GetAktColumn ();
	double i = (double) Col;

	i =  Col / (double) Work::AnzPers;
	Col =   (int) Work::Round (( Col / (double)Work::AnzPers)-0.01,0);
	if (Col < 7) Col += 1;
    ColButton *Cub = (ColButton *) UbForm.mask[Col].item->GetFeldPtr ();
    strcpy (dat, Cub->text1);
    return dat;
}

void PERSLST::UpdateList (void)
{

	StopList();
    eListe.DestroyFocusWindow ();
    ShowDB ();
    eListe.SetFeldFocus0 (eListe.GetAktRow (), eListe.GetAktColumn ());
    BeforeCol (DataForm.mask [eListe.GetAktColumn ()].item->GetItemName ());
}

void PERSLST::RechneSumme ()
{
	if (strlen(AktPersBz) == 0) return;
	int i = 0;
	int x = 0;
    long day, aktdat;
	char* Recsav;
	char AktDat [12];
	char Dat [12];
	char PersBz[26];
	int Feldanz = DataForm.fieldanz - 1;
    Recsav = new char[RecLen + 1];
    GetAktDat (AktDat);
    aktdat = dasc_to_long (AktDat);

	memcpy (Recsav, Rec, RecLen);
    int recanz = eListe.GetRecanz ();
	double akt_pers_zeit = 0.0;
	double aktmaschme = 0.0;
	char aktmaschnr[26]; 
	double AktSumZeit = 0.0;
	for (i = 0; i < recanz; i ++)
    {
		memcpy (Rec, LstTabPers [i], RecLen);
		 strcpy (aktmaschnr , PersLsts[0]->GetFeld ());
		 aktmaschme = work->GetMaschMe(dasc_to_long(AktDat),atoi(aktmaschnr));
		 int i = Work::BeginPersCol;
        for (int z = i; i < DataForm.fieldanz ; i += Work::AnzPers,z++)
        {
	        for (int y = 0; y < Work::AnzPers ; y ++)
		    {
				if ((i+y) >= DataForm.fieldanz) break;


  
				int Col =   (int) work->Round (( (i+y) / (double)work->AnzPers)-0.01,0);
				if (Col < 7) Col += 1;
				ColButton *Cub = (ColButton *) UbForm.mask[Col].item->GetFeldPtr ();
				strcpy (Dat, Cub->text1);
			    day = dasc_to_long (Dat);



				 strcpy (PersBz , PersLsts[i+y]->GetFeld ());
				 if (strlen(clipped(PersBz)) > 0)
				 {
					if (strcmp (clipped(PersBz), clipped(AktPersBz)) == 0 && day == aktdat)
					{
						akt_pers_zeit += work->GetAktSumZeit(); 
					}
				 }
	             if (atoi(aktmaschnr) == atoi(AktMaschNr))
		         {
						akt_masch_me = aktmaschme;
						AktSumZeit = work->GetAktSumZeit();
				 }
			}
		}

	}
    ((SpezDlg *) Dlg)->SetMaschPers (AktMaschNr, AktMaschBz, prstatpers.pers, AktPersBz
				,akt_masch_me , (int) AktSumZeit, (int) akt_pers_zeit);
	memcpy (Rec, Recsav, RecLen);
	delete Recsav;
}
