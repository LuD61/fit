#ifndef _PRSTATCHGK_DEF
#define _PRSTATCHGK_DEF

#include "windows.h"
#include "mo_draw.h"
#include "wmask.h"
#include "dbclass.h"

struct PRSTATCHGK {
   short     mdn;
   short     fil;
   long      dat;
   double    a;
   char      chargennr[14];
   double    me_ist;
   double    gew;
   short     varb_ok;
   short     zut_ok;
   short     huell_ok;
   char      rez[9];
   short     plan;
   short     stat;
   char      chg_fest[14];
   short     chg_lfd;
   short     prodabt;
   short     posi;
};
extern struct PRSTATCHGK prstatchgk, prstatchgk_null;

#line 10 "prstatchgk.rh"

class PRSTATCHGK_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               PRSTATCHGK_CLASS () : DB_CLASS ()
               {
               }
};
#endif
