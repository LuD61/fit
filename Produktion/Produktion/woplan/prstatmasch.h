#ifndef _PRSTATMASCH_DEF
#define _PRSTATMASCH_DEF

#include "windows.h"
#include "mo_draw.h"
#include "wmask.h"
#include "dbclass.h"

struct PRSTATMASCH {
   short     mdn;
   long      dat;
   double    a;
   short     phase;
   long      masch_nr;
   double    me;
   short     folge;
   short     status;
   short     posi;
};
extern struct PRSTATMASCH prstatmasch, prstatmasch_null;

#line 10 "prstatmasch.rh"

class PRSTATMASCH_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               PRSTATMASCH_CLASS () : DB_CLASS ()
               {
               }
};
#endif
