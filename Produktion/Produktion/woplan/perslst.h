#ifndef _PERSLST_DEF
#define _PERSLST_DEF
#include "listenter.h"
#include "listclex.h"
#include "work.h"
#include "mo_progcfg.h"

class PERSLST : public virtual LISTENTER, 
                public virtual KEYS,
                public virtual Info
{
         private :
                      static CFIELD *_fChoise0[];
                      static CFORM fChoise0;
                      static CFIELD *_fChoise[];
                      static CFORM fChoise;

                      int cursor;
                      CFORM *Toolbar2;
                      BOOL cfgOK;
                      PROG_CFG *ProgCfg;
                      HBITMAP ArrDown;
                      long AktDat;
                      long StartDay;
                      static short kw;
                      static short jr;
         public :

                     static Work *work;
                     static struct CHATTR ChAttra[];
                     static struct CHATTR *ChAttr;
//                     static struct PERSLSTS Lsts;
//                     static struct PERSLSTS *LstTab;
                     static char *Rec;
                     static int RecLen;
                     static CFELD **PersLsts;
                     static char *LstTabPers[];
                     static ITEM **ufelder;
                     static ITEM ufiller;
                     static field *_UbForm;
                     static form UbForm;
                     static int PersLen;


                     void GenWoFormsPers (short,short, short);
                     static ITEM umasch;
                     static ITEM uprodabt;
                     static ITEM umontag;
                     static ITEM udienstag;
                     static ITEM umittwoch;
                     static ITEM udonnerstag;
                     static ITEM ufreitag;
                     static ITEM usamstag;
//                     static ITEM ufiller;
                     static ITEM *PersTab[];
//                     static field _UbForm[];
//                     static form UbForm;
                     static int UbAnz;

                     static ITEM iline;
                     static field *_LineForm;
  


//                     static field _LineForm[];
                     static form LineForm;
                     static int LineAnz;

                     static ITEM imasch;
                     static ITEM iprodabt;
                     static ITEM imo_1;
                     static ITEM imo_2;
                     static ITEM imo_3;
                     static ITEM imo_4;

                     static ITEM idi_1;
                     static ITEM idi_2;
                     static ITEM idi_3;
                     static ITEM idi_4;

                     static ITEM imi_1;
                     static ITEM imi_2;
                     static ITEM imi_3;
                     static ITEM imi_4;

                     static ITEM ido_1;
                     static ITEM ido_2;
                     static ITEM ido_3;
                     static ITEM ido_4;

                     static ITEM ifr_1;
                     static ITEM ifr_2;
                     static ITEM ifr_3;
                     static ITEM ifr_4;

                     static ITEM isa_1;
                     static ITEM isa_2;
                     static ITEM isa_3;
                     static ITEM isa_4;

//                     static field _DataForm[];
                     static form DataForm;
                     static field *_DataForm;


                     static int DataAnz;
                     static int FirstPhasePos;

                     static int *ubrows;
//                     static int ubrows[];
                     static int NoRecNrSet;
                     static char *InfoItem;
                     void SetButtonSize (int);

                     void SetToolbar2 (CFORM *Toolbar2)
                     {
                               this->Toolbar2 = Toolbar2;
                     }
 
                     CFORM *GetToolbar2 (void)
                     {
                               return Toolbar2;
                     }

                     void SetWork (Work *work)
                     {
                         this->work = work;
                         this->work->SetListDlg (this);
                     }

                     Work *GetWork (void)
                     {
                         return work;
                     }

                     PERSLST (short, short , short);
					 ~PERSLST ();
                     char *GetAktDat (char *);
                     void ReadProdPhasen (void);
                     void GetSysPar (void);
                     void GetCfgValues (void);
                     void Setfeld2Removed (BOOL);
                     void Prepare (void);
                     void Open (void);
                     int Fetch (void);
                     int FetchM (void);
                     void Close (void);
 				     unsigned char *GetDataRec (void);
				     int GetDataRecLen (void);
 				     char *GetListTab (int);
                     int WriteRec (int);
                     void InitRow (void);
                     int ToMemory (int);
                     void uebertragen (void);
                     long GetAktPersNr (void);
                     void UpdateList ();

                     void SetNoRecNr (void);
                     BOOL StopList (void);
                     BOOL OnKey5 (void);
                     BOOL OnKey6 (void);
                     BOOL OnKey7 (void);
                     BOOL OnKey8 (void);
                     BOOL OnKey9 (void);
                     BOOL OnKey10 (void);
                     BOOL OnKey11 (void);
                     BOOL OnKey12 (void);
                     BOOL OnKeyDel (void);
                     BOOL OnKeyInsert (void);
                     BOOL OnKeyUp (void);
                     BOOL OnKeyDown (void);
                     BOOL OnKeyNext (void);
                     BOOL OnKeyPrior (void);
                     BOOL OnCommand (HWND, UINT, WPARAM, LPARAM);
                     BOOL OnPaint (HWND, UINT, WPARAM, LPARAM);
                     BOOL OnMove (HWND, UINT, WPARAM, LPARAM);
                     int AfterRow (int);
                     int AfterCol (char *);
                     int BeforeCol (char *);
                     void RechneSumme (void);
                     int BeforeRow (int);
                     void CreateListQuikInfos (HWND);
                     void TestRemoves (void);
                     int  ShowPersNr (void);
                     static int TestAppend (void);
//                     static int InfoProc (char **, char *, char *);

//                     BOOL CallInfo (HWND, RECT *, char *, char *, DWORD);
                     void CallInfoEx (void);
};
#endif