#ifndef _PRSTATATAG_DEF
#define _PRSTATATAG_DEF

#include "windows.h"
#include "mo_draw.h"
#include "wmask.h"
#include "dbclass.h"

struct PRSTATATAG {
   short     mdn;
   long      dat;
   double    a;
   char      rez[9];
   double    me;
   double    chg;
   double    mat_o_b;
   double    hk_teilk;
   double    hk_vollk;
   short     planstatus;
   long      phase1;
   long      phase2;
   long      phase3;
   long      phase4;
   long      phase5;
   short     aufteilung;
   short     variante;
   short     varb_ok;
   short     zut_ok;
   short     huell_ok;
   short     charge_ok;
   double    me_zusatz;
   short     prodabt;
   short     posi;
   long      phase6;
   long      phase7;
   long      phase8;
   long      phase9;
   long      phase10;
   double    auf_me;
   short     vpk_ok;
   double    planartikel;
   short     rework;
   long posi_long;
};
extern struct PRSTATATAG prstatatag, prstatatag_null;

#line 10 "prstatatag.rh"

class PRSTATATAG_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               PRSTATATAG_CLASS () : DB_CLASS ()
               {
               }
               int dbupdate (void);
               int dbupdateerf (void);
               
};
#endif
