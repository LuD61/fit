#ifndef _WEWORK_DEF
#define _WEWORK_DEF
#include "ptab.h"
#include "searchptab.h"
#include "mdn.h"
#include "dlg.h"
#include "a_bas.h"
#include "listenter.h"
#include "lief_bzg.h"
#include "lief.h"
#include "dbclass.h"
#include "wmaskc.h"
#include "prdk_k.h"
#include "prdk_varb.h"
#include "prdk_zut.h"
#include "prdk_huel.h"
#include "prdk_vpk.h"
#include "prdk_kost.h"
#include "prstatatag.h"
#include "verbr_tag.h"
#include "prstatchgk.h"
#include "prstatmasch.h"
#include "prstatpers.h"
#include "maschinen.h"
#include "woplan_del.h"


#define MAXA 3000

#define ARTIKEL 0
#define LIEFERANT 1

#define SMALL 0
#define BIG 1

#define MENGE 0
#define CHARGEN 1
#define AUFTRAEGE 2

struct MASCHLSTS
{
    char a [15];
    char a_bz1 [25];
    char chg [15];
    char me [15];
    char masch_nr1 [10];
    char masch_nr2 [10];
    char masch_nr3 [10];
    char masch_nr4 [10];
    char masch_nr5 [10];
    char masch_nr6 [10];
    char masch_nr7 [10];
    char masch_nr8 [10];
    char masch_nr9 [10];
    char masch_nr10 [10];
    char rez [10];
    int planstatus;
    short posi;     
    ListButton chmasch_nr;
};

struct PERSLSTS
{
    char masch_nr [15];
    char prodabt [25];
//    char mo_1 [10];
    ListButton chmasch_nr;
};

struct PHASELSTS
{
    char a [15];
    char a_bz1 [25];
    char chg [15];
    char me [15];
    char folge [8];
    char masch_nr [10];
    char me2 [15];
    char folge2 [8];
    char masch_nr2 [10];
    char me3 [15];
    char folge3 [8];
    char masch_nr3 [10];
    char rez [10];
    double PrStatMe;
    long MaschNr2;
    long MaschNr3;
	short posi; //300905
    ListButton chmasch_nr;
    ListButton chmasch_nr3;
};

struct ERFLSTS
{
    char a [15];
    char a_bz1 [50];
    char chg [15];
    char me [15];
	char me_einh [20];
    char folge [8];
    char rez [10];
    double PrStatMe;
	short posi;
    ListButton cha;
};

extern short JR;
extern short KW;

class CFELD
{
  private :
    char feldname [40];
    char *feldadr;
    int feldlen;
  public :
      CFELD ()
      {
          feldadr  = NULL;
          feldlen  = 0;
      }

      CFELD (char *feldname, char *feldadr)
      {
          strncpy (this->feldname,feldname, 39);
          this->feldname[39] = 0;
          this->feldadr  = feldadr;
          this->feldlen  = strlen (feldadr);
      }

      CFELD (char *feldname, char *feldadr, int len)
      {
          strncpy (this->feldname,feldname, 39);
          this->feldname[39] = 0;
          this->feldadr  = feldadr;
          this->feldlen  = len;
      }


      void SetFeld (char *feldadr, int len)
      {
          this->feldadr  = feldadr;
          this->feldlen  = len;
      }
           
      void SetFeld (char *feldadr)
      {
          this->feldadr  = feldadr;
      }

      const CFELD& operator=(char *Str)
      {
                if (feldadr == NULL)
                {
                    return *this;
                }
                strncpy (feldadr, Str, feldlen);
                feldadr[feldlen] = 0;
                return *this;
      }

      void Init (void)
      {
          if (feldadr == NULL) return;
          memset (feldadr, 0, feldlen - 1);
          feldadr [feldlen - 1] = 0;
      }

      void Init (char c)
      {
          if (feldadr == NULL) return;
          memset (feldadr, c, feldlen - 1);
          feldadr [feldlen - 1] = 0;
      }

      char *GetFeld (void)
      {
          return feldadr;
      }

      char *GetFeldname (void)
      {
          return feldname;
      }

      char *GetFeld (char *feldname)
      {
          if (strcmp (this->feldname, feldname) == 0)
          {
              return feldadr;
          }
          return NULL;
      }

      CFELD *GetClass (void)
      {
          return this;
      }

      CFELD *GetClass (char *feldname)
      {
          if (strcmp (this->feldname, feldname) == 0)
          {
              return this;
          }
          return NULL;
      }

      int GetLen (void)
      {
          return feldlen;
      }

      static int GetRecAnz (CFELD **Felder)
      {

          for (int i = 0; Felder [i] != NULL; i ++)
          {
          }
          return i;
      }

      static int GetRecLen (CFELD **Felder)
      {
          for (int i = 0, len = 0; Felder [i] != NULL; i ++)
          {
              len += Felder[i]->GetLen ();
          }
          return len;
      }

      static int GetFeldpos (CFELD **Felder, char *Feldname)
      {
          for (int i = 0; Felder[i] != NULL; i ++)
          {
              if (Felder[i]->GetClass (Feldname) != NULL)
              {
                  return i;
              }
          }
          return -1;
      }

      static void SetFeldWrt (CFELD **Felder, char *Feldname, char *Wrt)
      {
          int fpos = GetFeldpos (Felder, Feldname);
          if (fpos == -1)
          {
              return;
          }
          *Felder[fpos] = Wrt;
      }

      static char *GetFeldWrt (CFELD **Felder, char *Feldname)
      {
          int fpos = GetFeldpos (Felder, Feldname);
          if (fpos == -1)
          {
              return NULL;
          }
          return Felder[fpos]->GetFeld ();
      }
};


class Work
{
    private :
        PTAB_CLASS Ptab;
        MDN_CLASS Mdn;

        CFELD **Lsts;
        CFELD **PersLsts;
        int reclen;
        int feldanz;
        DB_CLASS DbClass;

        static long LongNull;
        DLG *Dlg;
        LISTENTER *ListDlg;
        int cursor;
        int LstCursor;
        int masch_cursor;
        int pers_cursor;
        int phase_cursor;
        int phase_cursor2;
        int varb_cursor;
        int varb_cursor1;
        int varb_cursor2;
        int zut_cursor;
        int zut_cursor1;
        int zut_cursor2;
        int huel_cursor;
        int huel_cursor1;
        int huel_cursor2;
        int erf_cursor;
        int vpk_cursor;
        short abt;
        short stnd;
        char *abt_bz;
        int ListMeMode;
        long  AktDat;
        double  AktSumZeit;
        long  StartDat;
        long  DatVon;
        long  DatBis;
		long DatOffset;
        short AktPhase;
        long  AktMaschNr;
        long  AktPersNr;
        short AktErf;
        static char *ChargeMacro;
        static char *ProdAbt;
        static char *OrderBy;
        static char *ChgOhneHuell;
        static char *ChargengroesseVarb;

        WOPLAN_DEL_CLASS WoplanDel;
        PRSTATATAG_CLASS PrStatatag;
        VERBR_TAG_CLASS VerbrTag;
		PRDK_VARB_CLASS PrdkVarb;
		PRDK_ZUT_CLASS PrdkZut;
		PRDK_HUEL_CLASS PrdkHuel;
		PRDK_VPK_CLASS PrdkVpk;
        PRSTATMASCH_CLASS PrStatMasch;
        MASCHINEN_CLASS Maschinen;
        PRSTATPERS_CLASS PrStatPers;
        PRSTATCHGK_CLASS *PrStatchgk;
        struct MASCHLSTS *MaschLsts;
//        struct PERSLSTS *PersLsts;
        struct PHASELSTS *PhaseLsts;
        struct ERFLSTS *ErfLsts;

    public :
        static double Round (double , int);
        static int MatrixTyp;
        static int LiefTagevon;
        static int LiefMeHolen;
        static int LiefTagebis;
        static int ArtScroll;
        static int Datumsperre [];
		static short AnzPers;
		static short BeginPersCol;
		static char datvon [12];
		static char datbis [12];
		long ldat1, ldat2, ldat3, ldat4, ldat5, ldat6, ldat7;
        void DeleteAllMe0 (void);
        void SetzeMenge0 (void);
		BOOL GetErfMatName (void);
		void GetErfAbz1 (char *a_bz1, char *me_einh,double *inh_prod, double *a_gew, double a_erf,char *rez);

        void SetAktDat (long dat)
        {
            AktDat = dat;
        }
        void SetDatOffset (long dat)
        {
            DatOffset = dat;
        }
        void SetStartDat (long dat)
        {
            StartDat = DatVon = dat;
			DatBis = DatVon + 6;
        }

        long GetAktDat (void)
        {
            return AktDat;
        }
        double GetAktSumZeit (void)
        {
            return AktSumZeit;
        }
        long GetDatOffset (void)
        {
            return DatOffset;
        }

        void SetAktPhase (int phase)
        {
            AktPhase = phase;
        }

        short GetAktPhase (void)
        {
            return AktPhase;
        }

        void SetAktMaschNr (long masch_nr)
        {
            AktMaschNr = masch_nr;
        }

        long GetAktMaschNr (void)
        {
            return AktMaschNr;
        }

        void SetAktPersNr (long pers_nr)
        {
            AktPersNr = pers_nr;
        }

        long GetAktPersNr (void)
        {
            return AktPersNr;
        }

        void SetAktErf (int erf)
        {
            AktErf = erf;
        }

        short GetAktErf (void)
        {
            return AktErf;
        }



        void SetListMeMode (int ListMeMode)
        {
            this->ListMeMode = ListMeMode;
        }

        int GetMatrix (void)
        {
            return MatrixTyp;
        }

        short GetListMeMode (void)
        {
            return ListMeMode;
        }

        void SetAbt (short abt)
        {
            this->abt = abt;
        }
        void SetStnd (short stnd)
        {
            this->stnd = stnd;
        }

        short GetAbt (void)
        {
            return abt;
        }
        short GetStnd (void)
        {
            return stnd;
        }


        void SetAbtBz (char *abt_bz)
        {
            this->abt_bz = abt_bz;
        }

        char *GetAbtBz (void)
        {
            return abt_bz;
        }

        void SetDlg (DLG *Dlg)
        {
            this->Dlg = Dlg;
        }

        DLG *GetDlg (void)
        {
            return Dlg;
        }

        void SetListDlg (LISTENTER *ListDlg)
        {
            this->ListDlg = ListDlg;
        }


        LISTENTER *GetListDlg (void)
        {
            return ListDlg;
        }

        void SetLsts (CFELD **Lsts)
        {
            this->Lsts = Lsts;
        }

        CFELD **GetLsts (void)
        {
            return Lsts;
        }
        CFELD **GetPersLsts (void)
        {
            return PersLsts;
        }

        void SetMaschLsts (struct MASCHLSTS *Lsts)
        {
            this->MaschLsts = Lsts;
        }

        void SetPersLsts (CFELD **Lsts)
        {
            this->PersLsts = Lsts;
        }
		/*
        void SetPersLsts (struct PERSLSTS *Lsts)
        {
            this->PersLsts = Lsts;
        }
*/

        struct MASCHLSTS *GetMaschLsts (void)
        {
            return MaschLsts;
        }
		/*
        struct PERSLSTS *GetPersLsts (void)
        {
            return PersLsts;
        }
		*/

        void SetPhaseLsts (struct PHASELSTS *Lsts)
        {
            this->PhaseLsts = Lsts;
        }

        struct PHASELSTS *GetPhaseLsts (void)
        {
            return PhaseLsts;
        }

        void SetErfLsts (struct ERFLSTS *Lsts)
        {
            this->ErfLsts = Lsts;
        }

        struct ERFLSTS *GeterfLsts (void)
        {
            return ErfLsts;
        }


        Work ()
        {
            Lsts = NULL; 
            Dlg = NULL;
            ListDlg = NULL;
		    LstCursor = -1;
            MatrixTyp = SMALL;
            LiefTagevon = 7;   //liefermengen 7 Tage zur�ck schauen
            LiefTagebis = 0;   //0 : ab Systemdatum
			LiefMeHolen = 0;
            abt = 0;
            ListMeMode = MENGE;
            cursor = -1;
            masch_cursor = -1;
            pers_cursor = -1;
            phase_cursor = -1;
            phase_cursor2 = -1;
            varb_cursor = -1;
            varb_cursor1 = -1;
            varb_cursor2 = -1;
            zut_cursor = -1;
            zut_cursor1 = -1;
            zut_cursor2 = -1;
            huel_cursor = -1;
            huel_cursor1 = -1;
            huel_cursor2 = -1;
            vpk_cursor = -1;
            erf_cursor = -1;
        }


        void ExitError (BOOL);
        void TestNullDat (long *);
        void TestAllDat (void);
        void BeginWork (void);
        void CommitWork (void);
        void RollbackWork (void);
        void InitRec (void);
		double HoleLiefMe (double);
        void FillRow (long, int);
        int GetPtab (char *, char *, char *);
        int ShowPtab (char *dest, char *item);
        int ShowPtabEx (HWND, char *dest, char *item);
        int GetMdnName (char *, short);
        int GetFilName (char *, short, short);
        int GetAbtBez (char *, short);
        double GetMaschMe (long, long);
        int GetAbtNr (char *, char *);
        void FillRow (char *);
        void DeleteAllPos (void);
        void PrepareLst (void);
        void OpenLst (void);
        int FetchLst (void);
        void CloseLst (void);
        void InitRow (void);
        long GetAbtAnz (short);
        long AbtFirst (short);
        long AbtNext (void);
        BOOL TestListAnz (void);
        int Prepare (void);
        void Open (void);
        int Fetch (void);
        int FetchPlanGew (void);
        void Close (void);
        int WriteRec (form *,int, long,short);
        int WritePersRec (form *,int, long,short);
        int WriteRecLief (form *,int);
        BOOL GetPlanStatus (double,short, char *);
//        long GenCharge (void);
        char * GenCharge (char *);
        char *AutoCharge (char *);
        int WritePrstatChgk (void);
        int WriteVerbrTag (short, long, double,short, double, double, short);
        int WritePrstatatagGbrt (short, long, double,short, double, double);
        int DeleteVerbrTag (void);
        int PruefeStatus (char*, double, short,short, double*);
        int PruefeGrundbraete (char*, double, short, short);
        int SchreibeZusatz (char*, double, short, double); //141105 bgut1
        int MaschStatusZurueck (char*, double, short);
		void HoleAuftraege(void);
		double GetArtGew (double , double, short);
		void ins_woplan_del (double);
		void loesch_woplan_del (void);
		short HoleStnd (short);

// Maschinenliste

        void GetMaschBz (char *, long);
        void FillMaschRow (void);
        void InitMaschRow (void);
        int TestPhasen (void);
        void WriteMaschRec (void);
        void WriteMaschRec (double);
        int PrepareMasch (void);
        int OpenMasch (void);
        int FetchMasch (void);
        int CloseMasch (void);
        BOOL GetPlanStatusR (double, short, long);

// Reihenfolgeliste

        void GetPhaseAbz1 (char *, double);
        void FillPhaseRow (void);
        void InitPhaseRow (void);
        void WritePhaseRec (void);

        int PreparePhase (void);
        int OpenPhase (void);
        int FetchPhase (void);
        int ClosePhase (void);
        int PreparePhase2 (void);
        int OpenPhase2 (void);
        int FetchPhase2 (void);
        int ClosePhase2 (void);

// Personenliste

        int GetPersBz (char *, char *, short);
        void FillPersRow (long,int);
        void InitPersRow (void);
//        void WritePersRec (void);
//        void WritePersRec (double);
        int PreparePers (void);
        int OpenPers (void);
        int FetchPers (void);
        int ClosePers (void);

// Erfassungsliste

        void FillErfRow (void);
        void InitErfRow (void);
        void WriteErfRec (void);
        int PrepareErf (void);
        int OpenErf (void);
        int FetchErf (void);
        int CloseErf (void);




        int CloseVarb (void);
        int PrepareVarb (void);
        int PrepareVarb1 (void);
        int PrepareVarb2 (void);
        int OpenVarb (short);
        int FetchVarb (short);
        int CloseZut (void);
        int PrepareZut (void);
        int PrepareZut1 (void);
        int PrepareZut2 (void);
        int OpenZut (short);
        int FetchZut (short);
        int CloseHuel (void);
        int PrepareHuel (void);
        int PrepareHuel1 (void);
        int PrepareHuel2 (void);
        int OpenHuel (short);
        int FetchHuel (short);
        int CloseVpk (void);
        int PrepareVpk (void);
        int OpenVpk (void);
        int FetchVpk (void);
        int DeleteVerbrTag (short, long, short); 
        int DeletePrstatatagGbrt (short, long, short); 
        int Delete0MengeVonPrstatatag (short, long, short); 
        int GetProdPhasen (ITEM *[]);
        long DatToLong (char *);
        char *LongToDat (long, char *);

        static void SetChargeMacro (char *);
        static void SetProdAbt (char *);
        static char *GetProdAbt (void);
        static void SetOrderBy (char *);
        static void SetChgOhneHuell (char *);
        static void SetChargengroesseVarb (char *);
		static void SetKwJr(short,short);

        int PruefZut (short, char *);
        void Upd99 (void);

}; 

#endif