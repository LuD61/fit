#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include <string.h>
#include "comcthlp.h"
#include "itemc.h"
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_menu.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_intp.h"
#include "inflib.h"
#include "spezdlg.h"
#include "enterfunc.h"
#include "MaschLst.h"
#include "spezdlg.h"
#include "searchmasch.h"
// #include "searchvarb.h"
// #include "dlg.h"

#define IDM_CANCEL 3001
#ifndef IDM_WRITE
#define IDM_WRITE  3002
#endif

static int StdSize = STDSIZE;

static int dlgsize    = STDSIZE;
static int ltgraysize = STDSIZE;
static int dlgpossize = STDSIZE;
static int cpsize     = 200;


static mfont dlgfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         GRAYCOL,
                         0,
                         NULL};


static ColButton Ctxt    = { "Text",  10, -1, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, -1, -1,
                             NULL,  0, 0,
                             BLACKCOL,
                             LTGRAYCOL,
                             0, 
};


struct MASCHLSTS  MASCHLST::Lsts;
struct MASCHLSTS *MASCHLST::LstTab;
char *MASCHLST::InfoItem = "a";
Work *MASCHLST::work = NULL;
short MASCHLST::kw;
short MASCHLST::jr;


CFIELD *MASCHLST::_fChoise0 [] = {
    new CFIELD ("cancel", " abbrechen ", 10, 0, 0, 0, NULL, "",
                CBUTTON, 
                IDM_CANCEL, &dlgfont, 0, 0),
    new CFIELD ("write", " speichern ", 10, 0,12, 0, NULL, "",
                CBUTTON, 
                IDM_WRITE, &dlgfont, 0, 0),
    new CFIELD ("show",  "  ansehen  ", 10, 0,24, 0, NULL, "",
                CBUTTON, 
                IDM_CHOISE, &dlgfont, 0, 0),
    new CFIELD ("delete","  l�schen  ", 10, 0,36, 0, NULL, "",
                CBUTTON, 
                IDM_DELETE, &dlgfont, 0, 0),
    new CFIELD ("insert"," einf�gen  ", 10, 0,48, 0, NULL, "",
                CBUTTON, 
                IDM_INSERT, &dlgfont, 0, 0),
    NULL,
};

CFORM MASCHLST::fChoise0 (5, _fChoise0);

CFIELD *MASCHLST::_fChoise [] = {
    new CFIELD ("fChoise0", (char *) &fChoise0, 0, 0, -1 , -2, NULL, "",
                CFFORM, 
                500, &dlgfont, 0, 0),
    NULL,
};

CFORM MASCHLST::fChoise (1, _fChoise);


ITEM MASCHLST::ua                  ("a",                  "Artikel",          "", 0);
ITEM MASCHLST::ua_bz1              ("a_bz1",              "Bezeichnung",      "", 0);  
ITEM MASCHLST::uchg                ("chgsize",            "ChgGr��e.",        "", 0);
ITEM MASCHLST::ume                 ("me",                 "Menge",            "", 0);
ITEM MASCHLST::umasch1             ("masch1",             "F�llen",           "", 0);
ITEM MASCHLST::umasch2             ("masch2",             "Kutter",           "", 0);
ITEM MASCHLST::umasch3             ("masch3",             "Kochraum",         "", 0);
ITEM MASCHLST::umasch4             ("masch4",             "Phase4  ",         "", 0);
ITEM MASCHLST::umasch5             ("masch5",             "Phase5  ",         "", 0);
ITEM MASCHLST::umasch6             ("masch6",             "Phase6  ",         "", 0);
ITEM MASCHLST::umasch7             ("masch7",             "Phase7  ",         "", 0);
ITEM MASCHLST::umasch8             ("masch8",             "Phase8  ",         "", 0);
ITEM MASCHLST::umasch9             ("masch9",             "Phase9  ",         "", 0);
ITEM MASCHLST::umasch10            ("masch10",            "Phase10 ",         "", 0);
ITEM MASCHLST::ufiller             ("",                   " ",                "",0);

ITEM *MASCHLST::MaschTab[] = {&umasch1,
                              &umasch2,
                              &umasch3,
                              &umasch4,
                              &umasch5,
                              &umasch6,
                              &umasch7,
                              &umasch8,
                              &umasch9,
                              &umasch10,
                              NULL,
};

ITEM MASCHLST::iline ("", "1", "", 0);

ITEM MASCHLST::ia                  ("a",                  Lsts.a,                   "", 0);
ITEM MASCHLST::ia_bz1              ("a_bz1",              Lsts.a_bz1,               "", 0);  
ITEM MASCHLST::ichg                ("chgsize",            Lsts.chg,                 "", 0);
ITEM MASCHLST::ime                 ("me",                 Lsts.me,                  "", 0);
ITEM MASCHLST::imasch1             ("masch1",             Lsts.masch_nr1,           "", 0);
ITEM MASCHLST::imasch2             ("masch2",             Lsts.masch_nr2,           "", 0);
ITEM MASCHLST::imasch3             ("masch3",             Lsts.masch_nr3,           "", 0);
ITEM MASCHLST::imasch4             ("masch4",             Lsts.masch_nr4,           "", 0);
ITEM MASCHLST::imasch5             ("masch5",             Lsts.masch_nr5,           "", 0);
ITEM MASCHLST::imasch6             ("masch6",             Lsts.masch_nr6,           "", 0);
ITEM MASCHLST::imasch7             ("masch7",             Lsts.masch_nr7,           "", 0);
ITEM MASCHLST::imasch8             ("masch8",             Lsts.masch_nr8,           "", 0);
ITEM MASCHLST::imasch9             ("masch9",             Lsts.masch_nr9,           "", 0);
ITEM MASCHLST::imasch10            ("masch10",            Lsts.masch_nr10,          "", 0);
ITEM MASCHLST::ichmasch_nr         ("chmasch_nr",         (char *) &Lsts.chmasch_nr, "", 0);

field MASCHLST::_UbForm [] = {
&ua,       17, 0, 0,  6,  NULL, "", BUTTON, 0, 0, 0,     
&ua_bz1,   21, 0, 0, 23,  NULL, "", BUTTON, 0, 0, 0, 
&uchg,     15, 0, 0, 44,  NULL, "", BUTTON, 0, 0, 0, 
&ume,      15, 0, 0, 59,  NULL, "", BUTTON, 0, 0, 0, 
&umasch1,  10, 0, 0, 74,  NULL, "", BUTTON, 0, 0, 0, 
&umasch2,  10, 0, 0, 84,  NULL, "", BUTTON, 0, 0, 0, 
&umasch3,  10, 0, 0, 94,  NULL, "", BUTTON, 0, 0, 0, 
&umasch4,  10, 0, 0,104,  NULL, "", BUTTON, 0, 0, 0, 
&umasch5,  10, 0, 0,114,  NULL, "", BUTTON, 0, 0, 0, 
&umasch6,  10, 0, 0,124,  NULL, "", BUTTON, 0, 0, 0, 
&umasch7,  10, 0, 0,134,  NULL, "", BUTTON, 0, 0, 0, 
&umasch8,  10, 0, 0,144,  NULL, "", BUTTON, 0, 0, 0, 
&umasch9,  10, 0, 0,154,  NULL, "", BUTTON, 0, 0, 0, 
&umasch10, 10, 0, 0,164,  NULL, "", BUTTON, 0, 0, 0, 
&ufiller, 120, 0, 0,174,  NULL, "", BUTTON, 0, 0, 0,
};


form MASCHLST::UbForm = {15, 0, 0, _UbForm, 0, 0, 0, 0, NULL};
int MASCHLST::UbAnz = 15;

field MASCHLST::_LineForm [] = {
&iline,   1, 0, 0, 23,  NULL, "",  DISPLAYONLY, 0, 0, 0, 
&iline,   1, 0, 0, 44,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0, 59,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0, 74,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0, 84,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0, 94,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,104,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,114,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,124,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,134,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,144,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,154,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,164,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,174,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
};

form MASCHLST::LineForm = {14, 0, 0, _LineForm,0, 0, 0, 0, NULL};
int MASCHLST::LineAnz = 14;

field MASCHLST::_DataForm [] = {
&ia,       15, 0, 0,  7,  NULL, "%13.0f", DISPLAYONLY, 0, 0, 0,     
&ia_bz1,   19, 0, 0, 24,  NULL, "",       DISPLAYONLY, 0, 0, 0, 
&ichg,     13, 0, 0, 45,  NULL, "%12.3f", DISPLAYONLY, 0, 0, 0, 
&ime,      13, 0, 0, 60,  NULL, "%13.3f", DISPLAYONLY, 0, 0, 0, 
&imasch1,   9, 0, 0, 75,  NULL, "%8dn",    EDIT,        0, 0, 0, 
&imasch2,   9, 0, 0, 85,  NULL, "%8dn",    EDIT,        0, 0, 0, 
&imasch3,   9, 0, 0, 95,  NULL, "%8dn",    EDIT,        0, 0, 0, 
&imasch4,   9, 0, 0,105,  NULL, "%8dn",    EDIT,        0, 0, 0, 
&imasch5,   9, 0, 0,115,  NULL, "%8dn",    EDIT,        0, 0, 0, 
&imasch6,   9, 0, 0,125,  NULL, "%8dn",    EDIT,        0, 0, 0, 
&imasch7,   9, 0, 0,135,  NULL, "%8dn",    EDIT,        0, 0, 0, 
&imasch8,   9, 0, 0,145,  NULL, "%8dn",    EDIT,        0, 0, 0, 
&imasch9,   9, 0, 0,165,  NULL, "%8dn",    EDIT,        0, 0, 0, 
&imasch10,  9, 0, 0,175,  NULL, "%8dn",    EDIT,        0, 0, 0, 
};


form MASCHLST::DataForm = {14, 0, 0, _DataForm,0, 0, 0, 0, NULL};
int MASCHLST::DataAnz = 14;

int MASCHLST::FirstPhasePos = 4;

int MASCHLST::ubrows [] = {0, 
//                          0,
//                          1, 
                          1,
                          2,
                          3, 
                          4,
                          5,
                          6,
                          7,
                          8,
                          9,
                         10,
                         11,
                         12,
                         -1,
};


struct CHATTR MASCHLST::ChAttra [] = {
                                      "chmasch_nr",   REMOVED,BUTTON,                
//                                      "nr",     DISPLAYONLY,COMBOBOX,                
                                      NULL,  0,           0};

struct CHATTR *MASCHLST::ChAttr = ChAttra;
int MASCHLST::NoRecNrSet = FALSE;



/*
BOOL MASCHLST::CallInfo (HWND hWnd, RECT *rect, char *Item, char *ItemValue, DWORD WinMode)
/?*
Einstieg in Infosystem mit Artikel. Abgeleitet von Klasse Info.
*?/
{
 		 char buffer [1024];
         BOOL ret;

         sprintf (buffer, "where a = 2");
         ret = Info::CallInfo (hWnd, rect, "a", buffer, WinMode); 
         SetListFocus ();
         return ret;
}


int MASCHLST::InfoProc (char **Item, char *Value, char *where)
/?**
Spezielle Procedure fuer Info-System. Alter Ablauf, ist nicht mehr Aktiv 
*?/
{

   *Item = "a";


   sprintf (where, "where a = %.0lf", Lsts.a);
   return 1;
}
*/

void MASCHLST::ReadProdPhasen (void)
{
    int prpanz = work->GetProdPhasen (MaschTab);
	prpanz --;
	if (prpanz <= 0)
	{
		prpanz = 4;
	}
    int diff = max (0, 10 - prpanz);
    ubform->fieldanz   = UbAnz - diff;
    lineform->fieldanz = LineAnz - (diff - 1);
    dataform->fieldanz = DataAnz - (diff - 1);
}

    
void MASCHLST::CallInfoEx (void)
/*
Info wird aus Dialog aufgerufen.
*/ 

{
   char where [512];
   
   sprintf (where, "where a = %.0lf", Lsts.a);
   _CallInfoEx (hMainWindow, NULL, InfoItem, where, 0l); 
}

void MASCHLST::SetButtonSize (int Size)
{
    int fpos;

    fpos = GetItemPos (&DataForm, "chmasch_nr");
    if (fpos < 0) return;

    if (Size == SMALL)
    {
         DataForm.mask [fpos].length = 2;
         DataForm.mask [fpos].rows = 1;
    }
    else 
    {
         DataForm.mask [fpos].length = 3;
         DataForm.mask [fpos].rows = 0;
    }
}


void MASCHLST::GetCfgValues (void)
{
       char cfg_v [512];
       COLORREF Color;

       if (ProgCfg == NULL) return;
	   if (cfgOK) return;

       cfgOK = TRUE;
       if (ProgCfg->GetCfgValue ("3DList", cfg_v) == TRUE)
       {
                    eListe.Set3D (atoi (cfg_v));
	   }
       if (ProgCfg->GetCfgValue ("3DSize", cfg_v) == TRUE)
       {
                    eListe.SetPlus3D (atoi (cfg_v));
	   }
       if (ProgCfg->GetCfgValue ("ListFocus", cfg_v) == TRUE)
       {
                    ListFocus = atoi (cfg_v);
	   }
        if (ProgCfg->GetCfgValue ("rowheight", cfg_v) == TRUE)
		{
			        RowHeight = ratod (cfg_v);
					if (RowHeight < (double) 1.0)
					{
						RowHeight = (double) 1.0;
					}
		}
        if (ProgCfg->GetCfgValue ("ubheight", cfg_v) == TRUE)
		{
			        UbHeight = atoi (cfg_v);
					SetUbHeight ();
		}
		eListe.SetRowHeight (RowHeight);


        if (ProgCfg->GetCfgValue ("NoRecNr", cfg_v) == TRUE)
        {
			        NoRecNr = atoi (cfg_v);
		            if (atoi (cfg_v))
					{
	                        SetNoRecNr ();
					}
		}
        if (ProgCfg->GetCfgValue ("ListColor", cfg_v) == TRUE)
        {
                     GetListColor (&Color, cfg_v);
		             eListe.SetColor (Color);
        }
        if (ProgCfg->GetCfgValue ("ListBkColor", cfg_v) == TRUE)
        {
                     GetListColor (&Color, cfg_v);
		             eListe.SetBkColor (Color);
        }
        if (ProgCfg->GetCfgValue ("ListLine", cfg_v) == TRUE)
        {
		             eListe.SetLines (GetListLine (cfg_v));
        }
        if (ProgCfg->GetCfgValue ("VarbInfo", cfg_v) == TRUE)
        {
                     InfoItem = new char [80];
                     strcpy (InfoItem, cfg_v);
        }
}


void MASCHLST::TestRemoves (void)
{
/*
        int fpos;

        fpos = GetItemPos (&DataForm, "feld3");
        if (fpos >= 0)
        {
            if (DataForm.mask[fpos].attribut & REMOVED)
            {
                DestroyListField ("feld3");
            }
        }
*/
}


void MASCHLST::SetNoRecNr (void)
{
	   int i;

	   eListe.SetNoRecNr (TRUE);
	   if (NoRecNrSet) return;

	   for (i = 0; i < dataform->fieldanz; i ++)
	   {
		   dataform->mask[i].pos[1] -= 6;
	   }
	   for (i = 0; i < ubform->fieldanz; i ++)
	   {
		   ubform->mask[i].pos[1] -= 6;
	   }
	   for (i = 0; i < lineform->fieldanz; i ++)
	   {
		   lineform->mask[i].pos[1] -= 6;
	   }
       NoRecNrSet = TRUE;
}


long MASCHLST::GetAktMaschNr (void)
{
    int Col = eListe.GetAktColumn ();
    return atol (DataForm.mask[Col].item->GetFeldPtr ());
}
    

short MASCHLST::GetAktPhase (char *PhaseBz)
{
    int Col = eListe.GetAktColumn ();
    strcpy (PhaseBz, UbForm.mask[Col].item->GetFeldPtr ());
    return Col + 1 - FirstPhasePos;
}
    
MASCHLST::MASCHLST (short mdn, long dat) : KEYS (), LISTENTER () 
{

    ProgCfg = new PROG_CFG ("woplan");
	Dlg = NULL;
    cfgOK = FALSE;
	cursor = -1;
    prdk_k.mdn = mdn;
    prstatatag.mdn = mdn;
    AktDat = dat;

    LstTab = new MASCHLSTS [0x10000];

    dataform  = &DataForm;
    ubform    = &UbForm;
    lineform  = &LineForm;
    ReadProdPhasen ();
//    DelListField ("kosten");
//    SetFieldLen ("a", 9);
    eListe.SetUbRows (ubrows); 
	eListe.SetKeys (this);

    if ((work.ArtScroll == FALSE))
    {

           eListe.SetHscrollStart (4);
           UbForm.ScrollStart = 4;
    }

    LISTENTER::GetCfgValues ();
    GetCfgValues ();
    TestRemoves ();
    SetNoRecNr ();
	fChoise.Setchpos (0, 2);
    work = NULL;
    ChAttr = ChAttra;
    eListe.SetChAttr (ChAttr); 
    Toolbar2 = NULL;
    eListe.SetTestAppend (TestAppend);
//    eListe.SetInfoProc (InfoProc);
    eListe.SetInfo (this);
    ArrDown = NULL;
    ArrDown = LoadBitmap (DLG::hInstance, "ARRDOWN");
    eListe.SetListclipp (TRUE);
    eListe.SetNoScrollBkColor (GetSysColor (COLOR_3DFACE));
//    Ctxt.BkColor = GetSysColor (COLOR_3DFACE);
}


MASCHLST::~MASCHLST ()
{
    int i, anz;
	if (work != NULL)
	{
          work->SetListDlg (NULL);
	}
    fChoise.destroy ();
	if (LstTab != NULL)
	{

        anz =   eListe.GetRecanz ()	;
        for (i = 0; i < anz; i ++)
        {
        }

  	    delete []LstTab;
		LstTab = NULL;
	}
    if (ProgCfg != NULL)
    {
        ProgCfg->CloseCfg ();
        delete ProgCfg;
        ProgCfg = NULL;
    }
    if (work != NULL)
    {
        work->SetMaschLsts (NULL);
    }

}


// Funktionen f�r den Zugriff auf die Daten. Sind als abstrakte Schnittstellen in
// listenter definiert.

unsigned char *MASCHLST::GetDataRec (void)
{
    return (unsigned char *) &Lsts;
}

int MASCHLST::GetDataRecLen (void)
{
    return sizeof (struct MASCHLSTS);
}

char *MASCHLST::GetListTab (int i)
{
    return (char *) &LstTab[i];
}

// Ende der Schnittstellen.

/*
void MASCHLST::Setfeld2Removed (BOOL removed)
{
    if (removed)
    {
            SetFieldAttr ("feld2", REMOVED);
    }
    else
    {
            SetFieldAttr ("feld2", EDIT);
    }
}
*/

void MASCHLST::Prepare (void)
{
    if (work != NULL)
    {
        cursor = work->PrepareMasch ();
    }
}

void MASCHLST::Open (void)
{
	if (cursor == -1)
	{
		Prepare ();
		if (cursor == -1) return;
	}
    if (work != NULL)
    {
        work->OpenMasch ();
    }
}

int MASCHLST::Fetch (void)
{
	if (cursor == -1)
	{
		Prepare ();
		if (cursor == -1) return 100;
	}
    if (work != NULL)
    {
        return work->FetchMasch ();
    }
	return 100;
}


void MASCHLST::Close (void)
{
	if (cursor != -1)
	{
		cursor = -1;
	}
    if (work != NULL)
    {
        work->CloseMasch ();
    }
}



BOOL MASCHLST::BeforeCol (char *colname)
{
         double a = ratod (Lsts.a);
         int phase     = eListe.GetAktColumn () - 3;
         long masch_nr = atol (eListe.GetDataForm ()->mask[phase + 3].item->GetFeldPtr ());
         ((SpezDlg *) Dlg)->SetChargen (0, 0, a);
         ((SpezDlg *) Dlg)->SetBearbR (work->GetPlanStatusR (a, phase, masch_nr));
         return FALSE;
}
         

BOOL MASCHLST::AfterCol (char *colname)
{
         
         if (strcmp (colname, "chmasch_nr") == 0)
         {
              return TRUE;
         }
         return FALSE;
}

int MASCHLST::AfterRow (int Row)
{
//         form * DataForm; 

/*
         if (syskey == KEYUP && ratod (Lsts.nr) == 0.0)
         {
                return OnKey7 ();
         }
*/
         memcpy (&LstTab [Row], &Lsts, sizeof (Lsts));
         work->SetMaschLsts (&Lsts);
         return 1;
}

int MASCHLST::BeforeRow (int Row)
{
         memcpy (&LstTab [Row], &Lsts, sizeof (Lsts));
         double a = ratod (Lsts.a);
         ((SpezDlg *) Dlg)->SetChargen (0, 0, a);
         ((SpezDlg *) Dlg)->SetBearbR (Lsts.planstatus);
         return 1;
}


int MASCHLST::ShowMaschNr (void)
{

        struct SMASCH *smasch;
        SEARCHMASCH SearchMasch;


        SearchMasch.SetWindowStyle (WS_POPUP | WS_THICKFRAME);
        SearchMasch.SetParams (DLG::hInstance, GetParent (hMainWindow));
        if (Dlg != NULL)
        {
               SearchMasch.Setawin (((SpezDlg *) Dlg)->GethMainWindow ());
        }
        SearchMasch.Search ();
        if (syskey == KEY5)
        {
            syskey = 0;
            SetListFocus ();
            return 0;
        }

        smasch = SearchMasch.GetSMasch ();
        if (smasch == NULL)
        {
            syskey = 0;
            SetListFocus ();
            return 0;
        }

        strcpy (DataForm.mask[eListe.GetAktColumn ()].item->GetFeldPtr (),
                smasch->masch_nr);
        memcpy (&LstTab [eListe.GetAktRow ()], &Lsts, sizeof (Lsts));
        eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
        eListe.ShowAktRow ();
        SetListFocus ();
        return 0;
}


BOOL MASCHLST::StopList (void)
{
    int i;
    int recanz;
    double chg_gew = 0.0;

    if (AfterRow (eListe.GetAktRow ()) == -1)
    {
        return TRUE;
    }

    eListe.GetFocusText ();
    recanz = eListe.GetRecanz ();

    for (i = 0; i < recanz; i ++)
    {
        WriteRec (i);
    }
	return TRUE;
}

BOOL MASCHLST::OnKeyNext (void)
{
        if (GetKeyState (VK_CONTROL) >= 0)
        {
              if (Dlg != NULL)
              {
                  ((SpezDlg *) Dlg)->NextPage ();
                  return TRUE;
              }
        }
        return FALSE;
}


BOOL MASCHLST::OnKeyPrior (void)
{
        if (GetKeyState (VK_CONTROL) >= 0)
        {
              if (Dlg != NULL)
              {
                  ((SpezDlg *) Dlg)->PriorPage ();
                  return TRUE;
              }
        }
        return FALSE;
}


BOOL MASCHLST::OnKey5 (void)
{

//    StopList ();
    syskey = KEY5;
    if (work != NULL)
    {
        work->RollbackWork ();
    }
	return TRUE;

}

int MASCHLST::TestAppend (void)
{
    switch (syskey)
    {
         case KEY6 :
           return 0;
         case KEY7 :
           return 0;
         case KEY8 :
           return 0;
    }
    return 1;
}

BOOL MASCHLST::OnKey6 (void)
{
//    eListe.InsertLine ();
    return TRUE;
}

BOOL MASCHLST::OnKeyInsert (void)
{
    return FALSE;
}

BOOL MASCHLST::OnKey7 (void)
{
//    eListe.DeleteLine ();
    return TRUE;
}

BOOL MASCHLST::OnKeyDel (void)
{
    return FALSE;
}

BOOL MASCHLST::OnKey8 (void)
{
         return FALSE;
}


BOOL MASCHLST::OnKey9 (void)
{
    char * Item = eListe.GetAktItem ();
    ShowMaschNr ();
    return TRUE;
}


BOOL MASCHLST::OnKey10 (void)
{
    if (Dlg != NULL)
    {
         work->SetMaschLsts (&Lsts);
// Hier Workfunktion einsetzen
         SetListFocus ();
         return TRUE;
	}
    return FALSE;
}

BOOL MASCHLST::OnKey11 (void)
{

    if (Dlg != NULL)
    {
        ((SpezDlg *) Dlg)->OnKey11 ();
        SetAktFocus ();
    }
    return FALSE;
}


int MASCHLST::ToMemory (int i)
{

    if (ratod (Lsts.a) <= 0.0)
    {
        return 0;
    }
    LISTENTER::ToMemory (i);
    return 0;
}


void MASCHLST::InitRow (void)
{

    LISTENTER::InitRow ();


    work->SetMaschLsts (&Lsts);
    work->InitMaschRow ();

    if (ArrDown != NULL)
    {
        Lsts.chmasch_nr.SetBitMap (ArrDown);
    }


}

void MASCHLST::uebertragen (void)
{
    work->SetMaschLsts (&Lsts);
    work->FillMaschRow ();

    if (ArrDown != NULL)
    {
        Lsts.chmasch_nr.SetBitMap (ArrDown);
    }
}

void MASCHLST::WriteRec (int i)
{
    memcpy (&Lsts, &LstTab[i], sizeof (Lsts));
    work->SetMaschLsts (&LstTab[i]);
    work->WriteMaschRec ();
}

BOOL MASCHLST::OnKey12 (void)
{
    StopList ();
    syskey = KEY12;
    if (work != NULL)
    {
        work->CommitWork ();
    }
    return TRUE;
}


BOOL MASCHLST::OnKeyUp (void)
{
    return FALSE;
}

BOOL MASCHLST::OnKeyDown (void)
{

    return FALSE;
}


void MASCHLST::CreateListQuikInfos (HWND hWnd)
{
       if (Toolbar2 == NULL) return;

       Toolbar2->GetCfield ("f5")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("f12")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("f9")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("del")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("insert")->CreateQuikInfos (hWnd);
}


BOOL MASCHLST::OnCommand (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    if (LOWORD (wParam) == IDM_CHOISE)
    {
        return OnKey9 ();
    }
    if (LOWORD (wParam) == IDM_CANCEL)
    {
        return OnKey5 ();
    }
    if (LOWORD (wParam) == IDM_WRITE)
    {
        return OnKey12 ();
    }
    if (LOWORD (wParam) == VK_DELETE)
    {
        return OnKeyDel ();
    }
    if (LOWORD (wParam) == IDM_DELETE)
    {
        return OnKey7 ();
    }
    if (LOWORD (wParam) == VK_INSERT)
    {
        return OnKeyInsert ();
    }
    if (LOWORD (wParam) == IDM_INSERT)
    {
        return OnKey6 ();
    }
    if (HIWORD (wParam)  == CBN_CLOSEUP)
	{
        if (Dlg != NULL)
        {
	           return ((SpezDlg *) Dlg)->OnCloseUp (hWnd, msg, wParam, lParam);
        }
	}
    if (HIWORD (wParam)  == CBN_DROPDOWN)
	{
/*
        if (strcmp (DataForm.mask[eListe.GetAktColumn ()].item->GetItemName (),
            "nr") == 0)
        {
             ShowNr ();
             eListe.CloseDropDown ();
             return TRUE;
        }
*/
	}
    return FALSE;
}

BOOL MASCHLST::OnPaint (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    HDC hdc;

    if (Modal == FALSE) return FALSE;
    if (this->mamain1 != hWnd) return FALSE; 
    
    hdc = BeginPaint (hWnd, &ps);
    fChoise.display (hWnd, hdc);
    EndPaint (hWnd,&ps);
    return FALSE;
}

BOOL MASCHLST::OnMove (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{

    if (Modal == FALSE) return FALSE;
    if (this->mamain1 != hWnd) return FALSE; 

    fChoise.MoveWindow ();
    return FALSE;
}








