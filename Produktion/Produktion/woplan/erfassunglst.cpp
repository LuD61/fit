//240111
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include <string.h>
#include "comcthlp.h"
#include "itemc.h"
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_menu.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_intp.h"
#include "inflib.h"
#include "spezdlg.h"
#include "enterfunc.h"
#include "erfassunglst.h"
#include "spezdlg.h"
#include "searchmasch.h"
#include "searcherf.h"
#include "Text.h"
// #include "searchvarb.h"
// #include "dlg.h"

#define IDM_CANCEL 3001
#ifndef IDM_WRITE
#define IDM_WRITE  3002
#endif

static int StdSize = STDSIZE;

static int dlgsize    = STDSIZE;
static int ltgraysize = STDSIZE;
static int dlgpossize = STDSIZE;
static int cpsize     = 200;


static mfont dlgfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         GRAYCOL,
                         0,
                         NULL};


static ColButton Ctxt    = { "Text",  10, -1, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, -1, -1,
                             NULL,  0, 0,
                             BLACKCOL,
                             LTGRAYCOL,
                             0, 
};


struct ERFLSTS  ERFLST::Lsts;
struct ERFLSTS *ERFLST::LstTab;
char *ERFLST::InfoItem = "a";
Work *ERFLST::work = NULL;
short ERFLST::kw;
short ERFLST::jr;

extern short SperreGesetzt;
extern double AktMe;

CFIELD *ERFLST::_fChoise0 [] = {
    new CFIELD ("cancel", " abbrechen ", 10, 0, 0, 0, NULL, "",
                CBUTTON, 
                IDM_CANCEL, &dlgfont, 0, 0),
    new CFIELD ("write", " speichern ", 10, 0,12, 0, NULL, "",
                CBUTTON, 
                IDM_WRITE, &dlgfont, 0, 0),
    new CFIELD ("show",  "  ansehen  ", 10, 0,24, 0, NULL, "",
                CBUTTON, 
                IDM_CHOISE, &dlgfont, 0, 0),
    new CFIELD ("delete","  l�schen  ", 10, 0,36, 0, NULL, "",
                CBUTTON, 
                IDM_DELETE, &dlgfont, 0, 0),
    new CFIELD ("insert"," einf�gen  ", 10, 0,48, 0, NULL, "",
                CBUTTON, 
                IDM_INSERT, &dlgfont, 0, 0),
    NULL,
};

CFORM ERFLST::fChoise0 (5, _fChoise0);

CFIELD *ERFLST::_fChoise [] = {
    new CFIELD ("fChoise0", (char *) &fChoise0, 0, 0, -1 , -2, NULL, "",
                CFFORM, 
                500, &dlgfont, 0, 0),
    NULL,
};

CFORM ERFLST::fChoise (1, _fChoise);


ITEM ERFLST::ua                  ("a",                  "Artikel",          "", 0);
ITEM ERFLST::ua_bz1              ("a_bz1",              "Bezeichnung",      "", 0);  
ITEM ERFLST::uchg                ("chgsize",            "ChgGr��e.",        "", 0);
ITEM ERFLST::ume                 ("me",                 "Menge",            "", 0);
ITEM ERFLST::ume_einh              ("me_einh",          "Einheit",      "", 0);
ITEM ERFLST::ufiller             ("",                   " ",                "", 0);

ITEM ERFLST::iline ("", "1", "", 0);

ITEM ERFLST::ia                  ("a",                  Lsts.a,                   "", 0);
ITEM ERFLST::ia_bz1              ("a_bz1",              Lsts.a_bz1,               "", 0);  
ITEM ERFLST::ichg                ("chgsize",            Lsts.chg,                 "", 0);
ITEM ERFLST::ime                 ("me",                 Lsts.me,                  "", 0);
ITEM ERFLST::ime_einh            ("me_einh",            Lsts.me_einh,             "", 0);
ITEM ERFLST::icha           ("cha",           (char *) &Lsts.cha,   "", 0);

field ERFLST::_UbForm [] = {
&ua,         21, 0, 0,  6,  NULL, "", BUTTON, 0, 0, 0,     
&ua_bz1,     45, 0, 0, 26,  NULL, "", BUTTON, 0, 0, 0, 
//&uchg,       15, 0, 0, 60,  NULL, "", BUTTON, 0, 0, 0, 
&ume,        15, 0, 0, 71,  NULL, "", BUTTON, 0, 0, 0, 
&ume_einh,     15, 0, 0, 86,  NULL, "", BUTTON, 0, 0, 0, 
&ufiller, 120, 0, 0 , 165,  NULL, "", BUTTON, 0, 0, 0,
};


form ERFLST::UbForm = {5, 0, 0, _UbForm, 0, 0, 0, 0, NULL};

field ERFLST::_LineForm [] = {
&iline,   1, 0, 0, 26,  NULL, "",  DISPLAYONLY, 0, 0, 0, 
//&iline,   1, 0, 0, 60,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0, 71,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0, 86,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0, 101,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,165,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
};

form ERFLST::LineForm = {5, 0, 0, _LineForm,0, 0, 0, 0, NULL};

field ERFLST::_DataForm [] = {
&ia,         17, 0, 0,  7,  NULL, "%6.0f", DISPLAYONLY, 0, 0, 0,     
&icha,      2, 1, 0, 24,   NULL, "",      REMOVED,      0, 0, 0,
&ia_bz1,     42, 0, 0, 27,  NULL, "",       DISPLAYONLY, 0, 0, 0, 
//&ichg,       13, 0, 0, 61,  NULL, "%12.3f", DISPLAYONLY, 0, 0, 0, 
&ime,        14, 0, 0, 71,  NULL, "%13.3f", EDIT,        0, 0, 0, 
&ime_einh,     15, 0, 0, 87,  NULL, "",    DISPLAYONLY,        0, 0, 0, 
};


form ERFLST::DataForm = {5, 0, 0, _DataForm,0, 0, 0, 0, NULL};

int ERFLST::ubrows [] = {0, 
                           1,
                           2,
                           3, 
                           4,
                           5,
                         -1,
};


struct CHATTR ERFLST::ChAttra [] = {
                                      "a",  DISPLAYONLY,EDIT,                
                                      "cha",   REMOVED,BUTTON,               
                                      NULL,  0,           0};

struct CHATTR *ERFLST::ChAttr = ChAttra;
int ERFLST::NoRecNrSet = FALSE;




    
void ERFLST::CallInfoEx (void)
/*
Info wird aus Dialog aufgerufen.
*/

{
   char where [512];
   
   sprintf (where, "where a = %.0lf", Lsts.a);
   _CallInfoEx (hMainWindow, NULL, InfoItem, where, 0l); 
}

void ERFLST::SetButtonSize (int Size)
{
    int fpos;

    fpos = GetItemPos (&DataForm, "chmasch_nr");
    if (fpos < 0) return;

    if (Size == SMALL)
    {
         DataForm.mask [fpos].length = 2;
         DataForm.mask [fpos].rows = 1;
    }
    else 
    {
         DataForm.mask [fpos].length = 3;
         DataForm.mask [fpos].rows = 0;
    }
    fpos = GetItemPos (&DataForm, "cha");
    if (fpos < 0) return;

    if (Size == SMALL)
    {
         DataForm.mask [fpos].length = 2;
         DataForm.mask [fpos].rows = 1;
    }
    else 
    {
         DataForm.mask [fpos].length = 3;
         DataForm.mask [fpos].rows = 0;
    }

}


void ERFLST::GetCfgValues (void)
{
       char cfg_v [512];
       COLORREF Color;

       if (ProgCfg == NULL) return;
	   if (cfgOK) return;

       cfgOK = TRUE;
       if (ProgCfg->GetCfgValue ("3DList", cfg_v) == TRUE)
       {
                    eListe.Set3D (atoi (cfg_v));
	   }
       if (ProgCfg->GetCfgValue ("3DSize", cfg_v) == TRUE)
       {
                    eListe.SetPlus3D (atoi (cfg_v));
	   }
       if (ProgCfg->GetCfgValue ("ListFocus", cfg_v) == TRUE)
       {
                    ListFocus = atoi (cfg_v);
	   }
        if (ProgCfg->GetCfgValue ("rowheight", cfg_v) == TRUE)
		{
			        RowHeight = ratod (cfg_v);
					if (RowHeight < (double) 1.0)
					{
						RowHeight = (double) 1.0;
					}
		}
        if (ProgCfg->GetCfgValue ("ubheight", cfg_v) == TRUE)
		{
			        UbHeight = atoi (cfg_v);
					SetUbHeight ();
		}
		eListe.SetRowHeight (RowHeight);


        if (ProgCfg->GetCfgValue ("NoRecNr", cfg_v) == TRUE)
        {
			        NoRecNr = atoi (cfg_v);
		            if (atoi (cfg_v))
					{
	                        SetNoRecNr ();
					}
		}
        if (ProgCfg->GetCfgValue ("ListColor", cfg_v) == TRUE)
        {
                     GetListColor (&Color, cfg_v);
		             eListe.SetColor (Color);
        }
        if (ProgCfg->GetCfgValue ("ListBkColor", cfg_v) == TRUE)
        {
                     GetListColor (&Color, cfg_v);
		             eListe.SetBkColor (Color);
        }
        if (ProgCfg->GetCfgValue ("ListLine", cfg_v) == TRUE)
        {
		             eListe.SetLines (GetListLine (cfg_v));
        }
        if (ProgCfg->GetCfgValue ("VarbInfo", cfg_v) == TRUE)
        {
                     InfoItem = new char [80];
                     strcpy (InfoItem, cfg_v);
        }
}


void ERFLST::TestRemoves (void)
{
/*
        int fpos;

        fpos = GetItemPos (&DataForm, "feld3");
        if (fpos >= 0)
        {
            if (DataForm.mask[fpos].attribut & REMOVED)
            {
                DestroyListField ("feld3");
            }
        }
*/
}

void ERFLST::SetNoRecNr (void)
{
	   int i;

	   eListe.SetNoRecNr (TRUE);
	   if (NoRecNrSet) return;

	   for (i = 0; i < dataform->fieldanz; i ++)
	   {
		   dataform->mask[i].pos[1] -= 6;
	   }
	   for (i = 0; i < ubform->fieldanz; i ++)
	   {
		   ubform->mask[i].pos[1] -= 6;
	   }
	   for (i = 0; i < lineform->fieldanz; i ++)
	   {
		   lineform->mask[i].pos[1] -= 6;
	   }
       NoRecNrSet = TRUE;
}


ERFLST::ERFLST (short mdn, long dat) : KEYS (), LISTENTER () 
{

    ProgCfg = new PROG_CFG ("woplan");
	Dlg = NULL;
    cfgOK = FALSE;
	cursor = -1;
    prdk_k.mdn = mdn;
    prstatatag.mdn = mdn;
    AktDat = dat;

    LstTab = new ERFLSTS [0x10000];

    dataform  = &DataForm;
    ubform    = &UbForm;
    lineform  = &LineForm;
//    DelListField ("kosten");
//    SetFieldLen ("a", 9);
    eListe.SetUbRows (ubrows); 
	eListe.SetKeys (this);

    if ((work.ArtScroll == FALSE))
    {

           eListe.SetHscrollStart (3);
           UbForm.ScrollStart = 3;
    }

    LISTENTER::GetCfgValues ();
    GetCfgValues ();
    TestRemoves ();
    SetNoRecNr ();
	fChoise.Setchpos (0, 2);
    work = NULL;
    ChAttr = ChAttra;
    eListe.SetChAttr (ChAttr); 
    Toolbar2 = NULL;
//160107    eListe.SetTestAppend (TestAppend);
//    eListe.SetInfoProc (InfoProc);
    eListe.SetInfo (this);
    ArrDown = NULL;
    ArrDown = LoadBitmap (DLG::hInstance, "ARRDOWN");
    eListe.SetListclipp (TRUE);
    eListe.SetNoScrollBkColor (GetSysColor (COLOR_3DFACE));
//    Ctxt.BkColor = GetSysColor (COLOR_3DFACE);
}


ERFLST::~ERFLST ()
{
    int i, anz;
	if (work != NULL)
	{
          work->SetListDlg (NULL);
	}
    fChoise.destroy ();
	if (LstTab != NULL)
	{

        anz =   eListe.GetRecanz ()	;
        for (i = 0; i < anz; i ++)
        {
        }

  	    delete []LstTab;
		LstTab = NULL;
	}
    if (ProgCfg != NULL)
    {
        ProgCfg->CloseCfg ();
        delete ProgCfg;
        ProgCfg = NULL;
    }
    if (work != NULL)
    {
        work->SetErfLsts (NULL);
    }

}


// Funktionen f�r den Zugriff auf die Daten. Sind als abstrakte Schnittstellen in
// listenter definiert.

unsigned char *ERFLST::GetDataRec (void)
{
    return (unsigned char *) &Lsts;
}

int ERFLST::GetDataRecLen (void)
{
    return sizeof (struct ERFLSTS);
}

char *ERFLST::GetListTab (int i)
{
    return (char *) &LstTab[i];
}

// Ende der Schnittstellen.

/*
void ERFLST::Setfeld2Removed (BOOL removed)
{
    if (removed)
    {
            SetFieldAttr ("feld2", REMOVED);
    }
    else
    {
            SetFieldAttr ("feld2", EDIT);
    }
}
*/

void ERFLST::Prepare (void)
{
    if (work != NULL)
    {
        cursor = work->PrepareErf ();
    }
}

void ERFLST::Open (void)
{
	if (cursor == -1)
	{
		Prepare ();
		if (cursor == -1) return;
	}
    if (work != NULL)
    {
        work->OpenErf ();
    }
}

int ERFLST::Fetch (void)
{
	if (cursor == -1)
	{
		Prepare ();
		if (cursor == -1) return 100;
	}
    if (work != NULL)
    {
        return work->FetchErf ();
    }
	return 100;
}


void ERFLST::Close (void)
{
	if (cursor != -1)
	{
		cursor = -1;
	}
    if (work != NULL)
    {
        work->CloseErf ();
    }
}



BOOL ERFLST::BeforeCol (char *colname)
{
         double a = ratod (Lsts.a);
         char dat [12];
 
         dlong_to_asc (AktDat, dat);

         ((SpezDlg *) Dlg)->SetChargen (0, 0, a);

         SperreGesetzt = ((SpezDlg *) Dlg)->SetSperreErf (dat, a,Lsts.posi);

		AktMe = ratod(Lsts.me);

         return FALSE;
}
         

BOOL ERFLST::AfterCol (char *colname)
{
         double a = ratod (Lsts.a);
         char dat [12];
		 double me = 0.0;
 
		 me = ratod(Lsts.me);
         dlong_to_asc (AktDat, dat);
         
         if (strcmp (colname, "me") == 0)
         {
              memcpy (&LstTab[eListe.GetAktRow ()], &Lsts, sizeof (Lsts));
              eListe.ShowAktRow ();

					if (AktMe != me) 
					{
						work->MaschStatusZurueck(dat,a,prstatatag.mdn);
					}
					if (SperreGesetzt >= 1) //100905 jetzt auch neue Zeile bei Grundbr�t in bearb.
					{
						if (AktMe != me) 
						{
							print_mess (1, "Planmenge kann nur noch auf der 1. Seite ver�ndert werden");
							syskey = KEYCR;
							sprintf(Lsts.me,"%.2f", AktMe);
						    eListe.ShowAktRow ();
							return TRUE; //auf dem Feld stehenbleiben
						}
					}
//310805					if (SperreGesetzt == 2 & (me - AktMe) < 0.0)  //Grundbr�t in Bearb.
					if (SperreGesetzt == 2 && (me - AktMe) < 0.0)  //Grundbr�t in Bearb.
					{
						print_mess (1, "Planmenge kann nicht mehr verkleinert werden");
						syskey = KEYCR;
							sprintf(Lsts.me,"%.2f", AktMe);
						    eListe.ShowAktRow ();
						return TRUE;
					}

              return FALSE;
         }
         else if (strcmp (colname, "cha") == 0)
         {
             if (eListe.IsNewRec ())
             {
                    ShowNr ();
             }
             return TRUE;
         }
         else if (strcmp (colname, "a") == 0)
         {
             if (TestNr () == FALSE)
             {
                 return TRUE;
             }
         }

         return FALSE;
}

int ERFLST::AfterRow (int Row)
{
//         form * DataForm; 

         if (syskey == KEYUP && ratod (Lsts.a) == 0.0)
         {
                return OnKey7 ();
         }
         memcpy (&LstTab [Row], &Lsts, sizeof (Lsts));
         work->SetErfLsts (&Lsts);
         return 1;
}

int ERFLST::BeforeRow (int Row)
{
         memcpy (&LstTab [Row], &Lsts, sizeof (Lsts));
         double a = ratod (Lsts.a);
         ((SpezDlg *) Dlg)->SetChargen (0, 0, a);

         return 1;
}

/****
int ERFLST::ShowMaschNr (void)
{

        struct SMASCH *smasch;
        SEARCHMASCH SearchMasch;


        SearchMasch.SetWindowStyle (WS_POPUP | WS_THICKFRAME);
        SearchMasch.SetParams (DLG::hInstance, GetParent (hMainWindow));
        if (Dlg != NULL)
        {
               SearchMasch.Setawin (((SpezDlg *) Dlg)->GethMainWindow ());
        }
        SearchMasch.Search ();
        if (syskey == KEY5)
        {
            syskey = 0;
            SetListFocus ();
            return 0;
        }

        smasch = SearchMasch.GetSMasch ();
        if (smasch == NULL)
        {
            syskey = 0;
            SetListFocus ();
            return 0;
        }

        int chpos = eListe.GetAktColumn ();
        if (chpos != -1)
        {
              strcpy (DataForm.mask[chpos].item->GetFeldPtr (),
                      smasch->masch_nr);
              memcpy (&LstTab [eListe.GetAktRow ()], &Lsts, sizeof (Lsts));
              eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
              eListe.ShowAktRow ();
        }
        SetListFocus ();
        return 0;
}

int ERFLST::ShowMaschNr (char *colname)
{

        struct SMASCH *smasch;
        SEARCHMASCH SearchMasch;


        SearchMasch.SetWindowStyle (WS_POPUP | WS_THICKFRAME);
        SearchMasch.SetParams (DLG::hInstance, GetParent (hMainWindow));
        if (Dlg != NULL)
        {
               SearchMasch.Setawin (((SpezDlg *) Dlg)->GethMainWindow ());
        }
        SearchMasch.Search ();
        if (syskey == KEY5)
        {
            syskey = 0;
            SetListFocus ();
            return 0;
        }

        smasch = SearchMasch.GetSMasch ();
        if (smasch == NULL)
        {
            syskey = 0;
            SetListFocus ();
            return 0;
        }

        int chpos = GetItemPos (&DataForm, colname);
        if (chpos != -1)
        {
              strcpy (DataForm.mask[chpos].item->GetFeldPtr (),
                      smasch->masch_nr);
              memcpy (&LstTab [eListe.GetAktRow ()], &Lsts, sizeof (Lsts));
              eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
              eListe.ShowAktRow ();
        }
        SetListFocus ();
        return 0;
}


BOOL ERFLST::NewMaschNr (long MnrTab[], long masch_nr, int NrPos)
{
    if (masch_nr == 0)
    {
        return FALSE;
    }

    for (int i = 0; i < NrPos; i ++)
    {
        if (MnrTab[i] == masch_nr)
        {
            return FALSE;
        }
    }
    return TRUE;
}

*****************/

BOOL ERFLST::StopList (void)
{
    int i;
    int recanz;
    double chg_gew = 0.0;
    Text WText;
    long MnrTab[256];
    int NrPos = 0;     
    
    if (AfterRow (eListe.GetAktRow ()) == -1)
    {
        return TRUE;
    }

    eListe.GetFocusText ();
    recanz = eListe.GetRecanz ();


    work->SetzeMenge0 ();
    work->DeleteVerbrTag (prdk_k.mdn,AktDat,work->GetAbt()); //050207

    for (i = 0; i < recanz; i ++)
    {
        WriteRec (i);
    }
    work->DeleteAllMe0 ();
    if (NrPos > 0)
    {
        WText = "Die Reihenfolge f�r folgende Maschinen muss bearbeitet werden\nMaschinen : ";
        for (i = 0; i < NrPos; i ++)
        {
            if (i > 0)
            {
                WText + ",";
            }
            WText.Format ("%s%ld ", WText.GetBuffer (), MnrTab[i]);
        }
        disp_mess (WText.GetBuffer (), 1);
    }
	return TRUE;
}

BOOL ERFLST::OnKeyNext (void)
{
        if (GetKeyState (VK_CONTROL) >= 0)
        {
              if (Dlg != NULL)
              {
                  ((SpezDlg *) Dlg)->NextPage ();
                  return TRUE;
              }
        }
        return FALSE;
}


BOOL ERFLST::OnKeyPrior (void)
{
        if (GetKeyState (VK_CONTROL) >= 0)
        {
              if (Dlg != NULL)
              {
                  ((SpezDlg *) Dlg)->PriorPage ();
                  return TRUE;
              }
        }
        return FALSE;
}


BOOL ERFLST::OnKey5 (void)
{

//    StopList ();
    syskey = KEY5;
    if (work != NULL)
    {
        work->RollbackWork ();
    }
	return TRUE;

}

/*** 160107
int ERFLST::TestAppend (void)
{
    switch (syskey)
    {
         case KEY6 :
           return 0;
         case KEY7 :
           return 0;
         case KEY8 :
           return 0;
    }
    return 1;
}
****/

BOOL ERFLST::OnKey6 (void)
{
    eListe.InsertLine ();
    return TRUE;
}

BOOL ERFLST::OnKeyInsert (void)
{
    return FALSE;
}

BOOL ERFLST::OnKey7 (void)
{
    eListe.DeleteLine ();
    return TRUE;
}

BOOL ERFLST::OnKeyDel (void)
{
    return FALSE;
}

BOOL ERFLST::OnKey8 (void)
{
         return FALSE;
}


BOOL ERFLST::OnKey9 (void)
{
    char * Item = eListe.GetAktItem ();
    if (strcmp (Item, "a") == 0)
    {
          ShowNr ();
    }
    return TRUE;
}


BOOL ERFLST::OnKey10 (void)
{
    if (Dlg != NULL)
    {
         work->SetErfLsts (&Lsts);
// Hier Workfunktion einsetzen
         SetListFocus ();
         return TRUE;
	}
    return FALSE;
}

BOOL ERFLST::OnKey11 (void)
{

    if (Dlg != NULL)
    {
        ((SpezDlg *) Dlg)->OnKey11 ();
        SetAktFocus ();
    }
    return FALSE;
}


int ERFLST::ToMemory (int i)
{

//    if (atol (Lsts.masch_nr) != work->GetAktMaschNr ())
//    {
//        return 0;
//    }
    LISTENTER::ToMemory (i);
    return 0;
}


void ERFLST::InitRow (void)
{

    LISTENTER::InitRow ();


    work->SetErfLsts (&Lsts);
    work->InitMaschRow ();

//    if (ArrDown != NULL)
//    {
//        Lsts.chmasch_nr.SetBitMap (ArrDown);
//    }
    if (ArrDown != NULL)
    {
        Lsts.cha.SetBitMap (ArrDown);
    }


}

void ERFLST::uebertragen (void)
{
    work->SetErfLsts (&Lsts);
    work->FillErfRow ();

//    if (ArrDown != NULL)
//    {
//        Lsts.chmasch_nr.SetBitMap (ArrDown);
//    }
    if (ArrDown != NULL)
    {
        Lsts.cha.SetBitMap (ArrDown);
    }
}

void ERFLST::WriteRec (int i)
{
    memcpy (&Lsts, &LstTab[i], sizeof (Lsts));
    work->SetErfLsts (&LstTab[i]);
    work->WriteErfRec ();
}

BOOL ERFLST::OnKey12 (void)
{
    StopList ();
    syskey = KEY12;
    if (work != NULL)
    {
        work->CommitWork ();
    }
    return TRUE;
}


BOOL ERFLST::OnKeyUp (void)
{
    return FALSE;
}

BOOL ERFLST::OnKeyDown (void)
{
    if (atol (Lsts.a) == 0l)
    {
        return TRUE;
    }

    return FALSE;
}


void ERFLST::CreateListQuikInfos (HWND hWnd)
{
       if (Toolbar2 == NULL) return;

       Toolbar2->GetCfield ("f5")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("f12")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("f9")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("del")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("insert")->CreateQuikInfos (hWnd);
}


BOOL ERFLST::OnCommand (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    if (LOWORD (wParam) == IDM_CHOISE)
    {
        return OnKey9 ();
    }
    if (LOWORD (wParam) == IDM_CANCEL)
    {
        return OnKey5 ();
    }
    if (LOWORD (wParam) == IDM_WRITE)
    {
        return OnKey12 ();
    }
    if (LOWORD (wParam) == VK_DELETE)
    {
        return OnKeyDel ();
    }
    if (LOWORD (wParam) == IDM_DELETE)
    {
        return OnKey7 ();
    }
    if (LOWORD (wParam) == VK_INSERT)
    {
        return OnKeyInsert ();
    }
    if (LOWORD (wParam) == IDM_INSERT)
    {
        return OnKey6 ();
    }
    if (HIWORD (wParam)  == CBN_CLOSEUP)
	{
        if (Dlg != NULL)
        {
	           return ((SpezDlg *) Dlg)->OnCloseUp (hWnd, msg, wParam, lParam);
        }
	}
    if (HIWORD (wParam)  == CBN_DROPDOWN)
	{
/*
        if (strcmp (DataForm.mask[eListe.GetAktColumn ()].item->GetItemName (),
            "nr") == 0)
        {
             ShowNr ();
             eListe.CloseDropDown ();
             return TRUE;
        }
*/
	}
    return FALSE;
}

BOOL ERFLST::OnPaint (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    HDC hdc;

    if (Modal == FALSE) return FALSE;
    if (this->mamain1 != hWnd) return FALSE; 
    
    hdc = BeginPaint (hWnd, &ps);
    fChoise.display (hWnd, hdc);
    EndPaint (hWnd,&ps);
    return FALSE;
}

BOOL ERFLST::OnMove (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{

    if (Modal == FALSE) return FALSE;
    if (this->mamain1 != hWnd) return FALSE; 

    fChoise.MoveWindow ();
    return FALSE;
}


BOOL ERFLST::TestNr (void)
{
         char me_einh [5]; 
         double a_gew;
         double inh_prod;

         work->SetErfLsts (&Lsts);
         if (work->GetErfMatName () == FALSE)
         {
             disp_mess ("Falsche Material-Nr oder falsche Abteilung", 2);
             eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
             eListe.ShowAktRow ();
             SetListFocus ();
             return FALSE;
         }
         work->GetErfAbz1 (Lsts.a_bz1, me_einh, &inh_prod,&a_gew, ratod (Lsts.a),Lsts.rez);
         work->GetPtab (Lsts.me_einh, "me_einh", me_einh);
         if (atoi (me_einh) == 0)
         {
                  strcpy (me_einh, "2");
         }
         work->GetPtab (Lsts.me_einh, "me_einh", me_einh);
//		 Lsts.inh_ek = a_preis.inh_ek;
         if (atoi (me_einh) == 2)
         {
                 a_gew = 1.0;
         }
//         Lsts.a_gew = a_gew;
//         sprintf (Lsts.kutf, "%d", KtfNext ());
         memcpy (&LstTab[eListe.GetAktRow ()], &Lsts, sizeof (Lsts));
         eListe.ShowAktRow ();
         return TRUE;
}


int ERFLST::ShowNr (void)
{

        SEARCHERF SearchErf;
        struct SERF *serf;

       SearchErf.SetWindowStyle (WS_POPUP | WS_THICKFRAME);
       SearchErf.SetParams (DLG::hInstance, GetParent (hMainWindow));

        if (Dlg != NULL)
        {
               SearchErf.Setawin (((SpezDlg *) Dlg)->GethMainWindow ());
        }
           
//240111 lies sich nicht kompillieren !		SearchErf.SetAbt(abt,ProdAbt);
        SearchErf.Search ();
        if (syskey == KEY5)
        {
            syskey = 0;
            SetListFocus ();
            return 0;
        }
        serf = SearchErf.GetSerf ();
        if (serf == NULL)
        {
            return 0;
        }

        sprintf (LstTab [eListe.GetAktRow ()].a, "%.0lf", ratod (serf->a));
        memcpy (&Lsts, &LstTab [eListe.GetAktRow ()], sizeof (Lsts));
        TestNr ();
        eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
        eListe.ShowAktRow ();
        SetListFocus ();
        return 0;
}




