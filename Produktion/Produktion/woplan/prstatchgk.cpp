#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "wmaskc.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "prstatchgk.h"

struct PRSTATCHGK prstatchgk, prstatchgk_null;

void PRSTATCHGK_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *)   &prstatchgk.mdn, 1, 0);
            ins_quest ((char *)   &prstatchgk.fil, 1, 0);
            ins_quest ((char *)   &prstatchgk.a,  3, 0);
            ins_quest ((char *)   &prstatchgk.chargennr,  2, 0);
            ins_quest ((char *)   &prstatchgk.dat,  2, 0);
    out_quest ((char *) &prstatchgk.mdn,1,0);
    out_quest ((char *) &prstatchgk.fil,1,0);
    out_quest ((char *) &prstatchgk.dat,2,0);
    out_quest ((char *) &prstatchgk.a,3,0);
    out_quest ((char *) prstatchgk.chargennr,0,14);
    out_quest ((char *) &prstatchgk.me_ist,3,0);
    out_quest ((char *) &prstatchgk.gew,3,0);
    out_quest ((char *) &prstatchgk.varb_ok,1,0);
    out_quest ((char *) &prstatchgk.zut_ok,1,0);
    out_quest ((char *) &prstatchgk.huell_ok,1,0);
    out_quest ((char *) prstatchgk.rez,0,9);
    out_quest ((char *) &prstatchgk.plan,1,0);
    out_quest ((char *) &prstatchgk.stat,1,0);
    out_quest ((char *) prstatchgk.chg_fest,0,14);
    out_quest ((char *) &prstatchgk.chg_lfd,1,0);
    out_quest ((char *) &prstatchgk.prodabt,1,0);
    out_quest ((char *) &prstatchgk.posi,1,0);
            cursor = prepare_sql ("select prstatchgk.mdn,  "
"prstatchgk.fil,  prstatchgk.dat,  prstatchgk.a,  prstatchgk.chargennr,  "
"prstatchgk.me_ist,  prstatchgk.gew,  prstatchgk.varb_ok,  "
"prstatchgk.zut_ok,  prstatchgk.huell_ok,  prstatchgk.rez,  "
"prstatchgk.plan,  prstatchgk.stat,  prstatchgk.chg_fest,  "
"prstatchgk.chg_lfd,  prstatchgk.prodabt,  prstatchgk.posi from prstatchgk "

#line 24 "prstatchgk.rpp"
                                  "where mdn = ? "
                                  "and fil = ? "
                                  "and   a   = ? "
                                  "and chargennr = ? "
                                  "and   dat = ?");
    ins_quest ((char *) &prstatchgk.mdn,1,0);
    ins_quest ((char *) &prstatchgk.fil,1,0);
    ins_quest ((char *) &prstatchgk.dat,2,0);
    ins_quest ((char *) &prstatchgk.a,3,0);
    ins_quest ((char *) prstatchgk.chargennr,0,14);
    ins_quest ((char *) &prstatchgk.me_ist,3,0);
    ins_quest ((char *) &prstatchgk.gew,3,0);
    ins_quest ((char *) &prstatchgk.varb_ok,1,0);
    ins_quest ((char *) &prstatchgk.zut_ok,1,0);
    ins_quest ((char *) &prstatchgk.huell_ok,1,0);
    ins_quest ((char *) prstatchgk.rez,0,9);
    ins_quest ((char *) &prstatchgk.plan,1,0);
    ins_quest ((char *) &prstatchgk.stat,1,0);
    ins_quest ((char *) prstatchgk.chg_fest,0,14);
    ins_quest ((char *) &prstatchgk.chg_lfd,1,0);
    ins_quest ((char *) &prstatchgk.prodabt,1,0);
    ins_quest ((char *) &prstatchgk.posi,1,0);
            sqltext = "update prstatchgk set "
"prstatchgk.mdn = ?,  prstatchgk.fil = ?,  prstatchgk.dat = ?,  "
"prstatchgk.a = ?,  prstatchgk.chargennr = ?,  "
"prstatchgk.me_ist = ?,  prstatchgk.gew = ?,  "
"prstatchgk.varb_ok = ?,  prstatchgk.zut_ok = ?,  "
"prstatchgk.huell_ok = ?,  prstatchgk.rez = ?,  prstatchgk.plan = ?,  "
"prstatchgk.stat = ?,  prstatchgk.chg_fest = ?,  "
"prstatchgk.chg_lfd = ?,  prstatchgk.prodabt = ?,  "
"prstatchgk.posi = ? "

#line 30 "prstatchgk.rpp"
                                  "where mdn = ? "
                                  "and fil = ? "
                                  "and   a   = ? "
                                  "and chargennr = ? "
                                  "and dat = ?";
            ins_quest ((char *)   &prstatchgk.mdn, 1, 0);
            ins_quest ((char *)   &prstatchgk.fil, 1, 0);
            ins_quest ((char *)   &prstatchgk.a,  3, 0);
            ins_quest ((char *)   &prstatchgk.chargennr,  2, 0);
            ins_quest ((char *)   &prstatchgk.dat,  2, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &prstatchgk.mdn, 1, 0);
            ins_quest ((char *)   &prstatchgk.fil, 1, 0);
            ins_quest ((char *)   &prstatchgk.a,  3, 0);
            ins_quest ((char *)   &prstatchgk.chargennr,  2, 0);
            ins_quest ((char *)   &prstatchgk.dat,  2, 0);
            test_upd_cursor = prepare_sql ("select a from prstatchgk "
                                  "where mdn = ? "
                                  "and fil = ? "
                                  "and   a = ? "
                                  "and chargennr = ? "
                                  "and dat = ? "
                                  "for update");

            ins_quest ((char *) &prstatchgk.mdn, 1, 0);
            ins_quest ((char *)   &prstatchgk.fil, 1, 0);
            ins_quest ((char *)   &prstatchgk.a,  3, 0);
            ins_quest ((char *)   &prstatchgk.chargennr,  2, 0);
            ins_quest ((char *)   &prstatchgk.dat,  2, 0);
            del_cursor = prepare_sql ("delete from prstatchgk "
                                  "where mdn = ? "
                                  "and fil = ? "
                                  "and   a = ? "
                                  "and chargennr = ? "
                                  "and dat = ?");
    ins_quest ((char *) &prstatchgk.mdn,1,0);
    ins_quest ((char *) &prstatchgk.fil,1,0);
    ins_quest ((char *) &prstatchgk.dat,2,0);
    ins_quest ((char *) &prstatchgk.a,3,0);
    ins_quest ((char *) prstatchgk.chargennr,0,14);
    ins_quest ((char *) &prstatchgk.me_ist,3,0);
    ins_quest ((char *) &prstatchgk.gew,3,0);
    ins_quest ((char *) &prstatchgk.varb_ok,1,0);
    ins_quest ((char *) &prstatchgk.zut_ok,1,0);
    ins_quest ((char *) &prstatchgk.huell_ok,1,0);
    ins_quest ((char *) prstatchgk.rez,0,9);
    ins_quest ((char *) &prstatchgk.plan,1,0);
    ins_quest ((char *) &prstatchgk.stat,1,0);
    ins_quest ((char *) prstatchgk.chg_fest,0,14);
    ins_quest ((char *) &prstatchgk.chg_lfd,1,0);
    ins_quest ((char *) &prstatchgk.prodabt,1,0);
    ins_quest ((char *) &prstatchgk.posi,1,0);
            ins_cursor = prepare_sql ("insert into prstatchgk ("
"mdn,  fil,  dat,  a,  chargennr,  me_ist,  gew,  varb_ok,  zut_ok,  huell_ok,  rez,  plan,  stat,  "
"chg_fest,  chg_lfd,  prodabt,  posi) "

#line 67 "prstatchgk.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?)"); 

#line 69 "prstatchgk.rpp"
}
