//020905
//030805
#include<stdio.h>
#include<stdarg.h>
#include<string.h>
#include<windows.h>
#include "stdfkt.h"
#include "strfkt.h"
#include "spezdlg.h" 
#include "mo_gdruck.h" 
#include "colbut.h"
#include "help.h"
#include "lbox.h"
#include "searchmdn.h"
#include "searchfil.h"
#include "searchabt.h"
#include "ptab.h"
#ifdef BIWAK
#include "conf_env.h"
#endif
#include "mo_progcfg.h"
#include "datum.h"
#include "enterfunc.h"
#include "mo_menu.h"


static int StdSize = STDSIZE;

static int dlgsize    = STDSIZE;
static int ltgraysize = STDSIZE;
static int dlgpossize = STDSIZE;
static int cpsize     = 200;
extern int SperreAktiv ;
extern int ohneSamstag ;
static short TabMengen = 0;
static short TabMaschinen = 1;
static short TabReihenfolge = 2;
static short TabPersonal = 3;
static short TabErfassung = 4;

static struct PMENUE MenuePosEnter = {"Positionen erfassen F6",      "G",  NULL, IDM_LIST}; 
static struct PMENUE MenueInsert   = {"einf�gen F6",                 "G",  NULL, IDM_INSERT}; 


static mfont dlgfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         GRAYCOL,
                         0,
                         NULL};

static mfont rdofont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLUECOL,
                         WHITECOL,
                         0,
                         NULL};

static mfont infofont = {
                         "ARIAL", 150, 0, 0,
                         BLUECOL,
                         WHITECOL,
                         0,
                         NULL};

static mfont warningfont = {
                         "ARIAL", 150, 0, 0,
                         REDCOL,
                         WHITECOL,
                         0,
                         NULL};
static mfont ltgrayfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         LTGRAYCOL,
                        0,
                         NULL};

static mfont checkfonth = {
                           "ARIAL", STDSIZE, 0, 0,
                            BLACKCOL,
                            LTGRAYCOL,
                            0,
                            NULL};

static mfont dlgposfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         LTGRAYCOL,
                         0,
                         NULL};


static mfont cpfont = {
                        "Times New Roman", 200, 0, 1,
                         BLUECOL,
                         GRAYCOL,
                         0,
                         NULL};

static char *ChgTxtMe    = "Chargen";
static char *ChgTxtChg   = "Menge";
static char *ChgTxtAuf   = "Auftr�ge";
static char ChgTxt[40]   = {"Chargen"};
static char ErfInfo [40] = {"Mengenerfassung\0"};
static char SperrInfo [40] = {"\0"};
static char SummeInfo [40] = {"\0"};


char *SpezDlg::ErfInfoMe   =   "Mengenerfassung";
char *SpezDlg::ErfInfoChg  =   "Chargenerfassung";
char *SpezDlg::ErfInfoAuf  =   "Mengen aus Auftr�ge"; 
char SpezDlg::ErfInfoBu [40] = {"Chargenerfassung\0"};

struct DEFAULTFF
{
      char mdn [7];
      char mdn_krz [20];
      char fil [7];
      char fil_krz [20];
      char abt [10];
      char abt_bz1 [28];
      char kw [5];
      char jr [10];
      char chg [15];
      char chg_gew [15];
      char a [15];
      char prod_dat [15];
      char akt_phase [5];
      char akt_phase_bz [21];
      char akt_masch_nr [10];
      char akt_masch_bz [26];
      char akt_masch_me [26];
      char akt_masch_zeit [26];
      char akt_pers_nr [10];
      char akt_pers_bz [26];
      char akt_pers_zeit [26];
      char akt_prodabt [26];
      char bearb [10];
      char bearbr [10];
      char check_mo [10];
      char check_di [10];
      char check_mi [10];
      char check_do [10];
      char check_fr [10];
      char check_sa [10];
      char datvon [12];
      char datbis [12];
      char stnd [10];
      char stnd_bz1 [28];
} defaultlistf, defaultlistf_null;



COLORREF SpezDlg::Colors[] = {BLACKCOL,
                            WHITECOL,
                            WHITECOL,
                            WHITECOL};

COLORREF SpezDlg::BkColors[] = {WHITECOL,
                            BLUECOL,
                            BLACKCOL,
                            GRAYCOL};

HBITMAP SpezDlg::ListBitmap  = NULL;
HBITMAP SpezDlg::ListBitmapi = NULL;
BitImage SpezDlg::ImageDelete;
BitImage SpezDlg::ImageInsert;
BitImage SpezDlg::ImageList;
COLORREF SpezDlg::SysBkColor = LTGRAYCOL;
BOOL SpezDlg::ListButtons = FALSE;
int SpezDlg::ButtonSize = SMALL;
COLORREF SpezDlg::ListColor = BLACKCOL;
COLORREF SpezDlg::ListBkColor = WHITECOL;
int SpezDlg::ListLines = 1;
BOOL SpezDlg::LockPage = FALSE;

mfont *SpezDlg::Font = &dlgposfont;

CFIELD *SpezDlg::_fHead [] = {

// Hier Kopffelder eintragen

                     new CFIELD ("mdn_txt", "Mandant",  0, 0, 1, 1,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("mdn", defaultlistf.mdn,  6, 0, 18, 1,  NULL, "%4d", CEDIT,
                                 MDN_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("mdn_choise", "", 2, 0, 24, 1, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("mdn_krz", defaultlistf.mdn_krz, 19, 0, 30, 1,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, 0),
                     new CFIELD ("fil_txt", "Filiale",  0, 0, 50, 1,  NULL, "", 
                                 CREMOVED,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("fil", defaultlistf.fil,  6, 0, 58, 1,  NULL, "%4d", CREMOVED,
                                 FIL_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("fil_choise", "", 2, 0, 64, 1, NULL, "", CREMOVED,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("fil_krz", defaultlistf.fil_krz, 17, 0, 67, 1,  NULL, "", 
                                 CREMOVED,
                                 500, Font, 0, 0),

                     new CFIELD ("abt_txt", "Abteilung",  0, 0, 1, 2,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("abt", defaultlistf.abt, 6, 0, 18, 2,  NULL, "%4d", CEDIT,
                                 ABT_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("abt_choise", "", 2, 0, 24, 2, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("abt_bz1", defaultlistf.abt_bz1, 26, 0, 30, 2,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, 0),

                     new CFIELD ("stnd_txt", "Standardplan",  0, 0, 1, 2,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("stnd", defaultlistf.stnd, 6, 0, 18, 2,  NULL, "%4d", CEDIT,
                                 STND_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("stnd_choise", "", 2, 0, 24, 2, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("stnd_bz1", defaultlistf.stnd_bz1, 26, 0, 30, 2,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, 0),


                     new CFIELD ("kw_txt", "Woche / Jahr",  0, 0, 1, 3,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("kw", defaultlistf.kw, 4, 0, 18, 3,  NULL, "%2d", CEDIT,
                                 KW_CTL, Font, 0, ES_RIGHT),

                     new CFIELD ("w_j_txt", "/",  0, 0, 23, 3,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("jr", defaultlistf.jr, 6, 0, 25, 3,  NULL, "%4d", CEDIT,
                                 JR_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("liefdat_txt", "Lieferdatum",  0, 0, 1, 4,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("dat_von", defaultlistf.datvon, 11, 0, 18, 4,  NULL, "dd.mm.yyyy", CEDIT,
                                 DATVON_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("bis_txt", "bis",  3, 0, 30, 4,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("dat_bis", defaultlistf.datbis, 11, 0, 34, 4,  NULL, "dd.mm.yyyy", CEDIT,
                                 DATBIS_CTL, Font, 0, ES_RIGHT),
                     NULL,
};

CFORM SpezDlg::fHead (24, _fHead);

CFIELD *SpezDlg::_fHeadM [] = {

// Hier Kopffelder eintragen

                     NULL,
};

CFORM SpezDlg::fHeadM (6, _fHeadM);

CFIELD *SpezDlg::_fPos [] = {
                     new CFIELD ("a_txt", "Artikel.",  10, 0, 60, 1,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("a", defaultlistf.a, 14, 0, 75, 1,  NULL, "%13.0lf", CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("chg_gew_txt", "Chg.Gew.",  10, 0, 60, 2,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("chg_gew", defaultlistf.chg_gew, 12, 0, 75, 2,  NULL, "%10.3lf", CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("chg_txt", ChgTxt,  10, 0, 60, 3,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("chg", defaultlistf.chg, 12, 0, 75, 3,  NULL, "%10.3lf", CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("erf_info", ErfInfo,  20, 1, 49, 0,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("sperr_info", SperrInfo,  20, 0, 90, 0,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("summe_info", SummeInfo,  30, 1, 90, 2,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("pos_frame", "",    200,13, 0, 5,  NULL, "",
                                 CBORDER,   
                                 500, Font, 0, TRANSPARENT),    
                     new CFIELD ("bearb", "Bearbeitet", defaultlistf.bearb,
                                  12, 0, 35, 3,  NULL, "", CBUTTON,
                                 BEARB_CTL, Font, 0, BS_CHECKBOX | BS_LEFTTEXT),
                     new CFIELD ("check_mo", "Mon", defaultlistf.check_mo,
                                  6, 0, 50, 4,  NULL, "", CBUTTON,
                                 CHECK_MO_CTL, Font, 0, BS_AUTOCHECKBOX | BS_LEFTTEXT),
                     new CFIELD ("check_di", "Die", defaultlistf.check_di,
                                  6, 0, 63, 4,  NULL, "", CBUTTON,
                                 CHECK_DI_CTL, Font, 0, BS_AUTOCHECKBOX | BS_LEFTTEXT),
                     new CFIELD ("check_mi", "Mit", defaultlistf.check_mi,
                                  6, 0, 76, 4,  NULL, "", CBUTTON,
                                 CHECK_MI_CTL, Font, 0, BS_AUTOCHECKBOX | BS_LEFTTEXT),
                     new CFIELD ("check_do", "Don", defaultlistf.check_do,
                                  6, 0, 89, 4,  NULL, "", CBUTTON,
                                 CHECK_DO_CTL, Font, 0, BS_AUTOCHECKBOX | BS_LEFTTEXT),
                     new CFIELD ("check_fr", "Fre", defaultlistf.check_fr,
                                  6, 0, 102, 4,  NULL, "", CBUTTON,
                                 CHECK_FR_CTL, Font, 0, BS_AUTOCHECKBOX | BS_LEFTTEXT),
                     new CFIELD ("check_sa", "Sam", defaultlistf.check_sa,
                                  6, 0, 115, 4,  NULL, "", CBUTTON,
                                 CHECK_SA_CTL, Font, 0, BS_AUTOCHECKBOX | BS_LEFTTEXT),

// Hier Positionsfelder eintragen

                     NULL,
};


CFORM SpezDlg::fPos (8, _fPos);

CFIELD *SpezDlg::_fPosM [] = {
                     new CFIELD ("a_txt", "Artikel.",  10, 0, 60, 1,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("a", defaultlistf.a, 14, 0, 75, 1,  NULL, "%13.0lf", CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("prod_dat_txt", "Produktionstag",  14, 0, 60, 2,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("prod_dat", defaultlistf.prod_dat, 12, 0, 75, 2,  NULL, "dd.mm.yyyy", CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("bearbr", "Bearbeitet", defaultlistf.bearbr,
                                  12, 0, 60, 4,  NULL, "", CBUTTON,
                                 BEARB_CTL, Font, 0, BS_CHECKBOX | BS_LEFTTEXT),
                     NULL,
};

CFORM SpezDlg::fPosM (1, _fPosM);


CFIELD *SpezDlg::_fPosE [] = {
                     new CFIELD ("prod_dat_txt", "Produktionstag",  14, 0, 60, 2,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("sperr_info", SperrInfo,  20, 0, 90, 0,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),

                     new CFIELD ("prod_dat", defaultlistf.prod_dat, 12, 0, 75, 2,  NULL, "dd.mm.yyyy", CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     NULL,
};

CFORM SpezDlg::fPosE (1, _fPosE);



CFIELD *SpezDlg::_fPosP [] = {
                     new CFIELD ("a_txt", "Artikel.",  10, 0, 60, 1,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("a", defaultlistf.a, 14, 0, 75, 1,  NULL, "%13.0lf", CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("prod_dat_txt", "Produktionstag",  14, 0, 60, 2,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("prod_dat", defaultlistf.prod_dat, 12, 0, 75, 2,  NULL, "dd.mm.yyyy", CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("akt_phase_txt", "Phase",  14, 0, 60, 3,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("akt_phase_bz", defaultlistf.akt_phase_bz, 12, 0, 75, 3,  
                                 NULL, "", CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER),
                     new CFIELD ("akt_masch_txt", "Maschine",  14, 0, 60, 4,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("akt_masch_nr", defaultlistf.akt_masch_nr, 10, 0, 75, 4,  
                                 NULL, "%8ld", CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("akt_masch_bz", defaultlistf.akt_masch_bz, 24, 0, 75, 5,  
                                 NULL, "", CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER),
                     new CFIELD ("pos_frame", "",    200,13, 0, 6,  NULL, "",
                                 CBORDER,   
                                 500, Font, 0, TRANSPARENT),    
                     NULL,
};

CFORM SpezDlg::fPosP (1, _fPosP);

CFIELD *SpezDlg::_fPosPers [] = {
                     new CFIELD ("pos_frame", "",    200,13, 0, 5,  NULL, "",
                                 CBORDER,   
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("akt_masch_txt", "Maschine",  14, 0, 60, 0,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                    new CFIELD ("akt_masch_nr", defaultlistf.akt_masch_nr, 10, 0, 60, 1,  
                                 NULL, "%8ld", CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_LEFT),
                     new CFIELD ("akt_masch_bz", defaultlistf.akt_masch_bz, 24, 0, 60, 2,  
                                 NULL, "", CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER),
                     new CFIELD ("akt_masch_me", defaultlistf.akt_masch_me, 24, 0, 60, 3,  
                                 NULL, "", CREADONLY,
                                 500, Font, 0, TRANSPARENT ),
                     new CFIELD ("akt_masch_zeit", defaultlistf.akt_masch_zeit, 24, 0, 60, 4,  
                                 NULL, "", CREADONLY,
                                 500, Font, 0, TRANSPARENT ),

                     new CFIELD ("akt_pers_txt", "Mitarbeiter",  14, 0, 86, 0,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                    new CFIELD ("akt_pers_nr", defaultlistf.akt_pers_nr, 10, 0, 86, 1,  
                                 NULL, "%8ld", CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_LEFT),
                     new CFIELD ("akt_pers_bz", defaultlistf.akt_pers_bz, 24, 0, 86, 2,  
                                 NULL, "", CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER),
                     new CFIELD ("akt_pers_zeit", defaultlistf.akt_pers_zeit, 24, 0, 86, 3,  
                                 NULL, "", CREADONLY,
                                 500, Font, 0, TRANSPARENT ),
/*
                     new CFIELD ("akt_prodabt_txt", "Prod.Abt.",  14, 0, 60, 3,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("akt_prodabt", defaultlistf.akt_prodabt, 24, 0, 72, 3,  
                                 NULL, "", CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER),
*/
                     NULL,
};

CFORM SpezDlg::fPosPers (1, _fPosPers);



CFIELD *SpezDlg::_fcField [] = {
                     new CFIELD ("fcField1", (CFORM *) &fHead, 0, 0, 0, 0, NULL, "", CFFORM,
                                   HEADCTL, Font, 0, 0),
                     new CFIELD ("fcField2", (CFORM *) &fPos,  0, 0, 0, 0, NULL, "", CFFORM,
                                   POSCTL, Font, 0, 0),
                     NULL,                      
};

CFORM SpezDlg::fcForm (2, _fcField);

CFIELD *SpezDlg::_fcField0 [] = { 
                     new CFIELD ("fcForm", (CFORM *) &fcForm, 0, 0, 0, 1, NULL, "", CFFORM,
                                   500, Font, 0, 0),
                     NULL,
};
CFORM SpezDlg::fcForm0 (1, _fcField0);

CFIELD *SpezDlg::_fcFieldM [] = {
                     new CFIELD ("fcField1", (CFORM *) &fHead, 0, 0, 0, 0, NULL, "", CFFORM,
                                   HEADCTL, Font, 0, 0),
                     new CFIELD ("fcField2", (CFORM *) &fPosM,  0, 0, 0, 0, NULL, "", CFFORM,
                                   POSCTL, Font, 0, 0),
                     NULL,                      
};

CFORM SpezDlg::fcFormM (2, _fcFieldM);

CFIELD *SpezDlg::_fcField0M [] = { 
                     new CFIELD ("fcForm", (CFORM *) &fcFormM, 0, 0, 0, 1, NULL, "", CFFORM,
                                   500, Font, 0, 0),
                     NULL,
};
CFORM SpezDlg::fcForm0M (1, _fcField0M);

CFIELD *SpezDlg::_fcFieldE [] = {
                     new CFIELD ("fcField1", (CFORM *) &fHead, 0, 0, 0, 0, NULL, "", CFFORM,
                                   HEADCTL, Font, 0, 0),
                     new CFIELD ("fcField2", (CFORM *) &fPosE,  0, 0, 0, 0, NULL, "", CFFORM,
                                   POSCTL, Font, 0, 0),
                     NULL,                      
};

CFORM SpezDlg::fcFormE (2, _fcFieldE);


CFIELD *SpezDlg::_fcField0E [] = { 
                     new CFIELD ("fcForm", (CFORM *) &fcFormE, 0, 0, 0, 1, NULL, "", CFFORM,
                                   500, Font, 0, 0),
                     NULL,
};
CFORM SpezDlg::fcForm0E (1, _fcField0E);




CFIELD *SpezDlg::_fcFieldP [] = {
                     new CFIELD ("fcField1", (CFORM *) &fHead, 0, 0, 0, 0, NULL, "", CFFORM,
                                   HEADCTL, Font, 0, 0),
                     new CFIELD ("fcField2", (CFORM *) &fPosP,  0, 0, 0, 0, NULL, "", CFFORM,
                                   POSCTL, Font, 0, 0),
                     NULL,                      
};

CFORM SpezDlg::fcFormP (2, _fcFieldP);


CFIELD *SpezDlg::_fcField0P [] = { 
                     new CFIELD ("fcForm", (CFORM *) &fcFormP, 0, 0, 0, 1, NULL, "", CFFORM,
                                   500, Font, 0, 0),
                     NULL,
};
CFORM SpezDlg::fcForm0P (1, _fcField0P);








CFIELD *SpezDlg::_fcFieldPers [] = {
                     new CFIELD ("fcField1", (CFORM *) &fHead, 0, 0, 0, 0, NULL, "", CFFORM,
                                   HEADCTL, Font, 0, 0),
                     new CFIELD ("fcField2", (CFORM *) &fPosPers,  0, 0, 0, 0, NULL, "", CFFORM,
                                   POSCTL, Font, 0, 0),
                     NULL,                      
};

CFORM SpezDlg::fcFormPers (2, _fcFieldPers);


CFIELD *SpezDlg::_fcField0Pers [] = { 
                     new CFIELD ("fcForm", (CFORM *) &fcFormPers, 0, 0, 0, 1, NULL, "", CFFORM,
                                   500, Font, 0, 0),
                     NULL,
};
CFORM SpezDlg::fcForm0Pers (1, _fcField0Pers);



Work SpezDlg::work;
char *SpezDlg::HelpName = "woplan.cmd";
SpezDlg *SpezDlg::ActiveSpezDlg = NULL;
HBITMAP SpezDlg::SelBmp;
int SpezDlg::pos = 0;
char SpezDlg::ptwert[5];


void SpezDlg::ChangeF6 (DWORD Id)
{

    if (Id == IDM_LIST)
    {
         if (DeleteMenu (ActiveDlg->GethMenu (), IDM_LIST , MF_BYCOMMAND)) 
         {
                InsertMenu (ActiveDlg->GethMenu (), IDM_DOCKLIST, MF_BYCOMMAND | MF_STRING, 
                            MenueInsert.menid, MenueInsert.text);
                            
         }
    }
    else if (Id == IDM_INSERT)
    {
         if (DeleteMenu (ActiveDlg->GethMenu (), IDM_INSERT , MF_BYCOMMAND))
         {
                 InsertMenu (ActiveDlg->GethMenu (), IDM_DOCKLIST, MF_BYCOMMAND | MF_STRING, 
                            MenuePosEnter.menid, MenuePosEnter.text);
         }
    }
}

int SpezDlg::ReadMdn (void)
{
         if (syskey == 0) return 0;
         if (syskey == KEY9) return 0;

         fPos.GetText ();
         FromForm (dbheadfields);
         work.GetMdnName (defaultlistf.mdn_krz,   atoi (defaultlistf.mdn));
         if (atoi (defaultlistf.mdn) == 0)
         {
             sprintf (defaultlistf.fil, "%4d", 0);
             work.GetFilName (defaultlistf.fil_krz,   atoi (defaultlistf.mdn), atoi (defaultlistf.fil));
             Enable (&fcForm, EnableHeadFil, FALSE); 
             fHead.SetCurrentName ("text1");
         }
         else
         {
             if (fHead.GetCfield ("fil")->IsDisabled ())
             {
                    Enable (&fcForm, EnableHeadFil, TRUE); 
                    fHead.SetCurrentName ("fil");
             }
         }
         if (syskey == KEYUP)
         {
                    fHead.SetCurrentName ("kun");
         }

         fHead.SetText ();
		 
		 

         return 0;
}

int SpezDlg::ReadFil (void)
{
          
         if (syskey == 0) return 0;
         if (syskey == KEY9) return 0;
         fPos.GetText ();
         FromForm (dbheadfields);
         work.GetFilName (defaultlistf.fil_krz,   atoi (defaultlistf.mdn), atoi (defaultlistf.fil));
         fHead.SetText ();
         return 0;
}
int SpezDlg::setzeSperre (void)
{
	int ldatum, StartDat;
	char datum [12];
	short kw,jr;
	
        fPos.GetCfield ("check_mo")->Enable (TRUE);
        fPos.GetCfield ("check_di")->Enable (TRUE);
        fPos.GetCfield ("check_mi")->Enable (TRUE);
        fPos.GetCfield ("check_do")->Enable (TRUE);
        fPos.GetCfield ("check_fr")->Enable (TRUE);
        fPos.GetCfield ("check_sa")->Enable (TRUE);
        sysdate (datum);
        ldatum = dasc_to_long (datum) + 1;
		kw =  atoi (defaultlistf.kw) ;
		jr =  atoi (defaultlistf.jr) ;
        work.SetKwJr(kw,jr);
		if (kw < 1 || kw > 53)
		{
	            fHead.SetCurrentName ("kw");
				return 0;
		}
		if (jr < 2000)
		{
	            fHead.SetCurrentName ("jr");
				return 0;
		}
        StartDat = first_kw_day ((short) kw, (short) jr);
		work.SetStartDat (StartDat);
        if (StartDat + 1 < ldatum && SperreAktiv == 1)
		{
			strcpy(defaultlistf.check_mo,"N") ;
            fPos.GetCfield ("check_mo")->Enable (FALSE);
		} 
		else if (StartDat + 1 == ldatum) strcpy(defaultlistf.check_mo,"J") ;

		StartDat++;
        if (StartDat + 1< ldatum && SperreAktiv == 1)
		{
			strcpy(defaultlistf.check_di,"N") ;
            fPos.GetCfield ("check_di")->Enable (FALSE);
		}
		else if (StartDat + 1 == ldatum) strcpy(defaultlistf.check_di,"J") ;

		StartDat++;
        if (StartDat + 1 < ldatum && SperreAktiv == 1)
		{
			strcpy(defaultlistf.check_mi,"N") ;
            fPos.GetCfield ("check_mi")->Enable (FALSE);
		}
		else if (StartDat + 1 == ldatum) strcpy(defaultlistf.check_mi,"J") ;

		StartDat++;
        if (StartDat + 1 < ldatum && SperreAktiv == 1)
		{
			strcpy(defaultlistf.check_do,"N") ;
            fPos.GetCfield ("check_do")->Enable (FALSE);
		}
		else if (StartDat + 1 == ldatum) strcpy(defaultlistf.check_do,"J") ;

		StartDat++;
        if (StartDat + 1 < ldatum && SperreAktiv == 1)
		{
			strcpy(defaultlistf.check_fr,"N") ;
            fPos.GetCfield ("check_fr")->Enable (FALSE);
		}
		else if (StartDat + 1 == ldatum) strcpy(defaultlistf.check_fr,"J") ;

		StartDat++;
        if (StartDat + 1 < ldatum && SperreAktiv == 1)
		{
			strcpy(defaultlistf.check_sa,"N") ;
            fPos.GetCfield ("check_sa")->Enable (FALSE);
		}
		else if (StartDat + 1 == ldatum) strcpy(defaultlistf.check_sa,"J") ;


		if (ohneSamstag == 1) 
		{
			strcpy(defaultlistf.check_sa,"N");
            fPos.GetCfield ("check_sa")->Enable (FALSE);
		}

        fPos.SetText ();

	return 0;
}

int SpezDlg::ReadAbt (void)
{
//         short wg; 
//         long ag; 
         if (atoi (defaultlistf.abt) == 0) return 0;  
         if (syskey == 0) return 0;
         if (syskey == KEY9) return 0;
         if (syskey == KEY5) return 0;
         fPos.GetText ();
         FromForm (dbheadfields);
         if (work.GetAbtBez (defaultlistf.abt_bz1,   atoi (defaultlistf.abt)) == 100)
         {
                 disp_mess ("Abteilung nicht gefunden",2);
                 fHead.SetCurrentName ("abt");
                 return 0;
         }
/*
         wg = atoi (defaultlistf.wg);
         work.TestWgAbt (&wg, atoi (defaultlistf.abt));
         sprintf (defaultlistf.wg, "%d", wg);
         if (wg == 0)
         {
             strcpy (defaultlistf.wg_bz1, "");
         }
         ag = atol (defaultlistf.ag);
         work.TestAgWg (&ag, wg);
         sprintf (defaultlistf.ag, "%ld", ag);
         if (ag == 0l)
         {
             strcpy (defaultlistf.ag_bz1, "");
         }
*/
         fHead.SetText ();
         return 0;
}


int SpezDlg::EnterPos (void)
{

//Hier Postionsdialog eintragen, falls vorhanden.
/*
         switch (syskey)
         {
               case KEYDOWN :
               case KEYTAB :
                   fHead.SetCurrentName ("mdn");
                   return TRUE;
         }
        
         if (DLG::MousePressed)
         {
             return FALSE;
         }

         if (syskey != KEYCR)
         {
             return FALSE;
         }

         
         fHead.GetText ();

         FromForm (dbheadfields);

         work.BeginWork ();
*/

         ToolBar_SetState (ActiveDlg->GethwndTB (),   IDM_DELALL, TBSTATE_ENABLED);
         EnableMenuItem (ActiveDlg->GethMenu (),      IDM_LIST,     MF_ENABLED);
         EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELETE,   MF_GRAYED);
         EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELALL,   MF_GRAYED);
         EnableMenuItem (ActiveDlg->GethMenu (),      VK_INSERT,    MF_GRAYED);
         EnableMenuItem (ActiveDlg->GethMenu (),      VK_F10,       MF_GRAYED);
         if (ActiveSpezDlg->GetToolbar2 () != NULL)
         {
                 if  (ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f6") != NULL)
                 {
                      ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f6")->GetFeld ())->bmp = 
                                   ImageList.Image;
                 }
				 /*251105
                 ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("del")->GetFeld ())->bmp = 
					 ImageDelete.Inactive;
                 ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("ins")->GetFeld ())->bmp = 
					 ImageInsert.Inactive;
					 */
                 ActiveSpezDlg->GetToolbar2 ()->display ();
         }
// Bei zus�tzlichen Positionseingaben Positionsfelder aktiv setzen
//         Enable (&fcForm, EnableHead, FALSE); 
//         Enable (&fcForm, EnablePos,  TRUE); 
// Erstes Positionsfeld setzen. 
         fPos.SetCurrentName ("adr_krz");

         return 0;
}


int SpezDlg::GetPtab (void)
{
//         char wert [5];
         CFIELD *Cfield;


         fPos.GetText ();

         Cfield = ActiveDlg->GetCurrentCfield ();
         return 0;
}


ItProg *SpezDlg::After [] = {
                                   new ItProg ("mdn",         ReadMdn),
                                   new ItProg ("fil",         ReadFil),
                                   new ItProg ("abt",         ReadAbt),
                                   new ItProg ("kw",         setzeSperre),
                                   new ItProg ("jr",         setzeSperre),
                                   NULL,
};

ItProg *SpezDlg::Before [] = {NULL,
};

ItFont *SpezDlg::itFont [] = {
                                  new ItFont ("mdn_krz",       &ltgrayfont),
                                  new ItFont ("abt_bz1",          &ltgrayfont),
                                  new ItFont ("stnd_bz1",          &ltgrayfont),
                                  NULL
};

ItFont *SpezDlg::RdoFont [] = {
                                  new ItFont ("a",         &rdofont),
                                  new ItFont ("chg",       &rdofont),
                                  new ItFont ("chg_gew",   &rdofont),
                                  new ItFont ("prod_dat",  &rdofont),
                                  new ItFont ("akt_phase_bz", 
                                                            &rdofont),
                                  new ItFont ("akt_masch_nr",&rdofont),
                                  new ItFont ("akt_masch_bz", &rdofont),
                                  new ItFont ("akt_masch_me", &rdofont),
                                  new ItFont ("akt_masch_zeit", &rdofont),
                                  new ItFont ("akt_pers_nr",&rdofont),
                                  new ItFont ("akt_pers_bz", &rdofont),
                                  new ItFont ("akt_pers_zeit", &infofont),
                                  new ItFont ("akt_prodabt", &rdofont),
                                  NULL
};

ItFont *SpezDlg::InfoFont [] = {
                                  new ItFont ("erf_info",  &infofont),
                                  new ItFont ("sperr_info",  &warningfont),
                                  new ItFont ("summe_info",  &infofont),
                                  NULL
};

FORMFIELD *SpezDlg::dbheadfields [] = {
//         new FORMFIELD ("mdn",       defaultlistf.mdn,       (short *)   &partner.mdn,       FSHORT,   NULL),
//         new FORMFIELD ("fil",       defaultlistf.fil,       (short *)   &partner.fil,       FSHORT,   NULL),
           NULL,
};

FORMFIELD *SpezDlg::dbfields [] = {
         NULL,
};



char *SpezDlg::EnableHead [] = {
                                "mdn",
                                "mdn_choise",
                                "fil",
                                "fil_choise",
                                "abt",
                                "abt_choise",
                                "stnd",
                                "stnd_choise",
                                "kw",
                                "jr",
								"dat_von",
								"dat_bis",
                                 NULL
};

char *SpezDlg::EnableHeadFil [] = {
                                     "fil",
                                     NULL
};

//XXXXXXXXXX
char *SpezDlg::EnablePos[] = {
							  "check_mo",
							  "check_di",
							  "check_mi",
							  "check_do",
							  "check_fr",
							  "check_sa",
                               NULL,
};

                         
SpezDlg::SpezDlg (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel) :
         DLG (x, y, cx, cy, Caption, Size, Pixel)
{
          
             BorderType = HIGHCOLBORDER;
             Init (x, y, cx, cy, Caption, Size, Pixel);
}

SpezDlg::SpezDlg (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel, 
                  int BorderType) :
         DLG (x, y, cx, cy, Caption, Size, Pixel)
{
          
             this->BorderType = BorderType;
             Init (x, y, cx, cy, Caption, Size, Pixel);
}

SpezDlg::SpezDlg (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel, 
                  int BorderType, int EnterM) :
         DLG (x, y, cx, cy, Caption, Size, Pixel)
{
          
             this->BorderType = BorderType;
//             EnterMode = EnterM;
             Init (x, y, cx, cy, Caption, Size, Pixel);
}


void SpezDlg::Init (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel)
{
             PROG_CFG ProgCfg ("woplan");
//             char cfg_v [256];
             int i;
             int xfull, yfull;

//             Work.Prepare ();

			 if (sys_ben.mdn > 0)
			 {
				sprintf(defaultlistf.mdn,"%4d",sys_ben.mdn);
				syskey = KEYCR;
				ReadMdn();
				fHead.GetCfield ("mdn")->SetAttribut (CREADONLY);
	            fHead.SetCurrentName ("kw");
				fHead.GetCfield ("mdn_choise")->SetAttribut (CREMOVED);
			 }
             ImageDelete.Image    = BMAP::LoadBitmap (hInstance, "DEL",       "DELMASK",      SysBkColor);
             ImageDelete.Inactive = BMAP::LoadBitmap (hInstance, "DELIA",     "DELMASK",      SysBkColor);
             ImageInsert.Image    = BMAP::LoadBitmap (hInstance, "INSERT",    "INSERTMASK",   SysBkColor);
             ImageInsert.Inactive = BMAP::LoadBitmap (hInstance, "INSERTIA",  "INSERTMASK",   SysBkColor);
             ImageList.Image      = BMAP::LoadBitmap (hInstance, "ARRDOWND",  "ARRDOWNDMASK", SysBkColor);
             ImageList.Inactive   = BMAP::LoadBitmap (hInstance, "ARRDOWNDI","ARRDOWNDMASK", SysBkColor);
             hwndCombo1 = NULL;
             hwndCombo2 = NULL;
             Combo1 = NULL;
             Combo2 = NULL;
             ColorSet = FALSE;
             LineSet = FALSE;
             Toolbar2 = NULL;
             DockList = TRUE;
             ListDlg = NULL;
             MaschLst = NULL;
             PhaseLst = NULL;
             ErfLst = NULL;
             PersLst = NULL;
             hwndList = NULL;
             AktDat = 0l;
             strcpy (AktDatS, "");
             AktMaschNr = 0l;
             AktPhase = 0;
             AktPersNr = 0;
             strcpy (AktPhaseBz, "");
             MaschSel = FALSE;

             ActiveSpezDlg = this; 
             xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
             yfull = GetSystemMetrics (SM_CYFULLSCREEN);        

             ltgrayfont.FontBkColor = SysBkColor;
             dlgposfont.FontBkColor = SysBkColor;
             infofont.FontBkColor = SysBkColor;
             warningfont.FontBkColor = SysBkColor;

             StdSize = Size;
             dlgfont.FontHeight    = Size;
             rdofont.FontHeight    = Size;
             dlgposfont.FontHeight = Size;
             ltgrayfont.FontHeight = Size;
             cpfont.FontHeight     = cpsize;


             if (xfull < 1000) 
             {
                 dlgfont.FontHeight = 90;
                 rdofont.FontHeight    = Size;
                 dlgposfont.FontHeight = 90;
                 ltgrayfont.FontHeight = 90;
                 checkfonth.FontHeight = 90;
                 cpfont.FontHeight = 150;
             }

             if (xfull < 800) 
             {

                 dlgfont.FontHeight = 70;
                 rdofont.FontHeight    = Size;
                 dlgposfont.FontHeight = 70;
                 ltgrayfont.FontHeight = 70;
                 checkfonth.FontHeight = 70;
                 cpfont.FontHeight = 120;
             }

             fHead.SetFieldanz ();
             fHeadM.SetFieldanz ();
             fPos.SetFieldanz ();
             fPosM.SetFieldanz ();
             fPosP.SetFieldanz ();
             fPosE.SetFieldanz ();
             fPosPers.SetFieldanz ();
             fcForm.SetFieldanz ();

             fHead.GetCfield ("mdn_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fHead.GetCfield ("fil_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fHead.GetCfield ("abt_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fHead.GetCfield ("stnd_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));

             fHead.GetCfield ("mdn_choise")->SetTabstop (FALSE); 
             fHead.GetCfield ("fil_choise")->SetTabstop (FALSE); 
             fHead.GetCfield ("abt_choise")->SetTabstop (FALSE); 
             fHead.GetCfield ("stnd_choise")->SetTabstop (FALSE); 
             fPos.GetCfield ("bearb")->SetTabstop (FALSE); 
             fPosM.GetCfield ("bearbr")->SetTabstop (FALSE); 


             for (i = 0; i < fHead.GetFieldanz (); i ++)
             {
//                       fHead.GetCfield ()[i]->SetFont (&dlgfont); 
//                       fHead.GetCfield ()[i]->SetPosFont (&dlgposfont); 
             }

             SetTmFont (&dlgfont);
             for (i = 0; Before[i] != NULL; i ++)
             {
                 Before[i]->SetBefore (&fcForm);
             }

             for (i = 0; After[i] != NULL; i ++)
             {
                 After[i]->SetAfter (&fcForm);
             }


             for (i = 0; itFont[i] != NULL; i ++)
             {
                         itFont[i]->SetFont (&fcForm);
             }

             for (i = 0; RdoFont[i] != NULL; i ++)
             {
                         RdoFont[i]->SetFont (&fcForm);
                         RdoFont[i]->SetFont (&fcFormM);
                         RdoFont[i]->SetFont (&fcFormP);
                         RdoFont[i]->SetFont (&fcFormPers);
             }

             for (i = 0; InfoFont[i] != NULL; i ++)
             {
                         InfoFont[i]->SetFont (&fcForm);
             }

             fPos.GetCfield ("pos_frame")->SetBorder (GRAYCOL, WHITECOL, RAISEDHLINE); 
//             fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, RAISED); 
/*
             if (BorderType == HIGHCOLBORDER)
             {

                for (i = 0; itFont[i] != NULL; i ++)
                {
                         itFont[i]->SetFont (&fcForm);
                }

                fPos.GetCfield ("pos_frame")->SetBorder 
                 (new FILLEDBORDER (BLACKCOL, WHITECOL, HIGH, 0, LTGRAYCOL)); 
             }
             else if (BorderType == LOWCOLBORDER)
             {

                for (i = 0; itFont[i] != NULL; i ++)
                {
                         itFont[i]->SetFont (&fcForm);
                }
                fPos.GetCfield ("pos_frame")->SetBorder 
                 (new FILLEDBORDER (BLACKCOL, WHITECOL, LOW, 0, LTGRAYCOL)); 
             }
             else if (BorderType == HIGHBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, HIGH); 
             }
             else if (BorderType == LOWBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, LOW); 
             }
             else if (BorderType == RAISEDBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, RAISED); 
             }
             else if (BorderType == LINEBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, LINE); 
             }
*/

             char datum [12];
             sysdate (datum);
             int kw = get_woche (datum);
             sprintf (defaultlistf.kw, "%d", kw);
             strcpy (defaultlistf.jr, &datum[6]);
	         long ldatum = dasc_to_long (datum);
			 strcpy (defaultlistf.datvon,work.datvon);
			 strcpy (defaultlistf.datbis,work.datbis);
             Enable (&fcForm, EnableHead, TRUE); 
             Enable (&fcForm, EnablePos,  TRUE);
			 setzeSperre();
             SetDialog (&fcForm0);
			if (strupcmp (work.GetProdAbt(), "0", strlen (work.GetProdAbt())) == 0)
			{
				 strcpy (defaultlistf.abt,"0");
                 fHead.GetCfield ("abt")->SetAttribut (CREMOVED);
                 fHead.GetCfield ("abt_choise")->SetAttribut (CREMOVED);
                 fHead.GetCfield ("abt_bz1")->SetAttribut (CREMOVED);
                 fHead.GetCfield ("abt_txt")->SetAttribut (CREMOVED);
			}
			if (strupcmp (work.GetProdAbt(), "Standardplan", strlen (work.GetProdAbt())) == 0)
			{
				 strcpy (defaultlistf.abt,"0");
                 fHead.GetCfield ("abt")->SetAttribut (CREMOVED);
                 fHead.GetCfield ("abt_choise")->SetAttribut (CREMOVED);
                 fHead.GetCfield ("abt_bz1")->SetAttribut (CREMOVED);
                 fHead.GetCfield ("abt_txt")->SetAttribut (CREMOVED);

                 fHead.GetCfield ("stnd_choise")->SetAttribut (CREMOVED); // da Auswahl noch nicht realisiert
			}
			else
			{
                 fHead.GetCfield ("stnd")->SetAttribut (CREMOVED);
                 fHead.GetCfield ("stnd_choise")->SetAttribut (CREMOVED);
                 fHead.GetCfield ("stnd_bz1")->SetAttribut (CREMOVED);
                 fHead.GetCfield ("stnd_txt")->SetAttribut (CREMOVED);
			}
			if (Work::LiefMeHolen == 0)
			{
                 fHead.GetCfield ("bis_txt")->SetAttribut (CREMOVED);
                 fHead.GetCfield ("liefdat_txt")->SetAttribut (CREMOVED);
                 fHead.GetCfield ("dat_von")->SetAttribut (CREMOVED);
                 fHead.GetCfield ("dat_bis")->SetAttribut (CREMOVED);
			}
}


BOOL SpezDlg::OnKeyReturn (void)
{
        HWND hWnd;
        
        hWnd = GetFocus ();
        return FALSE;
}


BOOL SpezDlg::OnKeyDown (void)
{
        return FALSE;
}


BOOL SpezDlg::OnKeyUp (void)
{
        return FALSE;
}


BOOL SpezDlg::OnKey2 ()
{
        return TRUE;
}

BOOL SpezDlg::OnKeyHome ()
{
        return TRUE;
}

BOOL SpezDlg::OnKeyEnd ()
{
        return TRUE;

}


BOOL SpezDlg::OnPreTranslateMessage (MSG *msg)
{
        int ret;


        if (ListDlg != NULL)
        {
            ret = ListDlg->GeteListe ()->PerformMessage (msg);
            if (ret && (syskey == KEY5 ||
                        syskey == KEY12))
            {
                   ListDlg->DestroyWindows ();
                   delete ListDlg;
                   ListDlg = NULL;
                   fPos.display ();
                   if  (ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f6") != NULL)
                   {
                      ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f6")->GetFeld ())->bmp = 
                                   ImageList.Image;
                   }
                   return FALSE;
            }
            else
            {
                   return TRUE;
            }
        }
        else if (MaschLst != NULL)
        {
            ret = MaschLst->GeteListe ()->PerformMessage (msg);
            if (ret && (syskey == KEY5 ||
                        syskey == KEY12))
            {
                   MaschLst->DestroyWindows ();
                   delete MaschLst;
                   MaschLst = NULL;
                   fPosM.destroy ();
                   SetDialog (&fcForm0);
                   fPos.display ();
                   EnableMenuItem (ActiveDlg->GethMenu (),      IDM_ERFMODE,      MF_ENABLED);
                   if  (ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f6") != NULL)
                   {
                      ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f6")->GetFeld ())->bmp = 
                                   ImageList.Image;
                       ActiveSpezDlg->GetToolbar2 ()->GetCfield ("absch")->Enable (TRUE); 
                   }
                   return FALSE;
            }
            else
            {
                   return TRUE;
            }
        }
        else if (PhaseLst != NULL)
        {
            ret = PhaseLst->GeteListe ()->PerformMessage (msg);
            if (ret && (syskey == KEY5 ||
                        syskey == KEY12))
            {
                   PhaseLst->DestroyWindows ();
                   delete PhaseLst;
                   PhaseLst = NULL;
                   fPosP.destroy ();
                   SetDialog (&fcForm0);
                   fPos.display ();
                   EnableMenuItem (ActiveDlg->GethMenu (),      IDM_ERFMODE,      MF_ENABLED);
                   if  (ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f6") != NULL)
                   {
                      ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f6")->GetFeld ())->bmp = 
                                   ImageList.Image;
                       ActiveSpezDlg->GetToolbar2 ()->GetCfield ("absch")->Enable (TRUE); 
                   }
                   return FALSE;
            }
            else
            {
                   return TRUE;
            }
        }
        else if (PersLst != NULL)
        {
            ret = PersLst->GeteListe ()->PerformMessage (msg);
            if (ret && (syskey == KEY5 ||
                        syskey == KEY12))
            {
                   PersLst->DestroyWindows ();
                   delete PersLst;
                   PersLst = NULL;
                   fPosPers.destroy ();
                   SetDialog (&fcForm0);
                   fPos.display ();
                   EnableMenuItem (ActiveDlg->GethMenu (),      IDM_ERFMODE,      MF_ENABLED);
                   if  (ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f6") != NULL)
                   {
                      ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f6")->GetFeld ())->bmp = 
                                   ImageList.Image;
                       ActiveSpezDlg->GetToolbar2 ()->GetCfield ("absch")->Enable (TRUE); 
                   }
                   return FALSE;
            }
            else
            {
                   return TRUE;
            }
        }
        else if (ErfLst != NULL)
        {
            ret = ErfLst->GeteListe ()->PerformMessage (msg);
            if (ret && (syskey == KEY5 ||
                        syskey == KEY12))
            {
                   ErfLst->DestroyWindows ();
                   delete ErfLst;
                   ErfLst = NULL;
                   fPosE.destroy ();
                   SetDialog (&fcForm0);
                   fPos.display ();
                   EnableMenuItem (ActiveDlg->GethMenu (),      IDM_ERFMODE,      MF_ENABLED);
                   if  (ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f6") != NULL)
                   {
                      ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f6")->GetFeld ())->bmp = 
                                   ImageList.Image;
                       ActiveSpezDlg->GetToolbar2 ()->GetCfield ("absch")->Enable (TRUE); 
                   }
                   return FALSE;
            }
            else
            {
                   return TRUE;
            }
        }

        return FALSE;
}

BOOL SpezDlg::OnKey1 ()
{
        syskey = KEY1;
        Help ();
        return TRUE;
}

BOOL SpezDlg::OnKey3 ()
{
        syskey = KEY3;
        return TRUE;
}

BOOL SpezDlg::OnKey4 ()
{
        syskey = KEY4;
        CallInfo ();
        return TRUE;
}

BOOL SpezDlg::OnKey5 ()
{

        syskey = KEY5;
        if (fHead.GetCfield ("mdn")->IsDisabled () == FALSE)
        {
                  DestroyWindow ();
                  return TRUE;
        }
        if (iPage != TabMengen)
        {
                  TabCtrl_SetCurFocus (GetParent (hWnd), 0);
        }
        Enable (&fcForm, EnableHead, TRUE); 
        Enable (&fcForm, EnablePos,  TRUE); //XXXXXXXXXXXXX
        work.RollbackWork ();
        work.InitRec ();
        ToForm (dbfields);
        sprintf (defaultlistf.chg,     "%.3lf", 0.0);  
        sprintf (defaultlistf.chg_gew, "%.3lf", 0.0);  
        sprintf (defaultlistf.a,       "%.0lf", 0.0);  
        strcpy (defaultlistf.bearb, "N");
        strcpy (defaultlistf.bearbr, "N");
        fHead.SetText ();
        fPos.SetText ();
        fHead.SetCurrentName ("kw");
        ToolBar_SetState (ActiveDlg->GethwndTB (),   IDM_DELALL,   TBSTATE_INDETERMINATE);
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_LIST,     MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELETE,   MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELALL,   MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      VK_INSERT,    MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      VK_F12,       MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_MASCHENTER, 
                                                                   MF_GRAYED);
        if (ActiveSpezDlg->GetToolbar2 () != NULL)
        {
//                 ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f6")->GetFeld ())->bmp = 
//					 ImageDelete.Inactive;
                 ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("del")->GetFeld ())->bmp = 
					 ImageDelete.Inactive;
                 ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("ins")->GetFeld ())->bmp = 
                     ImageInsert.Inactive;
                 if  (ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f6") != NULL)
                 {
                      ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f6")->GetFeld ())->bmp = 
                                   ImageList.Image;
                 }
                 if  (ActiveSpezDlg->GetToolbar2 ()->GetCfield ("Texte") != NULL)
                 {
                      ActiveSpezDlg->GetToolbar2 ()->GetCfield ("Texte")->Enable (FALSE);
                 }
                 ActiveSpezDlg->GetToolbar2 ()->display ();
        }
        return TRUE;
}

BOOL SpezDlg::OnKey12 ()
{
        return OnKey5 ();

        syskey = KEY12;

        fWork->GetText ();
        FromForm (dbfields); 
        if (fHead.GetCfield ("mdn")->IsDisabled ())
        {
               if (iPage != TabMengen)
               {
                  TabCtrl_SetCurFocus (GetParent (hWnd), 0);
               }
               Enable (&fcForm, EnableHead, TRUE); 
               Enable (&fcForm, EnablePos,  TRUE); //XXXXXXXXXXXX
               work.CommitWork ();
               work.InitRec ();
               ToForm (dbfields);

               sprintf (defaultlistf.chg,     "%.3lf", 0.0);  
               sprintf (defaultlistf.chg_gew, "%.3lf", 0.0);  
               sprintf (defaultlistf.a,       "%.0lf", 0.0);  
               strcpy (defaultlistf.bearb, "N");
               strcpy (defaultlistf.bearbr, "N");
               fHead.SetText ();
               fPos.SetText ();
               fHead.SetCurrentName ("kw");
               ToolBar_SetState (ActiveDlg->GethwndTB (),   IDM_DELALL, TBSTATE_INDETERMINATE);
               EnableMenuItem (ActiveDlg->GethMenu (),      IDM_LIST,     MF_GRAYED);
               EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELETE,   MF_GRAYED);
               EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELALL,   MF_GRAYED);
               EnableMenuItem (ActiveDlg->GethMenu (),      VK_INSERT,    MF_GRAYED);
               EnableMenuItem (ActiveDlg->GethMenu (),      VK_F12,       MF_GRAYED);
               EnableMenuItem (ActiveDlg->GethMenu (),      IDM_MASCHENTER, 
                                                                          MF_GRAYED);
               if (ActiveSpezDlg->GetToolbar2 () != NULL)
               {
//                 ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f6")->GetFeld ())->bmp = 
//					 ImageDelete.Inactive;
                 ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("del")->GetFeld ())->bmp = 
					 ImageDelete.Inactive;
                 ((ColButton *)ActiveSpezDlg->Toolbar2->GetCfield ("ins")->GetFeld ())->bmp = 
                     ImageInsert.Inactive;
                 if  (ActiveSpezDlg->GetToolbar2 ()->GetCfield ("Texte") != NULL)
                 {
                      ActiveSpezDlg->GetToolbar2 ()->GetCfield ("Texte")->Enable (FALSE);
                 }
                 if  (ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f6") != NULL)
                 {
                      ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f6")->GetFeld ())->bmp = 
                                   ImageList.Image;
                 }
                 ActiveSpezDlg->GetToolbar2 ()->display ();
               }
        }
        return TRUE;
}


BOOL SpezDlg::OnKey10 (void)
{
      if (ListDlg != NULL) 
	  {
		SwitchListMeMode ();
	  }
      if (PersLst != NULL) 
	  {
            PersLst->UpdateList ();
	  }

      return TRUE;
}

BOOL SpezDlg::OnKey11 (void)
{
      char Maschine[12];
      if (MaschLst == NULL)
      {
          return FALSE;
      }

      EnterF *Enter = new EnterF ();
      Enter->SetColor (GetSysColor (COLOR_3DFACE));
      Enter->EnterText (hWnd, "Maschinennummer", Maschine, 9, "%8ld"); 
      delete Enter;  
      AktMaschNr = atol (Maschine);
      AktPhase = MaschLst->GetAktPhase (AktPhaseBz);
      MaschSel = TRUE;
      PostMessage (WM_KEYDOWN, VK_NEXT, 0l);
      return TRUE;
}


BOOL SpezDlg::OnKeyDelete (void)
{
        return FALSE;
}

BOOL SpezDlg::OnKeyInsert ()
{
        return FALSE;
}

BOOL SpezDlg::PriorPage ()
{
        return DLG::OnKeyPrior (); 
}

BOOL SpezDlg::OnKeyPrior ()
{
        if (GetKeyState (VK_CONTROL) >= 0)
        {
                if (ListDlg == NULL && 
                    MaschLst == NULL &&
                    PhaseLst == NULL &&
                    ErfLst == NULL &&
                    PersLst == NULL)
                {
                    return PriorPage ();
                }
        }

        if (vTrack != NULL)
        {
             SendMessage (WM_VSCROLL, SB_PAGEUP, 0l);
             return TRUE;
        }
        return FALSE;
}


BOOL SpezDlg::NextPage ()
{
        return DLG::OnKeyNext (); 
}

BOOL SpezDlg::OnKeyNext ()
{
        if (GetKeyState (VK_CONTROL) >= 0)
        {
                if (ListDlg == NULL && 
                    MaschLst == NULL &&
                    PersLst == NULL &&
                    ErfLst == NULL &&
                    PhaseLst == NULL)
                {
                    return DLG::OnKeyNext ();
                }
        }

        if (vTrack != NULL)
        {
             SendMessage (WM_VSCROLL, SB_PAGEDOWN, 0l);
             return TRUE;
        }

        return FALSE;
}

void SpezDlg::SetListMeMode (int ListMeMode)
{
        if (ListDlg != NULL)
        {
            ListDlg->WriteList ();
			work.Upd99 ();
        }
        work.SetListMeMode (ListMeMode);
        if (ListMeMode == MENGE)
        {
            strcpy (ChgTxt, ChgTxtMe);
            strcpy (ErfInfo, ErfInfoMe);
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_ERFMODE,  MF_UNCHECKED);
            ((ColButton *)Toolbar2->GetCfield ("absch")->GetFeld ())->text1 = 
                     ErfInfoAuf;
            Toolbar2->SetText ();
        }
        else if (ListMeMode == CHARGEN)
        {
            strcpy (ChgTxt, ChgTxtChg);
            strcpy (ErfInfo, ErfInfoChg);
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_ERFMODE,  MF_CHECKED);
            ((ColButton *)Toolbar2->GetCfield ("absch")->GetFeld ())->text1 = 
                     ErfInfoMe;
            Toolbar2->SetText ();
        }
        else if (ListMeMode == AUFTRAEGE)
        {
            strcpy (ChgTxt, ChgTxtAuf);
            strcpy (ErfInfo, ErfInfoAuf);
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_ERFMODE,  MF_CHECKED);
            ((ColButton *)Toolbar2->GetCfield ("absch")->GetFeld ())->text1 = 
                     ErfInfoChg;
            Toolbar2->SetText ();
        }
        fHead.SetText ();
        fPos.SetText ();
        if (ListDlg != NULL)
        {
            ListDlg->UpdateList ();
        }
}

void SpezDlg::SwitchListMeMode (void)
{
        if (work.GetListMeMode () == MENGE)
        {
			work.HoleAuftraege();
            SetListMeMode (AUFTRAEGE);
        }
        else if (work.GetListMeMode () == AUFTRAEGE)
        {
            SetListMeMode (CHARGEN);
        }
        else if (work.GetListMeMode () == CHARGEN)
        {
            SetListMeMode (MENGE);
        }
}


BOOL SpezDlg::OnKey6 ()
{
         Enable (&fcForm, EnablePos,  FALSE); 
        work.BeginWork ();
		 HoleStnd();
        return EnterList ();
}

BOOL SpezDlg::EnterList ()
{
	char aktcol [5];
        if (ListDlg != NULL) 
        {
            return TRUE;
        }

		 strcpy (work.datvon,defaultlistf.datvon);
		 strcpy (work.datbis,defaultlistf.datbis);
		 /**
        if (atoi (defaultlistf.mdn) == 0)
        {
            disp_mess ("Es wurde kein Mandant erfasst",2);
            fHead.SetCurrentName ("mdn");
            return TRUE;
        }
		***/
        if (atoi (defaultlistf.kw) == 0)
        { 
            disp_mess ("Es wurde keine Kalenderwoche erfasst erfasst",2);
            fHead.SetCurrentName ("kw");
            return TRUE;
        }
		              //041105 bgut1 AktDat stand falsch 
		AktDat = 0;   
		if (GetCheckMo() == 1) 
		{
			if (AktDat == 0) AktDat = 2;
		}
		if (GetCheckDi() == 1) 
		{
			if (AktDat == 0) AktDat = 3;
		}
		if (GetCheckMi() == 1) 
		{
			if (AktDat == 0) AktDat = 4;
		}
		if (GetCheckDo() == 1) 
		{
			if (AktDat == 0) AktDat = 5;
		}
		if (GetCheckFr() == 1) 
		{
			if (AktDat == 0) AktDat = 6;
		}
		if (GetCheckSa() == 1) 
		{
			if (AktDat == 0) AktDat = 7;
		}
		if (AktDat == 0) AktDat = 2;

		work.SetDatOffset (AktDat); 

        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_ERFMODE,      MF_ENABLED);
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_MASCHENTER,   MF_GRAYED);
        if  (ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f6") != NULL)
        {
		             SetListMeMode (work.GetListMeMode ());

                      ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f6")->GetFeld ())->bmp = 
                                   ImageList.Inactive;
                       ActiveSpezDlg->GetToolbar2 ()->GetCfield ("absch")->Enable (TRUE); 
        }

        fHead.GetText ();
        prstatatag.mdn = atoi (defaultlistf.mdn);
        work.SetAbt (atoi (defaultlistf.abt));
        work.SetStnd (atoi (defaultlistf.stnd));
        if (work.TestListAnz () == FALSE)
        {
            print_mess (2, "Keine Artikel f�r Abteilung %hd gefunden",
			atoi (defaultlistf.abt));
            fHead.SetCurrentName ("abt");
            return TRUE;
        }

        EnableMenuItem (ActiveDlg->GethMenu (),      VK_F12,       MF_ENABLED);
        DLG::OnMove (hMainWindow, WM_MOVE, 0, 0l);
        UpdateWindow (hMainWindow);
        fHead.GetText (); 
        LISTDLG::work = &work; 
        ListDlg = new LISTDLG ((short) atoi (defaultlistf.mdn), 
                               (short) atoi (defaultlistf.abt),
                               (short) atoi (defaultlistf.kw),
                               (short) atoi (defaultlistf.jr));
        ShowList ();
        ListClass::ActiveList->SetListLines (ListLines);
        ListClass::ActiveList->SetColors (ListColor,ListBkColor);


        Enable (&fcForm, EnableHead, FALSE); 
        ListDlg->SetDockButtonSpace (1);
        ListDlg->ShowList ();
        ListDlg->SetWorkMode (TRUE);
        InValidate ();
//020905 muss in Abh�ngigkeit vom einstieg erfolgen         ListDlg->BeforeCol ("me1");
         sprintf (aktcol, "me%d", work.GetDatOffset()); 
         ListDlg->BeforeCol (clipped(aktcol)); 
       return TRUE;
} 

BOOL SpezDlg::EnterMaschLst ()
{
        if (MaschLst != NULL)
        {
            return TRUE;
        }


        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_ERFMODE,      MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_MASCHENTER,   MF_ENABLED);
        if  (ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f6") != NULL)
        {
                      ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f6")->GetFeld ())->bmp = 
                                   ImageList.Inactive;
                       ActiveSpezDlg->GetToolbar2 ()->GetCfield ("absch")->Enable (FALSE); 
        }

        fHead.GetText ();
        prstatatag.mdn = atoi (defaultlistf.mdn); 
        work.SetAbt (atoi (defaultlistf.abt));
        work.SetStnd (atoi (defaultlistf.stnd));
        EnableMenuItem (ActiveDlg->GethMenu (),      VK_F12,       MF_ENABLED);
        DLG::OnMove (hMainWindow, WM_MOVE, 0, 0l);
        UpdateWindow (hMainWindow);
        fHead.GetText (); 
        MASCHLST::work = &work; 
        work.SetAktDat (AktDat);
        strcpy (defaultlistf.prod_dat, AktDatS);
        MaschLst = new MASCHLST ((short) atoi (defaultlistf.mdn), AktDat); 
        ShowMaschLst ();
        ListClass::ActiveList->SetListLines (ListLines);
        ListClass::ActiveList->SetColors (ListColor,ListBkColor);

        Enable (&fcForm, EnableHead, FALSE); 
        MaschLst->SetDockButtonSpace (1);
        MaschLst->ShowList ();
        MaschLst->SetWorkMode (TRUE);
        InValidate ();
        MaschLst->BeforeCol ("masch1");
        return TRUE;
} 

BOOL SpezDlg::EnterPhaseLst ()
{
        if (PhaseLst != NULL)
        {
            return FALSE;
        }


        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_ERFMODE,      MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_MASCHENTER,   MF_GRAYED);
        if  (ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f6") != NULL)
        {
                      ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f6")->GetFeld ())->bmp = 
                                   ImageList.Inactive;
                       ActiveSpezDlg->GetToolbar2 ()->GetCfield ("absch")->Enable (FALSE); 
        }

        fHead.GetText ();
        prstatatag.mdn = atoi (defaultlistf.mdn);
        work.SetAbt (atoi (defaultlistf.abt));
        work.SetStnd (atoi (defaultlistf.stnd));

        EnableMenuItem (ActiveDlg->GethMenu (),      VK_F12,       MF_ENABLED);
        DLG::OnMove (hMainWindow, WM_MOVE, 0, 0l);
        UpdateWindow (hMainWindow);
        fHead.GetText (); 
        PHASELST::work = &work; 
        work.SetAktDat (AktDat);
        strcpy (defaultlistf.prod_dat, AktDatS);
        PhaseLst = new PHASELST ((short) atoi (defaultlistf.mdn), AktDat); 
        ShowPhaseLst ();
        ListClass::ActiveList->SetListLines (ListLines);
        ListClass::ActiveList->SetColors (ListColor,ListBkColor);

        Enable (&fcForm, EnableHead, FALSE); 
        PhaseLst->SetDockButtonSpace (1);
        PhaseLst->ShowList ();
        PhaseLst->SetWorkMode (TRUE);
        InValidate ();
        return TRUE;
} 

BOOL SpezDlg::EnterPersLst ()
{
	char aktcol [5];
        if (PersLst != NULL)
        {
            return FALSE;
        }


        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_ERFMODE,      MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_MASCHENTER,   MF_GRAYED);
        if  (ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f6") != NULL)
        {
			// ToDo Button zum switchen
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_ERFMODE,  MF_CHECKED);
            ((ColButton *)Toolbar2->GetCfield ("absch")->GetFeld ())->text1 = "Namen<->Nummer";
            Toolbar2->SetText ();

                      ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f6")->GetFeld ())->bmp = 
                                   ImageList.Inactive;
                       ActiveSpezDlg->GetToolbar2 ()->GetCfield ("absch")->Enable (TRUE); 

        }

        fHead.GetText ();
        prstatatag.mdn = atoi (defaultlistf.mdn);
        work.SetAbt (atoi (defaultlistf.abt));
        work.SetStnd (atoi (defaultlistf.stnd));

        EnableMenuItem (ActiveDlg->GethMenu (),      VK_F12,       MF_ENABLED);
        DLG::OnMove (hMainWindow, WM_MOVE, 0, 0l);
        UpdateWindow (hMainWindow);
        fHead.GetText (); 
        PERSLST::work = &work; 
        work.SetAktDat (AktDat);
        strcpy (defaultlistf.prod_dat, AktDatS);
        PersLst = new PERSLST ((short) atoi (defaultlistf.mdn), (short) atoi (defaultlistf.kw),(short) atoi (defaultlistf.jr)); 
        ShowPersLst ();
        ListClass::ActiveList->SetListLines (ListLines);
        ListClass::ActiveList->SetColors (ListColor,ListBkColor);

        Enable (&fcForm, EnableHead, FALSE); 
        PersLst->SetDockButtonSpace (1);
        PersLst->ShowList ();
        PersLst->SetWorkMode (TRUE);
        InValidate ();
         sprintf (aktcol, "pers%d_1", work.GetDatOffset()); 
         PersLst->BeforeCol (clipped(aktcol)); 
        return TRUE;
} 

BOOL SpezDlg::EnterErfLst ()
{
        if (ErfLst != NULL)
        {
            return FALSE;
        }


        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_ERFMODE,      MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_MASCHENTER,   MF_GRAYED);
        if  (ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f6") != NULL)
        {
                      ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f6")->GetFeld ())->bmp = 
                                   ImageList.Inactive;
                       ActiveSpezDlg->GetToolbar2 ()->GetCfield ("absch")->Enable (FALSE); 
        }

        fHead.GetText ();
        prstatatag.mdn = atoi (defaultlistf.mdn);
        work.SetAbt (atoi (defaultlistf.abt));

        EnableMenuItem (ActiveDlg->GethMenu (),      VK_F12,       MF_ENABLED);
        DLG::OnMove (hMainWindow, WM_MOVE, 0, 0l);
        UpdateWindow (hMainWindow);
        fHead.GetText (); 
        ERFLST::work = &work; 
        work.SetAktDat (AktDat);
        strcpy (defaultlistf.prod_dat, AktDatS);
        ErfLst = new ERFLST ((short) atoi (defaultlistf.mdn), AktDat); 
        ShowErfLst ();
        ListClass::ActiveList->SetListLines (ListLines);
        ListClass::ActiveList->SetColors (ListColor,ListBkColor);

        Enable (&fcForm, EnableHead, FALSE); 
        ErfLst->SetDockButtonSpace (1);
        ErfLst->ShowList ();
        ErfLst->SetWorkMode (TRUE);
        InValidate ();
        return TRUE;
} 


BOOL SpezDlg::OnKey7 ()
{
        if (ErfLst != NULL)
        {
            return ErfLst->OnKey7 ();
        }

        return FALSE;
}


BOOL SpezDlg::ShowMdn (void)
{
             char mdn_nr [6];
             SEARCHMDN SearchMdn;
 
             SearchMdn.SetParams (DLG::hInstance, ActiveDlg->GethWnd ());
             SearchMdn.Setawin (ActiveDlg->GethMainWindow ());
             SearchMdn.Search ();
             if (SearchMdn.GetKey (mdn_nr) == TRUE)
             {
                 sprintf (defaultlistf.mdn, "%4d", atoi (mdn_nr));
                 work.GetMdnName (defaultlistf.mdn_krz,   atoi (defaultlistf.mdn));
                 fHead.SetText ();
                 fHead.SetCurrentName ("mdn");
             }
             return TRUE;
}


BOOL SpezDlg::ShowAbt (void)
{
//             short abt; 
/*
             SEARCHABT SearchAbt;
 
             SearchAbt.SetParams (DLG::hInstance, ActiveDlg->GethWnd ()); 
             SearchAbt.Setawin (ActiveDlg->GethMainWindow ());
                                                  
             SearchAbt.Search ();
             if (SearchAbt.GetKey (&abt) == TRUE)
             {
                 sprintf (defaultlistf.abt, "%d", abt);
                 work.GetAbtBez (defaultlistf.abt_bz1, abt);
                 fHead.SetText ();
                 fHead.SetCurrentName ("abt");
             }
*/
             if (work.ShowPtabEx (hWnd, defaultlistf.abt, "prod_abt") != -1)
             {
                 work.GetAbtBez (defaultlistf.abt_bz1, atoi (defaultlistf.abt));
                 fHead.SetText ();
             }
             fHead.SetCurrentName ("abt");
             return TRUE;
}


BOOL SpezDlg::ShowFil (void)
{
             char fil_nr [6];
             SEARCHFIL SearchFil;
 
             if (atoi (defaultlistf.mdn) == 0) return TRUE;

             SearchFil.SetParams (DLG::hInstance, ActiveDlg->GethWnd (), 
                                                  atoi (defaultlistf.mdn));
             SearchFil.Setawin (ActiveDlg->GethMainWindow ());
             SearchFil.Search ();
             if (SearchFil.GetKey (fil_nr) == TRUE)
             {
                 sprintf (defaultlistf.fil, "%4d", atoi (fil_nr));
                 work.GetFilName (defaultlistf.fil_krz,   atoi (defaultlistf.mdn), atoi (defaultlistf.fil));
                 fHead.SetText ();
                 fHead.SetCurrentName ("fil");
             }
             return TRUE;
}


BOOL SpezDlg::OnKey9 ()
{
         CFIELD *Cfield;
         HWND hWnd;

         syskey = KEY9;
         hWnd = GetFocus ();


         if (ListDlg != NULL)
         {
             return ListDlg->OnKey9 ();
         }
         else if (MaschLst != NULL)
         {
             return MaschLst->OnKey9 ();
         }
         else if (PhaseLst != NULL)
         {
             return PhaseLst->OnKey9 ();
         }
         else if (PersLst != NULL)
         {
             return PersLst->OnKey9 ();
         }
         else if (ErfLst != NULL)
         {
             return ErfLst->OnKey9 ();
         }

         if (hWnd == fHead.GetCfield ("mdn_choise")->GethWnd ())
         {
             return ShowMdn ();
         }

         if (hWnd == fHead.GetCfield ("fil_choise")->GethWnd ())
         {
             return ShowFil ();
         }

         if (hWnd == fHead.GetCfield ("abt_choise")->GethWnd ())
         {
             return ShowAbt ();
         }


         Cfield = ActiveDlg->GetCurrentCfield ();

         if (strcmp (Cfield->GetName (), "mdn") == 0)
         {
             return ShowMdn ();
         }

         if (strcmp (Cfield->GetName (), "fil") == 0)
         {
             return ShowFil ();
         }
         if (strcmp (Cfield->GetName (), "abt") == 0)
         {
             return ShowAbt ();
         }
         return FALSE;
}


BOOL SpezDlg::OnPaint (HWND hWnd,HDC hdc, UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (fWork && this->hWnd == hWnd)
        {
             PrintMess (MessText);
             fWork->display (hWnd, hdc);
             return TRUE;
        }
        return DLG::OnPaint (hWnd,hdc, msg, wParam,lParam);
}


BOOL SpezDlg::OnMove (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (hWnd != this->hMainWindow) return FALSE; 
        DLG::OnMove (hWnd,msg,wParam,lParam); 
        if (ListDlg != NULL)
        {
            ListDlg->MoveMamain1 ();
        }
        if (MaschLst != NULL)
        {
            MaschLst->MoveMamain1 ();
        }
        if (PhaseLst != NULL)
        {
            PhaseLst->MoveMamain1 ();
        }
        if (PersLst != NULL)
        {
            PersLst->MoveMamain1 ();
        }
        if (ErfLst != NULL)
        {
            ErfLst->MoveMamain1 ();
        }

        return FALSE;
}

BOOL SpezDlg::OnActivate (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (LOWORD (wParam) == WA_ACTIVE ||
                LOWORD (wParam) == WA_CLICKACTIVE)
        {
              if (WithTabStops)
              {
                  SetTabFocus (); 
              }
              else
              {
                  fWork->SetFocus ();
              }
              return TRUE;
        }
        return FALSE;
}

BOOL SpezDlg::ChangeDockList (void)
{
        if (DockList)
        {
            DockList = FALSE;
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_DOCKLIST,   MF_UNCHECKED);
        }
        else
        {
            DockList = TRUE;
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_DOCKLIST,   MF_CHECKED);
        }
        return TRUE;
}


BOOL SpezDlg::ChangeScrPrint (void)
{
        if (LISTDLG::ScrPrint)
        {
            LISTDLG::ScrPrint = FALSE;
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_SCRPRINT,   MF_UNCHECKED);
        }
        else
        {
            LISTDLG::ScrPrint = TRUE;
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_SCRPRINT,   MF_CHECKED);
        }
        return TRUE;
}


BOOL SpezDlg::ChangeMatrix (void)
{

        if (work.MatrixTyp == BIG)
        {
            work.MatrixTyp = SMALL;
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_MALIEF,   MF_CHECKED);
        }
        else
        {
            work.MatrixTyp = BIG;
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_MALIEF,   MF_UNCHECKED);
        }

        return TRUE;
}


BOOL SpezDlg::ChangeArtScroll (void)
{

        if (work.ArtScroll)
        {
            work.ArtScroll = FALSE;
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_ARTSCROLL,   MF_UNCHECKED);
        }
        else
        {
            work.ArtScroll = TRUE;
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_ARTSCROLL,   MF_CHECKED);
        }

        return TRUE;
}


BOOL SpezDlg::OnCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (fWork)
        {

              if (LOWORD (wParam) == CANCEL_CTL)
              {
                  return OnKey5 ();
              }
              if (LOWORD (wParam) == OK_CTL)
              {
                  return OnKey12 ();
              }
              if (LOWORD (wParam) == IDM_CHOISE)
              {
                  return OnKey9 ();
              }

              if (LOWORD (wParam) == IDM_LIST)
              {
                  return OnKey6 ();
              }

              if (LOWORD (wParam) == IDM_ERFMODE)
              {
                  return OnKey10 ();
              }

              if (LOWORD (wParam) == IDM_MASCHENTER)
              {
                  return OnKey11 ();
              }

              if (LOWORD (wParam) == IDM_PRINT)
              {
                  if (ListDlg != NULL)
                  {
                         return ListDlg->OnCommand (hWnd,msg,wParam,lParam);
                  }
              }
              if (LOWORD (wParam) == IDM_CHPRINT)
              {
                  if (ListDlg != NULL)
                  {
			            ListDlg->WriteList (); //020204
						work.Upd99 ();

					    if (&work != NULL)
						{
							work.CommitWork ();
							work.BeginWork ();
						}
			 			 ListDlg->PrintRezaufloesung ();
//                         return ListDlg->OnCommand (hWnd,msg,wParam,lParam);
						 return TRUE;
                  }
              }

              if (LOWORD (wParam) == IDM_SCRPRINT)
              {
                  return ChangeScrPrint ();
              }

              if (LOWORD (wParam) == IDM_DOCKLIST)
              {
                  return ChangeDockList ();
              }

              if (LOWORD (wParam) == IDM_MALIEF)
              {
                  return ChangeMatrix ();
              }

              if (LOWORD (wParam) == IDM_ARTSCROLL)
              {
                  return ChangeArtScroll ();
              }

 		      if (HIWORD (wParam)  == CBN_CLOSEUP)
			  {
				  return OnCloseUp (hWnd, msg, wParam, lParam);
			  }
              else if (HIWORD (wParam) == WM_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == EN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == CBN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == WM_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == LBN_SELCHANGE)
              {
//                  return ListChanged (wParam, lParam);
              }
              else if (HIWORD (wParam) == EN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == CBN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == BN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == BN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
        }
        return DLG::OnCommand (hWnd, msg, wParam, lParam);
}


BOOL SpezDlg::OnSysCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        
        if (wParam == SC_CLOSE)
        {
            if (abfragejn (hWnd, "Verarbeitung abbrechen ?", "N") == 0)
            {
                ActiveDlg->SetCurrentFocus ();
                return TRUE;
            }
            ExitProcess (0);;
        }
        return DLG::OnSysCommand (hWnd, msg, wParam, lParam); 
}


BOOL SpezDlg::OnTcnSelChange (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
         if (fHead.GetCfield ("mdn")->IsDisabled () == FALSE)
         {
               TabCtrl_SetCurSel (GetParent (this->hWnd), 0);
               iPage = TabMengen;
               return TRUE;
         }
         if (syskey == KEY5 || syskey == KEY12)
         {
               iPage = TabMengen;
               return TRUE;
         }
              
         if (LockPage)
         {
              LockWindowUpdate (hWnd);
         }
         int oldPage = iPage;
         iPage = TabCtrl_GetCurSel(GetParent (this->hWnd));
		 if (oldPage == TabMengen)
         {
                   AktDat = work.DatToLong (ListDlg->GetAktDat (AktDatS));
                   if (iPage == TabMaschinen && 
                       AktDat == 0l)
                   {
                        iPage = TabMengen;
                        TabCtrl_SetCurSel (GetParent (this->hWnd), iPage);
                        return TRUE;
                   }
                   if (iPage == TabReihenfolge && 
                       (AktDat == 0l || AktMaschNr == 0l || AktPhase == 0))
                   {
                        iPage = TabMengen;
                        TabCtrl_SetCurSel (GetParent (this->hWnd), iPage);
                        return TRUE;
                   }
                   if (iPage == TabPersonal &&    
                       AktDat == 0l)
                   {
                        iPage = TabMengen;
                        TabCtrl_SetCurSel (GetParent (this->hWnd), iPage);
                        return TRUE;
                   }
                   if (ListDlg != NULL)
                   {
                        ListDlg->StopList ();
                        ListDlg->DestroyWindows ();
                        delete ListDlg;
                        ListDlg = NULL;
                   }
         }
		 else if (oldPage == TabMaschinen)
		 {
                   if (MaschSel == FALSE)
                   {
                        AktMaschNr = MaschLst->GetAktMaschNr ();
                        AktPhase   = MaschLst->GetAktPhase (AktPhaseBz);
                   }
                   MaschSel = FALSE;
                   sprintf (defaultlistf.akt_masch_nr, "%ld", AktMaschNr);
                   sprintf (defaultlistf.akt_phase,    "%ld", AktPhase);
                   sprintf (defaultlistf.akt_phase_bz, "%s", AktPhaseBz);
                   work.GetMaschBz (defaultlistf.akt_masch_bz, AktMaschNr);
                   work.SetAktPhase (AktPhase);
                   work.SetAktMaschNr (AktMaschNr);
                   if (iPage == TabReihenfolge && 
                       (AktDat == 0l || AktMaschNr == 0l || AktPhase == 0))
                   {
                        iPage = TabMaschinen;
                        TabCtrl_SetCurSel (GetParent (this->hWnd), iPage);
                        return TRUE;
                   }

                   if (MaschLst != NULL)
                   {
                         MaschLst->StopList ();
                         MaschLst->DestroyWindows ();
                         delete MaschLst;
                         MaschLst = NULL;
                   }
         }
		 else if (oldPage == TabReihenfolge)
		 {
                   if (PhaseLst != NULL)
                   {
                         PhaseLst->StopList ();
                         PhaseLst->DestroyWindows ();
                         delete PhaseLst;
                         PhaseLst = NULL;
                   }
         }
		 else if (oldPage == TabPersonal)
		 {
                   if (PersLst != NULL)
                   {
                         PersLst->StopList ();
                         PersLst->DestroyWindows ();
                         delete PersLst;
                         PersLst = NULL;
                   }
         }
		 else if (oldPage == TabErfassung)
		 {
                   if (ErfLst != NULL)
                   {
                         ErfLst->StopList ();
                         ErfLst->DestroyWindows ();
                         delete ErfLst;
                         ErfLst = NULL;
                   }
         }

		 if (iPage == TabMengen)
         {
                   if (oldPage == TabMaschinen)
                   {
                         fPosM.destroy ();
                   }
                   else if (oldPage == TabReihenfolge)
                   {
                         fPosP.destroy ();
                   }
                   else if (oldPage == TabPersonal)
                   {
                         fPosPers.destroy ();
                   }
                   else if (oldPage == TabErfassung)
                   {
                         fPosE.destroy ();
                   }
                   SetDialog (&fcForm0);
                   EnterList ();
		 }
		 else if (iPage == TabMaschinen)
         {
                   if (oldPage == TabMengen)
                   {
                         fPos.destroy ();
                   }
                   else if (oldPage == TabReihenfolge)
                   {
                         fPosP.destroy ();
                   }
                   else if (oldPage == TabPersonal)
                   {
                         fPosPers.destroy ();
                   }
                   else if (oldPage == TabErfassung)
                   {
                         fPosE.destroy ();
                   }
                   SetDialog (&fcForm0M);
                   EnterMaschLst ();
		 }
		 else if (iPage == TabReihenfolge)
         {
                   if (oldPage == TabMengen)
                   {
                         fPos.destroy ();
                   }
                   else if (oldPage == TabMaschinen)
                   {
                         fPosM.destroy ();
                   }
                   else if (oldPage == TabPersonal)
                   {
                         fPosPers.destroy ();
                   }
                   else if (oldPage == TabErfassung)
                   {
                         fPosE.destroy ();
                   }
                   SetDialog (&fcForm0P);
                   EnterPhaseLst ();
		 }
		 else if (iPage == TabPersonal)
         {
                   if (oldPage == TabMengen)
                   {
                         fPos.destroy ();
                   }
                   else if (oldPage == TabMaschinen)
                   {
                         fPosM.destroy (); 
                   }
                   else if (oldPage == TabReihenfolge)
                   {
                         fPosP.destroy (); 
                   }
                   else if (oldPage == TabErfassung)
                   {
                         fPosE.destroy ();
                   }
                   SetDialog (&fcForm0Pers);
                   EnterPersLst ();
         }
		 else if (iPage == TabErfassung)
         {
                   if (oldPage == TabMengen)
                   {
                         fPos.destroy ();
                   }
                   else if (oldPage == TabMaschinen)
                   {
                         fPosM.destroy (); 
                   }
                   else if (oldPage == TabReihenfolge)
                   {
                         fPosP.destroy ();
                   }
                   SetDialog (&fcForm0E);
//                   SetDialog (&fcForm0);
                   EnterErfLst ();
         }
         if (LockPage)
         {
              LockWindowUpdate (NULL);
         }
         return TRUE;
}


BOOL SpezDlg::OnDestroy (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{

        if (hWnd == this->hWnd)
        {
             if (CloseProg != NULL)
             {
                   (*CloseProg) ();
             }
             PostQuitMessage (0);
             fcForm.destroy ();
        }
        return TRUE;
}

void SpezDlg::SetWinBackground (COLORREF Col)
{
          dlgfont.FontBkColor = Col;
          cpfont.FontBkColor = Col;
          DLG::SetWinBackground (Col);
}


void SpezDlg::ToClipboard (void)
{
          CFIELD *Cfield;
          HGLOBAL hb;
          LPVOID p;
          char *text;

          Cfield = GetCurrentCfield ();
          Cfield->GetText ();
          text = (char *) Cfield->GetFeld ();
          if (text == NULL) return;
          hb = GlobalAlloc (GHND, strlen (text) + 1);
          if (hb == NULL) return;
          p = GlobalLock (hb);
          strcpy ((char *) p, text);
          GlobalUnlock (hb);
          OpenClipboard (hWnd);
          EmptyClipboard ();
          SetClipboardData (CF_TEXT, hb);
          CloseClipboard ();
}

void SpezDlg::FromClipboard (void)
{
          CFIELD *Cfield;
          HGLOBAL hb;
          LPVOID p;
          char *text;

          Cfield = GetCurrentCfield ();
          text = (char *) Cfield->GetFeld ();
          if (text == NULL) return;
          if (IsClipboardFormatAvailable (CF_TEXT) == FALSE) return;
          OpenClipboard (hWnd);
          hb = GetClipboardData (CF_TEXT);
          if (hb == NULL)
          {
                 CloseClipboard ();
                 return;
          }
          p = GlobalLock (hb);
          if (strlen ((char *) p) > (unsigned int ) Cfield->GetLength ())
          {
                 GlobalUnlock (hb);
                 CloseClipboard ();
                 return;
          }
          strcpy (text, (char *) p);
          GlobalUnlock (hb);
          CloseClipboard ();
          Cfield->SetText ();
}

void SpezDlg::Help (void)
{
          CFIELD *Cfield;
          HELP *Help; 
          char *Item;

          Cfield = GetCurrentCfield ();
          if (Cfield == NULL) return;

          Item = Cfield->GetName ();

          Help = new HELP (HelpName, Item, hInstance,hMainWindow);
          delete Help;
          Cfield->SetFocus ();
}

HWND SpezDlg::OpenWindow (HINSTANCE hInstance, HWND hMainWindow)
{
          HWND hWnd;

          Settchar ('|');
          hWnd = DLG::OpenWindow (hInstance, hMainWindow);
//          FillListCaption ();
          EnterPos ();
          return hWnd;
}

void SpezDlg::DestroyPos (void)
{
    fPos.destroy ();
    fcForm.SetFieldanz (fcForm.GetFieldanz () - 1);
}

void SpezDlg::SetPos (void)
{
    fcForm.SetFieldanz (2);
    fcForm0.Invalidate (TRUE);
    fcForm0.MoveWindow ();
    UpdateWindow (hWnd);
}

HWND SpezDlg::OpenScrollWindow (HINSTANCE hInstance, HWND hMainWindow)
{
          HWND hWnd;

          EnableMenuItem (ActiveSpezDlg->GethMenu (),    IDM_DEL,   MF_GRAYED);

          Settchar ('|');
          hWnd = DLG::OpenScrollWindow (hInstance, hMainWindow);
          EnterPos ();
          return hWnd;
}

HWND SpezDlg::OpenTabWindow (HINSTANCE hInstance, HWND hMainWindow, char *TxtTab)
{
          HWND hWnd;
          TEXTMETRIC tm;
          HDC hdc;

          LockWindowUpdate (hMainWindow);
          hWnd = DLG::OpenTabWindow (hInstance, hMainWindow, TxtTab);
          OnMove (hMainWindow, 0, 0, 0l);

          hdc = GetDC (hWndTab);
          tabFont = SetDeviceFont (hdc, &dlgfont, &tm);
          oldfont = SelectObject (hdc, tabFont);
          ReleaseDC (hWndTab, hdc);
          ::SendMessage (hWndTab, WM_SETFONT, (WPARAM) tabFont, (LPARAM) NULL);
          LockWindowUpdate (NULL);
          return hWnd;
}

HWND SpezDlg::OpenScrollTabWindow (HINSTANCE hInstance, HWND hMainWindow, char *TxtTab)
{
          HWND hWnd;
          TEXTMETRIC tm;
          HDC hdc;

          LockWindowUpdate (hMainWindow);
          hWnd = DLG::OpenScrollTabWindow (hInstance, hMainWindow, TxtTab);
          OnMove (hMainWindow, 0, 0, 0l);

          hdc = GetDC (hWndTab);
          tabFont = SetDeviceFont (hdc, &dlgfont, &tm);
          oldfont = SelectObject (hdc, tabFont);
          ReleaseDC (hWndTab, hdc);
          ::SendMessage (hWndTab, WM_SETFONT, (WPARAM) tabFont, (LPARAM) NULL);
          LockWindowUpdate (NULL);
          return hWnd;
}

void SpezDlg::SetChargen (double chg, double chg_gew, double a)
{
           sprintf (defaultlistf.chg_gew, "%.3lf", chg_gew);
           sprintf (defaultlistf.chg,     "%.3lf", chg);
           sprintf (defaultlistf.a,     "%.0lf", a);
           if (ListDlg != NULL)
           {
	      
                  fHead.SetText ();
                  fPos.SetText ();
           }
           else if (MaschLst != NULL)
           {
                  fHead.SetText ();
                  fPosM.SetText ();
           }
           else if (PhaseLst != NULL)
           {
                  fHead.SetText ();
                  fPosP.SetText ();
           }
           else if (ErfLst != NULL)
           {
                  fHead.SetText ();
                  fPosE.SetText ();
           }

}
void SpezDlg::SetMaschPers (char* masch_nr, char* masch_bz, char* pers_nr , char* pers_bz, 
							double masch_me, long masch_zeit, long pers_zeit)
{
	long std ;
	long min ;
           sprintf (defaultlistf.akt_masch_nr, "%s", masch_nr);
           sprintf (defaultlistf.akt_masch_bz, "%s", masch_bz);
           sprintf (defaultlistf.akt_pers_nr, "%s", pers_nr);
           sprintf (defaultlistf.akt_pers_bz, "%s", pers_bz);
           sprintf (defaultlistf.akt_masch_me, "%.3lf kg", masch_me);
		   std = -1;
		   min = 0;
		   while (pers_zeit > 0) 
		   {
			   std++;
			   min = pers_zeit;
			   pers_zeit -= 60;
		   }
		   sprintf (defaultlistf.akt_pers_zeit, "%ld Std %ld min", std,min);
		   if (std < 1) sprintf (defaultlistf.akt_pers_zeit, "%ld min", min);
		   if (std < 1 && min == 0) sprintf (defaultlistf.akt_pers_zeit, "%s", "");


		   std = -1;
		   min = 0;
		   while (masch_zeit > 0) 
		   {
			   std++;
			   min = masch_zeit;
			   masch_zeit -= 60;
		   }
           sprintf (defaultlistf.akt_masch_zeit, "%ld min", masch_zeit);
		   sprintf (defaultlistf.akt_masch_zeit, "%ld Std %ld min", std,min);
		   if (std < 1) sprintf (defaultlistf.akt_masch_zeit, "%ld min", min);
		   if (std < 1 && min == 0) sprintf (defaultlistf.akt_masch_zeit, "%s", "");

		   if (pers_nr == 0) sprintf (defaultlistf.akt_pers_nr, "%s", "");

           if (PersLst != NULL)
           {
//                  fHead.SetText ();
                  fPosPers.SetText ();
           }
}

short SpezDlg::SetSperre (char *date, double a, short posi)
{
	double me_zusatz = (double) 0;
	char text [60];
           if (ListDlg != NULL)
           {
			    short mdn = atoi(defaultlistf.mdn);
			    strcpy (SperrInfo, "");
                if (work.GetListMeMode () == CHARGEN)	sprintf(SummeInfo,"Summe: %4.1f Chargen",ListDlg->RechneSumme());
                if (work.GetListMeMode () == MENGE)	sprintf(SummeInfo,"Summe: %4.3f kg",ListDlg->RechneSumme());
                if (work.GetListMeMode () == AUFTRAEGE)	sprintf(SummeInfo,"AuftragsSumme: %4.0f kg",ListDlg->RechneSumme());
                int status = work.PruefeStatus(date,a,mdn,posi,&me_zusatz);
			    //if (work.PruefeStatus(date,a,mdn) == 0)
				if (status > 0)
				{
					 if (status == 9) 
					 {
						 sprintf(text,"+ %.3f kg",me_zusatz);
						 strcpy (SperrInfo, text);
					 }
					 if (status == 1) strcpy (SperrInfo, "in Bearbeitung");
					 if (status == 2) strcpy (SperrInfo, "Abgeschlossen");
	                 fPos.SetText ();
					 return 1;
				}
                status = work.PruefeGrundbraete(date,a,posi,mdn); //010705
				if (status == 10)
				{
					strcpy (SperrInfo, "Grundbr�t in Bearb.");
	                fPos.SetText ();
					return 2;
				}
		   }
		   return 0; 
}

short SpezDlg::SetSperreErf (char *date, double a, short posi)
{
	double me_zusatz = (double) 0;
	char text [60];
           if (ErfLst != NULL)
           {
			    short mdn = atoi(defaultlistf.mdn);
			    strcpy (SperrInfo, "");
                int status = work.PruefeStatus(date,a,mdn,posi,&me_zusatz);
				if (status > 0)
				{
					 if (status == 9) 
					 {
						 sprintf(text,"+ %.3f kg",me_zusatz);
						 strcpy (SperrInfo, text);
					 }
					 if (status == 1) strcpy (SperrInfo, "in Bearbeitung");
					 if (status == 2) strcpy (SperrInfo, "Abgeschlossen");
	                 fPosE.SetText ();
					 return 1;
				}
                status = work.PruefeGrundbraete(date,a,posi,mdn); //010705
				if (status == 10)
				{
					strcpy (SperrInfo, "Grundbr�t in Bearb.");
	                fPosE.SetText ();
					return 2;
				}
		   }
		   return 0; 
}

int SpezDlg::SchreibeZusatz (char *date, double a, short mdn, double menge )
{
           if (ListDlg != NULL)
           {
//			    short mdn = atoi(defaultlistf.mdn);
				return work.SchreibeZusatz (date,a,mdn,menge);
		   }
		   return -1;
}

int SpezDlg::GetCheckMo (void) 
{
    fPos.GetText ();
	if (strcmp(defaultlistf.check_mo,"J") == 0) return 1;
	return 0;
}
int SpezDlg::GetCheckDi (void)
{
    fPos.GetText ();
	if (strcmp(defaultlistf.check_di,"J") == 0) return 1;
	return 0;
}
int SpezDlg::GetCheckMi (void)
{
    fPos.GetText ();
	if (strcmp(defaultlistf.check_mi,"J") == 0) return 1;
	return 0;
}
int SpezDlg::GetCheckDo (void)
{
    fPos.GetText ();
	if (strcmp(defaultlistf.check_do,"J") == 0) return 1;
	return 0;
}
int SpezDlg::GetCheckFr (void)
{
    fPos.GetText ();
	if (strcmp(defaultlistf.check_fr,"J") == 0) return 1;
	return 0;
}
int SpezDlg::GetCheckSa (void)
{
    fPos.GetText ();
	if (strcmp(defaultlistf.check_sa,"J") == 0) return 1;
	return 0;
}

void SpezDlg::SetBearb (BOOL b)
{
           if (b)
           {
                  strcpy (defaultlistf.bearb, "J");
           }
           else
           {
                  strcpy (defaultlistf.bearb, "N");
           }
           fWork->GetCfield ("bearb")->SetText ();
}

void SpezDlg::SetBearbR (BOOL b)
{
           if (b)
           {
                  strcpy (defaultlistf.bearbr, "J");
           }
           else
           {
                  strcpy (defaultlistf.bearbr, "N");
           }
           fWork->GetCfield ("bearbr")->SetText ();
}

void SpezDlg::ShowList (void)
{
          int DockY;
          RECT rect;

		  ListDlg->SetDlg (this);
          ListDlg->SetWork (&work);
          if (DockList == FALSE)
          {
                 ListDlg->SetWinStyle (ListDlg->GetWinStyle () | WS_CAPTION | 
                                                                 WS_SYSMENU |
                                                                 WS_MINIMIZEBOX |
                                                                 WS_MAXIMIZEBOX);
                ListDlg->SetDockWin (FALSE);
                ListDlg->SetDimension (750, 400);
                ListDlg->SetModal (TRUE);
          }
          else
          {
                if (DockMode)
                {
                   ListDlg->SetWinStyle (WS_THICKFRAME | WS_CHILD | WS_CLIPSIBLINGS);
                   Enable (&fcForm, EnablePos, FALSE); 
//                DestroyPos ();
                }
                GetWindowRect (hWnd, &rect);
                DockY = fPos.GetCfield ("pos_frame")->GetYP () + rect.top;
//                DockY = 309;
                ListDlg->SetDockY (DockY);
          }

          ListDlg->SethMenu (hMenu);
          ListDlg->SethwndTB (hwndTB);
          ListDlg->SethMainWindow (hWnd);
          ListDlg->SetTextMetric (&DlgTm);
          ListDlg->SetLineRow (100);
// Liste gleich anzeigen
//          if (DockList)
//          {
//               ListDlg->ShowList ();
//          }
}

void SpezDlg::ShowMaschLst (void)
{
          int DockY;
          RECT rect;

		  MaschLst->SetDlg (this);
          MaschLst->SetWork (&work);
          if (DockList == FALSE)
          {
                 MaschLst->SetWinStyle (MaschLst->GetWinStyle () | WS_CAPTION | 
                                                                 WS_SYSMENU |
                                                                 WS_MINIMIZEBOX |
                                                                 WS_MAXIMIZEBOX);
                MaschLst->SetDockWin (FALSE);
                MaschLst->SetDimension (750, 400);
                MaschLst->SetModal (TRUE);
          }
          else
          {
                if (DockMode)
                {
                   MaschLst->SetWinStyle (WS_THICKFRAME | WS_CHILD | WS_CLIPSIBLINGS);
                   Enable (&fcForm, EnablePos, FALSE); 
//                DestroyPos ();
                }
                GetWindowRect (hWnd, &rect);
                DockY = fPos.GetCfield ("pos_frame")->GetYP () + rect.top;
//                DockY = 309;
                MaschLst->SetDockY (DockY);
          }

          MaschLst->SethMenu (hMenu);
          MaschLst->SethwndTB (hwndTB);
          MaschLst->SethMainWindow (hWnd);
          MaschLst->SetTextMetric (&DlgTm);
          MaschLst->SetLineRow (100);
// Liste gleich anzeigen
//          if (DockList)
//          {
//               MaschLst->ShowList ();
//          }
}

void SpezDlg::ShowPhaseLst (void)
{
          int DockY;
          RECT rect;

		  PhaseLst->SetDlg (this);
          PhaseLst->SetWork (&work);
          if (DockList == FALSE)
          {
                 PhaseLst->SetWinStyle (PhaseLst->GetWinStyle () | WS_CAPTION | 
                                                                 WS_SYSMENU |
                                                                 WS_MINIMIZEBOX |
                                                                 WS_MAXIMIZEBOX);
                PhaseLst->SetDockWin (FALSE);
                PhaseLst->SetDimension (750, 400);
                PhaseLst->SetModal (TRUE);
          }
          else
          {
                if (DockMode)
                {
                   PhaseLst->SetWinStyle (WS_THICKFRAME | WS_CHILD | WS_CLIPSIBLINGS);
                   Enable (&fcForm, EnablePos, FALSE); 
//                DestroyPos ();
                }
                GetWindowRect (hWnd, &rect);
                DockY = fPosP.GetCfield ("pos_frame")->GetYP () + rect.top;
//                DockY = 309;
                PhaseLst->SetDockY (DockY);
          }

          PhaseLst->SethMenu (hMenu);
          PhaseLst->SethwndTB (hwndTB);
          PhaseLst->SethMainWindow (hWnd);
          PhaseLst->SetTextMetric (&DlgTm);
          PhaseLst->SetLineRow (100);
// Liste gleich anzeigen
//          if (DockList)
//          {
//               PhaseLst->ShowList ();
//          }
}
void SpezDlg::ShowPersLst (void)
{
          int DockY;
          RECT rect;

		  PersLst->SetDlg (this);
          PersLst->SetWork (&work);
          if (DockList == FALSE)
          {
                 PersLst->SetWinStyle (PersLst->GetWinStyle () | WS_CAPTION | 
                                                                 WS_SYSMENU |
                                                                 WS_MINIMIZEBOX |
                                                                 WS_MAXIMIZEBOX);
                PersLst->SetDockWin (FALSE);
                PersLst->SetDimension (750, 400);
                PersLst->SetModal (TRUE);
          }
          else 
          {
                if (DockMode)
                {
                   PersLst->SetWinStyle (WS_THICKFRAME | WS_CHILD | WS_CLIPSIBLINGS);
                   Enable (&fcForm, EnablePos, FALSE); 
                }
                GetWindowRect (hWnd, &rect);
                DockY = fPosPers.GetCfield ("pos_frame")->GetYP () + rect.top;
                PersLst->SetDockY (DockY);
          }

          PersLst->SethMenu (hMenu);
          PersLst->SethwndTB (hwndTB);
          PersLst->SethMainWindow (hWnd);
          PersLst->SetTextMetric (&DlgTm);
          PersLst->SetLineRow (100);
}

void SpezDlg::ShowErfLst (void)
{
          int DockY;
          RECT rect;

		  ErfLst->SetDlg (this);
          ErfLst->SetWork (&work);
          if (DockList == FALSE)
          {
                 ErfLst->SetWinStyle (PhaseLst->GetWinStyle () | WS_CAPTION | 
                                                                 WS_SYSMENU |
                                                                 WS_MINIMIZEBOX |
                                                                 WS_MAXIMIZEBOX);
                ErfLst->SetDockWin (FALSE);
                ErfLst->SetDimension (750, 400);
                ErfLst->SetModal (TRUE);
          }
          else
          {
                if (DockMode)
                {
                   ErfLst->SetWinStyle (WS_THICKFRAME | WS_CHILD | WS_CLIPSIBLINGS);
                   Enable (&fcForm, EnablePos, FALSE); 
                }
                GetWindowRect (hWnd, &rect);
                DockY = fPos.GetCfield ("pos_frame")->GetYP () + rect.top;
                ErfLst->SetDockY (DockY);
          }

          ErfLst->SethMenu (hMenu);
          ErfLst->SethwndTB (hwndTB);
          ErfLst->SethMainWindow (hWnd);
          ErfLst->SetTextMetric (&DlgTm);
          ErfLst->SetLineRow (100);
}


void SpezDlg::PrintComment (char *Name)
{
          HELP *Help; 
          char Comment [256];

          Help = new HELP (HelpName, Name, hInstance,hMainWindow, INITCOMM);
          Help->GetItem (Name, Comment);
          PrintMess (Comment);
          delete Help;
}

void SpezDlg::CallInfo (void)
{
          DLG::CallInfo ();
}



void SpezDlg::SetListLines (int ListLines)
{
          this->ListLines = ListLines;
          if (ListClass::ActiveList != NULL)
          {
		      ListClass::ActiveList->SetListLines (ListLines);
          }
          LineSet         = TRUE;
}

void SpezDlg::SetListColors (COLORREF ListColor, COLORREF ListBkColor)
{
          this->ListColor   = ListColor;
          this->ListBkColor = ListBkColor;
          if (ListClass::ActiveList != NULL)
          {
		      ListClass::ActiveList->SetColors (ListColor,ListBkColor);
          }
          ColorSet          = TRUE;

}


HWND SpezDlg::SetToolCombo (int Id, int tbn1, int tbn2, int ComboPtr)
{
         HWND hwndCombo; 

		 hwndCombo = DLG::SetToolCombo (Id, tbn1, tbn2);

		 switch (ComboPtr)
		 {
		     case 0:
                   hwndCombo1 = hwndCombo;
				   break;
			 case 1:
                   hwndCombo2 = hwndCombo;
				   break;
		 }
         return hwndCombo;
}

void SpezDlg::SetComboTxt (HWND hwndCombo, char **Combo, int Select, int ComboPtr)
{

	  DLG::SetComboTxt (hwndCombo, Combo, Select);

 	  switch (ComboPtr)
	  {
		     case 0:
                   Combo1 = Combo;
				   break;
			 case 1:
                   Combo2 = Combo;
				   break;
	  }
}


BOOL SpezDlg::OnCloseUp (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
	    if ((HWND) lParam == hwndCombo1)
		{
			ChoiseCombo1 ();
			return TRUE;
		}
		else if ((HWND) lParam == hwndCombo2)
		{
			ChoiseCombo2 ();
			return TRUE;
		}
		return FALSE;
}


void SpezDlg::ChoiseCombo1 (void)
/**
Aktuell gewaehlten eintrag in Combobox 1 ermitteln.
**/
{
          int i;
          char text [80];

          GetWindowText (hwndCombo1,
                         text,
                         79);
          clipped (text);
          for (i = 0; Combo1[i]; i ++)
          {
              if (strcmp (Combo1[i],text) == 0)
              {
                   SetListLines (i);
                   break;
              }
          }
          if (ActiveDlg != NULL && 
              ActiveDlg->GetCurrentCfield ()->IsDisabled == FALSE)
          {
              ActiveDlg->SetCurrentFocus ();
          }
}

void SpezDlg::ChoiseCombo2 (void)
/**
Aktuell gewaehlten eintrag in Combobox 1 ermitteln.
**/
{
          int i;
          char text [80];

          GetWindowText (hwndCombo2,
                         text,
                         79);
          clipped (text);
          for (i = 0; Combo2[i]; i ++)
          {
              if (strcmp (Combo2[i],text) == 0)
              {
                   SetListColors (Colors[i], BkColors[i]);
                   break;
              }
          }
          if (ActiveDlg != NULL && 
              ActiveDlg->GetCurrentCfield ()->IsDisabled == FALSE)
          {
              ActiveDlg->SetCurrentFocus ();
          }
}


void SpezDlg::ProcessMessages (void)
{
//          ShowList ();
          DLG::ProcessMessages ();
}


void SpezDlg::HoleStnd (void)
{
//	char datum[11];
	short checkanz = 0;
	short wotag = 0;
		if (GetCheckMo() == 1) 
		{
			wotag = 1;
			checkanz ++;
		}
		if (GetCheckDi() == 1) 
		{
			wotag = 2;
			checkanz ++;
		}
		if (GetCheckMi() == 1) 
		{
			wotag = 3;
			checkanz ++;
		}
		if (GetCheckDo() == 1) 
		{
			wotag = 4;
			checkanz ++;
		}
		if (GetCheckFr() == 1) 
		{
			wotag = 5;
			checkanz ++;
		}
		if (GetCheckSa() == 1) 
		{
			wotag = 6;
			checkanz ++;
		}
//		 sysdate (datum);
//		 short plan = work.HoleStnd (get_wochentag(datum));
		 short plan = work.HoleStnd (wotag);
		 if (atoi(defaultlistf.stnd) == 0)
		 {
			 if (checkanz == 1)
			 {
				sprintf (defaultlistf.stnd, "%4d", plan);
		         fHead.SetText ();
			 }
		 }

}

void SpezDlg::SetTabStack (short pos1, short pos2, short pos3, short pos4)
{
if (pos1 != TabMaschinen && pos2 != TabMaschinen && pos3 != TabMaschinen && pos4 != TabMaschinen) TabMaschinen = 99;
if (pos1 == TabMaschinen) TabMaschinen = 1;
if (pos2 == TabMaschinen) TabMaschinen = 2;
if (pos3 == TabMaschinen) TabMaschinen = 3;
if (pos4 == TabMaschinen) TabMaschinen = 4;

if (pos1 != TabReihenfolge && pos2 != TabReihenfolge && pos3 != TabReihenfolge && pos4 != TabReihenfolge) TabReihenfolge = 99;
if (pos1 == TabReihenfolge) TabReihenfolge = 1;
if (pos2 == TabReihenfolge) TabReihenfolge = 2;
if (pos3 == TabReihenfolge) TabReihenfolge = 3;
if (pos4 == TabReihenfolge) TabReihenfolge = 4;

if (pos1 != TabPersonal && pos2 != TabPersonal && pos3 != TabPersonal && pos4 != TabPersonal) TabPersonal = 99;
if (pos1 == TabPersonal) TabPersonal = 1;
if (pos2 == TabPersonal) TabPersonal = 2;
if (pos3 == TabPersonal) TabPersonal = 3;
if (pos4 == TabPersonal) TabPersonal = 5;

if (pos1 != TabErfassung && pos2 != TabErfassung && pos3 != TabErfassung && pos4 != TabErfassung) TabErfassung = 99;
if (pos1 == TabErfassung) TabErfassung = 1;
if (pos2 == TabErfassung) TabErfassung = 2;
if (pos3 == TabErfassung) TabErfassung = 3;
if (pos4 == TabErfassung) TabErfassung = 4;

}
