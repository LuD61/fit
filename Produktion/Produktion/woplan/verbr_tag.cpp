#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "wmaskc.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "verbr_tag.h"

struct VERBR_TAG verbr_tag, verbr_tag_null;

void VERBR_TAG_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *)   &verbr_tag.mdn, 1, 0);
            ins_quest ((char *)   &verbr_tag.dat,  2, 0);
            ins_quest ((char *)   &verbr_tag.artikel,   3, 0);
            ins_quest ((char *)   &verbr_tag.a,   3, 0);
            ins_quest ((char *)   &verbr_tag.bearb_info,   0, 81);
            ins_quest ((char *)   &verbr_tag.prdk_stufe, 1, 0);
    out_quest ((char *) &verbr_tag.mdn,1,0);
    out_quest ((char *) &verbr_tag.dat,2,0);
    out_quest ((char *) &verbr_tag.a,3,0);
    out_quest ((char *) &verbr_tag.me_ist,3,0);
    out_quest ((char *) &verbr_tag.anz_einh,3,0);
    out_quest ((char *) &verbr_tag.zerl_me,3,0);
    out_quest ((char *) &verbr_tag.me_einh,1,0);
    out_quest ((char *) &verbr_tag.mat_o_b,3,0);
    out_quest ((char *) &verbr_tag.hk_vollk,3,0);
    out_quest ((char *) &verbr_tag.hk_teilk,3,0);
    out_quest ((char *) &verbr_tag.typ,1,0);
    out_quest ((char *) &verbr_tag.prdk_stufe,1,0);
    out_quest ((char *) &verbr_tag.artikel,3,0);
    out_quest ((char *) verbr_tag.bearb_info,0,81);
    out_quest ((char *) &verbr_tag.prodabt,1,0);
            cursor = prepare_sql ("select verbr_tag.mdn,  "
"verbr_tag.dat,  verbr_tag.a,  verbr_tag.me_ist,  verbr_tag.anz_einh,  "
"verbr_tag.zerl_me,  verbr_tag.me_einh,  verbr_tag.mat_o_b,  "
"verbr_tag.hk_vollk,  verbr_tag.hk_teilk,  verbr_tag.typ,  "
"verbr_tag.prdk_stufe,  verbr_tag.artikel,  verbr_tag.bearb_info, "
"verbr_tag.prodabt from verbr_tag "

#line 25 "verbr_tag.rpp"
                                  "where mdn = ? "
                                  "and   dat = ? "
                                  "and artikel = ? "
                                  "and   a = ?  and (bearb_info = ? or bearb_info is null) and prdk_stufe = ? ");

    ins_quest ((char *) &verbr_tag.mdn,1,0);
    ins_quest ((char *) &verbr_tag.dat,2,0);
    ins_quest ((char *) &verbr_tag.a,3,0);
    ins_quest ((char *) &verbr_tag.me_ist,3,0);
    ins_quest ((char *) &verbr_tag.anz_einh,3,0);
    ins_quest ((char *) &verbr_tag.zerl_me,3,0);
    ins_quest ((char *) &verbr_tag.me_einh,1,0);
    ins_quest ((char *) &verbr_tag.mat_o_b,3,0);
    ins_quest ((char *) &verbr_tag.hk_vollk,3,0);
    ins_quest ((char *) &verbr_tag.hk_teilk,3,0);
    ins_quest ((char *) &verbr_tag.typ,1,0);
    ins_quest ((char *) &verbr_tag.prdk_stufe,1,0);
    ins_quest ((char *) &verbr_tag.artikel,3,0);
    ins_quest ((char *) verbr_tag.bearb_info,0,81);
    ins_quest ((char *) &verbr_tag.prodabt,1,0);
            sqltext = "update verbr_tag set "
"verbr_tag.mdn = ?,  verbr_tag.dat = ?,  verbr_tag.a = ?,  "
"verbr_tag.me_ist = ?,  verbr_tag.anz_einh = ?,  "
"verbr_tag.zerl_me = ?,  verbr_tag.me_einh = ?,  "
"verbr_tag.mat_o_b = ?,  verbr_tag.hk_vollk = ?,  "
"verbr_tag.hk_teilk = ?,  verbr_tag.typ = ?,  "
"verbr_tag.prdk_stufe = ?,  verbr_tag.artikel = ?,  "
"verbr_tag.bearb_info = ?, verbr_tag.prodabt = ? "

#line 31 "verbr_tag.rpp"
                                  "where mdn = ? "
                                  "and   dat = ? "
                                  "and artikel = ? "
                                  "and   a = ?  and (bearb_info = ? or bearb_info is null) and prdk_stufe = ? ";
            ins_quest ((char *)   &verbr_tag.mdn, 1, 0);
            ins_quest ((char *)   &verbr_tag.dat,  2, 0);
            ins_quest ((char *)   &verbr_tag.artikel,  3, 0);
            ins_quest ((char *)   &verbr_tag.a,  3, 0);
            ins_quest ((char *)   &verbr_tag.bearb_info,   0, 81);
            ins_quest ((char *)   &verbr_tag.prdk_stufe, 1, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &verbr_tag.mdn, 1, 0);
            ins_quest ((char *)   &verbr_tag.dat,  2, 0);
            ins_quest ((char *)   &verbr_tag.artikel,  3, 0);
            ins_quest ((char *)   &verbr_tag.a,  3, 0);
            ins_quest ((char *)   &verbr_tag.prdk_stufe, 1, 0);
            ins_quest ((char *)   &verbr_tag.bearb_info,   0, 81);
            test_upd_cursor = prepare_sql ("select dat from verbr_tag "
                                  "where mdn = ? "
                                  "and   dat = ? "
                                  "and artikel = ? "
                                  "and   a = ? and prdk_stufe = ? and (bearb_info = ? or bearb_info is null)"
                                  "for update");

            ins_quest ((char *) &verbr_tag.mdn, 1, 0);
            ins_quest ((char *)   &verbr_tag.dat,  2, 0);
            del_cursor = prepare_sql ("delete from verbr_tag "
                                  "where mdn = ? "
                                  "and   dat = ? ");

    ins_quest ((char *) &verbr_tag.mdn,1,0);
    ins_quest ((char *) &verbr_tag.dat,2,0);
    ins_quest ((char *) &verbr_tag.a,3,0);
    ins_quest ((char *) &verbr_tag.me_ist,3,0);
    ins_quest ((char *) &verbr_tag.anz_einh,3,0);
    ins_quest ((char *) &verbr_tag.zerl_me,3,0);
    ins_quest ((char *) &verbr_tag.me_einh,1,0);
    ins_quest ((char *) &verbr_tag.mat_o_b,3,0);
    ins_quest ((char *) &verbr_tag.hk_vollk,3,0);
    ins_quest ((char *) &verbr_tag.hk_teilk,3,0);
    ins_quest ((char *) &verbr_tag.typ,1,0);
    ins_quest ((char *) &verbr_tag.prdk_stufe,1,0);
    ins_quest ((char *) &verbr_tag.artikel,3,0);
    ins_quest ((char *) verbr_tag.bearb_info,0,81);
    ins_quest ((char *) &verbr_tag.prodabt,1,0);
            ins_cursor = prepare_sql ("insert into verbr_tag ("
"mdn,  dat,  a,  me_ist,  anz_einh,  zerl_me,  me_einh,  mat_o_b,  hk_vollk,  hk_teilk,  "
"typ,  prdk_stufe,  artikel,  bearb_info, prodabt) "

#line 63 "verbr_tag.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?)");

#line 65 "verbr_tag.rpp"
}
