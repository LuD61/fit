#ifndef _MASCHINEN_DEF
#define _MASCHINEN_DEF

#include "windows.h"
#include "mo_draw.h"
#include "wmask.h"
#include "dbclass.h"

struct MASCHINEN {
   long      masch_nr;
   short     prod_abt;
   char      masch_bz[25];
   short     masch_typ;
   long      masch_kap;
   short     prod_abt2;
   short     prod_abt3;
   short     prod_abt4;
   char      pers1[13];
   char      pers2[13];
   char      pers3[13];
   char      pers4[13];
};
extern struct MASCHINEN maschinen, maschinen_null;

#line 10 "maschinen.rh"

class MASCHINEN_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               MASCHINEN_CLASS () : DB_CLASS ()
               {
               }
};
#endif
