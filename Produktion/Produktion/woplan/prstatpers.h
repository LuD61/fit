#ifndef _PRSTATPERS_DEF
#define _PRSTATPERS_DEF

#include "windows.h"
#include "mo_draw.h"
#include "wmask.h"
#include "dbclass.h"

struct PRSTATPERS {
   short     mdn;
   long      dat;
   long      masch_nr;
   short     prodabt;
   char      pers[13];
   long      zeit_min;
   short     status;
   double    menge;
};
extern struct PRSTATPERS prstatpers, prstatpers_null;

#line 10 "prstatpers.rh"

class PRSTATPERS_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               PRSTATPERS_CLASS () : DB_CLASS ()
               {
               }
};
#endif
