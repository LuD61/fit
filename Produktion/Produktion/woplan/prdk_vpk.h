#ifndef _PRDK_VPK_DEF
#define _PRDK_VPK_DEF

#include "windows.h"
#include "mo_draw.h"
#include "wmask.h"
#include "dbclass.h"

struct PRDK_VPK {
   short     mdn;
   double    a;
   double    a_vpk;
   short     bearb_flg;
   char      bearb_info[49];
   double    me;
   short     me_einh;
   double    fuel_gew;
   double    gew;
   short     variante;
   short     typ;
   char      me_buch[2];
};
extern struct PRDK_VPK prdk_vpk, prdk_vpk_null;

#line 10 "prdk_vpk.rh"

class PRDK_VPK_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               PRDK_VPK_CLASS () : DB_CLASS ()
               {
               }
};
#endif
