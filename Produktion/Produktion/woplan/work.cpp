//010611 chg_gew -> varb_gew , wenn Grundlage f�r Chargengr��e nur die Menge der Verarbeitungsmaterialien sein soll
//100905 me_old 
//040705
//010705
//280405 Problematik Grundbr�te -> Zusatz charge_ok = 9
#include <windows.h>
#include <stdio.h>
#include "Work.h"
#include "strfkt.h"
#include "mo_menu.h"
#include "mo_wdebug.h"
#include "infmacro.h"
#include "ptab.h"
#include "auto_nr.h"
#include "mo_auto.h"
#include "maschinen.h"
#include "prdk_k.h"
#include "text.h"

//#include "mo_wdebug.h"

PRDK_K_CLASS Prdk_k; //010705
extern char LONGNULL[];
double varb_artikel;
double me_ist = double(0);
short me_einh = 0;
short Work::AnzPers = 4;
short Work::BeginPersCol = 0;
short dprodabt1 = 1;
short dprodabt2 = 1;
short dprodabt3 = 1;
short dprodabt4 = 1;

#define MAXTRIES 100

long Work::LongNull = 0x80000000;

short KW;
short JR;
int Work::LiefTagevon = 7;
int Work::LiefTagebis = 0;
int Work::LiefMeHolen = 0;
char Work::datvon [12];
char Work::datbis [12];
char *Work::OrderBy = "a_bas.a             ";
int Work::MatrixTyp = BIG;
int Work::ArtScroll = FALSE;
char *Work::ChargeMacro = "Auto";
char *Work::ProdAbt = "0";
char *Work::ChgOhneHuell = "0";
char *Work::ChargengroesseVarb = "0";
int Work::Datumsperre [] = {FALSE,
                                  FALSE,
                                  FALSE,
                                  FALSE,
                                  FALSE,
                                  FALSE,
                                  FALSE,
                                  FALSE,
                                  FALSE,
                                   FALSE,
                                   FALSE,
                                   FALSE,
        };

void Work::ExitError (BOOL b)
{
    if (b)
    {
        disp_mess ("Speicher kann nicht zugeornet werden", 2);
        ExitProcess (1);
    }
} 

void Work::TestNullDat (long *dat)
{
    if (*dat == 0l)
    {
        memcpy (dat, (long *) LONGNULL, sizeof (long));
    }
}


void Work::SetChargeMacro (char *cfg_v)
{
     if (strcmp (cfg_v, "NULL"))
     {
         ChargeMacro = new char [strlen (cfg_v) + 1];
         if (ChargeMacro != NULL)
         {
                strcpy (ChargeMacro, cfg_v);
         }
     }
     else
     {
         ChargeMacro = NULL;
     }
}

void Work::SetKwJr (short kw, short jr)
{
	KW = kw;
	JR = jr;
}

void Work::SetProdAbt (char *cfg_v)
{
     if (strcmp (cfg_v, "NULL"))
     {
         ProdAbt = new char [strlen (cfg_v) + 1];
         if (ProdAbt != NULL)
         {
                strcpy (ProdAbt, cfg_v);
         }
     }
     else
     {
         ProdAbt = NULL;
     }
}

char* Work::GetProdAbt (void)
{
	return ProdAbt;
}

void Work::SetOrderBy (char *cfg_v)
{
     if (strcmp (cfg_v, "NULL"))
     {
         OrderBy = new char [strlen (cfg_v) + 1];
         if (OrderBy != NULL)
         {
                strcpy (OrderBy, cfg_v);
         }
     }
     else
     {
         OrderBy = NULL;
     }
}

void Work::SetChgOhneHuell (char *cfg_v)
{
     if (strcmp (cfg_v, "NULL"))
     {
         ChgOhneHuell = new char [strlen (cfg_v) + 1];
         if (ChgOhneHuell != NULL)
         {
                strcpy (ChgOhneHuell, cfg_v);
         }
     }
     else
     {
         ChgOhneHuell = NULL;
     }
}

void Work::SetChargengroesseVarb (char *cfg_v)
{
     if (strcmp (cfg_v, "NULL"))
     {
         ChargengroesseVarb = new char [strlen (cfg_v) + 1];
         if (ChargengroesseVarb != NULL)
         {
                strcpy (ChargengroesseVarb, cfg_v);
         }
     }
     else
     {
         ChargengroesseVarb = NULL;
     }
}

double Work::HoleLiefMe (double Artikel)
{
	char datum [12];
	char datvon [12];
	char datbis [12];
	double Menge = 0.0;
    sysdate (datum);
    int ldatum = dasc_to_long (datum) ;
	if (LiefMeHolen == 0) return 0; 

	strcpy (datvon,Work.datvon);
	strcpy (datbis,Work.datbis);
//	dlong_to_asc (ldatum - LiefTagevon, datvon);
//	dlong_to_asc (ldatum - LiefTagebis, datbis);

    DbClass.sqlin  ((short *) &prstatatag.mdn, 1, 0);
    DbClass.sqlin  ((char *)  datvon, 0, 11);
    DbClass.sqlin  ((char *)  datbis, 0, 11);
    DbClass.sqlin  ((double *) &Artikel, 3, 0);
    DbClass.sqlout  ((double *) &Menge, 3, 0);
    int dsqlstatus = DbClass.sqlcomm ("select me_vk_tag from a_tag_wa "
                                      "where mdn = ? "
									  "and fil = 0 "
                                      "and dat >= ? "
                                      "and dat <= ? "
                                      "and a = ? " );

	if (dsqlstatus == 100) Menge = 0.0;
	if (dsqlstatus < 0) Menge = double (dsqlstatus);
	return Menge;
}

void Work::FillRow (long StartDay, int fieldanz)
{
    char buffer [15];
    long day;
    int i;

    if (Lsts == NULL) return;

    prstatatag.a   = ratod (Lsts[0]->GetFeld ());
    CFELD::SetFeldWrt (Lsts, "a_bz1", clipped(_a_bas.a_bz1));
    sprintf (buffer, "%.3lf",  HoleLiefMe(prstatatag.a));
    CFELD::SetFeldWrt (Lsts, "mengen", buffer);
    if (MatrixTyp == BIG)
    {
        i = 1;
    }
    else if (MatrixTyp == SMALL)
    {
        i = 3; 
		if (LiefMeHolen == 0) i = 2; //300709
    }
	int flg_me_old = 0;
//    for (day = StartDay; i < fieldanz - 1; i ++, day ++)
    for (day = StartDay; i < fieldanz ; i ++, day ++)   //testtest
    {
           prstatatag.dat = day;
           prstatatag.me  = 0.0;
           prstatatag.planstatus  = 0;
		   prstatatag.prodabt = abt;
           int dsqlstatus = PrStatatag.dbreadfirst ();
           if (dsqlstatus == 0)
           {
                 if (ListMeMode == MENGE)
                 {
                      sprintf (buffer, "%.3lf", prstatatag.me);
                 }
                 if (ListMeMode == AUFTRAEGE)
                 {
                      sprintf (buffer, "%.3lf", prstatatag.auf_me);
                 }
                 else if (ListMeMode == CHARGEN)
                 {
                      sprintf (buffer, "%.3lf", prstatatag.me / prdk_k.kutter_gew); //250504
                 }

                 *Lsts[i] = (char *) buffer;
				 flg_me_old = 1;
		   }
           else
           {
                 *Lsts[i] = " ";
           }

    }
    CFELD::SetFeldWrt (Lsts, "rez", prdk_k.rez);
    sprintf (buffer, "%hd", prdk_k.variante);
    CFELD::SetFeldWrt (Lsts, "variante", buffer);
    sprintf (buffer, "%.3lf", prdk_k.kutter_gew); //250504
    CFELD::SetFeldWrt (Lsts, "chg_gew", buffer);
    sprintf (buffer, "%.4lf", prdk_k.nvpk_mat_o_b);
    CFELD::SetFeldWrt (Lsts, "mat_o_b", buffer);
    sprintf (buffer, "%.4lf", prdk_k.a_hk_vollk);
    CFELD::SetFeldWrt (Lsts, "hk_vollk", buffer);
    sprintf (buffer, "%.4lf", prdk_k.a_hk_teilk);
    CFELD::SetFeldWrt (Lsts, "hk_teilk", buffer);
    sprintf (buffer, "%.hd",  prstatatag.planstatus);
    CFELD::SetFeldWrt (Lsts, "planstatus", buffer);
    sprintf (buffer, "%.hd",  prstatatag.posi); //020905
    CFELD::SetFeldWrt (Lsts, "posi", buffer);
//    sprintf (buffer, "%.hd",  flg_me_old); //100905
//    CFELD::SetFeldWrt (Lsts, "flg_me_old", buffer);
}


BOOL Work::GetPlanStatus (double a,short posi, char *date)
{
    short planstatus = 0;

    DbClass.sqlin  ((short *) &prstatatag.mdn, 1, 0);
    DbClass.sqlin  ((char *)  date, 0, 11);
    DbClass.sqlin  ((double *) &a, 3, 0);
    DbClass.sqlin  ((short *) &abt, 1, 0);
    DbClass.sqlin  ((short *) &posi, 1, 0);
    DbClass.sqlout ((short *) &planstatus, 1, 0);
    int dsqlstatus = DbClass.sqlcomm ("select planstatus from prstatatag "
                                      "where mdn = ? "
                                      "and dat = ? "
                                      "and a = ? " 
									  "and prodabt = ? "
									  "and posi = ?");
    if (planstatus == 2)
    {
        return TRUE;
    }
    return FALSE;
}

void Work::InitRow (void)
{
    if (Lsts == NULL) return;
}

int Work::GetPtab (char *dest, char *item, char *wert)
{
    int dsqlstatus;

    dest[0] = 0;
    dsqlstatus = Ptab.lese_ptab (item, wert);  
    if (dsqlstatus == 100)
    {
        return -1;
    }
    strcpy (dest, ptabn.ptbezk);
    return 0;
}

int Work::ShowPtab (char *dest, char *item)
{
    int dsqlstatus;

    dsqlstatus = Ptab.Show (item);  
    if (syskey == KEY5)
    {
        return -1;
    }
    strcpy (dest, ptabn.ptwert);
    return 0;
}

int Work::ShowPtabEx (HWND hMainWindow, char *dest, char *item)
{
       char wert [80];
       SEARCHPTAB *SearchPtab;
	   HINSTANCE hMainInst;


       hMainInst = (HINSTANCE) GetWindowLong (hMainWindow, GWL_HINSTANCE);
       SearchPtab = new SEARCHPTAB (item);
       SearchPtab->SetParams (hMainInst, hMainWindow); 
       SearchPtab->Setawin (hMainWindow);
                                                  
       SearchPtab->Search ();
       if (SearchPtab->GetKey (wert) == TRUE)
       {
           delete SearchPtab;
           strcpy (dest, wert);
		   return 0;
       }
       delete SearchPtab;
       return -1;
}


int Work::GetMdnName (char *dest, short mdn)
{
    int dsqlstatus;

    if (mdn == 0)
    {
        strcpy (dest, "Zentrale");
        return 0;
    }
    dest[0] = 0;
    DbClass.sqlin ((short *) &mdn, 1, 0);
    DbClass.sqlout ((char *) dest, 0, 17);
    dsqlstatus =  DbClass.sqlcomm ("select adr.adr_krz from mdn,adr "
                                   "where mdn.mdn = ? " 
                                   "and adr.adr = mdn.adr");
    if (dsqlstatus == 100)
    {
        return 100;
    }
    return Mdn.lese_mdn (mdn);
}


int Work::GetFilName (char *dest, short mdn, short fil)
{

    if (mdn == 0 || fil == 0)
    {
        strcpy (dest, "Mandant");
        return 0;
    }
    dest[0] = 0;
    DbClass.sqlin ((short *) &mdn, 1, 0);
    DbClass.sqlin ((short *) &fil, 1, 0);
    DbClass.sqlout ((char *) dest, 0, 17);
    return DbClass.sqlcomm ("select adr.adr_krz from fil,adr "
                             "where fil.mdn = ? " 
                             "and   fil.fil = ? " 
                             "and adr.adr = fil.adr");
}

int Work::GetAbtBez (char *dest, short abt)
{
/*
    int dsqlstatus;

    if (abt == 0) return 0;
    dest[0] = 0;
    DbClass.sqlin ((short *) &abt, 1, 0);
    DbClass.sqlout ((char *) dest, 0, 17);
    dsqlstatus =  DbClass.sqlcomm ("select abt_bz1 from abt "
                                   "where abt = ?"); 
    if (dsqlstatus == 100)
    {
        return 100;
    }
*/
    char wert [5];

    sprintf (wert, "%hd", abt);
    if (GetPtab (dest, "prod_abt", wert) == -1)
    {
        return 100;
    }
    return 0;
}

int Work::GetAbtNr (char *dest, char *Abtbez)
{
    DbClass.sqlin ((char *) Abtbez, 0, 9);
    DbClass.sqlout ((char *) dest, 0, 4);
    int dsqlstatus =  DbClass.sqlcomm ("select ptwert from ptabn "
                                   "where ptitem = \"prod_abt\" and ptbezk = ?"); 
    if (dsqlstatus == 100)
    {
		strcpy (dest,"-1");
        return 100;
    }
    return 0;
}


void Work::BeginWork (void)
{
    ::beginwork ();
}

void Work::CommitWork (void)
{
    ::commitwork ();
}

void Work::RollbackWork (void)
{
    ::rollbackwork ();
}

void Work::InitRec (void)
{
}



void Work::DeleteAllPos (void)
{
}


void Work::PrepareLst (void)
{
}

void Work::OpenLst (void)
{
	if (LstCursor == -1)
	{
		PrepareLst ();
		if (LstCursor == -1) return;
	}
}

int Work::FetchLst (void)
{
	if (LstCursor == -1)
	{
		PrepareLst ();
		if (LstCursor == -1) return 100;
	}
    return 0;
}

void Work::CloseLst (void)
{
	if (LstCursor != -1)
	{
		LstCursor = -1;
	}
}

long Work::GetAbtAnz (short abt)
{
    long anz;

    this->abt = abt;
    DbClass.sqlin ((short *) &abt, 1, 0);
    cursor = DbClass.sqlcursor ("select a from a_bas where abt = ? and a > 0 and delstatus = 0");
    anz = 0;
    while (DbClass.sqlfetch (cursor) == 0)
    {
           if (anz >= MAXA) break;
           anz ++;
    }
    DbClass.sqlclose (cursor);
    cursor = -1;
    return anz;
}


long Work::AbtFirst (short abt)
{
    DbClass.sqlin ((short *) &abt, 1, 0);
    DbClass.sqlout ((double *) &_a_bas.a, 3, 0);
    cursor = DbClass.sqlcursor ("select a from a_bas where abt = ? " 
                                "and a > 0 "
                                "and delstatus = 0 "
                                "order by a");
    int dsqlstatus = DbClass.sqlfetch (cursor);
    if (dsqlstatus == 0)
    {
        dsqlstatus = lese_a_bas (_a_bas.a);
    }
    return dsqlstatus;
}

long Work::AbtNext (void)
{

    if (cursor == -1) 
    {
        return 100;
    }
    int dsqlstatus = DbClass.sqlfetch (cursor);
    if (dsqlstatus == 0)
    {
        dsqlstatus = lese_a_bas (_a_bas.a);
    }
    else
    {
        DbClass.sqlclose (cursor);
        cursor = -1;
    }
    return dsqlstatus;
}


BOOL Work::TestListAnz (void)
{
     int dsqlstatus = Fetch ();
     Close ();
     if (dsqlstatus != 0)
     {
         return FALSE;
     }
     return TRUE;
}

int Work::Prepare (void)
{

            char sqltext [800];
     DbClass.sqlin ((short *)   &prstatatag.mdn, 1, 0);
     DbClass.sqlin ((short *)   &abt, 1, 0);
     DbClass.sqlin ((long *)   &DatVon, 2, 0);
     DbClass.sqlin ((long *)   &DatBis, 2, 0);

     DbClass.sqlin ((short *)   &prstatatag.mdn, 1, 0);
     DbClass.sqlin ((short *)   &JR, 1, 0);
     DbClass.sqlin ((short *)   &KW, 1, 0);

     DbClass.sqlout ((long *)  &prstatatag.posi_long, 2, 0);
     DbClass.sqlout ((short *)   &_a_bas.hwg, 1, 0);
     DbClass.sqlout ((short *)   &_a_bas.wg, 1, 0);
     DbClass.sqlout ((long *)   &_a_bas.ag, 2, 0);
     DbClass.sqlout ((double *) &_a_bas.a, 3, 0);
     DbClass.sqlout ((double *) &prstatatag.a, 3, 0);
     DbClass.sqlout ((char *)   _a_bas.a_bz1, 0, 25);
     DbClass.sqlout ((char *)    prdk_k.rez, 0, 9);
     DbClass.sqlout ((short *)   &prdk_k.variante, 1, 0);
     DbClass.sqlout ((double *)  &prdk_k.kutter_gew, 3, 0);
     DbClass.sqlout ((double *)  &prdk_k.nvpk_mat_o_b, 3, 0);
     DbClass.sqlout ((double *)  &prdk_k.a_hk_vollk, 3, 0);
     DbClass.sqlout ((double *)  &prdk_k.a_hk_teilk, 3, 0);

//250504 nicht mehr chg_gew aus prdk_k , sondern kutter_gew
//020905 mit outer prstatatag um mehrere DS pro Artikel zu generieren 
	 if (abt == 0 && stnd > 0)  //alle a_bas.abt Suche �ber Standardplan
	 {
	     DbClass.sqlin ((short *)   &stnd, 1, 0);

			strcpy (sqltext, "select unique(prstatatag.posi), a_bas.hwg,a_bas.wg, a_bas.ag, a_bas.a, a_bas.a, a_bas.a_bz1, ");
			strcat (sqltext, "prdk_k.rez, prdk_k.variante, ");
                                 strcat (sqltext, "prdk_k.kutter_gew, ");
                                 strcat (sqltext, "prdk_k.nvpk_mat_o_b, prdk_k.a_hk_vollk, ");
                                 strcat (sqltext, "prdk_k.a_hk_teilk ");
                                 strcat (sqltext, "from prdk_k, a_bas,plpos, outer prstatatag ");
                                 strcat (sqltext, "where prdk_k.mdn = ? ");
                                 strcat (sqltext, "and (a_bas.abt = ? or a_bas.abt >= 0) ");
                                 strcat (sqltext, "and prdk_k.a = a_bas.a ");
                                 strcat (sqltext, "and prdk_k.akv = 1 ");
								 strcat (sqltext, "and prstatatag.mdn = prdk_k.mdn ");
								 strcat (sqltext, "and prstatatag.a = a_bas.a ");
								 strcat (sqltext, "and a_bas.a = plpos.a ");
								 strcat (sqltext, "and plpos.mdn = prdk_k.mdn ");
								 strcat (sqltext, "and prstatatag.dat >= ? ");
								 strcat (sqltext, "and prstatatag.dat <= ? ");

								 strcat (sqltext, "and prdk_k.a  not in (select a from woplan_del where mdn = ? ");
								 strcat (sqltext, "and jr = ? and kw = ?) and plpos.plan = ? ");

                                 strcat (sqltext, "order by ");
                                 strcat (sqltext, OrderBy);
	 }
	 else if (abt == 0 && stnd == 0 && strupcmp (ProdAbt, "Standardplan", strlen (ProdAbt)) == 0)  //alle Standardpl�ne
	 {
			strcpy (sqltext, "select unique(prstatatag.posi), a_bas.hwg,a_bas.wg, a_bas.ag, a_bas.a, a_bas.a, a_bas.a_bz1, ");
			strcat (sqltext, "prdk_k.rez, prdk_k.variante, ");
                                 strcat (sqltext, "prdk_k.kutter_gew, ");
                                 strcat (sqltext, "prdk_k.nvpk_mat_o_b, prdk_k.a_hk_vollk, ");
                                 strcat (sqltext, "prdk_k.a_hk_teilk ");
                                 strcat (sqltext, "from prdk_k, a_bas,plpos, outer prstatatag ");
                                 strcat (sqltext, "where prdk_k.mdn = ? ");
                                 strcat (sqltext, "and (a_bas.abt = ? or a_bas.abt >= 0) ");
                                 strcat (sqltext, "and prdk_k.a = a_bas.a ");
                                 strcat (sqltext, "and prdk_k.akv = 1 ");
								 strcat (sqltext, "and prstatatag.mdn = prdk_k.mdn ");
								 strcat (sqltext, "and prstatatag.a = a_bas.a ");
								 strcat (sqltext, "and a_bas.a = plpos.a ");
								 strcat (sqltext, "and plpos.mdn = prdk_k.mdn ");
								 strcat (sqltext, "and prstatatag.dat >= ? ");
								 strcat (sqltext, "and prstatatag.dat <= ? ");

								 strcat (sqltext, "and prdk_k.a  not in (select a from woplan_del where mdn = ? ");
								 strcat (sqltext, "and jr = ? and kw = ?)  ");

                                 strcat (sqltext, "order by ");
                                 strcat (sqltext, OrderBy);
	 }

	 else if (abt == 0 )  //alle a_bas.abt
	 {
		strcpy (sqltext, "select unique(prstatatag.posi), a_bas.hwg,a_bas.wg, a_bas.ag, a_bas.a, a_bas.a, a_bas.a_bz1, ");
			strcat (sqltext, "prdk_k.rez, prdk_k.variante, ");
                                 strcat (sqltext, "prdk_k.kutter_gew, ");
                                 strcat (sqltext, "prdk_k.nvpk_mat_o_b, prdk_k.a_hk_vollk, ");
                                 strcat (sqltext, "prdk_k.a_hk_teilk ");
                                 strcat (sqltext, "from prdk_k, a_bas, outer prstatatag ");
                                 strcat (sqltext, "where prdk_k.mdn = ? ");
                                 strcat (sqltext, "and (a_bas.abt = ? or a_bas.abt >= 0) ");
                                 strcat (sqltext, "and prdk_k.a = a_bas.a ");
                                 strcat (sqltext, "and prdk_k.akv = 1 ");
								 strcat (sqltext, "and prstatatag.mdn = prdk_k.mdn ");
								 strcat (sqltext, "and prstatatag.a = a_bas.a ");
								 strcat (sqltext, "and prstatatag.dat >= ? ");
								 strcat (sqltext, "and prstatatag.dat <= ? ");

								 strcat (sqltext, "and prdk_k.a  not in (select a from woplan_del where mdn = ? ");
								 strcat (sqltext, "and jr = ? and kw = ?) ");

                                 strcat (sqltext, "order by ");
                                 strcat (sqltext, OrderBy);
	 }
	 else
	 {
		if (strupcmp (ProdAbt, "a_bas", strlen (ProdAbt)) == 0)
		{ 
			strcpy (sqltext, "select unique(prstatatag.posi),  a_bas.hwg,a_bas.wg, a_bas.ag, a_bas.a, a_bas.a, a_bas.a_bz1, ");
                                 strcat (sqltext, "prdk_k.rez, prdk_k.variante, ");
                                 strcat (sqltext, "prdk_k.kutter_gew, ");
                                 strcat (sqltext, "prdk_k.nvpk_mat_o_b, prdk_k.a_hk_vollk, ");
                                 strcat (sqltext, "prdk_k.a_hk_teilk ");
                                 strcat (sqltext, "from prdk_k, a_bas, outer prstatatag ");
                                 strcat (sqltext, "where prdk_k.mdn = ? ");
                                 strcat (sqltext, "and a_bas.abt = ? ");
                                 strcat (sqltext, "and prdk_k.a = a_bas.a ");
                                 strcat (sqltext, "and prdk_k.akv = 1 ");
								 strcat (sqltext, "and prstatatag.mdn = prdk_k.mdn ");
								 strcat (sqltext, "and prstatatag.a = a_bas.a ");
								 strcat (sqltext, "and prstatatag.dat >= ? ");
								 strcat (sqltext, "and prstatatag.dat <= ? ");
                                 strcat (sqltext, "order by ");
                                 strcat (sqltext, OrderBy);
		}
		else if (strupcmp (ProdAbt, "prdk_k", strlen (ProdAbt)) == 0)  //200210  jetzt auch �ber prdk_k.prod_abt
		{ 
			strcpy (sqltext, "select unique(prstatatag.posi),  a_bas.hwg,a_bas.wg, a_bas.ag, a_bas.a, a_bas.a, a_bas.a_bz1, ");
                                 strcat (sqltext, "prdk_k.rez, prdk_k.variante, ");
                                 strcat (sqltext, "prdk_k.kutter_gew, ");
                                 strcat (sqltext, "prdk_k.nvpk_mat_o_b, prdk_k.a_hk_vollk, ");
                                 strcat (sqltext, "prdk_k.a_hk_teilk ");
                                 strcat (sqltext, "from prdk_k, a_bas, outer prstatatag ");
                                 strcat (sqltext, "where prdk_k.mdn = ? ");
                                 strcat (sqltext, "and prdk_k.prod_abt = ? ");
                                 strcat (sqltext, "and prdk_k.a = a_bas.a ");
                                 strcat (sqltext, "and prdk_k.akv = 1 ");
								 strcat (sqltext, "and prstatatag.mdn = prdk_k.mdn ");
								 strcat (sqltext, "and prstatatag.a = a_bas.a ");
								 strcat (sqltext, "and prstatatag.dat >= ? ");
								 strcat (sqltext, "and prstatatag.dat <= ? ");
                                 strcat (sqltext, "order by ");
                                 strcat (sqltext, OrderBy);
		}

		else 
		{ 
			strcpy (sqltext, "select unique(prstatatag.posi), a_bas.hwg,a_bas.wg,  a_bas.ag, a_bas.a, a_bas.a, a_bas.a_bz1, ");
                                 strcat (sqltext, "prdk_k.rez, prdk_k.variante, ");
                                 strcat (sqltext, "prdk_k.kutter_gew, ");
                                 strcat (sqltext, "prdk_k.nvpk_mat_o_b, prdk_k.a_hk_vollk, ");
                                 strcat (sqltext, "prdk_k.a_hk_teilk, ");
                                 strcat (sqltext, "a_preis.druckfolge ");
                                 strcat (sqltext, "from prdk_k, a_bas, a_preis, outer prstatatag ");
                                 strcat (sqltext, "where prdk_k.mdn = ? ");
                                 strcat (sqltext, "and a_preis.prodabt = ? ");
                                 strcat (sqltext, "and prdk_k.a = a_bas.a ");
                                 strcat (sqltext, "and a_preis.mdn = prdk_k.mdn ");
                                 strcat (sqltext, "and a_preis.a = prdk_k.a ");
								 strcat (sqltext, "and a_preis.fil = 0 ");
                                 strcat (sqltext, "and prdk_k.akv = 1 ");
								 strcat (sqltext, "and prstatatag.mdn = prdk_k.mdn ");
								 strcat (sqltext, "and prstatatag.a = a_bas.a ");
								 strcat (sqltext, "and prstatatag.dat >= ? ");
								 strcat (sqltext, "and prstatatag.dat <= ? ");
                                 strcat (sqltext, "order by a_bas.ag,a_preis.druckfolge, a_bas.a");
		}
	 }

	cursor = DbClass.sqlcursor (sqltext);

     return cursor;
}
     

void Work::Open (void)
{
	if (cursor == -1)
	{
		Prepare ();
		if (cursor == -1) return;
	}
    DbClass.sqlopen (cursor);
}

int Work::Fetch (void)
{
	if (cursor == -1)
	{
		Prepare ();
		if (cursor == -1) return 100;
	}
    short mdn = prstatatag.mdn;
    memcpy (&prstatatag, &prstatatag_null, sizeof (prstatatag));
    prstatatag.mdn = mdn;
    prstatatag.prodabt = abt;
    int dsqlstatus = DbClass.sqlfetch (cursor);
	if (prstatatag.posi < -10 || prstatatag.posi > 10) 
	{
		prstatatag.posi = 0; //020905 nicht definiert, wenn kein Satz gef. in prstatatag
	}
	else
	{
		prstatatag.posi = (short) prstatatag.posi_long;
	}
	/** 020905 Wozu dies ??
    if (dsqlstatus == 0)
    {
        prstatatag.a   = _a_bas.a;
        prstatatag.dat = AktDat;
        if (PrStatatag.dbreadfirst () != 0)
        {
            short mdn = prstatatag.mdn;
            memcpy (&prstatatag, &prstatatag_null, sizeof (prstatatag));
            prstatatag.mdn = mdn;
            prstatatag.a   = _a_bas.a;
            prstatatag.dat = AktDat;
        }
    }
	*******/
	return dsqlstatus;
}

int Work::FetchPlanGew ()
{
     return 100;
}

void Work::Close (void)
{

	if (cursor != -1)
	{
        DbClass.sqlclose (cursor);
//        DbClass.sqlclose (cursor_lief);
//        DbClass.sqlclose (cursor_a);
		cursor = -1;
	}
}

int Work::WriteRec (form *UbForm, int Feldanz, long StartDay,short dzeil)
{
    long day;
    int i;
	int flg_me_old;

    if (Lsts == NULL) 
    {
        return 0;
    }


    short mdn = prstatatag.mdn;
    if (MatrixTyp == BIG)
    {
        i = 1;
    }
    else if (MatrixTyp == SMALL)
    {
        i = 3;  
		if (LiefMeHolen == 0) i = 2; //300709
    }
//    PrStatchgk = new PRSTATCHGK_CLASS ();
	if (dzeil > 9) 
	{
		int y = 1;
		y = 2;
    }
	double me = 0.0;
    for (day = StartDay; i < Feldanz; i ++, day ++)
    {
		   

		   if (day != ldat1 && day != ldat2 && day != ldat3 && day != ldat4 
			   && day != ldat5 && day != ldat6 && day != ldat7) continue;

	       flg_me_old = atoi (CFELD::GetFeldWrt (Lsts, "flg_me_old")); //100905
           me = ratod (Lsts[i]->GetFeld ());
	       if (me == 0.0 && flg_me_old == 0) continue ; 

           memcpy (&prstatatag, &prstatatag_null, sizeof (prstatatag));
           prstatatag.mdn = mdn;
           prstatatag.a   = ratod (Lsts[0]->GetFeld ());
           prstatatag.posi = atoi (CFELD::GetFeldWrt (Lsts, "posi")); //020905
           prstatatag.dat = day;
		   prstatatag.prodabt = abt;
            prdk_k.chargierung = 1;
		    PrStatchgk->sqlin ((short *)  &prstatatag.mdn, 1, 0);
			PrStatchgk->sqlin ((double *) &prstatatag.a, 3, 0);
			PrStatchgk->sqlout ((short *) &prdk_k.chargierung, 1, 0);
			PrStatchgk->sqlout ((double *) &prdk_k.kutter_gew, 3, 0);
			int dsqlstatus = PrStatchgk->sqlcomm ("select chargierung,kutter_gew from prdk_k "
                                         "where mdn = ? " 
                                         "and a = ? "
                                         "and akv = 1");


		   if (dzeil == 0)
           {
//100905			   DeleteVerbrTag (mdn,day,abt);
			   DeletePrstatatagGbrt (mdn + 50,day,abt);
			   Delete0MengeVonPrstatatag (mdn,day,abt);
           }
//100905     if (me != 0.0)
//           {
				   prstatatag.prodabt = abt;
				   prstatatag.me = 0.0;
                   PrStatatag.dbreadfirst ();

				   double me_old = prstatatag.me;
// nix drin warum ???                  double chg_gew = ratod (CFELD::GetFeldWrt (Lsts, "chg_gew"));
                  double chg_gew = prdk_k.kutter_gew;
                   if (ListMeMode == MENGE)
                   {
                          prstatatag.me = me - me_old;
                   }
                   else if (ListMeMode == CHARGEN)
                   {  
                          prstatatag.me = (me - (me_old / chg_gew)) * chg_gew;
                   }
				   if (prstatatag.me > 0.001 || prstatatag.me < -0.001)
				   {
						prstatatag.chg  = chg_gew;
						strcpy (prstatatag.rez, CFELD::GetFeldWrt (Lsts, "rez"));
						prstatatag.hk_vollk = ratod (CFELD::GetFeldWrt (Lsts, "hk_vollk"));
						prstatatag.hk_teilk = ratod (CFELD::GetFeldWrt (Lsts, "hk_teilk"));
						prstatatag.mat_o_b  = ratod (CFELD::GetFeldWrt (Lsts, "mat_o_b"));
						short dvarb_ok  = prstatatag.varb_ok;
						short dzut_ok  = prstatatag.zut_ok;
						if (prdk_k.chargierung == 0)
						{
							prstatatag.varb_ok = 2;
							prstatatag.zut_ok = 2;
							prstatatag.huell_ok = 2;
						}
		
						if (atoi(ChgOhneHuell) == 1) prstatatag.huell_ok = 3; 
						if (PruefZut(mdn,prstatatag.rez) == 100)
						{
							prstatatag.zut_ok = 3;
						}
						prstatatag.vpk_ok = 3;
						PrStatatag.dbupdate ();
						prstatatag.varb_ok = dvarb_ok;
						prstatatag.zut_ok = dzut_ok;
// wird in enterchrg geschrieben              WritePrstatChgk ();
						WriteVerbrTag (mdn,day,prstatatag.a,prstatatag.posi,prstatatag.me,chg_gew,0);
				   }
//           }
    }
    delete PrStatchgk;
	CloseVarb ();
    return 0;
}


int Work::WritePrstatChgk (void)
{
    int charganz;
    double gew;

    if (prstatatag.chg == 0.0)
    {
        return -1;
    }

    charganz = (int) (double) (prstatatag.me / prstatatag.chg);
    prstatchgk.mdn = prstatatag.mdn;
    prstatchgk.fil = 0;
    prstatchgk.a   = prstatatag.a;
    prstatchgk.dat = prstatatag.dat;
    PrStatchgk->sqlin ((short *)  &prstatchgk.mdn, 1, 0);
    PrStatchgk->sqlin ((short *)  &prstatchgk.fil, 1, 0);
    PrStatchgk->sqlin ((double *) &prstatchgk.a, 3, 0);
    PrStatchgk->sqlin ((long *)   &prstatchgk.dat, 2, 0);
    int dsqlstatus = PrStatchgk->sqlcomm ("delete from prstatchgk "
                                         "where mdn = ? " 
                                         "and fil = ? "
                                         "and a = ? "
                                         "and dat = ?");
    gew = 0;
    strcpy (prstatchgk.rez, prstatatag.rez);
    for (int i = 0; i < charganz; i ++)
    {
        if (GenCharge (prstatchgk.chargennr) == NULL)
        {
            continue;
        }
        prstatchgk.gew = prstatatag.chg;
        PrStatchgk->dbupdate ();
        gew += prstatchgk.gew;
    }
    prstatchgk.gew = prstatatag.me - gew;
    if (prstatchgk.gew > 0.0)
    {
        if (GenCharge (prstatchgk.chargennr) == NULL)
        {
            return -1;
        }
        PrStatchgk->dbupdate ();
    }
    return 0;
}


char *Work::AutoCharge (char *charge)
{
        AUTO_CLASS AutoNr;

        int dsqlstatus = AutoNr.nvholid (prstatchgk.mdn, prstatchgk.fil, "charge"); 
        if (dsqlstatus == 0)
        {
            sprintf (charge, "%ld", auto_nr.nr_nr);
            return charge;
        }
        return NULL;
}

char *Work::GenCharge (char *charge)
{
        static char Charge [21];
        static InfMacro *infMacro = NULL;
        static int lfd = 0;
        char buffer [1024];
        char dat [12];

        dlong_to_asc (prstatchgk.dat, dat);
        strcpy (Charge, "  ");
        SetDebug ();
        SetDebugMain (DLG::AppWindow);
        if (infMacro == NULL)
        {
               infMacro = new InfMacro ("genrezcharge.mco");
               if (infMacro == NULL) return NULL;
        }

        if (infMacro->LoadMacro () == FALSE)
        {
                return AutoCharge (charge);
        }

        strcpy (buffer, "");
        {
            sprintf (buffer, "a=%.0lf;dat=%s", prstatchgk.a, dat);
        }
        if (ChargeMacro == NULL)
        {
                infMacro->RunMacro (buffer, "charge", Charge);
        }
        else if (strupcmp (ChargeMacro, "Auto", strlen (ChargeMacro)) == 0)
        {
                return AutoCharge (charge);
        }
        else
        {
                infMacro->RunMacro (buffer, "charge", Charge, ChargeMacro);
        }
        strncpy (charge, Charge, 13);
        charge[13] = 0;
        return charge;
}


int Work::WriteRecLief (form *UbForm, int Feldanz)
{
    LIEF_BZG_CLASS LiefBzg;
    double pr_ek;
    char ek_old [20];
    char ek_new [20];
    lief_bzg.a = ratod (Lsts[0]->GetFeld ());

    for (int i = 1; i < Feldanz; i ++)
    {
        pr_ek = ratod (Lsts[i]->GetFeld ());
        if (UbForm->mask[i].attribut == BUTTON)
        {
            strcpy (lief_bzg.lief, UbForm->mask[i].item->GetFeldPtr ());
        }
        else if (UbForm->mask[i].attribut == COLBUTTON)
        {
            ColButton *Cub = (ColButton *) UbForm->mask[i].item->GetFeldPtr ();
            strcpy (lief_bzg.lief, Cub->text1);
        }
        lief_bzg.pr_ek = 0.0;
        if (lief_bzg.a == 0.0) continue;
        int dsqlstatus = LiefBzg.dbreadfirst ();
        if (dsqlstatus != 0) 
        {
            if (pr_ek == 0.0)
            {
                   continue;
            }
            sprintf (lief_bzg.lief_best, "%.0lf", lief_bzg.a);
        }

        sprintf (ek_old, "%.4lf", lief_bzg.pr_ek_eur);
        sprintf (ek_new, "%.4lf", pr_ek);
        if (strcmp (ek_old, ek_new) == 0) continue;
        lief_bzg.pr_ek_eur = pr_ek;
        lief_bzg.pr_ek = pr_ek * 1.95583;
        dsqlstatus = LiefBzg.dbupdate ();
        if (dsqlstatus != 0)
        {
            return dsqlstatus;
        }
    }
    return 0;
}

//    ============================================================================
//							=====================
//    =======================    Maschinen Liste  ==================================
//							=====================
//    ============================================================================

void Work::GetMaschBz (char *MaschBz, long masch_nr)
{
    strcpy (MaschBz, "");
    if (masch_nr == 0l)
    {
        return;
    }
    DbClass.sqlin ((long *)  &masch_nr, 2, 0);
    DbClass.sqlout ((char *) MaschBz,   0,25);
    DbClass.sqlcomm ("select masch_bz from maschinen where masch_nr = ?");
}


void Work::FillMaschRow (void)
{
    PRDK_K_CLASS Prdk_k;

    if (MaschLsts == NULL) return;

//030805    sprintf (MaschLsts->a,     "%.0lf", prstatatag.a);
    sprintf (MaschLsts->a,     "%.0lf", _a_bas.a);
    sprintf (MaschLsts->a_bz1, "%s",    _a_bas.a_bz1);
    sprintf (MaschLsts->chg,   "%.3lf", prdk_k.kutter_gew); //250504
    MaschLsts->posi = prstatatag.posi; 
    prdk_k.mdn = prstatatag.mdn;
    prdk_k.a = _a_bas.a;
    if (prstatatag.phase1 == DbClass.LongNull)
    {
        prstatatag.phase1 = 0l;
    }
    if (prstatatag.phase2 == DbClass.LongNull)
    {
        prstatatag.phase2 = 0l;
    }
    if (prstatatag.phase3 == DbClass.LongNull)
    {
        prstatatag.phase3 = 0l;
    }
    if (prstatatag.phase4 == DbClass.LongNull)
    {
        prstatatag.phase4 = 0l;
    }
    if (prstatatag.phase5 == DbClass.LongNull)
    {
        prstatatag.phase5 = 0l;
    }
    if (prstatatag.phase6 == DbClass.LongNull)
    {
        prstatatag.phase6 = 0l;
    }
    if (prstatatag.phase7 == DbClass.LongNull)
    {
        prstatatag.phase7 = 0l;
    }
    if (prstatatag.phase8 == DbClass.LongNull)
    {
        prstatatag.phase8 = 0l;
    }
    if (prstatatag.phase9 == DbClass.LongNull)
    {
        prstatatag.phase9 = 0l;
    }
    if (prstatatag.phase10 == DbClass.LongNull)
    {
        prstatatag.phase10 = 0l;
    }

    int dsqlstatus = Prdk_k.dbreadfirst ();
    if (dsqlstatus == 0)
    {
       if (prstatatag.planstatus == 0)
       {
            prstatatag.phase1 = prdk_k.masch_nr1;
            prstatatag.phase2 = prdk_k.masch_nr2;
            prstatatag.phase3 = prdk_k.masch_nr3;
            prstatatag.phase4 = prdk_k.masch_nr4;
            prstatatag.phase5 = prdk_k.masch_nr5;
            prstatatag.phase6 = prdk_k.masch_nr6;  
            prstatatag.phase7 = prdk_k.masch_nr7;  
            prstatatag.phase8 = prdk_k.masch_nr8;  
            prstatatag.phase9 = prdk_k.masch_nr9;  
            prstatatag.phase10 = prdk_k.masch_nr10;  
       }
    }

    strcpy (MaschLsts->rez, prdk_k.rez);
    sprintf (MaschLsts->me, "%.3lf", prstatatag.me);
    sprintf (MaschLsts->masch_nr1, "%ld", prstatatag.phase1);
    sprintf (MaschLsts->masch_nr2, "%ld", prstatatag.phase2);
    sprintf (MaschLsts->masch_nr3, "%ld", prstatatag.phase3);
    sprintf (MaschLsts->masch_nr4, "%ld", prstatatag.phase4);
    sprintf (MaschLsts->masch_nr5, "%ld", prstatatag.phase5);
    sprintf (MaschLsts->masch_nr6, "%ld", prstatatag.phase6); 
    sprintf (MaschLsts->masch_nr7, "%ld", prstatatag.phase7);
    sprintf (MaschLsts->masch_nr8, "%ld", prstatatag.phase8);
    sprintf (MaschLsts->masch_nr9, "%ld", prstatatag.phase9); 
    sprintf (MaschLsts->masch_nr10, "%ld", prstatatag.phase10); 
    prstatatag.phase1 = 0l;
    prstatatag.phase2 = 0l;
    prstatatag.phase3 = 0l;
    prstatatag.phase4 = 0l;
    prstatatag.phase5 = 0l;
    prstatatag.phase6 = 0l;
    prstatatag.phase7 = 0l;
    prstatatag.phase8 = 0l;
    prstatatag.phase9 = 0l;
    prstatatag.phase10 = 0l;

    MaschLsts->chmasch_nr.SetFeld ("..");
}


void Work::InitMaschRow (void)
{
    if (MaschLsts == NULL) return;

}

int Work::TestPhasen (void)
{
    int planstatus = 2;
    short phase;
    static long *phasetab[] = {&prstatatag.phase1,
                               &prstatatag.phase2,
                               &prstatatag.phase3,
                               &prstatatag.phase4,
                               &prstatatag.phase5,
                               &prstatatag.phase6,
                               &prstatatag.phase7,
                               &prstatatag.phase8,
                               &prstatatag.phase9,
                               &prstatatag.phase10,
                               NULL};

    DbClass.sqlin ((short *)   &prstatatag.mdn, 1, 0);
    DbClass.sqlin ((long *)    &prstatatag.dat, 2, 0);
    DbClass.sqlin ((double *)  &prstatatag.a, 3, 0);
    DbClass.sqlin ((short *)  &prstatatag.posi, 1, 0); //300905
    DbClass.sqlin ((short *)   &phase, 1, 0);

    int cursor = DbClass.sqlcursor ("select a from prstatmasch "
                                    "where mdn = ? "
                                    "and dat = ? "
                                    "and a = ? "
									"and posi = ? "
                                    "and phase = ? and status = 1"); //041105 bgut1

    for (int i = 0; phasetab[i] != NULL; i ++)
    {
        if (*phasetab[i] == 0l)
        {
            continue;
        }
        phase = i + 1;
        DbClass.sqlopen (cursor);
        int dsqlstatus = DbClass.sqlfetch (cursor);
        if (dsqlstatus == 100)
        {
            DbClass.sqlclose (cursor);
            return 1;
        }
    }
    DbClass.sqlclose (cursor);
    return 2;
}

void Work::WriteMaschRec (void)
{
    if (MaschLsts == NULL) return;

    short mdn = prstatatag.mdn;
    memcpy (&prstatatag, &prstatatag_null, sizeof (prstatatag));
    prstatatag.mdn = mdn;
    prstatatag.a = ratod (MaschLsts->a);
    prstatatag.dat = AktDat;
    prstatatag.prodabt = abt; 
    prstatatag.posi = MaschLsts->posi; 

    int dsqlstatus = PrStatatag.dbreadfirst ();
    if (dsqlstatus != 0)
    {
        strcpy (prstatatag.rez, MaschLsts->rez);
    }
    prstatatag.phase1 = atol (MaschLsts->masch_nr1);
    prstatatag.phase2 = atol (MaschLsts->masch_nr2);
    prstatatag.phase3 = atol (MaschLsts->masch_nr3);
    prstatatag.phase4 = atol (MaschLsts->masch_nr4);
    prstatatag.phase5 = atol (MaschLsts->masch_nr5);
    prstatatag.phase6 = atol (MaschLsts->masch_nr6);
    prstatatag.phase7 = atol (MaschLsts->masch_nr7);
    prstatatag.phase8 = atol (MaschLsts->masch_nr8);
    prstatatag.phase9 = atol (MaschLsts->masch_nr9);
    prstatatag.phase10 = atol (MaschLsts->masch_nr10);
    prstatatag.planstatus = TestPhasen ();;
	prstatatag.me = 0.0;
	prstatatag.vpk_ok = 3;
    PrStatatag.dbupdate ();
}

void Work::WriteMaschRec (double a)
{

    short mdn = prstatatag.mdn;
    memcpy (&prstatatag, &prstatatag_null, sizeof (prstatatag));
    prstatatag.mdn = mdn;
    prstatatag.a = a;
    prstatatag.dat = AktDat;
    prstatatag.prodabt = abt; 


    int dsqlstatus = PrStatatag.dbreadfirst ();
    prstatatag.planstatus = TestPhasen ();
	prstatatag.me = 0.0;  
	prstatatag.vpk_ok = 3;
    PrStatatag.dbupdate ();
}


int Work::PrepareMasch (void)
{
    masch_cursor = Prepare ();
    return masch_cursor;
}

int Work::OpenMasch (void)
{
    if (masch_cursor == -1)
    {
        PrepareMasch ();
        if (masch_cursor == -1)
        {
            return 0;
        }
    }
    DbClass.sqlopen (masch_cursor);
    return 0;
}


int Work::FetchMasch (void)
{
    if (masch_cursor == -1)
    {
        PrepareMasch ();
        if (masch_cursor == -1)
        {
            return 0;
        }
    } 

    short mdn = prstatatag.mdn;
    memcpy (&prstatatag, &prstatatag_null, sizeof (prstatatag));
    memcpy (&prstatmasch, &prstatmasch_null, sizeof (prstatmasch));
    prstatatag.mdn = mdn;
    int dsqlstatus = DbClass.sqlfetch (masch_cursor);
    if (dsqlstatus == 0)
    {
        prstatatag.a   = _a_bas.a;
        prstatatag.dat = AktDat;
	    prstatatag.prodabt = abt;  
        if (PrStatatag.dbreadfirst () != 0)
        {
            memcpy (&prstatatag, &prstatatag_null, sizeof (prstatatag));
            prstatatag.mdn = mdn;
            prstatatag.a   = 0.0;
            prstatatag.dat = AktDat;
        }
    }

    return dsqlstatus;
}


int Work::CloseMasch (void)
{
    if (masch_cursor != -1)
    {
        Close ();
        masch_cursor = -1;
    }
    return 0;
}








BOOL Work::GetPlanStatusR (double a, short phase, long masch_nr)
{
    DbClass.sqlin  ((short *)  &prstatatag.mdn, 1, 0);
    DbClass.sqlin  ((long *)   &AktDat, 2, 0);
    DbClass.sqlin  ((double *) &a, 3, 0);
    DbClass.sqlin  ((short *)  &phase, 1, 0);
    DbClass.sqlin  ((long *)   &masch_nr, 2, 0);
    int dsqlstatus = DbClass.sqlcomm ("select a from prstatmasch "
                                      "where mdn = ? "
                                      "and dat = ? "
                                      "and a = ? "
                                      "and phase = ? "
                                      "and masch_nr = ? and status = 1");   //041105 Bgut1 
    if (dsqlstatus == 0)
    {
        return TRUE;
    }
    return FALSE;
}



//    ============================================================================
//							=====================
//    =======================    Personen Liste  ==================================
//							=====================
//    ============================================================================

double Work::GetMaschMe (long datum, long masch_nr)
{
	double menge = 0.0;
	double sum_me = 0.0;
	long anzahl = 0;
    long aktdat = datum;
	long masch1 = 0;
	long masch2 = 0;
	long masch3 = 0;
	long masch4 = 0;
	long masch5 = 0;
	long masch6 = 0;
	long masch7 = 0;
	long masch8 = 0;
	long masch9 = 0;
	long masch10 = 0;
	short masch1_zeit = 0;
	short masch2_zeit = 0;
	short masch3_zeit = 0;
	short masch4_zeit = 0;
	short masch5_zeit = 0;
	short masch6_zeit = 0;
	short masch7_zeit = 0;
	short masch8_zeit = 0;
	short masch9_zeit = 0;
	short masch10_zeit = 0;
	AktSumZeit = 0.0;

    DbClass.sqlin  ((long *)   &aktdat, 2, 0);
    DbClass.sqlin  ((long *)   &masch_nr, 2, 0);
    DbClass.sqlin  ((short *)  &prstatatag.mdn, 1, 0);
    DbClass.sqlout  ((double *)  &menge, 3, 0);
    DbClass.sqlout  ((long *)  &masch1, 2, 0);
    DbClass.sqlout  ((long *)  &masch2, 2, 0);
    DbClass.sqlout  ((long *)  &masch3, 2, 0);
    DbClass.sqlout  ((long *)  &masch4, 2, 0);
    DbClass.sqlout  ((long *)  &masch5, 2, 0);
    DbClass.sqlout  ((long *)  &masch6, 2, 0);
    DbClass.sqlout  ((long *)  &masch7, 2, 0);
    DbClass.sqlout  ((long *)  &masch8, 2, 0);
    DbClass.sqlout  ((long *)  &masch9, 2, 0);
    DbClass.sqlout  ((long *)  &masch10, 2, 0);
    DbClass.sqlout  ((short *)  &masch1_zeit, 1, 0);
    DbClass.sqlout  ((short *)  &masch2_zeit, 1, 0);
    DbClass.sqlout  ((short *)  &masch3_zeit, 1, 0);
    DbClass.sqlout  ((short *)  &masch4_zeit, 1, 0);
    DbClass.sqlout  ((short *)  &masch5_zeit, 1, 0);
    DbClass.sqlout  ((short *)  &masch6_zeit, 1, 0);
    DbClass.sqlout  ((short *)  &masch7_zeit, 1, 0);
    DbClass.sqlout  ((short *)  &masch8_zeit, 1, 0);
    DbClass.sqlout  ((short *)  &masch9_zeit, 1, 0);
    DbClass.sqlout  ((short *)  &masch10_zeit, 1, 0);
    int cursormasch = DbClass.sqlcursor ("select prstatmasch.me, "
											"prdk_k.masch_nr1, "
											"prdk_k.masch_nr2, "
											"prdk_k.masch_nr3, "
											"prdk_k.masch_nr4, "
											"prdk_k.masch_nr5, "
											"prdk_k.masch_nr6, "
											"prdk_k.masch_nr7, "
											"prdk_k.masch_nr8, "
											"prdk_k.masch_nr9, "
											"prdk_k.masch_nr10, "
											"prdk_k.masch1_zeit, "
											"prdk_k.masch2_zeit, "
											"prdk_k.masch3_zeit, "
											"prdk_k.masch4_zeit, "
											"prdk_k.masch5_zeit, "
											"prdk_k.masch6_zeit, "
											"prdk_k.masch7_zeit, "
											"prdk_k.masch8_zeit, "
											"prdk_k.masch9_zeit, "
											"prdk_k.masch10_zeit "
									   "from prstatmasch,prdk_k "
                                      "where prstatmasch.dat = ? "
                                      "and prstatmasch.masch_nr = ? "
                                      "and prstatmasch.mdn = ? "
									  "and prstatmasch.mdn = prdk_k.mdn "
									  "and prstatmasch.a = prdk_k.a "
									  "and prdk_k.akv = 1 ");
	while (DbClass.sqlfetch (cursormasch) == 0)
	{
		sum_me += menge;
		if (masch1 == masch_nr) AktSumZeit += (masch1_zeit * menge / 100);
		if (masch2 == masch_nr) AktSumZeit += (masch2_zeit * menge / 100);
		if (masch3 == masch_nr) AktSumZeit += (masch3_zeit * menge / 100);
		if (masch4 == masch_nr) AktSumZeit += (masch4_zeit * menge / 100);
		if (masch5 == masch_nr) AktSumZeit += (masch5_zeit * menge / 100);
		if (masch6 == masch_nr) AktSumZeit += (masch6_zeit * menge / 100);
		if (masch7 == masch_nr) AktSumZeit += (masch7_zeit * menge / 100);
		if (masch8 == masch_nr) AktSumZeit += (masch8_zeit * menge / 100);
		if (masch9 == masch_nr) AktSumZeit += (masch9_zeit * menge / 100);
		if (masch10 == masch_nr) AktSumZeit += (masch10_zeit * menge / 100);
	}
	DbClass.sqlclose (cursormasch);
    return sum_me;
}


int Work::GetPersBz (char *PersBz, char *eingabe, short mdn)
{

	char Pers [17];
	if (strlen(clipped(eingabe)) < 1) return 100;
    int pers_nr = atoi (eingabe);
	if (pers_nr > 0)
	{
		strcpy(Pers,clipped(eingabe));
	    DbClass.sqlin ((char *)  &mdn, 1, 0);
	    DbClass.sqlin ((char *)  Pers, 0, 13);
	    DbClass.sqlout ((char *) PersBz,   0,17);
	    DbClass.sqlout ((char *) prstatpers.pers,   0,13);
	    int dsqlstatus = DbClass.sqlcomm ("select adr_krz,pers.pers from pers,adr where pers.mdn = ? and pers = ? "
						 "and pers.adr = adr.adr ");
		if (dsqlstatus != 0) strcpy (PersBz, "");
		return dsqlstatus;
	
	}
	sprintf(Pers,"%s*",clipped(eingabe));
    DbClass.sqlin ((char *)  &mdn, 1, 0);
    DbClass.sqlin ((char *)  Pers, 0, 16);
    DbClass.sqlout ((char *) PersBz,   0,17);
    DbClass.sqlout ((char *) prstatpers.pers,   0,13);
    int dsqlstatus = DbClass.sqlcomm ("select adr_krz,pers.pers from pers,adr where pers.mdn = ? and adr_krz matches ? "
					 "and pers.adr = adr.adr ");
	if (dsqlstatus != 0) strcpy (PersBz, "");
	return dsqlstatus;
}


void Work::FillPersRow (long StartDay, int fieldanz)
{
    char buffer [30];
	char cpers [13];
	char cpers1 [13];
	char cpers2 [13];
	char cpers3 [13];
	char cpers4 [13];
	char PersBz [30];
	char cprodabt [34];
    long day;
    int i;
	bool gefunden ;

    if (PersLsts == NULL) return;

	gefunden = FALSE;
    prstatpers.masch_nr   = atoi (PersLsts[0]->GetFeld ());
    CFELD::SetFeldWrt (PersLsts, "masch_bz", clipped(maschinen.masch_bz));
	GetAbtBez (cprodabt,prstatpers.prodabt);
    CFELD::SetFeldWrt (PersLsts, "prodabt", clipped(cprodabt));

//    sprintf (buffer, "%.3lf",  HoleLiefMe(prstatatag.a));
//    CFELD::SetFeldWrt (PersLsts, "mengen", buffer);
    i = BeginPersCol;
    for (day = StartDay; i < fieldanz - 1; i+= AnzPers , day ++)
    {
	    DbClass.sqlin ((short *) &prstatatag.mdn, 1, 0);
	    DbClass.sqlin ((long *) &day, 2, 0);
	    DbClass.sqlin ((int *) &prstatpers.masch_nr, 2, 0);
	    DbClass.sqlin ((short *) &prstatpers.prodabt, 1, 0);
	    DbClass.sqlout ((char *) cpers, 0, 13);
	    int cursorpers = DbClass.sqlcursor ("select pers from prstatpers where mdn = ? and dat = ? "
											"and masch_nr = ? and prodabt = ?");
		for (int anz = 0; anz < AnzPers; anz++)
		{
			if ((i+anz) < fieldanz - 1)
			{
				if (DbClass.sqlfetch (cursorpers) == 0)
				{
					 if (GetPersBz(PersBz, cpers, prstatatag.mdn) == 0)
					 {
		 					sprintf (buffer, "%s", clipped(PersBz));
					 }
					 else
					 {
							sprintf (buffer, "%s", clipped(cpers));
					 }
					*PersLsts[i+anz] = (char *) buffer;
					gefunden = TRUE;
				}
				else
				{
					*PersLsts[i+anz] = " ";
				}
			}
		}
		DbClass.sqlclose (cursorpers);
    }
	if (gefunden == FALSE)  //ToDO in diesem Fall defaulteinstellung aus Maschinenprogramm �bernehmen
	{
		i = BeginPersCol;
		for (day = StartDay; i < fieldanz - 1; i+= AnzPers , day ++)
		{
			DbClass.sqlin ((int *) &prstatpers.masch_nr, 2, 0);
			DbClass.sqlout ((char *) cpers1, 0, 13);
			DbClass.sqlout ((char *) cpers2, 0, 13);
			DbClass.sqlout ((char *) cpers3, 0, 13);
			DbClass.sqlout ((char *) cpers4, 0, 13);
			int cursorpers = DbClass.sqlcursor ("select pers1,pers2,pers3,pers4 from maschinen where masch_nr = ?");
			if (DbClass.sqlfetch (cursorpers) == 0)
			{
					sprintf (buffer, "%s", cpers1);
					if (GetPersBz(PersBz, cpers1, prstatatag.mdn) == 0)
					{
		 				sprintf (buffer, "%s", clipped(PersBz));
					}
					*PersLsts[i] = (char *) buffer;
					if (AnzPers >= 2)
					{
						sprintf (buffer, "%s", cpers2);
						if (GetPersBz(PersBz, cpers2, prstatatag.mdn) == 0)
						{
			 				sprintf (buffer, "%s", clipped(PersBz));
						}
						*PersLsts[i+1] = (char *) buffer;
					}
					if (AnzPers >= 3)
					{
						sprintf (buffer, "%s", cpers3);
						if (GetPersBz(PersBz, cpers3, prstatatag.mdn) == 0)
						{
			 				sprintf (buffer, "%s", clipped(PersBz));
						}
						*PersLsts[i+2] = (char *) buffer;
					}
					if (AnzPers >= 4)
					{
						sprintf (buffer, "%s", cpers4);
						if (GetPersBz(PersBz, cpers4, prstatatag.mdn) == 0)
						{
			 				sprintf (buffer, "%s", clipped(PersBz));
						}
						*PersLsts[i+3] = (char *) buffer;
					}
			}
			DbClass.sqlclose (cursorpers);
		}
	}
//    CFELD::SetFeldWrt (PersLsts, "rez", prdk_k.rez);
//    sprintf (buffer, "%hd", prdk_k.variante);
}


void Work::InitPersRow (void)
{
    if (PersLsts == NULL) return;

}


int Work::WritePersRec (form *UbForm, int Feldanz, long StartDay,short dzeil)
{
    long day;
    int i;
	char PersBz [17];
	char eingabe [17];
	char cprodabt [6];

    if (PersLsts == NULL) 
    {
        return 0;
    }


    short mdn = prstatatag.mdn;
     i = BeginPersCol;
	double me = 0.0;

    for (day = StartDay; i <= Feldanz ; i+= AnzPers , day ++)
    {
		prstatpers.mdn = prstatatag.mdn;
		prstatpers.dat = day;
		prstatpers.masch_nr =  atoi (CFELD::GetFeldWrt (PersLsts, "masch_nr"));
	    GetAbtNr(cprodabt, CFELD::GetFeldWrt (PersLsts, "prodabt"));
	    prstatpers.prodabt =  atoi (cprodabt);

            DbClass.sqlin ((short *)   &prstatpers.mdn, 1, 0);
            DbClass.sqlin ((long *)   &prstatpers.dat,  2, 0);
            DbClass.sqlin ((long *)   &prstatpers.masch_nr,  2, 0);
            DbClass.sqlin ((short *)   &prstatpers.prodabt, 1, 0);
            DbClass.sqlcomm ("delete from prstatpers "
                                  "where mdn = ? "
                                  "and   dat = ? "
                                  "and   masch_nr = ? "
                                  "and prodabt = ? ");

		for (int anz = 0; anz < AnzPers; anz++)
		{
			if ((i+anz) < Feldanz)
			{
				strcpy(eingabe, PersLsts[i+anz]->GetFeld ());
				if (prstatpers.masch_nr > 0 && strlen(clipped(eingabe)) > 0)
				{
				    if (GetPersBz(PersBz, eingabe, prstatpers.mdn) == 0)
					{
			  		    prstatpers.menge = GetMaschMe(day,prstatpers.masch_nr);
						AktSumZeit = GetAktSumZeit();
						PrStatPers.dbupdate ();
					}
				}
			}
		}
	}
    return 0;
}





int Work::PreparePers (void)
{
    DbClass.sqlin ((short *)   &prstatatag.mdn, 1, 0);
     DbClass.sqlin ((long *)   &DatVon, 2, 0);
     DbClass.sqlin ((long *)   &DatBis, 2, 0);
    DbClass.sqlout ((long *)   &prstatmasch.masch_nr, 2, 0);
    DbClass.sqlout ((char *)   maschinen.masch_bz, 0, 13);
    pers_cursor = DbClass.sqlcursor ("select unique (prstatmasch.masch_nr),maschinen.masch_bz from prstatmasch,maschinen "
                                       "where prstatmasch.mdn = ? "
                                       "and prstatmasch.dat >= ? "
                                       "and prstatmasch.dat <= ? "
									   " and prstatmasch.masch_nr = maschinen.masch_nr ");
    return pers_cursor;
}

int Work::OpenPers (void)
{
    if (pers_cursor == -1)
    {
        PreparePers ();
        if (pers_cursor == -1)
        {
            return 0;
        }
    }
    DbClass.sqlopen (pers_cursor);
    return 0;
}


int Work::FetchPers (void)
{
    if (pers_cursor == -1)
    {
        PreparePers ();
        if (masch_cursor == -1)
        {
            return 0;
        }
    } 

	if (dprodabt2 != 1)
	{
		prstatpers.prodabt = dprodabt2;
		dprodabt2 = 1;
		return 0;
	}
	if (dprodabt3 != 1)
	{
		prstatpers.prodabt = dprodabt3;
		dprodabt3 = 1;
		return 0;
	}
	if (dprodabt4 != 1)
	{
		prstatpers.prodabt = dprodabt4;
		dprodabt4 = 1;
		return 0;
	}
    short mdn = prstatatag.mdn;
    memcpy (&prstatpers, &prstatpers_null, sizeof (prstatpers));
    prstatpers.mdn = mdn;
    int dsqlstatus = DbClass.sqlfetch (pers_cursor);
    if (dsqlstatus == 0)
    {
        maschinen.masch_nr   = prstatmasch.masch_nr;
        if (Maschinen.dbreadfirst () == 0)
        {
            dprodabt1 = maschinen.prod_abt;
			if (maschinen.prod_abt2 != dprodabt1) dprodabt2 = maschinen.prod_abt2;  
			if (maschinen.prod_abt3 != dprodabt1 && 
				maschinen.prod_abt3 != dprodabt2) dprodabt3 = maschinen.prod_abt3;  
			if (maschinen.prod_abt4 != dprodabt1 && 
				maschinen.prod_abt4 != dprodabt2 && 
				maschinen.prod_abt4 != dprodabt3) dprodabt4 = maschinen.prod_abt4;  
			prstatpers.prodabt = dprodabt1;
        }
    }

    return dsqlstatus;
}


int Work::ClosePers (void)
{
    if (pers_cursor != -1)
    {
        Close ();
        pers_cursor = -1;
    }
    return 0;
}




//    ============================================================================
//							=====================
//    =======================    Phasen Liste  ==================================
//							=====================
//    ============================================================================

void Work::GetPhaseAbz1 (char *a_bz1, double a)
{
    strcpy (a_bz1, "");
    if (a == 0.0)
    {
        return;
    }
}


void Work::FillPhaseRow (void)
{
    PRDK_K_CLASS Prdk_k;
    long masch_nr;

    if (PhaseLsts == NULL) return;

    sprintf (PhaseLsts->a,     "%.0lf", prstatatag.a);
    PhaseLsts->PrStatMe = prstatatag.me;
    sprintf (PhaseLsts->a_bz1, "%s",    _a_bas.a_bz1);
    sprintf (PhaseLsts->chg,   "%.3lf", prdk_k.kutter_gew); //250504
    PhaseLsts->posi = prstatatag.posi; //300905
    prdk_k.mdn = prstatatag.mdn;
    prdk_k.a = _a_bas.a;

    switch (AktPhase)
    {
        case 1 :
          sprintf (PhaseLsts->masch_nr, "%ld", prstatatag.phase1);
          break;
        case 2 :
          sprintf (PhaseLsts->masch_nr, "%ld", prstatatag.phase2);
          break;
        case 3 :
          sprintf (PhaseLsts->masch_nr, "%ld", prstatatag.phase3);
          break;
        case 4 :
          sprintf (PhaseLsts->masch_nr, "%ld", prstatatag.phase4);
          break;
        case 5 :
          sprintf (PhaseLsts->masch_nr, "%ld", prstatatag.phase5);
          break;
        case 6 :
          sprintf (PhaseLsts->masch_nr, "%ld", prstatatag.phase6);
          break;
        case 7 :
          sprintf (PhaseLsts->masch_nr, "%ld", prstatatag.phase7);
          break;
        case 8 :
          sprintf (PhaseLsts->masch_nr, "%ld", prstatatag.phase8);
          break;
        case 9 :
          sprintf (PhaseLsts->masch_nr, "%ld", prstatatag.phase9);
          break;
        case 10 :
          sprintf (PhaseLsts->masch_nr, "%ld", prstatatag.phase10);
          break;
    }

    PhaseLsts->MaschNr2 = 0l;
    PhaseLsts->MaschNr3 = 0l;

    if (prstatmasch.me != 0.0)
    {
        masch_nr = AktMaschNr;
        sprintf (PhaseLsts->masch_nr, "%ld", prstatmasch.masch_nr);
        sprintf (PhaseLsts->me,    "%.3lf", prstatmasch.me);
        sprintf (PhaseLsts->folge, "%hd",   prstatmasch.folge);
        DbClass.sqlin ((short *)  &prstatatag.mdn, 1, 0);
        DbClass.sqlin ((double *) &prstatatag.a, 3, 0);
        DbClass.sqlin ((short *) &prstatatag.posi, 1, 0);  //300905
        DbClass.sqlin ((long *)   &AktDat, 2, 0);
        DbClass.sqlin ((short *)  &AktPhase, 1, 0);
        DbClass.sqlin ((long *)   &masch_nr, 2, 0);
        DbClass.sqlin ((long *)   &AktMaschNr, 2, 0);
        DbClass.sqlout ((long *)  &prstatmasch.masch_nr, 2, 0);
        int cursor = DbClass.sqlcursor ("select masch_nr from prstatmasch "
                                          "where mdn = ? "  
                                          "and   a = ? "  
										  "and posi = ? "
                                          "and   dat = ? "  
                                          "and   phase = ? "  
                                          "and   masch_nr != ? "
                                          "and   masch_nr != ?");  
        int dsqlstatus = DbClass.sqlfetch (cursor);
        if (dsqlstatus == 0 && PrStatMasch.dbreadfirst () == 0)
        {
               sprintf (PhaseLsts->me2,    "%.3lf",    prstatmasch.me);
               sprintf (PhaseLsts->folge2, "%hd",      prstatmasch.folge);
               sprintf (PhaseLsts->masch_nr2, "%hd",   prstatmasch.masch_nr);
               PhaseLsts->MaschNr2 = prstatmasch.masch_nr;

               masch_nr = prstatmasch.masch_nr;
               DbClass.sqlopen (cursor);
               dsqlstatus = DbClass.sqlfetch (cursor);
               if (dsqlstatus == 0 && PrStatMasch.dbreadfirst () == 0)
               {
                     sprintf (PhaseLsts->me3,       "%.3lf",    prstatmasch.me);
                     sprintf (PhaseLsts->folge3,    "%hd",      prstatmasch.folge);
                     sprintf (PhaseLsts->masch_nr3, "%hd",      prstatmasch.masch_nr);
                     PhaseLsts->MaschNr3 = prstatmasch.masch_nr;
               }
               else
               {
                     sprintf (PhaseLsts->me3,    "%.3lf", 0.0);
                     sprintf (PhaseLsts->folge3, "%hd",   0);
                     sprintf (PhaseLsts->masch_nr3, "%hd",   0);
               }

               DbClass.sqlclose (cursor);
        }
        else
        {
               sprintf (PhaseLsts->me2,    "%.3lf", 0.0);
               sprintf (PhaseLsts->folge2, "%hd",   0);
               sprintf (PhaseLsts->masch_nr2, "%hd",   0);
               sprintf (PhaseLsts->me3,    "%.3lf", 0.0);
               sprintf (PhaseLsts->folge3, "%hd",   0);
               sprintf (PhaseLsts->masch_nr3, "%hd",   0);
        }
    }
    else
    {
        sprintf (PhaseLsts->me,    "%.3lf", prstatatag.me);
        sprintf (PhaseLsts->folge, "%hd",   1);
        sprintf (PhaseLsts->me2,    "%.3lf", 0.0);
        sprintf (PhaseLsts->folge2, "%hd",   0);
        sprintf (PhaseLsts->masch_nr2, "%hd",   0);
        sprintf (PhaseLsts->me3,    "%.3lf", 0.0);
        sprintf (PhaseLsts->folge3, "%hd",   0);
        sprintf (PhaseLsts->masch_nr3, "%hd",   0);
    }
    if (ratod (PhaseLsts->me2) == 0.0)
    {
        sprintf (PhaseLsts->me,    "%.3lf", prstatatag.me);
        sprintf (PhaseLsts->folge2, "%hd",   0);
        sprintf (PhaseLsts->masch_nr2, "%hd",   0);
    }


    PhaseLsts->chmasch_nr.SetFeld ("..");
    PhaseLsts->chmasch_nr3.SetFeld ("..");
}


void Work::InitPhaseRow (void)
{
    if (PhaseLsts == NULL) return;
}


void Work::WritePhaseRec (void)
{
    if (PhaseLsts == NULL) return;

    memcpy (&prstatmasch, &prstatmasch_null, sizeof (prstatmasch));
    prstatmasch.mdn       = prstatatag.mdn;
    prstatmasch.a         = ratod (PhaseLsts->a);
    prstatmasch.posi      = PhaseLsts->posi; //300905
    prstatmasch.dat       = AktDat;
    prstatmasch.phase     = AktPhase;
    prstatmasch.masch_nr  = AktMaschNr;

    int dsqlstatus = PrStatMasch.dbreadfirst ();
    if (atol (PhaseLsts->masch_nr2) > 0l &&
        ratod (PhaseLsts->me2) != 0.0 &&
        atol (PhaseLsts->masch_nr2) != AktMaschNr)
    {
          prstatmasch.me    = ratod (PhaseLsts->me);
    }
    else
    {
          prstatmasch.me    = PhaseLsts->PrStatMe;
    }
    prstatmasch.folge    = atoi (PhaseLsts->folge);
	prstatmasch.status = 1; //041105 bugt1
    PrStatMasch.dbupdate ();
    if (atol (PhaseLsts->masch_nr2) > 0l &&
        ratod (PhaseLsts->me2) != 0.0 &&
        atol (PhaseLsts->masch_nr2) != AktMaschNr)
    {
           prstatmasch.masch_nr  = atol (PhaseLsts->masch_nr2);
           prstatmasch.me        = ratod (PhaseLsts->me2);
           prstatmasch.folge     = atoi (PhaseLsts->folge2);
           PrStatMasch.dbupdate ();
           if (PhaseLsts->MaschNr2 != prstatmasch.masch_nr)
           {
               prstatmasch.masch_nr = PhaseLsts->MaschNr2;
               PrStatMasch.dbdelete ();
           }
    }
    if (atol (PhaseLsts->masch_nr3) > 0l &&
        ratod (PhaseLsts->me3) != 0.0 &&
        atol (PhaseLsts->masch_nr3) != AktMaschNr)
    {
           prstatmasch.masch_nr  = atol (PhaseLsts->masch_nr3);
           prstatmasch.me        = ratod (PhaseLsts->me3);
           prstatmasch.folge     = atoi (PhaseLsts->folge3);
           PrStatMasch.dbupdate ();
           if (PhaseLsts->MaschNr3 != prstatmasch.masch_nr)
           {
               prstatmasch.masch_nr = PhaseLsts->MaschNr3;
               PrStatMasch.dbdelete ();
           }
    }
    WriteMaschRec (prstatmasch.a);
}


int Work::PreparePhase (void)
{
    phase_cursor = Prepare ();
    return phase_cursor;
}

int Work::OpenPhase (void)
{
    if (phase_cursor == -1)
    {
        PreparePhase ();
        if (phase_cursor == -1)
        {
            return 0;
        }
    }
    DbClass.sqlopen (phase_cursor);
    return 0;
}

int Work::FetchPhase (void)
{
    if (phase_cursor == -1)
    {
        PreparePhase ();
        if (phase_cursor == -1)
        {
            return 0;
        }
    }

    short mdn = prstatatag.mdn;
    memcpy (&prstatatag, &prstatatag_null, sizeof (prstatatag));
    prstatatag.mdn = mdn;
    if (phase_cursor2 != -1)
    {
        return FetchPhase2 ();
    }
    int dsqlstatus = DbClass.sqlfetch (phase_cursor);
    if (dsqlstatus == 0)
    {
        prstatatag.a   = _a_bas.a;
        prstatatag.dat = AktDat;
	    prstatatag.prodabt = abt;  
        if (PrStatatag.dbreadfirst () != 0)
        {
            memcpy (&prstatatag, &prstatatag_null, sizeof (prstatatag));
            prstatatag.mdn = mdn;
            prstatatag.a   = 0.0;
            prstatatag.dat = AktDat;
        }
        prstatmasch.mdn      = mdn;
        prstatmasch.a        = _a_bas.a;
        prstatmasch.dat      = AktDat;
        prstatmasch.phase    = AktPhase;
        prstatmasch.masch_nr = AktMaschNr;
        if (PrStatMasch.dbreadfirst () != 0)
        {
            memcpy (&prstatmasch, &prstatmasch_null, sizeof (prstatmasch));
        }
    }
/*
    if (dsqlstatus == 100 && phase_cursor2 == -1)
    {
        PreparePhase2 ();
        return FetchPhase2 ();
    }
*/

    return dsqlstatus;
}

int Work::ClosePhase (void)
{
    if (phase_cursor != -1)
    {
        Close ();
        phase_cursor = -1;
    }
    ClosePhase2 ();
    return 0;
}

int Work::PreparePhase2 (void)
{
    DbClass.sqlin ((short *)   &prstatatag.mdn, 1, 0);
    DbClass.sqlin ((long *)    &AktDat, 2, 0);
    DbClass.sqlin ((long *)    &AktMaschNr, 2, 0);
    DbClass.sqlin ((short *)   &AktPhase, 1, 0);
    DbClass.sqlout ((double *) &_a_bas.a, 3, 0);
    DbClass.sqlout ((char *)   _a_bas.a_bz1, 0, 25);
    phase_cursor2 = DbClass.sqlcursor ("select a_bas.a, a_bas.a_bz1 from prstatmasch, a_bas "
                                       "where prstatmasch.mdn = ? "
                                       "and prstatmasch.dat = ? "
                                       "and prstatmasch.masch_nr = ? "
                                       "and prstatmasch.phase = ? "
//                                       "and prstatmasch.status = 2 "
                                       "and a_bas.a = prstatmasch.a");
    return phase_cursor2;
}

int Work::OpenPhase2 (void)
{
    if (phase_cursor2 == -1)
    {
        phase_cursor2 = PreparePhase2 ();
    }
    return DbClass.sqlopen (phase_cursor2);
}

int Work::FetchPhase2 (void)
{
    if (phase_cursor2 == -1)
    {
        phase_cursor2 = PreparePhase2 ();
    }
    int dsqlstatus = DbClass.sqlfetch (phase_cursor2);
    if (dsqlstatus != 0)
    {
        return dsqlstatus;
    }
    prstatatag.mdn      = prstatatag.mdn;
    prstatatag.dat      = AktDat;
    prstatatag.a        = _a_bas.a;

    prstatmasch.mdn      = prstatatag.mdn;
    prstatmasch.dat      = AktDat;
    prstatmasch.masch_nr = AktMaschNr;
    prstatmasch.phase    = AktPhase;
    prstatmasch.a        = _a_bas.a;

    PrStatMasch.dbreadfirst ();
    return dsqlstatus;
}


int Work::ClosePhase2 (void)
{
    if (phase_cursor2 != -1)
    {
        DbClass.sqlclose (phase_cursor2);
        phase_cursor2 = -1;
    }
    return 0;
}

int Work::GetProdPhasen (ITEM *MaschTab[])
{
    PTAB_CLASS Ptab;

    int anz = 0;
    int dsqlstatus = Ptab.lese_ptab_all ("prodphase");
    while (dsqlstatus == 0)
    {
        MaschTab[anz]->SetFeld (ptabn.ptbezk);
        anz ++;
        dsqlstatus = Ptab.lese_ptab_all ();
    }
    return anz --;
}

long Work::DatToLong (char *dats)
{
    return dasc_to_long (dats);
}

char *Work::LongToDat (long dat, char *dats)
{
    dlong_to_asc (dat, dats);
    return dats;
}

int Work::DeleteVerbrTag (short mdn, long dat, short abt)
{
	char dats[12];
    verbr_tag.mdn = mdn;
    verbr_tag.dat   = dat;
    dlong_to_asc (verbr_tag.dat, dats);
    DbClass.sqlin  ((short *) &verbr_tag.mdn, 1, 0);
    DbClass.sqlin  ((char *)   dats, 0, 12);
    DbClass.sqlin  ((short *) &abt, 1, 0);
	if (abt == 0)
	{
		return DbClass.sqlcomm ("delete from verbr_tag "
                                      "where mdn = ? "
                                      "and dat = ? "
									  "and (prodabt = ? or prodabt >= 0) ");
	}
	else
	{
		if (strupcmp (ProdAbt, "a_bas", strlen (ProdAbt)) == 0)
		{
			return DbClass.sqlcomm ("delete from verbr_tag "
                                      "where mdn = ? "
                                      "and dat = ? "
									  "and prodabt = ? ");
//									   "and artikel in (select a from a_bas where abt = ? )" );
	    }
		else
	    {
			return DbClass.sqlcomm ("delete from verbr_tag "
                                      "where mdn = ? "
                                      "and dat = ? "
									  "and prodabt = ? ");
//									   "and artikel in (select a from a_preis where prodabt = ? )" );
		}
	}
}

int Work::DeletePrstatatagGbrt (short mdn, long dat, short abt)
{
	char dats[12];
    prstatatag.mdn = mdn;
    prstatatag.dat   = dat;
    dlong_to_asc (prstatatag.dat, dats);
    DbClass.sqlin  ((short *) &prstatatag.mdn, 1, 0);
    DbClass.sqlin  ((char *)   dats, 0, 12);
		return DbClass.sqlcomm ("delete from prstatatag "
                                      "where mdn = ? "
                                      "and dat = ? "
									   "and charge_ok = 0 and me = 0  ");
}

int Work::Delete0MengeVonPrstatatag (short mdn, long dat, short abt)
{
	char dats[12];
    prstatatag.mdn = mdn;
    prstatatag.dat   = dat;
    dlong_to_asc (prstatatag.dat, dats);
    DbClass.sqlin  ((short *) &prstatatag.mdn, 1, 0);
    DbClass.sqlin  ((char *)   dats, 0, 12);
	//150305
		return DbClass.sqlcomm ("delete from prstatatag "
                                      "where mdn = ? "
                                      "and dat = ? "
									   "and charge_ok = 0 " 
									   "and me = 0" );
}


int Work::PrepareVarb (void)
{
    DbClass.sqlin ((short *)   &verbr_tag.mdn, 1, 0);
    DbClass.sqlin ((double *)    &verbr_tag.artikel, 3, 0);
    DbClass.sqlout ((char *) prdk_varb.rez, 0, 8);
    DbClass.sqlout ((double *) &varb_artikel, 3, 0);
    DbClass.sqlout ((long *) &prdk_varb.mat, 2, 0);
    DbClass.sqlout ((short *) &prdk_varb.variante, 1, 0);
    DbClass.sqlout ((short *) &prdk_varb.id_nr, 1, 0);
    DbClass.sqlout ((double *) &me_ist, 3, 0);
    DbClass.sqlout ((short *)   &me_einh, 1, 0);
    DbClass.sqlout ((char *) prdk_varb.bearb_info, 0, 81);
    DbClass.sqlout ((short *)   &prdk_varb.typ, 1, 0);
    varb_cursor = DbClass.sqlcursor ("select prdk_k.rez, a_mat.a,prdk_varb.mat, prdk_varb.variante, prdk_varb.id_nr, "
									   "prdk_varb.varb_me, a_bas.me_einh, prdk_varb.bearb_info, prdk_varb.typ from prdk_varb, a_bas, a_mat, prdk_k "
                                       "where prdk_k.mdn = ? "
                                       "and prdk_k.a  = ? "
                                       "and prdk_k.akv  = 1 "
                                       "and prdk_k.mdn  = prdk_varb.mdn "
                                       "and prdk_k.variante  = prdk_varb.variante "
                                       "and prdk_k.rez  = prdk_varb.rez "
                                       "and a_bas.a = a_mat.a "
									   "and a_mat.mat = prdk_varb.mat");
    return varb_cursor;
}

int Work::PrepareVarb1 (void)
{
    DbClass.sqlin ((short *)   &verbr_tag.mdn, 1, 0);
    DbClass.sqlin ((double *)    &verbr_tag.artikel, 3, 0);
    DbClass.sqlout ((char *) prdk_varb.rez, 0, 8);
    DbClass.sqlout ((double *) &varb_artikel, 3, 0);
    DbClass.sqlout ((long *) &prdk_varb.mat, 2, 0);
    DbClass.sqlout ((short *) &prdk_varb.variante, 1, 0);
    DbClass.sqlout ((short *) &prdk_varb.id_nr, 1, 0);
    DbClass.sqlout ((double *) &me_ist, 3, 0);
    DbClass.sqlout ((short *)   &me_einh, 1, 0);
    DbClass.sqlout ((char *) prdk_varb.bearb_info, 0, 81);
    DbClass.sqlout ((short *)   &prdk_varb.typ, 1, 0);
    varb_cursor1 = DbClass.sqlcursor ("select prdk_k.rez, a_mat.a,prdk_varb.mat, prdk_varb.variante, prdk_varb.id_nr, "
									   "prdk_varb.varb_me, a_bas.me_einh, prdk_varb.bearb_info, prdk_varb.typ  from prdk_varb, a_bas, a_mat, prdk_k "
                                       "where prdk_k.mdn = ? "
                                       "and prdk_k.a  = ? "
                                       "and prdk_k.akv  = 1 "
                                       "and prdk_k.mdn  = prdk_varb.mdn "
                                       "and prdk_k.variante  = prdk_varb.variante "
                                       "and prdk_k.rez  = prdk_varb.rez "
                                       "and a_bas.a = a_mat.a "
									   "and a_mat.mat = prdk_varb.mat");
    return varb_cursor1;
}
int Work::PrepareVarb2 (void)
{
    DbClass.sqlin ((short *)   &verbr_tag.mdn, 1, 0);
    DbClass.sqlin ((double *)    &verbr_tag.artikel, 3, 0);
    DbClass.sqlout ((char *) prdk_varb.rez, 0, 8);
    DbClass.sqlout ((double *) &varb_artikel, 3, 0);
    DbClass.sqlout ((long *) &prdk_varb.mat, 2, 0);
    DbClass.sqlout ((short *) &prdk_varb.variante, 1, 0);
    DbClass.sqlout ((short *) &prdk_varb.id_nr, 1, 0);
    DbClass.sqlout ((double *) &me_ist, 3, 0);
    DbClass.sqlout ((short *)   &me_einh, 1, 0);
    DbClass.sqlout ((char *) prdk_varb.bearb_info, 0, 81);
    DbClass.sqlout ((short *)   &prdk_varb.typ, 1, 0);
    varb_cursor2 = DbClass.sqlcursor ("select prdk_k.rez, a_mat.a,prdk_varb.mat, prdk_varb.variante, prdk_varb.id_nr, "
									   "prdk_varb.varb_me, a_bas.me_einh, prdk_varb.bearb_info, prdk_varb.typ  from prdk_varb, a_bas, a_mat, prdk_k "
                                       "where prdk_k.mdn = ? "
                                       "and prdk_k.a  = ? "
                                       "and prdk_k.akv  = 1 "
                                       "and prdk_k.mdn  = prdk_varb.mdn "
                                       "and prdk_k.variante  = prdk_varb.variante "
                                       "and prdk_k.rez  = prdk_varb.rez "
                                       "and a_bas.a = a_mat.a "
									   "and a_mat.mat = prdk_varb.mat");
    return varb_cursor2;
}
int Work::OpenVarb (short di)
{
 switch (di)
 {
        case 0 :
		    if (varb_cursor == -1)
			{
				varb_cursor = PrepareVarb ();
		    }
		    return DbClass.sqlopen (varb_cursor);
			break;
			
        case 1 :
		    if (varb_cursor1 == -1)
			{
				varb_cursor1 = PrepareVarb1 ();
		    }
		    return DbClass.sqlopen (varb_cursor1);
			break;
        case 2 :
		    if (varb_cursor2 == -1)
			{
				varb_cursor2 = PrepareVarb2 ();
		    }
		    return DbClass.sqlopen (varb_cursor2);
			break;
 }
 return DbClass.sqlopen (varb_cursor);
}

int Work::FetchVarb (short di)
{
 int dsqlstatus;
 switch (di)
 {
        case 0 :
		    if (varb_cursor == -1)
			{
				varb_cursor = PrepareVarb ();
			}
			dsqlstatus = DbClass.sqlfetch (varb_cursor);
			if (dsqlstatus != 0)
			{
				return dsqlstatus;
			}
			break;
        case 1 :
		    if (varb_cursor1 == -1)
			{
				varb_cursor1 = PrepareVarb1 ();
			}
			dsqlstatus = DbClass.sqlfetch (varb_cursor1);
			if (dsqlstatus != 0)
			{
				return dsqlstatus;
			}
			break;
        case 2 :
		    if (varb_cursor2 == -1)
			{
				varb_cursor2 = PrepareVarb2 ();
			}
			dsqlstatus = DbClass.sqlfetch (varb_cursor2);
			if (dsqlstatus != 0)
			{
				return dsqlstatus;
			}
			break;
 }
  prdk_varb.mdn      = verbr_tag.mdn;
//  return PrdkVarb.dbreadfirst ();
  return dsqlstatus;

}


int Work::CloseVarb (void)
{
    if (varb_cursor != -1)
    {
        DbClass.sqlclose (varb_cursor);
        varb_cursor = -1;
    }
    if (varb_cursor1 != -1)
    {
        DbClass.sqlclose (varb_cursor1);
        varb_cursor1 = -1;
    }
    if (varb_cursor2 != -1)
    {
        DbClass.sqlclose (varb_cursor2);
        varb_cursor2 = -1;
    }
    return 0;
}

int Work::PrepareZut (void)
{
    DbClass.sqlin ((short *)   &verbr_tag.mdn, 1, 0);
    DbClass.sqlin ((double *)    &verbr_tag.artikel, 3, 0);
    DbClass.sqlout ((char *) prdk_zut.rez, 0, 8);
    DbClass.sqlout ((double *) &varb_artikel, 3, 0);
    DbClass.sqlout ((long *) &prdk_zut.mat, 2, 0);
    DbClass.sqlout ((short *) &prdk_zut.variante, 1, 0);
    DbClass.sqlout ((double *) &me_ist, 3, 0);
    DbClass.sqlout ((short *)   &me_einh, 1, 0);
    zut_cursor = DbClass.sqlcursor ("select prdk_k.rez, a_mat.a,prdk_zut.mat, prdk_zut.variante, prdk_zut.zut_me, a_bas.me_einh from prdk_zut, a_bas, a_mat, prdk_k "
                                       "where prdk_k.mdn = ? "
                                       "and prdk_k.a  = ? "
                                       "and prdk_k.akv  = 1 "
                                       "and prdk_k.mdn  = prdk_zut.mdn "
                                       "and prdk_k.variante  = prdk_zut.variante "
                                       "and prdk_k.rez  = prdk_zut.rez "
                                       "and a_bas.a = a_mat.a "
									   "and a_mat.mat = prdk_zut.mat");
    return zut_cursor;
}
int Work::PrepareZut1 (void)
{
    DbClass.sqlin ((short *)   &verbr_tag.mdn, 1, 0);
    DbClass.sqlin ((double *)    &verbr_tag.artikel, 3, 0);
    DbClass.sqlout ((char *) prdk_zut.rez, 0, 8);
    DbClass.sqlout ((double *) &varb_artikel, 3, 0);
    DbClass.sqlout ((long *) &prdk_zut.mat, 2, 0);
    DbClass.sqlout ((short *) &prdk_zut.variante, 1, 0);
    DbClass.sqlout ((double *) &me_ist, 3, 0);
    DbClass.sqlout ((short *)   &me_einh, 1, 0);
    zut_cursor1 = DbClass.sqlcursor ("select prdk_k.rez, a_mat.a,prdk_zut.mat, prdk_zut.variante, prdk_zut.zut_me, a_bas.me_einh from prdk_zut, a_bas, a_mat, prdk_k "
                                       "where prdk_k.mdn = ? "
                                       "and prdk_k.a  = ? "
                                       "and prdk_k.akv  = 1 "
                                       "and prdk_k.mdn  = prdk_zut.mdn "
                                       "and prdk_k.variante  = prdk_zut.variante "
                                       "and prdk_k.rez  = prdk_zut.rez "
                                       "and a_bas.a = a_mat.a "
									   "and a_mat.mat = prdk_zut.mat");
    return zut_cursor1;
}
int Work::PrepareZut2 (void)
{
    DbClass.sqlin ((short *)   &verbr_tag.mdn, 1, 0);
    DbClass.sqlin ((double *)    &verbr_tag.artikel, 3, 0);
    DbClass.sqlout ((char *) prdk_zut.rez, 0, 8);
    DbClass.sqlout ((double *) &varb_artikel, 3, 0);
    DbClass.sqlout ((long *) &prdk_zut.mat, 2, 0);
    DbClass.sqlout ((short *) &prdk_zut.variante, 1, 0);
    DbClass.sqlout ((double *) &me_ist, 3, 0);
    DbClass.sqlout ((short *)   &me_einh, 1, 0);
    zut_cursor2 = DbClass.sqlcursor ("select prdk_k.rez, a_mat.a,prdk_zut.mat, prdk_zut.variante, prdk_zut.zut_me, a_bas.me_einh from prdk_zut, a_bas, a_mat, prdk_k "
                                       "where prdk_k.mdn = ? "
                                       "and prdk_k.a  = ? "
                                       "and prdk_k.akv  = 1 "
                                       "and prdk_k.mdn  = prdk_zut.mdn "
                                       "and prdk_k.variante  = prdk_zut.variante "
                                       "and prdk_k.rez  = prdk_zut.rez "
                                       "and a_bas.a = a_mat.a "
									   "and a_mat.mat = prdk_zut.mat");
    return zut_cursor2;
}


int Work::OpenZut (short di)
{
 switch (di)
 {
        case 0 :
		    if (zut_cursor == -1)
			{
				zut_cursor = PrepareZut ();
		    }
		    return DbClass.sqlopen (zut_cursor);
			break;
			
        case 1 :
		    if (zut_cursor1 == -1)
			{
				zut_cursor1 = PrepareZut1 ();
		    }
		    return DbClass.sqlopen (zut_cursor1);
			break;
        case 2 :
		    if (zut_cursor2 == -1)
			{
				zut_cursor2 = PrepareZut2 ();
		    }
		    return DbClass.sqlopen (zut_cursor2);
			break;
 }
 return DbClass.sqlopen (zut_cursor);
}

/*
int Work::FetchZut (void)
{
 int dsqlstatus;
	    if (zut_cursor == -1)
		{
			zut_cursor = PrepareZut ();
		}
		dsqlstatus = DbClass.sqlfetch (zut_cursor);
		if (dsqlstatus != 0)
		{
			return dsqlstatus;
		}
	    prdk_zut.mdn      = verbr_tag.mdn;
	    return PrdkZut.dbreadfirst ();

}
*/
int Work::FetchZut (short di)
{
 int dsqlstatus;
 switch (di)
 {
        case 0 :
		    if (zut_cursor == -1)
			{
				zut_cursor = PrepareZut ();
			}
			dsqlstatus = DbClass.sqlfetch (zut_cursor);
			if (dsqlstatus != 0)
			{
				return dsqlstatus;
			}
			break;
        case 1 :
		    if (zut_cursor1 == -1)
			{
				zut_cursor1 = PrepareZut1 ();
			}
			dsqlstatus = DbClass.sqlfetch (zut_cursor1);
			if (dsqlstatus != 0)
			{
				return dsqlstatus;
			}
			break;
        case 2 :
		    if (zut_cursor2 == -1)
			{
				zut_cursor2 = PrepareZut2 ();
			}
			dsqlstatus = DbClass.sqlfetch (zut_cursor2);
			if (dsqlstatus != 0)
			{
				return dsqlstatus;
			}
			break;
 }
  prdk_zut.mdn      = verbr_tag.mdn;
  return dsqlstatus;

}

int Work::CloseZut (void)
{
    if (zut_cursor != -1)
    {
        DbClass.sqlclose (zut_cursor);
        zut_cursor = -1;
    }
    if (zut_cursor1 != -1)
    {
        DbClass.sqlclose (zut_cursor1);
        zut_cursor = -1;
    }
    if (zut_cursor2 != -1)
    {
        DbClass.sqlclose (zut_cursor2);
        zut_cursor = -1;
    }
    return 0;
}


int Work::PrepareHuel (void)
{
    DbClass.sqlin ((short *)   &verbr_tag.mdn, 1, 0);
    DbClass.sqlin ((double *)    &verbr_tag.artikel, 3, 0);
    DbClass.sqlout ((double *) &prdk_huel.a, 3, 0);
    DbClass.sqlout ((double *) &varb_artikel, 3, 0);
    DbClass.sqlout ((long *) &prdk_huel.mat, 2, 0);
    DbClass.sqlout ((short *) &prdk_huel.variante, 1, 0);
    DbClass.sqlout ((double *) &me_ist, 3, 0);
    DbClass.sqlout ((short *)   &me_einh, 1, 0);
    huel_cursor = DbClass.sqlcursor ("select prdk_k.a, a_mat.a,prdk_huel.mat, prdk_huel.variante, prdk_huel.me, a_bas.me_einh from prdk_huel, a_bas, a_mat, prdk_k "
                                       "where prdk_k.mdn = ? "
                                       "and prdk_k.a  = ? "
                                       "and prdk_k.akv  = 1 "
                                       "and prdk_k.mdn  = prdk_huel.mdn "
                                       "and prdk_k.variante  = prdk_huel.variante "
                                       "and prdk_k.a  = prdk_huel.a "
                                       "and a_bas.a = a_mat.a "
									   "and a_mat.mat = prdk_huel.mat");
    return huel_cursor;
}

int Work::PrepareHuel1 (void)
{
    DbClass.sqlin ((short *)   &verbr_tag.mdn, 1, 0);
    DbClass.sqlin ((double *)    &verbr_tag.artikel, 3, 0);
    DbClass.sqlout ((double *) &prdk_huel.a, 3, 0);
    DbClass.sqlout ((double *) &varb_artikel, 3, 0);
    DbClass.sqlout ((long *) &prdk_huel.mat, 2, 0);
    DbClass.sqlout ((short *) &prdk_huel.variante, 1, 0);
    DbClass.sqlout ((double *) &me_ist, 3, 0);
    DbClass.sqlout ((short *)   &me_einh, 1, 0);
    huel_cursor1 = DbClass.sqlcursor ("select prdk_k.a, a_mat.a,prdk_huel.mat, prdk_huel.variante, prdk_huel.me, a_bas.me_einh from prdk_huel, a_bas, a_mat, prdk_k "
                                       "where prdk_k.mdn = ? "
                                       "and prdk_k.a  = ? "
                                       "and prdk_k.akv  = 1 "
                                       "and prdk_k.mdn  = prdk_huel.mdn "
                                       "and prdk_k.variante  = prdk_huel.variante "
                                       "and prdk_k.a  = prdk_huel.a "
                                       "and a_bas.a = a_mat.a "
									   "and a_mat.mat = prdk_huel.mat");
    return huel_cursor1;
}
int Work::PrepareHuel2 (void)
{
    DbClass.sqlin ((short *)   &verbr_tag.mdn, 1, 0);
    DbClass.sqlin ((double *)    &verbr_tag.artikel, 3, 0);
    DbClass.sqlout ((double *) &prdk_huel.a, 3, 0);
    DbClass.sqlout ((double *) &varb_artikel, 3, 0);
    DbClass.sqlout ((long *) &prdk_huel.mat, 2, 0);
    DbClass.sqlout ((short *) &prdk_huel.variante, 1, 0);
    DbClass.sqlout ((double *) &me_ist, 3, 0);
    DbClass.sqlout ((short *)   &me_einh, 1, 0);
    huel_cursor2 = DbClass.sqlcursor ("select prdk_k.a, a_mat.a,prdk_huel.mat, prdk_huel.variante, prdk_huel.me, a_bas.me_einh from prdk_huel, a_bas, a_mat, prdk_k "
                                       "where prdk_k.mdn = ? "
                                       "and prdk_k.a  = ? "
                                       "and prdk_k.akv  = 1 "
                                       "and prdk_k.mdn  = prdk_huel.mdn "
                                       "and prdk_k.variante  = prdk_huel.variante "
                                       "and prdk_k.a  = prdk_huel.a "
                                       "and a_bas.a = a_mat.a "
									   "and a_mat.mat = prdk_huel.mat");
    return huel_cursor2;
}

int Work::OpenHuel (short di)
{
 switch (di)
 {
        case 0 :
		    if (huel_cursor == -1)
			{
				huel_cursor = PrepareHuel ();
		    }
		    return DbClass.sqlopen (huel_cursor);
			break;
			
        case 1 :
		    if (huel_cursor1 == -1)
			{
				huel_cursor1 = PrepareHuel1 ();
		    }
		    return DbClass.sqlopen (huel_cursor1);
			break;
        case 2 :
		    if (huel_cursor2 == -1)
			{
				huel_cursor2 = PrepareHuel2 ();
		    }
		    return DbClass.sqlopen (huel_cursor2);
			break;
 }
 return DbClass.sqlopen (huel_cursor);
}

/*
int Work::FetchHuel (void)
{
 int dsqlstatus;
	    if (huel_cursor == -1)
		{
			huel_cursor = PrepareHuel ();
		}
		dsqlstatus = DbClass.sqlfetch (huel_cursor);
		if (dsqlstatus != 0)
		{
			return dsqlstatus;
		}
	    prdk_huel.mdn      = verbr_tag.mdn;
	    return PrdkHuel.dbreadfirst ();

}
*/

int Work::FetchHuel (short di)
{
 int dsqlstatus;
 switch (di)
 {
        case 0 :
		    if (huel_cursor == -1)
			{
				huel_cursor = PrepareHuel ();
			}
			dsqlstatus = DbClass.sqlfetch (huel_cursor);
			if (dsqlstatus != 0)
			{
				return dsqlstatus;
			}
			break;
        case 1 :
		    if (huel_cursor1 == -1)
			{
				huel_cursor1 = PrepareHuel1 ();
			}
			dsqlstatus = DbClass.sqlfetch (huel_cursor1);
			if (dsqlstatus != 0)
			{
				return dsqlstatus;
			}
			break;
        case 2 :
		    if (huel_cursor2 == -1)
			{
				huel_cursor2 = PrepareHuel2 ();
			}
			dsqlstatus = DbClass.sqlfetch (huel_cursor2);
			if (dsqlstatus != 0)
			{
				return dsqlstatus;
			}
			break;
 }
  prdk_huel.mdn      = verbr_tag.mdn;
  return dsqlstatus;

}


int Work::CloseHuel (void)
{
    if (huel_cursor != -1)
    {
        DbClass.sqlclose (huel_cursor);
        huel_cursor = -1;
    }
    if (huel_cursor1 != -1)
    {
        DbClass.sqlclose (huel_cursor1);
        huel_cursor1 = -1;
    }
    if (huel_cursor2 != -1)
    {
        DbClass.sqlclose (huel_cursor2);
        huel_cursor2 = -1;
    }
    return 0;
}

int Work::PrepareVpk (void)
{
    DbClass.sqlin ((short *)   &verbr_tag.mdn, 1, 0);
    DbClass.sqlin ((double *)    &verbr_tag.artikel, 3, 0);
    DbClass.sqlout ((double *) &prdk_vpk.a, 3, 0);
    DbClass.sqlout ((double *) &varb_artikel, 3, 0);
    DbClass.sqlout ((double *) &prdk_vpk.a_vpk, 3, 0);
    DbClass.sqlout ((short *) &prdk_vpk.variante, 1, 0);
    DbClass.sqlout ((double *) &me_ist, 3, 0);
    DbClass.sqlout ((short *)   &me_einh, 1, 0);
    vpk_cursor = DbClass.sqlcursor ("select prdk_k.a, prdk_vpk.a_vpk,prdk_vpk.a_vpk, prdk_vpk.variante, prdk_vpk.me, a_bas.me_einh from prdk_vpk, a_bas, prdk_k "
                                       "where prdk_k.mdn = ? "
                                       "and prdk_k.a  = ? "
                                       "and prdk_k.akv  = 1 "
                                       "and prdk_k.mdn  = prdk_vpk.mdn "
                                       "and prdk_k.variante  = prdk_vpk.variante "
                                       "and prdk_k.a  = prdk_vpk.a "
									   "and a_bas.a = prdk_vpk.a_vpk");
    return vpk_cursor;
}

int Work::OpenVpk (void)
{
    if (vpk_cursor == -1)
	{
		vpk_cursor = PrepareVpk ();
    }
    return DbClass.sqlopen (vpk_cursor);
			
}

int Work::FetchVpk (void)
{
 int dsqlstatus;
	    if (vpk_cursor == -1)
		{
			vpk_cursor = PrepareVpk ();
		}
		dsqlstatus = DbClass.sqlfetch (vpk_cursor);
		if (dsqlstatus != 0)
		{
			return dsqlstatus;
		}
	    prdk_vpk.mdn      = verbr_tag.mdn;
	    return PrdkVpk.dbreadfirst ();

}

int Work::CloseVpk (void)
{
    if (vpk_cursor != -1)
    {
        DbClass.sqlclose (vpk_cursor);
        vpk_cursor = -1;
    }
    return 0;
}

int Work::WriteVerbrTag (short mdn, long dat, double a, short posi, double me, double chg_gew,short AufrufTiefe)
{
    verbr_tag.mdn = mdn; 
    verbr_tag.dat   = dat;
    verbr_tag.artikel   = a;
	double me_zusatz = 0.0; //280405
	double varb_gew = 0.0;
//100905 status wird nie     int status = PruefeStatus(LongToDat(dat, dats),a,mdn,posi,&me_zusatz);  //280405
	// 070604 jetzt immer chg_gew holen, da ich hier sonst nur kutter_gew im Zugriff habe !!
//070604 	if (chg_gew == 0.0) 
//    {
		DbClass.sqlin  ((short *) &mdn, 1, 0);
		DbClass.sqlin  ((double *) &a, 3, 0);
		DbClass.sqlout  ((double *) &chg_gew, 3, 0);
		DbClass.sqlout  ((double *) &varb_gew, 3, 0);
//010611 chg_gew -> varb_gew , wenn Grundlage f�r Chargengr��e nur die Menge der Verarbeitungsmaterialien sein soll
		int dsqlstatus = DbClass.sqlcomm ("select chg_gew, varb_gew from prdk_k "
                                      "where mdn = ? "
                                      "and a = ? "
									  "and akv = 1" );
		if (dsqlstatus != 0) return dsqlstatus;
		if (chg_gew == 0.0) return 1;
		if (atoi(ChargengroesseVarb) == 1) chg_gew = varb_gew; //010611
//	}
	OpenVarb (AufrufTiefe);
	int dnot_gbrt = 1;
	while (FetchVarb (AufrufTiefe) == 0)
    {
		if (varb_artikel == 6890.0)
		{
			int dxxx = 1;  
		}
		dnot_gbrt = 1;

	    memcpy (&verbr_tag, &verbr_tag_null, sizeof (verbr_tag));


	    verbr_tag.mdn = mdn;
	    verbr_tag.dat   = dat;
	    verbr_tag.artikel   = a;
		verbr_tag.prdk_stufe = AufrufTiefe; //wenn > 0 , dann Materialien aus Grundbr�t 
		strcpy(verbr_tag.bearb_info, clipped(prdk_varb.bearb_info));
//		if (prdk_varb.typ == 2) verbr_tag.prdk_stufe = 10 + AufrufTiefe; //nicht aufgel�stes Grundbr�t
		verbr_tag.a = varb_artikel;
		verbr_tag.typ = 10;
		if (prdk_varb.typ == 2) verbr_tag.typ = 11; //Grundbr�t
		VerbrTag.dbreadfirst ();
//        verbr_tag.me_ist = verbr_tag.me_ist + (me * (me_ist / chg_gew))  ; 
        verbr_tag.me_ist = verbr_tag.me_ist + ((me + me_zusatz) * (me_ist / chg_gew))  ; //280405
		verbr_tag.me_einh = me_einh;
		verbr_tag.prodabt = abt;
		VerbrTag.dbupdate ();
		if (prdk_varb.typ == 2)  //Grundbr�t
        {
//     		WritePrstatatagGbrt (mdn,dat,varb_artikel, (me * (me_ist / chg_gew))) ;
     		WritePrstatatagGbrt (mdn,dat,varb_artikel,posi, (me * (me_ist / chg_gew)),(me_zusatz * (me_ist / chg_gew))) ; //280405
			prstatatag.mdn = mdn;
			dnot_gbrt = WriteVerbrTag (mdn,dat,varb_artikel,posi, (me * (me_ist / chg_gew)),0.0,(AufrufTiefe + 1)) ;
        }
	}


    verbr_tag.mdn = mdn;
    verbr_tag.dat   = dat;
    verbr_tag.artikel   = a;
	OpenZut (AufrufTiefe);
	while (FetchZut (AufrufTiefe) == 0)
    {
	    memcpy (&verbr_tag, &verbr_tag_null, sizeof (verbr_tag));
	    verbr_tag.mdn = mdn;
	    verbr_tag.dat   = dat;
	    verbr_tag.artikel   = a;
		verbr_tag.prdk_stufe = AufrufTiefe; //wenn > 0 , dann Materialien aus Grundbr�t 
		verbr_tag.typ = 20; //Zutaten
		verbr_tag.a = varb_artikel;
		VerbrTag.dbreadfirst ();

//        verbr_tag.me_ist = verbr_tag.me_ist + (me * (me_ist / chg_gew))  ;
        verbr_tag.me_ist = verbr_tag.me_ist + ((me + me_zusatz) * (me_ist / chg_gew))  ;//280405
		verbr_tag.me_einh = me_einh;
		verbr_tag.prodabt = abt;

		VerbrTag.dbupdate ();
	}

    verbr_tag.mdn = mdn;
    verbr_tag.dat   = dat;
    verbr_tag.artikel   = a;
	OpenHuel (AufrufTiefe);
	while (FetchHuel (AufrufTiefe) == 0)
    {
	    memcpy (&verbr_tag, &verbr_tag_null, sizeof (verbr_tag));
	    verbr_tag.mdn = mdn;
	    verbr_tag.dat   = dat;
	    verbr_tag.artikel   = a;
		verbr_tag.prdk_stufe = AufrufTiefe; //wenn > 0 , dann Materialien aus Grundbr�t 
		verbr_tag.typ = 30; //Huellen
		verbr_tag.a = varb_artikel;
		VerbrTag.dbreadfirst ();

//        verbr_tag.me_ist = verbr_tag.me_ist + (me * (me_ist / chg_gew))  ;
        verbr_tag.me_ist = verbr_tag.me_ist + ((me + me_zusatz) * (me_ist / chg_gew))  ;//280405
		verbr_tag.me_einh = me_einh;
		verbr_tag.prodabt = abt;

		VerbrTag.dbupdate ();
	}

    verbr_tag.mdn = mdn;
    verbr_tag.dat   = dat;
    verbr_tag.artikel   = a;
	OpenVpk ();
	while (FetchVpk () == 0)
    {
	    memcpy (&verbr_tag, &verbr_tag_null, sizeof (verbr_tag));
	    verbr_tag.mdn = mdn;
	    verbr_tag.dat   = dat;
	    verbr_tag.artikel   = a;
		verbr_tag.prdk_stufe = 0; //wenn > 0 , dann Materialien aus Grundbr�t 
		verbr_tag.typ = 40; //Verpackung
		verbr_tag.a = varb_artikel;
		VerbrTag.dbreadfirst ();

//        verbr_tag.me_ist = verbr_tag.me_ist + (me * (me_ist / chg_gew))  ;
        verbr_tag.me_ist = verbr_tag.me_ist + ((me + me_zusatz) * (me_ist / chg_gew))  ;//280405
		verbr_tag.me_einh = me_einh;
		verbr_tag.prodabt = abt;

		VerbrTag.dbupdate ();
	}

    return 0;
}



int Work::WritePrstatatagGbrt (short mdn, long dat, double a,short posi, double me, double mezusatz)
{
	double chg_gew = (double) 0;
	double gew_chgk = (double) 0;;
	char cdat [12];
    prstatatag.mdn = mdn + 50;  //(50 ist der Grundbr�toffset !)
    prstatatag.a   = a;
    prstatatag.posi = posi;
    prstatatag.dat = dat;
	prstatatag.prodabt = abt;
	prstatatag.varb_ok = 0;
	prstatatag.zut_ok = 0;
	prstatatag.huell_ok = 0;
	prstatatag.charge_ok = 0;
	dlong_to_asc(dat,cdat);
	//250504 chg_gew -> kutter_gew
		DbClass.sqlin  ((short *) &mdn, 1, 0);
		DbClass.sqlin  ((double *) &a, 3, 0);
		DbClass.sqlout  ((double *) &chg_gew, 3, 0);
		DbClass.sqlout  ((char *) prstatatag.rez, 0, 9);
		int dsqlstatus = DbClass.sqlcomm ("select kutter_gew,rez from prdk_k "
                                      "where mdn = ? "
                                      "and a = ? "
									  "and akv = 1" );
		if (dsqlstatus != 0) return dsqlstatus;
		if (chg_gew < (double) 0.0001) return 1;

    if (me >= (double) 0.001 || me <= (double) 0,001)
    {
		prstatatag.me = 0;   //080604
		prstatatag.me_zusatz = 0.0; //300605
        PrStatatag.dbreadfirst ();
//        prstatatag.me = me + prstatatag.me ;
        prstatatag.me = me ; //aufsummiert wird im dbupdate selber!! 
        prstatatag.chg  = chg_gew;

        if (atoi(ChgOhneHuell) == 1) prstatatag.huell_ok = 3;  
 	    if (PruefZut(mdn,prstatatag.rez) == 100)
		{
			   prstatatag.zut_ok = 3;
		}

		prstatatag.vpk_ok = 3;
        dsqlstatus = PrStatatag.dbupdate ();
    }
	return dsqlstatus;
}
int Work::PruefeStatus (char *dat, double a, short mdn, short posi, double* me_zusatz)
{
	int status = 0;
	int dsqlstatus = 0;
				DbClass.sqlin  ((short *) &mdn, 1, 0);
				DbClass.sqlin  ((double *) &a, 3, 0);
				DbClass.sqlin  ((char *) dat, 0, 10);
				DbClass.sqlin  ((short *) &posi, 1, 0);
				DbClass.sqlout  ((short *) &status, 1, 0);
				DbClass.sqlout  ((double *) me_zusatz, 3, 0);
				dsqlstatus = DbClass.sqlcomm ("select charge_ok,me_zusatz from prstatatag "
                                      "where mdn = ? "
                                      "and a = ? "
									  "and dat = ? "
									  "and posi = ? "
									  "and charge_ok > 0");
				if (dsqlstatus != 0) return 0;
				return status;
}
int Work::PruefeGrundbraete (char *dat, double a, short posi, short mdn) //010705
{
	int status = 0;
	int dsqlstatus = 0;
	short charge_ok = 0;


	DbClass.sqlin  ((double *) &a, 3, 0);
	DbClass.sqlin  ((short *) &mdn, 1, 0);
	DbClass.sqlout  ((short *) &prdk_k.variante, 1, 0);
    dsqlstatus = DbClass.sqlcomm ("select variante from prdk_k "
                                      "where a = ? "
									  "and mdn = ? "
									  "and akv = 1 ");

	if (dsqlstatus == 0)
	{
		prdk_k.mdn = mdn;
		short MdnGbrt = mdn + 50;
		prdk_k.a = a;
		Prdk_k.dbreadfirst();
		verbr_tag.mdn = mdn;
		verbr_tag.artikel = a;
		OpenVarb (0);
		while (FetchVarb (0) == 0)
	    {
			if (prdk_varb.typ == 2)  //Grundbr�t
	        {
				DbClass.sqlin  ((short *) &MdnGbrt, 1, 0);
				DbClass.sqlin  ((double *) &varb_artikel, 3, 0);
				DbClass.sqlin  ((short *) &posi, 1, 0);
				DbClass.sqlin  ((char *) dat, 0, 11);
				DbClass.sqlout ((short *) &charge_ok, 1, 0);
				dsqlstatus = DbClass.sqlcomm ("select charge_ok from prstatatag "
                                      "where mdn = ? "
									  "and a = ? "
									  "and posi = ? "
									  "and dat = ? ");
				if (dsqlstatus != 0)
				{
					status = dsqlstatus;
					break;
				}
				if (charge_ok > 0) 
				{
					status = 10;
					break;
				}

			}
				
		}
	}
	else
	{
		status = dsqlstatus;
	}
	CloseVarb();
	Prdk_k.dbclose();

	return status;
}

int Work::MaschStatusZurueck (char *dat, double a, short mdn) //041105 bgut1
{
				DbClass.sqlin  ((double *) &a, 3, 0);
				DbClass.sqlin  ((char *) dat, 0, 10);
				DbClass.sqlin  ((short *) &mdn, 1, 0);
				return DbClass.sqlcomm ("update prstatmasch set status = 0 "
                                      "where a = ? "
                                      "and dat = ? "
									  "and mdn = ? "
									  "and status > 0 ");
}

int Work::SchreibeZusatz (char *dat, double a, short mdn, double menge)
{
	int status = 0;
	int dsqlstatus = 0;
	short varb_ok = 0; 
	short zut_ok = 0; 
	short huell_ok = 0; 
				DbClass.sqlin  ((short *) &mdn, 1, 0);
				DbClass.sqlin  ((double *) &a, 3, 0);
				DbClass.sqlin  ((char *) dat, 0, 10);
				DbClass.sqlout  ((short *) &varb_ok, 1, 0);
				DbClass.sqlout  ((short *) &zut_ok, 1, 0);
				DbClass.sqlout  ((short *) &huell_ok, 1, 0);
				dsqlstatus = DbClass.sqlcomm ("select varb_ok,zut_ok,huell_ok from prstatatag "
                                      "where mdn = ? "
                                      "and a = ? "
									  "and dat = ? ");
				if (dsqlstatus != 0) return dsqlstatus;
				if (varb_ok == 2) varb_ok = 1;
				if (zut_ok == 2) zut_ok = 1;
				if (huell_ok == 2) huell_ok = 1;

				DbClass.sqlin  ((double *) &menge, 3, 0);
				DbClass.sqlin  ((short *) &varb_ok, 1, 0);
				DbClass.sqlin  ((short *) &zut_ok, 1, 0);
				DbClass.sqlin  ((short *) &huell_ok, 1, 0);
				DbClass.sqlin  ((short *) &mdn, 1, 0);
				DbClass.sqlin  ((double *) &a, 3, 0);
				DbClass.sqlin  ((char *) dat, 0, 10);
				dsqlstatus = DbClass.sqlcomm ("update prstatatag set charge_ok = 9, me_zusatz = ?, "
									  "varb_ok = ?, zut_ok = ?, huell_ok = ? "
                                      "where mdn = ? "
                                      "and a = ? "
									  "and dat = ? ");
				if (dsqlstatus != 0) return dsqlstatus;
				return status;
}

int Work::PruefZut (short mdn, char *rez)
//sind Zutaten in der Rezeptur vorhanden ??
{
    DbClass.sqlin ((long *)  &mdn, 1, 0);
    DbClass.sqlin ((char *) rez,   0,9);
    DbClass.sqlin ((long *)  &mdn, 1, 0);
    DbClass.sqlin ((char *) rez,   0,9);
    return DbClass.sqlcomm ("select rez from prdk_zut where mdn = ? and rez = ? and variante in "
				" (select variante from prdk_k where mdn = ? and rez = ? and akv = 1)");
}

void Work::Upd99 (void) //040705
{
//030805       DbClass.sqlcomm ("update prstatatag set charge_ok = 9, me_zusatz = me_zusatz - me  where charge_ok = 99 ");
	
//020905        DbClass.sqlcomm ("update prstatatag set me_zusatz = me_zusatz - me  where charge_ok = 99 "); //030805
//020905        DbClass.sqlcomm ("update prstatatag set charge_ok = 9  where charge_ok = 99 and me_zusatz > 0.001 "); //030805

}

double Work::Round (double value, int nk)
{
    Text Frm;
    Text SValue;

    Frm.Format ("%c.%dlf", '%',nk);
    SValue.Format (Frm.GetBuffer (), value);
    value = ratod (SValue.GetBuffer ());
    return value;
}

void Work::HoleAuftraege (void)
{
	int auf_cursor;
	short me_einh, vorlaufzeit;
	double lief_me;
	long Datum ;
     DbClass.sqlin ((long *)   &DatVon, 2, 0);
     DbClass.sqlin ((long *)   &DatBis, 2, 0);
    DbClass.sqlin ((char *)   &prstatatag.mdn, 1, 0);
    DbClass.sqlout ((double *)   &prstatatag.a, 3, 0);
    DbClass.sqlout ((double *)   &lief_me, 3, 0);
    DbClass.sqlout ((short *)   &me_einh, 1, 0);
    DbClass.sqlout ((short *)   &vorlaufzeit, 1, 0);
    DbClass.sqlout ((long *)   &Datum, 2, 0);
    auf_cursor = DbClass.sqlcursor ("select aufp.a,aufp.lief_me,aufp.me_einh,prdk_k.vorlaufzeit,aufk.lieferdat from aufk,aufp,prdk_k "
					"where aufk.lieferdat between ? + prdk_k.vorlaufzeit and ? + prdk_k.vorlaufzeit "
					"and prdk_k.akv = 1 and prdk_k.a = aufp.a "
					"and aufp.mdn = aufk.mdn and aufp.auf = aufk.auf and aufk.mdn = ? and prdk_k.mdn = aufk.mdn ");

    while (DbClass.sqlfetch (auf_cursor) == 0)
    {
           prstatatag.mdn = prstatatag.mdn;
           prstatatag.a   = prstatatag.a;
           prstatatag.posi = 0;
           prstatatag.dat = Datum - vorlaufzeit;
		   prstatatag.prodabt = abt;
		   prstatatag.auf_me = GetArtGew(prstatatag.a,lief_me,me_einh);
   		   prstatatag.vpk_ok = 3;
		   PrStatatag.dbupdate ();

    }
    DbClass.sqlclose (auf_cursor);
}

double Work::GetArtGew (double a, double lief_me, short me_einh )
{
          double a_gew;
          char buffer [512];
          
          if (me_einh == 2)
          {
                    return lief_me;   
          }

          a_gew = 0.0;
          sprintf (buffer, "select a_gew from a_bas where a = %.0lf", a);
          DbClass.sqlout ((double *) &a_gew, 3, 0);
          DbClass.sqlcomm (buffer);
          if (a_gew == (double) 0.0)
          {
              a_gew = 1.0;
          }

          return a_gew * lief_me;
}

void Work::ins_woplan_del (double a)
{
	woplan_del.mdn = prstatatag.mdn;
	woplan_del.jr = JR;
	woplan_del.kw = KW;
	woplan_del.a = a;
	WoplanDel.dbupdate();
}

void Work::loesch_woplan_del (void)
{
	woplan_del.mdn = prstatatag.mdn;
	woplan_del.jr = JR;
	woplan_del.kw = KW;
    DbClass.sqlin ((short *) &woplan_del.mdn, 1, 0);
    DbClass.sqlin ((short *) &woplan_del.jr, 1, 0);
    DbClass.sqlin ((short *) &woplan_del.kw, 1, 0);
    DbClass.sqlcomm ("delete from woplan_del where mdn = ? and jr = ? and kw = ?");
}


short Work::HoleStnd (short wochentag)
{
    char buffer [512];
	short plan = 0;
		if (strupcmp (ProdAbt, "Standardplan", strlen (ProdAbt)) == 0)
		{
          sprintf (buffer, "select plan from plkopf where wo_tag  = %hd", wochentag);
          DbClass.sqlout ((short *) &plan, 1, 0);
          DbClass.sqlcomm (buffer);
		  return plan;
		}
		return 0;
}


//Erfassungsliste 

void Work::FillErfRow (void)
{
    PRDK_K_CLASS Prdk_k;
	char a_bez [50];

    if (ErfLsts == NULL) return;

    sprintf (ErfLsts->a,     "%6.0f", prstatatag.a);
    ErfLsts->PrStatMe = prstatatag.me;
    sprintf (a_bez, "%s %s",clipped(_a_bas.a_bz1),clipped(_a_bas.a_bz2)); //041105
    sprintf (ErfLsts->a_bz1, "%s",    a_bez);
    sprintf (ErfLsts->chg,   "%.3lf", prdk_k.kutter_gew); //250504
    ErfLsts->posi = prstatatag.posi; //300905
	sprintf(ErfLsts->me_einh, "%14s", _a_bas.a_bz3);
    prdk_k.mdn = prstatatag.mdn;
    prdk_k.a = _a_bas.a;


    sprintf (ErfLsts->me,    "%.3lf", prstatatag.me);


    ErfLsts->cha.SetFeld ("..");
}


void Work::InitErfRow (void)
{
    if (ErfLsts == NULL) return;     
}


void Work::WriteErfRec (void)
{
    if (ErfLsts == NULL) return;

//    prstatmasch.phase     = AktPhase;



		    memcpy (&prstatatag, &prstatatag_null, sizeof (prstatatag)); //030210
           prstatatag.mdn = _mdn.mdn;
           prstatatag.a   = ratod (ErfLsts->a);
           prstatatag.dat = AktDat;
           prstatatag.prodabt = abt;
           prstatatag.posi = ErfLsts->posi;
		   prstatatag.me = 0.0  ;

           PrStatatag.dbreadfirst ();
		   if (sqlstatus == 100)
		   {
			   prstatatag.charge_ok = 0;
			   prstatatag.varb_ok = 0;
			   prstatatag.zut_ok = 0;
			   prstatatag.huell_ok = 0;
			   prstatatag.vpk_ok = 0;
		   }
		   strcpy (prstatatag.rez,ErfLsts->rez);
		   double menge_ist = prstatatag.me;
           prstatatag.me = ratod(ErfLsts->me)- menge_ist;  //beim update wird aufaddiert!!
		   if (prstatatag.a > 0.0)
		   {
			   if (prstatatag.me + menge_ist != 0.0)
			   {
					PrStatatag.dbupdateerf ();
			   }
		   }
		   else
		   {
				PrStatatag.dbdelete (); 
		   }
            prdk_k.chargierung = 1;
		    PrStatchgk->sqlin ((short *)  &prstatatag.mdn, 1, 0);
			PrStatchgk->sqlin ((double *) &prstatatag.a, 3, 0);
			PrStatchgk->sqlout ((short *) &prdk_k.chargierung, 1, 0);
			PrStatchgk->sqlout ((double *) &prdk_k.kutter_gew, 3, 0);
			int dsqlstatus = PrStatchgk->sqlcomm ("select chargierung,kutter_gew from prdk_k "
                                         "where mdn = ? " 
                                         "and a = ? "
                                         "and akv = 1");

			WriteVerbrTag (prstatatag.mdn,prstatatag.dat,prstatatag.a,prstatatag.posi,prstatatag.me + menge_ist,prdk_k.kutter_gew,0); //050207





}


int Work::PrepareErf (void)
{
//    vpk_cursor = Prepare ();
     DbClass.sqlin ((short *)   &prstatatag.mdn, 1, 0);
     DbClass.sqlin ((short *)   &abt, 1, 0);
     DbClass.sqlin ((long *)   &AktDat, 2, 0);
     DbClass.sqlout ((short *)  &prstatatag.posi, 1, 0);
     DbClass.sqlout ((short *)   &_a_bas.ag, 1, 0);
     DbClass.sqlout ((double *) &_a_bas.a, 3, 0);
     DbClass.sqlout ((double *) &prstatatag.a, 3, 0);
     DbClass.sqlout ((char *)   _a_bas.a_bz1, 0, 25);
     DbClass.sqlout ((char *)   _a_bas.a_bz2, 0, 25); 
     DbClass.sqlout ((char *)   _a_bas.a_bz3, 0, 25);    //me_einh
		erf_cursor = DbClass.sqlcursor ("select unique(prstatatag.posi), a_bas.ag, a_bas.a, a_bas.a, a_bas.a_bz1, a_bas.a_bz2, "
								 "ptabn.ptbezk "
                                 "from a_bas, prstatatag, ptabn "
                                 "where prstatatag.mdn = ? "
                                 "and prstatatag.prodabt = ? "
								 "and prstatatag.a = a_bas.a "
								 "and prstatatag.dat = ? "
								 "and prstatatag.me > 0 "
								 "and ptabn.ptitem = \"me_einh\" and ptabn.ptlfnr = a_bas.me_einh "
                                 "order by a_bas.a");
    return erf_cursor;
}

int Work::OpenErf (void)
{
    if (erf_cursor == -1)
    {
        PrepareErf ();
        if (erf_cursor == -1)
        {
            return 0;
        }
    }
    DbClass.sqlopen (erf_cursor);
    return 0;
}

int Work::FetchErf (void)
{
    if (erf_cursor == -1)
    {
        PrepareErf ();
        if (erf_cursor == -1)
        {
            return 0;
        }
    }

    short mdn = _mdn.mdn;
    memcpy (&prstatatag, &prstatatag_null, sizeof (prstatatag));
    prstatatag.mdn = mdn;
    int dsqlstatus = DbClass.sqlfetch (erf_cursor);
    if (dsqlstatus == 0)
    {
        prstatatag.a   = _a_bas.a;
        prstatatag.dat = AktDat;
	    prstatatag.prodabt = abt; //300905
        if (PrStatatag.dbreadfirst () != 0)
        {
            memcpy (&prstatatag, &prstatatag_null, sizeof (prstatatag));
//            prstatatag.mdn = mdn;
//            prstatatag.a   = 0.0;
//            prstatatag.dat = AktDat;
        }
    }

    return dsqlstatus;
}

int Work::CloseErf (void)
{
    if (erf_cursor != -1)
    {
        Close ();
        erf_cursor = -1;
    }
    return 0;
}


void Work::DeleteAllMe0 (void)
{ 
	Prdk_k.sqlin ((short *) &_mdn.mdn, 1, 0);
    DbClass.sqlin  ((long *)   &AktDat, 2, 0);
    DbClass.sqlin  ((short *) &abt, 1, 0);
    Prdk_k.sqlcomm ("delete from prstatatag "
                    "where mdn = ? "
                    "and dat = ? "
                    "and prodabt = ? and me = 0 and charge_ok = 0");
}
void Work::SetzeMenge0 (void)
{ 
	Prdk_k.sqlin ((short *) &_mdn.mdn, 1, 0);
    DbClass.sqlin  ((long *)   &AktDat, 2, 0);
    DbClass.sqlin  ((short *) &abt, 1, 0);
    Prdk_k.sqlcomm ("update prstatatag  set me = 0"
                    "where mdn = ? "
                    "and dat = ? "
                    "and prodabt = ? and charge_ok = 0");
}

BOOL Work::GetErfMatName (void)
{
    double a_erf;
    double a;
	int dsqlstatus;

    a_erf = ratod (ErfLsts->a);
    if (a_erf == 0l)
    {
        return FALSE;
    }

	strcpy (ErfLsts->a_bz1, "");
	if (abt == 0)
	{
		Prdk_k.sqlin ((double *) &a_erf, 3, 0);
		Prdk_k.sqlout ((double *) &a, 3, 0);
		Prdk_k.sqlout ((char *) ErfLsts->a_bz1, 0, 25);
		dsqlstatus = Prdk_k.sqlcomm ("select a_bas.a, a_bz1 from a_bas "
                                     "where a_bas.a = ? "
                                     "and a in (select prdk_k.a from prdk_k where akv = 1 ) ");
	}
	else
	{
		Prdk_k.sqlin ((double *) &a_erf, 3, 0);
		Prdk_k.sqlin ((short *) &abt, 1, 0);
		Prdk_k.sqlout ((double *) &a, 3, 0);
		Prdk_k.sqlout ((char *) ErfLsts->a_bz1, 0, 25);
		dsqlstatus = Prdk_k.sqlcomm ("select a_bas.a, a_bz1 from a_bas "
                                     "where a_bas.a = ? "
                                     "and a in (select prdk_k.a from prdk_k where akv = 1 and prod_abt = ? ) ");
	}
    if (dsqlstatus != 0)
    {
        return FALSE;
    }

    return TRUE;
}

void Work::GetErfAbz1 (char *a_bz1, char *me_einh,double *inh_prod, double *a_gew, double a_erf,char *rez)
{
	char prod_einh[5];
	double a = 0.0;
    strcpy (a_bz1, "");
    strcpy (rez, "");
    if (a_erf == 0.0)
    {
        return;
    }
    Prdk_k.sqlin ((long *) &a_erf, 3, 0);
    Prdk_k.sqlin ((short *) &prdk_k.mdn, 1, 0);
    Prdk_k.sqlout ((double *) &a, 3, 0);
    Prdk_k.sqlout ((char *) a_bz1, 0, 25);
    Prdk_k.sqlout ((char *) me_einh, 0, 5);
    Prdk_k.sqlout ((double *) a_gew, 3, 0);
    Prdk_k.sqlout ((char *) prod_einh, 0, 5);
    Prdk_k.sqlout ((double *) inh_prod, 3, 0);
    Prdk_k.sqlout ((char *) rez, 0, 9);
    Prdk_k.sqlcomm ("select a_bas.a, a_bas.a_bz1, a_bas.me_einh, a_bas.a_gew ,a_bas.lief_einh, a_bas.inh_lief, prdk_k.rez from a_bas, prdk_k "
                    "where a_bas.a = ? "
					"and prdk_k.a = a_bas.a "
					"and prdk_k.akv = 1 "
					"and prdk_k.mdn = ? ");
	if (strcmp(prod_einh,me_einh) != 0)
    {
		if (prod_einh != 0l) 
        {  
			strcpy(me_einh,prod_einh);
		}
		else
        {
			*inh_prod = 1.0;
		}
    }
	else
    {
		*inh_prod = 1,0;
    }
	sprintf(ErfLsts->a, "%13.0lf", a);

}

