#include <windows.h>
#include "wmaskc.h"
#include "searcherf.h"
#include "strfkt.h"
#include "mo_wmess.h"
#include "lbox.h"
#include "dlglst.h"
#include "a_bas.h" 


struct SERF *SEARCHERF::serftab = NULL;
struct SERF SEARCHERF::serf;
int SEARCHERF::idx = -1;
long SEARCHERF::erfanz;
CHQEX *SEARCHERF::Query = NULL;
DB_CLASS SEARCHERF::DbClass; 
HINSTANCE SEARCHERF::hMainInst;
HWND SEARCHERF::hMainWindow;
HWND SEARCHERF::awin;
int SEARCHERF::SearchField = 1;
int SEARCHERF::somat = 1; 
int SEARCHERF::soa = 1; 
int SEARCHERF::soa_bz1 = 1; 
short SEARCHERF::mdn = 0; 


ITEM SEARCHERF::ua        ("a",         "ArtikelNr.",    "", 0);  
ITEM SEARCHERF::umat      ("mat",       "MaterialNr.",   "", 0);  
ITEM SEARCHERF::ua_bz1    ("a_bz1" ,    "Bezeichnung",   "", 0);  


field SEARCHERF::_UbForm[] = {
&ua,        15, 0, 0,  0,  NULL, "", BUTTON, 0, 0, 0,     
&ua_bz1,    26, 0, 0, 15 , NULL, "", BUTTON, 0, 0, 0,     
};

form SEARCHERF::UbForm = {2, 0, 0, _UbForm, 0, 0, 0, 0, NULL};

ITEM SEARCHERF::ia        ("a",         serf.a,      "", 0);  
ITEM SEARCHERF::imat      ("mat",       serf.mat,    "", 0);  
ITEM SEARCHERF::ia_bz1    ("a_bz1" ,    serf.a_bz1,  "", 0);  

field SEARCHERF::_DataForm[] = {
&ia,        13, 0, 0,  1,  NULL, "%13.0lf", DISPLAYONLY, 0, 0, 0,     
&ia_bz1,    24, 0, 0, 17 , NULL, "",        DISPLAYONLY, 0, 0, 0,     
};

form SEARCHERF::DataForm = {2, 0, 0, _DataForm, 0, 0, 0, 0, NULL};

ITEM SEARCHERF::iline ("", "1", "", 0);

field SEARCHERF::_LineForm[] = {
&iline,       1, 0, 0, 15 , NULL, "", DISPLAYONLY, 0, 0, 0,     
};

form SEARCHERF::LineForm = {1, 0, 0, _LineForm, 0, 0, 0, 0, NULL};

char *SEARCHERF::query = NULL;

int SEARCHERF::sortmat (const void *elem1, const void *elem2)
{
	      struct SERF *el1; 
	      struct SERF *el2; 

		  el1 = (struct SERF *) elem1;
		  el2 = (struct SERF *) elem2;
	      return ((atol (el1->mat) - atol (el2->mat)) * somat);
}

void SEARCHERF::SortMat (HWND hWnd)
{
   	   qsort (serftab, erfanz, sizeof (struct SERF),
				   sortmat);
       somat *= -1;
}


int SEARCHERF::sorta (const void *elem1, const void *elem2)
{
	      struct SERF *el1; 
	      struct SERF *el2; 

		  el1 = (struct SERF *) elem1;
		  el2 = (struct SERF *) elem2;
          clipped (el1->a);
          clipped (el2->a);

          if (ratod (el1->a) > ratod (el2->a))
          {
              return 1 * soa;
          }
          else if (ratod (el1->a) < ratod (el2->a))
          {
              return -1 * soa;
          }

          return 0;
}

void SEARCHERF::SortA (HWND hWnd)
{
   	   qsort (serftab, erfanz, sizeof (struct SERF),
				   sorta);
       soa *= -1;
}

int SEARCHERF::sorta_bz1 (const void *elem1, const void *elem2)
{
	      struct SERF *el1; 
	      struct SERF *el2; 

		  el1 = (struct SERF *) elem1;
		  el2 = (struct SERF *) elem2;
          clipped (el1->a_bz1);
          clipped (el2->a_bz1);
          if (strlen (el1->a_bz1) == 0 &&
              strlen (el2->a_bz1) == 0)
          {
              return 0;
          }
          if (strlen (el1->a_bz1) == 0)
          {
              return -1;
          }
          if (strlen (el2->a_bz1) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->a_bz1,el2->a_bz1) * soa_bz1);
}

void SEARCHERF::SortABz1 (HWND hWnd)
{
   	   qsort (serftab, erfanz, sizeof (struct SERF),
				   sorta_bz1);
       soa_bz1 *= -1;
}


void SEARCHERF::SortLst (int Col, HWND hWnd)
/**
Procedure zum Sortiern der Liste.
**/
{
       switch (Col)
       {
              case 0 :
                  SortA (hWnd);
                  SearchField = 0;
                  break;
              case 1 :
                  SortMat (hWnd);
                  SearchField = 1;
                  break;
              case 2 :
                  SortABz1 (hWnd);
                  SearchField = 2;
                  break;
              default :
                  return;
       }
       UpdateList ();
}

void SEARCHERF::FillFormat (char *buffer, int size)
{
        DlgLst::FillFrmAttr (&DataForm, buffer, size);
}


void SEARCHERF::FillCaption (char *buffer, int size)
{
        DlgLst::FillFrmTitle (&UbForm, buffer, 0, size);
}

void SEARCHERF::FillVlines (char *buffer, int size) 
{
        DlgLst::FillFrmLines (&LineForm, buffer, 0, size);
}


void SEARCHERF::FillRec (char *buffer, int i, int size)
{
          DlgLst::FillFrmData (&DataForm, buffer, 0, size);
}


void SEARCHERF::UpdateList (void)
{
       int i;
 	   char buffer [512];

       for (i = 0; i < erfanz; i ++)
       {
		  memcpy (&serf, &serftab[i],  sizeof (struct SERF));
          FillRec (buffer, i, 512);
	      Query->UpdateRecord (buffer, i);
       }
}

int SEARCHERF::SearchLst (char *sebuff)
/**
Nach Kurzname in Liste suchen.
**/
{
/*
	   Query->SearchList ();
	   return 0;
*/
	   int i;
	   int len;
       char sabuff [80];
       char *pos;

	   if (serftab == NULL) return 0;
	   if (strlen (sebuff) == 0) 
	   {
	       Query->SetSel (0);
		   return 0;
	   }

	   for (i = 0; i < erfanz; i ++)
	   {
		   len = min (16, strlen (sebuff));
           if (SearchField == 0)
           {
                   strcpy (sabuff, serftab[i].a);
                   for (pos = sabuff; *pos <= ' ' && *pos != 0 ; pos ++);
                   pos[len] = 0;
                   strcpy (sabuff, pos);
                   if (ratod (sebuff) == ratod (sabuff)) break;
           }
           else if (SearchField == 1)
           {
                   strcpy (sabuff, serftab[i].mat);
                   for (pos = sabuff; *pos <= ' ' && *pos != 0 ; pos ++);
                   pos[len] = 0;
                   strcpy (sabuff, pos);
                   if (atol (sebuff) == atol (sabuff)) break;
           }
           else if (SearchField == 2)
           {
		           if (strupcmp (sebuff, serftab[i].a_bz1, len) == 0) break;
           }
	   }
	   if (i == erfanz) return 0;
	   Query->SetSel (i);
	   return 0;
}


int SEARCHERF::Read (char *squery)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int cursor;
	  int i;
      WMESS Wmess;


	  if (serftab) 
	  {
           delete serftab;
           serftab = NULL;
	  }


      idx = -1;
	  erfanz = 0;
      SearchField = 2;
      Query->SetSBuff (NULL);
      clipped (squery);
      if (strcmp (squery, " ") == 0) squery[0] = 0;

      if (erfanz == 0) erfanz = 0x10000;

	  serftab = new struct SERF [erfanz];
      if (squery == NULL || 
          strlen (squery) == 0)
      {
             sprintf (buffer, "select a_bas.a, a_bz1 "
	 			       "from a_bas,a_mat "
                       "where a_bas.a > 0 and a_bas.a = a_mat.a "
                       "and a_bas.a in (select prdk_k.a from prdk_k where akv = 1 )");

              if (query != NULL && strlen (query) > 0)
              {
                  strcat (buffer, " ");
                  strcat (buffer, query);
              }

              DbClass.sqlout ((char *) serf.a, 0, 14);
              DbClass.sqlout ((char *) serf.a_bz1, 0, 25);
              cursor = DbClass.sqlcursor (buffer);
      }
      else
      {
             sprintf (buffer, "select a_bas.a, a_bz1 "
	 			       "from a_bas,a_mat "
                       "where a_bas.a > 0 and a_bas.a = a_mat.a "
                       "and a_bas.a in (select prdk_k.a from prdk_k where akv = 1 ) "
                       "and a_bez matches \"%s*\"", squery); 
              if (query != NULL && strlen (query) > 0)
              {
                  strcat (buffer, " ");
                  strcat (buffer, query);
              }
              DbClass.sqlout ((char *) serf.a, 0, 14);
//              DbClass.sqlout ((char *) serf.mat, 0, 9);
              DbClass.sqlout ((char *) serf.a_bz1, 0, 25);
              cursor = DbClass.sqlcursor (buffer);
      }
      if (cursor < 0) 
      {
          return -1;
      }

  	  i = 0;
	  while (DbClass.sqlfetch (cursor) == 0)
      {
		  memcpy (&serftab[i], &serf, sizeof (struct SERF));
          FillRec (buffer, i, 512);
	      Query->InsertRecord (buffer);
		  i ++;
	  }
      erfanz = i;
	  DbClass.sqlclose (cursor);

      soa_bz1 = 1;
      if (erfanz > 1)
      {
             SortABz1 (Query->GethWnd ());
      }
      UpdateList ();

	  EnableWindow (awin, FALSE);
	  return 0;
}

void SEARCHERF::FillBox (void)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int i;

      for (i = 0; i < erfanz; i ++)
      {
		  memcpy (&serf, &serftab[i],  sizeof (struct SERF));
          FillRec (buffer, i, 512);
	      Query->InsertRecord (buffer);
      }
      if (idx > -1)
      {
	      Query->SetSel (idx);
      }
}


void SEARCHERF::SetParams (HINSTANCE hMainInst, HWND hMainWindow)
{
      this->hMainInst   = hMainInst;
      this->hMainWindow = hMainWindow;
      awin = hMainWindow;
}


void SEARCHERF::Search (void)
{
  	  int cx, cy;
	  char buffer [512];
      RECT rect;
      BOOL doret = FALSE; 

      DlgLst::TChar = 0xFF;
      Settchar ((char) DlgLst::TChar);
      GetClientRect (hMainWindow, &rect);
      cx = 80;
      cy = 25;
//      EnableWindow (hMainWindow, FALSE);
      SetSortProc (SortLst);
      if (Query == NULL)
      {
        idx = -1;
        if (query != NULL && strlen (query) > 0)
        {
               Query = new CHQEX (cx, cy);
        }
        else
        {
               Query = new CHQEX (cx, cy);
//               Query = new CHQEX (cx, cy, "Name", "");
        }
        Query->EnableSort (TRUE);
        if (SetStyle)
        {
            Query->SetWindowStyle (WindowStyle);
        }
        Query->OpenWindow (hMainInst, hMainWindow, TRUE);
        FillFormat (buffer, 512);
        Query->RowAttr (buffer);
        FillVlines (buffer, 512);
	    Query->VLines (buffer, 3);
	    EnableWindow (awin, FALSE);

        FillCaption (buffer, 512);
	    Query->InsertCaption (buffer);

	    Query->SetFillDb (Read);
	    Query->SetSearchLst (SearchLst);
//        if (query != NULL && strlen (query) > 0)
        {
              Read ("");
              if (erfanz == 0)
              {
                  doret = TRUE;
              }
        }
      }
      else
      {
        Query->RestorefWork ();
        if (SetStyle)
        {
            Query->SetWindowStyle (WindowStyle);
        }
        Query->OpenWindow (hMainInst, hMainWindow, TRUE);
        FillFormat (buffer, 512);
        Query->RowAttr (buffer);
        FillVlines (buffer, 512);
	    Query->VLines (buffer, 3);
	    EnableWindow (awin, FALSE);

        FillCaption (buffer, 512);
	    Query->InsertCaption (buffer);

	    Query->SetFillDb (Read);
	    Query->SetSearchLst (SearchLst);
        FillBox ();
      }
      if (doret == FALSE)
      {
          Query->ProcessMessages ();
          idx = Query->GetSel ();
      }
      else
      {
          idx = -1;
      }
      query = NULL;
      EnableWindow (awin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (awin);
      if (serftab == NULL)
      {
          delete Query;
          Query = NULL;
      }
	  if (idx == -1) return;
	  memcpy (&serf, &serftab[idx], sizeof (serf));
      SetSortProc (NULL);
      if (serftab != NULL)
      {
          Query->SavefWork ();
      }
}



