#ifndef _PHASELST_DEF
#define _PHASELST_DEF
#include "listenter.h"
#include "listclex.h"
#include "work.h"
#include "mo_progcfg.h"

#define SMALL 0
#define BIG 1
 
class PHASELST : public virtual LISTENTER, 
                public virtual KEYS,
                public virtual Info
{
         private :
                      static CFIELD *_fChoise0[];
                      static CFORM fChoise0;
                      static CFIELD *_fChoise[];
                      static CFORM fChoise;

                      int cursor;
                      CFORM *Toolbar2;
                      BOOL cfgOK;
                      PROG_CFG *ProgCfg;
                      HBITMAP ArrDown;
                      long AktDat;
                      double oldme2;
                      static short kw;
                      static short jr;
         public :

                     static Work *work;
                     static struct CHATTR ChAttra[];
                     static struct CHATTR *ChAttr;
                     static struct PHASELSTS Lsts;
                     static struct PHASELSTS *LstTab;
                     static ITEM ua;
                     static ITEM ua_bz1;
                     static ITEM uchg;
                     static ITEM ume;
                     static ITEM ufolge;
                     static ITEM ume2;
                     static ITEM ufolge2;
                     static ITEM umasch_nr2;
                     static ITEM ume3;
                     static ITEM ufolge3;
                     static ITEM umasch_nr3;
                     static ITEM ufiller;
                     static ITEM *MaschTab[];
                     static field _UbForm[];
                     static form UbForm;
                     static int UbAnz;

                     static ITEM iline;
                     static field _LineForm[];
                     static form LineForm;
                     static int LineAnz;

                     static ITEM ia;
                     static ITEM ia_bz1;
                     static ITEM ichg;
                     static ITEM ime;
                     static ITEM ifolge;
                     static ITEM ime2;
                     static ITEM ifolge2;
                     static ITEM imasch_nr2;
                     static ITEM ime3;
                     static ITEM ifolge3;
                     static ITEM imasch_nr3;
                     static ITEM ichmasch_nr;
                     static ITEM ichmasch_nr3;
                     static field _DataForm[];
                     static form DataForm;
                     static int DataAnz;

                     static int ubrows[];
                     static int NoRecNrSet;
                     static char *InfoItem;
                     void SetButtonSize (int);

                     void SetToolbar2 (CFORM *Toolbar2)
                     {
                               this->Toolbar2 = Toolbar2;
                     }
 
                     CFORM *GetToolbar2 (void)
                     {
                               return Toolbar2;
                     }

                     void SetWork (Work *work)
                     {
                         this->work = work;
                         this->work->SetListDlg (this);
                     }

                     Work *GetWork (void)
                     {
                         return work;
                     }

                     PHASELST (short, long);
					 ~PHASELST ();
                     void ReadProdPhasen (void);
                     void GetSysPar (void);
                     void GetCfgValues (void);
                     void Setfeld2Removed (BOOL);
                     void Prepare (void);
                     void Open (void);
                     int Fetch (void);
                     void Close (void);
 				     unsigned char *GetDataRec (void);
				     int GetDataRecLen (void);
 				     char *GetListTab (int);
                     BOOL NewMaschNr (long[], long, int);
                     void WriteRec (int);
                     void InitRow (void);
                     int ToMemory (int);
                     void uebertragen (void);

                     void SetNoRecNr (void);
                     BOOL StopList (void);
                     BOOL OnKey5 (void);
                     BOOL OnKey6 (void);
                     BOOL OnKey7 (void);
                     BOOL OnKey8 (void);
                     BOOL OnKey9 (void);
                     BOOL OnKey10 (void);
                     BOOL OnKey11 (void);
                     BOOL OnKey12 (void);
                     BOOL OnKeyDel (void);
                     BOOL OnKeyInsert (void);
                     BOOL OnKeyUp (void);
                     BOOL OnKeyDown (void);
                     BOOL OnKeyNext (void);
                     BOOL OnKeyPrior (void);
                     BOOL OnCommand (HWND, UINT, WPARAM, LPARAM);
                     BOOL OnPaint (HWND, UINT, WPARAM, LPARAM);
                     BOOL OnMove (HWND, UINT, WPARAM, LPARAM);
                     int AfterRow (int);
                     int AfterCol (char *);
                     int BeforeCol (char *);
                     int BeforeRow (int);
                     void CreateListQuikInfos (HWND);
                     void TestRemoves (void);
                     int  ShowMaschNr (void);
                     int  ShowMaschNr (char *);
                     static int TestAppend (void);
//                     static int InfoProc (char **, char *, char *);

//                     BOOL CallInfo (HWND, RECT *, char *, char *, DWORD);
                     void CallInfoEx (void);
};
#endif