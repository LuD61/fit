//290410
//=============================================================================
//
//  Project: List & Label
//           Copyright (c) 1998-2002 combit GmbH, All Rights Reserved
//
//  Authors: combit Software Team
//
//-----------------------------------------------------------------------------
//
//  Module:  LLMFC - List & Label MFC Sample Application
//
// Diese Applikation z.Z. nur f�r Rezepturdruck (Tabelle reportrez wird ausgelesen)
// Erstellung : 24.95.2003  LuD

//=============================================================================

// mainfrm.cpp : implementation of the CMainFrame class

#include "stdafx.h"
#include "llmfc.h"
#include "mainfrm.h"
#include "wmask.h"
#include "DbClass.h"
#include "reportrez.h"
#include "form_feld.h"
#include "systables.h"
#include "syscolumns.h"
#include "strfkt.h"
#include "Token.h"

#include "cmbtl9.h"

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// GR: Waehlen Sie hier die List & Label Sprache f�r den Designer und die
//     Dialoge aus :
//    (Die entsprechenden Sprachkonstanten entnehmen Sie der Datei cmbtl9.h)

// US: choose your LuL-Language for all designer, dialogs, etc... here:
//    (see cmbtl9.h for other language-constants)

const int LUL_LANGUAGE = CMBTLANG_GERMAN;

//LuD...
//const char *FILENAME_DEFAULT = "c:\\temp\\rezeptur.lst";  // Zeiger auf eine Konstante
const char *FILENAME_DEFAULT = "c:\\user\\fit\\format\\LL\\rezeptur.lst";  // Zeiger auf eine Konstante
bool cfgOK;
char Listenname[512];
char Listenname_ohnePreis[512];
int lese_a_preism = 0;   
int Listenauswahl = 0;   // wenn 1 : Auswahl mehrerer Reports
bool lohnePreis = 0;   
int Hauptmenue = 0;   // wenn 1 : Auswahl edit oder print �ber Menue
extern short mdn ;
extern double avon ;
extern double abis ;
extern int agvon ;
extern int agbis ;
extern short variantevon;
extern short variantebis;
extern short akvvon;
extern short akvbis;
extern double rez_schwund;
extern double rez_chargengewicht;
extern char rez_preis_bz[12];
extern char rez_bep_bz[12];
extern double rez_preis;
extern double sk_preis;
extern double rez_kosten;
extern char bearb_info[81];
extern short mitGB ;
//extern short ohnePreis;
int irez = 0;



  DB_CLASS dbClass;
  REPORTREZ_CLASS ReportRez;
//  A_PREIS_CLASS APreis;
  FORM_FELD form_feld;
  SYSCOLUMNS syscolumns;
  SYSTABLES systables;

HWND hMainWindow;

int form_feld_curs, tabid_curs, coltyp_curs,  dynacurs ; 
/////////////////////////////////////////////////////////////////////////////
// CMainFrame

// GR: Registrieren eines Nachrichtenkanals f�r List & Label Callbacks
// US: register LuL MessageBase:
UINT CMainFrame::m_uLuLMessageBase = RegisterWindowMessage("cmbtLLMessage");

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_COMMAND(ID_FILE_START_DEBUG, OnStartDebug)
	ON_COMMAND(ID_FILE_STOP_DEBUG, OnStopDebug)
	ON_COMMAND(ID_EDIT_LABEL, OnEditLabel)
	ON_COMMAND(ID_EDIT_LIST, OnEditList)
	ON_UPDATE_COMMAND_UI(ID_FILE_START_DEBUG, OnUpdateStartDebug)
	ON_UPDATE_COMMAND_UI(ID_FILE_STOP_DEBUG, OnUpdateStopDebug)
	ON_COMMAND(ID_PRINT_LABEL, OnPrintLabel)
	ON_COMMAND(ID_PRINT_REPORT, OnPrintReport)
	//}}AFX_MSG_MAP
	ON_REGISTERED_MESSAGE(m_uLuLMessageBase,OnLulMessage)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	m_bDebug = FALSE;
    ProgCfg = new PROG_CFG ("rezepturdruck");

}

CMainFrame::~CMainFrame()
{
    if (ProgCfg != NULL)
    {
        ProgCfg->CloseCfg ();
        delete ProgCfg;
        ProgCfg = NULL;
    }

}


void holeAPreism (void)
{
    if (lese_a_preism > 0)
	{
		if (reportrez.grouprez == 1)  //Verarbeitungsmat.
		{
//			 a_preis.bep = reportrez.bep;  //Werte aus a_preism sind hier schon gespeichert
//			 a_preis.ek = reportrez.ek;
		}
	}
}


/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	
	LlAddCtlSupport(m_hWnd,
					LL_CTL_ADDTOSYSMENU | LL_CTL_CONVERTCONTROLS | LL_CTL_ALSOCHILDREN,
					"combit.ini");
	GetCfgValues();
	
	if (Hauptmenue == 0)  //#LuD
	{
		PostMessage(WM_COMMAND,ID_PRINT_REPORT,0l);
	}
		
return 0;
}

void CMainFrame::OnStartDebug()
{
	MessageBox("Make sure that DEBWIN2 had been started before this demo application. "
				"If this doesn't happen you won't see any debug outputs now!",
				"List & Label Sample App", MB_OK | MB_ICONINFORMATION);
//	LlSetDebug(LL_DEBUG_CMBTLL);
	m_bDebug = TRUE;

}

void CMainFrame::OnStopDebug()
{
	LlSetDebug(FALSE);
	m_bDebug = FALSE;
}

void CMainFrame::OnUpdateStopDebug(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bDebug);
}

void CMainFrame::OnUpdateStartDebug(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(!m_bDebug);
}


//=============================================================================
void CMainFrame::OnEditLabel()
//=============================================================================
{
	CHAR szFilename[128+1] = "*.lbl";
	HWND hWnd = m_hWnd;
	HJOB hJob;

	// GR: Initialisieren von List & Label.
	//     Es wird ein Job geoeffnet
	// US: initialize Job
	hJob = LlJobOpen(LUL_LANGUAGE);
	if (hJob==LL_ERR_BAD_JOBHANDLE)
	{
		MessageBox("Job can't be initialized!", "List & Label Sample App", MB_OK|MB_ICONSTOP);
		return;
	}
	else if (hJob==LL_ERR_NO_LANG_DLL)
	{
		MessageBox("Language file not found!\nEnsure that *.lng files can be found in your LuL DLL directory.",
					"List & Label Sample App",
					MB_OK|MB_ICONSTOP);
		return;
	}

	// GR: Setzen der List & Label Optionen
	// US: Setting the List & Label options
	LlSetOption(hJob, LL_OPTION_NEWEXPRESSIONS, TRUE);

	//GR: Exporter aktivieren
	//US: Enable exporter
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cm32l9ex.llx");

	// GR: Auswahl der Projekt-Datei �ber Datei-Auswahl-Dialog
/* ---> GrJ GrJ
	if (LlSelectFileDlgTitleEx(hJob, hWnd, "", LL_PROJECT_LABEL|LL_FILE_ALSONEW
								, szFilename, sizeof(szFilename), NULL) < 0)
    {
        LlJobClose(hJob);
        return;
    }
< ---- */

	// GR: Zur�cksetzen der internen Variablen-Puffer
    LlDefineVariableStart(hJob);

	// GR: Aufruf des Designers

   	sprintf ( szFilename, FILENAME_DEFAULT );

 	if (LlDefineLayout(hJob, hWnd, "Designer", LL_PROJECT_LABEL, szFilename) < 0)
    {
        MessageBox("Error by calling LlDefineLayout", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
        LlJobClose(hJob);
        return;
    }

    // GR: Beenden des List & Label Jobs
    LlJobClose(hJob);
}

long holetyp(char* tab_nam , char* feld_nam)
{
	static char alttabnam[20];
	static long alttabid ;
	if ( strncmp ( tab_nam, alttabnam, strlen(tab_nam)) == 0 )
	{
		// bereits gefunden
	}
	else
	{
		sprintf ( systables.tabname, "%s" , tab_nam );
		dbClass.sqlopen (tabid_curs);	// destroyed den Cursor, sqlopen refreshed
		if (!dbClass.sqlfetch(tabid_curs))	// gefunden
		{
			alttabid = systables.tabid ;
			sprintf ( alttabnam, "%s", tab_nam );
		}
		else

			return LL_TEXT ;

	}
 	sprintf ( syscolumns.colname, "%s" , feld_nam );
 	syscolumns.tabid = alttabid ;
	dbClass.sqlopen (coltyp_curs);	// destroyed den Cursor, sqlopen refreshed
	if (!dbClass.sqlfetch(coltyp_curs))	// gefunden
	{
		switch (syscolumns.coltype)
		{
		case(iDBCHAR):
			return LL_TEXT ;
		case(iDBSMALLINT):	// smallint
		case(iDBINTEGER):	// integer
		case(iDBDECIMAL):	// decimal
			return LL_NUMERIC ;
		case(iDBDATUM):	// Datumsfeld
			return LL_DATE ;
		}
		return LL_TEXT ;
	}
	else
		return LL_TEXT ;
		
}


int GetRecCount()  //#LuD
{
//	int ianzahl;
//	dbClass.sqlin ((double *) &reportrez.a, SQLDOUBLE, 0);
//	dbClass.sqlout ((int *)  &ianzahl, SQLLONG, 0);
//    reportrez_curs = dbClass.sqlcursor ("select count(*) from reportrez ");
//	dbClass.sqlfetch (count_cursor);
//	dbClass.sqlclose (count_cursor);	
	return ReportRez.dbcount ();
}


void Felderdefinition (HJOB hJob)
{
    LlDefineFieldExt(hJob, "bearb_info",    "bearb_info", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "reportrez.grouprez", "2", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "reportrez.a",        "7774.0", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "reportrez.a_bz1",    "Artikelbez.", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "reportrez.rez",      "4711", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "reportrez.rez_bz",   "Rez.Bez.", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "reportrez.variante", "1", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "reportrez.variante_bz", "Variante.Bez", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "akv", "1", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "reportrez.preis_basis", "EK", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "reportrez.schwund", "22", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "reportrez.chargengewicht", "210", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "reportrez.datum", "01.01.2003", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "reportrez.posi", "1", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "reportrez.mat", "6809", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "reportrez.mat_bz", "Material.Bez", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "reportrez.menge", "12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "reportrez.einheit", "kg", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "reportrez.ek", "2.3", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "reportrez.wert_ek", "27", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "reportrez.kosten", "0.1", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "reportrez.bep", "2.4", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "reportrez.wert_bep", "28", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "reportrez.k_a_bez", "Bezeichnung", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "reportrez.k_diff", "0.4", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "reportrez.kostenkg", "4", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "reportrez.kostenart", "1", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "reportrez.kostenart_bz", "kostenart.Bez", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "reportrez.mdn", "1", LL_NUMERIC, NULL);
//    LlDefineFieldExt(hJob, "a_preis.inh_ek", "1", LL_NUMERIC, NULL);
//    LlDefineFieldExt(hJob, "a_preis.ek", "2.23", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "a_bas.a_gew", "0.5", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "a_bas.inh_lief", "1", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "prdk_k.chg_anz_bzg", "1", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "prdk_k.rez_bz", "1", LL_TEXT, NULL);
}
 
void Variablendefinition (HJOB hJob)
{

    LlDefineVariableExt(hJob, "Artikelnummer",        "7774.0", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "Artikelbezeichnung",    "Artikelbez.", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "Rezepturnummer",      "4711", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "Rezepturbezeichnung",   "Rez.Bez.", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "Variante", "1", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "akv", "1", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "Variante Bezeichnung", "Variante.Bez", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "Schwund", "10.00", LL_NUMERIC, NULL);
	LlDefineVariableExt(hJob, "Chargengewicht", "71.5", LL_NUMERIC, NULL);
	LlDefineVariableExt(hJob, "Einheit Bez", "Preis / Einh", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "BEP Bez", "BEP   / Einh", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "Preis", "0.555", LL_NUMERIC, NULL);
	LlDefineVariableExt(hJob, "SKPreis", "0.666", LL_NUMERIC, NULL);
	LlDefineVariableExt(hJob, "PreisBep", "0.555", LL_NUMERIC, NULL);
	LlDefineVariableExt(hJob, "Folgeseite", "1", LL_NUMERIC, NULL) ;
	LlDefineVariableExt(hJob, "prdk_k.chg_anz_bzg", "1", LL_NUMERIC, NULL) ;
	LlDefineVariableExt(hJob, "prdk_k.rez_bz", "1", LL_TEXT, NULL) ;
}

void VariablenUebergabe ( HJOB hJob, char szTemp2[], int nRecno )
{
		sprintf(szTemp2, "%.0lf", reportrez.a );
		LlDefineVariableExt(hJob, "Artikelnummer",        szTemp2, LL_NUMERIC, NULL);
		LlDefineVariableExt(hJob, "Artikelbezeichnung",    reportrez.a_bz1, LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "Rezepturnummer",      reportrez.rez, LL_NUMERIC, NULL);
		LlDefineVariableExt(hJob, "Rezepturbezeichnung",   reportrez.rez_bz, LL_TEXT, NULL);
					sprintf(szTemp2, "%d", reportrez.variante );
		LlDefineVariableExt(hJob, "Variante", szTemp2, LL_NUMERIC, NULL);
		LlDefineVariableExt(hJob, "Variante Bezeichnung", reportrez.variante_bz, LL_TEXT, NULL);
					sprintf(szTemp2, "%d", prdk_k.akv );
		LlDefineVariableExt(hJob, "akv", szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", rez_schwund );
		LlDefineVariableExt(hJob, "Schwund", szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.3lf", rez_chargengewicht );
		LlDefineVariableExt(hJob, "Chargengewicht", szTemp2, LL_NUMERIC, NULL);
		LlDefineVariableExt(hJob, "Einheit Bez", rez_preis_bz, LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "BEP Bez", rez_bep_bz, LL_TEXT, NULL);
		sprintf(szTemp2, "%.3lf", rez_preis );
		LlDefineVariableExt(hJob, "Preis", szTemp2, LL_NUMERIC, NULL) ;
		sprintf(szTemp2, "%.3lf", sk_preis );
		LlDefineVariableExt(hJob, "SKPreis", szTemp2, LL_NUMERIC, NULL) ;
		sprintf(szTemp2, "%.3lf", rez_kosten );
		LlDefineVariableExt(hJob, "PreisBep", szTemp2, LL_NUMERIC, NULL) ;
		sprintf(szTemp2, "%ld", prdk_k.chg_anz_bzg );
		LlDefineVariableExt(hJob, "prdk_k.chg_anz_bzg", szTemp2, LL_NUMERIC, NULL) ;
		LlDefineVariableExt(hJob, "prdk_k.rez_bz", prdk_k.rez_bz, LL_TEXT, NULL) ;
		if (irez > 0 && irez != atoi(reportrez.rez))
		{	
			LlDefineVariableExt(hJob, "Folgeseite", "0", LL_NUMERIC, NULL) ;
		}
		else
		{	
			LlDefineVariableExt(hJob, "Folgeseite", "1", LL_NUMERIC, NULL) ;
		}
		irez = atoi(reportrez.rez);

}

void FelderUebergabe ( HJOB hJob, char szTemp2[], int nRecno )
{

			if (nRecno > 1 && reportrez.posi == 0)
			{
				reportrez.posi = 99;
			}
			if (reportrez.grouprez == 5 && reportrez.posi == 7)
            {
				reportrez.wert_ek = 0.0;
				reportrez.wert_bep = 0.0;
			}
			/***
			if (reportrez.grouprez == 5 && reportrez.posi == 8)
            {
				reportrez.bep = rez_kosten;
			}
			if (reportrez.grouprez == 5 && reportrez.posi == 9)
            {
				reportrez.bep = rez_preis + rez_kosten;
			} 
			*******/

	ReportRez.GetVerarbInfo();
    LlDefineFieldExt(hJob, "bearb_info",    clipped(bearb_info), LL_TEXT, NULL);
	sprintf(szTemp2, "%d", reportrez.grouprez );
    LlDefineFieldExt(hJob, "reportrez.grouprez", szTemp2, LL_NUMERIC, NULL);
				sprintf(szTemp2, "%.0lf", reportrez.a );
    LlDefineFieldExt(hJob, "reportrez.a",        szTemp2, LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "reportrez.a_bz1",    reportrez.a_bz1, LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "reportrez.rez",      reportrez.rez, LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "reportrez.rez_bz",   reportrez.rez_bz, LL_TEXT, NULL);
				sprintf(szTemp2, "%d", reportrez.variante );
    LlDefineFieldExt(hJob, "reportrez.variante", szTemp2, LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "reportrez.variante_bz", reportrez.variante_bz, LL_TEXT, NULL);
					sprintf(szTemp2, "%d", prdk_k.akv );
	LlDefineFieldExt(hJob, "akv", szTemp2, LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "reportrez.preis_basis", reportrez.preis_basis, LL_TEXT, NULL);
				sprintf(szTemp2, "%.2lf", reportrez.schwund );
    LlDefineFieldExt(hJob, "reportrez.schwund", szTemp2, LL_NUMERIC, NULL);
				sprintf(szTemp2, "%.3lf", reportrez.chargengewicht );
    LlDefineFieldExt(hJob, "reportrez.chargengewicht", szTemp2, LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "reportrez.datum", "01.01.2003", LL_TEXT, NULL);
				sprintf(szTemp2, "%d", reportrez.posi );
    LlDefineFieldExt(hJob, "reportrez.posi", szTemp2, LL_NUMERIC, NULL);
				sprintf(szTemp2, "%d", reportrez.mat );
    LlDefineFieldExt(hJob, "reportrez.mat", szTemp2, LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "reportrez.mat_bz", reportrez.mat_bz, LL_TEXT, NULL);
				sprintf(szTemp2, "%.03f", reportrez.menge );
    LlDefineFieldExt(hJob, "reportrez.menge", szTemp2, LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "reportrez.einheit", reportrez.einheit, LL_TEXT, NULL);
				sprintf(szTemp2, "%.3lf", reportrez.ek );
    LlDefineFieldExt(hJob, "reportrez.ek", szTemp2, LL_NUMERIC, NULL);
				sprintf(szTemp2, "%.3lf", reportrez.wert_ek );
    LlDefineFieldExt(hJob, "reportrez.wert_ek", szTemp2, LL_NUMERIC, NULL);
				sprintf(szTemp2, "%.3lf", reportrez.kosten );
    LlDefineFieldExt(hJob, "reportrez.kosten", szTemp2, LL_NUMERIC, NULL);
				sprintf(szTemp2, "%.3lf", reportrez.bep );
    LlDefineFieldExt(hJob, "reportrez.bep", szTemp2, LL_NUMERIC, NULL);
				sprintf(szTemp2, "%.3lf", reportrez.wert_bep );
    LlDefineFieldExt(hJob, "reportrez.wert_bep", szTemp2, LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "reportrez.k_a_bez", reportrez.k_a_bez, LL_TEXT, NULL);
				sprintf(szTemp2, "%.3lf", reportrez.k_diff );
    LlDefineFieldExt(hJob, "reportrez.k_diff", szTemp2, LL_NUMERIC, NULL);
				sprintf(szTemp2, "%.3lf", reportrez.kostenkg );
    LlDefineFieldExt(hJob, "reportrez.kostenkg", szTemp2, LL_NUMERIC, NULL);
				sprintf(szTemp2, "%d", reportrez.kostenart );
    LlDefineFieldExt(hJob, "reportrez.kostenart", szTemp2, LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "reportrez.kostenart_bz", reportrez.kostenart_bz, LL_TEXT, NULL);
				sprintf(szTemp2, "%d", reportrez.mdn );
    LlDefineFieldExt(hJob, "reportrez.mdn", szTemp2, LL_NUMERIC, NULL);
//			    if (a_preis.inh_ek == 0) a_preis.inh_ek = 1;
//			    a_preis.inh_ek = 1;
//				sprintf(szTemp2, "%d", a_preis.inh_ek );
//    LlDefineFieldExt(hJob, "a_preis.inh_ek", szTemp2, LL_NUMERIC, NULL);
	    if (strcmp(reportrez.preis_basis,"EK      ") == 0)  
        {
			if (reportrez.kostenart == 2) //#Verarbeitungsmat. als Rezeptur !!
            {
//				sprintf(szTemp2, "%.3lf", a_preis.bep );
			}
			else
            {
//				sprintf(szTemp2, "%.3lf", a_preis.ek );
			}
		}
		else
        { 
//			sprintf(szTemp2, "%.3lf", a_preis.plan_ek );
		}
//    LlDefineFieldExt(hJob, "a_preis.ek", szTemp2, LL_NUMERIC, NULL);
	if (a_bas.lief_einh == 0) a_bas.lief_einh = a_bas.me_einh ;
// 200410	if (a_bas.lief_einh == 2) a_bas.a_gew = 1.0;
	if (a_bas.me_einh == 2) a_bas.a_gew = 1.0; //290410
	if (a_bas.inh_lief == 0.0) a_bas.inh_lief = 1.0;
				sprintf(szTemp2, "%.3lf", a_bas.a_gew );
    LlDefineFieldExt(hJob, "a_bas.a_gew", szTemp2, LL_NUMERIC, NULL);
				sprintf(szTemp2, "%.3lf", a_bas.inh_lief );
    LlDefineFieldExt(hJob, "a_bas.inh_lief", szTemp2, LL_NUMERIC, NULL);
	sprintf(szTemp2, "%ld", prdk_k.chg_anz_bzg );
	LlDefineFieldExt(hJob, "prdk_k.chg_anz_bzg", szTemp2, LL_NUMERIC, NULL) ;
	LlDefineFieldExt(hJob, "prdk_k.rez_bz", prdk_k.rez_bz, LL_TEXT, NULL) ;



}


//=============================================================================
void CMainFrame::OnEditList()
//=============================================================================
{
	CHAR szFilename[128+1] = "*.lst";
	HWND hWnd = m_hWnd;
	HJOB hJob;
    char *etc;
	char filename[512];

	etc = getenv ("BWS");
	if (lohnePreis == FALSE)
    {
		sprintf (filename, "%s\\format\\LL\\Rezeptur\\%s.lst", etc, Listenname);
    }
	else
    {
		sprintf (filename, "%s\\format\\LL\\Rezeptur\\ohne_Preis\\%s.lst", etc, Listenname);
    }

	FILENAME_DEFAULT = filename;
	// GR: Initialisieren von List & Label.
	//     Es wird ein Job geoeffnet
	hJob = LlJobOpen(LUL_LANGUAGE);
	if (hJob==LL_ERR_BAD_JOBHANDLE)
	{
		MessageBox("Job can't be initialized!", "List & Label Sample App", MB_OK|MB_ICONSTOP);
		return;
	}
	else if (hJob==LL_ERR_NO_LANG_DLL)
	{
		MessageBox("Language file not found!\nEnsure that *.lng files can be found in your LuL DLL directory.",
					"List & Label Sample App",
					MB_OK|MB_ICONSTOP);
		return;
	}

 // Ich muss wohl noch meine Lizenz setzen , damit Lauff�hig beim Kunden (Siehe redist.txt)
	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "q6GLOw");


	// GR: Setzen der List & Label Optionen
	LlSetOption(hJob, LL_OPTION_NEWEXPRESSIONS, TRUE);

	// GR: Setze den Modus multiple datalines fuer Tabellen (wird nur fuer SAMPLE3.LST benoetigt)
	//     Der Benutzer hat nun mehrere Zeilendefinitionen in Tabellen zu verfuegung.
	LlSetOption(hJob, LL_OPTION_MULTIPLETABLELINES, TRUE);

	LlSetOption(hJob, LL_OPTION_WIZ_FILENEW, TRUE);

	//GR: Exporter aktivieren
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cm32l9ex.llx");


	// GR: Auswahl der Projekt-Datei �ber Datei-Auswahl-Dialog
   	sprintf ( szFilename, FILENAME_DEFAULT );
	if (Listenauswahl == 1)  //#LuD
    {
		if (LlSelectFileDlgTitleEx(hJob, hWnd, "", LL_PROJECT_LIST|LL_FILE_ALSONEW
								, szFilename, sizeof(szFilename), NULL) < 0)
		{
	        LlJobClose(hJob);
			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
			return;
		}
	}

	// GR: Zur�cksetzen der internen Variablen-Puffer
    LlDefineVariableStart(hJob);

    // GR: Zur�cksetzen der internen Feld-Puffer
    LlDefineFieldStart(hJob);

//	dbClass.opendbase ("bws");
    Variablendefinition (hJob);
    Felderdefinition (hJob);

	if (Listenauswahl != 1)  
    {
	   	sprintf ( szFilename, FILENAME_DEFAULT );
	}
	// GR: Aufruf des Designers
    if (LlDefineLayout(hJob, hWnd, "Design Rezeptur", LL_PROJECT_LIST, szFilename) < 0)
    {
        MessageBox("Error by calling LlDefineLayout", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
        LlJobClose(hJob);
		PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
        return;
    }
    // GR: Beenden des List & Label Jobs
    LlJobClose(hJob);
	PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
}


//=============================================================================
void CMainFrame::OnPrintLabel()
//=============================================================================
{
	DoLabelPrint();
}


//=============================================================================
void CMainFrame::OnPrintReport()
//=============================================================================
{
    LPSTR CommandLine = GetCommandLine ();
    CToken Token (CommandLine, " ");

    char *p;
    Token.NextToken ();
	while ((p = Token.NextToken ()) != NULL)
	{
		if (strcmp (p, "-h") == 0 || strcmp (p, "-?") == 0)
		{
              MessageBox ("-mdn         <Mandant>\n"
				          "-avon        <Artikel-von>\n"
						  "-abis        <Artikel-bis>\n" 
				          "-agvon       <AG-von>\n"
						  "-agbis       <AG-bis>\n" 
						  "-variante    <Variante>\n"
						  "-akv         (nur aktive Varianten)\n"
						  "-nakv        (nur nichtaktive Varianten)\n"
						  "-wahl        (Listenauswahl wird aktiviert)\n"
						  "-ohnePreis   (Liste ohne Preise wird aufgerufen)\n"
						  "-mitGB       (Grundbr�te werden auch separat ausgedruckt)\n",
				          " ----- m�gliche Aufrufparameter ----- ",
                                MB_OK);
  			  PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
              return;
		}
		if (strcmp (p, "-mdn") == 0)
		{
			p = Token.NextToken ();
			if (p == NULL) break;
            mdn = (atoi (p));
		}
		if (strcmp (p, "-avon") == 0)
		{
			p = Token.NextToken ();
			if (p == NULL) break;
            avon = (atof (p));
		}
		else if (strcmp (p, "-abis") == 0)
		{
			p = Token.NextToken ();
			if (p == NULL) break;
            abis = (atof (p));
		}
		if (strcmp (p, "-agvon") == 0)
		{
			p = Token.NextToken ();
			if (p == NULL) break;
            agvon = (atoi (p));
		}
		else if (strcmp (p, "-agbis") == 0)
		{
			p = Token.NextToken ();
			if (p == NULL) break;
            agbis = (atoi (p));
		}
		else if (strcmp (p, "-variante") == 0)
		{
			p = Token.NextToken ();
			if (p == NULL) break;
            variantevon = (atoi (p));
			variantebis = variantevon;
		}
		else if (strcmp (p, "-akv") == 0)
		{
			akvvon = 1;
			akvbis = 1;
		}
		else if (strcmp (p, "-nakv") == 0)
		{
            akvvon = 0;
			akvbis = 0;
		}
		else if (strcmp (p, "-ohnePreis") == 0)
		{
			memcpy (&Listenname, &Listenname_ohnePreis,sizeof(Listenname));
			Listenauswahl = 1;
			lohnePreis = 1;
		}
		else if (strcmp (p, "-wahl") == 0)
        {  
			Listenauswahl = 1;
        }
		if (strcmp (p, "-mitGB") == 0)
		{
			p = Token.NextToken ();
			if (p == NULL) break;
            mitGB = (atoi (p));
		}


	}

	DoListPrint();
}

//=============================================================================
void CMainFrame::DoLabelPrint()
//=============================================================================
{
    HWND hWnd = m_hWnd;
    CHAR szFilename[128+1] = "*.lbl", szTemp[30], szTemp2[40], szBoxText[200];
	HJOB hJob;
	int  nRet = 0;

	// GR: Initialisieren von List & Label.
	//     Es wird ein Job geoeffnet
	hJob = LlJobOpen(LUL_LANGUAGE);
	if (hJob==LL_ERR_BAD_JOBHANDLE)
	{
		MessageBox("Job can't be initialized!", "List & Label Sample App", MB_OK|MB_ICONSTOP);
		return;
    }
	else if (hJob==LL_ERR_NO_LANG_DLL)
	{
		MessageBox("Language file not found!\nEnsure that *.lng files can be found in your LuL DLL directory.",
					"List & Label Sample App",
					MB_OK|MB_ICONSTOP);
		return;
    }

	//GR: Exporter aktivieren
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cm32l9ex.llx");

	// US: Choose new expression mode with formula support
	LlSetOption(hJob, LL_OPTION_NEWEXPRESSIONS, TRUE);
	LlSetOption(hJob, LL_OPTION_WIZ_FILENEW, TRUE);

	// GR: Auswahl der Projekt-Datei �ber Datei-Auswahl-Dialog

   	sprintf ( szFilename, FILENAME_DEFAULT );
	if (Listenauswahl == 1)  //#LuD
    {
		nRet = LlSelectFileDlgTitleEx(hJob, hWnd, "", LL_PROJECT_LABEL
								, szFilename,sizeof(szFilename), NULL);
		if (nRet < 0)
   		{
	    	if (nRet != LL_ERR_USER_ABORTED)
				MessageBox("Error While Printing", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
			LlJobClose(hJob);
			return;
		}
	}

    // GR: Zur�cksetzen der internen Variablen-Puffer
    LlDefineVariableStart(hJob);

    for (int i=1; i<10; i++)
	{
		sprintf(szTemp, "Field%d", i);
    	LlDefineVariable(hJob, szTemp, szTemp);
   	}

    // GR: Definition der Barcode-Variablen
    //     Normalerweise definiert man nur die Barcode-Typen die man benoetigt

    // GR: Druck starten

	if (LlPrintWithBoxStart(hJob, LL_PROJECT_LABEL, szFilename,
							LL_PRINT_EXPORT,
							LL_BOXTYPE_BRIDGEMETER, hWnd, "Printing...") < 0)
    {
        MessageBox("Error While Printing", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
        LlJobClose(hJob);
        return;
    }

	// GR: Einstellungen bzw. optionen f�r den Drucker-Optionen-Dialog
	LlPrintSetOption(hJob, LL_PRNOPT_COPIES, LL_COPIES_HIDE);
    LlPrintSetOption(hJob, LL_PRNOPT_STARTPAGE, 1);
	LlPrintSetOption(hJob, LL_PRNOPT_OFFSET, 0);
	if (LlPrintOptionsDialog(hJob, hWnd, "Select printing options") < 0)
    {
        LlPrintEnd(hJob,0);
        LlJobClose(hJob);
        return;
    }


	//GR: Druckziel abfragen
	CString sMedia;
	LlPrintGetOptionString(hJob, LL_PRNOPTSTR_EXPORT, sMedia.GetBuffer(256), 256);
	sMedia.ReleaseBuffer();

	char szPrinter[80], szPort[20];
	int  nRecCount = 30, nErrorValue = 0, nRecno, nLastPage;

	LlPrintGetPrinterInfo(hJob, szPrinter, sizeof(szPrinter), szPort, sizeof(szPort));

    nLastPage = LlPrintGetOption(hJob, LL_OPTION_LASTPAGE);
    nRecno = 1;

    // GR: Definition der Barcode-Variablen
    //     Normalerweise definiert man nur die Barcode-Typen die man benoetigt

	// GR: Druckschleife
    //     Diese wird so lange wiederholt, bis saemtliche Daten abgearbeitet wurden, oder
    //     ein Fehler auftritt.
	while (nRecno < nRecCount
			&& (LlPrintGetCurrentPage(hJob) <= nLastPage)
			&& nErrorValue == 0)
	{
		for (i=1; i<10; i++)
		{
			sprintf(szTemp, "Field%d", i);
			if (LlPrintIsVariableUsed(hJob, szTemp))
			{
				sprintf(szTemp2, "contents of Field%d, record %d", i, nRecno);
    			LlDefineVariable(hJob, szTemp, szTemp2);
			}
    	}

    	// GR: Prozentbalken in der Fortschritts-Anzeige setzen
    	sprintf(szBoxText, "printing on %s %s", szPrinter, szPort);
    	nErrorValue = LlPrintSetBoxText(hJob, szBoxText, (100 * nRecno / nRecCount));
        if (nErrorValue == LL_ERR_USER_ABORTED)
    	{
    		LlPrintEnd(hJob,0);
    		LlJobClose(hJob);
    		return;
    	}

    	// GR: Variablen drucken
    	nErrorValue = LlPrint(hJob);

		// GR: gehe zum naechsten Datensatz
    	nRecno++;
	}

	//GR: Druck beenden
	LlPrintEnd(hJob,0);


	//GR: Druckziel = Preview?
	if (sMedia=="PRV")
    {
        // GR: Beim Preview-Druck Preview anzeigen und dann Preview-Datei (.LL) l�schen
        LlPreviewDisplay(hJob, szFilename, "", hWnd);

        LlPreviewDeleteFiles(hJob, szFilename, "");
    }

	// GR: Beenden des List & Label Jobs
	LlJobClose(hJob);
}

//=============================================================================
void CMainFrame::DoListPrint()
//=============================================================================
{
    HWND hWnd = m_hWnd;
    CHAR szFilename[128+1] = "*.lst", szTemp[50], szTemp2[60], szBoxText[200];
	CHAR dmess[128] = "";
	HJOB hJob;
	int  nRet = 0;
	int  nRet1 = -1;
	strcpy(szTemp, " ");
	char *etc;
	char filename[512];

	etc = getenv ("BWS");
	if (lohnePreis == FALSE)
    {
		sprintf (filename, "%s\\format\\LL\\Rezeptur\\%s.lst", etc, Listenname);
    }
	else
    {
		sprintf (filename, "%s\\format\\LL\\Rezeptur\\ohne_Preis\\%s.lst", etc, Listenname);
    }

	FILENAME_DEFAULT = filename;


	// GR: Initialisieren von List & Label.
	//     Es wird ein Job geoeffnet
	hJob = LlJobOpen(LUL_LANGUAGE);
	if (hJob==LL_ERR_BAD_JOBHANDLE)
	{
		MessageBox("Job can't be initialized!", "List & Label Sample App", MB_OK|MB_ICONSTOP);
		return;
    }
	else if (hJob==LL_ERR_NO_LANG_DLL)
	{
		MessageBox("Language file not found!\nEnsure that *.lng files can be found in your LuL DLL directory.",
					"List & Label Sample App",
					MB_OK|MB_ICONSTOP);
		return;
    }

 // Ich muss wohl noch meine Lizenz setzen , damit Lauff�hig beim Kunden !?(Siehe redist.txt)
	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "q6GLOw");


	//GR: Exporter aktivieren
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cm32l9ex.llx");

	// GR: Setzen der List & Label Optionen
	LlSetOption(hJob, LL_OPTION_NEWEXPRESSIONS, TRUE);

	// GR: Setze den Modus multiple datalines fuer Tabellen (wird nur fuer SAMPLE3.LST benoetigt)
	//     Der Benutzer hat nun mehrere Zeilendefinitionen in Tabellen zu verfuegung.
	LlSetOption(hJob, LL_OPTION_MULTIPLETABLELINES, TRUE);

	LlSetOption(hJob, LL_OPTION_WIZ_FILENEW, TRUE);

	// GR: Auswahl der Projekt-Datei �ber Datei-Auswahl-Dialog
   	sprintf ( szFilename, FILENAME_DEFAULT );
	if (Listenauswahl == 1)  //#LuD
    {
		nRet = LlSelectFileDlgTitleEx(hJob, hWnd, "", LL_PROJECT_LIST
								, szFilename,sizeof(szFilename), NULL);
		if (nRet < 0)
	   	{
    		if (nRet != LL_ERR_USER_ABORTED)
				MessageBox("Error While Printing", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
			LlJobClose(hJob);
			return;
		}
		nRet1 = -1;
		nRet1 = strcspn( szFilename, "_" );
		if (strlen (szFilename) == (unsigned) nRet1) nRet1 = -1;
		while (nRet1 == -1 && lohnePreis == TRUE && nRet != LL_ERR_USER_ABORTED) 
        {
			MessageBox("keine Berechtigung f�r diese Liste !", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
			nRet = LlSelectFileDlgTitleEx(hJob, hWnd, "", LL_PROJECT_LIST
								, szFilename,sizeof(szFilename), NULL);
			if (nRet < 0)
		   	{
	    		if (nRet != LL_ERR_USER_ABORTED)
					MessageBox("Error While Printing", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
				LlJobClose(hJob);
				return;
			}
   		    nRet1 = -1;
		    nRet1 = strcspn( szFilename, "_" );
   		    if (strlen (szFilename) == (unsigned) nRet1) nRet1 = -1; 
			 
        }
	}

    // GR: Zur�cksetzen der internen Variablen-Puffer
    LlDefineVariableStart(hJob); 

	dbClass.opendbase ("bws");
    Variablendefinition (hJob);
    Felderdefinition (hJob);

    // GR: Druck starten
	if (LlPrintWithBoxStart(hJob, LL_PROJECT_LIST, szFilename,
							LL_PRINT_EXPORT,
							LL_BOXTYPE_NORMALMETER, hWnd, "Printing...") < 0)
    {
        MessageBox("Error While Printing", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
        LlJobClose(hJob);
        return;
    }

    // GR: Einstellungen bzw. optionen f�r den Drucker-Optionen-Dialog
	LlPrintSetOption(hJob, LL_PRNOPT_COPIES, 	LL_COPIES_HIDE);
    LlPrintSetOption(hJob, LL_PRNOPT_STARTPAGE, 1);
	if (LlPrintOptionsDialog(hJob, hWnd, "Select printing options") < 0)
    {
        LlPrintEnd(hJob,0);
        LlJobClose(hJob);
		if (Hauptmenue == 0)  //#LuD
		{
			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
		}
		
        return;
    }

	//GR: Druckziel abfragen
	CString sMedia;
	LlPrintGetOptionString(hJob, LL_PRNOPTSTR_EXPORT, sMedia.GetBuffer(256), 256);
	sMedia.ReleaseBuffer();

    int  nRecCount = 1, nErrorValue = 0, nPrintFehler = 0, nLastPage, nRecno;
	char szPrinter[80], szPort[20];

    LlPrintGetPrinterInfo(hJob, szPrinter, sizeof(szPrinter), szPort, sizeof(szPort));

	nLastPage = LlPrintGetOption(hJob, LL_OPTION_LASTPAGE);
    nRecno = 1;

    // GR: Druckschleife
    //     Diese wird so lange wiederholt, bis saemtliche Daten abgearbeitet wurden, oder
    //     ein Fehler auftritt.
	nRecCount = GetRecCount () ;
//	sprintf (dmess, "nRecCount = %hd",nRecCount);  //050803
//	MessageBox(dmess, "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
	int di = ReportRez.lesereportrez (nErrorValue);   //       1.DATENSATZ lesen f�r Variablen�bergabe
	if (di != 0) 
    {
		sprintf (dmess, "Datensatz nicht gefunden SQL:%hd",di);
		MessageBox (dmess, "", MB_ICONERROR);
    			LlPrintEnd(hJob,0);
    			LlJobClose(hJob);
				if (Hauptmenue == 0)  //#LuD
				{
					PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
				}
    			return;

	}
	/**
	APreis.SetDatabase (dbClass.GetDatabase());
	APreis.prepare (); 
	APreis.opena_preis (); 
	int dp = APreis.lesea_preis (); 
	holeAPreism();
	**/

	/*
	if (dp != 0 ) 
    {
		sprintf (dmess, "a_preis nicht gefunden mat = %ld SQL:%hd",reportrez.mat,dp);
		MessageBox (dmess, "", MB_ICONERROR);
    			LlPrintEnd(hJob,0);
    			LlJobClose(hJob);
				if (Hauptmenue == 0)  //#LuD
				{
					PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
				}
    			return;
	}
	*/
	ReportRez.openreportrez ();   
	while (nRecno < nRecCount
			&& (LlPrintGetCurrentPage(hJob) <= nLastPage)
			&& (nErrorValue == 0 || nErrorValue == LL_WRN_REPEAT_DATA))
   	{
	    VariablenUebergabe ( hJob, szTemp2, nRecno );

		nErrorValue = LlPrint(hJob);
		while (nRecno <= nRecCount 
			&& (nErrorValue == 0) 
			&& (LlPrintGetCurrentPage(hJob) <= nLastPage)
			&& (ReportRez.lesereportrez (nPrintFehler) == 0))  //       DATENSATZ lesen

		{
/***
		APreis.opena_preis (); 
		dp = APreis.lesea_preis (); 
		holeAPreism();
***/
		/*
		if (dp != 0 ) 
	    {
			sprintf (dmess, "a_preis nicht gefunden mat = %ld SQL:%hd",reportrez.mat,dp);
			MessageBox (dmess, "", MB_ICONERROR);
    			LlPrintEnd(hJob,0);
    			LlJobClose(hJob);
				if (Hauptmenue == 0)  //#LuD
				{
					PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
				}
    			return;
		}
		*/

		    FelderUebergabe ( hJob, szTemp2, nRecno );
    		// GR: Prozentbalken in der Fortschritts-Anzeige setzen
    		sprintf(szBoxText, "printing on %s %s", szPrinter, szPort);
    		nErrorValue = LlPrintSetBoxText(hJob, szBoxText, (100 * nRecno / nRecCount));
        	if (nErrorValue == LL_ERR_USER_ABORTED)
   			{
    			LlPrintEnd(hJob,0);
    			LlJobClose(hJob);
				if (Hauptmenue == 0)  //#LuD
				{
					PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
				}
    			return;
   			}

    		// GR: Drucken der aktuellen Tabellenzeile
    		nErrorValue = LlPrintFields(hJob);
			nPrintFehler = nErrorValue;
//050803
			if (nErrorValue == LL_WRN_REPEAT_DATA)
			{
					// GR: Aktualisieren der seitenabhaengigen Variablen
			    VariablenUebergabe ( hJob, szTemp2, nRecno );
				nErrorValue = LlPrint(hJob);
//			    FelderUebergabe ( hJob, szTemp2, nRecno );
//	    		nErrorValue = LlPrintFields(hJob);
			}


			if (nErrorValue == 0)	// US: everything fine, record could have been printed...
	    	{
				// US: ...but probably the user defined a filter condition!
				//     So before updating time dependent variables we have to check if the record really has been printed:
				if (LlPrintDidMatchFilter(hJob))
				{
					// GR: Aktualisieren der zeitabhaengigen Variable 'FixedVariable2'
					// US: update the time dependent variable 'FixedVariable2'
					//     coming after printig the list(s)
		    		sprintf(szTemp2, "FixedVariable2, record %d", nRecno);
		    		LlDefineVariable(hJob, "FixedVariable2", szTemp2);
		    	}

	    		if (nPrintFehler == 0) nRecno++;	// GR: gehe zum naechsten Datensatz
			}
		}
  	}

	// US: all records have been printed, now flush the table
	//     If footer doesn't fit to this page try again for the next page:

	nErrorValue = LlPrintFieldsEnd(hJob);
	if (nErrorValue == LL_WRN_REPEAT_DATA)
	{
		// GR: Aktualisieren der seitenabhaengigen Variablen
	    VariablenUebergabe ( hJob, szTemp2, nRecno );
	    FelderUebergabe ( hJob, szTemp2, nRecno );

		// US: ... and then try again:
		nErrorValue = LlPrintFieldsEnd(hJob);
	}

	if (nErrorValue == LL_WRN_REPEAT_DATA)	// US: footer still doesn't fit!
		MessageBox("Error because table is too small for footer!", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);

	//GR: Druck beenden
	LlPrintEnd(hJob,0);
	
	//GR: Druckziel = Preview? 
	if (sMedia=="PRV")
    {
        // GR: Beim Preview-Druck Preview anzeigen und dann Preview-Datei (.LL) loeschen
        LlPreviewDisplay(hJob, szFilename, "", hWnd);

        LlPreviewDeleteFiles(hJob, szFilename, "");
    }
	// GR: Beenden des List & Label Jobs
	LlJobClose(hJob);
	if (Hauptmenue == 0)  //#LuD
	{
		PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
	}
}

//=============================================================================
LRESULT CMainFrame::OnLulMessage(WPARAM wParam, LPARAM lParam)
//=============================================================================
{
	// GR: Dies ist die List & Label Callback Funktion.
	//     Saemtliche Rueckmeldungen bzw. Events werden dieser Funktion
	//     gemeldet.
	// US: This is the List & Label Callback function.
	//     Is is called for requests an notifications.


	PSCLLCALLBACK	pscCallback = (PSCLLCALLBACK) lParam;
	LRESULT			lRes = TRUE;
	CString			sVariableDescr;

	static CHAR szHelpText[256];

	ASSERT(pscCallback->_nSize == sizeof(scLlCallback));	// GR: Die groesse der Callback Struktur muss stimmen!
															// US: sizes of callback structs must match!

	switch(wParam)
	{
		case LL_CMND_VARHELPTEXT:	// GR: Es wird ein Beschreibungstext f�r eine Variable erfragt.
									// US: Helptext needed for selected variable within designer

				// GR: ( pscCallback->_lParam enth�lt einen LPCSTR des Beschreibungstextes )
				// US: ( pscCallback->_lParam contains a LPCSTR to the name of the selected variable )

				sVariableDescr = (LPCSTR)pscCallback->_lParam;

				if (!sVariableDescr.IsEmpty())
					sprintf(szHelpText,
							"This is the sample field / variable '%s'.",
							(LPCSTR)sVariableDescr);
				else
					strcpy(szHelpText, "No variable or field selected.");

				pscCallback->_lReply = (LPARAM)szHelpText;
				break;

		default:
				pscCallback->_lReply = lRes = FALSE; // GR: Die Nachricht wurde nicht bearbeitet.
													 // US: indicate that message hasn't been processed
	}

	return(lRes);
}


void CMainFrame::GetCfgValues (void)
{
       char cfg_v [512];

       if (ProgCfg == NULL) return;

       cfgOK = TRUE;
	   sprintf(Listenname,"rezeptur");
	   sprintf(Listenname_ohnePreis,"rezeptur_ohnePreis");
       if (ProgCfg->GetCfgValue ("Listenname", cfg_v) == TRUE)
       {
		   sprintf(Listenname,cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("Listenname_ohnePreis", cfg_v) == TRUE)
       {
		   sprintf(Listenname_ohnePreis,cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("Listenauswahl", cfg_v) == TRUE)
       {
           Listenauswahl = atoi (cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("Hauptmenue", cfg_v) == TRUE)
       {
           Hauptmenue = atoi (cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("Lese_a_preism", cfg_v) == TRUE)
       {
                     lese_a_preism = atoi (cfg_v);
       }
       if (ProgCfg->GetCfgValue ("Mandant", cfg_v) == TRUE)
       {
           mdn = atoi (cfg_v);
	   }
	   if (mdn == 0) mdn = 3 ;
}


    // GR: Definition der Barcode-Variablen
    //     Normalerweise definiert man nur die Barcode-Typen die man benoetigt
	/************
	LlDefineFieldExt(hJob, "Barcode_EAN13", "44|44444|44444", LL_BARCODE_EAN13, NULL);
	LlDefineFieldExt(hJob, "Barcode_EAN13P2", "44|44444|44444|44", LL_BARCODE_EAN13, NULL);
	LlDefineFieldExt(hJob, "Barcode_EAN13P5", "44|44444|44444|44444", LL_BARCODE_EAN13, NULL);
	LlDefineFieldExt(hJob, "Barcode_EAN128", "EAN128ean128", LL_BARCODE_EAN128, NULL);
	LlDefineFieldExt(hJob, "Barcode_CODE128", "Code 128", LL_BARCODE_CODE128, NULL);
	LlDefineFieldExt(hJob, "Barcode_Codabar", "A123456A", LL_BARCODE_CODABAR, NULL);
	LlDefineFieldExt(hJob, "Barcode_UPCA", "44|44444", LL_BARCODE_EAN8, NULL);
	LlDefineFieldExt(hJob, "Barcode_UPCE", "4|44444|44444", LL_BARCODE_UPCA, NULL);
	LlDefineFieldExt(hJob, "Barcode_3OF9", "*TEST*", LL_BARCODE_3OF9, NULL);
	LlDefineFieldExt(hJob, "Barcode_25IND", "44444", LL_BARCODE_25INDUSTRIAL, NULL);
	LlDefineFieldExt(hJob, "Barcode_25IL", "444444", LL_BARCODE_25INTERLEAVED, NULL);
	LlDefineFieldExt(hJob, "Barcode_25MAT", "44444", LL_BARCODE_25MATRIX, NULL);
	LlDefineFieldExt(hJob, "Barcode_25DL", "44444", LL_BARCODE_25DATALOGIC, NULL);
	LlDefineFieldExt(hJob, "Barcode_POSTNET5", "44444", LL_BARCODE_POSTNET, NULL);
	LlDefineFieldExt(hJob, "Barcode_POSTNET10", "44444-4444", LL_BARCODE_POSTNET, NULL);
	LlDefineFieldExt(hJob, "Barcode_POSTNET12", "44444-444444", LL_BARCODE_POSTNET, NULL);
	LlDefineFieldExt(hJob, "Barcode_FIM", "A", LL_BARCODE_FIM, NULL);
	**********/
void fitapldefines (HJOB hJob)
{

//char tabdotfeld[101];
//char defabeleg [101];
//long ll_typ ;

	// GrJ :spaeter soll hier ne gigantische Schleife alles automatisch generieren
	// Namenskonvention ( Vorschlag 1 : 1. Buchstabe Tabelle/feld grossbuchstabe
	//									2. fuer ptab-Felder ein kleines "c" vor Feldname
	//								d.h. Original-feld mit Schluessel wird immer angeboten,
	//									ptabn-Feld nur, falls angekreuzt(Performance)
	//									3. fuer adress-felder : ein kleines "a"
	//									, danach nummer-feld-name, danach feldname
	//										Bsp.: Kun.aAdr1.Str
	//									Aufloesung nur, wenn angekreuzt

// 		dbClass.opendbase ("bws");
//	}
/* ---> Mustereintrag von Wille ---> 
	dbClass.sqlin ((double *) &a_bas.a, SQLDOUBLE, 0);
	dbClass.sqlout ((char *)  a_bas.a_bz1, SQLCHAR, 25);
	dbClass.sqlout ((char *)  a_bas.a_bz2, SQLCHAR, 25);
    cursor = dbClass.sqlcursor ("select a_bz1, a_bz2 from a_bas where a = ?");
	a = 2.0;
	while (dbClass.sqlfetch (cursor) == 0)
	{
	}
	dbClass.sqlclose (cursor);	// destroyed den Cursor, sqlopen refreshed 
< ---- */

/****************		
		dbClass.sqlin ((long *) &syscolumns.tabid, SQLLONG, 0);
		dbClass.sqlin ((char *)  syscolumns.colname, SQLCHAR, 19);

		dbClass.sqlout ((short *) &syscolumns.coltype, SQLSHORT,0);
    
		coltyp_curs = dbClass.sqlcursor ("select coltype from syscolumns where tabid = ? and colname = ?");
    
		dbClass.sqlin ((char *) systables.tabname, SQLCHAR, 19);

		dbClass.sqlout ((long *)  &systables.tabid, SQLLONG, 0);

		tabid_curs = dbClass.sqlcursor ("select tabid from systables where tabname = ?");

// Hier kommt der Hauptcursor

		dbClass.sqlin ((char *) form_feld.form_nr, SQLCHAR, 25);

		dbClass.sqlout ((char *)  form_feld.agg_funk, SQLCHAR, 2);
		dbClass.sqlout ((char *)  form_feld.feld_id, SQLCHAR, 2);
		dbClass.sqlout ((char *)  form_feld.feld_nam, SQLCHAR, 19);
		dbClass.sqlout ((char *)  form_feld.feld_typ, SQLCHAR, 2);
		dbClass.sqlout ((short *) &form_feld.delstatus, SQLSHORT, 0);
		dbClass.sqlout ((char *)  form_feld.tab_nam, SQLCHAR, 19);
		dbClass.sqlout ((long *)  &form_feld.lfd, SQLLONG, 0);
    
		form_feld_curs = dbClass.sqlcursor 
			("select agg_funk, feld_id, feld_nam, feld_typ, delstatus, tab_nam, lfd from form_feld where form_nr = ?");
    
	}

	sprintf (form_feld.form_nr, "test1");

	dbClass.sqlopen (form_feld_curs);

	while (dbClass.sqlfetch (form_feld_curs) == 0)
	{
		ll_typ = holetyp(form_feld.tab_nam , form_feld.feld_nam );

		form_feld.tab_nam[0] = toupper(form_feld.tab_nam[0]) ;
		form_feld.feld_nam[0] = toupper(form_feld.feld_nam[0]) ;
		CString s = form_feld.tab_nam ;
		s.TrimRight() ;
		strcpy ( form_feld.tab_nam, s ) ;
		s = form_feld.feld_nam ;
		s.TrimRight() ;
		strcpy ( form_feld.feld_nam, s ) ;
		sprintf( tabdotfeld , "%s.%s", form_feld.tab_nam, form_feld.feld_nam);
		sprintf ( defabeleg, "%s", form_feld.feld_nam);
		if (ll_typ == LL_NUMERIC )
		{
			sprintf ( defabeleg, "12345678");
		};
	
			LlDefineFieldExt(hJob, tabdotfeld, defabeleg,ll_typ,NULL);

	}
********************************/
}


