#include "stdafx.h"
#include "DbClass.h"
#include "a_preis.h"
#include "reportrez.h"
#include "cmbtl9.h"

struct A_PREIS a_preis, a_preis_null;
struct A_KALK_MAT a_kalk_mat, a_kalk_mat_null;
struct A_KALKHNDW a_kalkhndw, a_kalkhndw_null;
int readcursor1, readcursor1_0, readcursor2, readcursor2_0;
	int preism_cursor;

void A_PREIS_CLASS::prepare (void)
{

		    sqlin ((long *)   &reportrez.mdn, SQLLONG, 0);
			sqlin ((long *) &reportrez.mat,SQLLONG,0);
//jetzt ohne a_mat vorraussetzung a = mat trift immer zu!!!
    sqlout ((long *) &a_preis.mdn,SQLLONG,0);
    sqlout ((short *) &a_preis.fil,SQLSHORT,0);
    sqlout ((double *) &a_preis.a,SQLDOUBLE,0);
    sqlout ((short *) &a_preis.prodabt,SQLSHORT,0);
    sqlout ((short *) &a_preis.druckfolge,SQLSHORT,0);
    sqlout ((double *) &a_preis.ek,SQLDOUBLE,0);
    sqlout ((double *) &a_preis.plan_ek,SQLDOUBLE,0);
    sqlout ((double *) &a_preis.kostenkg,SQLDOUBLE,0);
    sqlout ((double *) &a_preis.kosteneinh,SQLDOUBLE,0);
    sqlout ((short *) &a_preis.flg_bearb_weg,SQLSHORT,0);
    sqlout ((short *) &a_preis.bearb_hk,SQLSHORT,0);
    sqlout ((short *) &a_preis.bearb_p1,SQLSHORT,0);
    sqlout ((short *) &a_preis.bearb_p2,SQLSHORT,0);
    sqlout ((short *) &a_preis.bearb_p3,SQLSHORT,0);
    sqlout ((short *) &a_preis.bearb_p4,SQLSHORT,0);
    sqlout ((short *) &a_preis.bearb_p5,SQLSHORT,0);
    sqlout ((short *) &a_preis.bearb_p6,SQLSHORT,0);
    sqlout ((short *) &a_preis.bearb_vk,SQLSHORT,0);
    sqlout ((double *) &a_preis.sp_sk,SQLDOUBLE,0);
    sqlout ((double *) &a_preis.sp_fil,SQLDOUBLE,0);
    sqlout ((double *) &a_preis.sp_vk,SQLDOUBLE,0);
//    sqlout ((DATE_STRUCT *) &a_preis.dat,SQLDATE,0);
    sqlout ((char *) a_preis.dat,SQLCHAR,11);
    sqlout ((char *) a_preis.zeit,SQLCHAR,6);
    sqlout ((char *) a_preis.pers_nam,SQLCHAR,9);
    sqlout ((long *) &a_preis.inh_ek,SQLLONG,0);
    sqlout ((double *) &a_preis.bep,SQLDOUBLE,0);
    sqlout ((double *) &a_preis.plan_bep,SQLDOUBLE,0);
    sqlout ((double *) &a_preis.test_ek,SQLDOUBLE,0);
            readcursor = sqlcursor ("select a_preis.mdn,  "
"a_preis.fil,  a_preis.a,  a_preis.prodabt,  a_preis.druckfolge,  "
"a_preis.ek,  a_preis.plan_ek,  a_preis.kostenkg,  a_preis.kosteneinh,  "
"a_preis.flg_bearb_weg,  a_preis.bearb_hk,  a_preis.bearb_p1,  "
"a_preis.bearb_p2,  a_preis.bearb_p3,  a_preis.bearb_p4,  "
"a_preis.bearb_p5,  a_preis.bearb_p6,  a_preis.bearb_vk,  "
"a_preis.sp_sk,  a_preis.sp_fil,  a_preis.sp_vk,  a_preis.dat,  "
"a_preis.zeit,  a_preis.pers_nam,  a_preis.inh_ek,  a_preis.bep,  "
"a_preis.plan_bep,  a_preis.test_ek from a_preis "

#line 17 "a_preis.rpp"
                                  "where a_preis.mdn = ? "
                                  "and   a_preis.a   = ? ");

		    sqlin ((long *)   &reportrez.mdn, SQLLONG, 0);
			sqlin ((long *) &reportrez.mat,SQLLONG,0);
    sqlout ((double *) &a_kalk_mat.a,SQLDOUBLE,0);
    sqlout ((short *) &a_kalk_mat.bearb_sk,SQLSHORT,0);
    sqlout ((short *) &a_kalk_mat.delstatus,SQLSHORT,0);
    sqlout ((short *) &a_kalk_mat.fil,SQLSHORT,0);
    sqlout ((double *) &a_kalk_mat.hk_teilk,SQLDOUBLE,0);
    sqlout ((double *) &a_kalk_mat.hk_vollk,SQLDOUBLE,0);
    sqlout ((double *) &a_kalk_mat.kost,SQLDOUBLE,0);
    sqlout ((double *) &a_kalk_mat.mat_o_b,SQLDOUBLE,0);
    sqlout ((short *) &a_kalk_mat.mdn,SQLSHORT,0);
    sqlout ((double *) &a_kalk_mat.sp_hk,SQLDOUBLE,0);
//    sqlout ((DATE_STRUCT *) &a_kalk_mat.dat,SQLDATE,0);
    sqlout ((char *) &a_kalk_mat.dat,SQLCHAR,11);
            readcursor1 = sqlcursor ("select a_kalk_mat.a,  "
"a_kalk_mat.bearb_sk,  a_kalk_mat.delstatus,  a_kalk_mat.fil,  "
"a_kalk_mat.hk_teilk,  a_kalk_mat.hk_vollk,  a_kalk_mat.kost,  "
"a_kalk_mat.mat_o_b,  a_kalk_mat.mdn,  a_kalk_mat.sp_hk,  "
"a_kalk_mat.dat from a_kalk_mat, a_mat "

#line 23 "a_preis.rpp"
                                  "where a_kalk_mat.mdn = ? "
                                  "and   a_mat.mat   = ? "
								  "and a_mat.a = a_kalk_mat.a");


			sqlin ((long *) &reportrez.mat,SQLLONG,0);
    sqlout ((double *) &a_kalk_mat.a,SQLDOUBLE,0);
    sqlout ((short *) &a_kalk_mat.bearb_sk,SQLSHORT,0);
    sqlout ((short *) &a_kalk_mat.delstatus,SQLSHORT,0);
    sqlout ((short *) &a_kalk_mat.fil,SQLSHORT,0);
    sqlout ((double *) &a_kalk_mat.hk_teilk,SQLDOUBLE,0);
    sqlout ((double *) &a_kalk_mat.hk_vollk,SQLDOUBLE,0);
    sqlout ((double *) &a_kalk_mat.kost,SQLDOUBLE,0);
    sqlout ((double *) &a_kalk_mat.mat_o_b,SQLDOUBLE,0);
    sqlout ((short *) &a_kalk_mat.mdn,SQLSHORT,0);
    sqlout ((double *) &a_kalk_mat.sp_hk,SQLDOUBLE,0);
//    sqlout ((DATE_STRUCT *) &a_kalk_mat.dat,SQLDATE,0);
    sqlout ((char *) &a_kalk_mat.dat,SQLCHAR,11);
            readcursor1_0 = sqlcursor ("select a_kalk_mat.a,  "
"a_kalk_mat.bearb_sk,  a_kalk_mat.delstatus,  a_kalk_mat.fil,  "
"a_kalk_mat.hk_teilk,  a_kalk_mat.hk_vollk,  a_kalk_mat.kost,  "
"a_kalk_mat.mat_o_b,  a_kalk_mat.mdn,  a_kalk_mat.sp_hk,  "
"a_kalk_mat.dat from a_kalk_mat, a_mat "

#line 30 "a_preis.rpp"
                                  "where a_mat.mat   = ? "
								  "and a_kalk_mat.mdn = 0 "
								  "and a_mat.a = a_kalk_mat.a");


//alternativ zu a_preis :aus a_kalkhndw bei Verpackungen 
		    sqlin ((long *)   &reportrez.mdn, SQLLONG, 0);
			sqlin ((long *) &reportrez.mat,SQLLONG,0);
    sqlout ((double *) &a_kalkhndw.a,SQLDOUBLE,0);
    sqlout ((short *) &a_kalkhndw.bearb_fil,SQLSHORT,0);
    sqlout ((short *) &a_kalkhndw.bearb_lad,SQLSHORT,0);
    sqlout ((short *) &a_kalkhndw.bearb_sk,SQLSHORT,0);
    sqlout ((short *) &a_kalkhndw.delstatus,SQLSHORT,0);
    sqlout ((short *) &a_kalkhndw.fil,SQLSHORT,0);
    sqlout ((double *) &a_kalkhndw.fil_ek_teilk,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkhndw.fil_ek_vollk,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkhndw.lad_vk_teilk,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkhndw.lad_vk_vollk,SQLDOUBLE,0);
    sqlout ((short *) &a_kalkhndw.mdn,SQLSHORT,0);
    sqlout ((double *) &a_kalkhndw.pr_ek1,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkhndw.pr_ek2,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkhndw.pr_ek3,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkhndw.sk_teilk,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkhndw.sk_vollk,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkhndw.sp_fil,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkhndw.sp_lad,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkhndw.sp_vk,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkhndw.we_me1,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkhndw.we_me2,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkhndw.we_me3,SQLDOUBLE,0);
//    sqlout ((DATE_STRUCT *) &a_kalkhndw.dat,SQLDATE,0);
    sqlout ((char *) &a_kalkhndw.dat,SQLCHAR,11);
            readcursor2 = sqlcursor ("select a_kalkhndw.a,  "
"a_kalkhndw.bearb_fil,  a_kalkhndw.bearb_lad,  a_kalkhndw.bearb_sk,  "
"a_kalkhndw.delstatus,  a_kalkhndw.fil,  a_kalkhndw.fil_ek_teilk,  "
"a_kalkhndw.fil_ek_vollk,  a_kalkhndw.lad_vk_teilk,  "
"a_kalkhndw.lad_vk_vollk,  a_kalkhndw.mdn,  a_kalkhndw.pr_ek1,  "
"a_kalkhndw.pr_ek2,  a_kalkhndw.pr_ek3,  a_kalkhndw.sk_teilk,  "
"a_kalkhndw.sk_vollk,  a_kalkhndw.sp_fil,  a_kalkhndw.sp_lad,  "
"a_kalkhndw.sp_vk,  a_kalkhndw.we_me1,  a_kalkhndw.we_me2,  "
"a_kalkhndw.we_me3,  a_kalkhndw.dat from a_kalkhndw "

#line 39 "a_preis.rpp"
                                  "where a_kalkhndw.mdn = ? "
                                  "and   a_kalkhndw.a   = ? ");

			sqlin ((long *) &reportrez.mat,SQLLONG,0);
    sqlout ((double *) &a_kalkhndw.a,SQLDOUBLE,0);
    sqlout ((short *) &a_kalkhndw.bearb_fil,SQLSHORT,0);
    sqlout ((short *) &a_kalkhndw.bearb_lad,SQLSHORT,0);
    sqlout ((short *) &a_kalkhndw.bearb_sk,SQLSHORT,0);
    sqlout ((short *) &a_kalkhndw.delstatus,SQLSHORT,0);
    sqlout ((short *) &a_kalkhndw.fil,SQLSHORT,0);
    sqlout ((double *) &a_kalkhndw.fil_ek_teilk,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkhndw.fil_ek_vollk,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkhndw.lad_vk_teilk,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkhndw.lad_vk_vollk,SQLDOUBLE,0);
    sqlout ((short *) &a_kalkhndw.mdn,SQLSHORT,0);
    sqlout ((double *) &a_kalkhndw.pr_ek1,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkhndw.pr_ek2,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkhndw.pr_ek3,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkhndw.sk_teilk,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkhndw.sk_vollk,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkhndw.sp_fil,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkhndw.sp_lad,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkhndw.sp_vk,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkhndw.we_me1,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkhndw.we_me2,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkhndw.we_me3,SQLDOUBLE,0);
//    sqlout ((DATE_STRUCT *) &a_kalkhndw.dat,SQLDATE,0);
    sqlout ((char *) &a_kalkhndw.dat,SQLCHAR,11);
            readcursor2_0 = sqlcursor ("select a_kalkhndw.a,  "
"a_kalkhndw.bearb_fil,  a_kalkhndw.bearb_lad,  a_kalkhndw.bearb_sk,  "
"a_kalkhndw.delstatus,  a_kalkhndw.fil,  a_kalkhndw.fil_ek_teilk,  "
"a_kalkhndw.fil_ek_vollk,  a_kalkhndw.lad_vk_teilk,  "
"a_kalkhndw.lad_vk_vollk,  a_kalkhndw.mdn,  a_kalkhndw.pr_ek1,  "
"a_kalkhndw.pr_ek2,  a_kalkhndw.pr_ek3,  a_kalkhndw.sk_teilk,  "
"a_kalkhndw.sk_vollk,  a_kalkhndw.sp_fil,  a_kalkhndw.sp_lad,  "
"a_kalkhndw.sp_vk,  a_kalkhndw.we_me1,  a_kalkhndw.we_me2,  "
"a_kalkhndw.we_me3,  a_kalkhndw.dat from a_kalkhndw "

#line 44 "a_preis.rpp"
                                  "where a_kalkhndw.a   = ? "
								  "and a_kalkhndw.mdn = 0 ");
}

int A_PREIS_CLASS::lesea_preis (void)
{
      if (reportrez.mat <= 1) return 0;
      if (reportrez.grouprez == 6) return 0;  


      int dsqlstatus = sqlfetch (readcursor);
	  if (dsqlstatus == 100)
	  {
		  dsqlstatus = sqlfetch (readcursor1);
		  if (dsqlstatus == 0)
		  {
			 a_preis.inh_ek = 1;
			 a_preis.bep = a_kalk_mat.hk_vollk;
			 a_preis.ek = a_kalk_mat.mat_o_b;
			 a_preis.plan_ek = a_preis.ek;
		  }
	  }
	  if (dsqlstatus == 100)
	  {
		  dsqlstatus = sqlfetch (readcursor1_0);
		  if (dsqlstatus == 0)
		  {
			 a_preis.inh_ek = 1;
			 a_preis.bep = a_kalk_mat.hk_vollk;
			 a_preis.ek = a_kalk_mat.mat_o_b;
			 a_preis.plan_ek = a_preis.ek;
		  }
	  }
	  if (dsqlstatus == 100)
	  {
		  dsqlstatus = sqlfetch (readcursor2);
		  if (dsqlstatus == 0)
		  {
			 a_preis.inh_ek = 1;
			 a_preis.bep = a_kalkhndw.sk_vollk;
			 a_preis.ek = a_kalkhndw.pr_ek1;
			 a_preis.plan_ek = a_preis.ek;
		  }
	  }
	  if (dsqlstatus == 100)
	  {
		  dsqlstatus = sqlfetch (readcursor2_0);
		  if (dsqlstatus == 0)
		  {
			 a_preis.inh_ek = 1;
			 a_preis.bep = a_kalkhndw.sk_vollk;
			 a_preis.ek = a_kalkhndw.pr_ek1;
			 a_preis.plan_ek = a_preis.ek;
		  }
	  }
	  return dsqlstatus;
}

int A_PREIS_CLASS::opena_preis (void)
{
		 sqlopen (readcursor1);
		 sqlopen (readcursor1_0);
		 sqlopen (readcursor2);
		 sqlopen (readcursor2_0);
         return sqlopen (readcursor);
}

