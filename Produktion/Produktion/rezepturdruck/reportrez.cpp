#include "stdafx.h"
#include "DbClass.h"
#include "reportrez.h"
#include "cmbtl9.h"

struct REPORTREZ reportrez, reportrez_null;
struct PRDK_K prdk_k;
struct A_BAS a_bas;
extern DB_CLASS dbClass;
static long anzzfelder = -1;
short mdn = 0;
double avon = 1.0;
double abis = 99999999.0;
int agvon = 1;
int agbis = 999999;
short variantevon = 0;
short variantebis = 9999;
short akvvon = 0;
short akvbis = 1;
double rez_schwund;
double rez_chargengewicht;
char rez_preis_bz[12];
char rez_bep_bz[12];
double rez_preis;
double sk_preis;
double rez_kosten;
char bearb_info[81];
short ohnePreis = 0;
short mitGB = 0;


int REPORTREZ_CLASS::dbcount (void)
/**
Tabelle eti lesen.
**/
{

         if (test_upd_cursor == -1)
         {
             prepare ();
         }
         dbClass.sqlopen (count_cursor);
         dbClass.sqlfetch (count_cursor);
         if (sqlstatus == 0)
         {
                      return anzzfelder;
         }
         return sqlstatus;   //050803
}

int REPORTREZ_CLASS::lesereportrez (int fehlercode)
{
	  if (fehlercode == LL_WRN_REPEAT_DATA)
	  {
         return 0;    // Hier kein fetch , da DS nicht mehr auf Seite passt , und nochmal 
					  // ausegeben werden mu�
	  }
      int di = dbClass.sqlfetch (readcursor);
	  if (reportrez.grouprez == 0)
      {
		  rez_schwund = reportrez.schwund;
		  rez_chargengewicht = reportrez.chargengewicht;
		  strcpy (rez_preis_bz, reportrez.k_a_bez);
		  strcpy (rez_bep_bz, reportrez.einheit);
		  rez_preis = reportrez.bep;
		  sk_preis = reportrez.wert_ek;
/***
          if (a_bas.me_einh != 2) 
          {
                 if (a_preis.kosteneinh > 0.0)
                 {
					rez_kosten = a_preis.kosteneinh;
                 }
                 else if (a_preis.kostenkg > 0.0)
                 {
					   rez_kosten = a_preis.kostenkg * a_bas.a_gew;
                 }
          }
          else
          {
                 if (a_preis.kostenkg > 0.0)
                 {
					   rez_kosten = a_preis.kostenkg;
                 }
                 else if (a_preis.kosteneinh > 0.0)
                 {
					   rez_kosten = a_preis.kosteneinh / a_bas.a_gew;
                 }
          }
******/

	  }
	  if (reportrez.mat > 1)
      { 
  		dbClass.sqlin ((long *) &reportrez.mat, SQLLONG, 0);
  		dbClass.sqlout ((short *) &a_bas.me_einh, SQLSHORT, 0);
  		dbClass.sqlout ((short *) &a_bas.lief_einh, SQLSHORT, 0);
  		dbClass.sqlout ((double *) &a_bas.a_gew, SQLDOUBLE, 0);
  		dbClass.sqlout ((double *) &a_bas.inh_lief, SQLDOUBLE, 0);
		dbClass.sqlcomm ("select me_einh,lief_einh,a_gew,inh_lief from a_bas,a_mat where a_bas.a = a_mat.a and a_mat.mat = ?");
      }
	  return di;
}

int REPORTREZ_CLASS::openreportrez (void)
{
         return dbClass.sqlopen (readcursor);
}

void REPORTREZ_CLASS::prepare (void)
{

  if (mitGB == 1)  //210807
  {
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((double *) &avon, SQLDOUBLE, 0);
	dbClass.sqlin ((long *) &agvon, SQLLONG, 0);
	dbClass.sqlin ((long *) &agbis, SQLLONG, 0);
	dbClass.sqlout ((long *)  &anzzfelder, SQLLONG, 4);

 //kostenart : hier kommt prdk_varb.typ rein : 2 = Grundbr�t
    count_cursor = dbClass.sqlcursor ("select count(*) from reportrez,a_bas,prdk_k "
										"where reportrez.mdn = ? and "
										"("
										"reportrez.a in (select a_mat.a from a_mat,reportrez "
										"where reportrez.mat = a_mat.mat and grouprez = 1 and kostenart = 2 and "
										"reportrez.a = ?) "
										")"
										"and a_bas.ag between ? and ? "
										"and reportrez.a = a_bas.a "
										"and prdk_k.akv = 1 "
										"and reportrez.mdn = prdk_k.mdn "
										"and reportrez.a = prdk_k.a "
										"and reportrez.variante = prdk_k.variante ");
										
	test_upd_cursor = 1;

	dbClass.sqlin ((double *) &avon, SQLDOUBLE, 0);
	dbClass.sqlin ((long *) &agvon, SQLLONG, 0);
	dbClass.sqlin ((long *) &agbis, SQLLONG, 0);
    dbClass.sqlout ((short *) &reportrez.grouprez,SQLSHORT,0);
    dbClass.sqlout ((double *) &reportrez.a,SQLDOUBLE,0);
    dbClass.sqlout ((char *) reportrez.a_bz1,SQLCHAR,25);
    dbClass.sqlout ((char *) reportrez.rez,SQLCHAR,9);
    dbClass.sqlout ((char *) reportrez.rez_bz,SQLCHAR,73);
    dbClass.sqlout ((short *) &reportrez.variante,SQLSHORT,0);
    dbClass.sqlout ((char *) reportrez.variante_bz,SQLCHAR,25);
    dbClass.sqlout ((char *) reportrez.preis_basis,SQLCHAR,9);
    dbClass.sqlout ((double *) &reportrez.schwund,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &reportrez.chargengewicht,SQLDOUBLE,0);
    dbClass.sqlout ((char *) reportrez.datum,SQLCHAR,11);
    dbClass.sqlout ((short *) &reportrez.posi,SQLSHORT,0);
    dbClass.sqlout ((long *) &reportrez.mat,SQLLONG,0);
    dbClass.sqlout ((char *) reportrez.mat_bz,SQLCHAR,25);
    dbClass.sqlout ((double *) &reportrez.menge,SQLDOUBLE,0);
    dbClass.sqlout ((char *) reportrez.einheit,SQLCHAR,21);
    dbClass.sqlout ((double *) &reportrez.ek,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &reportrez.wert_ek,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &reportrez.kosten,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &reportrez.bep,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &reportrez.wert_bep,SQLDOUBLE,0);
    dbClass.sqlout ((char *) reportrez.k_a_bez,SQLCHAR,13);
    dbClass.sqlout ((double *) &reportrez.k_diff,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &reportrez.kostenkg,SQLDOUBLE,0);
    dbClass.sqlout ((short *) &reportrez.kostenart,SQLSHORT,0);
    dbClass.sqlout ((char *) reportrez.kostenart_bz,SQLCHAR,31);
    dbClass.sqlout ((short *) &reportrez.mdn,SQLSHORT,0);
    dbClass.sqlout ((short *) &a_bas.ag,SQLSHORT,0);
    dbClass.sqlout ((short *) &a_bas.dr_folge,SQLSHORT,0);
    dbClass.sqlout ((short *) &prdk_k.akv,SQLSHORT,0);
    dbClass.sqlout ((short *) &prdk_k.chg_anz_bzg,SQLSHORT,0);
    dbClass.sqlout ((char *) prdk_k.rez_bz,SQLCHAR,73);
    dbClass.sqlout ((short *) &a_bas.me_einh,SQLSHORT,0);
//    dbClass.sqlout ((double *) &a_preis.kostenkg,SQLDOUBLE,0);
//    dbClass.sqlout ((double *) &a_preis.kosteneinh,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &a_bas.a_gew,SQLDOUBLE,0);

              readcursor = dbClass.sqlcursor ("select reportrez.grouprez,  "
"reportrez.a,  reportrez.a_bz1,  reportrez.rez,  reportrez.rez_bz,  "
"reportrez.variante,  reportrez.variante_bz,  reportrez.preis_basis,  "
"reportrez.schwund,  reportrez.chargengewicht,  reportrez.datum,  "
"reportrez.posi,  reportrez.mat,  reportrez.mat_bz,  reportrez.menge,  "
"reportrez.einheit,  reportrez.ek,  reportrez.wert_ek,  "
"reportrez.kosten,  reportrez.bep,  reportrez.wert_bep,  "
"reportrez.k_a_bez,  reportrez.k_diff,  reportrez.kostenkg,  "
"reportrez.kostenart,  reportrez.kostenart_bz,  reportrez.mdn, a_bas.ag, a_bas.dr_folge, "
"prdk_k.akv,prdk_k.chg_anz_bzg, prdk_k.rez_bz, a_bas.me_einh, a_bas.a_gew "
                                  "from reportrez,a_bas,prdk_k where "
										"("
										"reportrez.a in (select a_mat.a from a_mat,reportrez "
										"where reportrez.mat = a_mat.mat and grouprez = 1 and kostenart = 2 and "
										"reportrez.a = ?) "
										")"
    							  "and a_bas.ag between ? and ? " 
								  "and prdk_k.akv = 1 "
								  "and reportrez.a = a_bas.a "
								  "and reportrez.mdn = prdk_k.mdn "
								  "and reportrez.a = prdk_k.a "
								  "and reportrez.variante = prdk_k.variante "
								   " order by a,variante,grouprez,posi");
//								  " order by a_bas.ag,a_bas.dr_folge,a,variante,grouprez,posi");
  }
  else    // hier wie gehabt, ohne mitGB
  {
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((double *) &avon, SQLDOUBLE, 0);
	dbClass.sqlin ((double *) &abis, SQLDOUBLE, 0);
	dbClass.sqlin ((long *) &agvon, SQLLONG, 0);
	dbClass.sqlin ((long *) &agbis, SQLLONG, 0);
	dbClass.sqlin ((short *) &variantevon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &variantebis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &akvvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &akvbis, SQLSHORT, 0);
	dbClass.sqlout ((long *)  &anzzfelder, SQLLONG, 4);


    count_cursor = dbClass.sqlcursor ("select count(*) from reportrez,a_bas,prdk_k "
										"where reportrez.mdn = ? and reportrez.a between ? and ? "
										"and a_bas.ag between ? and ? "
										"and reportrez.a = a_bas.a "
										"and reportrez.variante between ? and ? "
										"and prdk_k.akv between ? and ? "
										"and reportrez.mdn = prdk_k.mdn "
										"and reportrez.a = prdk_k.a "
										"and reportrez.variante = prdk_k.variante ");
										
	test_upd_cursor = 1;

	dbClass.sqlin ((double *) &avon, SQLDOUBLE, 0);
	dbClass.sqlin ((double *) &abis, SQLDOUBLE, 0);
	dbClass.sqlin ((long *) &agvon, SQLLONG, 0);
	dbClass.sqlin ((long *) &agbis, SQLLONG, 0);
	dbClass.sqlin ((short *) &variantevon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &variantebis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &akvvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &akvbis, SQLSHORT, 0);
    dbClass.sqlout ((short *) &reportrez.grouprez,SQLSHORT,0);
    dbClass.sqlout ((double *) &reportrez.a,SQLDOUBLE,0);
    dbClass.sqlout ((char *) reportrez.a_bz1,SQLCHAR,25);
    dbClass.sqlout ((char *) reportrez.rez,SQLCHAR,9);
    dbClass.sqlout ((char *) reportrez.rez_bz,SQLCHAR,73);
    dbClass.sqlout ((short *) &reportrez.variante,SQLSHORT,0);
    dbClass.sqlout ((char *) reportrez.variante_bz,SQLCHAR,25);
    dbClass.sqlout ((char *) reportrez.preis_basis,SQLCHAR,9);
    dbClass.sqlout ((double *) &reportrez.schwund,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &reportrez.chargengewicht,SQLDOUBLE,0);
    dbClass.sqlout ((char *) reportrez.datum,SQLCHAR,11);
    dbClass.sqlout ((short *) &reportrez.posi,SQLSHORT,0);
    dbClass.sqlout ((long *) &reportrez.mat,SQLLONG,0);
    dbClass.sqlout ((char *) reportrez.mat_bz,SQLCHAR,25);
    dbClass.sqlout ((double *) &reportrez.menge,SQLDOUBLE,0);
    dbClass.sqlout ((char *) reportrez.einheit,SQLCHAR,21);
    dbClass.sqlout ((double *) &reportrez.ek,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &reportrez.wert_ek,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &reportrez.kosten,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &reportrez.bep,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &reportrez.wert_bep,SQLDOUBLE,0);
    dbClass.sqlout ((char *) reportrez.k_a_bez,SQLCHAR,13);
    dbClass.sqlout ((double *) &reportrez.k_diff,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &reportrez.kostenkg,SQLDOUBLE,0);
    dbClass.sqlout ((short *) &reportrez.kostenart,SQLSHORT,0);
    dbClass.sqlout ((char *) reportrez.kostenart_bz,SQLCHAR,31);
    dbClass.sqlout ((short *) &reportrez.mdn,SQLSHORT,0);
    dbClass.sqlout ((short *) &a_bas.ag,SQLSHORT,0);
    dbClass.sqlout ((short *) &a_bas.dr_folge,SQLSHORT,0);
    dbClass.sqlout ((short *) &prdk_k.akv,SQLSHORT,0);
    dbClass.sqlout ((short *) &prdk_k.chg_anz_bzg,SQLSHORT,0);
    dbClass.sqlout ((char *) prdk_k.rez_bz,SQLCHAR,73);
    dbClass.sqlout ((short *) &a_bas.me_einh,SQLSHORT,0);
//    dbClass.sqlout ((double *) &a_preis.kostenkg,SQLDOUBLE,0);
//    dbClass.sqlout ((double *) &a_preis.kosteneinh,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &a_bas.a_gew,SQLDOUBLE,0);

              readcursor = dbClass.sqlcursor ("select reportrez.grouprez,  "
"reportrez.a,  reportrez.a_bz1,  reportrez.rez,  reportrez.rez_bz,  "
"reportrez.variante,  reportrez.variante_bz,  reportrez.preis_basis,  "
"reportrez.schwund,  reportrez.chargengewicht,  reportrez.datum,  "
"reportrez.posi,  reportrez.mat,  reportrez.mat_bz,  reportrez.menge,  "
"reportrez.einheit,  reportrez.ek,  reportrez.wert_ek,  "
"reportrez.kosten,  reportrez.bep,  reportrez.wert_bep,  "
"reportrez.k_a_bez,  reportrez.k_diff,  reportrez.kostenkg,  "
"reportrez.kostenart,  reportrez.kostenart_bz,  reportrez.mdn, a_bas.ag, a_bas.dr_folge, "
"prdk_k.akv,prdk_k.chg_anz_bzg, prdk_k.rez_bz, a_bas.me_einh, a_bas.a_gew "
                                  "from reportrez,a_bas,prdk_k where reportrez.a between ? and ? "
	  							"and a_bas.ag between ? and ? "
									  "and reportrez.variante between ? and ? "
									  "and prdk_k.akv between ? and ? "
										"and reportrez.a = a_bas.a "
										"and reportrez.mdn = prdk_k.mdn "
										"and reportrez.a = prdk_k.a "
										"and reportrez.variante = prdk_k.variante "
//								   " order by a_bas.ag,a_bas.dr_folge,a,variante,grouprez,posi");
								   " order by a,variante,grouprez,posi");
  }
}

void REPORTREZ_CLASS::GetVerarbInfo (void)
/**
Verarbeitungstexte holen 
**/
{
 short kutf = atoi(reportrez.k_a_bez);

	strcpy(bearb_info, " ");
	if (reportrez.grouprez == 1)
    {
  		dbClass.sqlin ((short *) &reportrez.mdn, SQLSHORT, 0);
  		dbClass.sqlin ((char *) reportrez.rez, SQLCHAR, 8);
  		dbClass.sqlin ((short *) &reportrez.variante, SQLSHORT, 0);
  		dbClass.sqlin ((long *) &reportrez.mat, SQLLONG, 0);
  		dbClass.sqlin ((short *) &kutf, SQLSHORT, 0);
  		dbClass.sqlin ((short *) &reportrez.kostenart, SQLSHORT, 0);
  		dbClass.sqlout ((char *) bearb_info, SQLCHAR, 81);
		dbClass.sqlcomm ("select bearb_info from prdk_varb where mdn = ? and rez = ? and variante = ? and mat = ? and kutf = ? and typ = ?"); 
    }
	else if (reportrez.grouprez == 2)
    {
  		dbClass.sqlin ((short *) &reportrez.mdn, SQLSHORT, 0);
  		dbClass.sqlin ((char *) reportrez.rez, SQLCHAR, 8);
  		dbClass.sqlin ((short *) &reportrez.variante, SQLSHORT, 0);
  		dbClass.sqlin ((long *) &reportrez.mat, SQLLONG, 0);
  		dbClass.sqlin ((short *) &kutf, SQLSHORT, 0);
  		dbClass.sqlout ((char *) bearb_info, SQLCHAR, 81);
		dbClass.sqlcomm ("select bearb_info from prdk_zut where mdn = ? and rez = ? and variante = ? and mat = ? and kutf = ? "); 
    }
}