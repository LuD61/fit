#ifndef _REPORTREZ_DEF
#define _REPORTREZ_DEF

struct PRDK_K {
   short     akv;
   short	chg_anz_bzg;
   char		rez_bz[73];
};

struct A_BAS {
	short ag ;
	short dr_folge;
	short me_einh;
	short lief_einh;
	double inh_lief;
	double a_gew;
};

struct REPORTREZ {
   short     grouprez;
   double    a;
   char      a_bz1[25];
   char      rez[9];
   char      rez_bz[73];
   short     variante;
   char      variante_bz[25];
   char      preis_basis[9];
   double    schwund;
   double    chargengewicht;
   char      datum[11];
   short     posi;
   long      mat;
   char      mat_bz[25];
   double    menge;
   char      einheit[21];
   double    ek;
   double    wert_ek;
   double    kosten;
   double    bep;
   double    wert_bep;
   char      k_a_bez[13];
   double    k_diff;
   double    kostenkg;
   short     kostenart;
   char      kostenart_bz[31];
   short     mdn;
};
extern struct REPORTREZ reportrez, reportrez_null;
extern struct PRDK_K prdk_k;
extern struct A_BAS a_bas;

#line 10 "reportrez.rh"

class REPORTREZ_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int dbcount (void);
               int lesereportrez (int);
               int openreportrez (void);
               void GetVerarbInfo (void);
               REPORTREZ_CLASS () : DB_CLASS ()
               {
               }
};
#endif
