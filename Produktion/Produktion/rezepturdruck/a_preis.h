#ifndef _A_PREIS_DEF
#define _A_PREIS_DEF

struct A_PREIS {
   long      mdn;
   short     fil;
   double    a;
   short     prodabt;
   short     druckfolge;
   double    ek;
   double    plan_ek;
   double    kostenkg;
   double    kosteneinh;
   short     flg_bearb_weg;
   short     bearb_hk;
   short     bearb_p1;
   short     bearb_p2;
   short     bearb_p3;
   short     bearb_p4;
   short     bearb_p5;
   short     bearb_p6;
   short     bearb_vk;
   double    sp_sk;
   double    sp_fil;
   double    sp_vk;
   char      dat[12];
   char      zeit[6];
   char      pers_nam[9];
   long      inh_ek;
   double    bep;
   double    plan_bep;
   double    test_ek;
};
extern struct A_PREIS a_preis, a_preis_null;

#line 5 "a_preis.rh"
struct A_KALK_MAT {
   double    a;
   short     bearb_sk;
   short     delstatus;
   short     fil;
   double    hk_teilk;
   double    hk_vollk;
   double    kost;
   double    mat_o_b;
   short     mdn;
   double    sp_hk;
   char      dat[12];
};
extern struct A_KALK_MAT a_kalk_mat, a_kalk_mat_null;

#line 6 "a_preis.rh"
struct A_KALKHNDW {
   double    a;
   short     bearb_fil;
   short     bearb_lad;
   short     bearb_sk;
   short     delstatus;
   short     fil;
   double    fil_ek_teilk;
   double    fil_ek_vollk;
   double    lad_vk_teilk;
   double    lad_vk_vollk;
   short     mdn;
   double    pr_ek1;
   double    pr_ek2;
   double    pr_ek3;
   double    sk_teilk;
   double    sk_vollk;
   double    sp_fil;
   double    sp_lad;
   double    sp_vk;
   double    we_me1;
   double    we_me2;
   double    we_me3;
   char      dat[12];
};
extern struct A_KALKHNDW a_kalkhndw, a_kalkhndw_null;

#line 7 "a_preis.rh"

class A_PREIS_CLASS : public DB_CLASS
{
       private :
       public :
               void prepare (void);
               int lesea_preis (void);
               int opena_preis (void);
               A_PREIS_CLASS () : DB_CLASS ()
               {
               }
};
#endif
