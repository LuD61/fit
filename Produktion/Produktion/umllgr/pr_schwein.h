#ifndef _PR_SCHWEIN_DEF
#define _PR_SCHWEIN_DEF

#include "dbclass.h"

struct PR_SCHWEIN {
   short     mdn;
   short     zuord_nr;
   short     ag;
   double    grundpr;
   double    grund_mfa;
   double    ideal_von;
   double    ideal_bis;
   double    grenz_mfa1;
   double    grenz_mfa2;
   double    grenz_mfa3;
   double    grenz_kg1;
   double    grenz_kg2;
   double    grenz_kg3;
   double    von_kg1;
   double    von_kg2;
   double    von_kg3;
   double    von_kg4;
   double    bis_kg1;
   double    bis_kg2;
   double    bis_kg3;
   double    bis_kg4;
   double    zuschl_kg1;
   double    zuschl_kg2;
   double    zuschl_kg3;
   double    zuschl_kg4;
   double    von_mfa1;
   double    von_mfa2;
   double    von_mfa3;
   double    von_mfa4;
   double    bis_mfa1;
   double    bis_mfa2;
   double    bis_mfa3;
   double    bis_mfa4;
   double    zuschl_mfa1;
   double    zuschl_mfa2;
   double    zuschl_mfa3;
   double    zuschl_mfa4;
   short     delstatus;
   double    von_kg5;
   double    von_kg6;
   double    bis_kg5;
   double    bis_kg6;
   double    zuschl_kg5;
   double    zuschl_kg6;
   double    von_mfa5;
   double    von_mfa6;
   double    bis_mfa5;
   double    bis_mfa6;
   double    zuschl_mfa5;
   double    zuschl_mfa6;
};
extern struct PR_SCHWEIN pr_schwein, pr_schwein_null;

#line 7 "pr_schwein.rh"

class PR_SCHWEIN_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               PR_SCHWEIN_CLASS () : DB_CLASS ()
               {
               }
               double HoleAufschlaege (int,double,double);
};
#endif

