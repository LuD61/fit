#ifndef _LGR_UMLGRP_DEF
#define _LGR_UMLGRP_DEF

#include "dbclass.h"

struct LGR_UMLGRP {
   short     delstatus;
   double    a;
   short     mdn;
   double    me;
   double    a_na;
   double    me_na;
   double    pr_ek_von;
   double    pr_ek_na;
   double    pr_vk_von;
   double    pr_vk_na;
   long      txt_nr;
   long      umlgr_nr;
   char      zeit[11];
   long      int_pos;
   char      chargennr[14];
   long      dat;
};
extern struct LGR_UMLGRP lgr_umlgrp, lgr_umlgrp_null;

#line 7 "lgr_umlgrp.rh"
struct BSD_BUCH {
   long      nr;
   char      blg_typ[3];
   short     mdn;
   short     fil;
   short     kun_fil;
   double    a;
   long      dat;
   char      zeit[9];
   char      pers[13];
   char      bsd_lgr_ort[13];
   short     qua_status;
   double    me;
   double    bsd_ek_vk;
   char      chargennr[14];
   char      ident_nr[14];
   char      herk_nachw[14];
   char      lief[17];
   long      auf;
   char      verfall[2];
   long      verf_dat;
   short     delstatus;
   char      err_txt[17];
   double    me2;
   short     me_einh2;
   long      lieferdat;
};
extern struct BSD_BUCH bsd_buch, bsd_buch_null;

#line 8 "lgr_umlgrp.rh"

class LGR_UMLGRP_CLASS : public DB_CLASS
{
       private :
               int cursor_o;
               int ins_buch_cursor;
               void prepare (void);
               void prepare_o (char *);
       public :
               LGR_UMLGRP_CLASS () : DB_CLASS (), cursor_o (-1)
               {
               }
               int dbreadfirst (void);
               int dbreadfirst_o (char *);
               int dbread_o (void);
               int dbclose_o (void);
               int dbupdate (void);
               void InsBsdBuch (int,double,char*);
};
#endif
