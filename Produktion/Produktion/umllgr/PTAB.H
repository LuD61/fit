/*   Datenbankfunktionen                                  */

struct PTABK {
   char      ptitem[19];
   char      ptbenutz[9];
   char      pttitle[19];
   long      ptanzpos;
   char      ptdtyp[16];
   char      ptwer1[9];
   char      ptwer2[9];
   short     delstatus;
   long      akv;
   long      bearb;
   char      pers_nam[9];
};

struct PTABN {
   char      ptitem[19];
   long      ptlfnr;
   char      ptwert[4];
   char      ptbez[33];
   char      ptbezk[9];
   char      ptwer1[9];
   char      ptwer2[9];
   short     delstatus;
   long      akv;
   long      bearb;
   char      pers_nam[9];
};


extern struct PTABK ptabk;
extern struct PTABN ptabn, ptabn_null;

class PTAB_CLASS
{
       private :
            short cursor_ptabk;
            short cursor_ptabn;
            short cursor_ptabn_all;
       public:
           PTAB_CLASS ()
           {
                    cursor_ptabk = -1;
                    cursor_ptabn = -1;
                    cursor_ptabn_all = -1;
           }
           void prepare (void);
           int lese_ptab (char *, char *);
           int lese_ptab (void);
           int lese_ptab_all (char *);
           int lese_ptab_all (void);
};
