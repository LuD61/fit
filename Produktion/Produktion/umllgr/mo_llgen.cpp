#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include <time.h>
#include "wmask.h"
#include "70001.h"
#include "strfkt.h"                
#include "stdfkt.h"
#include "dbfunc.h"
//#include "formfeld.h"
#include "mo_menu.h"


#define MAXTAB 100


static int dsqlstatus;
static int label = 0;

/* AAAA  */
static char iformname[30] ;
static char outmodus[30] ;
static int anzlpsz ;
static int ubzeilen ;
static int MenStart ;
static int MenScroll ;

static char akt_drucker [40] [64];
static int drkanz = 0;
static int ausgabe = PRINTER_OUT;

static char  feld_tab_txt [15][25] ;
static char feld_feld_txt [15][25] ;

static char feld1_von_txt [40];
static char feld2_von_txt [40];
static char feld3_von_txt [40];
static char feld4_von_txt [40];
static char feld5_von_txt [40];
static char feld6_von_txt [40];
static char feld7_von_txt [40];
static char feld8_von_txt [40];
static char feld9_von_txt [40];
static char feld10_von_txt [40];
static char feld11_von_txt [40];
static char feld12_von_txt [40];
static char feld13_von_txt [40];
static char feld14_von_txt [40];
static char feld15_von_txt [40];

static char feld1_von [40];
static char feld1_bis [40];
static char feld2_von [40];
static char feld2_bis [40];
static char feld3_von [40];
static char feld3_bis [40];
static char feld4_von [40];
static char feld4_bis [40];
static char feld5_von [40];
static char feld5_bis [40];
static char feld6_von [40];
static char feld6_bis [40];
static char feld7_von [40];
static char feld7_bis [40];
static char feld8_von [40];
static char feld8_bis [40];
static char feld9_von [40];
static char feld9_bis [40];
static char feld10_von [40];
static char feld10_bis [40];
static char feld11_von [40];
static char feld11_bis [40];
static char feld12_von [40];
static char feld12_bis [40];
static char feld13_von [40];
static char feld13_bis [40];
static char feld14_von [40];
static char feld14_bis [40];
static char feld15_von [40];
static char feld15_bis [40];

char *feld_von[] = {feld1_von,
                    feld2_von,
                    feld3_von,
                    feld4_von,
                    feld5_von,
                    feld6_von,
                    feld7_von,
                    feld8_von,
                    feld9_von,
                    feld10_von,
                    feld11_von,
                    feld12_von,
                    feld13_von,
                    feld14_von,
                    feld15_von
                   };

char *feld_bis[] = {feld1_bis,
                    feld2_bis,
                    feld3_bis,
                    feld4_bis,
                    feld5_bis,
                    feld6_bis,
                    feld7_bis,
                    feld8_bis,
                    feld9_bis,
                    feld10_bis,
                    feld11_bis,
                    feld12_bis,
                    feld13_bis,
                    feld14_bis,
                    feld15_bis
                   };

static int feld_nr = 0;
static char akt_form[20];

static int textdatei = 0;
static int windatei = 0;
static int exeldatei = 0;

static int SetWin (void);
static int SetText (void);
static int SetExel (void);
static int BreakBox (void);


//FORMFELD_CLASS formfeld_class;
//FORMFRM_CLASS formfrm_class;
//FORMHEAD_CLASS formhead_class;

static int error;



void DruckeFile (void)
/**
Hier wird der Listgenerator in Rosi oder C aufgrufen.
**/
{

  FILE *lst ;
  char bfnam[22] ;
  char datenam[22] ;
  char buffer [512] ;
  char zeilbuffer[80] ;
  char komma[128] ;
  int funki ;
  int schleifi ;
  char zwi_von[25] ;
  char zwi_bis[25] ;
//  funki = formfrm_class.lese_formfrm (iformname);
  
  time_t t = GetCurrentTime() ;
  sprintf (bfnam,"%dL", t ) ;
  sprintf ( datenam , "%s.llf", iformname ) ;

  if (getenv ("TMPPATH"))
  {
    sprintf (buffer, "%s\\%s", getenv("TMPPATH"), datenam);
  }
  else
  {
    strcpy (buffer, datenam );
  }
  remove (buffer) ;
  if ( (lst = fopen (buffer, "wt" )) == NULL ) 
  {
                     print_mess (2, "%s kann nicht ge�ffnet werden", buffer);
                     return ;
  }

  for ( funki = 0 ; funki < 5 ; funki ++ )
  {
    switch (funki)
    {
    case 0:             /* Formatname */
        memset ( zeilbuffer, '\0', 128 ) ;
        sprintf ( zeilbuffer, "NAME %s" , iformname ) ;
        fprintf ( lst , "%s\n" , zeilbuffer ) ;
        break ;
    case 1:             /* Druckauswahl */
        memset ( zeilbuffer, '\0', 128 ) ;
        sprintf ( zeilbuffer, "DRUCK %hd" , 0 ) ;
        fprintf ( lst , "%s\n" , zeilbuffer ) ;
        break ;
    case 2:             /* Liste, nicht label */
        memset ( zeilbuffer, '\0', 128 ) ;
        sprintf ( zeilbuffer, "LABEL %hd" , label ) ;
        fprintf ( lst , "%s\n" , zeilbuffer ) ;
        break ;
    case 3:             /* Text */
        memset ( zeilbuffer, '\0', 80 ) ;
        sprintf ( zeilbuffer, "%-16s %12s %12s","#Feldname", "von", "bis") ;
        fprintf ( lst , "%s\n" , zeilbuffer ) ;
        break ;

    case 4:             /* Bereiche */
        for ( schleifi = 0; schleifi < feld_nr ; schleifi++ )
        { 
          switch (schleifi)
          {
            case 0:
                sprintf ( zwi_von , "%s" , feld1_von ) ;
                sprintf ( zwi_bis  ,"%s" , feld1_bis ) ;
                break ;
            case 1:
                sprintf ( zwi_von , "%s" , feld2_von ) ;
                sprintf ( zwi_bis  ,"%s" , feld2_bis ) ;
                break ;                                       
            case 2:
                sprintf ( zwi_von , "%s" , feld3_von ) ;
                sprintf ( zwi_bis  ,"%s" , feld3_bis ) ;
                break ;
            case 3:
                sprintf ( zwi_von , "%s" , feld4_von ) ;
                sprintf ( zwi_bis  ,"%s" , feld4_bis ) ;
                break ;
            case 4:
                sprintf ( zwi_von , "%s" , feld5_von ) ;
                sprintf ( zwi_bis  ,"%s" , feld5_bis ) ;
                break ;
            case 5:
                sprintf ( zwi_von , "%s" , feld6_von ) ;
                sprintf ( zwi_bis  ,"%s" , feld6_bis ) ;
                break ;
            case 6:
                sprintf ( zwi_von , "%s" , feld7_von ) ;
                sprintf ( zwi_bis  ,"%s" , feld7_bis ) ;
                break ;
            case 7:
                sprintf ( zwi_von , "%s" , feld8_von ) ;
                sprintf ( zwi_bis  ,"%s" , feld8_bis ) ;
                break ;
            case 8:
                sprintf ( zwi_von , "%s" , feld9_von ) ;
                sprintf ( zwi_bis  ,"%s" , feld9_bis ) ;
                break ;
            case 9:
                sprintf ( zwi_von , "%s" , feld10_von ) ;
                sprintf ( zwi_bis  ,"%s" , feld10_bis ) ;
                break ;
            case 10:
                sprintf ( zwi_von , "%s" , feld11_von ) ;
                sprintf ( zwi_bis  ,"%s" , feld11_bis ) ;
                break ;
            case 11:
                sprintf ( zwi_von , "%s" , feld12_von ) ;
                sprintf ( zwi_bis  ,"%s" , feld12_bis ) ;
                break ;
            case 12:
                sprintf ( zwi_von , "%s" , feld13_von ) ;
                sprintf ( zwi_bis  ,"%s" , feld13_bis ) ;
                break ;
            case 13:
                sprintf ( zwi_von , "%s" , feld14_von ) ;
                sprintf ( zwi_bis  ,"%s" , feld14_bis ) ;
                break ;
            case 14:
                sprintf ( zwi_von , "%s" , feld15_von ) ;
                sprintf ( zwi_bis  ,"%s" , feld15_bis ) ;
                break ;
            default:
                memset ( zwi_von , '\0', 25 ) ;
                memset ( zwi_bis , '\0', 25 ) ;
                break ;
          }     /* END switch */
          memset ( zeilbuffer, '\0', 80 ) ;
          sprintf ( zeilbuffer, "%-16s %12s %12s",feld_feld_txt[schleifi], zwi_von, zwi_bis) ;
          fprintf ( lst , "%s\n" , zeilbuffer ) ;
        }      /* END schleifi-while */
        break ;
        }       /* ende switch */
    }          /* ende for funki */
    fclose ( lst ) ;

	/*
    if (getenv ("TMPPATH"))
    {
      sprintf ( komma, "%s\\%s.sta", getenv ("TMPPATH"), iformname);
    }
    else
    {
      strcpy (komma, "liste.lst");
    }
    remove (komma) ;
	*/



  if (getenv ("TMPPATH"))
  {
    sprintf (komma, "%s -name %s -datei %s\\%s","dr70001",iformname, getenv("TMPPATH"), datenam);
  }
  else
  {
    strcpy (komma, datenam );
  }

    HCURSOR oldcursor = SetCursor ( LoadCursor ( NULL , IDC_WAIT)) ;

    ProcWaitExecEx ( komma, SW_SHOWNORMAL, 0, 40, -1, 0);
}

void WaitListEnd (void)
/**
Auf Ende der Listenerstellung warten.
**/
{
          FILE *ls;
          char buffer [512];


          if (getenv ("TMPPATH"))
          {
                        sprintf (buffer, "%s\\%s.sta",
                                 getenv ("TMPPATH"), iformname);
          }
          else
          {
                        strcpy (buffer, "liste.lst");
          }

          HCURSOR oldcursor =SetCursor ( LoadCursor ( NULL , IDC_WAIT)) ;

          ls = fopen (buffer, "r");
          if (ls == (FILE *) 0)
          {
                   return;
          }

     /*
          while (ls == (FILE *) 0)
          {
            Sleep ((DWORD) 1500) ;  
            ls = fopen (buffer, "r") ;
          }
     */

          SetCursor ( oldcursor ) ;

          error = ubzeilen = MenStart = 0;
          if (fgets (buffer, 255, ls))
          {
                        cr_weg (buffer);
                        error =  atoi (buffer);
          }
          if (fgets (buffer, 255, ls))
          {
                        cr_weg (buffer);
                        ubzeilen =  atoi (buffer);
          }
          if (fgets (buffer, 255, ls))
          {
                        cr_weg (buffer);
                        MenStart =  atoi (buffer);
          }
          MenScroll = 0;
          if (fgets (buffer, 255, ls))
          {
                        cr_weg (buffer);
                        MenScroll =  atoi (buffer);
          }
          fclose (ls);
          return;
}

int IsError ()
/**
Fehler bei Druck testen.
**/
{
           static char *errtab []= {"",
                                    "Liste zu breit",
                                    "Liste abgebrochen",
                                    "Liste abgebrochen",
                                    "Fehler beim Einlesen der temp table",
                                    "Fehler beim Einlesen der Adress-Tabelle",
                                    "Fehler beim Aktivieren des Druckers",
                                    "Fehler beim Lesen der Steuerzeichen"};

           static char *NoData =    "Keine Daten vorhanden";
           static char *SQL    =    "Fehler beim Ausf�hren des SQL-Befehls";             

          if (error == 0)
          {
                    return (0);
          }
          else if (error < 7 && error > 0)
          {
                    disp_mess (errtab [error], 2);
                    return (1);
          }
          else if (error == 100)
          {
                    disp_mess (NoData, 2);
                    return (1);
          }
          else
          {
                    disp_mess (SQL, 2);
                    return (1);
          }
          return (1);
}


void print_file (void)
/**
Druck am Bildschirm.
**/
{
         DruckeFile ();
         WaitListEnd ();
         IsError ();
}


void printerfile (void)
/**
Liste auf Drucker schicken
**/
{
          char buffer [512];

          WaitListEnd ();
          if (IsError ()) return; 

          if (getenv ("TMPPATH"))
          {
             sprintf (buffer, "fitprint %s\\%s.lst %s",
                      getenv ("TMPPATH"), iformname, sys_ben.pers_nam);
          }
          else
          {
             sprintf (buffer, "fitprint liste.lst %s", sys_ben.pers_nam);
          }

          ProcWaitExecEx (buffer, SW_SHOW, -1, 0, -1, 0);  
}



BOOL LeseFormat (char *form_nr)
/**
Format einlesen.
**/
{

	/*
      strcpy (form_head.form_nr, form_nr);
      formhead_class.dbreadfirst ();
      dsqlstatus = formfeld_class.lese_formfeld (form_nr);
      if (dsqlstatus == 100)
      {
               print_mess (2, "Format %s nicht gefunden", form_nr); 
               return FALSE; 
      }
      strcpy (akt_form, form_nr);
      clipped (akt_form);
      strcpy (iformname, akt_form);
      feld_nr = 0;
	  */
      strcpy (iformname, form_nr);
      feld_nr = 0;
      return TRUE;
}

BOOL FillFormFeld (char *feldname, char *valfrom, char *valto)
/**
Formfeld fuellen.
**/
{
        int anz;
//        int dsqlstatus;

        char tab_nam [21];
        char feld_nam [21];

        anz = wsplit (feldname, ".");
        if (anz < 2) return FALSE;
 
        strcpy (tab_nam, wort[0]); 
        strcpy (feld_nam, wort[1]); 
/**
        dsqlstatus = formfeld_class.lese_feld (akt_form, tab_nam, feld_nam);
        if (dsqlstatus == 100)
        {
                       return FALSE;
        }
		*/
        
        sprintf ( feld_tab_txt[feld_nr],  "%s", tab_nam )  ;
        sprintf ( feld_feld_txt[feld_nr], "%s", feld_nam) ;
        sprintf (feld_von[feld_nr],       "%s", valfrom);
        sprintf (feld_bis[feld_nr],       "%s", valto);
        feld_nr ++;
        return TRUE;
}





void StartPrint  (void)
/**
Drucken.
**/
{
                            DruckeFile ();
                            printerfile();
}




void SetAusgabe (int ausg)
/**
Ausgabe-Medium setzen.
**/
{
	   ausgabe = ausg;
}

