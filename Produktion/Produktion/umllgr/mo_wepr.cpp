/*
-----------------------------------------------------------------------------
-
-	Modulname		:	mo_wepr
-
-       Autor                   :       W. Roth
-       Erstellungsdatum        :       11.10.99
-       Modifikationsdatum      :       11.10.99
-
-	Projekt			:	BWS
-	Version			:	1.03
-	Laendervariante		:	BRD
-
-       Sprache                 :       C++
-
-	Aenderungsjournal	:
-
-	lfd. -	Version	Datum	    Name	Beschreibung
-       ----------------------------------------------------------------------

-
------------------------------------------------------------------------------
-
-	Modulbeschreibung	:  	Prozeduren zur Preisfindung fuer
-					den Wareneingang
-
-					FIT - VERSION
-
------------------------------------------------------------------------------
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include "wmask.h"
#include "strfkt.h"
#include "mo_curso.h"
#include "mdn.h"
#include "fil.h"
#include "we_kopf.h"
#include "we_pos.h"
#include "mo_wepr.h"
#include "lief.h"
#include "lief_bzg.h"

extern int _WMDEBUG;
extern int writelog (char *, ...);
extern void disp_mess (char *, int modus);

static LIEF_BZG_CLASS LiefBzg;
static LIEF_CLASS LiefClass;
static DB_CLASS DbClass;

/*---------------------------*/
/*--- P R O C E D U R E N ---*/
/*---------------------------*/


int WE_PREISE::preis_holen (short dmdn, short dfil, char *dlief,
                            double da)
/**
Ek-Preis hierarchisch holen.
**/
{
             int dsqlstatus;
  
             lief_bzg.mdn = dmdn;
             lief_bzg.fil = dfil;
             lief_bzg.a   = da;
             strcpy (lief_bzg.lief, dlief);
             dsqlstatus = LiefBzg.dbreadfirst ();
             while (dsqlstatus == 100)
             {
                        if (lief_bzg.fil)
                        {
                                   lief_bzg.fil = 0;
                        }
                        else if (lief_bzg.mdn)
                        {
                                   lief_bzg.mdn = 0;
                        }
                        else
                        {
                                   break;
                        }   
                        dsqlstatus = LiefBzg.dbreadfirst ();
              }
              return dsqlstatus;
}


int WE_PREISE::fetchrab (int cursor)
{
	          int dsqlstatus;

	          DbClass.sqlopen (cursor);
	          dsqlstatus = DbClass.sqlfetch (cursor);
			  while (dsqlstatus == 100)
			  {
				  if (rfil > 0)
				  {
					  rfil = 0;
				  }
				  else if (rmdn > 0)
				  {
					  rmdn = 0;
				  }
				  else
				  {
					  break;
				  }
  	              DbClass.sqlopen (cursor);
	              dsqlstatus = DbClass.sqlfetch (cursor);
			  }
			  return dsqlstatus;
}


double WE_PREISE::GetRabEk (short mdn, short fil, char *lieferant, 
							 double inh, double pr_ek, int me_kz)
/**
Generalrabatte verrechnen.
**/
{
	          int kette_curs;
			  int gruppe_curs;
 	          int dsqlstatus;
			  double rab;
			  short lfd;
          
              rmdn = mdn;
			  rfil = fil;
              DbClass.sqlin ((short *) &rmdn, 1, 0); 
              DbClass.sqlin ((short *) &rfil, 1, 0); 
              DbClass.sqlin ((char *)  lieferant, 0, 17); 
              DbClass.sqlout ((double *)  &proz_sum, 3, 0); 
              DbClass.sqlout ((double *)  &lfd, 1, 0); 
              kette_curs = DbClass.sqlcursor ("select proz_sum,lfd from lief_r_kte "
				                              "where mdn = ? "
											  "and fil = ? "
											  "and lief = ? "
											  "order by lfd"); 

              DbClass.sqlin ((short *) &rmdn, 1, 0); 
              DbClass.sqlin ((short *) &rfil, 1, 0); 
              DbClass.sqlin ((char *)  lieferant, 0, 17); 
              DbClass.sqlout ((double *)  &rab_proz, 3, 0); 
              DbClass.sqlout ((double *)  &lfd, 1, 0); 
              gruppe_curs = DbClass.sqlcursor ("select rab_proz, lfd from lief_r_gr "
				                               "where mdn = ? "
								  			   "and fil = ? "
											   "and lief = ? "
											   "order by lfd");
			  
			  if (inh == 0) inh = 1;
			  proz_sum = rab_proz = (double) 0.0;
              if (strcmp (lief.rab_abl, "G") == 0)
			  {
                         dsqlstatus = fetchrab (gruppe_curs);
                         if (IsDoublenull (rab_proz)) rab_proz = (double) 0.0;
                         if (me_kz == 2)
						 {
							 rab = (double) (pr_ek / inh) * (rab_proz / 100);
						 }
						 else
						 {
							 rab = (double) pr_ek * (rab_proz / 100);
						 }
			  }
              else if (strcmp (lief.rab_abl, "K") == 0)
			  {
                         dsqlstatus = fetchrab (kette_curs);
                         if (IsDoublenull (proz_sum)) proz_sum = (double) 0.0;
                         if (me_kz == 2)
						 {
//							 rab = (double) (pr_ek / inh) * (proz_sum / 100);
							 rab = (double) pr_ek * (proz_sum / 100);
						 }
                         else
						 {
							 rab = (double) pr_ek * (proz_sum / 100);
						 }
			  }
			  DbClass.sqlclose (gruppe_curs);
			  DbClass.sqlclose (kette_curs);
			  return rab;
}


