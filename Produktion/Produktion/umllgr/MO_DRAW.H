#ifndef DRAWOK
#define DRAWOK

class FIGURES
      {
           private :
               POINT hexapoint [6];
               POINT dim3 [20];
               int rand;
               int shad;
               double stretch;
               struct HEXASTRUCT *hexastructs [1000];
               void PrintText (struct HEXASTRUCT *, HDC, char *, int, int,
                               COLORREF, COLORREF, int);
               void PrintHexaText (HDC, HWND, char *);
               void PaintHexaRand (HDC, int);
               void SetSpezColor (COLORREF, COLORREF, HDC);
          public :
               FIGURES ()
               {
                          rand = 10;
                          stretch = 0.7;
                          shad = 100;
               }
               void SetRand (int);
               void DisplayHexaText (HWND hWnd, HDC hdc);
               void PaintHexagon (HDC, int, int, int, COLORREF);
               void PaintHexagon3Dim (HDC, int, int, int, COLORREF, int);
               void DelHexagon (HDC, int, int, int, HBRUSH);
               void DelHexagon3Dim (HDC, int, int, int, HBRUSH, int);
               HWND CreateHexagon (HWND, int, int, int, COLORREF, int);
               void DestroyHexagon (HWND);
               void UpdateHexagon (HWND, HWND);
               void PaintAllHexagon (HWND, HDC);
               void SetHexaFont (HWND, HFONT);
               int MouseInHexagon (HWND, POINT *);
               HFONT GetHexaFont (HWND);
               HWND GetHexaParent (HWND);
};

class BMAP
{
          public :
			  DWORD drawmode;
              HGLOBAL hglbDlg;
              BITMAPINFO FAR *bmap;
              HBITMAP hBitmap;
              HBITMAP hBitmapOrg;
              HBITMAP hBitmapZoom;
              HWND bmphWnd;

              BMAP () : hBitmap (NULL), hBitmapZoom (NULL),
                        bmap (NULL)
              {
              }

              HBITMAP ReadBitmap (HDC, char *);
              void DestroyBitmap (void);
              void BitmapSize (HDC,POINT *);
              void DrawBitmap (HDC, int, int);
              void DrawBitmapMask (HDC, int, int, HBITMAP, DWORD, DWORD);
              void DrawBitmapEx (HDC, int, int, DWORD);
              void DrawBitmap (HDC, HBITMAP, int, int);
              void StrechBitmap (HDC, int, int);
              void StrechBitmap (HWND, int, int, int, int);
              void DrawBitmapPart (HDC, int, int, int, int, int, int);
              void DrawBitmapPart (HDC, HBITMAP, 
                                   int, int, int, int, int, int);
              void ZoomBitmap (HDC, int, int, int, int, int, int, double);
              void StrechBitmapMem (HWND, int, int);
};      

#endif  // DRAWOK
