#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmask.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "a_hndw.h"
#include "dbfunc.h"

struct A_HNDW a_hndw, a_hndw_null;
struct A_EIG a_eig, a_eig_null;
struct A_EIG_DIV a_eig_div, a_eig_div_null;
struct A_PFA a_pfa, a_pfa_null;
struct A_LEER a_leer, a_leer_null;

void HNDW_CLASS::prepare_hndw  (void)
{
    ins_quest ((char *) &a_hndw.a, 3, 0);
    out_quest ((char *) &a_hndw.a,3,0);
    out_quest ((char *) a_hndw.a_grnd,0,2);
    out_quest ((char *) &a_hndw.a_krz,2,0);
    out_quest ((char *) &a_hndw.a_leer,3,0);
    out_quest ((char *) &a_hndw.a_pfa,3,0);
    out_quest ((char *) a_hndw.a_pfa_kz,0,2);
    out_quest ((char *) &a_hndw.anz_reg_eti,1,0);
    out_quest ((char *) &a_hndw.anz_theke_eti,1,0);
    out_quest ((char *) &a_hndw.anz_waren_eti,1,0);
    out_quest ((char *) &a_hndw.delstatus,1,0);
    out_quest ((char *) &a_hndw.fil,1,0);
    out_quest ((char *) &a_hndw.gew_bto,3,0);
    out_quest ((char *) &a_hndw.herk_land,1,0);
    out_quest ((char *) a_hndw.hdkl,0,5);
    out_quest ((char *) &a_hndw.inh,3,0);
    out_quest ((char *) a_hndw.mwst_ueb,0,2);
    out_quest ((char *) &a_hndw.me_einh_kun,1,0);
    out_quest ((char *) &a_hndw.mdn,1,0);
    out_quest ((char *) a_hndw.pr_ausz,0,2);
    out_quest ((char *) a_hndw.pr_man,0,2);
    out_quest ((char *) a_hndw.pr_ueb,0,2);
    out_quest ((char *) a_hndw.reg_eti,0,2);
    out_quest ((char *) &a_hndw.sg1,1,0);
    out_quest ((char *) &a_hndw.sg2,1,0);
    out_quest ((char *) a_hndw.smt,0,2);
    out_quest ((char *) &a_hndw.tara,3,0);
    out_quest ((char *) a_hndw.theke_eti,0,2);
    out_quest ((char *) &a_hndw.verk_beg,2,0);
    out_quest ((char *) a_hndw.verk_art,0,2);
    out_quest ((char *) a_hndw.vk_typ,0,2);
    out_quest ((char *) a_hndw.vpk,0,2);
    out_quest ((char *) a_hndw.waren_eti,0,2);
    cursor_a_hndw = prepare_sql ("select a_hndw.a,  "
"a_hndw.a_grnd,  a_hndw.a_krz,  a_hndw.a_leer,  a_hndw.a_pfa,  "
"a_hndw.a_pfa_kz,  a_hndw.anz_reg_eti,  a_hndw.anz_theke_eti,  "
"a_hndw.anz_waren_eti,  a_hndw.delstatus,  a_hndw.fil,  a_hndw.gew_bto,  "
"a_hndw.herk_land,  a_hndw.hdkl,  a_hndw.inh,  a_hndw.mwst_ueb,  "
"a_hndw.me_einh_kun,  a_hndw.mdn,  a_hndw.pr_ausz,  a_hndw.pr_man,  "
"a_hndw.pr_ueb,  a_hndw.reg_eti,  a_hndw.sg1,  a_hndw.sg2,  a_hndw.smt,  "
"a_hndw.tara,  a_hndw.theke_eti,  a_hndw.verk_beg,  a_hndw.verk_art,  "
"a_hndw.vk_typ,  a_hndw.vpk,  a_hndw.waren_eti from a_hndw "
                                    "where a = ?");
}
                        
void HNDW_CLASS::prepare_eig (void)
{
    ins_quest ((char *) &a_eig.a, 3, 0);
    out_quest ((char *) &a_eig.a,3,0);
    out_quest ((char *) &a_eig.a_krz,2,0);
    out_quest ((char *) &a_eig.anz_theke_eti,1,0);
    out_quest ((char *) &a_eig.bem_offs,2,0);
    out_quest ((char *) &a_eig.delstatus,1,0);
    out_quest ((char *) &a_eig.fil,1,0);
    out_quest ((char *) &a_eig.gew_bto,3,0);
    out_quest ((char *) &a_eig.inh,3,0);
    out_quest ((char *) &a_eig.mat,2,0);
    out_quest ((char *) &a_eig.mdn,1,0);
    out_quest ((char *) &a_eig.me_einh_ek,1,0);
    out_quest ((char *) a_eig.mwst_ueb,0,2);
    out_quest ((char *) a_eig.pr_ausz,0,2);
    out_quest ((char *) a_eig.pr_man,0,2);
    out_quest ((char *) a_eig.pr_ueb,0,2);
    out_quest ((char *) a_eig.rez,0,9);
    out_quest ((char *) &a_eig.sg1,1,0);
    out_quest ((char *) &a_eig.sg2,1,0);
    out_quest ((char *) &a_eig.tara,3,0);
    out_quest ((char *) a_eig.theke_eti,0,2);
    out_quest ((char *) a_eig.verk_art,0,2);
    out_quest ((char *) &a_eig.verk_beg,2,0);
       cursor_a_eig = prepare_sql ("select a_eig.a,  "
"a_eig.a_krz,  a_eig.anz_theke_eti,  a_eig.bem_offs,  a_eig.delstatus,  "
"a_eig.fil,  a_eig.gew_bto,  a_eig.inh,  a_eig.mat,  a_eig.mdn,  "
"a_eig.me_einh_ek,  a_eig.mwst_ueb,  a_eig.pr_ausz,  a_eig.pr_man,  "
"a_eig.pr_ueb,  a_eig.rez,  a_eig.sg1,  a_eig.sg2,  a_eig.tara,  "
"a_eig.theke_eti,  a_eig.verk_art,  a_eig.verk_beg from a_eig "
                                    "where a = ?");
}

void HNDW_CLASS::prepare_eig_div (void)
{
    ins_quest ((char *) &a_eig_div.a, 3, 0);
    out_quest ((char *) &a_eig_div.a,3,0);
    out_quest ((char *) &a_eig_div.a_krz,2,0);
    out_quest ((char *) &a_eig_div.anz_reg_eti,1,0);
    out_quest ((char *) &a_eig_div.anz_theke_eti,1,0);
    out_quest ((char *) &a_eig_div.anz_waren_eti,1,0);
    out_quest ((char *) &a_eig_div.delstatus,1,0);
    out_quest ((char *) &a_eig_div.fil,1,0);
    out_quest ((char *) &a_eig_div.gew_bto,3,0);
    out_quest ((char *) &a_eig_div.inh,3,0);
    out_quest ((char *) &a_eig_div.mdn,1,0);
    out_quest ((char *) &a_eig_div.me_einh_ek,1,0);
    out_quest ((char *) a_eig_div.mwst_ueb,0,2);
    out_quest ((char *) a_eig_div.pr_ausz,0,2);
    out_quest ((char *) a_eig_div.pr_man,0,2);
    out_quest ((char *) a_eig_div.pr_ueb,0,2);
    out_quest ((char *) a_eig_div.reg_eti,0,2);
    out_quest ((char *) a_eig_div.rez,0,9);
    out_quest ((char *) &a_eig_div.sg1,1,0);
    out_quest ((char *) &a_eig_div.sg2,1,0);
    out_quest ((char *) &a_eig_div.tara,3,0);
    out_quest ((char *) a_eig_div.theke_eti,0,2);
    out_quest ((char *) a_eig_div.verk_art,0,2);
    out_quest ((char *) &a_eig_div.verk_beg,2,0);
    out_quest ((char *) a_eig_div.vpk_kz,0,2);
    out_quest ((char *) a_eig_div.waren_eti,0,2);
       cursor_a_eig_div  = prepare_sql ("select a_eig_div.a,  "
"a_eig_div.a_krz,  a_eig_div.anz_reg_eti,  a_eig_div.anz_theke_eti,  "
"a_eig_div.anz_waren_eti,  a_eig_div.delstatus,  a_eig_div.fil,  "
"a_eig_div.gew_bto,  a_eig_div.inh,  a_eig_div.mdn,  "
"a_eig_div.me_einh_ek,  a_eig_div.mwst_ueb,  a_eig_div.pr_ausz,  "
"a_eig_div.pr_man,  a_eig_div.pr_ueb,  a_eig_div.reg_eti,  "
"a_eig_div.rez,  a_eig_div.sg1,  a_eig_div.sg2,  a_eig_div.tara,  "
"a_eig_div.theke_eti,  a_eig_div.verk_art,  a_eig_div.verk_beg,  "
"a_eig_div.vpk_kz,  a_eig_div.waren_eti from a_eig_div "
                                    "where a = ?");
}

void HNDW_CLASS::prepare_pfa (void)
{
    ins_quest ((char *) &a_pfa.a, 3, 0);
    out_quest ((char *) &a_pfa.a,3,0);
    out_quest ((char *) &a_pfa.a_krz,2,0);
    out_quest ((char *) &a_pfa.delstatus,1,0);
    out_quest ((char *) &a_pfa.fil,1,0);
    out_quest ((char *) &a_pfa.mdn,1,0);
    out_quest ((char *) a_pfa.mwst_ueb,0,2);
    out_quest ((char *) &a_pfa.verk_beg,2,0);
    out_quest ((char *) &a_pfa.sg1,1,0);
    out_quest ((char *) &a_pfa.sg2,1,0);
    out_quest ((char *) a_pfa.verk_art,0,2);
       cursor_a_pfa  = prepare_sql ("select a_pfa.a,  "
"a_pfa.a_krz,  a_pfa.delstatus,  a_pfa.fil,  a_pfa.mdn,  a_pfa.mwst_ueb,  "
"a_pfa.verk_beg,  a_pfa.sg1,  a_pfa.sg2,  a_pfa.verk_art from a_pfa "
                                    "where a = ?");
}

void HNDW_CLASS::prepare_leer (void)
{
    ins_quest ((char *) &a_leer.a, 3, 0);
    out_quest ((char *) &a_leer.a,3,0);
    out_quest ((char *) &a_leer.a_krz,2,0);
    out_quest ((char *) &a_leer.a_pfa,3,0);
    out_quest ((char *) &a_leer.delstatus,1,0);
    out_quest ((char *) &a_leer.fil,1,0);
    out_quest ((char *) &a_leer.mdn,1,0);
    out_quest ((char *) a_leer.mwst_ueb,0,2);
    out_quest ((char *) &a_leer.verk_beg,2,0);
    out_quest ((char *) &a_leer.sg1,1,0);
    out_quest ((char *) &a_leer.sg2,1,0);
    out_quest ((char *) a_leer.verk_art,0,2);
       cursor_a_pfa  = prepare_sql ("select a_leer.a,  "
"a_leer.a_krz,  a_leer.a_pfa,  a_leer.delstatus,  a_leer.fil,  a_leer.mdn,  "
"a_leer.mwst_ueb,  a_leer.verk_beg,  a_leer.sg1,  a_leer.sg2,  "
"a_leer.verk_art from a_leer "
                                    "where a = ?");
}


int HNDW_CLASS::lese_a_hndw (double a)
/**
Tabelle a_hndw lesen.
**/
{
         if (cursor_a_hndw == -1)
         {
                      prepare_hndw ();
         }
         a_hndw.a = a;
         open_sql (cursor_a_hndw);
         fetch_sql (cursor_a_hndw);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int HNDW_CLASS::lese_a_hndw (void)
/**
Naechsten Satz aus Tabelle a_hndw lesen.
**/
{
         fetch_sql (cursor_a_hndw);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int HNDW_CLASS::lese_a_eig (double a)
/**
Tabelle a_eig lesen.
**/
{
         if (cursor_a_eig == -1)
         {
                      prepare_eig ();
         }
         a_eig.a = a;
         open_sql (cursor_a_eig);
         fetch_sql (cursor_a_eig);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int HNDW_CLASS::lese_a_eig (void)
/**
Naechsten Satz aus Tabelle a_eig lesen.
**/
{
         fetch_sql (cursor_a_eig);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int HNDW_CLASS::lese_a_eig_div (double a)
/**
Tabelle a_eig_div lesen.
**/
{
         if (cursor_a_eig == -1)
         {
                      prepare_eig_div ();
         }
         a_eig_div.a = a;
         open_sql (cursor_a_eig_div);
         fetch_sql (cursor_a_eig_div);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int HNDW_CLASS::lese_a_eig_div (void)
/**
Naechsten Satz aus Tabelle a_eig_div lesen.
**/
{
         fetch_sql (cursor_a_eig_div);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int HNDW_CLASS::lese_a_pfa (double a)
/**
Tabelle a_pfa lesen.
**/
{
         if (cursor_a_pfa == -1)
         {
                      prepare_pfa ();
         }
         a_pfa.a = a;
         open_sql (cursor_a_pfa);
         fetch_sql (cursor_a_pfa);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int HNDW_CLASS::lese_a_pfa (void)
/**
Naechsten Satz aus Tabelle a_pfa lesen.
**/
{
         fetch_sql (cursor_a_pfa);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int HNDW_CLASS::lese_a_leer (double a)
/**
Tabelle a_leer lesen.
**/
{
         if (cursor_a_leer == -1)
         {
                      prepare_leer ();
         }
         a_leer.a = a;
         open_sql (cursor_a_leer);
         fetch_sql (cursor_a_leer);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int HNDW_CLASS::lese_a_leer (void)
/**
Naechsten Satz aus Tabelle a_leer lesen.
**/
{
         fetch_sql (cursor_a_leer);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}


void HNDW_CLASS::close_a_hndw (void)
/**
Cursor a_hndw schliessen.
**/
{
         if (cursor_a_hndw == -1) return;

         close_sql (cursor_a_hndw);
         cursor_a_hndw = -1;
}

void HNDW_CLASS::close_a_eig (void)
/**
Cursor a_eig schliessen.
**/
{
         if (cursor_a_eig == -1) return;

         close_sql (cursor_a_eig);
         cursor_a_eig = -1;
}

void HNDW_CLASS::close_a_eig_div (void)
/**
Cursor a_eig_div schliessen.
**/
{
         if (cursor_a_eig_div == -1) return;

         close_sql (cursor_a_eig_div);
         cursor_a_eig_div = -1;
}

void HNDW_CLASS::close_a_pfa (void)
/**
Cursor a_pfa schliessen.
**/
{
         if (cursor_a_pfa == -1) return;

         close_sql (cursor_a_pfa);
         cursor_a_pfa = -1;
}

void HNDW_CLASS::close_a_leer (void)
/**
Cursor a_leer schliessen.
**/
{
         if (cursor_a_leer == -1) return;

         close_sql (cursor_a_leer);
         cursor_a_leer = -1;
}
