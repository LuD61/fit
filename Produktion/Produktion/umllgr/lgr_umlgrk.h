#ifndef _LGR_UMLGRK_DEF
#define _LGR_UMLGRK_DEF

#include "dbclass.h"

struct LGR_UMLGRK {
   short     delstatus;
   long      dat;
   short     lgr_na;
   short     lgr_von;
   short     mdn;
   char      txt[61];
   long      umlgr_nr;
   short     stat;
   char      zeit[7];
   char      ueb_kz[2];
   double    diff_a;
};
extern struct LGR_UMLGRK lgr_umlgrk, lgr_umlgrk_null;

#line 7 "lgr_umlgrk.rh"

class LGR_UMLGRK_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               LGR_UMLGRK_CLASS () : DB_CLASS ()
               {
               }
               int dbreadfirst (void);
};
#endif
