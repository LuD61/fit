#include<stdio.h>
#include<stdarg.h>
#include<string.h>
#include<windows.h>
#include<winbase.h>
#define _CPP 1
#include "etubi.h"
#include "fit.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_curso.h"
#include "mo_menu.h"
#include "mo_expl.h"
#include "mo_draw.h"
#include "wmask.h"
#include "mo_meld.h"
#include "a_bas.h"
#include "a_hndw.h"
#include "ptab.h"
#include "dbfunc.h"
#include "fnb.h"
#include "mdn.h"
#include "fil.h"
#include "lief.h"
#include "we_kopf.h"
#include "we_pos.h"
#include "a_pr.h"
#include "akt_krz.h"
#include "aktion.h"
#include "mo_a_pr.h"
#include "mo_geb.h"
#include "mo_numme.h"
#include "sys_peri.h"
#include "pht_wg.h"
#include "sys_par.h"
#include "mo_progcfg.h"
#include "datum.h"
#include "dbclass.h"
#include "mo_wepr.h"
#include "lief_bzg.h"
#include "mo_nr.h"
#include "we_wiepro.h"
#include "mo_gdruck.h"
#include "nublock.h"
#include "mo_qm.h"
#include "qswekopf.h"
#include "qswepos.h"
#include "a_kalkhndw.h"
#include "a_kalk_mat.h"
#include "lgr_umlgrk.h"
#include "lgr_umlgrp.h"
#include "a_schl.h"
#include "pr_schwein.h"
#include "touchf.h" 
//include "mo_lsgen.h"
#include "mo_llgen.h"
#include "lgr.h"


static BOOL GraphikMode = FALSE;

#define AUFMAX 1000
#define GEDRUCKT 2
#define VERBUCHT 3

#define ENTER 0
#define SUCHEN 1
#define AUSWAHL 2

#define BWS "BWS"
#define RSDIR "RSDIR"

#define sql_in ins_quest
#define sql_out out_quest

#define BAR 6
#define SOFORT 1

#define WIEGEN 9
#define AUSZEICHNEN 1
#define STOP 2
#define MINUS -1
#define PLUS 1

#define VK_CLS 910

#define STANDARD 0
#define ENTNAHME 1

#define SCHWEIN 2
#define RIND 1
#define MFA_E_MAX 80
#define MFA_E_MIN 55
#define MFA_U_MIN 50
#define MFA_R_MIN 45
#define MFA_O_MIN 40
#define MFA_P_MIN 35
#define FETTST_MIN 0
#define FETTST_MAX 9

static char* LagerMinus2 = {"INVENTUR (nur Zubuchung)"};
static char* LagerMinus1 = {"nur Zubuchung"};
static char* LagerNull = {"Lagerloser Bestand"};
static int screenwidth  = 800;
static int screenheight = 580;
static BOOL GebTara = FALSE;

static int EnterTyp = ENTER;

static int break_lief_enter = 0;
static int SperreZugangswechsel = 0;

static int EnterEtiAnz = 1;
static double a_wahl = 0.0;
static int mit_trans = 1;
static int paz_direkt = 0;
static int auto_eti = 0;
static long auszeichner;
static int ls_charge_par = 0;
static int lsc2_par = 0;
static long StdWaage;
static double AktPrWieg[1000];
static double StornoWieg;
static int AktWiegPos = 0;
static BOOL CalcOn = FALSE;
static int aufart9999;
static short auf_art = 0;
static BOOL best_ausw_direct = FALSE;
static BOOL liste_direct = TRUE;
static BOOL a_ausw_direct = FALSE;
static BOOL lief_ausw_direct = FALSE;
static BOOL hbk_direct = FALSE;
static BOOL InBearbeitung = FALSE;
static int wieg_neu_direct = 0;
static BOOL wieg_direct = FALSE;
static BOOL hand_direct = FALSE;
static BOOL lief_bzg_zwang = FALSE;
static BOOL druck_zwang = FALSE;
static BOOL sort_a      = FALSE;
static BOOL hand_tara_sum = 0;
static BOOL fest_tara_sum = 0;
static BOOL fest_tara_anz = TRUE;
static BOOL EktoMeEinh = FALSE;
static long AutoSleep = 20;
static BOOL QsZwang = FALSE;
static BOOL autocursor = TRUE;
static BOOL DrKomplett = TRUE;
static BOOL flgNeu = FALSE; //gibt an , ob Neuer DS , oder bearbeiten eines bestehenden

static int WiegeTyp = STANDARD;

static char KomDatum [12];
static char ChargeAuswahl[14];

static HWND ePreisWindow;


static BOOL WithSpezEti =  FALSE;
static BOOL WithZusatz =  FALSE;
static char *eti_kiste   = NULL;
static char *eti_nve   = NULL;
static char *eti_ez      = NULL;
static char *eti_ev      = NULL;
static char *eti_artikel = NULL;

static char *eti_ki_drk  = NULL;
static char *eti_nve_drk  = NULL;
static char *eti_ez_drk  = NULL;
static char *eti_ev_drk  = NULL;
static char *eti_a_drk   = NULL;

static int eti_ki_typ  = -1;
static int eti_nve_typ  = -1;
static int eti_ez_typ  = -1;
static int eti_ev_typ  = -1;
static int eti_a_typ   = -1;



static double pr_ek_bto0;
static double pr_ek0;
static double pr_ek_bto0_euro;
static double pr_ek0_euro;

static double basispreis;
static char BasisPreis[13];
static char WiegTxt[13];
static char UmlgrNr[13];

static int testmode = 0;
extern BOOL ColBorder;
static BOOL colborder = FALSE;
static BOOL lsdefault = FALSE;
static BOOL multibest = TRUE;
static char fettst_mfa[8];
static char pictf_mfa[8];
static char Artikel[14];
static char Handelsklasse[7];
static char Fettstufe[7];
static char VkPreis[14];
static int posInsOK = 0;
static char drformat[6];

static char LONGNULL []   = {(UCHAR) 0x00, (UCHAR) 0x00, (UCHAR) 0x00, (UCHAR) 0x80};
static long LongNull = 0x80000000;

static int best_koresp;


static char *korestab[10] =   {we_kopf.koresp_best0,
                               we_kopf.koresp_best1,
                               we_kopf.koresp_best2,
						       we_kopf.koresp_best3,
						       we_kopf.koresp_best4,
						       we_kopf.koresp_best5,
						       we_kopf.koresp_best6,
						       we_kopf.koresp_best7,
						       we_kopf.koresp_best8,
						       we_kopf.koresp_best9};

static long *termtab[10] =      {&we_kopf.best0_term,
                                 &we_kopf.best1_term,
                                 &we_kopf.best2_term,
					  	         &we_kopf.best3_term,
						         &we_kopf.best4_term,
						         &we_kopf.best5_term,
						         &we_kopf.best6_term,
						         &we_kopf.best7_term,
						         &we_kopf.best8_term,
						         &we_kopf.best9_term};

static long *lieftab[10] =      {&we_kopf.lief0_term,
                                 &we_kopf.lief1_term,
                                 &we_kopf.lief2_term,
					  	         &we_kopf.lief3_term,
						         &we_kopf.lief4_term,
						         &we_kopf.lief5_term,
						         &we_kopf.lief6_term,
						         &we_kopf.lief7_term,
						         &we_kopf.lief8_term,
						         &we_kopf.lief9_term};


static int PtX = -1;
static int PtY = -1;


struct SART
{
    char s_art [30];
};

struct SART s_art;

struct SART s_artarr[] = {"nix",
					   "Gewerblich",
                       "Hausschlachtung",
                       "Sanit�tsschlachtung",
                       0};


//  Prototypen

int      PASCAL WinMain(HANDLE,HANDLE,LPSTR,int);
LONG FAR PASCAL WndProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL GebProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL FitLogoProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL UhrProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL EntNumProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL EntNuProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL TouchfProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL PreisProc(HWND,UINT, WPARAM,LPARAM);
LONG FAR PASCAL ListKopfProc(HWND,UINT, WPARAM,LPARAM);
LONG FAR PASCAL PtFussProc(HWND,UINT, WPARAM,LPARAM);
LONG FAR PASCAL MsgProc(HWND,UINT, WPARAM,LPARAM);
LONG FAR PASCAL PosLeerProc(HWND,UINT, WPARAM,LPARAM);
int             ShowWKopf (HWND, char * , int, char *,
                 int, form *, form *, form *);
void            fetch_std_geb (void);
void            update_kun_geb (void);
long            GetStdAuszeichner (void);
long            GetStdWaage (void);
int             AuswahlSysPeri (void);
int             AuswahlKopf (void);
void            IsDblClck (int);
void            GetAufArt9999 (void);
int             WriteLS (void);
void            DestroyKistBmp (void);
void            ReadKopftext (void);
int             ReadPr (int, double);
int             Reada_pr (void);

int             ShowPtBox (HWND, char *, int, char *, int, form *,
                           form *, form *);
int AuswahlSchlArt (void);
int AuswahlLager (int,int);
int AuswahlWiePro (void);

int IsWiegMessage (MSG *);
void InitAufKopf (void);

static     void SelectAktCtr (void);
void            InitFirstInstance(HANDLE);
BOOL            InitNewInstance(HANDLE, int);
int             ProcessMessages(void);
LONG            MenuJob(HWND, WPARAM, LPARAM);
void            LiefKopf (void);
void            EnterNumBox (HWND, char *, char *, int, char *);
void            EnterCalcBox (HWND, char *, char *, int, char *);
void            EnterPreisBox (HWND, int, int);
void            EnterListe (void);
void            ArtikelWiegen (HWND);
void            ArtikelHand (void);
void            WaageInit (void);
static int      DbAuswahl (short,void (*) (char *), void (*) (char *));
void            SetStorno (int (*) (void), int);
int             ShowFestTara (char *);
int             SetKomplett (void);
void            DisplayLiefKopf (void);
void            CloseLiefKopf (void);
void            LiefFocus (void);
int             print_messG (int, char *, ...);
void            disp_messG (char *, int);
int             abfragejnG (HWND, char *, char *);
int             AuswahlWieg (void);
void            fnbexec (int, char *[]);
BOOL            TestPosStatus (void);
BOOL            LsLeerGut (void);
static void     SetEti (double, double, double);
static void     SetEtiStk (double);
void            setautotara (BOOL);
void            EnterLeerPos (void);
void	 	    GetGebGewicht (char *);
int             deltaraw (void);
BOOL            TestQs (void);
BOOL            TestQsPosA (double);
BOOL            TestQsPos (void);
//void			Read_We_Wiepro (void);
BOOL            Lock_Lsp (void);
static int      la_zahl = 0;
static int      AktKopf = 0;
void			Kontrollbeleg (void);

//  Globale Variablen


HWND ForegroundWindow;

void GetForeground (void)
{
	ForegroundWindow = GetForegroundWindow ();
}

void SetForeground (void)
{
	SetForegroundWindow (ForegroundWindow);
}


HWND     hMainWindow;
HWND     hFitWindow = NULL;
HWND     BuDialog1;

HWND     BackWindow1;
HWND     BackWindow2;
HWND     BackWindow3;

HWND     LogoWindow;

HWND     FitWindow = NULL;
HWND     UhrWindow = NULL;
HWND     MessWindow = NULL;
HANDLE   hMainInst;
static   HWND eWindow = NULL;
static   HWND InsWindow = NULL;
static   HWND PtWindow = NULL;
static   HWND eKopfWindow = NULL;
static   HWND eFussWindow = NULL;
static   HWND PtFussWindow = NULL;
static   HWND GebWindow = NULL;
static   HWND GebWindButton = NULL;

LPSTR    lpszMainTitle = "TOUCH Umlagerung                         Version 12-2004 ";
LPSTR    lpszClassName = "fit";
HBITMAP  fitbmp;
HBITMAP   pfeill;
HBITMAP   pfeilo;
HBITMAP   pfeilu;
HBITMAP   pfeilr;
HBITMAP   hbmOld;
HDC       hdc;
HDC       hdcMemory;
BITMAP    bm;
BITMAP    fitbm;
BITMAP    hfitbm;
HDC       fithdc;
int       procwait = 0;
int       IsFhs = 0;
BOOL      LockAuf = 1;
BOOL      EinLock = 0;
double scrfx, scrfy;
static int sysxplus = 50;
int Sebmpw;
int Sebmph;

/*
int lmx = 10;
int lmx2 = 0;
int lmy = 50;
int lmxs = 20;
int lmys = 0;
*/


//WA_PREISE WaPreis;
WE_PREISE WePreis;
AB_PREISE AbPreise;

static BOOL KistPaint = FALSE;
static BMAP *Kist;
static BMAP *KistG;
HBITMAP KistBmp;
HBITMAP KistBmpG;
POINT kistpoint;
RECT KistRect;
BOOL KistCol = FALSE;

static BMAP *KistMask;
HBITMAP KistMaskBmp;

HBRUSH LtBlueBrush;
HBRUSH YellowBrush;



static char waadir [128] = {"C:\\waakt"};
static char ergebnis [128] = {"C:\\waakt\\ergebnis"};
static int WaaInit = 0;
static BOOL neuer_we;

static DWORD GebColor = RGB (0, 0, 255);

/* Globale Parameter fuer printlogo                   */

static long twait = 50;
static long twaitU = 500;
static int plus =  3;
static int xplus = 3;
static int ystretch = 0;
static int xstretch = 0;
static int logory = 0;
static int logorx = 1;
static int sign = 1;
static int signx = 1;

static short akt_mdn;
static short akt_fil;

static char fitpath [256];
static char rosipath [256];
static char progname [256];

struct MENUE scmenue;
struct MENUE leermenue;

static int ListeAktiv = 0;
static int PtListeAktiv = 0;
static int NumEnterAktiv = 0;
static int PreisAktiv = 0;
static int KopfEnterAktiv = 0;
static int WiegenAktiv = 0;
static int LiefEnterAktiv = 0;
static int KomplettAktiv = 0;
static int WorkModus = WIEGEN;
static HWND NumEnterWindow;
static HWND AufKopfWindow;
static HWND WiegWindow;


static int dsqlstatus;

WE_KOPF_CLASS WeKopfClass;
LIEF_CLASS LiefClass;
LGR_UMLGRK_CLASS Lgr_umlgrkClass;
LGR_UMLGRP_CLASS Lgr_umlgrpClass;

//class LS_CLASS ls_class;
//class LSPT_CLASS lspt_class;
// class KUN_CLASS kun_class;
class FIL_CLASS fil_class;
class MDN_CLASS mdn_class;
class ADR_CLASS adr_class;
class PTAB_CLASS ptab_class;
class MENUE_CLASS menue_class;
//class EINH_CLASS einh_class;
class HNDW_CLASS hndw_class;
class GEB_CLASS geb_class;
class SYS_PERI_CLASS sys_peri_class;
class LGR_CLASS lgr_class;
class PHT_WG_CLASS pht_wg_class;
class SYS_PAR_CLASS sys_par_class;
static DB_CLASS DbClass;
static PROG_CFG ProgCfg ("umllgr");
static BMAP bMap;
static AutoNrClass AutoNr;
static WE_WIEPRO_CLASS WeWiePro;
static LIEF_BZG_CLASS LiefBzg;
static NUBLOCK NuBlock;
static QSWEKOPF_CLASS QsWeKopf;
static QSWEPOS_CLASS QsWePos;
static QMKOPF *Qmkopf;
static TOUCHF Touchf;

static int tara_kz;
static int mhd_kz;

// HMENU hMenu;
static char *mentab[] = {scmenue.menue1,
                         scmenue.menue2,
                         scmenue.menue3,
                         scmenue.menue4,
                         scmenue.menue5,
                         scmenue.menue6,
                         scmenue.menue7,
                         scmenue.menue8,
                         scmenue.menue9,
                         scmenue.menue0};
static char *zeitab [] = {scmenue.zei1,
                          scmenue.zei2,
                          scmenue.zei3,
                          scmenue.zei4,
                          scmenue.zei5,
                          scmenue.zei6,
                          scmenue.zei7,
                          scmenue.zei8,
                          scmenue.zei9,
                          scmenue.zei0};

static char *mentab0[] = {menue.menue1,
                          menue.menue2,
                          menue.menue3,
                          menue.menue4,
                          menue.menue5,
                          menue.menue6,
                          menue.menue7,
                          menue.menue8,
                          menue.menue9,
                          menue.menue0};
static char *zeitab0[] = {menue.zei1,
                          menue.zei2,
                          menue.zei3,
                          menue.zei4,
                          menue.zei5,
                          menue.zei6,
                          menue.zei7,
                          menue.zei8,
                          menue.zei9,
                          menue.zei0};


static HMENU hMenu;

/* Masken fuer Logofenster                                 */

mfont fittextlogo = {"Arial", 1000, 0, 3, RGB (255, 0, 0),
                                               RGB (0, 255, 255),
                                               1,
                                               NULL};

mfont fittextlogo2 = {"Arial", 200, 0, 3, RGB (0, 0, 255),
                                      RGB (0, 255, 255),
                                      1,
                                      NULL};

mfont buttonfont = {"", 90, 0, 1,
                                       RGB (0, 255, 255),
                                       BLUECOL,
                                       1,
                                       NULL};

mfont uhrfont     = {"Arial", 80, 0, 1,
                                       BLACKCOL,
                                       LTGRAYCOL,
                                       1,
                                       NULL};

field _fittext[] = {"     f i t", 0, 0, 50, -1, 0, "", DISPLAYONLY, 0, 0, 0};

form fittext = {1, 0, 0, _fittext, 0, 0, 0, 0, &fittextlogo};

field _fittext2[] = {"in Frische und Fleisch", 0, 0, 250, -1, 0, "",
                       DISPLAYONLY, 0, 0, 0};

form fittext2 = {1, 0, 0, _fittext2, 0, 0, 0, 0, &fittextlogo2};

static int menu_ebene = 0;
static int ohne_preis = 1;
static int immer_preis = 0;
static int ls_leer_zwang = 0;

struct FITMENU
{
           char menutext[80];
           char menuprog[256];
           int progtyp;
           int start_mode;
};

struct FITMENU fitmenu [11];

static char auftrag_nr [22];
static char lief_nr [22];
static char ein_dat [22];
static char kun_nr [22];
static char lieferant [22];
static char kun_krz1 [17];
static char lief_krz1 [17];
static char lieferdatum  [22];
static char beldatum  [22];
static char komm_dat [22];
static char lieferzeit   [22];
static char ls_stat [22];
static char ls_stat_txt [27];
static char LAN [22];

static int menfunc1 (void);
static int menfunc2 (void);
static int menfunc3 (void);
static int menfunc3_1 (void);
static int menfunc4 (void);
static int menfunc5 (void);
static int menfunc6 (void);
static int menfunc7 (void);
static int menfunc8 (void);
static int menfunc9 (void);
static int menfunc0 (void);

static int func1 (void);
static int func2 (void);
static int func3 (void);
static int func4 (void);
static int func5 (void);
static int func6 (void);
static int func7 (void);
static int func8 (void);


static short Mandant = 1;
static short Filiale = 0;
static short Schlachtart = 1;
static short LagerAb = 0;
static short LagerZu = 0;
static short Wiegen = 1;
static short WeWiepro = 1;
static short Gewogen = 0;


static char *Mtxt = "Mandant";
static char *Ftxt = "Filiale";
static char *Stxt = "Schlachtart";
static char *LZutxt = "Buchen in Lager :";
static char *LAbtxt = "Holen aus Lager :";
static char *Btxt = "Umlagern";

static char Mandanttxt[20];
static char Filialetxt[20];
static char Schlachtarttxt[20];
static char LagerAbtxt[20];
static char LagerZutxt[20];
static char Bearbeitetxt[20];

static char LgrZu_krz[20];
static char LgrAb_krz[20];

ColButton Ende = {"Ende", -1, -1,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   RGB (0, 255, 255),
                   BLUECOL,
                   TRUE};


ColButton Auftrag  =   {"&Auftrag-Nr", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         -1};

ColButton Bearbeite  =   {Bearbeitetxt, -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               RGB (0, 255, 255),
                               BLUECOL,
                               TRUE};

ColButton Rinder  =   {"&Rinder", -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               RGB (0, 255, 255),
                               BLUECOL,
                               TRUE};

ColButton Liste    = {
                        "L&iste", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         -1};

ColButton Suchen   =  {"&Suchen", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                          -1};



ColButton Bestellung   =  {"&Bestellungen", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         -1};


ColButton QmKopf   =  {"&QM Kopf", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         -1};


ColButton Geraet = {
                        "&Ger�t", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton WiegArt = {
                        "&Wiegart", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton CMandant = {
                         Mandanttxt, -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton BLagerZu = {
                         LagerZutxt, -1, 5,
                         LgrZu_krz, -1, 25,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};
ColButton BLagerAb = {
                         LagerAbtxt, -1, 5,
                         LgrAb_krz, -1, 25,
                         NULL, 0, 0,
                         NULL, 0, 0,

                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};
ColButton KDatum    =  {"&Komm-Datum", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         -1};

ColButton Vorratsauftrag =  {
                        "Vor.Auftrag", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         -1};

ColButton BestInv =  {
                        "&Bestand, Inventur", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton Auswertungen =  {
                        "Auswer&tungen", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton Anwendungen =  {
                        "Anwen&dungen", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton Systemfunktionen =  {
                        "Systemf&unktionen", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton Benutzer =  {
                        "Ben&utzer", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};



ColButton Hilfe =  {
//                        "Hilfe &?", -1, -1,
                        "Startwiegung", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         -1};


ColButton Hauptmenue =  {
                        "Hauptmen&�", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton Back  =  {
                        "Zur�ck F5", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton BuOK = {WiegTxt, -1, -1,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   RGB (0, 255, 255),
                   BLUECOL,
                   -1};

ColButton BuOKS = {"OK", -1, -1,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   RGB (0, 255, 255),
                   BLUECOL,
                   10};

ColButton BuKomplett =
                  {"Komplett", -1, -1,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   RGB (0, 255, 255),
                   REDCOL,
                   -1};


ColButton PfeilL       =  {
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         pfeill, -1, -1,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         10};

ColButton PfeilO     =  {
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         pfeilo, -1, -1,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         10};

ColButton PfeilU     =  {
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         pfeilu, -1, -1,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         10};

ColButton PfeilR     =  {
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         pfeilr, -1, -1,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         10};


ColButton BuSchlNr =   {"&Schlacht-", -1, 25,
                         "Nummer", -1, 60,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         2};

ColButton BuOhr =   {"&Ohr-", -1, 25,
                         "Marke", -1, 60,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
//                         DKYELLOWCOL,
                         2};
ColButton BuAG =   {"&Artikel-", -1, 25,
                         "Gruppe", -1, 60,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         2};

ColButton BuCharge =   {"Chargen-", -1, 25,
                         "Korrektur", -1, 60,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         2};
ColButton BuHDKL =   {"&Handels-", -1, 25,
                         "Klasse", -1, 60,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         REDCOL,
                         2};

ColButton BuMFA =   {"MFA", -1, 25,
                         " ", -1, 60,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         REDCOL,
                         2};
ColButton BuFettSt =   {"Fettst.", -1, 25,
                         " ", -1, 60,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         REDCOL,
                         2};
ColButton BuBasisPreis =   {"&Basis", -1, 25,
                         "Preis", -1, 60,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         2};

ColButton BuVkPreis =   {"&VK-Preis", -1, 25,
                         "Preis", -1, 60,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         2};

ColButton BuGewicht =   {"&Gewicht", -1, 25,
                         WiegTxt, -1, 60,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         2};
ColButton BuLadPreis =   {"&Netto-", -1, 25,
                          "Preis", -1, 60,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 0, 0),
                         YELLOWCOL,
                         2};

ColButton BuPreisOK =   {"&Zur�ck", -1, -1,
                          NULL, 0, 0,
                          NULL, 0, 0,
                          NULL, 0, 0,
                          NULL, 0, 0,
                          RGB (255, 255, 255),
                          REDCOL,
                          2};

static int pnbss0 = 80;
int pny    = 3;
int pnx    = 1;
static int pcbss0 = 100;
static int pcbss  = 64;
static int pcbsz  = 64;
static int pcbsz0 = 100;
static int doa ();
static int docharge ();
static int dohdkl ();
static int dohdklchoise ();
static int dovk ();
static int dogewicht ();
static int dolad ();
static int doprok ();


mfont preisfont   = {"", 120, 0, 1,
                     RGB (255, 0, 0),
                     BLUECOL,
                     1,
                     NULL};


static field _PrWahl[] = {
(char *) &BuCharge,      pcbss0, pcbsz0, pny, pnx,        0, "",
                                                           COLBUTTON, 0,
                                                           docharge, 0,
(char *) &BuPreisOK,      pcbss0, pcbsz0, pny, pnx + 1 * pcbss0 , 0, "",
                                                           COLBUTTON, 0,
                                                           doprok, 0
};


static form PrWahl = {2, 0, 0, _PrWahl, 0, 0, 0, 0, &preisfont};

static field _PrWahl_s[] = {
(char *) &BuAG,      pcbss0, pcbsz0, pny, pnx,        0, "",
                                                           COLBUTTON, 0,
                                                           doa, 0,
(char *) &BuSchlNr,     pcbss0, pcbsz0, pny, pnx + 1 * pcbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           doa, 0,
(char *) &BuMFA,     pcbss0, pcbsz0, pny, pnx + 2 * pcbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           doa, 0,
(char *) &BuVkPreis,     pcbss0, pcbsz0, pny, pnx + 3 * pcbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           dovk, 0,
(char *) &BuGewicht,     pcbss0, pcbsz0, pny, pnx + 4 * pcbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           dogewicht, 0,
(char *) &BuPreisOK,      pcbss0, pcbsz0, pny, pnx + 5 * pcbss0 , 0, "",
                                                           COLBUTTON, 0,
                                                           doprok, 0};

static form PrWahl_s = {6, 0, 0, _PrWahl_s, 0, 0, 0, 0, &preisfont};


static field _PrWahl_r[] = {
(char *) &BuAG,      pcbss0, pcbsz0, pny, pnx,        0, "",
                                                           COLBUTTON, 0,
                                                           doa, 0,
(char *) &BuHDKL,     pcbss0, pcbsz0, pny, pnx + 1 * pcbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           dohdklchoise, 0,
(char *) &BuFettSt,     pcbss0, pcbsz0, pny, pnx + 2 * pcbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           doa, 0,
(char *) &BuOhr,     pcbss0, pcbsz0, pny, pnx + 3 * pcbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           doa,0,
(char *) &BuSchlNr,     pcbss0, pcbsz0, pny, pnx + 4 * pcbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           doa, 0,
(char *) &BuVkPreis,     pcbss0, pcbsz0, pny, pnx + 5 * pcbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           dovk, 0,
(char *) &BuGewicht,     pcbss0, pcbsz0, pny, pnx + 6 * pcbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           dogewicht, 0,
(char *) &BuPreisOK,      pcbss0, pcbsz0, pny, pnx + 7 * pcbss0 , 0, "",
                                                           COLBUTTON, 0,
                                                           doprok, 0};

static form PrWahl_r = {8, 0, 0, _PrWahl_r, 0, 0, 0, 0, &preisfont};




static int bsb  = 85;

static int bss0 = 73;
static int bss  = 77;
static int bsz  = 40;
static int bsz0 = 36;
static int AktButton = 0;


field _MainButton0[] = {
(char *) &Bearbeite,         128, bsz0, 115 + 0, -1, 0, "", COLBUTTON, 0,
                                                     menfunc3, 0,
(char *) &Liste,            128, bsz0, 115 + 1 * bsz, -1, 0, "", COLBUTTON, 0,
                                                     menfunc2, 0,
(char *) &Geraet,           128, bsz0, 115 + 5 * bsz, -1, 0, "", COLBUTTON, 0,
                                                     menfunc6, 0,
(char *) &BLagerZu,            128, bsz0, 115 + 6 * bsz, -1, 0, "", COLBUTTON, 0,
                                                     menfunc8, 0,
(char *) &BLagerAb,         128, bsz0, 115 + 7 * bsz, -1, 0, "", COLBUTTON, 0,
                                                     menfunc9, 0,
(char *) &CMandant,         128, bsz0, 115 + 8 * bsz, -1, 0, "", COLBUTTON, 0,
                                                     menfunc4, 0,
(char *) &Ende,             128, bsz0, 130 + 9 * bsz,  -1, 0, "", COLBUTTON, 0,
                                                          menfunc0, 0};
form MainButton = {7, 0, 0, _MainButton0, 0, 0, 0, 0, &buttonfont};


field _MainButton2[] = {
(char *) &Hilfe,             bss0, bsz0, 5, 4+7*bss, 0, "", COLBUTTON, 0,
                                                              func7, 0,
(char *) &Back,              bss0, bsz0, 5, 4+6*bss, 0, "", COLBUTTON, 0,
 *                                                              func5, 0,
(char *) &BuOK,              bss0, bsz0, 5, 4+5*bss, 0, "", COLBUTTON, 0,
 *                                                              func6, 0,
(char *) &BuKomplett,        bss0, bsz0, 5, 4+4*bss, 0, "", COLBUTTON, 0,
 *                                                              func8, 0,
(char *) &PfeilR,            bss0, bsz0, 5, 4+3*bss, 0, "", COLBUTTON, 0,
                                                          func4, 0,
(char *) &PfeilU,            bss0, bsz0, 5, 4+2*bss, 0, "", COLBUTTON, 0,
                                                          func3, 0,
(char *) &PfeilO,            bss0, bsz0, 5, 4+1*bss, 0, "", COLBUTTON, 0,
                                                          func2, 0,
(char *) &PfeilL,            bss0,  bsz0,5, 4,     0, "", COLBUTTON, 0,
                                                          func1, 0};

form MainButton2 = {8, 0, 0, _MainButton2, 0, 0, 0, 0, &buttonfont};

static char datum [12];
static char zeit [12];
static char datumzeit [24];

/*
static field _uhrform [] = {
datum,            0, 0,  5, -1, 0, "", DISPLAYONLY, 0, 0, 0,
zeit,             0, 0, 20, -1, 0, "", DISPLAYONLY, 0, 0, 0};
*/

static field _uhrform [] = {
datumzeit,        0, 0,  -1, -1, 0, "", DISPLAYONLY, 0, 0, 0};

static form uhrform = {1, 0, 0, _uhrform, 0, 0, 0, 0, &uhrfont};


struct AUFP_S
{
   char ag[13];
   char ag_bz[24];
   char umlgr_nr[13];
   char a_krz_bz[13];
   char anzahl[13];
   char hdkl[7];
   char mfa[13];
   char grund_pr[13];
   char basis_pr[13];
   char schlacht_nr[13];
   char abrech_kz[5];
   char stat[2];
   char leb_pr[13];
   char gew_leb[13];
   char bnp[2];

	    char pnr [5];
        char s [2];
        char a [14];
        char a_bz1 [25];
        char a_bz2 [25];
        char a_me_einh [5];
        char auf_me [13];
        char lief_me [13];
        char auf_me_vgl [13];
        char tara [13];
        char auf_me_bz [10];
        char lief_me_bz [10];
        char auf_pr_vk [13];
        char auf_lad_pr [13];
        char basis_me_bz [7];
        char erf_kz [2];
        char rest [13];
        char ls_charge [21];
        char lsp_txt [10];
        char me_einh_kun [5];
        char me_einh [5];
        char hbk_date [11];
        char ls_vk_euro [13];
        char ls_vk_fremd [13];
        char ls_lad_euro [13];
        char ls_lad_fremd [13];
        char rab_satz [10];
		char hnd_gew [2];
		char erf_gew_kz [2];
		char anz_einh [13];
		char inh [13];
		char temp [10];
		char ph_wert [10];
        char pr_fil_ek [10];
        char pr_fil_ek_eu [10];
		int  me_kz;
		double min_best;
};

struct AUFP_S aufps, aufp_null;
struct AUFP_S aufparr [AUFMAX];
int aufpanz = 0;
int aufpidx = 0;
int aufpanz_upd = 0;
static char auf_me [12];


struct WIEP
{
    char a [15];
    char a_bz1 [26];
    char brutto [12];
    char dat [15];
    char zeit [10];
};

struct WIEP wiep, wieparr[50];

static int wiepidx;
static int wiepanz = 0;


mfont ListFont = {"Courier New", 200, 150, 1, RGB (0, 0, 0),
                                      RGB (255, 255, 255),
                                      0,
                                      NULL};
mfont InsFontT = {"Courier New", 110, 0, 1, BLACKCOL,
                                            LTGRAYCOL,
                                            1,
                                            NULL};

mfont InsFont = {"Courier New", 150, 0, 1, BLACKCOL,
                                           LTGRAYCOL,
                                           1,
                                           NULL};

mfont lKopfFont = {"Courier New", 150, 0, 1, BLACKCOL,
                                             LTGRAYCOL,
                                             1,
                                             NULL};

mfont lFussFont = {"", 90, 0, 1,       RGB (0, 255, 255),
                                       BLUECOL,
                                       1,
                                       NULL};

static field _insform [] =
{
        aufps.a,            12, 0, 1, 1, 0, "%11.0f", READONLY, 0, 0, 0,
        aufps.a_bz1,        20, 0, 1, 14,0, "",       READONLY, 0, 0, 0,
        aufps.ls_charge,   21, 0, 1, 36, 0, "", READONLY, 0, 0, 0,
};

static form insform = {3, 0, 0, _insform, 0, 0, 0, 0, &InsFont};

static field _insub [] = {
"Artikel-Nr",            0, 0, 0,  1, 0, "", DISPLAYONLY, 0, 0, 0,
"Bezeichnung",           0, 0, 0, 14, 0, "", DISPLAYONLY, 0, 0, 0,
"Chargennr.",              0, 0, 0, 36, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form insub = { 3, 0, 0,_insub, 0, 0, 0, 0, &InsFontT};

/*
static form insform = {1, 0, 0, _insform, 0, 0, 0, 0, &InsFont};

static field _insub [] = {
" ",         0, 0, 0,  1, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form insub = { 1, 0, 0,_insub, 0, 0, 0, 0, &InsFontT};

static field _insform_s [] =
{
        aufps.ag_bz,       19, 0, 1, 1, 0, ""      , READONLY, 0, 0, 0,
        aufps.hdkl,         6, 0, 1, 20,0, "",       READONLY, 0, 0, 0,
        aufps.mfa,          7, 0, 1, 26, 0, "%3.1f", READONLY, 0, 0, 0,
        aufps.basis_pr,    10, 0, 1, 33, 0, "%6.2f", READONLY, 0, 0, 0,
        aufps.grund_pr,    10, 0, 1, 43, 0, "%6.2f", READONLY , 0, 0, 0,
        aufps.schlacht_nr, 25, 0, 1, 55, 0,      "", READONLY , 0, 0, 0,
};

static form insform_s = {6, 0, 0, _insform_s, 0, 0, 0, 0, &InsFont};

static field _insub_s [] = {
"Artikel",         0, 0, 0,  1, 0, "", DISPLAYONLY, 0, 0, 0,
"HDKL",                  0, 0, 0, 20, 0, "", DISPLAYONLY, 0, 0, 0,
"MFA",              0, 0, 0, 26, 0, "", DISPLAYONLY, 0, 0, 0,
"Grund-Pr.",             0, 0, 0, 33, 0, "", DISPLAYONLY, 0, 0, 0,
"VK-Preis",              0, 0, 0, 43, 0, "", DISPLAYONLY, 0, 0, 0,
"SchlachtNr",            0, 0, 0, 55, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form insub_s = { 6, 0, 0,_insub_s, 0, 0, 0, 0, &InsFontT};

static field _insform_r [] =
{
        aufps.ag_bz,       19, 0, 1, 1, 0, ""      , READONLY, 0, 0, 0,
        aufps.hdkl,         4, 0, 1, 21, 0, ""     , READONLY, 0, 0, 0,
        aufps.mfa,          4, 0, 1, 27, 0, "%hd"  , READONLY, 0, 0, 0,
        aufps.grund_pr,    10, 0, 1, 33, 0, "%8.2f", READONLY , 0, 0, 0,
        aufps.schlacht_nr,  8, 0, 1, 45, 0, "%hd"  , READONLY , 0, 0, 0,
        aufps.ls_charge, 25, 0, 1, 55, 0,      "", READONLY , 0, 0, 0,
};

static form insform_r = {6, 0, 0, _insform_r, 0, 0, 0, 0, &InsFont};

static field _insub_r [] = {
"Artikel",         0, 0, 0,  1, 0, "", DISPLAYONLY, 0, 0, 0,
"HDKL.",                 0, 0, 0, 21, 0, "", DISPLAYONLY, 0, 0, 0,
"F.st.",                 0, 0, 0, 27, 0, "", DISPLAYONLY, 0, 0, 0,
"VK-Preis",              0, 0, 0, 33, 0, "", DISPLAYONLY, 0, 0, 0,
"SchlachtNr",            0, 0, 0, 45, 0, "", DISPLAYONLY, 0, 0, 0,
"Ohrmarke",              0, 0, 0, 55, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form insub_r = { 6, 0, 0,_insub_r, 0, 0, 0, 0, &InsFontT};



*/


int GetAktButton (void)
/**
Aktiven Button holen.
**/
{
        HWND hWnd;
        int i;

        hWnd = GetFocus ();

        for (i = 0; i < MainButton.fieldanz; i ++)
        {
                    if (hWnd == MainButton.mask[i].feldid)
                    {
                               return (i);
                    }
        }

        for (i = 0; i < MainButton2.fieldanz; i ++)
        {
                    if (hWnd == MainButton2.mask[i].feldid)
                    {
                            return (MainButton.fieldanz + i);
                    }
        }
        return (AktButton);
}

field *GetAktField ()
/**
Field zu AktButton holen.
**/
{
        if (AktButton < MainButton.fieldanz)
        {
                      return (&MainButton.mask [AktButton]);
        }
        return (&MainButton2.mask[AktButton - MainButton.fieldanz]);
}


int setartikel (void)
{
    if (lese_a_bas (ratod (aufps.a)) != 0)
    {
              return -1;
    }
	strcpy (aufps.a_bz1,_a_bas.a_bz1);
	return 0;
}



BOOL TestOK (void)
/**
Test, ob OK-Button aktiv ist.
**/
{
	     if (BuOK.aktivate > -1)
		 {
			 return TRUE;
		 }
		 return FALSE;
}

int menfunc1 (void)
/**
Function fuer Button 1
**/
{
         return 0;
}

int menfunc2 (void)
/**
Function fuer Button 2
**/
{

         HWND aktfocus;
		 extern form LiefKopfE;
		 extern form LiefKopfED;


         if (EinLock) return 0;

         if (atol (lief_nr) > 0 && LiefEnterAktiv)
         {
                      EnterListe ();
                      break_lief_enter = 1;
         }


         aktfocus = GetFocus ();
         AktButton = GetAktButton ();

         return 0;
}

int menfunc3 (void)
/**
Function fuer Button 3
**/
{
         HWND aktfocus;


         auf_art = 0;
		 LiefKopf ();
         aktfocus = GetFocus ();
         SetColFocus (MainButton.mask[0].feldid);
         AktButton = GetAktButton ();
         return 0;
}

int menfunc3_1 (void)
/**
Function fuer Button 3
**/
{
         HWND aktfocus;

         auf_art = 0;
         LiefKopf ();
         aktfocus = GetFocus ();
         SetColFocus (MainButton.mask[0].feldid);
         AktButton = GetAktButton ();
         return 0;
}
int ItPos (form *frm, char *It)
/**
Position eines Maskenfeldes ermitteln.
**/
{
	      int i;

		  for (i = 0; i < frm->fieldanz; i ++)
		  {
			  if (frm->mask[i].feld == It) return i;
		  }
		  return 0;
}

int menfunc4 (void)
/**
Function fuer Button 4
**/
{
         HWND aktfocus;
		 char mdn [5];

		 sprintf (mdn, "%hd", Mandant);
         EnterNumBox (LogoWindow,  "  Mandant  ", mdn, 5, "%4d");
		 if (syskey == KEY5) return 0;
		 Mandant = atoi (mdn);
		 sprintf (Mandanttxt, "%s %hd", Mtxt, Mandant);
	     display_field (BackWindow2, &MainButton.mask[ItPos (&MainButton, (char *) &CMandant)]);

         aktfocus = GetFocus ();
         AktButton = GetAktButton ();


         return 0;
}


int menfunc5 (void)
/**
Function fuer Button 4
**/
{
	/***
         HWND aktfocus;
		 char fil [5];

		 sprintf (fil, "%hd", Filiale);
         EnterNumBox (LogoWindow,  "  Filiale  ", fil, 5, "%4d");
		 if (syskey == KEY5) return 0;
		 Filiale = atoi (fil);
		 sprintf (Filialetxt, "%s %hd", Ftxt, Filiale);
	     display_field (BackWindow2, &MainButton.mask[ItPos(&MainButton, (char *) &CFiliale)]);

         aktfocus = GetFocus ();
         AktButton = GetAktButton ();
	 *****/


         return 0;
}
int menfunc8 (void)
/**
Function fuer Button 8
**/
{
         HWND aktfocus;

         aktfocus = GetFocus ();
         LagerZu = AuswahlLager (1,LagerZu);
         if ( lgr_class.lese_lgr_bz (LagerZu) == 0) strcpy (LgrZu_krz,lgr.lgr_bz);
		 sprintf (LagerZutxt, "%s %hd", LZutxt, LagerZu);
		 posInsOK = 0;
	     display_field (BackWindow2, &MainButton.mask[ItPos(&MainButton, (char *) &LagerZu)]);
         AktButton = GetAktButton ();
         return 0;
}
int menfunc9 (void)
/**
Function fuer Button 9
**/
{
         HWND aktfocus;
         aktfocus = GetFocus ();
         LagerAb = AuswahlLager (-1,LagerAb);
         if ( lgr_class.lese_lgr_bz (LagerAb) == 0) strcpy (LgrAb_krz,lgr.lgr_bz);
		 if (LagerAb == -2) strcpy (LgrAb_krz,LagerMinus2);
		 if (LagerAb == -1) strcpy (LgrAb_krz,LagerMinus1);
		 if (LagerAb == 0) strcpy (LgrAb_krz,LagerNull);
		 sprintf (LagerAbtxt, "%s %hd", LAbtxt, LagerAb);
		 posInsOK = 0;
	     display_field (BackWindow2, &MainButton.mask[ItPos(&MainButton, (char *) &LagerAb)]);
         AktButton = GetAktButton ();
         return 0;
}

int menfunc6 (void)
/**
Function fuer Button 6
**/
{
         HWND aktfocus;

         aktfocus = GetFocus ();
         AuswahlSysPeri ();
         AktButton = GetAktButton ();
         return 0;
}

void MainBuInactivQm (void)
/**
Alle Button auf Inaktiv setzen.
**/
{
          ActivateColButton (BackWindow2, &MainButton, 0, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 1, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 2, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 3, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 4, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 5, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 6, -1, 1);
}
void MainBuActiv (void)
/**
Alle Button auf Inaktiv setzen.
**/
{
          ActivateColButton (BackWindow2, &MainButton, 0, 1, 1);
          ActivateColButton (BackWindow2, &MainButton, 1, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 2, 1, 1);
		  if (SperreZugangswechsel > 0)
		  {
				ActivateColButton (BackWindow2, &MainButton, 3, -1, 1);
		  }
		  else
		  {
				ActivateColButton (BackWindow2, &MainButton, 3, 1, 1);
		  }
          ActivateColButton (BackWindow2, &MainButton, 4, 1, 1);
          set_fkt (NULL, 6);
}

void MainBuActivQm (void)
/**
Alle Button auf Inaktiv setzen.
**/
{
          ActivateColButton (BackWindow2, &MainButton, 1, 1, 1);
          ActivateColButton (BackWindow2, &MainButton, 2, 1, 1);
          ActivateColButton (BackWindow2, &MainButton, 4, 1, 1);
          set_fkt (NULL, 6);
}

void MainBuInactivQmPos (void)
/**
Alle Button auf Inaktiv setzen.
**/
{
          MainBuInactivQm ();
          ActivateColButton (BackWindow3, &MainButton2, 0, -1, 1);
          ActivateColButton (BackWindow3, &MainButton2, 3, -1, 1);
}

void MainBuActivQmPos (void)
/**
Alle Button auf Inaktiv setzen.
**/
{
          MainBuActivQm ();
          ActivateColButton (BackWindow3, &MainButton2, 0, 1, 1);
          ActivateColButton (BackWindow3, &MainButton2, 3, 1, 1);
}


BOOL QmAktiv = FALSE;

static char *Ub[] = {"Qualit�tssicherung Hygiene",
                      NULL,
};


static char *Texte0[] = {"Laderaum hygienisch",
                         "Haaken und Beh�ltnisse i.O.",
			             "Holzpaletten/Kartonagen",
			             "Fleisch korrekt verpackt",
			             "Kleidung hygienisch",
			             "Kopfbedeckung",
				          NULL, NULL
};


char **Texte = Texte0;

static int Yes0[] = {0, 0, 0, 0, 0, 0, 0, 0};
static int No0[]  = {0, 0, 0, 0, 0, 0, 0, 0};

static int *Yes = Yes0;
static int *No  = No0;
static int qsanz = 6;

void FillQsHead ()
/**
Texte fuer Qualitaetssicherung fuellen.
**/
{
    int dsqlstatus;
    int cursor;
    char txt [25];
    int i;

    DbClass.sqlout ((short *) &qsanz, 1, 0);
    dsqlstatus = DbClass.sqlcomm ("select count (*) from qstxt where qstyp = 1 "
                                                               "and lief = \" \"");
    if (dsqlstatus != 0)
    {
        return;
    }

    if (qsanz == 0)
    {
        return;
    }

    Texte = new char* [qsanz + 2];
    Yes   = new int [qsanz + 2];
    No    = new int [qsanz + 2];

    for (i = 0; i < qsanz; i ++)
    {
        Yes[i] = No[i] = 0;
    }


    DbClass.sqlout ((char *) txt, 0, 25);
    cursor = DbClass.sqlcursor ("select txt,sort from qstxt where qstyp = 1 "
                                                            "and lief = \" \" "
                                                            "order by sort");
    if (cursor < 0)
    {
        return;
    }

    i = 0;
    while (DbClass.sqlfetch (cursor) == 0)
    {
        Texte [i] = new char [25];
        strcpy (Texte[i], txt);
        i ++;
    }

    Texte[i] = NULL;
    DbClass.sqlclose (cursor);
}


void FillQsHead (char *lief_nr)
/**
Texte fuer Qualitaetssicherung fuellen.
**/
{
    int cursor;
    char txt [25];
    char lief [17];
    int i;

    strcpy (lief, lief_nr);
    DbClass.sqlout ((short *) &qsanz, 1, 0);
    DbClass.sqlin ((char *) lief, 0, 17);
    cursor = DbClass.sqlcursor ("select count (*) from qstxt where qstyp = 1 "
                                                               "and lief = ?");
    qsanz = 0;
    DbClass.sqlfetch (cursor);
    while (qsanz == 0)
    {
        if (strcmp (lief, " ") > 0)
        {
            strcpy (lief, " ");
        }
        else
        {
            break;
        }
        DbClass.sqlopen (cursor);
        DbClass.sqlfetch (cursor);
    }
    DbClass.sqlclose (cursor);

    if (qsanz == 0)
    {
        return;
    }

    for (i = 0; Texte[i] != NULL; i ++)
    {
          delete Texte[i];
    }
    delete Texte;
    delete Yes;
    delete No;

    Texte = new char* [qsanz + 2];
    Yes   = new int [qsanz + 2];
    No    = new int [qsanz + 2];

    for (i = 0; i < qsanz; i ++)
    {
        Yes[i] = No[i] = 0;
    }


    DbClass.sqlout ((char *) txt, 0, 25);
    DbClass.sqlin ((char *) lief, 0, 17);
    cursor = DbClass.sqlcursor ("select txt,sort from qstxt where qstyp = 1 "
                                                            "and lief = ? "
                                                            "order by sort");
    if (cursor < 0)
    {
        return;
    }

    i = 0;
    while (DbClass.sqlfetch (cursor) == 0)
    {
        Texte [i] = new char [25];
        strcpy (Texte[i], txt);
        i ++;
    }

    Texte[i] = NULL;
    DbClass.sqlclose (cursor);
}



void InitQm (int *Yes, int *No, int anz)
/**
Eingaben pruefen.
**/
{
	      int i;

		  for (i = 0; i < anz; i ++)
		  {
			  Yes[i] = 0;
			  No [i] = 0;
		  }
}


BOOL TestQm (int *Yes, int *No, int anz)
/**
Eingaben pruefen.
**/
{
	      int i;

		  for (i = 0; i < anz; i ++)
		  {
			  if (Yes[i] == 0 &&
				  No [i] == 0)
			  {
				  return FALSE;
			  }
		  }
		  return TRUE;
}

BOOL TestQs (void)
{
         if (QsZwang == FALSE)
         {
             return TRUE;
         }

		 qswekopf.mdn = Mandant;
		 qswekopf.fil = Filiale;
		 strcpy (qswekopf.lief, we_kopf.lief);
		 strcpy (qswekopf.lief_rech_nr, we_kopf.lief_rech_nr);
		 qswekopf.dat = dasc_to_long (datum);
         qswekopf.lfd = (int) 1;
	     int dsqlstatus = QsWeKopf.dbreadfirst ();
         if (dsqlstatus != 0)
         {
             disp_messG ("Qualt�tsmerkmale wurden noch nicht erfasst", 2);
             return FALSE;
         }
         return TRUE;
}


void WriteQs (void)
/**
Eingaben aus Qualit�tssicherung speichern.
**/
{
	     int i;
		 char datum [12];

		 beginwork ();
		 sysdate (datum);
		 qswekopf.mdn = Mandant;
		 qswekopf.fil = Filiale;
		 strcpy (qswekopf.lief, we_kopf.lief);
		 strcpy (qswekopf.lief_rech_nr, we_kopf.lief_rech_nr);
		 qswekopf.dat = dasc_to_long (datum);
         qswekopf.lfd = (int) 1;
	     QsWeKopf.dbreadfirst ();
		 strcpy (qswekopf.pers_nam, sys_ben.pers_nam);
		 for (i = 0; i < qsanz; i ++)
		 {
 		          qswekopf.lfd = (int) i + 1;
				  strcpy (qswekopf.txt, Texte[i]);
				  qswekopf.wrt = Yes[i];
				  QsWeKopf.dbupdate ();
		 }
		 commitwork ();
}


int menfunc7 (void)
/**
Function fuer Button 7
**/
{
         HWND aktfocus;
		 extern form LiefKopfT;
		 extern form LiefKopfE;
		 extern form LiefKopfTD;
		 extern form LiefKopfED;

		 QmAktiv = TRUE;
         GetWindowText (LiefKopfE.mask [1].feldid,
                        LiefKopfE.mask [1].feld,
                        LiefKopfE.mask[1].length);
         FillQsHead (lieferant);
		 if (AufKopfWindow)
		 {
	            Qmkopf->SethMainWnd (AufKopfWindow);
                CloseControls (&LiefKopfT);
                CloseControls (&LiefKopfE);
                CloseControls (&LiefKopfTD);
                CloseControls (&LiefKopfED);
		 }
		 else
		 {
	            Qmkopf->SethMainWnd (LogoWindow);
		 }
         aktfocus = GetFocus ();
		 MainBuInactivQm ();
	     InitQm (Yes, No, qsanz);
		 while (TRUE)
		 {
//		        Qmkopf->SetParams (Ub, Texte, Yes, No, WS_CHILD);
		        Qmkopf->SetParamsEx (Ub, Texte, Yes, No,
			                  WS_CHILD, 0, TRUE);
		        Qmkopf->Enter ();
		        if (syskey == KEY12)
				{
					if (TestQm (Yes, No, qsanz) == FALSE)
					{
						disp_messG ("Bitte alle Punkte bearbeiten", 2);
						continue;
					}
			        WriteQs ();
				}
				break;
		 }
		 MainBuActivQm ();
		 if (AufKopfWindow)
		 {
                display_form (AufKopfWindow, &LiefKopfT, 0, 0);
                create_enter_form  (AufKopfWindow, &LiefKopfE,  0, 0);
                display_form (AufKopfWindow, &LiefKopfTD, 0, 0);
                create_enter_form  (AufKopfWindow, &LiefKopfED, 0, 0);
		 }
         AktButton = GetAktButton ();
		 QmAktiv = FALSE;
         return 0;
}


static char *UbPos[] = {"Qualit�tssicherung Fleisch",
                      NULL,
};

static char *TextePos0[] = {"Fettgehalt OK",
                           "Gewicht OK",
		           	       "Zuschnitt OK",
			               "Spezifikationen OK",
			               "Zustand der Ware OK",
			               "ordnungsgem�� gestempelt",
				           NULL, NULL
};

static int YesPos0[] = {0, 0, 0, 0, 0, 0, 0, 0};
static int NoPos0[]  = {0, 0, 0, 0, 0, 0, 0, 0};
static int qsanzpos = 6;

static char **TextePos = TextePos0;
static int *YesPos = YesPos0;
static int *NoPos  = NoPos0;

void FillQsPos ()
/**
Texte fuer Qualitaetssicherung fuellen.
**/
{
    int dsqlstatus;
    int cursor;
    char txt [25];
    int i;

    DbClass.sqlout ((short *) &qsanzpos, 1, 0);
    dsqlstatus = DbClass.sqlcomm ("select count (*) from qstxt where qstyp = 2 "
                                                               "and hwg = 0 "
                                                               "and wg = 0 "
                                                               "and ag = 0");
    if (dsqlstatus != 0)
    {
        return;
    }

    if (qsanzpos == 0)
    {
        return;
    }

    TextePos = new char* [qsanzpos + 2];
    YesPos   = new int [qsanzpos + 2];
    NoPos    = new int [qsanzpos + 2];

    for (i = 0; i < qsanzpos; i ++)
    {
        YesPos[i] = NoPos[i] = 0;
    }


    DbClass.sqlout ((char *) txt, 0, 25);
    cursor = DbClass.sqlcursor ("select txt,sort from qstxt where qstyp = 2 "
                                                               "and hwg = 0 "
                                                               "and wg = 0 "
                                                               "and ag = 0 "
                                                               "order by sort");
    if (cursor < 0)
    {
        return;
    }

    i = 0;
    while (DbClass.sqlfetch (cursor) == 0)
    {
        TextePos [i] = new char [25];
        strcpy (TextePos[i], txt);
        i ++;
    }

    TextePos[i] = NULL;
    DbClass.sqlclose (cursor);
}


void FillQsPos (double a)
/**
Texte fuer Qualitaetssicherung fuellen.
**/
{
    int cursor;
    char txt [25];
    int i;
    short hwg = 0;
    short wg = 0;
    short ag = 0l;

    DbClass.sqlout ((short *)  &hwg, 1, 0);
    DbClass.sqlout ((short *)  &wg,  1, 0);
    DbClass.sqlout ((long *)   &ag,  2, 0);
    DbClass.sqlin  ((double *) &a,   3, 0);
    DbClass.sqlcomm ("select hwg, wg, ag from a_bas where a = ?");

    DbClass.sqlout ((short *) &qsanzpos, 1, 0);
    DbClass.sqlin ((short *)  &hwg, 1, 0);
    DbClass.sqlin ((short *)  &wg,  1, 0);
    DbClass.sqlin ((long *)   &ag,  2, 0);
    cursor = DbClass.sqlcursor ("select count (*) from qstxt where qstyp = 2 "
                                                               "and hwg = ? "
                                                               "and wg = ? "
                                                               "and ag = ?");
   qsanzpos = 0;
   DbClass.sqlfetch (cursor);
   while (qsanzpos == 0)
    {
        if (ag > 0l)
        {
            ag = 0l;
        }
        else if (wg > 0)
        {
            wg = 0;
        }
        else if (hwg > 0)
        {
            hwg = 0;
        }
        else
        {
            break;
        }
        DbClass.sqlopen (cursor);
        DbClass.sqlfetch (cursor);
    }
    DbClass.sqlclose (cursor);

    if (qsanzpos == 0)
    {
        return;
    }

    if (TextePos != NULL &&
        TextePos != TextePos0)
    {
       for (i = 0; TextePos[i] != NULL; i ++)
       {
           delete TextePos[i];
       }
       delete TextePos;
       delete YesPos;
       delete NoPos;
    }

    TextePos = new char* [qsanzpos + 2];
    YesPos   = new int [qsanzpos + 2];
    NoPos    = new int [qsanzpos + 2];

    for (i = 0; i < qsanzpos; i ++)
    {
        YesPos[i] = NoPos[i] = 0;
    }


    DbClass.sqlout ((char *) txt, 0, 25);
    DbClass.sqlin ((short *)  &hwg, 1, 0);
    DbClass.sqlin ((short *)  &wg,  1, 0);
    DbClass.sqlin ((long *)   &ag,  2, 0);
    cursor = DbClass.sqlcursor ("select txt,sort from qstxt where qstyp = 2 "
                                                            "and hwg = ? "
                                                            "and wg = ? "
                                                            "and ag = ? "
                                                            "order by sort");
    if (cursor < 0)
    {
        return;
    }

    i = 0;
    while (DbClass.sqlfetch (cursor) == 0)
    {
        TextePos [i] = new char [25];
        strcpy (TextePos[i], txt);
        i ++;
    }

    TextePos[i] = NULL;
    DbClass.sqlclose (cursor);
}


BOOL TestQsPosA (double a)
{

         if (QsZwang == FALSE)
         {
             return TRUE;
         }
		 qswepos.mdn = Mandant;
		 qswepos.fil = Filiale;
		 strcpy (qswepos.lief, clipped (we_kopf.lief));
		 strcpy (qswepos.lief_rech_nr, clipped (we_kopf.lief_rech_nr));
         qswepos.a   = a;
         DbClass.sqlin ((short *) &qswepos.mdn, 1, 0);
         DbClass.sqlin ((short *) &qswepos.fil, 1, 0);
         DbClass.sqlin ((char *)  qswepos.lief, 0, 17);
         DbClass.sqlin ((char *)  qswepos.lief_rech_nr, 0, 17);
         DbClass.sqlin ((double *) &qswepos.a, 3, 0);
	     int dsqlstatus = DbClass.sqlcomm ("select a from qswepos "
                                            "where mdn = ? "
                                            "and fil = ? "
                                            "and lief = ? "
                                            "and lief_rech_nr = ? "
                                            "and a = ?");
         if (dsqlstatus != 0)
         {
             print_messG (2, "Qualit�tsmermale f�r Artikel %.0lf\n"
                             "wurden noch nicht erfasst", a);
             return FALSE;
         }
         return TRUE;
}

BOOL TestQsPos (void)
{
         if (QsZwang == FALSE)
         {
             return TRUE;
         }
         for (int i = 0; i < aufpanz; i ++)
         {
             if (TestQsPosA (ratod (aufparr[i].a)) == FALSE)
             {
                 return FALSE;
             }
         }
         return TRUE;
}


void WriteQsPos (void)
/**
Eingaben aus Qualit�tssicherung speichern.
**/
{
	     int i;
		 char datum [12];
		 extern int aufpidx;

         aufpidx = SendMessage  (Getlbox (), LB_GETCURSEL, 0l, 0l);
         memcpy (&aufps, &aufparr[aufpidx], sizeof (struct AUFP_S));

		 sysdate (datum);
		 qswepos.mdn = Mandant;
		 qswepos.fil = Filiale;
		 strcpy (qswepos.lief, we_kopf.lief);
		 strcpy (qswepos.lief_rech_nr, we_kopf.lief_rech_nr);
		 qswepos.dat = dasc_to_long (datum);
         qswepos.lfd = (int) 1;
         qswepos.a   = ratod (aufps.a);
         qswepos.int_pos   = atol (aufps.pnr);
         qswepos.temp      = ratod (aufps.temp);
         qswepos.ph_wert   = ratod (aufps.ph_wert);
	     QsWePos.dbreadfirst ();
		 strcpy (qswepos.pers_nam, sys_ben.pers_nam);

		 for (i = 0; i < qsanzpos; i ++)
		 {
 		          qswepos.lfd = (int) i + 1;
				  strcpy (qswepos.txt, TextePos[i]);
				  qswepos.wrt = YesPos[i];
				  QsWePos.dbupdate ();
		 }
}


int doQsPos (void)
/**
Function fuer Button 7
**/
{

		 if (eWindow == NULL) return 0;
         int aufpidx = SendMessage  (Getlbox (), LB_GETCURSEL, 0l, 0l);
         FillQsPos (ratod (aufparr[aufpidx].a));
         if (qsanzpos == 0)
         {
             print_messG (2, "Es sind keine Qualit�tstexte f�r Artikel %.0lf\n"
                             "angelegt", ratod (aufparr[aufpidx].a));
             return 0;
         }
		 QmAktiv = TRUE;
//         Qmkopf->SethMainWnd (eWindow);
         Qmkopf->SethMainWnd (GethListBox ());
		 MainBuInactivQmPos ();
         EnableWindow (eFussWindow, FALSE);
	     InitQm (YesPos, NoPos, qsanzpos);
		 while (TRUE)
		 {
		        Qmkopf->SetParamsEx (UbPos, TextePos, YesPos, NoPos,
			                  WS_POPUP, WS_EX_CLIENTEDGE, TRUE);
		        Qmkopf->Enter ();
		        if (syskey == KEY12)
				{
					if (TestQm (YesPos, NoPos, qsanzpos) == FALSE)
					{
						disp_messG ("Bitte alle Punkte bearbeiten", 2);
						continue;
					}
		           WriteQsPos ();
				}
				break;
		 }
         EnableWindow (eFussWindow, TRUE);
		 MainBuActivQmPos ();
		 QmAktiv = FALSE;
         return 0;
}

// Auswahl ueber Bestellungen

mfont cbestfont = {"Courier New", 120, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                0,
                                NULL};

struct CBE
{
char best_blg [9];
char best_term [12];
char lief_term [12];
};

static struct CBE cbest, *cbestarr = NULL;
static int cbestanz;

static int sortcbest ();
static int sortcbestt ();
static int sortclieft ();

static field _cbestform [] = {
	cbest.best_blg,     8, 0, 0, 1, 0, "%8d",    DISPLAYONLY, 0, 0, 0,
	cbest.best_term,   10, 0, 0,11, 0, "",       DISPLAYONLY, 0, 0, 0,
	cbest.lief_term,   10, 0, 0,23, 0, "",       DISPLAYONLY, 0, 0, 0,
};

static form cbestform = {3, 0, 0, _cbestform, 0, 0, 0, 0, &cbestfont};


static field _bestub [] =
{
        " Best-Nr",        10, 1, 0, 0, 0, "", BUTTON, 0, sortcbest, 0,
        " Best.Term",      12, 1, 0,10, 0, "", BUTTON, 0, sortcbestt, 1,
        " Lief.Term",      12, 1, 0,22, 0, "", BUTTON, 0, sortclieft, 2,
};

static form bestub = {3, 0, 0, _bestub, 0, 0, 0, 0, &cbestfont};

static field _bestvl [] =
{
        "1",                1, 1, 0,10, 0, "", DISPLAYONLY, 0, 0, 0,
        "1",                1, 1, 0,22, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form bestvl = {2, 0, 0, _bestvl, 0, 0, 0, 0, &cbestfont};
static int cbestsort = 1;
static int cbtsort = 1;
static int cltsort = 1;


static int sortcbest0 (const void *elem1, const void *elem2)
{
	        struct CBE *el1;
	        struct CBE *el2;

			el1 = (struct CBE *) elem1;
			el2 = (struct CBE *) elem2;
	        return ((int) (atol (el1->best_blg) - atol (el2->best_blg)) * cbestsort);
}


static int sortcbest (void)
/**
Nach Bestellnummer sortieren.
**/
{
	        qsort (cbestarr, cbestanz, sizeof (struct CBE),
				   sortcbest0);
			cbestsort *= -1;
            ShowNewElist ((char *) cbestarr,
                             cbestanz,
                            (int) sizeof (struct CBE));
			return 0;
}


static int sortcbestt0 (const void *elem1, const void *elem2)
{
	        struct CBE *el1;
	        struct CBE *el2;

			el1 = (struct CBE *) elem1;
			el2 = (struct CBE *) elem2;
            long dat1 = dasc_to_long (el1->best_term);
            long dat2 = dasc_to_long (el2->best_term);
            return (dat1 - dat2) * cbtsort;
//			return ( (int) (strcmp (el1->best_term, el2->best_term)) * cbtsort);
}

static int sortcbestt (void)
/**
Nach Bestelltermin sortieren.
**/
{
	        qsort (cbestarr, cbestanz, sizeof (struct CBE),
				   sortcbestt0);
			cbtsort *= -1;
            ShowNewElist ((char *) cbestarr,
                             cbestanz,
                            (int) sizeof (struct CBE));
			return 0;
}
static int sortclieft0 (const void *elem1, const void *elem2)
{
	        struct CBE *el1;
	        struct CBE *el2;

			el1 = (struct CBE *) elem1;
			el2 = (struct CBE *) elem2;
            long dat1 = dasc_to_long (el1->lief_term);
            long dat2 = dasc_to_long (el2->lief_term);
            return (dat1 - dat2) * cltsort;
//			return ( (int) (strcmp (el1->lief_term, el2->lief_term)) * cltsort);
}


static int sortclieft (void)
/**
Nach Liefertermin sortieren.
**/
{
	        qsort (cbestarr, cbestanz, sizeof (struct CBE),
				   sortclieft0);
			cltsort *= -1;
            ShowNewElist ((char *) cbestarr,
                             cbestanz,
                            (int) sizeof (struct CBE));
			return 0;
}


// Auswahl ueber Lieferanten

mfont clieffont = {"Courier New", 120, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                0,
                                NULL};

struct CLI
{
char lief [17];
char name [37];
};

static struct CLI clief, *cliefarr = NULL;
static int cliefanz;

static int sortclief ();
static int sortcname ();

static field _cliefform [] = {
	clief.lief,        16, 0, 0, 1, 0, "",    DISPLAYONLY, 0, 0, 0,
	clief.name,        36, 0, 0,19, 0, "",    DISPLAYONLY, 0, 0, 0,
};

static form cliefform = {2, 0, 0, _cliefform, 0, 0, 0, 0, &clieffont};


static field _liefub [] =
{
        " Lieferant",      18, 1, 0, 0, 0, "", BUTTON, 0, sortclief, 0,
        " Name",           40, 1, 0,18, 0, "", BUTTON, 0, sortcname, 1,
};

static form liefub = {2, 0, 0, _liefub, 0, 0, 0, 0, &clieffont};

static field _liefvl [] =
{
        "1",                1, 1, 0,18, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form liefvl = {1, 0, 0, _liefvl, 0, 0, 0, 0, &clieffont};
static int cliefsort = 1;
static int cnamesort = 1;


static int sortclief0 (const void *elem1, const void *elem2)
{
	        struct CLI *el1;
	        struct CLI *el2;

			el1 = (struct CLI *) elem1;
			el2 = (struct CLI *) elem2;
			return ( (int) (strcmp (el1->lief, el2->lief)) * cliefsort);
}


static int sortclief (void)
/**
Nach Lieferantennummer sortieren.
**/
{
	        qsort (cliefarr, cliefanz, sizeof (struct CLI),
				   sortclief0);
			cliefsort *= -1;
            ShowNewElist ((char *) cliefarr,
                             cliefanz,
                            (int) sizeof (struct CLI));
			return 0;
}


static int sortcname0 (const void *elem1, const void *elem2)
{
	        struct CLI *el1;
	        struct CLI *el2;

			el1 = (struct CLI *) elem1;
			el2 = (struct CLI *) elem2;
			return ( (int) (strcmp (el1->name, el2->name)) * cnamesort);
}

static int sortcname (void)
/**
Nach Lieferantennamen sortieren.
**/
{
	        qsort (cliefarr, cliefanz, sizeof (struct CLI),
				   sortcname0);
			cnamesort *= -1;
            ShowNewElist ((char *) cliefarr,
                             cliefanz,
                            (int) sizeof (struct CLI));
			return 0;
}
int doliefchoise ()
{
	    static long liefanz = 0;
		char lief [17];
		char name [37];
		int cursor;
		int idx;
		int i;

		if (scrfx > (double) 1.0)
		{
                clieffont.FontHeight = (int) (double) ((double) 120 *  scrfx);
		}
		cliefsort = cnamesort = 1;
		if (cliefarr)
		{
			    GlobalFree (cliefarr);
		}
        DbClass.sqlout  ((long *) &liefanz, 2, 0);
        DbClass.sqlcomm ("select count (*) from lief where lief <> \"-1\"");
		if (liefanz == 0)
		{
			return 0;
		}

        cliefarr = (struct CLI *) GlobalAlloc (GMEM_FIXED, liefanz * sizeof (struct CLI));
		if (cliefarr == NULL)
		{
			disp_messG ("Fehler beim Zuweisen von Speicherplatz", 2);
			return 0;
		}

        DbClass.sqlout  ((char *)  lief, 0, 17);
        DbClass.sqlout  ((char *)  name, 0, 37);
		cursor = DbClass.sqlcursor ("select lief.lief, adr.adr_nam1 from lief,adr "
									"where lief.lief <> \"-1\" and adr.adr = lief.adr "
									"order by lief");
		i = 0;
		while (DbClass.sqlfetch (cursor) == 0)
		{
			strcpy  (cliefarr[i].lief, lief);
			strcpy  (cliefarr[i].name, name);
			i = i + 1;
		}
		DbClass.sqlclose (cursor);

		liefanz = cliefanz = i;
        SetHoLines (1);
        SetVeLines (1);
		PtY = 410;
		if (scrfx > (double) 1.0)
		{
			PtY = (int) (double) ((double) PtY * scrfx);
		}
        syskey = 0;

        EnableWindow (NuBlock.GeteNumWindow (), FALSE);
        idx = ShowWKopf (NuBlock.GeteNumWindow (), (char *) cliefarr, liefanz,
                        (char *) &clief, sizeof (struct CLI),
                         &cliefform, &liefvl, &liefub);
		CloseControls (&liefub);
        EnableWindows (AufKopfWindow, FALSE);
        EnableWindow (NuBlock.GeteNumWindow (), TRUE);
        SetHoLines (1);
        if (syskey == KEY5 || syskey == KEYESC)
        {
			    GlobalFree (cbestarr);
		        cbestarr = NULL;
                return FALSE;
        }

        strcpy (NuBlock.Geteditbuff (), cliefarr[idx].lief);
		GlobalFree (cliefarr);
		cliefarr = NULL;
		Sleep (50);
		display_form (NuBlock.GeteNumWindow (), NuBlock.GetEditForm (), 0, 0);
		NuBlock.SetEditFocus ();
		if (lief_ausw_direct)
		{
			   NuBlock.NumEnterBreak ();
		}
		return 0;
}


// Auswahl ueber Artikel (a)

/*****************
mfont cafont = {"Courier New", 220, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                0,
                                NULL};

struct CA_BAS
{
char a [14];
char a_bz1 [25];
};

static struct CA_BAS ca_bas, *caarr = NULL;
static int caanz;


static field _caform [] = {
	ca_bas.a,         13, 0, 0, 1, 0, "%13.0f", DISPLAYONLY, 0, 0, 0,
	ca_bas.a_bz1,     24, 0, 0,16, 0, "",       DISPLAYONLY, 0, 0, 0,
};

static form caform = {2, 0, 0, _caform, 0, 0, 0, 0, &cafont};


static field _aub [] =
{
        " Artikel",     15, 1, 0, 0, 0, "", BUTTON, 0, 0, 0,
        " Bezeichnung  ",  26, 1, 0,15, 0, "", BUTTON, 0, 0, 1,
};

static form aub = {2, 0, 0, _aub, 0, 0, 0, 0, &cafont};

static field _avl [] =
{
        "1",                1, 1, 0,15, 0, "", DISPLAYONLY, 0, 0, 0,
        "1",                1, 1, 0,40, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form avl = {2, 0, 0, _avl, 0, 0, 0, 0, &cafont};

int doachoise (void)
//Auswahl ueber Artikel
{
	    static long aanz = 0;
		double a;
		char a_bz1 [25];
		char a_bz2 [25];
		int cursor;
		int idx;
		int i;

		if (scrfx > (double) 1.0)
		{
                cafont.FontHeight = (int) (double) ((double) 220 *  scrfx);
		}
		if (caarr)
		{
			    GlobalFree (caarr);
		}
        DbClass.sqlout  ((long *) &aanz, 2, 0);
			DbClass.sqlcomm ("select count (*) from a_bas where bsd_kz = \"J\" ");
		if (aanz == 0)
		{
			disp_messG ("Kein Artikel gefunden !", 2);
		    NuBlock.NumEnterBreak ();
			return 0;
		}

        caarr = (struct CA_BAS *) GlobalAlloc (GMEM_FIXED, aanz * sizeof (struct CA_BAS));
		if (caarr == NULL)
		{
			disp_messG ("Fehler beim Zuweisen von Speicherplatz", 2);
		    NuBlock.NumEnterBreak ();
			return 0;
		}

        DbClass.sqlout  ((long *) &a, 3, 0);
        DbClass.sqlout  ((char *)  a_bz1, 0, 25);
        DbClass.sqlout  ((char *)  a_bz2, 0, 25);
	    cursor = DbClass.sqlcursor ("select a, a_bz1, a_bz2 from a_bas where bsd_kz = \"J\" "
			                        "order by a_bz1" );
		i = 0;
		while (DbClass.sqlfetch (cursor) == 0)
		{
			sprintf (caarr[i].a, "%d", a);
			strcpy  (caarr[i].a_bz1, a_bz1);
			i = i + 1;
		}
		DbClass.sqlclose (cursor);

		aanz = caanz = i;
        SetHoLines (1);
        SetVeLines (1);
		PtY = 410;
		if (scrfx > (double) 1.0)
		{
			PtY = (int) (double) ((double) PtY * scrfx);
		}
        syskey = 0;

        EnableWindow (NuBlock.GeteNumWindow (), FALSE);
        idx = ShowWKopf (NuBlock.GeteNumWindow (), (char *) caarr, aanz,
                        (char *) &ca_bas, sizeof (struct CA_BAS),
                         &caform, &avl, &aub);
		CloseControls (&aub);
        EnableWindow (NuBlock.GeteNumWindow (), TRUE);
        SetHoLines (1);
        if (syskey == KEY5 || syskey == KEYESC)
        {
			    GlobalFree (caarr);
		        caarr = NULL;
                return FALSE;
        }

        strcpy (NuBlock.Geteditbuff (), caarr[idx].a);
		GlobalFree (caarr);
		cagarr = NULL;
		Sleep (50);
		display_form (NuBlock.GeteNumWindow (), NuBlock.GetEditForm (), 0, 0);
		NuBlock.SetEditFocus ();
		if (a_ausw_direct)
		{
			   NuBlock.NumEnterBreak ();
		}
		return 0;
}

  **************/
/*

Auswahl fuer Festtara.

*/

struct PT
{
        char ptbez [33];
        char ptwer1 [9];
};

struct PT ptab, ptabarr [20];
static int ptabanz = 0;


static int ptidx;

mfont PtFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                               RGB (255, 255, 255),
                               1,
                               NULL};

static field _ftaraform [] =
{
        ptab.ptbez,     12, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        ptab.ptwer1,     9, 1, 0,13, 0, "%7.3f", DISPLAYONLY, 0, 0, 0
};

static form ftaraform = {2, 0, 0, _ftaraform, 0, 0, 0, 0, &PtFont};

static field _ftaraub [] =
{
        "Bezeichnung",     12, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        "Tara",             9, 1, 0,13, 0, "", DISPLAYONLY, 0, 0, 0
};

static form ftaraub = {2, 0, 0, _ftaraub, 0, 0, 0, 0, &PtFont};

static field _ftaravl [] =
{      "1",                  1,  0, 0, 12, 0, "", NORMAL, 0, 0, 0,
};

static form ftaravl = {1, 0, 0, _ftaravl, 0, 0, 0, 0, &PtFont};

int ShowFestTara (char *pttara)
/**
Ausahlfenster fuer Festtara anzeigen.
**/
{
        if (ptabanz == 0)
        {
               dsqlstatus = ptab_class.lese_ptab_all ("tara_fest");
               while (dsqlstatus == 0)
               {
                   strcpy (ptabarr[ptabanz].ptbez,  ptabn.ptbez);
                   strcpy (ptabarr[ptabanz].ptwer1, ptabn.ptwer1);
                   ptabanz ++;
                   if (ptabanz == 20) break;
                   dsqlstatus = ptab_class.lese_ptab_all ();
               }
        }
        EnableWindow (WiegWindow, FALSE);
        ptidx = ShowPtBox (WiegWindow, (char *) ptabarr, ptabanz,
                     (char *) &ptab,(int) sizeof (struct PT),
                     &ftaraform, &ftaravl, &ftaraub);
        EnableWindow (WiegWindow, TRUE);
        strcpy (pttara, ptabarr [ptidx].ptwer1);
        return ptidx;
}


struct PT ptabmh, ptabmharr [30];
static int gebanz = 0;
static int hdklanz = 0;

static field _fptabform [] =
{
        ptabmh.ptbez,     12, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        ptabmh.ptwer1,     9, 1, 0,13, 0, "%2d", DISPLAYONLY, 0, 0, 0
};

static form fptabform = {2, 0, 0, _fptabform, 0, 0, 0, 0, &PtFont};

static field _fptabub [] =
{
        "Bezeichnung",     12, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        "Wert",             9, 1, 0,13, 0, "", DISPLAYONLY, 0, 0, 0
};

static form fptabub = {2, 0, 0, _fptabub, 0, 0, 0, 0, &PtFont};

static field _fptabvl [] =
{      "1",                  1,  0, 0, 12, 0, "", NORMAL, 0, 0, 0,
};

static form fptabvl = {1, 0, 0, _fptabvl, 0, 0, 0, 0, &PtFont};




int ShowGebMeEinh (char *me_einh)
/**
Ausahlfenster fuer Festtara anzeigen.
**/
{
        if (gebanz == 0)
        {
               dsqlstatus = ptab_class.lese_ptab_all ("me_einh_fill");
               while (dsqlstatus == 0)
               {
                   strcpy (ptabmharr[gebanz].ptbez,  ptabn.ptbezk);
                   strcpy (ptabmharr[gebanz].ptwer1, ptabn.ptwert);
                   gebanz ++;
                   if (gebanz == 30) break;
                   dsqlstatus = ptab_class.lese_ptab_all ();
               }
        }
        syskey = 0;
        ptidx = ShowPtBox (GebWindow, (char *) ptabmharr, gebanz,
                     (char *) &ptabmh,(int) sizeof (struct PT),
                     &fptabform, &fptabvl, &fptabub);
        if (syskey != KEY5 && syskey != KEYESC)
        {
                     strcpy (me_einh, ptabmharr [ptidx].ptwer1);
        }
        return ptidx;
}

int dohdklchoise (void)
/**
Auswahl ueber Handelsklasse
**/
{
	/*****
        if (hdklanz == 0)
        {
               dsqlstatus = ptab_class.lese_ptab_all ("hdkl");
               while (dsqlstatus == 0)
               {
                   strcpy (ptabmharr[hdklanz].ptbez,  ptabn.ptbezk);
                   strcpy (ptabmharr[hdklanz].ptwer1, ptabn.ptwert);
                   hdklanz ++;
                   if (hdklanz == 30) break;
                   dsqlstatus = ptab_class.lese_ptab_all ();
               }
        }
        syskey = 0;
        EnableWindow (ePreisWindow, FALSE);
        ptidx = ShowPtBox (ePreisWindow, (char *) ptabmharr, hdklanz,
                     (char *) &ptabmh,(int) sizeof (struct PT),
                     &fptabform, &fptabvl, &fptabub);
        if (syskey != KEY5 && syskey != KEYESC)
        {
                     strcpy (lgr_umlgrp.hdkl, ptabmharr [ptidx].ptwer1);
                     strcpy (aufps.hdkl, ptabmharr [ptidx].ptwer1);
        }
        EnableWindow (ePreisWindow, TRUE);
		******/
        return ptidx;
}







// Auswahl ueber Artikel (a_bas)

mfont cartfont = {"Courier New", 120, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                0,
                                NULL};

struct CA
{
char a [14];
char a_bz1 [25];
char a_bz2 [25];
};

static struct CA ca, *caarr = NULL;
static int caanz;

static int sortca ();
static int sortcbz ();

static field _cartform [] = {
	ca.a,         13, 0, 0, 1, 0, "%13.0f", DISPLAYONLY, 0, 0, 0,
	ca.a_bz1,     24, 0, 0,16, 0, "",       DISPLAYONLY, 0, 0, 0,
	ca.a_bz2,     24, 0, 0,41, 0, "",       DISPLAYONLY, 0, 0, 0,
};

static form cartform = {3, 0, 0, _cartform, 0, 0, 0, 0, &cartfont};


static field _artub [] =
{
        "    Artikel",     15, 1, 0, 0, 0, "", BUTTON, 0, sortca, 0,
        " Bezeichnung 1",  25, 1, 0,15, 0, "", BUTTON, 0, sortcbz, 1,
        " Bezeichnung 2",  40, 1, 0,40, 0, "", BUTTON, 0, 0, 2,
};

static form artub = {3, 0, 0, _artub, 0, 0, 0, 0, &cartfont};

static field _artvl [] =
{
        "1",                1, 1, 0,15, 0, "", DISPLAYONLY, 0, 0, 0,
        "1",                1, 1, 0,40, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form artvl = {2, 0, 0, _artvl, 0, 0, 0, 0, &cartfont};
static int casort = 1;
static int cbsort = 1;

struct CCHARGE
{
char a [14];
char a_bz1 [25];
char chargennr [14];
char menge [14];
};

static struct CCHARGE ccharge, *cchargearr = NULL;

static field _cchgform [] = {
	ccharge.a,         13, 0, 0, 1, 0, "%13.0f", DISPLAYONLY, 0, 0, 0,
	ccharge.a_bz1,     24, 0, 0,16, 0, "",       DISPLAYONLY, 0, 0, 0,
	ccharge.chargennr, 14, 0, 0,41, 0, "",       DISPLAYONLY, 0, 0, 0,
	ccharge.menge,     14, 0, 0,54, 0, "%10.3f", DISPLAYONLY, 0, 0, 0,
};

static form cchgform = {4, 0, 0, _cchgform, 0, 0, 0, 0, &cartfont};

static field _chgub [] =
{
        "    Artikel",     15, 1, 0, 0, 0, "", BUTTON, 0, sortca, 0,
        "Bezeichnung 1",  25, 1, 0,15, 0, "", BUTTON, 0, sortcbz, 1,
        "Charge   ",        14, 1, 0,40, 0, "", BUTTON, 0, 0, 2,
        "Menge    ",        14, 1, 0,54, 0, "", BUTTON, 0, 0, 2,
};

static form chgub = {4, 0, 0, _chgub, 0, 0, 0, 0, &cartfont};

static field _chgvl [] =
{
        "1",                1, 1, 0,15, 0, "", DISPLAYONLY, 0, 0, 0,
        "1",                1, 1, 0,40, 0, "", DISPLAYONLY, 0, 0, 0,
        "1",                1, 1, 0,54, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form chgvl = {2, 0, 0, _chgvl, 0, 0, 0, 0, &cartfont};

static int sortca0 (const void *elem1, const void *elem2)
{
	        struct CA *el1;
	        struct CA *el2;

			el1 = (struct CA *) elem1;
			el2 = (struct CA *) elem2;
	        return ((int) (ratod(el1->a) - ratod (el2->a)) * casort);
}


static int sortca (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (caarr, caanz, sizeof (struct CA),
				   sortca0);
			casort *= -1;
            ShowNewElist ((char *) caarr,
                             caanz,
                            (int) sizeof (struct CA));
			return 0;
}


static int sortcbz0 (const void *elem1, const void *elem2)
{
	        struct CA *el1;
	        struct CA *el2;

			el1 = (struct CA *) elem1;
			el2 = (struct CA *) elem2;
			return ( (int) (strcmp (el1->a_bz1, el2->a_bz1)) * cbsort);
}

static int sortcbz (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (caarr, caanz, sizeof (struct CA),
				   sortcbz0);
			cbsort *= -1;
            ShowNewElist ((char *) caarr,
                             caanz,
                            (int) sizeof (struct CA));
			return 0;
}



int doartchoise (void)
/**
Auswahl ueber Artikel
**/
{
	    static long aanz = 0;
		double a;
		char a_bz1 [25];
		char a_bz2 [25];
		int cursor;
		int idx;
		int i;

		if (scrfx > (double) 1.0)
		{
                cartfont.FontHeight = (int) (double) ((double) 120 *  scrfx);
		}
		casort = cbsort = 1;
		if (caarr)
		{
			    GlobalFree (caarr);
		}
        DbClass.sqlout  ((long *) &aanz, 2, 0);
        DbClass.sqlcomm ("select count (*) from a_bas where bsd_kz = \"J\" ");
		if (aanz == 0)
		{
			disp_messG ("Fehler beim Zuwaeisen von Speicherplatz", 2);
  	        EnableWindows (eKopfWindow, FALSE);
            EnableWindows (eFussWindow, FALSE);
            EnableWindows (BackWindow2, FALSE);
            EnableWindows (BackWindow3, FALSE);
            EnableWindows (eWindow, FALSE);
			return 0;
		}

        caarr = (struct CA *) GlobalAlloc (GMEM_FIXED, aanz * sizeof (struct CA));
		if (caarr == NULL)
		{
			disp_messG ("Fehler beim Zuweisen von Speicherplatz", 2);
  	        EnableWindows (eKopfWindow, FALSE);
            EnableWindows (eFussWindow, FALSE);
            EnableWindows (BackWindow2, FALSE);
            EnableWindows (BackWindow3, FALSE);
            EnableWindows (eWindow, FALSE);
			return 0;
		}

        DbClass.sqlout  ((double *) &a, 3, 0);
        DbClass.sqlout  ((char *)  a_bz1, 0, 25);
        DbClass.sqlout  ((char *)  a_bz2, 0, 25);
		if (sort_a == FALSE)
		{
		         cursor = DbClass.sqlcursor ("select a, a_bz1, a_bz2 from a_bas "
			                        "where a > 0 and bsd_kz = \"J\" order by a_bz1 ");
		}
		else
		{
		         cursor = DbClass.sqlcursor ("select a, a_bz1, a_bz2 from a_bas "
			                        "where a > 0 and bsd_kz = \"J\" order by a ");
		}
		i = 0;
		while (DbClass.sqlfetch (cursor) == 0)
		{
			sprintf (caarr[i].a, "%13.0lf", a);
			strcpy  (caarr[i].a_bz1, a_bz1);
			strcpy  (caarr[i].a_bz2, a_bz2);
			i = i + 1;
		}
		DbClass.sqlclose (cursor);

		aanz = caanz = i;
        SetHoLines (1);
        SetVeLines (1);
		PtY = 410;
		if (scrfx > (double) 1.0)
		{
			PtY = (int) (double) ((double) PtY * scrfx);
		}
        syskey = 0;

        EnableWindow (NuBlock.GeteNumWindow (), FALSE);
        idx = ShowWKopf (NuBlock.GeteNumWindow (), (char *) caarr, aanz,
                        (char *) &ca, sizeof (struct CA),
                         &cartform, &artvl, &artub);
		CloseControls (&artub);
        EnableWindow (NuBlock.GeteNumWindow (), TRUE);
        SetHoLines (1);
	    EnableWindows (eKopfWindow, FALSE);
        EnableWindows (eFussWindow, FALSE);
        EnableWindows (BackWindow2, FALSE);
        EnableWindows (BackWindow3, FALSE);
        EnableWindows (eWindow, FALSE);
        if (syskey == KEY5 || syskey == KEYESC)
        {
			    GlobalFree (caarr);
		        caarr = NULL;
                return FALSE;
        }

        strcpy (NuBlock.Geteditbuff (), caarr[idx].a);
		GlobalFree (caarr);
		caarr = NULL;
		Sleep (50);
		display_form (NuBlock.GeteNumWindow (), NuBlock.GetEditForm (), 0, 0);
		NuBlock.SetEditFocus ();
		if (a_ausw_direct)
		{
			   NuBlock.NumEnterBreak ();
		}
		return 0;
}

double doartchoiselgr (void)
/**
Auswahl ueber Artikel im Abgangslager
**/
{
	    static long aanz = 0;
		double a;
		char a_bz1 [25];
		char a_bz2 [25];
		char bsd_lager [13];
		int cursor;
		int idx;
		int i;

		if (scrfx > (double) 1.0)
		{
                cartfont.FontHeight = (int) (double) ((double) 120 *  scrfx);
		}
		casort = cbsort = 1;
		if (caarr)
		{
			    GlobalFree (caarr);
		}
		sprintf(bsd_lager,"%ld",LagerAb);
        DbClass.sqlin  ((char *) bsd_lager, 0, 12);
        DbClass.sqlout  ((long *) &aanz, 2, 0);
        DbClass.sqlcomm ("select count (*) from a_bas where bsd_kz = \"J\" "
			               "and a in (select a from bsds where bsds.bsd_lgr_ort = ?)");
		if (aanz == 0)
		{
			disp_messG ("Fehler beim Zuwaeisen von Speicherplatz", 2);
  	        EnableWindows (eKopfWindow, FALSE);
            EnableWindows (eFussWindow, FALSE);
            EnableWindows (BackWindow2, FALSE);
            EnableWindows (BackWindow3, FALSE);
            EnableWindows (eWindow, FALSE);
			return 0;
		}

        caarr = (struct CA *) GlobalAlloc (GMEM_FIXED, aanz * sizeof (struct CA));
		if (caarr == NULL)
		{
			disp_messG ("Fehler beim Zuweisen von Speicherplatz", 2);
  	        EnableWindows (eKopfWindow, FALSE);
            EnableWindows (eFussWindow, FALSE);
            EnableWindows (BackWindow2, FALSE);
            EnableWindows (BackWindow3, FALSE);
            EnableWindows (eWindow, FALSE);
			return 0;
		}

        DbClass.sqlin  ((char *) bsd_lager, 0, 12);
        DbClass.sqlout  ((double *) &a, 3, 0);
        DbClass.sqlout  ((char *)  a_bz1, 0, 25);
        DbClass.sqlout  ((char *)  a_bz2, 0, 25);
		if (sort_a == FALSE)
		{
		         cursor = DbClass.sqlcursor ("select a, a_bz1, a_bz2 from a_bas "
			                        "where a > 0 and bsd_kz = \"J\" "
									 "and a in (select a from bsds where bsds.bsd_lgr_ort = ?) order by a_bz1 ");
		}
		else
		{
		         cursor = DbClass.sqlcursor ("select a, a_bz1, a_bz2 from a_bas "
			                        "where a > 0 and bsd_kz = \"J\" "
									 "and a in (select a from bsds where bsds.bsd_lgr_ort = ?) order by a ");
		}
		i = 0;
		while (DbClass.sqlfetch (cursor) == 0)
		{
			sprintf (caarr[i].a, "%13.0lf", a);
			strcpy  (caarr[i].a_bz1, a_bz1);
			strcpy  (caarr[i].a_bz2, a_bz2);
			i = i + 1;
		}
		DbClass.sqlclose (cursor);

		aanz = caanz = i;
        SetHoLines (1);
        SetVeLines (1);
		PtY = 410;
		if (scrfx > (double) 1.0)
		{
			PtY = (int) (double) ((double) PtY * scrfx);
		}
        syskey = 0;

        EnableWindow (NuBlock.GeteNumWindow (), FALSE);
        idx = ShowWKopf (NuBlock.GeteNumWindow (), (char *) caarr, aanz,
                        (char *) &ca, sizeof (struct CA),
                         &cartform, &artvl, &artub);
		CloseControls (&artub);
        EnableWindow (NuBlock.GeteNumWindow (), TRUE);
        SetHoLines (1);
	    EnableWindows (eKopfWindow, FALSE);
        EnableWindows (eFussWindow, FALSE);
        EnableWindows (BackWindow2, FALSE);
        EnableWindows (BackWindow3, FALSE);
        EnableWindows (eWindow, FALSE);
        if (syskey == KEY5 || syskey == KEYESC)
        {
			    GlobalFree (caarr);
		        caarr = NULL;
                return FALSE;
        }

//        strcpy (NuBlock.Geteditbuff (), caarr[idx].a);
		a_wahl = ratod(caarr[idx].a);
		GlobalFree (caarr);
		caarr = NULL;
		Sleep (50);
//		display_form (NuBlock.GeteNumWindow (), NuBlock.GetEditForm (), 0, 0);
//		NuBlock.SetEditFocus ();
		return 0;
}


int doChargeChoise (void)
/**
Auswahl ueber Chargen im Abgangslager
**/
{
	    static long aanz = 0;
		double a;
		char a_bz1 [25];
//		char a_bz2 [25];
		char chargennr [14];
		char bsd_lager [13];
		double bsd_gew;
		int cursor;
		int idx;
		int i;
//		a_wahl = double(0);
		a_wahl = ratod(NuBlock.Geteditbuff());
		if (a_wahl == double(0))
		{
			doartchoiselgr(); 
			if (syskey == KEY5) return 0;
		}
		if (scrfx > (double) 1.0)
		{
                cartfont.FontHeight = (int) (double) ((double) 120 *  scrfx);
		}
		casort = cbsort = 1;
		if (cchargearr)
		{
			    GlobalFree (cchargearr);
		}
		sprintf(bsd_lager,"%ld",LagerAb);
        DbClass.sqlin  ((char *) bsd_lager, 0, 12);
        DbClass.sqlin  ((double *) &a_wahl, 3, 0);
        DbClass.sqlout  ((long *) &aanz, 2, 0);

        DbClass.sqlcomm ("select count (*) from a_bas,bsds where a_bas.bsd_kz = \"J\" "
								"and a_bas.a = bsds.a and bsds.bsd_lgr_ort = ? and a_bas.a = ? ");
		if (aanz == 0)
		{
			disp_messG ("Keine Charge gefunden", 2);
  	        EnableWindows (eKopfWindow, FALSE);
            EnableWindows (eFussWindow, FALSE);
            EnableWindows (BackWindow2, FALSE);
            EnableWindows (BackWindow3, FALSE);
            EnableWindows (eWindow, FALSE);
			return 0;
		}

        cchargearr = (struct CCHARGE *) GlobalAlloc (GMEM_FIXED, aanz * sizeof (struct CCHARGE));
		if (cchargearr == NULL)
		{
			disp_messG ("Kein Artikel im Lager gefunden", 2);
  	        EnableWindows (eKopfWindow, FALSE);
            EnableWindows (eFussWindow, FALSE);
            EnableWindows (BackWindow2, FALSE);
            EnableWindows (BackWindow3, FALSE);
            EnableWindows (eWindow, FALSE);
			return 0;
		}

        DbClass.sqlin  ((char *) bsd_lager, 0, 12);
        DbClass.sqlin  ((double *) &a_wahl, 3, 0);
        DbClass.sqlout  ((double *) &a, 3, 0);
        DbClass.sqlout  ((char *)  a_bz1, 0, 25);
        DbClass.sqlout  ((char *)  chargennr, 0, 14);
        DbClass.sqlout  ((double *)  &bsd_gew, 3, 0);
		if (sort_a == FALSE)
		{
		         cursor = DbClass.sqlcursor ("select a_bas.a, a_bas.a_bz1, bsds.chargennr, bsds.bsd_gew from a_bas,bsds "
			                        "where a_bas.a > 0 and a_bas.bsd_kz = \"J\" "
								"and a_bas.a = bsds.a and bsds.bsd_lgr_ort = ? and a_bas.a = ? "
									"order by a_bas.a_bz1 ");
		}
		else
		{
		         cursor = DbClass.sqlcursor ("select a_bas.a, a_bas.a_bz1, bsds.chargennr, bsds.bsd_gew from a_bas,bsds "
			                        "where a_bas.a > 0 and a_bas.bsd_kz = \"J\" "
								"and a_bas.a = bsds.a and bsds.bsd_lgr_ort = ? and a_bas.a = ?  "
									"order by a_bas.a ");
		}
		i = 0;
		while (DbClass.sqlfetch (cursor) == 0)
		{
			sprintf (cchargearr[i].a, "%13.0lf", a);
			strcpy  (cchargearr[i].a_bz1, a_bz1);
			strcpy  (cchargearr[i].chargennr, chargennr);
			sprintf(cchargearr[i].menge,"%10.3f",bsd_gew);
			i = i + 1;
		}
		DbClass.sqlclose (cursor);

		aanz = caanz = i;
        SetHoLines (1);
        SetVeLines (1);
		PtY = 410;
		if (scrfx > (double) 1.0)
		{
			PtY = (int) (double) ((double) PtY * scrfx);
		}
        syskey = 0;

        EnableWindow (NuBlock.GeteNumWindow (), FALSE);
        idx = ShowWKopf (NuBlock.GeteNumWindow (), (char *) cchargearr, aanz,
                        (char *) &ccharge, sizeof (struct CCHARGE),
                         &cchgform, &chgvl, &chgub);
		CloseControls (&chgub);
        EnableWindow (NuBlock.GeteNumWindow (), TRUE);
        SetHoLines (1);
	    EnableWindows (eKopfWindow, FALSE);
        EnableWindows (eFussWindow, FALSE);
        EnableWindows (BackWindow2, FALSE);
        EnableWindows (BackWindow3, FALSE);
        EnableWindows (eWindow, FALSE);
        if (syskey == KEY5 || syskey == KEYESC)
        {
			    GlobalFree (cchargearr);
		        cchargearr = NULL;
                return FALSE;
        }

        strcpy (NuBlock.Geteditbuff (), cchargearr[idx].a);
        strcpy (ChargeAuswahl, cchargearr[idx].chargennr);
		GlobalFree (cchargearr);
		caarr = NULL;
		cchargearr = NULL;
		Sleep (50);
		display_form (NuBlock.GeteNumWindow (), NuBlock.GetEditForm (), 0, 0);
		NuBlock.SetEditFocus ();
	    NuBlock.NumEnterBreak ();
		return 0;
}



void menue_end (void)
/**
Menue beenden.
**/
{
         PostQuitMessage (0);
}

int menfunc0 (void)
/**
Function fuer Button 10
**/
{
         HWND aktfocus;
         field *feld;

         feld = &MainButton2.mask[3];
         aktfocus = GetFocus ();
         menue_end ();
         return 0;
}

int func1 (void)
/**
Button fuer Pfeil links bearbeiten.
**/
{
         field *feld;

         if (KomplettAktiv)
         {
                 // PostMessage (NULL, WM_CHAR,  (WPARAM) ' ', 0l);
                 SelectAktCtr ();
                 return 1;
         }

         if (ListeAktiv)
         {
                 PostMessage (eWindow, WM_KEYDOWN,  (WPARAM) VK_LEFT, 0l);
                 return 1;
         }

         if (PtListeAktiv)
         {
                 PostMessage (PtWindow, WM_KEYDOWN,  (WPARAM) VK_LEFT, 0l);
                 return 1;
         }

         if (NumEnterAktiv)
         {
                 SetFocus (NumEnterWindow);
                 SendMessage (NumEnterWindow, WM_KEYDOWN,
                                     (WPARAM) VK_LEFT, 0l);
                 return 1;
         }

         feld = GetAktField ();
         SetFocus (feld->feldid);
         PostMessage (GetParent (feld->feldid), WM_KEYDOWN,
                                            (WPARAM) VK_LEFT,
                                            0l);
         feld = GetAktField ();
         SetColFocus (feld->feldid);
         display_form (BackWindow3, &MainButton2, 0, 0);
         AktButton = GetAktButton ();
         return 0;
}

int func2 (void)
/**
Button fuer Pfeil oben bearbeiten.
**/
{
         field *feld;

         if (KomplettAktiv)
         {
                 PostMessage (AktivDialog, WM_KEYDOWN,  (WPARAM) VK_UP, 0l);
                 return 1;
         }
         if (ListeAktiv)
         {
                 PostMessage (eWindow, WM_KEYDOWN,  (WPARAM) VK_UP, 0l);
                 return 1;
         }
         if (PtListeAktiv)
         {
                 PostMessage (PtWindow, WM_KEYDOWN,  (WPARAM) VK_UP, 0l);
                 return 1;
         }
         if (NumEnterAktiv)
         {
                 SetFocus (NumEnterWindow);
                 SendMessage (NumEnterWindow, WM_KEYDOWN,
                                     (WPARAM) VK_UP, 0l);
                 return 1;
         }
         feld = GetAktField ();
         SetFocus (feld->feldid);
         PostMessage (GetParent (feld->feldid), WM_KEYDOWN,
                                            (WPARAM) VK_UP,
                                            0l);
         feld = GetAktField ();
         SetColFocus (feld->feldid);
         display_form (BackWindow3, &MainButton2, 0, 0);
         AktButton = GetAktButton ();
         return 0;
}

int func3 (void)
/**
Button fuer Pfeil unten bearbeiten.
**/
{
         field *feld;

         if (KomplettAktiv)
         {
                 PostMessage (AktivDialog, WM_KEYDOWN,  (WPARAM) VK_DOWN, 0l);
                 return 1;
         }
         if (ListeAktiv)
         {
                 PostMessage (eWindow, WM_KEYDOWN,  (WPARAM) VK_DOWN, 0l);
                 return 1;
         }
         if (PtListeAktiv)
         {
                 PostMessage (PtWindow, WM_KEYDOWN,  (WPARAM) VK_DOWN, 0l);
                 return 1;
         }
         if (NumEnterAktiv)
         {
                 SetFocus (NumEnterWindow);
                 SendMessage (NumEnterWindow, WM_KEYDOWN,
                                     (WPARAM) VK_DOWN, 0l);
                 return 1;
         }
         feld = GetAktField ();
         SetFocus (feld->feldid);
         PostMessage (GetParent (feld->feldid), WM_KEYDOWN,
                                            (WPARAM) VK_DOWN,
                                             0l);
         feld = GetAktField ();
         SetColFocus (feld->feldid);
         display_form (BackWindow3, &MainButton2, 0, 0);
         AktButton = GetAktButton ();
         return 0;
}

int func4 (void)
/**
Button fuer Pfeil rechts bearbeiten.
**/
{
         field *feld;

         if (KomplettAktiv)
         {
                 PostMessage (AktivDialog, WM_KEYDOWN,  (WPARAM) ' ', 0l);
                 return 1;
         }

         if (ListeAktiv)
         {
                 PostMessage (eWindow, WM_KEYDOWN,  (WPARAM) VK_RIGHT, 0l);
                 return 1;
         }
         if (PtListeAktiv)
         {
                 PostMessage (PtWindow, WM_KEYDOWN,  (WPARAM) VK_RIGHT, 0l);
                 return 1;
         }
         if (NumEnterAktiv)
         {
                 SetFocus (NumEnterWindow);
                 SendMessage (NumEnterWindow, WM_KEYDOWN,
                                     (WPARAM) VK_RIGHT, 0l);
                 return 1;
         }

         feld = GetAktField ();
         SetFocus (feld->feldid);
         PostMessage (GetParent (feld->feldid), WM_KEYDOWN,
                                            (WPARAM) VK_RIGHT,
                                            0l);
         feld = GetAktField ();
         SetColFocus (feld->feldid);
         display_form (BackWindow3, &MainButton2, 0, 0);
         AktButton = GetAktButton ();
         return 0;
}

void backmenue (char *men)
/**
Vorhergehendes Menue ermitteln.
**/
{
          char *pos;

          menue_end ();
          return;

          pos = men + strlen (men) - 1;

          for (; pos >= men; pos --)
          {
                    if (*pos != '0')
                     {
                            *pos = '0';
                            break;
                    }
          }
}


int func5 (void)
/**
Function fuer Zurueckbutton bearbeiten
**/
{
         if (PtListeAktiv)
         {
                 PostMessage (PtWindow, WM_KEYDOWN,  (WPARAM) VK_F5, 0l);
                 return 1;
         }

         if (ListeAktiv)
         {
                 PostMessage (eWindow, WM_KEYDOWN,  (WPARAM) VK_F5, 0l);
                 return 1;
         }

         if (KomplettAktiv)
         {
                 PostMessage (AktivDialog, WM_KEYDOWN,  (WPARAM) VK_F5, 0l);
                 return 1;
         }
         if (NumEnterAktiv)
         {
                 SetFocus (NumEnterWindow);
                 PostMessage (NumEnterWindow, WM_KEYDOWN,
                                     (WPARAM) VK_F5, 0l);
                 return 1;
         }

         if (KopfEnterAktiv)
         {
                 PostMessage (AufKopfWindow, WM_KEYDOWN,
                                     (WPARAM) VK_F5, 0l);
                 return 1;
         }


         backmenue (menue.prog);
         return 0;
}

int dokey5 (void)
/**
Aktion bei Taste F5 (zurueck)
**/
{
         field *feld;

         menue_end ();
         return 0;

         backmenue (menue.prog);
         feld = &MainButton.mask[0];
         SetFocus (feld->feldid);
         AktButton = GetAktButton ();
         display_form (BackWindow2, &MainButton, 0, 0);
         UpdateWindow (BackWindow2);
         display_form (BackWindow3, &MainButton2, 0, 0);
         AktButton = GetAktButton ();
         return 0;
}


int func6 (void)
/**
Function fuer OK setzen.
**/
{
	     if (QmAktiv)
		 {
                  PostMessage (AufKopfWindow,
                               WM_KEYDOWN,  (WPARAM) VK_F12, 0l);
				  return 1;
		 }

         if (PtListeAktiv)
         {
                  PostMessage (PtWindow,
                               WM_KEYDOWN,  (WPARAM) VK_RETURN, 0l);
                 return 1;
         }


         if (KomplettAktiv)
         {
                 SelectAktCtr ();
                 return 1;
         }

         if (KopfEnterAktiv && ListeAktiv == 0)
         {
                 if (EnterTyp == SUCHEN)
                 {
                        PostMessage (AufKopfWindow,
                               WM_KEYDOWN,  (WPARAM) VK_F12, 0l);
                 }
                 else
                 {
                        PostMessage (AufKopfWindow,
                               WM_KEYDOWN,  (WPARAM) VK_RETURN, 0l);
                 }
                 return 1;
         }

         if (LiefEnterAktiv && ListeAktiv == 0)
         {
                 syskey = KEY12;
                 PostMessage (AufKopfWindow,
                               WM_KEYDOWN,  (WPARAM) VK_F12, 0l);
                 return 1;
         }

         if (ListeAktiv == 0)
         {
                 return 0;
         }

         PostMessage (eWindow, WM_KEYDOWN,  (WPARAM) VK_RETURN, 0l);
         return 0;
}

int func7 (void)
/**
Function fuer OK setzen.
**/
{
         extern BOOL StartWiegen;
         if (KomplettAktiv) return 0;

         memcpy (&aufps, &aufp_null, sizeof (aufps));
         StartWiegen = TRUE;
//         beginwork ();
         ArtikelWiegen (LogoWindow);
//         commitwork ();
         StartWiegen = FALSE;
         memcpy (&aufps, &aufparr[0], sizeof (aufps));

         return 0;
}

int func8 (void)
/**
Function fuer Benutzer bearbeiten
**/
{
         if (ListeAktiv == 0) return 0;
         if (KomplettAktiv) return 0;

         EnableWindow (eKopfWindow, FALSE);
         EnableWindow (eFussWindow, FALSE);
         SetKomplett ();
         EnableWindow (eKopfWindow, TRUE);
         EnableWindow (eFussWindow, TRUE);
         break_list ();
         return 0;
}


/*
void printlogo (HBITMAP hbr, int x, int y, DWORD mode)
{

        int bmy;
        int bmx;
        HDC      hdc;

        GetObject (hbr, sizeof (BITMAP), &bm);
        bmy = bm.bmHeight / 2;
        bmx = bm.bmWidth / 2;
        hdc = fithdc;
        hdcMemory  = CreateCompatibleDC(hdc);
        hbmOld = SelectObject (hdcMemory, hbr);

        SetViewportOrgEx (hdc, bmx, bmy, NULL);
        SetStretchBltMode (hdc, COLORONCOLOR);
        StretchBlt (hdc, -bmx + xstretch / 2, -bmy + ystretch / 2,
                         bm.bmWidth - xstretch, bm.bmHeight - ystretch,
                         hdcMemory,0, 0,
                         bm.bmWidth, bm.bmHeight, mode);

        SelectObject (hdcMemory, hbmOld);

        DeleteDC (hdcMemory);
        if (sign == 1)
        {
            ystretch += (plus * logory);
            if (ystretch >= bm.bmHeight * 2)
            {
                        ystretch = bm.bmHeight * 2;
                        sign = -1;
            }
        }
        else if (sign == -1)
        {
            ystretch -= (plus * logory);
            if (ystretch <= 0)
            {
                        ystretch = 0;
                        sign = 1;
            }
        }
        if (signx == 1)
        {
            xstretch += (xplus * logorx);
            if (xstretch >= bm.bmWidth * 2)
            {
                        xstretch = bm.bmWidth * 2;
                        signx = -1;
            }
        }
        else if (signx == -1)
        {
            xstretch -= (xplus * logorx);
            if (xstretch <= 0)
            {
                        xstretch = 0;
                        signx = 1;
            }
        }
}
*/

void paintlogo (HDC hdc, HBITMAP hbr, int x, int y, DWORD mode)
{

        int bmy;
        int bmx;

        GetObject (hbr, sizeof (BITMAP), &bm);
        bmy = bm.bmHeight / 2;
        bmx = bm.bmWidth / 2;
        hdcMemory  = CreateCompatibleDC(hdc);
        hbmOld = SelectObject (hdcMemory, hbr);

        SetViewportOrgEx (hdc, bmx, bmy, NULL);
        SetStretchBltMode (hdc, COLORONCOLOR);
        StretchBlt (hdc, -bmx + xstretch / 2, -bmy + ystretch / 2,
                         bm.bmWidth - xstretch, bm.bmHeight - ystretch,
                         hdcMemory,0, 0,
                         bm.bmWidth, bm.bmHeight, mode);

        SelectObject (hdcMemory, hbmOld);

        DeleteDC (hdcMemory);
        if (sign == 1)
        {
            ystretch += (plus * logory);
            if (ystretch >= bm.bmHeight * 2)
            {
                        ystretch = bm.bmHeight * 2;
                        sign = -1;
            }
        }
        else if (sign == -1)
        {
            ystretch -= (plus * logory);
            if (ystretch <= 0)
            {
                        ystretch = 0;
                        sign = 1;
            }
        }
        if (signx == 1)
        {
            xstretch += (xplus * logorx);
            if (xstretch >= bm.bmWidth * 2)
            {
                        xstretch = bm.bmWidth * 2;
                        signx = -1;
            }
        }
        else if (signx == -1)
        {
            xstretch -= (xplus * logorx);
            if (xstretch <= 0)
            {
                        xstretch = 0;
                        signx = 1;
            }
        }
}

void strechbmp (HBITMAP hbr, int x, int y, DWORD mode)
{

        int bmy;
        int bmx;
		int bmyst;
        int bmxst;
        HDC      hdc;

        xstretch = ystretch = 0;
        GetObject (hbr, sizeof (BITMAP), &bm);
		bmxst = (int) (double) ((double) bm.bmWidth * scrfx);
		bmyst = (int) (double) ((double) bm.bmHeight * scrfy);

        bmy = bmyst / 2;
        bmx = bmxst / 2;
//        bmy = bm.bmHeight / 2;
//        bmx = bm.bmWidth / 2;
        hdc = fithdc;
        hdcMemory  = CreateCompatibleDC(hdc);
        hbmOld = SelectObject (hdcMemory, hbr);

        SetViewportOrgEx (hdc, bmx, bmy, NULL);
        SetStretchBltMode (hdc, COLORONCOLOR);
        StretchBlt (hdc, -bmx + xstretch / 2, -bmy + ystretch / 2,
                         bmxst - xstretch, bmyst - ystretch,
                         hdcMemory,0, 0,
                         bm.bmWidth, bm.bmHeight, mode);

        SelectObject (hdcMemory, hbmOld);

        DeleteDC (hdcMemory);
}


void printlogo (HBITMAP hbr, int x, int y, DWORD mode)
{

        int bmy;
        int bmx;
		int bmyst;
        int bmxst;
        HDC      hdc;

//        GetObject (hbr, sizeof (BITMAP), &bm);
		bmxst = (int) (double) ((double) bm.bmWidth * scrfx);
		bmyst = (int) (double) ((double) bm.bmHeight * scrfy);

        bmy = bmyst / 2;
        bmx = bmxst / 2;
//        bmy = bm.bmHeight * scrfy / 2);
//        bmx = bm.bmWidth / 2;
//        hdc = fithdc;

/*
		xstretch = (int) (double) ((double) xstretch * scrfx);
		ystretch = (int) (double) ((double) ystretch * scrfy);
*/

        hdc = GetDC (FitWindow);
        hdcMemory  = CreateCompatibleDC(hdc);
        hbmOld = SelectObject (hdcMemory, hbr);

        SetViewportOrgEx (hdc, bmx, bmy, NULL);
        SetStretchBltMode (hdc, COLORONCOLOR);
        StretchBlt (hdc, -bmx + xstretch / 2, -bmy + ystretch / 2,
                         bmxst - xstretch, bmyst - ystretch,
                         hdcMemory,0, 0,
                         bm.bmWidth, bm.bmHeight, mode);

//        SelectObject (hdcMemory, hbmOld);

        DeleteDC (hdcMemory);
        ReleaseDC (FitWindow, hdc);
        if (sign == 1)
        {
            ystretch += (plus * logory);
            if (ystretch >= bmyst * 2)
            {
                        ystretch = bmyst * 2;
                        sign = -1;
            }
        }
        else if (sign == -1)
        {
            ystretch -= (plus * logory);
            if (ystretch <= 0)
            {
                        ystretch = 0;
                        sign = 1;
            }
        }
        if (signx == 1)
        {
            xstretch += (xplus * logorx);
            if (xstretch >= bmxst * 2)
            {
                        xstretch = bmxst * 2;
                        signx = -1;
            }
        }
        else if (signx == -1)
        {
            xstretch -= (xplus * logorx);
            if (xstretch <= 0)
            {
                        xstretch = 0;
                        signx = 1;
            }
        }
}

void LogoBitmap (HDC hdc)
/**
Bitmap in Lo�gofenster schreiben.
**/
{
	    RECT rect;

		GetClientRect (LogoWindow, &rect);

		bMap.StrechBitmap (hdc, rect.right, rect.bottom);
}


/*
void strechbmp (HBITMAP hbr, int x, int y, DWORD mode)
{

        HDC      hdc;

        GetObject (hbr, sizeof (BITMAP), &bm);
        hdc = fithdc;
        hdcMemory  = CreateCompatibleDC(hdc);
        hbmOld = SelectObject (hdcMemory, hbr);

        // SetViewportOrgEx (hdc, bmx, bmy, NULL);
        SetStretchBltMode (hdc, COLORONCOLOR);
        StretchBlt (hdc, 0, 0,
                         bm.bmWidth, bm.bmHeight,
                         hdcMemory,0, 0,
                         bm.bmWidth, bm.bmHeight, mode);

        SelectObject (hdcMemory, hbmOld);
        DeleteDC (hdcMemory);
}
*/

void printbmp (HBITMAP hbr, int x, int y, DWORD mode)
{
        RECT rect;

        GetClientRect (hMainWindow,  &rect);
        GetObject (hbr, sizeof (BITMAP), &bm);
        x = (rect.right - bm.bmWidth) / 2;
        y = (rect.bottom - bm.bmHeight) / 2;
        hdc = GetDC (hFitWindow);
        hdcMemory  = CreateCompatibleDC(hdc);
        hbmOld = SelectObject (hdcMemory, hbr);

        BitBlt (hdc, x, y, bm.bmWidth, bm.bmHeight,
                           hdcMemory,0, 0, mode);

        SelectObject (hdcMemory, hbmOld);

        DeleteDC (hdcMemory);
        ReleaseDC (hFitWindow, hdc);
}

int ScrollMenue ()
/**
Leere MenueZeilen entfernrn.
**/
{
        int i, j;

        memcpy (&scmenue, &menue, sizeof (MENUE));
        memcpy (&menue,   &leermenue, sizeof (MENUE));
        menue.mdn = scmenue.mdn;
        menue.fil = scmenue.fil;
        strcpy (menue.pers, scmenue.pers);
        strcpy (menue.prog, scmenue.prog);
        menue.anz_menue = scmenue.anz_menue;

        for (i = 0,j = 0; j < 10; j ++)
        {
                clipped (mentab[j]);
                if (strcmp (mentab[j], " ") > 0)
                {
                                    strcpy (mentab0[i], mentab[j]);
                                    strcpy (zeitab0[i], zeitab[j]);
                                    i ++;
                }
        }
        return (i);
}

void GetAutotara (void)
/**
Default fuer Atou-Tara holen und setzen.
**/
{
	    char tara_auto [2];

        strcpy (tara_auto, "N");
        DbClass.sqlin   ((long *) &StdWaage, 2, 0);
        DbClass.sqlout  ((char *) tara_auto, 0, 2);
        DbClass.sqlcomm ("select tara_auto from opt_mci where sys = ?");
		if (tara_auto [0] == 'J')
		{
			setautotara (TRUE);
		}
		else
		{
			setautotara (FALSE);
		}
}


//  Hauptprogramm

int      PASCAL WinMain(HANDLE hInstance,HANDLE hPrevInstance,
                        LPSTR lpszCmdLine,int nCmdShow)
{
        HCURSOR oldcursor;
        char cfg_v [5];
		HDC hdc;
		char buffer [256];


		sprintf (Bearbeitetxt, "%s", Btxt);
        sprintf (LagerZutxt, "%s ???", LZutxt);
        sprintf (LagerAbtxt, "%s ???", LAbtxt);
		strcpy (WiegTxt,"Bearbeiten");
	    GetForeground ();
        GraphikMode = IsGdiPrint ();

		WithSpezEti = FALSE;
        if (ProgCfg.GetCfgValue ("eti_kiste", cfg_v) ==TRUE)
        {
			         if (strcmp (cfg_v, "NO"))
					 {
                           eti_kiste = (char *) GlobalAlloc (GMEM_FIXED , strlen (cfg_v) + 1);
					       if (eti_kiste)
						   {
					           strcpy (eti_kiste, cfg_v);
						   }
					 }
		}
        if (ProgCfg.GetCfgValue ("eti_ki_drk", cfg_v) ==TRUE)
        {
			         if (strcmp (cfg_v, "NO"))
					 {
                           eti_ki_drk = (char *) GlobalAlloc (GMEM_FIXED , strlen (cfg_v) + 1);
					       if (eti_ki_drk)
						   {
					           strcpy (eti_ki_drk, cfg_v);
						   }
					 }
		}

        if (ProgCfg.GetCfgValue ("eti_ki_typ", cfg_v) ==TRUE)
        {
                     eti_ki_typ = atoi (cfg_v);
		}

        if (ProgCfg.GetCfgValue ("eti_nve", cfg_v) ==TRUE)
        {
			         if (strcmp (cfg_v, "NO"))
					 {
                           eti_nve = (char *) GlobalAlloc (GMEM_FIXED , strlen (cfg_v) + 1);
					       if (eti_nve)
						   {
					           strcpy (eti_nve, cfg_v);
						   }
					 }
		}
        if (ProgCfg.GetCfgValue ("eti_nve_drk", cfg_v) ==TRUE)
        {
			         if (strcmp (cfg_v, "NO"))
					 {
                           eti_nve_drk = (char *) GlobalAlloc (GMEM_FIXED , strlen (cfg_v) + 1);
					       if (eti_nve_drk)
						   {
					           strcpy (eti_nve_drk, cfg_v);
						   }
					 }
		}

        if (ProgCfg.GetCfgValue ("eti_nve_typ", cfg_v) ==TRUE)
        {
                     eti_nve_typ = atoi (cfg_v);
		}

        if (ProgCfg.GetCfgValue ("eti_ez", cfg_v) ==TRUE)
        {
			         if (strcmp (cfg_v, "NO"))
					 {
                            eti_ez = (char *) GlobalAlloc (GMEM_FIXED , strlen (cfg_v) + 1);
					        if (eti_ez)
							{
					           strcpy (eti_ez, cfg_v);
							}
					 }
		}
        if (ProgCfg.GetCfgValue ("eti_ez_drk", cfg_v) ==TRUE)
        {
			         if (strcmp (cfg_v, "NO"))
					 {
                            eti_ez_drk = (char *) GlobalAlloc (GMEM_FIXED , strlen (cfg_v) + 1);
					        if (eti_ez_drk)
							{
					           strcpy (eti_ez_drk, cfg_v);
							}
					 }
		}

        if (ProgCfg.GetCfgValue ("eti_ez_typ", cfg_v) ==TRUE)
        {
                     eti_ez_typ = atoi (cfg_v);
		}

        if (ProgCfg.GetCfgValue ("eti_ev", cfg_v) ==TRUE)
        {
			         if (strcmp (cfg_v, "NO"))
					 {
                            eti_ev = (char *) GlobalAlloc (GMEM_FIXED , strlen (cfg_v) + 1);
					        if (eti_ev)
							{
					           strcpy (eti_ev, cfg_v);
							}
					 }
		}

        if (ProgCfg.GetCfgValue ("eti_ev_drk", cfg_v) ==TRUE)
        {
			         if (strcmp (cfg_v, "NO"))
					 {
                            eti_ev_drk = (char *) GlobalAlloc (GMEM_FIXED , strlen (cfg_v) + 1);
					        if (eti_ev_drk)
							{
					           strcpy (eti_ev_drk, cfg_v);
							}
					 }
		}

        if (ProgCfg.GetCfgValue ("eti_ev_typ", cfg_v) ==TRUE)
        {
                     eti_ev_typ = atoi (cfg_v);
		}

        if (ProgCfg.GetCfgValue ("eti_artikel", cfg_v) ==TRUE)
        {
			         if (strcmp (cfg_v, "NO"))
					 {
                            eti_artikel = (char *) GlobalAlloc (GMEM_FIXED , strlen (cfg_v) + 1);
					        if (eti_artikel)
							{
					           strcpy (eti_artikel, cfg_v);
							}
					 }
		}

        if (ProgCfg.GetCfgValue ("eti_a_drk", cfg_v) ==TRUE)
        {
			         if (strcmp (cfg_v, "NO"))
					 {
                            eti_a_drk = (char *) GlobalAlloc (GMEM_FIXED , strlen (cfg_v) + 1);
					        if (eti_a_drk)
							{
					           strcpy (eti_a_drk, cfg_v);
							}
					 }
		}

        if (ProgCfg.GetCfgValue ("eti_a_typ", cfg_v) ==TRUE)
        {
                     eti_a_typ = atoi (cfg_v);
		}

        if (ProgCfg.GetCfgValue ("EnterEtiAnz", cfg_v) ==TRUE)
        {
			         TOUCHF::EnterAnz = atoi (cfg_v);
					 EnterEtiAnz = atoi(cfg_v);
		}

        if (ProgCfg.GetCfgValue ("auto_eti", cfg_v) ==TRUE)
        {
                    auto_eti = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("colborder", cfg_v) ==TRUE)
        {
                     colborder = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("lsdefault", cfg_v) ==TRUE)
        {
                     lsdefault = min (1, max (0,atoi (cfg_v)));
        }

        if (ProgCfg.GetCfgValue ("best_auswahl_direct", cfg_v) ==TRUE)
        {
                     best_ausw_direct = min (1, max (0,atoi (cfg_v)));
        }

		/** sollte TRUE sein , funkt. sonst nicht !!!
        if (ProgCfg.GetCfgValue ("liste_direct", cfg_v) ==TRUE)
        {
                     liste_direct = min (1, max (0,atoi (cfg_v)));
        }
		*******/

        if (ProgCfg.GetCfgValue ("lief_bzg_zwang", cfg_v) ==TRUE)
        {
                     lief_bzg_zwang = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("druck_zwang", cfg_v) ==TRUE)
        {
                     druck_zwang = atoi (cfg_v);
        }


        if (ProgCfg.GetCfgValue ("a_auswahl_direct", cfg_v) ==TRUE)
        {
                     a_ausw_direct = atoi (cfg_v);
        }

        if (ProgCfg.GetCfgValue ("lief_auswahl_direct", cfg_v) ==TRUE)
        {
                     lief_ausw_direct = atoi (cfg_v);
        }

        if (ProgCfg.GetCfgValue ("hbk_direct", cfg_v) ==TRUE)
        {
                     hbk_direct = atoi (cfg_v);
        }

        if (ProgCfg.GetCfgValue ("multibest", cfg_v) ==TRUE)
        {
                     multibest = min (1, max (0,atoi (cfg_v)));
        }

        if (ProgCfg.GetCfgValue ("wieg_neu_direct", cfg_v) ==TRUE)
        {
                     wieg_neu_direct = atoi (cfg_v);
        }

        if (ProgCfg.GetCfgValue ("AutoSleep", cfg_v) ==TRUE)
        {
                     AutoSleep = atol (cfg_v);
        }

        if (ProgCfg.GetCfgValue ("wieg_direct", cfg_v) ==TRUE)
        {
                     wieg_direct = atoi (cfg_v);
        }

        if (ProgCfg.GetCfgValue ("hand_direct", cfg_v) ==TRUE)
        {
                     hand_direct = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("sort_a", cfg_v) == TRUE)
        {
		             sort_a =  min (1, max (0, atoi (cfg_v)));
        }

        if (ProgCfg.GetCfgValue ("hand_tara_sum", cfg_v) == TRUE)
        {
		             hand_tara_sum =  atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("fest_tara_sum", cfg_v) == TRUE)
        {
		             fest_tara_sum =  atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("fest_tara_anz", cfg_v) == TRUE)
        {
		             fest_tara_anz =  atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("EinhEk", cfg_v) == TRUE)
        {
		             EktoMeEinh =  atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("autocursor", cfg_v) == TRUE)
		{
			         autocursor = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("DrKomplett", cfg_v) == TRUE)
		{
			         DrKomplett = atoi (cfg_v);
		}

        if (getenv ("testmode"))
        {
                     testmode = atoi (getenv ("testmode"));
        }
        if (ProgCfg.GetCfgValue ("mdn_default", cfg_v) ==TRUE)
        {
                    akt_mdn = atoi (cfg_v);
                    Mandant = atoi (cfg_v);

        }
        if (ProgCfg.GetCfgValue ("fil_default", cfg_v) ==TRUE)
        {
                    akt_fil = atoi (cfg_v);
                    Filiale = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("schlart_default", cfg_v) ==TRUE)
        {
                    Schlachtart = atoi (cfg_v);
        }

        if (ProgCfg.GetCfgValue ("LagerAbgang_default", cfg_v) ==TRUE)
        {
                   LagerAb = atoi (cfg_v);
			       sprintf (LagerAbtxt, "%s %hd", LAbtxt, LagerAb);
        }
        if (ProgCfg.GetCfgValue ("LagerZugang_default", cfg_v) ==TRUE)
        {
                   LagerZu = atoi (cfg_v);
			       sprintf (LagerZutxt, "%s %hd", LZutxt, LagerZu);
        }

        if (ProgCfg.GetCfgValue ("Wiegen", cfg_v) ==TRUE)
        {
                   Wiegen = atoi (cfg_v);
                   if (Wiegen == 1) strcpy(WiegTxt, " Wiegen ");

        }

        if (ProgCfg.GetCfgValue ("WeWiepro", cfg_v) ==TRUE)
        {
                   WeWiepro = atoi (cfg_v);

        }
        if (ProgCfg.GetCfgValue ("QsZwang", cfg_v) ==TRUE)
        {
                    QsZwang = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("SperreZugangswechsel", cfg_v) ==TRUE)
        {
                    SperreZugangswechsel = atoi (cfg_v);
        }

/****

		if (eti_kiste && eti_ki_drk)
		{
			         WithSpezEti = TRUE;
		}
		else
		{
			eti_kiste = eti_ki_drk = NULL;
		}

		if (eti_ez && eti_ez_drk)
		{
			         WithSpezEti = TRUE;
		}
		else
		{
			eti_ez = eti_ez_drk = NULL;
		}

		if (eti_ev && eti_ev_drk)
		{
			         WithSpezEti = TRUE;
		}
		else
		{
			eti_ev = eti_ev_drk = NULL;
		}

		if (eti_artikel && eti_a_drk)
		{
			         WithSpezEti = TRUE;
		}
		else
		{
			eti_artikel = eti_a_drk = NULL;
		}

		Touchf.SetEtiAttr (eti_kiste, eti_ez, eti_ev, eti_artikel);
******/

        Kist = new BMAP;
		if (Kist)
		{
                  hdc = GetDC (hMainWindow);
		          sprintf (buffer, "%s\\kiste2.bmp", getenv ("BWSETC"));
		          KistBmp = Kist->ReadBitmap (hdc, buffer);
				  ReleaseDC (hMainWindow, hdc);
		}
        KistG= new BMAP;
		if (KistG)
		{
                  hdc = GetDC (hMainWindow);
		          sprintf (buffer, "%s\\kiste2g.bmp", getenv ("BWSETC"));
		          KistBmpG = KistG->ReadBitmap (hdc, buffer);
				  ReleaseDC (hMainWindow, hdc);
		}

        KistMask = new BMAP;
		if (KistMask)
		{
                  hdc = GetDC (hMainWindow);
		          sprintf (buffer, "%s\\kiste2.msk", getenv ("BWSETC"));
		          KistMaskBmp = KistMask->ReadBitmap (hdc, buffer);
				  ReleaseDC (hMainWindow, hdc);
		}

		LtBlueBrush = CreateSolidBrush (BLUECOL);
		YellowBrush = CreateSolidBrush (YELLOWCOL);
        opendbase ("bws");
        sysdate (KomDatum);
        menue_class.SetSysBen ();
        if ( lgr_class.lese_lgr_bz (LagerAb) == 0) strcpy (LgrAb_krz,lgr.lgr_bz);
		 if (LagerAb == -2) strcpy (LgrAb_krz,LagerMinus2);
		 if (LagerAb == -1) strcpy (LgrAb_krz,LagerMinus1);
		 if (LagerAb == 0) strcpy (LgrAb_krz,LagerNull);
        if ( lgr_class.lese_lgr_bz (LagerZu) == 0) strcpy (LgrZu_krz,lgr.lgr_bz);
        if (sys_ben.berecht != 0)
        {
                   ohne_preis = 1;
        }

        if (getenv ("WAAKT"))
        {
                    strcpy (waadir, getenv ("WAAKT"));
                    sprintf (ergebnis, "%s\\ergebnis", waadir);
        }

        menue_class.Lesesys_inst ();
        FillQsHead ();
        FillQsPos();

        auszeichner = GetStdAuszeichner ();
        sys_peri_class.lese_sys (auszeichner);

        StdWaage = GetStdWaage ();
        sys_peri_class.lese_sys (StdWaage);
        la_zahl = sys_peri.la_zahl;

		GetAutotara ();

        strcpy (sys_par.sys_par_nam, "immer_preis");
        if (sys_par_class.dbreadfirst () == 0)
        {
                    immer_preis = atoi (sys_par.sys_par_wrt);
        }

        strcpy (sys_par.sys_par_nam, "ls_leer_zwang");
        if (sys_par_class.dbreadfirst () == 0)
        {
                    ls_leer_zwang = atoi (sys_par.sys_par_wrt);
        }

        strcpy (sys_par.sys_par_nam, "ls_charge_par");
        if (sys_par_class.dbreadfirst () == 0)
        {
                    ls_charge_par = atoi (sys_par.sys_par_wrt);
        }
        strcpy (sys_par.sys_par_nam, "lsc2_par");
        if (sys_par_class.dbreadfirst () == 0)
        {
                    lsc2_par = atoi (sys_par.sys_par_wrt);
        }


        sprintf (Mandanttxt, "%s %hd", Mtxt, Mandant);
        sprintf (Filialetxt, "%s %hd", Ftxt, Filiale);
//        sprintf (Schlachtarttxt, "%s %hd", Stxt, Schlachtart);
        memcpy (&Schlachtarttxt, &s_artarr[Schlachtart], sizeof (s_artarr));

		/*
		 if (Linie == SCHWEIN)
		 {
			 sprintf (Bearbeitetxt, "%s %s", Btxt, "Schweine");
			 strcpy (fettst_mfa,"MFA");
			 strcpy (pictf_mfa,"%3.1f");
			 memcpy (&insform, &insform_s, sizeof (form));
			 memcpy (&insub, &insub_s, sizeof (form));
			 memcpy (&PrWahl, &PrWahl_s, sizeof (form));
		 }
		 if (Linie == RIND)
		 {
			 sprintf (Bearbeitetxt, "%s %s", Btxt, "Gro�vieh");
			 strcpy (fettst_mfa,"F.St");
			 strcpy (pictf_mfa,"%hd");
			 memcpy (&insform, &insform_r, sizeof (form));
			 memcpy (&insub, &insub_r, sizeof (form));
			 memcpy (&PrWahl, &PrWahl_r, sizeof (form));
		 }
		 */
  	     posInsOK = 0;


		ColBorder = colborder;
        if (strcmp (clipped (sys_inst.projekt), "F H S"))
        {
                    fitbmp   = LoadBitmap (hInstance, "fit1");
        }
        else
        {
                    fitbmp   = LoadBitmap (hInstance, "fhs");
                    IsFhs = 1;
        }
        PfeilL.bmp = LoadBitmap (hInstance, "pfeill");
        PfeilR.bmp = LoadBitmap (hInstance, "pfeilr");
        PfeilU.bmp = LoadBitmap (hInstance, "pfeilu");
        PfeilO.bmp = LoadBitmap (hInstance, "pfeilo");
        GetObject (fitbmp, sizeof (BITMAP), &bm);

        hMainInst = hInstance;
        if (getenv (BWS))
        {
                   strcpy (fitpath, getenv (BWS));
        }
        else
        {
                   strcpy (fitpath, "\\USER\\BWS8000");
        }


        strcpy (rosipath, getenv (RSDIR));

        GetObject (fitbmp, sizeof (BITMAP), &fitbm);

        InitFirstInstance (hInstance);
        if (InitNewInstance (hInstance, nCmdShow) == 0) return 0;

		Qmkopf = new QMKOPF (hMainInst, LogoWindow);
		Qmkopf->SetDevice (scrfy, scrfy);
//		Qmkopf->SetDevice (0.75, 0.75);

        oldcursor = SetCursor (LoadCursor (NULL, IDC_WAIT));
        WaageInit ();
        SetCursor (oldcursor);
        SetFocus (MainButton.mask[0].feldid);
        MainBuActiv ();
        return ProcessMessages ();
}


void InitFirstInstance(HANDLE hInstance)
{
        WNDCLASS wc;

		HDC hdc;
		int col;

		col = 0;
		hdc = GetDC (NULL);
		col = GetDeviceCaps (hdc, BITSPIXEL);
		ReleaseDC (NULL, hdc);
		if (col < 16) ColBorder = FALSE;

        wc.style         =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                            | CS_CLASSDC;
        wc.lpfnWndProc   =  WndProc;
        wc.cbClsExtra    =  0;
        wc.cbWndExtra    =  0;
        wc.hInstance     =  hInstance;
        wc.hIcon         =  LoadIcon (hInstance, "FITICON");
        wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
        wc.lpszMenuName  =  "";
        wc.lpszClassName =  lpszClassName;

        RegisterClass(&wc);

        wc.style         =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                            | CS_OWNDC;
        wc.lpfnWndProc   =  FitLogoProc;
        wc.lpszMenuName  =  "";
        wc.hbrBackground =  GetStockObject (WHITE_BRUSH);
        wc.lpszClassName =  "Fitlogo";
        RegisterClass(&wc);

        wc.lpfnWndProc   =  UhrProc;
        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
        wc.lpszClassName =  "Uhr";
        RegisterClass(&wc);


        wc.lpfnWndProc   =  WndProc;
        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
        wc.lpszClassName =  "Backwind";

        RegisterClass(&wc);

        wc.hbrBackground =  CreateSolidBrush (RGB (0, 255, 255));
        wc.lpszClassName =  "LogoWind";
        RegisterClass(&wc);

        wc.lpfnWndProc   =  GebProc;
        wc.hbrBackground =  CreateSolidBrush (GebColor);
        wc.lpszClassName =  "GebWind";
        RegisterClass(&wc);

        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
        wc.lpszClassName =  "GebWindB";
        RegisterClass(&wc);

        wc.lpfnWndProc   =   MsgProc;
        wc.hbrBackground =   GetStockObject (WHITE_BRUSH);
        wc.lpszClassName =  "AufWiegWhite";
        RegisterClass(&wc);

        wc.lpfnWndProc   =   MsgProc;
        wc.hbrBackground =   CreateSolidBrush (RGB (0, 0, 255));
        wc.lpszClassName =  "AufWiegBlue";
        RegisterClass(&wc);

        wc.lpfnWndProc   =   PosLeerProc;
        wc.hbrBackground =   CreateSolidBrush (RGB (0, 255, 255));
        wc.lpszClassName =  "PosLeer";
        RegisterClass(&wc);

        wc.lpfnWndProc   =   PosLeerProc;
        wc.hbrBackground =   GetStockObject (WHITE_BRUSH);
        wc.lpszClassName =  "PosLeerKopf";
        RegisterClass(&wc);
}

void hKorrMainButton (double fcx, double fcy)
/**
Position von Mainbutton korrigieren.
**/
{
	    fittextlogo.FontHeight = (short) (double)
			           ((double) fittextlogo.FontHeight * fcy);
	    fittextlogo2.FontHeight = (short) (double)
			           ((double) fittextlogo2.FontHeight * fcy);
//	    fittextlogo3.FontHeight = (short) (double)
//			           ((double) fittextlogo3.FontHeight * fcy);
	    if (fittext.mask[0].pos[0] != -1)
		{
			     fittext.mask[0].pos[0] = (short) (double)
					 ((double) fittext.mask[0].pos[0] * fcy);
		}
		if (fittext.mask[0].pos[1] != -1)
		{
			     fittext.mask[0].pos[1] = (short) (double)
					 ((double) fittext.mask[0].pos[1] * fcx);

		}

	    if (fittext2.mask[0].pos[0] != -1)
		{
			     fittext2.mask[0].pos[0] = (short) (double)
					 ((double) fittext2.mask[0].pos[0] * fcy);
		}
		if (fittext2.mask[0].pos[1] != -1)
		{
			     fittext2.mask[0].pos[1] = (short) (double)
					 ((double) fittext2.mask[0].pos[1] * fcx);

		}

/*
	    if (fittext3.mask[0].pos[0] != -1)
		{
			     fittext3.mask[0].pos[0] = (short) (double)
					 ((double) fittext3.mask[0].pos[0] * fcy);
		}
		if (fittext3.mask[0].pos[1] != -1)
		{
			     fittext3.mask[0].pos[1] = (short) (double)
					 ((double) fittext3.mask[0].pos[1] * fcx);

		}
*/
}


void KorrMainButton (form * MainButton, double fcx, double fcy)
/**
Position von Mainbutton korrigieren.
**/
{
	    int i;

		for (i = 0; i < MainButton->fieldanz; i ++)
		{
			if (MainButton->mask[i].pos[0] != -1)
			{
			     MainButton->mask[i].pos[0] = (short) (double)
					 ((double) MainButton->mask[i].pos[0] * fcy);
			}
			if (MainButton->mask[i].pos[1] != -1)
			{
			     MainButton->mask[i].pos[1] = (short) (double)
					 ((double) MainButton->mask[i].pos[1] * fcx);

			}
	        MainButton->mask[i].rows = (short) (double)
			 ((double) MainButton->mask[i].rows * fcy);
	        MainButton->mask[i].length = (short) (double)
			 ((double) MainButton->mask[i].length * fcx);
		}
}

void KorrMainButton2 (form * MainButton, double fcx, double fcy)
/**
Position von Mainbutton korrigieren.
**/
{
	    int i;

		for (i = 0; i < MainButton->fieldanz; i ++)
		{
			if (MainButton->mask[i].pos[0] != -1)
			{
			     MainButton->mask[i].pos[0] = (short) (double)
					 ((double) MainButton->mask[i].pos[0] * fcy);
			}
			if (MainButton->mask[i].pos[1] != -1)
			{
			     MainButton->mask[i].pos[1] = (short) (double)
					 ((double) MainButton->mask[i].pos[1] * fcx);

			}
	        MainButton->mask[i].rows = (short) (double)
			 ((double) MainButton->mask[i].rows * fcy);
	        MainButton->mask[i].length = (short) (double)
			 ((double) MainButton->mask[i].length * fcx);
		}
}



BOOL InitNewInstance(HANDLE hInstance, int nCmdShow)
{
        RECT rect;
        RECT rect1;
        HCURSOR oldcursor;
		TEXTMETRIC tm;
		HDC hdc;
		double fcx, fcy;
        extern mfont ListFont;
		extern form lFussform;
		extern form EtiButton;

        hMainWindow = CreateWindow (lpszClassName,
                                    lpszMainTitle,
                                    WS_DLGFRAME | WS_CAPTION | WS_SYSMENU |
                                    WS_MINIMIZEBOX,
                                    0, 0,
                                    800, 580,
                                    NULL,
                                    NULL,
                                    hInstance,
                                    NULL);
        if (hMainWindow == 0) return FALSE;

        ShowWindow(hMainWindow, SW_SHOWMAXIMIZED);
        hdc = GetDC (hMainWindow);
		GetTextMetrics (hdc, &tm);
		ReleaseDC (hMainWindow, hdc);

		GetWindowRect (hMainWindow, &rect);
		rect.bottom -= (tm.tmHeight + tm.tmHeight / 2);
        ShowWindow(hMainWindow, nCmdShow);
        MoveWindow (hMainWindow, rect.left,
                                rect.top, rect.right, rect.bottom, TRUE);


        fcx = fcy = 1;
        if (rect.right > 900 || rect.right < 700)
        {
		            fcx = (double) rect.right / 800;
 		            fcy = (double) rect.bottom / 562;
                    hKorrMainButton (fcx, fcy);
		            KorrMainButton (&MainButton, fcx, fcy);
		            KorrMainButton2 (&MainButton2, fcx, fcy);
		            KorrMainButton2 (&lFussform, fcx, fcy);
					ListFont.FontHeight = (int) (double)
						((double)  ListFont.FontHeight * fcy);
					ListFont.FontWidth = (int) (double)
						((double)  ListFont.FontWidth * fcx);
					EtiButton.mask[0].pos[1] = (int)
						(double) ((double) EtiButton.mask[0].pos[1] *
						         fcx);
					EtiButton.mask[1].pos[1] = (int)
						(double) ((double) EtiButton.mask[1].pos[1] *
						         fcx);
        }

		scrfx = fcx;
		scrfy = fcy;
        hFitWindow = hMainWindow;
		NuBlock.MainInstance (hMainInst, hMainWindow);
		NuBlock.ScreenParam (scrfx, scrfy);
		Touchf.MainInstance (hMainInst, hMainWindow);
		Touchf.ScreenParam (scrfx, scrfy);
//		Touchf.SetWriteParams (WriteParamKiste, WriteParamEZ,
//			                   WriteParamEV, WriteParamArt);


//        ShowWindow(hMainWindow, nCmdShow);
//        UpdateWindow(hMainWindow);

        hFitWindow = hMainWindow;

        oldcursor = SetCursor (LoadCursor (NULL, IDC_WAIT));
        strcpy (menue.prog, "00000");
        strcpy (menue.prog, "00000");
        SetCursor (oldcursor);

        GetClientRect (hMainWindow,  &rect);

        Sebmpw = (int) (double) ((double) (fitbm.bmWidth + 40) *
                                           scrfx);
        BackWindow1  = CreateWindowEx (
                              WS_EX_CLIENTEDGE,
                              "Backwind",
                              "",
                              WS_CHILD | WS_VISIBLE,
                              10, 10,
/*
                              rect.right - (fitbm.bmWidth + 40),
                              rect.bottom - 100,
*/
                              rect.right - Sebmpw,
                              rect.bottom - 100,
                              hMainWindow,
                              NULL,
                              hInstance,
                              NULL);

        GetClientRect (BackWindow1,  &rect1);
        Sebmpw = (int) (double) ((double) (fitbm.bmWidth + 25) *
                                           scrfx);

        BackWindow2  = CreateWindowEx (
                              WS_EX_CLIENTEDGE,
                              "Backwind",
                              "",
                              WS_CHILD | WS_VISIBLE,
//                              rect.right - (fitbm.bmWidth + 25),
                              rect.right - Sebmpw,
                              10,
//                              fitbm.bmWidth + 15,
                              (int) (double) ((double) (fitbm.bmWidth + 15) * scrfx),
                              rect.bottom - 20,
                              hMainWindow,
                              NULL,
                              hInstance,
                              NULL);

        BackWindow3  = CreateWindowEx (
                              WS_EX_CLIENTEDGE,
                              "Backwind",
                              "",
                              WS_CHILD | WS_VISIBLE,
                              10,
                              rect.bottom - (int) ((double) 60 * scrfy),
//                              rect.bottom - 60,
//                              rect.right - (fitbm.bmWidth + 40),
                              rect.right - (int) (double) ((double) (fitbm.bmWidth + 40) *
                                           scrfx),
//                              rect.bottom - 20 - (rect.bottom - 70),
                              (int) ((double) (rect.bottom - 20 - (rect.bottom - 70)) * scrfy),
                              hMainWindow,
                              NULL,
                              hInstance,
                              NULL);


        LogoWindow  = CreateWindowEx (
                              WS_EX_CLIENTEDGE,
                              "LogoWind",
                              "",
                              WS_CHILD | WS_VISIBLE,
                              5, 5,
                              rect1.right - 10,
                              rect1.bottom - 10,
                              BackWindow1,
                              NULL,
                              hInstance,
                              NULL);
        UpdateWindow (BackWindow1);
        UpdateWindow (BackWindow2);
        UpdateWindow (BackWindow3);
        UpdateWindow (LogoWindow);
		NuBlock.SetParent (LogoWindow);
        return TRUE;
}

int CreateFitWindow (void)
/**
FitWindow erzeugen.
**/
{

        FitWindow = CreateWindow   ("Fitlogo",
                                    "",
                                    WS_CHILD | WS_VISIBLE | WS_DLGFRAME,
                                    5, 5,
//                                    fitbm.bmWidth, fitbm.bmHeight,
                                    (int) (double) ((double) fitbm.bmWidth * scrfx),
									(int) (double) ((double) fitbm.bmHeight * scrfy),
                                    BackWindow2,
                                    NULL,
                                    hMainInst,
                                    NULL);
        fithdc = GetDC (FitWindow);
        if (IsFhs == 0)
        {
                    SetTimer (FitWindow , 1, twait, 0);
        }
        return TRUE;
}

int CreateUhrWindow (void)
/**
UhrWindow erzeugen.
**/
{
        RECT rect;
        int x,y, cx, cy;

        GetClientRect (LogoWindow,  &rect);
        cx = fitbm.bmWidth;
        cy = 13;
        x = 5;
        y = fitbm.bmHeight + 5;

        UhrWindow = CreateWindowEx (
                                    0,
                                    "Uhr",
                                    "",
                                    WS_CHILD | WS_VISIBLE,
                                    x, y,
                                    cx, cy,
                                    BackWindow2,
                                    NULL,
                                    hMainInst,
                                    NULL);
        SetTimer (UhrWindow , 1, twaitU, 0);
        return TRUE;
}


int IsHotKey (MSG *msg)
/**
HotKey Testen.
**/
{
         UCHAR taste;
         int i;
         ColButton *ColBut;
         char *pos;

         if (msg->message != WM_CHAR)
         {
                      return FALSE;
         }

         taste = (char) msg->wParam;
         for (i = 0; i < MainButton.fieldanz; i ++)
         {
                      ColBut = (ColButton *) MainButton.mask[i].feld;
                      if (ColBut->text1)
                      {
                           if ((pos = strchr (ColBut->text1, '&')) &&
                              ((UCHAR) toupper (*(pos + 1)) == toupper (taste)))
                           {
                                   SetFocus (MainButton.mask[i].feldid);
                                   AktButton = GetAktButton ();
                                   SendMessage (AktivWindow,WM_KEYDOWN,
                                                VK_RETURN, 0);
                                   return TRUE;
                           }
                      }
         }

         for (i = 0; i < MainButton2.fieldanz; i ++)
         {
                      ColBut = (ColButton *) MainButton2.mask[i].feld;
                      if (ColBut->text1)
                      {
                           if ((pos = strchr (ColBut->text1, '&')) &&
                              ((UCHAR) toupper (*(pos + 1)) == toupper (taste)))
                           {
                                   SetFocus (MainButton2.mask[i].feldid);
                                   AktButton = GetAktButton ();
                                   return TRUE;
                           }
                      }
         }
         return FALSE;
}

void PrevKey (void)
/**
Vorhergenden Button aktivieren.
**/
{

         if (AktButton >= MainButton.fieldanz) return;
         AktButton = GetAktButton ();
         if (AktButton == 0)
         {
                     AktButton = MainButton.fieldanz - 1;
         }
         else
         {
                     AktButton --;
         }
         if (AktButton < MainButton.fieldanz)
         {
               SetFocus (
                 MainButton.mask [AktButton].feldid);
         }
         else
         {
               SetFocus (
                 MainButton2.mask [AktButton - MainButton.fieldanz].feldid);
         }
}


void NextKey (void)
/**
Naechsten Button aktivieren.
**/
{
	     ColButton *Col;

         if (AktButton >= MainButton.fieldanz) return;

         AktButton = GetAktButton ();
         if (AktButton == MainButton.fieldanz - 1)
         {
                     AktButton = 0;
         }
         else
         {
                     AktButton ++;
         }

         if (AktButton < MainButton.fieldanz)
         {
 		       Col = (ColButton *) MainButton.mask[AktButton].feld;
               SetFocus (
                 MainButton.mask [AktButton].feldid);
         }
         else
         {
               SetFocus (
                 MainButton2.mask [AktButton - MainButton.fieldanz].feldid);
         }
}

void RightKey (void)
/**
Button nach rechts.
**/
{

         AktButton = GetAktButton ();
         if (AktButton <  MainButton.fieldanz)
         {
                     AktButton = MainButton.fieldanz +
                                 MainButton2.fieldanz - 5;
         }
         else if (AktButton == MainButton.fieldanz)
         {
                     AktButton = MainButton.fieldanz - 1;
         }
         else
         {
                     AktButton --;
         }
         if (AktButton < MainButton.fieldanz)
         {
               SetFocus (
                 MainButton.mask [AktButton].feldid);
         }
         else
         {
               SetFocus (
                 MainButton2.mask [AktButton - MainButton.fieldanz].feldid);
         }
}

void LeftKey (void)
/**
Button nach rechts.
**/
{

         AktButton = GetAktButton ();
         if (AktButton <  MainButton.fieldanz)
         {
                     AktButton = MainButton.fieldanz;
         }
         else if (AktButton == MainButton.fieldanz +
                               MainButton2.fieldanz - 5)
         {
                     AktButton = MainButton.fieldanz - 1;
         }
         else
         {
                     AktButton ++;
         }

         if (AktButton < MainButton.fieldanz)
         {
               SetFocus (
                 MainButton.mask [AktButton].feldid);
         }
         else
         {
               SetFocus (
                 MainButton2.mask [AktButton - MainButton.fieldanz].feldid);
         }
}


void ActionKey (void)
/**
Button-Aktion ausfuehren.
**/
{
         field *feld;

         AktButton = GetAktButton ();
         feld = GetAktField ();

         if (feld->after != (int (*) (void)) 0)
         {
                    (*feld->after) ();
         }
         else if (feld->BuId != 0)
         {
                    SendMessage (GetParent (feld->feldid), WM_COMMAND,
                                            MAKELONG (feld->BuId,
                                            feld->feldid),
                                            0l);
         }
}


int IsKeyPress (MSG *msg)
/**
HotKey Testen.
**/
{
         static char keypressed = 0;
         static HWND keyhwnd = 0;
         int taste;
         int i;
         ColButton *ColBut;
         char *pos;



         switch (msg->message)
         {
              case WM_KEYDOWN :
              {
                     if (keypressed)
                     {
                                  return FALSE;
                     }
                     taste = (int) msg->wParam;
                     if (taste == VK_UP)
                     {
                               PrevKey ();
                               return TRUE;
                     }
                     if (taste == VK_DOWN)
                     {
                               NextKey ();
                               return TRUE;
                     }
                     if (taste == VK_RIGHT)
                     {
                               RightKey ();
                               return TRUE;
                     }
                     if (taste == VK_LEFT)
                     {
                               LeftKey ();
                               return TRUE;
                     }
                     if (taste == VK_TAB)
                     {
                         if (GetKeyState (VK_SHIFT) < 0)
                         {
                              PrevKey ();
                              return TRUE;
                         }
                         NextKey ();
                         return TRUE;
                     }
                     if (taste == VK_RETURN)
                     {
                               ActionKey ();
                               return TRUE;
                     }
                     for (i = 0; i < MainButton.fieldanz; i ++)
                     {
                         ColBut = (ColButton *) MainButton.mask[i].feld;
                         if (ColBut->text1)
                         {
                              if ((pos = strchr (ColBut->text1, '&')) &&
                                 ((UCHAR) toupper (*(pos + 1)) == taste))
                              {
                                        keypressed = i + 1;
                                        keyhwnd = MainButton.mask[i].feldid;
                                        SendMessage (keyhwnd,
                                                WM_LBUTTONDOWN,
                                                0l,
                                                0l);
                                        return TRUE;
                              }
                         }
                    }

                    for (i = 0; i < MainButton2.fieldanz; i ++)
                    {
                         ColBut = (ColButton *) MainButton2.mask[i].feld;
                         if (ColBut->text1)
                         {
                              if ((pos = strchr (ColBut->text1, '&')) &&
                                 ((UCHAR) toupper (*(pos + 1)) == taste))
                              {
                                         keypressed = i + MainButton.fieldanz;
                                         keyhwnd = MainButton2.mask[i].feldid;
                                         SendMessage (keyhwnd,
                                                WM_LBUTTONDOWN,
                                                0l,
                                                0l);
                                         return TRUE;
                              }
                         }
                      }
              }
              case WM_KEYUP :
              {
                     if (!keypressed)
                     {
                                  return FALSE;
                     }
                     taste = (int) msg->wParam;
                     for (i = 0; i < MainButton.fieldanz; i ++)
                     {
                         ColBut = (ColButton *) MainButton.mask[i].feld;
                         if (ColBut->text1)
                         {
                             if ((pos = strchr (ColBut->text1, '&')) &&
                                ((UCHAR) toupper (*(pos + 1)) == taste))
                             {
                                   if (keyhwnd != MainButton.mask[i].feldid)
                                   {
                                                return FALSE;
                                   }
                                   keypressed = 0;
                                   keyhwnd = MainButton.mask[i].feldid;
                                   SendMessage (keyhwnd,
                                                WM_LBUTTONUP,
                                                0l,
                                                0l);
                                   keyhwnd = NULL;
                                   return TRUE;
                            }
                         }
                    }

                    for (i = 0; i < MainButton2.fieldanz; i ++)
                    {
                         ColBut = (ColButton *) MainButton2.mask[i].feld;
                         if (ColBut->text1)
                         {
                              if ((pos = strchr (ColBut->text1, '&')) &&
                                 ((UCHAR) toupper (*(pos + 1)) == taste))
                              {
                                   if (keyhwnd != MainButton2.mask[i].feldid)
                                   {
                                                return FALSE;
                                   }
                                   keypressed = 0;
                                   keyhwnd = MainButton2.mask[i].feldid;
                                   SendMessage (keyhwnd,
                                                WM_LBUTTONUP,
                                                0l,
                                                0l);
                                   keyhwnd = NULL;
                                   return TRUE;
                            }
                         }
                      }
             }
         }
         return FALSE;
}


int     ProcessMessages(void)
{
       MSG msg;

        while (GetMessage (&msg, NULL, 0, 0))
        {
              if (IsMouseMessage (&msg));
              else if (IsHotKey (&msg));
              else if (IsKeyPress (&msg));
              else
              {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
              }
        }
        DestroyFonts ();
	    SetForeground ();
		closedbase ();
        return msg.wParam;
}


LONG FAR PASCAL WndProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        PAINTSTRUCT ps;
		HDC hdc;

        switch(msg)
        {
              case WM_PAINT :
                    if (hWnd == BackWindow2)
                    {
                             if (FitWindow == 0)
                             {
                                        CreateFitWindow ();
                             }
                             display_form (BackWindow2,&MainButton, 0, 0);
                    }
                    else if (hWnd == BackWindow3)
                    {
                             if (MainButton2.mask[0].feldid == 0)
                             {
                                  display_form (BackWindow3,&MainButton2, 0, 0);
                             }
                    }
                    else if (hWnd == FitWindow)
                    {
                           hdc = BeginPaint (hWnd, &ps);
                           paintlogo (hdc, fitbmp, 0, 0, SRCCOPY);
                           EndPaint (hWnd, &ps);
                    }
                    break;
              case WM_KEYDOWN :
                         switch (wParam)
                         {
                              case VK_F5 :
                                      func5 ();
                                      return 0;

                         }
                         break;
              case WM_COMMAND :
                    if (LOWORD (wParam) == KEY5)
                    {
                            syskey = KEY5;
                            SendKey (VK_F5);
                            break;
                    }
                    return MenuJob(hWnd,wParam,lParam);
              case WM_DESTROY :
                    if (hWnd == hMainWindow)
                    {
	                          SetForeground ();
	                          closedbase ();
                              ExitProcess (0);
                             return 0;
                    }
                    return 0;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

LONG MenuJob(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
        HMENU hMenu;

        hMenu = GetMenu (hWnd);

        switch (wParam)
        {
              case CM_HROT :
                     logorx = (logorx + 1) % 2;
                     if (logorx)
                     {
                               CheckMenuItem (hMenu, CM_HROT, MF_CHECKED);
                               CheckMenuItem (hMenu, CM_VROT, MF_UNCHECKED);
                               logory = 0;
                     }
                     else
                     {
                               CheckMenuItem (hMenu, CM_HROT, MF_UNCHECKED);
                     }
                     sign = signx = 1;
                     xstretch = ystretch = 0;
                     return 0;
              case CM_VROT :
                     logory = (logory + 1) % 2;
                     if (logory)
                     {
                               CheckMenuItem (hMenu, CM_VROT, MF_CHECKED);
                               CheckMenuItem (hMenu, CM_HROT, MF_UNCHECKED);
                               logorx = 0;
                     }
                     else
                     {
                               CheckMenuItem (hMenu, CM_VROT, MF_UNCHECKED);
                     }
                     sign = signx = 1;
                     xstretch = ystretch = 0;
                     return 0;
              case CM_PROGRAMMENDE :
                     DeleteObject (fitbmp);
                     DestroyWindow (hMainWindow);
                     KillTimer (FitWindow, 1);
                     ReleaseDC (FitWindow, fithdc);
                     PostQuitMessage(0);
                     return 0;
        }
        return (0);
}

LONG FAR PASCAL UhrProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        switch(msg)
        {
              case WM_TIMER :
                    switch (wParam)
                    {
                        case 1 :
                           sysdate (datum);
                           systime (zeit);
                           sprintf (datumzeit, "%s %s", datum, zeit);
                           InvalidateRect (UhrWindow, NULL, TRUE);
                           UpdateWindow (UhrWindow);
                           return 0;
                    }
                    break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

LONG FAR PASCAL FitLogoProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        PAINTSTRUCT ps;

        switch(msg)
        {
              case WM_TIMER :
                    switch (wParam)
                    {
                        case 1 :
                           printlogo (fitbmp, 0, 0, SRCCOPY);
                           return 0;
                    }
                    break;
              case WM_PAINT :
                    if (IsFhs)
                    {
                                fithdc = BeginPaint (hWnd, &ps);
                                strechbmp (fitbmp, 0, 0, SRCCOPY);
                                EndPaint (hWnd, &ps);
                    }
                    break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}


/** Ab hier Eingabe einer Nummer in einem extra Fenster mit
    numerischen Block fuer die Maus.
**/

static HWND eNumWindow;
static break_num_enter = 0;

mfont EditNumFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                                         RGB (255, 255, 255),
                                         1,
                                         NULL};

/*
mfont TextNumFont = {"Arial", 200, 0, 1, RGB (0, 0, 0),
                                         RGB (255, 255, 255),
                                         1,
                                         NULL};
*/

mfont TextNumFont = {"Arial", 200, 0, 1, BLACKCOL,
                                         LTGRAYCOL,
                                         1,
                                         NULL};

mfont numfieldfont = {"", 150, 0, 1,
                                       RGB (255, 255, 255),
                                       BLUECOL,
                                       1,
                                       NULL};

mfont ctrlfont = {"", 150, 0, 1,
                                       RGB (255, 255, 255),
                                       BLUECOL,
                                       1,
                                       NULL};

mfont stornofont = {"", 150, 0, 1,
                                       RGB (255, 0, 0),
                                       BLUECOL,
                                       1,
                                       NULL};

mfont OKfont = {"", 150, 0, 1,
                                       RGB (255, 255, 255),
                                       BLUECOL,
                                       1,
                                       NULL};

mfont Cafont = {"", 90, 0, 1,
                                       RGB (255, 255, 255),
                                       REDCOL,
                                       1,
                                       NULL};

ColButton Num1      =   {"1", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton Num2      =   {"2", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton Num3      =   {"3", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton Num4      =   {"4", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton Num5      =   {"5", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton Num6      =   {"6", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton Num7      =   {"7", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton Num8      =   {"8", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton Num9      =   {"9", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton Num0      =   {"0", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton NumP     =   {",", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                        14};

ColButton NumM     =   {"-", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};


ColButton NumLeft   =   {"", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         pfeill, -1, -1,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton NumRight   =   {"", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         pfeilr, -1, -1,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton NumDel     =   {"DEL", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         REDCOL,
                         14};

ColButton NumPlus   =   {"+", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton NumMinus   =   {"-", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton NumCls    =   {"C", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         REDCOL,
                         14};
ColButton NumAbbruch  =   {"Abbruch", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         REDCOL,
                         14};

ColButton NumOK       =   {"OK", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         BLACKCOL,
                         GREENCOL,
                         14};

ColButton BuStorno =   {"&Storno", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         REDCOL,
                         14};


static int nbsb  = 85;

static int nbsx   = 5;
int ny    = 10;
/*
static int nbss0 = 80;
static int nbss  = 64;
static int nbsz  = 40;
static int nbsz0 = 64;
*/
static int nbss0 = 64;
static int nbss  = 64;
static int nbsz  = 40;
static int nbsz0 = 64;
static int nAktButton = 0;

static int donum1 ();
static int donum2 ();
static int donum3 ();
static int donum4 ();
static int donum5 ();
static int donum6 ();
static int donum7 ();
static int donum8 ();
static int donum9 ();
static int donum0 ();
static int donump ();
static int donumm ();

static int doleft ();
static int doright ();
static int dodel ();
static int doabbruch ();
static int doOK ();
static int doplus ();
static int dominus ();
static int docls ();

static field _NumField[] = {
(char *) &Num1,          nbss0, nbsz0, ny, nbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum1, 0,
(char *) &Num2,          nbss0, nbsz0, ny, nbsx + 2 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum2, 0,
(char *) &Num3,          nbss0, nbsz0, ny, nbsx + 3 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum3, 0,
(char *) &Num4,          nbss0, nbsz0, ny + 1 * nbsz0, nbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum4, 0,
(char *) &Num5,          nbss0, nbsz0, ny + 1 * nbsz0, nbsx + 2 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum5, 0,
(char *) &Num6,          nbss0, nbsz0, ny + 1 * nbsz0, nbsx + 3 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum6, 0,
(char *) &Num7,          nbss0, nbsz0, ny + 2 * nbsz0, nbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum7, 0,
(char *) &Num8,          nbss0, nbsz0, ny + 2 * nbsz0, nbsx + 2 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum8, 0,
(char *) &Num9,          nbss0, nbsz0, ny + 2 * nbsz0, nbsx + 3 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum9, 0,
(char *) &Num0,          nbss0, nbsz0, ny + 2 * nbsz0, nbsx, 0, "",
                                                           COLBUTTON, 0,
                                                           donum0, 0,
(char *) &NumP,          nbss0, nbsz0, ny + 1 * nbsz0, nbsx, 0, "",
                                                           COLBUTTON, 0,
                                                           donump, 0,
(char *) &NumM,          nbss0, nbsz0, ny, nbsx, 0, "",
                                                           COLBUTTON, 0,
                                                           donumm, 0};

static form NumField = {12, 0, 0, _NumField, 0, 0, 0, 0, &numfieldfont};

/*
static int cbss0 = 80;
static int cbss  = 64;
static int cbsz  = 40;
static int cbsz0 = 64;
*/

static int cbss0 = 64;
static int cbss  = 64;
static int cbsz  = 40;
static int cbsz0 = 64;
static int cbsx = nbsx + 5 * nbss0;

static field _NumControl1[] = {
(char *) &NumLeft,       cbss0, cbsz0, ny, cbsx,             0, "",
                                                           COLBUTTON, 0,
                                                           doleft, 0,
(char *) &NumRight,       cbss0, cbsz0, ny, cbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           doright, 0,
(char *) &NumDel,         cbss0, cbsz0, ny, cbsx            , 0, "",
                                                           COLBUTTON, 0,
                                                           dodel, 0,
(char *) &NumAbbruch,     cbss0, cbsz0, ny, cbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           doabbruch, 0};

static form NumControl1 = {3, 0, 0, _NumControl1, 0, 0, 0, 0, &ctrlfont};

static field _NumControl2[] = {
(char *) &NumPlus,       cbss0, cbsz0, ny, cbsx,             0, "",
                                                           COLBUTTON, 0,
                                                           doplus, 0,
(char *) &NumMinus,       cbss0, cbsz0, ny, cbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           dominus, 0,
(char *) &NumCls,         cbss0, cbsz0, ny, cbsx            , 0, "",
                                                           COLBUTTON, 0,
                                                           docls, 0,
(char *) &NumAbbruch,     cbss0, cbsz0, ny, cbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           doabbruch, 0};

static form NumControl2 = {3, 0, 0, _NumControl2, 0, 0, 0, 0, &ctrlfont};

static form NumControl;


static field _CaControl[] = {
(char *) &NumAbbruch,     cbss0, cbsz0, ny, cbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           doabbruch, 0};
static form CaControl = {1, 0, 0, _CaControl, 0, 0, 0, 0, &Cafont};

static field _OKControl[] = {
(char *) &NumOK,         cbss0 * 2,
                                cbsz0, ny, cbsx,             0, "",
                                                           COLBUTTON, 0,
                                                             doOK, 0};
static form OKControl = {1, 0, 0, _OKControl, 0, 0, 0, 0, &OKfont};


static char editbuff [40];
static double editsum;
static char eaction = ' ';

static field _EditForm [] = {
editbuff,             0, 0, 0, 0, 0, "", EDIT, 0, 0, 0};

static form EditForm = {1, 0, 0, _EditForm, 0, 0, 0, 0, &EditNumFont};


static field _TextForm [] = {
editbuff,             0, 0, 0, 0, 0, "", READONLY, 0, 0, 0};

static form TextForm = {1, 0, 0, _TextForm, 0, 0, 0, 0, &TextNumFont};


static field _StornoForm [] = {
(char *) &BuStorno,       100, 64, 0, 0, 0, "", COLBUTTON, 0, 0, 0};

static form StornoForm = {1, 0, 0, _StornoForm, 0, 0, 0, 0, &stornofont};


void SetStorno (int (*sproc) (void), int BuId)
/**
Procedure fuer Storno Setzen.
**/
{
           StornoForm.mask[0].after = sproc;
           StornoForm.mask[0].BuId  = BuId;
}

int donum1 ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '1', 0l);
        return 1;
}

int donum2 ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '2', 0l);
        return 1;
}

int donum3 ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '3', 0l);
        return 1;
}

int donum4 ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '4', 0l);
        return 1;
}

int donum5 ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '5', 0l);
        return 1;
}

int donum6 ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '6', 0l);
        return 1;
}

int donum7 ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '7', 0l);
        return 1;
}

int donum8 ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '8', 0l);
        return 1;
}

int donum9 ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '9', 0l);
        return 1;
}

int donum0 ()
/**
Aktion bei Button ,
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '0', 0l);
        return 1;
}

int donump ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '.', 0l);
        return 1;
}

int donumm ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '-', 0l);
        return 1;
}


int doleft ()
/**
Aktion bei Button links
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_KEYDOWN,
                    (WPARAM) VK_LEFT, 0l);
        return 1;
}

int doright ()
/**
Aktion bei Button rechts
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_KEYDOWN,
                    (WPARAM) VK_RIGHT, 0l);
        return 1;
}

int dodel ()
/**
Aktion bei Button rechts
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_KEYDOWN,
                    (WPARAM) VK_DELETE, 0l);
        return 1;
}


int docls (void)
/**
EditFenster loeschen.
**/
{
        memset (editbuff, ' ', 39);
        editbuff [39] = (char) 0;
		editsum = (double) 0.0;
		strcpy (editbuff, "");
		CloseControls (&EditForm);
        create_enter_form (eNumWindow, &EditForm, 0, 0);
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
		return 1;
}

int doplus (void)
/**
EditFenster loeschen.
**/
{
	    GetWindowText (EditForm.mask[0].feldid, editbuff, 39);
		if (eaction == '+')
		{
		        editsum += ratod (editbuff);
		}
		else if (eaction == '-')
		{
		        editsum -= ratod (editbuff);
		}
		else
		{
		        editsum = ratod (editbuff);
		}
		eaction = '+';
	    sprintf (editbuff, "%lf", editsum);
		CloseControls (&EditForm);
        create_enter_form (eNumWindow, &EditForm, 0, 0);
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
		return 1;
}

int dominus (void)
/**
EditFenster loeschen.
**/
{
	    GetWindowText (EditForm.mask[0].feldid, editbuff, 39);
		if (eaction == '+')
		{
		        editsum += ratod (editbuff);
		}
		else if (eaction == '-')
		{
		        editsum -= ratod (editbuff);
		}
		else
		{
		        editsum = ratod (editbuff);
		}

		eaction = '-';
	    sprintf (editbuff, "%lf", editsum);
		CloseControls (&EditForm);
        create_enter_form (eNumWindow, &EditForm, 0, 0);
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
		return 1;
}

int dogleich (void)
/**
EditFenster loeschen.
**/
{
	    GetWindowText (EditForm.mask[0].feldid, editbuff, 39);
		if (eaction == '+')
		{
		        editsum += ratod (editbuff);
		}
		else if (eaction == '-')
		{
		        editsum -= ratod (editbuff);
		}
		else
		{
		        editsum = ratod (editbuff);
		}

		eaction = ' ';
	    sprintf (editbuff, "%lf", editsum);
		CloseControls (&EditForm);
        create_enter_form (eNumWindow, &EditForm, 0, 0);
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
		return 1;
}

int doabbruch ()
/**
Aktion bei Button F5
**/
{
        SetFocus (EditForm.mask[0].feldid);
        PostMessage (EditForm.mask[0].feldid, WM_KEYDOWN,
                    (WPARAM) VK_F5, 0l);
        return 1;
}

int doOK ()
/**
Aktion bei Button rechts
**/
{
	    if (CalcOn && eaction != ' ')
		{
			      dogleich ();
				  return 1;
		}
        SetFocus (EditForm.mask[0].feldid);
        PostMessage (EditForm.mask[0].feldid, WM_KEYDOWN,
                    (WPARAM) VK_RETURN, 0l);
        return 1;
}

void NumEnterBreak ()
/**
Endeflag fuer Erfassung setzen.
**/
{
        break_num_enter = 1;
}

HWND IsNumChild (POINT *mousepos)
/**
Test, ob sich der Maus-Cursor in einem KindFensert befinden.
**/
{
        int i;

        for (i = 0; i < NumField.fieldanz; i ++)
        {
                   if (MouseinWindow (NumField.mask[i].feldid,
                                      mousepos))
                   {
                                 return (NumField.mask[i].feldid);
                   }
        }

        for (i = 0; i < NumControl.fieldanz; i ++)
        {
                   if (MouseinWindow (NumControl.mask[i].feldid,
                                      mousepos))
                   {
                                 return (NumControl.mask[i].feldid);
                   }
        }

        for (i = 0; i < OKControl.fieldanz; i ++)
        {
                   if (MouseinWindow (OKControl.mask[i].feldid,
                                      mousepos))
                   {
                                 return (OKControl.mask[i].feldid);
                   }
        }
        for (i = 0; i < CaControl.fieldanz; i ++)
        {
                   if (MouseinWindow (CaControl.mask[i].feldid,
                                      mousepos))
                   {
                                 return (CaControl.mask[i].feldid);
                   }
        }

        for (i = 0; i < EditForm.fieldanz; i ++)
        {
                   if (MouseinWindow (EditForm.mask[i].feldid,
                                      mousepos))
                   {
                                 return (EditForm.mask[i].feldid);
                   }
        }
        if (StornoForm.mask[0].feldid == 0)
        {
                   return FALSE;
        }

        for (i = 0; i < StornoForm.fieldanz; i ++)
        {
                   if (MouseinWindow (StornoForm.mask[i].feldid,
                                      mousepos))
                   {
                                 return (StornoForm.mask[i].feldid);
                   }
        }
        return NULL;
}


LONG FAR PASCAL EntNumProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        POINT mousepos;
        HWND enchild;
        static int SendChild = 0;
        int HasFocus = 1;

        GetCursorPos (&mousepos);

        switch(msg)
        {
              case WM_PAINT :
                    display_form (eNumWindow, &NumField, 0, 0);
                    display_form (eNumWindow, &NumControl, 0, 0);
                    display_form (eNumWindow, &OKControl, 0, 0);
                    display_form (eNumWindow, &TextForm, 0, 0);
                    display_form (eNumWindow, &EditForm, 0, 0);
                    display_form (eNumWindow, &CaControl, 0, 0);

                    if (StornoForm.mask[0].after)
                    {
                              display_form (eNumWindow, &StornoForm, 0, 0);
                    }
                    else if (StornoForm.mask[0].BuId)
                    {
                              display_form (eNumWindow, &StornoForm, 0, 0);
                    }

                    SetFocus (EditForm.mask[0].feldid);
                    break;
              case WM_LBUTTONDOWN :
              case WM_LBUTTONUP :
              case WM_MOUSEMOVE :
                    enchild = IsNumChild (&mousepos);
//                    MouseTest (enchild, msg);
                    if (enchild)
                    {
                              SendMessage (enchild, msg, wParam, lParam);
                              return TRUE;
                    }
                    return TRUE;
              case WM_ACTIVATE :
                    switch (wParam)
                    {
                         case WA_ACTIVE :
                         case WA_CLICKACTIVE :
//                              SetCapture (eNumWindow);
                              break;
                    }
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

LONG FAR PASCAL EntNuProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{

        switch(msg)
        {
              case WM_PAINT :
				    NuBlock.OnPaint (hWnd, msg, wParam, lParam);
					break;

              case WM_LBUTTONDOWN :
              case WM_LBUTTONUP :
              case WM_MOUSEMOVE :
				    NuBlock.OnButton (hWnd, msg, wParam, lParam);
                    return TRUE;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

LONG FAR PASCAL TouchfProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{

        switch(msg)
        {
		      case WM_DIRECT :
				    Touchf.StartDirect ();
				    return 0;
              case WM_PAINT :
/*
				    if (hWnd == NuBlock.eNumWindow)
					{
						NuBlock.OnPaint (hWnd, msg, wParam, lParam);
					}
					else
*/
					{
						Touchf.OnPaint (hWnd, msg, wParam, lParam);
					}
					break;

              case WM_LBUTTONDOWN :
              case WM_LBUTTONUP :
              case WM_MOUSEMOVE :
				    Touchf.OnButton (hWnd, msg, wParam, lParam);
                    return TRUE;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

void RegisterNumEnter (void)
/**
Fenster fuer numerische Eingabe registrieren
**/
{
        static int registered = 0;
        WNDCLASS wc;

        if (registered == 0)
        {
                   wc.style =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                               | CS_OWNDC;
                   wc.cbClsExtra    =  0;
                   wc.cbWndExtra    =  0;
                   wc.hInstance     =  hMainInst;
                   wc.hIcon         =  LoadIcon (hMainInst, "FITICON");
                   wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                   wc.lpszMenuName  =  "";
                   wc.lpfnWndProc   =  EntNumProc;
                   wc.lpszMenuName  =  "";
                   wc.hbrBackground =  CreateSolidBrush (RGB (0, 0, 120));
                   wc.lpszClassName =  "EnterNum";
                   RegisterClass(&wc);
                   registered = 1;
        }
}

int IsNumKey (MSG *msg)
/**
HotKey Testen.
**/
{
         static char keypressed = 0;
         static HWND keyhwnd = 0;
         ColButton *ColBut;
         int taste;
         char *pos;

         switch (msg->message)
         {
	          case EM_SETSEL :
				  break;
              case WM_KEYDOWN :
              {
                     switch (msg->wParam)
                     {
                            case VK_F5 :
                                    NumEnterBreak ();
                                    syskey = KEY5;
                                    return TRUE;
                            case VK_DELETE :
								     docls ();
									 return TRUE;
                            case VK_RETURN :
								    if (CalcOn && eaction != ' ')
									{
										dogleich ();
										return TRUE;
									}
                                    NumEnterBreak ();
                                    GetWindowText (EditForm.mask[0].feldid,
                                                   EditForm.mask [0].feld,
                                                   EditForm.mask [0].length);
                                    syskey = KEYCR;
                                    return TRUE;
                     }
                     taste = (int) msg->wParam;
                     ColBut = (ColButton *) StornoForm.mask[0].feld;
                     if (ColBut->text1)
                     {
                           if ((pos = strchr (ColBut->text1, '&')) &&
                               ((UCHAR) toupper (*(pos + 1)) == taste))
                           {
                                        keypressed = 1;
                                        keyhwnd = StornoForm.mask[0].feldid;
                                        SendMessage (keyhwnd,
                                                WM_LBUTTONDOWN,
                                                0l,
                                                0l);
                                        return TRUE;
                            }
                     }
                     return FALSE;
              }
              case WM_KEYUP :
              {
                         taste = (int) msg->wParam;
                         ColBut = (ColButton *) StornoForm.mask[0].feld;
                         if (ColBut->text1)
                         {
                             if ((pos = strchr (ColBut->text1, '&')) &&
                                ((UCHAR) toupper (*(pos + 1)) == taste))
                             {
                                   if (keyhwnd != StornoForm.mask[0].feldid)
                                   {
                                                return FALSE;
                                   }
                                   keypressed = 0;
                                   keyhwnd = StornoForm.mask[0].feldid;
                                   SendMessage (keyhwnd,
                                                WM_LBUTTONUP,
                                                0l,
                                                0l);
                                   keyhwnd = NULL;
                                   return TRUE;
                            }
                         }
                         return FALSE;
              }
              case WM_CHAR :
              {
				     switch (msg->wParam)
					 {
					       case '+' :
								     doplus ();
									 return TRUE;
						   case '-' :
								     dominus ();
									 return TRUE;
					 }
                     SendMessage (EditForm.mask[0].feldid,
                                   msg->message,
                                   msg->wParam,
                                   msg->lParam);
                                   return TRUE;
               }
          }

          return FALSE;
}


void EnterNumBox (HWND hWnd, char *text, char *nummer, int nlen, char *pic)
/**
Fenster fuer numerische Eingabe oeffnen.
**/
{
        RECT rect;
        MSG msg;
        TEXTMETRIC tm;
        HFONT hFont;
        HDC hdc;
        int x, y, cx, cy;
        int i, j;
        int tlen;
        int elen, epos;
        int eheight;
        HCURSOR oldcursor;
        static int BitMapOK = 0;
        HWND shListBox;
        HWND slbox;
        struct LMENUE smenue;
		static BOOL NumKorr = FALSE;

//		NumKorr = FALSE; // TESTTESET
		if (NumKorr == FALSE)
		{
			      NumKorr = TRUE;
				  nbsz0 = (int) (double) ((double) nbsz0 * scrfy);
				  nbss0 = (int) (double) ((double) nbss0 * scrfx);
		          KorrMainButton (&NumField, scrfx, scrfy);
		          KorrMainButton (&NumControl1, scrfx, scrfy);
		          KorrMainButton (&NumControl2, scrfx, scrfy);
		          KorrMainButton (&StornoForm, scrfx, scrfy);
		          KorrMainButton (&OKControl, scrfx, scrfy);
		          KorrMainButton (&CaControl, scrfx, scrfy);
				  ctrlfont.FontHeight = (int) (double)
					  ((double) ctrlfont.FontHeight * scrfy);
				  ctrlfont.FontWidth = (int) (double)
					  ((double) ctrlfont.FontWidth * scrfx);
		}

		if (CalcOn)
		{
			memcpy (&NumControl, &NumControl2, sizeof (form));
			editsum = (double) 0.0;
			eaction = ' ';
		}
		else
		{
			memcpy (&NumControl, &NumControl1, sizeof (form));
		}

        GetMenue (&smenue);
        MenTextSave (&smenue);
        shListBox = GethListBox ();
        slbox = Getlbox ();
        tlen = strlen (text);

        if (BitMapOK == 0)
        {
                 NumLeft.bmp = LoadBitmap (hMainInst, "pfeill");
                 NumRight.bmp = LoadBitmap (hMainInst, "pfeilr");
                 NumPlus.bmp  = LoadBitmap (hMainInst, "plus");
                 NumMinus.bmp = LoadBitmap (hMainInst, "minus");
                 NumCls.bmp = LoadBitmap (hMainInst, "clear");
                 BitMapOK = 1;
        }

        RegisterNumEnter ();
        GetWindowRect (LogoWindow, &rect);
        x = rect.left;
        y = (int) (double) ((double) rect.bottom - (3 * nbsz0) - (46 * scrfy));
        cx = rect.right - rect.left;
        cy = rect.bottom - y;

        ny = (int) (double) ((double) cy - (3 * nbsz0) - (30 * scrfy));
        for (i = 0,j = 0; j < NumField.fieldanz - 3; i ++,j += 3)
        {
                     NumField.mask[j].pos[0] = ny + i * nbsz0;
                     NumField.mask[j + 1].pos[0] = ny + i * nbsz0;
                     NumField.mask[j + 2].pos[0] = ny + i * nbsz0;
        }
        NumField.mask[j].pos[0] = ny + 2 * nbsz0;
        NumField.mask[j + 1].pos[0] = ny + 1 * nbsz0;
        NumField.mask[j + 2].pos[0] = ny;


        OKControl.mask[0].pos[0]  = ny;

        NumControl.mask[0].pos[0] = ny + 1 * nbsz0;
        NumControl.mask[1].pos[0] = ny + 1 * nbsz0;
        NumControl.mask[2].pos[0] = ny + 2 * nbsz0;
        NumControl.mask[3].pos[0] = ny + 2 * nbsz0;
        CaControl.mask[0].pos[0]  = ny + 2 * nbsz0;

        cbsx = cx - 2 * nbss0 - 5;
        OKControl.mask[0].pos[1]  = cbsx;

        NumControl.mask[0].pos[1] = cbsx;
        NumControl.mask[1].pos[1] = cbsx + nbss0;
        NumControl.mask[2].pos[1] = cbsx;
        NumControl.mask[3].pos[1] = cbsx + nbss0;
        CaControl.mask[0].pos[1] = cbsx + nbss0;

        epos = NumControl.mask[0].pos[1] - (NumField.mask[2].pos[1] + nbss0);
        EditNumFont.FontHeight = 200;
        while (TRUE)
        {
                 spezfont (&EditNumFont);
                 hFont = SetWindowFont (hMainWindow);
                 hdc = GetDC (hMainWindow);
                 SelectObject (hdc, hFont);
                 GetTextMetrics (hdc, &tm);
                 DeleteObject (hFont);
                 ReleaseDC (hMainWindow, hdc);

                 elen = tm.tmAveCharWidth * nlen;
                 if (elen < epos) break;
                 EditNumFont.FontHeight -= 20;
                 if (EditNumFont.FontHeight <= 80) break;
        }

        TextNumFont.FontHeight = 200;

        epos = max (0, (epos - elen) / 2);
        epos = NumField.mask[2].pos[1] + nbss0 + epos;
        eheight = tm.tmHeight;
        eheight += eheight / 3;
        EditForm.mask[0].length = elen;
        EditForm.mask[0].rows   = eheight;
        EditForm.mask[0].pos[1] = epos;
        EditForm.mask[0].pos[0] = ny;

        epos = NumControl.mask[0].pos[1] - (NumField.mask[2].pos[1] + nbss0);
        elen = StornoForm.mask[0].length;
        epos = max (0, (epos - elen) / 2);
        epos = NumField.mask[2].pos[1] + nbss0 + epos;
        StornoForm.mask[0].pos[1] = epos;
        StornoForm.mask[0].pos[0] = cy - 30 - StornoForm.mask[0].rows;

        if (pic)
        {
                       EditForm.mask[0].picture = pic;
        }

        spezfont (&TextNumFont);
        hFont = SetWindowFont (hMainWindow);
        hdc = GetDC (hMainWindow);
        SelectObject (hdc, hFont);
        GetTextMetrics (hdc, &tm);
        DeleteObject (hFont);
        ReleaseDC (hMainWindow, hdc);

        elen = tm.tmAveCharWidth * strlen (text);
        epos = NumControl.mask[0].pos[1] - (NumField.mask[2].pos[1] + nbss0);
        epos = max (0, (epos - elen) / 2);
        epos = NumField.mask[2].pos[1] + nbss0 + epos;
        eheight = tm.tmHeight;
        TextForm.mask[0].length = elen;
        TextForm.mask[0].rows   = eheight;
        TextForm.mask[0].pos[1] = epos;
        TextForm.mask[0].pos[0] = (short)
                            EditForm.mask[0].pos[0] + 2 * (short) tm.tmHeight;
        TextForm.mask[0].feld = text;

        while (TRUE)
        {
                 spezfont (&TextNumFont);
                 hFont = SetWindowFont (hMainWindow);
                 hdc = GetDC (hMainWindow);
                 SelectObject (hdc, hFont);
                 GetTextMetrics (hdc, &tm);
                 DeleteObject (hFont);
                 ReleaseDC (hMainWindow, hdc);

                 elen = tm.tmAveCharWidth * tlen;
                 if (elen < epos) break;
                 TextNumFont.FontHeight -= 20;
                 if (TextNumFont.FontHeight <= 80) break;
        }

        spezfont (&TextNumFont);
        hFont = SetWindowFont (hMainWindow);
        hdc = GetDC (hMainWindow);
        SelectObject (hdc, hFont);
        GetTextMetrics (hdc, &tm);
        DeleteObject (hFont);
        ReleaseDC (hMainWindow, hdc);
        elen = tm.tmAveCharWidth * strlen (text);

        TextNumFont.FontHeight -= 20;

        spezfont (&TextNumFont);
        hFont = SetWindowFont (hMainWindow);
        hdc = GetDC (hMainWindow);
        SelectObject (hdc, hFont);
        GetTextMetrics (hdc, &tm);
        DeleteObject (hFont);
        ReleaseDC (hMainWindow, hdc);
        eheight = tm.tmHeight + tm.tmHeight / 3;
        epos = NumControl.mask[0].pos[1] - (NumField.mask[2].pos[1] + nbss0);
        epos = max (0, (epos - elen) / 2);
        epos = NumField.mask[2].pos[1] + nbss0 + epos;
        TextForm.mask[0].length = elen;
        TextForm.mask[0].rows   = eheight;
        TextForm.mask[0].pos[1] = epos;

		if (EditForm.font->hFont)
		{
			     DeleteObject (EditForm.font->hFont);
		         EditForm.font->hFont = NULL;
		}
		if (TextForm.font->hFont)
		{
		        DeleteObject (TextForm.font->hFont);
		        TextForm.font->hFont = NULL;
		}

        memset (editbuff, ' ', 39);
        editbuff [39] = (char) 0;
        memcpy (editbuff, nummer, strlen (nummer));

        eNumWindow  = CreateWindowEx (
                              0,
                              "EnterNum",
                              "",
                              WS_POPUP | WS_CAPTION,
                              x, y,
                              cx, cy,
                              // hMainWindow,
                              hWnd,
                              NULL,
                              hMainInst,
                              NULL);

        create_enter_form (eNumWindow, &EditForm, 0, 0);
        ShowWindow (eNumWindow, SW_SHOW);
        UpdateWindow (eNumWindow);
        SetFocus (EditForm.mask[0].feldid);

        EnableWindows (hWnd, FALSE);
        oldcursor = SetCursor (LoadCursor (NULL, IDC_ARROW));

        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));

        NumEnterWindow = EditForm.mask[0].feldid;
//        SetCapture (eNumWindow);

        NumEnterAktiv = 1;
        break_num_enter = 0;
        while (GetMessage (&msg, NULL, 0, 0))
        {
              if (IsNumKey (&msg));
              else
              {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
              }
              if (break_num_enter) break;
        }
        display_form (eNumWindow, &EditForm, 0, 0);
        SetActiveWindow (hMainWindow);
        EnableWindows (hWnd, TRUE);
        CloseControls (&NumField);
        CloseControls (&NumControl);
        CloseControls (&OKControl);
        CloseControls (&CaControl);
        CloseControls (&EditForm);
        CloseControls (&TextForm);
        if (StornoForm.mask[0].after)
        {
                    CloseControls (&StornoForm);
                    StornoForm.mask[0].after = NULL;
                    StornoForm.mask[0].BuId = 0;
        }
        else if (StornoForm.mask[0].BuId)
        {
                    CloseControls (&StornoForm);
                    StornoForm.mask[0].after = NULL;
                    StornoForm.mask[0].BuId = 0;
        }
//        ReleaseCapture ();
        DestroyWindow (eNumWindow);
		eNumWindow = NULL;
        NumEnterAktiv = 0;
        strcpy (nummer, editbuff);
        SetCursor (oldcursor);

        SetMenue (&smenue);
        SethListBox (shListBox);
        Setlbox (slbox);
}

void EnterCalcBox (HWND hWnd, char *text, char *nummer, int nlen, char *pic)
/**
Numbox als rechner aufrufen.
**/
{
	     CalcOn = TRUE;
         EnterNumBox (hWnd, text, nummer, nlen, pic);
		 CalcOn = FALSE;
}


/**

  Listenerfassung

**/

// static int lfbsz0 = 36;
static int lfbsz0 = 42;
static int lfbss0 = 73;

//static int SetLAN (void);
static int doinsert (void);
static int domess (void);
static int dodelete (void);
static int Auszeichnen ();
static int HbkInput ();
static int PreisInput ();
static int SatzBearbeiten ();
static int Nachliefern ();
static int Nichtliefern ();
static int SysChoise ();
static int SwitchwKopf ();
void arrtolgr_umlgrp (int);
void LesePos (void) ;
BOOL LeseLspUpd (void);
static int CheckHbk ();

static int sorta (void);
static int sortb (void);
static int sortame (void);
static int sortlme (void);
static int sorts (void);


ColButton BuInsert = {"Neu", -1, 5,
                       "F6", -1, 21,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       RGB (0, 255, 255),
                       BLUECOL,
                       FALSE};


ColButton BuPreise = { "Bearbeiten", -1, 5,
                      "F7", -1, 21,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       RGB (0, 255, 255),
                       BLUECOL,
                       FALSE};


ColButton BuDel   = {"l�schen", -1, 5,
                      "F9", -1, 21,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      RGB (0, 255, 255),
                      BLUECOL,
                      FALSE};

ColButton BuSys =   {"Ger�t", -1, 5,
                      "F11", -1, 21,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      RGB (0, 255, 255),
                      BLUECOL,
                      FALSE};

ColButton BuQsPos =   {"QS Pos", -1, 5,
                       "F2", -1, 21,
                        NULL, 0, 0,
                        NULL, 0, 0,
                        NULL, 0, 0,
                        RGB (0, 255, 255),
                        BLUECOL,
                        TRUE};

ColButton BuMess =   {"Messwerte", -1, 5,
                       "F3", -1, 21,
                        NULL, 0, 0,
                        NULL, 0, 0,
                        NULL, 0, 0,
                        RGB (0, 255, 255),
                        BLUECOL,
                        TRUE};

ColButton BuKopf =   {"Wiegekopf", -1, 5,
                      "F10", -1, 21,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      RGB (0, 255, 255),
                      BLUECOL,
                      FALSE};

ColButton BuAusZ = {"HBK", -1, 5,
                     "F8", -1, 21,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
//                     BLACKCOL,
//                     YELLOWCOL,
                      RGB (0, 255, 255),
                      BLUECOL,
                     -1};

static field _lKopfform [] =
{
        "Umlagerungsnummer",      0, 0, -1, 5, 0, "",     DISPLAYONLY, 0, 0, 0,
        auftrag_nr,     0, 0, -1, 140, 0, "%8d", DISPLAYONLY, 0, 0, 0,
		"Datum",          0, 0, -1, 120, 0, "",    DISPLAYONLY, 0, 0, 0,
		ein_dat,            0, 0, -1, 170, 0, "",    DISPLAYONLY, 0, 0, 0,
};

static form lKopfform = {4, 0, 0, _lKopfform, 0, 0, 0, 0, &lKopfFont};

field _lFussform[] = {
(char *) &BuPreise,      lfbss0, lfbsz0,-1, 10 + 0 * (15 + lfbss0),   0, "", COLBUTTON, 0,
                                                           PreisInput, 0,
(char *) &BuInsert,      lfbss0, lfbsz0,-1, 10 + 4* (15 + lfbss0),     0, "", COLBUTTON, 0,
                                                           doinsert,  0,
(char *) &BuAusZ,        lfbss0, lfbsz0,-1, 10 + 1 * (15 + lfbss0),    0, "", COLBUTTON, 0,
                                                           HbkInput, 0,
(char *) &BuDel,         lfbss0, lfbsz0,-1, 10 + 2 * (15 + lfbss0),   0, "", COLBUTTON, 0,
                                                           dodelete, 0,
(char *) &BuMess,        lfbss0, lfbsz0,-1, 10 + 3 * (15 + lfbss0),   0, "", REMOVED, 0,
                                                           domess, 0,
(char *) &BuQsPos,       lfbss0, lfbsz0,-1, 10 + 5 * (15 + lfbss0),   0, "", REMOVED, 0,
                                                           doQsPos, 0,
(char *) &BuSys,         lfbss0, lfbsz0,-1, 40 + 6 * (15 + lfbss0),   0, "", COLBUTTON, 0,
                                                           SysChoise, 0,
(char *) &BuKopf,        lfbss0, lfbsz0,-1, 40 + 7 * (15 + lfbss0),   0, "", COLBUTTON, 0,
                                                           SwitchwKopf, 0,
};

form lFussform = {8, 0, 0, _lFussform, 0, 0, 0, 0, &lFussFont};



static field _testform [] =
{
        aufps.a,           11, 1, 0, 0, 0, "%11.0f", EDIT, 0, 0, 0,
        aufps.a_bz1,       20, 1, 0, 13,0, "",       READONLY, 0, 0, 0,
        aufps.lief_me,    8, 1, 0, 35, 0, "%8.3f", EDIT, 0, 0, 0,
		aufps.ls_charge,    21, 1, 0, 45, 0, "",      READONLY, 0, 0, 0,
};

static form testform = { 4, 0, 4,_testform, 0, CheckHbk, 0, 0, &ListFont};



static field _testub [] = {
"Artikel-Nr",           12, 1, 0, 0, 0, "", BUTTON, 0, sorta,  106,
"Bezeichnung",          22, 1, 0,12, 0, "", BUTTON, 0, sortb,  102,
"Menge",              11, 1, 0,34, 0, "", BUTTON, 0, sortame,103,
"Charge",              21, 1, 0,45, 0, "", BUTTON, 0, sortlme,104,
};

static form testub = { 4, 0, 0,_testub, 0, 0, 0, 0, &ListFont};

static field _testvl [] =
{      "1",                  1,  0, 0, 12, 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 34 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 45 , 0, "", NORMAL, 0, 0, 0,
};


static form testvl = {3, 0, 0,_testvl, 0, 0, 0, 0, &ListFont};

static int asort = 1;
static int bsort = 1;
static int amesort = 1;
static int lmesort = 1;
static int ssort = 1;


static int sorta0 (const void *elem1, const void *elem2)
{
	        struct AUFP_S *el1;
	        struct AUFP_S *el2;

			el1 = (struct AUFP_S *) elem1;
			el2 = (struct AUFP_S *) elem2;
	        return ((int) (ratod(el1->a) - ratod (el2->a)) * asort);
}


static int sorta (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufparr, aufpanz, sizeof (struct AUFP_S),
				   sorta0);
			asort *= -1;
            ActivateColButton (eFussWindow, &lFussform, 2, -1, 1);
            ShowNewElist ((char *) aufparr,
                             aufpanz,
                            (int) sizeof (struct AUFP_S));
			return 0;
}


static int sortb0 (const void *elem1, const void *elem2)
{
	        struct AUFP_S *el1;
	        struct AUFP_S *el2;

			el1 = (struct AUFP_S *) elem1;
			el2 = (struct AUFP_S *) elem2;
	        return ((int) (strcmp (el1->a_bz1,el2->a_bz1)) * bsort);
}


static int sortb (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufparr, aufpanz, sizeof (struct AUFP_S),
				   sortb0);
			bsort *= -1;
            ActivateColButton (eFussWindow, &lFussform, 2, -1, 1);
            ShowNewElist ((char *) aufparr,
                             aufpanz,
                            (int) sizeof (struct AUFP_S));
			return 0;
}


static int sortame0 (const void *elem1, const void *elem2)
{
	        struct AUFP_S *el1;
	        struct AUFP_S *el2;

			el1 = (struct AUFP_S *) elem1;
			el2 = (struct AUFP_S *) elem2;
			if (ratod (el1->auf_me) > ratod (el2->auf_me))
			{
				          return (1 * amesort);
            }
			if (ratod (el1->auf_me) < ratod (el2->auf_me))
			{
				          return (-1 * amesort);
            }
			return 0;
}

static int sortame (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufparr, aufpanz, sizeof (struct AUFP_S),
				   sortame0);
			amesort *= -1;
            ActivateColButton (eFussWindow, &lFussform, 2, -1, 1);
            ShowNewElist ((char *) aufparr,
                             aufpanz,
                            (int) sizeof (struct AUFP_S));
			return 0;
}

static int sortlme0 (const void *elem1, const void *elem2)
{
	        struct AUFP_S *el1;
	        struct AUFP_S *el2;

			el1 = (struct AUFP_S *) elem1;
			el2 = (struct AUFP_S *) elem2;
			if (ratod (el1->lief_me) > ratod (el2->lief_me))
			{
				          return (1 * lmesort);
            }
			if (ratod (el1->lief_me) < ratod (el2->lief_me))
			{
				          return (-1 * lmesort);
            }
			return 0;
}


static int sortlme (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufparr, aufpanz, sizeof (struct AUFP_S),
				   sortlme0);
			lmesort *= -1;
            ActivateColButton (eFussWindow, &lFussform, 2, -1, 1);
            ShowNewElist ((char *) aufparr,
                             aufpanz,
                            (int) sizeof (struct AUFP_S));
			return 0;
}

static int sorts0 (const void *elem1, const void *elem2)
{
	        struct AUFP_S *el1;
	        struct AUFP_S *el2;

			el1 = (struct AUFP_S *) elem1;
			el2 = (struct AUFP_S *) elem2;
	        return ((atoi (el1->s) - atoi (el2->s)) * ssort);
}


static int sorts (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufparr, aufpanz, sizeof (struct AUFP_S),
				   sorts0);
			ssort *= -1;
            ActivateColButton (eFussWindow, &lFussform, 2, -1, 1);
            ShowNewElist ((char *) aufparr,
                             aufpanz,
                            (int) sizeof (struct AUFP_S));
			return 0;
}


BOOL IsAuszArtikel (void)
/**
Feld pr_ausz pruefen.
**/
{
            char pr_ausz [2];

            if (lese_a_bas (ratod (aufps.a)) != 0)
            {
                          return FALSE;
            }
            strcpy (pr_ausz, "N");
            switch (_a_bas.a_typ)
            {
                   case 1 :
                         strcpy (a_hndw.pr_ausz, "N");
                         hndw_class.lese_a_hndw (_a_bas.a);
                         strcpy (pr_ausz, a_hndw.pr_ausz);
                         break;
                   case 2 :
                         strcpy (a_eig.pr_ausz, "N");
                         hndw_class.lese_a_eig (_a_bas.a);
                         strcpy (pr_ausz, a_eig.pr_ausz);
                         a_hndw.tara = a_eig.tara;
                         break;
                   case 3 :
                         strcpy (a_eig_div.pr_ausz, "N");
                         hndw_class.lese_a_eig_div (_a_bas.a);
                         strcpy (pr_ausz, a_eig_div.pr_ausz);
                         a_hndw.tara = a_eig_div.tara;
                         break;
                   default :
                         return FALSE;
            }
            if (pr_ausz[0] == 'N')
            {
                    return FALSE;
            }
            return TRUE;
}

int CheckHbk ()
/**
Pruefen, ob der Artikel ausgezeichnet werden kann.
**/
{
	        lese_a_bas (ratod (aufps.a));
// Default-Halbarkeits-KZ = 'T' Tage  07.08.2002
/*
            if (_a_bas.hbk_kz[0] <= ' ')
            {
                strcpy (_a_bas.hbk_kz, "T");
            }
*/

            if (_a_bas.hbk_kz[0] > ' ')
            {
                         ActivateColButton (eFussWindow, &lFussform, 2, 0, 1);
            }
            else
            {
                         ActivateColButton (eFussWindow, &lFussform, 2, -1, 1);
            }
//			SetLAN ();
            return (0);
}

void GetMeEinh (void)
/**
Mengeneinheit fuer Kunde und Artikel holen.
**/
{
/*
         KEINHEIT keinheit;
         double auf_me, auf_me_vgl;

         einh_class.GetKunEinh (lsk.mdn, lsk.fil,
                                lsk.kun, _a_bas.a, &keinheit);

         strcpy (aufps.auf_me_bz,    keinheit.me_einh_kun_bez);
         strcpy (aufps.lief_me_bz,   keinheit.me_einh_bas_bez);
         sprintf (aufps.me_einh_kun, "%d", keinheit.me_einh_kun);
         sprintf (aufps.me_einh,     "%d", keinheit.me_einh_bas);
         clipped (aufps.auf_me_bz);
         clipped (aufps.lief_me_bz);
         if (keinheit.me_einh_kun == keinheit.me_einh_bas)
         {
                     strcpy (aufps.auf_me_vgl, aufps.auf_me);
         }
         else
         {
                     if (keinheit.inh <= (double) 0.0)
                     {
                                   keinheit.inh = (double) 1.0;
                     }
                     auf_me = (double) ratod (aufps.auf_me);
                     auf_me_vgl = auf_me * keinheit.inh;
                     sprintf (aufps.auf_me_vgl, "%.3lf", auf_me_vgl);
         }
*/
         return;
}

int lese_lief_bzg (char *buffer, double *a)
{
	     int dsqlstatus;

	     lief_bzg.mdn = we_kopf.mdn;
	     lief_bzg.fil = we_kopf.fil;
		 strcpy (lief_bzg.lief, we_kopf.lief);
		 strcpy (lief_bzg.lief_best, buffer);
		 dsqlstatus = LiefBzg.dbreadfirstbest ();
		 while (dsqlstatus == 100)
		 {
			 if (lief_bzg.fil)
			 {
				 lief_bzg.fil = 0;
			 }
			 else if (lief_bzg.mdn)
			 {
				 lief_bzg.mdn = 0;
			 }
			 else
			 {
				 break;
			 }
  		     dsqlstatus = LiefBzg.dbreadfirstbest ();
		 }
		 if (dsqlstatus == 0)
		 {
			 *a = lief_bzg.a;
	         dsqlstatus = lese_a_bas (*a);
		 }
		 else
		 {
			 *a = (double) 0;
		 }
		 return dsqlstatus;
}

int Reada_pr (void)
/**
Artikelpreis aus a_pr holen.
**/
{
    char datum [12];
    short mdn_gr;
    short fil_gr;
    short sa;
    double pr_fil_ek, pr_vk;

    sysdate (datum);
    mdn_gr = fil_gr = 0;

    if (we_kopf.mdn > 0)
    {
        LiefBzg.sqlin ((short *) &we_kopf.mdn, 1, 0);
        LiefBzg.sqlout ((short *) &mdn_gr, 1, 0);
        LiefBzg.sqlcomm ("select mdn_gr from mdn where mdn = ?");
    }
    if (we_kopf.mdn > 0 && we_kopf.fil > 0)
    {
        LiefBzg.sqlin ((short *) &we_kopf.mdn, 1, 0);
        LiefBzg.sqlin ((short *) &we_kopf.fil, 1, 0);
        LiefBzg.sqlout ((short *) &fil_gr, 1, 0);
        LiefBzg.sqlcomm ("select fil_gr from fil where mdn = ? and fil = ?");
    }

    sa = 0;
    pr_fil_ek = pr_vk = 0.0;
    sa = AbPreise.fetch_preis_tag (mdn_gr, we_kopf.mdn, fil_gr, we_kopf.fil, _a_bas.a,
                                   datum, &pr_fil_ek, &pr_vk);
    mdn_class.lese_mdn (Mandant);
    if (atoi (_mdn.waehr_prim) == 2)
    {
        sprintf (aufps.pr_fil_ek_eu, "%4lf", pr_fil_ek);
        sprintf (aufps.pr_fil_ek, "%4lf", pr_fil_ek * _mdn.konversion);
        sprintf (aufps.ls_lad_euro,"%4lf", pr_vk);
        sprintf (aufps.auf_lad_pr,"%4lf", pr_vk * _mdn.konversion);
    }
    else
    {
        sprintf (aufps.pr_fil_ek, "%4lf", pr_fil_ek);
        sprintf (aufps.pr_fil_ek_eu, "%4lf", pr_fil_ek / _mdn.konversion);
        sprintf (aufps.auf_lad_pr,"%4lf", pr_vk);
        sprintf (aufps.ls_lad_euro,"%4lf", pr_vk / _mdn.konversion);
    }
    return 0;
}

void CalcEkBto (void)
{
       pr_ek_bto0 = lief_bzg.pr_ek;
       pr_ek_bto0_euro = lief_bzg.pr_ek_eur;
	   if (lief_bzg.pr_ek > 0.0)
	   {
              pr_ek0  = pr_ek_bto0 - (double) WePreis.GetRabEk
				                                      (we_kopf.mdn, we_kopf.fil,
			                                           we_kopf.lief,
	  						                           1, pr_ek_bto0, _a_bas.me_einh);
	   }
	   else
	   {
			  pr_ek0 = (double) 0.0;
	   }
	   if (lief_bzg.pr_ek_eur > 0.0)
	   {
              pr_ek0_euro  = pr_ek_bto0_euro - (double) WePreis.GetRabEk
				                                      (we_kopf.mdn, we_kopf.fil,
			                                           we_kopf.lief,
	  						                           1, pr_ek_bto0_euro, _a_bas.me_einh);
	   }
	   else
	   {
			  pr_ek0_euro = (double) 0.0;
	   }
}

int ReadPr (int ag, double a)
{
	if (a > double(0))
	{
		DbClass.sqlin ((double *)   &a, 3, 0);
		DbClass.sqlout ((double *) &basispreis, 3, 0);
		dsqlstatus = DbClass.sqlcomm ("select unique(grund_pr) from a_schl "
			                          "where a = ?");
		if (dsqlstatus != 0) basispreis = 0;
	}
	else
	{
		DbClass.sqlin ((long *)   &ag, 2, 0);
		DbClass.sqlout ((double *) &basispreis, 3, 0);
		dsqlstatus = DbClass.sqlcomm ("select unique(grund_pr) from a_schl "
			                          "where ag = ?");
		if (dsqlstatus != 0) basispreis = 0;
		if (dsqlstatus == 0) sprintf (BasisPreis, "%9.2f", basispreis);

	}

    return 0;
}

int PosOK (void)
/**
Pr�ft Positionssatz lgr_umlgrp ob alle N�tigen Daten vorhanden sind
**/
{
	return TRUE;
}

int ArtikelOK (char *buffer)
/**
Artikel-Nummer in Basisdaten suchen.
**/
{
	    double a;

			    a = ratod (buffer);
		        dsqlstatus = lese_a_bas (ratod (buffer));
		        if (dsqlstatus != 0)
		        {
							disp_messG ("Falsche Artikelnummer", 2);
							return FALSE;
				}
		        sprintf (aufps.a, "%11.0lf", _a_bas.a);
		        strcpy (aufps.a_bz1, _a_bas.a_bz1);
		        strcpy (aufps.a_bz2, _a_bas.a_bz2);
				sprintf (aufps.me_einh, "%hd", _a_bas.me_einh);
		        strcpy (aufps.hnd_gew, _a_bas.hnd_gew);
		        sprintf (aufps.ag, "%d", _a_bas.ag);
//		 CheckHbk ();
         display_form (InsWindow, &insform, 0, 0);
         return TRUE;
}


void KorrInsPos (TEXTMETRIC *tm)
/**
Positionen in insform korrigieren.
**/
{
        int i;

        if (posInsOK) return;

        posInsOK = 1;
        for (i = 0; i < insform.fieldanz; i ++)
        {
                    insform.mask[i].length *= (short) tm->tmAveCharWidth;
                    insform.mask[i].pos[1] *= (short) tm->tmAveCharWidth;
                    insform.mask[i].pos[0] *= (short) tm->tmHeight;
        }

        for (i = 0; i < insub.fieldanz; i ++)
        {
                    insub.mask[i].pos[1] *= (short) tm->tmAveCharWidth;
                    insub.mask[i].pos[0] *= (short) tm->tmHeight;
        }
}

void OpenInsert (void)
/**
Fenster fuer Insert-Row oeffnen.
**/
{
        RECT rect;
        RECT frmrect;
        HFONT hFont;
        TEXTMETRIC tm;
        int x, y, cx, cy;

        GetWindowRect (hMainWindow, &rect);
        GetFrmRectEx (&insform, insform.font, &frmrect);

        x = rect.left + 5;
        y = rect.top + 60;
        cx = frmrect.right + 26;
        cy = rect.bottom - rect.top - 200;

        spezfont (&InsFont);
        hFont = SetWindowFont (hMainWindow);
        hdc = GetDC (hMainWindow);
        SelectObject (hdc, hFont);
        GetTextMetrics (hdc, &tm);
        ReleaseDC (hMainWindow, hdc);
        DeleteObject (hFont);

        KorrInsPos (&tm);

        InsWindow = CreateWindow ("ListKopf",
                                    "",
                                    WS_POPUP | WS_VISIBLE | WS_DLGFRAME,
                                    x, y,
                                    cx + 0, 72,
                                    eWindow,
                                    NULL,
                                    hMainInst,
                                    NULL);
}

void DeleteInsert (void)
/**
Insertwindow schliessen.
**/
{
       CloseControls (&insform);
       CloseControls (&insub);
       DestroyWindow (InsWindow);
       InsWindow = NULL;
}

BOOL NewPosi (void)
/**
Positionsnummern neu vergeben und in Datenbank schreiben.
**/
{
	/*
          int i;

          beginwork ();
          if (LeseLspUpd () == FALSE) return FALSE;
          ls_class.delete_lspls (lsk.mdn, lsk.fil, lsk.ls);
          for (i = 0; i < aufpanz_upd; i ++)
          {
                   sprintf (aufparr[i].pnr, "%d", (i + 1) * 10);
                   arrtolgr_umlgrp (i);
                   ls_class.update_lsp (lsp.mdn, lsp.fil, lsp.ls,
                                        lsp.a, lsp.posi);
          }
          commitwork ();
		  */
          return TRUE;

}

BOOL DelLeerPos (void)
/**
Positionsnummern neu vergeben und in Datenbank schreiben.
**/
{
/*
          int i;

          beginwork ();
          if (LeseLspUpd () == FALSE) return FALSE;
          ls_class.delete_lspls (lsk.mdn, lsk.fil, lsk.ls);
          for (i = 0; i < aufpanz_upd; i ++)
          {
                   if (atoi (aufparr[i].s) > 2)
                   {
                         arrtolgr_umlgrp (i);
                         ls_class.update_lsp (lsp.mdn, lsp.fil, lsp.ls,
                                              lsp.a, lsp.posi);
                   }
          }
          commitwork ();
          if (eWindow)
          {
                   LeseWePos ();
                   ShowNewElist ((char *) aufparr,
                                aufpanz,
                                (int) sizeof (struct AUFP_S));
          }
*/
          return TRUE;

}

BOOL SollToIst (void)
/**
Bei nicht bearbeiteten Positionen Ist-Wert auf Soll-Wert setzen.
**/
{
/*
          int i;

          beginwork ();
          if (LeseLspUpd () == FALSE) return FALSE;
          ls_class.delete_lspls (lsk.mdn, lsk.fil, lsk.ls);
          for (i = 0; i < aufpanz_upd; i ++)
          {
                   if (atoi (aufparr[i].s) < 3)
                   {
                       strcpy (aufparr[i].lief_me,
                               aufparr[i].auf_me_vgl);
                       strcpy (aufparr[i].s, "3");
                   }
                   arrtolgr_umlgrp (i);
                   ls_class.update_lsp (lsp.mdn, lsp.fil, lsp.ls,
                                              lsp.a, lsp.posi);
          }
          commitwork ();
          if (eWindow)
          {
                   LeseWePos ();
                   ShowNewElist ((char *) aufparr,
                                aufpanz,
                                (int) sizeof (struct AUFP_S));
          }
*/
          return TRUE;
}

BOOL SetAllPosStat (void)
/**
Bei nicht bearbeiteten Positionen Ist-Wert auf Soll-Wert setzen.
**/
{
/*
          int i;

          beginwork ();
          if (LeseLspUpd () == FALSE) return FALSE;
          ls_class.delete_lspls (lsk.mdn, lsk.fil, lsk.ls);
          for (i = 0; i < aufpanz_upd; i ++)
          {
                   if (atoi (aufparr[i].s) < 3)
                   {
                       strcpy (aufparr[i].s, "3");
                   }
                   arrtolgr_umlgrp (i);
                   ls_class.update_lsp (lsp.mdn, lsp.fil, lsp.ls,
                                              lsp.a, lsp.posi);
          }
          commitwork ();
          if (eWindow)
          {
                   LeseWePos ();
                   ShowNewElist ((char *) aufparr,
                                aufpanz,
                                (int) sizeof (struct AUFP_S));
          }
*/
          return TRUE;
}

long GetNextPosi (int pos)
/**
Naechste gueltige Positionnummer ermitteln.
**/
{
          long ap;
          long posi;

          if (pos > 0)
          {
                strcpy (aufparr[pos].pnr, aufparr[pos - 1].pnr);
          }
          for (pos ++;pos <= aufpanz; pos ++)
          {
                 ap = atol (aufparr[pos].pnr);
                 if ((ap % 10) == 0) break;
          }
          posi = atol (aufparr[pos - 1].pnr) + 1;
          return (posi);
}

void ScrollAufp (int pos)
/**
aufparr ab pos zum Einfuegen scrollen.
**/
{
         int i;

         for (i = aufpanz; i > pos; i --)
         {
                       memcpy (&aufparr [i], &aufparr [i + 1],
                                sizeof (struct AUFP_S));
         }
         aufpanz ++;
}

void ScrollAufpDel (int pos)
/**
aufparr ab pos nach Oben scrollen.
**/
{
         int i;

         for (i = pos; i < aufpanz; i ++)
         {
                       memcpy (&aufparr [i], &aufparr [i + 1],
                                sizeof (struct AUFP_S));
         }
         aufpanz --;
}


int dodelete (void)
/**
Eine Zeile aus Liste loeschen.
**/
{
	     int pos;

		 if (BuDel.aktivate == -1) return (0);
		 if (abfragejnG (eWindow, "Position wirklich l�schen ?", "N") == 0)
		 {
			 return (0);
		 }


         pos = GetListPos ();
         arrtolgr_umlgrp (pos);
         beginwork ();
		 DbClass.sqlin ((long *)   &lgr_umlgrp.int_pos, 2, 0);
		 DbClass.sqlin ((long *)  &lgr_umlgrp.umlgr_nr, 2, 0);
		 DbClass.sqlin ((short *)  &lgr_umlgrp.mdn, 1, 0);
		 DbClass.sqlcomm ("delete from lgr_umlgrp  "
			              "where int_pos = ? "
						  "and umlgr_nr = ? "
						  "and mdn = ? ");
		 Lgr_umlgrpClass.InsBsdBuch(-1,lgr_umlgrp.me, lgr_umlgrp.chargennr);
         commitwork ();
         LesePos ();
         ShowNewElist ((char *) aufparr,
                                aufpanz,
                                (int) sizeof (struct AUFP_S));
		 if (aufpanz == 0)
		 {
            ActivateColButton (BackWindow3,  &MainButton2, 2, -1, 1);
            ActivateColButton (eFussWindow, &lFussform,   0, -1, 1);
            ActivateColButton (eFussWindow, &lFussform,   2, -1, 1);
            ActivateColButton (eFussWindow, &lFussform,   3, -1, 1);
            ActivateColButton (eFussWindow, &lFussform,   4, -1, 1);
            ActivateColButton (eFussWindow, &lFussform,   5, -1, 1);
		 }
         return 0;
}

int domess (void)
/**
Eine Zeile in Liste einfuegen.
**/
{
	     char buffer [10];

         aufpidx = SendMessage  (Getlbox (), LB_GETCURSEL, 0l, 0l);
         memcpy (&aufps, &aufparr[aufpidx], sizeof (struct AUFP_S));
	     EnableWindows (hMainWindow, FALSE);
		 strcpy (buffer, aufps.temp);
         NuBlock.EnterNumBox (eWindow,"  Temperatur  ",
                                         buffer,    5, "%3.1f" ,
	      						 	     EntNuProc);
		 if (syskey == KEY5)
		 {
             EnableWindows (hMainWindow, TRUE);
			 return 0;
		 }
		 strcpy (aufps.temp, buffer);

		 strcpy (buffer, aufps.ph_wert);
         NuBlock.EnterNumBox (eWindow,"  PH-Wert  ",
                                         buffer, 6, "%4.2f" ,
	      						 	     EntNuProc);
		 if (syskey == KEY5)
		 {
             EnableWindows (hMainWindow, TRUE);
			 return 0;
		 }
		 strcpy (aufps.ph_wert, buffer);

         memcpy (&aufparr[aufpidx], &aufps, sizeof (struct AUFP_S));
         EnableWindows (hMainWindow, TRUE);
		 return 0;
}

int doinsert0 (void)
/**
Eine Zeile in Liste einfuegen.
**/
{
         struct AUFP_S aufp;
         char buffer [20];
         int pos;
         long akt_posi;

		 Gewogen = 0;
         OpenInsert ();
         memcpy (&aufps, &aufp_null, sizeof (struct AUFP_S));

/**
	    DbClass.sqlin ((short *) &Mandant, 1, 0);
	    DbClass.sqlin ((long *) &lgr_umlgrk.umlgr_nr, 2, 0);
	    DbClass.sqlout ((long *) &lgr_umlgrp.a, 2, 0);
        dsqlstatus = DbClass.sqlcomm ("select a from lgr_umlgrp where mdn = ? "
						 "and umlgr_nr = ? order by a desc");

*/

         while (TRUE)
         {
                   strcpy (buffer, "0");
                   strcpy (ChargeAuswahl, "0");
                          NuBlock.SetChoise (doartchoise, 0, doChargeChoise, 0,
							                 "Auswahl\nArtikel", "Auswahl\nChargen");
						  EnableWindows (hMainWindow, FALSE);
  	                      EnableWindows (eKopfWindow, FALSE);
                          EnableWindows (eFussWindow, FALSE);
                          EnableWindows (BackWindow2, FALSE);
                          EnableWindows (BackWindow3, FALSE);
                          EnableWindows (eWindow, TRUE);
                          NuBlock.EnterNumBox (eWindow,"  Artikel  ",
                                               buffer, 12,testform.mask[0].picture ,
	      									  EntNuProc);

						  EnableWindows (hMainWindow, FALSE);
  	                      EnableWindows (eKopfWindow, FALSE);
                          EnableWindows (eFussWindow, FALSE);
                          EnableWindows (BackWindow2, FALSE);
                          EnableWindows (BackWindow3, FALSE);
                          EnableWindows (eWindow, TRUE);

/**
						  flgNeu = TRUE;
						  InBearbeitung = TRUE;
                          EnterPreisBox (eWindow, -1, -1);
						  flgNeu = FALSE;
******/
  	                      EnableWindows (eKopfWindow, TRUE);
                          EnableWindows (eFussWindow, TRUE);
                          EnableWindows (BackWindow2, TRUE);
                          EnableWindows (BackWindow3, TRUE);
                          EnableWindows (eWindow, TRUE);
						  EnableWindows (hMainWindow, TRUE);
                   InvalidateRect (eWindow, NULL, TRUE);
                   UpdateWindow (eWindow);
                   SetDblClck (IsDblClck, 0);
				   /*
                   if ((double) ratod (buffer) <= (double) 0.0)
                   {
                              memcpy (&aufps, &aufp, sizeof (struct AUFP_S));
                              DeleteInsert ();
                              return 0;
                   }
				   */
                   if (syskey == KEY5)
                   {
                              memcpy (&aufps, &aufp, sizeof (struct AUFP_S));
                              DeleteInsert ();
                              return 0;
                   }
				   if (strcmp(clipped(ChargeAuswahl),"0") != 0) strcpy(aufps.ls_charge,ChargeAuswahl);		
                   if (ArtikelOK (buffer))
                   {
                              break;
                   }
                   DeleteInsert ();
                   return 0;
         }

		 if (hbk_direct)
		 {
/*
		           strcpy (aufps.hbk_date, "");


		           if (_a_bas.hbk_kz[0] > ' ')
				   {
                           sysdate (buffer);
                           EnterNumBox (eWindow,
                                " Haltbarkeitsdatum ", buffer, 11,
                                   "dd.mm.yyyy");
                           InvalidateRect (eWindow, NULL, TRUE);
                           UpdateWindow (eWindow);
                           if (syskey != KEY5)
						   {
					          strcpy (aufps.hbk_date,buffer);
						   }
				   }
*/
         }


//         if (ohne_preis == 0 && immer_preis)
//         {
//                   EnterPreisBox (eWindow, -1, -1);
//         }

         current_form = &testform;
         DeleteInsert ();
         InvalidateRect (eWindow, NULL, TRUE);
         UpdateWindow (eWindow);
//         pos = GetListPos () + 1;
		 pos = GetListAnz ();
         akt_posi = GetNextPosi (pos);
         sprintf (aufps.pnr, "%ld", akt_posi);
         ScrollAufp (pos);
		 
		 if (strcmp(clipped(ChargeAuswahl),"0") == 0) strcpy (aufps.ls_charge, "");
         memcpy (&aufparr [pos], &aufps, sizeof (struct AUFP_S));
         arrtolgr_umlgrp (pos);
         beginwork ();
         Lgr_umlgrpClass.dbupdate ();
         commitwork ();
         LesePos ();
         SetListPos (pos);
         ShowNewElist ((char *) aufparr,
                                aufpanz,
                                (int) sizeof (struct AUFP_S));
		 if (aufpanz == 1)
		 {
            ActivateColButton (BackWindow3,  &MainButton2, 2, 0, 1);
            ActivateColButton (eFussWindow, &lFussform,   0, 0, 1);
            ActivateColButton (eFussWindow, &lFussform,   2, 0, 1);
            ActivateColButton (eFussWindow, &lFussform,   3, 0, 1);
            ActivateColButton (eFussWindow, &lFussform,   4, 0, 1);
            ActivateColButton (eFussWindow, &lFussform,   5, 0, 1);
		 }
         memcpy (&aufps, &aufparr [pos], sizeof (struct AUFP_S));
         CheckHbk ();
		 if (wieg_neu_direct)
		 {
                      IsDblClck (pos);
		 }
         return 0;
}


int doinsert (void)
{
	    if (BuInsert.aktivate == -1) return (0);
	    while (TRUE)
		{
	             doinsert0 ();
				 if (syskey == KEY5) break;
				 if (wieg_neu_direct != 2) break;
		}
		return 0;
}

 

int endlist (void)
/**
Liste beenden.
**/
{
        break_list ();
        return (1);
}

void arrtolgr_umlgrp (int idx)
/**
Satz aufarr in lgr_umlgrp uebertragen.
**/
{
		lgr_umlgrp.mdn = lgr_umlgrk.mdn;
		lgr_umlgrp.umlgr_nr = lgr_umlgrk.umlgr_nr;
		lgr_umlgrp.dat = lgr_umlgrk.dat;



        lgr_umlgrp.a           = (double) ratod (aufparr[idx].a);
        lgr_umlgrp.int_pos     = atol (aufparr[idx].pnr);
        lgr_umlgrp.me   = (double) ratod (aufparr[idx].lief_me);
		strcpy (lgr_umlgrp.chargennr,aufparr[idx].ls_charge);
}


long GenLText (void)
/**
Text-Nr fuer lsp.lsp_txt genereieren.
**/
{
        int dsqlstatus;
        short mdn;
        short fil;
        long gen_nr;
        extern short sql_mode;

        mdn = Mandant;
        fil = Filiale;

        sql_mode = 1;
        dsqlstatus = nvholid (mdn, fil, "lspt_txt");
        if (dsqlstatus == 100)
        {
              dsqlstatus = nvanmprf (mdn,
                                     fil,
                                     "lspt_txt",
                                     (long) 1,
                                     (long) 999999,
                                     (long) 10,
                                      "");

              if (dsqlstatus == 0)
              {
                          dsqlstatus = nvholid (mdn,
                                                fil,
                                                "lspt_txt");
              }
         }
         gen_nr = auto_nr.nr_nr;
         sql_mode = 0;
         return gen_nr;
}


void TextInsert (void)
/**
Text fuer Nicht- osder Nachliefern einfuegen.
**/
{
/*
       int aufidx;
       long lsp_txt;


       beginwork ();
       if (Lock_WePos () == FALSE)
       {
            commitwork ();
            return;
       }
       aufidx = GetListPos ();
       memcpy (&aufparr [aufidx], &aufps, sizeof (struct AUFP_S));

       lsp_txt = atol (aufparr [aufidx].lsp_txt);
       if (lsp_txt == 0l)
       {
           lsp_txt = GenLText ();
       }
       if (lsp_txt == 0l) return;
       strcpy (aufparr[aufidx].s, "3");
       sprintf (aufparr[aufidx].lsp_txt, "%ld", lsp_txt);
       memcpy (&aufps, &aufparr [aufidx], sizeof (struct AUFP_S));
       lspt.nr = lsp_txt;
       lspt.zei = 10;
       lspt_class.dbupdate ();
       arrtolgr_umlgrp (aufidx);
       Lgr_umlgrpClass.dbupdate ();
       commitwork ();
       ShowNewElist ((char *) aufparr,
                      aufpanz,
                      (int) sizeof (struct AUFP_S));
*/
}

int Nachliefern ()
/**
Status NACHLIEFERN setzen.
**/
{
      ptab_class.lese_ptab ("na_lief_kz", "2");
      TextInsert ();
      return (0);
}


int Nichtliefern ()
/**
Status NICHTLIEFERN setzen.
**/
{
      ptab_class.lese_ptab ("na_lief_kz", "1");
      TextInsert ();
      return (0);
}


int SwitchwKopf ()
{
        static int arganz;
        static char *args[4];
        char kopf [4];

        if (la_zahl < 0) return 0;

        if (la_zahl == 2)
        {
            AktKopf = (AktKopf  + 1) % la_zahl;
            sprintf (kopf, "%d", AktKopf + 1);
            arganz = 4;
            args[0] = "Leer";
            args[1] = "6";
            args[2] = waadir;
            args[3] = (char *) kopf;
            fnbexec (arganz, args);
            print_messG (1, "Wiegkopf %s gew�hlt", kopf);
            return 0;
        }
        SaveMenue ();
        EnableWindow (eWindow, FALSE);
        EnableWindow (eKopfWindow, FALSE);
        EnableWindow (eFussWindow, FALSE);
        AktKopf = AuswahlKopf ();
        EnableWindow (eWindow, TRUE);
        EnableWindow (eKopfWindow, TRUE);
        EnableWindow (eFussWindow, TRUE);
        SetActiveWindow (eWindow);
        AktivWindow = eWindow;
        RestoreMenue ();
        SetDblClck (IsDblClck, 0);

        sprintf (kopf, "%d", AktKopf + 1);
        arganz = 4;
        args[0] = "Leer";
        args[1] = "6";
        args[2] = waadir;
        args[3] = (char *) kopf;
        fnbexec (arganz, args);
        return 0;
}


int SysChoise ()
/**
Auswahl ueber Geraete.
**/
{
        SaveMenue ();
        EnableWindow (eWindow, FALSE);
        EnableWindow (eKopfWindow, FALSE);
        EnableWindow (eFussWindow, FALSE);
        AuswahlSysPeri ();
        EnableWindow (eWindow, TRUE);
        EnableWindow (eKopfWindow, TRUE);
        EnableWindow (eFussWindow, TRUE);
        SetActiveWindow (eWindow);
        AktivWindow = eWindow;
        RestoreMenue ();
        SetDblClck (IsDblClck, 0);
        return (0);
}


int SatzBearbeiten (void)
/**
Satz bearbeiten.
**/
{

        aufpidx = SendMessage  (Getlbox (), LB_GETCURSEL, 0l, 0l) ;
        OpenInsert ();
        EnterPreisBox (eWindow, -1, -1);
		DeleteInsert ();
		if (aufpidx >= 0)
		{
			if (syskey != KEY5)
			{
                memcpy (&aufparr [aufpidx], &aufps, sizeof (struct AUFP_S));
                arrtolgr_umlgrp (aufpidx);
                beginwork ();
                Lgr_umlgrpClass.dbupdate ();
                commitwork ();
                InvalidateRect (eWindow, NULL, TRUE);
                UpdateWindow (eWindow);
                SetDblClck (IsDblClck, 0);
			}
		}
		return 0;
}
int PreisInput (void)
/**
Preise eingeben.
**/
{

	    Gewogen = 1;
        aufpidx = SendMessage  (Getlbox (), LB_GETCURSEL, 0l, 0l) ;
        memcpy (&aufps, &aufparr[aufpidx], sizeof (struct AUFP_S));
        OpenInsert ();
        EnterPreisBox (eWindow, -1, -1);
//		dovk();
		DeleteInsert ();
		if (aufpidx >= 0)
		{
			if (syskey != KEY5)
			{
                memcpy (&aufparr [aufpidx], &aufps, sizeof (struct AUFP_S));
                arrtolgr_umlgrp (aufpidx);
                beginwork ();
                Lgr_umlgrpClass.dbupdate ();
                commitwork ();
                InvalidateRect (eWindow, NULL, TRUE);
                UpdateWindow (eWindow);
                SetDblClck (IsDblClck, 0);
			}
		}
		InBearbeitung = TRUE;
//		func6();

        IsDblClck (aufpidx);
		Gewogen = 0;
		return 0;
}

int HbkInput (void)
/**
Haltbarkeitsdatum eingeben.
**/
{
	    char buffer [12];

//        aufpidx = SendMessage  (Getlbox (), LB_GETCURSEL, 0l, 0l) - 1;
        aufpidx = SendMessage  (Getlbox (), LB_GETCURSEL, 0l, 0l);

		if (dasc_to_long (aufps.hbk_date) > 0l)
		{
			       strcpy (buffer, aufps.hbk_date);
		}
		else
		{
                   sysdate (buffer);
		}
        EnterNumBox (eWindow, " Haltbarkeitsdatum ", buffer, 11, "dd.mm.yyyy");
        InvalidateRect (eWindow, NULL, TRUE);
        UpdateWindow (eWindow);
        if (syskey != KEY5)
        {
	               strcpy (aufps.hbk_date,buffer);
                   memcpy (&aufparr[aufpidx], &aufps, sizeof (struct AUFP_S));
        }
		return 0;
}
int Auszeichnen ()
/**
Reaktion auf Doppelclick in der Liste.
**/
{
        if (BuAusZ.aktivate == -1) return (0);
        WorkModus = AUSZEICHNEN;
        PostMessage (eWindow, WM_KEYDOWN,  (WPARAM) VK_RETURN, 0l);
        return (0);
}


void IsDblClck (int idx)
/**
Reaktion auf Doppelclick in der Liste.
**/
{
	    if (TestOK () == FALSE) return;
        beginwork ();
        if (Lock_Lsp () == FALSE)
        {
            commitwork ();
            return;
        }
        aufpidx = idx;
        memcpy (&aufps, &aufparr[aufpidx], sizeof (struct AUFP_S));
        strcpy (auf_me, aufps.rest);
        if (Wiegen == 1 && Gewogen == 0)
        {
                       ArtikelWiegen (eFussWindow);
        }
        else
        {
//                       ArtikelHand ();
						 if (InBearbeitung == FALSE ) SatzBearbeiten();
						 InBearbeitung = FALSE;
        }
        WorkModus = WIEGEN;
        SetDblClck (IsDblClck, 0);
        current_form = &testform;
        if (ratod (aufps.a) != 0.0)
        {
               memcpy (&aufparr[aufpidx], &aufps, sizeof (struct AUFP_S));
               arrtolgr_umlgrp (idx);
               Lgr_umlgrpClass.dbupdate ();
        }
        ShowNewElist ((char *) aufparr,
                             aufpanz,
                            (int) sizeof (struct AUFP_S));
        commitwork ();
        if (autocursor)
        {
                   PostMessage (eWindow, WM_KEYDOWN, VK_DOWN, 0l);
        }
        return;
}

BOOL TestSysWg (void)
/**
Pruefen, ob die Warengruppe dem aaktiven Geraet zugeordnet ist.
**/
{
        pht_wg.sys = auszeichner;
        pht_wg.wg  = _a_bas.wg;
        if (pht_wg_class.dbreadfirst_wg () != 0)
        {
            return FALSE;
        }
        return TRUE;
}




BOOL lgr_umlgrptoarr (int idx)
/**
Satz aus lsp in aufarr uebertragen.
**/
{

	char ag_bz [25];
	strcpy (ag_bz, "  ");

        memcpy (&aufparr[idx], &aufp_null, sizeof (aufps));
        sprintf (aufparr[idx].a, "%.0lf", lgr_umlgrp.a);
        sprintf (aufparr[idx].lief_me, "%.3lf", lgr_umlgrp.me);
        sprintf (aufparr[idx].pnr, "%ld", lgr_umlgrp.int_pos);
		strcpy (aufparr[idx].ls_charge, lgr_umlgrp.chargennr);
        if (lese_a_bas (lgr_umlgrp.a) == 0) strcpy (aufparr[idx].a_bz1,_a_bas.a_bz1);
        sprintf (aufparr[idx].a_me_einh, "%hd", _a_bas.me_einh);
        strcpy (aufparr[idx].hnd_gew, _a_bas.hnd_gew);

//		setartikel();

        return 0;
}


BOOL LeseLspUpd (void)
/**
Daten aus lsp in aufarr einlesen.
**/
{
        extern short sql_mode;
        aufpanz = 0;
        aufpidx = 0;

        sql_mode = 1;
		lgr_umlgrp.mdn = lgr_umlgrk.mdn;
		lgr_umlgrp.umlgr_nr = lgr_umlgrk.umlgr_nr;
        dsqlstatus = Lgr_umlgrpClass.dbreadfirst_o ("order by int_pos, a");
        while (dsqlstatus == 0)
        {
                   if (Lgr_umlgrpClass.dblock () != 0)
                   {
                       return FALSE;
                   }
                   if (lgr_umlgrptoarr (aufpanz) != 0)
                   {
                       dsqlstatus = Lgr_umlgrpClass.dbread_o ();
                       continue;
                   }

                   aufpanz ++;
                   if (aufpanz == AUFMAX) break;
                   dsqlstatus = Lgr_umlgrpClass.dbread_o ();
        }
        aufpanz_upd = aufpanz;
        return TRUE;
}


void LesePos (void)
/**
Daten aus lgr_umlgrp in aufarr einlesen.
**/
{
        aufpanz = 0;
        aufpidx = 0;
		lgr_umlgrp.mdn = lgr_umlgrk.mdn;
		lgr_umlgrp.umlgr_nr = lgr_umlgrk.umlgr_nr;
        dsqlstatus = Lgr_umlgrpClass.dbreadfirst_o ("order by int_pos");
        while (dsqlstatus == 0)
        {
                   if (lgr_umlgrptoarr (aufpanz) != 0)
                   {
                       dsqlstatus = Lgr_umlgrpClass.dbread_o ();
                       continue;
                   }

                   aufpanz ++;
                   if (aufpanz == AUFMAX) break;
                   dsqlstatus = Lgr_umlgrpClass.dbread_o ();
        }
		if (aufpanz == 0)
		{
//            ActivateColButton (BackWindow3, &MainButton2, 2, -1, 1);
//            ActivateColButton (eFussWindow, &lFussform,   0, -1, 0);
            ActivateColButton (eFussWindow, &lFussform,   2, -1, 0);
            ActivateColButton (eFussWindow, &lFussform,   3, -1, 0);
            ActivateColButton (eFussWindow, &lFussform,   4, -1, 0);
            ActivateColButton (eFussWindow, &lFussform,   5, -1, 0);
		}
		else
		{
//            ActivateColButton (BackWindow3, &MainButton2, 0, 0, 1);
//            ActivateColButton (eFussWindow, &lFussform, 0, 0, 0);
            ActivateColButton (eFussWindow, &lFussform, 2, 0, 0);
            ActivateColButton (eFussWindow, &lFussform, 3, 0, 0);
            ActivateColButton (eFussWindow, &lFussform, 4, 0, 0);
            ActivateColButton (eFussWindow, &lFussform, 5, 0, 0);
		}
}

void RegisterListe (void)
/**
Kopf - und Fuss - Fenster fuer Liste registrieren
**/
{
        static int registered = 0;
        WNDCLASS wc;

        if (registered == 0)
        {
                   wc.style =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                               | CS_OWNDC;
                   wc.cbClsExtra    =  0;
                   wc.cbWndExtra    =  0;
                   wc.hInstance     =  hMainInst;
                   wc.hIcon         =  LoadIcon (hMainInst, "FITICON");
                   wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                   wc.lpszMenuName  =  "";
                   wc.lpfnWndProc   =  ListKopfProc;
                   wc.lpszMenuName  =  "";
                   wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
                   wc.lpszClassName =  "ListKopf";
                   RegisterClass(&wc);
                   registered = 1;
        }
}


LONG FAR PASCAL ListKopfProc(HWND hWnd,UINT msg,
                          WPARAM wParam,LPARAM lParam)
{
        POINT mousepos;

        GetCursorPos (&mousepos);

        switch(msg)
        {
              case WM_PAINT :
                    if (hWnd == eKopfWindow)
                    {
                             SetListEWindow (0);
                             display_form (eKopfWindow, &lKopfform, 0, 0);
//                             SetListEWindow (1);
                    }
                    if (hWnd == eFussWindow)
                    {
                             display_form (eFussWindow, &lFussform, 0, 0);
                    }
                    if (hWnd == InsWindow)
                    {
                             SetListEWindow (0);
                             display_form (InsWindow, &insub, 0, 0);
                             display_form (InsWindow, &insform, 0, 0);
//                             SetListEWindow (1);
                    }
                    break;
              case WM_LBUTTONDOWN :
              case WM_LBUTTONUP :
              case WM_MOUSEMOVE :
                    return TRUE;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}


void EnterListe (void)
/**
Listenerfassung
**/
{
        RECT rect;
        RECT frmrect;
        int x, y, cx, cy;
        HCURSOR oldcursor;
        static long ls_alt = 0;
        HWND phWnd;
        HWND shListBox;
        HWND slbox;
        struct LMENUE smenue;
		TEXTMETRIC tm;


        if (TestQs () == FALSE)
        {
            return;
        }

        memcpy (&aufps, &aufp_null, sizeof (aufps));
        memcpy (&aufparr[0], &aufps, sizeof (aufps));

		commitwork ();
		beginwork ();
		if (lgr_umlgrk.umlgr_nr == 0) return;
		if (lgr_umlgrk.lgr_na == 0) return;

   	        SetTextMetrics (hMainWindow, &tm, &lKopfFont);
            lKopfform.mask[0].feld = "UmlagerungsNr.";
            lKopfform.mask[0].length = 0;
            sprintf((lKopfform.mask[1].feld),"%d", lgr_umlgrk.umlgr_nr);
            lKopfform.mask[1].pos[1] = 200;
			lKopfform.mask[2].pos[1] = (short)  180 + 30 * (short) tm.tmAveCharWidth;
			lKopfform.mask[3].pos[1] = (short)  lKopfform.mask[2].pos[1] + 13 *
				                       (short)  tm.tmAveCharWidth;

        GetMenue (&smenue);
        MenTextSave (&smenue);
        shListBox = GethListBox ();
        slbox = Getlbox ();

        ActivateColButton (eFussWindow, &lFussform, 2, 0, 0);
		/*
		if (ohne_preis)
		{
                  ActivateColButton (eFussWindow, &lFussform, 0, -1, 0);
                  set_fkt (NULL, 7);
		}
		else
		{
                  ActivateColButton (eFussWindow, &lFussform, 0, 0, 0);
                  set_fkt (PreisInput, 7);
		}
		*/

        if (AufKopfWindow)
        {
                   phWnd = AufKopfWindow;
        }
        else
        {
                   phWnd = LogoWindow;
        }
        SetActiveWindow (phWnd);
        SetAktivWindow (phWnd);
        oldcursor = SetCursor (LoadCursor (NULL, IDC_WAIT));
        LesePos ();

        RegisterNumEnter ();
        RegisterListe ();
        GetWindowRect (hMainWindow, &rect);
        GetFrmRectEx (&testform, testform.font, &frmrect);

        x = rect.left + 5;
        y = rect.top + 60;
//        cx = frmrect.right + 15;
//        cy = rect.bottom - rect.top - 200;
        cx = (int) (double) ((double) frmrect.right + 15 * scrfx);
        cy = (int) (double) ((double) rect.bottom - rect.top - 200 * scrfy);


        SetAktivWindow (phWnd);
        set_fkt (doQsPos, 2);
        set_fkt (domess, 3);
        set_fkt (endlist, 5);
        set_fkt (doinsert, 6);
        set_fkt (PreisInput, 7);
        set_fkt (HbkInput, 8);
        set_fkt (dodelete, 9);
        set_fkt (SysChoise, 11);
        set_fkt (SetKomplett, 12);
        SetDblClck (IsDblClck, 0);
        ListeAktiv = 1;
//        SetListEWindow (1);
        SetListFont (TRUE);
        spezfont (testform.font);
        eKopfWindow = CreateWindow ("ListKopf",
                                    "",
                                    WS_POPUP | WS_DLGFRAME,
                                    x, y - 35,
                                    (int) ((double) cx + 24 * scrfx),
									(int) ((double) 36 * scrfy),
                                    phWnd,
                                    NULL,
                                    hMainInst,
                                    NULL);

        eWindow = OpenListWindowEnEx (x, y, cx, cy);

        GetWindowRect (eWindow, &rect);
        eFussWindow = CreateWindow ("ListKopf",
                                    "",
                                    WS_POPUP | WS_DLGFRAME,
                                    x, rect.bottom - 2,
                                    (int) ((double) cx + 24 * scrfx),
									(int) ((double) 50 * scrfy),
                                    phWnd,
                                    NULL,
                                    hMainInst,
                                    NULL);
        ShowWindow (eKopfWindow, SW_SHOWNORMAL);
        ShowWindow (eFussWindow, SW_SHOWNORMAL);
        ElistVl (&testvl);
        ElistUb (&testub);
        ActivateColButton (BackWindow3, &MainButton2, 3, 1, 1);
		if (aufpanz == 0)
		{
                   ActivateColButton (BackWindow3, &MainButton2, 2, -1, 1);
		}
/*
        if (WiegeTyp == STANDARD)
		{
                   ActivateColButton (BackWindow3, &MainButton2, 0, -1, 1);
		}
*/

        ShowElist ((char *) aufparr,
                    aufpanz,
                  (char *) &aufps,
                  (int) sizeof (struct AUFP_S),
                  &testform);
        SetCursor (oldcursor);
        EnterElist (eWindow, (char *) aufparr,
                             aufpanz,
                             (char *) &aufps,
                             (int) sizeof (struct AUFP_S),
                             &testform);
         NewPosi ();
         CloseEWindow (eWindow);
         CloseControls (&lKopfform);
         CloseControls (&lFussform);
         if (testub.mask[0].attribut & BUTTON)
         {
                    CloseControls (&testub);
         }
         DestroyWindow (eFussWindow);
         DestroyWindow (eKopfWindow);
         set_fkt (NULL, 2);
         set_fkt (NULL, 3);
         set_fkt (NULL, 5);
         set_fkt (NULL, 6);
         set_fkt (NULL, 7);
         set_fkt (NULL, 8);
         set_fkt (NULL, 9);
         set_fkt (NULL, 11);
         set_fkt (NULL, 12);
         SetListFont (FALSE);
         ListeAktiv = 0;
         ActivateColButton (BackWindow3, &MainButton2, 3, -1, 1);
         SetDblClck (NULL, 1);
//         if (mit_trans)  commitwork ();
         SetMenue (&smenue);
         SethListBox (shListBox);
         Setlbox (slbox);
         set_fkt (menfunc2, 6);
 		 commitwork ();
}


/** Auswahl ueber Auftraege                                 */

struct AUF_S
{
        char auf [10];
        char kun [10];
        char kun_krz1 [18];
        char lieferdat [12];
        char komm_dat [12];
        char ls_stat [3];
};

struct AUF_S aufs, aufs_null;
struct AUF_S aufsarr [AUFMAX];
int aufanz = 0;
int aufidx = 0;

static int aufsort ();
static int kunsort ();
static int kunnsort ();
static int datsort ();
static int kdatsort ();
static int statsort ();

static field _aufform [] =
{
        aufs.auf,          9, 1, 0, 0, 0, "%8d", READONLY, 0, 0, 0,
        aufs.kun,          9, 1, 0, 10,0, "%8d", READONLY, 0, 0, 0,
        aufs.kun_krz1,    17, 1, 0, 21,0, "",    READONLY, 0, 0, 0,
        aufs.lieferdat,    9, 1, 0, 40,0, "dd.mm.yy", READONLY, 0, 0, 0,
        aufs.komm_dat,     9, 1, 0, 49,0, "dd.mm.yy", READONLY, 0, 0, 0,
        aufs.ls_stat,      2, 1, 0, 58, 0, "%1d", READONLY, 0, 0, 0
};


static form aufform = { 6, 0, 0,_aufform, 0, 0, 0, 0, &ListFont};

/*
static field _aufub [] = {
"Auftrag",            9, 1, 0, 0, 0, "", DISPLAYONLY, 0, 0, 0,
"Kunden-Nr",         10, 1, 0,10, 0, "", DISPLAYONLY, 0, 0, 0,
"Kunden-Name",       17, 1, 0,21, 0, "", DISPLAYONLY, 0, 0, 0,
"Lieferdatum",       11, 1, 0,40, 0, "", DISPLAYONLY, 0, 0, 0,
"S",                  1, 1, 0,57, 0, "", DISPLAYONLY, 0, 0, 0
};
*/

static field _aufub [] = {
"Auftrag",            9, 1, 0, 0, 0, "", BUTTON, 0, aufsort,  102,
"Kunden-Nr",         11, 1, 0, 9, 0, "", BUTTON, 0, kunsort,  103,
"Kunden-Name",       20, 1, 0,20, 0, "", BUTTON, 0, kunnsort, 104,
"L-Datum",            9, 1, 0,40, 0, "", BUTTON, 0, datsort,  105,
"K-Datum",            9, 1, 0,49, 0, "", BUTTON, 0, kdatsort,  105,
"S",                  2, 1, 0,58, 0, "", BUTTON, 0, statsort, 106,
" ",                  4, 1, 0,60, 0, "", BUTTON, 0, 0, 0,
};

static form aufub = { 6, 0, 0,_aufub, 0, 0, 0, 0, &ListFont};

static field _aufvl [] =
{      "1",                  1,  0, 0,  9, 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 20 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 40 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 49 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 58 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 60 , 0, "", NORMAL, 0, 0, 0,
};
static form aufvl = {6, 0, 0,_aufvl, 0, 0, 0, 0, &ListFont};


static int faufsort = 1;
static int fkunsort = 1;
static int fkunnsort = 1;
static int fdatsort = 1;
static int fkdatsort = 1;
static int fstatsort = 1;

static int aufsort0 (const void *elem1, const void *elem2)
{
	        struct AUF_S *el1;
	        struct AUF_S *el2;

		  el1 = (struct AUF_S *) elem1;
		  el2 = (struct AUF_S *) elem2;
	        return ((int) (atol(el1->auf) - atol (el2->auf)) *
				                                  faufsort);
}


static int aufsort (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufsarr, aufanz, sizeof (struct AUF_S),
				   aufsort0);
		  faufsort *= -1;
              ShowNewElist ((char *) aufsarr,
                             aufanz,
                            (int) sizeof (struct AUF_S));
		  return 0;
}

static int kunsort0 (const void *elem1, const void *elem2)
{
	        struct AUF_S *el1;
	        struct AUF_S *el2;

		  el1 = (struct AUF_S *) elem1;
		  el2 = (struct AUF_S *) elem2;
	        return ((int) (atol(el1->kun) - atol (el2->kun)) *
		                                  fkunsort);
}

static int kunsort (void)
/**
Nach Artikelnummer sortieren.
**/
{
	      qsort (aufsarr, aufanz, sizeof (struct AUF_S),
				    kunsort0);
		fkunsort *= -1;
            ShowNewElist ((char *) aufsarr,
                             aufanz,
                            (int) sizeof (struct AUF_S));
		return 0;
}


static int kunnsort0 (const void *elem1, const void *elem2)
{
	        struct AUF_S *el1;
	        struct AUF_S *el2;

		  el1 = (struct AUF_S *) elem1;
		  el2 = (struct AUF_S *) elem2;
	        return ((int) strcmp(el1->kun_krz1, el2->kun_krz1) *
				                                  fkunnsort);
}

static int kunnsort (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufsarr, aufanz, sizeof (struct AUF_S),
				   kunnsort0);
 		  fkunnsort *= -1;
              ShowNewElist ((char *) aufsarr,
                             aufanz,
                            (int) sizeof (struct AUF_S));
		  return 0;
}


static int datsort0 (const void *elem1, const void *elem2)
{
	        struct AUF_S *el1;
	        struct AUF_S *el2;
              long dat1;
              long dat2;

		  el1 = (struct AUF_S *) elem1;
		  el2 = (struct AUF_S *) elem2;
              dat1 = dasc_to_long (el1->lieferdat);
              dat2 = dasc_to_long (el2->lieferdat);
              if (dat1 > dat2)
              {
                            return  (1 * fdatsort);
              }
              else
              if (dat1 < dat2)
              {
                            return  (-1 * fdatsort);
              }
              return 0;
}

static int datsort (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufsarr, aufanz, sizeof (struct AUF_S),
				   datsort0);
		  fdatsort *= -1;
              ShowNewElist ((char *) aufsarr,
                             aufanz,
                            (int) sizeof (struct AUF_S));
		  return 0;
}
static int kdatsort0 (const void *elem1, const void *elem2)
{
	        struct AUF_S *el1;
	        struct AUF_S *el2;
              long dat1;
              long dat2;

		  el1 = (struct AUF_S *) elem1;
		  el2 = (struct AUF_S *) elem2;
              dat1 = dasc_to_long (el1->komm_dat);
              dat2 = dasc_to_long (el2->komm_dat);
              if (dat1 > dat2)
              {
                            return  (1 * fkdatsort);
              }
              else
              if (dat1 < dat2)
              {
                            return  (-1 * fkdatsort);
              }
              return 0;
}

static int kdatsort (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufsarr, aufanz, sizeof (struct AUF_S),
				   datsort0);
		  fkdatsort *= -1;
              ShowNewElist ((char *) aufsarr,
                             aufanz,
                            (int) sizeof (struct AUF_S));
		  return 0;
}


static int statsort0 (const void *elem1, const void *elem2)
{
	        struct AUF_S *el1;
	        struct AUF_S *el2;

		  el1 = (struct AUF_S *) elem1;
		  el2 = (struct AUF_S *) elem2;
	        return ((int) (atoi(el1->ls_stat) - atoi (el2->ls_stat)) *
		                                  fstatsort);
}

static int statsort (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufsarr, aufanz, sizeof (struct AUF_S),
				   statsort0);
		  fstatsort *= -1;
              ShowNewElist ((char *) aufsarr,
                             aufanz,
                            (int) sizeof (struct AUF_S));
		  return 0;
}


void IsAwClck (int idx)
/**
Reaktion auf Doppelclick in der Liste.
**/
{
        aufidx = idx;
return;
}


static int DbAuswahl (short cursor,void (*fillub) (char *),
                      void (*fillvalues) (char *))
/**
Auswahl ueber Auftraege.
**/
{
/*
        RECT rect;
        RECT frmrect;
        int x, y, cx, cy;
        static long ls_alt = 0;
        HCURSOR oldcursor;

        aufanz = 0;
        oldcursor = SetCursor (LoadCursor (NULL, IDC_WAIT));
        while (fetch_scroll (cursor, NEXT) == 0)
        {
                kun_class.lese_kun (1, 0, lsk.kun);
                sprintf (aufsarr[aufanz].auf, "%8ld", lsk.auf);
                sprintf (aufsarr[aufanz].kun, "%8ld",  lsk.kun);
                dlong_to_asc (lsk.lieferdat, aufsarr[aufanz].lieferdat);
                dlong_to_asc (lsk.komm_dat, aufsarr[aufanz].komm_dat);
                strcpy  (aufsarr[aufanz].kun_krz1, kun.kun_krz1);
                sprintf (aufsarr [aufanz].ls_stat, "%1hd", lsk.ls_stat);
                aufanz ++;
                if (aufanz == AUFMAX) break;
        }

        RegisterNumEnter ();
        GetWindowRect (hMainWindow, &rect);
        GetFrmRectEx (&testform, testform.font, &frmrect);

        x = rect.left + 5;
        y =  rect.top + 20;
        cx = (int) (double) ((double) frmrect.right + 15 * scrfx);
        cy = (int) (double) ((double) rect.bottom - rect.top - 120 * scrfy);

        set_fkt (endlist, 5);
        syskey = 1;
        SetDblClck (IsAwClck, 0);
        ListeAktiv = 2;
        SetAktivWindow (hMainWindow);
        SetListFont (TRUE);
        spezfont (testform.font);
        eWindow = OpenListWindowEnEx (x, y, cx, cy);
        ElistVl (&aufvl);
        ElistUb (&aufub);
        ShowElist ((char *) aufsarr,
                    aufanz,
                  (char *) &aufs,
                  (int) sizeof (struct AUF_S),
                  &aufform);
        SetCursor (oldcursor);
        EnterElist (eWindow, (char *) aufsarr,
                             aufanz,
                             (char *) &aufs,
                             (int) sizeof (struct AUF_S),
                             &aufform);
        if (aufub.mask[0].attribut & BUTTON)
        {
                    CloseControls (&aufub);
        }
        CloseEWindow (eWindow);
        set_fkt (NULL, 5);
        SetListFont (FALSE);
        ListeAktiv = 0;
        SetDblClck (NULL, 1);
*/
        return aufidx;
}

/**

  Auftragskopf anzeigen und Eingabefelder waehlen.

**/

mfont AufKopfFontT = {"Courier New", 200, 0, 1, BLACKCOL,
                                                RGB (0, 255, 255),
                                                0,
                                                NULL};

mfont AufKopfFontE = {"Courier New", 200, 0, 1,
                                                RGB (0, 0, 0),
                                                RGB (255, 255, 255),
                                                0,
                                                NULL};


mfont AufKopfFontTD = {"Courier New", 150, 0, 1, BLACKCOL,
                                                RGB (0, 255, 255),
                                                0,
                                                NULL};

mfont AufKopfFontED = {"Courier New", 150, 0, 1,
                                                RGB (0, 0, 0),
                                                RGB (255, 255, 255),
                                                0,
                                                NULL};


static char KundeFiliale [14] = {"Kunden-Nr   :"};
static int ReadAuftrag (void);
static int AufAuswahl (void);
static int BreakAuswahl (void);

static field _AufKopfT[] = {
"Auftrag-Nr  :",     12, 0, 1, 2, 0, "", DISPLAYONLY, 0, 0, 0};

form AufKopfT = {1, 0, 0, _AufKopfT, 0, 0, 0, 0, &AufKopfFontT};


static field _AufKopfTD[] = {
KundeFiliale,        13, 0, 4, 2, 0, "", DISPLAYONLY, 0, 0, 0,
kun_krz1,            16, 0, 4,28, 0, "", DISPLAYONLY,        0, 0, 0,
"Lieferdatum :",     13, 0, 5, 2, 0, "", DISPLAYONLY, 0, 0, 0,
"Komm.-Datum :",     13, 0, 6, 2, 0, "", DISPLAYONLY, 0, 0, 0,
"Lieferzeit  :",     13, 0, 7, 2, 0, "", DISPLAYONLY, 0, 0, 0,
"Status      :",     13, 0, 8, 2, 0, "", DISPLAYONLY, 0, 0, 0,
 ls_stat_txt,        16, 0, 8,28, 0, "", DISPLAYONLY,        0, 0, 0
};

form AufKopfTD = {7,0, 0, _AufKopfTD, 0, 0, 0, 0, &AufKopfFontTD};


static field _AufKopfE[] = {
  auftrag_nr,           9, 0, 1,18, 0, "%8d", EDIT,        0, ReadAuftrag, 0};

form AufKopfE = {1, 0, 0, _AufKopfE, 0, 0, 0, 0, &AufKopfFontE};


static field _AufKopfED[] = {
  kun_nr,              20, 0, 4,18, 0, "%8d", EDIT,        0, 0, 0,
  lieferdatum,         20, 0, 5,18, 0, "dd.mm.yyyy",
                                              EDIT,        0, 0, 0,
  komm_dat,            20, 0, 6,18, 0, "dd.mm.yyyy",
                                              EDIT,        0, 0, 0,
  lieferzeit,          20, 0, 7,18, 0, "",
                                              EDIT,        0, 0, 0,
  ls_stat,             20, 0, 8,18, 0, "",
                                              EDIT,        0, BreakAuswahl, 0
};

form AufKopfED = {5, 0, 0, _AufKopfED, 0, 0, 0, 0, &AufKopfFontED};

static field _AufKopfQuery[] = {
  auftrag_nr,           9, 0, 1,18, 0, "", EDIT,        0, 0, 0,
  kun_nr,               9, 0, 4,18, 0, "", EDIT,        0, 0, 0,
  lieferdatum,         11, 0, 5,18, 0, "",
                                           EDIT,        0, 0, 0,
  komm_dat,            11, 0, 6,18, 0, "",
                                           EDIT,        0, 0, 0,
  lieferzeit,           6, 0, 7,18, 0, "",
                                              EDIT,        0, 0, 0,
  ls_stat,              2, 0, 8,18, 0, "",
                                              EDIT,        0, 0, 0
};

static form AufKopfQuery = {6, 0, 0, _AufKopfQuery, 0, 0, 0, 0, 0};
static char *AufKopfnamen[] = {"auf", "kun", "lieferdat", "komm_dat",
                                     "lieferzeit",
                                     "ls_stat"};

static int break_kopf_enter = 0;
static int aspos = 0;
static int asend = 4;

int BreakAuswahl (void)
/**
Auswahl ueber Auftraege.
**/
{
        break_kopf_enter = 1;
        return 0;
}


int AufAuswahl (void)
/**
Auswahl ueber Auftraege.
**/
{
/*
        SetDbAwSpez (DbAuswahl);
        break_kopf_enter = 1;
        ReadQuery (&AufKopfQuery, AufKopfnamen);
        if (ls_class.Auswahl_lsk_aufQuery () == 0)
        {
                     sprintf (auftrag_nr, "%ld", lsk.auf);
                     if (aufanz)
                     {
                               LockAuf = 0;
                               ReadAuftrag ();
                               LockAuf = 1;
                     }
                     else
                     {
                               InitAufKopf ();
                     }
                     SetDbAwSpez (NULL);
                     return 0;
        }
        InitAufKopf ();
        SetDbAwSpez (NULL);
*/
        return 0;
}

void FillAufForm (void)
/**
Datenbankfelder in Maske uebertragn.
**/
{
/*
             kun_class.lese_kun (1, 0, lsk.kun);
             sprintf (kun_nr, "%ld", lsk.kun);
             dlong_to_asc (lsk.lieferdat, lieferdatum);
             dlong_to_asc (lsk.komm_dat, komm_dat);
             strcpy (lieferzeit, lsk.lieferzeit);
             sprintf (ls_stat, "%hd", lsk.ls_stat);
             strcpy (kun_krz1, kun.kun_krz1);
             ptab_class.lese_ptab ("ls_stat", ls_stat);
             strcpy (ls_stat_txt, ptabn.ptbezk);
*/
}

BOOL Lock_Lgr_umlgrk (void)
/**
Lieferschein sperren.
**/
{
    short sqlm;
    extern short sql_mode;

    sqlm = sql_mode;
    sql_mode = 1;
    dsqlstatus = Lgr_umlgrkClass.dblock ();
    if (dsqlstatus < 0)
    {
        print_messG (2, "Satz ist gesperrt");
        sql_mode = sqlm;
        EinLock = 1;
        ActivateColButton (BackWindow2, &MainButton, 1, -1, 1);
        ActivateColButton (BackWindow2, &MainButton, 2, -1, 1);
        ActivateColButton (BackWindow2, &MainButton, 4, -1, 1);
        return FALSE;
    }
    EinLock = 0;
    ActivateColButton (BackWindow2, &MainButton, 1, 1, 1);
    ActivateColButton (BackWindow2, &MainButton, 4, 1, 1);
    sql_mode = sqlm;
    return TRUE;
}

BOOL Lock_Lsp (void)
/**
Lieferschein sperren.
**/
{
/*
    short sqlm;
    extern short sql_mode;

    sqlm = sql_mode;
    sql_mode = 1;
    dsqlstatus = ls_class.lock_lsp (1, 0, lsp.ls, ratod (aufps.a),
                                                  atol (aufps.pnr));
    if (dsqlstatus < 0)
    {
               print_messG (2, "Position wird von einem anderen "
                               "Benutzer bearbeitet");
               return FALSE;
    }
    sql_mode = sqlm;
*/
    return TRUE;
}


int ReadAuftrag (void)
/**
Lieferschein nach Auftragsnummer lesen.
**/
{
/*
            if (atol (auftrag_nr) <= 0l) return (-1);

            dsqlstatus = ls_class.lese_lsk_auf (1, 0, atol (auftrag_nr));
            if (dsqlstatus)
            {
                   ActivateColButton (BackWindow2, &MainButton, 4, -1, 1);
                   set_fkt (NULL, 6);
                   print_messG (0, "Auftrag %s nicht gefunden", auftrag_nr);
                   return -1;
            }

//            if (Lock_Lsk () == FALSE) return 0;

            ActivateColButton (BackWindow2, &MainButton, 4, 1, 1);
            set_fkt (menfunc2, 6);
            FillAufForm ();
*/
            return 0;
}


void KonvTextPos (mfont *Font1,  mfont *Font2, int *z, int *s)
/**
Textposition von einer Schriftgroesse zur anderen konvertieren.
**/
{
         TEXTMETRIC tm1, tm2;
         HDC hdc;
         HFONT hFont, oldfont;
         int ax, ay;

         spezfont (Font1);
         hFont = SetWindowFont (hMainWindow);
         hdc = GetDC (hMainWindow);
         oldfont = SelectObject (hdc, hFont);
         GetTextMetrics (hdc, &tm1);
         DeleteObject (SelectObject (hdc, oldfont));
         ReleaseDC (hMainWindow, hdc);
         DeleteObject (hFont);

         spezfont (Font2);
         hFont = SetWindowFont (hMainWindow);
         hdc = GetDC (hMainWindow);
         oldfont = SelectObject (hdc, hFont);
         GetTextMetrics (hdc, &tm2);
         DeleteObject (SelectObject (hdc, oldfont));
         ReleaseDC (hMainWindow, hdc);
         DeleteObject (hFont);

         ax = *s;
         ay = *z;

         ax = ax * tm1.tmAveCharWidth / tm2.tmAveCharWidth;
         ay = ay * tm1.tmHeight / tm2.tmHeight;

         *s = ax;
         *z = ay;
}

void KonvTextAbsPos (mfont *Font, int *cy, int *cx)
/**
Absolute Textposition fuer eine Schriftgroesse ermitteln.
**/
{
         TEXTMETRIC tm;
         HDC hdc;
         HFONT hFont, oldfont;

         spezfont (Font);
         hFont = SetWindowFont (hMainWindow);
         hdc = GetDC (hMainWindow);
         oldfont = SelectObject (hdc, hFont);
         GetTextMetrics (hdc, &tm);
         DeleteObject (SelectObject (hdc, oldfont));
         ReleaseDC (hMainWindow, hdc);
         DeleteObject (hFont);
         *cx *= tm.tmAveCharWidth;
         *cy *= tm.tmHeight;
}

void GetTextPos (mfont *Font, int *cy, int *cx)
/**
Zeichenorientierte Textposition fuer eine Schriftgroesse ermitteln.
**/
{
         TEXTMETRIC tm;
         HDC hdc;
         HFONT hFont, oldfont;

         spezfont (Font);
         hFont = SetWindowFont (hMainWindow);
         hdc = GetDC (hMainWindow);
         oldfont = SelectObject (hdc, hFont);
         GetTextMetrics (hdc, &tm);
         DeleteObject (SelectObject (hdc, oldfont));
         ReleaseDC (hMainWindow, hdc);
         DeleteObject (hFont);

         *cx /= tm.tmAveCharWidth;
         *cy /= tm.tmHeight;
}



void FontTextLen (mfont *Font, char *text, int *cx, int *cy)
/**
Laenge und Hoehe eines Textes fuer den gewaehlten Font holen.
**/
{
         TEXTMETRIC tm;
         HDC hdc;
         HFONT hFont, oldfont;
         SIZE size;

         spezfont (Font);
         hFont = SetWindowFont (hMainWindow);
         hdc = GetDC (hMainWindow);
         oldfont = SelectObject (hdc, hFont);
         GetTextMetrics (hdc, &tm);
         GetTextExtentPoint (hdc, text, *cx, &size);
         DeleteObject (SelectObject (hdc, oldfont));
         ReleaseDC (hMainWindow, hdc);
         DeleteObject (hFont);

         *cx = size.cx;
         *cy = tm.tmHeight;
}

void SetAufFocus (int pos)
/**
Focus auf Eingabefelder AuftragKopfsetzen.
**/
{
        switch (pos)
        {
               case 0 :
                      SetFocus (AufKopfE.mask[0].feldid);
                      SendMessage (AufKopfE.mask[0].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
                      break;
               default :
                      SetFocus (AufKopfED.mask[pos - 1].feldid);
                      SendMessage (AufKopfED.mask[pos -1].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
                      break;
        }
}

void PrintKLines ()
/**
Icons am Bildschirm anzeigen.
**/
{
         RECT rect;
         PAINTSTRUCT ps;
         HDC hdc;
         HPEN hPen;
         int x, y;

         GetClientRect (AufKopfWindow, &rect);

         hdc = BeginPaint (AufKopfWindow, &ps);

         hPen = CreatePen (PS_SOLID, 2, BLACKCOL);
         SelectObject (hdc, hPen);
         x = rect.right - 4;
         y = 10;
         MoveToEx (hdc, 2, y, NULL);
         LineTo (hdc, x, y);
         DeleteObject (hPen);

         hPen = CreatePen (PS_SOLID, 2, GRAYCOL);
         SelectObject (hdc, hPen);
         MoveToEx (hdc, 2, y - 1, NULL);
         LineTo (hdc, x, y - 2);
         DeleteObject (hPen);

         hPen = CreatePen (PS_SOLID, 2, BLACKCOL);
         SelectObject (hdc, hPen);
         x = rect.right - 4;
         y = 150;
         MoveToEx (hdc, 2, y, NULL);
         LineTo (hdc, x, y);
         DeleteObject (hPen);

         hPen = CreatePen (PS_SOLID, 2, GRAYCOL);
         SelectObject (hdc, hPen);
         MoveToEx (hdc, 2, y - 1, NULL);
         LineTo (hdc, x, y - 2);
         DeleteObject (hPen);
         EndPaint (WiegWindow, &ps);
         return;

         hPen = CreatePen (PS_SOLID, 2, BLACKCOL);
         SelectObject (hdc, hPen);
         x = rect.right - 4;
         y = 300;
         MoveToEx (hdc, 2, y, NULL);
         LineTo (hdc, x, y);
         DeleteObject (hPen);

         hPen = CreatePen (PS_SOLID, 2, GRAYCOL);
         SelectObject (hdc, hPen);
         MoveToEx (hdc, 2, y - 1, NULL);
         LineTo (hdc, x, y - 2);
         DeleteObject (hPen);

         EndPaint (WiegWindow, &ps);
}

static int NoDisplay;


LONG FAR PASCAL AufKopfProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        switch(msg)
        {

              case WM_PAINT :
				    if (QmAktiv) break;
                    if (LiefEnterAktiv)
                    {
                                DisplayLiefKopf ();
                                PrintKLines ();
                                if (MessWindow == 0)
                                {
                                        LiefFocus ();
                                }
                    }
                    else if (KopfEnterAktiv)
                    {
                                display_form (AufKopfWindow, &AufKopfT, 0, 0);
                                display_form (AufKopfWindow, &AufKopfTD, 0, 0);
                                if (AufKopfE.mask[0].feldid)
                                {
                                      display_form (AufKopfWindow, &AufKopfE, 0, 0);
                                }
                                if (AufKopfE.mask[0].feldid || asend == 0)
                                {
                                      display_form (AufKopfWindow, &AufKopfED, 0, 0);
                                }
                                PrintKLines ();
                                if (MessWindow == 0)
                                {
                                         SetAufFocus (aspos);
                                }
                    }
                    break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}


void RegisterKopfEnter (void)
/**
Fenster fuer Auftragskopf registrieren
**/
{
        static int registered = 0;
        WNDCLASS wc;

        if (registered == 0)
        {
                   wc.style =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                               | CS_OWNDC;
                   wc.cbClsExtra    =  0;
                   wc.cbWndExtra    =  0;
                   wc.hInstance     =  hMainInst;
                   wc.hIcon         =  LoadIcon (hMainInst, "FITICON");
                   wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                   wc.lpszMenuName  =  "";
                   wc.lpfnWndProc   =  AufKopfProc;
                   wc.lpszMenuName  =  "";
                   wc.hbrBackground =  CreateSolidBrush (RGB (0, 255, 255));
                   wc.lpszClassName =  "AufKopf";
                   RegisterClass(&wc);
                   registered = 1;
        }
}

void GetEditText (void)
/**
Text aus Editierfeld holen.
**/
{
        switch (aspos)
        {
                 case 0 :
                    GetWindowText (AufKopfE.mask [0].feldid,
                                   AufKopfE.mask [0].feld,
                                   AufKopfE.mask[0].length);
                    break;
                 case 1 :
                    GetWindowText (AufKopfED.mask [0].feldid,
                                   AufKopfED.mask [0].feld,
                                   AufKopfED.mask[0].length);
                    break;
                 case 2 :
                    GetWindowText (AufKopfED.mask [1].feldid,
                                   AufKopfED.mask [1].feld,
                                   AufKopfED.mask[1].length);
                    break;
                 case 3 :
                    GetWindowText (AufKopfED.mask [2].feldid,
                                   AufKopfED.mask [2].feld,
                                   AufKopfED.mask[2].length);
                    break;
                 case 4 :
                    GetWindowText (AufKopfED.mask [3].feldid,
                                   AufKopfED.mask [3].feld,
                                   AufKopfED.mask[3].length);

                 case 5 :
                    GetWindowText (AufKopfED.mask [4].feldid,
                                   AufKopfED.mask [4].feld,
                                   AufKopfED.mask[4].length);
                    break;
        }
}

int MenKey (int taste)
{
    int i;
    ColButton *ColBut;
    char *pos;

    for (i = 0; i < MainButton.fieldanz; i ++)
    {
           ColBut = (ColButton *) MainButton.mask[i].feld;
           if (ColBut->aktivate < 0) continue;
           if (ColBut->text1)
           {
                 if ((pos = strchr (ColBut->text1, '&')) &&
                    ((UCHAR) toupper (*(pos + 1)) == taste))
                 {
                             if (MainButton.mask[i].after)
                             {
                                 (*MainButton.mask[i].after) ();
                             }
                             return TRUE;
                  }
           }
    }
    return FALSE;
}

int IsKopfMessage (MSG *msg)
/**
HotKey Testen.
**/
{
         POINT mousepos;

         switch (msg->message)
         {
              case WM_KEYDOWN :
              {
                     switch (msg->wParam)
                     {
                            case VK_F5 :
                                    break_kopf_enter = 1;
                                    return TRUE;
                            case VK_F6 :
                                    syskey = KEY6;
                                    testkeys ();
                                    return TRUE;
                            case VK_F12 :
								    if (TestOK () == FALSE) return TRUE;
                                    syskey = KEY12;
                                    testkeys ();
                                    return TRUE;
                            case VK_DOWN :
                            case VK_RETURN :
                                    GetEditText ();
                                    if (asend == 0 && AufKopfE.mask[0].after)
                                    {
                                         if ((*AufKopfE.mask[0].after) () == -1)
                                         {
                                                  return TRUE;
                                         }
                                    }
                                    if (aspos > 0 &&
                                        AufKopfED.mask [aspos - 1].after)
                                    {
                                      (*AufKopfED.mask [aspos - 1].after) ();
                                    }
                                    aspos ++;
                                    if (aspos > asend) aspos = 0;
                                    display_form (AufKopfWindow,
                                                  &AufKopfE, 0, 0);
                                    display_form (AufKopfWindow,
                                                  &AufKopfED, 0, 0);
                                    SetAufFocus (aspos);
                                    return TRUE;
                            case VK_UP :
                                    GetEditText ();
                                    aspos --;
                                    if (aspos < 0) aspos = asend;
                                    display_form (AufKopfWindow,
                                                  &AufKopfE, 0, 0);
                                    display_form (AufKopfWindow,
                                                  &AufKopfED, 0, 0);
                                    SetAufFocus (aspos);
                                    return TRUE;
                            case VK_TAB :
                                    GetEditText ();
                                    if (GetKeyState (VK_SHIFT) < 0)
                                    {
                                            aspos --;
                                            if (aspos < 0) aspos = 4;
                                            SetAufFocus (aspos);
                                     }
                                     else
                                     {
                                            aspos ++;
                                            if (aspos > 4) aspos = 0;
                                            SetAufFocus (aspos);
                                     }
                                     display_form (AufKopfWindow,
                                                 &AufKopfE, 0, 0);
                                     display_form (AufKopfWindow,
                                                 &AufKopfED, 0, 0);
                                      return TRUE;
                            default :
                                    return MenKey (msg->wParam);
                     }
                     break;
              }
              case WM_LBUTTONDOWN :
              {
                    GetCursorPos (&mousepos);

                    if (MouseinWindow (AufKopfE.mask[0].feldid, &mousepos))
                    {
                          EnterNumBox (AufKopfWindow, "  Auftrags-Nr  ",
                                       auftrag_nr, 9, AufKopfE.mask[0].picture);
                          if (asend == 0 && AufKopfE.mask[0].after)
                          {
                                if ((*AufKopfE.mask[0].after) () == -1)
                                {
                                       return TRUE;
                                }
                          }
                          display_form (AufKopfWindow, &AufKopfE, 0, 0);
						  PostMessage (msg->hwnd, WM_KEYDOWN, VK_RETURN, 0l);
                          return TRUE;
                    }
                    else if (MouseinWindow (AufKopfED.mask[0].feldid, &mousepos))
                    {
                          if (asend == 0) return TRUE;
                          EnterNumBox (AufKopfWindow,"  Kunden-Nr  ",
                                          kun_nr, 9, AufKopfED.mask[0].picture);
                          display_form (AufKopfWindow, &AufKopfE, 0, 0);
						  PostMessage (msg->hwnd, WM_KEYDOWN, VK_RETURN, 0l);
                          return TRUE;
                    }
                    else if (MouseinWindow (AufKopfED.mask[1].feldid, &mousepos))
                    {
                         if (asend == 0) return TRUE;
                         EnterNumBox (AufKopfWindow,"  Lieferdatum  ",
                                      lieferdatum, 11,
//                                      "dd.mm.yyyy");
                                            AufKopfED.mask[1].picture);
                         display_form (AufKopfWindow, &AufKopfE, 0, 0);
					     PostMessage (msg->hwnd, WM_KEYDOWN, VK_RETURN, 0l);
                         return TRUE;
                    }
                    else if (MouseinWindow (AufKopfED.mask[2].feldid, &mousepos))
                    {
                         if (asend == 0) return TRUE;
                         EnterNumBox (AufKopfWindow,"  Komm.-Datum  ",
                                      komm_dat   , 11,
//                                      "dd.mm.yyyy");
                                          AufKopfED.mask[2].picture);
                         display_form (AufKopfWindow, &AufKopfE, 0, 0);
					     PostMessage (msg->hwnd, WM_KEYDOWN, VK_RETURN, 0l);
                         return TRUE;
                    }
                    else if (MouseinWindow (AufKopfED.mask[3].feldid, &mousepos))
                    {
                         if (asend == 0) return TRUE;
                         EnterNumBox (AufKopfWindow,"  Lieferzeit  ",
                                      lieferzeit, 6,
                                            AufKopfED.mask[3].picture);
                         display_form (AufKopfWindow, &AufKopfE, 0, 0);
					     PostMessage (msg->hwnd, WM_KEYDOWN, VK_RETURN, 0l);
                         return TRUE;
                    }
                    else if (MouseinWindow (AufKopfED.mask[4].feldid, &mousepos))
                    {
                         if (asend == 0) return TRUE;
                         EnterNumBox (AufKopfWindow,"  Status  ",
                                         ls_stat,  2,
                                            AufKopfED.mask[4].picture);
                         display_form (AufKopfWindow, &AufKopfE, 0, 0);
					     PostMessage (msg->hwnd, WM_KEYDOWN, VK_RETURN, 0l);
                         return TRUE;
                    }
             }
          }

          return FALSE;
}

void KonvAufKopf (void)
/**
Spaltenposition von AufKopfED anpassen.
**/
{
        static int KonvOK = 0;
        int z, s;
        int i;

        if (KonvOK) return;

        KonvOK = 1;
        for (i = 0; i < AufKopfED.fieldanz; i ++)
        {
                    s = AufKopfED.mask[i].pos [1];
                    KonvTextPos (AufKopfE.font,
                                 AufKopfED.font,
                                 &z, &s);
                    AufKopfED.mask[i].pos [1] = s;
        }

        s = AufKopfTD.mask[1].pos [1];
        KonvTextPos (AufKopfE.font,
                    AufKopfED.font,
                    &z, &s);
        AufKopfTD.mask[1].pos [1] = s;

        s = AufKopfTD.mask[5].pos [1];
        KonvTextPos (AufKopfE.font,
                    AufKopfED.font,
                    &z, &s);
        AufKopfTD.mask[5].pos [1] = s;
}

void InitAufKopf (void)
/**
Auftrag-Kopf initialisieren.
**/
{
        int i;

        AufKopfE.mask[0].feld[0] = (char) 0;

        for (i = 0; i < AufKopfED.fieldanz; i ++)
        {
                      AufKopfED.mask[i].feld[0] = (char) 0;
        }
        kun_krz1[0] = (char) 0;
        ls_stat_txt[0] = (char) 0;
}


void MainBuInactiv (void)
/**
Alle Button auf Inaktiv setzen.
**/
{
          ActivateColButton (BackWindow2, &MainButton, 0, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 1, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 2, -1, 1);
//          ActivateColButton (BackWindow2, &MainButton, 4, -1, 1);
}




/**

Wiegen

**/


#define BUTARA  502
static int break_wieg = 0;

HWND WiegKg;
HWND WiegCtr;

static char WiegText [30] = {"Kiloware"};
static char WiegMe [12] = {"14.150"};


mfont WiegKgFont  = {"Courier New", 300, 0, 1, BLACKCOL,
                                               WHITECOL,
                                               1,
                                               NULL};

mfont WiegKgFontMe = {"Courier New", 300, 0, 1, BLACKCOL,
                                                RGB (0, 255, 255),
                                                1,
                                                NULL};


mfont WiegKgFontT  = {"Courier New", 150, 0, 1, WHITECOL,
                                                BLUECOL,
                                                1,
                                                NULL};

mfont WiegBuFontE  = {"Courier New", 200, 0, 1, BLACKCOL,
                                                REDCOL,
                                                1,
                                                NULL};

mfont WiegBuFontW  = {"Courier New", 150, 0, 1, BLACKCOL,
                                                GREENCOL,
                                                1,
                                                NULL};

mfont EtiFont  = {"Courier New", 120, 0, 1, WHITECOL,
                                            BLUECOL,
                                            1,
                                            NULL};

ColButton WieOK = { "OK", -1, -1,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     BLACKCOL,
                     GREENCOL,
                     2};


ColButton WieEnde = {"Zur�ck", -1, -1,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     BLACKCOL,
                     REDCOL,
                     2};

ColButton WieWie = {"Wiegen", -1, -1,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     BLACKCOL,
                     GREENCOL,
                     2};

ColButton WieHand = {"Hand", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      BLACKCOL,
                      YELLOWCOL,
                      2};

ColButton WiePreis = {"Preise", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      BLACKCOL,
                      YELLOWCOL,
                      2};

ColButton WieTara = {"Tara", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      // BLACKCOL,
                      // RGB (0, 255, 255),
                      WHITECOL,
                      RGB (0, 0, 255),
                      2};

ColButton AutoTara = {"Auto-", -1,15,
                      "Tara ", -1,40,
                      "aus",   -1,65,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       // BLACKCOL,
                       // RGB (0, 255, 255),
                       WHITECOL,
                       RGB (0, 0, 255),
                       3};

ColButton DelTara = { "Tara   ", -1, 25,
                      "L�schen", -1, 50,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      RGB (0, 0, 255),
                      2};

ColButton FestTara = {"Fest", -1, 25,
                      "Tara", -1, 50,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      RGB (0, 0, 255),
                      2};


ColButton SetTara1 = { "Tara  ", -1, 15,
                       "�ber- ", -1, 40,
                       "nehmen", -1, 65,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       WHITECOL,
                       RGB (0, 0, 255),
                       2};

ColButton SetTara2 = { "Start-",  -1, 15,
                       "wiegen",  -1,  50,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       WHITECOL,
                       RGB (0, 0, 255),
                       2};

ColButton SetTara;

ColButton HandTara = {"Hand", -1, 25,
                      "Tara", -1, 50,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      RGB (0, 0, 255),
                      2};


ColButton Etikett = {"Etikett", -1, -1,
                     "", 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      2};

ColButton SumEti  = {"Summen", -1,   5,
                     "Etikett",-1 , 30,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      2};

static int current_wieg;
static long last_anz;
static int CtrMode;
static int wiegend = 8;
static int etiend = 6;
static int dowiegen (void);
static int doWOK (void);
static int dohand (void);
static int dotara (void);
static int dogebinde (void);
static int dopreis (void);
static int doauto (void);
static int deltara (void);
static int handtara (void);
static int festtara (void);
static int settara (void);
static int settara0 (void);
static int closetara (void);
static int doetikett (void);
static int dosumeti (void);
static int autoan = 0;
static int autopos = 1;
static char *anaus[] = {"aus", "ein"};
static char chargen_nr [18];
static int WiegY;
static double akt_tara = (double) 0.0;
static char last_charge [21] = {""};

static int bsize = 100;
static int babs = 100;

static int bsizek = 60;

static field _WieButton[] = {
(char *) &WieTara,          bsize, bsize, -1, 275, 0, "", COLBUTTON, 0,
                                                      dotara, BUTARA,
(char *) &WiePreis,         bsize, bsize, -1, 275, 0, "", REMOVED, 0,
                                                      dopreis, 0,
(char *) &WieHand,          bsize, bsize, -1, 15, 0, "", COLBUTTON, 0,
                                                      dohand, 0,
(char *) &WieWie,           bsize, bsize, -1, 145, 0, "", COLBUTTON, 0,
                                                      dowiegen, 0,
(char *) &WieOK,            bsize, bsize, -1, 275, 0, "", COLBUTTON, 0,
                                                      doWOK, 0,
(char *) &WieEnde,          bsize, bsize, -1, 405, 0, "", COLBUTTON, 0,
                                                      0, VK_F5};

static form WieButton = {6, 0, 0, _WieButton, 0, 0, 0, 0, &WiegBuFontW};


static BOOL WithEti = FALSE;;
static int eti_nr;
static char krz_txt [18];

static BOOL WithSumEti = FALSE;;
static int sum_eti_nr;
static char sum_krz_txt [18];

static double eti_netto;
static double eti_brutto;
static double eti_tara;
static double eti_stk;

static double sum_eti_netto;
static double sum_eti_brutto;
static double sum_eti_tara;
static double sum_eti_stk;

static void SetEti (double netto, double brutto, double tara)
/**
Wete fuer Etikettendruck setzen.
**/
{
	      eti_netto  = netto;
          eti_brutto = brutto;
		  eti_tara   = tara;
	      sum_eti_netto  += netto;
          sum_eti_brutto += brutto;
		  sum_eti_tara   += tara;
}


static void SetEtiStk (double stk)
/**
Wete fuer Etikettendruck setzen.
**/
{
	      eti_stk  = stk;
	      sum_eti_stk  += stk;
}

static field _EtiButton[] = {
(char *) &Etikett,          bsize, bsizek*1.2, 30, 700, 0, "", COLBUTTON, 0,
                                                      doetikett, 0,
(char *) &SumEti,           bsize, bsizek,100, 700, 0, "", COLBUTTON, 0,
                                                      dosumeti, 0};

form EtiButton = {2, 0, 0, _EtiButton, 0, 0, 0, 0, &EtiFont};

static int bsize2 = 100;
static int babs2 = 100;

static field _WieButtonT[] = {
(char *) &AutoTara,         bsize, bsize, -1, 275, 0, "", COLBUTTON, 0,
                                                      doauto, 0,
(char *) &DelTara,          bsize2, bsize2, -1, 145, 0, "", COLBUTTON, 0,
                                                      deltara, 0,
(char *) &FestTara,         bsize2, bsize2, -1, 15, 0, "", COLBUTTON, 0,
                                                      festtara, 0,
(char *) &HandTara,         bsize2, bsize2, -1, 405, 0, "", COLBUTTON, 0,
                                                      handtara, 0,
(char *) &SetTara,          bsize2, bsize2, -1, 15, 0, "", COLBUTTON, 0,
                                                      settara0, 0,
(char *) &WieEnde,          bsize2, bsize2, -1, 405, 0, "", COLBUTTON, 0,
                                                      0, VK_F5};

form WieButtonT = {6, 0, 0, _WieButtonT, 0, 0, 0, 0, &WiegBuFontW};

static field _WiegKgT [] = {
WiegText,           0, 0, 10, -1, 0, "", DISPLAYONLY, 0, 0, 0};

static form WiegKgT = {1, 0, 0, _WiegKgT, 0, 0, 0, 0, &WiegKgFontT};

static field _WiegKgE [] = {
WiegMe,             0, 0, 0, -1, 0, "%11.3f", READONLY, 0, 0, 0};

static form WiegKgE = {1, 0, 0, _WiegKgE, 0, 0, 0, 0, &WiegKgFont};

static field _WiegKgMe [] = {
"KG",                   10, 1, 0, 0, 0, "", DISPLAYONLY, 0, 0, 0};

static form WiegKgMe = {1, 0, 0, _WiegKgMe, 0, 0, 0, 0, &WiegKgFontMe};


mfont ArtWFont = {"Courier New", 150, 0, 1, RGB (0, 0, 0),
                                      RGB (255, 255, 255),
                                      1,
                                      NULL};
mfont ArtWFontT = {"Courier New", 120, 0, 1, RGB (0, 0, 0),
                                        RGB (0, 255, 255),
                                        1,
                                        NULL};

mfont StartWFont = {"Courier New", 170, 0, 1,
//                                              RGB (0, 0, 0),
//                                              RGB (0, 255, 255),
                                              BLUECOL,
                                              WHITECOL,
                                              1,
                                              NULL};
/*
mfont ArtWCFont = {"Courier New", 100, 0, 0, RGB (0, 0, 0),
                                      RGB (0, 255, 255),
                                      1,
                                      NULL};
                                      */

mfont ArtWCFont = {"Courier New", 150, 0, 1, RGB (0, 0, 0),
                                      RGB (255, 255, 255),
                                      1,
                                      NULL};

mfont ArtWCFontT = {"Courier New", 120, 0, 1, RGB (0, 0, 0),
                                        RGB (0, 255, 255),
                                        1,
                                        NULL};

mfont ftarafont  = {"Courier New", 150, 0, 1, RGB (0, 0, 0),
                                              RGB (255, 255, 255),
                                              1,
                                              NULL};

mfont ftaratfont  = {"Courier New", 150, 0, 1, RGB (0, 0, 0),
                                               RGB (0, 255, 255),
                                               1,
                                               NULL};

static field _artwconstT [] =
{
"Artikel",                 9, 1, 3, 3,  0, "", DISPLAYONLY,    0, 0, 0,
"Artikelbezeichnung",     18, 1, 3,14,  0, "", DISPLAYONLY,    0, 0, 0,
"Umlagerung",                 10, 1, 1, 3,  0, "", DISPLAYONLY,    0, 0, 0,
"Lager",                   9, 1, 1,14,  0, "", DISPLAYONLY,    0, 0, 0
};

static form artwconstT = {4, 0, 0,_artwconstT, 0, 0, 0, 0, &ArtWCFontT};

static field _startwieg[] =
{
    "Startwiegung",           13, 1, 8, 3, 0,  "", READONLY,    0, 0, 0
};

static form startwieg = {1, 0, 0, _startwieg, 0, 0, 0, 0, &StartWFont};


static char summe1 [40];
static char summe2 [40];
static char summe3 [40];
static char zsumme1 [40];
static char zsumme2 [40];
static char zsumme3 [40];

static char vorgabe1[40] = {"100"};
static char veinh1[10] = {"kg"};
static char vorgabe2[40] = {"100"};
static char veinh2[10] = {"kg"};

BOOL StartWiegen = FALSE;
BOOL Entnahmehand = FALSE;

static double EntnahmeStartGew = 0.0;
static double EntnahmeActGew   = 0.0;
static double EntnahmeTara     = 0.0;

static char wiegzahl [40];

static field _artwconst [] =
{
        aufps.a,            9, 1,  4, 3,  0, "%8.0f",  READONLY, 0, 0, 0,
        aufps.a_bz1,       21, 1,  4, 14, 0, "",       READONLY, 0, 0, 0,
        UmlgrNr,         9, 1,  2,  3, 0, "%8d",    READONLY , 0, 0, 0,
        LgrZu_krz,          21, 1,  2, 14, 0, "",       READONLY , 0, 0, 0
};


static form artwconst = {4, 0, 0,_artwconst, 0, 0, 0, 0, &ArtWCFont};



static form artwformT;
static form artwform;


// Masken fuer Wiegen

static field _artwformTW [] =
{
"Chargen-Nr",             10, 1, 5, 3,  0, "", DISPLAYONLY,    0, 0, 0,
"Restmenge",              10, 1, 1,37,  0, "", REMOVED,    0, 0, 0,
"Menge",                  11, 1, 5,37,  0, "", DISPLAYONLY,    0, 0, 0,
aufps.lief_me_bz,         10, 1, 7,37,  0, "", REMOVED,    0, 0, 0,
"Wiegungen",              10, 1, 1,37,  0, "", DISPLAYONLY,    0, 0, 0,
"Bestellmenge",           13, 1, 5,23,  0, "", REMOVED,    0, 0, 0,
aufps.auf_me_bz,          10, 1, 7,23,  0, "", REMOVED,    0, 0, 0,
"Sollmenge",              11, 1, 3,37,  0, "", REMOVED,    0, 0, 0,
"Summe1",                  6, 1, 3,49,  0, "", REMOVED,    0, 0, 0,
"Summe2",                  6, 1, 5,49,  0, "", REMOVED,    0, 0, 0,
};

static form artwformTW = {10, 0, 0,_artwformTW, 0, 0, 0, 0, &ArtWFontT};


static field _artwformW [] =
{
        aufps.ls_charge,   19, 0, 6,  3, 0, "",       EDIT,     0, 0, 0,
        auf_me,            10, 1, 2, 37, 0, "%9.3f",  REMOVED , 0, 0, 0,
        wiegzahl,          10, 1, 2, 37, 0, "%9.0f",  READONLY, 0, 0, 0,
        aufps.lief_me,     10, 1, 6, 37, 0, "%9.3f",  READONLY, 0, 0, 0,
        aufps.auf_me,      12, 1, 6, 23, 0, "%11.3f", REMOVED, 0, 0, 0,
        aufps.auf_me_vgl,  10, 1, 4, 37, 0, "%9.3f",  REMOVED, 0, 0, 0,
        summe1,            10, 1, 4, 49, 0, "%9.3f",  REMOVED, 0, 0, 0,
        summe2,            10, 1, 6, 49, 0, "%9.3f",  REMOVED, 0, 0, 0,
        zsumme1,            4, 1, 4, 61, 0, "%3d",    REMOVED, 0, 0, 0,
        zsumme2,            4, 1, 6, 61, 0, "%3d",    REMOVED, 0, 0, 0,
};

static form artwformW = {10, 0, 0,_artwformW, 0, 0, 0, 0, &ArtWFont};

static char ActEntnahmeGew [20] = {"0.0"};

static field _fEntnahmeGewTxt [] =
{
"Aktuelles Gewicht",       20, 1, 1,49,  0, "", DISPLAYONLY,    0, 0, 0,
};

static form fEntnahmeGewTxt = {1, 0, 0,_fEntnahmeGewTxt, 0, 0, 0, 0, &ArtWFontT};

static field _fEntnahmeGew [] =
{
ActEntnahmeGew,            10, 1, 2,49,  0, "%9.3f", READONLY,    0, 0, 0,
};

static form fEntnahmeGew = {1, 0, 0,_fEntnahmeGew, 0, 0, 0, 0, &ArtWFont};

// Masken fuer Auszeichnung

static field _artwformTP [] =
{
"Chargen-Nr",             10, 1, 5, 3,  0, "", DISPLAYONLY,    0, 0, 0,
"Vorgaben",                10, 1, 1,37,  0, "", DISPLAYONLY,    0, 0, 0,
veinh2,                    9, 1, 5,37,  0, "", DISPLAYONLY,    0, 0, 0,
aufps.lief_me_bz,         10, 1, 7,37,  0, "", REMOVED,        0, 0, 0,
"Liefermenge",            13, 1, 5,23,  0, "", DISPLAYONLY,        0, 0, 0,
aufps.lief_me_bz,         10, 1, 7,23,  0, "", DISPLAYONLY,    0, 0, 0,
aufps.auf_me_bz,          10, 1, 7,37,  0, "", DISPLAYONLY,    0, 0, 0,
veinh1,                    9, 1, 3,37,  0, "", DISPLAYONLY,    0, 0, 0,
"Summen",                  6, 1, 1,49,  0, "", DISPLAYONLY,    0, 0, 0,
"Summe2",                  6, 1, 3,49,  0, "", REMOVED,    0, 0, 0,
"Summe3",                  6, 1, 5,49,  0, "", REMOVED,    0, 0, 0,
"kg",                      6, 1, 7,49,  0, "", REMOVED,    0, 0, 0,
"St�ck",                   7, 1, 3, 61, 0, "", DISPLAYONLY , 0, 0, 0,
"Karton",                  7, 1, 5, 61, 0, "", DISPLAYONLY , 0, 0, 0,
"Palette",                 7, 1, 7, 61, 0, "", DISPLAYONLY , 0, 0, 0,
veinh1,                    9, 1, 3, 49,  0, "", DISPLAYONLY,    0, 0, 0,
veinh2,                    9, 1, 5, 49,  0, "", DISPLAYONLY,    0, 0, 0,
aufps.auf_me_bz,          10, 1, 7, 49,  0, "", DISPLAYONLY,    0, 0, 0,
};

static form artwformTP = {18, 0, 0,_artwformTP, 0, 0, 0, 0, &ArtWFontT};


static field _artwformP [] =
{
        aufps.ls_charge,   19, 0, 6,  3, 0, "",       EDIT,     0, 0, 0,
        vorgabe1,          12, 1, 2, 37, 0, "%11.3f", READONLY, 0, 0, 0,
        aufps.lief_me,     10, 1, 6, 23, 0, "%9.3f",  READONLY,  0, 0, 0,
        aufps.auf_me,      12, 1, 6, 37, 0, "%11.3f", READONLY, 0, 0, 0,
        vorgabe2,          12, 1, 4, 37, 0, "%11.3f",  READONLY, 0, 0, 0,
        summe1,            10, 1, 2, 49, 0, "%9.3f",  READONLY, 0, 0, 0,
        summe2,            10, 1, 4, 49, 0, "%9.3f",  READONLY, 0, 0, 0,
        summe3,            10, 1, 6, 49, 0, "%9.3f",  READONLY, 0, 0, 0,
        zsumme1,            4, 1, 2, 61, 0, "%3d",    READONLY, 0, 0, 0,
        zsumme2,            4, 1, 4, 61, 0, "%3d",    READONLY, 0, 0, 0,
        zsumme3,            4, 1, 6, 61, 0, "%3d",    READONLY, 0, 0, 0,
};

static form artwformP = {11, 0, 0,_artwformP, 0, 0, 0, 0, &ArtWFont};

// Ende Maske fuer Preisauszeichnung

static field _ftaratxt [] =
{
"Tara ",                   5, 1,  8, 13, 0, "",  DISPLAYONLY, 0, 0, 0
};

static form ftaratxt = {1, 0, 0, _ftaratxt, 0, 0, 0, 0, &ftaratfont};

static field _ftara [] =
{
        aufps.tara,        8, 1,  8, 20, 0, "%7.3f",  READONLY, 0, 0, 0
};

static form ftara = {1, 0, 0, _ftara, 0, 0, 0, 0, &ftarafont};


void SwitchLiefAuf (void)
/**
Text fuer Autrags-Nr oder Lieferschein belegen.
**/
{
	/********
         if (LiefEnterAktiv)
         {
                   artwconstT.mask[2].feld   = "Lieferschein";
                   artwconstT.mask[2].length = 0;
                   artwconst.mask[2].feld    = lief_nr;
         }
         else
         {
                   artwconstT.mask[2].feld   = "Auftrag";
                   artwconstT.mask[2].length = 0;
                   artwconst.mask[2].feld    = auftrag_nr;
         }
		 ***********/
}


void write_wewiepro (double brutto, double netto, double tara)
/**
In Tabelle we_wiepro schreiben.
**/
{
         char datum [12];
         char zeit  [12];

		 if (brutto  == (double) 0) return ;
		 sysdate (datum);
         systime (zeit);

		 we_wiepro.mdn = lgr_umlgrk.mdn;
         we_wiepro.fil = 0;
         we_wiepro.a   = ratod (aufps.a);
         we_wiepro.best_blg = 0;
         strcpy (we_wiepro.blg_typ, "U"); //blg_tyxp Schlachtung
         sprintf (we_wiepro.lief_rech_nr, "%ld", lgr_umlgrk.umlgr_nr);
		 we_wiepro.me = brutto;
         we_wiepro.tara = tara;
         memcpy (we_wiepro.zeit, zeit, 5);
		 we_wiepro.zeit[5] = 0;	 ;
         we_wiepro.dat = dasc_to_long (datum);
         we_wiepro.sys = sys_peri.sys;
         strcpy (we_wiepro.pers, sys_ben.pers_nam);
         strcpy (we_wiepro.erf_kz, aufps.erf_kz);
         we_wiepro.anzahl = last_anz;
         strcpy (we_wiepro.qua_kz, "0");
         strcpy (we_wiepro.ls_charge, aufps.ls_charge);
		 WeWiePro.dbinsert ();
         we_wiepro.es_nr = 0l;
         strcpy (we_wiepro.pers, "Umlagerung");
}


int GetGewicht (char *datei, double *brutto, double *netto, double *tara)
/**
Gewichtswerte aus der Datei ergebnis holen.
**/
{
         FILE *fp;
         char buffer [81];

         fp = fopen (datei, "r");
         if (fp == NULL) return (-1);

/*
         if (testmode)
         {
                    disp_messG ("ergebnis ge�ffnet", 1);
         }
*/

         while (fgets (buffer, 80, fp))
         {
/*
               if (testmode)
               {
                     print_messG (1, "gelesen %s", buffer);
               }
*/
               switch (buffer [0])
               {
                        case 'B' :
                                *brutto = (double) ratod (&buffer[1]);
                                break;
                        case 'N' :
                                *netto = (double) ratod (&buffer[1]);
                                break;
                        case 'T' :
                                *tara = (double) ratod (&buffer[1]);
                                break;
                        case 'E' :
                                if (strlen (&buffer[1]) < 7)
                                {
                                    sprintf (we_wiepro.pers, "%06ld", atol (&buffer[1]));
                                    we_wiepro.es_nr = atol (&buffer[1]);
                                }
                                else
                                {
                                    sprintf (we_wiepro.pers, "%012.0lf",
                                             ratod (&buffer[1]));
                                }
               }
         }
         fclose (fp);

         if (*netto == (double) 0)
         {
                        *netto = *brutto - *tara;
         }
         else if (*brutto == (double) 0)
         {
                        *brutto = *netto + *tara;
         }

         if (*tara == (double) 0.0)
         {
                        *tara = *brutto - *netto;
         }

         if (WiegeTyp == STANDARD)
         {
/*
               if (autoan)
               {
                       akt_tara = *brutto;
               }
               else
*/
               {
                       akt_tara = *tara;
               }
         }


         if (WiegeTyp == ENTNAHME && StartWiegen == FALSE)
         {
                   double gew = EntnahmeActGew - *netto;
                   gew -= EntnahmeTara;
                   if (gew < 0.0)
                   {
                       gew = 0.0;
                   }
                   EntnahmeActGew = *netto;
                   sprintf (ActEntnahmeGew, "%.3lf",EntnahmeActGew);
                   *netto = gew;
                   *tara  = EntnahmeTara;
                   *brutto = *netto + *tara;
         }

         if (StartWiegen == FALSE)
         {
		               write_wewiepro (*brutto, *netto, *tara);
         }

/*
         if (testmode)
         {
                    print_messG (1,"%.2lf  %.2lf %-2lf", *brutto, *netto, *tara);
         }
*/
         return 0;
}

void fnbexec (int arganz, char *argv[])
/**
**/
{
        char aufruf [256];
        int i;

        if (getenv (BWS) == NULL) return;

        sprintf (aufruf, "%s\\fnb.exe", waadir);

        for (i = 1; i < arganz; i ++)
        {
                     strcat (aufruf, " ");
                     strcat (aufruf, argv[i]);
        }

//        WaitConsoleExec (aufruf, CREATE_NO_WINDOW);
        ProcWaitExec (aufruf, SW_SHOWMINIMIZED, -1, 0, -1, 0);
}


BOOL WaaInitOk (char *wa)
/**
Test, ob die Waage schon initialisiert wurde.
**/
{
        static char *waadirs [20];
        static int waaz = 0;
        int i;

        if (waaz == 20) return FALSE;
        clipped (wa);
        for (i = 0; i < waaz; i ++)
        {
            if (strcmp (wa, waadirs [i]) == 0)
            {
                return TRUE;
            }
        }
        waadirs [i] = (char *) malloc (strlen (wa) + 3);
        if (waadirs [i] == NULL)
        {
            return FALSE;
        }
        strcpy (waadirs[i], wa);
        if (waaz < 20) waaz ++;
        return FALSE;
}



void WaageInit (void)
/**
Waage Initialisieren.
**/
{
        static int arganz;
        static char *args[4];


//        if (WaaInit) return;

        if (WaaInitOk (waadir)) return;

        deltaraw ();
        WaaInit = 1;
        arganz = 3;
        args[0] = "Leer";
        args[1] = "0";
        args[2] = waadir;
        fnbexec (arganz, args);
}

int doWOK (void)
/**
Wiegen durchfuehren.
**/
{
	    if (artwform.mask[0].attribut == EDIT)
		{
                 GetWindowText (artwform.mask[0].feldid,
                      artwform.mask[0].feld,
                      artwform.mask[0].length);
		}
        strcpy (aufps.s, "3");
        break_wieg = 1;
        return 0;
}


void setautotara (BOOL an)
/**
Wiegen durchfuehren.
**/
{
	    if (an)
		{
              AutoTara.aktivate = 4;
              autoan = 1;
        }
        else
        {
              AutoTara.aktivate = 3;
              autoan = 0;
        }
        AutoTara.text3 = anaus [autoan];
}


int doauto (void)
/**
Wiegen durchfuehren.
**/
{
        if (AutoTara.aktivate == 4)
        {
              autoan = 1;
        }
        else
        {
              autoan = 0;
        }
        AutoTara.text3 = anaus [autoan];
        current_wieg = autopos;
        return 0;
}

void PressAuto ()
/**
Autotara druecken.
**/
{
       SendMessage (WieButton.mask[autopos].feldid, WM_LBUTTONDOWN, 0l, 0l);
       SendMessage (WieButton.mask[autopos].feldid, WM_LBUTTONUP, 0l, 0l);
}

int festtara (void)
/**
Wiegen durchfuehren.
**/
{
        double netto;
        double brutto;
        double tara;
        static int arganz;
        static char ftara [11];
        static char *args[5];
        char buffer [20];


        syskey = 0;
        ShowFestTara (buffer);

        if (syskey == KEY5)
        {
                  return 0;
        }


        sprintf (ftara, "%.3lf", (double) ratod (buffer));
        if (fest_tara_anz)
        {
  		        NuBlock.SetParent (LogoWindow);
                sprintf (buffer, "%d", 1);
                NuBlock.EnterCalcBox (WiegWindow,"  Anzahl ",  buffer, 5, "%4df", EntNuProc);
                if (syskey != KEY5 && atoi (buffer) > 1)
                {
                   sprintf (ftara, "%.3lf", (double) ratod (ftara) * atoi (buffer));
                }
        }

		if (fest_tara_sum || autoan)
		{
                  akt_tara = akt_tara + (double) ratod (ftara);
		}
		else
		{
                  akt_tara = (double) ratod (ftara);
		}
        sprintf (aufps.tara, "%.3lf", akt_tara);
        sprintf (ftara, "%.3lf", akt_tara);
        if (WiegeTyp == ENTNAHME)
        {
                  EntnahmeTara = akt_tara;
                  return 0;
        }
        arganz = 4;
        args[0] = "Leer";
        args[1] = "23";
        args[2] = waadir;
        args[3] = (char *) ftara;
        fnbexec (arganz, args);
        brutto = netto = tara = (double) 0.0;
        GetGewicht (ergebnis, &brutto, &netto, &tara);
        return 0;

/*
        ShowFestTara (buffer);
        sprintf (ftara, "%.3lf", (double) ratod (buffer));
        arganz = 4;
        args[0] = "Leer";
        args[1] = "23";
        args[2] = waadir;
        args[3] = (char *) ftara;
        fnbexec (arganz, args);
        brutto = netto = tara = (double) 0.0;
        GetGewicht (ergebnis, &brutto, &netto, &tara);
        sprintf (aufps.tara, "%.3lf", ratod (ftara));
        display_form (WiegKg, &WiegKgE, 0, 0);
        return 0;
*/
}

int deltaraw (void)
/**
Wiegen durchfuehren.
**/
{
        static int arganz;
        static char *args[4];

        arganz = 3;
        args[0] = "Leer";
        args[1] = "5";
        args[2] = waadir;
        fnbexec (arganz, args);
        return 0;
}

int deltara (void)
/**
Wiegen durchfuehren.
**/
{
        double netto;
        double brutto;
        double tara;
        static int arganz;
        static char *args[4];

        sprintf (aufps.tara, "%.3lf", (double) 0.0);
        if (WiegeTyp == ENTNAHME)
        {
                  EntnahmeTara = 0.0;
                  akt_tara = 0.0;
//             	  display_form (WiegWindow, &ftara, 0, 0);
//                  return 0;
        }
        arganz = 3;
        args[0] = "Leer";
        args[1] = "5";
        args[2] = waadir;
        fnbexec (arganz, args);
        brutto = netto = tara = (double) 0.0;
        GetGewicht (ergebnis, &brutto, &netto, &tara);
        akt_tara = (double) 0.0;
		display_form (WiegWindow, &ftara, 0, 0);
        return 0;

/*
        arganz = 3;
        args[0] = "Leer";
        args[1] = "5";
        args[2] = waadir;
        fnbexec (arganz, args);
        brutto = netto = tara = (double) 0.0;
        GetGewicht (ergebnis, &brutto, &netto, &tara);
        sprintf (aufps.tara, "%.3lf", (double) 0.0);
        display_form (WiegKg, &WiegKgE, 0, 0);
        akt_tara = (double) 0.0;
        return 0;
*/
}

int settara (void)
/**
Wiegen durchfuehren.
**/
{
        double netto;
        double brutto;
        double tara;
        static int arganz;
        static char *args[4];

        arganz = 3;
        args[0] = "Leer";
        args[1] = "4";
        args[2] = waadir;
        fnbexec (arganz, args);
        if (WiegeTyp == STANDARD)
        {
             Sleep (AutoSleep);
             arganz = 3;
             args[0] = "Leer";
             args[1] = "11";
             args[2] = waadir;
             fnbexec (arganz, args);
        }
        brutto = netto = tara = (double) 0.0;
        GetGewicht (ergebnis, &brutto, &netto, &tara);
		sprintf (aufps.tara, "%.3lf", akt_tara);
        if (WiegeTyp == ENTNAHME)
        {
                  EntnahmeTara = akt_tara;
        }
		display_form (WiegWindow, &ftara, 0, 0);
        return 0;

/*
        arganz = 3;
        args[0] = "Leer";
        args[1] = "4";
        args[2] = waadir;
        fnbexec (arganz, args);
        brutto = netto = tara = (double) 0.0;
        GetGewicht (ergebnis, &brutto, &netto, &tara);
        sprintf (aufps.tara, "%.3lf", tara);
        display_form (WiegKg, &WiegKgE, 0, 0);
        return 0;
*/
}

int settara0 (void)
{
        if (WiegeTyp == STANDARD)
        {
            return settara ();
        }
        StartWiegen = TRUE;
        dowiegen ();
        StartWiegen = FALSE;
        sprintf (ActEntnahmeGew, "%.3lf", EntnahmeActGew);
        return 0;
}


int setgebindetara (void)
{
	    GebTara = TRUE;
        NuBlock.NumEnterBreak ();
	    return 0;
}

int dogebindetara (char *ftara)
{
	    sprintf (ftara, "%7.3lf", (double) 0.0);
		EnterLeerPos ();
	    GebTara = FALSE;
		if (syskey == KEY5) return 0;
		GetGebGewicht (ftara);
	    return 0;
}

int handtara (void)
/**
Wiegen durchfuehren.
**/
{
        double netto;
        double brutto;
        double tara;
        static char ftara [11];
        static int arganz;
        static char *args[4];

        sprintf (ftara, "%7.3lf", (double) 0.0);
//        BuChoise.text1 = "&Gebinde";
		NuBlock.SetParent (LogoWindow);
        NuBlock.SetChoise (setgebindetara, 0, "&Gebinde");
        NuBlock.EnterCalcBox (WiegWindow,"  Tara  ",  ftara, 8, "%7.3f", EntNuProc);
        SetAktivWindow (WiegWindow);
		if (GebTara)
		{
			dogebindetara (ftara);
		}

		if (hand_tara_sum || autoan)
		{
                  akt_tara = akt_tara + (double) ratod (ftara);
		}
		else
		{
                  akt_tara = (double) ratod (ftara);
		}

        sprintf (aufps.tara, "%.3lf", akt_tara);
        sprintf (ftara, "%.3lf", akt_tara);
        if (WiegeTyp == ENTNAHME)
        {
                  EntnahmeTara = akt_tara;
                  current_wieg = 2;
                  return 0;
        }
        arganz = 4;
        args[0] = "Leer";
        args[1] = "23";
        args[2] = waadir;
        args[3] = (char *) ftara;
        fnbexec (arganz, args);
        brutto = netto = tara = (double) 0.0;
        GetGewicht (ergebnis, &brutto, &netto, &tara);
        current_wieg = 2;
        return 0;

/*
        akt_tara = akt_tara + (double) ratod (ftara);
        sprintf (ftara, "%.3lf", akt_tara);
        arganz = 4;
        args[0] = "Leer";
        args[1] = "23";
        args[2] = waadir;
        args[3] = (char *) ftara;
        fnbexec (arganz, args);
        brutto = netto = tara = (double) 0.0;
        GetGewicht (ergebnis, &brutto, &netto, &tara);
        current_wieg = 2;
        sprintf (aufps.tara, "%.3lf", akt_tara);
        display_form (WiegKg, &WiegKgE, 0, 0);
        return 0;
*/
}

static int StornoSet = 0;

int dostorno (void)
/**
SornoFlag setzen.
**/
{
       StornoSet = 1;
       PostMessage (EditForm.mask[0].feldid, WM_KEYDOWN,
                    (WPARAM) VK_RETURN, 0l);
       return 0;
}

BOOL TestAllStorno (void)
/**
Abfragem, ob die ganze Position stormiert werden soll.
**/
{
         double rest;
         double lief_me;

         if (WorkModus == AUSZEICHNEN && AktWiegPos)
         {
                 if (abfragejnG (WiegWindow,
                         "Einzelne Wiegung stornieren", "J"))
                 {
                          AuswahlWieg ();
                          rest    = (double) ratod (auf_me);
                          lief_me = (double) ratod (aufps.lief_me);
                          rest += StornoWieg;
                          lief_me -= StornoWieg;
                          sprintf (auf_me, "%.3lf", rest);
                          sprintf (aufps.lief_me, "%3lf", lief_me);
                          return FALSE;
                 }
         }

         if (abfragejnG (WiegWindow,
                         "Die gesamte Position stornieren", "N"))
         {
             return TRUE;
         }
         int idx = AuswahlWiePro ();
         if (idx == -1)
         {
             StornoSet = 0;
             return FALSE;
         }
         if (idx > wiepanz)
         {
             StornoSet = 0;
             return FALSE;
         }
         memcpy (&wiep, &wieparr[idx], sizeof (wiep));
         rest    = (double) ratod (auf_me);
         rest += ratod (wiep.brutto);
         if (WiegeTyp == ENTNAHME)
         {
                 StartWiegen = TRUE;
                 dowiegen ();
                 StartWiegen = FALSE;
                 sprintf (ActEntnahmeGew, "%.3lf", EntnahmeActGew);
          }
          lief_me = ratod (aufps.lief_me);
          lief_me -= ratod (wiep.brutto);
 	      sprintf (aufps.anz_einh, "%.3lf", atoi (aufps.anz_einh) - 1);
  	      strcpy (wiegzahl, aufps.anz_einh);
/*
          strcpy (auf_me, aufps.auf_me_vgl);
          strcpy (summe1, "0");
          strcpy (summe2, "0");
          strcpy (summe3, "0");
          strcpy (zsumme1, "0");
          strcpy (zsumme2, "0");
          strcpy (zsumme3, "0");
*/
          sprintf (aufps.lief_me, "%3lf", lief_me);
          strcpy (WiegMe, "0.000");
          memcpy (&aufparr [aufpidx], &aufps, sizeof (struct AUFP_S));
          arrtolgr_umlgrp (aufpidx);
          Lgr_umlgrpClass.dbupdate ();

          lief_me = ratod (wiep.brutto) * -1;
          last_anz = 1;
          write_wewiepro (lief_me, lief_me, (double) 0.0);
          StornoSet = 0;
          commitwork ();
          beginwork ();
          return FALSE;
}

void stornoaction (void)
/**
Storno ausfuehren.
**/
{
        double lief_me;
        double rest;
        double wiegme;

        lief_me = (double) ratod (aufps.lief_me);
        rest    = (double) ratod (auf_me);
        wiegme = ratod (editbuff);
        if (wiegme != (double) 0.0)
        {
            rest += wiegme;
            lief_me -= wiegme;
            sprintf (auf_me, "%.3lf", rest);
        }
        else
        {
            if (TestAllStorno () == FALSE)
            {
                return;
            }
            rest += lief_me;
            if (WiegeTyp == ENTNAHME)
            {
//                   EntnahmeActGew += lief_me;
                   StartWiegen = TRUE;
                   dowiegen ();
                   StartWiegen = FALSE;
                   sprintf (ActEntnahmeGew, "%.3lf", EntnahmeActGew);
            }
            lief_me = (double) 0;
  		    sprintf (aufps.anz_einh, "%.3lf", 0.0);
		    strcpy (wiegzahl, aufps.anz_einh);
            strcpy (auf_me, aufps.auf_me_vgl);
            strcpy (summe1, "0");
            strcpy (summe2, "0");
            strcpy (summe3, "0");
            strcpy (zsumme1, "0");
            strcpy (zsumme2, "0");
            strcpy (zsumme3, "0");
        }
        sprintf (aufps.lief_me, "%3lf", lief_me);
        strcpy (WiegMe, "0.000");
//        StornoSet = 0;
        memcpy (&aufparr [aufpidx], &aufps, sizeof (struct AUFP_S));
        arrtolgr_umlgrp (aufpidx);
        Lgr_umlgrpClass.dbupdate ();
        commitwork ();
        beginwork ();
}

int dohand (void)
/**
Wiegen durchfuehren.
**/
{
        double alt;
        double netto;

		DestroyKistBmp ();
        StornoSet = 0;
        SetStorno (dostorno, 0);
        sprintf (WiegMe, "%.3lf", (double) 0);
		_a_bas.me_einh = atoi(aufps.a_me_einh);
		if (_a_bas.me_einh == 2 )
		{
			EnterCalcBox (WiegWindow,"  Gewicht  ",  WiegMe, 12, WiegKgE.mask[0].picture);
		}
		else
		{
			EnterCalcBox (WiegWindow,"  Anzahl  ",  WiegMe, 12, "%ld");
		}
        if (syskey != KEY5)
        {
              if (WiegeTyp == ENTNAHME && StartWiegen)
              {
                     EntnahmeStartGew = ratod (WiegMe);
                     EntnahmeActGew   = EntnahmeStartGew;
                     sprintf (ActEntnahmeGew, "%lf", EntnahmeActGew);
                     display_form (WiegWindow, &fEntnahmeGew, 0, 0);
                     return 0;
              }
              strcpy (aufps.erf_kz, "H");
              if (StornoSet)
              {
				  netto = ratod (aufps.lief_me);
				  netto *= (double) -1.0;
				  last_anz = atol (aufps.anz_einh);
                  stornoaction ();
                  if (StornoSet)
                  {
                         write_wewiepro (netto, netto, (double) 0.0);
                  }
                  StornoSet = 0;
              }
              else
              {
                  netto = (double) ratod (WiegMe);
				  if (netto <= 99999.99)
				  {
                           if (WiegeTyp == ENTNAHME && Entnahmehand)
                           {
                                double gew = EntnahmeActGew - netto;
                                gew -= EntnahmeTara;
                                EntnahmeActGew = netto;
                                sprintf (ActEntnahmeGew, "%.3lf",EntnahmeActGew);
                                netto = gew;
                                sprintf (WiegMe, "%.3lf", netto);
                           }

                           write_wewiepro (netto, netto, (double) 0.0 );

                           alt = (double) ratod (auf_me);
                           alt -= netto;
                           sprintf (auf_me, "%3lf", alt);
                           alt = (double) ratod (aufps.lief_me);
                           alt += netto;
						   if (alt <= 99999.999)
						   {
                                   sprintf (aufps.lief_me, "%3lf", alt);
						   }
				  }
                  memcpy (&aufparr [aufpidx], &aufps, sizeof (struct AUFP_S));
                  arrtolgr_umlgrp (aufpidx);
                  Lgr_umlgrpClass.dbupdate ();
                  commitwork ();
                  beginwork ();
				  SetEti (netto, netto, 0.0);
              }
              display_form (WiegKg, &WiegKgE, 0, 0);
              display_form (WiegWindow, &artwform, 0, 0);
              display_form (WiegWindow, &artwconst, 0, 0);
        }
        current_wieg = 2;
		if (hand_direct)
		{
               strcpy (aufps.s, "3");
               break_wieg = 1;
		}
        return 0;
}

int GetDefault (char *env, char *wert)
/**
Wert aus bws_default holen.
**/
{
        char *etc;
        int anz;
        char buffer [512];
        FILE *fp;

        etc = getenv ("BWSETC");
        if (etc == (char *) 0)
        {
                    etc = "C:\\USER\\FIT\\ETC";
        }

        sprintf (buffer, "%s\\bws_defa", etc);
        fp = fopen (buffer, "r");
/*
        if (fp == NULL)
        {
                    sprintf (buffer, "%s\\bws_defa", etc);
                    fp = fopen (buffer, "r");
        }
*/
        if (fp == NULL) return -1;

        clipped (env);
        while (fgets (buffer, 511, fp))
        {
                     cr_weg (buffer);
                     anz = split (buffer);
                     if (anz < 2) continue;
                     if (strupcmp (wort[1], env, strlen (env)) == 0)
                     {
                                 strcpy (wert, wort [2]);
                                 fclose (fp);
                                 return 0;
                     }
         }
         fclose (fp);
         return (-1);
}

long GetStdAuszeichner (void)
/**
System-Nummer fuer Touch-Screen holen.
**/
{
        static long auszeichner = 0;
        char defwert [20];

        if (auszeichner) return auszeichner;

        if (GetDefault ("TOUCH-AUSZEICHNER", defwert) == -1)
        {
                             return (long) 1;
        }
        auszeichner = atol (defwert);
        if (GetDefault ("PAZ_DIREKT", defwert) != -1)
        {
                        paz_direkt = atoi (defwert);
        }
        return auszeichner;
}

long GetStdWaage (void)
/**
System-Nummer fuer Touch-Screen holen.
**/
{
        static long waage = 0;
        char defwert [20];

        if (waage) return waage;

        if (GetDefault ("TOUCH-WAAGE", defwert) == -1)
        {
                             return (long) 1;
        }
        waage = atol (defwert);
        return waage;
}

void GetAufArt9999 (void)
/**
System-Nummer fuer Touch-Screen holen.
**/
{
/*
        char defwert [20];
        if (GetDefault ("aufart9999", defwert) == -1)
        {
                             return (long) 1;
        }
        aufart9999 = atol (defwert);
        return waage;
*/
		aufart9999 = 0;
        strcpy (sys_par.sys_par_nam, "aufart9999");
        if (sys_par_class.dbreadfirst () == 0)
        {
                    aufart9999 = atoi (sys_par.sys_par_wrt);
        }
}

static long start_aufme;
static long start_liefme;
static long alarm_me = 0;
static short alarm_ok = 0;
static HWND AlarmWindow = NULL;
static short a_me_einh;
static int me_faktor = 1;
static long akt_me = 0;
static int break_ausz = 0;
int wiegx, wiegy, wiegcx, wiegcy;
int alarmy;
static int GxStop = 0;

static char amenge [80];

mfont AlarmFont  = {"Courier New", 150, 0, 1, BLACKCOL,
                                              LTGRAYCOL,
                                              1,
                                              NULL};

static field _alfield [] =
                 {"A C H T U N G ! ! !", 0, 0, 40, -1, 0, "", DISPLAYONLY, 0, 0, 0,
                  amenge,                0, 0, 70, -1, 0, "", DISPLAYONLY, 0, 0, 0};

static form alform = {2, 0, 0, _alfield, 0, 0, 0, 0, &AlarmFont};

mfont MessFont  = {"Courier New", 150, 0, 1, BLACKCOL,
                                             WHITECOL,
                                             1,
                                             NULL};

mfont MessFontBlue  = {"Courier New", 180, 0, 1, WHITECOL,
                                                 BLUECOL,
                                                 1,
                                                 NULL};

static char msgstring  [80];
static char msgstring1 [80];
static char msgstring2 [80];

static int msgy = 1;
static int oky = 4;
static int okcy = 3;
static int okcx = 8;
static int BreakKey5 (void);
static int BreakKey12 (void);
static int BreakOK (void);

static field _msgfield [] =
{
                  msgstring,  0, 0, 1, -1, 0, "", DISPLAYONLY, 0, 0, 0,
                  msgstring1, 0, 0, 2, -1, 0, "", DISPLAYONLY, 0, 0, 0,
                  msgstring2, 0, 0, 3, -1, 0, "", DISPLAYONLY, 0, 0, 0,
                  " OK ",     0, 2, 4, -1, 0, "", BUTTON, 0, 0, KEYCR,
};


static form msgform = {1, 0, 0, _msgfield, 0, 0, 0, 0, &MessFont};

static field _msgfieldjn [] =
{
                  msgstring,  0, 0, 1, -1, 0, "", DISPLAYONLY, 0, 0, 0,
                  msgstring1, 0, 0, 2, -1, 0, "", DISPLAYONLY, 0, 0, 0,
                  msgstring2, 0, 0, 3, -1, 0, "", DISPLAYONLY, 0, 0, 0,
                  "  Ja  ",   0, 2, 4, -1, 0, "", BUTTON, 0, 0,  KEY12,
                  " Nein ",   0, 2, 4, -1, 0, "", BUTTON, 0, 0,  KEY5,
};


static form msgformjn = {5, 0, 0, _msgfieldjn, 0, 0, 0, 0, &MessFont};

static form *aktmessform;


void TestMessage (void)
/**
Window-Meldungen testen.
**/
{
         MSG msg;

         if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE) == 0) return;

         if (msg.message == WM_KEYDOWN && msg.wParam != VK_RETURN) return;

         if (IsWiegMessage (&msg) == 0)
         {
                          TranslateMessage(&msg);
                          DispatchMessage(&msg);
         }
}

void WritePrcomm (char *comm)
/**
Datei Prcomm schreiben.
**/
{
        FILE *fp;
        char *tmp;
        char buffer [512];
        int z;

        tmp = getenv ("TMPPATH");
        if (tmp == NULL)
        {
                  tmp = "C:\\USER\\FIT\\TMP";
        }
        sprintf (buffer, "%s\\prcomm", tmp);
        z = 0;
        fp = fopen (buffer, "w");
        while (fp == NULL)
        {
                    if (z == 1000) break;
                    TestMessage ();
                    if (break_ausz) break;
                    Sleep (10);
                    z ++;
                    fp = fopen (buffer, "w");
        }
        if (fp == NULL) return;
        fprintf (fp, "%s\n", comm);
        fclose (fp);
}

void Alarm (void)
/**
Warnung beim Ueberschreiten der Alarmmenge.
**/
{
        int i;

        if (AlarmWindow) return;

        sprintf (amenge, "Sollmenge fast erreicht");

/*
        AlarmWindow  = CreateWindowEx (
                              0,
                              "AufWiegCtr",
                              "",
                              WS_POPUP | WS_DLGFRAME,
                              wiegx, wiegy - wiegcy - 5,
                              wiegcx, wiegcy,
                              WiegWindow,
                              NULL,
                              hMainInst,
                              NULL);
*/
        AlarmWindow  = CreateWindowEx (
                              0,
                              "AufWiegCtr",
                              "",
                              WS_POPUP | WS_DLGFRAME,
                              wiegx,
//                              wiegy,
                              alarmy,
                              wiegcx, wiegcy,
                              WiegWindow,
                              NULL,
                              hMainInst,
                              NULL);
        if (AlarmWindow == NULL) return;
        ShowWindow (AlarmWindow, SW_SHOWNORMAL);
        UpdateWindow (AlarmWindow);
        for (i = 0; i < 3; i ++)
        {
                    MessageBeep (0xFFFFFFFF);
        }
}


LONG FAR PASCAL MsgProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        switch(msg)
        {

              case WM_PAINT :
                    if (hWnd == MessWindow)
                    {
                            display_form (hWnd, aktmessform, 0, 0);
                    }
                    break;
             case WM_COMMAND :
                    if (LOWORD (wParam) == KEY5 ||
                        LOWORD (wParam) == VK_F5)
                     {
                            syskey = KEY5;
                            SendKey (VK_F5);
                            break;
                    }
                    if (LOWORD (wParam) == KEYCR)
                    {
                            syskey = KEYCR;
                            SendKey (VK_RETURN);
                            break;
                    }
                    if (LOWORD (wParam) == KEY12)
                    {
                            syskey = KEY12;
                            SendKey (VK_F12);
                            break;
                    }
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}


void CreateMessage (HWND hWnd, char *message)
/**
Warnung beim Ueberschreiten der Alarmmenge.
**/
{
        int x, y, cx, cy;
        int i;
        RECT rect;
        TEXTMETRIC tm;

        if (MessWindow) return;

        aktmessform = &msgform;
        for (i = 0; i < msgform.fieldanz; i ++)
        {
            msgform.mask[i].length = 0;
        }

        msgform.fieldanz = 1;
        msgform.mask[0].pos[0] = -1;
        msgform.mask[0].pos[1] = -1;
        msgform.font = &MessFont;
        SetTextMetrics (hWnd, &tm, msgform.font);

        GetClientRect (hWnd, &rect);
        strcpy (msgstring, message);

        cx = (strlen (msgstring) + 3) * tm.tmAveCharWidth;
        x = (rect.right - cx) / 2;
        if (x == 0) x = 0;

        cy = tm.tmHeight * 2;
        y = (rect.bottom - cy) / 2 ;
        MessWindow  = CreateWindowEx (
                              0,
                              "AufWiegWhite",
                              "",
                              WS_POPUP | WS_DLGFRAME,
                              x, y,
                              cx, cy,
                              hWnd,
                              NULL,
                              hMainInst,
                              NULL);
        if (MessWindow == NULL) return;
        ShowWindow (MessWindow, SW_SHOWNORMAL);
        UpdateWindow (MessWindow);
}

void DestroyMessage (void)
{
        if (MessWindow == NULL) return;
        CloseControls (&msgform);
        DestroyWindow (MessWindow);
        MessWindow = 0;
}

int BreakKey5 ()
{
        syskey = KEY5;
        break_enter ();
        return 0;
}

int BreakKey12 ()
{
        syskey = KEY12;
        break_enter ();
        return 0;
}

int BreakOK ()
{
        break_enter ();
        return 0;
}


int CreateMessageJN (HWND hWnd, char *message, char *def)
/**
Warnung beim Ueberschreiten der Alarmmenge.
**/
{
        int x, y, cx, cy;
        RECT rect;
        TEXTMETRIC tm;
        char *ltext;
        int anz;
        int i;
        int okpos;
        unsigned int len, pos;
        HWND OldFocus;

        if (MessWindow) return 0;

        aktmessform = &msgformjn;
        OldFocus = GetFocus ();
        anz = wsplit (message, "\n");
        if (anz == 0) return 0;

        for (i = 0; i < msgformjn.fieldanz; i ++)
        {
            msgformjn.mask[i].length = 0;
        }

        msgformjn.font = &MessFontBlue;
        SetTextMetrics (hWnd, &tm, msgformjn.font);

        msgformjn.mask[1].attribut = REMOVED;
        msgformjn.mask[2].attribut = REMOVED;
        strcpy (msgstring, wort[0]);
        len = strlen (msgstring);
        ltext = msgstring;
        okpos = oky;
        if (anz > 1)
        {
            strcpy (msgstring1, wort[1]);
            if (strlen (msgstring1) > len)
            {
                 len = strlen (msgstring1);
                 ltext = msgstring1;
            }
            msgformjn.mask[1].attribut = DISPLAYONLY;
            okpos += 1;
        }
        if (anz > 2)
        {
            strcpy (msgstring2, wort[2]);
            if (strlen (msgstring2) > len)
            {
                 len = strlen (msgstring2);
                 ltext = msgstring2;
            }
            msgformjn.mask[2].attribut = DISPLAYONLY;
            okpos += 1;
        }

        msgform.fieldanz = 5;

        GetClientRect (hMainWindow, &rect);

        cx = (len + 3) * tm.tmAveCharWidth;

        x = (rect.right - cx) / 2;
        if (x == 0) x = 0;

        cy = tm.tmHeight * 4;
        y = (rect.bottom - cy) / 2 ;

        msgformjn.mask[0].pos[0] = tm.tmHeight * msgy;
        msgformjn.mask[1].pos[0] = (int) (double) (tm.tmHeight * (msgy + 1 * 1.5));
        msgformjn.mask[2].pos[0] = (int) (double) (tm.tmHeight * (msgy + 2 * 1.5));
        msgformjn.mask[msgformjn.fieldanz - 2].pos[0] =
                                 (int) (double) (tm.tmHeight * okpos);
        msgformjn.mask[msgformjn.fieldanz - 1].pos[0] =
                                 (int) (double) (tm.tmHeight * okpos);

        msgformjn.mask[msgformjn.fieldanz - 2].rows   = tm.tmHeight * okcy;
        msgformjn.mask[msgformjn.fieldanz - 2].length = tm.tmAveCharWidth * okcx;
        msgformjn.mask[msgformjn.fieldanz - 1].rows   = tm.tmHeight * okcy;
        msgformjn.mask[msgformjn.fieldanz - 1].length = tm.tmAveCharWidth * okcx;
        strcpy (msgformjn.mask[msgformjn.fieldanz - 2].feld, "  Ja  ");
        strcpy (msgformjn.mask[msgformjn.fieldanz - 1].feld, " Nein ");

        pos = (len + 3 - 16) / 2;
        if (pos <= 0) pos = 0;

        msgformjn.mask[msgformjn.fieldanz - 2].pos[1] = pos * tm.tmAveCharWidth;
        msgformjn.mask[msgformjn.fieldanz - 1].pos[1] = (pos + 9)
                                                     * tm.tmAveCharWidth;

        cy =  msgformjn.mask[msgformjn.fieldanz - 1].pos[0] +
              msgformjn.mask[msgformjn.fieldanz - 1].rows +
              tm.tmHeight;
        y = (rect.bottom - cy) / 2 ;
        MessWindow  = CreateWindowEx (
                              0,
                              "AufWiegBlue",
                              "Meldung",
                              WS_POPUP | WS_DLGFRAME,
                              x, y,
                              cx, cy,
                              NULL,
                              NULL,
                              hMainInst,
                              NULL);
        if (MessWindow == NULL) return 0;
        ShowWindow (MessWindow, SW_SHOWNORMAL);
//        SetWindowPos (MessWindow, HWND_TOPMOST, 0, 0, 0, 0,
//                   SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
        UpdateWindow (MessWindow);
        display_form (MessWindow, &msgformjn, 0, 0);
        save_fkt (5);
        save_fkt (12);
        set_fkt (BreakKey5, 5);
        set_fkt (BreakKey12, 12);
        no_break_end ();
        EnableWindows (hMainWindow, FALSE);
        EnableWindows (hWnd, FALSE);
        SetButtonTab (TRUE);
        enter_form (MessWindow, &msgformjn, 0, 0);
        SetButtonTab (FALSE);
        EnableWindows (hWnd, TRUE);
        EnableWindows (hMainWindow, TRUE);
        DestroyMessage ();
        SetFocus (OldFocus);
        restore_fkt (5);
        restore_fkt (12);
        if (syskey == KEY5) return 0;
        return 1;
}


void CreateMessageOK (HWND hWnd, char *message)
/**
Warnung beim Ueberschreiten der Alarmmenge.
**/
{
        int x, y, cx, cy;
        RECT rect;
        TEXTMETRIC tm;
        char *ltext;
        int anz;
        int i;
        int okpos;
        unsigned int len;
        HWND OldFocus;

        if (MessWindow) return;

        aktmessform = &msgform;
        OldFocus = GetFocus ();
        anz = wsplit (message, "\n");
        if (anz == 0) return;

        for (i = 0; i < msgform.fieldanz; i ++)
        {
            msgform.mask[i].length = 0;
        }

        msgform.font = &MessFontBlue;
        SetTextMetrics (hWnd, &tm, msgform.font);

        msgform.mask[1].attribut = REMOVED;
        msgform.mask[2].attribut = REMOVED;
        strcpy (msgstring, wort[0]);
        len = strlen (msgstring);
        ltext = msgstring;
        okpos = oky;
        if (anz > 1)
        {
            strcpy (msgstring1, wort[1]);
            if (strlen (msgstring1) > len)
            {
                 len = strlen (msgstring1);
                 ltext = msgstring1;
            }
            msgform.mask[1].attribut = DISPLAYONLY;
            okpos += 1;
        }
        if (anz > 2)
        {
            strcpy (msgstring2, wort[2]);
            if (strlen (msgstring2) > len)
            {
                 len = strlen (msgstring2);
                 ltext = msgstring2;
            }
            msgform.mask[2].attribut = DISPLAYONLY;
            okpos += 1;
        }

        msgform.fieldanz = 4;

        GetClientRect (hMainWindow, &rect);

        cx = (len + 3) * tm.tmAveCharWidth;

        x = (rect.right - cx) / 2;
        if (x == 0) x = 0;

        cy = tm.tmHeight * 4;
        y = (rect.bottom - cy) / 2 ;

        msgform.mask[0].pos[0] = tm.tmHeight * msgy;
        msgform.mask[1].pos[0] = (int) (double) (tm.tmHeight * (msgy + 1 * 1.5));
        msgform.mask[2].pos[0] = (int) (double) (tm.tmHeight * (msgy + 2 * 1.5));
        msgform.mask[msgform.fieldanz - 1].pos[0] =
                                 (int) (double) (tm.tmHeight * okpos);

        msgform.mask[msgform.fieldanz - 1].rows   = tm.tmHeight * okcy;
        msgform.mask[msgform.fieldanz - 1].length = tm.tmAveCharWidth * okcx;
        strcpy (msgform.mask[msgform.fieldanz - 1].feld, " OK ");

        cy =  msgform.mask[msgform.fieldanz - 1].pos[0] +
              msgform.mask[msgform.fieldanz - 1].rows +
              tm.tmHeight;
        y = (rect.bottom - cy) / 2 ;
        MessWindow  = CreateWindowEx (
                              0,
                              "AufWiegBlue",
                              "Meldung",
                              WS_POPUP | WS_DLGFRAME,
                              x, y,
                              cx, cy,
                              NULL,
                              NULL,
                              hMainInst,
                              NULL);
        if (MessWindow == NULL) return;
        ShowWindow (MessWindow, SW_SHOWNORMAL);
//        SetWindowPos (MessWindow, HWND_TOPMOST, 0, 0, 0, 0,
//                   SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
        UpdateWindow (MessWindow);
        display_form (MessWindow, &msgform, 0, 0);
        set_fkt (BreakOK, 12);
        break_end ();
        EnableWindows (hMainWindow, FALSE);
        EnableWindows (hWnd, FALSE);
        SetButtonTab (TRUE);
        enter_form (MessWindow, &msgform, 0, 0);
        SetButtonTab (FALSE);
        EnableWindows (hWnd, TRUE);
        EnableWindows (hMainWindow, TRUE);
        DestroyMessage ();
        SetFocus (OldFocus);
}

void disp_messG (char *text, int modus)
{
        if (AktivWindow == NULL)
        {
                 AktivWindow = GetActiveWindow ();
        }
        if (modus == 2)
        {
                    MessageBeep (0xFFFFFFFF);
        }
        CreateMessageOK (AktivWindow, text);
}

static char messbuffer [1024];

int print_messG (int mode, char * format, ...)
/**
Ausgabe in dbfile.
**/
{
        va_list args;

        va_start (args, format);
        vsprintf (messbuffer, format, args);
        va_end (args);
        disp_messG (messbuffer, mode);
        return (0);
}


int abfragejnG (HWND hWnd, char *text, char *def)
/**
Ausgabe in dbfile.
**/
{
        return CreateMessageJN (hWnd, text, def);
}


void WaitStat (char *stat)
/**
Ergebnis-Datei vom Preisauszeichner lesen. Auf stat warten
**/
{
        FILE *fp;
        char *tmp;
        char buffer [512];
        char satz [512];
        int z;

        tmp = getenv ("TMPPATH");
        if (tmp == NULL)
        {
                  tmp = "C:\\USER\\FIT\\TMP";
        }
        sprintf (buffer, "%s\\prstat", tmp);

        z = 0;
        while (TRUE)
        {
                  if (z == 30000) break;
                  fp = fopen (buffer, "r");
                  if (fp == NULL)
                  {
                              z ++;
                              Sleep (10);
                              continue;
                  }
                  if (fgets (satz, 511, fp) == 0)
                  {
                              z ++;
                              fclose (fp);
                              Sleep (10);
                              continue;
                  }
                  fclose (fp);
                  cr_weg (satz);
                  split (satz);
                  if (strcmp (wort[1], stat) == 0) break;
                  z ++;
                  TestMessage ();
                  if (break_ausz) break;
                  Sleep (10);
         }
}


static long sup;
static long sup0;
static long sup1;
static short kunme_einh = 0;
static long  kunmez = 0l;
static long  kunme = 0l;
static double akt_zsu1 = (double) 0;
static double akt_zsu2 = (double) 0;
static char rec [256] = {" "};


void TestSu (double zsu1, double zsu2, double zsu3)
{
         sup = (long) zsu2;
         sup0 = (long) zsu3;
}


int AuszKomplett (void)
/**
Test, ob die Auftragsmenge erreicht ist.
**/
{
        long auf_me_kun;

        auf_me_kun = atol (aufps.auf_me);
        switch (kunme_einh)
        {
            case 2 :
                 if (ratod (auf_me) <= (double) 0.0)
                 {
                         return 1;
                 }
                 return 0;
            case 1 :
                 if (kunme > auf_me_kun)
                 {
                         return 1;
                 }
                 return 0;
            case 8 :
                 if (sup1 > auf_me_kun)
                 {
                         return 1;
                 }
                 return 0;
            case 16 :
                 if (sup0 > auf_me_kun)
                 {
                         return 1;
                 }
                 return 0;
        }
        return 0;
}


void GetKunMe ()
/**
Auftragsmenge in Kundenbestelleinheit
**/
{
         int me_einh_kun;

         me_einh_kun = atoi (aufps.me_einh_kun);
         switch (me_einh_kun)
         {
             case 2 :
                   kunme_einh = 2;
                   break;
             case 6 :
             case 7 :
                   kunme_einh = 8;
                   break;
             case 10 :
                   kunme_einh = 16;
                   break;
             default :
                   kunme_einh = 1;
                   break;
         }
}

void FillSummen (double su1, double su2,
                 double zsu1, double zsu2)
/**
Aktuellen Inhalte der einzelnen Summen in Abhaengigkeit
von der Einheit fuellen.
**/
{

         switch (a_kun_geb.geb_fill)
         {
               case 2 :
                    sprintf (summe1, "%.3lf", su1 / me_faktor);
                    break;
               case 1:
                    sprintf (summe1, "%.0lf", zsu1);
                    break;
               default :
                    sprintf (summe1, "%.3lf", su1 / me_faktor);
                    break;
         }

         switch (a_kun_geb.pal_fill)
         {
               case 2 :
                    sprintf (summe2, "%.3lf", su2 / me_faktor);
                    break;
               case 1:
                    sprintf (summe2, "%.0lf", zsu2);
                    break;
               case 5:
                    sprintf (summe2, "%ld", sup);
                    break;
               default :
                    sprintf (summe2, "%.3lf", su2 / me_faktor);
                    break;
         }
}

void FillSumme3 (double su3, double zsu3)
/**
Aktuellen Inhalte von Summe3 in Abhaengigkeit
von der Einheit fuellen.
**/
{

        sprintf (summe3, "%ld", sup0);
}

int KorrMe (char *satz)
/**
Auftragsmenge korrigieren.
**/
{
         int anz;
         long me;
         long zielme;
         double me_vgl;
         double su1;
         double su2;
         double zsu1;
         double zsu2;
         double zsu3;

         if (atoi (aufps.a_me_einh) == 2)
         {
                    me_vgl = ratod (aufps.auf_me_vgl) * 1000;
         }
         else
         {
                    me_vgl = ratod (aufps.auf_me_vgl);
         }

         cr_weg (satz);
         anz = split (satz);
         if (anz < 9)
         {
                   return 1;
         }

         if (wort[1][0] == 'E')
         {
                   return 0;
         }

         if (GxStop) return 1;


		 if (strcmp (rec," ") ==  0) strcpy (rec,satz);
		 if (strcmp (satz, rec) == 0) return 1;

		 strcpy (rec, satz);
         me   = atol (wort[9]);

         zsu1 = ratod (wort[4]);
         su1  = ratod (wort[5]);
         zsu2 = ratod (wort[6]);
         su2  = ratod (wort[7]);
         zsu3 = ratod (wort[8]);

         TestSu (zsu1, zsu2, zsu3);
         FillSummen (su1, su2, zsu1, zsu2);

         zsu2 = (double) sup;


         sprintf (zsumme1, "%.0lf", zsu1);
		 if (a_kun_geb.pal_fill == 5 || a_kun_geb.pal_fill == 8)
		 {
                 sprintf (zsumme2, "%.0lf", zsu2);
		 }
		 else if (kunme_einh == 8)
		 {
                 sprintf (zsumme2, "%.0lf", zsu3);
		 }
         if (!alarm_ok && me > 0l)
         {
                     alarm_me = (long) ((double) me_vgl - 3 * me);
                     alarm_ok = 1;
         }

         kunme ++;
         zielme = start_aufme - me;
         sprintf (auf_me, "%.3lf", (double) ((double) zielme / me_faktor));

         zielme = start_liefme + me;
         sprintf (aufps.lief_me, "%.3lf", (double) ((double) zielme / me_faktor));
         FillSumme3 ((double) zielme, zsu3);
         zsu3 = (double) sup0;
		 if (kunme_einh == 16)
		 {
                   sprintf (zsumme3, "%.0lf", zsu3);
		 }
         AktPrWieg[AktWiegPos] = ((double) (me - akt_me)) / me_faktor;
         sprintf (WiegMe, "%.3lf", (double) ((double) (me - akt_me) / me_faktor));


         display_field (WiegWindow, &artwform.mask[1], 0, 0);
         display_field (WiegWindow, &artwform.mask[2], 0, 0);

         display_field (WiegWindow, &artwform.mask[3], 0, 0);
         display_field (WiegWindow, &artwform.mask[4], 0, 0);
         display_field (WiegWindow, &artwform.mask[5], 0, 0);
         display_field (WiegWindow, &artwform.mask[6], 0, 0);
         display_field (WiegWindow, &artwform.mask[7], 0, 0);
         display_field (WiegWindow, &artwform.mask[8], 0, 0);
         display_field (WiegWindow, &artwform.mask[9], 0, 0);
         display_field (WiegWindow, &artwform.mask[10], 0, 0);



         display_field (WiegKg,     &WiegKgE.mask[0], 0, 0);

         memcpy (&aufparr [aufpidx], &aufps, sizeof (struct AUFP_S));
         arrtolgr_umlgrp (aufpidx);
         Lgr_umlgrpClass.dbupdate ();
         commitwork ();
         beginwork ();

         SetFocus (WieButton.mask[3].feldid);
         if (AktWiegPos < 998) AktWiegPos ++;
         akt_me = (long) me;

         return 1;
}

void PollAusz (void)
/**
Ergebnis-Datei vom Preisauszeichner lesen.
**/
{
        FILE *fp;
        char *tmp;
        char buffer [512];
        char satz [512];
        int pollanz;

        a_me_einh = atoi (aufps.a_me_einh);
        alarm_me = 0l;
        alarm_ok = 0;
        kunme = 0;
        kunmez = 0l;
        GetKunMe ();
        sup = (long) 0;
        sup0 = 0l;
        sup1 = 0l;
		akt_zsu1 = (double) 0.0;
		akt_zsu2 = (double) 0.0;
		strcpy (rec, " ");
        if (a_me_einh == 2)
        {
                      me_faktor = 1000;
        }
        else
        {
                      me_faktor = 1;
        }

        start_aufme  =  (long) ((double) ratod (auf_me) * me_faktor);
        start_liefme =  (long) ((double) ratod (aufps.lief_me) * me_faktor);

        tmp = getenv ("TMPPATH");
        if (tmp == NULL)
        {
                  tmp = "C:\\USER\\FIT\\TMP";
        }
        sprintf (buffer, "%s\\prstat", tmp);

        GxStop = pollanz = 0;
        while (TRUE)
        {
                  TestMessage ();
                  if (GxStop) pollanz ++;
                  if (pollanz >= 40) break;
                  fp = fopen (buffer, "r");
                  if (fp == NULL)
                  {
                              TestMessage ();
                              Sleep (50);
                              continue;
                  }
                  if (fgets (satz, 511, fp) == 0)
                  {
                              TestMessage ();
                              fclose (fp);
                              Sleep (50);
                              continue;
                  }
                  fclose (fp);
                  if (KorrMe (satz) == 0)
                  {
                              break;
                  }
                  TestMessage ();
                  SetFocus (WieButton.mask[3].feldid);
                  Sleep (50);
         }
}

void starte_prakt (void)
/**
Prgramm prakt starten.
**/
{
        char *tmp;
        char *bws;
        char buffer [512];

        tmp = getenv ("TMPPATH");
        if (tmp == NULL)
        {
                  tmp = "C:\\USER\\FIT\\TMP";
        }
        sprintf (buffer, "%s\\prstat", tmp);
        unlink (buffer);

        bws = getenv ("BWS");
        if (bws == NULL)
        {
                  bws = "C:\\USER\\FIT";
        }
        sprintf (buffer, "%s\\bin\\gxakt", bws);

        if (ProcExec (buffer, SW_SHOWMINNOACTIVE, -1, 0, -1, 0) == 0)
        {
                       print_messG (2, "Fehler beim Start von %s", buffer);
//                       break_ausz = 1;
        }
}

void SetAuszText (int mode)
/**
Text fuer AuszeicnenButton setzen.
**/
{
         if (mode == 0)
         {
               WieWie.text1 = "Aus-";
               WieWie.text2 = "zeichnen";
               WieWie.tx1 = -1;
               WieWie.ty1 = 25;
               WieWie.tx2 = -1;
               WieWie.ty2 = 50;
         }
         else
         {
               WieWie.text1 = "Stop";
               WieWie.text2 = "";
               WieWie.tx1 = -1;
               WieWie.ty1 = -1;
               WieWie.tx2 = -1;
               WieWie.ty2 = -1;
          }
          WieWie.aktivate = 2;
          ActivateColButton (WiegCtr, &WieButton, 3, 2, 1);
          SetFocus (WieButton.mask[3].feldid);
}

void BuInactiv (void)
/**
Alle Button auf Inaktiv setzen.
**/
{
          ActivateColButton (WiegCtr, &WieButton, 0, -1, 1);
          ActivateColButton (WiegCtr, &WieButton, 1, -1, 1);
          ActivateColButton (WiegCtr, &WieButton, 2, -1, 1);
          ActivateColButton (WiegCtr, &WieButton, 3, -1, 1);
          ActivateColButton (WiegCtr, &WieButton, 4, -1, 1);
          ActivateColButton (WiegCtr, &WieButton, 5, -1, 1);
          ActivateColButton (WiegCtr, &WieButton, 6, -1, 1);
}

void BuActiv (void)
/**
Alle Button auf Inaktiv setzen.
**/
{
          ActivateColButton (WiegCtr, &WieButton, 0, 2, 1);
          ActivateColButton (WiegCtr, &WieButton, 1, 2, 1);
          ActivateColButton (WiegCtr, &WieButton, 2, 2, 1);
          ActivateColButton (WiegCtr, &WieButton, 3, 2, 1);
          ActivateColButton (WiegCtr, &WieButton, 4, 2, 1);
          ActivateColButton (WiegCtr, &WieButton, 5, 2, 1);
          ActivateColButton (WiegCtr, &WieButton, 6, 2, 1);
}

static void FileError (void)
{

          disp_messG ("Es wurden keine Daten zum Auszeichnen gefunden\n"
                     "Pr�fen Sie bitte den Verkaufspreis\n und die Hauptwarengruppenzuordnung",
                     2);
          return;
}


BOOL KunGxOk (void)
/**
Pruefen, ob die Dateien fuer gxakt leer sind.
**/
{
        char *tmp;
        char dname [256];
        HANDLE fp;
        DWORD fsize;

        tmp = getenv ("TMPPATH");
        if (tmp == (char *) 0)
        {
                        tmp = "C:\\USER\\FIT\\TMP";
        }
        sprintf (dname, "%s\\kungx", tmp);
        fp = CreateFile (dname, GENERIC_READ, 0, NULL,
                         OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
        if (fp == INVALID_HANDLE_VALUE)
        {
                         FileError ();
                         return FALSE;
        }

        fsize = GetFileSize (fp, NULL);
        if (fsize == (DWORD) 0 ||
            fsize == 0xFFFFFFFF)
        {
                         CloseHandle (fp);
                         FileError ();
                         return FALSE;
        }

        CloseHandle (fp);
        return TRUE;
}



int doauszeichnen (void)
/**
Auftragsposition auszeichnen.
**/
{
        char buffer [512];
        char *tmp;
        double auf_me_vgl;
        double auf_me_kun;
        DWORD ex;

        akt_me = 0;
        break_ausz = 0;
        tmp = getenv ("TMPPATH");
        if (tmp == (char *) 0)
        {
                        tmp = "C:\\USER\\FIT\\TMP";
        }

        GetKunMe ();
        if (atoi (aufps.me_einh_kun) == 2)
        {
                    auf_me_kun = ratod (aufps.auf_me) * 1000;
        }
        else
        {
                    auf_me_kun = ratod (aufps.auf_me);
        }
        if (auf_me_kun <= (double) 0.0)
        {

                       disp_messG ("Menge 0 kann nicht ausgezeichnet werden", 2);
                       return (0);
        }
        if (atoi (aufps.a_me_einh) == 2)
        {
                    auf_me_vgl = ratod (aufps.auf_me_vgl) * 1000;
        }
/*
        if (auf_me_vgl <= (double) 0.0)
        {

                       disp_messG ("Menge 0 kann nicht ausgezeichnet werden", 2);
                       return (0);
        }
*/

        if (ratod (auf_me) <= (double) 0.0)
        {

                       disp_messG ("Die Sollmenge ist erreicht", 2);
                       return (0);
        }
        if (paz_direkt == 3)
        {
/*
                      RunBwsasc (auszeichner, auf_me_vgl);
                      BuInactiv ();
*/
        }
        else if (paz_direkt == 2)
        {

/*
                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungx kungx.if %ld %ld %s %.0lf %s",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr), lieferdatum, auf_me_vgl,
                          aufps.auf_lad_pr);
*/
                      sprintf (buffer, "pr_paz -dokt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungx kungx.if %ld %ld %s %.0lf %s %hd",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr), lieferdatum, auf_me_kun,
                          aufps.auf_lad_pr, kunme_einh);

                      BuInactiv ();
                      ex = ProcWaitExec (buffer, SW_SHOWNORMAL,
                                                 -1, 0, -1, 0);
                      if (ex != 0l)
                      {
                          print_messG (2,
                              "Fehler beim 1. Start von pr_paz");
                          BuActiv ();
                          return (-1);
                      }

                      sprintf (buffer, "pr_paz -odkt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungtex kungtex.if %ld %ld",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr));

                      ex = ProcWaitExec (buffer, SW_SHOWNORMAL,
                                                 -1, 0, -1, 0);
                      if (ex != 0l)
                      {
                          print_messG (2,
                              "Fehler beim 2. Start von pr_paz");
                          BuActiv ();
                          return (-1);
                      }
       }
        else if (paz_direkt == 1)
        {
/*
                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungx kungx.if %ld %ld %s %.0lf %s",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr), lieferdatum, auf_me_vgl,
                          aufps.auf_lad_pr);
*/
                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungx kungx.if %ld %ld %s %.0lf %s %hd",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr), lieferdatum, auf_me_kun,
                          aufps.auf_lad_pr, kunme_einh);


                      BuInactiv ();
                      CreateBatch (buffer);

                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungtex kungtex.if %ld %ld",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr));

                     RunBatch (buffer);
        }
        else
        {
/*
                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungx kungx.if %ld %ld %s %.0lf %s",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr), lieferdatum, auf_me_vgl,
                          aufps.auf_lad_pr);
*/
                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungx kungx.if %ld %ld %s %.0lf %s %hd",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr), lieferdatum, auf_me_kun,
                          aufps.auf_lad_pr, kunme_einh );

                      BuInactiv ();
                      CreateMessage (WiegWindow, "Die Daten werden vorbereitet");
                      ex = ProcWaitExec (buffer, SW_HIDE,
                                                 -1, 0, -1, 0);
                      if (ex == (long) -1)
                      {
                          DestroyMessage ();
                          AktivWindow = WiegWindow;
                          print_messG (2,
                              "Fehler beim 1. Start von pr_paz");
                          BuActiv ();
                          return (-1);
                      }
                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungtex kungtex.if %ld %ld",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr));

                      ex = ProcWaitExec (buffer, SW_HIDE,
                                                 -1, 0, -1, 0);
                      if (ex == (long) -1)
                      {
                          DestroyMessage ();
                          AktivWindow = WiegWindow;
                          print_messG (2,
                              "Fehler beim 2. Start von pr_paz");
                          BuActiv ();
                          return (-1);
                      }
                      DestroyMessage ();
                      AktivWindow = WiegWindow;
       }
       if (! KunGxOk ())
       {
                    BuActiv ();
                    WorkModus = AUSZEICHNEN;
                    return (-1);
        }

        WorkModus = STOP;
        WritePrcomm ("G");
        if (! break_ausz)
        {
                   SetAuszText (1);
                   starte_prakt ();
                   SetActiveWindow (WiegCtr);
        }
        if (! break_ausz)
        {
                   PollAusz ();
        }
        WritePrcomm ("E");
        SetAuszText (0);
        BuActiv ();
        WorkModus = AUSZEICHNEN;
        if (AlarmWindow)
        {
                     CloseControls (&alform);
                     DestroyWindow (AlarmWindow);
                     AlarmWindow = NULL;
        }
        return 0;
}

int StopAuszeichnen (void)
{

        GxStop = 1;
        if (abfragejnG (WiegWindow, "Auszeichnen stoppen ?", "N") == 0)
        {
            GxStop = 0;
            return 0;
        }
        WritePrcomm ("E");
        GxStop = 1;
        return 0;
}

void IncWiegZahl (void)
/**
Wieganzahl erhoehen.
**/
{
	    sprintf (wiegzahl, "%ld", atol (wiegzahl) + 1);
}

void PaintKist (HDC hdc)
/**
Kiste zeichnen.
**/
{
	    if (KistPaint == FALSE) return;

        KistMask->DrawBitmapEx (hdc, KistRect.left, KistRect.top, SRCAND);
		if (KistCol)
		{
                 KistG->DrawBitmapEx (hdc, KistRect.left, KistRect.top, SRCPAINT);
		}
		else
		{
                 Kist->DrawBitmapEx (hdc, KistRect.left, KistRect.top, SRCPAINT);
		}
}

void PaintKistBmp (void)
{
	    if (KistPaint == TRUE) return;
		if (KistCol)
		{
			KistCol = FALSE;
			SetClassLong (WiegKg, GCL_HBRBACKGROUND, (long) LtBlueBrush);
		}
		else
		{
			KistCol = TRUE;
			SetClassLong (WiegKg, GCL_HBRBACKGROUND, (long) YellowBrush);
		}
	    KistPaint = TRUE;
		MessageBeep (MB_ICONASTERISK);
		InvalidateRect (WiegWindow, &KistRect, TRUE);
		InvalidateRect (WiegKg, NULL, TRUE);
		UpdateWindow (WiegWindow);
		UpdateWindow (WiegKg);
}

void DestroyKistBmp (void)
{
	    if (KistPaint == FALSE) return;
	    KistPaint = FALSE;
		InvalidateRect (WiegWindow, &KistRect, TRUE);
		UpdateWindow (WiegWindow);
}


void SetKistRect (int x, int y)
/**
Position und Groesse fuer WQiegBitmap setzen.
**/
{
	    POINT bpoint;
		HDC hdc;

		hdc = GetDC (WiegWindow);
		Kist->BitmapSize (hdc, &bpoint);
		ReleaseDC (WiegWindow, hdc);
		KistRect.left = 50;
		KistRect.top  = y;
		KistRect.right   = KistRect.left + bpoint.x;
		KistRect.bottom  = KistRect.top  + bpoint.y;
}




int dowiegen (void)
/**
Wiegen durchfuehren.
**/
{
        double netto;
        double brutto;
        double tara;
        double alt;
        static int arganz;
        static char *args[4];
		static char WiegAnz[20];

        if (WorkModus == AUSZEICHNEN)
        {
                       return (doauszeichnen ());
        }
        else if (WorkModus == STOP)
        {
                       return (StopAuszeichnen ());
        }
		DestroyKistBmp ();

        arganz = 3;
        args[0] = "Leer";
        args[1] = "11";
        args[2] = waadir;
        fnbexec (arganz, args);
/* Test  */

/*
		Sleep (2000);
*/

/* Testende  */

        brutto = netto = tara = (double) 0.0;
        GetGewicht (ergebnis, &brutto, &netto, &tara);
//		brutto = netto = 2.0;  // TESTTESTTEST
/*
		if (aufps.hnd_gew[0] == 'H' || aufps.erf_gew_kz[0] == 'N')
		{
                SetStorno (dostorno, 0);
                sprintf (WiegAnz, "%.0lf", (double) 1);
                EnterCalcBox (WiegWindow,"  Anzahl  ",  WiegAnz, 12, "%8.3lf");
                if (syskey != KEY5)
				{
					SetEtiStk (ratod (WiegAnz));
				}
				else
				{
                    SetEtiStk ((double) 1.0);
				}
		}
		else
		{
                SetEtiStk ((double) 1.0);
		}
*/

        if (WiegeTyp == ENTNAHME && StartWiegen)
        {
                   EntnahmeStartGew = brutto;
                   EntnahmeActGew   = brutto;
                   sprintf (ActEntnahmeGew, "%lf", EntnahmeActGew);
                   display_form (WiegWindow, &fEntnahmeGew, 0, 0);
                   return 0;
        }

		if (netto <= 99999.999)
		{
                SetEti (netto, brutto, tara);
		}
        if (tara == (double) 0.0)
        {
                   strcpy (aufps.erf_kz, "W");
        }
        else
        {
                   strcpy (aufps.erf_kz, "E");
        }
		if (netto <= 99999.999)
		{
                  alt = (double) ratod (auf_me);
                  alt -= netto;
                  sprintf (WiegMe, "%11.3lf", netto);
                  sprintf (auf_me, "%3lf", alt);
                  alt = (double) ratod (aufps.lief_me);
                  alt += netto;
				  if (alt <= 99999.999)
				  {
                           sprintf (aufps.lief_me, "%3lf", alt);
				  }
		}
		if (tara <= 99999.999)
		{
                  sprintf (aufps.tara, "%.3lf", tara);
		}
        if (WiegeTyp == STANDARD && autoan)
        {
            akt_tara = brutto;
            Sleep (AutoSleep);
            settara ();
        }
        if (netto > 0.0)
        {
		    IncWiegZahl ();
        }
        display_form (WiegKg, &WiegKgE, 0, 0);
        display_form (WiegWindow, &artwform, 0, 0);
        display_form (WiegWindow, &artwconst, 0, 0);
        if (WiegeTyp == STANDARD && autoan) settara ();
		PaintKistBmp ();
        current_wieg = 3;
        if (ratod (aufps.ag) != 0.0)
        {
		          strcpy (aufps.anz_einh, wiegzahl);
                  memcpy (&aufparr [aufpidx], &aufps, sizeof (struct AUFP_S));
                  arrtolgr_umlgrp (aufpidx);
                  Lgr_umlgrpClass.dbupdate ();
        }
        commitwork ();
        beginwork ();
		if (auto_eti)
		{
                 doetikett ();
		}
//        return 0;
		if (wieg_direct && StartWiegen == FALSE)
		{
		            doWOK ();
		}
        return 0;
}

void PrintWLines ()
/**
Icons am Bildschirm anzeigen.
**/
{
         RECT rect, frect;
         PAINTSTRUCT ps;
         HDC hdc;
         HPEN hPen;
         int x, y;

         GetClientRect (WiegWindow, &rect);
         GetFrmRect (&artwform, &frect);

         hdc = BeginPaint (WiegWindow, &ps);

         hPen = CreatePen (PS_SOLID, 2, BLACKCOL);
         SelectObject (hdc, hPen);
         x = rect.right - 4;
         y = 10;
         MoveToEx (hdc, 2, y, NULL);
         LineTo (hdc, x, y);
         DeleteObject (hPen);

         hPen = CreatePen (PS_SOLID, 2, GRAYCOL);
         SelectObject (hdc, hPen);
         MoveToEx (hdc, 2, y - 1, NULL);
         LineTo (hdc, x, y - 2);
         DeleteObject (hPen);


         hPen = CreatePen (PS_SOLID, 2, BLACKCOL);
         SelectObject (hdc, hPen);
         x = rect.right - 4;
         y = frect.bottom + 25;
         alarmy = y;
         MoveToEx (hdc, 2, y, NULL);
         LineTo (hdc, x, y);
         DeleteObject (hPen);

         hPen = CreatePen (PS_SOLID, 2, GRAYCOL);
         SelectObject (hdc, hPen);
         MoveToEx (hdc, 2, y - 1, NULL);
         LineTo (hdc, x, y - 2);
         DeleteObject (hPen);

		 PaintKist (hdc);

         EndPaint (WiegWindow, &ps);
}


LONG FAR PASCAL WiegProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        switch(msg)
        {

              case WM_PAINT :
                    if (hWnd == MessWindow)
                    {
                            display_form (hWnd, &msgform, 0, 0);
                    }


                    if (hWnd == WiegKg)
                    {
                            display_form (hWnd, &WiegKgT, 0, 0);
                            display_form (hWnd, &WiegKgE, 0, 0);
                    }
                    else if (hWnd == WiegCtr)
                    {       if (CtrMode == 0)
                            {
                                    display_form (hWnd, &WieButton, 0, 0);
                                    SetFocus (
                                           WieButton.mask[current_wieg].feldid);
                            }
                            else
                            {
                                    display_form (hWnd, &WieButtonT, 0, 0);
                                    SetFocus (
                                           WieButtonT.mask[current_wieg].feldid);
                            }
                    }
                    else if (hWnd == WiegWindow)
                    {
                            display_form (hWnd, &WiegKgMe, 0, 0);
                            display_form (hWnd, &artwformT, 0, 0);
                            display_form (hWnd, &artwconstT, 0, 0);
                            if (WiegeTyp == ENTNAHME)
                            {
                                  if (StartWiegen)
                                  {
                                        display_form (hWnd, &startwieg, 0, 0);
                                  }
                                  display_form (hWnd, &fEntnahmeGewTxt, 0, 0);
                                  display_form (hWnd, &fEntnahmeGew, 0, 0);
                            }
                            display_form (hWnd, &EtiButton, 0, 0);
                            if (artwform.mask[0].feldid)
                            {
                                 display_form (hWnd, &artwform, 0, 0);
                                 display_form (hWnd, &artwconst, 0, 0);
                            }
                            display_form (hWnd, &ftara, 0, 0);
                            display_form (hWnd, &ftaratxt, 0, 0);
                            PrintWLines ();
                    }
                    else if (hWnd == AlarmWindow)
                    {
                            display_form (hWnd, &alform, 0, 0);
                    }
                    break;
             case WM_COMMAND :
                    if (LOWORD (wParam) == KEY5 ||
                        LOWORD (wParam) == VK_F5)
                    {
                            syskey = KEY5;
                            SendKey (VK_F5);
                            break;
                    }
                    if (LOWORD (wParam) == KEYCR)
                    {
                            syskey = KEYCR;
                            SendKey (VK_RETURN);
                            break;
                    }
                    if (LOWORD (wParam) == BUTARA)
                    {
                            PostMessage (WiegWindow, WM_USER, wParam, 0l);
                            break;
                    }
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}


void RegisterWieg (void)
/**
Fenster fuer Auftragskopf registrieren
**/
{
        static int registered = 0;
        WNDCLASS wc;

        if (registered == 0)
        {
                   wc.style =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                               | CS_OWNDC;
                   wc.cbClsExtra    =  0;
                   wc.cbWndExtra    =  0;
                   wc.hInstance     =  hMainInst;
                   wc.hIcon         =  LoadIcon (hMainInst, "FITICON");
                   wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                   wc.lpszMenuName  =  "";
                   wc.lpfnWndProc   =  WiegProc;
                   wc.lpszMenuName  =  "";
                   wc.hbrBackground =  CreateSolidBrush (RGB (0, 255, 255));
                   wc.lpszClassName =  "AufWieg";
                   RegisterClass(&wc);

                   // wc.hbrBackground =  CreateSolidBrush (RGB (0, 0, 120));
                   wc.hbrBackground =   GetStockObject (LTGRAY_BRUSH);
                   wc.lpszClassName =  "AufWiegCtr";
                   RegisterClass(&wc);

                   wc.lpfnWndProc   =   WiegProc;
                   wc.hbrBackground =  CreateSolidBrush (RGB (0, 0, 255));
                   wc.lpszClassName =  "AufWiegKg";
                   RegisterClass(&wc);
                   registered = 1;
        }
}

void TestEnter (void)
/**
Eingabefeld testen.
**/
{
       GetWindowText (artwform.mask[0].feldid,
                      artwform.mask[0].feld,
                      artwform.mask[0].length);
}


void SetWiegFocus (void)
/**
Focus fuer Wiegmaske setzen.
**/
{
       form *wform;

       if (CtrMode == 1)
       {
                  wform = &WieButtonT;
       }
       else
       {
                  wform = &WieButton;
       }

       if (current_wieg < etiend)
       {
                  SetFocus (wform->mask[current_wieg].feldid);
       }
       else if (current_wieg < wiegend)
	   {
                  SetFocus (EtiButton.mask[current_wieg - etiend].feldid);
	   }
       else if (artwform.mask[0].attribut != EDIT)
	   {
		          current_wieg = 0;
                  SetFocus (wform->mask[current_wieg].feldid);
	   }
	   else
       {
                  SetFocus (artwform.mask[0].feldid);
                  SendMessage (artwform.mask[0].feldid, EM_SETSEL,
                               (WPARAM) 0, MAKELONG (-1, 0));
       }
}

int ReturnActionW (void)
/**
Returntaste behandeln.
**/
{
       form *wform;
	   int current;

       if (current_wieg == wiegend)
       {
                  PostMessage (WiegWindow, WM_KEYDOWN, VK_DOWN, 0l);
                  return TRUE;
       }

	   if (current_wieg >= etiend)
	   {
	              wform = &EtiButton;
				  current = current_wieg - etiend;
	   }

       else if (CtrMode == 1)
       {
                  wform = &WieButtonT;
				  current = current_wieg;
       }
       else
       {
                  wform = &WieButton;
				  current = current_wieg;
       }

       if (CtrMode == 0 && current == autopos)
       {
                        PressAuto ();
       }
       else if (wform->mask[current].after)
       {
                       (*wform->mask[current].after) ();
       }
       else if (wform->mask[current].BuId)
       {
                       SendKey
                                 (wform->mask[current].BuId);
       }
       return TRUE;
}

int TestAktivButton (int sign, int current_wieg)
/**
Status von Colbutton pruefen.
**/
{
       ColButton *Cub;
       int run;
       form *wform, *wform0;;
	   int current;

       if (CtrMode == 1)
       {
                  wform0= &WieButtonT;
       }
       else
       {
                  wform0= &WieButton;
       }

       run = 1;
       while (TRUE)
       {
              if (current_wieg >= wform0->fieldanz + EtiButton.fieldanz) break;
   	          if (current_wieg >= etiend)
			  {
				  wform = &EtiButton;
				  current = current_wieg - etiend;
			  }
			  else
			  {
				  wform = wform0;
				  current = current_wieg;
			  }
              Cub = (ColButton *) wform->mask[current].feld;
              if (((wform->mask[current].attribut & REMOVED) == 0) &&
				   (Cub->aktivate != -1)) break;
			  current_wieg += sign;
              if (sign == PLUS && current_wieg > wiegend)
              {
                                if (run == 2) break;
                                current_wieg = 0;
                                run ++;
              }
              else if (sign == MINUS && current_wieg < 0)
              {
                                if (run == 2) break;
                                current_wieg = wiegend;
                                run ++;
              }
       }
       return (current_wieg);
}


int IsWiegMessage (MSG *msg)
/**
HotKey Testen.
**/
{
         POINT mousepos;

         switch (msg->message)
         {
              case WM_KEYDOWN :
              {
                     TestEnter ();
                     switch (msg->wParam)
                     {
                            case VK_F5 :
                                    if (CtrMode == 1)
                                    {
                                          closetara ();
                                    }
                                    else
                                    {
                                          break_wieg = 1;
                                    }
                                    return TRUE;
                            case VK_RETURN :
                                    return (ReturnActionW ());
                            case VK_DOWN :
                            case VK_RIGHT :
                                    current_wieg ++;
                                    if (current_wieg > wiegend)
                                                current_wieg = 0;
                                    current_wieg = TestAktivButton (PLUS, current_wieg);
                                    SetWiegFocus ();
                                    return TRUE;
                            case VK_UP :
                            case VK_LEFT :
                                   current_wieg --;
                                   if (current_wieg < 0)
                                                current_wieg = wiegend;
                                    current_wieg = TestAktivButton (MINUS, current_wieg);
                                    SetWiegFocus ();
                                    return TRUE;
                            case VK_TAB :
                                    if (GetKeyState (VK_SHIFT) < 0)
                                    {
                                             current_wieg --;
                                             if (current_wieg < 0)
                                                  current_wieg = wiegend;
                                             current_wieg =
                                                 TestAktivButton (MINUS, current_wieg);
                                     }
                                     else
                                     {
                                             current_wieg ++;
                                             if (current_wieg > wiegend)
                                                  current_wieg = 0;
                                             current_wieg =
                                                 TestAktivButton (PLUS, current_wieg);
                                     }
                                     SetWiegFocus ();
                                     return TRUE;
                            default :
                                    return FALSE;
                     }
                     break;
              }
              case WM_LBUTTONDOWN :
              {
                    GetCursorPos (&mousepos);

                    if (MouseinWindow (artwform.mask[0].feldid, &mousepos) &&
						artwform.mask[0].attribut == EDIT)
                    {
                          EnterNumBox (WiegWindow,"  Chargen-Nr  ",
                                       aufps.ls_charge, 17,
                                       artwform.mask[0].picture);
                          display_form (WiegWindow, &artwform, 0, 0);
                          current_wieg = TestAktivButton (PLUS, current_wieg);
                          SetWiegFocus ();
                          return TRUE;
                    }
              }
              case WM_USER :
                    if (LOWORD (msg->wParam) == BUTARA)
                    {
                          dotara ();
                          return TRUE;
                    }
         }
         return FALSE;
}

void PosArtFormW (void)
/**
Position fuer artform festlegen.
**/
{
        static int posOK = 0;
        int i, s, z;


        if (posOK) return;

        posOK = 1;
        for (i = 0; i < artwformT.fieldanz; i ++)
        {
                    s = artwformT.mask[i].pos [1];
                    z = artwformT.mask[i].pos [0];
                    KonvTextAbsPos (artwform.font,
                                    &z, &s);
                    artwformT.mask[i].pos [1] = s;
                    artwformT.mask[i].pos [0] = z;
                    s = artwformT.mask[i].length;
                    z = artwformT.mask[i].rows;
                    KonvTextAbsPos (artwform.font,
                                    &z, &s);
                    artwformT.mask[i].length = s;
                    artwformT.mask[i].rows = z;
        }
        for (i = 0; i < artwform.fieldanz; i ++)
        {
                    s = artwform.mask[i].pos [1];
                    z = artwform.mask[i].pos [0];
                    KonvTextAbsPos (artwform.font,
                                    &z, &s);
                    artwform.mask[i].pos [1] = s;
                    artwform.mask[i].pos [0] = z;
                    s = artwform.mask[i].length;
                    z = artwform.mask[i].rows;
                    KonvTextAbsPos (artwform.font,
                                    &z, &s);
                    artwform.mask[i].length = s;
                    artwform.mask[i].rows = z;
        }
        for (i = 0; i < fEntnahmeGewTxt.fieldanz; i ++)
        {
                    s = fEntnahmeGewTxt.mask[i].pos [1];
                    z = fEntnahmeGewTxt.mask[i].pos [0];
                    KonvTextAbsPos (fEntnahmeGew.font,
                                    &z, &s);
                    fEntnahmeGewTxt.mask[i].pos [1] = s;
                    fEntnahmeGewTxt.mask[i].pos [0] = z;
                    s = fEntnahmeGewTxt.mask[i].length;
                    z = fEntnahmeGewTxt.mask[i].rows;
                    KonvTextAbsPos (fEntnahmeGew.font,
                                    &z, &s);
                    fEntnahmeGewTxt.mask[i].length = s;
                    fEntnahmeGewTxt.mask[i].rows = z;
        }
        for (i = 0; i < fEntnahmeGew.fieldanz; i ++)
        {
                    s = fEntnahmeGew.mask[i].pos [1];
                    z = fEntnahmeGew.mask[i].pos [0];
                    KonvTextAbsPos (fEntnahmeGew.font,
                                    &z, &s);
                    fEntnahmeGew.mask[i].pos [1] = s;
                    fEntnahmeGew.mask[i].pos [0] = z;
                    s = fEntnahmeGew.mask[i].length;
                    z = fEntnahmeGew.mask[i].rows;
                    KonvTextAbsPos (fEntnahmeGew.font,
                                    &z, &s);
                    fEntnahmeGew.mask[i].length = s;
                    fEntnahmeGew.mask[i].rows = z;
        }
}

void PosArtFormC (void)
/**
Position fuer artform festlegen.
**/
{
        static int posOK = 0;
        int i, s, z;

        if (posOK) return;

        posOK = 1;
        for (i = 0; i < artwconstT.fieldanz; i ++)
        {
                    s = artwconstT.mask[i].pos [1];
                    z = artwconstT.mask[i].pos [0];
                    KonvTextAbsPos (artwconst.font,
                                    &z, &s);
                    artwconstT.mask[i].pos [1] = s;
                    artwconstT.mask[i].pos [0] = z;
                    s = artwconstT.mask[i].length;
                    z = artwconstT.mask[i].rows;
                    KonvTextAbsPos (artwconst.font,
                                    &z, &s);
                    artwconstT.mask[i].length = s;
                    artwconstT.mask[i].rows = z;
        }
        for (i = 0; i < startwieg.fieldanz; i ++)
        {
                    s = startwieg.mask[i].pos [1];
                    z = startwieg.mask[i].pos [0];
                    KonvTextAbsPos (startwieg.font,
                                    &z, &s);
                    startwieg.mask[i].pos [1] = s;
                    startwieg.mask[i].pos [0] = z;
                    s = startwieg.mask[i].length;
                    z = startwieg.mask[i].rows;
                    KonvTextAbsPos (startwieg.font,
                                    &z, &s);
                    startwieg.mask[i].length = s;
                    startwieg.mask[i].rows = z;
        }
        for (i = 0; i < artwconst.fieldanz; i ++)
        {
                    s = artwconst.mask[i].pos [1];
                    z = artwconst.mask[i].pos [0];
                    KonvTextAbsPos (artwconst.font,
                                    &z, &s);
                    artwconst.mask[i].pos [1] = s;
                    artwconst.mask[i].pos [0] = z;
                    s = artwconst.mask[i].length;
                    z = artwconst.mask[i].rows;
                    KonvTextAbsPos (artwconst.font,
                                    &z, &s);
                    artwconst.mask[i].length = s;
                    artwconst.mask[i].rows = z;
        }
}

void PosArtFormP (void)
/**
Position fuer artform festlegen.
**/
{
        static int posOK = 0;
        int i, s, z;

        if (posOK) return;

        posOK = 1;
        for (i = 0; i < artwformT.fieldanz; i ++)
        {
                    s = artwformT.mask[i].pos [1];
                    z = artwformT.mask[i].pos [0];
                    KonvTextAbsPos (artwform.font,
                                    &z, &s);
                    artwformT.mask[i].pos [1] = s;
                    artwformT.mask[i].pos [0] = z;
                    s = artwformT.mask[i].length;
                    z = artwformT.mask[i].rows;
                    KonvTextAbsPos (artwform.font,
                                    &z, &s);
                    artwformT.mask[i].length = s;
                    artwformT.mask[i].rows = z;
        }
        for (i = 0; i < artwform.fieldanz; i ++)
        {
                    s = artwform.mask[i].pos [1];
                    z = artwform.mask[i].pos [0];
                    KonvTextAbsPos (artwform.font,
                                    &z, &s);
                    artwform.mask[i].pos [1] = s;
                    artwform.mask[i].pos [0] = z;
                    s = artwform.mask[i].length;
                    z = artwform.mask[i].rows;
                    KonvTextAbsPos (artwform.font,
                                    &z, &s);
                    artwform.mask[i].length = s;
                    artwform.mask[i].rows = z;
        }
        for (i = 0; i < fEntnahmeGewTxt.fieldanz; i ++)
        {
                    s = fEntnahmeGewTxt.mask[i].pos [1];
                    z = fEntnahmeGewTxt.mask[i].pos [0];
                    KonvTextAbsPos (fEntnahmeGewTxt.font,
                                    &z, &s);
                    fEntnahmeGewTxt.mask[i].pos [1] = s;
                    fEntnahmeGewTxt.mask[i].pos [0] = z;
                    s = fEntnahmeGewTxt.mask[i].length;
                    z = fEntnahmeGewTxt.mask[i].rows;
                    KonvTextAbsPos (fEntnahmeGewTxt.font,
                                    &z, &s);
                    fEntnahmeGewTxt.mask[i].length = s;
                    fEntnahmeGewTxt.mask[i].rows = z;
        }
        for (i = 0; i < fEntnahmeGew.fieldanz; i ++)
        {
                    s = fEntnahmeGew.mask[i].pos [1];
                    z = fEntnahmeGew.mask[i].pos [0];
                    KonvTextAbsPos (fEntnahmeGew.font,
                                    &z, &s);
                    fEntnahmeGew.mask[i].pos [1] = s;
                    fEntnahmeGew.mask[i].pos [0] = z;
                    s = fEntnahmeGew.mask[i].length;
                    z = fEntnahmeGew.mask[i].rows;
                    KonvTextAbsPos (fEntnahmeGewTxt.font,
                                    &z, &s);
                    fEntnahmeGew.mask[i].length = s;
                    fEntnahmeGew.mask[i].rows = z;
        }
}

void PosMeForm (int yw, int xw, int cyw, int cxw)
/**
Position fuer WiegKgMe festlegen.
**/
{
        static int posOK = 0;
        int s, z;

        if (posOK) return;

        posOK = 1;

        WiegKgMe.mask[0].pos[0] = yw + WiegKgE.mask[0].pos[0];
        WiegKgMe.mask[0].pos[1] = xw + cxw + 30;
        s = WiegKgMe.mask[0].length;
        z = WiegKgMe.mask[0].rows;
        KonvTextAbsPos (WiegKgMe.font, &z, &s);
        WiegKgMe.mask[0].length = s;
        WiegKgMe.mask[0].rows = z;
}

void GetZentrierWerte (int *buglen, int *buabs, int wlen, int anz,
                       int abs, int size)
/**
Werte fuer Zentrierung ermitteln.
**/
{
        int blen;
        int spos;

        while (TRUE)
        {
                 blen = anz * (size + abs) - abs;
                 spos = (wlen - blen) / 2;
                 if (spos > 9)
                 {
                              *buglen = blen;
                              *buabs  = abs + size;
                              return;
                 }
                 abs -= 10;
                 if (abs <= 0) return;
       }
}

int closetara (void)
/**
Tara-Leiste schliessen.
**/
{
        CtrMode = 0;
        CloseControls (&WieButtonT);
        current_wieg = 0;
        if (ohne_preis)
        {
                 wiegend = 7;
        }
        else
        {
                 wiegend = 8;
        }
        return 0;
}

void testeti (void)
/**
Wiegen durchfuehren.
**/
{
		double a;
//		int dsqlstatus;

		a = ratod (aufps.a);
		WithEti = TRUE;
		WithSumEti = FALSE;

/***
		DbClass.sqlin ((long *)   &sys_peri.sys, 2, 0);
		DbClass.sqlin ((double *) &a, 3, 0);
        DbClass.sqlout ((long *) &eti_nr, 2, 0);
        DbClass.sqlout ((char *) krz_txt, 0, 17);
		dsqlstatus = DbClass.sqlcomm ("select eti_nr, krz_txt from klebdest "
			                          "where sys = ? "
									  "and a = ? "
									  "and we = 1 "
									  "and einz = 1");
		if (dsqlstatus)
		{
			WithEti = FALSE;
		}
		else
		{
			WithEti = TRUE;
		}

		DbClass.sqlin ((long *)   &sys_peri.sys, 2, 0);
		DbClass.sqlin ((double *) &a, 3, 0);
        DbClass.sqlout ((long *) &sum_eti_nr, 2, 0);
        DbClass.sqlout ((char *) sum_krz_txt, 0, 17);
		dsqlstatus = DbClass.sqlcomm ("select eti_nr, krz_txt from klebdest "
			                          "where sys = ? "
									  "and a = ? "
									  "and we = 1 "
									  "and summ = 1");
		if (dsqlstatus)
		{
			WithSumEti = FALSE;
		}
		else
		{
			WithSumEti = TRUE;
		}
*/   
		if (WithEti == FALSE && WithSumEti == FALSE)
		{
			EtiButton.mask[0].attribut = REMOVED;
			EtiButton.mask[1].attribut = REMOVED;
			wiegend = 9;
		}
		else if (WithSumEti == FALSE)
		{
			EtiButton.mask[0].attribut = COLBUTTON;
			EtiButton.mask[1].attribut = REMOVED;
			wiegend = 9;
		}
		else 
		{
			EtiButton.mask[0].attribut = COLBUTTON;
			EtiButton.mask[1].attribut = COLBUTTON;
			wiegend = 9;
		}

/*
		wiegend = 6;
		if (WithEti) wiegend ++;
		if (WithSumEti) wiegend ++;
		if (WithSpezEti) wiegend ++;
		if (WithZusatz) wiegend ++;
*/
		if (WithEti)
		{
            ActivateColButton (NULL, &EtiButton, 0, 2, 0);
		}
		else
		{
            ActivateColButton (NULL, &EtiButton, 0, -1, 0);
		}

		if (WithSumEti)
		{
            ActivateColButton (NULL, &EtiButton, 1, 2, 0);
		}
		else
		{
            ActivateColButton (NULL, &EtiButton, 1, -1, 0);
		}
}

void WriteParafile (char *tmp)
/**
Parameter-Datei fuer put_eti schreiben.
**/
{
	    char dname [512];
		FILE *pa;

		sprintf (dname, "%s\\parafile", tmp);
		pa = fopen (dname, "w");
		if (pa == NULL) return;
		fprintf (pa, "$$LIEF;%s\n",      we_kopf.lief);
		fprintf (pa, "$$LIEFNR;%s\n",    we_kopf.lief_rech_nr);
		fprintf (pa, "$$ABEZ1;%s\n",     aufps.a_bz1);
		fprintf (pa, "$$ABEZ2;%s\n",     aufps.a_bz2);
		fprintf (pa, "$$ARTNR;%.0lf\n",  ratod (aufps.a));
		fprintf (pa, "$$WEDAT;%s\n",     lieferdatum);
		fprintf (pa, "$$NETTOG;%.3lf\n",  eti_netto);
		fprintf (pa, "$$TARAG;%.3lf\n",   eti_tara);
		fprintf (pa, "$$BRUTTOG;%.3lf\n", eti_brutto);
		fprintf (pa, "$$STUECK;%.2lf\n",  eti_stk );
		fprintf (pa, "$$LIEFNAME;%s\n",   lief_krz1);
		fprintf (pa, "$$CHARGE;%s\n",     aufps.ls_charge);
		fprintf (pa, "$$MHD;%s\n",        aufps.hbk_date);
		fclose (pa);

}

void WriteSumParafile (char *tmp)
/**
Parameter-Datei fuer put_eti schreiben.
**/
{
	    char dname [512];
		FILE *pa;

		sprintf (dname, "%s\\parafile", tmp);
		pa = fopen (dname, "w");
		if (pa == NULL) return;
		fprintf (pa, "$$LIEF;%s\n",      we_kopf.lief);
		fprintf (pa, "$$LIEFNR;%s\n",    we_kopf.lief_rech_nr);
		fprintf (pa, "$$ABEZ1;%s\n",     aufps.a_bz1);
		fprintf (pa, "$$ABEZ2;%s\n",     aufps.a_bz2);
		fprintf (pa, "$$ARTNR;%.0lf\n",  ratod (aufps.a));
		fprintf (pa, "$$WEDAT;%s\n",     lieferdatum);
		fprintf (pa, "$$NETTOG;%.3lf\n",  sum_eti_netto);
		fprintf (pa, "$$TARAG;%.3lf\n",   sum_eti_tara);
		fprintf (pa, "$$BRUTTOG;%.3lf\n", sum_eti_brutto);
		fprintf (pa, "$$STUECK;%.2lf\n",  sum_eti_stk );
		fprintf (pa, "$$ANZAHL;%ld\n",    atol (wiegzahl));
		fprintf (pa, "$$LIEFNAME;%s\n",   lief_krz1);
		fprintf (pa, "$$CHARGE;%s\n",     aufps.ls_charge);
		fprintf (pa, "$$MHD;%s\n",        aufps.hbk_date);
		fclose (pa);

/*
        sum_eti_netto  = (double) 0.0 ;
        sum_eti_brutto = (double) 0.0;
		sum_eti_tara   = (double) 0.0;
		sum_eti_stk    = (double) 0.0;
*/
}

static char EtiPath [512];

int doetikett (void)
/**
Wiegen durchfuehren.
**/
{
		char *etc;
		int etianz;
		char *tmp;
	    char dname [512];
		FILE *pa;

        char anz [20];

		syskey = 0;
		sprintf (anz, "%8d", 1);


        DestroyKistBmp ();
        if (WithEti == FALSE) return 0;


		tmp = getenv ("TMPPATH");
		if (tmp == NULL) return 0;
		etc = getenv ("BWSETC");
		if (etc == NULL) return 0;


        adr_class.lese_adr (lief.adr);

		sprintf (dname, "%s\\parafile", tmp);
		pa = fopen (dname, "w");
		if (pa == NULL) return 0;
/***
		fprintf (pa, "$$ABEZ1;%s\n",     aufps.a_bz1); 
		fprintf (pa, "$$ABEZ2;%s\n",     aufps.a_bz2);
		fprintf (pa, "$$ARTNR;%.0lf\n",  ratod (aufps.a));
		fprintf (pa, "$$WEDAT;%s\n",     lieferdatum);
		fprintf (pa, "$$NETTOG;%.3lf\n",  eti_netto);
		fprintf (pa, "$$TARAG;%.3lf\n",   eti_tara);
		fprintf (pa, "$$BRUTTOG;%.3lf\n", eti_brutto);
		fprintf (pa, "$$STUECK;%.2lf\n",  eti_stk );
		fprintf (pa, "$$CHARGE;%s\n",     aufps.ls_charge);
		fprintf (pa, "$$MHD;%s\n",        aufps.hbk_date);
		fprintf (pa, "$$ANZAHL;%d\n",     etianz);
		fprintf (pa, "$$WIEGANZ;%ld\n",  atol (wiegzahl));
		fprintf (pa, "$$LIEF;%s\n",      ". ");
		fprintf (pa, "$$LIEFNR;%s\n",    ". ");
		fprintf (pa, "$$LIEFNAME;%s\n",   ". ");
		fprintf (pa, "$$NAME1;%s\n",      ". ");
		fprintf (pa, "$$NAME2;%s\n",      ". ");
		fprintf (pa, "$$STRASSE;%s\n",    ". ");
		fprintf (pa, "$$PLZ;%s\n",        ". ");
		fprintf (pa, "$$ORT;%s\n",        ". ");
**/
		fprintf (pa, "$$CHARGE;%s\n",     aufps.ls_charge);
		fprintf (pa, "$$DATUM;%s\n",     lieferdatum);
		fprintf (pa, "$$ABEZ1;%s\n",     aufps.a_bz1); 
		fprintf (pa, "$$ABEZ2;%s\n",     aufps.a_bz2);
		fprintf (pa, "$$ARTNR;%.0lf\n",  ratod (aufps.a));

		fclose (pa);

		etianz = 1;
		if (EnterEtiAnz)
		{
		        EnterCalcBox (WiegWindow,"  Anzahl  ",  anz, 12, "%8d");
		}
		etianz = atoi(anz);

/**
        sprintf (progname, "%s\\BIN\\rswrun put_eti %s %s\\eti%04d.prm %s\\parafile %d",
                                     rosipath, krz_txt, etc, eti_nr, tmp, etianz);
        ProcWaitExec (progname, SW_SHOWMINIMIZED, 0, 40, -1, 0);
**/
        sprintf (EtiPath, "%s\\%s", etc, eti_artikel);

        int ret = PutEti (eti_a_drk, EtiPath, dname, etianz, eti_a_typ) ;
        if (ret == -1)
        {
            disp_messG ("Etikett kann nicht gedruckt werden", 2);
        }

        return 0;
}

int dosumeti (void)
/**
Wiegen durchfuehren.
**/
{
	    char progname [256];
		char *etc;
		int etianz;
		char *tmp;


        DestroyKistBmp ();
        if (WithSumEti == FALSE) return 0;

		etc = getenv ("BWSETC");
		if (etc == NULL) etc = "c:\\user\\fit\\etc";

		tmp = getenv ("TMPPATH");
		if (tmp == NULL) tmp = "c:\\user\\fit\\tmp";

		WriteSumParafile (tmp);

		etianz = 1;
        sprintf (progname, "%s\\BIN\\rswrun put_eti %s %s\\eti%04d.prm %s\\parafile %d",
                                     rosipath, sum_krz_txt, etc, sum_eti_nr, tmp, etianz);
        ProcWaitExec (progname, SW_SHOWMINIMIZED, 0, 40, -1, 0);

        return 0;
}

int dotara (void)
/**
Wiegen durchfuehren.
**/
{
		DestroyKistBmp ();
        if (WieTara.aktivate == -1) return 0;

        if (WorkModus == AUSZEICHNEN)
        {
                        dogebinde ();
                        return (0);
        }
        CtrMode = 1;
        CloseControls (&WieButton);
        current_wieg = 0;
        CtrMode = 1;
        wiegend = 8;
        return 0;
}


int doanzahl (void)
/**
Stueckeingabe durchfuehren.
**/
{
		static char WiegAnz[20];

        SetStorno (NULL, 0);
        last_anz = 0l;
        sprintf (WiegAnz, "%.0lf", (double) 1);
        EnterCalcBox (WiegWindow,"  Anzahl  ",  WiegAnz, 12, "%8.0lf");
        if (syskey != KEY5)
		{
					SetEtiStk (ratod (WiegAnz));
		}
  	    else
		{
                    SetEtiStk ((double) 1.0);
					strcpy (WiegAnz, "1");
		}

        display_form (WiegKg, &WiegKgE, 0, 0);
        display_form (WiegWindow, &artwform, 0, 0);
        display_form (WiegWindow, &artwconst, 0, 0);
        if (autoan) settara ();
        current_wieg = 1;
		last_anz = atol (WiegAnz);
//		sprintf (aufps.anz_einh, "%.3lf", ratod (aufps.anz_einh) + ratod (WiegAnz));
//		last_anz = atol (aufps.anz_einh);
		sprintf (aufps.anz_einh, "%.3lf", ratod (WiegAnz));
		strcpy (wiegzahl, aufps.anz_einh);
        memcpy (&aufparr [aufpidx], &aufps, sizeof (struct AUFP_S));
        arrtolgr_umlgrp (aufpidx);
        Lgr_umlgrpClass.dbupdate ();
        commitwork ();
        beginwork ();
        return 0;
}

int dopreis (void)
/**
Wiegen durchfuehren.
**/
{
        HWND fhWnd;

		DestroyKistBmp ();
		if (strcmp (WiePreis.text1, "Anzahl") == 0)
		{
			return doanzahl ();
		}

        fhWnd = GetFocus ();
        EnterPreisBox (WiegWindow, -1, WiegY - 86);
        SetFocus (fhWnd);
        return 0;
}

void ArtikelHand (void)
/**
Artikel-Menge von Hand eingeben.
**/
{
        double alt;
        double netto;

        StornoSet = 0;
        SetStorno (dostorno, 0);
        sprintf (WiegMe, "%.3lf", (double) 0);
        EnterCalcBox (eWindow,"  Anzahl  ",  WiegMe, 12, WiegKgE.mask[0].picture);
        if (syskey != KEY5)
        {
              strcpy (aufps.erf_kz, "H");
              if (StornoSet)
              {
                  stornoaction ();
              }
              else
              {
                  netto = (double) ratod (WiegMe);
                  alt = (double) ratod (auf_me);
                  alt -= netto;
                  sprintf (auf_me, "%3lf", alt);
                  alt = (double) ratod (aufps.lief_me);
                  alt += netto;
                  sprintf (aufps.lief_me, "%3lf", alt);
                  strcpy (aufps.s, "3");
                  SetEtiStk (ratod (WiegMe));
				  SetEti ((double) 0.0, (double) 0.0, (double) 0.0);
              }
        }
}

char *GenCharge (void)
/**
Chargen-Nummer generieren.
**/
{

	    static char cha [21];

		char datum [12];
		long datl;
		int wk,wt;
		char yy [5];
		char ww [3];
		char cwt [3];


		datl = dasc_to_long (lieferdatum); 
	//	datl += 2;
		dlong_to_asc (datl, datum); 
		strcpy (yy, &datum[8]);
		wk = get_woche (datum);
		wt = get_wochentag (datum);
		sprintf (ww, "%02d", wk);
		sprintf (cwt, "%02d", wt);
		sprintf (cha, "%s%s%s%05.0lf",yy,ww,cwt,ratod(aufps.a));
		return cha;
}


void TestCharge (void)
/**
Parameter fuer die Chargennummer bearbeiten.
**/
{

	    if (ls_charge_par == 0 || lsc2_par == 0)
		{
			return;
		}
		if (_a_bas.charg_hand == 0)
		{
			    artwformW.mask[0].attribut = READONLY;
			    artwformP.mask[0].attribut = READONLY;
				if (strcmp (aufps.ls_charge, " ") <= 0)
				{
					strcpy (aufps.ls_charge,last_charge);
				}
		}
		else if (_a_bas.charg_hand == 1)
		{
			    artwformW.mask[0].attribut = EDIT;
			    artwformP.mask[0].attribut = EDIT;
				if (strcmp (aufps.ls_charge, " ") <= 0)
				{
					strcpy (aufps.ls_charge,last_charge);
				}
		}
		else if (_a_bas.charg_hand == 2)
		{
			    artwformW.mask[0].attribut = READONLY;
			    artwformP.mask[0].attribut = READONLY;
				strcpy (aufps.ls_charge, GenCharge ());
		}
		else if (_a_bas.charg_hand == 3)
		{
			    artwformW.mask[0].attribut = READONLY;
			    artwformP.mask[0].attribut = READONLY;
				strcpy (aufps.ls_charge, "");
		}
}

void ArtikelWiegen (HWND hWnd)
/**
Artikel wiegen.
**/
{
        RECT rect;
        MSG msg;
        int x, y, cx, cy;
        int xw, yw, cxw, cyw;
        int xc, yc, cxc, cyc;
        int wlen, wrows;
        int buglen, buabs;
        int z, s;
        static int first = 0;

        if (TestQsPosA (ratod (aufps.a)) == FALSE)
        {
            return;
        }
        if (WiegeTyp == ENTNAHME && StartWiegen == FALSE &&
            EntnahmeActGew == 0.0)
        {
            disp_messG ("Es wurde keine Startwiegung durchgef�hrt", 2);
            return;
        }
        last_anz = 0l;
        if (WiegenAktiv) return;

        if (StartWiegen)
        {
            EntnahmeStartGew = EntnahmeActGew = 0.0;
            memcpy (&SetTara, &SetTara2, sizeof (SetTara));
        }
        else if (WiegeTyp == STANDARD)
        {
            EntnahmeStartGew = EntnahmeActGew = 0.0;
            memcpy (&SetTara, &SetTara1, sizeof (SetTara));
        }
        else
        {
            deltaraw ();
            akt_tara = EntnahmeTara;
            sprintf (aufps.tara, "%.3lf", akt_tara);
            memcpy (&SetTara, &SetTara2, sizeof (SetTara));
//            sprintf (ftara, "%.3lf", akt_tara);
        }


        if (strcmp (aufps.hnd_gew, "G") == 0 || strcmp (aufps.erf_gew_kz, "J") == 0 ||
            WiegeTyp == ENTNAHME)
		{
          ActivateColButton (WiegCtr, &WieButton, 3, 2, 0);
          ActivateColButton (WiegCtr, &WieButton, 0, 2, 0);
		}
		else
		{
          ActivateColButton (WiegCtr, &WieButton, 3, -1, 0);
          ActivateColButton (WiegCtr, &WieButton, 0, -1, 0);
		}
		if (ratod (aufps.auf_me) == (double) 0.0)
		{
			artwformTW.mask[1].attribut = REMOVED;
			artwformW.mask[1].attribut = REMOVED;
		}
		else
		{
			artwformTW.mask[1].attribut = DISPLAYONLY;
			artwformW.mask[1].attribut  = READONLY;
		}

		strcpy (wiegzahl, aufps.anz_einh);
        sum_eti_netto  = (double) 0.0 ;
        sum_eti_brutto = (double) 0.0;
		sum_eti_tara   = (double) 0.0;
		sum_eti_stk    = (double) 0.0;
        SwitchLiefAuf ();
		TestCharge ();
		testeti ();
        if (WorkModus == AUSZEICHNEN)
        {
//               WieWie.text1 = "Auszeichn.";
               strcpy (summe1, "0");
               strcpy (summe2, "0");
               strcpy (summe3, "0");
               strcpy (zsumme1, "0");
               strcpy (zsumme2, "0");
               strcpy (zsumme3, "0");
               WieWie.text1 = "Aus-";
               WieWie.text2 = "zeichnen";
               WieWie.tx1 = -1;
               WieWie.ty1 = 25;
               WieWie.tx2 = -1;
               WieWie.ty2 = 50;
//               WieTara.aktivate = -1;
               WieTara.text1 = "Gebinde";
               WieTara.text2 = "Tara";
               WieTara.tx1 = -1;
               WieTara.ty1 = 25;
               WieTara.tx2 = -1;
               WieTara.ty2 = 50;
               fetch_std_geb ();
               memcpy (&artwformT,&artwformTP, sizeof (form));
               memcpy (&artwform,&artwformP, sizeof (form));
               PosArtFormP ();
        }
        else
        {
               WieWie.text1 = "Wiegen";
               WieWie.text2 = NULL;
               WieWie.tx1 = -1;
               WieWie.ty1 = -1;
               WieWie.tx2 = 0;
               WieWie.ty2 = 0;
//               WieTara.aktivate = 2;
               WieTara.text1 = "Tara";
               WieTara.text2 = NULL;
               WieTara.tx1 = -1;
               WieTara.ty1 = -1;
               WieTara.tx2 = 0;
               WieTara.ty2 = 0;
               memcpy (&artwformT,&artwformTW, sizeof (form));
               memcpy (&artwform,&artwformW, sizeof (form));
               PosArtFormW ();
        }

		/*
        if (aufps.hnd_gew[0] == 'H' || aufps.erf_gew_kz[0] == 'N')
		{
               WiePreis.text1 = "Anzahl";
		}
		else
		{
               WiePreis.text1 = "Preise";
		}
		*/
               WiePreis.text1 = "Charge";

        WieButton.mask[1].BuId = 0;
        CtrMode = 0;

        if (WiegeTyp == ENTNAHME)
        {
               AutoTara.text3 = "";
               AutoTara.aktivate = -1;
        }
        else
        {
               AutoTara.text3 = anaus [autoan];
               if (autoan)
               {
                      AutoTara.aktivate = 4;
               }
               else
               {
                      AutoTara.aktivate = 3;
               }
        }
        SetListEWindow (0);
        strcpy (WiegMe, "0.000");
        sprintf (summe1, "%.0lf", (double) 0);
        sprintf (summe2, "%.0lf", (double) 0);
        sprintf (zsumme1, "0");
        sprintf (zsumme2, "0");
        AktWiegPos = 0;
        RegisterWieg ();
        GetWindowRect (hMainWindow, &rect);
        x = rect.left;
        y = rect.top + 24;
        cx = rect.right - rect.left;
        cy = rect.bottom - rect.top - 24;

        PosArtFormC ();
        wlen = strlen (WiegText);
        FontTextLen (WiegKgT.font, WiegText, &wlen,
                                             &wrows);
        WiegKgT.mask[0].length = wlen;
        WiegKgT.mask[0].rows   = wrows;

        WiegKgE.mask[0].pos[0]  = WiegKgT.mask[0].pos[0] + 2 * wrows;

        wlen = 11;
        FontTextLen (WiegKgE.font, "0000000,000", &wlen,
                                                  &wrows);

        WiegKgE.mask[0].length = wlen;
        WiegKgE.mask[0].rows   = wrows;

        if (wlen > WiegKgT.mask[0].length)
        {
                   cxw = wlen;
        }
        else
        {
                   cxw = WiegKgT.mask[0].length;
        }

        cxw += 20;

        cyw = WiegKgE.mask[0].pos[0] + wrows + wrows / 2 + 10;
        xw = (cx - cxw) / 2;
        yw = (cy - cxw) / 2 + 120;

        PosMeForm (yw, xw, cyw, cxw);

        if (first == 0)
        {
                  ftaratxt.mask[0].pos[1] = xw;
                  ftaratxt.mask[0].pos[0] = yw - 40;

                  z = yw - 40;
                  s = xw;
                  GetTextPos (ftaratxt.font, &z, &s);
                  s += 7;
                  KonvTextAbsPos (ftaratxt.font, &z, &s);
                  ftara.mask[0].pos[0] = z + 10;
                  ftara.mask[0].pos[1] = s;

                  s = strlen (ftaratxt.mask[0].feld);
                  FontTextLen (ftaratxt.font, ftaratxt.mask[0].feld, &s, &z);
                  ftaratxt.mask[0].length = s;
                  ftaratxt.mask[0].rows   = z;

                  s = 8;
                  FontTextLen (ftara.font, "0000.000", &s, &z);
                  ftara.mask[0].length = s;
                  ftara.mask[0].rows   = z;
                  first = 1;
        }

        WiegWindow  = CreateWindowEx (
                              0,
                              "AufWieg",
                              "",
                              WS_POPUP,
                              x, y,
                              cx, cy,
                              // hMainWindow,
                              hWnd,
                              NULL,
                              hMainInst,
                              NULL);
        if (WiegWindow == NULL) return;
        EnableWindow (hMainWindow, FALSE);

        WiegY = yw + cyw;
        wiegx  = xw;
        wiegy  = yw;
        wiegcx = cxw;
        wiegcy = cyw;

        WiegKg  = CreateWindowEx (
                              0,
                              "AufWiegKg",
                              "",
                              WS_CHILD | WS_BORDER,
                              xw, yw,
                              cxw, cyw,
                              WiegWindow,
                              NULL,
                              hMainInst,
                              NULL);

		KistCol = FALSE;
		SetClassLong (WiegKg, GCL_HBRBACKGROUND, (long) LtBlueBrush);
		SetKistRect (xw, yw);

        cxc = cx - 20;
        cyc = 150;
        xc  = 10;
        yc  = cy - 160;

        if (ohne_preis && strcmp (WiePreis.text1, "Preise") == 0)
        {
              WieButton.mask[1].attribut = REMOVED;
              GetZentrierWerte (&buglen, &buabs, cxc, 5, babs, bsize);
              WieButton.mask[0].pos[1] = (cxc - buglen) / 2;
              WieButton.mask[2].pos[1] = WieButton.mask[0].pos[1] + buabs;
              WieButton.mask[3].pos[1] = WieButton.mask[2].pos[1] + buabs;
              WieButton.mask[4].pos[1] = WieButton.mask[3].pos[1] + buabs;
              WieButton.mask[5].pos[1] = WieButton.mask[4].pos[1] + buabs;
        }
        else
        {
              WieButton.mask[1].attribut = COLBUTTON;
              GetZentrierWerte (&buglen, &buabs, cxc, 6, babs, bsize);
              WieButton.mask[0].pos[1] = (cxc - buglen) / 2;
              WieButton.mask[1].pos[1] = WieButton.mask[0].pos[1] + buabs;
              WieButton.mask[2].pos[1] = WieButton.mask[1].pos[1] + buabs;
              WieButton.mask[3].pos[1] = WieButton.mask[2].pos[1] + buabs;
              WieButton.mask[4].pos[1] = WieButton.mask[3].pos[1] + buabs;
              WieButton.mask[5].pos[1] = WieButton.mask[4].pos[1] + buabs;
        }

        GetZentrierWerte (&buglen, &buabs, cxc, 6, babs2, bsize2);
        WieButtonT.mask[0].pos[1] = (cxc - buglen) / 2;
        WieButtonT.mask[1].pos[1] = WieButtonT.mask[0].pos[1] + buabs;
        WieButtonT.mask[2].pos[1] = WieButtonT.mask[1].pos[1] + buabs;
        WieButtonT.mask[3].pos[1] = WieButtonT.mask[2].pos[1] + buabs;
        WieButtonT.mask[4].pos[1] = WieButtonT.mask[3].pos[1] + buabs;
        WieButtonT.mask[5].pos[1] = WieButtonT.mask[4].pos[1] + buabs;

        WiegCtr  = CreateWindowEx (
                              WS_EX_CLIENTEDGE,
                              "AufWiegCtr",
                              "",
                              WS_CHILD,
                              xc, yc,
                              cxc, cyc,
                              WiegWindow,
                              NULL,
                              hMainInst,
                              NULL);

        ShowWindow (WiegWindow, SW_SHOW);
        UpdateWindow (WiegWindow);
        ShowWindow (WiegKg, SW_SHOW);
        UpdateWindow (WiegKg);
        ShowWindow (WiegCtr, SW_SHOW);
        UpdateWindow (WiegCtr);
        create_enter_form (WiegWindow, &artwform, 0, 0);
        display_form (WiegWindow, &artwconst, 0, 0);
        current_wieg = TestAktivButton (PLUS, 0);
        SetFocus (WieButton.mask[current_wieg].feldid);

        WiegenAktiv = 1;
        current_wieg = 0;
        break_wieg = 0;
        while (GetMessage (&msg, NULL, 0, 0))
        {
              if (IsWiegMessage (&msg));
              else
              {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
              }
              if (break_wieg) break;
        }
        if (AlarmWindow)
        {
                     CloseControls (&alform);
                     DestroyWindow (AlarmWindow);
                     AlarmWindow = NULL;
        }
		DestroyKistBmp ();
        EnableWindow (hMainWindow, TRUE);
        CloseControls (&WiegKgT);
        CloseControls (&WiegKgE);
        CloseControls (&WieButton);
        CloseControls (&EtiButton);
        CloseControls (&artwform);
        CloseControls (&artwformT);
        CloseControls (&artwconst);
        CloseControls (&artwconstT);
        CloseControls (&startwieg);
        CloseControls (&fEntnahmeGewTxt);
        CloseControls (&fEntnahmeGew);
        CloseControls (&WiegKgMe);
        CloseControls (&ftara);
        CloseControls (&ftaratxt);
        DestroyWindow (WiegWindow);
//        SetListEWindow (1);
        WiegenAktiv = 0;
		sprintf(aufps.lief_me, "%8.3lf", ratod(auf_me) * -1);
}

/** Ab hier Auswahl fuer Preiseingabe
**/

static break_preis_enter = 0;


mfont PreisTextFont = {"Arial", 200, 0, 1, RGB (0, 0, 0),
                                         RGB (255, 255, 255),
                                         1,
                                         NULL};




static HWND fhWnd;

static char preisbuff [256];

static field _PrTextForm [] = {
preisbuff,             0, 0, 0, 0, 0, "", DISPLAYONLY, 0, 0, 0};

static form PrTextForm = {1, 0, 0, _PrTextForm, 0, 0, 0, 0, &PreisTextFont};

void PreisEnterBreak ()
/**
Endeflag fuer Erfassung setzen.
**/
{
        break_preis_enter = 1;
}

int doa (void)
{
         char buffer [20];
                   strcpy (buffer, Artikel);
     		       NuBlock.SetParent (eWindow);
                          NuBlock.SetChoise (doartchoise, 0);
                          NuBlock.EnterNumBox (ePreisWindow,"  Artikel  ",
                                               buffer, 12,"%hd" ,
	      									  EntNuProc);
				   SetActiveWindow (ePreisWindow);
                   InvalidateRect (ePreisWindow, NULL, TRUE);
                   UpdateWindow (ePreisWindow);
		           SetFocus (fhWnd);
			       if (ratod (buffer) > (double) 0.0)
				   {
					   strcpy (aufps.a, buffer);
					   lgr_umlgrp.a = atoi(buffer);
	                   strcpy (Artikel, buffer);
					   setartikel();
	                   if (InsWindow)
		               {
                             InvalidateRect (InsWindow, NULL, TRUE);
                             UpdateWindow (InsWindow);
			           }
				   }
				   return 0;

}

int dohdkl (void)
{
	/*
         char buffer [20];
 		 strcpy (buffer,Handelsklasse);
                   strcpy (buffer, Artikelgruppe);
     		       NuBlock.SetParent (eWindow);
                   NuBlock.SetChoise (dohdklchoise, 0);
                   NuBlock.EnterNumBox (ePreisWindow,"  Handelsklasse  ",
                                        buffer, 12,"%hd" ,
  									  EntNuProc);
				   SetActiveWindow (ePreisWindow);
                   InvalidateRect (ePreisWindow, NULL, TRUE);
                   UpdateWindow (ePreisWindow);
		           SetFocus (fhWnd);
			       if (buffer > " ")
				   {
					   strcpy (aufps.hdkl, buffer);
					   strcpy (lgr_umlgrp.hdkl, buffer);
					   setartikel();
	                   if (InsWindow)
		               {
                             InvalidateRect (InsWindow, NULL, TRUE);
                             UpdateWindow (InsWindow);
			           }
				   }
*/
				   return 0;

}

int docharge (void)
{
        char charge [20];



		if (flgNeu == TRUE)
		{
			strcpy(charge,lgr_umlgrp.chargennr);
		}
		else
		{
			strcpy (charge, aufps.ls_charge);
		}
        EnterCalcBox (ePreisWindow," Charge ", charge, 20, "");
        SetActiveWindow (ePreisWindow);
//        if (ratod (schl_nr) > (double) 0.0)
//        {
			strcpy(aufps.ls_charge, charge);
			strcpy(lgr_umlgrp.chargennr, charge);
                   if (InsWindow)
                   {
                             InvalidateRect (InsWindow, NULL, TRUE);
                             UpdateWindow (InsWindow);
                   }
//        }
        SetFocus (fhWnd);
        return 0;
}



int dogewicht (void)
{
        char gewicht [20];

		if (Wiegen == 0)
		{
			strcpy (gewicht, "0.000");
			EnterCalcBox (ePreisWindow,"    Gewicht    ", gewicht, 10, "%8.3f");
			SetActiveWindow (ePreisWindow);
			if (ratod (gewicht) > (double) 0.0)
			{
				strcpy (aufps.lief_me, gewicht);
				lgr_umlgrp.me = ratod(gewicht);
				setartikel();
				if (InsWindow)
				{
						InvalidateRect (InsWindow, NULL, TRUE);
						UpdateWindow (InsWindow);
				}
			}
		}
		Gewogen = 0;
//		doprok();

        SetFocus (fhWnd);
        return 0;
}

int dovk (void)
{
	/********
        char preis [20];
		PR_SCHWEIN_CLASS PrSchwein;

		strcpy (preis, VkPreis);
        EnterCalcBox (ePreisWindow,"    VK-Preis    ", preis, 10, "%8.2f");
        SetActiveWindow (ePreisWindow);
        if (ratod (preis) >= (double) 0.0)
        {
			if (syskey == KEY5)
			{
				strcpy (aufps.grund_pr, preis);
				strcpy (VkPreis, preis);
				lgr_umlgrp.grund_pr = ratod(preis);
				strcpy(lgr_umlgrp.abrech_kz," ");
				strcpy (aufps.abrech_kz, " ");
				DbClass.sqlin ((double *)   &lgr_umlgrp.a, 3, 0);
				DbClass.sqlout ((double *) &lgr_umlgrp.grund_pr, 3, 0);
				dsqlstatus = DbClass.sqlcomm ("select grund_pr from a_schl "
	                          "where a = ? ");
				if (lgr_umlgrp.anzahl == 0) lgr_umlgrp.anzahl = 1;
				lgr_umlgrp.grund_pr = lgr_umlgrp.grund_pr + PrSchwein.HoleAufschlaege
								(lgr_umlgrp.ag,lgr_umlgrp.mfa,(lgr_umlgrp.netto_gew / lgr_umlgrp.anzahl));
				sprintf (aufps.grund_pr,"%.2lf", lgr_umlgrp.grund_pr);
				syskey = KEYCR;
			}
			else
			{
				strcpy (aufps.grund_pr, preis);
				strcpy (VkPreis, preis);
				lgr_umlgrp.grund_pr = ratod(preis);
				strcpy(lgr_umlgrp.abrech_kz,"H");
				strcpy (aufps.abrech_kz, "H");
			}
            if (InsWindow)
            {
                      InvalidateRect (InsWindow, NULL, TRUE);
                      UpdateWindow (InsWindow);
            }
        }

        SetFocus (fhWnd);
		**********/
        return 0;
}


int doprok (void)
{
        PreisEnterBreak ();
        return 0;
}

HWND IsPreisChild (POINT *mousepos)
/**
Test, ob sich der Maus-Cursor in einem KindFensert befinden.
**/
{
        int i;

        for (i = 0; i < PrWahl.fieldanz; i ++)
        {
                   if (MouseinWindow (PrWahl.mask[i].feldid,
                                      mousepos))
                   {
                                 return (PrWahl.mask[i].feldid);
                   }
        }

        return NULL;
}


LONG FAR PASCAL PreisProc(HWND hWnd,UINT msg,
                          WPARAM wParam,LPARAM lParam)
{
        POINT mousepos;
        HWND enchild;
        static int SendChild = 0;
        int HasFocus = 1;

        GetCursorPos (&mousepos);

        switch(msg)
        {
              case WM_PAINT :
                    display_form (ePreisWindow, &PrWahl, 0, 0);
                    SetFocus (fhWnd);
                    break;
              case WM_LBUTTONDOWN :
              case WM_LBUTTONUP :
              case WM_MOUSEMOVE :
                    enchild = IsPreisChild (&mousepos);
//                    MouseTest (enchild, msg);
                    if (enchild)
                    {
                              SendMessage (enchild, msg, wParam, lParam);
                              return TRUE;
                    }
                    return TRUE;
              case WM_ACTIVATE :
                    switch (wParam)
                    {
                         case WA_ACTIVE :
                         case WA_CLICKACTIVE :
                              SetCapture (ePreisWindow);
                              break;
                    }
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

void RegisterPreis (void)
/**
Fenster fuer Preis registrieren.
**/
{
        static int registered = 0;
        WNDCLASS wc;

        if (registered == 0)
        {
                   wc.style =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                               | CS_OWNDC;
                   wc.cbClsExtra    =  0;
                   wc.cbWndExtra    =  0;
                   wc.hInstance     =  hMainInst;
                   wc.hIcon         =  LoadIcon (hMainInst, "FITICON");
                   wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                   wc.lpszMenuName  =  "";
                   wc.lpfnWndProc   =  PreisProc;
                   wc.lpszMenuName  =  "";
                   wc.hbrBackground =  CreateSolidBrush (LTGRAYCOL);
                   wc.lpszClassName =  "EnterPreis";
                   RegisterClass(&wc);
                   registered = 1;
        }
}

int PrGetAktButton (void)
/**
Aktiven Button holen.
**/
{
        HWND hWnd;
        int i;

        hWnd = GetFocus ();

        for (i = 0; i < PrWahl.fieldanz; i ++)
        {
                    if (hWnd == PrWahl.mask[i].feldid)
                    {
                               return (i);
                    }
        }
        return (0);
}


void PrPrevKey (void)
/**
Vorhergenden Button aktivieren.
**/
{
         int AktButton;

         AktButton = PrGetAktButton ();
         if (AktButton == 0)
         {
                     AktButton = PrWahl.fieldanz - 1;
         }
         else
         {
                     AktButton --;
         }
         SetFocus (PrWahl.mask [AktButton].feldid);
         fhWnd = GetFocus ();
}

void PrNextKey (void)
/**
Naechsten Button aktivieren.
**/
{
         int AktButton;

         AktButton = PrGetAktButton ();
         if (AktButton == PrWahl.fieldanz - 1)
         {
                     AktButton = 0;
         }
         else
         {
                     AktButton ++;
         }
         SetFocus (PrWahl.mask [AktButton].feldid);
         fhWnd = GetFocus ();
}

void PrActionKey (void)
/**
Button-Aktion ausfuehren.
**/
{
         int AktButton;
         field *feld;

         AktButton = PrGetAktButton ();
         feld =  &PrWahl.mask[AktButton];

         if (feld->after != (int (*) (void)) 0)
         {
                    (*feld->after) ();
         }
         else if (feld->BuId != 0)
         {
                    SendMessage (GetParent (feld->feldid), WM_COMMAND,
                                            MAKELONG (feld->BuId,
                                            feld->feldid),
                                            0l);
         }
}

int IsPreisKey (MSG *msg)
/**
HotKey Testen.
**/
{
         static char keypressed = 0;
         static HWND keyhwnd = 0;
         ColButton *ColBut;
         int taste;
         int i;
         char *pos;

         switch (msg->message)
         {
              case WM_KEYDOWN :
              {
                     if (keypressed)
                     {
                                  return FALSE;
                     }
                     switch (msg->wParam)
                     {
                            case VK_F5 :
                               PreisEnterBreak ();
                               syskey = KEY5;
                               return TRUE;
                            case  VK_UP :
                               PrPrevKey ();
                               return TRUE;
                            case VK_DOWN :
                               PrNextKey ();
                               return TRUE;
                            case VK_RIGHT :
                               PrNextKey ();
                               return TRUE;
                            case VK_LEFT :
                               PrPrevKey ();
                               return TRUE;
                            case VK_TAB :
                               if (GetKeyState (VK_SHIFT) < 0)
                               {
                                      PrPrevKey ();
                                      return TRUE;
                               }
                               PrNextKey ();
                               return TRUE;
                             case VK_RETURN :
                               PrActionKey ();
                               return TRUE;
                     }
                     taste = (int) msg->wParam;
//                     for (i = 0; i < 3; i ++)
                     for (i = 0; i < PrWahl.fieldanz; i ++)
                     {
                        ColBut = (ColButton *) PrWahl.mask[i].feld;
                        if (ColBut->text1)
                        {
                           if ((pos = strchr (ColBut->text1, '&')) &&
                               ((UCHAR) toupper (*(pos + 1)) == taste))
                           {
                                        keypressed = 1;
                                        keyhwnd = PrWahl.mask[i].feldid;
                                        SendMessage (keyhwnd,
                                                WM_LBUTTONDOWN,
                                                0l,
                                                0l);
                                        return TRUE;
                            }
                       }
                     }
                     return FALSE;
              }
              case WM_KEYUP :
              {
                     taste = (int) msg->wParam;
//                     for (i = 0; i < 3; i ++)
                     for (i = 0; i < PrWahl.fieldanz; i ++)
                     {
                         if (keyhwnd == PrWahl.mask[i].feldid)
                         {
                                   keypressed = 0;
                                   SendMessage (keyhwnd,
                                                WM_LBUTTONUP,
                                                0l,
                                                0l);
                                   keyhwnd = NULL;
                                   return TRUE;
                         }
                     }
                     return FALSE;
              }
          }
          return FALSE;
}


void EnterPreisBox (HWND hWnd, int px, int py)
/**
Fenster fuer Preisauswahl oeffnen.
**/
{
        RECT rect;
        MSG msg;
        int x, y, cx, cy;
        HCURSOR oldcursor;
        static int BitMapOK = 0;

        RegisterPreis ();
        GetWindowRect (hWnd, &rect);
        cx = PrWahl.fieldanz * pcbss0 + 7;
        cy = pcbsz0 + 10;
        if (px == -1)
        {
                  x = rect.left + (rect.right - rect.left - cx) / 2;
        }
        else
        {
                  x = px;
        }
        if (py == -1)
        {
                  y = (rect.bottom - cy) / 2;
        }
        else
        {
                  y = py;
        }

        ePreisWindow  = CreateWindowEx (
                              WS_EX_CLIENTEDGE,
                              "EnterPreis",
                              "",
                              WS_POPUP,
                              x, y,
                              cx, cy,
                              hWnd,
                              NULL,
                              hMainInst,
                              NULL);

        ShowWindow (ePreisWindow, SW_SHOW);
        UpdateWindow (ePreisWindow);
        oldcursor = SetCursor (LoadCursor (NULL, IDC_ARROW));
        SetFocus (PrWahl.mask[0].feldid);
        fhWnd = GetFocus ();
        SetCapture (ePreisWindow);

        PreisAktiv = 1;
        break_preis_enter = 0;
        while (GetMessage (&msg, NULL, 0, 0))
        {
              if (IsPreisKey (&msg));
              else
              {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
              }
              if (break_preis_enter) break;
        }
//		setartikel();

        CloseControls (&PrWahl);
        ReleaseCapture ();
        DestroyWindow (ePreisWindow);
        PreisAktiv = 0;
        SetCursor (oldcursor);
}

/*

Auswahl fuer Drucker.

*/

struct DR
{
        char drucker [33];
};

struct DR ldrucker, ldruckarr [20];
static int ldruckanz = 0;
static int ldidx;

mfont DrFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                               RGB (255, 255, 255),
                               1,
                               NULL};
static field _ldruckform [] =
{
        ldrucker.drucker,    22, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form ldruckform = {1, 0, 0, _ldruckform, 0, 0, 0, 0, &DrFont};

static field _ldruckub [] =
{
        "Drucker",     22, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form ldruckub = {1, 0, 0, _ldruckub, 0, 0, 0, 0, &DrFont};


void BelegeGdiDrucker (void)
/**
Drucker Win.ini holen
**/
{
       int i;
	   char *pr;
//	   char AktDevice [80];
       char ziel [256];
       char quelle [256];
       char *frmenv;

       frmenv = getenv ("FRMPATH");
       if (frmenv == NULL) return;

       GetAllPrinters ();

       i = 0;
       while (TRUE)
       {
                pr = GetNextPrinter ();
                if (pr == NULL) break;
                strcpy (ldruckarr[i].drucker, pr);
                i ++;
       }
       ldruckanz = i;

       sprintf (quelle, "%s\\%s", getenv ("FRMPATH"), "gdi.cfg");
       sprintf (ziel, "%s\\%s", getenv ("FRMPATH"), "drucker.akt");
       CopyFile (quelle, ziel, FALSE);
       sprintf (ziel, "%s\\%s.%s", getenv ("FRMPATH"),
                                 clipped (sys_ben.pers_nam),
                                 "akt");
       CopyFile (quelle, ziel, FALSE);

}


void BelegeDrucker (void)
/**
Drucker aus Formatverzeichnis holen.
**/
{
      char frmdir [256];
      char *frmenv;
      WIN32_FIND_DATA lpffd;
      HANDLE hFile;
      char *p;
      int i;
      static int druckerOK = 0;

      if (druckerOK) return;

      druckerOK = 1;
      if (GraphikMode)
      {
               BelegeGdiDrucker ();
               return;
      }
      frmenv = getenv ("FRMPATH");
      if (frmenv == NULL) return;

      sprintf (frmdir, "%s\\*.cfg", frmenv);


      hFile = FindFirstFile (frmdir, &lpffd);

      if (hFile == INVALID_HANDLE_VALUE)
      {
                      return;
      }

      i = 0;
      p = strchr (lpffd.cFileName, '.');
      if (p) *p = (char) 0;

      strcpy (ldruckarr[i].drucker, lpffd.cFileName);
      i ++;

      while (FindNextFile (hFile, &lpffd))
      {
               p = strchr (lpffd.cFileName, '.');
               if (p) *p = (char) 0;
               strcpy (ldruckarr[i].drucker, lpffd.cFileName);
               i ++;
      }
      FindClose (hFile);

      ldruckanz = i;
}

void drucker_akt (char *dname)
/**
Ausgewaehlten Drucker in drucker.akt_uebertragen.
**/
{
        char ziel [256];
        char quelle [256];

		if (GraphikMode)
		{
		        SetPrinter (dname);
				return;
		}
        sprintf (quelle, "%s\\%s.cfg", getenv ("FRMPATH"), dname);
        sprintf (ziel, "%s\\%s", getenv ("FRMPATH"), "drucker.akt");
        CopyFile (quelle, ziel, FALSE);
        sprintf (ziel, "%s\\%s.%s", getenv ("FRMPATH"),
                                 clipped (sys_ben.pers_nam),
                                 "akt");
        CopyFile (quelle, ziel, FALSE);
}

int DruckChoise ()
/**
Ausahlfenster fuer Festtara anzeigen.
**/
{
        char aktdrucker [80];

        BelegeDrucker ();
        ldidx = ShowPtBox (eWindow, (char *) ldruckarr, ldruckanz,
                     (char *) &ldrucker,(int) sizeof (struct DR),
                     &ldruckform, NULL, &ldruckub);
        strcpy (aktdrucker, ldruckarr [ldidx].drucker);
        clipped (aktdrucker);
        drucker_akt (aktdrucker);
        CloseControls (&testub);
		display_form (Getlbox (), &testub, 0, 0);
        return ldidx;
}


/** Ptabanzeige
**/

/*
static int ptbss0 = 73;
static int ptbss  = 77;
static int ptbsz  = 40;
static int ptbsz0 = 36;
*/

static int ptbss0 = 103;
static int ptbss  = 107;
static int ptbsz  = 60;
static int ptbsz0 = 56;


field _PtButton[] = {
(char *) &Back,         ptbss0, ptbsz0, -1, 4+0*ptbss, 0, "", COLBUTTON, 0,
 *                                                        func5, 0,
(char *) &BuOKS,        ptbss0, ptbsz0, -1, 4+1*ptbss, 0, "", COLBUTTON, 0,
 *                                                        func6, 0,
(char *) &PfeilU,       ptbss0, ptbsz0, -1, 4+2*ptbss, 0, "", COLBUTTON, 0,
                                                          func3, 0,
(char *) &PfeilO,       ptbss0, ptbsz0, -1, 4+3*ptbss, 0, "", COLBUTTON, 0,
                                                          func2, 0
};

form PtButton = {4, 0, 0, _PtButton, 0, 0, 0, 0, &buttonfont};


void IsPtClck (int idx)
/**
Reaktion auf Doppelclick in der Liste.
**/
{
        ptidx = idx;
        break_list ();
        return;
}

void RegisterPt (void)
/**
Kopf - und Fuss - Fenster fuer Liste registrieren
**/
{
        static int registered = 0;
        WNDCLASS wc;

        if (registered == 0)
        {
                   wc.style =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                               | CS_OWNDC;
                   wc.cbClsExtra    =  0;
                   wc.cbWndExtra    =  0;
                   wc.hInstance     =  hMainInst;
                   wc.hIcon         =  LoadIcon (hMainInst, "FITICON");
                   wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                   wc.lpszMenuName  =  "";
                   wc.lpfnWndProc   =  PtFussProc;
                   wc.lpszMenuName  =  "";
                   wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
                   wc.lpszClassName =  "PtFuss";
                   RegisterClass(&wc);
                   registered = 1;
        }
}

HWND IsPtFussChild (POINT *mousepos)
/**
Test, ob sich der Maus-Cursor in einem KindFensert befinden.
**/
{
        int i;

        for (i = 0; i < PtButton.fieldanz; i ++)
        {
                   if (MouseinWindow (PtButton.mask[i].feldid,
                                      mousepos))
                   {
                                 return (PtButton.mask[i].feldid);
                   }
        }
        return NULL;
}

int MouseInPt (POINT *mpos)
{
        HWND hListBox;

        hListBox = GethListBox ();

        if (MouseinDlg (hListBox, mpos)) return TRUE;
        if (MouseinDlg (PtFussWindow, mpos)) return TRUE;
        return FALSE;
}


LONG FAR PASCAL PtFussProc(HWND hWnd,UINT msg,
                           WPARAM wParam,LPARAM lParam)
{
        HWND enchild;
        POINT mousepos;
        HWND lbox;

        GetCursorPos (&mousepos);

        switch(msg)
        {
              case WM_PAINT :
                    display_form (PtFussWindow, &PtButton, 0, 0);
                    break;
              case WM_LBUTTONDOWN :
              case WM_LBUTTONUP :
              case WM_MOUSEMOVE :
                    enchild = IsPtFussChild (&mousepos);
//                    MouseTest (enchild, msg);
                    if (enchild)
                    {
                              SendMessage (enchild, msg, wParam, lParam);
                              return TRUE;
                    }
                    lbox = Getlbox ();
                    if (MouseinWindow (lbox, &mousepos))
                    {
                              ScreenToClient (lbox, &mousepos);
                              lParam = MAKELONG (mousepos.x, mousepos.y);
                              SendMessage (lbox, msg, wParam, lParam);
                              return TRUE;
                    }
                    return TRUE;
              case WM_VSCROLL :
                    lbox = Getlbox ();
                    if (MouseinWindow (lbox, &mousepos))
                    {
                              SendMessage (lbox, msg, wParam, lParam);
                    }
                    return TRUE;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

int ShowPtBox (HWND hWnd, char * ptabarr, int ptabanz, char * ptab,
               int ptsize, form *ptform, form *ptvl, form *ptub)
{
        RECT rect;
        RECT frmrect;
        int x, y, cx, cy;
        HWND shListBox;
        HWND slbox;
        struct LMENUE smenue;

        GetMenue (&smenue);
        MenTextSave (&smenue);
        shListBox = GethListBox ();
        slbox = Getlbox ();

        RegisterPt ();
        GetWindowRect (hMainWindow, &rect);
        GetFrmRectEx (ptform, ptform->font, &frmrect);

        x = rect.left + 50;
        y = rect.top + 20;
        cx = frmrect.right + 45;
        cy = 185;

        set_fkt (endlist, 5);
        SetDblClck (IsPtClck, 0);
        PtListeAktiv = 1;
        SetAktivWindow (hWnd);
//        SetListEWindow (1);
        SetListFont (TRUE);
        spezfont (ptform->font);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_VISIBLE);
        PtWindow = OpenListWindowBoEx (x, y, cx, cy);
//        PtWindow = OpenListWindowEnEx (x, y, cx, cy);
        GetWindowRect (PtWindow, &rect);
        y = rect.bottom;
		cx = rect.right - rect.left;
        PtFussWindow = CreateWindow ("PtFuss",
                                    "",
                                    WS_POPUP | WS_VISIBLE | WS_DLGFRAME,
                                    x,
//                                    y + cy + 23,
                                    y,
                                    cx, 70,
                                    hWnd,
                                    NULL,
                                    hMainInst,
                                    NULL);
        if (ptvl) ElistVl (ptvl);
        if (ptub) ElistUb (ptub);
        ShowElist ((char *) ptabarr,
                  ptabanz,
                  (char *) ptab,
                  (int) ptsize,
                  ptform);
//        SetMouseLock (TRUE);
        SetMouseLockProc (MouseInPt);
        EnterElist (PtWindow, (char *) ptabarr,
                             ptabanz,
                             (char *) ptab,
                             (int) ptsize,
                             ptform);
        CloseControls (&PtButton);
        CloseEWindow (PtWindow);
        init_break_list ();
//        SetMouseLock (FALSE);
        SetMouseLockProc (NULL);
        DestroyWindow (PtFussWindow);
        PtListeAktiv = 0;
        SetMenue (&smenue);
        SethListBox (shListBox);
        Setlbox (slbox);
        SetAktivWindow (hWnd);
        return ptidx;
}

/**
Lieferschein auf komplett setzen.
**/

static int ldruck   = 0;
static int ldrfr    = 0;
static int lfrei    = 0;
static int lbar     = 0;
static int lsofort  = 0;
static int drwahl   = 0;

static int *lbuttons [] = {&ldruck, &ldrfr, &drwahl, NULL};

static int Setldruck (void);
static int Setlfrei (void);
static int Setldrfr (void);

/*
static int Setlbar (void);
static int Setlsofort (void);
*/
static int Setdrwahl (void);

static int BreakBox (void);
static int BoxBreak (void);

static int IsDrChoise;


static field _cbox [] = {
" Drucken      ",     22, 2, 0, 6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             Setldruck, 101,
" Druckerwahl     ",     19, 2, 4, 6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             Setdrwahl, 106,
"   OK    ",             10, 1, 7, 2,  0, "", BUTTON, 0, BreakBox, 107,
" Abbruch ",             10, 1, 7, 15, 0, "", BUTTON, 0, BoxBreak, KEY5};

form cbox = {4, 0, 0, _cbox, 0,0,0,0,&ListFont};

int KomplettEnd (void)
/**
EnterButton abbrechen.
**/
{
           if (syskey == KEY5)
           {
                    break_enter ();
           }
           return 1;
}

void UnsetBox (int pos)
/**
Buttona auf selectierte setzen. (ausser pos);
**/
{
        int i;

        for (i = 0; lbuttons [i]; i ++)
        {
                  if (i == pos) continue;

                  SendMessage (cbox.mask[i].feldid, BM_SETCHECK, 0, 0l);
                  *lbuttons [i] = 0;
        }
}


int Setldruck (void)
/**
Druck setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (cbox.mask[0].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       ldruck = 0;
          }
          else
          {
                       ldruck = 1;
          }
          if (syskey == KEYCR)
          {
                       SendMessage (cbox.mask[0].feldid, BM_SETCHECK,
                       !ldruck, 0l);
                       ldruck = !ldruck;
          }
          UnsetBox (0);
          return 1;
}

int Setldrfr (void)
/**
Freigabe und Druck setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (cbox.mask[1].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                        ldrfr = 0;
          }
          else
          {
                        ldrfr = 1;
          }
          if (syskey == KEYCR)
          {
                       SendMessage (cbox.mask[1].feldid, BM_SETCHECK,
                       !ldrfr, 0l);
                       ldrfr = !ldrfr;
          }
          UnsetBox (1);
          return 1;
}

int Setlfrei (void)

/**
Freigabe setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (cbox.mask[1].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       lfrei = 0;
          }
          else
          {
                       lfrei = 1;
          }
          if (syskey == KEYCR)
          {
                       SendMessage (cbox.mask[1].feldid, BM_SETCHECK,
                       !lfrei, 0l);
                       lfrei = !lfrei;
          }
          UnsetBox (1);
          return 1;
}

int Setlbar (void)

/**
Barverkauf setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (cbox.mask[3].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       lbar = 0;
          }
          else
          {
                       lbar = 1;
          }
          if (syskey == KEYCR)
          {
                       SendMessage (cbox.mask[3].feldid, BM_SETCHECK,
                       !lbar, 0l);
                       lbar = !lbar;
          }
          UnsetBox (3);
          return 1;
}

int Setlsofort (void)

/**
Sofortrechnung setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (cbox.mask[4].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       lsofort = 0;
          }
          else
          {
                       lsofort = 1;
          }
          if (syskey == KEYCR)
          {
                       SendMessage (cbox.mask[4].feldid, BM_SETCHECK,
                       !lsofort, 0l);
                       lsofort = !lsofort;
          }
          UnsetBox (4);
          return 1;
}

int Setdrwahl (void)

/**
Sofortrechnung setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          DruckChoise ();
          set_fkt (KomplettEnd, 5);
          SetFocus (cbox.mask[5].feldid);
          return 1;
}


int BreakBox (void)
/**
EnterButton abbrechen.
**/
{
           if (syskey == KEYUP || syskey == KEYDOWN) return 0;
           syskey = KEYCR;
           break_enter ();
           return 1;
}

int BoxBreak (void)
/**
EnterButton abbrechen.
**/
{
           if (syskey == KEYUP || syskey == KEYDOWN) return 0;
           syskey = KEY5;
           break_enter ();
           return 1;
}

int MouseInButton (POINT *mpos)
{
        int i;

        if (MouseinDlg (MainButton2.mask[2].feldid, mpos))
        {
                  return TRUE;
        }

        for (i = 5; i < 7; i ++)
        {
                if (MouseinDlg (MainButton2.mask[i].feldid, mpos))
                {
                                   return TRUE;
                }
        }
        return FALSE;
}

static void SelectAktCtr (void)
/**
Aktiven Button in EnterButton selectieren.
**/
{
        syskey = KEYCR;
        if (cbox.mask[currentfield].after)
        {
                       (*cbox.mask[currentfield].after) ();
        }
        syskey = KEYCR;
}

BOOL SetLskStatus (int ls_stat)
/**
ls_stat auf 3 setzen.
**/
{
        return TRUE;
}

void BearbKomplett (void)
/**
Auswahl aus Komplett bearbeiten.
**/
{


         if (strcmp (sys_ben.pers_nam, " ") <= 0)
         {
             strcpy (sys_ben.pers_nam, "fit");
         }
         commitwork ();

         if (ldruck && DrKomplett)
         {
// wird beim Drucken der Lieferschein sofort
// auf komplett gesetzt. Ist einstellbar �ber DrKomplett
                lgr_umlgrk.stat = 4;
                Lgr_umlgrkClass.dbupdate ();
				Kontrollbeleg ();
				break_list ();
         }
         else if (ldruck)
         {
				Kontrollbeleg ();
				break_list ();
         }
         else if (lfrei)
         {

                lgr_umlgrk.stat = 4;
                Lgr_umlgrkClass.dbupdate ();
				break_list ();
         }
         beginwork ();
}



void WritePr (int idx)
{
    double pr_ek_nto = ratod (aufparr[idx].ls_lad_euro);
	double inh = aufparr[idx].min_best;
	int me_kz = aufparr[idx].me_kz;
    double we_me = ratod (aufparr[idx].auf_me);

	if (inh == 0.0)
	{
		inh = 1.0;
	}

    if (EktoMeEinh || me_kz == 2)
    {
			 pr_ek_nto /= inh;
    }


    if (_a_bas.a_typ == 1)
    {
           A_KALKHNDW_CLASS *Hndw = new A_KALKHNDW_CLASS ();
           a_kalkhndw.mdn = akt_mdn;
           a_kalkhndw.fil = akt_fil;
           a_kalkhndw.a   = ratod (aufparr[idx].a);
           int dsqlstatus = Hndw->dbreadfirst ();
           if (dsqlstatus == 0)
           {
               a_kalkhndw.pr_ek3 = a_kalkhndw.pr_ek2;
               a_kalkhndw.pr_ek2 = a_kalkhndw.pr_ek1;
               a_kalkhndw.we_me3 = a_kalkhndw.we_me2;
               a_kalkhndw.we_me2 = a_kalkhndw.we_me1;
           }
           a_kalkhndw.pr_ek1 = pr_ek_nto;
           a_kalkhndw.we_me1 = we_me;
           Hndw->dbupdate ();
           delete Hndw;
    }
    else if (_a_bas.a_typ == 5 ||
             _a_bas.a_typ == 6 ||
             _a_bas.a_typ == 9)
    {
           A_KALK_MAT_CLASS *Mat = new A_KALK_MAT_CLASS ();
           a_kalk_mat.mdn = akt_mdn;
           a_kalk_mat.fil = akt_fil;
           a_kalk_mat.a   = ratod (aufparr[idx].a);
           int dsqlstatus = Mat->dbreadfirst ();
           a_kalk_mat.mat_o_b = pr_ek_nto;
           Mat->dbupdate ();
           delete Mat;
    }
    else if (_a_bas.a_typ2 == 1)
    {
           A_KALKHNDW_CLASS *Hndw = new A_KALKHNDW_CLASS ();
           a_kalkhndw.mdn = akt_mdn;
           a_kalkhndw.fil = akt_fil;
           a_kalkhndw.a   = ratod (aufparr[idx].a);
           int dsqlstatus = Hndw->dbreadfirst ();
           if (dsqlstatus == 0)
           {
               a_kalkhndw.pr_ek3 = a_kalkhndw.pr_ek2;
               a_kalkhndw.pr_ek2 = a_kalkhndw.pr_ek1;
               a_kalkhndw.we_me3 = a_kalkhndw.we_me2;
               a_kalkhndw.we_me2 = a_kalkhndw.we_me1;
           }
           a_kalkhndw.pr_ek1 = pr_ek_nto;
           a_kalkhndw.we_me1 = we_me;
           Hndw->dbupdate ();
           delete Hndw;
    }
    else if (_a_bas.a_typ2 == 5 ||
             _a_bas.a_typ2 == 6 ||
             _a_bas.a_typ2 == 9)
    {
           A_KALK_MAT_CLASS *Mat = new A_KALK_MAT_CLASS ();
           a_kalk_mat.mdn = akt_mdn;
           a_kalk_mat.fil = akt_fil;
           a_kalk_mat.a   = ratod (aufparr[idx].a);
           int dsqlstatus = Mat->dbreadfirst ();
           a_kalk_mat.mat_o_b = pr_ek_nto;
           Mat->dbupdate ();
           delete Mat;
    }
}


void WritePreise (void)
/*
Einkaufspreise zurueckschreiben.
**/
{
         for (int i = 0; i < aufpanz; i ++)
         {
             lese_a_bas (ratod (aufparr[i].a));
             WritePr (i);
         }
}

int SetKomplett ()
/**
Komplett setzen.
**/
{
         int x,y;
         int cx, cy;
		 int cy0;
         int mx;
         RECT rect;
         TEXTMETRIC tm;
         HDC hdc;
         HFONT hFont;
//		 int i;

/*
         if (abfragejnG (eWindow,
                          "Wareneingang abschlie�en ?\n", "J") == 0)

		 {
                          return 0;
		 }

         if (TestQsPos () == FALSE)
         {
             return 0;
         }
*/
 	     y =  3;
		 cy = 7;
         set_fkt (KomplettEnd, 5);
         spezfont (&ListFont);
         hFont = SetWindowFont (hMainWindow);
         hdc = GetDC (hMainWindow);
         SelectObject (hdc, hFont);
         GetTextMetrics (hdc, &tm);
         DelFont (hFont);
         ReleaseDC (hMainWindow, hdc);
         GetClientRect (eWindow, &rect);
         mx = rect.right / tm.tmAveCharWidth;
         GetFrmSize (&cbox, &cx, &cy0);

         x = max (0, (mx - cx) / 2);

         KomplettAktiv = 1;

         beginwork ();
//         WritePreise ();
         commitwork ();
         ldruck = ldrfr = lfrei = lbar = lsofort = drwahl = 0;
         SetMouseLockProc (MouseInButton);
         SetBorder (WS_VISIBLE | WS_DLGFRAME | WS_POPUP);
         if (EnterButtonWindow (eWindow, &cbox, y, x, cy, cx) == -1)
         {
                        KomplettAktiv = 0;
                        set_fkt (endlist, 5);
                        CloseControls (&testub);
                        display_form (Getlbox (), &testub, 0, 0);
						syskey = 0;
                        return 0;
         }

	     syskey = 0;
         CloseControls (&testub);
         display_form (Getlbox (), &testub, 0, 0);
         KomplettAktiv = 0;
         SetMouseLockProc (NULL);
		 InvalidateRect (eWindow, NULL, TRUE);
		 UpdateWindow (eWindow);
         BearbKomplett ();
         set_fkt (endlist, 5);
         return 0;
}


/*   Gebinde eingeben          */

mfont GebUbFont = {"Courier New", 190, 0, 1, RGB (255, 255, 255),
                                             GebColor,
                                             0,
                                             NULL};
mfont GebFont   = {"Courier New", 190, 0, 1, RGB (255, 255, 255),
                                             GebColor,
                                             0,
                                             NULL};

static char gebinde [4] = {"N"};
static char palette [4] = {"N"};
static char geb_einh [4] = {"1"};
static char pal_einh [4] = {"1"};
static char geb_me [9];
static char pal_me [9];
static char gtara [9];
static char gtara_einh [3] = {"2"};

static int gebcx = 620;
static int gebcy = 400;

static int break_geb = 0;

static int buttony = gebcy - bsz0 - 20;
static int buttonpos = 5;

static int current_geb = 0;
static int gebend = 6;

static int gbanz = 0;

static int testgebinde ();
static int testpalette ();
static int testgeb_einh ();
static int testpal_einh ();

static int gebfunc1 (void);
static int gebfunc4 (void);

field _gebub[] = {
           "Etikett",  8, 0, 1,10, 0, "", DISPLAYONLY, 0, 0, 0,
           "Einheit",  8, 0, 1,20, 0, "", DISPLAYONLY, 0, 0, 0,
           "Menge  ",  0, 0, 1,30, 0, "", DISPLAYONLY, 0, 0, 0,
           "Gebinde",  10, 0, 3,  1, 0, "",      DISPLAYONLY, 0, 0, 0,
           "Palette",  10, 0, 5,  1, 0, "",      DISPLAYONLY, 0, 0, 0,
           gtara_einh,  3, 0, 7, 22, 0, "%2d",   READONLY, 0, 0, 0,
           "Einzel-Tara", 11, 0, 7,  1, 0, "",   DISPLAYONLY, 0, 0, 0,
};

form gebub = {7, 0, 0, _gebub, 0, 0, 0, 0, &GebUbFont};


field _gebform[] = {

         gebinde,     2, 0, 3, 13, 0, "",      NORMAL, 0, testgebinde, 0,
         geb_einh,    3, 0, 3, 22, 0, "%2d",   NORMAL, 0, testgeb_einh, 0,
         geb_me,      8, 0, 3, 30, 0, "%3.3f", NORMAL, 0, 0, 0,

         palette,     2, 0, 5, 13, 0, "",      NORMAL, 0, testpalette, 0,
         pal_einh,    3, 0, 5, 22, 0, "%2d",   NORMAL, 0, testpal_einh, 0,
         pal_me,      8, 0, 5, 30, 0, "%3.3f", NORMAL, 0, 0, 0,
         gtara,       8, 0, 7, 30, 0, "%3.3f", NORMAL, 0, 0, 0,
};

form gebform = {7, 0, 0, _gebform, 0, 0, 0, 0, &GebFont};

field _GebButton[] = {
(char *) &Back,              bss0, bsz0, buttonpos, 4+5*bss, 0, "", COLBUTTON, 0,
 *                                                              func5, 0,
(char *) &BuOK,              bss0, bsz0, buttonpos, 4+4*bss, 0, "", COLBUTTON, 0,
 *                                                              func6, 0,
(char *) &PfeilR,            bss0, bsz0, buttonpos, 4+3*bss, 0, "", COLBUTTON, 0,
                                                          gebfunc4, 0,
(char *) &PfeilU,            bss0, bsz0, buttonpos, 4+2*bss, 0, "", COLBUTTON, 0,
                                                          func3, 0,
(char *) &PfeilO,            bss0, bsz0, buttonpos, 4+1*bss, 0, "", COLBUTTON, 0,
                                                          func2, 0,
(char *) &PfeilL,            bss0, bsz0, buttonpos, 4,     0, "", COLBUTTON, 0,
                                                          gebfunc1, 0};

form GebButton = {6, 0, 0, _GebButton, 0, 0, 0, 0, &buttonfont};

int gebfunc1 (void)
/**
Button fuer Pfeil links bearbeiten.
**/
{

         SetFocus (gebform.mask[current_geb].feldid);
         SendMessage (gebform.mask[current_geb].feldid, WM_KEYDOWN,  (WPARAM) VK_LEFT, 0l);
         return 1;
}

int gebfunc4 (void)
/**
Button fuer Pfeil links bearbeiten.
**/
{

         SetFocus (gebform.mask[current_geb].feldid);
         SendMessage (gebform.mask[current_geb].feldid, WM_KEYDOWN,  (WPARAM) VK_RIGHT, 0l);
         return 1;
}

void PrintGLines ()
/**
Icons am Bildschirm anzeigen.
**/
{
//         RECT rect, frect;
         PAINTSTRUCT ps;
         HDC hdc;
         HPEN hPen;
         int x, y;

         hdc = BeginPaint (GebWindow, &ps);

/*
         GetFrmRectEx (&gebform, &GebFont, &frect);

         hPen = CreatePen (PS_SOLID, 2, BLACKCOL);
         SelectObject (hdc, hPen);
         x = gebcx - 4;
         y = frect.top - 30;
         MoveToEx (hdc, 2, y, NULL);
         LineTo (hdc, x, y);
         DeleteObject (hPen);

         hPen = CreatePen (PS_SOLID, 2, GRAYCOL);
         SelectObject (hdc, hPen);
         MoveToEx (hdc, 2, y - 1, NULL);
         LineTo (hdc, x, y - 2);
         DeleteObject (hPen);
*/

         y = buttony - 10;
         x = gebcx - 4;

         hPen = CreatePen (PS_SOLID, 2, BLACKCOL);
          SelectObject (hdc, hPen);
         MoveToEx (hdc, 2, y, NULL);
         LineTo (hdc, x, y);
         DeleteObject (hPen);

         hPen = CreatePen (PS_SOLID, 2, GRAYCOL);
         SelectObject (hdc, hPen);
         MoveToEx (hdc, 2, y - 1, NULL);
         LineTo (hdc, x, y - 2);
         DeleteObject (hPen);

         EndPaint (WiegWindow, &ps);
}

void SetGebFocus (void)
/**
Focus fuer Wiegmaske setzen.
**/
{

          SetFocus (gebform.mask[current_geb].feldid);
          SendMessage (gebform.mask[current_geb].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
}


LONG FAR PASCAL GebProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{

        switch(msg)
        {
              case WM_PAINT :
                    if (hWnd == GebWindow)
                    {
                                 display_form (GebWindow, &gebub, 0, 0);
                                 display_form (GebWindow, &gebform, 0, 0);
                        //        PrintGLines ();
                                 SetGebFocus ();
                    }
                    else if (hWnd == GebWindButton)
                    {
                                 display_form (GebWindButton, &GebButton, 0, 0);
                    }
                    break;
              case WM_KEYDOWN :
                         switch (wParam)
                         {
                              case VK_F5 :
                                      func5 ();
                                      return 0;

                         }
                         break;
              case WM_COMMAND :
                    if (LOWORD (wParam) == KEY5)
                    {
                            syskey = KEY5;
                            SendKey (VK_F5);
                            break;
                    }
                    break;
              case WM_DESTROY :
                    if (hWnd == hMainWindow)
                    {
	                         SetForeground ();
	                         closedbase ();
                             ExitProcess (0);
                             return 0;
                    }
                    return 0;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

static int testgeb_einh (void)
/**
Felde Kundenbestell-Einheit testen.
**/
{
/*
	       if (ptab_class.lese_ptab ("me_einh_fill",
                   geb_einh)
			!= 0)
		{
					strcpy (geb_einh, "0");
		}
*/
            display_form (GebWindow, &gebform, 0, 0);
		return 0;
}

static int testpal_einh (void)
/**
Felde Kundenbestell-Einheit testen.
**/
{
/*
	       if (ptab_class.lese_ptab ("me_einh_fill",
                   pal_einh)
			!= 0)
		{
					strcpy (pal_einh, "0");
		}
*/
            display_form (GebWindow, &gebform, 0, 0);
		return 0;
}


int testgebinde ()
/**
Eingabe fuer Gebinde testen.
**/
{
         if (gebinde[0] == 'J' || gebinde[0] == 'j')
         {
                            gebinde[0] = 'J';
                            display_form (GebWindow, &gebform, 0, 0);
                            return 0;
         }

         if (atoi (gebinde))
         {
                            gebinde[0] = 'J';
                            display_form (GebWindow, &gebform, 0, 0);
                            return 0;
         }
         gebinde[0] = 'N';
         display_form (GebWindow, &gebform, 0, 0);
         return 0;
}

int testpalette ()
/**
Eingabe fuer Gebinde testen.
**/
{
         if (palette[0] == 'J' || palette[0] == 'j')
         {
                            palette[0] = 'J';
                            return 0;
         }

         if (atoi (palette))
         {
                            palette[0] = 'J';
                            return 0;
         }
         palette[0] = 'N';
         return 0;
}


int testgebfields (void)
/**
Eingabefelder testen.
**/
{
        if (current_geb == 0)
        {
                   testgebinde ();
        }
        else if (current_geb == 3)
        {
                   testpalette ();
        }
        else if (current_geb == 1)
        {
                   testgeb_einh ();
        }
        else if (current_geb == 4)
        {
                   testpal_einh ();
        }
        return (0);
}

void gebaktion (void)
/**
Aktion auf aktivem Feld.
**/
{
        switch (current_geb)
        {
                  case 0 :
                          if (gebinde[0] == 'J')
                          {
                                       gebinde[0] = 'N';
                          }
                          else
                          {
                                       gebinde[0] = 'J';
                          }
                          testgebinde ();
                          display_form (GebWindow, &gebform, 0, 0);
                          break;
                  case 1 :
                          ShowGebMeEinh (geb_einh);
                          display_form (GebWindow, &gebform, 0, 0);
                          break;
                  case 2 :
                          EnterCalcBox (GebWindow,"  Gebinde-Menge  ",
                                       geb_me, gebform.mask[2].length,
                                       gebform.mask[2].picture);
                          display_form (GebWindow, &gebform, 0, 0);
                          break;

                  case 3 :
                          if (palette[0] == 'J')
                          {
                                       palette[0] = 'N';
                          }
                          else
                          {
                                       palette[0] = 'J';
                          }
                          testpalette ();
                          display_form (GebWindow, &gebform, 0, 0);
                          break;
                  case 4 :
                          ShowGebMeEinh (pal_einh);
                          display_form (GebWindow, &gebform, 0, 0);
                          break;
                  case 5 :
                          EnterCalcBox (GebWindow,"  Paletten-Menge  ",
                                       pal_me, gebform.mask[5].length,
                                       gebform.mask[5].picture);
                          display_form (GebWindow, &gebform, 0, 0);
                          break;
                  case 6 :
                          EnterCalcBox (GebWindow,"       Tara       ",
                                       gtara, gebform.mask[6].length,
                                       gebform.mask[6].picture);
                          display_form (GebWindow, &gebform, 0, 0);
                          break;
             }
}

int IsGebMessage (MSG *msg)
/**
HotKey Testen.
**/
{
         POINT mousepos;

         switch (msg->message)
         {
              case WM_KEYDOWN :
              {
                     switch (msg->wParam)
                     {
                            case VK_ESCAPE :
                            case VK_F5 :
                                    break_geb = 1;
                                    return TRUE;
                            case VK_RETURN :
                                    gebaktion ();
                                    return TRUE;
                            case VK_DOWN :
                                    testgebfields ();
                                    current_geb ++;
                                    if (current_geb > gebend)
                                                current_geb = 0;
                                    SetGebFocus ();
                                    return TRUE;
                            case VK_UP :
                                   testgebfields ();
                                   current_geb --;
                                   if (current_geb < 0)
                                                current_geb = gebend;
                                    SetGebFocus ();
                                    return TRUE;
                            case VK_TAB :
                                    testgebfields ();
                                    if (GetKeyState (VK_SHIFT) < 0)
                                    {
                                             current_geb --;
                                             if (current_geb < 0)
                                                  current_geb = gebend;
                                     }
                                     else
                                     {
                                             current_geb ++;
                                             if (current_geb > gebend)
                                                  current_geb = 0;
                                     }
                                     SetGebFocus ();
                                     return TRUE;

                             case VK_F9 :
                                     syskey = KEY9;
                                     if (current_geb == 1)
                                     {
                                               ShowGebMeEinh (geb_einh);
                                               display_form (GebWindow, &gebform, 0, 0);
                                     }
                                     else if (current_geb == 4)
                                     {
                                               ShowGebMeEinh (pal_einh);
                                               display_form (GebWindow, &gebform, 0, 0);
                                     }
                                     return TRUE;

                            default :
                                    return FALSE;
                     }
                     break;
              }
              case WM_LBUTTONDOWN :
              {
                    GetCursorPos (&mousepos);

                    if (MouseinWindow (gebform.mask[0].feldid, &mousepos))
                    {
                          current_geb = 0;
/*
                          EnterNumBox (GebWindow,"  Gebinde-Etikett  ",
                                       gebinde, gebform.mask[0].length,
                                       "%1d");
*/
                          if (gebinde[0] == 'J')
                          {
                                       gebinde[0] = 'N';
                          }
                          else
                          {
                                       gebinde[0] = 'J';
                          }
                          testgebinde ();
                          display_form (GebWindow, &gebform, 0, 0);
//                          PostMessage (GebWindow, WM_KEYDOWN, VK_RETURN, 0l);
                          return TRUE;
                    }
                    else if (MouseinWindow (gebform.mask[1].feldid, &mousepos))
                    {
/*
                          EnterNumBox (GebWindow,"  Gebinde-Einheit  ",
                                       geb_einh, gebform.mask[1].length,
                                       gebform.mask[1].picture);
*/
                          current_geb = 1;
                          ShowGebMeEinh (geb_einh);
                          display_form (GebWindow, &gebform, 0, 0);
//                          PostMessage (GebWindow, WM_KEYDOWN, VK_RETURN, 0l);
                          return TRUE;
                    }
                    else if (MouseinWindow (gebform.mask[2].feldid, &mousepos))
                    {
                          current_geb = 2;
                          EnterCalcBox (GebWindow,"  Gebinde-Menge  ",
                                       geb_me, gebform.mask[2].length,
                                       gebform.mask[2].picture);
                          display_form (GebWindow, &gebform, 0, 0);
//                          PostMessage (GebWindow, WM_KEYDOWN, VK_RETURN, 0l);
                          return TRUE;
                    }

                    else if (MouseinWindow (gebform.mask[3].feldid, &mousepos))
                    {
                          current_geb = 3;
/*
                          EnterNumBox (GebWindow,"  Paletten-Etikett  ",
                                       palette, gebform.mask[3].length,
                                       "%1d");
*/
                          if (palette[0] == 'J')
                          {
                                       palette[0] = 'N';
                          }
                          else
                          {
                                       palette[0] = 'J';
                          }
                          testpalette ();
                          display_form (GebWindow, &gebform, 0, 0);
//                          PostMessage (GebWindow, WM_KEYDOWN, VK_RETURN, 0l);
                          return TRUE;
                    }
                    else if (MouseinWindow (gebform.mask[4].feldid, &mousepos))
                    {
/*
                          EnterNumBox (GebWindow,"  Paletten-Einheit  ",
                                       pal_einh, gebform.mask[4].length,
                                       gebform.mask[4].picture);
*/
                          current_geb = 4;
                          ShowGebMeEinh (pal_einh);
                          display_form (GebWindow, &gebform, 0, 0);
                          return TRUE;
                    }
                    else if (MouseinWindow (gebform.mask[5].feldid, &mousepos))
                    {
                          current_geb = 5;
                          EnterCalcBox (GebWindow,"  Paletten-Menge  ",
                                       pal_me, gebform.mask[5].length,
                                       gebform.mask[5].picture);
                          display_form (GebWindow, &gebform, 0, 0);
                          return TRUE;
                    }
                    else if (MouseinWindow (gebform.mask[6].feldid, &mousepos))
                    {
                          current_geb = 6;
                          EnterCalcBox (GebWindow, "       Tara       ",
                                       gtara, gebform.mask[6].length,
                                       gebform.mask[6].picture);
                          display_form (GebWindow, &gebform, 0, 0);
                          return TRUE;
                    }
              }
              case WM_USER :
                    if (LOWORD (msg->wParam) == BUTARA)
                    {
                          dotara ();
                          return TRUE;
                    }
         }
         return FALSE;
}


int dogebinde ()
/**
Parameter fuer Gebinde - und Palettenetikett eingeben.
**/
{
        MSG msg;
        int  x, y;
        int cx, cy;

        cx = gebcx;
        x = (screenwidth - cx) / 2;
        cy = gebcy;
        y = (screenheight - cy) / 2;

        GebWindow  = CreateWindowEx (
                              0,
                              "GebWind",
                              "",
                              WS_POPUP | WS_DLGFRAME | WS_VISIBLE,
                              x, y,
                              cx, cy,
                              WiegWindow,
                              NULL,
                              hMainInst,
                              NULL);
        if (GebWindow == NULL) return (0);

        x = 5;
        cx -= 15;
        y = buttony;
        cy = bsz0 + 10;

        GebWindButton  = CreateWindowEx (
                              WS_EX_CLIENTEDGE,
                              "GebWindB",
                              "",
                              WS_CHILD | WS_VISIBLE,
                              x, y,
                              cx, cy,
                              GebWindow,
                              NULL,
                              hMainInst,
                              NULL);

        EnableWindow (WiegWindow, FALSE);
        create_enter_form (GebWindow, &gebform, 0, 0);
        current_geb = 0;
        SetGebFocus ();
        break_geb = 0;
        while (GetMessage (&msg, NULL, 0, 0))
        {
              if (IsGebMessage (&msg));
              else
              {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
              }
              if (break_geb) break;
        }
        CloseControls (&gebub);
        CloseControls (&gebform);
        CloseControls (&GebButton);
        DestroyWindow (GebWindButton);
        DestroyWindow (GebWindow);
        GebWindow = NULL;
        SetActiveWindow (WiegWindow);
        EnableWindow (WiegWindow, TRUE);
        update_kun_geb ();
        return (0);
}

void ToVorgabe (char *veinh, short einh)
/**
Vorgabe-Einheit als Klartext.
**/
{
        switch (einh)
        {
           case 1 :
                strcpy (veinh, "St�ck");
                break;
           case 2:
                strcpy (veinh, "kg");
                break;
           case 4:
                strcpy (veinh, "Preis");
                break;
           case 5:
                strcpy (veinh, "Karton");
                break;
           case 6:
                strcpy (veinh, "Palette");
                break;
           default:
                strcpy (veinh, "kg");
                break;
        }
}


void gebindelsp (void)
/**
Gebindevorgaben in lsp testen.
Wenn brauchbare Gebindewerte in der lsp vorhanden sind,
Gebindewerte aus a_kun_gx ueberschreiben.
**/
{
/*
        short   me_einh_kun1;
        double  auf_me1;
        double  inh1;
        short   me_einh_kun2;
        double  auf_me2;
        double  inh2;
        short   me_einh_kun3;
        double  auf_me3;
        double  inh3;

        me_einh_kun1 = atoi (aufps.me_einh_kun1);
        auf_me1      = ratod (aufps.auf_me1);
        inh1         = ratod (aufps.inh1);

        me_einh_kun2 = atoi (aufps.me_einh_kun2);
        auf_me2      = ratod (aufps.auf_me2);
        inh2         = ratod (aufps.inh2);

        me_einh_kun3 = atoi (aufps.me_einh_kun3);
        auf_me3      = ratod (aufps.auf_me3);
        inh3         = ratod (aufps.inh3);

        if (me_einh_kun1 <= 0 &&
            me_einh_kun2 <= 0 &&
            me_einh_kun3 <= 0)
        {
            return;
        }

        if (me_einh_kun1 == 2)
        {
              a_kun_geb.geb_fill = 2;
              sprintf (geb_einh, "%hd", a_kun_geb.geb_fill);
              ToVorgabe (veinh1, a_kun_geb.geb_fill);
        }
        else
        {
              a_kun_geb.geb_fill = 1;
              sprintf (geb_einh, "%hd", a_kun_geb.geb_fill);
              ToVorgabe (veinh1, a_kun_geb.geb_fill);
        }

        if (me_einh_kun2 == 5)
        {
              a_kun_geb.pal_fill = 5;
              sprintf (pal_einh, "%hd", a_kun_geb.pal_fill);
              ToVorgabe (veinh2, a_kun_geb.pal_fill);
        }
        else if (me_einh_kun2 == 2)
        {
              a_kun_geb.pal_fill = 2;
              sprintf (pal_einh, "%hd", a_kun_geb.pal_fill);
              ToVorgabe (veinh2, a_kun_geb.pal_fill);
        }
        else
        {
              a_kun_geb.pal_fill = 1;
              sprintf (pal_einh, "%hd", a_kun_geb.pal_fill);
              ToVorgabe (veinh2, a_kun_geb.pal_fill);
        }

        if (auf_me2 > (double) 0.0 && me_einh_kun2)
        {
              if (inh3)
              {
                  auf_me2 = inh3;
              }
              sprintf (pal_me,   "%.0lf",  auf_me2);
              strcpy (vorgabe2, pal_me);
        }
		else if (inh3)
		{
              sprintf (pal_me,   "%.3lf",  inh3);
              strcpy (vorgabe2, pal_me);
		}

        if (auf_me1 > (double) 0.0 && me_einh_kun1)
        {
              if (inh2)
              {
                  auf_me1 = inh2;
              }
              sprintf (geb_me,   "%.3lf", auf_me1);
              strcpy (vorgabe1, geb_me);
        }
		else if (inh2)
		{
              sprintf (geb_me,   "%.3lf",  inh2);
              strcpy (vorgabe1, geb_me);
		}
*/
}


void fetch_std_geb (void)
/**
Standard-Gebindeeinstellungen aus a_kun lesen und in kun_geb uebertragen.
Der Eintrag wird nur temporaer benutzt        .
**/
{
/*
        short sqm;
        extern short sql_mode;


        strcpy (a_kun_geb.geb_eti, "N");
        strcpy (a_kun_geb.pal_eti, "N");
        a_kun_geb.geb_fill = 0;
        a_kun_geb.pal_fill = 0;
        a_kun_geb.geb_anz = 0;
        a_kun_geb.pal_anz = 0;

        a_kun_geb.mdn = 1;
        a_kun_geb.fil = 0;
        a_kun_geb.kun = lsk.kun;
        a_kun_geb.a   = ratod (aufps.a);
        a_kun_geb.tara   = 0;
        a_kun_geb.mhd   = 0;
        strcpy (gebinde, a_kun_geb.geb_eti);

        sprintf (geb_einh, "%hd", a_kun_geb.geb_fill);
        ToVorgabe (veinh1, a_kun_geb.geb_fill);

        sprintf (geb_me,   "%.3lf",
            (double) ((double) a_kun_geb.geb_anz / 1000));
        strcpy (vorgabe1, geb_me);

        strcpy (palette, a_kun_geb.pal_eti);

        sprintf (pal_einh, "%hd", a_kun_geb.pal_fill);
        ToVorgabe (veinh2, a_kun_geb.pal_fill);

        sprintf (pal_me,   "%.3lf",
            (double) ((double) a_kun_geb.pal_anz / 1000));
        strcpy (vorgabe2, pal_me);

        gebindelsp ();
        a_kun_geb.geb_anz = (long) (double) (ratod (geb_me) * 1000);
        a_kun_geb.pal_anz = (long) (double) (ratod (pal_me) * 1000);

        sprintf (gtara,     "%.3lf", a_kun_geb.tara);

        if (tara_kz == 1)
        {
                  sprintf (gtara,     "%.3lf", a_hndw.tara);
                  a_kun_geb.tara =  a_hndw.tara;
        }
        if (mhd_kz == 1)
        {
              if (_a_bas.hbk_kz[0] == 'T' ||
                  _a_bas.hbk_kz[0] <= ' ')
              {
                  a_kun_geb.mhd =  _a_bas.hbk_ztr;
              }
        }
        sprintf (aufps.tara,     "%.3lf", a_kun_geb.tara);
        sqm = sql_mode;
        sql_mode = 1;
        geb_class.update_kun_geb ();
        sql_mode = sqm;
*/
}

void updategeblsp (void)
/**
Gebinde in lsp aktialisieren.
**/
{
/*
 	    sprintf (aufps.inh2, "%.3lf", (double) ((double)
				        a_kun_geb.geb_anz / 1000));
	    sprintf (aufps.inh3, "%.3lf", (double) ((double)
				        a_kun_geb.pal_anz / 1000));

		sprintf (aufps.me_einh_kun1, "%d", a_kun_geb.geb_fill);
		sprintf (aufps.me_einh_kun2, "%d", a_kun_geb.pal_fill);

        memcpy (&aufparr [aufpidx], &aufps, sizeof (struct AUFP_S));
*/
}


void update_kun_geb (void)
/**
Tabelle kun_geb aktualisieren.
**/
{
/*
        short sqm;
        extern short sql_mode;

        sqm = sql_mode;
        strcpy (a_kun_geb.geb_eti, gebinde);
        a_kun_geb.geb_fill = atoi (geb_einh);
        ToVorgabe (veinh1, a_kun_geb.geb_fill);
        strcpy (vorgabe1, geb_me);
        a_kun_geb.geb_anz =  (long) (double) (ratod (geb_me) * 1000);

        strcpy (a_kun_geb.pal_eti, palette);
        a_kun_geb.pal_fill = atoi (pal_einh);
        ToVorgabe (veinh2, a_kun_geb.pal_fill);
        strcpy (vorgabe2, pal_me);
        a_kun_geb.pal_anz =  (long) (double) ratod (pal_me) * 1000;
        a_kun_geb.tara    =  ratod (gtara);
        sprintf (aufps.tara,     "%.3lf", a_kun_geb.tara);
        sql_mode = 1;
        geb_class.update_kun_geb ();
        sql_mode = sqm;
        updategeblsp ();
*/
}


/**

Neuen Lieferschein eingeben.

**/

mfont LiefKopfFontT = {"Courier New", 200, 0, 1, BLACKCOL,
                                                RGB (0, 255, 255),
                                                0,
                                                NULL};

mfont LiefKopfFontE = {"Courier New", 200, 0, 1,
                                                RGB (0, 0, 0),
                                                RGB (255, 255, 255),
                                                0,
                                                NULL};


mfont LiefKopfFontTD = {"Courier New", 150, 0, 1, BLACKCOL,
                                                RGB (0, 255, 255),
                                                0,
                                                NULL};

mfont LiefKopfFontED = {"Courier New", 150, 0, 1,
                                                RGB (0, 0, 0),
                                                RGB (255, 255, 255),
                                                0,
                                                NULL};


static char KundeFil0 [14] = {"Kunden-Nr   :"};
static char KundeFil1 [14] = {"Filial-Nr   :"};
static char kunfil[3];
static long gen_lief_nr;

static field _LiefKopfT[] = {
"Datum  :",     16, 0, 1, 2, 0, "", DISPLAYONLY, 0, 0, 0,
};

form LiefKopfT = {1, 0, 0, _LiefKopfT, 0, 0, 0, 0, &LiefKopfFontT};


static field _LiefKopfTD[] = {
lgr.lgr_bz,        24, 0, 5,40, 0, "", DISPLAYONLY, 0, 0, 0,
"WE.Datum    : ", 13, 0, 10, 2, 0, "", REMOVED, 0, 0, 0,
"Bel.Eing.Dat: ", 13, 0, 11, 2, 0, "", REMOVED, 0, 0, 0,
"Status      : ", 13, 0,13, 2, 0, "", REMOVED, 0, 0, 0,
ls_stat_txt,      16, 0,13,28, 0, "", REMOVED, 0, 0, 0
};

form LiefKopfTD = {5, 0, 0, _LiefKopfTD, 0, 0, 0, 0, &LiefKopfFontTD};

static int ls_stat_pos = 4;
static int setlspos = 0;
static int SetKunFil (void);
static int ReadLief (void);
static int ReadUmlgr (void);


static field _LiefKopfE[] = {
  ein_dat,               17, 0, 1,18, 0, "dd.mm.yyyy", EDIT,        0, ReadUmlgr, 0,
};

form LiefKopfE = {1, 0, 0, _LiefKopfE, 0, 0, 0, 0, &LiefKopfFontE};


static field _LiefKopfED[] = {
  lieferdatum,         11, 0, 10,18, 0, "dd.mm.yyyy",
                                              REMOVED,     0, 0, 0,
  beldatum,            11, 0, 11,18, 0, "dd.mm.yyyy",
                                              REMOVED,     0, 0, 0,
  ls_stat,              2, 0,13,18, 0, "",
                                              REMOVED, 0, 0, 0
};

form LiefKopfED = {3, 0, 0, _LiefKopfED, 0, 0, 0, 0, &LiefKopfFontED};


static int lspos = 0;
static int lsend = 4;

void FillLSForm (void)
/**
Datenbankfelder in Maske uebertragn.
**/
{
/*
             dlong_to_asc (we_kopf.we_dat, lieferdatum);
             dlong_to_asc (we_kopf.blg_eing_dat, beldatum);
             strcpy (lieferzeit, lsk.lieferzeit);
             sprintf (ls_stat, "%hd", we_kopf.we_status);
             strcpy (kun_krz1, _adr.adr_krz);
			 sprintf (ls_stat, "%hd", we_kopf.we_status);
*/
/*
             ptab_class.lese_ptab ("we_stat", ls_stat);
             strcpy (ls_stat_txt, ptabn.ptbezk);
*/
}

void FromLSForm (void)
/**
Datenbankfelder in Maske uebertragn.
**/
{
             short mdn;
             short fil;

             mdn = Mandant;
             fil = Filiale;

             we_kopf.we_dat       = dasc_to_long (lieferdatum);
             we_kopf.blg_eing_dat = dasc_to_long (beldatum);
             we_kopf.we_status    = atoi (ls_stat);
}

static int readstatus = 0;
static int freestat = 0;

void FreeEinNr (void)
/**
Lieferschein-Nummer freigeben.
**/
{
     short mdn;
     short fil;

	 return;
     mdn = Mandant;
     fil = Filiale;

     if (freestat == 1) return;

     beginwork ();
     dsqlstatus = nveinid (mdn, fil,
                          "umlgr_nr",  gen_lief_nr);
     commitwork ();
     freestat = 1;
}

void UpdateLfd (void)
/**
Laufende Nummer in we_kopf updaten.
**/
{
	  char sqlbuffer [1024];

	  sprintf (sqlbuffer, "update we_kopf set lfd = %ld "
		                  "where mdn = %hd "
						  "and   fil = %hd "
                          "and   lief_rech_nr = \"%s\" "
                          "and   lief = \"%s\" "
                          "and   blg_typ = \"%s\"",
						  we_kopf.lfd, we_kopf.mdn, we_kopf.fil,
						  we_kopf.lief_rech_nr, we_kopf.lief, we_kopf.blg_typ);
	  DbClass.sqlcomm (sqlbuffer);
}


int WriteLS (void)
/**
Lieferscheinkopf schreiben.
**/
{
    char datum [12];
    long dat1, dat2;

    if (readstatus > 1) return 0;

	if (strcmp (lief_nr,   "                ") <= 0) return 0;
	if (strcmp (lieferant, "                ") <= 0) return 0;

    if (TestQs () == FALSE)
    {
        return 0;
    }

    sysdate (datum);
    dat1 = dasc_to_long (datum);
    dat2 = dasc_to_long (lieferdatum);
    if (dat1 > dat2)
    {
        disp_messG ("Lieferdatum nicht korrekt", 2);
        return (0);
    }

	if (numeric (we_kopf.lief))
	{
		we_kopf.lief_s = atol (we_kopf.lief);
	}

    FromLSForm ();
    if (Lock_Lgr_umlgrk () == FALSE)
    {
        PostQuitMessage (0);
        break_lief_enter = 1;
        rollbackwork ();
        return 0;
    }
    beginwork ();
	if (neuer_we)
	{
         we_kopf.lfd = AutoNr.GetGJ (1, 0, "we_nr");
		 UpdateLfd ();
	}
    WeKopfClass.dbupdate ();
    ActivateColButton (BackWindow2, &MainButton, 1, 1, 1);
    set_fkt (menfunc2, 6);
    commitwork ();
	neuer_we = FALSE;
	if (liste_direct)
	{
		menfunc2 ();
	}
    return 0;
}


int  ReadLfd (void)
/**
Laufende Nummer in we_kopf updaten.
**/
{
	  char sqlbuffer [1024];
	  int dsqlstatus;


      DbClass.sqlout ((short *) &we_kopf.lfd, 1, 0);

	  sprintf (sqlbuffer, "select lfd from we_kopf "
		                  "where mdn = %hd "
						  "and   fil = %hd "
                          "and   lief_rech_nr = \"%s\" "
                          "and   lief = \"%s\" "
                          "and   blg_typ = \"%s\"",
						  we_kopf.mdn, we_kopf.fil,
						  we_kopf.lief_rech_nr, we_kopf.lief, we_kopf.blg_typ);
	  dsqlstatus = DbClass.sqlcomm (sqlbuffer);
      return dsqlstatus;
}


int ReadUmlgr (void)
/**
lgr_umlgrk nach Datum und lagerab und lagerzu suchen
**/
{
	        int dsqlstatus;
	        char datum [12];
            readstatus = 0;
//			int i;

			if (strcmp (ein_dat, "                 ") <= 0) return 0;
//			sysdate (datum);
            freestat = 0;
			lgr_umlgrk.mdn = Mandant;
		    lgr_umlgrk.dat = dasc_to_long (ein_dat);
		    lgr_umlgrk.lgr_von = LagerAb;
		    lgr_umlgrk.lgr_na = LagerZu;
            dsqlstatus = Lgr_umlgrkClass.dbreadfirst ();
            if (dsqlstatus == 0 && lgr_umlgrk.stat == 4)
			{
			}

            if (dsqlstatus == 0 && lgr_umlgrk.stat < 6)
            {

				   we_kopf.bearb = dasc_to_long (datum);
                   beginwork ();
                   if (Lock_Lgr_umlgrk () == FALSE)
                   {
                          break_lief_enter = 1;
                          return -1;
                   }
                   ActivateColButton (BackWindow3, &MainButton2, 2, 1, 1);
                   ActivateColButton (BackWindow2, &MainButton,  1, 1, 1);
				   FreeEinNr();

                   set_fkt (menfunc2, 6);
//  			       if (WeWiepro == 1) Read_We_Wiepro ();
                   FillLSForm ();
				   neuer_we = FALSE;
  	               if (liste_direct)
				   {
		                    menfunc2 ();
							return 0;
				   }
            }
            else if (dsqlstatus == 100)
            {
//				   disp_messG ("Neuer Einsenderschein", 1);
                   ActivateColButton (BackWindow2, &MainButton, 2, 1, 1);


                   beginwork ();
				   Lgr_umlgrkClass.dbupdate ();
                   if (Lock_Lgr_umlgrk () == FALSE)
                   {
                          break_lief_enter = 1;
                          return -1;
                   }
                   ActivateColButton (BackWindow3, &MainButton2,  2,  1, 1);
                   ActivateColButton (BackWindow2, &MainButton,   1,  -1, 1);
                   set_fkt (menfunc2, 6);
//  			       if (WeWiepro == 1) Read_We_Wiepro ();
//                   FillLSForm ();
				   neuer_we = TRUE;
  	               if (liste_direct)
				   {
		                    menfunc2 ();
							return 0;
				   }
            }
//			if (WeWiepro == 1) Read_We_Wiepro ();
            return 0;
}



int SetKunFil (void)
{

          display_form (AufKopfWindow, &LiefKopfTD, 0, 0);
          display_form (AufKopfWindow, &LiefKopfED, 0, 0);
          return (0);
}


void FillLskAdr (void)
/**
Adressfelder in Lsk fuellen.
**/
{
}

int ReadLief (void)
{
          short mdn;
          short fil;

          mdn = Mandant;
		  fil = Filiale;

          lief.mdn = mdn;
          lief.fil = fil;
		  strcpy (lief.lief, lieferant);
          kun_krz1[0] = (char) 0;
          dsqlstatus = LiefClass.dbreadfirst ();
		  if (dsqlstatus == 100 && lief.mdn)
		  {
			         lief.mdn = 0;
                     dsqlstatus = LiefClass.dbreadfirst ();
		  }
          if (dsqlstatus == 0)
          {
                      adr_class.lese_adr (lief.adr);
                      strcpy (lief_krz1, _adr.adr_krz);
                      FillLskAdr ();
          }
          if (dsqlstatus)
          {
			      print_messG (2, "Lieferant %s nicht gefunden",
					               lieferant);
				  lspos = 1;
				  setlspos = 1;
                  strcpy (lieferant, " ");
				  return 100;
          }

          current_form = &LiefKopfTD;
          display_field (AufKopfWindow, &LiefKopfTD.mask[0], 0, 0);
          return 0;
}


void DisplayLiefKopf (void)
/**
Masken fuer LiefKopf anzeigen.
**/
{
          display_form (AufKopfWindow, &LiefKopfT, 0, 0);
          display_form (AufKopfWindow, &LiefKopfTD, 0, 0);
          if (LiefKopfE.mask[0].feldid)
          {
              display_form (AufKopfWindow, &LiefKopfE, 0, 0);
              display_form (AufKopfWindow, &LiefKopfED, 0, 0);
          }
}

void CloseLiefKopf (void)
/**
Masken fuer LiefKopf anzeigen.
**/
{
        CloseControls (&LiefKopfT);
        CloseControls (&LiefKopfE);
        CloseControls (&LiefKopfTD);
        CloseControls (&LiefKopfED);
}

void GetEditTextL (void)
/**
Text aus Editierfeld holen.
**/
{
        switch (lspos)
        {
                 case 0 :
                    GetWindowText (LiefKopfE.mask [0].feldid,
                                   LiefKopfE.mask [0].feld,
                                   LiefKopfE.mask[0].length);
                    break;
                 case 1 :
                    GetWindowText (LiefKopfE.mask [1].feldid,
                                   LiefKopfE.mask [1].feld,
                                   LiefKopfE.mask[1].length);
                    break;
                 case 2 :
                    GetWindowText (LiefKopfED.mask [0].feldid,
                                   LiefKopfED.mask [0].feld,
                                   LiefKopfED.mask[0].length);
                    break;
                 case 3 :
                    GetWindowText (LiefKopfED.mask [1].feldid,
                                   LiefKopfED.mask [1].feld,
                                   LiefKopfED.mask[1].length);
                    break;
                 case 4 :
                    GetWindowText (LiefKopfED.mask [2].feldid,
                                   LiefKopfED.mask [2].feld,
                                   LiefKopfED.mask[2].length);
                    break;
        }
        if (lspos == 0 && LiefKopfE.mask[0].after)
        {
                    (*LiefKopfE.mask[0].after) ();
        }
        else if (lspos == 1 && LiefKopfE.mask[1].after)
        {
                    (*LiefKopfE.mask[1].after) ();
        }
        else if (lspos &&  LiefKopfED.mask[lspos - 2].after)
        {
                    (*LiefKopfED.mask[lspos - 2].after) ();
        }
}


void LiefAfter (int pos)
{
        lspos = pos;
        if (lspos == 0 && LiefKopfE.mask[0].after)
        {
                    (*LiefKopfE.mask[0].after) ();
        }
        else if (lspos == 1 && LiefKopfE.mask[1].after)
        {
                    (*LiefKopfE.mask[1].after) ();
        }
        else if (lspos &&  LiefKopfED.mask[lspos - 2].after)
        {
                    (*LiefKopfED.mask[lspos - 2].after) ();
        }
}

void SetLiefFocus (int pos)
/**
Focus auf Eingabefelder AuftragKopfsetzen.
**/
{
	    setlspos = 0;
        switch (pos)
        {
               case 0 :
                      SetFocus (LiefKopfE.mask[0].feldid);
                      SendMessage (LiefKopfE.mask[0].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
                      break;
               case 1 :
                      SetFocus (LiefKopfE.mask[1].feldid);
                      SendMessage (LiefKopfE.mask[1].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
                      break;
               default :

				   SetFocus (LiefKopfED.mask[pos - 2].feldid);
                      SendMessage (LiefKopfED.mask[pos - 2].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
                      break;
        }
}

void LiefFocus (void)
{
        SetLiefFocus (lspos);
}


int IsLiefMessage (MSG *msg)
/**
HotKey Testen.
**/
{
         POINT mousepos;

         switch (msg->message)
         {
              case WM_KEYDOWN :
              {
                     switch (msg->wParam)
                     {
                            case VK_F5 :
                                    break_lief_enter = 1;
                                    return TRUE;
                            case VK_F6 :
                                    syskey = KEY6;
                                    testkeys ();
                                    return TRUE;
                            case VK_F12 :
								    if (TestOK () == FALSE) return TRUE;
                                    syskey = KEY12;
                                    WriteLS ();
                                    return TRUE;
                            case VK_DOWN :
                            case VK_RETURN :
                                    GetEditTextL ();
									if (setlspos == 0)
									{
                                           lspos ++;
									}
                                    if (lspos > lsend - 1) lspos = 0;
                                    SetLiefFocus (lspos);
                                    return TRUE;
                            case VK_UP :
                                    GetEditTextL ();
									if (setlspos == 0)
									{
                                            lspos --;
									}
                                    if (lspos < 0) lspos = lsend - 1;
                                    SetLiefFocus (lspos);
                                    return TRUE;
                            case VK_TAB :
                                    GetEditTextL ();
                                    if (GetKeyState (VK_SHIFT) < 0)
                                    {
									        if (setlspos == 0)
											{
                                                    lspos --;
											}
                                            if (lspos < 0) lspos = lsend - 1;
                                            SetLiefFocus (lspos);
                                     }
                                     else
                                     {
        									if (setlspos == 0)
											{
                                                     lspos ++;
											}
                                            if (lspos > lsend - 1) lspos = 0;
                                            SetLiefFocus (lspos);
                                     }
                                      return TRUE;
                            default :
                                    return FALSE;
                     }
                     break;
              }
              case WM_LBUTTONDOWN :
              {
				    setlspos = 0;
                    GetCursorPos (&mousepos);

                    if (MouseinWindow (LiefKopfE.mask[0].feldid, &mousepos))
                    {
/*
                          EnterNumBox (AufKopfWindow, "  Lieferschein-Nr  ",
                                       lief_nr, 16, LiefKopfE.mask[0].picture);
*/
                          NuBlock.EnterNumBox (AufKopfWindow, "Schlachtdatum  ",
                                       ein_dat, 11, LiefKopfE.mask[0].picture, EntNuProc);
                          display_field (AufKopfWindow, &LiefKopfE.mask[0], 0, 0);
                          LiefAfter (0);
                          return TRUE;
                    }
                    else if (MouseinWindow (LiefKopfE.mask[1].feldid, &mousepos))
                    {
/*
                          EnterNumBox (AufKopfWindow, "  Lieferanten-Nr  ",
                                       lieferant, 16, LiefKopfE.mask[1].picture);
*/
                          NuBlock.SetChoise (doliefchoise, 0);
                          NuBlock.EnterNumBox (AufKopfWindow,"  Lieferanten-Nr  ",
                                               lieferant, 16, LiefKopfE.mask[1].picture,
	      									  EntNuProc);
                          display_field (AufKopfWindow, &LiefKopfE.mask[1], 0, 0);
                          LiefAfter (1);
                          return TRUE;
                    }
                    else if (MouseinWindow (LiefKopfED.mask[0].feldid, &mousepos))
                    {
                         NuBlock.EnterNumBox (AufKopfWindow,"  WE-Datum  ",
                                      lieferdatum, 11,
                                            LiefKopfED.mask[0].picture, EntNuProc);
                         display_field (AufKopfWindow, &LiefKopfED.mask[0], 0, 0);
                         LiefAfter (2);
//					     PostMessage (msg->hwnd, WM_KEYDOWN, VK_RETURN, 0l);
                         return TRUE;
                    }
                    else if (MouseinWindow (LiefKopfED.mask[1].feldid, &mousepos))
                    {
                         NuBlock.EnterNumBox (AufKopfWindow,"  Bel.Eing.Datum  ",
                                      beldatum, 11,
                                            LiefKopfED.mask[1].picture, EntNuProc);
                         display_field (AufKopfWindow, &LiefKopfED.mask[1], 0, 0);
                         LiefAfter (3);
//					     PostMessage (msg->hwnd, WM_KEYDOWN, VK_RETURN, 0l);
                         return TRUE;
                    }
             }
          }

          return FALSE;
}

void KonvLiefKopf (void)
/**
Spaltenposition von LiefKopfED anpassen.
**/
{
        static int KonvOK = 0;
        int z, s;
        int i;

        if (KonvOK) return;

        KonvOK = 1;
        for (i = 0; i < LiefKopfED.fieldanz; i ++)
        {
                    s = LiefKopfED.mask[i].pos [1];
                    KonvTextPos (LiefKopfE.font,
                                 LiefKopfED.font,
                                 &z, &s);
                    LiefKopfED.mask[i].pos [1] = s;
        }

        s = LiefKopfTD.mask[0].pos [1];
        KonvTextPos (LiefKopfE.font,
                    LiefKopfED.font,
                    &z, &s);
        LiefKopfTD.mask[0].pos [1] = s;
/*
        s = LiefKopfTD.mask[1].pos [1];
        KonvTextPos (LiefKopfE.font,
                    LiefKopfED.font,
                    &z, &s);
        LiefKopfTD.mask[1].pos [1] = s;

        s = LiefKopfTD.mask[2].pos [1];
        KonvTextPos (LiefKopfE.font,
                    LiefKopfED.font,
                    &z, &s);
        LiefKopfTD.mask[2].pos [1] = s;

        s = LiefKopfTD.mask[3].pos [1];
        KonvTextPos (LiefKopfE.font,
                    LiefKopfED.font,
                    &z, &s);
        LiefKopfTD.mask[3].pos [1] = s;
*/
}

void LiefInit (field *feld)
/**
Feld Initialisieren.
**/
{
        memset (feld->feld, ' ', feld->length);
        feld->feld[feld->length - 1] = (char) 0;
}

void InitLiefKopf (void)
/**
Auftrag-Kopf initialisieren.
**/
{
        int i;

        LiefKopfE.mask[0].feld[0] = (char) 0;

        for (i = 0; i < LiefKopfED.fieldanz; i ++)
        {
                      LiefInit (&LiefKopfED.mask[i]);
        }
        LiefInit (&LiefKopfTD.mask[0]);
        LiefInit (&LiefKopfTD.mask[ls_stat_pos]);
}

void GenEinNr (void)
/**
Einsenderscheinnummer generieren.
**/
{
        int dsqlstatus;
        short mdn;
        short fil;

        mdn = Mandant;
        fil = Filiale;
        beginwork ();
        dsqlstatus = nvholid (mdn, fil, "umlgr_nr");
        if (dsqlstatus == 100)
        {
              dsqlstatus = nvanmprf (mdn,
                                     fil,
                                     "umlgr_nr",
                                     (long) 1,
                                     (long) 999999,
                                     (long) 10,
                                      "");

              if (dsqlstatus == 0)
              {
                          dsqlstatus = nvholid (mdn,
                                                fil,
                                                "umlgr_nr");
              }
         }
         commitwork ();
         lgr_umlgrk.umlgr_nr = auto_nr.nr_nr;
		 sprintf (UmlgrNr,"%ld",lgr_umlgrk.umlgr_nr); 
         gen_lief_nr = auto_nr.nr_nr;
}



void LiefKopf (void)
/**
Auftragskopf bearbeiten.
**/
{
        RECT rect;
//        MSG msg;
        int x, y, cx, cy;
		char datum[12];

        if (KopfEnterAktiv) return;
        if (LiefEnterAktiv) return;

        MainBuInactiv ();
        freestat = 0;
        CloseControls (&AufKopfT);
        CloseControls (&AufKopfE);
        CloseControls (&AufKopfTD);
        CloseControls (&AufKopfED);

        lspos = 0;
        RegisterKopfEnter ();
        GetWindowRect (LogoWindow, &rect);
        x = rect.left;
        y = rect.top;
        cx = rect.right - rect.left;
        cy = rect.bottom - rect.top;

//		lsk.auf_art = auf_art;
        KonvLiefKopf ();
        InitLiefKopf ();
		if (lsdefault)
		{
			sysdate (datum);
			memcpy (lief_nr, &datum[6], 4);
			memcpy (&lief_nr[4], &datum[3], 2);
			memcpy (&lief_nr[6], &datum[0], 2);
			lief_nr[8] = 0;
		}
		sysdate (lieferdatum);
		sysdate (beldatum);
        sysdate (ein_dat);
        GenEinNr ();

        beginwork ();
        LiefEnterAktiv = 1;
		strcpy (ls_stat,"0");
        if (AufKopfWindow == 0)
        {
                  AufKopfWindow  = CreateWindowEx (
                              0,
                              "AufKopf",
                              "",
                              WS_POPUP,
                              x, y,
                              cx, cy,
                              hMainWindow,
                              NULL,
                              hMainInst,
                              NULL);
        }

        if (AufKopfWindow == NULL)
        {
                 LiefEnterAktiv = 0;
                 return;
        }
        ShowWindow (AufKopfWindow, SW_SHOW);
        UpdateWindow (AufKopfWindow);
        create_enter_form (AufKopfWindow, &LiefKopfE, 0, 0);
        create_enter_form (AufKopfWindow, &LiefKopfED, 0, 0);
        SetLiefFocus (lspos);
        break_lief_enter = 1;
         NuBlock.EnterNumBox (AufKopfWindow, "Datum",
          ein_dat, 11, LiefKopfE.mask[0].picture, EntNuProc);
          display_field (AufKopfWindow, &LiefKopfE.mask[0], 0, 0);
          LiefAfter (0);  //-> ReadUmlgrNr 

/* 
        while (GetMessage (&msg, NULL, 0, 0))
        {
              if (IsLiefMessage (&msg));
              else
              {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
              }
              if (break_lief_enter) break;
        }
*/
        CloseControls (&LiefKopfT);
        CloseControls (&LiefKopfE);
        CloseControls (&LiefKopfTD);
        CloseControls (&LiefKopfED);

        DestroyWindow (AufKopfWindow);
        AufKopfWindow = NULL;
        LiefEnterAktiv = 0;
        MainBuActiv ();
		rollbackwork ();
        ActivateColButton (BackWindow3, &MainButton2, 2, -1, 1);
}


/* Auswahl ueber Geraete (sys_peri    */

#define MCI 6
#define GX  16

struct SYSPS
{
        char sys [9];
        char peri_typ [3];
        char txt [61];
        char peri_nam [21];
};


struct SYSPS sysarr [50], sysps;


static int sysidx;
static int sysanz = 0;

mfont SysFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                1,
                                NULL};

static field _sysform [] =
{
        sysps.sys,      12, 1, 0, 1, 0, "%8d", DISPLAYONLY, 0, 0, 0,
        sysps.peri_typ,  4, 1, 0,13, 0, "",    DISPLAYONLY, 0, 0, 0,
        sysps.txt,      20, 1, 0,17, 0, "",    DISPLAYONLY, 0, 0, 0
};

static form sysform = {3, 0, 0, _sysform, 0, 0, 0, 0, &SysFont};

static field _sysub [] =
{
        "Ger�te-Nr",     12, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        "Typ",            4, 1, 0,13, 0, "", DISPLAYONLY, 0, 0, 0,
        "Ger�t",         20, 1, 0,17, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form sysub = {3, 0, 0, _sysub, 0, 0, 0, 0, &SysFont};

static field _sysvl [] =
{
    "1",                  1,  0, 0, 12, 0, "", NORMAL, 0, 0, 0,
    "1",                  1,  0, 0, 16, 0, "", NORMAL, 0, 0, 0,
};

static form sysvl = {2, 0, 0, _sysvl, 0, 0, 0, 0, &SysFont};

int ShowSysPeri (HWND hWnd, char * ptabarr, int ptabanz, char * ptab,
                 int ptsize, form *ptform, form *ptvl, form *ptub)
{
        RECT rect;
        RECT frmrect;
        int x, y, cx, cy;
        HWND shListBox;
        HWND slbox;
        struct LMENUE smenue;

        GetMenue (&smenue);
        MenTextSave (&smenue);
        shListBox = GethListBox ();
        slbox = Getlbox ();

        RegisterPt ();
        GetWindowRect (hMainWindow, &rect);
        GetFrmRectEx (ptform, ptform->font, &frmrect);

        x = rect.left + sysxplus;
        y = rect.top + 20;
        cx = frmrect.right + 15;
        cy = 185;

        set_fkt (endlist, 5);
        SetDblClck (IsPtClck, 0);
        PtListeAktiv = 1;
        SetAktivWindow (hWnd);
//        SetListEWindow (1);
        SetListFont (TRUE);
        spezfont (ptform->font);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_VISIBLE);
        PtWindow = OpenListWindowBoEx (x, y, cx, cy);
//        PtWindow = OpenListWindowEnEx (x, y, cx, cy);
        GetWindowRect (PtWindow, &rect);
        y = rect.bottom;
		cx = rect.right - rect.left;
        PtFussWindow = CreateWindow ("PtFuss",
                                    "",
                                    WS_POPUP | WS_VISIBLE | WS_DLGFRAME,
                                    x,
//                                    y + cy + 23,
                                    y,
                                    cx, 70,
                                    hWnd,
                                    NULL,
                                    hMainInst,
                                    NULL);
        if (ptvl) ElistVl (ptvl);
        if (ptub) ElistUb (ptub);
        EnableWindow (hMainWindow, FALSE);
        ShowElist ((char *) ptabarr,
                  ptabanz,
                  (char *) ptab,
                  (int) ptsize,
                  ptform);
//        SetMouseLock (TRUE);
        SetMouseLockProc (MouseInPt);
        EnterElist (PtWindow, (char *) ptabarr,
                             ptabanz,
                             (char *) ptab,
                             (int) ptsize,
                             ptform);
        CloseControls (&PtButton);
        CloseEWindow (PtWindow);
        SetActiveWindow (hMainWindow);
        EnableWindow (hMainWindow, TRUE);
        init_break_list ();
//        SetMouseLock (FALSE);
        SetMouseLockProc (NULL);
        DestroyWindow (PtFussWindow);
        PtListeAktiv = 0;
        SetMenue (&smenue);
        SethListBox (shListBox);
        Setlbox (slbox);
        SetAktivWindow (hWnd);
        return ptidx;
}


int AuswahlSysPeri (void)
/**
Ausahlfenster ueber Geraete anzeigen.
**/
{
        if (sysanz == 0)
        {
               dsqlstatus = sys_peri_class.lese_sys_peri ("order by sys");
               while (dsqlstatus == 0)
               {
                   if (sys_peri.peri_typ == MCI)
                   {
                       sprintf (sysarr[sysanz].sys, "%ld",
                                     sys_peri.sys);
                       sprintf (sysarr[sysanz].peri_typ,"%hd",
                                     sys_peri.peri_typ);
                       strcpy (sysarr[sysanz].txt, sys_peri.txt);
                       strcpy (sysarr[sysanz].peri_nam,
                                      sys_peri.peri_nam);
                       sysanz ++;
                       if (sysanz == 50) break;
                   }
                   dsqlstatus = sys_peri_class.lese_sys_peri ();
               }
        }
        if (sysanz == 0)
        {
               disp_messG ("Keine Ger�te gefunden", 1);
               return 0;
        }
        syskey = 0;
        sysidx = ShowSysPeri (LogoWindow, (char *) sysarr, sysanz,
                     (char *) &sysps,(int) sizeof (struct SYSPS),
                     &sysform, &sysvl, &sysub);
        if (syskey == KEY5 || syskey == KEYESC)
        {
                return 0;
        }
        if (atoi (sysarr[sysidx].peri_typ) == 6)
        {
                strcpy (waadir, sysarr[sysidx].peri_nam);
                clipped (waadir);
                sprintf (ergebnis, "%s\\ergebnis", waadir);
                WaaInit = 0;
                WaageInit ();
                sys_peri_class.lese_sys (atol (sysarr[sysidx].sys));
                la_zahl = sys_peri.la_zahl;
                AktKopf = 0;
        }
        else
        {
              auszeichner = atol (sysarr[sysidx].sys);
//            F�r Preisauszeichner noch nicht aktiv.
        }

        return sysidx;
}

// Wiegart ausw�hlen. Standard oder Entnahmeverwiegung

struct WGA
{
    char wiegart [30];
};

struct WGA wiegart;

struct WGA wgaarr[] = {"  1 Gewerblich",
                       "  2 Hausschlachtung",
                       "  3 Sanit�tsschlachtung",
                       0};

static int wgaidx;
static int wgaanz = 3;

mfont WgaFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                1,
                                NULL};

static field _wgaform [] =
{
        wiegart.wiegart,  22, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form wgaform = {1, 0, 0, _wgaform, 0, 0, 0, 0, &WgaFont};

static field _wgaub [] =
{
        "Schlachtart",       22, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form wgaub = {1, 0, 0, _wgaub, 0, 0, 0, 0, &WgaFont};

static field _wgavl [] =
{
    "1",                  1,  0, 0, 23, 0, "", NORMAL, 0, 0, 0,
};

static form wgavl = {0, 0, 0, _wgavl, 0, 0, 0, 0, &WgaFont};

int AuswahlSchlArt (void)
/**
Ausahlfenster ueber Schlachtart anzeigen.
**/
{

        Entnahmehand = FALSE;
        syskey = 0;
        wgaidx = ShowSysPeri (LogoWindow, (char *) wgaarr, wgaanz,
                     (char *) &wiegart,(int) sizeof (struct WGA),
                     &wgaform, &wgavl, &wgaub);
        if (syskey == KEY5 || syskey == KEYESC)
        {
                return 0;
        }
        return wgaidx;
}

// Lager ausw�hlen. Schwein oder Rind

struct LAGER
{
    char lgr [10];
    char lgr_bz [25];
};


struct LAGER lgrarr[50], lager;

static int lgridx;
static int lgranz = 0;

mfont LgrFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                1,
                                NULL};

static field _lgrform [] =
{
        lager.lgr,  8, 1, 0, 1, 0, "%ld", DISPLAYONLY, 0, 0, 0,
        lager.lgr_bz,  24, 1, 0, 9, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form lgrform = {2, 0, 0, _lgrform, 0, 0, 0, 0, &LgrFont};

static field _lgrub [] =
{
        "Lager",       8, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        "Bezeichnung",       24, 1, 0, 9, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form lgrub = {2, 0, 0, _lgrub, 0, 0, 0, 0, &LgrFont};

static field _lgrvl [] =
{
    "1",                  1,  0, 0, 9, 0, "", NORMAL, 0, 0, 0,
};

static form lgrvl = {1, 0, 0, _lgrvl, 0, 0, 0, 0, &LgrFont};

int AuswahlLager (int zuab, int lgrsav)
/**
Ausahlfenster ueber vorh. L�ger anzeigen.
**/
{
      while (TRUE)
	  {
        if (lgranz == 0)
        {
               dsqlstatus = lgr_class.lese_lgr ("order by lgr");
               while (dsqlstatus == 0)
               {
                       sprintf (lgrarr[lgranz].lgr, "%ld",
                                     lgr.lgr);
                       strcpy (lgrarr[lgranz].lgr_bz, lgr.lgr_bz);
                       lgranz ++;
                       if (lgranz == 50) break;
                   dsqlstatus = lgr_class.lese_lgr ();
               }
			if (zuab = -1)
			{
                       sprintf (lgrarr[lgranz].lgr, "%ld",
                                     -2);
                       strcpy (lgrarr[lgranz].lgr_bz, LagerMinus2);
                       lgranz ++;
                       sprintf (lgrarr[lgranz].lgr, "%ld",
                                     -1);
                       strcpy (lgrarr[lgranz].lgr_bz, LagerMinus1);
                       lgranz ++;
                       sprintf (lgrarr[lgranz].lgr, "%ld",
                                     0);
                       strcpy (lgrarr[lgranz].lgr_bz, LagerNull);
                       lgranz ++;
			}
        }
        if (lgranz == 0)
        {
               disp_messG ("Kein Lager gefunden", 1);
               return lgrsav;
        }
        syskey = 0;
        lgridx = ShowSysPeri (LogoWindow, (char *) lgrarr, lgranz,
                     (char *) &lager,(int) sizeof (struct LAGER),
                     &lgrform, &lgrvl, &lgrub);
        if (syskey == KEY5 || syskey == KEYESC)
        {
                return lgrsav;
        }
		if (atoi (lgrarr[lgridx].lgr) < -1) 
		{
              disp_messG ("Diese Auswahl ist zur Zeit\n"
                          "nicht m�glich", 2);
			continue;
		}
		break;
	  }

        return atoi (lgrarr[lgridx].lgr); 
}

// Auswahl �ber Wiegungen aus wiepro


mfont WiepFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                                 RGB (255, 255, 255),
                                 1,
                                 NULL};

static field _wiepform [] =
{
        wiep.a,      14, 1, 0,  1, 0, "%13.0f", DISPLAYONLY, 0, 0, 0,
        wiep.a_bz1,  20, 1, 0, 17, 0, "",       DISPLAYONLY, 0, 0, 0,
        wiep.brutto, 11, 1, 0, 39, 0, "%7.3",   DISPLAYONLY, 0, 0, 0,
        wiep.zeit,    6, 1, 0, 51, 0, "",       DISPLAYONLY, 0, 0, 0,
};

static form wiepform = {4, 0, 0, _wiepform, 0, 0, 0, 0, &WiepFont};

static field _wiepub [] =
{
        "Artikel",     15, 1, 0,  1, 0, "",   DISPLAYONLY, 0, 0, 0,
        "Bezeichnung", 22, 1, 0, 16, 0, "",   DISPLAYONLY, 0, 0, 0,
        "Gewicht",     12, 1, 0, 38, 0, "",   DISPLAYONLY, 0, 0, 0,
        "Zeit",         7, 1, 0, 50, 0, "",   DISPLAYONLY, 0, 0, 0,
};

static form wiepub = {4, 0, 0, _wiepub, 0, 0, 0, 0, &WiepFont};

static field _wiepvl [] =
{
    "1",                  1,  0, 0, 16, 0, "", NORMAL, 0, 0, 0,
    "1",                  1,  0, 0, 38, 0, "", NORMAL, 0, 0, 0,
    "1",                  1,  0, 0, 50, 0, "", NORMAL, 0, 0, 0,
};

static form wiepvl = {3, 0, 0, _wiepvl, 0, 0, 0, 0, &WiepFont};

int AuswahlWiePro (void)
/**
Ausahlfenster ueber Geraete anzeigen.
**/
{
        int cursor;
        int idx;

        sql_in ((char *)  &Mandant, 1, 0);
        sql_in ((char *)  &lgr_umlgrk.umlgr_nr , 2, 0);
        sql_out ((char *) wiep.a, 0, 14);
        sql_out ((char *) wiep.a_bz1, 0, 25);
        sql_out ((char *) wiep.brutto, 0, 11);
        sql_out ((char *) wiep.dat, 0, 11);
        sql_out ((char *) wiep.zeit, 0, 9);
        cursor = prepare_sql ("select we_wiepro.a, a_bas.a_bz1, me, dat, zeit "
                                      "from we_wiepro,a_bas "
                                      "where a_bas.a = we_wiepro.a "
                                      "and we_wiepro.mdn = ? "
                                      "and we_wiepro.lief_rech_nr = ? "
                                      "order by dat desc, zeit desc");

        idx = 0;
        while (fetch_sql (cursor) == 0)
        {
                memcpy (&wieparr[idx], &wiep, sizeof (wiep));
                idx ++;
                if (idx == 20)
                {
                    break;
                }
        }
        close_sql (cursor);
        wiepanz = idx;

        sysxplus = 0;
        syskey = 0;
        wiepidx = ShowSysPeri (WiegWindow, (char *) wieparr, wiepanz,
                     (char *) &wiep,(int) sizeof (struct WIEP),
                     &wiepform, &wiepvl, &wiepub);
        sysxplus = 50;
        if (syskey == KEY5 || syskey == KEYESC)
        {
                return -1;
        }
        return wiepidx;
}


/* Auswahl ueber Wiegungen                    */


struct WIEGT
{
        char wieg [5];
        char menge [12];
};


struct WIEGT wiegarr [1000], wiegt;


static int wiegidx;
static int wieganz = 0;

mfont WiegFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                1,
                                NULL};

static field _wiegform [] =
{
        wiegt.wieg,      9, 1, 0, 1, 0, "%7d",   DISPLAYONLY, 0, 0, 0,
        wiegt.menge,    13, 1, 0,10, 0, ":9.3f", DISPLAYONLY, 0, 0, 0,
};

static form wiegform = {2, 0, 0, _wiegform, 0, 0, 0, 0, &WiegFont};

static field _wiegub [] =
{
        "Wiegung",        9, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        "Menge",         13, 1, 0,10, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form wiegub = {2, 0, 0, _wiegub, 0, 0, 0, 0, &WiegFont};

static field _wiegvl [] =
{
    "1",                  1,  0, 0,  9, 0, "", NORMAL, 0, 0, 0,
    "1",                  1,  0, 0, 22, 0, "", NORMAL, 0, 0, 0,
};

static form wiegvl = {2, 0, 0, _wiegvl, 0, 0, 0, 0, &WiegFont};

int ShowWieg (HWND hWnd, char * ptabarr, int ptabanz, char * ptab,
                 int ptsize, form *ptform, form *ptvl, form *ptub)
{
        RECT rect;
        RECT frmrect;
        int x, y, cx, cy;
        HWND shListBox;
        HWND slbox;
        struct LMENUE smenue;

        GetMenue (&smenue);
        MenTextSave (&smenue);
        shListBox = GethListBox ();
        slbox = Getlbox ();

        RegisterPt ();
        GetWindowRect (hMainWindow, &rect);
        GetFrmRectEx (ptform, ptform->font, &frmrect);

        x = rect.left + 50;
        y = rect.top + 20;
        cx = frmrect.right + 15;
        cy = 185;

        set_fkt (endlist, 5);
        SetDblClck (IsPtClck, 0);
        PtListeAktiv = 1;
        SetAktivWindow (hWnd);
//        SetListEWindow (1);
        SetListFont (TRUE);
        spezfont (ptform->font);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_VISIBLE);
//        PtWindow = OpenListWindowEnEx (x, y, cx, cy);
        PtWindow = OpenListWindowBoEx (x, y, cx, cy);
//        PtWindow = OpenListWindowEnEx (x, y, cx, cy);
        GetWindowRect (PtWindow, &rect);
        y = rect.bottom;
		cx = rect.right - rect.left;
        PtFussWindow = CreateWindow ("PtFuss",
                                    "",
                                    WS_POPUP | WS_VISIBLE | WS_DLGFRAME,
                                    x,
//                                    y + cy + 23,
                                    y,
                                    cx, 70,
                                    hWnd,
                                    NULL,
                                    hMainInst,
                                    NULL);
        if (ptvl) ElistVl (ptvl);
        if (ptub) ElistUb (ptub);
        EnableWindow (hMainWindow, FALSE);
        ShowElist ((char *) ptabarr,
                  ptabanz,
                  (char *) ptab,
                  (int) ptsize,
                  ptform);
//        SetMouseLock (TRUE);
        SetMouseLockProc (MouseInPt);
        EnterElist (PtWindow, (char *) ptabarr,
                             ptabanz,
                             (char *) ptab,
                             (int) ptsize,
                             ptform);
        CloseControls (&PtButton);
        CloseEWindow (PtWindow);
        SetActiveWindow (hMainWindow);
        EnableWindow (hMainWindow, TRUE);
        init_break_list ();
//        SetMouseLock (FALSE);
        SetMouseLockProc (NULL);
        DestroyWindow (PtFussWindow);
        PtListeAktiv = 0;
        SetMenue (&smenue);
        SethListBox (shListBox);
        Setlbox (slbox);
        SetAktivWindow (hWnd);
        return ptidx;
}

int AuswahlWieg (void)
/**
Ausahlfenster ueber Wiegungen anzeigen
**/
{
        int i;
        int j;

        for (i = AktWiegPos - 1, j = 0; i >= 0; i --, j ++)
        {
            sprintf (wiegarr[j].wieg, "%d", i + 1);
            sprintf (wiegarr[j].menge, "%.3lf", AktPrWieg[i]);
        }
        wieganz = j;

        if (wieganz == 0)
        {
               return 0;
        }
        syskey = 0;
        wiegidx = ShowWieg (WiegWindow, (char *) wiegarr, wieganz,
                     (char *) &wiegt,(int) sizeof (struct WIEGT),
                     &wiegform, &wiegvl, &wiegub);
        if (syskey == KEY5 || syskey == KEYESC)
        {
                return 0;
        }
        StornoWieg = ratod (wiegarr[wiegidx].menge);
        return TRUE;
}


/* Auswahl ueber Wiegekoepfe                    */


struct WKOPF
{
        char nr [10];
        char akt [10];
};


struct WKOPF wkopfarr [20], wkopf;


static int kopfidx;
static int kopfanz = 0;

mfont KopfFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                1,
                                NULL};

static field _wkopfform [] =
{
        wkopf.nr,     11, 1, 0, 1, 0, "%4d",   DISPLAYONLY, 0, 0, 0,
        wkopf.akt,    13, 1, 0,12, 0, "",      DISPLAYONLY, 0, 0, 0,
};

static form wkopfform = {2, 0, 0, _wkopfform, 0, 0, 0, 0, &KopfFont};

static field _wkopfub [] =
{
        "Wiegekopf",       11, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        "  aktiv",         13, 1, 0,12, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form wkopfub = {2, 0, 0, _wkopfub, 0, 0, 0, 0, &KopfFont};

static field _wkopfvl [] =
{
    "1",                  1,  0, 0, 12, 0, "", NORMAL, 0, 0, 0,
    "1",                  1,  0, 0, 24, 0, "", NORMAL, 0, 0, 0,
};

static form wkopfvl = {2, 0, 0, _wkopfvl, 0, 0, 0, 0, &KopfFont};

int ShowWKopf (HWND hWnd, char * ptabarr, int ptabanz, char * ptab,
                 int ptsize, form *ptform, form *ptvl, form *ptub)
{
        RECT rect;
        RECT frmrect;
        int x, y, cx, cy;
        HWND shListBox;
        HWND slbox;
        struct LMENUE smenue;

        GetMenue (&smenue);
        MenTextSave (&smenue);
        shListBox = GethListBox ();
        slbox = Getlbox ();

        RegisterPt ();
        GetWindowRect (hMainWindow, &rect);
        GetFrmRectEx (ptform, ptform->font, &frmrect);

        x = rect.left + 50;
        y = rect.top + 20;
        cx = frmrect.right + 15;
		if (PtY > -1)
		{
			cy = PtY;
			PtY = -1;
		}
		else
		{
            cy = 185;
		}

        set_fkt (endlist, 5);
        SetDblClck (IsPtClck, 0);
        PtListeAktiv = 1;
        SetAktivWindow (hWnd);
        SetListFont (TRUE);
        spezfont (ptform->font);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_VISIBLE);
        PtWindow = OpenListWindowBoEx (x, y, cx, cy);
        GetWindowRect (PtWindow, &rect);
        y = rect.bottom;
		cx = rect.right - rect.left;
        PtFussWindow = CreateWindow ("PtFuss",
                                    "",
                                    WS_POPUP | WS_VISIBLE | WS_DLGFRAME,
                                    x,
                                    y,
                                    cx, 70,
                                    hWnd,
                                    NULL,
                                    hMainInst,
                                    NULL);
        if (ptvl) ElistVl (ptvl);
        if (ptub) ElistUb (ptub);
        EnableWindow (hMainWindow, FALSE);
        ShowElist ((char *) ptabarr,
                  ptabanz,
                  (char *) ptab,
                  (int) ptsize,
                  ptform);
//        SetMouseLock (TRUE);
//        SetMouseLockProc (MouseInPt);
        EnterElist (PtWindow, (char *) ptabarr,
                             ptabanz,
                             (char *) ptab,
                             (int) ptsize,
                             ptform);
        CloseControls (&PtButton);
        CloseEWindow (PtWindow);
        SetActiveWindow (hMainWindow);
        EnableWindow (hMainWindow, TRUE);
        init_break_list ();
//        SetMouseLock (FALSE);
//        SetMouseLockProc (NULL);
        DestroyWindow (PtFussWindow);
        PtListeAktiv = 0;
        SetMenue (&smenue);
        SethListBox (shListBox);
        Setlbox (slbox);
        SetAktivWindow (hWnd);
        return ptidx;
}


int AuswahlKopf (void)
/**
Ausahlfenster ueber Geraete anzeigen.
**/
{
        int i;

        for (i = 0; i < la_zahl; i ++)
        {
                       sprintf (wkopfarr[i].nr, "%d", i + 1);
                       if (i == AktKopf)
                       {
                               sprintf (wkopfarr[i].akt, "   * ");
                       }
                       else
                       {
                               sprintf (wkopfarr[i].akt, "     ");
                       }
        }
        kopfanz = i;
        if (kopfanz == 0)
        {
               return 0;
        }
        syskey = 0;
        kopfidx = ShowWKopf (LogoWindow, (char *) wkopfarr, kopfanz,
                     (char *) &wkopf,(int) sizeof (struct WKOPF),
                     &wkopfform, &wkopfvl, &wkopfub);
        if (syskey == KEY5 || syskey == KEYESC)
        {
                return 0;
        }
        return kopfidx;
}


/* Auswahl bei leeren Positionen   */

mfont KtFont = {"Courier New", 150, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                1,
                                NULL};

static char ktext [61];
static char ktextarr [10][61];

static int ktidx;

static field _ktform [] =
{
        ktext,     50, 1, 0, 1, 0, "",   DISPLAYONLY, 0, 0, 0,
};

static form ktform = {1, 0, 0, _ktform, 0, 0, 0, 0, &KtFont};

static field _ktub [] =
{
        "Auswahl",     50, 1, 0, 1, 0, "",   DISPLAYONLY, 0, 0, 0,
};

static form ktub = {1, 0, 0, _ktub, 0, 0, 0, 0, &KtFont};


BOOL LeerPosAuswahl ()
{
        static BOOL CpOK = FALSE;

        if (! CpOK)
        {
            strcpy (ktextarr[0],
                "Unbearbeitete Positionen ignorieren");
            strcpy (ktextarr[1],
                "Unbearbeitete Positionen l�schen");
            strcpy (ktextarr[2],
                "Ist-Menge = Soll-Menge");
        }
        SetHoLines (0);
        syskey = 0;
        if (eWindow)
        {
                EnableWindow (eWindow, FALSE);
                EnableWindow (eFussWindow, FALSE);
                EnableWindow (eKopfWindow, FALSE);
        }
        ktidx = ShowWKopf (eWindow, (char *) ktextarr, 3,
                     (char *) &ktext, 61,
                     &ktform, NULL, &ktub);
        if (eWindow)
        {
                EnableWindow (eWindow, TRUE);
                EnableWindow (eFussWindow, TRUE);
                EnableWindow (eKopfWindow, TRUE);
        }

        SetHoLines (1);
        if (syskey == KEY5 || syskey == KEYESC)
        {
                return FALSE;
        }
        switch (ktidx)
        {
                case 0 :
                    SetAllPosStat ();
                    return TRUE;
                case 1 :
                    DelLeerPos ();
                    return TRUE;
                case 2:
                    SollToIst ();
                    return TRUE;
        }
        return TRUE;
}



BOOL PosLeer (void)
/**
Reaktion auf nicht abgeschlossene Positionen.
**/
{
          EnableWindow (hMainWindow, FALSE);
          EnableWindow (eWindow, FALSE);
          EnableWindow (eFussWindow, FALSE);
          EnableWindow (eKopfWindow, FALSE);
          if (abfragejnG (eWindow,
                          "Es sind nicht bearbeitete Positionen vorhanden.\n"
                          "Abbrechen ?", "J"))
          {
              EnableWindow (hMainWindow, TRUE);
              EnableWindow (eWindow, TRUE);
              EnableWindow (eFussWindow, TRUE);
              EnableWindow (eKopfWindow, TRUE);
              return FALSE;
          }
          EnableWindow (hMainWindow, TRUE);
          EnableWindow (eWindow, TRUE);
          EnableWindow (eFussWindow, TRUE);
          EnableWindow (eKopfWindow, TRUE);
          return LeerPosAuswahl ();
}


BOOL TestPosis (void)
/**
Einzelne Positionen testen.
**/
{
          int i;

          for (i = 0; i < aufpanz_upd; i ++)
          {
              if (atoi (aufparr[i].s) < 3)
              {
                  return PosLeer ();
              }
          }
          return TRUE;
}


BOOL TestPosStatus (void)
/**
Test, ob der Lieferschein auf komplett gesetzt werden darf.
**/
{
          if (NewPosi () == FALSE)
          {
              disp_messG ("Der Lieferschein wird noch von einem\n"
                          "anderen Benutzer bearbeitet", 2);
              return FALSE;
          }

          if (TestPosis () == FALSE)
          {
              return FALSE;
          }
          return TRUE;
}

int LeerGutExist (void)
/**
Test, ob Leergutartikel erfasst wurden.
**/
{
          int i;
          int lganz;

          lganz = 0;
          for (i = 0; i < aufpanz_upd; i ++)
          {
              if (lese_a_bas (ratod (aufparr[i].a)) != 0)
              {
                  continue;
              }
              if (_a_bas.a_typ == 11)
              {
                  lganz += atoi (aufparr[i].lief_me);
              }
          }
          return lganz;
}

BOOL PrintAufkleber (int lganz)
/**
Druck fuer Aufkleber starten.
**/
{
/*
          char buffer [256];
          DWORD ret;

          sprintf (buffer, "%d", lganz);

          EnterCalcBox (eWindow," Anzahl Aufkleber", buffer, 9,
                                   "%4d");

/?
          EnterNumBox (eWindow,
              " Wieviele Aufkleber sollen ausgedruckt werden", buffer, 9,
                                   "%4d");
?/
          lganz = atoi (buffer);
          if (lganz == 0) return TRUE;

          if (strcmp (sys_ben.pers_nam, " ") <= 0)
          {
             strcpy (sys_ben.pers_nam, "fit");
          }
          sprintf (buffer, "rswrun 53800 0 %s %hd %hd %ld %d",
                                 clipped (sys_ben.pers_nam),
                                 lsk.mdn, lsk.fil, lsk.kun, lganz);
          SetAktivWindow (eWindow);

          ret = ProcExec (buffer, SW_SHOWMINIMIZED, -1, 0, -1, 0);
          SetAktivWindow (eWindow);
          if (ret == 0)
          {
              disp_messG ("Fehler beim Start des Etikettendrucks",2);
          }
          else
          {
              disp_messG ("Etikettendruck gestartet", 2);
          }
*/
          return TRUE;
}


BOOL LsLeerGut (void)
/**
Leergut testen.
**/
{
         int lganz;

         if (ls_leer_zwang == 0) return TRUE;
         if (lganz = LeerGutExist ())
         {
             return PrintAufkleber (lganz);
         }
         if (abfragejnG (eWindow, "Es wurde kein Leergutartikel erfasst\n"
                                  "Leergut erfassen", "J"))
         {
// Durch PostMessage wuerde das Fenster zum Erfassen von Artikeln
// geoeffnet.
//             PostMessage (eWindow, WM_KEYDOWN, VK_F6, 0l);
             return FALSE;
         }
         return TRUE;
}


/**
Leergut als Kopf oder Fusstext eingeben.
**/

#define LEERMAX 100

form *fAuswahlEinh;

struct LP
{
	double lart;
	int    lanz;
} LeerPosArt [LEERMAX];


static BOOL BreakLeerEnter = FALSE;
static HWND lPosWindow;
static HWND hWndLeer;

static char KopfText[61];
static char KopfTextTab[LEERMAX][61];
static char KopfTextFTab[10][61];
static int kopftextfpos;
static int kopftextfscroll;

static int kopftextpos = 0;
static int kopftextanz = 0;
static int ktpos = 0;
static int ktend = 0;
static TEXTMETRIC tmLP;
static BOOL CrCaret = FALSE;
static char *ztab[] = {" = ", " + "};
static int  zpos = 0;
static BOOL NumIn = FALSE;

static char *meeinhLP[100];
static char *meeinhLPK[100];
static double leer_art [100];

static int meeinhpos  = 0;
static int palettepos = 0;
static int ptanz = 0;
static int ptscroll = 0;
static int ptstart  = 0;
static int ptend    = 0;
static int LeerScrollDown (void);
static int LeerScrollUp (void);

mfont PosLeerFontE = {"Courier New", 200, 0, 1,
                                     RGB (0, 0, 0),
                                     RGB (255, 255, 255),
                                     1,
                                     NULL};

COLORREF LeerBuColor = RGB (0, 255, 255);
COLORREF LeerBuColorPf = RGB (0, 0, 255);


static field _PosLeerE[] = {
  KopfTextFTab[0],          61, 0, 0, 1, 0, "", DISPLAYONLY,        0, 0, 0,
  KopfTextFTab[1],          61, 0, 1, 1, 0, "", DISPLAYONLY,        0, 0, 0,
  KopfTextFTab[2],          61, 0, 2, 1, 0, "", DISPLAYONLY,        0, 0, 0,
  KopfTextFTab[3],          61, 0, 3, 1, 0, "", DISPLAYONLY,        0, 0, 0};

form PosLeerE = {4, 0, 0, _PosLeerE, 0, 0, 0, 0, &PosLeerFontE};

int StopPosLeer (void)
{
	     BreakLeerEnter = TRUE;
		 return (0);
  }


LONG FAR PASCAL PosLeerProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        switch(msg)
        {

             case WM_PAINT :
				    if (hWnd == hWndLeer)
					{
				           display_form (hWnd, &PosLeerE, 0, 0);
					}
                    break;

             case WM_KEYDOWN :
				    if (wParam == VK_F5)
					{
						syskey = KEY5;
						StopPosLeer ();
					}
					break;
             case WM_COMMAND :
                    if (LOWORD (wParam) == KEY5 ||
                        LOWORD (wParam) == VK_F5)
                     {
                            syskey = KEY5;
                            SendKey (VK_F5);
                            break;
                    }
                    if (LOWORD (wParam) == KEYCR)
                    {
                            syskey = KEYCR;
                            SendKey (VK_RETURN);
                            break;
                    }
                    if (LOWORD (wParam) == KEY12)
                    {
                            syskey = KEY12;
                            SendKey (VK_F12);
                            break;
                    }
                    if (LOWORD (wParam) == KEYUP)
                    {
                            syskey = KEYUP;
							if (currentfield > ptstart)
							{
								currentfield --;
							    SetFocus (fAuswahlEinh->mask[currentfield].feldid);
							}
							else
							{
								LeerScrollDown ();
							}
							meeinhpos = ptscroll + currentfield - 1;
                            break;
                    }
                    if (LOWORD (wParam) == KEYDOWN)
                    {
                            syskey = KEYUP;
							if (currentfield < ptend)
							{
								currentfield ++;
							    SetFocus (fAuswahlEinh->mask[currentfield].feldid);
							}
							else
							{
								LeerScrollUp ();
							}
							meeinhpos = ptscroll + currentfield - 1;
                            break;
					}

        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

static void KonvFormParams (void)
/**
Laenge des Eingabefeldes anpassen.
**/
{
	     static BOOL paramsOK = FALSE;
		 HDC hdc;
		 HFONT hFont, oldfont;
		 int i;

		 if (paramsOK) return;

		 paramsOK = TRUE;
         spezfont (&PosLeerFontE);
         hFont = SetWindowFont (hMainWindow);
         hdc = GetDC (hMainWindow);
         oldfont = SelectObject (hdc, hFont);
         GetTextMetrics (hdc, &tmLP);
         DelFont (SelectObject (hdc, oldfont));
         ReleaseDC (hMainWindow, hdc);

		 for (i = 0; i < PosLeerE.fieldanz; i ++)
		 {
			 PosLeerE.mask[i].length = PosLeerE.mask[i].length * (short) tmLP.tmAveCharWidth;
			 if (PosLeerE.mask[i].rows == 0)
			 {
				 PosLeerE.mask[i].rows = (int) (double) ((double) tmLP.tmHeight * 1.3);
			 }
			 else
			 {
				 PosLeerE.mask[i].rows = PosLeerE.mask[i].rows * (short) tmLP.tmHeight;
			 }

			 if (PosLeerE.mask[i].pos[0] != -1)
			 {
				 PosLeerE.mask[i].pos[0] = PosLeerE.mask[i].pos[0] * (short) tmLP.tmHeight;
			 }

			 if (PosLeerE.mask[i].pos[1] != -1)
			 {
				 PosLeerE.mask[i].pos[1] = PosLeerE.mask[i].pos[1] * (short) tmLP.tmAveCharWidth;
			 }
		 }
}

void SetLPCaret (void)
{
	int x,y;
	int CaretH;
	BOOL cret;

	DestroyCaret ();

    CaretH = (int) (double) ((double) tmLP.tmHeight * 1.1);
    SetFocus (PosLeerE.mask[kopftextfpos].feldid);
    cret = CreateCaret (PosLeerE.mask[kopftextfpos].feldid, NULL, 0, CaretH);
    CrCaret = TRUE;

	x = ktpos * tmLP.tmAveCharWidth;
	y = 0;
	cret = ShowCaret (PosLeerE.mask[kopftextfpos].feldid);
	cret = SetCaretPos (x,y);
	ShowCaret (PosLeerE.mask[kopftextfpos].feldid);
}

static void DispWindowText (HWND hWnd, char *text)
/**
Text in Fenster anzeigen.
**/
{
	 InvalidateRect (hWnd, NULL, TRUE);
	 UpdateWindow (hWnd);
}

static void ToKopfFtab (void)
/**
Kopfdelder aus Array in FormFelder uebertragen.
**/
{
      int i, j;

	  while (kopftextfscroll < 0) kopftextfscroll ++;

      for (i = 0, j = kopftextfscroll; i < PosLeerE.fieldanz; i ++, j ++)
	  {
		  strcpy (KopfTextFTab[i], KopfTextTab[j]);
	  }
	  display_form (hWndLeer, &PosLeerE, 0, 0);
}



static void EinhToStr (void)
/**
Einheit an String anhaengen.
**/
{
	char Einheit[20];
	char *MeH;

	if (NumIn == FALSE) return;


    strcpy (Einheit, meeinhLPK[meeinhpos]);
	MeH = strtok (Einheit, " ");
	strcat (KopfTextFTab[kopftextfpos], " ");
    strcat (KopfTextFTab[kopftextfpos], MeH);

	if (zpos == 0)
	{
	        strcat (KopfTextFTab[kopftextfpos], ztab [zpos]);
	        zpos = 2;
	}

	ktend = strlen (KopfTextFTab[kopftextfpos]);
	ktpos = ktend;
    DispWindowText (PosLeerE.mask[kopftextfpos].feldid, NULL);
	SetLPCaret ();
	NumIn = FALSE;
    SetFocus (fAuswahlEinh->mask[currentfield].feldid);
}

void TestKopfText (char *kText)
/**
Beim Verlassen einer Zeile, den Kopfinhalt testen.
**/
{
	 int len;
	 char *gpos;


	 len = strlen (kText);
	 if (kText[len - 1] != '=' &&
		 kText[len - 2] != '=')
	 {
		 return;
	 }

	 gpos = strchr (kText, '=');

	 for (gpos = gpos - 1;  gpos != kText; gpos -=1)
	 {
		 if (*gpos > ' ')
		 {
			 *(gpos + 1) = 0;
			 return;
		 }
	 }
}


void TestKopfTextBef (char *kText)
/**
Vor dem Enter einer Zeile, den Kopfinhalt testen.
**/
{
	 if (strcmp (kText, " ") <= 0) return;

	 if (strchr (kText, '=') == NULL)
	 {
		 strcat (kText, " = ");
	 }
}

void ScrollKopfTextDown (int zeilen)
/**
Kopftext nach unten scrollen.
**/
{

	if (kopftextfscroll > 0)
	{
		kopftextfscroll --;
	}
	ToKopfFtab ();
}


void ScrollKopfTextUp (int zeilen)
/**
Kopftext nach oben scrollen.
**/
{
	if (kopftextfscroll < 95)
	{
		kopftextfscroll ++;
	}
	ToKopfFtab ();
}


void NextKopfZeile (void)
{
	int pos;

	EinhToStr ();
	strcpy (KopfTextTab[kopftextpos], KopfTextFTab [kopftextfpos]);
	TestKopfText (KopfTextTab[kopftextpos]);
	if (kopftextpos > 59) return;

	if (strcmp (KopfTextTab[kopftextpos], " ") <= 0) return;

	kopftextpos ++;
	if (kopftextfpos < PosLeerE.fieldanz - 1)
	{
	          kopftextfpos ++;
	}
	else
	{
              ScrollKopfTextUp (1);
	}
	if (kopftextpos >= kopftextanz) kopftextanz = kopftextpos + 1;
    strcpy (KopfTextFTab[kopftextfpos], KopfTextTab[kopftextpos]);
	clipped (KopfTextFTab[kopftextfpos]);
	TestKopfTextBef (KopfTextFTab[kopftextfpos]);
    DispWindowText (PosLeerE.mask[kopftextfpos].feldid, NULL);
	ktend = strlen (KopfTextFTab[kopftextfpos]);
	strcpy (KopfText,KopfTextFTab[kopftextfpos]);
    ktpos = ktend;
	if (ktend == 0)
	{
 	         zpos = 0;
	}
	else
	{
		if (strchr (KopfText, '+'))
		{
		     zpos = 1;
		}
		else if (strchr (KopfText, '='))
		{
            pos = (int) strlen (KopfText);
			if (KopfText[pos - 1] == '=' ||
				KopfText[pos - 2] == '=')
			{
           	        zpos = 2;
			}
			else
			{
				    zpos = 1;
			}
		}
		else
		{
			zpos = 0;
		}
	}
	strcpy (KopfTextFTab[kopftextfpos], KopfText);
    NumIn = FALSE;
}


void PriorKopfZeile (void)
{
	int pos;

	EinhToStr ();
    GetWindowText (PosLeerE.mask[0].feldid, KopfText, 60);
	strcpy (KopfTextTab[kopftextpos], KopfTextFTab[kopftextfpos]);
	TestKopfText (KopfTextTab[kopftextpos]);
	if (kopftextpos < 1) return;
	kopftextpos --;
	if (kopftextfpos > 0)
	{
	          kopftextfpos --;
	}
	else
	{
              ScrollKopfTextDown (1);
	}
    strcpy (KopfTextFTab[kopftextfpos], KopfTextTab[kopftextpos]);
	strcpy (KopfText,KopfTextFTab[kopftextfpos]);
	clipped (KopfTextFTab[kopftextfpos]);
	TestKopfTextBef (KopfTextFTab[kopftextfpos]);
    DispWindowText (PosLeerE.mask[kopftextfpos].feldid, NULL);
	ktend = strlen (KopfTextFTab[kopftextfpos]);
	strcpy (KopfText,KopfTextFTab[kopftextfpos]);
    ktpos = ktend;
	if (ktend == 0)
	{
 	         zpos = 0;
	}
	else
	{
		if (strchr (KopfText, '+'))
		{
		     zpos = 1;
		}
		else if (strchr (KopfText, '='))
		{
            pos = (int) strlen (KopfText);
			if (KopfText[pos - 1] == '=' ||
				KopfText[pos - 2] == '=')
			{
           	        zpos = 2;
			}
			else
			{
				    zpos = 1;
			}
		}
		else
		{
			zpos = 0;
		}
	}
	strcpy (KopfTextFTab[kopftextfpos], KopfText);
    NumIn = FALSE;
}


void SetThisCaret(void)
{
	int pos;

	if (ktend == 0)
	{
 	         zpos = 0;
	}
	else
	{
		if (strchr (KopfText, '+'))
		{
		     zpos = 1;
		}
		else if (strchr (KopfText, '='))
		{
            pos = (int) strlen (KopfText);
			if (KopfText[pos - 1] == '=' ||
				KopfText[pos - 2] == '=')
			{
           	        zpos = 2;
			}
			else
			{
				    zpos = 1;
			}
		}
		else
		{
			zpos = 0;
		}
	}
    NumIn = FALSE;
	ktend = strlen (KopfText);
    ktpos = ktend;
	SetLPCaret ();
}


int ProcPosLeer (WPARAM wParam)
/**
Aktion auf Tasten in EnterNum ausfuehren.
**/
{
	      if (wParam >= (WPARAM) '0' &&
			  wParam <= (WPARAM) '9')
		  {
              	   if (NumIn == FALSE && zpos == 2)
				   {
	                      zpos = 1;
				   }
	               else if (NumIn == FALSE && zpos == 1)
				   {
	                      strcat (KopfTextFTab[kopftextfpos], ztab [zpos]);
						  ktpos = strlen (KopfTextFTab[kopftextfpos]);
				   }

			       KopfTextFTab[kopftextfpos][ktpos] = (char) wParam;
				   if (ktpos < 59) ktpos ++;
				   if (ktend < ktpos) ktend = ktpos;
			       KopfTextFTab[kopftextfpos][ktend] = (char) 0;
				   DispWindowText (PosLeerE.mask[kopftextfpos].feldid, NULL);
                   SetFocus (eNumWindow);
	               NumIn = TRUE;
		  }
		  else if (wParam == (WPARAM) ' ')
		  {
			       EinhToStr ();
		  }

		  else if (wParam == VK_F5)
		  {
			       syskey = KEY5;
  		           PostMessage (NULL, WM_USER, 0, 0);
                   PostMessage (NULL, WM_KEYDOWN,
                               (WPARAM) VK_F5, 0l);
                   SetFocus (eNumWindow);
		  }
		  else if (wParam == (WPARAM) '=')
		  {
			       EinhToStr ();
                   SetFocus (eNumWindow);
		  }
		  else if (wParam == (WPARAM) VK_RETURN)
		  {
			       EinhToStr ();
//                 SetFocus (eNumWindow);
			       syskey = KEYCR;
  		           PostMessage (NULL, WM_USER, 0, 0);
                   PostMessage (NULL, WM_KEYDOWN,
                               (WPARAM) VK_RETURN, 0l);
                   SetFocus (eNumWindow);
		  }
		  else if (wParam == VK_CLS)
		  {
 		          zpos = 0;
		          NumIn = FALSE;
                  memset (KopfTextFTab[kopftextfpos], ' ', 60);
                  KopfTextFTab[kopftextfpos] [0] = (char) 0;
				  ktpos = ktend = 0;
			      DispWindowText (PosLeerE.mask[kopftextpos].feldid, NULL);
                  SetFocus (eNumWindow);
		  }

		  else if (wParam == VK_DOWN)
		  {
			      NextKopfZeile ();
                  SetFocus (eNumWindow);
		  }
		  else if (wParam == VK_UP)
		  {
			      PriorKopfZeile ();
                  SetFocus (eNumWindow);
		  }

          SetLPCaret ();

          SetFocus (fAuswahlEinh->mask[currentfield].feldid);
		  return 1;
}

static int MeProc (HWND hWnd)
/**
Mengeneinheit wechseln.
**/
{
		  meeinhpos ++;
		  if (meeinhLP[meeinhpos] == NULL)
		  {
			  meeinhpos = 0;
		  }
          TextForm.mask[0].feld = meeinhLP[meeinhpos];
		  InvalidateRect (hWnd, NULL, TRUE);
		  return 0;
}

static void strzcpy (char *dest, char *source, int len)
/**
source in dest zentrieren.
**/
{
	    int zpos;

		memset (dest, ' ', len);
		dest[len] = (char) 0;
		if ((int) strlen (source) > len - 1)
		{
			source[len - 1] = (char) 0;
		}
		zpos = (int) (double) ((double) (len - strlen (source)) / 2 + 0.5);
		zpos = max (0, zpos);
		memcpy (&dest[zpos], source, strlen (source));
}

void FillEinhLP (void)
/**
Einheiten aus Prueftabelle lesen.
**/
{
	    static BOOL EinhOK = FALSE;
		int dsqlstatus;

		if (EinhOK) return;

		EinhOK = TRUE;

		ptanz = 0;
		palettepos = ptanz;
        dsqlstatus = ptab_class.lese_ptab_all ("me_einh_leer");
        while (dsqlstatus == 0)
        {
			if ((meeinhLP[ptanz] = (char *) malloc (40)) == 0)
			{
				break;
			}
			if ((meeinhLPK[ptanz] = (char *) malloc (10)) == 0)
			{
				break;
			}
            strzcpy (meeinhLP[ptanz],  clipped (ptabn.ptbez), 16);
            strcpy  (meeinhLPK[ptanz], clipped (ptabn.ptbezk));
			leer_art[ptanz] = ratod (ptabn.ptwer1);
			if (strupcmp (ptabn.ptbez, "PALETTE", 7) == 0)
			{
				palettepos = ptanz;
			}
            ptanz ++;
            if (ptanz == 99) break;
            dsqlstatus = ptab_class.lese_ptab_all ();
        }
		meeinhLP[ptanz]  = NULL;
 	    meeinhLPK[ptanz] = NULL;
		ptscroll = 0;
}

void LeerBuScrollDown (int start, int end, int ze)
{
	    int i;
		ColButton *ColBut1, *ColBut2;

        while (start - ze + 1 < 0) ze --;
		for (i = end; i > start; i --)
		{
			ColBut1 = (ColButton *) fAuswahlEinh->mask[i].feld;
			ColBut2 = (ColButton *) fAuswahlEinh->mask[i - ze].feld;
			ColBut1->text1 = ColBut2->text1;
		}
		ColBut1 = (ColButton *) fAuswahlEinh->mask[i].feld;
		ColBut1->text1 = "";
}


int GetMeEinhPos (char *text)
{
	    int i;

		for (i = 0; i < ptanz; i ++)
		{
			if (strcmp (text, meeinhLP[i]) == 0)
			{
				return i;
			}
		}
		return meeinhpos;
}


int SetLeerText (void)
{
		ColButton *ColBut;

	    ColBut = (ColButton *) fAuswahlEinh->mask[currentfield].feld;
		meeinhpos = GetMeEinhPos (ColBut->text1);
		EinhToStr ();
		return 0;
}

int LeerScrollUp (void)
{
        int i, j;
		int scrollanz;
		int scrollplus;
		ColButton *ColBut;

		scrollanz  = fAuswahlEinh->fieldanz - 2;
		scrollplus = fAuswahlEinh->fieldanz - 3;

		scrollplus = max (0, scrollplus);
        while (scrollplus > 0)
		{
			if ((ptscroll + scrollplus + scrollanz) <= ptanz)
			{
				break;
			}
			scrollplus --;
		}

		ptscroll += scrollplus;
		for (i = ptscroll, j = 1; j <= scrollanz; i ++, j ++)
		{
			ColBut = (ColButton *) fAuswahlEinh->mask[j].feld;
			ColBut->text1 = meeinhLP[i];
		}
		display_form (NuBlock.GeteNumWindow (), fAuswahlEinh, 0, 0);
		return 0;
}

int LeerScrollDown (void)
{
        int i, j;
		int scrollanz;
		int scrollplus;
		ColButton *ColBut;

		scrollanz  = fAuswahlEinh->fieldanz - 2;
		scrollplus = fAuswahlEinh->fieldanz - 3;

		scrollplus = max (0, scrollplus);
        while (scrollplus > 0)
		{
			if ((ptscroll - scrollplus) >= 0)
			{
				break;
			}
			scrollplus --;
		}


		ptscroll -= scrollplus;
		for (i = ptscroll, j = 1; j <= scrollanz; i ++, j ++)
		{
			ColBut = (ColButton *) fAuswahlEinh->mask[j].feld;
			ColBut->text1 = meeinhLP[i];
		}
		display_form (NuBlock.GeteNumWindow (), fAuswahlEinh, 0, 0);
		return 0;
}


void SetLeerBuColors (void)
{
	    int i;
		ColButton *ColBut;

		for (i = 0; i < fAuswahlEinh->fieldanz; i ++)
		{
			ColBut = (ColButton *) fAuswahlEinh->mask[i].feld;
			ColBut->BkColor = LeerBuColor;
		}
}

void SetLeerBuTxt (void)
{
	    int i;
		ColButton *ColBut;

		currentfield = 0;
		ptstart = 0;
		ptend = fAuswahlEinh->fieldanz - 1;
        if (pfeilu == NULL)
		{
                  pfeilu = LoadBitmap (hMainInst, "pfeilu");
		}
        if (pfeilo == NULL)
		{
                  pfeilo = LoadBitmap (hMainInst, "pfeilo");
		}

		for (i = 0; i < fAuswahlEinh->fieldanz; i ++)
		{
			if (meeinhLP[i] == NULL) break;
			ColBut = (ColButton *) fAuswahlEinh->mask[i].feld;
			ColBut->text1 = meeinhLP[i];
			fAuswahlEinh->mask[i].after = SetLeerText;
		}
		if (ptanz >= fAuswahlEinh->fieldanz)
		{
			i = fAuswahlEinh->fieldanz - 1;
			ColBut = (ColButton *) fAuswahlEinh->mask[i].feld;
			ColBut->text1 = "";
			ColBut->bmp   = pfeilu;
			ColBut->BkColor = LeerBuColorPf;
			ColBut->aktivate = 14;
			fAuswahlEinh->mask[i].after = LeerScrollUp;
 		    currentfield = 1;
			ptstart = 1;
			ptend = fAuswahlEinh->fieldanz - 2;

		}
        LeerBuScrollDown (0, fAuswahlEinh->fieldanz - 2, 1);
		ColBut = (ColButton *) fAuswahlEinh->mask[0].feld;
		ColBut->text1 = "";
		ColBut->bmp   = pfeilo;
		ColBut->BkColor = LeerBuColorPf;
		ColBut->aktivate = 14;
		fAuswahlEinh->mask[0].after = LeerScrollDown;
}

void EnterLeerPos (void)
/**
Leergut auf Positionsebene einegen.
**/
{
	    int x,y, cx, cy;
		HFONT hFont;
        HDC hdc;
		TEXTMETRIC tm;
		int i;
		int pos;
		HWND ParentWindow;

        fAuswahlEinh = NuBlock.GetAuswahlEinh ();
		if (GebTara)
		{
		        ParentWindow = WiegWindow;
		}
		else
		{
		        ParentWindow = eWindow;
		}
		FillEinhLP ();
		if (ptanz == 0)
		{
			disp_messG ("Keine Pr�ftabelleneintr�ge f�r Leergut", 2);
		    return;
		}
		RECT eRect, fRect;

 	    save_fkt (4);
		save_fkt (5);
		save_fkt (6);
		save_fkt (7);
		save_fkt (8);
		save_fkt (9);
		save_fkt (10);
		save_fkt (11);

		set_fkt (NULL, 4);
		set_fkt (NULL, 5);
		set_fkt (NULL, 6);
		set_fkt (NULL, 7);
		set_fkt (NULL, 8);
		set_fkt (NULL, 9);
		set_fkt (NULL, 10);
		set_fkt (NULL, 11);

		set_fkt (StopPosLeer, 5);

		NuBlock.SetNum3 (TRUE);

        if (eWindow)
        {
		      GetWindowRect (eWindow,     &eRect);
		      GetWindowRect (eFussWindow, &fRect);
        }
        else
        {
		      GetWindowRect (LogoWindow,  &eRect);
		      GetWindowRect (BackWindow3, &fRect);
        }


		EnableWindows (ParentWindow, FALSE);
		EnableWindows (BackWindow3, FALSE);
		EnableWindows (BackWindow2, FALSE);
        x  = eRect.left;
		y  = eRect.top;
		cx = eRect.right - eRect.left;
		cy = eRect.bottom - eRect.top + fRect.bottom - fRect.top;

        PosLeerFontE.FontHeight = 200;
        while (TRUE)
        {
                 spezfont (&PosLeerFontE);
                 hFont = SetWindowFont (hMainWindow);
                 hdc = GetDC (hMainWindow);
                 SelectObject (hdc, hFont);
                 GetTextMetrics (hdc, &tm);
                 DelFont (hFont);
                 ReleaseDC (hMainWindow, hdc);

                 if ((tm.tmAveCharWidth * 60) < cx) break;
                 PosLeerFontE.FontHeight -= 5;
                 if (PosLeerFontE.FontHeight <= 80) break;
        }
        KonvFormParams ();
        spezfont (&PosLeerFontE);
        hFont = SetWindowFont (hMainWindow);
        hdc = GetDC (hMainWindow);
        SelectObject (hdc, hFont);
        GetTextMetrics (hdc, &tm);
        for (i = 1; i < PosLeerE.fieldanz; i ++)
		{
			PosLeerE.mask[i].pos[0] = (int) (double)
				((double) PosLeerE.mask[i - 1].pos[0] + tm.tmHeight * 1.3);
		}

        lPosWindow = CreateWindow ("PosLeer",
                                    "",
                                    WS_VISIBLE | WS_POPUP | WS_DLGFRAME,
                                    x, y,
                                    cx, cy,
                                    ParentWindow,
                                    NULL,
                                    hMainInst,
                                    NULL);
		if (lPosWindow == NULL)
		{
  		    EnableWindows (ParentWindow, TRUE);
		    EnableWindows (BackWindow3,  TRUE);
		    EnableWindows (BackWindow2,  TRUE);
			return;
		}

		x = (cx - 62 * tm.tmAveCharWidth) / 2;
		y = (int) (double) ((double) 1 * tm.tmHeight * 1.3);
		cx = 62 * tm.tmAveCharWidth;
		cy = (int) (double) ((double) 4 * tm.tmHeight * 1.3);

		hWndLeer = CreateWindowEx (WS_EX_CLIENTEDGE,
			                       "PosLeerKopf",
								   "",
                                    WS_VISIBLE | WS_CHILD,
                                    x, y,
                                    cx, cy,
                                    lPosWindow,
                                    NULL,
                                    hMainInst,
                                    NULL);

		NuBlock.SetParent (lPosWindow);
		NuBlock.SetNumZent (TRUE, 0, (int) (double) ((double) 30 * scrfy));
//		FillEinhLP ();
		SetLeerBuColors ();
		SetLeerBuTxt ();
		ktpos = ktend = 0;
		meeinhpos = palettepos;
		kopftextpos = 0;
		zpos = 0;
		NumIn = FALSE;
		CrCaret = FALSE;
		KopfText[ktend] = (char) 0;
        create_enter_form (hWndLeer, &PosLeerE, 0, 0);

        NuBlock.SetEform (&PosLeerE, lPosWindow, ProcPosLeer);
        NuBlock.SetMeProc (MeProc);

        ReadKopftext ();
        strcpy (KopfTextFTab[0], KopfTextTab[0]);
        strcpy (KopfText, KopfTextTab[0]);
	    TestKopfTextBef (KopfTextFTab[0]);
   	    ktend = strlen (KopfTextFTab[0]);

        ktpos = ktend;
	    strcpy (KopfText,KopfTextFTab[0]);
	    if (ktend == 0)
		{
 	         zpos = 0;
		}
	    else
		{
		     if (strchr (KopfText, '+'))
			 {
		            zpos = 1;
			 }
		     else if (strchr (KopfText, '='))
			 {
                    pos = (int) strlen (KopfText);
			        if (KopfText[pos - 1] == '=' ||
				        KopfText[pos - 2] == '=')
					{
           	                  zpos = 2;
					}
			        else
					{
				              zpos = 1;
					}
			 }
		     else
			 {
			        zpos = 0;
			 }
		}

	    strcpy (KopfTextFTab[0], KopfText);
        NumIn = FALSE;

        DispWindowText (PosLeerE.mask[0].feldid, KopfTextFTab[0]);
        NuBlock.SetNumCaretProc (SetThisCaret);

        NuBlock.EnterCalcBox (lPosWindow, meeinhLP[1], "", 40, "", EntNuProc);
	    strcpy (KopfTextTab[kopftextpos], KopfTextFTab[kopftextfpos]);
	    TestKopfText (KopfTextTab[kopftextpos]);

		SetActiveWindow (lPosWindow);

		set_fkt (StopPosLeer, 5);

		EnableWindows (ParentWindow, TRUE);
		EnableWindows (BackWindow3, TRUE);
		EnableWindows (BackWindow2, TRUE);

		DestroyWindow (hWndLeer);
		DestroyWindow (lPosWindow);

		DestroyCaret ();
		SetActiveWindow (ParentWindow);
        ActivateColButton (BackWindow3, &MainButton2, 3, 0, 1);
 	    restore_fkt (4);
		restore_fkt (5);
		restore_fkt (6);
		restore_fkt (7);
		restore_fkt (8);
		restore_fkt (9);
		restore_fkt (10);
		restore_fkt (11);
}


void ReadKopftext (void)
/**
Kopftext lesen.
**/
{
		int zei;
		int i;


		for (i = 0; i < PosLeerE.fieldanz; i ++)
		{
		          memset (KopfTextFTab[i], ' ', 60);
				  KopfTextFTab[i][60] = 0;
	 		      clipped (KopfTextFTab[i]);
		}
		kopftextfpos = kopftextfscroll = 0;


		for (i = 0; i < LEERMAX; i ++)
		{
		          memset (KopfTextTab[i], ' ', 60);
				  KopfTextTab[i][60] = 0;
	 		      clipped (KopfTextTab[i]);
		}

		kopftextanz = zei = 0;

/*
		if (lsk.kopf_txt == 0l)
		{
		           ToKopfFtab ();
				   return;
		}
*/
		if (GebTara) return;

}


static void InitLeerPosArt (void)
/**
Tabelle mit Leergutartikel initialisieren.
**/
{
	    int i;

		for (i = 0; i < LEERMAX; i ++)
		{
			LeerPosArt[i].lart = (double) 0.0;
			LeerPosArt[i].lanz = 0;
		}
}

static void SetLeerPosArt (double art, int anz)
/**
Tabelle mit Leergutartikel initialisieren.
**/
{
	    int i;

		if (anz == 0) return;
		if (art == (double) 0.0) return;

		for (i = 0; LeerPosArt[i].lanz; i ++)
		{
			if (i == LEERMAX) return;
			if (LeerPosArt[i].lart == art)
			{
				break;
			}
		}

		LeerPosArt[i].lart  = art;
		LeerPosArt[i].lanz += anz;
}

static double GetLeerArt (char *LeerBez)
/**
Artikel-Nummer zu Leergutbezeichnung holen.
**/
{
	    int i;

		for (i = 0; i < ptanz; i ++)
		{
			if (strcmp (meeinhLPK[i], LeerBez) == 0)
			{
				return leer_art[i];
			}
		}
		return (double) 0.0;
}

static BOOL GetNextEinh (char *LeerZeile, int *anz, char *LeerBez, BOOL mode)
/**
Nexte Leergutbezeichnung und Leergutanzahl aus Zeile holen.
**/
{
	     static char *pos = NULL;
		 static char *LZ = NULL;
		 char anzstr [10];
		 int i;

		 if (LZ != LeerZeile)
		 {
			 pos = LeerZeile;
		 }
		 if (mode == FALSE)
		 {
			 pos = LeerZeile;
		 }

		 for (; *pos; pos += 1)
		 {
			 if (*pos == '+');
			 else if (*pos == '=');
			 else if (*pos > ' ')
			 {
				 break;
			 }
		 }

         if (*pos == 0)
		 {
			  *anz = 0;
              LeerBez[0] = 0;
			  return FALSE;
		 }

		 i = 0;
		 for (; *pos; pos += 1)
		 {
			 if (*pos < '0' || *pos > '9')
			 {
				 break;
			 }
			 anzstr[i] = *pos;
			 i ++;
		 }

         if (*pos == 0)
		 {
			  *anz = 0;
              LeerBez[0] = 0;
			  return FALSE;
		 }

		 LZ = LeerZeile;
         anzstr[i] = 0;
		 *anz = atoi (anzstr);
		 for (; *pos; pos += 1)
		 {
			 if (*pos == '+');
			 else if (*pos == '=');
			 else if (*pos > ' ')
			 {
				 break;
			 }
		 }

         if (*pos == 0)
		 {
              LeerBez[0] = 0;
			  return FALSE;
		 }


		 i = 0;
		 for (; *pos; pos += 1)
		 {
			 if (*pos <= ' ')
			 {
				 break;
			 }
             LeerBez[i] = *pos;
			 i ++;
		 }
		 LeerBez[i] = 0;
		 return TRUE;
}

static BOOL IsLeerGutZeile (char *Zeile)
/**
Test, ob die Zeile eine Leergutzeile ist.
**/
{
	     int i, j;
		 int anz;

	     for (i = 0; i < ptanz; i ++)
		 {
			 if (strstr (Zeile, meeinhLPK[i]) == NULL)
			 {
				 break;
			 }
		 }
		 if (i == ptanz) return FALSE;

		 anz = wsplit (Zeile, " ");
		 if (anz == 0) return FALSE;

		 for (i = 0; i < anz; i ++)
		 {
			 if (strcmp (wort[i], "+") == 0) continue;
			 if (strcmp (wort[i], "=") == 0)continue;
			 if (numeric (wort[i])) continue;
  	         for (j = 0; j < ptanz; j ++)
			 {
			        if (strcmp (wort[i], meeinhLPK[j]) == 0)
					{
				              break;
					}
			 }
			 if (j == ptanz) return FALSE;
		 }
		 return TRUE;
}

static BOOL IsLeerGut (double a)
/**
Artikeltyp pruefen.
**/
{
	     int dsqlstatus;

		 if (a <= (double) 0.0) return FALSE;
		 dsqlstatus = lese_a_bas (a);
		 if (dsqlstatus == 100) return FALSE;
         if (_a_bas.a_typ != 11) return FALSE;
		 return TRUE;
}


void WriteLeerZeileArt (char *LeerZeile)
/**
Eine Zeile fuer Leegutartikel bearbeiten.
**/
{
	     double art;
		 int anz;
		 char Bez[20];

		 if (IsLeerGutZeile(LeerZeile) == FALSE) return;

		 if (GetNextEinh (LeerZeile, &anz, Bez, 0) == FALSE) return;

		 art = GetLeerArt (Bez);
		 if (IsLeerGut (art))
		 {
			 SetLeerPosArt (art, anz);
		 }

		 while (GetNextEinh (LeerZeile, &anz, Bez, 1))
		 {
           		 art = GetLeerArt (Bez);
		         if (IsLeerGut (art))
				 {
				         SetLeerPosArt (art, anz);
				 }
		 }
}

static BOOL IsinLeerPos (double art)
/**
Test, ob Leergutartikel aud Positionen im Kopftext erfasst wurden.
**/
{
        int i;

		for (i = 0; LeerPosArt[i].lanz; i ++)
		{
			if (i == LEERMAX) return FALSE;
			if (LeerPosArt[i].lart == art)
			{
				return TRUE;
			}
		}
		return FALSE;
}


double GetGebGew (char *LeerZeile)
/**
Eine Zeile fuer Leegutartikel bearbeiten.
**/
{
	     double art;
		 int anz;
		 char Bez[20];
		 double gew = 0.0;

		 if (IsLeerGutZeile(LeerZeile) == FALSE) return 0.0;

		 if (GetNextEinh (LeerZeile, &anz, Bez, 0) == FALSE) return 0.0;

		 art = GetLeerArt (Bez);
		 if (IsLeerGut (art))
		 {
		         gew += _a_bas.a_gew * anz;
		 }

		 while (GetNextEinh (LeerZeile, &anz, Bez, 1))
		 {
           		 art = GetLeerArt (Bez);
		         if (IsLeerGut (art))
				 {
				         gew += _a_bas.a_gew * anz;
				 }
		 }
		 return gew;
}


void GetGebGewicht (char *ftara)
/**
Gewicht fuer Gebinde holen.
**/
{
	     int i;
		 double gew = 0.0;

 		 if (kopftextanz == 0)
		 {
			kopftextanz ++;
		 }
		 InitLeerPosArt ();
		 for (i = 0; i < kopftextanz; i ++)
		 {
			 gew += GetGebGew (KopfTextTab[i]);
		 }
		 sprintf (ftara, "%7.3lf", gew);
}



/*****
void Kontrollbeleg (void)

//Liste drucken.

{
    char valfrom [20];
    char valto [20];
    strcpy (drformat,"umllgr");


        if (LeseFormat (drformat) == FALSE)
        {
                   return;
        }

        sprintf (valfrom, "%hd", lgr_umlgrk.mdn);
        sprintf (valto, "%hd", lgr_umlgrk.mdn);
        FillFormFeld ("lgr_umlgrk.mdn", valfrom, valto);

        sprintf (valfrom, "%ld", lgr_umlgrk.umlgr_nr);
        sprintf (valto, "%ld", lgr_umlgrk.umlgr_nr);
        FillFormFeld ("lgr_umlgrk.umlgr_nr", valfrom, valto);
        StartPrint ();
}
**************/

void Kontrollbeleg (void)
{
    char valfrom [20];
    char valto [20];

    LeseFormat ("umllgr");

    sprintf (valfrom, "%hd", lgr_umlgrk.mdn);
    sprintf (valto, "%hd", lgr_umlgrk.mdn);
    FillFormFeld ("lgr_umlgrk.mdn", valfrom, valto);

    sprintf (valfrom, "%ld", lgr_umlgrk.umlgr_nr);
    sprintf (valto, "%ld", lgr_umlgrk.umlgr_nr);
    FillFormFeld ("lgr_umlgrk.umlgr_nr", valfrom, valto);

    DruckeFile();

}



