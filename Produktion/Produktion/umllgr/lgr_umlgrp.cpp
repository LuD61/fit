#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "lgr_umlgrp.h"
#include "lgr_umlgrk.h"

struct LGR_UMLGRP lgr_umlgrp, lgr_umlgrp_null;
struct BSD_BUCH bsd_buch, bsd_buch_null;

double menge ;
char charge [14];

void LGR_UMLGRP_CLASS::InsBsdBuch (int faktor, double menge, char *charge)
{
    if ( menge != double(0))
    {
                bsd_buch.nr = lgr_umlgrp.umlgr_nr;
                strcpy(bsd_buch.blg_typ,"UM");
                bsd_buch.mdn = lgr_umlgrp.mdn;
                bsd_buch.fil = 0;
                bsd_buch.a = lgr_umlgrp.a;
                bsd_buch.dat = lgr_umlgrp.dat;
                strcpy(bsd_buch.zeit,lgr_umlgrp.zeit);
                strcpy(bsd_buch.pers,"umllgr");
                sprintf(bsd_buch.bsd_lgr_ort,"%ld",lgr_umlgrk.lgr_na);
				if (lgr_umlgrk.lgr_von == -1) strcpy(bsd_buch.blg_typ,"M");
                bsd_buch.me = menge * faktor;
                strcpy(bsd_buch.chargennr,charge);
                execute_curs (ins_buch_cursor);

				if (lgr_umlgrk.lgr_von > -1) 
				{
					bsd_buch.nr = lgr_umlgrp.umlgr_nr;
					strcpy(bsd_buch.blg_typ,"UM");
					bsd_buch.mdn = lgr_umlgrp.mdn;
					bsd_buch.fil = 0;
					bsd_buch.a = lgr_umlgrp.a;
					bsd_buch.dat = lgr_umlgrp.dat;
					strcpy(bsd_buch.zeit,lgr_umlgrp.zeit);
					strcpy(bsd_buch.pers,"umllgr");
					sprintf(bsd_buch.bsd_lgr_ort,"%ld",lgr_umlgrk.lgr_von);
					bsd_buch.me = menge * faktor * -1;
					strcpy(bsd_buch.chargennr,charge);
					execute_curs (ins_buch_cursor);
				}
    }



}
void LGR_UMLGRP_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &lgr_umlgrp.mdn, 1, 0);
            ins_quest ((char *) &lgr_umlgrp.umlgr_nr, 2, 0);
            ins_quest ((char *) &lgr_umlgrp.int_pos, 2, 0);
    out_quest ((char *) &lgr_umlgrp.delstatus,1,0);
    out_quest ((char *) &lgr_umlgrp.a,3,0);
    out_quest ((char *) &lgr_umlgrp.mdn,1,0);
    out_quest ((char *) &lgr_umlgrp.me,3,0);
    out_quest ((char *) &lgr_umlgrp.a_na,3,0);
    out_quest ((char *) &lgr_umlgrp.me_na,3,0);
    out_quest ((char *) &lgr_umlgrp.pr_ek_von,3,0);
    out_quest ((char *) &lgr_umlgrp.pr_ek_na,3,0);
    out_quest ((char *) &lgr_umlgrp.pr_vk_von,3,0);
    out_quest ((char *) &lgr_umlgrp.pr_vk_na,3,0);
    out_quest ((char *) &lgr_umlgrp.txt_nr,2,0);
    out_quest ((char *) &lgr_umlgrp.umlgr_nr,2,0);
    out_quest ((char *) lgr_umlgrp.zeit,0,11);
    out_quest ((char *) &lgr_umlgrp.int_pos,2,0);
    out_quest ((char *) lgr_umlgrp.chargennr,0,14);
    out_quest ((char *) &lgr_umlgrp.dat,2,0);
            cursor = prepare_sql ("select "
"lgr_umlgrp.delstatus,  lgr_umlgrp.a,  lgr_umlgrp.mdn,  lgr_umlgrp.me,  "
"lgr_umlgrp.a_na,  lgr_umlgrp.me_na,  lgr_umlgrp.pr_ek_von,  "
"lgr_umlgrp.pr_ek_na,  lgr_umlgrp.pr_vk_von,  lgr_umlgrp.pr_vk_na,  "
"lgr_umlgrp.txt_nr,  lgr_umlgrp.umlgr_nr,  lgr_umlgrp.zeit,  "
"lgr_umlgrp.int_pos,  lgr_umlgrp.chargennr,  lgr_umlgrp.dat from lgr_umlgrp "

#line 64 "lgr_umlgrp.rpp"
                                  "where mdn = ? "
                                  "and   umlgr_nr = ? "
                                  "and   int_pos = ? ");

    ins_quest ((char *) &lgr_umlgrp.delstatus,1,0);
    ins_quest ((char *) &lgr_umlgrp.a,3,0);
    ins_quest ((char *) &lgr_umlgrp.mdn,1,0);
    ins_quest ((char *) &lgr_umlgrp.me,3,0);
    ins_quest ((char *) &lgr_umlgrp.a_na,3,0);
    ins_quest ((char *) &lgr_umlgrp.me_na,3,0);
    ins_quest ((char *) &lgr_umlgrp.pr_ek_von,3,0);
    ins_quest ((char *) &lgr_umlgrp.pr_ek_na,3,0);
    ins_quest ((char *) &lgr_umlgrp.pr_vk_von,3,0);
    ins_quest ((char *) &lgr_umlgrp.pr_vk_na,3,0);
    ins_quest ((char *) &lgr_umlgrp.txt_nr,2,0);
    ins_quest ((char *) &lgr_umlgrp.umlgr_nr,2,0);
    ins_quest ((char *) lgr_umlgrp.zeit,0,11);
    ins_quest ((char *) &lgr_umlgrp.int_pos,2,0);
    ins_quest ((char *) lgr_umlgrp.chargennr,0,14);
    ins_quest ((char *) &lgr_umlgrp.dat,2,0);
            sqltext = "update lgr_umlgrp set "
"lgr_umlgrp.delstatus = ?,  lgr_umlgrp.a = ?,  lgr_umlgrp.mdn = ?,  "
"lgr_umlgrp.me = ?,  lgr_umlgrp.a_na = ?,  lgr_umlgrp.me_na = ?,  "
"lgr_umlgrp.pr_ek_von = ?,  lgr_umlgrp.pr_ek_na = ?,  "
"lgr_umlgrp.pr_vk_von = ?,  lgr_umlgrp.pr_vk_na = ?,  "
"lgr_umlgrp.txt_nr = ?,  lgr_umlgrp.umlgr_nr = ?,  "
"lgr_umlgrp.zeit = ?,  lgr_umlgrp.int_pos = ?,  "
"lgr_umlgrp.chargennr = ?,  lgr_umlgrp.dat = ? "

#line 69 "lgr_umlgrp.rpp"
                                  "where mdn = ? "
                                  "and   umlgr_nr = ? "
                                  "and   int_pos = ? ";

            ins_quest ((char *) &lgr_umlgrp.mdn, 1, 0);
            ins_quest ((char *) &lgr_umlgrp.umlgr_nr, 2, 0);
            ins_quest ((char *) &lgr_umlgrp.int_pos, 2, 0);
            upd_cursor = prepare_sql (sqltext);

            out_quest ((char *) &menge, 3, 0);
            out_quest ((char *) &charge, 0, 13);
            ins_quest ((char *) &lgr_umlgrp.mdn, 1, 0);
            ins_quest ((char *) &lgr_umlgrp.umlgr_nr, 2, 0);
            ins_quest ((char *) &lgr_umlgrp.int_pos, 2, 0);
            test_upd_cursor = prepare_sql ("select me,chargennr from lgr_umlgrp "
                                  "where mdn = ? "
                                  "and   umlgr_nr = ? "
                                  "and   int_pos = ? ");
            ins_quest ((char *) &lgr_umlgrp.mdn, 1, 0);
            ins_quest ((char *) &lgr_umlgrp.umlgr_nr, 2, 0);
            ins_quest ((char *) &lgr_umlgrp.int_pos, 2, 0);
            del_cursor = prepare_sql ("delete from lgr_umlgrp "
                                  "where mdn = ? "
                                  "and   umlgr_nr = ? "
                                  "and   int_pos = ? ");
    ins_quest ((char *) &lgr_umlgrp.delstatus,1,0);
    ins_quest ((char *) &lgr_umlgrp.a,3,0);
    ins_quest ((char *) &lgr_umlgrp.mdn,1,0);
    ins_quest ((char *) &lgr_umlgrp.me,3,0);
    ins_quest ((char *) &lgr_umlgrp.a_na,3,0);
    ins_quest ((char *) &lgr_umlgrp.me_na,3,0);
    ins_quest ((char *) &lgr_umlgrp.pr_ek_von,3,0);
    ins_quest ((char *) &lgr_umlgrp.pr_ek_na,3,0);
    ins_quest ((char *) &lgr_umlgrp.pr_vk_von,3,0);
    ins_quest ((char *) &lgr_umlgrp.pr_vk_na,3,0);
    ins_quest ((char *) &lgr_umlgrp.txt_nr,2,0);
    ins_quest ((char *) &lgr_umlgrp.umlgr_nr,2,0);
    ins_quest ((char *) lgr_umlgrp.zeit,0,11);
    ins_quest ((char *) &lgr_umlgrp.int_pos,2,0);
    ins_quest ((char *) lgr_umlgrp.chargennr,0,14);
    ins_quest ((char *) &lgr_umlgrp.dat,2,0);
            ins_cursor = prepare_sql ("insert into lgr_umlgrp ("
"delstatus,  a,  mdn,  me,  a_na,  me_na,  pr_ek_von,  pr_ek_na,  pr_vk_von,  pr_vk_na,  "
"txt_nr,  umlgr_nr,  zeit,  int_pos,  chargennr,  dat) "

#line 95 "lgr_umlgrp.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?)");

#line 97 "lgr_umlgrp.rpp"
    ins_quest ((char *) &bsd_buch.nr,2,0);
    ins_quest ((char *) bsd_buch.blg_typ,0,3);
    ins_quest ((char *) &bsd_buch.mdn,1,0);
    ins_quest ((char *) &bsd_buch.fil,1,0);
    ins_quest ((char *) &bsd_buch.kun_fil,1,0);
    ins_quest ((char *) &bsd_buch.a,3,0);
    ins_quest ((char *) &bsd_buch.dat,2,0);
    ins_quest ((char *) bsd_buch.zeit,0,9);
    ins_quest ((char *) bsd_buch.pers,0,13);
    ins_quest ((char *) bsd_buch.bsd_lgr_ort,0,13);
    ins_quest ((char *) &bsd_buch.qua_status,1,0);
    ins_quest ((char *) &bsd_buch.me,3,0);
    ins_quest ((char *) &bsd_buch.bsd_ek_vk,3,0);
    ins_quest ((char *) bsd_buch.chargennr,0,14);
    ins_quest ((char *) bsd_buch.ident_nr,0,14);
    ins_quest ((char *) bsd_buch.herk_nachw,0,14);
    ins_quest ((char *) bsd_buch.lief,0,17);
    ins_quest ((char *) &bsd_buch.auf,2,0);
    ins_quest ((char *) bsd_buch.verfall,0,2);
    ins_quest ((char *) &bsd_buch.verf_dat,2,0);
    ins_quest ((char *) &bsd_buch.delstatus,1,0);
    ins_quest ((char *) bsd_buch.err_txt,0,17);
    ins_quest ((char *) &bsd_buch.me2,3,0);
    ins_quest ((char *) &bsd_buch.me_einh2,1,0);
    ins_quest ((char *) &bsd_buch.lieferdat,2,0);
            ins_buch_cursor = prepare_sql ("insert into bsd_buch ("
"nr,  blg_typ,  mdn,  fil,  kun_fil,  a,  dat,  zeit,  pers,  bsd_lgr_ort,  qua_status,  me,  "
"bsd_ek_vk,  chargennr,  ident_nr,  herk_nachw,  lief,  auf,  verfall,  verf_dat,  "
"delstatus,  err_txt,  me2,  me_einh2,  lieferdat) "

#line 98 "lgr_umlgrp.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

#line 100 "lgr_umlgrp.rpp"
}
void LGR_UMLGRP_CLASS::prepare_o (char *order)
{

            ins_quest ((char *) &lgr_umlgrp.mdn, 1, 0);
            ins_quest ((char *) &lgr_umlgrp.umlgr_nr, 2, 0);
            if (order)
            {
    out_quest ((char *) &lgr_umlgrp.delstatus,1,0);
    out_quest ((char *) &lgr_umlgrp.a,3,0);
    out_quest ((char *) &lgr_umlgrp.mdn,1,0);
    out_quest ((char *) &lgr_umlgrp.me,3,0);
    out_quest ((char *) &lgr_umlgrp.a_na,3,0);
    out_quest ((char *) &lgr_umlgrp.me_na,3,0);
    out_quest ((char *) &lgr_umlgrp.pr_ek_von,3,0);
    out_quest ((char *) &lgr_umlgrp.pr_ek_na,3,0);
    out_quest ((char *) &lgr_umlgrp.pr_vk_von,3,0);
    out_quest ((char *) &lgr_umlgrp.pr_vk_na,3,0);
    out_quest ((char *) &lgr_umlgrp.txt_nr,2,0);
    out_quest ((char *) &lgr_umlgrp.umlgr_nr,2,0);
    out_quest ((char *) lgr_umlgrp.zeit,0,11);
    out_quest ((char *) &lgr_umlgrp.int_pos,2,0);
    out_quest ((char *) lgr_umlgrp.chargennr,0,14);
    out_quest ((char *) &lgr_umlgrp.dat,2,0);
                    cursor_o = prepare_sql ("select "
"lgr_umlgrp.delstatus,  lgr_umlgrp.a,  lgr_umlgrp.mdn,  lgr_umlgrp.me,  "
"lgr_umlgrp.a_na,  lgr_umlgrp.me_na,  lgr_umlgrp.pr_ek_von,  "
"lgr_umlgrp.pr_ek_na,  lgr_umlgrp.pr_vk_von,  lgr_umlgrp.pr_vk_na,  "
"lgr_umlgrp.txt_nr,  lgr_umlgrp.umlgr_nr,  lgr_umlgrp.zeit,  "
"lgr_umlgrp.int_pos,  lgr_umlgrp.chargennr,  lgr_umlgrp.dat from lgr_umlgrp "

#line 109 "lgr_umlgrp.rpp"
                                          "where mdn = ? "
                                          "and   umlgr_nr = ? %s", order);
            }
            else
            {
    out_quest ((char *) &lgr_umlgrp.delstatus,1,0);
    out_quest ((char *) &lgr_umlgrp.a,3,0);
    out_quest ((char *) &lgr_umlgrp.mdn,1,0);
    out_quest ((char *) &lgr_umlgrp.me,3,0);
    out_quest ((char *) &lgr_umlgrp.a_na,3,0);
    out_quest ((char *) &lgr_umlgrp.me_na,3,0);
    out_quest ((char *) &lgr_umlgrp.pr_ek_von,3,0);
    out_quest ((char *) &lgr_umlgrp.pr_ek_na,3,0);
    out_quest ((char *) &lgr_umlgrp.pr_vk_von,3,0);
    out_quest ((char *) &lgr_umlgrp.pr_vk_na,3,0);
    out_quest ((char *) &lgr_umlgrp.txt_nr,2,0);
    out_quest ((char *) &lgr_umlgrp.umlgr_nr,2,0);
    out_quest ((char *) lgr_umlgrp.zeit,0,11);
    out_quest ((char *) &lgr_umlgrp.int_pos,2,0);
    out_quest ((char *) lgr_umlgrp.chargennr,0,14);
    out_quest ((char *) &lgr_umlgrp.dat,2,0);
                    cursor_o = prepare_sql ("select "
"lgr_umlgrp.delstatus,  lgr_umlgrp.a,  lgr_umlgrp.mdn,  lgr_umlgrp.me,  "
"lgr_umlgrp.a_na,  lgr_umlgrp.me_na,  lgr_umlgrp.pr_ek_von,  "
"lgr_umlgrp.pr_ek_na,  lgr_umlgrp.pr_vk_von,  lgr_umlgrp.pr_vk_na,  "
"lgr_umlgrp.txt_nr,  lgr_umlgrp.umlgr_nr,  lgr_umlgrp.zeit,  "
"lgr_umlgrp.int_pos,  lgr_umlgrp.chargennr,  lgr_umlgrp.dat from lgr_umlgrp "

#line 115 "lgr_umlgrp.rpp"
                                          "where mdn = ? "
                                          "and   umlgr_nr = ? ");
           }
}


int LGR_UMLGRP_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int LGR_UMLGRP_CLASS::dbreadfirst_o (char *order)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         if (cursor_o == -1)
         {
                this->prepare_o (order);
         }
         open_sql (cursor_o);
         fetch_sql (cursor_o);
         return (sqlstatus);
}

int LGR_UMLGRP_CLASS::dbread_o ()
/**
Ersten Satz aus Tabelle lesen.
**/
{

         fetch_sql (cursor_o);
         return (sqlstatus);
}

int LGR_UMLGRP_CLASS::dbclose_o ()
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1) return 0;
         close_sql (cursor_o);
         cursor_o = -1;
         return (0);
}

int LGR_UMLGRP_CLASS::dbupdate ()
/**
Tabelle lgr_umlgrp Updaten.
**/
{
    char zeit  [12];
    if ( test_upd_cursor == -1)
    {
        this->prepare ();
    }
         open_sql (test_upd_cursor);
         fetch_sql (test_upd_cursor);
         if (sqlstatus == 100)
         {
				   systime (zeit);
		           memcpy (lgr_umlgrp.zeit, zeit, 5);

                   InsBsdBuch(1,lgr_umlgrp.me, lgr_umlgrp.chargennr);
                   execute_curs (ins_cursor);
         }
         else if (sqlstatus == 0)
         {
            if ( strcmp (charge,lgr_umlgrp.chargennr) != 0 ||
                    double(menge) != double(lgr_umlgrp.me) )
            {

                InsBsdBuch(-1,menge, charge);
                InsBsdBuch(1,lgr_umlgrp.me, lgr_umlgrp.chargennr);
                execute_curs (upd_cursor);
            }
         }

         return 0;
}

