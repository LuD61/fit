#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "lgr_umlgrk.h"

struct LGR_UMLGRK lgr_umlgrk, lgr_umlgrk_null;

void LGR_UMLGRK_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &lgr_umlgrk.mdn, 1, 0);
            ins_quest ((char *) &lgr_umlgrk.dat, 2, 0);
            ins_quest ((char *) &lgr_umlgrk.lgr_na, 1, 0);
            ins_quest ((char *) &lgr_umlgrk.lgr_von, 1, 0);
    out_quest ((char *) &lgr_umlgrk.delstatus,1,0);
    out_quest ((char *) &lgr_umlgrk.dat,2,0);
    out_quest ((char *) &lgr_umlgrk.lgr_na,1,0);
    out_quest ((char *) &lgr_umlgrk.lgr_von,1,0);
    out_quest ((char *) &lgr_umlgrk.mdn,1,0);
    out_quest ((char *) lgr_umlgrk.txt,0,61);
    out_quest ((char *) &lgr_umlgrk.umlgr_nr,2,0);
    out_quest ((char *) &lgr_umlgrk.stat,1,0);
    out_quest ((char *) lgr_umlgrk.zeit,0,7);
    out_quest ((char *) lgr_umlgrk.ueb_kz,0,2);
    out_quest ((char *) &lgr_umlgrk.diff_a,3,0);
            cursor = prepare_sql ("select "
"lgr_umlgrk.delstatus,  lgr_umlgrk.dat,  lgr_umlgrk.lgr_na,  "
"lgr_umlgrk.lgr_von,  lgr_umlgrk.mdn,  lgr_umlgrk.txt,  "
"lgr_umlgrk.umlgr_nr,  lgr_umlgrk.stat,  lgr_umlgrk.zeit,  "
"lgr_umlgrk.ueb_kz,  lgr_umlgrk.diff_a from lgr_umlgrk "

#line 26 "lgr_umlgrk.rpp"
                                  "where mdn = ? "
                                  "and   dat = ? "
                                  "and   lgr_na = ? "
                                  "and   lgr_von = ? ");

    ins_quest ((char *) &lgr_umlgrk.delstatus,1,0);
    ins_quest ((char *) &lgr_umlgrk.dat,2,0);
    ins_quest ((char *) &lgr_umlgrk.lgr_na,1,0);
    ins_quest ((char *) &lgr_umlgrk.lgr_von,1,0);
    ins_quest ((char *) &lgr_umlgrk.mdn,1,0);
    ins_quest ((char *) lgr_umlgrk.txt,0,61);
    ins_quest ((char *) &lgr_umlgrk.umlgr_nr,2,0);
    ins_quest ((char *) &lgr_umlgrk.stat,1,0);
    ins_quest ((char *) lgr_umlgrk.zeit,0,7);
    ins_quest ((char *) lgr_umlgrk.ueb_kz,0,2);
    ins_quest ((char *) &lgr_umlgrk.diff_a,3,0);
            sqltext = "update lgr_umlgrk set "
"lgr_umlgrk.delstatus = ?,  lgr_umlgrk.dat = ?,  "
"lgr_umlgrk.lgr_na = ?,  lgr_umlgrk.lgr_von = ?,  "
"lgr_umlgrk.mdn = ?,  lgr_umlgrk.txt = ?,  lgr_umlgrk.umlgr_nr = ?,  "
"lgr_umlgrk.stat = ?,  lgr_umlgrk.zeit = ?,  lgr_umlgrk.ueb_kz = ?,  "
"lgr_umlgrk.diff_a = ? "

#line 32 "lgr_umlgrk.rpp"
                                  "where mdn = ? "
                                  "and   dat = ? "
                                  "and   lgr_na = ? "
                                  "and   lgr_von = ? ";

            ins_quest ((char *) &lgr_umlgrk.mdn, 1, 0);
            ins_quest ((char *) &lgr_umlgrk.dat, 2, 0);
            ins_quest ((char *) &lgr_umlgrk.lgr_na, 1, 0);
            ins_quest ((char *) &lgr_umlgrk.lgr_von, 1, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &lgr_umlgrk.mdn, 1, 0);
            ins_quest ((char *) &lgr_umlgrk.dat, 2, 0);
            ins_quest ((char *) &lgr_umlgrk.lgr_na, 1, 0);
            ins_quest ((char *) &lgr_umlgrk.lgr_von, 1, 0);
            test_upd_cursor = prepare_sql ("select umlgr_nr from lgr_umlgrk "
                                  "where mdn = ? "
                                  "and   dat = ? "
                                  "and   lgr_na = ? "
                                  "and   lgr_von = ? ");
            ins_quest ((char *) &lgr_umlgrk.mdn, 1, 0);
            ins_quest ((char *) &lgr_umlgrk.dat, 2, 0);
            ins_quest ((char *) &lgr_umlgrk.lgr_na, 1, 0);
            ins_quest ((char *) &lgr_umlgrk.lgr_von, 1, 0);
            del_cursor = prepare_sql ("delete from lgr_umlgrk "
                                  "where mdn = ? "
                                  "and   dat = ? "
                                  "and   lgr_na = ? "
                                  "and   lgr_von = ? ");
    ins_quest ((char *) &lgr_umlgrk.delstatus,1,0);
    ins_quest ((char *) &lgr_umlgrk.dat,2,0);
    ins_quest ((char *) &lgr_umlgrk.lgr_na,1,0);
    ins_quest ((char *) &lgr_umlgrk.lgr_von,1,0);
    ins_quest ((char *) &lgr_umlgrk.mdn,1,0);
    ins_quest ((char *) lgr_umlgrk.txt,0,61);
    ins_quest ((char *) &lgr_umlgrk.umlgr_nr,2,0);
    ins_quest ((char *) &lgr_umlgrk.stat,1,0);
    ins_quest ((char *) lgr_umlgrk.zeit,0,7);
    ins_quest ((char *) lgr_umlgrk.ueb_kz,0,2);
    ins_quest ((char *) &lgr_umlgrk.diff_a,3,0);
            ins_cursor = prepare_sql ("insert into lgr_umlgrk ("
"delstatus,  dat,  lgr_na,  lgr_von,  mdn,  txt,  umlgr_nr,  stat,  zeit,  ueb_kz,  diff_a) "

#line 62 "lgr_umlgrk.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?)");

#line 64 "lgr_umlgrk.rpp"
}
int LGR_UMLGRK_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

