#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "a_kun.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "a_tara.h"

struct A_TARA a_tara, a_tara_null;

void A_TARA_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &a_tara.a, 3, 0);
            ins_quest ((char *) &a_tara.a_vpk, 3, 0);
    out_quest ((char *) &a_tara.a,3,0);
    out_quest ((char *) &a_tara.a_vpk,3,0);
    out_quest ((char *) a_tara.a_krz,0,17);
    out_quest ((char *) &a_tara.a_gew,3,0);
            cursor = prepare_sql ("select a_tara.a,  "
"a_tara.a_vpk,  a_tara.a_krz,  a_tara.a_gew from a_tara "

#line 27 "a_tara.rpp"
                                  "where a = ? and a_vpk = ? ");

            ins_quest ((char *) &a_tara.a, 3, 0);
    out_quest ((char *) &a_tara.a,3,0);
    out_quest ((char *) &a_tara.a_vpk,3,0);
    out_quest ((char *) a_tara.a_krz,0,17);
    out_quest ((char *) &a_tara.a_gew,3,0);
            cursor_all = prepare_sql ("select a_tara.a,  "
"a_tara.a_vpk,  a_tara.a_krz,  a_tara.a_gew from a_tara where a = ? order by a_krz ");

#line 31 "a_tara.rpp"
            
    ins_quest ((char *) &a_tara.a,3,0);
    ins_quest ((char *) &a_tara.a_vpk,3,0);
    ins_quest ((char *) a_tara.a_krz,0,17);
    ins_quest ((char *) &a_tara.a_gew,3,0);
            sqltext = "update a_tara set a_tara.a = ?,  "
"a_tara.a_vpk = ?,  a_tara.a_krz = ?,  a_tara.a_gew = ? "

#line 33 "a_tara.rpp"
                                  "where a = ? and a_vpk = ? ";


            ins_quest ((char *) &a_tara.a, 3, 0);
            ins_quest ((char *) &a_tara.a_vpk, 3, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &a_tara.a, 3, 0);
            ins_quest ((char *) &a_tara.a_vpk, 3, 0);
            test_upd_cursor = prepare_sql ("select a from a_tara "
                                  "where a = ? and a_vpk = ? ");
            ins_quest ((char *) &a_tara.a, 3, 0);
            ins_quest ((char *) &a_tara.a_vpk, 3, 0);
            del_cursor = prepare_sql ("delete from a_tara "
                                  "where a = ? and a_vpk = ? ");
    ins_quest ((char *) &a_tara.a,3,0);
    ins_quest ((char *) &a_tara.a_vpk,3,0);
    ins_quest ((char *) a_tara.a_krz,0,17);
    ins_quest ((char *) &a_tara.a_gew,3,0);
            ins_cursor = prepare_sql ("insert into a_tara (a,  "
"a_vpk,  a_krz,  a_gew) "

#line 49 "a_tara.rpp"
                                      "values "
                                      "(?,?,?,?)"); 

#line 51 "a_tara.rpp"
}
int A_TARA_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int A_TARA_CLASS::readfirstall (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         open_sql (cursor_all);
         fetch_sql (cursor_all);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}
int A_TARA_CLASS::readnextall (void)
/**
**/
{
         fetch_sql (cursor_all);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}
