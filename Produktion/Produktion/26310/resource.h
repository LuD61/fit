//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by 26310.rc
//
#define IDR_SCANOK                      101
#define IDR_COMPLETE                    102
#define IDR_ERROR                       103
#define IDR_OK                          104
#define IDD_DIALOG1                     105
#define IDI_ICON1                       106

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        107
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
