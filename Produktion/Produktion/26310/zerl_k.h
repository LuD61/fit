#ifndef _ZERL_K_DEF
#define _ZERL_K_DEF

#include "dbclass.h"

struct ZERL_K {
   short     mdn;
   double    partie;
   char      partie_bz[49];
   char      lief[17];
   char      zer_part[9];
   long      zer_dat;
   short     delstatus;
   short     schnitt;
   short     partie_status;
   double    a;
   short     fanggebiet;
   double    a_prod;
};
extern struct ZERL_K zerl_k, zerl_k_null;

#line 7 "zerl_k.rh"

class ZERL_K_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               ZERL_K_CLASS () : DB_CLASS ()
               {
               }
               int dbreadfirst (void);
};
#endif
