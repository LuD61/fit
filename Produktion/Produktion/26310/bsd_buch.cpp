#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "bsd_buch.h"
#include "zerl_k.h"
#include "zerl_p.h"
#include "mo_menu.h"

struct BSD_BUCH bsd_buch, bsd_buch_null;

double menge ;
char charge [14];

void BSD_BUCH_CLASS::BucheBsd (int faktor, double menge, char *charge, int EinAusgang)
{

    if ( menge != double(0))
    {
         if (ins_buch_cursor == -1)
         {
                this->prepare ();
         }
		        charge[13] = 0;

                bsd_buch.nr = zerl_k.zer_dat;
				if (EinAusgang == 1)
				{
					strcpy(bsd_buch.blg_typ,"ZE");
				}
				else if (EinAusgang == 2)
				{
					strcpy(bsd_buch.blg_typ,"ZA");
				}
				else if (EinAusgang == 4) //Gesamt Zerlegung
				{
	                bsd_buch.mdn = zerl_k.mdn;
		            bsd_buch.fil = 0;
			        bsd_buch.a = zerl_p.a;
					bsd_buch.dat = zerl_p.bearb;
					strcpy(bsd_buch.zeit,zerl_p.zeit);
					strcpy(bsd_buch.pers,"26310");

					if (zerl_p.kng == 1 || zerl_p.kng == 10)  //Zerlegeeingang
					{
						strcpy(bsd_buch.blg_typ,"ZE");
			            sprintf(bsd_buch.bsd_lgr_ort,"%ld",zerl_p.lgr_ab);
					    bsd_buch.me = menge * faktor * -1;
					}
					if (zerl_p.kng == 2 || zerl_p.kng == 20) //Zerlegeausgang
					{
						strcpy(bsd_buch.blg_typ,"ZA");
						sprintf(bsd_buch.bsd_lgr_ort,"%ld",zerl_p.lgr_zu);
						bsd_buch.me = menge * faktor;
					}
					strcpy(bsd_buch.chargennr,charge);
					execute_curs (ins_buch_cursor);
					return ; 
				}
				else if (EinAusgang == 6) //PRODUKTIONSSTART   //FISCH-5
				{
//Hier: BuchBsd : zerl_p.status : 0 : Abbuchung WE-Chargen, 1: nach Abschluss . buchen ders Produktionssartikels   
					if (zerl_p.status == 1) 
					{
						strcpy(bsd_buch.blg_typ,"UM");
						bsd_buch.mdn = zerl_k.mdn;
						bsd_buch.fil = 0;
						bsd_buch.a = zerl_p.a;
						bsd_buch.dat = zerl_p.bearb;
						strcpy(bsd_buch.zeit,zerl_p.zeit);
						strcpy(bsd_buch.pers,sys_ben.pers);
						sprintf(bsd_buch.bsd_lgr_ort,"%ld",zerl_p.lgr_zu);
						bsd_buch.me = menge * faktor;
						sprintf(bsd_buch.chargennr,"%.0lf",zerl_p.partie);
						sprintf (bsd_buch.herk_nachw, "%hd",zerl_p.fortschritt);
						execute_curs (ins_buch_cursor);
					}
					else
					{
						bsd_buch.nr = zerl_k.zer_dat;
						strcpy(bsd_buch.blg_typ,"UM");
						bsd_buch.mdn = zerl_k.mdn;
						bsd_buch.fil = 0;
						bsd_buch.a = zerl_p.a;
						bsd_buch.dat = zerl_p.bearb;
						strcpy(bsd_buch.zeit,zerl_p.zeit);
						strcpy(bsd_buch.pers,sys_ben.pers);
						sprintf(bsd_buch.bsd_lgr_ort,"%ld",zerl_p.lgr_ab);
						bsd_buch.me = menge * faktor * -1;
						strcpy(bsd_buch.chargennr,charge);
						sprintf (bsd_buch.herk_nachw, "%hd",zerl_p.fortschritt);
						execute_curs (ins_buch_cursor);
					}
					return ; 
				}
				else if (EinAusgang == 7) //PRODUKTIONSFORTSCHRITT   //FISCH-5
				{
						strcpy(bsd_buch.blg_typ,"UM");
						bsd_buch.mdn = zerl_k.mdn;
						bsd_buch.fil = 0;
						bsd_buch.a = zerl_p.a;
						bsd_buch.dat = zerl_p.bearb;
						strcpy(bsd_buch.zeit,zerl_p.zeit);
						strcpy(bsd_buch.pers,sys_ben.pers);
						sprintf(bsd_buch.bsd_lgr_ort,"%ld",zerl_p.lgr_zu);
						bsd_buch.me = menge * faktor;
						sprintf(bsd_buch.chargennr,"%.0lf",zerl_p.partie);
						sprintf (bsd_buch.herk_nachw, "%hd",zerl_p.fortschritt);
						execute_curs (ins_buch_cursor); 
					return;
				}
				else 
				{
					strcpy(bsd_buch.blg_typ,"UM");
				}
                bsd_buch.mdn = zerl_k.mdn;
                bsd_buch.fil = 0;
                bsd_buch.a = zerl_p.a;
                bsd_buch.dat = zerl_p.bearb;
                strcpy(bsd_buch.zeit,zerl_p.zeit);
				strcpy(bsd_buch.pers,sys_ben.pers);
                sprintf(bsd_buch.bsd_lgr_ort,"%ld",zerl_p.lgr_zu);
                bsd_buch.me = menge * faktor;
                strcpy(bsd_buch.chargennr,charge);
				sprintf (bsd_buch.herk_nachw, "%hd",zerl_p.fortschritt);
                execute_curs (ins_buch_cursor);

                bsd_buch.nr = zerl_k.zer_dat;
                strcpy(bsd_buch.blg_typ,"UM");
                bsd_buch.mdn = zerl_k.mdn;
                bsd_buch.fil = 0;
                bsd_buch.a = zerl_p.a;
                bsd_buch.dat = zerl_p.bearb;
                strcpy(bsd_buch.zeit,zerl_p.zeit);
				strcpy(bsd_buch.pers,sys_ben.pers);
                sprintf(bsd_buch.bsd_lgr_ort,"%ld",zerl_p.lgr_ab);
                bsd_buch.me = menge * faktor * -1;
                strcpy(bsd_buch.chargennr,charge);
				sprintf (bsd_buch.herk_nachw, "%hd",zerl_p.fortschritt);
                execute_curs (ins_buch_cursor);
    }



}
void BSD_BUCH_CLASS::prepare (void)
{
    ins_quest ((char *) &bsd_buch.nr,2,0);
    ins_quest ((char *) bsd_buch.blg_typ,0,3);
    ins_quest ((char *) &bsd_buch.mdn,1,0);
    ins_quest ((char *) &bsd_buch.fil,1,0);
    ins_quest ((char *) &bsd_buch.kun_fil,1,0);
    ins_quest ((char *) &bsd_buch.a,3,0);
    ins_quest ((char *) &bsd_buch.dat,2,0);
    ins_quest ((char *) bsd_buch.zeit,0,9);
    ins_quest ((char *) bsd_buch.pers,0,13);
    ins_quest ((char *) bsd_buch.bsd_lgr_ort,0,13);
    ins_quest ((char *) &bsd_buch.qua_status,1,0);
    ins_quest ((char *) &bsd_buch.me,3,0);
    ins_quest ((char *) &bsd_buch.bsd_ek_vk,3,0);
    ins_quest ((char *) bsd_buch.chargennr,0,31);
    ins_quest ((char *) bsd_buch.ident_nr,0,21);
    ins_quest ((char *) bsd_buch.herk_nachw,0,14);
    ins_quest ((char *) bsd_buch.lief,0,17);
    ins_quest ((char *) &bsd_buch.auf,2,0);
    ins_quest ((char *) bsd_buch.verfall,0,2);
    ins_quest ((char *) &bsd_buch.verf_dat,2,0);
    ins_quest ((char *) &bsd_buch.delstatus,1,0);
    ins_quest ((char *) bsd_buch.err_txt,0,17);
    ins_quest ((char *) &bsd_buch.me2,3,0);
    ins_quest ((char *) &bsd_buch.me_einh2,1,0);
    ins_quest ((char *) &bsd_buch.lieferdat,2,0);
    ins_quest ((char *) &bsd_buch.a_grund,3,0);
    ins_quest ((char *) &bsd_buch.me_einh,1,0);
            ins_buch_cursor = prepare_sql ("insert into bsd_buch ("
"nr,  blg_typ,  mdn,  fil,  kun_fil,  a,  dat,  zeit,  pers,  bsd_lgr_ort,  qua_status,  me,  "
"bsd_ek_vk,  chargennr,  ident_nr,  herk_nachw,  lief,  auf,  verfall,  verf_dat,  "
"delstatus,  err_txt,  me2,  me_einh2,  lieferdat,  a_grund,  me_einh) "

#line 64 "bsd_buch.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

#line 66 "bsd_buch.rpp"
}


