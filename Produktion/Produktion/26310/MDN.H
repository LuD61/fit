#ifndef _MDN_OK
#define _MDN_OK

struct MDN {
   char      abr_period[2];
   long      adr;
   long      adr_lief;
   long      dat_ero;
   short     daten_mnp;
   short     delstatus;
   char      fil_bel_o_sa[2];
   double    fl_lad;
   double    fl_nto;
   double    fl_vk_ges;
   long      iakv;
   char      inv_rht[2];
   long      kun;
   char      lief[17];
   char      lief_rht[2];
   long      lief_s;
   short     mdn;
   char      mdn_kla[2];
   short     mdn_gr;
   char      pers[13];
   short     pers_anz;
   char      pr_bel_entl[2];
   long      pr_lst;
   double    reg_bed_theke_lng;
   double    reg_kt_lng;
   double    reg_kue_lng;
   double    reg_lng;
   double    reg_tks_lng;
   double    reg_tkt_lng;
   char      smt_kz[2];
   short     sonst_einh;
   char      sp_kz[2];
   short     sprache;
   char      sw_kz[2];
   long      tou;
   char      umlgr[2];
   char      verk_st_kz[2];
   short     vrs_typ;
   char      inv_akv[2];
   double    gbr;
   char      bbn[14];
   char      ust_id[15];
   char      waehr_prim[2];
   double    konversion;
};

struct ADR {
   long      adr;
   char      adr_krz[17];
   char      adr_nam1[37];
   char      adr_nam2[37];
   short     adr_typ;
   char      adr_verkt[17];
   short     anr;
   short     delstatus;
   char      fax[21];
   short     fil;
   long      geb_dat;
   short     land;
   short     mdn;
   char      merkm_1[3];
   char      merkm_2[3];
   char      merkm_3[3];
   char      merkm_4[3];
   char      merkm_5[3];
   char      modem[21];
   char      ort1[37];
   char      ort2[37];
   char      partner[37];
   char      pf[17];
   char      plz[9];
   short     staat;
   char      str[37];
   char      tel[21];
   char      telex[21];
   long      txt_nr;
};

struct SMT_ZUORD {
   short     fil;
   short     hwg;
   short     mdn;
   short     verk_st;
   long      zuord_dat;
   short     fil_gr;
};

extern struct MDN _mdn, _mdn_null;
extern struct ADR _adr, _adr_null;
extern struct SMT_ZUORD smt_zuord, smt_zuord_null;

class MDN_CLASS 
{
       private :
               struct MDN mdn;
               struct ADR adr;
               short cursor_mdn_gr;
               short test_upd_cursor;
               short upd_cursor;
               short ins_cursor;
               short del_cursor;

       public :
               short cursor_mdn;
               MDN_CLASS ()
               {
                         cursor_mdn      = -1;
                         cursor_mdn_gr   = -1;
                         test_upd_cursor = -1;
                         upd_cursor      = -1;
                         ins_cursor      = -1;
                         del_cursor      = -1;
               }

               int lese_mdn (short);
               int lese_mdn (void);
               int lese_mdn_gr (short);
               int lese_mdn_gr (void);
               int ShowAllMdn (void);
};

class ADR_CLASS
{
     private :
               short cursor_adr;
               void prepare_adr (void);

     public :
               ADR_CLASS ()
               {
                         cursor_adr      = -1;
               }
               int lese_adr (long);
               int lese_adr (void);
};

class SMT_ZU_CLASS
{
      private :
               short cursor_smt_zuord;
               void prepare (void);

      public :
               SMT_ZU_CLASS ()
               {
                          cursor_smt_zuord = -1;
               }
               lese_smt_zuord (short, short, short, short);
};

#endif  /* _MDN_OK   */
