#ifndef AKT_KRZ_OK
#define AKT_KRZ_OK 1
struct AKT_KRZ {
   double    a;
   long      bel_akt_bis;
   long      bel_akt_von;
   short     fil;
   short     fil_gr;
   long      lad_akt_bis;
   long      lad_akt_von;
   char      lad_akv_sa[2];
   char      lief_akv_sa[2];
   short     mdn;
   short     mdn_gr;
   double    pr_ek_sa;
   double    pr_ek_sa_euro;
   double    pr_vk_sa;
   double    pr_vk_sa_euro;
   char      zeit_bis[7];
   char      zeit_von[7];
   short     zeit_von_n;
   short     zeit_bis_n;
   double    pr_vk1_sa;
   double    pr_vk2_sa;
   double    pr_vk3_sa;
   char      dr_status[2];
};

extern struct AKT_KRZ _akt_krz, _akt_krz_null;

class AKT_KRZ_CLASS 
{
       private :
               short cursor_akt_krz;
               short cursor_akt_krz_s;
               short cursor_akt_krz_d;
               short cursor_neu_pr;
               short test_upd_cursor;
               short test_neu_pr;
               short upd_cursor;
               short ins_cursor;
               short del_cursor;
               short upd_cursor1;
               short del_cursor1;
               short aktiv_cursor;
               short test_upd_a_pr;
               short upd_a_pr;
               short ins_a_pr;
       public :
               AKT_KRZ_CLASS ()
               {
                         cursor_akt_krz  = -1;
                         cursor_akt_krz_s = -1;
                         cursor_akt_krz_d = -1;
                         cursor_neu_pr   = -1;
                         test_upd_cursor = -1;
                         test_neu_pr     = -1;
                         upd_cursor      = -1;
                         upd_cursor1     = -1;
                         del_cursor1     = -1;
                         ins_cursor      = -1;
                         del_cursor      = -1;
                         aktiv_cursor     = -1;
               }

               int lese_akt_krz (double);
               int lese_akt_krz (void);
               int lese_akt_krz_s (short, short, short, short, double);
               int lese_akt_krz_s (void);
               int lese_akt_krz_d (short, short, short, short, double,
                                   char *);
               int lese_akt_krz_d (void);
               int lese_neu_pr (double);
               int lese_neu_pr (void);
               int update_akt_krz (double, short, short, short,
                                   short, char *, char *);
               int delete_akt_krz (double, short, short,
                                   short, short, char *);
               int update_neu_pr (double, short, short, short,
                                   short, char *, char *);
               int delete_neu_pr (double, short, short,
                                   short, short, char *);
               int aktion_aktiv (double, short, short, short, short);
               void prepare (void);
               int update_a_pr (double, short, short, short, short);
               void close_akt_krz (void);
               void close_akt_krz_s (void);
               void close_akt_krz_d (void);
               void close_neu_pr (void);
               void close_upd (void);
               void close_del (void);
               void close_aktiv (void);
               void close_del_n (void);
               void close_upd_n (void);
               void close_ins (void);
};
#endif
