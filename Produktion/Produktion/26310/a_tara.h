#ifndef _A_TARA_DEF
#define _A_TARA_DEF

#include "dbclass.h"

struct A_TARA {
   double    a;
   double    a_vpk;
   char      a_krz[17];
   double    a_gew;
};
extern struct A_TARA a_tara, a_tara_null;

#line 7 "a_tara.rh"

class A_TARA_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
               short cursor_all;
       public :
               A_TARA_CLASS () : DB_CLASS ()
               {
                    cursor_all = -1;
               }
               int dbreadfirst (void);
               int readfirstall(void);
               int readnextall (void);
};
#endif
