#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "a_kun.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "zerl_p.h"

struct ZERL_P zerl_p, zerl_p_null;

void ZERL_P_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &zerl_p.mdn, 1, 0);
            ins_quest ((char *) &zerl_p.partie, 3, 0);
            ins_quest ((char *) &zerl_p.a, 3, 0);
            ins_quest ((char *) &zerl_p.kng, 1, 0);
            ins_quest ((char *) &zerl_p.int_pos, 2, 0);
    out_quest ((char *) &zerl_p.mdn,1,0);
    out_quest ((char *) &zerl_p.partie,3,0);
    out_quest ((char *) &zerl_p.bearb,2,0);
    out_quest ((char *) &zerl_p.a,3,0);
    out_quest ((char *) &zerl_p.nto_gew,3,0);
    out_quest ((char *) &zerl_p.tara,3,0);
    out_quest ((char *) zerl_p.pers,0,13);
    out_quest ((char *) &zerl_p.lgr_zu,2,0);
    out_quest ((char *) &zerl_p.lgr_ab,2,0);
    out_quest ((char *) &zerl_p.kng,1,0);
    out_quest ((char *) &zerl_p.stk,3,0);
    out_quest ((char *) &zerl_p.we_txt,2,0);
    out_quest ((char *) zerl_p.zeit,0,11);
    out_quest ((char *) zerl_p.charge,0,31);
    out_quest ((char *) &zerl_p.int_pos,2,0);
    out_quest ((char *) &zerl_p.standort,1,0);
    out_quest ((char *) &zerl_p.fortschritt,1,0);
    out_quest ((char *) &zerl_p.pcharge,3,0);
    out_quest ((char *) &zerl_p.status,1,0);
    out_quest ((char *) &zerl_p.buch_gew,3,0);
    out_quest ((char *) &zerl_p.mhd,2,0);
    out_quest ((char *) zerl_p.es_nr,0,11);
    out_quest ((char *) zerl_p.ez_nz,0,11);
            cursor = prepare_sql ("select zerl_p.mdn,  "
"zerl_p.partie,  zerl_p.bearb,  zerl_p.a,  zerl_p.nto_gew,  zerl_p.tara,  "
"zerl_p.pers,  zerl_p.lgr_zu,  zerl_p.lgr_ab,  zerl_p.kng,  zerl_p.stk,  "
"zerl_p.we_txt,  zerl_p.zeit,  zerl_p.charge,  zerl_p.int_pos,  "
"zerl_p.standort,  zerl_p.fortschritt,  zerl_p.pcharge,  zerl_p.status,  "
"zerl_p.buch_gew,  zerl_p.mhd,  zerl_p.es_nr,  zerl_p.ez_nz from zerl_p "

#line 30 "zerl_p.rpp"
                                  "where mdn = ? "
                                  "and   partie = ? "
                                  "and   a = ? "
                                  "and   kng = ? and int_pos = ? ");

    ins_quest ((char *) &zerl_p.mdn,1,0);
    ins_quest ((char *) &zerl_p.partie,3,0);
    ins_quest ((char *) &zerl_p.bearb,2,0);
    ins_quest ((char *) &zerl_p.a,3,0);
    ins_quest ((char *) &zerl_p.nto_gew,3,0);
    ins_quest ((char *) &zerl_p.tara,3,0);
    ins_quest ((char *) zerl_p.pers,0,13);
    ins_quest ((char *) &zerl_p.lgr_zu,2,0);
    ins_quest ((char *) &zerl_p.lgr_ab,2,0);
    ins_quest ((char *) &zerl_p.kng,1,0);
    ins_quest ((char *) &zerl_p.stk,3,0);
    ins_quest ((char *) &zerl_p.we_txt,2,0);
    ins_quest ((char *) zerl_p.zeit,0,11);
    ins_quest ((char *) zerl_p.charge,0,31);
    ins_quest ((char *) &zerl_p.int_pos,2,0);
    ins_quest ((char *) &zerl_p.standort,1,0);
    ins_quest ((char *) &zerl_p.fortschritt,1,0);
    ins_quest ((char *) &zerl_p.pcharge,3,0);
    ins_quest ((char *) &zerl_p.status,1,0);
    ins_quest ((char *) &zerl_p.buch_gew,3,0);
    ins_quest ((char *) &zerl_p.mhd,2,0);
    ins_quest ((char *) zerl_p.es_nr,0,11);
    ins_quest ((char *) zerl_p.ez_nz,0,11);
            sqltext = "update zerl_p set zerl_p.mdn = ?,  "
"zerl_p.partie = ?,  zerl_p.bearb = ?,  zerl_p.a = ?,  "
"zerl_p.nto_gew = ?,  zerl_p.tara = ?,  zerl_p.pers = ?,  "
"zerl_p.lgr_zu = ?,  zerl_p.lgr_ab = ?,  zerl_p.kng = ?,  "
"zerl_p.stk = ?,  zerl_p.we_txt = ?,  zerl_p.zeit = ?,  "
"zerl_p.charge = ?,  zerl_p.int_pos = ?,  zerl_p.standort = ?,  "
"zerl_p.fortschritt = ?,  zerl_p.pcharge = ?,  zerl_p.status = ?,  "
"zerl_p.buch_gew = ?,  zerl_p.mhd = ?,  zerl_p.es_nr = ?,  "
"zerl_p.ez_nz = ? "

#line 36 "zerl_p.rpp"
                                  "where mdn = ? "
                                  "and   partie = ? "
                                  "and   a = ? "
                                  "and   kng = ? and int_pos = ? ";


            ins_quest ((char *) &zerl_p.mdn, 1, 0);
            ins_quest ((char *) &zerl_p.partie, 3, 0);
            ins_quest ((char *) &zerl_p.a, 3, 0);
            ins_quest ((char *) &zerl_p.kng, 1, 0);
            ins_quest ((char *) &zerl_p.int_pos, 2, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &zerl_p.mdn, 1, 0);
            ins_quest ((char *) &zerl_p.partie, 3, 0);
            ins_quest ((char *) &zerl_p.a, 3, 0);
            ins_quest ((char *) &zerl_p.kng, 1, 0);
            ins_quest ((char *) &zerl_p.int_pos, 2, 0);
            test_upd_cursor = prepare_sql ("select a from zerl_p "
                                  "where mdn = ? "
                                  "and   partie = ? "
                                  "and   a = ? "
                                  "and   kng = ? and int_pos = ? ");
                                  
            ins_quest ((char *) &zerl_p.mdn, 1, 0);
            ins_quest ((char *) &zerl_p.partie, 3, 0);
            ins_quest ((char *) &zerl_p.a, 3, 0);
            ins_quest ((char *) &zerl_p.kng, 1, 0);
            ins_quest ((char *) &zerl_p.int_pos, 2, 0);
            del_cursor = prepare_sql ("delete from zerl_p "
                                  "where mdn = ? "
                                  "and   partie = ? "
                                  "and   a = ? "
                                  "and   kng = ? and int_pos = ? ");
                                  
    ins_quest ((char *) &zerl_p.mdn,1,0);
    ins_quest ((char *) &zerl_p.partie,3,0);
    ins_quest ((char *) &zerl_p.bearb,2,0);
    ins_quest ((char *) &zerl_p.a,3,0);
    ins_quest ((char *) &zerl_p.nto_gew,3,0);
    ins_quest ((char *) &zerl_p.tara,3,0);
    ins_quest ((char *) zerl_p.pers,0,13);
    ins_quest ((char *) &zerl_p.lgr_zu,2,0);
    ins_quest ((char *) &zerl_p.lgr_ab,2,0);
    ins_quest ((char *) &zerl_p.kng,1,0);
    ins_quest ((char *) &zerl_p.stk,3,0);
    ins_quest ((char *) &zerl_p.we_txt,2,0);
    ins_quest ((char *) zerl_p.zeit,0,11);
    ins_quest ((char *) zerl_p.charge,0,31);
    ins_quest ((char *) &zerl_p.int_pos,2,0);
    ins_quest ((char *) &zerl_p.standort,1,0);
    ins_quest ((char *) &zerl_p.fortschritt,1,0);
    ins_quest ((char *) &zerl_p.pcharge,3,0);
    ins_quest ((char *) &zerl_p.status,1,0);
    ins_quest ((char *) &zerl_p.buch_gew,3,0);
    ins_quest ((char *) &zerl_p.mhd,2,0);
    ins_quest ((char *) zerl_p.es_nr,0,11);
    ins_quest ((char *) zerl_p.ez_nz,0,11);
            ins_cursor = prepare_sql ("insert into zerl_p ("
"mdn,  partie,  bearb,  a,  nto_gew,  tara,  pers,  lgr_zu,  lgr_ab,  kng,  stk,  we_txt,  zeit,  "
"charge,  int_pos,  standort,  fortschritt,  pcharge,  status,  buch_gew,  mhd,  "
"es_nr,  ez_nz) "

#line 72 "zerl_p.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?)"); 

#line 74 "zerl_p.rpp"
}

void ZERL_P_CLASS::prepare_o (char *order)
{

            ins_quest ((char *) &zerl_p.mdn, 1, 0);
            ins_quest ((char *) &zerl_p.partie, 3, 0);
            ins_quest ((char *) &zerl_p.kng, 1, 0);
            if (order)
            {
    out_quest ((char *) &zerl_p.mdn,1,0);
    out_quest ((char *) &zerl_p.partie,3,0);
    out_quest ((char *) &zerl_p.bearb,2,0);
    out_quest ((char *) &zerl_p.a,3,0);
    out_quest ((char *) &zerl_p.nto_gew,3,0);
    out_quest ((char *) &zerl_p.tara,3,0);
    out_quest ((char *) zerl_p.pers,0,13);
    out_quest ((char *) &zerl_p.lgr_zu,2,0);
    out_quest ((char *) &zerl_p.lgr_ab,2,0);
    out_quest ((char *) &zerl_p.kng,1,0);
    out_quest ((char *) &zerl_p.stk,3,0);
    out_quest ((char *) &zerl_p.we_txt,2,0);
    out_quest ((char *) zerl_p.zeit,0,11);
    out_quest ((char *) zerl_p.charge,0,31);
    out_quest ((char *) &zerl_p.int_pos,2,0);
    out_quest ((char *) &zerl_p.standort,1,0);
    out_quest ((char *) &zerl_p.fortschritt,1,0);
    out_quest ((char *) &zerl_p.pcharge,3,0);
    out_quest ((char *) &zerl_p.status,1,0);
    out_quest ((char *) &zerl_p.buch_gew,3,0);
    out_quest ((char *) &zerl_p.mhd,2,0);
    out_quest ((char *) zerl_p.es_nr,0,11);
    out_quest ((char *) zerl_p.ez_nz,0,11);
                    cursor_o = prepare_sql ("select "
"zerl_p.mdn,  zerl_p.partie,  zerl_p.bearb,  zerl_p.a,  zerl_p.nto_gew,  "
"zerl_p.tara,  zerl_p.pers,  zerl_p.lgr_zu,  zerl_p.lgr_ab,  zerl_p.kng,  "
"zerl_p.stk,  zerl_p.we_txt,  zerl_p.zeit,  zerl_p.charge,  "
"zerl_p.int_pos,  zerl_p.standort,  zerl_p.fortschritt,  "
"zerl_p.pcharge,  zerl_p.status,  zerl_p.buch_gew,  zerl_p.mhd,  "
"zerl_p.es_nr,  zerl_p.ez_nz from zerl_p "

#line 85 "zerl_p.rpp"
                                  "where mdn = ? "
                                  "and   partie = ? "
                                  "and   kng = ? %s", order );

            }
            else
            {
    out_quest ((char *) &zerl_p.mdn,1,0);
    out_quest ((char *) &zerl_p.partie,3,0);
    out_quest ((char *) &zerl_p.bearb,2,0);
    out_quest ((char *) &zerl_p.a,3,0);
    out_quest ((char *) &zerl_p.nto_gew,3,0);
    out_quest ((char *) &zerl_p.tara,3,0);
    out_quest ((char *) zerl_p.pers,0,13);
    out_quest ((char *) &zerl_p.lgr_zu,2,0);
    out_quest ((char *) &zerl_p.lgr_ab,2,0);
    out_quest ((char *) &zerl_p.kng,1,0);
    out_quest ((char *) &zerl_p.stk,3,0);
    out_quest ((char *) &zerl_p.we_txt,2,0);
    out_quest ((char *) zerl_p.zeit,0,11);
    out_quest ((char *) zerl_p.charge,0,31);
    out_quest ((char *) &zerl_p.int_pos,2,0);
    out_quest ((char *) &zerl_p.standort,1,0);
    out_quest ((char *) &zerl_p.fortschritt,1,0);
    out_quest ((char *) &zerl_p.pcharge,3,0);
    out_quest ((char *) &zerl_p.status,1,0);
    out_quest ((char *) &zerl_p.buch_gew,3,0);
    out_quest ((char *) &zerl_p.mhd,2,0);
    out_quest ((char *) zerl_p.es_nr,0,11);
    out_quest ((char *) zerl_p.ez_nz,0,11);
                    cursor_o = prepare_sql ("select "
"zerl_p.mdn,  zerl_p.partie,  zerl_p.bearb,  zerl_p.a,  zerl_p.nto_gew,  "
"zerl_p.tara,  zerl_p.pers,  zerl_p.lgr_zu,  zerl_p.lgr_ab,  zerl_p.kng,  "
"zerl_p.stk,  zerl_p.we_txt,  zerl_p.zeit,  zerl_p.charge,  "
"zerl_p.int_pos,  zerl_p.standort,  zerl_p.fortschritt,  "
"zerl_p.pcharge,  zerl_p.status,  zerl_p.buch_gew,  zerl_p.mhd,  "
"zerl_p.es_nr,  zerl_p.ez_nz from zerl_p "

#line 93 "zerl_p.rpp"
                                  "where mdn = ? "
                                  "and   partie = ? "
                                  "and   kng = ?");
           }

            ins_quest ((char *) &zerl_p.mdn, 1, 0);
            ins_quest ((char *) &zerl_p.partie, 3, 0);
            if (order)
            {
    out_quest ((char *) &zerl_p.mdn,1,0);
    out_quest ((char *) &zerl_p.partie,3,0);
    out_quest ((char *) &zerl_p.bearb,2,0);
    out_quest ((char *) &zerl_p.a,3,0);
    out_quest ((char *) &zerl_p.nto_gew,3,0);
    out_quest ((char *) &zerl_p.tara,3,0);
    out_quest ((char *) zerl_p.pers,0,13);
    out_quest ((char *) &zerl_p.lgr_zu,2,0);
    out_quest ((char *) &zerl_p.lgr_ab,2,0);
    out_quest ((char *) &zerl_p.kng,1,0);
    out_quest ((char *) &zerl_p.stk,3,0);
    out_quest ((char *) &zerl_p.we_txt,2,0);
    out_quest ((char *) zerl_p.zeit,0,11);
    out_quest ((char *) zerl_p.charge,0,31);
    out_quest ((char *) &zerl_p.int_pos,2,0);
    out_quest ((char *) &zerl_p.standort,1,0);
    out_quest ((char *) &zerl_p.fortschritt,1,0);
    out_quest ((char *) &zerl_p.pcharge,3,0);
    out_quest ((char *) &zerl_p.status,1,0);
    out_quest ((char *) &zerl_p.buch_gew,3,0);
    out_quest ((char *) &zerl_p.mhd,2,0);
    out_quest ((char *) zerl_p.es_nr,0,11);
    out_quest ((char *) zerl_p.ez_nz,0,11);
                    cursor_oz = prepare_sql ("select "
"zerl_p.mdn,  zerl_p.partie,  zerl_p.bearb,  zerl_p.a,  zerl_p.nto_gew,  "
"zerl_p.tara,  zerl_p.pers,  zerl_p.lgr_zu,  zerl_p.lgr_ab,  zerl_p.kng,  "
"zerl_p.stk,  zerl_p.we_txt,  zerl_p.zeit,  zerl_p.charge,  "
"zerl_p.int_pos,  zerl_p.standort,  zerl_p.fortschritt,  "
"zerl_p.pcharge,  zerl_p.status,  zerl_p.buch_gew,  zerl_p.mhd,  "
"zerl_p.es_nr,  zerl_p.ez_nz from zerl_p "

#line 103 "zerl_p.rpp"
                                  "where mdn = ? "
                                  "and   partie = ? "
                                  "and   kng in (1,2) %s", order );

            }
            else
            {
    out_quest ((char *) &zerl_p.mdn,1,0);
    out_quest ((char *) &zerl_p.partie,3,0);
    out_quest ((char *) &zerl_p.bearb,2,0);
    out_quest ((char *) &zerl_p.a,3,0);
    out_quest ((char *) &zerl_p.nto_gew,3,0);
    out_quest ((char *) &zerl_p.tara,3,0);
    out_quest ((char *) zerl_p.pers,0,13);
    out_quest ((char *) &zerl_p.lgr_zu,2,0);
    out_quest ((char *) &zerl_p.lgr_ab,2,0);
    out_quest ((char *) &zerl_p.kng,1,0);
    out_quest ((char *) &zerl_p.stk,3,0);
    out_quest ((char *) &zerl_p.we_txt,2,0);
    out_quest ((char *) zerl_p.zeit,0,11);
    out_quest ((char *) zerl_p.charge,0,31);
    out_quest ((char *) &zerl_p.int_pos,2,0);
    out_quest ((char *) &zerl_p.standort,1,0);
    out_quest ((char *) &zerl_p.fortschritt,1,0);
    out_quest ((char *) &zerl_p.pcharge,3,0);
    out_quest ((char *) &zerl_p.status,1,0);
    out_quest ((char *) &zerl_p.buch_gew,3,0);
    out_quest ((char *) &zerl_p.mhd,2,0);
    out_quest ((char *) zerl_p.es_nr,0,11);
    out_quest ((char *) zerl_p.ez_nz,0,11);
                    cursor_oz = prepare_sql ("select "
"zerl_p.mdn,  zerl_p.partie,  zerl_p.bearb,  zerl_p.a,  zerl_p.nto_gew,  "
"zerl_p.tara,  zerl_p.pers,  zerl_p.lgr_zu,  zerl_p.lgr_ab,  zerl_p.kng,  "
"zerl_p.stk,  zerl_p.we_txt,  zerl_p.zeit,  zerl_p.charge,  "
"zerl_p.int_pos,  zerl_p.standort,  zerl_p.fortschritt,  "
"zerl_p.pcharge,  zerl_p.status,  zerl_p.buch_gew,  zerl_p.mhd,  "
"zerl_p.es_nr,  zerl_p.ez_nz from zerl_p "

#line 111 "zerl_p.rpp"
                                  "where mdn = ? "
                                  "and   partie = ? "
                                  "and   kng in (1,2)");
            }

}
void ZERL_P_CLASS::prepare_f (char *order)
{

            ins_quest ((char *) &zerl_p.mdn, 1, 0);
            ins_quest ((char *) &zerl_p.status, 1, 0);
            ins_quest ((char *) &zerl_p.kng, 1, 0);

    out_quest ((char *) &zerl_p.mdn,1,0);
    out_quest ((char *) &zerl_p.partie,3,0);
    out_quest ((char *) &zerl_p.bearb,2,0);
    out_quest ((char *) &zerl_p.a,3,0);
    out_quest ((char *) &zerl_p.nto_gew,3,0);
    out_quest ((char *) &zerl_p.tara,3,0);
    out_quest ((char *) zerl_p.pers,0,13);
    out_quest ((char *) &zerl_p.lgr_zu,2,0);
    out_quest ((char *) &zerl_p.lgr_ab,2,0);
    out_quest ((char *) &zerl_p.kng,1,0);
    out_quest ((char *) &zerl_p.stk,3,0);
    out_quest ((char *) &zerl_p.we_txt,2,0);
    out_quest ((char *) zerl_p.zeit,0,11);
    out_quest ((char *) zerl_p.charge,0,31);
    out_quest ((char *) &zerl_p.int_pos,2,0);
    out_quest ((char *) &zerl_p.standort,1,0);
    out_quest ((char *) &zerl_p.fortschritt,1,0);
    out_quest ((char *) &zerl_p.pcharge,3,0);
    out_quest ((char *) &zerl_p.status,1,0);
    out_quest ((char *) &zerl_p.buch_gew,3,0);
    out_quest ((char *) &zerl_p.mhd,2,0);
    out_quest ((char *) zerl_p.es_nr,0,11);
    out_quest ((char *) zerl_p.ez_nz,0,11);
                    cursor_f = prepare_sql ("select "
"zerl_p.mdn,  zerl_p.partie,  zerl_p.bearb,  zerl_p.a,  zerl_p.nto_gew,  "
"zerl_p.tara,  zerl_p.pers,  zerl_p.lgr_zu,  zerl_p.lgr_ab,  zerl_p.kng,  "
"zerl_p.stk,  zerl_p.we_txt,  zerl_p.zeit,  zerl_p.charge,  "
"zerl_p.int_pos,  zerl_p.standort,  zerl_p.fortschritt,  "
"zerl_p.pcharge,  zerl_p.status,  zerl_p.buch_gew,  zerl_p.mhd,  "
"zerl_p.es_nr,  zerl_p.ez_nz from zerl_p "

#line 125 "zerl_p.rpp"
                                  "where mdn = ? "
                                  "and   status <= ? "
                                  "and   kng = ? %s", order );
}

int ZERL_P_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int ZERL_P_CLASS::dbreadfirst_o (char *order)
/**
Ersten Satz aus Tabelle lesen.
**/
{
     
         if (cursor == -1)
         {
                this->prepare ();
         }
         if (cursor_o == -1)
         {
                this->prepare_o (order);
         }
         open_sql (cursor_o);
         fetch_sql (cursor_o);
         return (sqlstatus);
}
int ZERL_P_CLASS::dbreadfirst_f (char *order)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor_f == -1)
         {
                this->prepare_f (order);
         }
         open_sql (cursor_f);
         fetch_sql (cursor_f);
         return (sqlstatus);
}

int ZERL_P_CLASS::dbread_o ()
/**
Ersten Satz aus Tabelle lesen.
**/
{
       
         fetch_sql (cursor_o);
         return (sqlstatus);
}
int ZERL_P_CLASS::dbread_f ()
/**
Ersten Satz aus Tabelle lesen.
**/
{
       
         fetch_sql (cursor_f);
         return (sqlstatus);
}

int ZERL_P_CLASS::dbclose_o ()
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1) return 0;
         close_sql (cursor_o);
         cursor_o = -1;
         close_sql (cursor_oz);
         cursor_oz = -1;
         return (0);
}
int ZERL_P_CLASS::dbclose_f ()
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor_f == -1) return 0;
         close_sql (cursor_f);
         cursor_f = -1;
         return (0);
}

int ZERL_P_CLASS::dbreadfirst_oz (char *order)
/**
Ersten Satz aus Tabelle lesen.
**/
{
     
         if (cursor == -1)
         {
                this->prepare ();
         }
         if (cursor_o == -1)
         {
                this->prepare_o (order);
         }
         open_sql (cursor_oz);
         fetch_sql (cursor_oz);
         return (sqlstatus);
}

int ZERL_P_CLASS::dbread_oz ()
/**
Ersten Satz aus Tabelle lesen.
**/
{
       
         fetch_sql (cursor_oz);
         return (sqlstatus);
}


int ZERL_P_CLASS::dbupdate (int dm)
/**
Tabelle eti Updaten.
**/
{
	     if (cursor == -1)
		 {
			 prepare ();
		 }

         open_sql (test_upd_cursor);
         fetch_sql (test_upd_cursor);
         if (sqlstatus == 100)
         {
         	    if (dm == 7 || dm == 8) return 0;  //Bei PRODUKTIONSFORTSCHRITT  und BUCHENFERTIGLAGER  Kein Insert erlaubt
                  execute_curs (ins_cursor);
         }  
         else if (sqlstatus == 0)
         {
                   execute_curs (upd_cursor);
         }  
          
         return 0;
} 