create table "fit".zerl_k 
  (
    mdn smallint,
    partie integer,
    partie_bz char(48),
    lief char(16),
    zer_part char(8),   -- referenz zu zer_partk,zer_partp
    zer_dat date,
    delstatus smallint,
    schnitt smallint,
    partie_status smallint
  ) in fit_dat extent size 32 next size 256 lock mode row;
revoke all on "fit".zerl_k from "public";

create unique index "fit".i01zerl_k on "fit".zerl_k 
    (mdn,partie) in fit_dat ;

    
create table "fit".zerl_p 
  (
    mdn smallint,
    partie integer,
    bearb date,
    a decimal(13,0),
    nto_gew decimal(8,3),
    tara decimal(5,2),
    pers char(12),
    lgr_zu integer,
    lgr_ab integer,
    kng smallint,      -- Zerlegezugang = 1, abgang = 0
    stk decimal(5,2),
    we_txt integer,
    zeit char(10),
    charge char(30),
    int_pos integer,
    standort smallint
  ) in fit_dat extent size 32 next size 256 lock mode row;
revoke all on "fit".zerl_p from "public";

create unique index "fit".i01zerl_p on "fit".zerl_p 
    (mdn,partie,a,kng,int_pos) in fit_dat ;

{----24.01.2010----}
create table "fit".gebindetara 
  (
    lfd smallint,
    a decimal(13,0),   -- Leergut:  a = 0 Verpackung > 0
    zaehler integer,
    bezeichnung char(60),
    gebinde1 decimal(13,0),
    anz1 smallint,
    gebinde2 decimal(13,0),
    anz2 smallint,
    gebinde3 decimal(13,0),
    anz3 smallint,
    gebinde4 decimal(13,0),
    anz4 smallint,
    gebinde5 decimal(13,0),
    anz5 smallint,
    gebinde6 decimal(13,0),
    anz6 smallint,
    akv date,
    tara decimal(8,3)
  ) in fit_dat extent size 32 next size 256 lock mode row;
revoke all on "fit".gebindetara from "public";

create unique index "fit".i01gebindetara on "fit".gebindetara 
    (a,bezeichnung) in fit_dat ;
    

{----26.01.2010----}
create table "fit".a_tara 
  (
    a decimal(13,0),
    a_vpk decimal(13,0),
    a_krz char(16),
    a_gew decimal(8,3)
  ) in fit_dat extent size 32 next size 256 lock mode row;
revoke all on "fit".a_tara from "public";

create unique index "fit".i01a_tara on "fit".a_tara (a,a_vpk) in fit_dat 
    ;

    