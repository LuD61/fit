#ifndef _QSTXT_DEF
#define _QSTXT_DEF

#include "windows.h"
#include "mo_draw.h"
#include "wmask.h"
#include "dbclass.h"

struct QSTXT {
   short     qstyp;
   char      txt[25];
   short     sort;
   short     hwg;
   short     wg;
   long      ag;
   char      lief[17];
   char      mtxt1[37];
   char      mtxt2[37];
   char      mtxt3[37];
};
extern struct QSTXT qstxt, qstxt_null;

#line 10 "qstxt.rh"

class QSTXT_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               QSTXT_CLASS () : DB_CLASS ()
               {
               }
               int dbreadfirst (void);

               ~QSTXT_CLASS ()
               {
               } 
};
#endif
