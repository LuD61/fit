#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "a_kun.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "gebindetara.h"

struct GEBINDETARA gebindetara, gebindetara_null;

void GEBINDETARA_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &gebindetara.a, 3, 0);
            ins_quest ((char *) &gebindetara.bezeichnung, 0, 60);
    out_quest ((char *) &gebindetara.lfd,1,0);
    out_quest ((char *) &gebindetara.a,3,0);
    out_quest ((char *) &gebindetara.zaehler,2,0);
    out_quest ((char *) gebindetara.bezeichnung,0,61);
    out_quest ((char *) &gebindetara.gebinde1,3,0);
    out_quest ((char *) &gebindetara.anz1,1,0);
    out_quest ((char *) &gebindetara.gebinde2,3,0);
    out_quest ((char *) &gebindetara.anz2,1,0);
    out_quest ((char *) &gebindetara.gebinde3,3,0);
    out_quest ((char *) &gebindetara.anz3,1,0);
    out_quest ((char *) &gebindetara.gebinde4,3,0);
    out_quest ((char *) &gebindetara.anz4,1,0);
    out_quest ((char *) &gebindetara.gebinde5,3,0);
    out_quest ((char *) &gebindetara.anz5,1,0);
    out_quest ((char *) &gebindetara.gebinde6,3,0);
    out_quest ((char *) &gebindetara.anz6,1,0);
    out_quest ((char *) &gebindetara.akv,2,0);
    out_quest ((char *) &gebindetara.tara,3,0);
            cursor = prepare_sql ("select gebindetara.lfd,  "
"gebindetara.a,  gebindetara.zaehler,  gebindetara.bezeichnung,  "
"gebindetara.gebinde1,  gebindetara.anz1,  gebindetara.gebinde2,  "
"gebindetara.anz2,  gebindetara.gebinde3,  gebindetara.anz3,  "
"gebindetara.gebinde4,  gebindetara.anz4,  gebindetara.gebinde5,  "
"gebindetara.anz5,  gebindetara.gebinde6,  gebindetara.anz6,  "
"gebindetara.akv,  gebindetara.tara from gebindetara "

#line 27 "gebindetara.rpp"
                                  "where a = ? and bezeichnung = ? ");

            ins_quest ((char *) &gebindetara.a, 3, 0);
    out_quest ((char *) &gebindetara.lfd,1,0);
    out_quest ((char *) &gebindetara.a,3,0);
    out_quest ((char *) &gebindetara.zaehler,2,0);
    out_quest ((char *) gebindetara.bezeichnung,0,61);
    out_quest ((char *) &gebindetara.gebinde1,3,0);
    out_quest ((char *) &gebindetara.anz1,1,0);
    out_quest ((char *) &gebindetara.gebinde2,3,0);
    out_quest ((char *) &gebindetara.anz2,1,0);
    out_quest ((char *) &gebindetara.gebinde3,3,0);
    out_quest ((char *) &gebindetara.anz3,1,0);
    out_quest ((char *) &gebindetara.gebinde4,3,0);
    out_quest ((char *) &gebindetara.anz4,1,0);
    out_quest ((char *) &gebindetara.gebinde5,3,0);
    out_quest ((char *) &gebindetara.anz5,1,0);
    out_quest ((char *) &gebindetara.gebinde6,3,0);
    out_quest ((char *) &gebindetara.anz6,1,0);
    out_quest ((char *) &gebindetara.akv,2,0);
    out_quest ((char *) &gebindetara.tara,3,0);
            cursor_all = prepare_sql ("select "
"gebindetara.lfd,  gebindetara.a,  gebindetara.zaehler,  "
"gebindetara.bezeichnung,  gebindetara.gebinde1,  gebindetara.anz1,  "
"gebindetara.gebinde2,  gebindetara.anz2,  gebindetara.gebinde3,  "
"gebindetara.anz3,  gebindetara.gebinde4,  gebindetara.anz4,  "
"gebindetara.gebinde5,  gebindetara.anz5,  gebindetara.gebinde6,  "
"gebindetara.anz6,  gebindetara.akv,  gebindetara.tara from gebindetara where a = ? order by zaehler desc ");

#line 31 "gebindetara.rpp"
            
    ins_quest ((char *) &gebindetara.lfd,1,0);
    ins_quest ((char *) &gebindetara.a,3,0);
    ins_quest ((char *) &gebindetara.zaehler,2,0);
    ins_quest ((char *) gebindetara.bezeichnung,0,61);
    ins_quest ((char *) &gebindetara.gebinde1,3,0);
    ins_quest ((char *) &gebindetara.anz1,1,0);
    ins_quest ((char *) &gebindetara.gebinde2,3,0);
    ins_quest ((char *) &gebindetara.anz2,1,0);
    ins_quest ((char *) &gebindetara.gebinde3,3,0);
    ins_quest ((char *) &gebindetara.anz3,1,0);
    ins_quest ((char *) &gebindetara.gebinde4,3,0);
    ins_quest ((char *) &gebindetara.anz4,1,0);
    ins_quest ((char *) &gebindetara.gebinde5,3,0);
    ins_quest ((char *) &gebindetara.anz5,1,0);
    ins_quest ((char *) &gebindetara.gebinde6,3,0);
    ins_quest ((char *) &gebindetara.anz6,1,0);
    ins_quest ((char *) &gebindetara.akv,2,0);
    ins_quest ((char *) &gebindetara.tara,3,0);
            sqltext = "update gebindetara set "
"gebindetara.lfd = ?,  gebindetara.a = ?,  gebindetara.zaehler = ?,  "
"gebindetara.bezeichnung = ?,  gebindetara.gebinde1 = ?,  "
"gebindetara.anz1 = ?,  gebindetara.gebinde2 = ?,  "
"gebindetara.anz2 = ?,  gebindetara.gebinde3 = ?,  "
"gebindetara.anz3 = ?,  gebindetara.gebinde4 = ?,  "
"gebindetara.anz4 = ?,  gebindetara.gebinde5 = ?,  "
"gebindetara.anz5 = ?,  gebindetara.gebinde6 = ?,  "
"gebindetara.anz6 = ?,  gebindetara.akv = ?,  gebindetara.tara = ? "

#line 33 "gebindetara.rpp"
                                  "where a = ? and bezeichnung = ? ";


            ins_quest ((char *) &gebindetara.a, 3, 0);
            ins_quest ((char *) &gebindetara.bezeichnung, 0, 60);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &gebindetara.a, 3, 0);
            ins_quest ((char *) &gebindetara.bezeichnung, 0, 60);
            test_upd_cursor = prepare_sql ("select tara from gebindetara "
                                  "where a = ? and bezeichnung = ? ");
            ins_quest ((char *) &gebindetara.a, 3, 0);
            ins_quest ((char *) &gebindetara.bezeichnung, 0, 60);
            del_cursor = prepare_sql ("delete from gebindetara "
                                  "where a = ? and bezeichnung = ? ");
    ins_quest ((char *) &gebindetara.lfd,1,0);
    ins_quest ((char *) &gebindetara.a,3,0);
    ins_quest ((char *) &gebindetara.zaehler,2,0);
    ins_quest ((char *) gebindetara.bezeichnung,0,61);
    ins_quest ((char *) &gebindetara.gebinde1,3,0);
    ins_quest ((char *) &gebindetara.anz1,1,0);
    ins_quest ((char *) &gebindetara.gebinde2,3,0);
    ins_quest ((char *) &gebindetara.anz2,1,0);
    ins_quest ((char *) &gebindetara.gebinde3,3,0);
    ins_quest ((char *) &gebindetara.anz3,1,0);
    ins_quest ((char *) &gebindetara.gebinde4,3,0);
    ins_quest ((char *) &gebindetara.anz4,1,0);
    ins_quest ((char *) &gebindetara.gebinde5,3,0);
    ins_quest ((char *) &gebindetara.anz5,1,0);
    ins_quest ((char *) &gebindetara.gebinde6,3,0);
    ins_quest ((char *) &gebindetara.anz6,1,0);
    ins_quest ((char *) &gebindetara.akv,2,0);
    ins_quest ((char *) &gebindetara.tara,3,0);
            ins_cursor = prepare_sql ("insert into gebindetara ("
"lfd,  a,  zaehler,  bezeichnung,  gebinde1,  anz1,  gebinde2,  anz2,  gebinde3,  anz3,  "
"gebinde4,  anz4,  gebinde5,  anz5,  gebinde6,  anz6,  akv,  tara) "

#line 49 "gebindetara.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?)"); 

#line 51 "gebindetara.rpp"
}
int GEBINDETARA_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int GEBINDETARA_CLASS::readfirstall (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         open_sql (cursor_all);
         fetch_sql (cursor_all);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}
int GEBINDETARA_CLASS::readnextall (void)
/**
**/
{
         fetch_sql (cursor_all);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}
