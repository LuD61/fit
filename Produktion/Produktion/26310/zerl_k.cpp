#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "a_kun.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "zerl_k.h"

struct ZERL_K zerl_k, zerl_k_null;

void ZERL_K_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &zerl_k.mdn, 1, 0);
            ins_quest ((char *) &zerl_k.partie, 3, 0);
    out_quest ((char *) &zerl_k.mdn,1,0);
    out_quest ((char *) &zerl_k.partie,3,0);
    out_quest ((char *) zerl_k.partie_bz,0,49);
    out_quest ((char *) zerl_k.lief,0,17);
    out_quest ((char *) zerl_k.zer_part,0,9);
    out_quest ((char *) &zerl_k.zer_dat,2,0);
    out_quest ((char *) &zerl_k.delstatus,1,0);
    out_quest ((char *) &zerl_k.schnitt,1,0);
    out_quest ((char *) &zerl_k.partie_status,1,0);
    out_quest ((char *) &zerl_k.a,3,0);
    out_quest ((char *) &zerl_k.fanggebiet,1,0);
    out_quest ((char *) &zerl_k.a_prod,3,0);
            cursor = prepare_sql ("select zerl_k.mdn,  "
"zerl_k.partie,  zerl_k.partie_bz,  zerl_k.lief,  zerl_k.zer_part,  "
"zerl_k.zer_dat,  zerl_k.delstatus,  zerl_k.schnitt,  "
"zerl_k.partie_status,  zerl_k.a,  zerl_k.fanggebiet,  zerl_k.a_prod from zerl_k "

#line 27 "zerl_k.rpp"
                                  "where mdn = ? "
                                  "and   partie = ?");

    ins_quest ((char *) &zerl_k.mdn,1,0);
    ins_quest ((char *) &zerl_k.partie,3,0);
    ins_quest ((char *) zerl_k.partie_bz,0,49);
    ins_quest ((char *) zerl_k.lief,0,17);
    ins_quest ((char *) zerl_k.zer_part,0,9);
    ins_quest ((char *) &zerl_k.zer_dat,2,0);
    ins_quest ((char *) &zerl_k.delstatus,1,0);
    ins_quest ((char *) &zerl_k.schnitt,1,0);
    ins_quest ((char *) &zerl_k.partie_status,1,0);
    ins_quest ((char *) &zerl_k.a,3,0);
    ins_quest ((char *) &zerl_k.fanggebiet,1,0);
    ins_quest ((char *) &zerl_k.a_prod,3,0);
            sqltext = "update zerl_k set zerl_k.mdn = ?,  "
"zerl_k.partie = ?,  zerl_k.partie_bz = ?,  zerl_k.lief = ?,  "
"zerl_k.zer_part = ?,  zerl_k.zer_dat = ?,  zerl_k.delstatus = ?,  "
"zerl_k.schnitt = ?,  zerl_k.partie_status = ?,  zerl_k.a = ?,  "
"zerl_k.fanggebiet = ?,  zerl_k.a_prod = ? "

#line 31 "zerl_k.rpp"
                                  "where mdn = ? "
                                  "and   partie = ?";

            ins_quest ((char *) &zerl_k.mdn, 1, 0);
            ins_quest ((char *) &zerl_k.partie, 3, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &zerl_k.mdn, 1, 0);
            ins_quest ((char *) &zerl_k.partie, 3, 0);
            test_upd_cursor = prepare_sql ("select partie from zerl_k "
                                  "where mdn = ? "
                                  "and   partie = ?");
            ins_quest ((char *) &zerl_k.mdn, 1, 0);
            ins_quest ((char *) &zerl_k.partie, 3, 0);
            del_cursor = prepare_sql ("delete from zerl_k "
                                  "where mdn = ? "
                                  "and   partie = ?");
    ins_quest ((char *) &zerl_k.mdn,1,0);
    ins_quest ((char *) &zerl_k.partie,3,0);
    ins_quest ((char *) zerl_k.partie_bz,0,49);
    ins_quest ((char *) zerl_k.lief,0,17);
    ins_quest ((char *) zerl_k.zer_part,0,9);
    ins_quest ((char *) &zerl_k.zer_dat,2,0);
    ins_quest ((char *) &zerl_k.delstatus,1,0);
    ins_quest ((char *) &zerl_k.schnitt,1,0);
    ins_quest ((char *) &zerl_k.partie_status,1,0);
    ins_quest ((char *) &zerl_k.a,3,0);
    ins_quest ((char *) &zerl_k.fanggebiet,1,0);
    ins_quest ((char *) &zerl_k.a_prod,3,0);
            ins_cursor = prepare_sql ("insert into zerl_k ("
"mdn,  partie,  partie_bz,  lief,  zer_part,  zer_dat,  delstatus,  schnitt,  "
"partie_status,  a,  fanggebiet,  a_prod) "

#line 49 "zerl_k.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?)"); 

#line 51 "zerl_k.rpp"
}
int ZERL_K_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

