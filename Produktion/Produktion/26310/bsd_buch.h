#ifndef _BSD_BUCH_DEF
#define _BSD_BUCH_DEF

#include "dbclass.h"

struct BSD_BUCH {
   long      nr;
   char      blg_typ[3];
   short     mdn;
   short     fil;
   short     kun_fil;
   double    a;
   long      dat;
   char      zeit[9];
   char      pers[13];
   char      bsd_lgr_ort[13];
   short     qua_status;
   double    me;
   double    bsd_ek_vk;
   char      chargennr[31];
   char      ident_nr[21];
   char      herk_nachw[14];
   char      lief[17];
   long      auf;
   char      verfall[2];
   long      verf_dat;
   short     delstatus;
   char      err_txt[17];
   double    me2;
   short     me_einh2;
   long      lieferdat;
   double    a_grund;
   short     me_einh;
};
extern struct BSD_BUCH bsd_buch, bsd_buch_null;

#line 7 "bsd_buch.rh"

class BSD_BUCH_CLASS : public DB_CLASS
{
       private :
               int ins_buch_cursor;
               void prepare (void);
               
       public :
               BSD_BUCH_CLASS () : DB_CLASS ()
               {
				   ins_buch_cursor = -1;
               }
               void BucheBsd (int,double,char*,int);
};
#endif
