// 251109    26310 erstellt aus 35900   
#include<stdio.h>
#include<stdarg.h>
#include<string.h>
#include<windows.h>
#include<winbase.h>
#include "fit.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_curso.h"
#include "mo_menu.h"
#include "mo_expl.h"
#include "mo_draw.h"
#include "wmask.h"
#include "mo_meld.h"
#include "a_bas.h"
#include "a_hndw.h"
#include "ptab.h"
#include "gebindetara.h"
#include "dbfunc.h"
#include "fnb.h"
#include "mdn.h"
#include "fil.h"
#include "lief.h"
#include "zerl_k.h"
#include "zerl_p.h"
#include "best_kopf.h"
#include "best_pos.h"
#include "a_pr.h"
#include "akt_krz.h"
#include "aktion.h"
#include "mo_geb.h"
#include "mo_numme.h"
#include "sys_peri.h"
#include "pht_wg.h"
#include "sys_par.h"
#include "mo_progcfg.h"
#include "datum.h"
#include "dbclass.h"
#include "lief_bzg.h"
#include "mo_nr.h"
#include "we_wiepro.h"
#include "mo_gdruck.h"
#include "nublock.h"
#include "mo_qm.h"
#include "a_kalkhndw.h"
#include "a_kalk_mat.h"
#include "a_best.h"
#include "touchf.h"
#include "textblock.h"
#include "Text.h"
#include "Vector.h"
#include "rind.h"
#include "zerldaten.h"
#include "bsd_buch.h"
#include "mo_passw.h"
#include "scanean.h"
#include "Integer.h"
#include "we_txt.h"
#include "ls.h"
#include "lgr.h"
#include "zer_partk.h"
#include "zer_partp.h"
#include "a_lgr.h"
#include "Sounds.h"
#include "a_tara.h"
#include "qstxt.h"
#include "qswepos.h"
#include "resource.h"
#include "prdk_k.h"
#include "atext.h"
#include "DllPreise.h"

#define _CPP 1
#include "etubi.h"

static BOOL GraphikMode = FALSE;

#define TIMER_PASSW 2

#define AUFMAX 1000
#define GEDRUCKT 2
#define VERBUCHT 3
#define EINGANG 1
#define AUSGANG 2
#define UMLAGERUNG 3
#define ZERLEGUNG 4
#define FEINZERLEGUNG 5
#define PRODUKTIONSSTART 6
#define PRODUKTIONSFORTSCHRITT 7
#define BUCHENFERTIGLAGER 8


#define LEERGUT 1
#define NEUES_LEERGUT 2
#define VERPACKUNGEN 3
#define NEUE_VERPACKUNGEN 4
#define SETZE_TARA 5


#define LEERMAX 100

#define ENTER 0
#define SUCHEN 1
#define AUSWAHL 2

#define BWS "BWS"
#define RSDIR "RSDIR"

#define sql_in ins_quest
#define sql_out out_quest

#define BAR 6
#define SOFORT 1

#define WIEGEN 9
#define AUSZEICHNEN 1
#define STOP 2
#define MINUS -1
#define PLUS 1

#define VK_CLS 910

#define STANDARD 0
#define ENTNAHME 1

static BOOL FlgNeuePartie = FALSE;
static int screenwidth  = 800;
static int screenheight = 580;
static BOOL GebTara = FALSE;
static int  PassWord = FALSE;
static int  TimerPassWord = 0;

static int EnterTyp = ENTER;

static int break_lief_enter = 0;

static short NextStep = 0;
static int ReadLS (void);
static int GenPartie (void);
static double GenPartie0 (void);
static char sqlstring [0x1000];
static BOOL stopauswahl   = FALSE;
static BOOL rdoptimize = FALSE;
static BOOL rdoptimizepos = FALSE;
static BOOL rdoptactive = TRUE;;
static BOOL query_changed = TRUE;
static int mit_trans = 1;
static int paz_direkt = 0;
static int auto_eti = 0;
static long auszeichner;
static int ls_charge_par = 0;
static int NeuePartie = 0;
static int lsc2_par = 0;
static long StdWaage;
static double AktPrWieg[1000];
static double StornoWieg;
static int AktWiegPos = 0;
static BOOL CalcOn = FALSE;
static int aufart9999;
static short auf_art = 0;
static BOOL best_ausw_direct = FALSE;
static BOOL liste_direct = FALSE;
static BOOL a_ausw_direct = FALSE;
static BOOL lief_ausw_direct = FALSE;
static BOOL hbk_direct = FALSE;
static int wieg_neu_direct = 0;
static BOOL wieg_direct = FALSE;
static BOOL hand_direct = FALSE;
static BOOL lief_bzg_zwang = FALSE;
static BOOL druck_zwang = FALSE;
static BOOL sort_a      = FALSE;
static BOOL hand_tara_sum = 0;
static BOOL fest_tara_sum = 0;
static BOOL fest_tara_anz = TRUE;
static BOOL EktoMeEinh = FALSE;
static long AutoSleep = 20;
static short QsZwang = FALSE;
static BOOL autocursor = TRUE;
static BOOL DrKomplett = TRUE;
static BOOL ScannMode = FALSE;
static BOOL InScanning = FALSE;
static double akt_buch_me;
static double a_wahl = 0.0;
static char ChargeAuswahl[14];
	    static BOOL EinhVPOK = FALSE;
	    static BOOL EinhLPOK = FALSE;

static BOOL AuswahlAktiv = FALSE;
static 	char bsd_lager [13];
static int dschnitt = 0;
static long lzerldat = 0;

static char KopfText[61];
static char KopfTextTab[LEERMAX][61];
static char KopfTextFTab[10][61];
static int kopftextfpos;
static int kopftextfscroll;

static int kopftextpos = 0;
static int kopftextanz = 0;

static BOOL AuswahlZerlegeeingang = FALSE;
static BOOL AuswahlZerlegeausgang = FALSE;
static BOOL AuswahlUmlagerung     = TRUE;
static BOOL AuswahlZerlegung      = FALSE;
static BOOL AuswahlFeinZerlegung  = FALSE;
static BOOL AuswahlProduktionsstart = FALSE;
static BOOL AuswahlProduktionsfortschritt = FALSE;
static BOOL AuswahlBuchenFertiglager = FALSE;
static BOOL DirektScannProduktionsstart = FALSE;
static BOOL DirektScannProduktionsfortschritt = FALSE;
static BOOL DirektScannBuchenFertiglager = FALSE;

static int zutat_at = 0;   // FISCH-5
static int zutat_at_par = 0; // FISCH-5

static int WiegeTyp = STANDARD;
BOOL ScanMulti = FALSE;
int Schwundabzug = 0;

char zeit [12];
static char *LZutxt = "Buchen in Lager :";
static char *LAbtxt = "Holen aus Lager :";
static short ZELagerAb = 0;
static short ZELagerZu = 0;
static short ZALagerAb = 0;
static short ZALagerZu = 0;
static short UMLagerAb = 0;
static short UMLagerZu = 0;
static short ZRLagerAb = 0;
static short ZRLagerZu = 0;
static short FZRLagerAb = 0;
static short FZRLagerZu = 0;
static short A6_LagerAb = 0;
static short A6_LagerZu = 0;
static short A7_LagerAb = 0;
static short A7_LagerZu = 0;
static short A8_LagerAb = 0;
static short A8_LagerZu = 0;
static short Standort = 0;
static char Standorttxt[60];
static short LagerAb = 0;
static short LagerZu = 0;
static double PartieNr = 0.0;
static char LagerAbtxt[20];
static char LagerZutxt[20];
static char LgrZu_krz[20];
static char LgrAb_krz[20];
static char Zuruecktext[20];
static char Einstiegtxt[20];
static char Auswahltxt[20];
static BOOL QsPosWieg = FALSE;

char ctara [12];

static COLORREF KompfColor = BLACKCOL;
static COLORREF KompbColor = GRAYCOL;
BOOL            ListRowColor (COLORREF *, COLORREF *, int);






static char KomDatum [12];
char cha [21];


static int MitKunde = 0;
static char *EinAusgang   = NULL;
static BOOL WithSpezEti =  TRUE;//FISCH-5 test
static BOOL WithLagerAuswahl =  TRUE;
static BOOL WithZusatz =  TRUE;  //FISCH-5 test
static short generiere_charge =  0;
static char *eti_kiste   = NULL;
static char *eti_nve   = NULL;
static char *eti_ez      = NULL;
static char *eti_ev      = NULL;
static char *eti_artikel = NULL;
static char *eti_artikel1 = NULL;
static char *eti_artikel2 = NULL;
static char *eti_artikel3 = NULL;
static char *eti_artikel4 = NULL;
static char *eti_artikel5 = NULL;
static char *eti_artikel6 = NULL;
static char *eti_artikel7 = NULL;
static char *eti_artikel8 = NULL;

static char *eti_ki_drk  = NULL;
static char *eti_nve_drk  = NULL;
static char *eti_ez_drk  = NULL;
static char *eti_ev_drk  = NULL;
static char *eti_a_drk   = NULL;

static int eti_ki_typ  = -1;
static int eti_nve_typ  = -1;
static int eti_ez_typ  = -1;
static int eti_ev_typ  = -1;
static int eti_a_typ   = -1;


static double pr_ek_bto0;
static double pr_ek0;
static double pr_ek_bto0_euro;
static double pr_ek0_euro;

static int testmode = 0;
extern BOOL ColBorder;
static BOOL colborder = FALSE;
static BOOL lsdefault = FALSE;
static BOOL multibest = TRUE;

static char LONGNULL []   = {(UCHAR) 0x00, (UCHAR) 0x00, (UCHAR) 0x00, (UCHAR) 0x80};
static long LongNull = 0x80000000;

static int best_koresp;

static int PtX = -1;
static int PtY = -1;
long akt_lager_ab = 0;
long akt_lager_zu = 0;

//  Prototypen

int      PASCAL WinMain(HANDLE,HANDLE,LPSTR,int);
LONG FAR PASCAL WndProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL GebProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL FitLogoProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL UhrProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL EntNumProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL EntNuProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL TouchfProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL PreisProc(HWND,UINT, WPARAM,LPARAM);
LONG FAR PASCAL ListKopfProc(HWND,UINT, WPARAM,LPARAM);
LONG FAR PASCAL PtFussProc(HWND,UINT, WPARAM,LPARAM);
LONG FAR PASCAL MsgProc(HWND,UINT, WPARAM,LPARAM);
LONG FAR PASCAL PosLeerProc(HWND,UINT, WPARAM,LPARAM);
int             ShowWKopf (HWND, char * , int, char *,
                 int, form *, form *, form *);
void            fetch_std_geb (void);
void            update_kun_geb (void);
long            GetStdAuszeichner (void);
long            GetStdWaage (void);
int             AuswahlSysPeri (void);
int             AuswahlKopf (void);
int				AuswahlLager (int,int);
int				AuswahlPartie (void);
double			ScanneCharge (void);
double			ScanneChargeFeinZerl (void);
int				BelegeZerl_p (void);
int				AuswahlALager (int,bool);
double	    	AuswahlVerpackungstara (short);
int				AuswahlLager (void);
void            IsDblClck (int);
void            GetAufArt9999 (void);
int             WriteLS (void);
void            DestroyKistBmp (void);
void            ReadKopftext (void);
int             ReadPr (void);
int             Reada_pr (void);
int		ReadWEBestell (int);
int		TestWE (void);
int DoScann (void);
void FillQsPosWieg (double a);  //220712
void FuelleAufpArr (void); //GROM-6

static int doetikett (void);
static int doetispez (void);
int doEtikettendruck (void); //FISCH-5
short MaxFortschritt (double a); //FISCH-5
BOOL UpdateFortschritt (int dfortschritt); //FISCH-5
void stringcopy(char * Ziel, const char * Quelle, int sizeZiel); 
void stringcopy(char * Ziel, const char * Quelle) ;
int  HoleZeilenAnzahl (char *string, int len); 
void EntferneZeilenende(char *string, int len); 
int ReadKun (void);
int ScanEan128Ai10 (Text& Ean128);
int EnterCharge (void); //FISCH-5
int EnterGewicht (void); //FISCH-5
int AuswahlFanggebiet (void);
int ScanProdStart (void);
BOOL SchreibeProdAbschluss (void);
double HoleProdartikel (double artikel);
int  HoleFanggebiet (char *charge);
int AuswahlFortschritt (void);
BOOL ChargeInListe (char *Charge);
int SelectPosTexte (void);
void PROD_MainButtons_inaktiv (void);



double HoleBsdMe (int mdn,double a, int lgr);
void BucheBsd (int faktor,double me, char *chargennr);
int             ShowPtBox (HWND, char *, int, char *, int, form *,
                           form *, form *);

static BMAP *FestTaraAn = NULL; //220712
static BMAP *ok = NULL; //220712
static BMAP *nok = NULL; //220712
static int FestAn[25] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}; //220712

int AuswahlWiegArt (void);
int AuswahlAbteilung (void);
int AuswahlWiePro (void);

int IsWiegMessage (MSG *);
void InitAufKopf (void);

static     void SelectAktCtr (void);
void            InitFirstInstance(HANDLE);
BOOL            InitNewInstance(HANDLE, int);
int             ProcessMessages(void);
LONG            MenuJob(HWND, WPARAM, LPARAM);
void            AuftragKopf (int);
void            LiefKopf (void);
void            EnterNumBox (HWND, char *, char *, int, char *);
void            EnterCalcBox (HWND, char *, char *, int, char *);
void            EnterListe (void);
void            ArtikelWiegen (HWND);
void            ArtikelHand (void);
void            WriteRind ();
void            WaageInit (void);
static int      DbAuswahl (short,void (*) (char *), void (*) (char *));
void            SetStorno (int (*) (void), int);
int             ShowFestTara (char *);
void             BelegeFestTara (void); //220712
int             ShowGebindeTara (char *);
int             ShowVerpackungTara (char *);
int             SetKomplett (void);
void            DisplayLiefKopf (void);
void            CloseLiefKopf (void);
void            LiefFocus (void);
int             print_messG (int, char *, ...);
void            disp_messG (char *, int);
int             abfragejnG (HWND, char *, char *);
int             AuswahlWieg (void);
void            fnbexec (int, char *[]);
BOOL            TestPosStatus (void);
BOOL            LsLeerGut (void);
static void     SetEti (double, double, double);
static void     SetEtiStk (double);
void            setautotara (BOOL);
void            EnterLeerPos (short);
void	 	    GetGebGewicht (char *,short);
int             deltaraw (void);
BOOL            TestQs (BOOL);
BOOL            TestQsPosA (double);
BOOL            TestQsPos (void);
void            WriteParamKiste (int);
void            WriteParamEZ (int);
void            WriteParamEV (int);
void            WriteParamArt (int);

void            EnterRCharge (int);
void            EnterREz (int);
void            EnterREs (int);
void            EnterLand (int);
int             ShowTierLand (char *);
void            BucheChargenBsd ();
void            SetScannMode ();
int 		    ScanEan128Charge (Text& Ean, BOOL Ident);
int             ChangeChar (int key);
void            DrawNoteIcon ();
void            DrawNoteIcon (HDC hdc);
void            SetListRowProc (void (*) (HDC, RECT *, int));
long            GenWepTxt (void);
void            DrawRowNoteIcon (HDC hdc, RECT *rect, int idx);
int             AuswahlTemperature (void);
void            DoChargeZwang (void);
void GenLief_rech_nr (short di);
static          int TestNewArt (double, long);
int				TestNewArtCharge (double a, Text& ListCharge);


int artikelOK (char *buffer, BOOL DispMode=TRUE);
int artikelOK (double a, BOOL DispMode=TRUE)
{
	char buffer [20];
	if (atoi (EinAusgang) == PRODUKTIONSFORTSCHRITT) return TRUE; 
	sprintf (buffer, "%.0lf", a);
	return artikelOK (buffer, DispMode);
}


int ScanA (void);
int ScanArtikel (double a);
int ScanEan128 (Text& Ean128, BOOL DispMode=TRUE);
char *GenCharge (void);


BOOL            Lock_Lsp (void);
static int      la_zahl = 0;
static int      AktKopf = 0;


static BOOL SmallScann = TRUE;
static SCANEAN Scanean ((Text) "scanean.cfg");
static long scangew = 0l;
CEan128Date Ean128Date;
static BOOL Scannen = FALSE;
static int Scanlen = 14;
static BOOL ScanMode = FALSE;
static BOOL BreakScan = FALSE;
static BOOL ScanAFromEan = TRUE;
static int Ean128Eanlen = 12;
static BOOL ChargeZwang = FALSE;
static BOOL ScanCharge = FALSE;
static BOOL MultiTemperature = TRUE;
static BOOL MesswerteZwang = FALSE;

//  Globale Variablen



ColButton BuInsert = {"Neu", -1, 5,
                       "F6", -1, 21,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       RGB (0, 255, 255),
                       BLUECOL,
                       FALSE};


ColButton BuPreise = { "Preise", -1, 5,
                      "F7", -1, 21,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       RGB (0, 255, 255),
                       BLUECOL,
                       FALSE};


ColButton BuDel   = {"l�schen", -1, 5,
                      "F9", -1, 21,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      RGB (0, 255, 255),
                      BLUECOL,
                      FALSE};

ColButton BuSys =   {"Ger�t", -1, 5,
                      "F11", -1, 21,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      RGB (0, 255, 255),
                      BLUECOL,
                      FALSE};

ColButton BuQsPos =   {"QS Pos", -1, 5,
                       "F2", -1, 21,
                        NULL, 0, 0,
                        NULL, 0, 0,
                        NULL, 0, 0,
                        RGB (0, 255, 255),
                        BLUECOL,
                        TRUE};

ColButton BuMess =   {"Messwerte", -1, 5,
                       "F3", -1, 21,
                        NULL, 0, 0,
                        NULL, 0, 0,
                        NULL, 0, 0,
                        RGB (0, 255, 255),
                        BLUECOL,
                        TRUE};
char *Wiegekopf = "Wiegekopf";
char *Scann     = "Scannen";


ColButton BuKopf =   {Wiegekopf, -1, 5,
                      "F10", -1, 21,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      RGB (0, 255, 255),
                      BLUECOL,
                      FALSE};

ColButton BuAusZ = {"HBK", -1, 5,
                     "F8", -1, 21,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
//                     BLACKCOL,
//                     YELLOWCOL,
                      RGB (0, 255, 255),
                      BLUECOL,
                     -1};









HWND ForegroundWindow;

void GetForeground (void)
{
	ForegroundWindow = GetForegroundWindow ();
}

void SetForeground (void)
{
	SetForegroundWindow (ForegroundWindow);
}


HWND     hMainWindow;
HWND     hFitWindow = NULL;
HWND     BuDialog1;

HWND     BackWindow1;
HWND     BackWindow2;
HWND     BackWindow3;

HWND     LogoWindow;

HWND     FitWindow = NULL;
HWND     UhrWindow = NULL;
HWND     MessWindow = NULL;
HANDLE   hMainInst;
static   HWND eWindow = NULL;
static   HWND InsWindow = NULL;
static   HWND PtWindow = NULL;
static   HWND eKopfWindow = NULL;
static   HWND eFussWindow = NULL;
static   HWND PtFussWindow = NULL;
static   HWND GebWindow = NULL;
static   HWND GebWindButton = NULL;

LPSTR    lpszMainTitle = "TOUCH Zerlegepartien ";
LPSTR    lpszClassName = "fit";
HBITMAP  fitbmp;
HBITMAP   pfeill;
HBITMAP   pfeilo;
HBITMAP   pfeilu;
HBITMAP   pfeilr;
HBITMAP   hbmOld;
HDC       hdc;
HDC       hdcMemory;
BITMAP    bm;
BITMAP    fitbm;
BITMAP    hfitbm;
HDC       fithdc;
int       procwait = 0;
int       IsFhs = 0;
BOOL      LockAuf = 1;
BOOL      AufLock = 0;
double scrfx, scrfy;
static int sysxplus = 50;
int Sebmpw;
int Sebmph;

/*
int lmx = 10;
int lmx2 = 0;
int lmy = 50;
int lmxs = 20;
int lmys = 0;
*/


//WA_PREISE WaPreis;

static BOOL KistPaint = FALSE;
static BMAP *Kist;
static BMAP *KistG;
HBITMAP KistBmp;
HBITMAP KistBmpG;
POINT kistpoint;
RECT KistRect;
BOOL KistCol = FALSE;

static BMAP *KistMask;
HBITMAP KistMaskBmp;

HBRUSH LtBlueBrush;
HBRUSH YellowBrush;



//220712 A
COLORREF AutoForgroundCol = RGB (255, 255, 255); 
COLORREF AutoBackgroundCol = RGB (0, 0, 255);
COLORREF FesttaraBackgroundCol = RGB (0, 0, 255); 
COLORREF FesttaraBackgroundColSelect = RGB (128, 64, 0); 

struct FT
{
        char ptbez [33];
        char ptbezk [9];
        char ptwer1 [9];
        char ptwer2 [9];
};

struct FT ftab, ftabarr [25];
static int AnzFestTara = 0;
static int AnzFestTaraLen = 0;



static int sorta (void);
static int sortb (void);
static int sortame (void);
static int sortlme (void);
static int sorts (void);



ColButton FestTara1 = {ftabarr[0].ptbez, 0, 15,
					   ftabarr[0].ptbezk, 0, 30,
						NULL, 0, 0,
						NULL, 0, -1,
						NULL, 0, 0,
						AutoForgroundCol,
                        FesttaraBackgroundCol,
						0};


ColButton FestTara2 = {ftabarr[1].ptbez, 0, 15,
					   ftabarr[1].ptbezk, 0, 40,
						NULL, 0, 0,
						NULL, 0, -1,
						NULL, 0, 0,
						AutoForgroundCol,
                        FesttaraBackgroundCol,
						0};


ColButton FestTara3 = {ftabarr[2].ptbez, 0, 15,
					   ftabarr[2].ptbezk, 0, 40,
						NULL, 0, 0,
						NULL, 0, -1,
						NULL, 0, 0,
						AutoForgroundCol,
                        FesttaraBackgroundCol,
						0};

ColButton FestTara4 = {ftabarr[3].ptbez, 0, 15,
					   ftabarr[3].ptbezk, 0, 40,
						NULL, 0, 0,
						NULL, 0, -1,
						NULL, 0, 0,
						AutoForgroundCol,
                        FesttaraBackgroundCol,
						0};

ColButton FestTara5 = {ftabarr[4].ptbez, 0, 15,
					   ftabarr[4].ptbezk, 0, 40,
						NULL, 0, 0,
						NULL, 0, -1,
						NULL, 0, 0,
						AutoForgroundCol,
                        FesttaraBackgroundCol,
						0};

ColButton FestTara6 = {ftabarr[5].ptbez, 0, 15,
					   ftabarr[5].ptbezk, 0, 40,
						NULL, 0, 0,
						NULL, 0, -1,
						NULL, 0, 0,
						AutoForgroundCol,
                        FesttaraBackgroundCol,
						0};

ColButton FestTara7 = {ftabarr[6].ptbez, 0, 15,
					   ftabarr[6].ptbezk, 0, 40,
						NULL, 0, 0,
						NULL, 0, -1,
						NULL, 0, 0,
						AutoForgroundCol,
                        FesttaraBackgroundCol,
						0};

ColButton FestTara8 = {ftabarr[7].ptbez, 0, 15,
					   ftabarr[7].ptbezk, 0, 40,
						NULL, 0, 0,
						NULL, 0, -1,
						NULL, 0, 0,
						AutoForgroundCol,
                        FesttaraBackgroundCol,
						0};

ColButton FestTara9 = {ftabarr[8].ptbez, 0, 15,
					   ftabarr[8].ptbezk, 0, 40,
						NULL, 0, 0,
						NULL, 0, -1,
						NULL, 0, 0,
						AutoForgroundCol,
                        FesttaraBackgroundCol,
						0};

ColButton FestTara10 = {ftabarr[9].ptbez, 0, 15,
					   ftabarr[9].ptbezk, 0, 40,
						NULL, 0, 0,
						NULL, 0, -1,
						NULL, 0, 0,
						AutoForgroundCol,
                        FesttaraBackgroundCol,
						0};

ColButton FestTara11 = {ftabarr[10].ptbez, 0, 15,
					   ftabarr[10].ptbezk, 0, 40,
						NULL, 0, 0,
						NULL, 0, -1,
						NULL, 0, 0,
						AutoForgroundCol,
                        FesttaraBackgroundCol,
						0};

ColButton FestTara12 = {ftabarr[11].ptbez, 0, 15,
					   ftabarr[11].ptbezk, 0, 40,
						NULL, 0, 0,
						NULL, 0, -1,
						NULL, 0, 0,
						AutoForgroundCol,
                        FesttaraBackgroundCol,
						0};

ColButton FestTara13 = {ftabarr[12].ptbez, 0, 15,
					   ftabarr[12].ptbezk, 0, 40,
						NULL, 0, 0,
						NULL, 0, -1,
						NULL, 0, 0,
						AutoForgroundCol,
                        FesttaraBackgroundCol,
						0};

ColButton FestTara14 = {ftabarr[13].ptbez, 0, 15,
					   ftabarr[13].ptbezk, 0, 40,
						NULL, 0, 0,
						NULL, 0, -1,
						NULL, 0, 0,
						AutoForgroundCol,
                        FesttaraBackgroundCol,
						0};
ColButton FestTara15 = {ftabarr[14].ptbez, 0, 15,
					   ftabarr[14].ptbezk, 0, 40,
						NULL, 0, 0,
						NULL, 0, -1,
						NULL, 0, 0,
						AutoForgroundCol,
                        FesttaraBackgroundCol,
						0};

ColButton FestTara16 = {ftabarr[15].ptbez, 0, 15,
					   ftabarr[15].ptbezk, 0, 40,
						NULL, 0, 0,
						NULL, 0, -1,
						NULL, 0, 0,
						AutoForgroundCol,
                        FesttaraBackgroundCol,
						0};
ColButton FestTara17 = {ftabarr[16].ptbez, 0, 15,
					   ftabarr[16].ptbezk, 0, 40,
						NULL, 0, 0,
						NULL, 0, -1,
						NULL, 0, 0,
						AutoForgroundCol,
                        FesttaraBackgroundCol,
						0};
ColButton FestTara18 = {ftabarr[17].ptbez, 0, 15,
					   ftabarr[17].ptbezk, 0, 40,
						NULL, 0, 0,
						NULL, 0, -1,
						NULL, 0, 0,
						AutoForgroundCol,
                        FesttaraBackgroundCol,
						0};
ColButton FestTara19 = {ftabarr[18].ptbez, 0, 15,
					   ftabarr[18].ptbezk, 0, 40,
						NULL, 0, 0,
						NULL, 0, -1,
						NULL, 0, 0,
						AutoForgroundCol,
                        FesttaraBackgroundCol,
						0};
ColButton FestTara20 = {ftabarr[19].ptbez, 0, 15,
					   ftabarr[19].ptbezk, 0, 40,
						NULL, 0, 0,
						NULL, 0, -1,
						NULL, 0, 0,
						AutoForgroundCol,
                        FesttaraBackgroundCol,
						0};
ColButton FestTara21 = {ftabarr[20].ptbez, 0, 15,
					   ftabarr[20].ptbezk, 0, 40,
						NULL, 0, 0,
						NULL, 0, -1,
						NULL, 0, 0,
						AutoForgroundCol,
                        FesttaraBackgroundCol,
						0};
ColButton FestTara22 = {ftabarr[21].ptbez, 0, 15,
					   ftabarr[21].ptbezk, 0, 40,
						NULL, 0, 0,
						NULL, 0, -1,
						NULL, 0, 0,
						AutoForgroundCol,
                        FesttaraBackgroundCol,
						0};
ColButton FestTara23 = {ftabarr[22].ptbez, 0, 15,
					   ftabarr[22].ptbezk, 0, 40,
						NULL, 0, 0,
						NULL, 0, -1,
						NULL, 0, 0,
						AutoForgroundCol,
                        FesttaraBackgroundCol,
						0};
ColButton FestTara24 = {ftabarr[23].ptbez, 0, 15,
					   ftabarr[23].ptbezk, 0, 40,
						NULL, 0, 0,
						NULL, 0, -1,
						NULL, 0, 0,
						AutoForgroundCol,
                        FesttaraBackgroundCol,
						0};
ColButton FestTara25 = {ftabarr[24].ptbez, 0, 15,
					   ftabarr[24].ptbezk, 0, 40,
						NULL, 0, 0,
						NULL, 0, -1,
						NULL, 0, 0,
						AutoForgroundCol,
                        FesttaraBackgroundCol,
						0};



mfont ListFont = {"Courier New", 200, 150, 1, RGB (0, 0, 0),
                                      RGB (255, 255, 255),
                                      0,
                                      NULL};
mfont InsFontT = {"Courier New", 110, 0, 1, BLACKCOL,
                                            LTGRAYCOL,
                                            1,
                                            NULL};

mfont InsFont = {"Courier New", 150, 0, 1, BLACKCOL,
                                           LTGRAYCOL,
                                           1,
                                           NULL};

mfont lKopfFont = {"Courier New", 150, 0, 1, BLACKCOL,
                                             LTGRAYCOL,
                                             1,
                                             NULL};

mfont lFussFont = {"", 90, 0, 1,       RGB (0, 255, 255),
                                       BLUECOL,
                                       1,
                                       NULL};



void SetFestBitmap (int i, HBITMAP hBitmap)
{
	if ( i == 0) FestTara1.bmp = hBitmap;
	if ( i == 1) FestTara2.bmp = hBitmap;
	if ( i == 2) FestTara3.bmp = hBitmap;
	if ( i == 3) FestTara4.bmp = hBitmap;
	if ( i == 4) FestTara5.bmp = hBitmap;
	if ( i == 5) FestTara6.bmp = hBitmap;
	if ( i == 6) FestTara7.bmp = hBitmap;
	if ( i == 7) FestTara8.bmp = hBitmap;
	if ( i == 8) FestTara9.bmp = hBitmap;
	if ( i == 9) FestTara10.bmp = hBitmap;
	if ( i == 10) FestTara11.bmp = hBitmap;
	if ( i == 11) FestTara12.bmp = hBitmap;
	if ( i == 12) FestTara13.bmp = hBitmap;
	if ( i == 13) FestTara14.bmp = hBitmap;
	if ( i == 14) FestTara15.bmp = hBitmap;
	if ( i == 15) FestTara16.bmp = hBitmap;
	if ( i == 16) FestTara17.bmp = hBitmap;
	if ( i == 17) FestTara18.bmp = hBitmap;
	if ( i == 18) FestTara19.bmp = hBitmap;
	if ( i == 19) FestTara20.bmp = hBitmap;
	if ( i == 20) FestTara21.bmp = hBitmap;
	if ( i == 21) FestTara22.bmp = hBitmap;
	if ( i == 22) FestTara23.bmp = hBitmap;
	if ( i == 23) FestTara24.bmp = hBitmap;
	if ( i == 24) FestTara25.bmp = hBitmap;
}

void SetFestAllBitmaps (void)
{

if (QsPosWieg == TRUE)
{
	if (FestAn[0] == 1) { FestTara1.bmp = ok->hBitmap; FestTara1.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[1] == 1) { FestTara2.bmp = ok->hBitmap; FestTara2.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[2] == 1) { FestTara3.bmp = ok->hBitmap; FestTara3.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[3] == 1) { FestTara4.bmp = ok->hBitmap; FestTara4.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[4] == 1) { FestTara5.bmp = ok->hBitmap; FestTara5.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[5] == 1) { FestTara6.bmp = ok->hBitmap; FestTara6.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[6] == 1) { FestTara7.bmp = ok->hBitmap; FestTara7.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[7] == 1) { FestTara8.bmp = ok->hBitmap; FestTara8.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[8] == 1) { FestTara9.bmp = ok->hBitmap; FestTara9.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[9] == 1) { FestTara10.bmp = ok->hBitmap; FestTara10.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[10] == 1) { FestTara11.bmp = ok->hBitmap; FestTara11.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[11] == 1) { FestTara12.bmp = ok->hBitmap; FestTara12.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[12] == 1) { FestTara13.bmp = ok->hBitmap; FestTara13.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[13] == 1) { FestTara14.bmp = ok->hBitmap; FestTara14.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[14] == 1) { FestTara15.bmp = ok->hBitmap; FestTara15.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[15] == 1) { FestTara16.bmp = ok->hBitmap; FestTara16.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[16] == 1) { FestTara17.bmp = ok->hBitmap; FestTara17.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[17] == 1) { FestTara18.bmp = ok->hBitmap; FestTara18.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[18] == 1) { FestTara19.bmp = ok->hBitmap; FestTara19.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[19] == 1) { FestTara20.bmp = ok->hBitmap; FestTara20.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[20] == 1) { FestTara21.bmp = ok->hBitmap; FestTara21.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[21] == 1) { FestTara22.bmp = ok->hBitmap; FestTara22.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[22] == 1) { FestTara23.bmp = ok->hBitmap; FestTara23.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[23] == 1) { FestTara24.bmp = ok->hBitmap; FestTara24.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[24] == 1) { FestTara25.bmp = ok->hBitmap; FestTara25.BkColor = FesttaraBackgroundColSelect; }

	if (FestAn[0] == 2) { FestTara1.bmp = nok->hBitmap; FestTara1.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[1] == 2) { FestTara2.bmp = nok->hBitmap; FestTara2.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[2] == 2) { FestTara3.bmp = nok->hBitmap; FestTara3.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[3] == 2) { FestTara4.bmp = nok->hBitmap; FestTara4.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[4] == 2) { FestTara5.bmp = nok->hBitmap; FestTara5.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[5] == 2) { FestTara6.bmp = nok->hBitmap; FestTara6.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[6] == 2) { FestTara7.bmp = nok->hBitmap; FestTara7.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[7] == 2) { FestTara8.bmp = nok->hBitmap; FestTara8.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[8] == 2) { FestTara9.bmp = nok->hBitmap; FestTara9.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[9] == 2) { FestTara10.bmp = nok->hBitmap; FestTara10.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[10] == 2) { FestTara11.bmp = nok->hBitmap; FestTara11.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[11] == 2) { FestTara12.bmp = nok->hBitmap; FestTara12.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[12] == 2) { FestTara13.bmp = nok->hBitmap; FestTara13.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[13] == 2) { FestTara14.bmp = nok->hBitmap; FestTara14.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[14] == 2) { FestTara15.bmp = nok->hBitmap; FestTara15.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[15] == 2) { FestTara16.bmp = nok->hBitmap; FestTara16.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[16] == 2) { FestTara17.bmp = nok->hBitmap; FestTara17.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[17] == 2) { FestTara18.bmp = nok->hBitmap; FestTara18.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[18] == 2) { FestTara19.bmp = nok->hBitmap; FestTara19.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[19] == 2) { FestTara20.bmp = nok->hBitmap; FestTara20.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[20] == 2) { FestTara21.bmp = nok->hBitmap; FestTara21.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[21] == 2) { FestTara22.bmp = nok->hBitmap; FestTara22.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[22] == 2) { FestTara23.bmp = nok->hBitmap; FestTara23.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[23] == 2) { FestTara24.bmp = nok->hBitmap; FestTara24.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[24] == 2) { FestTara25.bmp = nok->hBitmap; FestTara25.BkColor = FesttaraBackgroundColSelect; }



	if (FestAn[0] == 0) { FestTara1.bmp = NULL; FestTara1.BkColor = FesttaraBackgroundCol; }
	if (FestAn[1] == 0) { FestTara2.bmp = NULL; FestTara2.BkColor = FesttaraBackgroundCol; }
	if (FestAn[2] == 0) { FestTara3.bmp = NULL; FestTara3.BkColor = FesttaraBackgroundCol; }
	if (FestAn[3] == 0) { FestTara4.bmp = NULL; FestTara4.BkColor = FesttaraBackgroundCol; }
	if (FestAn[4] == 0) { FestTara5.bmp = NULL; FestTara5.BkColor = FesttaraBackgroundCol; }
	if (FestAn[5] == 0) { FestTara6.bmp = NULL; FestTara6.BkColor = FesttaraBackgroundCol; }
	if (FestAn[6] == 0) { FestTara7.bmp = NULL; FestTara7.BkColor = FesttaraBackgroundCol; }
	if (FestAn[7] == 0) { FestTara8.bmp = NULL; FestTara8.BkColor = FesttaraBackgroundCol; }
	if (FestAn[8] == 0) { FestTara9.bmp = NULL; FestTara9.BkColor = FesttaraBackgroundCol; }
	if (FestAn[9] == 0) { FestTara10.bmp = NULL; FestTara10.BkColor = FesttaraBackgroundCol; }
	if (FestAn[10] == 0) { FestTara11.bmp = NULL; FestTara11.BkColor = FesttaraBackgroundCol; }
	if (FestAn[11] == 0) { FestTara12.bmp = NULL; FestTara12.BkColor = FesttaraBackgroundCol; }
	if (FestAn[12] == 0) { FestTara13.bmp = NULL; FestTara13.BkColor = FesttaraBackgroundCol; }
	if (FestAn[13] == 0) { FestTara14.bmp = NULL; FestTara14.BkColor = FesttaraBackgroundCol; }
	if (FestAn[14] == 0) { FestTara15.bmp = NULL; FestTara15.BkColor = FesttaraBackgroundCol; }
	if (FestAn[15] == 0) { FestTara16.bmp = NULL; FestTara16.BkColor = FesttaraBackgroundCol; }
	if (FestAn[16] == 0) { FestTara17.bmp = NULL; FestTara17.BkColor = FesttaraBackgroundCol; }
	if (FestAn[17] == 0) { FestTara18.bmp = NULL; FestTara18.BkColor = FesttaraBackgroundCol; }
	if (FestAn[18] == 0) { FestTara19.bmp = NULL; FestTara19.BkColor = FesttaraBackgroundCol; }
	if (FestAn[19] == 0) { FestTara20.bmp = NULL; FestTara20.BkColor = FesttaraBackgroundCol; }
	if (FestAn[20] == 0) { FestTara21.bmp = NULL; FestTara21.BkColor = FesttaraBackgroundCol; }
	if (FestAn[21] == 0) { FestTara22.bmp = NULL; FestTara22.BkColor = FesttaraBackgroundCol; }
	if (FestAn[22] == 0) { FestTara23.bmp = NULL; FestTara23.BkColor = FesttaraBackgroundCol; }
	if (FestAn[23] == 0) { FestTara24.bmp = NULL; FestTara24.BkColor = FesttaraBackgroundCol; }
	if (FestAn[24] == 0) { FestTara25.bmp = NULL; FestTara25.BkColor = FesttaraBackgroundCol; }
}
else
{

	if (FestAn[0] == 1) { FestTara1.bmp = FestTaraAn->hBitmap; FestTara1.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[1] == 1) { FestTara2.bmp = FestTaraAn->hBitmap; FestTara2.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[2] == 1) { FestTara3.bmp = FestTaraAn->hBitmap; FestTara3.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[3] == 1) { FestTara4.bmp = FestTaraAn->hBitmap; FestTara4.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[4] == 1) { FestTara5.bmp = FestTaraAn->hBitmap; FestTara5.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[5] == 1) { FestTara6.bmp = FestTaraAn->hBitmap; FestTara6.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[6] == 1) { FestTara7.bmp = FestTaraAn->hBitmap; FestTara7.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[7] == 1) { FestTara8.bmp = FestTaraAn->hBitmap; FestTara8.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[8] == 1) { FestTara9.bmp = FestTaraAn->hBitmap; FestTara9.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[9] == 1) { FestTara10.bmp = FestTaraAn->hBitmap; FestTara10.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[10] == 1) { FestTara11.bmp = FestTaraAn->hBitmap; FestTara11.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[11] == 1) { FestTara12.bmp = FestTaraAn->hBitmap; FestTara12.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[12] == 1) { FestTara13.bmp = FestTaraAn->hBitmap; FestTara13.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[13] == 1) { FestTara14.bmp = FestTaraAn->hBitmap; FestTara14.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[14] == 1) { FestTara15.bmp = FestTaraAn->hBitmap; FestTara15.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[15] == 1) { FestTara16.bmp = FestTaraAn->hBitmap; FestTara16.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[16] == 1) { FestTara17.bmp = FestTaraAn->hBitmap; FestTara17.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[17] == 1) { FestTara18.bmp = FestTaraAn->hBitmap; FestTara18.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[18] == 1) { FestTara19.bmp = FestTaraAn->hBitmap; FestTara19.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[19] == 1) { FestTara20.bmp = FestTaraAn->hBitmap; FestTara20.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[20] == 1) { FestTara21.bmp = FestTaraAn->hBitmap; FestTara21.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[21] == 1) { FestTara22.bmp = FestTaraAn->hBitmap; FestTara22.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[22] == 1) { FestTara23.bmp = FestTaraAn->hBitmap; FestTara23.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[23] == 1) { FestTara24.bmp = FestTaraAn->hBitmap; FestTara24.BkColor = FesttaraBackgroundColSelect; }
	if (FestAn[24] == 1) { FestTara25.bmp = FestTaraAn->hBitmap; FestTara25.BkColor = FesttaraBackgroundColSelect; }




	if (FestAn[0] == 0) { FestTara1.bmp = NULL; FestTara1.BkColor = FesttaraBackgroundCol; }
	if (FestAn[1] == 0) { FestTara2.bmp = NULL; FestTara2.BkColor = FesttaraBackgroundCol; }
	if (FestAn[2] == 0) { FestTara3.bmp = NULL; FestTara3.BkColor = FesttaraBackgroundCol; }
	if (FestAn[3] == 0) { FestTara4.bmp = NULL; FestTara4.BkColor = FesttaraBackgroundCol; }
	if (FestAn[4] == 0) { FestTara5.bmp = NULL; FestTara5.BkColor = FesttaraBackgroundCol; }
	if (FestAn[5] == 0) { FestTara6.bmp = NULL; FestTara6.BkColor = FesttaraBackgroundCol; }
	if (FestAn[6] == 0) { FestTara7.bmp = NULL; FestTara7.BkColor = FesttaraBackgroundCol; }
	if (FestAn[7] == 0) { FestTara8.bmp = NULL; FestTara8.BkColor = FesttaraBackgroundCol; }
	if (FestAn[8] == 0) { FestTara9.bmp = NULL; FestTara9.BkColor = FesttaraBackgroundCol; }
	if (FestAn[9] == 0) { FestTara10.bmp = NULL; FestTara10.BkColor = FesttaraBackgroundCol; }
	if (FestAn[10] == 0) { FestTara11.bmp = NULL; FestTara11.BkColor = FesttaraBackgroundCol; }
	if (FestAn[11] == 0) { FestTara12.bmp = NULL; FestTara12.BkColor = FesttaraBackgroundCol; }
	if (FestAn[12] == 0) { FestTara13.bmp = NULL; FestTara13.BkColor = FesttaraBackgroundCol; }
	if (FestAn[13] == 0) { FestTara14.bmp = NULL; FestTara14.BkColor = FesttaraBackgroundCol; }
	if (FestAn[14] == 0) { FestTara15.bmp = NULL; FestTara15.BkColor = FesttaraBackgroundCol; }
	if (FestAn[15] == 0) { FestTara16.bmp = NULL; FestTara16.BkColor = FesttaraBackgroundCol; }
	if (FestAn[16] == 0) { FestTara17.bmp = NULL; FestTara17.BkColor = FesttaraBackgroundCol; }
	if (FestAn[17] == 0) { FestTara18.bmp = NULL; FestTara18.BkColor = FesttaraBackgroundCol; }
	if (FestAn[18] == 0) { FestTara19.bmp = NULL; FestTara19.BkColor = FesttaraBackgroundCol; }
	if (FestAn[19] == 0) { FestTara20.bmp = NULL; FestTara20.BkColor = FesttaraBackgroundCol; }
	if (FestAn[20] == 0) { FestTara21.bmp = NULL; FestTara21.BkColor = FesttaraBackgroundCol; }
	if (FestAn[21] == 0) { FestTara22.bmp = NULL; FestTara22.BkColor = FesttaraBackgroundCol; }
	if (FestAn[22] == 0) { FestTara23.bmp = NULL; FestTara23.BkColor = FesttaraBackgroundCol; }
	if (FestAn[23] == 0) { FestTara24.bmp = NULL; FestTara24.BkColor = FesttaraBackgroundCol; }
	if (FestAn[24] == 0) { FestTara25.bmp = NULL; FestTara25.BkColor = FesttaraBackgroundCol; }
}

}
//220712 E



static char waadir [128] = {"C:\\waakt"};
static char ergebnis [128] = {"C:\\waakt\\ergebnis"};
static int WaaInit = 0;
static HANDLE waaLibrary = NULL;
static char *fnbname = "fnb";
static char *fnbInstancename = "setzefnbInstance";

int (*fnbproc) (int, char **);
int (*fnbInstanceproc) (void *);
static BOOL neuer_we;

static DWORD GebColor = RGB (0, 0, 255);



/* Globale Parameter fuer printlogo                   */

static long twait = 50;
static long twaitU = 500;
static int plus =  3;
static int xplus = 3;
static int ystretch = 0;
static int xstretch = 0;
static int logory = 0;
static int logorx = 1;
static int sign = 1;
static int signx = 1;

static short akt_mdn;
static short akt_fil;

static char fitpath [256];
static char rosipath [256];
static char progname [256];

struct MENUE scmenue;
struct MENUE leermenue;

static int ListeAktiv = 0;
static int PtListeAktiv = 0;
static int NumEnterAktiv = 0;
static int PreisAktiv = 0;
static int KopfEnterAktiv = 0;
static int WiegenAktiv = 0;
static int LiefEnterAktiv = 0;
static int KomplettAktiv = 0;
static int WorkModus = WIEGEN;
static HWND NumEnterWindow;
static HWND AufKopfWindow;
static HWND WiegWindow;

static int CheckHbk ();

static int dsqlstatus;

CDllPreise DllPreise;
PRDK_K_CLASS Prdk_k;
class LSPT_CLASS lspt_class;
class ATEXTE_CLASS atexte_class;
static QSWEPOS_CLASS QsWePos;
QSTXT_CLASS QsTxtClass;
ZERL_K_CLASS ZerlkClass;
ZERL_P_CLASS ZerlpClass;
A_TARA_CLASS a_taraClass;
BEST_KOPF_CLASS BestKopfClass;
BEST_POS_CLASS BestPosClass;
LIEF_CLASS LiefClass;
class LGR_CLASS lgr_class;
class ZER_PARTK_CLASS zer_partk_class;
class ZER_PARTP_CLASS zer_partp_class;
class A_LGR_CLASS a_lgr_class;

class LS_CLASS ls_class;
//class LSPT_CLASS lspt_class;
// class KUN_CLASS kun_class;
static int      enterpass = 0;
static TEXTBLOCK *textblock = NULL;
class FIL_CLASS fil_class;
class MDN_CLASS mdn_class;
class ADR_CLASS adr_class;
class PTAB_CLASS ptab_class;
class GEBINDETARA_CLASS gebindetara_class;
class MENUE_CLASS menue_class;
//class EINH_CLASS einh_class;
class HNDW_CLASS hndw_class;
class GEB_CLASS geb_class;
class SYS_PERI_CLASS sys_peri_class;
class PHT_WG_CLASS pht_wg_class;
class SYS_PAR_CLASS sys_par_class;
static DB_CLASS DbClass;
static PROG_CFG ProgCfg ("26310");
static BMAP bMap;
static AutoNrClass AutoNr;
static WE_WIEPRO_CLASS WeWiePro;
static LIEF_BZG_CLASS LiefBzg;
static NUBLOCK NuBlock;
static TOUCHF Touchf;
static QMKOPF *Qmkopf;
static RIND_CLASS Rind;
static ZERLDATEN_CLASS Zerldaten;
static CVector Chargen;
static BSD_BUCH_CLASS BsdBuch;
static WE_TXT_CLASS cWept;

static int tara_kz;
static int mhd_kz = 2;

HICON NoteIcon = NULL;
BOOL DrawNote = FALSE;

// HMENU hMenu;
static char *mentab[] = {scmenue.menue1,
                         scmenue.menue2,
                         scmenue.menue3,
                         scmenue.menue4,
                         scmenue.menue5,
                         scmenue.menue6,
                         scmenue.menue7,
                         scmenue.menue8,
                         scmenue.menue9,
                         scmenue.menue0};
static char *zeitab [] = {scmenue.zei1,
                          scmenue.zei2,
                          scmenue.zei3,
                          scmenue.zei4,
                          scmenue.zei5,
                          scmenue.zei6,
                          scmenue.zei7,
                          scmenue.zei8,
                          scmenue.zei9,
                          scmenue.zei0};

static char *mentab0[] = {menue.menue1,
                          menue.menue2,
                          menue.menue3,
                          menue.menue4,
                          menue.menue5,
                          menue.menue6,
                          menue.menue7,
                          menue.menue8,
                          menue.menue9,
                          menue.menue0};
static char *zeitab0[] = {menue.zei1,
                          menue.zei2,
                          menue.zei3,
                          menue.zei4,
                          menue.zei5,
                          menue.zei6,
                          menue.zei7,
                          menue.zei8,
                          menue.zei9,
                          menue.zei0};


static HMENU hMenu;

/* Masken fuer Logofenster                                 */

mfont fittextlogo = {"Arial", 1000, 0, 3, RGB (255, 0, 0),
                                               RGB (0, 255, 255),
                                               1,
                                               NULL};

mfont fittextlogo2 = {"Arial", 200, 0, 3, RGB (0, 0, 255),
                                      RGB (0, 255, 255),
                                      1,
                                      NULL};

mfont buttonfont = {"", 90, 0, 1,
                                       RGB (0, 255, 255),
                                       BLUECOL,
                                       1,
                                       NULL};

mfont uhrfont     = {"Arial", 80, 0, 1,
                                       BLACKCOL,
                                       LTGRAYCOL,
                                       1,
                                       NULL};

field _fittext[] = {"     f i t", 0, 0, 50, -1, 0, "", DISPLAYONLY, 0, 0, 0};

form fittext = {1, 0, 0, _fittext, 0, 0, 0, 0, &fittextlogo};

field _fittext2[] = {"in Frische und Fleisch", 0, 0, 250, -1, 0, "",
                       DISPLAYONLY, 0, 0, 0};

form fittext2 = {1, 0, 0, _fittext2, 0, 0, 0, 0, &fittextlogo2};

static int menu_ebene = 0;
static int ohne_preis = 0;
static int DruckAuswahl = 1;
static int immer_preis = 0;
static int ls_leer_zwang = 0;

struct FITMENU
{
           char menutext[80];
           char menuprog[256];
           int progtyp;
           int start_mode;
};

struct FITMENU fitmenu [11];

static char auftrag_nr [22];
static char bestell_nr [22];
static char lief_nr [22];
static char kun_nr [22];
static char kunde_nr [22];
static int default_kunde ;
static char lieferant [22];
static char kun_krz1 [17];
static char lief_krz1 [17];
static char bestellhinweis [60];
static char lieferdatum  [22];
static char fanggebiet  [40];
static char scanprodstart  [40];
static char artikel  [40];
static char artikelbez  [40];
static char beldatum  [22];
static char komm_dat [22];
static char lieferzeit   [22];
static char ls_stat [22];
static char ls_stat_txt [27];
static char LAN [22];

static int menfunc1 (void);
static int menfunc2 (void);
static int menfunc3 (void);
static int menfunc4 (void);
static int menfunc5 (void);
static int menfunc6 (void);
static int menfunc7 (void);
static int menfunc8 (void);
static int menfunc9 (void);
static int menfunc10 (void);
static int menfunc0 (void);
static int menfuncA_LgrAb (void); //11
static int menfuncA_LgrZu (void); //12
static int menfuncLgrAb (void);    //13
static int menfuncLgrZu (void);   //14
static int bin_in_menfunc = 0;

static int func1 (void);
static int func2 (void);
static int func3 (void);
static int func4 (void);
static int func5 (void);
static int func6 (void);
static int func7 (void);
static int func8 (void);


static short Mandant = 1;
static int Kunde = 1031;
static short Filiale = 0;

static char *Mtxt = "Mandant";
static char *Ftxt = "Filiale";

static char Mandanttxt[20];
static char EinAusgangtxt[20];
static char LagertxtAb[30];
static char LagertxtZu[30];
static char LagertxtAb0[30];
static char LagertxtZu0[30];
static char Buchartikeltxt[30];
static char Filialetxt[20];


ColButton Ende = {"Ende", -1, -1,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   RGB (0, 255, 255),
                   BLUECOL,
                   TRUE};


ColButton Auftrag  =   {"&Auftrag-Nr", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         -1};

ColButton Zerlegepartie  =   {Einstiegtxt, -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               RGB (0, 255, 255),
                               BLUECOL,
                               TRUE};

ColButton Liste    = {
                        "L&iste", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         -1};

ColButton Suchen   =  {"&Suchen", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                          -1};



ColButton Partieauswahl   =  {Auswahltxt, -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         -1};


ColButton QmKopf   =  {"&QM Kopf", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         -1};


ColButton Geraet = {
                        "&Ger�t", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton WiegArt = {
                        "&Wiegart", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};
ColButton ColEinAusgang = {
                        EinAusgangtxt, -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         REDCOL,
                         TRUE};

						/**
ColButton ColLager = {
                        Lagertxt, -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};
						 **/


ColButton BLagerZu = {
                         LagerZutxt, -1, 5,
                         LgrZu_krz, -1, 25,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};
ColButton BLagerAb = {
                         LagerAbtxt, -1, 5,
                         LgrAb_krz, -1, 25,
                         NULL, 0, 0,
                         NULL, 0, 0,

                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};


ColButton CMandant = {
                         Mandanttxt, -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton CFiliale = {
                         Filialetxt, -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton KDatum    =  {"&Komm-Datum", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         -1};

ColButton Vorratsauftrag =  {
                        "Vor.Auftrag", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         -1};

ColButton BestInv =  {
                        "&Bestand, Inventur", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton Auswertungen =  {
                        "Auswer&tungen", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton Anwendungen =  {
                        "Anwen&dungen", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton Systemfunktionen =  {
                        "Systemf&unktionen", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton Benutzer =  {
                        "Ben&utzer", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};



ColButton EtikettKopf =  {
//                        "Hilfe &?", -1, -1,
//                        "Startwiegung", -1, -1,
                        "&Etikett", -1, -1,     //FISCH-5
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         -1};


ColButton Hauptmenue =  {
                        "Hauptmen&�", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton Back  =  {
                        Zuruecktext, -1, -1,
//                        "Zur�ck F5", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton BuOK = {"OK", -1, -1,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   RGB (0, 255, 255),
                   BLUECOL,
                   -1};

ColButton BuOKS = {"OK", -1, -1,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   RGB (0, 255, 255),
                   BLUECOL,
                   10};

ColButton BuKomplett =
                  {"komplett", -1, -1,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   RGB (0, 255, 255),
                   REDCOL,
                   -1};


ColButton PfeilL       =  {
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         pfeill, -1, -1,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         10};

ColButton PfeilO     =  {
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         pfeilo, -1, -1,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         10};

ColButton PfeilU     =  {
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         pfeilu, -1, -1,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         10};

ColButton PfeilR     =  {
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         pfeilr, -1, -1,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         10};


static int bsb  = 85;

static int bss0 = 73;
static int bss  = 77;
static int bsz  = 40;
static int bsz0 = 36;
static int AktButton = 0;


field _MainButton0[] = {
(char *) &Zerlegepartie,     128, bsz0, 115, -1, 0, "", COLBUTTON, 0,
                                                     menfunc3, 0,
(char *) &Liste,            128, bsz0, 115 + 1 * bsz, -1, 0, "", COLBUTTON, 0,
                                                     menfunc2, 0,
(char *) &Partieauswahl,       128, bsz0, 115 + 2 * bsz, -1, 0, "", COLBUTTON, 0,
                                                     menfunc8, 0,
(char *) &Geraet,           128, bsz0, 115 + 3 * bsz, -1, 0, "", COLBUTTON, 0,
                                                     menfunc6, 0,
//(char *) &QmKopf,           128, bsz0, 115 + 4 * bsz, -1, 0, "", COLBUTTON, 0,
//                                                     menfunc7, 0,
//(char *) &WiegArt,          128, bsz0, 115 + 4 * bsz, -1, 0, "", COLBUTTON, 0,
//                                                     menfunc9, 0,
(char *) &ColEinAusgang,        128, bsz0, 115 + 5 * bsz, -1, 0, "", COLBUTTON, 0,
                                                     menfunc10, 0,
(char *) &CMandant,         128, bsz0, 115 + 6 * bsz, -1, 0, "", COLBUTTON, 0,
                                                     menfunc4, 0,
(char *) &BLagerZu,        128, bsz0, 115 + 7 * bsz, -1, 0, "", COLBUTTON, 0,
                                                     menfuncLgrZu, 0,
(char *) &BLagerAb,        128, bsz0, 115 + 8 * bsz, -1, 0, "", COLBUTTON, 0,
                                                     menfuncLgrAb, 0,
(char *) &Ende,             128, bsz0, 130 + 9 * bsz,  -1, 0, "", COLBUTTON, 0,
                                                          menfunc0, 0};
form MainButton = {9, 0, 0, _MainButton0, 0, 0, 0, 0, &buttonfont};


field _MainButton2[] = {
(char *) &EtikettKopf,             bss0, bsz0, 5, 4+7*bss, 0, "", COLBUTTON, 0,
                                                              //func7, 0,
                                                              doEtikettendruck, 0,
(char *) &Back,              bss0, bsz0, 5, 4+6*bss, 0, "", COLBUTTON, 0,
 *                                                              func5, 0,
(char *) &BuOK,              bss0, bsz0, 5, 4+5*bss, 0, "", COLBUTTON, 0,
 *                                                              func6, 0,
//(char *) &BuKomplett,        bss0, bsz0, 5, 4+4*bss, 0, "", COLBUTTON, 0,
 //*                                                              func8, 0,
(char *) &PfeilR,            bss0, bsz0, 5, 4+3*bss, 0, "", COLBUTTON, 0,
                                                          func4, 0,
(char *) &PfeilU,            bss0, bsz0, 5, 4+2*bss, 0, "", COLBUTTON, 0,
                                                          func3, 0,
(char *) &PfeilO,            bss0, bsz0, 5, 4+1*bss, 0, "", COLBUTTON, 0,
                                                          func2, 0,
(char *) &PfeilL,            bss0,  bsz0,5, 4,     0, "", COLBUTTON, 0,
                                                          func1, 0};

form MainButton2 = {7, 0, 0, _MainButton2, 0, 0, 0, 0, &buttonfont};

static char datum [12];
static char datumzeit [24];

/*
static field _uhrform [] = {
datum,            0, 0,  5, -1, 0, "", DISPLAYONLY, 0, 0, 0,
zeit,             0, 0, 20, -1, 0, "", DISPLAYONLY, 0, 0, 0};
*/

static field _uhrform [] = {
datumzeit,        0, 0,  -1, -1, 0, "", DISPLAYONLY, 0, 0, 0};

static form uhrform = {1, 0, 0, _uhrform, 0, 0, 0, 0, &uhrfont};


struct AUFP_S
{
        char pnr [5];
        char s [2];
        char a [14];
        char a_best [17];
        char a_bz1 [25];
        char a_bz2 [25];
        char a_me_einh [5];
        char auf_me [13];
        char lief_me [13];
        char zerlein_me [13];
        char zerlaus_me [13];
        char auf_me_vgl [13];
        char tara [13];
        char auf_me_bz [10];
        char lief_me_bz [10];
        char auf_pr_vk [13];
        char auf_lad_pr [13];
        char basis_me_bz [7];
        char erf_kz [2];
        char rest [13];
        char ls_charge [21];
        char lsp_txt [10];
        char me_einh_kun [5];
        char me_einh [5];
        char hbk_date [11];
        char ls_vk_euro [13];
        char ls_vk_fremd [13];
        char ls_lad_euro [13];
        char ls_lad_fremd [13];
        char rab_satz [10];
		char hnd_gew [2];
		char erf_gew_kz [2];
		char anz_einh [13];
		char inh [13];
		char temp [10];
		char temp2 [10];
		char temp3 [10];
		char ph_wert [10];
        char pr_fil_ek [10];
        char pr_fil_ek_eu [10];
		int  me_kz;
		double min_best;
		char ident_nr [22];
		char ez_nr [22];
		char es_nr [22];
		char laender [22];
		char txtschwundabzug [30];   
		int lgr_zu;
		int lgr_ab;
		char bsd_me[13];
		short kng;
		char fortschritt [3] ;
		char fortschritt_bz [40];
		double pcharge;
		short status;
		char partie[14];
        char buch_gew [13];
		char ls_pos_kz [3];
		char posi_ext [9];
        short na_lief_kz;
		short pos_txt_kz;


};

struct AUFP_S aufps, aufp_null;
struct AUFP_S aufparr [AUFMAX];
int aufpanz = 0;
int aufpidx = 0;
int aufpanz_upd = 0;
static char auf_me [12];





static field _testform [] =
{
        aufps.a,           11, 1, 0, 0, 0, "%11.0f", EDIT, 0, 0, 0,
        aufps.a_bz1,       20, 1, 0, 13,0, "",       READONLY, 0, 0, 0,
        aufps.bsd_me,       6, 1, 0, 35, 0, "%9.3f", EDIT, 0, 0, 0,
		aufps.auf_me_bz,    2, 1, 0, 42, 0, "",      READONLY, 0, 0, 0,
        aufps.lief_me,      9, 1, 0, 46, 0, "%9.3f", READONLY , 0, 0, 0,
        aufps.s,            3, 1, 0, 57, 0, "",      READONLY, 0, 0, 0,
};


static form testform = { 6, 0, 4,_testform, 0, CheckHbk, 0, 0, &ListFont};


static field _testub [] = {
"Artikel-Nr",           12, 1, 0, 0, 0, "", BUTTON, 0, sorta,  106,
"Bezeichnung",          22, 1, 0,12, 0, "", BUTTON, 0, sortb,  102,
"Im Lager",             11, 1, 0,34, 0, "", BUTTON, 0, sortame,103,
"Menge",                11, 1, 0,45, 0, "", BUTTON, 0, sortlme,104,
"S",                     5, 1, 0,56, 0, "", BUTTON, 0, sorts,  105
};

static form testub = { 5, 0, 0,_testub, 0, 0, 0, 0, &ListFont};

static field _testvl [] =
{      "1",                  1,  0, 0, 12, 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 34 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 45 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 56 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 60 , 0, "", NORMAL, 0, 0, 0
};

static form testvl = {5, 0, 0,_testvl, 0, 0, 0, 0, &ListFont};

//==========================  ListForm Zerlegung Gesamt  =====================
static field _zerform [] =
{
        aufps.a,           11, 1, 0, 0, 0, "%11.0f", EDIT, 0, 0, 0,
        aufps.a_bz1,       18, 1, 0, 13,0, "",       READONLY, 0, 0, 0,
        aufps.bsd_me,       6, 1, 0, 33, 0, "%7.3f", EDIT, 0, 0, 0,
		aufps.auf_me_bz,    2, 1, 0, 40, 0, "",      READONLY, 0, 0, 0,
        aufps.zerlein_me,   7, 1, 0, 45, 0, "",      READONLY , 0, 0, 0,
        aufps.zerlaus_me,   8, 1, 0, 54, 0, "",      READONLY , 0, 0, 0,
        aufps.s,            2, 1, 0, 63, 0, "",      READONLY, 0, 0, 0,
};


static form zerform = { 7, 0, 4,_zerform, 0, CheckHbk, 0, 0, &ListFont};


static field _zerub [] = {
"Artikel-Nr",           12, 1, 0, 0, 0, "", BUTTON, 0, sorta,  106,
"Bezeichnung",          20, 1, 0,12, 0, "", BUTTON, 0, sortb,  102,
"Im Lager",             11, 1, 0,32, 0, "", BUTTON, 0, sortame,103,
"Entnommen",            10, 1, 0,43, 0, "", BUTTON, 0, sortlme,104,
"Zerlegt",               9, 1, 0,53, 0, "", BUTTON, 0, sortlme,104,
"S",                     4, 1, 0,62, 0, "", BUTTON, 0, sorts,  105
};

static form zerub = { 6, 0, 0,_zerub, 0, 0, 0, 0, &ListFont};

static field _zervl [] =
{      "1",                  1,  0, 0, 12, 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 32 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 43 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 53 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 62 , 0, "", NORMAL, 0, 0, 0
};

static form zervl = {5, 0, 0,_zervl, 0, 0, 0, 0, &ListFont};


//==========================  ListForm ZerlegeEingang   =============================
static field _zereinform [] =
{
        aufps.a,           11, 1, 0, 0, 0, "%11.0f", EDIT, 0, 0, 0,
        aufps.a_bz1,       20, 1, 0, 13,0, "",       READONLY, 0, 0, 0,
        aufps.bsd_me,       6, 1, 0, 35, 0, "%9.3f", EDIT, 0, 0, 0,
		aufps.auf_me_bz,    2, 1, 0, 42, 0, "",      READONLY, 0, 0, 0,
        aufps.lief_me,      9, 1, 0, 46, 0, "%9.3f", READONLY , 0, 0, 0,
        aufps.s,            3, 1, 0, 57, 0, "",      READONLY, 0, 0, 0,
};


static form zereinform = { 6, 0, 4,_zereinform, 0, CheckHbk, 0, 0, &ListFont};


static field _zereinub [] = {
"Artikel-Nr",           12, 1, 0, 0, 0, "", BUTTON, 0, sorta,  106,
"Bezeichnung",          22, 1, 0,12, 0, "", BUTTON, 0, sortb,  102,
"Im Lager0",            11, 1, 0,34, 0, "", BUTTON, 0, sortame,103,
"Menge",                11, 1, 0,45, 0, "", BUTTON, 0, sortlme,104,
"S",                     5, 1, 0,56, 0, "", BUTTON, 0, sorts,  105
};

static form zereinub = { 5, 0, 0,_zereinub, 0, 0, 0, 0, &ListFont};

static field _zereinvl [] =
{      "1",                  1,  0, 0, 12, 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 34 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 45 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 56 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 60 , 0, "", NORMAL, 0, 0, 0
};

static form zereinvl = {5, 0, 0,_zereinvl, 0, 0, 0, 0, &ListFont};


//==========================  ListForm ZerlegeAusgang   =============================
static field _zerausform [] =
{
        aufps.a,           11, 1, 0, 0, 0, "%11.0f", EDIT, 0, 0, 0,
        aufps.a_bz1,       20, 1, 0, 13,0, "",       READONLY, 0, 0, 0,
        aufps.bsd_me,       6, 1, 0, 35, 0, "%9.3f", EDIT, 0, 0, 0,
		aufps.auf_me_bz,    2, 1, 0, 42, 0, "",      READONLY, 0, 0, 0,
        aufps.lief_me,      9, 1, 0, 46, 0, "%9.3f", READONLY , 0, 0, 0,
        aufps.s,            3, 1, 0, 57, 0, "",      READONLY, 0, 0, 0,
};


static form zerausform = { 6, 0, 4,_zerausform, 0, CheckHbk, 0, 0, &ListFont};


static field _zerausub [] = {
"Artikel-Nr",           12, 1, 0, 0, 0, "", BUTTON, 0, sorta,  106,
"Bezeichnung",          22, 1, 0,12, 0, "", BUTTON, 0, sortb,  102,
"Im Lager1",            11, 1, 0,34, 0, "", BUTTON, 0, sortame,103,
"Menge",                11, 1, 0,45, 0, "", BUTTON, 0, sortlme,104,
"S",                     5, 1, 0,56, 0, "", BUTTON, 0, sorts,  105
};

static form zerausub = { 5, 0, 0,_zerausub, 0, 0, 0, 0, &ListFont};

static field _zerausvl [] =
{      "1",                  1,  0, 0, 12, 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 34 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 45 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 56 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 60 , 0, "", NORMAL, 0, 0, 0
};

static form zerausvl = {5, 0, 0,_zerausvl, 0, 0, 0, 0, &ListFont};


//==========================  ListForm Umlagerung   =============================
static field _umlform [] =
{
        aufps.a,           11, 1, 0, 0, 0, "%11.0f", EDIT, 0, 0, 0,
        aufps.a_bz1,       20, 1, 0, 13,0, "",       READONLY, 0, 0, 0,
        aufps.bsd_me,       6, 1, 0, 35, 0, "%9.3f", EDIT, 0, 0, 0,
		aufps.auf_me_bz,    2, 1, 0, 42, 0, "",      READONLY, 0, 0, 0,
        aufps.lief_me,      9, 1, 0, 46, 0, "%9.3f", READONLY , 0, 0, 0,
        aufps.s,            3, 1, 0, 57, 0, "",      READONLY, 0, 0, 0,
};


static form umlform = { 6, 0, 4,_umlform, 0, CheckHbk, 0, 0, &ListFont};


static field _umlub [] = {
"Artikel-Nr",           12, 1, 0, 0, 0, "", BUTTON, 0, sorta,  106,
"Bezeichnung",          22, 1, 0,12, 0, "", BUTTON, 0, sortb,  102,
"Im Lager",             11, 1, 0,34, 0, "", BUTTON, 0, sortame,103,
"Menge",                11, 1, 0,45, 0, "", BUTTON, 0, sortlme,104,
"S",                     5, 1, 0,56, 0, "", BUTTON, 0, sorts,  105
};

static form umlub = { 5, 0, 0,_umlub, 0, 0, 0, 0, &ListFont};

static field _umlvl [] =
{      "1",                  1,  0, 0, 12, 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 34 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 45 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 56 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 60 , 0, "", NORMAL, 0, 0, 0
};

static form umlvl = {5, 0, 0,_umlvl, 0, 0, 0, 0, &ListFont};

//==========================  ListForm PRODUKTIONSSTART     =============================
static field _prodform [] =
{
        aufps.a,           11, 1, 0, 0, 0, "%11.0f", EDIT, 0, 0, 0,
        aufps.a_bz1,       20, 1, 0, 13,0, "",       READONLY, 0, 0, 0,
        aufps.ls_charge,   14, 1, 0, 35, 0, "", EDIT, 0, 0, 0,
        aufps.lief_me,      9, 1, 0, 48, 0, "%9.3f", READONLY , 0, 0, 0,
//        aufps.s,            3, 1, 0, 57, 0, "",      READONLY, 0, 0, 0,
};


static form prodform = { 4, 0, 4,_prodform, 0, CheckHbk, 0, 0, &ListFont};


static field _produb [] = {
"Artikel-Nr",           12, 1, 0, 0, 0, "", BUTTON, 0, sorta,  106,
"Bezeichnung",          22, 1, 0,12, 0, "", BUTTON, 0, sortb,  102,
"WE-Charge",            14, 1, 0,34, 0, "", BUTTON, 0, sortame,103,
"Menge",                11, 1, 0,48, 0, "", BUTTON, 0, sortlme,104,
//"S",                     5, 1, 0,56, 0, "", BUTTON, 0, sorts,  105
};

static form produb = { 4, 0, 0,_produb, 0, 0, 0, 0, &ListFont};

static field _prodvl [] =
{      "1",                  1,  0, 0, 12, 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 34 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 48 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 59 , 0, "", NORMAL, 0, 0, 0,
//       "1",                  1,  0, 0, 60 , 0, "", NORMAL, 0, 0, 0
};

static form prodvl = {4, 0, 0,_prodvl, 0, 0, 0, 0, &ListFont};


//==========================  ListForm  BUCHENFERTIGLAGER    =============================
static field _buchform [] =
{
        aufps.a,            9, 1, 0, 0, 0, "%9.0f", EDIT, 0, 0, 0,
        aufps.a_bz1,       18, 1, 0, 11,0, "",       READONLY, 0, 0, 0,
        aufps.partie,      12, 1, 0, 31, 0, "", EDIT, 0, 0, 0,
        aufps.buch_gew,     9, 1, 0, 44, 0, "%9.3f", READONLY , 0, 0, 0,
        aufps.lief_me,      9, 1, 0, 53, 0, "%9.3f", READONLY , 0, 0, 0,
};


static form buchform = { 5, 0, 4,_buchform, 0, CheckHbk, 0, 0, &ListFont};


static field _buchub [] = {
"Artikel",              10, 1, 0, 0, 0, "", BUTTON, 0, sorta,  106,
"Bezeichnung",          21, 1, 0,10, 0, "", BUTTON, 0, sortb,  102,
"Charge",               13, 1, 0,31, 0, "", BUTTON, 0, sortame,103,
"WEMenge",              10, 1, 0,44, 0, "", BUTTON, 0, sortlme,104,
"Menge",                 9, 1, 0,54, 0, "", BUTTON, 0, sortlme,104,
};

static form buchub = { 5, 0, 0,_buchub, 0, 0, 0, 0, &ListFont};

static field _buchvl [] =
{      "1",                  1,  0, 0, 10, 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 31 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 44 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 54 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 63 , 0, "", NORMAL, 0, 0, 0
};

static form buchvl = {5, 0, 0,_buchvl, 0, 0, 0, 0, &ListFont};



//==========================  ListForm PRODUKTIONSFORTSCHRITT    =============================
static field _fortschrittform [] =
{
        aufps.a,            9, 1, 0, 0, 0, "%9.0f", EDIT, 0, 0, 0,
        aufps.a_bz1,       20, 1, 0, 11,0, "",       READONLY, 0, 0, 0,
        aufps.fortschritt_bz, 9, 1, 0, 33, 0, "", EDIT, 0, 0, 0,
        aufps.partie,       13, 1, 0, 44, 0, "%.0lf", READONLY , 0, 0, 0,
//        aufps.s,            3, 1, 0, 55, 0, "",      READONLY, 0, 0, 0,
};


static form fortschrittform = { 4, 0, 4,_fortschrittform, 0, CheckHbk, 0, 0, &ListFont};


static field _fortschrittub [] = {
"Artikel-Nr",           10, 1, 0, 0, 0, "", BUTTON, 0, sorta,  106,
"Bezeichnung",          22, 1, 0,10, 0, "", BUTTON, 0, sortb,  102,
"Fortschritt",          11, 1, 0,32, 0, "", BUTTON, 0, sortame,103,
"Charge",               14, 1, 0,43, 0, "", BUTTON, 0, sortlme,104,
//"S",                     5, 1, 0,54, 0, "", BUTTON, 0, sorts,  105
};

static form fortschrittub = { 4, 0, 0,_fortschrittub, 0, 0, 0, 0, &ListFont};

static field _fortschrittvl [] =
{      "1",                  1,  0, 0, 10, 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 32 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 43 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 57 , 0, "", NORMAL, 0, 0, 0,
//       "1",                  1,  0, 0, 58 , 0, "", NORMAL, 0, 0, 0
};

static form fortschrittvl = {4, 0, 0,_fortschrittvl, 0, 0, 0, 0, &ListFont};






/** Auswahl ueber Auftraege                                 */

struct AUF_S
{
        char partie [10];
        char partie_bz [49];
        char lief [10];
        char zer_part [12];
        char zer_dat [12];
        char partie_status [3];
};

struct AUF_S aufs, aufs_null;
struct AUF_S aufsarr [AUFMAX];
int aufanz = 0;
int aufidx = 0;

static int aufsort ();
static int kunsort ();
static int kunnsort ();
static int datsort ();
static int kdatsort ();
static int statsort ();



static field _aufform [] =
{
        aufs.partie,          9, 1, 0, 0, 0, "%8d", READONLY, 0, 0, 0,
        aufs.zer_dat,         9, 1, 0, 9,0, "dd.mm.yy", READONLY, 0, 0, 0,
        aufs.partie_bz,       39, 1, 0, 20,0, "", READONLY, 0, 0, 0,
        aufs.partie_status,      2, 1, 0, 59, 0, "%1d", READONLY, 0, 0, 0
};


static form aufform = { 4, 0, 0,_aufform, 0, 0, 0, 0, &ListFont};

/*
static field _aufub [] = {
"Auftrag",            9, 1, 0, 0, 0, "", DISPLAYONLY, 0, 0, 0,
"Kunden-Nr",         10, 1, 0,10, 0, "", DISPLAYONLY, 0, 0, 0,
"Kunden-Name",       17, 1, 0,21, 0, "", DISPLAYONLY, 0, 0, 0,
"Lieferdatum",       11, 1, 0,40, 0, "", DISPLAYONLY, 0, 0, 0,
"S",                  1, 1, 0,57, 0, "", DISPLAYONLY, 0, 0, 0
};
*/

static field _aufub [] = {
"Partie",            9,  1, 0, 0, 0, "", BUTTON, 0, aufsort,  102,
"Datum",             11, 1, 0, 9, 0, "", BUTTON, 0, datsort,  103,
"Bez.",              39, 1, 0,20, 0, "", BUTTON, 0, kunnsort, 104,
"S",                  2, 1, 0,59, 0, "", BUTTON, 0, statsort, 106,
//" ",                  4, 1, 0,52, 0, "", BUTTON, 0, 0, 0,
};

static form aufub = { 4, 0, 0,_aufub, 0, 0, 0, 0, &ListFont};

static field _aufvl [] =
{      "1",                  1,  0, 0,  9, 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 20 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 59 , 0, "", NORMAL, 0, 0, 0,
};
static form aufvl = {3, 0, 0,_aufvl, 0, 0, 0, 0, &ListFont};



//********************   aufform -> StdAuswForm bei Standard , wenn nichts anderes definiert
static field _StdAuswForm [] =
{
        aufs.partie,          9, 1, 0, 0, 0, "%8d", READONLY, 0, 0, 0,
        aufs.zer_dat,         9, 1, 0, 9,0, "dd.mm.yy", READONLY, 0, 0, 0,
        aufs.partie_bz,       39, 1, 0, 20,0, "", READONLY, 0, 0, 0,
        aufs.partie_status,      2, 1, 0, 59, 0, "%1d", READONLY, 0, 0, 0
};
static form StdAuswForm = { 4, 0, 0,_StdAuswForm, 0, 0, 0, 0, &ListFont};
static field _StdAuswub [] = {
"Partie",            9,  1, 0, 0, 0, "", BUTTON, 0, aufsort,  102,
"Datum",             11, 1, 0, 9, 0, "", BUTTON, 0, datsort,  103,
"Bez.",              39, 1, 0,20, 0, "", BUTTON, 0, kunnsort, 104,
"S",                  2, 1, 0,59, 0, "", BUTTON, 0, statsort, 106,
//" ",                  4, 1, 0,52, 0, "", BUTTON, 0, 0, 0,
};
static form StdAuswub = { 4, 0, 0,_StdAuswub, 0, 0, 0, 0, &ListFont};
static field _StdAuswvl [] =
{      "1",                  1,  0, 0,  9, 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 20 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 59 , 0, "", NORMAL, 0, 0, 0,
};
static form StdAuswvl = {3, 0, 0,_StdAuswvl, 0, 0, 0, 0, &ListFont};


//********************   aufform -> AuswProdForm bei PRODUKTIONSSTART , PRODUKTIONSFORTSCHRITT,BUCHENFERTIGLAGER    //FISCH-5
static field _AuswProdForm [] =
{
        aufs.partie,          14, 1, 0, 0, 0, "%8d", READONLY, 0, 0, 0,
        aufs.zer_dat,         9, 1, 0, 14,0, "dd.mm.yy", READONLY, 0, 0, 0,
        aufs.partie_bz,       28, 1, 0, 25,0, "", READONLY, 0, 0, 0,
        aufs.partie_status,      2, 1, 0, 54, 0, "%1d", READONLY, 0, 0, 0
};
static form AuswProdForm = { 4, 0, 0,_AuswProdForm, 0, 0, 0, 0, &ListFont};
static field _AuswProdub [] = {
"Prod.-charge",      14,  1, 0, 0, 0, "", BUTTON, 0, aufsort,  102,
"Datum",             11, 1, 0,14, 0, "", BUTTON, 0, datsort,  103,
"    ",              29, 1, 0,25, 0, "", BUTTON, 0, kunnsort, 104,
"S",                  3, 1, 0,54, 0, "", BUTTON, 0, statsort, 106,
//" ",                  4, 1, 0,52, 0, "", BUTTON, 0, 0, 0,
};
static form AuswProdub = { 4, 0, 0,_AuswProdub, 0, 0, 0, 0, &ListFont};
static field _AuswProdvl [] =
{      "1",                  1,  0, 0, 14, 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 25 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 54 , 0, "", NORMAL, 0, 0, 0,
};
static form AuswProdvl = {3, 0, 0,_AuswProdvl, 0, 0, 0, 0, &ListFont};

//********************   aufform -> AuswFortschrittform bei PRODUKTIONSFORTSCHRITT    //FISCH-5
static field _AuswFortschrittform [] =
{
        aufs.partie,          14, 1, 0, 0, 0, "%8d", READONLY, 0, 0, 0,
        aufs.zer_dat,         9, 1, 0, 14,0, "dd.mm.yy", READONLY, 0, 0, 0,
        aufs.partie_bz,       28, 1, 0, 25,0, "", READONLY, 0, 0, 0,
        aufs.partie_status,      2, 1, 0, 54, 0, "%1d", READONLY, 0, 0, 0
};
static form AuswFortschrittform = { 4, 0, 0,_AuswFortschrittform, 0, 0, 0, 0, &ListFont};
static field _AuswFortschrittub [] = {
"Prod.-charge",      14,  1, 0, 0, 0, "", BUTTON, 0, aufsort,  102,
"Datum",             11, 1, 0,14, 0, "", BUTTON, 0, datsort,  103,
"    ",              29, 1, 0,25, 0, "", BUTTON, 0, kunnsort, 104,
"S",                  3, 1, 0,54, 0, "", BUTTON, 0, statsort, 106,
//" ",                  4, 1, 0,52, 0, "", BUTTON, 0, 0, 0,
};
static form AuswFortschrittub = { 4, 0, 0,_AuswFortschrittub, 0, 0, 0, 0, &ListFont};
static field _AuswFortschrittvl [] =
{      "1",                  1,  0, 0, 14, 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 25 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 54 , 0, "", NORMAL, 0, 0, 0,
};
static form AuswFortschrittvl = {3, 0, 0,_AuswFortschrittvl, 0, 0, 0, 0, &ListFont};




static int faufsort = 1;
static int fkunsort = 1;
static int fkunnsort = 1;
static int fdatsort = 1;
static int fkdatsort = 1;
static int fstatsort = 1;

static int aufsort0 (const void *elem1, const void *elem2)
{
	        struct AUF_S *el1;
	        struct AUF_S *el2;

		  el1 = (struct AUF_S *) elem1;
		  el2 = (struct AUF_S *) elem2;
	        return ((int) (atol(el1->partie) - atol (el2->partie)) *
				                                  faufsort);
}


static int aufsort (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufsarr, aufanz, sizeof (struct AUF_S),
				   aufsort0);
		  faufsort *= -1;
              ShowNewElist ((char *) aufsarr,
                             aufanz,
                            (int) sizeof (struct AUF_S));
		  return 0;
}

static int kunsort0 (const void *elem1, const void *elem2)
{
	        struct AUF_S *el1;
	        struct AUF_S *el2;

		  el1 = (struct AUF_S *) elem1;
		  el2 = (struct AUF_S *) elem2;
	        return ((int) (atol(el1->zer_dat) - atol (el2->zer_dat)) *
		                                  fkunsort);
}

static int kunsort (void)
/**
Nach Artikelnummer sortieren.
**/
{
	      qsort (aufsarr, aufanz, sizeof (struct AUF_S),
				    kunsort0);
		fkunsort *= -1;
            ShowNewElist ((char *) aufsarr,
                             aufanz,
                            (int) sizeof (struct AUF_S));
		return 0;
}


static int kunnsort0 (const void *elem1, const void *elem2)
{
	        struct AUF_S *el1;
	        struct AUF_S *el2;

		  el1 = (struct AUF_S *) elem1;
		  el2 = (struct AUF_S *) elem2;
	        return ((int) strcmp(el1->partie_bz, el2->partie_bz) *
				                                  fkunnsort);
}

static int kunnsort (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufsarr, aufanz, sizeof (struct AUF_S),
				   kunnsort0);
 		  fkunnsort *= -1;
              ShowNewElist ((char *) aufsarr,
                             aufanz,
                            (int) sizeof (struct AUF_S));
		  return 0;
}


static int datsort0 (const void *elem1, const void *elem2)
{
	        struct AUF_S *el1;
	        struct AUF_S *el2;
              long dat1;
              long dat2;

		  el1 = (struct AUF_S *) elem1;
		  el2 = (struct AUF_S *) elem2;
              dat1 = dasc_to_long (el1->zer_dat);
              dat2 = dasc_to_long (el2->zer_dat);
              if (dat1 > dat2)
              {
                            return  (1 * fdatsort);
              }
              else
              if (dat1 < dat2)
              {
                            return  (-1 * fdatsort);
              }
              return 0;
}

static int datsort (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufsarr, aufanz, sizeof (struct AUF_S),
				   datsort0);
		  fdatsort *= -1;
              ShowNewElist ((char *) aufsarr,
                             aufanz,
                            (int) sizeof (struct AUF_S));
		  return 0;
}
static int kdatsort0 (const void *elem1, const void *elem2)
{
	        struct AUF_S *el1;
	        struct AUF_S *el2;
              long dat1;
              long dat2;

		  el1 = (struct AUF_S *) elem1;
		  el2 = (struct AUF_S *) elem2;
              dat1 = dasc_to_long (el1->zer_dat);
              dat2 = dasc_to_long (el2->zer_dat);
              if (dat1 > dat2)
              {
                            return  (1 * fkdatsort);
              }
              else
              if (dat1 < dat2)
              {
                            return  (-1 * fkdatsort);
              }
              return 0;
}

static int kdatsort (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufsarr, aufanz, sizeof (struct AUF_S),
				   datsort0);
		  fkdatsort *= -1;
              ShowNewElist ((char *) aufsarr,
                             aufanz,
                            (int) sizeof (struct AUF_S));
		  return 0;
}


static int statsort0 (const void *elem1, const void *elem2)
{
	        struct AUF_S *el1;
	        struct AUF_S *el2;

		  el1 = (struct AUF_S *) elem1;
		  el2 = (struct AUF_S *) elem2;
	        return ((int) (atoi(el1->partie_status) - atoi (el2->partie_status)) *
		                                  fstatsort);
}

static int statsort (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufsarr, aufanz, sizeof (struct AUF_S),
				   statsort0);
		  fstatsort *= -1;
              ShowNewElist ((char *) aufsarr,
                             aufanz,
                            (int) sizeof (struct AUF_S));
		  return 0;
}


void IsAwClck (int idx)
/**
Reaktion auf Doppelclick in der Liste.
**/
{
        aufidx = idx;
		{
            break_list ();
		}
return;
}








//=====================================================
//============== Prodartikel FISCH-21 ================= 
//=====================================================

struct PRODARTIKEL
{
    double a ;
	char ca [14] ;
    char a_bz1 [33];
};


struct PRODARTIKEL prodartikelarr[50], prodartikel;

static int prodartikelidx;
static int prodartikelanz = 0;

mfont ProdartikelFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                               RGB (255, 255, 255),
                               1,
                               NULL};


static field _prodartikelform [] =
{
        prodartikel.ca,    6, 1, 0, 1, 0, "%.0lf", DISPLAYONLY, 0, 0, 0,
        prodartikel.a_bz1,     20, 1, 0,7, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form fprodartikelform = {2, 0, 0, _prodartikelform, 0, 0, 0, 0, &ProdartikelFont};

static field _fprodartikelub [] =
{
        "Artikel",              6, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        "Bezeichnung",         20, 1, 0,7, 0, "", DISPLAYONLY, 0, 0, 0
};

static form fprodartikelub = {2, 0, 0, _fprodartikelub, 0, 0, 0, 0, &ProdartikelFont};

static field _fprodartikelvl [] =
{ 
	"1",                  1,  0, 0, 6, 0, "", NORMAL, 0, 0, 0,
};

static form fprodartikelvl = {1, 0, 0, _fprodartikelvl, 0, 0, 0, 0, &ProdartikelFont};

double a_prod = 0;
char a_prod_bz [sizeof _a_bas.a_bz1] ; 

double ShowProdartikel (void)
{
        EnableWindow (hMainWindow, FALSE);
        prodartikelidx = ShowPtBox (eWindow, (char *) prodartikelarr, prodartikelanz,
                     (char *) &prodartikel,(int) sizeof (struct PRODARTIKEL),
                     &fprodartikelform, &fprodartikelvl, &fprodartikelub);
        EnableWindow (hMainWindow, TRUE);
                prodartikel.a = prodartikelarr[prodartikelidx].a  ;
                sprintf (prodartikel.a_bz1,"%s", prodartikelarr[prodartikelidx].a_bz1);
		if (syskey == KEY5) return 0.0;
        return prodartikel.a;
}

double HoleProdartikel (double weartikel)
{
	static short acursor = -1;
		prodartikelanz = 0;
		     acursor = -1; 
			if (acursor == -1)
			{
			    DbClass.sqlout ((double *) &prodartikel.a, 3, 0);
			    DbClass.sqlout ((char *) prodartikel.a_bz1, 0, sizeof (prodartikel.a_bz1));
				DbClass.sqlin ((short *)  &Mandant, 1, 0);
				DbClass.sqlin ((double *)  &weartikel,  3, 0);
				acursor = DbClass.sqlcursor ("select a,a_bz1 from a_bas where a in "
											  "(select a from prdk_k where rez in "
												 "(select rez from prdk_varb where mdn = ? and mat in "
												   "(select mat from a_mat where a  = ? "
													" )))  ");
			}
		    DbClass.sqlopen (acursor); 
		    dsqlstatus =DbClass.sqlfetch (acursor); 
            while (dsqlstatus == 0)
            {
                prodartikelarr[prodartikelanz].a =   prodartikel.a;
                sprintf (prodartikelarr[prodartikelanz].ca ,"%.0lf",   prodartikel.a);
                sprintf (prodartikelarr[prodartikelanz].a_bz1 ,"%s",   prodartikel.a_bz1);
		       prodartikelanz ++;
			   if (prodartikelanz == 49) break;
			    dsqlstatus =DbClass.sqlfetch (acursor); 
            }
			if (prodartikelanz == 0) 
			{
				prodartikel.a = 0.0;
				return 0.0;
			}
			if (prodartikelanz == 1)
			{
	               prodartikel.a = prodartikelarr[prodartikelanz].a  ;
				   return prodartikel.a;
			}
			return ShowProdartikel ();
}



//ENDE========== Prodartikel =========================== 









//******************************** SCANNEN ******************************************
//******************************** SCANNEN ******************************************
//******************************** SCANNEN ******************************************
class CScanMessage
{
public:
	CSounds Sounds;
	BOOL Extern;
	Text ErrorName;
	Text OkName;
	Text ScanOkName;
	Text CompleteName;
	CScanMessage ()
	{
		char *etc;
		Extern = FALSE;
		etc = getenv ("BWSETC");
		if (etc != NULL)
		{
			ErrorName.Format ("%s\\wav\\Error.wav", etc);
			ScanOkName.Format ("%s\\wav\\ScanOK.wav", etc);
			OkName.Format ("%s\\wav\\OK.wav", etc);
			CompleteName.Format ("%s\\wav\\Complete.wav", etc);
		}
		else
		{
			ErrorName = "";
			ScanOkName = "";
			OkName = "";
			CompleteName = "";
		}

	};

	virtual void Error ()
	{
		if (Extern)
		{
			Sounds.SetSoundName (ErrorName);
			Sounds.Play ();
		}
		else
		{
			Sounds.Play (IDR_ERROR);
		}
			
	}

	virtual void ScanOK ()
	{
		if (Extern)
		{
			Sounds.SetSoundName (ScanOkName);
			Sounds.Play ();
		}
		else
		{
			Sounds.Play (IDR_SCANOK);
		}
	}

	virtual void OK ()
	{
		if (Extern)
		{
			Sounds.SetSoundName (OkName);
			Sounds.Play ();
		}
		else
		{
			Sounds.Play (IDR_OK);
		}
	}

	virtual void Complete ()
	{
		if (Extern)
		{
			Sounds.SetSoundName (CompleteName);
			Sounds.Play ();
		}
		else
		{
			Sounds.Play (IDR_COMPLETE);
		}
	}
};

CScanMessage ScanMessage;


class CScanValues 
{
public:
	double a;
	double eangew;
	double gew;
	Text Charge;
	Text Mhd;
	double auf_me;
	Text ACharge;
	BOOL aflag;
	BOOL gewflag;
	BOOL ChargeFlag;
	BOOL MhdFlag;
	CScanValues ()
	{
		a = 0.0;
		eangew = 0.0;
		gew = 0.0;
        Charge = "";
		Mhd = "";
		auf_me = 0;
		ACharge = "";
		aflag = FALSE;
		gewflag = FALSE;
		ChargeFlag = FALSE;
		MhdFlag = FALSE;
	}

	void Init ()
	{
		eangew = 0.0;
		a = 0.0;
        Charge = "";
		Mhd = "";
		aflag = FALSE;
		gewflag = FALSE;
		ChargeFlag = FALSE;
		MhdFlag = FALSE;
	}

	void SetA (double a)
	{
		this->a = a;
		aflag = TRUE;
	}

	void SetGew (double gew)
	{
		this->eangew = gew;
		this->gew += gew;
		gewflag = TRUE;
	}

	void SetCharge (Text& Charge)
	{
		this->Charge = Charge;
		ChargeFlag = TRUE;
	}
	int GetChargeLen (void)
	{
		return this->Charge.GetLen();
	}

	void SetMhd (Text& Mhd)
	{
		this->Mhd = Mhd;
		MhdFlag = TRUE;
	}

	void IncAufMe ()
	{
		auf_me ++;
	}

	BOOL SetACharge ()
	{
		if (ACharge != "")
		{
			if (ACharge != Charge) return FALSE;
// Sp�ter eventuell eine neue Position erzeugen
			ACharge = Charge;
			return TRUE;
		}
        ACharge = Charge;
		return TRUE;
	}

	BOOL ScanOK ()
	{
		if (atoi(EinAusgang) == PRODUKTIONSSTART )
		{
			if (!aflag) return FALSE;
		}
		else if (atoi(EinAusgang) == PRODUKTIONSFORTSCHRITT )
		{
			if (!aflag) return FALSE;
			if (!ChargeFlag) return FALSE;
			if (!ChargeInListe (clipped(this->Charge.GetBuffer()))) return FALSE;
		}
		else
		{
			if (!aflag) return FALSE;
	 		if (!gewflag) return FALSE;
			if (!ChargeFlag) return FALSE;
		}
		return TRUE;
	}

	BOOL ScanValuesOK ()
	{
		if (a == 0.0) return FALSE;
		if (gew == 0.0) return FALSE;
		if (Charge == "") return FALSE;
		return TRUE;
	}
};

CScanValues *ScanValues = NULL;
















struct WIEP
{
    char a [15];
    char a_bz1 [26];
    char brutto [12];
    char dat [15];
    char zeit [10];
}; 

struct WIEP wiep, wieparr[50];

static int wiepidx;
static int wiepanz = 0;

int GetAktButton (void)
/**
Aktiven Button holen.
**/
{
        HWND hWnd;
        int i;

        hWnd = GetFocus ();

        for (i = 0; i < MainButton.fieldanz; i ++)
        {
                    if (hWnd == MainButton.mask[i].feldid)
                    {
                               return (i);
                    }
        }

        for (i = 0; i < MainButton2.fieldanz; i ++)
        {
                    if (hWnd == MainButton2.mask[i].feldid)
                    {
                            return (MainButton.fieldanz + i);
                    }
        }
        return (AktButton);
}

field *GetAktField ()
/**
Field zu AktButton holen.
**/
{
        if (AktButton < MainButton.fieldanz)
        {
                      return (&MainButton.mask [AktButton]);
        }
        return (&MainButton2.mask[AktButton - MainButton.fieldanz]);
}


BOOL TestOK (void)
/**
Test, ob OK-Button aktiv ist.
**/
{
	     if (BuOK.aktivate > -1)
		 {
			 return TRUE;
		 }
		 return FALSE;
}


HWND WiegKg;
HWND WiegCtr;

int menfuncA_LgrAb (void)
{
         HWND aktfocus;
		extern form LgrSpezButton;

		 bin_in_menfunc = 11;
         aktfocus = GetFocus ();
		 if (akt_lager_ab < 1) akt_lager_ab = LagerAb;
		 sprintf(Zuruecktext,"Zuordn.�ndern");
         akt_lager_ab = AuswahlALager (akt_lager_ab,FALSE);
		 sprintf(Zuruecktext,"Zurueck F5");

         if ( lgr_class.lese_lgr_bz (akt_lager_ab) == 0) sprintf (LagertxtAb,"Von: %s",lgr.lgr_bz);
       ColorColButton (WiegCtr, &LgrSpezButton, 0, BLUECOL, 0);
       ColorColButton (WiegCtr, &LgrSpezButton, 2, BLUECOL, 0);
         AktButton = GetAktButton ();
         return 0;
}
int menfuncA_LgrZu (void)
{
         HWND aktfocus;
		extern form LgrSpezButton;

		 bin_in_menfunc = 12;
         aktfocus = GetFocus ();
		 if (akt_lager_zu < 1) akt_lager_zu = LagerZu;
		 sprintf(Zuruecktext,"Zuordn.�ndern");
         akt_lager_zu = AuswahlALager (akt_lager_zu,TRUE);
		 sprintf(Zuruecktext,"Zurueck F5");
         if ( lgr_class.lese_lgr_bz (akt_lager_zu) == 0) sprintf (LagertxtZu,"Nach: %s",lgr.lgr_bz);
			 a_lgr.mdn = zerl_k.mdn;
			 a_lgr.lgr = lgr.lgr;
			 if (a_lgr_class.dbreadfirst() == 0)
			 {
				 sprintf (Buchartikeltxt, "");
				 if (a_lgr.buch_artikel > 0)
				 {
					 sprintf (Buchartikeltxt, "Art. %13.0lf",a_lgr.buch_artikel);
				 }
			 }
		 a_lgr.mdn = zerl_k.mdn;
		 a_lgr.lgr = lgr.lgr;
		 a_lgr_class.dbreadfirst();
       ColorColButton (WiegCtr, &LgrSpezButton, 0, BLUECOL, 0);
       ColorColButton (WiegCtr, &LgrSpezButton, 2, BLUECOL, 0);
         AktButton = GetAktButton ();
         return 0;
}

int menfuncLgrZu (void)
{
         HWND aktfocus;

		 bin_in_menfunc = 14;
         aktfocus = GetFocus ();
         LagerZu = AuswahlLager (1,LagerZu);
         if ( lgr_class.lese_lgr_bz (LagerZu) == 0) strcpy (LgrZu_krz,lgr.lgr_bz);
		 sprintf (LagerZutxt, "%s %hd", LZutxt, LagerZu);
         AktButton = GetAktButton ();
         return 0;
}
int menfuncLgrAb (void)
{
         HWND aktfocus;
		 bin_in_menfunc = 13;
         aktfocus = GetFocus ();
         LagerAb = AuswahlLager (-1,LagerAb);
         if ( lgr_class.lese_lgr_bz (LagerAb) == 0) strcpy (LgrAb_krz,lgr.lgr_bz);
		 sprintf (LagerAbtxt, "%s %hd", LAbtxt, LagerAb);
         AktButton = GetAktButton ();
         return 0;
}




int menfunc1 (void)
/**
Function fuer Button 1
**/
{
         HWND aktfocus;

		 bin_in_menfunc = 1;
         stopauswahl = FALSE;
         InitAufKopf ();
         AuftragKopf (ENTER);
         aktfocus = GetFocus ();
         SetColFocus (MainButton.mask[0].feldid);
         AktButton = GetAktButton ();
         return 0;
}

int menfunc2 (void)
/**
Function fuer Button 2
**/
{

         HWND aktfocus;
		 extern form LiefKopfE;
		 extern form LiefKopfED;


         if (AufLock) return 0;

		 bin_in_menfunc = 2;
		 double test = ratod(lief_nr);

		BuInsert.text1 = "Neu";
		BuDel.text1 = "L�schen";
		if (atoi(EinAusgang) == PRODUKTIONSFORTSCHRITT) BuInsert.text1 = "Forts.Bearb.";
		if (atoi(EinAusgang) == PRODUKTIONSFORTSCHRITT) BuDel.text1 = "Umbuchen";

//         if (atol (lief_nr) > 0 && LiefEnterAktiv) //100908 wenn strlen > 10 gehts nicht mit atol
//		 if (atoi(EinAusgang) == PRODUKTIONSFORTSCHRITT) strcpy (lief_nr,"1"); //FISCH-5

         if (ratod (lief_nr) > 0.0 && LiefEnterAktiv)
         {
                      EnterListe ();
                      break_lief_enter = 1;
         }
         else if (  (atoi (EinAusgang) == PRODUKTIONSFORTSCHRITT || atoi (EinAusgang) == BUCHENFERTIGLAGER )  && LiefEnterAktiv) //FISCH-5
         {
                      EnterListe ();
                      break_lief_enter = 1;
         }


         aktfocus = GetFocus ();
         AktButton = GetAktButton ();
		ActivateColButton (BackWindow3, &MainButton2, 0, -1, 1); //FISCH-5 Etikettenbutton
         return 0;
}


int ItPos (form *frm, char *It)
/**
Position eines Maskenfeldes ermitteln.
**/
{
	      int i;

		  for (i = 0; i < frm->fieldanz; i ++)
		  {
			  if (frm->mask[i].feld == It) return i;
		  }
		  return 0;
}

int menfunc4 (void)
/**
Function fuer Button 4
**/
{
         HWND aktfocus;
		 char mdn [5];

		 bin_in_menfunc = 4;
		 sprintf (mdn, "%hd", Mandant);
         EnterNumBox (LogoWindow,  "  Mandant  ", mdn, 5, "%4d");
		 if (syskey == KEY5) return 0;
		 Mandant = atoi (mdn);
		 sprintf (Mandanttxt, "%s %hd", Mtxt, Mandant);
	     display_field (BackWindow2, &MainButton.mask[ItPos (&MainButton, (char *) &CMandant)]);

         aktfocus = GetFocus ();
         AktButton = GetAktButton ();


         return 0;
}


int menfunc5 (void)
/**
Function fuer Button 4
**/
{
         HWND aktfocus;
		 char fil [5];

		 bin_in_menfunc = 5;
		 sprintf (fil, "%hd", Filiale);
         EnterNumBox (LogoWindow,  "  Filiale  ", fil, 5, "%4d");
		 if (syskey == KEY5) return 0;
		 Filiale = atoi (fil);
		 sprintf (Filialetxt, "%s %hd", Ftxt, Filiale);
	     display_field (BackWindow2, &MainButton.mask[ItPos(&MainButton, (char *) &CFiliale)]);

         aktfocus = GetFocus ();
         AktButton = GetAktButton ();


         return 0;
}

int menfunc6 (void)
/**
Function fuer Button 6
**/
{
         HWND aktfocus;

		 bin_in_menfunc = 6;
         aktfocus = GetFocus ();
         AuswahlSysPeri ();
         AktButton = GetAktButton ();
         return 0;
}

void MainBuInactivQm (void)
/**
Alle Button auf Inaktiv setzen.
**/
{
          ActivateColButton (BackWindow2, &MainButton, 0, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 1, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 2, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 3, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 4, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 5, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 6, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 7, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 8, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 9, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 10, -1, 1);
}

void MainBuActivQm (void)
/**
Alle Button auf Inaktiv setzen.
**/
{
          ActivateColButton (BackWindow2, &MainButton, 1, 1, 1);
          ActivateColButton (BackWindow2, &MainButton, 2, 1, 1);
          ActivateColButton (BackWindow2, &MainButton, 4, 1, 1);
          ActivateColButton (BackWindow2, &MainButton, 9, 1, 1);
          set_fkt (NULL, 6);
}

void MainBuInactivQmPos (void)
/**
Alle Button auf Inaktiv setzen.
**/
{
          MainBuInactivQm ();
  //        ActivateColButton (BackWindow3, &MainButton2, 0, -1, 1);
          ActivateColButton (BackWindow3, &MainButton2, 3, -1, 1);
}

void MainBuActivQmPos (void)
/**
Alle Button auf Inaktiv setzen.
**/
{
          MainBuActivQm ();
          ActivateColButton (BackWindow3, &MainButton2, 0, 1, 1);
          ActivateColButton (BackWindow3, &MainButton2, 3, 1, 1);
}


BOOL QmAktiv = FALSE;

static char *Ub[] = {"Qualit�tssicherung Hygiene",
                      NULL,
};


static char *Texte0[] = {"Laderaum hygienisch",
                         "Haaken und Beh�ltnisse i.O.",
			             "Holzpaletten/Kartonagen",
			             "Fleisch korrekt verpackt",
			             "Kleidung hygienisch",
			             "Kopfbedeckung",
				          NULL, NULL
};


char **Texte = Texte0;

static int Yes0[] = {0, 0, 0, 0, 0, 0, 0, 0};
static int No0[]  = {0, 0, 0, 0, 0, 0, 0, 0};

static int *Yes = Yes0;
static int *No  = No0;
static int qsanz = 6;

void FillQsHead ()
/**
Texte fuer Qualitaetssicherung fuellen.
**/
{
    int dsqlstatus;
    int cursor;
    char txt [25];
    int i;

    DbClass.sqlout ((short *) &qsanz, 1, 0);
    dsqlstatus = DbClass.sqlcomm ("select count (*) from qstxt where qstyp = 1 "
                                                               "and lief = \" \"");
    if (dsqlstatus != 0)
    {
        return;
    }

    if (qsanz == 0)
    {
        return;
    }

    Texte = new char* [qsanz + 2];
    Yes   = new int [qsanz + 2];
    No    = new int [qsanz + 2];

    for (i = 0; i < qsanz; i ++)
    {
        Yes[i] = No[i] = 0;
    }


    DbClass.sqlout ((char *) txt, 0, 25);
    cursor = DbClass.sqlcursor ("select txt,sort from qstxt where qstyp = 1 "
                                                            "and lief = \" \" "
                                                            "order by sort");
    if (cursor < 0)
    {
        return;
    }

    i = 0;
    while (DbClass.sqlfetch (cursor) == 0)
    {
        Texte [i] = new char [25];
        strcpy (Texte[i], txt);
        i ++;
    }

    Texte[i] = NULL;
    DbClass.sqlclose (cursor);
}


void FillQsHead (char *lief_nr)
/**
Texte fuer Qualitaetssicherung fuellen.
**/
{
    int cursor;
    char txt [25];
    char lief [17];
    int i;

    strcpy (lief, lief_nr);
    DbClass.sqlout ((short *) &qsanz, 1, 0);
    DbClass.sqlin ((char *) lief, 0, 17);
    cursor = DbClass.sqlcursor ("select count (*) from qstxt where qstyp = 1 "
                                                               "and lief = ?");
    qsanz = 0;
    DbClass.sqlfetch (cursor);
    while (qsanz == 0)
    {
        if (strcmp (lief, " ") > 0)
        {
            strcpy (lief, " ");
        }
		else
        {
            break;
        }
        DbClass.sqlopen (cursor);
        DbClass.sqlfetch (cursor);
    }

    DbClass.sqlclose (cursor);
        
    if (qsanz == 0)
    {
		return;
    }

    for (i = 0; Texte[i] != NULL; i ++)
    {
          delete Texte[i];
    }
    delete Texte;
    delete Yes;
    delete No;

    Texte = new char* [qsanz + 2];
    Yes   = new int [qsanz + 2];
    No    = new int [qsanz + 2];

    for (i = 0; i < qsanz; i ++)
    {
        Yes[i] = No[i] = 0;
    }


    DbClass.sqlout ((char *) txt, 0, 25);
    DbClass.sqlin ((char *) lief, 0, 17);
    cursor = DbClass.sqlcursor ("select txt,sort from qstxt where qstyp = 1 "
                                                            "and lief = ? "
                                                            "order by sort");
    if (cursor < 0)
    {
        return;
    }

    i = 0;
    while (DbClass.sqlfetch (cursor) == 0)
    {
        Texte [i] = new char [25];
        strcpy (Texte[i], txt);
        i ++;
    }

    Texte[i] = NULL;
    DbClass.sqlclose (cursor);
}



void InitQm (int *Yes, int *No, int anz)
/**
Eingaben pruefen.
**/
{
	      int i;

		  for (i = 0; i < anz; i ++)
		  {
			  Yes[i] = 0;
			  No [i] = 0;
		  }
}


BOOL TestQm (int *Yes, int *No, int anz)
/**
Eingaben pruefen.
**/
{
	      int i;

		  for (i = 0; i < anz; i ++)
		  {
			  if (Yes[i] == 0 &&
				  No [i] == 0)
			  {
				  return FALSE;
			  }
		  }
		  return TRUE;
}

BOOL TestQs (BOOL direkt)
{
	return TRUE;
}


void WriteQs (void)
/**
Eingaben aus Qualit�tssicherung speichern.
**/
{
}


int menfunc7 (void)
/**
Function fuer Button 7
**/
{
         return 0;
}


static char *UbPos[] = {"Qualit�tssicherung Fleisch",
                      NULL,
};

static char *TextePos0[] = {"Fettgehalt OK",
                           "Gewicht OK",
		           	       "Zuschnitt OK",
			               "Spezifikationen OK",
			               "Zustand der Ware OK",
			               "ordnungsgem�� gestempelt",
				           NULL, NULL
};

static int YesPos0[] = {0, 0, 0, 0, 0, 0, 0, 0};
static int NoPos0[]  = {0, 0, 0, 0, 0, 0, 0, 0};
static int qsanzpos = 6;

static char **TextePos = TextePos0;
static int *YesPos = YesPos0;
static int *NoPos  = NoPos0;

void FillQsPos ()
/**
Texte fuer Qualitaetssicherung fuellen.
**/
{
    int dsqlstatus;
    int cursor;
    char txt [25];
    int i;



    DbClass.sqlout ((short *) &qsanzpos, 1, 0);
    dsqlstatus = DbClass.sqlcomm ("select count (*) from qstxt where qstyp = 2 "
                                                               "and hwg = 0 "
                                                               "and wg = 0 "
                                                               "and ag = 0");
    if (dsqlstatus != 0)
    {
        return;
    }

    if (qsanzpos == 0)
    {
        return;
    }

    TextePos = new char* [qsanzpos + 2];
    YesPos   = new int [qsanzpos + 2];
    NoPos    = new int [qsanzpos + 2];

    for (i = 0; i < qsanzpos; i ++)
    {
        YesPos[i] = NoPos[i] = 0;
    }


    DbClass.sqlout ((char *) txt, 0, 25);
    cursor = DbClass.sqlcursor ("select txt,sort from qstxt where qstyp = 2 "
                                                               "and hwg = 0 "
                                                               "and wg = 0 "
                                                               "and ag = 0 "
                                                               "order by sort");
    if (cursor < 0)
    {
        return;
    }

    i = 0;
    while (DbClass.sqlfetch (cursor) == 0)
    {
        TextePos [i] = new char [25];
        strcpy (TextePos[i], txt);
        i ++;
    }

    TextePos[i] = NULL;
    DbClass.sqlclose (cursor);
}


void FillQsPos (double a)
/**
Texte fuer Qualitaetssicherung fuellen.
**/
{
    int cursor;
    char txt [25];
    int i;
    short hwg = 0;
    short wg = 0;
    short ag = 0l;

    DbClass.sqlout ((short *)  &hwg, 1, 0);
    DbClass.sqlout ((short *)  &wg,  1, 0);
    DbClass.sqlout ((long *)   &ag,  2, 0);
    DbClass.sqlin  ((double *) &a,   3, 0);
    DbClass.sqlcomm ("select hwg, wg, ag from a_bas where a = ?");

    DbClass.sqlout ((short *) &qsanzpos, 1, 0);
    DbClass.sqlin ((short *)  &hwg, 1, 0);
    DbClass.sqlin ((short *)  &wg,  1, 0);
    DbClass.sqlin ((long *)   &ag,  2, 0);
    cursor = DbClass.sqlcursor ("select count (*) from qstxt where qstyp = 2 "
                                                               "and hwg = ? "
                                                               "and wg = ? "
                                                               "and ag = ?");
   qsanzpos = 0;
   DbClass.sqlfetch (cursor); 
   while (qsanzpos == 0)
    {
        if (ag > 0l)
        {
            ag = 0l;
        }
        else if (wg > 0)
        {
            wg = 0;
        }
        else if (hwg > 0)
        {
            hwg = 0;
        }
        else
        {
            break;
        }
        DbClass.sqlopen (cursor);
        DbClass.sqlfetch (cursor); 
    }
    DbClass.sqlclose (cursor);

    if (qsanzpos == 0)
    {
        return;
    }

    if (TextePos != NULL &&
        TextePos != TextePos0)
    {
       for (i = 0; TextePos[i] != NULL; i ++)
       {
           delete TextePos[i];
       }
       delete TextePos;
       delete YesPos;
       delete NoPos;
    }

    TextePos = new char* [qsanzpos + 2];
    YesPos   = new int [qsanzpos + 2];
    NoPos    = new int [qsanzpos + 2];

    for (i = 0; i < qsanzpos; i ++)
    {
        YesPos[i] = NoPos[i] = 0;
    }


    DbClass.sqlout ((char *) txt, 0, 25);
    DbClass.sqlin ((short *)  &hwg, 1, 0);
    DbClass.sqlin ((short *)  &wg,  1, 0);
    DbClass.sqlin ((long *)   &ag,  2, 0);
    cursor = DbClass.sqlcursor ("select txt,sort from qstxt where qstyp = 2 "
                                                            "and hwg = ? "
                                                            "and wg = ? "
                                                            "and ag = ? "
                                                            "order by sort");
    if (cursor < 0)
    {
        return;
    }

    i = 0;
    while (DbClass.sqlfetch (cursor) == 0)
    {
        TextePos [i] = new char [25];
        strcpy (TextePos[i], txt);
        i ++;
    }

    TextePos[i] = NULL;
    DbClass.sqlclose (cursor);
}







BOOL TestQsPosA (double a)
{
         
         return TRUE;
}

BOOL TestQsPos (void)
{
         return TRUE;
}

void ReadQsPosWieg (void)
{
		 int i = 0;
		 char datum [12];
		 extern int aufpidx;
		 int dsqlstatus  = 100;

         aufpidx = SendMessage  (Getlbox (), LB_GETCURSEL, 0l, 0l);
         memcpy (&aufps, &aufparr[aufpidx], sizeof (struct AUFP_S));

		 sysdate (datum);
		 qswepos.mdn = Mandant;
		 qswepos.fil = Filiale;
		 strcpy (qswepos.lief, "0");
		 sprintf(qswepos.lief, "%ld%", zerl_p.standort * -1);
		 sprintf(qswepos.lief_rech_nr, "%8.0lf%", zerl_p.partie);
		 qswepos.dat = dasc_to_long (datum);
         qswepos.lfd = (int) 1;
         qswepos.a   = ratod (aufps.a);
         qswepos.int_pos   = atol (aufps.pnr);
	     dsqlstatus = QsWePos.dbreadfirst ();

		 for (i = 0; i < 25; i++)
		 {
			 FestAn[i] = 0;
		 }
		 while (dsqlstatus == 0)	
		 {
				if (qswepos.wrt == 0) FestAn[qswepos.lfd -1] = 2;
				if (qswepos.wrt == 1) FestAn[qswepos.lfd -1] = 1;
			    dsqlstatus = QsWePos.dbread ();
		 }
}

void WriteQsPosWieg (void)
{
		 int i = 0;
		 char datum [12];
		 extern int aufpidx;
		 int dsqlstatus  = 100;

         aufpidx = SendMessage  (Getlbox (), LB_GETCURSEL, 0l, 0l);
         memcpy (&aufps, &aufparr[aufpidx], sizeof (struct AUFP_S));

		 sysdate (datum);
		 qswepos.mdn = Mandant;
		 qswepos.fil = Filiale;
		 strcpy (qswepos.lief, "0");
		 sprintf(qswepos.lief, "%ld%", zerl_p.standort * -1);
		 sprintf(qswepos.lief_rech_nr, "%8.0lf", zerl_p.partie);
		 qswepos.dat = dasc_to_long (datum);
         qswepos.lfd = (int) 1;
         qswepos.a   = ratod (aufps.a);
         qswepos.int_pos   = atol (aufps.pnr);
	     dsqlstatus = QsWePos.dbreadfirst ();

		 for (i = 0; i < 25; i ++)
		 {
 		          qswepos.lfd = (int) i + 1;
				  strcpy (qswepos.txt, ftabarr[i].ptbez);
				  if (FestAn[i] == 2) qswepos.wrt = 0;
				  if (FestAn[i] == 1) qswepos.wrt = 1;
				  if (FestAn[i] > 0)
				  {
					QsWePos.dbupdate ();
				  }
		 }
}


void WriteQsPos (void)
/**
Eingaben aus Qualit�tssicherung speichern.
**/
{
	     int i;
		 char datum [12];
		 extern int aufpidx;

         aufpidx = SendMessage  (Getlbox (), LB_GETCURSEL, 0l, 0l);
         memcpy (&aufps, &aufparr[aufpidx], sizeof (struct AUFP_S));

		 sysdate (datum);
		 qswepos.mdn = Mandant;
		 qswepos.fil = Filiale;
		 strcpy (qswepos.lief, "0");
		 sprintf(qswepos.lief, "%ld%", zerl_p.standort * -1);
		 sprintf(qswepos.lief_rech_nr, "%8.0lf", zerl_p.partie);
		 qswepos.dat = dasc_to_long (datum);
         qswepos.lfd = (int) 1;
         qswepos.a   = ratod (aufps.a);
         qswepos.int_pos   = atol (aufps.pnr);
	     QsWePos.dbreadfirst ();
         qswepos.temp      = ratod (aufps.temp);
         qswepos.temp2     = ratod (aufps.temp2);
         qswepos.temp3     = ratod (aufps.temp3);
         qswepos.ph_wert   = ratod (aufps.ph_wert);
		 strcpy (qswepos.pers_nam, sys_ben.pers_nam);

		 for (i = 0; i < qsanzpos; i ++)
		 {
 		          qswepos.lfd = (int) i + 1;
				  strcpy (qswepos.txt, TextePos[i]);
				  qswepos.wrt = YesPos[i];
				  QsWePos.dbupdate ();
		 }
}

void HoleQsMess (int idx) //021209
{
		 char datum [12];
		 extern int aufpidx;

		 sysdate (datum);
		 qswepos.mdn = Mandant;
		 qswepos.fil = Filiale;
		 strcpy (qswepos.lief, "0");
		 sprintf(qswepos.lief, "%ld%", zerl_p.standort * -1);
		 sprintf(qswepos.lief_rech_nr, "%8.0lf", zerl_p.partie);
		 qswepos.dat = dasc_to_long (datum);
         qswepos.lfd = (int) 1;
         qswepos.a   = ratod (aufps.a);
         qswepos.int_pos   = atol (aufps.pnr);
	     if (QsWePos.dbreadfirst () == 0)
		 {
			sprintf(aufparr[idx].temp,"%3.1lf",qswepos.temp);
			sprintf(aufparr[idx].temp2,"%3.1lf",qswepos.temp2);
			sprintf(aufparr[idx].temp3,"%3.1lf",qswepos.temp3);
			sprintf(aufparr[idx].ph_wert,"%4.2lf",qswepos.ph_wert);
		 }
}

void WriteQsMess (void) //110908
/**
Eingaben aus Qualit�tssicherung speichern nur Messwerte nur lfd = 1.
**/
{
/***
		 char datum [12];
		 extern int aufpidx;


         aufpidx = SendMessage  (Getlbox (), LB_GETCURSEL, 0l, 0l);
         memcpy (&aufps, &aufparr[aufpidx], sizeof (struct AUFP_S));
		 sysdate (datum);
		 qswepos.mdn = Mandant;
		 qswepos.fil = Filiale;
		 strcpy (qswepos.lief, we_kopf.lief);
		 strcpy (qswepos.lief_rech_nr, we_kopf.lief_rech_nr);
		 qswepos.dat = dasc_to_long (datum);
         qswepos.lfd = (int) 1;
         qswepos.a   = ratod (aufps.a);
         qswepos.int_pos   = atol (aufps.pnr);
	     QsWePos.dbreadfirst ();
         qswepos.temp      = ratod (aufps.temp);
         qswepos.temp2     = ratod (aufps.temp2);
         qswepos.temp3     = ratod (aufps.temp3);
         qswepos.ph_wert   = ratod (aufps.ph_wert);
		 strcpy (qswepos.pers_nam, sys_ben.pers_nam);
         qswepos.lfd = 1;
	     QsWePos.dbupdate ();
		 if (Schwundabzug > 0 && _a_bas.sw > 0.0 ) //021109
		 {
			 if (qswepos.temp > Schwundabzug || qswepos.temp2 > Schwundabzug || qswepos.temp3 > Schwundabzug ) 
			 {
				sprintf (aufps.txtschwundabzug, "(Schwundabzug: %.1lf %s.)",_a_bas.sw,"%"); //021109
				memcpy (&aufparr[aufpidx],&aufps, sizeof (struct AUFP_S));
			 }
			 else
			 {
				sprintf (aufps.txtschwundabzug, ""); //021109
				memcpy (&aufparr[aufpidx],&aufps, sizeof (struct AUFP_S));
			 }

		 }
**/
}


int doQsPos (void)
/**
Function fuer Button 7
**/
{

		 if (eWindow == NULL) return 0;
         int aufpidx = SendMessage  (Getlbox (), LB_GETCURSEL, 0l, 0l);
         FillQsPos (ratod (aufparr[aufpidx].a));
         if (qsanzpos == 0)
         {
             print_messG (2, "Es sind keine Qualit�tstexte f�r Artikel %.0lf\n"
                             "angelegt", ratod (aufparr[aufpidx].a));
             return 0;
         }
		 QmAktiv = TRUE;
//         Qmkopf->SethMainWnd (eWindow);
         Qmkopf->SethMainWnd (GethListBox ());
		 MainBuInactivQmPos ();
         EnableWindow (eFussWindow, FALSE);
	     InitQm (YesPos, NoPos, qsanzpos);
		 while (TRUE)
		 {
		        Qmkopf->SetParamsEx (UbPos, TextePos, YesPos, NoPos,
			                  WS_POPUP, WS_EX_CLIENTEDGE, TRUE);
		        Qmkopf->Enter ();
		        if (syskey == KEY12)
				{
					if (TestQm (YesPos, NoPos, qsanzpos) == FALSE)
					{
						disp_messG ("Bitte alle Punkte bearbeiten", 2);
						continue;
					}
		           WriteQsPos ();
				}
				break;
		 }
         EnableWindow (eFussWindow, TRUE);
		 MainBuActivQmPos ();
		 QmAktiv = FALSE;
         return 0;
}







// Auswahl ueber Bestellungen

mfont cbestfont = {"Courier New", 120, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                0,
                                NULL};

struct CBE
{
char best_blg [9];
char best_term [12];
char lief_term [12];
};

static struct CBE cbest, *cbestarr = NULL;
static int cbestanz;

static int sortcbest ();
static int sortcbestt ();
static int sortclieft ();

static field _cbestform [] = {
	cbest.best_blg,     8, 0, 0, 1, 0, "%8d",    DISPLAYONLY, 0, 0, 0,
	cbest.best_term,   10, 0, 0,11, 0, "",       DISPLAYONLY, 0, 0, 0,
	cbest.lief_term,   10, 0, 0,23, 0, "",       DISPLAYONLY, 0, 0, 0,
};

static form cbestform = {3, 0, 0, _cbestform, 0, 0, 0, 0, &cbestfont};


static field _bestub [] =
{
        " Best-Nr",        10, 1, 0, 0, 0, "", BUTTON, 0, sortcbest, 0,
        " Best.Term",      12, 1, 0,10, 0, "", BUTTON, 0, sortcbestt, 1,
        " Lief.Term",      12, 1, 0,22, 0, "", BUTTON, 0, sortclieft, 2,
};

static form bestub = {3, 0, 0, _bestub, 0, 0, 0, 0, &cbestfont};

static field _bestvl [] =
{
        "1",                1, 1, 0,10, 0, "", DISPLAYONLY, 0, 0, 0,
        "1",                1, 1, 0,22, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form bestvl = {2, 0, 0, _bestvl, 0, 0, 0, 0, &cbestfont};
static int cbestsort = 1;
static int cbtsort = 1;
static int cltsort = 1;


static int sortcbest0 (const void *elem1, const void *elem2)
{
	        struct CBE *el1;
	        struct CBE *el2;

			el1 = (struct CBE *) elem1;
			el2 = (struct CBE *) elem2;
	        return ((int) (atol (el1->best_blg) - atol (el2->best_blg)) * cbestsort);
}


static int sortcbest (void)
/**
Nach Bestellnummer sortieren.
**/
{
	        qsort (cbestarr, cbestanz, sizeof (struct CBE),
				   sortcbest0);
			cbestsort *= -1;
            ShowNewElist ((char *) cbestarr,
                             cbestanz,
                            (int) sizeof (struct CBE));
			return 0;
}


static int sortcbestt0 (const void *elem1, const void *elem2)
{
	        struct CBE *el1;
	        struct CBE *el2;

			el1 = (struct CBE *) elem1;
			el2 = (struct CBE *) elem2;
            long dat1 = dasc_to_long (el1->best_term);
            long dat2 = dasc_to_long (el2->best_term);
            return (dat1 - dat2) * cbtsort;
//			return ( (int) (strcmp (el1->best_term, el2->best_term)) * cbtsort);
}

static int sortcbestt (void)
/**
Nach Bestelltermin sortieren.
**/
{
	        qsort (cbestarr, cbestanz, sizeof (struct CBE),
				   sortcbestt0);
			cbtsort *= -1;
            ShowNewElist ((char *) cbestarr,
                             cbestanz,
                            (int) sizeof (struct CBE));
			return 0;
}
static int sortclieft0 (const void *elem1, const void *elem2)
{
	        struct CBE *el1;
	        struct CBE *el2;

			el1 = (struct CBE *) elem1;
			el2 = (struct CBE *) elem2;
            long dat1 = dasc_to_long (el1->lief_term);
            long dat2 = dasc_to_long (el2->lief_term);
            return (dat1 - dat2) * cltsort;
//			return ( (int) (strcmp (el1->lief_term, el2->lief_term)) * cltsort);
}


static int sortclieft (void)
/**
Nach Liefertermin sortieren.
**/
{
	        qsort (cbestarr, cbestanz, sizeof (struct CBE),
				   sortclieft0);
			cltsort *= -1;
            ShowNewElist ((char *) cbestarr,
                             cbestanz,
                            (int) sizeof (struct CBE));
			return 0;
}


int dobestchoise ()
{
	    static long bestanz = 0;
		long best_blg;
		char best_term [12];
		char lief_term [12];
		int cursor;
		int idx;
		int i;

		if (scrfx > (double) 1.0)
		{
                cbestfont.FontHeight = (int) (double) ((double) 120 *  scrfx);
		}
		cbestsort = cbtsort = cltsort = 1;
		if (cbestarr)
		{
			    GlobalFree (cbestarr);
		}
        DbClass.sqlout  ((long *) &bestanz, 2, 0);
		DbClass.sqlin   ((short *) &Mandant ,1, 0);
		DbClass.sqlin   ((short *) &Filiale ,1, 0);
		DbClass.sqlin   ((char *) lieferant, 0, 17);
        DbClass.sqlcomm ("select count (*) from best_kopf where mdn = ? "
			                                              "and fil = ? "
														  "and lief = ? "
														  "and bearb_stat <= 2");
		if (bestanz == 0)
		{
			print_messG (2, "Keine Bestellungen f�r Lieferant %s\n"
				            "vorhanden", lieferant);
			return 0;
		}

        cbestarr = (struct CBE *) GlobalAlloc (GMEM_FIXED, bestanz * sizeof (struct CBE));
		if (cbestarr == NULL)
		{
			disp_messG ("Fehler beim Zuweisen von Speicherplatz", 2);
			return 0;
		}

        DbClass.sqlout  ((long *)  &best_blg, 2, 0);
        DbClass.sqlout  ((char *)  best_term, 0, 11);
        DbClass.sqlout  ((char *)  lief_term, 0, 11);
		DbClass.sqlin   ((short *) &Mandant ,1, 0);
		DbClass.sqlin   ((short *) &Filiale ,1, 0);
		DbClass.sqlin   ((char *) lieferant, 0, 17);
        if (druck_zwang)
        {
		cursor = DbClass.sqlcursor ("select best_blg, best_term, lief_term from best_kopf "
			                        "where mdn = ? "
									"and fil = ? "
									"and lief = ? "
									"and bearb_stat = 2 order by best_blg");
        }
        else
        {
		cursor = DbClass.sqlcursor ("select best_blg, best_term, lief_term from best_kopf "
			                        "where mdn = ? "
									"and fil = ? "
									"and lief = ? "
									"and bearb_stat <= 2 order by best_blg");
        }
		i = 0;
		while (DbClass.sqlfetch (cursor) == 0)
		{
			sprintf (cbestarr[i].best_blg, "%ld", best_blg);
			strcpy  (cbestarr[i].best_term, best_term);
			strcpy  (cbestarr[i].lief_term, lief_term);
			i = i + 1;
		}
		DbClass.sqlclose (cursor);

		bestanz = cbestanz = i;
        SetHoLines (1);
        SetVeLines (1);
		PtY = 410;
		if (scrfx > (double) 1.0)
		{
			PtY = (int) (double) ((double) PtY * scrfx);
		}
        syskey = 0;

        EnableWindow (NuBlock.GeteNumWindow (), FALSE);
        idx = ShowWKopf (NuBlock.GeteNumWindow (), (char *) cbestarr, bestanz,
                        (char *) &cbest, sizeof (struct CBE),
                         &cbestform, &bestvl, &bestub);
		CloseControls (&bestub);
        EnableWindows (AufKopfWindow, FALSE);
        EnableWindow (NuBlock.GeteNumWindow (), TRUE);
        SetHoLines (1);
        if (syskey == KEY5 || syskey == KEYESC)
        {
			    GlobalFree (cbestarr);
		        cbestarr = NULL;
                return FALSE;
        }

        strcpy (NuBlock.Geteditbuff (), cbestarr[idx].best_blg);
		GlobalFree (cbestarr);
		cbestarr = NULL;
		Sleep (50);
		display_form (NuBlock.GeteNumWindow (), NuBlock.GetEditForm (), 0, 0);
		NuBlock.SetEditFocus ();

		if (best_ausw_direct)
		{
		       NuBlock.NumEnterBreak ();
		}
		return 0;
}

// Auswahl ueber Lieferanten

mfont clieffont = {"Courier New", 120, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                0,
                                NULL};

struct CLI
{
char lief [17];
char name [37];
};

static struct CLI clief, *cliefarr = NULL;
static int cliefanz;

static int sortclief ();
static int sortcname ();

static field _cliefform [] = {
	clief.lief,        16, 0, 0, 1, 0, "",    DISPLAYONLY, 0, 0, 0,
	clief.name,        36, 0, 0,19, 0, "",    DISPLAYONLY, 0, 0, 0,
};

static form cliefform = {2, 0, 0, _cliefform, 0, 0, 0, 0, &clieffont};


static field _liefub [] =
{
        " Lieferant",      18, 1, 0, 0, 0, "", BUTTON, 0, sortclief, 0,
        " Name",           40, 1, 0,18, 0, "", BUTTON, 0, sortcname, 1,
};

static form liefub = {2, 0, 0, _liefub, 0, 0, 0, 0, &clieffont};

static field _liefvl [] =
{
        "1",                1, 1, 0,18, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form liefvl = {1, 0, 0, _liefvl, 0, 0, 0, 0, &clieffont};
static int cliefsort = 1;
static int cnamesort = 1;


static int sortclief0 (const void *elem1, const void *elem2)
{
	        struct CLI *el1;
	        struct CLI *el2;

			el1 = (struct CLI *) elem1;
			el2 = (struct CLI *) elem2;
			return ( (int) (strcmp (el1->lief, el2->lief)) * cliefsort);
}


static int sortclief (void)
/**
Nach Lieferantennummer sortieren.
**/
{
	        qsort (cliefarr, cliefanz, sizeof (struct CLI),
				   sortclief0);
			cliefsort *= -1;
            ShowNewElist ((char *) cliefarr,
                             cliefanz,
                            (int) sizeof (struct CLI));
			return 0;
}


static int sortcname0 (const void *elem1, const void *elem2)
{
	        struct CLI *el1;
	        struct CLI *el2;

			el1 = (struct CLI *) elem1;
			el2 = (struct CLI *) elem2;
			return ( (int) (strcmp (el1->name, el2->name)) * cnamesort);
}

static int sortcname (void)
/**
Nach Lieferantennamen sortieren.
**/
{
	        qsort (cliefarr, cliefanz, sizeof (struct CLI),
				   sortcname0);
			cnamesort *= -1;
            ShowNewElist ((char *) cliefarr,
                             cliefanz,
                            (int) sizeof (struct CLI));
			return 0;
}
int doliefchoise ()
{
	    static long liefanz = 0;
		char lief [17];
		char name [37];
		int cursor;
		int idx;
		int i;

		if (scrfx > (double) 1.0)
		{
                clieffont.FontHeight = (int) (double) ((double) 120 *  scrfx);
		}
		cliefsort = cnamesort = 1;
		if (cliefarr)
		{
			    GlobalFree (cliefarr);
		}
        DbClass.sqlout  ((long *) &liefanz, 2, 0);
        DbClass.sqlcomm ("select count (*) from lief where lief <> \"-1\"");
		if (liefanz == 0)
		{
			return 0;
		}

        cliefarr = (struct CLI *) GlobalAlloc (GMEM_FIXED, liefanz * sizeof (struct CLI));
		if (cliefarr == NULL)
		{
			disp_messG ("Fehler beim Zuweisen von Speicherplatz", 2);
			return 0;
		}

        DbClass.sqlout  ((char *)  lief, 0, 17);
        DbClass.sqlout  ((char *)  name, 0, 37);
		cursor = DbClass.sqlcursor ("select lief.lief, adr.adr_nam1 from lief,adr "
									"where lief.lief <> \"-1\" and adr.adr = lief.adr "
									"order by lief");
		i = 0;
		while (DbClass.sqlfetch (cursor) == 0)
		{
			strcpy  (cliefarr[i].lief, lief);
			strcpy  (cliefarr[i].name, name);
			i = i + 1;
		}
		DbClass.sqlclose (cursor);

		liefanz = cliefanz = i;
        SetHoLines (1);
        SetVeLines (1);
		PtY = 410;
		if (scrfx > (double) 1.0)
		{
			PtY = (int) (double) ((double) PtY * scrfx);
		}
        syskey = 0;

        EnableWindow (NuBlock.GeteNumWindow (), FALSE);
        idx = ShowWKopf (NuBlock.GeteNumWindow (), (char *) cliefarr, liefanz,
                        (char *) &clief, sizeof (struct CLI),
                         &cliefform, &liefvl, &liefub);
		CloseControls (&liefub);
        EnableWindows (AufKopfWindow, FALSE);
        EnableWindow (NuBlock.GeteNumWindow (), TRUE);
        SetHoLines (1);
        if (syskey == KEY5 || syskey == KEYESC)
        {
			    GlobalFree (cbestarr);
		        cbestarr = NULL;
                return FALSE;
        }

        strcpy (NuBlock.Geteditbuff (), cliefarr[idx].lief);
		GlobalFree (cliefarr);
		cliefarr = NULL;
		Sleep (50);
		display_form (NuBlock.GeteNumWindow (), NuBlock.GetEditForm (), 0, 0);
		NuBlock.SetEditFocus ();
		if (lief_ausw_direct)
		{
			   NuBlock.NumEnterBreak ();
		}
		return 0;
}


// Auswahl ueber Artikel (a_bas)

mfont cartfont = {"Courier New", 120, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                0,
                                NULL};

struct CA
{
char a [14];
char a_bz1 [25];
char a_bz2 [25];
};

static struct CA ca, *caarr = NULL;
static int caanz;

static int sortca ();
static int sortcbz ();

static field _cartform [] = {
	ca.a,         13, 0, 0, 1, 0, "%13.0f", DISPLAYONLY, 0, 0, 0,
	ca.a_bz1,     24, 0, 0,16, 0, "",       DISPLAYONLY, 0, 0, 0,
	ca.a_bz2,     24, 0, 0,41, 0, "",       DISPLAYONLY, 0, 0, 0,
};

static form cartform = {3, 0, 0, _cartform, 0, 0, 0, 0, &cartfont};


static field _artub [] =
{
        "    Artikel",     15, 1, 0, 0, 0, "", BUTTON, 0, sortca, 0,
        " Bezeichnung 1",  25, 1, 0,15, 0, "", BUTTON, 0, sortcbz, 1,
        " Bezeichnung 2",  40, 1, 0,40, 0, "", BUTTON, 0, 0, 2,
};

static form artub = {3, 0, 0, _artub, 0, 0, 0, 0, &cartfont};

static field _artvl [] =
{
        "1",                1, 1, 0,15, 0, "", DISPLAYONLY, 0, 0, 0,
        "1",                1, 1, 0,40, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form artvl = {2, 0, 0, _artvl, 0, 0, 0, 0, &cartfont};
static int casort = 1;
static int cbsort = 1;



struct CCHARGE
{
char a [14];
char a_bz1 [25];
char chargennr [14];
char menge [14];
};

static struct CCHARGE ccharge, *cchargearr = NULL;

static field _cchgform [] = {
	ccharge.a,         13, 0, 0, 1, 0, "%13.0f", DISPLAYONLY, 0, 0, 0,
	ccharge.a_bz1,     24, 0, 0,16, 0, "",       DISPLAYONLY, 0, 0, 0,
	ccharge.chargennr, 14, 0, 0,41, 0, "",       DISPLAYONLY, 0, 0, 0,
	ccharge.menge,     14, 0, 0,54, 0, "%10.3f", DISPLAYONLY, 0, 0, 0,
};

static form cchgform = {4, 0, 0, _cchgform, 0, 0, 0, 0, &cartfont};

static field _chgub [] =
{
        "    Artikel",     15, 1, 0, 0, 0, "", BUTTON, 0, sortca, 0,
        "Bezeichnung 1",  25, 1, 0,15, 0, "", BUTTON, 0, sortcbz, 1,
        "Charge   ",        14, 1, 0,40, 0, "", BUTTON, 0, 0, 2,
        "Menge    ",        14, 1, 0,54, 0, "", BUTTON, 0, 0, 2,
};

static form chgub = {4, 0, 0, _chgub, 0, 0, 0, 0, &cartfont};

static field _chgvl [] =
{
        "1",                1, 1, 0,15, 0, "", DISPLAYONLY, 0, 0, 0,
        "1",                1, 1, 0,40, 0, "", DISPLAYONLY, 0, 0, 0,
        "1",                1, 1, 0,54, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form chgvl = {2, 0, 0, _chgvl, 0, 0, 0, 0, &cartfont};








static int sortca0 (const void *elem1, const void *elem2)
{
	        struct CA *el1;
	        struct CA *el2;

			el1 = (struct CA *) elem1;
			el2 = (struct CA *) elem2;
	        return ((int) (ratod(el1->a) - ratod (el2->a)) * casort);
}


static int sortca (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (caarr, caanz, sizeof (struct CA),
				   sortca0);
			casort *= -1;
            ShowNewElist ((char *) caarr,
                             caanz,
                            (int) sizeof (struct CA));
			return 0;
}


static int sortcbz0 (const void *elem1, const void *elem2)
{
	        struct CA *el1;
	        struct CA *el2;

			el1 = (struct CA *) elem1;
			el2 = (struct CA *) elem2;
			return ( (int) (strcmp (el1->a_bz1, el2->a_bz1)) * cbsort);
}

static int sortcbz (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (caarr, caanz, sizeof (struct CA),
				   sortcbz0);
			cbsort *= -1;
            ShowNewElist ((char *) caarr,
                             caanz,
                            (int) sizeof (struct CA));
			return 0;
}


int doartchoise (void)
/**
Auswahl ueber Artikel
**/
{
	    static long aanz = 0;
		double a;
		char a_bz1 [25];
		char a_bz2 [25];
		int cursor;
		int idx;
		int i;

		if (scrfx > (double) 1.0)
		{
                cartfont.FontHeight = (int) (double) ((double) 120 *  scrfx);
		}
		casort = cbsort = 1;
		if (caarr)
		{
			    GlobalFree (caarr);
		}
		short tier_von = (short) PartieNr;
		short tier_bis = (short) PartieNr;
		if (PartieNr == -1) 
		{
			tier_von = 0;
			tier_bis = 99;
		}
        DbClass.sqlout  ((long *) &aanz, 2, 0);
        DbClass.sqlin  ((short *) &tier_von, 1, 0);
        DbClass.sqlin  ((short *) &tier_bis, 1, 0);
        DbClass.sqlcomm ("select count (*) from a_bas where (a_typ  in (5 ) or "
			                                                "a_typ2 in (5 )) "
															"and a in (select a from a_varb where tier >= ? and tier <= ?)");
					
		if (aanz == 0)
		{
			disp_messG ("Keinen Artikel gefunden", 2);
			/**
  	        EnableWindows (eKopfWindow, FALSE);
            EnableWindows (eFussWindow, FALSE);
            EnableWindows (BackWindow2, FALSE);
            EnableWindows (BackWindow3, FALSE);
            EnableWindows (eWindow, FALSE);
			**/
			return 0;
		}

        caarr = (struct CA *) GlobalAlloc (GMEM_FIXED, aanz * sizeof (struct CA));
		if (caarr == NULL)
		{
			disp_messG ("Fehler beim Zuweisen von Speicherplatz", 2);
			/**
  	        EnableWindows (eKopfWindow, FALSE);
            EnableWindows (eFussWindow, FALSE);
            EnableWindows (BackWindow2, FALSE);
            EnableWindows (BackWindow3, FALSE);
            EnableWindows (eWindow, FALSE);
			**/
			return 0;
		}

        DbClass.sqlout  ((double *) &a, 3, 0);
        DbClass.sqlout  ((char *)  a_bz1, 0, 25);
        DbClass.sqlout  ((char *)  a_bz2, 0, 25);
        DbClass.sqlin  ((short *) &tier_von, 1, 0);
        DbClass.sqlin  ((short *) &tier_bis, 1, 0);
		if (sort_a == FALSE)
		{
		         cursor = DbClass.sqlcursor ("select a, a_bz1, a_bz2 from a_bas "
			                        "where a > 0 and (a_typ in "
									"(5) or a_typ2 in (5)) "
									"and a in ( select a from a_varb where tier >= ? and tier <= ?) "
									" order by a_bz1");
		}
		else
		{
		         cursor = DbClass.sqlcursor ("select a, a_bz1, a_bz2 from a_bas "
			                        "where a > 0 and a_typ in "
									"(5) or a_typ2 in (5)) "
									"and a in ( select a from a_varb where tier >= ? and tier <= ?) "
									"order by a");
		}
		i = 0;
		while (DbClass.sqlfetch (cursor) == 0)
		{
			sprintf (caarr[i].a, "%13.0lf", a);
			strcpy  (caarr[i].a_bz1, a_bz1);
			strcpy  (caarr[i].a_bz2, a_bz2);
			i = i + 1;
		}
		DbClass.sqlclose (cursor);

		aanz = caanz = i;
        SetHoLines (1);
        SetVeLines (1);
		PtY = 410;
		if (scrfx > (double) 1.0)
		{
			PtY = (int) (double) ((double) PtY * scrfx);
		}
        syskey = 0;

        EnableWindow (NuBlock.GeteNumWindow (), FALSE);
        idx = ShowWKopf (NuBlock.GeteNumWindow (), (char *) caarr, aanz,
                        (char *) &ca, sizeof (struct CA),
                         &cartform, &artvl, &artub);
		CloseControls (&artub);
        EnableWindow (NuBlock.GeteNumWindow (), TRUE);
        SetHoLines (1);
		/**
	    EnableWindows (eKopfWindow, FALSE);
        EnableWindows (eFussWindow, FALSE);
        EnableWindows (BackWindow2, FALSE);
        EnableWindows (BackWindow3, FALSE);
        EnableWindows (eWindow, FALSE);
		**/
        if (syskey == KEY5 || syskey == KEYESC)
        {
			    GlobalFree (caarr);
		        caarr = NULL;
                return FALSE;
        }

        strcpy (NuBlock.Geteditbuff (), caarr[idx].a);
		GlobalFree (caarr);
		caarr = NULL;
		Sleep (50);
		display_form (NuBlock.GeteNumWindow (), NuBlock.GetEditForm (), 0, 0);
		NuBlock.SetEditFocus ();
		if (a_ausw_direct)
		{
			   NuBlock.NumEnterBreak ();
		}
		return 0;
}

// ==================== Suche �ber Lager bis Zerlegeeingang ========================
double doartchoiselgr (void)
/**
Auswahl ueber Artikel im Abgangslager
**/
{
	    static long aanz = 0;
		double a;
		char a_bz1 [25];
		char a_bz2 [25];
		int cursor;
		int idx;
		int i;

		if (scrfx > (double) 1.0)
		{
                cartfont.FontHeight = (int) (double) ((double) 120 *  scrfx);
		}
		casort = cbsort = 1;
		if (caarr)
		{
			    GlobalFree (caarr);
		}
		sprintf(bsd_lager,"%ld",LagerAb);
        DbClass.sqlin  ((char *) &akt_mdn, 1, 0);
        DbClass.sqlin  ((char *) bsd_lager, 0, 12);
        DbClass.sqlout  ((long *) &aanz, 2, 0);
        int dsqlstatus = DbClass.sqlcomm ("select mdn from a_bas where bsd_kz = \"J\" "
			               "and a_grund in (select a from bsds where bsds.mdn = ? and bsds.fil = 0 )");

		if (dsqlstatus != 0) 
		{
			aanz = 0;
		}
		else
		{
			if (atoi(EinAusgang) == ZERLEGUNG)
			{
				DbClass.sqlin  ((char *) &akt_mdn, 1, 0);
				DbClass.sqlin  ((char *) bsd_lager, 0, 12);
				DbClass.sqlout  ((long *) &aanz, 2, 0);
				DbClass.sqlcomm ("select count (*) from a_bas where bsd_kz = \"J\" "
							"and a_grund in (select a from bsds where bsds.mdn = ? and bsds.fil = 0 and bsds.bsd_lgr_ort = ? "
							"                and a in (select a from a_mat where mat in (select schnitt_mat from schnitt)))");
			}
			else if (atoi(EinAusgang) == FEINZERLEGUNG)
			{
				DbClass.sqlin  ((char *) &akt_mdn, 1, 0);
				DbClass.sqlin  ((char *) bsd_lager, 0, 12);
				DbClass.sqlout  ((long *) &aanz, 2, 0);
				DbClass.sqlcomm ("select count (*) from a_bas where bsd_kz = \"J\" "
							"and a_grund in (select a from bsds where bsds.mdn = ? and bsds.fil = 0 and bsds.bsd_lgr_ort = ? "
							"                and a in (select a from a_mat where mat in (select zerm_aus_mat from zerm where a_kz = \"VK\" )))");
			}
			else
			{
				DbClass.sqlin  ((char *) &akt_mdn, 1, 0);
				DbClass.sqlin  ((char *) bsd_lager, 0, 12);
				DbClass.sqlout  ((long *) &aanz, 2, 0);
				DbClass.sqlcomm ("select count (*) from a_bas where bsd_kz = \"J\" "
							"and a_grund in (select a from bsds where bsds.mdn = ? and bsds.fil = 0 and bsds.bsd_lgr_ort = ? "
							")");
			}
		}
		if (aanz == 0)
		{
			disp_messG ("Kein Artikel im Lager gefunden", 2);
  	        EnableWindows (eKopfWindow, FALSE);
            EnableWindows (eFussWindow, FALSE);
            EnableWindows (BackWindow2, FALSE);
            EnableWindows (BackWindow3, FALSE);
            EnableWindows (eWindow, FALSE);
			syskey = KEY5;
			return 0;
		}

        caarr = (struct CA *) GlobalAlloc (GMEM_FIXED, aanz * sizeof (struct CA));
		if (caarr == NULL)
		{
			disp_messG ("Fehler beim Zuweisen von Speicherplatz", 2);
  	        EnableWindows (eKopfWindow, FALSE);
            EnableWindows (eFussWindow, FALSE);
            EnableWindows (BackWindow2, FALSE);
            EnableWindows (BackWindow3, FALSE);
            EnableWindows (eWindow, FALSE);
			syskey = KEY5;
			return 0;
		}

        DbClass.sqlin  ((char *) &akt_mdn, 1, 0);
        DbClass.sqlin  ((char *) bsd_lager, 0, 12);
        DbClass.sqlout  ((double *) &a, 3, 0);
        DbClass.sqlout  ((char *)  a_bz1, 0, 25);
        DbClass.sqlout  ((char *)  a_bz2, 0, 25);
		if (sort_a == FALSE)
		{
			if (atoi(EinAusgang) == ZERLEGUNG)
			{

		         cursor = DbClass.sqlcursor ("select a_grund, a_bz1, a_bz2 from a_bas "
			                        "where a_grund > 0 and bsd_kz = \"J\" "
									 "and a_grund in (select a from bsds where bsds.mdn = ? and bsds.fil = 0 "
									 "and bsds.bsd_lgr_ort = ? "
									 "and a in (select a from a_mat where mat in (select schnitt_mat from schnitt))) order by a_bz1 ");
			}
			else if (atoi(EinAusgang) == FEINZERLEGUNG)
			{

		         cursor = DbClass.sqlcursor ("select a_grund, a_bz1, a_bz2 from a_bas "
			                        "where a_grund > 0 and bsd_kz = \"J\" "
									 "and a_grund in (select a from bsds where bsds.mdn = ? and bsds.fil = 0 "
									 "and bsds.bsd_lgr_ort = ? "
									 "and a in (select a from a_mat where mat in (select zerm_aus_mat from zerm where a_kz = \"VK\" ))) order by a_bz1 ");
			}
			else
			{
		         cursor = DbClass.sqlcursor ("select a_grund, a_bz1, a_bz2 from a_bas "
			                        "where a_grund > 0 and bsd_kz = \"J\" "
									 "and a_grund in (select a from bsds where bsds.mdn = ? and bsds.fil = 0 "
									 "and bsds.bsd_lgr_ort = ? "
									 ") order by a_bz1 ");
			}
		}
		else
		{
			if (atoi(EinAusgang) == ZERLEGUNG)
			{
		         cursor = DbClass.sqlcursor ("select a_grund, a_bz1, a_bz2 from a_bas "
			                        "where a_grund > 0 and bsd_kz = \"J\" "
									 "and a_grund in (select a from bsds where  bsds.mdn = ? and bsds.fil = 0 "
									 "and bsds.bsd_lgr_ort = ? "
									 "and a in (select a from a_mat where mat in (select schnitt_mat from schnitt))) order by a ");
			}
			else if (atoi(EinAusgang) == FEINZERLEGUNG)
			{
		         cursor = DbClass.sqlcursor ("select a_grund, a_bz1, a_bz2 from a_bas "
			                        "where a_grund > 0 and bsd_kz = \"J\" "
									 "and a_grund in (select a from bsds where  bsds.mdn = ? and bsds.fil = 0 "
									 "and bsds.bsd_lgr_ort = ? "
									 "and a in (select a from a_mat where mat in (select zerm_aus_mat from zerm where a_kz = \"VK\" ))) order by a ");
			}
			else
			{
		         cursor = DbClass.sqlcursor ("select a_grund, a_bz1, a_bz2 from a_bas "
			                        "where a_grund > 0 and bsd_kz = \"J\" "
									 "and a_grund in (select a from bsds where  bsds.mdn = ? and bsds.fil = 0 "
									 "and bsds.bsd_lgr_ort = ? "
									 ") order by a ");
			}
		}
		i = 0;
		while (DbClass.sqlfetch (cursor) == 0)
		{
			if (i == aanz) break;
			sprintf (caarr[i].a, "%13.0lf", a);
			strcpy  (caarr[i].a_bz1, a_bz1);
			strcpy  (caarr[i].a_bz2, a_bz2);
			i = i + 1;
		}
		DbClass.sqlclose (cursor);

		aanz = caanz = i;
        SetHoLines (1);
        SetVeLines (1);
		PtY = 410;
		if (scrfx > (double) 1.0)
		{
			PtY = (int) (double) ((double) PtY * scrfx);
		}
        syskey = 0;

        EnableWindow (NuBlock.GeteNumWindow (), FALSE);
        idx = ShowWKopf (NuBlock.GeteNumWindow (), (char *) caarr, aanz,
                        (char *) &ca, sizeof (struct CA),
                         &cartform, &artvl, &artub);
		CloseControls (&artub);
        EnableWindow (NuBlock.GeteNumWindow (), TRUE);
        SetHoLines (1);
	    EnableWindows (eKopfWindow, FALSE);
        EnableWindows (eFussWindow, FALSE);
        EnableWindows (BackWindow2, FALSE);
        EnableWindows (BackWindow3, FALSE);
        EnableWindows (eWindow, FALSE);
        if (syskey == KEY5 || syskey == KEYESC)
        {
			    GlobalFree (caarr);
		        caarr = NULL;
                return FALSE;
        }

        strcpy (NuBlock.Geteditbuff (), caarr[idx].a);
		a_wahl = ratod(caarr[idx].a);

		GlobalFree (caarr);
		caarr = NULL;
		Sleep (50);
		display_form (NuBlock.GeteNumWindow (), NuBlock.GetEditForm (), 0, 0);
		NuBlock.SetEditFocus ();
		if (a_ausw_direct)
		{
//			   NuBlock.NumEnterBreak ();
		}
		return 0;
}

// ==================== Suche �ber Lager bei FeinZerlegeeingang (Zerlegung in VK-Artikel) nach Scannen der Chargennummer ========================
double doartchoiseCharge (char *charge)
/**
Auswahl ueber Artikel im Abgangslager
**/
{
	    static long aanz = 0;
		double a;
		char a_bz1 [25];
		char a_bz2 [25];
		int cursor;
		int idx;
		int i;

		if (scrfx > (double) 1.0)
		{
                cartfont.FontHeight = (int) (double) ((double) 120 *  scrfx);
		}
		casort = cbsort = 1;
		if (caarr)
		{
			    GlobalFree (caarr);
		}
		sprintf(bsd_lager,"%ld",LagerAb);
        DbClass.sqlin  ((char *) &akt_mdn, 1, 0);
        DbClass.sqlin  ((char *) bsd_lager, 0, 12);
        DbClass.sqlout  ((long *) &aanz, 2, 0);
        int dsqlstatus = DbClass.sqlcomm ("select mdn from a_bas where bsd_kz = \"J\" "
			               "and a_grund in (select a from bsds where bsds.mdn = ? and bsds.fil = 0 )");

		if (dsqlstatus != 0) 
		{
			aanz = 0;
		}
		else
		{
				DbClass.sqlin  ((char *) &akt_mdn, 1, 0);
				DbClass.sqlin  ((char *) bsd_lager, 0, 12);
				DbClass.sqlout  ((long *) &aanz, 2, 0);
				DbClass.sqlcomm ("select count (*) from a_bas where bsd_kz = \"J\" "
							"and a_grund in (select a from bsds where bsds.mdn = ? and bsds.fil = 0 and bsds.bsd_lgr_ort = ? "
							"                and a in (select a from a_mat where mat in (select zerm_aus_mat from zerm where a_kz = \"VK\" )))");
		}
		if (aanz == 0)
		{
			disp_messG ("Kein Artikel im Lager gefunden", 2);
  	        EnableWindows (eKopfWindow, FALSE);
            EnableWindows (eFussWindow, FALSE);
            EnableWindows (BackWindow2, FALSE);
            EnableWindows (BackWindow3, FALSE);
            EnableWindows (eWindow, FALSE);
			syskey = KEY5;
			return 0;
		}

        caarr = (struct CA *) GlobalAlloc (GMEM_FIXED, aanz * sizeof (struct CA));
		if (caarr == NULL)
		{
			disp_messG ("Fehler beim Zuweisen von Speicherplatz", 2);
  	        EnableWindows (eKopfWindow, FALSE);
            EnableWindows (eFussWindow, FALSE);
            EnableWindows (BackWindow2, FALSE);
            EnableWindows (BackWindow3, FALSE);
            EnableWindows (eWindow, FALSE);
			syskey = KEY5;
			return 0;
		}

		int slen = strlen(clipped(charge));
		charge[slen] = '*';
		charge[slen+1] = 0;
        DbClass.sqlin  ((char *) &akt_mdn, 1, 0);
        DbClass.sqlin  ((char *) bsd_lager, 0, 12);
        DbClass.sqlin  ((char *)  charge, 0, 8);
        DbClass.sqlout  ((double *) &a, 3, 0);
        DbClass.sqlout  ((char *)  a_bz1, 0, 25);
        DbClass.sqlout  ((char *)  a_bz2, 0, 25);
		if (sort_a == FALSE)
		{

		         cursor = DbClass.sqlcursor ("select a_grund, a_bz1, a_bz2 from a_bas "
			                        "where a_grund > 0 and bsd_kz = \"J\" "
									 "and a_grund in (select a from bsds where bsds.mdn = ? and bsds.fil = 0 "
									 "and bsds.bsd_lgr_ort = ? and bsds.chargennr matches ? "
									 "and a in (select a from a_mat where mat in (select zerm_aus_mat from zerm where a_kz = \"VK\" ))) order by a_bz1 ");
		}
		else
		{
		         cursor = DbClass.sqlcursor ("select a_grund, a_bz1, a_bz2 from a_bas "
			                        "where a_grund > 0 and bsd_kz = \"J\" "
									 "and a_grund in (select a from bsds where  bsds.mdn = ? and bsds.fil = 0 "
									 "and bsds.bsd_lgr_ort = ?  and bsds.chargennr matches ? "
									 "and a in (select a from a_mat where mat in (select zerm_aus_mat from zerm where a_kz = \"VK\" ))) order by a ");
		}
		i = 0;
		while (DbClass.sqlfetch (cursor) == 0)
		{
			if (i == aanz) break;
			sprintf (caarr[i].a, "%13.0lf", a);
			strcpy  (caarr[i].a_bz1, a_bz1);
			strcpy  (caarr[i].a_bz2, a_bz2);
			i = i + 1;
		}
		DbClass.sqlclose (cursor);

		aanz = caanz = i;
        SetHoLines (1);
        SetVeLines (1);
		PtY = 410;
		if (scrfx > (double) 1.0)
		{
			PtY = (int) (double) ((double) PtY * scrfx);
		}
        syskey = 0;

        EnableWindow (NuBlock.GeteNumWindow (), FALSE);
        idx = ShowWKopf (NuBlock.GeteNumWindow (), (char *) caarr, aanz,
                        (char *) &ca, sizeof (struct CA),
                         &cartform, &artvl, &artub);
		CloseControls (&artub);
        EnableWindow (NuBlock.GeteNumWindow (), TRUE);
        SetHoLines (1);
	    EnableWindows (eKopfWindow, FALSE);
        EnableWindows (eFussWindow, FALSE);
        EnableWindows (BackWindow2, FALSE);
        EnableWindows (BackWindow3, FALSE);
        EnableWindows (eWindow, FALSE);
        if (syskey == KEY5 || syskey == KEYESC)
        {
			    GlobalFree (caarr);
		        caarr = NULL;
                return FALSE;
        }

        strcpy (NuBlock.Geteditbuff (), caarr[idx].a);
		a_wahl = ratod(caarr[idx].a);

		GlobalFree (caarr);
		caarr = NULL;
		Sleep (50);
		display_form (NuBlock.GeteNumWindow (), NuBlock.GetEditForm (), 0, 0);
		NuBlock.SetEditFocus ();
		if (a_ausw_direct)
		{
//			   NuBlock.NumEnterBreak ();
		}
		return 0;
}


int doChargeChoise (void)
/**
Auswahl ueber Chargen im Abgangslager
**/
{
	    static long aanz = 0;
		double a;
		char a_bz1 [25];
//		char a_bz2 [25];
		char chargennr [14];
		char bsd_lager [13];
		double bsd_gew;
		int cursor;
		int idx;
		int i;
//		a_wahl = double(0);
		a_wahl = ratod(NuBlock.Geteditbuff());
		if (a_wahl == double(0))
		{
			doartchoiselgr(); 
//			return 0;
			if (syskey == KEY5) return 0;
		}
		if (scrfx > (double) 1.0)
		{
                cartfont.FontHeight = (int) (double) ((double) 120 *  scrfx);
		}
		casort = cbsort = 1;
		if (cchargearr)
		{
			    GlobalFree (cchargearr);
		}
		sprintf(bsd_lager,"%ld",LagerAb);
        DbClass.sqlin  ((char *) &akt_mdn, 1, 0);
        DbClass.sqlin  ((char *) bsd_lager, 0, 12);
        DbClass.sqlin  ((double *) &a_wahl, 3, 0);
        DbClass.sqlout  ((long *) &aanz, 2, 0);

        int dsqlstatus = DbClass.sqlcomm ("select bsds.mdn from a_bas,bsds where a_bas.bsd_kz = \"J\" "
								"and a_bas.a_grund = bsds.a and bsds.mdn = ? and bsds.bsd_lgr_ort = ? "
								"and a_bas.a_grund = ? and bsds.fil = 0  ");

		if (dsqlstatus != 0) 
		{
			aanz = 0;
		}
		else
		{
	        DbClass.sqlin  ((char *) &akt_mdn, 1, 0);
		    DbClass.sqlin  ((char *) bsd_lager, 0, 12);
	        DbClass.sqlin  ((double *) &a_wahl, 3, 0);
			DbClass.sqlout  ((long *) &aanz, 2, 0);
			DbClass.sqlcomm ("select count (*) from a_bas,bsds where a_bas.bsd_kz = \"J\" "
								"and a_bas.a_grund = bsds.a and bsds.mdn = ? and bsds.bsd_lgr_ort = ? "
								"and a_bas.a_grund = ? and bsds.fil = 0 ");
		}

		if (aanz == 0)
		{
			disp_messG ("Keine Charge gefunden", 2);
  	        EnableWindows (eKopfWindow, FALSE);
            EnableWindows (eFussWindow, FALSE);
            EnableWindows (BackWindow2, FALSE);
            EnableWindows (BackWindow3, FALSE);
            EnableWindows (eWindow, FALSE);
			return 0;
		}

        cchargearr = (struct CCHARGE *) GlobalAlloc (GMEM_FIXED, aanz * sizeof (struct CCHARGE));
		if (cchargearr == NULL)
		{
			disp_messG ("Kein Artikel im Lager gefunden", 2);
  	        EnableWindows (eKopfWindow, FALSE);
            EnableWindows (eFussWindow, FALSE);
            EnableWindows (BackWindow2, FALSE);
            EnableWindows (BackWindow3, FALSE);
            EnableWindows (eWindow, FALSE);
			return 0;
		}

        DbClass.sqlin  ((double *) &akt_mdn, 1, 0);
        DbClass.sqlin  ((char *) bsd_lager, 0, 12);
        DbClass.sqlin  ((double *) &a_wahl, 3, 0);
        DbClass.sqlout  ((double *) &a, 3, 0);
        DbClass.sqlout  ((char *)  a_bz1, 0, 25);
        DbClass.sqlout  ((char *)  chargennr, 0, 15);
        DbClass.sqlout  ((double *)  &bsd_gew, 3, 0);
		if (sort_a == FALSE)
		{
		         cursor = DbClass.sqlcursor ("select a_bas.a_grund, a_bas.a_bz1, bsds.chargennr, bsds.bsd_gew from a_bas,bsds "
			                        "where a_bas.a_grund > 0 and a_bas.bsd_kz = \"J\" "
								"and a_bas.a_grund = bsds.a and bsds.mdn = ? and bsds.fil = 0 and bsds.bsd_lgr_ort = ? and bsds.bsd_gew >= 0.01 and a_bas.a = ? "
									"order by a_bas.a_bz1 ");
		}
		else
		{
		         cursor = DbClass.sqlcursor ("select a_bas.a_grund, a_bas.a_bz1, bsds.chargennr, bsds.bsd_gew from a_bas,bsds "
			                        "where a_bas.a_grund > 0 and a_bas.bsd_kz = \"J\" "
								"and a_bas.a_grund = bsds.a and bsds.mdn = ? and bsds.fil = 0 and and bsds.bsd_lgr_ort = ?  and bsds.bsd_gew >= 0.01 and a_bas.a = ?  "
									"order by a_bas.a_grund ");
		}
		i = 0;
		while (DbClass.sqlfetch (cursor) == 0)
		{
            chargennr[13] = 0;
			if (i == aanz) break;
			sprintf (cchargearr[i].a, "%13.0lf", a);
			strcpy  (cchargearr[i].a_bz1, a_bz1);
			strcpy  (cchargearr[i].chargennr, chargennr);
			sprintf(cchargearr[i].menge,"%10.3f",bsd_gew);
			i = i + 1;
		}
		DbClass.sqlclose (cursor);

		aanz = caanz = i;
        SetHoLines (1);
        SetVeLines (1);
		PtY = 410;
		if (scrfx > (double) 1.0)
		{
			PtY = (int) (double) ((double) PtY * scrfx);
		}
        syskey = 0;

        EnableWindow (NuBlock.GeteNumWindow (), FALSE);
        idx = ShowWKopf (NuBlock.GeteNumWindow (), (char *) cchargearr, aanz,
                        (char *) &ccharge, sizeof (struct CCHARGE),
                         &cchgform, &chgvl, &chgub);
		CloseControls (&chgub);
        EnableWindow (NuBlock.GeteNumWindow (), TRUE);
        SetHoLines (1);
	    EnableWindows (eKopfWindow, FALSE);
        EnableWindows (eFussWindow, FALSE);
        EnableWindows (BackWindow2, FALSE);
        EnableWindows (BackWindow3, FALSE);
        EnableWindows (eWindow, FALSE);
        if (syskey == KEY5 || syskey == KEYESC)
        {
			    GlobalFree (cchargearr);
		        cchargearr = NULL;
                return FALSE;
        }

		if (atoi(EinAusgang) == ZERLEGUNG)
		{
			strcpy (NuBlock.Geteditbuff (), cchargearr[idx].chargennr);
		}
		else if (atoi(EinAusgang) == FEINZERLEGUNG)
		{
			strcpy (NuBlock.Geteditbuff (), cchargearr[idx].chargennr);
		}
		else
		{
			strcpy (NuBlock.Geteditbuff (), cchargearr[idx].a);
		}
        strcpy (ChargeAuswahl, cchargearr[idx].chargennr);
		GlobalFree (cchargearr);
		caarr = NULL;
		cchargearr = NULL;
		Sleep (50);
		display_form (NuBlock.GeteNumWindow (), NuBlock.GetEditForm (), 0, 0);
		NuBlock.SetEditFocus ();
	    NuBlock.NumEnterBreak ();
		return 0;
}








// Auswahl ueber Artikel (Bezugsquellen)

mfont cbzgfont = {"Courier New", 120, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                0,
                                NULL};

struct CBZG
{
char a [14];
char a_best [17];
char a_bz1 [25];
};

static struct CBZG cbzg, *cbzgarr = NULL;
static int cbzganz;

static int sortcba ();
static int sortcbzg ();
static int sortcbbz ();

static field _cbzgform [] = {
	cbzg.a,         13, 0, 0, 1, 0, "%13.0f", DISPLAYONLY, 0, 0, 0,
	cbzg.a_best,    16, 0, 0,16, 0, "",       DISPLAYONLY, 0, 0, 0,
	cbzg.a_bz1,     24, 0, 0,34, 0, "",       DISPLAYONLY, 0, 0, 0,
};

static form cbzgform = {3, 0, 0, _cbzgform, 0, 0, 0, 0, &cbzgfont};


static field _bzgub [] =
{
        "    Artikel",     15, 1, 0, 0, 0, "", BUTTON, 0, sortcba, 0,
        " LAN",            17, 1, 0,15, 0, "", BUTTON, 0, sortcbzg, 1,
        " Bezeichnung",    40, 1, 0,32, 0, "", BUTTON, 0, sortcbbz, 2,
};

static form bzgub = {3, 0, 0, _bzgub, 0, 0, 0, 0, &cbzgfont};

static field _bzgvl [] =
{
        "1",                1, 1, 0,15, 0, "", DISPLAYONLY, 0, 0, 0,
        "1",                1, 1, 0,32, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form bzgvl = {2, 0, 0, _bzgvl, 0, 0, 0, 0, &cbzgfont};

static int cbasort   = 1;
static int cbzgsort = 1;
static int cbbsort   = 1;


static int sortcba0 (const void *elem1, const void *elem2)
{
	        struct CBZG *el1;
	        struct CBZG *el2;

			el1 = (struct CBZG *) elem1;
			el2 = (struct CBZG *) elem2;
	        return ((int) (ratod(el1->a) - ratod (el2->a)) * cbasort);
}


static int sortcba (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (cbzgarr, cbzganz, sizeof (struct CBZG),
				   sortcba0);
			cbasort *= -1;
            ShowNewElist ((char *) cbzgarr,
                             cbzganz,
                            (int) sizeof (struct CBZG));
			return 0;
}

static int sortcbzg0 (const void *elem1, const void *elem2)
{
	        struct CBZG *el1;
	        struct CBZG *el2;

			el1 = (struct CBZG *) elem1;
			el2 = (struct CBZG *) elem2;
	        return (strcmp (el1->a_best, el2->a_best) * cbzgsort);
}


static int sortcbzg (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (cbzgarr, cbzganz, sizeof (struct CBZG),
				   sortcbzg0);
			cbzgsort *= -1;
            ShowNewElist ((char *) cbzgarr,
                             cbzganz,
                            (int) sizeof (struct CBZG));
			return 0;
}


static int sortcbbz0 (const void *elem1, const void *elem2)
{
	        struct CBZG *el1;
	        struct CBZG *el2;

			el1 = (struct CBZG *) elem1;
			el2 = (struct CBZG *) elem2;
			return ((int) (strcmp (el1->a_bz1, el2->a_bz1)) * cbbsort);
}

static int sortcbbz (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (cbzgarr, cbzganz, sizeof (struct CBZG),
				   sortcbbz0);
			cbbsort *= -1;
            ShowNewElist ((char *) cbzgarr,
                             cbzganz,
                            (int) sizeof (struct CBZG));
			return 0;
}

BOOL GetBzgMdn (double a, short mdn, short fil, char *lief, char *a_best)
{
	    int cursor;
		int dsqlstatus;

	    DbClass.sqlin ((short *) &mdn, 1, 0);
	    DbClass.sqlin ((short *) &fil, 1, 0);
	    DbClass.sqlin ((char *)  lief, 0, 17);
		DbClass.sqlin ((double *) &a, 3, 0);

        DbClass.sqlout  ((char *)  a_best, 0, 17);
		cursor = DbClass.sqlcursor ("select lief_best from lief_bzg "
			                        "where mdn = ? "
									"and fil = ? "
									"and lief = ? "
									"and a = ?");
		if (cursor == -1) return FALSE;

		dsqlstatus = DbClass.sqlfetch (cursor);
		while (dsqlstatus == 100)
		{
			if (fil)
			{
				fil = 0;
			}
			else if (mdn)
			{
				mdn = 0;
			}
			else
			{
                DbClass.sqlclose (cursor);
				return FALSE;
			}
		    dsqlstatus = DbClass.sqlopen (cursor);
		    dsqlstatus = DbClass.sqlfetch (cursor);
		}
        DbClass.sqlclose (cursor);
		return TRUE;
}


int dobzgchoise (void)
/**
Auswahl ueber Artikel
**/
{
	    static long bzganz = 0;
		double a;
		char a_best [17];
		char a_bz1 [25];
		short mdn;
		short fil;
		double akt_a;
		int cursor;
		int idx;
		int i;

		if (scrfx > (double) 1.0)
		{
                cbzgfont.FontHeight = (int) (double) ((double) 120 *  scrfx);
		}
		casort = cbzgsort = cbsort = 1;
		if (cbzgarr)
		{
			    GlobalFree (cbzgarr);
		}
		DbClass.sqlin ((char *) lieferant, 0, 17);
        DbClass.sqlout  ((long *) &bzganz, 2, 0);
        DbClass.sqlcomm ("select count (*) from lief_bzg where lief = ?");
		if (bzganz == 0)
		{
			disp_messG ("Kein Eintrag in Bezugsquellen", 2);
  	        EnableWindows (eKopfWindow, FALSE);
            EnableWindows (eFussWindow, FALSE);
            EnableWindows (BackWindow2, FALSE);
            EnableWindows (BackWindow3, FALSE);
            EnableWindows (eWindow, FALSE);
			return 0;
		}

        cbzgarr = (struct CBZG *) GlobalAlloc (GMEM_FIXED, bzganz * sizeof (struct CBZG));
		if (cbzgarr == NULL)
		{
			disp_messG ("Fehler beim Zuaeisen von Speicherplatz", 2);
  	        EnableWindows (eKopfWindow, FALSE);
            EnableWindows (eFussWindow, FALSE);
            EnableWindows (BackWindow2, FALSE);
            EnableWindows (BackWindow3, FALSE);
            EnableWindows (eWindow, FALSE);
			return 0;
		}

/*
		DbClass.sqlin ((short *) &mdn, 1, 0);
		DbClass.sqlin ((short *) fil, 1, 0);
        DbClass.sqlout  ((double *) &a, 3, 0);
        DbClass.sqlout  ((char *)  a_best, 0, 17);
        DbClass.sqlout  ((char *)  a_bz1, 0, 25);
		cursor = DbClass.sqlcursor ("select lief_bzg.a, lief_bzg.lief_best, a_bas.a_bz1 from "
			                        "lief_bzg,a_bas "
			                        "where (lief_bzg.mdn = ? or lief_bzg.mdn = 0) "
									"and (lief_bzg.fil = ? or lief_bzg.fil = 0) "
									"and lief = ? and lief_bzg.a = a_bas.a "
									"order by a_bz1");
*/

		DbClass.sqlin ((char *) lieferant, 0, 17);
        DbClass.sqlout  ((double *) &a, 3, 0);
        DbClass.sqlout  ((char *)  a_best, 0, 17);
        DbClass.sqlout  ((char *)  a_bz1, 0, 25);
		cursor = DbClass.sqlcursor ("select lief_bzg.a, lief_bzg.lief_best, a_bas.a_bz1 from "
			                        "lief_bzg,a_bas "
									"where lief = ? and lief_bzg.a = a_bas.a "
									"order by a_bz1");
		i = 0;
		akt_a = (double) 0;
		mdn = zerl_k.mdn;
		while (DbClass.sqlfetch (cursor) == 0)
		{
			if (a == akt_a) continue;
			akt_a = a;
			if (GetBzgMdn (a, mdn, fil, lieferant, a_best) == FALSE) continue;
			sprintf (cbzgarr[i].a, "%13.0lf", a);
			strcpy  (cbzgarr[i].a_best, a_best);
			strcpy  (cbzgarr[i].a_bz1,  a_bz1);
			i = i + 1;
		}
		DbClass.sqlclose (cursor);

		bzganz = cbzganz = i;
        SetHoLines (1);
        SetVeLines (1);
		PtY = 410;
		if (scrfx > (double) 1.0)
		{
			PtY = (int) (double) ((double) PtY * scrfx);
		}
        syskey = 0;

        EnableWindow (NuBlock.GeteNumWindow (), FALSE);
        idx = ShowWKopf (NuBlock.GeteNumWindow (), (char *) cbzgarr, bzganz,
                        (char *) &cbzg, sizeof (struct CBZG),
                         &cbzgform, &bzgvl, &bzgub);
		CloseControls (&bzgub);
        EnableWindow (NuBlock.GeteNumWindow (), TRUE);
        SetHoLines (1);
	    EnableWindows (eKopfWindow, FALSE);
        EnableWindows (eFussWindow, FALSE);
        EnableWindows (BackWindow2, FALSE);
        EnableWindows (BackWindow3, FALSE);
        EnableWindows (eWindow, FALSE);
        if (syskey == KEY5 || syskey == KEYESC)
        {
			    GlobalFree (caarr);
		        caarr = NULL;
                return FALSE;
        }

        strcpy (NuBlock.Geteditbuff (), cbzgarr[idx].a_best);
		GlobalFree (cbzgarr);
		cbzgarr = NULL;
		Sleep (50);
		display_form (NuBlock.GeteNumWindow (), NuBlock.GetEditForm (), 0, 0);
		NuBlock.SetEditFocus ();

		if (a_ausw_direct)
		{
			   NuBlock.NumEnterBreak ();
		}

		return 0;
}

void BestpTxt2WepTxt ()
{
}


void BestToWe (long best_blg)
/**
Bestellung in Wareneingang uebernehmen.
**/
{
	     int i;
		 int dsqlstatus;
		 char datum [12];

         memcpy (&zerl_p, &zerl_p_null, sizeof (zerl_p));
		 sysdate (datum);
		 zerl_p.mdn = zerl_k.mdn;
         zerl_p.partie =  zerl_k.partie;
		 i = 0;
		 best_pos.mdn      = zerl_k.mdn;
		 best_kopf.mdn      = zerl_k.mdn;
		 strcpy (best_pos.lief, zerl_k.lief);
		 best_pos.best_blg = best_blg;
		 dsqlstatus = BestPosClass.dbreadfirst_o ("order by a");
		 while (dsqlstatus == 0)
		 {
 			      zerl_p.a         = best_pos.a;
		          dsqlstatus = ZerlpClass.dbreadfirst ();
    			  systime (zeit);
		    	  memcpy (zerl_p.zeit, zeit, 8);
		          zerl_p.bearb = dasc_to_long (datum);
                  zerl_p.we_txt = 0l;
				  if (best_pos.best_txt != 0l)
				  {
					  BestpTxt2WepTxt ();
				  }
/*** 290709 Preise hier nicht mehr holen , sind schon da aus der Bestellung !!!
//                  ReadPr ();
//                  zerl_p.pr_ek         = pr_ek_bto0;
//                  zerl_p.pr_ek_nto     = pr_ek0;
//                  zerl_p.pr_ek_euro    = pr_ek_bto0_euro;
//                  zerl_p.pr_ek_nto_eu  = pr_ek0_euro;

                  Reada_pr ();

				  best_pos.inh = (best_pos.inh == 0.0) ? 1.0 : best_pos.inh;
                  dsqlstatus =   WePreis.preis_holen (zerl_k.mdn,
                                                      zerl_k.fil,
                                                      zerl_k.lief,
                                                      zerl_p.a);
//                  if (lief_bzg.me_kz[0] == '1')   nicht n�tig, stimmt immer
				  {
                               zerl_p.pr_ek        = zerl_p.pr_ek /  best_pos.inh;
                               zerl_p.pr_ek_euro   = zerl_p.pr_ek_euro /  best_pos.inh;
                               zerl_p.pr_ek_nto    = zerl_p.pr_ek_nto /  best_pos.inh;
                               zerl_p.pr_ek_nto_eu = pr_ek0_euro /  best_pos.inh;
              				   zerl_p.inh          = 1.0;
				  }


**/

//                  zerl_p.pr_fil_ek = GetPrFilEk ();
//                  zerl_p.pr_vk     = GetPrVk ();
				  ZerlpClass.dbupdate (atoi(EinAusgang));
				  i++;
				  dsqlstatus = BestPosClass.dbread_o ();
		 }
		 BestPosClass.dbclose_o ();
		 best_kopf.bearb_stat = VERBUCHT;
		 BestKopfClass.dbupdate ();
}


int menfunc8 (void)
/**
Function fuer Button 8
**/
{
         HWND aktfocus;
		 int dsqlstatus;
		 char best_blg [10];
		 bin_in_menfunc = 8;



  if (strlen(zerl_k.lief) == 0)
  {
         stopauswahl = FALSE;

//		 query_changed = TRUE;
//		 if (rdoptimize)
//		 {
                  EnterTyp = AUSWAHL;
                  AuftragKopf (AUSWAHL);
				  while (stopauswahl == FALSE)
				  {
                         AuftragKopf (AUSWAHL);
				  }
                  InitAufKopf ();
                  aktfocus = GetFocus ();
                  AktButton = GetAktButton ();
				  strcpy(zerl_k.lief,"");
				  return 0;
//		 }

         AuftragKopf (AUSWAHL);
         if (syskey == KEY5)
         {
                       InitAufKopf ();
         }
         else
         {
                      EnterTyp = ENTER;
                      AuftragKopf (ENTER);
         }
         aktfocus = GetFocus ();
         AktButton = GetAktButton ();
   	     strcpy(zerl_k.lief,"");
         return 0;
  }




		 while (TRUE)
		 {
                 NuBlock.SetChoise (dobestchoise, 0);
				 strcpy (best_blg, "0");
                 NuBlock.EnterNumBox (AufKopfWindow,"  Bestell-Nr  ",
                                               best_blg, 9, "%8d",
	      									  EntNuProc);
		         if (syskey != KEY5 && syskey != KEYESC)
				 {
					         strcpy(bestellhinweis,"");
  		                     best_kopf.mdn = zerl_k.mdn;
		                     best_kopf.best_blg = atol (best_blg);
							 strcpy (best_kopf.lief, zerl_k.lief);
		                     dsqlstatus = BestKopfClass.dbreadfirst ();
		                     if (dsqlstatus != 0 || (druck_zwang &&
								                     best_kopf.bearb_stat != 2))
							 {
				                   print_messG (2, "Bestellung %ld nicht gefunden", best_kopf.best_blg);
							 }
		                     else if (dsqlstatus != 0 || best_kopf.bearb_stat > 2)
							 {
				                   print_messG (2, "Bestellung %ld nicht gefunden", best_kopf.best_blg);
							 }
			                 else
							 {
								   sprintf(bestellhinweis,"Bestellbeleg: %ld   %s",best_kopf.best_blg,best_kopf.freifeld1); //100908 
								   beginwork ();
 			                       BestToWe (atol (best_blg));
                                   if (QsZwang)
                                   {
                                       menfunc7 ();
                                   }
                                   WriteLS ();
								   break;
							 }
				 }
				 else
				 {
					 break;
				 }
		 }
         aktfocus = GetFocus ();
         SetColFocus (MainButton.mask[0].feldid);
         AktButton = GetAktButton ();
         return 0;
}


int menfunc9 (void)
/**
Function fuer Button 9
**/
{
         HWND aktfocus;

		 bin_in_menfunc = 9;
         aktfocus = GetFocus ();
         AuswahlWiegArt ();
         AktButton = GetAktButton ();
         return 0;
}
/**
int menfunc10 (void)
{
         HWND aktfocus;

         aktfocus = GetFocus ();
//         AuswahlAbteilung ();
         AuswahlWiegArt ();
         AktButton = GetAktButton ();
         return 0;
}
***/
int menfunc10 (void)
/**
Function fuer Button 9
**/
{
         HWND aktfocus;
		 int Ausw = 0;
		 bin_in_menfunc = 10;
         aktfocus = GetFocus ();
         Ausw = AuswahlAbteilung () ;
		 if (EinAusgang)
		 {
			sprintf(EinAusgang,"%ld",Ausw);
		 }
		 else
		 {
            EinAusgang = (char *) GlobalAlloc (GMEM_FIXED , 2);
			sprintf(EinAusgang,"%ld",Ausw);
		 }

		 if (Ausw == EINGANG)
		 {
			sprintf (EinAusgangtxt, "Zerlegeeingang");
		 }
		 else if (Ausw == AUSGANG)
		 {
			sprintf (EinAusgangtxt, "Zerlegeausgang");
		 }
		 else if (Ausw == UMLAGERUNG)
		 {
			sprintf (EinAusgangtxt, "Umlagerung");
		 }
		 else if (Ausw == ZERLEGUNG)
		 {
			sprintf (EinAusgangtxt, "Zerlegung");  //GROM-6
		 }
		 else if (Ausw == FEINZERLEGUNG)
		 {
			sprintf (EinAusgangtxt, "FeinZerlegung");  //GROM-6
		 }
		 else if (Ausw == PRODUKTIONSSTART )
		 {
			sprintf (EinAusgangtxt, "Produktionsstart"); 
		 }
		 else if (Ausw == PRODUKTIONSFORTSCHRITT  )
		 {
			sprintf (EinAusgangtxt, "Produktionsfortschritt"); 
		 }
		 else if (Ausw == BUCHENFERTIGLAGER  )
		 {
			sprintf (EinAusgangtxt, "Buchenfertiglager"); 
		 }
		 else 
		 {
			sprintf (EinAusgangtxt, "Auswahl Ein/Ausgang");
		 }
		if (EinAusgang)
		{
			if (atoi(EinAusgang) == EINGANG)
			{
				   LiefEnterAktiv = 0;
			       sprintf (Einstiegtxt, "%s", "Zerlegepartie");
			       sprintf (Auswahltxt, "%s", "Partieauswahl");
	               LagerAb = ZELagerAb;
			       sprintf (LagerAbtxt, "%s %hd", LAbtxt, LagerAb);
                   LagerZu = ZELagerZu;
			       sprintf (LagerZutxt, "%s %hd", LZutxt, LagerZu);
				   ActivateColButton (BackWindow2, &MainButton, 2, 1, 1);  
				   testform = zereinform; //Liste
				   testub = zereinub;
				   testvl = zereinvl;
				   aufform = StdAuswForm; //Auswahlliste
				   aufub = StdAuswub ;
				   aufvl = StdAuswvl;
			}
			else if (atoi(EinAusgang) == AUSGANG)
			{
				   LiefEnterAktiv = 0;
			       sprintf (Einstiegtxt, "%s", "Zerlegepartie");
			       sprintf (Auswahltxt, "%s", "Partieauswahl");
                   LagerAb = ZALagerAb;
			       sprintf (LagerAbtxt, "%s %hd", LAbtxt, LagerAb);
                   LagerZu = ZALagerZu;
			       sprintf (LagerZutxt, "%s %hd", LZutxt, LagerZu);
				   ActivateColButton (BackWindow2, &MainButton, 2, 1, 1);  
				   testform = zerausform;
				   testub = zerausub;
				   testvl = zerausvl;
				   aufform = StdAuswForm; //Auswahlliste
				   aufub = StdAuswub ;
				   aufvl = StdAuswvl;
			}
			else if (atoi(EinAusgang) == UMLAGERUNG)
			{
				   LiefEnterAktiv = 1;
			       sprintf (Einstiegtxt, "%s", "Start Umlagerung");
			       sprintf (Auswahltxt, "%s", "Auswahl");
                   LagerAb = UMLagerAb;
			       sprintf (LagerAbtxt, "%s %hd", LAbtxt, LagerAb);
                   LagerZu = UMLagerZu;
			       sprintf (LagerZutxt, "%s %hd", LZutxt, LagerZu);
				   ActivateColButton (BackWindow2, &MainButton, 2, -1, 1);  
				   testform = umlform;
				   testub = umlub;
				   testvl = umlvl;
				   aufform = StdAuswForm; //Auswahlliste
				   aufub = StdAuswub ;
				   aufvl = StdAuswvl;
			}
			else if (atoi(EinAusgang) == ZERLEGUNG)    
			{
				   LiefEnterAktiv = 1;
			       sprintf (Einstiegtxt, "%s", "Chargeneingabe");
			       sprintf (Auswahltxt, "%s", "Auswahl");
                   LagerAb = ZRLagerAb;
			       sprintf (LagerAbtxt, "%s %hd", LAbtxt, LagerAb);
                   LagerZu = ZRLagerZu;
			       sprintf (LagerZutxt, "%s %hd", LZutxt, LagerZu);
				   ActivateColButton (BackWindow2, &MainButton, 2, -1, 1);  
				   testform = zerform;
				   testub = zerub;
				   testvl = zervl;
				   aufform = StdAuswForm; //Auswahlliste
				   aufub = StdAuswub;
				   aufvl = StdAuswvl;
			}
			else if (atoi(EinAusgang) == FEINZERLEGUNG)
			{
				   LiefEnterAktiv = 1;
			       sprintf (Einstiegtxt, "%s", "Chargeneingabe");
			       sprintf (Auswahltxt, "%s", "Auswahl");
                   LagerAb = FZRLagerAb;
			       sprintf (LagerAbtxt, "%s %hd", LAbtxt, LagerAb);
                   LagerZu = FZRLagerZu;
			       sprintf (LagerZutxt, "%s %hd", LZutxt, LagerZu);
				   ActivateColButton (BackWindow2, &MainButton, 2, -1, 1);  
				   testform = zerform;
				   testub = zerub;
				   testvl = zervl;
				   aufform = StdAuswForm; //Auswahlliste
				   aufub = StdAuswub;
				   aufvl = StdAuswvl;
			}

			else if (atoi(EinAusgang) == PRODUKTIONSSTART ) 
			{
				   LiefEnterAktiv = 0;
			       sprintf (Einstiegtxt, "%s", "Starte Produktion");
			       sprintf (Auswahltxt, "%s", "Auswahl");
                   LagerAb = A6_LagerAb;
			       sprintf (LagerAbtxt, "%s %hd", LAbtxt, LagerAb);
                   LagerZu = A6_LagerZu;
			       sprintf (LagerZutxt, "%s %hd", LZutxt, LagerZu);
				   ActivateColButton (BackWindow2, &MainButton, 2, 1, 1);  
				   testform = prodform;
				   testub = produb;
				   testvl = prodvl;
				   aufform = AuswProdForm; //Auswahlliste
				   aufub = AuswProdub;
				   aufvl = AuswProdvl;
			}
			else if (atoi(EinAusgang) == PRODUKTIONSFORTSCHRITT )
			{
				   LiefEnterAktiv = 0;
			       sprintf (Einstiegtxt, "%s", "Fortschritt scannen");
			       sprintf (Auswahltxt, "%s", "Auswahl");
                   LagerAb = A7_LagerAb;
			       sprintf (LagerAbtxt, "%s %hd", LAbtxt, LagerAb);
                   LagerZu = A7_LagerZu;
			       sprintf (LagerZutxt, "%s %hd", LZutxt, LagerZu);
				   ActivateColButton (BackWindow2, &MainButton, 2, 1, 1);  
				   testform = fortschrittform;
				   testub = fortschrittub;
				   testvl = fortschrittvl;
				   aufform = AuswProdForm; //Auswahlliste
				   aufub = AuswProdub;
				   aufvl = AuswProdvl;
			}
			else if (atoi(EinAusgang) == BUCHENFERTIGLAGER )
			{
				   LiefEnterAktiv = 0;
			       sprintf (Einstiegtxt, "%s", "Buchen in Fertiglager");
			       sprintf (Auswahltxt, "%s", "Auswahl");
                   LagerAb = A8_LagerAb;
			       sprintf (LagerAbtxt, "%s %hd", LAbtxt, LagerAb);
                   LagerZu = A8_LagerZu;
			       sprintf (LagerZutxt, "%s %hd", LZutxt, LagerZu);
				   ActivateColButton (BackWindow2, &MainButton, 2, 1, 1);  
				   testform = buchform;
				   testub = buchub;
				   testvl = buchvl;
				   aufform = AuswProdForm; //Auswahlliste
				   aufub = AuswProdub;
				   aufvl = AuswProdvl;
			}

		     if ( lgr_class.lese_lgr_bz (LagerAb) == 0) strcpy (LgrAb_krz,lgr.lgr_bz); 
	         if ( lgr_class.lese_lgr_bz (LagerZu) == 0) strcpy (LgrZu_krz,lgr.lgr_bz);
		     display_field (BackWindow2, &MainButton.mask[ItPos (&MainButton, (char *) &BLagerZu)]);
		     display_field (BackWindow2, &MainButton.mask[ItPos (&MainButton, (char *) &BLagerAb)]);
		     display_field (BackWindow2, &MainButton.mask[ItPos (&MainButton, (char *) &Zerlegepartie)]);
		     display_field (BackWindow2, &MainButton.mask[ItPos (&MainButton, (char *) &Partieauswahl)]);
		}



	     display_field (BackWindow2, &MainButton.mask[ItPos (&MainButton, (char *) &ColEinAusgang)]);
         AktButton = GetAktButton ();
         return 0;
}


void menue_end (void)
/**
Menue beenden.
**/
{
         PostQuitMessage (0);
}

int menfunc0 (void)
/**
Function fuer Button 10
**/
{
         HWND aktfocus;
         field *feld;

		 bin_in_menfunc = 0;
         feld = &MainButton2.mask[3];
         aktfocus = GetFocus ();
         menue_end ();
         return 0;
}

int func1 (void)
/**
Button fuer Pfeil links bearbeiten.
**/
{
         field *feld;

         if (KomplettAktiv)
         {
                 // PostMessage (NULL, WM_CHAR,  (WPARAM) ' ', 0l);
                 SelectAktCtr ();
                 return 1;
         }

         if (ListeAktiv)
         {
                 PostMessage (eWindow, WM_KEYDOWN,  (WPARAM) VK_LEFT, 0l);
                 return 1;
         }

         if (PtListeAktiv)
         {
                 PostMessage (PtWindow, WM_KEYDOWN,  (WPARAM) VK_LEFT, 0l);
                 return 1;
         }

         if (NumEnterAktiv)
         {
                 SetFocus (NumEnterWindow);
                 SendMessage (NumEnterWindow, WM_KEYDOWN,
                                     (WPARAM) VK_LEFT, 0l);
                 return 1;
         }

         feld = GetAktField ();
         SetFocus (feld->feldid);
         PostMessage (GetParent (feld->feldid), WM_KEYDOWN,
                                            (WPARAM) VK_LEFT,
                                            0l);
         feld = GetAktField ();
         SetColFocus (feld->feldid);
         display_form (BackWindow3, &MainButton2, 0, 0);
         AktButton = GetAktButton ();
         return 0;
}

int func2 (void)
/**
Button fuer Pfeil oben bearbeiten.
**/
{
         field *feld;

         if (KomplettAktiv)
         {
                 PostMessage (AktivDialog, WM_KEYDOWN,  (WPARAM) VK_UP, 0l);
                 return 1;
         }
         if (ListeAktiv)
         {
                 PostMessage (eWindow, WM_KEYDOWN,  (WPARAM) VK_UP, 0l);
                 return 1;
         }
         if (PtListeAktiv)
         {
                 PostMessage (PtWindow, WM_KEYDOWN,  (WPARAM) VK_UP, 0l);
                 return 1;
         }
         if (NumEnterAktiv)
         {
                 SetFocus (NumEnterWindow);
                 SendMessage (NumEnterWindow, WM_KEYDOWN,
                                     (WPARAM) VK_UP, 0l);
                 return 1;
         }
         feld = GetAktField ();
         SetFocus (feld->feldid);
         PostMessage (GetParent (feld->feldid), WM_KEYDOWN,
                                            (WPARAM) VK_UP,
                                            0l);
         feld = GetAktField ();
         SetColFocus (feld->feldid);
         display_form (BackWindow3, &MainButton2, 0, 0);
         AktButton = GetAktButton ();
         return 0;
}

int func3 (void)
/**
Button fuer Pfeil unten bearbeiten.
**/
{
         field *feld;

         if (KomplettAktiv)
         {
                 PostMessage (AktivDialog, WM_KEYDOWN,  (WPARAM) VK_DOWN, 0l);
                 return 1;
         }
         if (ListeAktiv)
         {
                 PostMessage (eWindow, WM_KEYDOWN,  (WPARAM) VK_DOWN, 0l);
                 return 1;
         }
         if (PtListeAktiv)
         {
                 PostMessage (PtWindow, WM_KEYDOWN,  (WPARAM) VK_DOWN, 0l);
                 return 1;
         }
         if (NumEnterAktiv)
         {
                 SetFocus (NumEnterWindow);
                 SendMessage (NumEnterWindow, WM_KEYDOWN,
                                     (WPARAM) VK_DOWN, 0l);
                 return 1;
         }
         feld = GetAktField ();
         SetFocus (feld->feldid);
         PostMessage (GetParent (feld->feldid), WM_KEYDOWN,
                                            (WPARAM) VK_DOWN,
                                             0l);
         feld = GetAktField ();
         SetColFocus (feld->feldid);
         display_form (BackWindow3, &MainButton2, 0, 0);
         AktButton = GetAktButton ();
         return 0;
}

int func4 (void)
/**
Button fuer Pfeil rechts bearbeiten.
**/
{
         field *feld;

         if (KomplettAktiv)
         {
                 PostMessage (AktivDialog, WM_KEYDOWN,  (WPARAM) ' ', 0l);
                 return 1;
         }

         if (ListeAktiv)
         {
                 PostMessage (eWindow, WM_KEYDOWN,  (WPARAM) VK_RIGHT, 0l);
                 return 1;
         }
         if (PtListeAktiv)
         {
                 PostMessage (PtWindow, WM_KEYDOWN,  (WPARAM) VK_RIGHT, 0l);
                 return 1;
         }
         if (NumEnterAktiv)
         {
                 SetFocus (NumEnterWindow);
                 SendMessage (NumEnterWindow, WM_KEYDOWN,
                                     (WPARAM) VK_RIGHT, 0l);
                 return 1;
         }

         feld = GetAktField ();
         SetFocus (feld->feldid);
         PostMessage (GetParent (feld->feldid), WM_KEYDOWN,
                                            (WPARAM) VK_RIGHT,
                                            0l);
         feld = GetAktField ();
         SetColFocus (feld->feldid);
         display_form (BackWindow3, &MainButton2, 0, 0);
         AktButton = GetAktButton ();
         return 0;
}

void backmenue (char *men)
/**
Vorhergehendes Menue ermitteln.
**/
{
          char *pos;

          menue_end ();
          return;

          pos = men + strlen (men) - 1;

          for (; pos >= men; pos --)
          {
                    if (*pos != '0')
                     {
                            *pos = '0';
                            break;
                    }
          }
}


int func5 (void)  
/**
Function fuer Zurueckbutton bearbeiten
**/
{
         if (PtListeAktiv)
         {
                 PostMessage (PtWindow, WM_KEYDOWN,  (WPARAM) VK_F5, 0l);
                 return 1;
         }

         if (ListeAktiv)
         {
		    if (atoi(EinAusgang) == PRODUKTIONSSTART  && bin_in_menfunc == 2 && InScanning == 0) //FISCH-5
			{
				if (abfragejnG (NULL, "Produktionscharge komplett ?\n" ,"J"))
				{
					if (SchreibeProdAbschluss () == FALSE)
					{
					}
				}
			}
		    if (atoi(EinAusgang) == PRODUKTIONSFORTSCHRITT   && bin_in_menfunc == 2 && InScanning == 0) //FISCH-5
			{
					//zerl_k.partie_status von 0 auf 1   
					// zerl_p.fortschritt auf 1 setzen
//Hier: zerl_p.kng auf BUCHENFERTIGLAGER stellen, und zerl_k.partie_status auf 2
					beginwork ();
					zerl_p.mdn = zerl_k.mdn;
					zerl_p.partie = zerl_k.partie;
					zerl_p.kng = PRODUKTIONSFORTSCHRITT;
					int dkng = BUCHENFERTIGLAGER ;

					dsqlstatus = ZerlpClass.dbreadfirst_f ("order by status,bearb desc, zeit desc");
					while (dsqlstatus == 0)
					{
						short maxf = MaxFortschritt (zerl_p.a);
						if (zerl_p.fortschritt == maxf)
						{
							zerl_p.kng = PRODUKTIONSFORTSCHRITT; 
					        DbClass.sqlin   ((long *) &dkng, 2, 0);
					        DbClass.sqlin   ((short *) &zerl_p.mdn, 1, 0);
							DbClass.sqlin   ((double *) &zerl_p.partie, 3, 0);
							DbClass.sqlin   ((double *) &zerl_p.a, 3, 0);
							DbClass.sqlin   ((short *) &zerl_p.kng, 1, 0);
							DbClass.sqlin   ((long *) &zerl_p.int_pos, 2, 0);
							DbClass.sqlcomm ("update zerl_p set kng = ?, buch_gew = nto_gew, nto_gew = 0, charge = partie where mdn = ? and partie = ? and a = ? and kng = ? and int_pos = ? ");
						}
						dsqlstatus = ZerlpClass.dbread_f ();
					}
					zerl_k.partie_status = 2;
					ZerlkClass.dbupdate ();
					commitwork ();
			}



	         PostMessage (eWindow, WM_KEYDOWN,  (WPARAM) VK_F5, 0l);
		     return 1;
         }

         if (KomplettAktiv)
         {
                 PostMessage (AktivDialog, WM_KEYDOWN,  (WPARAM) VK_F5, 0l);
                 return 1;
         }
         if (NumEnterAktiv)
         {
                 SetFocus (NumEnterWindow);
                 PostMessage (NumEnterWindow, WM_KEYDOWN,
                                     (WPARAM) VK_F5, 0l);
                 return 1;
         }

         if (KopfEnterAktiv)
         {
                 PostMessage (AufKopfWindow, WM_KEYDOWN,
                                     (WPARAM) VK_F5, 0l);
                 return 1;
         }


         backmenue (menue.prog);
         return 0;
}

int dokey5 (void)
/**
Aktion bei Taste F5 (zurueck)
**/
{
         field *feld;

         menue_end ();
         return 0;

         backmenue (menue.prog);
         feld = &MainButton.mask[0];
         SetFocus (feld->feldid);
         AktButton = GetAktButton ();
         display_form (BackWindow2, &MainButton, 0, 0);
         UpdateWindow (BackWindow2);
         display_form (BackWindow3, &MainButton2, 0, 0);
         AktButton = GetAktButton ();
         return 0;
}


int func6 (void)
/**
Function fuer OK setzen.
**/
{
	     if (QmAktiv)
		 {
                  PostMessage (AufKopfWindow,
                               WM_KEYDOWN,  (WPARAM) VK_F12, 0l);
				  return 1;
		 }

         if (PtListeAktiv)
         {
				  PROD_MainButtons_inaktiv ();

				  syskey = KEYCR;
                  PostMessage (PtWindow,
                               WM_KEYDOWN,  (WPARAM) VK_RETURN, 0l);

                 return 1;
         }


         if (KomplettAktiv)
         {
                 SelectAktCtr ();
                 return 1;
         }

         if (KopfEnterAktiv && ListeAktiv == 0)
         {
                 if (EnterTyp == SUCHEN)
                 {
                        PostMessage (AufKopfWindow,
                               WM_KEYDOWN,  (WPARAM) VK_F12, 0l);
                 }
                 else if (EnterTyp == AUSWAHL)
                 { 
					  int ls_direct = liste_direct;
					  liste_direct = 0;

						     strcpy(bestellhinweis,"");
  		                     best_kopf.mdn = akt_mdn;
		                     best_kopf.fil = akt_fil;
		                     best_kopf.best_blg = best_kopf.best_blg;
							 strcpy (best_kopf.lief, lieferant);
		                     dsqlstatus = BestKopfClass.dbreadfirst ();
							 if (dsqlstatus == 0)
							 {
								sprintf(bestellhinweis,"Bestellbeleg: %ld   %s",best_kopf.best_blg,best_kopf.freifeld1);
							 }

					  if(ReadLS() != -1) //100908 wenn abgeschlossen, nicht nochmal schreiben
					  {
						if (QsZwang)
						{
								menfunc7 ();
								if (syskey == KEY5)   //100908 bei Abbruch mit "Zur�ck"  darf nicht geschrieben werden
								{
			    					  liste_direct = ls_direct;
				                      PostMessage (AufKopfWindow,
					                   WM_KEYDOWN,  (WPARAM) VK_RETURN, 0l);
									  return 1;
								}
						}
						beginwork ();
						BestToWe (best_kopf.best_blg);
						WriteLS ();
    					  liste_direct = ls_direct;

	                      PostMessage (AufKopfWindow,
                               WM_KEYDOWN,  (WPARAM) VK_F12, 0l);
					  }
					  else
					  {
  						liste_direct = ls_direct;

                        PostMessage (AufKopfWindow,
                               WM_KEYDOWN,  (WPARAM) VK_RETURN, 0l);  //100908 herVK_RETURN, da sonst WriteLS 
					  }
                 }
                 else
                 {
                        PostMessage (AufKopfWindow,
                               WM_KEYDOWN,  (WPARAM) VK_RETURN, 0l);
                 }
                 return 1;
         }

         if (LiefEnterAktiv && ListeAktiv == 0) 
         {
                 syskey = KEY12;
                 PostMessage (AufKopfWindow,
                               WM_KEYDOWN,  (WPARAM) VK_F12, 0l);
                 return 1;
         }

         if (ListeAktiv == 0)
         {
                 return 0;
         }

         PostMessage (eWindow, WM_KEYDOWN,  (WPARAM) VK_RETURN, 0l);
         return 0;
}

int func7 (void)
/**
Function fuer OK setzen.
**/
{
         extern BOOL StartWiegen;
         if (KomplettAktiv) return 0;

         memcpy (&aufps, &aufp_null, sizeof (aufps));
         StartWiegen = TRUE;
//         beginwork ();
         ArtikelWiegen (LogoWindow);
//         commitwork ();
         StartWiegen = FALSE;
         memcpy (&aufps, &aufparr[0], sizeof (aufps));
         
         return 0;
}

int func8 (void)
/**
Function fuer Benutzer bearbeiten
**/
{
         if (ListeAktiv == 0) return 0;
         if (KomplettAktiv) return 0;

         EnableWindow (eKopfWindow, FALSE);
         EnableWindow (eFussWindow, FALSE);
         SetKomplett ();
         EnableWindow (eKopfWindow, TRUE);
         EnableWindow (eFussWindow, TRUE);
         break_list ();
         return 0;
}


/*
void printlogo (HBITMAP hbr, int x, int y, DWORD mode)
{

        int bmy;
        int bmx;
        HDC      hdc;

        GetObject (hbr, sizeof (BITMAP), &bm);
        bmy = bm.bmHeight / 2;
        bmx = bm.bmWidth / 2;
        hdc = fithdc;
        hdcMemory  = CreateCompatibleDC(hdc);
        hbmOld = SelectObject (hdcMemory, hbr);

        SetViewportOrgEx (hdc, bmx, bmy, NULL);
        SetStretchBltMode (hdc, COLORONCOLOR);
        StretchBlt (hdc, -bmx + xstretch / 2, -bmy + ystretch / 2,
                         bm.bmWidth - xstretch, bm.bmHeight - ystretch,
                         hdcMemory,0, 0,
                         bm.bmWidth, bm.bmHeight, mode);

        SelectObject (hdcMemory, hbmOld);

        DeleteDC (hdcMemory);
        if (sign == 1)
        {
            ystretch += (plus * logory);
            if (ystretch >= bm.bmHeight * 2)
            {
                        ystretch = bm.bmHeight * 2;
                        sign = -1;
            }
        }
        else if (sign == -1)
        {
            ystretch -= (plus * logory);
            if (ystretch <= 0)
            {
                        ystretch = 0;
                        sign = 1;
            }
        }
        if (signx == 1)
        {
            xstretch += (xplus * logorx);
            if (xstretch >= bm.bmWidth * 2)
            {
                        xstretch = bm.bmWidth * 2;
                        signx = -1;
            }
        }
        else if (signx == -1)
        {
            xstretch -= (xplus * logorx);
            if (xstretch <= 0)
            {
                        xstretch = 0;
                        signx = 1;
            }
        }
}
*/

void paintlogo (HDC hdc, HBITMAP hbr, int x, int y, DWORD mode)
{

        int bmy;
        int bmx;

        GetObject (hbr, sizeof (BITMAP), &bm);
        bmy = bm.bmHeight / 2;
        bmx = bm.bmWidth / 2;
        hdcMemory  = CreateCompatibleDC(hdc);
        hbmOld = SelectObject (hdcMemory, hbr);

        SetViewportOrgEx (hdc, bmx, bmy, NULL);
        SetStretchBltMode (hdc, COLORONCOLOR);
        StretchBlt (hdc, -bmx + xstretch / 2, -bmy + ystretch / 2,
                         bm.bmWidth - xstretch, bm.bmHeight - ystretch,
                         hdcMemory,0, 0,
                         bm.bmWidth, bm.bmHeight, mode);

        SelectObject (hdcMemory, hbmOld);

        DeleteDC (hdcMemory);
        if (sign == 1)
        {
            ystretch += (plus * logory);
            if (ystretch >= bm.bmHeight * 2)
            {
                        ystretch = bm.bmHeight * 2;
                        sign = -1;
            }
        }
        else if (sign == -1)
        {
            ystretch -= (plus * logory);
            if (ystretch <= 0)
            {
                        ystretch = 0;
                        sign = 1;
            }
        }
        if (signx == 1)
        {
            xstretch += (xplus * logorx);
            if (xstretch >= bm.bmWidth * 2)
            {
                        xstretch = bm.bmWidth * 2;
                        signx = -1;
            }
        }
        else if (signx == -1)
        {
            xstretch -= (xplus * logorx);
            if (xstretch <= 0)
            {
                        xstretch = 0;
                        signx = 1;
            }
        }
}

void strechbmp (HBITMAP hbr, int x, int y, DWORD mode)
{

        int bmy;
        int bmx;
		int bmyst;
        int bmxst;
        HDC      hdc;

        xstretch = ystretch = 0;
        GetObject (hbr, sizeof (BITMAP), &bm);
		bmxst = (int) (double) ((double) bm.bmWidth * scrfx);
		bmyst = (int) (double) ((double) bm.bmHeight * scrfy);

        bmy = bmyst / 2;
        bmx = bmxst / 2;
//        bmy = bm.bmHeight / 2;
//        bmx = bm.bmWidth / 2;
        hdc = fithdc;
        hdcMemory  = CreateCompatibleDC(hdc);
        hbmOld = SelectObject (hdcMemory, hbr);

        SetViewportOrgEx (hdc, bmx, bmy, NULL);
        SetStretchBltMode (hdc, COLORONCOLOR);
        StretchBlt (hdc, -bmx + xstretch / 2, -bmy + ystretch / 2,
                         bmxst - xstretch, bmyst - ystretch,
                         hdcMemory,0, 0,
                         bm.bmWidth, bm.bmHeight, mode);

        SelectObject (hdcMemory, hbmOld);

        DeleteDC (hdcMemory);
}


void printlogo (HBITMAP hbr, int x, int y, DWORD mode)
{

        int bmy;
        int bmx;
		int bmyst;
        int bmxst;
        HDC      hdc;

//        GetObject (hbr, sizeof (BITMAP), &bm);
		bmxst = (int) (double) ((double) bm.bmWidth * scrfx);
		bmyst = (int) (double) ((double) bm.bmHeight * scrfy);

        bmy = bmyst / 2;
        bmx = bmxst / 2;
//        bmy = bm.bmHeight * scrfy / 2);
//        bmx = bm.bmWidth / 2;
//        hdc = fithdc;

/*
		xstretch = (int) (double) ((double) xstretch * scrfx);
		ystretch = (int) (double) ((double) ystretch * scrfy);
*/

        hdc = GetDC (FitWindow);
        hdcMemory  = CreateCompatibleDC(hdc);
        hbmOld = SelectObject (hdcMemory, hbr);

        SetViewportOrgEx (hdc, bmx, bmy, NULL);
        SetStretchBltMode (hdc, COLORONCOLOR);
        StretchBlt (hdc, -bmx + xstretch / 2, -bmy + ystretch / 2,
                         bmxst - xstretch, bmyst - ystretch,
                         hdcMemory,0, 0,
                         bm.bmWidth, bm.bmHeight, mode);

//        SelectObject (hdcMemory, hbmOld);

        DeleteDC (hdcMemory);
        ReleaseDC (FitWindow, hdc);
        if (sign == 1)
        {
            ystretch += (plus * logory);
            if (ystretch >= bmyst * 2)
            {
                        ystretch = bmyst * 2;
                        sign = -1;
            }
        }
        else if (sign == -1)
        {
            ystretch -= (plus * logory);
            if (ystretch <= 0)
            {
                        ystretch = 0;
                        sign = 1;
            }
        }
        if (signx == 1)
        {
            xstretch += (xplus * logorx);
            if (xstretch >= bmxst * 2)
            {
                        xstretch = bmxst * 2;
                        signx = -1;
            }
        }
        else if (signx == -1)
        {
            xstretch -= (xplus * logorx);
            if (xstretch <= 0)
            {
                        xstretch = 0;
                        signx = 1;
            }
        }
}

void LogoBitmap (HDC hdc)
/**
Bitmap in Lo�gofenster schreiben.
**/
{
	    RECT rect;

		GetClientRect (LogoWindow, &rect);

		bMap.StrechBitmap (hdc, rect.right, rect.bottom);
}


/*
void strechbmp (HBITMAP hbr, int x, int y, DWORD mode)
{

        HDC      hdc;

        GetObject (hbr, sizeof (BITMAP), &bm);
        hdc = fithdc;
        hdcMemory  = CreateCompatibleDC(hdc);
        hbmOld = SelectObject (hdcMemory, hbr);

        // SetViewportOrgEx (hdc, bmx, bmy, NULL);
        SetStretchBltMode (hdc, COLORONCOLOR);
        StretchBlt (hdc, 0, 0,
                         bm.bmWidth, bm.bmHeight,
                         hdcMemory,0, 0,
                         bm.bmWidth, bm.bmHeight, mode);

        SelectObject (hdcMemory, hbmOld);
        DeleteDC (hdcMemory);
}
*/

void printbmp (HBITMAP hbr, int x, int y, DWORD mode)
{
        RECT rect;

        GetClientRect (hMainWindow,  &rect);
        GetObject (hbr, sizeof (BITMAP), &bm);
        x = (rect.right - bm.bmWidth) / 2;
        y = (rect.bottom - bm.bmHeight) / 2;
        hdc = GetDC (hFitWindow);
        hdcMemory  = CreateCompatibleDC(hdc);
        hbmOld = SelectObject (hdcMemory, hbr);

        BitBlt (hdc, x, y, bm.bmWidth, bm.bmHeight,
                           hdcMemory,0, 0, mode);

        SelectObject (hdcMemory, hbmOld);

        DeleteDC (hdcMemory);
        ReleaseDC (hFitWindow, hdc);
}

int ScrollMenue ()
/**
Leere MenueZeilen entfernrn.
**/
{
        int i, j;

        memcpy (&scmenue, &menue, sizeof (MENUE));
        memcpy (&menue,   &leermenue, sizeof (MENUE));
        menue.mdn = scmenue.mdn;
        menue.fil = scmenue.fil;
        strcpy (menue.pers, scmenue.pers);
        strcpy (menue.prog, scmenue.prog);
        menue.anz_menue = scmenue.anz_menue;

        for (i = 0,j = 0; j < 10; j ++)
        {
                clipped (mentab[j]);
                if (strcmp (mentab[j], " ") > 0)
                {
                                    strcpy (mentab0[i], mentab[j]);
                                    strcpy (zeitab0[i], zeitab[j]);
                                    i ++;
                }
        }
        return (i);
}

void GetAutotara (void)
/**
Default fuer Atou-Tara holen und setzen.
**/
{
	    char tara_auto [2];

        strcpy (tara_auto, "N");
        DbClass.sqlin   ((long *) &StdWaage, 2, 0);
        DbClass.sqlout  ((char *) tara_auto, 0, 2);
        DbClass.sqlcomm ("select tara_auto from opt_mci where sys = ?");
		if (tara_auto [0] == 'J')
		{
			setautotara (TRUE);
		}
		else
		{
			setautotara (FALSE);
		}
}

void GetListColor (COLORREF *color, char *cfg_v)
/**
Farbunterlegung fuer Listen uebertragen.
**/
{
	static char *ColTxt[] = {"BLACKCOL",
		                     "WHITECOL",
							 "BLUECOL",
							 "REDCOL",
							 "LTGRAYCOL",
							 "GREENCOL",
							 "YELLOWCOL",
							 "DKYELLOWCOL",
							 NULL};

	static COLORREF ColVal[] = {BLACKCOL,
		                        WHITECOL,
					  		    BLUECOL,
							    REDCOL,
							    LTGRAYCOL,
							    GREENCOL,
							    YELLOWCOL,
								DKYELLOWCOL};
	int i;
	int red, blue, green;
	char *ColR;
	int Cols = 0;

	for (i = 0; ColTxt[i]; i ++)
	{
		if (strcmp (ColTxt[i], cfg_v) == 0)
		{
			*color = ColVal[i];
			return;
		}
	}

	ColR = strstr (cfg_v, "red");
	if (ColR)
	{
		red = atoi (&ColR[3]);
		Cols ++;
	}
	ColR = strstr (cfg_v, "blue");
	if (ColR)
	{
		blue = atoi (&ColR[4]);
		Cols ++;
	}
	ColR = strstr (cfg_v, "green");
	if (ColR)
	{
		green = atoi (&ColR[5]);
		Cols ++;
	}
	if (Cols == 3)
	{
		*color = RGB (red, green, blue);
	}
}



//  Hauptprogramm

int      PASCAL WinMain(HANDLE hInstance,HANDLE hPrevInstance,
                        LPSTR lpszCmdLine,int nCmdShow)
{
        HCURSOR oldcursor;
        char cfg_v [80];
		HDC hdc;
		char buffer [256];


		
		//FS-539 A
       int i;
       int anz = wsplit (lpszCmdLine, " ");
	   for (i = 0; i < anz; i ++)
	   {
		   if (memcmp (wort[i], "cfgname=", 8) == 0)
		   {
			   strncpy (progname ,&wort[i][8] , 25);
			   progname [25] = 0; 
			   ProgCfg.SetProgName (progname);
		   }
	   }
		//FS-539 E
		
		sprintf (EinAusgangtxt, "Auswahl Ein/Ausgang");


		sprintf (Zuruecktext,"Zur�ck F5");
		sprintf (MinusText,"-");
	    GetForeground ();
        GraphikMode = IsGdiPrint ();

		if (ProgCfg.GetCfgValue ("MitKunde", cfg_v) == TRUE)
        {
		             MitKunde =  atoi (cfg_v); // FISCH-5
        }
		if (ProgCfg.GetCfgValue ("default_kunde", cfg_v) == TRUE)
        {
		             default_kunde =  atoi (cfg_v); // FISCH-5
        }
		if (ProgCfg.GetCfgValue ("zutat_at", cfg_v) == TRUE)
        {
		             zutat_at =  atoi (cfg_v); // FISCH-5
        }
        if (ProgCfg.GetCfgValue ("KOMPFCOLOR", cfg_v) == TRUE)
        {
		             GetListColor (&KompfColor, cfg_v);
        }
        if (ProgCfg.GetCfgValue ("KOMPBCOLOR", cfg_v) == TRUE)
        {
		             GetListColor (&KompbColor, cfg_v);
        }

        if (ProgCfg.GetCfgValue ("Schwundabzug", cfg_v) == TRUE)
		{
			         Schwundabzug = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("ScanMulti", cfg_v) == TRUE)
		{
			         ScanMulti = atoi (cfg_v);
		}

        if (ProgCfg.GetCfgValue ("AuswahlZerlegeeingang", cfg_v) == TRUE)
		{
			         AuswahlZerlegeeingang = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("AuswahlZerlegeausgang", cfg_v) == TRUE)
		{
			         AuswahlZerlegeausgang = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("AuswahlUmlagerung", cfg_v) == TRUE)
		{
			         AuswahlUmlagerung = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("AuswahlZerlegung", cfg_v) == TRUE)
		{
			         AuswahlZerlegung = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("AuswahlFeinZerlegung", cfg_v) == TRUE)
		{
			         AuswahlFeinZerlegung = atoi (cfg_v);
		}

        if (ProgCfg.GetCfgValue ("AuswahlProduktionsstart", cfg_v) == TRUE)
		{
			         AuswahlProduktionsstart = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("AuswahlProduktionsfortschritt", cfg_v) == TRUE)
		{
			         AuswahlProduktionsfortschritt = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("AuswahlBuchenFertiglager", cfg_v) == TRUE)
		{
			         AuswahlBuchenFertiglager = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("DirektScannProduktionsstart", cfg_v) == TRUE)
		{
			         DirektScannProduktionsstart = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("DirektScannProduktionsfortschritt", cfg_v) == TRUE)
		{
			         DirektScannProduktionsfortschritt = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("DirektScannBuchenFertiglager", cfg_v) == TRUE)
		{
			         DirektScannBuchenFertiglager = atoi (cfg_v);
		}

        if (ProgCfg.GetCfgValue ("DruckAuswahl", cfg_v) ==TRUE)
        {
                    DruckAuswahl = atoi (cfg_v);  //LuD 020609
        }
        if (ProgCfg.GetCfgValue ("ohne_preis", cfg_v) ==TRUE)
        {
                    ohne_preis = atoi (cfg_v);  //LuD 090908
        }
        if (ProgCfg.GetCfgValue ("auto_eti", cfg_v) ==TRUE)
        {
                    auto_eti = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("colborder", cfg_v) ==TRUE)
        {
                     colborder = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("lsdefault", cfg_v) ==TRUE)
        {
                     lsdefault = min (1, max (0,atoi (cfg_v)));
        }

        if (ProgCfg.GetCfgValue ("best_auswahl_direct", cfg_v) ==TRUE)
        {
                     best_ausw_direct = min (1, max (0,atoi (cfg_v)));
        }

        if (ProgCfg.GetCfgValue ("liste_direct", cfg_v) ==TRUE)
        {
                     liste_direct = min (1, max (0,atoi (cfg_v)));
        }

        if (ProgCfg.GetCfgValue ("lief_bzg_zwang", cfg_v) ==TRUE)
        {
                     lief_bzg_zwang = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("druck_zwang", cfg_v) ==TRUE)
        {
                     druck_zwang = atoi (cfg_v);
        }


        if (ProgCfg.GetCfgValue ("a_auswahl_direct", cfg_v) ==TRUE)
        {
                     a_ausw_direct = atoi (cfg_v);
        }

        if (ProgCfg.GetCfgValue ("lief_auswahl_direct", cfg_v) ==TRUE)
        {
                     lief_ausw_direct = atoi (cfg_v);
        }

        if (ProgCfg.GetCfgValue ("hbk_direct", cfg_v) ==TRUE)
        {
                     hbk_direct = atoi (cfg_v);
        }

        if (ProgCfg.GetCfgValue ("multibest", cfg_v) ==TRUE)
        {
                     multibest = min (1, max (0,atoi (cfg_v)));
        }

        if (ProgCfg.GetCfgValue ("wieg_neu_direct", cfg_v) ==TRUE)
        {
                     wieg_neu_direct = atoi (cfg_v);
        }

        if (ProgCfg.GetCfgValue ("AutoSleep", cfg_v) ==TRUE)
        {
                     AutoSleep = atol (cfg_v);
        }

        if (ProgCfg.GetCfgValue ("wieg_direct", cfg_v) ==TRUE)
        {
                     wieg_direct = atoi (cfg_v);
        }

        if (ProgCfg.GetCfgValue ("hand_direct", cfg_v) ==TRUE)
        {
                     hand_direct = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("sort_a", cfg_v) == TRUE)
        {
		             sort_a =  min (1, max (0, atoi (cfg_v)));
        }

        if (ProgCfg.GetCfgValue ("hand_tara_sum", cfg_v) == TRUE)
        {
		             hand_tara_sum =  atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("fest_tara_sum", cfg_v) == TRUE)
        {
		             fest_tara_sum =  atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("fest_tara_anz", cfg_v) == TRUE)
        {
		             fest_tara_anz =  atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("EinhEk", cfg_v) == TRUE)
        {
		             EktoMeEinh =  atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("autocursor", cfg_v) == TRUE)
		{
			         autocursor = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("DrKomplett", cfg_v) == TRUE)
		{
			         DrKomplett = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("MultiTemperature", cfg_v) == TRUE)
		{
			         MultiTemperature = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("MesswerteZwang", cfg_v) == TRUE)
		{
			         MesswerteZwang = atoi (cfg_v);
		}

        if (getenv ("testmode"))
        {
                     testmode = atoi (getenv ("testmode"));
        }
        if (ProgCfg.GetCfgValue ("mdn_default", cfg_v) ==TRUE)
        {
                    akt_mdn = atoi (cfg_v);
                    Mandant = atoi (cfg_v);

        }
        if (ProgCfg.GetCfgValue ("PassWord", cfg_v) == TRUE)
		{
			         PassWord = atoi (cfg_v);
					 SetPassWordFlag (PassWord);
		}
        if (ProgCfg.GetCfgValue ("TimerPassWord", cfg_v) == TRUE)
		{
			         TimerPassWord = atoi (cfg_v);
					 SetTimerPassWord (TimerPassWord);
		}
        if (ProgCfg.GetCfgValue ("fil_default", cfg_v) ==TRUE)
        {
                    akt_fil = atoi (cfg_v);
                    Filiale = atoi (cfg_v);
        }

        if (ProgCfg.GetCfgValue ("QsZwang", cfg_v) ==TRUE)
        {
                    QsZwang = atoi (cfg_v); 
					//QSZwang = 1 : Zwang bie Kopf und Pos
					//QSZwang = 2 : Zwang nur bei Kopf
        }
        if (ProgCfg.GetCfgValue ("Rindfleischdaten", cfg_v) ==TRUE)
        {
			         WithZusatz = atoi (cfg_v); 
		}
        if (ProgCfg.GetCfgValue ("generiere_charge", cfg_v) ==TRUE)
        {
			         generiere_charge = atoi (cfg_v); 
		}
        if (ProgCfg.GetCfgValue ("ScannMode", cfg_v) ==TRUE)
        {
                    ScannMode = atoi (cfg_v);
        }

        if (ProgCfg.GetCfgValue ("eti_kiste", cfg_v) ==TRUE)
        {
			         if (strcmp (cfg_v, "NO"))
					 {
                           eti_kiste = (char *) GlobalAlloc (GMEM_FIXED , strlen (cfg_v) + 1);
					       if (eti_kiste)
						   {
					           strcpy (eti_kiste, cfg_v);
						   }
					 }
		}
        if (ProgCfg.GetCfgValue ("eti_ki_drk", cfg_v) ==TRUE)
        {
			         if (strcmp (cfg_v, "NO"))
					 {
                           eti_ki_drk = (char *) GlobalAlloc (GMEM_FIXED , strlen (cfg_v) + 1);
					       if (eti_ki_drk)
						   {
					           strcpy (eti_ki_drk, cfg_v);
						   }
					 }
		}

        if (ProgCfg.GetCfgValue ("eti_ki_typ", cfg_v) ==TRUE)
        {
                     eti_ki_typ = atoi (cfg_v);
		}

        if (ProgCfg.GetCfgValue ("eti_nve", cfg_v) ==TRUE)
        {
			         if (strcmp (cfg_v, "NO"))
					 {
                           eti_nve = (char *) GlobalAlloc (GMEM_FIXED , strlen (cfg_v) + 1);
					       if (eti_nve)
						   {
					           strcpy (eti_nve, cfg_v);
						   }
					 }
		}
        if (ProgCfg.GetCfgValue ("eti_nve_drk", cfg_v) ==TRUE)
        {
			         if (strcmp (cfg_v, "NO"))
					 {
                           eti_nve_drk = (char *) GlobalAlloc (GMEM_FIXED , strlen (cfg_v) + 1);
					       if (eti_nve_drk)
						   {
					           strcpy (eti_nve_drk, cfg_v);
						   }
					 }
		}

        if (ProgCfg.GetCfgValue ("eti_nve_typ", cfg_v) ==TRUE)
        {
                     eti_nve_typ = atoi (cfg_v);
		}

        if (ProgCfg.GetCfgValue ("eti_ez", cfg_v) ==TRUE)
        {
			         if (strcmp (cfg_v, "NO"))
					 {
                            eti_ez = (char *) GlobalAlloc (GMEM_FIXED , strlen (cfg_v) + 1);
					        if (eti_ez)
							{
					           strcpy (eti_ez, cfg_v);
							}
					 }
		}
        if (ProgCfg.GetCfgValue ("eti_ez_drk", cfg_v) ==TRUE)
        {
			         if (strcmp (cfg_v, "NO"))
					 {
                            eti_ez_drk = (char *) GlobalAlloc (GMEM_FIXED , strlen (cfg_v) + 1);
					        if (eti_ez_drk)
							{
					           strcpy (eti_ez_drk, cfg_v);
							}
					 }
		}

        if (ProgCfg.GetCfgValue ("eti_ez_typ", cfg_v) ==TRUE)
        {
                     eti_ez_typ = atoi (cfg_v);
		}

        if (ProgCfg.GetCfgValue ("eti_ev", cfg_v) ==TRUE)
        {
			         if (strcmp (cfg_v, "NO"))
					 {
                            eti_ev = (char *) GlobalAlloc (GMEM_FIXED , strlen (cfg_v) + 1);
					        if (eti_ev)
							{
					           strcpy (eti_ev, cfg_v);
							}
					 }
		}

        if (ProgCfg.GetCfgValue ("eti_ev_drk", cfg_v) ==TRUE)
        {
			         if (strcmp (cfg_v, "NO"))
					 {
                            eti_ev_drk = (char *) GlobalAlloc (GMEM_FIXED , strlen (cfg_v) + 1);
					        if (eti_ev_drk)
							{
					           strcpy (eti_ev_drk, cfg_v);
							}
					 }
		}

        if (ProgCfg.GetCfgValue ("eti_ev_typ", cfg_v) ==TRUE)
        {
                     eti_ev_typ = atoi (cfg_v);
		}

        if (ProgCfg.GetCfgValue ("eti_artikel1", cfg_v) ==TRUE)
        {
			         if (strcmp (cfg_v, "NO"))
					 {
                            eti_artikel1 = (char *) GlobalAlloc (GMEM_FIXED , strlen (cfg_v) + 1);
					        if (eti_artikel1)
							{
					           strcpy (eti_artikel1, cfg_v);
							}
					 }
		}
        if (ProgCfg.GetCfgValue ("eti_artikel2", cfg_v) ==TRUE)
        {
			         if (strcmp (cfg_v, "NO"))
					 {
                            eti_artikel2 = (char *) GlobalAlloc (GMEM_FIXED , strlen (cfg_v) + 1);
					        if (eti_artikel2)
							{
					           strcpy (eti_artikel2, cfg_v);
							}
					 }
		}
        if (ProgCfg.GetCfgValue ("eti_artikel3", cfg_v) ==TRUE)
        {
			         if (strcmp (cfg_v, "NO"))
					 {
                            eti_artikel3 = (char *) GlobalAlloc (GMEM_FIXED , strlen (cfg_v) + 1);
					        if (eti_artikel3)
							{
					           strcpy (eti_artikel3, cfg_v);
							}
					 }
		}
        if (ProgCfg.GetCfgValue ("eti_artikel4", cfg_v) ==TRUE)
        {
			         if (strcmp (cfg_v, "NO"))
					 {
                            eti_artikel4 = (char *) GlobalAlloc (GMEM_FIXED , strlen (cfg_v) + 1);
					        if (eti_artikel4)
							{
					           strcpy (eti_artikel4, cfg_v);
							}
					 }
		}
        if (ProgCfg.GetCfgValue ("eti_artikel5", cfg_v) ==TRUE)
        {
			         if (strcmp (cfg_v, "NO"))
					 {
                            eti_artikel5 = (char *) GlobalAlloc (GMEM_FIXED , strlen (cfg_v) + 1);
					        if (eti_artikel5)
							{
					           strcpy (eti_artikel5, cfg_v);
							}
					 }
		}
        if (ProgCfg.GetCfgValue ("eti_artikel6", cfg_v) ==TRUE)
        {
			         if (strcmp (cfg_v, "NO"))
					 {
                            eti_artikel6 = (char *) GlobalAlloc (GMEM_FIXED , strlen (cfg_v) + 1);
					        if (eti_artikel6)
							{
					           strcpy (eti_artikel6, cfg_v);
							}
					 }
		}
        if (ProgCfg.GetCfgValue ("eti_artikel7", cfg_v) ==TRUE)
        {
			         if (strcmp (cfg_v, "NO"))
					 {
                            eti_artikel7 = (char *) GlobalAlloc (GMEM_FIXED , strlen (cfg_v) + 1);
					        if (eti_artikel7)
							{
					           strcpy (eti_artikel7, cfg_v);
							}
					 }
		}
        if (ProgCfg.GetCfgValue ("eti_artikel8", cfg_v) ==TRUE)
        {
			         if (strcmp (cfg_v, "NO"))
					 {
                            eti_artikel8 = (char *) GlobalAlloc (GMEM_FIXED , strlen (cfg_v) + 1);
					        if (eti_artikel8)
							{
					           strcpy (eti_artikel8, cfg_v);
							}
					 }
		}
        if (ProgCfg.GetCfgValue ("eti_artikel", cfg_v) ==TRUE)
        {
			         if (strcmp (cfg_v, "NO"))
					 {
                            eti_artikel = (char *) GlobalAlloc (GMEM_FIXED , strlen (cfg_v) + 1);
					        if (eti_artikel)
							{
					           strcpy (eti_artikel, cfg_v);
							}
					 }
		}

        if (ProgCfg.GetCfgValue ("eti_a_drk", cfg_v) ==TRUE)
        {
			         if (strcmp (cfg_v, "NO"))
					 {
                            eti_a_drk = (char *) GlobalAlloc (GMEM_FIXED , strlen (cfg_v) + 1);
					        if (eti_a_drk)
							{
					           strcpy (eti_a_drk, cfg_v);
							}
					 }
		}

        if (ProgCfg.GetCfgValue ("eti_a_typ", cfg_v) ==TRUE)
        {
                     eti_a_typ = atoi (cfg_v);
		}

        if (ProgCfg.GetCfgValue ("EnterEtiAnz", cfg_v) ==TRUE)
        {
			         TOUCHF::EnterAnz = atoi (cfg_v);
		}

        if (ProgCfg.GetCfgValue ("Scannen", cfg_v) == TRUE)
		{
			         Scannen = atoi (cfg_v);
		}

        if (ProgCfg.GetCfgValue ("SmallScann", cfg_v) == TRUE)
		{
			         SmallScann = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("Scanlen", cfg_v) == TRUE)
		{
			         Scanlen = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("ChargeZwang", cfg_v) ==TRUE)
        {
					ChargeZwang = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("ScanCharge", cfg_v) ==TRUE)
        {
					ScanCharge = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("LagerAuswahl", cfg_v) ==TRUE)
        {
					WithLagerAuswahl = atoi (cfg_v);
        }

        if (ProgCfg.GetCfgValue ("EinAusgang", cfg_v) ==TRUE)   
        {
			         if (strcmp (clipped(cfg_v), "NO"))
					 {
                           EinAusgang = (char *) GlobalAlloc (GMEM_FIXED , strlen (cfg_v) + 1);
				           strcpy (EinAusgang, cfg_v);
					       if (EinAusgang)
						   {
							 if (atoi(EinAusgang) == EINGANG)
							 {
								sprintf (EinAusgangtxt, "Zerlegeeingang");
							    sprintf (Einstiegtxt, "%s", "Zerlegepartie");
						        sprintf (Auswahltxt, "%s", "Partieauswahl");
							   testform = zereinform;
							   testub = zereinub;
							   testvl = zereinvl;
							   aufform = StdAuswForm; //Auswahlliste
							   aufub = StdAuswub ;
							   aufvl = StdAuswvl;

							 }
							 else if (atoi(EinAusgang) == AUSGANG)
							 {
								sprintf (EinAusgangtxt, "Zerlegeausgang");
							    sprintf (Einstiegtxt, "%s", "Zerlegepartie");
						        sprintf (Auswahltxt, "%s", "Partieauswahl");
							   testform = zerausform;
							   testub = zerausub;
							   testvl = zerausvl;
							   aufform = StdAuswForm; //Auswahlliste
							   aufub = StdAuswub ;
							   aufvl = StdAuswvl;
							 }
							 else if (atoi(EinAusgang) == UMLAGERUNG)
							 {
								sprintf (EinAusgangtxt, "Umlagerung");
							    sprintf (Einstiegtxt, "%s", "Start Umlagerung");
						        sprintf (Auswahltxt, "%s", "Auswahl");
							   testform = umlform;
							   testub = umlub;
							   testvl = umlvl;
							   aufform = StdAuswForm; //Auswahlliste
							   aufub = StdAuswub ;
							   aufvl = StdAuswvl;
							 }
							 else if (atoi(EinAusgang) == ZERLEGUNG)   //GROM-6
							 {
								sprintf (EinAusgangtxt, "Zerlegung");
							    sprintf (Einstiegtxt, "%s", "Chargeneingabe");
						        sprintf (Auswahltxt, "%s", "Auswahl");
							   testform = zerform;
							   testub = zerub;
							   testvl = zervl;
							   aufform = StdAuswForm; //Auswahlliste
							   aufub = StdAuswub ;
							   aufvl = StdAuswvl;
							 }
							 else if (atoi(EinAusgang) == FEINZERLEGUNG)   //GROM-6
							 {
								sprintf (EinAusgangtxt, "FeinZerlegung");
							    sprintf (Einstiegtxt, "%s", "Chargeneingabe");
						        sprintf (Auswahltxt, "%s", "Auswahl");
							   testform = zerform;
							   testub = zerub;
							   testvl = zervl;
							   aufform = StdAuswForm; //Auswahlliste
							   aufub = StdAuswub ;
							   aufvl = StdAuswvl;
							 }
							 else if (atoi(EinAusgang) == PRODUKTIONSSTART )  
							 {
								sprintf (EinAusgangtxt, "Produktionsstart");
							    sprintf (Einstiegtxt, "%s", "Starte Produktion");
						        sprintf (Auswahltxt, "%s", "Auswahl");
							    testform = prodform; //Liste
								testub = produb;
								testvl = prodvl;
							    aufform = AuswProdForm; //Auswahlliste
								aufub = AuswProdub;
								aufvl = AuswProdvl;
				  		        zerl_p.lgr_zu = akt_lager_zu;
			                    zerl_p.lgr_ab = akt_lager_ab;

							 }
							 else if (atoi(EinAusgang) == PRODUKTIONSFORTSCHRITT )   
							 {
								sprintf (EinAusgangtxt, "Produktionsfortschritt");
							    sprintf (Einstiegtxt, "%s", "Fortschritt scannen");
						        sprintf (Auswahltxt, "%s", "Auswahl");
							    testform = fortschrittform; //Liste
								testub = fortschrittub;
								testvl = fortschrittvl;
							    aufform = AuswFortschrittform; //Auswahlliste
								aufub = AuswFortschrittub;
								aufvl = AuswFortschrittvl;
							 }
							 else if (atoi(EinAusgang) == BUCHENFERTIGLAGER )   
							 {
								sprintf (EinAusgangtxt, "BuchenFertiglager");
						       sprintf (Einstiegtxt, "%s", "Buchen in Fertiglager");
						       sprintf (Auswahltxt, "%s", "Auswahl");

						        sprintf (Auswahltxt, "%s", "Auswahl");
							    testform = buchform; //Liste
								testub = buchub;
								testvl = buchvl;
							    aufform = AuswProdForm; //Auswahlliste
								aufub = AuswProdub;
								aufvl = AuswProdvl;
							 }

						   }
					 }
        }



        if (ProgCfg.GetCfgValue ("ZELagerAbgang_default", cfg_v) ==TRUE)
        {
                   ZELagerAb = atoi (cfg_v);
			       sprintf (LagerAbtxt, "%s %hd", LAbtxt, ZELagerAb);
        }
        if (ProgCfg.GetCfgValue ("ZELagerZugang_default", cfg_v) ==TRUE)
        {
                   ZELagerZu = atoi (cfg_v);
			       sprintf (LagerZutxt, "%s %hd", LZutxt, ZELagerZu);
        }
        if (ProgCfg.GetCfgValue ("ZALagerAbgang_default", cfg_v) ==TRUE)
        {
                   ZALagerAb = atoi (cfg_v);
			       sprintf (LagerAbtxt, "%s %hd", LAbtxt, ZALagerAb);
        }
        if (ProgCfg.GetCfgValue ("ZALagerZugang_default", cfg_v) ==TRUE)
        {
                   ZALagerZu = atoi (cfg_v);
			       sprintf (LagerZutxt, "%s %hd", LZutxt, ZALagerZu);
        }
        if (ProgCfg.GetCfgValue ("UMLagerAbgang_default", cfg_v) ==TRUE)
        {
                   UMLagerAb = atoi (cfg_v);
			       sprintf (LagerAbtxt, "%s %hd", LAbtxt, UMLagerAb);
        }
        if (ProgCfg.GetCfgValue ("UMLagerZugang_default", cfg_v) ==TRUE)
        {
                   UMLagerZu = atoi (cfg_v);
			       sprintf (LagerZutxt, "%s %hd", LZutxt, UMLagerZu);
        }
        if (ProgCfg.GetCfgValue ("ZRLagerAbgang_default", cfg_v) ==TRUE)
        {
                   ZRLagerAb = atoi (cfg_v);
			       sprintf (LagerAbtxt, "%s %hd", LAbtxt, ZRLagerAb);
        }
        if (ProgCfg.GetCfgValue ("ZRLagerZugang_default", cfg_v) ==TRUE)
        {
                   ZRLagerZu = atoi (cfg_v);
			       sprintf (LagerZutxt, "%s %hd", LZutxt, ZRLagerZu);
        }
        if (ProgCfg.GetCfgValue ("FZRLagerAbgang_default", cfg_v) ==TRUE)
        {
                   FZRLagerAb = atoi (cfg_v);
			       sprintf (LagerAbtxt, "%s %hd", LAbtxt, ZRLagerAb);
        }
        if (ProgCfg.GetCfgValue ("FZRLagerZugang_default", cfg_v) ==TRUE)
        {
                   FZRLagerZu = atoi (cfg_v);
			       sprintf (LagerZutxt, "%s %hd", LZutxt, ZRLagerZu);
        }

        if (ProgCfg.GetCfgValue ("A6_LagerZugang_default", cfg_v) ==TRUE)
        {
                   A6_LagerZu = atoi (cfg_v);
			       sprintf (LagerZutxt, "%s %hd", LZutxt, A6_LagerZu);
        }
        if (ProgCfg.GetCfgValue ("A6_LagerAbgang_default", cfg_v) ==TRUE)
        {
                   A6_LagerAb = atoi (cfg_v);
			       sprintf (LagerAbtxt, "%s %hd", LAbtxt, A6_LagerAb);
        }
        if (ProgCfg.GetCfgValue ("A7_LagerZugang_default", cfg_v) ==TRUE)
        {
                   A7_LagerZu = atoi (cfg_v);
			       sprintf (LagerZutxt, "%s %hd", LZutxt, A7_LagerZu);
        }
        if (ProgCfg.GetCfgValue ("A7_LagerAbgang_default", cfg_v) ==TRUE)
        {
                   A7_LagerAb = atoi (cfg_v);
			       sprintf (LagerAbtxt, "%s %hd", LAbtxt, A7_LagerAb);
        }
        if (ProgCfg.GetCfgValue ("A8_LagerZugang_default", cfg_v) ==TRUE)
        {
                   A8_LagerZu = atoi (cfg_v);
			       sprintf (LagerZutxt, "%s %hd", LZutxt, A8_LagerZu);
        }
        if (ProgCfg.GetCfgValue ("A8_LagerAbgang_default", cfg_v) ==TRUE)
        {
                   A8_LagerAb = atoi (cfg_v);
			       sprintf (LagerAbtxt, "%s %hd", LAbtxt, A8_LagerAb);
        }

        if (ProgCfg.GetCfgValue ("Standort", cfg_v) ==TRUE)
        {
                   Standort = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("QsPosWieg", cfg_v) ==TRUE)
        {
                   QsPosWieg = atoi (cfg_v);
        }
        ActivateColButton (BackWindow2, &MainButton,  2, 1, 1); 
		if (EinAusgang)
		{
			if (atoi(EinAusgang) == EINGANG)
			{
                   LagerAb = ZELagerAb;
			       sprintf (LagerAbtxt, "%s %hd", LAbtxt, LagerAb);
                   LagerZu = ZELagerZu;
			       sprintf (LagerZutxt, "%s %hd", LZutxt, LagerZu);
			}
			else if (atoi(EinAusgang) == AUSGANG)
			{
                   LagerAb = ZALagerAb;
			       sprintf (LagerAbtxt, "%s %hd", LAbtxt, LagerAb);
                   LagerZu = ZALagerZu;
			       sprintf (LagerZutxt, "%s %hd", LZutxt, LagerZu);
			}
			else if (atoi(EinAusgang) == UMLAGERUNG)
			{
                   LagerAb = UMLagerAb;
			       sprintf (LagerAbtxt, "%s %hd", LAbtxt, LagerAb);
                   LagerZu = UMLagerZu;
			       sprintf (LagerZutxt, "%s %hd", LZutxt, LagerZu);
		           ActivateColButton (BackWindow2, &MainButton,  2, -1, 1); 
			}
			else if (atoi(EinAusgang) == ZERLEGUNG)
			{
                   LagerAb = ZRLagerAb;
			       sprintf (LagerAbtxt, "%s %hd", LAbtxt, LagerAb);
                   LagerZu = ZRLagerZu;
			       sprintf (LagerZutxt, "%s %hd", LZutxt, LagerZu);
			}
			else if (atoi(EinAusgang) == FEINZERLEGUNG)
			{
                   LagerAb = FZRLagerAb;
			       sprintf (LagerAbtxt, "%s %hd", LAbtxt, LagerAb);
                   LagerZu = FZRLagerZu;
			       sprintf (LagerZutxt, "%s %hd", LZutxt, LagerZu);
			}
			else if (atoi(EinAusgang) == PRODUKTIONSSTART )
			{
                   LagerAb = A6_LagerAb;
			       sprintf (LagerAbtxt, "%s %hd", LAbtxt, LagerAb);
                   LagerZu = A6_LagerZu;
			       sprintf (LagerZutxt, "%s %hd", LZutxt, LagerZu);
			}
			else if (atoi(EinAusgang) == PRODUKTIONSFORTSCHRITT )
			{
                   LagerAb = A7_LagerAb;
			       sprintf (LagerAbtxt, "%s %hd", LAbtxt, LagerAb);
                   LagerZu = A7_LagerZu;
			       sprintf (LagerZutxt, "%s %hd", LZutxt, LagerZu);
			}
			else if (atoi(EinAusgang) == BUCHENFERTIGLAGER )
			{
                   LagerAb = A8_LagerAb;
			       sprintf (LagerAbtxt, "%s %hd", LAbtxt, LagerAb);
                   LagerZu = A8_LagerZu;
			       sprintf (LagerZutxt, "%s %hd", LZutxt, LagerZu);
			}
			zerl_p.lgr_zu = LagerZu;
			zerl_p.lgr_ab = LagerAb;
		}




        Scanean.Read ();

		if (eti_kiste && eti_ki_drk)
		{
			         WithSpezEti = TRUE;
		}
		else
		{
			eti_kiste = eti_ki_drk = NULL;
		}

		if (eti_ez && eti_ez_drk)
		{
			         WithSpezEti = TRUE;
		}
		else
		{
			eti_ez = eti_ez_drk = NULL;
		}

		if (eti_ev && eti_ev_drk)
		{
			         WithSpezEti = TRUE;
		}
		else
		{
			eti_ev = eti_ev_drk = NULL;
		}

		if (eti_artikel && eti_a_drk)
		{
			         WithSpezEti = TRUE;
		}
		else
		{
			eti_artikel = eti_a_drk = NULL;
		}

		Touchf.SetEtiAttr (eti_kiste, eti_ez, eti_ev, eti_artikel);

		SetScannMode ();
        Kist = new BMAP;
		if (Kist)
		{
                  hdc = GetDC (hMainWindow);
		          sprintf (buffer, "%s\\kiste2.bmp", getenv ("BWSETC"));
		          KistBmp = Kist->ReadBitmap (hdc, buffer);
				  ReleaseDC (hMainWindow, hdc);
		}
        KistG= new BMAP;
		if (KistG)
		{
                  hdc = GetDC (hMainWindow);
		          sprintf (buffer, "%s\\kiste2g.bmp", getenv ("BWSETC"));
		          KistBmpG = KistG->ReadBitmap (hdc, buffer);
				  ReleaseDC (hMainWindow, hdc);
		}

        KistMask = new BMAP;
		if (KistMask)
		{
                  hdc = GetDC (hMainWindow);
		          sprintf (buffer, "%s\\kiste2.msk", getenv ("BWSETC"));
		          KistMaskBmp = KistMask->ReadBitmap (hdc, buffer);
				  ReleaseDC (hMainWindow, hdc);
		}

		NoteIcon = LoadIcon (hInstance, "NOTEICON");

		LtBlueBrush = CreateSolidBrush (BLUECOL);
		YellowBrush = CreateSolidBrush (YELLOWCOL);
        opendbase ("bws");
         if ( lgr_class.lese_lgr_bz (LagerAb) == 0) strcpy (LgrAb_krz,lgr.lgr_bz);
         if ( lgr_class.lese_lgr_bz (LagerZu) == 0) strcpy (LgrZu_krz,lgr.lgr_bz);
         memcpy (&a_lgr, &a_lgr_null, sizeof (struct A_LGR));

        sysdate (KomDatum);
        menue_class.SetSysBen ();
        if (sys_ben.berecht != 0)
        {
                   ohne_preis = 1;
        }

        if (getenv ("WAAKT"))
        {
                    strcpy (waadir, getenv ("WAAKT"));
                    sprintf (ergebnis, "%s\\ergebnis", waadir);
        }

        menue_class.Lesesys_inst ();
        FillQsHead ();
        FillQsPos();

        auszeichner = GetStdAuszeichner ();
        sys_peri_class.lese_sys (auszeichner);

        StdWaage = GetStdWaage ();
        sys_peri_class.lese_sys (StdWaage);
        la_zahl = sys_peri.la_zahl;

		GetAutotara ();

        strcpy (sys_par.sys_par_nam, "immer_preis");
        if (sys_par_class.dbreadfirst () == 0)
        {
                    immer_preis = atoi (sys_par.sys_par_wrt);
        }

        stringcopy (sys_par.sys_par_nam,"zutat_at",sizeof (sys_par.sys_par_nam));
        if (sys_par_class.dbreadfirst () == 0)
        {
                 zutat_at_par = atoi (sys_par.sys_par_wrt);
        }

        strcpy (sys_par.sys_par_nam, "ls_leer_zwang");
        if (sys_par_class.dbreadfirst () == 0)
        {
                    ls_leer_zwang = atoi (sys_par.sys_par_wrt);
        }

        strcpy (sys_par.sys_par_nam, "ls_charge_par");
        if (sys_par_class.dbreadfirst () == 0)
        {
                    ls_charge_par = atoi (sys_par.sys_par_wrt);
        }
        strcpy (sys_par.sys_par_nam, "lsc2_par");
        if (sys_par_class.dbreadfirst () == 0)
        {
                    lsc2_par = atoi (sys_par.sys_par_wrt);
        }


        sprintf (Mandanttxt, "%s %hd", Mtxt, Mandant);
        sprintf (Filialetxt, "%s %hd", Ftxt, Filiale);

		ColBorder = colborder;
        if (strcmp (clipped (sys_inst.projekt), "F H S"))
        {
                    fitbmp   = LoadBitmap (hInstance, "fit1");
        }
        else
        {
                    fitbmp   = LoadBitmap (hInstance, "fhs");
                    IsFhs = 1;
        }
        PfeilL.bmp = LoadBitmap (hInstance, "pfeill");
        PfeilR.bmp = LoadBitmap (hInstance, "pfeilr");
        PfeilU.bmp = LoadBitmap (hInstance, "pfeilu");
        PfeilO.bmp = LoadBitmap (hInstance, "pfeilo");
        GetObject (fitbmp, sizeof (BITMAP), &bm);


		//220712 A
		FestTaraAn = new BMAP ();
		FestTaraAn->hBitmap = FestTaraAn->LoadBitmap (hInstance, "Festtara", "", FesttaraBackgroundColSelect);  //testtest
		if (QsPosWieg == TRUE)
		{
			ok = new BMAP ();
			ok->hBitmap = ok->LoadBitmap (hInstance, "ok", "", FesttaraBackgroundColSelect);  //testtest
			nok = new BMAP ();
			nok->hBitmap = nok->LoadBitmap (hInstance, "nok", "", FesttaraBackgroundColSelect);  //testtest
		}
		//220712 E
		BelegeFestTara (); //220712

        hMainInst = hInstance;
        if (getenv (BWS))
        {
                   strcpy (fitpath, getenv (BWS));
        }
        else
        {
                   strcpy (fitpath, "\\USER\\BWS8000");
        }


        strcpy (rosipath, getenv (RSDIR));

        GetObject (fitbmp, sizeof (BITMAP), &fitbm);

        InitFirstInstance (hInstance);
        if (InitNewInstance (hInstance, nCmdShow) == 0) return 0;

		Qmkopf = new QMKOPF (hMainInst, LogoWindow);
		Qmkopf->SetDevice (scrfy, scrfy);
//		Qmkopf->SetDevice (0.75, 0.75);

        oldcursor = SetCursor (LoadCursor (NULL, IDC_WAIT));
        WaageInit ();
        SetCursor (oldcursor);
		PROD_MainButtons_inaktiv ();

        SetFocus (MainButton.mask[0].feldid);
        return ProcessMessages ();
}


void InitFirstInstance(HANDLE hInstance)
{
        WNDCLASS wc;

		HDC hdc;
		int col;

		col = 0;
		hdc = GetDC (NULL);
		col = GetDeviceCaps (hdc, BITSPIXEL);
		ReleaseDC (NULL, hdc);
		if (col < 16) ColBorder = FALSE;

        wc.style         =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                            | CS_CLASSDC;
        wc.lpfnWndProc   =  WndProc;
        wc.cbClsExtra    =  0;
        wc.cbWndExtra    =  0;
        wc.hInstance     =  hInstance;
        wc.hIcon         =  LoadIcon (hInstance, "FITICON");
        wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
        wc.lpszMenuName  =  "";
        wc.lpszClassName =  lpszClassName;

        RegisterClass(&wc);

        wc.style         =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                            | CS_OWNDC;
        wc.lpfnWndProc   =  FitLogoProc;
        wc.lpszMenuName  =  "";
        wc.hbrBackground =  GetStockObject (WHITE_BRUSH);
        wc.lpszClassName =  "Fitlogo";
        RegisterClass(&wc);

        wc.lpfnWndProc   =  UhrProc;
        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
        wc.lpszClassName =  "Uhr";
        RegisterClass(&wc);


        wc.lpfnWndProc   =  WndProc;
        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
        wc.lpszClassName =  "Backwind";

        RegisterClass(&wc);

        wc.hbrBackground =  CreateSolidBrush (RGB (0, 255, 255));
        wc.lpszClassName =  "LogoWind";
        RegisterClass(&wc);

        wc.lpfnWndProc   =  GebProc;
        wc.hbrBackground =  CreateSolidBrush (GebColor);
        wc.lpszClassName =  "GebWind";
        RegisterClass(&wc);

        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
        wc.lpszClassName =  "GebWindB";
        RegisterClass(&wc);

        wc.lpfnWndProc   =   MsgProc;
        wc.hbrBackground =   GetStockObject (WHITE_BRUSH);
        wc.lpszClassName =  "AufWiegWhite";
        RegisterClass(&wc);

        wc.lpfnWndProc   =   MsgProc;
        wc.hbrBackground =   CreateSolidBrush (RGB (0, 0, 255));
        wc.lpszClassName =  "AufWiegBlue";
        RegisterClass(&wc);

        wc.lpfnWndProc   =   PosLeerProc;
        wc.hbrBackground =   CreateSolidBrush (RGB (0, 255, 255));
        wc.lpszClassName =  "PosLeer";
        RegisterClass(&wc);

        wc.lpfnWndProc   =   PosLeerProc;
        wc.hbrBackground =   GetStockObject (WHITE_BRUSH);
        wc.lpszClassName =  "PosLeerKopf";
        RegisterClass(&wc);
}

void hKorrMainButton (double fcx, double fcy)
/**
Position von Mainbutton korrigieren.
**/
{
	    fittextlogo.FontHeight = (short) (double)
			           ((double) fittextlogo.FontHeight * fcy);
	    fittextlogo2.FontHeight = (short) (double)
			           ((double) fittextlogo2.FontHeight * fcy);
//	    fittextlogo3.FontHeight = (short) (double)
//			           ((double) fittextlogo3.FontHeight * fcy);
	    if (fittext.mask[0].pos[0] != -1)
		{
			     fittext.mask[0].pos[0] = (short) (double)
					 ((double) fittext.mask[0].pos[0] * fcy);
		}
		if (fittext.mask[0].pos[1] != -1)
		{
			     fittext.mask[0].pos[1] = (short) (double)
					 ((double) fittext.mask[0].pos[1] * fcx);

		}

	    if (fittext2.mask[0].pos[0] != -1)
		{
			     fittext2.mask[0].pos[0] = (short) (double)
					 ((double) fittext2.mask[0].pos[0] * fcy);
		}
		if (fittext2.mask[0].pos[1] != -1)
		{
			     fittext2.mask[0].pos[1] = (short) (double)
					 ((double) fittext2.mask[0].pos[1] * fcx);

		}

/*
	    if (fittext3.mask[0].pos[0] != -1)
		{
			     fittext3.mask[0].pos[0] = (short) (double)
					 ((double) fittext3.mask[0].pos[0] * fcy);
		}
		if (fittext3.mask[0].pos[1] != -1)
		{
			     fittext3.mask[0].pos[1] = (short) (double)
					 ((double) fittext3.mask[0].pos[1] * fcx);

		}
*/
}


void KorrMainButton (form * MainButton, double fcx, double fcy)
/**
Position von Mainbutton korrigieren.
**/
{
	    int i;

		for (i = 0; i < MainButton->fieldanz; i ++)
		{
			if (MainButton->mask[i].pos[0] != -1)
			{
			     MainButton->mask[i].pos[0] = (short) (double)
					 ((double) MainButton->mask[i].pos[0] * fcy);
			}
			if (MainButton->mask[i].pos[1] != -1)
			{
			     MainButton->mask[i].pos[1] = (short) (double)
					 ((double) MainButton->mask[i].pos[1] * fcx);

			}
	        MainButton->mask[i].rows = (short) (double)
			 ((double) MainButton->mask[i].rows * fcy);
	        MainButton->mask[i].length = (short) (double)
			 ((double) MainButton->mask[i].length * fcx);
		}
}

void KorrMainButton2 (form * MainButton, double fcx, double fcy)
/**
Position von Mainbutton korrigieren.
**/
{
	    int i;

		for (i = 0; i < MainButton->fieldanz; i ++)
		{
			if (MainButton->mask[i].pos[0] != -1)
			{
			     MainButton->mask[i].pos[0] = (short) (double)
					 ((double) MainButton->mask[i].pos[0] * fcy);
			}
			if (MainButton->mask[i].pos[1] != -1)
			{
			     MainButton->mask[i].pos[1] = (short) (double)
					 ((double) MainButton->mask[i].pos[1] * fcx);

			}
	        MainButton->mask[i].rows = (short) (double)
			 ((double) MainButton->mask[i].rows * fcy);
	        MainButton->mask[i].length = (short) (double)
			 ((double) MainButton->mask[i].length * fcx);
		}
}



BOOL InitNewInstance(HANDLE hInstance, int nCmdShow)
{
        RECT rect;
        RECT rect1;
        HCURSOR oldcursor;
		TEXTMETRIC tm;
		HDC hdc;
		double fcx, fcy;
        extern mfont ListFont;
		extern form lFussform;
		extern form EtiButton;
		extern form EtiSpezButton;
		extern form LgrSpezButton;
		extern form ZusatzButton;
        extern form fScanform;
        extern mfont lScanFont;

        hMainWindow = CreateWindow (lpszClassName,
                                    lpszMainTitle,
                                    WS_DLGFRAME | WS_CAPTION | WS_SYSMENU |
                                    WS_MINIMIZEBOX,
                                    0, 0,
                                    800, 580,
                                    NULL,
                                    NULL,
                                    hInstance,
                                    NULL);
        if (hMainWindow == 0) return FALSE;

        ShowWindow(hMainWindow, SW_SHOWMAXIMIZED);

        enterpass = 0;
        hdc = GetDC (hMainWindow);
		GetTextMetrics (hdc, &tm);
		ReleaseDC (hMainWindow, hdc);

		GetWindowRect (hMainWindow, &rect);
		rect.bottom -= (tm.tmHeight + tm.tmHeight / 2);
        ShowWindow(hMainWindow, nCmdShow);
        MoveWindow (hMainWindow, rect.left,
                                rect.top, rect.right, rect.bottom, TRUE);


        fcx = fcy = 1;
        int xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
        int yfull = GetSystemMetrics (SM_CYFULLSCREEN);        
        if (rect.right > 900 || rect.right < 700)
        {
		            fcx = (double) rect.right / 800;
 		            fcy = (double) rect.bottom / 562;
//		            fcx = (double) xfull / 800;
// 		            fcy = (double) yfull / 600;
                    hKorrMainButton (fcx, fcy);
		            KorrMainButton (&MainButton, fcx, fcy);
		            KorrMainButton2 (&MainButton2, fcx, fcy);
		            KorrMainButton2 (&lFussform, fcx, fcy);
		            KorrMainButton2 (&fScanform, fcx, fcy);
					ListFont.FontHeight = (int) (double)
						((double)  ListFont.FontHeight * fcy);
					ListFont.FontWidth = (int) (double)
						((double)  ListFont.FontWidth * fcx);
					EtiButton.mask[0].pos[1] = (int)
						(double) ((double) EtiButton.mask[0].pos[1] *
						         fcx);
					EtiButton.mask[1].pos[1] = (int)
						(double) ((double) EtiButton.mask[1].pos[1] *
						         fcx);
					EtiSpezButton.mask[0].pos[1] = (int)
						         (double) ((double) EtiSpezButton.mask[0].pos[1] *
						                    fcx);
					LgrSpezButton.mask[0].pos[1] = (int)
						         (double) ((double) LgrSpezButton.mask[0].pos[1] *
						                    fcx);
					LgrSpezButton.mask[1].pos[1] = (int)
						         (double) ((double) LgrSpezButton.mask[1].pos[1] *
						                    fcx);
					LgrSpezButton.mask[2].pos[1] = (int)
						         (double) ((double) LgrSpezButton.mask[2].pos[1] *
						                    fcx);
					ZusatzButton.mask[0].pos[1] = (int)
						         (double) ((double) ZusatzButton.mask[0].pos[1] *
						                    fcx);
					lScanFont.FontHeight = 320;
        }

		scrfx = fcx;
		scrfy = fcy;
        PasswScreenParams (scrfx, scrfy);
        hFitWindow = hMainWindow;
		NuBlock.MainInstance (hMainInst, hMainWindow);
		NuBlock.ScreenParam (scrfx, scrfy);
		Touchf.MainInstance (hMainInst, hMainWindow);
		Touchf.ScreenParam (scrfx, scrfy);
		Touchf.SetWriteParams (WriteParamKiste, WriteParamEZ,
			                   WriteParamEV, WriteParamArt);


//        ShowWindow(hMainWindow, nCmdShow);
//        UpdateWindow(hMainWindow);

        hFitWindow = hMainWindow;

        oldcursor = SetCursor (LoadCursor (NULL, IDC_WAIT));
        strcpy (menue.prog, "00000");
        strcpy (menue.prog, "00000");
        SetCursor (oldcursor);

        GetClientRect (hMainWindow,  &rect);

        Sebmpw = (int) (double) ((double) (fitbm.bmWidth + 40) *
                                           scrfx);
        BackWindow1  = CreateWindowEx (
                              WS_EX_CLIENTEDGE,
                              "Backwind",
                              "",
                              WS_CHILD | WS_VISIBLE,
                              10, 10,
/*
                              rect.right - (fitbm.bmWidth + 40),
                              rect.bottom - 100,
*/
                              rect.right - Sebmpw,
                              rect.bottom - 100,
                              hMainWindow,
                              NULL,
                              hInstance,
                              NULL);


        enterpass = 1;
		if (PassWord)
		{
            if (! EnterPasswTouch (hInstance, BackWindow1))
			{
                      enterpass = 0;
                      return FALSE;
			}
			if (TimerPassWord) SetTimer (hMainWindow , TIMER_PASSW, TimerPassWord * 1000, 0);  //FISCH-test
		}

        GetClientRect (BackWindow1,  &rect1);
        Sebmpw = (int) (double) ((double) (fitbm.bmWidth + 25) *
                                           scrfx);

        BackWindow2  = CreateWindowEx (
                              WS_EX_CLIENTEDGE,
                              "Backwind",
                              "",
                              WS_CHILD | WS_VISIBLE,
//                              rect.right - (fitbm.bmWidth + 25),
                              rect.right - Sebmpw,
                              10,
//                              fitbm.bmWidth + 15,
                              (int) (double) ((double) (fitbm.bmWidth + 15) * scrfx),
                              rect.bottom - 20,
                              hMainWindow,
                              NULL,
                              hInstance,
                              NULL);

        BackWindow3  = CreateWindowEx (
                              WS_EX_CLIENTEDGE,
                              "Backwind",
                              "",
                              WS_CHILD | WS_VISIBLE,
                              10,
                              rect.bottom - (int) ((double) 60 * scrfy),
//                              rect.bottom - 60,
//                              rect.right - (fitbm.bmWidth + 40),
                              rect.right - (int) (double) ((double) (fitbm.bmWidth + 40) *
                                           scrfx),
//                              rect.bottom - 20 - (rect.bottom - 70),
                              (int) ((double) (rect.bottom - 20 - (rect.bottom - 70)) * scrfy),
                              hMainWindow,
                              NULL,
                              hInstance,
                              NULL);


        LogoWindow  = CreateWindowEx (
                              WS_EX_CLIENTEDGE,
                              "LogoWind",
                              "",
                              WS_CHILD | WS_VISIBLE,
                              5, 5,
                              rect1.right - 10,
                              rect1.bottom - 10,
                              BackWindow1,
                              NULL,
                              hInstance,
                              NULL);
        UpdateWindow (BackWindow1);
        UpdateWindow (BackWindow2);
        UpdateWindow (BackWindow3);
        UpdateWindow (LogoWindow);
		NuBlock.SetParent (LogoWindow);
        return TRUE;
}

int CreateFitWindow (void)
/**
FitWindow erzeugen.
**/
{

        FitWindow = CreateWindow   ("Fitlogo",
                                    "",
                                    WS_CHILD | WS_VISIBLE | WS_DLGFRAME,
                                    5, 5,
//                                    fitbm.bmWidth, fitbm.bmHeight,
                                    (int) (double) ((double) fitbm.bmWidth * scrfx),
									(int) (double) ((double) fitbm.bmHeight * scrfy),
                                    BackWindow2,
                                    NULL,
                                    hMainInst,
                                    NULL);
        fithdc = GetDC (FitWindow);
        if (IsFhs == 0)
        {
                    SetTimer (FitWindow , 1, twait, 0);
        }
        return TRUE;
}

int CreateUhrWindow (void)
/**
UhrWindow erzeugen.
**/
{
        RECT rect;
        int x,y, cx, cy;

        GetClientRect (LogoWindow,  &rect);
        cx = fitbm.bmWidth;
        cy = 13;
        x = 5;
        y = fitbm.bmHeight + 5;

        UhrWindow = CreateWindowEx (
                                    0,
                                    "Uhr",
                                    "",
                                    WS_CHILD | WS_VISIBLE,
                                    x, y,
                                    cx, cy,
                                    BackWindow2,
                                    NULL,
                                    hMainInst,
                                    NULL);
        SetTimer (UhrWindow , 1, twaitU, 0);
        return TRUE;
}


int IsHotKey (MSG *msg)
/**
HotKey Testen.
**/
{
         UCHAR taste;
         int i;
         ColButton *ColBut;
         char *pos;

         if (msg->message != WM_CHAR)
         {
                      return FALSE;
         }

         taste = (char) msg->wParam;
         for (i = 0; i < MainButton.fieldanz; i ++)
         {
                      ColBut = (ColButton *) MainButton.mask[i].feld;
                      if (ColBut->text1)
                      {
                           if ((pos = strchr (ColBut->text1, '&')) &&
                              ((UCHAR) toupper (*(pos + 1)) == toupper (taste)))
                           {
                                   SetFocus (MainButton.mask[i].feldid);
                                   AktButton = GetAktButton ();
                                   SendMessage (AktivWindow,WM_KEYDOWN,
                                                VK_RETURN, 0);
                                   return TRUE;
                           }
                      }
         }

         for (i = 0; i < MainButton2.fieldanz; i ++)
         {
                      ColBut = (ColButton *) MainButton2.mask[i].feld;
                      if (ColBut->text1)
                      {
                           if ((pos = strchr (ColBut->text1, '&')) &&
                              ((UCHAR) toupper (*(pos + 1)) == toupper (taste)))
                           {
                                   SetFocus (MainButton2.mask[i].feldid);
                                   AktButton = GetAktButton ();
                                   return TRUE;
                           }
                      }
         }
         return FALSE;
}

void PrevKey (void)
/**
Vorhergenden Button aktivieren.
**/
{

         if (AktButton >= MainButton.fieldanz) return;
         AktButton = GetAktButton ();
         if (AktButton == 0)
         {
                     AktButton = MainButton.fieldanz - 1;
         }
         else
         {
                     AktButton --;
         }
         if (AktButton < MainButton.fieldanz)
         {
               SetFocus (
                 MainButton.mask [AktButton].feldid);
         }
         else
         {
               SetFocus (
                 MainButton2.mask [AktButton - MainButton.fieldanz].feldid);
         }
}


void NextKey (void)
/**
Naechsten Button aktivieren.
**/
{
	     ColButton *Col;

         if (AktButton >= MainButton.fieldanz) return;

         AktButton = GetAktButton ();
         if (AktButton == MainButton.fieldanz - 1)
         {
                     AktButton = 0;
         }
         else
         {
                     AktButton ++;
         }

         if (AktButton < MainButton.fieldanz)
         {
 		       Col = (ColButton *) MainButton.mask[AktButton].feld;
               SetFocus (
                 MainButton.mask [AktButton].feldid);
         }
         else
         {
               SetFocus (
                 MainButton2.mask [AktButton - MainButton.fieldanz].feldid);
         }
}

void RightKey (void)
/**
Button nach rechts.
**/
{

         AktButton = GetAktButton ();
         if (AktButton <  MainButton.fieldanz)
         {
                     AktButton = MainButton.fieldanz +
                                 MainButton2.fieldanz - 5;
         }
         else if (AktButton == MainButton.fieldanz)
         {
                     AktButton = MainButton.fieldanz - 1;
         }
         else
         {
                     AktButton --;
         }
         if (AktButton < MainButton.fieldanz)
         {
               SetFocus (
                 MainButton.mask [AktButton].feldid);
         }
         else
         {
               SetFocus (
                 MainButton2.mask [AktButton - MainButton.fieldanz].feldid);
         }
}

void LeftKey (void)
/**
Button nach rechts.
**/
{

         AktButton = GetAktButton ();
         if (AktButton <  MainButton.fieldanz)
         {
                     AktButton = MainButton.fieldanz;
         }
         else if (AktButton == MainButton.fieldanz +
                               MainButton2.fieldanz - 5)
         {
                     AktButton = MainButton.fieldanz - 1;
         }
         else
         {
                     AktButton ++;
         }

         if (AktButton < MainButton.fieldanz)
         {
               SetFocus (
                 MainButton.mask [AktButton].feldid);
         }
         else
         {
               SetFocus (
                 MainButton2.mask [AktButton - MainButton.fieldanz].feldid);
         }
}


void ActionKey (void)
/**
Button-Aktion ausfuehren.
**/
{
         field *feld;

         AktButton = GetAktButton ();
         feld = GetAktField ();

         if (feld->after != (int (*) (void)) 0)
         {
                    (*feld->after) ();
         }
         else if (feld->BuId != 0)
         {
                    SendMessage (GetParent (feld->feldid), WM_COMMAND,
                                            MAKELONG (feld->BuId,
                                            feld->feldid),
                                            0l);
         }
}


int IsKeyPress (MSG *msg)
/**
HotKey Testen.
**/
{
         static char keypressed = 0;
         static HWND keyhwnd = 0;
         int taste;
         int i;
         ColButton *ColBut;
         char *pos;



         switch (msg->message)
         {
              case WM_KEYDOWN :
              {
                     if (keypressed)
                     {
                                  return FALSE;
                     }
                     taste = (int) msg->wParam;
                     if (taste == VK_UP)
                     {
                               PrevKey ();
                               return TRUE;
                     }
                     if (taste == VK_DOWN)
                     {
                               NextKey ();
                               return TRUE;
                     }
                     if (taste == VK_RIGHT)
                     {
                               RightKey ();
                               return TRUE;
                     }
                     if (taste == VK_LEFT)
                     {
                               LeftKey ();
                               return TRUE;
                     }
                     if (taste == VK_TAB)
                     {
                         if (GetKeyState (VK_SHIFT) < 0)
                         {
                              PrevKey ();
                              return TRUE;
                         }
                         NextKey ();
                         return TRUE;
                     }
                     if (taste == VK_RETURN)
                     {
                               ActionKey ();
                               return TRUE;
                     }
                     for (i = 0; i < MainButton.fieldanz; i ++)
                     {
                         ColBut = (ColButton *) MainButton.mask[i].feld;
                         if (ColBut->text1)
                         {
                              if ((pos = strchr (ColBut->text1, '&')) &&
                                 ((UCHAR) toupper (*(pos + 1)) == taste))
                              {
                                        keypressed = i + 1;
                                        keyhwnd = MainButton.mask[i].feldid;
                                        SendMessage (keyhwnd,
                                                WM_LBUTTONDOWN,
                                                0l,
                                                0l);
                                        return TRUE;
                              }
                         }
                    }

                    for (i = 0; i < MainButton2.fieldanz; i ++)
                    {
                         ColBut = (ColButton *) MainButton2.mask[i].feld;
                         if (ColBut->text1)
                         {
                              if ((pos = strchr (ColBut->text1, '&')) &&
                                 ((UCHAR) toupper (*(pos + 1)) == taste))
                              {
                                         keypressed = i + MainButton.fieldanz;
                                         keyhwnd = MainButton2.mask[i].feldid;
                                         SendMessage (keyhwnd,
                                                WM_LBUTTONDOWN,
                                                0l,
                                                0l);
                                         return TRUE;
                              }
                         }
                      }
              }
              case WM_KEYUP :
              {
                     if (!keypressed)
                     {
                                  return FALSE;
                     }
                     taste = (int) msg->wParam;
                     for (i = 0; i < MainButton.fieldanz; i ++)
                     {
                         ColBut = (ColButton *) MainButton.mask[i].feld;
                         if (ColBut->text1)
                         {
                             if ((pos = strchr (ColBut->text1, '&')) &&
                                ((UCHAR) toupper (*(pos + 1)) == taste))
                             {
                                   if (keyhwnd != MainButton.mask[i].feldid)
                                   {
                                                return FALSE;
                                   }
                                   keypressed = 0;
                                   keyhwnd = MainButton.mask[i].feldid;
                                   SendMessage (keyhwnd,
                                                WM_LBUTTONUP,
                                                0l,
                                                0l);
                                   keyhwnd = NULL;
                                   return TRUE;
                            }
                         }
                    }

                    for (i = 0; i < MainButton2.fieldanz; i ++)
                    {
                         ColBut = (ColButton *) MainButton2.mask[i].feld;
                         if (ColBut->text1)
                         {
                              if ((pos = strchr (ColBut->text1, '&')) &&
                                 ((UCHAR) toupper (*(pos + 1)) == taste))
                              {
                                   if (keyhwnd != MainButton2.mask[i].feldid)
                                   {
                                                return FALSE;
                                   }
                                   keypressed = 0;
                                   keyhwnd = MainButton2.mask[i].feldid;
                                   SendMessage (keyhwnd,
                                                WM_LBUTTONUP,
                                                0l,
                                                0l);
                                   keyhwnd = NULL;
                                   return TRUE;
                            }
                         }
                      }
             }
         }
         return FALSE;
}


int     ProcessMessages(void)
{
       MSG msg;

        while (GetMessage (&msg, NULL, 0, 0))
        {
              if (IsMouseMessage (&msg));
              else if (IsHotKey (&msg));
              else if (IsKeyPress (&msg));
              else
              {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
              }
        }
        DestroyFonts ();
	    SetForeground ();
		closedbase ();
        return msg.wParam;
}


LONG FAR PASCAL WndProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        PAINTSTRUCT ps;
		HDC hdc;

        switch(msg)
        {
              case WM_TIMER :
                    switch (wParam)
                    {
                        case TIMER_PASSW :  //FISCH-test
						   KillTimer (hMainWindow, TIMER_PASSW); //FISCH-test
						   EnterPasswTouch (NULL, BackWindow1);
			  			   SetTimer (hMainWindow , TIMER_PASSW, TimerPassWord * 1000, 0);  //FISCH-test
                           return 0;
                    }
                    break;

              case WM_PAINT :
                    if (hWnd == BackWindow2)
                    {
                             if (FitWindow == 0)
                             {
                                        CreateFitWindow ();
                             }
                             display_form (BackWindow2,&MainButton, 0, 0);
                    }
                    else if (hWnd == BackWindow3)
                    {
                             if (MainButton2.mask[0].feldid == 0)
                             {
                                  display_form (BackWindow3,&MainButton2, 0, 0);
                             }
                    }
                    else if (hWnd == FitWindow)
                    {
                           hdc = BeginPaint (hWnd, &ps);
                           paintlogo (hdc, fitbmp, 0, 0, SRCCOPY);
                           EndPaint (hWnd, &ps);
                    }
                    break;
              case WM_KEYDOWN :
                         switch (wParam)
                         {
                              case VK_F5 :
                                      func5 ();
                                      return 0;

                         }
                         break;
              case WM_COMMAND :
                    if (LOWORD (wParam) == KEY5)
                    {
                            syskey = KEY5;
                            SendKey (VK_F5);
                            break;
                    }
                    else if (HIWORD (wParam) == LBN_SELCHANGE)
                    {
						    DrawNoteIcon ();
                    }
                    return MenuJob(hWnd,wParam,lParam);
              case WM_DESTROY :
                    if (hWnd == hMainWindow)
                    {
	                          SetForeground ();
	                          closedbase ();
                              ExitProcess (0);
                             return 0;
                    }
                    return 0;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

LONG MenuJob(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
        HMENU hMenu;

        hMenu = GetMenu (hWnd);

        switch (wParam)
        {
              case CM_HROT :
                     logorx = (logorx + 1) % 2;
                     if (logorx)
                     {
                               CheckMenuItem (hMenu, CM_HROT, MF_CHECKED);
                               CheckMenuItem (hMenu, CM_VROT, MF_UNCHECKED);
                               logory = 0;
                     }
                     else
                     {
                               CheckMenuItem (hMenu, CM_HROT, MF_UNCHECKED);
                     }
                     sign = signx = 1;
                     xstretch = ystretch = 0;
                     return 0;
              case CM_VROT :
                     logory = (logory + 1) % 2;
                     if (logory)
                     {
                               CheckMenuItem (hMenu, CM_VROT, MF_CHECKED);
                               CheckMenuItem (hMenu, CM_HROT, MF_UNCHECKED);
                               logorx = 0;
                     }
                     else
                     {
                               CheckMenuItem (hMenu, CM_VROT, MF_UNCHECKED);
                     }
                     sign = signx = 1;
                     xstretch = ystretch = 0;
                     return 0;
              case CM_PROGRAMMENDE :
                     DeleteObject (fitbmp);
                     DestroyWindow (hMainWindow);
                     KillTimer (FitWindow, 1);
                     ReleaseDC (FitWindow, fithdc);
                     PostQuitMessage(0);
                     return 0;
        }
        return (0);
}

LONG FAR PASCAL UhrProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        switch(msg)
        {
              case WM_TIMER :
                    switch (wParam)
                    {
                        case 1 :
                           sysdate (datum);
                           systime (zeit);
                           sprintf (datumzeit, "%s %s", datum, zeit);
                           InvalidateRect (UhrWindow, NULL, TRUE);
                           UpdateWindow (UhrWindow);
                           return 0;
                    }
                    break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

LONG FAR PASCAL FitLogoProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        PAINTSTRUCT ps;

        switch(msg)
        {
              case WM_TIMER :
                    switch (wParam)
                    {
                        case 1 :
                           printlogo (fitbmp, 0, 0, SRCCOPY);
                           return 0;
                    }
                    break;
              case WM_PAINT :
                    if (IsFhs)
                    {
                                fithdc = BeginPaint (hWnd, &ps);
                                strechbmp (fitbmp, 0, 0, SRCCOPY);
                                EndPaint (hWnd, &ps);
                    }
                    break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}


/** Ab hier Eingabe einer Nummer in einem extra Fenster mit
    numerischen Block fuer die Maus.
**/



static form *eForm = NULL;
static HWND ehWnd = NULL;
static int (*eFormProc) (WPARAM) = NULL;

static HWND eNumWindow;
static break_num_enter = 0;

mfont EditNumFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                                         RGB (255, 255, 255),
                                         1,
                                         NULL};

/*
mfont TextNumFont = {"Arial", 200, 0, 1, RGB (0, 0, 0),
                                         RGB (255, 255, 255),
                                         1,
                                         NULL};
*/

mfont TextNumFont = {"Arial", 200, 0, 1, BLACKCOL,
                                         LTGRAYCOL,
                                         1,
                                         NULL};

mfont numfieldfont = {"", 150, 0, 1,
                                       RGB (255, 255, 255),
                                       BLUECOL,
                                       1,
                                       NULL};

mfont ctrlfont = {"", 150, 0, 1,
                                       RGB (255, 255, 255),
                                       BLUECOL,
                                       1,
                                       NULL};

mfont stornofont = {"", 150, 0, 1,
                                       RGB (255, 0, 0),
                                       BLUECOL,
                                       1,
                                       NULL};

mfont OKfont = {"", 150, 0, 1,
                                       RGB (255, 255, 255),
                                       BLUECOL,
                                       1,
                                       NULL};

mfont Cafont = {"", 90, 0, 1,
                                       RGB (255, 255, 255),
                                       REDCOL,
                                       1,
                                       NULL};

ColButton Num1      =   {"1", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton Num2      =   {"2", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton Num3      =   {"3", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton Num4      =   {"4", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton Num5      =   {"5", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton Num6      =   {"6", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton Num7      =   {"7", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton Num8      =   {"8", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton Num9      =   {"9", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton Num0      =   {"0", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton NumP     =   {",", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                        14};

ColButton NumM     =   {"-", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};


ColButton NumLeft   =   {"", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         pfeill, -1, -1,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton NumRight   =   {"", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         pfeilr, -1, -1,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton NumDel     =   {"DEL", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         REDCOL,
                         14};

ColButton NumPlus   =   {"+", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton NumMinus   =   {"-", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton NumCls    =   {"C", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         REDCOL,
                         14};
ColButton NumAbbruch  =   {"Abbruch", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         REDCOL,
                         14};

ColButton NumOK       =   {"OK", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         BLACKCOL,
                         GREENCOL,
                         14};

ColButton BuStorno =   {"&Storno", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         REDCOL,
                         14};


static int nbsb  = 85;

static int nbsx   = 5;
int ny    = 10;
/*
static int nbss0 = 80;
static int nbss  = 64;
static int nbsz  = 40;
static int nbsz0 = 64;
*/
static int nbss0 = 64;
static int nbss  = 64;
static int nbsz  = 40;
static int nbsz0 = 64;
static int nAktButton = 0;

static int donum1 ();
static int donum2 ();
static int donum3 ();
static int donum4 ();
static int donum5 ();
static int donum6 ();
static int donum7 ();
static int donum8 ();
static int donum9 ();
static int donum0 ();
static int donump ();
static int donumm ();

static int doleft ();
static int doright ();
static int dodel ();
static int doabbruch ();
static int doOK ();
static int doplus ();
static int dominus ();
static int docls ();

static field _NumField[] = {
(char *) &Num1,          nbss0, nbsz0, ny, nbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum1, 0,
(char *) &Num2,          nbss0, nbsz0, ny, nbsx + 2 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum2, 0,
(char *) &Num3,          nbss0, nbsz0, ny, nbsx + 3 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum3, 0,
(char *) &Num4,          nbss0, nbsz0, ny + 1 * nbsz0, nbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum4, 0,
(char *) &Num5,          nbss0, nbsz0, ny + 1 * nbsz0, nbsx + 2 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum5, 0,
(char *) &Num6,          nbss0, nbsz0, ny + 1 * nbsz0, nbsx + 3 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum6, 0,
(char *) &Num7,          nbss0, nbsz0, ny + 2 * nbsz0, nbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum7, 0,
(char *) &Num8,          nbss0, nbsz0, ny + 2 * nbsz0, nbsx + 2 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum8, 0,
(char *) &Num9,          nbss0, nbsz0, ny + 2 * nbsz0, nbsx + 3 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum9, 0,
(char *) &Num0,          nbss0, nbsz0, ny + 2 * nbsz0, nbsx, 0, "",
                                                           COLBUTTON, 0,
                                                           donum0, 0,
(char *) &NumP,          nbss0, nbsz0, ny + 1 * nbsz0, nbsx, 0, "",
                                                           COLBUTTON, 0,
                                                           donump, 0,
(char *) &NumM,          nbss0, nbsz0, ny, nbsx, 0, "",
                                                           COLBUTTON, 0,
                                                           donumm, 0};

static form NumField = {12, 0, 0, _NumField, 0, 0, 0, 0, &numfieldfont};

/*
static int cbss0 = 80;
static int cbss  = 64;
static int cbsz  = 40;
static int cbsz0 = 64;
*/

static int cbss0 = 64;
static int cbss  = 64;
static int cbsz  = 40;
static int cbsz0 = 64;
static int cbsx = nbsx + 5 * nbss0;

static field _NumControl1[] = {
(char *) &NumLeft,       cbss0, cbsz0, ny, cbsx,             0, "",
                                                           COLBUTTON, 0,
                                                           doleft, 0,
(char *) &NumRight,       cbss0, cbsz0, ny, cbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           doright, 0,
(char *) &NumDel,         cbss0, cbsz0, ny, cbsx            , 0, "",
                                                           COLBUTTON, 0,
                                                           dodel, 0,
(char *) &NumAbbruch,     cbss0, cbsz0, ny, cbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           doabbruch, 0};

static form NumControl1 = {3, 0, 0, _NumControl1, 0, 0, 0, 0, &ctrlfont};

static field _NumControl2[] = {
(char *) &NumPlus,       cbss0, cbsz0, ny, cbsx,             0, "",
                                                           COLBUTTON, 0,
                                                           doplus, 0,
(char *) &NumMinus,       cbss0, cbsz0, ny, cbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           dominus, 0,
(char *) &NumCls,         cbss0, cbsz0, ny, cbsx            , 0, "",
                                                           COLBUTTON, 0,
                                                           docls, 0,
(char *) &NumAbbruch,     cbss0, cbsz0, ny, cbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           doabbruch, 0};

static form NumControl2 = {3, 0, 0, _NumControl2, 0, 0, 0, 0, &ctrlfont};

static form NumControl;


static field _CaControl[] = {
(char *) &NumAbbruch,     cbss0, cbsz0, ny, cbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           doabbruch, 0};
static form CaControl = {1, 0, 0, _CaControl, 0, 0, 0, 0, &Cafont};

static field _OKControl[] = {
(char *) &NumOK,         cbss0 * 2,
                                cbsz0, ny, cbsx,             0, "",
                                                           COLBUTTON, 0,
                                                             doOK, 0};
static form OKControl = {1, 0, 0, _OKControl, 0, 0, 0, 0, &OKfont};


static char editbuff [40];
static double editsum;
static char eaction = ' ';

static field _EditForm [] = {
editbuff,             0, 0, 0, 0, 0, "", EDIT, 0, 0, 0};

static form EditForm = {1, 0, 0, _EditForm, 0, 0, 0, 0, &EditNumFont};


static field _TextForm [] = {
editbuff,             0, 0, 0, 0, 0, "", READONLY, 0, 0, 0};

static form TextForm = {1, 0, 0, _TextForm, 0, 0, 0, 0, &TextNumFont};


static field _StornoForm [] = {
(char *) &BuStorno,       100, 64, 0, 0, 0, "", COLBUTTON, 0, 0, 0};

static form StornoForm = {1, 0, 0, _StornoForm, 0, 0, 0, 0, &stornofont};


void SetStorno (int (*sproc) (void), int BuId)
/**
Procedure fuer Storno Setzen.
**/
{
           StornoForm.mask[0].after = sproc;
           StornoForm.mask[0].BuId  = BuId;
}

int donum1 ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '1', 0l);
        return 1;
}

int donum2 ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '2', 0l);
        return 1;
}

int donum3 ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '3', 0l);
        return 1;
}

int donum4 ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '4', 0l);
        return 1;
}

int donum5 ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '5', 0l);
        return 1;
}

int donum6 ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '6', 0l);
        return 1;
}

int donum7 ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '7', 0l);
        return 1;
}

int donum8 ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '8', 0l);
        return 1;
}

int donum9 ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '9', 0l);
        return 1;
}

int donum0 ()
/**
Aktion bei Button ,
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '0', 0l);
        return 1;
}

int donump ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '.', 0l);
        return 1;
}

int donumm ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '-', 0l);
        return 1;
}


int doleft ()
/**
Aktion bei Button links
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_KEYDOWN,
                    (WPARAM) VK_LEFT, 0l);
        return 1;
}

int doright ()
/**
Aktion bei Button rechts
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_KEYDOWN,
                    (WPARAM) VK_RIGHT, 0l);
        return 1;
}

int dodel ()
/**
Aktion bei Button rechts
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_KEYDOWN,
                    (WPARAM) VK_DELETE, 0l);
        return 1;
}


int docls (void)
/**
EditFenster loeschen.
**/
{
        memset (editbuff, ' ', 39);
        editbuff [39] = (char) 0;
		editsum = (double) 0.0;
		strcpy (editbuff, "");
		CloseControls (&EditForm);
        create_enter_form (eNumWindow, &EditForm, 0, 0);
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
		return 1;
}

int doplus (void)
/**
EditFenster loeschen.
**/
{
	    GetWindowText (EditForm.mask[0].feldid, editbuff, 39);
		if (eaction == '+')
		{
		        editsum += ratod (editbuff);
		}
		else if (eaction == '-')
		{
		        editsum -= ratod (editbuff);
		}
		else
		{
		        editsum = ratod (editbuff);
		}
		eaction = '+';
	    sprintf (editbuff, "%lf", editsum);
		CloseControls (&EditForm);
        create_enter_form (eNumWindow, &EditForm, 0, 0);
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
		return 1;
}

int dominus (void)
/**
EditFenster loeschen.
**/
{
	    GetWindowText (EditForm.mask[0].feldid, editbuff, 39);
		if (eaction == '+')
		{
		        editsum += ratod (editbuff);
		}
		else if (eaction == '-')
		{
		        editsum -= ratod (editbuff);
		}
		else
		{
		        editsum = ratod (editbuff);
		}

		eaction = '-';
	    sprintf (editbuff, "%lf", editsum);
		CloseControls (&EditForm);
        create_enter_form (eNumWindow, &EditForm, 0, 0);
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
		return 1;
}

int dogleich (void)
/**
EditFenster loeschen.
**/
{
	    GetWindowText (EditForm.mask[0].feldid, editbuff, 39);
		if (eaction == '+')
		{
		        editsum += ratod (editbuff);
		}
		else if (eaction == '-')
		{
		        editsum -= ratod (editbuff);
		}
		else
		{
		        editsum = ratod (editbuff);
		}

		eaction = ' ';
	    sprintf (editbuff, "%lf", editsum);
		CloseControls (&EditForm);
        create_enter_form (eNumWindow, &EditForm, 0, 0);
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
		return 1;
}

int doabbruch ()
/**
Aktion bei Button F5
**/
{
        SetFocus (EditForm.mask[0].feldid);
        PostMessage (EditForm.mask[0].feldid, WM_KEYDOWN,
                    (WPARAM) VK_F5, 0l);
        return 1;
}

int doOK ()
/**
Aktion bei Button rechts
**/
{
	    if (CalcOn && eaction != ' ')
		{
			      dogleich ();
				  return 1;
		}
        SetFocus (EditForm.mask[0].feldid);
        PostMessage (EditForm.mask[0].feldid, WM_KEYDOWN,
                    (WPARAM) VK_RETURN, 0l);
        return 1;
}

void NumEnterBreak ()
/**
Endeflag fuer Erfassung setzen.
**/
{
        break_num_enter = 1;
}

HWND IsNumChild (POINT *mousepos)
/**
Test, ob sich der Maus-Cursor in einem KindFensert befinden.
**/
{
        int i;

        for (i = 0; i < NumField.fieldanz; i ++)
        {
                   if (MouseinWindow (NumField.mask[i].feldid,
                                      mousepos))
                   {
                                 return (NumField.mask[i].feldid);
                   }
        }

        for (i = 0; i < NumControl.fieldanz; i ++)
        {
                   if (MouseinWindow (NumControl.mask[i].feldid,
                                      mousepos))
                   {
                                 return (NumControl.mask[i].feldid);
                   }
        }

        for (i = 0; i < OKControl.fieldanz; i ++)
        {
                   if (MouseinWindow (OKControl.mask[i].feldid,
                                      mousepos))
                   {
                                 return (OKControl.mask[i].feldid);
                   }
        }
        for (i = 0; i < CaControl.fieldanz; i ++)
        {
                   if (MouseinWindow (CaControl.mask[i].feldid,
                                      mousepos))
                   {
                                 return (CaControl.mask[i].feldid);
                   }
        }

        for (i = 0; i < EditForm.fieldanz; i ++)
        {
                   if (MouseinWindow (EditForm.mask[i].feldid,
                                      mousepos))
                   {
                                 return (EditForm.mask[i].feldid);
                   }
        }
        if (StornoForm.mask[0].feldid == 0)
        {
                   return FALSE;
        }

        for (i = 0; i < StornoForm.fieldanz; i ++)
        {
                   if (MouseinWindow (StornoForm.mask[i].feldid,
                                      mousepos))
                   {
                                 return (StornoForm.mask[i].feldid);
                   }
        }
        return NULL;
}


LONG FAR PASCAL EntNumProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        POINT mousepos;
        HWND enchild;
        static int SendChild = 0;
        int HasFocus = 1;

        GetCursorPos (&mousepos);

        switch(msg)
        {
              case WM_PAINT :
/*
                    display_form (eNumWindow, &NumField, 0, 0);
                    display_form (eNumWindow, &NumControl, 0, 0);
                    display_form (eNumWindow, &OKControl, 0, 0);
                    display_form (eNumWindow, &TextForm, 0, 0);
                    display_form (eNumWindow, &EditForm, 0, 0);
                    display_form (eNumWindow, &CaControl, 0, 0);
*/

				    if (textblock)
					{
						textblock->OnPaint (hWnd, msg, wParam, lParam);
					}
					else
					{
						display_form (eNumWindow, &NumField, 0, 0);
						display_form (eNumWindow, &NumControl, 0, 0);
						display_form (eNumWindow, &OKControl, 0, 0);
						display_form (eNumWindow, &TextForm, 0, 0);
						display_form (eNumWindow, &EditForm, 0, 0);
						display_form (eNumWindow, &CaControl, 0, 0);

						if (StornoForm.mask[0].after)
						{
                              display_form (eNumWindow, &StornoForm, 0, 0);
						}	
						else if (StornoForm.mask[0].BuId)
						{
                              display_form (eNumWindow, &StornoForm, 0, 0);
						}

						SetFocus (EditForm.mask[0].feldid);
					}
					break;
/*
                        display_form (eNumWindow, &NumField, 0, 0);
                        display_form (eNumWindow, &NumControl, 0, 0);
                        display_form (eNumWindow, &OKControl, 0, 0);
					    if (eForm == NULL)
						{
                             display_form (eNumWindow, &TextForm, 0, 0);
                             display_form (eNumWindow, &EditForm, 0, 0);
					         SendMessage (EditForm.mask[0].feldid, EM_SETSEL,
                                                 (WPARAM) 0, MAKELONG (-1, 0));
						}
					    else
						{
                             display_form (eNumWindow, &fAuswahlEinh, 0, 0);
						}

                        display_form (eNumWindow, &CaControl, 0, 0);


                        if (StornoForm.mask[0].after)
						{
                              display_form (eNumWindow, &StornoForm, 0, 0);
						}
                        else if (StornoForm.mask[0].BuId)
						{
                              display_form (eNumWindow, &StornoForm, 0, 0);
						}

                        if (ChoiseForm.mask[0].after)
						{
                              display_form (eNumWindow, &ChoiseForm, 0, 0);
						}
                        else if (ChoiseForm.mask[0].BuId)
						{
                              display_form (eNumWindow, &ChoiseForm, 0, 0);
						}

					    if (eForm == NULL)
						{
                               SetFocus (EditForm.mask[0].feldid);
						}
					}

                    break;
*/


              case WM_LBUTTONDOWN :
              case WM_LBUTTONUP :
              case WM_MOUSEMOVE :
				    if (textblock)
					{
						textblock->OnButton (hWnd, msg, wParam, lParam);
					}
					else
					{
						enchild = IsNumChild (&mousepos);
//                    MouseTest (enchild, msg);
						if (enchild)
						{
                              SendMessage (enchild, msg, wParam, lParam);
                              return TRUE;
						}
                    }
                    return TRUE;
              case WM_ACTIVATE :
                    switch (wParam)
                    {
                         case WA_ACTIVE :
                         case WA_CLICKACTIVE :
//                              SetCapture (eNumWindow);
                              break;
                    }
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

LONG FAR PASCAL EntNuProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{

        switch(msg)
        {
              case WM_PAINT :
				    NuBlock.OnPaint (hWnd, msg, wParam, lParam);
					break;

              case WM_LBUTTONDOWN :
              case WM_LBUTTONUP :
              case WM_MOUSEMOVE :
				    NuBlock.OnButton (hWnd, msg, wParam, lParam);
                    return TRUE;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}


LONG FAR PASCAL TouchfProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{

        switch(msg)
        {
		      case WM_DIRECT :
				    Touchf.StartDirect ();
				    return 0;
              case WM_PAINT :
/*
				    if (hWnd == NuBlock.eNumWindow)
					{
						NuBlock.OnPaint (hWnd, msg, wParam, lParam);
					}
					else
*/
					{
						Touchf.OnPaint (hWnd, msg, wParam, lParam);
					}
					break;

              case WM_LBUTTONDOWN :
              case WM_LBUTTONUP :
              case WM_MOUSEMOVE :
				    Touchf.OnButton (hWnd, msg, wParam, lParam);
                    return TRUE;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}


void RegisterNumEnter (void)
/**
Fenster fuer numerische Eingabe registrieren
**/
{
        static int registered = 0;
        WNDCLASS wc;

        if (registered == 0)
        {
                   wc.style =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                               | CS_OWNDC;
                   wc.cbClsExtra    =  0;
                   wc.cbWndExtra    =  0;
                   wc.hInstance     =  hMainInst;
                   wc.hIcon         =  LoadIcon (hMainInst, "FITICON");
                   wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                   wc.lpszMenuName  =  "";
                   wc.lpfnWndProc   =  EntNumProc;
                   wc.lpszMenuName  =  "";
                   wc.hbrBackground =  CreateSolidBrush (RGB (0, 0, 120));
                   wc.lpszClassName =  "EnterNum";
                   RegisterClass(&wc);
                   registered = 1;
        }
}

int IsNumKey (MSG *msg)
/**
HotKey Testen.
**/
{
         static char keypressed = 0;
         static HWND keyhwnd = 0;
         ColButton *ColBut;
         int taste;
         char *pos;

         switch (msg->message)
         {
	          case EM_SETSEL :
				  break;
              case WM_KEYDOWN :
              {
                     switch (msg->wParam)
                     {
                            case VK_F5 :
                                    NumEnterBreak ();
                                    syskey = KEY5;
                                    return TRUE;
                            case VK_DELETE :
								     docls ();
									 return TRUE;
                            case VK_RETURN :
								    if (CalcOn && eaction != ' ')
									{
										dogleich ();
										return TRUE;
									}
                                    NumEnterBreak ();
                                    GetWindowText (EditForm.mask[0].feldid,
                                                   EditForm.mask [0].feld,
                                                   EditForm.mask [0].length);
                                    syskey = KEYCR;
                                    return TRUE;
                     }
                     taste = (int) msg->wParam;
                     ColBut = (ColButton *) StornoForm.mask[0].feld;
                     if (ColBut->text1)
                     {
                           if ((pos = strchr (ColBut->text1, '&')) &&
                               ((UCHAR) toupper (*(pos + 1)) == taste))
                           {
                                        keypressed = 1;
                                        keyhwnd = StornoForm.mask[0].feldid;
                                        SendMessage (keyhwnd,
                                                WM_LBUTTONDOWN,
                                                0l,
                                                0l);
                                        return TRUE;
                            }
                     }
                     return FALSE;
              }
              case WM_KEYUP :
              {
                         taste = (int) msg->wParam;
                         ColBut = (ColButton *) StornoForm.mask[0].feld;
                         if (ColBut->text1)
                         {
                             if ((pos = strchr (ColBut->text1, '&')) &&
                                ((UCHAR) toupper (*(pos + 1)) == taste))
                             {
                                   if (keyhwnd != StornoForm.mask[0].feldid)
                                   {
                                                return FALSE;
                                   }
                                   keypressed = 0;
                                   keyhwnd = StornoForm.mask[0].feldid;
                                   SendMessage (keyhwnd,
                                                WM_LBUTTONUP,
                                                0l,
                                                0l);
                                   keyhwnd = NULL;
                                   return TRUE;
                            }
                         }
                         return FALSE;
              }
              case WM_CHAR :
              {
				     switch (msg->wParam)
					 {
					       case '+' :
								     doplus ();
									 return TRUE;
						   case '-' :
								     dominus ();
									 return TRUE;
					 }
                     SendMessage (EditForm.mask[0].feldid,
                                   msg->message,
                                   msg->wParam,
                                   msg->lParam);
                                   return TRUE;
               }
          }

          return FALSE;
}


void EnterNumBox (HWND hWnd, char *text, char *nummer, int nlen, char *pic)
/**
Fenster fuer numerische Eingabe oeffnen.
**/
{
        RECT rect;
        MSG msg;
        TEXTMETRIC tm;
        HFONT hFont;
        HDC hdc;
        int x, y, cx, cy;
        int i, j;
        int tlen;
        int elen, epos;
        int eheight;
        HCURSOR oldcursor;
        static int BitMapOK = 0;
        HWND shListBox;
        HWND slbox;
        struct LMENUE smenue;
		static BOOL NumKorr = FALSE;

		if (NumKorr == FALSE)
		{
			      NumKorr = TRUE;
				  nbsz0 = (int) (double) ((double) nbsz0 * scrfy);
				  nbss0 = (int) (double) ((double) nbss0 * scrfx);
		          KorrMainButton (&NumField, scrfx, scrfy);
		          KorrMainButton (&NumControl1, scrfx, scrfy);
		          KorrMainButton (&NumControl2, scrfx, scrfy);
		          KorrMainButton (&StornoForm, scrfx, scrfy);
		          KorrMainButton (&OKControl, scrfx, scrfy);
		          KorrMainButton (&CaControl, scrfx, scrfy);
				  ctrlfont.FontHeight = (int) (double)
					  ((double) ctrlfont.FontHeight * scrfy);
				  ctrlfont.FontWidth = (int) (double)
					  ((double) ctrlfont.FontWidth * scrfx);
		}

		if (CalcOn)
		{
			memcpy (&NumControl, &NumControl2, sizeof (form));
			editsum = (double) 0.0;
			eaction = ' ';
		}
		else
		{
			memcpy (&NumControl, &NumControl1, sizeof (form));
		}

        GetMenue (&smenue);
        MenTextSave (&smenue);
        shListBox = GethListBox ();
        slbox = Getlbox ();
        tlen = strlen (text);

        if (BitMapOK == 0)
        {
                 NumLeft.bmp = LoadBitmap (hMainInst, "pfeill");
                 NumRight.bmp = LoadBitmap (hMainInst, "pfeilr");
                 NumPlus.bmp  = LoadBitmap (hMainInst, "plus");
                 NumMinus.bmp = LoadBitmap (hMainInst, "minus");
                 NumCls.bmp = LoadBitmap (hMainInst, "clear");
                 BitMapOK = 1;
        }

        RegisterNumEnter ();
        GetWindowRect (LogoWindow, &rect);
        x = rect.left;
        y = (int) (double) ((double) rect.bottom - (3 * nbsz0) - (46 * scrfy));
        cx = rect.right - rect.left;
        cy = rect.bottom - y;

        ny = (int) (double) ((double) cy - (3 * nbsz0) - (30 * scrfy));
        for (i = 0,j = 0; j < NumField.fieldanz - 3; i ++,j += 3)
        {
                     NumField.mask[j].pos[0] = ny + i * nbsz0;
                     NumField.mask[j + 1].pos[0] = ny + i * nbsz0;
                     NumField.mask[j + 2].pos[0] = ny + i * nbsz0;
        }
        NumField.mask[j].pos[0] = ny + 2 * nbsz0;
        NumField.mask[j + 1].pos[0] = ny + 1 * nbsz0;
        NumField.mask[j + 2].pos[0] = ny;


        OKControl.mask[0].pos[0]  = ny;

        NumControl.mask[0].pos[0] = ny + 1 * nbsz0;
        NumControl.mask[1].pos[0] = ny + 1 * nbsz0;
        NumControl.mask[2].pos[0] = ny + 2 * nbsz0;
        NumControl.mask[3].pos[0] = ny + 2 * nbsz0;
        CaControl.mask[0].pos[0]  = ny + 2 * nbsz0;

        cbsx = cx - 2 * nbss0 - 5;
        OKControl.mask[0].pos[1]  = cbsx;

        NumControl.mask[0].pos[1] = cbsx;
        NumControl.mask[1].pos[1] = cbsx + nbss0;
        NumControl.mask[2].pos[1] = cbsx;
        NumControl.mask[3].pos[1] = cbsx + nbss0;
        CaControl.mask[0].pos[1] = cbsx + nbss0;

        epos = NumControl.mask[0].pos[1] - (NumField.mask[2].pos[1] + nbss0);
        EditNumFont.FontHeight = 200;
        while (TRUE)
        {
                 spezfont (&EditNumFont);
                 hFont = SetWindowFont (hMainWindow);
                 hdc = GetDC (hMainWindow);
                 SelectObject (hdc, hFont);
                 GetTextMetrics (hdc, &tm);
                 DeleteObject (hFont);
                 ReleaseDC (hMainWindow, hdc);

                 elen = tm.tmAveCharWidth * nlen;
                 if (elen < epos) break;
                 EditNumFont.FontHeight -= 20;
                 if (EditNumFont.FontHeight <= 80) break;
        }

        TextNumFont.FontHeight = 200;

        epos = max (0, (epos - elen) / 2);
        epos = NumField.mask[2].pos[1] + nbss0 + epos;
        eheight = tm.tmHeight;
        eheight += eheight / 3;
        EditForm.mask[0].length = elen;
        EditForm.mask[0].rows   = eheight;
        EditForm.mask[0].pos[1] = epos;
        EditForm.mask[0].pos[0] = ny;

        epos = NumControl.mask[0].pos[1] - (NumField.mask[2].pos[1] + nbss0);
        elen = StornoForm.mask[0].length;
        epos = max (0, (epos - elen) / 2);
        epos = NumField.mask[2].pos[1] + nbss0 + epos;
        StornoForm.mask[0].pos[1] = epos;
        StornoForm.mask[0].pos[0] = cy - 30 - StornoForm.mask[0].rows;

        if (pic)
        {
                       EditForm.mask[0].picture = pic;
        }

        spezfont (&TextNumFont);
        hFont = SetWindowFont (hMainWindow);
        hdc = GetDC (hMainWindow);
        SelectObject (hdc, hFont);
        GetTextMetrics (hdc, &tm);
        DeleteObject (hFont);
        ReleaseDC (hMainWindow, hdc);

        elen = tm.tmAveCharWidth * strlen (text);
        epos = NumControl.mask[0].pos[1] - (NumField.mask[2].pos[1] + nbss0);
        epos = max (0, (epos - elen) / 2);
        epos = NumField.mask[2].pos[1] + nbss0 + epos;
        eheight = tm.tmHeight;
        TextForm.mask[0].length = elen;
        TextForm.mask[0].rows   = eheight;
        TextForm.mask[0].pos[1] = epos;
        TextForm.mask[0].pos[0] = (short)
                            EditForm.mask[0].pos[0] + 2 * (short) tm.tmHeight;
        TextForm.mask[0].feld = text;

        while (TRUE)
        {
                 spezfont (&TextNumFont);
                 hFont = SetWindowFont (hMainWindow);
                 hdc = GetDC (hMainWindow);
                 SelectObject (hdc, hFont);
                 GetTextMetrics (hdc, &tm);
                 DeleteObject (hFont);
                 ReleaseDC (hMainWindow, hdc);

                 elen = tm.tmAveCharWidth * tlen;
                 if (elen < epos) break;
                 TextNumFont.FontHeight -= 20;
                 if (TextNumFont.FontHeight <= 80) break;
        }

        spezfont (&TextNumFont);
        hFont = SetWindowFont (hMainWindow);
        hdc = GetDC (hMainWindow);
        SelectObject (hdc, hFont);
        GetTextMetrics (hdc, &tm);
        DeleteObject (hFont);
        ReleaseDC (hMainWindow, hdc);
        elen = tm.tmAveCharWidth * strlen (text);

        TextNumFont.FontHeight -= 20;

        spezfont (&TextNumFont);
        hFont = SetWindowFont (hMainWindow);
        hdc = GetDC (hMainWindow);
        SelectObject (hdc, hFont);
        GetTextMetrics (hdc, &tm);
        DeleteObject (hFont);
        ReleaseDC (hMainWindow, hdc);
        eheight = tm.tmHeight + tm.tmHeight / 3;
        epos = NumControl.mask[0].pos[1] - (NumField.mask[2].pos[1] + nbss0);
        epos = max (0, (epos - elen) / 2);
        epos = NumField.mask[2].pos[1] + nbss0 + epos;
        TextForm.mask[0].length = elen;
        TextForm.mask[0].rows   = eheight;
        TextForm.mask[0].pos[1] = epos;

		if (EditForm.font->hFont)
		{
			     DeleteObject (EditForm.font->hFont);
		         EditForm.font->hFont = NULL;
		}
		if (TextForm.font->hFont)
		{
		        DeleteObject (TextForm.font->hFont);
		        TextForm.font->hFont = NULL;
		}

        memset (editbuff, ' ', 39);
        editbuff [39] = (char) 0;
        memcpy (editbuff, nummer, strlen (nummer));

        eNumWindow  = CreateWindowEx (
                              0,
                              "EnterNum",
                              "",
                              WS_POPUP | WS_CAPTION,
                              x, y,
                              cx, cy,
                              // hMainWindow,
                              hWnd,
                              NULL,
                              hMainInst,
                              NULL);

        create_enter_form (eNumWindow, &EditForm, 0, 0);
        ShowWindow (eNumWindow, SW_SHOW);
        UpdateWindow (eNumWindow);
        SetFocus (EditForm.mask[0].feldid);

        EnableWindows (hWnd, FALSE);
        oldcursor = SetCursor (LoadCursor (NULL, IDC_ARROW));

        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));

        NumEnterWindow = EditForm.mask[0].feldid;
//        SetCapture (eNumWindow);

        NumEnterAktiv = 1;
        break_num_enter = 0;
        while (GetMessage (&msg, NULL, 0, 0))
        {
              if (IsNumKey (&msg));
              else
              {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
              }
              if (break_num_enter) break;
        }
        display_form (eNumWindow, &EditForm, 0, 0);
        SetActiveWindow (hMainWindow);
        EnableWindows (hWnd, TRUE);
        CloseControls (&NumField);
        CloseControls (&NumControl);
        CloseControls (&OKControl);
        CloseControls (&CaControl);
        CloseControls (&EditForm);
        CloseControls (&TextForm);
        if (StornoForm.mask[0].after)
        {
                    CloseControls (&StornoForm);
                    StornoForm.mask[0].after = NULL;
                    StornoForm.mask[0].BuId = 0;
        }
        else if (StornoForm.mask[0].BuId)
        {
                    CloseControls (&StornoForm);
                    StornoForm.mask[0].after = NULL;
                    StornoForm.mask[0].BuId = 0;
        }
//        ReleaseCapture ();
        DestroyWindow (eNumWindow);
		eNumWindow = NULL;
        NumEnterAktiv = 0;
        strcpy (nummer, clipped (editbuff));
        SetCursor (oldcursor);

        SetMenue (&smenue);
        SethListBox (shListBox);
        Setlbox (slbox);
}

void EnterCalcBox (HWND hWnd, char *text, char *nummer, int nlen, char *pic)
/**
Numbox als rechner aufrufen.
**/
{
	     CalcOn = TRUE;
         EnterNumBox (hWnd, text, nummer, nlen, pic);
		 CalcOn = FALSE;
}


/**

  Listenerfassung

**/

// static int lfbsz0 = 36;
static int lfbszs = 45;
static int lfbsz0 = 42;
static int lfbss0 = 73;

static int SetLAN (void);
static int doinsert (void);
static int domess (void);
static int dodelete (void);
static int Auszeichnen ();
static int HbkInput ();
static int Nachliefern ();
static int Nichtliefern ();
static int SysChoise ();
static int SwitchwKopf ();
void arrtozerl_p (int);
void LeseWePos (void) ;
BOOL LeseLspUpd (void);





void SetScannMode ()
{
	if (ScannMode)
	{
		BuSys.text1 = Scann;
	}
	else
	{
//		BuKopf.text1 = Wiegekopf;
		BuSys.text1 = "Ger�t";
	}
}

static field _lKopfform [] =
{
        "Auftrag",      0, 0, -1, 10, 0, "",     DISPLAYONLY, 0, 0, 0,
        auftrag_nr,     0, 0, -1, 100, 0, "%8d", DISPLAYONLY, 0, 0, 0,
		"LAN",          0, 0, -1, 120, 0, "",    DISPLAYONLY, 0, 0, 0,
//		LAN,            0, 0, -1, 150, 0, "",    DISPLAYONLY, 0, 0, 0,
};

static form lKopfform = {3, 0, 0, _lKopfform, 0, 0, 0, 0, &lKopfFont};

field _lFussform[] = {
(char *) &BuInsert,      lfbss0, lfbsz0,-1, 10 + 0* (15 + lfbss0),     0, "", COLBUTTON, 0,
                                                           doinsert,  0,
//(char *) &BuInsert,      lfbss0, lfbsz0,-1, 10 + 4* (15 + lfbss0),     0, "", COLBUTTON, 0,
//                                                           doinsert,  0,
(char *) &BuAusZ,        lfbss0, lfbsz0,-1, 10 + 1 * (15 + lfbss0),    0, "", COLBUTTON, 0,
                                                           HbkInput, 0,
(char *) &BuDel,         lfbss0, lfbsz0,-1, 10 + 2 * (15 + lfbss0),   0, "", COLBUTTON, 0,
                                                           dodelete, 0,
(char *) &BuMess,        lfbss0, lfbsz0,-1, 10 + 3 * (15 + lfbss0),   0, "", COLBUTTON, 0,
                                                           domess, 0,
(char *) &BuQsPos,       lfbss0, lfbsz0,-1, 10 + 5 * (15 + lfbss0),   0, "", COLBUTTON, 0,
                                                           doQsPos, 0,
(char *) &BuSys,         lfbss0, lfbsz0,-1, 40 + 6 * (15 + lfbss0),   0, "", COLBUTTON, 0,
                                                           SysChoise, 0,
(char *) &BuKopf,        lfbss0, lfbsz0,-1, 40 + 7 * (15 + lfbss0),   0, "", COLBUTTON, 0,
                                                           SwitchwKopf, 0,
};

form lFussform = {7, 0, 0, _lFussform, 0, 0, 0, 0, &lFussFont};


mfont lScanFont = {"", 270, 0, 0,       RGB (0, 255, 255),
                                        BLUECOL,
                                        1,
                                        NULL};



extern int lfbss0;
extern int lfbszs;


extern int BreakEanEnter ();
char eanbuffer [256] = {""};

field _fScanform[] = {
eanbuffer,   6 * lfbss0, lfbszs, -1, 10 + 0 * (15 + lfbss0),      0, "255", EDIT | SCROLL, 0, BreakEanEnter, 0,
};         

form fScanform = {1, 0, 0, _fScanform, 0, 0, 0, 0, &lScanFont};

static field _insform [] =
{
        aufps.a,            12, 0, 1, 1, 0, "%11.0f", READONLY, 0, 0, 0,
        aufps.a_bz1,        20, 0, 1, 14,0, "",       READONLY, 0, 0, 0,
        aufps.ls_vk_euro,   10, 0, 1, 36, 0, "%9.3f", READONLY, 0, 0, 0,
        aufps.ls_lad_euro,  10, 0, 1, 47, 0, "%9.3f", READONLY , 0, 0, 0,
};

static form insform = {4, 0, 0, _insform, 0, 0, 0, 0, &InsFont};

static field _insub [] = {
"Artikel-Nr",            0, 0, 0,  1, 0, "", DISPLAYONLY, 0, 0, 0,
"Bezeichnung",           0, 0, 0, 14, 0, "", DISPLAYONLY, 0, 0, 0,
"EK-Preis",              0, 0, 0, 36, 0, "", DISPLAYONLY, 0, 0, 0,
"VK-Preis",              0, 0, 0, 47, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form insub = { 4, 0, 0,_insub, 0, 0, 0, 0, &InsFontT};





static int asort = 1;
static int bsort = 1;
static int amesort = 1;
static int lmesort = 1;
static int ssort = 1;


static int sorta0 (const void *elem1, const void *elem2)
{
	        struct AUFP_S *el1;
	        struct AUFP_S *el2;

			el1 = (struct AUFP_S *) elem1;
			el2 = (struct AUFP_S *) elem2;
	        return ((int) (ratod(el1->a) - ratod (el2->a)) * asort);
}


static int sorta (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufparr, aufpanz, sizeof (struct AUFP_S),
				   sorta0);
			asort *= -1;
//            ActivateColButton (eFussWindow, &lFussform, 2, -1, 1);
            ShowNewElist ((char *) aufparr,
                             aufpanz,
                            (int) sizeof (struct AUFP_S));
			return 0;
}


static int sortb0 (const void *elem1, const void *elem2)
{
	        struct AUFP_S *el1;
	        struct AUFP_S *el2;

			el1 = (struct AUFP_S *) elem1;
			el2 = (struct AUFP_S *) elem2;
	        return ((int) (strcmp (el1->a_bz1,el2->a_bz1)) * bsort);
}


static int sortb (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufparr, aufpanz, sizeof (struct AUFP_S),
				   sortb0);
			bsort *= -1;
//            ActivateColButton (eFussWindow, &lFussform, 2, -1, 1);
            ShowNewElist ((char *) aufparr,
                             aufpanz,
                            (int) sizeof (struct AUFP_S));
			return 0;
}


static int sortame0 (const void *elem1, const void *elem2)
{
	        struct AUFP_S *el1;
	        struct AUFP_S *el2;

			el1 = (struct AUFP_S *) elem1;
			el2 = (struct AUFP_S *) elem2;
			if (ratod (el1->auf_me) > ratod (el2->auf_me))
			{
				          return (1 * amesort);
            }
			if (ratod (el1->auf_me) < ratod (el2->auf_me))
			{
				          return (-1 * amesort);
            }
			return 0;
}

static int sortame (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufparr, aufpanz, sizeof (struct AUFP_S),
				   sortame0);
			amesort *= -1;
//            ActivateColButton (eFussWindow, &lFussform, 2, -1, 1);
            ShowNewElist ((char *) aufparr,
                             aufpanz,
                            (int) sizeof (struct AUFP_S));
			return 0;
}

static int sortlme0 (const void *elem1, const void *elem2)
{
	        struct AUFP_S *el1;
	        struct AUFP_S *el2;

			el1 = (struct AUFP_S *) elem1;
			el2 = (struct AUFP_S *) elem2;
			if (ratod (el1->lief_me) > ratod (el2->lief_me))
			{
				          return (1 * lmesort);
            }
			if (ratod (el1->lief_me) < ratod (el2->lief_me))
			{
				          return (-1 * lmesort);
            }
			return 0;
}


static int sortlme (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufparr, aufpanz, sizeof (struct AUFP_S),
				   sortlme0);
			lmesort *= -1;
//            ActivateColButton (eFussWindow, &lFussform, 2, -1, 1);
            ShowNewElist ((char *) aufparr,
                             aufpanz,
                            (int) sizeof (struct AUFP_S));
			return 0;
}

static int sorts0 (const void *elem1, const void *elem2)
{
	        struct AUFP_S *el1;
	        struct AUFP_S *el2;

			el1 = (struct AUFP_S *) elem1;
			el2 = (struct AUFP_S *) elem2;
	        return ((atoi (el1->s) - atoi (el2->s)) * ssort);
}


static int sorts (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufparr, aufpanz, sizeof (struct AUFP_S),
				   sorts0);
			ssort *= -1;
//            ActivateColButton (eFussWindow, &lFussform, 2, -1, 1);
            ShowNewElist ((char *) aufparr,
                             aufpanz,
                            (int) sizeof (struct AUFP_S));
			return 0;
}


BOOL IsAuszArtikel (void)
/**
Feld pr_ausz pruefen.
**/
{
            char pr_ausz [2];

            if (lese_a_bas (ratod (aufps.a)) != 0)
            {
                          return FALSE;
            }
            strcpy (pr_ausz, "N");
            switch (_a_bas.a_typ)
            {
                   case 1 :
                         strcpy (a_hndw.pr_ausz, "N");
                         hndw_class.lese_a_hndw (_a_bas.a);
                         strcpy (pr_ausz, a_hndw.pr_ausz);
                         break;
                   case 2 :
                         strcpy (a_eig.pr_ausz, "N");
                         hndw_class.lese_a_eig (_a_bas.a);
                         strcpy (pr_ausz, a_eig.pr_ausz);
                         a_hndw.tara = a_eig.tara;
                         break;
                   case 3 :
                         strcpy (a_eig_div.pr_ausz, "N");
                         hndw_class.lese_a_eig_div (_a_bas.a);
                         strcpy (pr_ausz, a_eig_div.pr_ausz);
                         a_hndw.tara = a_eig_div.tara;
                         break;
                   default :
                         return FALSE;
            }
            if (pr_ausz[0] == 'N')
            {
                    return FALSE;
            }
            return TRUE;
}

int SetLAN (void)
/**
Bestellnummer des Lieferanten im Kopf anzeigen.
**/
{
			strcpy (LAN, aufps.a_best);
//			display_field (eKopfWindow, &lKopfform.mask[3]);
			return 0;
}


int CheckHbk ()
/**
Pruefen, ob der Artikel ausgezeichnet werden kann.
**/
{
	        lese_a_bas (ratod (aufps.a));
// Default-Halbarkeits-KZ = 'T' Tage  07.08.2002
/*
            if (_a_bas.hbk_kz[0] <= ' ')
            {
                strcpy (_a_bas.hbk_kz, "T");
            }
*/

			if (InScanning == 0) //110809
			{
            if (_a_bas.hbk_kz[0] > ' ')
            {
//                         ActivateColButton (eFussWindow, &lFussform, 2, 0, 1);
            }
            else
            {
  //                       ActivateColButton (eFussWindow, &lFussform, 2, -1, 1);
            }
			}
			SetLAN ();
            return (0);
}

void GetMeEinh (void)
/**
Mengeneinheit fuer Kunde und Artikel holen.
**/
{
/*
         KEINHEIT keinheit;
         double auf_me, auf_me_vgl;

         einh_class.GetKunEinh (lsk.mdn, lsk.fil,
                                lsk.kun, _a_bas.a, &keinheit);

         strcpy (aufps.auf_me_bz,    keinheit.me_einh_kun_bez);
         strcpy (aufps.lief_me_bz,   keinheit.me_einh_bas_bez);
         sprintf (aufps.me_einh_kun, "%d", keinheit.me_einh_kun);
         sprintf (aufps.me_einh,     "%d", keinheit.me_einh_bas);
         clipped (aufps.auf_me_bz);
         clipped (aufps.lief_me_bz);
         if (keinheit.me_einh_kun == keinheit.me_einh_bas)
         {
                     strcpy (aufps.auf_me_vgl, aufps.auf_me);
         }
         else
         {
                     if (keinheit.inh <= (double) 0.0)
                     {
                                   keinheit.inh = (double) 1.0;
                     }
                     auf_me = (double) ratod (aufps.auf_me);
                     auf_me_vgl = auf_me * keinheit.inh;
                     sprintf (aufps.auf_me_vgl, "%.3lf", auf_me_vgl);
         }
*/
         return;
}

int lese_lief_bzg (char *buffer, double *a)
{
		 return 0;
}

int Reada_pr (void)
/**
Artikelpreis aus a_pr holen.
**/
{
    return 0;
}

void CalcEkBto (void)
{
}

int ReadPr (void)
{
       return 0;
}

int ScanLiefEan (char *buffer, double *a)
{
//	  dsqlstatus = lese_lief_bzg (buffer, a);

//	  if (dsqlstatus != 0)
//	  {
 			dsqlstatus = lese_a_bas (ratod (buffer));
			*a = ratod (buffer);
			if (dsqlstatus == 100)
			{
						dsqlstatus = ScanArtikel (*a);
			}
//	  }
//	  else
//	  {
 //			dsqlstatus = lese_a_bas (*a);
//	  }

	  return dsqlstatus;
}

int ArtikelFromScann (char *buffer, BOOL DispMode)
{

    double a;

    if (strlen (buffer) >= 16)
	{
			 ScanEan128 (Text (buffer), DispMode);
	}
	else
	{
			 ScanLiefEan (buffer, &a);
	}
    return dsqlstatus;
}


int artikelOK (char *buffer, BOOL DispMode)
/**
Artikel-Nummer in Basisdaten suchen.
**/
{
	    double a;

		if (InScanning)
		{
			dsqlstatus = ArtikelFromScann (buffer, FALSE);
		}
	    else if (atoi (lief.we_erf_kz) != 2)
		{
			     if (strlen (buffer) >= 16)
				 {
					 ScanEan128 (Text (buffer));
				 }
				 else
				 {
					a = ratod (buffer);
					dsqlstatus = lese_a_bas (ratod (buffer));
					if (dsqlstatus == 100)
					{
						dsqlstatus = ScanArtikel (a);
					}
				 }
		}
		else
		{
		         dsqlstatus = lese_lief_bzg (buffer, &a);
		}
        if (dsqlstatus != 0)
        {
			if (DispMode && InScanning)
			{
                    disp_messG ("Die Artikelnummer wurde nicht gefunden.\n"
						        "Interne Artikelnummer �ber <neu>\n"
								"eingeben", 2);
			}
			else if (DispMode)
			{
                    disp_messG ("Falsche Artikelnummer", 2);
			}
            return FALSE;
        }
        sprintf (aufps.a, "%11.0lf", _a_bas.a);
        strcpy (aufps.a_bz1, _a_bas.a_bz1);
        strcpy (aufps.a_bz2, _a_bas.a_bz2);
		sprintf (aufps.me_einh, "%hd", _a_bas.me_einh);
        strcpy (aufps.hnd_gew, _a_bas.hnd_gew);
        zerl_p.a = _a_bas.a;
        dsqlstatus = ReadPr ();
/*
        dsqlstatus =   WePreis.preis_holen (zerl_k.mdn,
                                            zerl_k.fil,
                                            zerl_k.lief,
                                             _a_bas.a);
*/
         if (lief_bzg_zwang && dsqlstatus != 0)
         {
			          disp_messG ("Kein Eintrag in den Bezugsquellen", 2);
					  return FALSE;

         }
		 else if (dsqlstatus == 0)
		 {
			 zerl_p.a = _a_bas.a;
		 }

		 if (atoi (aufps.me_einh) == 0) strcpy (aufps.me_einh, "2");

         Reada_pr ();
//         if (EktoMeEinh && lief_bzg.me_kz[0] == '1')
/*
         if (lief_bzg.me_kz[0] == '1')
         {
             pr_ek_bto0      = pr_ek_bto0 *  lief_bzg.min_best;
             pr_ek_bto0_euro = pr_ek_bto0_euro *  lief_bzg.min_best;
             pr_ek0          = pr_ek0*  lief_bzg.min_best;
             pr_ek0_euro     = pr_ek0_euro *  lief_bzg.min_best;
         }
*/

		 CheckHbk ();
         //FISCH-5   flackert sonst wenn :  display_form (InsWindow, &insform, 0, 0);
         return TRUE;
}


void KorrInsPos (TEXTMETRIC *tm)
/**
Positionen in insform korrigieren.
**/
{
        static int posOK = 0;
        int i;

        if (posOK) return;

        posOK = 1;
        for (i = 0; i < insform.fieldanz; i ++)
        {
                    insform.mask[i].length *= (short) tm->tmAveCharWidth;
                    insform.mask[i].pos[1] *= (short) tm->tmAveCharWidth;
                    insform.mask[i].pos[0] *= (short) tm->tmHeight;
        }

        for (i = 0; i < insub.fieldanz; i ++)
        {
                    insub.mask[i].pos[1] *= (short) tm->tmAveCharWidth;
                    insub.mask[i].pos[0] *= (short) tm->tmHeight;
        }
}

void OpenInsert (void)
/**
Fenster fuer Insert-Row oeffnen.
**/
{
        RECT rect;
        RECT frmrect;
        HFONT hFont;
        TEXTMETRIC tm;
        int x, y, cx, cy;

        GetWindowRect (hMainWindow, &rect);
        GetFrmRectEx (&insform, insform.font, &frmrect);

        x = rect.left + 5;
        y = rect.top + 60;
        cx = frmrect.right + 26;
        cy = rect.bottom - rect.top - 200;

        spezfont (&InsFont);
        hFont = SetWindowFont (hMainWindow);
        hdc = GetDC (hMainWindow);
        SelectObject (hdc, hFont);
        GetTextMetrics (hdc, &tm);
        ReleaseDC (hMainWindow, hdc);
        DeleteObject (hFont);

        KorrInsPos (&tm);

        InsWindow = CreateWindow ("ListKopf",
                                    "",
                                    WS_POPUP | WS_VISIBLE | WS_DLGFRAME,
                                    x, y,
                                    cx + 24, 72,
                                    eWindow,
                                    NULL,
                                    hMainInst,
                                    NULL);
}

void DeleteInsert (void)
/**
Insertwindow schliessen.
**/
{
       CloseControls (&insform);
       CloseControls (&insub);
       DestroyWindow (InsWindow);
       InsWindow = NULL;
}

BOOL NewPosi (void)
/**
Positionsnummern neu vergeben und in Datenbank schreiben.
**/
{
/*
          int i;

          beginwork ();
          if (LeseLspUpd () == FALSE) return FALSE;
          ls_class.delete_lspls (lsk.mdn, lsk.fil, lsk.ls);
          for (i = 0; i < aufpanz_upd; i ++)
          {
                   sprintf (aufparr[i].pnr, "%d", (i + 1) * 10);
                   arrtozerl_p (i);
                   ls_class.update_lsp (lsp.mdn, lsp.fil, lsp.ls,
                                        lsp.a, lsp.posi);
          }
          commitwork ();
*/
          return TRUE;

}

BOOL DelLeerPos (void)
/**
Positionsnummern neu vergeben und in Datenbank schreiben.
**/
{
/*
          int i;

          beginwork ();
          if (LeseLspUpd () == FALSE) return FALSE;
          ls_class.delete_lspls (lsk.mdn, lsk.fil, lsk.ls);
          for (i = 0; i < aufpanz_upd; i ++)
          {
                   if (atoi (aufparr[i].s) > 2)
                   {
                         arrtozerl_p (i);
                         ls_class.update_lsp (lsp.mdn, lsp.fil, lsp.ls,
                                              lsp.a, lsp.posi);
                   }
          }
          commitwork ();
          if (eWindow)
          {
                   LeseWePos ();
                   ShowNewElist ((char *) aufparr,
                                aufpanz,
                                (int) sizeof (struct AUFP_S));
          }
*/
          return TRUE;

}

BOOL SollToIst (void)
/**
Bei nicht bearbeiteten Positionen Ist-Wert auf Soll-Wert setzen.
**/
{
/*
          int i;

          beginwork ();
          if (LeseLspUpd () == FALSE) return FALSE;
          ls_class.delete_lspls (lsk.mdn, lsk.fil, lsk.ls);
          for (i = 0; i < aufpanz_upd; i ++)
          {
                   if (atoi (aufparr[i].s) < 3)
                   {
                       strcpy (aufparr[i].lief_me,
                               aufparr[i].auf_me_vgl);
                       strcpy (aufparr[i].s, "3");
                   }
                   arrtozerl_p (i);
                   ls_class.update_lsp (lsp.mdn, lsp.fil, lsp.ls,
                                              lsp.a, lsp.posi);
          }
          commitwork ();
          if (eWindow)
          {
                   LeseWePos ();
                   ShowNewElist ((char *) aufparr,
                                aufpanz,
                                (int) sizeof (struct AUFP_S));
          }
*/
          return TRUE;
}

BOOL SetAllPosStat (void)
/**
Bei nicht bearbeiteten Positionen Ist-Wert auf Soll-Wert setzen.
**/
{
/*
          int i;

          beginwork ();
          if (LeseLspUpd () == FALSE) return FALSE;
          ls_class.delete_lspls (lsk.mdn, lsk.fil, lsk.ls);
          for (i = 0; i < aufpanz_upd; i ++)
          {
                   if (atoi (aufparr[i].s) < 3)
                   {
                       strcpy (aufparr[i].s, "3");
                   }
                   arrtozerl_p (i);
                   ls_class.update_lsp (lsp.mdn, lsp.fil, lsp.ls,
                                              lsp.a, lsp.posi);
          }
          commitwork ();
          if (eWindow)
          {
                   LeseWePos ();
                   ShowNewElist ((char *) aufparr,
                                aufpanz,
                                (int) sizeof (struct AUFP_S));
          }
*/
          return TRUE;
}

long GetNextPosi (int pos)
/**
Naechste gueltige Positionnummer ermitteln.
**/
{
          long ap;
          long posi;

          if (pos > 0)
          {
                strcpy (aufparr[pos].pnr, aufparr[pos - 1].pnr);
          }
          for (pos ++;pos <= aufpanz; pos ++)
          {
                 ap = atol (aufparr[pos].pnr);
                 if ((ap % 10) == 0) break;
          }
          posi = atol (aufparr[pos - 1].pnr) + 1;
          return (posi);
}

void ScrollAufp (int pos)
/**
aufparr ab pos zum Einfuegen scrollen.
**/
{
         int i;

         for (i = aufpanz; i > pos; i --)
         {
                       memcpy (&aufparr [i], &aufparr [i + 1],
                                sizeof (struct AUFP_S));
         }
         aufpanz ++;
}

void ScrollAufpDel (int pos)
/**
aufparr ab pos nach Oben scrollen.
**/
{
         int i;

         for (i = pos; i < aufpanz; i ++)
         {
                       memcpy (&aufparr [i], &aufparr [i + 1],
                                sizeof (struct AUFP_S));
         }
         aufpanz --;
}


int dodelete (void)
/**
Eine Zeile aus Liste loeschen.
**/
{
	     int pos;

		 if (BuDel.aktivate == -1) return (0);

		 if (atoi(EinAusgang) == PRODUKTIONSFORTSCHRITT) 
		 {
			 //Hier: Produktionsfortschritt : Umbuchen 
	         akt_lager_zu = AuswahlALager (akt_lager_zu,TRUE);  //dies in ausps rein , dann ab und zubuchen 
			 if (syskey != KEY5)
			 {
				SelectPosTexte ();
		        arrtozerl_p (pos);

			 }
			 return (0);
		 }
		 if (abfragejnG (eWindow, "Position wirklich l�schen ?", "N") == 0)
		 {
			 return (0);
		 }


         pos = GetListPos ();
         arrtozerl_p (pos);
         beginwork ();
//190709
			dsqlstatus = ZerlpClass.dbreadfirst ();
					BucheBsd (-1,zerl_p.nto_gew,zerl_p.charge);

		 DbClass.sqlin ((short *)  &zerl_p.mdn, 1, 0);
		 DbClass.sqlin ((double *)   &zerl_p.partie, 3, 0);
		 DbClass.sqlin ((double *) &zerl_p.a, 3, 0);
		 DbClass.sqlin ((short *)  &zerl_p.kng, 1, 0);
		 DbClass.sqlin ((short *)  &zerl_p.int_pos, 1, 0);
		 DbClass.sqlcomm ("delete from zerl_p  "
			              "where mdn = ? "
						  "and partie = ? "
						  "and a  = ? "
						  "and kng = ? and int_pos = ? ");

         commitwork ();
         LeseWePos ();
         ShowNewElist ((char *) aufparr,
                                aufpanz,
                                (int) sizeof (struct AUFP_S));
		 if (aufpanz == 0)
		 {
            ActivateColButton (BackWindow3,  &MainButton2, 2, -1, 1);
            ActivateColButton (eFussWindow, &lFussform,   0, 1, 1);
            ActivateColButton (eFussWindow, &lFussform,   1, -1, 1);
            ActivateColButton (eFussWindow, &lFussform,   2, 1, 1);
            ActivateColButton (eFussWindow, &lFussform,   3, -1, 1);
		 }
		 if (pos >= aufpanz -1)
		 {
			 pos = aufpanz - 1;
		 }
		 if (pos >= 0)
		 {
			SetListPos (pos);
		 }
         return 0;
}

int domess (void)
/**
Eine Zeile in Liste einfuegen.
**/
{
	     char buffer [10];
		 int aufpidx;

         aufpidx = SendMessage  (Getlbox (), LB_GETCURSEL, 0l, 0l);
         memcpy (&aufps, &aufparr[aufpidx], sizeof (struct AUFP_S));
		 if (MultiTemperature)
		 {
			AuswahlTemperature ();
			memcpy (&aufparr[aufpidx], &aufps, sizeof (struct AUFP_S));
            WriteQsMess (); //110908 Temperaturen und PH speichern
			return 0;
		 }

	     EnableWindows (hMainWindow, FALSE);
		 strcpy (buffer, aufps.temp);
         NuBlock.EnterNumBox (eWindow,"  Temperatur  ",
                                         buffer,    5, "%3.1f" ,
	      						 	     EntNuProc);
		 if (syskey == KEY5)
		 {
             EnableWindows (hMainWindow, TRUE);
			 return 0;
		 }
		 strcpy (aufps.temp, buffer);

		 strcpy (buffer, aufps.ph_wert);
         NuBlock.EnterNumBox (eWindow,"  PH-Wert  ",
                                         buffer, 6, "%4.2f" ,
	      						 	     EntNuProc);
		 if (syskey == KEY5)
		 {
             EnableWindows (hMainWindow, TRUE);
			 return 0;
		 }
		 strcpy (aufps.ph_wert, buffer);

         memcpy (&aufparr[aufpidx], &aufps, sizeof (struct AUFP_S));
         WriteQsMess (); //110908 Temperaturen und PH speichern
         EnableWindows (hMainWindow, TRUE);
		 return 0;
}



int doinsert0 (void)
/**
Eine Zeile in Liste einfuegen.
**/
{
      struct AUFP_S aufp;
      char buffer [80];
      int pos;
      long akt_posi;

	 scangew = 0l;
      OpenInsert ();
      memcpy (&aufps, &aufp_null, sizeof (struct AUFP_S));

	 if (atoi(EinAusgang) == PRODUKTIONSSTART)
	 {
		    sprintf (buffer,"%.0lf",zerl_k.a);
		    artikelOK (buffer);
			 //Hier: manuelle Gewicht und Chargeneingabe
		 	 EnterGewicht ();
             sprintf (aufps.lief_me, "%.3lf", ScanValues->gew);
			 if (syskey == KEY5) return 0;
			 EnterCharge ();
			 if (syskey == KEY5) return 0;
             sprintf (aufps.ls_charge, "%s", ScanValues->Charge);
				//Hier: Fanggebiet testen
				int dfanggebiet = HoleFanggebiet(aufps.ls_charge);
				if (dfanggebiet == 0) 
				{
					disp_messG ("Die Charge konnte nicht gefunden werden ", 2);
					return 0;
				}
				else if (dfanggebiet != zerl_k.fanggebiet)
				{   
					if (zerl_k.fanggebiet == 0) 
					{
						zerl_k.fanggebiet = dfanggebiet;
						ZerlkClass.dbupdate ();
			              sprintf (ptabn.ptwert, "%hd",zerl_k.fanggebiet);
						  ptab_class.lese_ptab ("fanggebiet", ptabn.ptwert);
						strcpy (fanggebiet, ptabn.ptbez);
						sprintf(Standorttxt,"Fanggebiet :%s",fanggebiet);
					}
					else
					{
						disp_messG ("Das Fanggebiet stimmt nicht �berein ", 2);
						return 0;
					}

				}
	 }
	 else
	 {
         while (TRUE)
	     {
                   strcpy (buffer, "0");
                   strcpy (ChargeAuswahl, "0");

					if (atoi(EinAusgang) == EINGANG)
						{
	                          NuBlock.SetChoise (doartchoise, 0, doChargeChoise, 0,
							                 "Auswahl\nArtikel", "Auswahl\nChargen");
						}
						else
						{
							 NuBlock.SetChoise (doartchoise, 0);
						}
						  EnableWindows (AufKopfWindow, FALSE);
  	                      EnableWindows (eKopfWindow, FALSE);
                          EnableWindows (eFussWindow, FALSE);
                          EnableWindows (BackWindow2, FALSE);
                          EnableWindows (BackWindow3, FALSE);
                          EnableWindows (eWindow, FALSE);
						  NuBlock.SetCharChangeProc (ChangeChar);
		                      NuBlock.EnterNumBox (eWindow,"  Artikel  ",
                                               buffer, -1, "" ,
	      									  EntNuProc);
  	                      EnableWindows (eKopfWindow, TRUE);
                          EnableWindows (eFussWindow, TRUE);
                          EnableWindows (BackWindow2, TRUE);
                          EnableWindows (BackWindow3, TRUE);
                          EnableWindows (eWindow, TRUE);
						  EnableWindows (AufKopfWindow, TRUE);
                   InvalidateRect (eWindow, NULL, TRUE);
                   UpdateWindow (eWindow);
                   SetDblClck (IsDblClck, 0);
                   if ((double) ratod (buffer) <= (double) 0.0)
                   {
                              memcpy (&aufps, &aufp, sizeof (struct AUFP_S));
                              current_form = &testform;
                              DeleteInsert ();
                              return 0;
                   }
                   if (syskey == KEY5)
                   {
                              memcpy (&aufps, &aufp, sizeof (struct AUFP_S));
                              current_form = &testform;
                              DeleteInsert ();
                              return 0;
                   }
				   if (strcmp(clipped(ChargeAuswahl),"0") != 0) strcpy(aufps.ls_charge,ChargeAuswahl);		
                   if (artikelOK (buffer))
                   {
                              break;
                   }
                   current_form = &testform;
                   DeleteInsert ();
                   return 0;
         }

         if (scangew != 0l)
         {
            sprintf (aufps.lief_me, "%.3lf", (double) scangew / 1000);
         }
	 } // else PRODUKTIONSSTART


		 
		 if (hbk_direct)
		 {
		           strcpy (aufps.hbk_date, "");

// Default-Halbarkeits-KZ = 'T' Tage  07.08.2002

/*
                   if (_a_bas.hbk_kz[0] <= ' ')
                   {
                           strcpy (_a_bas.hbk_kz, "T");
                   }
*/

		           if (_a_bas.hbk_kz[0] > ' ')
				   {
                           sysdate (buffer);
                           EnterNumBox (eWindow,
                                " Haltbarkeitsdatum ", buffer, 11,
                                   "dd.mm.yyyy");
                           InvalidateRect (eWindow, NULL, TRUE);
                           UpdateWindow (eWindow);
                           if (syskey != KEY5)
						   {
					          strcpy (aufps.hbk_date,buffer);
						   }
				   }
         }



         current_form = &testform;
         DeleteInsert ();
         InvalidateRect (eWindow, NULL, TRUE);
         UpdateWindow (eWindow);
//         pos = GetListPos () + 1;
         pos = aufpanz;
         akt_posi = GetNextPosi (pos);
         sprintf (aufps.pnr, "%ld", akt_posi);
         ScrollAufp (pos);
	     if (atoi(EinAusgang) != PRODUKTIONSSTART && atoi(EinAusgang) != BUCHENFERTIGLAGER ) 
		 {
			if (strcmp(clipped(ChargeAuswahl),"0") == 0) strcpy (aufps.ls_charge, "");
		 }
         memcpy (&aufparr [pos], &aufps, sizeof (struct AUFP_S));
         arrtozerl_p (pos);
         beginwork ();
         ZerlpClass.dbupdate (atoi (EinAusgang));
         commitwork ();
         LeseWePos ();
         SetListPos (pos);
         ShowNewElist ((char *) aufparr,
                                aufpanz,
                                (int) sizeof (struct AUFP_S));
		 if (aufpanz == 1)
		 {
            ActivateColButton (BackWindow3,  &MainButton2, 2, 0, 1);
            ActivateColButton (eFussWindow, &lFussform,   0, 0, 1);
            ActivateColButton (eFussWindow, &lFussform,   1, 0, 1);
            ActivateColButton (eFussWindow, &lFussform,   2, 0, 1);
            ActivateColButton (eFussWindow, &lFussform,   3, 0, 1);
		 }
         memcpy (&aufps, &aufparr [pos], sizeof (struct AUFP_S));
         CheckHbk ();
         SetListPos (pos);
		 if (wieg_neu_direct && MesswerteZwang == 0) //130509 MesswerteZwang 
		 {
                      IsDblClck (pos);
		 }
         return 0;
}


int doinsert (void)
{
	    if (BuInsert.aktivate == -1) return (0);
		if (atoi(EinAusgang) == PRODUKTIONSFORTSCHRITT)
		{
	         aufpidx = SendMessage  (Getlbox (), LB_GETCURSEL, 0l, 0l);
			AuswahlFortschritt ();
//			LeseWePos ();
//	         SetListPos (aufpidx);
		    ShowNewElist ((char *) aufparr,
                               aufpanz,
                               (int) sizeof (struct AUFP_S));
			memcpy (&aufps, &aufparr[aufpidx], sizeof (struct AUFP_S));
			return 0;
		}
	    while (TRUE)
		{
	             doinsert0 ();
//	             ScanA ();
				 if (syskey == KEY5) break;
				 if (wieg_neu_direct != 2) break;
		}
		return 0;
}


int StartScanning (void)
{
        InScanning = TRUE;
	    while (TRUE)
		{
	             doinsert0 ();
//	             ScanA ();
				 if (syskey == KEY5) break;
				 if (wieg_neu_direct != 2) break;
		}
        InScanning = FALSE;
		return 0;
}

int endlist (void)
/**
Liste beenden.
**/
{
        break_list ();
        return (1);
}

void arrtozerl_p (int idx)
/**
Satz aufarr in zerl_p uebertragen.
**/
{
	    char datum [12];
		int dsqlstatus;

        if (  (atoi (EinAusgang) == PRODUKTIONSFORTSCHRITT || atoi (EinAusgang) == BUCHENFERTIGLAGER ))  strcpy (lief_nr, aufparr[idx].partie);
	    sysdate (datum);
	    zerl_k.mdn = Mandant;
        zerl_k.partie  = ratod(lief_nr);
	    zerl_p.mdn = Mandant;
        zerl_p.partie  = ratod(lief_nr);
        zerl_p.kng  = aufparr[idx].kng; 
        zerl_p.fortschritt  = atoi (aufparr[idx].fortschritt); 
        zerl_p.buch_gew  = ratod (aufparr[idx].buch_gew); 
        zerl_p.pcharge  = aufparr[idx].pcharge; 
        zerl_p.status  = aufparr[idx].status;  
		zerl_p.mhd  = dasc_to_long (aufparr[idx].hbk_date);
		if (atoi(EinAusgang) == UMLAGERUNG) zerl_p.kng  = UMLAGERUNG;
		if (atoi(EinAusgang) == PRODUKTIONSSTART ) zerl_p.kng  = PRODUKTIONSSTART ;
		if (atoi(EinAusgang) == PRODUKTIONSFORTSCHRITT ) zerl_p.kng  = PRODUKTIONSFORTSCHRITT ;
		if (atoi(EinAusgang) == PRODUKTIONSFORTSCHRITT ) zerl_p.partie  = ratod(aufparr[idx].partie) ;
		if (atoi(EinAusgang) == BUCHENFERTIGLAGER  ) zerl_p.kng  = BUCHENFERTIGLAGER  ;
        zerl_p.int_pos  = atol (aufparr[idx].pnr);
        zerl_p.a        = (double) ratod (aufparr[idx].a);
		  systime (zeit);
    	  memcpy (zerl_p.zeit, zeit, 8);
		zerl_p.bearb  = dasc_to_long (datum);

		dsqlstatus = ZerlpClass.dblock ();

		zerl_p.nto_gew         = (double) ratod (aufparr[idx].lief_me);
        zerl_p.lgr_zu	  = aufparr[idx].lgr_zu;
        zerl_p.lgr_ab	  = aufparr[idx].lgr_ab;
        zerl_p.tara     = (double) ratod (aufparr[idx].tara);
        zerl_p.stk = (double) ratod (aufparr[idx].anz_einh);
		zerl_p.standort = Standort;



// Inhalt beim Touchscreen immer auf 1

        strcpy (zerl_p.charge, aufparr[idx].ls_charge);
		zerl_p.we_txt = atoi (aufparr[idx].lsp_txt);

		if (generiere_charge > 0)
		{
			// 23.06.2009 : hier nochmal f�llen, weil charge unterwegs verloren geht (nur auf toucher, auf server nicht ??!!)
				strcpy (zerl_p.charge, GenCharge ());
		}
}


long GenLText (void)
/**
Text-Nr fuer lsp.lsp_txt genereieren.
**/
{
        int dsqlstatus;
        short mdn;
        short fil;
        long gen_nr;
        extern short sql_mode;

        mdn = Mandant;
        fil = Filiale;

        sql_mode = 1;
        dsqlstatus = nvholid (mdn, fil, "lspt_txt");
        if (dsqlstatus == 100)
        {
              dsqlstatus = nvanmprf (mdn,
                                     fil,
                                     "lspt_txt",
                                     (long) 1,
                                     (long) 999999,
                                     (long) 10,
                                      "");

              if (dsqlstatus == 0)
              {
                          dsqlstatus = nvholid (mdn,
                                                fil,
                                                "lspt_txt");
              }
         }
         gen_nr = auto_nr.nr_nr;
         sql_mode = 0;
         return gen_nr;
}


void TextInsert (void)
/**
Text fuer Nicht- osder Nachliefern einfuegen.
**/
{
/*
       int aufidx;
       long lsp_txt;


       beginwork ();
       if (Lock_WePos () == FALSE)
       {
            commitwork ();
            return;
       }
       aufidx = GetListPos ();
       memcpy (&aufparr [aufidx], &aufps, sizeof (struct AUFP_S));

       lsp_txt = atol (aufparr [aufidx].lsp_txt);
       if (lsp_txt == 0l)
       {
           lsp_txt = GenLText ();
       }
       if (lsp_txt == 0l) return;
       strcpy (aufparr[aufidx].s, "3");
       sprintf (aufparr[aufidx].lsp_txt, "%ld", lsp_txt);
       memcpy (&aufps, &aufparr [aufidx], sizeof (struct AUFP_S));
       lspt.nr = lsp_txt;
       lspt.zei = 10;
       lspt_class.dbupdate ();
       arrtozerl_p (aufidx);
       ZerlpClass.dbupdate ();
       commitwork ();
       ShowNewElist ((char *) aufparr,
                      aufpanz,
                      (int) sizeof (struct AUFP_S));
*/
}

int Nachliefern ()
/**
Status NACHLIEFERN setzen.
**/
{
      ptab_class.lese_ptab ("na_lief_kz", "2");
      TextInsert ();
      return (0);
}


int Nichtliefern ()
/**
Status NICHTLIEFERN setzen.
**/
{
      ptab_class.lese_ptab ("na_lief_kz", "1");
      TextInsert ();
      return (0);
}


int SwitchwKopf ()
{
        static int arganz;
        static char *args[4];
        char kopf [4];

        if (la_zahl < 0) return 0;

        if (la_zahl == 2)
        {
            AktKopf = (AktKopf  + 1) % la_zahl;
            sprintf (kopf, "%d", AktKopf + 1);
            arganz = 4;
            args[0] = "Leer";
            args[1] = "6";
            args[2] = waadir;
            args[3] = (char *) kopf;
            fnbexec (arganz, args);
            print_messG (1, "Wiegkopf %s gew�hlt", kopf);
            return 0;
        }
        SaveMenue ();
        EnableWindow (eWindow, FALSE);
        EnableWindow (eKopfWindow, FALSE);
        EnableWindow (eFussWindow, FALSE);
        AktKopf = AuswahlKopf ();
        EnableWindow (eWindow, TRUE);
        EnableWindow (eKopfWindow, TRUE);
        EnableWindow (eFussWindow, TRUE);
        SetActiveWindow (eWindow);
        AktivWindow = eWindow;
        RestoreMenue ();
        SetDblClck (IsDblClck, 0);

        sprintf (kopf, "%d", AktKopf + 1);
        arganz = 4;
        args[0] = "Leer";
        args[1] = "6";
        args[2] = waadir;
        args[3] = (char *) kopf;
        fnbexec (arganz, args);
        return 0;
}


int SysChoise ()
/**
Auswahl ueber Geraete.
**/
{
		if (ScannMode)
		{
			int dret =	DoScann ();
//                 PostMessage (eWindow, WM_KEYDOWN,  (WPARAM) VK_F5, 0l);  //testtest
            return dret;
		}
        SaveMenue ();
        EnableWindow (eWindow, FALSE);
        EnableWindow (eKopfWindow, FALSE);
        EnableWindow (eFussWindow, FALSE);
        AuswahlSysPeri ();
        EnableWindow (eWindow, TRUE);
        EnableWindow (eKopfWindow, TRUE);
        EnableWindow (eFussWindow, TRUE);
        SetActiveWindow (eWindow);
        AktivWindow = eWindow;
        RestoreMenue ();
        SetDblClck (IsDblClck, 0);
        return (0);
}



int HbkInput (void)
/**
Haltbarkeitsdatum eingeben.
**/
{
	    char buffer [12];

//        aufpidx = SendMessage  (Getlbox (), LB_GETCURSEL, 0l, 0l) - 1;
        aufpidx = SendMessage  (Getlbox (), LB_GETCURSEL, 0l, 0l);

		if (dasc_to_long (aufps.hbk_date) > 0l)
		{
			       strcpy (buffer, aufps.hbk_date);
		}
		else
		{
                   sysdate (buffer);
		}
        EnterNumBox (eWindow, " Haltbarkeitsdatum ", buffer, 11, "dd.mm.yyyy");
        InvalidateRect (eWindow, NULL, TRUE);
        UpdateWindow (eWindow);
        if (syskey != KEY5)
        {
	               strcpy (aufps.hbk_date,buffer);
                   memcpy (&aufparr[aufpidx], &aufps, sizeof (struct AUFP_S));
	               arrtozerl_p (aufpidx);
		           ZerlpClass.dbupdate (atoi (EinAusgang));

        }
		return 0;
}
int Auszeichnen ()
/**
Reaktion auf Doppelclick in der Liste.
**/
{
        if (BuAusZ.aktivate == -1) return (0);
        WorkModus = AUSZEICHNEN;
        PostMessage (eWindow, WM_KEYDOWN,  (WPARAM) VK_RETURN, 0l);
        return (0);
}


void IsDblClck (int idx)
/**
Reaktion auf Doppelclick in der Liste.
**/
{
	    char datum [12];
		int dsqlstatus;
		double zubuch = 0.0;

	    if (TestOK () == FALSE) return;
        beginwork ();
        if (Lock_Lsp () == FALSE)
        {
            commitwork ();
            return;
        }
        aufpidx = idx;
        memcpy (&aufps, &aufparr[aufpidx], sizeof (struct AUFP_S));
		if (MesswerteZwang == 1)  //130509
		{
            if ( (double) ratod (aufps.lief_me) < 0.001)
			{
				domess(); 
			}
		}
//		BelegeFestTara(); //testtest
		if (QsPosWieg == TRUE)
		{
			FillQsPosWieg (ratod(aufps.a)); //220712
		}

        strcpy (auf_me, aufps.rest);

	    sysdate (datum);
	    zerl_p.mdn = zerl_k.mdn;
	    zerl_p.partie = zerl_k.partie;
        zerl_p.int_pos  = atol (aufps.pnr);
        zerl_p.a        = (double) ratod (aufps.a);
		zerl_p.kng = atoi(EinAusgang);
		  systime (zeit);
	   	  memcpy (zerl_p.zeit, zeit, 8);
		zerl_p.bearb  = dasc_to_long (datum);

		dsqlstatus = ZerlpClass.dblock ();
		if (dsqlstatus == 100)
		{
		}
		else
		{
		    //190709
			if (zerl_p.nto_gew != 0.0)    
			{
				dsqlstatus = ZerlpClass.dbreadfirst ();
//				BucheBsd (-1,zerl_p.nto_gew,zerl_p.charge);  
			}
		}


        memcpy (&we_wiepro, &we_wiepro_null, sizeof (struct WE_WIEPRO));
		double gew_alt = zerl_p.nto_gew;

        if (strcmp (aufps.hnd_gew, "G") == 0 || strcmp (aufps.erf_gew_kz, "J") == 0)
        {
                       ArtikelWiegen (eFussWindow);
        }
        else
        {
//                       ArtikelHand ();
                       ArtikelWiegen (eFussWindow);
        }
        WorkModus = WIEGEN;
        SetDblClck (IsDblClck, 0);
        current_form = &testform;
        if (ratod (aufps.a) != 0.0)
        {
               strcpy (aufps.rest, auf_me);
			   aufps.lgr_zu = akt_lager_zu;
			   aufps.lgr_ab = akt_lager_ab;
//		       sprintf (aufps.bsd_me, "%.3lf", HoleBsdMe(Mandant,ratod (aufps.a),LagerAb));
//		       sprintf (aufps.bsd_me, "%.3lf", ratod(aufps.bsd_me) - we_wiepro.me + we_wiepro.tara);
               memcpy (&aufparr[aufpidx], &aufps, sizeof (struct AUFP_S));
			   FuelleAufpArr (); //GROM-6

               arrtozerl_p (idx);
               ZerlpClass.dbupdate (atoi (EinAusgang));
			   WriteRind ();
			   //BucheChargenBsd ();
			   if (zerl_p.nto_gew != gew_alt)
			   {
				   zubuch = zerl_p.nto_gew - gew_alt;
			   }
			   if (zubuch != 0.0) BucheBsd (1,zubuch,zerl_p.charge);
				WriteQsPosWieg ();
        }
	    ShowNewElist ((char *) aufparr,
                             aufpanz,
                            (int) sizeof (struct AUFP_S));
        commitwork ();
		SetListPos (aufpidx);
		SetActiveWindow (eWindow);
        if (autocursor)
        {
                   PostMessage (eWindow, WM_KEYDOWN, VK_DOWN, 0l);
        }
        return;
}

BOOL TestSysWg (void)
/**
Pruefen, ob die Warengruppe dem aaktiven Geraet zugeordnet ist.
**/
{
        pht_wg.sys = auszeichner;
        pht_wg.wg  = _a_bas.wg;
        if (pht_wg_class.dbreadfirst_wg () != 0)
        {
            return FALSE;
        }
        return TRUE;
}



BOOL zerl_ptoarr (int idx)  
/**
Hier: Satz aus zerl_p in aufarr uebertragen.
**/
{
//	    char wert [4];

		double bsd_me = 0.0;

        lese_a_bas (zerl_p.a);

        memcpy (&aufparr[idx], &aufp_null, sizeof (aufps));
        strcpy (aufparr[idx].hnd_gew, _a_bas.hnd_gew);

        sprintf (aufparr[idx].s, "%1d", 0);
        sprintf (aufparr[idx].pnr, "%ld", zerl_p.int_pos);
        sprintf (aufparr[idx].a, "%.0lf", zerl_p.a);
        strcpy (aufparr[idx].a_bz1,_a_bas.a_bz1);
        sprintf (aufparr[idx].a_me_einh, "%hd", _a_bas.me_einh);
        sprintf (aufparr[idx].lief_me, "%.3lf", zerl_p.nto_gew);
		if (zerl_p.nto_gew > 0.0) strcpy (aufparr[idx].s, "3");

        sprintf (aufparr[idx].tara, "%.3lf", zerl_p.tara);
        sprintf (aufparr[idx].anz_einh, "%.3lf", zerl_p.stk);
        sprintf (aufparr[idx].inh,      "%.3lf", 1.0);
        sprintf (aufparr[idx].me_einh,  "%hd", _a_bas.me_einh);
        strcpy (aufparr[idx].ls_charge, clipped(zerl_p.charge));
        sprintf (aufparr[idx].lsp_txt,  "%ld",   zerl_p.we_txt);
		if (aufparr[idx].kng == EINGANG)
		{
			bsd_me = HoleBsdMe(Mandant,zerl_p.a,LagerAb); 
		}
		else if (aufparr[idx].kng == AUSGANG)
		{
			bsd_me = HoleBsdMe(Mandant,zerl_p.a,LagerZu); 
		}
		else
		{
			bsd_me = HoleBsdMe(Mandant,zerl_p.a,LagerAb); 
		}
        sprintf (aufparr[idx].bsd_me,      "%.3lf", bsd_me);

		aufparr[idx].lgr_zu = zerl_p.lgr_zu;
		aufparr[idx].lgr_ab = zerl_p.lgr_ab;
		aufparr[idx].kng = zerl_p.kng;
		aufparr[idx].pcharge = zerl_p.pcharge;
		aufparr[idx].status = zerl_p.status;
        dlong_to_asc (zerl_p.mhd, aufparr[idx].hbk_date);
        sprintf (aufparr[idx].buch_gew,      "%.3lf", zerl_p.buch_gew);
        sprintf (aufparr[idx].partie,  "%.0lf", zerl_p.partie);
        sprintf (aufparr[idx].fortschritt,  "%hd", zerl_p.fortschritt);
		if (zerl_p.fortschritt > 0)
		{
			if (ptab_class.lese_ptab ("prodphase", aufparr[idx].fortschritt) == 0)
			{
				sprintf (aufparr[idx].fortschritt_bz,  "%s", ptabn.ptbezk);
			}
			else
			{
				sprintf (aufparr[idx].fortschritt_bz,  "%d", zerl_p.fortschritt);
			}
		}
		else
		{
			sprintf (aufparr[idx].fortschritt_bz,  "%s", "Eingang");
		}
		HoleQsMess(idx);



        return 0;
}
BOOL zer_partptoarr (int idx)
/**
Satz aus zer_partp in aufarr uebertragen.
**/
{
		double bsd_me = 0.0;
        lese_a_bas (zerl_p.a);

        memcpy (&aufparr[idx], &aufp_null, sizeof (aufps));
        strcpy (aufparr[idx].hnd_gew, _a_bas.hnd_gew);

        sprintf (aufparr[idx].s, "%1d", 0);
        sprintf (aufparr[idx].pnr, "%ld", zer_partp.mat);
		double a = (double) zer_partp.mat;  
		lese_a_bas (a);
		sprintf (aufparr[idx].a, "%.0lf", a);
        strcpy (aufparr[idx].a_bz1,_a_bas.a_bz1);
        sprintf (aufparr[idx].a_me_einh, "%hd", _a_bas.me_einh);
        sprintf (aufparr[idx].inh,      "%.3lf", 1.0);
        sprintf (aufparr[idx].me_einh,  "%hd", _a_bas.me_einh);
		bsd_me = HoleBsdMe(Mandant,zerl_p.a,LagerAb); 
        sprintf (aufparr[idx].bsd_me,      "%.3lf", bsd_me);
		HoleQsMess(idx);


        return 0;
}

BOOL LeseLspUpd (void)
/**
Daten aus lsp in aufarr einlesen.
**/
{
        extern short sql_mode;
        aufpanz = 0;
        aufpidx = 0;

        sql_mode = 1;
		zerl_p.mdn = zerl_k.mdn;
        zerl_p.partie = zerl_k.partie;
        dsqlstatus = ZerlpClass.dbreadfirst_o ("order by int_pos, a");
        while (dsqlstatus == 0)
        {
                   if (ZerlpClass.dblock () != 0)
                   {
                       return FALSE;
                   }
                   if (zerl_ptoarr (aufpanz) != 0)
                   {
                       dsqlstatus = ZerlpClass.dbread_o ();
                       continue;
                   }

                   aufpanz ++;
                   if (aufpanz == AUFMAX) break;
                   dsqlstatus = ZerlpClass.dbread_o ();
        }
        aufpanz_upd = aufpanz;
        return TRUE;
}

//Hier: zerl_p lesen 
void LeseWePos (void)
/**
Daten aus lsp in aufarr einlesen.
**/
{
        aufpanz = 0;
        aufpidx = 0;

		zerl_k.mdn = Mandant;
		zerl_k.partie = ratod(lief_nr);
		zerl_p.mdn = zerl_k.mdn;
		zerl_p.partie = zerl_k.partie;
		zerl_p.kng = atoi(EinAusgang); 
		if (atoi(EinAusgang) == UMLAGERUNG) zerl_p.kng  = UMLAGERUNG;
		if (atoi(EinAusgang) == PRODUKTIONSSTART ) zerl_p.kng  = PRODUKTIONSSTART ;
		if (atoi(EinAusgang) == PRODUKTIONSFORTSCHRITT ) zerl_p.kng  = PRODUKTIONSFORTSCHRITT ;
		if (atoi(EinAusgang) == BUCHENFERTIGLAGER  ) zerl_p.kng  = BUCHENFERTIGLAGER  ;

		if (atoi(EinAusgang) == ZERLEGUNG)
		{
			dsqlstatus = ZerlpClass.dbreadfirst_oz ("order by int_pos, a");
			while (dsqlstatus == 0)
			{
					if (zerl_ptoarr (aufpanz) != 0)
					{
						dsqlstatus = ZerlpClass.dbread_oz ();
						continue;
					}
					aufpanz ++;
					if (aufpanz == AUFMAX) break;
					dsqlstatus = ZerlpClass.dbread_oz ();
			}
		}
		if (atoi(EinAusgang) == FEINZERLEGUNG)
		{
			dsqlstatus = ZerlpClass.dbreadfirst_oz ("order by int_pos, a");
			while (dsqlstatus == 0)
			{
					if (zerl_ptoarr (aufpanz) != 0)
					{
						dsqlstatus = ZerlpClass.dbread_oz ();
						continue;
					}
					aufpanz ++;
					if (aufpanz == AUFMAX) break;
					dsqlstatus = ZerlpClass.dbread_oz ();
			}
		}
		if (atoi(EinAusgang) == PRODUKTIONSFORTSCHRITT || atoi(EinAusgang) == BUCHENFERTIGLAGER   )
		{
			//Hier:Sortierung der Liste
			dsqlstatus = ZerlpClass.dbreadfirst_f ("order by status,bearb desc, zeit desc");  
			while (dsqlstatus == 0)
			{
					if (zerl_ptoarr (aufpanz) != 0)
					{
						dsqlstatus = ZerlpClass.dbread_f ();
						continue;
					}
					aufpanz ++;
					if (aufpanz == AUFMAX) break;
					dsqlstatus = ZerlpClass.dbread_f ();
			}
		}
		else if (atoi(EinAusgang) == PRODUKTIONSSTART)
		{
			//Hier:Sortierung der Liste Produktionsstart
			zerl_p.kng = PRODUKTIONSSTART;
		    zerl_k.partie = zerl_k.partie;
			dsqlstatus = ZerlpClass.dbreadfirst_o ("order by status,bearb desc, zeit desc");  
			while (dsqlstatus == 0)
			{
					if (zerl_ptoarr (aufpanz) != 0)
					{
						dsqlstatus = ZerlpClass.dbread_o ();
						continue;
					}
					aufpanz ++;
					if (aufpanz == AUFMAX) break;
					dsqlstatus = ZerlpClass.dbread_o ();
			}
		}
		else
		{
			dsqlstatus = ZerlpClass.dbreadfirst_o ("order by int_pos, a");
			while (dsqlstatus == 0)
			{
					if (zerl_ptoarr (aufpanz) != 0)
					{
						dsqlstatus = ZerlpClass.dbread_o ();
						continue;
					}
					aufpanz ++;
					if (aufpanz == AUFMAX) break;
					dsqlstatus = ZerlpClass.dbread_o ();
			}
		}
		if (NeuePartie == 1 && atoi(EinAusgang) == EINGANG) 
		{
			zer_partp.mdn = Mandant;
			strcpy(zer_partp.zer_part,zer_partk.zer_part);
			dsqlstatus = zer_partp_class.dbreadfirst_o ("order by mat");
			while (dsqlstatus == 0)
			{
					if (zer_partptoarr (aufpanz) != 0)
					{
						dsqlstatus = zer_partp_class.dbread_o ();
						continue;
					}
	
	
					aufpanz ++;
					if (aufpanz == AUFMAX) break;
					dsqlstatus = zer_partp_class.dbread_o ();
			}
		}
	    FuelleAufpArr (); //GROM-6



		if (InScanning == 0) //110809
		{
		if (aufpanz == 0)
		{
            ActivateColButton (BackWindow3, &MainButton2, 2, -1, 1);
            ActivateColButton (eFussWindow, &lFussform,   0, 1, 0);
            ActivateColButton (eFussWindow, &lFussform,   1, -1, 0);
            ActivateColButton (eFussWindow, &lFussform,   2, 1, 0);
            ActivateColButton (eFussWindow, &lFussform,   3, -1, 0);
		}
		else
		{
            ActivateColButton (BackWindow3, &MainButton2, 2, 1, 1);
            ActivateColButton (eFussWindow, &lFussform, 0, 0, 0);
            ActivateColButton (eFussWindow, &lFussform, 1, 0, 0);
            ActivateColButton (eFussWindow, &lFussform, 2, 0, 0);
            ActivateColButton (eFussWindow, &lFussform, 3, 0, 0);
		}
		}
}

void RegisterListe (void)
/**
Kopf - und Fuss - Fenster fuer Liste registrieren
**/
{
        static int registered = 0;
        WNDCLASS wc;

        if (registered == 0)
        {
                   wc.style =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                               | CS_OWNDC;
                   wc.cbClsExtra    =  0;
                   wc.cbWndExtra    =  0;
                   wc.hInstance     =  hMainInst;
                   wc.hIcon         =  LoadIcon (hMainInst, "FITICON");
                   wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                   wc.lpszMenuName  =  "";
                   wc.lpfnWndProc   =  ListKopfProc;
                   wc.lpszMenuName  =  "";
                   wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
                   wc.lpszClassName =  "ListKopf";
                   RegisterClass(&wc);
                   registered = 1;
        }
}


LONG FAR PASCAL ListKopfProc(HWND hWnd,UINT msg,
                          WPARAM wParam,LPARAM lParam)
{
        POINT mousepos;

        GetCursorPos (&mousepos);

        switch(msg)
        {
              case WM_PAINT :
                    if (hWnd == eKopfWindow)
                    {
                             SetListEWindow (0);
                             display_form (eKopfWindow, &lKopfform, 0, 0);
//                             SetListEWindow (1);
                    }
                    if (hWnd == eFussWindow)
                    {
                             display_form (eFussWindow, &lFussform, 0, 0);
                    }
                    if (hWnd == InsWindow)
                    {
                             SetListEWindow (0);
                             display_form (InsWindow, &insub, 0, 0);
                             display_form (InsWindow, &insform, 0, 0);
//                             SetListEWindow (1);
                    }
                    break;
              case WM_LBUTTONDOWN :
              case WM_LBUTTONUP :
              case WM_MOUSEMOVE :
                    return TRUE;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

void DrawRowNoteIcon (HDC hdc, RECT *rect, int idx)
{
	if (eWindow != GetActiveWindow ()) return;;


	if (eWindow != NULL &&NoteIcon != NULL)
	{
		if (atol (aufparr[idx].lsp_txt) != 0)
		{
			DrawIcon (hdc,rect->left + 10, rect->top, NoteIcon);
			DrawNote = TRUE;
		}
		
	}
}

void DrawNoteIcon (HDC hdc)
{
	if (eWindow != GetActiveWindow ()) return;;
	
	if (eWindow != NULL && NoteIcon != NULL)
	{
		if (atol (aufps.lsp_txt) != 0)
		{
			DrawIcon (hdc,10, 50, NoteIcon);
			DrawNote = TRUE;
		}
		
	}
}


void DrawNoteIcon ()
{
	if (!ListeAktiv) return;
	RECT rect;
	rect.top = 0 + 50;
	rect.left = 10;
	rect.bottom = 32 + 50;
	rect.right = 32 + 10;
	InvalidateRect (Getlbox (), &rect, TRUE);
}



void EnterListe (void)
/**
Listenerfassung
**/
{
        RECT rect;
        RECT frmrect;
        int x, y, cx, cy;
        HCURSOR oldcursor;
        static long ls_alt = 0;
        HWND phWnd;
        HWND shListBox;
        HWND slbox;
        struct LMENUE smenue;
		TEXTMETRIC tm;


        if (TestQs (1) == FALSE)
        {
            return;
        }

        memcpy (&aufps, &aufp_null, sizeof (aufps));
        memcpy (&aufparr[0], &aufps, sizeof (aufps));
       
		if (atoi(EinAusgang) != BUCHENFERTIGLAGER) Kunde  = atoi (lief_nr); //FISCH-5

		commitwork ();
		beginwork ();
//		if (atoi(EinAusgang) == PRODUKTIONSFORTSCHRITT) zerl_k.partie = 1; //FISCH-5
        if (zerl_k.partie <= 0 && (atoi(EinAusgang) != UMLAGERUNG && atoi(EinAusgang) != PRODUKTIONSFORTSCHRITT  && atoi(EinAusgang) != BUCHENFERTIGLAGER )) return;

        if (LiefEnterAktiv)
        {
			SetTextMetrics (hMainWindow, &tm, &lKopfFont);
			if (atoi(EinAusgang) < 3)
			{
				lKopfform.mask[0].feld = "Zerlegepartie";
				lKopfform.mask[0].length = 0;
				lKopfform.mask[1].feld = lief_nr;
				lKopfform.mask[1].pos[1] = 180;
				lKopfform.mask[2].feld = zerl_k.partie_bz;
				lKopfform.mask[2].pos[1] = 280;
			}
			else if (atoi(EinAusgang) == ZERLEGUNG)
			{
				lKopfform.mask[0].feld = "Charge";
				lKopfform.mask[0].length = 0;
				lKopfform.mask[1].feld = lief_nr;
				lKopfform.mask[1].pos[1] = 180;
				lKopfform.mask[2].feld = zerl_k.partie_bz;
				lKopfform.mask[2].pos[1] = 280;
			}
			else if (atoi(EinAusgang) == FEINZERLEGUNG)
			{
				lKopfform.mask[0].feld = "Charge";
				lKopfform.mask[0].length = 0;
				lKopfform.mask[1].feld = lief_nr;
				lKopfform.mask[1].pos[1] = 180;
				lKopfform.mask[2].feld = zerl_k.partie_bz;
				lKopfform.mask[2].pos[1] = 280;
			}
			else if (atoi(EinAusgang) == PRODUKTIONSSTART )
			{
				lKopfform.mask[0].feld = "Produktionsstart";
				lKopfform.mask[0].length = 0;
				lKopfform.mask[1].feld = lieferdatum;
				lKopfform.mask[1].pos[1] = 250;

				sprintf(Standorttxt,"Standort : %2d",Standort);
                sprintf (ptabn.ptwert, "%hd",zerl_k.fanggebiet);
		   	    ptab_class.lese_ptab ("fanggebiet", ptabn.ptwert);
				strcpy (fanggebiet, clipped(ptabn.ptbez));
				sprintf(Standorttxt,"Fanggebiet :%s",fanggebiet);
				lKopfform.mask[2].feld = Standorttxt;
				lKopfform.mask[2].pos[1] = 400;
			}
			else if (atoi(EinAusgang) == PRODUKTIONSFORTSCHRITT )
			{
				lKopfform.mask[0].feld = "Produktionsfortschritt";
				lKopfform.mask[0].length = 0;
				lKopfform.mask[1].feld = lieferdatum;
				lKopfform.mask[1].pos[1] = 250;
				sprintf(Standorttxt,"Standort : %2d",Standort);
				lKopfform.mask[2].feld = Standorttxt;
				lKopfform.mask[2].pos[1] = 400;
			}
			else if (atoi(EinAusgang) == BUCHENFERTIGLAGER )
			{
				lKopfform.mask[0].feld = "Buche in Fertiglager";
				lKopfform.mask[0].length = 0;
				lKopfform.mask[1].feld = lieferdatum;
				lKopfform.mask[1].pos[1] = 250;
				sprintf(Standorttxt,"Standort : %2d",Standort);
				lKopfform.mask[2].feld = Standorttxt;
				lKopfform.mask[2].pos[1] = 400;
			}
			else
			{
				lKopfform.mask[0].feld = "Umlagerung";
				lKopfform.mask[0].length = 0;
				lKopfform.mask[1].feld = lieferdatum;
				lKopfform.mask[1].pos[1] = 150;
				sprintf(Standorttxt,"Standort : %2d",Standort);
				lKopfform.mask[2].feld = Standorttxt;
				lKopfform.mask[2].pos[1] = 300;
			}
//			lKopfform.mask[2].pos[1] = (short)  160 + 18 * (short) tm.tmAveCharWidth;
//			lKopfform.mask[3].pos[1] = (short)  lKopfform.mask[2].pos[1] + 4 *
//				                       (short)  tm.tmAveCharWidth;
        }
        else
        {
            lKopfform.mask[0].feld = "Auftrag";
            lKopfform.mask[0].length = 0;
            lKopfform.mask[1].feld = auftrag_nr;
            lKopfform.mask[1].pos[1] = 100;
        }

        GetMenue (&smenue);
        MenTextSave (&smenue);
        shListBox = GethListBox ();
        slbox = Getlbox ();

//        ActivateColButton (eFussWindow, &lFussform, 2, 0, 0);
                  ActivateColButton (eFussWindow, &lFussform, 0, 1, 0);
                  set_fkt (NULL, 7);

        if (AufKopfWindow)
        {
                   phWnd = AufKopfWindow;
        }
        else
        {
                   phWnd = LogoWindow;
        }
        SetActiveWindow (phWnd);
        SetAktivWindow (phWnd);
        oldcursor = SetCursor (LoadCursor (NULL, IDC_WAIT));
        LeseWePos ();

        RegisterNumEnter ();
        RegisterListe ();
        GetWindowRect (hMainWindow, &rect);
        GetFrmRectEx (&testform, testform.font, &frmrect);

        x = rect.left + 5;
        y = rect.top + 60;
//        cx = frmrect.right + 15;
//        cy = rect.bottom - rect.top - 200;
        cx = (int) (double) ((double) frmrect.right + 60 * scrfx);
        cy = (int) (double) ((double) rect.bottom - rect.top - 200 * scrfy);


        SetAktivWindow (phWnd);
        set_fkt (doQsPos, 2);
        set_fkt (domess, 3);
        set_fkt (endlist, 5);
        set_fkt (doinsert, 6);
        set_fkt (HbkInput, 8);
        set_fkt (dodelete, 9);
        set_fkt (SysChoise, 11);
        set_fkt (SetKomplett, 12);
        SetDblClck (IsDblClck, 0);
        ListeAktiv = 1;
//        SetListEWindow (1);
        SetListFont (TRUE);
        spezfont (testform.font);
        eKopfWindow = CreateWindow ("ListKopf",
                                    "",
                                    WS_POPUP | WS_DLGFRAME,
                                    x, y - 45,
                                    (int) ((double) cx + 24 * scrfx),
									(int) ((double) 36 * scrfy),
                                    phWnd,
                                    NULL,
                                    hMainInst,
                                    NULL);

        eWindow = OpenListWindowEnEx (x, y, cx, cy);

        GetWindowRect (eWindow, &rect);
        eFussWindow = CreateWindow ("ListKopf",
                                    "",
                                    WS_POPUP | WS_DLGFRAME,
                                    x, rect.bottom - 2,
                                    (int) ((double) cx + 24 * scrfx),
									(int) ((double) 50 * scrfy),
                                    phWnd,
                                    NULL,
                                    hMainInst,
                                    NULL);
        ShowWindow (eKopfWindow, SW_SHOWNORMAL);
        ShowWindow (eFussWindow, SW_SHOWNORMAL);
        ElistVl (&testvl);
        ElistUb (&testub);
	    SetListRowProc (DrawRowNoteIcon);
        ActivateColButton (BackWindow3, &MainButton2, 3, 1, 1);
		if (aufpanz == 0)
		{
                   ActivateColButton (BackWindow3, &MainButton2, 2, -1, 1);
		}
/*
        if (WiegeTyp == STANDARD)
		{
//FISCH-5 ist jetzt Etikettendruck aus der Liste                    ActivateColButton (BackWindow3, &MainButton2, 0, -1, 1);
		}
*/

		ActivateColButton (BackWindow3, &MainButton2, 0, 1, 1); //FISCH-5
        ShowElist ((char *) aufparr,
                    aufpanz,
                  (char *) &aufps,
                  (int) sizeof (struct AUFP_S),
                  &testform);
        SetCursor (oldcursor);

		if (atoi(EinAusgang) == PRODUKTIONSSTART && DirektScannProduktionsstart )
		{
	         set_fkt (DoScann, 8);   //FISCH-5 Scanbutton soll sofort aktiviert sein
			PostMessage (eWindow, WM_KEYDOWN, VK_F8, 0l);//FISCH-5 Scanbutton soll sofort aktiviert sein
		}
		if (atoi(EinAusgang) == PRODUKTIONSFORTSCHRITT && DirektScannProduktionsfortschritt )
		{
	         set_fkt (DoScann, 8);   //FISCH-5 Scanbutton soll sofort aktiviert sein
			PostMessage (eWindow, WM_KEYDOWN, VK_F8, 0l);//FISCH-5 Scanbutton soll sofort aktiviert sein
		}
		if (atoi(EinAusgang) == BUCHENFERTIGLAGER  && DirektScannBuchenFertiglager )
		{
	         set_fkt (DoScann, 8);   //FISCH-5 Scanbutton soll sofort aktiviert sein
			PostMessage (eWindow, WM_KEYDOWN, VK_F8, 0l);//FISCH-5 Scanbutton soll sofort aktiviert sein
		}

        EnterElist (eWindow, (char *) aufparr,
                             aufpanz,
                             (char *) &aufps,
                             (int) sizeof (struct AUFP_S),
                             &testform);
         NewPosi ();
         CloseEWindow (eWindow);
         CloseControls (&lKopfform);
         CloseControls (&lFussform);
         if (testub.mask[0].attribut & BUTTON)
         {
                    CloseControls (&testub);
         }
         DestroyWindow (eFussWindow);
         DestroyWindow (eKopfWindow);
         set_fkt (NULL, 2);
         set_fkt (NULL, 3);
         set_fkt (NULL, 5);
         set_fkt (NULL, 6);
         set_fkt (NULL, 7);
         set_fkt (NULL, 8);
         set_fkt (NULL, 9);
         set_fkt (NULL, 11);
         set_fkt (NULL, 12);
         SetListFont (FALSE);
         ListeAktiv = 0;
         ActivateColButton (BackWindow3, &MainButton2, 3, -1, 1);
	     SetListRowProc (NULL);
         SetDblClck (NULL, 1);
//         if (mit_trans)  commitwork ();
         SetMenue (&smenue);
         SethListBox (shListBox);
         Setlbox (slbox);
         set_fkt (menfunc2, 6);
 		 commitwork ();
}



static int DbAuswahl (short cursor,void (*fillub) (char *),
                      void (*fillvalues) (char *))
/**
Auswahl ueber Auftraege.
**/
{
        RECT rect;
        RECT frmrect;
        int x, y, cx, cy;
        static long ls_alt = 0;
        HCURSOR oldcursor;

        aufanz = 0;
        oldcursor = SetCursor (LoadCursor (NULL, IDC_WAIT));
        while (fetch_scroll (cursor, NEXT) == 0)
        {
                sprintf (aufsarr[aufanz].partie, "%8.0lf", zerl_k.partie);
                sprintf (aufsarr[aufanz].lief, "%8s",  zerl_k.lief);
                sprintf (aufsarr[aufanz].partie_bz, "%47s",  zerl_k.partie_bz);
                dlong_to_asc (zerl_k.zer_dat, aufsarr[aufanz].zer_dat);
                sprintf (aufsarr [aufanz].partie_status, "%1hd", zerl_k.partie_status);
                aufanz ++;
                if (aufanz == AUFMAX) break;
        }

        RegisterNumEnter ();
        GetWindowRect (hMainWindow, &rect);
        GetFrmRectEx (&testform, testform.font, &frmrect);

        x = rect.left + 5;
        y =  rect.top + 20;
        cx = (int) (double) ((double) frmrect.right + 15 * scrfx);
        cy = (int) (double) ((double) rect.bottom - rect.top - 120 * scrfy);

        set_fkt (endlist, 5);
        syskey = 1;
        SetDblClck (IsAwClck, 0);
        ListeAktiv = 2;
        SetAktivWindow (hMainWindow);
        SetListFont (TRUE);
        spezfont (testform.font);
        eWindow = OpenListWindowEnEx (x, y, cx, cy);
        ElistVl (&aufvl);
        ElistUb (&aufub);
        ShowElist ((char *) aufsarr,
                    aufanz,
                  (char *) &aufs,
                  (int) sizeof (struct AUF_S),
                  &aufform);
	    SetCursor (oldcursor);
		 if (aufanz > 0)
		 {
            ActivateColButton (BackWindow3,  &MainButton2, 2, 0, 1);
            ActivateColButton (eFussWindow, &lFussform,   0, 0, 1);
            ActivateColButton (eFussWindow, &lFussform,   1, 0, 1);
            ActivateColButton (eFussWindow, &lFussform,   2, 0, 1);
            ActivateColButton (eFussWindow, &lFussform,   3, 0, 1);
		 }
	    EnterElist (eWindow, (char *) aufsarr,
                             aufanz,
                             (char *) &aufs,
                             (int) sizeof (struct AUF_S),
                             &aufform);
        if (aufub.mask[0].attribut & BUTTON)
        {
                    CloseControls (&aufub);
        }
        CloseEWindow (eWindow);
        set_fkt (NULL, 5);
        SetListFont (FALSE);
        ListeAktiv = 0;
        SetDblClck (NULL, 1);
        return aufidx;
}

/**

  Auftragskopf anzeigen und Eingabefelder waehlen.

**/

mfont AufKopfFontT = {"Courier New", 200, 0, 1, BLACKCOL,
                                                RGB (0, 255, 255),
                                                0,
                                                NULL};

mfont AufKopfFontE = {"Courier New", 200, 0, 1,
                                                RGB (0, 0, 0),
                                                RGB (255, 255, 255),
                                                0,
                                                NULL};


mfont AufKopfFontTD = {"Courier New", 150, 0, 1, BLACKCOL,
                                                RGB (0, 255, 255),
                                                0,
                                                NULL};

mfont AufKopfFontED = {"Courier New", 150, 0, 1,
                                                RGB (0, 0, 0),
                                                RGB (255, 255, 255),
                                                0,
                                                NULL};


static char KundeFiliale [14] = {"Kunden-Nr   :"};
static int ReadAuftrag (void);
static int PartieAuswahl (void);
static int BreakAuswahl (void);

static field _AufKopfT[] = {
"Auftrag-Nr  :",     12, 0, 1, 2, 0, "", DISPLAYONLY, 0, 0, 0};

form AufKopfT = {1, 0, 0, _AufKopfT, 0, 0, 0, 0, &AufKopfFontT};


static field _AufKopfTD[] = {
KundeFiliale,        13, 0, 4, 2, 0, "", DISPLAYONLY, 0, 0, 0,
kun_krz1,            16, 0, 4,28, 0, "", DISPLAYONLY,        0, 0, 0,
"Lieferdatum :",     13, 0, 5, 2, 0, "", DISPLAYONLY, 0, 0, 0,
"Komm.-Datum :",     13, 0, 6, 2, 0, "", DISPLAYONLY, 0, 0, 0,
"Lieferzeit  :",     13, 0, 7, 2, 0, "", DISPLAYONLY, 0, 0, 0,
"Status      :",     13, 0, 8, 2, 0, "", DISPLAYONLY, 0, 0, 0,
 ls_stat_txt,        16, 0, 8,28, 0, "", DISPLAYONLY,        0, 0, 0
};

form AufKopfTD = {7,0, 0, _AufKopfTD, 0, 0, 0, 0, &AufKopfFontTD};


static field _AufKopfE[] = {
  auftrag_nr,           9, 0, 1,18, 0, "%8d", EDIT,        0, ReadAuftrag, 0};

form AufKopfE = {1, 0, 0, _AufKopfE, 0, 0, 0, 0, &AufKopfFontE};


static field _AufKopfED[] = {
  kun_nr,              20, 0, 4,18, 0, "%8d", EDIT,        0, 0, 0,
  lieferdatum,         20, 0, 5,18, 0, "dd.mm.yyyy",
                                              EDIT,        0, 0, 0,
  komm_dat,            20, 0, 6,18, 0, "dd.mm.yyyy",
                                              EDIT,        0, 0, 0,
  lieferzeit,          20, 0, 7,18, 0, "",
                                              EDIT,        0, 0, 0,
  ls_stat,             20, 0, 8,18, 0, "",
                                              EDIT,        0, BreakAuswahl, 0
};

form AufKopfED = {5, 0, 0, _AufKopfED, 0, 0, 0, 0, &AufKopfFontED};

static field _AufKopfQuery[] = {
  auftrag_nr,           9, 0, 1,18, 0, "", EDIT,        0, 0, 0,
  kun_nr,               9, 0, 4,18, 0, "", EDIT,        0, 0, 0,
  lieferdatum,         11, 0, 5,18, 0, "",
                                           EDIT,        0, 0, 0,
  komm_dat,            11, 0, 6,18, 0, "",
                                           EDIT,        0, 0, 0,
  lieferzeit,           6, 0, 7,18, 0, "",
                                              EDIT,        0, 0, 0,
  ls_stat,              2, 0, 8,18, 0, "",
                                              EDIT,        0, 0, 0
};

static form AufKopfQuery = {6, 0, 0, _AufKopfQuery, 0, 0, 0, 0, 0};
static char *AufKopfnamen[] = {"auf", "kun", "lieferdat", "komm_dat",
                                     "lieferzeit",
                                     "ls_stat"};

static int break_kopf_enter = 0;
static int aspos = 0;
static int asend = 4;

int BreakAuswahl (void)
/**
Auswahl ueber Auftraege.
**/
{
        break_kopf_enter = 1;
        return 0;
}

int PartieAuswahl (void)
/**
Auswahl ueber Zerlegepartien.
**/
{
	char Status [20] ; 

        SetDbAwSpez (DbAuswahl);
        break_kopf_enter = 1;
		strcpy (Status, "0 and 9");
		if (atoi(EinAusgang) == PRODUKTIONSSTART ) strcpy (Status, " 0 and 0 ");
		if (atoi(EinAusgang) == PRODUKTIONSFORTSCHRITT ) strcpy (Status, " 1 and 1 ");
             sprintf (sqlstring ,
		     "select partie,partie_bz,lief,zer_part,zer_dat,partie_status from zerl_k "
			                        "where zerl_k.mdn = %d and zerl_k.partie_status between %s"
									, Mandant, Status);

		sprintf(sqlstring,"%s%s",sqlstring,"order by zerl_k.zer_dat desc");


//        ReadQuery (&AufKopfQuery0, AufKopfnamen0);
        if (ls_class.ShowZerl_k (sqlstring) == 0 && stopauswahl == 0)
        {
					 stopauswahl = 1;
			         strcpy(bestellhinweis,"");

					 if (syskey == KEY5) 
					 {
	                     sprintf (bestell_nr, "%ld", 0);
						 return 0;
					 }
                     sprintf (bestell_nr, "%ld", best_kopf.best_blg);
					 zerl_k.mdn = Mandant;
				     sprintf(bestellhinweis,"Partie: %8.0lf   %s",zerl_k.partie,zerl_k.partie_bz);
//	FISCH-5				 if (ReadWEBestell(best_kopf.best_blg) == 0)  //100908 direct in Liste, wenn WE schon da
					 if (ZerlkClass.dbreadfirst() == 0) //FISCH-5
					 {
						 sprintf(lief_nr,"%12.0lf",zerl_k.partie);
						 sprintf(lieferant,"%s",zerl_k.lief);
						 LiefEnterAktiv = 1;
						 menfunc2 ();
				         LiefEnterAktiv = 0;
						 return 0;
					 }
                     sprintf (lieferant, "%s", best_kopf.lief);
                     sprintf (zerl_k.lief, "%s", best_kopf.lief);
					GenLief_rech_nr(0);
					short di = 1;
					menfunc3();
                     return 0;
        }
        InitAufKopf ();
        SetDbAwSpez (NULL);
  	    if (EnterTyp == AUSWAHL)
		{
//				SmtChanged = FALSE;
		}
        return 0;

}

void FillAufForm (void)
/**
Datenbankfelder in Maske uebertragn.
**/
{
/*
             kun_class.lese_kun (1, 0, lsk.kun);
             sprintf (kun_nr, "%ld", lsk.kun);
             dlong_to_asc (lsk.lieferdat, lieferdatum);
             dlong_to_asc (lsk.komm_dat, komm_dat);
             strcpy (lieferzeit, lsk.lieferzeit);
             sprintf (ls_stat, "%hd", lsk.ls_stat);
             strcpy (kun_krz1, kun.kun_krz1);
             ptab_class.lese_ptab ("ls_stat", ls_stat);
             strcpy (ls_stat_txt, ptabn.ptbezk);
*/
}

BOOL Lock_Lsk (void)
/**
Lieferschein sperren.
**/
{
    short sqlm;
    extern short sql_mode;

    if (LockAuf == 0)
        return TRUE;
    sqlm = sql_mode;
    sql_mode = 1;
    dsqlstatus = ZerlkClass.dblock ();
    if (dsqlstatus < 0)
    {
        print_messG (2, "Satz ist gesperrt");
        sql_mode = sqlm;
        AufLock = 1;
        ActivateColButton (BackWindow2, &MainButton, 1, -1, 1);
        ActivateColButton (BackWindow2, &MainButton, 2, -1, 1);
        ActivateColButton (BackWindow2, &MainButton, 4, -1, 1);
        return FALSE;
    }
    AufLock = 0;
    ActivateColButton (BackWindow2, &MainButton, 1, 1, 1);
    ActivateColButton (BackWindow2, &MainButton, 4, 1, 1);
    sql_mode = sqlm;
    return TRUE;
}

BOOL Lock_Lsp (void)
/**
Lieferschein sperren.
**/
{
/*
    short sqlm;
    extern short sql_mode;

    sqlm = sql_mode;
    sql_mode = 1;
    dsqlstatus = ls_class.lock_lsp (1, 0, lsp.ls, ratod (aufps.a),
                                                  atol (aufps.pnr));
    if (dsqlstatus < 0)
    {
               print_messG (2, "Position wird von einem anderen "
                               "Benutzer bearbeitet");
               return FALSE;
    }
    sql_mode = sqlm;
*/
    return TRUE;
}


int ReadAuftrag (void)
/**
Lieferschein nach Auftragsnummer lesen.
**/
{
/*
            if (atol (auftrag_nr) <= 0l) return (-1);

            dsqlstatus = ls_class.lese_lsk_auf (1, 0, atol (auftrag_nr));
            if (dsqlstatus)
            {
                   ActivateColButton (BackWindow2, &MainButton, 4, -1, 1);
                   set_fkt (NULL, 6);
                   print_messG (0, "Auftrag %s nicht gefunden", auftrag_nr);
                   return -1;
            }

//            if (Lock_Lsk () == FALSE) return 0;

            ActivateColButton (BackWindow2, &MainButton, 4, 1, 1);
            set_fkt (menfunc2, 6);
            FillAufForm ();
*/
            return 0;
}


void KonvTextPos (mfont *Font1,  mfont *Font2, int *z, int *s)
/**
Textposition von einer Schriftgroesse zur anderen konvertieren.
**/
{
         TEXTMETRIC tm1, tm2;
         HDC hdc;
         HFONT hFont, oldfont;
         int ax, ay;

         spezfont (Font1);
         hFont = SetWindowFont (hMainWindow);
         hdc = GetDC (hMainWindow);
         oldfont = SelectObject (hdc, hFont);
         GetTextMetrics (hdc, &tm1);
         DeleteObject (SelectObject (hdc, oldfont));
         ReleaseDC (hMainWindow, hdc);
         DeleteObject (hFont);

         spezfont (Font2);
         hFont = SetWindowFont (hMainWindow);
         hdc = GetDC (hMainWindow);
         oldfont = SelectObject (hdc, hFont);
         GetTextMetrics (hdc, &tm2);
         DeleteObject (SelectObject (hdc, oldfont));
         ReleaseDC (hMainWindow, hdc);
         DeleteObject (hFont);

         ax = *s;
         ay = *z;

         ax = ax * tm1.tmAveCharWidth / tm2.tmAveCharWidth;
         ay = ay * tm1.tmHeight / tm2.tmHeight;

         *s = ax;
         *z = ay;
}

void KonvTextAbsPos (mfont *Font, int *cy, int *cx)
/**
Absolute Textposition fuer eine Schriftgroesse ermitteln.
**/
{
         TEXTMETRIC tm;
         HDC hdc;
         HFONT hFont, oldfont;

         spezfont (Font);
         hFont = SetWindowFont (hMainWindow);
         hdc = GetDC (hMainWindow);
         oldfont = SelectObject (hdc, hFont);
         GetTextMetrics (hdc, &tm);
         DeleteObject (SelectObject (hdc, oldfont));
         ReleaseDC (hMainWindow, hdc);
         DeleteObject (hFont);
         *cx *= tm.tmAveCharWidth;
         *cy *= tm.tmHeight;
}

void KonvTextStrLen (mfont *Font, char *Text, int *cx)
/**
Absolute Textposition fuer eine Schriftgroesse ermitteln.
**/
{
         TEXTMETRIC tm;
         HDC hdc;
         HFONT hFont, oldfont;

         spezfont (Font);
         hFont = SetWindowFont (hMainWindow);
         hdc = GetDC (hMainWindow);
         oldfont = SelectObject (hdc, hFont);
         GetTextMetrics (hdc, &tm);
		 SIZE size;
         GetTextExtentPoint (hdc, Text, strlen (Text), &size);
         DelFont (SelectObject (hdc, oldfont));
         ReleaseDC (hMainWindow, hdc);
//         DeleteObject (hFont);
         *cx = size.cx;
}

void GetTextPos (mfont *Font, int *cy, int *cx)
/**
Zeichenorientierte Textposition fuer eine Schriftgroesse ermitteln.
**/
{
         TEXTMETRIC tm;
         HDC hdc;
         HFONT hFont, oldfont;

         spezfont (Font);
         hFont = SetWindowFont (hMainWindow);
         hdc = GetDC (hMainWindow);
         oldfont = SelectObject (hdc, hFont);
         GetTextMetrics (hdc, &tm);
         DeleteObject (SelectObject (hdc, oldfont));
         ReleaseDC (hMainWindow, hdc);
         DeleteObject (hFont);

         *cx /= tm.tmAveCharWidth;
         *cy /= tm.tmHeight;
}



void FontTextLen (mfont *Font, char *text, int *cx, int *cy)
/**
Laenge und Hoehe eines Textes fuer den gewaehlten Font holen.
**/
{
         TEXTMETRIC tm;
         HDC hdc;
         HFONT hFont, oldfont;
         SIZE size;

         spezfont (Font);
         hFont = SetWindowFont (hMainWindow);
         hdc = GetDC (hMainWindow);
         oldfont = SelectObject (hdc, hFont);
         GetTextMetrics (hdc, &tm);
         GetTextExtentPoint (hdc, text, *cx, &size);
         DeleteObject (SelectObject (hdc, oldfont));
         ReleaseDC (hMainWindow, hdc);
         DeleteObject (hFont);

         *cx = size.cx;
         *cy = tm.tmHeight;
}

void SetAufFocus (int pos)
/**
Focus auf Eingabefelder AuftragKopfsetzen.
**/
{
        switch (pos)
        {
               case 0 :
                      SetFocus (AufKopfE.mask[0].feldid);
                      SendMessage (AufKopfE.mask[0].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
                      break;
               default :
                      SetFocus (AufKopfED.mask[pos - 1].feldid);
                      SendMessage (AufKopfED.mask[pos -1].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
                      break;
        }
}

void PrintKLines ()
/**
Icons am Bildschirm anzeigen.
**/
{
         RECT rect;
         PAINTSTRUCT ps;
         HDC hdc;
         HPEN hPen;
         int x, y;

         GetClientRect (AufKopfWindow, &rect);

         hdc = BeginPaint (AufKopfWindow, &ps);

         hPen = CreatePen (PS_SOLID, 2, BLACKCOL);
         SelectObject (hdc, hPen);
         x = rect.right - 4;
         y = 10;
         MoveToEx (hdc, 2, y, NULL);
         LineTo (hdc, x, y);
         DeleteObject (hPen);

         hPen = CreatePen (PS_SOLID, 2, GRAYCOL);
         SelectObject (hdc, hPen);
         MoveToEx (hdc, 2, y - 1, NULL);
         LineTo (hdc, x, y - 2);
         DeleteObject (hPen);

         hPen = CreatePen (PS_SOLID, 2, BLACKCOL);
         SelectObject (hdc, hPen);
         x = rect.right - 4;
         y = 150;
         MoveToEx (hdc, 2, y, NULL);
         LineTo (hdc, x, y);
         DeleteObject (hPen);

         hPen = CreatePen (PS_SOLID, 2, GRAYCOL);
         SelectObject (hdc, hPen);
         MoveToEx (hdc, 2, y - 1, NULL);
         LineTo (hdc, x, y - 2);
         DeleteObject (hPen);
         EndPaint (WiegWindow, &ps);
         return;

         hPen = CreatePen (PS_SOLID, 2, BLACKCOL);
         SelectObject (hdc, hPen);
         x = rect.right - 4;
         y = 300;
         MoveToEx (hdc, 2, y, NULL);
         LineTo (hdc, x, y);
         DeleteObject (hPen);

         hPen = CreatePen (PS_SOLID, 2, GRAYCOL);
         SelectObject (hdc, hPen);
         MoveToEx (hdc, 2, y - 1, NULL);
         LineTo (hdc, x, y - 2);
         DeleteObject (hPen);

         EndPaint (WiegWindow, &ps);
}

static int NoDisplay;


LONG FAR PASCAL AufKopfProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        switch(msg)
        {

              case WM_PAINT :
				    if (QmAktiv) break;
                    if (LiefEnterAktiv)
                    {
                                DisplayLiefKopf ();
                                PrintKLines ();
                                if (MessWindow == 0)
                                {
                                        LiefFocus ();
                                }
                    }
                    else if (KopfEnterAktiv)
                    {
                                display_form (AufKopfWindow, &AufKopfT, 0, 0);
                                display_form (AufKopfWindow, &AufKopfTD, 0, 0);
                                if (AufKopfE.mask[0].feldid)
                                {
                                      display_form (AufKopfWindow, &AufKopfE, 0, 0);
                                }
                                if (AufKopfE.mask[0].feldid || asend == 0)
                                {
                                      display_form (AufKopfWindow, &AufKopfED, 0, 0);
                                }
                                PrintKLines ();
                                if (MessWindow == 0)
                                {
                                         SetAufFocus (aspos);
                                }
                    }
                    break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}


void RegisterKopfEnter (void)
/**
Fenster fuer Auftragskopf registrieren
**/
{
        static int registered = 0;
        WNDCLASS wc;

        if (registered == 0)
        {
                   wc.style =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                               | CS_OWNDC;
                   wc.cbClsExtra    =  0;
                   wc.cbWndExtra    =  0;
                   wc.hInstance     =  hMainInst;
                   wc.hIcon         =  LoadIcon (hMainInst, "FITICON");
                   wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                   wc.lpszMenuName  =  "";
                   wc.lpfnWndProc   =  AufKopfProc;
                   wc.lpszMenuName  =  "";
                   wc.hbrBackground =  CreateSolidBrush (RGB (0, 255, 255));
                   wc.lpszClassName =  "AufKopf";
                   RegisterClass(&wc);
                   registered = 1;
        }
}

void GetEditText (void)
/**
Text aus Editierfeld holen.
**/
{
        switch (aspos)
        {
                 case 0 :
                    GetWindowText (AufKopfE.mask [0].feldid,
                                   AufKopfE.mask [0].feld,
                                   AufKopfE.mask[0].length);
                    break;
                 case 1 :
                    GetWindowText (AufKopfED.mask [0].feldid,
                                   AufKopfED.mask [0].feld,
                                   AufKopfED.mask[0].length);
                    break;
                 case 2 :
                    GetWindowText (AufKopfED.mask [1].feldid,
                                   AufKopfED.mask [1].feld,
                                   AufKopfED.mask[1].length);
                    break;
                 case 3 :
                    GetWindowText (AufKopfED.mask [2].feldid,
                                   AufKopfED.mask [2].feld,
                                   AufKopfED.mask[2].length);
                    break;
                 case 4 :
                    GetWindowText (AufKopfED.mask [3].feldid,
                                   AufKopfED.mask [3].feld,
                                   AufKopfED.mask[3].length);

                 case 5 :
                    GetWindowText (AufKopfED.mask [4].feldid,
                                   AufKopfED.mask [4].feld,
                                   AufKopfED.mask[4].length);
                    break;
        }
}

int MenKey (int taste)
{
    int i;
    ColButton *ColBut;
    char *pos;

    for (i = 0; i < MainButton.fieldanz; i ++)
    {
           ColBut = (ColButton *) MainButton.mask[i].feld;
           if (ColBut->aktivate < 0) continue;
           if (ColBut->text1)
           {
                 if ((pos = strchr (ColBut->text1, '&')) &&
                    ((UCHAR) toupper (*(pos + 1)) == taste))
                 {
                             if (MainButton.mask[i].after)
                             {
                                 (*MainButton.mask[i].after) ();
                             }
                             return TRUE;
                  }
           }
    }
    return FALSE;
}

int IsKopfMessage (MSG *msg)
/**
HotKey Testen.
**/
{
         POINT mousepos;

         switch (msg->message)
         {
              case WM_KEYDOWN :
              {
                     switch (msg->wParam)
                     {
                            case VK_F5 :
                                    break_kopf_enter = 1;
                                    return TRUE;
                            case VK_F6 :
                                    syskey = KEY6;
                                    testkeys ();
                                    return TRUE;
                            case VK_F12 :
								    if (TestOK () == FALSE) return TRUE;
                                    syskey = KEY12;
                                    testkeys ();
                                    return TRUE;
                            case VK_DOWN :
                            case VK_RETURN :
                                    GetEditText ();
                                    if (asend == 0 && AufKopfE.mask[0].after)
                                    {
                                         if ((*AufKopfE.mask[0].after) () == -1)
                                         {
                                                  return TRUE;
                                         }
                                    }
                                    if (aspos > 0 &&
                                        AufKopfED.mask [aspos - 1].after)
                                    {
                                      (*AufKopfED.mask [aspos - 1].after) ();
                                    }
                                    aspos ++;
                                    if (aspos > asend) aspos = 0;
                                    display_form (AufKopfWindow,
                                                  &AufKopfE, 0, 0);
                                    display_form (AufKopfWindow,
                                                  &AufKopfED, 0, 0);
                                    SetAufFocus (aspos);
                                    return TRUE;
                            case VK_UP :
                                    GetEditText ();
                                    aspos --;
                                    if (aspos < 0) aspos = asend;
                                    display_form (AufKopfWindow,
                                                  &AufKopfE, 0, 0);
                                    display_form (AufKopfWindow,
                                                  &AufKopfED, 0, 0);
                                    SetAufFocus (aspos);
                                    return TRUE;
                            case VK_TAB :
                                    GetEditText ();
                                    if (GetKeyState (VK_SHIFT) < 0)
                                    {
                                            aspos --;
                                            if (aspos < 0) aspos = 4;
                                            SetAufFocus (aspos);
                                     }
                                     else
                                     {
                                            aspos ++;
                                            if (aspos > 4) aspos = 0;
                                            SetAufFocus (aspos);
                                     }
                                     display_form (AufKopfWindow,
                                                 &AufKopfE, 0, 0);
                                     display_form (AufKopfWindow,
                                                 &AufKopfED, 0, 0);
                                      return TRUE;
                            default :
                                    return MenKey (msg->wParam);
                     }
                     break;
              }
              case WM_LBUTTONDOWN :
              {
                    GetCursorPos (&mousepos);

                    if (MouseinWindow (AufKopfE.mask[0].feldid, &mousepos))
                    {
                          EnterNumBox (AufKopfWindow, "  Auftrags-Nr  ",
                                       auftrag_nr, 9, AufKopfE.mask[0].picture);
                          if (asend == 0 && AufKopfE.mask[0].after)
                          {
                                if ((*AufKopfE.mask[0].after) () == -1)
                                {
                                       return TRUE;
                                }
                          }
                          display_form (AufKopfWindow, &AufKopfE, 0, 0);
						  PostMessage (msg->hwnd, WM_KEYDOWN, VK_RETURN, 0l);
                          return TRUE;
                    }
                    else if (MouseinWindow (AufKopfED.mask[0].feldid, &mousepos))
                    {
                          if (asend == 0) return TRUE;
                          EnterNumBox (AufKopfWindow,"  Kunden-Nr  ",
                                          kun_nr, 9, AufKopfED.mask[0].picture);
                          display_form (AufKopfWindow, &AufKopfE, 0, 0);
						  PostMessage (msg->hwnd, WM_KEYDOWN, VK_RETURN, 0l);
                          return TRUE;
                    }
                    else if (MouseinWindow (AufKopfED.mask[1].feldid, &mousepos))
                    {
                         if (asend == 0) return TRUE;
                         EnterNumBox (AufKopfWindow,"  Lieferdatum  ",
                                      lieferdatum, 11,
//                                      "dd.mm.yyyy");
                                            AufKopfED.mask[1].picture);
                         display_form (AufKopfWindow, &AufKopfE, 0, 0);
					     PostMessage (msg->hwnd, WM_KEYDOWN, VK_RETURN, 0l);
                         return TRUE;
                    }
                    else if (MouseinWindow (AufKopfED.mask[2].feldid, &mousepos))
                    {
                         if (asend == 0) return TRUE;
                         EnterNumBox (AufKopfWindow,"  Komm.-Datum  ",
                                      komm_dat   , 11,
//                                      "dd.mm.yyyy");
                                          AufKopfED.mask[2].picture);
                         display_form (AufKopfWindow, &AufKopfE, 0, 0);
					     PostMessage (msg->hwnd, WM_KEYDOWN, VK_RETURN, 0l);
                         return TRUE;
                    }
                    else if (MouseinWindow (AufKopfED.mask[3].feldid, &mousepos))
                    {
                         if (asend == 0) return TRUE;
                         EnterNumBox (AufKopfWindow,"  Lieferzeit  ",
                                      lieferzeit, 6,
                                            AufKopfED.mask[3].picture);
                         display_form (AufKopfWindow, &AufKopfE, 0, 0);
					     PostMessage (msg->hwnd, WM_KEYDOWN, VK_RETURN, 0l);
                         return TRUE;
                    }
                    else if (MouseinWindow (AufKopfED.mask[4].feldid, &mousepos))
                    {
                         if (asend == 0) return TRUE;
                         EnterNumBox (AufKopfWindow,"  Status  ",
                                         ls_stat,  2,
                                            AufKopfED.mask[4].picture);
                         display_form (AufKopfWindow, &AufKopfE, 0, 0);
					     PostMessage (msg->hwnd, WM_KEYDOWN, VK_RETURN, 0l);
                         return TRUE;
                    }
             }
          }

          return FALSE;
}

void KonvAufKopf (void)
/**
Spaltenposition von AufKopfED anpassen.
**/
{
        static int KonvOK = 0;
        int z, s;
        int i;

        if (KonvOK) return;

        KonvOK = 1;
        for (i = 0; i < AufKopfED.fieldanz; i ++)
        {
                    s = AufKopfED.mask[i].pos [1];
                    KonvTextPos (AufKopfE.font,
                                 AufKopfED.font,
                                 &z, &s);
                    AufKopfED.mask[i].pos [1] = s;
        }

        s = AufKopfTD.mask[1].pos [1];
        KonvTextPos (AufKopfE.font,
                    AufKopfED.font,
                    &z, &s);
        AufKopfTD.mask[1].pos [1] = s;

        s = AufKopfTD.mask[5].pos [1];
        KonvTextPos (AufKopfE.font,
                    AufKopfED.font,
                    &z, &s);
        AufKopfTD.mask[5].pos [1] = s;
}

void InitAufKopf (void)
/**
Auftrag-Kopf initialisieren.
**/
{
        int i;

        AufKopfE.mask[0].feld[0] = (char) 0;

        for (i = 0; i < AufKopfED.fieldanz; i ++)
        {
                      AufKopfED.mask[i].feld[0] = (char) 0;
        }
        kun_krz1[0] = (char) 0;
        ls_stat_txt[0] = (char) 0;
}


void MainBuInactiv (void)
/**
Alle Button auf Inaktiv setzen.
**/
{
          ActivateColButton (BackWindow2, &MainButton, 0, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 2, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 3, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 4, -1, 1);
}

void MainBuActiv (void)
/**
Alle Button auf Inaktiv setzen.
**/
{
          ActivateColButton (BackWindow2, &MainButton, 0, 1, 1);
          ActivateColButton (BackWindow2, &MainButton, 1, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 2, 1, 1);
          ActivateColButton (BackWindow2, &MainButton, 3, 1, 1);
          ActivateColButton (BackWindow2, &MainButton, 4, 1, 1);
          set_fkt (NULL, 6);
}


void AuftragKopf (int eType)
/**
Auftragskopf bearbeiten.
**/
{
        RECT rect;
        MSG msg;
        int x, y, cx, cy;

        if (KopfEnterAktiv) return;
        if (LiefEnterAktiv) return;

        MainBuInactiv ();

        if (eType == ENTER)
        {
            beginwork ();
        }
        else
        {
                   InitAufKopf ();
        }

        if (atoi (auftrag_nr))
        {
                  if (ReadAuftrag () == -1) return;;
        }
        CloseLiefKopf ();
        if (eType == AUSWAHL)
        {
                     KopfEnterAktiv = 2;
                     asend = 5;
//                     InitAufKopf ();
                     AufKopfE.mask[0].picture = "";
                     AufKopfED.mask[0].picture = "";
                     AufKopfED.mask[1].picture = "";
                     AufKopfED.mask[2].picture = "";

                     AufKopfED.mask[0].attribut = EDIT;
                     AufKopfED.mask[1].attribut = EDIT;
                     AufKopfED.mask[2].attribut = EDIT;
                     AufKopfED.mask[3].attribut = EDIT;
                     AufKopfED.mask[4].attribut = EDIT;
                     AufKopfTD.mask[1].attribut = REMOVED;
                     AufKopfTD.mask[6].attribut = REMOVED;

                     AufKopfE.mask[0].length = 20;
                     AufKopfED.mask[0].length = 20;
                     AufKopfED.mask[1].length = 20;
                     AufKopfED.mask[2].length = 20;
                     AufKopfED.mask[3].length = 20;
                     AufKopfED.mask[4].length = 20;
                     strcpy (ls_stat, "< 3");
                     strcpy (komm_dat, KomDatum);
                     PartieAuswahl ();
					 strcpy(bestellhinweis,"");
                     KopfEnterAktiv = 0;
                     MainBuActiv ();
                     return;
        }

        if (eType == ENTER)
        {
                     asend = 0;
                     AufKopfE.mask[0].picture  = "%8d";
                     AufKopfED.mask[0].picture = "%8d";
                     AufKopfED.mask[1].picture = "dd.mm.yyyy";
                     AufKopfED.mask[2].picture = "dd.mm.yyyy";

                     AufKopfED.mask[0].attribut = READONLY;
                     AufKopfED.mask[1].attribut = READONLY;
                     AufKopfED.mask[2].attribut = READONLY;
                     AufKopfED.mask[3].attribut = READONLY;
                     AufKopfED.mask[4].attribut = READONLY;
                     AufKopfTD.mask[1].attribut = DISPLAYONLY;
                     AufKopfTD.mask[6].attribut = DISPLAYONLY;

                     AufKopfE.mask[0].length = 9;
                     AufKopfED.mask[0].length = 9;
                     AufKopfED.mask[1].length = 11;
                     AufKopfED.mask[2].length = 11;
                     AufKopfED.mask[3].length = 6;
                     AufKopfED.mask[4].length = 2;
        }
        else if (eType == SUCHEN)
        {
                     asend = 5;
                     set_fkt (BreakAuswahl, 12);
                     InitAufKopf ();
                     AufKopfE.mask[0].picture = "";
                     AufKopfED.mask[0].picture = "";
                     AufKopfED.mask[1].picture = "";
                     AufKopfED.mask[2].picture = "";

                     AufKopfED.mask[0].attribut = EDIT;
                     AufKopfED.mask[1].attribut = EDIT;
                     AufKopfED.mask[2].attribut = EDIT;
                     AufKopfED.mask[3].attribut = EDIT;
                     AufKopfED.mask[4].attribut = EDIT;
                     AufKopfTD.mask[1].attribut = REMOVED;
                     AufKopfTD.mask[6].attribut = REMOVED;

                     AufKopfE.mask[0].length = 20;
                     AufKopfED.mask[0].length = 20;
                     AufKopfED.mask[1].length = 20;
                     AufKopfED.mask[2].length = 20;
                     AufKopfED.mask[3].length = 20;
                     AufKopfED.mask[4].length = 20;
        }

        aspos = 0;
        RegisterKopfEnter ();
        GetWindowRect (LogoWindow, &rect);
        x = rect.left;
        y = rect.top;
        cx = rect.right - rect.left;
        cy = rect.bottom - rect.top;

        KonvAufKopf ();

        KopfEnterAktiv = 1;
        if (AufKopfWindow == 0)
        {
                  AufKopfWindow  = CreateWindowEx (
                              0,
                              "AufKopf",
                              "",
                              WS_POPUP,
                              x, y,
                              cx, cy,
                              hMainWindow,
                              NULL,
                              hMainInst,
                              NULL);
        }

        if (AufKopfWindow == NULL)
        {
                    KopfEnterAktiv = 0;
                    MainBuActiv ();
                    return;
        }
        ShowWindow (AufKopfWindow, SW_SHOW);
        UpdateWindow (AufKopfWindow);
        create_enter_form (AufKopfWindow, &AufKopfE, 0, 0);
        if (asend > 0)
        {
                  create_enter_form (AufKopfWindow, &AufKopfED, 0, 0);
        }
        if (eType == ENTER)
        {
               set_fkt (menfunc2, 6);
        }
        SetAufFocus (aspos);
        break_kopf_enter = 0;
        while (GetMessage (&msg, NULL, 0, 0))
        {
              if (IsKopfMessage (&msg));
              else
              {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
              }
              if (break_kopf_enter) break;
        }
        CloseControls (&AufKopfT);
        CloseControls (&AufKopfE);
        CloseControls (&AufKopfTD);
        CloseControls (&AufKopfED);
        if (eType == ENTER)
        {
               set_fkt (NULL, 6);
        }

        if (eType == ENTER)
        {
                     DestroyWindow (AufKopfWindow);
                     AufKopfWindow = NULL;
                     commitwork ();
//                     InitAufKopf ();
        }

        if (eType == SUCHEN)
        {
                     set_fkt (NULL, 12);
                     DestroyWindow (AufKopfWindow);
                     AufKopfWindow = NULL;
                     PartieAuswahl ();
        }
        KopfEnterAktiv = 0;
        MainBuActiv ();
}

/**

Wiegen

**/


#define BUTARA  502
static int break_wieg = 0;


static char WiegText [30] = {"Kiloware"};
static char WiegMe [12] = {"14.150"};


mfont WiegKgFont  = {"Courier New", 300, 0, 1, BLACKCOL,
                                               WHITECOL,
                                               1,
                                               NULL};

mfont WiegKgFontMe = {"Courier New", 300, 0, 1, BLACKCOL,
                                                RGB (0, 255, 255),
                                                1,
                                                NULL};

mfont WiegEinAusgangFont = {"Courier New", 300, 0, 1, BLACKCOL,
                                                RGB (0, 255, 255),
                                                1,
                                                NULL};

mfont WiegKgFontT  = {"Courier New", 150, 0, 1, WHITECOL,
                                                BLUECOL,
                                                1,
                                                NULL};

mfont WiegBuFontE  = {"Courier New", 200, 0, 1, BLACKCOL,
                                                REDCOL,
                                                1,
                                                NULL};

mfont WiegBuFontW  = {"Courier New", 150, 0, 1, BLACKCOL,
                                                GREENCOL,
                                                1,
                                                NULL};

mfont EtiFont  = {"Courier New", 120, 0, 1, WHITECOL,
                                            BLUECOL,
                                            1,
                                            NULL};

mfont EtiSpezFont  = {"Courier New", 120, 0, 1, WHITECOL,
                                                BLUECOL,
                                                1,
                                                NULL};

mfont LgrSpezFont  = {"Courier New", 120, 0, 1, WHITECOL,
                                                BLUECOL,
                                                1,
                                                NULL};

mfont ZusatzFont  = {"Courier New", 100, 0, 1, WHITECOL,
                                                BLUECOL,
                                                1,
                                                NULL};

ColButton WieOK = { "OK", -1, -1,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     BLACKCOL,
                     GREENCOL,
                     2};


ColButton WieEnde = {"Zur�ck", -1, -1,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     BLACKCOL,
                     REDCOL,
                     2};

ColButton WieWie = {"Wiegen", -1, -1,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     BLACKCOL,
                     GREENCOL,
                     2};

ColButton WieHand = {"Hand", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      BLACKCOL,
                      YELLOWCOL,
                      2};

ColButton WiePreis = {"Preise", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      BLACKCOL,
                      YELLOWCOL,
                      2};

ColButton WieTara = {"Tara", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      // BLACKCOL,
                      // RGB (0, 255, 255),
                      WHITECOL,
                      RGB (0, 0, 255),
                      2};

ColButton WieGebinde = {"Gebinde-", -1, 15,
                      "Tara", -1, 40,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      RGB (0, 0, 255),
                      2};

ColButton AutoTara = {"Auto-", -1,15,
                      "Tara ", -1,40,
                      "aus",   -1,65,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       // BLACKCOL,
                       // RGB (0, 255, 255),
                       WHITECOL,
                       RGB (0, 0, 255),
                       3};

ColButton DelTara = { "Tara   ", -1, 25,
                      "L�schen", -1, 50,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      RGB (0, 0, 255),
                      2};

ColButton FestTara = {"Fest", -1, 25,
                      "Tara", -1, 50,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      RGB (0, 0, 255),
                      2};


ColButton SetTara1 = { "Tara  ", -1, 15,
                       "�ber- ", -1, 40,
                       "nehmen", -1, 65,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       WHITECOL,
                       RGB (0, 0, 255),
                       2};

ColButton SetTara2 = { "Start-",  -1, 15,
                       "wiegen",  -1,  50,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       WHITECOL,
                       RGB (0, 0, 255),
                       2};

ColButton SetTara;

ColButton HandTara = {"Hand", -1, 25,
                      "Tara", -1, 50,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      RGB (0, 0, 255),
                      2};


ColButton Etikett = {"Etikett", -1, -1,
                     "", 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      2};

ColButton SumEti  = {"Summen", -1,   5,
                     "Etikett",-1 , 30,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      2};

ColButton EtiSpez = {"Etiketten", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      2};

ColButton LgrSpezAb = {LagertxtAb, -1, -1,
//                      Buchartikeltxt, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      2};
ColButton LgrSpezZu = {LagertxtZu, -1, -1,
                      Buchartikeltxt, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      2};

ColButton Zusatz = {"Zusatz", -1, 10,
                     "daten", -1, 30,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     WHITECOL,
                     BLUECOL,
                     2};

static int current_wieg;
static long last_anz;
static int CtrMode;
static int wiegend = 9;
static int etiend = 6;
static int dowiegen (void);
static int doWOK (void);
static int doWF5 (void); //130509
static int dohand (void);
static int dotara (void);
static int dogebinde (void);
static int dogebindetara (void);

//220712 A
static int dofest1 (void); 
static int dofest2 (void); 
static int dofest3 (void); 
static int dofest4 (void); 
static int dofest5 (void); 
static int dofest6 (void); 
static int dofest7 (void); 
static int dofest8 (void); 
static int dofest9 (void); 
static int dofest10 (void);
static int dofest11 (void);
static int dofest12 (void);
static int dofest13 (void);
static int dofest14 (void);
static int dofest15 (void);
static int dofest16 (void);
static int dofest17 (void);
static int dofest18 (void);
static int dofest19 (void);
static int dofest20 (void);
static int dofest21 (void);
static int dofest22 (void);
static int dofest23 (void);
static int dofest24 (void);
static int dofest25 (void);
static int dofest (int i); 
//220712 E

static int doauto (void);
static int deltara (void);
static int handtara (void);
void tarieren (double); //220712
static int festtara (void);
static int settara (void);
static int settara0 (void);
static int closetara (void);
static int dolgrspez (void);
static int dozusatz (void);
static int dosumeti (void);
static int PosTexte ();
static int autoan = 0;
static int autopos = 1;
static char *anaus[] = {"aus", "ein"};
static char chargen_nr [18];
static int WiegY;
static double akt_tara = (double) 0.0;
static char last_charge [21] = {""};

static int bsize = 100;
static int babs = 100;

static int bsizek = 60;

static field _WieButton[] = {
(char *) &WieTara,          bsize, bsize, -1, 275, 0, "", COLBUTTON, 0,
                                                      dotara, BUTARA,
(char *) &WieGebinde,       bsize, bsize, -1, 275, 0, "", COLBUTTON, 0,
                                                      dogebindetara, 0,
(char *) &WieHand,          bsize, bsize, -1, 15, 0, "", COLBUTTON, 0,
                                                      dohand, 0,
(char *) &WieWie,           bsize, bsize, -1, 145, 0, "", COLBUTTON, 0,
                                                      dowiegen, 0,
(char *) &WieOK,            bsize, bsize, -1, 275, 0, "", COLBUTTON, 0,
                                                      doWOK, 0,
(char *) &WieEnde,          bsize, bsize, -1, 405, 0, "", COLBUTTON, 0,
                                                      doWF5, 0};

static form WieButton = {6, 0, 0, _WieButton, 0, 0, 0, 0, &WiegBuFontW};


static BOOL WithEti = FALSE;;
static int eti_nr;
static char krz_txt [18];

static BOOL WithSumEti = FALSE;;
static int sum_eti_nr;
static char sum_krz_txt [18];

static double eti_netto;
static double eti_brutto;
static double eti_tara;
static double eti_stk;

static double sum_eti_netto;
static double sum_eti_brutto;
static double sum_eti_tara;
static double sum_eti_stk;

static void SetEti (double netto, double brutto, double tara)
/**
Wete fuer Etikettendruck setzen.
**/
{
	      eti_netto  = netto;
          eti_brutto = brutto;
		  eti_tara   = tara;
	      sum_eti_netto  += netto;
          sum_eti_brutto += brutto;
		  sum_eti_tara   += tara;
}


static void SetEtiStk (double stk)
/**
Wete fuer Etikettendruck setzen.
**/
{
	      eti_stk  = stk;
	      sum_eti_stk  += stk;
}

static field _EtiButton[] = {
(char *) &Etikett,          bsize, bsizek, 30, 700, 0, "", COLBUTTON, 0,
                                                      doetikett, 0,
(char *) &SumEti,           bsize, bsizek,100, 700, 0, "", COLBUTTON, 0,
                                                      dosumeti, 0};

form EtiButton = {2, 0, 0, _EtiButton, 0, 0, 0, 0, &EtiFont};

static field _EtiSpezButton[] = {
(char *) &EtiSpez,          bsize, bsizek, 300, 700, 0, "", COLBUTTON, 0,
                                                      doetispez, 0
};

form EtiSpezButton = {1, 0, 0, _EtiSpezButton, 0, 0, 0, 0, &EtiSpezFont};


static field _LgrSpezButton[] = {
(char *) &LgrSpezZu,          bsize + 118, bsizek, 200, 624, 0, "", COLBUTTON, 0,
                                                      menfuncA_LgrZu, 0,
(char *) &PfeilR,          bsize -30, bsizek, 210, 570, 0, "", COLBUTTON, 0,
                                                      0, 0,

(char *) &LgrSpezAb,          bsize + 120, bsizek, 210, 400, 0, "", COLBUTTON, 0,
                                                      menfuncA_LgrAb, 0
};

form LgrSpezButton = {3, 0, 0, _LgrSpezButton, 0, 0, 0, 0, &LgrSpezFont};



static field _ZusatzButton[] = {
(char *) &Zusatz,          bsize, bsizek, 300, 700, 0, "", COLBUTTON, 0,
                                                      dozusatz, 0
};

form ZusatzButton = {1, 0, 0, _ZusatzButton, 0, 0, 0, 0, &ZusatzFont};

static int bsize2 = 100;
static int babs2 = 100;

static field _WieButtonT[] = {
(char *) &AutoTara,         bsize, bsize, -1, 275, 0, "", COLBUTTON, 0,
                                                      doauto, 0,
(char *) &DelTara,          bsize2, bsize2, -1, 145, 0, "", COLBUTTON, 0,
                                                      deltara, 0,
(char *) &FestTara,         bsize2, bsize2, -1, 15, 0, "", COLBUTTON, 0,
                                                      festtara, 0,
(char *) &HandTara,         bsize2, bsize2, -1, 405, 0, "", COLBUTTON, 0,
                                                      handtara, 0,
(char *) &SetTara,          bsize2, bsize2, -1, 15, 0, "", COLBUTTON, 0,
                                                      settara0, 0,
(char *) &WieEnde,          bsize2, bsize2, -1, 405, 0, "", COLBUTTON, 0,
                                                      0, VK_F5};

form WieButtonT = {6, 0, 0, _WieButtonT, 0, 0, 0, 0, &WiegBuFontW};

static field _WiegKgT [] = {
WiegText,           0, 0, 10, -1, 0, "", DISPLAYONLY, 0, 0, 0};

static form WiegKgT = {1, 0, 0, _WiegKgT, 0, 0, 0, 0, &WiegKgFontT};

static field _WiegKgE [] = {
WiegMe,             0, 0, 0, -1, 0, "%11.3f", READONLY, 0, 0, 0};

static form WiegKgE = {1, 0, 0, _WiegKgE, 0, 0, 0, 0, &WiegKgFont};

static field _WiegKgMe [] = {
"KG",                   3, 1, 0, 0, 0, "", DISPLAYONLY, 0, 0, 0};

static form WiegKgMe = {1, 0, 0, _WiegKgMe, 0, 0, 0, 0, &WiegKgFontMe};

static field _WiegEinAusgang [] = {
EinAusgangtxt,                   16, 1, 0, 0, 0, "", DISPLAYONLY, 0, 0, 0};

static form WiegEinAusgang = {1, 0, 0, _WiegEinAusgang, 0, 0, 0, 0, &WiegEinAusgangFont};

mfont ArtWFont = {"Courier New", 150, 0, 1, RGB (0, 0, 0),
                                      RGB (255, 255, 255),
                                      1,
                                      NULL};
mfont ArtWFontT = {"Courier New", 120, 0, 1, RGB (0, 0, 0),
                                        RGB (0, 255, 255),
                                        1,
                                        NULL};

mfont StartWFont = {"Courier New", 170, 0, 1, 
//                                              RGB (0, 0, 0),
//                                              RGB (0, 255, 255),
                                              BLUECOL,
                                              WHITECOL,
                                              1,
                                              NULL};
/*
mfont ArtWCFont = {"Courier New", 100, 0, 0, RGB (0, 0, 0),
                                      RGB (0, 255, 255),
                                      1,
                                      NULL};
                                      */

mfont ArtWCFont = {"Courier New", 150, 0, 1, RGB (0, 0, 0),
                                      RGB (255, 255, 255),
                                      1,
                                      NULL};

mfont ArtWCFontT = {"Courier New", 120, 0, 1, RGB (0, 0, 0),
                                        RGB (0, 255, 255),
                                        1,
                                        NULL};

mfont ftarafont  = {"Courier New", 150, 0, 1, RGB (0, 0, 0),
                                              RGB (255, 255, 255),
                                              1,
                                              NULL};

mfont ftaratfont  = {"Courier New", 150, 0, 1, RGB (0, 0, 0),
                                               RGB (0, 255, 255),
                                               1,
                                               NULL};
mfont zustxtfont  = {"Courier New", 150, 0, 1, BLUECOL,
                                              RGB (0, 255, 255),
                                              1,
                                              NULL};

mfont ffesttarafont  = {"Courier New", 150, 0, 1, AutoForgroundCol,
                                                  AutoBackgroundCol,
												  1,
                                                  NULL};   //220712
char ScaleTara[9] = {"0,0"}; //220712


COLORREF TexteForgroundCol = RGB (255, 255, 255);
COLORREF TexteBackgroundCol = RGB (0, 0, 255);

mfont ftextefont  = {"Courier New", 150, 0, 1, TexteForgroundCol,
                                                  TexteBackgroundCol,
												  1,
                                                  NULL};


ColButton STexte = {"Texte", -1, -1,
						NULL, 0, 0,
						NULL, 0, 0,
						NULL, 0, -1,
						NULL, 0, 0,
						TexteForgroundCol,
                        TexteBackgroundCol,
						0};

static field _artwconstT [] =
{
"Artikel",                 9, 1, 3, 3,  0, "", DISPLAYONLY,    0, 0, 0,
"Artikelbezeichnung",     18, 1, 3,14,  0, "", DISPLAYONLY,    0, 0, 0,
"Lieferschein",            7, 1, 1, 3,  0, "", DISPLAYONLY,    0, 0, 0,
"Lieferant",               9, 1, 1,14,  0, "", DISPLAYONLY,    0, 0, 0};

static form artwconstT = {4, 0, 0,_artwconstT, 0, 0, 0, 0, &ArtWCFontT};

static field _startwieg[] = 
{
    "Startwiegung",           13, 1, 8, 3, 0,  "", READONLY,    0, 0, 0
};

static form startwieg = {1, 0, 0, _startwieg, 0, 0, 0, 0, &StartWFont};


static char summe1 [40];
static char summe2 [40];
static char summe3 [40];
static char zsumme1 [40];
static char zsumme2 [40];
static char zsumme3 [40];

static char vorgabe1[40] = {"100"};
static char veinh1[10] = {"kg"};
static char vorgabe2[40] = {"100"};
static char veinh2[10] = {"kg"};

BOOL StartWiegen = FALSE;
BOOL Entnahmehand = FALSE;

static double EntnahmeStartGew = 0.0;
static double EntnahmeActGew   = 0.0;
static double EntnahmeTara     = 0.0;

static char wiegzahl [40];

static field _artwconst [] =
{
        aufps.a,            9, 1,  4, 3,  0, "%8.0f",  READONLY, 0, 0, 0,
        aufps.a_bz1,       21, 1,  4, 14, 0, "",       READONLY, 0, 0, 0,
        auftrag_nr,         9, 1,  2,  3, 0, "%8d",    READONLY , 0, 0, 0,
        lief_krz1,          21, 1,  2, 14, 0, "",       READONLY , 0, 0, 0
};


static form artwconst = {4, 0, 0,_artwconst, 0, 0, 0, 0, &ArtWCFont};



static form artwformT;
static form artwform;


char *ChargenNr = "Chargen-Nr";
char *IdentNr   = "Ident-Nr";


// Masken fuer Wiegen

static field _artwformTW [] =
{
ChargenNr,                10, 1, 5, 3,  0, "", DISPLAYONLY,    0, 0, 0,
"Restmenge",              10, 1, 1,37,  0, "", DISPLAYONLY,    0, 0, 0,
"Liefermenge",            11, 1, 5,37,  0, "", DISPLAYONLY,    0, 0, 0,
aufps.lief_me_bz,         10, 1, 7,37,  0, "", DISPLAYONLY,    0, 0, 0,
"Wiegungen",              10, 1, 1,37,  0, "", DISPLAYONLY,    0, 0, 0,
"Bestellmenge",           13, 1, 5,23,  0, "", DISPLAYONLY,    0, 0, 0,
aufps.auf_me_bz,          10, 1, 7,23,  0, "", DISPLAYONLY,    0, 0, 0,
"Sollmenge",              11, 1, 3,37,  0, "", DISPLAYONLY,    0, 0, 0,
"Summe1",                  6, 1, 3,49,  0, "", REMOVED,    0, 0, 0,
"Summe2",                  6, 1, 5,49,  0, "", REMOVED,    0, 0, 0,
};

static form artwformTW = {10, 0, 0,_artwformTW, 0, 0, 0, 0, &ArtWFontT};


static field _artwformW [] =
{
        aufps.ls_charge,   19, 0, 6,  3, 0, "",       EDIT,     0, 0, 0,
        auf_me,            10, 1, 2, 37, 0, "%9.3f",  READONLY , 0, 0, 0,
        wiegzahl,          10, 1, 2, 37, 0, "%9.0f",  READONLY, 0, 0, 0,
        aufps.lief_me,     10, 1, 6, 37, 0, "%9.3f",  READONLY, 0, 0, 0,
        aufps.auf_me,      12, 1, 6, 23, 0, "%11.3f", READONLY, 0, 0, 0,
        aufps.auf_me_vgl,  10, 1, 4, 37, 0, "%9.3f",  READONLY, 0, 0, 0,
        summe1,            10, 1, 4, 49, 0, "%9.3f",  REMOVED, 0, 0, 0,
        summe2,            10, 1, 6, 49, 0, "%9.3f",  REMOVED, 0, 0, 0,
        zsumme1,            4, 1, 4, 61, 0, "%3d",    REMOVED, 0, 0, 0,
        zsumme2,            4, 1, 6, 61, 0, "%3d",    REMOVED, 0, 0, 0,
};

static form artwformW = {10, 0, 0,_artwformW, 0, 0, 0, 0, &ArtWFont};

static char ActEntnahmeGew [20] = {"0.0"};

static field _fEntnahmeGewTxt [] =
{
"Aktuelles Gewicht",       20, 1, 1,49,  0, "", DISPLAYONLY,    0, 0, 0,
};

static form fEntnahmeGewTxt = {1, 0, 0,_fEntnahmeGewTxt, 0, 0, 0, 0, &ArtWFontT};

static field _fEntnahmeGew [] =
{
ActEntnahmeGew,            10, 1, 2,49,  0, "%9.3f", READONLY,    0, 0, 0,
};

static form fEntnahmeGew = {1, 0, 0,_fEntnahmeGew, 0, 0, 0, 0, &ArtWFont};

// Masken fuer Auszeichnung

static field _artwformTP [] =
{
"Chargen-Nr",             10, 1, 5, 3,  0, "", DISPLAYONLY,    0, 0, 0,
"Vorgaben",                10, 1, 1,37,  0, "", DISPLAYONLY,    0, 0, 0,
veinh2,                    9, 1, 5,37,  0, "", DISPLAYONLY,    0, 0, 0,
aufps.lief_me_bz,         10, 1, 7,37,  0, "", REMOVED,        0, 0, 0,
"Liefermenge",            13, 1, 5,23,  0, "", DISPLAYONLY,        0, 0, 0,
aufps.lief_me_bz,         10, 1, 7,23,  0, "", DISPLAYONLY,    0, 0, 0,
aufps.auf_me_bz,          10, 1, 7,37,  0, "", DISPLAYONLY,    0, 0, 0,
veinh1,                    9, 1, 3,37,  0, "", DISPLAYONLY,    0, 0, 0,
"Summen",                  6, 1, 1,49,  0, "", DISPLAYONLY,    0, 0, 0,
"Summe2",                  6, 1, 3,49,  0, "", REMOVED,    0, 0, 0,
"Summe3",                  6, 1, 5,49,  0, "", REMOVED,    0, 0, 0,
"kg",                      6, 1, 7,49,  0, "", REMOVED,    0, 0, 0,
"St�ck",                   7, 1, 3, 61, 0, "", DISPLAYONLY , 0, 0, 0,
"Karton",                  7, 1, 5, 61, 0, "", DISPLAYONLY , 0, 0, 0,
"Palette",                 7, 1, 7, 61, 0, "", DISPLAYONLY , 0, 0, 0,
veinh1,                    9, 1, 3, 49,  0, "", DISPLAYONLY,    0, 0, 0,
veinh2,                    9, 1, 5, 49,  0, "", DISPLAYONLY,    0, 0, 0,
aufps.auf_me_bz,          10, 1, 7, 49,  0, "", DISPLAYONLY,    0, 0, 0,
};

static form artwformTP = {18, 0, 0,_artwformTP, 0, 0, 0, 0, &ArtWFontT};


static field _artwformP [] =
{
        aufps.ls_charge,   19, 0, 6,  3, 0, "",       EDIT,     0, 0, 0,
        vorgabe1,          12, 1, 2, 37, 0, "%11.3f", READONLY, 0, 0, 0,
        aufps.lief_me,     10, 1, 6, 23, 0, "%9.3f",  READONLY,  0, 0, 0,
        aufps.auf_me,      12, 1, 6, 37, 0, "%11.3f", READONLY, 0, 0, 0,
        vorgabe2,          12, 1, 4, 37, 0, "%11.3f",  READONLY, 0, 0, 0,
        summe1,            10, 1, 2, 49, 0, "%9.3f",  READONLY, 0, 0, 0,
        summe2,            10, 1, 4, 49, 0, "%9.3f",  READONLY, 0, 0, 0,
        summe3,            10, 1, 6, 49, 0, "%9.3f",  READONLY, 0, 0, 0,
        zsumme1,            4, 1, 2, 61, 0, "%3d",    READONLY, 0, 0, 0,
        zsumme2,            4, 1, 4, 61, 0, "%3d",    READONLY, 0, 0, 0,
        zsumme3,            4, 1, 6, 61, 0, "%3d",    READONLY, 0, 0, 0,
};

static form artwformP = {11, 0, 0,_artwformP, 0, 0, 0, 0, &ArtWFont};

// Ende Maske fuer Preisauszeichnung

static field _ftaratxt [] =
{
"Tara",                   5, 1,  8, 13, 0, "",  DISPLAYONLY, 0, 0, 0
};

static form ftaratxt = {1, 0, 0, _ftaratxt, 0, 0, 0, 0, &ftaratfont};

//021109
static field _sw_abzug [] =
{
aufps.txtschwundabzug,                   5, 1,  8, 23, 0, "",  DISPLAYONLY, 0, 0, 0
};
static form sw_abzug = {1, 0, 0, _sw_abzug, 0, 0, 0, 0, &zustxtfont};


static field _ftara [] =
{
        aufps.tara,        8, 1,  8, 20, 0, "%7.3f",  READONLY, 0, 0, 0
};

static form ftara = {1, 0, 0, _ftara, 0, 0, 0, 0, &ftarafont};


static field _ftexte [] =
{
 (char *) &STexte,          12, 2,  8, 40, 0, "",  COLBUTTON, 0, PosTexte, 0
};

static form ftexte = {1, 0, 0, _ftexte, 0, 0, 0, 0, &ftextefont};

//220712 A
static field _ffesttara [] =
{
(char *) &FestTara1,          15, 2,  8, 40, 0, "",  COLBUTTON, 0, dofest1, 0,
(char *) &FestTara2,          11, 2,  8, 50, 0, "",  COLBUTTON, 0, dofest2, 0,
(char *) &FestTara3,          11, 2,  8, 50, 0, "",  COLBUTTON, 0, dofest3, 0,
(char *) &FestTara4,          11, 2,  8, 50, 0, "",  COLBUTTON, 0, dofest4, 0,
(char *) &FestTara5,          11, 2,  8, 50, 0, "",  COLBUTTON, 0, dofest5, 0,
(char *) &FestTara6,          11, 2,  8, 50, 0, "",  COLBUTTON, 0, dofest6, 0,
(char *) &FestTara7,          11, 2,  8, 50, 0, "",  COLBUTTON, 0, dofest7, 0,
(char *) &FestTara8,          11, 2,  8, 50, 0, "",  COLBUTTON, 0, dofest8, 0,
(char *) &FestTara9,          11, 2,  8, 50, 0, "",  COLBUTTON, 0, dofest9, 0,
(char *) &FestTara10,         11, 2,  8, 50, 0, "",  COLBUTTON, 0, dofest10, 0,
(char *) &FestTara11,         11, 2,  8, 50, 0, "",  COLBUTTON, 0, dofest11, 0,
(char *) &FestTara12,         11, 2,  8, 50, 0, "",  COLBUTTON, 0, dofest12, 0,
(char *) &FestTara13,         11, 2,  8, 50, 0, "",  COLBUTTON, 0, dofest13, 0,
(char *) &FestTara14,         11, 2,  8, 50, 0, "",  COLBUTTON, 0, dofest14, 0,
(char *) &FestTara15,         11, 2,  8, 50, 0, "",  COLBUTTON, 0, dofest15, 0,
(char *) &FestTara16,         11, 2,  8, 50, 0, "",  COLBUTTON, 0, dofest16, 0,
(char *) &FestTara17,         11, 2,  8, 50, 0, "",  COLBUTTON, 0, dofest17, 0,
(char *) &FestTara18,         11, 2,  8, 50, 0, "",  COLBUTTON, 0, dofest18, 0,
(char *) &FestTara19,         11, 2,  8, 50, 0, "",  COLBUTTON, 0, dofest19, 0,
(char *) &FestTara20,         11, 2,  8, 50, 0, "",  COLBUTTON, 0, dofest20, 0,
(char *) &FestTara21,         11, 2,  8, 50, 0, "",  COLBUTTON, 0, dofest21, 0,
(char *) &FestTara22,         11, 2,  8, 50, 0, "",  COLBUTTON, 0, dofest22, 0,
(char *) &FestTara23,         11, 2,  8, 50, 0, "",  COLBUTTON, 0, dofest23, 0,
(char *) &FestTara24,         11, 2,  8, 50, 0, "",  COLBUTTON, 0, dofest24, 0,
(char *) &FestTara25,         11, 2,  8, 50, 0, "",  COLBUTTON, 0, dofest25, 0
};

static form ffesttara = {25, 0, 0, _ffesttara, 0, 0, 0, 0, &ffesttarafont};
//220712 E



void SwitchLiefAuf (void)
/**
Text fuer Autrags-Nr oder Lieferschein belegen.
**/
{
         if (LiefEnterAktiv)
         {
                   artwconstT.mask[2].feld   = "Lieferschein";
                   artwconstT.mask[2].length = 0;
                   artwconst.mask[2].feld    = lief_nr;
         }
         else
         {
                   artwconstT.mask[2].feld   = "Auftrag";
                   artwconstT.mask[2].length = 0;
                   artwconst.mask[2].feld    = auftrag_nr;
         }
}


void write_wewiepro (double brutto, double netto, double tara)
/**
In Tabelle we_wiepro schreiben.
**/
{
         char datum [12];
         char zeit  [12];
         char zeits  [12];
		 char hh [3];
		 char mm [3];
		 char ss [3];

		 sysdate (datum);
         systime (zeit);
		 //zeitformat "hhmmss LuD 27.02.2009"  
                 memcpy (hh, (char *) zeit, 2);
                 hh[2] = 0;
                 memcpy (mm, (char *) zeit + 3, 2);
                 mm[2] = 0;
                 memcpy (ss, (char *) zeit + 6, 2);
                 ss[2] = 0;
				 sprintf (zeits,"%s%s%s",hh,mm,ss);


		 we_wiepro.mdn = zerl_k.mdn;
         we_wiepro.fil = 0;
         we_wiepro.a   = ratod (aufps.a);
         we_wiepro.best_blg = 0;
		 if (atoi(EinAusgang) == EINGANG)
		 {
			strcpy (we_wiepro.blg_typ, "Z");  //Zerlegeeingang 
		 }
		 else if (atoi(EinAusgang) == AUSGANG)
		 {
			strcpy (we_wiepro.blg_typ, "z"); //Zerlegeausgang
		 }
		 else if (atoi(EinAusgang) == ZERLEGUNG)
		 {
			strcpy (we_wiepro.blg_typ, "z"); //Zerlegeausgang
		 }
		 else if (atoi(EinAusgang) == FEINZERLEGUNG)
		 {
			strcpy (we_wiepro.blg_typ, "z"); //Zerlegeausgang
		 }
		 else if (atoi(EinAusgang) == PRODUKTIONSSTART )
		 {
			strcpy (we_wiepro.blg_typ, "E"); //Eingang
		 }
		 else if (atoi(EinAusgang) == PRODUKTIONSFORTSCHRITT )
		 {
			strcpy (we_wiepro.blg_typ, "F"); //Fortschritt
		 }
		 else if (atoi(EinAusgang) == BUCHENFERTIGLAGER )
		 {
			strcpy (we_wiepro.blg_typ, "U"); //Umlagerung
		 }
		 else
		 {
			strcpy (we_wiepro.blg_typ, "U"); //Umlagerung
		 }
		 sprintf (we_wiepro.lief_rech_nr,"%8.0lf",zerl_k.partie);
         we_wiepro.lief_s = atoi(zerl_k.lief) ;
		 we_wiepro.me = brutto;
         we_wiepro.tara = tara;
         memcpy (we_wiepro.zeit, zeits, 6);
		 we_wiepro.zeit[6] = 0;	 ;
         we_wiepro.dat = dasc_to_long (datum);
         we_wiepro.sys = sys_peri.sys;
         strcpy (we_wiepro.pers, sys_ben.pers_nam);
         strcpy (we_wiepro.erf_kz, aufps.erf_kz);
         we_wiepro.anzahl = last_anz;
         strcpy (we_wiepro.qua_kz, "0");
         strcpy (we_wiepro.lief, zerl_k.lief);
         strcpy (we_wiepro.ls_charge, aufps.ls_charge);
         strcpy (we_wiepro.ls_ident, " ");
		 WeWiePro.dbinsert ();
         we_wiepro.es_nr = 0l;
         strcpy (we_wiepro.pers, "");
}


int GetGewicht (char *datei, double *brutto, double *netto, double *tara)
/**
Gewichtswerte aus der Datei ergebnis holen.
**/
{
         FILE *fp;
         char buffer [81];

         fp = fopen (datei, "r");
         if (fp == NULL) return (-1);

/*
         if (testmode)
         {
                    disp_messG ("ergebnis ge�ffnet", 1);
         }
*/

         while (fgets (buffer, 80, fp))
         {
/*
               if (testmode)
               {
                     print_messG (1, "gelesen %s", buffer);
               }
*/
               switch (buffer [0])
               {
                        case 'B' :
                                *brutto = (double) ratod (&buffer[1]);
                                break;
                        case 'N' :
                                *netto = (double) ratod (&buffer[1]);
                                break;
                        case 'T' :
                                *tara = (double) ratod (&buffer[1]);
                                break;
                        case 'E' :
                                if (strlen (&buffer[1]) < 7)
                                {
                                    sprintf (we_wiepro.pers, "%06ld", atol (&buffer[1]));
                                    we_wiepro.es_nr = atol (&buffer[1]);
                                }
                                else
                                {
                                    sprintf (we_wiepro.pers, "%012.0lf", 
                                             ratod (&buffer[1]));
                                }
               }
         }
         fclose (fp);

         if (*netto == (double) 0)
         {
                        *netto = *brutto - *tara;
         }
         else if (*brutto == (double) 0)
         {
                        *brutto = *netto + *tara;
         }

         if (*tara == (double) 0.0)
         {
                        *tara = *brutto - *netto;
         }

         if (WiegeTyp == STANDARD)
         {
/*
               if (autoan)
               {
                       akt_tara = *brutto;
               }
               else
*/
               {
                       akt_tara = *tara;
               }
         }


         if (WiegeTyp == ENTNAHME && StartWiegen == FALSE)
         {
                   double gew = EntnahmeActGew - *netto;
                   gew -= EntnahmeTara;
                   if (gew < 0.0)
                   {
                       gew = 0.0;
                   }
                   EntnahmeActGew = *netto;
                   sprintf (ActEntnahmeGew, "%.3lf",EntnahmeActGew);
                   *netto = gew;
                   *tara  = EntnahmeTara;
                   *brutto = *netto + *tara;
         }

         if (StartWiegen == FALSE)
         {
		               write_wewiepro (*brutto, *netto, *tara);
         }

/*
         if (testmode)
         {
                    print_messG (1,"%.2lf  %.2lf %-2lf", *brutto, *netto, *tara);
         }
*/
         return 0;
}

void fnbexec (int arganz, char *argv[])
/**
**/
{
        char aufruf [256];
        int i;

        if (getenv (BWS) == NULL) return;
        if (fnbproc != NULL)
		{
                     if (fnbInstanceproc != NULL)
					 {
			              (*fnbInstanceproc) (hMainInst);
					 }
			         int ret = (*fnbproc) (arganz, argv);
					 return;
		}

        sprintf (aufruf, "%s\\fnb.exe", waadir);

        for (i = 1; i < arganz; i ++)
        {
                     strcat (aufruf, " ");
                     strcat (aufruf, argv[i]);
        }

//        WaitConsoleExec (aufruf, CREATE_NO_WINDOW);
        ProcWaitExec (aufruf, SW_SHOWMINIMIZED, -1, 0, -1, 0);
}


BOOL WaaInitOk (char *wa)
/**
Test, ob die Waage schon initialisiert wurde.
**/
{
        static char *waadirs [20];
        static int waaz = 0;
        int i;

        if (waaz == 20) return FALSE;
        clipped (wa);
        for (i = 0; i < waaz; i ++)
        {
            if (strcmp (wa, waadirs [i]) == 0)
            {
                return TRUE;
            }
        }
        waadirs [i] = (char *) malloc (strlen (wa) + 3);
        if (waadirs [i] == NULL)
        {
            return FALSE;
        }
        strcpy (waadirs[i], wa);
        if (waaz < 20) waaz ++;
        return FALSE;
}



void WaageInit (void)
/**
Waage Initialisieren.
**/
{
        static int arganz;
        static char *args[4];
		char WaaDll [512];



        if (WaaInitOk (waadir)) return;
		waaLibrary = fnbproc = NULL;
        sprintf (WaaDll, "%s\\fnb.dll", waadir);
        waaLibrary = LoadLibrary (WaaDll);
        if (waaLibrary != NULL)
		{
               fnbInstanceproc = (int (*) (void*)) GetProcAddress (waaLibrary, fnbInstancename);
               fnbproc = (int  (*) (int, char **)) GetProcAddress (waaLibrary, fnbname);
		}

        deltaraw ();
        WaaInit = 1;
        arganz = 3;
        args[0] = "Leer";
        args[1] = "0";
        args[2] = waadir;
        fnbexec (arganz, args);
}

int doWOK (void)
/**
Wiegen durchfuehren.
**/
{
	    if (ChargeZwang == 1) DoChargeZwang(); //130509
	    if (artwform.mask[0].attribut == EDIT)
		{
                 GetWindowText (artwform.mask[0].feldid,
                      artwform.mask[0].feld,
                      artwform.mask[0].length);
		}
        strcpy (aufps.s, "3");
        break_wieg = 1;
        return 0;
}
int doWF5 (void)
/**
Wiegen durchfuehren.
**/
{
	    if (ChargeZwang == 1) DoChargeZwang(); //130509
	    if (artwform.mask[0].attribut == EDIT)
		{
                 GetWindowText (artwform.mask[0].feldid,
                      artwform.mask[0].feld,
                      artwform.mask[0].length);
		}
//        strcpy (aufps.s, "3");
        break_wieg = 1;
        return 0;
}


void setautotara (BOOL an)
/**
Wiegen durchfuehren.
**/
{
	    if (an)
		{
              AutoTara.aktivate = 4;
              autoan = 1;
        }
        else
        {
              AutoTara.aktivate = 3;
              autoan = 0;
        }
        AutoTara.text3 = anaus [autoan];
}


int doauto (void)
/**
Wiegen durchfuehren.
**/
{
        if (AutoTara.aktivate == 4)
        {
              autoan = 1;
        }
        else
        {
              autoan = 0;
        }
        AutoTara.text3 = anaus [autoan];
        current_wieg = autopos;
        return 0;
}

void PressAuto ()
/**
Autotara druecken.
**/
{
       SendMessage (WieButton.mask[autopos].feldid, WM_LBUTTONDOWN, 0l, 0l);
       SendMessage (WieButton.mask[autopos].feldid, WM_LBUTTONUP, 0l, 0l);
}


int dofest (int i) //220712
{
	int AnAus;
        if (_WieButtonT[0].feldid != NULL)
		{
			return 0; 
		}

		if (QsPosWieg == TRUE)
		{
			if (FestAn[i] == 1)
			{
				FestAn[i] = 2;
			}
			else if (FestAn[i] == 2)
			{
	  		    FestAn[i] = 1;
			}
			else
			{
	  		    FestAn[i] = 1;
			}
		}
		else
		{
			if (FestAn[i])
			{
				FestAn[i] = 0;
				akt_tara = (double) 0;
			}
			else
			{
	  		    FestAn[i] = 1;
				akt_tara = (double) ratod (ftabarr[i].ptwer1);
			}
			AnAus = FestAn[i];
			int x;
			for ( x=0;x<AnzFestTara;x++)
			{
				FestAn[x] = 0;
			}
	
			current_wieg = autopos;
			FestAn[i] = AnAus;
		}
	    SetFestAllBitmaps ();

		if (WiegWindow != NULL)
		{
					display_form (WiegWindow, &ffesttara, 0, 0);
		}

        sprintf (aufps.tara, "%.3lf", akt_tara);
        sprintf (ScaleTara, "%.3lf", akt_tara);
		tarieren (akt_tara); 
        return 0;

}
//220712 A
int dofest1 (void) { return dofest(0);} 
int dofest2 (void) { return dofest(1);} 
int dofest3 (void) { return dofest(2);} 
int dofest4 (void) { return dofest(3);} 
int dofest5 (void) { return dofest(4);} 
int dofest6 (void) { return dofest(5);} 
int dofest7 (void) { return dofest(6);} 
int dofest8 (void) { return dofest(7);} 
int dofest9 (void) { return dofest(8);} 
int dofest10 (void) { return dofest(9);} 
int dofest11 (void) { return dofest(10);} 
int dofest12 (void) { return dofest(11);} 
int dofest13 (void) { return dofest(12);} 
int dofest14 (void) { return dofest(13);} 
int dofest15 (void) { return dofest(14);} 
int dofest16 (void) { return dofest(15);} 
int dofest17 (void) { return dofest(16);} 
int dofest18 (void) { return dofest(17);} 
int dofest19 (void) { return dofest(18);} 
int dofest20 (void) { return dofest(19);} 
int dofest21 (void) { return dofest(20);} 
int dofest22 (void) { return dofest(21);} 
int dofest23 (void) { return dofest(22);} 
int dofest24 (void) { return dofest(23);} 
int dofest25 (void) { return dofest(24);} 
//220712 E





int festtara (void)
/**
Wiegen durchfuehren.
**/
{
        double netto;
        double brutto;
        double tara;
        static int arganz;
        static char ftara [11];
        static char *args[5];
        char buffer [20];


        syskey = 0;
        ShowFestTara (buffer);

        if (syskey == KEY5)
        {
                  return 0;
        }


        sprintf (ftara, "%.3lf", (double) ratod (buffer));
        if (fest_tara_anz)
        {
  		        NuBlock.SetParent (LogoWindow);
                sprintf (buffer, "%d", 1);
                NuBlock.EnterCalcBox (WiegWindow,"  Anzahl ",  buffer, 5, "%4df", EntNuProc);
                if (syskey != KEY5 && atoi (buffer) > 1)
                {
                   sprintf (ftara, "%.3lf", (double) ratod (ftara) * atoi (buffer));
                }
        }

		if (fest_tara_sum || autoan)
		{
                  akt_tara = akt_tara + (double) ratod (ftara);
		}
		else
		{
                  akt_tara = (double) ratod (ftara);
		}
        sprintf (aufps.tara, "%.3lf", akt_tara);
        sprintf (ftara, "%.3lf", akt_tara);
        if (WiegeTyp == ENTNAHME)
        {
                  EntnahmeTara = akt_tara;
                  return 0;
        }
        arganz = 4;
        args[0] = "Leer";
        args[1] = "23";
        args[2] = waadir;
        args[3] = (char *) ftara;
        fnbexec (arganz, args);
        brutto = netto = tara = (double) 0.0;
        GetGewicht (ergebnis, &brutto, &netto, &tara);
        return 0;

/*
        ShowFestTara (buffer);
        sprintf (ftara, "%.3lf", (double) ratod (buffer));
        arganz = 4;
        args[0] = "Leer";
        args[1] = "23";
        args[2] = waadir;
        args[3] = (char *) ftara;
        fnbexec (arganz, args);
        brutto = netto = tara = (double) 0.0;
        GetGewicht (ergebnis, &brutto, &netto, &tara);
        sprintf (aufps.tara, "%.3lf", ratod (ftara));
        display_form (WiegKg, &WiegKgE, 0, 0);
        return 0;
*/
}

int deltaraw (void)
/**
Wiegen durchfuehren.
**/
{
        static int arganz;
        static char *args[4];

        arganz = 3;
        args[0] = "Leer";
        args[1] = "5";
        args[2] = waadir;
        fnbexec (arganz, args);
        return 0;
}

int deltara (void)
/**
Wiegen durchfuehren.
**/
{
        double netto;
        double brutto;
        double tara;
        static int arganz;
        static char *args[4];

        sprintf (aufps.tara, "%.3lf", (double) 0.0);
        if (WiegeTyp == ENTNAHME)
        {
                  EntnahmeTara = 0.0;
                  akt_tara = 0.0;
//             	  display_form (WiegWindow, &ftara, 0, 0);
//                  return 0;
        }
        arganz = 3;
        args[0] = "Leer";
        args[1] = "5";
        args[2] = waadir;
        fnbexec (arganz, args);
        brutto = netto = tara = (double) 0.0;
        GetGewicht (ergebnis, &brutto, &netto, &tara);
        akt_tara = (double) 0.0;
		display_form (WiegWindow, &ftara, 0, 0);
        return 0;

/*
        arganz = 3;
        args[0] = "Leer";
        args[1] = "5";
        args[2] = waadir;
        fnbexec (arganz, args);
        brutto = netto = tara = (double) 0.0;
        GetGewicht (ergebnis, &brutto, &netto, &tara);
        sprintf (aufps.tara, "%.3lf", (double) 0.0);
        display_form (WiegKg, &WiegKgE, 0, 0);
        akt_tara = (double) 0.0;
        return 0;
*/
}

int settara (void)
/**
Wiegen durchfuehren.
**/
{
        double netto;
        double brutto;
        double tara;
        static int arganz;
        static char *args[4];

        arganz = 3;
        args[0] = "Leer";
        args[1] = "4";
        args[2] = waadir;
        fnbexec (arganz, args);
        if (WiegeTyp == STANDARD)
        {
             Sleep (AutoSleep);
             arganz = 3;
             args[0] = "Leer";
             args[1] = "11";
             args[2] = waadir;
             fnbexec (arganz, args);
        }
        brutto = netto = tara = (double) 0.0;
        GetGewicht (ergebnis, &brutto, &netto, &tara);
		sprintf (aufps.tara, "%.3lf", akt_tara);
        if (WiegeTyp == ENTNAHME)
        {
                  EntnahmeTara = akt_tara;
        }
		display_form (WiegWindow, &ftara, 0, 0);
        return 0;

/*
        arganz = 3;
        args[0] = "Leer";
        args[1] = "4";
        args[2] = waadir;
        fnbexec (arganz, args);
        brutto = netto = tara = (double) 0.0;
        GetGewicht (ergebnis, &brutto, &netto, &tara);
        sprintf (aufps.tara, "%.3lf", tara);
        display_form (WiegKg, &WiegKgE, 0, 0);
        return 0;
*/
}

int settara0 (void)
{
        if (WiegeTyp == STANDARD)
        {
            return settara ();
        }
        StartWiegen = TRUE;
        dowiegen ();
        StartWiegen = FALSE;
        sprintf (ActEntnahmeGew, "%.3lf", EntnahmeActGew);
        return 0;
}


int setgebindetara (void)
{
	    GebTara = TRUE;
        NuBlock.NumEnterBreak ();
	    return 0;
}



//f�r Anzeige Gebinde-tara
struct PTG
{
        char ptbez [60];
        char tara [9];
};

struct PTG ptabG, ptabarrG [20];
static int ptabanzG = 0;


static int ptidxG;

mfont PtFontG = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                               RGB (255, 255, 255),
                               1,
                               NULL};

static field _fptabformG [] =
{
        ptabG.ptbez,     48, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        ptabG.tara,       6, 1, 0, 49, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form fptabformG = {2, 0, 0, _fptabformG, 0, 0, 0, 0, &PtFontG};

static field _fptabubG [] =
{
        "Gebinde",     48, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        "Tara",         6, 1, 0, 49, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form fptabubG = {2, 0, 0, _fptabubG, 0, 0, 0, 0, &PtFontG};

static field _fptabvlG [] =
{      "1",                  1,  0, 0, 48, 0, "", NORMAL, 0, 0, 0,
};

static form fptabvlG = {1, 0, 0, _fptabvlG, 0, 0, 0, 0, &PtFontG};



int ShowGebindeTara (char *pttara)
/**
Ausahlfenster fuer Festtara anzeigen.
**/
{
		double gew = 0.0;
		double dtara = 0.0;
        if (ptabanzG == 0)
        {
			   gebindetara.a = 0.0;
               dsqlstatus = gebindetara_class.readfirstall ();
			   if (dsqlstatus == 100)
			   {
				   strcpy(gebindetara.bezeichnung,"Kein Gebinde");
				   gebindetara.lfd = 1;
				   gebindetara.zaehler = 9999999;
				   gebindetara.tara = 0.0;
				   gebindetara_class.dbupdate();
    	            commitwork ();
					beginwork ();

			   }
               dsqlstatus = gebindetara_class.readfirstall ();
               while (dsqlstatus == 0)
               {
                   strcpy (ptabarrG[ptabanzG].ptbez,  gebindetara.bezeichnung);
				   
				   if (gebindetara.gebinde1 > 0)
				   {
						DbClass.sqlout ((long *)   &gew,  3, 0);
						DbClass.sqlin  ((double *) &gebindetara.gebinde1,   3, 0);
						if (DbClass.sqlcomm ("select a_gew from a_bas where a = ?") == 0)
						{
							dtara += gew * gebindetara.anz1;
						}
				   }

				   if (gebindetara.gebinde2 > 0)
				   {
						DbClass.sqlout ((long *)   &gew,  3, 0);
						DbClass.sqlin  ((double *) &gebindetara.gebinde2,   3, 0);
						if (DbClass.sqlcomm ("select a_gew from a_bas where a = ?") == 0)
						{
							dtara += gew * gebindetara.anz2;
						}
				   }
				   if (gebindetara.gebinde3 > 0)
				   {
						DbClass.sqlout ((long *)   &gew,  3, 0);
						DbClass.sqlin  ((double *) &gebindetara.gebinde3,   3, 0);
						if (DbClass.sqlcomm ("select a_gew from a_bas where a = ?") == 0)
						{
							dtara += gew * gebindetara.anz3;
						}
				   }
				   if (gebindetara.gebinde4 > 0)
				   {
						DbClass.sqlout ((long *)   &gew,  3, 0);
						DbClass.sqlin  ((double *) &gebindetara.gebinde4,   3, 0);
						if (DbClass.sqlcomm ("select a_gew from a_bas where a = ?") == 0)
						{
							dtara += gew * gebindetara.anz4;
						}
				   }
				   if (gebindetara.gebinde5 > 0)
				   {
						DbClass.sqlout ((long *)   &gew,  3, 0);
						DbClass.sqlin  ((double *) &gebindetara.gebinde5,   3, 0);
						if (DbClass.sqlcomm ("select a_gew from a_bas where a = ?") == 0)
						{
							dtara += gew * gebindetara.anz5;
						}
				   }
				   if (gebindetara.gebinde6 > 0)
				   {
						DbClass.sqlout ((long *)   &gew,  3, 0);
						DbClass.sqlin  ((double *) &gebindetara.gebinde6,   3, 0);
						if (DbClass.sqlcomm ("select a_gew from a_bas where a = ?") == 0)
						{
							dtara += gew * gebindetara.anz6;
						}
				   }

				    gebindetara.tara = dtara; 
		            sprintf (ptabarrG[ptabanzG].tara,"%.3lf", gebindetara.tara);

                   ptabanzG ++;
                   if (ptabanzG == 40) break;
				   dtara = 0.0;
                   dsqlstatus = gebindetara_class.readnextall ();
               }
        }
        EnableWindow (WiegWindow, FALSE);


		sprintf(Zuruecktext,"Neues Geb.");

        ptidxG = ShowPtBox (WiegWindow, (char *) ptabarrG, ptabanzG,
                     (char *) &ptabG,(int) sizeof (struct PTG),
                     &fptabformG, &fptabvlG, &fptabubG);
		 sprintf(Zuruecktext,"Zurueck F5");
        EnableWindow (WiegWindow, TRUE);
        strcpy (pttara, ptabarrG [ptidxG].tara);
        return ptidxG;
}


//f�r Anzeige Verpackungs-tara
struct PTV
{
        char ptbez [60];
        char tara [9];
};

struct PTV ptabV, ptabarrV [20];
static int ptabanzV = 0;


static int ptidxV;

mfont PtFontV = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                               RGB (255, 255, 255),
                               1,
                               NULL};

static field _fptabformV [] =
{
        ptabV.ptbez,     48, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        ptabV.tara,       6, 1, 0, 49, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form fptabformV = {2, 0, 0, _fptabformV, 0, 0, 0, 0, &PtFontV};

static field _fptabubV [] =
{
        "Verpackungen",     48, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        "Tara",         6, 1, 0, 49, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form fptabubV = {2, 0, 0, _fptabubV, 0, 0, 0, 0, &PtFontV};

static field _fptabvlV [] =
{      "1",                  1,  0, 0, 48, 0, "", NORMAL, 0, 0, 0,
};

static form fptabvlV = {1, 0, 0, _fptabvlV, 0, 0, 0, 0, &PtFontV};



int ShowVerpackungTara (char *pttara)
/**
Ausahlfenster fuer Festtara anzeigen.
**/
{
		double gew = 0.0;
		double dtara = 0.0;
		double akt_artikel ;
        if (ptabanzV == 0)
        {
			   gebindetara.a = ratod(aufps.a);
               dsqlstatus = gebindetara_class.readfirstall ();
			   if (dsqlstatus == 100)
			   {
				   strcpy(gebindetara.bezeichnung,"Keine Verpackung");
				   gebindetara.lfd = 1;
				   gebindetara.zaehler = 9999999;
				   gebindetara.tara = 0.0;
				   gebindetara_class.dbupdate();
    	            commitwork ();
					beginwork ();

			   }
               dsqlstatus = gebindetara_class.readfirstall ();
			   akt_artikel = ratod(aufps.a);
               while (dsqlstatus == 0)
               {
                   strcpy (ptabarrV[ptabanzV].ptbez,  gebindetara.bezeichnung);
				   
				   if (gebindetara.gebinde1 > 0)
				   {
						DbClass.sqlout ((long *)   &gew,  3, 0);
						DbClass.sqlin  ((double *) &akt_artikel,   3, 0);
						DbClass.sqlin  ((double *) &gebindetara.gebinde1,   3, 0);
						if (DbClass.sqlcomm ("select a_gew from a_tara where a = ? and a_vpk = ?") == 0)
						{
							dtara += gew * gebindetara.anz1;
						}
				   }

				   if (gebindetara.gebinde2 > 0)
				   {
						DbClass.sqlout ((long *)   &gew,  3, 0);
						DbClass.sqlin  ((double *) &akt_artikel,   3, 0);
						DbClass.sqlin  ((double *) &gebindetara.gebinde2,   3, 0);
						if (DbClass.sqlcomm ("select a_gew from a_tara where a = ? and a_vpk = ?") == 0)
						{
							dtara += gew * gebindetara.anz2;
						}
				   }
				   if (gebindetara.gebinde3 > 0)
				   {
						DbClass.sqlout ((long *)   &gew,  3, 0);
						DbClass.sqlin  ((double *) &akt_artikel,   3, 0);
						DbClass.sqlin  ((double *) &gebindetara.gebinde3,   3, 0);
						if (DbClass.sqlcomm ("select a_gew from a_tara where a = ? and a_vpk = ?") == 0)
						{
							dtara += gew * gebindetara.anz3;
						}
				   }
				   if (gebindetara.gebinde4 > 0)
				   {
						DbClass.sqlout ((long *)   &gew,  3, 0);
						DbClass.sqlin  ((double *) &akt_artikel,   3, 0);
						DbClass.sqlin  ((double *) &gebindetara.gebinde4,   3, 0);
						if (DbClass.sqlcomm ("select a_gew from a_tara where a = ? and a_vpk = ?") == 0)
						{
							dtara += gew * gebindetara.anz4;
						}
				   }
				   if (gebindetara.gebinde5 > 0)
				   {
						DbClass.sqlout ((long *)   &gew,  3, 0);
						DbClass.sqlin  ((double *) &akt_artikel,   3, 0);
						DbClass.sqlin  ((double *) &gebindetara.gebinde5,   3, 0);
						if (DbClass.sqlcomm ("select a_gew from a_tara where a = ? and a_vpk = ?") == 0)
						{
							dtara += gew * gebindetara.anz5;
						}
				   }
				   if (gebindetara.gebinde6 > 0)
				   {
						DbClass.sqlout ((long *)   &gew,  3, 0);
						DbClass.sqlin  ((double *) &akt_artikel,   3, 0);
						DbClass.sqlin  ((double *) &gebindetara.gebinde6,   3, 0);
						if (DbClass.sqlcomm ("select a_gew from a_tara where a = ? and a_vpk = ?") == 0)
						{
							dtara += gew * gebindetara.anz6;
						}
				   }

				    gebindetara.tara = dtara; 
		            sprintf (ptabarrV[ptabanzV].tara,"%.3lf", gebindetara.tara);

                   ptabanzV ++;
                   if (ptabanzV == 40) break;
				   dtara = 0.0;
                   dsqlstatus = gebindetara_class.readnextall ();
               }
        }
        EnableWindow (WiegWindow, FALSE);


		sprintf(Zuruecktext,"Neue Verpackung.");

        ptidxV = ShowPtBox (WiegWindow, (char *) ptabarrV, ptabanzV,
                     (char *) &ptabV,(int) sizeof (struct PTV),
                     &fptabformV, &fptabvlV, &fptabubV);
		 sprintf(Zuruecktext,"Zurueck F5");
        EnableWindow (WiegWindow, TRUE);
        strcpy (pttara, ptabarrV [ptidxV].tara);
        return ptidxV;
}




int dogebindetara (char *ftara)
{
	    sprintf (ftara, "%7.3lf", (double) 0.0);
		EnterLeerPos (LEERGUT);
	    GebTara = FALSE;
		if (syskey == KEY5) return 0;
		GetGebGewicht (ftara,LEERGUT);
	    return 0;
}


void EnterA_Krz (void)
{
	    char buffer [80] = {""};
		
		strcpy (buffer, a_tara.a_krz);
		textblock = new TEXTBLOCK;
		textblock->SetLongText (FALSE);
		EnableWindows (hMainWindow, FALSE);
		EnableWindow (WiegWindow, FALSE);
        textblock->MainInstance (hMainInst, WiegWindow);
        textblock->ScreenParam (scrfx, scrfy);
        textblock->SetUpshift (TRUE);
        textblock->EnterTextBox (BackWindow1, "Kurz-Bezeichnung", buffer, 16, "", EntNumProc);
		EnableWindows (hMainWindow, TRUE);
		EnableWindow (WiegWindow, TRUE);
		delete textblock;
		textblock = NULL;
		if (syskey != KEY5)
		{
			strcpy (a_tara.a_krz, buffer);
		}
}




int dogebindetara (void)
{
        double netto;
        double brutto;
        double tara;
        static int arganz;
        static char ftara [11];
        static char *args[5];
        char buffer [20];
		int idx ;

/******************************
        syskey = 0;
		gebindetara.a = 0.0;
        idx = ShowGebindeTara (buffer);
		memcpy (&gebindetara, &gebindetara_null, sizeof (struct GEBINDETARA));
        sprintf (ftara, "%.3lf", (double) ratod (buffer));

        if (syskey == KEY5)
        {
		    sprintf (ftara, "%7.3lf", (double) 0.0);
		    GebTara = TRUE;

			sprintf(MinusText,"Neu Leerg.");
			EnterLeerPos (LEERGUT);
			sprintf(MinusText,",");
			GebTara = FALSE;
			if (syskey == KEY5) return 0;
			GetGebGewicht (ftara,LEERGUT);
			gebindetara.tara = ratod(ftara);
	        strcpy (gebindetara.bezeichnung, KopfTextTab[0]);
			gebindetara_class.dbreadfirst();
			gebindetara.zaehler ++;
			ptabanzG = 0;

			if (strlen(gebindetara.bezeichnung) > 1)
			{
				gebindetara.a = 0.0;
				gebindetara_class.dbupdate ();
				ptabanzG = 0; 
  	            commitwork ();
				beginwork ();
			}

        }
		else
		{
	        strcpy (gebindetara.bezeichnung, ptabarrG[idx].ptbez);
			gebindetara_class.dbreadfirst();
			gebindetara.zaehler ++;
			if (strlen(gebindetara.bezeichnung) > 1)
			{
				gebindetara.a = 0.0;
				gebindetara_class.dbupdate ();
				ptabanzG = 0;
	            commitwork ();
				beginwork ();
			}

		}

        if (fest_tara_anz && ratod(ftara) > 0.0)
        {
  		        NuBlock.SetParent (LogoWindow);
                sprintf (buffer, "%d", 1);
                NuBlock.EnterCalcBox (WiegWindow,"  Anzahl ",  buffer, 5, "%4df", EntNuProc);
                if (syskey != KEY5 && atoi (buffer) > 1)
                {
                   sprintf (ftara, "%.3lf", (double) ratod (ftara) * atoi (buffer));
                }
        }
        akt_tara = (double) ratod (ftara);





************************/


		// ======= zuerst Gebindetara (artikelunabh�ngig (gebindetara.a = 0) =======================

		akt_tara = 0.0;

        syskey = 0;
		memcpy (&gebindetara, &gebindetara_null, sizeof (struct GEBINDETARA));
		gebindetara.a = 0.0;
		NextStep = LEERGUT;
		while (NextStep == LEERGUT)
		{
			idx = ShowGebindeTara (buffer);
			memcpy (&gebindetara, &gebindetara_null, sizeof (struct GEBINDETARA));
			sprintf (ftara, "%.3lf", (double) ratod (buffer));
	
			if (syskey == KEY5)
			{
			    sprintf (ftara, "%7.3lf", (double) 0.0);
				GebTara = TRUE;
	
				NextStep = NEUES_LEERGUT;
				while (NextStep == NEUES_LEERGUT)
				{
					sprintf(MinusText,"Leergut");
					NextStep = LEERGUT;
					EnterLeerPos (LEERGUT);
					sprintf(MinusText,",");
	
					if (NextStep == NEUES_LEERGUT)
					{
						double dtara = AuswahlVerpackungstara (NEUES_LEERGUT);
					}
				}
				GebTara = FALSE;
				GetGebGewicht (ftara,LEERGUT);
				strcpy (gebindetara.bezeichnung, KopfTextTab[0]);
				gebindetara_class.dbreadfirst();
				gebindetara.tara = ratod(ftara);
				gebindetara.zaehler ++;
				ptabanzG = 0;
	
				if (strlen(gebindetara.bezeichnung) > 1 && gebindetara.tara > 0.0001)
				{
					gebindetara.a = 0.0;
					gebindetara_class.dbupdate ();
					ptabanzV = 0;
  					commitwork ();
					beginwork ();
				}
	
			}
			else
			{
				NextStep = SETZE_TARA;
				strcpy (gebindetara.bezeichnung, ptabarrG[idx].ptbez);
				gebindetara.a = 0.0;
				gebindetara_class.dbreadfirst();
				gebindetara.zaehler ++;
				if (strlen(gebindetara.bezeichnung) > 1 && gebindetara.tara > 0.0001)
				{
					gebindetara_class.dbupdate ();
					ptabanzV = 0;
					commitwork ();
					beginwork ();
				}
	
			}
		}

        if (fest_tara_anz && ratod(ftara) > 0.0)
        {
  		        NuBlock.SetParent (LogoWindow);
                sprintf (buffer, "%d", 1);
                NuBlock.EnterCalcBox (WiegWindow,"  Anzahl ",  buffer, 5, "%4df", EntNuProc);
                if (syskey != KEY5 && atoi (buffer) > 1)
                {
                   sprintf (ftara, "%.3lf", (double) ratod (ftara) * atoi (buffer));
                }
        }
        akt_tara += (double) ratod (ftara);










		// ======= jetzt Verpackungstara ==(artikelabh�ngig (gebindetara.a == aufps.a)


        syskey = 0;
		memcpy (&gebindetara, &gebindetara_null, sizeof (struct GEBINDETARA));
		gebindetara.a = ratod(aufps.a);
		NextStep = VERPACKUNGEN;
		while (NextStep == VERPACKUNGEN)
		{
			idx = ShowVerpackungTara (buffer);
			memcpy (&gebindetara, &gebindetara_null, sizeof (struct GEBINDETARA));
			sprintf (ftara, "%.3lf", (double) ratod (buffer));
	
			if (syskey == KEY5)
			{
			    sprintf (ftara, "%7.3lf", (double) 0.0);
				GebTara = TRUE;
	
				NextStep = NEUE_VERPACKUNGEN;
				while (NextStep == NEUE_VERPACKUNGEN)
				{
					sprintf(MinusText,"Verpack");
					NextStep = VERPACKUNGEN;
					EnterLeerPos (VERPACKUNGEN);
					sprintf(MinusText,",");
	
					if (NextStep == NEUE_VERPACKUNGEN)
					{
						double dtara = AuswahlVerpackungstara (NEUE_VERPACKUNGEN);
					}
				}
				GebTara = FALSE;
				GetGebGewicht (ftara,VERPACKUNGEN);
				strcpy (gebindetara.bezeichnung, KopfTextTab[0]);
				gebindetara_class.dbreadfirst();
				gebindetara.tara = ratod(ftara);
				gebindetara.zaehler ++;
				ptabanzG = 0;
	
				if (strlen(gebindetara.bezeichnung) > 1 && gebindetara.tara > 0.0001)
				{
					gebindetara.a = ratod(aufps.a);
					gebindetara_class.dbupdate ();
					ptabanzV = 0;
  					commitwork ();
					beginwork ();
				}
	
			}
			else
			{
				NextStep = SETZE_TARA;
				strcpy (gebindetara.bezeichnung, ptabarrG[idx].ptbez);
				gebindetara.a = ratod(aufps.a);
				gebindetara_class.dbreadfirst();
				gebindetara.zaehler ++;
				if (strlen(gebindetara.bezeichnung) > 1 && gebindetara.tara > 0.0001)
				{
					gebindetara_class.dbupdate ();
					ptabanzV = 0;
					commitwork ();
					beginwork ();
				}
	
			}
		}

        if (fest_tara_anz && ratod(ftara) > 0.0)
        {
  		        NuBlock.SetParent (LogoWindow);
                sprintf (buffer, "%d", 1);
                NuBlock.EnterCalcBox (WiegWindow,"  Anzahl ",  buffer, 5, "%4df", EntNuProc);
                if (syskey != KEY5 && atoi (buffer) > 1)
                {
                   sprintf (ftara, "%.3lf", (double) ratod (ftara) * atoi (buffer));
                }
        }
        akt_tara += (double) ratod (ftara);





        sprintf (aufps.tara, "%.3lf", akt_tara);
        sprintf (ftara, "%.3lf", akt_tara);
        if (WiegeTyp == ENTNAHME)
        {
                  EntnahmeTara = akt_tara;
                  return 0;
        }
        arganz = 4;
        args[0] = "Leer";
        args[1] = "23";
        args[2] = waadir;
        args[3] = (char *) ftara;
        fnbexec (arganz, args);
        brutto = netto = tara = (double) 0.0;
        GetGewicht (ergebnis, &brutto, &netto, &tara);

	    return 0;
}

void tarieren (double tara) //220712
{
        static char ftara [11];
        static int arganz;
        static char *args[4];

        sprintf (ftara, "%.3lf", tara);
        arganz = 4;
        args[0] = "Leer";
        args[1] = "23";
        args[2] = waadir;
        args[3] = (char *) ftara;
        fnbexec (arganz, args);
}


int handtara (void)
/**
Wiegen durchfuehren.
**/
{
        double netto;
        double brutto;
        double tara;
        static char ftara [11];
        static int arganz;
        static char *args[4];

        sprintf (ftara, "%7.3lf", (double) 0.0);
//        BuChoise.text1 = "&Gebinde";
		NuBlock.SetParent (LogoWindow);
        NuBlock.SetChoise (setgebindetara, 0, "&Gebinde");
        NuBlock.EnterCalcBox (WiegWindow,"  Tara  ",  ftara, 8, "%7.3f", EntNuProc);
        SetAktivWindow (WiegWindow);
		if (GebTara)
		{
			dogebindetara (ftara);
		}

		if (hand_tara_sum || autoan)
		{
                  akt_tara = akt_tara + (double) ratod (ftara);
		}
		else
		{
                  akt_tara = (double) ratod (ftara);
		}

        sprintf (aufps.tara, "%.3lf", akt_tara);
        sprintf (ftara, "%.3lf", akt_tara);
        if (WiegeTyp == ENTNAHME)
        {
                  EntnahmeTara = akt_tara;
                  current_wieg = 2;
                  return 0;
        }
        arganz = 4;
        args[0] = "Leer";
        args[1] = "23";
        args[2] = waadir;
        args[3] = (char *) ftara;
        fnbexec (arganz, args);
        brutto = netto = tara = (double) 0.0;
        GetGewicht (ergebnis, &brutto, &netto, &tara);
        current_wieg = 2;
        return 0;

/*
        akt_tara = akt_tara + (double) ratod (ftara);
        sprintf (ftara, "%.3lf", akt_tara);
        arganz = 4;
        args[0] = "Leer";
        args[1] = "23";
        args[2] = waadir;
        args[3] = (char *) ftara;
        fnbexec (arganz, args);
        brutto = netto = tara = (double) 0.0;
        GetGewicht (ergebnis, &brutto, &netto, &tara);
        current_wieg = 2;
        sprintf (aufps.tara, "%.3lf", akt_tara);
        display_form (WiegKg, &WiegKgE, 0, 0);
        return 0;
*/
}

static int StornoSet = 0;

int dostorno (void)
/**
SornoFlag setzen.
**/
{
       StornoSet = 1;
       PostMessage (EditForm.mask[0].feldid, WM_KEYDOWN,
                    (WPARAM) VK_RETURN, 0l);
       return 0;
}

BOOL TestAllStorno (void)
/**
Abfragem, ob die ganze Position stormiert werden soll.
**/
{
         double rest;
         double lief_me;

         if (WorkModus == AUSZEICHNEN && AktWiegPos)
         {
                 if (abfragejnG (WiegWindow,
                         "Einzelne Wiegung stornieren", "J"))
                 {
                          AuswahlWieg ();
                          rest    = (double) ratod (auf_me);
                          lief_me = (double) ratod (aufps.lief_me);
                          rest += StornoWieg;
                          lief_me -= StornoWieg;
                          sprintf (auf_me, "%.3lf", rest);
                          sprintf (aufps.lief_me, "%3lf", lief_me);
                          return FALSE;
                 }
         }

         if (abfragejnG (WiegWindow,
                         "Die gesamte Position stornieren", "N"))
         {
             return TRUE;
         }
         int idx = AuswahlWiePro ();
         if (idx == -1)
         {
             StornoSet = 0;
             return FALSE;
         }
         if (idx > wiepanz)
         {
             StornoSet = 0;
             return FALSE;
         }
         memcpy (&wiep, &wieparr[idx], sizeof (wiep));
         rest    = (double) ratod (auf_me);
         rest += ratod (wiep.brutto);
         if (WiegeTyp == ENTNAHME)
         {
                 StartWiegen = TRUE;
                 dowiegen ();
                 StartWiegen = FALSE;
                 sprintf (ActEntnahmeGew, "%.3lf", EntnahmeActGew);
          }
          lief_me = ratod (aufps.lief_me);
          lief_me -= ratod (wiep.brutto);
 	      sprintf (aufps.anz_einh, "%.3lf", atoi (aufps.anz_einh) - 1);
  	      strcpy (wiegzahl, aufps.anz_einh);
/*
          strcpy (auf_me, aufps.auf_me_vgl);
          strcpy (summe1, "0");
          strcpy (summe2, "0");
          strcpy (summe3, "0");
          strcpy (zsumme1, "0");
          strcpy (zsumme2, "0");
          strcpy (zsumme3, "0");
*/
          sprintf (aufps.lief_me, "%3lf", lief_me);
          strcpy (WiegMe, "0.000");
          memcpy (&aufparr [aufpidx], &aufps, sizeof (struct AUFP_S));
          arrtozerl_p (aufpidx);
          ZerlpClass.dbupdate (atoi (EinAusgang));

          lief_me = ratod (wiep.brutto) * -1;
          last_anz = 1;
          write_wewiepro (lief_me, lief_me, (double) 0.0);
          StornoSet = 0;
          commitwork ();
          beginwork ();
          return FALSE;
}

void stornoaction (void)
/**
Storno ausfuehren.
**/
{
        double lief_me;
        double rest;
        double wiegme;

        lief_me = (double) ratod (aufps.lief_me);
        rest    = (double) ratod (auf_me);
        wiegme = ratod (editbuff);
        if (wiegme != (double) 0.0)
        {
            rest += wiegme;
            lief_me -= wiegme;
            sprintf (auf_me, "%.3lf", rest);
        }
        else
        {
            if (TestAllStorno () == FALSE) 
            {
                return;
            }
            rest += lief_me;
            if (WiegeTyp == ENTNAHME)
            {
//                   EntnahmeActGew += lief_me;
                   StartWiegen = TRUE;
                   dowiegen ();
                   StartWiegen = FALSE;
                   sprintf (ActEntnahmeGew, "%.3lf", EntnahmeActGew);
            }
            lief_me = (double) 0;
  		    sprintf (aufps.anz_einh, "%.3lf", 0.0);
		    strcpy (wiegzahl, aufps.anz_einh);
            strcpy (auf_me, aufps.auf_me_vgl);
            strcpy (summe1, "0");
            strcpy (summe2, "0");
            strcpy (summe3, "0");
            strcpy (zsumme1, "0");
            strcpy (zsumme2, "0");
            strcpy (zsumme3, "0");
        }
        sprintf (aufps.lief_me, "%3lf", lief_me);
        strcpy (WiegMe, "0.000");
//        StornoSet = 0;
        memcpy (&aufparr [aufpidx], &aufps, sizeof (struct AUFP_S));
        arrtozerl_p (aufpidx);
        ZerlpClass.dbupdate (atoi (EinAusgang));
        commitwork ();
        beginwork ();
}

int dohand (void)
/**
Wiegen durchfuehren.
**/
{
        double alt;
        double netto;

		DestroyKistBmp ();
        StornoSet = 0;
        SetStorno (dostorno, 0);
		if (atoi(EinAusgang) == EINGANG)  //Zerlegeeingang
		{
	        sprintf (WiegMe, "%.3lf", ratod(aufps.bsd_me));
		}
		else
		{
	        sprintf (WiegMe, "%.3lf", (double) 0);
		}

        EnterCalcBox (WiegWindow,"  Gewicht  ",  WiegMe, 12, WiegKgE.mask[0].picture);
        if (syskey != KEY5)
        {
              if (WiegeTyp == ENTNAHME && StartWiegen)
              {
                     EntnahmeStartGew = ratod (WiegMe);
                     EntnahmeActGew   = EntnahmeStartGew;
                     sprintf (ActEntnahmeGew, "%lf", EntnahmeActGew);
                     display_form (WiegWindow, &fEntnahmeGew, 0, 0);
                     return 0;
              }
              strcpy (aufps.erf_kz, "H");
              if (StornoSet)
              {
				  netto = ratod (aufps.lief_me);
				  netto *= (double) -1.0;
				  last_anz = atol (aufps.anz_einh);
                  stornoaction ();
                  if (StornoSet)
                  {
                         write_wewiepro (netto, netto, (double) 0.0);
                  }
                  StornoSet = 0;
              }
              else
              {
                  netto = (double) ratod (WiegMe);
				  if (netto <= 99999.99)
				  {
                           if (WiegeTyp == ENTNAHME && Entnahmehand)
                           {
                                double gew = EntnahmeActGew - netto;
                                gew -= EntnahmeTara;
                                EntnahmeActGew = netto;
                                sprintf (ActEntnahmeGew, "%.3lf",EntnahmeActGew);
                                netto = gew;
                                sprintf (WiegMe, "%.3lf", netto);
                           }

                           write_wewiepro (netto, netto, (double) 0.0 );

                           alt = (double) ratod (auf_me);
                           alt -= netto;
                           sprintf (auf_me, "%3lf", alt);
                           alt = (double) ratod (aufps.lief_me);
                           alt += netto;
						   if (alt <= 99999.999)
						   {
                                   sprintf (aufps.lief_me, "%3lf", alt);
						   }
				  }
                  memcpy (&aufparr [aufpidx], &aufps, sizeof (struct AUFP_S));
                  arrtozerl_p (aufpidx);
                  ZerlpClass.dbupdate (atoi (EinAusgang));
                  commitwork ();
                  beginwork ();
				  SetEti (netto, netto, 0.0);
              }
              display_form (WiegKg, &WiegKgE, 0, 0);
              display_form (WiegWindow, &artwform, 0, 0);
              display_form (WiegWindow, &artwconst, 0, 0);
        }
        current_wieg = 2;
		if (hand_direct)
		{
               strcpy (aufps.s, "3");
               break_wieg = 1;
		}
        return 0;
}

int GetDefault (char *env, char *wert)
/**
Wert aus bws_default holen.
**/
{
        char *etc;
        int anz;
        char buffer [512];
        FILE *fp;

        etc = getenv ("BWSETC");
        if (etc == (char *) 0)
        {
                    etc = "C:\\USER\\FIT\\ETC";
        }

        sprintf (buffer, "%s\\bws_defa", etc);
        fp = fopen (buffer, "r");
/*
        if (fp == NULL)
        {
                    sprintf (buffer, "%s\\bws_defa", etc);
                    fp = fopen (buffer, "r");
        }
*/
        if (fp == NULL) return -1;

        clipped (env);
        while (fgets (buffer, 511, fp))
        {
                     cr_weg (buffer);
                     anz = split (buffer);
                     if (anz < 2) continue;
                     if (strupcmp (wort[1], env, strlen (env)) == 0)
                     {
                                 strcpy (wert, wort [2]);
                                 fclose (fp);
                                 return 0;
                     }
         }
         fclose (fp);
         return (-1);
}

long GetStdAuszeichner (void)
/**
System-Nummer fuer Touch-Screen holen.
**/
{
        static long auszeichner = 0;
        char defwert [20];

        if (auszeichner) return auszeichner;

        if (GetDefault ("TOUCH-AUSZEICHNER", defwert) == -1)
        {
                             return (long) 1;
        }
        auszeichner = atol (defwert);
        if (GetDefault ("PAZ_DIREKT", defwert) != -1)
        {
                        paz_direkt = atoi (defwert);
        }
        return auszeichner;
}

long GetStdWaage (void)
/**
System-Nummer fuer Touch-Screen holen.
**/
{
        static long waage = 0;
        char defwert [20];

        if (waage) return waage;

        if (GetDefault ("TOUCH-WAAGE", defwert) == -1)
        {
                             return (long) 1;
        }
        waage = atol (defwert);
        return waage;
}

void GetAufArt9999 (void)
/**
System-Nummer fuer Touch-Screen holen.
**/
{
/*
        char defwert [20];
        if (GetDefault ("aufart9999", defwert) == -1)
        {
                             return (long) 1;
        }
        aufart9999 = atol (defwert);
        return waage;
*/
		aufart9999 = 0;
        strcpy (sys_par.sys_par_nam, "aufart9999");
        if (sys_par_class.dbreadfirst () == 0)
        {
                    aufart9999 = atoi (sys_par.sys_par_wrt);
        }
}

static long start_aufme;
static long start_liefme;
static long alarm_me = 0;
static short alarm_ok = 0;
static HWND AlarmWindow = NULL;
static short a_me_einh;
static int me_faktor = 1;
static long akt_me = 0;
static int break_ausz = 0;
int wiegx, wiegy, wiegcx, wiegcy;
int alarmy;
static int GxStop = 0;

static char amenge [80];

mfont AlarmFont  = {"Courier New", 150, 0, 1, BLACKCOL,
                                              LTGRAYCOL,
                                              1,
                                              NULL};

static field _alfield [] =
                 {"A C H T U N G ! ! !", 0, 0, 40, -1, 0, "", DISPLAYONLY, 0, 0, 0,
                  amenge,                0, 0, 70, -1, 0, "", DISPLAYONLY, 0, 0, 0};

static form alform = {2, 0, 0, _alfield, 0, 0, 0, 0, &AlarmFont};

mfont MessFont  = {"Courier New", 150, 0, 1, BLACKCOL,
                                             WHITECOL,
                                             1,
                                             NULL};

mfont MessFontBlue  = {"Courier New", 180, 0, 1, WHITECOL,
                                                 BLUECOL,
                                                 1,
                                                 NULL};

static char msgstring  [80];
static char msgstring1 [80];
static char msgstring2 [80];

static int msgy = 1;
static int oky = 4;
static int okcy = 3;
static int okcx = 8;
static int BreakKey5 (void);
static int BreakKey12 (void);
static int BreakOK (void);

HWND waithWnd;
HWND OldFocus;

static field _msgfield [] =
{
                  msgstring,  0, 0, 1, -1, 0, "", DISPLAYONLY, 0, 0, 0,
                  msgstring1, 0, 0, 2, -1, 0, "", DISPLAYONLY, 0, 0, 0,
                  msgstring2, 0, 0, 3, -1, 0, "", DISPLAYONLY, 0, 0, 0,
                  " OK ",     0, 2, 4, -1, 0, "", BUTTON, 0, 0, KEYCR,
};


static form msgform = {1, 0, 0, _msgfield, 0, 0, 0, 0, &MessFont};

static field _msgfieldjn [] =
{
                  msgstring,  0, 0, 1, -1, 0, "", DISPLAYONLY, 0, 0, 0,
                  msgstring1, 0, 0, 2, -1, 0, "", DISPLAYONLY, 0, 0, 0,
                  msgstring2, 0, 0, 3, -1, 0, "", DISPLAYONLY, 0, 0, 0,
                  "  Ja  ",   0, 2, 4, -1, 0, "", BUTTON, 0, 0,  KEY12,
                  " Nein ",   0, 2, 4, -1, 0, "", BUTTON, 0, 0,  KEY5,
};


static form msgformjn = {5, 0, 0, _msgfieldjn, 0, 0, 0, 0, &MessFont};

static form *aktmessform;


void TestMessage (void)
/**
Window-Meldungen testen.
**/
{
         MSG msg;

         if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE) == 0) return;

         if (msg.message == WM_KEYDOWN && msg.wParam != VK_RETURN) return;

         if (IsWiegMessage (&msg) == 0)
         {
                          TranslateMessage(&msg);
                          DispatchMessage(&msg);
         }
}

void WritePrcomm (char *comm)
/**
Datei Prcomm schreiben.
**/
{
        FILE *fp;
        char *tmp;
        char buffer [512];
        int z;

        tmp = getenv ("TMPPATH");
        if (tmp == NULL)
        {
                  tmp = "C:\\USER\\FIT\\TMP";
        }
        sprintf (buffer, "%s\\prcomm", tmp);
        z = 0;
        fp = fopen (buffer, "w");
        while (fp == NULL)
        {
                    if (z == 1000) break;
                    TestMessage ();
                    if (break_ausz) break;
                    Sleep (10);
                    z ++;
                    fp = fopen (buffer, "w");
        }
        if (fp == NULL) return;
        fprintf (fp, "%s\n", comm);
        fclose (fp);
}

void Alarm (void)
/**
Warnung beim Ueberschreiten der Alarmmenge.
**/
{
        int i;

        if (AlarmWindow) return;

        sprintf (amenge, "Sollmenge fast erreicht");

/*
        AlarmWindow  = CreateWindowEx (
                              0,
                              "AufWiegCtr",
                              "",
                              WS_POPUP | WS_DLGFRAME,
                              wiegx, wiegy - wiegcy - 5,
                              wiegcx, wiegcy,
                              WiegWindow,
                              NULL,
                              hMainInst,
                              NULL);
*/
        AlarmWindow  = CreateWindowEx (
                              0,
                              "AufWiegCtr",
                              "",
                              WS_POPUP | WS_DLGFRAME,
                              wiegx,
//                              wiegy,
                              alarmy,
                              wiegcx, wiegcy,
                              WiegWindow,
                              NULL,
                              hMainInst,
                              NULL);
        if (AlarmWindow == NULL) return;
        ShowWindow (AlarmWindow, SW_SHOWNORMAL);
        UpdateWindow (AlarmWindow);
        for (i = 0; i < 3; i ++)
        {
                    MessageBeep (0xFFFFFFFF);
        }
}


LONG FAR PASCAL MsgProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        switch(msg)
        {

              case WM_PAINT :
                    if (hWnd == MessWindow)
                    {
                            display_form (hWnd, aktmessform, 0, 0);
                    }
                    break;
             case WM_COMMAND :
                    if (LOWORD (wParam) == KEY5 ||
                        LOWORD (wParam) == VK_F5)
                     {
                            syskey = KEY5;
                            SendKey (VK_F5);
                            break_enter ();
                            break;
                    }
                    if (LOWORD (wParam) == KEYCR)
                    {
                            syskey = KEYCR;
                            SendKey (VK_RETURN);
                            break_enter ();
                            break;
                    }
                    if (LOWORD (wParam) == KEY12)
                    {
                            syskey = KEY12;
                            SendKey (VK_F12);
                            break_enter ();
                            break;
                    }
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}


void CreateMessage (HWND hWnd, char *message)
/**
Warnung beim Ueberschreiten der Alarmmenge.
**/
{
        int x, y, cx, cy;
        int i;
        RECT rect;
        TEXTMETRIC tm;

        if (MessWindow) return;

        aktmessform = &msgform;
        for (i = 0; i < msgform.fieldanz; i ++)
        {
            msgform.mask[i].length = 0;
        }

        msgform.fieldanz = 1;
        msgform.mask[0].pos[0] = -1;
        msgform.mask[0].pos[1] = -1;
        msgform.font = &MessFont;
        SetTextMetrics (hWnd, &tm, msgform.font);

        GetClientRect (hWnd, &rect);
        stringcopy (msgstring, message,sizeof(msgstring));

        cx = (strlen (msgstring) + 3) * tm.tmAveCharWidth;
        x = (rect.right - cx) / 2;
        if (x == 0) x = 0;

        cy = tm.tmHeight * 2;
        y = (rect.bottom - cy) / 2 ;
        MessWindow  = CreateWindowEx (
                              0,
                              "AufWiegWhite",
                              "",
                              WS_POPUP | WS_DLGFRAME,
                              x, y,
                              cx, cy,
                              hWnd,
                              NULL,
                              hMainInst,
                              NULL);
        if (MessWindow == NULL) return;
        ShowWindow (MessWindow, SW_SHOWNORMAL);
        UpdateWindow (MessWindow);
}

void DestroyMessage (void)
{
        if (MessWindow == NULL) return;
        CloseControls (&msgform);
        DestroyWindow (MessWindow);
        MessWindow = 0;
}

void DestroyMessageWait (void)
{
        if (MessWindow == NULL) return;
        CloseControls (&msgform);
        DestroyWindow (MessWindow);
        MessWindow = 0;
        EnableWindows (waithWnd, TRUE);
        EnableWindows (hMainWindow, TRUE);
        DestroyMessage ();
        SetFocus (OldFocus);
}

int BreakKey5 ()
{
        syskey = KEY5;
        break_enter ();
        return 0;
}

int BreakKey12 ()
{
        syskey = KEY12;
        break_enter ();
        return 0;
}

int BreakOK ()
{
        break_enter ();
        return 0;
}


int CreateMessageJN (HWND hWnd, char *message, char *def)
/**
Warnung beim Ueberschreiten der Alarmmenge.
**/
{
        int x, y, cx, cy;
        RECT rect;
        TEXTMETRIC tm;
        char *ltext;
        int anz;
        int i;
        int okpos;
        unsigned int len, pos;
        HWND OldFocus;

        if (MessWindow) return 0;

        aktmessform = &msgformjn;
        OldFocus = GetFocus ();
        anz = wsplit (message, "\n");
        if (anz == 0) return 0;

        for (i = 0; i < msgformjn.fieldanz; i ++)
        {
            msgformjn.mask[i].length = 0;
        }

        msgformjn.font = &MessFontBlue;
        SetTextMetrics (hWnd, &tm, msgformjn.font);

//        msgformjn.mask[1].attribut = REMOVED;
//        msgformjn.mask[2].attribut = REMOVED;
		stringcopy (msgstring, "");
		stringcopy (msgstring1, "");
		stringcopy (msgstring2, "");
          stringcopy (msgstring, wort[0],sizeof(msgstring));
        len = strlen (msgstring);
        ltext = msgstring;
        okpos = oky;
        if (anz > 1)
        {
            stringcopy (msgstring1, wort[1],sizeof(msgstring1));
            if (strlen (msgstring1) > len)
            {
                 len = strlen (msgstring1);
                 ltext = msgstring1;
            }
            msgformjn.mask[1].attribut = DISPLAYONLY;
            okpos += 1;
        }
        if (anz > 2)
        {
            stringcopy (msgstring2, wort[2],sizeof(msgstring2));
            if (strlen (msgstring2) > len)
            {
                 len = strlen (msgstring2);
                 ltext = msgstring2;
            }
            msgformjn.mask[2].attribut = DISPLAYONLY;
            okpos += 1;
        }

        msgform.fieldanz = 5;

        GetClientRect (hMainWindow, &rect);

        cx = (len + 3) * tm.tmAveCharWidth;

        x = (rect.right - cx) / 2;
        if (x == 0) x = 0;

        cy = tm.tmHeight * 4;
        y = (rect.bottom - cy) / 2 ;

        msgformjn.mask[0].pos[0] = tm.tmHeight * msgy;
        msgformjn.mask[1].pos[0] = (int) (double) (tm.tmHeight * (msgy + 1 * 1.5));
        msgformjn.mask[2].pos[0] = (int) (double) (tm.tmHeight * (msgy + 2 * 1.5));
        msgformjn.mask[msgformjn.fieldanz - 2].pos[0] =
                                 (int) (double) (tm.tmHeight * okpos);
        msgformjn.mask[msgformjn.fieldanz - 1].pos[0] =
                                 (int) (double) (tm.tmHeight * okpos);

        msgformjn.mask[msgformjn.fieldanz - 2].rows   = tm.tmHeight * okcy;
        msgformjn.mask[msgformjn.fieldanz - 2].length = tm.tmAveCharWidth * okcx;
        msgformjn.mask[msgformjn.fieldanz - 1].rows   = tm.tmHeight * okcy;
        msgformjn.mask[msgformjn.fieldanz - 1].length = tm.tmAveCharWidth * okcx;
        stringcopy (msgformjn.mask[msgformjn.fieldanz - 2].feld, "  Ja  ");
        stringcopy (msgformjn.mask[msgformjn.fieldanz - 1].feld, " Nein ");

        pos = (len + 3 - 16) / 2;
        if (pos <= 0) pos = 0;

        msgformjn.mask[msgformjn.fieldanz - 2].pos[1] = pos * tm.tmAveCharWidth;
        msgformjn.mask[msgformjn.fieldanz - 1].pos[1] = (pos + 9)
                                                     * tm.tmAveCharWidth;

        cy =  msgformjn.mask[msgformjn.fieldanz - 1].pos[0] +
              msgformjn.mask[msgformjn.fieldanz - 1].rows +
              tm.tmHeight;
        y = (rect.bottom - cy) / 2 ;
        EnableWindows (hWnd, FALSE);
        EnableWindow (hMainWindow, FALSE);
        MessWindow  = CreateWindowEx (
                              0,
                              "AufWiegBlue",
                              "Meldung",
                              WS_POPUP | WS_DLGFRAME | WS_VISIBLE,
                              x, y,
                              cx, cy,
                              hWnd,
//                              NULL,
                              NULL,
                              hMainInst,
                              NULL);
        if (MessWindow == NULL)
        {
            EnableWindow (hMainWindow, TRUE);
            EnableWindows (hWnd, TRUE);
            return 0;
        }
        ShowWindow (MessWindow, SW_SHOWNORMAL);
        SetWindowPos (MessWindow, HWND_TOPMOST, 0, 0, 0, 0,
                      SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
        UpdateWindow (MessWindow);
        save_fkt (5);
        save_fkt (12);
        set_fkt (BreakKey5, 5);
        set_fkt (BreakKey12, 12);
        no_break_end ();
        SetButtonTab (TRUE);
        enter_form (MessWindow, &msgformjn, 0, 0);
        EnableWindow (hMainWindow, TRUE);
        EnableWindows (hWnd, TRUE);
        SetButtonTab (FALSE);
        DestroyMessage ();
        SetFocus (OldFocus);
        restore_fkt (5);
        restore_fkt (12);
        if (syskey == KEY5) return 0;
        return 1;
}

void CreateMessageWait (HWND hWnd, char *message)
/**
Warnung beim Ueberschreiten der Alarmmenge.
**/
{
        int x, y, cx, cy;
        RECT rect;
        TEXTMETRIC tm;
        char *ltext;
        int anz;
        int i;
        unsigned int len;

        if (MessWindow) return;

		waithWnd = hWnd;
        aktmessform = &msgform;
        OldFocus = GetFocus ();
        anz = wsplit (message, "\n");
        if (anz == 0) return;

        for (i = 0; i < msgform.fieldanz; i ++)
        {
            msgform.mask[i].length = 0;
        }

        msgform.font = &MessFontBlue;
        SetTextMetrics (hWnd, &tm, msgform.font);

		stringcopy (msgstring, "");
		stringcopy (msgstring1, "");
		stringcopy (msgstring2, "");
        stringcopy (msgstring, wort[0],sizeof(msgstring));
        len = strlen (msgstring);
        ltext = msgstring;
        msgform.fieldanz = 1;
        if (anz > 1)
        {
            stringcopy (msgstring1, wort[1],sizeof(msgstring1));
            if (strlen (msgstring1) > len)
            {
                 len = strlen (msgstring1);
                 ltext = msgstring1;
            }
            msgform.mask[1].attribut = DISPLAYONLY;
			msgform.fieldanz ++;
        }
        if (anz > 2)
        {
            stringcopy (msgstring2, wort[2],sizeof(msgstring2));
            if (strlen (msgstring2) > len)
            {
                 len = strlen (msgstring2);
                 ltext = msgstring2;
            }
            msgform.mask[2].attribut = DISPLAYONLY;
  			msgform.fieldanz ++;
        }


        GetClientRect (hMainWindow, &rect);

        cx = (len + 3) * tm.tmAveCharWidth;

        x = (rect.right - cx) / 2;
        if (x == 0) x = 0;

        cy = tm.tmHeight * msgform.fieldanz;
        y = (rect.bottom - cy) / 2 ;

        msgform.mask[0].pos[0] = tm.tmHeight * msgy;
        msgform.mask[1].pos[0] = (int) (double) (tm.tmHeight * (msgy + 1 * 1.5));
        msgform.mask[2].pos[0] = (int) (double) (tm.tmHeight * (msgy + 2 * 1.5));

        cy =  msgform.mask[msgform.fieldanz - 1].pos[0] +
              2 * tm.tmHeight;

        y = (rect.bottom - cy) / 2 ;
        MessWindow  = CreateWindowEx (
//                              WS_EX_TOPMOST,
                               0,
                              "AufWiegBlue",
                              "Meldung",
                              WS_POPUP | WS_DLGFRAME,
                              x, y,
                              cx, cy,
                              NULL,
                              NULL,
                              hMainInst,
                              NULL);
        if (MessWindow == NULL) return;
        ShowWindow (MessWindow, SW_SHOWNORMAL);
        SetWindowPos (MessWindow, HWND_TOPMOST, 0, 0, 0, 0,
                   SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
        UpdateWindow (MessWindow);
        display_form (MessWindow, &msgform, 0, 0);
        set_fkt (BreakOK, 12);
        EnableWindows (hMainWindow, FALSE);
        EnableWindows (hWnd, FALSE);
}


void CreateMessageOK (HWND hWnd, char *message)
/**
Warnung beim Ueberschreiten der Alarmmenge.
**/
{
        int x, y, cx, cy;
        RECT rect;
        TEXTMETRIC tm;
        char *ltext;
        int anz;
        int i;
        int okpos;
        unsigned int len;
        HWND OldFocus;

        if (MessWindow) return;

        aktmessform = &msgform;
        OldFocus = GetFocus ();
        anz = wsplit (message, "\n");
        if (anz == 0) return;

        for (i = 0; i < msgform.fieldanz; i ++)
        {
            msgform.mask[i].length = 0;
        }

        msgform.font = &MessFontBlue;
        SetTextMetrics (hWnd, &tm, msgform.font);

//        msgform.mask[1].attribut = REMOVED;
//        msgform.mask[2].attribut = REMOVED;
		stringcopy (msgstring, "");
		stringcopy (msgstring1, "");
		stringcopy (msgstring2, "");
        stringcopy (msgstring, wort[0],sizeof(msgstring));
        len = strlen (msgstring);
        ltext = msgstring;
        okpos = oky;
        if (anz > 1)
        {
            stringcopy (msgstring1, wort[1],sizeof(msgstring1));
            if (strlen (msgstring1) > len)
            {
                 len = strlen (msgstring1);
                 ltext = msgstring1;
            }
            msgform.mask[1].attribut = DISPLAYONLY;
            okpos += 1;
        }
        if (anz > 2)
        {
            stringcopy (msgstring2, wort[2],sizeof(msgstring2));
            if (strlen (msgstring2) > len)
            {
                 len = strlen (msgstring2);
                 ltext = msgstring2;
            }
            msgform.mask[2].attribut = DISPLAYONLY;
            okpos += 1;
        }

        msgform.fieldanz = 4;

        GetClientRect (hMainWindow, &rect);

        cx = (len + 3) * tm.tmAveCharWidth;

        x = (rect.right - cx) / 2;
        if (x == 0) x = 0;

        cy = tm.tmHeight * 4;
        y = (rect.bottom - cy) / 2 ;

        msgform.mask[0].pos[0] = tm.tmHeight * msgy;
        msgform.mask[1].pos[0] = (int) (double) (tm.tmHeight * (msgy + 1 * 1.5));
        msgform.mask[2].pos[0] = (int) (double) (tm.tmHeight * (msgy + 2 * 1.5));
        msgform.mask[msgform.fieldanz - 1].pos[0] =
                                 (int) (double) (tm.tmHeight * okpos);

        msgform.mask[msgform.fieldanz - 1].rows   = tm.tmHeight * okcy;
        msgform.mask[msgform.fieldanz - 1].length = tm.tmAveCharWidth * okcx;
        stringcopy (msgform.mask[msgform.fieldanz - 1].feld, " OK ");

        cy =  msgform.mask[msgform.fieldanz - 1].pos[0] +
              msgform.mask[msgform.fieldanz - 1].rows +
              tm.tmHeight;
        y = (rect.bottom - cy) / 2 ;
        MessWindow  = CreateWindowEx (
//                              WS_EX_TOPMOST,
                               0,
                              "AufWiegBlue",
                              "Meldung",
                              WS_POPUP | WS_DLGFRAME,
                              x, y,
                              cx, cy,
                              NULL,
                              NULL,
                              hMainInst,
                              NULL);
        if (MessWindow == NULL) return;
        ShowWindow (MessWindow, SW_SHOWNORMAL);
        SetWindowPos (MessWindow, HWND_TOPMOST, 0, 0, 0, 0,
                   SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
        UpdateWindow (MessWindow);
        display_form (MessWindow, &msgform, 0, 0);
        set_fkt (BreakOK, 12);
        EnableWindows (hMainWindow, FALSE);
        EnableWindows (hWnd, FALSE);
        SetButtonTab (TRUE);
        enter_form (MessWindow, &msgform, 0, 0);
        SetButtonTab (FALSE);
        EnableWindows (hWnd, TRUE);
        EnableWindows (hMainWindow, TRUE);
        DestroyMessage ();
        SetFocus (OldFocus);
}


void disp_messG (char *text, int modus)
{
       if (AktivWindow == NULL)
       {
                 AktivWindow = GetActiveWindow ();
       }
        if (modus == 2)
        {
                    MessageBeep (0xFFFFFFFF);
        }
        CreateMessageOK (AktivWindow, text);
}

static char messbuffer [1024];

int print_messG (int mode, char * format, ...)
/**
Ausgabe in dbfile.
**/
{
        va_list args;

        va_start (args, format);
        vsprintf (messbuffer, format, args);
        va_end (args);
        disp_messG (messbuffer, mode);
        return (0);
}


int abfragejnG (HWND hWnd, char *text, char *def)
/**
Ausgabe in dbfile.
**/
{
        return CreateMessageJN (hWnd, text, def);
}


void WaitStat (char *stat)
/**
Ergebnis-Datei vom Preisauszeichner lesen. Auf stat warten
**/
{
        FILE *fp;
        char *tmp;
        char buffer [512];
        char satz [512];
        int z;

        tmp = getenv ("TMPPATH");
        if (tmp == NULL)
        {
                  tmp = "C:\\USER\\FIT\\TMP";
        }
        sprintf (buffer, "%s\\prstat", tmp);

        z = 0;
        while (TRUE)
        {
                  if (z == 30000) break;
                  fp = fopen (buffer, "r");
                  if (fp == NULL)
                  {
                              z ++;
                              Sleep (10);
                              continue;
                  }
                  if (fgets (satz, 511, fp) == 0)
                  {
                              z ++;
                              fclose (fp);
                              Sleep (10);
                              continue;
                  }
                  fclose (fp);
                  cr_weg (satz);
                  split (satz);
                  if (strcmp (wort[1], stat) == 0) break;
                  z ++;
                  TestMessage ();
                  if (break_ausz) break;
                  Sleep (10);
         }
}


static long sup;
static long sup0;
static long sup1;
static short kunme_einh = 0;
static long  kunmez = 0l;
static long  kunme = 0l;
static double akt_zsu1 = (double) 0;
static double akt_zsu2 = (double) 0;
static char rec [256] = {" "};


void TestSu (double zsu1, double zsu2, double zsu3)
{
         sup = (long) zsu2;
         sup0 = (long) zsu3;
}


int AuszKomplett (void)
/**
Test, ob die Auftragsmenge erreicht ist.
**/
{
        long auf_me_kun;

        auf_me_kun = atol (aufps.auf_me);
        switch (kunme_einh)
        {
            case 2 :
                 if (ratod (auf_me) <= (double) 0.0)
                 {
                         return 1;
                 }
                 return 0;
            case 1 :
                 if (kunme > auf_me_kun)
                 {
                         return 1;
                 }
                 return 0;
            case 8 :
                 if (sup1 > auf_me_kun)
                 {
                         return 1;
                 }
                 return 0;
            case 16 :
                 if (sup0 > auf_me_kun)
                 {
                         return 1;
                 }
                 return 0;
        }
        return 0;
}


void GetKunMe ()
/**
Auftragsmenge in Kundenbestelleinheit
**/
{
         int me_einh_kun;

         me_einh_kun = atoi (aufps.me_einh_kun);
         switch (me_einh_kun)
         {
             case 2 :
                   kunme_einh = 2;
                   break;
             case 6 :
             case 7 :
                   kunme_einh = 8;
                   break;
             case 10 :
                   kunme_einh = 16;
                   break;
             default :
                   kunme_einh = 1;
                   break;
         }
}

void FillSummen (double su1, double su2,
                 double zsu1, double zsu2)
/**
Aktuellen Inhalte der einzelnen Summen in Abhaengigkeit
von der Einheit fuellen.
**/
{

         switch (a_kun_geb.geb_fill)
         {
               case 2 :
                    sprintf (summe1, "%.3lf", su1 / me_faktor);
                    break;
               case 1:
                    sprintf (summe1, "%.0lf", zsu1);
                    break;
               default :
                    sprintf (summe1, "%.3lf", su1 / me_faktor);
                    break;
         }

         switch (a_kun_geb.pal_fill)
         {
               case 2 :
                    sprintf (summe2, "%.3lf", su2 / me_faktor);
                    break;
               case 1:
                    sprintf (summe2, "%.0lf", zsu2);
                    break;
               case 5:
                    sprintf (summe2, "%ld", sup);
                    break;
               default :
                    sprintf (summe2, "%.3lf", su2 / me_faktor);
                    break;
         }
}

void FillSumme3 (double su3, double zsu3)
/**
Aktuellen Inhalte von Summe3 in Abhaengigkeit
von der Einheit fuellen.
**/
{

        sprintf (summe3, "%ld", sup0);
}

int KorrMe (char *satz)
/**
Auftragsmenge korrigieren.
**/
{
         int anz;
         long me;
         long zielme;
         double me_vgl;
         double su1;
         double su2;
         double zsu1;
         double zsu2;
         double zsu3;

         if (atoi (aufps.a_me_einh) == 2)
         {
                    me_vgl = ratod (aufps.auf_me_vgl) * 1000;
         }
         else
         {
                    me_vgl = ratod (aufps.auf_me_vgl);
         }

         cr_weg (satz);
         anz = split (satz);
         if (anz < 9)
         {
                   return 1;
         }

         if (wort[1][0] == 'E')
         {
                   return 0;
         }

         if (GxStop) return 1;


		 if (strcmp (rec," ") ==  0) strcpy (rec,satz);
		 if (strcmp (satz, rec) == 0) return 1;

		 strcpy (rec, satz);
         me   = atol (wort[9]);

         zsu1 = ratod (wort[4]);
         su1  = ratod (wort[5]);
         zsu2 = ratod (wort[6]);
         su2  = ratod (wort[7]);
         zsu3 = ratod (wort[8]);

         TestSu (zsu1, zsu2, zsu3);
         FillSummen (su1, su2, zsu1, zsu2);

         zsu2 = (double) sup;


         sprintf (zsumme1, "%.0lf", zsu1);
		 if (a_kun_geb.pal_fill == 5 || a_kun_geb.pal_fill == 8)
		 {
                 sprintf (zsumme2, "%.0lf", zsu2);
		 }
		 else if (kunme_einh == 8)
		 {
                 sprintf (zsumme2, "%.0lf", zsu3);
		 }
         if (!alarm_ok && me > 0l)
         {
                     alarm_me = (long) ((double) me_vgl - 3 * me);
                     alarm_ok = 1;
         }

         kunme ++;
         zielme = start_aufme - me;
         sprintf (auf_me, "%.3lf", (double) ((double) zielme / me_faktor));

         zielme = start_liefme + me;
         sprintf (aufps.lief_me, "%.3lf", (double) ((double) zielme / me_faktor));
         FillSumme3 ((double) zielme, zsu3);
         zsu3 = (double) sup0;
		 if (kunme_einh == 16)
		 {
                   sprintf (zsumme3, "%.0lf", zsu3);
		 }
         AktPrWieg[AktWiegPos] = ((double) (me - akt_me)) / me_faktor;
         sprintf (WiegMe, "%.3lf", (double) ((double) (me - akt_me) / me_faktor));


         display_field (WiegWindow, &artwform.mask[1], 0, 0);
         display_field (WiegWindow, &artwform.mask[2], 0, 0);

         display_field (WiegWindow, &artwform.mask[3], 0, 0);
         display_field (WiegWindow, &artwform.mask[4], 0, 0);
         display_field (WiegWindow, &artwform.mask[5], 0, 0);
         display_field (WiegWindow, &artwform.mask[6], 0, 0);
         display_field (WiegWindow, &artwform.mask[7], 0, 0);
         display_field (WiegWindow, &artwform.mask[8], 0, 0);
         display_field (WiegWindow, &artwform.mask[9], 0, 0);
         display_field (WiegWindow, &artwform.mask[10], 0, 0);



         display_field (WiegKg,     &WiegKgE.mask[0], 0, 0);

         memcpy (&aufparr [aufpidx], &aufps, sizeof (struct AUFP_S));
         arrtozerl_p (aufpidx);
         ZerlpClass.dbupdate (atoi (EinAusgang));
         commitwork ();
         beginwork ();

         SetFocus (WieButton.mask[3].feldid);
         if (AktWiegPos < 998) AktWiegPos ++;
         akt_me = (long) me;

         return 1;
}

void PollAusz (void)
/**
Ergebnis-Datei vom Preisauszeichner lesen.
**/
{
        FILE *fp;
        char *tmp;
        char buffer [512];
        char satz [512];
        int pollanz;

        a_me_einh = atoi (aufps.a_me_einh);
        alarm_me = 0l;
        alarm_ok = 0;
        kunme = 0;
        kunmez = 0l;
        GetKunMe ();
        sup = (long) 0;
        sup0 = 0l;
        sup1 = 0l;
		akt_zsu1 = (double) 0.0;
		akt_zsu2 = (double) 0.0;
		strcpy (rec, " ");
        if (a_me_einh == 2)
        {
                      me_faktor = 1000;
        }
        else
        {
                      me_faktor = 1;
        }

        start_aufme  =  (long) ((double) ratod (auf_me) * me_faktor);
        start_liefme =  (long) ((double) ratod (aufps.lief_me) * me_faktor);

        tmp = getenv ("TMPPATH");
        if (tmp == NULL)
        {
                  tmp = "C:\\USER\\FIT\\TMP";
        }
        sprintf (buffer, "%s\\prstat", tmp);

        GxStop = pollanz = 0;
        while (TRUE)
        {
                  TestMessage ();
                  if (GxStop) pollanz ++;
                  if (pollanz >= 40) break;
                  fp = fopen (buffer, "r");
                  if (fp == NULL)
                  {
                              TestMessage ();
                              Sleep (50);
                              continue;
                  }
                  if (fgets (satz, 511, fp) == 0)
                  {
                              TestMessage ();
                              fclose (fp);
                              Sleep (50);
                              continue;
                  }
                  fclose (fp);
                  if (KorrMe (satz) == 0)
                  {
                              break;
                  }
                  TestMessage ();
                  SetFocus (WieButton.mask[3].feldid);
                  Sleep (50);
         }
}

void starte_prakt (void)
/**
Prgramm prakt starten.
**/
{
        char *tmp;
        char *bws;
        char buffer [512];

        tmp = getenv ("TMPPATH");
        if (tmp == NULL)
        {
                  tmp = "C:\\USER\\FIT\\TMP";
        }
        sprintf (buffer, "%s\\prstat", tmp);
        unlink (buffer);

        bws = getenv ("BWS");
        if (bws == NULL)
        {
                  bws = "C:\\USER\\FIT";
        }
        sprintf (buffer, "%s\\bin\\gxakt", bws);

        if (ProcExec (buffer, SW_SHOWMINNOACTIVE, -1, 0, -1, 0) == 0)
        {
                       print_messG (2, "Fehler beim Start von %s", buffer);
//                       break_ausz = 1;
        }
}

void SetAuszText (int mode)
/**
Text fuer AuszeicnenButton setzen.
**/
{
         if (mode == 0)
         {
               WieWie.text1 = "Aus-";
               WieWie.text2 = "zeichnen";
               WieWie.tx1 = -1;
               WieWie.ty1 = 25;
               WieWie.tx2 = -1;
               WieWie.ty2 = 50;
         }
         else
         {
               WieWie.text1 = "Stop";
               WieWie.text2 = "";
               WieWie.tx1 = -1;
               WieWie.ty1 = -1;
               WieWie.tx2 = -1;
               WieWie.ty2 = -1;
          }
          WieWie.aktivate = 2;
          ActivateColButton (WiegCtr, &WieButton, 3, 2, 1);
          SetFocus (WieButton.mask[3].feldid);
}

void BuInactiv (void)
/**
Alle Button auf Inaktiv setzen.
**/
{
          ActivateColButton (WiegCtr, &WieButton, 0, -1, 1);
          ActivateColButton (WiegCtr, &WieButton, 1, -1, 1);
          ActivateColButton (WiegCtr, &WieButton, 2, -1, 1);
          ActivateColButton (WiegCtr, &WieButton, 3, -1, 1);
          ActivateColButton (WiegCtr, &WieButton, 4, -1, 1);
          ActivateColButton (WiegCtr, &WieButton, 5, -1, 1);
          ActivateColButton (WiegCtr, &WieButton, 6, -1, 1);
}

void BuActiv (void)
/**
Alle Button auf Inaktiv setzen.
**/
{
          ActivateColButton (WiegCtr, &WieButton, 0, 2, 1);
          ActivateColButton (WiegCtr, &WieButton, 1, 2, 1);
          ActivateColButton (WiegCtr, &WieButton, 2, 2, 1);
          ActivateColButton (WiegCtr, &WieButton, 3, 2, 1);
          ActivateColButton (WiegCtr, &WieButton, 4, 2, 1);
          ActivateColButton (WiegCtr, &WieButton, 5, 2, 1);
          ActivateColButton (WiegCtr, &WieButton, 6, 2, 1);
}

static void FileError (void)
{

          disp_messG ("Es wurden keine Daten zum Auszeichnen gefunden\n"
                     "Pr�fen Sie bitte den Verkaufspreis\n und die Hauptwarengruppenzuordnung",
                     2);
          return;
}


BOOL KunGxOk (void)
/**
Pruefen, ob die Dateien fuer gxakt leer sind.
**/
{
        char *tmp;
        char dname [256];
        HANDLE fp;
        DWORD fsize;

        tmp = getenv ("TMPPATH");
        if (tmp == (char *) 0)
        {
                        tmp = "C:\\USER\\FIT\\TMP";
        }
        sprintf (dname, "%s\\kungx", tmp);
        fp = CreateFile (dname, GENERIC_READ, 0, NULL,
                         OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
        if (fp == INVALID_HANDLE_VALUE)
        {
                         FileError ();
                         return FALSE;
        }

        fsize = GetFileSize (fp, NULL);
        if (fsize == (DWORD) 0 ||
            fsize == 0xFFFFFFFF)
        {
                         CloseHandle (fp);
                         FileError ();
                         return FALSE;
        }

        CloseHandle (fp);
        return TRUE;
}



int doauszeichnen (void)
/**
Auftragsposition auszeichnen.
**/
{
        char buffer [512];
        char *tmp;
        double auf_me_vgl;
        double auf_me_kun;
        DWORD ex;

        akt_me = 0;
        break_ausz = 0;
        tmp = getenv ("TMPPATH");
        if (tmp == (char *) 0)
        {
                        tmp = "C:\\USER\\FIT\\TMP";
        }

        GetKunMe ();
        if (atoi (aufps.me_einh_kun) == 2)
        {
                    auf_me_kun = ratod (aufps.auf_me) * 1000;
        }
        else
        {
                    auf_me_kun = ratod (aufps.auf_me);
        }
        if (auf_me_kun <= (double) 0.0)
        {

                       disp_messG ("Menge 0 kann nicht ausgezeichnet werden", 2);
                       return (0);
        }
        if (atoi (aufps.a_me_einh) == 2)
        {
                    auf_me_vgl = ratod (aufps.auf_me_vgl) * 1000;
        }
/*
        if (auf_me_vgl <= (double) 0.0)
        {

                       disp_messG ("Menge 0 kann nicht ausgezeichnet werden", 2);
                       return (0);
        }
*/

        if (ratod (auf_me) <= (double) 0.0)
        {

                       disp_messG ("Die Sollmenge ist erreicht", 2);
                       return (0);
        }
        if (paz_direkt == 3)
        {
/*
                      RunBwsasc (auszeichner, auf_me_vgl);
                      BuInactiv ();
*/
        }
        else if (paz_direkt == 2)
        {

/*
                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungx kungx.if %ld %ld %s %.0lf %s",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr), lieferdatum, auf_me_vgl,
                          aufps.auf_lad_pr);
*/
                      sprintf (buffer, "pr_paz -dokt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungx kungx.if %ld %ld %s %.0lf %s %hd",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr), lieferdatum, auf_me_kun,
                          aufps.auf_lad_pr, kunme_einh);

                      BuInactiv ();
                      ex = ProcWaitExec (buffer, SW_SHOWNORMAL,
                                                 -1, 0, -1, 0);
                      if (ex != 0l)
                      {
                          print_messG (2,
                              "Fehler beim 1. Start von pr_paz");
                          BuActiv ();
                          return (-1);
                      }

                      sprintf (buffer, "pr_paz -odkt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungtex kungtex.if %ld %ld",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr));

                      ex = ProcWaitExec (buffer, SW_SHOWNORMAL,
                                                 -1, 0, -1, 0);
                      if (ex != 0l)
                      {
                          print_messG (2,
                              "Fehler beim 2. Start von pr_paz");
                          BuActiv ();
                          return (-1);
                      }
       }
        else if (paz_direkt == 1)
        {
/*
                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungx kungx.if %ld %ld %s %.0lf %s",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr), lieferdatum, auf_me_vgl,
                          aufps.auf_lad_pr);
*/
                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungx kungx.if %ld %ld %s %.0lf %s %hd",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr), lieferdatum, auf_me_kun,
                          aufps.auf_lad_pr, kunme_einh);


                      BuInactiv ();
                      CreateBatch (buffer);

                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungtex kungtex.if %ld %ld",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr));

                     RunBatch (buffer);
        }
        else
        {
/*
                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungx kungx.if %ld %ld %s %.0lf %s",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr), lieferdatum, auf_me_vgl,
                          aufps.auf_lad_pr);
*/
                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungx kungx.if %ld %ld %s %.0lf %s %hd",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr), lieferdatum, auf_me_kun,
                          aufps.auf_lad_pr, kunme_einh );

                      BuInactiv ();
                      CreateMessage (WiegWindow, "Die Daten werden vorbereitet");
                      ex = ProcWaitExec (buffer, SW_HIDE,
                                                 -1, 0, -1, 0);
                      if (ex == (long) -1)
                      {
                          DestroyMessage ();
                          AktivWindow = WiegWindow;
                          print_messG (2,
                              "Fehler beim 1. Start von pr_paz");
                          BuActiv ();
                          return (-1);
                      }
                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungtex kungtex.if %ld %ld",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr));

                      ex = ProcWaitExec (buffer, SW_HIDE,
                                                 -1, 0, -1, 0);
                      if (ex == (long) -1)
                      {
                          DestroyMessage ();
                          AktivWindow = WiegWindow;
                          print_messG (2,
                              "Fehler beim 2. Start von pr_paz");
                          BuActiv ();
                          return (-1);
                      }
                      DestroyMessage ();
                      AktivWindow = WiegWindow;
       }
       if (! KunGxOk ())
       {
                    BuActiv ();
                    WorkModus = AUSZEICHNEN;
                    return (-1);
        }

        WorkModus = STOP;
        WritePrcomm ("G");
        if (! break_ausz)
        {
                   SetAuszText (1);
                   starte_prakt ();
                   SetActiveWindow (WiegCtr);
        }
        if (! break_ausz)
        {
                   PollAusz ();
        }
        WritePrcomm ("E");
        SetAuszText (0);
        BuActiv ();
        WorkModus = AUSZEICHNEN;
        if (AlarmWindow)
        {
                     CloseControls (&alform);
                     DestroyWindow (AlarmWindow);
                     AlarmWindow = NULL;
        }
        return 0;
}

int StopAuszeichnen (void)
{

        GxStop = 1;
        if (abfragejnG (WiegWindow, "Auszeichnen stoppen ?", "N") == 0)
        {
            GxStop = 0;
            return 0;
        }
        WritePrcomm ("E");
        GxStop = 1;
        return 0;
}

void IncWiegZahl (void)
/**
Wieganzahl erhoehen.
**/
{
	    sprintf (wiegzahl, "%ld", atol (wiegzahl) + 1);
}

void PaintKist (HDC hdc)
/**
Kiste zeichnen.
**/
{
	    if (KistPaint == FALSE) return;

        KistMask->DrawBitmapEx (hdc, KistRect.left, KistRect.top, SRCAND);
		if (KistCol)
		{
                 KistG->DrawBitmapEx (hdc, KistRect.left, KistRect.top, SRCPAINT);
		}
		else
		{
                 Kist->DrawBitmapEx (hdc, KistRect.left, KistRect.top, SRCPAINT);
		}
}

void PaintKistBmp (void)
{
	    if (KistPaint == TRUE) return;
		if (KistCol)
		{
			KistCol = FALSE;
			SetClassLong (WiegKg, GCL_HBRBACKGROUND, (long) LtBlueBrush);
		}
		else
		{
			KistCol = TRUE;
			SetClassLong (WiegKg, GCL_HBRBACKGROUND, (long) YellowBrush);
		}
	    KistPaint = TRUE;
		MessageBeep (MB_ICONASTERISK);
		InvalidateRect (WiegWindow, &KistRect, TRUE);
		InvalidateRect (WiegKg, NULL, TRUE);
		UpdateWindow (WiegWindow);
		UpdateWindow (WiegKg);
}

void DestroyKistBmp (void)
{
	    if (KistPaint == FALSE) return;
	    KistPaint = FALSE;
		InvalidateRect (WiegWindow, &KistRect, TRUE);
		UpdateWindow (WiegWindow);
}


void SetKistRect (int x, int y)
/**
Position und Groesse fuer WQiegBitmap setzen.
**/
{
	    POINT bpoint;
		HDC hdc;

		hdc = GetDC (WiegWindow);
		Kist->BitmapSize (hdc, &bpoint);
		ReleaseDC (WiegWindow, hdc);
		KistRect.left = 50;
		KistRect.top  = y;
		KistRect.right   = KistRect.left + bpoint.x;
		KistRect.bottom  = KistRect.top  + bpoint.y;
}




int dowiegen (void)
/**
Wiegen durchfuehren.
**/
{
        double netto;
        double brutto;
        double tara;
        double alt;
        static int arganz;
        static char *args[4];
		static char WiegAnz[20];

        if (WorkModus == AUSZEICHNEN)
        {
                       return (doauszeichnen ());
        }
        else if (WorkModus == STOP)
        {
                       return (StopAuszeichnen ());
        }
		DestroyKistBmp ();

        arganz = 3;
        args[0] = "Leer";
        args[1] = "11";
        args[2] = waadir;
        fnbexec (arganz, args);
/* Test  */

/*
		Sleep (2000);
*/

/* Testende  */

        brutto = netto = tara = (double) 0.0;
        GetGewicht (ergebnis, &brutto, &netto, &tara);
/*
		if (aufps.hnd_gew[0] == 'H' || aufps.erf_gew_kz[0] == 'N')
		{
                SetStorno (dostorno, 0);
                sprintf (WiegAnz, "%.0lf", (double) 1);
                EnterCalcBox (WiegWindow,"  Anzahl  ",  WiegAnz, 12, "%8.3lf");
                if (syskey != KEY5)
				{
					SetEtiStk (ratod (WiegAnz));
				}
				else
				{
                    SetEtiStk ((double) 1.0);
				}
		}
		else
		{
                SetEtiStk ((double) 1.0);
		}
*/

        if (WiegeTyp == ENTNAHME && StartWiegen)
        {
                   EntnahmeStartGew = brutto;
                   EntnahmeActGew   = brutto;
                   sprintf (ActEntnahmeGew, "%lf", EntnahmeActGew);
                   display_form (WiegWindow, &fEntnahmeGew, 0, 0);
                   return 0;
        }

		if (netto <= 99999.999)
		{
                SetEti (netto, brutto, tara);
		}
        if (tara == (double) 0.0)
        {
                   strcpy (aufps.erf_kz, "W");
        }
        else
        {
                   strcpy (aufps.erf_kz, "E");
        }
		if (netto <= 99999.999)
		{
                  alt = (double) ratod (auf_me);
                  alt -= netto;
                  sprintf (WiegMe, "%11.3lf", netto);
                  sprintf (auf_me, "%3lf", alt);
                  alt = (double) ratod (aufps.lief_me);
                  alt += netto;
				  if (alt <= 99999.999)
				  {
                           sprintf (aufps.lief_me, "%3lf", alt);
				  }
		}
		if (tara <= 99999.999)
		{
                  sprintf (aufps.tara, "%.3lf", tara);
		}
        if (WiegeTyp == STANDARD && autoan) 
        {
            akt_tara = brutto;
            Sleep (AutoSleep);
            settara ();
        }
        if (netto > 0.0)
        {
		    IncWiegZahl ();
        }
        display_form (WiegKg, &WiegKgE, 0, 0);
        display_form (WiegWindow, &artwform, 0, 0);
        display_form (WiegWindow, &artwconst, 0, 0);
        if (WiegeTyp == STANDARD && autoan) settara ();
		PaintKistBmp ();
        current_wieg = 3;
        if (ratod (aufps.a) != 0.0)
        {
		          strcpy (aufps.anz_einh, wiegzahl);
                  memcpy (&aufparr [aufpidx], &aufps, sizeof (struct AUFP_S));
                  arrtozerl_p (aufpidx);
                  ZerlpClass.dbupdate (atoi (EinAusgang));
        }
        commitwork ();
        beginwork ();
		if (auto_eti)
		{
                 doetikett ();
		}
//        return 0;
		if (wieg_direct && StartWiegen == FALSE )
		{
		            doWOK ();
		}
        return 0;
}

void PrintWLines ()
/**
Icons am Bildschirm anzeigen.
**/
{
         RECT rect, frect;
         PAINTSTRUCT ps;
         HDC hdc;
         HPEN hPen;
         int x, y;

         GetClientRect (WiegWindow, &rect);
         GetFrmRect (&artwform, &frect);

         hdc = BeginPaint (WiegWindow, &ps);

         hPen = CreatePen (PS_SOLID, 2, BLACKCOL);
         SelectObject (hdc, hPen);
         x = rect.right - 4;
         y = 10;
         MoveToEx (hdc, 2, y, NULL);
         LineTo (hdc, x, y);
         DeleteObject (hPen);

         hPen = CreatePen (PS_SOLID, 2, GRAYCOL);
         SelectObject (hdc, hPen);
         MoveToEx (hdc, 2, y - 1, NULL);
         LineTo (hdc, x, y - 2);
         DeleteObject (hPen);


         hPen = CreatePen (PS_SOLID, 2, BLACKCOL);
         SelectObject (hdc, hPen);
         x = rect.right - 4;
         y = frect.bottom + 25;
         alarmy = y;
         MoveToEx (hdc, 2, y, NULL);
         LineTo (hdc, x, y);
         DeleteObject (hPen);

         hPen = CreatePen (PS_SOLID, 2, GRAYCOL);
         SelectObject (hdc, hPen);
         MoveToEx (hdc, 2, y - 1, NULL);
         LineTo (hdc, x, y - 2);
         DeleteObject (hPen);

		 PaintKist (hdc);

         EndPaint (WiegWindow, &ps);
}


LONG FAR PASCAL WiegProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        switch(msg)
        {

              case WM_PAINT :
                    if (hWnd == MessWindow)
                    {
                            display_form (hWnd, &msgform, 0, 0);
                    }


                    if (hWnd == WiegKg)
                    {
                            display_form (hWnd, &WiegKgT, 0, 0);
                            display_form (hWnd, &WiegKgE, 0, 0);
                    }
                    else if (hWnd == WiegCtr)
                    {       if (CtrMode == 0)
                            {
                                    display_form (hWnd, &WieButton, 0, 0);
                                    SetFocus (
                                           WieButton.mask[current_wieg].feldid);
                            }
                            else
                            {
                                    display_form (hWnd, &WieButtonT, 0, 0);
                                    SetFocus (
                                           WieButtonT.mask[current_wieg].feldid);
                            }
                    }
                    else if (hWnd == WiegWindow)
                    {
                            display_form (hWnd, &WiegKgMe, 0, 0);
                            display_form (hWnd, &WiegEinAusgang, 0, 0);
                            display_form (hWnd, &artwformT, 0, 0);
                            display_form (hWnd, &artwconstT, 0, 0);
                            if (WiegeTyp == ENTNAHME)
                            {
                                  if (StartWiegen)
                                  {
                                        display_form (hWnd, &startwieg, 0, 0);
                                  }
                                  display_form (hWnd, &fEntnahmeGewTxt, 0, 0);
                                  display_form (hWnd, &fEntnahmeGew, 0, 0);
                            }
                            display_form (hWnd, &EtiButton, 0, 0);
							if (WithSpezEti)
							{
                                 display_form (hWnd, &EtiSpezButton, 0, 0);
							}
							if (WithLagerAuswahl)
							{
	                            display_form (hWnd, &LgrSpezButton, 0, 0);
							}
							if (WithZusatz && (_a_bas.charg_hand == 1 || _a_bas.charg_hand == 9 ||
								_a_bas.zerl_eti == 1))
							{
                                 display_form (hWnd, &ZusatzButton, 0, 0);
							}

							if (_a_bas.charg_hand == 9)
							{
								artwformTW.mask[0].feld = IdentNr;
								artwform.mask[0].feld = aufps.ident_nr;
							}
							else
							{
								artwformTW.mask[0].feld = ChargenNr;
								artwform.mask[0].feld = aufps.ls_charge;
							}

                            if (artwform.mask[0].feldid)
                            {
                                 display_form (hWnd, &artwformTW, 0, 0);
                                 display_form (hWnd, &artwform, 0, 0);
                                 display_form (hWnd, &artwconst, 0, 0);
                            }
                            display_form (hWnd, &ftara, 0, 0);
                            display_form (hWnd, &ftaratxt, 0, 0);
                            display_form (hWnd, &sw_abzug, 0, 0); //021109
							display_form (hWnd, &ftexte, 0, 0);
							//220712 A
						    SetFestAllBitmaps ();
							display_form (hWnd, &ffesttara, 0, 0); 
							//220712 E
                            PrintWLines ();



                    }
                    else if (hWnd == AlarmWindow)
                    {
                            display_form (hWnd, &alform, 0, 0);
                    }
                    break;
             case WM_COMMAND :
                    if (LOWORD (wParam) == KEY5 ||
                        LOWORD (wParam) == VK_F5)
                    {
                            syskey = KEY5;
                            SendKey (VK_F5);
                            break;
                    }
                    if (LOWORD (wParam) == KEYCR)
                    {
                            syskey = KEYCR;
                            SendKey (VK_RETURN);
                            break;
                    }
                    if (LOWORD (wParam) == BUTARA)
                    {
                            PostMessage (WiegWindow, WM_USER, wParam, 0l);
                            break;
                    }
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}


void RegisterWieg (void)
/**
Fenster fuer Auftragskopf registrieren
**/
{
        static int registered = 0;
        WNDCLASS wc;

        if (registered == 0)
        {
                   wc.style =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                               | CS_OWNDC;
                   wc.cbClsExtra    =  0;
                   wc.cbWndExtra    =  0;
                   wc.hInstance     =  hMainInst;
                   wc.hIcon         =  LoadIcon (hMainInst, "FITICON");
                   wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                   wc.lpszMenuName  =  "";
                   wc.lpfnWndProc   =  WiegProc;
                   wc.lpszMenuName  =  "";
                   wc.hbrBackground =  CreateSolidBrush (RGB (0, 255, 255));
                   wc.lpszClassName =  "AufWieg";
                   RegisterClass(&wc);

                   // wc.hbrBackground =  CreateSolidBrush (RGB (0, 0, 120));
                   wc.hbrBackground =   GetStockObject (LTGRAY_BRUSH);
                   wc.lpszClassName =  "AufWiegCtr";
                   RegisterClass(&wc);

                   wc.lpfnWndProc   =   WiegProc;
                   wc.hbrBackground =  CreateSolidBrush (RGB (0, 0, 255));
                   wc.lpszClassName =  "AufWiegKg";
                   RegisterClass(&wc);
                   registered = 1;
        }
}

void TestEnter (void)
/**
Eingabefeld testen.
**/
{
       GetWindowText (artwform.mask[0].feldid,
                      artwform.mask[0].feld,
                      artwform.mask[0].length);
}


HWND *FocusTab [20];
int FocusTabCount = 0;
int SpezEtiPos = -1;
int SpezLgrPos = -1;
int ZusatzPos = -1;


void SetFocusTab (form *wform)
{
	   for (int i = 0; i < wform->fieldanz; i ++)
	   {
		   FocusTab[i] = &wform->mask[i].feldid;
	   }
	   FocusTabCount = i;
       if (CtrMode == 1) return;

//	   if (WithEti || WithSumEti)
	   {
		   FocusTab[i] = &EtiButton.mask[0].feldid;
		   i ++;
		   FocusTab[i] = &EtiButton.mask[1].feldid;
		   i ++;
	   }
	   if (WithZusatz)
	   {
		   FocusTab[i] = &ZusatzButton.mask[0].feldid;
		   ZusatzPos = i;
		   i ++;
	   }
	   if (WithSpezEti)
	   {
		   FocusTab[i] = &EtiSpezButton.mask[0].feldid;
		   SpezEtiPos = i;
		   i ++;
	   }
	   if (WithSpezEti)
	   {
		   FocusTab[i] = &LgrSpezButton.mask[0].feldid;
			i ++;
		   FocusTab[i] = &LgrSpezButton.mask[2].feldid;
		   SpezLgrPos = i;
			i ++;
	   }
	   FocusTabCount = i;
}

void SetWiegFocus (void)
/**
Focus fuer Wiegmaske setzen.
**/
{
       form *wform;

       if (CtrMode == 1)
       {
                  wform = &WieButtonT;
       }
       else
       {
                  wform = &WieButton;
       }

	   SetFocusTab (wform);
	   if (current_wieg >= FocusTabCount) current_wieg = 0;
       SetFocus (*FocusTab [current_wieg]);
	   return;

       if (current_wieg < etiend)
       {
                  SetFocus (wform->mask[current_wieg].feldid);
       }
       else if (current_wieg < wiegend)
	   {
                  SetFocus (EtiButton.mask[current_wieg - etiend].feldid);
	   }
	   else if (WithSpezEti && (current_wieg - wiegend < EtiSpezButton.fieldanz))
	   {
                  SetFocus (EtiSpezButton.mask[current_wieg - wiegend].feldid);
	   }
	   else if (WithLagerAuswahl && (current_wieg - wiegend < LgrSpezButton.fieldanz))
	   {
                  SetFocus (LgrSpezButton.mask[current_wieg - wiegend].feldid);
	   }
	   else if (WithZusatz && (current_wieg - wiegend < ZusatzButton.fieldanz))
	   {
                  SetFocus (ZusatzButton.mask[current_wieg - wiegend].feldid);
	   }
       else if (artwform.mask[0].attribut != EDIT)
	   {
		          current_wieg = 0;
                  SetFocus (wform->mask[current_wieg].feldid);
	   }
	   else
       {
                  SetFocus (artwform.mask[0].feldid);
                  SendMessage (artwform.mask[0].feldid, EM_SETSEL,
                               (WPARAM) 0, MAKELONG (-1, 0));
       }
}

int ReturnActionW (void)
/**
Returntaste behandeln.
**/
{
       form *wform;
	   int current;
	   int we;
	   we = wiegend;
	   if (WithSpezEti)
	   {
		   we ++;
	   }
	   if (WithLagerAuswahl)
	   {
		   we ++;
	   }
	   if (WithZusatz)
	   {
		   we ++;
	   }


       if (current_wieg == SpezEtiPos)
	   {
                  if (EtiSpezButton.mask[current_wieg - wiegend].after)
				  {
                       (*EtiSpezButton.mask[current_wieg - wiegend].after) ();
				  }
                  else if (EtiSpezButton.mask[current_wieg - wiegend].BuId)
				  {
                       SendKey
                                 (EtiSpezButton.mask[current_wieg - wiegend].BuId);
				  }
				  return TRUE;
	   }

       if (current_wieg == SpezLgrPos)
	   {
                  if (LgrSpezButton.mask[current_wieg - wiegend].after)
				  {
                       (*LgrSpezButton.mask[current_wieg - wiegend].after) ();
				  }
                  else if (LgrSpezButton.mask[current_wieg - wiegend].BuId)
				  {
                       SendKey
                                 (LgrSpezButton.mask[current_wieg - wiegend].BuId);
				  }
				  return TRUE;
	   }
       if (current_wieg == ZusatzPos)
	   {
                  if (ZusatzButton.mask[0].after)
				  {
                       (*ZusatzButton.mask[0].after) ();
				  }
                  else if (ZusatzButton.mask[0].BuId)
				  {
                       SendKey
                                 (ZusatzButton.mask[0].BuId);
				  }
				  return TRUE;
	   }

       if (WithSpezEti && (current_wieg == (wiegend + EtiSpezButton.fieldanz)))
       {
                  PostMessage (WiegWindow, WM_KEYDOWN, VK_DOWN, 0l);
                  return TRUE;
       }
       else if (WithLagerAuswahl && (current_wieg == (wiegend + LgrSpezButton.fieldanz)))
       {
                  PostMessage (WiegWindow, WM_KEYDOWN, VK_DOWN, 0l);
                  return TRUE;
       }
	   else if ((WithSpezEti == FALSE) && (current_wieg == wiegend))
       {
                  PostMessage (WiegWindow, WM_KEYDOWN, VK_DOWN, 0l);
                  return TRUE;
       }


       if (current_wieg == wiegend)
//       if (current_wieg == we)
       {
                  PostMessage (WiegWindow, WM_KEYDOWN, VK_DOWN, 0l);
                  return TRUE;
       }

	   if (current_wieg >= etiend)
	   {
	              wform = &EtiButton;
				  current = current_wieg - etiend;
	   }

       else if (CtrMode == 1)
       {
                  wform = &WieButtonT;
				  current = current_wieg;
       }
       else
       {
                  wform = &WieButton;
				  current = current_wieg;
       }

       if (CtrMode == 0 && current == autopos)
       {
                        PressAuto ();
       }
       else if (wform->mask[current].after)
       {
                       (*wform->mask[current].after) ();
       }
       else if (wform->mask[current].BuId)
       {
                       SendKey
                                 (wform->mask[current].BuId);
       }

	   else if (WithSpezEti)
	   {
                  if (EtiSpezButton.mask[current_wieg - wiegend].after)
				  {
                       (*EtiSpezButton.mask[current_wieg - wiegend].after) ();
				  }
                  else if (EtiSpezButton.mask[current_wieg - wiegend].BuId)
				  {
                       SendKey
                                 (EtiSpezButton.mask[current_wieg - wiegend].BuId);
				  }

	   }
	   else if (WithLagerAuswahl)
	   {
                  if (LgrSpezButton.mask[current_wieg - wiegend].after)
				  {
                       (*LgrSpezButton.mask[current_wieg - wiegend].after) ();
				  }
                  else if (LgrSpezButton.mask[current_wieg - wiegend].BuId)
				  {
                       SendKey
                                 (LgrSpezButton.mask[current_wieg - wiegend].BuId);
				  }
	   }
       return TRUE;
}

int TestAktivButton (int sign, int current_wieg)
/**
Status von Colbutton pruefen.
**/
{
       ColButton *Cub;
       int run;
       form *wform, *wform0;;
	   int current;

       if (CtrMode == 1)
       {
                  wform0= &WieButtonT;
       }
       else
       {
                  wform0= &WieButton;
       }

       run = 1;
       while (TRUE)
       {
              if (current_wieg >= wform0->fieldanz + EtiButton.fieldanz) break;
   	          if (current_wieg >= etiend)
			  {
				  wform = &EtiButton;
				  current = current_wieg - etiend;
			  }
			  else
			  {
				  wform = wform0;
				  current = current_wieg;
			  }
              Cub = (ColButton *) wform->mask[current].feld;
              if (((wform->mask[current].attribut & REMOVED) == 0) &&
				   (Cub->aktivate != -1)) break;
			  current_wieg += sign;
              if (sign == PLUS && current_wieg > wiegend)
              {
                                if (run == 2) break;
                                current_wieg = 0;
                                run ++;
              }
              else if (sign == MINUS && current_wieg < 0)
              {
                                if (run == 2) break;
                                current_wieg = wiegend;
                                run ++;
              }
       }
       return (current_wieg);
}


int IsWiegMessage (MSG *msg)
/**
HotKey Testen.
**/
{
         POINT mousepos;

         switch (msg->message)
         {
              case WM_KEYDOWN :
              {
                     TestEnter ();
                     switch (msg->wParam)
                     {
                            case VK_F5 :
                                    if (CtrMode == 1)
                                    {
                                          closetara ();
                                    }
                                    else
                                    {
                                          break_wieg = 1;
                                    }
                                    return TRUE;
                            case VK_RETURN :
                                    return (ReturnActionW ());
                            case VK_DOWN :
                            case VK_RIGHT :
                                    current_wieg ++;
                                    if (current_wieg > wiegend)
                                                current_wieg = 0;
                                    current_wieg = TestAktivButton (PLUS, current_wieg);
                                    SetWiegFocus ();
                                    return TRUE;
                            case VK_UP :
                            case VK_LEFT :
                                   current_wieg --;
                                   if (current_wieg < 0)
                                                current_wieg = wiegend;
                                    current_wieg = TestAktivButton (MINUS, current_wieg);
                                    SetWiegFocus ();
                                    return TRUE;
                            case VK_TAB :
                                    if (GetKeyState (VK_SHIFT) < 0)
                                    {
                                             current_wieg --;
                                             if (current_wieg < 0)
                                                  current_wieg = wiegend;
                                             current_wieg =
                                                 TestAktivButton (MINUS, current_wieg);
                                     }
                                     else
                                     {
                                             current_wieg ++;
                                             if (current_wieg > wiegend)
                                                  current_wieg = 0;
                                             current_wieg =
                                                 TestAktivButton (PLUS, current_wieg);
                                     }
                                     SetWiegFocus ();
                                     return TRUE;
                            default :
                                    return FALSE;
                     }
                     break;
              }
              case WM_LBUTTONDOWN :
              {
                    GetCursorPos (&mousepos);

                    if (MouseinWindow (artwform.mask[0].feldid, &mousepos) &&
						artwform.mask[0].attribut == EDIT)
                    {
						char buffer [255];
						if (_a_bas.charg_hand == 9)
						{
                          strcpy (buffer, aufps.ident_nr);
						  NuBlock.SetCharChangeProc (ChangeChar);
						  /*110908 so ist es auf den Touchern bei M�fli h�ngengeblieben
                          NuBlock.EnterNumBox (LogoWindow, "  Ident-Nr  ",
                                       buffer, -1, "", EntNuProc);
									   */
                          NuBlock.EnterNumBox (WiegWindow, "  Ident-Nr  ",
                                       buffer, -1, "", EntNuProc);    
						}
						else 
						{
                          strcpy (buffer, aufps.ls_charge); //110908
						  NuBlock.SetCharChangeProc (ChangeChar);//110908
/* 110908
                          EnterNumBox (WiegWindow,"  Chargen-Nr  ",
                                       buffer, 17,
                                       artwform.mask[0].picture);
									   */
                          NuBlock.EnterNumBox (WiegWindow,"  Chargen-Nr  ",
                                       buffer, -1, "", EntNuProc);    
						}
						if (syskey != KEY5)
						{
								if ((_a_bas.charg_hand == 9 || _a_bas.charg_hand == 1) && strlen (buffer) > 4)
								{
									Text Ean = buffer;
									ScanEan128Charge (Ean, 0);
								}
								else
								{
									strcpy (aufps.ls_charge, buffer); 
								}

								display_form (WiegWindow, &artwform, 0, 0);
								current_wieg = TestAktivButton (PLUS, current_wieg);
								SetWiegFocus ();
						 }
						 return TRUE;

                    }
              }
              case WM_USER :
                    if (LOWORD (msg->wParam) == BUTARA)
                    {
                          dotara ();
                          return TRUE;
                    }
         }
         return FALSE;
}

void PosArtFormW (void)
/**
Position fuer artform festlegen.
**/
{
        static int posOK = 0;
        int i, s, z;


        if (posOK) return;

        posOK = 1;
        for (i = 0; i < artwformT.fieldanz; i ++)
        {
                    s = artwformT.mask[i].pos [1];
                    z = artwformT.mask[i].pos [0];
                    KonvTextAbsPos (artwform.font,
                                    &z, &s);
                    artwformT.mask[i].pos [1] = s;
                    artwformT.mask[i].pos [0] = z;
                    s = artwformT.mask[i].length;
                    z = artwformT.mask[i].rows;
                    KonvTextAbsPos (artwform.font,
                                    &z, &s);
                    artwformT.mask[i].length = s;
                    artwformT.mask[i].rows = z;
        }
        for (i = 0; i < artwform.fieldanz; i ++)
        {
                    s = artwform.mask[i].pos [1];
                    z = artwform.mask[i].pos [0];
                    KonvTextAbsPos (artwform.font,
                                    &z, &s);
                    artwform.mask[i].pos [1] = s;
                    artwform.mask[i].pos [0] = z;
                    s = artwform.mask[i].length;
                    z = artwform.mask[i].rows;
                    KonvTextAbsPos (artwform.font,
                                    &z, &s);
                    artwform.mask[i].length = s;
                    artwform.mask[i].rows = z;
        }
        for (i = 0; i < fEntnahmeGewTxt.fieldanz; i ++)
        {
                    s = fEntnahmeGewTxt.mask[i].pos [1];
                    z = fEntnahmeGewTxt.mask[i].pos [0];
                    KonvTextAbsPos (fEntnahmeGew.font,
                                    &z, &s);
                    fEntnahmeGewTxt.mask[i].pos [1] = s;
                    fEntnahmeGewTxt.mask[i].pos [0] = z;
                    s = fEntnahmeGewTxt.mask[i].length;
                    z = fEntnahmeGewTxt.mask[i].rows;
                    KonvTextAbsPos (fEntnahmeGew.font,
                                    &z, &s);
                    fEntnahmeGewTxt.mask[i].length = s;
                    fEntnahmeGewTxt.mask[i].rows = z;
        }
        for (i = 0; i < fEntnahmeGew.fieldanz; i ++)
        {
                    s = fEntnahmeGew.mask[i].pos [1];
                    z = fEntnahmeGew.mask[i].pos [0];
                    KonvTextAbsPos (fEntnahmeGew.font,
                                    &z, &s);
                    fEntnahmeGew.mask[i].pos [1] = s;
                    fEntnahmeGew.mask[i].pos [0] = z;
                    s = fEntnahmeGew.mask[i].length;
                    z = fEntnahmeGew.mask[i].rows;
                    KonvTextAbsPos (fEntnahmeGew.font,
                                    &z, &s);
                    fEntnahmeGew.mask[i].length = s;
                    fEntnahmeGew.mask[i].rows = z;
        }
}

void PosArtFormC (void)
/**
Position fuer artform festlegen.
**/
{
        static int posOK = 0;
        int i, s, z;

        if (posOK) return;

        posOK = 1;
        for (i = 0; i < artwconstT.fieldanz; i ++)
        {
                    s = artwconstT.mask[i].pos [1];
                    z = artwconstT.mask[i].pos [0];
                    KonvTextAbsPos (artwconst.font,
                                    &z, &s);
                    artwconstT.mask[i].pos [1] = s;
                    artwconstT.mask[i].pos [0] = z;
                    s = artwconstT.mask[i].length;
                    z = artwconstT.mask[i].rows;
                    KonvTextAbsPos (artwconst.font,
                                    &z, &s);
                    artwconstT.mask[i].length = s;
                    artwconstT.mask[i].rows = z;
        }
        for (i = 0; i < startwieg.fieldanz; i ++)
        {
                    s = startwieg.mask[i].pos [1];
                    z = startwieg.mask[i].pos [0];
                    KonvTextAbsPos (startwieg.font,
                                    &z, &s);
                    startwieg.mask[i].pos [1] = s;
                    startwieg.mask[i].pos [0] = z;
                    s = startwieg.mask[i].length;
                    z = startwieg.mask[i].rows;
                    KonvTextAbsPos (startwieg.font,
                                    &z, &s);
                    startwieg.mask[i].length = s;
                    startwieg.mask[i].rows = z;
        }
        for (i = 0; i < artwconst.fieldanz; i ++)
        {
                    s = artwconst.mask[i].pos [1];
                    z = artwconst.mask[i].pos [0];
                    KonvTextAbsPos (artwconst.font,
                                    &z, &s);
                    artwconst.mask[i].pos [1] = s;
                    artwconst.mask[i].pos [0] = z;
                    s = artwconst.mask[i].length;
                    z = artwconst.mask[i].rows;
                    KonvTextAbsPos (artwconst.font,
                                    &z, &s);
                    artwconst.mask[i].length = s;
                    artwconst.mask[i].rows = z;
        }
}

void PosArtFormP (void)
/**
Position fuer artform festlegen.
**/
{
        static int posOK = 0;
        int i, s, z;

        if (posOK) return;

        posOK = 1;
        for (i = 0; i < artwformT.fieldanz; i ++)
        {
                    s = artwformT.mask[i].pos [1];
                    z = artwformT.mask[i].pos [0];
                    KonvTextAbsPos (artwform.font,
                                    &z, &s);
                    artwformT.mask[i].pos [1] = s;
                    artwformT.mask[i].pos [0] = z;
                    s = artwformT.mask[i].length;
                    z = artwformT.mask[i].rows;
                    KonvTextAbsPos (artwform.font,
                                    &z, &s);
                    artwformT.mask[i].length = s;
                    artwformT.mask[i].rows = z;
        }
        for (i = 0; i < artwform.fieldanz; i ++)
        {
                    s = artwform.mask[i].pos [1];
                    z = artwform.mask[i].pos [0];
                    KonvTextAbsPos (artwform.font,
                                    &z, &s);
                    artwform.mask[i].pos [1] = s;
                    artwform.mask[i].pos [0] = z;
                    s = artwform.mask[i].length;
                    z = artwform.mask[i].rows;
                    KonvTextAbsPos (artwform.font,
                                    &z, &s);
                    artwform.mask[i].length = s;
                    artwform.mask[i].rows = z;
        }
        for (i = 0; i < fEntnahmeGewTxt.fieldanz; i ++)
        {
                    s = fEntnahmeGewTxt.mask[i].pos [1];
                    z = fEntnahmeGewTxt.mask[i].pos [0];
                    KonvTextAbsPos (fEntnahmeGewTxt.font,
                                    &z, &s);
                    fEntnahmeGewTxt.mask[i].pos [1] = s;
                    fEntnahmeGewTxt.mask[i].pos [0] = z;
                    s = fEntnahmeGewTxt.mask[i].length;
                    z = fEntnahmeGewTxt.mask[i].rows;
                    KonvTextAbsPos (fEntnahmeGewTxt.font,
                                    &z, &s);
                    fEntnahmeGewTxt.mask[i].length = s;
                    fEntnahmeGewTxt.mask[i].rows = z;
        }
        for (i = 0; i < fEntnahmeGew.fieldanz; i ++)
        {
                    s = fEntnahmeGew.mask[i].pos [1];
                    z = fEntnahmeGew.mask[i].pos [0];
                    KonvTextAbsPos (fEntnahmeGew.font,
                                    &z, &s);
                    fEntnahmeGew.mask[i].pos [1] = s;
                    fEntnahmeGew.mask[i].pos [0] = z;
                    s = fEntnahmeGew.mask[i].length;
                    z = fEntnahmeGew.mask[i].rows;
                    KonvTextAbsPos (fEntnahmeGewTxt.font,
                                    &z, &s);
                    fEntnahmeGew.mask[i].length = s;
                    fEntnahmeGew.mask[i].rows = z;
        }
}

void PosMeForm (int yw, int xw, int cyw, int cxw)
/**
Position fuer WiegKgMe festlegen.
**/
{
        static int posOK = 0;
        int s, z;

        if (posOK) return;

        posOK = 1;

        WiegKgMe.mask[0].pos[0] = yw + WiegKgE.mask[0].pos[0];
        WiegKgMe.mask[0].pos[1] = xw + cxw + 30;
        s = WiegKgMe.mask[0].length;
        z = WiegKgMe.mask[0].rows;
        KonvTextAbsPos (WiegKgMe.font, &z, &s);
        WiegKgMe.mask[0].length = s;
        WiegKgMe.mask[0].rows = z;

        WiegEinAusgang.mask[0].pos[0] = 15 + WiegEinAusgang.mask[0].pos[0];
        WiegEinAusgang.mask[0].pos[1] = xw + cxw + 30;
        s = WiegEinAusgang.mask[0].length;
        z = WiegEinAusgang.mask[0].rows;
        KonvTextAbsPos (WiegEinAusgang.font, &z, &s);
        WiegEinAusgang.mask[0].length = s;
        WiegEinAusgang.mask[0].rows = z;

}

void GetZentrierWerte (int *buglen, int *buabs, int wlen, int anz,
                       int abs, int size)
/**
Werte fuer Zentrierung ermitteln.
**/
{
        int blen;
        int spos;

        while (TRUE)
        {
                 blen = anz * (size + abs) - abs;
                 spos = (wlen - blen) / 2;
                 if (spos > 9)
                 {
                              *buglen = blen;
                              *buabs  = abs + size;
                              return;
                 }
                 abs -= 10;
                 if (abs <= 0) return;
       }
}

int closetara (void)
/**
Tara-Leiste schliessen.
**/
{
        CtrMode = 0;
        CloseControls (&WieButtonT);
        current_wieg = 0;
        if (ohne_preis)
        {
                 wiegend = 8;
        }
        else
        {
                 wiegend = 9;
        }
        return 0;
}

void testeti (void)
/**
Wiegen durchfuehren.
**/
{
		double a;
		int dsqlstatus;

		a = ratod (aufps.a);

		DbClass.sqlin ((long *)   &sys_peri.sys, 2, 0);
		DbClass.sqlin ((double *) &a, 3, 0);
        DbClass.sqlout ((long *) &eti_nr, 2, 0);
        DbClass.sqlout ((char *) krz_txt, 0, 17);
		dsqlstatus = DbClass.sqlcomm ("select eti_nr, krz_txt from klebdest "
			                          "where sys = ? "
									  "and a = ? "
									  "and we = 1 "
									  "and einz = 1");
		if (dsqlstatus)
		{
			WithEti = FALSE;
		}
		else
		{
			WithEti = TRUE;
		}

		DbClass.sqlin ((long *)   &sys_peri.sys, 2, 0);
		DbClass.sqlin ((double *) &a, 3, 0);
        DbClass.sqlout ((long *) &sum_eti_nr, 2, 0);
        DbClass.sqlout ((char *) sum_krz_txt, 0, 17);
		dsqlstatus = DbClass.sqlcomm ("select eti_nr, krz_txt from klebdest "
			                          "where sys = ? "
									  "and a = ? "
									  "and we = 1 "
									  "and summ = 1");
		if (dsqlstatus)
		{
			WithSumEti = FALSE;
		}
		else
		{
			WithSumEti = TRUE;
		}
		if (WithEti == FALSE && WithSumEti == FALSE)
		{
			EtiButton.mask[0].attribut = REMOVED;
			EtiButton.mask[1].attribut = REMOVED;
			wiegend = 9;
		}
		else
		{
			EtiButton.mask[0].attribut = COLBUTTON;
			EtiButton.mask[1].attribut = COLBUTTON;
			wiegend = 9;
		}

/*
		wiegend = 6;
		if (WithEti) wiegend ++;
		if (WithSumEti) wiegend ++;
		if (WithSpezEti) wiegend ++;
		if (WithZusatz) wiegend ++;
*/
		if (WithEti)
		{
            ActivateColButton (NULL, &EtiButton, 0, 2, 0);
		}
		else
		{
            ActivateColButton (NULL, &EtiButton, 0, -1, 0);
		}

		if (WithSumEti)
		{
            ActivateColButton (NULL, &EtiButton, 1, 2, 0);
		}
		else
		{
            ActivateColButton (NULL, &EtiButton, 1, -1, 0);
		}
}




static char EtiPath [512];

void WriteParamKiste (int etianz)
/**
Parameter-Datei fuer Kistenetikettschreiben.
**/
{
	    char *tmp;
		char *etc;
	    char dname [512];
		FILE *pa;

		tmp = getenv ("TMPPATH");
		if (tmp == NULL) return;
		etc = getenv ("BWSETC");
		if (etc == NULL) return;


        adr_class.lese_adr (lief.adr);

		sprintf (dname, "%s\\parafile", tmp);
		pa = fopen (dname, "w");
		if (pa == NULL) return;


		fprintf (pa, "$$ARTNR;%.0lf\n",  ratod (aufps.a));
		fprintf (pa, "$$ABEZ1;%s\n",     _a_bas.a_bz1);
		fprintf (pa, "$$ABEZ2;%s\n",     _a_bas.a_bz2);
		fprintf (pa, "$$a_bas_erw.pp_a_bz1;%s\n", a_bas_erw.pp_a_bz1);
		fprintf (pa, "$$a_bas_erw.pp_a_bz2;%s\n", a_bas_erw.pp_a_bz2);

		fprintf (pa, "$$CHARGE;%.0lf\n",    zerl_k.partie);


		fprintf (pa, "$$LIEF$;%s\n",      zerl_k.lief);
		fprintf (pa, "$$LIEFNR$;%8.0lf\n",    zerl_k.partie);
		fprintf (pa, "$$ABEZ1;%s\n",     aufps.a_bz1);
		fprintf (pa, "$$ABEZ2;%s\n",     aufps.a_bz2);
		fprintf (pa, "$$ARTNR;%.0lf\n",  ratod (aufps.a));
		fprintf (pa, "$$WEDAT;%s\n",     lieferdatum);
		fprintf (pa, "$$NETTOG;%.3lf\n",  eti_netto);
		fprintf (pa, "$$TARAG;%.3lf\n",   eti_tara);
		fprintf (pa, "$$BRUTTOG;%.3lf\n", eti_brutto);
		fprintf (pa, "$$STUECK;%.2lf\n",  eti_stk );
		fprintf (pa, "$$LIEFNAME;%s\n",   lief_krz1);
		fprintf (pa, "$$WECHARGE;%s\n",     aufps.ls_charge);
		fprintf (pa, "$$MHD;%s\n",        aufps.hbk_date);
		fprintf (pa, "$$NAME1;%s\n",      _adr.adr_nam1);
		fprintf (pa, "$$NAME2;%s\n",      _adr.adr_nam2);
		fprintf (pa, "$$STRASSE;%s\n",    _adr.str);
		fprintf (pa, "$$PLZ;%s\n",        _adr.plz);
		fprintf (pa, "$$ORT;%s\n",        _adr.ort1);
		fprintf (pa, "$$ANZAHL;%d\n",     etianz);
		fprintf (pa, "$$WIEGANZ;%ld\n",  atol (wiegzahl));

		fclose (pa);

/*
        sprintf (progname, "%s\\BIN\\rswrun put_eti %s %s\\%s %s\\parafile %d",
                                     rosipath, eti_ki_drk, etc, eti_kiste, tmp, etianz);
        ProcWaitExec (progname, SW_SHOWMINIMIZED, 0, 40, -1, 0);
*/

        sprintf (EtiPath, "%s\\%s", etc, eti_kiste);

        int ret = PutEti (eti_ki_drk, EtiPath, dname, etianz, eti_ki_typ) ;
        if (ret == -1)
        {
            disp_messG ("Etikett kann nicht gedruckt werden", 2);
        }
}

void WriteParamEZ (int etianz)
/**
Parameter-Datei fuer Kistenetikettschreiben.
**/
{
	    char *tmp;
		char *etc;
	    char dname [512];
		FILE *pa;

		tmp = getenv ("TMPPATH");
		if (tmp == NULL) return;
		etc = getenv ("BWSETC");
		if (etc == NULL) return;

        adr_class.lese_adr (lief.adr);

		sprintf (dname, "%s\\parafile", tmp);
		pa = fopen (dname, "w");
		if (pa == NULL) return;
		fprintf (pa, "$$LIEF;%s\n",      zerl_k.lief);
		fprintf (pa, "$$LIEFNR;%8.0lf\n",    zerl_k.partie);
		fprintf (pa, "$$ABEZ1;%s\n",     aufps.a_bz1);
		fprintf (pa, "$$ABEZ2;%s\n",     aufps.a_bz2);
		fprintf (pa, "$$ARTNR;%.0lf\n",  ratod (aufps.a));
		fprintf (pa, "$$WEDAT;%s\n",     lieferdatum);
		fprintf (pa, "$$NETTOG;%.3lf\n",  eti_netto);
		fprintf (pa, "$$TARAG;%.3lf\n",   eti_tara);
		fprintf (pa, "$$BRUTTOG;%.3lf\n", eti_brutto);
		fprintf (pa, "$$STUECK;%.2lf\n",  eti_stk );
		fprintf (pa, "$$LIEFNAME;%s\n",   lief_krz1);
		fprintf (pa, "$$CHARGE;%s\n",     aufps.ls_charge);
		fprintf (pa, "$$MHD;%s\n",        aufps.hbk_date);
		fprintf (pa, "$$NAME1;%s\n",      _adr.adr_nam1);
		fprintf (pa, "$$NAME2;%s\n",      _adr.adr_nam2);
		fprintf (pa, "$$STRASSE;%s\n",    _adr.str);
		fprintf (pa, "$$PLZ;%s\n",        _adr.plz);
		fprintf (pa, "$$ORT;%s\n",        _adr.ort1);
		fprintf (pa, "$$ANZAHL;%d\n",     etianz);
		fprintf (pa, "$$WIEGANZ;%ld\n",  atol (wiegzahl));

		fclose (pa);
/*
        sprintf (progname, "%s\\BIN\\rswrun put_eti %s %s\\%s %s\\parafile %d",
                                     rosipath, eti_ez_drk, etc, eti_ez, tmp, etianz);
        ProcWaitExec (progname, SW_SHOWMINIMIZED, 0, 40, -1, 0);
*/

        sprintf (EtiPath, "%s\\%s", etc, eti_ez);

        int ret = PutEti (eti_ez_drk, EtiPath, dname, etianz, eti_ez_typ) ;
        if (ret == -1)
        {
            disp_messG ("Etikett kann nicht gedruckt werden", 2);
        }

}

void WriteParamEV (int etianz)
/**
Parameter-Datei fuer Kistenetikettschreiben.
**/
{
	    char *tmp;
		char *etc;
	    char dname [512];
		FILE *pa;

		tmp = getenv ("TMPPATH");
		if (tmp == NULL) return;
		etc = getenv ("BWSETC");
		if (etc == NULL) return;


        adr_class.lese_adr (lief.adr);

		sprintf (dname, "%s\\parafile", tmp);
		pa = fopen (dname, "w");
		if (pa == NULL) return;
		fprintf (pa, "$$LIEF;%s\n",      zerl_k.lief);
		fprintf (pa, "$$LIEFNR;%8.0lf\n",    zerl_k.partie);
		fprintf (pa, "$$ABEZ1;%s\n",     aufps.a_bz1);
		fprintf (pa, "$$ABEZ2;%s\n",     aufps.a_bz2);
		fprintf (pa, "$$ARTNR;%.0lf\n",  ratod (aufps.a));
		fprintf (pa, "$$WEDAT;%s\n",     lieferdatum);
		fprintf (pa, "$$NETTOG;%.3lf\n",  eti_netto);
		fprintf (pa, "$$TARAG;%.3lf\n",   eti_tara);
		fprintf (pa, "$$BRUTTOG;%.3lf\n", eti_brutto);
		fprintf (pa, "$$STUECK;%.2lf\n",  eti_stk );
		fprintf (pa, "$$LIEFNAME;%s\n",   lief_krz1);
		fprintf (pa, "$$CHARGE;%s\n",     aufps.ls_charge);
		fprintf (pa, "$$MHD;%s\n",        aufps.hbk_date);
		fprintf (pa, "$$NAME1;%s\n",      _adr.adr_nam1);
		fprintf (pa, "$$NAME2;%s\n",      _adr.adr_nam2);
		fprintf (pa, "$$STRASSE;%s\n",    _adr.str);
		fprintf (pa, "$$PLZ;%s\n",        _adr.plz);
		fprintf (pa, "$$ORT;%s\n",        _adr.ort1);
		fprintf (pa, "$$ANZAHL;%d\n",     etianz);

		fclose (pa);
/*
        sprintf (progname, "%s\\BIN\\rswrun put_eti %s %s\\%s %s\\parafile %d",
                                     rosipath, eti_ev_drk, etc, eti_ev, tmp, etianz);
        ProcWaitExec (progname, SW_SHOWMINIMIZED, 0, 40, -1, 0);
*/

        sprintf (EtiPath, "%s\\%s", etc, eti_ev);

        int ret = PutEti (eti_ev_drk, EtiPath, dname, etianz, eti_ev_typ) ;
        if (ret == -1)
        {
            disp_messG ("Etikett kann nicht gedruckt werden", 2);
        }

}

void WriteParamArt (int etianz)
/**
Parameter-Datei fuer Artikeletikettschreiben.
**/
{
	    char *tmp;
		char *etc;
	    char dname [512];
		FILE *pa;
		char cdatum [12];
		int prodphase[10] ;
		int i,y;
		double a;
        char lieferdat [12];
        short sa ;
        double pr_ek;
        double pr_vk;


		if (atoi(EinAusgang) == PRODUKTIONSSTART) 
		{
			if (prodartikel.a == 0.0) 
			{
				if (HoleProdartikel (_a_bas.a) == 0.0) return ;
	            if (lese_a_bas (prodartikel.a) != 0) return;
				zerl_k.a_prod = prodartikel.a; 
				ZerlkClass.dbupdate ();
			}
		}
		else
		{
	            if (lese_a_bas (ratod(aufps.a)) != 0) return;
		}

        pr_ek = pr_vk = (double) 0.0; sa = 0;
		sysdate (lieferdat);


		tmp = getenv ("TMPPATH");
		if (tmp == NULL) return;
		etc = getenv ("BWSETC");
		if (etc == NULL) return;
		sysdate(cdatum);


		      if (DllPreise.PriceLib != NULL && 
			  DllPreise.preise_holen != NULL)
			  {
			      dsqlstatus = (DllPreise.preise_holen) (Mandant, 
                                         0,
                                         0,
                                         Kunde,
                                         _a_bas.a,
                                         lieferdat,
                                         &sa,
                                         &pr_ek,
                                          &pr_vk);
			      if (dsqlstatus ==1) dsqlstatus = 0;
			  }



        adr_class.lese_adr (lief.adr);

		for (i = 0; i< 10; i++)
		{
			prodphase[i] = 0;
		}
        PRDK_K_CLASS Prdk_k;
        a = _a_bas.a;
        Prdk_k.sqlin ((short *)  &Mandant, 1, 0);
        Prdk_k.sqlin ((double *) &a,   3, 0);
        Prdk_k.sqlout ((double *) prdk_k.prodphase,   0, sizeof (prdk_k.prodphase));
        int dsqlstatus = Prdk_k.sqlcomm ("select prodphase from prdk_k "
                                         "where mdn = ? "
                                         "and a = ? "
                                         "and akv = 1");

		if (dsqlstatus == 0)
		{
			y = 0;
			for (i = 0; i< 10; i++)
			{
				if (prdk_k.prodphase[i] == 'J')
				{
					prodphase[y] = i+1; y++;
				}
			}
			
		}



		sprintf (dname, "%s\\parafile", tmp);
		pa = fopen (dname, "w");
		if (pa == NULL) return;


        if (WiegenAktiv == 0) eti_netto = ratod (aufps.lief_me);
		fprintf (pa, "$$ARTNR;%.0lf\n",  _a_bas.a);
		fprintf (pa, "$$ABEZ1;%s\n",     _a_bas.a_bz1);
		fprintf (pa, "$$ABEZ2;%s\n",     _a_bas.a_bz2);
		fprintf (pa, "$$a_bas_erw.pp_a_bz1;%s\n", a_bas_erw.pp_a_bz1);
		fprintf (pa, "$$a_bas_erw.pp_a_bz2;%s\n", a_bas_erw.pp_a_bz2);
		fprintf (pa, "$$XNETTOGEW;%.3lf\n",  eti_netto);
		fprintf (pa, "$$ZEIGEMHD;%s\n",  "J");    //????????????

			  sprintf (aufps.auf_pr_vk,  "%8.2lf", pr_ek);
			  sprintf (aufps.auf_lad_pr, "%8.2lf", pr_vk);


		fprintf (pa, "$$LSVKEURO;%.2lf\n",  pr_ek); 
		fprintf (pa, "$$LSLADEURO;%.2lf\n",  pr_vk);
		fprintf (pa, "$$LADENWERT;%.2lf\n",  pr_vk * eti_netto );

		fprintf (pa, "$$CHARGE;%s\n",       aufps.partie);
		fprintf (pa, "$$PRODPHASE1.NR;%d\n",   prodphase[0]);
		fprintf (pa, "$$PRODPHASE2.NR;%d\n",   prodphase[1]);
		fprintf (pa, "$$PRODPHASE3.NR;%d\n",   prodphase[2]);
		fprintf (pa, "$$PRODPHASE4.NR;%d\n",   prodphase[3]);
		fprintf (pa, "$$PRODPHASE5.NR;%d\n",   prodphase[4]);

		if (prodphase[0] > 0)
		{
			sprintf(ptabn.ptwert, "%d",prodphase[0]); 
			if (ptab_class.lese_ptab ("prodphase", ptabn.ptwert) == 0)
			{
				fprintf (pa, "$$PRODPHASE1.BEZK;%s\n",   ptabn.ptbezk);
				fprintf (pa, "$$PRODPHASE1.BEZ;%s\n",   ptabn.ptbez);
			}
			else
			{
				fprintf (pa, "$$PRODPHASE1.BEZK;%s\n",   "-nicht angelegt-");
				fprintf (pa, "$$PRODPHASE1.BEZ;%s\n",   "-nicht angelegt-");
			}
		}
		else
		{
				fprintf (pa, "$$PRODPHASE1.BEZK;%s\n",   "");
				fprintf (pa, "$$PRODPHASE1.BEZ;%s\n",   "");
		}

		if (prodphase[1] > 0)
		{
			sprintf(ptabn.ptwert, "%d",prodphase[1]); 
			if (ptab_class.lese_ptab ("prodphase", ptabn.ptwert) == 0)
			{
				fprintf (pa, "$$PRODPHASE2.BEZK;%s\n",   ptabn.ptbezk);
				fprintf (pa, "$$PRODPHASE2.BEZ;%s\n",   ptabn.ptbez);
			}
			else
			{
				fprintf (pa, "$$PRODPHASE2.BEZK;%s\n",   "-nicht angelegt-");
				fprintf (pa, "$$PRODPHASE2.BEZ;%s\n",   "-nicht angelegt-");
			}
		}
		else
		{
				fprintf (pa, "$$PRODPHASE2.BEZK;%s\n",   "");
				fprintf (pa, "$$PRODPHASE2.BEZ;%s\n",   "");
		}

		if (prodphase[2] > 0)
		{
			sprintf(ptabn.ptwert, "%d",prodphase[2]); 
			if (ptab_class.lese_ptab ("prodphase", ptabn.ptwert) == 0)
			{
				fprintf (pa, "$$PRODPHASE3.BEZK;%s\n",   ptabn.ptbezk);
				fprintf (pa, "$$PRODPHASE3.BEZ;%s\n",   ptabn.ptbez);
			}
			else
			{
				fprintf (pa, "$$PRODPHASE3.BEZK;%s\n",   "-nicht angelegt-");
				fprintf (pa, "$$PRODPHASE3.BEZ;%s\n",   "-nicht angelegt-");
			}
		}
		else
		{
				fprintf (pa, "$$PRODPHASE3.BEZK;%s\n",   "");
				fprintf (pa, "$$PRODPHASE3.BEZ;%s\n",   "");
		}

		if (prodphase[3] > 0)
		{
			sprintf(ptabn.ptwert, "%d",prodphase[3]); 
			if (ptab_class.lese_ptab ("prodphase", ptabn.ptwert) == 0)
			{
				fprintf (pa, "$$PRODPHASE4.BEZK;%s\n",   ptabn.ptbezk);
				fprintf (pa, "$$PRODPHASE4.BEZ;%s\n",   ptabn.ptbez);
			}
			else
			{
				fprintf (pa, "$$PRODPHASE4.BEZK;%s\n",   "-nicht angelegt-");
				fprintf (pa, "$$PRODPHASE4.BEZ;%s\n",   "-nicht angelegt-");
			}
		}
		else
		{
				fprintf (pa, "$$PRODPHASE4.BEZK;%s\n",   "");
				fprintf (pa, "$$PRODPHASE4.BEZ;%s\n",   "");
		}

		if (prodphase[4] > 0)
		{
			sprintf(ptabn.ptwert, "%d",prodphase[4]); 
			if (ptab_class.lese_ptab ("prodphase", ptabn.ptwert) == 0)
			{
				fprintf (pa, "$$PRODPHASE5.BEZK;%s\n",   ptabn.ptbezk);
				fprintf (pa, "$$PRODPHASE5.BEZ;%s\n",   ptabn.ptbez);
			}
			else
			{
				fprintf (pa, "$$PRODPHASE5.BEZK;%s\n",   "-nicht angelegt-");
				fprintf (pa, "$$PRODPHASE5.BEZ;%s\n",   "-nicht angelegt-");
			}
		}
		else
		{
				fprintf (pa, "$$PRODPHASE5.BEZK;%s\n",   "");
				fprintf (pa, "$$PRODPHASE5.BEZ;%s\n",   "");
		}

		fprintf (pa, "$$LIEF;%s\n",      zerl_k.lief);
		fprintf (pa, "$$LIEFNR;%8.0lf\n",    zerl_k.partie);
		fprintf (pa, "$$WECHARGE;%s\n",     aufps.ls_charge);
		fprintf (pa, "$$DATUM;%s\n",     cdatum);
		fprintf (pa, "$$WEDAT;%s\n",     lieferdatum);
		fprintf (pa, "$$ABEZ1;%s\n",     _a_bas.a_bz1);
		fprintf (pa, "$$ABEZ2;%s\n",     _a_bas.a_bz2);
		fprintf (pa, "$$NETTOG;%.3lf\n",  eti_netto);
		fprintf (pa, "$$TARAG;%.3lf\n",   eti_tara);
		fprintf (pa, "$$BRUTTOG;%.3lf\n", eti_brutto);
		fprintf (pa, "$$STUECK;%.2lf\n",  eti_stk );
		fprintf (pa, "$$LIEFNAME;%s\n",   lief_krz1);
		fprintf (pa, "$$MHD;%s\n",        aufps.hbk_date);
		fprintf (pa, "$$KUBAMHD;%s\n",     aufps.hbk_date);
		fprintf (pa, "$$LIKUBAMHD;%s\n",   aufps.hbk_date);
		fprintf (pa, "$$NAME1;%s\n",      _adr.adr_nam1);
		fprintf (pa, "$$NAME2;%s\n",      _adr.adr_nam2);
		fprintf (pa, "$$STRASSE;%s\n",    _adr.str);
		fprintf (pa, "$$PLZ;%s\n",        _adr.plz);
		fprintf (pa, "$$ORT;%s\n",        _adr.ort1);
		fprintf (pa, "$$ANZAHL;%d\n",     etianz);
		fprintf (pa, "$$WIEGANZ;%ld\n",  atol (wiegzahl));


		/*
		 * Neuer Systemparameter zutat_at
		 * zutat_at_par <-- Systemparameter zutat_at
		 * zutat_at     <-- 53600X.cfg Parameter zutat_at
		 */
    	memset( atexte.txt, '\0', sizeof( atexte.txt ) );
		if ( zutat_at_par == 1 )
		{
			memcpy( atexte.txt, a_bas_erw.zutat, sizeof (atexte.txt) );
			atexte.txt[sizeof(atexte.txt)-1] = '\0';
		}
		else if ( zutat_at_par == 2 )
		{
			lese_a_bas_erw2 ( _a_bas.a_grund );
			memcpy( atexte.txt, a_bas_erw2.zutat, sizeof (atexte.txt) );
			atexte.txt[sizeof(atexte.txt)-1] = '\0';
		}
		else
		{
			switch ( zutat_at )
			{
			case 1: atexte_class.lese_atexte( _a_bas.txt_nr1 ); break;
			case 2: atexte_class.lese_atexte( _a_bas.txt_nr2 ); break;
			case 3: atexte_class.lese_atexte( _a_bas.txt_nr3 ); break;
			case 'a': atexte_class.lese_atexte_a( _a_bas.a ); break;  /*  GK 05.08.2013  */
			}
		}

		int ZeilenAnz =  HoleZeilenAnzahl (atexte.txt, sizeof (atexte.txt)) ; //WAL-148
		if (ZeilenAnz > 0)
		{
			fprintf (pa, "$$ATEXT01;%s\n",  wort[0] );
			if (ZeilenAnz > 1) fprintf (pa, "$$ATEXT02;%s\n",  wort[1] );
			if (ZeilenAnz > 2) fprintf (pa, "$$ATEXT03;%s\n",  wort[2] );
			if (ZeilenAnz > 3) fprintf (pa, "$$ATEXT04;%s\n",  wort[3] );
			if (ZeilenAnz > 4) fprintf (pa, "$$ATEXT05;%s\n",  wort[4] );
			if (ZeilenAnz > 5) fprintf (pa, "$$ATEXT06;%s\n",  wort[5] );
			if (ZeilenAnz > 6) fprintf (pa, "$$ATEXT07;%s\n",  wort[6] );
			if (ZeilenAnz > 7) fprintf (pa, "$$ATEXT08;%s\n",  wort[7] );
			if (ZeilenAnz > 8) fprintf (pa, "$$ATEXT09;%s\n",  wort[8] );
			if (ZeilenAnz > 9) fprintf (pa, "$$ATEXT010;%s\n",  wort[9] );
		}
		else
		{
			EntferneZeilenende (atexte.txt, sizeof (atexte.txt)) ; //WAL-148
			atexte.txt[900] = '\0'; fprintf (pa, "$$ATEXT10;%s\n",  atexte.txt + 810 );
			atexte.txt[810] = '\0'; fprintf (pa, "$$ATEXT09;%s\n",  atexte.txt + 720 );
			atexte.txt[720] = '\0'; fprintf (pa, "$$ATEXT08;%s\n",  atexte.txt + 630 );
			atexte.txt[630] = '\0'; fprintf (pa, "$$ATEXT07;%s\n",  atexte.txt + 540 );
			atexte.txt[540] = '\0'; fprintf (pa, "$$ATEXT06;%s\n",  atexte.txt + 450 );
			atexte.txt[450] = '\0'; fprintf (pa, "$$ATEXT05;%s\n",  atexte.txt + 360 );
			atexte.txt[360] = '\0'; fprintf (pa, "$$ATEXT04;%s\n",  atexte.txt + 270 );
			atexte.txt[270] = '\0'; fprintf (pa, "$$ATEXT03;%s\n",  atexte.txt + 180 );
			atexte.txt[180] = '\0'; fprintf (pa, "$$ATEXT02;%s\n",  atexte.txt + 90 );
			atexte.txt[ 90] = '\0'; fprintf (pa, "$$ATEXT01;%s\n",  atexte.txt );
			// 20120603 GK bis hier. // 20121017 Wegen etubi Schnipsell�nge von 100 auf 90 runtergesetzt
		}

		/* 22.04.2013 GK Allergene und Naehrwerte*/ 
		fprintf( pa, "$$GLUTENE;%s\n", _a_bas.glg );
		fprintf( pa, "$$KREBSE;%s\n", _a_bas.krebs );
		fprintf( pa, "$$EIER;%s\n", _a_bas.ei );
		fprintf( pa, "$$FISCHE;%s\n", _a_bas.fisch );
		fprintf( pa, "$$ERDNUSS;%s\n", _a_bas.erdnuss );
		fprintf( pa, "$$SOJA;%s\n", _a_bas.soja );
		fprintf( pa, "$$MILCH;%s\n", _a_bas.milch );
		fprintf( pa, "$$SCHALENFR;%s\n", _a_bas.schal );
		fprintf( pa, "$$SELLERIE;%s\n", _a_bas.sellerie );
		fprintf( pa, "$$SENFSAAT;%s\n", _a_bas.senfsaat );
		fprintf( pa, "$$SESAMSAMEN;%s\n", _a_bas.sesamsamen );
		fprintf( pa, "$$SULFIT;%s\n", _a_bas.sulfit );
		fprintf( pa, "$$WEICHTIERE;%s\n", _a_bas.weichtier );
		fprintf( pa, "$$BRENNWERT;%s\n", _a_bas.brennwert );
		fprintf( pa, "$$EIWEISS;%6.2lf\n", _a_bas.eiweiss );
		fprintf( pa, "$$KHYDRAT;%6.2lf\n", _a_bas.kh );
		fprintf( pa, "$$FETT;%6.2lf\n", _a_bas.fett );


		fprintf( pa, "$$SONSTIGE;%s\n", _a_bas.sonstige );
		fprintf( pa, "$$GRUENER_PUNKT;%s\n", _a_bas.gruener_punkt );
		fprintf( pa, "$$VAKUUMIERT;%s\n", _a_bas.vakuumiert );


		fprintf( pa, "$$a_bas_erw.pp_a_bz1;%s\n", a_bas_erw.pp_a_bz1 );
		fprintf( pa, "$$a_bas_erw.pp_a_bz2;%s\n", a_bas_erw.pp_a_bz2 );
		fprintf( pa, "$$a_bas_erw.lgr_tmpr;%s\n", a_bas_erw.lgr_tmpr );
		fprintf( pa, "$$a_bas_erw.lupine;%s\n", a_bas_erw.lupine );
		fprintf( pa, "$$a_bas_erw.schutzgas;%s\n", a_bas_erw.schutzgas );
		fprintf( pa, "$$a_bas_erw.salz;%8.3lf\n", a_bas_erw.salz );
		fprintf( pa, "$$a_bas_erw.davonfett;%8.3lf\n", a_bas_erw.davonfett );
		fprintf( pa, "$$a_bas_erw.davonzucker;%8.3lf\n", a_bas_erw.davonzucker );
		fprintf( pa, "$$a_bas_erw.ballaststoffe;%8.3lf\n", a_bas_erw.ballaststoffe );
		fprintf( pa, "$$a_bas_erw.userdef1;%hd\n", a_bas_erw.userdef1 );
		fprintf( pa, "$$a_bas_erw.userdef2;%hd\n", a_bas_erw.userdef2 );
		fprintf( pa, "$$a_bas_erw.userdef3;%hd\n", a_bas_erw.userdef3 );



		fprintf (pa, "$$ALLGTEXTE.allgtxt_nr1;%ld\n",  _a_bas.allgtxt_nr1 );
		fprintf (pa, "$$ALLGTEXTE.allgtxt_nr2;%ld\n",  _a_bas.allgtxt_nr2 );
		fprintf (pa, "$$ALLGTEXTE.allgtxt_nr3;%ld\n",  _a_bas.allgtxt_nr3 );
		fprintf (pa, "$$ALLGTEXTE.allgtxt_nr4;%ld\n",  _a_bas.allgtxt_nr4 );
		fprintf (pa, "$$ALLGTEXTE.allgtxt_nr5;%ld\n",  _a_bas.txt_nr1 );
		fprintf (pa, "$$ALLGTEXTE.allgtxt_nr6;%ld\n",  _a_bas.txt_nr2 );
		fprintf (pa, "$$ALLGTEXTE.allgtxt_nr7;%ld\n",  _a_bas.txt_nr3 );
		fprintf (pa, "$$ALLGTEXTE.allgtxt_nr8;%ld\n",  _a_bas.txt_nr4 );
		fprintf (pa, "$$ALLGTEXTE.allgtxt_nr9;%ld\n",  _a_bas.txt_nr5 );
		fprintf (pa, "$$ALLGTEXTE.allgtxt_nr10;%ld\n",  _a_bas.txt_nr6 );
		fprintf (pa, "$$ALLGTEXTE.allgtxt_nr11;%ld\n",  _a_bas.txt_nr7 );
		fprintf (pa, "$$ALLGTEXTE.allgtxt_nr12;%ld\n",  _a_bas.txt_nr8 );
		fprintf (pa, "$$ALLGTEXTE.allgtxt_nr13;%ld\n",  _a_bas.txt_nr9 );
		fprintf (pa, "$$ALLGTEXTE.allgtxt_nr14;%ld\n",  _a_bas.txt_nr10 );
    	memset( atexte.disp_txt, '\0', sizeof( atexte.disp_txt ) );
		atexte_class.lese_atexte( _a_bas.allgtxt_nr1 );
		ZeilenAnz =  HoleZeilenAnzahl (atexte.disp_txt, sizeof (atexte.disp_txt)) ; //WAL-148
		if (ZeilenAnz > 0) 
		{
			fprintf (pa, "$$ALLGTEXTE.1.01;%s\n",  wort[0] );
			if (ZeilenAnz > 1) fprintf (pa, "$$ALLGTEXTE.1.02;%s\n",  wort[1] );
			if (ZeilenAnz > 2) fprintf (pa, "$$ALLGTEXTE.1.03;%s\n",  wort[2] );
			if (ZeilenAnz > 3) fprintf (pa, "$$ALLGTEXTE.1.04;%s\n",  wort[3] );
			if (ZeilenAnz > 4) fprintf (pa, "$$ALLGTEXTE.1.05;%s\n",  wort[4] );
			if (ZeilenAnz > 5) fprintf (pa, "$$ALLGTEXTE.1.06;%s\n",  wort[5] );
			if (ZeilenAnz > 6) fprintf (pa, "$$ALLGTEXTE.1.07;%s\n",  wort[6] );
			if (ZeilenAnz > 7) fprintf (pa, "$$ALLGTEXTE.1.08;%s\n",  wort[7] );
			if (ZeilenAnz > 8) fprintf (pa, "$$ALLGTEXTE.1.09;%s\n",  wort[8] );
			if (ZeilenAnz > 9) fprintf (pa, "$$ALLGTEXTE.1.10;%s\n",  wort[9] );
		}
		else
		{
			EntferneZeilenende (atexte.disp_txt, sizeof (atexte.disp_txt)); //WAL-148
			atexte.disp_txt[900] = '\0'; fprintf (pa, "$$ALLGTEXTE.1.10;%s\n",  atexte.disp_txt + 810 );
			atexte.disp_txt[810] = '\0'; fprintf (pa, "$$ALLGTEXTE.1.09;%s\n",  atexte.disp_txt + 720 );
			atexte.disp_txt[720] = '\0'; fprintf (pa, "$$ALLGTEXTE.1.08;%s\n",  atexte.disp_txt + 630 );
			atexte.disp_txt[630] = '\0'; fprintf (pa, "$$ALLGTEXTE.1.07;%s\n",  atexte.disp_txt + 540 );
			atexte.disp_txt[540] = '\0'; fprintf (pa, "$$ALLGTEXTE.1.06;%s\n",  atexte.disp_txt + 450 );
			atexte.disp_txt[450] = '\0'; fprintf (pa, "$$ALLGTEXTE.1.05;%s\n",  atexte.disp_txt + 360 );
			atexte.disp_txt[360] = '\0'; fprintf (pa, "$$ALLGTEXTE.1.04;%s\n",  atexte.disp_txt + 270 );
			atexte.disp_txt[270] = '\0'; fprintf (pa, "$$ALLGTEXTE.1.03;%s\n",  atexte.disp_txt + 180 );
			atexte.disp_txt[180] = '\0'; fprintf (pa, "$$ALLGTEXTE.1.02;%s\n",  atexte.disp_txt + 90 );
			atexte.disp_txt[ 90] = '\0'; fprintf (pa, "$$ALLGTEXTE.1.01;%s\n",  atexte.disp_txt );
		}
    	memset( atexte.disp_txt, '\0', sizeof( atexte.disp_txt ) );
		atexte_class.lese_atexte( _a_bas.allgtxt_nr2 );
		ZeilenAnz =  HoleZeilenAnzahl (atexte.disp_txt, sizeof (atexte.disp_txt)) ; //WAL-148
		if (ZeilenAnz > 0) 
		{
			fprintf (pa, "$$ALLGTEXTE.2.01;%s\n",  wort[0] );
			if (ZeilenAnz > 1) fprintf (pa, "$$ALLGTEXTE.2.02;%s\n",  wort[1] );
			if (ZeilenAnz > 2) fprintf (pa, "$$ALLGTEXTE.2.03;%s\n",  wort[2] );
			if (ZeilenAnz > 3) fprintf (pa, "$$ALLGTEXTE.2.04;%s\n",  wort[3] );
			if (ZeilenAnz > 4) fprintf (pa, "$$ALLGTEXTE.2.05;%s\n",  wort[4] );
			if (ZeilenAnz > 5) fprintf (pa, "$$ALLGTEXTE.2.06;%s\n",  wort[5] );
			if (ZeilenAnz > 6) fprintf (pa, "$$ALLGTEXTE.2.07;%s\n",  wort[6] );
			if (ZeilenAnz > 7) fprintf (pa, "$$ALLGTEXTE.2.08;%s\n",  wort[7] );
			if (ZeilenAnz > 8) fprintf (pa, "$$ALLGTEXTE.2.09;%s\n",  wort[8] );
			if (ZeilenAnz > 9) fprintf (pa, "$$ALLGTEXTE.2.10;%s\n",  wort[9] );
		}
		else
		{
			EntferneZeilenende (atexte.disp_txt, sizeof (atexte.disp_txt)); //WAL-148
			atexte.disp_txt[900] = '\0'; fprintf (pa, "$$ALLGTEXTE.2.10;%s\n",  atexte.disp_txt + 810 );
			atexte.disp_txt[810] = '\0'; fprintf (pa, "$$ALLGTEXTE.2.09;%s\n",  atexte.disp_txt + 720 );
			atexte.disp_txt[720] = '\0'; fprintf (pa, "$$ALLGTEXTE.2.08;%s\n",  atexte.disp_txt + 630 );
			atexte.disp_txt[630] = '\0'; fprintf (pa, "$$ALLGTEXTE.2.07;%s\n",  atexte.disp_txt + 540 );
			atexte.disp_txt[540] = '\0'; fprintf (pa, "$$ALLGTEXTE.2.06;%s\n",  atexte.disp_txt + 450 );
			atexte.disp_txt[450] = '\0'; fprintf (pa, "$$ALLGTEXTE.2.05;%s\n",  atexte.disp_txt + 360 );
			atexte.disp_txt[360] = '\0'; fprintf (pa, "$$ALLGTEXTE.2.04;%s\n",  atexte.disp_txt + 270 );
			atexte.disp_txt[270] = '\0'; fprintf (pa, "$$ALLGTEXTE.2.03;%s\n",  atexte.disp_txt + 180 );
			atexte.disp_txt[180] = '\0'; fprintf (pa, "$$ALLGTEXTE.2.02;%s\n",  atexte.disp_txt + 90 );
			atexte.disp_txt[ 90] = '\0'; fprintf (pa, "$$ALLGTEXTE.2.01;%s\n",  atexte.disp_txt );
		}
    	memset( atexte.disp_txt, '\0', sizeof( atexte.disp_txt ) );
		atexte_class.lese_atexte( _a_bas.allgtxt_nr3 );
		ZeilenAnz =  HoleZeilenAnzahl (atexte.disp_txt, sizeof (atexte.disp_txt)) ; //WAL-148
		if (ZeilenAnz > 0) 
		{
			fprintf (pa, "$$ALLGTEXTE.3.01;%s\n",  wort[0] );
			if (ZeilenAnz > 1) fprintf (pa, "$$ALLGTEXTE.3.02;%s\n",  wort[1] );
			if (ZeilenAnz > 2) fprintf (pa, "$$ALLGTEXTE.3.03;%s\n",  wort[2] );
			if (ZeilenAnz > 3) fprintf (pa, "$$ALLGTEXTE.3.04;%s\n",  wort[3] );
			if (ZeilenAnz > 4) fprintf (pa, "$$ALLGTEXTE.3.05;%s\n",  wort[4] );
			if (ZeilenAnz > 5) fprintf (pa, "$$ALLGTEXTE.3.06;%s\n",  wort[5] );
			if (ZeilenAnz > 6) fprintf (pa, "$$ALLGTEXTE.3.07;%s\n",  wort[6] );
			if (ZeilenAnz > 7) fprintf (pa, "$$ALLGTEXTE.3.08;%s\n",  wort[7] );
			if (ZeilenAnz > 8) fprintf (pa, "$$ALLGTEXTE.3.09;%s\n",  wort[8] );
			if (ZeilenAnz > 9) fprintf (pa, "$$ALLGTEXTE.3.10;%s\n",  wort[9] );
		}
		else
		{
			EntferneZeilenende (atexte.disp_txt, sizeof (atexte.disp_txt)); //WAL-148
			atexte.disp_txt[900] = '\0'; fprintf (pa, "$$ALLGTEXTE.3.10;%s\n",  atexte.disp_txt + 810 );
			atexte.disp_txt[810] = '\0'; fprintf (pa, "$$ALLGTEXTE.3.09;%s\n",  atexte.disp_txt + 720 );
			atexte.disp_txt[720] = '\0'; fprintf (pa, "$$ALLGTEXTE.3.08;%s\n",  atexte.disp_txt + 630 );
			atexte.disp_txt[630] = '\0'; fprintf (pa, "$$ALLGTEXTE.3.07;%s\n",  atexte.disp_txt + 540 );
			atexte.disp_txt[540] = '\0'; fprintf (pa, "$$ALLGTEXTE.3.06;%s\n",  atexte.disp_txt + 450 );
			atexte.disp_txt[450] = '\0'; fprintf (pa, "$$ALLGTEXTE.3.05;%s\n",  atexte.disp_txt + 360 );
			atexte.disp_txt[360] = '\0'; fprintf (pa, "$$ALLGTEXTE.3.04;%s\n",  atexte.disp_txt + 270 );
			atexte.disp_txt[270] = '\0'; fprintf (pa, "$$ALLGTEXTE.3.03;%s\n",  atexte.disp_txt + 180 );
			atexte.disp_txt[180] = '\0'; fprintf (pa, "$$ALLGTEXTE.3.02;%s\n",  atexte.disp_txt + 90 );
			atexte.disp_txt[ 90] = '\0'; fprintf (pa, "$$ALLGTEXTE.3.01;%s\n",  atexte.disp_txt );
		}
    	memset( atexte.disp_txt, '\0', sizeof( atexte.disp_txt ) );
		atexte_class.lese_atexte( _a_bas.allgtxt_nr4 );
		ZeilenAnz =  HoleZeilenAnzahl (atexte.disp_txt, sizeof (atexte.disp_txt)) ; //WAL-148
		if (ZeilenAnz > 0) 
		{
			fprintf (pa, "$$ALLGTEXTE.4.01;%s\n",  wort[0] );
			if (ZeilenAnz > 1) fprintf (pa, "$$ALLGTEXTE.4.02;%s\n",  wort[1] );
			if (ZeilenAnz > 2) fprintf (pa, "$$ALLGTEXTE.4.03;%s\n",  wort[2] );
			if (ZeilenAnz > 3) fprintf (pa, "$$ALLGTEXTE.4.04;%s\n",  wort[3] );
			if (ZeilenAnz > 4) fprintf (pa, "$$ALLGTEXTE.4.05;%s\n",  wort[4] );
			if (ZeilenAnz > 5) fprintf (pa, "$$ALLGTEXTE.4.06;%s\n",  wort[5] );
			if (ZeilenAnz > 6) fprintf (pa, "$$ALLGTEXTE.4.07;%s\n",  wort[6] );
			if (ZeilenAnz > 7) fprintf (pa, "$$ALLGTEXTE.4.08;%s\n",  wort[7] );
			if (ZeilenAnz > 8) fprintf (pa, "$$ALLGTEXTE.4.09;%s\n",  wort[8] );
			if (ZeilenAnz > 9) fprintf (pa, "$$ALLGTEXTE.4.10;%s\n",  wort[9] );
		}
		else
		{
			EntferneZeilenende (atexte.disp_txt, sizeof (atexte.disp_txt)); //WAL-148
			atexte.disp_txt[900] = '\0'; fprintf (pa, "$$ALLGTEXTE.4.10;%s\n",  atexte.disp_txt + 810 );
			atexte.disp_txt[810] = '\0'; fprintf (pa, "$$ALLGTEXTE.4.09;%s\n",  atexte.disp_txt + 720 );
			atexte.disp_txt[720] = '\0'; fprintf (pa, "$$ALLGTEXTE.4.08;%s\n",  atexte.disp_txt + 630 );
			atexte.disp_txt[630] = '\0'; fprintf (pa, "$$ALLGTEXTE.4.07;%s\n",  atexte.disp_txt + 540 );
			atexte.disp_txt[540] = '\0'; fprintf (pa, "$$ALLGTEXTE.4.06;%s\n",  atexte.disp_txt + 450 );
			atexte.disp_txt[450] = '\0'; fprintf (pa, "$$ALLGTEXTE.4.05;%s\n",  atexte.disp_txt + 360 );
			atexte.disp_txt[360] = '\0'; fprintf (pa, "$$ALLGTEXTE.4.04;%s\n",  atexte.disp_txt + 270 );
			atexte.disp_txt[270] = '\0'; fprintf (pa, "$$ALLGTEXTE.4.03;%s\n",  atexte.disp_txt + 180 );
			atexte.disp_txt[180] = '\0'; fprintf (pa, "$$ALLGTEXTE.4.02;%s\n",  atexte.disp_txt + 90 );
			atexte.disp_txt[ 90] = '\0'; fprintf (pa, "$$ALLGTEXTE.4.01;%s\n",  atexte.disp_txt );
		}


    	memset( atexte.disp_txt, '\0', sizeof( atexte.disp_txt ) );
		atexte_class.lese_atexte( _a_bas.txt_nr1 );
		ZeilenAnz =  HoleZeilenAnzahl (atexte.disp_txt, sizeof (atexte.disp_txt)) ; //WAL-148
		if (ZeilenAnz > 0) 
		{
			fprintf (pa, "$$ALLGTEXTE.5.01;%s\n",  wort[0] );
			if (ZeilenAnz > 1) fprintf (pa, "$$ALLGTEXTE.5.02;%s\n",  wort[1] );
			if (ZeilenAnz > 2) fprintf (pa, "$$ALLGTEXTE.5.03;%s\n",  wort[2] );
			if (ZeilenAnz > 3) fprintf (pa, "$$ALLGTEXTE.5.04;%s\n",  wort[3] );
			if (ZeilenAnz > 4) fprintf (pa, "$$ALLGTEXTE.5.05;%s\n",  wort[4] );
			if (ZeilenAnz > 5) fprintf (pa, "$$ALLGTEXTE.5.06;%s\n",  wort[5] );
			if (ZeilenAnz > 6) fprintf (pa, "$$ALLGTEXTE.5.07;%s\n",  wort[6] );
			if (ZeilenAnz > 7) fprintf (pa, "$$ALLGTEXTE.5.08;%s\n",  wort[7] );
			if (ZeilenAnz > 8) fprintf (pa, "$$ALLGTEXTE.5.09;%s\n",  wort[8] );
			if (ZeilenAnz > 9) fprintf (pa, "$$ALLGTEXTE.5.10;%s\n",  wort[9] );
		}
		else
		{
			EntferneZeilenende (atexte.disp_txt, sizeof (atexte.disp_txt)); //WAL-148
			atexte.disp_txt[900] = '\0'; fprintf (pa, "$$ALLGTEXTE.5.10;%s\n",  atexte.disp_txt + 810 );
			atexte.disp_txt[810] = '\0'; fprintf (pa, "$$ALLGTEXTE.5.09;%s\n",  atexte.disp_txt + 720 );
			atexte.disp_txt[720] = '\0'; fprintf (pa, "$$ALLGTEXTE.5.08;%s\n",  atexte.disp_txt + 630 );
			atexte.disp_txt[630] = '\0'; fprintf (pa, "$$ALLGTEXTE.5.07;%s\n",  atexte.disp_txt + 540 );
			atexte.disp_txt[540] = '\0'; fprintf (pa, "$$ALLGTEXTE.5.06;%s\n",  atexte.disp_txt + 450 );
			atexte.disp_txt[450] = '\0'; fprintf (pa, "$$ALLGTEXTE.5.05;%s\n",  atexte.disp_txt + 360 );
			atexte.disp_txt[360] = '\0'; fprintf (pa, "$$ALLGTEXTE.5.04;%s\n",  atexte.disp_txt + 270 );
			atexte.disp_txt[270] = '\0'; fprintf (pa, "$$ALLGTEXTE.5.03;%s\n",  atexte.disp_txt + 180 );
			atexte.disp_txt[180] = '\0'; fprintf (pa, "$$ALLGTEXTE.5.02;%s\n",  atexte.disp_txt + 90 );
			atexte.disp_txt[ 90] = '\0'; fprintf (pa, "$$ALLGTEXTE.5.01;%s\n",  atexte.disp_txt );
		}

    	memset( atexte.disp_txt, '\0', sizeof( atexte.disp_txt ) );
		atexte_class.lese_atexte( _a_bas.txt_nr2 );
		ZeilenAnz =  HoleZeilenAnzahl (atexte.disp_txt, sizeof (atexte.disp_txt)) ; //WAL-148
		if (ZeilenAnz > 0) 
		{
			fprintf (pa, "$$ALLGTEXTE.6.01;%s\n",  wort[0] );
			if (ZeilenAnz > 1) fprintf (pa, "$$ALLGTEXTE.6.02;%s\n",  wort[1] );
			if (ZeilenAnz > 2) fprintf (pa, "$$ALLGTEXTE.6.03;%s\n",  wort[2] );
			if (ZeilenAnz > 3) fprintf (pa, "$$ALLGTEXTE.6.04;%s\n",  wort[3] );
			if (ZeilenAnz > 4) fprintf (pa, "$$ALLGTEXTE.6.05;%s\n",  wort[4] );
			if (ZeilenAnz > 5) fprintf (pa, "$$ALLGTEXTE.6.06;%s\n",  wort[5] );
			if (ZeilenAnz > 6) fprintf (pa, "$$ALLGTEXTE.6.07;%s\n",  wort[6] );
			if (ZeilenAnz > 7) fprintf (pa, "$$ALLGTEXTE.6.08;%s\n",  wort[7] );
			if (ZeilenAnz > 8) fprintf (pa, "$$ALLGTEXTE.6.09;%s\n",  wort[8] );
			if (ZeilenAnz > 9) fprintf (pa, "$$ALLGTEXTE.6.10;%s\n",  wort[9] );
		}
		else
		{
			EntferneZeilenende (atexte.disp_txt, sizeof (atexte.disp_txt)); //WAL-148
			atexte.disp_txt[900] = '\0'; fprintf (pa, "$$ALLGTEXTE.6.10;%s\n",  atexte.disp_txt + 810 );
			atexte.disp_txt[810] = '\0'; fprintf (pa, "$$ALLGTEXTE.6.09;%s\n",  atexte.disp_txt + 720 );
			atexte.disp_txt[720] = '\0'; fprintf (pa, "$$ALLGTEXTE.6.08;%s\n",  atexte.disp_txt + 630 );
			atexte.disp_txt[630] = '\0'; fprintf (pa, "$$ALLGTEXTE.6.07;%s\n",  atexte.disp_txt + 540 );
			atexte.disp_txt[540] = '\0'; fprintf (pa, "$$ALLGTEXTE.6.06;%s\n",  atexte.disp_txt + 450 );
			atexte.disp_txt[450] = '\0'; fprintf (pa, "$$ALLGTEXTE.6.05;%s\n",  atexte.disp_txt + 360 );
			atexte.disp_txt[360] = '\0'; fprintf (pa, "$$ALLGTEXTE.6.04;%s\n",  atexte.disp_txt + 270 );
			atexte.disp_txt[270] = '\0'; fprintf (pa, "$$ALLGTEXTE.6.03;%s\n",  atexte.disp_txt + 180 );
			atexte.disp_txt[180] = '\0'; fprintf (pa, "$$ALLGTEXTE.6.02;%s\n",  atexte.disp_txt + 90 );
			atexte.disp_txt[ 90] = '\0'; fprintf (pa, "$$ALLGTEXTE.6.01;%s\n",  atexte.disp_txt );
		}

    	memset( atexte.disp_txt, '\0', sizeof( atexte.disp_txt ) );
		atexte_class.lese_atexte( _a_bas.txt_nr3 );
		ZeilenAnz =  HoleZeilenAnzahl (atexte.disp_txt, sizeof (atexte.disp_txt)) ; //WAL-148
		if (ZeilenAnz > 0) 
		{
			fprintf (pa, "$$ALLGTEXTE.7.01;%s\n",  wort[0] );
			if (ZeilenAnz > 1) fprintf (pa, "$$ALLGTEXTE.7.02;%s\n",  wort[1] );
			if (ZeilenAnz > 2) fprintf (pa, "$$ALLGTEXTE.7.03;%s\n",  wort[2] );
			if (ZeilenAnz > 3) fprintf (pa, "$$ALLGTEXTE.7.04;%s\n",  wort[3] );
			if (ZeilenAnz > 4) fprintf (pa, "$$ALLGTEXTE.7.05;%s\n",  wort[4] );
			if (ZeilenAnz > 5) fprintf (pa, "$$ALLGTEXTE.7.06;%s\n",  wort[5] );
			if (ZeilenAnz > 6) fprintf (pa, "$$ALLGTEXTE.7.07;%s\n",  wort[6] );
			if (ZeilenAnz > 7) fprintf (pa, "$$ALLGTEXTE.7.08;%s\n",  wort[7] );
			if (ZeilenAnz > 8) fprintf (pa, "$$ALLGTEXTE.7.09;%s\n",  wort[8] );
			if (ZeilenAnz > 9) fprintf (pa, "$$ALLGTEXTE.7.10;%s\n",  wort[9] );
		}
		else
		{
			EntferneZeilenende (atexte.disp_txt, sizeof (atexte.disp_txt)); //WAL-148
			atexte.disp_txt[900] = '\0'; fprintf (pa, "$$ALLGTEXTE.7.10;%s\n",  atexte.disp_txt + 810 );
			atexte.disp_txt[810] = '\0'; fprintf (pa, "$$ALLGTEXTE.7.09;%s\n",  atexte.disp_txt + 720 );
			atexte.disp_txt[720] = '\0'; fprintf (pa, "$$ALLGTEXTE.7.08;%s\n",  atexte.disp_txt + 630 );
			atexte.disp_txt[630] = '\0'; fprintf (pa, "$$ALLGTEXTE.7.07;%s\n",  atexte.disp_txt + 540 );
			atexte.disp_txt[540] = '\0'; fprintf (pa, "$$ALLGTEXTE.7.06;%s\n",  atexte.disp_txt + 450 );
			atexte.disp_txt[450] = '\0'; fprintf (pa, "$$ALLGTEXTE.7.05;%s\n",  atexte.disp_txt + 360 );
			atexte.disp_txt[360] = '\0'; fprintf (pa, "$$ALLGTEXTE.7.04;%s\n",  atexte.disp_txt + 270 );
			atexte.disp_txt[270] = '\0'; fprintf (pa, "$$ALLGTEXTE.7.03;%s\n",  atexte.disp_txt + 180 );
			atexte.disp_txt[180] = '\0'; fprintf (pa, "$$ALLGTEXTE.7.02;%s\n",  atexte.disp_txt + 90 );
			atexte.disp_txt[ 90] = '\0'; fprintf (pa, "$$ALLGTEXTE.7.01;%s\n",  atexte.disp_txt );
		}

    	memset( atexte.disp_txt, '\0', sizeof( atexte.disp_txt ) );
		atexte_class.lese_atexte( _a_bas.txt_nr4 );
		ZeilenAnz =  HoleZeilenAnzahl (atexte.disp_txt, sizeof (atexte.disp_txt)) ; //WAL-148
		if (ZeilenAnz > 0) 
		{
			fprintf (pa, "$$ALLGTEXTE.8.01;%s\n",  wort[0] );
			if (ZeilenAnz > 1) fprintf (pa, "$$ALLGTEXTE.8.02;%s\n",  wort[1] );
			if (ZeilenAnz > 2) fprintf (pa, "$$ALLGTEXTE.8.03;%s\n",  wort[2] );
			if (ZeilenAnz > 3) fprintf (pa, "$$ALLGTEXTE.8.04;%s\n",  wort[3] );
			if (ZeilenAnz > 4) fprintf (pa, "$$ALLGTEXTE.8.05;%s\n",  wort[4] );
			if (ZeilenAnz > 5) fprintf (pa, "$$ALLGTEXTE.8.06;%s\n",  wort[5] );
			if (ZeilenAnz > 6) fprintf (pa, "$$ALLGTEXTE.8.07;%s\n",  wort[6] );
			if (ZeilenAnz > 7) fprintf (pa, "$$ALLGTEXTE.8.08;%s\n",  wort[7] );
			if (ZeilenAnz > 8) fprintf (pa, "$$ALLGTEXTE.8.09;%s\n",  wort[8] );
			if (ZeilenAnz > 9) fprintf (pa, "$$ALLGTEXTE.8.10;%s\n",  wort[9] );
		}
		else
		{
			EntferneZeilenende (atexte.disp_txt, sizeof (atexte.disp_txt)); //WAL-148
			atexte.disp_txt[900] = '\0'; fprintf (pa, "$$ALLGTEXTE.8.10;%s\n",  atexte.disp_txt + 810 );
			atexte.disp_txt[810] = '\0'; fprintf (pa, "$$ALLGTEXTE.8.09;%s\n",  atexte.disp_txt + 720 );
			atexte.disp_txt[720] = '\0'; fprintf (pa, "$$ALLGTEXTE.8.08;%s\n",  atexte.disp_txt + 630 );
			atexte.disp_txt[630] = '\0'; fprintf (pa, "$$ALLGTEXTE.8.07;%s\n",  atexte.disp_txt + 540 );
			atexte.disp_txt[540] = '\0'; fprintf (pa, "$$ALLGTEXTE.8.06;%s\n",  atexte.disp_txt + 450 );
			atexte.disp_txt[450] = '\0'; fprintf (pa, "$$ALLGTEXTE.8.05;%s\n",  atexte.disp_txt + 360 );
			atexte.disp_txt[360] = '\0'; fprintf (pa, "$$ALLGTEXTE.8.04;%s\n",  atexte.disp_txt + 270 );
			atexte.disp_txt[270] = '\0'; fprintf (pa, "$$ALLGTEXTE.8.03;%s\n",  atexte.disp_txt + 180 );
			atexte.disp_txt[180] = '\0'; fprintf (pa, "$$ALLGTEXTE.8.02;%s\n",  atexte.disp_txt + 90 );
			atexte.disp_txt[ 90] = '\0'; fprintf (pa, "$$ALLGTEXTE.8.01;%s\n",  atexte.disp_txt );
		}

    	memset( atexte.disp_txt, '\0', sizeof( atexte.disp_txt ) );
		atexte_class.lese_atexte( _a_bas.txt_nr5 );
		ZeilenAnz =  HoleZeilenAnzahl (atexte.disp_txt, sizeof (atexte.disp_txt)) ; //WAL-148
		if (ZeilenAnz > 0) 
		{
			fprintf (pa, "$$ALLGTEXTE.9.01;%s\n",  wort[0] );
			if (ZeilenAnz > 1) fprintf (pa, "$$ALLGTEXTE.9.02;%s\n",  wort[1] );
			if (ZeilenAnz > 2) fprintf (pa, "$$ALLGTEXTE.9.03;%s\n",  wort[2] );
			if (ZeilenAnz > 3) fprintf (pa, "$$ALLGTEXTE.9.04;%s\n",  wort[3] );
			if (ZeilenAnz > 4) fprintf (pa, "$$ALLGTEXTE.9.05;%s\n",  wort[4] );
			if (ZeilenAnz > 5) fprintf (pa, "$$ALLGTEXTE.9.06;%s\n",  wort[5] );
			if (ZeilenAnz > 6) fprintf (pa, "$$ALLGTEXTE.9.07;%s\n",  wort[6] );
			if (ZeilenAnz > 7) fprintf (pa, "$$ALLGTEXTE.9.08;%s\n",  wort[7] );
			if (ZeilenAnz > 8) fprintf (pa, "$$ALLGTEXTE.9.09;%s\n",  wort[8] );
			if (ZeilenAnz > 9) fprintf (pa, "$$ALLGTEXTE.9.10;%s\n",  wort[9] );
		}
		else
		{
			EntferneZeilenende (atexte.disp_txt, sizeof (atexte.disp_txt)); //WAL-148
			atexte.disp_txt[900] = '\0'; fprintf (pa, "$$ALLGTEXTE.9.10;%s\n",  atexte.disp_txt + 810 );
			atexte.disp_txt[810] = '\0'; fprintf (pa, "$$ALLGTEXTE.9.09;%s\n",  atexte.disp_txt + 720 );
			atexte.disp_txt[720] = '\0'; fprintf (pa, "$$ALLGTEXTE.9.08;%s\n",  atexte.disp_txt + 630 );
			atexte.disp_txt[630] = '\0'; fprintf (pa, "$$ALLGTEXTE.9.07;%s\n",  atexte.disp_txt + 540 );
			atexte.disp_txt[540] = '\0'; fprintf (pa, "$$ALLGTEXTE.9.06;%s\n",  atexte.disp_txt + 450 );
			atexte.disp_txt[450] = '\0'; fprintf (pa, "$$ALLGTEXTE.9.05;%s\n",  atexte.disp_txt + 360 );
			atexte.disp_txt[360] = '\0'; fprintf (pa, "$$ALLGTEXTE.9.04;%s\n",  atexte.disp_txt + 270 );
			atexte.disp_txt[270] = '\0'; fprintf (pa, "$$ALLGTEXTE.9.03;%s\n",  atexte.disp_txt + 180 );
			atexte.disp_txt[180] = '\0'; fprintf (pa, "$$ALLGTEXTE.9.02;%s\n",  atexte.disp_txt + 90 );
			atexte.disp_txt[ 90] = '\0'; fprintf (pa, "$$ALLGTEXTE.9.01;%s\n",  atexte.disp_txt );
		}

    	memset( atexte.disp_txt, '\0', sizeof( atexte.disp_txt ) );
		atexte_class.lese_atexte( _a_bas.txt_nr6 );
		ZeilenAnz =  HoleZeilenAnzahl (atexte.disp_txt, sizeof (atexte.disp_txt)) ; //WAL-148
		if (ZeilenAnz > 0) 
		{
			fprintf (pa, "$$ALLGTEXTE.10.01;%s\n",  wort[0] );
			if (ZeilenAnz > 1) fprintf (pa, "$$ALLGTEXTE.10.02;%s\n",  wort[1] );
			if (ZeilenAnz > 2) fprintf (pa, "$$ALLGTEXTE.10.03;%s\n",  wort[2] );
			if (ZeilenAnz > 3) fprintf (pa, "$$ALLGTEXTE.10.04;%s\n",  wort[3] );
			if (ZeilenAnz > 4) fprintf (pa, "$$ALLGTEXTE.10.05;%s\n",  wort[4] );
			if (ZeilenAnz > 5) fprintf (pa, "$$ALLGTEXTE.10.06;%s\n",  wort[5] );
			if (ZeilenAnz > 6) fprintf (pa, "$$ALLGTEXTE.10.07;%s\n",  wort[6] );
			if (ZeilenAnz > 7) fprintf (pa, "$$ALLGTEXTE.10.08;%s\n",  wort[7] );
			if (ZeilenAnz > 8) fprintf (pa, "$$ALLGTEXTE.10.09;%s\n",  wort[8] );
			if (ZeilenAnz > 9) fprintf (pa, "$$ALLGTEXTE.10.10;%s\n",  wort[9] );
		}
		else
		{
			EntferneZeilenende (atexte.disp_txt, sizeof (atexte.disp_txt)); //WAL-148
			atexte.disp_txt[900] = '\0'; fprintf (pa, "$$ALLGTEXTE.10.10;%s\n",  atexte.disp_txt + 810 );
			atexte.disp_txt[810] = '\0'; fprintf (pa, "$$ALLGTEXTE.10.09;%s\n",  atexte.disp_txt + 720 );
			atexte.disp_txt[720] = '\0'; fprintf (pa, "$$ALLGTEXTE.10.08;%s\n",  atexte.disp_txt + 630 );
			atexte.disp_txt[630] = '\0'; fprintf (pa, "$$ALLGTEXTE.10.07;%s\n",  atexte.disp_txt + 540 );
			atexte.disp_txt[540] = '\0'; fprintf (pa, "$$ALLGTEXTE.10.06;%s\n",  atexte.disp_txt + 450 );
			atexte.disp_txt[450] = '\0'; fprintf (pa, "$$ALLGTEXTE.10.05;%s\n",  atexte.disp_txt + 360 );
			atexte.disp_txt[360] = '\0'; fprintf (pa, "$$ALLGTEXTE.10.04;%s\n",  atexte.disp_txt + 270 );
			atexte.disp_txt[270] = '\0'; fprintf (pa, "$$ALLGTEXTE.10.03;%s\n",  atexte.disp_txt + 180 );
			atexte.disp_txt[180] = '\0'; fprintf (pa, "$$ALLGTEXTE.10.02;%s\n",  atexte.disp_txt + 90 );
			atexte.disp_txt[ 90] = '\0'; fprintf (pa, "$$ALLGTEXTE.10.01;%s\n",  atexte.disp_txt );
		}


    	memset( atexte.disp_txt, '\0', sizeof( atexte.disp_txt ) );
		atexte_class.lese_atexte( _a_bas.txt_nr7 );
		EntferneZeilenende (atexte.disp_txt, sizeof (atexte.disp_txt)); //WAL-148
			atexte.disp_txt[900] = '\0'; fprintf (pa, "$$ALLGTEXTE.11.10;%s\n",  atexte.disp_txt + 810 );
			atexte.disp_txt[810] = '\0'; fprintf (pa, "$$ALLGTEXTE.11.09;%s\n",  atexte.disp_txt + 720 );
			atexte.disp_txt[720] = '\0'; fprintf (pa, "$$ALLGTEXTE.11.08;%s\n",  atexte.disp_txt + 630 );
			atexte.disp_txt[630] = '\0'; fprintf (pa, "$$ALLGTEXTE.11.07;%s\n",  atexte.disp_txt + 540 );
			atexte.disp_txt[540] = '\0'; fprintf (pa, "$$ALLGTEXTE.11.06;%s\n",  atexte.disp_txt + 450 );
			atexte.disp_txt[450] = '\0'; fprintf (pa, "$$ALLGTEXTE.11.05;%s\n",  atexte.disp_txt + 360 );
			atexte.disp_txt[360] = '\0'; fprintf (pa, "$$ALLGTEXTE.11.04;%s\n",  atexte.disp_txt + 270 );
			atexte.disp_txt[270] = '\0'; fprintf (pa, "$$ALLGTEXTE.11.03;%s\n",  atexte.disp_txt + 180 );
			atexte.disp_txt[180] = '\0'; fprintf (pa, "$$ALLGTEXTE.11.02;%s\n",  atexte.disp_txt + 90 );
			atexte.disp_txt[ 90] = '\0'; fprintf (pa, "$$ALLGTEXTE.11.01;%s\n",  atexte.disp_txt );

    	memset( atexte.disp_txt, '\0', sizeof( atexte.disp_txt ) );
		atexte_class.lese_atexte( _a_bas.txt_nr8 );
		EntferneZeilenende (atexte.disp_txt, sizeof (atexte.disp_txt)); //WAL-148
			atexte.disp_txt[900] = '\0'; fprintf (pa, "$$ALLGTEXTE.12.10;%s\n",  atexte.disp_txt + 810 );
			atexte.disp_txt[810] = '\0'; fprintf (pa, "$$ALLGTEXTE.12.09;%s\n",  atexte.disp_txt + 720 );
			atexte.disp_txt[720] = '\0'; fprintf (pa, "$$ALLGTEXTE.12.08;%s\n",  atexte.disp_txt + 630 );
			atexte.disp_txt[630] = '\0'; fprintf (pa, "$$ALLGTEXTE.12.07;%s\n",  atexte.disp_txt + 540 );
			atexte.disp_txt[540] = '\0'; fprintf (pa, "$$ALLGTEXTE.12.06;%s\n",  atexte.disp_txt + 450 );
			atexte.disp_txt[450] = '\0'; fprintf (pa, "$$ALLGTEXTE.12.05;%s\n",  atexte.disp_txt + 360 );
			atexte.disp_txt[360] = '\0'; fprintf (pa, "$$ALLGTEXTE.12.04;%s\n",  atexte.disp_txt + 270 );
			atexte.disp_txt[270] = '\0'; fprintf (pa, "$$ALLGTEXTE.12.03;%s\n",  atexte.disp_txt + 180 );
			atexte.disp_txt[180] = '\0'; fprintf (pa, "$$ALLGTEXTE.12.02;%s\n",  atexte.disp_txt + 90 );
			atexte.disp_txt[ 90] = '\0'; fprintf (pa, "$$ALLGTEXTE.12.01;%s\n",  atexte.disp_txt );

    	memset( atexte.disp_txt, '\0', sizeof( atexte.disp_txt ) );
		atexte_class.lese_atexte( _a_bas.txt_nr9 );
		EntferneZeilenende (atexte.disp_txt, sizeof (atexte.disp_txt)); //WAL-148
			atexte.disp_txt[900] = '\0'; fprintf (pa, "$$ALLGTEXTE.13.10;%s\n",  atexte.disp_txt + 810 );
			atexte.disp_txt[810] = '\0'; fprintf (pa, "$$ALLGTEXTE.13.09;%s\n",  atexte.disp_txt + 720 );
			atexte.disp_txt[720] = '\0'; fprintf (pa, "$$ALLGTEXTE.13.08;%s\n",  atexte.disp_txt + 630 );
			atexte.disp_txt[630] = '\0'; fprintf (pa, "$$ALLGTEXTE.13.07;%s\n",  atexte.disp_txt + 540 );
			atexte.disp_txt[540] = '\0'; fprintf (pa, "$$ALLGTEXTE.13.06;%s\n",  atexte.disp_txt + 450 );
			atexte.disp_txt[450] = '\0'; fprintf (pa, "$$ALLGTEXTE.13.05;%s\n",  atexte.disp_txt + 360 );
			atexte.disp_txt[360] = '\0'; fprintf (pa, "$$ALLGTEXTE.13.04;%s\n",  atexte.disp_txt + 270 );
			atexte.disp_txt[270] = '\0'; fprintf (pa, "$$ALLGTEXTE.13.03;%s\n",  atexte.disp_txt + 180 );
			atexte.disp_txt[180] = '\0'; fprintf (pa, "$$ALLGTEXTE.13.02;%s\n",  atexte.disp_txt + 90 );
			atexte.disp_txt[ 90] = '\0'; fprintf (pa, "$$ALLGTEXTE.13.01;%s\n",  atexte.disp_txt );

    	memset( atexte.disp_txt, '\0', sizeof( atexte.disp_txt ) );
		atexte_class.lese_atexte( _a_bas.txt_nr10 );
		EntferneZeilenende (atexte.disp_txt, sizeof (atexte.disp_txt)); //WAL-148
			atexte.disp_txt[900] = '\0'; fprintf (pa, "$$ALLGTEXTE.14.10;%s\n",  atexte.disp_txt + 810 );
			atexte.disp_txt[810] = '\0'; fprintf (pa, "$$ALLGTEXTE.14.09;%s\n",  atexte.disp_txt + 720 );
			atexte.disp_txt[720] = '\0'; fprintf (pa, "$$ALLGTEXTE.14.08;%s\n",  atexte.disp_txt + 630 );
			atexte.disp_txt[630] = '\0'; fprintf (pa, "$$ALLGTEXTE.14.07;%s\n",  atexte.disp_txt + 540 );
			atexte.disp_txt[540] = '\0'; fprintf (pa, "$$ALLGTEXTE.14.06;%s\n",  atexte.disp_txt + 450 );
			atexte.disp_txt[450] = '\0'; fprintf (pa, "$$ALLGTEXTE.14.05;%s\n",  atexte.disp_txt + 360 );
			atexte.disp_txt[360] = '\0'; fprintf (pa, "$$ALLGTEXTE.14.04;%s\n",  atexte.disp_txt + 270 );
			atexte.disp_txt[270] = '\0'; fprintf (pa, "$$ALLGTEXTE.14.03;%s\n",  atexte.disp_txt + 180 );
			atexte.disp_txt[180] = '\0'; fprintf (pa, "$$ALLGTEXTE.14.02;%s\n",  atexte.disp_txt + 90 );
			atexte.disp_txt[ 90] = '\0'; fprintf (pa, "$$ALLGTEXTE.14.01;%s\n",  atexte.disp_txt );







		fclose (pa);
/*
        sprintf (progname, "%s\\BIN\\rswrun put_eti %s %s\\%s %s\\parafile %d",
                                     rosipath, eti_a_drk, etc, eti_artikel, tmp, etianz);
        ProcWaitExec (progname, SW_SHOWMINIMIZED, 0, 40, -1, 0);
*/
	/*
#define EINGANG 1
#define AUSGANG 2
#define UMLAGERUNG 3
#define ZERLEGUNG 4
#define FEINZERLEGUNG 5
#define PRODUKTIONSSTART 6
#define PRODUKTIONSFORTSCHRITT 7
#define BUCHENFERTIGLAGER 8
	*/

        if (atoi(EinAusgang) == EINGANG)  sprintf (EtiPath, "%s\\%s", etc, eti_artikel1);
        if (atoi(EinAusgang) == AUSGANG)  sprintf (EtiPath, "%s\\%s", etc, eti_artikel2);
        if (atoi(EinAusgang) == UMLAGERUNG)  sprintf (EtiPath, "%s\\%s", etc, eti_artikel3);
        if (atoi(EinAusgang) == ZERLEGUNG)  sprintf (EtiPath, "%s\\%s", etc, eti_artikel4);
        if (atoi(EinAusgang) == FEINZERLEGUNG)  sprintf (EtiPath, "%s\\%s", etc, eti_artikel5);
        if (atoi(EinAusgang) == PRODUKTIONSSTART)  sprintf (EtiPath, "%s\\%s", etc, eti_artikel6);
        if (atoi(EinAusgang) == PRODUKTIONSFORTSCHRITT)  sprintf (EtiPath, "%s\\%s", etc, eti_artikel7);
        if (atoi(EinAusgang) == BUCHENFERTIGLAGER)  sprintf (EtiPath, "%s\\%s", etc, eti_artikel8);

        int ret = PutEti (eti_a_drk, EtiPath, dname, etianz, eti_a_typ) ;
        if (ret == -1)
        {
			sprintf (EtiPath, "%s\\%s", etc, eti_artikel);
	        int ret = PutEti (eti_a_drk, EtiPath, dname, etianz, eti_a_typ) ;
		    if (ret == -1)
			{
				print_messG (2, "Etikett %s kann nicht gedruckt werden", EtiPath);
			}
        }
}




void WriteParafile (char *tmp)
/**
Parameter-Datei fuer put_eti schreiben.
**/
{
	    char dname [512];
		FILE *pa;

		sprintf (dname, "%s\\parafile", tmp);
		pa = fopen (dname, "w");
		if (pa == NULL) return;
		fprintf (pa, "$$LIEF;%s\n",      zerl_k.lief);
		fprintf (pa, "$$LIEFNR;%8.0lf\n",    zerl_k.partie);
		fprintf (pa, "$$ABEZ1;%s\n",     aufps.a_bz1);
		fprintf (pa, "$$ABEZ2;%s\n",     aufps.a_bz2);
		fprintf (pa, "$$ARTNR;%.0lf\n",  ratod (aufps.a));
		fprintf (pa, "$$WEDAT;%s\n",     lieferdatum);
		fprintf (pa, "$$NETTOG;%.3lf\n",  eti_netto);
		fprintf (pa, "$$TARAG;%.3lf\n",   eti_tara);
		fprintf (pa, "$$BRUTTOG;%.3lf\n", eti_brutto);
		fprintf (pa, "$$STUECK;%.2lf\n",  eti_stk );
		fprintf (pa, "$$ANZAHL;%ld\n",    atol (wiegzahl));
		fprintf (pa, "$$LIEFNAME;%s\n",   lief_krz1);
		fprintf (pa, "$$CHARGE;%s\n",     aufps.ls_charge);
		fprintf (pa, "$$MHD;%s\n",        aufps.hbk_date);
		fclose (pa);

		syskey = 0;
}

void WriteSumParafile (char *tmp)
/**
Parameter-Datei fuer put_eti schreiben.
**/
{
	    char dname [512];
		FILE *pa;

		sprintf (dname, "%s\\parafile", tmp);
		pa = fopen (dname, "w");
		if (pa == NULL) return;
		fprintf (pa, "$$LIEF;%s\n",      zerl_k.lief);
		fprintf (pa, "$$LIEFNR;%8.0lf\n",    zerl_k.partie);
		fprintf (pa, "$$ABEZ1;%s\n",     aufps.a_bz1);
		fprintf (pa, "$$ABEZ2;%s\n",     aufps.a_bz2);
		fprintf (pa, "$$ARTNR;%.0lf\n",  ratod (aufps.a));
		fprintf (pa, "$$WEDAT;%s\n",     lieferdatum);
		fprintf (pa, "$$NETTOG;%.3lf\n",  sum_eti_netto);
		fprintf (pa, "$$TARAG;%.3lf\n",   sum_eti_tara);
		fprintf (pa, "$$BRUTTOG;%.3lf\n", sum_eti_brutto);
		fprintf (pa, "$$STUECK;%.2lf\n",  sum_eti_stk );
		fprintf (pa, "$$ANZAHL;%ld\n",    atol (wiegzahl));
		fprintf (pa, "$$LIEFNAME;%s\n",   lief_krz1);
		fprintf (pa, "$$CHARGE;%s\n",     aufps.ls_charge);
		fprintf (pa, "$$MHD;%s\n",        aufps.hbk_date);
		fclose (pa);

/*
        sum_eti_netto  = (double) 0.0 ;
        sum_eti_brutto = (double) 0.0;
		sum_eti_tara   = (double) 0.0;
		sum_eti_stk    = (double) 0.0;
*/
}

void FromChargeBsd ()
{
	TEXTBLOCK::Rows.DestroyAll ();
	Chargen.FirstPosition ();
	Text *txt;
	while ((txt = (Text *) Chargen.GetNext ()) != NULL)
	{
		TEXTBLOCK::Rows.Add (new Text (*txt));
	}
}

void ToChargeBsd ()
{
	Chargen.DestroyAll ();
	int anz = TEXTBLOCK::Rows.GetCount ();
	for (int i = anz - 1; i >= 0; i --)
	{
		Text *txt = (Text *) TEXTBLOCK::Rows.Get (i);
		txt->Trim ();
		if (*txt != "")
		{
			break;
		}
	}

	anz = i + 1;
	for (i = 0; i < anz; i ++)
	{
		Text *txt = (Text *) TEXTBLOCK::Rows.Get (i);
		Text *Charge = new Text (*txt);
		Chargen.Add (Charge);
	}
	TEXTBLOCK::Rows.DestroyAll ();
}


void BucheChargeBsd (double a, short me_einh_kun, double pr_vk, char *charge)
/**
Bestandsbuchung vorbereiten.
**/
{
//	double lief_me_vgl;
	double buchme;
	char datum [12];
//    long lgrort;

/*
	if (bsd_kz == 0) return;
	if (zerl_k.bsd_verb_kz[0] != 'J')
	{
		return;
	}

	if (BsdArtikel (a) == FALSE) return;
*/

//    me_vgl = GetMeVgl (a, auf_me,me_einh_lief);
//if (me_vgl == AktBsdMe) return;
	buchme = 0;
    charge[13] = 0;

	bsd_buch.nr  = best_kopf.best_blg;
	strcpy (bsd_buch.blg_typ, "WC"); 
 	bsd_buch.mdn = zerl_k.mdn;
	bsd_buch.fil = 0;
	bsd_buch.kun_fil = 0;
	bsd_buch.a   = a;
	sysdate (datum);
	bsd_buch.dat = dasc_to_long (datum);
	systime (bsd_buch.zeit);
	strcpy (bsd_buch.pers, sys_ben.pers);

//    sprintf (bsd_buch.bsd_lgr_ort, "%ld", akt_lager);

    sprintf (bsd_buch.bsd_lgr_ort, "%ld", 0l);
	bsd_buch.qua_status = 0;
	bsd_buch.me = 0;
	bsd_buch.bsd_ek_vk = pr_vk;
    strcpy (bsd_buch.chargennr, charge);
    strcpy (bsd_buch.ident_nr, aufps.ident_nr);
    strcpy (bsd_buch.herk_nachw, "");
    sprintf (bsd_buch.lief, "%s", zerl_k.lief);
    bsd_buch.auf = best_kopf.best_blg;
    strcpy  (bsd_buch.verfall, "");
    bsd_buch.delstatus = 0;
    sprintf  (bsd_buch.err_txt,"%8.0lf", zerl_k.partie);
//	BsdBuch.dbinsert ();
}

void BucheChargenBsd ()
{

	Chargen.FirstPosition ();
	Text *Charge;
	while ((Charge = (Text *) Chargen.GetNext ()) != NULL)
	{
		
        if (lief.waehrung == 2)
        {
				BucheChargeBsd (ratod (aufps.a), atoi (aufps.me_einh_kun), 
			           ratod (aufps.ls_lad_euro), Charge->GetBuffer ());
		}
		else
        {
				BucheChargeBsd (ratod (aufps.a), atoi (aufps.me_einh_kun), 
			           ratod (aufps.auf_lad_pr), Charge->GetBuffer ());
		}
					   
	}
}


void EnterRCharge (int value)
{
		textblock = new TEXTBLOCK;
		textblock->SetLongText (TRUE);
	    FromChargeBsd ();
		EnableWindows (hMainWindow, FALSE);
		EnableWindow (WiegWindow, FALSE);
        textblock->MainInstance (hMainInst, WiegWindow);
        textblock->ScreenParam (scrfx, scrfy);
        textblock->SetUpshift (TRUE);
        textblock->EnterTextBox (BackWindow1, "Chargen", "", 0, "", EntNumProc);
		EnableWindows (hMainWindow, TRUE);
		EnableWindow (WiegWindow, TRUE);
		delete textblock;
		textblock = NULL;
		if (syskey != KEY5)
		{
				ToChargeBsd ();
		}
		syskey = 0;
		return;
}

void EnterREz (int value)
{
	    char buffer [80] = {""};
		
		strcpy (buffer, aufps.ez_nr);
		textblock = new TEXTBLOCK;
		textblock->SetLongText (FALSE);
		EnableWindows (hMainWindow, FALSE);
		EnableWindow (WiegWindow, FALSE);
        textblock->MainInstance (hMainInst, WiegWindow);
        textblock->ScreenParam (scrfx, scrfy);
        textblock->SetUpshift (TRUE);
        textblock->EnterTextBox (BackWindow1, "EZ-Nummer", buffer, 20, "", EntNumProc);
		EnableWindows (hMainWindow, TRUE);
		EnableWindow (WiegWindow, TRUE);
		delete textblock;
		textblock = NULL;
		if (syskey != KEY5)
		{
			strcpy (aufps.ez_nr, buffer);
		}
		syskey = 0;
}

void EnterREs (int value)
{

	    char buffer [80] = {""};
		
		strcpy (buffer, aufps.es_nr);
		textblock = new TEXTBLOCK;
		textblock->SetLongText (FALSE);
		EnableWindows (hMainWindow, FALSE);
		EnableWindow (WiegWindow, FALSE);
        textblock->MainInstance (hMainInst, WiegWindow);
        textblock->ScreenParam (scrfx, scrfy);
        textblock->SetUpshift (TRUE);
        textblock->EnterTextBox (BackWindow1, "ES-Nummer", buffer, 20, "", EntNumProc);
		EnableWindows (hMainWindow, TRUE);
		EnableWindow (WiegWindow, TRUE);
		delete textblock;
		textblock = NULL;
		if (syskey != KEY5)
		{
			strcpy (aufps.es_nr, buffer);
		}
		syskey = 0;
}

void EnterRLand (int value)
{
	    char buffer [80] = {""};
	    
        ShowTierLand (buffer);
		if (syskey != KEY5)
		{
			strcpy (aufps.laender, buffer);
		}
		syskey = 0;
}


void EnterRIdent (int value)
{

	    char buffer [80] = {""};
		
		strcpy (buffer, aufps.ident_nr);
		if (_a_bas.charg_hand == 9)
		{
			  NuBlock.SetCharChangeProc (ChangeChar);
			  NuBlock.SetParent (eWindow);
              NuBlock.EnterNumBox (WiegWindow, "  Ident-Nr  ",
                                   buffer, -1, "", EntNuProc);
		}
		else
		{
			textblock = new TEXTBLOCK;
			textblock->SetLongText (FALSE);
			EnableWindows (hMainWindow, FALSE);
			EnableWindow (WiegWindow, FALSE);
			textblock->MainInstance (hMainInst, WiegWindow);
			textblock->ScreenParam (scrfx, scrfy);
			textblock->SetUpshift (TRUE);
			textblock->EnterTextBox (BackWindow1, "Ident-Nummer", buffer, -1, "", EntNumProc);
			EnableWindows (hMainWindow, TRUE);
			EnableWindow (WiegWindow, TRUE);
			delete textblock;
			textblock = NULL;
		}
		if (syskey != KEY5)
		{
			if (strlen (buffer) > 4)
			{
				Text Ean = buffer;
				ScanEan128Charge (Ean,1);
			}
			else
			{
				strcpy (aufps.ident_nr, buffer);
			}
		}
		syskey = 0;
}

void FillCountries ()
{
		static CVector Countries;
		PTABN *p;
		char *c;
		Token t;

		if (Countries.GetCount () == 0)
		{
			int dsqlstatus = ptab_class.lese_ptab_all ("staat");
			while (dsqlstatus == 0)
			{
				p = new PTABN;
				memcpy (p, &ptabn, sizeof (PTABN));
				Countries.Add (p);
				dsqlstatus = ptab_class.lese_ptab_all ();
			}
		}

		t.SetSep (" ");
		t = aufps.laender;
		int i = 0;
		while ((c = t.NextToken ()) != NULL)
		{
			Text L1 = c;
			L1.Trim ();
			Countries.FirstPosition ();
			while ((p = (PTABN *) Countries.GetNext ()) != NULL)
			{
				Text L2 = p->ptwer2;
				L2.Trim ();
				if (L1 == L2) break;
			}
			if (p == NULL)
			{
				continue;
			}
			switch (i)
			{
			case 0:
				rind.gebland = atoi (p->ptwert);
				break;
			case 1:
				rind.mastland = atoi (p->ptwert);
				break;
			case 2:
				rind.schlaland = atoi (p->ptwert);
				break;
			case 3:
				rind.zerlland = atoi (p->ptwert);
				break;
			}
			i ++;
			if (i > 3) break;
		}
}


void FillZerlLfd ()
{
	static int cursor = -1;
	static int ins_cursor = -1;
	static long lfd = 0;

	lfd = 0;
	if (cursor == -1)
	{
		DbClass.sqlout ((long *) &lfd, 2, 0);
		cursor = DbClass.sqlcursor ("select max (lfd) from zerldaten");
		DbClass.sqlin ((long *) &zerldaten.lfd, 2, 0);
		DbClass.sqlin ((long *) &zerldaten.lfd_i, 2, 0);
		ins_cursor = DbClass.sqlcursor ("update zerldaten set lfd_i = ? where lfd = ?");
	}
	if (cursor == -1) return;
	DbClass.sqlopen (cursor);
	if (DbClass.sqlfetch (cursor) == 0)
	{
		zerldaten.lfd = lfd;
		zerldaten.lfd_i = lfd;
		DbClass.sqlexecute (ins_cursor);
	}
}
                                 

void WriteRind ()
{
	/******************************** 
	    char datum[12];
		
		sysdate (datum);
	    Text LsIdent = aufps.ident_nr;
        Text LsEz    = aufps.ez_nr;
		Text LsEs    = aufps.es_nr;
		Text LsLand  = aufps.laender;

		LsIdent.TrimRight ();
		LsEz.TrimRight ();
		LsEs.TrimRight ();
		LsLand.TrimRight ();
		if (LsIdent == "")
		{
			if (LsEz != "" || LsEs != "" || LsLand != "")
			{
				disp_messG ("Es wurde keine Idennummer erfasst", 2);
			}
			return;
		}

		FillCountries ();
		strcpy (zerldaten.ident_intern, aufps.ident_nr); 
		strcpy (zerldaten.ident_extern, aufps.ident_nr); 
		zerldaten.a = ratod (aufps.a); 
		zerldaten.a_gt = ratod (aufps.a); 
		strcpy (zerldaten.eznum1, aufps.ez_nr); 
		strcpy (zerldaten.esnum,  aufps.es_nr); 
		zerldaten.gebland = rind.gebland;
		zerldaten.mastland = rind.mastland;
		zerldaten.schlaland = rind.schlaland;
		zerldaten.zerland = rind.zerlland;
		strcpy (zerldaten.lief, zerl_k.lief);
		strcpy (zerldaten.lief_rech_nr, zerl_k.lief_rech_nr);
		zerldaten.mdn = zerl_k.mdn;
		zerldaten.fil = zerl_k.fil;
		zerldaten.dat = dasc_to_long (datum);
		zerldaten.aktiv = 1;
        Zerldaten.dbinsert ();
		FillZerlLfd ();
		*************************************************/

}

int doetikett (void)
/**
Wiegen durchfuehren.
**/
{
//	    char progname [512];
		char *etc;
		int etianz;
		char *tmp;
	    char dname [512];


        DestroyKistBmp ();
        if (WithEti == FALSE) return 0;

		tmp = getenv ("TMPPATH");
		if (tmp == NULL) tmp = "c:\\user\\fit\\tmp";
		sprintf (dname, "%s\\parafile", tmp);
		etc = getenv ("BWSETC");
		if (etc == NULL) etc = "c:\\user\\fit\\etc";

		tmp = getenv ("TMPPATH");
		if (tmp == NULL) tmp = "c:\\user\\fit\\tmp";

		WriteParafile (tmp);

		etianz = 1;
/*
        sprintf (progname, "%s\\BIN\\rswrun put_eti %s %s\\eti%04d.prm %s\\parafile %d",
                                     rosipath, krz_txt, etc, eti_nr, tmp, etianz);
        ProcWaitExec (progname, SW_SHOWMINIMIZED, 0, 40, -1, 0);
*/



        sprintf (EtiPath, "%s\\eti%04d.prm", etc, eti_nr);

        int ret = PutEti (krz_txt, EtiPath, dname, etianz, -1) ;
        if (ret == -1)
        {
            disp_messG ("Etikett kann nicht gedruckt werden", 2);
        }
        return 0;
}

int dosumeti (void)
/**
Wiegen durchfuehren.
**/
{
//	    char progname [256];
		char *etc;
		int etianz;
		char *tmp;
	    char dname [512];


        DestroyKistBmp ();
        if (WithSumEti == FALSE) return 0;

		tmp = getenv ("TMPPATH");
		if (tmp == NULL) tmp = "c:\\user\\fit\\tmp";
		sprintf (dname, "%s\\parafile", tmp);
		etc = getenv ("BWSETC");
		if (etc == NULL) etc = "c:\\user\\fit\\etc";

		tmp = getenv ("TMPPATH");
		if (tmp == NULL) tmp = "c:\\user\\fit\\tmp";

		WriteSumParafile (tmp);

		etianz = 1;
/*
        sprintf (progname, "%s\\BIN\\rswrun put_eti %s %s\\eti%04d.prm %s\\parafile %d",
                                     rosipath, sum_krz_txt, etc, sum_eti_nr, tmp, etianz);
        ProcWaitExec (progname, SW_SHOWMINIMIZED, 0, 40, -1, 0);
*/

        sprintf (EtiPath, "%s\\eti%04d.prm", etc, eti_nr);

        int ret = PutEti (krz_txt, EtiPath, dname, etianz, -1) ;
        if (ret == -1)
        {
            disp_messG ("Etikett kann nicht gedruckt werden", 2);
        }
        return 0;
}

int doetispez (void)
/**
Wiegen durchfuehren.
**/
{
        static Text text1 = "&Kiste"; 
        static Text text2 = "&EZ"; 
        static Text text3 = "E&V"; 
        static Text text4 = "&Artikel"; 


		Touchf.SetParent (LogoWindow);
		Touchf.SetEtiAttr (eti_kiste, eti_ez, eti_ev, eti_artikel);
		Touchf.SetEtiText (text1.GetBuffer (),
				           text2.GetBuffer (),
				           text3.GetBuffer (),
				           text4.GetBuffer ());
		Touchf.SetWriteParams (WriteParamKiste, WriteParamEZ,
			                   WriteParamEV, WriteParamArt);

		Touchf.EnterFunkBox (WiegWindow, -1, -1, TouchfProc);
		return 0;
}

int dolgrspez (void)
/**
Lagerauswahl
**/
{
		return 0;
}

int dozusatz (void)
/**
Wiegen durchfuehren.
**/
{
        static Text text1 = "Mehrfach\n&Chargen"; 
        static Text text2 = "&EZ"; 
        static Text text3 = "E&S"; 
        static Text text4 = "&L�nder"; 
        static Text text5 = "&Ident\nNummer"; 

	    BOOL EnterAnz = TOUCHF::EnterAnz;

        text1 = "Mehrfach\n&Chargen"; 
        text2 = "&EZ"; 
        text3 = "E&S"; 
        text4 = "&L�nder"; 
        text5 = "&Ident\nNummer"; 

	    TOUCHF::EnterAnz = FALSE;
		Touchf.SetParent (WiegWindow);
		Touchf.SetEtiAttr ("", "", "", "");
		Touchf.SetEtiText (text1.GetBuffer (),
				           text2.GetBuffer (),
				           text3.GetBuffer (),
				           text4.GetBuffer (),
						   text5.GetBuffer ());

		Touchf.SetWriteParams (EnterRCharge, EnterREz,
			                   EnterREs, EnterRLand, EnterRIdent);

		Touchf.EnterFunkBox (WiegWindow, -1, -1, TouchfProc);
	    TOUCHF::EnterAnz = EnterAnz;
		return 0;
}


int dotara (void)
/**
Wiegen durchfuehren.
**/
{
		DestroyKistBmp ();
        if (WieTara.aktivate == -1) return 0;

        if (WorkModus == AUSZEICHNEN)
        {
                        dogebinde ();
                        return (0);
        }
        CtrMode = 1;
        CloseControls (&WieButton);
        current_wieg = 0;
        CtrMode = 1;
        wiegend = 9;
        return 0;
}


int doanzahl (void)
/**
Stueckeingabe durchfuehren.
**/
{
		static char WiegAnz[20];

        SetStorno (NULL, 0);
        last_anz = 0l;
        sprintf (WiegAnz, "%.0lf", (double) 1);
        EnterCalcBox (WiegWindow,"  Anzahl  ",  WiegAnz, 12, "%8.0lf");
        if (syskey != KEY5)
		{
					SetEtiStk (ratod (WiegAnz));
		}
  	    else
		{
                    SetEtiStk ((double) 1.0);
					strcpy (WiegAnz, "1");
		}

        display_form (WiegKg, &WiegKgE, 0, 0);
        display_form (WiegWindow, &artwform, 0, 0);
        display_form (WiegWindow, &artwconst, 0, 0);
        if (autoan) settara ();
        current_wieg = 1;
		last_anz = atol (WiegAnz);
//		sprintf (aufps.anz_einh, "%.3lf", ratod (aufps.anz_einh) + ratod (WiegAnz));
//		last_anz = atol (aufps.anz_einh);
		sprintf (aufps.anz_einh, "%.3lf", ratod (WiegAnz));
		strcpy (wiegzahl, aufps.anz_einh);
        memcpy (&aufparr [aufpidx], &aufps, sizeof (struct AUFP_S));
        arrtozerl_p (aufpidx);
        ZerlpClass.dbupdate (atoi (EinAusgang));
        commitwork ();
        beginwork ();
        return 0;
}


void ArtikelHand (void)
/**
Artikel-Menge von Hand eingeben.
**/
{
        double alt;
        double netto;

        StornoSet = 0;
        SetStorno (dostorno, 0);
        sprintf (WiegMe, "%.3lf", (double) 0);
        EnterCalcBox (eWindow,"  Anzahl  ",  WiegMe, 12, WiegKgE.mask[0].picture);
        if (syskey != KEY5)
        {
              strcpy (aufps.erf_kz, "H");
              if (StornoSet)
              {
                  stornoaction ();
              }
              else
              {
                  netto = (double) ratod (WiegMe);
                  alt = (double) ratod (auf_me);
                  alt -= netto;
                  sprintf (auf_me, "%3lf", alt);
                  alt = (double) ratod (aufps.lief_me);
                  alt += netto;
                  sprintf (aufps.lief_me, "%3lf", alt);
                  strcpy (aufps.s, "3");
                  SetEtiStk (ratod (WiegMe));
				  SetEti ((double) 0.0, (double) 0.0, (double) 0.0);
              }
        }
}

int GenPartie (void)
{
		GenPartie0();
		return ReadLS();
}
double GenPartie0 (void)
{
	int dsqlstatus;
	


		int kw;
		int wt;
		char yy [5];
		char mm [5];
		char tt [5];
		char ww [3];
		char w [2];
		int i;

	if (atoi(EinAusgang) == ZERLEGUNG)
	{
			sprintf (lief_nr, "%s",ChargeAuswahl);
			return ratod(lief_nr);
	}
	else if (atoi(EinAusgang) == FEINZERLEGUNG)
	{
			sprintf (lief_nr, "%s",ChargeAuswahl);
			return ratod(lief_nr);
	}
	else if (atoi(EinAusgang) == PRODUKTIONSSTART )
	{
  		  DbClass.sqlcomm ("delete from zerl_k where zer_dat = today and partie not in (select partie from zerl_p where bearb >= today) ");

	 	 zerl_k.zer_dat = dasc_to_long (lieferdatum);

	      DbClass.sqlout ((double *) &zerl_k.partie, 3, 0);
	      DbClass.sqlout ((short *) &zerl_k.partie_status, 1, 0);
	      DbClass.sqlout ((short *) &zerl_k.fanggebiet, 1, 0);
		  DbClass.sqlin ((short *) &Mandant, 1, 0);
		  DbClass.sqlin ((long *) &zerl_k.zer_dat, 2, 0);
		  DbClass.sqlin ((double *) &zerl_k.a, 3, 0);

		  dsqlstatus = DbClass.sqlcomm ("select partie, partie_status, fanggebiet from zerl_k "
		                  "where mdn = ? "
						  "and   zer_dat = ? and a = ?  order by partie desc ");

		  FlgNeuePartie = FALSE;
		  if (dsqlstatus == 0)
		  {
			  if (zerl_k.partie_status > 0) 
			  {
				  zerl_k.partie ++;
				  zerl_k.partie_status = 0;
				  FlgNeuePartie = TRUE;
			  }
			  sprintf (lief_nr ,"%.0lf",zerl_k.partie);  
              sprintf (ptabn.ptwert, "%hd",zerl_k.fanggebiet);
			  ptab_class.lese_ptab ("fanggebiet", ptabn.ptwert);
				strcpy (fanggebiet, ptabn.ptbez);

		  }
		  else
		  {
			  //Hier: Generierung der Charge beim Produktionsstart
				strcpy (yy, &lieferdatum[8]);
		        memcpy(&tt[0],&lieferdatum[0],2);
				tt[2] = 0;
		        memcpy(&mm[0],&lieferdatum[3],2);
				mm[2] = 0;
				if (strlen (clipped(artikel)) < 5)
				{
					sprintf (lief_nr, "%s%s%s%4.0lf00",yy,mm,tt,_a_bas.a);
					for (i = 0; i < 12 ; i++)
					{
						if (lief_nr[i] == ' ') lief_nr[i] = '0';
					}

				}
 			    FlgNeuePartie = TRUE;

		  }
			return ratod(lief_nr);
	}

		strcpy (yy, &lieferdatum[8]);
		kw = get_woche (lieferdatum);
		wt = get_wochentag (lieferdatum);
		sprintf (ww, "%02d", kw);
		sprintf (w, "%01d", wt);
		if (PartieNr == -1)
		{
			sprintf (lief_nr, "1%s%s%02d",yy,ww,99);
		}
		else
		{
			sprintf (lief_nr, "1%s%s%02d",yy,ww,PartieNr);
		}
		if (atoi(EinAusgang) == UMLAGERUNG) 
		{
			sprintf (lief_nr, "9%s%s%02d",yy,ww,Standort);
		}
		return ratod(lief_nr);
}


char *GenCharge (void)
/**
Chargen-Nummer generieren.
**/
{

		char datum [12];
		long datl;
		int kw;
		int wt;
		char yy [5];
		char ww [3];
		char w [2];
		char lz [3];


		datl = zerl_k.zer_dat;
		if (generiere_charge == 1) datl += 2;
		dlong_to_asc (datl, datum);
		strcpy (yy, &datum[8]);
		kw = get_woche (datum);
		wt = get_wochentag (datum);
		sprintf (ww, "%02d", kw);
		sprintf (lz, "%02d", lief.lief_zeit);
		sprintf (w, "%01d", wt);
		if (generiere_charge == 2)
		{
			if (lief.lief_zeit == 0)
			{
				sprintf (cha, "%s%s",ww,w);
			}
			else
			{
				sprintf (cha, "%s%s%s",ww,w,lz);
			}
		}
		else if (generiere_charge == 3)
		{
			sprintf (cha, "%s%s%s",ww,w,lief.lief);
		}
		else
		{
			sprintf (cha, "%s%s",yy,ww);
		}
		return cha;
}


void TestCharge (void)
/**
Parameter fuer die Chargennummer bearbeiten.
**/
{

	    if (ls_charge_par == 0 || lsc2_par == 0)
		{
			//011009
			if (generiere_charge > 0 && _a_bas.charg_hand == 1)
			{
			    artwformW.mask[0].attribut = READONLY;
			    artwformP.mask[0].attribut = READONLY;
				strcpy (aufps.ls_charge, GenCharge ());
			}
			return;
		}
		if (_a_bas.charg_hand == 0)
		{
			    artwformW.mask[0].attribut = READONLY;
			    artwformP.mask[0].attribut = READONLY;
				if (strcmp (aufps.ls_charge, " ") <= 0)
				{
					strcpy (aufps.ls_charge,last_charge);
				}
		}
		else if (_a_bas.charg_hand == 1)
		{
			if (generiere_charge > 0)
			{
			    artwformW.mask[0].attribut = READONLY;
			    artwformP.mask[0].attribut = READONLY;
				strcpy (aufps.ls_charge, GenCharge ());
			}
			else
			{

			    artwformW.mask[0].attribut = EDIT;
			    artwformP.mask[0].attribut = EDIT;
				if (strcmp (aufps.ls_charge, " ") <= 0)
				{
					strcpy (aufps.ls_charge,last_charge);
				}
			}
		}
		else if (_a_bas.charg_hand == 2)
		{
			    artwformW.mask[0].attribut = READONLY;
			    artwformP.mask[0].attribut = READONLY;
				strcpy (aufps.ls_charge, GenCharge ());
		}
		else if (_a_bas.charg_hand == 3)
		{
			    artwformW.mask[0].attribut = READONLY;
			    artwformP.mask[0].attribut = READONLY;
				strcpy (aufps.ls_charge, "");
		}
}

void ArtikelWiegen (HWND hWnd)
/**
Artikel wiegen.
**/
{
        RECT rect;
        MSG msg;
        int x, y, cx, cy;
        int xw, yw, cxw, cyw;
        int xc, yc, cxc, cyc;
        int wlen, wrows;
        int buglen, buabs;
        int z, s;
        static int first = 0;

		Chargen.DestroyAll ();
		AuswahlLager();

        if (TestQsPosA (ratod (aufps.a)) == FALSE)
        {
            return;
        }
        if (WiegeTyp == ENTNAHME && StartWiegen == FALSE &&
            EntnahmeActGew == 0.0)
        {
            disp_messG ("Es wurde keine Startwiegung durchgef�hrt", 2);
            return;
        }
        last_anz = 0l;
        if (WiegenAktiv) return;

        if (StartWiegen)
        {
            EntnahmeStartGew = EntnahmeActGew = 0.0;
            memcpy (&SetTara, &SetTara2, sizeof (SetTara));
        }
        else if (WiegeTyp == STANDARD)
        {
            EntnahmeStartGew = EntnahmeActGew = 0.0;
            memcpy (&SetTara, &SetTara1, sizeof (SetTara));
        }
        else
        {
            deltaraw ();
            akt_tara = EntnahmeTara;
            sprintf (aufps.tara, "%.3lf", akt_tara);
            memcpy (&SetTara, &SetTara2, sizeof (SetTara));
//            sprintf (ftara, "%.3lf", akt_tara);
        }


        if (strcmp (aufps.hnd_gew, "G") == 0 || strcmp (aufps.erf_gew_kz, "J") == 0 ||
            WiegeTyp == ENTNAHME)
		{
          ActivateColButton (WiegCtr, &WieButton, 3, 2, 0);
          ActivateColButton (WiegCtr, &WieButton, 0, 2, 0);
		}
		else
		{
          ActivateColButton (WiegCtr, &WieButton, 3, -1, 0);
          ActivateColButton (WiegCtr, &WieButton, 0, -1, 0);
		}

	    if (atoi(EinAusgang) == EINGANG)  //Zerlegeeingang
		{
	          ActivateColButton (WiegCtr, &LgrSpezButton, 0, -1, 1);
	          ActivateColButton (WiegCtr, &LgrSpezButton, 2, 1, 1);  //Lager zubuchung : deaktiviert
		}
		else if (atoi(EinAusgang) == AUSGANG)  //Zerlegeausgang
		{
	          ActivateColButton (WiegCtr, &LgrSpezButton, 0, 1, 1); //Lager Abbuchung : deaktiviert
	          ActivateColButton (WiegCtr, &LgrSpezButton, 2, -1, 1);  
		}
		else if (atoi(EinAusgang) == ZERLEGUNG)  
		{
			if (aufps.kng == EINGANG)
			{
	     	  sprintf (EinAusgangtxt, "Zerlegeeingang");
		      ActivateColButton (WiegCtr, &LgrSpezButton, 0, -1, 1);  //Lagarab
	          ActivateColButton (WiegCtr, &LgrSpezButton, 2,  1, 1);  // Lagerzu
			}
			else if (aufps.kng == AUSGANG)
			{
	     	  sprintf (EinAusgangtxt, "Zerlegeausgang");
	          ActivateColButton (WiegCtr, &LgrSpezButton, 0,  1, 1); 
	          ActivateColButton (WiegCtr, &LgrSpezButton, 2, -1, 1);  
			}
		}
		else if (atoi(EinAusgang) == FEINZERLEGUNG)  
		{
			if (aufps.kng == EINGANG)
			{
	     	  sprintf (EinAusgangtxt, "FeinZerlegeeingang");
		      ActivateColButton (WiegCtr, &LgrSpezButton, 0, -1, 1);  //Lagarab
	          ActivateColButton (WiegCtr, &LgrSpezButton, 2,  1, 1);  // Lagerzu
			}
			else if (aufps.kng == AUSGANG)
			{
	     	  sprintf (EinAusgangtxt, "FeinZerlegeausgang");
	          ActivateColButton (WiegCtr, &LgrSpezButton, 0,  1, 1); 
	          ActivateColButton (WiegCtr, &LgrSpezButton, 2, -1, 1);  
			}
		}
		else if (atoi(EinAusgang) == UMLAGERUNG)  
		{
	          ActivateColButton (WiegCtr, &LgrSpezButton, 0, 1, 1); 
	          ActivateColButton (WiegCtr, &LgrSpezButton, 2, 1, 1);  
		}
		else if (atoi(EinAusgang) == PRODUKTIONSSTART ) 
		{
	          ActivateColButton (WiegCtr, &LgrSpezButton, 0,  1, 1); 
	          ActivateColButton (WiegCtr, &LgrSpezButton, 2, -1, 1);  
		}
		else if (atoi(EinAusgang) == PRODUKTIONSFORTSCHRITT  ) 
		{
//	          ActivateColButton (WiegCtr, &LgrSpezButton, 0, 1, 1); 
//	          ActivateColButton (WiegCtr, &LgrSpezButton, 2, 1, 1);  
		}
		else if (atoi(EinAusgang) == BUCHENFERTIGLAGER  ) 
		{
//	          ActivateColButton (WiegCtr, &LgrSpezButton, 0, 1, 1); 
//	          ActivateColButton (WiegCtr, &LgrSpezButton, 2, 1, 1);  
		}
		if (ratod (aufps.auf_me) == (double) 0.0)
		{
			artwformTW.mask[1].attribut = REMOVED;
			artwformW.mask[1].attribut = REMOVED;
		}
		else
		{
			artwformTW.mask[1].attribut = DISPLAYONLY;
			artwformW.mask[1].attribut  = READONLY;
		}

		strcpy (wiegzahl, aufps.anz_einh);
        sum_eti_netto  = (double) 0.0 ;
        sum_eti_brutto = (double) 0.0;
		sum_eti_tara   = (double) 0.0;
		sum_eti_stk    = (double) 0.0;
        SwitchLiefAuf ();
		TestCharge ();
		testeti ();
        if (WorkModus == AUSZEICHNEN)
        {
//               WieWie.text1 = "Auszeichn.";
               strcpy (summe1, "0");
               strcpy (summe2, "0");
               strcpy (summe3, "0");
               strcpy (zsumme1, "0");
               strcpy (zsumme2, "0");
               strcpy (zsumme3, "0");
               WieWie.text1 = "Aus-";
               WieWie.text2 = "zeichnen";
               WieWie.tx1 = -1;
               WieWie.ty1 = 25;
               WieWie.tx2 = -1;
               WieWie.ty2 = 50;
//               WieTara.aktivate = -1;
               WieTara.text1 = "Gebinde";
               WieTara.text2 = "Tara";
               WieTara.tx1 = -1;
               WieTara.ty1 = 25;
               WieTara.tx2 = -1;
               WieTara.ty2 = 50;
               fetch_std_geb ();
               memcpy (&artwformT,&artwformTP, sizeof (form));
               memcpy (&artwform,&artwformP, sizeof (form));
               PosArtFormP ();
        }
        else
        {
               WieWie.text1 = "Wiegen";
               WieWie.text2 = NULL;
               WieWie.tx1 = -1;
               WieWie.ty1 = -1;
               WieWie.tx2 = 0;
               WieWie.ty2 = 0;
//               WieTara.aktivate = 2;
               WieTara.text1 = "Tara";
               WieTara.text2 = NULL;
               WieTara.tx1 = -1;
               WieTara.ty1 = -1;
               WieTara.tx2 = 0;
               WieTara.ty2 = 0;
               memcpy (&artwformT,&artwformTW, sizeof (form));
               memcpy (&artwform,&artwformW, sizeof (form));
               PosArtFormW ();
        }

        if (aufps.hnd_gew[0] == 'H' || aufps.erf_gew_kz[0] == 'N')
		{
               WiePreis.text1 = "Anzahl";
		}
		else
		{
               WiePreis.text1 = "Preise";
		}

        WieButton.mask[1].BuId = 0;
        CtrMode = 0;

        if (WiegeTyp == ENTNAHME)
        {
               AutoTara.text3 = "";
               AutoTara.aktivate = -1;
        }
        else
        {
               AutoTara.text3 = anaus [autoan];
               if (autoan)
               {
                      AutoTara.aktivate = 4;
               }
               else
               {
                      AutoTara.aktivate = 3;
               }
        }
        SetListEWindow (0);
        strcpy (WiegMe, "0.000");
        sprintf (summe1, "%.0lf", (double) 0);
        sprintf (summe2, "%.0lf", (double) 0);
        sprintf (zsumme1, "0");
        sprintf (zsumme2, "0");
        AktWiegPos = 0;
        RegisterWieg ();
        GetWindowRect (hMainWindow, &rect);
        x = rect.left;
        y = rect.top + 24;
        cx = rect.right - rect.left;
        cy = rect.bottom - rect.top - 24;

        PosArtFormC ();
        wlen = strlen (WiegText);
        FontTextLen (WiegKgT.font, WiegText, &wlen,
                                             &wrows);
        WiegKgT.mask[0].length = wlen;
        WiegKgT.mask[0].rows   = wrows;

        WiegKgE.mask[0].pos[0]  = WiegKgT.mask[0].pos[0] + 2 * wrows;

        wlen = 11;
        FontTextLen (WiegKgE.font, "0000000,000", &wlen,
                                                  &wrows);

        WiegKgE.mask[0].length = wlen;
        WiegKgE.mask[0].rows   = wrows;

        if (wlen > WiegKgT.mask[0].length)
        {
                   cxw = wlen;
        }
        else
        {
                   cxw = WiegKgT.mask[0].length;
        }

        cxw += 20;

        cyw = WiegKgE.mask[0].pos[0] + wrows + wrows / 2 + 10;
        xw = (cx - cxw) / 2;
        yw = (cy - cxw) / 2 + 120;

        PosMeForm (yw, xw, cyw, cxw);

        if (first == 0)
        {
                  ftaratxt.mask[0].pos[1] = xw;
                  ftaratxt.mask[0].pos[0] = yw - 40;

                  z = yw - 40;
                  s = xw;
                  GetTextPos (ftaratxt.font, &z, &s);
                  s += 7;
                  KonvTextAbsPos (ftaratxt.font, &z, &s);
                  ftara.mask[0].pos[0] = z + 10;
                  ftara.mask[0].pos[1] = s;

                  s = strlen (ftaratxt.mask[0].feld);
                  FontTextLen (ftaratxt.font, ftaratxt.mask[0].feld, &s, &z);
                  ftaratxt.mask[0].length = s;
                  ftaratxt.mask[0].rows   = z;

                  s = 8;
                  FontTextLen (ftara.font, "0000.000", &s, &z);
                  ftara.mask[0].length = s;
                  ftara.mask[0].rows   = z;

                  ftexte.mask[0].length = s;
                  ftexte.mask[0].rows = z * 2;


				//021109 A 
                  sw_abzug.mask[0].pos[1] = xw; 
                  sw_abzug.mask[0].pos[0] = yw - 80; 
                  z = yw - 80;
                  s = xw ;
                  GetTextPos (sw_abzug.font, &z, &s);
                  s += 7;
                  KonvTextAbsPos (sw_abzug.font, &z, &s);

                  s = strlen (sw_abzug.mask[0].feld);
                  FontTextLen (sw_abzug.font, sw_abzug.mask[0].feld, &s, &z);
                  sw_abzug.mask[0].length = s;
                  sw_abzug.mask[0].rows   = z;
				 //021109 E
				  /***
			  //220712 A    Hier wird die Gr��e der Festtara-Buttons bestimmt (aus dem ptab-Text heraus)
				int i;
		        for (i = 0; i < AnzFestTara; i ++)
		        {
				  s = ffesttara.mask[i].length ;
				  z = ffesttara.mask[i].rows ;
				  s = strlen(ftabarr[i].ptbez) + 4; 
                  FontTextLen (ffesttara.font, ftabarr[i].ptbez, &s, &z);
                  ffesttara.mask[i].length = s;
                  ffesttara.mask[i].rows = z * 2;
				}
			  //220712 E
			  *******/
				  

/*
                  KonvTextStrLen (ftexte.font, "Textex ", &s);
                  STexte.bmpx = s;
*/

                  first = 1;
        }

		EtiSpezButton.mask[0].pos[0] = cy - 160 - 3 * bsize;

		/**
		LgrSpezButton.mask[0].pos[0] = cy - 160 -  bsize;
		LgrSpezButton.mask[1].pos[0] = cy - 160 -  bsize;
		LgrSpezButton.mask[2].pos[0] = cy - 160 -  bsize;
		***/

		ZusatzButton.mask[0].pos[0] = cy -  160 - 5 - 2 * bsize;

        WiegWindow  = CreateWindowEx (
                              0,
                              "AufWieg",
                              "",
                              WS_POPUP,
                              x, y,
                              cx, cy,
                              // hMainWindow,
                              hWnd,
                              NULL,
                              hMainInst,
                              NULL);
        if (WiegWindow == NULL) return;


        EnableWindow (hMainWindow, FALSE);

        WiegY = yw + cyw;
        wiegx  = xw;
        wiegy  = yw;
        wiegcx = cxw;
        wiegcy = cyw;

        WiegKg  = CreateWindowEx (
                              0,
                              "AufWiegKg",
                              "",
                              WS_CHILD | WS_BORDER,
                              xw, yw,
                              cxw, cyw,
                              WiegWindow,
                              NULL,
                              hMainInst,
                              NULL);

		KistCol = FALSE;
		SetClassLong (WiegKg, GCL_HBRBACKGROUND, (long) LtBlueBrush);
		SetKistRect (xw, yw);

        cxc = cx - 20;
        cyc = 150;
        xc  = 10;
        yc  = cy - 160;

        if (ohne_preis && strcmp (WiePreis.text1, "Preise") == 0)
        {
              WieButton.mask[1].attribut = REMOVED;
              GetZentrierWerte (&buglen, &buabs, cxc, 5, babs, bsize);
              WieButton.mask[0].pos[1] = (cxc - buglen) / 2;
              WieButton.mask[2].pos[1] = WieButton.mask[0].pos[1] + buabs;
              WieButton.mask[3].pos[1] = WieButton.mask[2].pos[1] + buabs;
              WieButton.mask[4].pos[1] = WieButton.mask[3].pos[1] + buabs;
              WieButton.mask[5].pos[1] = WieButton.mask[4].pos[1] + buabs;
        }
        else
        {
              WieButton.mask[1].attribut = COLBUTTON;
              GetZentrierWerte (&buglen, &buabs, cxc, 6, babs, bsize);
              WieButton.mask[0].pos[1] = (cxc - buglen) / 2;
              WieButton.mask[1].pos[1] = WieButton.mask[0].pos[1] + buabs;
              WieButton.mask[2].pos[1] = WieButton.mask[1].pos[1] + buabs;
              WieButton.mask[3].pos[1] = WieButton.mask[2].pos[1] + buabs;
              WieButton.mask[4].pos[1] = WieButton.mask[3].pos[1] + buabs;
              WieButton.mask[5].pos[1] = WieButton.mask[4].pos[1] + buabs;
        }

        GetZentrierWerte (&buglen, &buabs, cxc, 6, babs2, bsize2);
        WieButtonT.mask[0].pos[1] = (cxc - buglen) / 2;
        WieButtonT.mask[1].pos[1] = WieButtonT.mask[0].pos[1] + buabs;
        WieButtonT.mask[2].pos[1] = WieButtonT.mask[1].pos[1] + buabs;
        WieButtonT.mask[3].pos[1] = WieButtonT.mask[2].pos[1] + buabs;
        WieButtonT.mask[4].pos[1] = WieButtonT.mask[3].pos[1] + buabs;
        WieButtonT.mask[5].pos[1] = WieButtonT.mask[4].pos[1] + buabs;

        WiegCtr  = CreateWindowEx (
                              WS_EX_CLIENTEDGE,
                              "AufWiegCtr",
                              "",
                              WS_CHILD,
                              xc, yc,
                              cxc, cyc,
                              WiegWindow,
                              NULL,
                              hMainInst,
                              NULL);




			  //220712 A    Hier wird die Gr��e der Festtara-Buttons bestimmt (aus dem ptab-Text heraus)
				int i;
		        for (i = 0; i < AnzFestTara; i ++)
		        {
				  s = ffesttara.mask[i].length ;
				  z = ffesttara.mask[i].rows ;
				  s = strlen(ftabarr[i].ptbez) + 4; 
                  FontTextLen (ffesttara.font, ftabarr[i].ptbez, &s, &z);
                  ffesttara.mask[i].length = s;
                  ffesttara.mask[i].rows = z * 2;
				}
			  //220712 E
			  //220712 A  Hier werden die Festtara-Buttons positioniert
				  RECT CtrRect1;
				  RECT frect, frectWiegKg;
				  int xpos , xpos2, ypos, anzy = 0;  // anzy = Anzahl der Zeilen in der die Festtara-Buttons stehen
				  GetWindowRect (WiegWindow, &CtrRect1);
				  GetFrmRect (&artwform, &frect);
				  GetWindowRect (WiegKg, &frectWiegKg);
				  xpos = xpos2 = CtrRect1.left + 26;
				  ypos = frect.bottom + 26;

		        for (i = 0; i < 25; i ++)
		        {
					if (i < AnzFestTara)
					{
						ffesttara.mask[i].attribut =  COLBUTTON;
					}
					else
					{
						 ffesttara.mask[i].attribut =  REMOVED;
					}
				}

		        for (i = 0; i < AnzFestTara; i ++)
		        {

					/**
				  if (anzy > 2)
				  {
					  xpos = xpos2;
				      ypos += ffesttara.mask[0].rows + 4;
					  anzy++;
				  }
				  **/
				  if (anzy >= 7)
				  {
					  ffesttara.mask[i].attribut =  REMOVED;
					  continue;
				  }
				  if (anzy <= 2 && xpos > CtrRect1.right - ffesttara.mask[i].length)
				  {
					  xpos = xpos2;
				      ypos += ffesttara.mask[i].rows + 4;
					  anzy++;
				  }
				  else if (anzy > 2 && xpos > frectWiegKg.left - ffesttara.mask[i].length) //frectkgt.left = linke Position des gro�en Wiegefensters
				  {
					  xpos = xpos2;
				      ypos += ffesttara.mask[i].rows + 4;
					  anzy++;
				  }
			
				  ffesttara.mask[i].pos[0] = ypos;
				  ffesttara.mask[i].pos[1] = xpos;
                  KonvTextStrLen (ffesttara.font, ftabarr[i].ptbez, &s);
				  xpos += ffesttara.mask[i].length + 4;
                  if (i == 0) FestTara1.bmpx = s+6;
                  if (i == 1) FestTara2.bmpx = s+6;
                  if (i == 2) FestTara3.bmpx = s+6;
                  if (i == 3) FestTara4.bmpx = s+6;
                  if (i == 4) FestTara5.bmpx = s+6;
                  if (i == 5) FestTara6.bmpx = s+6;
                  if (i == 6) FestTara7.bmpx = s+6;
                  if (i == 7) FestTara8.bmpx = s+6;
                  if (i == 8) FestTara9.bmpx = s+6;
                  if (i == 9) FestTara10.bmpx = s+6;
                  if (i == 10) FestTara11.bmpx = s+6;
                  if (i == 11) FestTara12.bmpx = s+6;
                  if (i == 12) FestTara13.bmpx = s+6;
                  if (i == 13) FestTara14.bmpx = s+6;
                  if (i == 14) FestTara15.bmpx = s+6;
                  if (i == 15) FestTara16.bmpx = s+6;
                  if (i == 16) FestTara17.bmpx = s+6;
                  if (i == 17) FestTara18.bmpx = s+6;
                  if (i == 18) FestTara19.bmpx = s+6;
                  if (i == 19) FestTara20.bmpx = s+6;
                  if (i == 20) FestTara21.bmpx = s+6;
                  if (i == 21) FestTara22.bmpx = s+6;
                  if (i == 22) FestTara23.bmpx = s+6;
                  if (i == 23) FestTara24.bmpx = s+6;
                  if (i == 24) FestTara25.bmpx = s+6;
				}
			  //220712 E


		if (first < 2)
		{
				  RECT CtrRect;
				  GetWindowRect (WiegCtr, &CtrRect);
				  POINT LeftPoint;
				  LeftPoint.x = CtrRect.left;
				  LeftPoint.y = CtrRect.top;
				  ScreenToClient (WiegWindow, &LeftPoint);
				  LeftPoint.y -= ftexte.mask[0].rows;
				  ftexte.mask[0].pos[0] = (short) LeftPoint.y;
				  ftexte.mask[0].pos[1] = WieButton.mask[0].pos[1] + (short) LeftPoint.x;
				  LeftPoint.y = CtrRect.top;
				  LeftPoint.y -= LgrSpezButton.mask[0].rows + 20 ;
				LgrSpezButton.mask[0].pos[0] = (short) LeftPoint.y;
				LgrSpezButton.mask[1].pos[0] = (short) LeftPoint.y;
				LgrSpezButton.mask[2].pos[0] = (short) LeftPoint.y;

				  first = 2;
		}

        ShowWindow (WiegWindow, SW_SHOW);
        UpdateWindow (WiegWindow);
        ShowWindow (WiegKg, SW_SHOW);
        UpdateWindow (WiegKg);
        ShowWindow (WiegCtr, SW_SHOW);
        UpdateWindow (WiegCtr);
        create_enter_form (WiegWindow, &artwform, 0, 0);
        display_form (WiegWindow, &artwconst, 0, 0);
        current_wieg = TestAktivButton (PLUS, 0);
        SetFocus (WieButton.mask[current_wieg].feldid);

        WiegenAktiv = 1;
        current_wieg = 0;
        break_wieg = 0;

        while (GetMessage (&msg, NULL, 0, 0))
        {
              if (IsWiegMessage (&msg));
              else
              {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
              }
              if (break_wieg) break;
        }
        if (AlarmWindow)
        {
                     CloseControls (&alform);
                     DestroyWindow (AlarmWindow);
                     AlarmWindow = NULL;
        }
		a_lgr.a = 0.0;
        if ( lgr_class.lese_lgr_bz (akt_lager_zu) == 0) sprintf (LagertxtZu,"Nach: %s",lgr.lgr_bz);
        if ( lgr_class.lese_lgr_bz (akt_lager_ab) == 0) sprintf (LagertxtAb,"Von: %s",lgr.lgr_bz);
		DestroyKistBmp ();
        EnableWindow (hMainWindow, TRUE);
        CloseControls (&WiegKgT);
        CloseControls (&WiegKgE);
        CloseControls (&WieButton);
        CloseControls (&EtiButton);
        CloseControls (&EtiSpezButton);
        CloseControls (&LgrSpezButton);
        CloseControls (&ZusatzButton);
        CloseControls (&artwform);
        CloseControls (&artwformT);
        CloseControls (&artwconst);
        CloseControls (&artwconstT);
        CloseControls (&startwieg);
        CloseControls (&fEntnahmeGewTxt);
        CloseControls (&fEntnahmeGew);
        CloseControls (&WiegKgMe);
        CloseControls (&WiegEinAusgang);
        CloseControls (&ftara);
        CloseControls (&ftaratxt);
        CloseControls (&sw_abzug);
        CloseFontControls (&ftexte);
        CloseFontControls (&ffesttara); //220712 

        DestroyWindow (WiegWindow);
//        SetListEWindow (1);
        WiegenAktiv = 0;
}

/** Ab hier Auswahl fuer Preiseingabe
**/

static HWND ePreisWindow;
static break_preis_enter = 0;


mfont PreisTextFont = {"Arial", 200, 0, 1, RGB (0, 0, 0),
                                         RGB (255, 255, 255),
                                         1,
                                         NULL};


mfont preisfont   = {"", 120, 0, 1,
                     RGB (255, 0, 0),
                     BLUECOL,
                     1,
                     NULL};


ColButton BuVkPreis =   {"&EK-Preis", -1, 25,
                         "Preis", -1, 60,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         2};

ColButton BuLadPreis =   {"&Netto-", -1, 25,
                          "Preis", -1, 60,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 0, 0),
                         YELLOWCOL,
                         2};

ColButton BuPreisOK =   {"&Zur�ck", -1, -1,
                          NULL, 0, 0,
                          NULL, 0, 0,
                          NULL, 0, 0,
                          NULL, 0, 0,
                          RGB (255, 255, 255),
                          REDCOL,
                          2};


static HWND fhWnd;
static int pnbss0 = 80;
int pny    = 3;
int pnx    = 1;
static int pcbss0 = 100;
static int pcbss  = 64;
static int pcbsz  = 64;
static int pcbsz0 = 100;



/*

Auswahl fuer Festtara.

*/

struct PT
{
        char ptbez [33];
        char ptwer1 [9];
};

struct PT ptab, ptabarr [50];
static int ptabanz = 0;
static int ptabmaxanz = 49;


static int ptidx;

mfont PtFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                               RGB (255, 255, 255),
                               1,
                               NULL};

static field _ftaraform [] =
{
        ptab.ptbez,     12, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        ptab.ptwer1,     9, 1, 0,13, 0, "%7.3f", DISPLAYONLY, 0, 0, 0
};

static form ftaraform = {2, 0, 0, _ftaraform, 0, 0, 0, 0, &PtFont};

static field _ftaraub [] =
{
        "Bezeichnung",     12, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        "Tara",             9, 1, 0,13, 0, "", DISPLAYONLY, 0, 0, 0
};

static form ftaraub = {2, 0, 0, _ftaraub, 0, 0, 0, 0, &PtFont};

static field _ftaravl [] =
{      "1",                  1,  0, 0, 12, 0, "", NORMAL, 0, 0, 0,
};

static form ftaravl = {1, 0, 0, _ftaravl, 0, 0, 0, 0, &PtFont};

int ShowFestTara (char *pttara)
/**
Ausahlfenster fuer Festtara anzeigen.
**/
{
        if (ptabanz == 0)
        {
               dsqlstatus = ptab_class.lese_ptab_all ("tara_fest");
               while (dsqlstatus == 0)
               {
                   strcpy (ptabarr[ptabanz].ptbez,  ptabn.ptbez);
                   strcpy (ptabarr[ptabanz].ptwer1, ptabn.ptwer1);
                   ptabanz ++;
                   if (ptabanz == 20) break;
                   dsqlstatus = ptab_class.lese_ptab_all ();
               }
        }
        EnableWindow (WiegWindow, FALSE);
        ptidx = ShowPtBox (WiegWindow, (char *) ptabarr, ptabanz,
                     (char *) &ptab,(int) sizeof (struct PT),
                     &ftaraform, &ftaravl, &ftaraub);
        EnableWindow (WiegWindow, TRUE);
        strcpy (pttara, ptabarr [ptidx].ptwer1);
        return ptidx;
}


void SetFestTara_ty (int i , int len2, int FontHigh) //220712
{
	   if (AnzFestTaraLen > 130)  ffesttarafont.FontHeight = 130;
	   if (AnzFestTaraLen > 150)  ffesttarafont.FontHeight = 115;
	   if (AnzFestTaraLen > 250)  ffesttarafont.FontHeight = 110;
	   if (AnzFestTaraLen > 350)  ffesttarafont.FontHeight = 100;
	   if (AnzFestTaraLen > 450)  ffesttarafont.FontHeight = 90;

	int ty1, ty2;
	if (len2 > 0)
	{
		if (FontHigh >= 130) { ty1 = 3; ty2 = 25; } 
		if (FontHigh < 130) { ty1 = 3; ty2 = 20; } 
		if (FontHigh < 115) { ty1 = 5; ty2 = 15; } 
	}
	else
	{
		ty1 = -1;
		ty2 = -1;
	}
	if (i == 0) { FestTara1.ty1 = ty1; FestTara1.ty2 = ty2;}
	if (i == 1) { FestTara2.ty1 = ty1; FestTara2.ty2 = ty2;}
	if (i == 2) { FestTara3.ty1 = ty1; FestTara3.ty2 = ty2;}
	if (i == 3) { FestTara4.ty1 = ty1; FestTara4.ty2 = ty2;}
	if (i == 4) { FestTara5.ty1 = ty1; FestTara5.ty2 = ty2;}
	if (i == 5) { FestTara6.ty1 = ty1; FestTara6.ty2 = ty2;}
	if (i == 6) { FestTara7.ty1 = ty1; FestTara7.ty2 = ty2;}
	if (i == 7) { FestTara8.ty1 = ty1; FestTara8.ty2 = ty2;}
	if (i == 8) { FestTara9.ty1 = ty1; FestTara9.ty2 = ty2;}
	if (i == 9) { FestTara10.ty1 = ty1; FestTara10.ty2 = ty2;}
	if (i == 10) { FestTara11.ty1 = ty1; FestTara11.ty2 = ty2;}
	if (i == 11) { FestTara12.ty1 = ty1; FestTara12.ty2 = ty2;}
	if (i == 12) { FestTara13.ty1 = ty1; FestTara13.ty2 = ty2;}
	if (i == 13) { FestTara14.ty1 = ty1; FestTara14.ty2 = ty2;}
	if (i == 14) { FestTara15.ty1 = ty1; FestTara15.ty2 = ty2;}
	if (i == 15) { FestTara16.ty1 = ty1; FestTara16.ty2 = ty2;}
	if (i == 16) { FestTara17.ty1 = ty1; FestTara17.ty2 = ty2;}
	if (i == 17) { FestTara18.ty1 = ty1; FestTara18.ty2 = ty2;}
	if (i == 18) { FestTara19.ty1 = ty1; FestTara19.ty2 = ty2;}
	if (i == 19) { FestTara20.ty1 = ty1; FestTara20.ty2 = ty2;}
	if (i == 20) { FestTara21.ty1 = ty1; FestTara21.ty2 = ty2;}
	if (i == 21) { FestTara22.ty1 = ty1; FestTara22.ty2 = ty2;}
	if (i == 22) { FestTara23.ty1 = ty1; FestTara23.ty2 = ty2;}
	if (i == 23) { FestTara24.ty1 = ty1; FestTara24.ty2 = ty2;}
	if (i == 24) { FestTara25.ty1 = ty1; FestTara25.ty2 = ty2;}
}

void BelegeFestTara (void) //220712
/**
Ausahlfenster fuer Festtara anzeigen.
**/
{
	if (QsPosWieg == TRUE) return;
    AnzFestTara = 0;
	AnzFestTaraLen = 0;

    if (AnzFestTara == 0)
    {
       dsqlstatus = ptab_class.lese_ptab_all ("tara_fest");
       while (dsqlstatus == 0)
       {
		   if (atoi(ptabn.ptwer2) > 0)
		   {
	            strcpy (ftabarr[AnzFestTara].ptbez,  clipped(ptabn.ptbez));
				strcpy (ftabarr[AnzFestTara].ptbezk,  clipped(ptabn.ptbezk));
				strcpy (ftabarr[AnzFestTara].ptwer1, ptabn.ptwer1);
				strcpy (ftabarr[AnzFestTara].ptwer2, ptabn.ptwer2);
				AnzFestTaraLen += strlen(clipped(ptabn.ptbez)) + 2;
				AnzFestTara ++;
		   }
            if (AnzFestTara == 25) break;
            dsqlstatus = ptab_class.lese_ptab_all ();
       }
	   if (AnzFestTaraLen > 130)  ffesttarafont.FontHeight = 130;
	   if (AnzFestTaraLen > 150)  ffesttarafont.FontHeight = 115;
	   if (AnzFestTaraLen > 250)  ffesttarafont.FontHeight = 110;
	   if (AnzFestTaraLen > 350)  ffesttarafont.FontHeight = 100;
	   if (AnzFestTaraLen > 450)  ffesttarafont.FontHeight = 90;
	   int i;
	   for (i = 0; i<AnzFestTara; i++)
	   {
			SetFestTara_ty(i,strlen(ftabarr[i].ptbezk),ffesttarafont.FontHeight);
	   }
	}
}


void FillQsPosWieg (double a)  //220712
/**
Texte fuer Qualitaetssicherung fuellen. in der Wiegemaske
**/
{

  // char txt [25];
    int i;
//	short sort; 
    short hwg = 0;
    short wg = 0;
    long ag = 0l;
	int dsqlstatus = 100;	



    i = 0;
    AnzFestTara = 0;
	AnzFestTaraLen = 0;
	qstxt.qstyp = 2;
	qstxt.hwg = _a_bas.hwg;
	qstxt.wg = _a_bas.wg;
	qstxt.ag = _a_bas.ag;
    dsqlstatus = QsTxtClass.dbreadfirst ();
	if (dsqlstatus == 100)
	{
		qstxt.ag = 0;
	    dsqlstatus = QsTxtClass.dbreadfirst ();
		if (dsqlstatus == 100)
		{
			qstxt.wg = 0;
		    dsqlstatus = QsTxtClass.dbreadfirst ();
			if (dsqlstatus == 100)
			{
				qstxt.hwg = 0;
				dsqlstatus = QsTxtClass.dbreadfirst ();
			}
		}
	}
    while (dsqlstatus == 0)
    {
	            strcpy (ftabarr[AnzFestTara].ptbez,  clipped(qstxt.txt));
	            strcpy (ftabarr[AnzFestTara].ptbezk,  "");
				strcpy (ftabarr[AnzFestTara].ptwer1, "0");
				strcpy (ftabarr[AnzFestTara].ptwer2, "0");
				AnzFestTaraLen += strlen(clipped(qstxt.txt)) + 2;
	  		    AnzFestTara ++;
	            if (AnzFestTara == 25) break;
				dsqlstatus = QsTxtClass.dbread();
    }
   if (AnzFestTaraLen > 130)  ffesttarafont.FontHeight = 130;
   if (AnzFestTaraLen > 150)  ffesttarafont.FontHeight = 115;
   if (AnzFestTaraLen > 250)  ffesttarafont.FontHeight = 110;
   if (AnzFestTaraLen > 350)  ffesttarafont.FontHeight = 100;
   if (AnzFestTaraLen > 450)  ffesttarafont.FontHeight = 90;
   for (i = 0; i<AnzFestTara; i++)
   {
		SetFestTara_ty(i,strlen(ftabarr[i].ptbezk),ffesttarafont.FontHeight);
   }
   ReadQsPosWieg ();

}


/*

L�nderauswahl fuer Rindfleisch.

*/


static field _flandform [] =
{
        ptab.ptwer1,    20, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
//        ptab.ptbez,     12, 1, 0,10, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form flandform = {1, 0, 0, _flandform, 0, 0, 0, 0, &PtFont};

static field _flandub [] =
{
        "Wert",              20, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
//        "Beschreibung",      12, 1, 0,10, 0, "", DISPLAYONLY, 0, 0, 0
};

static form flandub = {1, 0, 0, _flandub, 0, 0, 0, 0, &PtFont};

static field _flandvl [] =
{ 
	"1",                  1,  0, 0, 10, 0, "", NORMAL, 0, 0, 0,
};

static form flandvl = {0, 0, 0, _flandvl, 0, 0, 0, 0, &PtFont};

int ShowTierLand (char *value)
/**
Ausahlfenster fuer Festtara anzeigen.
**/
{
        if (ptabanz == 0)
        {
               dsqlstatus = ptab_class.lese_ptab_all ("tier_land");
               while (dsqlstatus == 0)
               {
                   strcpy (ptabarr[ptabanz].ptbez,  ptabn.ptbez);
                   strcpy (ptabarr[ptabanz].ptwer1, ptabn.ptwer1);
                   ptabanz ++;
                   if (ptabanz == 20) break;
                   dsqlstatus = ptab_class.lese_ptab_all ();
               }
        }
        EnableWindow (WiegWindow, FALSE);
        ptidx = ShowPtBox (WiegWindow, (char *) ptabarr, ptabanz,
                     (char *) &ptab,(int) sizeof (struct PT),
                     &flandform, &flandvl, &flandub);
        EnableWindow (WiegWindow, TRUE);
        strcpy (value, ptabarr [ptidx].ptwer1);
        return ptidx;
}


/*

Auswahl fuer Drucker.

*/

struct DR
{
        char drucker [33];
};

struct DR ldrucker, ldruckarr [20];
static int ldruckanz = 0;
static int ldidx;

mfont DrFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                               RGB (255, 255, 255),
                               1,
                               NULL};
static field _ldruckform [] =
{
        ldrucker.drucker,    22, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form ldruckform = {1, 0, 0, _ldruckform, 0, 0, 0, 0, &DrFont};

static field _ldruckub [] =
{
        "Drucker",     22, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form ldruckub = {1, 0, 0, _ldruckub, 0, 0, 0, 0, &DrFont};


void BelegeGdiDrucker (void)
/**
Drucker Win.ini holen
**/
{
       int i;
	   char *pr;
//	   char AktDevice [80];
       char ziel [256];
       char quelle [256];
       char *frmenv;

       frmenv = getenv ("FRMPATH");
       if (frmenv == NULL) return;

       GetAllPrinters ();

       i = 0;
       while (TRUE)
       {
                pr = GetNextPrinter ();
                if (pr == NULL) break;
                strcpy (ldruckarr[i].drucker, pr);
                i ++;
       }
       ldruckanz = i;

       sprintf (quelle, "%s\\%s", getenv ("FRMPATH"), "gdi.cfg");
       sprintf (ziel, "%s\\%s", getenv ("FRMPATH"), "drucker.akt");
       CopyFile (quelle, ziel, FALSE);
       sprintf (ziel, "%s\\%s.%s", getenv ("FRMPATH"),
                                 clipped (sys_ben.pers_nam),
                                 "akt");
       CopyFile (quelle, ziel, FALSE);

}


void BelegeDrucker (void)
/**
Drucker aus Formatverzeichnis holen.
**/
{
      char frmdir [256];
      char *frmenv;
      WIN32_FIND_DATA lpffd;
      HANDLE hFile;
      char *p;
      int i;
      static int druckerOK = 0;

      if (druckerOK) return;

      druckerOK = 1;
      if (GraphikMode)
      {
               BelegeGdiDrucker ();
               return;
      }
      frmenv = getenv ("FRMPATH");
      if (frmenv == NULL) return;

      sprintf (frmdir, "%s\\*.cfg", frmenv);


      hFile = FindFirstFile (frmdir, &lpffd);

      if (hFile == INVALID_HANDLE_VALUE)
      {
                      return;
      }

      i = 0;
      p = strchr (lpffd.cFileName, '.');
      if (p) *p = (char) 0;

      strcpy (ldruckarr[i].drucker, lpffd.cFileName);
      i ++;

      while (FindNextFile (hFile, &lpffd))
      {
               p = strchr (lpffd.cFileName, '.');
               if (p) *p = (char) 0;
               strcpy (ldruckarr[i].drucker, lpffd.cFileName);
               i ++;
      }
      FindClose (hFile);

      ldruckanz = i;
}

void drucker_akt (char *dname)
/**
Ausgewaehlten Drucker in drucker.akt_uebertragen.
**/
{
        char ziel [256];
        char quelle [256];

		if (GraphikMode)
		{
		        SetPrinter (dname);
				return;
		}
        sprintf (quelle, "%s\\%s.cfg", getenv ("FRMPATH"), dname);
        sprintf (ziel, "%s\\%s", getenv ("FRMPATH"), "drucker.akt");
        CopyFile (quelle, ziel, FALSE);
        sprintf (ziel, "%s\\%s.%s", getenv ("FRMPATH"),
                                 clipped (sys_ben.pers_nam),
                                 "akt");
        CopyFile (quelle, ziel, FALSE);
}

int DruckChoise ()
/**
Ausahlfenster fuer Festtara anzeigen.
**/
{
        char aktdrucker [80];

        BelegeDrucker ();
        ldidx = ShowPtBox (eWindow, (char *) ldruckarr, ldruckanz,
                     (char *) &ldrucker,(int) sizeof (struct DR),
                     &ldruckform, NULL, &ldruckub);
        strcpy (aktdrucker, ldruckarr [ldidx].drucker);
        clipped (aktdrucker);
        drucker_akt (aktdrucker);
        CloseControls (&testub);
		display_form (Getlbox (), &testub, 0, 0);
        return ldidx;
}


/** Ptabanzeige
**/

/*
static int ptbss0 = 73;
static int ptbss  = 77;
static int ptbsz  = 40;
static int ptbsz0 = 36;
*/

static int ptbss0 = 103;
static int ptbss  = 107;
static int ptbsz  = 60;
static int ptbsz0 = 56;


field _PtButton[] = {
(char *) &Back,         ptbss0, ptbsz0, -1, 4+0*ptbss, 0, "", COLBUTTON, 0,
 *                                                        func5, 0,
(char *) &BuOKS,        ptbss0, ptbsz0, -1, 4+1*ptbss, 0, "", COLBUTTON, 0,
 *                                                        func6, 0,
(char *) &PfeilU,       ptbss0, ptbsz0, -1, 4+2*ptbss, 0, "", COLBUTTON, 0,
                                                          func3, 0,
(char *) &PfeilO,       ptbss0, ptbsz0, -1, 4+3*ptbss, 0, "", COLBUTTON, 0,
                                                          func2, 0
};

form PtButton = {4, 0, 0, _PtButton, 0, 0, 0, 0, &buttonfont};


void IsPtClck (int idx)
/**
Reaktion auf Doppelclick in der Liste.
**/
{
        ptidx = idx;
        break_list ();
        return;
}

void RegisterPt (void)
/**
Kopf - und Fuss - Fenster fuer Liste registrieren
**/
{
        static int registered = 0;
        WNDCLASS wc;

        if (registered == 0)
        {
                   wc.style =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                               | CS_OWNDC;
                   wc.cbClsExtra    =  0;
                   wc.cbWndExtra    =  0;
                   wc.hInstance     =  hMainInst;
                   wc.hIcon         =  LoadIcon (hMainInst, "FITICON");
                   wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                   wc.lpszMenuName  =  "";
                   wc.lpfnWndProc   =  PtFussProc;
                   wc.lpszMenuName  =  "";
                   wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
                   wc.lpszClassName =  "PtFuss";
                   RegisterClass(&wc);
                   registered = 1;
        }
}

HWND IsPtFussChild (POINT *mousepos)
/**
Test, ob sich der Maus-Cursor in einem KindFensert befinden.
**/
{
        int i;

        for (i = 0; i < PtButton.fieldanz; i ++)
        {
                   if (MouseinWindow (PtButton.mask[i].feldid,
                                      mousepos))
                   {
                                 return (PtButton.mask[i].feldid);
                   }
        }
        return NULL;
}

int MouseInPt (POINT *mpos)
{
        HWND hListBox;

        hListBox = GethListBox ();

        if (MouseinDlg (hListBox, mpos)) return TRUE;
        if (MouseinDlg (PtFussWindow, mpos)) return TRUE;
        return FALSE;
}


LONG FAR PASCAL PtFussProc(HWND hWnd,UINT msg,
                           WPARAM wParam,LPARAM lParam)
{
        HWND enchild;
        POINT mousepos;
        HWND lbox;

        GetCursorPos (&mousepos);

        switch(msg)
        {
              case WM_PAINT :
                    display_form (PtFussWindow, &PtButton, 0, 0);
                    break;
              case WM_LBUTTONDOWN :
              case WM_LBUTTONUP :
              case WM_MOUSEMOVE :
                    enchild = IsPtFussChild (&mousepos);
//                    MouseTest (enchild, msg);
                    if (enchild)
                    {
                              SendMessage (enchild, msg, wParam, lParam);
                              return TRUE;
                    }
                    lbox = Getlbox ();
                    if (MouseinWindow (lbox, &mousepos))
                    {
                              ScreenToClient (lbox, &mousepos);
                              lParam = MAKELONG (mousepos.x, mousepos.y);
                              SendMessage (lbox, msg, wParam, lParam);
                              return TRUE;
                    }
                    return TRUE;
              case WM_VSCROLL :
                    lbox = Getlbox ();
                    if (MouseinWindow (lbox, &mousepos))
                    {
                              SendMessage (lbox, msg, wParam, lParam);
                    }
                    return TRUE;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

int ShowPtBox (HWND hWnd, char * ptabarr, int ptabanz, char * ptab,
               int ptsize, form *ptform, form *ptvl, form *ptub)
{
        RECT rect;
        RECT frmrect;
        int x, y, cx, cy;
        HWND shListBox;
        HWND slbox;
        struct LMENUE smenue;

        GetMenue (&smenue);
        MenTextSave (&smenue);
        shListBox = GethListBox ();
        slbox = Getlbox ();

        RegisterPt ();
        GetWindowRect (hMainWindow, &rect);
        GetFrmRectEx (ptform, ptform->font, &frmrect);

        x = rect.left + 50;
        y = rect.top + 20;
        cx = frmrect.right + 45;
		cy = 185;
		if (ptabanz > 4)
		{
			cy = 285;
		}
		if (ptabanz > 7)
		{
			cy = 385;
		}
		

        set_fkt (endlist, 5);
        SetDblClck (IsPtClck, 0);
        PtListeAktiv = 1;
        SetAktivWindow (hWnd);
//        SetListEWindow (1);
        SetListFont (TRUE);
        spezfont (ptform->font);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_VISIBLE);
        PtWindow = OpenListWindowBoEx (x, y, cx, cy);
//        PtWindow = OpenListWindowEnEx (x, y, cx, cy);
        GetWindowRect (PtWindow, &rect);
        y = rect.bottom;
		cx = rect.right - rect.left;
        PtFussWindow = CreateWindow ("PtFuss",
                                    "",
                                    WS_POPUP | WS_VISIBLE | WS_DLGFRAME,
                                    x,
//                                    y + cy + 23,
                                    y,
                                    cx, 70,
                                    hWnd,
                                    NULL,
                                    hMainInst,
                                    NULL);
        if (ptvl) ElistVl (ptvl);
        if (ptub) ElistUb (ptub);
		AuswahlAktiv = TRUE;
        ShowElist ((char *) ptabarr,
                  ptabanz,
                  (char *) ptab,
                  (int) ptsize,
                  ptform);
//        SetMouseLock (TRUE);
        SetMouseLockProc (MouseInPt);
        EnterElist (PtWindow, (char *) ptabarr,
                             ptabanz,
                             (char *) ptab,
                             (int) ptsize,
                             ptform);
		AuswahlAktiv = FALSE;
        CloseControls (&PtButton);
        CloseEWindow (PtWindow);
        init_break_list ();
//        SetMouseLock (FALSE);
        SetMouseLockProc (NULL);
        DestroyWindow (PtFussWindow);
        PtListeAktiv = 0;
        SetMenue (&smenue);
        SethListBox (shListBox);
        Setlbox (slbox);
        SetAktivWindow (hWnd);
        return ptidx;
}

/**
Lieferschein auf komplett setzen.
**/

static int ldruck   = 0;
static int ldrfr    = 0;
static int lfrei    = 0;
static int lbar     = 0;
static int lsofort  = 0;
static int drwahl   = 0;

static int *lbuttons [] = {&ldruck, &ldrfr, &drwahl, NULL};

static int Setldruck (void);
static int Setlfrei (void);
static int Setldrfr (void);

/*
static int Setlbar (void);
static int Setlsofort (void);
*/
static int Setdrwahl (void);

static int BreakBox (void);
static int BoxBreak (void);

static int IsDrChoise;


static field _cbox [] = {
" LS Drucken      ",     22, 2, 0, 6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             Setldruck, 101,
" LS Komplett setzen" ,  22, 2, 2, 6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             Setlfrei, 102,
" Druckerwahl     ",     19, 2, 4, 6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             Setdrwahl, 106,
"   OK    ",             10, 1, 7, 2,  0, "", BUTTON, 0, BreakBox, 107,
" Abbruch ",             10, 1, 7, 15, 0, "", BUTTON, 0, BoxBreak, KEY5};

form cbox = {5, 0, 0, _cbox, 0,0,0,0,&ListFont};

int KomplettEnd (void)
/**
EnterButton abbrechen.
**/
{
           if (syskey == KEY5)
           {
                    break_enter ();
           }
           return 1;
}

void UnsetBox (int pos)
/**
Buttona auf selectierte setzen. (ausser pos);
**/
{
        int i;

        for (i = 0; lbuttons [i]; i ++)
        {
                  if (i == pos) continue;

                  SendMessage (cbox.mask[i].feldid, BM_SETCHECK, 0, 0l);
                  *lbuttons [i] = 0;
        }
}


int Setldruck (void)
/**
Druck setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (cbox.mask[0].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       ldruck = 0;
          }
          else
          {
                       ldruck = 1;
          }
          if (syskey == KEYCR)
          {
                       SendMessage (cbox.mask[0].feldid, BM_SETCHECK,
                       !ldruck, 0l);
                       ldruck = !ldruck;
          }
          UnsetBox (0);
          return 1;
}
void ldruckdefault (void)
{
          ldruck = 1;
//          SendMessage (cbox.mask[0].feldid, BM_SETCHECK,
//          ldruck, 0l);
          UnsetBox (0);
}


int Setldrfr (void)
/**
Freigabe und Druck setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (cbox.mask[1].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                        ldrfr = 0;
          }
          else
          {
                        ldrfr = 1;
          }
          if (syskey == KEYCR)
          {
                       SendMessage (cbox.mask[1].feldid, BM_SETCHECK,
                       !ldrfr, 0l);
                       ldrfr = !ldrfr;
          }
          UnsetBox (1);
          return 1;
}

int Setlfrei (void)

/**
Freigabe setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (cbox.mask[1].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       lfrei = 0;
          }
          else
          {
                       lfrei = 1;
          }
          if (syskey == KEYCR)
          {
                       SendMessage (cbox.mask[1].feldid, BM_SETCHECK,
                       !lfrei, 0l);
                       lfrei = !lfrei;
          }
          UnsetBox (1);
          return 1;
}

int Setlbar (void)

/**
Barverkauf setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (cbox.mask[3].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       lbar = 0;
          }
          else
          {
                       lbar = 1;
          }
          if (syskey == KEYCR)
          {
                       SendMessage (cbox.mask[3].feldid, BM_SETCHECK,
                       !lbar, 0l);
                       lbar = !lbar;
          }
          UnsetBox (3);
          return 1;
}

int Setlsofort (void)

/**
Sofortrechnung setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (cbox.mask[4].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       lsofort = 0;
          }
          else
          {
                       lsofort = 1;
          }
          if (syskey == KEYCR)
          {
                       SendMessage (cbox.mask[4].feldid, BM_SETCHECK,
                       !lsofort, 0l);
                       lsofort = !lsofort;
          }
          UnsetBox (4);
          return 1;
}

int Setdrwahl (void)

/**
Sofortrechnung setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          DruckChoise ();
          set_fkt (KomplettEnd, 5);
          SetFocus (cbox.mask[5].feldid);
          return 1;
}


int BreakBox (void)
/**
EnterButton abbrechen.
**/
{
           if (syskey == KEYUP || syskey == KEYDOWN) return 0;
           syskey = KEYCR;
           break_enter ();
           return 1;
}

int BoxBreak (void)
/**
EnterButton abbrechen.
**/
{
           if (syskey == KEYUP || syskey == KEYDOWN) return 0;
           syskey = KEY5;
           break_enter ();
           return 1;
}

int MouseInButton (POINT *mpos)
{
        int i;

        if (MouseinDlg (MainButton2.mask[2].feldid, mpos))
        {
                  return TRUE;
        }

        for (i = 5; i < 7; i ++)
        {
                if (MouseinDlg (MainButton2.mask[i].feldid, mpos))
                {
                                   return TRUE;
                }
        }
        return FALSE;
}

static void SelectAktCtr (void)
/**
Aktiven Button in EnterButton selectieren.
**/
{
        syskey = KEYCR;
        if (cbox.mask[currentfield].after)
        {
                       (*cbox.mask[currentfield].after) ();
        }
        syskey = KEYCR;
}

BOOL SetLskStatus (int ls_stat)
/**
ls_stat auf 3 setzen.
**/
{
        return TRUE;
}

void BearbKomplett (void)
/**
Auswahl aus Komplett bearbeiten.
**/
{
	short BestandBuchen = 0 ; //weldr darf nicht mehr buchen, da dies jetzt schon hier geschieht 190709
	short JourErzeugen = 1 ; //we_jour wird schon geschrieben, wenn nur gedruckt wurde 200709


         if (strcmp (sys_ben.pers_nam, " ") <= 0)
         {
             strcpy (sys_ben.pers_nam, "fit");
         }
         commitwork ();

         if (ldruck && DrKomplett)
         {
// 09.12.2003 aud Vorschalg von Detlef wird beim Drucken der Lieferschein sofort
// auf komplett gesetzt. Ist einstellbar �ber DrKomplett

/***
                sprintf (progname, "%s\\BIN\\rswrun -q dl weldr L "
                                     "%hd %hd %s %s X %hd %hd",
                                     rosipath,
                                     zerl_k.mdn, zerl_k.fil, zerl_k.lief, zerl_k.lief_rech_nr, DruckAuswahl,BestandBuchen);
                ProcWaitExecEx (progname, SW_SHOWMINIMIZED, 0, 40, -1, 0);
				***/
         }
         else if (ldruck)
         {
			 /*****
                sprintf (progname, "%s\\BIN\\rswrun weldr L "
                                     "%hd %hd %s %s D %hd %hd %hd",
                                     rosipath,
                                     zerl_k.mdn, zerl_k.fil, zerl_k.lief, zerl_k.lief_rech_nr, DruckAuswahl,BestandBuchen,JourErzeugen);
                ProcWaitExecEx (progname, SW_SHOWMINIMIZED, 0, 40, -1, 0);
				****/
         }
         else if (lfrei)
         {
			 /*****

                sprintf (progname, "%s\\BIN\\rswrun weldr L "
                                     "%hd %hd %s %s K %hd %hd",
                                     rosipath,
                                     zerl_k.mdn, zerl_k.fil, zerl_k.lief,
									 zerl_k.lief_rech_nr, DruckAuswahl,BestandBuchen);

                ProcWaitExecEx (progname, SW_SHOWMINIMIZED, 0, 40, -1, 0);
				*****/
				break_list ();
         }
         beginwork ();
}

void WriteLief (int idx)
{
    A_BEST_CLASS Best;

    a_best.mdn = akt_mdn;
    a_best.fil = akt_fil;
    a_best.a   = ratod (aufparr[idx].a);
    Best.dbreadfirst ();
    strcpy (a_best.letzt_lief_nr, zerl_k.lief);
    Best.dbupdate ();
}



void WritePr (int idx)
{
    double pr_ek_nto = ratod (aufparr[idx].ls_lad_euro); 
	double inh = aufparr[idx].min_best;
	int me_kz = aufparr[idx].me_kz;
    double we_me = ratod (aufparr[idx].auf_me); 

	if (inh == 0.0) 
	{
		inh = 1.0;
	}

    if (EktoMeEinh || me_kz == 2)
    {
			 pr_ek_nto /= inh;
    }

  
    if (_a_bas.a_typ == 1)
    {
           A_KALKHNDW_CLASS *Hndw = new A_KALKHNDW_CLASS ();
           a_kalkhndw.mdn = akt_mdn;
           a_kalkhndw.fil = akt_fil;
           a_kalkhndw.a   = ratod (aufparr[idx].a);
           int dsqlstatus = Hndw->dbreadfirst ();
           if (dsqlstatus == 0)
           {
               a_kalkhndw.pr_ek3 = a_kalkhndw.pr_ek2;
               a_kalkhndw.pr_ek2 = a_kalkhndw.pr_ek1;
               a_kalkhndw.we_me3 = a_kalkhndw.we_me2;
               a_kalkhndw.we_me2 = a_kalkhndw.we_me1;
           }
           a_kalkhndw.pr_ek1 = pr_ek_nto;
           a_kalkhndw.we_me1 = we_me;
           Hndw->dbupdate ();
           delete Hndw;
           WriteLief (idx);
    }
    else if (_a_bas.a_typ == 5 ||
             _a_bas.a_typ == 6 ||
             _a_bas.a_typ == 9)
    {
           A_KALK_MAT_CLASS *Mat = new A_KALK_MAT_CLASS ();
           a_kalk_mat.mdn = akt_mdn;
           a_kalk_mat.fil = akt_fil;
           a_kalk_mat.a   = ratod (aufparr[idx].a);
           int dsqlstatus = Mat->dbreadfirst ();
           a_kalk_mat.mat_o_b = pr_ek_nto;
           Mat->dbupdate ();
           delete Mat;
    }
    else if (_a_bas.a_typ2 == 1)
    {
           A_KALKHNDW_CLASS *Hndw = new A_KALKHNDW_CLASS ();
           a_kalkhndw.mdn = akt_mdn;
           a_kalkhndw.fil = akt_fil;
           a_kalkhndw.a   = ratod (aufparr[idx].a);
           int dsqlstatus = Hndw->dbreadfirst ();
           if (dsqlstatus == 0)
           {
               a_kalkhndw.pr_ek3 = a_kalkhndw.pr_ek2;
               a_kalkhndw.pr_ek2 = a_kalkhndw.pr_ek1;
               a_kalkhndw.we_me3 = a_kalkhndw.we_me2;
               a_kalkhndw.we_me2 = a_kalkhndw.we_me1;
           }
           a_kalkhndw.pr_ek1 = pr_ek_nto;
           a_kalkhndw.we_me1 = we_me;
           Hndw->dbupdate ();
           delete Hndw;
           WriteLief (idx);
    }
    else if (_a_bas.a_typ2 == 5 ||
             _a_bas.a_typ2 == 6 ||
             _a_bas.a_typ2 == 9)
    {
           A_KALK_MAT_CLASS *Mat = new A_KALK_MAT_CLASS ();
           a_kalk_mat.mdn = akt_mdn;
           a_kalk_mat.fil = akt_fil;
           a_kalk_mat.a   = ratod (aufparr[idx].a);
           int dsqlstatus = Mat->dbreadfirst ();
           a_kalk_mat.mat_o_b = pr_ek_nto;
           Mat->dbupdate ();
           delete Mat;
    }
}


void WritePreise (void)
/*
Einkaufspreise zurueckschreiben.
**/
{
	//290709 mit sql_mode = 1!!!
	    short sqlm;
    extern short sql_mode;

    sqlm = sql_mode;
    sql_mode = 1;

         for (int i = 0; i < aufpanz; i ++)
         {
             lese_a_bas (ratod (aufparr[i].a));
             WritePr (i);
         }
   sql_mode = sqlm;
}

int SetKomplett ()
/**
Komplett setzen.
**/
{
         int x,y;
         int cx, cy;
		 int cy0;
         int mx;
         RECT rect;
         TEXTMETRIC tm;
         HDC hdc;
         HFONT hFont;
//		 int i;

/*
         if (abfragejnG (eWindow,
                          "Wareneingang abschlie�en ?\n", "J") == 0)

		 {
                          return 0;
		 }
*/

         if (TestQsPos () == FALSE)
         {
             return 0;
         }
 	     y =  3;
		 cy = 7;
         set_fkt (KomplettEnd, 5);
         spezfont (&ListFont);
         hFont = SetWindowFont (hMainWindow);
         hdc = GetDC (hMainWindow);
         SelectObject (hdc, hFont);
         GetTextMetrics (hdc, &tm);
         DelFont (hFont);
         ReleaseDC (hMainWindow, hdc);
         GetClientRect (eWindow, &rect);
         mx = rect.right / tm.tmAveCharWidth;
         GetFrmSize (&cbox, &cx, &cy0);

         x = max (0, (mx - cx) / 2);

         KomplettAktiv = 1;

         beginwork ();
         WritePreise ();
         commitwork ();
         ldruck = ldrfr = lfrei = lbar = lsofort = drwahl = 0;
         SetMouseLockProc (MouseInButton);
         SetBorder (WS_VISIBLE | WS_DLGFRAME | WS_POPUP);
//		 ldruckdefault(); //020609 geht so nicht 
         if (EnterButtonWindow (eWindow, &cbox, y, x, cy, cx) == -1)
         {
                        KomplettAktiv = 0;
                        set_fkt (endlist, 5);
                        CloseControls (&testub);
                        display_form (Getlbox (), &testub, 0, 0);
						syskey = 0;
                        return 0;
         }

	     syskey = 0;
         CloseControls (&testub);
         display_form (Getlbox (), &testub, 0, 0);
         KomplettAktiv = 0;
         SetMouseLockProc (NULL);
		 InvalidateRect (eWindow, NULL, TRUE);
		 UpdateWindow (eWindow);
         BearbKomplett ();
         set_fkt (endlist, 5);
         return 0;
}


/*   Gebinde eingeben          */

mfont GebUbFont = {"Courier New", 190, 0, 1, RGB (255, 255, 255),
                                             GebColor,
                                             0,
                                             NULL};
mfont GebFont   = {"Courier New", 190, 0, 1, RGB (255, 255, 255),
                                             GebColor,
                                             0,
                                             NULL};

static char gebinde [4] = {"N"};
static char palette [4] = {"N"};
static char geb_einh [4] = {"1"};
static char pal_einh [4] = {"1"};
static char geb_me [9];
static char pal_me [9];
static char gtara [9];
static char gtara_einh [3] = {"2"};

static int gebcx = 620;
static int gebcy = 400;

static int break_geb = 0;

static int buttony = gebcy - bsz0 - 20;
static int buttonpos = 5;

static int current_geb = 0;
static int gebend = 6;

static int gbanz = 0;

static int testgebinde ();
static int testpalette ();
static int testgeb_einh ();
static int testpal_einh ();

static int gebfunc1 (void);
static int gebfunc4 (void);

field _gebub[] = {
           "Etikett",  8, 0, 1,10, 0, "", DISPLAYONLY, 0, 0, 0,
           "Einheit",  8, 0, 1,20, 0, "", DISPLAYONLY, 0, 0, 0,
           "Menge  ",  0, 0, 1,30, 0, "", DISPLAYONLY, 0, 0, 0,
           "Gebinde",  10, 0, 3,  1, 0, "",      DISPLAYONLY, 0, 0, 0,
           "Palette",  10, 0, 5,  1, 0, "",      DISPLAYONLY, 0, 0, 0,
           gtara_einh,  3, 0, 7, 22, 0, "%2d",   READONLY, 0, 0, 0,
           "Einzel-Tara", 11, 0, 7,  1, 0, "",   DISPLAYONLY, 0, 0, 0,
};

form gebub = {7, 0, 0, _gebub, 0, 0, 0, 0, &GebUbFont};


field _gebform[] = {

         gebinde,     2, 0, 3, 13, 0, "",      NORMAL, 0, testgebinde, 0,
         geb_einh,    3, 0, 3, 22, 0, "%2d",   NORMAL, 0, testgeb_einh, 0,
         geb_me,      8, 0, 3, 30, 0, "%3.3f", NORMAL, 0, 0, 0,

         palette,     2, 0, 5, 13, 0, "",      NORMAL, 0, testpalette, 0,
         pal_einh,    3, 0, 5, 22, 0, "%2d",   NORMAL, 0, testpal_einh, 0,
         pal_me,      8, 0, 5, 30, 0, "%3.3f", NORMAL, 0, 0, 0,
         gtara,       8, 0, 7, 30, 0, "%3.3f", NORMAL, 0, 0, 0,
};

form gebform = {7, 0, 0, _gebform, 0, 0, 0, 0, &GebFont};

field _GebButton[] = {
(char *) &Back,              bss0, bsz0, buttonpos, 4+5*bss, 0, "", COLBUTTON, 0,
 *                                                              func5, 0,
(char *) &BuOK,              bss0, bsz0, buttonpos, 4+4*bss, 0, "", COLBUTTON, 0,
 *                                                              func6, 0,
(char *) &PfeilR,            bss0, bsz0, buttonpos, 4+3*bss, 0, "", COLBUTTON, 0,
                                                          gebfunc4, 0,
(char *) &PfeilU,            bss0, bsz0, buttonpos, 4+2*bss, 0, "", COLBUTTON, 0,
                                                          func3, 0,
(char *) &PfeilO,            bss0, bsz0, buttonpos, 4+1*bss, 0, "", COLBUTTON, 0,
                                                          func2, 0,
(char *) &PfeilL,            bss0, bsz0, buttonpos, 4,     0, "", COLBUTTON, 0,
                                                          gebfunc1, 0};

form GebButton = {6, 0, 0, _GebButton, 0, 0, 0, 0, &buttonfont};

int gebfunc1 (void)
/**
Button fuer Pfeil links bearbeiten.
**/
{

         SetFocus (gebform.mask[current_geb].feldid);
         SendMessage (gebform.mask[current_geb].feldid, WM_KEYDOWN,  (WPARAM) VK_LEFT, 0l);
         return 1;
}

int gebfunc4 (void)
/**
Button fuer Pfeil links bearbeiten.
**/
{

         SetFocus (gebform.mask[current_geb].feldid);
         SendMessage (gebform.mask[current_geb].feldid, WM_KEYDOWN,  (WPARAM) VK_RIGHT, 0l);
         return 1;
}

void PrintGLines ()
/**
Icons am Bildschirm anzeigen.
**/
{
//         RECT rect, frect;
         PAINTSTRUCT ps;
         HDC hdc;
         HPEN hPen;
         int x, y;

         hdc = BeginPaint (GebWindow, &ps);

/*
         GetFrmRectEx (&gebform, &GebFont, &frect);

         hPen = CreatePen (PS_SOLID, 2, BLACKCOL);
         SelectObject (hdc, hPen);
         x = gebcx - 4;
         y = frect.top - 30;
         MoveToEx (hdc, 2, y, NULL);
         LineTo (hdc, x, y);
         DeleteObject (hPen);

         hPen = CreatePen (PS_SOLID, 2, GRAYCOL);
         SelectObject (hdc, hPen);
         MoveToEx (hdc, 2, y - 1, NULL);
         LineTo (hdc, x, y - 2);
         DeleteObject (hPen);
*/

         y = buttony - 10;
         x = gebcx - 4;

         hPen = CreatePen (PS_SOLID, 2, BLACKCOL);
          SelectObject (hdc, hPen);
         MoveToEx (hdc, 2, y, NULL);
         LineTo (hdc, x, y);
         DeleteObject (hPen);

         hPen = CreatePen (PS_SOLID, 2, GRAYCOL);
         SelectObject (hdc, hPen);
         MoveToEx (hdc, 2, y - 1, NULL);
         LineTo (hdc, x, y - 2);
         DeleteObject (hPen);

         EndPaint (WiegWindow, &ps);
}

void SetGebFocus (void)
/**
Focus fuer Wiegmaske setzen.
**/
{

          SetFocus (gebform.mask[current_geb].feldid);
          SendMessage (gebform.mask[current_geb].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
}


LONG FAR PASCAL GebProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{

        switch(msg)
        {
              case WM_PAINT :
                    if (hWnd == GebWindow)
                    {
                                 display_form (GebWindow, &gebub, 0, 0);
                                 display_form (GebWindow, &gebform, 0, 0);
                        //        PrintGLines ();
                                 SetGebFocus ();
                    }
                    else if (hWnd == GebWindButton)
                    {
                                 display_form (GebWindButton, &GebButton, 0, 0);
                    }
                    break;
              case WM_KEYDOWN :
                         switch (wParam)
                         {
                              case VK_F5 :
                                      func5 ();
                                      return 0;

                         }
                         break;
              case WM_COMMAND :
                    if (LOWORD (wParam) == KEY5)
                    {
                            syskey = KEY5;
                            SendKey (VK_F5);
                            break;
                    }
                    break;
              case WM_DESTROY :
                    if (hWnd == hMainWindow)
                    {
	                         SetForeground ();
	                         closedbase ();
                             ExitProcess (0);
                             return 0;
                    }
                    return 0;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

static int testgeb_einh (void)
/**
Felde Kundenbestell-Einheit testen.
**/
{
/*
	       if (ptab_class.lese_ptab ("me_einh_fill",
                   geb_einh)
			!= 0)
		{
					strcpy (geb_einh, "0");
		}
*/
            display_form (GebWindow, &gebform, 0, 0);
		return 0;
}

static int testpal_einh (void)
/**
Felde Kundenbestell-Einheit testen.
**/
{
/*
	       if (ptab_class.lese_ptab ("me_einh_fill",
                   pal_einh)
			!= 0)
		{
					strcpy (pal_einh, "0");
		}
*/
            display_form (GebWindow, &gebform, 0, 0);
		return 0;
}


int testgebinde ()
/**
Eingabe fuer Gebinde testen.
**/
{
         if (gebinde[0] == 'J' || gebinde[0] == 'j')
         {
                            gebinde[0] = 'J';
                            display_form (GebWindow, &gebform, 0, 0);
                            return 0;
         }

         if (atoi (gebinde))
         {
                            gebinde[0] = 'J';
                            display_form (GebWindow, &gebform, 0, 0);
                            return 0;
         }
         gebinde[0] = 'N';
         display_form (GebWindow, &gebform, 0, 0);
         return 0;
}

int testpalette ()
/**
Eingabe fuer Gebinde testen.
**/
{
         if (palette[0] == 'J' || palette[0] == 'j')
         {
                            palette[0] = 'J';
                            return 0;
         }

         if (atoi (palette))
         {
                            palette[0] = 'J';
                            return 0;
         }
         palette[0] = 'N';
         return 0;
}

struct PT ptabmh, ptabmharr [30];
static int gebanz = 0;

static field _fptabform [] =
{
        ptabmh.ptbez,     12, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        ptabmh.ptwer1,     9, 1, 0,13, 0, "%2d", DISPLAYONLY, 0, 0, 0
};

static form fptabform = {2, 0, 0, _fptabform, 0, 0, 0, 0, &PtFont};

static field _fptabub [] =
{
        "Bezeichnung",     12, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        "Wert",             9, 1, 0,13, 0, "", DISPLAYONLY, 0, 0, 0
};

static form fptabub = {2, 0, 0, _fptabub, 0, 0, 0, 0, &PtFont};

static field _fptabvl [] =
{      "1",                  1,  0, 0, 12, 0, "", NORMAL, 0, 0, 0,
};

static form fptabvl = {1, 0, 0, _fptabvl, 0, 0, 0, 0, &PtFont};





int ShowGebMeEinh (char *me_einh)
/**
Ausahlfenster fuer Festtara anzeigen.
**/
{
        if (gebanz == 0)
        {
               dsqlstatus = ptab_class.lese_ptab_all ("me_einh_fill");
               while (dsqlstatus == 0)
               {
                   strcpy (ptabmharr[gebanz].ptbez,  ptabn.ptbezk);
                   strcpy (ptabmharr[gebanz].ptwer1, ptabn.ptwert);
                   gebanz ++;
                   if (gebanz == 30) break;
                   dsqlstatus = ptab_class.lese_ptab_all ();
               }
        }
        syskey = 0;
        ptidx = ShowPtBox (GebWindow, (char *) ptabmharr, gebanz,
                     (char *) &ptabmh,(int) sizeof (struct PT),
                     &fptabform, &fptabvl, &fptabub);
        if (syskey != KEY5 && syskey != KEYESC)
        {
                     strcpy (me_einh, ptabmharr [ptidx].ptwer1);
        }
        return ptidx;
}

int testgebfields (void)
/**
Eingabefelder testen.
**/
{
        if (current_geb == 0)
        {
                   testgebinde ();
        }
        else if (current_geb == 3)
        {
                   testpalette ();
        }
        else if (current_geb == 1)
        {
                   testgeb_einh ();
        }
        else if (current_geb == 4)
        {
                   testpal_einh ();
        }
        return (0);
}

void gebaktion (void)
/**
Aktion auf aktivem Feld.
**/
{
        switch (current_geb)
        {
                  case 0 :
                          if (gebinde[0] == 'J')
                          {
                                       gebinde[0] = 'N';
                          }
                          else
                          {
                                       gebinde[0] = 'J';
                          }
                          testgebinde ();
                          display_form (GebWindow, &gebform, 0, 0);
                          break;
                  case 1 :
                          ShowGebMeEinh (geb_einh);
                          display_form (GebWindow, &gebform, 0, 0);
                          break;
                  case 2 :
                          EnterCalcBox (GebWindow,"  Gebinde-Menge  ",
                                       geb_me, gebform.mask[2].length,
                                       gebform.mask[2].picture);
                          display_form (GebWindow, &gebform, 0, 0);
                          break;

                  case 3 :
                          if (palette[0] == 'J')
                          {
                                       palette[0] = 'N';
                          }
                          else
                          {
                                       palette[0] = 'J';
                          }
                          testpalette ();
                          display_form (GebWindow, &gebform, 0, 0);
                          break;
                  case 4 :
                          ShowGebMeEinh (pal_einh);
                          display_form (GebWindow, &gebform, 0, 0);
                          break;
                  case 5 :
                          EnterCalcBox (GebWindow,"  Paletten-Menge  ",
                                       pal_me, gebform.mask[5].length,
                                       gebform.mask[5].picture);
                          display_form (GebWindow, &gebform, 0, 0);
                          break;
                  case 6 :
                          EnterCalcBox (GebWindow,"       Tara       ",
                                       gtara, gebform.mask[6].length,
                                       gebform.mask[6].picture);
                          display_form (GebWindow, &gebform, 0, 0);
                          break;
             }
}

int IsGebMessage (MSG *msg)
/**
HotKey Testen.
**/
{
         POINT mousepos;

         switch (msg->message)
         {
              case WM_KEYDOWN :
              {
                     switch (msg->wParam)
                     {
                            case VK_ESCAPE :
                            case VK_F5 :
                                    break_geb = 1;
                                    return TRUE;
                            case VK_RETURN :
                                    gebaktion ();
                                    return TRUE;
                            case VK_DOWN :
                                    testgebfields ();
                                    current_geb ++;
                                    if (current_geb > gebend)
                                                current_geb = 0;
                                    SetGebFocus ();
                                    return TRUE;
                            case VK_UP :
                                   testgebfields ();
                                   current_geb --;
                                   if (current_geb < 0)
                                                current_geb = gebend;
                                    SetGebFocus ();
                                    return TRUE;
                            case VK_TAB :
                                    testgebfields ();
                                    if (GetKeyState (VK_SHIFT) < 0)
                                    {
                                             current_geb --;
                                             if (current_geb < 0)
                                                  current_geb = gebend;
                                     }
                                     else
                                     {
                                             current_geb ++;
                                             if (current_geb > gebend)
                                                  current_geb = 0;
                                     }
                                     SetGebFocus ();
                                     return TRUE;

                             case VK_F9 :
                                     syskey = KEY9;
                                     if (current_geb == 1)
                                     {
                                               ShowGebMeEinh (geb_einh);
                                               display_form (GebWindow, &gebform, 0, 0);
                                     }
                                     else if (current_geb == 4)
                                     {
                                               ShowGebMeEinh (pal_einh);
                                               display_form (GebWindow, &gebform, 0, 0);
                                     }
                                     return TRUE;

                            default :
                                    return FALSE;
                     }
                     break;
              }
              case WM_LBUTTONDOWN :
              {
                    GetCursorPos (&mousepos);

                    if (MouseinWindow (gebform.mask[0].feldid, &mousepos))
                    {
                          current_geb = 0;
/*
                          EnterNumBox (GebWindow,"  Gebinde-Etikett  ",
                                       gebinde, gebform.mask[0].length,
                                       "%1d");
*/
                          if (gebinde[0] == 'J')
                          {
                                       gebinde[0] = 'N';
                          }
                          else
                          {
                                       gebinde[0] = 'J';
                          }
                          testgebinde ();
                          display_form (GebWindow, &gebform, 0, 0);
//                          PostMessage (GebWindow, WM_KEYDOWN, VK_RETURN, 0l);
                          return TRUE;
                    }
                    else if (MouseinWindow (gebform.mask[1].feldid, &mousepos))
                    {
/*
                          EnterNumBox (GebWindow,"  Gebinde-Einheit  ",
                                       geb_einh, gebform.mask[1].length,
                                       gebform.mask[1].picture);
*/
                          current_geb = 1;
                          ShowGebMeEinh (geb_einh);
                          display_form (GebWindow, &gebform, 0, 0);
//                          PostMessage (GebWindow, WM_KEYDOWN, VK_RETURN, 0l);
                          return TRUE;
                    }
                    else if (MouseinWindow (gebform.mask[2].feldid, &mousepos))
                    {
                          current_geb = 2;
                          EnterCalcBox (GebWindow,"  Gebinde-Menge  ",
                                       geb_me, gebform.mask[2].length,
                                       gebform.mask[2].picture);
                          display_form (GebWindow, &gebform, 0, 0);
//                          PostMessage (GebWindow, WM_KEYDOWN, VK_RETURN, 0l);
                          return TRUE;
                    }

                    else if (MouseinWindow (gebform.mask[3].feldid, &mousepos))
                    {
                          current_geb = 3;
/*
                          EnterNumBox (GebWindow,"  Paletten-Etikett  ",
                                       palette, gebform.mask[3].length,
                                       "%1d");
*/
                          if (palette[0] == 'J')
                          {
                                       palette[0] = 'N';
                          }
                          else
                          {
                                       palette[0] = 'J';
                          }
                          testpalette ();
                          display_form (GebWindow, &gebform, 0, 0);
//                          PostMessage (GebWindow, WM_KEYDOWN, VK_RETURN, 0l);
                          return TRUE;
                    }
                    else if (MouseinWindow (gebform.mask[4].feldid, &mousepos))
                    {
/*
                          EnterNumBox (GebWindow,"  Paletten-Einheit  ",
                                       pal_einh, gebform.mask[4].length,
                                       gebform.mask[4].picture);
*/
                          current_geb = 4;
                          ShowGebMeEinh (pal_einh);
                          display_form (GebWindow, &gebform, 0, 0);
                          return TRUE;
                    }
                    else if (MouseinWindow (gebform.mask[5].feldid, &mousepos))
                    {
                          current_geb = 5;
                          EnterCalcBox (GebWindow,"  Paletten-Menge  ",
                                       pal_me, gebform.mask[5].length,
                                       gebform.mask[5].picture);
                          display_form (GebWindow, &gebform, 0, 0);
                          return TRUE;
                    }
                    else if (MouseinWindow (gebform.mask[6].feldid, &mousepos))
                    {
                          current_geb = 6;
                          EnterCalcBox (GebWindow, "       Tara       ",
                                       gtara, gebform.mask[6].length,
                                       gebform.mask[6].picture);
                          display_form (GebWindow, &gebform, 0, 0);
                          return TRUE;
                    }
              }
              case WM_USER :
                    if (LOWORD (msg->wParam) == BUTARA)
                    {
                          dotara ();
                          return TRUE;
                    }
         }
         return FALSE;
}


int dogebinde ()
/**
Parameter fuer Gebinde - und Palettenetikett eingeben.
**/
{
        MSG msg;
        int  x, y;
        int cx, cy;

        cx = gebcx;
        x = (screenwidth - cx) / 2;
        cy = gebcy;
        y = (screenheight - cy) / 2;

        GebWindow  = CreateWindowEx (
                              0,
                              "GebWind",
                              "",
                              WS_POPUP | WS_DLGFRAME | WS_VISIBLE,
                              x, y,
                              cx, cy,
                              WiegWindow,
                              NULL,
                              hMainInst,
                              NULL);
        if (GebWindow == NULL) return (0);

        x = 5;
        cx -= 15;
        y = buttony;
        cy = bsz0 + 10;

        GebWindButton  = CreateWindowEx (
                              WS_EX_CLIENTEDGE,
                              "GebWindB",
                              "",
                              WS_CHILD | WS_VISIBLE,
                              x, y,
                              cx, cy,
                              GebWindow,
                              NULL,
                              hMainInst,
                              NULL);

        EnableWindow (WiegWindow, FALSE);
        create_enter_form (GebWindow, &gebform, 0, 0);
        current_geb = 0;
        SetGebFocus ();
        break_geb = 0;
        while (GetMessage (&msg, NULL, 0, 0))
        {
              if (IsGebMessage (&msg));
              else
              {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
              }
              if (break_geb) break;
        }
        CloseControls (&gebub);
        CloseControls (&gebform);
        CloseControls (&GebButton);
        DestroyWindow (GebWindButton);
        DestroyWindow (GebWindow);
        GebWindow = NULL;
        SetActiveWindow (WiegWindow);
        EnableWindow (WiegWindow, TRUE);
        update_kun_geb ();
        return (0);
}

void ToVorgabe (char *veinh, short einh)
/**
Vorgabe-Einheit als Klartext.
**/
{
        switch (einh)
        {
           case 1 :
                strcpy (veinh, "St�ck");
                break;
           case 2:
                strcpy (veinh, "kg");
                break;
           case 4:
                strcpy (veinh, "Preis");
                break;
           case 5:
                strcpy (veinh, "Karton");
                break;
           case 6:
                strcpy (veinh, "Palette");
                break;
           default:
                strcpy (veinh, "kg");
                break;
        }
}


void gebindelsp (void)
/**
Gebindevorgaben in lsp testen.
Wenn brauchbare Gebindewerte in der lsp vorhanden sind,
Gebindewerte aus a_kun_gx ueberschreiben.
**/
{
/*
        short   me_einh_kun1;
        double  auf_me1;
        double  inh1;
        short   me_einh_kun2;
        double  auf_me2;
        double  inh2;
        short   me_einh_kun3;
        double  auf_me3;
        double  inh3;

        me_einh_kun1 = atoi (aufps.me_einh_kun1);
        auf_me1      = ratod (aufps.auf_me1);
        inh1         = ratod (aufps.inh1);

        me_einh_kun2 = atoi (aufps.me_einh_kun2);
        auf_me2      = ratod (aufps.auf_me2);
        inh2         = ratod (aufps.inh2);

        me_einh_kun3 = atoi (aufps.me_einh_kun3);
        auf_me3      = ratod (aufps.auf_me3);
        inh3         = ratod (aufps.inh3);

        if (me_einh_kun1 <= 0 &&
            me_einh_kun2 <= 0 &&
            me_einh_kun3 <= 0)
        {
            return;
        }

        if (me_einh_kun1 == 2)
        {
              a_kun_geb.geb_fill = 2;
              sprintf (geb_einh, "%hd", a_kun_geb.geb_fill);
              ToVorgabe (veinh1, a_kun_geb.geb_fill);
        }
        else
        {
              a_kun_geb.geb_fill = 1;
              sprintf (geb_einh, "%hd", a_kun_geb.geb_fill);
              ToVorgabe (veinh1, a_kun_geb.geb_fill);
        }

        if (me_einh_kun2 == 5)
        {
              a_kun_geb.pal_fill = 5;
              sprintf (pal_einh, "%hd", a_kun_geb.pal_fill);
              ToVorgabe (veinh2, a_kun_geb.pal_fill);
        }
        else if (me_einh_kun2 == 2)
        {
              a_kun_geb.pal_fill = 2;
              sprintf (pal_einh, "%hd", a_kun_geb.pal_fill);
              ToVorgabe (veinh2, a_kun_geb.pal_fill);
        }
        else
        {
              a_kun_geb.pal_fill = 1;
              sprintf (pal_einh, "%hd", a_kun_geb.pal_fill);
              ToVorgabe (veinh2, a_kun_geb.pal_fill);
        }

        if (auf_me2 > (double) 0.0 && me_einh_kun2)
        {
              if (inh3)
              {
                  auf_me2 = inh3;
              }
              sprintf (pal_me,   "%.0lf",  auf_me2);
              strcpy (vorgabe2, pal_me);
        }
		else if (inh3)
		{
              sprintf (pal_me,   "%.3lf",  inh3);
              strcpy (vorgabe2, pal_me);
		}

        if (auf_me1 > (double) 0.0 && me_einh_kun1)
        {
              if (inh2)
              {
                  auf_me1 = inh2;
              }
              sprintf (geb_me,   "%.3lf", auf_me1);
              strcpy (vorgabe1, geb_me);
        }
		else if (inh2)
		{
              sprintf (geb_me,   "%.3lf",  inh2);
              strcpy (vorgabe1, geb_me);
		}
*/
}


void fetch_std_geb (void)
/**
Standard-Gebindeeinstellungen aus a_kun lesen und in kun_geb uebertragen.
Der Eintrag wird nur temporaer benutzt        .
**/
{
/*
        short sqm;
        extern short sql_mode;


        strcpy (a_kun_geb.geb_eti, "N");
        strcpy (a_kun_geb.pal_eti, "N");
        a_kun_geb.geb_fill = 0;
        a_kun_geb.pal_fill = 0;
        a_kun_geb.geb_anz = 0;
        a_kun_geb.pal_anz = 0;

        a_kun_geb.mdn = 1;
        a_kun_geb.fil = 0;
        a_kun_geb.kun = lsk.kun;
        a_kun_geb.a   = ratod (aufps.a);
        a_kun_geb.tara   = 0;
        a_kun_geb.mhd   = 0;
        strcpy (gebinde, a_kun_geb.geb_eti);

        sprintf (geb_einh, "%hd", a_kun_geb.geb_fill);
        ToVorgabe (veinh1, a_kun_geb.geb_fill);

        sprintf (geb_me,   "%.3lf",
            (double) ((double) a_kun_geb.geb_anz / 1000));
        strcpy (vorgabe1, geb_me);

        strcpy (palette, a_kun_geb.pal_eti);

        sprintf (pal_einh, "%hd", a_kun_geb.pal_fill);
        ToVorgabe (veinh2, a_kun_geb.pal_fill);

        sprintf (pal_me,   "%.3lf",
            (double) ((double) a_kun_geb.pal_anz / 1000));
        strcpy (vorgabe2, pal_me);

        gebindelsp ();
        a_kun_geb.geb_anz = (long) (double) (ratod (geb_me) * 1000);
        a_kun_geb.pal_anz = (long) (double) (ratod (pal_me) * 1000);

        sprintf (gtara,     "%.3lf", a_kun_geb.tara);

        if (tara_kz == 1)
        {
                  sprintf (gtara,     "%.3lf", a_hndw.tara);
                  a_kun_geb.tara =  a_hndw.tara;
        }
        if (mhd_kz == 1)
        {
              if (_a_bas.hbk_kz[0] == 'T' ||
                  _a_bas.hbk_kz[0] <= ' ')
              {
                  a_kun_geb.mhd =  _a_bas.hbk_ztr;
              }
        }
        sprintf (aufps.tara,     "%.3lf", a_kun_geb.tara);
        sqm = sql_mode;
        sql_mode = 1;
        geb_class.update_kun_geb ();
        sql_mode = sqm;
*/
}

void updategeblsp (void)
/**
Gebinde in lsp aktialisieren.
**/
{
/*
 	    sprintf (aufps.inh2, "%.3lf", (double) ((double)
				        a_kun_geb.geb_anz / 1000));
	    sprintf (aufps.inh3, "%.3lf", (double) ((double)
				        a_kun_geb.pal_anz / 1000));

		sprintf (aufps.me_einh_kun1, "%d", a_kun_geb.geb_fill);
		sprintf (aufps.me_einh_kun2, "%d", a_kun_geb.pal_fill);

        memcpy (&aufparr [aufpidx], &aufps, sizeof (struct AUFP_S));
*/
}


void update_kun_geb (void)
/**
Tabelle kun_geb aktualisieren.
**/
{
/*
        short sqm;
        extern short sql_mode;

        sqm = sql_mode;
        strcpy (a_kun_geb.geb_eti, gebinde);
        a_kun_geb.geb_fill = atoi (geb_einh);
        ToVorgabe (veinh1, a_kun_geb.geb_fill);
        strcpy (vorgabe1, geb_me);
        a_kun_geb.geb_anz =  (long) (double) (ratod (geb_me) * 1000);

        strcpy (a_kun_geb.pal_eti, palette);
        a_kun_geb.pal_fill = atoi (pal_einh);
        ToVorgabe (veinh2, a_kun_geb.pal_fill);
        strcpy (vorgabe2, pal_me);
        a_kun_geb.pal_anz =  (long) (double) ratod (pal_me) * 1000;
        a_kun_geb.tara    =  ratod (gtara);
        sprintf (aufps.tara,     "%.3lf", a_kun_geb.tara);
        sql_mode = 1;
        geb_class.update_kun_geb ();
        sql_mode = sqm;
        updategeblsp ();
*/
}


/**

Neuen Lieferschein eingeben.

**/

mfont LiefKopfFontT = {"Courier New", 200, 0, 1, BLACKCOL,
                                                RGB (0, 255, 255),
                                                0,
                                                NULL};

mfont LiefKopfFontE = {"Courier New", 200, 0, 1,
                                                RGB (0, 0, 0),
                                                RGB (255, 255, 255),
                                                0,
                                                NULL};


mfont LiefKopfFontTD = {"Courier New", 150, 0, 1, BLACKCOL,
                                                RGB (0, 255, 255),
                                                0,
                                                NULL};

mfont LiefKopfFontED = {"Courier New", 150, 0, 1,
                                                RGB (0, 0, 0),
                                                RGB (255, 255, 255),
                                                0,
                                                NULL};


static char KundeFil0 [14] = {"Kunden-Nr   :"};
static char KundeFil1 [14] = {"Filial-Nr   :"};
static char kunfil[3];
static long gen_lief_nr;

static field _LiefKopfT[] = {
"Partie     :",     10, 0, 1, 2, 0, "", DISPLAYONLY, 0, 0, 0,
"Datum      :",     16, 0, 2, 2, 0, "", DISPLAYONLY, 0, 0, 0,
bestellhinweis,     60, 0, 1, 36, 0, "", DISPLAYONLY, 0, 0, 0,
};

form LiefKopfT = {3, 0, 0, _LiefKopfT, 0, 0, 0, 0, &LiefKopfFontT};



static field _LiefKopfTD[] = {
"Lieferant    : ", 13, 0, 7, 2, 0, "", DISPLAYONLY, 0, 0, 0,
lief_krz1,        17, 0, 5,18, 0, "", DISPLAYONLY, 0, 0, 0,
//bestellhinweis,   60, 0, 9, 2, 0, "", DISPLAYONLY, 0, 0, 0,
//"Status      : ", 13, 0,12, 2, 0, "", DISPLAYONLY, 0, 0, 0,
//ls_stat_txt,      16, 0,12,28, 0, "", DISPLAYONLY, 0, 0, 0
};

form LiefKopfTD = {2, 0, 0, _LiefKopfTD, 0, 0, 0, 0, &LiefKopfFontTD};

static int ls_stat_pos = 4;
static int setlspos = 0;
static int SetKunFil (void);
static int ReadLief (void);



static field _LiefKopfE[] = {
  lief_nr,               17, 0, 1,18, 0, "", EDIT,        0, ReadLS, 0,
  lieferdatum,               17, 0, 2,18, 0, "dd.mm.yyyy", EDIT,        0, GenPartie, 0
};

form LiefKopfE = {2, 0, 0, _LiefKopfE, 0, 0, 0, 0, &LiefKopfFontE};


//FISCH-5
static field _BuchKopfT[] = {
"Kunde  :",     20, 0, 1, 2, 0, "", DISPLAYONLY, 0, 0, 0,
"Datum             :",     14, 0, 2, 2, 0, "", DISPLAYONLY, 0, 0, 0,
bestellhinweis,     60, 0, 1, 36, 0, "", DISPLAYONLY, 0, 0, 0,
};

form BuchKopfT = {3, 0, 0, _BuchKopfT, 0, 0, 0, 0, &LiefKopfFontT};


static field _BuchKopfE[] = {
  lief_nr,               20, 0, 1,26, 0, "", EDIT,        0, ReadKun, 0,
  lieferdatum,           14, 0, 2,26, 0, "dd.mm.yyyy", EDIT,        0, 0, 0
};

form BuchKopfE = {2, 0, 0, _BuchKopfE, 0, 0, 0, 0, &LiefKopfFontE};


//FISCH-5
static field _ChrgKopfT[] = {
"Scannen           :",     20, 0, 1, 2, 0, "", DISPLAYONLY, 0, 0, 0,
"Produktionscharge :",     20, 0, 5, 2, 0, "", DISPLAYONLY, 0, 0, 0,
"Datum             :",     20, 0, 7, 2, 0, "", DISPLAYONLY, 0, 0, 0,
"Artikel           :",     20, 0, 8, 2, 0, "", DISPLAYONLY, 0, 0, 0,
 artikelbez,               20, 0, 8,37, 0, "", DISPLAYONLY,        0, 0, 0,
"Fanggebiet        :",     20, 0, 9, 2, 0, "", DISPLAYONLY, 0, 0, 0,
};

form ChrgKopfT = {6, 0, 0, _ChrgKopfT, 0, 0, 0, 0, &LiefKopfFontT};


static field _ChrgKopfE[] = {
  scanprodstart,         50, 0, 1,22, 0, "", EDIT,        0, ScanProdStart, 0,
  lief_nr,               14, 0, 5,22, 0, "", EDIT,        0, 0, 0,
  lieferdatum,           14, 0, 7,22, 0, "dd.mm.yyyy", EDIT,        0, GenPartie, 0,
  artikel,               14, 0, 8,22, 0, "", EDIT,        0, 0, 0,
  fanggebiet,            20, 0, 9,22, 0, "", EDIT,        0, AuswahlFanggebiet, 0
};

form ChrgKopfE = {5, 0, 0, _ChrgKopfE, 0, 0, 0, 0, &LiefKopfFontE};

/**
static field _LiefKopfE[] = {
  lieferdatum,               17, 0, 1,18, 0, "dd.mm.yyyy", EDIT,        0, func6, 0,
};

form LiefKopfE = {1, 0, 0, _LiefKopfE, 0, 0, 0, 0, &LiefKopfFontE};
**/

static field _LiefKopfED[] = {
  lieferdatum,         11, 0, 7,18, 0, "dd.mm.yyyy",
                                              EDIT,     0, 0, 0,
  ls_stat,              2, 0,12,18, 0, "",
                                              READONLY, 0, 0, 0
};

form LiefKopfED = {2, 0, 0, _LiefKopfED, 0, 0, 0, 0, &LiefKopfFontED};


static field _LiefKopfBestT[] = {
"Bestellung  : ", 13, 0,17, 2, 0, "", DISPLAYONLY, 0, 0, 0
};

form LiefKopfBestT = {1, 0, 0, _LiefKopfBestT, 0, 0, 0, 0, &LiefKopfFontTD};

static field _LiefKopfBest[] = {
  bestell_nr,              11, 0,17,18, 0, "",
                                              READONLY, 0, 0, 0
};

form LiefKopfBest = {1, 0, 0, _LiefKopfBest, 0, 0, 0, 0, &LiefKopfFontED};
static int lspos = 0;
static int lsend = 4;

void FillLSForm (void)
/**
Datenbankfelder in Maske uebertragn.
**/
{
/*
             dlong_to_asc (zerl_k.we_dat, lieferdatum);
             dlong_to_asc (zerl_k.blg_eing_dat, beldatum);
             strcpy (lieferzeit, lsk.lieferzeit);
             sprintf (ls_stat, "%hd", zerl_k.partie_status);
             strcpy (kun_krz1, _adr.adr_krz);
			 sprintf (ls_stat, "%hd", zerl_k.partie_status);
*/
/*
             ptab_class.lese_ptab ("we_stat", ls_stat);
             strcpy (ls_stat_txt, ptabn.ptbezk);
*/
}

void FromLSForm (void)
/**
Datenbankfelder in Maske uebertragn.
**/
{
             short mdn;
             short fil;

             mdn = Mandant;
             fil = Filiale;

			 zerl_k.mdn = mdn;
             zerl_k.zer_dat       = dasc_to_long (lieferdatum);
             zerl_k.partie       = ratod (lief_nr);
}

static int readstatus = 0;
static int freestat = 0;

void FreeLS (void)
/**
Lieferschein-Nummer freigeben.
**/
{
     short mdn;
     short fil;

	 return;
     mdn = Mandant;
     fil = Filiale;

     if (freestat == 1) return;

     beginwork ();
     dsqlstatus = nveinid (mdn, fil,
                          "ls",  gen_lief_nr);
     commitwork ();
     freestat = 1;
}



int WriteLS (void)
/**
Lieferscheinkopf schreiben.
**/
{
    char datum [12];
    long dat1, dat2;

    if (readstatus > 1) return 0;

	if (strcmp (lief_nr,   "                ") <= 0) return 0;
//	if (strcmp (lieferant, "                ") <= 0) return 0;

    if (TestQs (0) == FALSE)
    {
        return 0;
    }

    sysdate (datum);
    dat1 = dasc_to_long (datum);
    dat2 = dasc_to_long (lieferdatum);


    FromLSForm ();
    if (Lock_Lsk () == FALSE)
    {
        PostQuitMessage (0);
        break_lief_enter = 1;
        rollbackwork ();
        return 0;
    }
    beginwork ();
	if (neuer_we)
	{
//         zerl_k.lfd = AutoNr.GetGJ(1, 0, "we_nr");
//		 UpdateLfd ();
	}
	zerl_k.mdn = Mandant;
	strcpy(zerl_k.zer_part, zer_partk.zer_part);
	strcpy(zerl_k.partie_bz,zer_partk.zer_bz); 
    ZerlkClass.dbupdate ();
    ActivateColButton (BackWindow2, &MainButton, 1, 1, 1);
    set_fkt (menfunc2, 6);
    commitwork ();
	neuer_we = FALSE;
	if (liste_direct)
	{
		menfunc2 ();
	}
    return 0;
}



int  ReadWEBestell (int best_nr)
/**
Laufende Nummer in zerl_k updaten.
**/
{
	  int dsqlstatus;
	  /***********
	  char sqlbuffer [1024];
	  char cbest_nr [17];

	  sprintf (cbest_nr,"%ld",best_nr);

      DbClass.sqlout ((char *) &zerl_k.lief, 0, 17);
      DbClass.sqlout ((char *) &zerl_k.lief_rech_nr, 0, 17);
      DbClass.sqlout ((long *) &zerl_k.lfd, 2, 0);
      DbClass.sqlout ((char *) &zerl_k.blg_typ, 0, 2);

	  sprintf (sqlbuffer, "select lief,lief_rech_nr,lfd,blg_typ from zerl_k "
		                  "where mdn = %hd "
						  "and   fil = %hd "
						  "and koresp_best0 = \"%s\" ",
						  Mandant, Filiale,
						  cbest_nr);
	  dsqlstatus = DbClass.sqlcomm (sqlbuffer);
	  *************/
      return dsqlstatus;
}
int  TestWE (void)
/**
Laufende Nummer in zerl_k updaten.
**/
{
	  int dsqlstatus;


      DbClass.sqlout ((short *) &zerl_k.mdn, 1, 0);
      DbClass.sqlin ((short *) &zerl_k.mdn, 1, 0);
      DbClass.sqlin ((double *) &zerl_k.partie, 3, 0);

	  dsqlstatus = DbClass.sqlcomm ("select mdn from zerl_k "
		                  "where mdn = ? "
						  "and   partie = ? ");
      return dsqlstatus;
}



int ReadLS (void)
/**
Lieferschein nach Lieferschein-Nr und Lieferant lesen.
**/
{
	        int dsqlstatus;
	        char datum [12];
            readstatus = 0;
//            if (atoi(EinAusgang) == PRODUKTIONSSTART) return 0;
//liste_direct = FALSE; //testtest
			if (strcmp (lieferdatum, "                 ") <= 0) return 0;
			if (strcmp (lief_nr, "                 ") <= 0) return 0;
			sysdate (datum);
            freestat = 0;
			NeuePartie = 0;
			zerl_k.partie = ratod(lief_nr);
			zerl_k.mdn = Mandant;
			strcpy (zerl_k.lief, lieferant);
            dsqlstatus = ZerlkClass.dbreadfirst ();
			if (strcmp (lieferant, "                 ") <= 0) 
			{
			}
			else
			{
				if (ReadLief () == 100)
				{
	                ActivateColButton (BackWindow2, &MainButton, 2, -1, 1);
					return 100;
				}
			}
            if (dsqlstatus == 0 && zerl_k.partie_status < 1)  //200709 vorher 3
            {

				   zerl_k.zer_dat = dasc_to_long (datum);
                   beginwork ();
                   if (Lock_Lsk () == FALSE)
                   {
                          break_lief_enter = 1;
                          return -1;
                   }
                   ActivateColButton (BackWindow3, &MainButton2, 2, 1, 1);
                   ActivateColButton (BackWindow2, &MainButton,  1, 1, 1);

                   set_fkt (menfunc2, 6);
                   FillLSForm ();
				   neuer_we = FALSE;
  	               if (liste_direct)
				   {
		                    menfunc2 ();
				   }
            }
            else if (dsqlstatus == 100)
            {
				   if (atoi(EinAusgang) == AUSGANG) //Zerlegeausgang
				   {
					   disp_messG ("Partie nicht vorhanden", 1);
                       break_lief_enter = 1;
					   return -1;
				   }
				   disp_messG ("Neue Produktionscharge", 1);
				   NeuePartie = 1;

                   ActivateColButton (BackWindow2, &MainButton, 2, 1, 1);
				   zerl_k.partie_status = 0;
				   FromLSForm();
                   beginwork ();
			   	   strcpy(zerl_k.zer_part, zer_partk.zer_part);
				   strcpy(zerl_k.partie_bz,zer_partk.zer_bz); 
				   ZerlkClass.dbupdate ();
                   if (Lock_Lsk () == FALSE)
                   {
                          break_lief_enter = 1;
                          return -1;
                   }
                   ActivateColButton (BackWindow3, &MainButton2,  2,  1, 1);
                   ActivateColButton (BackWindow2, &MainButton,   1,  -1, 1);
                   set_fkt (menfunc2, 6);
				   neuer_we = TRUE;
  	               if (liste_direct)
				   {
		                    menfunc2 ();
				   }
            }
			else if (atoi(EinAusgang) == PRODUKTIONSSTART && zerl_k.partie_status > 0)  //FISCH-5
			{
				   disp_messG ("Produktionscharge ist schon komplett", 2);
				   return -1;
			}
			else if (zerl_k.partie_status > 2)
			{
				   disp_messG ("Lieferschein schon abgeschlossen", 2);
				   return -1;
			}
			else if (zerl_k.partie_status > 0)
			{
				   disp_messG ("Lieferschein ist schon gedruckt", 2);
				   return -1;
			}
            return 0;
}



int SetKunFil (void)
{

          display_form (AufKopfWindow, &LiefKopfTD, 0, 0);
          display_form (AufKopfWindow, &LiefKopfED, 0, 0);
          return (0);
}


void FillLskAdr (void)
/**
Adressfelder in Lsk fuellen.
**/
{
}

int ReadLief (void)
{
          short mdn;
          short fil;

          mdn = Mandant;
		  fil = Filiale;

          lief.mdn = mdn;
          lief.fil = fil;
		  strcpy (lief.lief, lieferant);
          kun_krz1[0] = (char) 0;
          dsqlstatus = LiefClass.dbreadfirst ();
		  if (dsqlstatus == 100 && lief.mdn)
		  {
			         lief.mdn = 0;
                     dsqlstatus = LiefClass.dbreadfirst ();
		  }
          if (dsqlstatus == 0)
          {
                      adr_class.lese_adr (lief.adr);
                      strcpy (lief_krz1, _adr.adr_krz);
                      FillLskAdr ();
          }
          if (dsqlstatus)
          {
			      print_messG (2, "Lieferant %s nicht gefunden",
					               lieferant);
				  lspos = 1;
				  setlspos = 1;
                  strcpy (lieferant, " ");
				  return 100;
          }

          current_form = &LiefKopfTD;
          display_field (AufKopfWindow, &LiefKopfTD.mask[0], 0, 0);
          return 0;
}


void DisplayLiefKopf (void)
/**
Masken fuer LiefKopf anzeigen.
**/
{
          display_form (AufKopfWindow, &LiefKopfT, 0, 0);
          display_form (AufKopfWindow, &LiefKopfTD, 0, 0);
          if (LiefKopfE.mask[0].feldid)
          {
              display_form (AufKopfWindow, &LiefKopfE, 0, 0);
              display_form (AufKopfWindow, &LiefKopfED, 0, 0);
          }
}

void CloseLiefKopf (void)
/**
Masken fuer LiefKopf anzeigen.
**/
{
        CloseControls (&LiefKopfT);
        CloseControls (&LiefKopfE);
        CloseControls (&LiefKopfTD);
        CloseControls (&LiefKopfED);
}

void GetEditTextL (void)
/**
Text aus Editierfeld holen.
**/
{
        switch (lspos)
        {
                 case 0 :
                    GetWindowText (LiefKopfE.mask [0].feldid,
                                   LiefKopfE.mask [0].feld,
                                   LiefKopfE.mask[0].length);
                    break;
                 case 1 :
                    GetWindowText (LiefKopfE.mask [1].feldid,
                                   LiefKopfE.mask [1].feld,
                                   LiefKopfE.mask[1].length);
                    break;
                 case 2 :
                    GetWindowText (LiefKopfED.mask [0].feldid,
                                   LiefKopfED.mask [0].feld,
                                   LiefKopfED.mask[0].length);
                    break;
                 case 3 :
                    GetWindowText (LiefKopfED.mask [1].feldid,
                                   LiefKopfED.mask [1].feld,
                                   LiefKopfED.mask[1].length);
                    break;
                 case 4 :
                    GetWindowText (LiefKopfED.mask [2].feldid,
                                   LiefKopfED.mask [2].feld,
                                   LiefKopfED.mask[2].length);
                    break;
        }
        if (lspos == 0 && LiefKopfE.mask[0].after)
        {
                    (*LiefKopfE.mask[0].after) ();
        }
        else if (lspos == 1 && LiefKopfE.mask[1].after)
        {
                    (*LiefKopfE.mask[1].after) ();
        }
        else if (lspos &&  LiefKopfED.mask[lspos - 2].after)
        {
                    (*LiefKopfED.mask[lspos - 2].after) ();
        }
}


void LiefAfter (int pos)
{
        lspos = pos;
        if (lspos == 0 && LiefKopfE.mask[0].after)
        {
                    (*LiefKopfE.mask[0].after) ();
        }
        else if (lspos == 1 && LiefKopfE.mask[1].after)
        {
                    (*LiefKopfE.mask[1].after) ();
        }
        else if (lspos &&  LiefKopfED.mask[lspos - 2].after)
        {
                    (*LiefKopfED.mask[lspos - 2].after) ();
        }
}

void SetLiefFocus (int pos)
/**
Focus auf Eingabefelder AuftragKopfsetzen.
**/
{
	    setlspos = 0;
        switch (pos)
        {
               case 0 :
                      SetFocus (LiefKopfE.mask[0].feldid);
                      SendMessage (LiefKopfE.mask[0].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
                      break;
               case 1 :
  						SetFocus (LiefKopfE.mask[1].feldid);
						SendMessage (LiefKopfE.mask[1].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
                      break;
               default :

				   SetFocus (LiefKopfED.mask[pos - 2].feldid);
                      SendMessage (LiefKopfED.mask[pos - 2].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
                      break;
        }
}

void LiefFocus (void)
{
        SetLiefFocus (lspos);
}


int IsLiefMessage (MSG *msg)
/**
HotKey Testen.
**/
{
         POINT mousepos;

         switch (msg->message)
         {
              case WM_KEYDOWN :
              {
                     switch (msg->wParam)
                     {
                            case VK_F5 :
                                    break_lief_enter = 1;
                                    return TRUE;
                            case VK_F6 :
                                    syskey = KEY6;
                                    testkeys ();
                                    return TRUE;
                            case VK_F12 :
								    if (TestOK () == FALSE) return TRUE;
                                    syskey = KEY12;
                                    WriteLS ();
                                    return TRUE;
                            case VK_DOWN :
                            case VK_RETURN :
                                    GetEditTextL ();
									if (setlspos == 0)
									{
                                           lspos ++;
									}
                                    if (lspos > lsend - 1) lspos = 0;
                                    SetLiefFocus (lspos);
                                    return TRUE;
                            case VK_UP :
                                    GetEditTextL ();
									if (setlspos == 0)
									{
                                            lspos --;
									}
                                    if (lspos < 0) lspos = lsend - 1;
                                    SetLiefFocus (lspos);
                                    return TRUE;
                            case VK_TAB :
                                    GetEditTextL ();
                                    if (GetKeyState (VK_SHIFT) < 0)
                                    {
									        if (setlspos == 0)
											{
                                                    lspos --;
											}
                                            if (lspos < 0) lspos = lsend - 1;
                                            SetLiefFocus (lspos);
                                     }
                                     else
                                     {
        									if (setlspos == 0)
											{
                                                     lspos ++;
											}
                                            if (lspos > lsend - 1) lspos = 0;
                                            SetLiefFocus (lspos);
                                     }
                                      return TRUE;
                            default :
                                    return FALSE;
                     }
                     break;
              }
              case WM_LBUTTONDOWN :
              {
				    setlspos = 0;
                    GetCursorPos (&mousepos);

//Hier: Aktionen nach Klick in LiefMessage   (LiefKopfE  (Datum.Artikel, Fanggebiet )
                    if (MouseinWindow (LiefKopfE.mask[0].feldid, &mousepos))      
                    {
						  if (atoi (EinAusgang) == BUCHENFERTIGLAGER)
						  {
							NuBlock.EnterNumBox (AufKopfWindow, "  Kunden-Nr  ",
										lief_nr, 16, LiefKopfE.mask[0].picture, EntNuProc);
							display_field (AufKopfWindow, &LiefKopfE.mask[0], 0, 0);
						  }
						  else if (atoi (EinAusgang) == PRODUKTIONSSTART) 
						  {
							  ScanProdStart ();
							  return TRUE;
						  }
						  else
						  {
							NuBlock.EnterNumBox (AufKopfWindow, "  Lieferschein-Nr  ",
										lief_nr, 16, LiefKopfE.mask[0].picture, EntNuProc);
							display_field (AufKopfWindow, &LiefKopfE.mask[0], 0, 0);
						  }
                          LiefAfter (0);
                          return TRUE;
                    }
                    else if (MouseinWindow (LiefKopfE.mask[1].feldid, &mousepos))
                    {
						 if (atoi (EinAusgang) == PRODUKTIONSSTART) 
						 {
							 return TRUE;
						 }
						 else
						 {
							NuBlock.EnterNumBox (AufKopfWindow,"  Zerlege-Datum  ",
                                      lieferdatum, 11,
                                            LiefKopfED.mask[0].picture, EntNuProc);
							display_field (AufKopfWindow, &LiefKopfED.mask[0], 0, 0);
						 }
                         LiefAfter (1);
//					     PostMessage (msg->hwnd, WM_KEYDOWN, VK_RETURN, 0l);
                         return TRUE;
                          return TRUE;
                    }

				   if (atoi (EinAusgang) == PRODUKTIONSSTART) 
				   {
						 if (MouseinWindow (LiefKopfE.mask[2].feldid, &mousepos))    // Ckick ins datumsfeld
						{	
								NuBlock.EnterNumBox (AufKopfWindow,"  Produktions-Datum  ",
                                      lieferdatum, 11,
                                            LiefKopfE.mask[2].picture, EntNuProc);
							if (syskey != KEY5)
							{
								display_field (AufKopfWindow, &LiefKopfE.mask[2], 0, 0);
							}
							if (zerl_k.a)
							{
								zerl_k.partie = GenPartie0 ();
 								sprintf(lief_nr,"%.0lf",zerl_k.partie);
								display_field (AufKopfWindow, &LiefKopfE.mask[1], 0, 0);
							}

							return TRUE;
						}

						else if (MouseinWindow (LiefKopfE.mask[3].feldid, &mousepos))    //Ckick ins Artikelfeld
						{
							 NuBlock.SetChoise (doartchoise, 0);
							NuBlock.EnterNumBox (AufKopfWindow,"  Artikel  ",                 
                                      artikel, 11,
                                            LiefKopfE.mask[3].picture, EntNuProc);
							if (syskey != KEY5)
							{
								display_field (AufKopfWindow, &LiefKopfE.mask[3], 0, 0);
								if (lese_a_bas (ratod(artikel)) == 0)
								{
									zerl_k.a = _a_bas.a;
							        strcpy (artikelbez,_a_bas.a_bz1);
									display_field (AufKopfWindow, &LiefKopfT.mask[4], 0, 0);
								}
							}
							//ZUTUN  :   Nach �nderung des datums, muss die Prodcharge neu erstellt werden 
							if (zerl_k.a)
							{
								zerl_k.partie = GenPartie0 ();
 								sprintf(lief_nr,"%.0lf",zerl_k.partie);
								display_field (AufKopfWindow, &LiefKopfE.mask[1], 0, 0);
								display_field (AufKopfWindow, &LiefKopfE.mask[4], 0, 0); 
							}
	                         return TRUE;
						}
			            else if (MouseinWindow (LiefKopfE.mask[4].feldid, &mousepos))    // Ckick ins fanggebiet
						{
							/**
							NuBlock.EnterNumBox (AufKopfWindow,"  Produktions-Datum  ",
                                      lieferdatum, 11,
                                            LiefKopfED.mask[0].picture, EntNuProc);
							display_field (AufKopfWindow, &LiefKopfE.mask[2], 0, 0);
							**/
                         return TRUE;
						}
				   }
                    else if (MouseinWindow (LiefKopfED.mask[0].feldid, &mousepos))
                    {
                         NuBlock.EnterNumBox (AufKopfWindow,"  WE-Datum  ",
                                      lieferdatum, 11,
                                            LiefKopfED.mask[0].picture, EntNuProc);
                         display_field (AufKopfWindow, &LiefKopfED.mask[0], 0, 0);
                         LiefAfter (2);
//					     PostMessage (msg->hwnd, WM_KEYDOWN, VK_RETURN, 0l);
                         return TRUE;
                    }
                    else if (MouseinWindow (LiefKopfED.mask[1].feldid, &mousepos))
                    {
                         NuBlock.EnterNumBox (AufKopfWindow,"  Bel.Eing.Datum  ",
                                      beldatum, 11,
                                            LiefKopfED.mask[1].picture, EntNuProc);
                         display_field (AufKopfWindow, &LiefKopfED.mask[1], 0, 0);
                         LiefAfter (3);
//					     PostMessage (msg->hwnd, WM_KEYDOWN, VK_RETURN, 0l);
                         return TRUE;
                    }
             }
          }

          return FALSE;
}

void KonvLiefKopf (void)
/**
Spaltenposition von LiefKopfED anpassen.
**/
{
        static int KonvOK = 0;
        int z, s;
        int i;

        if (KonvOK) return;

        KonvOK = 1;
        for (i = 0; i < LiefKopfED.fieldanz; i ++)
        {
                    s = LiefKopfED.mask[i].pos [1];
                    KonvTextPos (LiefKopfE.font,
                                 LiefKopfED.font,
                                 &z, &s);
                    LiefKopfED.mask[i].pos [1] = s;
        }

        s = LiefKopfTD.mask[0].pos [1];
        KonvTextPos (LiefKopfE.font,
                    LiefKopfED.font,
                    &z, &s);
        LiefKopfTD.mask[0].pos [1] = s;
/*
        s = LiefKopfTD.mask[1].pos [1];
        KonvTextPos (LiefKopfE.font,
                    LiefKopfED.font,
                    &z, &s);
        LiefKopfTD.mask[1].pos [1] = s;

        s = LiefKopfTD.mask[2].pos [1];
        KonvTextPos (LiefKopfE.font,
                    LiefKopfED.font,
                    &z, &s);
        LiefKopfTD.mask[2].pos [1] = s;

        s = LiefKopfTD.mask[3].pos [1];
        KonvTextPos (LiefKopfE.font,
                    LiefKopfED.font,
                    &z, &s);
        LiefKopfTD.mask[3].pos [1] = s;
*/
}

void LiefInit (field *feld)
/**
Feld Initialisieren.
**/
{
        memset (feld->feld, ' ', feld->length);
        feld->feld[feld->length - 1] = (char) 0;
}

void InitLiefKopf (void)
/**
Auftrag-Kopf initialisieren.
**/
{
        int i;
        LiefKopfE.mask[0].feld[0] = (char) 0;
        LiefKopfE.mask[1].feld[0] = (char) 0;

        for (i = 0; i < LiefKopfED.fieldanz; i ++)
        {
                      LiefInit (&LiefKopfED.mask[i]);
        }
        LiefInit (&LiefKopfTD.mask[0]);
//        LiefInit (&LiefKopfTD.mask[ls_stat_pos]);
}

void GenLief_rech_nr (short di)
{
	char cdi[2];
	sprintf(cdi,"%hd",di);
			sysdate (datum);
			memcpy (lief_nr, &datum[6], 4);
			memcpy (&lief_nr[4], &datum[3], 2);
			memcpy (&lief_nr[6], &datum[0], 2);
			if (di == 0)
			{
				lief_nr[8] = 0;
				return;
			}
			memcpy (&lief_nr[8], &cdi[0], 2);
			lief_nr[10] = 0;
			return;

}
void GenLS (void)
/**
Lieferscheinnummer generieren.
**/
{
        int dsqlstatus;
        short mdn;
        short fil;

        mdn = Mandant;
        fil = Filiale;
return;
        beginwork ();
        dsqlstatus = nvholid (mdn, fil, "ls");
        if (dsqlstatus == 100)
        {
              dsqlstatus = nvanmprf (mdn,
                                     fil,
                                     "ls",
                                     (long) 1,
                                     (long) 999999,
                                     (long) 10,
                                      "");

              if (dsqlstatus == 0)
              {
                          dsqlstatus = nvholid (mdn,
                                                fil,
                                                "ls");
              }
         }
         commitwork ();
         sprintf (lief_nr, "%ld", auto_nr.nr_nr);
         gen_lief_nr = auto_nr.nr_nr;
         sysdate (lieferdatum);
}



void LiefKopf (void)
/**
Auftragskopf bearbeiten.
**/
{
        RECT rect;
        MSG msg;
        int x, y, cx, cy;
//		char datum[12];

        if (KopfEnterAktiv == 1) return;
        if (LiefEnterAktiv) return;

        MainBuInactiv ();
        freestat = 0;
        CloseControls (&AufKopfT);
        CloseControls (&AufKopfE);
        CloseControls (&AufKopfTD);
        CloseControls (&AufKopfED);

        lspos = 0;
        RegisterKopfEnter ();
        GetWindowRect (LogoWindow, &rect);
        x = rect.left;
        y = rect.top;
        cx = rect.right - rect.left;
        cy = rect.bottom - rect.top;

//		lsk.auf_art = auf_art;
        KonvLiefKopf (); 
        if (KopfEnterAktiv == 0)
		{
			InitLiefKopf ();
		}
		if (lsdefault && best_kopf.best_blg == 0)
		{
//			GenLief_rech_nr(0);
			sysdate (lieferdatum);
			if (atoi (EinAusgang) == BUCHENFERTIGLAGER) 
			{
				 sprintf (lief_nr,"%ld", default_kunde); //fisch-5

			}
			else if (atoi (EinAusgang) == PRODUKTIONSSTART)
			{
			}
			else
			{
				GenPartie0();
			}
			/*
			sysdate (datum);
			memcpy (lief_nr, &datum[6], 4);
			memcpy (&lief_nr[4], &datum[3], 2);
			memcpy (&lief_nr[6], &datum[0], 2);
			lief_nr[8] = 0;
			*/
		}
//		sysdate (lieferdatum);
//		sysdate (beldatum);
//        GenLS ();

//Initialisieren 
		prodartikel.a = 0; 
		zerl_k.a_prod = 0;
		zerl_k.fanggebiet = 0;
		strcpy (artikel,"");
		strcpy (fanggebiet,"");

        beginwork ();
        LiefEnterAktiv = 1;
		strcpy (ls_stat,"0");
        if (AufKopfWindow == 0)
        {
                  AufKopfWindow  = CreateWindowEx (
                              0,
                              "AufKopf",
                              "",
                              WS_POPUP,
                              x, y,
                              cx, cy,
                              hMainWindow,
                              NULL,
                              hMainInst,
                              NULL);
        }

        if (AufKopfWindow == NULL)
        {
                 LiefEnterAktiv = 0;
                 return;
        }
        ShowWindow (AufKopfWindow, SW_SHOW);
        UpdateWindow (AufKopfWindow);
	    if (atoi(EinAusgang) == PRODUKTIONSSTART || atoi(EinAusgang) == BUCHENFERTIGLAGER ) 
		{
			ActivateColButton (BackWindow3, &MainButton2, 2, 1, 1); //OK-Button   FISCH-5

		}
        create_enter_form (AufKopfWindow, &LiefKopfE, 0, 0);
  //FISCH-5      create_enter_form (AufKopfWindow, &LiefKopfED, 0, 0);
//        if (KopfEnterAktiv == 2)  create_enter_form (AufKopfWindow, &LiefKopfBest, 0, 0);
//        if (KopfEnterAktiv == 2)  create_enter_form (AufKopfWindow, &LiefKopfBestT, 0, 0);
        SetLiefFocus (lspos);
        break_lief_enter = 0;
		
		if (PartieNr > 0)
		{
			break_lief_enter = 1;
            NuBlock.EnterNumBox (AufKopfWindow,"  Zerlege-Datum  ",
                               lieferdatum, 11,
                                 LiefKopfED.mask[0].picture, EntNuProc);
            display_field (AufKopfWindow, &LiefKopfED.mask[0], 0, 0);
			GenPartie();
			if (break_lief_enter == 0)	menfunc2();
		}
		else
		{
			while (GetMessage (&msg, NULL, 0, 0))
			{
				if (IsLiefMessage (&msg));
				else
				{
	                TranslateMessage(&msg);
					DispatchMessage(&msg);
				}
				if (break_lief_enter) break;
			}
		}
        CloseControls (&LiefKopfT);
        CloseControls (&LiefKopfE);
        CloseControls (&LiefKopfTD);
        CloseControls (&LiefKopfED);

//		if (KopfEnterAktiv == 2) CloseControls (&LiefKopfBest);
//		if (KopfEnterAktiv == 2) CloseControls (&LiefKopfBestT);
        DestroyWindow (AufKopfWindow);
        AufKopfWindow = NULL;
        LiefEnterAktiv = 0;
        MainBuActiv ();
		rollbackwork ();
        ActivateColButton (BackWindow3, &MainButton2, 2, -1, 1);
}


/* Auswahl ueber Geraete (sys_peri    */

#define MCI 6
#define GX  16

struct SYSPS
{
        char sys [9];
        char peri_typ [3];
        char txt [61];
        char peri_nam [21];
};


struct SYSPS sysarr [50], sysps;


static int sysidx;
static int sysanz = 0;

mfont SysFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                1,
                                NULL};

static field _sysform [] =
{
        sysps.sys,      12, 1, 0, 1, 0, "%8d", DISPLAYONLY, 0, 0, 0,
        sysps.peri_typ,  4, 1, 0,13, 0, "",    DISPLAYONLY, 0, 0, 0,
        sysps.txt,      20, 1, 0,17, 0, "",    DISPLAYONLY, 0, 0, 0
};

static form sysform = {3, 0, 0, _sysform, 0, 0, 0, 0, &SysFont};

static field _sysub [] =
{
        "Ger�te-Nr",     12, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        "Typ",            4, 1, 0,13, 0, "", DISPLAYONLY, 0, 0, 0,
        "Ger�t",         20, 1, 0,17, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form sysub = {3, 0, 0, _sysub, 0, 0, 0, 0, &SysFont};

static field _sysvl [] =
{
    "1",                  1,  0, 0, 12, 0, "", NORMAL, 0, 0, 0,
    "1",                  1,  0, 0, 16, 0, "", NORMAL, 0, 0, 0,
};

static form sysvl = {2, 0, 0, _sysvl, 0, 0, 0, 0, &SysFont};

int ShowSysPeri (HWND hWnd, char * ptabarr, int ptabanz, char * ptab,
                 int ptsize, form *ptform, form *ptvl, form *ptub, int laenge,int breite, HWND pWindow)
{
        RECT rect;
        RECT frmrect;
        int x, y, cx, cy;
        HWND shListBox;
        HWND slbox;
        struct LMENUE smenue;

        GetMenue (&smenue);
        MenTextSave (&smenue);
        shListBox = GethListBox ();
        slbox = Getlbox ();

        RegisterPt ();
        GetWindowRect (hMainWindow, &rect);
        GetFrmRectEx (ptform, ptform->font, &frmrect);

        x = rect.left + sysxplus;
        y = rect.top + 20;
        cx = frmrect.right + laenge;
//        cy = 185;
        cy = breite;

        set_fkt (endlist, 5);
        SetDblClck (IsPtClck, 0);
        PtListeAktiv = 1;
        SetAktivWindow (hWnd);
//        SetListEWindow (1);
        SetListFont (TRUE);
        spezfont (ptform->font);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_VISIBLE);
        PtWindow = OpenListWindowBoEx (x, y, cx, cy);
//        PtWindow = OpenListWindowEnEx (x, y, cx, cy);
        GetWindowRect (PtWindow, &rect);
        y = rect.bottom;
		cx = rect.right - rect.left;
        PtFussWindow = CreateWindow ("PtFuss",
                                    "",
                                    WS_POPUP | WS_VISIBLE | WS_DLGFRAME,
                                    x,
//                                    y + cy + 23,
                                    y,
                                    cx, 70,
                                    hWnd,
                                    NULL,
                                    hMainInst,
                                    NULL);
        if (ptvl) ElistVl (ptvl);
        if (ptub) ElistUb (ptub);
        EnableWindow (hMainWindow, FALSE);
        EnableWindow (pWindow, FALSE);
		AuswahlAktiv = TRUE;
        ShowElist ((char *) ptabarr,
                  ptabanz,
                  (char *) ptab,
                  (int) ptsize,
                  ptform);
//        SetMouseLock (TRUE);
        SetMouseLockProc (MouseInPt);
        EnterElist (PtWindow, (char *) ptabarr,
                             ptabanz,
                             (char *) ptab,
                             (int) ptsize,
                             ptform);
		AuswahlAktiv = FALSE;
        CloseControls (&PtButton);
        CloseEWindow (PtWindow);
        SetActiveWindow (hMainWindow);
        EnableWindow (hMainWindow, TRUE);
        EnableWindow (pWindow, TRUE);
        init_break_list ();
//        SetMouseLock (FALSE);
        SetMouseLockProc (NULL);
        DestroyWindow (PtFussWindow);
        PtListeAktiv = 0;
        SetMenue (&smenue);
        SethListBox (shListBox);
        Setlbox (slbox);
        SetAktivWindow (hWnd);
        return ptidx;
}


int AuswahlSysPeri (void)
/**
Ausahlfenster ueber Geraete anzeigen.
**/
{
        if (sysanz == 0)
        {
               dsqlstatus = sys_peri_class.lese_sys_peri ("order by sys");
               while (dsqlstatus == 0)
               {
                   if (sys_peri.peri_typ == MCI)
                   {
                       sprintf (sysarr[sysanz].sys, "%ld",
                                     sys_peri.sys);
                       sprintf (sysarr[sysanz].peri_typ,"%hd",
                                     sys_peri.peri_typ);
                       strcpy (sysarr[sysanz].txt, sys_peri.txt);
                       strcpy (sysarr[sysanz].peri_nam,
                                      sys_peri.peri_nam);
                       sysanz ++;
                       if (sysanz == 50) break;
                   }
                   dsqlstatus = sys_peri_class.lese_sys_peri ();
               }
        }
        if (sysanz == 0)
        {
               disp_messG ("Keine Ger�te gefunden", 1);
               return 0;
        }
        syskey = 0;
        sysidx = ShowSysPeri (LogoWindow, (char *) sysarr, sysanz,
                     (char *) &sysps,(int) sizeof (struct SYSPS),
                     &sysform, &sysvl, &sysub, 15,185,hMainWindow);
        if (syskey == KEY5 || syskey == KEYESC)
        {
                return 0;
        }
        if (atoi (sysarr[sysidx].peri_typ) == 6)
        {
                strcpy (waadir, sysarr[sysidx].peri_nam);
                clipped (waadir);
                sprintf (ergebnis, "%s\\ergebnis", waadir);
                WaaInit = 0;
                WaageInit ();
                sys_peri_class.lese_sys (atol (sysarr[sysidx].sys));
                la_zahl = sys_peri.la_zahl;
                AktKopf = 0;
        }
        else
        {
              auszeichner = atol (sysarr[sysidx].sys);
//            F�r Preisauszeichner noch nicht aktiv.
        }

        return sysidx;
}

// Wiegart ausw�hlen. Standard oder Entnahmeverwiegung

struct WGA
{
    char wiegart [30];
}; 

struct WGA wiegart;

struct WGA wgaarr[] = {"  Standardverwiegung",
                       "  Entnahmeverwiegung",
                       0};

static int wgaidx;
static int wgaanz = 2;

mfont WgaFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                1,
                                NULL};

static field _wgaform [] =
{
        wiegart.wiegart,  22, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form wgaform = {1, 0, 0, _wgaform, 0, 0, 0, 0, &WgaFont};

static field _wgaub [] =
{
        "Wiegart",       22, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form wgaub = {1, 0, 0, _wgaub, 0, 0, 0, 0, &WgaFont};

static field _wgavl [] =
{
    "1",                  1,  0, 0, 23, 0, "", NORMAL, 0, 0, 0,
};

static form wgavl = {0, 0, 0, _wgavl, 0, 0, 0, 0, &WgaFont};

int AuswahlWiegArt (void)
/**
Ausahlfenster ueber Geraete anzeigen.
**/
{

        Entnahmehand = FALSE;
        syskey = 0;
        wgaidx = ShowSysPeri (LogoWindow, (char *) wgaarr, wgaanz,
                     (char *) &wiegart,(int) sizeof (struct WGA),
                     &wgaform, &wgavl, &wgaub, 15,185,hMainWindow);
        if (syskey == KEY5 || syskey == KEYESC)
        {
                return 0;
        }
        WiegeTyp = wgaidx;
        if (WiegeTyp == STANDARD)
        {
                 ActivateColButton (BackWindow3, &MainButton2, 0, -1, 1);
        }
        else if (WiegeTyp == ENTNAHME)
        {
                 if (abfragejnG (NULL, "Handeingabe wie Entnahmewiegung ?", "J"))
                 {
                     Entnahmehand = TRUE;
                 }
                 ActivateColButton (BackWindow3, &MainButton2, 0, 1, 1);
        }
        return wgaidx;
}

struct WABT
{
	short abt;
    char abteilung [30];
}; 

struct WABT abteilung;
struct WABT abtarr[10]; 

/*
struct WABT abtarr[] = {1,"Zerlegeeingang",
                        2,"Zerlegeausgang ",
                        3, "Umlagerung ",
                        4, "Zerlegung ",
                        0};
**/

static int abtidx;
static int abtanz ;

mfont AbtFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                1,
                                NULL};

static field _abtform [] =
{
        abteilung.abteilung,  22, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form abtform = {1, 0, 0, _abtform, 0, 0, 0, 0, &AbtFont};

static field _abtaub [] =
{
        "Abteilung",       22, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form abtaub = {1, 0, 0, _abtaub, 0, 0, 0, 0, &AbtFont};

static field _abtavl [] =
{
    "1",                  1,  0, 0, 23, 0, "", NORMAL, 0, 0, 0,
};

static form abtavl = {0, 0, 0, _abtavl, 0, 0, 0, 0, &AbtFont};

int AuswahlAbteilung (void)
/**
Ausahlfenster ueber Geraete anzeigen.
**/
{
	abtanz = 0;
	if (AuswahlZerlegeeingang)
	{
		abtanz ++;
		abtarr[abtanz-1].abt = EINGANG;
		strcpy (abtarr[abtanz-1].abteilung, "Zerlegeeingang");
	}
	if (AuswahlZerlegeausgang)
	{
		abtanz ++;
		abtarr[abtanz-1].abt = AUSGANG;
		strcpy (abtarr[abtanz-1].abteilung, "Zerlegeausgang");
	}
	if (AuswahlUmlagerung)
	{
		abtanz ++;
		abtarr[abtanz-1].abt = UMLAGERUNG;
		strcpy (abtarr[abtanz-1].abteilung, "Umlagerung");
	}
	if (AuswahlZerlegung)
	{
		abtanz ++;
		abtarr[abtanz-1].abt = ZERLEGUNG;
		strcpy (abtarr[abtanz-1].abteilung, "Zerlegung");
	}
	if (AuswahlFeinZerlegung)
	{
		abtanz ++;
		abtarr[abtanz-1].abt = FEINZERLEGUNG;
		strcpy (abtarr[abtanz-1].abteilung, "Feinzerlegung");
	}

	if (AuswahlProduktionsstart)
	{
		abtanz ++;
		abtarr[abtanz-1].abt = PRODUKTIONSSTART;
		strcpy (abtarr[abtanz-1].abteilung, "Produktionsstart");
	}
	if (AuswahlProduktionsfortschritt)
	{
		abtanz ++;
		abtarr[abtanz-1].abt = PRODUKTIONSFORTSCHRITT;
		strcpy (abtarr[abtanz-1].abteilung, "Produktionsfortschritt");
	}
	if (AuswahlBuchenFertiglager)
	{
		abtanz ++;
		abtarr[abtanz-1].abt = BUCHENFERTIGLAGER;
		strcpy (abtarr[abtanz-1].abteilung, "Buchen in Fertiglager");
	}


        syskey = 0;
        abtidx = ShowSysPeri (LogoWindow, (char *) abtarr, abtanz,
                     (char *) &abteilung,(int) sizeof (struct WABT),
                     &abtform, &abtavl, &abtaub, 15,185,hMainWindow);
        if (syskey == KEY5 || syskey == KEYESC)
        {
                return 0;
        }
        return abtarr[abtidx].abt;
}



// Lager ausw�hlen. 

struct LAGER
{
    char lgr [10];
    char lgr_bz [25];
};


struct LAGER lgrarr[50], lager;

static int lgridx;
static int lgranz = 0;

mfont LgrFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                1,
                                NULL};

static field _lgrform [] =
{
        lager.lgr,  8, 1, 0, 1, 0, "%ld", DISPLAYONLY, 0, 0, 0,
        lager.lgr_bz,  24, 1, 0, 9, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form lgrform = {2, 0, 0, _lgrform, 0, 0, 0, 0, &LgrFont};

static field _lgrub [] =
{
        "Lager",       8, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        "Bezeichnung",       24, 1, 0, 9, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form lgrub = {2, 0, 0, _lgrub, 0, 0, 0, 0, &LgrFont};

static field _lgrvl [] =
{
    "1",                  1,  0, 0, 9, 0, "", NORMAL, 0, 0, 0,
};

static form lgrvl = {1, 0, 0, _lgrvl, 0, 0, 0, 0, &LgrFont};

int AuswahlLager (int zuab, int lgrsav)
/**
Ausahlfenster ueber vorh. L�ger anzeigen.
**/
{
	  lgranz = 0;
      while (TRUE)
	  {
               dsqlstatus = lgr_class.lese_lgr ("order by lgr");
               while (dsqlstatus == 0)
               {
                       sprintf (lgrarr[lgranz].lgr, "%ld",
                                     lgr.lgr);
                       strcpy (lgrarr[lgranz].lgr_bz, lgr.lgr_bz);
                       lgranz ++;
                       if (lgranz == 50) break;
                   dsqlstatus = lgr_class.lese_lgr ();
               }
        if (lgranz == 0)
        {
               disp_messG ("Kein Lager gefunden", 1);
               return lgrsav;
        }
        syskey = 0;
        lgridx = ShowSysPeri (LogoWindow, (char *) lgrarr, lgranz,
                     (char *) &lager,(int) sizeof (struct LAGER),
                     &lgrform, &lgrvl, &lgrub, 15,185,hMainWindow);
        if (syskey == KEY5 || syskey == KEYESC)
        {
                return lgrsav;
        }
		if (atoi (lgrarr[lgridx].lgr) < -1) 
		{
              disp_messG ("Diese Auswahl ist zur Zeit\n"
                          "nicht m�glich", 2);
			continue;
		}
		break;
	  }

        return atoi (lgrarr[lgridx].lgr); 
}




int AuswahlALager (int lgrsav, bool lZugang)
{
      while (TRUE)
	  {
	    a_lgr.a = ratod(aufps.a);  
  	    lgranz = 0 ; // immer wieder neu lesen, wg. Artikel-Lagerzuordnung
        if (lgranz == 0)
        {
               dsqlstatus = lgr_class.lese_lgr ("order by lgr");
               while (dsqlstatus == 0)
               {
                       sprintf (lgrarr[lgranz].lgr, "%ld",
                                     lgr.lgr);
                       strcpy (lgrarr[lgranz].lgr_bz, lgr.lgr_bz);
                       lgranz ++;
                       if (lgranz == 50) break;
                   dsqlstatus = lgr_class.lese_lgr ();
               }
        }
		/***
        if (lgranz == 0)
        {
               disp_messG ("Kein Lager gefunden", 1);
               return lgrsav;
        }
		***/
        syskey = 0;
		 sprintf(Zuruecktext,"Zuordn.�ndern");
        lgridx = ShowSysPeri (LogoWindow, (char *) lgrarr, lgranz,
                     (char *) &lager,(int) sizeof (struct LAGER),
                     &lgrform, &lgrvl, &lgrub,15,185,WiegWindow);
		 sprintf(Zuruecktext,"Zurueck F5");
        if (syskey == KEY5 || syskey == KEYESC)
        {
			//  Hier jetzt die Gesamtauswahl, 
		      lgranz = 0 ; // immer wieder neu lesen, wg. Artikel-Lagerzuordnung
			   a_lgr.a = 0.0;
			   syskey = 0;
               dsqlstatus = lgr_class.lese_lgr ("order by lgr");
               while (dsqlstatus == 0)
               {
                       sprintf (lgrarr[lgranz].lgr, "%ld",
                                     lgr.lgr);
                       strcpy (lgrarr[lgranz].lgr_bz, lgr.lgr_bz);
                       lgranz ++;
                       if (lgranz == 50) break;
                   dsqlstatus = lgr_class.lese_lgr ();
               }
		        lgridx = ShowSysPeri (LogoWindow, (char *) lgrarr, lgranz,
                     (char *) &lager,(int) sizeof (struct LAGER),
                     &lgrform, &lgrvl, &lgrub,15,185,WiegWindow);
			    if (syskey == KEY5 || syskey == KEYESC)
				{
					    break;
				}
//                AktivWindow = WiegWindow;
//				disp_messG ("Die Artikel-Lagerzuordnung wird aktualisiert\n"
//                          "", 2);
			a_lgr.lgr = atoi (lgrarr[lgridx].lgr);
		    a_lgr.a = ratod(aufps.a);  
			a_lgr.lgr_pla_kz = 0;
			a_lgr.mdn = zerl_k.mdn;
	        EnableWindow (hMainWindow, FALSE);
		    EnableWindow (WiegWindow, FALSE);
			if (a_lgr_class.dbreadfirst() == 100)
			{
	          if (abfragejnG (NULL,
                          "Lager in die Artikel-Lagerzuordnung hinzuf�gen ?.\n"
                          "", "J"))
			  {
					a_lgr_class.dbupdate();
			  }
			}
			else 
			{
	          if (abfragejnG (NULL,
                          "Lager ist schon vorhanden !\n"
                          "Soll das Lager aus der Artikel-Lagerzuordnung entfernt werden ?", "J"))
				a_lgr_class.dbdelete();
			}
	        EnableWindow (hMainWindow, TRUE);
		    EnableWindow (WiegWindow, TRUE);
			//hier jetzt mit einbuchen in a_lgr oder l�schen aus a_lgr
			continue;
		}
		if (atoi (lgrarr[lgridx].lgr) < -1) 
		{
              disp_messG ("Diese Auswahl ist zur Zeit\n"
                          "nicht m�glich", 2);
			continue;
		}
		break;
	  }
      if (lZugang == TRUE)
	  {
		   if (syskey == KEY5) 
		   {
			   return (lgrsav);
		   }
		   else
		   {
				akt_lager_zu = atoi (lgrarr[lgridx].lgr);
		   }
			lgr.lgr = (short) akt_lager_zu;
	        if ( lgr_class.lese_lgr_bz (akt_lager_zu) == 0) sprintf (LagertxtZu,"Nach: %s",lgr.lgr_bz);
			 a_lgr.mdn = zerl_k.mdn;
			 a_lgr.lgr = lgr.lgr;
			 if (a_lgr_class.dbreadfirst() == 0)
			 {
				 sprintf (Buchartikeltxt, "");
				 if (a_lgr.buch_artikel > 0)
				 {
					 sprintf (Buchartikeltxt, "Art. %13.0lf",a_lgr.buch_artikel);
				 }
			 }
	  }
	  else
	  {
		   if (syskey == KEY5) 
		   {
			   return (lgrsav);
		   }
		   else
		   {
				akt_lager_ab = atoi (lgrarr[lgridx].lgr);
		   }
			lgr.lgr = (short) akt_lager_ab;
	        if ( lgr_class.lese_lgr_bz (akt_lager_ab) == 0) sprintf (LagertxtAb,"Von: %s",lgr.lgr_bz);
	  }

      return atoi (lgrarr[lgridx].lgr); 
}




// Verpackungen ausw�hlen. 

struct VPK
{
    char a [12];
    char a_bz [50];
    char a_gew [10];
};


struct VPK vpkarr[50], vpk;

static int vpkidx;
static int vpkanz = 0;

mfont VpkFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                1,
                                NULL};

static field _vpkform [] =
{
        vpk.a,  10, 1, 0, 1, 0, "%.0lf", DISPLAYONLY, 0, 0, 0,
        vpk.a_bz,  38, 1, 0, 11, 0, "", DISPLAYONLY, 0, 0, 0,
        vpk.a_gew,  6, 1, 0, 50, 0, "&.3lf", DISPLAYONLY, 0, 0, 0,
};

static form vpkform = {3, 0, 0, _vpkform, 0, 0, 0, 0, &VpkFont};

static field _vpkub [] =
{
        "Artikel",           10, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        "Bezeichnung",       38, 1, 0, 11, 0, "", DISPLAYONLY, 0, 0, 0,
        "Art.Gew",           6, 1, 0, 50, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form vpkub = {3, 0, 0, _vpkub, 0, 0, 0, 0, &VpkFont};

static field _vpkvl [] =
{
    "1",                  1,  0, 0, 10, 0, "", NORMAL, 0, 0, 0,
    "1",                  1,  0, 0, 49, 0, "", NORMAL, 0, 0, 0,
};

static form vpkvl = {2, 0, 0, _vpkvl, 0, 0, 0, 0, &VpkFont};


double AuswahlVerpackungstara (short typ)
{
          char buffer [256];
		static int vpk_cursor;
		double a;
		double a_gew;
		char a_bz1 [25];
		char a_bz2 [25];
		short typ1;
		short typ2;

  	    vpkanz = 0 ; // immer wieder neu lesen, wg. Artikel-Lagerzuordnung
        if (vpkanz == 0)
        {
			if (typ == NEUES_LEERGUT)
			{
				typ1 = 11;
				typ2 = 11;
			}
			else if (typ == NEUE_VERPACKUNGEN)
			{
				typ1 = 8;
				typ2 = 9;
			}
			else return 0.0;

		    DbClass.sqlin ((short *) &typ1, 1, 0);
		    DbClass.sqlin ((short *) &typ2, 1, 0);
		    DbClass.sqlin ((short *) &typ1, 1, 0);
		    DbClass.sqlin ((short *) &typ2, 1, 0);
		    DbClass.sqlout ((short *) &a, 3, 0);
		    DbClass.sqlout ((short *) a_bz1, 0, 24);
		    DbClass.sqlout ((short *) a_bz2, 0, 24);
		    DbClass.sqlout ((short *) &a_gew, 3, 0);
			vpk_cursor = DbClass.sqlcursor ("select a,a_bz1,a_bz2,a_gew from a_bas where a_typ in (?,?) or a_typ2 in (?,?)   ");

			dsqlstatus = DbClass.sqlfetch (vpk_cursor);
			while (dsqlstatus == 0)
			{
				strcpy (vpk.a_bz , clipped(a_bz1));
				strcat (vpk.a_bz, clipped(a_bz2));
	            sprintf (vpkarr[vpkanz].a, "%.0lf",
							a);
				strcpy (vpkarr[vpkanz].a_bz, vpk.a_bz);
	            sprintf (vpkarr[vpkanz].a_gew, "%.3lf",
							a_gew);
				vpkanz ++;
				if (vpkanz == 50) break;
				dsqlstatus = DbClass.sqlfetch (vpk_cursor);
			}
		}

        syskey = 0;
//		 sprintf(Zuruecktext,"Zuordn.�ndern");
        vpkidx = ShowSysPeri (LogoWindow, (char *) vpkarr, vpkanz,
                     (char *) &vpk,(int) sizeof (struct VPK),
                     &vpkform, &vpkvl, &vpkub,15,185,WiegWindow);
		 sprintf(Zuruecktext,"Zurueck F5");
        if (syskey == KEY5 || syskey == KEYESC)
        {
			return 0.0;
		}

        NuBlock.SetParent (LogoWindow);
        sprintf (buffer, "%.3lf", ratod(vpkarr[vpkidx].a_gew));
        NuBlock.EnterCalcBox (WiegWindow,"  Gewicht ",  buffer, 8, "%.3lf", EntNuProc);
        if (syskey != KEY5 && ratod (buffer) > 0.0001)
        {
			 if (typ == NEUE_VERPACKUNGEN) a_tara.a = ratod(aufps.a);
			 if (typ == NEUES_LEERGUT) a_tara.a = 0.0;
			 a_tara.a_vpk = ratod(vpkarr[vpkidx].a);

			 strcpy(a_tara.a_krz,clipped(vpkarr[vpkidx].a));//TODO hier DIALOG einbauen , um K�rzel einzugeben
			 EnterA_Krz();
			 if (syskey != KEY5)
			 {
				a_tara.a_gew = ratod(buffer);
				a_taraClass.dbupdate();
				commitwork();
				beginwork();
	 			if (typ == NEUE_VERPACKUNGEN) EinhVPOK = FALSE;   //a_tara muss nochmal neu eingelesen werden
	 			if (typ == NEUES_LEERGUT) EinhLPOK = FALSE;   //a_tara muss nochmal neu eingelesen werden
			 }

		   
             return ratod (buffer);
        }
      return 0.0;
}








int AuswahlLager (void)
/**
Ausahlfenster ueber L�ger anzeigen wenn zuordnung in a_lgr nicht eindeutig.
**/
{
	  lgranz = 0 ; // immer wieder neu lesen, wg. Artikel-Lagerzuordnung
      a_lgr.a = ratod(aufps.a);  
	  if (aufps.lgr_ab > 0) 
	  {
			akt_lager_ab = aufps.lgr_ab;
	  }
	  else
	  {
		  akt_lager_ab = LagerAb;
	  }
 	  lgr.lgr = (short) akt_lager_ab;
	  if ( lgr_class.lese_lgr_bz (akt_lager_ab) == 0) sprintf (LagertxtAb0,"%s",lgr.lgr_bz);

	  if (aufps.lgr_zu > 0) 
	  {
			akt_lager_zu = aufps.lgr_zu;
	  }
	  else
	  {
		  akt_lager_zu = LagerZu;
	  }
  	  lgr.lgr = (short) akt_lager_zu;
	  if ( lgr_class.lese_lgr_bz (akt_lager_zu) == 0) sprintf (LagertxtZu0,"%s",lgr.lgr_bz);
	  a_lgr.mdn = zerl_k.mdn;
	  a_lgr.lgr = lgr.lgr;
	  if (a_lgr_class.dbreadfirst() == 0)
	  {
	 	 sprintf (Buchartikeltxt, "");
		 if (a_lgr.buch_artikel > 0)
		 {
			sprintf (Buchartikeltxt, "Art. %13.0lf",a_lgr.buch_artikel);
		 }
	  }
      dsqlstatus = lgr_class.lese_lgr ("order by lgr");
	  bool lager_zu_gefunden = FALSE;
	  bool lager_ab_gefunden = FALSE;
	  sprintf (LagertxtAb,"Von : %s",LagertxtAb0);
	  sprintf (LagertxtZu,"Nach: %s",LagertxtZu0);
      while (dsqlstatus == 0)
      {
		  if (akt_lager_zu == lgr.lgr) lager_zu_gefunden = TRUE;
		  if (akt_lager_ab == lgr.lgr) lager_ab_gefunden = TRUE;
          lgranz ++;
          dsqlstatus = lgr_class.lese_lgr ();
      }
       ColorColButton (WiegCtr, &LgrSpezButton, 0, BLUECOL, 0);
       ColorColButton (WiegCtr, &LgrSpezButton, 2, BLUECOL, 0);
      if (lager_ab_gefunden == FALSE) // && (atoi(EinAusgang) == UMLAGERUNG || atoi(EinAusgang) == EINGANG )) //Nur Pr�fen bei Zerlegeeingang und Umlagerung 
      {
          ColorColButton (WiegCtr, &LgrSpezButton, 2, REDCOL, 0);
		  sprintf (LagertxtAb,"Keine Zuordnung Lgr: %d",akt_lager_ab);
		  sprintf (LagertxtAb,"!!!!! %s",LagertxtAb0);
//		  akt_lager_ab = 0;
	  }
      if (lager_zu_gefunden == FALSE )//  && atoi(EinAusgang) != EINGANG) //Nur Pr�fen bei Zerlegeausgang und Umlagerung
      {
          ColorColButton (WiegCtr, &LgrSpezButton, 0, REDCOL, 0);
		  sprintf (LagertxtZu,"Zuordn.!!! %s",lgr.lgr_bz);
		  sprintf (LagertxtZu,"!!!!! %s",LagertxtZu0);
//		  akt_lager_zu = 0;
	  }
      return akt_lager_zu; 

/******************************


      while (TRUE)
	  {
        if (lgranz == 0)
        {
               dsqlstatus = lgr_class.lese_lgr ("order by lgr");
               while (dsqlstatus == 0)
               {
                       sprintf (lgrarr[lgranz].lgr, "%ld",
                                     lgr.lgr);
                       strcpy (lgrarr[lgranz].lgr_bz, lgr.lgr_bz);
                       lgranz ++;
                       if (lgranz == 50) break;
                   dsqlstatus = lgr_class.lese_lgr ();
               }
        }
		lgridx = 0;
        if (lgranz == 0)
        {
               disp_messG ("Kein Lager gefunden", 1);
               return 0;
        }
		if (lgranz > 1)
		{
			syskey = 0;
			lgridx = ShowSysPeri (LogoWindow, (char *) lgrarr, lgranz,
						(char *) &lager,(int) sizeof (struct LAGER),
						&lgrform, &lgrvl, &lgrub,15,185,hMainWindow);
			if (syskey == KEY5 || syskey == KEYESC)
			{
					continue;
			}
			if (atoi (lgrarr[lgridx].lgr) < -1) 
			{
				disp_messG ("Diese Auswahl ist zur Zeit\n"
							"nicht m�glich", 2);
				continue;
			}
		}
		break;
	  }


	  akt_lager = atoi (lgrarr[lgridx].lgr); 
	  lgr.lgr = (short) akt_lager;
        if ( lgr_class.lese_lgr_bz (akt_lager) == 0) sprintf (Lagertxt,"Lager: %s",lgr.lgr_bz);
//         memcpy (&a_lgr, &a_lgr_null, sizeof (struct A_LGR));
		 a_lgr.mdn = zerl_k.mdn;
		 a_lgr.lgr = lgr.lgr;
		 a_lgr_class.dbreadfirst();

       return akt_lager ;
	   **********************************/
}


// Partie ausw�hlen. 

struct PARTIE
{
    char zer_part [10];
    char zer_bz [49];
};


struct PARTIE partiearr[50], partie;

static int partieidx;
static int partieanz = 0;

mfont PartieFont = {"Courier New", 280, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                1,
                                NULL};

static field _partieform [] =
{
        partie.zer_part,  8, 1, 0, 1, 0, "%ld", DISPLAYONLY, 0, 0, 0,
        partie.zer_bz,  24, 1, 0, 9, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form partieform = {2, 0, 0, _partieform, 0, 0, 0, 0, &PartieFont};

static field _partieub [] =
{
        "Partie",       8, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        "Bezeichnung",       48, 1, 0, 9, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form partieub = {2, 0, 0, _partieub, 0, 0, 0, 0, &PartieFont};

static field _partievl [] =
{
    "1",                  1,  0, 0, 9, 0, "", NORMAL, 0, 0, 0,
};

static form partievl = {1, 0, 0, _partievl, 0, 0, 0, 0, &PartieFont};

int AuswahlPartie (void)
/**
Ausahlfenster ueber Standardpartien (zer_partk)
**/
{
      while (TRUE)
	  {
        if (partieanz == 0)
        {
               dsqlstatus = zer_partk_class.lese_partie ("order by zer_part");
               while (dsqlstatus == 0)
               {
                       sprintf (partiearr[partieanz].zer_part, "%s",
                                     zer_partk.zer_part);
                       strcpy (partiearr[partieanz].zer_bz, zer_partk.zer_bz);
                       partieanz ++;
                       if (partieanz == 50) break;
                   dsqlstatus = zer_partk_class.lese_partie ();
               }
        }
        if (partieanz == 0)
        {
               disp_messG ("Keine Standardpartie gefunden", 1);
               return 0;
        }
        syskey = 0;
        partieidx = ShowSysPeri (LogoWindow, (char *) partiearr, partieanz,
                     (char *) &partie,(int) sizeof (struct PARTIE),
                     &partieform, &partievl, &partieub, 120,260,hMainWindow);
        if (syskey == KEY5 || syskey == KEYESC)
        {
                return 0;
        }
		if (atoi (partiearr[lgridx].zer_part) < -1) 
		{
              disp_messG ("Diese Auswahl ist zur Zeit\n"
                          "nicht m�glich", 2);
			continue;
		}
		break;
	  }

		 sprintf (bestellhinweis, "%s", partiearr[partieidx].zer_bz);

		zer_partk.mdn = Mandant;
		strcpy(zer_partk.zer_part,partiearr[partieidx].zer_part);
	    zer_partk_class.lese_partie_bz(atoi(zer_partk.zer_part));
        return atoi(zer_partk.zer_part);
}










// Auswahl �ber Wiegungen aus wiepro


mfont WiepFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                                 RGB (255, 255, 255),
                                 1,
                                 NULL};

static field _wiepform [] =
{
        wiep.a,      14, 1, 0,  1, 0, "%13.0f", DISPLAYONLY, 0, 0, 0,
        wiep.a_bz1,  20, 1, 0, 17, 0, "",       DISPLAYONLY, 0, 0, 0,
        wiep.brutto, 11, 1, 0, 39, 0, "%7.3",   DISPLAYONLY, 0, 0, 0,
        wiep.zeit,    6, 1, 0, 51, 0, "",       DISPLAYONLY, 0, 0, 0,
};

static form wiepform = {4, 0, 0, _wiepform, 0, 0, 0, 0, &WiepFont};

static field _wiepub [] =
{
        "Artikel",     15, 1, 0,  1, 0, "",   DISPLAYONLY, 0, 0, 0,
        "Bezeichnung", 22, 1, 0, 16, 0, "",   DISPLAYONLY, 0, 0, 0,
        "Gewicht",     12, 1, 0, 38, 0, "",   DISPLAYONLY, 0, 0, 0,
        "Zeit",         7, 1, 0, 50, 0, "",   DISPLAYONLY, 0, 0, 0,
};

static form wiepub = {4, 0, 0, _wiepub, 0, 0, 0, 0, &WiepFont};

static field _wiepvl [] =
{
    "1",                  1,  0, 0, 16, 0, "", NORMAL, 0, 0, 0,
    "1",                  1,  0, 0, 38, 0, "", NORMAL, 0, 0, 0,
    "1",                  1,  0, 0, 50, 0, "", NORMAL, 0, 0, 0,
};

static form wiepvl = {3, 0, 0, _wiepvl, 0, 0, 0, 0, &WiepFont};

int AuswahlWiePro (void)
/**
Ausahlfenster ueber Geraete anzeigen.
**/
{
        int cursor;
        int idx;

		int lief_s;
		lief_s = atoi(zerl_k.lief);
		char lief_rech_nr [16];
		sprintf (lief_rech_nr,"%8.0lf",zerl_k.partie);
        sql_in ((char *)  &akt_mdn, 1, 0);
        sql_in ((char *)  lief_rech_nr , 0, 17);
        sql_in ((char *)  &lief_s , 2, 0);
        sql_out ((char *) wiep.a, 0, 14);
        sql_out ((char *) wiep.a_bz1, 0, 25);
        sql_out ((char *) wiep.brutto, 0, 11);
        sql_out ((char *) wiep.dat, 0, 11);
        sql_out ((char *) wiep.zeit, 0, 9);
        cursor = prepare_sql ("select we_wiepro.a, a_bas.a_bz1, me, dat, zeit "
                                      "from we_wiepro,a_bas "
                                      "where a_bas.a = we_wiepro.a "
                                      "and we_wiepro.mdn = ? "
                                      "and we_wiepro.lief_rech_nr = ? "
                                      "and we_wiepro.lief_s = ? "
                                      "order by dat desc, zeit desc");

        idx = 0;
        while (fetch_sql (cursor) == 0)
        {
                memcpy (&wieparr[idx], &wiep, sizeof (wiep));
                idx ++;
                if (idx == 20)
                {
                    break;
                }
        }
        close_sql (cursor);
        wiepanz = idx;

        sysxplus = 0;
        syskey = 0;
        wiepidx = ShowSysPeri (WiegWindow, (char *) wieparr, wiepanz,
                     (char *) &wiep,(int) sizeof (struct WIEP),
                     &wiepform, &wiepvl, &wiepub, 15,185,hMainWindow);
        sysxplus = 50;
        if (syskey == KEY5 || syskey == KEYESC)
        {
                return -1;
        }
        return wiepidx;
}


/* Auswahl ueber Wiegungen                    */


struct WIEGT
{
        char wieg [5];
        char menge [12];
};


struct WIEGT wiegarr [1000], wiegt;


static int wiegidx;
static int wieganz = 0;

mfont WiegFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                1,
                                NULL};

static field _wiegform [] =
{
        wiegt.wieg,      9, 1, 0, 1, 0, "%7d",   DISPLAYONLY, 0, 0, 0,
        wiegt.menge,    13, 1, 0,10, 0, ":9.3f", DISPLAYONLY, 0, 0, 0,
};

static form wiegform = {2, 0, 0, _wiegform, 0, 0, 0, 0, &WiegFont};

static field _wiegub [] =
{
        "Wiegung",        9, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        "Menge",         13, 1, 0,10, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form wiegub = {2, 0, 0, _wiegub, 0, 0, 0, 0, &WiegFont};

static field _wiegvl [] =
{
    "1",                  1,  0, 0,  9, 0, "", NORMAL, 0, 0, 0,
    "1",                  1,  0, 0, 22, 0, "", NORMAL, 0, 0, 0,
};

static form wiegvl = {2, 0, 0, _wiegvl, 0, 0, 0, 0, &WiegFont};

int ShowWieg (HWND hWnd, char * ptabarr, int ptabanz, char * ptab,
                 int ptsize, form *ptform, form *ptvl, form *ptub)
{
        RECT rect;
        RECT frmrect;
        int x, y, cx, cy;
        HWND shListBox;
        HWND slbox;
        struct LMENUE smenue;

        GetMenue (&smenue);
        MenTextSave (&smenue);
        shListBox = GethListBox ();
        slbox = Getlbox ();

        RegisterPt ();
        GetWindowRect (hMainWindow, &rect);
        GetFrmRectEx (ptform, ptform->font, &frmrect);

        x = rect.left + 50;
        y = rect.top + 20;
        cx = frmrect.right + 15;
        cy = 185;

        set_fkt (endlist, 5);
        SetDblClck (IsPtClck, 0);
        PtListeAktiv = 1;
        SetAktivWindow (hWnd);
//        SetListEWindow (1);
        SetListFont (TRUE);
        spezfont (ptform->font);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_VISIBLE);
//        PtWindow = OpenListWindowEnEx (x, y, cx, cy);
        PtWindow = OpenListWindowBoEx (x, y, cx, cy);
//        PtWindow = OpenListWindowEnEx (x, y, cx, cy);
        GetWindowRect (PtWindow, &rect);
        y = rect.bottom;
		cx = rect.right - rect.left;
        PtFussWindow = CreateWindow ("PtFuss",
                                    "",
                                    WS_POPUP | WS_VISIBLE | WS_DLGFRAME,
                                    x,
//                                    y + cy + 23,
                                    y,
                                    cx, 70,
                                    hWnd,
                                    NULL,
                                    hMainInst,
                                    NULL);
        if (ptvl) ElistVl (ptvl);
        if (ptub) ElistUb (ptub);
        EnableWindow (hMainWindow, FALSE);
        ShowElist ((char *) ptabarr,
                  ptabanz,
                  (char *) ptab,
                  (int) ptsize,
                  ptform);
//        SetMouseLock (TRUE);
        SetMouseLockProc (MouseInPt);
        EnterElist (PtWindow, (char *) ptabarr,
                             ptabanz,
                             (char *) ptab,
                             (int) ptsize,
                             ptform);
        CloseControls (&PtButton);
        CloseEWindow (PtWindow);
        SetActiveWindow (hMainWindow);
        EnableWindow (hMainWindow, TRUE);
        init_break_list ();
//        SetMouseLock (FALSE);
        SetMouseLockProc (NULL);
        DestroyWindow (PtFussWindow);
        PtListeAktiv = 0;
        SetMenue (&smenue);
        SethListBox (shListBox);
        Setlbox (slbox);
        SetAktivWindow (hWnd);
        return ptidx;
}

int AuswahlWieg (void)
/**
Ausahlfenster ueber Wiegungen anzeigen
**/
{
        int i;
        int j;

        for (i = AktWiegPos - 1, j = 0; i >= 0; i --, j ++)
        {
            sprintf (wiegarr[j].wieg, "%d", i + 1);
            sprintf (wiegarr[j].menge, "%.3lf", AktPrWieg[i]);
        }
        wieganz = j;

        if (wieganz == 0)
        {
               return 0;
        }
        syskey = 0;
        wiegidx = ShowWieg (WiegWindow, (char *) wiegarr, wieganz,
                     (char *) &wiegt,(int) sizeof (struct WIEGT),
                     &wiegform, &wiegvl, &wiegub);
        if (syskey == KEY5 || syskey == KEYESC)
        {
                return 0;
        }
        StornoWieg = ratod (wiegarr[wiegidx].menge);
        return TRUE;
}


/* Auswahl ueber Wiegekoepfe                    */


struct WKOPF
{
        char nr [10];
        char akt [10];
};


struct WKOPF wkopfarr [20], wkopf;


static int kopfidx;
static int kopfanz = 0;

mfont KopfFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                1,
                                NULL};

static field _wkopfform [] =
{
        wkopf.nr,     11, 1, 0, 1, 0, "%4d",   DISPLAYONLY, 0, 0, 0,
        wkopf.akt,    13, 1, 0,12, 0, "",      DISPLAYONLY, 0, 0, 0,
};

static form wkopfform = {2, 0, 0, _wkopfform, 0, 0, 0, 0, &KopfFont};

static field _wkopfub [] =
{
        "Wiegekopf",       11, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        "  aktiv",         13, 1, 0,12, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form wkopfub = {2, 0, 0, _wkopfub, 0, 0, 0, 0, &KopfFont};

static field _wkopfvl [] =
{
    "1",                  1,  0, 0, 12, 0, "", NORMAL, 0, 0, 0,
    "1",                  1,  0, 0, 24, 0, "", NORMAL, 0, 0, 0,
};

static form wkopfvl = {2, 0, 0, _wkopfvl, 0, 0, 0, 0, &KopfFont};

int ShowWKopf (HWND hWnd, char * ptabarr, int ptabanz, char * ptab,
                 int ptsize, form *ptform, form *ptvl, form *ptub)
{
        RECT rect;
        RECT frmrect;
        int x, y, cx, cy;
        HWND shListBox;
        HWND slbox;
        struct LMENUE smenue;

        GetMenue (&smenue);
        MenTextSave (&smenue);
        shListBox = GethListBox ();
        slbox = Getlbox ();

        RegisterPt ();
        GetWindowRect (hMainWindow, &rect);
        GetFrmRectEx (ptform, ptform->font, &frmrect);

        x = rect.left + 50;
        y = rect.top + 20;
        cx = frmrect.right + 15;
		if (PtY > -1)
		{
			cy = PtY;
			PtY = -1;
		}
		else
		{
            cy = 185;
		}

        set_fkt (endlist, 5);
        SetDblClck (IsPtClck, 0);
        PtListeAktiv = 1;
        SetAktivWindow (hWnd);
        SetListFont (TRUE);
        spezfont (ptform->font);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_VISIBLE);
        PtWindow = OpenListWindowBoEx (x, y, cx, cy);
        GetWindowRect (PtWindow, &rect);
        y = rect.bottom;
		cx = rect.right - rect.left;
        PtFussWindow = CreateWindow ("PtFuss",
                                    "",
                                    WS_POPUP | WS_VISIBLE | WS_DLGFRAME,
                                    x,
                                    y,
                                    cx, 70,
                                    hWnd,
                                    NULL,
                                    hMainInst,
                                    NULL);
        if (ptvl) ElistVl (ptvl);
        if (ptub) ElistUb (ptub);
        EnableWindow (hMainWindow, FALSE);
        ShowElist ((char *) ptabarr,
                  ptabanz,
                  (char *) ptab,
                  (int) ptsize,
                  ptform);
//        SetMouseLock (TRUE);
//        SetMouseLockProc (MouseInPt);
        EnterElist (PtWindow, (char *) ptabarr,
                             ptabanz,
                             (char *) ptab,
                             (int) ptsize,
                             ptform);
        CloseControls (&PtButton);
        CloseEWindow (PtWindow);
        SetActiveWindow (hMainWindow);
        EnableWindow (hMainWindow, TRUE);
        init_break_list ();
//        SetMouseLock (FALSE);
//        SetMouseLockProc (NULL);
        DestroyWindow (PtFussWindow);
        PtListeAktiv = 0;
        SetMenue (&smenue);
        SethListBox (shListBox);
        Setlbox (slbox);
        SetAktivWindow (hWnd);
        return ptidx;
}


int AuswahlKopf (void)
/**
Ausahlfenster ueber Geraete anzeigen.
**/
{
        int i;

        for (i = 0; i < la_zahl; i ++)
        {
                       sprintf (wkopfarr[i].nr, "%d", i + 1);
                       if (i == AktKopf)
                       {
                               sprintf (wkopfarr[i].akt, "   * ");
                       }
                       else
                       {
                               sprintf (wkopfarr[i].akt, "     ");
                       }
        }
        kopfanz = i;
        if (kopfanz == 0)
        {
               return 0;
        }
        syskey = 0;
        kopfidx = ShowWKopf (LogoWindow, (char *) wkopfarr, kopfanz,
                     (char *) &wkopf,(int) sizeof (struct WKOPF),
                     &wkopfform, &wkopfvl, &wkopfub);
        if (syskey == KEY5 || syskey == KEYESC)
        {
                return 0;
        }
        return kopfidx;
}


/* Auswahl bei leeren Positionen   */

mfont KtFont = {"Courier New", 150, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                1,
                                NULL};

static char ktext [61];
static char ktextarr [10][61];

static int ktidx;

static field _ktform [] =
{
        ktext,     50, 1, 0, 1, 0, "",   DISPLAYONLY, 0, 0, 0,
};

static form ktform = {1, 0, 0, _ktform, 0, 0, 0, 0, &KtFont};

static field _ktub [] =
{
        "Auswahl",     50, 1, 0, 1, 0, "",   DISPLAYONLY, 0, 0, 0,
};

static form ktub = {1, 0, 0, _ktub, 0, 0, 0, 0, &KtFont};


BOOL LeerPosAuswahl ()
{
        static BOOL CpOK = FALSE;

        if (! CpOK)
        {
            strcpy (ktextarr[0],
                "Unbearbeitete Positionen ignorieren");
            strcpy (ktextarr[1],
                "Unbearbeitete Positionen l�schen");
            strcpy (ktextarr[2],
                "Ist-Menge = Soll-Menge");
        }
        SetHoLines (0);
        syskey = 0;
        if (eWindow)
        {
                EnableWindow (eWindow, FALSE);
                EnableWindow (eFussWindow, FALSE);
                EnableWindow (eKopfWindow, FALSE);
        }
        ktidx = ShowWKopf (eWindow, (char *) ktextarr, 3,
                     (char *) &ktext, 61,
                     &ktform, NULL, &ktub);
        if (eWindow)
        {
                EnableWindow (eWindow, TRUE);
                EnableWindow (eFussWindow, TRUE);
                EnableWindow (eKopfWindow, TRUE);
        }

        SetHoLines (1);
        if (syskey == KEY5 || syskey == KEYESC)
        {
                return FALSE;
        }
        switch (ktidx)
        {
                case 0 :
                    SetAllPosStat ();
                    return TRUE;
                case 1 :
                    DelLeerPos ();
                    return TRUE;
                case 2:
                    SollToIst ();
                    return TRUE;
        }
        return TRUE;
}



BOOL PosLeer (void)
/**
Reaktion auf nicht abgeschlossene Positionen.
**/
{
          EnableWindow (hMainWindow, FALSE);
          EnableWindow (eWindow, FALSE);
          EnableWindow (eFussWindow, FALSE);
          EnableWindow (eKopfWindow, FALSE);
          if (abfragejnG (eWindow,
                          "Es sind nicht bearbeitete Positionen vorhanden.\n"
                          "Abbrechen ?", "J"))
          {
              EnableWindow (hMainWindow, TRUE);
              EnableWindow (eWindow, TRUE);
              EnableWindow (eFussWindow, TRUE);
              EnableWindow (eKopfWindow, TRUE);
              return FALSE;
          }
          EnableWindow (hMainWindow, TRUE);
          EnableWindow (eWindow, TRUE);
          EnableWindow (eFussWindow, TRUE);
          EnableWindow (eKopfWindow, TRUE);
          return LeerPosAuswahl ();
}


BOOL TestPosis (void)
/**
Einzelne Positionen testen.
**/
{
          int i;

          for (i = 0; i < aufpanz_upd; i ++)
          {
              if (atoi (aufparr[i].s) < 3)
              {
                  return PosLeer ();
              }
          }
          return TRUE;
}


BOOL TestPosStatus (void)
/**
Test, ob der Lieferschein auf komplett gesetzt werden darf.
**/
{
          if (NewPosi () == FALSE)
          {
              disp_messG ("Der Lieferschein wird noch von einem\n"
                          "anderen Benutzer bearbeitet", 2);
              return FALSE;
          }

          if (TestPosis () == FALSE)
          {
              return FALSE;
          }
          return TRUE;
}

int LeerGutExist (void)
/**
Test, ob Leergutartikel erfasst wurden.
**/
{
          int i;
          int lganz;

          lganz = 0;
          for (i = 0; i < aufpanz_upd; i ++)
          {
              if (lese_a_bas (ratod (aufparr[i].a)) != 0)
              {
                  continue;
              }
              if (_a_bas.a_typ == 11)
              {
                  lganz += atoi (aufparr[i].lief_me);
              }
          }
          return lganz;
}

BOOL PrintAufkleber (int lganz)
/**
Druck fuer Aufkleber starten.
**/
{
/*
          char buffer [256];
          DWORD ret;

          sprintf (buffer, "%d", lganz);

          EnterCalcBox (eWindow," Anzahl Aufkleber", buffer, 9,
                                   "%4d");

/?
          EnterNumBox (eWindow,
              " Wieviele Aufkleber sollen ausgedruckt werden", buffer, 9,
                                   "%4d");
?/
          lganz = atoi (buffer);
          if (lganz == 0) return TRUE;

          if (strcmp (sys_ben.pers_nam, " ") <= 0)
          {
             strcpy (sys_ben.pers_nam, "fit");
          }
          sprintf (buffer, "rswrun 53800 0 %s %hd %hd %ld %d",
                                 clipped (sys_ben.pers_nam),
                                 lsk.mdn, lsk.fil, lsk.kun, lganz);
          SetAktivWindow (eWindow);

          ret = ProcExec (buffer, SW_SHOWMINIMIZED, -1, 0, -1, 0);
          SetAktivWindow (eWindow);
          if (ret == 0)
          {
              disp_messG ("Fehler beim Start des Etikettendrucks",2);
          }
          else
          {
              disp_messG ("Etikettendruck gestartet", 2);
          }
*/
          return TRUE;
}


BOOL LsLeerGut (void)
/**
Leergut testen.
**/
{
         int lganz;

         if (ls_leer_zwang == 0) return TRUE;
         if (lganz = LeerGutExist ())
         {
             return PrintAufkleber (lganz);
         }
         if (abfragejnG (eWindow, "Es wurde kein Leergutartikel erfasst\n"
                                  "Leergut erfassen", "J"))
         {
// Durch PostMessage wuerde das Fenster zum Erfassen von Artikeln
// geoeffnet.
//             PostMessage (eWindow, WM_KEYDOWN, VK_F6, 0l);
             return FALSE;
         }
         return TRUE;
}


/**
Leergut als Kopf oder Fusstext eingeben.
**/


form *fAuswahlEinh;

struct LP
{
	double lart;
	int    lanz;
} LeerPosArt [LEERMAX];


static BOOL BreakLeerEnter = FALSE;
static HWND lPosWindow;
static HWND hWndLeer;

static int ktpos = 0;
static int ktend = 0;
static TEXTMETRIC tmLP;
static BOOL CrCaret = FALSE;
static char *ztab[] = {" = ", " + "};
static int  zpos = 0;
static BOOL NumIn = FALSE;

static char *meeinhLP[200];
static char *meeinhLPK[200];
static double leer_art [200];

static int meeinhpos  = 0;
static int palettepos = 0;
static int ptanz = 0;
static int ptscroll = 0;
static int ptstart  = 0;
static int ptend    = 0;
static int LeerScrollDown (void);
static int LeerScrollUp (void);

mfont PosLeerFontE = {"Courier New", 200, 0, 1,
                                     RGB (0, 0, 0),
                                     RGB (255, 255, 255),
                                     1,
                                     NULL};

COLORREF LeerBuColor = RGB (0, 255, 255);
COLORREF LeerBuColorPf = RGB (0, 0, 255);


static field _PosLeerE[] = {
  KopfTextFTab[0],          61, 0, 0, 1, 0, "", DISPLAYONLY,        0, 0, 0,
  KopfTextFTab[1],          61, 0, 1, 1, 0, "", DISPLAYONLY,        0, 0, 0,
  KopfTextFTab[2],          61, 0, 2, 1, 0, "", DISPLAYONLY,        0, 0, 0,
  KopfTextFTab[3],          61, 0, 3, 1, 0, "", DISPLAYONLY,        0, 0, 0};

form PosLeerE = {4, 0, 0, _PosLeerE, 0, 0, 0, 0, &PosLeerFontE};

int StopPosLeer (void)
{
	     BreakLeerEnter = TRUE;
		 return (0);
  }


LONG FAR PASCAL PosLeerProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        switch(msg)
        {

             case WM_PAINT :
				    if (hWnd == hWndLeer)
					{
				           display_form (hWnd, &PosLeerE, 0, 0);
					}
                    break;

             case WM_KEYDOWN :
				    if (wParam == VK_F5)
					{
						syskey = KEY5;
						StopPosLeer ();
					}
					break;
             case WM_COMMAND :
                    if (LOWORD (wParam) == KEY5 ||
                        LOWORD (wParam) == VK_F5)
                     {
                            syskey = KEY5;
                            SendKey (VK_F5);
                            break;
                    }
                    if (LOWORD (wParam) == KEYCR)
                    {
                            syskey = KEYCR;
                            SendKey (VK_RETURN);
                            break;
                    }
                    if (LOWORD (wParam) == KEY12)
                    {
                            syskey = KEY12;
                            SendKey (VK_F12);
                            break;
                    }
                    if (LOWORD (wParam) == KEYUP)
                    {
                            syskey = KEYUP;
							if (currentfield > ptstart)
							{
								currentfield --;
							    SetFocus (fAuswahlEinh->mask[currentfield].feldid);
							}
							else
							{
								LeerScrollDown ();
							}
							meeinhpos = ptscroll + currentfield - 1;
                            break;
                    }
                    if (LOWORD (wParam) == KEYDOWN)
                    {
                            syskey = KEYUP;
							if (currentfield < ptend)
							{
								currentfield ++;
							    SetFocus (fAuswahlEinh->mask[currentfield].feldid);
							}
							else
							{
								LeerScrollUp ();
							}
							meeinhpos = ptscroll + currentfield - 1;
                            break;
					}

        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

static void KonvFormParams (void)
/**
Laenge des Eingabefeldes anpassen.
**/
{
	     static BOOL paramsOK = FALSE;
		 HDC hdc;
		 HFONT hFont, oldfont;
		 int i;

		 if (paramsOK) return;

		 paramsOK = TRUE;
         spezfont (&PosLeerFontE);
         hFont = SetWindowFont (hMainWindow);
         hdc = GetDC (hMainWindow);
         oldfont = SelectObject (hdc, hFont);
         GetTextMetrics (hdc, &tmLP);
         DelFont (SelectObject (hdc, oldfont));
         ReleaseDC (hMainWindow, hdc);

		 for (i = 0; i < PosLeerE.fieldanz; i ++)
		 {
			 PosLeerE.mask[i].length = PosLeerE.mask[i].length * (short) tmLP.tmAveCharWidth;
			 if (PosLeerE.mask[i].rows == 0)
			 {
				 PosLeerE.mask[i].rows = (int) (double) ((double) tmLP.tmHeight * 1.3);
			 }
			 else
			 {
				 PosLeerE.mask[i].rows = PosLeerE.mask[i].rows * (short) tmLP.tmHeight;
			 }

			 if (PosLeerE.mask[i].pos[0] != -1)
			 {
				 PosLeerE.mask[i].pos[0] = PosLeerE.mask[i].pos[0] * (short) tmLP.tmHeight;
			 }

			 if (PosLeerE.mask[i].pos[1] != -1)
			 {
				 PosLeerE.mask[i].pos[1] = PosLeerE.mask[i].pos[1] * (short) tmLP.tmAveCharWidth;
			 }
		 }
}

void SetLPCaret (void)
{
	int x,y;
	int CaretH;
	BOOL cret;

	DestroyCaret ();

    CaretH = (int) (double) ((double) tmLP.tmHeight * 1.1);
    SetFocus (PosLeerE.mask[kopftextfpos].feldid);
    cret = CreateCaret (PosLeerE.mask[kopftextfpos].feldid, NULL, 0, CaretH);
    CrCaret = TRUE;

	x = ktpos * tmLP.tmAveCharWidth;
	y = 0;
	cret = ShowCaret (PosLeerE.mask[kopftextfpos].feldid);
	cret = SetCaretPos (x,y);
	ShowCaret (PosLeerE.mask[kopftextfpos].feldid);
}

static void DispWindowText (HWND hWnd, char *text)
/**
Text in Fenster anzeigen.
**/
{
	 InvalidateRect (hWnd, NULL, TRUE);
	 UpdateWindow (hWnd);
}

static void ToKopfFtab (void)
/**
Kopfdelder aus Array in FormFelder uebertragen.
**/
{
      int i, j;

	  while (kopftextfscroll < 0) kopftextfscroll ++;

      for (i = 0, j = kopftextfscroll; i < PosLeerE.fieldanz; i ++, j ++)
	  {
		  strcpy (KopfTextFTab[i], KopfTextTab[j]);
	  }
	  display_form (hWndLeer, &PosLeerE, 0, 0);
}



static void EinhToStr (void)
/**
Einheit an String anhaengen.
**/
{
	char Einheit[20];
	char *MeH;

	if (NumIn == FALSE) return;


    strcpy (Einheit, meeinhLPK[meeinhpos]);
	MeH = strtok (Einheit, " ");
	strcat (KopfTextFTab[kopftextfpos], " ");
    strcat (KopfTextFTab[kopftextfpos], MeH);

	if (zpos == 0)
	{
	        strcat (KopfTextFTab[kopftextfpos], ztab [zpos]);
	        zpos = 2;
	}

	ktend = strlen (KopfTextFTab[kopftextfpos]);
	ktpos = ktend;
    DispWindowText (PosLeerE.mask[kopftextfpos].feldid, NULL);
	SetLPCaret ();
	NumIn = FALSE;
    SetFocus (fAuswahlEinh->mask[currentfield].feldid);
}

void TestKopfText (char *kText)
/**
Beim Verlassen einer Zeile, den Kopfinhalt testen.
**/
{
	 int len;
	 char *gpos;


	 len = strlen (kText);
	 if (kText[len - 1] != '=' &&
		 kText[len - 2] != '=')
	 {
		 return;
	 }

	 gpos = strchr (kText, '=');

	 for (gpos = gpos - 1;  gpos != kText; gpos -=1)
	 {
		 if (*gpos > ' ')
		 {
			 *(gpos + 1) = 0;
			 return;
		 }
	 }
}


void TestKopfTextBef (char *kText)
/**
Vor dem Enter einer Zeile, den Kopfinhalt testen.
**/
{
	 if (strcmp (kText, " ") <= 0) return;

	 if (strchr (kText, '=') == NULL)
	 {
		 strcat (kText, " = ");
	 }
}

void ScrollKopfTextDown (int zeilen)
/**
Kopftext nach unten scrollen.
**/
{

	if (kopftextfscroll > 0)
	{
		kopftextfscroll --;
	}
	ToKopfFtab ();
}


void ScrollKopfTextUp (int zeilen)
/**
Kopftext nach oben scrollen.
**/
{
	if (kopftextfscroll < 95)
	{
		kopftextfscroll ++;
	}
	ToKopfFtab ();
}


void NextKopfZeile (void)
{
	int pos;

	EinhToStr ();
	strcpy (KopfTextTab[kopftextpos], KopfTextFTab [kopftextfpos]);
	TestKopfText (KopfTextTab[kopftextpos]);
	if (kopftextpos > 59) return;

	if (strcmp (KopfTextTab[kopftextpos], " ") <= 0) return;

	kopftextpos ++;
	if (kopftextfpos < PosLeerE.fieldanz - 1)
	{
	          kopftextfpos ++;
	}
	else
	{
              ScrollKopfTextUp (1);
	}
	if (kopftextpos >= kopftextanz) kopftextanz = kopftextpos + 1;
    strcpy (KopfTextFTab[kopftextfpos], KopfTextTab[kopftextpos]);
	clipped (KopfTextFTab[kopftextfpos]);
	TestKopfTextBef (KopfTextFTab[kopftextfpos]);
    DispWindowText (PosLeerE.mask[kopftextfpos].feldid, NULL);
	ktend = strlen (KopfTextFTab[kopftextfpos]);
	strcpy (KopfText,KopfTextFTab[kopftextfpos]);
    ktpos = ktend;
	if (ktend == 0)
	{
 	         zpos = 0;
	}
	else
	{
		if (strchr (KopfText, '+'))
		{
		     zpos = 1;
		}
		else if (strchr (KopfText, '='))
		{
            pos = (int) strlen (KopfText);
			if (KopfText[pos - 1] == '=' ||
				KopfText[pos - 2] == '=')
			{
           	        zpos = 2;
			}
			else
			{
				    zpos = 1;
			}
		}
		else
		{
			zpos = 0;
		}
	}
	strcpy (KopfTextFTab[kopftextfpos], KopfText);
    NumIn = FALSE;
}


void PriorKopfZeile (void)
{
	int pos;

	EinhToStr ();
    GetWindowText (PosLeerE.mask[0].feldid, KopfText, 60);
	strcpy (KopfTextTab[kopftextpos], KopfTextFTab[kopftextfpos]);
	TestKopfText (KopfTextTab[kopftextpos]);
	if (kopftextpos < 1) return;
	kopftextpos --;
	if (kopftextfpos > 0)
	{
	          kopftextfpos --;
	}
	else
	{
              ScrollKopfTextDown (1);
	}
    strcpy (KopfTextFTab[kopftextfpos], KopfTextTab[kopftextpos]);
	strcpy (KopfText,KopfTextFTab[kopftextfpos]);
	clipped (KopfTextFTab[kopftextfpos]);
	TestKopfTextBef (KopfTextFTab[kopftextfpos]);
    DispWindowText (PosLeerE.mask[kopftextfpos].feldid, NULL);
	ktend = strlen (KopfTextFTab[kopftextfpos]);
	strcpy (KopfText,KopfTextFTab[kopftextfpos]);
    ktpos = ktend;
	if (ktend == 0)
	{
 	         zpos = 0;
	}
	else
	{
		if (strchr (KopfText, '+'))
		{
		     zpos = 1;
		}
		else if (strchr (KopfText, '='))
		{
            pos = (int) strlen (KopfText);
			if (KopfText[pos - 1] == '=' ||
				KopfText[pos - 2] == '=')
			{
           	        zpos = 2;
			}
			else
			{
				    zpos = 1;
			}
		}
		else
		{
			zpos = 0;
		}
	}
	strcpy (KopfTextFTab[kopftextfpos], KopfText);
    NumIn = FALSE;
}


void SetThisCaret(void)
{
	int pos;

	if (ktend == 0)
	{
 	         zpos = 0;
	}
	else
	{
		if (strchr (KopfText, '+'))
		{
		     zpos = 1;
		}
		else if (strchr (KopfText, '='))
		{
            pos = (int) strlen (KopfText);
			if (KopfText[pos - 1] == '=' ||
				KopfText[pos - 2] == '=')
			{
           	        zpos = 2;
			}
			else
			{
				    zpos = 1;
			}
		}
		else
		{
			zpos = 0;
		}
	}
    NumIn = FALSE;
	ktend = strlen (KopfText);
    ktpos = ktend;
	SetLPCaret ();
}


int ProcPosLeer (WPARAM wParam)
/**
Aktion auf Tasten in EnterNum ausfuehren.
**/
{
	      if (wParam >= (WPARAM) '0' &&
			  wParam <= (WPARAM) '9')
		  {
              	   if (NumIn == FALSE && zpos == 2)
				   {
	                      zpos = 1;
				   }
	               else if (NumIn == FALSE && zpos == 1)
				   {
	                      strcat (KopfTextFTab[kopftextfpos], ztab [zpos]);
						  ktpos = strlen (KopfTextFTab[kopftextfpos]);
				   }

			       KopfTextFTab[kopftextfpos][ktpos] = (char) wParam;
				   if (ktpos < 59) ktpos ++;
				   if (ktend < ktpos) ktend = ktpos;
			       KopfTextFTab[kopftextfpos][ktend] = (char) 0;
				   DispWindowText (PosLeerE.mask[kopftextfpos].feldid, NULL);
                   SetFocus (eNumWindow);
	               NumIn = TRUE;
		  }
		  else if (wParam == (WPARAM) ' ')
		  {
			       EinhToStr ();
		  }

		  else if (wParam == (WPARAM) '-')
		  {
				   if (strstr (MinusText,"Ver") > 0)
				   {
					NextStep = NEUE_VERPACKUNGEN;
				   }
				   else
				   {
					NextStep = NEUES_LEERGUT;
				   }

				   if (NextStep == NEUE_VERPACKUNGEN || NextStep == NEUES_LEERGUT)
				   {
					syskey = KEY5;
  					PostMessage (NULL, WM_USER, 0, 0);
					PostMessage (NULL, WM_KEYDOWN,
								(WPARAM) VK_F5, 0l);
					SetFocus (eNumWindow);
				   }
		  }
		  else if (wParam == VK_F5)
		  {
			       syskey = KEY5;
  		           PostMessage (NULL, WM_USER, 0, 0);
                   PostMessage (NULL, WM_KEYDOWN,
                               (WPARAM) VK_F5, 0l);
                   SetFocus (eNumWindow);
		  }
		  else if (wParam == VK_F5)
		  {
			       syskey = KEY5;
  		           PostMessage (NULL, WM_USER, 0, 0);
                   PostMessage (NULL, WM_KEYDOWN,
                               (WPARAM) VK_F5, 0l);
                   SetFocus (eNumWindow);
		  }
		  else if (wParam == (WPARAM) '=')
		  {
			       EinhToStr ();
                   SetFocus (eNumWindow);
		  }
		  else if (wParam == (WPARAM) VK_RETURN)
		  {
			       EinhToStr ();
//                 SetFocus (eNumWindow);
			       syskey = KEYCR;
  		           PostMessage (NULL, WM_USER, 0, 0);
                   PostMessage (NULL, WM_KEYDOWN,
                               (WPARAM) VK_RETURN, 0l);
                   SetFocus (eNumWindow);
		  }
		  else if (wParam == VK_CLS)
		  {
 		          zpos = 0;
		          NumIn = FALSE;
                  memset (KopfTextFTab[kopftextfpos], ' ', 60);
                  KopfTextFTab[kopftextfpos] [0] = (char) 0;
				  ktpos = ktend = 0;
			      DispWindowText (PosLeerE.mask[kopftextpos].feldid, NULL);
                  SetFocus (eNumWindow);
		  }

		  else if (wParam == VK_DOWN)
		  {
			      NextKopfZeile ();
                  SetFocus (eNumWindow);
		  }
		  else if (wParam == VK_UP)
		  {
			      PriorKopfZeile ();
                  SetFocus (eNumWindow);
		  }

          SetLPCaret ();

          SetFocus (fAuswahlEinh->mask[currentfield].feldid);
		  return 1;
}

static int MeProc (HWND hWnd)
/**
Mengeneinheit wechseln.
**/
{
		  meeinhpos ++;
		  if (meeinhLP[meeinhpos] == NULL)
		  {
			  meeinhpos = 0;
		  }
          TextForm.mask[0].feld = meeinhLP[meeinhpos];
		  InvalidateRect (hWnd, NULL, TRUE);
		  return 0;
}

static void strzcpy (char *dest, char *source, int len)
/**
source in dest zentrieren.
**/
{
	    int zpos;

		memset (dest, ' ', len);
		dest[len] = (char) 0;
		if ((int) strlen (source) > len - 1)
		{
			source[len - 1] = (char) 0;
		}
		zpos = (int) (double) ((double) (len - strlen (source)) / 2 + 0.5);
		zpos = max (0, zpos);
		memcpy (&dest[zpos], source, strlen (source));
}

void FillEinhLP (void )
/**
Einheiten aus Prueftabelle lesen.
**/
{
		int dsqlstatus;

		if (EinhLPOK) return;

		EinhLPOK = TRUE;
		EinhVPOK = FALSE;

		ptanz = 0;
		palettepos = ptanz;
        dsqlstatus = ptab_class.lese_ptab_all ("me_einh_leer");
        while (dsqlstatus == 0)
        {
			if ((meeinhLP[ptanz] = (char *) malloc (40)) == 0)
			{
				break;
			}
			if ((meeinhLPK[ptanz] = (char *) malloc (10)) == 0)
			{
				break;
			}
            strzcpy (meeinhLP[ptanz],  clipped (ptabn.ptbez), 16);
            strcpy  (meeinhLPK[ptanz], clipped (ptabn.ptbezk));
			leer_art[ptanz] = ratod (ptabn.ptwer1);
			if (strupcmp (ptabn.ptbez, "PALETTE", 7) == 0)
			{
				palettepos = ptanz;
			}
            ptanz ++;
            if (ptanz == 99) break;
            dsqlstatus = ptab_class.lese_ptab_all ();
        }
		meeinhLP[ptanz]  = NULL;
 	    meeinhLPK[ptanz] = NULL;
		ptscroll = 0;
}

void FillEinhVP (short typ)
/**
Einheiten aus Prueftabelle lesen.
**/
{
	static int vpk_cursor;
		int dsqlstatus, i;

		if (typ == VERPACKUNGEN && EinhVPOK) return;
		if (typ == LEERGUT && EinhLPOK) return;

		if (typ == VERPACKUNGEN) EinhVPOK = TRUE;
		if (typ == VERPACKUNGEN) EinhLPOK = FALSE;

		if (typ == LEERGUT) EinhVPOK = FALSE;
		if (typ == LEERGUT) EinhLPOK = TRUE;

		for (i = 0; i < fAuswahlEinh->fieldanz; i ++)
		{
			if (meeinhLP[i] == NULL) break;
			meeinhLP[i] = NULL;
		}


		ptanz = 0;
		palettepos = ptanz;
		if (typ == VERPACKUNGEN) a_tara.a = ratod(aufps.a);
		if (typ == LEERGUT) a_tara.a = 0.0;
              DbClass.sqlout ((double *) &a_tara.a_gew, 3, 0);
              DbClass.sqlout ((double *) &a_tara.a_krz, 0, 17);
              DbClass.sqlout  ((double *) &a_tara.a_vpk, 3, 0);
              DbClass.sqlin  ((double *) &a_tara.a, 3, 0);
		vpk_cursor = DbClass.sqlcursor ("select a_gew,a_krz,a_vpk from a_tara where a = ?  ");

		dsqlstatus = DbClass.sqlfetch (vpk_cursor);
        while (dsqlstatus == 0)
        {
			if ((meeinhLP[ptanz] = (char *) malloc (40)) == 0)
			{
				break;
			}
			if ((meeinhLPK[ptanz] = (char *) malloc (10)) == 0)
			{
				break;
			}
            strzcpy (meeinhLP[ptanz],  clipped (a_tara.a_krz), 16);
            strcpy  (meeinhLPK[ptanz], clipped (a_tara.a_krz));
			leer_art[ptanz] = a_tara.a_vpk;
            ptanz ++;
            if (ptanz == 199) break;
			dsqlstatus = DbClass.sqlfetch (vpk_cursor);
        }
		if (ptanz == 0)
		{
			if ((meeinhLP[ptanz] = (char *) malloc (40)) == 0)
			{
				return;
			}
			if ((meeinhLPK[ptanz] = (char *) malloc (10)) == 0)
			{
				return;
			}
            strzcpy (meeinhLP[ptanz],  "kein Eintrag", 16);
            strcpy  (meeinhLPK[ptanz], "kein Eintrag");
			leer_art[ptanz] = 0.0;
			ptanz ++;
		}
		meeinhLP[ptanz]  = NULL;
 	    meeinhLPK[ptanz] = NULL;
		ptscroll = 0;
}

void LeerBuScrollDown (int start, int end, int ze)
{
	    int i;
		ColButton *ColBut1, *ColBut2;

        while (start - ze + 1 < 0) ze --;
		for (i = end; i > start; i --)
		{
			ColBut1 = (ColButton *) fAuswahlEinh->mask[i].feld;
			ColBut2 = (ColButton *) fAuswahlEinh->mask[i - ze].feld;
			ColBut1->text1 = ColBut2->text1;
		}
		ColBut1 = (ColButton *) fAuswahlEinh->mask[i].feld;
		ColBut1->text1 = "";
}


int GetMeEinhPos (char *text)
{
	    int i;

		for (i = 0; i < ptanz; i ++)
		{
			if (strcmp (text, meeinhLP[i]) == 0)
			{
				return i;
			}
		}
		return meeinhpos;
}


int SetLeerText (void)
{
		ColButton *ColBut;

	    ColBut = (ColButton *) fAuswahlEinh->mask[currentfield].feld;
		meeinhpos = GetMeEinhPos (ColBut->text1);
		EinhToStr ();
		return 0;
}

int LeerScrollUp (void)
{
        int i, j;
		int scrollanz;
		int scrollplus;
		ColButton *ColBut;

		scrollanz  = fAuswahlEinh->fieldanz - 2;
		scrollplus = fAuswahlEinh->fieldanz - 3;

		scrollplus = max (0, scrollplus);
        while (scrollplus > 0)
		{
			if ((ptscroll + scrollplus + scrollanz) <= ptanz)
			{
				break;
			}
			scrollplus --;
		}

		ptscroll += scrollplus;
		for (i = ptscroll, j = 1; j <= scrollanz; i ++, j ++)
		{
			ColBut = (ColButton *) fAuswahlEinh->mask[j].feld;
			ColBut->text1 = meeinhLP[i];
		}
		display_form (NuBlock.GeteNumWindow (), fAuswahlEinh, 0, 0);
		return 0;
}

int LeerScrollDown (void)
{
        int i, j;
		int scrollanz;
		int scrollplus;
		ColButton *ColBut;

		scrollanz  = fAuswahlEinh->fieldanz - 2;
		scrollplus = fAuswahlEinh->fieldanz - 3;

		scrollplus = max (0, scrollplus);
        while (scrollplus > 0)
		{
			if ((ptscroll - scrollplus) >= 0)
			{
				break;
			}
			scrollplus --;
		}


		ptscroll -= scrollplus;
		for (i = ptscroll, j = 1; j <= scrollanz; i ++, j ++)
		{
			ColBut = (ColButton *) fAuswahlEinh->mask[j].feld;
			ColBut->text1 = meeinhLP[i];
		}
		display_form (NuBlock.GeteNumWindow (), fAuswahlEinh, 0, 0);
		return 0;
}


void SetLeerBuColors (void)
{
	    int i;
		ColButton *ColBut;

		for (i = 0; i < fAuswahlEinh->fieldanz; i ++)
		{
			ColBut = (ColButton *) fAuswahlEinh->mask[i].feld;
			ColBut->BkColor = LeerBuColor;
		}
}

void SetLeerBuTxt (void)
{
	    int i;
		ColButton *ColBut;

		currentfield = 0;
		ptstart = 0;
		ptend = fAuswahlEinh->fieldanz - 1;
        if (pfeilu == NULL)
		{
                  pfeilu = LoadBitmap (hMainInst, "pfeilu");
		}
        if (pfeilo == NULL)
		{
                  pfeilo = LoadBitmap (hMainInst, "pfeilo");
		}

		for (i = 0; i < fAuswahlEinh->fieldanz; i ++)
		{
			ColBut = (ColButton *) fAuswahlEinh->mask[i].feld;
			ColBut->text1 = "";
			fAuswahlEinh->mask[i].after = SetLeerText;
		}

		for (i = 0; i < fAuswahlEinh->fieldanz; i ++)
		{
			if (meeinhLP[i] == NULL) break;
			ColBut = (ColButton *) fAuswahlEinh->mask[i].feld;
			ColBut->text1 = meeinhLP[i];
			fAuswahlEinh->mask[i].after = SetLeerText;
		}
		if (ptanz >= fAuswahlEinh->fieldanz)
		{
			i = fAuswahlEinh->fieldanz - 1;
			ColBut = (ColButton *) fAuswahlEinh->mask[i].feld;
			ColBut->text1 = "";
			ColBut->bmp   = pfeilu;
			ColBut->BkColor = LeerBuColorPf;
			ColBut->aktivate = 14;
			fAuswahlEinh->mask[i].after = LeerScrollUp;
 		    currentfield = 1;
			ptstart = 1;
			ptend = fAuswahlEinh->fieldanz - 2;

		}
        LeerBuScrollDown (0, fAuswahlEinh->fieldanz - 2, 1);
		ColBut = (ColButton *) fAuswahlEinh->mask[0].feld;
		ColBut->text1 = "";
		ColBut->bmp   = pfeilo;
		ColBut->BkColor = LeerBuColorPf;
		ColBut->aktivate = 14;
		fAuswahlEinh->mask[0].after = LeerScrollDown;
}

void EnterLeerPos (short verp_typ)
/**
Leergut auf Positionsebene einegen.
**/
{
	    int x,y, cx, cy;
		HFONT hFont;
        HDC hdc;
		TEXTMETRIC tm;
		int i;
		int pos;
		HWND ParentWindow;

        fAuswahlEinh = NuBlock.GetAuswahlEinh ();
		if (GebTara)
		{
		        ParentWindow = WiegWindow;
		}
		else
		{
		        ParentWindow = eWindow;
		}
//		if (verp_typ == LEERGUT) FillEinhLP ();
//		if (verp_typ == VERPACKUNGEN) FillEinhVP ();
		FillEinhVP (verp_typ);
		/**
		if (ptanz == 0)
		{
			if (verp_typ == LEERGUT)
			{
				disp_messG ("Keine Pr�ftabelleneintr�ge f�r Leergut", 2);
			}
			else
			{
				disp_messG ("Keine Verpackungseintr�ge gefunden", 2);
			}
		    return;
		}
		**/
		RECT eRect, fRect;

 	    save_fkt (4);
		save_fkt (5);
		save_fkt (6);
		save_fkt (7);
		save_fkt (8);
		save_fkt (9);
		save_fkt (10);
		save_fkt (11);

		set_fkt (NULL, 4);
		set_fkt (NULL, 5);
		set_fkt (NULL, 6);
		set_fkt (NULL, 7);
		set_fkt (NULL, 8);
		set_fkt (NULL, 9);
		set_fkt (NULL, 10);
		set_fkt (NULL, 11);

		set_fkt (StopPosLeer, 5);

		NuBlock.SetNum3 (TRUE);

        if (eWindow)
        {
		      GetWindowRect (eWindow,     &eRect);
		      GetWindowRect (eFussWindow, &fRect);
        }
        else
        {
		      GetWindowRect (LogoWindow,  &eRect);
		      GetWindowRect (BackWindow3, &fRect);
        }


		EnableWindows (ParentWindow, FALSE);
		EnableWindows (BackWindow3, FALSE);
		EnableWindows (BackWindow2, FALSE);
        x  = eRect.left;
		y  = eRect.top;
		cx = eRect.right - eRect.left;
		cy = eRect.bottom - eRect.top + fRect.bottom - fRect.top;

        PosLeerFontE.FontHeight = 200;
        while (TRUE)
        {
                 spezfont (&PosLeerFontE);
                 hFont = SetWindowFont (hMainWindow);
                 hdc = GetDC (hMainWindow);
                 SelectObject (hdc, hFont);
                 GetTextMetrics (hdc, &tm);
                 DelFont (hFont);
                 ReleaseDC (hMainWindow, hdc);

                 if ((tm.tmAveCharWidth * 60) < cx) break;
                 PosLeerFontE.FontHeight -= 5;
                 if (PosLeerFontE.FontHeight <= 80) break;
        }
        KonvFormParams ();
        spezfont (&PosLeerFontE);
        hFont = SetWindowFont (hMainWindow);
        hdc = GetDC (hMainWindow);
        SelectObject (hdc, hFont);
        GetTextMetrics (hdc, &tm);
        for (i = 1; i < PosLeerE.fieldanz; i ++)
		{
			PosLeerE.mask[i].pos[0] = (int) (double)
				((double) PosLeerE.mask[i - 1].pos[0] + tm.tmHeight * 1.3);
		}

        lPosWindow = CreateWindow ("PosLeer",
                                    "",
                                    WS_VISIBLE | WS_POPUP | WS_DLGFRAME,
                                    x, y,
                                    cx, cy,
                                    ParentWindow,
                                    NULL,
                                    hMainInst,
                                    NULL);
		if (lPosWindow == NULL)
		{
  		    EnableWindows (ParentWindow, TRUE);
		    EnableWindows (BackWindow3,  TRUE);
		    EnableWindows (BackWindow2,  TRUE);
			return;
		}

		x = (cx - 62 * tm.tmAveCharWidth) / 2;
		y = (int) (double) ((double) 1 * tm.tmHeight * 1.3);
		cx = 62 * tm.tmAveCharWidth;
		cy = (int) (double) ((double) 4 * tm.tmHeight * 1.3);

		hWndLeer = CreateWindowEx (WS_EX_CLIENTEDGE,
			                       "PosLeerKopf",
								   "",
                                    WS_VISIBLE | WS_CHILD,
                                    x, y,
                                    cx, cy,
                                    lPosWindow,
                                    NULL,
                                    hMainInst,
                                    NULL);

		NuBlock.SetParent (lPosWindow);
		NuBlock.SetNumZent (TRUE, 0, (int) (double) ((double) 30 * scrfy));
//		FillEinhLP ();
		SetLeerBuColors ();
		SetLeerBuTxt ();
		ktpos = ktend = 0;
		meeinhpos = palettepos;
		kopftextpos = 0;
		zpos = 0;
		NumIn = FALSE;
		CrCaret = FALSE;
		KopfText[ktend] = (char) 0;
        create_enter_form (hWndLeer, &PosLeerE, 0, 0);

        NuBlock.SetEform (&PosLeerE, lPosWindow, ProcPosLeer);
        NuBlock.SetMeProc (MeProc);

        ReadKopftext ();
        strcpy (KopfTextFTab[0], KopfTextTab[0]);
        strcpy (KopfText, KopfTextTab[0]);
	    TestKopfTextBef (KopfTextFTab[0]);
   	    ktend = strlen (KopfTextFTab[0]);

        ktpos = ktend;
	    strcpy (KopfText,KopfTextFTab[0]);
	    if (ktend == 0)
		{
 	         zpos = 0;
		}
	    else
		{
		     if (strchr (KopfText, '+'))
			 {
		            zpos = 1;
			 }
		     else if (strchr (KopfText, '='))
			 {
                    pos = (int) strlen (KopfText);
			        if (KopfText[pos - 1] == '=' ||
				        KopfText[pos - 2] == '=')
					{
           	                  zpos = 2;
					}
			        else
					{
				              zpos = 1;
					}
			 }
		     else
			 {
			        zpos = 0;
			 }
		}

	    strcpy (KopfTextFTab[0], KopfText);
        NumIn = FALSE;

        DispWindowText (PosLeerE.mask[0].feldid, KopfTextFTab[0]);
        NuBlock.SetNumCaretProc (SetThisCaret);

        NuBlock.EnterCalcBox (lPosWindow, meeinhLP[0], "", 40, "", EntNuProc);
	    strcpy (KopfTextTab[kopftextpos], KopfTextFTab[kopftextfpos]);
	    TestKopfText (KopfTextTab[kopftextpos]);

		SetActiveWindow (lPosWindow);

		set_fkt (StopPosLeer, 5);

		EnableWindows (ParentWindow, TRUE);
		EnableWindows (BackWindow3, TRUE);
		EnableWindows (BackWindow2, TRUE);

		DestroyWindow (hWndLeer);
		DestroyWindow (lPosWindow);

		DestroyCaret ();
		SetActiveWindow (ParentWindow);
        ActivateColButton (BackWindow3, &MainButton2, 3, 0, 1);
 	    restore_fkt (4);
		restore_fkt (5);
		restore_fkt (6);
		restore_fkt (7);
		restore_fkt (8);
		restore_fkt (9);
		restore_fkt (10);
		restore_fkt (11);
}


void ReadKopftext (void)
/**
Kopftext lesen.
**/
{
		int zei;
		int i;


		for (i = 0; i < PosLeerE.fieldanz; i ++)
		{
		          memset (KopfTextFTab[i], ' ', 60);
				  KopfTextFTab[i][60] = 0;
	 		      clipped (KopfTextFTab[i]);
		}
		kopftextfpos = kopftextfscroll = 0;


		for (i = 0; i < LEERMAX; i ++)
		{
		          memset (KopfTextTab[i], ' ', 60);
				  KopfTextTab[i][60] = 0;
	 		      clipped (KopfTextTab[i]);
		}

		kopftextanz = zei = 0;

/*
		if (lsk.kopf_txt == 0l)
		{
		           ToKopfFtab ();
				   return;
		}
*/
		if (GebTara) return;

}


static void InitLeerPosArt (void)
/**
Tabelle mit Leergutartikel initialisieren.
**/
{
	    int i;

		for (i = 0; i < LEERMAX; i ++)
		{
			LeerPosArt[i].lart = (double) 0.0;
			LeerPosArt[i].lanz = 0;
		}
}

static void SetLeerPosArt (double art, int anz)
/**
Tabelle mit Leergutartikel initialisieren.
**/
{
	    int i;

		if (anz == 0) return;
		if (art == (double) 0.0) return;

		for (i = 0; LeerPosArt[i].lanz; i ++)
		{
			if (i == LEERMAX) return;
			if (LeerPosArt[i].lart == art)
			{
				break;
			}
		}

		LeerPosArt[i].lart  = art;
		LeerPosArt[i].lanz += anz;
}

static double GetLeerArt (char *LeerBez)
/**
Artikel-Nummer zu Leergutbezeichnung holen.
**/
{
	    int i;

		for (i = 0; i < ptanz; i ++)
		{
			if (strcmp (meeinhLPK[i], LeerBez) == 0)
			{
				return leer_art[i];
			}
		}
		return (double) 0.0;
}


static BOOL GetNextEinh (char *LeerZeile, int *anz, char *LeerBez, BOOL mode)
/**
Nexte Leergutbezeichnung und Leergutanzahl aus Zeile holen.
**/
{
	     static char *pos = NULL;
		 static char *LZ = NULL;
		 char anzstr [10];
		 int i;

		 if (LZ != LeerZeile)
		 {
			 pos = LeerZeile;
		 }
		 if (mode == FALSE)
		 {
			 pos = LeerZeile;
		 }

		 for (; *pos; pos += 1)
		 {
			 if (*pos == '+');
			 else if (*pos == '=');
			 else if (*pos > ' ')
			 {
				 break;
			 }
		 }

         if (*pos == 0)
		 {
			  *anz = 0;
              LeerBez[0] = 0;
			  return FALSE;
		 }

		 i = 0;
		 for (; *pos; pos += 1)
		 {
			 if (*pos < '0' || *pos > '9')
			 {
				 break;
			 }
			 anzstr[i] = *pos;
			 i ++;
		 }

         if (*pos == 0)
		 {
			  *anz = 0;
              LeerBez[0] = 0;
			  return FALSE;
		 }

		 LZ = LeerZeile;
         anzstr[i] = 0;
		 *anz = atoi (anzstr);
		 for (; *pos; pos += 1)
		 {
			 if (*pos == '+');
			 else if (*pos == '=');
			 else if (*pos > ' ')
			 {
				 break;
			 }
		 }

         if (*pos == 0)
		 {
              LeerBez[0] = 0;
			  return FALSE;
		 }


		 i = 0;
		 for (; *pos; pos += 1)
		 {
			 if (*pos <= ' ')
			 {
				 break;
			 }
             LeerBez[i] = *pos;
			 i ++;
		 }
		 LeerBez[i] = 0;
		 return TRUE;
}

static BOOL IsLeerGutZeile (char *Zeile)
/**
Test, ob die Zeile eine Leergutzeile ist.
**/
{
	     int i, j;
		 int anz;

	     for (i = 0; i < ptanz; i ++)
		 {
			 if (strstr (Zeile, meeinhLPK[i]) == NULL)
			 {
				 break;
			 }
		 }
		 if (i == ptanz) return FALSE;

		 anz = wsplit (Zeile, " ");
		 if (anz == 0) return FALSE;

		 for (i = 0; i < anz; i ++)
		 {
			 if (strcmp (wort[i], "+") == 0) continue;
			 if (strcmp (wort[i], "=") == 0)continue;
			 if (numeric (wort[i])) continue;
  	         for (j = 0; j < ptanz; j ++)
			 {
			        if (strcmp (wort[i], meeinhLPK[j]) == 0)
					{
				              break;
					}
			 }
			 if (j == ptanz) return FALSE;
		 }
		 return TRUE;
}

static BOOL IsLeerGut (double a)
/**
Artikeltyp pruefen.
**/
{
	     int dsqlstatus;

		 if (a <= (double) 0.0) return FALSE;
		 dsqlstatus = lese_a_bas (a);
		 if (dsqlstatus == 100) return FALSE;
		 //140110 nicht nur Leergut, sondern auch Verpack. und H�llen !!
         if (_a_bas.a_typ != 11 && _a_bas.a_typ != 9 && _a_bas.a_typ != 8) return FALSE;
		 return TRUE;
}


void WriteLeerZeileArt (char *LeerZeile)
/**
Eine Zeile fuer Leegutartikel bearbeiten.
**/
{
	     double art;
		 int anz;
		 char Bez[20];

		 if (IsLeerGutZeile(LeerZeile) == FALSE) return;

		 if (GetNextEinh (LeerZeile, &anz, Bez, 0) == FALSE) return;

		 art = GetLeerArt (Bez);
		 if (IsLeerGut (art))
		 {
			 SetLeerPosArt (art, anz);
		 }

		 while (GetNextEinh (LeerZeile, &anz, Bez, 1))
		 {
           		 art = GetLeerArt (Bez);
		         if (IsLeerGut (art))
				 {
				         SetLeerPosArt (art, anz);
				 }
		 }
}

static BOOL IsinLeerPos (double art)
/**
Test, ob Leergutartikel aud Positionen im Kopftext erfasst wurden.
**/
{
        int i;

		for (i = 0; LeerPosArt[i].lanz; i ++)
		{
			if (i == LEERMAX) return FALSE;
			if (LeerPosArt[i].lart == art)
			{
				return TRUE;
			}
		}
		return FALSE;
}


double GetGebGew (char *LeerZeile,short typ)
/**
Eine Zeile fuer Leegutartikel bearbeiten.
**/
{
	     double art;
		 int anz;
		 char Bez[20];
		 double gew = 0.0;

//		 if (IsLeerGutZeile(LeerZeile) == FALSE) return 0.0;

		 if (GetNextEinh (LeerZeile, &anz, Bez, 0) == FALSE) return 0.0;

		 art = GetLeerArt (Bez);
		 if (IsLeerGut (art))
		 {
			 if (typ == VERPACKUNGEN) a_tara.a = ratod(aufps.a);
			 if (typ == LEERGUT) a_tara.a = 0.0;
			 a_tara.a_vpk = art;
			 if (a_taraClass.dbreadfirst() == 0)
			 {
				 gew += a_tara.a_gew * anz;
			 }
			 else
			 {
				gew += _a_bas.a_gew * anz;
			 }
			 gebindetara.gebinde1 = art;
			 gebindetara.anz1 = anz;

		 }

		 while (GetNextEinh (LeerZeile, &anz, Bez, 1))
		 {
           		 art = GetLeerArt (Bez);
		         if (IsLeerGut (art))
				 {
					 if (a_taraClass.dbreadfirst() == 0)
					{
						gew += a_tara.a_gew * anz;
					}
					else
					{
						gew += _a_bas.a_gew * anz;
					}
						 if (gebindetara.anz1 == 0)
						 {
							 gebindetara.gebinde1 = art;
							 gebindetara.anz1 = anz;
						 }
						 else if (gebindetara.anz2 == 0)
						 {
							 gebindetara.gebinde2 = art;
							 gebindetara.anz2 = anz;
						 }
						 else if (gebindetara.anz3 == 0)
						 {
							 gebindetara.gebinde3 = art;
							 gebindetara.anz3 = anz;
						 }
						 else if (gebindetara.anz4 == 0)
						 {
							 gebindetara.gebinde4 = art;
							 gebindetara.anz4 = anz;
						 }
						 else if (gebindetara.anz5 == 0)
						 {
							 gebindetara.gebinde5 = art;
							 gebindetara.anz5 = anz;
						 }
						 else if (gebindetara.anz6 == 0)
						 {
							 gebindetara.gebinde6 = art;
							 gebindetara.anz6 = anz;
						 }
				 }
		 }
		 return gew;
}


void GetGebGewicht (char *ftara,short typ)
/**
Gewicht fuer Gebinde holen.
**/
{
	     int i;
		 double gew = 0.0;

 		 if (kopftextanz == 0)
		 {
			kopftextanz ++;
		 }
		 InitLeerPosArt ();
		 for (i = 0; i < kopftextanz; i ++)
		 {
			 gew += GetGebGew (KopfTextTab[i],typ);
		 }
		 sprintf (ftara, "%7.3lf", gew);
}


static double ean_a = 0l;
static double ean = 0l;


int ScanArtikel (double a)
{
        Text Ean;
        Text EanDb;
        Text EanA;
        STRUCTEAN *EanStruct;
		static short cursor_ean = -1;
        double ean = 0l;

		writelog ("Scanartikel %.0lf\n", a);
        scangew = 0l;
        Ean.Format ("%13.0lf", a);
		if (Ean.GetLength () >= 12)
		{
			EanStruct = Scanean.GetStructean (Ean);
		}
		else
		{
			EanStruct = NULL;
		}
		writelog ("EanStruct %ld\n", EanStruct);
		char *sc;
        if (EanStruct != NULL)
        {
            switch (Scanean.GetType (Ean))
            {
            case EANGEW :
				if (Ean.GetLength () < 13)
				{
					break;
				}
                EanDb = Scanean.GetEanForDb (Ean);
                ean = ratod (EanDb.GetBuffer ());
				sc = Scanean.GetEanGew (Ean);
				if (sc != NULL)
				{
					scangew = atol (Scanean.GetEanGew (Ean));
				}
				else
				{
					EanStruct = NULL;
				}
                break;
            case EANPR :
                EanDb = Scanean.GetEanForDb (Ean);
                ean = ratod (EanDb.GetBuffer ());
                BreakScan = TRUE;
                break;
            default :
                ean = ratod (Ean.GetBuffer ());
                BreakScan = TRUE;
/*
                if (ScanKunEan (ean) == 0)
                {
                    return 0;
                }
*/
            }
        }
/*
        else
        {
                ean = ratod (Ean.GetBuffer ());
				writelog ("ScanEan : EAN %.0lf", ean);
                BreakScan = TRUE;
                if (ScanKunEan (ean) == 0)
                {
				    writelog ("ScanKunEan OK gefunden: a %.0lf", _a_bas.a);
                    return 0;
                }
			    writelog ("ScanKunEan OK nicht gefunden");
        }
*/

        if (cursor_ean == -1)
		{
              DbClass.sqlout ((double *) &ean_a, 3, 0);
              DbClass.sqlin  ((double *) &ean, 3, 0);
              cursor_ean = DbClass.sqlcursor ("select a from a_ean where ean = ?");
		}
        ean = ratod (Ean.GetBuffer ());
        writelog ("ScanEan: EAN %.0lf", ean);
		DbClass.sqlopen (cursor_ean);
		dsqlstatus = DbClass.sqlfetch (cursor_ean);

        if (dsqlstatus == 0)
        {
			     writelog ("ScanEan OK gefunden: a %.0lf", _a_bas.a);
                 dsqlstatus = lese_a_bas (ean_a);
        }
        if (dsqlstatus == 0)
        {
                 return dsqlstatus;
        }
        if (EanStruct != NULL && EanStruct->GetAFromEan () == TRUE)
//            ((EanStruct->GetType ().GetBuffer ())[0] == 'G' ||
//            (EanStruct->GetType ().GetBuffer ())[0] != 'P'))
        {
                 EanA = Ean.SubString (2, EanStruct->GetLen ());
                 a = ratod (EanA.GetBuffer ());
                 dsqlstatus = lese_a_bas (a);
        }
		else //FISCH-5
		{
                 dsqlstatus = lese_a_bas (ean);
		}
        return dsqlstatus;
}


BOOL TestArtikel (double a)
{

        dsqlstatus = ScanArtikel (a);
        if (dsqlstatus != 0)
        {
                     disp_messG ("Falsche Artikelnummer", 2);
                     return FALSE;
        }
        return TRUE;
}

int BreakEanEnter ()
{
	  break_enter ();
	  return 0;
}

void SetScannFocus ()
{
	  if (fScanform.mask[0].feldid != NULL)
	  {
		  SetFocus (fScanform.mask[0].feldid);
	  }
}


void EnterEan (char *buffer)
{

       save_fkt (5);
       save_fkt (12);
       set_fkt (BreakEanEnter, 5);
       set_fkt (BreakEanEnter, 12);
	   
	   strcpy (eanbuffer, "");
	   SetEnterCharChangeProc (ChangeChar);
       enter_form (eFussWindow, &fScanform, 0, 0);
       CloseControls (&fScanform);
	   SetEnterCharChangeProc (NULL);
	   if (syskey != KEY5)
	   {
		   strcpy (buffer, eanbuffer);
	   }
       restore_fkt (5);
       restore_fkt (12);
}


BOOL MeOK ()
{
// Vorl�ufig kein Abbruch, wenn die Menge erreicht ist.
	return FALSE;

	if (atoi (aufps.s) > 2) return TRUE;
	if (ratod (aufps.auf_me) == 0) return FALSE;

	if (ScanValues->auf_me >= ratod (aufps.auf_me))
	{
		return TRUE;
	}
	return FALSE;
}

int TestNewArt (double a, long posi)
/**
Test, ob der Artikel bei diesem Lieferschein existiert.
**/
{
	     int i;
     for (i = 0; i < aufpanz; i ++)
	 {
		 if (a == ratod (aufparr[i].a) && posi == -1)
		 {
			 break;
		 }
		 else if (a == ratod (aufparr[i].a) &&
			      posi == atol (aufparr[i].pnr) )
		 {
				  break;
		 }
	 }
	 if (i == aufpanz) return -1;
	 return (i);
}


int TestNewArtCharge (double a, Text& ListCharge)
/**
Test, ob der Artikel bei diesem Lieferschein existiert.
**/
{
	     int i;
		Text Charge;
	     for (i = 0; i < aufpanz; i ++)
		 {
			 Charge = aufparr[i].ls_charge;
			 if (atoi(EinAusgang) == PRODUKTIONSFORTSCHRITT || atoi(EinAusgang) == BUCHENFERTIGLAGER ) Charge = aufparr[i].partie; 
			 Charge.Trim ();
			 if (a == ratod (aufparr[i].a) && ListCharge == Charge)
			 {
					  break;
			 }
			 if (a == ratod (aufparr[i].a) && aufparr[i].s[0] < '3' && Charge == "")
			 {
					  break;
			 }
		 }
		 if (i == aufpanz) return -1;
		 return (i);
}

BOOL TestScanArticleExist ()
{
	int art_pos;
	art_pos = TestNewArtCharge (ScanValues->a, ScanValues->Charge);

		aufpidx = aufpanz;
		aufpanz ++;
		sprintf (aufps.auf_me, "%.3lf", 0.0);
		sprintf (aufps.lief_me, "%.3lf", 0.0);
		sprintf (aufps.pnr, "%ld", (aufpidx + 1) * 10);
		lsp.posi = (aufpidx + 1) * 10;
		if (atoi(EinAusgang) == PRODUKTIONSSTART) strcpy (aufps.ls_charge, ScanValues->Charge.GetBuffer ());
		if (atoi(EinAusgang) == PRODUKTIONSSTART) sprintf (aufps.lief_me, "%.3lf",  ScanValues->eangew);
					
		memcpy (&aufparr[aufpidx], &aufps, sizeof (struct AUFP_S));
         arrtozerl_p (aufpidx);
         beginwork ();
         ZerlpClass.dbupdate (atoi (EinAusgang));
		 //=== TODO hier noch Bestandsbuchung
         commitwork ();
         LeseWePos ();
         SetListPos (aufpidx);

		ScanValues->auf_me = 0.0;
		SetListPos (GetListPos () + 1);
		InvalidateRect (eWindow, NULL, FALSE);
		return FALSE;
	return TRUE;
}

BOOL CompareCharge ()
{
	int art_pos;

	Text Charge = aufps.ls_charge;
	Charge.Trim ();

    art_pos = TestNewArtCharge (ScanValues->a, ScanValues->Charge);

//	if (art_pos != -1 && art_pos != aufpidx)
	if (art_pos != -1)
	{
		aufpidx = art_pos;
		memcpy (&aufps, &aufparr[aufpidx],sizeof (struct AUFP_S)); 
		if (ScanValues->Charge.GetBuffer () != "")
		{
			if (atoi(EinAusgang) == PRODUKTIONSFORTSCHRITT  || atoi(EinAusgang) == BUCHENFERTIGLAGER) strcpy (aufps.partie, ScanValues->Charge.GetBuffer ()); 
			if (atoi(EinAusgang) == PRODUKTIONSFORTSCHRITT  || atoi(EinAusgang) == BUCHENFERTIGLAGER) aufps.pcharge = ratod (ScanValues->Charge.GetBuffer ()); 
			if (atoi(EinAusgang) == PRODUKTIONSSTART) 
			{
				if (strlen (clipped(aufps.ls_charge)) == 0)
				{
					strcpy (aufps.ls_charge, ScanValues->Charge.GetBuffer ());
				}
				else
				{
					if (strcmp (aufps.ls_charge,ScanValues->Charge.GetBuffer ()) != 0)
					{
						strcpy (aufps.ls_charge, "mehrere");
					}
				}
			}
		}
		return TRUE;
	}


	double auf_me_ges = ratod (aufps.auf_me);

	if (atoi(EinAusgang) == PRODUKTIONSFORTSCHRITT  || atoi(EinAusgang) == BUCHENFERTIGLAGER)
	{
		return FALSE;
	}
	else
	{
		aufpidx = aufpanz;
		aufpanz ++;
		strcpy (aufps.s, "2");
		sprintf (aufps.lief_me, "%.3lf", 0.0);
		sprintf (aufps.pnr, "%ld", (aufpidx + 1) * 10);
		lsp.posi = (aufpidx + 1) * 10;
		strcpy (aufps.ls_charge, ScanValues->Charge.GetBuffer ());
		memcpy (&aufparr[aufpidx], &aufps, sizeof (struct AUFP_S));
			arrtozerl_p (aufpidx);
			beginwork ();
			ZerlpClass.dbupdate (atoi (EinAusgang));
			//=== TODO hier noch Bestandsbuchung
			commitwork ();
			LeseWePos ();
			SetListPos (aufpidx);
		ScanValues->auf_me = 0.0;
		SetListPos (GetListPos () + 1);
		InvalidateRect (eWindow, NULL, FALSE);
	}
	return TRUE;
}



BOOL MultiScan ()
{
	char buffer [512];

	ScanValues = new CScanValues;
//	ScanValues->auf_me = ratod (aufps.auf_me); 
	if (ScanValues->auf_me < 0.0) ScanValues->auf_me = 0.0;

    while (!MeOK ())
	{
		while (!ScanValues->ScanOK ())
		{
			ScanValues->Init ();  //testtesttest
			strcpy (buffer, "");
			if (SmallScann == FALSE)
			{
		       NuBlock.SetCharChangeProc (ChangeChar);
               NuBlock.EnterNumBox (eWindow,"  Scannen  ",
                                    buffer, -1,
                                    "", EntNuProc);
			}
			else
			{
			 EnterEan (buffer);
			}

			if (syskey == KEY5) return FALSE;


			// =====================  2. Scann    (nur Chargennummer)   A
			if (atoi (EinAusgang) == PRODUKTIONSSTART && ratod(aufps.a) > (double) 0 && strlen (clipped(aufps.ls_charge)) == 0 )  
			{
			  /*****
				 Text Tbuffer = buffer;
				 ScanEan128Ai10 (Tbuffer);
				 if (Tbuffer.GetLen () > 0)
				 {
					ScanMessage.OK ();
					beginwork ();
					sprintf (aufps.ls_charge,"%s", ScanValues->Charge.GetBuffer ());
					write_wewiepro (ScanValues->eangew, ScanValues->eangew, 0.0);
					strcpy (aufps.s, "3");
					if (atoi (EinAusgang) == PRODUKTIONSFORTSCHRITT)  aufps.status = -1;  //FISCH-5 tempor�r auf -1 setzten wg, Sortierung
					memcpy (&aufparr[aufpidx], &aufps, sizeof (struct AUFP_S));
					arrtozerl_p (aufpidx);

					zerl_p.lgr_ab = LagerAb;
					zerl_p.lgr_zu = LagerZu;
		//			BucheBsd (1,ScanValues->gew,zerl_p.charge);  //FISCH-5 
			  
					BucheBsd (1,ScanValues->gew,ScanValues->Charge.GetBuffer ());  //FISCH-5     Vorsicht, hier wird noch ohne Charge in BuchBsd gebucht 
					ZerlpClass.dbupdate (atoi (EinAusgang));
					ScanValues->Init ();
					commitwork ();
					LeseWePos ();
					ScanMessage.Complete ();

					delete ScanValues;
					ScanValues = NULL;
					return TRUE;
				 }
				 ********/
			}
			//=====================  2. Scann    (Cahrgennummer)   E


			if (strlen (buffer) > 14)
			{
				Text Ean = buffer;
				ScanEan128 (Ean);
			}
			else
			{
				if (aufps.status == -1 && atoi(EinAusgang) == PRODUKTIONSFORTSCHRITT) 
				{
					ScanMessage.OK ();
					BOOL bf = UpdateFortschritt (atoi(buffer));
					ScanMessage.Complete ();
					return bf;
				}

				if (TestArtikel (ratod (buffer)) == FALSE)
				{
					continue;
				}
				ScanValues->SetA (_a_bas.a);
				double gew = (double) ((double) scangew / 1000.0);
				if (artikelOK (_a_bas.a, TRUE) == FALSE)
				{
					continue;
				}
				ScanValues->SetGew (gew);
				break;
			}
		}

			//Hier: =====================  2. Scann    (Chargennummer)   A
			if (atoi (EinAusgang) == PRODUKTIONSSTART && ScanValues->gew == 0.0 )  //Hier: Gewichtseingabe
			{
				EnterGewicht ();
			}
			if (atoi (EinAusgang) == PRODUKTIONSSTART && ScanValues->GetChargeLen () == 0 )  //Hier: 2. Scann: nur Charge scannen 
			{
				EnterCharge ();
			}
			//Hier: Produktionsfortschritt setzten
			if (atoi (EinAusgang) == PRODUKTIONSFORTSCHRITT )
			{
				AuswahlFortschritt ();
			}

		ScanValues->SetACharge ();
		BOOL bCompare = FALSE;
		if (atoi(EinAusgang) == PRODUKTIONSSTART || atoi(EinAusgang) == PRODUKTIONSFORTSCHRITT ||  atoi(EinAusgang) == BUCHENFERTIGLAGER )
		{
			if (atoi(EinAusgang) == PRODUKTIONSSTART )
			{
				//Hier: Fanggebiet testen
				int dfanggebiet = HoleFanggebiet(ScanValues->Charge.GetBuffer());
				if (dfanggebiet == 0) 
				{
					disp_messG ("Die Charge konnte nicht gefunden werden ", 2);
					bCompare = FALSE;
				}
				else if (dfanggebiet != zerl_k.fanggebiet)
				{   
					if (zerl_k.fanggebiet == 0) 
					{
						zerl_k.fanggebiet = dfanggebiet;
						ZerlkClass.dbupdate ();
			              sprintf (ptabn.ptwert, "%hd",zerl_k.fanggebiet);
						  ptab_class.lese_ptab ("fanggebiet", ptabn.ptwert);
						strcpy (fanggebiet, ptabn.ptbez);
						sprintf(Standorttxt,"Fanggebiet :%s",fanggebiet);

						/**** funktioniert beides nicht warum ? 
						lKopfform.mask[2].feld = Standorttxt;
				        SetWindowText (lKopfform.mask [2].feldid,
					       lKopfform.mask [2].feld);

						display_field (eKopfWindow, &lKopfform.mask[2]);
						***/


						bCompare = CompareCharge ();
					}
					else
					{
						disp_messG ("Das Fanggebiet stimmt nicht �berein ", 2);
						bCompare = FALSE;
					}
				}
				else
				{
					bCompare = CompareCharge ();
				}
			} else bCompare = CompareCharge ();
		}
		else
		{
			if (TestScanArticleExist ())
			{
				bCompare = CompareCharge ();
			}
		}
		ScanValues->IncAufMe ();
		ScanMessage.OK ();
		if (bCompare)
		{
			if (atoi(EinAusgang) != PRODUKTIONSFORTSCHRITT)
			{
				if (_a_bas.me_einh != 2 && _a_bas.me_einh != 3 && _a_bas.me_einh != 4)
				{
					if (_a_bas.a_gew != 0)
					{
						ScanValues->eangew /= _a_bas.a_gew;
						ScanValues->eangew += 0.499; // ab der 1. Dezimalstelle auf ganze Zahlen aufrunden
					}
					sprintf (aufps.lief_me, "%.0lf", ratod (aufps.lief_me) + ScanValues->eangew);
				}
				else
				{
					sprintf (aufps.lief_me, "%.3lf", ratod (aufps.lief_me) + ScanValues->eangew);
				}
			}
			if (mhd_kz == 2)
			{
				Ean128Date = ScanValues->Mhd;
				stringcopy (aufps.hbk_date, Ean128Date.ToString ().GetBuffer (),sizeof (aufps.hbk_date));
			}

			beginwork ();
			sprintf (aufps.auf_me,"%.3lf", ScanValues->auf_me);

//Hier:Wiepro Scannen Schreiben, nur wenn schon Charge vorhanden ist
			if (ScanValues->Charge.GetLen () > 0 )  write_wewiepro (ScanValues->eangew, ScanValues->eangew, 0.0);   
			strcpy (aufps.s, "3");
//			if (atoi (EinAusgang) == PRODUKTIONSFORTSCHRITT)  aufps.status = -1;  //FISCH-5 tempor�r auf -1 setzten wg, Sortierung
			memcpy (&aufparr[aufpidx], &aufps, sizeof (struct AUFP_S));
			arrtozerl_p (aufpidx);

			zerl_p.lgr_ab = LagerAb;
			zerl_p.lgr_zu = LagerZu;
//			BucheBsd (1,ScanValues->gew,zerl_p.charge);  //FISCH-5 
			  
//Hier:BuchBsd ScannenSchreiben, nur wenn schon Charge vorhanden ist 
			if (ScanValues->Charge.GetLen () > 0 )  BucheBsd (1,ScanValues->gew,ScanValues->Charge.GetBuffer ());  //FISCH-5   
			ZerlpClass.dbupdate (atoi (EinAusgang));
			ScanValues->Init ();
			commitwork ();
			LeseWePos ();
		}
		else
		{
			ScanValues->Init ();
			LeseWePos ();
		}

         SetListPos (aufpidx);
	    ShowNewElist ((char *) aufparr,
                               aufpanz,
                               (int) sizeof (struct AUFP_S));
		memcpy (&aufps, &aufparr[aufpidx], sizeof (struct AUFP_S));
		if (aufpanz == 1)
		{
//FISCH-21             ActivateColButton (BackWindow3,  &MainButton2, 2, 0, 1);
			ActivateColButton (eFussWindow, &lFussform,   0, 0, 1);
			ActivateColButton (eFussWindow, &lFussform,   1, 0, 1);
			ActivateColButton (eFussWindow, &lFussform,   2, 0, 1);
			ActivateColButton (eFussWindow, &lFussform,   3, 0, 1);
		}

	}
	ScanMessage.Complete ();

    delete ScanValues;
	ScanValues = NULL;
	return TRUE;
}


BOOL ScanMultiA ()
{

        struct AUFP_S aufps_save, aufps_save0;
        int idxsave;

        beginwork ();

        aufpidx = GetListPos ();


        memcpy (&aufps_save, &aufps,sizeof (struct AUFP_S));
        idxsave = aufpidx;
        aufpidx = 0;

        memcpy (&aufps_save0, &aufparr[aufpidx],sizeof (struct AUFP_S));


        if (Lock_Lsp () == FALSE)
        {
            commitwork ();
            return FALSE;
        }

        memcpy (&aufparr[aufpidx], &aufps, sizeof (struct AUFP_S));
        strcpy (auf_me, aufps.rest);
        akt_buch_me = ratod (aufps.lief_me);
        memcpy (&aufps, &aufparr[aufpidx], sizeof (struct AUFP_S));

		if (!MultiScan ()) 
        {
            commitwork ();

            return FALSE;
        }

        memcpy (&aufparr[aufpidx], &aufps, sizeof (struct AUFP_S));
         arrtozerl_p (aufpidx);
         beginwork ();
         ZerlpClass.dbupdate (atoi (EinAusgang));
		 //=== TODO hier noch Bestandsbuchung
         commitwork ();
         LeseWePos ();
         SetListPos (aufpidx);

        aufpidx = idxsave;
        ShowNewElist ((char *) aufparr,
                               aufpanz,
                               (int) sizeof (struct AUFP_S));

			 if (aufpanz == 1)
			 {
			    ActivateColButton (BackWindow3,  &MainButton2, 2, 0, 1);
				ActivateColButton (eFussWindow, &lFussform,   0, 0, 1);
				ActivateColButton (eFussWindow, &lFussform,   1, 0, 1);
				ActivateColButton (eFussWindow, &lFussform,   2, 0, 1);
				ActivateColButton (eFussWindow, &lFussform,   3, 0, 1);
			}

        return TRUE;
}

 //============ END TODO MultiScan
int ScanA (void)
/**
Eine Zeile in Liste einfuegen.
**/
{
//	     KEINHEIT keinheit;
         struct AUFP_S aufp;
         char buffer [256];
//         char me_buffer [20];
         int pos;
         long akt_posi;
//		 double auf_me auf_me_vgl;

		 // ===== TODO MultiScan
 		 if (ScanMulti)
		 {
			 return ScanMultiA ();
		 }

        scangew = 0l;
         ActivateColButton (BackWindow3, &MainButton2, 3, -1, 1);
         ActivateColButton (BackWindow3, &MainButton2, 0, -1, 1);
         memcpy (&aufp, &aufps, sizeof (struct AUFP_S));
         memcpy (&aufps, &aufp_null, sizeof (struct AUFP_S));

         while (TRUE)
         {
       		       EnableWindow (InsWindow, FALSE);
//       		       EnableWindow (BackWindow3, FALSE);

		           if (SmallScann)
				   {
 			             EnterEan (buffer);
				   }
				   else if (Scanlen != 14)
				   {
                         strcpy (buffer, "");
                         EnterNumBox (eWindow,
                                      "  Artikel  ", buffer, Scanlen,
                                                    "");
				   }
				   else
				   {
                         strcpy (buffer, "0");
                         EnterNumBox (eWindow,
                                      "  Artikel  ", buffer, 14,
                                                    "%13.0f");
				   }
//				   EnableWindow (BackWindow3, TRUE);
                   InvalidateRect (eWindow, NULL, TRUE);
                   UpdateWindow (eWindow);
                   memcpy (&aufps, &aufp_null, sizeof (struct AUFP_S));


                   if ((strlen (buffer) <= 13) && ((double) ratod (buffer) <= (double) 0.0))
                   {
                              memcpy (&aufps, &aufp, sizeof (struct AUFP_S));
                              DeleteInsert ();
                              return 0;
                   }
                   if (syskey == KEY5)
                   {
                              memcpy (&aufps, &aufp, sizeof (struct AUFP_S));
                              DeleteInsert ();
                              return 0;
                   }
                   OpenInsert ();
				   clipped (buffer);
				   if (strlen (buffer) >=16)
				   {
					          Text Ean = buffer;
							  int ret = ScanEan128 (Ean);
							  if (ret == 0)
							  {
								  break;
							  }
				   }
                   else if (artikelOK (buffer, TRUE))
                   {
                              break;
                   }
                   memcpy (&aufps, &aufp, sizeof (struct AUFP_S));
                   DeleteInsert ();
                   ActivateColButton (BackWindow3, &MainButton2, 3, 0, 1);
				   ActivateColButton (BackWindow3, &MainButton2, 0, 0, 1);
                   return 0;
         }

         if (scangew != 0l)
         {
            sprintf (aufps.lief_me, "%.3lf", (double) scangew / 1000);
         }
		 if (hbk_direct)
		 {
		           strcpy (aufps.hbk_date, "");

// Default-Halbarkeits-KZ = 'T' Tage  07.08.2002

/*
                   if (_a_bas.hbk_kz[0] <= ' ')
                   {
                           strcpy (_a_bas.hbk_kz, "T");
                   }
*/

		           if (_a_bas.hbk_kz[0] > ' ')
				   {
                           sysdate (buffer);
                           EnterNumBox (eWindow,
                                " Haltbarkeitsdatum ", buffer, 11,
                                   "dd.mm.yyyy");
                           InvalidateRect (eWindow, NULL, TRUE);
                           UpdateWindow (eWindow);
                           if (syskey != KEY5)
						   {
					          strcpy (aufps.hbk_date,buffer);
						   }
				   }
         }



         current_form = &testform;
         DeleteInsert ();
         InvalidateRect (eWindow, NULL, TRUE);
         UpdateWindow (eWindow);
         pos = GetListPos () + 1;
         akt_posi = GetNextPosi (pos);
         sprintf (aufps.pnr, "%ld", akt_posi);
         ScrollAufp (pos);
		 strcpy (aufps.ls_charge, "");
         memcpy (&aufparr [pos], &aufps, sizeof (struct AUFP_S));
         arrtozerl_p (pos);
         beginwork ();
         ZerlpClass.dbupdate (atoi (EinAusgang));
         commitwork ();
         LeseWePos ();
         SetListPos (pos);
         ShowNewElist ((char *) aufparr,
                                aufpanz,
                                (int) sizeof (struct AUFP_S));
		 if (aufpanz == 1)
		 {
            ActivateColButton (BackWindow3,  &MainButton2, 2, 0, 1);
            ActivateColButton (eFussWindow, &lFussform,   0, 0, 1);
            ActivateColButton (eFussWindow, &lFussform,   1, 0, 1);
            ActivateColButton (eFussWindow, &lFussform,   2, 0, 1);
            ActivateColButton (eFussWindow, &lFussform,   3, 0, 1);
		 }
         memcpy (&aufps, &aufparr [pos], sizeof (struct AUFP_S));
         CheckHbk ();
		 if (wieg_neu_direct)
		 {
                      IsDblClck (pos);
		 }
         return 0;
}


int DoScann ()
/**
Scannen f�r Artikel starten.
**/
{
        unsigned int scancount = 0; 
		int i;

		if (ScanMode)
		{
			return 0;
		}


        InScanning = TRUE;

  //      EnableWindow (eFussWindow, FALSE);
		for (i = 0; i < lFussform.fieldanz; i ++)
		{
                 ActivateColButton (eFussWindow, &lFussform, i, -1, 1);  
        }
		for (i = 0; i < MainButton.fieldanz; i ++)
		{
                 ActivateColButton (BackWindow2, &MainButton, i, -1, 1);
		}

        EnableWindow (BackWindow2, FALSE);
        if (eWindow != NULL)
        {
            EnableWindow (eWindow, FALSE);
        }
		for (i = 2; i < MainButton2.fieldanz; i ++)
		{
			ActivateColButton (BackWindow3, &MainButton2, i, -1, 1); 
		}

        BreakScan = FALSE;
        syskey = 0;
        while (TRUE)
        {
               BOOL ret = ScanA ();
               if (ret)
               {
                   scancount ++;
               }
               if (syskey == KEY5 || BreakScan)
               {
                   break;
               }
/*
               if (ret && eti_ez != NULL)
               {
                    PrintEti ();
               }
*/
/*
		       if (autocursor && scancount >= (unsigned int) lsp.auf_me)
               {
                   if (aufidx < (int) GetListAnz () - 1)
                   {
                         scancount = 0;
                         SetListPos (GetListPos () + 1);
                         InvalidateRect (eWindow, NULL, FALSE);
                   }
                   else
                   {
                         break;
                   }
               }
*/
        }
        syskey = 0; 
        EnableWindow (eFussWindow, TRUE);
		for (i = 0; i < lFussform.fieldanz; i ++)
		{
               if (atoi(EinAusgang) == PRODUKTIONSSTART)  ActivateColButton (eFussWindow, &lFussform, i, 1, 1);
        }
        ActivateColButton (eFussWindow, &lFussform, 0, 1, 1);  //Neu-Button
        ActivateColButton (eFussWindow, &lFussform, 2, 1, 1);  //L�sch-Button
        ActivateColButton (eFussWindow, &lFussform, 5, 1, 1);  //Scann-Button


		for (i = 0; i < MainButton.fieldanz; i ++)
		{
                 ActivateColButton (BackWindow2, &MainButton, i, 1, 1);
		}


		PROD_MainButtons_inaktiv ();
        ActivateColButton (BackWindow3, &MainButton2, 3, 0, 1);
        EnableWindow (BackWindow2, TRUE);
        if (eWindow != NULL)
        {
            EnableWindow (eWindow, TRUE);
        }
		for (i = 0; i < MainButton2.fieldanz; i ++)
		{
			ActivateColButton (BackWindow3, &MainButton2, i, 1, 1); //FISCH-5 Etikettenbutton
		}
        InScanning = FALSE;
        return 0;
}




int ScanEan128Ai01 (Text& Ean128)
{
        double ean1 = 0.0;

		if (ScanValues == NULL) return -1;

		if (ScanValues->aflag) return 0;

		Text *ean = Scanean.GetEan128Ai (Text ("01"), Ean128);
		if (ean == NULL)
		{

				ScanValues->Init (); //220712
				return 100;
		}

		Text Ean = ean->SubString (1, Ean128Eanlen);
		delete ean;
		double a = ratod (Ean.GetBuffer ());
		ean1 = a;
		if (artikelOK (a, TRUE) == FALSE)
		{
			ScanValues->Init (); //220712
			return 100;
		}
		ScanValues->SetA (_a_bas.a);
		return 0;
}

int ScanEan128Ai31 (Text& Ean128)
{
        double gew = 0.0;

		if (ScanValues == NULL) return -1;

		if (ScanValues->gewflag) return 0;

		Text *Gew = Scanean.GetEan128Ai (Text ("3103"), Ean128);
		if (Gew != NULL)
		{

				gew = ratod (Gew->GetBuffer ()) / 1000.0;
				delete Gew;
				ScanValues->SetGew (gew);
				return 0;
		}

		Gew = Scanean.GetEan128Ai (Text ("3102"), Ean128);
		if (Gew != NULL)
		{

				gew = ratod (Gew->GetBuffer ()) / 100.0;
				delete Gew;
				ScanValues->SetGew (gew);
				return 0;
		}

		Gew = Scanean.GetEan128Ai (Text ("3101"), Ean128);
		if (Gew != NULL)
		{

				gew = ratod (Gew->GetBuffer ()) / 10.0;
				delete Gew;
				ScanValues->SetGew (gew);
				return 0;
		}

// St�ck ohne Nachkomma wird im Moment nicht benutzt

		Gew = Scanean.GetEan128Ai (Text ("30"), Ean128);
		if (Gew != NULL)
		{

				gew = ratod (Gew->GetBuffer ());
				delete Gew;
				ScanValues->SetGew (gew);
				return 0;
		}

		Gew = Scanean.GetEan128Ai (Text ("37"), Ean128);
		if (Gew != NULL)
		{

				gew = ratod (Gew->GetBuffer ());
				delete Gew;
				ScanValues->SetGew (gew);
				return 0;
		}

		return 100;
}


int ScanEan128Ai15 (Text& Ean128)
{

		if (ScanValues == NULL) return -1;

		if (ScanValues->MhdFlag) return 0;
		Text Mhd = "";
		Text *mhd = Scanean.GetEan128Ai (Text ("15"), Ean128);
		if (mhd == NULL)
		{
			return 100;
		}
		Mhd = *mhd;
		delete mhd;
		ScanValues->SetMhd (Mhd);
        return 0;
}


int ScanEan128Ai10 (Text& Ean128)
{

		if (ScanValues == NULL) return -1;
		if (ScanValues->ChargeFlag) return 0;
		Text Charge = "";
		if (_a_bas.charg_hand == 0 && atoi (EinAusgang) != PRODUKTIONSFORTSCHRITT) 
		{
			ScanValues->SetCharge (Charge);
			return 0;
		}
		Text *charge = Scanean.GetEan128Ai (Text ("10"), Ean128);

		if (charge != NULL)
		{
				Charge = *charge;
				delete charge;
				ScanValues->SetCharge (Charge);
				return 0;
		}
        return 0;
}


int ScanEan128MultiAi (Text& Ean128)
{
	int dret;
	dret = ScanEan128Ai01 (Ean128);
	if (dret == 0)  //220712  Nur wenn Artikel erkannt wurde weitermachen !!!
	{
		ScanEan128Ai31 (Ean128);
		ScanEan128Ai10 (Ean128);
	    if (mhd_kz == 2) //040811
		{
			ScanEan128Ai15 (Ean128);
		}
    }

     return 0; 
}



int ScanEan128Ai (Text& Ean128, BOOL DispMode=TRUE)
{
        if (ScanMulti)
		{
			return ScanEan128MultiAi (Ean128);
		}

        double ean1 = 0.0;
		double Gew;


		Text *ean = Scanean.GetEan128Ai (Text ("01"), Ean128);
		if (ean == NULL)
		{

		        disp_messG ("Fehler bei Ean128 : Eancode", 2);
				return 100;
		}

		Text Ean = ean->SubString (1, Ean128Eanlen);
		delete ean;
		double a = ratod (Ean.GetBuffer ());
		ean1 = a;
		if (artikelOK (a, FALSE) == FALSE)
		{
			return 100;
		}

		sprintf (aufps.a, "%.0lf", _a_bas.a);
		BreakScan = FALSE;

		if (_a_bas.hnd_gew[0] == 'G')
		{
			Text *gew = Scanean.GetEan128Ai (Text ("3103"), Ean128);
			if (gew == NULL)
			{
				/*** wenn dies drin ist , schmierts auf dem tolle-Rechner ab !! l�st sich nicht starten
				delete gew;
				Text *gew = Scanean.GetEan128Ai (Text ("3102"), Ean128); //Detlef 031208 
				if (gew == NULL)
				{
					Gew = ratod (gew->GetBuffer ());
					delete gew;
				}
				*********/

//		        disp_messG ("Fehler bei Ean128 : keine Gewichtauszeichnung", 2);
//				return 100;
			}
			else
			{
				Gew = ratod (gew->GetBuffer ());
				delete gew;
			}
		}
		else
		{
			Text *gew = Scanean.GetEan128Ai (Text ("30"), Ean128);
			if (gew == NULL)
			{
				gew = Scanean.GetEan128Ai (Text ("37"), Ean128);
			}
			if (gew == NULL)
			{
//		        disp_messG ("Fehler bei Ean128 : keine St�ckauszeichnung", 2);
//				return 100;
			}
			else
			{
				Gew = ratod (gew->GetBuffer ()) * 1000.0;
				delete gew;
			}
		}
		Text Mhd = "";
		Text *mhd = Scanean.GetEan128Ai (Text ("15"), Ean128);
//		if (mhd == NULL)
		if (mhd != NULL)
		{
				Text Mhd = *mhd;
				Ean128Date = Mhd;
				strcpy (aufps.hbk_date, Ean128Date.ToString ().GetBuffer ());
				delete mhd;
		}
		else
		{
			Ean128Date = Mhd;
			strcpy (aufps.hbk_date, Ean128Date.ToString ().GetBuffer ());
		}
		Text Charge = "";
		Text *charge = Scanean.GetEan128Ai (Text ("10"), Ean128);

		if (ChargeZwang && _a_bas.charg_hand == 1 && charge == NULL)
		{
		        disp_messG ("Fehler bei Ean128 : Charge", 2);
				return 100;
		}

		if (charge != NULL)
		{
					
			    if (_a_bas.charg_hand == 9)
				{
					strcpy (aufps.ident_nr, charge->GetBuffer ());
				}
				else
				{
					Charge = *charge;
					if (atoi(EinAusgang) == PRODUKTIONSFORTSCHRITT) strcpy (aufps.partie, ScanValues->Charge.GetBuffer ()); 
					if (atoi(EinAusgang) == PRODUKTIONSFORTSCHRITT) aufps.pcharge, ratod (ScanValues->Charge.GetBuffer ()); 
					strcpy (aufps.ls_charge, Charge.GetBuffer ());
				}
		}
		else
		{
			    if (_a_bas.charg_hand == 9)
				{
					strcpy (aufps.ident_nr, "");
				}
				else
				{
					strcpy (aufps.ls_charge, "");
				}
		}
		sprintf (aufps.a, "%.0lf", _a_bas.a);
		sprintf (aufps.s, "%hd", 2);
		scangew = (long) (Gew);
		sprintf (aufps.me_einh, "%hd", 2);
		memcpy (&ptabn, &ptabn_null, sizeof (struct PTABN));
		if (ptab_class.lese_ptab ("me_einh", aufps.me_einh) == 0)
		{
                  strcpy (aufps.lief_me_bz, ptabn.ptbezk);
		}
        return 0;
}


int ScanEan128Charge (Text& Ean128, BOOL Ident)
{
		Text Charge = "";
		Text *charge = Scanean.GetEan128Ai (Text ("10"), Ean128);

/*
		if (ChargeZwang && _a_bas.charg_hand == 1 && charge == NULL)
		{
		        disp_messG ("Fehler bei Ean128 : Charge", 2);
				return 100;
		}
*/

		if (charge != NULL)
		{
				Charge = *charge;
				if (Charge.GetLength () > 20)
				{
					  Charge = Charge.SubString (0, 20);
				}
			    if (_a_bas.charg_hand == 9)
				{
					strcpy (aufps.ident_nr, Charge.GetBuffer ());
					Charge = "";
				}
				else
				{
		    		if (atoi(EinAusgang) == PRODUKTIONSFORTSCHRITT) strcpy (aufps.partie, ScanValues->Charge.GetBuffer ()); 
					if (atoi(EinAusgang) == PRODUKTIONSFORTSCHRITT) aufps.pcharge, ratod (ScanValues->Charge.GetBuffer ()); 
					strcpy (aufps.ls_charge, Charge.GetBuffer ());
					Charge = ""; //110908
				}
		}
		else
		{
			    if (_a_bas.charg_hand == 9 || Ident)
				{
					strcpy (aufps.ident_nr, Ean128.GetBuffer ()); //290709  
				}
				else
				{
					strcpy (aufps.ls_charge, Ean128.GetBuffer ());
				}
		}
//		sprintf (aufps.a, "%.0lf", _a_bas.a);
//		sprintf (aufps.s, "%hd", 2);
        return 0;
}



int ScanEan128 (Text& Ean128, BOOL DispMode)
{
/*
	if (Text (Ean128.SubString (0, 2)) == "00")
	{
		return ScanEan128Nve (Ean128);
	}
*/
	return ScanEan128Ai (Ean128, DispMode);
}

// RoW 12.11.2007 Ean128 mit Endezeichen

int ChangeChar (int key)
{

	CInteger Key (key);
	Scanean.AiInfos.FirstPosition ();
    AiInfo *ai;
	while ((ai = (AiInfo *) Scanean.AiInfos.GetNext ()) != NULL)
	{
		ai->EndChar.FirstPosition ();
		CInteger *ec;
		while ((ec = (CInteger *) ai->EndChar.GetNext ()) != NULL)
		{
			if (Key == *ec)
			{
				return ai->EndCharEdit;
			}
		}
	}
	return key;
}



long GenWepTxt0 (void)
/**
Nummer fuer Lieferschein-Positionstexte generieren.
**/
{
	int count; 
	long nr;

	nr = 0l;
    nr = AutoNr.GetNr (nr);
	count = 0;
	while (nr == 0l)
	{
		         Sleep (10);
		         count ++;
				 if (count == 10) break;
                 nr = AutoNr.GetNr (nr);
	}
	return nr;
}

BOOL TxtNrExist (long nr0)
{
	   int dsqlstatus;
	   static int cursor = -1;
	   static long nr;
	   
	   if (cursor == -1)
	   {
			DbClass.sqlin ((long *)  &nr, 2, 0);
			cursor = DbClass.sqlcursor ("select nr from we_txt where nr = ?");
	        if (cursor == -1) return FALSE;
	   }
	   nr = nr0;
	   DbClass.sqlopen (cursor);
	   dsqlstatus = DbClass.sqlfetch (cursor);
	   if (dsqlstatus != 0)
	   {
		    return FALSE;
	   }
	   return TRUE;
}


long GenWepTxt (void)
/**
Nummer fuer Lieferschein-Positionstexte generieren.
**/
{
	 long nr;

	 while (TRUE)
	 {
		 nr = GenWepTxt0 ();
		 if (TxtNrExist (nr) == FALSE) break;
		 if (nr > 99999999) 
		 {
			 return 0l;
		 }
	 }
	 return nr;
}


void FromPosTxt ()
{
	TEXTBLOCK::Rows.Init ();
	we_txt.nr = atol (aufps.lsp_txt);
	int dsqlstatus = cWept.dbreadfirst ();
	while (dsqlstatus == 0)
	{
		Text *txt = new Text (we_txt.txt);
		TEXTBLOCK::Rows.Add (txt);
	    dsqlstatus = cWept.dbread ();
	}
}


void ToPosTxt ()
{
	static int del_cursor = -1;
    int aufidx;
	
	beginwork ();
	if (del_cursor == -1)
	{
		DbClass.sqlin ((long *) &we_txt.nr, 2, 0);
		del_cursor = DbClass.sqlcursor ("delete from we_txt where nr = ?");
	}
    aufidx = GetListPos ();
    memcpy (&aufps, &aufparr [aufidx], sizeof (struct AUF_S));
	if (we_txt.nr == 0)
	{
       int wetxt = GenLText ();
	   if (wetxt == 0l)
	   {
		   return;
	   }
	   we_txt.nr = wetxt;
	   sprintf (aufps.lsp_txt, "%ld", we_txt.nr); 
       memcpy (&aufparr [aufidx], &aufps, sizeof (struct AUFP_S));
	}

	int anz = TEXTBLOCK::Rows.GetCount ();
	for (int i = anz - 1; i >= 0; i --)
	{
		Text *txt = (Text *) TEXTBLOCK::Rows.Get (i);
		txt->Trim ();
		if (*txt != "")
		{
			break;
		}
	}

	DbClass.sqlexecute (del_cursor);
	anz = i + 1;
	for (i = 0; i < anz; i ++)
	{

		Text *txt = (Text *) TEXTBLOCK::Rows.Get (i);
		we_txt.zei = (i + 1) * 10;
		strncpy (we_txt.txt, txt->GetBuffer (), 60);
		we_txt.txt[60] = 0;
        cWept.dbupdate ();
	}
    arrtozerl_p (aufidx);
    ZerlpClass.dbupdate (atoi (EinAusgang));
	commitwork ();
}



int PosTexte ()
{
            textblock = new TEXTBLOCK;
			textblock->SetLongText (TRUE);
	        FromPosTxt ();
			EnableWindows (hMainWindow, FALSE);
			EnableWindow (eWindow, FALSE);
			textblock->MainInstance (hMainInst, eWindow);
            textblock->ScreenParam (scrfx, scrfy);
			textblock->SetTopMost (TRUE);
            textblock->EnterTextBox (BackWindow1, "Pos-Texte", "", 0, "", EntNumProc);
			EnableWindows (hMainWindow, TRUE);
			EnableWindow (eWindow, TRUE);

			InvalidateRect (hMainWindow, NULL, TRUE);
			InvalidateRect (eWindow, NULL, TRUE);
			InvalidateRect (eFussWindow, NULL, TRUE);
			UpdateWindow (hMainWindow);
			UpdateWindow (eWindow);
			UpdateWindow (eFussWindow);

		    delete textblock;
		    textblock = NULL;
			if (syskey == KEY5)
			{
				return 0;
			}
			ToPosTxt ();
			return 0;
}


/* Auswahl ueber Temperaturen    */


struct TEMPS
{
        char temptext [20];
        char temp [6];
};


struct TEMPS temparr [4], temps;


static int tempidx;
static int tempanz = 0;

mfont TempFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                1,
                                NULL};

static field _tempform [] =
{
        temps.temptext, 16, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        temps.temp,      6, 1, 0,17, 0,     "%3.1",    DISPLAYONLY, 0, 0, 0,
};

static form tempform = {2, 0, 0, _tempform, 0, 0, 0, 0, &TempFont};

static field _tempub [] =
{
        "Temperatur",    16, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        "Wert",           6, 1, 0,17, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form tempub = {2, 0, 0, _tempub, 0, 0, 0, 0, &TempFont};

static field _tempvl [] =
{
    "1",                  1,  0, 0, 17, 0, "", NORMAL, 0, 0, 0,
};

static form tempvl = {1, 0, 0, _tempvl, 0, 0, 0, 0, &TempFont};


void EnterTemperature (int idx)
/**
Reaktion auf Doppelclick in der Liste.
**/
{
	    char buffer[20];
        tempidx = idx;
		if (tempidx == 0)
		{
			int cx = (int) (scrfx * 600.0);
  			int cy = (int) (scrfy * 30);
			NuBlock.SetNumParent (hMainWindow);
			NuBlock.SetNumZent (FALSE, cx, cy);
			strcpy (buffer, aufps.temp);
			NuBlock.EnterNumBox (hMainWindow,"  Temperatur 1 ",
                                         buffer,    5, "%3.1f" ,
	      						 	     EntNuProc);
			if (syskey != KEY5)
			{
				strcpy (aufps.temp, buffer);
				strcpy (temparr[0].temp, buffer); 
				ShowNewElist ((char *) temparr,
							   tempanz,
							   sizeof (struct TEMPS));
				SetListPos (0);
			}
		}
		else if (tempidx == 1)
		{
			int cx = (int) (scrfx * 600.0);
  			int cy = (int) (scrfy * 30);
			NuBlock.SetNumParent (hMainWindow);
			NuBlock.SetNumZent (FALSE, cx, cy);
			strcpy (buffer, aufps.temp2);
			NuBlock.EnterNumBox (hMainWindow,"  Temperatur 2 ",
                                         buffer,    5, "%3.1f" ,
	      						 	     EntNuProc);
			if (syskey != KEY5)
			{
				strcpy (aufps.temp2, buffer);
				strcpy (temparr[1].temp, buffer); 
				ShowNewElist ((char *) temparr,
							   tempanz,
							   sizeof (struct TEMPS));
				SetListPos (1);
			}
		}
		else if (tempidx == 2)
		{
			int cx = (int) (scrfx * 600.0);
  			int cy = (int) (scrfy * 30);
			NuBlock.SetNumParent (hMainWindow);
			NuBlock.SetNumZent (FALSE, cx, cy);
			strcpy (buffer, aufps.temp3);
			NuBlock.EnterNumBox (hMainWindow,"  Temperatur 3 ",
                                         buffer,    5, "%3.1f" ,
	      						 	     EntNuProc);
			if (syskey != KEY5)
			{
				strcpy (aufps.temp3, buffer);
				strcpy (temparr[2].temp, buffer); 
				ShowNewElist ((char *) temparr,
							   tempanz,
							   sizeof (struct TEMPS));
				SetListPos (2);
			}
		}
		else if (tempidx == 3)
		{
			int cx = (int) (scrfx * 600.0);
  			int cy = (int) (scrfy * 30);
			NuBlock.SetNumParent (hMainWindow);
			NuBlock.SetNumZent (FALSE, cx, cy);
			strcpy (buffer, aufps.ph_wert);
			NuBlock.EnterNumBox (hMainWindow,"  PH-Wert  ",
                                         buffer, 6, "%4.2f" ,
	      						 	     EntNuProc);
			if (syskey != KEY5)
			{
				strcpy (aufps.ph_wert, buffer);
				strcpy (temparr[3].temp, buffer); 
				ShowNewElist ((char *) temparr,
							   tempanz,
							   sizeof (struct TEMPS));
				SetListPos (3);
			}
		}
		NuBlock.SetNumParent (NULL);
		NuBlock.SetNumZent (FALSE, 0, 0);
        return;
}

int ShowTemperature (HWND hWnd, char * ptabarr, int ptabanz, char * ptab,
					int ptsize, form *ptform, form *ptvl, form *ptub)
{
        RECT rect;
        RECT frmrect;
        int x, y, cx, cy;
        HWND shListBox;
        HWND slbox;
        struct LMENUE smenue;

        GetMenue (&smenue);
        MenTextSave (&smenue);
        shListBox = GethListBox ();
        slbox = Getlbox ();

        RegisterPt ();
        GetWindowRect (hMainWindow, &rect);
        GetFrmRectEx (ptform, ptform->font, &frmrect);

        x = rect.left + sysxplus;
        y = rect.top + 20;
        cx = frmrect.right + 15;
        cy = 185;

        set_fkt (endlist, 5);
        SetDblClck (EnterTemperature, 0);
        PtListeAktiv = 1;
        SetAktivWindow (hWnd);
        SetListFont (TRUE);
        spezfont (ptform->font);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_VISIBLE);
        PtWindow = OpenListWindowBoEx (x, y, cx, cy);
//        PtWindow = OpenListWindowEnEx (x, y, cx, cy);
        GetWindowRect (PtWindow, &rect);
        y = rect.bottom;
		cx = rect.right - rect.left;
        PtFussWindow = CreateWindow ("PtFuss",
                                    "",
                                    WS_POPUP | WS_VISIBLE | WS_DLGFRAME,
                                    x,
//                                    y + cy + 23,
                                    y,
                                    cx, 70,
                                    hWnd,
                                    NULL,
                                    hMainInst,
                                    NULL);
        if (ptvl) ElistVl (ptvl);
        if (ptub) ElistUb (ptub);
        EnableWindows (hMainWindow, FALSE);
        ShowElist ((char *) ptabarr,
                  ptabanz,
                  (char *) ptab,
                  (int) ptsize,
                  ptform);
        SetMouseLockProc (MouseInPt);
        EnterElist (PtWindow, (char *) ptabarr,
                             ptabanz,
                             (char *) ptab,
                             (int) ptsize,
                             ptform);
        CloseControls (&PtButton);
        CloseEWindow (PtWindow);
        SetActiveWindow (hMainWindow);
        EnableWindows (hMainWindow, TRUE);
        init_break_list ();
//        SetMouseLock (FALSE);
        SetMouseLockProc (NULL);
        DestroyWindow (PtFussWindow);
        PtListeAktiv = 0;
        SetMenue (&smenue);
        SethListBox (shListBox);
        Setlbox (slbox);
        SetAktivWindow (hWnd);
        return tempidx;
}


int AuswahlTemperature (void)
/**
Ausahlfenster ueber Geraete anzeigen.
**/
{
        EnableWindow (eWindow, FALSE);
        EnableWindow (eKopfWindow, FALSE);
        EnableWindow (eFussWindow, FALSE);
	    tempanz = 0;
        sprintf (temparr[tempanz].temptext, "Temperatur 1");
        sprintf (temparr[tempanz].temp,"%s", aufps.temp);
	    tempanz ++;
        sprintf (temparr[tempanz].temptext, "Temperatur 2");
        sprintf (temparr[tempanz].temp,"%s", aufps.temp2);
	    tempanz ++;
        sprintf (temparr[tempanz].temptext, "Temperatur 3");
        sprintf (temparr[tempanz].temp,"%s", aufps.temp3);
	    tempanz ++;
        sprintf (temparr[tempanz].temptext, "PH-Wert");
        sprintf (temparr[tempanz].temp,"%s", aufps.ph_wert);
	    tempanz ++;

        syskey = 0;
        sysidx = ShowTemperature (LogoWindow, (char *) temparr, tempanz,
                     (char *) &temps,(int) sizeof (struct TEMPS),
                     &tempform, &tempvl, &tempub);
        EnableWindow (eWindow, TRUE);
        EnableWindow (eKopfWindow, TRUE);
        EnableWindow (eFussWindow, TRUE);
        SetDblClck (IsDblClck, 0);
        if (syskey == KEY5 || syskey == KEYESC)
        {
                return 0;
        }
        return sysidx;
}


//Hier: Auswahl Produktionsfortschritt


struct FORTS
{
        char fortstext [40];
        short forts;
};


struct FORTS fortsarr [4], forts;


static int fortsidx;
static int fortsanz = 0;

mfont FortsFont = {"Courier New", 450, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                1,
                                NULL};

static field _fortsform [] =
{
        forts.fortstext, 16, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form fortsform = {1, 0, 0, _fortsform, 0, 0, 0, 0, &FortsFont};

static field _fortsub [] =
{
        "buchen in",    16, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form fortsub = {1, 0, 0, _fortsub, 0, 0, 0, 0, &FortsFont};

static field _fortsvl [] =
{
    "1",                  1,  0, 0, 17, 0, "", NORMAL, 0, 0, 0,
};

static form fortsvl = {1, 0, 0, _fortsvl, 0, 0, 0, 0, &FortsFont};


void EnterFortschritt (int idx)
{
        fortsidx = idx;
		short fortschritt_alt = atoi(aufps.fortschritt);
         memcpy (&aufps, &aufparr[aufpidx], sizeof (struct AUFP_S));
		sprintf(aufps.fortschritt,"%hd",fortsarr[fortsidx].forts);
		sprintf(aufps.fortschritt_bz,"%s",fortsarr[fortsidx].fortstext);
    	memcpy (&aufparr[aufpidx], &aufps, sizeof (struct AUFP_S));
		short fortschritt_neu = atoi(aufps.fortschritt);
		if (fortschritt_alt != fortschritt_neu)
		{
			arrtozerl_p (aufpidx);
			beginwork ();
			ZerlpClass.dbupdate (atoi (EinAusgang));
			zerl_p.fortschritt = fortschritt_alt;
			BucheBsd (-1,zerl_p.nto_gew,zerl_p.charge); 
			zerl_p.fortschritt = fortschritt_neu;
			BucheBsd (1,zerl_p.nto_gew,zerl_p.charge); 
			commitwork ();
		}
	//sonst doppelt		LeseWePos ();
		//	SetListPos (aufpidx);

        break_list ();

}

int ShowFortschritt (HWND hWnd, char * ptabarr, int ptabanz, char * ptab,
					int ptsize, form *ptform, form *ptvl, form *ptub)
{
        RECT rect;
        RECT frmrect;
        int x, y, cx, cy;
        HWND shListBox;
        HWND slbox;
        struct LMENUE smenue;

        GetMenue (&smenue);
        MenTextSave (&smenue);
        shListBox = GethListBox ();
        slbox = Getlbox ();

        RegisterPt ();
        GetWindowRect (hMainWindow, &rect);
        GetFrmRectEx (ptform, ptform->font, &frmrect);

        x = rect.left + sysxplus;
        y = rect.top + 20;
        cx = frmrect.right + 15;
        cy = 285;

        set_fkt (endlist, 5);
        SetDblClck (EnterFortschritt, 0);
        PtListeAktiv = 1;
        SetAktivWindow (hWnd);
        SetListFont (TRUE);
        spezfont (ptform->font);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_VISIBLE);
        PtWindow = OpenListWindowBoEx (x, y, cx, cy);
//        PtWindow = OpenListWindowEnEx (x, y, cx, cy);
        GetWindowRect (PtWindow, &rect);
        y = rect.bottom;
		cx = rect.right - rect.left;
        PtFussWindow = CreateWindow ("PtFuss",
                                    "",
                                    WS_POPUP | WS_VISIBLE | WS_DLGFRAME,
                                    x,
//                                    y + cy + 23,
                                    y,
                                    cx, 70,
                                    hWnd,
                                    NULL,
                                    hMainInst,
                                    NULL);
        if (ptvl) ElistVl (ptvl);
        if (ptub) ElistUb (ptub);
		AuswahlAktiv = TRUE;
        EnableWindows (hMainWindow, FALSE);
        ShowElist ((char *) ptabarr,
                  ptabanz,
                  (char *) ptab,
                  (int) ptsize,
                  ptform);
        SetMouseLockProc (MouseInPt);
        EnterElist (PtWindow, (char *) ptabarr,
                             ptabanz,
                             (char *) ptab,
                             (int) ptsize,
                             ptform);
		AuswahlAktiv = FALSE;
        CloseControls (&PtButton);
        CloseEWindow (PtWindow);
        SetActiveWindow (hMainWindow);
        EnableWindows (hMainWindow, TRUE);
        init_break_list ();
//        SetMouseLock (FALSE);
        SetMouseLockProc (NULL);
        DestroyWindow (PtFussWindow);
        PtListeAktiv = 0;
        SetMenue (&smenue);
        SethListBox (shListBox);
        Setlbox (slbox);
        SetAktivWindow (hWnd);
        return fortsidx;
}


int AuswahlFortschritt (void)
/**
Ausahlfenster ueber Geraete anzeigen.
**/
{
		int prodphase[10] ;
		int i,y;
		double a;

		for (i = 0; i< 10; i++)
		{
			prodphase[i] = 0;
		}

		a = ratod(aufps.a);
        Prdk_k.sqlin ((short *)  &Mandant, 1, 0);
        Prdk_k.sqlin ((double *) &a,   3, 0);
        Prdk_k.sqlout ((double *) prdk_k.prodphase,   0, sizeof (prdk_k.prodphase));
        int dsqlstatus = Prdk_k.sqlcomm ("select prodphase from prdk_k "
                                         "where mdn = ? "
                                         "and a = ? "
                                         "and akv = 1");

		if (dsqlstatus == 0)
		{
			y = 0;
			for (i = 0; i< 10; i++)
			{
				if (prdk_k.prodphase[i] == 'J')
				{
					prodphase[y] = i+1; y++;
				}
			}
			if (y == 0) // keine prodphase gepflegt in der rezeptur , dann Standard  (1 und 2 (Vorbereitung und Produktion) )
			{
				prodphase[0] = 1; y++;
				prodphase[1] = 2; y++;
			}
		}


        EnableWindow (eWindow, FALSE);
        EnableWindow (eKopfWindow, FALSE);
        EnableWindow (eFussWindow, FALSE);
	    fortsanz = 0;
		while (prodphase[fortsanz] > 0)
		{
			sprintf (ptabn.ptwert, "%d", prodphase[fortsanz]);
		    if (ptab_class.lese_ptab ("prodphase", ptabn.ptwert) == 0)
			{
				sprintf (fortsarr[fortsanz].fortstext, ptabn.ptbez);
				fortsarr[fortsanz].forts = (short) ptabn.ptlfnr;
			}
			fortsanz ++;
		}

        syskey = 0;
        sysidx = ShowFortschritt (LogoWindow, (char *) fortsarr, fortsanz,
                     (char *) &forts,(int) sizeof (struct FORTS),
                     &fortsform, &fortsvl, &fortsub);
        EnableWindow (eWindow, TRUE);
        EnableWindow (eKopfWindow, TRUE);
        EnableWindow (eFussWindow, TRUE);
        SetDblClck (IsDblClck, 0);
        if (syskey == KEY5 || syskey == KEYESC)
        {
                return 0;
        }
        return sysidx;
}



void DoChargeZwang (void)
{

						char buffer [255];
						if (strlen(clipped(aufps.ls_charge)) > 0)
						{
							return;
						}
						if (_a_bas.charg_hand == 9)
						{
                          strcpy (buffer, aufps.ident_nr);
						  NuBlock.SetCharChangeProc (ChangeChar);
						  /*110908 so ist es auf den Touchern bei M�fli h�ngengeblieben
                          NuBlock.EnterNumBox (LogoWindow, "  Ident-Nr  ",
                                       buffer, -1, "", EntNuProc);
									   */
                          NuBlock.EnterNumBox (WiegWindow, "  Ident-Nr  ",
                                       buffer, -1, "", EntNuProc);    
						}
						else if (_a_bas.charg_hand == 1)
						{
                          strcpy (buffer, aufps.ls_charge); //110908
						  NuBlock.SetCharChangeProc (ChangeChar);//110908
/* 110908
                          EnterNumBox (WiegWindow,"  Chargen-Nr  ",
                                       buffer, 17,
                                       artwform.mask[0].picture);
									   */
                          NuBlock.EnterNumBox (WiegWindow,"  Chargen-Nr  ",
                                       buffer, -1, "", EntNuProc);    
						}
						if (syskey != KEY5)
						{
								if ((_a_bas.charg_hand == 9 || _a_bas.charg_hand == 1) && strlen (buffer) > 4)
								{
									Text Ean = buffer;
									ScanEan128Charge (Ean,0);
								}
								else
								{
									strcpy (aufps.ls_charge, buffer); 
								}

								display_form (WiegWindow, &artwform, 0, 0);
								current_wieg = TestAktivButton (PLUS, current_wieg);
								SetWiegFocus ();
						 }
}


BOOL BsdArtikel (double a)
/**
Bestandskennzeichen in a_bas pruefen.
**/
{
	     char bsd_kz [2];

		 strcpy (bsd_kz, "N");
		 LiefBzg.sqlin ((double *) &a, 3, 0);
		 LiefBzg.sqlout ((char *) bsd_kz, 0, 2);
		 LiefBzg.sqlcomm ("select bsd_kz from a_bas where a = ?");
		 if (bsd_kz[0] == 'J') return TRUE;
		 return FALSE;
}

void BucheBsd (int faktor,double me, char *chargennr)
// 190709
//Bestandsbuchung vorbereiten.

{
	BsdBuch.BucheBsd(faktor,me,chargennr,atoi(EinAusgang));
}


double HoleBsdMe (int mdn,double a, int lgr)
{
	char bsd_lgr_ort [17];
	double bsd_me = 0;;

	sprintf (bsd_lgr_ort,"%ld",lgr);
    DbClass.sqlout ((short *) &bsd_me, 3, 0);
    DbClass.sqlin ((short *) &mdn, 2, 0);
    DbClass.sqlin ((short *) &a, 3, 0);
    DbClass.sqlin ((short *) bsd_lgr_ort, 0, 16);
    dsqlstatus = DbClass.sqlcomm ("select bsd_gew from bsds where mdn = ? and bsds.fil = 0 "
						"and a = ? and bsd_lgr_ort = ? ");

	if (dsqlstatus != 0)
	{
		return 0.0;
	}


    DbClass.sqlout ((short *) &bsd_me, 3, 0);
    DbClass.sqlin ((short *) &mdn, 2, 0);
    DbClass.sqlin ((short *) &a, 3, 0);
    DbClass.sqlin ((short *) bsd_lgr_ort, 0, 16);
    dsqlstatus = DbClass.sqlcomm ("select sum(bsd_gew) from bsds where mdn = ? and bsds.fil = 0 "
						"and a = ? and bsd_lgr_ort = ? ");

	return bsd_me;

}



int menfunc3 (void)
/**
Function fuer Button 3
**/
{
         HWND aktfocus;

         auf_art = 0;
		 bin_in_menfunc = 3;
		 if (atoi(EinAusgang) == EINGANG) 
		 {
			 /**
        aufps.a,           11, 1, 0, 0, 0, "%11.0f", EDIT, 0, 0, 0,
        aufps.a_bz1,       20, 1, 0, 13,0, "",       READONLY, 0, 0, 0,
        aufps.bsd_me,       6, 1, 0, 35, 0, "%9.3f", EDIT, 0, 0, 0,
		aufps.auf_me_bz,    2, 1, 0, 42, 0, "",      READONLY, 0, 0, 0,
        aufps.lief_me,      9, 1, 0, 46, 0, "%9.3f", READONLY , 0, 0, 0,
        aufps.s,            3, 1, 0, 57, 0, "",      READONLY, 0, 0, 0,
		**/
//ZuTun
				testform.mask[0].feld = aufps.a;
				testform.mask[0].length = 11;
				testform.mask[0].pos[1] = 0;
				testform.mask[1].feld = aufps.a_bz1;
				testform.mask[1].length = 20;
				testform.mask[1].pos[1] = 13;
				testform.mask[2].feld = aufps.bsd_me;
				testform.mask[2].length = 6;
				testform.mask[2].pos[1] = 35;
				testform.mask[3].feld = aufps.auf_me_bz;
				testform.mask[3].length = 2;
				testform.mask[3].pos[1] = 42;
				testform.mask[4].feld = aufps.lief_me;
				testform.mask[4].length = 9;
				testform.mask[4].pos[1] = 46;
				testform.mask[5].feld = aufps.s;
				testform.mask[5].length = 3;
				testform.mask[5].pos[1] = 57;

	         PartieNr = AuswahlPartie ();
		     LiefKopf ();
	 	     strcpy(zerl_k.lief,"");
			aktfocus = GetFocus ();
			SetColFocus (MainButton.mask[0].feldid);
			AktButton = GetAktButton ();
			return 0;
		 }

		 if (atoi(EinAusgang) == UMLAGERUNG) 
		 {
			 zerl_p.kng = atoi(EinAusgang);
			 sysdate (lieferdatum);
			 LiefEnterAktiv = 1;
			 zerl_p.mdn = Mandant;
			 zerl_p.partie = GenPartie0();

			 menfunc2(); //direkt in Positionen
			 return 0;
		 }
		 if (atoi(EinAusgang) == ZERLEGUNG) 
		 {
			a_wahl = 0;
			aktfocus = GetFocus ();
	        PartieNr = ScanneCharge();
			if (syskey == KEY5 || PartieNr == 0.0) return 0;
			SetColFocus (MainButton.mask[0].feldid);
			AktButton = GetAktButton ();
		     BelegeZerl_p ();
			 LiefEnterAktiv = 1;
			 menfunc2(); //direkt in Positionen
			return 0;
		 }
		 if (atoi(EinAusgang) == FEINZERLEGUNG) 
		 {
			a_wahl = 0;
			aktfocus = GetFocus ();
	        PartieNr = ScanneChargeFeinZerl();
			if (syskey == KEY5 || PartieNr == 0.0) return 0;
			SetColFocus (MainButton.mask[0].feldid);
			AktButton = GetAktButton ();
		     BelegeZerl_p ();
			 LiefEnterAktiv = 1;
			 menfunc2(); //direkt in Positionen
			return 0;
		 }
		 if (atoi(EinAusgang) == PRODUKTIONSSTART ) 
		 {
			 zerl_p.kng = atoi(EinAusgang);
			 sysdate (lieferdatum);
			 LiefEnterAktiv = 0;
			 zerl_p.mdn = Mandant;
//  			 zerl_p.partie = GenPartie0();
		     zerl_p.lgr_zu = akt_lager_zu;
			 zerl_p.lgr_ab = akt_lager_ab;

		    LiefKopfE = ChrgKopfE;
		    LiefKopfT = ChrgKopfT;

		     LiefKopf ();
	 	     strcpy(zerl_k.lief,"");
			aktfocus = GetFocus ();
			SetColFocus (MainButton.mask[0].feldid);
			AktButton = GetAktButton ();


//			 menfunc2(); //direkt in Positionen
			 return 0;
		 }
		 if (atoi(EinAusgang) == PRODUKTIONSFORTSCHRITT ) 
		 {
			 zerl_p.kng = atoi(EinAusgang);
			 sysdate (lieferdatum);
			 LiefEnterAktiv = 1;
			 zerl_p.mdn = Mandant;
//			 zerl_p.partie = GenPartie0();

			 menfunc2(); //direkt in Positionen
			 return 0;
		 }
		 if (atoi(EinAusgang) == BUCHENFERTIGLAGER ) 
		 {
			 zerl_p.kng = atoi(EinAusgang);
			 sysdate (lieferdatum);
			 if (MitKunde )
			 {
				 LiefEnterAktiv = 0;
				 zerl_p.mdn = Mandant;
	
			    LiefKopfE = BuchKopfE;
			    LiefKopfT = BuchKopfT;

				LiefKopf ();
	 			strcpy(zerl_k.lief,"");
				aktfocus = GetFocus ();
				SetColFocus (MainButton.mask[0].feldid);
				AktButton = GetAktButton ();
				return 0;
			 }
			 else
			 {
				 LiefEnterAktiv = 1;
				 zerl_p.mdn = Mandant;
				 menfunc2(); //direkt in Positionen
				 return 0;
			 }
			 return 0;
		 }
		 return 0;
}

int BelegeZerl_p (void)
{

	int idx, cursor;
	beginwork ();

//======Zerlegeeingang
	memcpy (&zerl_p, &zerl_p_null, sizeof (zerl_p));
	zerl_p.mdn = zerl_k.mdn;
	zerl_p.partie = zerl_k.partie;
	zerl_p.kng = EINGANG;
	zerl_p.a = a_wahl;
	zerl_p.int_pos = 1;
    if (ZerlpClass.dbreadfirst () == 100)
	{
		zerl_p.bearb = lzerldat;
	    zerl_p.lgr_zu = akt_lager_zu;
		zerl_p.lgr_ab = akt_lager_ab;
		systime (zeit);
		memcpy (zerl_p.zeit, zeit, 8);
		zerl_p.kng = EINGANG;
		strcpy(zerl_p.charge, ChargeAuswahl);
		zerl_p.standort = Standort;
		zerl_p.int_pos = 1;
		ZerlpClass.dbupdate (atoi (EinAusgang));
	}

//======Zerlegeausgang
        sql_in ((char *)  &akt_mdn, 1, 0);
        sql_in ((char *)  &zerl_k.schnitt , 2, 0);
        sql_in ((char *)  &a_wahl, 3, 0);
        sql_out ((char *)  &zerl_p.a, 3, 0);
        cursor = prepare_sql ("select a_mat.a  "
                                      "from zerm,a_mat "
                                      "where zerm.zerm_mat = a_mat.mat "
                                      "and zerm.mdn = ? "
                                      "and zerm.schnitt = ? "
                                      "and zerm.zerm_aus_mat = ? "
									  " and zerm.zerm_aus_mat <> zerm.zerm_mat and zerm.a_kz <> \"ZV\" " 
                                      "order by a_mat.a");

        idx = 2;
        while (fetch_sql (cursor) == 0)
        {
			zerl_p.mdn = zerl_k.mdn;
			zerl_p.partie = zerl_k.partie;
			zerl_p.kng = AUSGANG;
			zerl_p.int_pos = idx;
            if (ZerlpClass.dbreadfirst () == 100)
			{
				zerl_p.bearb = lzerldat;
				zerl_p.lgr_zu = akt_lager_zu;
				zerl_p.lgr_ab = akt_lager_ab;
				systime (zeit);
				memcpy (zerl_p.zeit, zeit, 8);
				strcpy(zerl_p.charge, ChargeAuswahl);
				zerl_p.standort = Standort;
				ZerlpClass.dbupdate (atoi (EinAusgang));
			}
            idx ++;
        }
        close_sql (cursor);




	commitwork ();
	return 0;

}

double ScanneCharge (void)
{
	 char cCharge [10];
	int dCharge = 0;
	int dsqlstatus = 100;
	char a_bz1 [25];
	int lief_s;
		 sprintf (cCharge, "");
		 NuBlock.SetChoise (doChargeChoise, 0);
						  EnableWindows (AufKopfWindow, FALSE);
  	                      EnableWindows (eKopfWindow, FALSE);
                          EnableWindows (eFussWindow, FALSE);
                          EnableWindows (BackWindow2, FALSE);
                          EnableWindows (BackWindow3, FALSE);
                          EnableWindows (eWindow, FALSE);
         NuBlock.EnterNumBox (LogoWindow,  "  Charge ", cCharge, 10, "",EntNuProc);

  	                      EnableWindows (eKopfWindow, TRUE);
                          EnableWindows (eFussWindow, TRUE);
                          EnableWindows (BackWindow2, TRUE);
                          EnableWindows (BackWindow3, TRUE);
                          EnableWindows (eWindow, TRUE);
						  EnableWindows (AufKopfWindow, TRUE);
                   InvalidateRect (eWindow, NULL, TRUE);
                   UpdateWindow (eWindow);
                   SetDblClck (IsDblClck, 0);

		 if (syskey == KEY5) return 0.0;


		sprintf(bsd_lager,"%ld",LagerAb);
		 if (strlen(clipped(cCharge)) > 0)
		 {
	        DbClass.sqlin  ((char *) &akt_mdn, 1, 0);
		    DbClass.sqlin  ((char *) bsd_lager, 0, 12);
		    DbClass.sqlin  ((char *) cCharge, 0, 10);
			DbClass.sqlout  ((long *) &dschnitt, 2, 0);
	        DbClass.sqlout  ((double *) &a_wahl, 3, 0);
			DbClass.sqlout  ((long *) &lief_s, 2, 0);
			DbClass.sqlout  ((char *) a_bz1, 0, 24);
			dsqlstatus = DbClass.sqlcomm ("select unique schnitt.schnitt, bsds.a, bsds.lief_s,a_bas.a_bz1 from schnitt,a_mat,a_bas,bsds where "
								"    a_bas.a_grund = bsds.a "
								"and a_bas.a = a_mat.a "
								"and a_mat.mat = schnitt.schnitt_mat "
								"and a_bas.bsd_kz = \"J\" "
								"and bsds.mdn = ? and bsds.bsd_lgr_ort = ? "
								"and bsds.chargennr = ? and bsds.fil = 0 ");
			if (dsqlstatus == 0) 
			{
				strcpy (ChargeAuswahl, cCharge);
			    sprintf(lief_nr,"%s",ChargeAuswahl);

			    sysdate (datum);
				lzerldat = dasc_to_long (datum);
				zerl_k.mdn = akt_mdn;
				zerl_k.partie = ratod( lief_nr );
				strcpy (zerl_k.partie_bz, a_bz1);
				sprintf (zerl_k.lief, "%ld", lief_s);
				zerl_k.schnitt = dschnitt;
				beginwork();
				ZerlkClass.dbupdate ();
				commitwork();
				return zerl_k.partie;
			}

		 }
		 return 0;

}

double ScanneChargeFeinZerl (void)
{
	 char cCharge [10];
	int dCharge = 0;
	int dsqlstatus = 100;
	char a_bz1 [25];
	int lief_s;
		 sprintf (cCharge, "");
		 NuBlock.SetChoise (doChargeChoise, 0);
						  EnableWindows (AufKopfWindow, FALSE);
  	                      EnableWindows (eKopfWindow, FALSE);
                          EnableWindows (eFussWindow, FALSE);
                          EnableWindows (BackWindow2, FALSE);
                          EnableWindows (BackWindow3, FALSE);
                          EnableWindows (eWindow, FALSE);
         NuBlock.EnterNumBox (LogoWindow,  "  Charge ", cCharge, 10, "",EntNuProc);

  	                      EnableWindows (eKopfWindow, TRUE);
                          EnableWindows (eFussWindow, TRUE);
                          EnableWindows (BackWindow2, TRUE);
                          EnableWindows (BackWindow3, TRUE);
                          EnableWindows (eWindow, TRUE);
						  EnableWindows (AufKopfWindow, TRUE);
                   InvalidateRect (eWindow, NULL, TRUE);
                   UpdateWindow (eWindow);
                   SetDblClck (IsDblClck, 0);

		 if (syskey == KEY5) return 0.0;
		doartchoiseCharge(cCharge); 


		sprintf(bsd_lager,"%ld",LagerAb);
		 if (strlen(clipped(cCharge)) > 0)
		 {
	        DbClass.sqlin  ((char *) &akt_mdn, 1, 0);
		    DbClass.sqlin  ((char *) bsd_lager, 0, 12);
		    DbClass.sqlin  ((char *) cCharge, 0, 10);
			DbClass.sqlout  ((long *) &dschnitt, 2, 0);
	        DbClass.sqlout  ((double *) &a_wahl, 3, 0);
			DbClass.sqlout  ((long *) &lief_s, 2, 0);
			DbClass.sqlout  ((char *) a_bz1, 0, 24);
			dsqlstatus = DbClass.sqlcomm ("select unique zerm.schnitt, bsds.a, bsds.lief_s,a_bas.a_bz1 from zerm,a_mat,a_bas,bsds where "
								"    a_bas.a_grund = bsds.a "
								"and a_bas.a = a_mat.a "
								"and a_mat.mat = zerm.zerm_aus_mat and zerm.a_kz = \"VK\" "
								"and a_bas.bsd_kz = \"J\" "
								"and bsds.mdn = ? and bsds.bsd_lgr_ort = ? "
								"and bsds.chargennr = ? and bsds.fil = 0 ");
			if (dsqlstatus == 0) 
			{
				strcpy (ChargeAuswahl, cCharge);
			    sprintf(lief_nr,"%s",ChargeAuswahl);

			    sysdate (datum);
				lzerldat = dasc_to_long (datum);
				zerl_k.mdn = akt_mdn; 
				zerl_k.partie = ratod( lief_nr );
				strcpy (zerl_k.partie_bz, a_bz1);
				sprintf (zerl_k.lief, "%ld", lief_s);
				zerl_k.schnitt = dschnitt;
				beginwork();
				ZerlkClass.dbupdate ();
				commitwork();
				return zerl_k.partie;
			}

		 }
		 return 0;

}


void FuelleAufpArr (void) //GROM-6
{
		double zerlaus_me = 0.0;
		double bsd_me = 0.0;
		int i;

		SetPosColorProc (ListRowColor);

	      for (i = 0; i < aufpanz; i ++)
          {
			    if (aufparr[i].kng == EINGANG)
				{
				  sprintf (aufparr[i].zerlein_me,"%6.3lf", ratod (aufparr[i].lief_me)); 
				  sprintf (aufparr[i].zerlaus_me,"%s", " "); 
	 			  bsd_me = HoleBsdMe(Mandant,ratod(aufparr[i].a),LagerAb); 
				}
				else if (aufparr[i].kng == AUSGANG)
				{
				  sprintf (aufparr[i].zerlein_me,"%s", " "); 
				  sprintf (aufparr[i].zerlaus_me,"%6.3lf", ratod (aufparr[i].lief_me)); 
	 			  bsd_me = HoleBsdMe(Mandant,ratod(aufparr[i].a),LagerZu); 
				}
				else if (aufparr[i].kng == UMLAGERUNG)
				{
	 			  bsd_me = HoleBsdMe(Mandant,ratod(aufparr[i].a),LagerAb); 
				}
			    sprintf (aufparr[i].bsd_me,"%7.3lf", bsd_me); 

		  }

	      for (i = 1; i < aufpanz; i ++)
          {
				zerlaus_me += ratod (aufparr[i].zerlaus_me);
		  }
		  sprintf (aufparr[0].zerlaus_me,"%.3lf", zerlaus_me); 

}


BOOL ListRowColor (COLORREF *fcolor, COLORREF *bcolor, int idx)
/**
Farbe fuer Positionszeile ermitteln.
**/
{
     if (AuswahlAktiv) return FALSE;
		if (aufparr[idx].status == -1) 
		{
			     *fcolor = WHITECOL;
			     *bcolor = GREENCOL;
				 return TRUE;
		}

		if (atoi(EinAusgang) == ZERLEGUNG) 
		{
		     if (aufparr [idx].kng == EINGANG)
			 {
			     *fcolor = WHITECOL;
			     *bcolor = LTGRAYCOL;
				 return TRUE;
			 }
		}
		else if (atoi(EinAusgang) == FEINZERLEGUNG) 
		{
		     if (aufparr [idx].kng == EINGANG)
			 {
			     *fcolor = WHITECOL;
			     *bcolor = LTGRAYCOL;
				 return TRUE;
			 }
		}
	if (atoi(EinAusgang) != PRODUKTIONSSTART && atoi(EinAusgang) != PRODUKTIONSFORTSCHRITT && atoi(EinAusgang) != BUCHENFERTIGLAGER) 
	{
        if (ratod (aufparr[idx].a) > 0.0 && atoi (aufparr[idx].ls_charge) <= 0.0)   
        {
		     *fcolor = WHITECOL;
		     *bcolor = REDCOL;
 			 return TRUE;
        }
       
        if (atoi (aufparr [idx].s) > 2)
		{
            if (ratod (aufparr[idx].lief_me) <= 0.0)
            {
			     *fcolor = WHITECOL;
			     *bcolor = REDCOL;
            }
            else
            {
			     *fcolor = KompfColor;
			     *bcolor = KompbColor;
            }
		    return TRUE;
		}
	}


		 return FALSE;
}

int doEtikettendruck (void) //FISCH-5
{
         char buffer [80];

      strcpy (buffer,"1");
	  EnableWindows (AufKopfWindow, FALSE);
      EnableWindows (eKopfWindow, FALSE);
      EnableWindows (eFussWindow, FALSE);
      EnableWindows (BackWindow2, FALSE);
      EnableWindows (BackWindow3, FALSE);
      EnableWindows (eWindow, FALSE);
 	  NuBlock.SetCharChangeProc (ChangeChar);
     NuBlock.EnterNumBox (eWindow,"  Anzahl Etiketten  ",
                             buffer, -1, "" ,
						  EntNuProc);
  	  EnableWindows (eKopfWindow, TRUE);
      EnableWindows (eFussWindow, TRUE);
      EnableWindows (BackWindow2, TRUE);
      EnableWindows (BackWindow3, TRUE);
      EnableWindows (eWindow, TRUE);
	  EnableWindows (AufKopfWindow, TRUE);
      InvalidateRect (eWindow, NULL, TRUE);
      UpdateWindow (eWindow);

	  if (syskey != KEY5)
	  {
		WriteParamArt (atoi(buffer));
	  }
	  return 0;
}




short MaxFortschritt (double a) //FISCH-5
{
		int prodphase[10] ;
		int i,y;

		for (i = 0; i< 10; i++)
		{
			prodphase[i] = 0;
		}

        Prdk_k.sqlin ((short *)  &Mandant, 1, 0);
        Prdk_k.sqlin ((double *) &a,   3, 0);
        Prdk_k.sqlout ((double *) prdk_k.prodphase,   0, sizeof (prdk_k.prodphase));
        int dsqlstatus = Prdk_k.sqlcomm ("select prodphase from prdk_k "
                                         "where mdn = ? "
                                         "and a = ? "
                                         "and akv = 1");

		if (dsqlstatus == 0)
		{
			y = 0;
			for (i = 0; i< 10; i++)
			{
				if (prdk_k.prodphase[i] == 'J')
				{
					prodphase[y] = i+1; y++;
				}
			}
			if (y == 0) // keine prodphase gepflegt in der rezeptur , dann Standard  (1 und 2 (Vorbereitung und Produktion) )
			{
				prodphase[0] = 1; y++;
				prodphase[1] = 2; y++;
			}
			y--;
			sprintf (ptabn.ptwert, "%d", prodphase[y]);
		    if (ptab_class.lese_ptab ("prodphase", ptabn.ptwert) == 0)
			{
				return (atoi(ptabn.ptwert));
			}
		}
		return 0;
}

	
BOOL UpdateFortschritt (int dfortschritt) //FISCH-5
{
	char cfortschritt [8];
	char datum [12];
	beginwork ();
	aufparr[aufidx].status = 0;
	
	sprintf (cfortschritt ,"%d", dfortschritt);
	if (dfortschritt > 0)
	{
			if (ptab_class.lese_ptab ("prodphase", cfortschritt) == 0)
			{
				sprintf (aufparr[aufidx].fortschritt_bz,  "%s", ptabn.ptbezk);
				sprintf (aufparr[aufidx].fortschritt, "%d" , dfortschritt);
			}
								
	}
	ScanValues->Init ();
//	memcpy (&aufparr[aufpidx], &aufps, sizeof (struct AUFP_S));
	arrtozerl_p (aufpidx);
	sysdate(datum);
    zerl_p.bearb = dasc_to_long (datum);

	systime (zerl_p.zeit);
	ZerlpClass.dbupdate (atoi (EinAusgang));
	commitwork ();
	LeseWePos ();

    SetListPos (aufpidx);
    ShowNewElist ((char *) aufparr,
                               aufpanz,
                               (int) sizeof (struct AUFP_S));
	memcpy (&aufps, &aufparr[aufpidx], sizeof (struct AUFP_S));
	if (aufpanz == 1)
	{
        ActivateColButton (BackWindow3,  &MainButton2, 2, 0, 1);
		ActivateColButton (eFussWindow, &lFussform,   0, 0, 1);
		ActivateColButton (eFussWindow, &lFussform,   1, 0, 1);
		ActivateColButton (eFussWindow, &lFussform,   2, 0, 1);
		ActivateColButton (eFussWindow, &lFussform,   3, 0, 1);
	}

    delete ScanValues;
	ScanValues = NULL;
	return TRUE;
}
void stringcopy(char * Ziel, const char * Quelle, int sizeZiel) 
{
	 int lenQuelle = strlen (Quelle) ;
	 int lenZiel = strlen (Ziel) ;
	 if (sizeZiel == 1) {strcpy (Ziel,Quelle); return; }
	 if (sizeZiel == 4) {strcpy (Ziel,Quelle); return; }
	 if (lenQuelle > sizeZiel) 
	 {
		 lenQuelle = sizeZiel;
	 }
	 strncpy (Ziel, Quelle, lenQuelle);
	 Ziel[lenQuelle] = 0;
}
void stringcopy(char * Ziel, const char * Quelle) 
{
	 strcpy (Ziel,Quelle);
}

int  HoleZeilenAnzahl (char *string, int len) 
{
	int i=0;
	int b = 0;
	int anz ;
	char buffer[2049];
    memset (buffer, ' ', len);
    buffer [len -1] = (char) 0;

	for(;;)
	{
		if(string[i] != char (13) && string[i] != char (10))
		{
			buffer[b] = string[i];
			b++;
		}
		if(string[i] == char (13))
		{
				buffer[b] = '|';
				b++;
		}
		if(string[i] == '\0')
		{
			break;
		}
		i++;
	}

    anz = wsplit (buffer, "|");
	if (anz <= 1) return 0;
	if (anz > 10) return 0;
	len = 0;
	for (i = 0;i <= anz;i++)
	{
		if (strlen (clipped(wort[i])) > (unsigned) len) len = strlen(clipped(wort[i]));
	}
	if (len > 90) return 0;


	return anz;
}

void EntferneZeilenende(char *string, int len) 
{
	int i=0;
	int b = 0;
	char buffer[2049];
    memset (buffer, ' ', len -1);
    buffer [len -1] = (char) 0;

	for(;;)
	{
		if(string[i] != char (13) && string[i] != char (10))
		{
			buffer[b] = string[i];
			b++;
		}
		if(string[i] == char (13))
		{
			if (b > 0)
			{
				if (buffer[b -1] !=  ' ') 
				{
					buffer[b] = ' ';
					b++;
				}
			}
			else
			{
				buffer[b] = ' ';
				b++;
			}
		}
		if(string[i] == '\0')
		{
			break;
		}
		i++;
	}
	strncpy (string,buffer,len-1);
    string [len -1] = (char) 0;
	return ;
}


int ReadKun (void)
{
	        char datum [12];
            readstatus = 0;

			if (strcmp (lieferdatum, "                 ") <= 0) return 0;
			if (strcmp (lief_nr, "                 ") <= 0) return 0;
			sysdate (datum);
            freestat = 0;
			NeuePartie = 0;
			Kunde = atoi(lief_nr);
			zerl_k.mdn = Mandant;
            return 0;
}

int EnterCharge (void) //FISCH-5
{
         char buffer [80];
		 Text TCharge;

//      ScanValues->Init (); 
      strcpy (buffer,"");
	  EnableWindows (AufKopfWindow, FALSE);
      EnableWindows (eKopfWindow, FALSE);
      EnableWindows (eFussWindow, FALSE);
      EnableWindows (BackWindow2, FALSE);
      EnableWindows (BackWindow3, FALSE);
      EnableWindows (eWindow, FALSE);
 	  NuBlock.SetCharChangeProc (ChangeChar);
     NuBlock.EnterNumBox (eWindow,"  Charge  ",
                             buffer, -1, "" ,
						  EntNuProc);
  	  EnableWindows (eKopfWindow, TRUE);
      EnableWindows (eFussWindow, TRUE);
      EnableWindows (BackWindow2, TRUE);
      EnableWindows (BackWindow3, TRUE);
      EnableWindows (eWindow, TRUE);
	  EnableWindows (AufKopfWindow, TRUE);
      InvalidateRect (eWindow, NULL, TRUE);
      UpdateWindow (eWindow);

	  if (syskey != KEY5)
	  {
		  TCharge = buffer;	
          
  		  ScanEan128Ai10 (TCharge);  
		  if (ScanValues->Charge.GetLen() == 0)
		  {
			    Text Charge = buffer;
	  			ScanValues->SetCharge (Charge);
		  }
	  }
	  return 0;
}

int EnterGewicht (void) //FISCH-5
{
         char buffer [80];

      strcpy (buffer,"");
	  EnableWindows (AufKopfWindow, FALSE);
      EnableWindows (eKopfWindow, FALSE);
      EnableWindows (eFussWindow, FALSE);
      EnableWindows (BackWindow2, FALSE);
      EnableWindows (BackWindow3, FALSE);
      EnableWindows (eWindow, FALSE);
	  sprintf (buffer,"%.3lf", 0.0);
     NuBlock.EnterNumBox (eWindow,"  Gewicht  ",
                             buffer, -1, "" ,
						  EntNuProc);
  	  EnableWindows (eKopfWindow, TRUE);
      EnableWindows (eFussWindow, TRUE);
      EnableWindows (BackWindow2, TRUE);
      EnableWindows (BackWindow3, TRUE);
      EnableWindows (eWindow, TRUE);
	  EnableWindows (AufKopfWindow, TRUE);
      InvalidateRect (eWindow, NULL, TRUE);
      UpdateWindow (eWindow);

	  if (syskey != KEY5)
	  {
  			ScanValues->SetGew (ratod(buffer));
	  }
	  return 0;
}

int  HoleFanggebiet (char *charge)
/**
Laufende Nummer in zerl_k updaten.
**/
{
	  int dsqlstatus;
	  char cfanggebiet [10];

      DbClass.sqlout ((char *) cfanggebiet, 0, 10);
      DbClass.sqlin ((short *) &Mandant, 1, 0);
      DbClass.sqlin ((double *) &zerl_k.a, 3, 0);
      DbClass.sqlin ((char *) charge, 0, 30);

	  dsqlstatus = DbClass.sqlcomm ("select es_nr from we_jour "
		                  "where mdn = ? "
						  "and a = ? "
						  "and   ls_charge = ? ");

	 
	  if (dsqlstatus == 0) return atoi(cfanggebiet);
	  return 0;
}

int ScanProdStart (void)
{
	int dret;
	ScanValues = new CScanValues;
	Text TScan = scanprodstart;
	if (TScan.GetLen () == 0)
	{
	  SetFocus (LiefKopfE.mask[0].feldid);
		return 0;
	}
    ScanValues->Init ();
	if (zerl_k.a == 0.0)
	{
		dret = ScanEan128Ai01 (TScan);
		if (ScanValues->a > (double) 0)
		{
			if (lese_a_bas (ScanValues->a) == 0)
			{
				zerl_k.a = _a_bas.a;
		        strcpy (artikelbez,_a_bas.a_bz1);
				display_field (AufKopfWindow, &LiefKopfT.mask[4], 0, 0);
				sprintf (artikel, "%.0lf",_a_bas.a);
				zerl_k.a = _a_bas.a;
			}


		}
	}
	if (zerl_k.fanggebiet == 0)
	{
		ScanEan128Ai10 (TScan);
		if (ScanValues->GetChargeLen () > 0)
		{
			zerl_k.fanggebiet = HoleFanggebiet (ScanValues->Charge.GetBuffer());
			if (zerl_k.fanggebiet > 0)
			{
              sprintf (ptabn.ptwert, "%hd",zerl_k.fanggebiet);
		      ptab_class.lese_ptab ("fanggebiet", ptabn.ptwert);
			  strcpy (fanggebiet, ptabn.ptbez);
			}
		}
	}
	zerl_k.partie = GenPartie0();
	ScanMessage.Complete ();
	strcpy (scanprodstart, "");
 	display_field (AufKopfWindow, &LiefKopfE.mask[0], 0, 0);
 	display_field (AufKopfWindow, &LiefKopfE.mask[1], 0, 0);
	display_field (AufKopfWindow, &LiefKopfE.mask[2], 0, 0);
	display_field (AufKopfWindow, &LiefKopfE.mask[3], 0, 0);
	display_field (AufKopfWindow, &LiefKopfE.mask[4], 0, 0);
    delete ScanValues;
	ScanValues = NULL;
	setlspos = 1    ;  //Hier: setlspos !=0 hei�t,  Focus (lspos ++) wird nicht  wieter hochgesetzt
	if (liste_direct && FlgNeuePartie == FALSE)
	{
        WriteLS ();
		menfunc2 ();
	}


	return 0;
}
int AuswahlFanggebiet (void)
{
	return 0;
}

BOOL SchreibeProdAbschluss (void)
{
//Hier: Abschluss : des Produktionsstarts
//		es muss ausgew�hlt werden, welcher Produktionsartikel hergestellt werden soll 
//        Schreiben : 
//  	       zerl_k: update zerl_k.partie_status nach 1
//		   zerl_p. update : zerl_p.kng auf -6 
//		  	       insert : s�tze mit zerl_p.kng = 7    (kumuliert (auf prodcharge) lesen der S�tze mit kng = -6 insert mit kng=7,status = 1 und dem Produktionsartikel 


	//zerl_k.partie_status von 0 auf 1   
	// zerl_p.fortschritt auf 1 setzen
 double sum_gew = 0;
	beginwork ();

	if (zerl_k.a_prod == 0.0)
	{
		if (HoleProdartikel (_a_bas.a) == 0.0) return FALSE;
		zerl_k.a_prod = prodartikel.a;
		doEtikettendruck ();
	}
//1. Zerl_k auf Status 1
	zerl_k.partie_status = 1;
	ZerlkClass.dbupdate ();

    
//2. Zerl_p auf Kennung auf -6   (S�tze bleiben vorerst als Sicherung bestehen
	zerl_p.mdn = zerl_k.mdn;
	zerl_p.partie = zerl_k.partie;
	zerl_p.kng = PRODUKTIONSSTART ;
	int dkng = PRODUKTIONSSTART * -1 ; 
	dsqlstatus = ZerlpClass.dbreadfirst_o ("order by int_pos, a");
	sum_gew = 0;
	while (dsqlstatus == 0)
	{
		sum_gew = sum_gew + zerl_p.nto_gew;
        DbClass.sqlin   ((long *) &dkng, 2, 0);
        DbClass.sqlin   ((short *) &zerl_p.mdn, 1, 0);
        DbClass.sqlin   ((double *) &zerl_p.partie, 3, 0);
        DbClass.sqlin   ((double *) &zerl_p.a, 3, 0);
        DbClass.sqlin   ((short *) &zerl_p.kng, 1, 0);
        DbClass.sqlin   ((long *) &zerl_p.int_pos, 2, 0);
		DbClass.sqlcomm ("update zerl_p set kng = ?  where mdn = ? and partie = ? and a = ? and kng = ? and int_pos = ? ");

		zerl_p.kng = PRODUKTIONSFORTSCHRITT; 
		dsqlstatus = ZerlpClass.dbread_o ();
	}

//3. neuen kumulierten Satz schreiben mit dem Produktionsartikel
	zerl_p.mdn = zerl_k.mdn;
	zerl_p.partie = zerl_k.partie;
	zerl_p.kng = PRODUKTIONSSTART ;
	zerl_p.nto_gew = sum_gew;
	zerl_p.a = prodartikel.a;
    zerl_p.status = 1;
    zerl_p.int_pos = 10;
	zerl_p.fortschritt = 0; 
	sprintf (zerl_p.charge, "%.0lf", zerl_p.partie);
	ZerlpClass.dbupdate (zerl_p.kng);

//4. buchen dieses Satzes in den Bestand  (status = 1, daher nur Zubuchung)
	BucheBsd (1,zerl_p.nto_gew,zerl_p.charge); 

    
//5. kennung auf PRODUKTIONSFORTSCHRITT setzen  
	    dkng = PRODUKTIONSFORTSCHRITT ;
		zerl_p.status = 0;
        DbClass.sqlin   ((long *) &dkng, 2, 0);
        DbClass.sqlin   ((short *) &zerl_p.status, 1, 0);
        DbClass.sqlin   ((short *) &zerl_p.mdn, 1, 0);
        DbClass.sqlin   ((double *) &zerl_p.partie, 3, 0);
        DbClass.sqlin   ((double *) &zerl_p.a, 3, 0);
        DbClass.sqlin   ((short *) &zerl_p.kng, 1, 0);
        DbClass.sqlin   ((long *) &zerl_p.int_pos, 2, 0);
		DbClass.sqlcomm ("update zerl_p set kng = ?, status = ? where mdn = ? and partie = ? and a = ? and kng = ? and int_pos = ? ");

	commitwork ();
  return TRUE;
}


BOOL ChargeInListe (char *Charge)
{
	int i = 0;
	for ( i = 0; i < aufpanz ; i++)
	{
		if (memcmp (clipped(aufparr[i].ls_charge) , Charge, strlen(Charge) ) == 0) 
		{
			aufpidx = i;
			return TRUE;
		}
	}
	return FALSE;
}






char  pttext [20] = "Text";

field _fptabformt [] =
{
        ptab.ptbez,     34, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        ptab.ptwer1,     9, 1, 0,35, 0, "", DISPLAYONLY, 0, 0, 0
};

form fptabformt = {1, 0, 0, _fptabformt, 0, 0, 0, 0, &PtFont};

field _fptabubt [] =
{
        pttext,          34, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        " Wert",            9, 1, 0,35, 0, "", DISPLAYONLY, 0, 0, 0
};

form fptabubt = {1, 0, 0, _fptabubt, 0, 0, 0, 0, &PtFont};

field _fptabvlt [] =
{      "1",                  1,  0, 0, 35, 0, "", NORMAL, 0, 0, 0,
};

form fptabvlt = {1, 0, 0, _fptabvlt, 0, 0, 0, 0, &PtFont};


void AddPosTxt (char *txt, BOOL Zuschlag, double proz)
/**
Text fuer Nicht- osder Nachliefern einfuegen.
**/
{
       int aufidx;
       long lsp_txt;

       beginwork ();
       if (Lock_Lsp () == FALSE)
       {
            commitwork ();
            return;
       }
       aufidx = GetListPos ();
       lsp_txt = atol (aufparr [aufidx].lsp_txt);
       if (lsp_txt == 0l)
       {
           lsp_txt = GenLText ();
       }
       if (lsp_txt == 0l) return;
       sprintf (aufparr[aufidx].lsp_txt, "%ld", lsp_txt);
       memcpy (&aufps, &aufparr [aufidx], sizeof (struct AUFP_S));
       lspt.nr = lsp_txt;
       DbClass.sqlin ((long *) &lspt.nr, 2, 0);
       DbClass.sqlout ((long *) &lspt.zei, 2, 0);
       if (DbClass.sqlcomm ("select max (zei) from lspt where nr = ?") != 0)
       {
                    lspt.zei = 0;
       }
       else if (DbClass.IsLongnull (lspt.zei))
       {
                    lspt.zei = 0;
       }
       lspt.zei += 10;
       stringcopy (lspt.txt, txt,sizeof(lspt.txt));
       lspt_class.dbupdate ();
       arrtozerl_p (aufidx);
	   ZerlpClass.dbupdate (atoi(EinAusgang));
       commitwork ();
       ShowNewElist ((char *) aufparr,
                      aufpanz,
                      (int) sizeof (struct AUFP_S));
       memcpy (&aufps, &aufparr [aufidx], sizeof (struct AUFP_S));
}


char *PtCaption = NULL;
int SelectPosTexte (void)
{
	   char PosText [35];
       char PosWert [9];
	   ptidx = 1;
 	   SetPosColorProc (NULL);
	   PtCaption = "Positionstexte zuweisen";
	   stringcopy (pttext, "Buchungsgrund",sizeof(pttext));

       dsqlstatus = ptab_class.lese_ptab_all ("na_lief_kz");
	   int anfang_der_festwerte = 3;
	   ptabanz = 0;
       stringcopy (ptabarr[0].ptbez,  "==> Positionstext entfernen     ",sizeof(ptabarr[0].ptbez));   //WAL-191
       stringcopy (ptabarr[1].ptbez,  "==> neuen Positionstext erfassen",sizeof(ptabarr[1].ptbez));  //WAL-191
       stringcopy (ptabarr[2].ptbez,  "Texte :  ",sizeof(ptabarr[2].ptbez));  //WAL-191
	   ptabanz = anfang_der_festwerte;

       while (dsqlstatus == 0)
       {
                   stringcopy (ptabarr[ptabanz].ptbez,  ptabn.ptbez);
                   stringcopy (ptabarr[ptabanz].ptwer1, ptabn.ptwer1);
                   ptabanz ++;
                   if (ptabanz == ptabmaxanz) break;
                   dsqlstatus = ptab_class.lese_ptab_all ();
        }

        SaveMenue ();
	    EnableWindow (hMainWindow, FALSE);
        EnableWindow (eWindow, FALSE);
        EnableWindow (eFussWindow, FALSE);

        EnableWindow (eKopfWindow, FALSE);
        ptidx = ShowPtBox (eWindow, (char *) ptabarr, ptabanz,
                     (char *) &ptab,(int) sizeof (struct PT),
                     &fptabformt, &fptabvlt, &fptabubt);
        EnableWindow (hMainWindow, TRUE);
        EnableWindow (eWindow, TRUE);
        EnableWindow (eFussWindow, TRUE);
        EnableWindow (eKopfWindow, TRUE);

		// FS-362 A
        SetActiveWindow (eWindow);
        AktivWindow = eWindow;
		// FS-362 E
		RestoreMenue ();
		SetDblClck (IsDblClck, 0);     
	    aufidx = GetListPos (); //WAL-191

		if (ptidx == 0)
		{
			//PosText entfernen
				    stringcopy (aufparr[aufidx].lsp_txt,"");
					aufparr[aufidx].pos_txt_kz = (short) 0;
					aufparr[aufidx].na_lief_kz = (short) 0;  //WAL-191 
				    stringcopy (aufps.lsp_txt,"");
					aufps.pos_txt_kz = (short) 0;
					aufps.na_lief_kz = (short) 0;  //WAL-191 
			         arrtozerl_p (aufidx);
					beginwork ();
					ZerlpClass.dbupdate (atoi (EinAusgang));
					commitwork ();
					//LeseWePos ();
					//SetListPos (pos);
					ShowNewElist ((char *) aufparr,
                                aufpanz,
                                (int) sizeof (struct AUFP_S));

					print_messG (2, "Der Positionstext wurde wieder entfernt");
		}
		else if (ptidx == 1) 
		{
			//Pos Text Schreiben
	        PosTexte (); 
			SetActiveWindow (eWindow);
			InvalidateRect (eWindow, NULL, TRUE);
			if (syskey != KEY5) 
			{
				aufparr[aufidx].na_lief_kz = (short) 0;  
				aufps.na_lief_kz = (short) 0;  //WAL-191 
			}
		}
		else
		{
			stringcopy (PosText, ptabarr [ptidx].ptbez,sizeof(PosText));
			stringcopy (PosWert, ptabarr [ptidx].ptwer1,sizeof(PosWert));
			ptabanz = 0;
 			SetPosColorProc (ListRowColor);
	        if (syskey != KEY5)
			{
	            AddPosTxt (PosText, FALSE, 0.0);
				aufparr[aufidx].na_lief_kz =  ptidx - anfang_der_festwerte + 1;  //WAL-191
				aufps.na_lief_kz =  ptidx - anfang_der_festwerte + 1;  //WAL-191
			}
		}
        return 0;
}

void PROD_MainButtons_inaktiv (void)
{
	if (atoi(EinAusgang) == PRODUKTIONSSTART)
	{
         ActivateColButton (BackWindow2, &MainButton, 1, -1, 1);
         ActivateColButton (BackWindow2, &MainButton, 2, -1, 1);
         ActivateColButton (BackWindow2, &MainButton, 3, -1, 1);
	}
	if (atoi(EinAusgang) == PRODUKTIONSFORTSCHRITT)
	{
         ActivateColButton (BackWindow2, &MainButton, 1, -1, 1);
         ActivateColButton (BackWindow2, &MainButton, 2, -1, 1);
         ActivateColButton (BackWindow2, &MainButton, 3, -1, 1);
	}
	if (atoi(EinAusgang) == BUCHENFERTIGLAGER)
	{
         ActivateColButton (BackWindow2, &MainButton, 1, -1, 1);
         ActivateColButton (BackWindow2, &MainButton, 2, -1, 1);
         ActivateColButton (BackWindow2, &MainButton, 3, -1, 1);
	}
}
