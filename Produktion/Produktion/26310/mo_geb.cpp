#include <windows.h>
#include "mo_curso.h"
#include "mo_geb.h"

struct A_KUN_GEB a_kun_geb, a_kun_geb_null;

void GEB_CLASS::prepare (void)
{
        ins_quest ((char *) &a_kun_geb.mdn, 1, 0);
        ins_quest ((char *) &a_kun_geb.fil, 1, 0);
        ins_quest ((char *) &a_kun_geb.kun, 2, 0);
        ins_quest ((char *) &a_kun_geb.a,   3, 0);

        out_quest ((char *) a_kun_geb.geb_eti,   0, 2);
        out_quest ((char *) &a_kun_geb.geb_fill, 1, 0);
        out_quest ((char *) &a_kun_geb.geb_anz,  2, 0);

        out_quest ((char *) a_kun_geb.pal_eti,   0, 2);
        out_quest ((char *) &a_kun_geb.pal_fill, 1, 0);
        out_quest ((char *) &a_kun_geb.pal_anz,  2, 0);

        out_quest ((char *) &a_kun_geb.tara,  3, 0);
        out_quest ((char *) &a_kun_geb.mhd,  1, 0);

        a_kun_curs = prepare_sql ("select geb_eti, geb_fill, geb_anz, "
                                  "pal_eti, pal_fill, pal_anz,tara,hbk_ztr "
                                  "from a_kun_gx "
                                  "where mdn = ? and fil = ? and kun = ? and a = ?");

        out_quest ((char *) a_kun_geb.geb_eti,   0, 1);
        out_quest ((char *) &a_kun_geb.geb_fill, 1, 0);
        out_quest ((char *) &a_kun_geb.geb_anz,  2, 0);

        out_quest ((char *) a_kun_geb.pal_eti,   0, 1);
        out_quest ((char *) &a_kun_geb.pal_fill, 1, 0);
        out_quest ((char *) &a_kun_geb.pal_anz,  2, 0);

        out_quest ((char *) &a_kun_geb.tara,  3, 0);
        out_quest ((char *) &a_kun_geb.mhd,  1, 0);
        kun_geb_curs = prepare_sql ("select geb_eti, geb_fill, geb_anz, "
                                    "pal_eti, pal_fill, pal_anz, tara, mhd from kun_geb ");

/*  Update - Cursor                   */

        ins_quest ((char *) a_kun_geb.geb_eti,   0, 1);
        ins_quest ((char *) &a_kun_geb.geb_fill, 1, 0);
        ins_quest ((char *) &a_kun_geb.geb_anz,  2, 0);

        ins_quest ((char *) a_kun_geb.pal_eti,   0, 1);
        ins_quest ((char *) &a_kun_geb.pal_fill, 1, 0);
        ins_quest ((char *) &a_kun_geb.pal_anz,  2, 0);
        ins_quest ((char *) &a_kun_geb.tara,  3, 0);
        ins_quest ((char *) &a_kun_geb.mhd,  1, 0);
        geb_curs_upd = prepare_sql ("update  kun_geb set (geb_eti, geb_fill, geb_anz, "
                                    "pal_eti, pal_fill, pal_anz, tara, mhd) = "
                                    "(?, ?, ?, ?, ?, ?, ?, ?)");

/*  Insert - Cursor                   */

        ins_quest ((char *) a_kun_geb.geb_eti,   0, 1);
        ins_quest ((char *) &a_kun_geb.geb_fill, 1, 0);
        ins_quest ((char *) &a_kun_geb.geb_anz,  2, 0);

        ins_quest ((char *) a_kun_geb.pal_eti,   0, 1);
        ins_quest ((char *) &a_kun_geb.pal_fill, 1, 0);
        ins_quest ((char *) &a_kun_geb.pal_anz,  2, 0);
        ins_quest ((char *) &a_kun_geb.tara,  3, 0);
        ins_quest ((char *) &a_kun_geb.mhd,  1, 0);
        geb_curs_ins = prepare_sql ("insert into kun_geb  (geb_eti, geb_fill, geb_anz, "
                                    "pal_eti, pal_fill, pal_anz, tara, mhd) values "
                                    "(?, ?, ?, ?, ?, ?, ?, ?)");

/* Test-Cursor fuer Update  */


         test_geb_curs = prepare_sql ("select geb_eti from kun_geb");
       
}

int GEB_CLASS::read_a_kun_first (void)
/**
1. Satz aus a_kun lesen.
**/ 
{
        if (a_kun_curs == -1)
        {
                         prepare ();
        }
        open_sql (a_kun_curs);
        fetch_sql (a_kun_curs);
        return (sqlstatus);
} 

int GEB_CLASS::read_kun_geb_first (void)
/**
1. Satz aus kun_geb lesen.
**/ 
{
        if (a_kun_curs == -1)
        {
                         prepare ();
        }
        open_sql (kun_geb_curs);
        fetch_sql (kun_geb_curs);
        return (sqlstatus);
} 

int GEB_CLASS::update_kun_geb (void)
/**
kun_geb updaten.
**/ 
{
        if (a_kun_curs == -1)
        {
                         prepare ();
        }
        open_sql (test_geb_curs);
        fetch_sql (test_geb_curs);
        if (sqlstatus == 0)
        {
                       execute_curs (geb_curs_upd);
        }
        else if (sqlstatus == 100)
        {
                       execute_curs (geb_curs_ins);
        }
        return (sqlstatus);
} 
