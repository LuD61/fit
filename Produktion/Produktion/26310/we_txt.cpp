#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "a_kun.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "we_txt.h"

struct WE_TXT we_txt, we_txt_null;

void WE_TXT_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &we_txt.nr, 2, 0);
    out_quest ((char *) &we_txt.nr,2,0);
    out_quest ((char *) we_txt.txt,0,61);
    out_quest ((char *) &we_txt.zei,2,0);
            cursor = prepare_sql ("select we_txt.nr,  "
"we_txt.txt,  we_txt.zei from we_txt "

#line 26 "we_txt.rpp"
                                  "where nr = ? "
                                  "order by zei");

    ins_quest ((char *) &we_txt.nr,2,0);
    ins_quest ((char *) we_txt.txt,0,61);
    ins_quest ((char *) &we_txt.zei,2,0);
            sqltext = "update we_txt set we_txt.nr = ?,  "
"we_txt.txt = ?,  we_txt.zei = ? "

#line 30 "we_txt.rpp"
                                  "where nr = ? "
                                  "and zei  = ?";

            ins_quest ((char *) &we_txt.nr, 2, 0);
            ins_quest ((char *) &we_txt.zei, 2, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &we_txt.nr, 2, 0);
            ins_quest ((char *) &we_txt.zei, 2, 0);
            test_upd_cursor = prepare_sql ("select nr from we_txt "
                                  "where nr = ? "
                                  "and   zei  = ?");

            ins_quest ((char *) &we_txt.nr, 2, 0);
            ins_quest ((char *) &we_txt.zei, 2, 0);
            del_cursor = prepare_sql ("delete from we_txt "
                                  "where nr = ? "
                                  "and   zei  = ?");

            ins_quest ((char *) &we_txt.nr, 2, 0);
            del_cursor_posi = prepare_sql ("delete from we_txt "
                                  "where  nr = ?");

    ins_quest ((char *) &we_txt.nr,2,0);
    ins_quest ((char *) we_txt.txt,0,61);
    ins_quest ((char *) &we_txt.zei,2,0);
            ins_cursor = prepare_sql ("insert into we_txt ("
"nr,  txt,  zei) "

#line 54 "we_txt.rpp"
                                      "values "
                                      "(?,?,?)"); 

#line 56 "we_txt.rpp"
}
int WE_TXT_CLASS::dbreadfirst (void)
/**

Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int WE_TXT_CLASS::delete_wepposi (void)
/**
Alle text fuer eine Position in aufp loeschen
**/
{
         if (del_cursor_posi == -1)
         {  
                this->prepare ();
         }
         execute_curs (del_cursor_posi);
         return (sqlstatus);
}

