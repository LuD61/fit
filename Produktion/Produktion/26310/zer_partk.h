#ifndef _ZER_PARTKDEF
#define _ZER_PARTKDEF
#include "dbclass.h"

struct ZER_PARTK {
   short     mdn;
   char      zer_part[9];
   long      zer_dat;
   char      zer_bz[49];
   char      zer_band[5];
   char      zer_kol[4];
   double    ges_gew;
   double    zer_ges_pr;
   char      zer_status[2];
   char      zer_a_teil[5];
   short     delstatus;
   char      sort[2];
   short     planstatus;
   short     lgr_pla_kz;
   long      mhd;
   short     obergeb_art;
   short     untergeb_art;
   short     qua_kz;
};
extern struct ZER_PARTK zer_partk, zer_partk_null;

#line 6 "zer_partk.rh"

class ZER_PARTK_CLASS : public DB_CLASS
{
       private :
               void prepare (char*);
               void prepare (void);
               short cursor_partie;

       public :
               ZER_PARTK_CLASS () : DB_CLASS ()
               {
                    cursor_partie = -1;

               }
               int lese_partie (void);
               int lese_partie_bz (int);
               int lese_partie (char*);
};
#endif
