#ifndef _LGRDEF
#define _LGRDEF
#include "dbclass.h"

struct LGR {
   short     mdn;
   short     fil;
   short     lgr;
   char      lgr_bz[25];
   short     delstatus;
   short     lgr_kz;
   char      lgr_smt_kz[4];
   short     lgr_gr;
   char      lgr_kla[2];
   char      abr_period[2];
   long      adr;
   short     afl;
   char      bli_kz[2];
   long      dat_ero;
   double    fl_lad;
   double    fl_nto;
   double    fl_vk_ges;
   short     frm;
   long      iakv;
   char      inv_rht[2];
   char      ls_abgr[2];
   char      ls_kz[2];
   short     ls_sum;
   char      pers[13];
   short     pers_anz;
   char      pos_kum[2];
   char      pr_ausw[2];
   char      pr_bel_entl[2];
   char      pr_lgr_kz[2];
   long      pr_lst;
   char      pr_vk_kz[2];
   double    reg_bed_theke_lng;
   double    reg_kt_lng;
   double    reg_kue_lng;
   double    reg_lng;
   double    reg_tks_lng;
   double    reg_tkt_lng;
   char      smt_kz[2];
   short     sonst_einh;
   short     sprache;
   char      sw_kz[2];
   long      tou;
   short     vrs_typ;
   char      inv_akv[2];
};
extern struct LGR lgr, lgr_null;

#line 6 "lgr.rh"

class LGR_CLASS : public DB_CLASS
{
       private :
               void prepare (char*);
               void prepare (void);
               short cursor_lgr;
               short cursor_a_lgr;
               short cursor_lgr_bz;

       public :
               LGR_CLASS () : DB_CLASS ()
               {
                    cursor_lgr = -1;
                    cursor_a_lgr = -1;
                    cursor_lgr_bz = -1;

               }
               int lese_lgr (void);
               int lese_lgr_bz (int);
               int lese_lgr (char*);
};
#endif
