#ifndef _WE_WIEPRO_DEF
#define _WE_WIEPRO_DEF

#include "dbclass.h"

struct WE_WIEPRO {
   short     mdn;
   short     fil;
   double    a;
   long      best_blg;
   char      blg_typ[2];
   char      lief_rech_nr[17];
   long      lief_s;
   double    me;
   double    tara;
   char      zeit[7];
   long      dat;
   long      sys;
   char      pers[17];
   char      erf_kz[2];
   long      anzahl;
   char      qua_kz[2];
   char      lief[17];
   char      ls_charge[21];
   char      ls_ident[21];
   long      es_nr;
};
extern struct WE_WIEPRO we_wiepro, we_wiepro_null;

#line 7 "we_wiepro.rh"

class WE_WIEPRO_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               WE_WIEPRO_CLASS () : DB_CLASS ()
               {
               }
               int dbreadfirst (void);
               int dbinsert (void);
               int dbupdate (void);
               int dbdelete (void);
               int dbclose (void);
};
#endif
