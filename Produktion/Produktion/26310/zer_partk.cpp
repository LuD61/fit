#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "zer_partk.h"

struct ZER_PARTK zer_partk, zer_partk_null;

/* Variablen fuer Auswahl mit Buttonueberschrift    */


void ZER_PARTK_CLASS::prepare (void)
{
    out_quest ((char *) &zer_partk.mdn,1,0);
    out_quest ((char *) zer_partk.zer_part,0,9);
    out_quest ((char *) &zer_partk.zer_dat,2,0);
    out_quest ((char *) zer_partk.zer_bz,0,49);
    out_quest ((char *) zer_partk.zer_band,0,5);
    out_quest ((char *) zer_partk.zer_kol,0,4);
    out_quest ((char *) &zer_partk.ges_gew,3,0);
    out_quest ((char *) &zer_partk.zer_ges_pr,3,0);
    out_quest ((char *) zer_partk.zer_status,0,2);
    out_quest ((char *) zer_partk.zer_a_teil,0,5);
    out_quest ((char *) &zer_partk.delstatus,1,0);
    out_quest ((char *) zer_partk.sort,0,2);
    out_quest ((char *) &zer_partk.planstatus,1,0);
    out_quest ((char *) &zer_partk.lgr_pla_kz,1,0);
    out_quest ((char *) &zer_partk.mhd,2,0);
    out_quest ((char *) &zer_partk.obergeb_art,1,0);
    out_quest ((char *) &zer_partk.untergeb_art,1,0);
    out_quest ((char *) &zer_partk.qua_kz,1,0);
         cursor_partie = prepare_sql ("select zer_partk.mdn,  "
"zer_partk.zer_part,  zer_partk.zer_dat,  zer_partk.zer_bz,  "
"zer_partk.zer_band,  zer_partk.zer_kol,  zer_partk.ges_gew,  "
"zer_partk.zer_ges_pr,  zer_partk.zer_status,  zer_partk.zer_a_teil,  "
"zer_partk.delstatus,  zer_partk.sort,  zer_partk.planstatus,  "
"zer_partk.lgr_pla_kz,  zer_partk.mhd,  zer_partk.obergeb_art,  "
"zer_partk.untergeb_art,  zer_partk.qua_kz from zer_partk");

#line 23 "zer_partk.rpp"

}
void ZER_PARTK_CLASS::prepare (char *order )
{
    if ( order)
    {
    out_quest ((char *) &zer_partk.mdn,1,0);
    out_quest ((char *) zer_partk.zer_part,0,9);
    out_quest ((char *) &zer_partk.zer_dat,2,0);
    out_quest ((char *) zer_partk.zer_bz,0,49);
    out_quest ((char *) zer_partk.zer_band,0,5);
    out_quest ((char *) zer_partk.zer_kol,0,4);
    out_quest ((char *) &zer_partk.ges_gew,3,0);
    out_quest ((char *) &zer_partk.zer_ges_pr,3,0);
    out_quest ((char *) zer_partk.zer_status,0,2);
    out_quest ((char *) zer_partk.zer_a_teil,0,5);
    out_quest ((char *) &zer_partk.delstatus,1,0);
    out_quest ((char *) zer_partk.sort,0,2);
    out_quest ((char *) &zer_partk.planstatus,1,0);
    out_quest ((char *) &zer_partk.lgr_pla_kz,1,0);
    out_quest ((char *) &zer_partk.mhd,2,0);
    out_quest ((char *) &zer_partk.obergeb_art,1,0);
    out_quest ((char *) &zer_partk.untergeb_art,1,0);
    out_quest ((char *) &zer_partk.qua_kz,1,0);
         cursor_partie = prepare_sql ("select zer_partk.mdn,  "
"zer_partk.zer_part,  zer_partk.zer_dat,  zer_partk.zer_bz,  "
"zer_partk.zer_band,  zer_partk.zer_kol,  zer_partk.ges_gew,  "
"zer_partk.zer_ges_pr,  zer_partk.zer_status,  zer_partk.zer_a_teil,  "
"zer_partk.delstatus,  zer_partk.sort,  zer_partk.planstatus,  "
"zer_partk.lgr_pla_kz,  zer_partk.mhd,  zer_partk.obergeb_art,  "
"zer_partk.untergeb_art,  zer_partk.qua_kz from zer_partk "

#line 30 "zer_partk.rpp"
                               " %s ",order);
    }
    else
    {
    out_quest ((char *) &zer_partk.mdn,1,0);
    out_quest ((char *) zer_partk.zer_part,0,9);
    out_quest ((char *) &zer_partk.zer_dat,2,0);
    out_quest ((char *) zer_partk.zer_bz,0,49);
    out_quest ((char *) zer_partk.zer_band,0,5);
    out_quest ((char *) zer_partk.zer_kol,0,4);
    out_quest ((char *) &zer_partk.ges_gew,3,0);
    out_quest ((char *) &zer_partk.zer_ges_pr,3,0);
    out_quest ((char *) zer_partk.zer_status,0,2);
    out_quest ((char *) zer_partk.zer_a_teil,0,5);
    out_quest ((char *) &zer_partk.delstatus,1,0);
    out_quest ((char *) zer_partk.sort,0,2);
    out_quest ((char *) &zer_partk.planstatus,1,0);
    out_quest ((char *) &zer_partk.lgr_pla_kz,1,0);
    out_quest ((char *) &zer_partk.mhd,2,0);
    out_quest ((char *) &zer_partk.obergeb_art,1,0);
    out_quest ((char *) &zer_partk.untergeb_art,1,0);
    out_quest ((char *) &zer_partk.qua_kz,1,0);
         cursor_partie = prepare_sql ("select zer_partk.mdn,  "
"zer_partk.zer_part,  zer_partk.zer_dat,  zer_partk.zer_bz,  "
"zer_partk.zer_band,  zer_partk.zer_kol,  zer_partk.ges_gew,  "
"zer_partk.zer_ges_pr,  zer_partk.zer_status,  zer_partk.zer_a_teil,  "
"zer_partk.delstatus,  zer_partk.sort,  zer_partk.planstatus,  "
"zer_partk.lgr_pla_kz,  zer_partk.mhd,  zer_partk.obergeb_art,  "
"zer_partk.untergeb_art,  zer_partk.qua_kz from zer_partk "

#line 35 "zer_partk.rpp"
                               " ");
    }
}

int ZER_PARTK_CLASS::lese_partie (char *order)
/**
Tabelle zer_partk
**/
{
         if (cursor_partie == -1)
         {
            prepare (order);
         }
       	 open_sql (cursor_partie);
       	 fetch_sql (cursor_partie);
         
         return sqlstatus;
}

int ZER_PARTK_CLASS::lese_partie (void)
/**
Naechsten Satz aus Tabelle zer_partk lesen.
**/
{
       	fetch_sql (cursor_partie);
         
        if (sqlstatus == 0)
        {
                return 0;
        }
        return 100;
}

int ZER_PARTK_CLASS::lese_partie_bz (int partie)
{
	sprintf (zer_partk.zer_part,"%ld",partie);

        ins_quest ((char *) zer_partk.zer_part, 0, 8);
        out_quest ((char *) zer_partk.zer_bz,0,48);
		return sqlcomm ("select zer_partk.zer_bz from zer_partk where zer_part = ? ");
}
