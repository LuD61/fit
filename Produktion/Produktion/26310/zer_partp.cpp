#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "a_kun.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "zer_partp.h"

struct ZER_PARTP zer_partp, zer_partp_null;

void ZER_PARTP_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &zer_partp.mdn, 1, 0);
            ins_quest ((char *) zer_partp.zer_part, 0, 8);
            ins_quest ((char *) &zer_partp.schnitt, 1, 0);
            ins_quest ((char *) &zer_partp.mat, 2, 0);
    out_quest ((char *) &zer_partp.mdn,1,0);
    out_quest ((char *) zer_partp.zer_part,0,9);
    out_quest ((char *) &zer_partp.schnitt,1,0);
    out_quest ((char *) &zer_partp.mat,2,0);
    out_quest ((char *) zer_partp.a_bz1,0,25);
    out_quest ((char *) zer_partp.hdkl,0,5);
    out_quest ((char *) &zer_partp.zer_stk,1,0);
    out_quest ((char *) &zer_partp.zer_gew,3,0);
    out_quest ((char *) &zer_partp.zer_ek_pr,3,0);
    out_quest ((char *) &zer_partp.zer_kv,3,0);
    out_quest ((char *) zer_partp.zer_lgr_ort,0,13);
    out_quest ((char *) &zer_partp.zer_anz,1,0);
    out_quest ((char *) &zer_partp.zer_gew_ist,3,0);
    out_quest ((char *) &zer_partp.zer_stk_ist,1,0);
    out_quest ((char *) zer_partp.wiegen,0,2);
    out_quest ((char *) &zer_partp.ist_gew,3,0);
    out_quest ((char *) &zer_partp.ist_stk,1,0);
    out_quest ((char *) zer_partp.chargennr,0,11);
    out_quest ((char *) &zer_partp.gebinde,2,0);
            cursor = prepare_sql ("select zer_partp.mdn,  "
"zer_partp.zer_part,  zer_partp.schnitt,  zer_partp.mat,  "
"zer_partp.a_bz1,  zer_partp.hdkl,  zer_partp.zer_stk,  "
"zer_partp.zer_gew,  zer_partp.zer_ek_pr,  zer_partp.zer_kv,  "
"zer_partp.zer_lgr_ort,  zer_partp.zer_anz,  zer_partp.zer_gew_ist,  "
"zer_partp.zer_stk_ist,  zer_partp.wiegen,  zer_partp.ist_gew,  "
"zer_partp.ist_stk,  zer_partp.chargennr,  zer_partp.gebinde from zer_partp "

#line 29 "zer_partp.rpp"
                                  "where mdn = ? "
                                  "and   zer_part = ? "
                                  "and   schnitt = ? "
                                  "and   mat = ? ");

    ins_quest ((char *) &zer_partp.mdn,1,0);
    ins_quest ((char *) zer_partp.zer_part,0,9);
    ins_quest ((char *) &zer_partp.schnitt,1,0);
    ins_quest ((char *) &zer_partp.mat,2,0);
    ins_quest ((char *) zer_partp.a_bz1,0,25);
    ins_quest ((char *) zer_partp.hdkl,0,5);
    ins_quest ((char *) &zer_partp.zer_stk,1,0);
    ins_quest ((char *) &zer_partp.zer_gew,3,0);
    ins_quest ((char *) &zer_partp.zer_ek_pr,3,0);
    ins_quest ((char *) &zer_partp.zer_kv,3,0);
    ins_quest ((char *) zer_partp.zer_lgr_ort,0,13);
    ins_quest ((char *) &zer_partp.zer_anz,1,0);
    ins_quest ((char *) &zer_partp.zer_gew_ist,3,0);
    ins_quest ((char *) &zer_partp.zer_stk_ist,1,0);
    ins_quest ((char *) zer_partp.wiegen,0,2);
    ins_quest ((char *) &zer_partp.ist_gew,3,0);
    ins_quest ((char *) &zer_partp.ist_stk,1,0);
    ins_quest ((char *) zer_partp.chargennr,0,11);
    ins_quest ((char *) &zer_partp.gebinde,2,0);
            sqltext = "update zer_partp set "
"zer_partp.mdn = ?,  zer_partp.zer_part = ?,  zer_partp.schnitt = ?,  "
"zer_partp.mat = ?,  zer_partp.a_bz1 = ?,  zer_partp.hdkl = ?,  "
"zer_partp.zer_stk = ?,  zer_partp.zer_gew = ?,  "
"zer_partp.zer_ek_pr = ?,  zer_partp.zer_kv = ?,  "
"zer_partp.zer_lgr_ort = ?,  zer_partp.zer_anz = ?,  "
"zer_partp.zer_gew_ist = ?,  zer_partp.zer_stk_ist = ?,  "
"zer_partp.wiegen = ?,  zer_partp.ist_gew = ?,  "
"zer_partp.ist_stk = ?,  zer_partp.chargennr = ?,  "
"zer_partp.gebinde = ? "

#line 35 "zer_partp.rpp"
                                  "where mdn = ? "
                                  "and   zer_part = ? "
                                  "and   schnitt = ? "
                                  "and   mat = ? ";


            ins_quest ((char *) &zer_partp.mdn, 1, 0);
            ins_quest ((char *) zer_partp.zer_part, 0, 8);
            ins_quest ((char *) &zer_partp.schnitt, 1, 0);
            ins_quest ((char *) &zer_partp.mat, 2, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &zer_partp.mdn, 1, 0);
            ins_quest ((char *) zer_partp.zer_part, 0, 8);
            ins_quest ((char *) &zer_partp.schnitt, 1, 0);
            ins_quest ((char *) &zer_partp.mat, 2, 0);
            test_upd_cursor = prepare_sql ("select mat from zer_partp "
                                  "where mdn = ? "
                                  "and   zer_part = ? "
                                  "and   schnitt = ? "
                                  "and   mat = ? ");
                                  
            ins_quest ((char *) &zer_partp.mdn, 1, 0);
            ins_quest ((char *) zer_partp.zer_part, 0, 8);
            ins_quest ((char *) &zer_partp.schnitt, 1, 0);
            ins_quest ((char *) &zer_partp.mat, 2, 0);
            del_cursor = prepare_sql ("delete from zer_partp "
                                  "where mdn = ? "
                                  "and   zer_part = ? "
                                  "and   schnitt = ? "
                                  "and   mat = ? ");
                                  
    ins_quest ((char *) &zer_partp.mdn,1,0);
    ins_quest ((char *) zer_partp.zer_part,0,9);
    ins_quest ((char *) &zer_partp.schnitt,1,0);
    ins_quest ((char *) &zer_partp.mat,2,0);
    ins_quest ((char *) zer_partp.a_bz1,0,25);
    ins_quest ((char *) zer_partp.hdkl,0,5);
    ins_quest ((char *) &zer_partp.zer_stk,1,0);
    ins_quest ((char *) &zer_partp.zer_gew,3,0);
    ins_quest ((char *) &zer_partp.zer_ek_pr,3,0);
    ins_quest ((char *) &zer_partp.zer_kv,3,0);
    ins_quest ((char *) zer_partp.zer_lgr_ort,0,13);
    ins_quest ((char *) &zer_partp.zer_anz,1,0);
    ins_quest ((char *) &zer_partp.zer_gew_ist,3,0);
    ins_quest ((char *) &zer_partp.zer_stk_ist,1,0);
    ins_quest ((char *) zer_partp.wiegen,0,2);
    ins_quest ((char *) &zer_partp.ist_gew,3,0);
    ins_quest ((char *) &zer_partp.ist_stk,1,0);
    ins_quest ((char *) zer_partp.chargennr,0,11);
    ins_quest ((char *) &zer_partp.gebinde,2,0);
            ins_cursor = prepare_sql ("insert into zer_partp ("
"mdn,  zer_part,  schnitt,  mat,  a_bz1,  hdkl,  zer_stk,  zer_gew,  zer_ek_pr,  zer_kv,  "
"zer_lgr_ort,  zer_anz,  zer_gew_ist,  zer_stk_ist,  wiegen,  ist_gew,  ist_stk,  "
"chargennr,  gebinde) "

#line 68 "zer_partp.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?)"); 

#line 70 "zer_partp.rpp"
}

void ZER_PARTP_CLASS::prepare_o (char *order)
{

            ins_quest ((char *) &zer_partp.mdn, 1, 0);
            ins_quest ((char *) zer_partp.zer_part, 0, 8);
            if (order)
            {
    out_quest ((char *) &zer_partp.mdn,1,0);
    out_quest ((char *) zer_partp.zer_part,0,9);
    out_quest ((char *) &zer_partp.schnitt,1,0);
    out_quest ((char *) &zer_partp.mat,2,0);
    out_quest ((char *) zer_partp.a_bz1,0,25);
    out_quest ((char *) zer_partp.hdkl,0,5);
    out_quest ((char *) &zer_partp.zer_stk,1,0);
    out_quest ((char *) &zer_partp.zer_gew,3,0);
    out_quest ((char *) &zer_partp.zer_ek_pr,3,0);
    out_quest ((char *) &zer_partp.zer_kv,3,0);
    out_quest ((char *) zer_partp.zer_lgr_ort,0,13);
    out_quest ((char *) &zer_partp.zer_anz,1,0);
    out_quest ((char *) &zer_partp.zer_gew_ist,3,0);
    out_quest ((char *) &zer_partp.zer_stk_ist,1,0);
    out_quest ((char *) zer_partp.wiegen,0,2);
    out_quest ((char *) &zer_partp.ist_gew,3,0);
    out_quest ((char *) &zer_partp.ist_stk,1,0);
    out_quest ((char *) zer_partp.chargennr,0,11);
    out_quest ((char *) &zer_partp.gebinde,2,0);
                    cursor_o = prepare_sql ("select "
"zer_partp.mdn,  zer_partp.zer_part,  zer_partp.schnitt,  "
"zer_partp.mat,  zer_partp.a_bz1,  zer_partp.hdkl,  zer_partp.zer_stk,  "
"zer_partp.zer_gew,  zer_partp.zer_ek_pr,  zer_partp.zer_kv,  "
"zer_partp.zer_lgr_ort,  zer_partp.zer_anz,  zer_partp.zer_gew_ist,  "
"zer_partp.zer_stk_ist,  zer_partp.wiegen,  zer_partp.ist_gew,  "
"zer_partp.ist_stk,  zer_partp.chargennr,  zer_partp.gebinde from zer_partp "

#line 80 "zer_partp.rpp"
                                  "where mdn = ? "
                                  "and   zer_part = ? %s", order );
            }
            else
            {
    out_quest ((char *) &zer_partp.mdn,1,0);
    out_quest ((char *) zer_partp.zer_part,0,9);
    out_quest ((char *) &zer_partp.schnitt,1,0);
    out_quest ((char *) &zer_partp.mat,2,0);
    out_quest ((char *) zer_partp.a_bz1,0,25);
    out_quest ((char *) zer_partp.hdkl,0,5);
    out_quest ((char *) &zer_partp.zer_stk,1,0);
    out_quest ((char *) &zer_partp.zer_gew,3,0);
    out_quest ((char *) &zer_partp.zer_ek_pr,3,0);
    out_quest ((char *) &zer_partp.zer_kv,3,0);
    out_quest ((char *) zer_partp.zer_lgr_ort,0,13);
    out_quest ((char *) &zer_partp.zer_anz,1,0);
    out_quest ((char *) &zer_partp.zer_gew_ist,3,0);
    out_quest ((char *) &zer_partp.zer_stk_ist,1,0);
    out_quest ((char *) zer_partp.wiegen,0,2);
    out_quest ((char *) &zer_partp.ist_gew,3,0);
    out_quest ((char *) &zer_partp.ist_stk,1,0);
    out_quest ((char *) zer_partp.chargennr,0,11);
    out_quest ((char *) &zer_partp.gebinde,2,0);
                    cursor_o = prepare_sql ("select "
"zer_partp.mdn,  zer_partp.zer_part,  zer_partp.schnitt,  "
"zer_partp.mat,  zer_partp.a_bz1,  zer_partp.hdkl,  zer_partp.zer_stk,  "
"zer_partp.zer_gew,  zer_partp.zer_ek_pr,  zer_partp.zer_kv,  "
"zer_partp.zer_lgr_ort,  zer_partp.zer_anz,  zer_partp.zer_gew_ist,  "
"zer_partp.zer_stk_ist,  zer_partp.wiegen,  zer_partp.ist_gew,  "
"zer_partp.ist_stk,  zer_partp.chargennr,  zer_partp.gebinde from zer_partp "

#line 86 "zer_partp.rpp"
                                  "where mdn = ? "
                                  "and   zer_part = ?");
           }
}

int ZER_PARTP_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int ZER_PARTP_CLASS::dbreadfirst_o (char *order)
/**
Ersten Satz aus Tabelle lesen.
**/
{
     
         if (cursor == -1)
         {
                this->prepare ();
         }
         if (cursor_o == -1)
         {
                this->prepare_o (order);
         }
         open_sql (cursor_o);
         fetch_sql (cursor_o);
         return (sqlstatus);
}

int ZER_PARTP_CLASS::dbread_o ()
/**
Ersten Satz aus Tabelle lesen.
**/
{
       
         fetch_sql (cursor_o);
         return (sqlstatus);
}

int ZER_PARTP_CLASS::dbclose_o ()
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1) return 0;
         close_sql (cursor_o);
         cursor_o = -1;
         return (0);
}

