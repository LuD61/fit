#include <windows.h>
#include "infmacro.h"
#include "mo_intp.h"
#include "strfkt.h"

InfMacro::InfMacro (char *McName)
{
      this->McName = McName;
      inf = NULL;
      Loaded = NULL;
}

InfMacro::~InfMacro ()
{
      if (Loaded)
      {
          perform_ende ();
          Loaded = NULL;
      }
}

BOOL InfMacro::LoadMacro (void)
{
      char *konvert;
      char buffer [512];

      konvert = getenv ("KONVERT");
      if (konvert != NULL)
      {
          sprintf (buffer, "%s\\macros\\%s", konvert, McName);
      }
      else
      {
          sprintf (buffer, "%s", McName);
      }

      inf = fopen (buffer, "r");
      if (inf == NULL && konvert != NULL)
      {
          sprintf (buffer, "%s", McName);
          inf = fopen (buffer, "r");
      }
      if (inf == NULL)
      {
          return FALSE;
      }

      belege_code (inf, buffer);
      belege_data (inf, buffer);
      belege_structs (inf, buffer);
      fclose (inf);
      inf = NULL;
      Loaded = TRUE;
      perform_begin ();
      return TRUE;
}

BOOL InfMacro::RunMacro (char *in, char *outname, char *out)
{
       char *p;
       char *inbuffer;
       int anz;

       if (Loaded == NULL)
       {
             if (LoadMacro () == FALSE)
             {
                 return FALSE;
             }
       }

       if (in != NULL)
       {
          inbuffer = new char [strlen (in) + 1];
          if (inbuffer == NULL)
          {
             return FALSE;
          }
          strcpy (inbuffer, in);
          p = strtok (inbuffer, ";");
          while (p != NULL)
          {
           anz = wsplit (p, "=");
           if (anz > 1) 
           {
                 mov (wort[0], "%s", wort[1]);
           }
           mov (wort[0], "%s", wort[1]);
           p = strtok (NULL, ";");
          }
       }
 	   perform_code ();
       getwert (outname, out, strlen (outname));
       return TRUE;
}

BOOL InfMacro::RunMacro (char *in, char *outname, char *out, char *prog)
{
       char *p;
       char *inbuffer;
       int anz;
       char buffer [256];

       if (Loaded == NULL)
       {
             if (LoadMacro () == FALSE)
             {
                 return FALSE;
             }
       }

       if (in != NULL)
       {
          inbuffer = new char [strlen (in) + 1];
          if (inbuffer == NULL)
          {
             return FALSE;
          }
          strcpy (inbuffer, in);
          p = strtok (inbuffer, ";");
          while (p != NULL)
          {
           anz = wsplit (p, "=");
           if (anz > 1) 
           {
                 mov (wort[0], "%s", wort[1]);
           }
           mov (wort[0], "%s", wort[1]);
           p = strtok (NULL, ";");
          }
       }
       sprintf (buffer, "call %s", prog);
       do_call (buffer);
       getwert (outname, out, strlen (outname));
       return TRUE;
}


      
      