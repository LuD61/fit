#ifndef _GEBINDETARA_DEF
#define _GEBINDETARA_DEF

#include "dbclass.h"

struct GEBINDETARA {
   short     lfd;
   double    a;
   long      zaehler;
   char      bezeichnung[61];
   double    gebinde1;
   short     anz1;
   double    gebinde2;
   short     anz2;
   double    gebinde3;
   short     anz3;
   double    gebinde4;
   short     anz4;
   double    gebinde5;
   short     anz5;
   double    gebinde6;
   short     anz6;
   long      akv;
   double    tara;
};
extern struct GEBINDETARA gebindetara, gebindetara_null;

#line 7 "gebindetara.rh"

class GEBINDETARA_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
               short cursor_all;
       public :
               GEBINDETARA_CLASS () : DB_CLASS ()
               {
                    cursor_all = -1;
               }
               int dbreadfirst (void);
               int readfirstall(void);
               int readnextall (void);
};
#endif
