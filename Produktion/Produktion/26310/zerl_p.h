#ifndef _ZERL_P_DEF
#define _ZERL_P_DEF

#include "dbclass.h"

struct ZERL_P {
   short     mdn;
   double    partie;
   long      bearb;
   double    a;
   double    nto_gew;
   double    tara;
   char      pers[13];
   long      lgr_zu;
   long      lgr_ab;
   short     kng;
   double    stk;
   long      we_txt;
   char      zeit[11];
   char      charge[31];
   long      int_pos;
   short     standort;
   short     fortschritt;
   double    pcharge;
   short     status;
   double    buch_gew;
   long      mhd;
   char      es_nr[11];
   char      ez_nz[11];
};
extern struct ZERL_P zerl_p, zerl_p_null;

#line 7 "zerl_p.rh"

class ZERL_P_CLASS : public DB_CLASS 
{
       private :
               int cursor_o; 
               int cursor_f; 
               int cursor_oz; 
               void prepare (void);
               void prepare_o (char *);
               void prepare_f (char *);
       public :
               ZERL_P_CLASS () : DB_CLASS (), cursor_o (-1), cursor_f (-1)
               {
               }
               int dbreadfirst (void);
               int dbreadfirst_o (char *);
               int dbread_oz (void);
               int dbreadfirst_oz (char *);
               int dbread_o (void);
               int dbclose_o (void);
               int dbreadfirst_f (char *);
               int dbread_f (void);
               int dbclose_f (void);
							 int dbupdate (int );
               
              
};
#endif
