#ifndef _ZER_PARTP_DEF
#define _ZER_PARTP_DEF

#include "dbclass.h"

struct ZER_PARTP {
   short     mdn;
   char      zer_part[9];
   short     schnitt;
   long      mat;
   char      a_bz1[25];
   char      hdkl[5];
   short     zer_stk;
   double    zer_gew;
   double    zer_ek_pr;
   double    zer_kv;
   char      zer_lgr_ort[13];
   short     zer_anz;
   double    zer_gew_ist;
   short     zer_stk_ist;
   char      wiegen[2];
   double    ist_gew;
   short     ist_stk;
   char      chargennr[11];
   long      gebinde;
};
extern struct ZER_PARTP zer_partp, zer_partp_null;

#line 7 "zer_partp.rh"

class ZER_PARTP_CLASS : public DB_CLASS 
{
       private :
               int cursor_o; 
               void prepare (void);
               void prepare_o (char *);
       public :
               ZER_PARTP_CLASS () : DB_CLASS (), cursor_o (-1)
               {
               }
               int dbreadfirst (void);
               int dbreadfirst_o (char *);
               int dbread_o (void);
               int dbclose_o (void);
              
};
#endif
