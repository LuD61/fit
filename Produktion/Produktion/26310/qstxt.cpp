#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "wmask.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "a_kun.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "qstxt.h"

struct QSTXT qstxt, qstxt_null;

void QSTXT_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &qstxt.qstyp, 1, 0);
            ins_quest ((char *) &qstxt.hwg, 1, 0);
            ins_quest ((char *) &qstxt.wg, 1, 0);
            ins_quest ((char *) &qstxt.ag, 1, 0);
    out_quest ((char *) &qstxt.qstyp,1,0);
    out_quest ((char *) qstxt.txt,0,25);
    out_quest ((char *) &qstxt.sort,1,0);
    out_quest ((char *) &qstxt.hwg,1,0);
    out_quest ((char *) &qstxt.wg,1,0);
    out_quest ((char *) &qstxt.ag,2,0);
    out_quest ((char *) qstxt.lief,0,17);
    out_quest ((char *) qstxt.mtxt1,0,37);
    out_quest ((char *) qstxt.mtxt2,0,37);
    out_quest ((char *) qstxt.mtxt3,0,37);
            cursor = prepare_sql ("select qstxt.qstyp,  "
"qstxt.txt,  qstxt.sort,  qstxt.hwg,  qstxt.wg,  qstxt.ag,  qstxt.lief,  "
"qstxt.mtxt1,  qstxt.mtxt2,  qstxt.mtxt3 from qstxt "

#line 29 "qstxt.rpp"
                                  "where qstyp = ? "
                                  "and   hwg = ? "
                                  "and   wg = ? "
                                  "and   ag = ? ");

    ins_quest ((char *) &qstxt.qstyp,1,0);
    ins_quest ((char *) qstxt.txt,0,25);
    ins_quest ((char *) &qstxt.sort,1,0);
    ins_quest ((char *) &qstxt.hwg,1,0);
    ins_quest ((char *) &qstxt.wg,1,0);
    ins_quest ((char *) &qstxt.ag,2,0);
    ins_quest ((char *) qstxt.lief,0,17);
    ins_quest ((char *) qstxt.mtxt1,0,37);
    ins_quest ((char *) qstxt.mtxt2,0,37);
    ins_quest ((char *) qstxt.mtxt3,0,37);
            sqltext = "update qstxt set qstxt.qstyp = ?,  "
"qstxt.txt = ?,  qstxt.sort = ?,  qstxt.hwg = ?,  qstxt.wg = ?,  "
"qstxt.ag = ?,  qstxt.lief = ?,  qstxt.mtxt1 = ?,  qstxt.mtxt2 = ?,  "
"qstxt.mtxt3 = ? "

#line 35 "qstxt.rpp"
                                  "where qstyp = ? "
                                  "and   hwg = ? "
                                  "and   wg = ? "
                                  "and   ag = ? ";

            ins_quest ((char *) &qstxt.qstyp, 1, 0);
            ins_quest ((char *) &qstxt.hwg, 1, 0);
            ins_quest ((char *) &qstxt.wg, 1, 0);
            ins_quest ((char *) &qstxt.ag, 1, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &qstxt.qstyp, 1, 0);
            ins_quest ((char *) &qstxt.hwg, 1, 0);
            ins_quest ((char *) &qstxt.wg, 1, 0);
            ins_quest ((char *) &qstxt.ag, 1, 0);
            test_upd_cursor = prepare_sql ("select lief from qstxt "
                                  "where qstyp = ? "
                                  "and   hwg = ? "
                                  "and   wg = ? "
                                  "and   ag = ? ");
            ins_quest ((char *) &qstxt.qstyp, 1, 0);
            ins_quest ((char *) &qstxt.hwg, 1, 0);
            ins_quest ((char *) &qstxt.wg, 1, 0);
            ins_quest ((char *) &qstxt.ag, 1, 0);
            del_cursor = prepare_sql ("delete from qstxt "
                                  "where qstyp = ? "
                                  "and   hwg = ? "
                                  "and   wg = ? "
                                  "and   ag = ? ");
    ins_quest ((char *) &qstxt.qstyp,1,0);
    ins_quest ((char *) qstxt.txt,0,25);
    ins_quest ((char *) &qstxt.sort,1,0);
    ins_quest ((char *) &qstxt.hwg,1,0);
    ins_quest ((char *) &qstxt.wg,1,0);
    ins_quest ((char *) &qstxt.ag,2,0);
    ins_quest ((char *) qstxt.lief,0,17);
    ins_quest ((char *) qstxt.mtxt1,0,37);
    ins_quest ((char *) qstxt.mtxt2,0,37);
    ins_quest ((char *) qstxt.mtxt3,0,37);
            ins_cursor = prepare_sql ("insert into qstxt ("
"qstyp,  txt,  sort,  hwg,  wg,  ag,  lief,  mtxt1,  mtxt2,  mtxt3) "

#line 65 "qstxt.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?)"); 

#line 67 "qstxt.rpp"
}

int QSTXT_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

