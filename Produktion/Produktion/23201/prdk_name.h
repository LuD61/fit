#ifndef _PRDK_NAME_DEF
#define _PRDK_NAME_DEF

#include "windows.h"
#include "mo_draw.h"
#include "wmask.h"
#include "dbclass.h"

struct PRDK_NAME {
   short     mdn;
   char      rez[9];
   char      rez_bz[73];
   short     delstatus;
};
extern struct PRDK_NAME prdk_name, prdk_name_null;

#line 10 "prdk_name.rh"

class PRDK_NAME_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               PRDK_NAME_CLASS () : DB_CLASS ()
               {
               }
};
#endif
