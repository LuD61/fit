#ifndef _QUID_DEF
#define _QUID_DEF

#include "windows.h"
#include "mo_draw.h"
#include "wmask.h"
#include "dbclass.h"

struct QUID {
   short     quid_nr;
   char      bezeichnung[81];
   char      quid_kat[4];
   short     fleisch_kz;
   short     fett_grenze;
};
extern struct QUID quid, quid_null;

#line 10 "quid.rh"

class QUID_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               QUID_CLASS () : DB_CLASS ()
               {
               }
               ~QUID_CLASS ()
               {
               }
};
#endif
