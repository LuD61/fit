#include <windows.h>
#include "wmaskc.h"
#include "searchvpk.h"
#include "strfkt.h"
#include "mo_wmess.h"
#include "lbox.h"
#include "dlglst.h"
#include "a_bas.h"


struct SVPK *SEARCHVPK::svpktab = NULL;
struct SVPK SEARCHVPK::svpk;
int SEARCHVPK::idx = -1;
long SEARCHVPK::vpkanz;
CHQEX *SEARCHVPK::Query = NULL;
DB_CLASS SEARCHVPK::DbClass; 
HINSTANCE SEARCHVPK::hMainInst;
HWND SEARCHVPK::hMainWindow;
HWND SEARCHVPK::awin;
int SEARCHVPK::SearchField = 1;
int SEARCHVPK::somat = 1; 
int SEARCHVPK::soa = 1; 
int SEARCHVPK::soa_bz1 = 1; 
short SEARCHVPK::mdn = 0; 


ITEM SEARCHVPK::ua        ("a",         "ArtikelNr.",    "", 0);  
ITEM SEARCHVPK::umat      ("mat",       "MaterialNr.",    "", 0);  
ITEM SEARCHVPK::ua_bz1    ("a_bz1" ,    "Bezeichnung",   "", 0);  


field SEARCHVPK::_UbForm[] = {
&ua,        15, 0, 0,  0,  NULL, "", BUTTON, 0, 0, 0,     
&ua_bz1,    26, 0, 0, 15 , NULL, "", BUTTON, 0, 0, 0,     
};

form SEARCHVPK::UbForm = {2, 0, 0, _UbForm, 0, 0, 0, 0, NULL};

ITEM SEARCHVPK::ia        ("a",         svpk.a,      "", 0);  
ITEM SEARCHVPK::imat      ("mat",       svpk.mat,    "", 0);  
ITEM SEARCHVPK::ia_bz1    ("a_bz1" ,    svpk.a_bz1,  "", 0);  

field SEARCHVPK::_DataForm[] = {
&ia,        13, 0, 0,  1,  NULL, "%13.0lf", DISPLAYONLY, 0, 0, 0,     
&ia_bz1,    24, 0, 0, 17 , NULL, "",        DISPLAYONLY, 0, 0, 0,     
};

form SEARCHVPK::DataForm = {2, 0, 0, _DataForm, 0, 0, 0, 0, NULL};

ITEM SEARCHVPK::iline ("", "1", "", 0);

field SEARCHVPK::_LineForm[] = {
&iline,       1, 0, 0, 15 , NULL, "", DISPLAYONLY, 0, 0, 0,     
};

form SEARCHVPK::LineForm = {1, 0, 0, _LineForm, 0, 0, 0, 0, NULL};

char *SEARCHVPK::query = NULL;

int SEARCHVPK::sortmat (const void *elem1, const void *elem2)
{
	      struct SVPK *el1; 
	      struct SVPK *el2; 

		  el1 = (struct SVPK *) elem1;
		  el2 = (struct SVPK *) elem2;
	      return ((atol (el1->mat) - atol (el2->mat)) * somat);
}

void SEARCHVPK::SortMat (HWND hWnd)
{
   	   qsort (svpktab, vpkanz, sizeof (struct SVPK),
				   sortmat);
       somat *= -1;
}


int SEARCHVPK::sorta (const void *elem1, const void *elem2)
{
	      struct SVPK *el1; 
	      struct SVPK *el2; 

		  el1 = (struct SVPK *) elem1;
		  el2 = (struct SVPK *) elem2;
          clipped (el1->a);
          clipped (el2->a);

          if (ratod (el1->a) > ratod (el2->a))
          {
              return 1 * soa;
          }
          else if (ratod (el1->a) < ratod (el2->a))
          {
              return -1 * soa;
          }

          return 0;
}

void SEARCHVPK::SortA (HWND hWnd)
{
   	   qsort (svpktab, vpkanz, sizeof (struct SVPK),
				   sorta);
       soa *= -1;
}

int SEARCHVPK::sorta_bz1 (const void *elem1, const void *elem2)
{
	      struct SVPK *el1; 
	      struct SVPK *el2; 

		  el1 = (struct SVPK *) elem1;
		  el2 = (struct SVPK *) elem2;
          clipped (el1->a_bz1);
          clipped (el2->a_bz1);
          if (strlen (el1->a_bz1) == 0 &&
              strlen (el2->a_bz1) == 0)
          {
              return 0;
          }
          if (strlen (el1->a_bz1) == 0)
          {
              return -1;
          }
          if (strlen (el2->a_bz1) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->a_bz1,el2->a_bz1) * soa_bz1);
}

void SEARCHVPK::SortABz1 (HWND hWnd)
{
   	   qsort (svpktab, vpkanz, sizeof (struct SVPK),
				   sorta_bz1);
       soa_bz1 *= -1;
}


void SEARCHVPK::SortLst (int Col, HWND hWnd)
/**
Procedure zum Sortiern der Liste.
**/
{
       switch (Col)
       {
              case 0 :
                  SortA (hWnd);
                  SearchField = 0;
                  break;
              case 1 :
                  SortMat (hWnd);
                  SearchField = 1;
                  break;
              case 2 :
                  SortABz1 (hWnd);
                  SearchField = 2;
                  break;
              default :
                  return;
       }
       UpdateList ();
}

void SEARCHVPK::FillFormat (char *buffer, int size)
{
        DlgLst::FillFrmAttr (&DataForm, buffer, size);
}


void SEARCHVPK::FillCaption (char *buffer, int size)
{
        DlgLst::FillFrmTitle (&UbForm, buffer, 0, size);
}

void SEARCHVPK::FillVlines (char *buffer, int size) 
{
        DlgLst::FillFrmLines (&LineForm, buffer, 0, size);
}


void SEARCHVPK::FillRec (char *buffer, int i, int size)
{
          DlgLst::FillFrmData (&DataForm, buffer, 0, size);
}


void SEARCHVPK::UpdateList (void)
{
       int i;
 	   char buffer [512];

       for (i = 0; i < vpkanz; i ++)
       {
		  memcpy (&svpk, &svpktab[i],  sizeof (struct SVPK));
          FillRec (buffer, i, 512);
	      Query->UpdateRecord (buffer, i);
       }
}

int SEARCHVPK::SearchLst (char *sebuff)
/**
Nach Kurzname in Liste suchen.
**/
{
/*
	   Query->SearchList ();
	   return 0;
*/
	   int i;
	   int len;
       char sabuff [80];
       char *pos;

	   if (svpktab == NULL) return 0;
	   if (strlen (sebuff) == 0) 
	   {
	       Query->SetSel (0);
		   return 0;
	   }

	   for (i = 0; i < vpkanz; i ++)
	   {
		   len = min (16, strlen (sebuff));
           if (SearchField == 0)
           {
                   strcpy (sabuff, svpktab[i].a);
                   for (pos = sabuff; *pos <= ' ' && *pos != 0 ; pos ++);
                   pos[len] = 0;
                   strcpy (sabuff, pos);
                   if (ratod (sebuff) == ratod (sabuff)) break;
           }
           else if (SearchField == 1)
           {
                   strcpy (sabuff, svpktab[i].mat);
                   for (pos = sabuff; *pos <= ' ' && *pos != 0 ; pos ++);
                   pos[len] = 0;
                   strcpy (sabuff, pos);
                   if (atol (sebuff) == atol (sabuff)) break;
           }
           else if (SearchField == 2)
           {
		           if (strupcmp (sebuff, svpktab[i].a_bz1, len) == 0) break;
           }
	   }
	   if (i == vpkanz) return 0;
	   Query->SetSel (i);
	   return 0;
}


int SEARCHVPK::Read (char *squery)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int cursor;
	  int i;
      WMESS Wmess;


	  if (svpktab) 
	  {
           delete svpktab;
           svpktab = NULL;
	  }


      idx = -1;
	  vpkanz = 0;
      SearchField = 2;
      Query->SetSBuff (NULL);
      clipped (squery);
      if (strcmp (squery, " ") == 0) squery[0] = 0;

      if (vpkanz == 0) vpkanz = 0x10000;

	  svpktab = new struct SVPK [vpkanz];
      if (squery == NULL || 
          strlen (squery) == 0)
      {
             sprintf (buffer, "select a_bas.a, a_bz1 "
	 			       "from a_bas "
                       "where a > 0 "
                       "and (a_bas.a_typ in (8,9,10) or "
                       "     a_bas.a_typ2 in (8,9,10))");
              if (query != NULL && strlen (query) > 0)
              {
                  strcat (buffer, " ");
                  strcat (buffer, query);
              }

              DbClass.sqlout ((char *) svpk.a, 0, 14);
//              DbClass.sqlout ((char *) svpk.mat, 0, 9);
              DbClass.sqlout ((char *) svpk.a_bz1, 0, 25);
              cursor = DbClass.sqlcursor (buffer);
      }
      else
      {
             sprintf (buffer, "select a_bas.a, a_bz1 "
	 			       "from a_bas "
                       "where a > 0 "
                       "and (a_bas.a_typ in (8,9,10) or "
                       "     a_bas.a_typ2 in (8,9,10)) "
                       "and a_bez matches \"%s*\"", squery); 
              if (query != NULL && strlen (query) > 0)
              {
                  strcat (buffer, " ");
                  strcat (buffer, query);
              }
              DbClass.sqlout ((char *) svpk.a, 0, 14);
//              DbClass.sqlout ((char *) svpk.mat, 0, 9);
              DbClass.sqlout ((char *) svpk.a_bz1, 0, 25);
              cursor = DbClass.sqlcursor (buffer);
      }
      if (cursor < 0) 
      {
          return -1;
      }

  	  i = 0;
	  while (DbClass.sqlfetch (cursor) == 0)
      {
		  memcpy (&svpktab[i], &svpk, sizeof (struct SVPK));
          FillRec (buffer, i, 512);
	      Query->InsertRecord (buffer);
		  i ++;
	  }
      vpkanz = i;
	  DbClass.sqlclose (cursor);

      soa_bz1 = 1;
      if (vpkanz > 1)
      {
             SortABz1 (Query->GethWnd ());
      }
      UpdateList ();

	  EnableWindow (awin, FALSE);
	  return 0;
}

void SEARCHVPK::FillBox (void)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int i;

      for (i = 0; i < vpkanz; i ++)
      {
		  memcpy (&svpk, &svpktab[i],  sizeof (struct SVPK));
          FillRec (buffer, i, 512);
	      Query->InsertRecord (buffer);
      }
      if (idx > -1)
      {
	      Query->SetSel (idx);
      }
}


void SEARCHVPK::SetParams (HINSTANCE hMainInst, HWND hMainWindow)
{
      this->hMainInst   = hMainInst;
      this->hMainWindow = hMainWindow;
      awin = hMainWindow;
}


void SEARCHVPK::Search (void)
{
  	  int cx, cy;
	  char buffer [512];
      RECT rect;
      BOOL doret = FALSE; 

      Settchar ('|');
      GetClientRect (hMainWindow, &rect);
      cx = 80;
      cy = 25;
//      EnableWindow (hMainWindow, FALSE);
      SetSortProc (SortLst);
      if (Query == NULL)
      {
        idx = -1;
        if (query != NULL && strlen (query) > 0)
        {
               Query = new CHQEX (cx, cy);
        }
        else
        {
               Query = new CHQEX (cx, cy);
//               Query = new CHQEX (cx, cy, "Name", "");
        }
        Query->EnableSort (TRUE);
        if (SetStyle)
        {
            Query->SetWindowStyle (WindowStyle);
        }
        Query->OpenWindow (hMainInst, hMainWindow, TRUE);
        FillFormat (buffer, 512);
        Query->RowAttr (buffer);
        FillVlines (buffer, 512);
	    Query->VLines (buffer, 3);
	    EnableWindow (awin, FALSE);

        FillCaption (buffer, 512);
	    Query->InsertCaption (buffer);

	    Query->SetFillDb (Read);
	    Query->SetSearchLst (SearchLst);
//        if (query != NULL && strlen (query) > 0)
        {
              Read ("");
              if (vpkanz == 0)
              {
                  doret = TRUE;
              }
        }
      }
      else
      {
        Query->RestorefWork ();
        if (SetStyle)
        {
            Query->SetWindowStyle (WindowStyle);
        }
        Query->OpenWindow (hMainInst, hMainWindow, TRUE);
        FillFormat (buffer, 512);
        Query->RowAttr (buffer);
        FillVlines (buffer, 512);
	    Query->VLines (buffer, 3);
	    EnableWindow (awin, FALSE);

        FillCaption (buffer, 512);
	    Query->InsertCaption (buffer);

	    Query->SetFillDb (Read);
	    Query->SetSearchLst (SearchLst);
        FillBox ();
      }
      if (doret == FALSE)
      {
          Query->ProcessMessages ();
          idx = Query->GetSel ();
      }
      else
      {
          idx = -1;
      }
      query = NULL;
      EnableWindow (awin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (awin);
      if (svpktab == NULL)
      {
          delete Query;
          Query = NULL;
      }
	  if (idx == -1) return;
	  memcpy (&svpk, &svpktab[idx], sizeof (svpk));
      SetSortProc (NULL);
      if (svpktab != NULL)
      {
          Query->SavefWork ();
      }
}



