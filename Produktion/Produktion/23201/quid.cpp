#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "wmaskc.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "quid.h"

struct QUID quid, quid_null;

void QUID_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &quid.quid_nr, 1, 0);
    out_quest ((char *) &quid.quid_nr,1,0);
    out_quest ((char *) quid.bezeichnung,0,81);
    out_quest ((char *) quid.quid_kat,0,4);
    out_quest ((char *) &quid.fleisch_kz,1,0);
    out_quest ((char *) &quid.fett_grenze,1,0);
            cursor = prepare_sql ("select quid.quid_nr,  "
"quid.bezeichnung,  quid.quid_kat,  quid.fleisch_kz,  quid.fett_grenze from quid "

#line 20 "quid.rpp"
                                  "where quid_nr = ?");
    ins_quest ((char *) &quid.quid_nr,1,0);
    ins_quest ((char *) quid.bezeichnung,0,81);
    ins_quest ((char *) quid.quid_kat,0,4);
    ins_quest ((char *) &quid.fleisch_kz,1,0);
    ins_quest ((char *) &quid.fett_grenze,1,0);
            sqltext = "update quid set quid.quid_nr = ?,  "
"quid.bezeichnung = ?,  quid.quid_kat = ?,  quid.fleisch_kz = ?,  "
"quid.fett_grenze = ? "

#line 22 "quid.rpp"
                                  "where quid_nr = ?";
            ins_quest ((char *) &quid.quid_nr, 1, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &quid.quid_nr, 1, 0);
            test_upd_cursor = prepare_sql ("select quid_nr from quid "
                                  "where quid_nr = ?");
            ins_quest ((char *) &quid.quid_nr, 1, 0);
            del_cursor = prepare_sql ("delete from quid "
                                  "where quid_nr = ?");
    ins_quest ((char *) &quid.quid_nr,1,0);
    ins_quest ((char *) quid.bezeichnung,0,81);
    ins_quest ((char *) quid.quid_kat,0,4);
    ins_quest ((char *) &quid.fleisch_kz,1,0);
    ins_quest ((char *) &quid.fett_grenze,1,0);
            ins_cursor = prepare_sql ("insert into quid ("
"quid_nr,  bezeichnung,  quid_kat,  fleisch_kz,  fett_grenze) "

#line 33 "quid.rpp"
                                      "values "
                                      "(?,?,?,?,?)");

#line 35 "quid.rpp"
}

