#include <windows.h>
#include "wmaskc.h"
#include "searchrez.h"
#include "strfkt.h"
#include "mo_wmess.h"
#include "lbox.h"
#include "dlglst.h"
#include "a_bas.h"
#include "lbox.h"


struct SREZ *SEARCHREZ::sreztab = NULL;
struct SREZ SEARCHREZ::srez;
int SEARCHREZ::idx = -1;
long SEARCHREZ::rezanz;
CHQEX *SEARCHREZ::Query = NULL;
DB_CLASS SEARCHREZ::DbClass; 
HINSTANCE SEARCHREZ::hMainInst;
HWND SEARCHREZ::hMainWindow;
HWND SEARCHREZ::awin;
int SEARCHREZ::SearchField = 1;
int SEARCHREZ::sorez = 1; 
int SEARCHREZ::sorez_bz = 1; 
int SEARCHREZ::soa = 1; 
int SEARCHREZ::soa_bz1 = 1; 
int SEARCHREZ::sovariante = 1; 
short SEARCHREZ::mdn = 0; 


ITEM SEARCHREZ::urez      ("rez",       "Rezeptur",      "", 0);  
ITEM SEARCHREZ::urez_bz   ("rez_bz",    "Bezeichnung",   "", 0);  
ITEM SEARCHREZ::ua        ("a",         "Artikel",       "", 0);  
ITEM SEARCHREZ::ua_bz1    ("a_bz1",     "Bezeichnung",   "", 0);  
ITEM SEARCHREZ::uvariante ("variante",  "Variante",      "", 0);  


field SEARCHREZ::_UbForm[] = {
&urez,      10, 0, 0,  0,  NULL, "", BUTTON, 0, 0, 0,     
&urez_bz,   26, 0, 0, 10 , NULL, "", BUTTON, 0, 0, 0,     
&ua,        15, 0, 0, 36 , NULL, "", BUTTON, 0, 0, 0,     
&ua_bz1,    26, 0, 0, 51 , NULL, "", BUTTON, 0, 0, 0,     
&uvariante, 10, 0, 0, 77 , NULL, "", BUTTON, 0, 0, 0,     
};

form SEARCHREZ::UbForm = {5, 0, 0, _UbForm, 0, 0, 0, 0, NULL};


ITEM SEARCHREZ::irez      ("rez",       srez.rez,        "", 0);  
ITEM SEARCHREZ::irez_bz   ("rez_bz",    srez.rez_bz,     "", 0);  
ITEM SEARCHREZ::ia        ("a",         srez.a,          "", 0);  
ITEM SEARCHREZ::ia_bz1    ("a_bz1",     srez.a_bz1,      "", 0);  
ITEM SEARCHREZ::ivariante ("variante",  srez.variante,   "", 0);  

field SEARCHREZ::_DataForm[] = {
&irez,       8, 0, 0,  1,  NULL, "",       DISPLAYONLY, 0, 0, 0,     
&irez_bz,   24, 0, 0, 11 , NULL, "",       DISPLAYONLY, 0, 0, 0,     
&ia,        13, 0, 0, 37 , NULL, "%13.0f", DISPLAYONLY, 0, 0, 0,     
&ia_bz1,    24, 0, 0, 52 , NULL, "",       DISPLAYONLY, 0, 0, 0,     
&ivariante, 20, 0, 0, 81 , NULL, "",     DISPLAYONLY, 0, 0, 0,     
};

form SEARCHREZ::DataForm = {5, 0, 0, _DataForm, 0, 0, 0, 0, NULL};

ITEM SEARCHREZ::iline ("", "1", "", 0);

field SEARCHREZ::_LineForm[] = {
&iline,       1, 0, 0, 10 , NULL, "", DISPLAYONLY, 0, 0, 0,     
&iline,       1, 0, 0, 36 , NULL, "", DISPLAYONLY, 0, 0, 0,     
&iline,       1, 0, 0, 51 , NULL, "", DISPLAYONLY, 0, 0, 0,     
&iline,       1, 0, 0, 77 , NULL, "", DISPLAYONLY, 0, 0, 0,     
};

form SEARCHREZ::LineForm = {4, 0, 0, _LineForm, 0, 0, 0, 0, NULL};

char *SEARCHREZ::query = NULL;

int SEARCHREZ::sortrez (const void *elem1, const void *elem2)
{
	      struct SREZ *el1; 
	      struct SREZ *el2; 

		  el1 = (struct SREZ *) elem1;
		  el2 = (struct SREZ *) elem2;
          clipped (el1->rez);
          clipped (el2->rez);
          if (strlen (el1->rez) == 0 &&
              strlen (el2->rez) == 0)
          {
              return 0;
          }
          if (strlen (el1->rez) == 0)
          {
              return -1;
          }
          if (strlen (el2->rez) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->rez,el2->rez) * sorez);
}

void SEARCHREZ::SortRez (HWND hWnd)
{
   	   qsort (sreztab, rezanz, sizeof (struct SREZ),
				   sortrez);
       sorez *= -1;
}

int SEARCHREZ::sortrez_bz (const void *elem1, const void *elem2)
{
	      struct SREZ *el1; 
	      struct SREZ *el2; 

		  el1 = (struct SREZ *) elem1;
		  el2 = (struct SREZ *) elem2;
          clipped (el1->rez_bz);
          clipped (el2->rez_bz);
          if (strlen (el1->rez_bz) == 0 &&
              strlen (el2->rez_bz) == 0)
          {
              return 0;
          }
          if (strlen (el1->rez_bz) == 0)
          {
              return -1;
          }
          if (strlen (el2->rez_bz) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->rez_bz,el2->rez_bz) * sorez_bz);
}

void SEARCHREZ::SortRezBz (HWND hWnd)
{
   	   qsort (sreztab, rezanz, sizeof (struct SREZ),
				   sortrez_bz);
       sorez_bz *= -1;
}

int SEARCHREZ::sorta (const void *elem1, const void *elem2)
{
	      struct SREZ *el1; 
	      struct SREZ *el2; 

		  el1 = (struct SREZ *) elem1;
		  el2 = (struct SREZ *) elem2;
          clipped (el1->a);
          clipped (el2->a);

          if (ratod (el1->a) > ratod (el2->a))
          {
              return 1 * soa;
          }
          else if (ratod (el1->a) < ratod (el2->a))
          {
              return -1 * soa;
          }

          return 0;
/*
          if (strlen (el1->a) == 0 &&
              strlen (el2->a) == 0)
          {
              return 0;
          }
          if (strlen (el1->a) == 0)
          {
              return -1;
          }
          if (strlen (el2->a) == 0)
          {
              return 1;
          }

	      return (strcmp (el1->a,el2->a) * soa);
*/
}

void SEARCHREZ::SortA (HWND hWnd)
{
   	   qsort (sreztab, rezanz, sizeof (struct SREZ),
				   sorta);
       soa *= -1;
}

int SEARCHREZ::sorta_bz1 (const void *elem1, const void *elem2)
{
	      struct SREZ *el1; 
	      struct SREZ *el2; 

		  el1 = (struct SREZ *) elem1;
		  el2 = (struct SREZ *) elem2;
          clipped (el1->a_bz1);
          clipped (el2->a_bz1);
          if (strlen (el1->a_bz1) == 0 &&
              strlen (el2->a_bz1) == 0)
          {
              return 0;
          }
          if (strlen (el1->a_bz1) == 0)
          {
              return -1;
          }
          if (strlen (el2->a_bz1) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->a_bz1,el2->a_bz1) * soa_bz1);
}

void SEARCHREZ::SortABz1 (HWND hWnd)
{
   	   qsort (sreztab, rezanz, sizeof (struct SREZ),
				   sorta_bz1);
       soa_bz1 *= -1;
}

int SEARCHREZ::sortvariante (const void *elem1, const void *elem2)
{
	      struct SREZ *el1; 
	      struct SREZ *el2; 

		  el1 = (struct SREZ *) elem1;
		  el2 = (struct SREZ *) elem2;
          return (atoi (el1->variante) - atoi (el2->variante)) * sovariante;
/*
          clipped (el1->variante);
          clipped (el2->variante);
          if (strlen (el1->variante) == 0 &&
              strlen (el2->variante) == 0)
          {
              return 0;
          }
          if (strlen (el1->variante) == 0)
          {
              return -1;
          }
          if (strlen (el2->variante) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->variante,el2->variante) * sovariante);
*/
}

void SEARCHREZ::SortVariante (HWND hWnd)
{
   	   qsort (sreztab, rezanz, sizeof (struct SREZ),
				   sortvariante);
       sovariante *= -1;
}

void SEARCHREZ::SortLst (int Col, HWND hWnd)
/**
Procedure zum Sortiern der Liste.
**/
{
       switch (Col)
       {
              case 0 :
                  SortRez (hWnd);
                  SearchField = 0;
                  break;
              case 1 :
                  SortRezBz (hWnd);
                  SearchField = 1;
                  break;
              case 2 :
                  SortA (hWnd);
                  SearchField = 2;
                  break;
              case 3 :
                  SortABz1 (hWnd);
                  SearchField = 3;
                  break;
              case 4 :
                  SortVariante (hWnd);
                  SearchField = 4;
                  break;
              default :
                  return;
       }
       UpdateList ();
}

void SEARCHREZ::FillFormat (char *buffer, int size)
{
        DlgLst::FillFrmAttr (&DataForm, buffer, size);
}


void SEARCHREZ::FillCaption (char *buffer, int size)
{
        DlgLst::FillFrmTitle (&UbForm, buffer, 0, size);
}

void SEARCHREZ::FillVlines (char *buffer, int size) 
{
        DlgLst::FillFrmLines (&LineForm, buffer, 0, size);
}


void SEARCHREZ::FillRec (char *buffer, int i, int size)
{
          DlgLst::FillFrmData (&DataForm, buffer, 0, size);
}


void SEARCHREZ::UpdateList (void)
{
       int i;
 	   char buffer [512];

       for (i = 0; i < rezanz; i ++)
       {
		  memcpy (&srez, &sreztab[i],  sizeof (struct SREZ));
          FillRec (buffer, i, 512);
	      Query->UpdateRecord (buffer, i);
       }
}

int SEARCHREZ::SearchLst (char *sebuff)
/**
Nach Kurzname in Liste suchen.
**/
{
/*
	   Query->SearchList ();
	   return 0;
*/

	   int i;
	   int len;
       char sabuff [80];
       char *pos;

	   if (sreztab == NULL) return 0;
	   if (strlen (sebuff) == 0) 
	   {
	       Query->SetSel (0);
		   return 0;
	   }

	   for (i = 0; i < rezanz; i ++)
	   {
		   len = min (16, strlen (sebuff));
           if (SearchField == 0)
           {
		           if (strupcmp (sebuff, sreztab[i].rez, len) == 0) break;
           }
           else if (SearchField == 1)
           {
		           if (strupcmp (sebuff, sreztab[i].rez_bz, len) == 0) break;
           }
           else if (SearchField == 2)
           {
                   strcpy (sabuff, sreztab[i].a);
                   for (pos = sabuff; *pos <= ' ' && *pos != 0 ; pos ++);
                   pos[len] = 0;
                   strcpy (sabuff, pos);
                   if (ratod (sebuff) == ratod (sabuff)) break;
           }
           else if (SearchField == 3)
           {
		           if (strupcmp (sebuff, sreztab[i].a_bz1, len) == 0) break;
           }
           if (SearchField == 4)
           {
                   strcpy (sabuff, sreztab[i].variante);
                   for (pos = sabuff; *pos <= ' ' && *pos != 0 ; pos ++);
                   pos[len] = 0;
                   strcpy (sabuff, pos);
                   if (atoi (sebuff) == atoi (sabuff)) break;
           }
	   }
	   if (i == rezanz) return 0;
	   Query->SetSel (i);
	   return 0;
}


int SEARCHREZ::Read (char *squery)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int cursor;
	  int i;
      WMESS Wmess;


	  if (sreztab) 
	  {
           delete sreztab;
           sreztab = NULL;
	  }


      idx = -1;
	  rezanz = 0;
      SearchField = 1;
      Query->SetSBuff (NULL);
      clipped (squery);
      if (strcmp (squery, " ") == 0) squery[0] = 0;

      if (rezanz == 0) rezanz = 0x10000;

	  sreztab = new struct SREZ [rezanz];
      if (squery == NULL || 
          strlen (squery) == 0)
      {
             sprintf (buffer, "select rez, rez_bz, a, variante,variante_bz "
	 			       "from prdk_k "
                       "where mdn = %hd ", mdn); 
              if (query != NULL && strlen (query) > 0)
              {
                  strcat (buffer, " ");
                  strcat (buffer, query);
              }

              DbClass.sqlout ((char *) srez.rez, 0, 9);
              DbClass.sqlout ((char *) srez.rez_bz, 0, 73);
              DbClass.sqlout ((char *) srez.a, 0, 14);
              DbClass.sqlout ((char *) srez.variante, 0, 5);
              DbClass.sqlout ((char *) srez.variante_bz, 0, 24);
              cursor = DbClass.sqlcursor (buffer);
      }
      else
      {
             sprintf (buffer, "select rez, rez_bz, a, variante,variante_bz "
	 			       "from prdk_k "
                       "where mdn = %hd "
                       "and rez_bz matches \"%s*\"", mdn, squery); 
              if (query != NULL && strlen (query) > 0)
              {
                  strcat (buffer, " ");
                  strcat (buffer, query);
              }
              DbClass.sqlout ((char *) srez.rez, 0, 9);
              DbClass.sqlout ((char *) srez.rez_bz, 0, 73);
              DbClass.sqlout ((char *) srez.a, 0, 14);
              DbClass.sqlout ((char *) srez.variante, 0, 5);
              DbClass.sqlout ((char *) srez.variante_bz, 0, 24);
              cursor = DbClass.sqlcursor (buffer);
      }
      if (cursor < 0) 
      {
          return -1;
      }

  	  i = 0;
	  while (DbClass.sqlfetch (cursor) == 0)
      {
          strcpy (_a_bas.a_bz1, "");
          lese_a_bas (ratod (srez.a));
          strcpy (srez.a_bz1, _a_bas.a_bz1);
		  sprintf(srez.variante, "%s %s",clipped(srez.variante),clipped(srez.variante_bz));
		  memcpy (&sreztab[i], &srez, sizeof (struct SREZ));
          FillRec (buffer, i, 512);
	      Query->InsertRecord (buffer);
		  i ++;
	  }
      rezanz = i;
	  DbClass.sqlclose (cursor);

      sorez = 1;
      if (rezanz > 1)
      {
             SortRez (Query->GethWnd ());
      }
      UpdateList ();

	  EnableWindow (awin, FALSE);
	  return 0;
}

void SEARCHREZ::FillBox (void)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int i;

      for (i = 0; i < rezanz; i ++)
      {
		  memcpy (&srez, &sreztab[i],  sizeof (struct SREZ));
          FillRec (buffer, i, 512);
	      Query->InsertRecord (buffer);
      }
      if (idx > -1)
      {
	      Query->SetSel (idx);
      }
}


void SEARCHREZ::SetParams (HINSTANCE hMainInst, HWND hMainWindow)
{
      this->hMainInst   = hMainInst;
      this->hMainWindow = hMainWindow;
      awin = hMainWindow;
}


void SEARCHREZ::Search (short mdn)
{
  	  int cx, cy;
	  char buffer [512];
      RECT rect;
      BOOL doret = FALSE; 

      this->mdn = mdn;
      DlgLst::TChar = (UCHAR) 0xFF;
//      DlgLst::TChar = '|';
      Settchar ((char) DlgLst::TChar);
      GetClientRect (hMainWindow, &rect);
      cx = 100;
      cy = 25;
      SetSortProc (SortLst);
      if (Query == NULL)
      {
        idx = -1;
        if (query != NULL && strlen (query) > 0)
        {
               Query = new CHQEX (cx, cy);
        }
        else
        {
//               Query = new CHQEX (cx, cy);
               Query = new CHQEX (cx, cy, "Name", "");
        }
        Query->EnableSort (TRUE);
        if (SetStyle)
        {
            Query->SetWindowStyle (WindowStyle);
        }
        Query->OpenWindow (hMainInst, hMainWindow, TRUE);
        FillFormat (buffer, 512);
        Query->RowAttr (buffer);
        FillVlines (buffer, 512);
	    Query->VLines (buffer, 3);
	    EnableWindow (awin, FALSE);

        FillCaption (buffer, 512);
	    Query->InsertCaption (buffer);

	    Query->SetFillDb (Read);
	    Query->SetSearchLst (SearchLst);
        if (query != NULL && strlen (query) > 0)
        {
              Read ("");
              if (rezanz == 0)
              {
                  doret = TRUE;
              }
        }
      }
      else
      {
        Query->RestorefWork ();
        if (SetStyle)
        {
            Query->SetWindowStyle (WindowStyle);
        }
        Query->OpenWindow (hMainInst, hMainWindow, TRUE);
        FillFormat (buffer, 512);
        Query->RowAttr (buffer);
        FillVlines (buffer, 512);
	    Query->VLines (buffer, 3);
	    EnableWindow (awin, FALSE);

        FillCaption (buffer, 512);
	    Query->InsertCaption (buffer);

	    Query->SetFillDb (Read);
	    Query->SetSearchLst (SearchLst);
        FillBox ();
      }
      if (doret == FALSE)
      {
          Query->ProcessMessages ();
          idx = Query->GetSel ();
      }
      else
      {
          idx = -1;
      }
      query = NULL;
      EnableWindow (awin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (awin);
      if (sreztab == NULL)
      {
          delete Query;
          Query = NULL;
      }
	  if (idx == -1) return;
	  memcpy (&srez, &sreztab[idx], sizeof (srez));
      SetSortProc (NULL);
      if (sreztab != NULL)
      {
          Query->SavefWork ();
      }
}



