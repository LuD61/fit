#ifndef _WORK_DEF
#define _WORK_DEF
#include "prdk_k.h"
#include "prdk_name.h"
#include "reportrez.h"
#include "prdk_varb.h"
#include "prdk_zut.h"
#include "prdk_huel.h"
#include "prdk_vpk.h"
#include "prdk_kost.h"
#include "prdk_zeit.h"
#include "prdk_quid.h"
#include "quid.h"
#include "ptab.h"
#include "mdn.h"
#include "mo_auto.h"
#include "auto_nr.h"
#include "mo_txt.h"
#include "a_bas.h"
#include "a_hndw.h"
#include "a_preis.h"
#include "a_kalk_eig.h"
#include "a_kalkpreis.h"
#include "a_pr.h"
#include "a_kalkhndw.h"
#include "listenter.h"

#define REZTYP 2
#define ABTYP 3   //Aschnitte
#define REWORKTYP 4 //Reworkartikel f�r Chargierung (M�fli)
static char *VERSION = "22.06.09";


struct VARBLSTS
{
    char typ[40];
    short typ_kz;
    double a;
    char nr[40];
    char a_bz1 [25];
    char kutf[40];
    char basis[40];
    short basis_kz;
    char anteil_proz[40];
    char varb_gew_nto[40];
    char varb_me[40];
    char me_einh [10];
    char pr_ek[40];
    char kosten [40];
    char preisme [40];
    char text [82];
    ListButton chtyp;
    ListButton chnr;
    ListButton chbasis;
    double gewek;
    double prek;
    double a_gew;
};

struct A_LSTS
{
    double a;
    char nr[40];
    char rez [10];
    char a_bz1 [25];
    char pr_ek[20];
    char plan_ek[20];
    char variante[10];
    char akv[10];
};

struct ZUTLSTS
{
    char typ[40];
    char basis[40];
    short typ_kz;
    short basis_kz;
    double a;
    char nr[40];
    char a_bz1 [25];
    char kutf[40];
    char zut_me[40];
    char me_einh [10];
    char pr_ek[40];
    char kosten [40];
    char zug_me [15];
    char preisme [40];
    char text [82];
    ListButton chtyp;
    ListButton chbasis;
    ListButton chnr;
    double gewek;
    double prek;
    double a_gew;
};

struct HUELLSTS
{
    char typ[40];
    short typ_kz;
    double a;
    char nr[40];
    char a_bz1 [25];
    char kutf[40];
    char fuell_me [40];
    char huel_me[40];
    char me_einh [10];
    char pr_ek[40];
    char me_buch [5];
    char kosten [40];
    char preisme [40];
    ListButton chtyp;
    ListButton chnr;
    ListButton mebuch;
    double gewek;
    double prek;
    double a_gew;
};


struct VPKLSTS
{
    char typ[40];
    short typ_kz;
    double a;
    char nr[40];
    char a_bz1 [25];
    char kutf[40];
    char fuell_me [40];
    char vpk_me[40];
    char me_einh [10];
    char pr_ek[40];
    char kosten [40];
    char preisme [40];
    ListButton chtyp;
    ListButton chnr;
    double gewek;
    double prek;
    double a_gew;
};

struct KOSTLSTS
{
    short stufe;
    char stufe_bez [40];
    char kost_art[40];
    char kost_art_bez [25];
    char kost_wert[40];
    char me_einh [10];
    char kosten [40];
    char kostenek [40];
    long vkost_rechb;
    double kost_wrt;
    double faktor;
    ListButton chtyp;
    ListButton chnr;
};

struct ZEITLSTS
{
	short schritt;
	short lfd;
    char prod_schritt [40];
    char text [40];
    char kost_min[40];
    char kosten [25];
    char zeit_fix [40];
    char zeit_spfix [10];
    char zeit_var [40];
    char kostenek [40];
    char su_zeit [40];
    char su_kost [40];
	short c1;
	double m1;
	short c2;
	double m2;
    ListButton chtyp;
    ListButton chnr;
};

struct QUIDLSTS
{
    char quid_kat [40];
	char quid_nr [20];
    char quid_bez [40];
	char quid_proz [20];
    ListButton chtyp;
    ListButton chnr;
};

struct PRDK_KF
{
      char rework [10];
      char akv [10];
      char nakv [10];
      char mdn [7];
      char mdn_krz [20];
      char pageinfo [40];
      char a [15];
      char a_bz1 [30];
      char a_bz2 [30];
      char rez [12];
      char rez_bz [75];
      char variante [6];
      char variante_bz [26];
      char sw_kalk [10];
      char sw_analy [10];
      char chg_gew [12];
      char ek [10];
      char plan_ek [10];
      char a_bis [15];
      char a_von [15];
      char ag_bis [15];
      char ag_von [15];
	  char zeit_m1 [20];
	  char zeit_c1 [20];
	  char zeit_m2 [20];
	  char zeit_c2 [20];
	  char zeit_p1 [20];
	  char zeit_p10 [20];
	  char zeit_p2 [20];
	  char zeit_p20 [20];
	  char zeit_p3 [20];
	  char zeit_p30[20];
	  char zeit_p4 [20];
	  char zeit_p40 [20];
	  char zeit_p5 [20];
	  char zeit_p50[20];
	  char quid_kat[50];
	  char hinweis_fett[100];
	  char hinweis_be[100];
	  char hinweis[100];
	  char warnung3[100];
	  char warnung4[100];
	  char warnung1[100];
	  char warnung2[100];
	  char fett_rel_max[10];
	  char be_rel_max[10];
	  char fett_rel_ist[10];
	  char be_rel_ist[10];
	  char vorlaufzeit[10];
	  char hinweis2[100];
	  char prod_abt[10];
//	  char prod_abt_bz[30];
};
extern struct PRDK_KF prdk_kf, prdk_kf_null;

		static short dmdn;
		static double da_von;
		static double da_bis;
		static int dag_von;
		static int dag_bis;
		static short dakv;
		static short dnakv;

class Work
{
    private :
        PRDK_K_CLASS Prdk_k;
        PRDK_NAME_CLASS Prdk_name;
        PRDK_VARB_CLASS Prdk_varb;
        A_PREIS_CLASS A_preis;
        PRDK_ZUT_CLASS Prdk_zut;
        PRDK_HUEL_CLASS Prdk_huel;
        PRDK_VPK_CLASS Prdk_vpk;
        PRDK_KOST_CLASS Prdk_kost;
        PRDK_ZEIT_CLASS Prdk_zeit;
        PRDK_QUID_CLASS Prdk_quid;
        QUID_CLASS Quid;
        A_PREIS_CLASS APreis;
        A_KALK_EIG_CLASS AKalkEig;
        A_KALKPREIS_CLASS AKalkPreis;
        A_PR_CLASS APr;
        A_KALKHNDW_CLASS AKalkHndw;
        PRDK_QUID_CLASS PrdkQuid;
        REPORTREZ_CLASS ReportRez;
        PTAB_CLASS Ptab;
        MDN_CLASS Mdn;
        ADR_CLASS Adr;
        AUTO_CLASS AutoClass;
        int kunlief_cursor;
        int a_cursor;
        int varb_cursor;
        int varb_cursor1;
        int varb_cursor2;
        int varb_cursor3;
        int kopf_cursor2;
        int kopf_cursor3;
        int cursor_quid;
        int zut_cursor;
		int kalk_cursor;
        int huel_cursor;
        int vpk_cursor;
        int kost_cursor;
        int zeit_cursor;
        int quid_cursor;
        static long adr_start;
        static long adr_end;
        static long adr_start_hand;
        static long adr_end_hand;
        long adr_start_p;
        long adr_end_p;
        HNDW_CLASS Hndw;
        LISTENTER *ListDlg;
        BOOL PlanEk;
        double kosteneinh;
        double kostenkg;
        double RezGew;
        double HuelWrt;
        double HuelHkWrt;
        double VpkWrt;
        double VpkHkWrt;

        double SuKostVarb;
        double SuKostZut;
        double SuKostHuel;
        double SuKostVpk;
        double SuKostSw;
        double SuKostHK;
        double SuKostAufs;
        double SuKostSK;
        double SuKostFEK;
        double SuKostFVK;
        double SuKostAufsEk;
        double chgfaktor;
        BOOL GBVorhanden;

        struct VARBLSTS *VarbLsts;
        struct A_LSTS *A_Lsts;
        struct ZUTLSTS *ZutLsts;
        struct HUELLSTS *HuelLsts;
        struct VPKLSTS *VpkLsts;
        struct KOSTLSTS *KostLsts;
        struct ZEITLSTS *ZeitLsts;
        struct QUIDLSTS *QuidLsts;
    public :
        Work () : varb_cursor (-1), 
                  zut_cursor (-1), 
                  kalk_cursor (-1), 
                  huel_cursor (-1), 
                  varb_cursor1 (-1), 
                  varb_cursor2 (-1), 
                  varb_cursor3 (-1), 
                  kopf_cursor2 (-1), 
                  kopf_cursor3 (-1), 
                  cursor_quid (-1), 
                  a_cursor (-1), 
                  PlanEk (FALSE),
                  RezGew (0.0),
                  HuelWrt (0.0),
                  HuelHkWrt (0.0),
                  VpkWrt (0.0),
                  VpkHkWrt (0.0),
                  SuKostVarb (0.0),
                  SuKostZut (0.0),
                  SuKostHuel (0.0),
                  SuKostVpk (0.0),
                  SuKostHK (0.0),
                  SuKostAufs (0.0),
                  SuKostSK (0.0),
                  SuKostFEK (0.0),
                  SuKostFVK (0.0),
                  SuKostAufsEk (0.0),
                  chgfaktor (0.0)

        {
        }

        ~Work ()
        {
            if (varb_cursor != -1)
            {
                Prdk_k.sqlclose (varb_cursor);
            }
            if (zut_cursor != -1)
            {
                Prdk_k.sqlclose (zut_cursor);
            }
            if (kalk_cursor != -1)
            {
                Prdk_k.sqlclose (kalk_cursor);
            }
            if (huel_cursor != -1)
            {
                Prdk_k.sqlclose (huel_cursor);
            }
            if (cursor_quid != -1)
            {
                Prdk_k.sqlclose (cursor_quid);
            }
        }

        void SetKostenEinh (double kosteneinh)
        {
            this->kosteneinh = kosteneinh;
        }

        double GetKostenEinh (void)
        {
            return kosteneinh;
        }

        double GetKostenKg (void)
        {
            return kostenkg;
        }

        void SetHuelWrt (double HuelWrt)
        {
            this->HuelWrt = HuelWrt;
        }

        double GetHuelWrt (void)
        {
            return HuelWrt;
        }
        void SetHuelHkWrt (double HuelHkWrt)
        {
            this->HuelHkWrt = HuelHkWrt;
        }

        double GetHuelHkWrt (void)
        {
            return HuelHkWrt;
        }

        void SetVpkWrt (double VpkWrt)
        {
            this->VpkWrt = VpkWrt;
        }

        double GetVpkWrt (void)
        {
            return VpkWrt;
        }
        void SetVpkHkWrt (double VpkHkWrt)
        {
            this->VpkHkWrt = VpkHkWrt;
        }

        double GetVpkHkWrt (void)
        {
            return VpkHkWrt;
        }


        void SetSuKostVarb (double SuKostVarb)
        {
            this->SuKostVarb = SuKostVarb;
        }

        double GetSuKostVarb (void)
        {
            return SuKostVarb;
        }

        void SetSuKostZut (double SuKostZut)
        {
            this->SuKostZut = SuKostZut;
        }

        double GetSuKostZut (void)
        {
            return SuKostZut;
        }

        void SetSuKostHuel (double SuKostHuel)
        {
            this->SuKostHuel = SuKostHuel;
        }

        double GetSuKostHuel (void)
        {
            return SuKostHuel;
        }

        void SetSuKostVpk (double SuKostVpk)
        {
            this->SuKostVpk = SuKostVpk;
        }

        double GetSuKostVpk (void)
        {
            return SuKostVpk;
        }
        void SetSuKostHK (double SuKostHK)
        {
            this->SuKostHK = SuKostHK;
        }

        double GetSuKostHK (void)
        {
            return SuKostHK;
        }

        void SetSuKostSw (double SuKostSw)
        {
            this->SuKostSw = SuKostSw;
        }

        double GetSuKostSw (void)
        {
            return SuKostSw;
        }

        BOOL GetGBVorhanden (void)
        {
            return GBVorhanden;
        }
        void SetGBVorhanden (BOOL GBVorhanden)
        {
            this->GBVorhanden = GBVorhanden;
        }

        void SetSuKostAufs (double SuKostAufs)
        {
            this->SuKostAufs = SuKostAufs;
        }

        double GetSuKostAufs (void)
        {
            return SuKostAufs;
        }
        double GetSuKostSK (void)
        {
            return SuKostSK;
        }
        void SetSuKostSK (double SuKostSK)
        {
            this->SuKostSK = SuKostSK;
			prdk_k.sk_wrt = SuKostSK;
        }
        double GetSuKostFEK (void)
        {
            return SuKostFEK;
        }
        void SetSuKostFEK (double SuKostFEK)
        {
            this->SuKostFEK = SuKostFEK;
			prdk_k.filek_wrt = SuKostFEK; 
        }
        double GetSuKostFVK (void)
        {
            return SuKostFVK;
        }
        void SetSuKostFVK (double SuKostFVK)
        {
            this->SuKostFVK = SuKostFVK;
			prdk_k.filvk_wrt = SuKostFVK; 
        }

        void SetSuKostAufsEk (double SuKostAufsEk)
        {
            this->SuKostAufsEk = SuKostAufsEk;
        }

        double GetSuKostAufsEk (void)
        {
            return SuKostAufsEk;
        }
        void SetRezGew (double RezGew)
        {
            this->RezGew = RezGew;
        }

        double GetRezGew (void)
        {
            return RezGew;
        }


        void SetPlanEk (BOOL PlanEk)
        {
            this->PlanEk = PlanEk;
        }

        void SetListDlg (LISTENTER *ListDlg)
        {
            this->ListDlg = ListDlg;
        }

        void SetVarbLsts (struct VARBLSTS *Lsts)
        {
            this->VarbLsts = Lsts;
        }

        struct VARBLSTS *GetVarbLsts (void)
        {
            return VarbLsts;
        }

        void SetA_Lsts (struct A_LSTS *Lsts)
        {
            this->A_Lsts = Lsts;
        }

        struct A_LSTS *GetA_Lsts (void)
        {
            return A_Lsts;
        }


        void SetZutLsts (struct ZUTLSTS *Lsts)
        {
            this->ZutLsts = Lsts;
        }

        struct ZUTLSTS *GetZutLsts (void)
        {
            return ZutLsts;
        }

        void SetHuelLsts (struct HUELLSTS *Lsts)
        {
            this->HuelLsts = Lsts;
        }

        struct HUELLSTS *GetHuelLsts (void)
        {
            return HuelLsts;
        }

        void SetVpkLsts (struct VPKLSTS *Lsts)
        {
            this->VpkLsts = Lsts;
        }

        struct VPKLSTS *GetVpkLsts (void)
        {
            return VpkLsts;
        }

        void SetKostLsts (struct KOSTLSTS *Lsts)
        {
            this->KostLsts = Lsts;
        }

        struct KOSTLSTS *GetKostLsts (void)
        {
            return KostLsts;
        }

		void Work::SetChgFaktor (double f)
		{
			chgfaktor = f;
		}

        void SetZeitLsts (struct ZEITLSTS *Lsts)
        {
            this->ZeitLsts = Lsts;
        }

        struct ZEITLSTS *GetZeitLsts (void)
        {
            return ZeitLsts;
        }
        void SetQuidLsts (struct QUIDLSTS *Lsts)
        {
            this->QuidLsts = Lsts;
        }

        struct QUIDLSTS *GetQuidLsts (void)
        {
            return QuidLsts;
        }
        void BeginWork (void);
        void CommitWork (void);
        void RollbackWork (void);
        void InitRec (void);
        int ReadPrdk_k (short);
        int ReadPrdk_k (short, double, short);
        int ReadPrdk_k (short, char *, short);
        int TestAkv (short, double, short, short);
        void FuelleQuid (char *,short,short,double,double,double);
        void RechneQuid (void);
        int WritePrdk_k (short, double, char *, char *, char *);
        int DeletePrdk_k (short, double, char *);
        int GetPtab (char *, char *, char *);
        int GetPtab (char *, char *, short);
        int GetPtabLong (char *, char *, char *);
        int GetPtabLong (char *, char *, short);
        char *GetPtWert (char *, short);
        int ShowPtab (char *dest, char *item);
        int GetABez (char *, double,char *);
        int GetMdnName (char *, short);
        int GetRez (char *, char *, double ,short);
        int upd_a_bas (double);
        double GetRezAGew (void);
		void rechne_zeit (void);
		void rechne_varb (void);
		void rechne_zut (void);
        long GetAnzKalk (void);
        long updPrdk_k (void);
        long dbupdate_prdk_k (void);
        void FillRow (char *);
        void FillCombo (char **, char *, int);
        void FillComboLong (char **, char *, int);
        void FillAdrNr (void);
        long GenAdrNr (void);
        long GenAdrNrEx (void);
        void FreeAdr (long);
        BOOL AdrOK (void);
//        long GenTxtNr (void);
//        void FreeTxtNr (long);
        BOOL TxtNrOK (void);
//        void ReadTxt (long, MTXT *);
//        void WriteTxt (long, MTXT *);
        int  CutLines (char *, char *, int, int);
        double Round (double , int);

// Artikel von-bis (Batchlauf)
        void FillA_Row (void);
        void InitA_Row (void);
        int PrepareA (void);
        int OpenA (void);
        int FetchA (void);
        int CloseA (void);

// Verabeitungsmaterial

        double GetChgGew (void);
        double GetVarbGew (void);
        double GetVarbGewNto (void);
        double GetZutGew (void);
        double GetHuelGew (void);
        void UpdateChgGew (double);
        void UpdateVpkMe (void);
        void GetVarbAbz1 (char *, char *, double *, double *,long);
        double GetArtikel (char *, double *,long);
        void GetRezAbz1 (char *, char *, double *, long);
        BOOL GetVarbMatName (void);
        long GetAG (void);
        short GetVarbRezName (void);
        double GetRezChgGew (double);
        double GetMatEk (double, short, double,short,int);
        void FillVarbRow (void);
        short hole_a_typ1 (double);
        void InitVarbRow (void);
        void DeleteAllVarbPos (void);
        void WriteVarbRec (void);
        int PrepareVarb (void);
        int PrepareVarb1 (void);
        int PrepareVarb2 (void);
        int PrepareVarb3 (void);
        int PrepareKopf2 (void);
        int PrepareKopf3 (void);
        int Preparecursor_quid (void);
        int OpenVarb (void);
        int OpenVarbQuid (short);
        int FetchVarb (void);
        int FetchVarbQuid (short);
        int FetchKopfQuid (short);
        int CloseVarb (void);
        void CalcSumVarbBatch (void);
        int CloseVarbQuid (void);
        int CloseKopfQuid (void);
		long Mat_aus_a (double);
		double A_aus_Mat (long);

// Zutaten
        void GetZutAbz1 (char *, char *, double *,double *,long);
        void GetRezZutAbz1 (char *, char *,double *,long);
        BOOL GetZutMatName (void);
        void FillZutRow (void);
        void InitZutRow (void);
        void DeleteAllZutPos (void);
        void WriteZutRec (void);
        int PrepareZut (void);
        int OpenZut (void);
        int FetchZut (void);
        int CloseZut (void);

// Huellen
        void GetHuelAbz1 (char *, char *,double *,double *,long);
        BOOL GetHuelMatName (void);
        void FillHuelRow (void);
        void InitHuelRow (void);
        void DeleteAllHuelPos (void);
        void WriteHuelRec (void);
        int PrepareHuel (void);
        int OpenHuel (void);
        int FetchHuel (void);
        int CloseHuel (void);


// Verpackung
        void GetVpkAbz1 (char *, char *, double *,double *,double);
        void GetRezVpkAbz1 (char *, char *,double *,long);
        BOOL GetVpkMatName (void);
//        BOOL GetVpkRezName (void);
        void FillVpkRow (void);
        void InitVpkRow (void);
        void DeleteAllVpkPos (void);
        void WriteVpkRec (void);
        int PrepareVpk (void);
        int OpenVpk (void);
        int FetchVpk (void);
        int CloseVpk (void);

// Kosten
        void GetKostBez (char *, double *, char *, double *, short);
        BOOL GetKostBez (void);
        void FillKostRow (void);
        void InitKostRow (void);
        void DeleteAllKostPos (void);
        void WriteKostRec (void);
        int PrepareKost (void);
        int OpenKost (void);
        int FetchKost (void);
        int CloseKost (void);
        void CalcKosten (void);
        double CalcKosten (long, double, short, double);

// Zeit
        void GetZeitBez (char *, short);
        BOOL GetZeitBez (void);
        void FillZeitRow (void);
        void FillQuidRow (void);
        void InitZeitRow (void);
        void InitQuidRow (void);
        void DeleteAllZeitPos (void);
        void WriteZeitRec (int);
        int PrepareZeit (void);
        int OpenZeit (void);
        int FetchZeit (void);
        int CloseZeit (void);
        int PrepareQuid (void);
        int OpenQuid (void);
        int FetchQuid (void);
        int CloseQuid (void);
        void CopyZeit (short, double);
//        void CalcZeiten (void);


//Quid
		void PruefeAnalyse(double,int,double,double,double,double);
		void FuelleWarnung(char *);


// Report
        void ReportRezUpdate (void);
        int  ReportDelete (void);


        void CalcMe (void);
        void CalcFuellMe (void);
        void CalcVpkMe (void);
        void CalcVpkFuellMe (void);
        void ReadMasch (char *, long);
        int CopyActiveVar (short, char *, double, short);
        int CopyArtikel (short, char *, double, short,short, char *, double, short);

		static short GetPreisHerkunft(void);
		static void SetPreisHerkunft (char *);
		static int GetPlanPreisHerkunft(void);
		static int GetRezOhneVarianten(void);
		static void SetPlanPreisHerkunft (char *);
		static int GetMaterialOhneKosten(void);
		static void SetMaterialOhneKosten (char *);
		static void SetRezPreiseSpeichern (char *);
		static void SetRezOhneVarianten (char *);
		static int Geta_preism(void);
		static void Seta_preism (char *);
        void SetSummen (double, double, double, int);




};

#endif