#ifndef _KOST_ART_DEF
#define _KOST_ART_DEF

#include "windows.h"
#include "mo_draw.h"
#include "wmask.h"
#include "dbclass.h"

struct KOST_ART {
   short     mdn;
   short     fil;
   short     kost_art;
   char      kost_art_bz[25];
   short     delstatus;
   long      erl_kto;
   char      vkost_rechb[4];
   double    vkost_wt;
   double    faktor;
   long      dat;
   char      pers_nam[9];
};
extern struct KOST_ART kost_art, kost_art_null;

#line 10 "kost_art.rh"

class KOST_ART_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               KOST_ART_CLASS () : DB_CLASS ()
               {
               }
};
#endif
