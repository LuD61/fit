#include "Progress.h"
#include "wmaskc.h"
#include "dlg.h"

static mfont Pfont = {
                         "ARIAL", 
                         120, 0, 0,
                         0,
                         0,
                         0,
                         NULL};

PROGRESS::PROGRESS (HWND hWndMain, int x, int y, int cx, int cy, BOOL Pixel) 
{
        HFONT hFont, oldfont;
        TEXTMETRIC tm;
        SIZE size;
        RECT wrect;
        RECT crect;

        GetWindowRect (hWndMain, &wrect);
        GetClientRect (hWndMain, &crect);

  	    HDC hdc = GetDC (NULL);
        hFont = SetDeviceFont (hdc, &Pfont, &tm);
        oldfont = SelectObject (hdc, hFont);
	    GetTextMetrics (hdc, &tm);
		GetTextExtentPoint32 (hdc, "X", 1, &size);
		DeleteObject (SelectObject (hdc, oldfont));

        if (Pixel == FALSE)
        {
            cx *= size.cx;
            if (cy == 0)
            {
                cy = (int) (double) ((double) size.cy * 1.5);
            }
            else
            {
                cy *= size.cy;
            }
            if (x > 0)
            {
                x *= size.cx;
            }
            if (y > 0)
            {
                y *= size.cy;
            }
        }
        if (x == -1)
        {
            x = max (0,(crect.right - cx) / 2);
            x += wrect.left;
        }
        if (y == -1)
        {
            y = max (0,(crect.bottom - cy) / 2);
            y += wrect.top;
        }

        else if (y == -2)
        {
            y = max (0,crect.bottom - cy);
            y += wrect.top;
        }
        hWnd = CreateWindowEx (WS_EX_CLIENTEDGE, 
                               PROGRESS_CLASS,
                               NULL,
                               WS_POPUP | WS_DLGFRAME,
                               x, y,
                               cx, cy,
                               hWndMain,
                               NULL,
                               DLG::hInstance,
                               NULL);
         
        DWORD ret = SetClassLong (hWnd, GCL_HBRBACKGROUND, (long) GetStockObject (WHITE_BRUSH));
        ShowWindow (hWnd, SW_SHOWNORMAL);
        UpdateWindow (hWnd);
        SetStep (20);
}

PROGRESS::~PROGRESS ()
{
        if (hWnd != NULL)
        {
               DestroyWindow (hWnd);
        }
}

void PROGRESS::SetRange (int min, int max)
{
        SendMessage (hWnd, PBM_SETSTEP, (WPARAM) min, (LPARAM) max);
}


void PROGRESS::SetStep (int step)
{
        SendMessage (hWnd, PBM_SETSTEP, (WPARAM) step, (LPARAM) 0);
}


void PROGRESS::StepIt (void)
{
        SendMessage (hWnd, PBM_STEPIT, (WPARAM) 0, (LPARAM) 0);
        InvalidateRect (hWnd, NULL, FALSE);
        UpdateWindow (hWnd);
}

void PROGRESS::SetPos (int pos)
{
        SendMessage (hWnd, PBM_SETPOS, (WPARAM) pos, (LPARAM) 0);
}

            

