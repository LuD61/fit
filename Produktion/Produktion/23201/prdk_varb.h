#ifndef _PRDK_VARB_DEF
#define _PRDK_VARB_DEF

#include "windows.h"
#include "mo_draw.h"
#include "wmask.h"
#include "dbclass.h"

struct PRDK_VARB {
   short     mdn;
   char      rez[9];
   long      mat;
   short     id_nr;
   short     kutf;
   char      bearb_info[81];
   double    varb_me;
   double    gew_bto;
   double    gew;
   double    toler;
   short     variante;
   short     typ;
   double    anteil_proz;
   char      zug_bas[3];
};
extern struct PRDK_VARB prdk_varb, prdk_varb_null;

#line 10 "prdk_varb.rh"

class PRDK_VARB_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               PRDK_VARB_CLASS () : DB_CLASS ()
               {
               }
};
#endif
