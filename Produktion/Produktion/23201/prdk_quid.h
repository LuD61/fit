#ifndef _PRDK_QUID_DEF
#define _PRDK_QUID_DEF

#include "windows.h"
#include "mo_draw.h"
#include "wmask.h"
#include "dbclass.h"

struct PRDK_QUID {
   short     mdn;
   char      rez[9];
   short     variante;
   char      quid_kat[4];
   short     quid_nr;
   double    flkg;
   double    gflkg;
   double    flproz;
   double    gflproz;
   double    fettproz;
   double    gfettproz;
   double    feproz;
   double    gfeproz;
   double    beffeproz;
   double    gbeffeproz;
   double    schwundkg;
   double    gschwundkg;
};
extern struct PRDK_QUID prdk_quid, prdk_quid_null;

#line 10 "prdk_quid.rh"

class PRDK_QUID_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               PRDK_QUID_CLASS () : DB_CLASS ()
               {
               }
};
#endif
