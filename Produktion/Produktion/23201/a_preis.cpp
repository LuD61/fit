#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "wmaskc.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "a_preis.h"

struct A_PREIS a_preis, a_preis_null;

void A_PREIS_CLASS::prepare (void)
{
            char *sqltext;

//            init_sqlin ();
            ins_quest ((char *)   &a_preis.mdn, 2, 0);
            ins_quest ((char *)   &a_preis.a,  3, 0);
    out_quest ((char *) &a_preis.mdn,2,0);
    out_quest ((char *) &a_preis.fil,1,0);
    out_quest ((char *) &a_preis.a,3,0);
    out_quest ((char *) &a_preis.prodabt,1,0);
    out_quest ((char *) &a_preis.druckfolge,1,0);
    out_quest ((char *) &a_preis.ek,3,0);
    out_quest ((char *) &a_preis.plan_ek,3,0);
    out_quest ((char *) &a_preis.kostenkg,3,0);
    out_quest ((char *) &a_preis.kosteneinh,3,0);
    out_quest ((char *) &a_preis.flg_bearb_weg,1,0);
    out_quest ((char *) &a_preis.bearb_hk,1,0);
    out_quest ((char *) &a_preis.bearb_p1,1,0);
    out_quest ((char *) &a_preis.bearb_p2,1,0);
    out_quest ((char *) &a_preis.bearb_p3,1,0);
    out_quest ((char *) &a_preis.bearb_p4,1,0);
    out_quest ((char *) &a_preis.bearb_p5,1,0);
    out_quest ((char *) &a_preis.bearb_p6,1,0);
    out_quest ((char *) &a_preis.bearb_vk,1,0);
    out_quest ((char *) &a_preis.sp_sk,3,0);
    out_quest ((char *) &a_preis.sp_fil,3,0);
    out_quest ((char *) &a_preis.sp_vk,3,0);
    out_quest ((char *) &a_preis.dat,2,0);
    out_quest ((char *) a_preis.zeit,0,6);
    out_quest ((char *) a_preis.pers_nam,0,9);
    out_quest ((char *) &a_preis.inh_ek,2,0);
    out_quest ((char *) &a_preis.bep,3,0);
    out_quest ((char *) &a_preis.plan_bep,3,0);
            cursor = prepare_sql ("select a_preis.mdn,  "
"a_preis.fil,  a_preis.a,  a_preis.prodabt,  a_preis.druckfolge,  "
"a_preis.ek,  a_preis.plan_ek,  a_preis.kostenkg,  a_preis.kosteneinh,  "
"a_preis.flg_bearb_weg,  a_preis.bearb_hk,  a_preis.bearb_p1,  "
"a_preis.bearb_p2,  a_preis.bearb_p3,  a_preis.bearb_p4,  "
"a_preis.bearb_p5,  a_preis.bearb_p6,  a_preis.bearb_vk,  "
"a_preis.sp_sk,  a_preis.sp_fil,  a_preis.sp_vk,  a_preis.dat,  "
"a_preis.zeit,  a_preis.pers_nam,  a_preis.inh_ek,  a_preis.bep,  "
"a_preis.plan_bep from a_preis "

#line 22 "a_preis.rpp"
                                  "where mdn = ? "
                                  "and   a   = ?");
    ins_quest ((char *) &a_preis.mdn,2,0);
    ins_quest ((char *) &a_preis.fil,1,0);
    ins_quest ((char *) &a_preis.a,3,0);
    ins_quest ((char *) &a_preis.prodabt,1,0);
    ins_quest ((char *) &a_preis.druckfolge,1,0);
    ins_quest ((char *) &a_preis.ek,3,0);
    ins_quest ((char *) &a_preis.plan_ek,3,0);
    ins_quest ((char *) &a_preis.kostenkg,3,0);
    ins_quest ((char *) &a_preis.kosteneinh,3,0);
    ins_quest ((char *) &a_preis.flg_bearb_weg,1,0);
    ins_quest ((char *) &a_preis.bearb_hk,1,0);
    ins_quest ((char *) &a_preis.bearb_p1,1,0);
    ins_quest ((char *) &a_preis.bearb_p2,1,0);
    ins_quest ((char *) &a_preis.bearb_p3,1,0);
    ins_quest ((char *) &a_preis.bearb_p4,1,0);
    ins_quest ((char *) &a_preis.bearb_p5,1,0);
    ins_quest ((char *) &a_preis.bearb_p6,1,0);
    ins_quest ((char *) &a_preis.bearb_vk,1,0);
    ins_quest ((char *) &a_preis.sp_sk,3,0);
    ins_quest ((char *) &a_preis.sp_fil,3,0);
    ins_quest ((char *) &a_preis.sp_vk,3,0);
    ins_quest ((char *) &a_preis.dat,2,0);
    ins_quest ((char *) a_preis.zeit,0,6);
    ins_quest ((char *) a_preis.pers_nam,0,9);
    ins_quest ((char *) &a_preis.inh_ek,2,0);
    ins_quest ((char *) &a_preis.bep,3,0);
    ins_quest ((char *) &a_preis.plan_bep,3,0);
            sqltext = "update a_preis set a_preis.mdn = ?,  "
"a_preis.fil = ?,  a_preis.a = ?,  a_preis.prodabt = ?,  "
"a_preis.druckfolge = ?,  a_preis.ek = ?,  a_preis.plan_ek = ?,  "
"a_preis.kostenkg = ?,  a_preis.kosteneinh = ?,  "
"a_preis.flg_bearb_weg = ?,  a_preis.bearb_hk = ?,  "
"a_preis.bearb_p1 = ?,  a_preis.bearb_p2 = ?,  a_preis.bearb_p3 = ?,  "
"a_preis.bearb_p4 = ?,  a_preis.bearb_p5 = ?,  a_preis.bearb_p6 = ?,  "
"a_preis.bearb_vk = ?,  a_preis.sp_sk = ?,  a_preis.sp_fil = ?,  "
"a_preis.sp_vk = ?,  a_preis.dat = ?,  a_preis.zeit = ?,  "
"a_preis.pers_nam = ?,  a_preis.inh_ek = ?,  a_preis.bep = ?,  "
"a_preis.plan_bep = ? "

#line 25 "a_preis.rpp"
                                  "where mdn = ? "
                                  "and   a   = ?";
            ins_quest ((char *)   &a_preis.mdn, 2, 0);
            ins_quest ((char *)   &a_preis.a,  3, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &a_preis.mdn, 2, 0);
            ins_quest ((char *)   &a_preis.a,  3, 0);
            test_upd_cursor = prepare_sql ("select a from a_preis "
                                  "where mdn = ? "
                                  "and   a = ? "
                                  "for update");

            ins_quest ((char *) &a_preis.mdn, 2, 0);
            ins_quest ((char *)   &a_preis.a,  3, 0);
            del_cursor = prepare_sql ("delete from a_preis "
                                  "where mdn = ? "
                                  "and   a = ?");
    ins_quest ((char *) &a_preis.mdn,2,0);
    ins_quest ((char *) &a_preis.fil,1,0);
    ins_quest ((char *) &a_preis.a,3,0);
    ins_quest ((char *) &a_preis.prodabt,1,0);
    ins_quest ((char *) &a_preis.druckfolge,1,0);
    ins_quest ((char *) &a_preis.ek,3,0);
    ins_quest ((char *) &a_preis.plan_ek,3,0);
    ins_quest ((char *) &a_preis.kostenkg,3,0);
    ins_quest ((char *) &a_preis.kosteneinh,3,0);
    ins_quest ((char *) &a_preis.flg_bearb_weg,1,0);
    ins_quest ((char *) &a_preis.bearb_hk,1,0);
    ins_quest ((char *) &a_preis.bearb_p1,1,0);
    ins_quest ((char *) &a_preis.bearb_p2,1,0);
    ins_quest ((char *) &a_preis.bearb_p3,1,0);
    ins_quest ((char *) &a_preis.bearb_p4,1,0);
    ins_quest ((char *) &a_preis.bearb_p5,1,0);
    ins_quest ((char *) &a_preis.bearb_p6,1,0);
    ins_quest ((char *) &a_preis.bearb_vk,1,0);
    ins_quest ((char *) &a_preis.sp_sk,3,0);
    ins_quest ((char *) &a_preis.sp_fil,3,0);
    ins_quest ((char *) &a_preis.sp_vk,3,0);
    ins_quest ((char *) &a_preis.dat,2,0);
    ins_quest ((char *) a_preis.zeit,0,6);
    ins_quest ((char *) a_preis.pers_nam,0,9);
    ins_quest ((char *) &a_preis.inh_ek,2,0);
    ins_quest ((char *) &a_preis.bep,3,0);
    ins_quest ((char *) &a_preis.plan_bep,3,0);
            ins_cursor = prepare_sql ("insert into a_preis ("
"mdn,  fil,  a,  prodabt,  druckfolge,  ek,  plan_ek,  kostenkg,  kosteneinh,  "
"flg_bearb_weg,  bearb_hk,  bearb_p1,  bearb_p2,  bearb_p3,  bearb_p4,  "
"bearb_p5,  bearb_p6,  bearb_vk,  sp_sk,  sp_fil,  sp_vk,  dat,  zeit,  pers_nam,  "
"inh_ek,  bep,  plan_bep) "

#line 44 "a_preis.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"); 

#line 46 "a_preis.rpp"
}
