#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include <string.h>
#include "comcthlp.h"
#include "itemc.h"
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_menu.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_intp.h"
#include "inflib.h"
#include "spezdlg.h"
#include "enterfunc.h"
#include "KostLst.h"
#include "spezdlg.h"
#include "searchrezv.h"
#include "searchka.h"
// #include "dlg.h"

#define IDM_CANCEL 3001
#ifndef IDM_WRITE
#define IDM_WRITE  3002
#endif

#ifndef MAXCOMBO
#define MAXCOMBO 20
#endif

static int StdSize = STDSIZE;

static int dlgsize    = STDSIZE;
static int ltgraysize = STDSIZE;
static int dlgpossize = STDSIZE;
static int cpsize     = 200;


static mfont dlgfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         GRAYCOL,
                         0,
                         NULL};

struct KOSTLSTS  KOSTLST::Lsts;
struct KOSTLSTS *KOSTLST::LstTab;


CFIELD *KOSTLST::_fChoise0 [] = {
    new CFIELD ("cancel", " abbrechen ", 10, 0, 0, 0, NULL, "",
                CBUTTON, 
                IDM_CANCEL, &dlgfont, 0, 0),
    new CFIELD ("write", " speichern ", 10, 0,12, 0, NULL, "",
                CBUTTON, 
                IDM_WRITE, &dlgfont, 0, 0),
    new CFIELD ("show",  "  ansehen  ", 10, 0,24, 0, NULL, "",
                CBUTTON, 
                IDM_CHOISE, &dlgfont, 0, 0),
    new CFIELD ("delete","  l�schen  ", 10, 0,36, 0, NULL, "",
                CBUTTON, 
                IDM_DELETE, &dlgfont, 0, 0),
    new CFIELD ("insert"," einf�gen  ", 10, 0,48, 0, NULL, "",
                CBUTTON, 
                IDM_INSERT, &dlgfont, 0, 0),
    NULL,
};

CFORM KOSTLST::fChoise0 (5, _fChoise0);

CFIELD *KOSTLST::_fChoise [] = {
    new CFIELD ("fChoise0", (char *) &fChoise0, 0, 0, -1 , -2, NULL, "",
                CFFORM, 
                500, &dlgfont, 0, 0),
    NULL,
};

CFORM KOSTLST::fChoise (1, _fChoise);


static char *EkTxt = "EK";
static char *PlanEkTxt = "Plan-EK";


ITEM KOSTLST::ustufe_bez          ("stufe_bez",          "Stufe",      "", 0); 
ITEM KOSTLST::ukost_art           ("kost_art",           "Kostenart",        "", 0);
ITEM KOSTLST::ukost_art_bez       ("kost_art_bez",       "Bezeichnung",      "", 0);
ITEM KOSTLST::ukost_wert          ("kost_wert",          "Wert",             "", 0);
ITEM KOSTLST::ume_einh            ("me_einh",            "Einheit",          "", 0);
ITEM KOSTLST::ukosten             ("kosten",             "Kosten",           "", 0);
ITEM KOSTLST::ufiller             ("",                   " ",                "",0);

ITEM KOSTLST::iline ("", "1", "", 0);

ITEM KOSTLST::istufe_bez      ("stufe_bez",      Lsts.stufe_bez,    "", 0);
ITEM KOSTLST::ikost_art       ("kost_art",       Lsts.kost_art,     "", 0);  
ITEM KOSTLST::ikost_art_bez   ("kost_art_bez",   Lsts.kost_art_bez, "", 0);
ITEM KOSTLST::ikost_wert      ("kost_wert",      Lsts.kost_wert,    "", 0);
ITEM KOSTLST::ime_einh        ("me_einh",        Lsts.me_einh,      "", 0);
ITEM KOSTLST::ikosten         ("kosten",         Lsts.kosten,       "", 0);
ITEM KOSTLST::ichtyp          ("chtyp",          (char *) &Lsts.chtyp,  "", 0);
ITEM KOSTLST::ichnr           ("chnr",           (char *) &Lsts.chnr,   "", 0);

field KOSTLST::_UbForm [] = {
&ustufe_bez,    17, 0, 0,  6,  NULL, "", BUTTON, 0, 0, 0,     
&ukost_art,     21, 0, 0, 23,  NULL, "", BUTTON, 0, 0, 0,     
&ukost_art_bez, 22, 0, 0, 44,  NULL, "", BUTTON, 0, 0, 0, 
&ukost_wert,    12, 0, 0, 66,  NULL, "", BUTTON, 0, 0, 0,     
&ume_einh,      11, 0, 0, 78,  NULL, "", BUTTON, 0, 0, 0,     
&ukosten,       11, 0, 0, 89,  NULL, "", BUTTON, 0, 0, 0,     
&ufiller,      120, 0, 0,100,  NULL, "", BUTTON, 0, 0, 0,
};


form KOSTLST::UbForm = {7, 0, 0, _UbForm, 0, 0, 0, 0, NULL};

field KOSTLST::_LineForm [] = {
&iline,   1, 0, 0, 23,  NULL, "",  DISPLAYONLY, 0, 0, 0, 
&iline,   1, 0, 0, 44,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0, 66,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0, 78,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0, 89,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,100,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
};

form KOSTLST::LineForm = {6, 0, 0, _LineForm,0, 0, 0, 0, NULL};

field KOSTLST::_DataForm [] = {
&istufe_bez,    16,10, 0,  7,   NULL, "",      DISPLAYONLY, 0, 0, 0,     
&ikost_art,     17, 0, 0, 24,   NULL, "%8d",   EDIT,        0, 0, 0,     
&ichnr,          2, 1, 0, 41,   NULL, "",      BUTTON,      0, 0, 0,
&ikost_art_bez, 20, 0, 0, 45,   NULL, "",      DISPLAYONLY, 0, 0, 0, 
&ikost_wert,    10, 0, 0, 67,   NULL, "%7.2f", DISPLAYONLY, 0, 0, 0,     
&ime_einh,       8, 0, 0, 79,   NULL, "",      DISPLAYONLY, 0, 0, 0,      
&ikosten,        9, 0, 0, 90,   NULL, "%7.3f", DISPLAYONLY, 0, 0, 0,     
};


form KOSTLST::DataForm = {7, 0, 0, _DataForm,0, 0, 0, 0, NULL};

char *KOSTLST::StufeCombo [MAXCOMBO] = {"HK-Material",
                                        "HK-Zutaten",
                                        "HK-H�llen",
                                        "HK-Verpackung",
                                        "Herstellkosten",
                                        "SK-Rampe",
                                        "Filial-EK",
                                        "Filial-VK",
                                        NULL,
};
/*
char *KOSTLST::StufeCombo [MAXCOMBO] = {
										"Aufschlag HK",
                                        "SK-Rampe",
                                        "Filial-EK",
                                        "Filial-VK",
                                        NULL,
};
*/

void KOSTLST::FillCombo (HWND hWnd, char *ItemName)
{
    static BOOL FillOK = FALSE;

    if (strcmp (ItemName, "stufe_bez") == 0)
    {
       if (work != NULL)
       {
 // nich mehr �ber ptab!        work->FillCombo (StufeCombo, "prdk_stuf", MAXCOMBO - 1);
       }
       for (int i = 0; StufeCombo [i]; i ++)
       {
               SendMessage (hWnd, CB_ADDSTRING, 0, 
                           (LPARAM) StufeCombo[i]);
       }
       if (strcmp (clipped (Lsts.stufe_bez), " ") <= 0)
       {
              strcpy (Lsts.stufe_bez, StufeCombo [0]);
       }
    }
}


int KOSTLST::ubrows [] = {0, 
//                          0,
                          1, 1,
                          2,
                          3, 
                          4,
                          5,
                          6,
                         -1,
};


struct CHATTR KOSTLST::ChAttra [] = {
                                     "stufe_bez",     DISPLAYONLY,COMBOBOX | DROPLIST, 
//                                      "kost_art",     DISPLAYONLY,EDIT,                
//                                      "chnr",         REMOVED,BUTTON,                
                                      NULL,  0,           0};

struct CHATTR *KOSTLST::ChAttr = ChAttra;
int KOSTLST::NoRecNrSet = FALSE;
char *KOSTLST::InfoItem = "kost_art";


void KOSTLST::SetPlanEk (BOOL PlanEk)
{
         this->PlanEk = PlanEk;
}


BOOL KOSTLST::CallInfo (HWND hWnd, RECT *rect, char *Item, char *ItemValue, DWORD WinMode)
/**
Einstieg in Infosystem mit Artikel. Abgeleitet von Klasse Info.
**/
{
 		 char buffer [1024];
         BOOL ret;

         sprintf (buffer, "where a = 2");
         ret = Info::CallInfo (hWnd, rect, "a", buffer, WinMode); 
         SetListFocus ();
         return ret;
}


int KOSTLST::InfoProc (char **Item, char *Value, char *where)
/**
Spezielle Procedure fuer Info-System. Alter Ablauf, ist nicht mehr Aktiv 
**/
{

   *Item = "a";

   sprintf (where, "where a = 2");
   return 1;
}

void KOSTLST::CallInfoEx (void)
/*
Info wird aus Dialog aufgerufen.
*/

{
   char where [512];
   
   sprintf (where, "where kost_art = %d", Lsts.kost_art);
   _CallInfoEx (hMainWindow, NULL, InfoItem, where, 0l); 
}


void KOSTLST::SetButtonSize (int Size)
{
    int fpos;

//    return;
    fpos = GetItemPos (&DataForm, "chtyp");
    if (fpos < 0) return;

    if (Size == SMALL)
    {
         DataForm.mask [fpos].length = 2;
         DataForm.mask [fpos].rows = 1;
    }
    else 
    {
         DataForm.mask [fpos].length = 3;
         DataForm.mask [fpos].rows = 0;
    }
    fpos = GetItemPos (&DataForm, "chnr");
    if (fpos < 0) return;

    if (Size == SMALL)
    {
         DataForm.mask [fpos].length = 2;
         DataForm.mask [fpos].rows = 1;
    }
    else 
    {
         DataForm.mask [fpos].length = 3;
         DataForm.mask [fpos].rows = 0;
    }
}


void KOSTLST::GetCfgValues (void)
{
       char cfg_v [512];
       COLORREF Color;

       if (ProgCfg == NULL) return;
	   if (cfgOK) return;

       cfgOK = TRUE;
       if (ProgCfg->GetCfgValue ("3DList", cfg_v) == TRUE)
       {
                    eListe.Set3D (atoi (cfg_v));
	   }
       if (ProgCfg->GetCfgValue ("3DSize", cfg_v) == TRUE)
       {
                    eListe.SetPlus3D (atoi (cfg_v));
	   }
       if (ProgCfg->GetCfgValue ("ListFocus", cfg_v) == TRUE)
       {
                    ListFocus = atoi (cfg_v);
	   }
        if (ProgCfg->GetCfgValue ("rowheight", cfg_v) == TRUE)
		{
			        RowHeight = ratod (cfg_v);
					if (RowHeight < (double) 1.0)
					{
						RowHeight = (double) 1.0;
					}
		}
        if (ProgCfg->GetCfgValue ("ubheight", cfg_v) == TRUE)
		{
			        UbHeight = atoi (cfg_v);
					SetUbHeight ();
		}
		eListe.SetRowHeight (RowHeight);


        if (ProgCfg->GetCfgValue ("NoRecNr", cfg_v) == TRUE)
        {
			        NoRecNr = atoi (cfg_v);
		            if (atoi (cfg_v))
					{
	                        SetNoRecNr ();
					}
		}
        if (ProgCfg->GetCfgValue ("ListColor", cfg_v) == TRUE)
        {
                     GetListColor (&Color, cfg_v);
		             eListe.SetColor (Color);
        }
        if (ProgCfg->GetCfgValue ("ListBkColor", cfg_v) == TRUE)
        {
                     GetListColor (&Color, cfg_v);
		             eListe.SetBkColor (Color);
        }
        if (ProgCfg->GetCfgValue ("ListLine", cfg_v) == TRUE)
        {
		             eListe.SetLines (GetListLine (cfg_v));
        }
        if (ProgCfg->GetCfgValue ("KostInfo", cfg_v) == TRUE)
        {
                     InfoItem = new char [80];
                     strcpy (InfoItem, cfg_v);
        }
}


void KOSTLST::TestRemoves (void)
{
        int fpos;

        fpos = GetItemPos (&DataForm, "feld3");
        if (fpos >= 0)
        {
            if (DataForm.mask[fpos].attribut & REMOVED)
            {
                DestroyListField ("feld3");
            }
        }
}


void KOSTLST::SetNoRecNr (void)
{
	   int i;

	   eListe.SetNoRecNr (TRUE);
	   if (NoRecNrSet) return;

	   for (i = 0; i < dataform->fieldanz; i ++)
	   {
		   dataform->mask[i].pos[1] -= 6;
	   }
	   for (i = 0; i < ubform->fieldanz; i ++)
	   {
		   ubform->mask[i].pos[1] -= 6;
	   }
	   for (i = 0; i < lineform->fieldanz; i ++)
	   {
		   lineform->mask[i].pos[1] -= 6;
	   }
       NoRecNrSet = TRUE;
}


KOSTLST::KOSTLST () : KEYS (), LISTENTER () 
{

    ProgCfg = new PROG_CFG ("23201");
	Dlg = NULL;
    cfgOK = FALSE;
	cursor = -1;
    LstTab = new KOSTLSTS [0x10000];

    dataform  = &DataForm;
    ubform    = &UbForm;
    lineform  = &LineForm;
//    SetFieldLen ("stufe_bez",10);
    SetFieldLen ("kost_art", 15);
    eListe.SetUbRows (ubrows); 
	eListe.SetKeys (this);
    LISTENTER::GetCfgValues ();
    GetCfgValues ();
//    GetSysPar ();
    TestRemoves ();
    if (NoRecNr)
    {
	    SetNoRecNr ();
    }
	fChoise.Setchpos (0, 2);
    work = NULL;
    ChAttr = ChAttra;
    eListe.SetChAttr (ChAttr); 
    Toolbar2 = NULL;
    eListe.SetInfoProc (InfoProc);
    eListe.SetInfo (this);
    ArrDown = NULL;
    ArrDown = LoadBitmap (DLG::hInstance, "ARRDOWN");
    PlanEk = FALSE;
}


KOSTLST::~KOSTLST ()
{
	if (work != NULL)
	{
          work->SetListDlg (NULL);
	}
    fChoise.destroy ();
	if (LstTab != NULL)
	{
  	    delete []LstTab;
		LstTab = NULL;
	}
    if (ProgCfg != NULL)
    {
        ProgCfg->CloseCfg ();
        delete ProgCfg;
        ProgCfg = NULL;
    }
    if (work != NULL)
    {
        work->SetKostLsts (NULL);
    }
}


// Funktionen f�r den Zugriff auf die Daten. Sind als abstrakte Schnittstellen in
// listenter definiert.

unsigned char *KOSTLST::GetDataRec (void)
{
    return (unsigned char *) &Lsts;
}

int KOSTLST::GetDataRecLen (void)
{
    return sizeof (struct KOSTLSTS);
}

char *KOSTLST::GetListTab (int i)
{
    return (char *) &LstTab[i];
}

// Ende der Schnittstellen.


void KOSTLST::Setfeld2Removed (BOOL removed)
{
    if (removed)
    {
            SetFieldAttr ("feld2", REMOVED);
    }
    else
    {
            SetFieldAttr ("feld2", EDIT);
    }
}


void KOSTLST::Prepare (void)
{
 // nicht mehr aus ptab!   work->FillCombo (StufeCombo, "prdk_stuf", MAXCOMBO - 1);
    if (work != NULL)
    {
        cursor = work->PrepareKost ();
    }
}

void KOSTLST::Open (void)
{
	if (cursor == -1)
	{
		Prepare ();
		if (cursor == -1) return;
	}
    if (work != NULL)
    {
        work->OpenKost ();
    }
}

int KOSTLST::Fetch (void)
{
	if (cursor == -1)
	{
		Prepare ();
		if (cursor == -1) return 100;
	}
    if (work != NULL)
    {
        return work->FetchKost ();
    }
	return 100;
}

void KOSTLST::CalcSum (void)
{

    double wert   = 0.0;
    double kosten = 0.0;
    double sukostvarb = 0.0;
    double sukostzut  = 0.0;
    double sukosthuel = 0.0;
    double sukostvpk  = 0.0;
    double sukosthk  = 0.0;
    double sukostsw  = 0.0;
    double sukostaufs = 0.0;
    double sukostaufsek = 0.0;
    double sukostsk = 0.0;
    double sukostfek = 0.0;
    double sukostfvk = 0.0;
 

    for (int i = 0; i < eListe.GetRecanz (); i ++)
    {
        wert   += ratod (LstTab[i].kost_wert); 
        kosten += ratod (LstTab[i].kosten);
        switch (LstTab[i].stufe) 
        {
           case 1 :
              sukostvarb += ratod (LstTab[i].kosten);
              sukostaufs += ratod (LstTab[i].kosten);
              sukostaufsek += ratod (LstTab[i].kostenek);
              break;
           case 2 :
              sukostzut += ratod (LstTab[i].kosten);
              sukostaufs += ratod (LstTab[i].kosten);
              sukostaufsek += ratod (LstTab[i].kostenek);
              break;
           case 3 :
              sukosthuel += ratod (LstTab[i].kosten);
              sukostaufs += ratod (LstTab[i].kosten);
              sukostaufsek += ratod (LstTab[i].kostenek);
              break;
           case 4 :
              sukostvpk += ratod (LstTab[i].kosten);
              sukostaufs += ratod (LstTab[i].kosten);
              sukostaufsek += ratod (LstTab[i].kostenek);
              break;
           case 5 :
              sukosthk += ratod (LstTab[i].kosten);
              sukostaufs += ratod (LstTab[i].kosten);
              sukostaufsek += ratod (LstTab[i].kostenek);
              break;
           case 6 :
//              sukostsk += ratod (LstTab[i].kosten);
			   sukostsk += work->CalcKosten(LstTab[i].vkost_rechb,
											LstTab[i].kost_wrt,
											LstTab[i].stufe,
											LstTab[i].faktor);
              break;
           case 7 :
//              sukostfek += ratod (LstTab[i].kosten);
			   sukostfek += work->CalcKosten(LstTab[i].vkost_rechb,
											LstTab[i].kost_wrt,
											LstTab[i].stufe,
											LstTab[i].faktor);
              break;
           case 8 :
//              sukostfvk += ratod (LstTab[i].kosten);
			   sukostfvk += work->CalcKosten(LstTab[i].vkost_rechb,
											LstTab[i].kost_wrt,
											LstTab[i].stufe,
											LstTab[i].faktor);
              break;
           default :
//              sukostaufs += ratod (LstTab[i].kosten);
//              sukostaufsek += ratod (LstTab[i].kostenek);
              break;
        }
    }

    work->SetSuKostVarb (sukostvarb);
    work->SetSuKostZut  (sukostzut);
    work->SetSuKostHuel (sukosthuel);
    work->SetSuKostVpk  (sukostvpk);
    work->SetSuKostHK  (sukosthk);
    work->SetSuKostSw   (sukostsw);
    work->SetSuKostAufs (sukostaufs);
    work->SetSuKostAufsEk (sukostaufsek);
    work->SetSuKostSK (sukostsk);
    work->SetSuKostFEK (sukostfek);
    work->SetSuKostFVK (sukostfvk);
//  memcpy (&LstTab[eListe.GetAktRow ()], &Lsts, sizeof (Lsts));
    ((SpezDlg *) Dlg)->SetSummen (kosten, wert, 0.0, KA,0,0);
    ((SpezDlg *) Dlg)->SetKostFields ();
}

void KOSTLST::ReportFill (void)
{

    for (int i = 0; i < eListe.GetRecanz (); i ++)
    {

	    reportrez.grouprez = 70;
	    reportrez.a = prdk_k.a;
	    strcpy(reportrez.a_bz1, prdk_kf.a_bz1);
	    strcpy(reportrez.rez ,prdk_k.rez);
	    strcpy(reportrez.rez_bz, prdk_k.rez_bz);
	    reportrez.variante = prdk_k.variante;
	    strcpy(reportrez.variante_bz, prdk_k.variante_bz);
	    if (strcmp(prdk_kf.ek,"J") == 0)  
        {
			strcpy(reportrez.preis_basis, "EK");				
		}
		else
        {
			strcpy(reportrez.preis_basis, "PLAN-EK");			
		}
	    reportrez.schwund = prdk_k.sw_kalk;
	    reportrez.chargengewicht = prdk_k.chg_gew;
	    strcpy(reportrez.datum, "01.01.2003");                      //muss nocht !
	    reportrez.posi = i + 1;
	    reportrez.mat = eListe.GetRecanz ();
	    strcpy(reportrez.mat_bz, "");
	    reportrez.menge = ratod(LstTab[i].kost_wert);
	    strcpy(reportrez.einheit, LstTab[i].me_einh);
	    reportrez.ek = 0.0;
	    reportrez.wert_ek = 0.0;
	    reportrez.kosten = ratod(LstTab[i].kosten);
	    reportrez.bep = 0.0;
	    reportrez.wert_bep = 0.0;
	    strcpy(reportrez.k_a_bez, LstTab[i].stufe_bez);           //Bezeichnung f�r Kosten und Aufschl�ge
	    reportrez.k_diff = 0.0;
	    reportrez.kostenkg = 0.0;
	    reportrez.kostenart = atoi(LstTab[i].kost_art);
	    strcpy(reportrez.kostenart_bz, LstTab[i].kost_art_bez);
	    reportrez.mdn = prdk_k.mdn;
	    work->ReportRezUpdate ();
    }
}




void KOSTLST::Close (void)
{
	if (cursor != -1)
	{
		cursor = -1;
	}
    if (work != NULL)
    {
        work->CloseKost ();
    }
    CalcSum ();
}

BOOL KOSTLST::BeforeCol (char *colname)
{
         return FALSE;
}
         

BOOL KOSTLST::TestKostArt (void)
{
          char me_einh [5];

          work->SetKostLsts (&Lsts);
          if (work->GetKostBez () == FALSE)
          {
                   disp_mess ("Falsche Material-Nr", 2);
                   eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
                   eListe.ShowAktRow ();
                   SetListFocus ();
                   return FALSE;
          }
          work->GetKostBez (Lsts.kost_art_bez, &Lsts.kost_wrt, me_einh, 
                            &Lsts.faktor, (short) atoi (Lsts.kost_art));

         if (Lsts.faktor == 0,0)
         {
             Lsts.faktor = 1.0;
         }
         sprintf (Lsts.kost_wert, "%.3lf", Lsts.kost_wrt);
         Lsts.vkost_rechb = atoi (me_einh);
         work->GetPtab (Lsts.me_einh, "vkost_rechb", clipped (me_einh));
         work->CalcKosten ();
         memcpy (&LstTab[eListe.GetAktRow ()], &Lsts, sizeof (Lsts));
         eListe.ShowAktRow ();
         return TRUE;
}


void KOSTLST::FillStufe (int pos)
{
          clipped (Lsts.stufe_bez);

          Lsts.stufe = 5;
          for (int i = 0; StufeCombo[i] != NULL; i ++)
          {
              if (strcmp ((Lsts.stufe_bez), clipped (StufeCombo [i])) == 0)
              {
                  Lsts.stufe = i + 1;
                  break;
              }
          }
          memcpy (&LstTab[pos], &Lsts, sizeof (Lsts));
}

BOOL KOSTLST::AfterCol (char *colname)
{
         
         if (strcmp (colname, "stufe_bez") == 0)
         {
             FillStufe (eListe.GetAktRow ());
             return FALSE;
         }
         else if (strcmp (colname, "chnr") == 0)
         {
//             if (eListe.IsNewRec ())
             {
                     ShowKostArt ();
                     CalcSum ();
             }
             return TRUE;
         }
         else if (strcmp (colname, "kost_art") == 0)
         {
             if (syskey == KEYUP && atoi (Lsts.kost_art) == 0)
             {
                 return FALSE;
             }
             if (TestKostArt () == FALSE)
             {
                 return TRUE;
             }
             CalcSum ();
         }
         else if (strcmp (colname, "kost_wert") == 0)
         {
             CalcSum ();
         }
         return FALSE;
}

int KOSTLST::AfterRow (int Row)
{

         if (syskey == KEYUP && atoi (Lsts.kost_art) == 0)
         {
                return OnKey7 ();
         }
         memcpy (&LstTab [Row], &Lsts, sizeof (Lsts));
         work->SetKostLsts (&Lsts);
         CalcSum ();
         return 1;
}

int KOSTLST::BeforeRow (int Row)
{
//         memcpy (&LstTab [Row], &Lsts, sizeof (Lsts));
         return 1;
}

int KOSTLST::ShowKostArt (void)
{

        SEARCHKA SearchKost;
        struct SKA *skost;
        char query [40];

       SearchKost.SetWindowStyle (WS_POPUP | WS_THICKFRAME);
       SearchKost.SetParams (DLG::hInstance, GetParent (hMainWindow));

        if (Dlg != NULL)
        {
               SearchKost.Setawin (((SpezDlg *) Dlg)->GethMainWindow ());
        }
            
        sprintf (query, "and mdn = %hd", prdk_k.mdn);
//        SearchKost.SetQuery (query);
        SearchKost.Search ();
        if (syskey == KEY5)
        {
            syskey = 0;
            SetListFocus ();
            return 0;
        }
        skost = SearchKost.GetSka ();
        if (skost == NULL)
        {
            return 0;
        }

        sprintf (LstTab [eListe.GetAktRow ()].kost_art, "%d", atoi (skost->kost_art));
        memcpy (&Lsts, &LstTab [eListe.GetAktRow ()], sizeof (Lsts));
        TestKostArt ();
        eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
        eListe.ShowAktRow ();
        SetListFocus ();
        return 0;
}


BOOL KOSTLST::StopList (void)
{
    int i;
    int recanz;
    double chg_gew = 0.0;

    if (AfterRow (eListe.GetAktRow ()) == -1)
    {
        return TRUE;
    }

    if (atol (Lsts.kost_art) == 0)
    {
        OnKey7 ();
    }

    else
    {
    }

    recanz = eListe.GetRecanz ();

    work->DeleteAllKostPos ();
    for (i = 0; i < recanz; i ++)
    {
        WriteRec (i);
//        chg_gew += prdk_kost.gew;
    }
//    work->UpdateChgGew (chg_gew);
//	eListe.BreakList ();
	return TRUE;
}

BOOL KOSTLST::OnKeyNext (void)
{
        if (GetKeyState (VK_CONTROL) >= 0)
        {
              if (Dlg != NULL)
              {
                  ((SpezDlg *) Dlg)->NextPage ();
                  return TRUE;
              }
        }
        return FALSE;
}


BOOL KOSTLST::OnKeyPrior (void)
{
        if (GetKeyState (VK_CONTROL) >= 0)
        {
              if (Dlg != NULL)
              {
                  ((SpezDlg *) Dlg)->PriorPage ();
                  return TRUE;
              }
        }
        return FALSE;
}

BOOL KOSTLST::OnKey5 (void)
{

    OnKey12 ();
    syskey = KEY5;
//	eListe.BreakList ();
	return TRUE;

}

BOOL KOSTLST::OnKey6 (void)
{
    eListe.InsertLine ();
    return TRUE;
}

BOOL KOSTLST::OnKeyInsert (void)
{
    return FALSE;
/*
    eListe.InsertLine ();
    return TRUE;
*/
}

BOOL KOSTLST::OnKey7 (void)
{
    eListe.DeleteLine ();
    CalcSum ();
    return TRUE;
}

BOOL KOSTLST::OnKeyDel (void)
{
    return FALSE;
/*
    eListe.DeleteLine ();
    CalcSum ();
    return TRUE;
*/
}

BOOL KOSTLST::OnKey8 (void)
{
         return FALSE;
}


BOOL KOSTLST::OnKey9 (void)
{
    char * Item = eListe.GetAktItem ();
    if (strcmp (Item, "stufe_bez") == 0)
    {
        eListe.ShowDropDown ();
        return TRUE;
    }
    else if (strcmp (Item, "kost_art") == 0)
    {
        ShowKostArt ();
        return TRUE;
    }
    return FALSE;
}


BOOL KOSTLST::OnKey10 (void)
{
    if (Dlg != NULL)
    {
         work->SetKostLsts (&Lsts);
// Hier Workfunktion einsetzen
         SetListFocus ();
         return TRUE;
	}
    return FALSE;
}

BOOL KOSTLST::OnKey11 (void)
{

    if (Dlg != NULL)
    {
        ((SpezDlg *) Dlg)->OnKey11 ();
        SetAktFocus ();
    }
    return FALSE;
}


int KOSTLST::ToMemory (int i)
{

    LISTENTER::ToMemory (i);
    return 0;
}


void KOSTLST::InitRow (void)
{

    LISTENTER::InitRow ();


    work->SetKostLsts (&Lsts);
    work->InitKostRow ();

/*
    if (ArrDown != NULL && Lsts.chtyp[0] != NULL)
    {
        Lsts.chtyp[0]->SetBitMap (ArrDown);
    }
*/


    if (ArrDown != NULL)
    {
        Lsts.chnr.SetBitMap (ArrDown);
    }


}

void KOSTLST::uebertragen (void)
{
    work->SetKostLsts (&Lsts);
    work->FillKostRow ();
    if (Lsts.stufe <= 0)
    {
          Lsts.stufe = 1;
    }
	for (int i = 0; StufeCombo[i] != NULL; i ++);
	if (Lsts.stufe <= i)
	{
          strcpy (Lsts.stufe_bez, StufeCombo [Lsts.stufe - 1]);
	}
	else
	{
          strcpy (Lsts.stufe_bez, StufeCombo [0]);
	}

    if (ArrDown != NULL)
    {
        Lsts.chtyp.SetBitMap (ArrDown);
    }
    if (ArrDown != NULL)
    {
        Lsts.chnr.SetBitMap (ArrDown);
    }
}

void KOSTLST::WriteRec (int i)
{
    memcpy (&Lsts, &LstTab[i], sizeof (Lsts));
    if (atol (Lsts.kost_art) == 0)
    {
        return;
    }
    FillStufe (i);
    work->SetKostLsts (&LstTab[i]);
    work->WriteKostRec ();
}

BOOL KOSTLST::OnKey12 (void)
{
	if (sys_ben.berecht >= 3) return TRUE;
    CalcSum ();
    StopList ();
    syskey = KEY12;
    return TRUE;
}


BOOL KOSTLST::OnKeyUp (void)
{
//    form *DataForm;
//    int fpos;
/*
    if (Leere Zeile)
    {
        return OnKeyDel ();
    }
*/

    return FALSE;
}

BOOL KOSTLST::OnKeyDown (void)
{
    if (atol (Lsts.kost_art) == 0l)
    {
        return TRUE;
    }

    return FALSE;
}


void KOSTLST::CreateListQuikInfos (HWND hWnd)
{
       if (Toolbar2 == NULL) return;

       Toolbar2->GetCfield ("f5")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("f12")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("f9")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("del")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("insert")->CreateQuikInfos (hWnd);
}


BOOL KOSTLST::OnCommand (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    if (LOWORD (wParam) == IDM_CHOISE)
    {
        return OnKey9 ();
    }
    if (LOWORD (wParam) == IDM_CANCEL)
    {
        return OnKey5 ();
    }
    if (LOWORD (wParam) == IDM_WRITE)
    {
        return OnKey12 ();
    }
    if (LOWORD (wParam) == VK_DELETE)
    {
        return OnKeyDel ();
    }
    if (LOWORD (wParam) == IDM_DELETE)
    {
        return OnKey7 ();
    }
    if (LOWORD (wParam) == VK_INSERT)
    {
        return OnKeyInsert ();
    }
    if (LOWORD (wParam) == IDM_INSERT)
    {
        return OnKey6 ();
    }
    if (HIWORD (wParam)  == CBN_CLOSEUP)
	{
        eListe.GetFocusText ();
        FillStufe (eListe.GetAktRow ());
        if (Dlg != NULL)
        {
	           return ((SpezDlg *) Dlg)->OnCloseUp (hWnd, msg, wParam, lParam);
        }
	}
    return FALSE;
}

BOOL KOSTLST::OnPaint (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    HDC hdc;

    if (Modal == FALSE) return FALSE;
    if (this->mamain1 != hWnd) return FALSE; 
    
    hdc = BeginPaint (hWnd, &ps);
    fChoise.display (hWnd, hdc);
    EndPaint (hWnd,&ps);
    return FALSE;
}

BOOL KOSTLST::OnMove (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{

    if (Modal == FALSE) return FALSE;
    if (this->mamain1 != hWnd) return FALSE; 

    fChoise.MoveWindow ();
    return FALSE;
}


BOOL KOSTLST::InFirstField ()
{
	if (eListe,GetAktRow () > 0)
	{
		return FALSE;
	}
	if (eListe.GetAktColumn () > 1)
	{
		return FALSE;
	}
	if (eListe.GetAktColumn () == 1 &&
		eListe.GetDataForm ()->mask[0].attribut != DISPLAYONLY)
	{
		return FALSE;
	}
	return TRUE;
}






