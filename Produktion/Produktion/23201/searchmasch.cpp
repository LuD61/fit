#include <windows.h>
#include "wmaskc.h"
#include "searchmasch.h"
#include "strfkt.h"
#include "mo_wmess.h"
#include "lbox.h"
#include "dlglst.h"
#include "a_bas.h"
#include "lbox.h"
#include "ptab.h"


struct SMASCH *SEARCHMASCH::smaschtab = NULL;
struct SMASCH SEARCHMASCH::smasch;
int SEARCHMASCH::idx = -1;
long SEARCHMASCH::maschanz;
CHQEX *SEARCHMASCH::Query = NULL;
DB_CLASS SEARCHMASCH::DbClass; 
HINSTANCE SEARCHMASCH::hMainInst;
HWND SEARCHMASCH::hMainWindow;
HWND SEARCHMASCH::awin;
int SEARCHMASCH::SearchField = 1;
int SEARCHMASCH::somasch_nr = 1; 
int SEARCHMASCH::somasch_bz = 1; 
int SEARCHMASCH::soprod_abt = 1; 
int SEARCHMASCH::somasch_typ = 1; 


ITEM SEARCHMASCH::umasch_nr   ("masch_nr",    "Maschine",            "", 0);  
ITEM SEARCHMASCH::umasch_bz   ("masch_bz",    "Bezeichnung",         "", 0);  
ITEM SEARCHMASCH::uprod_abt   ("prod_abt",    "Proukt.-Abteilung",   "", 0);  
ITEM SEARCHMASCH::umasch_typ   ("masch_typ",  "Maschinentyp",        "", 0);  


field SEARCHMASCH::_UbForm[] = {
&umasch_nr,   10, 0, 0,  0,  NULL, "", BUTTON, 0, 0, 0,     
&umasch_bz,   26, 0, 0, 10 , NULL, "", BUTTON, 0, 0, 0,     
&uprod_abt,   26, 0, 0, 36 , NULL, "", BUTTON, 0, 0, 0,     
&umasch_typ,  26, 0, 0, 62 , NULL, "", BUTTON, 0, 0, 0,     
};

form SEARCHMASCH::UbForm = {4, 0, 0, _UbForm, 0, 0, 0, 0, NULL};


ITEM SEARCHMASCH::imasch_nr   ("masch_nr",    smasch.masch_nr,     "", 0);  
ITEM SEARCHMASCH::imasch_bz   ("masch_bz",    smasch.masch_bz,     "", 0);  
ITEM SEARCHMASCH::iprod_abt   ("prod_abt",    smasch.prod_abt,     "", 0);  
ITEM SEARCHMASCH::imasch_typ  ("masch_typ",   smasch.masch_typ,    "", 0);  

field SEARCHMASCH::_DataForm[] = {
&imasch_nr,    8, 0, 0,  1,  NULL, "%8d",    DISPLAYONLY, 0, 0, 0,     
&imasch_bz,   24, 0, 0, 11 , NULL, "",       DISPLAYONLY, 0, 0, 0,     
&iprod_abt,   24, 0, 0, 38 , NULL, "",       DISPLAYONLY, 0, 0, 0,     
&imasch_typ,  26, 0, 0, 64 , NULL, "",       DISPLAYONLY, 0, 0, 0,     
};

form SEARCHMASCH::DataForm = {4, 0, 0, _DataForm, 0, 0, 0, 0, NULL};

ITEM SEARCHMASCH::iline ("", "1", "", 0);

field SEARCHMASCH::_LineForm[] = {
&iline,       1, 0, 0, 10 , NULL, "", DISPLAYONLY, 0, 0, 0,     
&iline,       1, 0, 0, 36 , NULL, "", DISPLAYONLY, 0, 0, 0,     
&iline,       1, 0, 0, 62 , NULL, "", DISPLAYONLY, 0, 0, 0,     
&iline,       1, 0, 0, 88 , NULL, "", DISPLAYONLY, 0, 0, 0,     
};

form SEARCHMASCH::LineForm = {4, 0, 0, _LineForm, 0, 0, 0, 0, NULL};

char *SEARCHMASCH::query = NULL;

int SEARCHMASCH::sortmasch (const void *elem1, const void *elem2)
{
	      struct SMASCH *el1; 
	      struct SMASCH *el2; 

		  el1 = (struct SMASCH *) elem1;
		  el2 = (struct SMASCH *) elem2;
	      return (atol (el1->masch_nr) - atol (el2->masch_nr)) * somasch_nr;
}

void SEARCHMASCH::SortMasch (HWND hWnd)
{
   	   qsort (smaschtab, maschanz, sizeof (struct SMASCH),
				   sortmasch);
       somasch_nr *= -1;
}

int SEARCHMASCH::sortmasch_bz (const void *elem1, const void *elem2)
{
	      struct SMASCH *el1; 
	      struct SMASCH *el2; 

		  el1 = (struct SMASCH *) elem1;
		  el2 = (struct SMASCH *) elem2;
          clipped (el1->masch_bz);
          clipped (el2->masch_bz);
          if (strlen (el1->masch_bz) == 0 &&
              strlen (el2->masch_bz) == 0)
          {
              return 0;
          }
          if (strlen (el1->masch_bz) == 0)
          {
              return -1;
          }
          if (strlen (el2->masch_bz) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->masch_bz,el2->masch_bz) * somasch_bz);
}

void SEARCHMASCH::SortMaschBz (HWND hWnd)
{
   	   qsort (smaschtab, maschanz, sizeof (struct SMASCH),
				   sortmasch_bz);
       somasch_bz *= -1;
}

int SEARCHMASCH::sortprod_abt (const void *elem1, const void *elem2)
{
	      struct SMASCH *el1; 
	      struct SMASCH *el2; 

		  el1 = (struct SMASCH *) elem1;
		  el2 = (struct SMASCH *) elem2;
          clipped (el1->prod_abt);
          clipped (el2->prod_abt);
          if (strlen (el1->prod_abt) == 0 &&
              strlen (el2->prod_abt) == 0)
          {
              return 0;
          }
          if (strlen (el1->prod_abt) == 0)
          {
              return -1;
          }
          if (strlen (el2->prod_abt) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->prod_abt,el2->prod_abt) * soprod_abt);
}

void SEARCHMASCH::SortProdAbt (HWND hWnd)
{
   	   qsort (smaschtab, maschanz, sizeof (struct SMASCH),
				   sortprod_abt);
       soprod_abt *= -1;
}

int SEARCHMASCH::sortmasch_typ (const void *elem1, const void *elem2)
{
	      struct SMASCH *el1; 
	      struct SMASCH *el2; 

		  el1 = (struct SMASCH *) elem1;
		  el2 = (struct SMASCH *) elem2;
          clipped (el1->prod_abt);
          clipped (el2->prod_abt);
          if (strlen (el1->masch_typ) == 0 &&
              strlen (el2->masch_typ) == 0)
          {
              return 0;
          }
          if (strlen (el1->masch_typ) == 0)
          {
              return -1;
          }
          if (strlen (el2->masch_typ) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->masch_typ,el2->masch_typ) * somasch_typ);
}

void SEARCHMASCH::SortMaschTyp (HWND hWnd)
{
   	   qsort (smaschtab, maschanz, sizeof (struct SMASCH),
				   sortmasch_typ);
       somasch_typ *= -1;
}


void SEARCHMASCH::SortLst (int Col, HWND hWnd)
/**
Procedure zum Sortiern der Liste.
**/
{
       switch (Col)
       {
              case 0 :
                  SortMasch (hWnd);
                  SearchField = 0;
                  break;
              case 1 :
                  SortMaschBz (hWnd);
                  SearchField = 1;
                  break;
              case 2 :
                  SortProdAbt (hWnd);
                  SearchField = 2;
                  break;
              case 3 :
                  SortMaschTyp (hWnd);
                  SearchField = 3;
                  break;
              default :
                  return;
       }
       UpdateList ();
}

void SEARCHMASCH::FillFormat (char *buffer, int size)
{
        DlgLst::FillFrmAttr (&DataForm, buffer, size);
}


void SEARCHMASCH::FillCaption (char *buffer, int size)
{
        DlgLst::FillFrmTitle (&UbForm, buffer, 0, size);
}

void SEARCHMASCH::FillVlines (char *buffer, int size) 
{
        DlgLst::FillFrmLines (&LineForm, buffer, 0, size);
}


void SEARCHMASCH::FillRec (char *buffer, int i, int size)
{
          DlgLst::FillFrmData (&DataForm, buffer, 0, size);
}


void SEARCHMASCH::UpdateList (void)
{
       int i;
 	   char buffer [512];

       for (i = 0; i < maschanz; i ++)
       {
		  memcpy (&smasch, &smaschtab[i],  sizeof (struct SMASCH));
          FillRec (buffer, i, 512);
	      Query->UpdateRecord (buffer, i);
       }
}

int SEARCHMASCH::SearchLst (char *sebuff)
/**
Nach Kurzname in Liste suchen.
**/
{
/*
	   Query->SearchList ();
	   return 0;
*/

	   int i;
	   int len;
//       char sabuff [80];
//       char *pos;

	   if (smaschtab == NULL) return 0;
	   if (strlen (sebuff) == 0) 
	   {
	       Query->SetSel (0);
		   return 0;
	   }

	   for (i = 0; i < maschanz; i ++)
	   {
		   len = min (16, strlen (sebuff));
           if (SearchField == 0)
           {
		           if (strupcmp (sebuff, smaschtab[i].masch_nr, len) == 0) break;
           }
           else if (SearchField == 1)
           {
		           if (strupcmp (sebuff, smaschtab[i].masch_bz, len) == 0) break;
           }
           else if (SearchField == 2)
           {
		           if (strupcmp (sebuff, smaschtab[i].prod_abt, len) == 0) break;
           }
           else if (SearchField == 3)
           {
		           if (strupcmp (sebuff, smaschtab[i].masch_typ, len) == 0) break;
           }
	   }
	   if (i == maschanz) return 0;
	   Query->SetSel (i);
	   return 0;
}


int SEARCHMASCH::Read (char *squery)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int cursor;
	  int i;
      WMESS Wmess;
      PTAB_CLASS Ptab;


	  if (smaschtab) 
	  {
           delete smaschtab;
           smaschtab = NULL;
	  }


      idx = -1;
	  maschanz = 0;
      SearchField = 1;
      Query->SetSBuff (NULL);
      clipped (squery);
      if (strcmp (squery, " ") == 0) squery[0] = 0;

      if (maschanz == 0) maschanz = 0x10000;

	  smaschtab = new struct SMASCH [maschanz];
      if (squery == NULL || 
          strlen (squery) == 0)
      {
             sprintf (buffer, "select masch_nr, masch_bz, prod_abt, masch_typ "
	 			       "from maschinen "
                       "where masch_nr > 0"); 
              if (query != NULL && strlen (query) > 0)
              {
                  strcat (buffer, " ");
                  strcat (buffer, query);
              }

      }
      else
      {
             sprintf (buffer, "select masch_nr, masch_bz, prod_abt, masch_typ "
	 			       "from maschinen "
                       "where masch_bz matches \"%s*\"", squery); 
              if (query != NULL && strlen (query) > 0)
              {
                  strcat (buffer, " ");
                  strcat (buffer, query);
              }
      }
      DbClass.sqlout ((char *) smasch.masch_nr, 0, 9);
      DbClass.sqlout ((char *) smasch.masch_bz, 0, 73);
      DbClass.sqlout ((char *) smasch.prod_abt, 0, 5);
      DbClass.sqlout ((char *) smasch.masch_typ, 0, 5);
      cursor = DbClass.sqlcursor (buffer);
      if (cursor < 0) 
      {
          return -1;
      }

  	  i = 0;
	  while (DbClass.sqlfetch (cursor) == 0)
      {
          int dsqlstatus = Ptab.lese_ptab ("prod_abt", smasch.prod_abt);
          if (dsqlstatus == 0)
          {
              strcpy (smasch.prod_abt, ptabn.ptbezk);
          }
          dsqlstatus = Ptab.lese_ptab ("prodphase", smasch.masch_typ);
          if (dsqlstatus == 0)
          {
              strcpy (smasch.masch_typ, ptabn.ptbezk);
          }
		  memcpy (&smaschtab[i], &smasch, sizeof (struct SMASCH));
          FillRec (buffer, i, 512);
	      Query->InsertRecord (buffer);
		  i ++;
	  }
      maschanz = i;
	  DbClass.sqlclose (cursor);

      somasch_nr = 1;
      if (maschanz > 1)
      {
             SortMasch (Query->GethWnd ());
      }
      UpdateList ();

	  EnableWindow (awin, FALSE);
	  return 0;
}

void SEARCHMASCH::FillBox (void)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int i;

      for (i = 0; i < maschanz; i ++)
      {
		  memcpy (&smasch, &smaschtab[i],  sizeof (struct SMASCH));
          FillRec (buffer, i, 512);
	      Query->InsertRecord (buffer);
      }
      if (idx > -1)
      {
	      Query->SetSel (idx);
      }
}


void SEARCHMASCH::SetParams (HINSTANCE hMainInst, HWND hMainWindow)
{
      this->hMainInst   = hMainInst;
      this->hMainWindow = hMainWindow;
      awin = hMainWindow;
}


void SEARCHMASCH::Search (void)
{
  	  int cx, cy;
	  char buffer [512];
      RECT rect;
      BOOL doret = FALSE; 

      DlgLst::TChar = 0xFF;
      Settchar ((char) DlgLst::TChar);
      GetClientRect (hMainWindow, &rect);
      cx = 80;
      cy = 25;
      SetSortProc (SortLst);
      if (Query == NULL)
      {
        idx = -1;
        if (query != NULL && strlen (query) > 0)
        {
               Query = new CHQEX (cx, cy);
        }
        else
        {
               Query = new CHQEX (cx, cy);
//               Query = new CHQEX (cx, cy, "Name", "");
        }
        Query->EnableSort (TRUE);
        if (SetStyle)
        {
            Query->SetWindowStyle (WindowStyle);
        }
        Query->OpenWindow (hMainInst, hMainWindow, TRUE);
        FillFormat (buffer, 512);
        Query->RowAttr (buffer);
        FillVlines (buffer, 512);
	    Query->VLines (buffer, 3);
	    EnableWindow (awin, FALSE);

        FillCaption (buffer, 512);
	    Query->InsertCaption (buffer);

	    Query->SetFillDb (Read);
	    Query->SetSearchLst (SearchLst);
//        if (query != NULL && strlen (query) > 0)
        {
              Read ("");
              if (maschanz == 0)
              {
                  doret = TRUE;
              }
        }
      }
      else
      {
        Query->RestorefWork ();
        if (SetStyle)
        {
            Query->SetWindowStyle (WindowStyle);
        }
        Query->OpenWindow (hMainInst, hMainWindow, TRUE);
        FillFormat (buffer, 512);
        Query->RowAttr (buffer);
        FillVlines (buffer, 512);
	    Query->VLines (buffer, 3);
	    EnableWindow (awin, FALSE);

        FillCaption (buffer, 512);
	    Query->InsertCaption (buffer);

	    Query->SetFillDb (Read);
	    Query->SetSearchLst (SearchLst);
        FillBox ();
      }
      if (doret == FALSE)
      {
          Query->ProcessMessages ();
          idx = Query->GetSel ();
      }
      else
      {
          idx = -1;
      }
      query = NULL;
      EnableWindow (awin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (awin);
      if (smaschtab == NULL)
      {
          delete Query;
          Query = NULL;
      }
	  if (idx == -1) return;
	  memcpy (&smasch, &smaschtab[idx], sizeof (smasch));
      SetSortProc (NULL);
      if (smaschtab != NULL)
      {
          Query->SavefWork ();
      }
}



