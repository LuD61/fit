#include <windows.h>
#include "wmaskc.h"
#include "searchvarb.h"
#include "strfkt.h"
#include "mo_wmess.h"
#include "lbox.h"
#include "dlglst.h"
#include "a_bas.h"


struct SVARB *SEARCHVARB::svarbtab = NULL;
struct SVARB SEARCHVARB::svarb;
int SEARCHVARB::idx = -1;
long SEARCHVARB::varbanz;
CHQEX *SEARCHVARB::Query = NULL;
DB_CLASS SEARCHVARB::DbClass; 
HINSTANCE SEARCHVARB::hMainInst;
HWND SEARCHVARB::hMainWindow;
HWND SEARCHVARB::awin;
int SEARCHVARB::SearchField = 1;
int SEARCHVARB::somat = 1; 
int SEARCHVARB::soa = 1; 
int SEARCHVARB::soa_bz1 = 1; 
short SEARCHVARB::mdn = 0; 


ITEM SEARCHVARB::ua        ("a",         "ArtikelNr.",    "", 0);  
ITEM SEARCHVARB::umat      ("mat",       "MaterialNr.",   "", 0);  
ITEM SEARCHVARB::ua_bz1    ("a_bz1" ,    "Bezeichnung",   "", 0);  


field SEARCHVARB::_UbForm[] = {
&ua,        15, 0, 0,  0,  NULL, "", BUTTON, 0, 0, 0,     
&umat,      10, 0, 0, 15 , NULL, "", BUTTON, 0, 0, 0,     
&ua_bz1,    50, 0, 0, 28 , NULL, "", BUTTON, 0, 0, 0,     
};

form SEARCHVARB::UbForm = {3, 0, 0, _UbForm, 0, 0, 0, 0, NULL};

ITEM SEARCHVARB::ia        ("a",         svarb.a,      "", 0);  
ITEM SEARCHVARB::imat      ("mat",       svarb.mat,    "", 0);  
ITEM SEARCHVARB::ia_bz1    ("a_bz1" ,    svarb.a_bz1,  "", 0);  

field SEARCHVARB::_DataForm[] = {
&ia,        13, 0, 0,  1,  NULL, "%13.0lf", DISPLAYONLY, 0, 0, 0,     
&imat   ,    8, 0, 0, 16 , NULL, "%8d",     DISPLAYONLY, 0, 0, 0,     
&ia_bz1,    50, 0, 0, 25 , NULL, "",        DISPLAYONLY, 0, 0, 0,     
};

form SEARCHVARB::DataForm = {3, 0, 0, _DataForm, 0, 0, 0, 0, NULL};

ITEM SEARCHVARB::iline ("", "1", "", 0);

field SEARCHVARB::_LineForm[] = {
&iline,       1, 0, 0, 15 , NULL, "", DISPLAYONLY, 0, 0, 0,     
&iline,       1, 0, 0, 25 , NULL, "", DISPLAYONLY, 0, 0, 0,     
};

form SEARCHVARB::LineForm = {2, 0, 0, _LineForm, 0, 0, 0, 0, NULL};

char *SEARCHVARB::query = NULL;

int SEARCHVARB::sortmat (const void *elem1, const void *elem2)
{
	      struct SVARB *el1; 
	      struct SVARB *el2; 

		  el1 = (struct SVARB *) elem1;
		  el2 = (struct SVARB *) elem2;
	      return ((atol (el1->mat) - atol (el2->mat)) * somat);
}

void SEARCHVARB::SortMat (HWND hWnd)
{
   	   qsort (svarbtab, varbanz, sizeof (struct SVARB),
				   sortmat);
       somat *= -1;
}


int SEARCHVARB::sorta (const void *elem1, const void *elem2)
{
	      struct SVARB *el1; 
	      struct SVARB *el2; 

		  el1 = (struct SVARB *) elem1;
		  el2 = (struct SVARB *) elem2;
          clipped (el1->a);
          clipped (el2->a);

          if (ratod (el1->a) > ratod (el2->a))
          {
              return 1 * soa;
          }
          else if (ratod (el1->a) < ratod (el2->a))
          {
              return -1 * soa;
          }

          return 0;
}

void SEARCHVARB::SortA (HWND hWnd)
{
   	   qsort (svarbtab, varbanz, sizeof (struct SVARB),
				   sorta);
       soa *= -1;
}

int SEARCHVARB::sorta_bz1 (const void *elem1, const void *elem2)
{
	      struct SVARB *el1; 
	      struct SVARB *el2; 

		  el1 = (struct SVARB *) elem1;
		  el2 = (struct SVARB *) elem2;
          clipped (el1->a_bz1);
          clipped (el2->a_bz1);
          if (strlen (el1->a_bz1) == 0 &&
              strlen (el2->a_bz1) == 0)
          {
              return 0;
          }
          if (strlen (el1->a_bz1) == 0)
          {
              return -1;
          }
          if (strlen (el2->a_bz1) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->a_bz1,el2->a_bz1) * soa_bz1);
}

void SEARCHVARB::SortABz1 (HWND hWnd)
{
   	   qsort (svarbtab, varbanz, sizeof (struct SVARB),
				   sorta_bz1);
       soa_bz1 *= -1;
}


void SEARCHVARB::SortLst (int Col, HWND hWnd)
/**
Procedure zum Sortiern der Liste.
**/
{
       switch (Col)
       {
              case 0 :
                  SortA (hWnd);
                  SearchField = 0;
                  break;
              case 1 :
                  SortMat (hWnd);
                  SearchField = 1;
                  break;
              case 2 :
                  SortABz1 (hWnd);
                  SearchField = 2;
                  break;
              default :
                  return;
       }
       UpdateList ();
}

void SEARCHVARB::FillFormat (char *buffer, int size)
{
        DlgLst::FillFrmAttr (&DataForm, buffer, size);
}


void SEARCHVARB::FillCaption (char *buffer, int size)
{
        DlgLst::FillFrmTitle (&UbForm, buffer, 0, size);
}

void SEARCHVARB::FillVlines (char *buffer, int size) 
{
        DlgLst::FillFrmLines (&LineForm, buffer, 0, size);
}


void SEARCHVARB::FillRec (char *buffer, int i, int size)
{
          DlgLst::FillFrmData (&DataForm, buffer, 0, size);
}


void SEARCHVARB::UpdateList (void)
{
       int i;
 	   char buffer [512];

       for (i = 0; i < varbanz; i ++)
       {
		  memcpy (&svarb, &svarbtab[i],  sizeof (struct SVARB));
          FillRec (buffer, i, 512);
	      Query->UpdateRecord (buffer, i);
       }
}

int SEARCHVARB::SearchLst (char *sebuff)
/**
Nach Kurzname in Liste suchen.
**/
{
/*
	   Query->SearchList ();
	   return 0;
*/
	   int i;
	   int len;
       char sabuff [80];
       char *pos;

	   if (svarbtab == NULL) return 0;
	   if (strlen (sebuff) == 0) 
	   {
	       Query->SetSel (0);
		   return 0;
	   }

	   for (i = 0; i < varbanz; i ++)
	   {
		   len = min (16, strlen (sebuff));
           if (SearchField == 0)
           {
                   strcpy (sabuff, svarbtab[i].a);
                   for (pos = sabuff; *pos <= ' ' && *pos != 0 ; pos ++);
                   pos[len] = 0;
                   strcpy (sabuff, pos);
                   if (ratod (sebuff) == ratod (sabuff)) break;
           }
           else if (SearchField == 1)
           {
                   strcpy (sabuff, svarbtab[i].mat);
                   for (pos = sabuff; *pos <= ' ' && *pos != 0 ; pos ++);
                   pos[len] = 0;
                   strcpy (sabuff, pos);
                   if (atol (sebuff) == atol (sabuff)) break;
           }
           else if (SearchField == 2)
           {
		           if (strupcmp (sebuff, svarbtab[i].a_bz1, len) == 0) break;
           }
	   }
	   if (i == varbanz) return 0;
	   Query->SetSel (i);
	   return 0;
}


int SEARCHVARB::Read (char *squery)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  char a_bz2 [25];
	  int cursor;
	  int i;
      WMESS Wmess;


	  if (svarbtab) 
	  {
           delete svarbtab;
           svarbtab = NULL;
	  }


      idx = -1;
	  varbanz = 0;
      SearchField = 2;
      Query->SetSBuff (NULL);
      clipped (squery);
      if (strcmp (squery, " ") == 0) squery[0] = 0;

      if (varbanz == 0) varbanz = 0x10000;

	  svarbtab = new struct SVARB [varbanz];
      if (squery == NULL || 
          strlen (squery) == 0)
      {
             sprintf (buffer, "select a_mat.a, a_mat.mat, a_bz1, a_bz2 "
	 			       "from a_mat, a_bas "
                       "where a_mat.mat > 0 "
                       "and a_bas.a = a_mat.a"); 
              if (query != NULL && strlen (query) > 0)
              {
                  strcat (buffer, " ");
                  strcat (buffer, query);
              }

              DbClass.sqlout ((char *) svarb.a, 0, 14);
              DbClass.sqlout ((char *) svarb.mat, 0, 9);
              DbClass.sqlout ((char *) svarb.a_bz1, 0, 25);
              DbClass.sqlout ((char *) a_bz2, 0, 25);
              cursor = DbClass.sqlcursor (buffer);
      }
      else
      {
             sprintf (buffer, "select a_mat.a, a_mat.mat, a_bz1, a_bz2 "
	 			       "from a_mat, a_bas "
                       "where a_mat.mat > 0 "
                       "and a_mat.a = a_bas.a " 
//                       "and rez_bz matches \"%s*\"", squery); 
						"and (a_bz1 matches \"%s*\" or a_bz2 matches \"%s*\")", squery,squery);
              if (query != NULL && strlen (query) > 0)
              {
                  strcat (buffer, " ");
                  strcat (buffer, query);
              }
              DbClass.sqlout ((char *) svarb.a, 0, 14);
              DbClass.sqlout ((char *) svarb.mat, 0, 9);
              DbClass.sqlout ((char *) svarb.a_bz1, 0, 25);
              DbClass.sqlout ((char *) a_bz2, 0, 25);
              cursor = DbClass.sqlcursor (buffer);
      }
      if (cursor < 0) 
      {
          return -1;
      }

  	  i = 0;
	  while (DbClass.sqlfetch (cursor) == 0)
      {
/*
          strcpy (_a_bas.a_bz1, "");
          lese_a_bas (ratod (svarb.a));
          strcpy (svarb.a_bz1, _a_bas.a_bz1);
*/ 
		  sprintf (svarb.a_bz1, "%s %s", svarb.a_bz1, a_bz2);
		  memcpy (&svarbtab[i], &svarb, sizeof (struct SVARB));
          FillRec (buffer, i, 512);
	      Query->InsertRecord (buffer);
		  i ++;
	  }
      varbanz = i;
	  DbClass.sqlclose (cursor);

      soa_bz1 = 1;
      if (varbanz > 1)
      {
             SortABz1 (Query->GethWnd ());
      }
      UpdateList ();

	  EnableWindow (awin, FALSE);
	  return 0;
}

void SEARCHVARB::FillBox (void)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int i;

      for (i = 0; i < varbanz; i ++)
      {
		  memcpy (&svarb, &svarbtab[i],  sizeof (struct SVARB));
          FillRec (buffer, i, 512);
	      Query->InsertRecord (buffer);
      }
      if (idx > -1)
      {
	      Query->SetSel (idx);
      }
}


void SEARCHVARB::SetParams (HINSTANCE hMainInst, HWND hMainWindow)
{
      this->hMainInst   = hMainInst;
      this->hMainWindow = hMainWindow;
      awin = hMainWindow;
}


void SEARCHVARB::Search (void)
{
  	  int cx, cy;
	  char buffer [512];
      RECT rect;
      BOOL doret = FALSE; 

      DlgLst::TChar = 0xFF;
      Settchar ((char) DlgLst::TChar);
      GetClientRect (hMainWindow, &rect);
      cx = 80;
      cy = 25;
//      EnableWindow (hMainWindow, FALSE);
      SetSortProc (SortLst);
      if (Query == NULL)
      {
        idx = -1;
        if (query != NULL && strlen (query) > 0)
        {
               Query = new CHQEX (cx, cy);
        }
        else
        {

			if (Suchvariante == 1)
			{
               Query = new CHQEX (cx, cy, "Name", "");
			}
			else
			{
               Query = new CHQEX (cx, cy);
			}
        }
        Query->EnableSort (TRUE);
        if (SetStyle)
        {
            Query->SetWindowStyle (WindowStyle);
        }
        Query->OpenWindow (hMainInst, hMainWindow, TRUE);
        FillFormat (buffer, 512);
        Query->RowAttr (buffer);
        FillVlines (buffer, 512);
	    Query->VLines (buffer, 3);
	    EnableWindow (awin, FALSE);

        FillCaption (buffer, 512);
	    Query->InsertCaption (buffer);

	    Query->SetFillDb (Read);
	    Query->SetSearchLst (SearchLst);
//        if (query != NULL && strlen (query) > 0)
        {
              Read ("");
              if (varbanz == 0)
              {
                  doret = TRUE;
              }
        }
      }
      else
      {
        Query->RestorefWork ();
        if (SetStyle)
        {
            Query->SetWindowStyle (WindowStyle);
        }
        Query->OpenWindow (hMainInst, hMainWindow, TRUE);
        FillFormat (buffer, 512);
        Query->RowAttr (buffer);
        FillVlines (buffer, 512);
	    Query->VLines (buffer, 3);
	    EnableWindow (awin, FALSE);

        FillCaption (buffer, 512);
	    Query->InsertCaption (buffer);

	    Query->SetFillDb (Read);
	    Query->SetSearchLst (SearchLst);
        FillBox ();
      }
      if (doret == FALSE)
      {
          Query->ProcessMessages ();
          idx = Query->GetSel ();
      }
      else
      {
          idx = -1;
      }
      query = NULL;
      EnableWindow (awin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (awin);
      if (svarbtab == NULL)
      {
          delete Query;
          Query = NULL;
      }
	  if (idx == -1) return;
	  memcpy (&svarb, &svarbtab[idx], sizeof (svarb));
      SetSortProc (NULL);
      if (svarbtab != NULL)
      {
          Query->SavefWork ();
      }
}



