//050210 prdk_kost nicht mehr variantenspezifisch
//210807 Verpackungen Schwund ber�cksichtigen
//240507 Schwund richtig handeln
//251005
//190505
//100605 Planek 
#include <windows.h>
#include <stdio.h>
#include "Work.h"
#include "strfkt.h"
#include "spezdlg.h"
#include "searchvarb.h"
#include "dlg.h"
#include "text.h"

#include "searchmdn.h"
#include "searchrez.h"
#include "searchvariante.h"
#include "searchmasch.h"
#include "searchvarb.h"
#include "searchzut.h"
#include "searchhuel.h"
#include "searchvpk.h"


#define TLEN 60
static short PreisHerk = 1;
static int PlanPreisHerk = 0;
static int MaterialOhneKosten = 0;
static int RezPreiseSpeichern = 2;
static int RezOhneVarianten = 0;
static int LeseA_Preism = 0;


	double Artikel = 0.0; 
	double fFE = 0.0;
	double fFett = 0.0;
	double fBEFFE = 0.0;
	double fh2o = 0.0;
    double fFlKg = 0.0;
//	double fFLProz = 0.0;
	double fFettProz =  0.0;
	double fFEProz = 0.0;
	double fBEFFEProz = 0.0;
	double fChargengew = 0.0;
	double fSchwund = 0.0;
	double fVarb_gew = 0.0;
	double fVarb_gew_nto = 0.0;
	double fchg_gew = 0.0;
	char cRez [10];
	int dVariante = 0;

int Work::ReadPrdk_k (short mdn, double a, short variante)
{
    int dsqlstatus;

    prdk_k.mdn = mdn;
    prdk_k.a   = a;
    prdk_k.variante = variante;
    dsqlstatus = Prdk_k.dbreadfirst ();
    return dsqlstatus;
}

int Work::ReadPrdk_k (short mdn) 
{
    int dsqlstatus;

    prdk_k.mdn = mdn;
    prdk_k.a = atof (A_Lsts->nr);
    prdk_k.variante = atoi (A_Lsts->variante);
    strcpy(prdk_k.rez, A_Lsts->rez);
    dsqlstatus = Prdk_k.dbreadfirst ();
    return dsqlstatus;
}

int Work::ReadPrdk_k (short mdn, char *rez, short variante)
{
    int dsqlstatus;

    Prdk_k.sqlin ((short *)   &mdn, 1, 0);
    Prdk_k.sqlin ((char *)    rez,  0, 8);
    Prdk_k.sqlin ((short *)   &variante, 1, 0);
    Prdk_k.sqlout ((double *) &prdk_k.a, 3, 0);
    dsqlstatus = Prdk_k.sqlcomm ("select a from prdk_k "
                                 "where mdn = ? "
                                 "and rez = ? "
                                 "and variante = ?");
    if (dsqlstatus == 0)
    {
        return ReadPrdk_k (mdn, prdk_k.a, variante);
    }
    return dsqlstatus;
}


int Work::WritePrdk_k (short mdn, double a, char *rez, char *kalkzeit, char *rez_bz)
{
    extern short sql_mode;
	double rez_mat_o_b;
	int dsqlstatus;
	char datum[12];
    prdk_k.mdn = mdn;
    prdk_k.a   = a;  
    strcpy (prdk_k.rez, rez);
    strcpy (prdk_k.rez_bz, rez_bz);
    sysdate (datum);
	prdk_k.dat = dasc_to_long (datum);
	strcpy (prdk_k.kalk_stat, "K"); 
    a_preis.mdn = prdk_k.mdn;
    a_preis.a   = prdk_k.a;


    double SwProz = prdk_k.sw_kalk / 100;
	double Menge_bto = prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew;
	double Menge_nto = prdk_k.varb_gew_nto + prdk_k.zut_gew + prdk_k.huel_gew;
    double SwMe = (prdk_k.varb_gew - prdk_k.varb_gew_nto) + (SwProz * Menge_nto );
	SwProz = (Menge_bto -  (Menge_bto - SwMe)) / Menge_bto ;

	double varb_wrt = prdk_k.varb_mat_o_b   * prdk_k.chg_gew;
	double zut_wrt = prdk_k.zut_mat_o_b   * prdk_k.chg_gew;
	double rez_gew =	prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew + 
				 							    (SwProz * (prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew) * -1) ;


		rez_mat_o_b =  Round(((varb_wrt + zut_wrt + prdk_k.huel_wrt) / rez_gew),3);



    if (APreis.dbreadfirst () == 100)
	{
		APreis.dbupdate (); //300905 wenn noch nichts da, unbedingt anlegen, da sonst kein Ausdruck 
    }
	double a_gew = GetRezAGew ();   //#180803
	if (PlanEk) //100605
	{
		a_preis.plan_ek  = prdk_k.a_hk_teilk * a_gew;
		a_preis.plan_bep  = prdk_k.a_hk_vollk * a_gew;
	}
	else
	{
	    a_preis.ek  = prdk_k.a_hk_teilk * a_gew;
		a_preis.bep  = prdk_k.a_hk_vollk * a_gew;
	}
		rez_mat_o_b =  rez_mat_o_b * a_gew;
//    systime(a_preis.zeit);
	strcpy (a_preis.zeit, kalkzeit);
    sysdate (datum);
	a_preis.dat = dasc_to_long (datum);
	(a_preis.inh_ek < 0.0001) ? a_preis.inh_ek = 0 : a_preis.inh_ek; //LuD #130503 Null-Value abfangen bei Insert 
	if (prdk_k.akv == 1) 
    {
		APreis.dbupdate ();   //#180903 update a_preis nur bei aktiven


	    if ((dsqlstatus = lese_a_bas (a)) == 100)
	    {
			return dsqlstatus;
		}
		if (!PlanEk && RezPreiseSpeichern > 0) //100605 //251005  Nicht beim PlanEK, sondern nur bei normaler Kalk.
		{
		    a_kalkpreis.mdn = prdk_k.mdn;
			a_kalkpreis.fil = 0;
			a_kalkpreis.a   = prdk_k.a;
			AKalkPreis.dbreadfirst ();
//			a_kalkpreis.mat_o_b  = prdk_k.a_hk_teilk * a_gew;
			a_kalkpreis.mat_o_b  = rez_mat_o_b;
			a_kalkpreis.hk_teilk  = (prdk_k.a_hk_vollk * a_gew) - (prdk_k.vpk_hk_vollk * a_gew);
			a_kalkpreis.hk_vollk  = prdk_k.a_hk_vollk * a_gew;
			a_kalkpreis.sk_vollk  = prdk_k.a_sk_vollk * a_gew;
			a_kalkpreis.fil_ek_vollk = prdk_k.a_filek_vollk * a_gew;
            a_kalkpreis.fil_vk_vollk = prdk_k.a_filvk_vollk * a_gew;  
			a_kalkpreis.dat = dasc_to_long (datum);
			a_kalkpreis.kost = a_kalkpreis.hk_vollk - a_kalkpreis.mat_o_b; //230310
			strcpy (a_kalkpreis.zeit,kalkzeit);
            sprintf(a_kalkpreis.programm,"%s","23201");
            sprintf(a_kalkpreis.version,"%s",VERSION);
			AKalkPreis.dbupdate ();
			if (RezPreiseSpeichern == 2 && prdk_k.mdn > 0) // f�r "Multimandant"-Kalkulation (H�nnger) immer auch in Mdn = 0 
									     // aus a_kalkpreis mit mdn = 0 wird dann bie der int. kalkulation in die Mandanten 1 und 2 verteilt
			{
			    a_kalkpreis.mdn = 0;
				a_kalkpreis.fil = 0;
				a_kalkpreis.a   = prdk_k.a;
				AKalkPreis.dbreadfirst ();
				a_kalkpreis.mat_o_b  = rez_mat_o_b;
				a_kalkpreis.hk_teilk  = (prdk_k.a_hk_vollk * a_gew) - (prdk_k.vpk_hk_vollk * a_gew);
				a_kalkpreis.hk_vollk  = prdk_k.a_hk_vollk * a_gew;
				a_kalkpreis.sk_vollk  = prdk_k.a_sk_vollk * a_gew;
				a_kalkpreis.fil_ek_vollk = prdk_k.a_filek_vollk * a_gew;
				a_kalkpreis.fil_vk_vollk = prdk_k.a_filvk_vollk * a_gew;  
				a_kalkpreis.dat = dasc_to_long (datum);
				a_kalkpreis.kost = a_kalkpreis.hk_vollk - a_kalkpreis.mat_o_b; //230310
				strcpy (a_kalkpreis.zeit,kalkzeit);
				sprintf(a_kalkpreis.programm,"%s","23201");
				sprintf(a_kalkpreis.version,"%s",VERSION);
				AKalkPreis.dbupdate ();
			}

		if (RezPreiseSpeichern == 9 )  //z.b. f�r Merte
		{
		    a_pr.mdn = prdk_k.mdn;
			a_pr.fil = 0;
			a_pr.a   = prdk_k.a;
			a_pr.fil_gr   = 99;
			APr.dbreadfirst ();
			a_pr.pr_ek  = prdk_k.a_sk_vollk * a_gew;
			a_pr.pr_ek_euro  = prdk_k.a_sk_vollk * a_gew;
			a_pr.bearb = dasc_to_long (datum);
            sprintf(a_pr.pers_nam,"%s","23201");
			APr.dbupdate ();
		}
 	    switch (_a_bas.a_typ)
	    {

			case EIG :
			    a_kalk_eig.mdn = prdk_k.mdn;
				a_kalk_eig.fil = 0;
				a_kalk_eig.a   = prdk_k.a;
				AKalkEig.dbreadfirst ();
//				a_kalk_eig.mat_o_b  = prdk_k.a_hk_teilk * a_gew;
				a_kalk_eig.mat_o_b  = rez_mat_o_b;
				// hk_teilk wird bei Grundbr�ten herangezogen  = Preise incl. H�llen) 090207
				a_kalk_eig.hk_teilk  = (prdk_k.a_hk_vollk * a_gew) - (prdk_k.vpk_hk_vollk * a_gew);
				a_kalk_eig.hk_vollk  = prdk_k.a_hk_vollk * a_gew;
				a_kalk_eig.sk_vollk  = prdk_k.a_sk_vollk * a_gew;
				a_kalk_eig.fil_ek_vollk = prdk_k.a_filek_vollk * a_gew;
                a_kalk_eig.lad_vk_vollk = prdk_k.a_filvk_vollk * a_gew;  
				a_kalk_eig.kost = a_kalk_eig.hk_vollk - a_kalk_eig.mat_o_b; //230310
				a_kalk_eig.aend_dat = dasc_to_long (datum);
				AKalkEig.dbupdate ();
	            break;
	        case HNDW :
				a_kalkhndw.mdn = prdk_k.mdn;
				a_kalkhndw.fil = 0;
				a_kalkhndw.a   = prdk_k.a;
				AKalkHndw.dbreadfirst ();
				a_kalkhndw.sk_teilk  = prdk_k.a_hk_teilk * a_gew;
				a_kalkhndw.sk_vollk  = prdk_k.a_sk_vollk * a_gew;
				a_kalkhndw.pr_ek1  = prdk_k.a_hk_vollk * a_gew;
				a_kalkhndw.fil_ek_vollk = prdk_k.a_filek_vollk * a_gew;
                a_kalkhndw.lad_vk_vollk = prdk_k.a_filvk_vollk * a_gew;  
				a_kalkhndw.aend_dat = dasc_to_long (datum);
				AKalkHndw.dbupdate ();
				break;
		}
	    switch (_a_bas.a_typ2)
	    {
			case EIG :
			    a_kalk_eig.mdn = prdk_k.mdn;
				a_kalk_eig.fil = 0;
				a_kalk_eig.a   = prdk_k.a;
				AKalkEig.dbreadfirst ();
//				a_kalk_eig.mat_o_b  = prdk_k.a_hk_teilk * a_gew;
				a_kalk_eig.mat_o_b  = rez_mat_o_b;
				a_kalk_eig.hk_teilk  = prdk_k.a_hk_teilk * a_gew;
				a_kalk_eig.hk_vollk  = prdk_k.a_hk_vollk * a_gew;
				a_kalk_eig.sk_vollk  = prdk_k.a_sk_vollk * a_gew;
				a_kalk_eig.fil_ek_vollk = prdk_k.a_filek_vollk * a_gew;
                a_kalk_eig.lad_vk_vollk = prdk_k.a_filvk_vollk * a_gew;  
				a_kalk_eig.kost = a_kalk_eig.hk_vollk - a_kalk_eig.mat_o_b; //230310
				a_kalk_eig.aend_dat = dasc_to_long (datum);
				AKalkEig.dbupdate ();
	            break;
	        case HNDW :
				a_kalkhndw.mdn = prdk_k.mdn;
				a_kalkhndw.fil = 0;
				a_kalkhndw.a   = prdk_k.a;
				AKalkHndw.dbreadfirst ();
				a_kalkhndw.sk_teilk  = prdk_k.a_hk_teilk * a_gew;
				a_kalkhndw.sk_vollk  = prdk_k.a_sk_vollk * a_gew;
				a_kalkhndw.pr_ek1  = prdk_k.a_hk_vollk * a_gew;
				a_kalkhndw.fil_ek_vollk = prdk_k.a_filek_vollk * a_gew;
                a_kalkhndw.lad_vk_vollk = prdk_k.a_filvk_vollk * a_gew;  
				a_kalkhndw.aend_dat = dasc_to_long (datum);
				AKalkHndw.dbupdate ();
				break;
		}
		//101105
        short sqlmode = sql_mode;
		sql_mode = 1;
		Prdk_k.sqlin ((double *) &a_kalk_eig.sk_vollk,   3, 0);
	    Prdk_k.sqlin ((double *) &_a_bas.a,   3, 0);
		if (Prdk_k.sqlcomm ("update a_bas set sk_vollk = ? "
                    "where a = ? ") < 0)
		{
					disp_mess("SK-Preis konnte nicht zur�ckgeschrieben werden: Artikel gesperrt !",2);
		}
		sql_mode = sqlmode;
		}  //PlanEK


    }

	prdk_k.rez_mat_o_b = rez_mat_o_b;
    Prdk_k.dbupdate ();
	prdk_name.mdn = prdk_k.mdn;
	strcpy (prdk_name.rez,prdk_k.rez);
	strcpy (prdk_name.rez_bz,prdk_k.rez_bz);
    Prdk_name.dbupdate ();

    return 0;
}

void Work::ReportRezUpdate (void)
{
    ReportRez.dbupdate ();
}

int Work::ReportDelete (void)
{
        ReportRez.sqlin ((double *)  &prdk_k.a, 3, 0);
        ReportRez.sqlin ((short *)  &prdk_k.variante, 1, 0);

        return ReportRez.sqlcomm ("delete from reportrez where a = ? and variante = ?");
}


int Work::TestAkv (short mdn, double a, short variante, short akv)
{
    if (akv == 0)
    {
/*
        Prdk_k.sqlin ((short *)  &mdn, 1, 0);
        Prdk_k.sqlin ((double *) &a,   3, 0);
        Prdk_k.sqlin ((short *) &variante,   1, 0);
        int dsqlstatus = Prdk_k.sqlcomm ("select a from prdk_k "
                                         "where mdn = ? "
                                         "and a = ? "
                                         "and variante != ? "
                                         "and akv != 0");
        if (dsqlstatus == 100)
        {
            return 1;
        }
*/
        return akv;
    }
    else
    {
        Prdk_k.sqlin ((short *)  &mdn, 1, 0);
        Prdk_k.sqlin ((double *) &a,   3, 0);
        Prdk_k.sqlcomm ("update prdk_k set akv = 0 "
                        "where mdn = ? "
                        "and a = ? "
                         "and akv = 1");
    }
    return 1;
}

int Work::DeletePrdk_k (short mdn, double a, char *rez)
{
    prdk_k.mdn = mdn;
    prdk_k.a   = a;
    strcpy (prdk_k.rez, rez);
    Prdk_k.dbdelete ();
    return 0;
}


int Work::GetPtab (char *dest, char *item, char *wert)
{
    int dsqlstatus;

    ptabn.ptlfnr = 1;
    dest[0] = 0;
    dsqlstatus = Ptab.lese_ptab (item, wert);  
    if (dsqlstatus == 100)
    {
        return -1;
    }
    strcpy (dest, ptabn.ptbezk);
    return 0;
}

int Work::GetPtab (char *dest, char *item, short swert)
{
    int dsqlstatus;
	char wert [5];

	sprintf (wert, "%hd", swert);
    ptabn.ptlfnr = 1;
    dest[0] = 0;
    dsqlstatus = Ptab.lese_ptab (item, wert);  
    if (dsqlstatus == 100)
    {
        return -1;
    }
    strcpy (dest, ptabn.ptbezk);
    return 0;
}


int Work::GetPtabLong (char *dest, char *item, char *wert)
{
    int dsqlstatus;

    ptabn.ptlfnr = 1;
    dest[0] = 0;
    dsqlstatus = Ptab.lese_ptab (item, wert);  
    if (dsqlstatus == 100)
    {
        return -1;
    }
    strcpy (dest, ptabn.ptbez);
    return 0;
}

int Work::GetPtabLong (char *dest, char *item, short swert)
{
    int dsqlstatus;
	char wert [5];

	sprintf (wert, "%hd", swert);
    ptabn.ptlfnr = 1;
    dest[0] = 0;
    dsqlstatus = Ptab.lese_ptab (item, wert);  
    if (dsqlstatus == 100)
    {
        return -1;
    }
    strcpy (dest, ptabn.ptbez);
    return 0;
}

char *Work::GetPtWert (char *item, short lfd)
{
    int dsqlstatus;

    Prdk_k.sqlout ((char *)  ptabn.ptwert, 0, 4);
    Prdk_k.sqlin  ((char *)  item, 0, 20);
    Prdk_k.sqlin  ((short *) &lfd, 1, 0);
    dsqlstatus = Prdk_k.sqlcomm ("select ptwert from ptabn "
                                  "where ptitem = ? and ptlfnr = ?");
    if (dsqlstatus == 0)
    {
        return ptabn.ptwert;
    }
    return "";
}

int Work::ShowPtab (char *dest, char *item)
{
    int dsqlstatus;

    dsqlstatus = Ptab.Show (item);  
    if (syskey == KEY5)
    {
        return -1;
    }
    strcpy (dest, ptabn.ptwert);
    return 0;
}

int Work::GetMdnName (char *dest, short mdn)
{
    int dsqlstatus;

    _mdn.konversion = 1.95583;
    if (mdn == 0)
    {
        strcpy (dest, "Zentrale");
        return 0;
    }
    dest[0] = 0;
    Prdk_k.sqlin ((short *) &mdn, 1, 0);
    Prdk_k.sqlout ((char *) dest, 0, 17);
    dsqlstatus =  Prdk_k.sqlcomm ("select adr.adr_krz from mdn,adr "
                                   "where mdn.mdn = ? " 
                                   "and adr.adr = mdn.adr");
    if (dsqlstatus == 100)
    {
        return 100;
    }
    Mdn.lese_mdn (mdn);
    if (_mdn.konversion == 0)
    {
        _mdn.konversion = 1.0;
    }
    return 0;
}

int Work::GetABez (char *dest, double a, char *pers_nam)
{
    int dsqlstatus;
    int ret = 0;
	int rechte;

    dest[0] = 0;


    if ((dsqlstatus = lese_a_bas (a)) == 100)
    {
        return dsqlstatus;
    }

   Prdk_k.sqlout ((short *) &rechte,   1, 0);
   Prdk_k.sqlin ((char *) pers_nam,   0, 9);
    Prdk_k.sqlin ((long *) &_a_bas.hwg,   2, 0);
    Prdk_k.sqlin ((long *) &_a_bas.wg,   2, 0);
	ret = Prdk_k.sqlcomm ("select rechte from pers_rechte where pers_nam = ? and prog = \"23201\" "
						  " and (hwg = ? or wg = ?)");

	if (ret == 0 && rechte == 0) 
	{
		ret = 999;
	    strcpy (dest, _a_bas.a_bz1);
		return ret;

	}
	else
	{
		ret = 0;
	}
 


    if (strcmp (_a_bas.stk_lst_kz, "R") != 0)
    {
        ret = 200;
    }


    memcpy (&a_eig, &a_eig_null, sizeof (a_eig));
    memcpy (&a_eig_div, &a_eig_div_null, sizeof (a_eig_div));
    switch (_a_bas.a_typ)
    {
        case EIG :
            Hndw.lese_a_eig (a);
            break;
        case EIG_DIV :
            if (Hndw.lese_a_eig_div (a) == 0)
            {
                strcpy (a_eig.rez, a_eig_div.rez);
            }
			break;
		default:
		    switch (_a_bas.a_typ2)
			{
				case EIG :
					Hndw.lese_a_eig (a);
					break;
				case EIG_DIV :
					if (Hndw.lese_a_eig_div (a) == 0)
					{
						strcpy (a_eig.rez, a_eig_div.rez);
					}
					break;
				default:
					ret = 100;

			}

    }


    strcpy (dest, _a_bas.a_bz1);
    return ret;
}

int Work::GetRez (char *dest, char *rez , double a,short mdn) //#150204
{
    int ret = 0;
    extern short sql_mode;

    dest[0] = 0;
    sql_mode = 1;
    Prdk_k.sqlin ((char *) rez,   0, 9);
    Prdk_k.sqlin ((double *) &a,   3, 0);
	ret = Prdk_k.sqlcomm ("update a_eig set rez = ? where a = ?");
    strcpy (a_eig.rez, rez);

    Prdk_k.sqlin ((short *) &prdk_k.mdn,   1, 0);
    Prdk_k.sqlin ((char *) rez,   0, 9);
    Prdk_k.sqlout ((char *)  dest, 0, 73);
    int dsqlstatus = Prdk_k.sqlcomm ("select rez_bz  from prdk_name where mdn = ? and rez = ?");

	if (strlen (clipped(dest)) < 1)
	{
		Prdk_k.sqlin ((short *) &prdk_k.mdn,   1, 0);
		Prdk_k.sqlin ((double *) &a,   3, 0);
		Prdk_k.sqlout ((char *)  dest, 0, 73);
		ret =  Prdk_k.sqlcomm ("select rez_bz from prdk_k where mdn = ? and a = ?");
	}
	sql_mode = 0;

	return ret;

}

int Work::upd_a_bas (double a) 
{
    int ret = 0;
    extern short sql_mode;

    sql_mode = 1;
    Prdk_k.sqlin ((double *) &a,   3, 0);
	ret = Prdk_k.sqlcomm ("update a_bas set stk_lst_kz = \"R\" where a = ?");
	sql_mode = 0;

	return ret;

}

double Work::GetRezAGew (void)
{
    double a_gew = 0.0;
    short me_einh = 0;

    Prdk_k.sqlout ((double *) &a_gew,   3, 0);
    Prdk_k.sqlout ((short *)  &me_einh, 1, 0);
    Prdk_k.sqlin  ((double *)  &prdk_k.a, 3, 0);
    Prdk_k.sqlcomm ("select a_gew, me_einh from a_bas where a = ?");
    if (me_einh == 2)
    {
        return 1.0;
    }
    if (a_gew == 0.0)
    {
        return 1.0;
    }
    return a_gew;
}

long Work::updPrdk_k (void)
{
//	    Prdk_k.sqlin ((char *) "N", 0, 2);
//	    return Prdk_k.sqlcomm ("update prdk_k set kalk_stat = ? ");
	return 0;
                                 
}        
long Work::dbupdate_prdk_k (void)
{
	    return Prdk_k.dbupdate();                                 
}        

long Work::GetAnzKalk (void)
{
    long anzkalk = 0;
/**
    Prdk_k.sqlin ((long *) &a_preis.dat, 2, 0);
    Prdk_k.sqlin ((char *)  a_preis.zeit, 0, 7);
    Prdk_k.sqlout ((long *)  &anzkalk, 2, 0);
    Prdk_k.sqlcomm ("select count(*)  from a_preis "
                                     "where dat = ? "
                                     "and zeit = ? ");
**/
    Prdk_k.sqlin ((char *) "K", 0, 2);
    Prdk_k.sqlout ((long *)  &anzkalk, 2, 0);
    Prdk_k.sqlcomm ("select count(*)  from prdk_k "
                                     "where kalk_stat = ? ");
    return anzkalk;
}

void Work::FillCombo (char **Combo, char *Item, int anz)
{
    int dsqlstatus;
    char *anr;
    
    int i = 0;
    dsqlstatus = Ptab.lese_ptab_all (Item);
    while (dsqlstatus == 0)
    {
        anr = new char [20];
        if (anr == NULL) return;
        strcpy (anr, ptabn.ptbezk);
        Combo[i] = anr;
        i ++;
        if (i ==anz) break;
        dsqlstatus = Ptab.lese_ptab_all ();
    }
    if (i > 0)
    {
        Combo[i] = NULL;
    }
}

void Work::FillComboLong (char **Combo, char *Item, int anz)
{
    int dsqlstatus;
    char *anr;
    
    int i = 0;
    dsqlstatus = Ptab.lese_ptab_all (Item);
    while (dsqlstatus == 0)
    {
        anr = new char [256];
        if (anr == NULL) return;
        strcpy (anr, ptabn.ptbez);
        Combo[i] = anr;
        i ++;
        if (i ==anz) break;
        dsqlstatus = Ptab.lese_ptab_all ();
    }
    Combo[i] = NULL;
}


/*
BOOL Work::TxtNrOK (void)
{
       int dsqlstatus;

       Prdk_k.sqlin ((long *) &auto_nr.nr_nr, 2, 0);
       dsqlstatus = Prdk_k.sqlcomm ("select nr from we_txt where nr = ?");
       if (dsqlstatus == 0)
       {
           return FALSE;
       }
       return TRUE;
}


void Work::FreeTxtNr (long nr)
{
       int dsqlstatus;

       commitwork ();
       beginwork ();
       dsqlstatus = AutoClass.nveinid (0, 0,
                             "adrp_txt",  nr);
       commitwork ();
       beginwork ();
}

long Work::GenTxtNr (long nr)
{
       extern short sql_mode; 
	   int i;
	   static int MAXWAIT = 100;
       MSG msg;
       int dsqlstatus;

       if (nr > 0l)
       {
             return nr;
       }

       commitwork ();
       beginwork ();
       sql_mode = 1;
	   i = 0;
       while (TRUE)
       {
                dsqlstatus = AutoClass.nvholid (0, 0, NR_NAM);

				if (dsqlstatus == -1)
				{
							Prdk_k.sqlcomm ("delete from auto_nr where nr_nam = \"NR_NAM\" "
								              "and mdn = 0 and fil = 0");
							dsqlstatus = 100;
				}
					   
                if (dsqlstatus == 100)
                {
                           dsqlstatus = AutoClass.nvanmprf (0,
                                                  0,
                                                  NR_NAM,
                                                  (long) 1,
                                                  (long) 99999999,
                                                  (long) 10,
                                                  "");

                           if (dsqlstatus == 0)
                           {
                                  dsqlstatus = AutoClass.nvholid (0,
                                                        0,
                                                        "adrp_txt");
                           }
                }

                if (dsqlstatus == 0 && TxtNrOK ()) break;
				Sleep (50);
                if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
                {
                          TranslateMessage(&msg);
                          DispatchMessage(&msg);
                }

				i ++;
				if (i > MAXWAIT) break;
       }
       sql_mode = 0;
       commitwork ();
       beginwork ();
       return auto_nr.nr_nr;
}


void Work::ReadTxt (long txtnr, MTXT *Mtxt)
{
	   int dsqlstatus;
       int cursor;
       char txt [61];
       short zei;
	   
	   Mtxt->InitText ();
       Prdk_k.sqlin ((long *)  &txtnr, 2, 0);
       Prdk_k.sqlin ((short *) &zei, 1, 0);
       Prdk_k.sqlout ((char *) txt, 0, 61);
       cursor = Prdk_k.sqlcursor ("select txt,zei  from we_txt "
                                   "where nr = ? order by zei");
       if (cursor == -1) return;

       dsqlstatus = Prdk_k.sqlfetch (cursor);
	   while (dsqlstatus == 0)
	   {
		    clipped (txt);
			Mtxt->AddText (txt);
			dsqlstatus = Prdk_k.sqlfetch (cursor);
	   }
       Prdk_k.sqlclose (cursor);
	   Mtxt->SetText ();
	   return;
}
*/

int Work::CutLines (char *txt, char *txtout, int zei, int cursor)
{
	   char *params [5];
	   char zeile[5];
	   char tlen[5];

	   if (strlen (txt) <= TLEN)
	   {
		   cr_weg (txtout);
	       strcpy (txtout, txt);
           Prdk_k.sqlexecute (cursor);
		   return (zei + 1);
	   }

	   params[0] = txt;
	   params[1] = txtout;
	   sprintf (zeile, "1");
	   sprintf (tlen, "%d", TLEN);
	   params[2] = tlen;
	   params[3] = zeile;
	   params[4] = NULL;
	   while (getline (params))
	   {
		   cr_weg (txtout);
           Prdk_k.sqlexecute (cursor);
		   zei ++;
		   sprintf (zeile, "%d", atoi (zeile) + 1);
	   }
	   return zei;
}

/*
void Work::WriteTxt (long txtnr, MTXT *Mtxt)
{
       int cursor;
	   short zei;
	   char *p;
	   char txt [512];
       char txtout [TLEN + 1];
	   
       Prdk_k.sqlin ((long *) &txtnr, 2, 0);

       Prdk_k.sqlcomm ("delete from we_txt where nr = ?");
       Prdk_k.sqlin ((long *) &txtnr, 2, 0);
       Prdk_k.sqlin ((short *) &zei, 1, 0);
       Prdk_k.sqlin ((char *) txt, 0, 61);
       cursor = Prdk_k.sqlcursor ("insert into we_txt (nr, zei, txt) "
                                   "values (?,?,?)");
	   zei = 1;
	   p = Mtxt->FirstRow (txt, 511);
       while (p)
	   {
		   clipped (txt);
		   zei = CutLines (txt, txtout, zei, cursor);
	       p = Mtxt->NextRow (txt, 511);
		   zei ++;
	   }
       Prdk_k.sqlclose (cursor);
       _adr.txt_nr = txtnr;
	   return;
}
*/


// Vewrarbeitungsmaterial

void Work::GetVarbAbz1 (char *a_bz1, char *me_einh, double *inh_prod, double *a_gew, long mat)
{
	char prod_einh[5];
    strcpy (a_bz1, "");
    if (mat == 0l)
    {
        return;
    }
    VarbLsts->a = 0.0;
    Prdk_k.sqlin ((long *) &mat, 2, 0);
    Prdk_k.sqlout ((double *) &VarbLsts->a, 3, 0);
    Prdk_k.sqlout ((char *) a_bz1, 0, 25);
    Prdk_k.sqlout ((char *) me_einh, 0, 5);
    Prdk_k.sqlout ((double *) a_gew, 3, 0);
    Prdk_k.sqlout ((char *) prod_einh, 0, 5);
    Prdk_k.sqlout ((double *) inh_prod, 3, 0);
    Prdk_k.sqlcomm ("select a_mat.a, a_bz1, me_einh, a_gew, me_einh,inh_lief from a_mat, a_bas "
                    "where a_mat.mat = ? "
                    "and a_bas.a = a_mat.a");
	if (strcmp(prod_einh,me_einh) != 0)
    {
		if (prod_einh != 0l) 
        {  
			strcpy(me_einh,prod_einh);
		}
		else
        {
			*inh_prod = 1.0;
		}
    }
	else
    {
		*inh_prod = 1,0;
    }


}
double Work::GetArtikel (char *me_einh, double *a_gew, long mat)
{
	double Artikel = 0.0;
    if (mat == 0l)
    {
        return 0.0;
    }
    Prdk_k.sqlin ((long *) &mat, 2, 0);
    Prdk_k.sqlout ((double *) &Artikel, 3, 0);
    Prdk_k.sqlout ((char *) me_einh, 0, 5);
    Prdk_k.sqlout ((double *) a_gew, 3, 0);
    Prdk_k.sqlcomm ("select a_mat.a, me_einh, a_gew from a_mat, a_bas "
                    "where a_mat.mat = ? "
                    "and a_bas.a = a_mat.a");

	return Artikel;
}

void Work::GetRezAbz1 (char *a_bz1, char *me_einh, double *a_gew, long mat)
{
    strcpy (a_bz1, "");
    if (mat == 0l)
    {
        return;
    }
	/*** wozu??? 131205
    Prdk_k.sqlin ((long *) &mat, 2, 0);
    Prdk_k.sqlout ((double *) &VarbLsts->a, 3, 0);
    Prdk_k.sqlout ((char *) a_bz1, 0, 25);
    Prdk_k.sqlout ((char *) me_einh, 0, 5);
    Prdk_k.sqlout ((double *) a_gew, 3, 0);
    if (Prdk_k.sqlcomm ("select a_varb.a, a_bz1, me_einh, a_gew from a_varb, a_bas "
                    "where a_varb.mat = ? "
                    "and a_bas.a = a_varb.a") == 0)
	{
		return;
	}
	********/
    Prdk_k.sqlin ((long *) &mat, 2, 0);
    Prdk_k.sqlout ((double *) &VarbLsts->a, 3, 0);
    Prdk_k.sqlout ((char *) a_bz1, 0, 25);
    Prdk_k.sqlout ((char *) me_einh, 0, 5);
    Prdk_k.sqlout ((double *) a_gew, 3, 0);
    Prdk_k.sqlcomm ("select a_bas.a, a_bz1, me_einh, a_gew from a_bas,a_mat "
                    "where a_mat.mat = ? and a_mat.a = a_bas.a");
}

double Work::GetRezChgGew (double a)
{
    double chg_gew = 0.0;

    Prdk_k.sqlout ((double *) &chg_gew, 3, 0);
    Prdk_k.sqlin  ((short *)  &prdk_k.mdn, 1, 0);
    Prdk_k.sqlin  ((double *) &a, 3, 0);
    Prdk_k.sqlcomm ("select chg_gew from prdk_k where mdn = ? and a = ?");
    return chg_gew;
}

long Work::GetAG (void)
{
    long mat, ag;
	short a_typ, a_typ2;


    mat = atol (VarbLsts->nr);
    if (mat == 0l)
    {
        return FALSE;
    }
    strcpy (VarbLsts->a_bz1, "");
    Prdk_k.sqlin ((long *) &mat, 2, 0);
    Prdk_k.sqlout ((char *) VarbLsts->a_bz1, 0, 25);
    Prdk_k.sqlout ((short *) &a_typ, 1, 0);
    Prdk_k.sqlout ((short *) &a_typ2, 1, 0);
    Prdk_k.sqlout ((long *) &ag, 2, 0);
    int dsqlstatus = Prdk_k.sqlcomm ("select a_bz1,a_typ,a_typ2, ag from a_mat, a_bas "
                                     "where a_mat.mat = ? "
                                     "and a_bas.a = a_mat.a ");
//									 "and (a_bas.a_typ = 5 or a_bas.a_typ2 = 5 or a_bas.a_typ = 2 or a_bas.a_typ2 = 2)");
    if (dsqlstatus != 0)
    {
        return 0;
    }
	if (a_typ != 5 && a_typ2 != 5 && a_typ != 2 && a_typ2 != 2)
	{
//		disp_mess("Material ist keine Verarbeitungsmaterial !",1);
		return 0;
	}
    return ag;
}

BOOL Work::GetVarbMatName (void)
{
    long mat;
	short a_typ, a_typ2;


    mat = atol (VarbLsts->nr);
    if (mat == 0l)
    {
        return FALSE;
    }
    strcpy (VarbLsts->a_bz1, "");
    Prdk_k.sqlin ((long *) &mat, 2, 0);
    Prdk_k.sqlout ((char *) VarbLsts->a_bz1, 0, 25);
    Prdk_k.sqlout ((short *) &a_typ, 1, 0);
    Prdk_k.sqlout ((short *) &a_typ2, 1, 0);
    int dsqlstatus = Prdk_k.sqlcomm ("select a_bz1,a_typ,a_typ2 from a_mat, a_bas "
                                     "where a_mat.mat = ? "
                                     "and a_bas.a = a_mat.a ");
//									 "and (a_bas.a_typ = 5 or a_bas.a_typ2 = 5 or a_bas.a_typ = 2 or a_bas.a_typ2 = 2)");
    if (dsqlstatus != 0)
    {
        return FALSE;
    }
	if (a_typ != 5 && a_typ2 != 5 && a_typ != 2 && a_typ2 != 2)
	{
		disp_mess("Material ist keine Verarbeitungsmaterial !",1);
	}
    return TRUE;
}

short Work::GetVarbRezName (void)
{
	short dakv = 0;
	short drework = 0;

    clipped (VarbLsts->nr);
    long mat = atol (VarbLsts->nr);
    if (mat == 0l)
    {
        return FALSE;
    }

    strcpy (VarbLsts->a_bz1, "");
    Prdk_k.sqlin ((short *) &prdk_k.mdn, 1, 0);
    Prdk_k.sqlin ((long *)  &mat, 2, 0);
    Prdk_k.sqlout ((char *) VarbLsts->a_bz1, 0, 25);
    Prdk_k.sqlout ((short *) &dakv, 1, 0);
    Prdk_k.sqlout ((short *) &drework, 1, 0);
    int dsqlstatus = Prdk_k.sqlcomm ("select rez_bz,akv,rework from prdk_k,a_mat "
                                     "where mdn = ? "
                                     "and a_mat.mat = ? and a_mat.a = prdk_k.a order by akv desc  ");
    if (dsqlstatus != 0)
    {
        return 0;
    }
	if (dakv == 0) return 2;
	if (drework == 1) return 10;
    return 1;
}
long Work::Mat_aus_a (double a)
{
	long mat = 0;
    Prdk_k.sqlin ((double *) &a, 3, 0);
    Prdk_k.sqlout ((long *) &mat, 2, 0);
    int dsqlstatus = Prdk_k.sqlcomm ("select mat from a_mat where a = ?");
    if (dsqlstatus != 0)
    {
        return 0;
    }
    return mat;
}

double Work::A_aus_Mat (long mat)
{
	double a = 0.0;
    Prdk_k.sqlin ((long *) &mat, 2, 0);
    Prdk_k.sqlout ((double *) &a, 3, 0);
    int dsqlstatus = Prdk_k.sqlcomm ("select a from a_mat where mat = ?");
    if (dsqlstatus != 0)
    {
        return 0;
    }
    return a;
}

double Work::GetMatEk (double a, short typ, double inh_prod, short PreisHerkunft,int PlanPreisHerkunft)  //#LuD #130503 mit typ , bep ..
{
	// 140703 : mit inh_prod (inh_lief aus a_bas)
    short mdn;
    short fil;
    double pr_ek;
    double plan_ek;
    double bep;
    double plan_bep;
    double plan_preis;
    long inh_ek;
	double inhalt;
	short me_kz;
    int cursor, preism_cursor ;
	int dsqlstatus;

    pr_ek = 0.0;
    kosteneinh = 0.0;
    kostenkg = 0.0;

    mdn = prdk_k.mdn;
    fil = 0;
	char cdatum [13]; 
	char cheute [13]; 
	long heute = 0;
	long datum = 0;
    sysdate (cdatum);
    sysdate (cheute);
	heute = dasc_to_long(cdatum);

	if (typ == REWORKTYP)
	{
		return (0.0);
	}
    if (Geta_preism() && !PlanEk  && typ != REZTYP)
	{
	    Prdk_k.sqlin ((short *) &mdn, 1, 0);
	    Prdk_k.sqlin ((double *) &a, 3, 0);
	    Prdk_k.sqlin ((char *) cheute, 0, 12);
	    Prdk_k.sqlout ((double *) &pr_ek, 3, 0);
	    Prdk_k.sqlout ((char *) cdatum, 0, 12);
		preism_cursor = Prdk_k.sqlcursor ("select preis,dat "
										"from a_preism "
										"where mdn = ? "
										"and a = ? "
										"and dat <= ?  order by dat desc ");

	
	    dsqlstatus = Prdk_k.sqlfetch (preism_cursor);
		if (dsqlstatus == 0)
		{
		    Prdk_k.sqlclose (preism_cursor);
			return Round (pr_ek,3);
		}
	    Prdk_k.sqlclose (preism_cursor);

	}




  if (PlanPreisHerkunft >= 1 && PlanEk)
  {

    Prdk_k.sqlin ((short *) &mdn, 1, 0);
    Prdk_k.sqlin ((short *) &fil, 1, 0);
    Prdk_k.sqlin ((double *) &a, 3, 0);
    Prdk_k.sqlin ((long *) &PlanPreisHerkunft, 2, 0);
    Prdk_k.sqlout ((double *) &plan_preis, 3, 0);
    Prdk_k.sqlout ((short *) &me_kz, 1, 0);
    Prdk_k.sqlout ((double *) &inhalt, 3, 0);
    dsqlstatus = Prdk_k.sqlcomm ("select pr_ek,me_kz,min_best "
                                     "from lief_bzg "
                                     "where mdn = ? "
                                     "and fil   = ? "
                                     "and a = ? "
									 "and lief_s = ? " );
	if (dsqlstatus == 0 && PlanEk)
	{
		if (inhalt == double(0)) inhalt = 1.0;
		if (me_kz == 0) plan_preis /= inhalt; 
		return plan_preis;
	}
  }

  if (PreisHerkunft >= 1)
  {
    Prdk_k.sqlin ((short *) &mdn, 1, 0);
    Prdk_k.sqlin ((short *) &fil, 1, 0);
    Prdk_k.sqlin ((double *) &a, 3, 0);
    Prdk_k.sqlout ((double *) &pr_ek, 3, 0);
    Prdk_k.sqlout ((double *) &plan_ek, 3, 0);
    Prdk_k.sqlout ((double *) &bep, 3, 0);
    Prdk_k.sqlout ((double *) &plan_bep, 3, 0);
    Prdk_k.sqlout ((double *) &kosteneinh, 3, 0);
    Prdk_k.sqlout ((double *) &kostenkg, 3, 0);
    Prdk_k.sqlout ((long *)   &inh_ek, 2, 0);
    dsqlstatus = Prdk_k.sqlcomm ("select ek,plan_ek,bep,plan_bep, kosteneinh, kostenkg, inh_ek "
                                     "from a_preis "
                                     "where mdn = ? "
                                     "and fil   = ? "
                                     "and a = ? ");

	pr_ek = Round (pr_ek,3); //#090903
	plan_ek = Round (plan_ek,3); //#090903
	plan_bep = Round (plan_bep,3); //#090903
	bep = Round (bep,3); //#090903
    (inh_ek == 0) ? inh_ek = 1 : inh_ek;
    pr_ek /= inh_ek;
    plan_ek /= inh_ek;

    (inh_prod == 0l) ? inh_prod = 1.0 : inh_prod;
    pr_ek /= inh_prod;
    plan_ek /= inh_prod;

    if (dsqlstatus == 0)
    {
		if (typ == REZTYP) //Grundbr�t : dann bep aus a_preis
		{
	        if (PlanEk)
			{
	            return plan_bep;
			}
			return bep;
		}	
		else
		{
	        if (PlanEk)
			{
	            return plan_ek;
			}
			return pr_ek;
		}
    }
  }
  if (PreisHerkunft < 2)
  {

    if (PlanEk)  // 100605    
	{
	    Prdk_k.sqlin ((short *) &mdn, 1, 0);
	    Prdk_k.sqlin ((short *) &fil, 1, 0);
	    Prdk_k.sqlin ((double *) &a, 3, 0);
	    Prdk_k.sqlout ((double *) &plan_ek, 3, 0);
	    Prdk_k.sqlout ((double *) &plan_bep, 3, 0);
	    dsqlstatus = Prdk_k.sqlcomm ("select plan_ek,plan_bep "
                                     "from a_preis "
                                     "where mdn = ? "
                                     "and fil   = ? "
                                     "and a = ? ");

		plan_ek = Round (plan_ek,3); 
		plan_bep = Round (plan_bep,3);
	    if (dsqlstatus == 0)
		{
			if (typ == REZTYP) //Grundbr�t : dann bep aus a_preis
			{
	            if (plan_bep > 0.0) return plan_bep;
			}	
			else
			{
	            if (plan_ek > 0.0) return plan_ek;
			}
		}
	}












    if (typ == REZTYP) //Grundbr�t : dann Preis aus a_kalk_eig   //todo
	{
/*
	    Prdk_k.sqlin ((double *) &mdn, 1, 0);
		Prdk_k.sqlin ((double *) &fil, 1, 0);
		Prdk_k.sqlin ((double *) &a, 3, 0);
		Prdk_k.sqlout ((double *) &pr_ek, 3, 0);
		Prdk_k.sqlout ((double *) &kostenkg, 3, 0);
        cursor = Prdk_k.sqlcursor ("select mat_o_b, (hk_vollk - mat_o_b) from a_kalk_eig "
                                     "where mdn = ? "
                                     "and fil   = ? "
                                     "and a = ? ");
*/
		//090207 in hk_teilk. stehen jetzt die Preise incl. H�llen, ohne Verpackungen
		/*** 
	    Prdk_k.sqlin ((double *) &mdn, 1, 0);
		Prdk_k.sqlin ((double *) &fil, 1, 0);
		Prdk_k.sqlin ((double *) &a, 3, 0);
		Prdk_k.sqlout ((double *) &pr_ek, 3, 0);
		Prdk_k.sqlout ((double *) &kostenkg, 3, 0);
        cursor = Prdk_k.sqlcursor ("select hk_teilk, (hk_teilk - hk_teilk) from a_kalk_eig "
                                     "where mdn = ? "
                                     "and fil   = ? "
                                     "and a = ? ");
***/
		//�nderung vom 09.2. erst mal r�ckg�ngig (M�fli) in a_kalk_eig.hk_teilk steht da der mat_o_b !!?? 040807
	    Prdk_k.sqlin ((double *) &mdn, 1, 0);
		Prdk_k.sqlin ((double *) &fil, 1, 0);
		Prdk_k.sqlin ((double *) &a, 3, 0);
		Prdk_k.sqlout ((double *) &pr_ek, 3, 0);
		Prdk_k.sqlout ((double *) &kostenkg, 3, 0);
        cursor = Prdk_k.sqlcursor ("select mat_o_b, (hk_vollk - mat_o_b) from a_kalk_eig "
                                     "where mdn = ? "
                                     "and fil   = ? "
                                     "and a = ? ");

	}
    else
	{
		short a_typ1 = hole_a_typ1(a);
	    if (a_typ1 == 1)
	    {
			/****
			Prdk_k.sqlin ((double *) &mdn, 1, 0);
			Prdk_k.sqlin ((double *) &fil, 1, 0);
			Prdk_k.sqlin ((double *) &a, 3, 0);
			Prdk_k.sqlout ((double *) &pr_ek, 3, 0);
			cursor = Prdk_k.sqlcursor ("select pr_ek1 from a_kalkhndw "
                                       "where mdn = ? "
                                     "and fil   = ? "
                                     "and a = ? ");
*********/

		    Prdk_k.sqlin ((double *) &mdn, 1, 0);
			Prdk_k.sqlin ((double *) &fil, 1, 0);
			Prdk_k.sqlin ((double *) &a, 3, 0);
			Prdk_k.sqlout ((double *) &pr_ek, 3, 0);
			Prdk_k.sqlout ((double *) &kostenkg, 3, 0);
			cursor = Prdk_k.sqlcursor ("select mat_o_b, (hk_vollk - mat_o_b) from a_kalkpreis "
                                     "where mdn = ? "
                                     "and fil   = ? "
                                     "and a = ? ");

		}
		else if (a_typ1 == 2)//Eigenprodukt, auch dann Preis aus a_kalk_eig
		{
		    Prdk_k.sqlin ((double *) &mdn, 1, 0);
			Prdk_k.sqlin ((double *) &fil, 1, 0);
			Prdk_k.sqlin ((double *) &a, 3, 0);
			Prdk_k.sqlout ((double *) &pr_ek, 3, 0);
			Prdk_k.sqlout ((double *) &kostenkg, 3, 0);
			cursor = Prdk_k.sqlcursor ("select mat_o_b, (hk_vollk - mat_o_b) from a_kalk_eig "
                                     "where mdn = ? "
                                     "and fil   = ? "
                                     "and a = ? ");
		}
        else
		{
		    Prdk_k.sqlin ((double *) &mdn, 1, 0);
			Prdk_k.sqlin ((double *) &fil, 1, 0);
			Prdk_k.sqlin ((double *) &a, 3, 0);
			Prdk_k.sqlout ((double *) &pr_ek, 3, 0);
			Prdk_k.sqlout ((double *) &kostenkg, 3, 0);
			cursor = Prdk_k.sqlcursor ("select mat_o_b, (hk_vollk - mat_o_b) from a_kalk_mat "
                                       "where mdn = ? "
                                     "and fil   = ? "
                                     "and a = ? ");
		}
	}
  
    dsqlstatus = Prdk_k.sqlfetch (cursor);
	if (GetMaterialOhneKosten() == 1)
	{
		if (typ == REZTYP) 
		{
			pr_ek += kostenkg ;  //10.12.08  Bei Grundbr�t soll insgesamt der hk_vollk genommen werden 
		}
		kostenkg = 0.0;
	}
	else
	{
		if (kostenkg < 0) kostenkg = 0;
	}

    while (dsqlstatus == 100)
    {
        if (fil > 0)
        {
            fil = 0;
        }
        else if (mdn > 0)
        {
            mdn = 0;
        }
        else
        {
            break;
        }
        Prdk_k.sqlopen (cursor);
        dsqlstatus = Prdk_k.sqlfetch (cursor);
		if (GetMaterialOhneKosten() == 1)
		{
			if (typ == REZTYP) 
			{
				pr_ek += kostenkg ;  //10.12.08  Bei Grundbr�t soll insgesamt der hk_vollk genommen werden 
			}
			kostenkg = 0.0;
		}
		else
		{
			if (kostenkg < 0) kostenkg = 0;
		}
    }
    Prdk_k.sqlclose (cursor);
	pr_ek = Round (pr_ek,3); //#090903
	if (pr_ek < 0.001 && typ != REZTYP)
	{
	    mdn = prdk_k.mdn;
		fil = 0;
		kostenkg = 0;
	    Prdk_k.sqlin ((double *) &mdn, 1, 0);
		Prdk_k.sqlin ((double *) &fil, 1, 0);
		Prdk_k.sqlin ((double *) &a, 3, 0);
		Prdk_k.sqlout ((double *) &pr_ek, 3, 0);
	    cursor = Prdk_k.sqlcursor ("select pr_ek1 from a_kalkhndw "
                                       "where mdn = ? "
                                     "and fil   = ? "
                                     "and a = ? ");
  
		dsqlstatus = Prdk_k.sqlfetch (cursor);
		while (dsqlstatus == 100)
		{
	        if (fil > 0)
			{
	            fil = 0;
			}
			else if (mdn > 0)
			{
	            mdn = 0;
			}
			else
			{
	            break;
			}
			Prdk_k.sqlopen (cursor);
			dsqlstatus = Prdk_k.sqlfetch (cursor);
		}
		Prdk_k.sqlclose (cursor);
		pr_ek = Round (pr_ek,3); 
    }
  }
  return pr_ek;
}


short Work::hole_a_typ1 (double a)
{
	short a_typ = 0;
	int dsqlstatus;

    Prdk_k.sqlout ((short *) &a_typ, 1, 0);
    Prdk_k.sqlin ((double *)   &a, 3, 0);
    dsqlstatus = Prdk_k.sqlcomm ("select a_typ from a_bas  where a = ? ");
	if (dsqlstatus != 0) a_typ = 0;
	return a_typ;
}


void Work::FillVarbRow (void)
{
    char me_einh [5];
    double pr_ek;
    double a_gew = 0.0;
    double inh_prod = 1.0;

    if (VarbLsts == NULL) return;
    InitVarbRow();
    VarbLsts->a = 0.0;
    if (strcmp(clipped(prdk_kf.rework),"J"))
	{
		sprintf (VarbLsts->typ,       "%s",      "Material");
	}
	else
	{
		sprintf (VarbLsts->typ,       "%s",      "Rezeptur");
	}

    sprintf (VarbLsts->basis,       "%s",      "Gesamt");
	VarbLsts->basis_kz == 1;
	
    VarbLsts->typ_kz = prdk_varb.typ;
//    if (prdk_varb.zug_bas == 0)
//   {
//        prdk_varb.zug_bas = 1;
//    }
	if (strcmp(prdk_varb.zug_bas,"GE") == 0) VarbLsts->basis_kz = 1;
	if (strcmp(prdk_varb.zug_bas,"FF") == 0) VarbLsts->basis_kz = 2;
	if (strcmp(prdk_varb.zug_bas,"EL") == 0) VarbLsts->basis_kz = 3;
	if (strcmp(prdk_varb.zug_bas,"KZ") == 0) VarbLsts->basis_kz = 4;
	if (strcmp(prdk_varb.zug_bas,"E1") == 0) VarbLsts->basis_kz = 5;
	if (strcmp(prdk_varb.zug_bas,"E2") == 0) VarbLsts->basis_kz = 6;
	if (strcmp(prdk_varb.zug_bas,"RW") == 0) VarbLsts->basis_kz = 7;

    sprintf (VarbLsts->nr,        "%d",      prdk_varb.mat);
    if (prdk_varb.typ == 0)
    {
        prdk_varb.typ = 1;
    }
	switch (prdk_varb.typ)
	{
	    case 1:
	    case ABTYP:
            GetVarbAbz1 (VarbLsts->a_bz1, me_einh, &inh_prod, &a_gew, prdk_varb.mat );
			break;
		case REZTYP:
            GetRezAbz1 (VarbLsts->a_bz1, me_einh, &a_gew, prdk_varb.mat );
			inh_prod = 1.0;
			break;
		case REWORKTYP:
            GetRezAbz1 (VarbLsts->a_bz1, me_einh, &a_gew, prdk_varb.mat );
			prdk_varb.varb_me = 0.0;
			prdk_varb.gew = 0.0;
			inh_prod = 1.0;
			break;
	}
    if (atoi (me_einh) == 0)
    {
        strcpy (me_einh, "2");
    }

    GetPtab (VarbLsts->me_einh, "me_einh", me_einh);
    sprintf (VarbLsts->kutf,      "%4d",     prdk_varb.kutf);
    sprintf (VarbLsts->anteil_proz,   "%8.2lf",  prdk_varb.anteil_proz );
    sprintf (VarbLsts->varb_me,   "%8.3lf",  prdk_varb.varb_me * chgfaktor);
    sprintf (VarbLsts->varb_gew_nto,   "%8.3lf",  prdk_varb.gew * chgfaktor);
    pr_ek = GetMatEk (VarbLsts->a,VarbLsts->typ_kz,inh_prod,GetPreisHerkunft(),GetPlanPreisHerkunft()); //LuD #130503 mit typ
    if (atoi (me_einh) != 2)
    {
        if (a_gew == 0.0) a_gew = 1.0;
        VarbLsts->gewek = pr_ek;
    }
    else
    {
        a_gew = 1.0;
        VarbLsts->gewek = pr_ek;
    }
    VarbLsts->prek = pr_ek; //#090903
    VarbLsts->a_gew = a_gew;
    sprintf (VarbLsts->pr_ek,     "%.3lf",  pr_ek);
    sprintf (VarbLsts->kosten,    "%.3lf", kostenkg);
	/* das war Bgut-Zeugs , muss raus 
    if (atoi (me_einh) != 2)
    {
        if (kosteneinh > 0.0)
        {
             sprintf (VarbLsts->kosten,    "%.3lf", kosteneinh);
        }
        else if (kostenkg > 0.0)
        {
             sprintf (VarbLsts->kosten,    "%.3lf", kostenkg * a_gew);
        }
		else
        {
             sprintf (VarbLsts->kosten,    "%.3lf", 0.0);
		}
    }
    else
    {
        if (kostenkg > 0.0)
        {
             sprintf (VarbLsts->kosten,    "%.3lf", kostenkg);
        }
        else if (kosteneinh > 0.0)
        {
             sprintf (VarbLsts->kosten,    "%.3lf", kosteneinh / a_gew);
        }
		else
        {
             sprintf (VarbLsts->kosten,    "%.3lf", 0.0);
		}
    }
	*/
    sprintf (VarbLsts->preisme, "%.3lf", Round((pr_ek * prdk_varb.varb_me),3)); //#090903
    strcpy  (VarbLsts->text, prdk_varb.bearb_info);
    VarbLsts->chtyp.SetFeld ("..");
    VarbLsts->chnr.SetFeld ("..");
}


void Work::DeleteAllVarbPos (void)
{
    prdk_varb.mdn = prdk_k.mdn;
    strcpy (prdk_varb.rez, prdk_k.rez);
    prdk_varb.variante = prdk_k.variante;
    
    Prdk_k.sqlin ((short *) &prdk_k.mdn, 1, 0);
    Prdk_k.sqlin ((char *)  prdk_k.rez, 0, 9);
    Prdk_k.sqlin ((short *) &prdk_k.variante, 1, 0);
    Prdk_k.sqlcomm ("delete from prdk_varb "
                    "where mdn = ? "
                    "and rez = ? "
                    "and variante = ?");
}



void Work::InitVarbRow (void)
{
    if (VarbLsts == NULL) return;
    if (strcmp(clipped(prdk_kf.rework),"J"))
	{
	    sprintf (VarbLsts->typ,       "%s",      "Material");
	    VarbLsts->typ_kz = 1;
	}
	else
	{
	    sprintf (VarbLsts->typ,       "%s",      "Rezeptur");
	    VarbLsts->typ_kz = 2;
	}
    sprintf (VarbLsts->nr,        "%d",      0l);
    sprintf (VarbLsts->kutf,      "%4d",     0);
    sprintf (VarbLsts->varb_me,   "%8.3lf",  0.0);
    sprintf (VarbLsts->varb_gew_nto,   "%8.3lf",  0.0);
    sprintf (VarbLsts->pr_ek,     "%.3lf",  0.0);
    sprintf (VarbLsts->kosten,    "%.3lf",  0.0);
    strcpy  (VarbLsts->text, "");
    VarbLsts->gewek = 0.0;
    VarbLsts->a_gew = 0.0;
}

void Work::FillA_Row (void)
{

    if (A_Lsts == NULL) return;
	double sk_kosten = prdk_k.a_sk_vollk - prdk_k.a_hk_vollk;

    A_Lsts->a = 0.0;
    sprintf (A_Lsts->nr,        "%.0lf",      prdk_k.a);
    sprintf (A_Lsts->rez,        "%s",      prdk_k.rez);
    sprintf (A_Lsts->a_bz1,        "%s",      _a_bas.a_bz1);
    sprintf (A_Lsts->pr_ek,     "%.3lf",  a_preis.bep + sk_kosten);
    sprintf (A_Lsts->plan_ek,     "%.3lf",  a_preis.plan_bep + sk_kosten);
    sprintf (A_Lsts->variante,     "%d",  prdk_k.variante);
    sprintf (A_Lsts->akv,         "%s",  "");
	if (prdk_k.akv == 1) sprintf (A_Lsts->akv,         "%s",  "Aktiv");
}

void Work::InitA_Row (void)
{
    if (A_Lsts == NULL) return;
    sprintf (A_Lsts->nr,        "%13.0lf",      0l);
    sprintf (A_Lsts->a_bz1,        "%s",      "");
    sprintf (A_Lsts->rez,        "%s",      "");
    sprintf (A_Lsts->pr_ek,     "%.3lf",  0.0);
    sprintf (A_Lsts->plan_ek,     "%.3lf",  0.0);
    sprintf (A_Lsts->variante,     "%d",  0);
}


void Work::WriteVarbRec (void)
{
   double a_gew;
   short me_einh;

   me_einh = 2;
   prdk_varb.mat = atol (VarbLsts->nr);
   Prdk_k.sqlin ((long *) &prdk_varb.mat, 2, 0);
   Prdk_k.sqlout ((short *) &me_einh, 1, 0);
   Prdk_k.sqlout ((double *) &a_gew, 3, 0);
   Prdk_k.sqlcomm ("select me_einh, a_gew from a_varb, a_bas "
                   "where a_varb.mat = ? "
                   "and a_bas.a = a_varb.a");
   if (a_gew == 0.0)
   {
       a_gew = 1.0;
   }
   prdk_varb.kutf = atoi (VarbLsts->kutf);
   prdk_varb.anteil_proz = atoi (VarbLsts->anteil_proz);
   prdk_varb.varb_me = ratod (VarbLsts->varb_me);
   prdk_varb.gew = ratod (VarbLsts->varb_gew_nto);
   prdk_varb.typ     = VarbLsts->typ_kz;
   sprintf (prdk_varb.zug_bas, "%s","GE");
   switch (VarbLsts->basis_kz)
	{
	case 1:
		sprintf (prdk_varb.zug_bas, "%s","GE");
		break;
	case 2:
		sprintf (prdk_varb.zug_bas, "%s","FF");
		break;
	case 3:
		sprintf (prdk_varb.zug_bas, "%s","EL");
		break;
	case 4:
		sprintf (prdk_varb.zug_bas, "%s","KZ");
		break;
	case 5:
		sprintf (prdk_varb.zug_bas, "%s","E1");
		break;
	case 6:
		sprintf (prdk_varb.zug_bas, "%s","E2");
		break;
	case 7:
		sprintf (prdk_varb.zug_bas, "%s","RW");
		break;
	}
   if (me_einh == 2)
   {
           prdk_varb.gew_bto = prdk_varb.varb_me;
   }
   else
   {
           prdk_varb.gew_bto = prdk_varb.varb_me * a_gew;
   }

/*   040608 Beim Kochen dann auch eine Gewichtszunahme erfolgen, so dass MengeNetto > MengeBrutto sein kann
   if (prdk_varb.gew_bto < prdk_varb.gew)
   {
		prdk_varb.gew = prdk_varb.gew_bto;
   }
*/
   if (prdk_varb.gew <= 0.0)
   {
		prdk_varb.gew = prdk_varb.gew_bto;
   }

   strcpy (prdk_varb.bearb_info, VarbLsts->text);
   Prdk_varb.dbupdate ();
   if (PlanEk)  //#141205
   {
	   a_preis.mdn = prdk_k.mdn;
	   a_preis.plan_ek = ratod (VarbLsts->pr_ek);
	   a_preis.plan_bep = ratod (VarbLsts->pr_ek);
	   a_preis.a = A_aus_Mat (atol(VarbLsts->nr));
	   A_preis.dbupdate ();
   }


}

double Work::GetChgGew (void)
{
    return prdk_k.chg_gew;
}

double Work::GetVarbGew (void)
{
    return prdk_k.varb_gew;
}

double Work::GetVarbGewNto (void)
{
    return prdk_k.varb_gew_nto;
}


double Work::GetZutGew (void)
{
    return prdk_k.zut_gew;
}

double Work::GetHuelGew (void)
{
    return prdk_k.huel_gew;
}

void Work::UpdateVpkMe (void) //210807
{
    CloseVpk ();
    OpenVpk ();
    while (FetchVpk () == 0)
    {
        if (prdk_vpk.fuel_gew != 0.0)
        {
             double vpk_me = prdk_vpk.me;
             prdk_vpk.me = (prdk_k.varb_gew + prdk_k.zut_gew) / prdk_vpk.fuel_gew;
			 prdk_vpk.me = prdk_vpk.me * (100 - prdk_k.sw_kalk) / 100; //210807

             if (vpk_me != 0.0) prdk_vpk.gew *= prdk_vpk.me / vpk_me;
             Prdk_vpk.dbupdate ();
        }
    }
    CloseVpk ();
}

void Work::UpdateChgGew (double chg_gew)
{
//    prdk_k.chg_gew = chg_gew;
    OpenHuel ();
    while (FetchHuel () == 0)
    {
        if (prdk_huel.fuel_gew != 0.0)
        {
             double huel_me = Round(prdk_huel.me,1);
             if (prdk_huel.me_buch[0] == 'J')
             {
                     prdk_k.chg_gew -= prdk_huel.gew;
             }
             prdk_huel.me = Round((chg_gew / prdk_huel.fuel_gew),1);
	         if (huel_me != 0.0) prdk_huel.gew *= prdk_huel.me / huel_me;
             if (prdk_huel.me_buch[0] == 'J')
             {
                     prdk_k.chg_gew += prdk_huel.gew;
             }
             Prdk_huel.dbupdate ();
        }
    }
    CloseHuel ();

	//#150308 1
    CloseVpk ();
    OpenVpk ();
    while (FetchVpk () == 0)
    {
        if (prdk_vpk.fuel_gew != 0.0)
        {
             double vpk_me = prdk_vpk.me;
             prdk_vpk.me = chg_gew / prdk_vpk.fuel_gew;
			 prdk_vpk.me = prdk_vpk.me * (100 - prdk_k.sw_kalk) / 100; //210807

             if (vpk_me != 0.0) prdk_vpk.gew *= prdk_vpk.me / vpk_me;
             Prdk_vpk.dbupdate ();
        }
    }
    CloseVpk ();
}


// Zutaten


void Work::GetZutAbz1 (char *a_bz1, char *me_einh, double *inh_prod, double *a_gew, long mat)
{
	char prod_einh[5];
    strcpy (a_bz1, "");
    if (mat == 0l)
    {
        return;
    }
    ZutLsts->a = 0.0;
    Prdk_k.sqlin ((long *) &mat, 2, 0);
    Prdk_k.sqlout ((double *) &ZutLsts->a, 3, 0);
    Prdk_k.sqlout ((char *) a_bz1, 0, 25);
    Prdk_k.sqlout ((char *) me_einh, 0, 5);
    Prdk_k.sqlout ((double *) a_gew, 3, 0);
    Prdk_k.sqlout ((char *) prod_einh, 0, 5);
    Prdk_k.sqlout ((double *) inh_prod, 3, 0);
    Prdk_k.sqlcomm ("select a_mat.a, a_bz1, me_einh, a_gew,me_einh,inh_lief from a_mat, a_bas "
                    "where a_mat.mat = ? "
//                    "and (a_bas.a_typ = 6 or "
//                         "a_bas.a_typ2 = 6) "
                    "and a_bas.a = a_mat.a");
	if (strcmp(prod_einh,me_einh) != 0)
    {
		if (prod_einh != 0l) 
        {  
			strcpy(me_einh,prod_einh);
		}
		else
        {
			*inh_prod = 1.0;
		}
    }
	else
    {
		*inh_prod = 1,0;
    }

}

void Work::GetRezZutAbz1 (char *a_bz1, char *me_einh, double *a_gew, long mat)
{
    strcpy (a_bz1, "");
    if (mat == 0l)
    {
        return;
    }
    Prdk_k.sqlin ((long *) &mat, 2, 0);
    Prdk_k.sqlout ((double *) &ZutLsts->a, 3, 0);
    Prdk_k.sqlout ((char *) a_bz1, 0, 25);
    Prdk_k.sqlout ((char *) me_einh, 0, 5);
    Prdk_k.sqlout ((double *) a_gew, 3, 0);
    if (Prdk_k.sqlcomm ("select a_mat.a, a_bz1, me_einh, a_gew from a_mat, a_bas "
                    "where a_mat.mat = ? "
                    "and a_bas.a = a_mat.a") == 0)
	{
		return;
	}
    Prdk_k.sqlin ((long *) &mat, 2, 0);
    Prdk_k.sqlout ((double *) &ZutLsts->a, 3, 0);
    Prdk_k.sqlout ((char *) a_bz1, 0, 25);
    Prdk_k.sqlout ((char *) me_einh, 0, 5);
    Prdk_k.sqlout ((double *) a_gew, 3, 0);
    Prdk_k.sqlcomm ("select a_bas.a, a_bz1, me_einh,a_gew from a_bas "
                    "where a_bas.a = ?");
}

BOOL Work::GetZutMatName (void)
{
    long mat;
	short a_typ, a_typ2;

    mat = atol (ZutLsts->nr);
    if (mat == 0l)
    {
        return FALSE;
    }
    strcpy (ZutLsts->a_bz1, "");
    Prdk_k.sqlin ((long *) &mat, 2, 0);
    Prdk_k.sqlout ((char *) ZutLsts->a_bz1, 0, 25);
    Prdk_k.sqlout ((short *) &a_typ, 1, 0);
    Prdk_k.sqlout ((short *) &a_typ2, 1, 0);
    int dsqlstatus = Prdk_k.sqlcomm ("select a_bz1,a_typ,a_typ2 from a_mat, a_bas "
                                     "where a_mat.mat = ? "
//                                     "and (a_bas.a_typ = 6 or "
//                                          "a_bas.a_typ2 = 6) "
                                     "and a_bas.a = a_mat.a");
    if (dsqlstatus != 0)
    {
        return FALSE;
    }
	if (a_typ != 6 && a_typ2 != 6)
	{
		disp_mess("Material ist keine Zutat !",1);
	}
    return TRUE;
}


void Work::FillZutRow (void)
{
    char me_einh [5];
    double pr_ek;
    double a_gew = 0.0;
    double inh_prod = 1.0;

    if (ZutLsts == NULL) return;
    InitZutRow();
    sprintf (ZutLsts->typ,       "%s",      "Material");
    ZutLsts->typ_kz = prdk_zut.typ;
    sprintf (ZutLsts->nr,        "%d",      prdk_zut.mat);

	ZutLsts->basis_kz == 1;
	
	if (strcmp(prdk_zut.zug_bas,"GE") == 0) ZutLsts->basis_kz = 1;
	if (strcmp(prdk_zut.zug_bas,"FF") == 0) ZutLsts->basis_kz = 2;
	if (strcmp(prdk_zut.zug_bas,"EL") == 0) ZutLsts->basis_kz = 3;
	if (strcmp(prdk_zut.zug_bas,"OE") == 0) ZutLsts->basis_kz = 4;
	if (strcmp(prdk_zut.zug_bas,"E1") == 0) ZutLsts->basis_kz = 5;
	if (strcmp(prdk_zut.zug_bas,"E2") == 0) ZutLsts->basis_kz = 6;


    if (prdk_zut.typ == 0)
    {
        prdk_zut.typ = 1;
    }
	switch (prdk_zut.typ)
	{
	    case 1:
            GetZutAbz1 (ZutLsts->a_bz1, me_einh, &inh_prod,&a_gew, prdk_zut.mat );
			break;
		case 2:
            GetRezZutAbz1 (ZutLsts->a_bz1, me_einh, &a_gew, prdk_zut.mat );
			break;
	}
    if (atoi (me_einh) == 0)
    {
        strcpy (me_einh, "2");
    }
    GetPtab (ZutLsts->me_einh, "me_einh", me_einh);
    sprintf (ZutLsts->kutf,      "%4d",     prdk_zut.kutf);
    sprintf (ZutLsts->zut_me,   "%8.3lf",  prdk_zut.zut_me * chgfaktor);
    sprintf (ZutLsts->zug_me,   "%8.3lf",  prdk_zut.zug_me);
//    sprintf (ZutLsts->zug_me,   "%.6lf",   prdk_zut.zut_me * chgfaktor * 1000 / prdk_k.varb_gew_nto);//271106
			 double gew = 0.0;
			switch (ZutLsts->basis_kz) 
			{
			case 1:  //GE Gesamt
				gew =  prdk_k.summegew_GE + prdk_k.summegew_EL + prdk_k.summegew_FF + prdk_k.summegew_EL1 + prdk_k.summegew_EL2;
				break;
			case 2:  // FF Fett und Fleisch
				gew = prdk_k.summegew_FF;
				break;
			case 3: // EL Einlage
				gew = prdk_k.summegew_EL;
				break;
			case 4: // OE ohne Einlage
				gew = prdk_k.summegew_GE + prdk_k.summegew_FF + prdk_k.summegew_EL1;
				break;
			case 5: // Behandelt
				gew = prdk_k.summegew_EL1;
				break;
			case 6: // EL2 Einlage
				gew = prdk_k.summegew_EL2;
				break;
			}
    if (gew == 0.0)
	{
	    sprintf (ZutLsts->zug_me,   "%.6lf",   0.0);//271106
//	    sprintf (ZutLsts->zut_me,   "%.6lf",   0.0);
	}
	else
	{
		if (prdk_zut.zug_me > 0.0000001)
		{
			sprintf (ZutLsts->zut_me,   "%.6lf",   prdk_zut.zug_me * gew / (chgfaktor * 1000) );
		}
//	    sprintf (ZutLsts->zug_me,   "%.6lf",   prdk_zut.zut_me * chgfaktor * 1000 / gew);
	}

    pr_ek = GetMatEk (ZutLsts->a,0,inh_prod,GetPreisHerkunft(),GetPlanPreisHerkunft());

    if (atoi (me_einh) != 2)
    {
        if (a_gew == 0.0) a_gew = 1.0;
        ZutLsts->gewek = pr_ek;
    }
    else
    {
        a_gew = 1.0;
        ZutLsts->gewek = pr_ek;
    }
    ZutLsts->a_gew = a_gew;
    ZutLsts->prek = pr_ek; //#090903

    sprintf (ZutLsts->pr_ek,     "%.3lf",  pr_ek);
    sprintf (ZutLsts->kosten,    "%.3lf", kostenkg);
	/* Bgut-Zeugs : muss raus
    if (atoi (me_einh) != 2)
    {
        if (kosteneinh > 0.0)
        {
             sprintf (ZutLsts->kosten,    "%.3lf", kosteneinh);
        }
        else if (kostenkg > 0.0)
        {
             sprintf (ZutLsts->kosten,    "%.3lf", kostenkg * a_gew);
        }
    }
    else
    {
        if (kostenkg > 0.0)
        {
             sprintf (ZutLsts->kosten,    "%.3lf", kostenkg);
        }
        else if (kosteneinh > 0.0)
        {
             sprintf (ZutLsts->kosten,    "%.3lf", kosteneinh / a_gew);
        }
    }
	*/
    sprintf (ZutLsts->preisme, "%.3lf", Round((pr_ek * prdk_zut.zut_me),3)); //#090903
    strcpy  (ZutLsts->text, prdk_zut.bearb_info);
    ZutLsts->chtyp.SetFeld ("..");
    ZutLsts->chnr.SetFeld ("..");
}


void Work::InitZutRow (void)
{
    if (ZutLsts == NULL) return;
    sprintf (ZutLsts->typ,       "%s",      "Material");
    ZutLsts->typ_kz = 1;
    ZutLsts->basis_kz = 1;
    sprintf (ZutLsts->nr,        "%d",      0l);
    sprintf (ZutLsts->kutf,      "%4d",     0);
    sprintf (ZutLsts->zut_me,    "%8.3lf",  0.0);
    sprintf (ZutLsts->zug_me,    "%.6lf",  0.0);
    sprintf (ZutLsts->pr_ek,     "%.3lf",  0.0);
    sprintf (ZutLsts->kosten,    "%.3lf",  0.0);
    strcpy  (ZutLsts->text, "");
    ZutLsts->gewek = 0.0;
    ZutLsts->a_gew = 0.0;
}

void Work::DeleteAllZutPos (void)
{
    prdk_zut.mdn = prdk_k.mdn;
    strcpy (prdk_zut.rez, prdk_k.rez);
    prdk_zut.variante = prdk_k.variante;
    
    Prdk_k.sqlin ((short *) &prdk_k.mdn, 1, 0);
    Prdk_k.sqlin ((char *)  prdk_k.rez, 0, 9);
    Prdk_k.sqlin ((short *) &prdk_k.variante, 1, 0);
    Prdk_k.sqlcomm ("delete from prdk_zut "
                    "where mdn = ? "
                    "and rez = ? "
                    "and variante = ?");
}

void Work::WriteZutRec (void)
{
   double a_gew;
   short me_einh;

   me_einh = 2;
   prdk_zut.mat = atol (ZutLsts->nr);
   Prdk_k.sqlin ((long *) &prdk_zut.mat, 2, 0);
   Prdk_k.sqlout ((short *) &me_einh, 1, 0);
   Prdk_k.sqlout ((double *) &a_gew, 3, 0);
   Prdk_k.sqlcomm ("select me_einh, a_gew from a_mat, a_bas "
                   "where a_mat.mat = ? "
                   "and a_bas.a = a_mat.a");
   if (a_gew == 0.0)
   {
       a_gew = 1.0;
   }
   prdk_zut.kutf   = atoi (ZutLsts->kutf);
   if ((prdk_k.summegew_GE + prdk_k.summegew_EL + prdk_k.summegew_FF + prdk_k.summegew_EL1 + prdk_k.summegew_EL2) == 0.0)
   {
	    prdk_zut.zut_me =  ratod (ZutLsts->zut_me);
		prdk_zut.zug_me = 0.0;
   }
   else
   {
	   prdk_zut.zut_me =  ratod (ZutLsts->zut_me);
		prdk_zut.zug_me = ratod (ZutLsts->zug_me);
		if (prdk_zut.zut_me > 0.000001 && prdk_zut.zug_me < 0.000001) 
		{
			if ((prdk_k.summegew_GE + prdk_k.summegew_EL + prdk_k.summegew_FF + prdk_k.summegew_EL1 + prdk_k.summegew_EL2) > 0.0)
			{
				prdk_zut.zug_me =  prdk_zut.zut_me * 1000 / (prdk_k.summegew_GE + prdk_k.summegew_EL + prdk_k.summegew_FF + prdk_k.summegew_EL1 + prdk_k.summegew_EL2);
			}
			switch (ZutLsts->basis_kz) 
			{
			case 1:  //GE Gesamt
				if ((prdk_k.summegew_GE + prdk_k.summegew_EL + prdk_k.summegew_FF + prdk_k.summegew_EL1 + prdk_k.summegew_EL2) > 0.0)
				{
					prdk_zut.zug_me =  prdk_zut.zut_me * 1000 / (prdk_k.summegew_GE + prdk_k.summegew_EL + prdk_k.summegew_FF + prdk_k.summegew_EL1 + prdk_k.summegew_EL2);
				}
				break;
			case 2:  // FF Fett und Fleisch
					if (prdk_k.summegew_FF > 0.0)
					{
						prdk_zut.zug_me = prdk_zut.zut_me * 1000 / prdk_k.summegew_FF;
					}
				break;
			case 3: // EL Einlage
					if (prdk_k.summegew_EL > 0.0)
					{
						prdk_zut.zug_me = prdk_zut.zut_me * 1000 / prdk_k.summegew_EL;
					}
				break;
			case 4: // OE ohne Einlage
					if ((prdk_k.summegew_GE + prdk_k.summegew_FF + prdk_k.summegew_EL1) > 0.0)
					{
						prdk_zut.zug_me = prdk_zut.zut_me * 1000 / (prdk_k.summegew_GE + prdk_k.summegew_FF + prdk_k.summegew_EL1);
					}
				break;
			case 5: // behandelt
					if (prdk_k.summegew_EL1 > 0.0)
					{
						prdk_zut.zug_me = prdk_zut.zut_me * 1000 / prdk_k.summegew_EL1;
					}
				break;
			case 6: // EI2 Einlage
					if (prdk_k.summegew_EL2 > 0.0)
					{
						prdk_zut.zug_me = prdk_zut.zut_me * 1000 / prdk_k.summegew_EL2;
					}
				break;
			}
		}
   }
   
//   prdk_zut.zug_me = prdk_zut.zut_me * 1000 / prdk_k.chg_gew;   
//   prdk_zut.zug_me = prdk_zut.zut_me * 1000 / prdk_k.varb_gew_nto; //271106  
   prdk_zut.typ     = ZutLsts->typ_kz;
   sprintf (prdk_zut.zug_bas, "%s","GE");
   switch (ZutLsts->basis_kz)
	{
	case 1:
		sprintf (prdk_zut.zug_bas, "%s","GE");
		break;
	case 2:
		sprintf (prdk_zut.zug_bas, "%s","FF");
		break;
	case 3:
		sprintf (prdk_zut.zug_bas, "%s","EL");
		break;
	case 4:
		sprintf (prdk_zut.zug_bas, "%s","OE");
		break;
	case 5:
		sprintf (prdk_zut.zug_bas, "%s","E1");
		break;
	case 6:
		sprintf (prdk_zut.zug_bas, "%s","E2");
		break;
	}


   if (me_einh == 2)
   {
           prdk_zut.gew = prdk_zut.zut_me;
   }
   else
   {
           prdk_zut.gew = prdk_zut.zut_me * a_gew;
   }
   strcpy  (prdk_zut.bearb_info, ZutLsts->text);
//   prdk_zut.gew_bto = prdk_zut.gew;
   Prdk_zut.dbupdate ();
   if (PlanEk)  //#141205
   {
	   a_preis.mdn = prdk_k.mdn;
	   a_preis.plan_ek = ratod (ZutLsts->pr_ek);
	   a_preis.plan_bep = ratod (ZutLsts->pr_ek);
	   a_preis.a = A_aus_Mat (atol(ZutLsts->nr));
	   A_preis.dbupdate ();
   }
}


void Work::BeginWork (void)
{
    ::beginwork ();
}

void Work::CommitWork (void)
{
    ::commitwork ();
}

void Work::RollbackWork (void)
{
    ::rollbackwork ();
}

void Work::InitRec (void)
{
    memcpy (&prdk_k, &prdk_k_null, sizeof (struct PRDK_K));
}



// Cursor Artikel von-bis (Batchlauf)

int Work::PrepareA (void)
{
    dakv = 0; dnakv = 1;
	dmdn = atoi(prdk_kf.mdn);
	da_von = atof(prdk_kf.a_von);
	da_bis = atof(prdk_kf.a_bis);
	dag_von = atoi(prdk_kf.ag_von);
	dag_bis = atoi(prdk_kf.ag_bis);
	if (strcmp(prdk_kf.akv, "J") == 0) dakv = 1;
	if (strcmp(prdk_kf.nakv, "J") == 0) dnakv = 0;

    Prdk_k.sqlin ((short *) &dmdn, 1, 0);
    Prdk_k.sqlin ((double *) &da_von, 3, 0);
    Prdk_k.sqlin ((double *) &da_bis, 3, 0);
    Prdk_k.sqlin ((int *) &dag_von, 2, 0);
    Prdk_k.sqlin ((int *) &dag_bis, 2, 0);
    Prdk_k.sqlin ((short *) &dakv, 1, 0);
    Prdk_k.sqlin ((short *) &dnakv, 1, 0);

    Prdk_k.sqlout ((short *) &prdk_k.mdn, 1, 0);
    Prdk_k.sqlout ((double *) &prdk_k.a, 3, 0);
    Prdk_k.sqlout ((short *) &prdk_k.variante, 1, 0);
    Prdk_k.sqlout ((char *) _a_bas.a_bz1, 0, 25);
    Prdk_k.sqlout ((double *) &a_preis.bep, 3, 0);
    Prdk_k.sqlout ((double *) &a_preis.plan_bep, 3, 0);

	 a_cursor =  Prdk_k.sqlcursor ("select prdk_k.mdn,prdk_k.a,prdk_k.variante,a_bas.a_bz1,a_preis.bep,a_preis.plan_bep "
									"from prdk_k,a_bas,a_preis "
                                     "where prdk_k.mdn = ? and prdk_k.a >= ? and prdk_k.a <= ? and a_bas.ag >= ?" 
									  "and a_bas.ag <= ? and a_bas.a = prdk_k.a and a_bas.a = a_preis.a and "
									  "prdk_k.mdn = a_preis.mdn and a_preis.fil = 0 "
									  "and (prdk_k.akv = ? or prdk_k.akv = ?)"
                                     "order by prdk_k.a");
    return a_cursor;
}

int Work::OpenA (void)
{
    if (a_cursor == -1)
    {
        PrepareA ();
        if (a_cursor == -1)
        {
            return 0;
        }
    }
    Prdk_k.sqlopen (a_cursor);
    return 0;
}

int Work::FetchA (void)
{
    if (a_cursor == -1)
    {
        PrepareA ();
        if (a_cursor == -1)
        {
            return 0;
        }
    }
    int dsqlstatus = Prdk_k.sqlfetch (a_cursor);
    if (dsqlstatus == 0)
    {
        dsqlstatus = Prdk_k.dbreadfirst ();
		if (dsqlstatus == 0)
        {
		prdk_k.manr = 0;
		if (PlanEk) prdk_k.manr = 1;
	    Prdk_k.sqlin ((char *) "b", 0, 2);
	    Prdk_k.sqlin ((long *) &prdk_k.manr, 2, 0);
	    Prdk_k.sqlin ((short *) &prdk_k.mdn, 1, 0);
	    Prdk_k.sqlin ((double *) &prdk_k.a, 3, 0);
	    Prdk_k.sqlin ((short *) &prdk_k.variante, 1, 0);
	    dsqlstatus = Prdk_k.sqlcomm ("update prdk_k set kalk_stat = ?, manr = ? "
                                 "where mdn = ? "
                                 "and a = ? "
                                 "and variante = ?");
		}

    }
    return dsqlstatus;
}

int Work::CloseA (void)
{
    if (a_cursor != -1)
    {
        Prdk_k.sqlclose (varb_cursor);
        a_cursor = -1;
    }
    return 0;
}



// Cursor Verabeitungmaterial

int Work::PrepareVarb (void)
{
    Prdk_k.sqlin ((short *) &prdk_k.mdn, 1, 0);
    Prdk_k.sqlin ((char *)  prdk_k.rez, 0, 9);
    Prdk_k.sqlin ((short *) &prdk_k.variante, 1, 0);

    Prdk_k.sqlout ((short *) &prdk_varb.mdn, 1, 0);
    Prdk_k.sqlout ((char *)  prdk_varb.rez, 0, 9);
    Prdk_k.sqlout ((long *) &prdk_varb.mat, 2, 0);
    Prdk_k.sqlout ((short *) &prdk_varb.variante, 1, 0);
    Prdk_k.sqlout ((long *) &prdk_varb.kutf, 1, 0);
    varb_cursor =  Prdk_k.sqlcursor ("select mdn, rez, mat, variante, kutf  from prdk_varb "
                                     "where mdn = ? "
                                     "and rez = ? "
                                     "and variante = ? "
                                     "order by kutf");
    return varb_cursor;
}

int Work::OpenVarb (void)
{
    if (varb_cursor == -1)
    {
        PrepareVarb ();
        if (varb_cursor == -1)
        {
            return 0;
        }
    }
    Prdk_k.sqlopen (varb_cursor);
    return 0;
}

int Work::FetchVarb (void)
{
    if (varb_cursor == -1)
    {
        PrepareVarb ();
        if (varb_cursor == -1)
        {
            return 0;
        }
    }
    int dsqlstatus = Prdk_k.sqlfetch (varb_cursor);
    if (dsqlstatus == 0)
    {
        dsqlstatus = Prdk_varb.dbreadfirst ();
    }
    return dsqlstatus;
}

int Work::CloseVarb (void)
{
    if (varb_cursor != -1)
    {
        Prdk_k.sqlclose (varb_cursor);
        varb_cursor = -1;
    }
    return 0;
}


// Cursor Zutaten

int Work::PrepareZut (void)
{
    Prdk_k.sqlin ((short *) &prdk_k.mdn, 1, 0);
    Prdk_k.sqlin ((char *)  prdk_k.rez, 0, 9);
    Prdk_k.sqlin ((short *) &prdk_k.variante, 1, 0);

    Prdk_k.sqlout ((short *) &prdk_zut.mdn, 1, 0);
    Prdk_k.sqlout ((char *)  prdk_zut.rez, 0, 9);
    Prdk_k.sqlout ((long *)  &prdk_zut.mat, 2, 0);
    Prdk_k.sqlout ((short *) &prdk_zut.variante, 1, 0);
    Prdk_k.sqlout ((long *)  &prdk_zut.kutf, 1, 0);
    zut_cursor =  Prdk_k.sqlcursor ("select mdn, rez, mat, variante, kutf  from prdk_zut "
                                     "where mdn = ? "
                                     "and rez = ? "
                                     "and variante = ? "
                                     "order by kutf");
    return zut_cursor;
}

int Work::OpenZut (void)
{
    if (zut_cursor == -1)
    {
        PrepareZut ();
        if (zut_cursor == -1)
        {
            return 0;
        }
    }
    Prdk_k.sqlopen (zut_cursor);
    return 0;
}

int Work::FetchZut (void)
{
    if (zut_cursor == -1)
    {
        PrepareZut ();
        if (zut_cursor == -1)
        {
            return 0;
        }
    }
    int dsqlstatus = Prdk_k.sqlfetch (zut_cursor);
    if (dsqlstatus == 0)
    {
        dsqlstatus = Prdk_zut.dbreadfirst ();
    }
    return dsqlstatus;
}

int Work::CloseZut (void)
{
    if (zut_cursor != -1)
    {
        Prdk_k.sqlclose (zut_cursor);
        zut_cursor = -1;
    }
    return 0;
}


// Huellen


void Work::GetHuelAbz1 (char *a_bz1, char *me_einh, double *inh_prod, double *a_gew, long mat)
{
	char prod_einh[5];
    strcpy (a_bz1, "");
    if (mat == 0l)
    {
        return;
    }
	strcpy(prod_einh,"");
    HuelLsts->a = 0.0;
    Prdk_k.sqlin ((long *) &mat, 2, 0);
    Prdk_k.sqlout ((double *) &HuelLsts->a, 3, 0);
    Prdk_k.sqlout ((char *) a_bz1, 0, 25);
    Prdk_k.sqlout ((char *) me_einh, 0, 5);
    Prdk_k.sqlout ((double *) a_gew, 3, 0);
    Prdk_k.sqlout ((char *) prod_einh, 0, 5);
    Prdk_k.sqlout ((double * ) inh_prod, 3, 0);
    Prdk_k.sqlcomm ("select a_mat.a, a_bz1, me_einh, a_gew, me_einh,inh_lief from a_mat, a_bas "
                    "where a_mat.mat = ? "
//                    "and (a_bas.a_typ = 9 or "
//                         "a_bas.a_typ2 = 9) "
                    "and a_bas.a = a_mat.a");
	if (strcmp(prod_einh,me_einh) != 0)
    {
		if (prod_einh != 0l) 
        {  
			strcpy(me_einh,prod_einh);
		}
		else
        {
			*inh_prod = 1.0;
		}
    }
	else
    {
		*inh_prod = 1,0;
    }
}

BOOL Work::GetHuelMatName (void)
{
    long mat;
    double a;
    double fuel_gew;
	short a_typ , a_typ2;

    mat = atol (HuelLsts->nr);
    if (mat == 0l)
    {
        return FALSE;
    }
    strcpy (HuelLsts->a_bz1, "");
    Prdk_k.sqlin ((long *) &mat, 2, 0);
    Prdk_k.sqlout ((double *) &a, 3, 0);
    Prdk_k.sqlout ((char *) HuelLsts->a_bz1, 0, 25);
    Prdk_k.sqlout ((short *) &a_typ, 1, 0);
    Prdk_k.sqlout ((short *) &a_typ2, 1, 0);
    int dsqlstatus = Prdk_k.sqlcomm ("select a_mat.a, a_bz1,a_typ,a_typ2 from a_mat, a_bas "
                                     "where a_mat.mat = ? "
//                                     "and (a_bas.a_typ = 9 or "
//                                          "a_bas.a_typ2 = 9) "
                                     "and a_bas.a = a_mat.a");
    if (dsqlstatus != 0)
    {
        return FALSE;
    }
    fuel_gew = 0.0;
	if (a_typ != 9 && a_typ2 != 9)
	{
		disp_mess("Material geh�rt nicht zum Artikeltyp 9 \"H�llen,D�rme,Dosen\" !",1);
	}
	else
	{
		Prdk_k.sqlin ((double *) &a, 3, 0);
		Prdk_k.sqlout ((double *) &fuel_gew, 3, 0);
		Prdk_k.sqlcomm ("select fuel_gew from a_huel "
                    "where a = ?");
	}
    sprintf (HuelLsts->fuell_me, "%.4lf", fuel_gew / 1000);
    CalcMe ();
    return TRUE;
}


void Work::FillHuelRow (void)
{
    char me_einh [5];
    double pr_ek;
    double inh_prod;
    double a_gew;

    if (HuelLsts == NULL) return;
    InitHuelRow();
    sprintf (HuelLsts->typ,       "%s",      "Material");
    HuelLsts->typ_kz = prdk_huel.typ;
    sprintf (HuelLsts->nr,        "%d",      prdk_huel.mat);
    if (prdk_huel.typ == 0)
    {
        prdk_huel.typ = 1;
    }
	switch (prdk_huel.typ)
	{
	    case 1:
            GetHuelAbz1 (HuelLsts->a_bz1, me_einh, &inh_prod, &a_gew, prdk_huel.mat );
			break;
	}
    if (atoi (me_einh) == 0)
    {
        strcpy (me_einh, "2");
    }
    GetPtab (HuelLsts->me_einh, "me_einh", me_einh);
    sprintf (HuelLsts->kutf,      "%4d",     prdk_huel.bearb_flg);
    sprintf (HuelLsts->fuell_me,  "%8.4lf",  prdk_huel.fuel_gew);
    sprintf (HuelLsts->huel_me,   "%8.3lf",  prdk_huel.me);
    sprintf (HuelLsts->me_buch,   "%s",      prdk_huel.me_buch);
    pr_ek = GetMatEk (HuelLsts->a,0,inh_prod,GetPreisHerkunft(),GetPlanPreisHerkunft());
    if (atoi (me_einh) != 2)
    {
        if (a_gew == 0.0) a_gew = 1.0;
        HuelLsts->gewek = pr_ek;
    }
    else
    {
        a_gew = 1.0;
        HuelLsts->gewek = pr_ek;
    }
    HuelLsts->prek = pr_ek; //#090903

    HuelLsts->a_gew = a_gew;
    sprintf (HuelLsts->pr_ek,     "%.3lf",  pr_ek);
    sprintf (HuelLsts->kosten,    "%.3lf", kostenkg);
	/* Bgut-Zeugs : muss raus
    if (atoi (me_einh) != 2)
    {
        if (kosteneinh > 0.0)
        {
             sprintf (HuelLsts->kosten,    "%.3lf", kosteneinh);
        }
        else if (kostenkg > 0.0)
        {
             sprintf (HuelLsts->kosten,    "%.3lf", kostenkg * a_gew);
        }
    }
    else
    {
        if (kostenkg > 0.0)
        {
             sprintf (HuelLsts->kosten,    "%.3lf", kostenkg);
        }
        else if (kosteneinh > 0.0)
        {
             sprintf (HuelLsts->kosten,    "%.3lf", kosteneinh / a_gew);
        }
    }
	*/
    sprintf (HuelLsts->preisme, "%.3lf", Round((pr_ek * prdk_huel.me),3));
    HuelLsts->chtyp.SetFeld ("..");
    HuelLsts->chnr.SetFeld ("..");
    HuelLsts->mebuch.SetTyp (1); 
    HuelLsts->mebuch.SetFeld (prdk_huel.me_buch); 
    HuelLsts->mebuch.SetBitMap (BMAP::LoadBitmap (DLG::hInstance, "SEL", "MSK", WHITECOL));
	CalcMe();
}


void Work::InitHuelRow (void)
{
    if (HuelLsts == NULL) return;
    sprintf (HuelLsts->typ,       "%s",      "Material");
    HuelLsts->typ_kz = 1;
    sprintf (HuelLsts->nr,        "%d",      0l);
    sprintf (HuelLsts->kutf,      "%4d",     0);
    sprintf (HuelLsts->fuell_me,  "%8.4lf",  0.0);
    sprintf (HuelLsts->huel_me,   "%8.3lf",  0.0);
    sprintf (HuelLsts->pr_ek,     "%.3lf",  0.0);
    sprintf (HuelLsts->kosten,    "%.3lf",  0.0);
    sprintf (HuelLsts->me_buch,   "%s",      "N");
    HuelLsts->mebuch.SetTyp (1); 
    HuelLsts->mebuch.SetBitMap (BMAP::LoadBitmap (DLG::hInstance, "SEL", "MSK", WHITECOL));
    HuelLsts->gewek = 0.0;
    HuelLsts->a_gew = 0.0;
}


void Work::DeleteAllHuelPos (void)
{
    prdk_huel.mdn =  prdk_k.mdn;
    prdk_huel.a  = prdk_k.a;
    prdk_huel.variante = prdk_k.variante;
    
    Prdk_k.sqlin ((short *) &prdk_k.mdn, 1, 0);
    Prdk_k.sqlin ((double *)&prdk_k.a, 3, 0);
    Prdk_k.sqlin ((short *) &prdk_k.variante, 1, 0);
    Prdk_k.sqlcomm ("delete from prdk_HUEL "
                    "where mdn = ? "
                    "and a = ? "
                    "and variante = ?");
}

void Work::WriteHuelRec (void)
{
   double a_gew;
   short me_einh;

   me_einh = 2;
   prdk_huel.mat = atol (HuelLsts->nr);
   Prdk_k.sqlin ((long *) &prdk_huel.mat, 2, 0);
   Prdk_k.sqlout ((short *) &me_einh, 1, 0);
   Prdk_k.sqlout ((double *) &a_gew, 3, 0);
   Prdk_k.sqlcomm ("select me_einh, a_gew from a_mat, a_bas "
                   "where a_mat.mat = ? "
                   "and a_bas.a = a_mat.a");
   if (a_gew == 0.0)
   {
       a_gew = 1.0;
   }
   prdk_huel.bearb_flg = atoi (HuelLsts->kutf);
   prdk_huel.fuel_gew  = ratod (HuelLsts->fuell_me);
   prdk_huel.me        = ratod (HuelLsts->huel_me);
   prdk_huel.me = Round(prdk_huel.me,1);
   prdk_huel.typ        = HuelLsts->typ_kz;
   if (me_einh == 2)
   {
           prdk_huel.gew = Round(prdk_huel.me,3);
   }
   else
   {
           prdk_huel.gew = Round((prdk_huel.me * a_gew),3);
   }
   strcpy (prdk_huel.me_buch, HuelLsts->mebuch.GetFeld ());
//   strcpy (prdk_huel.me_buch, HuelLsts->me_buch);
   Prdk_huel.dbupdate ();
   if (PlanEk)  //#141205
   {
	   a_preis.mdn = prdk_k.mdn;
	   a_preis.plan_ek = ratod (HuelLsts->pr_ek);
	   a_preis.plan_bep = ratod (HuelLsts->pr_ek);
	   a_preis.a = A_aus_Mat (atol(HuelLsts->nr));
	   A_preis.dbupdate ();
   }
}

int Work::PrepareHuel (void)
{
    Prdk_k.sqlin ((short *) &prdk_k.mdn, 1, 0);
    Prdk_k.sqlin ((double *)&prdk_k.a, 3, 0);
    Prdk_k.sqlin ((short *) &prdk_k.variante, 1, 0);

    Prdk_k.sqlout ((short *) &prdk_huel.mdn, 1, 0);
    Prdk_k.sqlout ((double *) &prdk_huel.a, 3, 0);
    Prdk_k.sqlout ((long *)  &prdk_huel.mat, 2, 0);
    Prdk_k.sqlout ((short *) &prdk_huel.variante, 1, 0);
    Prdk_k.sqlout ((short *) &prdk_huel.bearb_flg, 1, 0);
    huel_cursor =  Prdk_k.sqlcursor ("select mdn, a, mat, variante, bearb_flg "
                                     "from prdk_huel "
                                     "where mdn = ? "
                                     "and a = ? "
                                     "and variante = ? "
                                     "order by bearb_flg");
    return huel_cursor;
}

int Work::OpenHuel (void)
{
    if (huel_cursor == -1)
    {
        PrepareHuel ();
        if (huel_cursor == -1)
        {
            return 0;
        }
    }
    Prdk_k.sqlopen (huel_cursor);
    return 0;
}

int Work::FetchHuel (void)
{
    if (huel_cursor == -1)
    {
        PrepareHuel ();
        if (huel_cursor == -1)
        {
            return 0;
        }
    }
    int dsqlstatus = Prdk_k.sqlfetch (huel_cursor);
    if (dsqlstatus == 0)
    {
        dsqlstatus = Prdk_huel.dbreadfirst ();
    }
    return dsqlstatus;
}

int Work::CloseHuel (void)
{
    if (huel_cursor != -1)
    {
        Prdk_k.sqlclose (huel_cursor);
        huel_cursor = -1;
    }
    return 0;
}




// Verpackung


void Work::GetVpkAbz1 (char *a_bz1, char *me_einh,double *inh_prod, double *a_gew, double a_vpk)
{
	char prod_einh[5];
    strcpy (a_bz1, "");
    if (a_vpk == 0.0)
    {
        return;
    }
    VpkLsts->a = 0.0;
    Prdk_k.sqlin ((long *) &a_vpk, 3, 0);
    Prdk_k.sqlout ((double *) &VpkLsts->a, 3, 0);
    Prdk_k.sqlout ((char *) a_bz1, 0, 25);
    Prdk_k.sqlout ((char *) me_einh, 0, 5);
    Prdk_k.sqlout ((double *) a_gew, 3, 0);
    Prdk_k.sqlout ((char *) prod_einh, 0, 5);
    Prdk_k.sqlout ((double *) inh_prod, 3, 0);
    Prdk_k.sqlcomm ("select a_bas.a, a_bz1, me_einh, a_gew ,me_einh, inh_lief from a_bas "
                    "where a_bas.a = ? ");
//                    "and (a_bas.a_typ in (8,9,10) or "
//                         "a_bas.a_typ2 in (8,9,10)) ");
	if (strcmp(prod_einh,me_einh) != 0)
    {
		if (prod_einh != 0l) 
        {  
			strcpy(me_einh,prod_einh);
		}
		else
        {
			*inh_prod = 1.0;
		}
    }
	else
    {
		*inh_prod = 1,0;
    }

}

BOOL Work::GetVpkMatName (void)
{
    double a_vpk;
    double a;
    double fuel_gew;
	short a_typ, a_typ2;

    a_vpk = ratod (VpkLsts->nr);
    if (a_vpk == 0l)
    {
        return FALSE;
    }
    strcpy (VpkLsts->a_bz1, "");
    Prdk_k.sqlin ((double *) &a_vpk, 3, 0);
    Prdk_k.sqlout ((double *) &a, 3, 0);
    Prdk_k.sqlout ((char *) VpkLsts->a_bz1, 0, 25);
    Prdk_k.sqlout ((short *) &a_typ, 1, 0);
    Prdk_k.sqlout ((short *) &a_typ2, 1, 0);
    int dsqlstatus = Prdk_k.sqlcomm ("select a_bas.a, a_bz1, a_typ,a_typ2 from a_bas "
                                     "where a_bas.a = ? ");
//                                     "and (a_bas.a_typ in (8,9,10) or "
//                                     "a_bas.a_typ2 in (8,9,10)) ");
    if (dsqlstatus != 0)
    {
        return FALSE;
    }

	if (a_typ != 8 && a_typ2 != 8 && a_typ != 9 && a_typ2 != 9 && a_typ != 10 && a_typ2 != 10 )
	{
		disp_mess("Material ist kein Verpackungsmaterial",1);
	}
    fuel_gew = 0.0;
/*
    Prdk_k.sqlin ((double *) &a, 3, 0);
    Prdk_k.sqlout ((double *) &fuel_gew, 3, 0);
    Prdk_k.sqlcomm ("select fuel_gew from a_huel "
                    "where a = ?");
    sprintf (HuelLsts->fuell_me, "%.4lf", fuel_gew / 1000);
*/
    CalcVpkMe ();
    return TRUE;
}


void Work::FillVpkRow (void)
{
    char me_einh [5];
    double pr_ek;
    double a_gew;
	double inh_prod;

    if (VpkLsts == NULL) return;
    InitVpkRow();
    sprintf (VpkLsts->typ,       "%s",      "Material");
    VpkLsts->typ_kz = prdk_vpk.typ;
    sprintf (VpkLsts->nr,        "%.0lf",      prdk_vpk.a_vpk);
    if (prdk_vpk.typ == 0)
    {
        prdk_vpk.typ = 1;
    }
	switch (prdk_vpk.typ)
	{
	    case 1:
            GetVpkAbz1 (VpkLsts->a_bz1, me_einh,&inh_prod, &a_gew, prdk_vpk.a_vpk );
			break;
	}
    if (atoi (me_einh) == 0)
    {
        strcpy (me_einh, "2");
    }
    GetPtab (VpkLsts->me_einh, "me_einh", me_einh);
    sprintf (VpkLsts->kutf,      "%4d",     prdk_vpk.bearb_flg);
    sprintf (VpkLsts->fuell_me,  "%8.4lf",  prdk_vpk.fuel_gew);
     prdk_vpk.me = (prdk_k.varb_gew + prdk_k.zut_gew) / prdk_vpk.fuel_gew; //210807
	 prdk_vpk.me = prdk_vpk.me * (100 - prdk_k.sw_kalk) / 100; //210807
    sprintf (VpkLsts->vpk_me,   "%8.3lf",  prdk_vpk.me);
    pr_ek = GetMatEk (VpkLsts->a,0,inh_prod,GetPreisHerkunft(),GetPlanPreisHerkunft());
    if (atoi (me_einh) != 2)
    {
        if (a_gew == 0.0) a_gew = 1.0;
        VpkLsts->gewek = pr_ek;
    }
    else
    {
        a_gew = 1.0;
        VpkLsts->gewek = pr_ek;
    }
    VpkLsts->prek = pr_ek; //#090903

    VpkLsts->a_gew = a_gew;
    sprintf (VpkLsts->pr_ek,   "%.3lf",  pr_ek);
    sprintf (VpkLsts->kosten,    "%.3lf", kostenkg);
	/* Bgut-Zeugs : muss raus
    if (atoi (me_einh) != 2)
    {
        if (kosteneinh > 0.0)
        {
             sprintf (VpkLsts->kosten,    "%.3lf", kosteneinh);
        }
        else if (kostenkg > 0.0)
        {
             sprintf (VpkLsts->kosten,    "%.3lf", kostenkg * a_gew);
        }
    }
    else
    {
        if (kostenkg > 0.0)
        {
             sprintf (VpkLsts->kosten,    "%.3lf", kostenkg);
        }
        else if (kosteneinh > 0.0)
        {
             sprintf (VpkLsts->kosten,    "%.3lf", kosteneinh / a_gew);
        }
    }
	*/
    sprintf (VpkLsts->preisme, "%.3lf", Round((pr_ek * prdk_vpk.me),3));
    VpkLsts->chtyp.SetFeld ("..");
    VpkLsts->chnr.SetFeld ("..");
	CalcVpkMe();
/*
    VpkLsts->mebuch[0] = new ListButton (VpkLsts->me_buch, NULL, NULL, 1);
    VpkLsts->mebuch[0]->SetBitMap (BMAP::LoadBitmap (DLG::hInstance, "SEL", "MSK", WHITECOL));
*/
}


void Work::InitVpkRow (void)
{
    if (VpkLsts == NULL) return;
    sprintf (VpkLsts->typ,       "%s",      "Material");
    VpkLsts->typ_kz = 1;
    sprintf (VpkLsts->nr,        "%d",      0l);
    sprintf (VpkLsts->kutf,      "%4d",     0);
    sprintf (VpkLsts->vpk_me,  "%8.3lf",  0.0);
    sprintf (VpkLsts->fuell_me,  "%8.4lf",  0.0);
    sprintf (VpkLsts->pr_ek,     "%.3lf",  0.0);
    sprintf (VpkLsts->kosten,    "%.3lf",  0.0);
    VpkLsts->gewek = 0.0;
    VpkLsts->a_gew = 0.0;
/*
    sprintf (VpkLsts->me_buch,   "%s",      "N");
    if (VpkLsts->mebuch != NULL &&
        VpkLsts->mebuch[0] == NULL)
    {
        VpkLsts->mebuch[0] = new ListButton (VpkLsts->me_buch, NULL, NULL, 1);
        VpkLsts->mebuch[0]->SetBitMap (BMAP::LoadBitmap (DLG::hInstance, "SEL", "MSK", WHITECOL));
   }
   else
   {
        VpkLsts->mebuch[0]->SetFeld (VpkLsts->me_buch);
   }
*/
}


void Work::DeleteAllVpkPos (void)
{
    prdk_vpk.mdn =  prdk_k.mdn;
    prdk_vpk.a  = prdk_k.a;
    prdk_vpk.variante = prdk_k.variante;
    
    Prdk_k.sqlin ((short *) &prdk_k.mdn, 1, 0);
    Prdk_k.sqlin ((double *)&prdk_k.a, 3, 0);
    Prdk_k.sqlin ((short *) &prdk_k.variante, 1, 0);
    Prdk_k.sqlcomm ("delete from prdk_vpk "
                    "where mdn = ? "
                    "and a = ? "
                    "and variante = ?");
}

void Work::WriteVpkRec (void)
{
   double a_gew;
   short me_einh;

   me_einh = 2;
   prdk_vpk.a_vpk = ratod (VpkLsts->nr);
   Prdk_k.sqlin ((double *) &prdk_vpk.a_vpk, 3, 0);
   Prdk_k.sqlout ((short *) &me_einh, 1, 0);
   Prdk_k.sqlout ((double *) &a_gew, 3, 0);
   Prdk_k.sqlcomm ("select me_einh, a_gew from a_bas "
                   "where a_bas.a = ?");
   if (a_gew == 0.0)
   {
       a_gew = 1.0;
   }
   prdk_vpk.bearb_flg  = atoi (VpkLsts->kutf);
   prdk_vpk.fuel_gew  = ratod (VpkLsts->fuell_me);
   prdk_vpk.me         = ratod (VpkLsts->vpk_me);
   prdk_vpk.typ        = VpkLsts->typ_kz;
   if (me_einh == 2)
   {
           prdk_vpk.gew = prdk_vpk.me;
   }
   else
   {
           prdk_vpk.gew = prdk_vpk.me * a_gew;
   }
//   strcpy (prdk_vpk.me_buch, VpkLsts->mebuch[0]->GetFeld ());
   Prdk_vpk.dbupdate ();
   if (PlanEk)  //#141205
   {
	   a_preis.mdn = prdk_k.mdn;
	   a_preis.plan_ek = ratod (VpkLsts->pr_ek);
	   a_preis.plan_bep = ratod (VpkLsts->pr_ek);
	   a_preis.a = ratod (VpkLsts->nr);
	   A_preis.dbupdate ();
   }
}

int Work::PrepareVpk (void)
{
    Prdk_k.sqlin ((short *) &prdk_k.mdn, 1, 0);
    Prdk_k.sqlin ((double *)&prdk_k.a, 3, 0);
    Prdk_k.sqlin ((short *) &prdk_k.variante, 1, 0);

    Prdk_k.sqlout ((short *)  &prdk_vpk.mdn, 1, 0);
    Prdk_k.sqlout ((double *) &prdk_vpk.a, 3, 0);
    Prdk_k.sqlout ((double *) &prdk_vpk.a_vpk, 3, 0);
    Prdk_k.sqlout ((short *)  &prdk_vpk.variante, 1, 0);
    Prdk_k.sqlout ((short *)  &prdk_vpk.bearb_flg, 1, 0);
    vpk_cursor =  Prdk_k.sqlcursor ("select mdn, a, a_vpk, variante, bearb_flg "
                                    "from prdk_vpk "
                                    "where mdn = ? "
                                    "and a = ? "
                                    "and variante = ? "
                                    "order by bearb_flg");
    return vpk_cursor;
}

int Work::OpenVpk (void)
{
    if (vpk_cursor == -1)
    {
        PrepareVpk ();
        if (vpk_cursor == -1)
        {
            return 0;
        }
    }
    Prdk_k.sqlopen (vpk_cursor);
    return 0;
}

int Work::FetchVpk (void)
{
    if (vpk_cursor == -1)
    {
        PrepareVpk ();
        if (vpk_cursor == -1)
        {
            return 0;
        }
    }
    int dsqlstatus = Prdk_k.sqlfetch (vpk_cursor);
    if (dsqlstatus == 0)
    {
        dsqlstatus = Prdk_vpk.dbreadfirst ();
    }
    return dsqlstatus;
}

int Work::CloseVpk (void)
{
    if (vpk_cursor != -1)
    {
        Prdk_k.sqlclose (vpk_cursor);
        vpk_cursor = -1;
    }
    return 0;
}


// Kosten


void Work::GetKostBez (char *kost_bez, double *kost_wrt, char *vkost_rechb, 
                       double *faktor, short kost_art)
{
    strcpy (kost_bez, "");
    if (kost_art == 0.0)
    {
        return;
    }
    Prdk_k.sqlin  ((short *) &kost_art, 1, 0);
    Prdk_k.sqlout ((char *) kost_bez, 0, 25);
    Prdk_k.sqlout ((double *) kost_wrt, 3, 0);
    Prdk_k.sqlout ((char *) vkost_rechb, 0, 4);
    Prdk_k.sqlout ((double *) faktor, 3, 0);
    Prdk_k.sqlcomm ("select kost_art_bz, vkost_wt, vkost_rechb, faktor from kost_art "
                    "where kost_art = ?");
}

void Work::rechne_zeit (void)
{
    Prdk_k.sqlin ((double *) &chgfaktor, 3, 0);
    Prdk_k.sqlin  ((short *) &prdk_k.mdn, 1, 0);
    Prdk_k.sqlin ((double *) &prdk_k.a, 3, 0);
    Prdk_k.sqlcomm ("update prdk_zeit set zeit_var = (zeit_var * ?)  "
                    "where mdn = ? "
					"and a = ?");
}
void Work::rechne_varb (void)
{
    Prdk_k.sqlin ((double *) &chgfaktor, 3, 0);
    Prdk_k.sqlin ((double *) &chgfaktor, 3, 0);
    Prdk_k.sqlin ((double *) &chgfaktor, 3, 0);

    Prdk_k.sqlin  ((short *) &prdk_k.mdn, 1, 0);
    Prdk_k.sqlin ((char *) prdk_k.rez, 0, 9);
    Prdk_k.sqlin  ((short *) &prdk_k.variante, 1, 0);
    Prdk_k.sqlcomm ("update prdk_varb set "
					 "varb_me = (varb_me * ?),  "
					 "gew_bto = (gew_bto * ?),  "
					 "gew = (gew * ?)  "
                    "where mdn = ? "
					"and rez = ? "
					 " and variante = ?");
}
void Work::rechne_zut (void)
{
    Prdk_k.sqlin ((double *) &chgfaktor, 3, 0);

    Prdk_k.sqlin  ((short *) &prdk_k.mdn, 1, 0);
    Prdk_k.sqlin ((char *) prdk_k.rez, 0, 9);
    Prdk_k.sqlin  ((short *) &prdk_k.variante, 1, 0);
    Prdk_k.sqlcomm ("update prdk_zut set "
					 "zut_me = (zut_me * ?)  "
                    "where mdn = ? "
					"and rez = ? "
					 " and variante = ?");
}


BOOL Work::GetKostBez (void)
{
    short kost_art;

    kost_art = atoi (KostLsts->kost_art);
    if (kost_art == 0l)
    {
        return FALSE;
    }
    strcpy (KostLsts->kost_art_bez, "");
    Prdk_k.sqlin ((short *) &kost_art, 1, 0);
    Prdk_k.sqlout ((char *) KostLsts->kost_art_bez, 0, 25);
    Prdk_k.sqlout ((double *) &KostLsts->kost_wrt, 3, 0);
    Prdk_k.sqlout ((char *) KostLsts->me_einh, 0, 4);
    Prdk_k.sqlout ((double *) &KostLsts->faktor, 3, 0);
    int dsqlstatus = Prdk_k.sqlcomm ("select kost_art_bz, vkost_wt, vkost_rechb, faktor "
                                     "from kost_art "
                                     "where kost_art.kost_art = ?");
    if (dsqlstatus != 0)
    {
        return FALSE;
    }

    CalcMe ();
    return TRUE;
}



double Work::CalcKosten (long vkost_rechb, double kost_wrt, short stufe, double faktor)
{
    double kosten = 0.0;
	double kostenek = 0.0;
    double VarbWertBep;
    double ZutWertBep;
    double HuelWertBep;
    double VpkWertBep;
    double VarbWertEk;
    double ZutWertEk;
    double HuelWertEk;
    double VpkWertEk;
    double AfsWertEk;
    double AfsWertBep;
    double SwWertBep;
    double SwGew;
	double SKWertVollk;
	double FilEKWertVollk;
	double FilVKWertVollk;
	double HKWertVollk;



    if (vkost_rechb == 4)           // % Aufschlag   
    {
		double SwProz = prdk_k.sw_kalk / 100;
		double Menge_bto = prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew;
		double Menge_nto = prdk_k.varb_gew_nto + prdk_k.zut_gew + prdk_k.huel_gew;
        double SwMenge = ((prdk_k.varb_gew - prdk_k.varb_gew_nto) + SwProz * Menge_nto) * -1;
		SwProz = (Menge_bto -  (Menge_bto - SwMenge)) / Menge_bto  * -1;
       kosten = kost_wrt / 100;
       kostenek = kost_wrt / 100;
	   VarbWertBep = Round((prdk_k.varb_hk_vollk * prdk_k.chg_gew),2);
	   ZutWertBep = Round((prdk_k.zut_hk_vollk * prdk_k.chg_gew),2);
	   HuelWertBep = Round((prdk_k.huel_hk_wrt),2);
	   VpkWertBep = Round((prdk_k.vpk_hk_wrt),2);
	   VarbWertEk = Round((prdk_k.varb_mat_o_b * prdk_k.chg_gew),2);
	   ZutWertEk = Round((prdk_k.zut_mat_o_b * prdk_k.chg_gew),2);
	   HuelWertEk = Round((prdk_k.huel_wrt),2);
	   VpkWertEk = Round((prdk_k.vpk_wrt),2);
	   AfsWertBep = Round((prdk_k.kost_hk_vollk * prdk_k.nto_gew),2);
	   AfsWertEk = Round((prdk_k.kost_hk_teilk * prdk_k.nto_gew),2);
	   SKWertVollk = Round((prdk_k.sk_wrt),2);
	   FilEKWertVollk = Round((prdk_k.filek_wrt),2);
	   FilVKWertVollk = Round((prdk_k.filvk_wrt),2);

	   /* 230109 jetzt muss auch Menge Brutto, Nette mit ber�cksichtigt werden 
	   SwWertBep = Round(
							(VarbWertBep + ZutWertBep + HuelWertBep) * prdk_k.sw_kalk / 100 * -1
							 ,2);
       HKWertVollk = prdk_k.a_hk_vollk * ((prdk_k.varb_gew +
												prdk_k.zut_gew +
												prdk_k.huel_gew) *
												(1 - prdk_k.sw_kalk / 100) +
												prdk_k.vpk_gew);
*******/

	   SwWertBep = Round(
							(VarbWertBep + ZutWertBep + HuelWertBep) * SwProz ,2);
	
       
	   HKWertVollk = prdk_k.a_hk_vollk * ((prdk_k.varb_gew +
												prdk_k.zut_gew +
												prdk_k.huel_gew) *
 											(1 - SwProz ) +
												prdk_k.vpk_gew);
       switch (stufe)  
       {

        case 1 :
		   kosten *= VarbWertBep; //LuD 280403
		   kostenek *= VarbWertEk; 
           break;
        case 2 :
		   kosten *= (VarbWertBep + ZutWertBep);
		   kostenek *= (VarbWertEk + ZutWertEk);
           break;
        
		case 3 :
			 kosten *= (VarbWertBep + ZutWertBep + HuelWertBep);
			 kostenek *= (VarbWertEk + ZutWertEk + HuelWertEk);
           break;
        case 4 :
			 kosten *= (VarbWertBep + ZutWertBep + HuelWertBep + VpkWertBep);
			 kostenek *= (VarbWertEk + ZutWertEk + HuelWertEk + VpkWertBep);

           break;
        case 5 :
			 kosten *= (VarbWertBep + ZutWertBep + HuelWertBep + VpkWertBep );
			 kostenek *= (VarbWertEk + ZutWertEk + HuelWertEk + VpkWertEk );
           break;
        case 6 :   //SK
//			 kosten *= (VarbWertBep + ZutWertBep + HuelWertBep + VpkWertBep + AfsWertBep );
//			 kostenek *= (VarbWertEk + ZutWertEk + HuelWertEk + VpkWertEk + AfsWertEk);
			kosten *= HKWertVollk;
			kostenek *= HKWertVollk;
           break;
        case 7 :  //FilEK
//			 kosten *= (VarbWertBep + ZutWertBep + HuelWertBep + VpkWertBep + HKWertVollk + SKWertVollk   );
//			 kostenek *= (VarbWertEk + ZutWertEk + HuelWertEk + VpkWertEk + HKWertVollk +SKWertVollk );
			 kosten *= (HKWertVollk + SKWertVollk   );
			 kostenek *= (HKWertVollk +SKWertVollk );
           break;
        case 8 :  //FilVK
//			 kosten *= (VarbWertBep + ZutWertBep + HuelWertBep + VpkWertBep + HKWertVollk +SKWertVollk + FilEKWertVollk );
//			 kostenek *= (VarbWertEk + ZutWertEk + HuelWertEk + VpkWertEk + HKWertVollk +SKWertVollk  + FilEKWertVollk );
			 kosten *= (HKWertVollk +SKWertVollk + FilEKWertVollk );
			 kostenek *= (HKWertVollk +SKWertVollk  + FilEKWertVollk );
           break;
        default :
			 kosten *= (VarbWertBep + ZutWertBep + HuelWertBep + VpkWertBep + AfsWertBep);//TODO
			 kostenek *= (VarbWertEk + ZutWertEk + HuelWertEk + VpkWertEk + AfsWertEk);
           break;
       }
    }
    else
    {
       kosten = kost_wrt * faktor;
  	   SwGew	=  (prdk_k.sw_kalk / 100 * (prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew) * -1) ;
       switch (stufe)
       {
        case 1 :
           kosten *= prdk_k.varb_gew;
           break;
        case 2 :
           kosten *= (prdk_k.varb_gew +
                      prdk_k.zut_gew);
           break;
        case 3 :
           kosten *= (prdk_k.varb_gew +
                      prdk_k.zut_gew +
                      prdk_k.huel_gew);
           break;
        case 4 :
           kosten *= (prdk_k.varb_gew +
                        prdk_k.zut_gew +
                        prdk_k.huel_gew +
						SwGew +
                        prdk_k.vpk_gew);
           break;
        default :
           kosten *= ((prdk_k.varb_gew +
                        prdk_k.zut_gew +
                        prdk_k.huel_gew) +
						SwGew +
                        prdk_k.vpk_gew);
           break;
       }
    }
	return kosten;
}






void Work::CalcKosten (void)
{
    double kosten = 0.0;
	double kostenek = 0.0;
    double VarbWertBep;
    double ZutWertBep;
    double HuelWertBep;
    double VpkWertBep;
    double VarbWertEk;
    double ZutWertEk;
    double HuelWertEk;
    double VpkWertEk;
    double AfsWertEk;
    double AfsWertBep;
    double SwWertBep;
    double SwGew;
	double SKWertVollk = 0.0;
	double FilEKWertVollk = 0.0;
	double FilVKWertVollk = 0.0;
	double HKWertVollk = 0.0;

	
    if (KostLsts == NULL) return;


    if (KostLsts->vkost_rechb == 4)           // % Aufschlag   
    {
		double SwProz = prdk_k.sw_kalk / 100;
		double Menge_bto = prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew;
		double Menge_nto = prdk_k.varb_gew_nto + prdk_k.zut_gew + prdk_k.huel_gew;
	    double SwMenge = ((prdk_k.varb_gew - prdk_k.varb_gew_nto) + SwProz * Menge_nto) * -1;
		SwProz = (Menge_bto -  (Menge_bto - SwMenge)) / Menge_bto  * -1;

       kosten = KostLsts->kost_wrt / 100;
       kostenek = KostLsts->kost_wrt / 100;
	   VarbWertBep = Round((prdk_k.varb_hk_vollk * prdk_k.chg_gew),2);
	   ZutWertBep = Round((prdk_k.zut_hk_vollk * prdk_k.chg_gew),2);
	   HuelWertBep = Round((prdk_k.huel_hk_wrt),2);
	   VpkWertBep = Round((prdk_k.vpk_hk_wrt),2);
	   VarbWertEk = Round((prdk_k.varb_mat_o_b * prdk_k.chg_gew),2);
	   ZutWertEk = Round((prdk_k.zut_mat_o_b * prdk_k.chg_gew),2);
	   HuelWertEk = Round((prdk_k.huel_wrt),2);
	   VpkWertEk = Round((prdk_k.vpk_wrt),2);
	   AfsWertBep = Round((prdk_k.kost_hk_vollk * prdk_k.nto_gew),2);
	   AfsWertEk = Round((prdk_k.kost_hk_teilk * prdk_k.nto_gew),2);
	   SKWertVollk = Round((prdk_k.sk_wrt),2);
	   FilEKWertVollk = Round((prdk_k.filek_wrt),2);
	   FilVKWertVollk = Round((prdk_k.filvk_wrt),2);
	   /* 230109 jetzt muss auch Menge Brutto, Nette mit ber�cksichtigt werden 
	   SwWertBep = Round(
							(VarbWertBep + ZutWertBep + HuelWertBep) * prdk_k.sw_kalk / 100 * -1
							 ,2);
	
       
	   HKWertVollk = prdk_k.a_hk_vollk * ((prdk_k.varb_gew +
												prdk_k.zut_gew +
												prdk_k.huel_gew) *
 											(1 - prdk_k.sw_kalk / 100) +
												prdk_k.vpk_gew);
*/
	   SwWertBep = Round(
							(VarbWertBep + ZutWertBep + HuelWertBep) * SwProz ,2);
	
       
	   HKWertVollk = prdk_k.a_hk_vollk * ((prdk_k.varb_gew +
												prdk_k.zut_gew +
												prdk_k.huel_gew) *
 											(1 - SwProz ) +
												prdk_k.vpk_gew);

       switch (KostLsts->stufe)  
       {

        case 1 :
		   kosten *= VarbWertBep; //LuD 280403
		   kostenek *= VarbWertEk; 
           break;
        case 2 :
		   kosten *= (VarbWertBep + ZutWertBep);
		   kostenek *= (VarbWertEk + ZutWertEk);
           break;
        
		case 3 :
			 kosten *= (VarbWertBep + ZutWertBep + HuelWertBep);
			 kostenek *= (VarbWertEk + ZutWertEk + HuelWertEk);
           break;
        case 4 :
			 kosten *= (VarbWertBep + ZutWertBep + HuelWertBep + VpkWertBep);
			 kostenek *= (VarbWertEk + ZutWertEk + HuelWertEk + VpkWertBep);

           break;
        case 5 :
			 kosten *= (VarbWertBep + ZutWertBep + HuelWertBep + VpkWertBep );
			 kostenek *= (VarbWertEk + ZutWertEk + HuelWertEk + VpkWertEk );
           break;
        case 6 :   //SK
//			 kosten *= (VarbWertBep + ZutWertBep + HuelWertBep + VpkWertBep + AfsWertBep );
//			 kostenek *= (VarbWertEk + ZutWertEk + HuelWertEk + VpkWertEk + AfsWertEk);
			kosten *= HKWertVollk;
			kostenek *= HKWertVollk;
           break;
        case 7 :  //FilEK
//			 kosten *= (VarbWertBep + ZutWertBep + HuelWertBep + VpkWertBep + HKWertVollk + SKWertVollk   );
//			 kostenek *= (VarbWertEk + ZutWertEk + HuelWertEk + VpkWertEk + HKWertVollk +SKWertVollk );
			 kosten *= (HKWertVollk + SKWertVollk   );
			 kostenek *= (HKWertVollk +SKWertVollk );
           break;
        case 8 :  //FilVK
//			 kosten *= (VarbWertBep + ZutWertBep + HuelWertBep + VpkWertBep + HKWertVollk +SKWertVollk + FilEKWertVollk );
//			 kostenek *= (VarbWertEk + ZutWertEk + HuelWertEk + VpkWertEk + HKWertVollk +SKWertVollk  + FilEKWertVollk );
			 kosten *= (HKWertVollk +SKWertVollk + FilEKWertVollk );
			 kostenek *= (HKWertVollk +SKWertVollk  + FilEKWertVollk );
           break;
         default :
			 kosten *= (VarbWertBep + ZutWertBep + HuelWertBep + VpkWertBep + AfsWertBep);//TODO
			 kostenek *= (VarbWertEk + ZutWertEk + HuelWertEk + VpkWertEk + AfsWertEk);
           break;
       }
    }
    else
    {
       kosten = KostLsts->kost_wrt * KostLsts->faktor;
//  	   SwGew	=  (prdk_k.sw_kalk / 100 * (prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew) * -1) ;
		double SwProz = prdk_k.sw_kalk / 100;
		double Menge_nto = prdk_k.varb_gew_nto + prdk_k.zut_gew + prdk_k.huel_gew;

  	   SwGew	=  ((prdk_k.varb_gew - prdk_k.varb_gew_nto) + SwProz * Menge_nto) * -1; //281111


       switch (KostLsts->stufe)
       {
        case 1 :
           kosten *= prdk_k.varb_gew;
           break;
        case 2 :
           kosten *= (prdk_k.varb_gew +
                      prdk_k.zut_gew);
           break;
        case 3 :
           kosten *= (prdk_k.varb_gew +
                      prdk_k.zut_gew +
                      prdk_k.huel_gew);
           break;
        case 4 :
           kosten *= (prdk_k.varb_gew +
                        prdk_k.zut_gew +
                        prdk_k.huel_gew +
						SwGew +
                        prdk_k.vpk_gew);
           break;
        default :
           kosten *= ((prdk_k.varb_gew +
                        prdk_k.zut_gew +
                        prdk_k.huel_gew) +
						SwGew +
                        prdk_k.vpk_gew);
           break;
       }
    }
    sprintf (KostLsts->kosten, "%.3lf", kosten);
    sprintf (KostLsts->kostenek, "%.3lf", kostenek);
}


void Work::FillKostRow (void)
{
    char me_einh [5];

    if (KostLsts == NULL) return;
	InitKostRow();

    KostLsts->stufe = prdk_kost.prdk_stufe;
    sprintf (KostLsts->kost_art, "%hd",  prdk_kost.kost_art);
    if (prdk_kost.prdk_stufe == 0)
    {
        prdk_kost.prdk_stufe = 1;
    }
    GetKostBez (KostLsts->kost_art_bez, &KostLsts->kost_wrt, me_einh, 
                &KostLsts->faktor, prdk_kost.kost_art);
    sprintf (KostLsts->kost_wert,   "%8.3lf",  KostLsts->kost_wrt);
    if (KostLsts->faktor == 0.0)
    {
        KostLsts->faktor = 1.0; 
    }
    KostLsts->vkost_rechb = atoi (me_einh);
    GetPtab (KostLsts->me_einh, "vkost_rechb", clipped (me_einh));
    sprintf (KostLsts->kosten,      "%.2lf",   prdk_kost.kosten);
    CalcKosten ();
    KostLsts->chtyp.SetFeld ("..");
    KostLsts->chnr.SetFeld ("..");
}

void Work::InitKostRow (void)
{
    if (KostLsts == NULL) return;
    KostLsts->stufe = 1; 
    sprintf (KostLsts->kost_art,   "%hd",     0);
    sprintf (KostLsts->kost_wert,   "%.3lf",  0.0);
    sprintf (KostLsts->kosten,      "%8.3lf",  0.0);
    strcpy (KostLsts->kost_art_bez, "");
}


void Work::DeleteAllKostPos (void)
{

	/*
    prdk_kost.mdn      =  prdk_k.mdn;
    prdk_kost.a        =  prdk_k.a;
    prdk_kost.variante = prdk_k.variante;
    
    Prdk_k.sqlin ((short *) &prdk_k.mdn, 1, 0);
    Prdk_k.sqlin ((double *)&prdk_k.a, 3, 0);
    Prdk_k.sqlin ((short *) &prdk_k.variante, 1, 0);
    Prdk_k.sqlcomm ("delete from prdk_kost "
                    "where mdn = ? "
                    "and a = ? "
                    "and variante = ?");
*/
	if (prdk_k.bearb_weg < 1 ) prdk_k.bearb_weg = prdk_k.a;
	if (prdk_k.bearb_weg_sk < 1 ) prdk_k.bearb_weg_sk = prdk_k.a;
	if (prdk_k.bearb_weg_fek < 1 ) prdk_k.bearb_weg_fek = prdk_k.a;
	if (prdk_k.bearb_weg_fvk < 1 ) prdk_k.bearb_weg_fvk = prdk_k.a;
    prdk_kost.mdn      =  prdk_k.mdn;
    prdk_kost.a        =  prdk_k.bearb_weg;
//    prdk_kost.variante = prdk_k.variante;
	prdk_kost.variante = 0;  //050210 prdk_kost nicht mehr variantenspezifisch!!
    
    Prdk_k.sqlin ((short *) &prdk_k.mdn, 1, 0);
    Prdk_k.sqlin ((double *)&prdk_k.bearb_weg, 3, 0);
    Prdk_k.sqlin ((short *) &prdk_kost.variante, 1, 0);
//    Prdk_k.sqlin ((short *) &prdk_k.mdn, 1, 0);
//    Prdk_k.sqlin ((double *)&prdk_k.a, 3, 0);
    Prdk_k.sqlcomm ("delete from prdk_kost "
                    "where mdn = ? "
                    "and a = ? "
                    "and variante = ? "
					"and prdk_stufe <= 5 ");
// 					" and a not in (select bearb_weg from prdk_k where mdn = ? and a <> ?)");

    prdk_kost.mdn      =  prdk_k.mdn;
    prdk_kost.a        =  prdk_k.bearb_weg_sk;
//    prdk_kost.variante = prdk_k.variante;
	prdk_kost.variante = 0;  //050210 prdk_kost nicht mehr variantenspezifisch!!
    
    Prdk_k.sqlin ((short *) &prdk_k.mdn, 1, 0);
    Prdk_k.sqlin ((double *)&prdk_k.bearb_weg_sk, 3, 0);
    Prdk_k.sqlin ((short *) &prdk_kost.variante, 1, 0);
//    Prdk_k.sqlin ((short *) &prdk_k.mdn, 1, 0);
//    Prdk_k.sqlin ((double *)&prdk_k.a, 3, 0);
    Prdk_k.sqlcomm ("delete from prdk_kost "
                    "where mdn = ? "
                    "and a = ? "
                    "and variante = ? "
					"and prdk_stufe = 6 ");
//					" and a not in (select bearb_weg_sk from prdk_k where mdn = ? and a <> ?)");

    prdk_kost.mdn      =  prdk_k.mdn;
    prdk_kost.a        =  prdk_k.bearb_weg_fek;
//    prdk_kost.variante = prdk_k.variante;
	prdk_kost.variante = 0;  //050210 prdk_kost nicht mehr variantenspezifisch!!
    
    Prdk_k.sqlin ((short *) &prdk_k.mdn, 1, 0);
    Prdk_k.sqlin ((double *)&prdk_k.bearb_weg_fek, 3, 0);
    Prdk_k.sqlin ((short *) &prdk_kost.variante, 1, 0);
//    Prdk_k.sqlin ((short *) &prdk_k.mdn, 1, 0);
//    Prdk_k.sqlin ((double *)&prdk_k.a, 3, 0);
    Prdk_k.sqlcomm ("delete from prdk_kost "
                    "where mdn = ? "
                    "and a = ? "
                    "and variante = ? "
					"and prdk_stufe = 7 ");
//					" and a not in (select bearb_weg_fek from prdk_k where mdn = ? and a <> ?)");

    prdk_kost.mdn      =  prdk_k.mdn;
    prdk_kost.a        =  prdk_k.bearb_weg_fvk;
//    prdk_kost.variante = prdk_k.variante;
	prdk_kost.variante = 0;  //050210 prdk_kost nicht mehr variantenspezifisch!!
    
    Prdk_k.sqlin ((short *) &prdk_k.mdn, 1, 0);
    Prdk_k.sqlin ((double *)&prdk_k.bearb_weg_fvk, 3, 0);
    Prdk_k.sqlin ((short *) &prdk_kost.variante, 1, 0);
//    Prdk_k.sqlin ((short *) &prdk_k.mdn, 1, 0);
//    Prdk_k.sqlin ((double *)&prdk_k.a, 3, 0);
    Prdk_k.sqlcomm ("delete from prdk_kost "
                    "where mdn = ? "
                    "and a = ? "
                    "and variante = ? "
					"and prdk_stufe = 8 ");
//					" and a not in (select bearb_weg_fvk from prdk_k where mdn = ? and a <> ?)");

}

void Work::WriteKostRec (void)
{

   prdk_kost.prdk_stufe  = KostLsts->stufe;
   prdk_kost.kost_me     = ratod (KostLsts->kost_wert);
   prdk_kost.kosten      = ratod (KostLsts->kosten);
   prdk_kost.kost_art    = atoi (KostLsts->kost_art);

	if (prdk_k.bearb_weg < 1 ) prdk_k.bearb_weg = prdk_k.a;
	if (prdk_k.bearb_weg_sk < 1 ) prdk_k.bearb_weg_sk = prdk_k.a;
	if (prdk_k.bearb_weg_fek < 1 ) prdk_k.bearb_weg_fek = prdk_k.a;
	if (prdk_k.bearb_weg_fvk < 1 ) prdk_k.bearb_weg_fvk = prdk_k.a;

   if (prdk_kost.prdk_stufe <= 5) prdk_kost.a = prdk_k.bearb_weg;
   if (prdk_kost.prdk_stufe == 6) prdk_kost.a = prdk_k.bearb_weg_sk;
   if (prdk_kost.prdk_stufe == 7) prdk_kost.a = prdk_k.bearb_weg_fek;
   if (prdk_kost.prdk_stufe == 8) prdk_kost.a = prdk_k.bearb_weg_fvk;
	prdk_kost.variante = 0;  //050210 prdk_kost nicht mehr variantenspezifisch!!
   Prdk_kost.dbupdate ();

}

int Work::PrepareKost (void)
{
/*
    Prdk_k.sqlin ((short *) &prdk_k.mdn, 1, 0);
    Prdk_k.sqlin ((double *)&prdk_k.a, 3, 0);
    Prdk_k.sqlin ((short *) &prdk_k.variante, 1, 0);

    Prdk_k.sqlout ((short *)  &prdk_kost.mdn, 1, 0);
    Prdk_k.sqlout ((double *) &prdk_kost.a, 3, 0);
    Prdk_k.sqlout ((short *)  &prdk_kost.prdk_stufe, 1, 0);
    Prdk_k.sqlout ((short *)  &prdk_kost.kost_art, 1, 0);
    Prdk_k.sqlout ((short *)  &prdk_kost.variante, 1, 0);
    kost_cursor =  Prdk_k.sqlcursor ("select mdn, a, prdk_stufe, kost_art, variante "
                                     "from prdk_kost "
                                     "where mdn = ? "
                                     "and a = ? "
                                     "and variante = ? "
                                     "order by prdk_stufe");
*/
	if (prdk_k.bearb_weg < 1 ) prdk_k.bearb_weg = prdk_k.a;
	if (prdk_k.bearb_weg_sk < 1 ) prdk_k.bearb_weg_sk = prdk_k.a;
	if (prdk_k.bearb_weg_fek < 1 ) prdk_k.bearb_weg_fek = prdk_k.a;
	if (prdk_k.bearb_weg_fvk < 1 ) prdk_k.bearb_weg_fvk = prdk_k.a;

    Prdk_k.sqlin ((short *) &prdk_k.mdn, 1, 0);
    Prdk_k.sqlin ((short *) &prdk_kost.variante, 1, 0);
    Prdk_k.sqlin ((double *)&prdk_k.bearb_weg, 3, 0);
    Prdk_k.sqlin ((double *)&prdk_k.bearb_weg_sk, 3, 0);
    Prdk_k.sqlin ((double *)&prdk_k.bearb_weg_fek, 3, 0);
    Prdk_k.sqlin ((double *)&prdk_k.bearb_weg_fvk, 3, 0);

    Prdk_k.sqlout ((short *)  &prdk_kost.mdn, 1, 0);
    Prdk_k.sqlout ((double *) &prdk_kost.a, 3, 0);
    Prdk_k.sqlout ((short *)  &prdk_kost.prdk_stufe, 1, 0);
    Prdk_k.sqlout ((short *)  &prdk_kost.kost_art, 1, 0);
    Prdk_k.sqlout ((short *)  &prdk_kost.variante, 1, 0);
    kost_cursor =  Prdk_k.sqlcursor ("select mdn, a, prdk_stufe, kost_art, variante "
                                     "from prdk_kost "
                                     "where mdn = ? "
                                     "and variante = ? "
                                     "and ( ( "
											  "prdk_stufe <= 5 and a = ? "
										    ") "
                                        "or ( "
											"prdk_stufe = 6 and a = ? "
										    ") "
                                        "or ( "
											"prdk_stufe = 7 and a = ? "
										    ") "
                                        "or ( "
											"prdk_stufe = 8 and a = ? "
										    ") "
                                        " ) "
                                     "order by prdk_stufe");

    return kost_cursor;
}

int Work::OpenKost (void)
{
    if (kost_cursor == -1)
    {
        PrepareKost ();
        if (kost_cursor == -1)
        {
            return 0;
        }
    }
	prdk_kost.variante = 0;  //050210 prdk_kost nicht mehr variantenspezifisch!!

    Prdk_k.sqlopen (kost_cursor);
    return 0;
}

int Work::FetchKost (void)
{
    if (kost_cursor == -1)
    {
        PrepareKost ();
        if (kost_cursor == -1)
        {
            return 0;
        }
    }
	prdk_kost.variante = 0;  //050210 prdk_kost nicht mehr variantenspezifisch!!
    int dsqlstatus = Prdk_k.sqlfetch (kost_cursor);
    if (dsqlstatus == 0)
    {
        dsqlstatus = Prdk_kost.dbreadfirst ();
    }
    return dsqlstatus;
}

int Work::CloseKost (void)
{
    if (kost_cursor != -1)
    {
        Prdk_k.sqlclose (kost_cursor);
        kost_cursor = -1;
    }
    return 0;
}


// Zeiten


void Work::GetZeitBez (char *zeit_bez, short prod_schritt)
{
    strcpy (zeit_bez, "");
	GetPtabLong (zeit_bez, "prod_schritt", prod_schritt);

}


BOOL Work::GetZeitBez (void)
{
    return TRUE;
}

/*
void Work::CalcZeiten (void)
{
}
*/


void Work::FillZeitRow (void)
{
//    char prod_schritt [5];

    if (ZeitLsts == NULL) return;
	InitZeitRow();

	ZeitLsts->schritt      = prdk_zeit.prod_schritt;
	ZeitLsts->lfd          = prdk_zeit.lfd;
    GetZeitBez (ZeitLsts->prod_schritt, prdk_zeit.prod_schritt); 
//	sprintf (ZeitLsts->prod_schritt, "%hd %s", prdk_zeit.prod_schritt, prod_schritt);
    sprintf (ZeitLsts->text, "%s", prdk_zeit.text);
    sprintf (ZeitLsts->kosten,     "%lf", prdk_zeit.kosten);
    sprintf (ZeitLsts->zeit_fix,   "%lf", prdk_zeit.zeit_fix);
    sprintf (ZeitLsts->zeit_spfix, "%lf", prdk_zeit.zeit_spfix);
    sprintf (ZeitLsts->zeit_var,   "%lf", prdk_zeit.zeit_var);
    sprintf (ZeitLsts->su_zeit,   "%lf", prdk_zeit.zeit_var + prdk_zeit.zeit_fix + prdk_zeit.zeit_spfix);
    sprintf (ZeitLsts->su_kost,   "%lf", (prdk_zeit.zeit_var + prdk_zeit.zeit_fix + prdk_zeit.zeit_spfix)
											* prdk_zeit.kosten);
//    CalcKosten ();
    ZeitLsts->chtyp.SetFeld ("..");
    ZeitLsts->chnr.SetFeld ("..");
	ZeitLsts->m1 = prdk_k.chg_gew * prdk_k.chg_anz_bzg;
	ZeitLsts->c1 = prdk_k.chg_anz_bzg;
	ZeitLsts->m2 = prdk_k.kalk_me;
	ZeitLsts->c2 = prdk_k.chg_anz_kalk;
}

void Work::InitZeitRow (void)
{
    if (ZeitLsts == NULL) return;

	ZeitLsts->schritt      = 0;
	ZeitLsts->lfd          = 0;
    GetZeitBez (ZeitLsts->prod_schritt, 1); 
    strcpy (ZeitLsts->text, "");
    strcpy (ZeitLsts->kosten, "0");
    strcpy (ZeitLsts->zeit_fix, "0");
    strcpy (ZeitLsts->zeit_spfix, "0");
    strcpy (ZeitLsts->zeit_var, "0");
    strcpy (ZeitLsts->kostenek, "0");
    strcpy (ZeitLsts->su_zeit, "0");
    strcpy (ZeitLsts->su_kost, "0");
	ZeitLsts->m1 = prdk_k.chg_gew * prdk_k.chg_anz_bzg;
	ZeitLsts->c1 = prdk_k.chg_anz_bzg;
	ZeitLsts->m2 = prdk_k.kalk_me;
	ZeitLsts->c2 = prdk_k.chg_anz_kalk;
}

void Work::FillQuidRow (void)
{

    if (QuidLsts == NULL) return;
	InitQuidRow();
    if (prdk_quid.quid_nr == 0) sprintf (QuidLsts->quid_kat, "%s", ptabn.ptbez);
    if (strcmp (clipped(prdk_quid.quid_kat),"000") == 0) sprintf (QuidLsts->quid_kat, "%s", "FLEISCH ANTEIL");
    if (prdk_quid.quid_nr > 0) sprintf (QuidLsts->quid_nr, "%hd", prdk_quid.quid_nr);
    sprintf (QuidLsts->quid_bez, "%s", quid.bezeichnung);
	double quid_proz = prdk_quid.flproz + prdk_quid.gflproz;

    if (strcmp (clipped(prdk_quid.quid_kat),"K") != 0) sprintf (QuidLsts->quid_proz, "%5.2lf %s", quid_proz,"%");

}

void Work::InitQuidRow (void)
{
    if (QuidLsts == NULL) return;
    sprintf (QuidLsts->quid_kat, "%s", "");
    sprintf (QuidLsts->quid_nr, "%s", "");
    sprintf (QuidLsts->quid_bez, "%s", "");
    sprintf (QuidLsts->quid_proz, "%s", "");
}


int Work::PrepareQuid (void)
{
	prdk_quid.mdn = prdk_k.mdn;
	strcpy(prdk_quid.rez,prdk_k.rez);
	prdk_quid.variante   = prdk_k.variante;
    Prdk_k.sqlin ((short *) &prdk_k.mdn, 1, 0);
    Prdk_k.sqlin ((char *) &prdk_k.rez, 0, 9);
    Prdk_k.sqlin ((short *) &prdk_k.variante, 1, 0);

    Prdk_k.sqlout ((char *) &prdk_quid.quid_kat, 0, 4);
    Prdk_k.sqlout ((short *) &prdk_quid.quid_nr, 1, 0);
    Prdk_k.sqlout ((double *)  &prdk_quid.flproz, 3, 0);
    Prdk_k.sqlout ((double *)  &prdk_quid.gflproz, 3, 0);
    Prdk_k.sqlout ((long *)  &ptabn.ptlfnr, 1, 0);
    Prdk_k.sqlout ((char *)  &ptabn.ptbez, 0, 33);
    Prdk_k.sqlout ((char *)  &quid.bezeichnung, 0, 81);
    quid_cursor =  Prdk_k.sqlcursor ("select prdk_quid.quid_kat,prdk_quid.quid_nr,flproz,gflproz,ptabn.ptlfnr,ptabn.ptbez,quid.bezeichnung "
                                     "from prdk_quid,outer quid,outer ptabn "
                                     "where prdk_quid.mdn = ? "
                                     "and prdk_quid.rez = ? "
                                     "and prdk_quid.variante = ? "
									 "and prdk_quid.quid_kat = ptabn.ptwert "
									 "and ptabn.ptitem = \"quid_kategorie\" "
									 "and prdk_quid.quid_nr = quid.quid_nr "
                                     "order by ptabn.ptlfnr, prdk_quid.quid_nr ");
    return quid_cursor;
}

int Work::OpenQuid (void)
{
    if (zeit_cursor == -1)
    {
        PrepareQuid ();
        if (zeit_cursor == -1)
        {
            return 0;
        }
    }
    Prdk_k.sqlopen (quid_cursor);
    return 0;
}

int Work::FetchQuid (void)
{
    if (quid_cursor == -1)
    {
        PrepareQuid ();
        if (quid_cursor == -1)
        {
            return 0;
        }
    }
    int dsqlstatus = Prdk_k.sqlfetch (quid_cursor);
    return dsqlstatus;
}

int Work::CloseQuid (void)
{
    if (quid_cursor != -1)
    {

        Prdk_k.sqlclose (quid_cursor);
        quid_cursor = -1;
    }
    return 0;
}






void Work::DeleteAllZeitPos (void)
{

    prdk_kost.mdn      =  prdk_k.mdn;
    prdk_kost.a        =  prdk_k.a;
    
    Prdk_k.sqlin ((short *) &prdk_k.mdn, 1, 0);
    Prdk_k.sqlin ((double *)&prdk_k.a, 3, 0);
    Prdk_k.sqlcomm ("delete from prdk_zeit "
                    "where mdn = ? "
                    "and a = ?");

}


void Work::WriteZeitRec (int lfd)
{

	prdk_zeit.lfd = lfd;
    prdk_zeit.prod_schritt = ZeitLsts->schritt;    
	sprintf ( prdk_zeit.text, "%s", ZeitLsts->text);
    prdk_zeit.kosten     = ratod (ZeitLsts->kosten);
    prdk_zeit.zeit_fix   = ratod  (ZeitLsts->zeit_fix);
    prdk_zeit.zeit_spfix =  ratod (ZeitLsts->zeit_spfix);
    prdk_zeit.zeit_var   = ratod (ZeitLsts->zeit_var);
    Prdk_zeit.dbupdate ();

}

int Work::PrepareZeit (void)
{
	prdk_zeit.mdn = prdk_k.mdn;
	prdk_zeit.a   = prdk_k.a;
    Prdk_k.sqlin ((short *) &prdk_k.mdn, 1, 0);
    Prdk_k.sqlin ((double *)&prdk_k.a, 3, 0);

    Prdk_k.sqlout ((short *)  &prdk_zeit.mdn, 1, 0);
    Prdk_k.sqlout ((double *) &prdk_zeit.a, 3, 0);
    Prdk_k.sqlout ((short *)  &prdk_zeit.lfd, 1, 0);
    zeit_cursor =  Prdk_k.sqlcursor ("select mdn, a, lfd "
                                     "from prdk_zeit "
                                     "where mdn = ? "
                                     "and a = ? "
                                     "order by lfd");
    return zeit_cursor;
}

int Work::OpenZeit (void)
{
    if (zeit_cursor == -1)
    {
        PrepareZeit ();
        if (zeit_cursor == -1)
        {
            return 0;
        }
    }
    Prdk_k.sqlopen (zeit_cursor);
    return 0;
}

int Work::FetchZeit (void)
{
    if (zeit_cursor == -1)
    {
        PrepareZeit ();
        if (zeit_cursor == -1)
        {
            return 0;
        }
    }
    int dsqlstatus = Prdk_k.sqlfetch (zeit_cursor);
    if (dsqlstatus == 0)
    {
        dsqlstatus = Prdk_zeit.dbreadfirst ();
    }
    return dsqlstatus;
}

int Work::CloseZeit (void)
{
    if (zeit_cursor != -1)
    {

        Prdk_k.sqlclose (zeit_cursor);
        zeit_cursor = -1;
    }
    return 0;
}

void Work::CopyZeit (short mdn, double a)
{
	short lfd;
	short newlfd;

	if (mdn == prdk_zeit.mdn &&
		a   == prdk_zeit.a)
	{
		return;
	}
	Prdk_k.sqlin ((short *) &mdn, 1, 0);
	Prdk_k.sqlin ((double *) &a,  3, 0);
	Prdk_k.sqlout ((short *) &lfd, 1, 0);
	int cursor = Prdk_k.sqlcursor ("select lfd from prdk_zeit "
                                   "where mdn = ? "
								   "and a = ? "
								   "order by lfd");
	int dsqlstatus = Prdk_k.sqlfetch (cursor);
	if (dsqlstatus != 0)
	{
		return;
	}
	Prdk_k.sqlin ((short *)  &prdk_k.mdn, 1, 0);
	Prdk_k.sqlin ((double *) &prdk_k.a,  3, 0);
	Prdk_k.sqlcomm ("delete from prdk_zeit "
                                   "where mdn = ? "
								   "and a = ?");

// fals nicht gel�scht wurde, wird nich lfd erh�ht, sp�ter 
// eventuel zum Einstellen.

	Prdk_k.sqlin ((short *)  &prdk_k.mdn, 1, 0);
	Prdk_k.sqlin ((double *) &prdk_k.a,  3, 0);
	Prdk_k.sqlout ((short *) &newlfd, 1, 0);
	Prdk_k.sqlcomm ("select max (lfd) from prdk_zeit "
                    "where mdn = ? "
	                "and a = ?");
	newlfd = (newlfd < 0) ? 1 : newlfd + 1;

	while (dsqlstatus == 0)
	{
		prdk_zeit.mdn = mdn;
		prdk_zeit.a   = a;
		prdk_zeit.lfd = lfd;
		if (Prdk_zeit.dbreadfirst () != 0)
		{
			break;
		}

		prdk_zeit.mdn = prdk_k.mdn;
		prdk_zeit.a   = prdk_k.a;
		prdk_zeit.lfd = newlfd;
        Prdk_zeit.dbupdate ();
        newlfd ++;
		dsqlstatus = Prdk_k.sqlfetch (cursor);
	}
    Prdk_k.sqlclose (cursor);		               
}

void Work::CalcMe (void)
{
          if (HuelLsts == NULL) return;

          double fill_me = ratod (HuelLsts->fuell_me);
//          double me = prdk_k.chg_gew / fill_me;
          double me = (prdk_k.varb_gew_nto + prdk_k.zut_gew) / fill_me; 

//          double me = (prdk_k.summegew_GE + prdk_k.summegew_EL + prdk_k.summegew_FF + prdk_k.zut_gew) / fill_me; 
		  me = Round(me,3);//#180803
          if (fill_me == 0)
          {
              return;
          }
          sprintf (HuelLsts->huel_me, "%.1lf", me);
          sprintf (HuelLsts->preisme, "%.3lf", ratod (HuelLsts->pr_ek) *
                                             ratod (HuelLsts->huel_me));
}

void Work::CalcVpkMe (void)
{
          if (VpkLsts == NULL) return;

          double fill_me = ratod (VpkLsts->fuell_me);
//          double me = prdk_k.chg_gew / fill_me;
          double me = (prdk_k.varb_gew_nto + prdk_k.zut_gew) / fill_me;
//          double me = (prdk_k.summegew_GE + prdk_k.summegew_EL + prdk_k.summegew_FF + prdk_k.zut_gew) / fill_me;
		  me = me * (100 - prdk_k.sw_kalk) / 100; //210807
		  me = Round(me,3); //#180803
          if (fill_me == 0)
          {
              return;
          }
          sprintf (VpkLsts->vpk_me, "%.3lf", me);
          sprintf (VpkLsts->preisme, "%.3lf", Round((VpkLsts->prek) *
                                             ratod (VpkLsts->vpk_me),3)); 
}

void Work::CalcFuellMe (void)
{
          if (HuelLsts == NULL) return;

          double me = ratod (HuelLsts->huel_me);
          if (me == 0)
          {
              return;
          }
//          double fill_me = prdk_k.chg_gew / me;
          double fill_me = (prdk_k.varb_gew_nto + prdk_k.zut_gew) / me;
          sprintf (HuelLsts->fuell_me, "%.4lf", fill_me);
}

void Work::CalcVpkFuellMe (void)
{
          if (VpkLsts == NULL) return;

          double me = ratod (VpkLsts->vpk_me);
          if (me == 0)
          {
              return;
          }
//          double fill_me = prdk_k.chg_gew / me;
          double fill_me = (prdk_k.varb_gew_nto + prdk_k.zut_gew) / me;
		  fill_me = fill_me * (100 - prdk_k.sw_kalk) / 100; //210807
		  fill_me = Round(fill_me,4); //#180803
          sprintf (VpkLsts->fuell_me, "%.4lf", fill_me);


}

void Work::ReadMasch (char *dest, long masch_nr)
{
          strcpy (dest, "");

          Prdk_k.sqlin ((long *) &masch_nr, 2, 0);
          Prdk_k.sqlout ((char *) dest, 0, 25);
          Prdk_k.sqlcomm ("select masch_bz from maschinen "
                          "where masch_nr = ?");
}

int Work::CopyArtikel (short Ursprung_mdn, char *Ursprung_rez, double Ursprung_a, short Ursprung_variante,
					   short Kopie_mdn,    char *Kopie_rez,    double Kopie_a,    short Kopie_variante)
{
	int dsqlstatus ;
	short flg_bearb_weg ;
	short flg_bearb_weg_sk ;
	short flg_bearb_weg_fek;
	short flg_bearb_weg_fvk;

	flg_bearb_weg = 0;
	flg_bearb_weg_sk = 0;
	flg_bearb_weg_fek = 0;
	flg_bearb_weg_fvk = 0;

		prdk_k.a = Ursprung_a;
		prdk_k.mdn = Ursprung_mdn;
		strcpy(prdk_k.rez,Ursprung_rez);
		prdk_k.variante = Ursprung_variante;
    dsqlstatus = Prdk_k.dbreadfirst ();
	if (dsqlstatus == 0)
	{
		prdk_k.a = Kopie_a;
		prdk_k.mdn = Kopie_mdn;
		strcpy(prdk_k.rez,Kopie_rez);
		prdk_k.variante = Kopie_variante;

        if (prdk_k.bearb_weg == Ursprung_a)
		{
			prdk_k.bearb_weg = Kopie_a;
			flg_bearb_weg = 1;
		}
        if (prdk_k.bearb_weg_sk == Ursprung_a)
		{
			prdk_k.bearb_weg_sk = Kopie_a;
			flg_bearb_weg_sk = 1;
		}
        if (prdk_k.bearb_weg_fek == Ursprung_a)
		{
			prdk_k.bearb_weg_fek = Kopie_a;
			flg_bearb_weg_fek = 1;
		}
        if (prdk_k.bearb_weg_fvk == Ursprung_a)
		{
			prdk_k.bearb_weg_fvk = Kopie_a;
			flg_bearb_weg_fvk = 1;
		}

		Prdk_k.dbupdate();


// Verarbeitungsmaterial

           prdk_varb.mdn = Ursprung_mdn;
           strcpy (prdk_varb.rez, Ursprung_rez);
		   prdk_varb.variante = Ursprung_variante;

           Prdk_k.sqlin ((short *) &Ursprung_mdn, 1, 0);
           Prdk_k.sqlin ((char *)  Ursprung_rez,  0, 9);
           Prdk_k.sqlin ((short *) &Ursprung_variante, 1, 0);

           Prdk_k.sqlout ((long *) &prdk_varb.mat, 2, 0);
           Prdk_k.sqlout ((short *) &prdk_varb.kutf, 1, 0);
           Prdk_k.sqlout ((short *) &prdk_varb.id_nr, 1, 0);
           int cursor = Prdk_k.sqlcursor ("select mat,kutf,id_nr from prdk_varb "
                                          "where mdn = ? "
                                          "and rez = ? "
                                          "and variante = ?");
           dsqlstatus = Prdk_k.sqlfetch (cursor);
           while (dsqlstatus == 0)
           {
				prdk_varb.mdn = Ursprung_mdn;
				strcpy(prdk_varb.rez,Ursprung_rez);
				prdk_varb.variante = Ursprung_variante;
               dsqlstatus = Prdk_varb.dbreadfirst ();
				prdk_varb.mdn = Kopie_mdn;
				strcpy(prdk_varb.rez,Kopie_rez);
				prdk_varb.variante = Kopie_variante;
               Prdk_varb.dbupdate ();
               dsqlstatus = Prdk_k.sqlfetch (cursor);
           }
           Prdk_k.sqlclose (cursor);

// Zutaten

           prdk_zut.mdn = Ursprung_mdn;
           strcpy (prdk_zut.rez, Ursprung_rez);
		   prdk_zut.variante = Ursprung_variante;

           Prdk_k.sqlin ((short *)   &Ursprung_mdn, 1, 0);
           Prdk_k.sqlin ((char *)    Ursprung_rez,  0, 9);
           Prdk_k.sqlin ((short *) &Ursprung_variante, 1, 0);

           Prdk_k.sqlout ((long *) &prdk_zut.mat, 2, 0);
           Prdk_k.sqlout ((short *) &prdk_zut.kutf, 1, 0);
           cursor = Prdk_k.sqlcursor ("select mat,kutf from prdk_zut "
                                          "where mdn = ? "
                                          "and rez = ? "
                                          "and variante = ?");
           dsqlstatus = Prdk_k.sqlfetch (cursor);
           while (dsqlstatus == 0)
           {
				prdk_zut.mdn = Ursprung_mdn;
				strcpy(prdk_zut.rez,Ursprung_rez);
				prdk_zut.variante = Ursprung_variante;
               dsqlstatus = Prdk_zut.dbreadfirst ();
				prdk_zut.mdn = Kopie_mdn;
				strcpy(prdk_zut.rez,Kopie_rez);
				prdk_zut.variante = Kopie_variante;
               Prdk_zut.dbupdate ();
               dsqlstatus = Prdk_k.sqlfetch (cursor);
           }
           Prdk_k.sqlclose (cursor);

// Huellen
           prdk_huel.mdn = Ursprung_mdn;
           prdk_huel.a   = Ursprung_a;

           Prdk_k.sqlin ((short *)   &Ursprung_mdn, 1, 0);
           Prdk_k.sqlin ((double *)  &Ursprung_a,  3, 0);
           Prdk_k.sqlin ((short *) &Ursprung_variante, 1, 0);

           Prdk_k.sqlout ((long *) &prdk_huel.mat, 2, 0);
           Prdk_k.sqlout ((short *) &prdk_huel.bearb_flg, 1, 0);
           cursor = Prdk_k.sqlcursor ("select mat, bearb_flg from prdk_huel "
                                          "where mdn = ? "
                                          "and a = ? "
                                          "and variante = ?");
           dsqlstatus = Prdk_k.sqlfetch (cursor);
           while (dsqlstatus == 0)
           {
				prdk_huel.mdn = Ursprung_mdn;
				prdk_huel.a = Ursprung_a;
				prdk_huel.variante = Ursprung_variante;
               dsqlstatus = Prdk_huel.dbreadfirst ();
				prdk_huel.mdn = Kopie_mdn;
				prdk_huel.a = Kopie_a;
				prdk_huel.variante = Kopie_variante;
               Prdk_huel.dbupdate ();
               dsqlstatus = Prdk_k.sqlfetch (cursor);
           }
           Prdk_k.sqlclose (cursor);

// Verpackung
           prdk_vpk.mdn = Ursprung_mdn;
           prdk_vpk.a   = Ursprung_a;

           Prdk_k.sqlin ((short *)   &Ursprung_mdn, 1, 0);
           Prdk_k.sqlin ((double *)  &Ursprung_a,  3, 0);
           Prdk_k.sqlin ((short *) &Ursprung_variante, 1, 0);

           Prdk_k.sqlout ((double *) &prdk_vpk.a_vpk, 3, 0);
           Prdk_k.sqlout ((short *) &prdk_vpk.bearb_flg, 1, 0);
           cursor = Prdk_k.sqlcursor ("select a_vpk, bearb_flg from prdk_vpk "
                                          "where mdn = ? "
                                          "and a = ? "
                                          "and variante = ?");
           dsqlstatus = Prdk_k.sqlfetch (cursor);
           while (dsqlstatus == 0)
           {
				prdk_vpk.mdn = Ursprung_mdn;
				prdk_vpk.a = Ursprung_a;
				prdk_vpk.variante = Ursprung_variante;
               dsqlstatus = Prdk_vpk.dbreadfirst ();
				prdk_vpk.mdn = Kopie_mdn;
				prdk_vpk.a = Kopie_a;
				prdk_vpk.variante = Kopie_variante;
               Prdk_vpk.dbupdate ();
               dsqlstatus = Prdk_k.sqlfetch (cursor);
           }
           Prdk_k.sqlclose (cursor);


// Kosten bearb_weg KH  prdk_stufe <= 5
        if (flg_bearb_weg == 1)
		{
           prdk_kost.mdn = Ursprung_mdn;
           prdk_kost.a   = Ursprung_a;

           Prdk_k.sqlin ((short *)   &Ursprung_mdn, 1, 0);
           Prdk_k.sqlin ((double *)  &Ursprung_a,  3, 0);
           Prdk_k.sqlin ((short *) &Ursprung_variante, 1, 0);

           Prdk_k.sqlout ((short *) &prdk_kost.prdk_stufe, 1, 0);
           Prdk_k.sqlout ((short *) &prdk_kost.kost_art, 1, 0);
           cursor = Prdk_k.sqlcursor ("select prdk_stufe, kost_art from prdk_kost "
                                          "where mdn = ? "
                                          "and a = ? "
                                          "and variante = ? and prdk_stufe <= 5");
           dsqlstatus = Prdk_k.sqlfetch (cursor);
           while (dsqlstatus == 0)
           {
				prdk_kost.mdn = Ursprung_mdn;
				prdk_kost.a = Ursprung_a;
//050110				prdk_kost.variante = Ursprung_variante;
				prdk_kost.variante = 0; //050110
               dsqlstatus = Prdk_kost.dbreadfirst ();
				prdk_kost.mdn = Kopie_mdn;
				prdk_kost.a = Kopie_a;
//050110				prdk_kost.variante = Kopie_variante;
				prdk_kost.variante = 0; //050110
               Prdk_kost.dbupdate ();
               dsqlstatus = Prdk_k.sqlfetch (cursor);
           }
           Prdk_k.sqlclose (cursor);
		}

// Kosten bearb_weg_sk  prdk_stufe = 6
        if (flg_bearb_weg_sk == 1)
		{
           prdk_kost.mdn = Ursprung_mdn;
           prdk_kost.a   = Ursprung_a;

           Prdk_k.sqlin ((short *)   &Ursprung_mdn, 1, 0);
           Prdk_k.sqlin ((double *)  &Ursprung_a,  3, 0);
           Prdk_k.sqlin ((short *) &Ursprung_variante, 1, 0);

           Prdk_k.sqlout ((short *) &prdk_kost.prdk_stufe, 1, 0);
           Prdk_k.sqlout ((short *) &prdk_kost.kost_art, 1, 0);
           cursor = Prdk_k.sqlcursor ("select prdk_stufe, kost_art from prdk_kost "
                                          "where mdn = ? "
                                          "and a = ? "
                                          "and variante = ? and prdk_stufe = 6");
           dsqlstatus = Prdk_k.sqlfetch (cursor);
           while (dsqlstatus == 0)
           {
				prdk_kost.mdn = Ursprung_mdn;
				prdk_kost.a = Ursprung_a;
//050110				prdk_kost.variante = Ursprung_variante;
				prdk_kost.variante = 0; //050110
               dsqlstatus = Prdk_kost.dbreadfirst ();
				prdk_kost.mdn = Kopie_mdn;
				prdk_kost.a = Kopie_a;
//050110				prdk_kost.variante = Kopie_variante;
				prdk_kost.variante = 0; //050110
               Prdk_kost.dbupdate ();
               dsqlstatus = Prdk_k.sqlfetch (cursor);
           }
           Prdk_k.sqlclose (cursor);
		}

// Kosten bearb_weg_fek  prdk_stufe = 7
        if (flg_bearb_weg_fek == 1)
		{
           prdk_kost.mdn = Ursprung_mdn;
           prdk_kost.a   = Ursprung_a;

           Prdk_k.sqlin ((short *)   &Ursprung_mdn, 1, 0);
           Prdk_k.sqlin ((double *)  &Ursprung_a,  3, 0);
           Prdk_k.sqlin ((short *) &Ursprung_variante, 1, 0);

           Prdk_k.sqlout ((short *) &prdk_kost.prdk_stufe, 1, 0);
           Prdk_k.sqlout ((short *) &prdk_kost.kost_art, 1, 0);
           cursor = Prdk_k.sqlcursor ("select prdk_stufe, kost_art from prdk_kost "
                                          "where mdn = ? "
                                          "and a = ? "
                                          "and variante = ? and prdk_stufe = 7");
           dsqlstatus = Prdk_k.sqlfetch (cursor);
           while (dsqlstatus == 0)
           {
				prdk_kost.mdn = Ursprung_mdn;
				prdk_kost.a = Ursprung_a;
//050110				prdk_kost.variante = Ursprung_variante;
				prdk_kost.variante = 0; //050110
               dsqlstatus = Prdk_kost.dbreadfirst ();
				prdk_kost.mdn = Kopie_mdn;
				prdk_kost.a = Kopie_a;
//050110				prdk_kost.variante = Kopie_variante;
				prdk_kost.variante = 0; //050110
               Prdk_kost.dbupdate ();
               dsqlstatus = Prdk_k.sqlfetch (cursor);
           }
           Prdk_k.sqlclose (cursor);
		}
// Kosten bearb_weg_fvk  prdk_stufe = 8
        if (flg_bearb_weg_fvk == 1)
		{
           prdk_kost.mdn = Ursprung_mdn;
           prdk_kost.a   = Ursprung_a;

           Prdk_k.sqlin ((short *)   &Ursprung_mdn, 1, 0);
           Prdk_k.sqlin ((double *)  &Ursprung_a,  3, 0);
           Prdk_k.sqlin ((short *) &Ursprung_variante, 1, 0);

           Prdk_k.sqlout ((short *) &prdk_kost.prdk_stufe, 1, 0);
           Prdk_k.sqlout ((short *) &prdk_kost.kost_art, 1, 0);
           cursor = Prdk_k.sqlcursor ("select prdk_stufe, kost_art from prdk_kost "
                                          "where mdn = ? "
                                          "and a = ? "
                                          "and variante = ? and prdk_stufe = 8");
           dsqlstatus = Prdk_k.sqlfetch (cursor);
           while (dsqlstatus == 0)
           {
				prdk_kost.mdn = Ursprung_mdn;
				prdk_kost.a = Ursprung_a;
//050110				prdk_kost.variante = Ursprung_variante;
				prdk_kost.variante = 0; //050110
               dsqlstatus = Prdk_kost.dbreadfirst ();
				prdk_kost.mdn = Kopie_mdn;
				prdk_kost.a = Kopie_a;
				prdk_kost.variante = 0; //050110
//050110				prdk_kost.variante = Kopie_variante;
               Prdk_kost.dbupdate ();
               dsqlstatus = Prdk_k.sqlfetch (cursor);
           }
           Prdk_k.sqlclose (cursor);
		}


	}
	return 0;
}

int Work::CopyActiveVar (short mdn, char *rez, double a, short variante)
{
           short varactive;

           
// Kopf  
           Prdk_k.sqlin ((short *)   &mdn, 1, 0);
           Prdk_k.sqlin ((char *)    rez,  0, 9);
           Prdk_k.sqlin ((double *)  &a,  3, 0);
           Prdk_k.sqlout ((short *) &varactive, 1, 0);
           int dsqlstatus = Prdk_k.sqlcomm ("select variante from prdk_k "
                                            "where mdn = ? "
                                            "and rez = ? "
                                            "and akv = 1");
           if (dsqlstatus != 0)
           {
               return 100;
           }

           prdk_k.mdn = mdn;
           strcpy (prdk_k.rez, rez);
           prdk_k.a = a;
           prdk_k.variante = varactive;
           dsqlstatus = Prdk_k.dbreadfirst ();
           if (dsqlstatus == 0)
           {
               prdk_k.variante = variante;
               Prdk_k.dbupdate ();
           }


// Verarbeitungsmaterial

           prdk_varb.mdn = mdn;
           strcpy (prdk_varb.rez, rez);

           Prdk_k.sqlin ((short *) &mdn, 1, 0);
           Prdk_k.sqlin ((char *)  rez,  0, 9);
           Prdk_k.sqlin ((short *) &varactive, 1, 0);

           Prdk_k.sqlout ((long *) &prdk_varb.mat, 2, 0);
           Prdk_k.sqlout ((short *) &prdk_varb.kutf, 1, 0);
           Prdk_k.sqlout ((short *) &prdk_varb.id_nr, 1, 0);
           int cursor = Prdk_k.sqlcursor ("select mat,kutf,id_nr from prdk_varb "
                                          "where mdn = ? "
                                          "and rez = ? "
                                          "and variante = ?");
           dsqlstatus = Prdk_k.sqlfetch (cursor);
           while (dsqlstatus == 0)
           {
               prdk_varb.variante = varactive;
               dsqlstatus = Prdk_varb.dbreadfirst ();
               prdk_varb.variante = variante;
               Prdk_varb.dbupdate ();
               dsqlstatus = Prdk_k.sqlfetch (cursor);
           }
           Prdk_k.sqlclose (cursor);

// Zutaten

           prdk_zut.mdn = mdn;
           strcpy (prdk_zut.rez, rez);

           Prdk_k.sqlin ((short *)   &mdn, 1, 0);
           Prdk_k.sqlin ((char *)    rez,  0, 9);
           Prdk_k.sqlin ((short *) &varactive, 1, 0);

           Prdk_k.sqlout ((long *) &prdk_zut.mat, 2, 0);
           Prdk_k.sqlout ((short *) &prdk_zut.kutf, 1, 0);
           cursor = Prdk_k.sqlcursor ("select mat,kutf from prdk_zut "
                                          "where mdn = ? "
                                          "and rez = ? "
                                          "and variante = ?");
           dsqlstatus = Prdk_k.sqlfetch (cursor);
           while (dsqlstatus == 0)
           {
               prdk_zut.variante = varactive;
               dsqlstatus = Prdk_zut.dbreadfirst ();
               prdk_zut.variante = variante;
               Prdk_zut.dbupdate ();
               dsqlstatus = Prdk_k.sqlfetch (cursor);
           }
           Prdk_k.sqlclose (cursor);

// Huellen
           prdk_huel.mdn = mdn;
           prdk_huel.a   = a;

           Prdk_k.sqlin ((short *)   &mdn, 1, 0);
           Prdk_k.sqlin ((double *)  &a,  3, 0);
           Prdk_k.sqlin ((short *) &varactive, 1, 0);

           Prdk_k.sqlout ((long *) &prdk_huel.mat, 2, 0);
           Prdk_k.sqlout ((short *) &prdk_huel.bearb_flg, 1, 0);
           cursor = Prdk_k.sqlcursor ("select mat, bearb_flg from prdk_huel "
                                          "where mdn = ? "
                                          "and a = ? "
                                          "and variante = ?");
           dsqlstatus = Prdk_k.sqlfetch (cursor);
           while (dsqlstatus == 0)
           {
               prdk_huel.variante = varactive;
               dsqlstatus = Prdk_huel.dbreadfirst ();
               prdk_huel.variante = variante;
               Prdk_huel.dbupdate ();
               dsqlstatus = Prdk_k.sqlfetch (cursor);
           }
           Prdk_k.sqlclose (cursor);

// Verpackung
           prdk_vpk.mdn = mdn;
           prdk_vpk.a   = a;

           Prdk_k.sqlin ((short *)   &mdn, 1, 0);
           Prdk_k.sqlin ((double *)  &a,  3, 0);
           Prdk_k.sqlin ((short *) &varactive, 1, 0);

           Prdk_k.sqlout ((double *) &prdk_vpk.a_vpk, 3, 0);
           Prdk_k.sqlout ((short *) &prdk_vpk.bearb_flg, 1, 0);
           cursor = Prdk_k.sqlcursor ("select a_vpk, bearb_flg from prdk_vpk "
                                          "where mdn = ? "
                                          "and a = ? "
                                          "and variante = ?");
           dsqlstatus = Prdk_k.sqlfetch (cursor);
           while (dsqlstatus == 0)
           {
               prdk_vpk.variante = varactive;
               dsqlstatus = Prdk_vpk.dbreadfirst ();
               prdk_vpk.variante = variante;
               Prdk_vpk.dbupdate ();
               dsqlstatus = Prdk_k.sqlfetch (cursor);
           }
           Prdk_k.sqlclose (cursor);
           return 0;
}

double Work::Round (double value, int nk)
{
    Text Frm;
    Text SValue;

    Frm.Format ("%c.%dlf", '%',nk);
    SValue.Format (Frm.GetBuffer (), value);
    value = ratod (SValue.GetBuffer ());
    return value;
}

void Work::SetPreisHerkunft (char * c)
{
	PreisHerk = atoi (c);
}
void Work::SetPlanPreisHerkunft (char * c)
{
	PlanPreisHerk = atoi (c);
}
short Work::GetPreisHerkunft (void)
{
	return PreisHerk ;
}
void Work::Seta_preism (char * c)
{
	LeseA_Preism = atoi (c);
}

int Work::GetPlanPreisHerkunft (void)
{
	return PlanPreisHerk ;
}
int Work::GetRezOhneVarianten (void)
{
	return RezOhneVarianten ;
}
void Work::SetMaterialOhneKosten (char * c)
{
	MaterialOhneKosten = atoi (c);
}
void Work::SetRezPreiseSpeichern (char * c)
{
	RezPreiseSpeichern = atoi (c);
}
void Work::SetRezOhneVarianten (char * c)
{
	RezOhneVarianten = atoi (c);
}
int Work::GetMaterialOhneKosten (void)
{
	return MaterialOhneKosten ;
}

int Work::Geta_preism (void)
{
	return LeseA_Preism ;
}


int Work::PrepareVarb1 (void)
{
           Prdk_k.sqlin ((short *) &prdk_k.mdn, 1, 0);
           Prdk_k.sqlin ((char *)  prdk_varb.rez,  0, 9);
           Prdk_k.sqlin ((short *) &prdk_varb.variante, 1, 0);

           Prdk_k.sqlout ((double *) &Artikel, 3, 0);
           Prdk_k.sqlout ((double *) &prdk_varb.gew, 3, 0);
           Prdk_k.sqlout ((double *) &prdk_varb.gew_bto, 3, 0);
           Prdk_k.sqlout ((short *) &prdk_varb.typ, 1, 0);
           Prdk_k.sqlout ((short *) &quid.quid_nr, 1, 0);
           Prdk_k.sqlout ((double *) &fFE, 3, 0);
           Prdk_k.sqlout ((double *) &fFett, 3, 0);
           Prdk_k.sqlout ((double *) &fBEFFE, 3, 0);
           Prdk_k.sqlout ((double *) &fh2o, 3, 0);
           Prdk_k.sqlout ((char *)  cRez,  0, 9);
           Prdk_k.sqlout ((short *) &dVariante, 1, 0);
           Prdk_k.sqlout ((double *) &fChargengew, 3, 0);
           Prdk_k.sqlout ((double *) &fSchwund, 3, 0);
           Prdk_k.sqlout ((double *) &fVarb_gew, 3, 0);
           Prdk_k.sqlout ((double *) &fVarb_gew_nto, 3, 0);
           varb_cursor1 = Prdk_k.sqlcursor ("select a_mat.a,prdk_varb.gew,prdk_varb.gew_bto,prdk_varb.typ,a_varb.quid_nr,a_varb.fett_eiw, "
			                              "a_varb.fett,a_varb.befe,a_varb.h2o,prdk_k.rez,prdk_k.variante, prdk_k.chg_gew, "
										  "prdk_k.sw, prdk_k.varb_gew, prdk_k.varb_gew_nto "
										   "from prdk_varb,a_mat,outer a_varb, outer prdk_k "
                                          "where prdk_varb.mdn = ? "
                                          "and prdk_varb.rez = ? "
                                          "and prdk_varb.variante = ? "
										  "and prdk_varb.mat = a_mat.mat "
										  "and a_mat.a = a_varb.a "
										  "and prdk_varb.mdn = prdk_k.mdn "
										  "and a_mat.a = prdk_k.a "
										  "and prdk_k.akv = 1");
    return varb_cursor1;
}

int Work::PrepareVarb2 (void)
{
           Prdk_k.sqlin ((short *) &prdk_k.mdn, 1, 0);
           Prdk_k.sqlin ((char *)  prdk_varb.rez,  0, 9);
           Prdk_k.sqlin ((short *) &prdk_varb.variante, 1, 0);

           Prdk_k.sqlout ((double *) &Artikel, 3, 0);
           Prdk_k.sqlout ((double *) &prdk_varb.gew, 3, 0);
           Prdk_k.sqlout ((double *) &prdk_varb.gew_bto, 3, 0);
           Prdk_k.sqlout ((short *) &prdk_varb.typ, 1, 0);
           Prdk_k.sqlout ((short *) &quid.quid_nr, 1, 0);
           Prdk_k.sqlout ((double *) &fFE, 3, 0);
           Prdk_k.sqlout ((double *) &fFett, 3, 0);
           Prdk_k.sqlout ((double *) &fBEFFE, 3, 0);
           Prdk_k.sqlout ((double *) &fh2o, 3, 0);
           Prdk_k.sqlout ((char *)  cRez,  0, 9);
           Prdk_k.sqlout ((short *) &dVariante, 1, 0);
           varb_cursor2 = Prdk_k.sqlcursor ("select a_mat.a,prdk_varb.gew,prdk_varb.gew_bto,prdk_varb.typ,a_varb.quid_nr,a_varb.fett_eiw, "
			                              "a_varb.fett,a_varb.befe,a_varb.h2o,prdk_k.rez,prdk_k.variante "
										   "from prdk_varb,a_mat,outer a_varb, outer prdk_k "
                                          "where prdk_varb.mdn = ? "
                                          "and prdk_varb.rez = ? "
                                          "and prdk_varb.variante = ? "
										  "and prdk_varb.mat = a_mat.mat "
										  "and a_mat.a = a_varb.a "
										  "and prdk_varb.mdn = prdk_k.mdn "
										  "and a_mat.a = prdk_k.a "
										  "and prdk_k.akv = 1");
    return varb_cursor2;
}
int Work::PrepareKopf2 (void)     //240507  Muss extra geholt werden, da nicht eindeutig �ber die Rezeptur
{
           Prdk_k.sqlin ((short *) &prdk_k.mdn, 1, 0);
           Prdk_k.sqlin ((char *)  prdk_varb.rez,  0, 9);
           Prdk_k.sqlin ((short *) &prdk_varb.variante, 1, 0);

           Prdk_k.sqlout ((double *) &fChargengew, 3, 0);
           Prdk_k.sqlout ((double *) &fSchwund, 3, 0);
           Prdk_k.sqlout ((double *) &fVarb_gew, 3, 0);
           Prdk_k.sqlout ((double *) &fVarb_gew_nto, 3, 0);
           kopf_cursor2 = Prdk_k.sqlcursor ("select prdk_k.chg_gew, "
										  "prdk_k.sw, prdk_k.varb_gew, prdk_k.varb_gew_nto "
										   "from  prdk_k "
                                          "where prdk_k.mdn = ? "
                                          "and prdk_k.rez = ? "
                                          "and prdk_k.variante = ? ");
    return kopf_cursor2;
}
int Work::PrepareVarb3 (void)
{
           Prdk_k.sqlin ((short *) &prdk_k.mdn, 1, 0);
           Prdk_k.sqlin ((char *)  prdk_varb.rez,  0, 9);
           Prdk_k.sqlin ((short *) &prdk_varb.variante, 1, 0);

           Prdk_k.sqlout ((double *) &Artikel, 3, 0);
           Prdk_k.sqlout ((double *) &prdk_varb.gew, 3, 0);
           Prdk_k.sqlout ((double *) &prdk_varb.gew_bto, 3, 0);
           Prdk_k.sqlout ((short *) &prdk_varb.typ, 1, 0);
           Prdk_k.sqlout ((short *) &quid.quid_nr, 1, 0);
           Prdk_k.sqlout ((double *) &fFE, 3, 0);
           Prdk_k.sqlout ((double *) &fFett, 3, 0);
           Prdk_k.sqlout ((double *) &fBEFFE, 3, 0);
           Prdk_k.sqlout ((double *) &fh2o, 3, 0);
           Prdk_k.sqlout ((char *)  cRez,  0, 9);
           Prdk_k.sqlout ((short *) &dVariante, 1, 0);
           varb_cursor3 = Prdk_k.sqlcursor ("select a_mat.a,prdk_varb.gew,prdk_varb.gew_bto,prdk_varb.typ,a_varb.quid_nr,a_varb.fett_eiw, "
			                              "a_varb.fett,a_varb.befe,a_varb.h2o,prdk_k.rez,prdk_k.variante "
										   "from prdk_varb,a_mat,outer a_varb, outer prdk_k "
                                          "where prdk_varb.mdn = ? "
                                          "and prdk_varb.rez = ? "
                                          "and prdk_varb.variante = ? "
										  "and prdk_varb.mat = a_mat.mat "
										  "and a_mat.a = a_varb.a "
										  "and prdk_varb.mdn = prdk_k.mdn "
										  "and a_mat.a = prdk_k.a "
										  "and prdk_k.akv = 1");
    return varb_cursor3;
}
int Work::PrepareKopf3 (void)     //240507  Muss extra geholt werden, da nicht eindeutig �ber die Rezeptur
{
           Prdk_k.sqlin ((short *) &prdk_k.mdn, 1, 0);
           Prdk_k.sqlin ((char *)  prdk_varb.rez,  0, 9);
           Prdk_k.sqlin ((short *) &prdk_varb.variante, 1, 0);

           Prdk_k.sqlout ((double *) &fChargengew, 3, 0);
           Prdk_k.sqlout ((double *) &fSchwund, 3, 0);
           Prdk_k.sqlout ((double *) &fVarb_gew, 3, 0);
           Prdk_k.sqlout ((double *) &fVarb_gew_nto, 3, 0);
           kopf_cursor3 = Prdk_k.sqlcursor ("select prdk_k.chg_gew, "
										  "prdk_k.sw, prdk_k.varb_gew, prdk_k.varb_gew_nto "
										   "from  prdk_k "
                                          "where prdk_k.mdn = ? "
                                          "and prdk_k.rez = ? "
                                          "and prdk_k.variante = ? ");
    return kopf_cursor3;
}
int Work::OpenVarbQuid (short di)
{
 switch (di)
 {
        case 1 :
		    if (varb_cursor1 == -1)
			{
				varb_cursor1 = PrepareVarb1 ();
		    }
		    return Prdk_k.sqlopen (varb_cursor1);
			break;
			
        case 2 :
		    if (varb_cursor2 == -1)
			{
				varb_cursor2 = PrepareVarb2 ();
				kopf_cursor2 = PrepareKopf2 ();
		    }
		    return Prdk_k.sqlopen (varb_cursor2);
			break;
        case 3 :
		    if (varb_cursor3 == -1)
			{
				varb_cursor3 = PrepareVarb3 ();
				kopf_cursor3 = PrepareKopf3 ();
		    }
		    return Prdk_k.sqlopen (varb_cursor3);
			break;
 }
 return Prdk_k.sqlopen (varb_cursor);
}

int Work::FetchVarbQuid (short di)
{
 int dsqlstatus;
 switch (di)
 {
        case 1 :
		    if (varb_cursor1 == -1)
			{
				varb_cursor1 = PrepareVarb1 ();
			}
			dsqlstatus = Prdk_k.sqlfetch (varb_cursor1);
			if (dsqlstatus != 0)
			{
				return dsqlstatus;
			}
			break;
        case 2 :
		    if (varb_cursor2 == -1)
			{
				varb_cursor2 = PrepareVarb2 ();
			}
			dsqlstatus = Prdk_k.sqlfetch (varb_cursor2);
			if (dsqlstatus != 0)
			{
				return dsqlstatus;
			}
			break;
        case 3 :
		    if (varb_cursor3 == -1)
			{
				varb_cursor3 = PrepareVarb3 ();
			}
			dsqlstatus = Prdk_k.sqlfetch (varb_cursor3);
			if (dsqlstatus != 0)
			{
				return dsqlstatus;
			}
			break;
 }
  return dsqlstatus;
}

int Work::FetchKopfQuid (short di)
{
 int dsqlstatus;
 switch (di)
 {
        case 1 :
				return dsqlstatus;
			break;
        case 2 :
		    if (kopf_cursor2 == -1)
			{
				kopf_cursor2 = PrepareKopf2 ();
			}
			dsqlstatus = Prdk_k.sqlfetch (kopf_cursor2);
			if (dsqlstatus != 0)
			{
				return dsqlstatus;
			}
			break;
        case 3 :
		    if (kopf_cursor3 == -1)
			{
				kopf_cursor3 = PrepareKopf3 ();
			}
			dsqlstatus = Prdk_k.sqlfetch (kopf_cursor3);
			if (dsqlstatus != 0)
			{
				return dsqlstatus;
			}
			break;
 }
  return dsqlstatus;
}

int Work::CloseVarbQuid (void)
{
    if (varb_cursor1 != -1)
    {
        Prdk_k.sqlclose (varb_cursor1);
        varb_cursor1 = -1;
    }
    if (varb_cursor2 != -1)
    {
        Prdk_k.sqlclose (varb_cursor2);
        varb_cursor2 = -1;
    }
    if (varb_cursor3 != -1)
    {
        Prdk_k.sqlclose (varb_cursor3);
        varb_cursor3 = -1;
    }
    return 0;
}
int Work::CloseKopfQuid (void)
{
    if (kopf_cursor2 != -1)
    {
        Prdk_k.sqlclose (kopf_cursor2);
        kopf_cursor2 = -1;
    }
    if (kopf_cursor3 != -1)
    {
        Prdk_k.sqlclose (kopf_cursor3);
        kopf_cursor3 = -1;
    }
    return 0;
}


void Work::FuelleQuid (char * rez, short variante,short AufrufTiefe, double fchg_gew, double Anteil,double GbrtGew)
{
 //240507 prdk_varb.gew_bto benutzen und nicht prdk_varb.gew 
	int dsql_quid;
	int drez_old = 0;
	double sw_bto_nto;

	sprintf(prdk_kf.warnung1,"");
	sprintf(prdk_kf.warnung2,"");

		   if (AufrufTiefe == 1)
		   {
			   varb_cursor1 = -1;
			   varb_cursor2 = -1;
			   varb_cursor3 = -1;
	           Prdk_k.sqlin ((short *) &prdk_k.mdn, 1, 0);
		       Prdk_k.sqlin ((char *)  prdk_k.rez,  0, 9);
	           Prdk_k.sqlin ((short *) &prdk_k.variante, 1, 0);
			   Prdk_k.sqlcomm ("delete from prdk_quid where mdn = ? and rez = ? and variante = ? ");
		   }
           prdk_varb.mdn = prdk_k.mdn;
           strcpy (prdk_varb.rez, rez);
		   prdk_varb.variante = variante;

		   OpenVarbQuid (AufrufTiefe);
		   FetchKopfQuid (AufrufTiefe);


		   drez_old = 0;
		   while (FetchVarbQuid (AufrufTiefe) == 0)
           {
                fFlKg = prdk_varb.gew_bto * Anteil;
				if (prdk_varb.typ == 2)  //Grundbr�t
				{
					FuelleQuid(cRez,dVariante,AufrufTiefe + 1,fChargengew,(prdk_varb.gew_bto / fchg_gew) * Anteil,fFlKg);
					continue;
				}
//				Anteil = 1;
                fFlKg = prdk_varb.gew_bto * Anteil;
//		        fFLProz = (prdk_varb.gew_bto * 100 / fchg_gew) * Anteil;
			    fFettProz = (prdk_varb.gew_bto * 100 / fchg_gew * fFett) * Anteil;
//			    fFettProz = fFlProz * fFett) * Anteil; //100507
				fFEProz = (prdk_varb.gew_bto * 100 / fchg_gew * fFE) * Anteil;
//				fFEProz = (prdk_varb.gew_bto * fFE) * Anteil;
				fBEFFEProz = (prdk_varb.gew_bto * 100 / fchg_gew * fBEFFE) * Anteil;
//				fBEFFEProz = (prdk_varb.gew_bto  * fBEFFE) * Anteil;
				if (fFett == 0.0) fFettProz = 0.0;
				if (fFE == 0.0) fFEProz = 0.0;
				if (fBEFFE == 0.0) fBEFFEProz = 0.0;

				PruefeAnalyse(Artikel,quid.quid_nr,fFE,fFett,fBEFFE,fh2o);
               if (Quid.dbreadfirst () == 0)
			   {
				    memcpy (&prdk_quid, &prdk_quid_null, sizeof (prdk_quid));
					prdk_quid.mdn = prdk_k.mdn;
					strcpy(prdk_quid.rez,prdk_k.rez);
					prdk_quid.variante = prdk_k.variante;
					strcpy(prdk_quid.quid_kat,quid.quid_kat);
					prdk_quid.quid_nr = quid.quid_nr;

					dsql_quid = Prdk_quid.dbreadfirst ();
					prdk_quid.schwundkg = (ratod(prdk_kf.sw_analy) / 100 * (prdk_k.varb_gew_nto + prdk_k.zut_gew)) + (prdk_k.varb_gew - prdk_k.varb_gew_nto);
					if (AufrufTiefe == 1)
					{
						prdk_quid.flkg = prdk_quid.flkg + fFlKg;
						prdk_quid.flproz = prdk_quid.flkg * 100 / prdk_k.chg_gew; 
						prdk_quid.fettproz = prdk_quid.fettproz + fFettProz;
						prdk_quid.feproz = prdk_quid.feproz + fFEProz;
						prdk_quid.beffeproz = prdk_quid.beffeproz + fBEFFEProz;
					} else if (AufrufTiefe > 1)
					{
						prdk_quid.gflkg = prdk_quid.gflkg + fFlKg;
						prdk_quid.gflproz = prdk_quid.gflkg * 100 / fchg_gew; 
						prdk_quid.gfettproz = prdk_quid.gfettproz + fFettProz;
						prdk_quid.gfeproz = prdk_quid.gfeproz + fFEProz;
						prdk_quid.gbeffeproz = prdk_quid.gbeffeproz + fBEFFEProz;

						if (drez_old != atoi(prdk_varb.rez)) // innerhalb des Grundbr�tes nicht aufsummieren, nur denn mehrere Gbrt in Rez.
						{
							sw_bto_nto = 100 / fChargengew * (fVarb_gew - fVarb_gew_nto);
							sw_bto_nto = sw_bto_nto /100 * GbrtGew;
							prdk_quid.gschwundkg = prdk_quid.gschwundkg + (fSchwund / 100 * (GbrtGew - sw_bto_nto)) + sw_bto_nto;
						}
					}
					Prdk_quid.dbupdate ();

				    memcpy (&prdk_quid, &prdk_quid_null, sizeof (prdk_quid));
					prdk_quid.mdn = prdk_k.mdn;
					strcpy(prdk_quid.rez,prdk_k.rez);
					prdk_quid.variante = prdk_k.variante;
					strcpy(prdk_quid.quid_kat,quid.quid_kat);
					prdk_quid.quid_nr = 0;
					dsql_quid = Prdk_quid.dbreadfirst ();
					prdk_quid.schwundkg = (ratod(prdk_kf.sw_analy) / 100 * (prdk_k.varb_gew_nto + prdk_k.zut_gew)) + (prdk_k.varb_gew - prdk_k.varb_gew_nto);
					if (AufrufTiefe == 1)
					{
						prdk_quid.flkg = prdk_quid.flkg + fFlKg;
						prdk_quid.flproz = prdk_quid.flkg * 100 / prdk_k.chg_gew; 
						prdk_quid.fettproz = prdk_quid.fettproz + fFettProz;
						prdk_quid.feproz = prdk_quid.feproz + fFEProz;
						prdk_quid.beffeproz = prdk_quid.beffeproz + fBEFFEProz;
					} else if (AufrufTiefe > 1)
					{
						prdk_quid.gflkg = prdk_quid.gflkg + fFlKg;
						prdk_quid.gflproz = prdk_quid.gflkg * 100 / fchg_gew; 
						prdk_quid.gfettproz = prdk_quid.gfettproz + fFettProz;
						prdk_quid.gfeproz = prdk_quid.gfeproz + fFEProz;
						prdk_quid.gbeffeproz = prdk_quid.gbeffeproz + fBEFFEProz;
						if (drez_old != atoi(prdk_varb.rez)) // innerhalb des Grundbr�tes nicht aufsummieren, nur denn mehrere Gbrt in Rez.
						{
							sw_bto_nto = 100 / fChargengew * (fVarb_gew - fVarb_gew_nto);
							sw_bto_nto = sw_bto_nto /100 * GbrtGew;
							prdk_quid.gschwundkg = prdk_quid.gschwundkg + (fSchwund / 100 * (GbrtGew - sw_bto_nto)) + sw_bto_nto;
						}
					}
					Prdk_quid.dbupdate ();

                    if (quid.fleisch_kz == 1)    //aufsummieren gesamt nur �ber FLEISCH!
					{
					    memcpy (&prdk_quid, &prdk_quid_null, sizeof (prdk_quid));
						prdk_quid.mdn = prdk_k.mdn;
						strcpy(prdk_quid.rez,prdk_k.rez);
						prdk_quid.variante = prdk_k.variante;
						strcpy(prdk_quid.quid_kat,"000");
						prdk_quid.quid_nr = 0;
						dsql_quid = Prdk_quid.dbreadfirst ();

						prdk_quid.schwundkg = (ratod(prdk_kf.sw_analy) / 100 * (prdk_k.varb_gew_nto + prdk_k.zut_gew)) + (prdk_k.varb_gew - prdk_k.varb_gew_nto);
						if (AufrufTiefe == 1)
						{
							prdk_quid.flkg = prdk_quid.flkg + fFlKg;
							prdk_quid.flproz = prdk_quid.flkg * 100 / prdk_k.chg_gew; 
							prdk_quid.fettproz = prdk_quid.fettproz + fFettProz;
							prdk_quid.feproz = prdk_quid.feproz + fFEProz;
							prdk_quid.beffeproz = prdk_quid.beffeproz + fBEFFEProz;
						} else if (AufrufTiefe > 1)
						{
							prdk_quid.gflkg = prdk_quid.gflkg + fFlKg;
							prdk_quid.gflproz = prdk_quid.gflkg * 100 / fchg_gew; 
							prdk_quid.gfettproz = prdk_quid.gfettproz + fFettProz;
							prdk_quid.gfeproz = prdk_quid.gfeproz + fFEProz;
							prdk_quid.gbeffeproz = prdk_quid.gbeffeproz + fBEFFEProz;
							if (drez_old != atoi(prdk_varb.rez)) // innerhalb des Grundbr�tes nicht aufsummieren, nur denn mehrere Gbrt in Rez.
							{
								sw_bto_nto = 100 / fChargengew * (fVarb_gew - fVarb_gew_nto);
								sw_bto_nto = sw_bto_nto /100 * GbrtGew;
								prdk_quid.gschwundkg = prdk_quid.gschwundkg + (fSchwund / 100 * (GbrtGew - sw_bto_nto)) + sw_bto_nto;
							}
						}
						Prdk_quid.dbupdate ();
					}
			   }
			   drez_old = atoi(prdk_varb.rez); 
           }
		   if (AufrufTiefe == 1 ) 
		   {
			   CloseVarbQuid();
			   CloseKopfQuid();
		   }
}

void Work::FuelleWarnung (char* warnung)
{

	if (strlen(prdk_kf.warnung1) <= 1)
	{
		sprintf(prdk_kf.warnung1, clipped(warnung)); 
	}
	else
	{
		sprintf(prdk_kf.warnung2, clipped(warnung)); 
	}
}

void Work::PruefeAnalyse(double Artikel,int quid_nr,double fFE,double fFett,double fBEFFE,double fh2o)
{
	char warnung [80];
	if (strlen(prdk_kf.warnung1) > 1 && strlen(prdk_kf.warnung2) > 1)  return;
	if (quid_nr == 0)
	{
		sprintf(warnung, "Artikel %13.0lf : Keine Quidnummer hinterlegt!!",Artikel); 
		FuelleWarnung (warnung);
	}
	if (fh2o == 0.0 && fFE == 0.0 && fFett == 0.0 && fBEFFE == 0.0)
	{
		sprintf(warnung, "Artikel %13.0lf : Keine Analysenwerte hinterlegt!!",Artikel); 
		FuelleWarnung (warnung);
	}

}


void Work::RechneQuid (void)
{
	double fFettProzGes = 0.0;
	double fFEProzGes = 0.0;
	double fFlProzGes = 0.0;
	double fBEFFEProzGes = 0.0;
	double fSchwundGes = 0.0;
    char   cKopfQUIDKat [4];
	double fFettProzRel = 0.0;
	double fFleischiSQUID_FL = 0.0;
	double fFleischiSQUID = 0.0;
	double fBEAbsProz = 0.0;
	double fBEGrenz = 0.0;
	double fQUIDFaktor = 1.0;

   cursor_quid = Preparecursor_quid ();

   int lNurSchwein = 1;
   int lNurGefl = 1;
   int lMitSchwein = 0;
   int dsqlstatus = Prdk_k.sqlopen (cursor_quid);
   dsqlstatus = Prdk_k.sqlfetch (cursor_quid);
   while (dsqlstatus == 0)
   {
	   if (prdk_quid.flkg > 0.0)
	   {
//240507			prdk_quid.fettproz = prdk_quid.fettproz / prdk_quid.flkg;
//240507			prdk_quid.feproz = prdk_quid.feproz / prdk_quid.flkg;
//240507			prdk_quid.beffeproz = prdk_quid.beffeproz / prdk_quid.flkg;
			prdk_quid.fettproz = prdk_quid.fettproz / 100;
			prdk_quid.feproz = prdk_quid.feproz / 100;
			prdk_quid.beffeproz = prdk_quid.beffeproz / 100;
	   }
	   else
	   {
			prdk_quid.fettproz = 0.0;
			prdk_quid.feproz = 0.0;
			prdk_quid.beffeproz = 0.0;
	   }
	   if (prdk_quid.gflkg > 0.0)
	   {
			prdk_quid.gfettproz = prdk_quid.gfettproz / 100;
			prdk_quid.gfeproz = prdk_quid.gfeproz / 100;
			prdk_quid.gbeffeproz = prdk_quid.gbeffeproz / 100;
	   }
	   else
	   {
			prdk_quid.gfettproz = 0.0;
			prdk_quid.gfeproz = 0.0;
			prdk_quid.gbeffeproz = 0.0;
	   }
	   if (strcmp(prdk_quid.quid_kat, "000") == 0) 
	   {
/* 250507
           fFettProzGes = (prdk_quid.fettproz * prdk_quid.flkg) + (prdk_quid.gfettproz * prdk_quid.gflkg);
           fFettProzGes = fFettProzGes / prdk_k.chg_gew;

           fFEProzGes = (prdk_quid.feproz * prdk_quid.flkg) + (prdk_quid.gfeproz * prdk_quid.gflkg);
           fFEProzGes = fFEProzGes / prdk_k.chg_gew;
           fFlProzGes = prdk_quid.flproz + prdk_quid.gflproz;
           fBEFFEProzGes = (prdk_quid.beffeproz * prdk_quid.flkg) + (prdk_quid.gbeffeproz * prdk_quid.gflkg);
           fBEFFEProzGes = fBEFFEProzGes / prdk_k.chg_gew;
           fSchwundGes = (prdk_quid.schwundkg + prdk_quid.gschwundkg) * 100 / prdk_k.chg_gew;
*/
           fFettProzGes = (prdk_quid.fettproz * prdk_quid.flproz) + (prdk_quid.gfettproz * prdk_quid.gflproz);
           fFettProzGes = fFettProzGes / 100;

           fFEProzGes = (prdk_quid.feproz * prdk_quid.flproz) + (prdk_quid.gfeproz * prdk_quid.gflproz);
           fFEProzGes = fFEProzGes / 100;
           fFlProzGes = prdk_quid.flproz + prdk_quid.gflproz;
           fBEFFEProzGes = (prdk_quid.beffeproz * prdk_quid.flproz) + (prdk_quid.gbeffeproz * prdk_quid.gflproz);
           fBEFFEProzGes = fBEFFEProzGes / 100;
           fSchwundGes = (prdk_quid.schwundkg + prdk_quid.gschwundkg) * 100 / prdk_k.chg_gew;  
	   }
	   if (strcmp(prdk_quid.quid_kat, "000") != 0 && strcmp(clipped(ptabn.ptwer1),"1") == 0) //nur Fleisch  
	   {
            if (strcmp(clipped(prdk_quid.quid_kat), "S") != 0)
			{
                lNurSchwein = 0;
			}
            if (strcmp(clipped(prdk_quid.quid_kat), "G") != 0)
			{
                lNurGefl = 0;
			}
            if (strcmp(clipped(prdk_quid.quid_kat), "S") == 0)
			{
                lMitSchwein = 1;
			}
	   }
	   dsqlstatus = Prdk_k.sqlfetch (cursor_quid);
   }
   Prdk_k.sqlclose (cursor_quid);
   cursor_quid = -1;



  strcpy(prdk_kf.quid_kat,"   ");
  strcpy(cKopfQUIDKat,"   ");
  strcpy(prdk_kf.be_rel_max,"25 %");
  short fBEProz = 25;
  short fFettProz = 0;
  if (lNurSchwein == 1)
  {
     strcpy (cKopfQUIDKat,"S");
     strcpy (prdk_kf.fett_rel_max,"30 %");
     fFettProz = 30;
  }
  if (lNurGefl == 1)
  {
     strcpy (cKopfQUIDKat,"G");
     fFettProz = 15;
     fBEProz = 10;
     strcpy(prdk_kf.fett_rel_max,"15 %");
     strcpy(prdk_kf.be_rel_max,"10 %");
  }
  if (lNurGefl == 0 && lNurSchwein == 0)
  {
     strcpy (cKopfQUIDKat,"R");
     strcpy(prdk_kf.fett_rel_max,"25 %");
     fFettProz = 25;
  }
  GetPtabLong (prdk_kf.quid_kat, "quid_kategorie", cKopfQUIDKat);


 //============Deklaration Fett =========================================
  if (fFlProzGes != 0.0) fFettProzRel = fFettProzGes * 100 / fFlProzGes;
  if (fFettProzRel <= fFettProz)
  {
     fFleischiSQUID_FL = fFlProzGes;
	 strcpy (prdk_kf.hinweis_fett, "nicht erforderlich");
  }
  else
  {
     if (lMitSchwein == 1)
	 {
		strcpy (prdk_kf.hinweis_fett, "Speck/ ...-Fett");
	 }
	 else
	 {
		strcpy (prdk_kf.hinweis_fett, " ...-Fett");
	 }

     fFleischiSQUID_FL = fFlProzGes * ( 100 - fFettProzRel) / ( 100 - fFettProz);
  }
  sprintf(prdk_kf.fett_rel_ist," %5.2lf %s",fFettProzRel,"%");


  fFleischiSQUID_FL = fFleischiSQUID_FL / ( 100 - fSchwundGes ) * 100;


//=================Berechnungen f�r Hinweis  ===========================================
  if (fFlProzGes != 0.0) fBEAbsProz = (fFEProzGes - fBEFFEProzGes) * 100 / fFlProzGes; 
  if (fFlProzGes != 0.0) fBEGrenz = fBEProz * fBEAbsProz / fFlProzGes;

  if (fBEAbsProz > fBEGrenz)
  {

		double fBEMaxProz = (fFEProzGes * fBEProz) / 100;
		fFleischiSQUID = ( fFlProzGes * (100 - fBEAbsProz) ) / (100 - fBEMaxProz);
		fFleischiSQUID = fFleischiSQUID / ( 100 - fSchwundGes ) * 100;
  }
  else
  {
		fFleischiSQUID = fFleischiSQUID_FL;
  }

  if (fFleischiSQUID > fFleischiSQUID_FL) fFleischiSQUID = fFleischiSQUID_FL;
  strcpy (prdk_kf.hinweis, "");
  strcpy (prdk_kf.hinweis2, "");

  if (fFleischiSQUID >= 100)
  {
	  lese_a_bas (prdk_k.a);
	  strcpy (prdk_kf.hinweis, "Fleischanteil in Gramm/100g ausweisen");
	  if (lNurSchwein == 1)
	  {
		  sprintf(prdk_kf.hinweis2,"z.B. : 100 g %s enthalten %4.0lf g Schweinefleisch",_a_bas.a_bz1,fFleischiSQUID);
	  }
	  if (lNurGefl == 0 && lNurSchwein == 0)
	  {
			sprintf(prdk_kf.hinweis2,"z.B. : 100 g %s enthalten %4.0lf g Schweine- und Rindfleischfleisch",_a_bas.a_bz1,fFleischiSQUID);
	  }
  }


//=================Deklaration BE ===========================================
  if (fBEAbsProz <= fBEProz)
  {
	 strcpy (prdk_kf.hinweis_be, "nicht erforderlich");
  }
  else 
  {
	 strcpy (prdk_kf.hinweis_be, "Bindegewebe");
  }
  sprintf(prdk_kf.be_rel_ist," %5.2lf %s",fBEAbsProz,"%");

  //fFlProzGesSchwund = fFlProzGes / ( 100 - fSchwundGes ) * 100;

  if (fFlProzGes > 0.0) fQUIDFaktor = fFleischiSQUID / fFlProzGes;

  cursor_quid = Preparecursor_quid ();
  dsqlstatus = Prdk_k.sqlopen (cursor_quid);
  dsqlstatus = Prdk_k.sqlfetch (cursor_quid);
  while (dsqlstatus == 0)
  {
	   if (prdk_quid.flkg > 0.0)
	   {
			prdk_quid.fettproz = prdk_quid.fettproz / prdk_quid.flkg;
			prdk_quid.feproz = prdk_quid.feproz / prdk_quid.flkg;
			prdk_quid.beffeproz = prdk_quid.beffeproz / prdk_quid.flkg;
	   }
	   else
	   {
			prdk_quid.fettproz = 0.0;
			prdk_quid.feproz = 0.0;
			prdk_quid.beffeproz = 0.0;
	   }
//	   if (strcmp(prdk_quid.quid_kat, "000") != 0 && strcmp(clipped(ptabn.ptwer1),"1") == 0) //nur Fleisch  
	   if (strcmp(prdk_quid.quid_kat, "000") == 0 || strcmp(clipped(ptabn.ptwer1),"1") == 0) //nur Fleisch  
	   {
   		   prdk_quid.flproz = prdk_quid.flproz * fQUIDFaktor;
   		   prdk_quid.gflproz = prdk_quid.gflproz * fQUIDFaktor;
	   }
	   else
	   {
	        prdk_quid.flproz = prdk_quid.flproz / ( 100 - fSchwundGes ) * 100;
	        prdk_quid.gflproz = prdk_quid.gflproz / ( 100 - fSchwundGes ) * 100;
	   }
       prdk_quid.flproz = Round(prdk_quid.flproz,1);
       prdk_quid.gflproz = Round(prdk_quid.gflproz,1);
	   PrdkQuid.dbupdate ();

	   dsqlstatus = Prdk_k.sqlfetch (cursor_quid);
  }


}

int Work::Preparecursor_quid (void)
{
    //ins_quest ((char *)   &prdk_k.mdn, 1, 0);
//    ins_quest ((char *)   rez,  0, 9);
/*
    ins_quest ((char *)   &variante,  1, 0);

    out_quest ((char *) &prdk_quid.mdn,1,0);
            cursor_quid = Prdk_k.sqlcursor ("select prdk_quid.mdn  "
                          "from prdk_quid "

                                  "where  variante = ? ");

  */

    ins_quest ((char *)   &prdk_k.mdn, 1, 0);
    ins_quest ((char *)   prdk_k.rez,  0, 9);
    ins_quest ((char *)   &prdk_k.variante, 1, 0);

    out_quest ((char *) &prdk_quid.mdn,1,0);
    out_quest ((char *) prdk_quid.rez,0,9);
    out_quest ((char *) &prdk_quid.variante,1,0);
    out_quest ((char *) prdk_quid.quid_kat,0,4);
    out_quest ((char *) &prdk_quid.quid_nr,1,0);
    out_quest ((char *) &prdk_quid.flkg,3,0);
    out_quest ((char *) &prdk_quid.gflkg,3,0);
    out_quest ((char *) &prdk_quid.flproz,3,0);
    out_quest ((char *) &prdk_quid.gflproz,3,0);
    out_quest ((char *) &prdk_quid.fettproz,3,0);
    out_quest ((char *) &prdk_quid.gfettproz,3,0);
    out_quest ((char *) &prdk_quid.feproz,3,0);
    out_quest ((char *) &prdk_quid.gfeproz,3,0);
    out_quest ((char *) &prdk_quid.beffeproz,3,0);
    out_quest ((char *) &prdk_quid.gbeffeproz,3,0);
    out_quest ((char *) &prdk_quid.schwundkg,3,0);
    out_quest ((char *) &prdk_quid.gschwundkg,3,0);
    out_quest ((char *) ptabn.ptwer1,0,9);
            cursor_quid = Prdk_k.sqlcursor ("select prdk_quid.mdn,  "
"prdk_quid.rez,  prdk_quid.variante,  prdk_quid.quid_kat,  "
"prdk_quid.quid_nr,  prdk_quid.flkg,  prdk_quid.gflkg,  "
"prdk_quid.flproz,  prdk_quid.gflproz,  prdk_quid.fettproz,  "
"prdk_quid.gfettproz,  prdk_quid.feproz,  prdk_quid.gfeproz,  "
"prdk_quid.beffeproz,  prdk_quid.gbeffeproz,  prdk_quid.schwundkg,  "
"prdk_quid.gschwundkg, ptabn.ptwer1 from prdk_quid, outer ptabn "

                                  "where mdn = ? "
                                  "and   rez = ? "
                                  "and   variante = ? "
								  "and prdk_quid.quid_kat = ptabn.ptwert "
								  "and ptabn.ptitem = \"quid_kategorie\" ");
    return cursor_quid;
}


void Work::CalcSumVarbBatch (void)
{
       double summegew = 0.0;
       double summegew_nto = 0.0;
	   double pr_ek = 0.0;
	   double inh_prod = 1.0;
	   double a_gew = 1.0;
	   char me_einh [10];
	   double Kosten = 0.0;
	   double artikel = 0.0;
		double ekds = 0.0;
		double kostds = 0.0;
	    double summegew_FF = 0.0;
		double summegew_EL = 0.0;
		double summegew_GE = 0.0;
		double summegew_KZ = 0.0;
		double summegew_EL1 = 0.0;
		double summegew_EL2 = 0.0;

         OpenVarb ();
         int dsqlstatus = FetchVarb ();
         while (dsqlstatus == 0)
         {
			switch (prdk_varb.typ)
			{
				case 1:
				case ABTYP:
					artikel = GetArtikel (me_einh, &a_gew, prdk_varb.mat );
					break;
				case REZTYP:
				case REWORKTYP:
					artikel = GetArtikel (me_einh, &a_gew, prdk_varb.mat );
					inh_prod = 1.0;
					break;
			}
	        double posgew = prdk_varb.varb_me * a_gew;
			double posme = prdk_varb.varb_me;
			double posme_nto = prdk_varb.gew;
			summegew += Round (posgew, 3);
			summegew_nto += Round (posgew, 3);
            pr_ek = GetMatEk (artikel, prdk_varb.typ,inh_prod,GetPreisHerkunft(),GetPlanPreisHerkunft());
			ekds += pr_ek * Round (posme, 3); 
	        double kosteneinh = GetKostenEinh ();
		    double kostenkg   = GetKostenKg ();
            Kosten = kostenkg;
            short basis_kz = 1;
			if (strcmp(prdk_varb.zug_bas,"GE") == 0) basis_kz = 1;
			if (strcmp(prdk_varb.zug_bas,"FF") == 0) basis_kz = 2;
			if (strcmp(prdk_varb.zug_bas,"EL") == 0) basis_kz = 3;
			if (strcmp(prdk_varb.zug_bas,"KZ") == 0) basis_kz = 4;
			if (strcmp(prdk_varb.zug_bas,"E1") == 0) basis_kz = 5;
			if (strcmp(prdk_varb.zug_bas,"E2") == 0) basis_kz = 6;
			switch (basis_kz) 
			{
			case 1:  //GE Gesamt
				summegew_GE += prdk_varb.gew * chgfaktor * a_gew;
				break;
			case 2:  // FF Fett und Fleisch
				summegew_FF += prdk_varb.gew * chgfaktor * a_gew;
				break;
			case 3: // EL Einlage
				summegew_EL += prdk_varb.gew * chgfaktor * a_gew;
				break;
			case 4: // KZ keine Zugabe
				summegew_KZ += prdk_varb.gew * chgfaktor * a_gew;
				break;
			case 5: // behandelt
				summegew_EL1 += prdk_varb.gew * chgfaktor * a_gew;
				break;
			case 6: // EL Einlage
				summegew_EL2 += prdk_varb.gew * chgfaktor * a_gew;
				break;
			}

/* bgut-Zeugs : muss raus
			if (atoi (me_einh) != 2)
			{
                 if (kosteneinh > 0.0)
                 {
					 Kosten = kosteneinh;
                 }
                 else if (kostenkg > 0.0)
                 {
                       Kosten =  kostenkg * a_gew;
                 }
			}
			else
			{
                 if (kostenkg > 0.0)
                 {
                       Kosten = kostenkg;
                 }
                 else if (kosteneinh > 0.0)
                 {
                       Kosten =  kosteneinh / a_gew;
                 }
			}
			*/
			   
            dsqlstatus = FetchVarb ();
	 	    kostds += Kosten * Round (posme, 3);
        }
		if (summegew != 0.0)
	    {
		    ekds /= (summegew  + GetZutGew () + GetHuelGew ());
			kostds /= (summegew  + GetZutGew () + GetHuelGew ());
		}
		SetSummen (summegew, ekds, kostds, VARB);
		SetSummen (summegew_nto, ekds, kostds, 999);
		prdk_k.summegew_GE = summegew_GE;
		prdk_k.summegew_FF = summegew_FF;
		prdk_k.summegew_EL = summegew_EL;
		prdk_k.summegew_KZ = summegew_KZ;
		prdk_k.summegew_EL1 = summegew_EL1;
		prdk_k.summegew_EL2 = summegew_EL2;
         CloseVarb ();

}

void Work::SetSummen (double summegew, double ekds, double kosten, int type)
{

        if (type == 999)
        {
				prdk_k.varb_gew_nto = summegew;
		}
        if (type == VARB)
        {
                 prdk_k.varb_hk_vollk  = ekds + kosten;
                 prdk_k.varb_hk_teilk  = ekds + kosten;
                 prdk_k.varb_mat_o_b   = ekds;
                 prdk_k.varb_gew       = summegew;
        }
        else if (type == ZUT)
        {
                 prdk_k.zut_hk_vollk = ekds + kosten;;
                 prdk_k.zut_hk_teilk = ekds + kosten;;
                 prdk_k.zut_mat_o_b  = ekds;
                 prdk_k.zut_gew      = summegew;
        }
        else if (type == HUEL)
        {
                 prdk_k.huel_hk_vollk = ekds + kosten;;
                 prdk_k.huel_hk_teilk = ekds + kosten;;
                 prdk_k.huel_mat_o_b  = ekds;
                 prdk_k.huel_gew      = summegew;

                 prdk_k.huel_wrt      = GetHuelWrt ();
                 prdk_k.huel_hk_wrt   = GetHuelHkWrt ();
        }
        else if (type == VPK)
        {
                 prdk_k.vpk_hk_vollk = ekds + kosten;
                 prdk_k.vpk_hk_teilk = ekds + kosten;
                 prdk_k.vpk_mat_o_b  = ekds;
                 prdk_k.vpk_gew      = summegew;
                 prdk_k.vpk_wrt      = GetVpkWrt ();
                 prdk_k.vpk_hk_wrt   = GetVpkHkWrt ();
        }

        double chg_gew = prdk_k.chg_gew;
        prdk_k.chg_gew = prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew;
		prdk_k.kutter_gew = prdk_k.chg_gew;

        if (type != VARB)
        {
			(prdk_k.chg_gew == 0) ? prdk_k.varb_mat_o_b = 0l : prdk_k.varb_mat_o_b *= chg_gew / prdk_k.chg_gew; 
            (prdk_k.chg_gew == 0) ? prdk_k.varb_hk_vollk = 0l : prdk_k.varb_hk_vollk *= chg_gew / prdk_k.chg_gew; 
            (prdk_k.chg_gew == 0) ? prdk_k.varb_hk_teilk = 0l : prdk_k.varb_hk_teilk *= chg_gew / prdk_k.chg_gew; 
        }
        if (type != ZUT)
        {
			(prdk_k.chg_gew == 0) ? prdk_k.zut_mat_o_b = 0l : prdk_k.zut_mat_o_b *= chg_gew / prdk_k.chg_gew; 
            (prdk_k.chg_gew == 0) ? prdk_k.zut_hk_vollk = 0l : prdk_k.zut_hk_vollk *= chg_gew / prdk_k.chg_gew; 
            (prdk_k.chg_gew == 0) ? prdk_k.zut_hk_teilk = 0l : prdk_k.zut_hk_teilk *= chg_gew / prdk_k.chg_gew; 
        }
		

        prdk_k.rez_hk_vollk = 0.0; 
        prdk_k.rez_hk_teilk = 0.0;
        prdk_k.rez_mat_o_b  = 0.0;
        prdk_k.bto_hk_vollk = 0.0;
        prdk_k.bto_hk_teilk = 0.0;
        prdk_k.bto_mat_o_b  = 0.0;
        prdk_k.nto_hk_vollk = 0.0;
        prdk_k.nto_hk_teilk = 0.0;
        prdk_k.nto_mat_o_b  = 0.0;
        prdk_k.nvpk_hk_vollk  = 0.0;
        prdk_k.nvpk_hk_teilk  = 0.0;
        prdk_k.nvpk_mat_o_b   = 0.0;

        if ((prdk_k.varb_gew + prdk_k.zut_gew) != 0.0)
        {
             prdk_k.rez_hk_vollk = (prdk_k.varb_hk_vollk * prdk_k.varb_gew +
                                    prdk_k.zut_hk_vollk * prdk_k.zut_gew) /
                                    (prdk_k.varb_gew + prdk_k.zut_gew);
             prdk_k.rez_hk_teilk = (prdk_k.varb_hk_vollk * prdk_k.varb_gew +
                                    prdk_k.zut_hk_vollk * prdk_k.zut_gew) /
                                    (prdk_k.varb_gew + prdk_k.zut_gew);

//             prdk_k.rez_mat_o_b   = (prdk_k.varb_mat_o_b  * prdk_k.varb_gew +
//                                     prdk_k.zut_mat_o_b  * prdk_k.zut_gew) /
//                                    (prdk_k.varb_gew + prdk_k.zut_gew);
        }

        if ((prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew) != 0.0)
        {
              prdk_k.bto_hk_vollk = (prdk_k.varb_hk_vollk * prdk_k.varb_gew +
                                     prdk_k.zut_hk_vollk * prdk_k.zut_gew +
                                     prdk_k.huel_hk_wrt) /
                                     (prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew);

              prdk_k.bto_hk_teilk = (prdk_k.varb_hk_vollk * prdk_k.varb_gew +
                                     prdk_k.zut_hk_vollk * prdk_k.zut_gew +
                                     prdk_k.huel_hk_wrt) /
                                     (prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew);

              prdk_k.bto_mat_o_b = (prdk_k.varb_mat_o_b * prdk_k.varb_gew +
                                    prdk_k.zut_mat_o_b * prdk_k.zut_gew +
                                    prdk_k.huel_wrt) /
                                   (prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew);
        }

        if (((prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew) * (1 - prdk_k.sw_kalk / 100)) 
              != 0.0)
        {
             prdk_k.nto_hk_vollk = (prdk_k.varb_hk_vollk * prdk_k.varb_gew +
                                    prdk_k.zut_hk_vollk * prdk_k.zut_gew +
                                    prdk_k.huel_hk_wrt) /
                                   ((prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew) *
                                   (1 - prdk_k.sw_kalk / 100));

             prdk_k.nto_hk_teilk = (prdk_k.varb_hk_vollk * prdk_k.varb_gew +
                                    prdk_k.zut_hk_vollk * prdk_k.zut_gew +
                                    prdk_k.huel_hk_wrt) /
                                    ((prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew) *
                                    (1 - prdk_k.sw_kalk / 100));

              prdk_k.nto_mat_o_b = (prdk_k.varb_mat_o_b * prdk_k.varb_gew +
                                   prdk_k.zut_mat_o_b * prdk_k.zut_gew +
                                   prdk_k.huel_wrt) /
                                   ((prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew) *
                                   (1 - prdk_k.sw_kalk / 100));
        }

        if (((prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew) * 
             (1 - prdk_k.sw_kalk / 100) + prdk_k.vpk_gew) 
              != 0.0)
        {
              prdk_k.nvpk_hk_vollk  = (prdk_k.varb_hk_vollk * prdk_k.varb_gew +
                                    prdk_k.zut_hk_vollk * prdk_k.zut_gew +
                                    prdk_k.huel_hk_wrt +
                                    prdk_k.vpk_hk_wrt)/
                                    ((prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew) *
                                     (1 - prdk_k.sw_kalk / 100) + prdk_k.vpk_gew);
              prdk_k.nvpk_hk_teilk  = (prdk_k.varb_hk_vollk * prdk_k.varb_gew +
                                       prdk_k.zut_hk_vollk * prdk_k.zut_gew +
                                       prdk_k.huel_hk_wrt +
                                       prdk_k.vpk_hk_wrt)/
                                       ((prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew) *
                                        (1 - prdk_k.sw_kalk / 100) + prdk_k.vpk_gew);
              prdk_k.nvpk_mat_o_b   = (prdk_k.varb_mat_o_b * prdk_k.varb_gew +
                                       prdk_k.zut_mat_o_b * prdk_k.zut_gew +
                                       prdk_k.huel_wrt +
                                       prdk_k.vpk_wrt)/
                                       ((prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew) *
                                       (1 - prdk_k.sw_kalk / 100) + prdk_k.vpk_gew);
        }
}


