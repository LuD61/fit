#include <windows.h>
#include <stdio.h>
#include "SpezDlg.h"
#include "mo_arg.h"
#include "strfkt.h"
#include "mo_meld.h"
#include "colbut.h"
#include "mo_progcfg.h"
#include "cmask.h"
#include "mo_vers.h"
#include "help.h"
#include "stdfkt.h"
#include "mo_curso.h"
#include "mo_menu.h"
#include "sys_par.h"

/*
Letzte �nderung : LuD	//#141205  Plan-EK editierbar, zur�ckspeichern in a_preis
Letzte �nderung : LuD	//#150308 1 :
		C 1. die Verpackungen werden bei einer �nderung der Chargengr��e nicht wie
		bei den H�llen automatisch �ber das F�llgewicht neu berechnet.
Letzte �nderung : LuD #110603   Rezepturdruck Plan EK richtig abfragen 
Letzte �nderung : LuD #270503   Rezepturdruck 
Letzte �nderung : LuD #130503   Lsts.a wurde bei Grundbr�ten nicht best�ckt    


*/

VINFO Vinfo;
static DLG *BaseDlg;
static HWND BaseWindow;
SpezDlg *Dlg;
Work *work;
HWND hMainWindow;
HINSTANCE hMainInst;
static BOOL ToolButton = FALSE;
static char *Programm = "23201";

static char *Version[] = {"  23201 : Rezepturen Bearbeiten ",
                          "  Standard    ", 
						  "  Version vom 22.06.2009  ",
                          "  letzte �nderung : Zugabemenge �ber Basis",
						   NULL,
};


struct PMENUE dateimen[] = {
	                        "&1 Bearbeiten", "C",  NULL, IDM_WORK, 
//                            "&2 Anzeigen",   " ",  NULL, IDM_SHOW,
						    "&3 L�schen <STRG-DEL>", "G",  NULL, IDM_DELETE,
						    "&4 Einf�gen <INS>",   "G",  NULL, IDM_INSERT,
						    "&5 Drucken",    " ",  NULL, IDM_PRINT,
							"",              "S",  NULL, 0, 
						    "&6 Rezptur l�schen",    "G",  NULL, IDM_DELALL,
							"",              "S",  NULL, 0, 
	                        "B&eenden",      " ",  NULL, IDM_EXIT,

                             NULL, NULL, NULL, 0};

struct PMENUE bearbmen[] = {
	                        "Abbruch F5",    " ",   NULL, VK_F5, 
                            "speichern F12", " ",  NULL, VK_F12,
                            "ansehen F9",    " ",  NULL, IDM_CHOISE,
                            "Word F11",      "G",  NULL, VK_F11,
                            "kopieren",      " ",  NULL, IDM_COPY,
                            "einf�gen",      " ",  NULL, IDM_INS,
                             NULL, NULL, NULL, 0};


struct PMENUE menuetab[] = {"&Tabelle",      "M", dateimen,   0, 
                            "&Bearbeiten",   "M", bearbmen,   0, 
                            "&?",          " ", NULL,       IDM_VINFO, 
						     NULL, NULL, NULL, 0};

HWND hwndTB;
static TBBUTTON tbb[] =
{
 0,               IDM_WORK,   TBSTATE_ENABLED | TBSTATE_CHECKED, 
                              TBSTYLE_CHECKGROUP, 
 0, 0, 0, 0,
/*
 1,               IDM_SHOW,   TBSTATE_ENABLED, 
	                          TBSTYLE_CHECKGROUP,
 0, 0, 0, 0,
*/
 2,               IDM_DELALL,   TBSTATE_INDETERMINATE, 
                                TBSTYLE_BUTTON,
 0, 0, 0, 0,
 3,               IDM_PRINT,  TBSTATE_ENABLED, 
                              TBSTYLE_BUTTON,
 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED,
                  TBSTYLE_SEP, 
 0, 0, 0, 0,
 6,               IDM_INFO,   TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
 
 8,               KEYSTAB, TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
 7,               KEYTAB, TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
 9,               KEYDOWN, TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
10 ,              KEYUP, TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,

 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,

 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
};


static char *qInfo [] = {"Bearbeiten",
                         "Anzeigen",
                         "Rezeptur l�schen",
                         "Drucken",
                         "Funktion f�r aktives Eingabefeld",
                         "S�tze in Listenform anzeigen",
                          0, 
                          0};

static UINT qIdfrom [] = {IDM_WORK, IDM_SHOW, IDM_DELALL,
                         IDM_PRINT, IDM_INFO,
                         VK_F5, 0, 0};

static HWND hWndF5;
static HWND hWndF9;
static HWND hWndF12;
static HWND hWndDel;
static char *qhWndInfo [] = {"Abbrechen", 
                             "Speichern",
                             "Zeile l�schen",
                             "ansehen",
                             NULL};

static HWND *qhWndFrom [] = {
                             &hWndF5,
                             &hWndF12,
                             &hWndDel,
                             &hWndF9,
                             NULL,
}; 


HWND mamain1;
static int Size = 120;
static char *Caption = "Rezepturen"; 


static mfont buttonfont = {"MS SANS SERIF", Size, 0, 0,
                           RGB (0, 255, 255),
                           0,
                           NULL};

static mfont buttontxtfont = {"Arial", 100, 0, 0,
                               RGB (0, 255, 255),
                               0,
                               NULL};

static ColButton Cf5   = { "",    0, 0, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, -1, -1,
                             NULL,  0, 0,
                             BLACKCOL,
                             LTGRAYCOL,
//                             BLUECOL,
//                             NOCOLPRESS | NOCOLBORDER,
                             0, 
};

static ColButton Cf12   = { "",    0, 0, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, -1, -1,
                             NULL,  0, 0,
                             BLACKCOL,
                             LTGRAYCOL,
//                             BLUECOL,
//                             NOCOLPRESS | NOCOLBORDER,
                             0,
};

static ColButton CDel   = { "",    0, 0, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, -1, -1,
                             NULL,  0, 0,
                             BLACKCOL,
                             LTGRAYCOL,
//                             BLUECOL,
//                             NOCOLPRESS | NOCOLBORDER,
                             0,
};

static ColButton CInsert   = { "",    0, 0, 
                               NULL,  0, 0,
                               NULL,  0, 0,
                               NULL, -1, -1,
                               NULL,  0, 0,
                               BLACKCOL,
                               LTGRAYCOL,
//                             BLUECOL,
//                             NOCOLPRESS | NOCOLBORDER,
                             0,
};


static ColButton Cf9   = { "",    0, 0, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, -1, -1,
                             NULL,  0, 0,
                             BLACKCOL,
                             LTGRAYCOL,
//                             BLUECOL,
//                             NOCOLPRESS | NOCOLBORDER,
                             0,
};


static ColButton CTexte   = {"Word",   -1, -1, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, 100, -1,
                             NULL,  0, 0,
                             BLUECOL,
                             LTGRAYCOL,
//                             BLUECOL,
//                             NOCOLPRESS | NOCOLBORDER,
                             0,
};
static ColButton CAnteil   = {"% Anteil",   -1, -1, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, 100, -1,
                             NULL,  0, 0,
                             BLUECOL,
                             LTGRAYCOL,
                             0,
};

static ColButton CMeEinh   = {"Verkn�pfung",   -1, -1, 
                               NULL,  0, 0,
                               NULL,  0, 0,
                               NULL, 100, -1,
                               NULL,  0, 0,
                               BLUECOL,
                               LTGRAYCOL,
//                             BLUECOL,
//                             NOCOLPRESS | NOCOLBORDER,
                             0,
};

static ColButton Cprior   = { "",    0, 0, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, -1, -1,
                             NULL,  0, 0,
                             BLACKCOL,
                             LTGRAYCOL,
                             NOCOLPRESS | NOCOLBORDER,
};

static ColButton Cnext   = { "",    0, 0, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, -1, -1,
                             NULL,  0, 0,
                             BLACKCOL,
                             LTGRAYCOL,
                             NOCOLPRESS | NOCOLBORDER,
};

static ColButton Cfirst   = { "",    0, 0, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, -1, -1,
                             NULL,  0, 0,
                             BLACKCOL,
                             LTGRAYCOL,
                             NOCOLPRESS | NOCOLBORDER,
};

static ColButton Clast   = { "",    0, 0, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, -1, -1,
                             NULL,  0, 0,
                             BLACKCOL,
                             LTGRAYCOL,
                             NOCOLPRESS | NOCOLBORDER,
};

static ColButton *CubTab [] = {&Cf5, &Cf12, &Cf9, &CDel, &CInsert, &CTexte,&CAnteil, &CMeEinh, NULL};

/*
static CFIELD *_fButtons[] = {
                     new CFIELD ("f5", (ColButton *) &Cf5,  3, 0, 1, 1,  NULL, "", 
                                 CCOLBUTTON,
                                 VK_F5, &buttonfont, 0, TRANSPARENT),
                     new CFIELD ("f12", (ColButton *) &Cf12,  3, 0, 4, 1,  NULL, "", 
                                 CCOLBUTTON,
                                 VK_F12, &buttonfont, 0, TRANSPARENT),
                     NULL,
                     };
*/

static CFIELD *_fButtons1[] = {
                     new CFIELD ("f5", (ColButton *) &Cf5,  3, 0, 0, 1,  NULL, "", 
                                 CCOLBUTTON,
                                 VK_F5, &buttonfont, 0, TRANSPARENT),
                     new CFIELD ("f12", (ColButton *) &Cf12,  3, 0, 3, 1,  NULL, "", 
                                 CCOLBUTTON,
                                 VK_F12, &buttonfont, 0, TRANSPARENT),

                     new CFIELD ("del", (ColButton *) &CDel,  3, 0, 6, 1,  NULL, "", 
                                 CCOLBUTTON,
                                 IDM_DELETE, &buttonfont, 0, TRANSPARENT),

                     new CFIELD ("f9", (ColButton *) &Cf9,  3, 0,9, 1,  NULL, "", 
                                 CCOLBUTTON,
                                 IDM_CHOISE, &buttonfont, 0, TRANSPARENT),
                     NULL,
};

static CFIELD *_fButtons2[] = {
                     new CFIELD ("f5", (ColButton *) &Cf5,  3, 0, 0, 1,  NULL, "", 
                                 CCOLBUTTON,
                                 VK_F5, &buttonfont, 0, TRANSPARENT),

                     new CFIELD ("f12", (ColButton *) &Cf12,  3, 0, 3, 1,  NULL, "", 
                                 CCOLBUTTON,
                                 VK_F12, &buttonfont, 0, TRANSPARENT),
		             new CFIELD ("Line1", "",
				                     0, 1, 6, 1, NULL, "", CBORDER,
  								     500, &buttonfont, 0, 0),
                     new CFIELD ("del", (ColButton *) &CDel,  2, 0, 7, 1,  NULL, "", 
                                 CCOLBUTTON,
                                 IDM_DELETE, &buttonfont, 0, TRANSPARENT),

                     new CFIELD ("insert", (ColButton *) &CInsert,  2, 0,9, 1,  NULL, "", 
                                 CCOLBUTTON,
                                 IDM_INSERT, &buttonfont, 0, TRANSPARENT),

		             new CFIELD ("Line2", "",
				                     0, 1,12, 1, NULL, "", CBORDER,
  								     500, &buttonfont, 0, 0),

                     new CFIELD ("f9", (ColButton *) &Cf9,  2, 0,13, 1,  NULL, "", 
                                 CCOLBUTTON,
                                 IDM_CHOISE, &buttonfont, 0, TRANSPARENT),
		             new CFIELD ("Line3", "",
				                     0, 1, 16, 1, NULL, "", CBORDER,
  								     500, &buttonfont, 0, 0),

                     new CFIELD ("Texte", (ColButton *) &CTexte, 10, 0, 17, 1,  NULL, "", 
                                 CCOLBUTTON,
                                 VK_F11, &buttonfont, 0, TRANSPARENT),

		             new CFIELD ("Line5", "",
				                     0, 1, 25, 1, NULL, "", CBORDER,
  								     500, &buttonfont, 0, 0),
                     new CFIELD ("Anteil", (ColButton *) &CAnteil, 10, 0, 27, 1,  NULL, "", 
                                 CCOLBUTTON,
                                 VK_F10, &buttonfont, 0, TRANSPARENT),
		             new CFIELD ("Line6", "",
				                     0, 1, 35, 1, NULL, "", CBORDER,
  								     500, &buttonfont, 0, 0),
/*
                     new CFIELD ("meeinh", (ColButton *) &CMeEinh, 11, 0, 30, 1,  NULL, "", 
                                 CCOLBUTTON,
                                 VK_F8, &buttonfont, 0, TRANSPARENT),
		             new CFIELD ("Line6", "",
				                     0, 1, 38, 1, NULL, "", CBORDER,
  								     500, &buttonfont, 0, 0),
*/
		             new CFIELD ("Line4", "",
				                    150, 1, 0, 2, NULL, "", CBORDER,
  								     500, &buttonfont, 0, 0),
                     NULL,
};

static CFORM fButtons (10, _fButtons1);

static int DlgY = 4;
static BOOL PrintSerial = FALSE;
HANDLE PrintPid = 0;
static BOOL anzeigen = 0;
//char *PrintCommand = "70001 13100";
//char *PrintCommand = "rezepturdruck";
static MENUE_CLASS Menue;
static int StartSize = 0;

char SumAnzeige[50];


static short mdn = 0;
static short fil = 0;
static long kun = 0l;

void tst_arg (char *arg)
{
/*
          for (; *arg; arg += 1)
          {
              switch (*arg)
              {
              }
          }
*/
          return;
}

void SetCubBk (COLORREF BkColor)
{
	  int i;

	  for (i = 0; CubTab[i] != NULL; i ++)
	  {
		  CubTab [i]->BkColor = BkColor;
	  }
}




void UnCheckAll (HMENU hMenu)
{

     EnableMenuItem (hMenu,      IDM_WORK,   MF_ENABLED);
	 CheckMenuItem  (hMenu,      IDM_WORK,   MF_UNCHECKED);

     EnableMenuItem (hMenu,      IDM_SHOW,   MF_ENABLED);
	 CheckMenuItem  (hMenu,      IDM_SHOW,   MF_UNCHECKED);

     EnableMenuItem (hMenu,      IDM_DELETE,   MF_ENABLED);
	 CheckMenuItem  (hMenu,      IDM_DELETE, MF_UNCHECKED);

     ToolBar_SetState (Dlg->GethwndTB (),IDM_WORK,   TBSTATE_INDETERMINATE);
     ToolBar_SetState (Dlg->GethwndTB (),IDM_SHOW,   TBSTATE_INDETERMINATE);
     ToolBar_SetState (Dlg->GethwndTB (),IDM_DELETE, TBSTATE_INDETERMINATE);

     ToolBar_SetState (Dlg->GethwndTB (), IDM_WORK,   TBSTATE_ENABLED);
     ToolBar_SetState (Dlg->GethwndTB (), IDM_SHOW,   TBSTATE_ENABLED);
     ToolBar_SetState (Dlg->GethwndTB (), IDM_DELETE, TBSTATE_ENABLED);
}

void WorkRights (HMENU hMenu)
{
     EnableMenuItem (hMenu,      IDM_SHOW,   MF_ENABLED);
     EnableMenuItem (hMenu, IDM_SHOW, MF_GRAYED);
     ToolBar_SetState(Dlg->GethwndTB (),IDM_SHOW, TBSTATE_INDETERMINATE);
     EnableMenuItem (hMenu, IDM_DEL, MF_GRAYED);
     ToolBar_SetState(Dlg->GethwndTB (),IDM_DEL, TBSTATE_INDETERMINATE);

     EnableMenuItem (hMenu,      IDM_WORK,   MF_ENABLED);
     CheckMenuItem  (BaseDlg->GethMenu (),   IDM_WORK, MF_CHECKED);
     ToolBar_SetState(Dlg->GethwndTB (), IDM_WORK, TBSTATE_ENABLED | TBSTATE_CHECKED);
}

void ShowRights (HMENU hMenu)
{
	 CheckMenuItem  (hMenu,      IDM_WORK,   MF_UNCHECKED);
     EnableMenuItem (hMenu, IDM_WORK, MF_GRAYED);
     ToolBar_SetState(Dlg->GethwndTB (),IDM_WORK, TBSTATE_INDETERMINATE);
     EnableMenuItem (hMenu, IDM_DEL, MF_GRAYED);
     ToolBar_SetState(Dlg->GethwndTB (),IDM_DEL, TBSTATE_INDETERMINATE);

     EnableMenuItem (hMenu,      IDM_SHOW,   MF_ENABLED);
     CheckMenuItem  (BaseDlg->GethMenu (),   IDM_SHOW, MF_CHECKED);
     ToolBar_SetState(Dlg->GethwndTB (), IDM_SHOW, TBSTATE_ENABLED | TBSTATE_CHECKED);
}

int InfoVersion (void)
/**
Waehlen ausfuehren.
**/
{
	    Vinfo.VInfoF (hMainInst, hMainWindow, Version);
        return 0;
}


BOOL TestMenue (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (LOWORD (wParam) == IDM_WORK && Dlg != NULL)
        {
            UnCheckAll (BaseDlg->GethMenu ());
            ToolBar_SetState(Dlg->GethwndTB (), IDM_WORK, TBSTATE_ENABLED | TBSTATE_CHECKED);
            EnableMenuItem (BaseDlg->GethMenu (),   IDM_WORK, MF_ENABLED);
	        CheckMenuItem  (BaseDlg->GethMenu (),   IDM_WORK, MF_CHECKED);
            return TRUE;
        }
        else if (LOWORD (wParam) == IDM_SHOW && Dlg != NULL)
        {
            UnCheckAll (BaseDlg->GethMenu ());
            ToolBar_SetState(Dlg->GethwndTB (), IDM_SHOW, TBSTATE_ENABLED | TBSTATE_CHECKED);
            EnableMenuItem (BaseDlg->GethMenu (),   IDM_SHOW, MF_ENABLED);
	        CheckMenuItem  (BaseDlg->GethMenu (),   IDM_SHOW, MF_CHECKED);
            return TRUE;
        }
        else if (LOWORD (wParam) == IDM_DELETE && Dlg != NULL)
        {
/*
            UnCheckAll (BaseDlg->GethMenu ());
            ToolBar_SetState(Dlg->GethwndTB (), IDM_DELETE, TBSTATE_ENABLED | TBSTATE_CHECKED);
            EnableMenuItem (BaseDlg->GethMenu (),   IDM_DELETE, MF_ENABLED);
	        CheckMenuItem  (BaseDlg->GethMenu (),   IDM_DELETE, MF_CHECKED);
*/
//            return Dlg->OnRowDelete ();            
            return TRUE;
        }
        else if (LOWORD (wParam) == IDM_DELALL && Dlg != NULL)
        {
            return Dlg->DeleteRow ();            
        }
        else if (LOWORD (wParam) == IDM_PRINT && Dlg != NULL)
        {
//aus der Callbackfunktion raus      PrintPartner ();
            return FALSE;
        }
        else if (LOWORD (wParam) == IDM_COPY && Dlg != NULL)
        {
//            Dlg->ToClipboard ();
            Dlg->Kopieren ();
            return TRUE;
        }
        else if (LOWORD (wParam) == IDM_INS && Dlg != NULL)
        {
            Dlg->FromClipboard ();
            return TRUE;
        }
        else if (LOWORD (wParam) == IDM_VINFO)
        {
            InfoVersion ();
            return TRUE;
        }
        else if (LOWORD (wParam) == IDM_INFO)
        {
            Dlg->CallInfo ();
//            Dlg->Help ();
            return TRUE;
        }
        else if (LOWORD (wParam) == IDM_EXIT)
        {
            if (abfragejn (Dlg->GethWnd (), "Verarbeitung abbrechen ?", "N") == 0)
            {
                Dlg->SetCurrentFocus ();
                return TRUE;
            }
            ExitProcess (0);
        }
        return FALSE;
}


static PROG_CFG ProgCfg ("23201");
static COLORREF SysBackground = LTGRAYCOL;
static COLORREF Background = LTGRAYCOL;
static COLORREF HelpBackground = DKYELLOWCOL;
static int BorderType = RAISEDBORDER;
static char Bitmap[256] = {"NULL"};
static int Bitmapmode = 1;
static BOOL DockMenue = FALSE;


void GetCfgColor (COLORREF *color, char *cfg_v)
/**
Farbunterlegung fuer Listen uebertragen.
**/
{
	static char *ColTxt[] = {"BLACKCOL",
		                     "WHITECOL",
							 "BLUECOL",
							 "REDCOL",
							 "LTGRAYCOL",
							 "GREENCOL",
							 "YELLOWCOL",
							 "DKYELLOWCOL",
							 NULL};
	
	static COLORREF ColVal[] = {BLACKCOL,
		                        WHITECOL,
					  		    BLUECOL,
							    REDCOL,
							    LTGRAYCOL,
							    GREENCOL,
							    YELLOWCOL,
								DKYELLOWCOL};
	int i;
	int red, blue, green;
	char *ColR;
	int Cols = 0;

	for (i = 0; ColTxt[i]; i ++)
	{
		if (strcmp (ColTxt[i], cfg_v) == 0)
		{
			*color = ColVal[i];
			return;
		}
	}


	ColR = strstr (cfg_v, "red");
	if (ColR)
	{
		red = atoi (&ColR[3]);
		Cols ++;
	}
	ColR = strstr (cfg_v, "blue");
	if (ColR)
	{
		blue = atoi (&ColR[4]);
		Cols ++;
	}
	ColR = strstr (cfg_v, "green");
	if (ColR)
	{
		green = atoi (&ColR[5]);
		Cols ++;
	}
	if (Cols == 3)
	{
		*color = RGB (red, green, blue);
	}

}

void GetSysPar (void)
{
       SYS_PAR_CLASS SysPar;

       strcpy (sys_par.sys_par_nam, "RezPreiseSpeichern");
       if (SysPar.dbreadfirst () == 0)
       {
		   Work::SetRezPreiseSpeichern (sys_par.sys_par_wrt);
                     Work::SetMaterialOhneKosten (sys_par.sys_par_wrt);
       }
       strcpy (sys_par.sys_par_nam, "RezOhneVarianten");
       if (SysPar.dbreadfirst () == 0)
       {
		   Work::SetRezOhneVarianten (sys_par.sys_par_wrt);
       }
}

void GetCfgValues (void)
/**
Werte aus artpfleg.cfg holen.
**/
{
	   static BOOL cfgOK = FALSE;
       char cfg_v [256];

	   if (cfgOK) return;

	   cfgOK = TRUE; 

       if (ProgCfg.GetCfgValue ("ToolButton", cfg_v) == TRUE)
       {
                    ToolButton = atoi (cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("BorderType", cfg_v) == TRUE)
       {
                    BorderType = atoi (cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("Background", cfg_v) == TRUE)
       {
		             GetCfgColor (&Background, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("Bitmap", cfg_v) == TRUE)
       {
		             strcpy (Bitmap, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("Bitmapmode", cfg_v) == TRUE)
       {
		             Bitmapmode = atoi (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("DockMenue", cfg_v) == TRUE)
       {
		             DockMenue = atoi (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("HelpBackground", cfg_v) == TRUE)
       {
		             GetCfgColor (&HelpBackground, cfg_v);
                     HELP::SetBackground (HelpBackground);
       }
       if (ProgCfg.GetCfgValue ("PrintSerial", cfg_v) == TRUE)
       {
		             PrintSerial = atoi (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("batchArtikelauswahl", cfg_v) == TRUE)
       {
//                     SpezDlg::Artikelauswahl = new char [512];
		               SpezDlg::Artikelauswahl = atoi (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("PrintCommand", cfg_v) == TRUE)
       {
                     SpezDlg::PrintCommand = new char [512];
		             strcpy (SpezDlg::PrintCommand, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("LockPage", cfg_v) == TRUE)
       {
                     SpezDlg::LockPage = atoi (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("KostStyle", cfg_v) == TRUE)
       {
                     SpezDlg::KostStyle = atoi (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("StartSize", cfg_v) == TRUE)
       {
                    StartSize = atoi (cfg_v);
	   }
       if (ProgCfg.GetGroupDefault ("WordPath", cfg_v) == TRUE)
       {
                     strcpy (SpezDlg::WordPath, cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("WordPath", cfg_v) == TRUE)
       {
                     strcpy (SpezDlg::WordPath, cfg_v);
	   }
       if (ProgCfg.GetGroupDefault ("DocPath", cfg_v) == TRUE)
       {
                     strcpy (SpezDlg::DocPath, cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("DocPath", cfg_v) == TRUE)
       {
                     strcpy (SpezDlg::DocPath, cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("HiKostRows", cfg_v) == TRUE)
       {
                     SpezDlg::SetHiKostRows (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("mitTabZutaten", cfg_v) == TRUE)
       {
                     SpezDlg::SetTabZutaten (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("mitTabQuid", cfg_v) == TRUE)
       {
                     SpezDlg::SetTabQuid (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("mitTabVerpackung", cfg_v) == TRUE)
       {
                     SpezDlg::SetTabVerpackung (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("RestartProzess", cfg_v) == TRUE)
       {
                     SpezDlg::SetRestartProzess (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("mitTabArbeitszeit", cfg_v) == TRUE)
       {
                     SpezDlg::SetTabArbeitszeit (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("Lese_a_preism", cfg_v) == TRUE)
       {
                     Work::Seta_preism (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("mitMaschinen", cfg_v) == TRUE)
       {
                     SpezDlg::SetMaschinen (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("mitRework", cfg_v) == TRUE)
       {
                     SpezDlg::SetRework (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("PreisHerkunft", cfg_v) == TRUE)
       {
                     SpezDlg::SetPreisHerkunft (cfg_v);
                     Work::SetPreisHerkunft (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("PlanPreisHerkunft", cfg_v) == TRUE)
       {
		             SpezDlg::SetPlanPreisHerkunft (cfg_v);
                     Work::SetPlanPreisHerkunft (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("HiKostColor", cfg_v) == TRUE)
       {
		             COLORREF HiColor;
		             GetCfgColor (&HiColor, cfg_v);
                     SpezDlg::SetHiKostColor (HiColor);
       }
       if (ProgCfg.GetCfgValue ("MaterialOhneKosten", cfg_v) == TRUE)
       {
                     Work::SetMaterialOhneKosten (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("SummenAnzeige", cfg_v) == TRUE)
       {
                     SpezDlg::SetSummenAnzeige (cfg_v);
					 if (atoi(cfg_v) == 0) strcpy (SumAnzeige,"(Preise bezogen auf Chargengr��e)") ;
					 if (atoi(cfg_v) == 1) strcpy (SumAnzeige,"(Preise bezogen auf Endgewicht)") ;
					 if (atoi(cfg_v) == 2) strcpy (SumAnzeige,"") ;
       }
       if (ProgCfg.GetCfgValue ("RestartProcess", cfg_v) == TRUE)
       {
                     SpezDlg::SetRestartProcess (cfg_v);
       }
}


void SaveRect (void)
/**
Aktuelle Windowgroesse sichern.
**/
{
	    char *etc;
		char rectname [512];
		RECT rect;
		FILE *fp;

		etc = getenv ("BWSETC");
		if (etc == NULL) return;

		sprintf (rectname, "%s\\%s.rct", etc,Programm);
		GetWindowRect (hMainWindow, &rect);

		fp = fopen (rectname, "w");
		fprintf (fp, "left    %d\n", rect.left);
		fprintf (fp, "top     %d\n", rect.top);
		fprintf (fp, "right   %d\n", rect.right - rect.left);
		fprintf (fp, "bottom  %d\n", rect.bottom - rect.top);
		fclose (fp);
}


void MoveRect (void)
/**
Fenster auf gesicherte Daten anpassen.
**/
{
	    char *etc;
		char buffer [512];
		RECT rect;
		FILE *fp;
		int x, y, cx, cy;
		int anz;

		etc = getenv ("BWSETC");
		if (etc == NULL) return;

		GetWindowRect (hMainWindow, &rect);
		x = rect.left;
		y = rect.top;
		cx = rect.right;
		cy = rect.bottom;
		sprintf (buffer, "%s\\%s.rct", etc,Programm);
		fp = fopen (buffer, "r");
		if (fp == NULL) return;
		while (fgets (buffer, 511, fp))
		{
			cr_weg (buffer);
			anz = wsplit (buffer, " ");
			if (anz < 2) continue;
			if (strcmp (wort[0], "left") == 0)
			{
				x = atoi (wort[1]);
			}
			else if (strcmp (wort[0], "top") == 0)
			{
				y = atoi (wort[1]);
			}
			else if (strcmp (wort[0], "right") == 0)
			{
				cx = atoi (wort[1]);
			}
			else if (strcmp (wort[0], "bottom") == 0)
			{
				cy = atoi (wort[1]);
			}
		}
		fclose (fp);
		MoveWindow (hMainWindow, x, y, cx, cy, TRUE);
}

void GetDockParams (HWND hWnd)
{
       char *etc;
       char buffer [512];
       FILE *fp;
       int anz;
  	   RECT rect;

       if (DockMenue == FALSE) return;
       if (StartSize != 0) 
       {
           return;
       }


       etc = getenv ("BWSETC");
       if (etc == NULL) return;

       sprintf (buffer, "%s\\fit.rct", etc); 
                
       fp = fopen (buffer, "r");
       if (fp == NULL) return;
       if (fgets (buffer, 511,fp) == 0)
       {
           fclose (fp);
           return;
       }
       if (fgets (buffer, 511,fp) == 0)
       {
           fclose (fp);
           return;
       }
       fclose (fp);
       anz = wsplit (buffer, " ");
       if (anz < 4) return;
       rect.left   = atoi (wort[0]);
       rect.top    = atoi (wort[1]);
       rect.right  = atoi (wort[2]);
       rect.bottom = atoi (wort[3]);
  	   rect.left ++; 
	   rect.top ++; 
	   rect.right  = rect.right  - rect.left - 2;
	   rect.bottom = rect.bottom - rect.top - 2;
       MoveWindow (hMainWindow, rect.left, rect.top, rect.right, rect.bottom, TRUE);
}

/*
int DbError (void)
{
       extern int dbstatus;

       print_mess (2, "Datenbankfehler %d", dbstatus);
       ExitProcess (1);
       return -1;
}
*/


void SetToolbar2 (void)
{
        int cx, cy;
        int y;
        int spacey;
        HFONT hFont;
        TEXTMETRIC tm;
        HDC hdc;
        RECT rect;
        RECT mrect;
        RECT frect;
        int i;

        GetClientRect (hMainWindow, &mrect);
        fButtons.GetRect (&frect);
        for (i = 0; i < fButtons.GetFieldanz (); i ++)
        {
            fButtons.GetCfield () [i]->SetY (fButtons.GetCfield () [i]->GetYorg () - 1);
        }
        fButtons.GetRect (&cx, &cy);
        hdc = GetDC (hMainWindow);
        hFont = SetDeviceFont (hdc, &buttonfont, &tm);
        ReleaseDC (hMainWindow, hdc);
        DeleteObject (hFont);
        spacey = DlgY * (tm.tmHeight  + CFIELD::fspaceh);
        frect.top = 0;
        frect.bottom = spacey;
        frect.right = mrect.right;
        if (hwndTB != NULL)
        {
            GetClientRect (hwndTB, &rect);
            spacey -= rect.bottom;
            frect.top += rect.bottom;
        }

        y = max (0, (spacey - cy) / 2);
        if (hwndTB != NULL)
        {
            y += rect.bottom;
        }
        fButtons.destroy ();
        fButtons.SetAbsPos (0, y);
        InvalidateRect (hMainWindow, &frect, TRUE);
        UpdateWindow (hMainWindow);
        fButtons.display ();
}


int    PASCAL WinMain(HANDLE hInstance,HANDLE hPrevInstance,
                        LPSTR lpszCmdLine,int nCmdShow)
{
       char **varargs;
	   int i, anz;
       double scrfcx = 1.0; 
       double scrfcy = 1.0;
       int xfull, yfull;
//       extern void on_dberr (int (*) (void));

//031211 weiter unten       opendbase ("bws");
	   LoadLibrary ("RICHED32.DLL");
	   Background = GetSysColor (COLOR_3DFACE);
	   SysBackground = GetSysColor (COLOR_3DFACE);
	   SpezDlg::SysBkColor = SysBackground;
	   SetCubBk (SysBackground);
//       Menue.SetSysBen (lpszCmdLine);
// 	   sys_ben.zahl = 99;  //TESTTESTTESTTEST
// 	   sys_ben.berecht = 2;  //TESTTESTTESTTEST
	   
       GetCfgValues ();
//       GetSysPar ();
       if (ToolButton)
       { 
                   Cf5.aktivate  = 0;
                   Cf12.aktivate = 0;
                   fButtons.SetFieldanz (4);
                   fButtons.SetCfield (_fButtons1);
       }
       else
       {
                   Cf5.aktivate      = ACTCOLPRESS | NOCOLBORDER;
                   Cf9.aktivate      = ACTCOLPRESS | NOCOLBORDER;
                   Cf12.aktivate     = ACTCOLPRESS | NOCOLBORDER;
                   CDel.aktivate     = ACTCOLPRESS | NOCOLBORDER;
                   CInsert.aktivate  = ACTCOLPRESS | NOCOLBORDER;
                   CTexte.aktivate   = ACTCOLPRESS | NOCOLBORDER;
                   CAnteil.aktivate  = ACTCOLPRESS | NOCOLBORDER;
                   CMeEinh.aktivate  = ACTCOLPRESS | NOCOLBORDER;
                   fButtons.SetCfield (_fButtons2);
                   fButtons.SetFieldanz ();

                   fButtons.GetCfield ("Texte")->SetFont (&buttontxtfont);
                   fButtons.GetCfield ("Anteil")->SetFont (&buttontxtfont);
                   fButtons.GetCfield ("Anteil")->Enable (FALSE);
//                   fButtons.GetCfield ("Texte")->Enable (FALSE);
                   fButtons.GetCfield ("insert")->Enable (FALSE);
                   fButtons.GetCfield ("del")->Enable (FALSE);
/*
                   fButtons.GetCfield ("quik")->SetFont (&buttontxtfont);
                   fButtons.GetCfield ("meeinh")->SetFont (&buttontxtfont);
                   fButtons.GetCfield ("meeinh")->Enable (FALSE);
*/
                   fButtons.GetCfield ("Texte")->Enable (FALSE);
       }

       xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
       yfull = GetSystemMetrics (SM_CYFULLSCREEN);        

       if (xfull > 800)
       {
            scrfcx = (double) xfull / 800; 
            scrfcy = (double) yfull / 562; 
       }

       if (xfull > 800)
       {
            scrfcx = (double) xfull / 800; 
            scrfcy = (double) yfull / 562; 
       }


       buttonfont.FontHeight = Size;

       if (ToolButton == FALSE)
       {

            fButtons.GetCfield ("Line1")->SetBorder (GRAYCOL, WHITECOL, RAISEDVLINE); 
            fButtons.GetCfield ("Line2")->SetBorder (GRAYCOL, WHITECOL, RAISEDVLINE); 
            fButtons.GetCfield ("Line3")->SetBorder (GRAYCOL, WHITECOL, RAISEDVLINE); 
            fButtons.GetCfield ("Line4")->SetBorder (GRAYCOL, WHITECOL, RAISEDHLINE); 
       }

       anz = wsplit (lpszCmdLine, " ");
	   // 011211 A
	   short Anfang = 0;
	   char Datenbank [16] = "bws";
	   if (strcmp(wort[0],"Datenbank") == 0)
	   {
		   strcpy(Datenbank,wort[1]);
		   Anfang = 2;
		   strcpy (lpszCmdLine,"");
	   }
       opendbase (Datenbank);
       GetSysPar ();
	   // 011211 E


       if (anz)
       {
	      varargs = new char * [anz]; 
             for (i = Anfang; i < anz; i ++)
             {
				   varargs[i -Anfang] = new char [strlen (wort[i]) + 1]; 
                   strcpy (varargs[i -Anfang], wort[i]);
				   if (Anfang > 0) 
				   {
					   strcat (lpszCmdLine,wort[i]);
					   strcat (lpszCmdLine," ");
				   }
             }
			 anz -= Anfang;
            argtst (&anz, varargs, tst_arg);
	   }

       if (anz > 0)
       {
           mdn = atoi (varargs [0]);
		   sys_ben.mdn = mdn;
       }
       if (anz > 1)
       {
           fil = atoi (varargs [1]);
       }

       if (anz == 1)
	   {
	       Menue.SetSysBen (lpszCmdLine);
	   }

       if (anz > 2)
       {
           sys_ben.zahl = atol (varargs [2]);
           sys_ben.berecht = atoi (varargs [2]);
       }
       if (anz > 3)
       {
           sys_ben.fil = atoi (varargs [3]);
       }
       if (anz > 3)
       {
// 250609          sys_ben.fil = atoi (varargs [3]);
           prdk_k.a = atoi (varargs [3]);
       }
       if (anz > 4)
       {
           prdk_k.a = ratod (varargs [3]);
		   strcpy (prdk_k.rez,varargs [4]); 
       }
       if (anz > 5)
       {
           prdk_k.variante = atoi (varargs [5]);
       }

       hMainInst = hInstance;

       Cf5.bmp     = BMAP::LoadBitmap (hInstance, "F5", "F5MASK", SysBackground);
       Cf9.bmp     = BMAP::LoadBitmap (hInstance, "ARRDOWNB", "ARRDOWNBMASK", SysBackground);
       Cf12.bmp    = LoadBitmap (hInstance, "F12");
       CDel.bmp    = BMAP::LoadBitmap (hInstance,     "DELIA", "DELMASK",    SysBackground);
       CInsert.bmp = BMAP::LoadBitmap (hInstance, "INSERTIA", "INSERTMASK", SysBackground);
       SpezDlg::SelBmp = BMAP::LoadBitmap (hInstance, "SEL",   "MSK",     SysBackground);
       ITEM::SetHelpName ("23201.cmd");
       if (xfull > 800)
       {
//               BaseDlg = new DLG (-1, -1, 80, 24, Caption, Size, FALSE);
               BaseDlg = new DLG (-1, -1, 800, 600, Caption, Size, TRUE);
       }
       else if (xfull > 700)
       {
              BaseDlg = new DLG (-1, -1, 700, 560, Caption, Size, TRUE);
       }
       else
       {
              BaseDlg = new DLG (-1, -1, 640, 450, Caption, Size, TRUE);
       }
       BaseDlg->ScreenParam (scrfcx, scrfcy);
       BaseDlg->SetMenue (menuetab, TestMenue);
       BaseDlg->SetStyle (WS_VISIBLE | WS_POPUP | 
                      WS_THICKFRAME | WS_CAPTION | WS_SYSMENU
				      | WS_MINIMIZEBOX | WS_MAXIMIZEBOX);

//       BaseDlg->SetStyleEx (WS_EX_CLIENTEDGE);

       BaseDlg->SetWinBackground (SysBackground);
    
       BaseDlg->SetDialog (&fButtons);
       DLG::CloseProg = SaveRect;
       hMainWindow = BaseDlg->OpenWindow (hInstance, NULL);
//DDDDDDDDD       SetWindowText(hMainWindow,"dudu");

//       LockWindowUpdate (hMainWindow);
       mamain1 = hMainWindow;
       if (xfull > 1000)
       {
              GetDockParams (hMainWindow);
       }
     
	   if (StartSize == 1)
	   {
	               ShowWindow (hMainWindow, SW_SHOWMAXIMIZED);
	   }

	   else if (StartSize == 2)
	   {
	               MoveRect ();
	   }

       DLG::hInstance = hMainInst;

       Dlg = new SpezDlg (1, DlgY, -1, -1, "Rezepturen bearbeiten", 105, FALSE,
                             BorderType, ENTERHEAD);

       { 
               Dlg->OpenMess (hMainWindow);
//               Dlg->DiffListDimension (0, -2);
               Dlg->PrintMess ("Ready");
       } 

       Dlg->SetToolbar2 (&fButtons); 

       Dlg->ScreenParam (scrfcx, scrfcy);
       Dlg->SetMenue (NULL, TestMenue);
       Dlg->SethMenu (BaseDlg->GethMenu ());

//       Dlg->SetStyle (WS_VISIBLE | WS_CHILD | WS_DLGFRAME);
       Dlg->SetStyle (WS_VISIBLE | WS_CHILD | WS_DLGFRAME | TCS_MULTILINE);
//       Dlg->SetStyle (WS_VISIBLE | WS_CHILD | TCS_MULTILINE);
       Dlg->SetStyleEx (WS_EX_CLIENTEDGE);
       if (Background != NULL)
       {
              Dlg->SetWinBackground (Background);
       }
       if (strcmp (Bitmap, "NULL"))
       {
             Dlg->ReadBmp (hMainWindow, Bitmap, 0, 0);
             Dlg->SetBitmapmode (Bitmapmode);
       }
       Dlg->SettbMain (hMainWindow);


       Dlg->SetToolbar (tbb, 53,qInfo, qIdfrom, qhWndInfo, qhWndFrom);

	   if (sys_ben.zahl == 99)
	   {
			BaseWindow = Dlg->OpenScrollWindow (hInstance, hMainWindow);
//       Dlg->SetTrackAllways (TRUE);
	   }
	   else if (sys_ben.berecht > 1 && (sys_ben.berecht < 4)) 
		{
           BaseWindow = Dlg->OpenScrollTabWindow (hInstance, hMainWindow, 
                    "Kopfdaten;Verarbeitungsmaterial;Zutaten;H�llen;Verpackung");
			   Dlg->SetTabStack(1,2,3,4,0,0,0);
		}
		else
		{
		  if (Dlg->GetTabQuid() == FALSE)
		  {
           if (Dlg->GetTabZutaten() == FALSE && Dlg->GetTabVerpackung() == FALSE)
           { 
	           BaseWindow = Dlg->OpenScrollTabWindow (hInstance, hMainWindow, 
                    "Kopfdaten;Verarbeitungsmaterial;H�llen;Arbeitszeitwerte;"
					"Kosten");
			   Dlg->SetTabStack(1,3,5,6,0,0,0);
           }
           else if (Dlg->GetTabZutaten() == TRUE && Dlg->GetTabVerpackung() == FALSE)
           { 
	           BaseWindow = Dlg->OpenScrollTabWindow (hInstance, hMainWindow, 
                    "Kopfdaten;Verarbeitungsmaterial;Zutaten;H�llen;Arbeitszeitwerte;"
					"Kosten");
			   Dlg->SetTabStack(1,2,3,5,6,0,0);
           }
           else if (Dlg->GetTabZutaten() == FALSE && Dlg->GetTabVerpackung() == TRUE)
           { 
	           BaseWindow = Dlg->OpenScrollTabWindow (hInstance, hMainWindow, 
                    "Kopfdaten;Verarbeitungsmaterial;H�llen;Verpackung;Arbeitszeitwerte;"
					"Kosten");
			   Dlg->SetTabStack(1,3,4,5,6,0,0);
           }
           else if (Dlg->GetTabZutaten() == TRUE && Dlg->GetTabVerpackung() == TRUE && Dlg->GetTabArbeitszeit() == FALSE)
           { 
	           BaseWindow = Dlg->OpenScrollTabWindow (hInstance, hMainWindow, 
                    "Kopfdaten;Verarbeitungsmaterial;Zutaten;H�llen;Verpackung;"
					"Kosten");
			   Dlg->SetTabStack(1,2,3,4,6,0,0);
           }
           else 
           {
				BaseWindow = Dlg->OpenScrollTabWindow (hInstance, hMainWindow, 
                    "Kopfdaten;Verarbeitungsmaterial;Zutaten;H�llen;Verpackung;Arbeitszeitwerte;"
					"Kosten");
			   Dlg->SetTabStack(1,2,3,4,5,6,0);
           }
		  }
		  else //GetTabQuid
		  {
           if (Dlg->GetTabZutaten() == FALSE && Dlg->GetTabVerpackung() == FALSE)
           { 
	           BaseWindow = Dlg->OpenScrollTabWindow (hInstance, hMainWindow, 
                    "Kopfdaten;Verarbeitungsmaterial;H�llen;Arbeitszeitwerte;"
					"Kosten;Quid-Info");
			   Dlg->SetTabStack(1,3,5,6,7,0,0);
           }
           else if (Dlg->GetTabZutaten() == TRUE && Dlg->GetTabVerpackung() == FALSE)
           { 
	           BaseWindow = Dlg->OpenScrollTabWindow (hInstance, hMainWindow, 
                    "Kopfdaten;Verarbeitungsmaterial;Zutaten;H�llen;Arbeitszeitwerte;"
					"Kosten;Quid-Info");
			   Dlg->SetTabStack(1,2,3,5,6,7,0);
           }
           else if (Dlg->GetTabZutaten() == FALSE && Dlg->GetTabVerpackung() == TRUE)
           { 
	           BaseWindow = Dlg->OpenScrollTabWindow (hInstance, hMainWindow, 
                    "Kopfdaten;Verarbeitungsmaterial;H�llen;Verpackung;Arbeitszeitwerte;"
					"Kosten;Quid-Info");
			   Dlg->SetTabStack(1,3,4,5,6,7,0);
           }
           else if (Dlg->GetTabZutaten() == TRUE && Dlg->GetTabVerpackung() == TRUE && Dlg->GetTabArbeitszeit() == FALSE)
           { 
	           BaseWindow = Dlg->OpenScrollTabWindow (hInstance, hMainWindow, 
                    "Kopfdaten;Verarbeitungsmaterial;Zutaten;H�llen;Verpackung;"
					"Kosten;Quid-Info");
			   Dlg->SetTabStack(1,2,3,4,6,7,0);
           }
           else 
           {
				BaseWindow = Dlg->OpenScrollTabWindow (hInstance, hMainWindow, 
                    "Kopfdaten;Verarbeitungsmaterial;Zutaten;H�llen;Verpackung;Arbeitszeitwerte;"
					"Kosten;Quid-Info");
			   Dlg->SetTabStack(1,2,3,4,5,6,7);
           }
		  }

		}
	   

       hwndTB = Dlg->GethwndTB ();
       SetToolbar2 ();

       hWndF5  = fButtons.GetCfield ("f5")->GethWnd ();
       hWndF12 = fButtons.GetCfield ("f12")->GethWnd ();
       hWndDel = fButtons.GetCfield ("del")->GethWnd ();
       hWndF9  = fButtons.GetCfield ("f9")->GethWnd ();
       fButtons.GetCfield ("f5")->CreateQuikInfos (hMainWindow);
       fButtons.GetCfield ("f12")->CreateQuikInfos (hMainWindow);
       fButtons.GetCfield ("insert")->CreateQuikInfos (hMainWindow);
       fButtons.GetCfield ("del")->CreateQuikInfos (hMainWindow);
       fButtons.GetCfield ("f9")->CreateQuikInfos (hMainWindow);

       ToolBar_SetState (Dlg->GethwndTB (),IDM_DELETE, TBSTATE_INDETERMINATE);

       LockWindowUpdate (NULL);
/*
       if (kun != 0l)
       {
            Dlg->SetKun (mdn, fil, kun);
       }
*/
	   if (sys_ben.fil == 1 && anz == 4)
       {
		   Dlg->AListe ();
//		   Dlg->Batchlauf();
           work->CommitWork ();
		   SendMessage (hMainWindow, WM_COMMAND, VK_F5,
			                                    (LPARAM) hMainWindow); 
       } 
	   else if (anz == 4)
	   {
		   sys_ben.fil = 1;
		   Dlg->AListe ();
           work->CommitWork ();
		   SendMessage (hMainWindow, WM_COMMAND, VK_F5,
			                                    (LPARAM) hMainWindow); 
       } 
       
//	   SendMessage (hMainWindow, WM_COMMAND, MAKELONG (VK_F5, VK_RETURN),
//			                                    (LPARAM) hMainWindow); 
	   Dlg->ProcessMessages ();
       delete Dlg;
       DestroyWindow (hMainWindow);
       delete BaseDlg;
	   for (i = 0; i < anz; i ++)
	   {
		   delete varargs[i];
	   }
	   delete varargs;
       return 0;
}
       
