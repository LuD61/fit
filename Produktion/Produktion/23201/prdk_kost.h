#ifndef _PRDK_KOST_DEF
#define _PRDK_KOST_DEF

#include "windows.h"
#include "mo_draw.h"
#include "wmask.h"
#include "dbclass.h"

struct PRDK_KOST {
   short     mdn;
   double    a;
   short     prdk_stufe;
   double    kost_me;
   double    mat_o_b;
   double    hk_teilk;
   double    hk_vollk;
   short     kost_art;
   double    kosten;
   short     variante;
};
extern struct PRDK_KOST prdk_kost, prdk_kost_null;

#line 10 "prdk_kost.rh"

class PRDK_KOST_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               PRDK_KOST_CLASS () : DB_CLASS ()
               {
               }
};
#endif
