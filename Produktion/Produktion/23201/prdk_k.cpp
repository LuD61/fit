#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "wmaskc.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "prdk_k.h"

struct PRDK_K prdk_k, prdk_k_null;

void PRDK_K_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *)   &prdk_k.mdn, 1, 0);
            ins_quest ((char *)   &prdk_k.a,   3, 0);
            ins_quest ((char *)   &prdk_k.variante,  1, 0);
    out_quest ((char *) &prdk_k.mdn,1,0);
    out_quest ((char *) &prdk_k.a,3,0);
    out_quest ((char *) prdk_k.rez,0,9);
    out_quest ((char *) prdk_k.rez_bz,0,73);
    out_quest ((char *) prdk_k.kalk_stat,0,2);
    out_quest ((char *) prdk_k.grbrt,0,2);
    out_quest ((char *) prdk_k.leits,0,10);
    out_quest ((char *) &prdk_k.chg_gew,3,0);
    out_quest ((char *) &prdk_k.varb_gew,3,0);
    out_quest ((char *) &prdk_k.zut_gew,3,0);
    out_quest ((char *) &prdk_k.huel_gew,3,0);
    out_quest ((char *) &prdk_k.tr_sw,3,0);
    out_quest ((char *) &prdk_k.is_befe_abs,3,0);
    out_quest ((char *) &prdk_k.is_befe_rel,3,0);
    out_quest ((char *) &prdk_k.is_be_rel,3,0);
    out_quest ((char *) &prdk_k.is_fett,3,0);
    out_quest ((char *) &prdk_k.is_fett_tol,3,0);
    out_quest ((char *) &prdk_k.is_fett_fe,3,0);
    out_quest ((char *) &prdk_k.is_f_h2o,3,0);
    out_quest ((char *) &prdk_k.varb_mat_o_b,3,0);
    out_quest ((char *) &prdk_k.varb_hk_teilk,3,0);
    out_quest ((char *) &prdk_k.varb_hk_vollk,3,0);
    out_quest ((char *) &prdk_k.zut_mat_o_b,3,0);
    out_quest ((char *) &prdk_k.zut_hk_teilk,3,0);
    out_quest ((char *) &prdk_k.zut_hk_vollk,3,0);
    out_quest ((char *) &prdk_k.huel_mat_o_b,3,0);
    out_quest ((char *) &prdk_k.huel_hk_teilk,3,0);
    out_quest ((char *) &prdk_k.huel_hk_vollk,3,0);
    out_quest ((char *) &prdk_k.rez_hk_teilk,3,0);
    out_quest ((char *) &prdk_k.rez_hk_vollk,3,0);
    out_quest ((char *) &prdk_k.dat,2,0);
    out_quest ((char *) &prdk_k.sw_kalk,3,0);
    out_quest ((char *) &prdk_k.bto_gew,3,0);
    out_quest ((char *) &prdk_k.nto_gew,3,0);
    out_quest ((char *) prdk_k.bearb_weg_p,0,5);
    out_quest ((char *) &prdk_k.bto_mat_o_b,3,0);
    out_quest ((char *) &prdk_k.bto_hk_teilk,3,0);
    out_quest ((char *) &prdk_k.bto_hk_vollk,3,0);
    out_quest ((char *) &prdk_k.nto_mat_o_b,3,0);
    out_quest ((char *) &prdk_k.nto_hk_teilk,3,0);
    out_quest ((char *) &prdk_k.nto_hk_vollk,3,0);
    out_quest ((char *) &prdk_k.a_hk_teilk,3,0);
    out_quest ((char *) &prdk_k.a_hk_vollk,3,0);
    out_quest ((char *) &prdk_k.delstatus,1,0);
    out_quest ((char *) &prdk_k.rez_mat_o_b,3,0);
    out_quest ((char *) &prdk_k.nto_berech_gew,3,0);
    out_quest ((char *) &prdk_k.bearb_weg,3,0);
    out_quest ((char *) prdk_k.bru,0,2);
    out_quest ((char *) &prdk_k.sw,3,0);
    out_quest ((char *) prdk_k.prodphase,0,21);
    out_quest ((char *) &prdk_k.manr,2,0);
    out_quest ((char *) &prdk_k.variante,1,0);
    out_quest ((char *) prdk_k.variante_bz,0,25);
    out_quest ((char *) &prdk_k.akv,1,0);
    out_quest ((char *) &prdk_k.vpk_gew,3,0);
    out_quest ((char *) &prdk_k.vpk_mat_o_b,3,0);
    out_quest ((char *) &prdk_k.vpk_hk_teilk,3,0);
    out_quest ((char *) &prdk_k.vpk_hk_vollk,3,0);
    out_quest ((char *) &prdk_k.nvpk_mat_o_b,3,0);
    out_quest ((char *) &prdk_k.nvpk_hk_teilk,3,0);
    out_quest ((char *) &prdk_k.nvpk_hk_vollk,3,0);
    out_quest ((char *) &prdk_k.huel_wrt,3,0);
    out_quest ((char *) &prdk_k.vpk_wrt,3,0);
    out_quest ((char *) &prdk_k.huel_hk_wrt,3,0);
    out_quest ((char *) &prdk_k.vpk_hk_wrt,3,0);
    out_quest ((char *) &prdk_k.kost_gew,3,0);
    out_quest ((char *) &prdk_k.kost_mat_o_b,3,0);
    out_quest ((char *) &prdk_k.kost_hk_teilk,3,0);
    out_quest ((char *) &prdk_k.kost_hk_vollk,3,0);
    out_quest ((char *) &prdk_k.masch_nr1,2,0);
    out_quest ((char *) &prdk_k.masch_nr2,2,0);
    out_quest ((char *) &prdk_k.masch_nr3,2,0);
    out_quest ((char *) &prdk_k.masch_nr4,2,0);
    out_quest ((char *) &prdk_k.masch_nr5,2,0);
    out_quest ((char *) &prdk_k.masch_nr6,2,0);
    out_quest ((char *) &prdk_k.masch_nr7,2,0);
    out_quest ((char *) &prdk_k.masch_nr8,2,0);
    out_quest ((char *) &prdk_k.masch_nr9,2,0);
    out_quest ((char *) &prdk_k.masch_nr10,2,0);
    out_quest ((char *) &prdk_k.chg_anz_bzg,1,0);
    out_quest ((char *) &prdk_k.kalk_me,3,0);
    out_quest ((char *) &prdk_k.chg_anz_kalk,1,0);
    out_quest ((char *) &prdk_k.pers_wrt_bzg,3,0);
    out_quest ((char *) &prdk_k.pers_wrt_kalk,3,0);
    out_quest ((char *) &prdk_k.kutter_gew,3,0);
    out_quest ((char *) &prdk_k.chargierung,1,0);
    out_quest ((char *) &prdk_k.a_sk_teilk,3,0);
    out_quest ((char *) &prdk_k.a_sk_vollk,3,0);
    out_quest ((char *) &prdk_k.a_filek_teilk,3,0);
    out_quest ((char *) &prdk_k.a_filek_vollk,3,0);
    out_quest ((char *) &prdk_k.a_filvk_teilk,3,0);
    out_quest ((char *) &prdk_k.a_filvk_vollk,3,0);
    out_quest ((char *) &prdk_k.sk_wrt,3,0);
    out_quest ((char *) &prdk_k.filek_wrt,3,0);
    out_quest ((char *) &prdk_k.filvk_wrt,3,0);
    out_quest ((char *) &prdk_k.bearb_weg_sk,3,0);
    out_quest ((char *) &prdk_k.bearb_weg_fek,3,0);
    out_quest ((char *) &prdk_k.bearb_weg_fvk,3,0);
    out_quest ((char *) &prdk_k.masch1_zeit,1,0);
    out_quest ((char *) &prdk_k.masch2_zeit,1,0);
    out_quest ((char *) &prdk_k.masch3_zeit,1,0);
    out_quest ((char *) &prdk_k.masch4_zeit,1,0);
    out_quest ((char *) &prdk_k.masch5_zeit,1,0);
    out_quest ((char *) &prdk_k.masch6_zeit,1,0);
    out_quest ((char *) &prdk_k.masch7_zeit,1,0);
    out_quest ((char *) &prdk_k.masch8_zeit,1,0);
    out_quest ((char *) &prdk_k.masch9_zeit,1,0);
    out_quest ((char *) &prdk_k.masch10_zeit,1,0);
    out_quest ((char *) &prdk_k.vorlaufzeit,1,0);
    out_quest ((char *) &prdk_k.varb_gew_nto,3,0);
    out_quest ((char *) &prdk_k.rework,1,0);
    out_quest ((char *) &prdk_k.anz_pers,2,0);
    out_quest ((char *) &prdk_k.prod_abt,1,0);
            cursor = prepare_sql ("select prdk_k.mdn,  prdk_k.a,  "
"prdk_k.rez,  prdk_k.rez_bz,  prdk_k.kalk_stat,  prdk_k.grbrt,  "
"prdk_k.leits,  prdk_k.chg_gew,  prdk_k.varb_gew,  prdk_k.zut_gew,  "
"prdk_k.huel_gew,  prdk_k.tr_sw,  prdk_k.is_befe_abs,  "
"prdk_k.is_befe_rel,  prdk_k.is_be_rel,  prdk_k.is_fett,  "
"prdk_k.is_fett_tol,  prdk_k.is_fett_fe,  prdk_k.is_f_h2o,  "
"prdk_k.varb_mat_o_b,  prdk_k.varb_hk_teilk,  prdk_k.varb_hk_vollk,  "
"prdk_k.zut_mat_o_b,  prdk_k.zut_hk_teilk,  prdk_k.zut_hk_vollk,  "
"prdk_k.huel_mat_o_b,  prdk_k.huel_hk_teilk,  prdk_k.huel_hk_vollk,  "
"prdk_k.rez_hk_teilk,  prdk_k.rez_hk_vollk,  prdk_k.dat,  "
"prdk_k.sw_kalk,  prdk_k.bto_gew,  prdk_k.nto_gew,  prdk_k.bearb_weg_p,  "
"prdk_k.bto_mat_o_b,  prdk_k.bto_hk_teilk,  prdk_k.bto_hk_vollk,  "
"prdk_k.nto_mat_o_b,  prdk_k.nto_hk_teilk,  prdk_k.nto_hk_vollk,  "
"prdk_k.a_hk_teilk,  prdk_k.a_hk_vollk,  prdk_k.delstatus,  "
"prdk_k.rez_mat_o_b,  prdk_k.nto_berech_gew,  prdk_k.bearb_weg,  "
"prdk_k.bru,  prdk_k.sw,  prdk_k.prodphase,  prdk_k.manr,  "
"prdk_k.variante,  prdk_k.variante_bz,  prdk_k.akv,  prdk_k.vpk_gew,  "
"prdk_k.vpk_mat_o_b,  prdk_k.vpk_hk_teilk,  prdk_k.vpk_hk_vollk,  "
"prdk_k.nvpk_mat_o_b,  prdk_k.nvpk_hk_teilk,  prdk_k.nvpk_hk_vollk,  "
"prdk_k.huel_wrt,  prdk_k.vpk_wrt,  prdk_k.huel_hk_wrt,  "
"prdk_k.vpk_hk_wrt,  prdk_k.kost_gew,  prdk_k.kost_mat_o_b,  "
"prdk_k.kost_hk_teilk,  prdk_k.kost_hk_vollk,  prdk_k.masch_nr1,  "
"prdk_k.masch_nr2,  prdk_k.masch_nr3,  prdk_k.masch_nr4,  "
"prdk_k.masch_nr5,  prdk_k.masch_nr6,  prdk_k.masch_nr7,  "
"prdk_k.masch_nr8,  prdk_k.masch_nr9,  prdk_k.masch_nr10,  "
"prdk_k.chg_anz_bzg,  prdk_k.kalk_me,  prdk_k.chg_anz_kalk,  "
"prdk_k.pers_wrt_bzg,  prdk_k.pers_wrt_kalk,  prdk_k.kutter_gew,  "
"prdk_k.chargierung,  prdk_k.a_sk_teilk,  prdk_k.a_sk_vollk,  "
"prdk_k.a_filek_teilk,  prdk_k.a_filek_vollk,  prdk_k.a_filvk_teilk,  "
"prdk_k.a_filvk_vollk,  prdk_k.sk_wrt,  prdk_k.filek_wrt,  "
"prdk_k.filvk_wrt,  prdk_k.bearb_weg_sk,  prdk_k.bearb_weg_fek,  "
"prdk_k.bearb_weg_fvk,  prdk_k.masch1_zeit,  prdk_k.masch2_zeit,  "
"prdk_k.masch3_zeit,  prdk_k.masch4_zeit,  prdk_k.masch5_zeit,  "
"prdk_k.masch6_zeit,  prdk_k.masch7_zeit,  prdk_k.masch8_zeit,  "
"prdk_k.masch9_zeit,  prdk_k.masch10_zeit,  prdk_k.vorlaufzeit,  "
"prdk_k.varb_gew_nto,  prdk_k.rework,  prdk_k.anz_pers,  "
"prdk_k.prod_abt from prdk_k "

#line 22 "prdk_k.rpp"
                                  "where mdn = ? "
                                  "and   a = ? "
                                  "and   variante = ?");
    ins_quest ((char *) &prdk_k.mdn,1,0);
    ins_quest ((char *) &prdk_k.a,3,0);
    ins_quest ((char *) prdk_k.rez,0,9);
    ins_quest ((char *) prdk_k.rez_bz,0,73);
    ins_quest ((char *) prdk_k.kalk_stat,0,2);
    ins_quest ((char *) prdk_k.grbrt,0,2);
    ins_quest ((char *) prdk_k.leits,0,10);
    ins_quest ((char *) &prdk_k.chg_gew,3,0);
    ins_quest ((char *) &prdk_k.varb_gew,3,0);
    ins_quest ((char *) &prdk_k.zut_gew,3,0);
    ins_quest ((char *) &prdk_k.huel_gew,3,0);
    ins_quest ((char *) &prdk_k.tr_sw,3,0);
    ins_quest ((char *) &prdk_k.is_befe_abs,3,0);
    ins_quest ((char *) &prdk_k.is_befe_rel,3,0);
    ins_quest ((char *) &prdk_k.is_be_rel,3,0);
    ins_quest ((char *) &prdk_k.is_fett,3,0);
    ins_quest ((char *) &prdk_k.is_fett_tol,3,0);
    ins_quest ((char *) &prdk_k.is_fett_fe,3,0);
    ins_quest ((char *) &prdk_k.is_f_h2o,3,0);
    ins_quest ((char *) &prdk_k.varb_mat_o_b,3,0);
    ins_quest ((char *) &prdk_k.varb_hk_teilk,3,0);
    ins_quest ((char *) &prdk_k.varb_hk_vollk,3,0);
    ins_quest ((char *) &prdk_k.zut_mat_o_b,3,0);
    ins_quest ((char *) &prdk_k.zut_hk_teilk,3,0);
    ins_quest ((char *) &prdk_k.zut_hk_vollk,3,0);
    ins_quest ((char *) &prdk_k.huel_mat_o_b,3,0);
    ins_quest ((char *) &prdk_k.huel_hk_teilk,3,0);
    ins_quest ((char *) &prdk_k.huel_hk_vollk,3,0);
    ins_quest ((char *) &prdk_k.rez_hk_teilk,3,0);
    ins_quest ((char *) &prdk_k.rez_hk_vollk,3,0);
    ins_quest ((char *) &prdk_k.dat,2,0);
    ins_quest ((char *) &prdk_k.sw_kalk,3,0);
    ins_quest ((char *) &prdk_k.bto_gew,3,0);
    ins_quest ((char *) &prdk_k.nto_gew,3,0);
    ins_quest ((char *) prdk_k.bearb_weg_p,0,5);
    ins_quest ((char *) &prdk_k.bto_mat_o_b,3,0);
    ins_quest ((char *) &prdk_k.bto_hk_teilk,3,0);
    ins_quest ((char *) &prdk_k.bto_hk_vollk,3,0);
    ins_quest ((char *) &prdk_k.nto_mat_o_b,3,0);
    ins_quest ((char *) &prdk_k.nto_hk_teilk,3,0);
    ins_quest ((char *) &prdk_k.nto_hk_vollk,3,0);
    ins_quest ((char *) &prdk_k.a_hk_teilk,3,0);
    ins_quest ((char *) &prdk_k.a_hk_vollk,3,0);
    ins_quest ((char *) &prdk_k.delstatus,1,0);
    ins_quest ((char *) &prdk_k.rez_mat_o_b,3,0);
    ins_quest ((char *) &prdk_k.nto_berech_gew,3,0);
    ins_quest ((char *) &prdk_k.bearb_weg,3,0);
    ins_quest ((char *) prdk_k.bru,0,2);
    ins_quest ((char *) &prdk_k.sw,3,0);
    ins_quest ((char *) prdk_k.prodphase,0,21);
    ins_quest ((char *) &prdk_k.manr,2,0);
    ins_quest ((char *) &prdk_k.variante,1,0);
    ins_quest ((char *) prdk_k.variante_bz,0,25);
    ins_quest ((char *) &prdk_k.akv,1,0);
    ins_quest ((char *) &prdk_k.vpk_gew,3,0);
    ins_quest ((char *) &prdk_k.vpk_mat_o_b,3,0);
    ins_quest ((char *) &prdk_k.vpk_hk_teilk,3,0);
    ins_quest ((char *) &prdk_k.vpk_hk_vollk,3,0);
    ins_quest ((char *) &prdk_k.nvpk_mat_o_b,3,0);
    ins_quest ((char *) &prdk_k.nvpk_hk_teilk,3,0);
    ins_quest ((char *) &prdk_k.nvpk_hk_vollk,3,0);
    ins_quest ((char *) &prdk_k.huel_wrt,3,0);
    ins_quest ((char *) &prdk_k.vpk_wrt,3,0);
    ins_quest ((char *) &prdk_k.huel_hk_wrt,3,0);
    ins_quest ((char *) &prdk_k.vpk_hk_wrt,3,0);
    ins_quest ((char *) &prdk_k.kost_gew,3,0);
    ins_quest ((char *) &prdk_k.kost_mat_o_b,3,0);
    ins_quest ((char *) &prdk_k.kost_hk_teilk,3,0);
    ins_quest ((char *) &prdk_k.kost_hk_vollk,3,0);
    ins_quest ((char *) &prdk_k.masch_nr1,2,0);
    ins_quest ((char *) &prdk_k.masch_nr2,2,0);
    ins_quest ((char *) &prdk_k.masch_nr3,2,0);
    ins_quest ((char *) &prdk_k.masch_nr4,2,0);
    ins_quest ((char *) &prdk_k.masch_nr5,2,0);
    ins_quest ((char *) &prdk_k.masch_nr6,2,0);
    ins_quest ((char *) &prdk_k.masch_nr7,2,0);
    ins_quest ((char *) &prdk_k.masch_nr8,2,0);
    ins_quest ((char *) &prdk_k.masch_nr9,2,0);
    ins_quest ((char *) &prdk_k.masch_nr10,2,0);
    ins_quest ((char *) &prdk_k.chg_anz_bzg,1,0);
    ins_quest ((char *) &prdk_k.kalk_me,3,0);
    ins_quest ((char *) &prdk_k.chg_anz_kalk,1,0);
    ins_quest ((char *) &prdk_k.pers_wrt_bzg,3,0);
    ins_quest ((char *) &prdk_k.pers_wrt_kalk,3,0);
    ins_quest ((char *) &prdk_k.kutter_gew,3,0);
    ins_quest ((char *) &prdk_k.chargierung,1,0);
    ins_quest ((char *) &prdk_k.a_sk_teilk,3,0);
    ins_quest ((char *) &prdk_k.a_sk_vollk,3,0);
    ins_quest ((char *) &prdk_k.a_filek_teilk,3,0);
    ins_quest ((char *) &prdk_k.a_filek_vollk,3,0);
    ins_quest ((char *) &prdk_k.a_filvk_teilk,3,0);
    ins_quest ((char *) &prdk_k.a_filvk_vollk,3,0);
    ins_quest ((char *) &prdk_k.sk_wrt,3,0);
    ins_quest ((char *) &prdk_k.filek_wrt,3,0);
    ins_quest ((char *) &prdk_k.filvk_wrt,3,0);
    ins_quest ((char *) &prdk_k.bearb_weg_sk,3,0);
    ins_quest ((char *) &prdk_k.bearb_weg_fek,3,0);
    ins_quest ((char *) &prdk_k.bearb_weg_fvk,3,0);
    ins_quest ((char *) &prdk_k.masch1_zeit,1,0);
    ins_quest ((char *) &prdk_k.masch2_zeit,1,0);
    ins_quest ((char *) &prdk_k.masch3_zeit,1,0);
    ins_quest ((char *) &prdk_k.masch4_zeit,1,0);
    ins_quest ((char *) &prdk_k.masch5_zeit,1,0);
    ins_quest ((char *) &prdk_k.masch6_zeit,1,0);
    ins_quest ((char *) &prdk_k.masch7_zeit,1,0);
    ins_quest ((char *) &prdk_k.masch8_zeit,1,0);
    ins_quest ((char *) &prdk_k.masch9_zeit,1,0);
    ins_quest ((char *) &prdk_k.masch10_zeit,1,0);
    ins_quest ((char *) &prdk_k.vorlaufzeit,1,0);
    ins_quest ((char *) &prdk_k.varb_gew_nto,3,0);
    ins_quest ((char *) &prdk_k.rework,1,0);
    ins_quest ((char *) &prdk_k.anz_pers,2,0);
    ins_quest ((char *) &prdk_k.prod_abt,1,0);
            sqltext = "update prdk_k set prdk_k.mdn = ?,  "
"prdk_k.a = ?,  prdk_k.rez = ?,  prdk_k.rez_bz = ?,  "
"prdk_k.kalk_stat = ?,  prdk_k.grbrt = ?,  prdk_k.leits = ?,  "
"prdk_k.chg_gew = ?,  prdk_k.varb_gew = ?,  prdk_k.zut_gew = ?,  "
"prdk_k.huel_gew = ?,  prdk_k.tr_sw = ?,  prdk_k.is_befe_abs = ?,  "
"prdk_k.is_befe_rel = ?,  prdk_k.is_be_rel = ?,  prdk_k.is_fett = ?,  "
"prdk_k.is_fett_tol = ?,  prdk_k.is_fett_fe = ?,  "
"prdk_k.is_f_h2o = ?,  prdk_k.varb_mat_o_b = ?,  "
"prdk_k.varb_hk_teilk = ?,  prdk_k.varb_hk_vollk = ?,  "
"prdk_k.zut_mat_o_b = ?,  prdk_k.zut_hk_teilk = ?,  "
"prdk_k.zut_hk_vollk = ?,  prdk_k.huel_mat_o_b = ?,  "
"prdk_k.huel_hk_teilk = ?,  prdk_k.huel_hk_vollk = ?,  "
"prdk_k.rez_hk_teilk = ?,  prdk_k.rez_hk_vollk = ?,  prdk_k.dat = ?,  "
"prdk_k.sw_kalk = ?,  prdk_k.bto_gew = ?,  prdk_k.nto_gew = ?,  "
"prdk_k.bearb_weg_p = ?,  prdk_k.bto_mat_o_b = ?,  "
"prdk_k.bto_hk_teilk = ?,  prdk_k.bto_hk_vollk = ?,  "
"prdk_k.nto_mat_o_b = ?,  prdk_k.nto_hk_teilk = ?,  "
"prdk_k.nto_hk_vollk = ?,  prdk_k.a_hk_teilk = ?,  "
"prdk_k.a_hk_vollk = ?,  prdk_k.delstatus = ?,  "
"prdk_k.rez_mat_o_b = ?,  prdk_k.nto_berech_gew = ?,  "
"prdk_k.bearb_weg = ?,  prdk_k.bru = ?,  prdk_k.sw = ?,  "
"prdk_k.prodphase = ?,  prdk_k.manr = ?,  prdk_k.variante = ?,  "
"prdk_k.variante_bz = ?,  prdk_k.akv = ?,  prdk_k.vpk_gew = ?,  "
"prdk_k.vpk_mat_o_b = ?,  prdk_k.vpk_hk_teilk = ?,  "
"prdk_k.vpk_hk_vollk = ?,  prdk_k.nvpk_mat_o_b = ?,  "
"prdk_k.nvpk_hk_teilk = ?,  prdk_k.nvpk_hk_vollk = ?,  "
"prdk_k.huel_wrt = ?,  prdk_k.vpk_wrt = ?,  prdk_k.huel_hk_wrt = ?,  "
"prdk_k.vpk_hk_wrt = ?,  prdk_k.kost_gew = ?,  "
"prdk_k.kost_mat_o_b = ?,  prdk_k.kost_hk_teilk = ?,  "
"prdk_k.kost_hk_vollk = ?,  prdk_k.masch_nr1 = ?,  "
"prdk_k.masch_nr2 = ?,  prdk_k.masch_nr3 = ?,  prdk_k.masch_nr4 = ?,  "
"prdk_k.masch_nr5 = ?,  prdk_k.masch_nr6 = ?,  prdk_k.masch_nr7 = ?,  "
"prdk_k.masch_nr8 = ?,  prdk_k.masch_nr9 = ?,  "
"prdk_k.masch_nr10 = ?,  prdk_k.chg_anz_bzg = ?,  "
"prdk_k.kalk_me = ?,  prdk_k.chg_anz_kalk = ?,  "
"prdk_k.pers_wrt_bzg = ?,  prdk_k.pers_wrt_kalk = ?,  "
"prdk_k.kutter_gew = ?,  prdk_k.chargierung = ?,  "
"prdk_k.a_sk_teilk = ?,  prdk_k.a_sk_vollk = ?,  "
"prdk_k.a_filek_teilk = ?,  prdk_k.a_filek_vollk = ?,  "
"prdk_k.a_filvk_teilk = ?,  prdk_k.a_filvk_vollk = ?,  "
"prdk_k.sk_wrt = ?,  prdk_k.filek_wrt = ?,  prdk_k.filvk_wrt = ?,  "
"prdk_k.bearb_weg_sk = ?,  prdk_k.bearb_weg_fek = ?,  "
"prdk_k.bearb_weg_fvk = ?,  prdk_k.masch1_zeit = ?,  "
"prdk_k.masch2_zeit = ?,  prdk_k.masch3_zeit = ?,  "
"prdk_k.masch4_zeit = ?,  prdk_k.masch5_zeit = ?,  "
"prdk_k.masch6_zeit = ?,  prdk_k.masch7_zeit = ?,  "
"prdk_k.masch8_zeit = ?,  prdk_k.masch9_zeit = ?,  "
"prdk_k.masch10_zeit = ?,  prdk_k.vorlaufzeit = ?,  "
"prdk_k.varb_gew_nto = ?,  prdk_k.rework = ?,  prdk_k.anz_pers = ?,  "
"prdk_k.prod_abt = ? "

#line 26 "prdk_k.rpp"
                                  "where mdn = ? "
                                  "and   a = ? "
                                  "and   variante = ?";
            ins_quest ((char *)   &prdk_k.mdn, 1, 0);
            ins_quest ((char *)   &prdk_k.a,   3, 0);
            ins_quest ((char *)   &prdk_k.variante,  1, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &prdk_k.mdn, 1, 0);
            ins_quest ((char *)   &prdk_k.a,   3, 0);
            ins_quest ((char *)   &prdk_k.variante,  1, 0);
            test_upd_cursor = prepare_sql ("select a from prdk_k "
                                  "where mdn = ? "
                                  "and   a = ? "
                                  "and   variante = ? "
                                  "for update");

            ins_quest ((char *) &prdk_k.mdn, 1, 0);
            ins_quest ((char *)   &prdk_k.a,   3, 0);
            ins_quest ((char *)   &prdk_k.variante,  1, 0);
            del_cursor = prepare_sql ("delete from prdk_k "
                                  "where mdn = ? "
                                  "and   a = ? "
                                  "and   variante = ?");
    ins_quest ((char *) &prdk_k.mdn,1,0);
    ins_quest ((char *) &prdk_k.a,3,0);
    ins_quest ((char *) prdk_k.rez,0,9);
    ins_quest ((char *) prdk_k.rez_bz,0,73);
    ins_quest ((char *) prdk_k.kalk_stat,0,2);
    ins_quest ((char *) prdk_k.grbrt,0,2);
    ins_quest ((char *) prdk_k.leits,0,10);
    ins_quest ((char *) &prdk_k.chg_gew,3,0);
    ins_quest ((char *) &prdk_k.varb_gew,3,0);
    ins_quest ((char *) &prdk_k.zut_gew,3,0);
    ins_quest ((char *) &prdk_k.huel_gew,3,0);
    ins_quest ((char *) &prdk_k.tr_sw,3,0);
    ins_quest ((char *) &prdk_k.is_befe_abs,3,0);
    ins_quest ((char *) &prdk_k.is_befe_rel,3,0);
    ins_quest ((char *) &prdk_k.is_be_rel,3,0);
    ins_quest ((char *) &prdk_k.is_fett,3,0);
    ins_quest ((char *) &prdk_k.is_fett_tol,3,0);
    ins_quest ((char *) &prdk_k.is_fett_fe,3,0);
    ins_quest ((char *) &prdk_k.is_f_h2o,3,0);
    ins_quest ((char *) &prdk_k.varb_mat_o_b,3,0);
    ins_quest ((char *) &prdk_k.varb_hk_teilk,3,0);
    ins_quest ((char *) &prdk_k.varb_hk_vollk,3,0);
    ins_quest ((char *) &prdk_k.zut_mat_o_b,3,0);
    ins_quest ((char *) &prdk_k.zut_hk_teilk,3,0);
    ins_quest ((char *) &prdk_k.zut_hk_vollk,3,0);
    ins_quest ((char *) &prdk_k.huel_mat_o_b,3,0);
    ins_quest ((char *) &prdk_k.huel_hk_teilk,3,0);
    ins_quest ((char *) &prdk_k.huel_hk_vollk,3,0);
    ins_quest ((char *) &prdk_k.rez_hk_teilk,3,0);
    ins_quest ((char *) &prdk_k.rez_hk_vollk,3,0);
    ins_quest ((char *) &prdk_k.dat,2,0);
    ins_quest ((char *) &prdk_k.sw_kalk,3,0);
    ins_quest ((char *) &prdk_k.bto_gew,3,0);
    ins_quest ((char *) &prdk_k.nto_gew,3,0);
    ins_quest ((char *) prdk_k.bearb_weg_p,0,5);
    ins_quest ((char *) &prdk_k.bto_mat_o_b,3,0);
    ins_quest ((char *) &prdk_k.bto_hk_teilk,3,0);
    ins_quest ((char *) &prdk_k.bto_hk_vollk,3,0);
    ins_quest ((char *) &prdk_k.nto_mat_o_b,3,0);
    ins_quest ((char *) &prdk_k.nto_hk_teilk,3,0);
    ins_quest ((char *) &prdk_k.nto_hk_vollk,3,0);
    ins_quest ((char *) &prdk_k.a_hk_teilk,3,0);
    ins_quest ((char *) &prdk_k.a_hk_vollk,3,0);
    ins_quest ((char *) &prdk_k.delstatus,1,0);
    ins_quest ((char *) &prdk_k.rez_mat_o_b,3,0);
    ins_quest ((char *) &prdk_k.nto_berech_gew,3,0);
    ins_quest ((char *) &prdk_k.bearb_weg,3,0);
    ins_quest ((char *) prdk_k.bru,0,2);
    ins_quest ((char *) &prdk_k.sw,3,0);
    ins_quest ((char *) prdk_k.prodphase,0,21);
    ins_quest ((char *) &prdk_k.manr,2,0);
    ins_quest ((char *) &prdk_k.variante,1,0);
    ins_quest ((char *) prdk_k.variante_bz,0,25);
    ins_quest ((char *) &prdk_k.akv,1,0);
    ins_quest ((char *) &prdk_k.vpk_gew,3,0);
    ins_quest ((char *) &prdk_k.vpk_mat_o_b,3,0);
    ins_quest ((char *) &prdk_k.vpk_hk_teilk,3,0);
    ins_quest ((char *) &prdk_k.vpk_hk_vollk,3,0);
    ins_quest ((char *) &prdk_k.nvpk_mat_o_b,3,0);
    ins_quest ((char *) &prdk_k.nvpk_hk_teilk,3,0);
    ins_quest ((char *) &prdk_k.nvpk_hk_vollk,3,0);
    ins_quest ((char *) &prdk_k.huel_wrt,3,0);
    ins_quest ((char *) &prdk_k.vpk_wrt,3,0);
    ins_quest ((char *) &prdk_k.huel_hk_wrt,3,0);
    ins_quest ((char *) &prdk_k.vpk_hk_wrt,3,0);
    ins_quest ((char *) &prdk_k.kost_gew,3,0);
    ins_quest ((char *) &prdk_k.kost_mat_o_b,3,0);
    ins_quest ((char *) &prdk_k.kost_hk_teilk,3,0);
    ins_quest ((char *) &prdk_k.kost_hk_vollk,3,0);
    ins_quest ((char *) &prdk_k.masch_nr1,2,0);
    ins_quest ((char *) &prdk_k.masch_nr2,2,0);
    ins_quest ((char *) &prdk_k.masch_nr3,2,0);
    ins_quest ((char *) &prdk_k.masch_nr4,2,0);
    ins_quest ((char *) &prdk_k.masch_nr5,2,0);
    ins_quest ((char *) &prdk_k.masch_nr6,2,0);
    ins_quest ((char *) &prdk_k.masch_nr7,2,0);
    ins_quest ((char *) &prdk_k.masch_nr8,2,0);
    ins_quest ((char *) &prdk_k.masch_nr9,2,0);
    ins_quest ((char *) &prdk_k.masch_nr10,2,0);
    ins_quest ((char *) &prdk_k.chg_anz_bzg,1,0);
    ins_quest ((char *) &prdk_k.kalk_me,3,0);
    ins_quest ((char *) &prdk_k.chg_anz_kalk,1,0);
    ins_quest ((char *) &prdk_k.pers_wrt_bzg,3,0);
    ins_quest ((char *) &prdk_k.pers_wrt_kalk,3,0);
    ins_quest ((char *) &prdk_k.kutter_gew,3,0);
    ins_quest ((char *) &prdk_k.chargierung,1,0);
    ins_quest ((char *) &prdk_k.a_sk_teilk,3,0);
    ins_quest ((char *) &prdk_k.a_sk_vollk,3,0);
    ins_quest ((char *) &prdk_k.a_filek_teilk,3,0);
    ins_quest ((char *) &prdk_k.a_filek_vollk,3,0);
    ins_quest ((char *) &prdk_k.a_filvk_teilk,3,0);
    ins_quest ((char *) &prdk_k.a_filvk_vollk,3,0);
    ins_quest ((char *) &prdk_k.sk_wrt,3,0);
    ins_quest ((char *) &prdk_k.filek_wrt,3,0);
    ins_quest ((char *) &prdk_k.filvk_wrt,3,0);
    ins_quest ((char *) &prdk_k.bearb_weg_sk,3,0);
    ins_quest ((char *) &prdk_k.bearb_weg_fek,3,0);
    ins_quest ((char *) &prdk_k.bearb_weg_fvk,3,0);
    ins_quest ((char *) &prdk_k.masch1_zeit,1,0);
    ins_quest ((char *) &prdk_k.masch2_zeit,1,0);
    ins_quest ((char *) &prdk_k.masch3_zeit,1,0);
    ins_quest ((char *) &prdk_k.masch4_zeit,1,0);
    ins_quest ((char *) &prdk_k.masch5_zeit,1,0);
    ins_quest ((char *) &prdk_k.masch6_zeit,1,0);
    ins_quest ((char *) &prdk_k.masch7_zeit,1,0);
    ins_quest ((char *) &prdk_k.masch8_zeit,1,0);
    ins_quest ((char *) &prdk_k.masch9_zeit,1,0);
    ins_quest ((char *) &prdk_k.masch10_zeit,1,0);
    ins_quest ((char *) &prdk_k.vorlaufzeit,1,0);
    ins_quest ((char *) &prdk_k.varb_gew_nto,3,0);
    ins_quest ((char *) &prdk_k.rework,1,0);
    ins_quest ((char *) &prdk_k.anz_pers,2,0);
    ins_quest ((char *) &prdk_k.prod_abt,1,0);
            ins_cursor = prepare_sql ("insert into prdk_k ("
"mdn,  a,  rez,  rez_bz,  kalk_stat,  grbrt,  leits,  chg_gew,  varb_gew,  zut_gew,  "
"huel_gew,  tr_sw,  is_befe_abs,  is_befe_rel,  is_be_rel,  is_fett,  "
"is_fett_tol,  is_fett_fe,  is_f_h2o,  varb_mat_o_b,  varb_hk_teilk,  "
"varb_hk_vollk,  zut_mat_o_b,  zut_hk_teilk,  zut_hk_vollk,  huel_mat_o_b,  "
"huel_hk_teilk,  huel_hk_vollk,  rez_hk_teilk,  rez_hk_vollk,  dat,  sw_kalk,  "
"bto_gew,  nto_gew,  bearb_weg_p,  bto_mat_o_b,  bto_hk_teilk,  bto_hk_vollk,  "
"nto_mat_o_b,  nto_hk_teilk,  nto_hk_vollk,  a_hk_teilk,  a_hk_vollk,  "
"delstatus,  rez_mat_o_b,  nto_berech_gew,  bearb_weg,  bru,  sw,  prodphase,  "
"manr,  variante,  variante_bz,  akv,  vpk_gew,  vpk_mat_o_b,  vpk_hk_teilk,  "
"vpk_hk_vollk,  nvpk_mat_o_b,  nvpk_hk_teilk,  nvpk_hk_vollk,  huel_wrt,  "
"vpk_wrt,  huel_hk_wrt,  vpk_hk_wrt,  kost_gew,  kost_mat_o_b,  "
"kost_hk_teilk,  kost_hk_vollk,  masch_nr1,  masch_nr2,  masch_nr3,  "
"masch_nr4,  masch_nr5,  masch_nr6,  masch_nr7,  masch_nr8,  masch_nr9,  "
"masch_nr10,  chg_anz_bzg,  kalk_me,  chg_anz_kalk,  pers_wrt_bzg,  "
"pers_wrt_kalk,  kutter_gew,  chargierung,  a_sk_teilk,  a_sk_vollk,  "
"a_filek_teilk,  a_filek_vollk,  a_filvk_teilk,  a_filvk_vollk,  sk_wrt,  "
"filek_wrt,  filvk_wrt,  bearb_weg_sk,  bearb_weg_fek,  bearb_weg_fvk,  "
"masch1_zeit,  masch2_zeit,  masch3_zeit,  masch4_zeit,  masch5_zeit,  "
"masch6_zeit,  masch7_zeit,  masch8_zeit,  masch9_zeit,  masch10_zeit,  "
"vorlaufzeit,  varb_gew_nto,  rework,  anz_pers,  prod_abt) "

#line 51 "prdk_k.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?)"); 

#line 53 "prdk_k.rpp"
}
