#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include <string.h>
#include "comcthlp.h"
#include "itemc.h"
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_menu.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_intp.h"
#include "inflib.h"
#include "spezdlg.h"
#include "enterfunc.h"
#include "ZutLst.h"
#include "spezdlg.h"
#include "searchrezv.h"
#include "searchzut.h"
// #include "dlg.h"

#define IDM_CANCEL 3001
#ifndef IDM_WRITE
#define IDM_WRITE  3002
#endif

static int StdSize = STDSIZE;

static int dlgsize    = STDSIZE;
static int ltgraysize = STDSIZE;
static int dlgpossize = STDSIZE;
static int cpsize     = 200;
static int Suchvariante  = 0;


static mfont dlgfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         GRAYCOL,
                         0,
                         NULL};

static char texttext [] = {"Text"};

static ColButton Ctxt    = { texttext,  10, -1, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, -1, -1,
                             NULL,  0, 0,
                             BLACKCOL,
                             LTGRAYCOL,
                             0, 
};

struct ZUTLSTS  ZUTLST::Lsts;
struct ZUTLSTS *ZUTLST::LstTab;


CFIELD *ZUTLST::_fChoise0 [] = {
    new CFIELD ("cancel", " abbrechen ", 10, 0, 0, 0, NULL, "",
                CBUTTON, 
                IDM_CANCEL, &dlgfont, 0, 0),
    new CFIELD ("write", " speichern ", 10, 0,12, 0, NULL, "",
                CBUTTON, 
                IDM_WRITE, &dlgfont, 0, 0),
    new CFIELD ("show",  "  ansehen  ", 10, 0,24, 0, NULL, "",
                CBUTTON, 
                IDM_CHOISE, &dlgfont, 0, 0),
    new CFIELD ("delete","  l�schen  ", 10, 0,36, 0, NULL, "",
                CBUTTON, 
                IDM_DELETE, &dlgfont, 0, 0),
    new CFIELD ("insert"," einf�gen  ", 10, 0,48, 0, NULL, "",
                CBUTTON, 
                IDM_INSERT, &dlgfont, 0, 0),
    NULL,
};

CFORM ZUTLST::fChoise0 (5, _fChoise0);

CFIELD *ZUTLST::_fChoise [] = {
    new CFIELD ("fChoise0", (char *) &fChoise0, 0, 0, -1 , -2, NULL, "",
                CFFORM, 
                500, &dlgfont, 0, 0),
    NULL,
};

CFORM ZUTLST::fChoise (1, _fChoise);


static char *EkTxt = "EK";
static char *PlanEkTxt = "Plan-EK";


ITEM ZUTLST::utyp                ("typ",                "Typ"     ,         "", 0);
ITEM ZUTLST::ubasis              ("basis",              "Basis"     ,         "", 0);
ITEM ZUTLST::unr                 ("nr",                 "Nummer",           "", 0);
ITEM ZUTLST::ua_bz1              ("a_bz1",              "Bezeichnung",      "", 0);  
ITEM ZUTLST::ukutf               ("kutf",               "Ktf.",             "", 0);
ITEM ZUTLST::uzut_me             ("zut_me",             "Menge",            "", 0);
ITEM ZUTLST::ume_einh            ("me_einh",            "Einheit",          "", 0);
ITEM ZUTLST::upr_ek              ("pr_ek",              "Preis",            "", 0);
ITEM ZUTLST::ukosten             ("kosten",             "Kosten",           "", 0);
ITEM ZUTLST::uzug_me             ("zug_me",             "Zugabe(g/kg)",           "", 0);
ITEM ZUTLST::upreisme            ("preisme",           "Preis x Me",       "", 0);
//ITEM ZUTLST::utext               ("text",               "Text",             "", 0);
ITEM ZUTLST::utext               ("text",               (char *) &Ctxt,     "", 0);
ITEM ZUTLST::ufiller             ("",                   " ",                "",0);

ITEM ZUTLST::iline ("", "1", "", 0);

ITEM ZUTLST::ityp            ("typ",            Lsts.typ,          "", 0);
ITEM ZUTLST::ibasis          ("basis",          Lsts.basis,        "", 0);
ITEM ZUTLST::inr             ("nr",             Lsts.nr,           "", 0);
ITEM ZUTLST::ia_bz1          ("a_bz1",          Lsts.a_bz1,        "", 0);  
ITEM ZUTLST::ikutf           ("kutf",           Lsts.kutf,         "", 0);
ITEM ZUTLST::izut_me         ("zut_me",         Lsts.zut_me,      "", 0);
ITEM ZUTLST::ime_einh        ("me_einh",        Lsts.me_einh,      "", 0);
ITEM ZUTLST::ipr_ek          ("pr_ek",          Lsts.pr_ek,        "", 0);
ITEM ZUTLST::ikosten         ("kosten",         Lsts.kosten,       "", 0);
ITEM ZUTLST::izug_me         ("zug_me",         Lsts.zug_me,       "", 0);
ITEM ZUTLST::ipreisme        ("preisme",        Lsts.preisme,      "", 0);
ITEM ZUTLST::itext           ("text",           Lsts.text,         "", 0);
ITEM ZUTLST::ichtyp          ("chtyp",          (char *) &Lsts.chtyp,  "", 0);
ITEM ZUTLST::ichbasis        ("chbasis",        (char *) &Lsts.chbasis,  "", 0);
ITEM ZUTLST::ichnr           ("chnr",           (char *) &Lsts.chnr,   "", 0);

field ZUTLST::_UbForm [] = {
&utyp,     17, 0, 0,  6,  NULL, "", BUTTON, 0, 0, 0,     
&unr,      21, 0, 0, 23,  NULL, "", BUTTON, 0, 0, 0,     
&ua_bz1,   22, 0, 0, 44,  NULL, "", BUTTON, 0, 0, 0, 
&ubasis,   10, 0, 0, 66,  NULL, "", BUTTON, 0, 0, 0,     
&ukutf,    12, 0, 0, 76,  NULL, "", BUTTON, 0, 0, 0,     
&uzut_me, 12, 0, 0, 88,  NULL, "", BUTTON, 0, 0, 0,     
&uzug_me,  14, 0, 0,100,  NULL, "", BUTTON, 0, 0, 0,     
&ume_einh, 10, 0, 0, 114,  NULL, "", BUTTON, 0, 0, 0,     
&upr_ek,   11, 0, 0,124,  NULL, "", BUTTON, 0, 0, 0,     
&ukosten,  11, 0, 0,135,  NULL, "", BUTTON, 0, 0, 0,     
&upreisme, 12, 0, 0,146,  NULL, "", BUTTON, 0, 0, 0,     
&utext,    42, 0, 0,158,  NULL, "", COLBUTTON, 0, 0, 0,     
&ufiller, 120, 0, 0,200,  NULL, "", BUTTON, 0, 0, 0,
};


form ZUTLST::UbForm = {13, 0, 0, _UbForm, 0, 0, 0, 0, NULL};

field ZUTLST::_LineForm [] = {
&iline,   1, 0, 0, 23,  NULL, "",  DISPLAYONLY, 0, 0, 0, 
&iline,   1, 0, 0, 44,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0, 66,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0, 76,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0, 88,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,100,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,114,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,124,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,135,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,146,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,158,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,200,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
};

form ZUTLST::LineForm = {12, 0, 0, _LineForm,0, 0, 0, 0, NULL};

field ZUTLST::_DataForm [] = {
&ityp,      16,10, 0, 7,  NULL, "",      DISPLAYONLY, 0, 0, 0,     
&inr,       17, 0, 0,24,   NULL, "%8d",   DISPLAYONLY,  0, 0, 0,     
&ichnr,      2, 1, 0,41,   NULL, "",      REMOVED,      0, 0, 0,
&ia_bz1,    20, 0, 0,45,   NULL, "",      DISPLAYONLY, 0, 0, 0, 
&ibasis,    10,10, 0,67,  NULL, "",      DISPLAYONLY, 0, 0, 0,     
&ikutf,     10, 0, 0,77,   NULL, "%2d",   EDIT,        0, 0, 0,     
&izut_me,   10, 0, 0,89,   NULL, "%8.3f", EDIT,        0, 0, 0,
&izug_me,   12, 0, 0,101,  NULL, "%11.3f",EDIT, 0, 0, 0,     
&ime_einh,   8, 0, 0,115,   NULL, "",      DISPLAYONLY, 0, 0, 0,      
&ipr_ek,     9, 0, 0,125,  NULL, "%7.3f", DISPLAYONLY, 0, 0, 0,     
&ikosten,    9, 0, 0,136,  NULL, "%7.3f", DISPLAYONLY, 0, 0, 0,     
&ipreisme,   9, 0, 0,147,  NULL, "%7.3f", DISPLAYONLY, 0, 0, 0,     
&itext,     40, 0, 0,160,  NULL, "80",    EDIT,        0, 0, 0,     
};


form ZUTLST::DataForm = {13, 0, 0, _DataForm,0, 0, 0, 0, NULL};

char *ZUTLST::TypCombo [] = {"Material",
//                             "Kosten",
                              NULL,
};
char *ZUTLST::BasisCombo [] = {"Gesamt",
                              "Fleisch u.Fett",
                              "Einlage",
                              "Ohne Einlage",
							  "behandelt",
							  "Einlage2",
                               NULL,
};


void ZUTLST::FillCombo (HWND hWnd, char *ItemName)
{
    if (strcmp (ItemName, "typ") == 0)
    {
       for (int i = 0; TypCombo [i]; i ++)
       {
               SendMessage (hWnd, CB_ADDSTRING, 0, 
                           (LPARAM) TypCombo[i]);
       }
    }
    if (strcmp (ItemName, "basis") == 0)
    {
       for (int i = 0; BasisCombo [i]; i ++)
       {
               SendMessage (hWnd, CB_ADDSTRING, 0, 
                           (LPARAM) BasisCombo[i]);
       }
    }
}


int ZUTLST::ubrows [] = {0, 
                          1, 1,
                          2,
                          3, 
                          4,
                          5,
                          6,
                          7,
                          8,
                          9,
                         10,
                         11,
                         -1,
};


struct CHATTR ZUTLST::ChAttra [] = {
//                                     "typ",     DISPLAYONLY,COMBOBOX | DROPLIST, 
                                     "basis",     COMBOBOX | DROPLIST,COMBOBOX | DROPLIST, 
                                      "nr",     DISPLAYONLY,EDIT,                
                                      "chnr",   REMOVED,BUTTON,                
                                      NULL,  0,           0};
struct CHATTR ZUTLST::ChAttrx [] = {
                                     "typ",     DISPLAYONLY,DISPLAYONLY, 
                                     "basis",   DISPLAYONLY,DISPLAYONLY, 
                                      "nr",     DISPLAYONLY,DISPLAYONLY,                
                                      "chnr",   REMOVED,REMOVED,                
                                      "kutf",   EDIT,EDIT,                
									  "zut_me", DISPLAYONLY,DISPLAYONLY,
									  "text",  DISPLAYONLY,DISPLAYONLY,
                                      NULL,  0,           0};


struct CHATTR *ZUTLST::ChAttr = ChAttra;
int ZUTLST::NoRecNrSet = FALSE;
char *ZUTLST::InfoItem = "a";


void ZUTLST::SetPlanEk (BOOL PlanEk)
{
         this->PlanEk = PlanEk;
         
         if (PlanEk)
         {
                upr_ek.SetFeld (PlanEkTxt);
				SetFieldAttr ("pr_ek", EDIT);  //#141205
         }
         else
         {
                upr_ek.SetFeld (EkTxt);
         }
}


BOOL ZUTLST::CallInfo (HWND hWnd, RECT *rect, char *Item, char *ItemValue, DWORD WinMode)
/**
Einstieg in Infosystem mit Artikel. Abgeleitet von Klasse Info.
**/
{
 		 char buffer [1024];
         BOOL ret;

         sprintf (buffer, "where a = 2");
         ret = Info::CallInfo (hWnd, rect, "a", buffer, WinMode); 
         SetListFocus ();
         return ret;
}


int ZUTLST::InfoProc (char **Item, char *Value, char *where)
/**
Spezielle Procedure fuer Info-System. Alter Ablauf, ist nicht mehr Aktiv 
**/
{

   *Item = "a";

   sprintf (where, "where a = 2");
   return 1;
}

void ZUTLST::CallInfoEx (void)
/*
Info wird aus Dialog aufgerufen.
*/

{
   char where [512];
   
   sprintf (where, "where a = %.0lf", Lsts.a);
   _CallInfoEx (hMainWindow, NULL, InfoItem, where, 0l); 
}


void ZUTLST::SetButtonSize (int Size)
{
    int fpos;

//    return;
    fpos = GetItemPos (&DataForm, "chtyp");
    if (fpos < 0) return;

    if (Size == SMALL)
    {
         DataForm.mask [fpos].length = 2;
         DataForm.mask [fpos].rows = 1;
    }
    else 
    {
         DataForm.mask [fpos].length = 3;
         DataForm.mask [fpos].rows = 0;
    }
    fpos = GetItemPos (&DataForm, "chnr");
    if (fpos < 0) return;

    if (Size == SMALL)
    {
         DataForm.mask [fpos].length = 2;
         DataForm.mask [fpos].rows = 1;
    }
    else 
    {
         DataForm.mask [fpos].length = 3;
         DataForm.mask [fpos].rows = 0;
    }
}


void ZUTLST::GetCfgValues (void)
{
       char cfg_v [512];
       COLORREF Color;

       if (ProgCfg == NULL) return;
	   if (cfgOK) return;

       cfgOK = TRUE;
       if (ProgCfg->GetCfgValue ("Suchvariante", cfg_v) == TRUE)
       {
                 Suchvariante =  (atoi (cfg_v));
	   }
       if (ProgCfg->GetCfgValue ("3DList", cfg_v) == TRUE)
       {
                    eListe.Set3D (atoi (cfg_v));
	   }
       if (ProgCfg->GetCfgValue ("3DSize", cfg_v) == TRUE)
       {
                    eListe.SetPlus3D (atoi (cfg_v));
	   }
       if (ProgCfg->GetCfgValue ("ListFocus", cfg_v) == TRUE)
       {
                    ListFocus = atoi (cfg_v);
	   }
        if (ProgCfg->GetCfgValue ("rowheight", cfg_v) == TRUE)
		{
			        RowHeight = ratod (cfg_v);
					if (RowHeight < (double) 1.0)
					{
						RowHeight = (double) 1.0;
					}
		}
        if (ProgCfg->GetCfgValue ("ubheight", cfg_v) == TRUE)
		{
			        UbHeight = atoi (cfg_v);
					SetUbHeight ();
		}
		eListe.SetRowHeight (RowHeight);


        if (ProgCfg->GetCfgValue ("NoRecNr", cfg_v) == TRUE)
        {
			        NoRecNr = atoi (cfg_v);
		            if (atoi (cfg_v))
					{
	                        SetNoRecNr ();
					}
		}
        if (ProgCfg->GetCfgValue ("ListColor", cfg_v) == TRUE)
        {
                     GetListColor (&Color, cfg_v);
		             eListe.SetColor (Color);
        }
        if (ProgCfg->GetCfgValue ("ListBkColor", cfg_v) == TRUE)
        {
                     GetListColor (&Color, cfg_v);
		             eListe.SetBkColor (Color);
        }
        if (ProgCfg->GetCfgValue ("ListLine", cfg_v) == TRUE)
        {
		             eListe.SetLines (GetListLine (cfg_v));
        }
        if (ProgCfg->GetCfgValue ("ZutInfo", cfg_v) == TRUE)
        {
                     InfoItem = new char [80];
                     strcpy (InfoItem, cfg_v);
        }
        if (ProgCfg->GetCfgValue ("ZugMeRemoved", cfg_v) == TRUE)
        {
                      if (atoi (cfg_v) != 0)
                      {
                             DestroyListField ("zug_me");
                      }
        }
        if (ProgCfg->GetCfgValue ("PreisMeRemoved", cfg_v) == TRUE)
        {
                      if (atoi (cfg_v) != 0)
                      {
                             DestroyListField ("preisme");
                      }
        }
        if (ProgCfg->GetCfgValue ("TextRemoved", cfg_v) == TRUE)
        {
                      if (atoi (cfg_v) != 0)
                      {
						     strncpy(texttext, "    ",5);
//			                 SetFieldAttr ("text", DISPLAYONLY);
                             DestroyListField ("text");
                      }
        }

}


void ZUTLST::TestRemoves (void)
{
        int fpos;

        fpos = GetItemPos (&DataForm, "feld3");
        if (fpos >= 0)
        {
            if (DataForm.mask[fpos].attribut & REMOVED)
            {
                DestroyListField ("feld3");
            }
        }
}


void ZUTLST::SetNoRecNr (void)
{
	   int i;

	   eListe.SetNoRecNr (TRUE);
	   if (NoRecNrSet) return;

	   for (i = 0; i < dataform->fieldanz; i ++)
	   {
		   dataform->mask[i].pos[1] -= 6;
	   }
	   for (i = 0; i < ubform->fieldanz; i ++)
	   {
		   ubform->mask[i].pos[1] -= 6;
	   }
	   for (i = 0; i < lineform->fieldanz; i ++)
	   {
		   lineform->mask[i].pos[1] -= 6;
	   }
       NoRecNrSet = TRUE;
}


ZUTLST::ZUTLST () : KEYS (), LISTENTER () 
{

    ProgCfg = new PROG_CFG ("23201");
	Dlg = NULL;
    cfgOK = FALSE;
	cursor = -1;
    LstTab = new ZUTLSTS [0x10000];

    dataform  = &DataForm;
    ubform    = &UbForm;
    lineform  = &LineForm;
	SetDisplayonly();
    if (sys_ben.berecht > 1 && (sys_ben.berecht < 4))
    {
           DelListField ("pr_ek");
           DelListField ("preisme");
           DelListField ("kosten");
    }
    SetFieldLen ("typ", 9);
    SetFieldLen ("basis", 15);
    SetFieldLen ("nr", 15);
    SetFieldLen ("kutf", 6);
    eListe.SetUbRows (ubrows); 
	eListe.SetKeys (this);
    LISTENTER::GetCfgValues ();
    GetCfgValues ();
//    GetSysPar ();
    TestRemoves ();
    if (NoRecNr)
    {
	    SetNoRecNr ();
    }
	fChoise.Setchpos (0, 2);
    work = NULL;
	if (sys_ben.berecht >= 3)
    {
		ChAttr = ChAttrx;

    }
	else
	{	
		ChAttr = ChAttra;
    }
    eListe.SetChAttr (ChAttr); 
    Toolbar2 = NULL;
    eListe.SetInfoProc (InfoProc);
    eListe.SetInfo (this);
    ArrDown = NULL;
    ArrDown = LoadBitmap (DLG::hInstance, "ARRDOWN");
    PlanEk = FALSE;
    Ctxt.BkColor = GetSysColor (COLOR_3DFACE);
}


ZUTLST::~ZUTLST ()
{
	if (work != NULL)
	{
          work->SetListDlg (NULL);
	}
    fChoise.destroy ();
	if (LstTab != NULL)
	{
 	    delete []LstTab;
		LstTab = NULL;
	}
    if (ProgCfg != NULL)
    {
        ProgCfg->CloseCfg ();
        delete ProgCfg;
        ProgCfg = NULL;
    }
    if (work != NULL)
    {
        work->SetZutLsts (NULL);
    }
}


// Funktionen f�r den Zugriff auf die Daten. Sind als abstrakte Schnittstellen in
// listenter definiert.

unsigned char *ZUTLST::GetDataRec (void)
{
    return (unsigned char *) &Lsts;
}

int ZUTLST::GetDataRecLen (void)
{
    return sizeof (struct ZUTLSTS);
}

char *ZUTLST::GetListTab (int i)
{
    return (char *) &LstTab[i];
}

// Ende der Schnittstellen.


void ZUTLST::Setfeld2Removed (BOOL removed)
{
    if (removed)
    {
            SetFieldAttr ("feld2", REMOVED);
    }
    else
    {
            SetFieldAttr ("feld2", EDIT);
    }
}


void ZUTLST::Prepare (void)
{
    if (work != NULL)
    {
        cursor = work->PrepareZut ();
    }
}

void ZUTLST::Open (void)
{
	if (cursor == -1)
	{
		Prepare ();
		if (cursor == -1) return;
	}
    if (work != NULL)
    {
        work->OpenZut ();
    }
}

int ZUTLST::Fetch (void)
{
	if (cursor == -1)
	{
		Prepare ();
		if (cursor == -1) return 100;
	}
    if (work != NULL)
    {
        return work->FetchZut ();
    }
	return 100;
}

void ZUTLST::CalcSum (void)
{
    summegew = 0.0;
    ekds = 0.0;
    double kostds = 0.0;

    for (int i = 0; i < eListe.GetRecanz (); i ++)
    {
        double posgew = ratod (LstTab[i].zut_me) * LstTab[i].a_gew;
        double posme = ratod (LstTab[i].zut_me);
        summegew += posgew;
        ekds += LstTab[i].gewek * posme;
        kostds += ratod (LstTab[i].kosten) * posme;
    }
    if (summegew != 0.0)
    {
        ekds /= (summegew  + work->GetVarbGew () + work->GetHuelGew ());
        kostds /= (summegew  + work->GetVarbGew () + work->GetHuelGew ());
    }
    double SwProz = prdk_k.sw_kalk / 100;
	double Menge_bto = summegew + work->GetVarbGew () + work->GetHuelGew ();
    double SwMe =  ((work->GetVarbGew () - work->GetVarbGewNto ()) + SwProz *  (Menge_bto)) * -1;

    ((SpezDlg *) Dlg)->SetSummen (summegew, ekds, kostds, ZUT,Menge_bto,SwMe);
}

void ZUTLST::ReportFill (void)
{

    for (int i = 0; i < eListe.GetRecanz (); i ++)
    {


	    reportrez.grouprez = 2;
	    reportrez.a = prdk_k.a;
	    strcpy(reportrez.a_bz1, prdk_kf.a_bz1);	
	    strcpy(reportrez.rez ,prdk_k.rez);
	    strcpy(reportrez.rez_bz, prdk_k.rez_bz);
	    reportrez.variante = prdk_k.variante;
	    strcpy(reportrez.variante_bz, prdk_k.variante_bz);
	    if (strcmp(prdk_kf.ek,"J") == 0)  
        {
			strcpy(reportrez.preis_basis, "EK");				
		}
		else
        {
			strcpy(reportrez.preis_basis, "PLAN-EK");			
		}
	    reportrez.schwund = prdk_k.sw_kalk;
	    reportrez.chargengewicht = prdk_k.chg_gew;
	    strcpy(reportrez.datum, "01.01.2003");                      //muss nocht !
	    reportrez.posi = i + 1;
	    reportrez.mat = atoi (LstTab[i].nr);
	    strcpy(reportrez.mat_bz, LstTab[i].a_bz1);
	    reportrez.menge = ratod (LstTab[i].zut_me);
	    strcpy(reportrez.einheit, LstTab[i].me_einh);
	    reportrez.ek = ratod (LstTab[i].pr_ek);
	    reportrez.wert_ek = ratod (LstTab[i].preisme);
	    reportrez.kosten = ratod (LstTab[i].kosten);
	    reportrez.bep = reportrez.ek + reportrez.kosten;
	    reportrez.wert_bep = work->Round((reportrez.bep * reportrez.menge),3); //#180803
	    strcpy(reportrez.k_a_bez, LstTab[i].kutf);           //Bezeichnung f�r Kosten und Aufschl�ge
	    reportrez.k_diff = ratod (LstTab[i].zug_me);
	    reportrez.kostenkg = 0.0;
	    reportrez.kostenart = 0;
	    strcpy(reportrez.kostenart_bz, LstTab[i].basis);
	    reportrez.mdn = prdk_k.mdn;
	    work->ReportRezUpdate ();
    }
}


void ZUTLST::Close (void)
{
	if (cursor != -1)
	{
		cursor = -1;
	}
    if (work != NULL)
    {
        work->CloseZut ();
    }
    CalcSum ();
}

BOOL ZUTLST::BeforeCol (char *colname)
{
         return FALSE;
}
         

BOOL ZUTLST::TestNr (void)
{
         char me_einh [5]; 
         double pr_ek;
         double a_gew;
         double inh_prod = 1.0;

         switch (Lsts.typ_kz)
         {
               case 1 :
                   work->SetZutLsts (&Lsts);
                   if (work->GetZutMatName () == FALSE)
                   {
                       disp_mess ("Falsche Material-Nr", 2);
                       eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
                       eListe.ShowAktRow ();
                       SetListFocus ();
                       return FALSE;
                   }
                   work->GetZutAbz1 (Lsts.a_bz1, me_einh, &inh_prod, &a_gew, atol (Lsts.nr));
                   work->GetPtab (Lsts.me_einh, "me_einh", me_einh);
				   break;
         }
         if (atoi (me_einh) == 0)
         {
                  strcpy (me_einh, "2");
         }
         work->GetPtab (Lsts.me_einh, "me_einh", me_einh);
         pr_ek = work->GetMatEk (Lsts.a,0,inh_prod,((SpezDlg *) Dlg)->GetPreisHerkunft(),((SpezDlg *) Dlg)->GetPlanPreisHerkunft());
         if (atoi (me_einh) == 2)
         {
                 a_gew = 1.0;
         }
         Lsts.gewek = pr_ek;
         Lsts.a_gew = a_gew;
         sprintf (Lsts.pr_ek,     "%.3lf",  pr_ek);
//         sprintf (Lsts.kosten,    "%.3lf",  work->GetKostenEinh ());
         double kosteneinh = work->GetKostenEinh ();
         double kostenkg   = work->GetKostenKg ();
         sprintf (Lsts.kosten,    "%.3lf", kostenkg);
		 /* Bgut-Zeugs: muss raus
         if (atoi (me_einh) != 2)
         {
                 if (kosteneinh > 0.0)
                 {
                       sprintf (Lsts.kosten,    "%.3lf", kosteneinh);
                 }
                 else if (kostenkg > 0.0)
                 {
                       sprintf (Lsts.kosten,    "%.3lf", kostenkg * a_gew);
                 }
         }
         else
         {
                 if (kostenkg > 0.0)
                 {
                       sprintf (Lsts.kosten,    "%.3lf", kostenkg);
                 }
                 else if (kosteneinh > 0.0)
                 {
                       sprintf (Lsts.kosten,    "%.3lf", kosteneinh / a_gew);
                 }
         }
		 */
         sprintf (Lsts.kutf, "%d", KtfNext ());
         memcpy (&LstTab[eListe.GetAktRow ()], &Lsts, sizeof (Lsts));
         eListe.ShowAktRow ();
         return TRUE;
}


void ZUTLST::FillTypKz (int pos)
{
          clipped (Lsts.typ);

          Lsts.typ_kz = 1;
          for (int i = 0; TypCombo[i] != NULL; i ++)
          {
              if (strcmp (Lsts.typ, TypCombo [i]) == 0)
              {
                  Lsts.typ_kz = i + 1;
                  break;
              }
          }
          memcpy (&LstTab[pos], &Lsts, sizeof (Lsts));
}

void ZUTLST::FillBasisKz (int pos)
{
          clipped (Lsts.basis);

          Lsts.basis_kz = 1;
          for (int i = 0; BasisCombo[i] != NULL; i ++)
          {
              if (strcmp (Lsts.basis, BasisCombo [i]) == 0)
              {
                  Lsts.basis_kz = i + 1;
                  break;
              }
          }
          memcpy (&LstTab[pos], &Lsts, sizeof (Lsts));
}

BOOL ZUTLST::KtfExist ()
{
         int Row = eListe.GetAktRow ();
         int Count = eListe.GetRecanz (); 

         long nr = atol (Lsts.nr);
         short kutf = atoi (Lsts.kutf);  
         for (int i = 0; i < Count; i ++)
         {
             if (i == Row) continue;

             if (nr == atol (LstTab[i].nr) &&
                 kutf == atoi (LstTab[i].kutf))
             {
                 return TRUE;
             }
         }
         return FALSE;
}


int ZUTLST::KtfNext (void)
{
         int kutf = 0;
         int Count = eListe.GetRecanz (); 
         int Row = eListe.GetAktRow ();

         for (int i = 0; i < Count; i ++)
         {
             if (i == Row) continue;
             if (atoi (LstTab[i].kutf) > kutf)
             {
                 kutf = atoi (LstTab[i].kutf);
             }
         }
         return kutf + 1;
}


BOOL ZUTLST::AfterCol (char *colname)
{
/*
         if (strcmp (colname, "chtyp") == 0)
         {
             disp_mess ("Typ gedr�ckt", 2);
             return TRUE;
         }
*/
         
         if (strcmp (colname, "typ") == 0)
         {
             FillTypKz (eListe.GetAktRow ());
             return FALSE;
         }
         else if (strcmp (colname, "chnr") == 0)
         {
             if (eListe.IsNewRec ())
             {
                 switch (Lsts.typ_kz)
                 {
                     case 1 :
                         ShowNr ();
                         break;
                     case 2 :
                         ShowRez ();
                         break;
                 }
             }
             return TRUE;
         }
         else if (strcmp (colname, "nr") == 0)
         {
             if (TestNr () == FALSE)
             {
                 return TRUE;
             }
         }
         else if (strcmp (colname, "kutf") == 0)
         {
             if (KtfExist ())
             {
                 disp_mess ("Nummer mit dieser Kutterfolge existiert schon",2); 
                 return TRUE;
             }
         }
         else if (strcmp (colname, "zut_me") == 0 )
         {
			 double zug_me = 0.0;
			switch (Lsts.basis_kz) 
			{
			case 1:  //GE Gesamt
				if (prdk_k.summegew_GE + prdk_k.summegew_EL + prdk_k.summegew_FF + prdk_k.summegew_EL1 + prdk_k.summegew_EL2 > 0.0)
				{
					zug_me = ratod (Lsts.zut_me) * 1000 / (prdk_k.summegew_GE + prdk_k.summegew_EL + prdk_k.summegew_FF + prdk_k.summegew_EL1 + prdk_k.summegew_EL2);
				} 
				break;
			case 2:  // FF Fett und Fleisch
				if (prdk_k.summegew_FF > 0.0)
				{
					zug_me = ratod (Lsts.zut_me) * 1000 / (prdk_k.summegew_FF);
				}
				break;
			case 3: // EL Einlage
				if (prdk_k.summegew_EL > 0.0)
				{
					zug_me = ratod (Lsts.zut_me) * 1000 / (prdk_k.summegew_EL);
				}
				break;
			case 4: // OE ohne Einlage
				if (prdk_k.summegew_GE + prdk_k.summegew_FF > 0.0)
				{
					zug_me = ratod (Lsts.zut_me) * 1000 / (prdk_k.summegew_GE + prdk_k.summegew_FF + prdk_k.summegew_EL1);
				}
				break;
			case 5: // behandelt
				if (prdk_k.summegew_EL1 > 0.0)
				{
					zug_me = ratod (Lsts.zut_me) * 1000 / (prdk_k.summegew_EL1);
				}
				break;
			case 6: // EL2 Einlage
				if (prdk_k.summegew_EL2 > 0.0)
				{
					zug_me = ratod (Lsts.zut_me) * 1000 / (prdk_k.summegew_EL2);
				}
				break;
			}
			 if (zug_me != 0.0)
			 {
				sprintf (Lsts.zug_me, "%.6lf", zug_me); 
			 }
//			 if (zug_me == 0.0) 
//			 {
//				 sprintf (Lsts.zut_me, "%.6lf", 0.0);
//			 }
             CalcSum ();
             sprintf (Lsts.preisme, "%.3lf", work->Round(ratod (Lsts.pr_ek) *
                                             ratod (Lsts.zut_me),3));
             memcpy (&LstTab [eListe.GetAktRow ()], &Lsts, sizeof (Lsts));
             eListe.ShowAktRow ();
         }
         else if (strcmp (colname, "zug_me") == 0 || strcmp (colname, "basis") == 0)
         {
	         if (strcmp (colname, "basis") == 0)
		     {
	             FillBasisKz (eListe.GetAktRow ());
			 }
			 if (ratod(Lsts.zug_me) > 0.000001)
			 {
				double zut_me = 0.0;
				switch (Lsts.basis_kz) 
				{
				case 1:  //GE Gesamt
					zut_me = ratod (Lsts.zug_me) * (prdk_k.summegew_GE + prdk_k.summegew_EL + prdk_k.summegew_FF + prdk_k.summegew_EL1 + prdk_k.summegew_EL2) / 1000;
					break;
				case 2:  // FF Fett und Fleisch
	 				zut_me = ratod (Lsts.zug_me) * prdk_k.summegew_FF / 1000;
					break;
				case 3: // EL Einlage
	 				zut_me = ratod (Lsts.zug_me) * prdk_k.summegew_EL / 1000;
					break;
				case 4: // OE ohne Einlage
	 				zut_me = ratod (Lsts.zug_me) * (prdk_k.summegew_GE + prdk_k.summegew_FF + prdk_k.summegew_EL1) / 1000;
					break;
				case 5: // behandelt
	 				zut_me = ratod (Lsts.zug_me) * prdk_k.summegew_EL1 / 1000;
					break;
				case 6: // EL2 Einlage
	 				zut_me = ratod (Lsts.zug_me) * prdk_k.summegew_EL2 / 1000;
					break;
				}
				sprintf (Lsts.zut_me, "%.6lf", zut_me); 
			 }
             CalcSum ();
             sprintf (Lsts.preisme, "%.3lf", work->Round(ratod (Lsts.pr_ek) *
                                             ratod (Lsts.zut_me),3));
             memcpy (&LstTab [eListe.GetAktRow ()], &Lsts, sizeof (Lsts));
             eListe.ShowAktRow ();
         }
         else if (strcmp (colname, "pr_ek") == 0) //#141205
         {
			 Lsts.gewek = ratod(Lsts.pr_ek);
             CalcSum ();
             sprintf (Lsts.preisme, "%.3lf", work->Round(ratod (Lsts.pr_ek) *
                                             ratod (Lsts.zut_me),3));
             memcpy (&LstTab [eListe.GetAktRow ()], &Lsts, sizeof (Lsts));
             eListe.ShowAktRow ();
         }
         return FALSE;
}

int ZUTLST::AfterRow (int Row)
{
//         form * DataForm; 

         if (syskey == KEYUP && ratod (Lsts.nr) == 0.0)
         {
                return OnKey7 ();
         }
         memcpy (&LstTab [Row], &Lsts, sizeof (Lsts));
         if (KtfExist ())
         {
                 return -1;
         }
         work->SetZutLsts (&Lsts);
         CalcSum ();
         return 1;
}

int ZUTLST::BeforeRow (int Row)
{
         memcpy (&LstTab [Row], &Lsts, sizeof (Lsts));
         return 1;
}

int ZUTLST::ShowNr (void)
{

        SEARCHZUT SearchZut;
        struct SZUT *szut;

       SearchZut.SetWindowStyle (WS_POPUP | WS_THICKFRAME);
       SearchZut.SetParams (DLG::hInstance, GetParent (hMainWindow));
       SearchZut.SetVariante (Suchvariante);

        if (Dlg != NULL)
        {
               SearchZut.Setawin (((SpezDlg *) Dlg)->GethMainWindow ());
        }
            
        SearchZut.Search ();
        if (syskey == KEY5)
        {
            syskey = 0;
            SetListFocus ();
            return 0;
        }
        szut = SearchZut.GetSzut ();
        if (szut == NULL)
        {
            return 0;
        }

        sprintf (LstTab [eListe.GetAktRow ()].nr, "%ld", atol (szut->mat));
        memcpy (&Lsts, &LstTab [eListe.GetAktRow ()], sizeof (Lsts));
        TestNr ();
        eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
        eListe.ShowAktRow ();
        SetListFocus ();
        return 0;
}

int ZUTLST::ShowRez (void)
{

        SEARCHREZV SearchRez;
        struct SREZV *srez;
        char query [40];

       SearchRez.SetWindowStyle (WS_POPUP | WS_THICKFRAME);
       SearchRez.SetParams (DLG::hInstance, GetParent (hMainWindow));
       SearchRez.SetVariante (Suchvariante);

        if (Dlg != NULL)
        {
//               SearchRez.SetParams (DLG::hInstance, ((SpezDlg *) Dlg)->GethWnd ());
               SearchRez.Setawin (((SpezDlg *) Dlg)->GethMainWindow ());
        }
            
        sprintf (query, "and rez != %s", prdk_k.rez);
        SearchRez.SetQuery (query);
        SearchRez.Search (prdk_k.mdn);
        SearchRez.SetQuery (NULL);
        if (syskey == KEY5)
        {
            SetListFocus ();
            return 0;
        }
        srez = SearchRez.GetSRez ();
        if (srez == NULL)
        {
            return 0;
        }

        sprintf (LstTab [eListe.GetAktRow ()].nr, "%s", srez->rez);
        memcpy (&Lsts, &LstTab [eListe.GetAktRow ()], sizeof (Lsts));
        TestNr ();
        eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
        eListe.ShowAktRow ();
        SetListFocus ();
        return 0;
}


BOOL ZUTLST::StopList (void)
{
    int i;
    int recanz;
    double chg_gew = 0.0;

    if (AfterRow (eListe.GetAktRow ()) == -1)
    {
        return TRUE;
    }

    if (atol (Lsts.nr) == 0)
    {
        OnKey7 ();
    }

    else
    {
    }

    recanz = eListe.GetRecanz ();

    work->DeleteAllZutPos ();
    for (i = 0; i < recanz; i ++)
    {
        WriteRec (i);
        chg_gew += prdk_zut.gew;
    }
    work->UpdateChgGew (chg_gew + work->GetVarbGew ());
	return TRUE;
}

BOOL ZUTLST::OnKeyNext (void)
{
        if (GetKeyState (VK_CONTROL) >= 0)
        {
              if (Dlg != NULL)
              {
                  ((SpezDlg *) Dlg)->NextPage ();
                  return TRUE;
              }
        }
        return FALSE;
}


BOOL ZUTLST::OnKeyPrior (void)
{
        if (GetKeyState (VK_CONTROL) >= 0)
        {
              if (Dlg != NULL)
              {
                  ((SpezDlg *) Dlg)->PriorPage ();
                  return TRUE;
              }
        }
        return FALSE;
}

BOOL ZUTLST::OnKey5 (void)
{

    OnKey12 ();
    syskey = KEY5;
//	eListe.BreakList ();
	return TRUE;

}

BOOL ZUTLST::OnKey6 (void)
{
    eListe.InsertLine ();
    return TRUE;
}

BOOL ZUTLST::OnKeyInsert (void)
{
    return FALSE;
/*
    eListe.InsertLine ();
    return TRUE;
*/
}

BOOL ZUTLST::OnKey7 (void)
{
    eListe.DeleteLine ();
    CalcSum ();
    return TRUE;
}

BOOL ZUTLST::OnKeyDel (void)
{
    return FALSE;
/*
    eListe.DeleteLine ();
    CalcSum ();
    return TRUE;
*/
}

BOOL ZUTLST::OnKey8 (void)
{
         return FALSE;
}


BOOL ZUTLST::OnKey9 (void)
{
    char * Item = eListe.GetAktItem ();
    if (strcmp (Item, "typ") == 0)
    {
        eListe.ShowDropDown ();
        return TRUE;
    }
    else if (strcmp (Item, "basis") == 0)
    {
        eListe.ShowDropDown ();
        return TRUE;
    }
    else if (strcmp (Item, "nr") == 0)
    {
        switch (Lsts.typ_kz)
        {
             case 1 :
                ShowNr ();
                break;
             case 2 :
                ShowRez ();
                break;
        }
        return TRUE;
    }
    return FALSE;
}


BOOL ZUTLST::OnKey10 (void)
{
    if (Dlg != NULL)
    {
         work->SetZutLsts (&Lsts);
// Hier Workfunktion einsetzen
         SetListFocus ();
         return TRUE;
	}
    return FALSE;
}

BOOL ZUTLST::OnKey11 (void)
{

    if (Dlg != NULL)
    {
        ((SpezDlg *) Dlg)->OnKey11 ();
        SetAktFocus ();
    }
    return FALSE;
}


int ZUTLST::ToMemory (int i)
{

    LISTENTER::ToMemory (i);
    return 0;
}


void ZUTLST::InitRow (void)
{

    LISTENTER::InitRow ();


    work->SetZutLsts (&Lsts);
    work->InitZutRow ();

/*
    if (ArrDown != NULL && Lsts.chtyp[0] != NULL)
    {
        Lsts.chtyp[0]->SetBitMap (ArrDown);
    }
*/


    if (ArrDown != NULL)
    {
        Lsts.chnr.SetBitMap (ArrDown);
    }


}

void ZUTLST::uebertragen (void)
{
    work->SetZutLsts (&Lsts);
    work->FillZutRow ();
    if (Lsts.typ_kz <= 0)
    {
          Lsts.typ_kz = 1;
    }
	for (int i = 0; TypCombo[i] != NULL; i ++);
	if (Lsts.typ_kz <= i)
	{
          strcpy (Lsts.typ, TypCombo [Lsts.typ_kz - 1]);
	}
	else
	{
          strcpy (Lsts.typ, TypCombo [0]);
	}
    if (Lsts.basis_kz <= 0)
    {
          Lsts.basis_kz = 1;
    }
	for ( i = 0; BasisCombo[i] != NULL; i ++);
	if (Lsts.basis_kz <= i)
	{
          strcpy (Lsts.basis, BasisCombo [Lsts.basis_kz - 1]);
	}
	else
	{
          strcpy (Lsts.basis, BasisCombo [0]);
	}

    if (ArrDown != NULL)
    {
        Lsts.chtyp.SetBitMap (ArrDown);
        Lsts.chnr.SetBitMap (ArrDown);
        Lsts.chbasis.SetBitMap (ArrDown);
    }
}

void ZUTLST::WriteRec (int i)
{
    memcpy (&Lsts, &LstTab[i], sizeof (Lsts));
    if (atol (Lsts.nr) == 0l)
    {
        return;
    }
    FillTypKz (i);
    FillBasisKz (i);
    work->SetZutLsts (&LstTab[i]);
    work->WriteZutRec ();
}

BOOL ZUTLST::OnKey12 (void)
{
	if (sys_ben.berecht >= 3) return TRUE;
    if (KtfExist ())
    {
           disp_mess ("Nummer mit dieser Kutterfolge existiert schon",2); 
           return TRUE;
    }
    CalcSum ();
    StopList ();
    syskey = KEY12;
    return TRUE;
}


BOOL ZUTLST::OnKeyUp (void)
{
//    form *DataForm;
//    int fpos;
/*
    if (Leere Zeile)
    {
        return OnKeyDel ();
    }
*/

    return FALSE;
}

BOOL ZUTLST::OnKeyDown (void)
{
    if (atol (Lsts.nr) == 0l)
    {
        return TRUE;
    }

    return FALSE;
}


void ZUTLST::CreateListQuikInfos (HWND hWnd)
{
       if (Toolbar2 == NULL) return;

       Toolbar2->GetCfield ("f5")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("f12")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("f9")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("del")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("insert")->CreateQuikInfos (hWnd);
}


BOOL ZUTLST::OnCommand (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    if (LOWORD (wParam) == IDM_CHOISE)
    {
        return OnKey9 ();
    }
    if (LOWORD (wParam) == IDM_CANCEL)
    {
        return OnKey5 ();
    }
    if (LOWORD (wParam) == IDM_WRITE)
    {
        return OnKey12 ();
    }
    if (LOWORD (wParam) == VK_DELETE)
    {
        return OnKeyDel ();
    }
    if (LOWORD (wParam) == IDM_DELETE)
    {
        return OnKey7 ();
    }
    if (LOWORD (wParam) == VK_INSERT)
    {
        return OnKeyInsert ();
    }
    if (LOWORD (wParam) == IDM_INSERT)
    {
        return OnKey6 ();
    }
    if (HIWORD (wParam)  == CBN_CLOSEUP)
	{
        eListe.GetFocusText ();
        FillTypKz (eListe.GetAktRow ());
        FillBasisKz (eListe.GetAktRow ());
        if (Dlg != NULL)
        {
	           return ((SpezDlg *) Dlg)->OnCloseUp (hWnd, msg, wParam, lParam);
        }
	}
    return FALSE;
}

BOOL ZUTLST::OnPaint (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    HDC hdc;

    if (Modal == FALSE) return FALSE;
    if (this->mamain1 != hWnd) return FALSE; 
    
    hdc = BeginPaint (hWnd, &ps);
    fChoise.display (hWnd, hdc);
    EndPaint (hWnd,&ps);
    return FALSE;
}

BOOL ZUTLST::OnMove (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{

    if (Modal == FALSE) return FALSE;
    if (this->mamain1 != hWnd) return FALSE; 

    fChoise.MoveWindow ();
    return FALSE;
}


void ZUTLST::SetDisplayonly (void)
{
	if ((prdk_k.summegew_GE + prdk_k.summegew_EL + prdk_k.summegew_FF + prdk_k.summegew_EL1 + prdk_k.summegew_EL2) == 0.0)
	{
	    SetFieldAttr ("zug_me", DISPLAYONLY);
//        DelListField ("zug_me");
	}
	else
	{
	    SetFieldAttr ("zug_me", EDIT);
	}
    if (sys_ben.berecht >= 3)
    {
            SetFieldAttr ("typ", DISPLAYONLY);
            SetFieldAttr ("basis", DISPLAYONLY);
            SetFieldAttr ("zut_me", DISPLAYONLY);
//            SetFieldAttr ("nr", DISPLAYONLY);
            SetFieldAttr ("text", DISPLAYONLY);
    }
}






