#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "wmaskc.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "prdk_zeit.h"

struct PRDK_ZEIT prdk_zeit, prdk_zeit_null;

void PRDK_ZEIT_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *)   &prdk_zeit.mdn, 1, 0);
            ins_quest ((char *)   &prdk_zeit.a,  3, 0);
            ins_quest ((char *)   &prdk_zeit.lfd, 1, 0);
    out_quest ((char *) &prdk_zeit.mdn,1,0);
    out_quest ((char *) &prdk_zeit.a,3,0);
    out_quest ((char *) &prdk_zeit.lfd,1,0);
    out_quest ((char *) &prdk_zeit.prod_schritt,1,0);
    out_quest ((char *) prdk_zeit.text,0,61);
    out_quest ((char *) &prdk_zeit.kosten,3,0);
    out_quest ((char *) &prdk_zeit.zeit_fix,3,0);
    out_quest ((char *) &prdk_zeit.zeit_spfix,3,0);
    out_quest ((char *) &prdk_zeit.zeit_var,3,0);
            cursor = prepare_sql ("select prdk_zeit.mdn,  "
"prdk_zeit.a,  prdk_zeit.lfd,  prdk_zeit.prod_schritt,  prdk_zeit.text,  "
"prdk_zeit.kosten,  prdk_zeit.zeit_fix,  prdk_zeit.zeit_spfix,  "
"prdk_zeit.zeit_var from prdk_zeit "

#line 22 "prdk_zeit.rpp."
                                  "where mdn = ? "
                                  "and   a = ? "
                                  "and lfd = ?");
    ins_quest ((char *) &prdk_zeit.mdn,1,0);
    ins_quest ((char *) &prdk_zeit.a,3,0);
    ins_quest ((char *) &prdk_zeit.lfd,1,0);
    ins_quest ((char *) &prdk_zeit.prod_schritt,1,0);
    ins_quest ((char *) prdk_zeit.text,0,61);
    ins_quest ((char *) &prdk_zeit.kosten,3,0);
    ins_quest ((char *) &prdk_zeit.zeit_fix,3,0);
    ins_quest ((char *) &prdk_zeit.zeit_spfix,3,0);
    ins_quest ((char *) &prdk_zeit.zeit_var,3,0);
            sqltext = "update prdk_zeit set "
"prdk_zeit.mdn = ?,  prdk_zeit.a = ?,  prdk_zeit.lfd = ?,  "
"prdk_zeit.prod_schritt = ?,  prdk_zeit.text = ?,  "
"prdk_zeit.kosten = ?,  prdk_zeit.zeit_fix = ?,  "
"prdk_zeit.zeit_spfix = ?,  prdk_zeit.zeit_var = ? "

#line 26 "prdk_zeit.rpp."
                                  "where mdn = ? "
                                  "and   a = ? "
                                  "and lfd = ?";
            ins_quest ((char *)   &prdk_zeit.mdn, 1, 0);
            ins_quest ((char *)   &prdk_zeit.a,  3, 0);
            ins_quest ((char *)   &prdk_zeit.lfd, 1, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *)   &prdk_zeit.mdn, 1, 0);
            ins_quest ((char *)   &prdk_zeit.a,  3, 0);
            ins_quest ((char *)   &prdk_zeit.lfd, 1, 0);
            test_upd_cursor = prepare_sql ("select a from prdk_zeit "
                                  "where mdn = ? "
                                  "and   a = ? "
                                  "and lfd = ?");
            ins_quest ((char *)   &prdk_zeit.mdn, 1, 0);
            ins_quest ((char *)   &prdk_zeit.a,  3, 0);
            ins_quest ((char *)   &prdk_zeit.lfd, 1, 0);
            del_cursor = prepare_sql ("delete from prdk_zeit "
                                  "where mdn = ? "
                                  "and   a = ? "
                                  "and lfd = ?");
    ins_quest ((char *) &prdk_zeit.mdn,1,0);
    ins_quest ((char *) &prdk_zeit.a,3,0);
    ins_quest ((char *) &prdk_zeit.lfd,1,0);
    ins_quest ((char *) &prdk_zeit.prod_schritt,1,0);
    ins_quest ((char *) prdk_zeit.text,0,61);
    ins_quest ((char *) &prdk_zeit.kosten,3,0);
    ins_quest ((char *) &prdk_zeit.zeit_fix,3,0);
    ins_quest ((char *) &prdk_zeit.zeit_spfix,3,0);
    ins_quest ((char *) &prdk_zeit.zeit_var,3,0);
            ins_cursor = prepare_sql ("insert into prdk_zeit ("
"mdn,  a,  lfd,  prod_schritt,  text,  kosten,  zeit_fix,  zeit_spfix,  zeit_var) "

#line 49 "prdk_zeit.rpp."
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?)"); 

#line 51 "prdk_zeit.rpp."
}

