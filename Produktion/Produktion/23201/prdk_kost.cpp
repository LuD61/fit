#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "wmaskc.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "prdk_kost.h"

struct PRDK_KOST prdk_kost, prdk_kost_null;

void PRDK_KOST_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *)   &prdk_kost.mdn, 1, 0);
            ins_quest ((char *)   &prdk_kost.a,  3, 0);
            ins_quest ((char *)   &prdk_kost.prdk_stufe, 1, 0);
            ins_quest ((char *)   &prdk_kost.kost_art, 1, 0);
            ins_quest ((char *)   &prdk_kost.variante,  1, 0);
    out_quest ((char *) &prdk_kost.mdn,1,0);
    out_quest ((char *) &prdk_kost.a,3,0);
    out_quest ((char *) &prdk_kost.prdk_stufe,1,0);
    out_quest ((char *) &prdk_kost.kost_me,3,0);
    out_quest ((char *) &prdk_kost.mat_o_b,3,0);
    out_quest ((char *) &prdk_kost.hk_teilk,3,0);
    out_quest ((char *) &prdk_kost.hk_vollk,3,0);
    out_quest ((char *) &prdk_kost.kost_art,1,0);
    out_quest ((char *) &prdk_kost.kosten,3,0);
    out_quest ((char *) &prdk_kost.variante,1,0);
            cursor = prepare_sql ("select prdk_kost.mdn,  "
"prdk_kost.a,  prdk_kost.prdk_stufe,  prdk_kost.kost_me,  "
"prdk_kost.mat_o_b,  prdk_kost.hk_teilk,  prdk_kost.hk_vollk,  "
"prdk_kost.kost_art,  prdk_kost.kosten,  prdk_kost.variante from prdk_kost "

#line 24 "prdk_kost.rpp"
                                  "where mdn = ? "
                                  "and   a = ? "
                                  "and prdk_stufe = ? "
                                  "and kost_art = ? "
                                  "and   variante = ?");
    ins_quest ((char *) &prdk_kost.mdn,1,0);
    ins_quest ((char *) &prdk_kost.a,3,0);
    ins_quest ((char *) &prdk_kost.prdk_stufe,1,0);
    ins_quest ((char *) &prdk_kost.kost_me,3,0);
    ins_quest ((char *) &prdk_kost.mat_o_b,3,0);
    ins_quest ((char *) &prdk_kost.hk_teilk,3,0);
    ins_quest ((char *) &prdk_kost.hk_vollk,3,0);
    ins_quest ((char *) &prdk_kost.kost_art,1,0);
    ins_quest ((char *) &prdk_kost.kosten,3,0);
    ins_quest ((char *) &prdk_kost.variante,1,0);
            sqltext = "update prdk_kost set "
"prdk_kost.mdn = ?,  prdk_kost.a = ?,  prdk_kost.prdk_stufe = ?,  "
"prdk_kost.kost_me = ?,  prdk_kost.mat_o_b = ?,  "
"prdk_kost.hk_teilk = ?,  prdk_kost.hk_vollk = ?,  "
"prdk_kost.kost_art = ?,  prdk_kost.kosten = ?,  "
"prdk_kost.variante = ? "

#line 30 "prdk_kost.rpp"
                                  "where mdn = ? "
                                  "and   a = ? "
                                  "and prdk_stufe = ? "
                                  "and kost_art = ? "
                                  "and   variante = ?"; 
            ins_quest ((char *)   &prdk_kost.mdn, 1, 0);
            ins_quest ((char *)   &prdk_kost.a,  3, 0);
            ins_quest ((char *)   &prdk_kost.prdk_stufe, 1, 0);
            ins_quest ((char *)   &prdk_kost.kost_art, 1, 0);
            ins_quest ((char *)   &prdk_kost.variante,  1, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *)   &prdk_kost.mdn, 1, 0);
            ins_quest ((char *)   &prdk_kost.a,  3, 0);
            ins_quest ((char *)   &prdk_kost.prdk_stufe, 1, 0);
            ins_quest ((char *)   &prdk_kost.kost_art, 1, 0);
            ins_quest ((char *)   &prdk_kost.variante,  1, 0);
            test_upd_cursor = prepare_sql ("select a from prdk_kost "
                                  "where mdn = ? "
                                  "and   a = ? "
                                  "and prdk_stufe = ? "
                                  "and kost_art = ? "
                                  "and   variante = ? "  
                                  "for update");

            ins_quest ((char *)   &prdk_kost.mdn, 1, 0);
            ins_quest ((char *)   &prdk_kost.a,  3, 0);
            ins_quest ((char *)   &prdk_kost.prdk_stufe, 1, 0);
            ins_quest ((char *)   &prdk_kost.kost_art, 1, 0);
            ins_quest ((char *)   &prdk_kost.variante,  1, 0);
            del_cursor = prepare_sql ("delete from prdk_kost "
                                  "where mdn = ? "
                                  "and   a = ? "
                                  "and prdk_stufe = ? "
                                  "and kost_art = ? "
                                  "and   variante = ?");  
    ins_quest ((char *) &prdk_kost.mdn,1,0);
    ins_quest ((char *) &prdk_kost.a,3,0);
    ins_quest ((char *) &prdk_kost.prdk_stufe,1,0);
    ins_quest ((char *) &prdk_kost.kost_me,3,0);
    ins_quest ((char *) &prdk_kost.mat_o_b,3,0);
    ins_quest ((char *) &prdk_kost.hk_teilk,3,0);
    ins_quest ((char *) &prdk_kost.hk_vollk,3,0);
    ins_quest ((char *) &prdk_kost.kost_art,1,0);
    ins_quest ((char *) &prdk_kost.kosten,3,0);
    ins_quest ((char *) &prdk_kost.variante,1,0);
            ins_cursor = prepare_sql ("insert into prdk_kost ("
"mdn,  a,  prdk_stufe,  kost_me,  mat_o_b,  hk_teilk,  hk_vollk,  kost_art,  kosten,  "
"variante) "

#line 67 "prdk_kost.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?)"); 

#line 69 "prdk_kost.rpp"
}
