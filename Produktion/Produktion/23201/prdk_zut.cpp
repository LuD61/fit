#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "wmaskc.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "prdk_zut.h"

struct PRDK_ZUT prdk_zut, prdk_zut_null;

void PRDK_ZUT_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *)   &prdk_zut.mdn, 1, 0);
            ins_quest ((char *)   prdk_zut.rez,  0, 9);
            ins_quest ((char *)   &prdk_zut.mat,  2, 0);
            ins_quest ((char *)   &prdk_zut.kutf,  1, 0);
            ins_quest ((char *)   &prdk_zut.variante,  1, 0);
    out_quest ((char *) &prdk_zut.mdn,1,0);
    out_quest ((char *) prdk_zut.rez,0,9);
    out_quest ((char *) &prdk_zut.mat,2,0);
    out_quest ((char *) &prdk_zut.kutf,1,0);
    out_quest ((char *) prdk_zut.bearb_info,0,81);
    out_quest ((char *) prdk_zut.zug_bas,0,3);
    out_quest ((char *) prdk_zut.zug_stnd,0,2);
    out_quest ((char *) &prdk_zut.zug_me,3,0);
    out_quest ((char *) &prdk_zut.zut_me,3,0);
    out_quest ((char *) &prdk_zut.me_einh,1,0);
    out_quest ((char *) &prdk_zut.gew,3,0);
    out_quest ((char *) &prdk_zut.variante,1,0);
    out_quest ((char *) &prdk_zut.typ,1,0);
            cursor = prepare_sql ("select prdk_zut.mdn,  "
"prdk_zut.rez,  prdk_zut.mat,  prdk_zut.kutf,  prdk_zut.bearb_info,  "
"prdk_zut.zug_bas,  prdk_zut.zug_stnd,  prdk_zut.zug_me,  "
"prdk_zut.zut_me,  prdk_zut.me_einh,  prdk_zut.gew,  prdk_zut.variante,  "
"prdk_zut.typ from prdk_zut "

#line 24 "prdk_zut.rpp"
                                  "where mdn = ? "
                                  "and   rez = ? "
                                  "and   mat = ? " 
                                  "and kutf = ? "
                                  "and variante = ?");
    ins_quest ((char *) &prdk_zut.mdn,1,0);
    ins_quest ((char *) prdk_zut.rez,0,9);
    ins_quest ((char *) &prdk_zut.mat,2,0);
    ins_quest ((char *) &prdk_zut.kutf,1,0);
    ins_quest ((char *) prdk_zut.bearb_info,0,81);
    ins_quest ((char *) prdk_zut.zug_bas,0,3);
    ins_quest ((char *) prdk_zut.zug_stnd,0,2);
    ins_quest ((char *) &prdk_zut.zug_me,3,0);
    ins_quest ((char *) &prdk_zut.zut_me,3,0);
    ins_quest ((char *) &prdk_zut.me_einh,1,0);
    ins_quest ((char *) &prdk_zut.gew,3,0);
    ins_quest ((char *) &prdk_zut.variante,1,0);
    ins_quest ((char *) &prdk_zut.typ,1,0);
            sqltext = "update prdk_zut set prdk_zut.mdn = ?,  "
"prdk_zut.rez = ?,  prdk_zut.mat = ?,  prdk_zut.kutf = ?,  "
"prdk_zut.bearb_info = ?,  prdk_zut.zug_bas = ?,  "
"prdk_zut.zug_stnd = ?,  prdk_zut.zug_me = ?,  prdk_zut.zut_me = ?,  "
"prdk_zut.me_einh = ?,  prdk_zut.gew = ?,  prdk_zut.variante = ?,  "
"prdk_zut.typ = ? "

#line 30 "prdk_zut.rpp"
                                  "where mdn = ? "
                                  "and   rez = ? "
                                  "and   mat = ? "
                                  "and kutf = ? "
                                  "and variante = ?";
            ins_quest ((char *)   &prdk_zut.mdn, 1, 0);
            ins_quest ((char *)   prdk_zut.rez,  0, 9);
            ins_quest ((char *)   &prdk_zut.mat,  2, 0);
            ins_quest ((char *)   &prdk_zut.kutf,  1, 0);
            ins_quest ((char *)   &prdk_zut.variante,  1, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &prdk_zut.mdn, 1, 0);
            ins_quest ((char *)   prdk_zut.rez,  0, 9);
            ins_quest ((char *)   &prdk_zut.mat,  2, 0);
            ins_quest ((char *)   &prdk_zut.kutf,  1, 0);
            ins_quest ((char *)   &prdk_zut.variante,  1, 0);
            test_upd_cursor = prepare_sql ("select rez from prdk_zut "
                                  "where mdn = ? "
                                  "and   rez = ? "
                                  "and   mat = ? "
                                  "and kutf = ? "
                                  "and variante = ? "
                                  "for update");

            ins_quest ((char *) &prdk_zut.mdn, 1, 0);
            ins_quest ((char *)   prdk_zut.rez,  0, 9);
            ins_quest ((char *)   &prdk_zut.mat,  2, 0);
            ins_quest ((char *)   &prdk_zut.kutf,  1, 0);
            ins_quest ((char *)   &prdk_zut.variante,  1, 0);
            del_cursor = prepare_sql ("delete from prdk_zut "
                                  "where mdn = ? "
                                  "and   rez = ? "
                                  "and   mat = ? "
                                  "and kutf = ? "
                                  "and variante = ?");
    ins_quest ((char *) &prdk_zut.mdn,1,0);
    ins_quest ((char *) prdk_zut.rez,0,9);
    ins_quest ((char *) &prdk_zut.mat,2,0);
    ins_quest ((char *) &prdk_zut.kutf,1,0);
    ins_quest ((char *) prdk_zut.bearb_info,0,81);
    ins_quest ((char *) prdk_zut.zug_bas,0,3);
    ins_quest ((char *) prdk_zut.zug_stnd,0,2);
    ins_quest ((char *) &prdk_zut.zug_me,3,0);
    ins_quest ((char *) &prdk_zut.zut_me,3,0);
    ins_quest ((char *) &prdk_zut.me_einh,1,0);
    ins_quest ((char *) &prdk_zut.gew,3,0);
    ins_quest ((char *) &prdk_zut.variante,1,0);
    ins_quest ((char *) &prdk_zut.typ,1,0);
            ins_cursor = prepare_sql ("insert into prdk_zut ("
"mdn,  rez,  mat,  kutf,  bearb_info,  zug_bas,  zug_stnd,  zug_me,  zut_me,  me_einh,  gew,  "
"variante,  typ) "

#line 67 "prdk_zut.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?)"); 

#line 69 "prdk_zut.rpp"
}
