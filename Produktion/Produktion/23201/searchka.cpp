#include <windows.h>
#include "wmaskc.h"
#include "searchka.h"
#include "strfkt.h"
#include "mo_wmess.h"
#include "lbox.h"
#include "dlglst.h"
#include "a_bas.h"


struct SKA *SEARCHKA::skatab = NULL;
struct SKA SEARCHKA::ska;
int SEARCHKA::idx = -1;
long SEARCHKA::kaanz;
CHQEX *SEARCHKA::Query = NULL;
DB_CLASS SEARCHKA::DbClass; 
HINSTANCE SEARCHKA::hMainInst;
HWND SEARCHKA::hMainWindow;
HWND SEARCHKA::awin;
int SEARCHKA::SearchField = 1;
int SEARCHKA::sokost_art = 1; 
int SEARCHKA::sokost_art_bz = 1; 
short SEARCHKA::mdn = 0; 


ITEM SEARCHKA::ukost_art      ("kost_art",       "Kostenart",    "", 0);  
ITEM SEARCHKA::ukost_art_bz   ("kost_art_bz",    "Bezeichnung",   "", 0);  


field SEARCHKA::_UbForm[] = {
&ukost_art,        11, 0, 0,  0,  NULL, "", BUTTON, 0, 0, 0,     
&ukost_art_bz,     26, 0, 0, 11 , NULL, "", BUTTON, 0, 0, 0,     
};

form SEARCHKA::UbForm = {2, 0, 0, _UbForm, 0, 0, 0, 0, NULL};

ITEM SEARCHKA::ikost_art    ("kost_art",     ska.kost_art,   "", 0);  
ITEM SEARCHKA::ikost_art_bz ("kost_art_bz" , ska.kost_art_bz,"", 0);  

field SEARCHKA::_DataForm[] = {
&ikost_art,     5, 0, 0,  4,  NULL, "%4d",     DISPLAYONLY, 0, 0, 0,     
&ikost_art_bz, 24, 0, 0, 12 , NULL, "",        DISPLAYONLY, 0, 0, 0,     
};

form SEARCHKA::DataForm = {2, 0, 0, _DataForm, 0, 0, 0, 0, NULL};

ITEM SEARCHKA::iline ("", "1", "", 0);

field SEARCHKA::_LineForm[] = {
&iline,       1, 0, 0, 11 , NULL, "", DISPLAYONLY, 0, 0, 0,     
};

form SEARCHKA::LineForm = {1, 0, 0, _LineForm, 0, 0, 0, 0, NULL};

char *SEARCHKA::query = NULL;


int SEARCHKA::sortkost_art (const void *elem1, const void *elem2)
{
	      struct SKA *el1; 
	      struct SKA *el2; 

		  el1 = (struct SKA *) elem1;
		  el2 = (struct SKA *) elem2;
          clipped (el1->kost_art);
          clipped (el2->kost_art);

          if (atoi (el1->kost_art) > atoi (el2->kost_art))
          {
              return 1 * sokost_art;
          }
          else if (atoi (el1->kost_art) < atoi (el2->kost_art))
          {
              return -1 * sokost_art;
          }

          return 0;
}

void SEARCHKA::SortKostArt (HWND hWnd)
{
   	   qsort (skatab, kaanz, sizeof (struct SKA),
				   sortkost_art);
       sokost_art *= -1;
}

int SEARCHKA::sortkost_art_bz (const void *elem1, const void *elem2)
{
	      struct SKA *el1; 
	      struct SKA *el2; 

		  el1 = (struct SKA *) elem1;
		  el2 = (struct SKA *) elem2;
          clipped (el1->kost_art_bz);
          clipped (el2->kost_art_bz);
          if (strlen (el1->kost_art_bz) == 0 &&
              strlen (el2->kost_art_bz) == 0)
          {
              return 0;
          }
          if (strlen (el1->kost_art_bz) == 0)
          {
              return -1;
          }
          if (strlen (el2->kost_art_bz) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->kost_art_bz,el2->kost_art_bz) * sokost_art_bz);
}

void SEARCHKA::SortKostArtBz (HWND hWnd)
{
   	   qsort (skatab, kaanz, sizeof (struct SKA),
				   sortkost_art_bz);
       sokost_art_bz *= -1;
}


void SEARCHKA::SortLst (int Col, HWND hWnd)
/**
Procedure zum Sortiern der Liste.
**/
{
       switch (Col)
       {
              case 0 :
                  SortKostArt (hWnd);
                  SearchField = 0;
                  break;
              case 1 :
                  SortKostArtBz (hWnd);
                  SearchField = 1;
                  break;
              default :
                  return;
       }
       UpdateList ();
}

void SEARCHKA::FillFormat (char *buffer, int size)
{
        DlgLst::FillFrmAttr (&DataForm, buffer, size);
}


void SEARCHKA::FillCaption (char *buffer, int size)
{
        DlgLst::FillFrmTitle (&UbForm, buffer, 0, size);
}

void SEARCHKA::FillVlines (char *buffer, int size) 
{
        DlgLst::FillFrmLines (&LineForm, buffer, 0, size);
}


void SEARCHKA::FillRec (char *buffer, int i, int size)
{
          DlgLst::FillFrmData (&DataForm, buffer, 0, size);
}


void SEARCHKA::UpdateList (void)
{
       int i;
 	   char buffer [512];

       for (i = 0; i < kaanz; i ++)
       {
		  memcpy (&ska, &skatab[i],  sizeof (struct SKA));
          FillRec (buffer, i, 512);
	      Query->UpdateRecord (buffer, i);
       }
}

int SEARCHKA::SearchLst (char *sebuff)
/**
Nach Kurzname in Liste suchen.
**/
{
/*
	   Query->SearchList ();
	   return 0;
*/
	   int i;
	   int len;
       char sabuff [80];
       char *pos;

	   if (skatab == NULL) return 0;
	   if (strlen (sebuff) == 0) 
	   {
	       Query->SetSel (0);
		   return 0;
	   }

	   for (i = 0; i < kaanz; i ++)
	   {
		   len = min (16, strlen (sebuff));
           if (SearchField == 0)
           {
                   strcpy (sabuff, skatab[i].kost_art);
                   for (pos = sabuff; *pos <= ' ' && *pos != 0 ; pos ++);
                   pos[len] = 0;
                   strcpy (sabuff, pos);
                   if (atoi (sebuff) == atoi (sabuff)) break;
           }
           else if (SearchField == 1)
           {
		           if (strupcmp (sebuff, skatab[i].kost_art_bz, len) == 0) break;
           }
	   }
	   if (i == kaanz) return 0;
	   Query->SetSel (i);
	   return 0;
}


int SEARCHKA::Read (char *squery)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int cursor;
	  int i;
      WMESS Wmess;


	  if (skatab) 
	  {
           delete skatab;
           skatab = NULL;
	  }


      idx = -1;
	  kaanz = 0;
      SearchField = 1;
      Query->SetSBuff (NULL);
      clipped (squery);
      if (strcmp (squery, " ") == 0) squery[0] = 0;

      if (kaanz == 0) kaanz = 0x1000;

	  skatab = new struct SKA [kaanz];
      if (squery == NULL || 
          strlen (squery) == 0)
      {
             sprintf (buffer, "select kost_art, kost_art_bz "
	 			       "from kost_art "
                       "where kost_art > 0");
              if (query != NULL && strlen (query) > 0)
              {
                  strcat (buffer, " ");
                  strcat (buffer, query);
              }

              DbClass.sqlout ((char *) ska.kost_art, 0, 5);
              DbClass.sqlout ((char *) ska.kost_art_bz, 0, 25);
              cursor = DbClass.sqlcursor (buffer);
      }
      else
      {
             sprintf (buffer, "select kost_art, kost_art_bz "
	 			       "from kost_art "
                       "where kost_art > 0 "
                       "and kost_art_bz matches \"%s*\"", squery); 
              if (query != NULL && strlen (query) > 0)
              {
                  strcat (buffer, " ");
                  strcat (buffer, query);
              }
              DbClass.sqlout ((char *) ska.kost_art, 0, 5);
              DbClass.sqlout ((char *) ska.kost_art_bz, 0, 25);
              cursor = DbClass.sqlcursor (buffer);
      }
      if (cursor < 0) 
      {
          return -1;
      }

  	  i = 0;
	  while (DbClass.sqlfetch (cursor) == 0)
      {
		  memcpy (&skatab[i], &ska, sizeof (struct SKA));
          FillRec (buffer, i, 512);
	      Query->InsertRecord (buffer);
		  i ++;
	  }
      kaanz = i;
	  DbClass.sqlclose (cursor);

      sokost_art_bz = 1;
      if (kaanz > 1)
      {
             SortKostArtBz (Query->GethWnd ());
      }
      UpdateList ();

	  EnableWindow (awin, FALSE);
	  return 0;
}

void SEARCHKA::FillBox (void)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int i;

      for (i = 0; i < kaanz; i ++)
      {
		  memcpy (&ska, &skatab[i],  sizeof (struct SKA));
          FillRec (buffer, i, 512);
	      Query->InsertRecord (buffer);
      }
      if (idx > -1)
      {
	      Query->SetSel (idx);
      }
}


void SEARCHKA::SetParams (HINSTANCE hMainInst, HWND hMainWindow)
{
      this->hMainInst   = hMainInst;
      this->hMainWindow = hMainWindow;
      awin = hMainWindow;
}


void SEARCHKA::Search (void)
{
  	  int cx, cy;
	  char buffer [512];
      RECT rect;
      BOOL doret = FALSE; 

      DlgLst::TChar = 0xFF;
      Settchar ((char) DlgLst::TChar);
      GetClientRect (hMainWindow, &rect);
      cx = 80;
      cy = 25;
//      EnableWindow (hMainWindow, FALSE);
      SetSortProc (SortLst);
      if (Query == NULL)
      {
        idx = -1;
        if (query != NULL && strlen (query) > 0)
        {
               Query = new CHQEX (cx, cy);
        }
        else
        {
               Query = new CHQEX (cx, cy);
//               Query = new CHQEX (cx, cy, "Name", "");
        }
        Query->EnableSort (TRUE);
        if (SetStyle)
        {
            Query->SetWindowStyle (WindowStyle);
        }
        Query->OpenWindow (hMainInst, hMainWindow, FALSE);
        FillFormat (buffer, 512);
        Query->RowAttr (buffer);
        FillVlines (buffer, 512);
	    Query->VLines (buffer, 3);
	    EnableWindow (awin, FALSE);

        FillCaption (buffer, 512);
	    Query->InsertCaption (buffer);

	    Query->SetFillDb (Read);
	    Query->SetSearchLst (SearchLst);
//        if (query != NULL && strlen (query) > 0)
        {
              Read ("");
              if (kaanz == 0)
              {
                  doret = TRUE;
              }
        }
      }
      else
      {
        Query->RestorefWork ();
        if (SetStyle)
        {
            Query->SetWindowStyle (WindowStyle);
        }
        Query->OpenWindow (hMainInst, hMainWindow, TRUE);
        FillFormat (buffer, 512);
        Query->RowAttr (buffer);
        FillVlines (buffer, 512);
	    Query->VLines (buffer, 3);
	    EnableWindow (awin, FALSE);

        FillCaption (buffer, 512);
	    Query->InsertCaption (buffer);

	    Query->SetFillDb (Read);
	    Query->SetSearchLst (SearchLst);
        FillBox ();
      }
      if (doret == FALSE)
      {
          Query->ProcessMessages ();
          idx = Query->GetSel ();
      }
      else
      {
          idx = -1;
      }
      query = NULL;
      EnableWindow (awin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (awin);
      if (skatab == NULL)
      {
          delete Query;
          Query = NULL;
      }
	  if (idx == -1) return;
	  memcpy (&ska, &skatab[idx], sizeof (ska));
      SetSortProc (NULL);
      if (skatab != NULL)
      {
          Query->SavefWork ();
      }
}



