#ifndef _SEARCHKA_DEF
#define _SEARCHKA_DEF
#include "wmaskc.h"
#include "cmask.h"
#include "mo_chqex.h"
#include "dbclass.h"

#define KA 10

struct SKA
{
      char kost_art [7];
	  char kost_art_bz [26];
};

class SEARCHKA
{
       private :
           static ITEM ukost_art;
           static ITEM ukost_art_bz;

           static ITEM ikost_art;
           static ITEM ikost_art_bz;


           static ITEM iline;

           static field _UbForm[];
           static form UbForm;

           static field _DataForm[];
           static form DataForm;

           static field _LineForm[];
           static form LineForm;

           static HINSTANCE hMainInst;
           static HWND hMainWindow;
           static DB_CLASS DbClass; 
           static HWND awin;
           static struct SKA *skatab;
           static struct SKA ska;
           static int idx;
           static long kaanz;
           static CHQEX *Query;
           static int SearchField;
           static char *query;
           static int sokost_art; 
           static int sokost_art_bz; 
           static short mdn;

           int SearchPos;
           int OKPos;
           int CAPos;
           DWORD WindowStyle;
           BOOL SetStyle;
        public :
           SEARCHKA ()
           {
                  SearchPos = 8;
                  OKPos = 9;
                  CAPos = 10;
                  SetStyle = FALSE;
                  mdn = 0;
                  WindowStyle = WS_POPUP | WS_THICKFRAME;
                  query = NULL;
           }

           ~SEARCHKA ()
           {
                  if (skatab != NULL)
                  {
                      delete skatab;
                      skatab = NULL;
                  }
                  if (Query != NULL)
                  {
                      delete Query;
                      Query = NULL;
                  }
           }

           void SetQuery (char *q)
           {
               if (query == NULL || (strcmp (query, q) != 0))
               {
                      query = q;
                      if (Query != NULL)
                      {
                             delete Query;
                      }
                      if (skatab != NULL)
                      {
                             delete skatab;
                      }
               }   
           }

           char *GetQuery (void)
           {
                return query;
           }

           SKA *GetSka (void)
           {
               if (idx == -1) return NULL;
               return &ska;
           }

           void Setawin (HWND awin)
           {
               this->awin = awin;
           }

           void SetWindowStyle (DWORD style)
           {
               WindowStyle = style;
               SetStyle = TRUE;
           }


           void FillBox (void);
           static void FillFormat (char *, int);
           static void FillVlines (char *, int);
           static void FillCaption (char *, int);
           static void FillRec (char *, int, int);
           static int sortkost_art (const void *, const void *);
           static void SortKostArt (HWND);
           static int sortkost_art_bz (const void *, const void *);
           static void SortKostArtBz (HWND);
           static void SortLst (int, HWND);
           static void UpdateList (void);

           static int SearchLst (char *);
           static int Read (char *);
           void SetParams (HINSTANCE, HWND);
           void Search (void);
};  
#endif