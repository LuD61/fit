#ifndef _PRDK_ZEIT_DEF
#define _PRDK_ZEIT_DEF

#include "windows.h"
#include "mo_draw.h"
#include "wmask.h"
#include "dbclass.h"

struct PRDK_ZEIT {
   short     mdn;
   double    a;
   short     lfd;
   short     prod_schritt;
   char      text[61];
   double    kosten;
   double    zeit_fix;
   double    zeit_spfix;
   double    zeit_var;
};
extern struct PRDK_ZEIT prdk_zeit, prdk_zeit_null;

#line 10 "prdk_zeit.rh"

class PRDK_ZEIT_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               PRDK_ZEIT_CLASS () : DB_CLASS ()
               {
               }
};
#endif
