#ifndef _PROGRESS_DEF
#define _PROGRESS_DEF
#include <windows.h>
#include <commctrl.h>


class PROGRESS
{
     private :
         HWND hWnd;
     public :
         PROGRESS ()
         {
         }

         PROGRESS (HWND, int, int, int, int, BOOL); 
         ~PROGRESS ();
         void SetRange (int, int);
         void SetStep (int);
         void StepIt (void);
         void SetPos (int);
};

#endif