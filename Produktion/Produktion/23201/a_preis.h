#ifndef _A_PREIS_DEF
#define _A_PREIS_DEF

#include "windows.h"
#include "mo_draw.h"
#include "wmask.h"
#include "dbclass.h"

struct A_PREIS {
   long      mdn;
   short     fil;
   double    a;
   short     prodabt;
   short     druckfolge;
   double    ek;
   double    plan_ek;
   double    kostenkg;
   double    kosteneinh;
   short     flg_bearb_weg;
   short     bearb_hk;
   short     bearb_p1;
   short     bearb_p2;
   short     bearb_p3;
   short     bearb_p4;
   short     bearb_p5;
   short     bearb_p6;
   short     bearb_vk;
   double    sp_sk;
   double    sp_fil;
   double    sp_vk;
   long      dat;
   char      zeit[6];
   char      pers_nam[9];
   long      inh_ek;
   double    bep;
   double    plan_bep;
};
extern struct A_PREIS a_preis, a_preis_null;

#line 10 "a_preis.rh"

class A_PREIS_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               A_PREIS_CLASS () : DB_CLASS ()
               {
               }
};
#endif
