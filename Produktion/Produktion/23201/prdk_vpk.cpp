#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "wmaskc.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "prdk_Vpk.h"

struct PRDK_VPK prdk_vpk, prdk_vpk_null;

void PRDK_VPK_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *)   &prdk_vpk.mdn, 1, 0);
            ins_quest ((char *)   &prdk_vpk.a,  3, 0);
            ins_quest ((char *)   &prdk_vpk.a_vpk,  3, 0);
            ins_quest ((char *)   &prdk_vpk.bearb_flg,  1, 0);
            ins_quest ((char *)   &prdk_vpk.variante,  1, 0);
    out_quest ((char *) &prdk_vpk.mdn,1,0);
    out_quest ((char *) &prdk_vpk.a,3,0);
    out_quest ((char *) &prdk_vpk.a_vpk,3,0);
    out_quest ((char *) &prdk_vpk.bearb_flg,1,0);
    out_quest ((char *) prdk_vpk.bearb_info,0,49);
    out_quest ((char *) &prdk_vpk.me,3,0);
    out_quest ((char *) &prdk_vpk.me_einh,1,0);
    out_quest ((char *) &prdk_vpk.fuel_gew,3,0);
    out_quest ((char *) &prdk_vpk.gew,3,0);
    out_quest ((char *) &prdk_vpk.variante,1,0);
    out_quest ((char *) &prdk_vpk.typ,1,0);
    out_quest ((char *) prdk_vpk.me_buch,0,2);
            cursor = prepare_sql ("select prdk_vpk.mdn,  "
"prdk_vpk.a,  prdk_vpk.a_vpk,  prdk_vpk.bearb_flg,  "
"prdk_vpk.bearb_info,  prdk_vpk.me,  prdk_vpk.me_einh,  "
"prdk_vpk.fuel_gew,  prdk_vpk.gew,  prdk_vpk.variante,  prdk_vpk.typ,  "
"prdk_vpk.me_buch from prdk_vpk "

#line 24 "prdk_vpk.rpp"
                                  "where mdn = ? "
                                  "and   a   = ? "
                                  "and   a_vpk = ? "
                                  "and bearb_flg = ? " 
                                  "and variante = ?");
    ins_quest ((char *) &prdk_vpk.mdn,1,0);
    ins_quest ((char *) &prdk_vpk.a,3,0);
    ins_quest ((char *) &prdk_vpk.a_vpk,3,0);
    ins_quest ((char *) &prdk_vpk.bearb_flg,1,0);
    ins_quest ((char *) prdk_vpk.bearb_info,0,49);
    ins_quest ((char *) &prdk_vpk.me,3,0);
    ins_quest ((char *) &prdk_vpk.me_einh,1,0);
    ins_quest ((char *) &prdk_vpk.fuel_gew,3,0);
    ins_quest ((char *) &prdk_vpk.gew,3,0);
    ins_quest ((char *) &prdk_vpk.variante,1,0);
    ins_quest ((char *) &prdk_vpk.typ,1,0);
    ins_quest ((char *) prdk_vpk.me_buch,0,2);
            sqltext = "update prdk_vpk set prdk_vpk.mdn = ?,  "
"prdk_vpk.a = ?,  prdk_vpk.a_vpk = ?,  prdk_vpk.bearb_flg = ?,  "
"prdk_vpk.bearb_info = ?,  prdk_vpk.me = ?,  prdk_vpk.me_einh = ?,  "
"prdk_vpk.fuel_gew = ?,  prdk_vpk.gew = ?,  prdk_vpk.variante = ?,  "
"prdk_vpk.typ = ?,  prdk_vpk.me_buch = ? "

#line 30 "prdk_vpk.rpp"
                                  "where mdn = ? "
                                  "and   a   = ? "
                                  "and a_vpk = ? "
                                  "and bearb_flg = ? " 
                                  "and variante = ?";
            ins_quest ((char *)   &prdk_vpk.mdn, 1, 0);
            ins_quest ((char *)   &prdk_vpk.a,  3, 0);
            ins_quest ((char *)   &prdk_vpk.a_vpk,  3, 0);
            ins_quest ((char *)   &prdk_vpk.bearb_flg,  1, 0);
            ins_quest ((char *)   &prdk_vpk.variante,  1, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &prdk_vpk.mdn, 1, 0);
            ins_quest ((char *)   &prdk_vpk.a,  3, 0);
            ins_quest ((char *)   &prdk_vpk.a_vpk,  3, 0);
            ins_quest ((char *)   &prdk_vpk.bearb_flg,  1, 0);
            ins_quest ((char *)   &prdk_vpk.variante,  1, 0);
            test_upd_cursor = prepare_sql ("select a from prdk_vpk "
                                  "where mdn = ? "
                                  "and   a = ? "
                                  "and a_vpk = ? "
                                  "and bearb_flg = ? " 
                                  "and variante = ? "
                                  "for update");

            ins_quest ((char *) &prdk_vpk.mdn, 1, 0);
            ins_quest ((char *)   &prdk_vpk.a,  3, 0);
            ins_quest ((char *)   &prdk_vpk.a_vpk,  3, 0);
            ins_quest ((char *)   &prdk_vpk.bearb_flg,  1, 0);
            ins_quest ((char *)   &prdk_vpk.variante,  1, 0);
            del_cursor = prepare_sql ("delete from prdk_vpk "
                                  "where mdn = ? "
                                  "and   a = ? "
                                  "and a_vpk = ? "
                                  "and bearb_flg = ? " 
                                  "and variante = ?");
    ins_quest ((char *) &prdk_vpk.mdn,1,0);
    ins_quest ((char *) &prdk_vpk.a,3,0);
    ins_quest ((char *) &prdk_vpk.a_vpk,3,0);
    ins_quest ((char *) &prdk_vpk.bearb_flg,1,0);
    ins_quest ((char *) prdk_vpk.bearb_info,0,49);
    ins_quest ((char *) &prdk_vpk.me,3,0);
    ins_quest ((char *) &prdk_vpk.me_einh,1,0);
    ins_quest ((char *) &prdk_vpk.fuel_gew,3,0);
    ins_quest ((char *) &prdk_vpk.gew,3,0);
    ins_quest ((char *) &prdk_vpk.variante,1,0);
    ins_quest ((char *) &prdk_vpk.typ,1,0);
    ins_quest ((char *) prdk_vpk.me_buch,0,2);
            ins_cursor = prepare_sql ("insert into prdk_vpk ("
"mdn,  a,  a_vpk,  bearb_flg,  bearb_info,  me,  me_einh,  fuel_gew,  gew,  variante,  typ,  "
"me_buch) "

#line 67 "prdk_vpk.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?)"); 

#line 69 "prdk_vpk.rpp"
}
