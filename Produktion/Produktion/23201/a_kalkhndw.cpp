#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "wmaskc.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "a_kalkhndw.h"

struct A_KALKHNDW a_kalkhndw, a_kalkhndw_null;

void A_KALKHNDW_CLASS::prepare (void)
{
            char *sqltext;

//            init_sqlin ();
            ins_quest ((char *)   &a_kalkhndw.mdn, 2, 0);
            ins_quest ((char *)   &a_kalkhndw.fil, 2, 0);
            ins_quest ((char *)   &a_kalkhndw.a,  3, 0);
    out_quest ((char *) &a_kalkhndw.a,3,0);
    out_quest ((char *) &a_kalkhndw.bearb_fil,1,0);
    out_quest ((char *) &a_kalkhndw.bearb_lad,1,0);
    out_quest ((char *) &a_kalkhndw.bearb_sk,1,0);
    out_quest ((char *) &a_kalkhndw.delstatus,1,0);
    out_quest ((char *) &a_kalkhndw.fil,1,0);
    out_quest ((char *) &a_kalkhndw.fil_ek_teilk,3,0);
    out_quest ((char *) &a_kalkhndw.fil_ek_vollk,3,0);
    out_quest ((char *) &a_kalkhndw.lad_vk_teilk,3,0);
    out_quest ((char *) &a_kalkhndw.lad_vk_vollk,3,0);
    out_quest ((char *) &a_kalkhndw.mdn,1,0);
    out_quest ((char *) &a_kalkhndw.pr_ek1,3,0);
    out_quest ((char *) &a_kalkhndw.pr_ek2,3,0);
    out_quest ((char *) &a_kalkhndw.pr_ek3,3,0);
    out_quest ((char *) &a_kalkhndw.sk_teilk,3,0);
    out_quest ((char *) &a_kalkhndw.sk_vollk,3,0);
    out_quest ((char *) &a_kalkhndw.sp_fil,3,0);
    out_quest ((char *) &a_kalkhndw.sp_lad,3,0);
    out_quest ((char *) &a_kalkhndw.sp_vk,3,0);
    out_quest ((char *) &a_kalkhndw.we_me1,3,0);
    out_quest ((char *) &a_kalkhndw.we_me2,3,0);
    out_quest ((char *) &a_kalkhndw.we_me3,3,0);
    out_quest ((char *) &a_kalkhndw.aend_dat,2,0);
            cursor = prepare_sql ("select a_kalkhndw.a,  "
"a_kalkhndw.bearb_fil,  a_kalkhndw.bearb_lad,  a_kalkhndw.bearb_sk,  "
"a_kalkhndw.delstatus,  a_kalkhndw.fil,  a_kalkhndw.fil_ek_teilk,  "
"a_kalkhndw.fil_ek_vollk,  a_kalkhndw.lad_vk_teilk,  "
"a_kalkhndw.lad_vk_vollk,  a_kalkhndw.mdn,  a_kalkhndw.pr_ek1,  "
"a_kalkhndw.pr_ek2,  a_kalkhndw.pr_ek3,  a_kalkhndw.sk_teilk,  "
"a_kalkhndw.sk_vollk,  a_kalkhndw.sp_fil,  a_kalkhndw.sp_lad,  "
"a_kalkhndw.sp_vk,  a_kalkhndw.we_me1,  a_kalkhndw.we_me2,  "
"a_kalkhndw.we_me3,  a_kalkhndw.aend_dat from a_kalkhndw "

#line 23 "a_kalkhndw.rpp"
                                  "where mdn = ? "
                                  "and fil = ? "
                                  "and   a   = ?");

    ins_quest ((char *) &a_kalkhndw.a,3,0);
    ins_quest ((char *) &a_kalkhndw.bearb_fil,1,0);
    ins_quest ((char *) &a_kalkhndw.bearb_lad,1,0);
    ins_quest ((char *) &a_kalkhndw.bearb_sk,1,0);
    ins_quest ((char *) &a_kalkhndw.delstatus,1,0);
    ins_quest ((char *) &a_kalkhndw.fil,1,0);
    ins_quest ((char *) &a_kalkhndw.fil_ek_teilk,3,0);
    ins_quest ((char *) &a_kalkhndw.fil_ek_vollk,3,0);
    ins_quest ((char *) &a_kalkhndw.lad_vk_teilk,3,0);
    ins_quest ((char *) &a_kalkhndw.lad_vk_vollk,3,0);
    ins_quest ((char *) &a_kalkhndw.mdn,1,0);
    ins_quest ((char *) &a_kalkhndw.pr_ek1,3,0);
    ins_quest ((char *) &a_kalkhndw.pr_ek2,3,0);
    ins_quest ((char *) &a_kalkhndw.pr_ek3,3,0);
    ins_quest ((char *) &a_kalkhndw.sk_teilk,3,0);
    ins_quest ((char *) &a_kalkhndw.sk_vollk,3,0);
    ins_quest ((char *) &a_kalkhndw.sp_fil,3,0);
    ins_quest ((char *) &a_kalkhndw.sp_lad,3,0);
    ins_quest ((char *) &a_kalkhndw.sp_vk,3,0);
    ins_quest ((char *) &a_kalkhndw.we_me1,3,0);
    ins_quest ((char *) &a_kalkhndw.we_me2,3,0);
    ins_quest ((char *) &a_kalkhndw.we_me3,3,0);
    ins_quest ((char *) &a_kalkhndw.aend_dat,2,0);
            sqltext = "update a_kalkhndw set "
"a_kalkhndw.a = ?,  a_kalkhndw.bearb_fil = ?,  "
"a_kalkhndw.bearb_lad = ?,  a_kalkhndw.bearb_sk = ?,  "
"a_kalkhndw.delstatus = ?,  a_kalkhndw.fil = ?,  "
"a_kalkhndw.fil_ek_teilk = ?,  a_kalkhndw.fil_ek_vollk = ?,  "
"a_kalkhndw.lad_vk_teilk = ?,  a_kalkhndw.lad_vk_vollk = ?,  "
"a_kalkhndw.mdn = ?,  a_kalkhndw.pr_ek1 = ?,  a_kalkhndw.pr_ek2 = ?,  "
"a_kalkhndw.pr_ek3 = ?,  a_kalkhndw.sk_teilk = ?,  "
"a_kalkhndw.sk_vollk = ?,  a_kalkhndw.sp_fil = ?,  "
"a_kalkhndw.sp_lad = ?,  a_kalkhndw.sp_vk = ?,  "
"a_kalkhndw.we_me1 = ?,  a_kalkhndw.we_me2 = ?,  "
"a_kalkhndw.we_me3 = ?,  a_kalkhndw.aend_dat = ? "

#line 28 "a_kalkhndw.rpp"
                                  "where mdn = ? "
                                  "and fil = ? "
                                  "and   a   = ?";
            ins_quest ((char *)   &a_kalkhndw.mdn, 2, 0);
            ins_quest ((char *)   &a_kalkhndw.fil, 2, 0);
            ins_quest ((char *)   &a_kalkhndw.a,  3, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &a_kalkhndw.mdn, 2, 0);
            ins_quest ((char *) &a_kalkhndw.fil, 2, 0);
            ins_quest ((char *)   &a_kalkhndw.a,  3, 0);
            test_upd_cursor = prepare_sql ("select a from a_kalkhndw "
                                  "where mdn = ? "
                                  "and fil = ? "
                                  "and   a = ? "
                                  "for update");

            ins_quest ((char *) &a_kalkhndw.mdn, 2, 0);
            ins_quest ((char *) &a_kalkhndw.fil, 2, 0);
            ins_quest ((char *)   &a_kalkhndw.a,  3, 0);
            del_cursor = prepare_sql ("delete from a_kalkhndw "
                                  "where mdn = ? "
                                  " and fil = ? "
                                  "and   a = ?");

    ins_quest ((char *) &a_kalkhndw.a,3,0);
    ins_quest ((char *) &a_kalkhndw.bearb_fil,1,0);
    ins_quest ((char *) &a_kalkhndw.bearb_lad,1,0);
    ins_quest ((char *) &a_kalkhndw.bearb_sk,1,0);
    ins_quest ((char *) &a_kalkhndw.delstatus,1,0);
    ins_quest ((char *) &a_kalkhndw.fil,1,0);
    ins_quest ((char *) &a_kalkhndw.fil_ek_teilk,3,0);
    ins_quest ((char *) &a_kalkhndw.fil_ek_vollk,3,0);
    ins_quest ((char *) &a_kalkhndw.lad_vk_teilk,3,0);
    ins_quest ((char *) &a_kalkhndw.lad_vk_vollk,3,0);
    ins_quest ((char *) &a_kalkhndw.mdn,1,0);
    ins_quest ((char *) &a_kalkhndw.pr_ek1,3,0);
    ins_quest ((char *) &a_kalkhndw.pr_ek2,3,0);
    ins_quest ((char *) &a_kalkhndw.pr_ek3,3,0);
    ins_quest ((char *) &a_kalkhndw.sk_teilk,3,0);
    ins_quest ((char *) &a_kalkhndw.sk_vollk,3,0);
    ins_quest ((char *) &a_kalkhndw.sp_fil,3,0);
    ins_quest ((char *) &a_kalkhndw.sp_lad,3,0);
    ins_quest ((char *) &a_kalkhndw.sp_vk,3,0);
    ins_quest ((char *) &a_kalkhndw.we_me1,3,0);
    ins_quest ((char *) &a_kalkhndw.we_me2,3,0);
    ins_quest ((char *) &a_kalkhndw.we_me3,3,0);
    ins_quest ((char *) &a_kalkhndw.aend_dat,2,0);
            ins_cursor = prepare_sql ("insert into a_kalkhndw ("
"a,  bearb_fil,  bearb_lad,  bearb_sk,  delstatus,  fil,  fil_ek_teilk,  "
"fil_ek_vollk,  lad_vk_teilk,  lad_vk_vollk,  mdn,  pr_ek1,  pr_ek2,  pr_ek3,  "
"sk_teilk,  sk_vollk,  sp_fil,  sp_lad,  sp_vk,  we_me1,  we_me2,  we_me3,  aend_dat) "

#line 54 "a_kalkhndw.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?)");

#line 56 "a_kalkhndw.rpp"
}
