#include <windows.h>
#include "wmaskc.h"
#include "searchzut.h"
#include "strfkt.h"
#include "mo_wmess.h"
#include "lbox.h"
#include "dlglst.h"
#include "a_bas.h"


struct SZUT *SEARCHZUT::szuttab = NULL;
struct SZUT SEARCHZUT::szut;
int SEARCHZUT::idx = -1;
long SEARCHZUT::zutanz;
CHQEX *SEARCHZUT::Query = NULL;
DB_CLASS SEARCHZUT::DbClass; 
HINSTANCE SEARCHZUT::hMainInst;
HWND SEARCHZUT::hMainWindow;
HWND SEARCHZUT::awin;
int SEARCHZUT::SearchField = 1;
int SEARCHZUT::somat = 1; 
int SEARCHZUT::soa = 1; 
int SEARCHZUT::soa_bz1 = 1; 
short SEARCHZUT::mdn = 0; 


ITEM SEARCHZUT::ua        ("a",         "ArtikelNr.",    "", 0);  
ITEM SEARCHZUT::umat      ("mat",       "MaterialNr.",   "", 0);  
ITEM SEARCHZUT::ua_bz1    ("a_bz1" ,    "Bezeichnung",   "", 0);  


field SEARCHZUT::_UbForm[] = {
&ua,        15, 0, 0,  0,  NULL, "", BUTTON, 0, 0, 0,     
&umat,      10, 0, 0, 15 , NULL, "", BUTTON, 0, 0, 0,     
&ua_bz1,    52, 0, 0, 25 , NULL, "", BUTTON, 0, 0, 0,     
};

form SEARCHZUT::UbForm = {3, 0, 0, _UbForm, 0, 0, 0, 0, NULL};

ITEM SEARCHZUT::ia        ("a",         szut.a,      "", 0);  
ITEM SEARCHZUT::imat      ("mat",       szut.mat,    "", 0);  
ITEM SEARCHZUT::ia_bz1    ("a_bz1" ,    szut.a_bz1,  "", 0);  

field SEARCHZUT::_DataForm[] = {
&ia,        13, 0, 0,  1,  NULL, "%13.0lf", DISPLAYONLY, 0, 0, 0,     
&imat   ,    8, 0, 0, 16 , NULL, "%8d",     DISPLAYONLY, 0, 0, 0,     
&ia_bz1,    52, 0, 0, 27 , NULL, "",        DISPLAYONLY, 0, 0, 0,     
};

form SEARCHZUT::DataForm = {3, 0, 0, _DataForm, 0, 0, 0, 0, NULL};

ITEM SEARCHZUT::iline ("", "1", "", 0);

field SEARCHZUT::_LineForm[] = {
&iline,       1, 0, 0, 15 , NULL, "", DISPLAYONLY, 0, 0, 0,     
&iline,       1, 0, 0, 25 , NULL, "", DISPLAYONLY, 0, 0, 0,     
};

form SEARCHZUT::LineForm = {2, 0, 0, _LineForm, 0, 0, 0, 0, NULL};

char *SEARCHZUT::query = NULL;

int SEARCHZUT::sortmat (const void *elem1, const void *elem2)
{
	      struct SZUT *el1; 
	      struct SZUT *el2; 

		  el1 = (struct SZUT *) elem1;
		  el2 = (struct SZUT *) elem2;
	      return ((atol (el1->mat) - atol (el2->mat)) * somat);
}

void SEARCHZUT::SortMat (HWND hWnd)
{
   	   qsort (szuttab, zutanz, sizeof (struct SZUT),
				   sortmat);
       somat *= -1;
}


int SEARCHZUT::sorta (const void *elem1, const void *elem2)
{
	      struct SZUT *el1; 
	      struct SZUT *el2; 

		  el1 = (struct SZUT *) elem1;
		  el2 = (struct SZUT *) elem2;
          clipped (el1->a);
          clipped (el2->a);

          if (ratod (el1->a) > ratod (el2->a))
          {
              return 1 * soa;
          }
          else if (ratod (el1->a) < ratod (el2->a))
          {
              return -1 * soa;
          }

          return 0;
}

void SEARCHZUT::SortA (HWND hWnd)
{
   	   qsort (szuttab, zutanz, sizeof (struct SZUT),
				   sorta);
       soa *= -1;
}

int SEARCHZUT::sorta_bz1 (const void *elem1, const void *elem2)
{
	      struct SZUT *el1; 
	      struct SZUT *el2; 

		  el1 = (struct SZUT *) elem1;
		  el2 = (struct SZUT *) elem2;
          clipped (el1->a_bz1);
          clipped (el2->a_bz1);
          if (strlen (el1->a_bz1) == 0 &&
              strlen (el2->a_bz1) == 0)
          {
              return 0;
          }
          if (strlen (el1->a_bz1) == 0)
          {
              return -1;
          }
          if (strlen (el2->a_bz1) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->a_bz1,el2->a_bz1) * soa_bz1);
}

void SEARCHZUT::SortABz1 (HWND hWnd)
{
   	   qsort (szuttab, zutanz, sizeof (struct SZUT),
				   sorta_bz1);
       soa_bz1 *= -1;
}


void SEARCHZUT::SortLst (int Col, HWND hWnd)
/**
Procedure zum Sortiern der Liste.
**/
{
       switch (Col)
       {
              case 0 :
                  SortA (hWnd);
                  SearchField = 0;
                  break;
              case 1 :
                  SortMat (hWnd);
                  SearchField = 1;
                  break;
              case 2 :
                  SortABz1 (hWnd);
                  SearchField = 2;
                  break;
              default :
                  return;
       }
       UpdateList ();
}

void SEARCHZUT::FillFormat (char *buffer, int size)
{
        DlgLst::FillFrmAttr (&DataForm, buffer, size);
}


void SEARCHZUT::FillCaption (char *buffer, int size)
{
        DlgLst::FillFrmTitle (&UbForm, buffer, 0, size);
}

void SEARCHZUT::FillVlines (char *buffer, int size) 
{
        DlgLst::FillFrmLines (&LineForm, buffer, 0, size);
}


void SEARCHZUT::FillRec (char *buffer, int i, int size)
{
          DlgLst::FillFrmData (&DataForm, buffer, 0, size);
}


void SEARCHZUT::UpdateList (void)
{
       int i;
 	   char buffer [512];

       for (i = 0; i < zutanz; i ++)
       {
		  memcpy (&szut, &szuttab[i],  sizeof (struct SZUT));
          FillRec (buffer, i, 512);
	      Query->UpdateRecord (buffer, i);
       }
}

int SEARCHZUT::SearchLst (char *sebuff)
/**
Nach Kurzname in Liste suchen.
**/
{
/*
	   Query->SearchList ();
	   return 0;
*/
	   int i;
	   int len;
       char sabuff [80];
       char *pos;

	   if (szuttab == NULL) return 0;
	   if (strlen (sebuff) == 0) 
	   {
	       Query->SetSel (0);
		   return 0;
	   }

	   for (i = 0; i < zutanz; i ++)
	   {
		   len = min (16, strlen (sebuff));
           if (SearchField == 0)
           {
                   strcpy (sabuff, szuttab[i].a);
                   for (pos = sabuff; *pos <= ' ' && *pos != 0 ; pos ++);
                   pos[len] = 0;
                   strcpy (sabuff, pos);
                   if (ratod (sebuff) == ratod (sabuff)) break;
           }
           else if (SearchField == 1)
           {
                   strcpy (sabuff, szuttab[i].mat);
                   for (pos = sabuff; *pos <= ' ' && *pos != 0 ; pos ++);
                   pos[len] = 0;
                   strcpy (sabuff, pos);
                   if (atol (sebuff) == atol (sabuff)) break;
           }
           else if (SearchField == 2)
           {
		           if (strupcmp (sebuff, szuttab[i].a_bz1, len) == 0) break;
           }
	   }
	   if (i == zutanz) return 0;
	   Query->SetSel (i);
	   return 0;
}


int SEARCHZUT::Read (char *squery)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  char a_bz2 [25];
	  int cursor;
	  int i;
      WMESS Wmess;


	  if (szuttab) 
	  {
           delete szuttab;
           szuttab = NULL;
	  }


      idx = -1;
	  zutanz = 0;
      SearchField = 2;
      Query->SetSBuff (NULL);
      clipped (squery);
      if (strcmp (squery, " ") == 0) squery[0] = 0;

      if (zutanz == 0) zutanz = 0x10000;

	  szuttab = new struct SZUT [zutanz];
      if (squery == NULL || 
          strlen (squery) == 0)
      {
             sprintf (buffer, "select a_bas.a, a_mat.mat, a_bz1, a_bz2 "
	 			       "from a_mat, a_bas "
                       "where mat > 0 "
                       "and (a_bas.a_typ = 6 or "
                       "     a_bas.a_typ2 = 6) "
                       "and a_bas.a = a_mat.a"); 
              if (query != NULL && strlen (query) > 0)
              {
                  strcat (buffer, " ");
                  strcat (buffer, query);
              }

              DbClass.sqlout ((char *) szut.a, 0, 14);
              DbClass.sqlout ((char *) szut.mat, 0, 9);
              DbClass.sqlout ((char *) szut.a_bz1, 0, 25);
              DbClass.sqlout ((char *) a_bz2, 0, 25);
              cursor = DbClass.sqlcursor (buffer);
      }
      else
      {
             sprintf (buffer, "select a_bas.a, a_mat.mat, a_bz1, a_bz2 "
	 			       "from a_mat, a_bas "
                       "where mat > 0 "
                       "and a_bas.a = a_mat.a " 
                       "and (a_bas.a_typ = 6 or "
                       "     a_bas.a_typ2 = 6) "
//                       "and rez_bz matches \"%s*\"", squery); 
						"and (a_bz1 matches \"%s*\" or a_bz2 matches \"%s*\")", squery,squery);
              if (query != NULL && strlen (query) > 0)
              {
                  strcat (buffer, " ");
                  strcat (buffer, query);
              }
              DbClass.sqlout ((char *) szut.a, 0, 14);
              DbClass.sqlout ((char *) szut.mat, 0, 9);
              DbClass.sqlout ((char *) szut.a_bz1, 0, 25);
              DbClass.sqlout ((char *) a_bz2, 0, 25);
              cursor = DbClass.sqlcursor (buffer);
      }
      if (cursor < 0) 
      {
          return -1;
      }

  	  i = 0;
	  while (DbClass.sqlfetch (cursor) == 0)
      {
		  sprintf (szut.a_bz1, "%s %s", szut.a_bz1, a_bz2);
		  memcpy (&szuttab[i], &szut, sizeof (struct SZUT));
          FillRec (buffer, i, 512);
	      Query->InsertRecord (buffer);
		  i ++;
	  }
      zutanz = i;
	  DbClass.sqlclose (cursor);

      soa_bz1 = 1;

      if (zutanz > 1)
      {
             SortABz1 (Query->GethWnd ());
      }
      UpdateList ();

	  EnableWindow (awin, FALSE);
	  return 0;
}

void SEARCHZUT::FillBox (void)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int i;

      for (i = 0; i < zutanz; i ++)
      {
		  memcpy (&szut, &szuttab[i],  sizeof (struct SZUT));
          FillRec (buffer, i, 512);
	      Query->InsertRecord (buffer);
      }
      if (idx > -1)
      {
	      Query->SetSel (idx);
      }
}


void SEARCHZUT::SetParams (HINSTANCE hMainInst, HWND hMainWindow)
{
      this->hMainInst   = hMainInst;
      this->hMainWindow = hMainWindow;
      awin = hMainWindow;
}


void SEARCHZUT::Search (void)
{
  	  int cx, cy;
	  char buffer [512];
      RECT rect;
      BOOL doret = FALSE; 

      DlgLst::TChar = 0xFF;
      Settchar ((char) DlgLst::TChar);
      GetClientRect (hMainWindow, &rect);
      cx = 80;
      cy = 25;
//      EnableWindow (hMainWindow, FALSE);
      SetSortProc (SortLst);
      if (Query == NULL)
      {
        idx = -1;
        if (query != NULL && strlen (query) > 0)
        {
               Query = new CHQEX (cx, cy);
        }
        else
        {
			if (Suchvariante == 1)
			{
               Query = new CHQEX (cx, cy, "Name", "");
			}
			else
			{
               Query = new CHQEX (cx, cy);
			}
        }
        Query->EnableSort (TRUE);
        if (SetStyle)
        {
            Query->SetWindowStyle (WindowStyle);
        }
        Query->OpenWindow (hMainInst, hMainWindow, TRUE);
        FillFormat (buffer, 512);
        Query->RowAttr (buffer);
        FillVlines (buffer, 512);
	    Query->VLines (buffer, 3);
	    EnableWindow (awin, FALSE);

        FillCaption (buffer, 512);
	    Query->InsertCaption (buffer);

	    Query->SetFillDb (Read);
	    Query->SetSearchLst (SearchLst);
//        if (query != NULL && strlen (query) > 0)
        {
              Read ("");
              if (zutanz == 0)
              {
                  doret = TRUE;
              }
        }
      }
      else
      {
        Query->RestorefWork ();
        if (SetStyle)
        {
            Query->SetWindowStyle (WindowStyle);
        }
        Query->OpenWindow (hMainInst, hMainWindow, TRUE);
        FillFormat (buffer, 512);
        Query->RowAttr (buffer);
        FillVlines (buffer, 512);
	    Query->VLines (buffer, 3);
	    EnableWindow (awin, FALSE);

        FillCaption (buffer, 512);
	    Query->InsertCaption (buffer);

	    Query->SetFillDb (Read);
	    Query->SetSearchLst (SearchLst);
        FillBox ();
      }
      if (doret == FALSE)
      {
          Query->ProcessMessages ();
          idx = Query->GetSel ();
      }
      else
      {
          idx = -1;
      }
      query = NULL;
      EnableWindow (awin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (awin);
      if (szuttab == NULL)
      {
          delete Query;
          Query = NULL;
      }
	  if (idx == -1) return;
	  memcpy (&szut, &szuttab[idx], sizeof (szut));
      SetSortProc (NULL);
      if (szuttab != NULL)
      {
          Query->SavefWork ();
      }
}



