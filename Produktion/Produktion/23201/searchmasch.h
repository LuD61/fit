#ifndef _SEARCHMASCH_DEF
#define _SEARCHMASCH_DEF
#include "wmaskc.h"
#include "cmask.h"
#include "mo_chqex.h"
#include "dbclass.h"

struct SMASCH
{
        char masch_nr [10];
	  char masch_bz [40];
        char prod_abt [40];
        char masch_typ [40];
};

class SEARCHMASCH
{
       private :
           static ITEM umasch_nr;
           static ITEM umasch_bz;
           static ITEM uprod_abt;
           static ITEM umasch_typ;
           static ITEM imasch_nr;
           static ITEM imasch_bz;
           static ITEM iprod_abt;
           static ITEM imasch_typ;

           static ITEM iline;

           static field _UbForm[];
           static form UbForm;

           static field _DataForm[];
           static form DataForm;

           static field _LineForm[];
           static form LineForm;

           static HINSTANCE hMainInst;
           static HWND hMainWindow;
           static DB_CLASS DbClass; 
           static HWND awin;
           static struct SMASCH *smaschtab;
           static struct SMASCH smasch;
           static int idx;
           static long maschanz;
           static CHQEX *Query;
           static int SearchField;
           static char *query;
           static int somasch_nr; 
           static int somasch_bz; 
           static int soprod_abt; 
           static int somasch_typ; 
           int SearchPos;
           int OKPos;
           int CAPos;
           DWORD WindowStyle;
           BOOL SetStyle;
        public :
           SEARCHMASCH ()
           {
                  SearchPos = 8;
                  OKPos = 9;
                  CAPos = 10;
                  SetStyle = FALSE;
                  WindowStyle = WS_POPUP | WS_THICKFRAME;
           }

           ~SEARCHMASCH ()
           {
                  if (smaschtab != NULL)
                  {
                      delete smaschtab;
                      smaschtab = NULL;
                  }
                  if (Query != NULL)
                  {
                      delete Query;
                      Query = NULL;
                  }
           }

           void DeleteQuery (void)
           {
/*
                  if (smaschtab != NULL)
                  {
                      delete smaschtab;
                      smaschtab = NULL;
                  }
*/
                  if (Query != NULL)
                  {
                      delete Query;
                      Query = NULL;
                  }
           }

           void SetQuery (char *q)
           {
               this->query = q;
/* ???
               if (query == NULL || (strcmp (query, q) != 0))
               {
                      query = q;

                      if (Query != NULL)
                      {
                             delete Query;
                             Query = NULL;
                      }
                      if (smaschtab != NULL)
                      {
                             delete smaschtab;
                             smaschtab = NULL;
                      }
               }   
*/
           }

           char *GetQuery (void)
           {
                return query;
           }

           SMASCH *GetSMasch (void)
           {
               if (idx == -1) return NULL;
               return &smasch;
           }

           void Setawin (HWND awin)
           {
               this->awin = awin;
           }

           void SetWindowStyle (DWORD style)
           {
               WindowStyle = style;
               SetStyle = TRUE;
           }


           void FillBox (void);
           static void FillFormat (char *, int);
           static void FillVlines (char *, int);
           static void FillCaption (char *, int);
           static void FillRec (char *, int, int);
           static int sortmasch (const void *, const void *);
           static void SortMasch (HWND);
           static int sortmasch_bz (const void *, const void *);
           static void SortMaschBz (HWND);
           static int sortprod_abt (const void *, const void *);
           static void SortProdAbt (HWND);
           static int sortmasch_typ (const void *, const void *);
           static void SortMaschTyp (HWND);
           static void SortLst (int, HWND);
           static void UpdateList (void);

           static int SearchLst (char *);
           static int Read (char *);
           void SetParams (HINSTANCE, HWND);
           void Search (void);
};  
#endif