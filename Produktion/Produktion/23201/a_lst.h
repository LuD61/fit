#ifndef _A_LST_DEF
#define _A_LST_DEF
#include "listenter.h"
#include "listclex.h"
#include "work.h"
#include "mo_progcfg.h"

#define SMALL 0
#define BIG 1

class A_LST : public virtual LISTENTER,
                public virtual KEYS,
                public virtual Info
{
         private :
                      static CFIELD *_fChoise0[];
                      static CFORM fChoise0;
                      static CFIELD *_fChoise[];
                      static CFORM fChoise;

                      static struct CHATTR ChAttra [];
                      static struct CHATTR *ChAttr;
                      Work *work;
                      int cursor;
                      CFORM *Toolbar2;
                      BOOL cfgOK;
                      PROG_CFG *ProgCfg;
                      HBITMAP ArrDown;
                      double ekds;
                      BOOL PlanEk;

         public :

                     static struct A_LSTS Lsts;
                     static struct A_LSTS *LstTab;
                     static ITEM unr;
                     static ITEM urez;
                     static ITEM ua_bz1;
                     static ITEM upr_ek;
                     static ITEM upr_plan_ek;
                     static ITEM uvariante;
                     static ITEM uakv;
                     static ITEM ufiller;
                     static field _UbForm[];
                     static form UbForm;

                     static ITEM iline;
                     static field _LineForm[];
                     static form LineForm;

                     static ITEM inr;
                     static ITEM irez;
                     static ITEM ia_bz1;
                     static ITEM ipr_ek;
                     static ITEM ipr_plan_ek;
                     static ITEM ivariante;
                     static ITEM iakv;
                     static field _DataForm[];
                     static form DataForm;
                     static int ubrows[];
                     static int NoRecNrSet;
                     static char *InfoItem;
                     void SetButtonSize (int);

                     void SetToolbar2 (CFORM *Toolbar2)
                     {
                               this->Toolbar2 = Toolbar2;
                     }

                     CFORM *GetToolbar2 (void)
                     {
                               return Toolbar2;
                     }

                     void SetWork (Work *work)
                     {
                         this->work = work;
                         this->work->SetListDlg (this);
                     }

                     Work *GetWork (void)
                     {
                         return work;
                     }

                     A_LST ();
                     ~A_LST ();
                     void SetPlanEk (BOOL);
                     void GetSysPar (void);
                     void GetCfgValues (void);
                     void Setfeld2Removed (BOOL);
                     void Prepare (void);
                     void Open (void);
                     int Fetch (void);
                     void Close (void);

 				     unsigned char *GetDataRec (void);
				     int GetDataRecLen (void);
 				     char *GetListTab (int);
 				     int GetRecanz (void);
                     void WriteRec (int);
                     void SetA_Lsts (int);
                     void InitRow (void);
                     int ToMemory (int);
                     void uebertragen (void);

                     void SetNoRecNr (void);
                     BOOL StopList (void);
                     BOOL OnKey5 (void);
                     BOOL OnKey6 (void);
                     BOOL OnKey7 (void);
                     BOOL OnKey8 (void);
                     BOOL OnKey9 (void);
                     BOOL OnKey10 (void);
                     BOOL OnKey11 (void);
                     BOOL OnKey12 (void);
                     BOOL OnKeyDel (void);
                     BOOL OnKeyInsert (void);
                     BOOL OnKeyUp (void);
                     BOOL OnKeyDown (void);
                     BOOL OnKeyNext (void);
                     BOOL OnKeyPrior (void);
                     BOOL OnCommand (HWND, UINT, WPARAM, LPARAM);
                     BOOL OnPaint (HWND, UINT, WPARAM, LPARAM);
                     BOOL OnMove (HWND, UINT, WPARAM, LPARAM);
                     int AfterRow (int);
                     BOOL TestNr (void);
                     void FillTypKz (int);
                     int AfterCol (char *);
                     int BeforeCol (char *);
                     int BeforeRow (int);
                     void CreateListQuikInfos (HWND);
                     void TestRemoves (void);
                     int  ShowRez (void);
                     static int InfoProc (char **, char *, char *);

                     BOOL CallInfo (HWND, RECT *, char *, char *, DWORD);
                     void CallInfoEx (void);
};
#endif
