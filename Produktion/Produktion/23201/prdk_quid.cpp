#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "wmaskc.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "prdk_quid.h"

struct PRDK_QUID prdk_quid, prdk_quid_null;

void PRDK_QUID_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *)   &prdk_quid.mdn, 1, 0);
            ins_quest ((char *)   prdk_quid.rez,  0, 9);
            ins_quest ((char *)   &prdk_quid.variante,  1, 0);
            ins_quest ((char *)   prdk_quid.quid_kat,  0, 4);
            ins_quest ((char *)   &prdk_quid.quid_nr,  1, 0);

    out_quest ((char *) &prdk_quid.mdn,1,0);
    out_quest ((char *) prdk_quid.rez,0,9);
    out_quest ((char *) &prdk_quid.variante,1,0);
    out_quest ((char *) prdk_quid.quid_kat,0,4);
    out_quest ((char *) &prdk_quid.quid_nr,1,0);
    out_quest ((char *) &prdk_quid.flkg,3,0);
    out_quest ((char *) &prdk_quid.gflkg,3,0);
    out_quest ((char *) &prdk_quid.flproz,3,0);
    out_quest ((char *) &prdk_quid.gflproz,3,0);
    out_quest ((char *) &prdk_quid.fettproz,3,0);
    out_quest ((char *) &prdk_quid.gfettproz,3,0);
    out_quest ((char *) &prdk_quid.feproz,3,0);
    out_quest ((char *) &prdk_quid.gfeproz,3,0);
    out_quest ((char *) &prdk_quid.beffeproz,3,0);
    out_quest ((char *) &prdk_quid.gbeffeproz,3,0);
    out_quest ((char *) &prdk_quid.schwundkg,3,0);
    out_quest ((char *) &prdk_quid.gschwundkg,3,0);
            cursor = prepare_sql ("select prdk_quid.mdn,  "
"prdk_quid.rez,  prdk_quid.variante,  prdk_quid.quid_kat,  "
"prdk_quid.quid_nr,  prdk_quid.flkg,  prdk_quid.gflkg,  "
"prdk_quid.flproz,  prdk_quid.gflproz,  prdk_quid.fettproz,  "
"prdk_quid.gfettproz,  prdk_quid.feproz,  prdk_quid.gfeproz,  "
"prdk_quid.beffeproz,  prdk_quid.gbeffeproz,  prdk_quid.schwundkg,  "
"prdk_quid.gschwundkg from prdk_quid "

#line 25 "prdk_quid.rpp"
                                  "where mdn = ? "
                                  "and   rez = ? "
                                  "and   variante = ? "
                                  "and   quid_kat = ? "
                                  "and   quid_nr = ?");

    ins_quest ((char *) &prdk_quid.mdn,1,0);
    ins_quest ((char *) prdk_quid.rez,0,9);
    ins_quest ((char *) &prdk_quid.variante,1,0);
    ins_quest ((char *) prdk_quid.quid_kat,0,4);
    ins_quest ((char *) &prdk_quid.quid_nr,1,0);
    ins_quest ((char *) &prdk_quid.flkg,3,0);
    ins_quest ((char *) &prdk_quid.gflkg,3,0);
    ins_quest ((char *) &prdk_quid.flproz,3,0);
    ins_quest ((char *) &prdk_quid.gflproz,3,0);
    ins_quest ((char *) &prdk_quid.fettproz,3,0);
    ins_quest ((char *) &prdk_quid.gfettproz,3,0);
    ins_quest ((char *) &prdk_quid.feproz,3,0);
    ins_quest ((char *) &prdk_quid.gfeproz,3,0);
    ins_quest ((char *) &prdk_quid.beffeproz,3,0);
    ins_quest ((char *) &prdk_quid.gbeffeproz,3,0);
    ins_quest ((char *) &prdk_quid.schwundkg,3,0);
    ins_quest ((char *) &prdk_quid.gschwundkg,3,0);
            sqltext = "update prdk_quid set "
"prdk_quid.mdn = ?,  prdk_quid.rez = ?,  prdk_quid.variante = ?,  "
"prdk_quid.quid_kat = ?,  prdk_quid.quid_nr = ?,  "
"prdk_quid.flkg = ?,  prdk_quid.gflkg = ?,  prdk_quid.flproz = ?,  "
"prdk_quid.gflproz = ?,  prdk_quid.fettproz = ?,  "
"prdk_quid.gfettproz = ?,  prdk_quid.feproz = ?,  "
"prdk_quid.gfeproz = ?,  prdk_quid.beffeproz = ?,  "
"prdk_quid.gbeffeproz = ?,  prdk_quid.schwundkg = ?,  "
"prdk_quid.gschwundkg = ? "

#line 32 "prdk_quid.rpp"
                                  "where mdn = ? "
                                  "and   rez = ? "
                                  "and   variante = ? "
                                  "and   quid_kat = ? "
                                  "and   quid_nr = ?";
            ins_quest ((char *)   &prdk_quid.mdn, 1, 0);
            ins_quest ((char *)   prdk_quid.rez,  0, 9);
            ins_quest ((char *)   &prdk_quid.variante,  1, 0);
            ins_quest ((char *)   prdk_quid.quid_kat,  0, 4);
            ins_quest ((char *)   &prdk_quid.quid_nr,  1, 0);

            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &prdk_quid.mdn, 1, 0);
            ins_quest ((char *)   prdk_quid.rez,  0, 9);
            ins_quest ((char *)   &prdk_quid.variante,  1, 0);
            ins_quest ((char *)   &prdk_quid.quid_kat,  0, 4);
            ins_quest ((char *)   &prdk_quid.quid_nr,  1, 0);

            test_upd_cursor = prepare_sql ("select rez from prdk_quid "
                                  "where mdn = ? "
                                  "and   rez = ? "
                                  "and   variante = ? "
                                  "and   quid_kat = ? "
                                  "and   quid_nr = ? "
                                  "for update");

            ins_quest ((char *) &prdk_quid.mdn, 1, 0);
            ins_quest ((char *)   prdk_quid.rez,  0, 9);
            ins_quest ((char *)   &prdk_quid.variante,  1, 0);
            ins_quest ((char *)   prdk_quid.quid_kat,  0, 4);
            ins_quest ((char *)   &prdk_quid.quid_nr,  1, 0);

            del_cursor = prepare_sql ("delete from prdk_quid "
                                  "where mdn = ? "
                                  "and   rez = ? "
                                  "and   variante = ? "
                                  "and   quid_kat = ? "
                                  "and   quid_nr = ?");

    ins_quest ((char *) &prdk_quid.mdn,1,0);
    ins_quest ((char *) prdk_quid.rez,0,9);
    ins_quest ((char *) &prdk_quid.variante,1,0);
    ins_quest ((char *) prdk_quid.quid_kat,0,4);
    ins_quest ((char *) &prdk_quid.quid_nr,1,0);
    ins_quest ((char *) &prdk_quid.flkg,3,0);
    ins_quest ((char *) &prdk_quid.gflkg,3,0);
    ins_quest ((char *) &prdk_quid.flproz,3,0);
    ins_quest ((char *) &prdk_quid.gflproz,3,0);
    ins_quest ((char *) &prdk_quid.fettproz,3,0);
    ins_quest ((char *) &prdk_quid.gfettproz,3,0);
    ins_quest ((char *) &prdk_quid.feproz,3,0);
    ins_quest ((char *) &prdk_quid.gfeproz,3,0);
    ins_quest ((char *) &prdk_quid.beffeproz,3,0);
    ins_quest ((char *) &prdk_quid.gbeffeproz,3,0);
    ins_quest ((char *) &prdk_quid.schwundkg,3,0);
    ins_quest ((char *) &prdk_quid.gschwundkg,3,0);
            ins_cursor = prepare_sql ("insert into prdk_quid ("
"mdn,  rez,  variante,  quid_kat,  quid_nr,  flkg,  gflkg,  flproz,  gflproz,  fettproz,  "
"gfettproz,  feproz,  gfeproz,  beffeproz,  gbeffeproz,  schwundkg,  gschwundkg) "

#line 73 "prdk_quid.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?)");

#line 75 "prdk_quid.rpp"
}
