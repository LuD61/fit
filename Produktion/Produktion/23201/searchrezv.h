#ifndef _SEARCREZV_DEF
#define _SEARCREZV_DEF
#include "wmaskc.h"
#include "cmask.h"
#include "mo_chqex.h"
#include "dbclass.h"

struct SREZV
{
      char rez [10];
	  char rez_bz [73];
      char mat [12];
	  char a [15];
	  char a_bz1 [26];
	  char variante [6];
};

class SEARCHREZV
{
       private :
           static ITEM urez;
           static ITEM urez_bz;
           static ITEM umat;
           static ITEM ua;
           static ITEM ua_bz1;
           static ITEM uvariante;

           static ITEM irez;
           static ITEM irez_bz;
           static ITEM imat;
           static ITEM ia;
           static ITEM ia_bz1;
           static ITEM ivariante;


           static ITEM iline;

           static field _UbForm[];
           static form UbForm;

           static field _DataForm[];
           static form DataForm;

           static field _LineForm[];
           static form LineForm;

           static HINSTANCE hMainInst;
           static HWND hMainWindow;
           static DB_CLASS DbClass; 
           static HWND awin;
           static struct SREZV *srezvtab;
           static struct SREZV srezv;
           static int idx;
           static long rezanz;
           static CHQEX *Query;
           static int SearchField;
           static char *query;
           static int sorez; 
           static int sorez_bz; 
           static int soa; 
           static int soa_bz1; 
           static int sovariante; 
           static int somat; 
           static short mdn;
		   int Suchvariante;

           int SearchPos;
           int OKPos;
           int CAPos;
           DWORD WindowStyle;
           BOOL SetStyle;
        public :
           SEARCHREZV ()
           {
                  SearchPos = 8;
                  OKPos = 9;
                  CAPos = 10;
                  SetStyle = FALSE;
                  mdn = 0;
				  Suchvariante = 0;
                  WindowStyle = WS_POPUP | WS_THICKFRAME;
           }

           ~SEARCHREZV ()
           {
                  if (srezvtab != NULL)
                  {
                      delete srezvtab;
                      srezvtab = NULL;
                  }
                  if (Query != NULL)
                  {
                      delete Query;
                      Query = NULL;
                  }
           }

           void SetQuery (char *q)
           {
               this->query = q;
/* ???
               if (query == NULL || (strcmp (query, q) != 0))
               {
                      query = q;

                      if (Query != NULL)
                      {
                             delete Query;
                             Query = NULL;
                      }
                      if (srezvtab != NULL)
                      {
                             delete srezvtab;
                             srezvtab = NULL;
                      }
               }   
*/
           }

           char *GetQuery (void)
           {
                return query;
           }

           SREZV *GetSRez (void)
           {
               if (idx == -1) return NULL;
               return &srezv;
           }

           void Setawin (HWND awin)
           {
               this->awin = awin;
           }
           void SetVariante (short variante)
           {
               this->Suchvariante = variante;
           }

           void SetWindowStyle (DWORD style)
           {
               WindowStyle = style;
               SetStyle = TRUE;
           }


           void FillBox (void);
           static void FillFormat (char *, int);
           static void FillVlines (char *, int);
           static void FillCaption (char *, int);
           static void FillRec (char *, int, int);
           static int sortrez (const void *, const void *);
           static void SortRez (HWND);
           static int sortrez_bz (const void *, const void *);
           static void SortRezBz (HWND);
           static int sorta (const void *, const void *);
           static void SortA (HWND);
           static int sorta_bz1 (const void *, const void *);
           static void SortABz1 (HWND);
           static int sortmat (const void *, const void *);
           static void SortMat (HWND);
           static int sortvariante (const void *, const void *);
           static void SortVariante (HWND);
           static void SortLst (int, HWND);
           static void UpdateList (void);

           static int SearchLst (char *);
           static int Read (char *);
           void SetParams (HINSTANCE, HWND);
           void Search (short);
};  
#endif