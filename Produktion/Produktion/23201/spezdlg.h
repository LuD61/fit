#ifndef _SPEZDLG_DEF
#define _SPEZDLG_DEF
#include "dlg.h"
#include "itprog.h"
#include "itfont.h"
#include "formfield.h"
#include "searcha.h"
#include "mo_txt.h"
#include "varblst.h"
#include "a_lst.h"
#include "zutlst.h"
#include "huellst.h"
#include "vpklst.h"
#include "kostlst.h"
#include "zeitlst.h"
#include "quidlst.h"

#define ARB_ZEIT 11
#define MDN_CTL 1801
#define FIL_CTL 1802 

#define A_CTL 1804
#define REZ_CTL 1805
#define VARIANTE_CTL 1806
#define AKV_CTL 1899
#define REZ_BZ_CTL 1808
#define VARIANTE_BZ_CTL 1809
#define MANR_CTL 1810
#define REWORK_CTL 1811
#define PHASE_CTL 1900
#define MASCH_CTL 1950
#define MASCHZ_CTL 2000
#define MASCH_BZ_CTL 2050
#define EK_CTL 2056
#define PLAN_EK_CTL 2057
#define A_VON_CTL 2060
#define A_BIS_CTL 2061
#define AG_VON_CTL 2062
#define AG_BIS_CTL 2073
#define ZEIT_C1_CTL 2074
#define ZEIT_M2_CTL 2075
#define KOST_HK_CTL 2076
#define KOST_SK_CTL 2077
#define KOST_FEK_CTL 2078
#define KOST_FVK_CTL 2079
#define SWA_CTL 3051
#define SW_CTL 3052
#define VZEIT_CTL 3053
#define PROD_ABT_CTL 3054

#define BORDER 0
#define TANSPARENT 1
#define DOWN 2
#define NOBORDER 3

#include "Work.h"

#define PARTNER_LST_CTL 1917

#define IDM_CHOISE 2002
#define IDM_QUIK   2003

#define HEADCTL 700
#define POSCTL 701
#define FOOTCTL 702

#define RAISEDBORDER 0
#define HIGHBORDER 1
#define LOWBORDER 2
#define HIGHCOLBORDER 3
#define LOWCOLBORDER 4
#define LINEBORDER 5

#define ENTERHEAD 0
#define ENTERA 1
#define ENTERADATA 2

#define IDM_LIST 3001
#define IDM_DOCKLIST 3003
#define IDM_INSERT 3004
#define IDM_DELALL 5000

class SpezDlg : virtual public DLG 
{
          private :
             static ItProg *After [];
             static ItProg *After1 [];
             static ItProg *AfterZeit [];
             static ItProg *AfterQuid [];
             static ItProg *AfterKost [];
             static ItProg *Before [];
             static ItFont *Font [];
             static ItFont *Font1 [];
             static ItFont *WhiteRdoFont [];
             static ItFont *VarbRdoFont [];
             static ItFont *KostRdoFont [];
             static ItFont *QuidRdoFont [];
             static ItFont *KostSuRdoFont [];
             static ItFont *RdoFont [];
             static ItFont *RdoFont1 [];
             static FORMFIELD *dbfields [];
             static FORMFIELD *dbheadfields [];
             static char *EnableHead[];
             static char *EnablePos [];
             static char *EnablePos1 [];
             static char *EnablePosZeit [];
             static char *EnableKost [];
			 static BitImage ImageDelete;
			 static BitImage ImageInsert;
             static char *ProdAbtCombo[]; 
             int    BorderType;
             static Work work;
             static SEARCHA SearchA;
             static char *HelpName;
//             static double saveda;
             CFORM *Toolbar2;
             MTXT *Mtxt;
             A_LST *A_Lst;
             VARBLST *VarbLst;
             ZUTLST *ZutLst;
             HUELLST *HuelLst;
             VPKLST *VpkLst;
             KOSTLST *KostLst;
             ZEITLST *ZeitLst;
             QUIDLST *QuidLst;

             static SpezDlg *ActiveSpezDlg;
             static char ptwert[];
			 static double SaveChg;
			 static double SaveMe;
             BOOL ListEnded;
             BOOL NewCalc;

          public :

            static HBITMAP SelBmp;
            static COLORREF SysBkColor;
            static BOOL DlgCenterX;
            static BOOL DlgCenterY;
            static char *ProdPhasenTxt[];
            static char ProdPhasenTxt0[][37];
            static char *ProdPhasen[];
            static char *Masch[];
            static char *MaschZeit[];
            static char *MaschBz[];
            static long *PrdkMasch[];
            static short *PrdkMaschZeit[];
            static BOOL LockPage;
            static int Artikelauswahl;
			static char kalkzeit[];
            static int KostStyle;
            static char WordPath[];
            static char DocPath[];
			static char *PrintCommand;

            void SetToolbar2 (CFORM *Toolbar2)
            {
                this->Toolbar2 = Toolbar2;
            }

            CFORM *GetToolbar2 (void)
            {
                return Toolbar2;
            }
 	        SpezDlg (int, int, int, int, char *, int, BOOL);
 	        SpezDlg (int, int, int, int, char *, int, BOOL, int);
 	        SpezDlg (int, int, int, int, char *, int, BOOL, int, int);
 	        void Init (int, int, int, int, char *, int, BOOL);
            void EnablePhasen (BOOL);
            void GenPhasen (void);
            void NotKostBkMode (DWORD);
//            BOOL OnKeyDown (void);
//            BOOL OnKeyUp (void);
            BOOL NextPage (void);
            BOOL PriorPage (void);
            BOOL IsKeyDlgMsg (MSG *, BOOL *);
            BOOL OnPreTranslateMessage (MSG *);
            BOOL OnKey1 (void);
            BOOL OnKey2 (void);
            BOOL OnKey3 (void);
            BOOL OnKey4 (void);
            BOOL OnKey5 (void);
            BOOL OnKey6 (void);
            BOOL OnKey7 (void);
            BOOL OnKey9 (void);
            BOOL OnKey10 (void);
            BOOL OnKey11 (void);
            BOOL OnKey12 (void);
			void RestartProcess (BOOL flgPrint);
            BOOL OnKeyDelete (void);
            BOOL OnKeyInsert (void);
            BOOL OnKeyPrior (void);
            BOOL OnKeyNext (void);
            BOOL OnKeyHome (void);
            BOOL OnKeyEnd (void);
            BOOL OnKeyReturn (void);
            BOOL OnPaint (HWND,HDC,UINT,WPARAM,LPARAM);
            BOOL OnActivate (HWND,UINT,WPARAM,LPARAM);
            BOOL OnCommand (HWND,UINT,WPARAM,LPARAM);
            BOOL OnSysCommand (HWND,UINT,WPARAM,LPARAM);
            BOOL OnMove (HWND,UINT,WPARAM,LPARAM);
            BOOL OnTcnSelChange (HWND,UINT,WPARAM,LPARAM);
            BOOL OnDestroy (HWND,UINT,WPARAM,LPARAM);
            BOOL OnCloseUp (HWND,UINT,WPARAM,LPARAM);
            BOOL OnClicked (HWND,UINT,WPARAM,LPARAM);
            BOOL EnterVarbLst (void);
            BOOL EnterALst (void);
            BOOL EnterZutLst (void);
            BOOL EnterHuelLst (void);
            BOOL EnterVpkLst (void);
            BOOL EnterKostLst (void);
            BOOL EnterZeitLst (void);
            BOOL EnterQuidLst (void);
            void SetKostFields (void);
            void PrintComment (char *);
            void SetWinBackground (COLORREF);
            void ZeitToClipboard (void);
            void ToClipboard (void);
            void Kopieren (void);
            BOOL IsZeitCopy (char *);
            void FromClipboard (void);
            void Help (void);
            BOOL ShowMdn (void);
            BOOL ShowA (void);
            BOOL ShowRez (void);
            BOOL ShowVariante (void);
            BOOL ShowMasch (int);
            void SetSummen (double summegew, double ekes, double kosten, int type,double chgmenge,double swmenge );
            void TestNewCalc (void);
            static void NeuCalc (void);
            void CalcNewKost (void);
            void CalcNewKostBatch (void);
            void Batchlauf (void);
            HWND OpenScrollTabWindow (HANDLE, HWND, char *);
            void CallInfo (void);
            static int WriteRow (void);
            static int DeleteRow (void);
//            static int SaveA (void);
            void CalcPersKosten (void);
            void CalcMeKosten ();
            static void SetPosText (void); 
            static void GetPosText (void); 
            static void setWindowText (short); 
            static int ReadPrdk_k (void);
            static int ReadMdn (void);
            int AListe (void);
            static int FillBz (void);
            int CalcChg (void);
            int KostRecanz (void);
            static int EnterPos (void);
            static void EnableAnteil (bool);
            static int GetPtab (void);
            static void GetCombos (void);
            static void SetCombos (void);
            static int SavefromChg (void);
            static int SavefromChgGew (void);
            static int CalcNeuChgGew (void);
            static int SavefromHKTab (void);
            static int SavefromSKTab (void);
            static int SavefromFEKTab (void);
            static int SavefromFVKTab (void);
            static int SavefromMe (void);
            static int CalcfromChg (void);
            static int GetPutfromHKTab (void);
            static int GetPutfromSKTab (void);
            static int GetPutfromFEKTab (void);
            static int GetPutfromFVKTab (void);
            static int CalcfromMe (void);
            void ReportFill (void);
            bool ArtikelZugewiesen (void);
            void PrintRez (void);
            void ShowKostLst (void);
            void AddRow (CFIELD *[], char *);
            void SetHiKost (char *, char *);
            void SetHiKostRow (int);
            static void SetHiKostRows (char *);
            static void SetHiKostColor (COLORREF);
			static void SetFlgAnteil (short);
			static void SetTabVerpackung (char *);
			static void SetRestartProzess (char *);
			static void SetTabArbeitszeit (char *);
			static void SetTabZutaten (char *);
			static void SetTabQuid (char *);
			static void SetPreisHerkunft (char *);
			static void SetPlanPreisHerkunft (char *);
			static void SetMaschinen (char *);
			static void SetRework (char *);
			static void SetSummenAnzeige (char *);
			static void SetRestartProcess (char *);
			static short GetTabVerpackung(void);
			static short GetFlgAnteil(void);
			static short GetTabArbeitszeit(void);
			static short GetTabZutaten(void);
			static short GetTabQuid(void);
			static short GetMaschinen(void);
			static short GetRework(void);
			static void GetFlgBasis(char *);
			static short GetPreisHerkunft(void);
			static int GetPlanPreisHerkunft(void);
			void SetTabStack(short,short,short,short,short,short,short);
};
#endif
