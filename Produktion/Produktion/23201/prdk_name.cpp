#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "wmaskc.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "prdk_name.h"

struct PRDK_NAME prdk_name, prdk_name_null;

void PRDK_NAME_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *)   &prdk_name.mdn, 1, 0);
            ins_quest ((char *)   prdk_name.rez,   0, 72);
    out_quest ((char *) &prdk_name.mdn,1,0);
    out_quest ((char *) prdk_name.rez,0,9);
    out_quest ((char *) prdk_name.rez_bz,0,73);
    out_quest ((char *) &prdk_name.delstatus,1,0);
            cursor = prepare_sql ("select prdk_name.mdn,  "
"prdk_name.rez,  prdk_name.rez_bz,  prdk_name.delstatus from prdk_name "

#line 21 "prdk_name.rpp"
                                  "where mdn = ? "
                                  "and   rez = ? ");
    ins_quest ((char *) &prdk_name.mdn,1,0);
    ins_quest ((char *) prdk_name.rez,0,9);
    ins_quest ((char *) prdk_name.rez_bz,0,73);
    ins_quest ((char *) &prdk_name.delstatus,1,0);
            sqltext = "update prdk_name set "
"prdk_name.mdn = ?,  prdk_name.rez = ?,  prdk_name.rez_bz = ?,  "
"prdk_name.delstatus = ? "

#line 24 "prdk_name.rpp"
                                  "where mdn = ? "
                                  "and   rez = ? ";
            ins_quest ((char *)   &prdk_name.mdn, 1, 0);
            ins_quest ((char *)   prdk_name.rez,   0, 72);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *)   &prdk_name.mdn, 1, 0);
            ins_quest ((char *)   prdk_name.rez,   0, 72);
            test_upd_cursor = prepare_sql ("select rez from prdk_name "
                                  "where mdn = ? "
                                  "and   rez = ? "
                                  "for update");

            ins_quest ((char *)   &prdk_name.mdn, 1, 0);
            ins_quest ((char *)   prdk_name.rez,   0, 72);
            del_cursor = prepare_sql ("delete from prdk_name "
                                  "where mdn = ? "
                                  "and   rez = ? ");
    ins_quest ((char *) &prdk_name.mdn,1,0);
    ins_quest ((char *) prdk_name.rez,0,9);
    ins_quest ((char *) prdk_name.rez_bz,0,73);
    ins_quest ((char *) &prdk_name.delstatus,1,0);
            ins_cursor = prepare_sql ("insert into prdk_name ("
"mdn,  rez,  rez_bz,  delstatus) "

#line 43 "prdk_name.rpp"
                                      "values "
                                      "(?,?,?,?)"); 

#line 45 "prdk_name.rpp"
}
