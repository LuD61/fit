#include <windows.h>
#include "wmaskc.h"
#include "searchvariante.h"
#include "strfkt.h"
#include "mo_wmess.h"
#include "lbox.h"
#include "dlglst.h"
#include "a_bas.h"
#include "spezdlg.h"
#include "lbox.h"
#include "ptab.h"


struct SVARIANTE *SEARCHVARIANTE::svariantetab = NULL;
struct SVARIANTE SEARCHVARIANTE::svariante;
int SEARCHVARIANTE::idx = -1;
long SEARCHVARIANTE::varanz;
CHQEX *SEARCHVARIANTE::Query = NULL;
DB_CLASS SEARCHVARIANTE::DbClass; 
HINSTANCE SEARCHVARIANTE::hMainInst;
HWND SEARCHVARIANTE::hMainWindow;
HWND SEARCHVARIANTE::awin;
int SEARCHVARIANTE::SearchField = 1;
int SEARCHVARIANTE::sovariante = 1; 
int SEARCHVARIANTE::sovariante_bz = 1; 
int SEARCHVARIANTE::soactive = 1; 
short SEARCHVARIANTE::mdn = 0; 


ITEM SEARCHVARIANTE::uvariante      ("variante",    "Variante",            "", 0);  
ITEM SEARCHVARIANTE::uvariante_bz   ("variante_bz",    "Bezeichnung",         "", 0);  
ITEM SEARCHVARIANTE::uactive        ("prod_abt",    "Aktiv",   "", 0);  


field SEARCHVARIANTE::_UbForm[] = {
&uvariante,       10, 0, 0,  0,  NULL, "", BUTTON, 0, 0, 0,     
&uvariante_bz,    26, 0, 0, 10 , NULL, "", BUTTON, 0, 0, 0,     
&uactive,          6, 0, 0, 36 , NULL, "", BUTTON, 0, 0, 0,     
};

form SEARCHVARIANTE::UbForm = {3, 0, 0, _UbForm, 0, 0, 0, 0, NULL};


ITEM SEARCHVARIANTE::ivariante     ("variante",    svariante.variante,      "", 0);  
ITEM SEARCHVARIANTE::ivariante_bz  ("variante_bz", svariante.variante_bz,   "", 0);  
ITEM SEARCHVARIANTE::iactive       ("active",      svariante.active,        "", 0);  

field SEARCHVARIANTE::_DataForm[] = {
&ivariante,     2, 0, 0,  4,  NULL, "%2d",    DISPLAYONLY, 0, 0, 0,     
&ivariante_bz, 24, 0, 0, 11 , NULL, "",       DISPLAYONLY, 0, 0, 0,     
&iactive,       4, 0, 0, 38 , NULL, "",       DISPLAYONLY, 0, 0, 0,     
};

form SEARCHVARIANTE::DataForm = {3, 0, 0, _DataForm, 0, 0, 0, 0, NULL};

ITEM SEARCHVARIANTE::iline ("", "1", "", 0);

field SEARCHVARIANTE::_LineForm[] = {
&iline,       1, 0, 0, 10 , NULL, "", DISPLAYONLY, 0, 0, 0,     
&iline,       1, 0, 0, 36 , NULL, "", DISPLAYONLY, 0, 0, 0,     
&iline,       1, 0, 0, 42 , NULL, "", DISPLAYONLY, 0, 0, 0,     
};

form SEARCHVARIANTE::LineForm = {3, 0, 0, _LineForm, 0, 0, 0, 0, NULL};

char *SEARCHVARIANTE::query = NULL;

int SEARCHVARIANTE::sortvariante (const void *elem1, const void *elem2)
{
	      struct SVARIANTE *el1; 
	      struct SVARIANTE *el2; 

		  el1 = (struct SVARIANTE *) elem1;
		  el2 = (struct SVARIANTE *) elem2;
	      return (atoi (el1->variante) - atoi (el2->variante)) * sovariante;
}

void SEARCHVARIANTE::SortVariante (HWND hWnd)
{
   	   qsort (svariantetab, varanz, sizeof (struct SVARIANTE),
				   sortvariante);
       sovariante *= -1;
}

int SEARCHVARIANTE::sortvariante_bz (const void *elem1, const void *elem2)
{
	      struct SVARIANTE *el1; 
	      struct SVARIANTE *el2; 

		  el1 = (struct SVARIANTE *) elem1;
		  el2 = (struct SVARIANTE *) elem2;
          clipped (el1->variante_bz);
          clipped (el2->variante_bz);
          if (strlen (el1->variante_bz) == 0 &&
              strlen (el2->variante_bz) == 0)
          {
              return 0;
          }
          if (strlen (el1->variante_bz) == 0)
          {
              return -1;
          }
          if (strlen (el2->variante_bz) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->variante_bz,el2->variante_bz) * sovariante_bz);
}

void SEARCHVARIANTE::SortVarianteBz (HWND hWnd)
{
   	   qsort (svariantetab, varanz, sizeof (struct SVARIANTE),
				   sortvariante_bz);
       sovariante_bz *= -1;
}

int SEARCHVARIANTE::sortactive (const void *elem1, const void *elem2)
{
	      struct SVARIANTE *el1; 
	      struct SVARIANTE *el2; 

		  el1 = (struct SVARIANTE *) elem1;
		  el2 = (struct SVARIANTE *) elem2;
          clipped (el1->active);
          clipped (el2->active);
          if (strlen (el1->active) == 0 &&
              strlen (el2->active) == 0)
          {
              return 0;
          }
          if (strlen (el1->active) == 0)
          {
              return -1;
          }
          if (strlen (el2->active) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->active,el2->active) * soactive);
}

void SEARCHVARIANTE::SortActive (HWND hWnd)
{
   	   qsort (svariantetab, varanz, sizeof (struct SVARIANTE),
				   sortactive);
       soactive *= -1;
}


void SEARCHVARIANTE::SortLst (int Col, HWND hWnd)
/**
Procedure zum Sortiern der Liste.
**/
{
       switch (Col)
       {
              case 0 :
                  SortVariante (hWnd);
                  SearchField = 0;
                  break;
              case 1 :
                  SortVarianteBz (hWnd);
                  SearchField = 1;
                  break;
              case 2 :
                  SortActive (hWnd);
                  SearchField = 2;
                  break;
              default :
                  return;
       }
       UpdateList ();
}

void SEARCHVARIANTE::FillFormat (char *buffer, int size)
{
        DlgLst::FillFrmAttr (&DataForm, buffer, size);
}


void SEARCHVARIANTE::FillCaption (char *buffer, int size)
{
        DlgLst::FillFrmTitle (&UbForm, buffer, 0, size);
}

void SEARCHVARIANTE::FillVlines (char *buffer, int size) 
{
        DlgLst::FillFrmLines (&LineForm, buffer, 0, size);
}


void SEARCHVARIANTE::FillRec (char *buffer, int i, int size)
{
          DlgLst::FillFrmData (&DataForm, buffer, 0, size);
}


void SEARCHVARIANTE::UpdateList (void)
{
       int i;
 	   char buffer [512];

       for (i = 0; i < varanz; i ++)
       {
		  memcpy (&svariante, &svariantetab[i],  sizeof (struct SVARIANTE));
          FillRec (buffer, i, 512);
	      Query->UpdateRecord (buffer, i);
       }
}

int SEARCHVARIANTE::SearchLst (char *sebuff)
/**
Nach Kurzname in Liste suchen.
**/
{
/*
	   Query->SearchList ();
	   return 0;
*/

	   int i;
	   int len;
//       char sabuff [80];
//       char *pos;

	   if (svariantetab == NULL) return 0;
	   if (strlen (sebuff) == 0) 
	   {
	       Query->SetSel (0);
		   return 0;
	   }

	   for (i = 0; i < varanz; i ++)
	   {
		   len = min (16, strlen (sebuff));
           if (SearchField == 0)
           {
		           if (strupcmp (sebuff, svariantetab[i].variante, len) == 0) break;
           }
           else if (SearchField == 1)
           {
		           if (strupcmp (sebuff, svariantetab[i].variante_bz, len) == 0) break;
           }
           else if (SearchField == 2)
           {
		           if (strupcmp (sebuff, svariantetab[i].active, len) == 0) break;
           }
	   }
	   if (i == varanz) return 0;
	   Query->SetSel (i);
	   return 0;
}


int SEARCHVARIANTE::Read (char *squery)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int cursor;
	  int i;
      WMESS Wmess;
      PTAB_CLASS Ptab;


	  if (svariantetab) 
	  {
           delete svariantetab;
           svariantetab = NULL;
	  }


      idx = -1;
	  varanz = 0;
      SearchField = 1;
      Query->SetSBuff (NULL);
      clipped (squery);
      if (strcmp (squery, " ") == 0) squery[0] = 0;

      if (varanz == 0) varanz = 0x10000;

	  svariantetab = new struct SVARIANTE [varanz];
      if (squery == NULL || 
          strlen (squery) == 0)
      {
             sprintf (buffer, "select variante, variante_bz, akv "
	 			       "from prdk_k "
                       "where variante >= 0 and mdn = %hd ",atoi(prdk_kf.mdn));
              if (query != NULL && strlen (query) > 0)
              {
                  strcat (buffer, " ");
                  strcat (buffer, query);
              }

      }
      else
      {
             sprintf (buffer, "select variante, variante_bz, akv "
	 			       "from prdk_k "
                       "where variante_bz matches \"%s*\" and mdn = %hd ", squery,atoi(prdk_kf.mdn)); 
              if (query != NULL && strlen (query) > 0)
              {
                  strcat (buffer, " ");
                  strcat (buffer, query);
              }
      }
      DbClass.sqlout ((char *) svariante.variante, 0, 5);
      DbClass.sqlout ((char *) svariante.variante_bz, 0, 25);
      DbClass.sqlout ((char *) svariante.active, 0, 5);
      cursor = DbClass.sqlcursor (buffer);
      if (cursor < 0) 
      {
          return -1;
      }

  	  i = 0;
	  while (DbClass.sqlfetch (cursor) == 0)
      {

 		  if (atoi (svariante.active) == 1)
          {
			          IMG::getImageNr (svariante.active, 1); 
 		  }
		  else
          {
			          strcpy (svariante.active, " ");
 		  }

		  memcpy (&svariantetab[i], &svariante, sizeof (struct SVARIANTE));
          FillRec (buffer, i, 512);
	      Query->InsertRecord (buffer);
		  i ++;
	  }
      varanz = i;
	  DbClass.sqlclose (cursor);

      sovariante = 1;
      if (varanz > 1)
      {
             SortVariante (Query->GethWnd ());
      }
      UpdateList ();

	  EnableWindow (awin, FALSE);
	  return 0;
}

void SEARCHVARIANTE::FillBox (void)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int i;

      for (i = 0; i < varanz; i ++)
      {
		  memcpy (&svariante, &svariantetab[i],  sizeof (struct SVARIANTE));
          FillRec (buffer, i, 512);
	      Query->InsertRecord (buffer);
      }
      if (idx > -1)
      {
	      Query->SetSel (idx);
      }
}


void SEARCHVARIANTE::SetParams (HINSTANCE hMainInst, HWND hMainWindow)
{
      this->hMainInst   = hMainInst;
      this->hMainWindow = hMainWindow;
      awin = hMainWindow;
}


void SEARCHVARIANTE::Search (short mdn)
{
  	  int cx, cy;
	  char buffer [512];
      RECT rect;
      BOOL doret = FALSE; 

	  Sel = LoadBitmap (hMainInst, "Sel2");
	  Msk = LoadBitmap (hMainInst, "Msk2");
      DlgLst::TChar = 0xFF;
      Settchar ((char) DlgLst::TChar);
      GetClientRect (hMainWindow, &rect);
      cx = 50;
      cy = 18;
      SetSortProc (SortLst);
      if (Query == NULL)
      {
        idx = -1;
        if (query != NULL && strlen (query) > 0)
        {
               Query = new CHQEX (cx, cy);
        }
        else
        {
               Query = new CHQEX (cx, cy);
//               Query = new CHQEX (cx, cy, "Name", "");
        }
        Query->EnableSort (TRUE);
        Query->AddImage (Sel, Msk);
        if (SetStyle)
        {
            Query->SetWindowStyle (WindowStyle);
        }
        Query->OpenWindow (hMainInst, hMainWindow, FALSE);
        FillFormat (buffer, 512);
        Query->RowAttr (buffer);
        FillVlines (buffer, 512);
	    Query->VLines (buffer, 3);
	    EnableWindow (awin, FALSE);

        FillCaption (buffer, 512);
	    Query->InsertCaption (buffer);

	    Query->SetFillDb (Read);
	    Query->SetSearchLst (SearchLst);
//        if (query != NULL && strlen (query) > 0)
        {
              Read ("");
              if (varanz == 0)
              {
                  doret = TRUE;
              }
        }
      }
      else
      {
        Query->RestorefWork ();
        if (SetStyle)
        {
            Query->SetWindowStyle (WindowStyle);
        }
        Query->OpenWindow (hMainInst, hMainWindow, TRUE);
        FillFormat (buffer, 512);
        Query->RowAttr (buffer);
        FillVlines (buffer, 512);
	    Query->VLines (buffer, 3);
	    EnableWindow (awin, FALSE);

        FillCaption (buffer, 512);
	    Query->InsertCaption (buffer);

	    Query->SetFillDb (Read);
	    Query->SetSearchLst (SearchLst);
        FillBox ();
      }
      if (doret == FALSE)
      {
          Query->ProcessMessages ();
          idx = Query->GetSel ();
      }
      else
      {
          idx = -1;
      }
      query = NULL;
      EnableWindow (awin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (awin);
      if (svariantetab == NULL)
      {
          delete Query;
          Query = NULL;
      }
	  if (idx == -1) return;
	  memcpy (&svariante, &svariantetab[idx], sizeof (svariante));
      SetSortProc (NULL);
      if (svariantetab != NULL)
      {
          Query->SavefWork ();
      }
}



