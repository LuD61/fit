#ifndef _PRDK_HUEL_DEF
#define _PRDK_HUEL_DEF

#include "windows.h"
#include "mo_draw.h"
#include "wmask.h"
#include "dbclass.h"

struct PRDK_HUEL {
   short     mdn;
   double    a;
   long      mat;
   short     bearb_flg;
   char      bearb_info[49];
   double    me;
   short     me_einh;
   double    fuel_gew;
   double    gew;
   short     variante;
   short     typ;
   char      me_buch[2];
};
extern struct PRDK_HUEL prdk_huel, prdk_huel_null;

#line 10 "prdk_huel.rh"

class PRDK_HUEL_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               PRDK_HUEL_CLASS () : DB_CLASS ()
               {
               }
};
#endif
