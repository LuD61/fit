#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include <string.h>
#include "comcthlp.h"
#include "itemc.h"
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_menu.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_intp.h"
#include "inflib.h"
#include "spezdlg.h"
#include "enterfunc.h"
#include "VarbLst.h"
#include "spezdlg.h"
#include "searchrezv.h"
#include "searchvarb.h"
// #include "dlg.h"

#define IDM_CANCEL 3001
#ifndef IDM_WRITE
#define IDM_WRITE  3002
#endif

static int StdSize = STDSIZE;

static int dlgsize    = STDSIZE;
static int ltgraysize = STDSIZE;
static int dlgpossize = STDSIZE;
static int cpsize     = 200;
static BOOL MengeNettoRemoved = FALSE;
static int Suchvariante  = 0;
static int AG_Beilage  = 0;

static mfont dlgfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         GRAYCOL,
                         0,
                         NULL};

char texttext [] = {"Text"};

static ColButton Ctxt    = { texttext,  10, -1, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, -1, -1,
                             NULL,  0, 0,
                             BLACKCOL,
                             LTGRAYCOL,
                             0, 
};


struct VARBLSTS  VARBLST::Lsts;
struct VARBLSTS *VARBLST::LstTab;
char *VARBLST::InfoItem = "a";

CFIELD *VARBLST::_fChoise0 [] = {
    new CFIELD ("cancel", " abbrechen ", 10, 0, 0, 0, NULL, "",
                CBUTTON, 
                IDM_CANCEL, &dlgfont, 0, 0),
    new CFIELD ("write", " speichern ", 10, 0,12, 0, NULL, "",
                CBUTTON, 
                IDM_WRITE, &dlgfont, 0, 0),
    new CFIELD ("show",  "  ansehen  ", 10, 0,24, 0, NULL, "",
                CBUTTON, 
                IDM_CHOISE, &dlgfont, 0, 0),
    new CFIELD ("delete","  l�schen  ", 10, 0,36, 0, NULL, "",
                CBUTTON, 
                IDM_DELETE, &dlgfont, 0, 0),
    new CFIELD ("insert"," einf�gen  ", 10, 0,48, 0, NULL, "",
                CBUTTON, 
                IDM_INSERT, &dlgfont, 0, 0),
    NULL,
};

CFORM VARBLST::fChoise0 (5, _fChoise0);

CFIELD *VARBLST::_fChoise [] = {
    new CFIELD ("fChoise0", (char *) &fChoise0, 0, 0, -1 , -2, NULL, "",
                CFFORM, 
                500, &dlgfont, 0, 0),
    NULL,
};

CFORM VARBLST::fChoise (1, _fChoise);

static char *EkTxt = "EK";
static char *PlanEkTxt = "Plan-EK";


ITEM VARBLST::utyp                ("typ",                "Typ"     ,         "", 0);
ITEM VARBLST::unr                 ("nr",                 "Nummer",           "", 0);
ITEM VARBLST::ua_bz1              ("a_bz1",              "Bezeichnung",      "", 0);  
ITEM VARBLST::ukutf               ("kutf",               "Ktf.",      "", 0);
ITEM VARBLST::uvarb_me            ("varb_me",            "Menge",            "", 0);
ITEM VARBLST::uvarb_gew_nto        ("varb_gew_nto",        "Menge Netto",      "", 0);
ITEM VARBLST::uanteil_proz        ("anteil_proz",        "% Anteil",          "", 0);
ITEM VARBLST::ubasis              ("basis",                "Basis"     ,         "", 0);
ITEM VARBLST::ume_einh            ("me_einh",            "Einheit",          "", 0);
ITEM VARBLST::upr_ek              ("pr_ek",              "Preis",            "", 0);
ITEM VARBLST::ukosten             ("kosten",             "Kosten",           "", 0);
ITEM VARBLST::upreisme             ("preisme",           "Preis x Me",       "", 0);
ITEM VARBLST::utext               ("text",               (char *) &Ctxt,     "", 0);
//ITEM VARBLST::utext               ("text",               "Text",             "", 0);
ITEM VARBLST::ufiller             ("",                   " ",                "",0);

ITEM VARBLST::iline ("", "1", "", 0);

ITEM VARBLST::ityp            ("typ",            Lsts.typ,          "", 0);
ITEM VARBLST::inr             ("nr",             Lsts.nr,           "", 0);
ITEM VARBLST::ia_bz1          ("a_bz1",          Lsts.a_bz1,        "", 0);  
ITEM VARBLST::ikutf           ("kutf",           Lsts.kutf,         "", 0);
ITEM VARBLST::ivarb_me        ("varb_me",        Lsts.varb_me,      "", 0);
ITEM VARBLST::ianteil_proz    ("anteil_proz",    Lsts.anteil_proz,  "", 0);
ITEM VARBLST::ibasis          ("basis",            Lsts.basis,          "", 0);
ITEM VARBLST::ivarb_gew_nto    ("varb_gew_nto",    Lsts.varb_gew_nto,  "", 0);
ITEM VARBLST::ime_einh        ("me_einh",        Lsts.me_einh,      "", 0);
ITEM VARBLST::ipr_ek          ("pr_ek",          Lsts.pr_ek,        "", 0);
ITEM VARBLST::ikosten         ("kosten",         Lsts.kosten,       "", 0);
ITEM VARBLST::ipreisme        ("preisme",        Lsts.preisme,      "", 0);
ITEM VARBLST::itext           ("text",           Lsts.text,         "", 0);
ITEM VARBLST::ichtyp          ("chtyp",          (char *) &Lsts.chtyp,  "", 0);
ITEM VARBLST::ichbasis        ("chbasis",          (char *) &Lsts.chbasis,  "", 0);
ITEM VARBLST::ichnr           ("chnr",           (char *) &Lsts.chnr,   "", 0);

field VARBLST::_UbForm [] = {
&utyp,     17, 0, 0,  6,  NULL, "", BUTTON, 0, 0, 0,     
&unr,      21, 0, 0, 23,  NULL, "", BUTTON, 0, 0, 0,     
&ua_bz1,   22, 0, 0, 44,  NULL, "", BUTTON, 0, 0, 0, 
&ubasis,   11, 0, 0, 66,  NULL, "", BUTTON, 0, 0, 0,     
&ukutf,    12, 0, 0, 77,  NULL, "", BUTTON, 0, 0, 0,     
&uanteil_proz, 10, 0, 0, 89,  NULL, "", BUTTON, 0, 0, 0,     
&uvarb_me, 12, 0, 0, 99,  NULL, "", BUTTON, 0, 0, 0,     
&uvarb_gew_nto, 11, 0, 0, 109,  NULL, "", BUTTON, 0, 0, 0,     
&ume_einh, 10, 0, 0,120,  NULL, "", BUTTON, 0, 0, 0,     
&upr_ek,   11, 0, 0,130,  NULL, "", BUTTON, 0, 0, 0,     
&ukosten,  11, 0, 0,141,  NULL, "", BUTTON, 0, 0, 0,     
&upreisme, 12, 0, 0,152,  NULL, "", BUTTON, 0, 0, 0,     
&utext,    52, 0, 0,163,  NULL, "", COLBUTTON, 0, 0, 0,     
&ufiller, 120, 0, 0,206,  NULL, "", BUTTON, 0, 0, 0,
};


form VARBLST::UbForm = {14, 0, 0, _UbForm, 0, 0, 0, 0, NULL};

field VARBLST::_LineForm [] = {
&iline,   1, 0, 0, 23,  NULL, "",  DISPLAYONLY, 0, 0, 0, 
&iline,   1, 0, 0, 44,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0, 66,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0, 77,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0, 89,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0, 99,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,109,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,120,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,130,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,141,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,152,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,163,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,206,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
};

form VARBLST::LineForm = {13, 0, 0, _LineForm,0, 0, 0, 0, NULL};

field VARBLST::_DataForm [] = {
&ityp,        16,10, 0, 7,   NULL, "",      DISPLAYONLY, 0, 0, 0,     
&inr,         17, 0, 0,24,   NULL, "%8d",   DISPLAYONLY,        0, 0, 0,     
&ichnr,        2, 1, 0,41,   NULL, "",      REMOVED,      0, 0, 0,
&ia_bz1,      20, 0, 0,45,   NULL, "",      DISPLAYONLY, 0, 0, 0, 
&ibasis,      10,10, 0,67,   NULL, "",      DISPLAYONLY, 0, 0, 0,     
&ikutf,       10, 0, 0,78,   NULL, "%2d",   EDIT,        0, 0, 0,     
&ianteil_proz, 10, 0, 0,89,   NULL, "%8.3f", EDIT,        0, 0, 0,
&ivarb_me,    10, 0, 0,99,   NULL, "%8.3f", EDIT,        0, 0, 0,
&ivarb_gew_nto,10, 0, 0, 110,   NULL, "%8.3f", EDIT,        0, 0, 0,
&ime_einh,     8, 0, 0,122,   NULL, "",      DISPLAYONLY, 0, 0, 0,      
&ipr_ek,       9, 0, 0,132,  NULL, "%7.3f", DISPLAYONLY, 0, 0, 0,     
&ikosten,      9, 0, 0,143,  NULL, "%7.3f", DISPLAYONLY, 0, 0, 0,     
&ipreisme,     9, 0, 0,154,  NULL, "%7.3f", DISPLAYONLY, 0, 0, 0,     
&itext,       40, 0, 0,164,  NULL, "80",    EDIT,        0, 0, 0,     
};


form VARBLST::DataForm = {14, 0, 0, _DataForm,0, 0, 0, 0, NULL};

char *VARBLST::TypCombo [] = {"Material",
                              "Rezeptur",
                              "Abschnitte",
                              "Rework",
                               NULL,
};
char *VARBLST::BasisCombo [] = {"Gesamt",
                              "Fleisch u.Fett",
                              "Einlage",
                              "Keine Zugabe",
  							  "behandelt",
							  "Einlage2",
							  "REWORK",
                               NULL,
};


void VARBLST::FillCombo (HWND hWnd, char *ItemName)
{
    if (strcmp (ItemName, "typ") == 0)
    {
       for (int i = 0; TypCombo [i]; i ++)
       {
               SendMessage (hWnd, CB_ADDSTRING, 0, 
                           (LPARAM) TypCombo[i]);
       }
    }
    if (strcmp (ItemName, "basis") == 0)
    {
       for (int i = 0; BasisCombo [i]; i ++)
       {
               SendMessage (hWnd, CB_ADDSTRING, 0, 
                           (LPARAM) BasisCombo[i]);
       }
    }
}


int VARBLST::ubrows [] = {0, 
                          1, 1,
                          2,
                          3, 
                          4,
                          5,
                          6,
                          7,
                          8,
                          9,
                         10, 
                         11, 
                         12, 
                         -1,
};


struct CHATTR VARBLST::ChAttra_rework [] = {
                                     "typ",     DISPLAYONLY,DISPLAYONLY, 
                                     "basis",     COMBOBOX | DROPLIST,COMBOBOX | DROPLIST, 
                                      "nr",     DISPLAYONLY,EDIT,                
                                      "chnr",   REMOVED,BUTTON,                
                                      NULL,  0,           0};
struct CHATTR VARBLST::ChAttra [] = {
                                     "typ",     DISPLAYONLY,COMBOBOX | DROPLIST, 
                                     "basis",     COMBOBOX | DROPLIST,COMBOBOX | DROPLIST, 
                                      "nr",     DISPLAYONLY,EDIT,                
                                      "chnr",   REMOVED,BUTTON,                
                                      NULL,  0,           0};

struct CHATTR VARBLST::ChAttrx [] = {
                                     "typ",     DISPLAYONLY,DISPLAYONLY, 
                                     "basis",   EDIT,EDIT, 
                                      "nr",     DISPLAYONLY,DISPLAYONLY,                
                                      "chnr",   REMOVED,REMOVED,                
                                      "kutf",   EDIT,EDIT,                
									  "anteil_proz", DISPLAYONLY,DISPLAYONLY,
									  "varb_me", DISPLAYONLY,DISPLAYONLY,
									  "varb_gew_bto", DISPLAYONLY,DISPLAYONLY,
									  "text",  DISPLAYONLY,DISPLAYONLY,
                                      NULL,  0,           0};
struct CHATTR *VARBLST::ChAttr = ChAttra;
int VARBLST::NoRecNrSet = FALSE;


void VARBLST::SetPlanEk (BOOL PlanEk)
{
         this->PlanEk = PlanEk;
         
         if (PlanEk)
         {
                upr_ek.SetFeld (PlanEkTxt);
				SetFieldAttr ("pr_ek", EDIT);  //#141205
         }
         else
         {
                upr_ek.SetFeld (EkTxt);
         }
}



BOOL VARBLST::CallInfo (HWND hWnd, RECT *rect, char *Item, char *ItemValue, DWORD WinMode)
/**
Einstieg in Infosystem mit Artikel. Abgeleitet von Klasse Info.
**/
{
 		 char buffer [1024];
         BOOL ret;

         sprintf (buffer, "where a = 2");
         ret = Info::CallInfo (hWnd, rect, "a", buffer, WinMode); 
         SetListFocus ();
         return ret;
}


int VARBLST::InfoProc (char **Item, char *Value, char *where)
/**
Spezielle Procedure fuer Info-System. Alter Ablauf, ist nicht mehr Aktiv 
**/
{

   *Item = "a";


   sprintf (where, "where a = %.0lf", Lsts.a);
   return 1;
}

void VARBLST::CallInfoEx (void)
/*
Info wird aus Dialog aufgerufen.
*/

{
   char where [512];
   
   sprintf (where, "where a = %.0lf", Lsts.a);
   _CallInfoEx (hMainWindow, NULL, InfoItem, where, 0l); 
}

void VARBLST::SetButtonSize (int Size)
{
    int fpos;

/*
    fpos = GetItemPos (&DataForm, "chtyp");
    if (fpos < 0) return;

    if (Size == SMALL)
    {
         DataForm.mask [fpos].length = 2;
         DataForm.mask [fpos].rows = 1;
    }
    else 
    {
         DataForm.mask [fpos].length = 3;
         DataForm.mask [fpos].rows = 0;
    }
*/
    fpos = GetItemPos (&DataForm, "chnr");
    if (fpos < 0) return;

    if (Size == SMALL)
    {
         DataForm.mask [fpos].length = 2;
         DataForm.mask [fpos].rows = 1;
    }
    else 
    {
         DataForm.mask [fpos].length = 3;
         DataForm.mask [fpos].rows = 0;
    }
}


void VARBLST::GetCfgValues (void)
{
       char cfg_v [512];
       COLORREF Color;

       if (ProgCfg == NULL) return;
	   if (cfgOK) return;

       SetFieldAttr ("anteil_proz", DISPLAYONLY);
       cfgOK = TRUE;
       if (ProgCfg->GetCfgValue ("3DList", cfg_v) == TRUE)
       {
                    eListe.Set3D (atoi (cfg_v));
	   }
       if (ProgCfg->GetCfgValue ("Suchvariante", cfg_v) == TRUE)
       {
                 Suchvariante =  (atoi (cfg_v));
	   }
       if (ProgCfg->GetCfgValue ("AG_Beilage", cfg_v) == TRUE)
       {
                 AG_Beilage =  (atoi (cfg_v));
	   }
       if (ProgCfg->GetCfgValue ("3DSize", cfg_v) == TRUE)
       {
                    eListe.SetPlus3D (atoi (cfg_v));
	   }
       if (ProgCfg->GetCfgValue ("ListFocus", cfg_v) == TRUE)
       {
                    ListFocus = atoi (cfg_v);
	   }
        if (ProgCfg->GetCfgValue ("rowheight", cfg_v) == TRUE)
		{
			        RowHeight = ratod (cfg_v);
					if (RowHeight < (double) 1.0)
					{
						RowHeight = (double) 1.0;
					}
		}
        if (ProgCfg->GetCfgValue ("ubheight", cfg_v) == TRUE)
		{
			        UbHeight = atoi (cfg_v);
					SetUbHeight ();
		}
		eListe.SetRowHeight (RowHeight);


        if (ProgCfg->GetCfgValue ("NoRecNr", cfg_v) == TRUE)
        {
			        NoRecNr = atoi (cfg_v);
		            if (atoi (cfg_v))
					{
	                        SetNoRecNr ();
					}
		}
        if (ProgCfg->GetCfgValue ("ListColor", cfg_v) == TRUE)
        {
                     GetListColor (&Color, cfg_v);
		             eListe.SetColor (Color);
        }
        if (ProgCfg->GetCfgValue ("ListBkColor", cfg_v) == TRUE)
        {
                     GetListColor (&Color, cfg_v);
		             eListe.SetBkColor (Color);
        }
        if (ProgCfg->GetCfgValue ("ListLine", cfg_v) == TRUE)
        {
		             eListe.SetLines (GetListLine (cfg_v));
        }
        if (ProgCfg->GetCfgValue ("VarbInfo", cfg_v) == TRUE)
        {
                     InfoItem = new char [80];
                     strcpy (InfoItem, cfg_v);
        }
        if (ProgCfg->GetCfgValue ("MengeNettoRemoved", cfg_v) == TRUE)
        {
                      if (atoi (cfg_v) != 0)
                      {
							 MengeNettoRemoved = TRUE;	
                             DestroyListField ("varb_gew_nto");
                      }
        }
        if (ProgCfg->GetCfgValue ("AnteilProzRemoved", cfg_v) == TRUE)
        {
                      if (atoi (cfg_v) != 0)
                      {
                             DestroyListField ("anteil_proz");
					         ((SpezDlg *) Dlg)->SetFlgAnteil (FALSE);
							 ((SpezDlg *) Dlg)->EnableAnteil(FALSE);

                      }
        }
        if (ProgCfg->GetCfgValue ("PreisMeRemoved", cfg_v) == TRUE)
        {
                      if (atoi (cfg_v) != 0)
                      {
                             DestroyListField ("preisme");
                      }
        }
        if (ProgCfg->GetCfgValue ("TextRemoved", cfg_v) == TRUE)
        {
                      if (atoi (cfg_v) != 0)
                      {
						     strncpy(texttext, "    ",5);
			                 SetFieldAttr ("text", DISPLAYONLY);
//                             DestroyListField ("text");
                      }
        }
}


void VARBLST::TestRemoves (void)
{
        int fpos;

        fpos = GetItemPos (&DataForm, "feld3");
        if (fpos >= 0)
        {
            if (DataForm.mask[fpos].attribut & REMOVED)
            {
                DestroyListField ("feld3");
            }
        }
}

void VARBLST::SetNoRecNr (void)
{
	   int i;

	   eListe.SetNoRecNr (TRUE);
	   if (NoRecNrSet) return;

	   for (i = 0; i < dataform->fieldanz; i ++)
	   {
		   dataform->mask[i].pos[1] -= 6;
	   }
	   for (i = 0; i < ubform->fieldanz; i ++)
	   {
		   ubform->mask[i].pos[1] -= 6;
	   }
	   for (i = 0; i < lineform->fieldanz; i ++)
	   {
		   lineform->mask[i].pos[1] -= 6;
	   }
       NoRecNrSet = TRUE;
}


VARBLST::VARBLST () : KEYS (), LISTENTER () 
{

    ProgCfg = new PROG_CFG ("23201");
	Dlg = NULL;
    cfgOK = FALSE;
	cursor = -1;
    LstTab = new VARBLSTS [0x10000];

    dataform  = &DataForm;
    ubform    = &UbForm;
    lineform  = &LineForm;
    if (sys_ben.berecht > 1 && (sys_ben.berecht < 4))
    {
           DelListField ("pr_ek");
           DelListField ("preisme");
           DelListField ("kosten");
    }
	SetDisplayonly();
    SetFieldLen ("typ", 9);
    SetFieldLen ("basis", 15);
    SetFieldLen ("nr", 15);
    SetFieldLen ("kutf", 6);
    eListe.SetUbRows (ubrows); 
	eListe.SetKeys (this);

//    eListe.SetHscrollStart (2);
//    UbForm.ScrollStart = 2;

    LISTENTER::GetCfgValues ();
    GetCfgValues ();
    if (((SpezDlg *) Dlg)->GetFlgAnteil ())
	{
             SetFieldAttr ("anteil_proz", NORMAL);
			 //TESTTEST
			 SetNewLen ("anteil_proz",10);
	}
	else
	{
             SetFieldAttr ("anteil_proz", DISPLAYONLY);
			 SetNewLen ("anteil_proz",0);
	}

//    GetSysPar ();
    TestRemoves ();
//    if (NoRecNr)
    {
	    SetNoRecNr ();
    }
	fChoise.Setchpos (0, 2);
    work = NULL;
	if (sys_ben.berecht >= 3)
    {
		ChAttr = ChAttrx;

    }
	else
	{	
	    if (prdk_k.rework == 1)
		{
			ChAttr = ChAttra_rework;
		}
		else
		{
			ChAttr = ChAttra;
		}
    }

    eListe.SetChAttr (ChAttr); 
    Toolbar2 = NULL;
    eListe.SetInfoProc (InfoProc);
    eListe.SetInfo (this);
    ArrDown = NULL;
    ArrDown = LoadBitmap (DLG::hInstance, "ARRDOWN");
    summegew == 0.0;
    ekds = 0.0;
    PlanEk = FALSE;
    eListe.SetListclipp (TRUE);
    Ctxt.BkColor = GetSysColor (COLOR_3DFACE);
}


VARBLST::~VARBLST ()
{
	if (work != NULL)
	{
          work->SetListDlg (NULL);
	}
    fChoise.destroy ();
	if (LstTab != NULL)
	{
  	    delete []LstTab;
		LstTab = NULL;
	}
    if (ProgCfg != NULL)
    {
        ProgCfg->CloseCfg ();
        delete ProgCfg;
        ProgCfg = NULL;
    }
    if (work != NULL)
    {
        work->SetVarbLsts (NULL);
    }

}


// Funktionen f�r den Zugriff auf die Daten. Sind als abstrakte Schnittstellen in
// listenter definiert.

unsigned char *VARBLST::GetDataRec (void)
{
    return (unsigned char *) &Lsts;
}

int VARBLST::GetDataRecLen (void)
{
    return sizeof (struct VARBLSTS);
}

char *VARBLST::GetListTab (int i)
{
    return (char *) &LstTab[i];
}

// Ende der Schnittstellen.


void VARBLST::Setfeld2Removed (BOOL removed)
{
    if (removed)
    {
            SetFieldAttr ("feld2", REMOVED);
    }
    else
    {
            SetFieldAttr ("feld2", EDIT);
    }
}

void VARBLST::SetDisplayonly (void)
{
    if (prdk_k.rework == 1)
    {
            SetFieldAttr ("typ", DISPLAYONLY);
	}
	else
	{
            SetFieldAttr ("typ", CEDIT);
	}
    if (sys_ben.berecht >= 3)
    {
            SetFieldAttr ("typ", DISPLAYONLY);
            SetFieldAttr ("basis", DISPLAYONLY);
            SetFieldAttr ("nr", DISPLAYONLY);
            SetFieldAttr ("varb_me", DISPLAYONLY);
            SetFieldAttr ("varb_gew_nto", DISPLAYONLY);
            SetFieldAttr ("anteil_proz", DISPLAYONLY);
//            SetFieldAttr ("kutf", DISPLAYONLY);
            SetFieldAttr ("text", DISPLAYONLY);
    }
 

}

void VARBLST::Prepare (void)
{
    if (work != NULL)
    {
        cursor = work->PrepareVarb ();
    }
}

void VARBLST::Open (void)
{
	if (cursor == -1)
	{
		Prepare ();
		if (cursor == -1) return;
	}
    if (work != NULL)
    {
        work->OpenVarb ();
    }
}

int VARBLST::Fetch (void)
{
	if (cursor == -1)
	{
		Prepare ();
		if (cursor == -1) return 100;
	}
    if (work != NULL)
    {
        return work->FetchVarb ();
    }
	return 100;
}

void VARBLST::CalcSum (void)
{
    summegew = 0.0;
    double summegew_nto = 0.0;
    double summegew_FF = 0.0;
    double summegew_EL = 0.0;
    double summegew_GE = 0.0;
    double summegew_KZ = 0.0;
    double summegew_EL1 = 0.0;
    double summegew_EL2 = 0.0;
    ekds = 0.0;
    double kostds = 0.0;

    for (int i = 0; i < eListe.GetRecanz (); i ++)
    {
        double posgew = ratod (LstTab[i].varb_me) * LstTab[i].a_gew;
        double posgew_nto = ratod (LstTab[i].varb_gew_nto) * LstTab[i].a_gew;
        double posme = ratod (LstTab[i].varb_me);
		switch (LstTab[i].basis_kz) 
		{
		case 1:  //GE Gesamt
			summegew_GE += ratod (LstTab[i].varb_gew_nto) * LstTab[i].a_gew;
			break;
		case 2:  // FF Fett und Fleisch
			summegew_FF += ratod (LstTab[i].varb_gew_nto) * LstTab[i].a_gew;
			break;
		case 3: // EL Einlage
			summegew_EL += ratod (LstTab[i].varb_gew_nto) * LstTab[i].a_gew;
			break;
		case 4: // KZ keine Zugabe
			summegew_KZ += ratod (LstTab[i].varb_gew_nto) * LstTab[i].a_gew;
			break;
		case 5: // Behandelt
			summegew_EL1 += ratod (LstTab[i].varb_gew_nto) * LstTab[i].a_gew;
			break;
		case 6: // EL Einlage
			summegew_EL2 += ratod (LstTab[i].varb_gew_nto) * LstTab[i].a_gew;
			break;
		}

        summegew += work->Round (posgew, 3);
        summegew_nto += work->Round (posgew_nto, 3);
        ekds += LstTab[i].gewek * work->Round (posme, 3); 
        kostds += ratod (LstTab[i].kosten) * work->Round (posme, 3);
		prdk_k.summegew_GE = summegew_GE;
		prdk_k.summegew_FF = summegew_FF;
		prdk_k.summegew_EL = summegew_EL;
		prdk_k.summegew_KZ = summegew_KZ;
		prdk_k.summegew_EL1 = summegew_EL1;
		prdk_k.summegew_EL2 = summegew_EL2;
    }

    double SwProz = prdk_k.sw_kalk / 100;
	double Menge_bto = summegew + work->GetZutGew () + work->GetHuelGew ();
    double SwMe =  ((summegew - summegew_nto) + SwProz *  (Menge_bto)) * -1;

    if (summegew != 0.0)
    {
	    ekds /= (summegew  + work->GetZutGew () + work->GetHuelGew ());
        kostds /= (summegew  + work->GetZutGew () + work->GetHuelGew ());
    }
    ((SpezDlg *) Dlg)->SetSummen (summegew, ekds, kostds, VARB,Menge_bto,SwMe);
    ((SpezDlg *) Dlg)->SetSummen (summegew_nto, ekds, kostds, 999,Menge_bto,SwMe);
}

void VARBLST::ReportFill () 
{

	work->SetGBVorhanden(FALSE);
    for (int i = 0; i < eListe.GetRecanz (); i ++)
    {


	    reportrez.grouprez = 1;
	    reportrez.a = prdk_k.a;
	    strcpy(reportrez.a_bz1, prdk_kf.a_bz1);	
	    strcpy(reportrez.rez ,prdk_k.rez);
	    strcpy(reportrez.rez_bz, prdk_k.rez_bz);
	    reportrez.variante = prdk_k.variante;
	    strcpy(reportrez.variante_bz, prdk_k.variante_bz);
	    if (strcmp(prdk_kf.ek,"J") == 0)  
        {
			strcpy(reportrez.preis_basis, "EK");				
		}
		else
        {
			strcpy(reportrez.preis_basis, "PLAN-EK");			
		}
	    reportrez.schwund = prdk_k.sw_kalk;
	    reportrez.chargengewicht = prdk_k.chg_gew;
	    strcpy(reportrez.datum, "01.01.2003");                      //muss nocht !
	    reportrez.posi = i + 1;
	    reportrez.mat = atoi (LstTab[i].nr);
	    strcpy(reportrez.mat_bz, LstTab[i].a_bz1);
	    reportrez.menge = ratod (LstTab[i].varb_me);
	    strcpy(reportrez.einheit, LstTab[i].me_einh);
	    reportrez.ek = ratod (LstTab[i].pr_ek);
	    reportrez.wert_ek = ratod (LstTab[i].preisme);
	    reportrez.kosten = ratod (LstTab[i].kosten);
	    reportrez.bep = reportrez.ek + reportrez.kosten;
	    reportrez.wert_bep = reportrez.bep * reportrez.menge;
	    strcpy(reportrez.k_a_bez, LstTab[i].kutf);           //Bezeichnung f�r Kosten und Aufschl�ge
	    reportrez.k_diff = 0.0;
	    reportrez.k_diff = ratod (LstTab[i].varb_gew_nto);   //NettoGewicht
	    reportrez.kostenkg = 0.0;
	    reportrez.kostenart = LstTab[i].typ_kz;
		if (reportrez.kostenart == 2)
		{
			work->SetGBVorhanden(TRUE);
		}
	    strcpy(reportrez.kostenart_bz, LstTab[i].basis);
	    reportrez.mdn = prdk_k.mdn;

	    work->ReportRezUpdate ();
    }
}

void VARBLST::Close (void)
{
	if (cursor != -1)
	{
		cursor = -1;
	}
    if (work != NULL)
    {
        work->CloseVarb ();
    }
    CalcSum ();
}



BOOL VARBLST::BeforeCol (char *colname)
{
         return FALSE;
}
         

BOOL VARBLST::TestNr (void)
{
         char me_einh [5]; 
         double pr_ek;
         double a_gew;
         double inh_prod = 1.0;

         switch (Lsts.typ_kz)
         {
               case 1 :
               case ABTYP :
                   work->SetVarbLsts (&Lsts);
                   if (work->GetVarbMatName () == FALSE)
                   {
                       disp_mess ("Falsche Material-Nr", 2);
                       eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
                       eListe.ShowAktRow ();
                       SetListFocus ();
                       return FALSE;
                   }
				   if (AG_Beilage > 0)
				   {
					   if (AG_Beilage == work->GetAG())
					   {
						    for (int i = 0; i < eListe.GetRecanz (); i ++)
							{
								if (strcmp(clipped(LstTab[i].text),"-- Beilage --") == 0)
								{
			                       disp_mess ("Es ist schon eine Beilage vorhanden !", 2);
			                       eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
									eListe.ShowAktRow ();
									SetListFocus ();
									return FALSE;
								}
							}

						   strcpy(Lsts.text, "-- Beilage --");
					   }
				   }

                   if (work->GetVarbRezName () == 1)
				   {
					   if (abfragejn (((SpezDlg *) Dlg)->GethMainWindow (), "Ist dies Material eine Rezeptur ?", "J"))
						{
							strcpy(Lsts.typ,TypCombo[1]);
							Lsts.typ_kz = REZTYP;
						}
				   }
                   if (work->GetVarbRezName () == 10)
				   {
//					   if (abfragejn (((SpezDlg *) Dlg)->GethMainWindow (), "Ist dies Material ein Rework ?", "J"))
//						{
							strcpy(Lsts.typ,TypCombo[3]);
							Lsts.typ_kz = REWORKTYP;
//						}
				   }
                   work->GetVarbAbz1 (Lsts.a_bz1, me_einh, &inh_prod, &a_gew, atol (Lsts.nr));
				   break;
               case REZTYP :
               case REWORKTYP :
                   work->SetVarbLsts (&Lsts);
                   if (work->GetVarbRezName () == 0)
				   {
                       disp_mess ("Falsche Rezeptur-Nr", 2);
                       eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
                       eListe.ShowAktRow ();
                       SetListFocus ();
                       return FALSE;
                   }
                   if (work->GetVarbRezName () == 2)
				   {
					   disp_mess ("Diese Rezeptur ist nicht aktiv !", 2);
                       eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
                       eListe.ShowAktRow ();
                       SetListFocus ();
                       return FALSE;
                   }
                   if (work->GetVarbRezName () == 10)
				   {
						strcpy(Lsts.typ,TypCombo[3]);
						Lsts.typ_kz = REWORKTYP;
				   }

                   if (work->GetVarbMatName () == FALSE)
                   {
                       disp_mess ("Falsche Material-Nr", 2);
                       eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
                       eListe.ShowAktRow ();
                       SetListFocus ();
                       return FALSE;
                   }
	               work->GetRezAbz1 (Lsts.a_bz1, me_einh, &a_gew, atol (Lsts.nr) ); //LuD #130503

                   sprintf (Lsts.varb_me, "%.3lf", work->GetRezChgGew (ratod (Lsts.nr)));
				   if (Lsts.typ_kz == REWORKTYP) sprintf (Lsts.varb_me, "0.000");
				   break;
         }
         if (atoi (me_einh) == 0)
         {
                  strcpy (me_einh, "2");
         }
         work->GetPtab (Lsts.me_einh, "me_einh", me_einh);
         pr_ek = work->GetMatEk (Lsts.a, Lsts.typ_kz,inh_prod,((SpezDlg *) Dlg)->GetPreisHerkunft(),((SpezDlg *) Dlg)->GetPlanPreisHerkunft());   //Lud #130503 typ_kz mit �bergeben 
         if (atoi (me_einh) == 2)
         {
                 a_gew = 1.0;
         }
         Lsts.gewek = pr_ek;
         Lsts.a_gew = a_gew;
         sprintf (Lsts.pr_ek,     "%.3lf",  pr_ek);
//         sprintf (Lsts.kosten,    "%.3lf",  work->GetKostenEinh ());

         double kosteneinh = work->GetKostenEinh ();
         double kostenkg   = work->GetKostenKg ();
         sprintf (Lsts.kosten,    "%.3lf", kostenkg);
		 /* Bgut-Zeugs : muss raus
         if (atoi (me_einh) != 2)
         {
                 if (kosteneinh > 0.0)
                 {
                       sprintf (Lsts.kosten,    "%.3lf", kosteneinh);
                 }
                 else if (kostenkg > 0.0)
                 {
                       sprintf (Lsts.kosten,    "%.3lf", kostenkg * a_gew);
                 }
         }
         else
         {
                 if (kostenkg > 0.0)
                 {
                       sprintf (Lsts.kosten,    "%.3lf", kostenkg);
                 }
                 else if (kosteneinh > 0.0)
                 {
                       sprintf (Lsts.kosten,    "%.3lf", kosteneinh / a_gew);
                 }
         }
		 */

         sprintf (Lsts.kutf, "%d", KtfNext ());
         memcpy (&LstTab[eListe.GetAktRow ()], &Lsts, sizeof (Lsts));
         eListe.ShowAktRow ();
         return TRUE;
}


void VARBLST::FillTypKz (int pos)
{
          clipped (Lsts.typ);

//          Lsts.typ_kz = 1;
          for (int i = 0; TypCombo[i] != NULL; i ++)
          {
              if (strcmp (Lsts.typ, TypCombo [i]) == 0)
              {
                  Lsts.typ_kz = i + 1;
                  break;
              }
          }
          memcpy (&LstTab[pos], &Lsts, sizeof (Lsts));
}
void VARBLST::FillBasisKz (int pos)
{
		  if (Lsts.typ_kz == REWORKTYP) strcpy(Lsts.basis,"Rework"); 
          clipped (Lsts.basis);

          Lsts.basis_kz = 1;
          for (int i = 0; BasisCombo[i] != NULL; i ++)
          {
              if (strcmp (Lsts.basis, BasisCombo [i]) == 0)
              {
                  Lsts.basis_kz = i + 1;
                  break;
              }
          }
          memcpy (&LstTab[pos], &Lsts, sizeof (Lsts));
}

BOOL VARBLST::KtfExist (void)
{
         int Row = eListe.GetAktRow ();
         int Count = eListe.GetRecanz (); 

         long nr = atol (Lsts.nr);
         short kutf = atoi (Lsts.kutf);  
         for (int i = 0; i < Count; i ++)
         {
             if (i == Row) continue;

             if (nr == atol (LstTab[i].nr) &&
                 kutf == atoi (LstTab[i].kutf))
             {
                 return TRUE;
             }
         }
         return FALSE;
}


int VARBLST::KtfNext (void)
{
         int kutf = 0;
         int Count = eListe.GetRecanz (); 
         int Row = eListe.GetAktRow ();

         for (int i = 0; i < Count; i ++)
         {
             if (i == Row) continue;
             if (atoi (LstTab[i].kutf) > kutf)
             {
                 kutf = atoi (LstTab[i].kutf);
             }
         }
         return kutf + 1;
}


BOOL VARBLST::AfterCol (char *colname)
{
/*
         if (strcmp (colname, "chtyp") == 0)
         {
             disp_mess ("Typ gedr�ckt", 2);
             return TRUE;
         }
*/
         
         if (strcmp (colname, "typ") == 0)
         {
             FillTypKz (eListe.GetAktRow ());
             return FALSE;
         }
         else if (strcmp (colname, "basis") == 0)
         {
             FillBasisKz (eListe.GetAktRow ());
             return FALSE;
         }
         else if (strcmp (colname, "chnr") == 0)
         {
             if (eListe.IsNewRec ())
             {
                 switch (Lsts.typ_kz)
                 {
                     case 1 :
                     case ABTYP :
                         ShowNr ();
                         break;
                     case REZTYP :
                     case REWORKTYP :
                         ShowRez (Lsts.typ_kz);
                         break;
                 }
             }
             return TRUE;
         }
         else if (strcmp (colname, "nr") == 0)
         {
             if (TestNr () == FALSE)
             {
                 return TRUE;
             }
         }
         else if (strcmp (colname, "kutf") == 0)
         {
             if (KtfExist ())
             {
                 disp_mess ("Nummer mit dieser Kutterfolge existiert schon",2); 
                 return TRUE;
             }
         }
         else if (strcmp (colname, "anteil_proz") == 0)
         {
            CalcSum ();
			if (ratod(Lsts.anteil_proz) > 0)
			{
				double menge = (ratod(Lsts.anteil_proz) * (prdk_k.varb_gew_nto - ratod(Lsts.varb_gew_nto)) / 100);
				sprintf(Lsts.varb_me,"%8.2lf",menge);
				sprintf(Lsts.varb_gew_nto,"%8.2lf",menge);
			}

             sprintf (Lsts.preisme, "%.3lf", work->Round(ratod (Lsts.pr_ek) *
                                             ratod (Lsts.varb_me),3)); //#090903
             memcpy (&LstTab [eListe.GetAktRow ()], &Lsts, sizeof (Lsts));
             eListe.ShowAktRow ();
         }
         else if (strcmp (colname, "varb_me") == 0)
         {
			 if (Lsts.typ_kz == REWORKTYP)
			 {
				sprintf(Lsts.varb_gew_nto,"0.000");
				sprintf(Lsts.varb_me,"0.000");
	             memcpy (&LstTab [eListe.GetAktRow ()], &Lsts, sizeof (Lsts));
		         eListe.ShowAktRow ();
				return FALSE;
			 }
			 if (Lsts.typ_kz == ABTYP)   
             {
				 if (ratod (Lsts.varb_me) > 0.0)
                 {
					 double dme = ratod (Lsts.varb_me) * -1;
					 sprintf(Lsts.varb_me, "%.3lf", dme);
				 }

			 }
/*   040608 Beim Kochen dann auch eine Gewichtszunahme erfolgen, so dass MengeNetto > MengeBrutto sein kann
			 if (ratod(Lsts.varb_me) < ratod(Lsts.varb_gew_nto))
			 {
			 	sprintf(Lsts.varb_gew_nto,"%s",Lsts.varb_me);
			 }
*/
			 if (MengeNettoRemoved == TRUE)
			 {
				sprintf(Lsts.varb_gew_nto,"%s",Lsts.varb_me);
			 }
             CalcSum ();
             sprintf (Lsts.preisme, "%.3lf", work->Round(ratod (Lsts.pr_ek) *
                                             ratod (Lsts.varb_me),3)); //#090903
             memcpy (&LstTab [eListe.GetAktRow ()], &Lsts, sizeof (Lsts));
             eListe.ShowAktRow ();
         }
         else if (strcmp (colname, "varb_gew_nto") == 0)
         {
			 if (Lsts.typ_kz == REWORKTYP)
			 {
				sprintf(Lsts.varb_gew_nto,"0.000");
				sprintf(Lsts.varb_me,"0.000");
		        memcpy (&LstTab [eListe.GetAktRow ()], &Lsts, sizeof (Lsts));
			     eListe.ShowAktRow ();
				return FALSE;
			 }
			 if (Lsts.typ_kz == ABTYP)   
             {
				 if (ratod (Lsts.varb_gew_nto) > 0.0)
                 {
					 double dme = ratod (Lsts.varb_gew_nto) * -1;
					 sprintf(Lsts.varb_gew_nto, "%.3lf", dme);
				 }

			 }
/*   040608 Beim Kochen dann auch eine Gewichtszunahme erfolgen, so dass MengeNetto > MengeBrutto sein kann
			 if (ratod(Lsts.varb_me) < ratod(Lsts.varb_gew_nto))
			 {
				sprintf(Lsts.varb_gew_nto,"%s",Lsts.varb_me);
			 }
*/
             CalcSum ();
             sprintf (Lsts.preisme, "%.3lf", work->Round(ratod (Lsts.pr_ek) *
                                             ratod (Lsts.varb_me),3)); //#090903
             memcpy (&LstTab [eListe.GetAktRow ()], &Lsts, sizeof (Lsts));
             eListe.ShowAktRow ();
         }
         else if (strcmp (colname, "pr_ek") == 0) //#141205
         {
			 Lsts.gewek = ratod(Lsts.pr_ek);
             CalcSum ();
             sprintf (Lsts.preisme, "%.3lf", work->Round(ratod (Lsts.pr_ek) *
                                             ratod (Lsts.varb_me),3)); //#090903
             memcpy (&LstTab [eListe.GetAktRow ()], &Lsts, sizeof (Lsts));
             eListe.ShowAktRow ();
         }
         return FALSE;
}

int VARBLST::AfterRow (int Row)
{
//         form * DataForm; 

         if (syskey == KEYUP && ratod (Lsts.nr) == 0.0)
         {
                return OnKey7 ();
         }
         memcpy (&LstTab [Row], &Lsts, sizeof (Lsts));
         if (KtfExist ())
         {
                 return -1;
         }
         work->SetVarbLsts (&Lsts);
         CalcSum ();
         return 1;
}

int VARBLST::BeforeRow (int Row)
{
         memcpy (&LstTab [Row], &Lsts, sizeof (Lsts));
         return 1;
}

int VARBLST::ShowNr (void)
{

        SEARCHVARB SearchVarb;
        struct SVARB *svarb;

       SearchVarb.SetWindowStyle (WS_POPUP | WS_THICKFRAME);
       SearchVarb.SetParams (DLG::hInstance, GetParent (hMainWindow));
       SearchVarb.SetVariante (Suchvariante);

        if (Dlg != NULL)
        {
//               SearchVarb.SetParams (DLG::hInstance, ((SpezDlg *) Dlg)->GethWnd ());
               SearchVarb.Setawin (((SpezDlg *) Dlg)->GethMainWindow ());
        }
            
        SearchVarb.Search ();
        if (syskey == KEY5)
        {
            syskey = 0;
            SetListFocus ();
            return 0;
        }
        svarb = SearchVarb.GetSvarb ();
        if (svarb == NULL)
        {
            return 0;
        }

        sprintf (LstTab [eListe.GetAktRow ()].nr, "%ld", atol (svarb->mat));
        memcpy (&Lsts, &LstTab [eListe.GetAktRow ()], sizeof (Lsts));
        TestNr ();
        eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
        eListe.ShowAktRow ();
        SetListFocus ();
        return 0;
}

int VARBLST::ShowRez (int Typ)
{

        SEARCHREZV SearchRez;
        struct SREZV *srez;
        char query [40];

       SearchRez.SetWindowStyle (WS_POPUP | WS_THICKFRAME);
       SearchRez.SetParams (DLG::hInstance, GetParent (hMainWindow));
       SearchRez.SetVariante (Suchvariante);

        if (Dlg != NULL)
        {
//               SearchRez.SetParams (DLG::hInstance, ((SpezDlg *) Dlg)->GethWnd ());
               SearchRez.Setawin (((SpezDlg *) Dlg)->GethMainWindow ());
        }
         
		sprintf (query, "and rez != %s", prdk_k.rez);
		if (Typ == REWORKTYP)  strcat(query," and rework = 1");
        SearchRez.SetQuery (query);
        SearchRez.Search (prdk_k.mdn);
        SearchRez.SetQuery (NULL);
        if (syskey == KEY5)
        {
            syskey = 0;
            SetListFocus ();
            return 0;
        }
        srez = SearchRez.GetSRez ();
        if (srez == NULL)
        {
            return 0;
        }
        

//        sprintf (LstTab [eListe.GetAktRow ()].nr, "%s", srez->rez);
        sprintf (LstTab [eListe.GetAktRow ()].nr, "%d", work->Mat_aus_a(ratod(srez->a)));
        memcpy (&Lsts, &LstTab [eListe.GetAktRow ()], sizeof (Lsts));
        TestNr ();
        eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
        eListe.ShowAktRow ();
        SetListFocus ();
        return 0;
}


BOOL VARBLST::StopList (void)
{
    int i;
    int recanz;
    double chg_gew = 0.0;

    if (AfterRow (eListe.GetAktRow ()) == -1)
    {
        return TRUE;
    }

    if (atol (Lsts.nr) == 0)
    {
        OnKey7 ();
    }

    else
    {
    }

    recanz = eListe.GetRecanz ();

    work->DeleteAllVarbPos ();
    for (i = 0; i < recanz; i ++)
    {
        WriteRec (i);
        chg_gew += prdk_varb.gew_bto;  //Chargengewicht jetzt �ber Brutto-Gewicht 271106
    }

    work->UpdateChgGew (chg_gew + work->GetZutGew ());
//	eListe.BreakList ();
	return TRUE;
}

BOOL VARBLST::OnKeyNext (void)
{
        if (GetKeyState (VK_CONTROL) >= 0)
        {
              if (Dlg != NULL)
              {
                  ((SpezDlg *) Dlg)->NextPage ();
                  return TRUE;
              }
        }
        return FALSE;
}


BOOL VARBLST::OnKeyPrior (void)
{
        if (GetKeyState (VK_CONTROL) >= 0)
        {
              if (Dlg != NULL)
              {
                  ((SpezDlg *) Dlg)->PriorPage ();
                  return TRUE;
              }
        }
        return FALSE;
}


BOOL VARBLST::OnKey5 (void)
{

    OnKey12 ();
    syskey = KEY5;
//	eListe.BreakList ();
	return TRUE;

}

BOOL VARBLST::OnKey6 (void)
{
    eListe.InsertLine ();
    return TRUE;
}

BOOL VARBLST::OnKeyInsert (void)
{
    return FALSE;
/*
    eListe.InsertLine ();
    return TRUE;
*/
}

BOOL VARBLST::OnKey7 (void)
{
    eListe.DeleteLine ();
    CalcSum ();
    return TRUE;
}

BOOL VARBLST::OnKeyDel (void)
{
    return FALSE;
/*
    eListe.DeleteLine ();
    CalcSum ();
    return TRUE;
*/
}

BOOL VARBLST::OnKey8 (void)
{
         return FALSE;
}


BOOL VARBLST::OnKey9 (void)
{
    char * Item = eListe.GetAktItem ();
    if (strcmp (Item, "typ") == 0)
    {
        eListe.ShowDropDown ();
        return TRUE;
    }
    else if (strcmp (Item, "basis") == 0)
    {
        eListe.ShowDropDown ();
        return TRUE;
    }
    else if (strcmp (Item, "nr") == 0)
    {
        switch (Lsts.typ_kz)
        {
             case 1 :
             case ABTYP :
                ShowNr ();
                break;
             case REZTYP :
             case REWORKTYP :
                ShowRez (Lsts.typ_kz);
                break;
        }
        return TRUE;
    }
    return FALSE;
}

/***
BOOL VARBLST::OnKey10 (void)
{
    if (Dlg != NULL)
    {
         work->SetVarbLsts (&Lsts);
// Hier Workfunktion einsetzen
         SetListFocus ();
         return TRUE;
	}
    return FALSE;
}
********/

BOOL VARBLST::OnKey11 (void)
{

    if (Dlg != NULL)
    {
        ((SpezDlg *) Dlg)->OnKey11 ();
        SetAktFocus ();
    }
    return FALSE;
}
BOOL VARBLST::OnKey10 (void)
{

    if (Dlg != NULL)
    {
        ((SpezDlg *) Dlg)->OnKey10 ();
       SetFieldAttr ("anteil_proz", NORMAL);
        eListe.ShowAktRow ();
        SetListFocus ();
        SetAktFocus ();
    }
    return FALSE;
}


int VARBLST::ToMemory (int i)
{

    LISTENTER::ToMemory (i);
    return 0;
}


void VARBLST::InitRow (void)
{

    LISTENTER::InitRow ();


    work->SetVarbLsts (&Lsts);
    work->InitVarbRow ();

/*
    if (ArrDown != NULL && Lsts.chtyp[0] != NULL)
    {
        Lsts.chtyp[0]->SetBitMap (ArrDown);
    }
*/


    if (ArrDown != NULL)
    {
        Lsts.chnr.SetBitMap (ArrDown);
    }


}

void VARBLST::uebertragen (void)
{
    work->SetVarbLsts (&Lsts);
    work->FillVarbRow ();
    if (Lsts.typ_kz <= 0)
    {
          Lsts.typ_kz = 1;
    }
	for (int i = 0; TypCombo[i] != NULL; i ++);
	if (Lsts.typ_kz <= i)
	{
          strcpy (Lsts.typ, TypCombo [Lsts.typ_kz - 1]);
	}
	else
	{
          strcpy (Lsts.typ, TypCombo [0]);
	}

    if (Lsts.basis_kz <= 0)
    {
          Lsts.basis_kz = 1;
    }
	for ( i = 0; BasisCombo[i] != NULL; i ++);
	if (Lsts.basis_kz <= i)
	{
          strcpy (Lsts.basis, BasisCombo [Lsts.basis_kz - 1]);
	}
	else
	{
          strcpy (Lsts.basis, BasisCombo [0]);
	}


    if (ArrDown != NULL)
    {
        Lsts.chtyp.SetBitMap (ArrDown);
        Lsts.chnr.SetBitMap (ArrDown);
        Lsts.chbasis.SetBitMap (ArrDown);
    }
}

void VARBLST::WriteRec (int i)
{
    memcpy (&Lsts, &LstTab[i], sizeof (Lsts));
    if (atol (Lsts.nr) == 0l)
    {
        return;
    }
    FillTypKz (i);
    FillBasisKz (i);
    work->SetVarbLsts (&LstTab[i]);
    work->WriteVarbRec ();
}

BOOL VARBLST::OnKey12 (void)
{
	if (sys_ben.berecht >= 3) return TRUE;
    if (KtfExist ())
    {
           disp_mess ("Nummer mit dieser Kutterfolge existiert schon",2); 
           return TRUE;
    }
    CalcSum ();
    StopList ();
    syskey = KEY12;
    return TRUE;
}


BOOL VARBLST::OnKeyUp (void)
{
//    form *DataForm;
//    int fpos;
/*
    if (Leere Zeile)
    {
        return OnKeyDel ();
    }
*/

    return FALSE;
}

BOOL VARBLST::OnKeyDown (void)
{
    if (atol (Lsts.nr) == 0l)
    {
        return TRUE;
    }

    return FALSE;
}


void VARBLST::CreateListQuikInfos (HWND hWnd)
{
       if (Toolbar2 == NULL) return;

       Toolbar2->GetCfield ("f5")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("f12")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("f9")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("del")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("insert")->CreateQuikInfos (hWnd);
}


BOOL VARBLST::OnCommand (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    if (LOWORD (wParam) == IDM_CHOISE)
    {
        return OnKey9 ();
    }
    if (LOWORD (wParam) == IDM_CANCEL)
    {
        return OnKey5 ();
    }
    if (LOWORD (wParam) == IDM_WRITE)
    {
        return OnKey12 ();
    }
    if (LOWORD (wParam) == VK_DELETE)
    {
        return OnKeyDel ();
    }
    if (LOWORD (wParam) == IDM_DELETE)
    {
        return OnKey7 ();
    }
    if (LOWORD (wParam) == VK_INSERT)
    {
        return OnKeyInsert ();
    }
    if (LOWORD (wParam) == IDM_INSERT)
    {
        return OnKey6 ();
    }
    if (HIWORD (wParam)  == CBN_CLOSEUP)
	{
        eListe.GetFocusText ();
        FillTypKz (eListe.GetAktRow ());
        FillBasisKz (eListe.GetAktRow ());
        if (Dlg != NULL)
        {
	           return ((SpezDlg *) Dlg)->OnCloseUp (hWnd, msg, wParam, lParam);
        }
	}
    if (HIWORD (wParam)  == CBN_DROPDOWN)
	{
        if (strcmp (DataForm.mask[eListe.GetAktColumn ()].item->GetItemName (),
            "nr") == 0)
        {
             ShowNr ();
             eListe.CloseDropDown ();
             return TRUE;
        }
	}
    return FALSE;
}

BOOL VARBLST::OnPaint (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    HDC hdc;

    if (Modal == FALSE) return FALSE;
    if (this->mamain1 != hWnd) return FALSE; 
    
    hdc = BeginPaint (hWnd, &ps);
    fChoise.display (hWnd, hdc);
    EndPaint (hWnd,&ps);
    return FALSE;
}

BOOL VARBLST::OnMove (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{

    if (Modal == FALSE) return FALSE;
    if (this->mamain1 != hWnd) return FALSE; 

    fChoise.MoveWindow ();
    return FALSE;
}








