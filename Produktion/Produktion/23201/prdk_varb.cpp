#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "wmaskc.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "prdk_varb.h"

struct PRDK_VARB prdk_varb, prdk_varb_null;

void PRDK_VARB_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *)   &prdk_varb.mdn, 1, 0);
            ins_quest ((char *)   prdk_varb.rez,  0, 9);
            ins_quest ((char *)   &prdk_varb.mat,  2, 0);
            ins_quest ((char *)   &prdk_varb.variante,  1, 0);
            ins_quest ((char *)   &prdk_varb.kutf,  1, 0);
            ins_quest ((char *)   &prdk_varb.id_nr,  1, 0);

    out_quest ((char *) &prdk_varb.mdn,1,0);
    out_quest ((char *) prdk_varb.rez,0,9);
    out_quest ((char *) &prdk_varb.mat,2,0);
    out_quest ((char *) &prdk_varb.id_nr,1,0);
    out_quest ((char *) &prdk_varb.kutf,1,0);
    out_quest ((char *) prdk_varb.bearb_info,0,81);
    out_quest ((char *) &prdk_varb.varb_me,3,0);
    out_quest ((char *) &prdk_varb.gew_bto,3,0);
    out_quest ((char *) &prdk_varb.gew,3,0);
    out_quest ((char *) &prdk_varb.toler,3,0);
    out_quest ((char *) &prdk_varb.variante,1,0);
    out_quest ((char *) &prdk_varb.typ,1,0);
    out_quest ((char *) &prdk_varb.anteil_proz,3,0);
    out_quest ((char *) prdk_varb.zug_bas,0,3);
            cursor = prepare_sql ("select prdk_varb.mdn,  "
"prdk_varb.rez,  prdk_varb.mat,  prdk_varb.id_nr,  prdk_varb.kutf,  "
"prdk_varb.bearb_info,  prdk_varb.varb_me,  prdk_varb.gew_bto,  "
"prdk_varb.gew,  prdk_varb.toler,  prdk_varb.variante,  prdk_varb.typ,  "
"prdk_varb.anteil_proz,  prdk_varb.zug_bas from prdk_varb "

#line 26 "prdk_varb.rpp"
                                  "where mdn = ? "
                                  "and   rez = ? "
                                  "and   mat = ? "
                                  "and   variante = ? "  
                                  "and   kutf = ? "
                                  "and id_nr = ?");
    ins_quest ((char *) &prdk_varb.mdn,1,0);
    ins_quest ((char *) prdk_varb.rez,0,9);
    ins_quest ((char *) &prdk_varb.mat,2,0);
    ins_quest ((char *) &prdk_varb.id_nr,1,0);
    ins_quest ((char *) &prdk_varb.kutf,1,0);
    ins_quest ((char *) prdk_varb.bearb_info,0,81);
    ins_quest ((char *) &prdk_varb.varb_me,3,0);
    ins_quest ((char *) &prdk_varb.gew_bto,3,0);
    ins_quest ((char *) &prdk_varb.gew,3,0);
    ins_quest ((char *) &prdk_varb.toler,3,0);
    ins_quest ((char *) &prdk_varb.variante,1,0);
    ins_quest ((char *) &prdk_varb.typ,1,0);
    ins_quest ((char *) &prdk_varb.anteil_proz,3,0);
    ins_quest ((char *) prdk_varb.zug_bas,0,3);
            sqltext = "update prdk_varb set "
"prdk_varb.mdn = ?,  prdk_varb.rez = ?,  prdk_varb.mat = ?,  "
"prdk_varb.id_nr = ?,  prdk_varb.kutf = ?,  "
"prdk_varb.bearb_info = ?,  prdk_varb.varb_me = ?,  "
"prdk_varb.gew_bto = ?,  prdk_varb.gew = ?,  prdk_varb.toler = ?,  "
"prdk_varb.variante = ?,  prdk_varb.typ = ?,  "
"prdk_varb.anteil_proz = ?,  prdk_varb.zug_bas = ? "

#line 33 "prdk_varb.rpp"
                                  "where mdn = ? "
                                  "and   rez = ? "
                                  "and   mat = ? "
                                  "and   variante = ? "  
                                  "and   kutf = ? "
                                  "and id_nr = ?";
            ins_quest ((char *)   &prdk_varb.mdn, 1, 0);
            ins_quest ((char *)   prdk_varb.rez,  0, 9);
            ins_quest ((char *)   &prdk_varb.mat,  2, 0);
            ins_quest ((char *)   &prdk_varb.variante,  1, 0);
            ins_quest ((char *)   &prdk_varb.kutf,  1, 0);
            ins_quest ((char *)   &prdk_varb.id_nr,  1, 0);

            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &prdk_varb.mdn, 1, 0);
            ins_quest ((char *)   prdk_varb.rez,  0, 9);
            ins_quest ((char *)   &prdk_varb.mat,  2, 0);
            ins_quest ((char *)   &prdk_varb.variante,  1, 0);
            ins_quest ((char *)   &prdk_varb.kutf,  1, 0);
            ins_quest ((char *)   &prdk_varb.id_nr,  1, 0);

            test_upd_cursor = prepare_sql ("select rez from prdk_varb "
                                  "where mdn = ? "
                                  "and   rez = ? "
                                  "and   mat = ? "
                                  "and   variante = ? "  
                                  "and   kutf = ? "
                                  "and id_nr = ? "
                                  "for update");

            ins_quest ((char *) &prdk_varb.mdn, 1, 0);
            ins_quest ((char *)   prdk_varb.rez,  0, 9);
            ins_quest ((char *)   &prdk_varb.mat,  2, 0);
            ins_quest ((char *)   &prdk_varb.variante,  1, 0);
            ins_quest ((char *)   &prdk_varb.kutf,  1, 0);
            ins_quest ((char *)   &prdk_varb.id_nr,  1, 0);

            del_cursor = prepare_sql ("delete from prdk_varb "
                                  "where mdn = ? "
                                  "and   rez = ? "
                                  "and   mat = ? "
                                  "and   variante = ? "  
                                  "and   kutf = ? "
                                  "and id_nr = ?");

    ins_quest ((char *) &prdk_varb.mdn,1,0);
    ins_quest ((char *) prdk_varb.rez,0,9);
    ins_quest ((char *) &prdk_varb.mat,2,0);
    ins_quest ((char *) &prdk_varb.id_nr,1,0);
    ins_quest ((char *) &prdk_varb.kutf,1,0);
    ins_quest ((char *) prdk_varb.bearb_info,0,81);
    ins_quest ((char *) &prdk_varb.varb_me,3,0);
    ins_quest ((char *) &prdk_varb.gew_bto,3,0);
    ins_quest ((char *) &prdk_varb.gew,3,0);
    ins_quest ((char *) &prdk_varb.toler,3,0);
    ins_quest ((char *) &prdk_varb.variante,1,0);
    ins_quest ((char *) &prdk_varb.typ,1,0);
    ins_quest ((char *) &prdk_varb.anteil_proz,3,0);
    ins_quest ((char *) prdk_varb.zug_bas,0,3);
            ins_cursor = prepare_sql ("insert into prdk_varb ("
"mdn,  rez,  mat,  id_nr,  kutf,  bearb_info,  varb_me,  gew_bto,  gew,  toler,  variante,  "
"typ,  anteil_proz,  zug_bas) "

#line 80 "prdk_varb.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?)"); 

#line 82 "prdk_varb.rpp"
}
