#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "wmaskc.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "kost_art.h"

struct KOST_ART kost_art, kost_art_null;

void KOST_ART_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *)   &kost_art.mdn, 1, 0);
            ins_quest ((char *)   &kost_art.kost_art,  1, 0);
    out_quest ((char *) &kost_art.mdn,1,0);
    out_quest ((char *) &kost_art.fil,1,0);
    out_quest ((char *) &kost_art.kost_art,1,0);
    out_quest ((char *) kost_art.kost_art_bz,0,25);
    out_quest ((char *) &kost_art.delstatus,1,0);
    out_quest ((char *) &kost_art.erl_kto,2,0);
    out_quest ((char *) kost_art.vkost_rechb,0,4);
    out_quest ((char *) &kost_art.vkost_wt,3,0);
    out_quest ((char *) &kost_art.faktor,3,0);
    out_quest ((char *) &kost_art.dat,2,0);
    out_quest ((char *) kost_art.pers_nam,0,9);
            cursor = prepare_sql ("select kost_art.mdn,  "
"kost_art.fil,  kost_art.kost_art,  kost_art.kost_art_bz,  "
"kost_art.delstatus,  kost_art.erl_kto,  kost_art.vkost_rechb,  "
"kost_art.vkost_wt,  kost_art.faktor,  kost_art.dat,  "
"kost_art.pers_nam from kost_art "

#line 21 "kost_art.rpp"
                                  "where mdn = ? "
                                  "and   kost_art = ?");
    ins_quest ((char *) &kost_art.mdn,1,0);
    ins_quest ((char *) &kost_art.fil,1,0);
    ins_quest ((char *) &kost_art.kost_art,1,0);
    ins_quest ((char *) kost_art.kost_art_bz,0,25);
    ins_quest ((char *) &kost_art.delstatus,1,0);
    ins_quest ((char *) &kost_art.erl_kto,2,0);
    ins_quest ((char *) kost_art.vkost_rechb,0,4);
    ins_quest ((char *) &kost_art.vkost_wt,3,0);
    ins_quest ((char *) &kost_art.faktor,3,0);
    ins_quest ((char *) &kost_art.dat,2,0);
    ins_quest ((char *) kost_art.pers_nam,0,9);
            sqltext = "update kost_art set kost_art.mdn = ?,  "
"kost_art.fil = ?,  kost_art.kost_art = ?,  "
"kost_art.kost_art_bz = ?,  kost_art.delstatus = ?,  "
"kost_art.erl_kto = ?,  kost_art.vkost_rechb = ?,  "
"kost_art.vkost_wt = ?,  kost_art.faktor = ?,  kost_art.dat = ?,  "
"kost_art.pers_nam = ? "

#line 24 "kost_art.rpp"
                                  "where mdn = ? "
                                  "and kost_art = ?";
            ins_quest ((char *)   &kost_art.mdn, 1, 0);
            ins_quest ((char *)   &kost_art.kost_art,  1, 0);

            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &kost_art.mdn, 1, 0);
            ins_quest ((char *) &kost_art.kost_art,  1, 0);

            test_upd_cursor = prepare_sql ("select kost_art from kost_art "
                                  "where mdn = ? "
                                  "and kost_art = ? "
                                  "for update");

            ins_quest ((char *) &kost_art.mdn, 1, 0);
            ins_quest ((char *)   &kost_art.kost_art,  1, 0);

            del_cursor = prepare_sql ("delete from kost_art "
                                  "where mdn = ? "
                                  "and kost_art = ?");

    ins_quest ((char *) &kost_art.mdn,1,0);
    ins_quest ((char *) &kost_art.fil,1,0);
    ins_quest ((char *) &kost_art.kost_art,1,0);
    ins_quest ((char *) kost_art.kost_art_bz,0,25);
    ins_quest ((char *) &kost_art.delstatus,1,0);
    ins_quest ((char *) &kost_art.erl_kto,2,0);
    ins_quest ((char *) kost_art.vkost_rechb,0,4);
    ins_quest ((char *) &kost_art.vkost_wt,3,0);
    ins_quest ((char *) &kost_art.faktor,3,0);
    ins_quest ((char *) &kost_art.dat,2,0);
    ins_quest ((char *) kost_art.pers_nam,0,9);
            ins_cursor = prepare_sql ("insert into kost_art ("
"mdn,  fil,  kost_art,  kost_art_bz,  delstatus,  erl_kto,  vkost_rechb,  vkost_wt,  "
"faktor,  dat,  pers_nam) "

#line 47 "kost_art.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?)"); 

#line 49 "kost_art.rpp"
}
