#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "wmaskc.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "a_kalk_eig.h"

struct A_KALK_EIG a_kalk_eig, a_kalk_eig_null;

void A_KALK_EIG_CLASS::prepare (void)
{
            char *sqltext;

//            init_sqlin ();
            ins_quest ((char *)   &a_kalk_eig.mdn, 2, 0);
            ins_quest ((char *)   &a_kalk_eig.fil, 2, 0);
            ins_quest ((char *)   &a_kalk_eig.a,  3, 0);
    out_quest ((char *) &a_kalk_eig.a,3,0);
    out_quest ((char *) &a_kalk_eig.bearb_fil,1,0);
    out_quest ((char *) &a_kalk_eig.bearb_lad,1,0);
    out_quest ((char *) &a_kalk_eig.bearb_sk,1,0);
    out_quest ((char *) &a_kalk_eig.delstatus,1,0);
    out_quest ((char *) &a_kalk_eig.fil,1,0);
    out_quest ((char *) &a_kalk_eig.fil_ek_teilk,3,0);
    out_quest ((char *) &a_kalk_eig.fil_ek_vollk,3,0);
    out_quest ((char *) &a_kalk_eig.hk_teilk,3,0);
    out_quest ((char *) &a_kalk_eig.hk_vollk,3,0);
    out_quest ((char *) &a_kalk_eig.kost,3,0);
    out_quest ((char *) &a_kalk_eig.lad_vk_teilk,3,0);
    out_quest ((char *) &a_kalk_eig.lad_vk_vollk,3,0);
    out_quest ((char *) &a_kalk_eig.mat_o_b,3,0);
    out_quest ((char *) &a_kalk_eig.mdn,1,0);
    out_quest ((char *) &a_kalk_eig.sk_teilk,3,0);
    out_quest ((char *) &a_kalk_eig.sk_vollk,3,0);
    out_quest ((char *) &a_kalk_eig.sp_fil,3,0);
    out_quest ((char *) &a_kalk_eig.sp_lad,3,0);
    out_quest ((char *) &a_kalk_eig.sp_vk,3,0);
    out_quest ((char *) &a_kalk_eig.aend_dat,2,0);
            cursor = prepare_sql ("select a_kalk_eig.a,  "
"a_kalk_eig.bearb_fil,  a_kalk_eig.bearb_lad,  a_kalk_eig.bearb_sk,  "
"a_kalk_eig.delstatus,  a_kalk_eig.fil,  a_kalk_eig.fil_ek_teilk,  "
"a_kalk_eig.fil_ek_vollk,  a_kalk_eig.hk_teilk,  "
"a_kalk_eig.hk_vollk,  a_kalk_eig.kost,  a_kalk_eig.lad_vk_teilk,  "
"a_kalk_eig.lad_vk_vollk,  a_kalk_eig.mat_o_b,  a_kalk_eig.mdn,  "
"a_kalk_eig.sk_teilk,  a_kalk_eig.sk_vollk,  a_kalk_eig.sp_fil,  "
"a_kalk_eig.sp_lad,  a_kalk_eig.sp_vk,  a_kalk_eig.aend_dat from a_kalk_eig "

#line 23 "a_kalk_eig.rpp"
                                  "where mdn = ? "
                                  "and fil = ? "
                                  "and   a   = ?");

    ins_quest ((char *) &a_kalk_eig.a,3,0);
    ins_quest ((char *) &a_kalk_eig.bearb_fil,1,0);
    ins_quest ((char *) &a_kalk_eig.bearb_lad,1,0);
    ins_quest ((char *) &a_kalk_eig.bearb_sk,1,0);
    ins_quest ((char *) &a_kalk_eig.delstatus,1,0);
    ins_quest ((char *) &a_kalk_eig.fil,1,0);
    ins_quest ((char *) &a_kalk_eig.fil_ek_teilk,3,0);
    ins_quest ((char *) &a_kalk_eig.fil_ek_vollk,3,0);
    ins_quest ((char *) &a_kalk_eig.hk_teilk,3,0);
    ins_quest ((char *) &a_kalk_eig.hk_vollk,3,0);
    ins_quest ((char *) &a_kalk_eig.kost,3,0);
    ins_quest ((char *) &a_kalk_eig.lad_vk_teilk,3,0);
    ins_quest ((char *) &a_kalk_eig.lad_vk_vollk,3,0);
    ins_quest ((char *) &a_kalk_eig.mat_o_b,3,0);
    ins_quest ((char *) &a_kalk_eig.mdn,1,0);
    ins_quest ((char *) &a_kalk_eig.sk_teilk,3,0);
    ins_quest ((char *) &a_kalk_eig.sk_vollk,3,0);
    ins_quest ((char *) &a_kalk_eig.sp_fil,3,0);
    ins_quest ((char *) &a_kalk_eig.sp_lad,3,0);
    ins_quest ((char *) &a_kalk_eig.sp_vk,3,0);
    ins_quest ((char *) &a_kalk_eig.aend_dat,2,0);
            sqltext = "update a_kalk_eig set "
"a_kalk_eig.a = ?,  a_kalk_eig.bearb_fil = ?,  "
"a_kalk_eig.bearb_lad = ?,  a_kalk_eig.bearb_sk = ?,  "
"a_kalk_eig.delstatus = ?,  a_kalk_eig.fil = ?,  "
"a_kalk_eig.fil_ek_teilk = ?,  a_kalk_eig.fil_ek_vollk = ?,  "
"a_kalk_eig.hk_teilk = ?,  a_kalk_eig.hk_vollk = ?,  "
"a_kalk_eig.kost = ?,  a_kalk_eig.lad_vk_teilk = ?,  "
"a_kalk_eig.lad_vk_vollk = ?,  a_kalk_eig.mat_o_b = ?,  "
"a_kalk_eig.mdn = ?,  a_kalk_eig.sk_teilk = ?,  "
"a_kalk_eig.sk_vollk = ?,  a_kalk_eig.sp_fil = ?,  "
"a_kalk_eig.sp_lad = ?,  a_kalk_eig.sp_vk = ?,  a_kalk_eig.aend_dat = ? "

#line 28 "a_kalk_eig.rpp"
                                  "where mdn = ? "
                                  "and fil = ? "
                                  "and   a   = ?";
            ins_quest ((char *)   &a_kalk_eig.mdn, 2, 0);
            ins_quest ((char *)   &a_kalk_eig.fil, 2, 0);
            ins_quest ((char *)   &a_kalk_eig.a,  3, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &a_kalk_eig.mdn, 2, 0);
            ins_quest ((char *) &a_kalk_eig.fil, 2, 0);
            ins_quest ((char *)   &a_kalk_eig.a,  3, 0);
            test_upd_cursor = prepare_sql ("select a from a_kalk_eig "
                                  "where mdn = ? "
                                  "and fil = ? "
                                  "and   a = ? "
                                  "for update");

            ins_quest ((char *) &a_kalk_eig.mdn, 2, 0);
            ins_quest ((char *) &a_kalk_eig.fil, 2, 0);
            ins_quest ((char *)   &a_kalk_eig.a,  3, 0);
            del_cursor = prepare_sql ("delete from a_kalk_eig "
                                  "where mdn = ? "
                                  " and fil = ? "
                                  "and   a = ?");

    ins_quest ((char *) &a_kalk_eig.a,3,0);
    ins_quest ((char *) &a_kalk_eig.bearb_fil,1,0);
    ins_quest ((char *) &a_kalk_eig.bearb_lad,1,0);
    ins_quest ((char *) &a_kalk_eig.bearb_sk,1,0);
    ins_quest ((char *) &a_kalk_eig.delstatus,1,0);
    ins_quest ((char *) &a_kalk_eig.fil,1,0);
    ins_quest ((char *) &a_kalk_eig.fil_ek_teilk,3,0);
    ins_quest ((char *) &a_kalk_eig.fil_ek_vollk,3,0);
    ins_quest ((char *) &a_kalk_eig.hk_teilk,3,0);
    ins_quest ((char *) &a_kalk_eig.hk_vollk,3,0);
    ins_quest ((char *) &a_kalk_eig.kost,3,0);
    ins_quest ((char *) &a_kalk_eig.lad_vk_teilk,3,0);
    ins_quest ((char *) &a_kalk_eig.lad_vk_vollk,3,0);
    ins_quest ((char *) &a_kalk_eig.mat_o_b,3,0);
    ins_quest ((char *) &a_kalk_eig.mdn,1,0);
    ins_quest ((char *) &a_kalk_eig.sk_teilk,3,0);
    ins_quest ((char *) &a_kalk_eig.sk_vollk,3,0);
    ins_quest ((char *) &a_kalk_eig.sp_fil,3,0);
    ins_quest ((char *) &a_kalk_eig.sp_lad,3,0);
    ins_quest ((char *) &a_kalk_eig.sp_vk,3,0);
    ins_quest ((char *) &a_kalk_eig.aend_dat,2,0);
            ins_cursor = prepare_sql ("insert into a_kalk_eig ("
"a,  bearb_fil,  bearb_lad,  bearb_sk,  delstatus,  fil,  fil_ek_teilk,  "
"fil_ek_vollk,  hk_teilk,  hk_vollk,  kost,  lad_vk_teilk,  lad_vk_vollk,  "
"mat_o_b,  mdn,  sk_teilk,  sk_vollk,  sp_fil,  sp_lad,  sp_vk,  aend_dat) "

#line 54 "a_kalk_eig.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?)");

#line 56 "a_kalk_eig.rpp"
}
