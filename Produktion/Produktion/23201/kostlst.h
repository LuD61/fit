#ifndef _KOSTLST_DEF
#define _KOSTLST_DEF
#include "listenter.h"
#include "listclex.h"
#include "work.h"
#include "mo_progcfg.h"

#define SMALL 0
#define BIG 1
 
class KOSTLST : public virtual LISTENTER, 
                public virtual KEYS,
                public virtual Info
{
         private :
                      static CFIELD *_fChoise0[];
                      static CFORM fChoise0;
                      static CFIELD *_fChoise[];
                      static CFORM fChoise;

                      static struct CHATTR ChAttra [];
                      static struct CHATTR *ChAttr;
                      Work *work;
                      int cursor;
                      CFORM *Toolbar2;
                      BOOL cfgOK;
                      PROG_CFG *ProgCfg;
                      HBITMAP ArrDown;
                      double summegew;
                      double ekds;
                      BOOL PlanEk;
                      static int NoRecNrSet;
                      static char *StufeCombo [];

         public :

                     static struct KOSTLSTS Lsts;
                     static struct KOSTLSTS *LstTab;
                     static ITEM ustufe;
                     static ITEM ustufe_bez;
                     static ITEM ukost_art;
                     static ITEM ukost_art_bez;
                     static ITEM ukost_wert;
                     static ITEM ume_einh;
                     static ITEM ukosten;
                     static ITEM ufiller;
                     static field _UbForm[];
                     static form UbForm;

                     static ITEM iline;
                     static field _LineForm[];
                     static form LineForm;

                     static ITEM istufe;
                     static ITEM istufe_bez;
                     static ITEM ikost_art;
                     static ITEM ikost_art_bez;
                     static ITEM ikost_wert;
                     static ITEM ime_einh;
                     static ITEM ikosten;
                     static ITEM ichtyp;
                     static ITEM ichnr;
                     static field _DataForm[];
                     static form DataForm;
                     static int ubrows[];
                     static char *InfoItem;
                     void SetButtonSize (int);


                     void SetToolbar2 (CFORM *Toolbar2)
                     {
                               this->Toolbar2 = Toolbar2;
                     }
 
                     CFORM *GetToolbar2 (void)
                     {
                               return Toolbar2;
                     }

                     void SetWork (Work *work)
                     {
                         this->work = work;
                         this->work->SetListDlg (this);
                     }

                     Work *GetWork (void)
                     {
                         return work;
                     }

                     KOSTLST ();
					 ~KOSTLST ();
                     void SetPlanEk (BOOL);
                     void GetSysPar (void);
                     void GetCfgValues (void);
                     void Setfeld2Removed (BOOL);
                     void Prepare (void);
                     void Open (void);
                     int Fetch (void);
                     void Close (void);
                     void CalcSum (void);
                     void ReportFill (void);
                     
 				     unsigned char *GetDataRec (void);
				     int GetDataRecLen (void);
 				     char *GetListTab (int);
                     void WriteRec (int);
                     void InitRow (void);
                     int ToMemory (int);
                     void uebertragen (void);

                     void FillCombo (HWND, char *);
                     void SetNoRecNr (void);
                     BOOL StopList (void);
                     BOOL OnKey5 (void);
                     BOOL OnKey6 (void);
                     BOOL OnKey7 (void);
                     BOOL OnKey8 (void);
                     BOOL OnKey9 (void);
                     BOOL OnKey10 (void);
                     BOOL OnKey11 (void);
                     BOOL OnKey12 (void);
                     BOOL OnKeyDel (void);
                     BOOL OnKeyInsert (void);
                     BOOL OnKeyUp (void);
                     BOOL OnKeyDown (void);
                     BOOL OnKeyNext (void);
                     BOOL OnKeyPrior (void);
                     BOOL OnCommand (HWND, UINT, WPARAM, LPARAM);
                     BOOL OnPaint (HWND, UINT, WPARAM, LPARAM);
                     BOOL OnMove (HWND, UINT, WPARAM, LPARAM);
                     int AfterRow (int);
                     BOOL TestKostArt (void);
                     void FillStufe (int);
                     BOOL InFirstField ();
                     int AfterCol (char *);
                     int BeforeCol (char *);
                     int BeforeRow (int);
                     void CreateListQuikInfos (HWND);
                     void TestRemoves (void);
                     int  ShowKostArt (void);
                     static int InfoProc (char **, char *, char *);

                     BOOL CallInfo (HWND, RECT *, char *, char *, DWORD);
                     void CallInfoEx (void);
};
#endif