#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include <string.h>
#include "comcthlp.h"
#include "itemc.h"
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_menu.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_intp.h"
#include "inflib.h"
#include "spezdlg.h"
#include "enterfunc.h"
#include "ZeitLst.h"
#include "spezdlg.h"
#include "searchrezv.h"
#include "searchka.h"
// #include "dlg.h"

#define IDM_CANCEL 3001
#ifndef IDM_WRITE
#define IDM_WRITE  3002
#endif

#ifndef MAXCOMBO
#define MAXCOMBO 20
#endif

static int StdSize = STDSIZE;

static int dlgsize    = STDSIZE;
static int ltgraysize = STDSIZE;
static int dlgpossize = STDSIZE;
static int cpsize     = 200;


static mfont dlgfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         GRAYCOL,
                         0,
                         NULL};

struct ZEITLSTS  ZEITLST::Lsts;
struct ZEITLSTS *ZEITLST::LstTab;


CFIELD *ZEITLST::_fChoise0 [] = {
    new CFIELD ("cancel", " abbrechen ", 10, 0, 0, 0, NULL, "",
                CBUTTON, 
                IDM_CANCEL, &dlgfont, 0, 0),
    new CFIELD ("write", " speichern ", 10, 0,12, 0, NULL, "",
                CBUTTON, 
                IDM_WRITE, &dlgfont, 0, 0),
    new CFIELD ("show",  "  ansehen  ", 10, 0,24, 0, NULL, "",
                CBUTTON, 
                IDM_CHOISE, &dlgfont, 0, 0),
    new CFIELD ("delete","  l�schen  ", 10, 0,36, 0, NULL, "",
                CBUTTON, 
                IDM_DELETE, &dlgfont, 0, 0),
    new CFIELD ("insert"," einf�gen  ", 10, 0,48, 0, NULL, "",
                CBUTTON, 
                IDM_INSERT, &dlgfont, 0, 0),
    NULL,
};

CFORM ZEITLST::fChoise0 (5, _fChoise0);

CFIELD *ZEITLST::_fChoise [] = {
    new CFIELD ("fChoise0", (char *) &fChoise0, 0, 0, -1 , -2, NULL, "",
                CFFORM, 
                500, &dlgfont, 0, 0),
    NULL,
};

CFORM ZEITLST::fChoise (1, _fChoise);


static char *EkTxt = "EK";
static char *PlanEkTxt = "Plan-EK";


ITEM ZEITLST::uprod_schritt        ("prod_schritt",       "Prod.Schritt",     "", 0); 
ITEM ZEITLST::utext                ("text",               "Text",             "", 0);
ITEM ZEITLST::ukosten              ("kosten",             "Kosten/min",       "", 0);
ITEM ZEITLST::uzeit_fix            ("zeit_fix",           "fixe Zeit",        "", 0);
ITEM ZEITLST::uzeit_pfix           ("zeit_pfix",          "sprungfixe Zeit",  "", 0);
ITEM ZEITLST::uzeit_var            ("zeit_var",           "variable Zeit",    "", 0);
ITEM ZEITLST::ukostenek            ("kost_ek",            "Kosten EK",        "", 0);
ITEM ZEITLST::usu_zeit             ("su_zeit",            "Summe Zeit",       "", 0);
ITEM ZEITLST::usu_kost             ("su_kost",            "Summe Kosten",     "", 0);
ITEM ZEITLST::ufiller              ("",                   " ",                "",0);

ITEM ZEITLST::iline ("", "1", "", 0);

ITEM ZEITLST::iprod_schritt        ("prod_schritt",       Lsts.prod_schritt,   "", 0); 
ITEM ZEITLST::itext                ("text",               Lsts.text	,      "", 0);
ITEM ZEITLST::ikosten              ("kosten",             Lsts.kosten,         "", 0);
ITEM ZEITLST::izeit_fix            ("zeit_fix",           Lsts.zeit_fix,       "", 0);
ITEM ZEITLST::izeit_pfix           ("zeit_pfix",          Lsts.zeit_spfix,     "", 0);
ITEM ZEITLST::izeit_var            ("zeit_var",           Lsts.zeit_var,       "", 0);
ITEM ZEITLST::ikostenek            ("kost_ek",            Lsts.kostenek,       "", 0);
ITEM ZEITLST::isu_zeit             ("su_zeit",            Lsts.su_zeit,        "", 0);
ITEM ZEITLST::isu_kost             ("su_kost",            Lsts.su_kost,        "", 0);
ITEM ZEITLST::ichtyp          ("chtyp",          (char *) &Lsts.chtyp,         "", 0);
ITEM ZEITLST::ichnr           ("chnr",           (char *) &Lsts.chnr,          "", 0);

field ZEITLST::_UbForm [] = {
&uprod_schritt, 17, 0, 0,  6,  NULL, "", BUTTON, 0, 0, 0,     
&utext,         21, 0, 0, 23,  NULL, "", BUTTON, 0, 0, 0,     
&ukosten,       12, 0, 0, 44,  NULL, "", BUTTON, 0, 0, 0, 
&uzeit_fix,     12, 0, 0, 56,  NULL, "", BUTTON, 0, 0, 0,     
&uzeit_pfix,    12, 0, 0, 68,  NULL, "", BUTTON, 0, 0, 0,     
&uzeit_var,     12, 0, 0, 80,  NULL, "", BUTTON, 0, 0, 0,     
&usu_zeit ,     14, 0, 0, 92,  NULL, "", BUTTON, 0, 0, 0,     
&usu_kost ,     14, 0, 0,106,  NULL, "", BUTTON, 0, 0, 0,     
&ufiller,      120, 0, 0,120,  NULL, "", BUTTON, 0, 0, 0,
};


form ZEITLST::UbForm = {9, 0, 0, _UbForm, 0, 0, 0, 0, NULL};

field ZEITLST::_LineForm [] = {
&iline,   1, 0, 0, 23,  NULL, "",  DISPLAYONLY, 0, 0, 0, 
&iline,   1, 0, 0, 44,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0, 56,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0, 68,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0, 80,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0, 92,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,106,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,120,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
};

form ZEITLST::LineForm = {8, 0, 0, _LineForm,0, 0, 0, 0, NULL};

field ZEITLST::_DataForm [] = {
&iprod_schritt, 16,10, 0,  7,   NULL, "",       EDIT, 0, 0, 0,     
&itext,         20, 0, 0, 24,   NULL, "60",     EDIT, 0, 0, 0,     
&ikosten,       10, 0, 0, 45,   NULL, "%7.2f",  EDIT, 0, 0, 0, 
&izeit_fix,     10, 0, 0, 57,   NULL, "%7.2f",  EDIT, 0, 0, 0,     
&izeit_pfix,    10, 0, 0, 69,   NULL, "%7.2f",  EDIT, 0, 0, 0,      
&izeit_var,     10, 0, 0, 81,   NULL, "%7.2f",  EDIT, 0, 0, 0,     
&isu_zeit,      12, 0, 0, 93,   NULL, "%12.2f", DISPLAYONLY, 0, 0, 0,     
&isu_kost,      12, 0, 0,107,   NULL, "%12.2f", DISPLAYONLY, 0, 0, 0,     
};


form ZEITLST::DataForm = {8, 0, 0, _DataForm,0, 0, 0, 0, NULL};

char *ZEITLST::ProdSchrittCombo [MAXCOMBO] = {"keine Produktionsschritte",
                                              NULL,
};


void ZEITLST::FillCombo (HWND hWnd, char *ItemName)
{
    static BOOL FillOK = FALSE;

    if (strcmp (ItemName, "prod_schritt") == 0)
    {
       if (work != NULL)
       {
           work->FillComboLong (ProdSchrittCombo, "prod_schritt", MAXCOMBO - 1);
       }
       for (int i = 0; ProdSchrittCombo [i]; i ++)
       {
               SendMessage (hWnd, CB_ADDSTRING, 0, 
                           (LPARAM) ProdSchrittCombo[i]);
       }
       if (strcmp (clipped (Lsts.prod_schritt), " ") <= 0)
       {
              strcpy (Lsts.prod_schritt, ProdSchrittCombo [0]);
       }
    }
}

int ZEITLST::ubrows [] = {0, 
//                          0,
                          1, 1,
                          2,
                          3, 
                          4,
                          5,
                          6,
                         -1,
};


struct CHATTR ZEITLST::ChAttra [] = {
                                     "prod_schritt",    COMBOBOX,COMBOBOX | DROPLIST, 
//                                      "kost_art",     DISPLAYONLY,EDIT,                
//                                      "chnr",         REMOVED,BUTTON,                
                                      NULL,  0,           0};

struct CHATTR *ZEITLST::ChAttr = ChAttra;
int ZEITLST::NoRecNrSet = FALSE;
char *ZEITLST::InfoItem = "kost_art";


void ZEITLST::SetPlanEk (BOOL PlanEk)
{
         this->PlanEk = PlanEk;
}


BOOL ZEITLST::CallInfo (HWND hWnd, RECT *rect, char *Item, char *ItemValue, DWORD WinMode)
/**
Einstieg in Infosystem mit Artikel. Abgeleitet von Klasse Info.
**/
{
 		 char buffer [1024];
         BOOL ret;

         sprintf (buffer, "where a = 2");
         ret = Info::CallInfo (hWnd, rect, "a", buffer, WinMode); 
         SetListFocus ();
         return ret;
}


int ZEITLST::InfoProc (char **Item, char *Value, char *where)
/**
Spezielle Procedure fuer Info-System. Alter Ablauf, ist nicht mehr Aktiv 
**/
{

   *Item = "a";

   sprintf (where, "where a = 2");
   return 1;
}

void ZEITLST::CallInfoEx (void)
/*
Info wird aus Dialog aufgerufen.
*/

{
//   char where [512];
   
//   sprintf (where, "where a = %.0lf", prdk_k.a);
//   _CallInfoEx (hMainWindow, NULL, InfoItem, where, 0l); 
}


void ZEITLST::SetButtonSize (int Size)
{
    int fpos;

//    return;
    fpos = GetItemPos (&DataForm, "chtyp");
    if (fpos < 0) return;

    if (Size == SMALL)
    {
         DataForm.mask [fpos].length = 2;
         DataForm.mask [fpos].rows = 1;
    }
    else 
    {
         DataForm.mask [fpos].length = 3;
         DataForm.mask [fpos].rows = 0;
    }
    fpos = GetItemPos (&DataForm, "chnr");
    if (fpos < 0) return;

    if (Size == SMALL)
    {
         DataForm.mask [fpos].length = 2;
         DataForm.mask [fpos].rows = 1;
    }
    else 
    {
         DataForm.mask [fpos].length = 3;
         DataForm.mask [fpos].rows = 0;
    }
}


void ZEITLST::GetCfgValues (void)
{
       char cfg_v [512];
       COLORREF Color;

       if (ProgCfg == NULL) return;
	   if (cfgOK) return;

       cfgOK = TRUE;
       if (ProgCfg->GetCfgValue ("3DList", cfg_v) == TRUE)
       {
                    eListe.Set3D (atoi (cfg_v));
	   }
       if (ProgCfg->GetCfgValue ("3DSize", cfg_v) == TRUE)
       {
                    eListe.SetPlus3D (atoi (cfg_v));
	   }
       if (ProgCfg->GetCfgValue ("ListFocus", cfg_v) == TRUE)
       {
                    ListFocus = atoi (cfg_v);
	   }
        if (ProgCfg->GetCfgValue ("rowheight", cfg_v) == TRUE)
		{
			        RowHeight = ratod (cfg_v);
					if (RowHeight < (double) 1.0)
					{
						RowHeight = (double) 1.0;
					}
		}
        if (ProgCfg->GetCfgValue ("ubheight", cfg_v) == TRUE)
		{
			        UbHeight = atoi (cfg_v);
					SetUbHeight ();
		}
		eListe.SetRowHeight (RowHeight);


        if (ProgCfg->GetCfgValue ("NoRecNr", cfg_v) == TRUE)
        {
			        NoRecNr = atoi (cfg_v);
		            if (atoi (cfg_v))
					{
	                        SetNoRecNr ();
					}
		}
        if (ProgCfg->GetCfgValue ("ListColor", cfg_v) == TRUE)
        {
                     GetListColor (&Color, cfg_v);
		             eListe.SetColor (Color);
        }
        if (ProgCfg->GetCfgValue ("ListBkColor", cfg_v) == TRUE)
        {
                     GetListColor (&Color, cfg_v);
		             eListe.SetBkColor (Color);
        }
        if (ProgCfg->GetCfgValue ("ListLine", cfg_v) == TRUE)
        {
		             eListe.SetLines (GetListLine (cfg_v));
        }
        if (ProgCfg->GetCfgValue ("KostInfo", cfg_v) == TRUE)
        {
                     InfoItem = new char [80];
                     strcpy (InfoItem, cfg_v);
        }
}


void ZEITLST::TestRemoves (void)
{
        int fpos;

        fpos = GetItemPos (&DataForm, "feld3");
        if (fpos >= 0)
        {
            if (DataForm.mask[fpos].attribut & REMOVED)
            {
                DestroyListField ("feld3");
            }
        }
}


void ZEITLST::SetNoRecNr (void)
{
	   int i;

	   eListe.SetNoRecNr (TRUE);
	   if (NoRecNrSet) return;

	   for (i = 0; i < dataform->fieldanz; i ++)
	   {
		   dataform->mask[i].pos[1] -= 6;
	   }
	   for (i = 0; i < ubform->fieldanz; i ++)
	   {
		   ubform->mask[i].pos[1] -= 6;
	   }
	   for (i = 0; i < lineform->fieldanz; i ++)
	   {
		   lineform->mask[i].pos[1] -= 6;
	   }
       NoRecNrSet = TRUE;
}


ZEITLST::ZEITLST () : KEYS (), LISTENTER () 
{

    ProgCfg = new PROG_CFG ("23201");
	Dlg = NULL;
    cfgOK = FALSE;
	cursor = -1;
    LstTab = new ZEITLSTS [0x10000];

    dataform  = &DataForm;
    ubform    = &UbForm;
    lineform  = &LineForm;
    SetFieldLen ("prod_schritt", 25);
    eListe.SetUbRows (ubrows); 
	eListe.SetKeys (this);
    LISTENTER::GetCfgValues ();
    GetCfgValues ();
//    GetSysPar ();
    TestRemoves ();
    if (NoRecNr)
    {
	    SetNoRecNr ();
    }
	fChoise.Setchpos (0, 2);
    work = NULL;
    ChAttr = ChAttra;
    eListe.SetChAttr (ChAttr); 
    Toolbar2 = NULL;
    eListe.SetInfoProc (InfoProc);
    eListe.SetInfo (this);
    ArrDown = NULL;
    ArrDown = LoadBitmap (DLG::hInstance, "ARRDOWN");
    PlanEk = FALSE;
	eListe.SetPos (0, 0);
}


ZEITLST::~ZEITLST ()
{
	if (work != NULL)
	{
          work->SetListDlg (NULL);
	}
    fChoise.destroy ();
	if (LstTab != NULL)
	{
  	    delete []LstTab;
		LstTab = NULL;
	}
    if (ProgCfg != NULL)
    {
        ProgCfg->CloseCfg ();
        delete ProgCfg;
        ProgCfg = NULL;
    }
    if (work != NULL)
    {
        work->SetKostLsts (NULL);
    }
}


// Funktionen f�r den Zugriff auf die Daten. Sind als abstrakte Schnittstellen in
// listenter definiert.

unsigned char *ZEITLST::GetDataRec (void)
{
    return (unsigned char *) &Lsts;
}

int ZEITLST::GetDataRecLen (void)
{
    return sizeof (struct ZEITLSTS);
}

char *ZEITLST::GetListTab (int i)
{
    return (char *) &LstTab[i];
}

// Ende der Schnittstellen.


void ZEITLST::Setfeld2Removed (BOOL removed)
{
    if (removed)
    {
            SetFieldAttr ("feld2", REMOVED);
    }
    else
    {
            SetFieldAttr ("feld2", EDIT);
    }
}


void ZEITLST::Prepare (void)
{
    work->FillComboLong (ProdSchrittCombo, "prod_schritt", MAXCOMBO - 1);
    if (work != NULL)
    {
        cursor = work->PrepareZeit ();
    }
}

void ZEITLST::Open (void)
{
	if (cursor == -1)
	{
		Prepare ();
		if (cursor == -1) return;
	}
    if (work != NULL)
    {
        work->OpenZeit ();
    }
}

int ZEITLST::Fetch (void)
{
	if (cursor == -1)
	{
		Prepare ();
		if (cursor == -1) return 100;
	}
    if (work != NULL)
    {
        return work->FetchZeit ();
    }
	return 100;
}


void ZEITLST::CalcPersKosten (double faktorchg , double faktorme)
{
//	char buffer [20];

	int recs = eListe.GetRecanz ();

	if (Dlg == NULL)
	{
		return;
	}


	double kost_bzg = 0.0;
	for (int i = 0; i < recs; i ++)
	{
  	    double kosten     = ratod (LstTab[i].kosten);
	    double zeit_fix   = ratod (LstTab[i].zeit_fix) ;
	    double zeit_spfix = ratod (LstTab[i].zeit_spfix) * faktorchg;
	    double zeit_var   = ratod (LstTab[i].zeit_var) * faktorme;
		sprintf (LstTab[i].zeit_fix,   "%lf", zeit_fix);
		sprintf (LstTab[i].zeit_spfix, "%lf", zeit_spfix);
		sprintf (LstTab[i].zeit_var,   "%lf", zeit_var);
   	    double summezeit   = zeit_fix + zeit_spfix + zeit_var;
	    double summekosten = summezeit * kosten;
		sprintf (LstTab[i].su_zeit,   "%lf", summezeit);
		sprintf (LstTab[i].su_kost,   "%lf", summekosten);
		kost_bzg += summekosten;
	}
/* TEST
	CFORM *fWork = ((SpezDlg *)Dlg)->GetDialog ();
	CFIELD *fP3  = fWork->GetCfield ("zeit_p3");
	CFIELD *fP30 = fWork->GetCfield ("zeit_p30");

    sprintf (buffer, "%.2lf", kost_bzg); 
    strcpy  ((char *) fP3->GetFeld (), buffer);
	*/

/*
	CFIELD *fM1  = fWork->GetCfield ("zeit_m1");
	CFIELD *fM2  = fWork->GetCfield ("zeit_m2");
    GetWindowText (fM1->GethWnd (), buffer, sizeof (buffer) - 1);
	double m1 = ratod (buffer);
    GetWindowText (fM2->GethWnd (), buffer, sizeof (buffer) - 1);
	double m2 = ratod (buffer);
	
	double kost_kalk = kost_bzg * m2 / m1;
    sprintf (buffer, "%.2lf", kost_kalk); 
    strcpy  ((char *) fP30->GetFeld (), buffer);

	fP3->SetText ();
	fP30->SetText ();
*/
	CalcPersKosten ();
	eListe.DisplayList ();
}



void ZEITLST::CalcPersKosten (void)
{
	char buffer [20];
	char FlgBasis[2];

	int recs = eListe.GetRecanz ();

	if (Dlg == NULL)
	{
		return;
	}

    ((SpezDlg *) Dlg)->GetFlgBasis (FlgBasis);
	CFORM *fWork = ((SpezDlg *)Dlg)->GetDialog ();
	CFIELD *fC1  = fWork->GetCfield ("zeit_c1");
	CFIELD *fC2  = fWork->GetCfield ("zeit_c2");
	if (fC1 == NULL) return;
	if (fC2 == NULL) return;
	GetWindowText (fC1->GethWnd (), buffer, sizeof (buffer) - 1);
	double c1 = ratod (buffer);
    GetWindowText (fC2->GethWnd (), buffer, sizeof (buffer) - 1);
	double c2 = ratod (buffer);


	CFIELD *fM1  = fWork->GetCfield ("zeit_m1");
	CFIELD *fM2  = fWork->GetCfield ("zeit_m2");
    GetWindowText (fM1->GethWnd (), buffer, sizeof (buffer) - 1);
	double m1 = ratod (buffer);
    GetWindowText (fM2->GethWnd (), buffer, sizeof (buffer) - 1);
	double m2 = ratod (buffer);
	double faktorc = c2 /c1;
	double faktorm = m2 / m1;

	double kost_bzg = 0.0;
	double kost_kalk = 0.0;
	for (int i = 0; i < recs; i ++)
	{
 	    double kosten     = ratod (LstTab[i].kosten);
	    double zeit_fix   = ratod (LstTab[i].zeit_fix);
	    double zeit_spfix = ratod (LstTab[i].zeit_spfix);
	    double zeit_var   = ratod (LstTab[i].zeit_var);
	    double summezeit   = zeit_fix + zeit_spfix + zeit_var;
 	    double summekosten = summezeit * kosten;
		if (strcmp(FlgBasis,"J") == 0)
		{
			kost_bzg += summekosten;
		}
		else
		{
			kost_kalk += summekosten;
		}
	    zeit_fix   = ratod (LstTab[i].zeit_fix);
		if (strcmp(FlgBasis,"J") == 0)
		{
			zeit_spfix = ratod (LstTab[i].zeit_spfix) * faktorc;
			zeit_var   = ratod (LstTab[i].zeit_var) * faktorm;
		}
		else
		{
		    zeit_spfix = ratod (LstTab[i].zeit_spfix) / faktorc;
		    zeit_var   = ratod (LstTab[i].zeit_var) / faktorm;
		}
	    summezeit   = zeit_fix + zeit_spfix + zeit_var;
 	    summekosten = summezeit * kosten;
		if (strcmp(FlgBasis,"J") == 0)
		{
			kost_kalk += summekosten;
		}
		else
		{
			kost_bzg += summekosten;
		}
	}
	CFIELD *fP3  = fWork->GetCfield ("zeit_p3");
	CFIELD *fP30 = fWork->GetCfield ("zeit_p30");

    sprintf (buffer, "%.2lf", kost_bzg); 
    strcpy  ((char *) fP3->GetFeld (), buffer);

    sprintf (buffer, "%.2lf", kost_kalk); 
    strcpy  ((char *) fP30->GetFeld (), buffer);

	fP3->SetText ();
	fP30->SetText ();
}

void ZEITLST::CalcSum (void)
{
	char sC1 [20];
	char sM1 [20];
	short c1;
	double m1;

	if (Dlg == NULL)
	{
		return;
	}
	CFORM *fWork = ((SpezDlg *)Dlg)->GetDialog ();
	CFIELD *fC1 = fWork->GetCfield ("zeit_c1");
	if (fC1 == NULL) return;
	CFIELD *fM1 = fWork->GetCfield ("zeit_m1");
	if (fM1 == NULL) return;
    GetWindowText (fC1->GethWnd (), sC1, sizeof (sC1) - 1);
    GetWindowText (fM1->GethWnd (), sM1, sizeof (sM1) - 1);
	c1 = atoi (sC1);
	m1 = ratod (sM1);

	double kosten     = ratod (Lsts.kosten);
	double zeit_fix   = ratod (Lsts.zeit_fix);
	double zeit_spfix = ratod (Lsts.zeit_spfix);
	double zeit_var   = ratod (Lsts.zeit_var);

	double summezeit   = zeit_fix + zeit_spfix + zeit_var;
	double summekosten = summezeit * kosten;
	sprintf (Lsts.su_zeit, "%lf", summezeit);
	sprintf (Lsts.su_kost, "%lf", summekosten);
    memcpy (&LstTab [eListe.GetAktRow ()], &Lsts, sizeof (Lsts));
	CalcPersKosten ();
    ((SpezDlg *) Dlg)->SetSummen (summegew, ekds, 0, ARB_ZEIT,0,0);
}


void ZEITLST::ReportFill (void)
{

    for (int i = 0; i < eListe.GetRecanz (); i ++)
    {


	    reportrez.grouprez = 70;
	    reportrez.a = prdk_k.a;
	    strcpy(reportrez.a_bz1, prdk_kf.a_bz1);
	    strcpy(reportrez.rez ,prdk_k.rez);
//	    strcpy(reportrez.rez_bz, prdk_k.rez_bz);
	    strcpy(reportrez.rez_bz, LstTab[i].text);
	    reportrez.variante = prdk_k.variante;
	    strcpy(reportrez.variante_bz, prdk_k.variante_bz);
	    if (strcmp(prdk_kf.ek,"J") == 0)  
        {
			strcpy(reportrez.preis_basis, "EK");				
		}
		else
        {
			strcpy(reportrez.preis_basis, "PLAN-EK");			
		}
	    reportrez.schwund = prdk_k.sw_kalk;
	    reportrez.chargengewicht = prdk_k.chg_gew;
	    strcpy(reportrez.datum, "01.01.2003");                      //muss nocht !
	    reportrez.posi = LstTab[i].lfd;
	    reportrez.mat = LstTab[i].schritt;
	    strcpy(reportrez.mat_bz, "");
	    reportrez.menge = ratod (LstTab[i].su_zeit);
	    strcpy(reportrez.einheit, "");
	    reportrez.ek = ratod (LstTab[i].kosten);
	    reportrez.wert_ek = ratod (LstTab[i].zeit_fix);
	    reportrez.kosten = ratod (LstTab[i].kosten);
	    reportrez.bep = ratod (LstTab[i].zeit_spfix);
	    reportrez.wert_bep = ratod (LstTab[i].zeit_var);
	    strcpy(reportrez.k_a_bez, "");           
	    reportrez.k_diff = 0.0;
	    reportrez.kostenkg = ratod (LstTab[i].su_kost);
	    reportrez.kostenart = 0;
	    strncpy(reportrez.kostenart_bz, LstTab[i].prod_schritt,32);//Bezeichnung der Produktionsschritte
	    reportrez.mdn = prdk_k.mdn;
	    work->ReportRezUpdate ();
    }
}


void ZEITLST::Close (void)
{
	if (cursor != -1)
	{
		cursor = -1;
	}
    if (work != NULL)
    {
        work->CloseZeit ();
    }
	eListe.SetPos (0, 0);
	for (int i = 0; i < eListe.GetRecanz (); i ++)
	{
		memcpy (&Lsts, &LstTab[i], sizeof (Lsts));
		eListe.SetPos (i, 0);
		CalcSum ();
	}
//	CalcPersKosten ();
	memcpy (&Lsts, &LstTab[0], sizeof (Lsts));
}

BOOL ZEITLST::BeforeCol (char *colname)
{
         return FALSE;
}
         
void ZEITLST::FillProdSchritt (int pos)
{
          clipped (Lsts.prod_schritt);

          Lsts.schritt = 1;
          for (int i = 0; ProdSchrittCombo[i] != NULL; i ++)
          {
              if (strcmp ((Lsts.prod_schritt), clipped (ProdSchrittCombo [i])) == 0)
              {
                  Lsts.schritt = i + 1;
                  break;
              }
          }
          memcpy (&LstTab[pos], &Lsts, sizeof (Lsts));
}

BOOL ZEITLST::AfterCol (char *colname)
{
         
         if (strcmp (colname, "prod_schritt") == 0)
         {
             FillProdSchritt (eListe.GetAktRow ());
             return FALSE;
         }
		 else if (strcmp (colname, "text") == 0)
		 {
		 }
		 else
		 {
			 CalcSum ();
             memcpy (&LstTab [eListe.GetAktRow ()], &Lsts, sizeof (Lsts));
          	 eListe.ShowAktRow ();
		 }
         return FALSE;
}

int ZEITLST::AfterRow (int Row)
{

         if (syskey == KEYUP && ratod (Lsts.kosten) == 0.0)
         {
                return OnKey7 ();
         }
         memcpy (&LstTab [Row], &Lsts, sizeof (Lsts));
         work->SetZeitLsts (&Lsts);
         CalcSum ();
         return 1;
}

int ZEITLST::BeforeRow (int Row)
{
//         memcpy (&LstTab [Row], &Lsts, sizeof (Lsts));
         return 1;
}

BOOL ZEITLST::StopList (void)
{
    int i;
    int recanz;
    double chg_gew = 0.0;

    if (AfterRow (eListe.GetAktRow ()) == -1)
    {
        return TRUE;
    }

    if (ratod (Lsts.kosten) == 0)
    {
        OnKey7 ();
    }

    else
    {
    }

    recanz = eListe.GetRecanz ();

    work->DeleteAllZeitPos ();
    for (i = 0; i < recanz; i ++)
    {
        WriteRec (i);
    }
    return TRUE;
}


BOOL ZEITLST::OnKeyNext (void)
{
        if (GetKeyState (VK_CONTROL) >= 0)
        {
              if (Dlg != NULL)
              {
                  ((SpezDlg *) Dlg)->NextPage ();
                  return TRUE;
              }
        }
        return FALSE;
}


BOOL ZEITLST::OnKeyPrior (void)
{
        if (GetKeyState (VK_CONTROL) >= 0)
        {
              if (Dlg != NULL)
              {
                  ((SpezDlg *) Dlg)->PriorPage ();
                  return TRUE;
              }
        }
        return FALSE;
}

BOOL ZEITLST::OnKey5 (void)
{

    OnKey12 ();
    syskey = KEY5;
    return TRUE;

}

BOOL ZEITLST::OnKey6 (void)
{
    eListe.InsertLine ();
    return TRUE;
}

BOOL ZEITLST::OnKeyInsert (void)
{
    return FALSE;
/*
    eListe.InsertLine ();
    return TRUE;
*/
}

BOOL ZEITLST::OnKey7 (void)
{
    eListe.DeleteLine ();
    CalcSum ();
    return TRUE;
}

BOOL ZEITLST::OnKeyDel (void)
{
    return FALSE;
/*
    eListe.DeleteLine ();
    CalcSum ();
    return TRUE;
*/
}

BOOL ZEITLST::OnKey8 (void)
{
         return FALSE;
}


BOOL ZEITLST::OnKey9 (void)
{
    char * Item = eListe.GetAktItem ();
    if (strcmp (Item, "prod_schritt") == 0)
    {
        eListe.ShowDropDown ();
        return TRUE;
    }
/*
    else if (strcmp (Item, "kost_art") == 0)
    {
        ShowKostArt ();
        return TRUE;
    }
*/
    return FALSE;
}

BOOL ZEITLST::InFirstField ()
{
	if (eListe,GetAktRow () > 0)
	{
		return FALSE;
	}
	if (eListe.GetAktColumn () > 1)
	{
		return FALSE;
	}
	if (eListe.GetAktColumn () == 1 &&
		eListe.GetDataForm ()->mask[0].attribut != DISPLAYONLY)
	{
		return FALSE;
	}
	return TRUE;
}



BOOL ZEITLST::OnKey10 (void)
{
    if (Dlg != NULL)
    {
         work->SetZeitLsts (&Lsts);
// Hier Workfunktion einsetzen
         SetListFocus ();
         return TRUE;
	}
    return FALSE;
}

BOOL ZEITLST::OnKey11 (void)
{

    if (Dlg != NULL)
    {
        ((SpezDlg *) Dlg)->OnKey11 ();
        SetAktFocus ();
    }
    return FALSE;
}


int ZEITLST::ToMemory (int i)
{

    LISTENTER::ToMemory (i);
    return 0;
}


void ZEITLST::InitRow (void)
{

    LISTENTER::InitRow ();


    work->SetZeitLsts (&Lsts);
    work->InitZeitRow ();
	FillProdSchritt (eListe.GetAktRow ());

/*
    if (ArrDown != NULL && Lsts.chtyp[0] != NULL)
    {
        Lsts.chtyp[0]->SetBitMap (ArrDown);
    }
*/


    if (ArrDown != NULL)
    {
        Lsts.chnr.SetBitMap (ArrDown);
    }


}

void ZEITLST::uebertragen (void)
{
    work->SetZeitLsts (&Lsts);
    work->FillZeitRow ();
    if (ArrDown != NULL)
    {
        Lsts.chtyp.SetBitMap (ArrDown);
    }
    if (ArrDown != NULL)
    {
        Lsts.chnr.SetBitMap (ArrDown);
    }
}

void ZEITLST::WriteRec (int i)
{
    memcpy (&Lsts, &LstTab[i], sizeof (Lsts));
    if (ratod (Lsts.kosten) == 0)
    {
        return;
    }
    FillProdSchritt (i);
    work->SetZeitLsts (&LstTab[i]);
    work->WriteZeitRec (i + 1);
}

BOOL ZEITLST::OnKey12 (void)
{
	if (sys_ben.berecht >= 3) return TRUE;
    CalcSum ();
    StopList ();
    syskey = KEY12;
    return TRUE;
}


BOOL ZEITLST::OnKeyUp (void)
{
//    form *DataForm;
//    int fpos;
/*
    if (Leere Zeile)
    {
        return OnKeyDel ();
    }
*/

    return FALSE;
}

BOOL ZEITLST::OnKeyDown (void)
{
    if (ratod (Lsts.kosten) == 0.0)
    {
        return TRUE;
    }

    return FALSE;
}


void ZEITLST::CreateListQuikInfos (HWND hWnd)
{
       if (Toolbar2 == NULL) return;

       Toolbar2->GetCfield ("f5")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("f12")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("f9")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("del")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("insert")->CreateQuikInfos (hWnd);
}


BOOL ZEITLST::OnCommand (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    if (LOWORD (wParam) == IDM_CHOISE)
    {
        return OnKey9 ();
    }
    if (LOWORD (wParam) == IDM_CANCEL)
    {
        return OnKey5 ();
    }
    if (LOWORD (wParam) == IDM_WRITE)
    {
        return OnKey12 ();
    }
    if (LOWORD (wParam) == VK_DELETE)
    {
        return OnKeyDel ();
    }
    if (LOWORD (wParam) == IDM_DELETE)
    {
        return OnKey7 ();
    }
    if (LOWORD (wParam) == VK_INSERT)
    {
        return OnKeyInsert ();
    }
    if (LOWORD (wParam) == IDM_INSERT)
    {
        return OnKey6 ();
    }
    if (HIWORD (wParam)  == CBN_CLOSEUP)
	{
		int AktCol = eListe.GetAktColumn ();
		eListe.SetPos (eListe.GetAktRow (), 0);
        eListe.GetFocusText ();
		eListe.SetPos (eListe.GetAktRow (),AktCol);
        FillProdSchritt (eListe.GetAktRow ());
        if (Dlg != NULL)
        {
	           return ((SpezDlg *) Dlg)->OnCloseUp (hWnd, msg, wParam, lParam);
        }
	}
    return FALSE;
}

BOOL ZEITLST::OnPaint (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    HDC hdc;

    if (Modal == FALSE) return FALSE;
    if (this->mamain1 != hWnd) return FALSE; 
    
    hdc = BeginPaint (hWnd, &ps);
    fChoise.display (hWnd, hdc);
    EndPaint (hWnd,&ps);
    return FALSE;
}

BOOL ZEITLST::OnMove (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{

    if (Modal == FALSE) return FALSE;
    if (this->mamain1 != hWnd) return FALSE; 

    fChoise.MoveWindow ();
    return FALSE;
}








