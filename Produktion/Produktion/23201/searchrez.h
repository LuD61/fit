#ifndef _SEARCHREZ_DEF
#define _SEARCHREZ_DEF
#include "wmaskc.h"
#include "cmask.h"
#include "mo_chqex.h"
#include "dbclass.h"

struct SREZ
{
        char rez [10];
	  char rez_bz [73];
	  char a [15];
	  char a_bz1 [26];
	  char variante [30];
	  char variante_bz [25];
};

class SEARCHREZ
{
       private :
           static ITEM urez;
           static ITEM urez_bz;
           static ITEM ua;
           static ITEM ua_bz1;
           static ITEM uvariante;

           static ITEM irez;
           static ITEM irez_bz;
           static ITEM ia;
           static ITEM ia_bz1;
           static ITEM ivariante;


           static ITEM iline;

           static field _UbForm[];
           static form UbForm;

           static field _DataForm[];
           static form DataForm;

           static field _LineForm[];
           static form LineForm;

           static HINSTANCE hMainInst;
           static HWND hMainWindow;
           static DB_CLASS DbClass; 
           static HWND awin;
           static struct SREZ *sreztab;
           static struct SREZ srez;
           static int idx;
           static long rezanz;
           static CHQEX *Query;
           static int SearchField;
           static char *query;
           static int sorez; 
           static int sorez_bz; 
           static int soa; 
           static int soa_bz1; 
           static int sovariante; 
           static short mdn;

           int SearchPos;
           int OKPos;
           int CAPos;
           DWORD WindowStyle;
           BOOL SetStyle;
        public :
           SEARCHREZ ()
           {
                  SearchPos = 8;
                  OKPos = 9;
                  CAPos = 10;
                  SetStyle = FALSE;
                  mdn = 0;
                  WindowStyle = WS_POPUP | WS_THICKFRAME;
           }

           ~SEARCHREZ ()
           {
                  if (sreztab != NULL)
                  {
                      delete sreztab;
                      sreztab = NULL;
                  }
                  if (Query != NULL)
                  {
                      delete Query;
                      Query = NULL;
                  }
           }

           void SetQuery (char *q)
           {
               this->query = q;
/* ???
               if (query == NULL || (strcmp (query, q) != 0))
               {
                      query = q;

                      if (Query != NULL)
                      {
                             delete Query;
                             Query = NULL;
                      }
                      if (sreztab != NULL)
                      {
                             delete sreztab;
                             sreztab = NULL;
                      }
               }   
*/
           }

           char *GetQuery (void)
           {
                return query;
           }

           SREZ *GetSRez (void)
           {
               if (idx == -1) return NULL;
               return &srez;
           }

           void Setawin (HWND awin)
           {
               this->awin = awin;
           }

           void SetWindowStyle (DWORD style)
           {
               WindowStyle = style;
               SetStyle = TRUE;
           }


           void FillBox (void);
           static void FillFormat (char *, int);
           static void FillVlines (char *, int);
           static void FillCaption (char *, int);
           static void FillRec (char *, int, int);
           static int sortrez (const void *, const void *);
           static void SortRez (HWND);
           static int sortrez_bz (const void *, const void *);
           static void SortRezBz (HWND);
           static int sorta (const void *, const void *);
           static void SortA (HWND);
           static int sorta_bz1 (const void *, const void *);
           static void SortABz1 (HWND);
           static int sortvariante (const void *, const void *);
           static void SortVariante (HWND);
           static void SortLst (int, HWND);
           static void UpdateList (void);

           static int SearchLst (char *);
           static int Read (char *);
           void SetParams (HINSTANCE, HWND);
           void Search (short);
};  
#endif