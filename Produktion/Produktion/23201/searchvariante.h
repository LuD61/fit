#ifndef _SEARCHVARIANTE_DEF
#define _SEARCHVARIANTE_DEF
#include "wmaskc.h"
#include "cmask.h"
#include "mo_chqex.h"
#include "dbclass.h"

struct SVARIANTE
{
        char variante [10];
	  char variante_bz [26];
        char active [6];
};

class SEARCHVARIANTE
{
       private :
           static ITEM uvariante;
           static ITEM uvariante_bz;
           static ITEM uactive;
           static ITEM ivariante;
           static ITEM ivariante_bz;
           static ITEM iactive;
           static ITEM iline;

           static field _UbForm[];
           static form UbForm;

           static field _DataForm[];
           static form DataForm;

           static field _LineForm[];
           static form LineForm;

           static HINSTANCE hMainInst;
           static HWND hMainWindow;
           static DB_CLASS DbClass; 
           static HWND awin;
           static struct SVARIANTE *svariantetab;
           static struct SVARIANTE svariante;
           static int idx;
           static long varanz;
           static CHQEX *Query;
           static int SearchField;
           static char *query;
           static int sovariante; 
           static int sovariante_bz; 
           static int soactive; 
           static short mdn; 
           int SearchPos;
           int OKPos;
           int CAPos;
           DWORD WindowStyle;
           BOOL SetStyle;
           HBITMAP Sel;
           HBITMAP Msk;
        public :
           SEARCHVARIANTE ()
           {
                  SearchPos = 0;
                  OKPos = 9;
                  CAPos = 10;
                  SetStyle = FALSE;
                  WindowStyle = WS_POPUP | WS_THICKFRAME;
           }

           ~SEARCHVARIANTE ()
           {
                  if (svariantetab != NULL)
                  {
                      delete svariantetab;
                      svariantetab = NULL;
                  }
                  if (Query != NULL)
                  {
                      delete Query;
                      Query = NULL;
                  }
           }

           void DeleteQuery (void)
           {
/*
                  if (svariantetab != NULL)
                  {
                      delete svariantetab;
                      svariantetab = NULL;
                  }
*/
                  if (Query != NULL)
                  {
                      delete Query;
                      Query = NULL;
                  }
           }

           void SetQuery (char *q)
           {
               this->query = q;
/* ???
               if (query == NULL || (strcmp (query, q) != 0))
               {
                      query = q;

                      if (Query != NULL)
                      {
                             delete Query;
                             Query = NULL;
                      }
                      if (svariantetab != NULL)
                      {
                             delete svariantetab;
                             svariantetab = NULL;
                      }
               }   
*/
           }

           char *GetQuery (void)
           {
                return query;
           }

           SVARIANTE *GetSVariante (void)
           {
               if (idx == -1) return NULL;
               return &svariante;
           }

           void Setawin (HWND awin)
           {
               this->awin = awin;
           }

           void SetWindowStyle (DWORD style)
           {
               WindowStyle = style;
               SetStyle = TRUE;
           }


           void FillBox (void);
           static void FillFormat (char *, int);
           static void FillVlines (char *, int);
           static void FillCaption (char *, int);
           static void FillRec (char *, int, int);
           static int sortvariante (const void *, const void *);
           static void SortVariante (HWND);
           static int sortvariante_bz (const void *, const void *);
           static void SortVarianteBz (HWND);
           static int sortactive (const void *, const void *);
           static void SortActive (HWND);
           static void SortLst (int, HWND);
           static void UpdateList (void);

           static int SearchLst (char *);
           static int Read (char *);
           void SetParams (HINSTANCE, HWND);
           void Search (short mdn);
};  
#endif