#ifndef _ZEITLST_DEF
#define _ZEITLST_DEF
#include "listenter.h"
#include "listclex.h"
#include "work.h"
#include "mo_progcfg.h"

#define SMALL 0
#define BIG 1
 
class ZEITLST : public virtual LISTENTER, 
                public virtual KEYS,
                public virtual Info
{
         private :
                      static CFIELD *_fChoise0[];
                      static CFORM fChoise0;
                      static CFIELD *_fChoise[];
                      static CFORM fChoise;

                      static struct CHATTR ChAttra [];
                      static struct CHATTR *ChAttr;
                      Work *work;
                      int cursor;
                      CFORM *Toolbar2;
                      BOOL cfgOK;
                      PROG_CFG *ProgCfg;
                      HBITMAP ArrDown;
                      double summegew;
                      double ekds;
                      BOOL PlanEk;
                      static int NoRecNrSet;
                      static char *ProdSchrittCombo [];

         public :

                     static struct ZEITLSTS Lsts;
                     static struct ZEITLSTS *LstTab;
                     static ITEM uprod_schritt;
                     static ITEM utext;
                     static ITEM ukosten;
                     static ITEM uzeit_fix;
                     static ITEM uzeit_pfix;
                     static ITEM uzeit_var;
                     static ITEM ukostenek;
                     static ITEM usu_zeit;
                     static ITEM usu_kost;
                     static ITEM ufiller;
                     static field _UbForm[];
                     static form UbForm;
                     static ITEM iline;
                     static field _LineForm[];
                     static form LineForm;

                     static ITEM iprod_schritt;
                     static ITEM itext;
                     static ITEM ikosten;
                     static ITEM izeit_fix;
                     static ITEM izeit_pfix;
                     static ITEM izeit_var;
                     static ITEM ikostenek;
                     static ITEM isu_zeit;
                     static ITEM isu_kost;
                     static ITEM ichtyp;
                     static ITEM ichnr;
                     static field _DataForm[];
                     static form DataForm;
                     static int ubrows[];
                     static char *InfoItem;
                     void SetButtonSize (int);


                     void SetToolbar2 (CFORM *Toolbar2)
                     {
                               this->Toolbar2 = Toolbar2;
                     }
 
                     CFORM *GetToolbar2 (void)
                     {
                               return Toolbar2;
                     }

                     void SetWork (Work *work)
                     {
                         this->work = work;
                         this->work->SetListDlg (this);
                     }

                     Work *GetWork (void)
                     {
                         return work;
                     }

                     ZEITLST ();
					 ~ZEITLST ();
                     void SetPlanEk (BOOL);
                     void GetSysPar (void);
                     void GetCfgValues (void);
                     void Setfeld2Removed (BOOL);
                     void Prepare (void);
                     void Open (void);
                     int Fetch (void);
                     void Close (void);
                     void CalcSum (void);
                     void CalcPersKosten (void);
                     void CalcPersKosten (double, double);
                     void ReportFill (void);
                     
 				     unsigned char *GetDataRec (void);
				     int GetDataRecLen (void);
 				     char *GetListTab (int);
                     void WriteRec (int);
                     void InitRow (void);
                     int ToMemory (int);
                     void uebertragen (void);

                     void FillCombo (HWND, char *);
                     void SetNoRecNr (void);
                     BOOL StopList (void);
                     BOOL OnKey5 (void);
                     BOOL OnKey6 (void);
                     BOOL OnKey7 (void);
                     BOOL OnKey8 (void);
                     BOOL OnKey9 (void);
                     BOOL OnKey10 (void);
                     BOOL OnKey11 (void);
                     BOOL OnKey12 (void);
                     BOOL OnKeyDel (void);
                     BOOL OnKeyInsert (void);
                     BOOL OnKeyUp (void);
                     BOOL OnKeyDown (void);
                     BOOL OnKeyNext (void);
                     BOOL OnKeyPrior (void);
                     BOOL OnCommand (HWND, UINT, WPARAM, LPARAM);
                     BOOL OnPaint (HWND, UINT, WPARAM, LPARAM);
                     BOOL OnMove (HWND, UINT, WPARAM, LPARAM);
                     BOOL InFirstField ();
                     int AfterRow (int);
                     void FillProdSchritt (int);
                     int AfterCol (char *);
                     int BeforeCol (char *);
                     int BeforeRow (int);
                     void CreateListQuikInfos (HWND);
                     void TestRemoves (void);
                     static int InfoProc (char **, char *, char *);

                     BOOL CallInfo (HWND, RECT *, char *, char *, DWORD);
                     void CallInfoEx (void);
};
#endif