#include <windows.h>
#include "wmaskc.h"
#include "searchrezv.h"
#include "strfkt.h"
#include "mo_wmess.h"
#include "lbox.h"
#include "dlglst.h"
#include "a_bas.h"
#include "lbox.h"


struct SREZV *SEARCHREZV::srezvtab = NULL;
struct SREZV SEARCHREZV::srezv;
int SEARCHREZV::idx = -1;
long SEARCHREZV::rezanz;
CHQEX *SEARCHREZV::Query = NULL;
DB_CLASS SEARCHREZV::DbClass; 
HINSTANCE SEARCHREZV::hMainInst;
HWND SEARCHREZV::hMainWindow;
HWND SEARCHREZV::awin;
int SEARCHREZV::SearchField = 1;
int SEARCHREZV::sorez = 1; 
int SEARCHREZV::sorez_bz = 1; 
int SEARCHREZV::soa = 1; 
int SEARCHREZV::soa_bz1 = 1; 
int SEARCHREZV::sovariante = 1; 
int SEARCHREZV::somat = 1; 
short SEARCHREZV::mdn = 0; 


ITEM SEARCHREZV::urez      ("rez",       "Rezeptur",      "", 0);  
ITEM SEARCHREZV::urez_bz   ("rez_bz",    "Bezeichnung",   "", 0);  
ITEM SEARCHREZV::umat      ("mat",       "Material",       "", 0);  
ITEM SEARCHREZV::ua        ("a",         "Artikel",       "", 0);  
ITEM SEARCHREZV::ua_bz1    ("a_bz1",     "Bezeichnung",   "", 0);  
ITEM SEARCHREZV::uvariante ("variante",  "Variante",      "", 0);  


field SEARCHREZV::_UbForm[] = {
&urez,      10, 0, 0,  0,  NULL, "", BUTTON, 0, 0, 0,     
&umat,      12, 0, 0, 10 , NULL, "", BUTTON, 0, 0, 0,     
&ua,        15, 0, 0, 22 , NULL, "", BUTTON, 0, 0, 0,     
&ua_bz1,    26, 0, 0, 37 , NULL, "", BUTTON, 0, 0, 0,     
};

form SEARCHREZV::UbForm = {4, 0, 0, _UbForm, 0, 0, 0, 0, NULL};


ITEM SEARCHREZV::irez      ("rez",       srezv.rez,        "", 0);  
ITEM SEARCHREZV::irez_bz   ("rez_bz",    srezv.rez_bz,     "", 0);  
ITEM SEARCHREZV::ia        ("a",         srezv.a,          "", 0);  
ITEM SEARCHREZV::imat      ("mat",       srezv.mat,        "", 0);  
ITEM SEARCHREZV::ia_bz1    ("a_bz1",     srezv.a_bz1,      "", 0);  
ITEM SEARCHREZV::ivariante ("variante",  srezv.variante,   "", 0);  

field SEARCHREZV::_DataForm[] = {
&irez,       8, 0, 0,  1,  NULL, "",       DISPLAYONLY, 0, 0, 0,     
&imat,      10, 0, 0, 11 , NULL, "%8d",    DISPLAYONLY, 0, 0, 0,     
&ia,        13, 0, 0, 23 , NULL, "%13.0f", DISPLAYONLY, 0, 0, 0,     
&ia_bz1,    24, 0, 0, 38 , NULL, "",       DISPLAYONLY, 0, 0, 0,     
};

form SEARCHREZV::DataForm = {4, 0, 0, _DataForm, 0, 0, 0, 0, NULL};

ITEM SEARCHREZV::iline ("", "1", "", 0);

field SEARCHREZV::_LineForm[] = {
&iline,       1, 0, 0, 10 , NULL, "", DISPLAYONLY, 0, 0, 0,     
&iline,       1, 0, 0, 22 , NULL, "", DISPLAYONLY, 0, 0, 0,     
&iline,       1, 0, 0, 37 , NULL, "", DISPLAYONLY, 0, 0, 0,     
};

form SEARCHREZV::LineForm = {3, 0, 0, _LineForm, 0, 0, 0, 0, NULL};

char *SEARCHREZV::query = NULL;

int SEARCHREZV::sortrez (const void *elem1, const void *elem2)
{
	      struct SREZV *el1; 
	      struct SREZV *el2; 

		  el1 = (struct SREZV *) elem1;
		  el2 = (struct SREZV *) elem2;
          clipped (el1->rez);
          clipped (el2->rez);
          if (strlen (el1->rez) == 0 &&
              strlen (el2->rez) == 0)
          {
              return 0;
          }
          if (strlen (el1->rez) == 0)
          {
              return -1;
          }
          if (strlen (el2->rez) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->rez,el2->rez) * sorez);
}

void SEARCHREZV::SortRez (HWND hWnd)
{
   	   qsort (srezvtab, rezanz, sizeof (struct SREZV),
				   sortrez);
       sorez *= -1;
}

int SEARCHREZV::sortrez_bz (const void *elem1, const void *elem2)
{
	      struct SREZV *el1; 
	      struct SREZV *el2; 

		  el1 = (struct SREZV *) elem1;
		  el2 = (struct SREZV *) elem2;
          clipped (el1->rez_bz);
          clipped (el2->rez_bz);
          if (strlen (el1->rez_bz) == 0 &&
              strlen (el2->rez_bz) == 0)
          {
              return 0;
          }
          if (strlen (el1->rez_bz) == 0)
          {
              return -1;
          }
          if (strlen (el2->rez_bz) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->rez_bz,el2->rez_bz) * sorez_bz);
}

void SEARCHREZV::SortRezBz (HWND hWnd)
{
   	   qsort (srezvtab, rezanz, sizeof (struct SREZV),
				   sortrez_bz);
       sorez_bz *= -1;
}

int SEARCHREZV::sorta (const void *elem1, const void *elem2)
{
	      struct SREZV *el1; 
	      struct SREZV *el2; 

		  el1 = (struct SREZV *) elem1;
		  el2 = (struct SREZV *) elem2;
          clipped (el1->a);
          clipped (el2->a);

          if (ratod (el1->a) > ratod (el2->a))
          {
              return 1 * soa;
          }
          else if (ratod (el1->a) < ratod (el2->a))
          {
              return -1 * soa;
          }

          return 0;
/*
          if (strlen (el1->a) == 0 &&
              strlen (el2->a) == 0)
          {
              return 0;
          }
          if (strlen (el1->a) == 0)
          {
              return -1;
          }
          if (strlen (el2->a) == 0)
          {
              return 1;
          }

	      return (strcmp (el1->a,el2->a) * soa);
*/
}

void SEARCHREZV::SortA (HWND hWnd)
{
   	   qsort (srezvtab, rezanz, sizeof (struct SREZV),
				   sorta);
       soa *= -1;
}

int SEARCHREZV::sorta_bz1 (const void *elem1, const void *elem2)
{
	      struct SREZV *el1; 
	      struct SREZV *el2; 

		  el1 = (struct SREZV *) elem1;
		  el2 = (struct SREZV *) elem2;
          clipped (el1->a_bz1);
          clipped (el2->a_bz1);
          if (strlen (el1->a_bz1) == 0 &&
              strlen (el2->a_bz1) == 0)
          {
              return 0;
          }
          if (strlen (el1->a_bz1) == 0)
          {
              return -1;
          }
          if (strlen (el2->a_bz1) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->a_bz1,el2->a_bz1) * soa_bz1);
}

void SEARCHREZV::SortABz1 (HWND hWnd)
{
   	   qsort (srezvtab, rezanz, sizeof (struct SREZV),
				   sorta_bz1);
       soa_bz1 *= -1;
}

int SEARCHREZV::sortvariante (const void *elem1, const void *elem2)
{
	      struct SREZV *el1; 
	      struct SREZV *el2; 

		  el1 = (struct SREZV *) elem1;
		  el2 = (struct SREZV *) elem2;
          return (atoi (el1->variante) - atoi (el2->variante)) * sovariante;
/*
          clipped (el1->variante);
          clipped (el2->variante);
          if (strlen (el1->variante) == 0 &&
              strlen (el2->variante) == 0)
          {
              return 0;
          }
          if (strlen (el1->variante) == 0)
          {
              return -1;
          }
          if (strlen (el2->variante) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->variante,el2->variante) * sovariante);
*/
}

void SEARCHREZV::SortVariante (HWND hWnd)
{
   	   qsort (srezvtab, rezanz, sizeof (struct SREZV),
				   sortvariante);
       sovariante *= -1;
}

int SEARCHREZV::sortmat (const void *elem1, const void *elem2)
{
	      struct SREZV *el1; 
	      struct SREZV *el2; 

		  el1 = (struct SREZV *) elem1;
		  el2 = (struct SREZV *) elem2;
          return (atol (el1->mat) - atol (el2->mat)) * somat;
/*
          clipped (el1->variante);
          clipped (el2->variante);
          if (strlen (el1->variante) == 0 &&
              strlen (el2->variante) == 0)
          {
              return 0;
          }
          if (strlen (el1->variante) == 0)
          {
              return -1;
          }
          if (strlen (el2->variante) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->variante,el2->variante) * sovariante);
*/
}

void SEARCHREZV::SortMat (HWND hWnd)
{
   	   qsort (srezvtab, rezanz, sizeof (struct SREZV),
				   sortvariante);
       sovariante *= -1;
}

void SEARCHREZV::SortLst (int Col, HWND hWnd)
/**
Procedure zum Sortiern der Liste.
**/
{
       switch (Col)
       {
              case 0 :
                  SortRez (hWnd);
                  SearchField = 0;
                  break;
              case 1 :
                  SortMat (hWnd);
                  SearchField = 1;
                  break;
              case 2 :
                  SortA (hWnd);
                  SearchField = 2;
                  break;
              case 3 :
                  SortABz1 (hWnd);
                  SearchField = 3;
                  break;
              default :
                  return;
       }
       UpdateList ();
}

void SEARCHREZV::FillFormat (char *buffer, int size)
{
        DlgLst::FillFrmAttr (&DataForm, buffer, size);
}


void SEARCHREZV::FillCaption (char *buffer, int size)
{
        DlgLst::FillFrmTitle (&UbForm, buffer, 0, size);
}

void SEARCHREZV::FillVlines (char *buffer, int size) 
{
        DlgLst::FillFrmLines (&LineForm, buffer, 0, size);
}


void SEARCHREZV::FillRec (char *buffer, int i, int size)
{
          DlgLst::FillFrmData (&DataForm, buffer, 0, size);
}


void SEARCHREZV::UpdateList (void)
{
       int i;
 	   char buffer [512];

       for (i = 0; i < rezanz; i ++)
       {
		  memcpy (&srezv, &srezvtab[i],  sizeof (struct SREZV));
          FillRec (buffer, i, 512);
	      Query->UpdateRecord (buffer, i);
       }
}

int SEARCHREZV::SearchLst (char *sebuff)
/**
Nach Kurzname in Liste suchen.
**/
{
/*
	   Query->SearchList ();
	   return 0;
*/

	   int i;
	   int len;
       char sabuff [80];
       char *pos;

	   if (srezvtab == NULL) return 0;
	   if (strlen (sebuff) == 0) 
	   {
	       Query->SetSel (0);
		   return 0;
	   }

	   for (i = 0; i < rezanz; i ++)
	   {
		   len = min (16, strlen (sebuff));
           if (SearchField == 0)
           {
		           if (strupcmp (sebuff, srezvtab[i].rez, len) == 0) break;
           }
           if (SearchField == 1)
           {
                   strcpy (sabuff, srezvtab[i].mat);
                   for (pos = sabuff; *pos <= ' ' && *pos != 0 ; pos ++);
                   pos[len] = 0;
                   strcpy (sabuff, pos);
                   if (atol (sebuff) == atol (sabuff)) break;
           }
/*
           else if (SearchField == 1)
           {
		           if (strupcmp (sebuff, srezvtab[i].rez_bz, len) == 0) break;
           }
*/
           else if (SearchField == 2)
           {
                   strcpy (sabuff, srezvtab[i].a);
                   for (pos = sabuff; *pos <= ' ' && *pos != 0 ; pos ++);
                   pos[len] = 0;
                   strcpy (sabuff, pos);
                   if (ratod (sebuff) == ratod (sabuff)) break;
           }
           else if (SearchField == 3)
           {
		           if (strupcmp (sebuff, srezvtab[i].a_bz1, len) == 0) break;
           }
/*
           if (SearchField == 4)
           {
                   strcpy (sabuff, srezvtab[i].variante);
                   for (pos = sabuff; *pos <= ' ' && *pos != 0 ; pos ++);
                   pos[len] = 0;
                   strcpy (sabuff, pos);
                   if (atoi (sebuff) == atoi (sabuff)) break;
           }
*/
	   }
	   if (i == rezanz) return 0;
	   Query->SetSel (i);
	   return 0;
}


int SEARCHREZV::Read (char *squery)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int cursor;
	  int i;
      WMESS Wmess;


	  if (srezvtab) 
	  {
           delete srezvtab;
           srezvtab = NULL;
	  }


      idx = -1;
	  rezanz = 0;
      SearchField = 3;
      Query->SetSBuff (NULL);
      clipped (squery);
      if (strcmp (squery, " ") == 0) squery[0] = 0;

      if (rezanz == 0) rezanz = 0x10000;

	  srezvtab = new struct SREZV [rezanz];
      if (squery == NULL || 
          strlen (squery) == 0)
      {
             sprintf (buffer, "select rez, rez_bz, a, variante "
	 			       "from prdk_k "
                       "where mdn = %hd "
                       "and akv = 1", mdn); 
              if (query != NULL && strlen (query) > 0)
              {
                  strcat (buffer, " ");
                  strcat (buffer, query);
              }

              DbClass.sqlout ((char *) srezv.rez, 0, 9);
              DbClass.sqlout ((char *) srezv.rez_bz, 0, 73);
              DbClass.sqlout ((char *) srezv.a, 0, 14);
              DbClass.sqlout ((char *) srezv.variante, 0, 5);
              cursor = DbClass.sqlcursor (buffer);
      }
      else
      {
             sprintf (buffer, "select rez, rez_bz, a, variante "
	 			       "from prdk_k "
                       "where mdn = %hd and akv = 1 "
                       "and rez_bz matches \"%s*\"", mdn, squery); 
              if (query != NULL && strlen (query) > 0)
              {
                  strcat (buffer, " ");
                  strcat (buffer, query);
              }
              DbClass.sqlout ((char *) srezv.rez, 0, 9);
              DbClass.sqlout ((char *) srezv.rez_bz, 0, 73);
              DbClass.sqlout ((char *) srezv.a, 0, 14);
              DbClass.sqlout ((char *) srezv.variante, 0, 5);
              cursor = DbClass.sqlcursor (buffer);
      }
      if (cursor < 0) 
      {
          return -1;
      }
      DbClass.sqlin ((char *) srezv.a, 0, 14);
      DbClass.sqlout ((char *) srezv.mat, 0, 9);
      int cursor1 = DbClass.sqlcursor ("select mat from a_mat where a = ?");
      if (cursor1 < 0) 
      {
          return -1;
      }

  	  i = 0;
	  while (DbClass.sqlfetch (cursor) == 0)
      {
          DbClass.sqlopen (cursor1);
          if (DbClass.sqlfetch (cursor1) != 0)
          {
                  if (strlen (clipped (srezv.a)) < 9)
				  {
					  strcpy (srezv.mat, srezv.a);
				  }
				  else
				  {
					  continue;
				  }
          }
          strcpy (_a_bas.a_bz1, "");
          lese_a_bas (ratod (srezv.a));
          strcpy (srezv.a_bz1, _a_bas.a_bz1);
		  memcpy (&srezvtab[i], &srezv, sizeof (struct SREZV));
          FillRec (buffer, i, 512);
	      Query->InsertRecord (buffer);
		  i ++;
	  }
      rezanz = i;
	  DbClass.sqlclose (cursor);
	  DbClass.sqlclose (cursor1);

      sorez = 1;
      if (rezanz > 1)
      {
             SortABz1 (Query->GethWnd ());
      }
      UpdateList ();

	  EnableWindow (awin, FALSE);
	  return 0;
}

void SEARCHREZV::FillBox (void)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int i;

      for (i = 0; i < rezanz; i ++)
      {
		  memcpy (&srezv, &srezvtab[i],  sizeof (struct SREZV));
          FillRec (buffer, i, 512);
	      Query->InsertRecord (buffer);
      }
      if (idx > -1)
      {
	      Query->SetSel (idx);
      }
}


void SEARCHREZV::SetParams (HINSTANCE hMainInst, HWND hMainWindow)
{
      this->hMainInst   = hMainInst;
      this->hMainWindow = hMainWindow;
      awin = hMainWindow;
}


void SEARCHREZV::Search (short mdn)
{
  	  int cx, cy;
	  char buffer [512];
      RECT rect;
      BOOL doret = FALSE; 

      this->mdn = mdn;
      DlgLst::TChar = 0xFF;
      Settchar ((char) DlgLst::TChar);
      GetClientRect (hMainWindow, &rect);
      cx = 80;
      cy = 25;
      SetSortProc (SortLst);
      if (Query == NULL)
      {
        idx = -1;
        if (query != NULL && strlen (query) > 0)
        {
               Query = new CHQEX (cx, cy);
        }
        else
        {
			if (Suchvariante == 1)
			{
               Query = new CHQEX (cx, cy, "Name", "");
			}
			else
			{
               Query = new CHQEX (cx, cy);
			}
        }
        Query->EnableSort (TRUE);
        if (SetStyle)
        {
            Query->SetWindowStyle (WindowStyle);
        }
        Query->OpenWindow (hMainInst, hMainWindow, TRUE);
        FillFormat (buffer, 512);
        Query->RowAttr (buffer);
        FillVlines (buffer, 512);
	    Query->VLines (buffer, 3);
	    EnableWindow (awin, FALSE);

        FillCaption (buffer, 512);
	    Query->InsertCaption (buffer);

	    Query->SetFillDb (Read);
	    Query->SetSearchLst (SearchLst);
        if (query != NULL && strlen (query) > 0)
        {
              Read ("");
              if (rezanz == 0)
              {
                  doret = TRUE;
              }
        }
      }
      else
      {
        Query->RestorefWork ();
        if (SetStyle)
        {
            Query->SetWindowStyle (WindowStyle);
        }
        Query->OpenWindow (hMainInst, hMainWindow, TRUE);
        FillFormat (buffer, 512);
        Query->RowAttr (buffer);
        FillVlines (buffer, 512);
	    Query->VLines (buffer, 3);
	    EnableWindow (awin, FALSE);

        FillCaption (buffer, 512);
	    Query->InsertCaption (buffer);

	    Query->SetFillDb (Read);
	    Query->SetSearchLst (SearchLst);
        FillBox ();
      }
      if (doret == FALSE)
      {
          Query->ProcessMessages ();
          idx = Query->GetSel ();
      }
      else
      {
          idx = -1;
      }
      query = NULL;
      EnableWindow (awin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (awin);
      if (srezvtab == NULL)
      {
          delete Query;
          Query = NULL;
      }
	  if (idx == -1) return;
	  memcpy (&srezv, &srezvtab[idx], sizeof (srezv));
      SetSortProc (NULL);
      if (srezvtab != NULL)
      {
          Query->SavefWork ();
      }
}



