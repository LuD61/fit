//130210 RezOhneVarianten
//090507 Falsche berechnung , wenn bei verarb.mat brutto <> netto
//261005
//200505 Problem mit Seite Kosten !!!
//240305
#include<stdio.h>
#include<stdarg.h>
#include<string.h>
#include<math.h>
#include<windows.h>
#include<direct.h>
#include "stdfkt.h"
#include "strfkt.h"
#include "time.h"
#include "SpezDlg.h" 
#include "mo_gdruck.h" 
#include "colbut.h"
#include "help.h"
#include "lbox.h"
#include "prdk_k.h"
#include "reportrez.h"
#include "searchmdn.h"
#include "searchrez.h"
#include "searchvariante.h"
#include "searchmasch.h"
#include "searchvarb.h"
#include "searchzut.h"
#include "searchhuel.h"
#include "searchvpk.h"
#include "searchka.h"
#include "ptab.h"
#include "mo_menu.h"
#ifdef BIWAK
#include "conf_env.h"
#endif
#include "mo_progcfg.h"
#include "mo_wmess.h"
#include "Text.h"
#include "Progress.h"

#define MAXPHASEN 10

static int StdSize = STDSIZE;

static int dlgsize    = STDSIZE;
static int ltgraysize = STDSIZE;
static int dlgpossize = STDSIZE;
static int cpsize     = 200;
static short FlgAnteil = 1;
static short mitTabZutaten = 1;
static short mitTabQuid = 0;
static short mitTabVerpackung = 1;
static short mitTabArbeitszeit = 0;
static short mitMaschinen = 1;
static short mitRework = 0;
static short SummenAnzeige = 0;
static short flgRestartProcess = 0;
static short PreisHerkunft = 1;
static int PlanPreisHerkunft = 0;
static short TabKopf = 0;
static short TabVarb = 1;
static short TabZut = 2;
static short TabHuell = 3;
static short TabVerpack = 4;
static short TabArbZeit = 5;
static short TabKost = 6;
static short TabQuid = 7;
static double chg_gew_old = 0;
static double BearbHKold = 0;
static double BearbSKold = 0;
static double BearbFEKold = 0;
static double BearbFVKold = 0;
extern char SumAnzeige[30];

static short Ursprung_mdn = 0;
static double Ursprung_a = 0.0;
static char Ursprung_rez [9];
static short Ursprung_variante = 0;
static short flg_kopieren = 0;

bool dOnKey12 = FALSE;
char FlgKalk[2]  = {"N"};
char FlgBasis[2] = {"J"};
char *SpezDlg::PrintCommand = "rezepturdruck";


static mfont dlgfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         GRAYCOL,
                         0,
                         NULL};

static mfont rdofont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLUECOL,
                         WHITECOL,
                         0,
                         NULL};

static mfont kostrdofont = {
                         "ARIAL", STDSIZE, 0, 0,
//                         BLUECOL,
                         BLACKCOL,
                         WHITECOL,
                         0,
                         NULL};

static mfont kostrdohi = {
                         "ARIAL", STDSIZE, 0, 0,
//                         BLUECOL,
                         BLACKCOL,
                         YELLOWCOL,
                         0,
                         NULL};


static mfont ltgrayfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         LTGRAYCOL,
                        0,
                         NULL};

static mfont checkfonth = {
                           "ARIAL", STDSIZE, 0, 0,
                            BLACKCOL,
                            LTGRAYCOL,
                            0,
                            NULL};

static mfont dlgposfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         LTGRAYCOL,
                         0,
                         NULL};


static mfont cpfont = {
                        "Times New Roman", 200, 0, 1,
                         BLUECOL,
                         GRAYCOL,
                         0,
                         NULL};

static mfont Quidfontk = {
                         "ARIAL", STDSIZE, 300, 0,
                         BLUECOL,
                         LTGRAYCOL,
                         0,
                         NULL};
static mfont Quidfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         REDCOL,
                         LTGRAYCOL,
                         0,
                         NULL};
static mfont quidrdofont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         WHITECOL,
                         0,
                         NULL};

static mfont *Font = &dlgposfont;
static mfont *QuidFontk = &Quidfontk;
static mfont *QuidFont = &Quidfont;

struct PRDK_KF prdk_kf, prdk_kf_null;

#define MAXHIROWS 10
int HiKostRows [MAXHIROWS] = {0,0,0,0,0,0,0,0,0,0};

CFIELD *_fHead [] = {
                     new CFIELD ("mdn_txt", "Mandant",  0, 0, 1, 0,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("mdn", prdk_kf.mdn,  6, 0, 10, 0,  NULL, "%4d", CEDIT,
                                 MDN_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("mdn_choise", "", 2, 0, 16, 0, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("mdn_krz", prdk_kf.mdn_krz, 19, 0, 27, 0,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, 0),
                     new CFIELD ("pageinfo", prdk_kf.pageinfo, 21, 0, 63, 0,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER),

                     new CFIELD ("a_txt", "Artikel",  0, 0, 1, 2,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("a", prdk_kf.a, 14, 0, 10, 2,  NULL, "%13.0lf", CEDIT,
                                 A_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("a_choise", "", 2, 0, 24, 2, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("a_bz1", prdk_kf.a_bz1, 25, 0, 27, 2,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, 0),
                     new CFIELD ("rez_txt", "Rezeptur",  0, 0, 1, 3,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("rez", prdk_kf.rez, 9, 0, 10, 3,  NULL, "", CEDIT,
                                 REZ_CTL, Font, 0, 0),
                     new CFIELD ("rez_choise", "", 2, 0, 19, 3, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("rez_bz_rd", prdk_kf.rez_bz, 25, 0, 27, 3,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER),
                     new CFIELD ("variante_txt", "Variante",  0, 0, 55, 2,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("variante", prdk_kf.variante, 3, 0, 63, 2,  NULL, "%2hd", CEDIT,
                                 VARIANTE_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("variante_choise", "", 2, 0, 66, 2, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("variante_bz_rd", prdk_kf.variante_bz, 25, 0, 63, 3,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER),



                     new CFIELD ("a_von_txt", "Artikel von",  0, 0, 1, 2,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("a_von", prdk_kf.a_von, 14, 0, 17, 2,  NULL, "%13.0lf", CEDIT,
                                 A_VON_CTL, Font, 0, ES_RIGHT),
//                     new CFIELD ("a_von_choise", "", 2, 0, 24, 2, NULL, "", CBUTTON,
//                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("a_bis_txt", "bis",  0, 0, 39, 2,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("a_bis", prdk_kf.a_bis, 14, 0, 42, 2,  NULL, "%13.0lf", CEDIT,
                                 A_BIS_CTL, Font, 0, ES_RIGHT),
//                     new CFIELD ("a_bis_choise", "", 2, 0, 56, 2, NULL, "", CBUTTON,
//                                  VK_F9, Font, 0, BS_BITMAP),

                     new CFIELD ("ag_von_txt", "Artikelgruppe von",  0, 0, 1, 3,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("ag_von", prdk_kf.ag_von, 14, 0, 17, 3,  NULL, "%ld", CEDIT,
                                 AG_VON_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("ag_bis_txt", "bis",  0, 0, 39, 3,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("ag_bis", prdk_kf.ag_bis, 14, 0, 42, 3,  NULL, "%ld", CEDIT,
                                 AG_BIS_CTL, Font, 0, ES_RIGHT),

                     new CFIELD ("akv99", "aktive Varianten", prdk_kf.akv,
                                  20, 0, 60, 1,  NULL, "", CBUTTON,
                                 AKV_CTL, Font, 0, BS_AUTOCHECKBOX | BS_LEFTTEXT),

                     new CFIELD ("nakv99", "nicht aktive Varianten", prdk_kf.nakv,
                                  20, 0, 60, 2,  NULL, "", CBUTTON,
                                 AKV_CTL, Font, 0, BS_AUTOCHECKBOX | BS_LEFTTEXT),


                     new CFIELD ("ek_frame99", "Preisbasis",  23,4 ,59, 4,  NULL, "",
                                 CBORDER,   
                                 500, Font, 0, OPAQUE),    
                     new CFIELD ("ek99", "EK", prdk_kf.ek,
                                  10, 0, 60, 5,  NULL, "", CBUTTON,
                                 EK_CTL, Font, 0, BS_AUTORADIOBUTTON | BS_LEFTTEXT),
                     new CFIELD ("plan_ek99", "Plan-EK", prdk_kf.plan_ek,
                                  10, 0, 60, 6,  NULL, "", CBUTTON,
                                 EK_CTL, Font, 0, BS_AUTORADIOBUTTON | BS_LEFTTEXT),
                     new CFIELD ("abstand", " ",  0, 0, 0, 7,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     NULL,

};

CFORM fHead (30, _fHead);


CFIELD *_fPos [165] = {

                     new CFIELD ("pos_frame", "",    95,20, 1, 4,  NULL, "",
                                 CBORDER,   
                                 500, Font, 0, TRANSPARENT),    
                     new CFIELD ("rez_bz_txt", "Bezeichnung Rezeptur", 0, 0, 2, 5,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("rez_bz", prdk_kf.rez_bz, 66, 0, 22, 5,  NULL, "", 
                                 CEDIT,
                                 REZ_BZ_CTL, Font, 0, 0),
                     new CFIELD ("variante_bz_txt", "Bezeichnung Variante", 0, 0, 2, 6,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("variante_bz", prdk_kf.variante_bz, 25, 0, 22, 6,  NULL, "", 
                                 CEDIT,
                                 VARIANTE_BZ_CTL, Font, 0, 0),
                     new CFIELD ("phasen_txt", "Arbeitsphasen", 0, 0, 4, 8,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("manr_txt", "MaschinenNr.",
                                    0, 0, 24, 8,  NULL, "", 
                                  CREADONLY,
                                  500, Font, 0, TRANSPARENT),
                     new CFIELD ("zeit_txt", "Zeit/100kg", 0, 0, 55, 7,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("zeit_txt1", "(Minuten)", 0, 0, 55, 8,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("rework", "Rework-Artikel", prdk_kf.rework,
                                  14, 0, 70, 7,  NULL, "", CBUTTON,
                                 REWORK_CTL, Font, 0, BS_AUTOCHECKBOX | BS_LEFTTEXT),
                     new CFIELD ("akv", "Aktiv", prdk_kf.akv,
                                  7, 0, 70, 8,  NULL, "", CBUTTON,
                                 AKV_CTL, Font, 0, BS_AUTOCHECKBOX | BS_LEFTTEXT),
                     new CFIELD ("ek_frame", "Preisbasis",  20,4 ,69, 10,  NULL, "",
                                 CBORDER,   
                                 500, Font, 0, OPAQUE),    
                     new CFIELD ("ek", "EK", prdk_kf.ek,
                                  10, 0, 70, 11,  NULL, "", CBUTTON,
                                 EK_CTL, Font, 0, BS_AUTORADIOBUTTON | BS_LEFTTEXT),
                     new CFIELD ("plan_ek", "Plan-EK", prdk_kf.plan_ek,
                                  10, 0, 70, 12,  NULL, "", CBUTTON,
                                 EK_CTL, Font, 0, BS_AUTORADIOBUTTON | BS_LEFTTEXT),

/*
                     new CFIELD ("manr_txt", "MaschinenNr.",
                                    0, 0, 55, 11,  NULL, "", 
                                  CREADONLY,
                                  500, Font, 0, TRANSPARENT),
                     new CFIELD ("manr", prdk_kf.manr,
                                  10, 0, 67, 11,  NULL, "%8d", CEDIT,
                                  MANR_CTL, Font, 0, ES_RIGHT),
*/

                     new CFIELD ("sw_txt", "analyt. Schwund",
                                    0, 0, 68, 14,  NULL, "", 
                                    CREADONLY,
                                    500, Font, 0, TRANSPARENT),
                     new CFIELD ("sw_txt2", "%",
                                    0, 0, 91, 14,  NULL, "", 
                                    CREADONLY,
                                    500, Font, 0, TRANSPARENT),
                     new CFIELD ("sw_analy", prdk_kf.sw_analy,
                                  9, 0, 82, 14,  NULL, "%8.3lf", CEDIT,
                                  SW_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("swk_txt", "kalk.   Schwund",
                                    0, 0, 68, 15,  NULL, "", 
                                    CREADONLY,
                                    500, Font, 0, TRANSPARENT),
                     new CFIELD ("swk_txt2", "%",
                                    0, 0, 91, 15,  NULL, "", 
                                    CREADONLY,
                                    500, Font, 0, TRANSPARENT),
                     new CFIELD ("sw_kalk", prdk_kf.sw_kalk,
                                  9, 0, 82, 15,  NULL, "%8.3lf", CEDIT,
                                  SWA_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("chg_gew_txt", "Chargengr��e",
                                    0, 0, 69, 16,  NULL, "", 
                                    CREADONLY,
                                    500, Font, 0, TRANSPARENT),
                     new CFIELD ("chg_gew_txt2", "kg",
                                    0, 0, 91, 16,  NULL, "", 
                                    CREADONLY,
                                    500, Font, 0, TRANSPARENT),
                     new CFIELD ("chg_gew", prdk_kf.chg_gew,
                                  9, 0, 82, 16,  NULL, "%8.3lf", CEDIT,
                                  501, Font, 0, TRANSPARENT | ES_RIGHT),

                     new CFIELD ("vorlauf_txt", "Vorlaufzeit ",
                                    0, 0, 72, 18,  NULL, "", 
                                    CREADONLY,
                                    500, Font, 0, TRANSPARENT),
                     new CFIELD ("vorlauf_txt2", "Tage",
                                    0, 0, 86, 18,  NULL, "", 
                                    CREADONLY,
                                    500, Font, 0, TRANSPARENT),
                     new CFIELD ("vorlaufzeit", prdk_kf.vorlaufzeit,
                                  4, 0, 82, 18,  NULL, "%hd", CEDIT,
                                  VZEIT_CTL, Font, 0, TRANSPARENT | ES_RIGHT),
                     new CFIELD ("prod_abt_txt", "Abteilung ",
                                    0, 0, 2, 18,  NULL, "", 
                                    CREADONLY,
                                    500, Font, 0, TRANSPARENT),
//                     new CFIELD ("prod_abt", prdk_kf.prod_abt,
//                                  6, 0, 12, 18,  NULL, "%hd", CEDIT,
//                                  VZEIT_CTL, Font, 0, TRANSPARENT | ES_RIGHT),
                     new CFIELD ("prod_abt", prdk_kf.prod_abt,
                                  18, 6, 12, 18,  NULL, "%hd", CCOMBOBOX,
                                  PROD_ABT_CTL, Font, 0, CBS_DROPDOWNLIST | WS_VSCROLL),



//                     new CFIELD ("prod_abt_choise", "", 2, 0, 18, 18, NULL, "", CBUTTON,
//                                  VK_F9, Font, 0, BS_BITMAP),
//                     new CFIELD ("prod_abt_bz", prdk_kf.prod_abt_bz, 25, 0, 21, 18,  NULL, "", 
//                                 CREADONLY,
//                                 500, Font, 0, 0),
                     NULL,
};


char preiskg [15]; 
char kostenkg [15];
char gewsumme [15];

CFIELD *_fVarbFoot[] = {
                     new CFIELD ("preiskg_txt", "EK / kg in \x80", 0, 0, 5,17,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("preiskg", preiskg,   10, 0, 16, 17,  NULL, "%8.4lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("kostenkg_txt", "Kosten / kg in \x80", 0, 0,28,17,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("kostenkg", kostenkg,   10, 0, 43, 17,  NULL, "%8.4lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("gewsumme_txt", "Summe Gewicht in kg", 0, 0, 58,17,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("gewsumme", gewsumme, 13, 0, 79, 17,  NULL, "%12.3lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),

                     new CFIELD ("gewsumme_txt", &SumAnzeige, 0, 0, 94,17,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),

                     NULL
};

CFORM fVarbFoot (0, _fVarbFoot);
WMESS Wmess;


// Verarbeitungsmaterial

static char VarbMe [15];
static char VarbWrtEk [15];
static char VarbWrtBep [15];
static char VarbEk [15];
static char VarbBep [15];
static char VarbDiff [15];
static char VarbKost [15];
static char VarbKostE [15];

// Zutaten

static char ZutMe [15];
static char ZutWrtEk [15];
static char ZutWrtBep [15];
static char ZutEk [15];
static char ZutBep [15];
static char ZutDiff [15];
static char ZutKost [15];
static char ZutKostE [15];

// Huellen

static char HuelMe [15];
static char HuelWrtEk [15];
static char HuelWrtBep [15];
static char HuelEk [15];
static char HuelBep [15];
static char HuelDiff [15];
static char HuelKost [15];
static char HuelKostE [15];

// Schwund

static char SwMe [15];
static char SwWrtEk [15];
static char SwWrtBep [15];
static char SwEk [15];
static char SwBep [15];
static char SwDiff [15];
static char SwKost [15];
static char SwKostE [15];

// Verpackung

static char VpkMe [15];
static char VpkWrtEk [15];
static char VpkWrtBep [15];
static char VpkEk [15];
static char VpkBep [15];
static char VpkDiff [15];
static char VpkKost [15];
static char VpkKostE [15];

// Personalkosten

static char ApzMe [15];
static char ApzWrtEk [15];
static char ApzWrtBep [15];
static char ApzEk [15];
static char ApzBep [15];
static char ApzDiff [15];
static char ApzKost [15];
static char ApzKostE [15];

// Aufschlag

static char AfsMe [15];
static char AfsWrtEk [15];
static char AfsWrtBep [15];
static char AfsEk [15];
static char AfsBep [15];
static char AfsDiff [15];
static char AfsKost [15];
static char AfsKostE [15];

// Stueckpreis

static char KgTxt0 [80] = {"\x80/kg"};
static char EinhTxt0 [80] = {"\x80/Einheit"};
static char *EinhPos0 = &EinhTxt0[2];
static char EinhTxt [80] = {"Preis HK / Einheit"};
static char *EinhPos = &EinhTxt[11];
static char AGew [15];
static char StkEk [15];
static char StkBep [15];
static char StkEkGes [15];
static char StkBepGes [15];
static char StkSK [15];
static char KgSK [15];
static char SKKost [15];
static char SKKostE [15];
static char StkFEK [15];
static char KgFEK [15];
static char FEKKost [15];
static char FEKKostE [15];
static char StkFVK [15];
static char KgFVK [15];
static char FVKKost [15];
static char FVKKostE [15];
static char BearbHK [15];
static char BearbSK [15];
static char BearbFEK [15];
static char BearbFVK [15];

// Summen

static char SuMe [15];
static char SuWrtEk [15];
static char SuWrtBep [15];
static char SuEk [15];
static char SuBep [15];
static char SuDiff [15];
static char SuKost [15];
static char SuKostE [15];


static int prdk_k_mdn ;
static double prdk_k_a ;
static char prdk_k_rez [9] ;
static short prdk_k_var ;
double MatZutWert = 0;
double MatZutWertVgl = 0;
double HuelVpkWert = 0;
double HuelVpkWertVgl = 0;
double KostWert = 0;
double KostWertVgl = 0;
double StkGesWert = 0;
double StkGesWertVgl = 0;
short PosHK = 8;

CFIELD *_fKost [] = {
                     new CFIELD ("me_txt", "Menge", 10, 0, 20, 0,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("wrtekxt", "Wert EK", 10, 0, 31, 0,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("wrtbeptxt", "Wert Vollk.", 10, 0, 42, 0,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("ektxt", "EK", 10, 0, 53, 0,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("ektxt", "EK", 10, 0, 53, 0,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("beptxt", "Vollk.", 10, 0, 64, 0,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("difftxt", "Differenz", 10, 0, 75, 0,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("kostentxt", "Kosten", 10, 0, 86, 0,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("kostenetxt", "Kosten \x80/kg", 11, 0, 97, 0,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),

                     new CFIELD ("kost_line", "",    107, 1, 0, 1,  NULL, "",
                                 CBORDER,   
                                 500, Font, 0, TRANSPARENT),    

// Verarbeitungsmaterial

                     new CFIELD ("kost_varb_txt", "Verarb-Mat.",  0, 0, 1, 2,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("varb_me", VarbMe,  10, 0, 20, 2,  NULL, "%8.3lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("varb_wrt_ek", VarbWrtEk,  10, 0, 31, 2,  NULL, "%8.3lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("varb_wrt_bep", VarbWrtBep,  10, 0, 42, 2,  NULL, "%8.3lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("varb_ek", VarbEk,  10, 0, 53, 2,  NULL, "%8.4lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("varb_bep", VarbBep,  10, 0, 64, 2,  NULL, "%8.4lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
//                   new CFIELD ("varb_diff", VarbDiff,  10, 0, 75, 2,  NULL, "%8.3lf", 
//                               CREADONLY,
//                               500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("varb_kost", VarbKost,  10, 0, 86, 2,  NULL, "%8.2lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("varb_koste", VarbKostE,  10, 0, 97,2,  NULL, "%8.2lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),

// Zutaten

                     new CFIELD ("kost_zut_txt", "Zutaten",  0, 0, 1, 3,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("zut_me", ZutMe,  10, 0, 20, 3,  NULL, "%8.3lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("zut_wrt_ek", ZutWrtEk,  10, 0, 31, 3,  NULL, "%8.3lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("zut_wrt_bep", ZutWrtBep,  10, 0, 42, 3,  NULL, "%8.3lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("zut_ek", ZutEk,  10, 0, 53, 3,  NULL, "%8.4lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("zut_bep", ZutBep,  10, 0, 64, 3,  NULL, "%8.4lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("zut_diff", ZutDiff,  10, 0, 75, 3,  NULL, "%8.4lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("zut_kost", ZutKost,  10, 0, 86, 3,  NULL, "%8.2lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("zut_koste", ZutKostE,  10, 0, 97, 3,  NULL, "%8.2lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),

// Huellen

                     new CFIELD ("kost_huel_txt", "H�llen",  0, 0, 1, 4,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("huel_me", HuelMe,  10, 0, 20, 4,  NULL, "%8.3lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("huel_wrt_ek", HuelWrtEk,  10, 0, 31, 4,  NULL, "%8.3lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("huel_wrt_bep", HuelWrtBep,  10, 0, 42, 4,  NULL, "%8.3lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("huel_ek", HuelEk,  10, 0, 53, 4,  NULL, "%8.4lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("huel_bep", HuelBep,  10, 0, 64, 4,  NULL, "%8.4lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("huel_diff", HuelDiff,  10, 0, 75, 4,  NULL, "%8.4lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("huel_kost", HuelKost,  10, 0, 86, 4,  NULL, "%8.2lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("huel_koste", HuelKostE,  10, 0, 97, 4,  NULL, "%8.2lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),

// Schwund

                     new CFIELD ("kost_sw_txt", "Schwund",  0, 0, 1, 5,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("sw_me", SwMe,  10, 0, 20, 5,  NULL, "%8.3lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("sw_wrt_ek", SwWrtEk,  10, 0, 31, 5,  NULL, "%8.3lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("sw_wrt_bep", SwWrtBep,  10, 0, 42, 5,  NULL, "%8.3lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("sw_ek", SwEk,  10, 0, 53, 5,  NULL, "%8.4lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("sw_bep", SwBep,  10, 0, 64, 5,  NULL, "%8.4lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("sw_diff", SwDiff,  10, 0, 75, 5,  NULL, "%8.4lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
/*
                     new CFIELD ("sw_kost", SwKost,  10, 0, 86, 5,  NULL, "%8.2lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("sw_koste", SwKostE,  10, 0, 97, 5,  NULL, "%8.2lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
*/

// Verpackung

                     new CFIELD ("kost_vpk_txt", "Verpackung",  0, 0, 1, 6,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
//                   new CFIELD ("vpk_me", VpkMe,  10, 0, 20, 6,  NULL, "%8.3lf", 
//                               CREADONLY,
//                               500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("vpk_wrt_ek", VpkWrtEk,  10, 0, 31, 6,  NULL, "%8.3lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("vpk_wrt_bep", VpkWrtBep,  10, 0, 42, 6,  NULL, "%8.3lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("vpk_ek", VpkEk,  10, 0, 53, 6,  NULL, "%8.4lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("vpk_bep", VpkBep,  10, 0, 64, 6,  NULL, "%8.4lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("vpk_diff", VpkDiff,  10, 0, 75, 6,  NULL, "%8.4lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("vpk_kost", VpkKost,  10, 0, 86, 6,  NULL, "%8.2lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("vpk_koste", VpkKostE,  10, 0, 97, 6,  NULL, "%8.2lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),



// Arbeitszeit

                     new CFIELD ("apz_txt", "Arbeitszeit",  0, 0, 1, 7,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
//                   new CFIELD ("apz_wrt_ek", ApzWrtEk,  10, 0, 31, 7,  NULL, "%8.3lf", 
//                               CREADONLY,
//                               500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("apz_wrt_bep", ApzWrtBep,  10, 0, 42, 7,  NULL, "%8.3lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("apz_ek", ApzEk,  10, 0, 53, 7,  NULL, "%8.4lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("apz_bep", ApzBep,  10, 0, 64, 7,  NULL, "%8.4lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("apz_diff", ApzDiff,  10, 0, 75, 7,  NULL, "%8.4lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),

// Aufschlag Herstellkosten

                     new CFIELD ("afs_hk_txt", "Herstellkosten",  0, 0, 1, PosHK,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("kost_hk1_txt1", "Tabelle :",  0, 0, 20, PosHK,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("bearb_hk", BearbHK,  10, 0, 30, PosHK,  NULL, "%8.0lf", 
                                 CEDIT,
                                 KOST_HK_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("afs_wrt_bep", AfsWrtBep,  10, 0, 42, PosHK,  NULL, "%8.3lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("afs_ek", AfsEk,  10, 0, 53, PosHK,  NULL, "%8.4lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("afs_bep", AfsBep,  10, 0, 64, PosHK,  NULL, "%8.4lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("afs_diff", AfsDiff,  10, 0, 75, PosHK,  NULL, "%8.4lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),

                     new CFIELD ("afs_kost", AfsKost,  10, 0, 86, PosHK,  NULL, "%8.2lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("afs_koste", AfsKostE,  10, 0, 97, PosHK,  NULL, "%8.2lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),



// St�ckpreis HK
                     new CFIELD ("kost_stk_txt", EinhTxt,  0, 0, 1, PosHK+1,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("a_gew", AGew,  10, 0, 20, PosHK+1,  NULL, "%8.4lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("stk_ek", StkEk,  10, 0, 53, PosHK+1,  NULL, "%8.4lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("stk_bep", StkBep,  10, 0, 64, PosHK+1,  NULL, "%8.4lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
//                     new CFIELD ("stk_bep_ges", StkBepGes,  10, 0, 97, 8,  NULL, "%8.3lf", 
//                     new CFIELD ("stk_bep_ges", SuBep,  10, 0, 97, 8,  NULL, "%8.3lf", 
//                                 CREADONLY,
//                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),

                     new CFIELD ("kost_stk0_txt", EinhTxt0,  0, 0, 68, PosHK+2,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("kost_kg0_txt", KgTxt0,  0, 0, 81, PosHK+2,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),

                     new CFIELD ("kost1_line", "",    107, 1, 0, PosHK+2,  NULL, "",
                                 CBORDER,   
                                 500, Font, 0, TRANSPARENT),    
// St�ckpreis SK
                     new CFIELD ("kost_sk1_txt", "SK Rampe",  0, 0, 1, PosHK+3,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("kost_sk1_txt1", "Tabelle :",  0, 0, 20, PosHK+3,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("bearb_sk", BearbSK,  10, 0, 30, PosHK+3,  NULL, "%8.0lf", 
                                 CEDIT,
                                 KOST_SK_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("kost_sk_txt", "Preis :",  0, 0, 55, PosHK+3,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("stk_sk", StkSK,  10, 0, 64, PosHK+3,  NULL, "%8.3lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("kg_sk",  KgSK,  10, 0, 75, PosHK+3,  NULL, "%8.3lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("sk_kost", SKKost,  10, 0, 86, PosHK+3,  NULL, "%8.2lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("sk_koste", SKKostE,  10, 0, 97, PosHK+3,  NULL, "%8.2lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
// St�ckpreis Fil-EK
                     new CFIELD ("kost_fek1_txt", "Filial-EK",  0, 0, 1, PosHK+4,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("kost_fek1_txt1", "Tabelle :",  0, 0, 20, PosHK+4,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("bearb_fek", BearbFEK,  10, 0, 30, PosHK+4,  NULL, "%8.0lf", 
                                 CEDIT,
                                 KOST_FEK_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("kost_fil_ek_txt", "Preis :",  0, 0, 55, PosHK+4,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("stk_fek", StkFEK,  10, 0, 64, PosHK+4,  NULL, "%8.3lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("kg_fek",  KgFEK,  10, 0, 75, PosHK+4,  NULL, "%8.3lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("fek_kost", FEKKost,  10, 0, 86, PosHK+4,  NULL, "%8.2lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("fek_koste", FEKKostE,  10, 0, 97, PosHK+4,  NULL, "%8.2lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
// St�ckpreis Fil-VK
                     new CFIELD ("kost_fvk1_txt", "Filial-VK",  0, 0, 1, PosHK+5,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("kost_fvk1_txt1", "Tabelle :",  0, 0, 20, PosHK+5,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("bearb_fvk", BearbFVK,  10, 0, 30, PosHK+5,  NULL, "%8.0lf", 
                                 CEDIT,
                                 KOST_FVK_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("kost_fil_vk_txt", "Preis :",  0, 0, 55, PosHK+5,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("stk_fvk", StkFVK,  10, 0, 64, PosHK+5,  NULL, "%8.3lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("kg_fvk",  KgFVK,  10, 0, 75, PosHK+5,  NULL, "%8.3lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("fvk_kost", FVKKost,  10, 0, 86, PosHK+5,  NULL, "%8.2lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("fvk_koste", FVKKostE,  10, 0, 97, PosHK+5,  NULL, "%8.2lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),



                     new CFIELD ("pos_line", "",    107, 1, 0, PosHK+6,  NULL, "",
                                 CBORDER,   
                                 500, Font, 0, TRANSPARENT),    
                     NULL
};

CFORM fKost (97, _fKost);

CFIELD *_fKostSu [] = {
// Summen

                     new CFIELD ("kost_su_txt", "Summen HK",  0, 0, 1, 6,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("su_me", SuMe,  10, 0, 20, 6,  NULL, "%8.3lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("su_wrt_ek", SuWrtEk,  10, 0, 31, 6,  NULL, "%8.3lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("su_wrt_bep", SuWrtBep,  10, 0, 42, 6,  NULL, "%8.3lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("su_ek", SuEk,  10, 0, 53, 6,  NULL, "%8.3lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("su_bep", SuBep,  10, 0, 64, 6,  NULL, "%8.3lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
//                     new CFIELD ("su_diff", SuDiff,  10, 0, 75, 6,  NULL, "%8.4lf", 
//                                 CREADONLY,
//                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("su_kost", SuKost,  10, 0, 86, 6,  NULL, "%8.3lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("su_koste", SuKostE,  10, 0, 97, 6,  NULL, "%8.3lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     NULL
};

CFORM fKostSu (0, _fKostSu);

CFIELD *Akv =  new CFIELD ("akv", "Aktiv", prdk_kf.akv,
                           10, 0, 55, 9,  NULL, "", CBUTTON,
                           AKV_CTL, Font, 0, BS_AUTOCHECKBOX | BS_LEFTTEXT);

CFIELD *MaNrTxt =  new CFIELD ("manr_txt", "MaschinenNr.",
                           10, 0, 55, 10,  NULL, "", CSTATIC,
                           500, Font, 0, 0);
/*
CFIELD *MaNr =  new CFIELD ("manr", prdk_kf.manr,
                           10, 0, 67, 10,  NULL, "%8d", CEDIT,
                           MANR_CTL, Font, 0, ES_RIGHT);
*/

CFIELD *_fProdPhasen [50] = {
                     NULL,
};


CFORM fProdPhasen (0, _fProdPhasen);

CFIELD *_fMaschBz [25] = {
                          NULL,
};


CFORM fMaschBz (0, _fMaschBz);

CFIELD *_fMaschZeit [25] = {
                          NULL,
};


CFORM fMaschZeit (0, _fMaschZeit);


CFORM fPos (80, _fPos); 

CFIELD *_fPos1 [] = {
/*
                     new CFIELD ("pos_frame", "",    87,13, 1, 4,  NULL, "",
                                 CBORDER,   
                                 500, Font, 0, TRANSPARENT),    
*/
                     NULL,
};


CFORM fPos1 (80, _fPos1);


CFIELD *_fPosZeit [] = {

                     new CFIELD ("Line1", "",    140,0, 0, 5,  NULL, "",
                                 CBORDER,   
                                 500, Font, 0, TRANSPARENT),    
                     new CFIELD ("bezgroesse", "Bezugsgr��e    ", FlgBasis,
                                  0, 0, 22, 6,  NULL, "", CBUTTON,
                                 EK_CTL, Font, 0, BS_AUTORADIOBUTTON | BS_LEFTTEXT),
//                     new CFIELD ("bez_gr_txt", "Bezugsgr��e", 0, 0, 20, 6,  NULL, "",                                  CREADONLY,
//                                 500, Font, 0, TRANSPARENT),


//                     new CFIELD ("kalk_gr_txt", "Kalkulationsgr��e", 0, 0, 40, 6,  NULL, "", 
//                                 CREADONLY,
//                                 500, Font, 0, TRANSPARENT),

                     new CFIELD ("kalkgroesse", "Kalkulationsgr��e    ", FlgKalk,
                                  0, 0, 42, 6,  NULL, "", CBUTTON,
                                 EK_CTL, Font, 0, BS_AUTORADIOBUTTON | BS_LEFTTEXT),

                     new CFIELD ("me_txt", "Mengen kg", 0, 0, 4, 7,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("chrg_txt", "Chargen", 0, 0, 4, 8,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("zeit_m1", prdk_kf.zeit_m1,  10, 0, 22, 7,  NULL, "%8.4lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("zeit_c1", prdk_kf.zeit_c1,  6, 0, 22, 8,  NULL, "%4d", 
                                 CEDIT,
                                 ZEIT_C1_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("zeit_m2", prdk_kf.zeit_m2,  10, 0, 42, 7,  NULL, "%8.4lf", 
                                 CEDIT,
                                 ZEIT_M2_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("zeit_c2", prdk_kf.zeit_c2,   5, 0, 42, 8,  NULL, "%4d", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("preise_txt", "Preise", 0, 0, 4, 10,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("mat_txt", "Material",  0, 0, 20, 10,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("vpk_txt", "Verpackung", 0, 0, 35, 10,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("pers_txt", "Personal", 0, 0, 50, 10,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("sachk_txt", "Sachkosten", 0, 0, 65, 10,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("Sueinh_txt", "Summe/Einh", 0, 0,80, 10,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("bzggr_txt", "Bezugsgr��e", 0, 0, 4, 11,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("calcgr_txt", "kalk. Gr��e", 0, 0, 4, 12,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),


                     new CFIELD ("zeit_p1", prdk_kf.zeit_p1,  12, 0, 21, 11,  NULL, "%8.3lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("zeit_p10", prdk_kf.zeit_p10,  12, 0, 21, 12,  NULL, "%8.3lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("zeit_p2", prdk_kf.zeit_p2,  12, 0, 36, 11,  NULL, "%8.3lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("zeit_p20", prdk_kf.zeit_p20,  12, 0, 36, 12,  NULL, "%8.3lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("zeit_p3", prdk_kf.zeit_p3,  12, 0, 51, 11,  NULL, "%8.3lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("zeit_p30", prdk_kf.zeit_p30,  12, 0, 51, 12,  NULL, "%8.3lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("zeit_p4", prdk_kf.zeit_p4,  12, 0, 66, 11,  NULL, "%8.3lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("zeit_p40", prdk_kf.zeit_p40,  12, 0, 66, 12,  NULL, "%8.3lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("zeit_p5", prdk_kf.zeit_p5,  12, 0, 81,11, NULL, "%8.3lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("zeit_p50", prdk_kf.zeit_p50, 12, 0, 81,12, NULL, "%8.3lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
                     new CFIELD ("pos_line", "",    140,1, 0, 13,  NULL, "",
                                 CBORDER,   
                                 500, Font, 0, TRANSPARENT),    
/*
                     new CFIELD ("basis_frame", "Basis",  23,4 ,92, 10,  NULL, "",
                                 CBORDER,   
                                 500, Font, 0, OPAQUE),    
                     new CFIELD ("kalkgroesse", "Kalk.", FlgKalk,
                                  10, 0, 92, 11,  NULL, "", CBUTTON,
                                 EK_CTL, Font, 0, BS_AUTORADIOBUTTON | BS_LEFTTEXT),
                     new CFIELD ("basisgroesse", "Basis", FlgBasis,
                                  10, 0, 92, 12,  NULL, "", CBUTTON,
                                 EK_CTL, Font, 0, BS_AUTORADIOBUTTON | BS_LEFTTEXT),

*/


                     NULL,
};


CFORM fPosZeit (34, _fPosZeit);

CFIELD *_fList [] = {
       			     new CFIELD ("partnerlst", "",
				                     87,7, 1,14, NULL, "", CLISTBOX,
												  PARTNER_LST_CTL, Font, 0, 0),
                     NULL,
};

CFORM fList (1, _fList);
    

CFIELD *_fFoot [] = {
                     new CFIELD ("ok",      "    OK    ", 10, 0, 0, 0,  NULL, "", 
                                 CBUTTON,
                                 OK_CTL, Font, 0, 0),
                     new CFIELD ("cancel", " Abbruch  ", 10, 0,12, 0,  NULL, "", 
                                 CBUTTON,
                                 CANCEL_CTL, Font, 0, 0),
                     NULL,
};


CFORM fFoot (2, _fFoot);


CFIELD *_fPrdk_k [] = {
                     new CFIELD ("fPrdk_k1", (CFORM *) &fHead, 0, 0, 0, 0, NULL, "", CFFORM,
                                   HEADCTL, Font, 0, 0),
                     new CFIELD ("fPrdk_k2", (CFORM *) &fPos,  0, 0, 0, 0, NULL, "", CFFORM,
                                   POSCTL, Font, 0, 0),
                     NULL,                      
};

CFORM fPrdk_k (3, _fPrdk_k);

CFIELD *_fPrdk_k0 [] = { 
                     new CFIELD ("fPrdk_k", (CFORM *) &fPrdk_k, 0, 0, 3, 0, NULL, "", CFFORM,
                                   500, Font, 0, 0),
                     NULL,
};
CFORM fPrdk_k0 (1, _fPrdk_k0);


CFIELD *_fPrdk_k1 [] = {
                     new CFIELD ("fPrdk_k1", (CFORM *) &fHead, 0, 0, 0, 0, NULL, "", CFFORM,
                                   HEADCTL, Font, 0, 0),
                     new CFIELD ("fPrdk_k2", (CFORM *) &fPos1,  0, 0, 0, 0, NULL, "", CFFORM,
                                   POSCTL, Font, 0, 0),
                     new CFIELD ("fVFood", (CFORM *) &fVarbFoot,  0, 0, 0, 0, NULL, "", CFFORM,
                                   POSCTL, Font, 0, 0),
                     NULL,                      
};

CFORM fPrdk_k1 (3, _fPrdk_k1);

CFIELD *_fPrdk_kZeit [] = {
                     new CFIELD ("fPrdk_kZeit1", (CFORM *) &fHead, 0, 0, 0, 0, NULL, "", CFFORM,
                                   HEADCTL, Font, 0, 0),
                     new CFIELD ("fPrdk_kZeit1", (CFORM *) &fPosZeit,  0, 0, 0, 0, NULL, "", CFFORM,
                                   POSCTL, Font, 0, 0),
                     NULL,                      
};

CFORM fPrdk_kZeit (2, _fPrdk_kZeit);


CFIELD *_fPrdk_k01 [] = { 
                     new CFIELD ("fPrdk_k1", (CFORM *) &fPrdk_k1, 0, 0, 3, 0, NULL, "", CFFORM,
                                   500, Font, 0, 0),
                     NULL,
};
CFORM fPrdk_k01 (1, _fPrdk_k01);

CFIELD *_fPrdk_k0Zeit [] = { 
                     new CFIELD ("fPrdk_kZeit", (CFORM *) &fPrdk_kZeit, 0, 0, 0, 0, NULL, "", CFFORM,
                                   500, Font, 0, 0),
                     NULL,
};

CFORM fPrdk_k0Zeit (1, _fPrdk_k0Zeit);


CFIELD *_fKosten [] = { 
                     new CFIELD ("fKost", (CFORM *) &fKost, 0, 0, 3, 0, NULL, "", CFFORM,
                                   500, Font, 0, 0),
                     new CFIELD ("fKostSu", (CFORM *) &fKostSu, 0, 0, 3, 0, NULL, "", CFFORM,
                                   500, Font, 0, 0),
                     NULL,
};

CFORM fKosten (2, _fKosten);

CFIELD *_fKosten0 [] = { 
                     new CFIELD ("fKosten0", (CFORM *) &fKosten, 0, 0, 0, 0, NULL, "", CFFORM,
                                   500, Font, 0, 0),
                     NULL,
};
CFORM fKosten0 (1, _fKosten0);



CFIELD *_fPosQuid [] = {
                     new CFIELD ("Line1", "",    140,0, 0, 5,  NULL, "",
                                 CBORDER,   
                                 500, Font, 0, TRANSPARENT),    
                     new CFIELD ("quid_txt", "Quid - Information", 0, 0, 4, 4,  NULL, "", 
                                 CREADONLY,
                                 500, QuidFontk, 0, TRANSPARENT),

                     new CFIELD ("quid_txt1", "Quid - Kategorie :", 0, 0, 1, 6,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("quid_kat", prdk_kf.quid_kat,  30, 0, 17, 6,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_LEFT),

                     new CFIELD ("quid_txt2", "Deklaration Fett :", 0, 0, 1, 7,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("quid_fett", prdk_kf.hinweis_fett,  30, 0, 17, 7,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_LEFT),

                     new CFIELD ("quid_txt3", "Deklaration BE :", 0, 0, 1, 8,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("quid_be", prdk_kf.hinweis_be,  30, 0, 17, 8,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_LEFT),

                     new CFIELD ("quid_txt4", "Hinweis        :", 0, 0, 1, 10,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("quid_hinweis", prdk_kf.hinweis,  50, 0, 17, 10,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_LEFT),

                     new CFIELD ("quid_hinweis2", prdk_kf.hinweis2,  90, 0, 17, 11,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_LEFT),

                     new CFIELD ("quid_txt5", "Fett rel.", 0, 0, 60, 7,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("quid_txt6", "Grenzwert", 0, 0, 71, 6,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("quid_txt7", "Ist", 0, 0, 86, 6,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("quid_fettmax", prdk_kf.fett_rel_max,  8, 0, 71, 7,  NULL, "%8.2f", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),

                     new CFIELD ("quid_fettist", prdk_kf.fett_rel_ist,  8, 0, 86, 7,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),

                     new CFIELD ("quid_txt5", "BE rel.", 0, 0, 60, 8,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("quid_bemax", prdk_kf.be_rel_max,  8, 0, 71, 8,  NULL, "%8.2f", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),

                     new CFIELD ("quid_beist", prdk_kf.be_rel_ist,  8, 0, 86, 8,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),

//========Warnungen 
                     new CFIELD ("warnung1", prdk_kf.warnung1,  80, 0, 1, 10,  NULL, "", 
                                 CREADONLY,
                                 500, QuidFont, 0, 0),
                     new CFIELD ("warnung2", prdk_kf.warnung2,  80, 0, 1, 11,  NULL, "", 
                                 CREADONLY,
                                 500, QuidFont, 0, 0),
/*
                     new CFIELD ("bezgroesse", "Bezugsgr��e    ", FlgBasis,
                                  0, 0, 22, 6,  NULL, "", CBUTTON,
                                 EK_CTL, Font, 0, BS_AUTORADIOBUTTON | BS_LEFTTEXT),

                     new CFIELD ("kalkgroesse", "Kalkulationsgr��e    ", FlgKalk,
                                  0, 0, 42, 6,  NULL, "", CBUTTON,
                                 EK_CTL, Font, 0, BS_AUTORADIOBUTTON | BS_LEFTTEXT),

                     new CFIELD ("me_txt", "Mengen kg", 0, 0, 4, 7,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("chrg_txt", "Chargen", 0, 0, 4, 8,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("zeit_m1", prdk_kf.zeit_m1,  10, 0, 22, 7,  NULL, "%8.4lf", 
                                 CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER | ES_RIGHT),
*/
                     new CFIELD ("pos_line", "",    140,1, 0, 10,  NULL, "",
                                 CBORDER,   
                                 500, QuidFont, 0, TRANSPARENT),    

                     NULL,
};


CFORM fPosQuid (37, _fPosQuid);









CFIELD *_fPrdk_kQuid [] = {
                     new CFIELD ("fPrdk_kQuid1", (CFORM *) &fHead, 0, 0, 0, 0, NULL, "", CFFORM,
                                   HEADCTL, Font, 0, 0),
                     new CFIELD ("fPrdk_kQuid1", (CFORM *) &fPosQuid,  0, 0, 0, 0, NULL, "", CFFORM,
                                   POSCTL, Font, 0, 0),
                     NULL,                      
};

CFORM fPrdk_kQuid (2, _fPrdk_kQuid);


CFIELD *_fPrdk_k0Quid [] = { 
                     new CFIELD ("fPrdk_kQuid", (CFORM *) &fPrdk_kQuid, 0, 0, 0, 0, NULL, "", CFFORM,
                                   500, Font, 0, 0),
                     NULL,
};

CFORM fPrdk_k0Quid (1, _fPrdk_k0Quid);











Work SpezDlg::work;
char *SpezDlg::HelpName = "23201.cmd";
SpezDlg *SpezDlg::ActiveSpezDlg = NULL;
SEARCHA SpezDlg::SearchA;
HBITMAP SpezDlg::SelBmp;
BitImage SpezDlg::ImageDelete;
BitImage SpezDlg::ImageInsert;
COLORREF SpezDlg::SysBkColor = LTGRAYCOL;
char SpezDlg::ptwert[5];
BOOL SpezDlg::DlgCenterX = FALSE;
BOOL SpezDlg::DlgCenterY = FALSE;
BOOL SpezDlg::LockPage = TRUE;
int  SpezDlg::KostStyle = BORDER;
int  SpezDlg::Artikelauswahl = 0;
char SpezDlg::kalkzeit[12];
char SpezDlg::WordPath[512] = {""};;
char SpezDlg::DocPath[512]  = {"doc"};
double SpezDlg::SaveChg = 1.0;
double SpezDlg::SaveMe = 1.0;

char SpezDlg::ProdPhasenTxt0[20][37] = {"Phase 1\0                          ",
                                        "Phase 2\0                          ",
                                        "Phase 3\0                          ",
                                        "Phase 4\0                          ",
                                        "\0                                 ",
};

char *SpezDlg::ProdPhasenTxt [] = {ProdPhasenTxt0[0],
                                   ProdPhasenTxt0[1],
                                   ProdPhasenTxt0[2],
                                   ProdPhasenTxt0[3],
                                   ProdPhasenTxt0[4],
                                   ProdPhasenTxt0[5],
                                   ProdPhasenTxt0[6],
                                   ProdPhasenTxt0[7],
                                   ProdPhasenTxt0[8],
                                   ProdPhasenTxt0[9],
                                   NULL,
                                   ProdPhasenTxt0[10],
                                   ProdPhasenTxt0[11],
                                   ProdPhasenTxt0[12],
                                   ProdPhasenTxt0[13],
                                   ProdPhasenTxt0[14],
                                   ProdPhasenTxt0[15],
                                   ProdPhasenTxt0[16],
                                   ProdPhasenTxt0[17],
                                   ProdPhasenTxt0[18],
                                   ProdPhasenTxt0[19],
                                   NULL,
};


char *SpezDlg::ProdPhasen[] = {"N",
                               "N",
                               "N",
                               "N",
                               "N",
                               "N",
                               "N",
                               "N",
                               "N",
                               "N",
                               "N",
                               "N",
                               "N",
                               "N",
                               "N",
                               "N",
                               "N",
                               "N",
                               "N",
                               "N",
                               NULL
};

char *SpezDlg::Masch[10];
char *SpezDlg::MaschZeit[10];
char *SpezDlg::MaschBz[10];

long *SpezDlg::PrdkMasch[] = {&prdk_k.masch_nr1,
                              &prdk_k.masch_nr2,
                              &prdk_k.masch_nr3,
                              &prdk_k.masch_nr4,
                              &prdk_k.masch_nr5,
                              &prdk_k.masch_nr6,
                              &prdk_k.masch_nr7,
                              &prdk_k.masch_nr8,
                              &prdk_k.masch_nr9,
                              &prdk_k.masch_nr10,
                              NULL,
};

short *SpezDlg::PrdkMaschZeit[] =  {&prdk_k.masch1_zeit,
									&prdk_k.masch2_zeit,
									&prdk_k.masch3_zeit,
									&prdk_k.masch4_zeit,
									&prdk_k.masch5_zeit,
									&prdk_k.masch6_zeit,
									&prdk_k.masch7_zeit,
									&prdk_k.masch8_zeit,
									&prdk_k.masch9_zeit,
									&prdk_k.masch10_zeit,
									NULL,
};


/*
void SpezDlg::SetListDimension (int cx, int cy)
{
         _fList[0]->SetCX (cx);
         _fList[0]->SetCY (cy);
}

void SpezDlg::DiffListDimension (int cx, int cy)
{
         _fList[0]->SetCX (_fList[0]->GetCXorg () + cx);
         _fList[0]->SetCY (_fList[0]->GetCYorg () + cy);
}
*/

void SpezDlg::SetHiKostRows (char *rows)
{
	char *row;
	Token t;
	
    t.SetSep (";");
	t = rows;
    int i = 0;
    while ((row = t.NextToken ()) != NULL) 
	{
		if (i >= MAXHIROWS) break;
		HiKostRows[i] = atoi (row);
		i ++;
	}
}

void SpezDlg::SetHiKostColor (COLORREF Color)
{
	kostrdohi.FontBkColor = Color;
}

void SpezDlg::SetTabZutaten (char * c)
{
	mitTabZutaten = atoi (c);
}
void SpezDlg::SetTabQuid (char * c)
{
	mitTabQuid = atoi (c);
}
short SpezDlg::GetTabZutaten (void)
{
	return mitTabZutaten ;
}
short SpezDlg::GetFlgAnteil (void)
{
	return FlgAnteil ;
}
short SpezDlg::GetTabQuid (void)
{
	return mitTabQuid ;
}

void SpezDlg::GetFlgBasis (char * flg)
{
	strcpy(flg,FlgBasis) ;
}

void SpezDlg::SetMaschinen (char * c)
{
	mitMaschinen = atoi (c);
}
void SpezDlg::SetRework (char * c)
{
	mitRework = atoi (c);
}
void SpezDlg::SetSummenAnzeige (char * c)
{
	SummenAnzeige = atoi (c);
}
void SpezDlg::SetRestartProcess (char * c)
{
	flgRestartProcess = atoi (c);
}
void SpezDlg::SetPreisHerkunft (char * c)
{
	PreisHerkunft = atoi (c);

}
void SpezDlg::SetPlanPreisHerkunft (char * c)
{
	PlanPreisHerkunft = atoi (c);

}
short SpezDlg::GetPreisHerkunft (void)
{
	return PreisHerkunft ;

}
int  SpezDlg::GetPlanPreisHerkunft (void)
{
	return PlanPreisHerkunft ;

}
short SpezDlg::GetMaschinen (void)
{
	return mitMaschinen ;
}
short SpezDlg::GetRework (void)
{
	return mitRework ;
}
void SpezDlg::SetTabVerpackung (char * c)
{
	mitTabVerpackung = atoi (c);
}
void SpezDlg::SetRestartProzess (char * c)
{
	flgRestartProcess = atoi (c);
}
void SpezDlg::SetFlgAnteil (short s)
{
	FlgAnteil = s;
}
short SpezDlg::GetTabVerpackung (void)
{
	return mitTabVerpackung ;
}
void SpezDlg::SetTabArbeitszeit (char * c)
{
	mitTabArbeitszeit = atoi (c);
}
short SpezDlg::GetTabArbeitszeit (void)
{
	return mitTabArbeitszeit ;
}

int SpezDlg::ReadMdn (void)
{
         if (syskey == 0) 
		 {
	         work.GetMdnName (prdk_kf.mdn_krz,   atoi (prdk_kf.mdn));
//	         fHead.SetText ();
			 return 0;
		 }
         if (syskey == KEY9) return 0;
         if (syskey == KEY5) return 0;

         GetPosText (); 
         FromForm (dbheadfields);
         work.GetMdnName (prdk_kf.mdn_krz,   atoi (prdk_kf.mdn));
/***
         if (atoi (prdk_kf.mdn) == 0)
         {
             disp_mess ("Mandant <> 0 eingeben", 2);
             fHead.SetCurrentName ("mdn");
         }
*/
         if (syskey == KEYUP)
         {
                    fHead.SetCurrentName ("kun");
         }
         fHead.SetText ();
         return 0;
}

int SpezDlg::SavefromChg (void)
{

		 fPosZeit.GetCfield ("zeit_c1")->GetText ();
	     ActiveSpezDlg->CalcPersKosten ();

		 return 0;
}

int SpezDlg::SavefromMe (void)
{

	     ActiveSpezDlg->CalcPersKosten ();

		 return 0;
}
void SpezDlg::CalcPersKosten ()
{
		 double c1 = ratod (prdk_kf.zeit_c1);
		 double m1 = ratod (prdk_kf.zeit_m1);
		 double c2 = ratod (prdk_kf.zeit_c2);
		 double m2 = ratod (prdk_kf.zeit_m2);
	     if (ZeitLst != NULL)
		 {
//			 if (abfragejn (hWnd, "Neu berechnen ?", "J"))
			 if (strcmp(FlgBasis, "J") == 0) 
			 {
				  ZeitLst->CalcPersKosten (c1 / SaveChg, m1 / SaveMe );
		  		  SaveChg = c1;
		  		  SaveMe = m1;
			 }
			 else
			 {
				  ZeitLst->CalcPersKosten (c2 / SaveChg, m2 / SaveMe );
		  		  SaveChg = c2;
		  		  SaveMe = m2;
			 }

//			  fPrdk_k0Zeit.GetCfield ("zeit_m2")->SetFocus ();
		 }
}

void SpezDlg::CalcMeKosten ()
{
	     if (ZeitLst != NULL)
		 {
//			  ZeitLst->CalcPersKosten (1);
			  ZeitLst->CalcPersKosten ();
		 }
}

int SpezDlg::GetPutfromHKTab (void)
{
         CFIELD *Cfield;

         if (syskey == 0) return 0;
         if (syskey == KEY9) return 0;
         if (syskey == KEY5) return 0;

		 fKost.GetText ();
		 if (ActiveDlg->GetDialog () == &fKosten0)
		 {
		      fKost.SetText ();
	   		  FromForm (dbfields);

		 }
		 if (BearbHKold != ratod(BearbHK))
		 {
			   
//			if (ActiveSpezDlg->KostRecanz())
//			{
//050210 Immer �bernehmen !! sonst sind sie nach dem Abspeichern weg !!     			if (abfragejn (ActiveDlg->GethWnd (), "Kosten �bernehmen ?", "J"))
//				{
	 				ActiveSpezDlg->ShowKostLst();
//				}
				BearbHKold = ratod(BearbHK);
				Cfield = ActiveDlg->GetCurrentCfield ();
				Cfield->SetFocus ();
//			}
//			else
//			{
// 				ActiveSpezDlg->ShowKostLst();
//			}

		 }
	return 0;
}
int SpezDlg::GetPutfromSKTab (void)
{
         CFIELD *Cfield;
         if (syskey == 0) return 0;
         if (syskey == KEY9) return 0;
         if (syskey == KEY5) return 0;

		 fKost.GetText ();
		 if (ActiveDlg->GetDialog () == &fKosten0)
		 {
		      fKost.SetText ();
	   		  FromForm (dbfields);

		 }
		 if (BearbSKold != ratod(BearbSK))
		 {
//050210 Immer �bernehmen !! sonst sind sie nach dem Abspeichern weg !!     			if (abfragejn (ActiveDlg->GethWnd (), "Kosten �bernehmen ?", "J"))
//			{
				ActiveSpezDlg->ShowKostLst();
//			}
			BearbSKold = ratod(BearbSK);
            Cfield = ActiveDlg->GetCurrentCfield ();
            Cfield->SetFocus ();
		 }
	return 0;
}
int SpezDlg::GetPutfromFEKTab (void)
{
         CFIELD *Cfield;
         if (syskey == 0) return 0;
         if (syskey == KEY9) return 0;
         if (syskey == KEY5) return 0;

		 fKost.GetText ();
		 if (ActiveDlg->GetDialog () == &fKosten0)
		 {
		      fKost.SetText ();
	   		  FromForm (dbfields);

		 }
		 if (BearbFEKold != ratod(BearbFEK))
		 {
//050210 Immer �bernehmen !! sonst sind sie nach dem Abspeichern weg !!     			if (abfragejn (ActiveDlg->GethWnd (), "Kosten �bernehmen ?", "J"))
//			{
				ActiveSpezDlg->ShowKostLst();
//			}
			BearbFEKold = ratod(BearbFEK);
            Cfield = ActiveDlg->GetCurrentCfield ();
            Cfield->SetFocus ();
		 }
	return 0;
}
int SpezDlg::GetPutfromFVKTab (void)
{
         CFIELD *Cfield;
         if (syskey == 0) return 0;
         if (syskey == KEY9) return 0;
         if (syskey == KEY5) return 0;

		 fKost.GetText ();
		 if (ActiveDlg->GetDialog () == &fKosten0)
		 {
		      fKost.SetText ();
	   		  FromForm (dbfields);

		 }
		 if (BearbFVKold != ratod(BearbFVK))
		 {
//050210 Immer �bernehmen !! sonst sind sie nach dem Abspeichern weg !!     			if (abfragejn (ActiveDlg->GethWnd (), "Kosten �bernehmen ?", "J"))
//			{
				ActiveSpezDlg->ShowKostLst();
//			}
			BearbFVKold = ratod(BearbFVK);
            Cfield = ActiveDlg->GetCurrentCfield ();
            Cfield->SetFocus ();
		 }
	return 0;
}

int SpezDlg::KostRecanz (void) 
{
	if (KostLst == NULL) return 0;
	return KostLst->GetRecanz(); 
}

void SpezDlg::ShowKostLst (void)
{
	/*
			   if (KostLst != NULL)
			   {
                   KostLst->DestroyWindows ();
                   delete KostLst;
                   KostLst = NULL;
			   }
               KostLst = new KOSTLST ();
		       KostLst->SetWork (&work);
			   KostLst->SetDlg (this);
			   */
    KostLst->ShowDB();
    KostLst->ShowList();

}		


int SpezDlg::CalcfromChg (void)
{
         if (syskey == 0) return 0;
         if (syskey == KEY9) return 0;
         if (syskey == KEY5) return 0;

		 fPosZeit.GetText ();
		 double c1 = ratod (prdk_kf.zeit_c1);
//		 prdk_k.chg_anz_bzg  = atoi (prdk_kf.zeit_c1);
		 double m1 = c1 * prdk_k.chg_gew;
		 sprintf (prdk_kf.zeit_m1, "%lf", m1);
		 if (ActiveDlg->GetDialog () == &fPrdk_k0Zeit)
		 {
		      fPosZeit.SetText ();
		 }
		 if (SaveChg != c1)
		 {
			 ActiveSpezDlg->CalcPersKosten ();
		 }
		 NeuCalc();
		 return 0;
}

int SpezDlg::CalcfromMe (void)
{
         if (syskey == 0) return 0;
         if (syskey == KEY9) return 0;
         if (syskey == KEY5) return 0;

		 fPosZeit.GetText ();
		 double m2 = ratod (prdk_kf.zeit_m2);
		 double c2 = m2 / prdk_k.chg_gew;
		 int zeit_c2 = (int) c2;
		 zeit_c2 = ((double) zeit_c2 < c2) ? zeit_c2 + 1 : zeit_c2;
		 sprintf (prdk_kf.zeit_c2, "%d", zeit_c2);
		 if (ActiveDlg->GetDialog () == &fPrdk_k0Zeit)
		 {
		      fPosZeit.SetText ();
		 }
 	     ActiveSpezDlg->CalcMeKosten ();
		 NeuCalc();
		 return 0;
}

int SpezDlg::AListe (void)
{
      fHead.GetText ();
	  if (A_Lst != NULL)
	  {	
	      A_Lst->StopList ();
   		  A_Lst->DestroyWindows ();
		  delete A_Lst;
		  A_Lst = NULL;
	  }

	  if (A_Lst == NULL)
       {
              A_Lst = new A_LST ();
       }
       Enable (&fPrdk_k, EnableHead, FALSE); 
	   EnterALst ();
       Enable (&fPrdk_k, EnableHead, TRUE);
	   /*
       if (VarbLst == NULL)
       {
              VarbLst = new VARBLST ();
       }
	   EnterVarbLst ();
	   */
  		if (sys_ben.zahl == 99) 
		{
//  		  Wmess.Destroy ();
			if (syskey == KEY5)
			{
				if (abfragejn (hWnd, "Batchlauf starten ?", "N"))
				{
					return OnKey12 ();
				}
			}
			else
			{
				return OnKey12 ();
			}
		}

	    return 0;
}


int SpezDlg::FillBz (void)
{
         if (syskey == 0) return 0;
         if (syskey == KEY5) return 0;

         fPos.GetText ();
         fHead.SetText ();
         return 0;
}

int SpezDlg::CalcChg (void)
{
	     if (chg_gew_old == 0.0) return 0;
	     double d1 = fabs(ratod(prdk_kf.chg_gew) - chg_gew_old);
         if (d1 > 0.001)
		 {
			double faktor = ratod(prdk_kf.chg_gew) / chg_gew_old;
			work.SetChgFaktor(faktor);
			work.rechne_zeit();
			strcpy(prdk_kf.zeit_m1, prdk_kf.chg_gew);
			prdk_k.chg_gew = ratod(prdk_kf.chg_gew);
			prdk_k.kutter_gew = prdk_k.chg_gew;
/*
            if (ZeitLst == NULL)
            {
                    ZeitLst = new ZEITLST ();
            }
	        ZeitLst->SetWork (&work);
		    ZeitLst->SetDlg (this);
	        ZeitLst->FillDB ();
           SetDialog (&fPrdk_k0Zeit);
*/
//             EnterZeitLst ();
//	ZeitLst->CalcPersKosten(faktor,faktor);
//			CalcNewKost ();
		 }

         return 0;
}

int SpezDlg::SavefromChgGew (void)
{
		 fPos.GetCfield ("chg_gew")->GetText ();
		 chg_gew_old = ratod(prdk_kf.chg_gew);
         return 0;
}
int SpezDlg::CalcNeuChgGew (void)
{
		 if (chg_gew_old == ratod(prdk_kf.chg_gew)) return 0;
		 dOnKey12 = TRUE;
         return 0;
}
int SpezDlg::SavefromHKTab (void)
{
		 fKost.GetCfield ("bearb_hk")->GetText ();
	     BearbHKold = ratod(BearbHK);
         return 0;
}
int SpezDlg::SavefromSKTab (void)
{
		 fKost.GetCfield ("bearb_sk")->GetText ();
	     BearbSKold = ratod(BearbSK);
         return 0;
}
int SpezDlg::SavefromFEKTab (void)
{
		 fKost.GetCfield ("bearb_fek")->GetText ();
	     BearbFEKold = ratod(BearbFEK);
         return 0;
}
int SpezDlg::SavefromFVKTab (void)
{
		 fKost.GetCfield ("bearb_fvk")->GetText ();
	     BearbFVKold = ratod(BearbFVK);
         return 0;
}
void SpezDlg::EnableAnteil (bool flgaktiv)
{
         if (ActiveSpezDlg->GetToolbar2 () != NULL)
         {
                 if  (ActiveSpezDlg->GetToolbar2 ()->GetCfield ("Anteil") != NULL)
                 {
                      ActiveSpezDlg->GetToolbar2 ()->GetCfield ("Anteil")->Enable (flgaktiv);
                 }
		 }

}
int SpezDlg::EnterPos (void)
{
         switch (syskey)
         {
               case KEYDOWN :
               case KEYTAB :
                   fHead.SetCurrentName ("mdn");
                   return TRUE;
         }
        
         if (DLG::MousePressed)
         {
             return FALSE;
         }

         if (syskey != KEYCR)
         {
             return FALSE;
         }
         
         fHead.GetText ();
         strcpy (prdk_kf.rez, a_eig.rez);  //#270403 LuD

         FromForm (dbheadfields);

//         work.BeginWork ();
         ToolBar_SetState (ActiveDlg->GethwndTB (),   IDM_DELALL, TBSTATE_ENABLED);
         EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELETE,   MF_ENABLED);
         EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELALL,   MF_ENABLED);
         EnableMenuItem (ActiveDlg->GethMenu (),      IDM_INSERT,    MF_ENABLED);
//         EnableMenuItem (ActiveDlg->GethMenu (),      VK_F10,       MF_ENABLED);
         EnableMenuItem (ActiveDlg->GethMenu (),      VK_F11,       MF_ENABLED);
             if (sys_ben.berecht >= 3)
             {

	               if (ActiveSpezDlg->GetToolbar2 () != NULL)
		           {
	
	                 ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f12")->GetFeld ())->bmp = 
						 ImageInsert.Image;
					ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f12")->Enable (TRUE);

					}

             }
         if (ActiveSpezDlg->GetToolbar2 () != NULL)
         {
                 ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("del")->GetFeld ())->bmp = 
					 ImageDelete.Image;
                 ActiveSpezDlg->GetToolbar2 ()->GetCfield ("del")->Enable (TRUE);

                 ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("insert")->GetFeld ())->bmp = 
					 ImageInsert.Image;
                 ActiveSpezDlg->GetToolbar2 ()->GetCfield ("insert")->Enable (TRUE);

                 if  (ActiveSpezDlg->GetToolbar2 ()->GetCfield ("Texte") != NULL)
                 {
                      ActiveSpezDlg->GetToolbar2 ()->GetCfield ("Texte")->Enable (TRUE);
                 }

                 ActiveSpezDlg->GetToolbar2 ()->display ();
         }
         Enable (&fPrdk_k, EnableHead, FALSE); 
         Enable (&fPrdk_k, EnablePos,  TRUE); 
         ActiveSpezDlg->EnablePhasen (TRUE);
         fPos.SetCurrentName ("rez_bz");

         return 0;
}

/*
int SpezDlg::ListChanged (WPARAM wParam, LPARAM lParam)
{
        return TRUE;
}

*/

void SpezDlg::setWindowText (short di)
{
	 char WindowText [250];
     if (di == 0)
	 {
	     SetWindowText(ActiveDlg->GethMainWindow (),"Rezepturen");
		 return;
	 }
	 sprintf(WindowText,"Rezepturen     %.0lf   %s%s  %s" ,_a_bas.a,clipped(_a_bas.a_bz1),clipped(_a_bas.a_bz2),clipped(prdk_k.variante_bz));
     if (strcmp (prdk_kf.akv, "N") == 0)
     {
    	 sprintf(WindowText,"Rezepturen     %.0lf   %s%s  %s ==> NICHT AKTIV " ,_a_bas.a,clipped(_a_bas.a_bz1),clipped(_a_bas.a_bz2),clipped(prdk_k.variante_bz));
	 }

     if (strcmp (prdk_kf.plan_ek, "J") == 0)
     {
		 strcat(WindowText,"                        ");
		 strcat(WindowText," ===> Preisbasis ist Plan-EK !!");
	 }
     SetWindowText(ActiveDlg->GethMainWindow (),clipped(WindowText));
}

void SpezDlg::GetPosText (void)
{
         switch (ActiveDlg->GetiPage ())
         {
               case 0 :
                    fPos.GetText ();
                    break;
               case 1 :
                    fPos1.GetText ();
                    break;
         }
}
         
void SpezDlg::SetPosText (void)
{
         switch (ActiveDlg->GetiPage ())
         {
               case 0 :
                    fPos.SetText ();
                    break;
               case 1 :
                    fPos1.SetText ();
                    break;
         }
}

         
int SpezDlg::ReadPrdk_k (void)
{
         int dsqlstatus;

         GetPosText ();
         clipped (prdk_kf.rez);
         if (strcmp (prdk_kf.rez, " ") <= 0)
         {
             disp_mess ("Keine Rezeptur-Nummer angegeben", 2);
             return 100;
         }

         strcpy (prdk_k.rez_bz, "");

         FromForm (dbheadfields);
         work.BeginWork ();
		 /* 260403 LuD Suche nur ! �ber eindeutigen Schl�ssel a in prdk_k 
         dsqlstatus = work.ReadPrdk_k 
                          (prdk_k.mdn, prdk_kf.rez, atoi (prdk_kf.variante));
         if (dsqlstatus != 0 && ratod (prdk_kf.a) != 0.0)
         {
                 dsqlstatus = work.ReadPrdk_k 
                             (atoi (prdk_kf.mdn), ratod (prdk_kf.a), atoi (prdk_kf.variante));
         }
		 */
         dsqlstatus = work.ReadPrdk_k 
                      (atoi (prdk_kf.mdn), ratod (prdk_kf.a), atoi (prdk_kf.variante));
         if (prdk_k.manr == 1)
         {
                 strcpy (prdk_kf.plan_ek, "J");
                 strcpy (prdk_kf.ek, "N");
         }
		 else
         {
                 strcpy (prdk_kf.plan_ek, "N");
                 strcpy (prdk_kf.ek, "J");
         }
/*
         else
         {
               dsqlstatus = work.ReadPrdk_k 
                               (prdk_k.mdn, prdk_kf.rez, atoi (prdk_kf.variante));
         }
*/


         if (dsqlstatus == 0)
         {
                ToForm (dbheadfields); 
                work.GetABez (prdk_kf.a_bz1, prdk_k.a,sys_ben.pers_nam);
                for (int i = 0; prdk_k.prodphase [i] > ' '; i ++)
                {
                        ProdPhasen[i][0] = prdk_k.prodphase [i];
                        if (i < MAXPHASEN && Masch[i] != NULL)
                        {
                              sprintf (MaschZeit[i], "%hd", *PrdkMaschZeit[i]);
                              sprintf (Masch[i], "%ld", *PrdkMasch[i]);
                              work.ReadMasch (MaschBz[i], *PrdkMasch[i]);
                        }
                }
				clipped (prdk_kf.rez_bz);
				if (strcmp (prdk_kf.rez_bz, " ") <= 0)
				{
			            strcpy (prdk_k.rez_bz, prdk_kf.a_bz1);
				}
         }
         else
         {
                
             if (work.CopyActiveVar (prdk_k.mdn, prdk_k.rez, prdk_k.a, prdk_k.variante) == 0)
             {

                ToForm (dbheadfields); 
                work.GetABez (prdk_kf.a_bz1, prdk_k.a,sys_ben.pers_nam);
                        for (int i = 0; prdk_k.prodphase [i] > ' '; i ++)
                        {
                                 ProdPhasen[i][0] = prdk_k.prodphase [i];
                                 if (i < MAXPHASEN && Masch[i] != NULL)
                                 {
                                         sprintf (MaschZeit[i], "%hd", *PrdkMaschZeit[i]);
                                         sprintf (Masch[i], "%ld", *PrdkMasch[i]);
                                         work.ReadMasch (MaschBz[i], *PrdkMasch[i]);
                                 }
                        }
				        clipped (prdk_kf.rez_bz);
				        if (strcmp (prdk_kf.rez_bz, " ") <= 0)
                        {
			                      strcpy (prdk_k.rez_bz, prdk_kf.a_bz1);
                        }
               }
               else
               {
                       strcpy (prdk_k.rez_bz, prdk_kf.a_bz1);
					   if (Ursprung_a > 0.0) flg_kopieren = 1;
               }
               prdk_k.akv = 1;
               prdk_k.rework = 0;
         }
         ToForm (dbfields); 
		 SaveChg = ratod (prdk_kf.zeit_c1);
		 CalcfromChg ();
		 CalcfromMe ();
         SetCombos ();
         if (prdk_k.akv)
         {
                 strcpy (prdk_kf.akv, "J");
         }
         else
         {
                 strcpy (prdk_kf.akv, "N");
         }
         if (prdk_k.rework)
         {
                 strcpy (prdk_kf.rework, "J");
         }
         else
         {
                 strcpy (prdk_kf.rework, "N");
         }
//         fHead.SetText ();
         SetPosText (); 
		 setWindowText (1);
         return 0;
}


int SpezDlg::GetPtab (void)
{
         CFIELD *Cfield;


         GetPosText (); 
         Cfield = ActiveDlg->GetCurrentCfield ();
         return 0;
}


ItProg *SpezDlg::After [] = {
                                   new ItProg ("mdn",         ReadMdn),
                                   new ItProg ("rez_bz",      FillBz),
                                   new ItProg ("variante_bz", FillBz),
								   new ItProg ("chg_gew",  CalcNeuChgGew),
                                   NULL,
};

ItProg *SpezDlg::After1 [] = {
                                   NULL,
};

ItProg *SpezDlg::AfterZeit [] = {
                                   new ItProg ("zeit_c1",     CalcfromChg),
                                   new ItProg ("zeit_m2",     CalcfromMe),
                                   NULL,
};
ItProg *SpezDlg::AfterQuid [] = {
                                   NULL,
};

ItProg *SpezDlg::AfterKost [] = {
                                   new ItProg ("bearb_hk",     GetPutfromHKTab),
                                   new ItProg ("bearb_sk",     GetPutfromSKTab),
                                   new ItProg ("bearb_fek",     GetPutfromFEKTab),
                                   new ItProg ("bearb_fvk",     GetPutfromFVKTab),
                                   NULL,
};
ItProg *SpezDlg::Before [] = {
									new ItProg ("zeit_c1",  SavefromChg),
									new ItProg ("zeit_m2",  SavefromMe),
									new ItProg ("chg_gew",  SavefromChgGew),
                                   new ItProg ("bearb_hk",     SavefromHKTab),
                                   new ItProg ("bearb_sk",     SavefromSKTab),
                                   new ItProg ("bearb_fek",     SavefromFEKTab),
                                   new ItProg ("bearb_fvk",     SavefromFVKTab),
	                          NULL,
};

ItFont *SpezDlg::WhiteRdoFont [] = {
                                  new ItFont ("chg_gew",          &dlgfont),
                                  new ItFont ("pageinfo",         &rdofont),
                                  new ItFont ("rez_bz_rd",        &rdofont),
                                  new ItFont ("variante_bz_rd",   &rdofont),
                                  new ItFont ("zeit_m1",          &kostrdofont),
                                  new ItFont ("zeit_c2",          &kostrdofont),
                                  new ItFont ("zeit_p1",          &rdofont),
                                  new ItFont ("zeit_p10",          &rdofont),
                                  new ItFont ("zeit_p2",          &rdofont),
                                  new ItFont ("zeit_p20",          &rdofont),
                                  new ItFont ("zeit_p3",          &rdofont),
                                  new ItFont ("zeit_p30",         &rdofont),
                                  new ItFont ("zeit_p4",          &rdofont),
                                  new ItFont ("zeit_p40",          &rdofont),
                                  new ItFont ("zeit_p5",          &rdofont),
                                  new ItFont ("zeit_p50",         &rdofont),
                                  NULL
};

ItFont *SpezDlg::VarbRdoFont [] = {
                                  new ItFont ("preiskg",          &rdofont),
                                  new ItFont ("kostenkg",         &rdofont),
                                  new ItFont ("gewsumme",         &rdofont),
                                  NULL
};

char *VarbRowStart = "varb_me";
char *VarbRowEnd   = "varb_koste";

char *ZutRowStart = "zut_me";
char *ZutRowEnd   = "zut_koste";

char *HuelRowStart = "huel_me";
char *HuelRowEnd   = "huel_koste";

char *SwRowStart = "sw_me";
char *SwRowEnd   = "sw_koste";

char *VpkRowStart = "vpk_me";
char *VpkRowEnd   = "vpk_koste";

char *ApzRowStart = "apz_me";
char *ApzRowEnd   = "apz_koste";

char *AfsRowStart = "afs_me";
char *AfsRowEnd   = "afs_koste";

char *KostRowsStart [] = {VarbRowStart,ZutRowStart,HuelRowStart,SwRowStart,
VpkRowStart,ApzRowStart,AfsRowStart, NULL}; 

char *KostRowsEnd [] = {VarbRowEnd,ZutRowEnd,HuelRowEnd,SwRowEnd,
VpkRowEnd,ApzRowEnd,AfsRowEnd, NULL}; 

ItFont *SpezDlg::KostRdoFont [] = {
// Verarbeitungsmaterial
                                  new ItFont ("varb_me",        &kostrdofont),
                                  new ItFont ("varb_wrt_ek",    &kostrdofont),
                                  new ItFont ("varb_wrt_bep",   &kostrdofont),
                                  new ItFont ("varb_ek",        &kostrdofont),
                                  new ItFont ("varb_bep",       &kostrdofont),
                                  new ItFont ("varb_diff",      &kostrdofont),
                                  new ItFont ("varb_kost",      &ltgrayfont),
                                  new ItFont ("varb_koste",     &ltgrayfont),

// Zutaten
                                  new ItFont ("zut_me",        &kostrdofont),
                                  new ItFont ("zut_wrt_ek",    &kostrdofont),
                                  new ItFont ("zut_wrt_bep",   &kostrdofont),
                                  new ItFont ("zut_ek",        &kostrdofont),
                                  new ItFont ("zut_bep",       &kostrdofont),
                                  new ItFont ("zut_diff",      &kostrdofont),
                                  new ItFont ("zut_kost",      &ltgrayfont),
                                  new ItFont ("zut_koste",     &ltgrayfont),

// Huellen
                                  new ItFont ("huel_me",        &kostrdofont),
                                  new ItFont ("huel_wrt_ek",    &kostrdofont),
                                  new ItFont ("huel_wrt_bep",   &kostrdofont),
                                  new ItFont ("huel_ek",        &kostrdofont),
                                  new ItFont ("huel_bep",       &kostrdofont),
                                  new ItFont ("huel_diff",      &kostrdofont),
                                  new ItFont ("huel_kost",      &ltgrayfont),
                                  new ItFont ("huel_koste",     &ltgrayfont),

// Schwund
                                  new ItFont ("sw_me",        &kostrdofont),
                                  new ItFont ("sw_wrt_ek",    &kostrdofont),
                                  new ItFont ("sw_wrt_bep",   &kostrdofont),
                                  new ItFont ("sw_ek",        &kostrdofont),
                                  new ItFont ("sw_bep",       &kostrdofont),
                                  new ItFont ("sw_diff",      &kostrdofont),
                                  new ItFont ("sw_kost",      &ltgrayfont),
                                  new ItFont ("sw_koste",     &ltgrayfont),

// Verpackung
                                  new ItFont ("vpk_me",        &kostrdofont),
                                  new ItFont ("vpk_wrt_ek",    &kostrdofont),
                                  new ItFont ("vpk_wrt_bep",   &kostrdofont),
                                  new ItFont ("vpk_ek",        &kostrdofont),
                                  new ItFont ("vpk_bep",       &kostrdofont),
                                  new ItFont ("vpk_diff",      &kostrdofont),
                                  new ItFont ("vpk_kost",      &ltgrayfont),
                                  new ItFont ("vpk_koste",     &ltgrayfont),


// Personalkosten
                                  new ItFont ("apz_me",        &kostrdofont),
                                  new ItFont ("apz_wrt_ek",    &kostrdofont),
                                  new ItFont ("apz_wrt_bep",   &kostrdofont),
                                  new ItFont ("apz_ek",        &kostrdofont),
                                  new ItFont ("apz_bep",       &kostrdofont),
                                  new ItFont ("apz_diff",      &kostrdofont),
                                  new ItFont ("apz_kost",      &ltgrayfont),
                                  new ItFont ("apz_koste",     &ltgrayfont),

// Aufschlag
                                  new ItFont ("afs_me",        &ltgrayfont),
                                  new ItFont ("afs_wrt_ek",    &ltgrayfont),
                                  new ItFont ("afs_wrt_bep",   &ltgrayfont),
                                  new ItFont ("afs_ek",        &ltgrayfont),
                                  new ItFont ("afs_bep",       &ltgrayfont),
                                  new ItFont ("afs_diff",      &ltgrayfont),
                                  new ItFont ("afs_kost",      &ltgrayfont),
                                  new ItFont ("afs_koste",     &ltgrayfont),

// Stueckpreis HK
                                  new ItFont ("a_gew",         &kostrdofont),
                                  new ItFont ("stk_ek",        &kostrdofont),
                                  new ItFont ("stk_bep",       &kostrdofont),
                                  new ItFont ("stk_bep_ges",   &kostrdofont),

// SK Rampe
                                  new ItFont ("stk_sk",        &kostrdofont),
                                  new ItFont ("kg_sk",         &kostrdofont),
                                  new ItFont ("sk_kost",       &kostrdofont),
                                  new ItFont ("sk_koste",      &kostrdofont),
// Filial EK
                                  new ItFont ("stk_fek",        &kostrdofont),
                                  new ItFont ("kg_fek",         &kostrdofont),
                                  new ItFont ("fek_kost",       &kostrdofont),
                                  new ItFont ("fek_koste",      &kostrdofont),
// Filial VK
                                  new ItFont ("stk_fvk",        &kostrdofont),
                                  new ItFont ("kg_fvk",         &kostrdofont),
                                  new ItFont ("fvk_kost",       &kostrdofont),
                                  new ItFont ("fvk_koste",      &kostrdofont),

                                  NULL
};

ItFont *SpezDlg::KostSuRdoFont [] = {
// Summen
                                  new ItFont ("su_me",        &rdofont),
                                  new ItFont ("su_wrt_ek",    &rdofont),
                                  new ItFont ("su_wrt_bep",   &rdofont),
                                  new ItFont ("su_ek",        &rdofont),
                                  new ItFont ("su_bep",       &rdofont),
                                  new ItFont ("su_diff",      &rdofont),
                                  new ItFont ("su_kost",      &rdofont),
                                  new ItFont ("su_koste",     &rdofont),
                                  NULL,
};

ItFont *SpezDlg::QuidRdoFont [] = {
                                  new ItFont ("quid_kat",        &rdofont),
                                  new ItFont ("quid_fett",    &rdofont),
                                  new ItFont ("quid_be",   &rdofont),
                                  new ItFont ("quid_fettmax",        &rdofont),
                                  new ItFont ("quid_bemax",       &rdofont),
                                  new ItFont ("quid_fettist",        &rdofont),
                                  new ItFont ("quid_beist",       &rdofont),
                                  new ItFont ("quid_hinweis",       &rdofont),
                                  NULL,
};

ItFont *SpezDlg::RdoFont [] = {
                                  NULL
};

ItFont *SpezDlg::RdoFont1 [] = {
                                  NULL
};

ItFont *SpezDlg::Font [] = {
                                  NULL
};

ItFont *SpezDlg::Font1 [] = {
                                  NULL
};

FORMFIELD *SpezDlg::dbheadfields [] = {
         new FORMFIELD ("mdn",         prdk_kf.mdn,         (short *)   &prdk_k.mdn,        FSHORT,   NULL),
         new FORMFIELD ("a",           prdk_kf.a,           (double *)  &prdk_k.a,          FDOUBLE,  NULL),
         new FORMFIELD ("rez",         prdk_kf.rez ,        (char *)    prdk_k.rez,         FCHAR,    NULL),
         new FORMFIELD ("variante",    prdk_kf.variante ,   (short *)   &prdk_k.variante,   FSHORT,    NULL),
         NULL,
};

FORMFIELD *SpezDlg::dbfields [] = {
         new FORMFIELD ("rez_bz",      prdk_kf.rez_bz ,     (char *)    prdk_k.rez_bz,         FCHAR,    NULL),
         new FORMFIELD ("variante_bz", prdk_kf.variante_bz ,(char *)    prdk_k.variante_bz,    FCHAR,    NULL),
//#150803         new FORMFIELD ("akv",         prdk_kf.akv ,        (short *)   &prdk_k.akv,        FSHORT,   NULL),
         new FORMFIELD ("chg_gew",     prdk_kf.chg_gew ,    (double *)  &prdk_k.chg_gew,       FDOUBLE,  NULL),
         new FORMFIELD ("sw_kalk",     prdk_kf.sw_kalk ,    (double *)  &prdk_k.sw_kalk,            FDOUBLE,  NULL),
         new FORMFIELD ("sw_analy",    prdk_kf.sw_analy ,         (double *)  &prdk_k.sw,            FDOUBLE,  NULL),
         new FORMFIELD ("vorlaufzeit",    prdk_kf.vorlaufzeit ,         (short *)  &prdk_k.vorlaufzeit, FSHORT,  NULL),
         new FORMFIELD ("prod_abt",    prdk_kf.prod_abt ,         (short *)  &prdk_k.prod_abt, FSHORT,  NULL),
//         new FORMFIELD ("manr",        prdk_kf.manr ,       (long *)    &prdk_k.manr,        FLONG,    NULL),
//         new FORMFIELD ("zeit_m1",     prdk_kf.zeit_m1 ,    (double *)  &prdk_k.chg_gew,       FDOUBLE,  NULL),
         new FORMFIELD ("zeit_c1",     prdk_kf.zeit_c1 ,    (short *)   &prdk_k.chg_anz_bzg,   FSHORT,  NULL),
         new FORMFIELD ("zeit_m2",     prdk_kf.zeit_m2 ,    (double *)  &prdk_k.kalk_me,       FDOUBLE,  NULL),
         new FORMFIELD ("zeit_c2",     prdk_kf.zeit_c2 ,    (short *)   &prdk_k.chg_anz_kalk,  FSHORT,  NULL),
//020304         new FORMFIELD ("zeit_p1",     prdk_kf.zeit_p1 ,    (double *)  &prdk_k.varb_hk_vollk, FDOUBLE,  NULL),
         new FORMFIELD ("zeit_p1",     prdk_kf.zeit_p1 ,    (double *)  &MatZutWert, FDOUBLE,  NULL),
         new FORMFIELD ("zeit_p10",     prdk_kf.zeit_p1 ,    (double *)  &MatZutWertVgl, FDOUBLE,  NULL),
//020304         new FORMFIELD ("zeit_p2",     prdk_kf.zeit_p2 ,    (double*)   &prdk_k.vpk_wrt,       FDOUBLE,  NULL),
         new FORMFIELD ("zeit_p2",     prdk_kf.zeit_p2 ,    (double*)   &HuelVpkWert,       FDOUBLE,  NULL),
         new FORMFIELD ("zeit_p20",     prdk_kf.zeit_p20 ,    (double*)   &HuelVpkWertVgl,       FDOUBLE,  NULL),
         new FORMFIELD ("zeit_p3",     prdk_kf.zeit_p3 ,    (double *)  &prdk_k.pers_wrt_bzg,  FDOUBLE,  NULL),
         new FORMFIELD ("zeit_p30",    prdk_kf.zeit_p3 ,    (double *)  &prdk_k.pers_wrt_kalk, FDOUBLE,  NULL),
//020304         new FORMFIELD ("zeit_p4",     prdk_kf.zeit_p4 ,    (double *)  &prdk_k.kost_hk_vollk, FDOUBLE,  NULL),
         new FORMFIELD ("zeit_p4",     prdk_kf.zeit_p4 ,    (double *)  &KostWert, FDOUBLE,  NULL),
         new FORMFIELD ("zeit_p40",     prdk_kf.zeit_p40 ,    (double *)  &KostWertVgl, FDOUBLE,  NULL),
//020304         new FORMFIELD ("zeit_p5",     prdk_kf.zeit_p5 ,    (double *)  &prdk_k.a_hk_vollk,    FDOUBLE,  NULL),
         new FORMFIELD ("zeit_p5",     prdk_kf.zeit_p5 ,    (double *)  &StkGesWert,    FDOUBLE,  NULL),
         new FORMFIELD ("zeit_p50",    prdk_kf.zeit_p50 ,   (double *)  &StkGesWertVgl,    FDOUBLE,  NULL),
         new FORMFIELD ("bearb_hk",    BearbHK ,   (double *)  &prdk_k.bearb_weg,    FDOUBLE,  NULL),
         new FORMFIELD ("bearb_sk",    BearbSK ,   (double *)  &prdk_k.bearb_weg_sk,    FDOUBLE,  NULL),
         new FORMFIELD ("bearb_fek",    BearbFEK ,   (double *)  &prdk_k.bearb_weg_fek,    FDOUBLE,  NULL),
         new FORMFIELD ("bearb_fvk",    BearbFVK ,   (double *)  &prdk_k.bearb_weg_fvk,    FDOUBLE,  NULL),
         new FORMFIELD ("kg_sk",    KgSK ,   (double *)  &prdk_k.a_sk_vollk,    FDOUBLE,  NULL),
         new FORMFIELD ("kg_fek",    KgFEK ,   (double *)  &prdk_k.a_filek_vollk,    FDOUBLE,  NULL),
         new FORMFIELD ("kg_fvk",    KgFVK ,   (double *)  &prdk_k.a_filvk_vollk,    FDOUBLE,  NULL),
         new FORMFIELD ("sk_kost",    SKKost ,   (double *)  &prdk_k.sk_wrt,    FDOUBLE,  NULL),
         new FORMFIELD ("fek_kost",    FEKKost ,   (double *)  &prdk_k.filek_wrt,    FDOUBLE,  NULL),
         new FORMFIELD ("fvk_kost",    FVKKost ,   (double *)  &prdk_k.filvk_wrt,    FDOUBLE,  NULL),
         NULL,
};

/*
FORMFIELD *SpezDlg::dbzeitfields [] = {
         new FORMFIELD ("zeit_m1",     prdk_kf.zeit_m1 ,    (double *)  &prdk_k.chg_gew,      FDOUBLE,  NULL),
         new FORMFIELD ("zeit_c1",     prdk_kf.zeit_c1 ,    (short *)   &prdk_k.chg_anz_bzg,  FSHORT,  NULL),
         new FORMFIELD ("zeit_m2",     prdk_kf.zeit_m2 ,    (double *)  &prdk_k.kalk_me,      FDOUBLE,  NULL),
         new FORMFIELD ("zeit_c2",     prdk_kf.zeit_c2 ,    (short *)   &prdk_k.chg_anz_kalk, FSHORT,  NULL),
         NULL,
};
*/
char *SpezDlg::EnableHead [] = {
                                "mdn",
                                "mdn_choise",
                                "a",
                                "a_choise",
                                "rez",
                                "rez_choise",
                                "variante",
                                "variante_choise",
                                "a_von",
//                                "a_von_choise",
                                "a_bis",
//                                "a_bis_choise",
                                "ag_von",
                                "ag_bis",
	                            "akv99",
	                            "nakv99",
                                "ek99",
                                "plan_ek99",
                                 NULL
};

char *SpezDlg::EnablePos[] = {
                               "rez_bz",
                               "variante_bz",
                               "akv",
                               "rework",
                               "manr",
                               "sw_kalk",
                               "sw_analy",
                               "vorlaufzeit",
                               "prod_abt",
//                               "prod_abt_choise",
                               "ek",
                               "plan_ek",
                               "chg_gew",
                               NULL,
};

char *SpezDlg::EnablePos1[] = {
                                NULL,
};


char *SpezDlg::EnablePosZeit[] = {
                               "zeit_c1",
                               "zeit_m2",
                               NULL,
};

char *SpezDlg::EnableKost[] = {
                               "bearb_hk",
                               "bearb_sk",
                               "bearb_fek",
                               "bearb_fvk",
                               NULL,
};

 char *SpezDlg::ProdAbtCombo[200]; 

void SpezDlg::AddRow (CFIELD *cField[], char *fieldname)
{
	       for (int i = 0; cField[i] != NULL; i ++)
		   {
			   if (strcmp (cField[i]->GetName (), fieldname) == 0)
			   {
				   break;
			   }
		   }

		   for (;cField[i] != NULL; i ++)
		   {
			   cField[i]->SetY (cField[i]->GetYorg () + 1);
		   }
}


SpezDlg::SpezDlg (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel) :
         DLG (x, y, cx, cy, Caption, Size, Pixel)
{
          
             BorderType = HIGHCOLBORDER;
             Init (x, y, cx, cy, Caption, Size, Pixel);
}

SpezDlg::SpezDlg (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel, 
                  int BorderType) :
         DLG (x, y, cx, cy, Caption, Size, Pixel)
{
          
             this->BorderType = BorderType;
             Init (x, y, cx, cy, Caption, Size, Pixel);
}

SpezDlg::SpezDlg (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel, 
                  int BorderType, int EnterM) :
         DLG (x, y, cx, cy, Caption, Size, Pixel)
{
          
             this->BorderType = BorderType;
//             EnterMode = EnterM;
             Init (x, y, cx, cy, Caption, Size, Pixel);
}

void SpezDlg::EnablePhasen (BOOL b)
{
    for (int i = 0; _fProdPhasen [i] != NULL; i += 3)
    {
        _fProdPhasen[i]->Enable (b);
    }
    for (i = 1; _fProdPhasen [i] != NULL; i += 3)
    {
        if (b == TRUE)
        {
            if (strcmp ((char *) _fProdPhasen[i - 1]->GetCheckboxFeld (), "J") != 0)
            {
                continue;
            }
        }
        _fProdPhasen[i]->Enable (b);
        _fProdPhasen[i + 1]->Enable (b);
    }
    ActiveDlg->NewTabStops (&fPrdk_k0);
}














void SpezDlg::GenPhasen (void)
{
//             int x = 21;
             int x = 4;
             int x1 = 24;
             int x2 = 37;
             int x3 = 56;
             int dy =  9;
             int y =  dy;
             char *item;
             int i;
             char wert [5];
             char ProdPhase [37];

             for (i = 0; i < MAXPHASEN; i ++)
             {
                 sprintf (wert, "%d", i + 1);
                 if (work.GetPtab (ProdPhase, "prodphase", wert) == -1)
                 {
                     break;
                 }
                 strcpy (ProdPhasenTxt0[i], ProdPhase);
                 Masch[i] = NULL;
                 MaschZeit[i] = NULL;
             }

             if (i == 0)
             {
                 for (i = 0; i < ProdPhasenTxt0[i][0] != 0; i ++);
             }

             ProdPhasenTxt[i] = NULL;


			 int z = 0;
             for (i = 0; ProdPhasenTxt[i] != NULL; i ++)
             {
                 item = new char [10];
                 sprintf (item, "phase%d", i);

                 CFIELD *Phase = new CFIELD (item, ProdPhasenTxt[i], 
                                      ProdPhasen[i],
                                      20, 0, x, y, NULL, "", CBUTTON,
                                      PHASE_CTL + i, &dlgposfont,
                                      0, BS_AUTOCHECKBOX);
                 AddCfield (&fProdPhasen, Phase);
                 AddCfield (&fPos, Phase, "akv");
                 item = new char [10];
                 sprintf (item, "masch%d", i);
                 Masch[i] = new char [12];
                 CFIELD *fMasch = new CFIELD (item, Masch[i], 9, 0, x1, y, NULL,
                                      "%8d", CEDIT,
                                      MASCH_CTL + z, &dlgposfont,
                                      0, ES_RIGHT);
                 AddCfield (&fProdPhasen, fMasch);
                 AddCfield (&fPos, fMasch, "akv");

                 item = new char [10];
                 sprintf (item, "maschz%d", i);
                 MaschZeit[i] = new char [12];
                 CFIELD *_fMaschZeit = new CFIELD (item, MaschZeit[i], 9, 0, x3, y, NULL,
                                      "%8d", CEDIT,
                                      MASCH_CTL + z + 1, &dlgposfont,
                                      0, ES_RIGHT);
//                 _fMaschZeit->SetTabstop (FALSE);
                 AddCfield (&fProdPhasen, _fMaschZeit);
                 if (i == 2) AddCfield (&fPos, _fMaschZeit, "akv"); //ToDo hier gings in den Wald warum ????? 
                 if (i <= 1) AddCfield (&fPos, _fMaschZeit, "akv");
                 if (i >= 3) AddCfield (&fPos, _fMaschZeit, "akv"); 

                 y ++;
                 z += 2;
             }

             _fProdPhasen[i * 3] = NULL;
             _fProdPhasen[i * 3 + 1] = NULL;

             y =  dy;
             for (i = 0; ProdPhasenTxt[i] != NULL; i ++)
             {
                 MaschBz[i] = new char [80];
                 strcpy (MaschBz[i], "");
                 CFIELD *_fMaschbz = new CFIELD ("masch_bz", MaschBz[i],17, 0, x2, y, NULL,
                                      "", CEDIT,
                                      MASCH_BZ_CTL + i, &dlgposfont,
                                      0, ES_AUTOHSCROLL | ES_READONLY);
                 _fMaschbz->SetTabstop (FALSE);
                 AddCfield (&fMaschBz, _fMaschbz);
                 AddCfield (&fPos, _fMaschbz, "akv");
                 y ++;
             }


             int cy = (int) (double) ((double) (i + 3) * 20 * 1.3);
             cy /= 20;
             CFIELD *PhBorder = 
                     new CFIELD ("phase_frame", "",    65,cy, 2, dy - 2,  NULL, "",
                                 CBORDER,   
                                 500, &dlgposfont, 0, TRANSPARENT);    
             AddCfield (&fPos, PhBorder);
}


void SpezDlg::NotKostBkMode (DWORD NotBkMode)
{
             for (int i = 0; KostRdoFont[i] != NULL; i ++)
             {
                 CFIELD *Cfield = fKost.GetCfield (KostRdoFont[i]->GetItem ());
                 if (Cfield == NULL)
                 {
                     continue;
                 }
                 DWORD BkMode = Cfield->GetBkMode ();
                 BkMode &=~NotBkMode;
                 Cfield->SetBkMode (BkMode);
             }
}

void SpezDlg::SetHiKost (char *start, char *end)
{
             for (int i = 0; KostRdoFont[i] != NULL; i ++)
             {
				 if (strcmp (KostRdoFont[i]->GetItem (), start) == 0)
				 {
					 break;
				 }
             }
			 for (; KostRdoFont[i] != NULL; i ++)
			 {
				 KostRdoFont[i]->SetFont (&kostrdohi);
				 if (strcmp (KostRdoFont[i]->GetItem (), end) == 0)
				 {
					 break;
				 }
			 }
}

void SpezDlg::SetHiKostRow (int row)
{
	         for (int i = 0; KostRowsStart[i] != NULL; i ++);
			 if (i < row)
			 {
				 return;
			 }
	         for (i = 0; KostRowsEnd[i] != NULL; i ++);
			 if (i < row)
			 {
				 return;
			 }
             SetHiKost (KostRowsStart[row], KostRowsEnd[row]);
}


void SpezDlg::Init (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel)
{
             PROG_CFG ProgCfg ("23201");
             int i;
             int xfull, yfull;

			 if (GetTabArbeitszeit() == FALSE)  PosHK = 7;
			 AddRow (_fKost, "afs_vpk_txt");
			 for (i = 0; i < MAXHIROWS; i ++)
			 {
				 if (HiKostRows[i] == 0) break;
			     SetHiKostRow (HiKostRows[i] - 1);
			 }
             CFORM::bottomspace = 43;
             NewCalc = FALSE;
             A_Lst = NULL;
             VarbLst = NULL;
             ZutLst = NULL;
             HuelLst = NULL;
             VpkLst = NULL;
             KostLst = NULL;
             ZeitLst = NULL;
             QuidLst = NULL;
		     work.SetChgFaktor(1);
//240305             chg_gew_old = ratod(prdk_kf.chg_gew);
			 if (sys_ben.zahl != 99 && GetMaschinen() == TRUE)
			 {
				GenPhasen ();
			 }
             if (DlgCenterX)
             {
                 _fPrdk_k0[0]->SetX (-1);
                 _fPrdk_k01[0]->SetX (-1);
             }
             if (DlgCenterY)
             {
                 _fPrdk_k0[0]->SetY (-1);
                 _fPrdk_k01[0]->SetY (-1);
             }

             if (DlgCenterX == FALSE)
             {
                 AddDlgX (&fHead, _fPrdk_k0[0]->GetXorg ());
                 AddDlgX (&fPos,  _fPrdk_k0[0]->GetXorg ());
                 AddDlgX (&fPos1, _fPrdk_k01[0]->GetXorg ());
             }

             if (DlgCenterY == FALSE)
             {
                 AddDlgY (&fHead, _fPrdk_k0[0]->GetYorg ());
                 AddDlgY (&fPos,  _fPrdk_k0[0]->GetYorg ());
                 AddDlgY (&fPos1, _fPrdk_k01[0]->GetYorg ());
             }
             ImageDelete.Image    = BMAP::LoadBitmap (hInstance, "DEL",       "DELMASK",      SysBkColor);
             ImageDelete.Inactive = BMAP::LoadBitmap (hInstance, "DELIA",     "DELMASK",      SysBkColor);
             ImageInsert.Image    = BMAP::LoadBitmap (hInstance, "INSERT",    "INSERTMASK",   SysBkColor);
             ImageInsert.Inactive = BMAP::LoadBitmap (hInstance, "INSERTIA",  "INSERTMASK",   SysBkColor);

             ActiveSpezDlg = this; 
             xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
             yfull = GetSystemMetrics (SM_CYFULLSCREEN);        


             StdSize = Size;
             dlgfont.FontHeight     = Size;
             rdofont.FontHeight     = Size;
             kostrdofont.FontHeight     = Size;
             kostrdohi.FontHeight     = Size;
             dlgposfont.FontHeight  = Size;
             ltgrayfont.FontHeight  = Size;
             cpfont.FontHeight      = cpsize;

             dlgfont.FontBkColor    = SysBkColor;
             dlgposfont.FontBkColor = SysBkColor;


             if (xfull < 1000) 
             {
                 dlgfont.FontHeight = 90;
                 rdofont.FontHeight = 90;
                 kostrdofont.FontHeight = 90;
                 kostrdohi.FontHeight = 90;
                 dlgposfont.FontHeight = 90;
                 ltgrayfont.FontHeight = 90;
                 checkfonth.FontHeight = 90;
                 cpfont.FontHeight = 150;
             }

             if (xfull < 800) 
             {

                 dlgfont.FontHeight = 70;
                 rdofont.FontHeight = 70;
                 kostrdofont.FontHeight = 70;
                 kostrdohi.FontHeight = 70;
                 dlgposfont.FontHeight = 70;
                 ltgrayfont.FontHeight = 70;
                 checkfonth.FontHeight = 70;
                 cpfont.FontHeight = 120;
             }


//             fPos.SetCfield (_fPos1); 
             fHead.SetFieldanz ();
             fPos.SetFieldanz ();
             fFoot.SetFieldanz ();
             fPrdk_k.SetFieldanz ();
             fPos1.SetFieldanz ();
             fPrdk_k1.SetFieldanz ();
             fPosZeit.SetFieldanz ();
             fPosQuid.SetFieldanz ();
             fPrdk_kZeit.SetFieldanz ();
             fPrdk_kQuid.SetFieldanz ();
             fVarbFoot.SetFieldanz ();
			 fVarbFoot.Setchpos (0, 2);
             fKost.SetFieldanz ();
             fKostSu.SetFieldanz ();
			 fKostSu.Setchpos (0, 2);
/***
             if (sys_ben.berecht >= 3)
             {

	               if (ActiveSpezDlg->GetToolbar2 () != NULL)
		           {
	
					((ColButton *)Toolbar2->GetCfield ("f12")->GetFeld ())->bmp = 
						ImageInsert.Inactive;
					ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f12")->Enable (FALSE);
					}

             }
******/
             if (sys_ben.berecht == 3) //261005 
             {
					fHead.GetCfield ("rez")->SetAttribut (CREADONLY);
			 }
             if (sys_ben.berecht > 1 && (sys_ben.berecht < 4))
             {
                 fVarbFoot.GetCfield ("preiskg_txt")->SetAttribut (CREMOVED);
                 fVarbFoot.GetCfield ("preiskg")->SetAttribut (CREMOVED);
                 fVarbFoot.GetCfield ("kostenkg_txt")->SetAttribut (CREMOVED);
                 fVarbFoot.GetCfield ("kostenkg")->SetAttribut (CREMOVED);
             }
             if (GetRework() == FALSE)
			 {
                 fPos.GetCfield ("rework")->SetAttribut (CREMOVED);
			 }
             if (GetMaschinen() == FALSE)
			 {
                 fPos.GetCfield ("phasen_txt")->SetAttribut (CREMOVED);
                 fPos.GetCfield ("manr_txt")->SetAttribut (CREMOVED);
                 fPos.GetCfield ("zeit_txt")->SetAttribut (CREMOVED);
                 fPos.GetCfield ("zeit_txt1")->SetAttribut (CREMOVED);
			 }

			 if (work.GetRezOhneVarianten() == 1) //130210
			 {
                 fHead.GetCfield ("variante_txt")->SetAttribut (CREMOVED);
                 fHead.GetCfield ("variante")->SetAttribut (CREMOVED);
                 fHead.GetCfield ("variante_choise")->SetAttribut (CREMOVED);
                 fHead.GetCfield ("variante_bz_rd")->SetAttribut (CREMOVED);
                 fPos.GetCfield ("variante_bz_txt")->SetAttribut (CREMOVED);
                 fPos.GetCfield ("variante_bz")->SetAttribut (CREMOVED);
			 }

             if (sys_ben.zahl == 99)
			 {
                 fPos.GetCfield ("pos_frame")->SetAttribut (CREMOVED);
                 fPos.GetCfield ("rez_bz_txt")->SetAttribut (CREMOVED);
                 fPos.GetCfield ("rez_bz")->SetAttribut (CREMOVED);
                 fPos.GetCfield ("variante_bz_txt")->SetAttribut (CREMOVED);
                 fPos.GetCfield ("variante_bz")->SetAttribut (CREMOVED);
                 fPos.GetCfield ("phasen_txt")->SetAttribut (CREMOVED);
                 fPos.GetCfield ("manr_txt")->SetAttribut (CREMOVED);
                 fPos.GetCfield ("zeit_txt")->SetAttribut (CREMOVED);
                 fPos.GetCfield ("zeit_txt1")->SetAttribut (CREMOVED);
                 fPos.GetCfield ("akv")->SetAttribut (CREMOVED);
                 fPos.GetCfield ("rework")->SetAttribut (CREMOVED);
                 fPos.GetCfield ("ek_frame")->SetAttribut (CREMOVED);
                 fPos.GetCfield ("ek")->SetAttribut (CREMOVED);
                 fPos.GetCfield ("plan_ek")->SetAttribut (CREMOVED);
                 fPos.GetCfield ("sw_txt")->SetAttribut (CREMOVED);
                 fPos.GetCfield ("swk_txt")->SetAttribut (CREMOVED);
                 fPos.GetCfield ("sw_txt2")->SetAttribut (CREMOVED);
                 fPos.GetCfield ("swk_txt2")->SetAttribut (CREMOVED);
                 fPos.GetCfield ("sw_kalk")->SetAttribut (CREMOVED);
                 fPos.GetCfield ("sw_analy")->SetAttribut (CREMOVED);
                 fPos.GetCfield ("vorlaufzeit")->SetAttribut (CREMOVED);
                 fPos.GetCfield ("vorlauf_txt")->SetAttribut (CREMOVED);
                 fPos.GetCfield ("vorlauf_txt2")->SetAttribut (CREMOVED);
                 fPos.GetCfield ("chg_gew_txt")->SetAttribut (CREMOVED);
                 fPos.GetCfield ("chg_gew_txt2")->SetAttribut (CREMOVED);
                 fPos.GetCfield ("chg_gew")->SetAttribut (CREMOVED);
                 fPos.GetCfield ("prod_abt_txt")->SetAttribut (CREMOVED);
                 fPos.GetCfield ("prod_abt")->SetAttribut (CREMOVED);
//                 fPos.GetCfield ("prod_abt_bz")->SetAttribut (CREMOVED);
//                 fPos.GetCfield ("prod_abt_choise")->SetAttribut (CREMOVED);


                 fHead.GetCfield ("a_txt")->SetAttribut (CREMOVED);
                 fHead.GetCfield ("a")->SetAttribut (CREMOVED);
                 fHead.GetCfield ("a_choise")->SetAttribut (CREMOVED);
                 fHead.GetCfield ("a_bz1")->SetAttribut (CREMOVED);
                 fHead.GetCfield ("rez_txt")->SetAttribut (CREMOVED);
                 fHead.GetCfield ("rez")->SetAttribut (CREMOVED);
                 fHead.GetCfield ("rez_choise")->SetAttribut (CREMOVED);
                 fHead.GetCfield ("rez_bz_rd")->SetAttribut (CREMOVED);
                 fHead.GetCfield ("variante_txt")->SetAttribut (CREMOVED);
                 fHead.GetCfield ("variante")->SetAttribut (CREMOVED);
                 fHead.GetCfield ("variante_choise")->SetAttribut (CREMOVED);
                 fHead.GetCfield ("variante_bz_rd")->SetAttribut (CREMOVED);

				 strcpy (prdk_kf.akv,"J");
				 strcpy (prdk_kf.rework,"N");
				 if (Artikelauswahl == 0)
				 {
					strcpy(prdk_kf.a_von, "1");
					strcpy(prdk_kf.a_bis, "9999999999999");
					fHead.GetCfield ("a_von")->SetAttribut (CREADONLY);
					fHead.GetCfield ("a_bis")->SetAttribut (CREADONLY);
				 } 
				 if (Artikelauswahl == 1)
				 {
					strcpy(prdk_kf.ag_von, "1");
					strcpy(prdk_kf.ag_bis, "999999");
					fHead.GetCfield ("ag_von")->SetAttribut (CREADONLY);
					fHead.GetCfield ("ag_bis")->SetAttribut (CREADONLY);
				 } 
				 if (sys_ben.fil == 1)
				 {
					strcpy(prdk_kf.a_von, "1");
					strcpy(prdk_kf.a_bis, "9999999999999");
					fHead.GetCfield ("a_von")->SetAttribut (CREADONLY);
					fHead.GetCfield ("a_bis")->SetAttribut (CREADONLY);
					strcpy(prdk_kf.ag_von, "1");
					strcpy(prdk_kf.ag_bis, "999999");
					fHead.GetCfield ("ag_von")->SetAttribut (CREADONLY);
					fHead.GetCfield ("ag_bis")->SetAttribut (CREADONLY);
				 } 

//	             fHead.GetCfield ("a_von_choise")->SetBitmap 
//		             (LoadBitmap (hInstance, "ARRDOWN"));
//	             fHead.GetCfield ("a_bis_choise")->SetBitmap 
//		             (LoadBitmap (hInstance, "ARRDOWN"));
//	             fHead.GetCfield ("a_von_choise")->SetTabstop (FALSE); 
//	             fHead.GetCfield ("a_bis_choise")->SetTabstop (FALSE); 
			 }	
			 else
			 {
                 fHead.GetCfield ("a_von_txt")->SetAttribut (CREMOVED);
                 fHead.GetCfield ("a_von")->SetAttribut (CREMOVED);
//                 fHead.GetCfield ("a_von_choise")->SetAttribut (CREMOVED);
                 fHead.GetCfield ("a_bis_txt")->SetAttribut (CREMOVED);
                 fHead.GetCfield ("a_bis")->SetAttribut (CREMOVED);
//                 fHead.GetCfield ("a_bis_choise")->SetAttribut (CREMOVED);
                 fHead.GetCfield ("ag_von_txt")->SetAttribut (CREMOVED);
                 fHead.GetCfield ("ag_von")->SetAttribut (CREMOVED);
                 fHead.GetCfield ("ag_bis_txt")->SetAttribut (CREMOVED);
                 fHead.GetCfield ("ag_bis")->SetAttribut (CREMOVED);
                 fHead.GetCfield ("akv99")->SetAttribut (CREMOVED);
                 fHead.GetCfield ("nakv99")->SetAttribut (CREMOVED);
				 fHead.GetCfield ("ek_frame99")->SetAttribut (CREMOVED);
				 fHead.GetCfield ("ek99")->SetAttribut (CREMOVED);
				 fHead.GetCfield ("plan_ek99")->SetAttribut (CREMOVED);
				 fHead.GetCfield ("abstand")->SetAttribut (CREMOVED);



			 }	

             fHead.GetCfield ("mdn_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fHead.GetCfield ("a_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fHead.GetCfield ("rez_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fHead.GetCfield ("variante_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));

//             fPos.GetCfield ("prod_abt_choise")->SetBitmap 
//                 (LoadBitmap (hInstance, "ARRDOWN"));

             fHead.GetCfield ("mdn_choise")->SetTabstop (FALSE); 

             fHead.GetCfield ("a_choise")->SetTabstop (FALSE); 
             fHead.GetCfield ("rez_choise")->SetTabstop (FALSE); 



             SetAttributFont (&fPos, &dlgposfont, CREADONLY);
             SetAttributFont (&fPos1, &dlgposfont, CREADONLY);

             if (KostStyle == DOWN)
             {
                 NotKostBkMode (WS_BORDER | TRANSPARENT);
             }
             else if (KostStyle == NOBORDER)
             {
                 NotKostBkMode (WS_BORDER);
             }
             else if (KostStyle == TRANSPARENT)
             {
                 NotKostBkMode (WS_BORDER);
                 kostrdofont.FontBkColor = SysBkColor;
                 kostrdohi.FontBkColor = SysBkColor;
             }

             for (i = 0; WhiteRdoFont[i] != NULL; i ++)
             {
                 WhiteRdoFont[i]->SetFont (&fPrdk_k);
             }

             for (i = 0; WhiteRdoFont[i] != NULL; i ++)
             {
                 WhiteRdoFont[i]->SetFont (&fPrdk_kZeit);
             }
             for (i = 0; WhiteRdoFont[i] != NULL; i ++)
             {
                 WhiteRdoFont[i]->SetFont (&fPrdk_kQuid);
             }
             for (i = 0; WhiteRdoFont[i] != NULL; i ++)
             {
                 WhiteRdoFont[i]->SetFont (&fKosten);
             }

             for (i = 0; VarbRdoFont[i] != NULL; i ++)
             {
                 VarbRdoFont[i]->SetFont (&fVarbFoot);
             }

             for (i = 0; KostRdoFont[i] != NULL; i ++)
             {
                 KostRdoFont[i]->SetFont (&fKost);
             }
             for (i = 0; QuidRdoFont[i] != NULL; i ++)
             {
                 QuidRdoFont[i]->SetFont (&fPosQuid);
             }

             for (i = 0; KostSuRdoFont[i] != NULL; i ++)
             {
                 KostSuRdoFont[i]->SetFont (&fKostSu);
             }

             for (i = 0; RdoFont[i] != NULL; i ++)
             {
                 RdoFont[i]->SetFont (&fPrdk_k);
             }

             for (i = 0; RdoFont1[i] != NULL; i ++)
             {
                 RdoFont1[i]->SetFont (&fPrdk_k1);
             }

                 
             for (i = 0; i < fHead.GetFieldanz (); i ++)
             {
//                       fHead.GetCfield ()[i]->SetFont (&dlgfont); 
//                       fHead.GetCfield ()[i]->SetPosFont (&dlgposfont); 
             }

             SetTmFont (&dlgfont);
             for (i = 0; Before[i] != NULL; i ++)
             {
                 Before[i]->SetBefore (&fPrdk_k);
             }

             for (i = 0; After[i] != NULL; i ++)
             {
                 After[i]->SetAfter (&fPrdk_k);
             }

             for (i = 0; After1[i] != NULL; i ++)
             {
                 After1[i]->SetAfter (&fPrdk_k1);
             }

             for (i = 0; AfterZeit[i] != NULL; i ++)
             {
                 AfterZeit[i]->SetAfter (&fPrdk_kZeit);
             }
             for (i = 0; AfterQuid[i] != NULL; i ++)
             {
                 AfterQuid[i]->SetAfter (&fPrdk_kQuid);
             }
             for (i = 0; AfterKost[i] != NULL; i ++)
             {
                 AfterKost[i]->SetAfter (&fKosten);
             }
             for (i = 0; Before[i] != NULL; i ++)
             {
                 Before[i]->SetBefore (&fPrdk_kZeit);
             }
             for (i = 0; Before[i] != NULL; i ++)
             {
                 Before[i]->SetBefore (&fPrdk_kQuid);
             }
             for (i = 0; Before[i] != NULL; i ++)
             {
                 Before[i]->SetBefore (&fKosten);
             }
//             fKost.GetCfield ("kost_line")->SetBorder (GRAYCOL, WHITECOL, RAISEDHLINE); 
//             fKost.GetCfield ("kost_line")->SetBorder (GRAYCOL, WHITECOL, HLINE); 
             fKost.GetCfield ("kost_line")->SetBorder (BLACKCOL, BLACKCOL, HLINE); 
             fKost.GetCfield ("pos_line")->SetBorder (BLACKCOL, BLACKCOL, HLINE); 
             fKost.GetCfield ("kost1_line")->SetBorder (BLACKCOL, BLACKCOL, HLINE); 
             if (BorderType == HIGHCOLBORDER)
             {

                for (i = 0; Font[i] != NULL; i ++)
                {
                 Font[i]->SetFont (&fPrdk_k);
                }

                for (i = 0; Font1[i] != NULL; i ++)
                {
                 Font1[i]->SetFont (&fPrdk_k1);
                }

                fPos.GetCfield ("pos_frame")->SetBorder 
//                 (new FILLEDBORDER (BLACKCOL, WHITECOL, HIGH, 0, LTGRAYCOL)); 
                 (new FILLEDBORDER (BLACKCOL, WHITECOL, HIGH, 0, SysBkColor)); 
                fPos1.GetCfield ("pos_frame")->SetBorder 
//                 (new FILLEDBORDER (BLACKCOL, WHITECOL, HIGH, 0, LTGRAYCOL)); 
                 (new FILLEDBORDER (BLACKCOL, WHITECOL, HIGH, 0, SysBkColor)); 
             }
             else if (BorderType == LOWCOLBORDER)
             {

                SetAttributFont (&fPos,     &ltgrayfont, CREADONLY);
                SetAttributFont (&fPos1,    &ltgrayfont, CREADONLY);
                SetAttributFont (&fPosZeit, &ltgrayfont, CREADONLY);
                SetAttributFont (&fPosQuid, &ltgrayfont, CREADONLY);
                for (i = 0; Font[i] != NULL; i ++)
                {
                 Font[i]->SetFont (&fPrdk_k);
                }
                fPos.GetCfield ("pos_frame")->SetBorder 
//                 (new FILLEDBORDER (BLACKCOL, WHITECOL, LOW, 0, LTGRAYCOL)); 
                 (new FILLEDBORDER (BLACKCOL, WHITECOL, HIGH, 0, SysBkColor)); 
                fPos1.GetCfield ("pos_frame")->SetBorder 
//                 (new FILLEDBORDER (BLACKCOL, WHITECOL, LOW, 0, LTGRAYCOL)); 
                 (new FILLEDBORDER (BLACKCOL, WHITECOL, HIGH, 0, SysBkColor)); 
             }
             else if (BorderType == HIGHBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, HIGH); 
                fPos1.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, HIGH); 
             }
             else if (BorderType == LOWBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, LOW); 
                fPos1.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, LOW); 
             }
             else if (BorderType == RAISEDBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, RAISED); 
                fPos1.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, RAISED); 
             }
             else if (BorderType == LINEBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, LINE); 
                fPos1.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, LINE); 
             }


             work.FillComboLong (ProdAbtCombo, "prod_abt", 200);
             strcpy (prdk_kf.prod_abt, ProdAbtCombo [0]);
             fPos.GetCfield ("prod_abt")->SetCombobox (ProdAbtCombo); 


             Enable (&fPrdk_k, EnableHead, TRUE); 
             Enable (&fPrdk_k, EnablePos,  FALSE);
             Enable (&fPrdk_k1, EnablePos1,  FALSE);
             EnablePhasen (FALSE);

             if (sys_ben.mdn > 0)
             {
                 sprintf (prdk_kf.mdn, "%hd", sys_ben.mdn);
				 ReadMdn();
                 fHead.GetCfield ("mdn")->Enable (FALSE);

             }
//             _fList[0]->Setchsize (0, 1);
			 if (sys_ben.zahl == 99)
			 {
	             strcpy (prdk_kf.pageinfo,  "Batchlauf");
				 if (prdk_k.a > 0.0)
				 {
//					 OnKey12();
				 }
			 }
			 else
			 {
		         strcpy (prdk_kf.pageinfo,  "Kopfdaten");
			 }	
//             strcpy (prdk_kf.ek, "J");
//             strcpy (prdk_kf.plan_ek, "N");
             SetDialog (&fPrdk_k0);

			 if (prdk_k.a > 0.0)
			 {
				 sprintf (prdk_kf.a,        "%13.0f",      prdk_k.a);
//		         fHead.SetText ();
			 }
			 if (strlen(prdk_k.rez) > 0)
			 {
				 sprintf (prdk_kf.rez,        "%s",      prdk_k.rez);
//		         fHead.SetText ();

				 sprintf (prdk_kf.variante,        "%hd",      prdk_k.variante);
//		         fHead.SetText ();

	            if (ReadPrdk_k () == 0)
		        {
		            int ret = work.GetRez (prdk_kf.rez_bz, a_eig.rez, ratod (prdk_kf.a),atoi (prdk_kf.mdn)); //150204
					strcpy (prdk_kf.rez, a_eig.rez);  //#270403 LuD
					strcpy (prdk_k.rez, a_eig.rez);  //#270403 LuD
//					fHead.SetText (); //150204
					chg_gew_old = 0.0; //240305
					syskey = KEYCR;
				    Toolbar2 = NULL;
					EnterPos ();
					if (KostLst == NULL) //040304
					{
						KostLst = new KOSTLST ();
						KostLst->SetWork (&work);
						KostLst->SetDlg (this);
  						KostLst->FillDB (); 
						delete KostLst;
						KostLst = NULL;
					}
				    strcpy (PrintCommand, "rezepturdruck");
						strcat(PrintCommand, " -mdn ");
						strcat(PrintCommand, prdk_kf.mdn);
						strcat(PrintCommand, " -avon ");
						strcat(PrintCommand, prdk_kf.a);
						strcat(PrintCommand, " -abis ");
						strcat(PrintCommand, prdk_kf.a);
						strcat(PrintCommand, " -variante ");
						strcat(PrintCommand, prdk_kf.variante);
						if (work.GetGBVorhanden() == TRUE)
						{
							if (abfragejn (hWnd, "Grundbr�te einzeln mit ausdrucken ?", "N"))
							{
								strcat(PrintCommand, " -mitGB 1 "); //210807 
							}
						}

						if (sys_ben.berecht > 1 && (sys_ben.berecht < 4))
						{
							strcat(PrintCommand, " -ohnePreis ");
							strcat(PrintCommand, "1");
						}

						ProcExec (PrintCommand, SW_SHOWNORMAL, -1, 0, -1, 0);  

				}
			 }

}


BOOL SpezDlg::OnKeyReturn (void)
{
        HWND hWnd;
        char wert [5];
        
        hWnd = GetFocus ();
		if (fWork == &fPrdk_k0Zeit)
		{
			return FALSE;
		}
		if (fWork == &fKosten0)
		{
			return FALSE;
		}

        if (hWnd == fWork->GetCfield ("a")->GethWnd ())
        {
            syskey = KEYCR;
            fHead.GetText ();
            if (ratod (prdk_kf.a) == 0.0)
            {
                return FALSE;
            }
            int ret = work.GetABez (prdk_kf.a_bz1, ratod (prdk_kf.a),sys_ben.pers_nam);
            switch (ret)
            {
              case 999 :
                disp_mess ("Sie haben keine Berechtigung f�r diesen Artikel ", 2);
                break;
              case 100 :
                disp_mess ("Artikel ist nicht angelegt", 2);
                break;
              case 200 :
  				if (abfragejn (hWnd, "Artikel ist kein Rezepturartikel Trotzdem anlegen ?", "N"))
				{
                    if (work.upd_a_bas(_a_bas.a) != 0)
					{
		                 disp_mess ("Artikel ist gesperrt (Tabelle a_bas)", 2);
						 break;
					}
					ret = 0;
				}
				else
				{
					break;
				}

//                 disp_mess ("Artikel ist kein Rezepturartikel", 2);
//                 break;
              
            }

            if (ret != 0)
            {
                 return TRUE;
            }
            strcpy (prdk_kf.rez, a_eig.rez);
            sprintf (wert, "%d", _a_bas.me_einh);
            work.GetPtab (EinhPos0, "me_einh", wert);
            work.GetPtab (EinhPos, "me_einh", wert);
            fHead.SetText ();
            return FALSE;
        }
        if (hWnd == fWork->GetCfield ("rez")->GethWnd ()) //#150204
        {
            syskey = KEYCR;
            fHead.GetText ();
            if (ratod (prdk_kf.rez) == 0.0)
            {
                return FALSE;
            }
			if (a_eig.a > 0.0)
			{
				if (strcmp(clipped(prdk_kf.rez),clipped(a_eig.rez)) != 0)
			  {
				if (abfragejn (ActiveDlg->GethWnd (), "Rezepturzuordnung �ndern ?", "N") == 0) //261005
				{
					strcpy (prdk_kf.rez,a_eig.rez);
		            fHead.SetText ();
					return FALSE;
				}
			  }
			}
            int ret = work.GetRez (prdk_kf.rez_bz, prdk_kf.rez, ratod (prdk_kf.a),atoi (prdk_kf.mdn));
            if (ret < 0) 
            {
				print_mess (2, "Rezeptur ist zur Bearbeitung gesperrt : %d", ret);
                return TRUE;
            }
            fHead.SetText ();

			if (work.GetRezOhneVarianten() == 1) //130210
			{
				prdk_k.variante = 0;
				strcpy (prdk_kf.variante,"0");
				syskey = KEYCR;
				fHead.GetText ();
				if (ReadPrdk_k () == 100)
				{
					flg_kopieren = 0;
					Ursprung_a = 0.0;
					return TRUE;
				}
				if (flg_kopieren == 1)
				{
					Text cText;
					cText.Format ("Artikel  %.0lf hier kopieren ?", Ursprung_a);
					if (abfragejn (hWnd, cText.GetBuffer (), "J"))
					{
						work.CopyArtikel (Ursprung_mdn, Ursprung_rez, Ursprung_a,  Ursprung_variante, 
								   		atoi(prdk_kf.mdn),prdk_kf.rez, ratod(prdk_kf.a), atoi(prdk_kf.variante) );
						if (ReadPrdk_k () == 100)
						{
							flg_kopieren = 0;
							Ursprung_a = 0.0;
							return TRUE;
						}
					}
				}
				flg_kopieren = 0;
				Ursprung_a = 0.0;
				int ret = work.GetRez (prdk_kf.rez_bz, a_eig.rez, ratod (prdk_kf.a),atoi (prdk_kf.mdn)); //150204
				if (ret < 0) 
				{
					print_mess (2, "Rezeptur ist zur Bearbeitung gesperrt : %d", ret);
					return TRUE;
				}
				strcpy (prdk_kf.rez, a_eig.rez);  //#270403 LuD
				strcpy (prdk_k.rez, a_eig.rez);  //#270403 LuD
				fHead.SetText (); //150204
				chg_gew_old = 0.0; //240305
				EnterPos ();
				if (KostLst == NULL) //040304
				{
					KostLst = new KOSTLST ();
					KostLst->SetWork (&work);
					KostLst->SetDlg (this);
  					KostLst->FillDB (); 
					delete KostLst;
					KostLst = NULL;
				}
			} // GetRezOhneVarianten 130210
	





            return FALSE;
        }
        else if (hWnd == fWork->GetCfield ("variante")->GethWnd ())
        {
            syskey = KEYCR;
            fHead.GetText ();
            if (ReadPrdk_k () == 100)
            {
				flg_kopieren = 0;
				Ursprung_a = 0.0;
                return TRUE;
            }
			if (flg_kopieren == 1)
			{
				Text cText;
			    cText.Format ("Artikel  %.0lf hier kopieren ?", Ursprung_a);
		       if (abfragejn (hWnd, cText.GetBuffer (), "J"))
				{
					work.CopyArtikel (Ursprung_mdn, Ursprung_rez, Ursprung_a,  Ursprung_variante, 
								   	atoi(prdk_kf.mdn),prdk_kf.rez, ratod(prdk_kf.a), atoi(prdk_kf.variante) );
		            if (ReadPrdk_k () == 100)
				    {
						flg_kopieren = 0;
						Ursprung_a = 0.0;
						return TRUE;
					}
				}
			}
			flg_kopieren = 0;
			Ursprung_a = 0.0;
            int ret = work.GetRez (prdk_kf.rez_bz, a_eig.rez, ratod (prdk_kf.a),atoi (prdk_kf.mdn)); //150204
            if (ret < 0) 
            {
				print_mess (2, "Rezeptur ist zur Bearbeitung gesperrt : %d", ret);
                return TRUE;
            }
            strcpy (prdk_kf.rez, a_eig.rez);  //#270403 LuD
            strcpy (prdk_k.rez, a_eig.rez);  //#270403 LuD
            fHead.SetText (); //150204
            chg_gew_old = 0.0; //240305
            EnterPos ();
	         if (KostLst == NULL) //040304
		     {
             KostLst = new KOSTLST ();
             KostLst->SetWork (&work);
             KostLst->SetDlg (this);
  		     KostLst->FillDB (); 
	         delete KostLst;
		     KostLst = NULL;

//   		     KostLst->CalcSum ();
			 }

            return TRUE;
        }
        else if (hWnd == fWork->GetCfield ("a_bis")->GethWnd ())
        {
//            syskey = KEYCR;
//            AListe ();
//            return TRUE;
            return FALSE;
        }
        else if (hWnd == fWork->GetCfield ("akv")->GethWnd ())
        {
            fWork->GetText ();
			setWindowText (1);
            return FALSE;
        }
        else
        {
            for (int i = 1; _fProdPhasen[i] != NULL; i += 1)
            {
                if (hWnd == _fProdPhasen[i]->GethWnd ())
                {
                    _fProdPhasen[i]->GetText ();
                    work.ReadMasch (MaschBz[(i - 1) / 3], atol (Masch[(i - 1) / 3])); 
                    _fMaschBz[(i - 1) / 3]->SetText ();
                }
            }
        }
        return FALSE;
}


void SpezDlg::GetCombos (void)
{

       int pos = fPos.GetCfield ("prod_abt")->GetComboPos ();
        prdk_k.prod_abt = atoi (work.GetPtWert ("prod_abt", pos + 1)); 


}


void SpezDlg::SetCombos (void)
{
         sprintf (ptwert, "%hd", prdk_k.prod_abt);
         work.GetPtab (prdk_kf.prod_abt, "prod_abt", ptwert);
         fPos.GetCfield ("prod_abt")->SetPosCombo (ptabn.ptlfnr - 1);
}


int SpezDlg::WriteRow (void)
{

        GetPosText (); 
        if (atol (prdk_kf.a) == 0l) return 0;
        FromForm (dbfields); 
        GetCombos ();
//        pos = fPos.GetCfield ("staat")->GetComboPos ();
        strcpy (prdk_k.prodphase, "");
        for (int i = 0; ProdPhasen[i] != NULL; i ++)
        {
                strcat (prdk_k.prodphase, ProdPhasen[i]);
                if (i < MAXPHASEN && Masch[i] != NULL)
                {
                       *PrdkMasch[i] = atol (Masch[i]);
                       *PrdkMaschZeit[i] = atoi (MaschZeit[i]);
                }
        }
        if (strcmp (prdk_kf.akv, "J") == 0)
        {
                 prdk_k.akv = 1;
        }
        else
        {
                 prdk_k.akv = 0;
        }
        if (strcmp (prdk_kf.rework, "J") == 0)
        {
                 prdk_k.rework = 1;
        }
        else
        {
                 prdk_k.rework = 0;
        }

        prdk_k.akv = work.TestAkv (atoi (prdk_kf.mdn), ratod (prdk_kf.a), 
                                   atoi (prdk_kf.variante), prdk_k.akv);

        FromForm (dbheadfields);
		systime (kalkzeit);
        if (work.WritePrdk_k (atoi (prdk_kf.mdn), ratod (prdk_kf.a), prdk_kf.rez, kalkzeit,prdk_kf.rez_bz) == -1)
        {
                ActiveDlg->SetCurrentFocus ();
                return -1;
        }
        return 0;
}

int SpezDlg::DeleteRow (void)
{
//         HWND lBox;
          
 	if (sys_ben.berecht >= 3) return 0;
       if (abfragejn (ActiveDlg->GethWnd (), "Rezeptur l�schen ?", "J") == 0)
        {
            return 0;
        }

        work.DeletePrdk_k (atoi (prdk_kf.mdn), ratod (prdk_kf.a), prdk_kf.rez);

        ToolBar_SetState (ActiveDlg->GethwndTB (),   IDM_DELALL,   TBSTATE_INDETERMINATE);
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELETE,   MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELALL,   MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_INSERT,    MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      VK_F10,       MF_GRAYED);
        if (ActiveSpezDlg->GetToolbar2 () != NULL)
        {
                 ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("del")->GetFeld ())->bmp = 
					 ImageDelete.Inactive;
                 ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("insert")->GetFeld ())->bmp = 
                     ImageInsert.Inactive;
                 ActiveSpezDlg->GetToolbar2 ()->display ();
        }
        Enable (&fPrdk_k, EnableHead, TRUE); 
        Enable (&fPrdk_k, EnablePos,  FALSE);
        ActiveSpezDlg->EnablePhasen (FALSE);
        work.CommitWork ();
        work.InitRec ();
        for (int i = 0; ProdPhasen [i] != NULL; i ++)
        {
                strcpy (ProdPhasen[i], "N");
                if (i < MAXPHASEN && Masch[i] != NULL)
                {
                      strcpy (Masch[i], "0");
                      strcpy (MaschZeit[i], "0");
                      strcpy (MaschBz[i], "");
                }
        }
        FromForm (dbheadfields);
        ToForm (dbfields);
        SetCombos ();
        SetPosText (); 
//        InvalidateRect (lBox, NULL, TRUE);
//        UpdateWindow (lBox);
        fPrdk_k.SetCurrentName ("a");
        return 0;
}
        

/*
BOOL SpezDlg::OnKeyDown (void)
{
        HWND hWnd;
        CFIELD *Cfield;

        hWnd == GetFocus ();

        Cfield = GetCurrentCfield ();
        if (Cfield != NULL)
        {
            if (Cfield->GetAttribut () & CCOMBOBOX != 0)
            {
                return FALSE;
            }
        }

        if (fHead.GetCfield ("a")->IsDisabled () == FALSE)
        {
            return FALSE;
        }
        if (_fList[0]->GethWnd () == NULL) 
        {
            return FALSE;
        }

        syskey = KEYDOWN;
        return TRUE;
}


BOOL SpezDlg::OnKeyUp (void)
{
        HWND hWnd;
        int idx;
        CFIELD *Cfield;

        hWnd == GetFocus ();


        Cfield = GetCurrentCfield ();
        if (Cfield != NULL)
        {
            if (Cfield->GetAttribut () & CCOMBOBOX != 0)
            {
                return FALSE;
            }
        }
 
        GetPosText (); 

        if (fHead.GetCfield ("a")->IsDisabled () == FALSE)
        {
            return FALSE;
        }


        return TRUE;
}
*/

BOOL SpezDlg::OnKey2 ()
{
        return TRUE;
}



BOOL SpezDlg::OnKeyHome ()
{
        return TRUE;
}

BOOL SpezDlg::OnKeyEnd ()
{
        return TRUE;

}
BOOL SpezDlg::OnKey1 ()
{
        syskey = KEY1;
        Help ();
        return TRUE;
}

BOOL SpezDlg::OnKey3 ()
{
        syskey = KEY3;
        return TRUE;
}

BOOL SpezDlg::OnKey4 ()
{
        syskey = KEY4;
        CallInfo ();
        return TRUE;
}

BOOL SpezDlg::OnKey5 ()
{

        syskey = KEY5;
//		chg_gew_old = 0.0; //240305
        if (fHead.GetCfield ("a")->IsDisabled () == FALSE)
        {
//             if (abfragejn (hWnd, "Bearbeitung beednen ?", "N"))
			 {
                  DestroyWindow ();
                  return TRUE;
			 }
		}
        
		if (sys_ben.berecht < 3)
        {
    		if (abfragejn (hWnd, "Rezeptur speichern ?", "N"))
			{
				return OnKey12 ();
			}
		}
		

		setWindowText (0);
        if (iPage != 0)
        {
/*
                  switch (iPage)
                  {
                      case 1 :
                      case 2 :
                      case 3 :
                          fPos1.GetText ();
                          fPos1.destroy ();
                          fVarbFoot.destroy ();
                          break;
                  }
*/
                  TabCtrl_SetCurFocus (GetParent (hWnd), 0);
        }
        Enable (&fPrdk_k, EnableHead, TRUE); 
        Enable (&fPrdk_k, EnablePos,  FALSE);
        EnablePhasen (FALSE);
        work.RollbackWork ();
        work.InitRec ();
        for (int i = 0; ProdPhasen [i] != NULL; i ++)
        {
                strcpy (ProdPhasen[i], "N");
                if (i < MAXPHASEN && Masch[i] != NULL)
                {
                      strcpy (Masch[i], "0");
                      strcpy (MaschZeit[i], "0");
                      strcpy (MaschBz[i], "");
                }
        }
        ToForm (dbfields);
        SetCombos ();
        SetPosText (); 
        fHead.SetCurrentName ("a");
        ToolBar_SetState (ActiveDlg->GethwndTB (),   IDM_DELALL,   TBSTATE_INDETERMINATE);
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELETE,   MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELALL,   MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_INSERT,    MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      VK_F10,       MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      VK_F11,       MF_GRAYED);
        if (ActiveSpezDlg->GetToolbar2 () != NULL)
        {
                 ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("del")->GetFeld ())->bmp = 
					 ImageDelete.Inactive;
                 ActiveSpezDlg->GetToolbar2 ()->GetCfield ("del")->Enable (FALSE);

                 ((ColButton *)Toolbar2->GetCfield ("insert")->GetFeld ())->bmp = 
                     ImageInsert.Inactive;
                 ActiveSpezDlg->GetToolbar2 ()->GetCfield ("insert")->Enable (FALSE);

                 if  (ActiveSpezDlg->GetToolbar2 ()->GetCfield ("Texte") != NULL)
                 {
                      ActiveSpezDlg->GetToolbar2 ()->GetCfield ("Texte")->Enable (FALSE);
                 }

                 ActiveSpezDlg->GetToolbar2 ()->display ();
        }
		RestartProcess(FALSE);
        return TRUE;
}

BOOL SpezDlg::OnKey12 ()
{

	BOOL flgRestart = TRUE;
    if (syskey == -1) flgRestart = FALSE;
	if (sys_ben.berecht >= 3.0 && sys_ben.zahl != 99)
    {
    		return TRUE;
	}
//    work.SetChgFaktor(1);
//240305    chg_gew_old = ratod(prdk_kf.chg_gew);

        syskey = KEY12;

	   if (sys_ben.zahl == 99)
	   {
           if (A_Lst == NULL)  
           {

			if  (work.updPrdk_k() == 0)
			{
				AListe ();
            }
			else
            {  
				strcpy (prdk_kf.pageinfo, "fehler update kalk_stat ");
				fHead.SetText ();
			} 

		   }
		   else
           {
			   Batchlauf();
		   }

           work.CommitWork ();
		   return TRUE;
	   }
        strcpy(FlgBasis, "J");
        strcpy(FlgKalk, "N");
  	    CalcfromChg ();
		CalcfromMe ();
        fWork->GetText ();
        FromForm (dbfields); 
		prdk_k.kutter_gew = prdk_k.chg_gew;
		prdk_k.chargierung = 1;
		prdk_k.manr = 0;
	    if (strcmp (prdk_kf.plan_ek, "J") == 0)
		{
			 prdk_k.manr = 1;
		}

        GetCombos ();
		setWindowText (0);
        if (fHead.GetCfield ("a")->IsDisabled ())
        {
               if (iPage != 0)
               {
                  TabCtrl_SetCurFocus (GetParent (hWnd), 0);
               }
               Validate ();
   			   CalcNewKost ();
	  		   work.FuelleQuid (prdk_k.rez,prdk_k.variante,1,prdk_k.chg_gew,1.0,0.0);
	  		   work.RechneQuid ();
               if (WriteRow () == -1)
               {
                 return TRUE;
               }
               Enable (&fPrdk_k, EnableHead, TRUE); 
               Enable (&fPrdk_k, EnablePos,  FALSE);
               EnablePhasen (FALSE);
               work.CommitWork ();
               work.InitRec ();
               for (int i = 0; ProdPhasen [i] != NULL; i ++)
               {
                        strcpy (ProdPhasen[i], "N");
                        if (i < MAXPHASEN && Masch[i] != NULL)
                        {
                               strcpy (Masch[i], "0");
                               strcpy (MaschZeit[i], "0");
                               strcpy (MaschBz[i], "");
                        }
               }
               ToForm (dbfields);
               SetCombos ();

               SetPosText (); 
               fHead.SetCurrentName ("a");
               ToolBar_SetState (ActiveDlg->GethwndTB (),   IDM_DELALL, TBSTATE_INDETERMINATE);
               EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELETE,   MF_GRAYED);
               EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELALL,   MF_GRAYED);
               EnableMenuItem (ActiveDlg->GethMenu (),      IDM_INSERT,    MF_GRAYED);
               EnableMenuItem (ActiveDlg->GethMenu (),      VK_F10,       MF_GRAYED);
               EnableMenuItem (ActiveDlg->GethMenu (),      VK_F11,       MF_GRAYED);
               if (ActiveSpezDlg->GetToolbar2 () != NULL)
               {
                 ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("del")->GetFeld ())->bmp = 
					 ImageDelete.Inactive;
                 ActiveSpezDlg->GetToolbar2 ()->GetCfield ("del")->Enable (FALSE);

                 ((ColButton *)Toolbar2->GetCfield ("insert")->GetFeld ())->bmp = 
                     ImageInsert.Inactive;
                 ActiveSpezDlg->GetToolbar2 ()->GetCfield ("insert")->Enable (FALSE);

                 if  (ActiveSpezDlg->GetToolbar2 ()->GetCfield ("Texte") != NULL)
                 {
                      ActiveSpezDlg->GetToolbar2 ()->GetCfield ("Texte")->Enable (FALSE);
                 }
                 ActiveSpezDlg->GetToolbar2 ()->display ();
               }
        }
		if (flgRestart == TRUE)	RestartProcess(FALSE);
        return TRUE;
}

void SpezDlg::RestartProcess (BOOL flgPrint)
{
       if (flgRestartProcess == 0) return;
	   char command [512]; 
/**
	   if (TransactMessage)
	   {
	          disp_mess ("Die Anzahl der Transaktionen pro Process ist �berschritten.\n" 
		                 "Ein Restart des Programms wird durchgef�hrt", 2); 
	   }
*/

//	   sprintf (command, "%s nomenu=true mdn=%hd", GetCommandLine (), atoi (mdn));
	   if (flgPrint == TRUE)
	   {
		    syskey = -1;
		    OnKey12();
		    if (sys_ben.zahl == 99)
			{
				return;
			}
			
			sprintf (command, "23201 %hd 0 0 %13.0lf %s %hd", atoi(prdk_kf.mdn),ratod(prdk_kf.a),prdk_kf.rez,atoi(prdk_kf.variante) );
	   }
	   else
	   {
			sprintf (command, "23201 %hd 0 0 %13.0lf", atoi(prdk_kf.mdn),ratod(prdk_kf.a) );
	   }
	   ProcExec (command, SW_SHOW, -1, 0, -1, 0);
	   Sleep (500);
	   ExitProcess (0);
}


BOOL SpezDlg::OnKey10 (void)
{
     if (VarbLst != NULL)
     {
	       if (GetFlgAnteil ())
		   {
			   SetFlgAnteil(FALSE);
		   }
		   else
		   {
			   SetFlgAnteil(TRUE);
		   }

           VarbLst->StopList ();
           VarbLst->DestroyWindows ();
           delete VarbLst;
           VarbLst = NULL;
           VarbLst = new VARBLST ();
           EnterVarbLst ();
     }
      return TRUE;
/*
  	  int cx, cy;
	  char textnr [10];
      long text_nr;


      text_nr = work.GenTxtNr ();
      if (text_nr == 0l)
      {
          return FALSE;
      }
      cx = 80;
      cy = 20;
      Mtxt = new MTXT (cx, cy, "Bemerkungen", "", NULL);
	  Mtxt->SetTextNr (LongToChar (textnr, "%ld", text_nr));
      Mtxt->OpenWindow (DLG::hInstance, hWnd);
	  EnableWindows (hWnd, FALSE);
      work.ReadTxt (text_nr, Mtxt); 

	  if (Mtxt->ProcessMessages ())
	  {
		  work.WriteTxt (atol (Mtxt->GetTextNr ()), Mtxt);
	  }
      else
      {

      }

	  SetActiveWindow (hWnd);
	  EnableWindows (hWnd, TRUE);
      Mtxt->DestroyWindow ();
	  delete Mtxt;
      ActiveDlg->SetCurrentFocus ();
*/
}

BOOL SpezDlg::OnKey11 (void)
{
      char command [512];
      char doc [512];
      FILE *dfile;
      CFIELD *Cfield;

      Cfield = GetCurrentCfield ();
      fHead.GetText ();
      if (ratod (prdk_kf.a) == 0.0)
      {
          disp_mess ("Es wurde keine Artikelnummer eingegeben",2);
          Cfield->SetFocus ();
          return TRUE;
      }
      if (strlen (WordPath) == 0)
      {
          strcpy (WordPath, "winword");
      }
      if (strchr (DocPath, '\\') == NULL)
      {
          strcpy (doc, DocPath);
          if (getenv ("BWS"))
          {
              sprintf (DocPath, "%s\\%s", getenv ("BWS"), doc);
          }
          else
          {
              sprintf (DocPath, "C:\\doc");
          }
      }
      _mkdir (DocPath);
      sprintf (command, "%s\\%.0lf.doc", DocPath, ratod (prdk_kf.a));
      dfile = fopen (command,"r");
	  if (dfile == NULL) // ist noch nicht angelegt
	  {
	      dfile = fopen (command,"w");
          if (dfile == (FILE *) 0)
          {
                     return 0;
          }
//          fprintf (dfile, "Artikel %.0lf", ratod(prdk_kf.a));
	  }
      fclose (dfile);

      sprintf (command, "%s %s\\%.0lf.doc", WordPath, DocPath, ratod (prdk_kf.a));

      if (ProcExec (command, SW_SHOWNORMAL, -1, 0, -1, 0) == 0)
      {
          print_mess (2, "%s kann nicht gestartet werden", WordPath);
      }
      if (VarbLst == NULL &&
          ZutLst ==  NULL &&
          HuelLst == NULL &&
          VpkLst  == NULL &&
          A_Lst  == NULL &&
          ZeitLst == NULL &&
          QuidLst == NULL &&
          KostLst == NULL)
      {
          Cfield->SetFocus ();
      }
      return TRUE;
}

BOOL SpezDlg::OnKeyDelete ()
{
        return FALSE;
}


BOOL SpezDlg::OnKeyInsert ()
{
        return FALSE;
}


BOOL SpezDlg::OnKeyPrior ()
{
        if (GetKeyState (VK_CONTROL) >= 0)
        {
                if (VarbLst == NULL &&
                    ZutLst == NULL &&
                    HuelLst == NULL &&
                    VpkLst == NULL &&
                    A_Lst == NULL &&
                    ZeitLst == NULL &&
                    QuidLst == NULL &&
                    KostLst == NULL)
                {
                    return PriorPage ();
                }
        }

        if (vTrack != NULL)
        {
             SendMessage (hWnd, WM_VSCROLL, SB_PAGEUP, 0l);
             return TRUE;
        }
        return FALSE;
}

BOOL SpezDlg::PriorPage ()
{
        return DLG::OnKeyPrior (); 
}

BOOL SpezDlg::OnKeyNext ()
{
        if (GetKeyState (VK_CONTROL) >= 0)
        {
                if (VarbLst == NULL &&
                    ZutLst == NULL &&
                    HuelLst == NULL &&
                    VpkLst == NULL &&
                    A_Lst == NULL &&
                    ZeitLst == NULL &&
                    QuidLst == NULL &&
                    KostLst == NULL)
                {
                    return NextPage ();
                }
        }

        if (vTrack != NULL)
        {
             SendMessage (hWnd, WM_VSCROLL, SB_PAGEDOWN, 0l);
             return TRUE;
        }

        return FALSE;
}


BOOL SpezDlg::NextPage ()
{
        return DLG::OnKeyNext (); 
}

BOOL SpezDlg::OnKey6 ()
{
/*
        
        if (GetKeyState (VK_CONTROL) < 0)
        {
            return OnRowDelete ();
        }
*/
        if (VarbLst != NULL)
        {
            return VarbLst->OnKey6 ();
        }
        else if (A_Lst != NULL)
        {
            return A_Lst->OnKey6 ();
        }
        else if (ZutLst != NULL)
        {
            return ZutLst->OnKey6 ();
        }
        else if (HuelLst != NULL)
        {
            return HuelLst->OnKey6 ();
        }
        else if (VpkLst != NULL)
        {
            return VpkLst->OnKey6 ();
        }
        else if (KostLst != NULL)
        {
            return KostLst->OnKey6 ();
        }
        else if (ZeitLst != NULL)
        {
            return ZeitLst->OnKey6 ();
        }
        else if (QuidLst != NULL)
        {
            return QuidLst->OnKey6 ();
        }

		if (sys_ben.zahl == 99)  	  
        {
//   		  Wmess.Destroy ();
			if  (work.updPrdk_k() == 0)
			{
				AListe ();
            }
			else
            {  
				strcpy (prdk_kf.pageinfo, "fehler update kalk_stat ");
				fHead.SetText ();
			} 
		}

        return FALSE;
} 


BOOL SpezDlg::OnKey7 ()
{
/*
        
        if (GetKeyState (VK_CONTROL) < 0)
        {
            return OnRowDelete ();
        }
*/
        if (VarbLst != NULL)
        {
            return VarbLst->OnKey7 ();
        }
        else if (ZutLst != NULL)
        {
            return ZutLst->OnKey7 ();
        }
        else if (HuelLst != NULL)
        {
            return HuelLst->OnKey7 ();
        }
        else if (VpkLst != NULL)
        {
            return VpkLst->OnKey7 ();
        }
        else if (KostLst != NULL)
        {
            return KostLst->OnKey7 ();
        }
        else if (ZeitLst != NULL)
        {
            return ZeitLst->OnKey7 ();
        }
        else if (QuidLst != NULL)
        {
            return QuidLst->OnKey7 ();
        }
        return FALSE;
}


BOOL SpezDlg::ShowMdn (void)
{
             char mdn_nr [6];
             SEARCHMDN SearchMdn;
 
             SearchMdn.SetParams (DLG::hInstance, ActiveDlg->GethWnd ());
             SearchMdn.Setawin (ActiveDlg->GethMainWindow ());
             SearchMdn.Search ();
             if (SearchMdn.GetKey (mdn_nr) == TRUE)
             {
                 sprintf (prdk_kf.mdn, "%4d", atoi (mdn_nr));
                 work.GetMdnName (prdk_kf.mdn_krz,   atoi (prdk_kf.mdn));
                 fHead.SetText ();
                 fHead.SetCurrentName ("mdn");
             }
             return TRUE;
}


BOOL SpezDlg::ShowA (void)
{
        struct SA *sa;

        SearchA.SetParams (DLG::hInstance, ActiveDlg->GethWnd ());
        SearchA.Setawin (ActiveDlg->GethMainWindow ());
        SearchA.SetQuery ("and stk_lst_kz = \"R\"");
        SearchA.SearchA ();
        SearchA.SetQuery (NULL);
        ActiveDlg->Validate ();
        if (syskey == KEY5)
        {
            fHead.SetCurrentName ("a");
            return 0;
        }
        sa = SearchA.GetSa ();
        if (sa == NULL)
        {
            return 0;
        }
        strcpy (prdk_kf.a, sa->a);
        strcpy (prdk_kf.a_bz1, sa->a_bz1);
        fHead.SetText ();
//        SetCurrentName ("a");
        PostMessage (hWnd, WM_KEYDOWN, VK_RETURN, 0l);
        return 0;
}


BOOL SpezDlg::ShowRez (void)
{
        struct SREZ *srez;
        SEARCHREZ SearchRez;

        SearchRez.SetParams (DLG::hInstance, ActiveDlg->GethWnd ());
        SearchRez.Setawin (ActiveDlg->GethMainWindow ());
        SearchRez.Search (atoi (prdk_kf.mdn));
        ActiveDlg->Validate ();
        if (syskey == KEY5)
        {
            fHead.SetCurrentName ("rez");
            return 0;
        }
        srez = SearchRez.GetSRez ();
        if (srez == NULL)
        {
            return 0;
        }
		if (atoi (prdk_kf.a) == 0)
        {
			strcpy (prdk_kf.a, srez->a);  //150204
        }
        strcpy (prdk_kf.rez, srez->rez);
        strcpy (prdk_k.rez, srez->rez); //150204
        strcpy (prdk_kf.rez_bz, srez->rez_bz);
        sprintf (prdk_kf.variante, "%hd", atoi (srez->variante));
        fHead.SetText ();
        SetCurrentName ("rez");
        return 0;
}


BOOL SpezDlg::ShowVariante (void)
{
        struct SVARIANTE *svariante;
        SEARCHVARIANTE SearchVariante;
        Text Query;

        fHead.GetText ();
        if (ratod (prdk_kf.a) == 0.0 ||
            strcmp (clipped (prdk_kf.rez), " ") <= 0)
        {
                 disp_mess ("Keine Artikel-Nr oder Rezeptur-Nr", 2);
                 return TRUE;
        }

        Query.Format ("and a = %.0lf and rez = \"%s\"",
                      ratod (prdk_kf.a), clipped (prdk_kf.rez));
        SearchVariante.SetQuery (Query.GetBuffer ());
        SearchVariante.SetParams (DLG::hInstance, ActiveDlg->GethWnd ());
        SearchVariante.Setawin (ActiveDlg->GethMainWindow ());
        SearchVariante.Search (atoi (prdk_kf.mdn));
        ActiveDlg->Validate ();
        if (syskey == KEY5)
        {
            return 0;
        }
        svariante = SearchVariante.GetSVariante ();
        if (svariante == NULL)
        {
            return 0;
        }
        sprintf (prdk_kf.variante, "%hd", atoi (svariante->variante));
        fHead.SetText ();
        SetCurrentName ("variante");
        fHead.GetCfield ("variante")->PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
        return 0;
}


BOOL SpezDlg::ShowMasch (int i)
{
        struct SMASCH *smasch;
        SEARCHMASCH SearchMasch;

        SearchMasch.SetParams (DLG::hInstance, ActiveDlg->GethWnd ());
        SearchMasch.Setawin (ActiveDlg->GethMainWindow ());
        SearchMasch.Search ();
        ActiveDlg->Validate ();
        if (syskey == KEY5)
        {
            fHead.SetCurrentName (_fProdPhasen[i]->GetName ());
            return 0;
        }

        smasch = SearchMasch.GetSMasch ();
        if (smasch == NULL)
        {
            return 0;
        }

        clipped (smasch->masch_nr);
        clipped (smasch->masch_bz);

        strcpy (Masch [(i - 1) / 3], smasch->masch_nr);
        strcpy (MaschBz [(i - 1) / 3], smasch->masch_bz);
        _fProdPhasen[i]->SetText ();
        _fMaschBz[(i - 1) / 3]->SetText ();
        SetCurrentName (_fProdPhasen[i]->GetName ());
        return 0;
}


BOOL SpezDlg::OnKey9 ()
{
         CFIELD *Cfield;
         HWND hWnd;

         syskey = KEY9;
         hWnd = GetFocus ();


         if (hWnd == fHead.GetCfield ("mdn_choise")->GethWnd ())
         {
             return ShowMdn ();
         }


         if (hWnd == fHead.GetCfield ("a_choise")->GethWnd ())
         {
             return ShowA ();
         }

         if (hWnd == fHead.GetCfield ("rez_choise")->GethWnd ())
         {
             return ShowRez ();
         }

         if (hWnd == fHead.GetCfield ("variante_choise")->GethWnd ())
         {
             return ShowVariante ();
         }
//         if (hWnd == fPos.GetCfield ("prod_abt_choise")->GethWnd ())
//         {
////             return ShowAbt ();
//         }

         Cfield = ActiveDlg->GetCurrentCfield ();

         if (strcmp (Cfield->GetName (), "mdn") == 0)
         {
             return ShowMdn ();
         }


         if (strcmp (Cfield->GetName (), "a") == 0)
         {
             return ShowA ();
         }

         if (strcmp (Cfield->GetName (), "rez") == 0)
         {
             return ShowRez ();
         }
         if (strcmp (Cfield->GetName (), "variante") == 0)
         {
             return ShowVariante ();
         }

         for (int i = 1; _fProdPhasen [i] != NULL; i += 3)
         {
             if (strcmp (Cfield->GetName (), _fProdPhasen[i]->GetName ()) == 0)
             {
                    return ShowMasch (i);
             }
         }


         if (VarbLst != NULL)
         {
             return VarbLst->OnKey9 ();
         }

         if (ZutLst != NULL)
         {
             return ZutLst->OnKey9 ();
         }

         if (HuelLst != NULL)
         {
             return HuelLst->OnKey9 ();
         }

         if (VpkLst != NULL)
         {
             return VpkLst->OnKey9 ();
         }

         if (KostLst != NULL)
         {
             return KostLst->OnKey9 ();
         }

         if (ZeitLst != NULL)
         {
             return ZeitLst->OnKey9 ();
         }
         if (QuidLst != NULL)
         {
             return QuidLst->OnKey9 ();
         }

         if (Cfield->GetAttribut () == CCOMBOBOX)
         {
             SendMessage (Cfield->GethWnd (),
                     CB_SHOWDROPDOWN, TRUE,  NULL);
             return TRUE;
         }
         return FALSE;
}


BOOL SpezDlg::OnPaint (HWND hWnd,HDC hdc, UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (fWork && this->hWnd == hWnd)
        {
             PrintMess (MessText);
             fWork->display (hWnd, hdc);
             return TRUE;
        }
        return DLG::OnPaint (hWnd,hdc, msg, wParam,lParam);
}


BOOL SpezDlg::OnActivate (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (LOWORD (wParam) == WA_ACTIVE ||
                LOWORD (wParam) == WA_CLICKACTIVE)
        {
              if (WithTabStops)
              {
                  SetTabFocus (); 
              }
              else
              {
                  fWork->SetFocus ();
              }
              return TRUE;
        }
        return FALSE;
}


BOOL SpezDlg::OnCloseUp (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
/*
	    if ((HWND) lParam == hwndCombo1)
		{
			ChoiseCombo1 ();
			return TRUE;
		}
		else if ((HWND) lParam == hwndCombo2)
		{
			ChoiseCombo2 ();
			return TRUE;
		}
*/
		return FALSE;
}

BOOL SpezDlg::OnClicked (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{       
        HWND hWndBu = (HWND) lParam; 
        if (hWndBu == fPos.GetCfield ("akv")->GethWnd ())
        {
		        fWork->GetText ();
				setWindowText (1); 
		}
        if (hWndBu == fPos.GetCfield ("rework")->GethWnd ())
        {
		        fWork->GetText ();
				setWindowText (1); 
		}
        if (hWndBu == fPos.GetCfield ("ek")->GethWnd ())
        {
                TestNewCalc ();  
				setWindowText (1);
                return TRUE;
        }

        if (hWndBu == fPos.GetCfield ("plan_ek")->GethWnd ())
        {
                TestNewCalc ();  
				setWindowText (1);
                return TRUE;
        }

        if (hWndBu == fPosZeit.GetCfield ("bezgroesse")->GethWnd ())
        {
		        fPosZeit.GetText ();
				ActiveSpezDlg->CalcPersKosten ();
                return TRUE;
        }
        if (hWndBu == fPosZeit.GetCfield ("kalkgroesse")->GethWnd ())
        {
		        fPosZeit.GetText ();
				ActiveSpezDlg->CalcPersKosten ();
                return TRUE;
        }

        for (int i = 0; _fProdPhasen[i] != NULL; i ++)
        {
                if (hWndBu == _fProdPhasen[i]->GethWnd ())
                {
                    _fProdPhasen[i]->GetText ();
                    if (strcmp ((char *) _fProdPhasen[i]->GetCheckboxFeld (), "J") == 0)
                    {
                            _fProdPhasen[i + 1]->Enable (TRUE);
                            _fProdPhasen[i + 2]->Enable (TRUE); //MaschZeit
                    }
                    else
                    {
                            _fProdPhasen[i + 1]->Enable (FALSE);
                            _fProdPhasen[i + 2]->Enable (FALSE); //MaschZeit
                            strcpy (Masch[i / 3], "0");
                            strcpy (MaschZeit[i / 3], "0");
                            strcpy (MaschBz[i / 3], "");
                            _fProdPhasen[i + 1]->SetText ();
                            _fProdPhasen[i + 2]->SetText ();
                            _fMaschBz[i / 3]->SetText ();
                    }
                    ActiveDlg->NewTabStops (&fPrdk_k0);
                    return TRUE;
                }
        }
        return FALSE;
}


BOOL SpezDlg::OnCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (fWork)
        {

			  if (dOnKey12 == TRUE) 
			  {
				 dOnKey12 = FALSE;
//		   		 if (abfragejn (hWnd, "Chargengr��e �ndern ?  Mengen der Verarbeitungsmaterialien und Zutaten werden angepasst ", "N"))
//				 {
					double dchg_gew = prdk_k.chg_gew;
				 	CalcChg();
					work.rechne_varb();
				    work.rechne_zut();
		  			work.SetChgFaktor(1);
					int di =  OnKey12();
					work.CommitWork ();
					prdk_k.chg_gew = prdk_k.kutter_gew = dchg_gew;
					work.BeginWork ();
					work.dbupdate_prdk_k();
					work.CommitWork ();
		  			work.SetChgFaktor(1);
					return di;
//				 }
//				 else
//				 {
//						sprintf(prdk_kf.chg_gew,"%.3lf",chg_gew_old);
//						prdk_k.chg_gew = chg_gew_old;
//					    fHead.SetText ();
//				 }
			  }


              if (LOWORD (wParam) == CANCEL_CTL)
              {
                  return OnKey5 ();
              }
              if (LOWORD (wParam) == OK_CTL)
              {
                  return OnKey12 ();
              }
              if (LOWORD (wParam) == IDM_CHOISE)
              {
                  return OnKey9 ();
              }

              if (LOWORD (wParam) == IDM_DELETE)
              {
                 return OnKey7 ();
              }

              if (LOWORD (wParam) == IDM_INSERT)
              {
                 return OnKey6 (); 
              }

              if (LOWORD (wParam) == IDM_PRINT)
              {
//				 if (VarbLst == NULL && ZutLst == NULL && HuelLst == NULL && VpkLst == NULL 
//					 && KostLst == NULL && ZeitLst == NULL)
//				 {

						RestartProcess(TRUE);
						PrintRez();
						syskey = 0; //ist n�tig, weil in PrintRez schon OnKey12 enthalten ist
//				 }

                 return TRUE;
              }
              if (HIWORD (wParam) == WM_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              if (HIWORD (wParam) == EN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == CBN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == WM_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == LBN_SELCHANGE)
              {
//                  return ListChanged (wParam, lParam);
              }
              else if (HIWORD (wParam) == EN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == CBN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == BN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == BN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == BN_CLICKED)
              {
                  OnClicked (hWnd, msg, wParam, lParam);
              }
        }
        return DLG::OnCommand (hWnd, msg, wParam, lParam);
}

BOOL SpezDlg::IsKeyDlgMsg (MSG *msg, BOOL *FocusSet)
{

	    if (msg->message != WM_KEYDOWN)
		{
			return FALSE;
		}


		if (fPosZeit.GetCfield ("zeit_c1")->GethWnd () == GetFocus ())
		{
			   
	         return TRUE;
		}

		if (fPosZeit.GetCfield ("zeit_m2")->GethWnd () == GetFocus ())
		{
	        switch (msg->wParam)
			{
			      case VK_RETURN :
                       ZeitLst->GeteListe ()->SetPos (0,0);
                       ZeitLst->SetListFocus ();
					   ZeitLst->CalcPersKosten ();
					   fPosZeit.GetCfield ("zeit_m2")->SetFocus ();	
//					   NeuCalc();

					   msg->wParam = KEYLEFT;
					   *FocusSet = TRUE;
			           return TRUE;
			      case VK_DOWN :
                       ZeitLst->GeteListe ()->SetPos (0,0);
                       ZeitLst->SetListFocus ();
					   ZeitLst->CalcPersKosten ();
					   *FocusSet = TRUE;
			           return FALSE;
                  default :
					   return TRUE;
			}
		}
     if (ZeitLst != NULL)
	 {

		if (ZeitLst->InFirstField ())
		{
			switch (msg->wParam)
			{
                  case VK_TAB :
                       if (GetKeyState (VK_SHIFT) >= 0)
					   {
						   break;
					   }
			      case VK_UP :
                      fPosZeit.GetCfield ("zeit_m2")->SetFocus ();					    
			  	      *FocusSet = TRUE;
		              return FALSE;
			}
		}
	 }

		if (fKost.GetCfield ("bearb_hk")->GethWnd () == GetFocus ()) 
		{
			 if (syskey == KEYUP)
			 {
	 			return TRUE;
			 }
			 else
			 {
//	 			fKost.GetCfield ("bearb_sk")->SetFocus ();	
			 }
	         return TRUE;
		}
		if (fKost.GetCfield ("bearb_sk")->GethWnd () == GetFocus ()) 
		{
			   
			 if (syskey == KEYUP)
			 {
//	 			fKost.GetCfield ("bearb_hk")->SetFocus ();	
			 }
			 else
			 {
//	 			fKost.GetCfield ("bearb_fek")->SetFocus ();	
			 }
	         return TRUE;
		}
		if (fKost.GetCfield ("bearb_fek")->GethWnd () == GetFocus ()) 
		{
			   
			 if (syskey == KEYUP)
			 {
//	 			fKost.GetCfield ("bearb_sk")->SetFocus ();	
			 }
			 else
			 {
//	 			fKost.GetCfield ("bearb_fvk")->SetFocus ();	
			 }
	         return TRUE;
		}

		if (fKost.GetCfield ("bearb_fvk")->GethWnd () == GetFocus ())   
		{
			 if (syskey == KEYUP)
			 {
	 			fKost.GetCfield ("bearb_fek")->SetFocus ();	
				return TRUE;
			 }
	        switch (msg->wParam)
			{
			      case VK_RETURN :
                       KostLst->GeteListe ()->SetPos (0,0);
                       KostLst->SetListFocus ();
//					   KostLst->CalcPersKosten ();
					   fKost.GetCfield ("bearb_fvk")->SetFocus ();	

					   msg->wParam = KEYLEFT;
					   *FocusSet = TRUE;
			           return TRUE;
			      case VK_DOWN :
                       KostLst->GeteListe ()->SetPos (0,0);
                       KostLst->SetListFocus ();
//					   KostLst->CalcPersKosten ();
					   *FocusSet = TRUE;
			           return FALSE;
                  default :
					   return TRUE;
			}
		}
     if (KostLst != NULL)
	 {
		if (KostLst->InFirstField ())
		{
			switch (msg->wParam)
			{
                  case VK_TAB :
                       if (GetKeyState (VK_SHIFT) >= 0)
					   {
						   break;
					   }
			      case VK_UP :
                      fKost.GetCfield ("bearb_hk")->SetFocus ();					    
			  	      *FocusSet = TRUE;
		              return FALSE;
			}
		}
	 }
		return FALSE;
}


BOOL SpezDlg::OnPreTranslateMessage (MSG *msg)
{
        int ret;


        if (VarbLst != NULL)
        {
            ret = VarbLst->GeteListe ()->PerformMessage (msg);
            if (ret && (syskey == KEY5 ||
                        syskey == KEY12))
            {
                   VarbLst->DestroyWindows ();
                   delete VarbLst;
                   VarbLst = NULL;
                   return FALSE;
            }
            else
            {
                   return TRUE;
            }
        }
        else if (A_Lst != NULL)
        {
            ret = A_Lst->GeteListe ()->PerformMessage (msg);
            if (ret && (syskey == KEY5 ||
                        syskey == KEY12))
            {
                   A_Lst->DestroyWindows ();
                   delete A_Lst;
                   A_Lst = NULL;
                   return FALSE;
            }
            else
            {
                   return TRUE;
            }
        }
        else if (ZutLst != NULL)
        {
            ret = ZutLst->GeteListe ()->PerformMessage (msg);
            if (ret && (syskey == KEY5 ||
                        syskey == KEY12))
            {
                   ZutLst->DestroyWindows ();
                   delete ZutLst;
                   ZutLst = NULL;
                   return FALSE;
            }
            else
            {
                   return TRUE;
            }
        }
        else if (HuelLst != NULL)
        {
            ret = HuelLst->GeteListe ()->PerformMessage (msg);
            if (ret && (syskey == KEY5 ||
                        syskey == KEY12))
            {
                   HuelLst->DestroyWindows ();
                   delete HuelLst;
                   HuelLst = NULL;
                   return FALSE;
            }
            else
            {
                   return TRUE;
            }
        }
        else if (VpkLst != NULL)
        {
            ret = VpkLst->GeteListe ()->PerformMessage (msg);
            if (ret && (syskey == KEY5 ||
                        syskey == KEY12))
            {
                   VpkLst->DestroyWindows ();
                   delete VpkLst;
                   VpkLst = NULL;
                   return FALSE;
            }
            else
            {
                   return TRUE;
            }
        }
        else if (KostLst != NULL)
        {
            BOOL FocusSet = FALSE;
  		    ret = IsKeyDlgMsg (msg, &FocusSet);
			if (ret)
			{
				         return FALSE;
			}
			if (FocusSet)
			{
				   return TRUE;
			}
            ret = KostLst->GeteListe ()->PerformMessage (msg);
            if (ret && (syskey == KEY5 ||
                        syskey == KEY12))
            {
                   KostLst->DestroyWindows ();
                   delete KostLst;
                   KostLst = NULL;
                   return FALSE;
            }
            else
            {
                   return TRUE;
            }
        }
        else if (ZeitLst != NULL)
        {
            BOOL FocusSet = FALSE;
  		    ret = IsKeyDlgMsg (msg, &FocusSet);
			if (ret)
			{
				         return FALSE;
			}

			if (FocusSet)
			{
				   return TRUE;
			}
            ret = ZeitLst->GeteListe ()->PerformMessage (msg);
            if (ret && (syskey == KEY5 ||
                        syskey == KEY12))
            {
                   ZeitLst->DestroyWindows ();
                   delete ZeitLst;
                   ZeitLst = NULL;
                   return FALSE;
            }
            else
            {
                   return TRUE;
            }
        }
        else if (QuidLst != NULL)
        {
            BOOL FocusSet = FALSE;
  		    ret = IsKeyDlgMsg (msg, &FocusSet);
			if (ret)
			{
				         return FALSE;
			}

			if (FocusSet)
			{
				   return TRUE;
			}
            ret = QuidLst->GeteListe ()->PerformMessage (msg);
            if (ret && (syskey == KEY5 ||
                        syskey == KEY12))
            {
                   QuidLst->DestroyWindows ();
                   delete QuidLst;
                   QuidLst = NULL;
                   return FALSE;
            }
            else
            {
                   return TRUE;
            }
        }
        return FALSE;
}

void SpezDlg::SetSummen (double summegew, double ekds, double kosten, int type,double chgmenge,double swmenge)
{

	if (type == 999) //netto-gewicht wird �bergeben
    {
        sprintf (gewsumme, "%.3lf", summegew);
        prdk_k.varb_gew_nto       = summegew;
	}
	if (type == ARB_ZEIT)
    {
		return;
			//=====MatZutBep====
			if ((prdk_k.varb_gew + prdk_k.zut_gew) == 0.0)
			{
				sprintf (ZutBep, "%.3lf",  0.0); 
			}
			else
			{
				sprintf (ZutBep, "%.3lf",  work.Round (((prdk_k.varb_hk_vollk   * prdk_k.chg_gew) 
														+ (prdk_k.zut_hk_vollk   * prdk_k.chg_gew))
													/ 
													(prdk_k.varb_gew + prdk_k.zut_gew),3)); 
			}
			MatZutWert = ratod(ZutBep);

			//=====HuelVpkWert====
			if ((prdk_k.huel_gew + prdk_k.vpk_gew) == 0.0)
			{
				sprintf (VpkBep, "%.3lf",  0.0); 
			}
			else
			{
				sprintf (VpkBep, "%.3lf",  work.Round (((prdk_k.huel_hk_vollk   * prdk_k.chg_gew) 
														+ (prdk_k.vpk_hk_vollk   * prdk_k.chg_gew))
													/ 
													(prdk_k.huel_gew + prdk_k.vpk_gew),3)); 
			}
			HuelVpkWert = ratod(ZutBep);
			return;
    }
	 //================ Anzeige Summen bei type AVRB,ZUT,HEUL,VPK =====================

        sprintf (gewsumme, "%.3lf", summegew);

		if (SummenAnzeige == 1)
		{
			// bezogen auf Gesamtmenge incl. Schwund
			sprintf (preiskg,  "%.4lf", (ekds * chgmenge) / (chgmenge + swmenge));
			sprintf (kostenkg, "%.4lf", (kosten * chgmenge) / (chgmenge + swmenge));
		}
		else if (SummenAnzeige == 2)
		{
			// bezogen auf Menge auf Tabseite
		    sprintf (preiskg,  "%.4lf", (ekds * chgmenge / summegew));
			sprintf (kostenkg, "%.4lf", (kosten * chgmenge / summegew));
		}
		else
		{
			// bezogen auf Chargengr��e
		    sprintf (preiskg,  "%.4lf", ekds);
			sprintf (kostenkg, "%.4lf", kosten);
		}

        summegew = work.Round (summegew, 3);
//LuD 260403        ekds     = work.Round (ekds, 4);
//LuD 260403        kosten   = work.Round (kosten, 4);
        fVarbFoot.SetText ();
        if (type == VARB)
        {
                 prdk_k.varb_hk_vollk  = ekds + kosten;
                 prdk_k.varb_hk_teilk  = ekds + kosten;
                 prdk_k.varb_mat_o_b   = ekds;
                 prdk_k.varb_gew       = summegew;
/*
                 double chg_gew = prdk_k.chg_gew;
                 prdk_k.chg_gew = prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew;
                 prdk_k.zut_mat_o_b *= chg_gew / prdk_k.chg_gew; 
                 prdk_k.zut_hk_vollk *= chg_gew / prdk_k.chg_gew; 
                 prdk_k.zut_hk_teilk *= chg_gew / prdk_k.chg_gew; 
*/
        }
        else if (type == ZUT)
        {
                 prdk_k.zut_hk_vollk = ekds + kosten;;
                 prdk_k.zut_hk_teilk = ekds + kosten;;
                 prdk_k.zut_mat_o_b  = ekds;
                 prdk_k.zut_gew      = summegew;
/*
                 double chg_gew = prdk_k.chg_gew;
                 prdk_k.chg_gew = prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew;
                 prdk_k.varb_mat_o_b *= chg_gew / prdk_k.chg_gew; 
                 prdk_k.varb_hk_vollk *= chg_gew / prdk_k.chg_gew; 
                 prdk_k.varb_hk_teilk *= chg_gew / prdk_k.chg_gew; 
*/
        }
        else if (type == HUEL)
        {
                 prdk_k.huel_hk_vollk = ekds + kosten;;
                 prdk_k.huel_hk_teilk = ekds + kosten;;
                 prdk_k.huel_mat_o_b  = ekds;
                 prdk_k.huel_gew      = summegew;

                 prdk_k.huel_wrt      = work.GetHuelWrt ();
                 prdk_k.huel_hk_wrt   = work.GetHuelHkWrt ();
/*
                 double chg_gew = prdk_k.chg_gew;
                 prdk_k.chg_gew = prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew;
                 prdk_k.varb_mat_o_b *= chg_gew / prdk_k.chg_gew; 
                 prdk_k.varb_hk_vollk *= chg_gew / prdk_k.chg_gew; 
                 prdk_k.varb_hk_teilk *= chg_gew / prdk_k.chg_gew; 
                 prdk_k.zut_mat_o_b *= chg_gew / prdk_k.chg_gew; 
                 prdk_k.zut_hk_vollk *= chg_gew / prdk_k.chg_gew; 
                 prdk_k.zut_hk_teilk *= chg_gew / prdk_k.chg_gew; 
*/
        }
        else if (type == VPK)
        {
                 prdk_k.vpk_hk_vollk = ekds + kosten;
                 prdk_k.vpk_hk_teilk = ekds + kosten;
                 prdk_k.vpk_mat_o_b  = ekds;
                 prdk_k.vpk_gew      = summegew;
                 prdk_k.vpk_wrt      = work.GetVpkWrt ();
                 prdk_k.vpk_hk_wrt   = work.GetVpkHkWrt ();
        }

        else if (type == KA)
        {
//                 prdk_k.kost_hk_vollk = ekds;
//                 prdk_k.kost_hk_teilk = ekds;
//               prdk_k.kost_gew      = summegew;
        }

        double chg_gew = prdk_k.chg_gew;
        prdk_k.chg_gew = prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew;
		prdk_k.kutter_gew = prdk_k.chg_gew;

        if (type != VARB)
        {
			(prdk_k.chg_gew == 0) ? prdk_k.varb_mat_o_b = 0l : prdk_k.varb_mat_o_b *= chg_gew / prdk_k.chg_gew; 
            (prdk_k.chg_gew == 0) ? prdk_k.varb_hk_vollk = 0l : prdk_k.varb_hk_vollk *= chg_gew / prdk_k.chg_gew; 
            (prdk_k.chg_gew == 0) ? prdk_k.varb_hk_teilk = 0l : prdk_k.varb_hk_teilk *= chg_gew / prdk_k.chg_gew; 
			  /* LuD 260403
              prdk_k.varb_mat_o_b = work.Round (prdk_k.varb_mat_o_b, 4);
              prdk_k.varb_hk_vollk = work.Round (prdk_k.varb_hk_vollk, 4);
              prdk_k.varb_hk_teilk = work.Round (prdk_k.varb_hk_teilk, 4);
			  */
        }
        if (type != ZUT)
        {
			(prdk_k.chg_gew == 0) ? prdk_k.zut_mat_o_b = 0l : prdk_k.zut_mat_o_b *= chg_gew / prdk_k.chg_gew; 
            (prdk_k.chg_gew == 0) ? prdk_k.zut_hk_vollk = 0l : prdk_k.zut_hk_vollk *= chg_gew / prdk_k.chg_gew; 
            (prdk_k.chg_gew == 0) ? prdk_k.zut_hk_teilk = 0l : prdk_k.zut_hk_teilk *= chg_gew / prdk_k.chg_gew; 
			  /* LuD 260403
              prdk_k.zut_mat_o_b = work.Round (prdk_k.zut_mat_o_b, 4);
              prdk_k.zut_hk_vollk = work.Round (prdk_k.zut_hk_vollk, 4);
              prdk_k.zut_hk_teilk = work.Round (prdk_k.zut_hk_teilk, 4);
			  */
        }
		
        ToForm (dbfields); 
		sprintf(prdk_kf.chg_gew,"%.3lf",ratod(prdk_kf.chg_gew));

        prdk_k.rez_hk_vollk = 0.0; 
        prdk_k.rez_hk_teilk = 0.0;
        prdk_k.rez_mat_o_b  = 0.0;
        prdk_k.bto_hk_vollk = 0.0;
        prdk_k.bto_hk_teilk = 0.0;
        prdk_k.bto_mat_o_b  = 0.0;
        prdk_k.nto_hk_vollk = 0.0;
        prdk_k.nto_hk_teilk = 0.0;
        prdk_k.nto_mat_o_b  = 0.0;
        prdk_k.nvpk_hk_vollk  = 0.0;
        prdk_k.nvpk_hk_teilk  = 0.0;
        prdk_k.nvpk_mat_o_b   = 0.0;

        if ((prdk_k.varb_gew + prdk_k.zut_gew) != 0.0)
        {
             prdk_k.rez_hk_vollk = (prdk_k.varb_hk_vollk * prdk_k.varb_gew +
                                    prdk_k.zut_hk_vollk * prdk_k.zut_gew) /
                                    (prdk_k.varb_gew + prdk_k.zut_gew);
             prdk_k.rez_hk_teilk = (prdk_k.varb_hk_vollk * prdk_k.varb_gew +
                                    prdk_k.zut_hk_vollk * prdk_k.zut_gew) /
                                    (prdk_k.varb_gew + prdk_k.zut_gew);

             prdk_k.rez_mat_o_b   = (prdk_k.varb_mat_o_b  * prdk_k.varb_gew +
                                     prdk_k.zut_mat_o_b  * prdk_k.zut_gew) /
                                    (prdk_k.varb_gew + prdk_k.zut_gew);
        }

        if ((prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew) != 0.0)
        {
              prdk_k.bto_hk_vollk = (prdk_k.varb_hk_vollk * prdk_k.varb_gew +
                                     prdk_k.zut_hk_vollk * prdk_k.zut_gew +
                                     prdk_k.huel_hk_wrt) /
                                     (prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew);

              prdk_k.bto_hk_teilk = (prdk_k.varb_hk_vollk * prdk_k.varb_gew +
                                     prdk_k.zut_hk_vollk * prdk_k.zut_gew +
                                     prdk_k.huel_hk_wrt) /
                                     (prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew);

              prdk_k.bto_mat_o_b = (prdk_k.varb_mat_o_b * prdk_k.varb_gew +
                                    prdk_k.zut_mat_o_b * prdk_k.zut_gew +
                                    prdk_k.huel_wrt) /
                                   (prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew);
        }

        if (((prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew) * (1 - prdk_k.sw_kalk / 100)) 
              != 0.0)
        {
             prdk_k.nto_hk_vollk = (prdk_k.varb_hk_vollk * prdk_k.varb_gew +
                                    prdk_k.zut_hk_vollk * prdk_k.zut_gew +
                                    prdk_k.huel_hk_wrt) /
                                   ((prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew) *
                                   (1 - prdk_k.sw_kalk / 100));

             prdk_k.nto_hk_teilk = (prdk_k.varb_hk_vollk * prdk_k.varb_gew +
                                    prdk_k.zut_hk_vollk * prdk_k.zut_gew +
                                    prdk_k.huel_hk_wrt) /
                                    ((prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew) *
                                    (1 - prdk_k.sw_kalk / 100));

              prdk_k.nto_mat_o_b = (prdk_k.varb_mat_o_b * prdk_k.varb_gew +
                                   prdk_k.zut_mat_o_b * prdk_k.zut_gew +
                                   prdk_k.huel_wrt) /
                                   ((prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew) *
                                   (1 - prdk_k.sw_kalk / 100));
        }

        if (((prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew) * 
             (1 - prdk_k.sw_kalk / 100) + prdk_k.vpk_gew) 
              != 0.0)
        {
              prdk_k.nvpk_hk_vollk  = (prdk_k.varb_hk_vollk * prdk_k.varb_gew +
                                    prdk_k.zut_hk_vollk * prdk_k.zut_gew +
                                    prdk_k.huel_hk_wrt +
                                    prdk_k.vpk_hk_wrt)/
                                    ((prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew) *
                                     (1 - prdk_k.sw_kalk / 100) + prdk_k.vpk_gew);
              prdk_k.nvpk_hk_teilk  = (prdk_k.varb_hk_vollk * prdk_k.varb_gew +
                                       prdk_k.zut_hk_vollk * prdk_k.zut_gew +
                                       prdk_k.huel_hk_wrt +
                                       prdk_k.vpk_hk_wrt)/
                                       ((prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew) *
                                        (1 - prdk_k.sw_kalk / 100) + prdk_k.vpk_gew);
              prdk_k.nvpk_mat_o_b   = (prdk_k.varb_mat_o_b * prdk_k.varb_gew +
                                       prdk_k.zut_mat_o_b * prdk_k.zut_gew +
                                       prdk_k.huel_wrt +
                                       prdk_k.vpk_wrt)/
                                       ((prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew) *
                                       (1 - prdk_k.sw_kalk / 100) + prdk_k.vpk_gew);
        }
		NeuCalc();
}


void SpezDlg::NeuCalc (void)
{
			//=====MatZutBep====
			if ((prdk_k.varb_gew + prdk_k.zut_gew) == 0.0)
			{
				sprintf (ZutBep, "%.3lf",  0.0); 
			}
			else
			{

				sprintf (ZutBep, "%.3lf",  work.Round (((prdk_k.varb_hk_vollk   * prdk_k.chg_gew) 
														+ (prdk_k.zut_hk_vollk   * prdk_k.chg_gew))
													       ,3)); 
			}
			//=====HuelVpkWert====
			sprintf (VpkBep, "%.3lf",  work.Round (((prdk_k.huel_hk_vollk   * prdk_k.chg_gew) 
														+ (prdk_k.vpk_hk_vollk   * prdk_k.chg_gew))
													       ,3)); 
             KostWert = work.GetSuKostZut () +
						work.GetSuKostVarb () +
						work.GetSuKostHuel () +
						work.GetSuKostVpk () +
						work.GetSuKostSw () +
						work.GetSuKostAufs ();
			 KostWertVgl = KostWert;

			if (prdk_k.chg_gew == 0)
			{
				MatZutWert = ratod(ZutBep);
				HuelVpkWert = ratod(VpkBep);
            }
			else
            {
				MatZutWert = ratod(ZutBep) * ratod (prdk_kf.zeit_m1) /  prdk_k.chg_gew;
				MatZutWertVgl = ratod(ZutBep) * ratod (prdk_kf.zeit_m2) /  prdk_k.chg_gew;
				HuelVpkWert = ratod(VpkBep) * ratod (prdk_kf.zeit_m1) /  prdk_k.chg_gew;
				HuelVpkWertVgl = ratod(VpkBep) * ratod (prdk_kf.zeit_m2) /  prdk_k.chg_gew;
				KostWert = KostWert * ratod (prdk_kf.zeit_m1) /  prdk_k.chg_gew;
				KostWertVgl = KostWertVgl * ratod (prdk_kf.zeit_m2) /  prdk_k.chg_gew;
            }
			if (ratod (prdk_kf.zeit_m1) == 0)
			{
				StkGesWert = 0;
				StkGesWertVgl = 0;
            }
			else
            {
				StkGesWert = (MatZutWert + HuelVpkWert + ratod (prdk_kf.zeit_p3) + KostWert)
				          / ratod (prdk_kf.zeit_m1);
				StkGesWertVgl = (MatZutWertVgl + HuelVpkWertVgl + ratod (prdk_kf.zeit_p30) 
								+ KostWertVgl)
								/ ratod (prdk_kf.zeit_m2);
            }
			sprintf (prdk_kf.zeit_p1, "%.3lf",  MatZutWert); 
			sprintf (prdk_kf.zeit_p10, "%.3lf",  MatZutWertVgl); 
			sprintf (prdk_kf.zeit_p2, "%.3lf",  HuelVpkWert); 
			sprintf (prdk_kf.zeit_p20, "%.3lf",  HuelVpkWertVgl); 
			sprintf (prdk_kf.zeit_p4, "%.3lf",  KostWert); 
			sprintf (prdk_kf.zeit_p40, "%.3lf",  KostWertVgl); 
			sprintf (prdk_kf.zeit_p5, "%.3lf",  StkGesWert); 
			sprintf (prdk_kf.zeit_p50, "%.3lf",  StkGesWertVgl); 
//			ToForm (dbfields);
		 if (ActiveDlg->GetDialog () == &fPrdk_k0Zeit)
		 {
		      fPosZeit.SetText ();
		 }
//            fPosZeit.GetCfield ("zeit_p1")->SetText ();

		 if (ActiveDlg->GetDialog () == &fPrdk_k0Quid)
		 {
		      fPosQuid.SetText ();
		 }

}









void SpezDlg::TestNewCalc (void)
{
         char ek [10];
         char plan_ek [10];

         strcpy (ek, prdk_kf.ek);
         strcpy (plan_ek, prdk_kf.plan_ek);
         fPos.GetText ();
         if (strcmp (ek, prdk_kf.ek) == 0)
         {
             return;
         }
         CalcNewKost ();
}

/**
Systemzeit holen.
**/
int systime5 (char *zeit)
{
 time_t timer;
 struct tm *ltime;

 time (&timer);
 ltime = localtime (&timer);
 sprintf (zeit, "%02d:%02d", ltime->tm_hour,
                                  ltime->tm_min);
 return (0);
}

void SpezDlg::Batchlauf (void) 
{

	char cMessage[30];
        char wert [5];
    work.BeginWork ();
	systime5 (kalkzeit);
	int x = A_Lst->GetRecanz();
	int i = (56 * 2 / x);
	if (i == 0) i = 1;

//         PROGRESS *Progress = new PROGRESS (hWnd, -1, -2, 56, 0, FALSE);
//         Progress->SetStep (i);

    for (i = 0; i < x; i ++)
    {
//         Progress->StepIt ();
		A_Lst->SetA_Lsts(i);
		prdk_k.mdn = atoi(prdk_kf.mdn);
		if (work.ReadPrdk_k(prdk_k.mdn)) return;
		prdk_k_mdn = prdk_k.mdn ;
		prdk_k_a = prdk_k.a;
		strcpy (prdk_k_rez, prdk_k.rez) ;
		prdk_k_var = prdk_k.variante;

	    sprintf (prdk_kf.a,        "%13.0f",      prdk_k.a);
        work.GetABez (prdk_kf.a_bz1, prdk_k.a,sys_ben.pers_nam);

//		CalcNewKostBatch();
         sprintf (wert, "%d", _a_bas.me_einh);
         work.GetPtab (EinhPos0, "me_einh", wert);
         work.GetPtab (EinhPos, "me_einh", wert);

		CalcNewKost();
        work.WritePrdk_k (prdk_k.mdn, prdk_k.a, prdk_k.rez, kalkzeit,prdk_k.rez_bz);
 	    work.FuelleQuid (prdk_k.rez,prdk_k.variante,1,prdk_k.chg_gew,1.0,0.0);
	    work.RechneQuid ();

	    work.CommitWork ();
	    work.BeginWork ();
	    sprintf(cMessage, "%ld von %ld ",i+1,x);
        strcpy (prdk_kf.pageinfo, cMessage);
	    fHead.SetText ();
        Validate ();

    }
//    delete Progress;
	strcpy(a_preis.zeit,kalkzeit);
	char cdatum [11];
	sysdate(cdatum); 
    a_preis.dat = dasc_to_long (cdatum);
	long anzahlKalk = work.GetAnzKalk ();
//    sprintf(cMessage, "%ld von %ld erfolgreich ",anzahlKalk,x);
//    strcpy (prdk_kf.pageinfo, cMessage);
    fHead.SetText ();
//    Wmess.Message (hInstance, hMainWindow, cMessage);



}

void SpezDlg::CalcNewKost (void)
{

         PROGRESS *Progress = new PROGRESS (hWnd, -1, -2, 56, 0, FALSE);
         Progress->SetStep (14);

         Progress->StepIt ();

		 work.ReportDelete();

// Verarbeitungsmaterial
         if (VarbLst == NULL)
         {
             VarbLst = new VARBLST ();
         }
         if (strcmp (prdk_kf.plan_ek, "J") == 0)
         {
            VarbLst->SetPlanEk (TRUE);
            work.SetPlanEk (TRUE);
         }
         else
         {
            VarbLst->SetPlanEk (FALSE);
            work.SetPlanEk (FALSE);
         }
         VarbLst->SetWork (&work);
         VarbLst->SetDlg (this);
         if (sys_ben.zahl == 99)
         {
     		 prdk_k.mdn = prdk_k_mdn ;
  			 prdk_k.a = prdk_k_a;
			 strcpy (prdk_k.rez, prdk_k_rez) ;
			 prdk_k.variante = prdk_k_var;
         }
         VarbLst->FillDB ();
         VarbLst->CalcSum ();
         VarbLst->ReportFill ();
         delete VarbLst;
         VarbLst = NULL;

         Progress->StepIt ();
// Zutaten

         if (ZutLst == NULL)
         {
             ZutLst = new ZUTLST ();
         }
         if (strcmp (prdk_kf.plan_ek, "J") == 0)
         {
            ZutLst->SetPlanEk (TRUE);
            work.SetPlanEk (TRUE);
         }
         else
         {
            ZutLst->SetPlanEk (FALSE);
            work.SetPlanEk (FALSE);
         }
         ZutLst->SetWork (&work);
         ZutLst->SetDlg (this);

         if (sys_ben.zahl == 99)
         {
     		 prdk_k.mdn = prdk_k_mdn ;
  			 prdk_k.a = prdk_k_a;
			 strcpy (prdk_k.rez, prdk_k_rez) ;
			 prdk_k.variante = prdk_k_var;
         }
         ZutLst->FillDB ();
         ZutLst->CalcSum ();
         ZutLst->ReportFill ();
         delete ZutLst;
         ZutLst = NULL;

         Progress->StepIt ();


// H�llen

         if (HuelLst == NULL)
         {
             HuelLst = new HUELLST ();
         }
         if (strcmp (prdk_kf.plan_ek, "J") == 0)
         {
            HuelLst->SetPlanEk (TRUE);
            work.SetPlanEk (TRUE);
         }
         else
         {
            HuelLst->SetPlanEk (FALSE);
            work.SetPlanEk (FALSE);
         }
         HuelLst->SetWork (&work);
         HuelLst->SetDlg (this);

         if (sys_ben.zahl == 99)
         {
     		 prdk_k.mdn = prdk_k_mdn ;
  			 prdk_k.a = prdk_k_a;
			 strcpy (prdk_k.rez, prdk_k_rez) ;
			 prdk_k.variante = prdk_k_var;
         }
		 HuelLst->FillDB ();
         HuelLst->CalcSum ();
         HuelLst->ReportFill ();
         delete HuelLst;
         HuelLst = NULL;

         Progress->StepIt ();

// Verpackung

         if (VpkLst == NULL)
         {
             VpkLst = new VPKLST ();
         }
         if (strcmp (prdk_kf.plan_ek, "J") == 0)
         {
            VpkLst->SetPlanEk (TRUE);
            work.SetPlanEk (TRUE);
         }
         else
         {
            VpkLst->SetPlanEk (FALSE);
            work.SetPlanEk (FALSE);
         }
         VpkLst->SetWork (&work);
         VpkLst->SetDlg (this);

         if (sys_ben.zahl == 99)
         {
     		 prdk_k.mdn = prdk_k_mdn ;
  			 prdk_k.a = prdk_k_a;
			 strcpy (prdk_k.rez, prdk_k_rez) ;
			 prdk_k.variante = prdk_k_var;
         }
		 work.UpdateVpkMe(); //210807
         VpkLst->FillDB ();
         VpkLst->CalcSum ();//#250603
         VpkLst->ReportFill ();
         delete VpkLst;
         VpkLst = NULL;

         Progress->StepIt ();


// Arbeitszeit

         if (ZeitLst == NULL)
         {
             ZeitLst = new ZEITLST ();
         }
         if (strcmp (prdk_kf.plan_ek, "J") == 0)
         {
            ZeitLst->SetPlanEk (TRUE);
            work.SetPlanEk (TRUE);
         }
         else
         {
            ZeitLst->SetPlanEk (FALSE);
            work.SetPlanEk (FALSE);
         }
         ZeitLst->SetWork (&work);
         ZeitLst->SetDlg (this);

         if (sys_ben.zahl == 99)
         {
     		 prdk_k.mdn = prdk_k_mdn ;
  			 prdk_k.a = prdk_k_a;
			 strcpy (prdk_k.rez, prdk_k_rez) ;
			 prdk_k.variante = prdk_k_var;
         }
         ZeitLst->FillDB ();
         ZeitLst->CalcSum ();//#250603
         ZeitLst->ReportFill ();
         delete ZeitLst;
         ZeitLst = NULL;



         Progress->StepIt ();

// Kosten

         if (KostLst == NULL)
         {
             KostLst = new KOSTLST ();
         }
         if (strcmp (prdk_kf.plan_ek, "J") == 0)
         {
            KostLst->SetPlanEk (TRUE);
            work.SetPlanEk (TRUE);
         }
         else
         {
            KostLst->SetPlanEk (FALSE);
            work.SetPlanEk (FALSE);
         }
         KostLst->SetWork (&work);
         KostLst->SetDlg (this);

         if (sys_ben.zahl == 99)
         {
     		 prdk_k.mdn = prdk_k_mdn ;
  			 prdk_k.a = prdk_k_a;
			 strcpy (prdk_k.rez, prdk_k_rez) ;
			 prdk_k.variante = prdk_k_var;
         }

         KostLst->FillDB ();
         KostLst->CalcSum ();  //#250603
         KostLst->ReportFill ();
         delete KostLst;
         KostLst = NULL;
         Progress->StepIt ();
         prdk_k.chg_gew = work.GetVarbGew () + work.GetZutGew () + work.GetHuelGew ();
  		 prdk_k.kutter_gew = prdk_k.chg_gew;

         ToForm (dbfields); 
         fPos.GetCfield ("chg_gew")->SetText ();  
		 ReportFill ();
         delete Progress;
//		 work.SetChgFaktor(1);
//240305		 chg_gew_old = ratod(prdk_kf.chg_gew);
}

void SpezDlg::CalcNewKostBatch (void)
{
         if (strcmp (prdk_kf.plan_ek, "J") == 0)
         {
            work.SetPlanEk (TRUE);
         }
         else
         {
            work.SetPlanEk (FALSE);
		 }
    	 prdk_k.mdn = prdk_k_mdn ;
 		 prdk_k.a = prdk_k_a;
		 strcpy (prdk_k.rez, prdk_k_rez) ;
		 prdk_k.variante = prdk_k_var;
		 work.CalcSumVarbBatch();
//         Progress->StepIt ();

// Zutaten

         if (ZutLst == NULL)
         {
             ZutLst = new ZUTLST ();
         }
         if (strcmp (prdk_kf.plan_ek, "J") == 0)
         {
            ZutLst->SetPlanEk (TRUE);
            work.SetPlanEk (TRUE);
         }
         else
         {
            ZutLst->SetPlanEk (FALSE);
            work.SetPlanEk (FALSE);
         }
         ZutLst->SetWork (&work);
         ZutLst->SetDlg (this);

         if (sys_ben.zahl == 99)
         {
     		 prdk_k.mdn = prdk_k_mdn ;
  			 prdk_k.a = prdk_k_a;
			 strcpy (prdk_k.rez, prdk_k_rez) ;
			 prdk_k.variante = prdk_k_var;
         }
         ZutLst->FillDB ();
         ZutLst->CalcSum ();
         ZutLst->ReportFill ();
         delete ZutLst;
         ZutLst = NULL;

//         Progress->StepIt ();


// H�llen

         if (HuelLst == NULL)
         {
             HuelLst = new HUELLST ();
         }
         if (strcmp (prdk_kf.plan_ek, "J") == 0)
         {
            HuelLst->SetPlanEk (TRUE);
            work.SetPlanEk (TRUE);
         }
         else
         {
            HuelLst->SetPlanEk (FALSE);
            work.SetPlanEk (FALSE);
         }
         HuelLst->SetWork (&work);
         HuelLst->SetDlg (this);

         if (sys_ben.zahl == 99)
         {
     		 prdk_k.mdn = prdk_k_mdn ;
  			 prdk_k.a = prdk_k_a;
			 strcpy (prdk_k.rez, prdk_k_rez) ;
			 prdk_k.variante = prdk_k_var;
         }
		 HuelLst->FillDB ();
         HuelLst->CalcSum ();
         HuelLst->ReportFill ();
         delete HuelLst;
         HuelLst = NULL;

//         Progress->StepIt ();

// Verpackung

         if (VpkLst == NULL)
         {
             VpkLst = new VPKLST ();
         }
         if (strcmp (prdk_kf.plan_ek, "J") == 0)
         {
            VpkLst->SetPlanEk (TRUE);
            work.SetPlanEk (TRUE);
         }
         else
         {
            VpkLst->SetPlanEk (FALSE);
            work.SetPlanEk (FALSE);
         }
         VpkLst->SetWork (&work);
         VpkLst->SetDlg (this);

         if (sys_ben.zahl == 99)
         {
     		 prdk_k.mdn = prdk_k_mdn ;
  			 prdk_k.a = prdk_k_a;
			 strcpy (prdk_k.rez, prdk_k_rez) ;
			 prdk_k.variante = prdk_k_var;
         }
         VpkLst->FillDB ();
         VpkLst->CalcSum ();//#250603
         VpkLst->ReportFill ();
         delete VpkLst;
         VpkLst = NULL;

//         Progress->StepIt ();


// Arbeitszeit

         if (ZeitLst == NULL)
         {
             ZeitLst = new ZEITLST ();
         }
         if (strcmp (prdk_kf.plan_ek, "J") == 0)
         {
            ZeitLst->SetPlanEk (TRUE);
            work.SetPlanEk (TRUE);
         }
         else
         {
            ZeitLst->SetPlanEk (FALSE);
            work.SetPlanEk (FALSE);
         }
         ZeitLst->SetWork (&work);
         ZeitLst->SetDlg (this);

         if (sys_ben.zahl == 99)
         {
     		 prdk_k.mdn = prdk_k_mdn ;
  			 prdk_k.a = prdk_k_a;
			 strcpy (prdk_k.rez, prdk_k_rez) ;
			 prdk_k.variante = prdk_k_var;
         }
         ZeitLst->FillDB ();
         ZeitLst->CalcSum ();//#250603
         ZeitLst->ReportFill ();
         delete ZeitLst;
         ZeitLst = NULL;



//         Progress->StepIt ();

// Kosten

         if (KostLst == NULL)
         {
             KostLst = new KOSTLST ();
         }
         if (strcmp (prdk_kf.plan_ek, "J") == 0)
         {
            KostLst->SetPlanEk (TRUE);
            work.SetPlanEk (TRUE);
         }
         else
         {
            KostLst->SetPlanEk (FALSE);
            work.SetPlanEk (FALSE);
         }
         KostLst->SetWork (&work);
         KostLst->SetDlg (this);

         if (sys_ben.zahl == 99)
         {
     		 prdk_k.mdn = prdk_k_mdn ;
  			 prdk_k.a = prdk_k_a;
			 strcpy (prdk_k.rez, prdk_k_rez) ;
			 prdk_k.variante = prdk_k_var;
         }

         KostLst->FillDB ();
         KostLst->CalcSum ();  //#250603
         KostLst->ReportFill ();
         delete KostLst;
         KostLst = NULL;
//         Progress->StepIt ();
         prdk_k.chg_gew = work.GetVarbGew () + work.GetZutGew () + work.GetHuelGew ();
  		 prdk_k.kutter_gew = prdk_k.chg_gew;

         ToForm (dbfields); 
         fPos.GetCfield ("chg_gew")->SetText ();  
		 ReportFill ();
//         delete Progress;
//		 work.SetChgFaktor(1);
//240305		 chg_gew_old = ratod(prdk_kf.chg_gew);
}





void SpezDlg::ReportFill (void)
{
    reportrez.grouprez = 0;   //Kopf-Informationen
    reportrez.a = prdk_k.a;
    strcpy(reportrez.a_bz1, prdk_kf.a_bz1);						
    strcpy(reportrez.rez ,prdk_k.rez);
    strcpy(reportrez.rez_bz, prdk_k.rez_bz);
    reportrez.variante = prdk_k.variante;
    strcpy(reportrez.variante_bz, prdk_k.variante_bz);
    if (strcmp(prdk_kf.ek,"J") == 0)  
    {
		strcpy(reportrez.preis_basis, "EK");				
	}
	else
    {
		strcpy(reportrez.preis_basis, "PLAN-EK");			
	}
    strcpy(reportrez.k_a_bez, EinhTxt0);      // Preis / Einh
    reportrez.bep = ratod(StkBep);
    reportrez.schwund = prdk_k.sw_kalk;
    reportrez.chargengewicht = prdk_k.chg_gew;
    strcpy(reportrez.datum, "01.01.2003");                      //muss nocht !
    reportrez.posi = 0;
    reportrez.mat = 0;
    strcpy(reportrez.mat_bz, "");
    reportrez.menge = 0;
    strcpy(reportrez.einheit, "");
    reportrez.ek = 0.0;
	    reportrez.wert_ek = ratod(StkSK);  //131008 zur Darstellung des SK-Preises (M�fli)
	    reportrez.wert_bep = ratod(KgSK);
    reportrez.kosten = 0.0;
    reportrez.k_diff = 0.0;
    reportrez.kostenkg = 0.0;
    reportrez.kostenart = 0;
    strcpy(reportrez.kostenart_bz, "");
    reportrez.mdn = prdk_k.mdn;
    work.ReportRezUpdate ();

	    reportrez.grouprez = 50;
	    reportrez.posi = 1;
	    strcpy(reportrez.k_a_bez, "Verarb.Mat");           //Bezeichnung f�r Kosten und Aufschl�ge
	    reportrez.a = prdk_k.a;
	    strcpy(reportrez.a_bz1, prdk_kf.a_bz1);			
	    strcpy(reportrez.rez ,prdk_k.rez);
	    strcpy(reportrez.rez_bz, prdk_k.rez_bz);
	    reportrez.variante = prdk_k.variante;
	    strcpy(reportrez.variante_bz, prdk_k.variante_bz);
	    if (strcmp(prdk_kf.ek,"J") == 0)  
	    {
			strcpy(reportrez.preis_basis, "EK");				
		}
		else
		{
			strcpy(reportrez.preis_basis, "PLAN-EK");			
		}
	    reportrez.schwund = prdk_k.sw_kalk;
	    reportrez.chargengewicht = prdk_k.chg_gew;
	    strcpy(reportrez.datum, "01.01.2003");                      //muss nocht !
	    reportrez.mat = 0;
	    strcpy(reportrez.mat_bz, "");
	    reportrez.menge = ratod(VarbMe);
	    strcpy(reportrez.einheit, "");
	    reportrez.ek = ratod(VarbEk);
	    reportrez.wert_ek = ratod(VarbWrtEk);
	    reportrez.kosten = ratod(VarbKost);
	    reportrez.bep = ratod(VarbBep);
	    reportrez.wert_bep = ratod(VarbWrtBep);
	    reportrez.k_diff = ratod(VarbDiff);
	    reportrez.kostenkg = ratod(VarbKostE);
	    reportrez.kostenart = 0;
	    strcpy(reportrez.kostenart_bz, "");
	    reportrez.mdn = prdk_k.mdn;
	    work.ReportRezUpdate ();

		if (GetTabZutaten() == TRUE)
		{
			reportrez.grouprez = 50;
			reportrez.posi = 2;
			strcpy(reportrez.k_a_bez, "Zutaten");           //Bezeichnung f�r Kosten und Aufschl�ge
			reportrez.a = prdk_k.a;
			strcpy(reportrez.a_bz1, prdk_kf.a_bz1);			
			strcpy(reportrez.rez ,prdk_k.rez);
			strcpy(reportrez.rez_bz, prdk_k.rez_bz);
			reportrez.variante = prdk_k.variante;
			strcpy(reportrez.variante_bz, prdk_k.variante_bz);
			if (strcmp(prdk_kf.ek,"J") == 0)  
			{
				strcpy(reportrez.preis_basis, "EK");				
			}
			else
			{
				strcpy(reportrez.preis_basis, "PLAN-EK");			
			}
			reportrez.schwund = prdk_k.sw_kalk;
			reportrez.chargengewicht = prdk_k.chg_gew;
			strcpy(reportrez.datum, "01.01.2003");                      //muss nocht !
			reportrez.mat = 0;
			strcpy(reportrez.mat_bz, "");
			reportrez.menge = ratod(ZutMe);
			strcpy(reportrez.einheit, "");
			reportrez.ek = ratod(ZutEk);
			reportrez.wert_ek = ratod(ZutWrtEk);
			reportrez.kosten = ratod(ZutKost);
			reportrez.bep = ratod(ZutBep);
			reportrez.wert_bep = ratod(ZutWrtBep);
			reportrez.k_diff = ratod(ZutDiff);
			reportrez.kostenkg = ratod(ZutKostE);
			reportrez.kostenart = 0;
			strcpy(reportrez.kostenart_bz, "");
			reportrez.mdn = prdk_k.mdn;
			work.ReportRezUpdate ();
		}

	    reportrez.grouprez = 50;
	    reportrez.posi = 3;
	    strcpy(reportrez.k_a_bez, "H�llen");           //Bezeichnung f�r Kosten und Aufschl�ge
	    reportrez.a = prdk_k.a;
	    strcpy(reportrez.a_bz1, prdk_kf.a_bz1);	
	    strcpy(reportrez.rez ,prdk_k.rez);
	    strcpy(reportrez.rez_bz, prdk_k.rez_bz);
	    reportrez.variante = prdk_k.variante;
	    strcpy(reportrez.variante_bz, prdk_k.variante_bz);
	    if (strcmp(prdk_kf.ek,"J") == 0)  
	    {
			strcpy(reportrez.preis_basis, "EK");				
		}
		else
		{
			strcpy(reportrez.preis_basis, "PLAN-EK");			
		}
	    reportrez.schwund = prdk_k.sw_kalk;
	    reportrez.chargengewicht = prdk_k.chg_gew;
	    strcpy(reportrez.datum, "01.01.2003");                      //muss nocht !
	    reportrez.mat = 0;
	    strcpy(reportrez.mat_bz, "");
	    reportrez.menge = ratod(HuelMe);
	    strcpy(reportrez.einheit, "");
	    reportrez.ek = ratod(HuelEk);
	    reportrez.wert_ek = ratod(HuelWrtEk);
	    reportrez.kosten = ratod(HuelKost);
	    reportrez.bep = ratod(HuelBep);
	    reportrez.wert_bep = ratod(HuelWrtBep);
	    reportrez.k_diff = ratod(HuelDiff);
	    reportrez.kostenkg = ratod(HuelKostE);
	    reportrez.kostenart = 0;
	    strcpy(reportrez.kostenart_bz, "");
	    reportrez.mdn = prdk_k.mdn;
	    work.ReportRezUpdate ();

	    reportrez.grouprez = 50;
	    reportrez.posi = 4;
	    strcpy(reportrez.k_a_bez, "Schwund");           //Bezeichnung f�r Kosten und Aufschl�ge
	    reportrez.a = prdk_k.a;
	    strcpy(reportrez.a_bz1, prdk_kf.a_bz1);	
	    strcpy(reportrez.rez ,prdk_k.rez);
	    strcpy(reportrez.rez_bz, prdk_k.rez_bz);
	    reportrez.variante = prdk_k.variante;
	    strcpy(reportrez.variante_bz, prdk_k.variante_bz);
	    if (strcmp(prdk_kf.ek,"J") == 0)  
	    {
			strcpy(reportrez.preis_basis, "EK");				
		}
		else
		{
			strcpy(reportrez.preis_basis, "PLAN-EK");			
		}
	    reportrez.schwund = prdk_k.sw_kalk;
	    reportrez.chargengewicht = prdk_k.chg_gew;
	    strcpy(reportrez.datum, "01.01.2003");                      //muss nocht !
	    reportrez.mat = 0;
	    strcpy(reportrez.mat_bz, "");
	    reportrez.menge = ratod(SwMe);
	    strcpy(reportrez.einheit, "");
	    reportrez.ek = ratod(SwEk);
	    reportrez.wert_ek = ratod(SwWrtEk);
	    reportrez.kosten = ratod(SwKost);
	    reportrez.bep = ratod(SwBep);
	    reportrez.wert_bep = ratod(SwWrtBep);
	    reportrez.k_diff = ratod(SwDiff);
	    reportrez.kostenkg = ratod(SwKostE);
	    reportrez.kostenart = 0;
	    strcpy(reportrez.kostenart_bz, "");
	    reportrez.mdn = prdk_k.mdn;
	    work.ReportRezUpdate ();
	    reportrez.grouprez = 4;
	    reportrez.posi = 1;
//	    reportrez.menge = prdk_k.sw_kalk *  (ratod(VarbMe) + ratod(ZutMe) + ratod(HuelMe)) / 100 * (-1);
	    reportrez.menge = prdk_k.sw_kalk *  (prdk_k.varb_gew_nto + ratod(ZutMe) + ratod(HuelMe)) / 100 * (-1);
		
	    work.ReportRezUpdate ();                       //120207 das gleiche nochmal zur Darstellung unterhalb SummeH�llen





		if (GetTabArbeitszeit() == TRUE)
		{
		    reportrez.grouprez = 50;
			reportrez.posi = 5;
			strcpy(reportrez.k_a_bez, "Arbeitszeit");           //Bezeichnung Arbeitszeiten / Arbeitswerte
			reportrez.a = prdk_k.a;
			strcpy(reportrez.a_bz1, prdk_kf.a_bz1);	
			strcpy(reportrez.rez ,prdk_k.rez);
			strcpy(reportrez.rez_bz, prdk_k.rez_bz);
			reportrez.variante = prdk_k.variante;
			strcpy(reportrez.variante_bz, prdk_k.variante_bz);
			if (strcmp(prdk_kf.ek,"J") == 0)  
			{
				strcpy(reportrez.preis_basis, "EK");				
			}
			else
			{
				strcpy(reportrez.preis_basis, "PLAN-EK");			
			}
			reportrez.schwund = prdk_k.sw_kalk;
			reportrez.chargengewicht = prdk_k.chg_gew;
			strcpy(reportrez.datum, "01.01.2003");                      //muss nocht !
			reportrez.mat = 0;
			strcpy(reportrez.mat_bz, "");
			reportrez.menge = ratod(ApzMe);
			strcpy(reportrez.einheit, "");
			reportrez.ek = ratod(ApzEk);
			reportrez.wert_ek = ratod(ApzWrtEk);
			reportrez.kosten = ratod(ApzKost);
			reportrez.bep = ratod(ApzBep);
			reportrez.wert_bep = ratod(ApzWrtBep);
			reportrez.k_diff = ratod(ApzDiff);
			reportrez.kostenkg = ratod(ApzKostE);
			reportrez.kostenart = 0;
			strcpy(reportrez.kostenart_bz, "");
			reportrez.mdn = prdk_k.mdn;
			work.ReportRezUpdate ();
		}



		if (GetTabVerpackung() == TRUE)
		{
		    reportrez.grouprez = 50;
		    reportrez.posi = 6;
		    strcpy(reportrez.k_a_bez, "Verpackung");           //Bezeichnung f�r Kosten und Aufschl�ge
		    reportrez.a = prdk_k.a;
			strcpy(reportrez.a_bz1, prdk_kf.a_bz1);	
			strcpy(reportrez.rez ,prdk_k.rez);
			strcpy(reportrez.rez_bz, prdk_k.rez_bz);
			reportrez.variante = prdk_k.variante;
			strcpy(reportrez.variante_bz, prdk_k.variante_bz);
			if (strcmp(prdk_kf.ek,"J") == 0)  
			{
				strcpy(reportrez.preis_basis, "EK");				
			}
			else
			{
				strcpy(reportrez.preis_basis, "PLAN-EK");			
			}
			reportrez.schwund = prdk_k.sw_kalk;
			reportrez.chargengewicht = prdk_k.chg_gew;
			strcpy(reportrez.datum, "01.01.2003");                      //muss nocht !
			reportrez.mat = 0;
			strcpy(reportrez.mat_bz, "");
			reportrez.menge = ratod(VpkMe);
			strcpy(reportrez.einheit, "");
			reportrez.ek = ratod(VpkEk);
			reportrez.wert_ek = ratod(VpkWrtEk);
			reportrez.kosten = ratod(VpkKost);
			reportrez.bep = ratod(VpkBep);
		    reportrez.wert_bep = ratod(VpkWrtBep);
			reportrez.k_diff = ratod(VpkDiff);
			reportrez.kostenkg = ratod(VpkKostE);
			reportrez.kostenart = 0;
			strcpy(reportrez.kostenart_bz, "");
			reportrez.mdn = prdk_k.mdn;
			work.ReportRezUpdate ();
		}

	    reportrez.grouprez = 50;
	    reportrez.posi = 7;
	    strcpy(reportrez.k_a_bez, "Herstellkosten");           //Bezeichnung f�r Kosten und Aufschl�ge
	    reportrez.a = prdk_k.a;
	    strcpy(reportrez.a_bz1, prdk_kf.a_bz1);	
	    strcpy(reportrez.rez ,prdk_k.rez);
	    strcpy(reportrez.rez_bz, prdk_k.rez_bz);
	    reportrez.variante = prdk_k.variante;
	    strcpy(reportrez.variante_bz, prdk_k.variante_bz);
	    if (strcmp(prdk_kf.ek,"J") == 0)  
	    {
			strcpy(reportrez.preis_basis, "EK");				
		}
		else
		{
			strcpy(reportrez.preis_basis, "PLAN-EK");			
		}
	    reportrez.schwund = prdk_k.sw_kalk;
	    reportrez.chargengewicht = prdk_k.chg_gew;
	    strcpy(reportrez.datum, "01.01.2003");                      //muss nocht !
	    reportrez.mat = 0;
	    strcpy(reportrez.mat_bz, "");
	    reportrez.menge = ratod(AfsMe);
	    strcpy(reportrez.einheit, "");
	    reportrez.ek = ratod(AfsEk);
	    reportrez.wert_ek = ratod(AfsWrtEk);
	    reportrez.kosten = ratod(AfsKost);
	    reportrez.bep = ratod(AfsBep);
	    reportrez.wert_bep = ratod(AfsWrtBep);
	    reportrez.k_diff = ratod(AfsDiff);
	    reportrez.kostenkg = ratod(AfsKostE);
	    reportrez.kostenart = 0;
	    strcpy(reportrez.kostenart_bz, "");
	    reportrez.mdn = prdk_k.mdn;
	    work.ReportRezUpdate ();



	    reportrez.grouprez = 50;
	    reportrez.posi = 8;      // Preis / kg   bzw Preis / <Einheit>
	    strcpy(reportrez.k_a_bez, EinhTxt0);           //Bezeichnung f�r Kosten und Aufschl�ge
	    reportrez.a = prdk_k.a;
	    strcpy(reportrez.a_bz1, prdk_kf.a_bz1);	
	    strcpy(reportrez.rez ,prdk_k.rez);
	    strcpy(reportrez.rez_bz, prdk_k.rez_bz);
	    reportrez.variante = prdk_k.variante;
	    strcpy(reportrez.variante_bz, prdk_k.variante_bz);
	    if (strcmp(prdk_kf.ek,"J") == 0)  
	    {
			strcpy(reportrez.preis_basis, "EK");				
		}
		else
		{
			strcpy(reportrez.preis_basis, "PLAN-EK");			
		}
	    reportrez.schwund = prdk_k.sw_kalk;
	    reportrez.chargengewicht = prdk_k.chg_gew;
	    strcpy(reportrez.datum, "01.01.2003");                      //muss nocht !
	    reportrez.mat = 0;
	    strcpy(reportrez.mat_bz, "");
	    reportrez.menge = ratod(AGew);
	    strcpy(reportrez.einheit, &EinhTxt0[2]);
	    reportrez.ek = ratod(StkEk);
	    reportrez.wert_ek = ratod(StkEk);
	    reportrez.kosten = 0.0;
	    reportrez.wert_bep = ratod(StkBep);
	    reportrez.kostenkg = ratod(StkBepGes);
	    reportrez.bep = ratod(StkBep);
	    reportrez.k_diff = 0.0;
	    reportrez.kostenart = 0;
	    strcpy(reportrez.kostenart_bz, "");
	    reportrez.mdn = prdk_k.mdn;
	    work.ReportRezUpdate ();

	    reportrez.grouprez = 60;
	    reportrez.posi = 1;      
	    strcpy(reportrez.k_a_bez, "SK-Rampe");  
	    reportrez.a = prdk_k.a;
	    strcpy(reportrez.a_bz1, prdk_kf.a_bz1);	
	    strcpy(reportrez.rez ,prdk_k.rez);
	    strcpy(reportrez.rez_bz, prdk_k.rez_bz);
	    reportrez.variante = prdk_k.variante;
	    strcpy(reportrez.variante_bz, prdk_k.variante_bz);
	    if (strcmp(prdk_kf.ek,"J") == 0)  
	    {
			strcpy(reportrez.preis_basis, "EK");				
		}
		else
		{
			strcpy(reportrez.preis_basis, "PLAN-EK");			
		}
	    reportrez.schwund = prdk_k.sw_kalk;
	    reportrez.chargengewicht = prdk_k.chg_gew;
	    strcpy(reportrez.datum, "01.01.2003");                      //muss nocht !
	    reportrez.mat = 0;
	    strcpy(reportrez.mat_bz, "");
	    reportrez.menge = ratod(AGew);
	    strcpy(reportrez.einheit, "");
	    reportrez.ek = 0.0;
	    reportrez.bep = 0.0;
	    reportrez.k_diff = 0.0;
	    reportrez.wert_ek = ratod(StkSK);
	    reportrez.wert_bep = ratod(KgSK);
	    reportrez.kosten = ratod(SKKost);
	    reportrez.kostenkg = ratod(SKKostE);
	    reportrez.kostenart = 0;
	    strcpy(reportrez.kostenart_bz, "");
	    reportrez.mdn = prdk_k.mdn;
	    work.ReportRezUpdate ();

	    reportrez.grouprez = 60;
	    reportrez.posi = 2;      
	    strcpy(reportrez.k_a_bez, "Filial-EK");  
	    reportrez.a = prdk_k.a;
	    strcpy(reportrez.a_bz1, prdk_kf.a_bz1);	
	    strcpy(reportrez.rez ,prdk_k.rez);
	    strcpy(reportrez.rez_bz, prdk_k.rez_bz);
	    reportrez.variante = prdk_k.variante;
	    strcpy(reportrez.variante_bz, prdk_k.variante_bz);
	    if (strcmp(prdk_kf.ek,"J") == 0)  
	    {
			strcpy(reportrez.preis_basis, "EK");				
		}
		else
		{
			strcpy(reportrez.preis_basis, "PLAN-EK");			
		}
	    reportrez.schwund = prdk_k.sw_kalk;
	    reportrez.chargengewicht = prdk_k.chg_gew;
	    strcpy(reportrez.datum, "01.01.2003");                      //muss nocht !
	    reportrez.mat = 0;
	    strcpy(reportrez.mat_bz, "");
	    reportrez.menge = ratod(AGew);
	    strcpy(reportrez.einheit, "");
	    reportrez.ek = 0.0;
	    reportrez.bep = 0.0;
	    reportrez.k_diff = 0.0;
	    reportrez.wert_ek = ratod(StkFEK);
	    reportrez.wert_bep = ratod(KgFEK);
	    reportrez.kosten = ratod(FEKKost);
	    reportrez.kostenkg = ratod(FEKKostE);
	    reportrez.kostenart = 0;
	    strcpy(reportrez.kostenart_bz, "");
	    reportrez.mdn = prdk_k.mdn;
	    work.ReportRezUpdate ();


	    reportrez.grouprez = 60;
	    reportrez.posi = 3;      
	    strcpy(reportrez.k_a_bez, "Filial-VK");
	    reportrez.a = prdk_k.a;
	    strcpy(reportrez.a_bz1, prdk_kf.a_bz1);	
	    strcpy(reportrez.rez ,prdk_k.rez);
	    strcpy(reportrez.rez_bz, prdk_k.rez_bz);
	    reportrez.variante = prdk_k.variante;
	    strcpy(reportrez.variante_bz, prdk_k.variante_bz);
	    if (strcmp(prdk_kf.ek,"J") == 0)  
	    {
			strcpy(reportrez.preis_basis, "EK");				
		}
		else
		{
			strcpy(reportrez.preis_basis, "PLAN-EK");			
		}
	    reportrez.schwund = prdk_k.sw_kalk;
	    reportrez.chargengewicht = prdk_k.chg_gew;
	    strcpy(reportrez.datum, "01.01.2003");                      //muss nocht !
	    reportrez.mat = 0;
	    strcpy(reportrez.mat_bz, "");
	    reportrez.menge = ratod(AGew);
	    strcpy(reportrez.einheit, "");
	    reportrez.ek = 0.0;
	    reportrez.bep = 0.0;
	    reportrez.k_diff = 0.0;
	    reportrez.wert_ek = ratod(StkFVK);
	    reportrez.wert_bep = ratod(KgFVK);
	    reportrez.kosten = ratod(FVKKost);
	    reportrez.kostenkg = ratod(FVKKostE);
	    reportrez.kostenart = 0;
	    strcpy(reportrez.kostenart_bz, "");
	    reportrez.mdn = prdk_k.mdn;
	    work.ReportRezUpdate ();

}

BOOL SpezDlg::EnterALst (void)
{
        RECT rect;
        if (A_Lst == NULL)
        {
            return TRUE;
        }
        A_Lst->SetDlg (this);
        A_Lst->SetWinStyle (WS_CHILD | WS_CLIPSIBLINGS);
//        A_Lst->SetWinStyle (WS_OVERLAPPEDWINDOW | WS_DLGFRAME);
        GetWindowRect (hWnd, &rect);
        int DockY = fHead.GetCfield ("abstand")->GetYP () + rect.top;
        A_Lst->SetDockY (DockY);
//        A_Lst->SetDockY (332);
        A_Lst->SethMenu (hMenu);
        A_Lst->SethwndTB (hwndTB);
        A_Lst->SethMainWindow (hWnd);
        A_Lst->SetTextMetric (&DlgTm);
        A_Lst->SetLineRow (50);
        A_Lst->SetWork (&work);
        if (strcmp (prdk_kf.plan_ek, "J") == 0)
        {
            A_Lst->SetPlanEk (TRUE);
            work.SetPlanEk (TRUE);
        }
        else
        {
            A_Lst->SetPlanEk (FALSE);
            work.SetPlanEk (FALSE);
        }

// Liste nicht vorhanden, Editiermodus mit Meldungsschleife im aktuellen Dialog
        A_Lst->SetDockButtonSpace (0);
        A_Lst->ShowList ();
        A_Lst->SetWorkMode (TRUE);
		if (sys_ben.fil != 1)
        {
			A_Lst->EnterList ();
		}

        return TRUE;
} 

BOOL SpezDlg::EnterVarbLst (void)
{
        RECT rect;
        if (VarbLst == NULL)
        {
            return TRUE;
        }
        VarbLst->SetDlg (this);
        VarbLst->SetWinStyle (WS_THICKFRAME | WS_CHILD | WS_CLIPSIBLINGS);
//        VarbLst->SetWinStyle (WS_CHILD | WS_CLIPSIBLINGS);
        GetWindowRect (hWnd, &rect);
        int DockY = fPos.GetCfield ("pos_frame")->GetYP () + rect.top;
//      VarbLst->SetButtonSize (BIG);
        VarbLst->SetDockY (DockY);
        VarbLst->SethMenu (hMenu);
        VarbLst->SethwndTB (hwndTB);
        VarbLst->SethMainWindow (hWnd);
        VarbLst->SetTextMetric (&DlgTm);
        VarbLst->SetLineRow (50);
        VarbLst->SetWork (&work);
        if (strcmp (prdk_kf.plan_ek, "J") == 0)
        {
            VarbLst->SetPlanEk (TRUE);
            work.SetPlanEk (TRUE);
        }
        else
        {
            VarbLst->SetPlanEk (FALSE);
            work.SetPlanEk (FALSE);
        }
// Button, wenn vorhanden Initialisieren
        if (work.GetVarbLsts () != NULL)
        {
        }

// Liste nicht vorhanden, Editiermodus mit Meldungsschleife im aktuellen Dialog
        VarbLst->SetDockButtonSpace (4);
        VarbLst->ShowList ();
        VarbLst->SetWorkMode (TRUE);
//        VarbLst->EnterList ();

        return TRUE;
} 

BOOL SpezDlg::EnterZutLst (void)
{
        RECT rect;
        if (ZutLst == NULL)
        {
            return TRUE;
        }
        ZutLst->SetDlg (this);
        ZutLst->SetWinStyle (WS_THICKFRAME | WS_CHILD | WS_CLIPSIBLINGS);
        GetWindowRect (hWnd, &rect);
        int DockY = fPos.GetCfield ("pos_frame")->GetYP () + rect.top;
//        ZutLst->SetButtonSize (BIG);
        ZutLst->SetDockY (DockY);
        ZutLst->SethMenu (hMenu);
        ZutLst->SethwndTB (hwndTB);
        ZutLst->SethMainWindow (hWnd);
        ZutLst->SetTextMetric (&DlgTm);
        ZutLst->SetLineRow (50);
        ZutLst->SetWork (&work);
        if (strcmp (prdk_kf.plan_ek, "J") == 0)
        {
            ZutLst->SetPlanEk (TRUE);
            work.SetPlanEk (TRUE);
        }
        else
        {
            ZutLst->SetPlanEk (FALSE);
            work.SetPlanEk (FALSE);
        }
// Button, wenn vorhanden Initialisieren
        if (work.GetZutLsts () != NULL)
        {
        }

// Liste nicht vorhanden, Editiermodus mit Meldungsschleife im aktuellen Dialog
        ZutLst->SetDockButtonSpace (4);
        ZutLst->ShowList ();
        ZutLst->SetWorkMode (TRUE);
//        ZutLst->EnterList ();


        return TRUE;
}

BOOL SpezDlg::EnterHuelLst (void)
{
        RECT rect;
        if (HuelLst == NULL)
        {
            return TRUE;
        }
        HuelLst->SetDlg (this);
        HuelLst->SetWinStyle (WS_THICKFRAME | WS_CHILD | WS_CLIPSIBLINGS);
        GetWindowRect (hWnd, &rect);
        int DockY = fPos.GetCfield ("pos_frame")->GetYP () + rect.top;
//        HuelLst->SetButtonSize (SMALL);
        HuelLst->SetDockY (DockY);
        HuelLst->SethMenu (hMenu);
        HuelLst->SethwndTB (hwndTB);
        HuelLst->SethMainWindow (hWnd);
        HuelLst->SetTextMetric (&DlgTm);
        HuelLst->SetLineRow (50);
        HuelLst->SetWork (&work);
        if (strcmp (prdk_kf.plan_ek, "J") == 0)
        {
            HuelLst->SetPlanEk (TRUE);
            work.SetPlanEk (TRUE);
        }
        else
        {
            HuelLst->SetPlanEk (FALSE);
            work.SetPlanEk (FALSE);
        }
// Button, wenn vorhanden Initialisieren
        if (work.GetHuelLsts () != NULL)
        {
        }

// Liste nicht vorhanden, Editiermodus mit Meldungsschleife im aktuellen Dialog
        HuelLst->SetDockButtonSpace (4);
        HuelLst->ShowList ();
        HuelLst->SetWorkMode (TRUE);
//        ZutLst->EnterList ();


        return TRUE;
}

BOOL SpezDlg::EnterVpkLst (void)
{
        RECT rect;
        if (VpkLst == NULL)
        {
            return TRUE;
        }
        VpkLst->SetDlg (this);
        VpkLst->SetWinStyle (WS_THICKFRAME | WS_CHILD | WS_CLIPSIBLINGS);
        GetWindowRect (hWnd, &rect);
        int DockY = fPos.GetCfield ("pos_frame")->GetYP () + rect.top;
        VpkLst->SetDockY (DockY);
        VpkLst->SethMenu (hMenu);
        VpkLst->SethwndTB (hwndTB);
        VpkLst->SethMainWindow (hWnd);
        VpkLst->SetTextMetric (&DlgTm);
        VpkLst->SetLineRow (50);
        VpkLst->SetWork (&work);
        if (strcmp (prdk_kf.plan_ek, "J") == 0)
        {
            VpkLst->SetPlanEk (TRUE);
            work.SetPlanEk (TRUE);
        }
        else
        {
            VpkLst->SetPlanEk (FALSE);
            work.SetPlanEk (FALSE);
        }
// Button, wenn vorhanden Initialisieren
        if (work.GetVpkLsts () != NULL)
        {
        }

// Liste nicht vorhanden, Editiermodus mit Meldungsschleife im aktuellen Dialog
        VpkLst->SetDockButtonSpace (4);
        VpkLst->ShowList ();
        VpkLst->SetWorkMode (TRUE);

        return TRUE;
}

BOOL SpezDlg::EnterKostLst (void)
{
        RECT rect;
        if (KostLst == NULL)
        {
            return TRUE;
        }

        KostLst->SetDlg (this);
//        KostLst->SetWinStyle (WS_THICKFRAME | WS_CHILD | WS_CLIPSIBLINGS);
        KostLst->SetWinStyle (WS_CHILD | WS_CLIPSIBLINGS);
        GetWindowRect (hWnd, &rect);
        int DockY = fKost.GetCfield ("pos_line")->GetYP () + rect.top;
        KostLst->SetDockY (DockY);
        KostLst->SethMenu (hMenu);
        KostLst->SethwndTB (hwndTB);
        KostLst->SethMainWindow (hWnd);
        KostLst->SetTextMetric (&DlgTm);
        KostLst->SetLineRow (50);
        KostLst->SetWork (&work);

        if (strcmp (prdk_kf.plan_ek, "J") == 0)
        {
            KostLst->SetPlanEk (TRUE);
            work.SetPlanEk (TRUE);
        }
        else
        {
            KostLst->SetPlanEk (FALSE);
            work.SetPlanEk (FALSE);
        }
// Button, wenn vorhanden Initialisieren
        if (work.GetKostLsts () != NULL)
        {
        }

// Liste nicht vorhanden, Editiermodus mit Meldungsschleife im aktuellen Dialog
        KostLst->SetDockButtonSpace (4);
        KostLst->ShowList ();
        KostLst->SetWorkMode (TRUE);

        return TRUE;
}

BOOL SpezDlg::EnterZeitLst (void)
{
        RECT rect;
        if (ZeitLst == NULL)
        {
            return TRUE;
        }
        ZeitLst->SetDlg (this);
        ZeitLst->SetWinStyle (WS_THICKFRAME | WS_CHILD | WS_CLIPSIBLINGS);
        GetWindowRect (hWnd, &rect);
        int DockY = fPosZeit.GetCfield ("pos_line")->GetYP () + rect.top;
        ZeitLst->SetDockY (DockY);
        ZeitLst->SethMenu (hMenu);
        ZeitLst->SethwndTB (hwndTB);
        ZeitLst->SethMainWindow (hWnd);
        ZeitLst->SetTextMetric (&DlgTm);
        ZeitLst->SetLineRow (50);
        ZeitLst->SetWork (&work);
/*
        if (strcmp (prdk_kf.plan_ek, "J") == 0)
        {
            ZeitLst->SetPlanEk (TRUE);
            work.SetPlanEk (TRUE);
        }
        else
        {
            ZeitLst->SetPlanEk (FALSE);
            work.SetPlanEk (FALSE);
        }
*/
// Button, wenn vorhanden Initialisieren
        if (work.GetZeitLsts () != NULL)
        {
        }

// Liste nicht vorhanden, Editiermodus mit Meldungsschleife im aktuellen Dialog
//        ZeitLst->SetDockButtonSpace (4);
        ZeitLst->ShowList ();
        ZeitLst->SetWorkMode (TRUE);

        return TRUE;
}

BOOL SpezDlg::EnterQuidLst (void)
{
        RECT rect;
        if (QuidLst == NULL)
        {
            return TRUE;
        }

        QuidLst->SetDlg (this);
        QuidLst->SetWinStyle (WS_THICKFRAME | WS_CHILD | WS_CLIPSIBLINGS);
        GetWindowRect (hWnd, &rect);
        int DockY = fPosQuid.GetCfield ("pos_line")->GetYP () + rect.top;
		QuidLst->SetLocation(400,280);
		QuidLst->SetDimension(450,400);
//        QuidLst->SetDockY (DockY);
        QuidLst->SethMenu (hMenu);
        QuidLst->SethwndTB (hwndTB);
        QuidLst->SethMainWindow (hWnd);
        QuidLst->SetTextMetric (&DlgTm);
        QuidLst->SetLineRow (50);
        QuidLst->SetWork (&work);
        if (work.GetQuidLsts () != NULL)
        {
        }

        QuidLst->ShowList ();
        QuidLst->SetWorkMode (TRUE);

        return TRUE;
}

/*
Verarbeitungsmaterial

static char VarbMe [15];
static char VarbWrtEk [15];
static char VarbWrtBep [15];
static char VarbEk [15];
static char VarbBep [15];
static char VarbDiff [15];
static char VarbKost [15];
static char VarbKostE [15];

Zutaten

static char ZutMe [15];
static char ZutWrtEk [15];
static char ZutWrtBep [15];
static char ZutEk [15];
static char ZutBep [15];
static char ZutDiff [15];
static char ZutKost [15];
static char ZutKostE [15];

Huellen

static char HuelMe [15];
static char HuelWrtEk [15];
static char HuelWrtBep [15];
static char HuelEk [15];
static char HuelBep [15];
static char HuelDiff [15];
static char HuelKost [15];
static char HuelKostE [15];

Schwund

static char SwMe [15];
static char SwWrtEk [15];
static char SwWrtBep [15];
static char SwEk [15];
static char SwBep [15];
static char SwDiff [15];
static char SwKost [15];
static char SwKostE [15];

Verpackung

static char VpkMe [15];
static char VpkWrtEk [15];
static char VpkWrtBep [15];
static char VpkEk [15];
static char VpkBep [15];
static char VpkDiff [15];
static char VpkKost [15];
static char VpkKostE [15];

Aufschlag

static char AfsMe [15];
static char AfsWrtEk [15];
static char AfsWrtBep [15];
static char AfsEk [15];
static char AfsBep [15];
static char AfsDiff [15];
static char AfsKost [15];
static char AfsKostE [15];


Stueckpreis

static char StkEk [15];
static char StkBep [15];

Summen

static char SuMe [15];
static char SuWrtEk [15];
static char SuWrtBep [15];
static char SuEk [15];
static char SuBep [15];
static char SuDiff [15];
static char SuKost [15];
static char SuKostE [15];
*/


void SpezDlg::SetKostFields (void)
{

    sprintf (VarbMe, "%.3lf",  prdk_k.varb_gew);
    sprintf (ZutMe,  "%.3lf",  prdk_k.zut_gew);
    sprintf (HuelMe, "%.3lf",  prdk_k.huel_gew);
    double SwProz = prdk_k.sw_kalk / 100;
	double Menge_bto = prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew;
	double Menge_nto = prdk_k.varb_gew_nto + prdk_k.zut_gew + prdk_k.huel_gew;
	

	//Schwund jetzt mit varb_gew_nto 
	//zus�tzlich ist die Schwundmenge die differenz zw. varb_gew und varb_gew_nto
    sprintf (SwMe,   "%.3lf",  ((prdk_k.varb_gew - prdk_k.varb_gew_nto) + SwProz * 
                 (Menge_nto)) * -1);
	SwProz = (Menge_bto -  (Menge_bto - ratod(SwMe))) / Menge_bto ;
	SwProz = SwProz * -1; 
    sprintf (VpkMe, "%.3lf",  prdk_k.vpk_gew);

    sprintf (SuMe,  "%.3lf", ratod (VarbMe) +
                             ratod (ZutMe)  +
                             ratod (HuelMe) +
                             ratod (VpkMe)  +
                             ratod (SwMe)); 
    work.SetRezGew (ratod (SuMe));

//    sprintf (VarbWrtEk, "%.4lf",  prdk_k.varb_mat_o_b   * prdk_k.varb_gew); 
    sprintf (VarbWrtEk, "%.3lf",  prdk_k.varb_mat_o_b   * prdk_k.chg_gew); 

//    sprintf (ZutWrtEk , "%.4lf",  prdk_k.zut_mat_o_b    * prdk_k.zut_gew); 
    sprintf (ZutWrtEk , "%.3lf",  prdk_k.zut_mat_o_b    * prdk_k.chg_gew); 
    sprintf (HuelWrtEk, "%.3lf",  prdk_k.huel_wrt); 
//    sprintf (SwWrtEk  , "%.4lf",  prdk_k.bto_mat_o_b    * SwProz * -1); 
/*
    sprintf (SwWrtEk  , "%.4lf",  (prdk_k.varb_mat_o_b *  prdk_k.varb_gew +
                                  prdk_k.zut_mat_o_b  *  prdk_k.zut_gew +
                                  prdk_k.huel_wrt) * SwProz * -1); 
*/
    sprintf (SwWrtEk  , "%.3lf",  (ratod (VarbWrtEk) +
                                  ratod (ZutWrtEk) + 
                                  ratod (HuelWrtEk)) * SwProz * -1); 
    sprintf (VpkWrtEk,  "%.3lf",  prdk_k.vpk_wrt); 
    sprintf (AfsWrtEk,  "%.3lf",  work.GetSuKostAufsEk ());


    double SuWrtEkPz  =         ratod (VarbWrtEk) +
                                ratod (ZutWrtEk)  +
                                ratod (HuelWrtEk) +
                                ratod (VpkWrtEk) +
                                ratod (ApzWrtEk); 

    sprintf (SuWrtEk,  "%.3lf", ratod (VarbWrtEk) +
                                ratod (ZutWrtEk)  +
                                ratod (HuelWrtEk) +
//                                ratod (SwWrtEk) + 
                                ratod (VpkWrtEk) +
                                ratod (ApzWrtEk) +
                                ratod (AfsWrtEk)); 

    if (ratod (SuMe) != 0.0)
    {
          sprintf (SuEk,     "%.3lf", ratod (SuWrtEk) / 
                                      ratod (SuMe));
    }
    else
    {
          sprintf (SuEk,     "%.3lf", 0.0); 
    }


//    sprintf (VarbWrtBep, "%.4lf",  prdk_k.varb_hk_vollk   * prdk_k.varb_gew); 
    sprintf (VarbWrtBep, "%.3lf",  prdk_k.varb_hk_vollk   * prdk_k.chg_gew); 
//    sprintf (ZutWrtBep , "%.4lf",  prdk_k.zut_hk_vollk    * prdk_k.zut_gew); 
   sprintf (ZutWrtBep , "%.3lf",  prdk_k.zut_hk_vollk    * prdk_k.chg_gew); 
    sprintf (HuelWrtBep, "%.3lf",  prdk_k.huel_hk_wrt); 
//    sprintf (SwWrtBep  , "%.4lf",  prdk_k.bto_hk_vollk    * SwProz * -1); 
/*
    sprintf (SwWrtBep  , "%.4lf",  (prdk_k.varb_hk_vollk *  prdk_k.varb_gew +
                                    prdk_k.zut_hk_vollk  *  prdk_k.zut_gew +
                                    prdk_k.huel_hk_wrt) * SwProz * -1); 
*/
    sprintf (SwWrtBep  , "%.3lf",  (ratod (VarbWrtBep) +
                                  ratod (ZutWrtBep) + 
                                  ratod (HuelWrtBep)) * SwProz * -1); 
    sprintf (VpkWrtBep,  "%.3lf",  prdk_k.vpk_hk_wrt); 
//100112    sprintf (ApzWrtBep,  "%.3lf",  prdk_k.pers_wrt_bzg / prdk_k.chg_anz_bzg);
    sprintf (ApzWrtBep,  "%.3lf",  prdk_k.pers_wrt_bzg * ((1 - SwProz) ) / prdk_k.chg_anz_bzg); // 100112  Personalkosten : Schwund nicht ber�cksichtigen
    sprintf (AfsWrtBep,  "%.3lf",  work.GetSuKostAufs ());    //TODO 
    if (GetTabArbeitszeit() == FALSE)  sprintf (ApzWrtBep,  "%.3lf",  0.0);


	double SuWrtBepPz = ratod (VarbWrtBep) +
                        ratod (ZutWrtBep)  +
                        ratod (HuelWrtBep) +
                        ratod (VpkWrtBep) +
						ratod (ApzWrtBep);

    sprintf (SuWrtBep,  "%.3lf", ratod (VarbWrtBep) +
                                 ratod (ZutWrtBep)  +
                                 ratod (HuelWrtBep) +
//                                 ratod (SwWrtBep) + 
                                 ratod (VpkWrtBep) +
								 ratod (ApzWrtBep) +
                                 ratod (AfsWrtBep)); 


    if (ratod (SuMe) != 0.0)
    {
           sprintf (SuBep,     "%.3lf", ratod (SuWrtBep) / 
                                        ratod (SuMe));
    }
    else
    {
           sprintf (SuBep,     "%.3lf", 0.0); 
    }

    sprintf (VarbEk, "%.3lf",  prdk_k.varb_mat_o_b); 
    sprintf (ZutEk , "%.3lf",  prdk_k.rez_mat_o_b); 
    sprintf (HuelEk, "%.3lf",  prdk_k.bto_mat_o_b); 
    sprintf (SwEk,   "%.3lf",  prdk_k.nto_mat_o_b); 
    sprintf (VpkEk,  "%.3lf",  prdk_k.nvpk_mat_o_b);

// 250403 LuD A
	//=====VarbEK====
	if (prdk_k.varb_gew == 0.0)
    {
	    sprintf (VarbEk, "%.3lf",  prdk_k.varb_gew); 
    }
	else
    {
//		sprintf (VarbEk, "%.4lf",  work.Round (prdk_k.varb_mat_o_b * prdk_k.chg_gew / prdk_k.varb_gew,4)); 
		sprintf (VarbEk, "%.3lf",  work.Round (ratod (VarbWrtEk) / prdk_k.varb_gew,3)); 
    }
                  

	//=====ZutEK====
	if ((prdk_k.varb_gew + prdk_k.zut_gew) == 0.0)
    {
	    sprintf (ZutEk, "%.3lf",  0.0); 
    }
	else
    {
//		sprintf (ZutEk, "%.4lf",  work.Round (
//											  ((prdk_k.varb_mat_o_b * prdk_k.chg_gew) +   
//											  (prdk_k.zut_mat_o_b * prdk_k.chg_gew))   
//											  / 
//											   (prdk_k.varb_gew + prdk_k.zut_gew),4)); 
		sprintf (ZutEk, "%.3lf",  work.Round ((ratod(VarbWrtEk) + ratod(ZutWrtEk))
											  / 
											   (prdk_k.varb_gew + prdk_k.zut_gew),3)); 
    }
	//=====HuelEK====
	if ((prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew) == 0.0)
    {
	    sprintf (HuelEk, "%.3lf",  0.0); 
    }
	else
    {
//		sprintf (HuelEk, "%.4lf",  work.Round (
//											  ((prdk_k.varb_mat_o_b * prdk_k.chg_gew) +   
//											  (prdk_k.zut_mat_o_b * prdk_k.chg_gew) +   
//											  (prdk_k.huel_mat_o_b * prdk_k.chg_gew))   
//											  / 
//											   (prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew),4)); 
		sprintf (HuelEk, "%.3lf",  work.Round ((ratod(VarbWrtEk) + ratod(ZutWrtEk) + ratod(HuelWrtEk))
											  / 
										   (prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew),3)); 
    }

	//=====SwEK====
	if ((prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew) == 0.0)
    {
	    sprintf (SwEk, "%.3lf",  0.0); 
    }
	else
    {
/*
		sprintf (SwEk, "%.4lf",  work.Round (
											  ((prdk_k.varb_mat_o_b * prdk_k.chg_gew) +   
											  (prdk_k.zut_mat_o_b * prdk_k.chg_gew) +   
											  (prdk_k.huel_mat_o_b * prdk_k.chg_gew))   
											  / 
											   (prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew + 
											    (SwProz * (prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew) * -1) 
											   ),4)); 
*/
		sprintf (SwEk, "%.3lf",  work.Round ((ratod(VarbWrtEk) + ratod(ZutWrtEk) + ratod(HuelWrtEk))
											  / 
											   (prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew + 
											    (SwProz * (prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew) * -1) 
											   ),3)); 
    }

	//=====VpkEK====
	if ((prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew) == 0.0)
    {
	    sprintf (VpkEk, "%.3lf",  0.0); 
    }
	else
    {
/*
		sprintf (VpkEk, "%.4lf",  work.Round (
											  ((prdk_k.varb_mat_o_b * prdk_k.chg_gew) +   
											  (prdk_k.zut_mat_o_b * prdk_k.chg_gew) +   
											  (prdk_k.huel_mat_o_b * prdk_k.chg_gew) +
											   prdk_k.vpk_wrt )
											  / 
											   (prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew + 
											    (SwProz * (prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew) * -1) 
											   ),4)); 
*/
		sprintf (VpkEk, "%.3lf",  work.Round ((ratod(VarbWrtEk) + ratod(ZutWrtEk) + ratod(HuelWrtEk) +
											   prdk_k.vpk_wrt )
											  / 
											   (prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew + 
											    (SwProz * (prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew) * -1) 
											   ),3)); 

    }
//250403 LuD E

    prdk_k.kost_gew = 0.0;

    if (((prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew) *
         (1 - prdk_k.sw_kalk / 100) + prdk_k.vpk_gew) != 0.0)
    {
           prdk_k.kost_mat_o_b = ratod (AfsWrtEk) /  
                                 ((prdk_k.varb_gew +
                                  prdk_k.zut_gew +
                                  prdk_k.huel_gew) *
                                  (1 - prdk_k.sw_kalk / 100) +
                                  prdk_k.vpk_gew);

           prdk_k.kost_hk_vollk = ratod (AfsWrtBep) / 
                                  ((prdk_k.varb_gew +
                                   prdk_k.zut_gew +
                                   prdk_k.huel_gew) *
                                   (1 - prdk_k.sw_kalk / 100) +
                                   prdk_k.vpk_gew);
    }
    else
    {
            prdk_k.kost_mat_o_b  = 0.0;  
            prdk_k.kost_hk_vollk = 0.0; 
    }

//090507    double ArtGewNto = ((prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew) *
//                         (1 - prdk_k.sw_kalk / 100) + prdk_k.vpk_gew);
    double ArtGewNto = ((prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew) *
                         (1 - SwProz) + prdk_k.vpk_gew); //090507


    double ArtWrtBepPz  =  prdk_k.varb_hk_vollk  * prdk_k.varb_gew + 
                           prdk_k.zut_hk_vollk   * prdk_k.zut_gew +
                           prdk_k.huel_hk_wrt +
                           prdk_k.vpk_hk_wrt + 
                           ratod (ApzWrtBep);

    double ArtWrtEk  =  prdk_k.varb_mat_o_b * prdk_k.varb_gew + 
                        prdk_k.zut_mat_o_b * prdk_k.zut_gew +
                        prdk_k.huel_wrt +
                        prdk_k.vpk_wrt + 
                        ratod (AfsWrtEk);

    double ArtWrtBep  =  prdk_k.varb_hk_vollk  * prdk_k.varb_gew + 
                         prdk_k.zut_hk_vollk   * prdk_k.zut_gew +
                         prdk_k.huel_hk_wrt +
                         prdk_k.vpk_hk_wrt + 
                         ratod (ApzWrtBep) +
                         ratod (AfsWrtBep);


    if (ArtGewNto != 0)
    {
             sprintf (ApzEk,  "%.3lf",  SuWrtEkPz / ArtGewNto);
             sprintf (ApzBep, "%.3lf",  SuWrtBepPz / ArtGewNto);
		     if (GetTabArbeitszeit() == FALSE)  sprintf (ApzEk,  "%.3lf",  0.0);
		     if (GetTabArbeitszeit() == FALSE)  sprintf (ApzBep,  "%.3lf",  0.0);

             sprintf (AfsEk,  "%.3lf",  ratod (SuWrtEk) / ArtGewNto);
             sprintf (AfsBep,  "%.3lf", ratod (SuWrtBep) / ArtGewNto);
             sprintf (KgSK,  "%.3lf", (ratod (SuWrtBep)  + ratod (SKKost)) / ArtGewNto);
             sprintf (KgFEK,  "%.3lf", (ratod (SuWrtBep) + ratod (SKKost) + ratod (FEKKost)) / ArtGewNto);
             sprintf (KgFVK,  "%.3lf", (ratod (SuWrtBep) + ratod (SKKost) + ratod (FEKKost) + ratod (FVKKost)) / ArtGewNto);
    }
    else
    {
             sprintf (ApzEk,  "%.3lf",  0.0);
             sprintf (ApzBep,  "%.3lf", 0.0);
             sprintf (AfsEk,  "%.3lf",  0.0);
             sprintf (AfsBep,  "%.3lf", 0.0);
             sprintf (KgSK,  "%.3lf", 0.0);
             sprintf (KgFEK,  "%.3lf", 0.0);
             sprintf (KgFVK,  "%.3lf", 0.0);
    }

    sprintf (VarbBep, "%.3lf",  prdk_k.varb_hk_vollk); 
    sprintf (ZutBep , "%.3lf",  prdk_k.rez_hk_vollk); 
    sprintf (HuelBep, "%.3lf",  prdk_k.bto_hk_vollk); 
    sprintf (SwBep,   "%.3lf",  prdk_k.nto_hk_vollk); 
    sprintf (VpkBep,  "%.3lf",  prdk_k.nvpk_hk_vollk); 


// 250403 LuD A
	//=====VarbBep====
	if (prdk_k.varb_gew == 0.0)
    {
	    sprintf (VarbBep, "%.3lf",  prdk_k.varb_gew); 
    }
	else
    {
		sprintf (VarbBep, "%.3lf",  work.Round (ratod (VarbWrtBep) / prdk_k.varb_gew,4)); 
    }
                  

	//=====ZutBep====
	if ((prdk_k.varb_gew + prdk_k.zut_gew) == 0.0)
    {
	    sprintf (ZutBep, "%.3lf",  0.0); 
    }
	else
    {
		sprintf (ZutBep, "%.3lf",  work.Round ((ratod(VarbWrtBep) + ratod(ZutWrtBep))
											  / 
											   (prdk_k.varb_gew + prdk_k.zut_gew),3)); 
    }
    reportrez.bep = ratod(ZutBep);

	//=====HuelBep====
	if ((prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew) == 0.0)
    {
	    sprintf (HuelBep, "%.3lf",  0.0); 
    }
	else
    {
		sprintf (HuelBep, "%.3lf",  work.Round ((ratod(VarbWrtBep) + ratod(ZutWrtBep) + ratod(HuelWrtBep))
											  / 
											   (prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew),3)); 
    }

	//=====SwBep====
	if ((prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew) == 0.0)
    {
	    sprintf (SwBep, "%.3lf",  0.0); 
    }
	else
    {
		sprintf (SwBep, "%.3lf",  work.Round ((ratod(VarbWrtBep) + ratod(ZutWrtBep) + ratod(HuelWrtBep))
											  / 
											   (prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew + 
											    (SwProz * (prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew) * -1) 
											   ),3)); 
    }

	//=====VpkBep====
	if ((prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew) == 0.0)
    {
	    sprintf (VpkBep, "%.3lf",  0.0); 
    }
	else
    {
		sprintf (VpkBep, "%.3lf",  work.Round ((ratod(VarbWrtBep) + ratod(ZutWrtBep) + ratod(HuelWrtBep) +
											   prdk_k.vpk_hk_wrt )
											  / 
											   (prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew + 
											    (SwProz * (prdk_k.varb_gew + prdk_k.zut_gew + prdk_k.huel_gew) * -1) 
											   ),3)); 

    }
//250403 LuD E


//    sprintf (ZutDiff , "%.4lf",  prdk_k.rez_hk_vollk - prdk_k.varb_hk_vollk); 
//    sprintf (HuelDiff, "%.4lf",  prdk_k.bto_hk_vollk - prdk_k.rez_hk_vollk); 
//    sprintf (SwDiff,   "%.4lf",  prdk_k.nto_hk_vollk - prdk_k.bto_hk_vollk); 
    sprintf (ZutDiff , "%.3lf",  ratod(ZutBep) - ratod(VarbBep));  //250403 LuD
    sprintf (HuelDiff, "%.3lf",  ratod(HuelBep) - ratod(ZutBep));  //250403 LuD
    sprintf (SwDiff,   "%.3lf",  ratod(SwBep) - ratod(HuelBep)); 
    sprintf (VpkDiff,  "%.3lf",  ratod (VpkBep) - ratod (SwBep)); 
    sprintf (ApzDiff,  "%.3lf",  ratod (ApzBep) - ratod (VpkBep)); 
    sprintf (AfsDiff,  "%.3lf",  ratod (AfsBep) - ratod (ApzBep)); 

    sprintf (VarbKost, "%.3lf",   work.GetSuKostVarb ());
    sprintf (ZutKost,  "%.3lf",   work.GetSuKostZut ());
    sprintf (HuelKost, "%.3lf",   work.GetSuKostHuel ());
    sprintf (VpkKost,  "%.3lf",   work.GetSuKostVpk ());
    sprintf (SwKost,   "%.3lf",   work.GetSuKostSw ());
    sprintf (AfsKost,  "%.2lf",   work.GetSuKostHK ());
    sprintf (SKKost,   "%.3lf",   work.GetSuKostSK ());
    sprintf (FEKKost,   "%.3lf",   work.GetSuKostFEK ());
    sprintf (FVKKost,   "%.3lf",   work.GetSuKostFVK ());

    if (ratod (VarbMe) != 0.0)
    {
          sprintf (VarbKostE, "%.3lf",  ratod (VarbKost) / ratod (VarbMe));
    }
    else
    {
          sprintf (VarbKostE, "%.3lf",  0.0);
    }

    if ((ratod (VarbMe) + ratod (ZutMe))!= 0.0)
    {
          sprintf (ZutKostE,  "%.3lf",  ratod (ZutKost) / (ratod (VarbMe) +
                                                     ratod (ZutMe)));
    }
    else
    {
          sprintf (ZutKostE,  "%.3lf",  0.0);
    }
    if ((ratod (VarbMe) + ratod (ZutMe) + ratod (HuelMe))!= 0.0)
    {
          sprintf (HuelKostE, "%.3lf",  ratod (HuelKost) / (ratod (VarbMe) +
                                                       ratod (ZutMe) +
                                                       ratod (HuelMe)));
    }
    else
    {
          sprintf (HuelKostE, "%.3lf",  0.0);
    }
    if ((ratod (VarbMe) + ratod (ZutMe) + 
         ratod (HuelMe) + ratod (SwMe))!= 0.0)
    {
           sprintf (SwKostE,   "%.3lf",  ratod (SwKost) / (ratod (VarbMe) +
                                                       ratod (ZutMe) +
                                                       ratod (HuelMe) +
                                                       ratod (SwMe)));
    }
    else
    {
           sprintf (SwKostE,   "%.3lf",  0.0);
    }

    if ((ratod (VarbMe) + ratod (ZutMe) + 
         ratod (HuelMe) + ratod (SwMe) +
         ratod (VpkMe)) != 0.0)
    {
          sprintf (VpkKostE,  "%.3lf",  ratod (VpkKost) / (ratod (VarbMe) +
                                                      ratod (ZutMe) +
                                                      ratod (HuelMe) +
                                                      ratod (SwMe) + 
                                                      ratod (VpkMe)));
          sprintf (AfsKostE,  "%.3lf",  ratod (AfsKost) / (ratod (VarbMe) +
                                                      ratod (ZutMe) +
                                                      ratod (HuelMe) +
                                                      ratod (SwMe) + 
                                                      ratod (VpkMe)));
          sprintf (SKKostE,  "%.3lf",  ratod (SKKost) / (ratod (VarbMe) +
                                                      ratod (ZutMe) +
                                                      ratod (HuelMe) +
                                                      ratod (SwMe) + 
                                                      ratod (VpkMe)));
          sprintf (FEKKostE,  "%.3lf",  ratod (FEKKost) / (ratod (VarbMe) +
                                                      ratod (ZutMe) +
                                                      ratod (HuelMe) +
                                                      ratod (SwMe) + 
                                                      ratod (VpkMe)));
          sprintf (FVKKostE,  "%.3lf",  ratod (FVKKost) / (ratod (VarbMe) +
                                                      ratod (ZutMe) +
                                                      ratod (HuelMe) +
                                                      ratod (SwMe) + 
                                                      ratod (VpkMe)));
    }
    else
    {
          sprintf (VpkKostE,  "%.3lf",  0.0);
          sprintf (AfsKostE,  "%.3lf",  0.0);
          sprintf (SKKostE,   "%.3lf",  0.0);
          sprintf (FEKKostE,  "%.3lf",  0.0);
          sprintf (FVKKostE,  "%.3lf",  0.0);
    }


    sprintf (SuKost,    "%.3lf",  ratod (VarbKost) + 
                                  ratod (ZutKost) + 
                                  ratod (HuelKost) + 
                                  ratod (SwKost) + 
                                  ratod (VpkKost) +
                                  ratod (AfsKost));

    sprintf (SuKostE,    "%.3lf",  ratod (VarbKostE) + 
                                   ratod (ZutKostE) + 
                                   ratod (HuelKostE) + 
                                   ratod (SwKostE) + 
                                   ratod (VpkKostE) +
                                   ratod (AfsKostE));

    double stkek  = ratod (AfsEk);
    double stkbep = ratod (AfsBep);
    double stksk = ratod (KgSK);
    double stkfek = ratod (KgFEK);
    double stkfvk = ratod (KgFVK);
    double stkbepges = 0.0;
    double stkekges = 0.0;

    if (ArtGewNto != 0.0)
    {
          stkbepges = ratod (SuBep) + 
                             ratod (SuKost) /
                             ArtGewNto;
          stkekges = ratod (SuEk);
    }

//    prdk_k.a_hk_vollk = stkbepges;
    prdk_k.a_hk_vollk = ratod (SuBep); //LuD 130503 ich brauchs erst mal ohne Kosten
										//sp�ter neues Feld in prdk_k
    prdk_k.a_hk_teilk = stkekges;
    prdk_k.a_sk_vollk = prdk_k.a_hk_vollk + ratod(SKKostE);
    prdk_k.a_filek_vollk = prdk_k.a_sk_vollk + ratod(FEKKostE);
    prdk_k.a_filvk_vollk = prdk_k.a_filek_vollk + ratod(FVKKostE);

    double a_gew = work.GetRezAGew ();
    stkek  *= a_gew;
    stkbep *= a_gew;
    stkbepges *= a_gew;
    stksk *= a_gew;
    stkfek *= a_gew;
    stkfvk *= a_gew;

    sprintf (AGew,      "%.3lf", a_gew);
    sprintf (StkEk,     "%.3lf", stkek);
    sprintf (StkBep,    "%.3lf", stkbep);
    sprintf (StkBepGes, "%.3lf", stkbepges);
    sprintf (StkSK,     "%.3lf", stksk);
    sprintf (StkFEK,     "%.3lf", stkfek);
    sprintf (StkFVK,     "%.3lf", stkfvk);

//    if (NewCalc == FALSE)
    {
             fKost.SetText (); 
             fKostSu.SetText (); 
    }
}


BOOL SpezDlg::OnTcnSelChange (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
         if (fHead.GetCfield ("a")->IsDisabled () == FALSE)
         {
               TabCtrl_SetCurSel (GetParent (this->hWnd), 0);
               iPage = 0;
               return TRUE;
         }
         if (LockPage || DlgCenterX || DlgCenterY)
         {
              LockWindowUpdate (hWnd);
         }
         int oldPage = iPage;
         fVarbFoot.SetText ();

		 if (oldPage == TabKopf)
         {
					FromForm (dbfields);   //#150803
			        if (strcmp (prdk_kf.akv, "J") == 0)
					{
						prdk_k.akv = 1;
					}
						else
					{
						prdk_k.akv = 0;
					}
			        if (strcmp (prdk_kf.rework, "J") == 0)
					{
						prdk_k.rework = 1;
					}
					else
					{
						prdk_k.rework = 0;
					}
					setWindowText (1);
//                    CalcChg();
         }
		 else if (oldPage == TabVarb)
		 {
                   if (VarbLst != NULL)
                   {
                       VarbLst->StopList ();
                       VarbLst->DestroyWindows ();
                       delete VarbLst;
                       VarbLst = NULL;
                   }

				   EnableAnteil(FALSE);
                   fPos1.GetText (); 
                   fPos1.destroy (); 
                   fVarbFoot.destroy (); 
                   sprintf (prdk_kf.chg_gew, "%8.3lf", prdk_k.chg_gew);
		 }
		 else if (oldPage == TabZut)
		 {
                   if (ZutLst != NULL)
                   {
                       ZutLst->StopList ();
                       fVarbFoot.destroy (); 
                       ZutLst->DestroyWindows ();
                       delete ZutLst;
                       ZutLst = NULL;
                   }
                   fPos1.GetText (); 
                   fPos1.destroy (); 
                   fVarbFoot.destroy (); 
                   sprintf (prdk_kf.chg_gew, "%8.3lf", prdk_k.chg_gew);
		 }
		 else if (oldPage == TabHuell)
		 {
                   if (HuelLst != NULL)
                   {
                       HuelLst->StopList ();
                       fVarbFoot.destroy (); 
                       HuelLst->DestroyWindows ();
                       delete HuelLst;
                       HuelLst = NULL;
                   }
                   fPos1.GetText (); 
                   fPos1.destroy (); 
                   fVarbFoot.destroy (); 
		 }
		 else if (oldPage == TabVerpack)
		 {
                   if (VpkLst != NULL)
                   {
                       VpkLst->StopList ();
                       fVarbFoot.destroy (); 
                       VpkLst->DestroyWindows ();
                       delete VpkLst;
                       VpkLst = NULL;
                   }
                   fPos1.GetText (); 
                   fPos1.destroy (); 
                   fVarbFoot.destroy (); 
		 }
		 else if (oldPage == TabArbZeit)
		 {
                   if (ZeitLst != NULL)
                   {
                       ZeitLst->StopList ();
//                       fVarbFoot.destroy (); 
                       ZeitLst->DestroyWindows ();
                       delete ZeitLst;
                       ZeitLst = NULL;
                   }
                   fPosZeit.GetText (); 
                   fPosZeit.destroy (); 
                   fVarbFoot.destroy (); 
		 }
		 else if (oldPage == TabKost)
		 {
                   if (KostLst != NULL)
                   {
                       KostLst->StopList ();
                       KostLst->DestroyWindows ();
                       delete KostLst;
                       KostLst = NULL;
                   }
                   fKosten.GetText (); 
                   fKosten.destroy (); 
		 }
		 else if (oldPage == TabQuid)
		 {
                   if (QuidLst != NULL)
                   {
                       QuidLst->StopList ();
                       QuidLst->DestroyWindows ();
                       delete QuidLst;
                       QuidLst = NULL;
                   }
                   fPosQuid.GetText (); 
                   fPosQuid.destroy (); 
                   fVarbFoot.destroy (); 
		 }


         iPage = TabCtrl_GetCurSel(GetParent (this->hWnd));

		 if (iPage == TabKopf)
         {
               strcpy (preiskg, "0");
               strcpy (gewsumme, "0");
               strcpy (prdk_kf.pageinfo,  "Kopfdaten");
               fHead.SetText ();
               SetDialog (&fPrdk_k0);
//               if (DlgCenterX || DlgCenterY)
               {
                   OnMove (hMainWindow, 0, 0, 0l);
               }
               InValidate ();
               NewTabStops (fWork);
//               ToForm (dbfields);
               fPos.SetText ();
               SetCurrentName ("rez_bz");
         }
		 else if (iPage == TabVarb)
		 {
               strcpy (preiskg, "0");
               strcpy (gewsumme, "0");
               strcpy (prdk_kf.pageinfo,  "Verarbeitungsmaterial");
               fHead.SetText ();
               iPage = 0;
               fPos.GetText (); 
               FromForm (dbfields);
               fPos.destroy (); 
               iPage = TabVarb;
//			   fPrdk_k01.destroy ();
		       if (GetFlgAnteil ())
			   {
					EnableAnteil(TRUE);
			        SetFlgAnteil (FALSE);
			   }
               SetDialog (&fPrdk_k01);
               if (DlgCenterX || DlgCenterY)
               {
                   OnMove (hMainWindow, 0, 0, 0l);
               }
               fVarbFoot.MoveToFoot (this->hWnd);
               InValidate ();
               NewTabStops (fWork);
               fPos1.SetText ();
               if (VarbLst == NULL)
               {
                      VarbLst = new VARBLST ();
               }
               EnterVarbLst ();
		 }
		 else if (iPage == TabZut)
		 {
               strcpy (preiskg, "0");
               strcpy (gewsumme, "0");
               strcpy (prdk_kf.pageinfo,  "Zutaten");
               fHead.SetText ();
               iPage = 0;
               fPos.GetText (); 
               FromForm (dbfields);
               fPos.destroy (); 
               iPage = TabZut;
               SetDialog (&fPrdk_k01);
               if (DlgCenterX || DlgCenterY)
               {
                   OnMove (hMainWindow, 0, 0, 0l);
               }
               fVarbFoot.MoveToFoot (this->hWnd);
               InValidate ();
               NewTabStops (fWork);
               fPos1.SetText ();
               if (ZutLst == NULL)
               {
                      ZutLst = new ZUTLST ();
               }
			   if (prdk_k.summegew_FF + prdk_k.summegew_EL + prdk_k.summegew_GE + prdk_k.summegew_KZ+ prdk_k.summegew_EL1 + prdk_k.summegew_EL2 == 0.0)
			   {
				 work.CalcSumVarbBatch();
			   }

               EnterZutLst ();
		 }
		 else if (iPage == TabHuell)
		 {
               strcpy (preiskg, "0");
               strcpy (gewsumme, "0");
               strcpy (prdk_kf.pageinfo,  "H�llen");
               fHead.SetText ();
               iPage = 0;
               fPos.GetText (); 
               FromForm (dbfields);
               fPos.destroy (); 
               iPage = TabHuell;
               SetDialog (&fPrdk_k01);
               if (DlgCenterX || DlgCenterY)
               {
                   OnMove (hMainWindow, 0, 0, 0l);
               }
               fVarbFoot.MoveToFoot (this->hWnd);
               InValidate ();
               NewTabStops (fWork);
               fPos1.SetText ();
               if (HuelLst == NULL)
               {
                      HuelLst = new HUELLST ();
               }
               EnterHuelLst ();
		 }
		 else if (iPage == TabVerpack)
		 {
               strcpy (preiskg, "0");
               strcpy (gewsumme, "0");
               strcpy (prdk_kf.pageinfo,  "Verpackung");
               fHead.SetText ();
               iPage = 0;
               fPos.GetText (); 
               FromForm (dbfields);
               fPos.destroy (); 
               iPage = TabVerpack;
               SetDialog (&fPrdk_k01);
               if (DlgCenterX || DlgCenterY)
               {
                   OnMove (hMainWindow, 0, 0, 0l);
               }
               fVarbFoot.MoveToFoot (this->hWnd);
               InValidate ();
               NewTabStops (fWork);
               fPos1.SetText ();
               if (VpkLst == NULL)
               {
                      VpkLst = new VPKLST ();
               }
               EnterVpkLst ();
		 }
		 else if (iPage == TabArbZeit)
		 {
               strcpy (preiskg, "0");
               strcpy (gewsumme, "0");
               strcpy (prdk_kf.pageinfo,  "Arbeitszeitwerte");
               fHead.SetText ();
               iPage = 0;
               fPos.GetText (); 
               FromForm (dbfields);
               fPos.destroy (); 
               iPage = TabArbZeit;
               SetDialog (&fPrdk_k0Zeit);
               if (DlgCenterX || DlgCenterY)
               {
                   OnMove (hMainWindow, 0, 0, 0l);
               }
               Enable (&fPrdk_kZeit, EnableHead, FALSE); 
               Enable (&fPrdk_kZeit, EnablePosZeit,TRUE);
               fVarbFoot.MoveToFoot (this->hWnd);
               InValidate ();
               NewTabStops (fWork);
               fPosZeit.SetText ();
               strcpy (FlgBasis, "J");
               strcpy (FlgKalk, "N");
	  		   SaveChg = ratod (prdk_kf.zeit_c1);
		       SaveMe = ratod (prdk_kf.zeit_m1);
       	       CalcfromChg ();
		       CalcfromMe ();

               if (ZeitLst == NULL)
               {
                      ZeitLst = new ZEITLST ();
               }
               EnterZeitLst ();
			   fPosZeit.GetCfield ("zeit_c1")->SetFocus ();
		 }
		 else if (iPage == TabKost)
		 {
				CalcNewKost(); //210807 
               fHead.destroy ();  
               strcpy (preiskg, "0");
               strcpy (gewsumme, "0");
//               strcpy (prdk_kf.pageinfo,  "Kosten");
//               fHead.SetText ();
               iPage = 0;
               fPos.GetText (); 
               FromForm (dbfields); 
               fPos.destroy (); 
               iPage = TabKost;
               SetDialog (&fKosten0);
               if (DlgCenterX || DlgCenterY)
               {
                   OnMove (hMainWindow, 0, 0, 0l);
               }
               Enable (&fKosten, EnableHead, FALSE); 
               Enable (&fKosten, EnableKost,TRUE);
               fKostSu.MoveToFoot (this->hWnd);
//               SetDialog (&fKosten);
               InValidate ();
               NewTabStops (fWork);   //200505<----- Hier liegt das Problem wird hier nicht gesetzt , TabStoplen = 0!!!!
               fKosten.SetText ();

               if (KostLst == NULL)
               {
                      KostLst = new KOSTLST ();
               }
			   if (GetTabZutaten() == FALSE)
			   {
					fKost.GetCfield ("kost_zut_txt")->SetAttribut (CREMOVED);
					fKost.GetCfield ("zut_me")->SetAttribut (CREMOVED);
					fKost.GetCfield ("zut_wrt_ek")->SetAttribut (CREMOVED);
					fKost.GetCfield ("zut_wrt_bep")->SetAttribut (CREMOVED);
					fKost.GetCfield ("zut_ek")->SetAttribut (CREMOVED);
					fKost.GetCfield ("zut_bep")->SetAttribut (CREMOVED);
					fKost.GetCfield ("zut_diff")->SetAttribut (CREMOVED);
					fKost.GetCfield ("zut_kost")->SetAttribut (CREMOVED);
					fKost.GetCfield ("zut_koste")->SetAttribut (CREMOVED);
			   }
			   if (GetTabVerpackung() == FALSE)
			   {
					fKost.GetCfield ("kost_vpk_txt")->SetAttribut (CREMOVED);
					fKost.GetCfield ("vpk_wrt_ek")->SetAttribut (CREMOVED);
					fKost.GetCfield ("vpk_wrt_bep")->SetAttribut (CREMOVED);
					fKost.GetCfield ("vpk_ek")->SetAttribut (CREMOVED);
					fKost.GetCfield ("vpk_bep")->SetAttribut (CREMOVED);
					fKost.GetCfield ("vpk_diff")->SetAttribut (CREMOVED);
					fKost.GetCfield ("vpk_kost")->SetAttribut (CREMOVED);
					fKost.GetCfield ("vpk_koste")->SetAttribut (CREMOVED);
			   }

			   if (GetTabArbeitszeit() == FALSE)
			   {
					fKost.GetCfield ("apz_txt")->SetAttribut (CREMOVED);
					fKost.GetCfield ("apz_wrt_bep")->SetAttribut (CREMOVED);
					fKost.GetCfield ("apz_ek")->SetAttribut (CREMOVED);
					fKost.GetCfield ("apz_bep")->SetAttribut (CREMOVED);
					fKost.GetCfield ("apz_diff")->SetAttribut (CREMOVED);
			   }

//               if (LockPage == FALSE)
               {
				   /** 010808 wenns drin war, erschien Tabseiten in FETT
                     OnMove (hMainWindow, 0, 0, 0l);
                     LockWindowUpdate (NULL);
                     InValidate ();
					 **/
               }
               EnterKostLst ();
			   fKost.GetCfield ("bearb_hk")->SetFocus ();

		 }

		 else if (iPage == TabQuid)
		 {
				work.FuelleQuid (prdk_k.rez,prdk_k.variante,1,prdk_k.chg_gew,1.0,0.0);
				work.RechneQuid ();
				if (strlen (prdk_kf.hinweis2) < 2)
				{
					fPosQuid.GetCfield ("quid_hinweis2")->SetAttribut (CREMOVED);
				}
				else
				{
					fPosQuid.GetCfield ("quid_hinweis2")->SetAttribut (CREADONLY);
				}
				if (strlen (prdk_kf.warnung1) < 2)
				{
					fPosQuid.GetCfield ("warnung1")->SetAttribut (CREMOVED);
				}
				else
				{
					fPosQuid.GetCfield ("warnung1")->SetAttribut (CREADONLY);
				}
				if (strlen (prdk_kf.warnung2) < 2)
				{
					fPosQuid.GetCfield ("warnung2")->SetAttribut (CREMOVED);
				}
				else
				{
					fPosQuid.GetCfield ("warnung2")->SetAttribut (CREADONLY);
				}
               strcpy (prdk_kf.pageinfo,  "Quid - Info");
               fHead.SetText ();
               iPage = 0;
               fPos.GetText (); 
               FromForm (dbfields);
               fPos.destroy (); 
               iPage = TabQuid;
               SetDialog (&fPrdk_k0Quid);
               if (DlgCenterX || DlgCenterY)
               {
                   OnMove (hMainWindow, 0, 0, 0l);
               }
               Enable (&fPrdk_kQuid, EnableHead, FALSE); 
               Enable (&fPrdk_kQuid, EnablePosZeit,TRUE);
               fVarbFoot.MoveToFoot (this->hWnd);
               InValidate ();
               NewTabStops (fWork);
               fPosQuid.SetText ();
               if (QuidLst == NULL)
               {
                      QuidLst = new QUIDLST ();
               }
               EnterQuidLst ();
//			   fPosQuid.GetCfield ("zeit_c1")->SetFocus ();

		 }
			 
         if (LockPage)
         {
              LockWindowUpdate (NULL);
		 }

//		 work.SetChgFaktor(1);
         return TRUE;
}

BOOL SpezDlg::OnSysCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        
        if (wParam == SC_CLOSE)
        {
            if (abfragejn (hWnd, "Verarbeitung abbrechen ?", "N") == 0)
            {
                ActiveDlg->SetCurrentFocus ();
                return TRUE;
            }
            syskey = KEY5;
        }
        return DLG::OnSysCommand (hWnd, msg, wParam, lParam); 
}

BOOL SpezDlg::OnMove (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (hWnd != this->hMainWindow) return FALSE; 
        DLG::OnMove (hWnd,msg,wParam,lParam); 
        if (VarbLst != NULL)
        {
            VarbLst->MoveMamain1 ();
        }
        else if (ZutLst != NULL)
        {
            ZutLst->MoveMamain1 ();
        }
        else if (HuelLst != NULL)
        {
            HuelLst->MoveMamain1 ();
        }
        else if (VpkLst != NULL)
        {
            VpkLst->MoveMamain1 ();
        }
        else if (KostLst != NULL)
        {
            KostLst->MoveMamain1 ();
        }
        else if (ZeitLst != NULL)
        {
            ZeitLst->MoveMamain1 ();
        }
        return FALSE;
}
             

BOOL SpezDlg::OnDestroy (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{

        if (hWnd == this->hWnd)
        {
            if (CloseProg != NULL)
            {
                   (*CloseProg) ();
            }
//             fPrdk_k.RemoveCfield ("partnerlst");
             PostQuitMessage (0);
             fPrdk_k.destroy ();
        }
        return TRUE;
}

/*
void SpezDlg::SetKun (short mdn, short fil, long kun)
{
         sprintf (prdk_kf.mdn, "%hd", mdn);
         if (atoi (prdk_kf.mdn) == 0)
         {
             sprintf (prdk_kf.fil, "%4d", 0);
             work.GetFilName (prdk_kf.fil_krz,   atoi (prdk_kf.mdn), atoi (prdk_kf.fil));
             Enable (&fPrdk_k, EnableHeadFil, FALSE); 
         }
         else if (work.GetMdnName (prdk_kf.mdn_krz,   atoi (prdk_kf.mdn)) != 0)
         {
             strcpy (prdk_kf.mdn, "0");
             return;
         }

         if (atoi (prdk_kf.mdn) != 0)
         {
             sprintf (prdk_kf.fil, "%hd", fil);
             work.GetFilName (prdk_kf.fil_krz, atoi (prdk_kf.mdn), atoi (prdk_kf.fil));
         }
         sprintf (prdk_kf.kun, "%ld", kun);
         fHead.SetText ();
}
*/


/*
void SpezDlg::ProcessMessages (void)
{
        if (atol (prdk_kf.kun) > 0)
        {
            syskey = KEYCR; 
            if (ReadKun ())
            {
                    EnterPos ();
            }
        }
        DLG::ProcessMessages ();
}
*/

void SpezDlg::SetWinBackground (COLORREF Col)
{
          dlgfont.FontBkColor = Col;
          cpfont.FontBkColor = Col;
          DLG::SetWinBackground (Col);
}

void SpezDlg::Kopieren (void)
{
	 if (abfragejn (hWnd, "Zum Kopieren Artikelnummer,Rezeptur und Variante eingeben ", "N"))
	 {
//             Enable (&fPrdk_k, EnableHead, TRUE); 
//             Enable (&fPrdk_k, EnablePos,  FALSE);
	   syskey = -1; //um Restart zu unterdr�cken
	   if (OnKey12())
	   {	
			Ursprung_mdn = atoi(prdk_kf.mdn);
			Ursprung_a = ratod(prdk_kf.a);
			strcpy(Ursprung_rez,prdk_kf.rez);
			Ursprung_variante = atoi(prdk_kf.variante);
	   }
	 }


}
void SpezDlg::ZeitToClipboard (void)
{
          HGLOBAL hb;
          LPVOID p;
	      Text cText;

		  ZeitLst->StopList ();  // Postiionen schreiben.
		  cText.Format ("PRDK_ZEIT\nMandant=%hd\nArtikel=%.0lf",prdk_k.mdn, prdk_k.a);
          char *text = cText.GetBuffer ();
 
	      hb = GlobalAlloc (GHND, strlen (text) + 1);
          if (hb == NULL) return;
          p = GlobalLock (hb);
          strcpy ((char *) p, text);
          GlobalUnlock (hb);
          OpenClipboard (hWnd);
          EmptyClipboard ();
          SetClipboardData (CF_TEXT, hb);
          CloseClipboard ();
}


void SpezDlg::ToClipboard (void)
{
          CFIELD *Cfield;
          HGLOBAL hb;
          LPVOID p;
          char *text;

		  if (ZeitLst != NULL)
		  {
			  ZeitToClipboard ();
			  return;
		  }
          Cfield = GetCurrentCfield ();
          Cfield->GetText ();
          text = (char *) Cfield->GetFeld ();
          if (text == NULL) return;
          hb = GlobalAlloc (GHND, strlen (text) + 1);
          if (hb == NULL) return;
          p = GlobalLock (hb);
          strcpy ((char *) p, text);
          GlobalUnlock (hb);
          OpenClipboard (hWnd);
          EmptyClipboard ();
          SetClipboardData (CF_TEXT, hb);
          CloseClipboard ();
}

BOOL SpezDlg::IsZeitCopy (char *text)
{
           Token rows;
		   Token items;

		   if (ZeitLst == NULL)
		   {
			   return FALSE;
		   }
		   rows.SetSep ("\n");
		   rows = text;
		   if (rows.GetAnzToken () < 2)
		   {
			   return FALSE;
		   }
		   Text cText = rows.NextToken ();
		   if (cText != "PRDK_ZEIT")
		   {
			   return FALSE;
		   }
		   cText = rows.NextToken ();

           items.SetSep ("=");
		   items = cText;
		   if (items.GetAnzToken () < 2)
		   {
			   return FALSE;
		   }
		   cText = items.NextToken ();
		   if (cText != "Mandant")
		   {
			   return FALSE;
		   }
		   short mdn = (short) atoi (items.NextToken ());

		   cText = rows.NextToken ();

		   items = cText;
		   if (items.GetAnzToken () < 2)
		   {
			   return FALSE;
		   }
		   cText = items.NextToken ();
		   if (cText != "Artikel")
		   {
			   return FALSE;
		   }
		   double a = ratod (items.NextToken ());
		   cText.Format ("Zeitwerte von Artikel %.0lf kopieren ?", a);
		   HWND ctrl = GetFocus ();
	       if (abfragejn (hWnd, cText.GetBuffer (), "J"))
		   {
			   work.CopyZeit (mdn, a);
			   ZeitLst->ShowDB ();
		   }
		   SetFocus (ctrl);
		   return TRUE;
}
    

void SpezDlg::FromClipboard (void)
{
          CFIELD *Cfield;
          HGLOBAL hb;
          LPVOID p;
          char *text;

          Cfield = GetCurrentCfield ();
          text = (char *) Cfield->GetFeld ();
          if (text == NULL) return;
          if (IsClipboardFormatAvailable (CF_TEXT) == FALSE) return;
          OpenClipboard (hWnd);
          hb = GetClipboardData (CF_TEXT);
          if (hb == NULL)
          {
                 CloseClipboard ();
                 return;
          }
          p = GlobalLock (hb);
/*
          if (strlen ((char *) p) > (unsigned int ) Cfield->GetLength ())
          {
                 GlobalUnlock (hb);
                 CloseClipboard ();
                 return;
          }
*/
          strcpy (text, (char *) p);
          GlobalUnlock (hb);
          CloseClipboard ();
		  if (IsZeitCopy (text))
		  {
			  return;
		  }
          if (strlen (text) <= (unsigned int ) Cfield->GetLength ())
          {
              Cfield->SetText ();
		  }
}

void SpezDlg::Help (void)
{
          CFIELD *Cfield;
          HELP *Help; 
          char *Item;

          Cfield = GetCurrentCfield ();
          if (Cfield == NULL) return;

          Item = Cfield->GetName ();

          Help = new HELP (HelpName, Item, hInstance,hMainWindow);
          delete Help;
          Cfield->SetFocus ();
}

/*
HWND SpezDlg::OpenWindow (HINSTANCE hInstance, HWND hMainWindow)
{
          HWND hWnd;

          Settchar ('|');
          hWnd = DLG::OpenWindow (hInstance, hMainWindow);
          FillListCaption ();
          return hWnd;
}

HWND SpezDlg::OpenTabWindow (HINSTANCE hInstance, HWND hMainWindow, char *TxtTab)
{
          HWND hWnd;

          Settchar ('|');
          hWnd = DLG::OpenTabWindow (hInstance, hMainWindow, TxtTab);
          FillListCaption ();
          return hWnd;
}

HWND SpezDlg::OpenScrollWindow (HINSTANCE hInstance, HWND hMainWindow)
{
          HWND hWnd;

          Settchar ('|');
          hWnd = DLG::OpenScrollWindow (hInstance, hMainWindow);
          FillListCaption ();
          return hWnd;
}
*/

HWND SpezDlg::OpenScrollTabWindow (HINSTANCE hInstance, HWND hMainWindow, char *TxtTab)
{
          HWND hWnd;
          TEXTMETRIC tm;
          HDC hdc;

//          Settchar ('|');
          LockWindowUpdate (hMainWindow);
          hWnd = DLG::OpenScrollTabWindow (hInstance, hMainWindow, TxtTab);
          OnMove (hMainWindow, 0, 0, 0l);

          hdc = GetDC (hWndTab);
          tabFont = SetDeviceFont (hdc, &dlgfont, &tm);
          oldfont = SelectObject (hdc, tabFont);
          ReleaseDC (hWndTab, hdc);
          SendMessage (hWndTab, WM_SETFONT, (WPARAM) tabFont, (LPARAM) NULL);
          LockWindowUpdate (NULL);
          return hWnd;
}


void SpezDlg::PrintComment (char *Name)
{
          HELP *Help; 
          char Comment [256];

          Help = new HELP (HelpName, Name, hInstance,hMainWindow, INITCOMM);
          Help->GetItem (Name, Comment);
          PrintMess (Comment);
          delete Help;
}

void SpezDlg::CallInfo (void)
{
  	      char value [256];

          if (VarbLst != 0)
          {
              VarbLst->CallInfoEx ();
              return;
          }
          if (ZutLst != 0)
          {
              ZutLst->CallInfoEx ();
              return;
          }
          if (HuelLst != 0)
          {
              HuelLst->CallInfoEx ();
              return;
          }
          if (VpkLst != 0)
          {
              VpkLst->CallInfoEx ();
              return;
          }
          if (ZeitLst != 0)
          {
              ZeitLst->CallInfoEx ();
              return;
          }
          if (QuidLst != 0)
          {
              QuidLst->CallInfoEx ();
              return;
          }
          fHead.GetText ();  
          if (ratod (prdk_kf.a) == 0.0)
          {
              disp_mess ("Die Artikelnummer ist 0", 2);
               return;
//               DLG::CallInfo ();
          }
          sprintf (value, "where a = %.0lf", ratod (prdk_kf.a));

          _CallInfoEx (hWndMain, NULL, "a", value, 0l); 
          return;
}



bool SpezDlg::ArtikelZugewiesen (void)
{
        return (fHead.GetCfield ("a")->IsDisabled () == TRUE);
}

void SpezDlg::PrintRez (void)
{
    strcpy (PrintCommand, "rezepturdruck");
    if (sys_ben.zahl == 99)
	{
		strcat(PrintCommand, " -mdn ");
		strcat(PrintCommand, prdk_kf.mdn);
		if (Artikelauswahl == 1)
        {
			if (instring(PrintCommand, "rezepturdruck") != -1)
			{
				strcat(PrintCommand, " -avon ");
				strcat(PrintCommand, prdk_kf.a_von);
				strcat(PrintCommand, " -abis ");
				strcat(PrintCommand, prdk_kf.a_bis);
		        if (strcmp (prdk_kf.akv, "J") == 0 && strcmp (prdk_kf.nakv, "N") == 0)
                {
					strcat(PrintCommand, " -akv "); //nur aktive Varianten
				}
		        if (strcmp (prdk_kf.akv, "N") == 0 && strcmp (prdk_kf.nakv, "J") == 0)
                {
					strcat(PrintCommand, " -nakv "); //nur nicht aktive Varianten
				}
			}
			ProcExec (PrintCommand, SW_SHOWNORMAL, -1, 0, -1, 0);  
        }
		if (Artikelauswahl == 0)
        {
			if (instring(PrintCommand, "rezepturdruck") != -1)
			{
				strcat(PrintCommand, " -agvon ");
				strcat(PrintCommand, prdk_kf.ag_von);
				strcat(PrintCommand, " -agbis ");
				strcat(PrintCommand, prdk_kf.ag_bis);
		        if (strcmp (prdk_kf.akv, "J") == 0 && strcmp (prdk_kf.nakv, "N") == 0)
                {
					strcat(PrintCommand, " -akv "); //nur aktive Varianten
				}
		        if (strcmp (prdk_kf.akv, "N") == 0 && strcmp (prdk_kf.nakv, "J") == 0)
                {
					strcat(PrintCommand, " -nakv "); //nur nicht aktive Varianten
				}
			}
			ProcExec (PrintCommand, SW_SHOWNORMAL, -1, 0, -1, 0);  
        }
		if (Artikelauswahl == 2)
        {
			if (instring(PrintCommand, "rezepturdruck") != -1)
			{
				strcat(PrintCommand, " -avon ");
				strcat(PrintCommand, prdk_kf.a_von);
				strcat(PrintCommand, " -abis ");
				strcat(PrintCommand, prdk_kf.a_bis);

				strcat(PrintCommand, " -agvon ");
				strcat(PrintCommand, prdk_kf.ag_von);
				strcat(PrintCommand, " -agbis ");
				strcat(PrintCommand, prdk_kf.ag_bis);
		        if (strcmp (prdk_kf.akv, "J") == 0 && strcmp (prdk_kf.nakv, "N") == 0)
                {
					strcat(PrintCommand, " -akv "); //nur aktive Varianten
				}
		        if (strcmp (prdk_kf.akv, "N") == 0 && strcmp (prdk_kf.nakv, "J") == 0)
                {
					strcat(PrintCommand, " -nakv "); //nur nicht aktive Varianten
				}
			}
			ProcExec (PrintCommand, SW_SHOWNORMAL, -1, 0, -1, 0);  
        }
	}
    else if (ArtikelZugewiesen()) 
	{
	   syskey = -1; //um Restart zu unterdr�cken
	   if (OnKey12())
	   {	
			if (instring(PrintCommand, "rezepturdruck") != -1)
			{
				strcat(PrintCommand, " -mdn ");
				strcat(PrintCommand, prdk_kf.mdn);
				strcat(PrintCommand, " -avon ");
				strcat(PrintCommand, prdk_kf.a);
				strcat(PrintCommand, " -abis ");
				strcat(PrintCommand, prdk_kf.a);
				strcat(PrintCommand, " -variante ");
				strcat(PrintCommand, prdk_kf.variante);

				if (sys_ben.berecht > 1 && (sys_ben.berecht < 4))
                {
					strcat(PrintCommand, " -ohnePreis ");
					strcat(PrintCommand, "1");
                }

			}
			ProcWaitExec (PrintCommand, SW_SHOWNORMAL, -1, 0, -1, 0);  
		    strcpy (PrintCommand, "rezepturdruck");
				if (work.GetGBVorhanden() == TRUE)
				{
				 if (abfragejn (hWnd, "Grundbr�te einzeln mit ausdrucken ?", "N"))
				 {
					strcat(PrintCommand, " -mdn ");
					strcat(PrintCommand, prdk_kf.mdn);
					strcat(PrintCommand, " -avon ");
					strcat(PrintCommand, prdk_kf.a);
					strcat(PrintCommand, " -abis ");
					strcat(PrintCommand, prdk_kf.a);
					strcat(PrintCommand, " -variante ");
					strcat(PrintCommand, prdk_kf.variante);
					strcat(PrintCommand, " -mitGB 1 "); //210807 
					if (sys_ben.berecht > 1 && (sys_ben.berecht < 4))
		            {
						strcat(PrintCommand, " -ohnePreis ");
						strcat(PrintCommand, "1");
					}
	   			    ProcExec (PrintCommand, SW_SHOWNORMAL, -1, 0, -1, 0);  
				 }
				}


	   }
	}

}

void SpezDlg::SetTabStack (short pos1, short pos2, short pos3, short pos4, short pos5, short pos6, short pos7)
{

if (pos1 != TabVarb && pos2 != TabVarb && pos3 
		!= TabVarb && pos4 != TabVarb && pos5 != TabVarb && pos6 != TabVarb  && pos7 != TabVarb) TabVarb = 99;
if (pos1 == TabVarb) TabVarb = 1;
if (pos2 == TabVarb) TabVarb = 2;
if (pos3 == TabVarb) TabVarb = 3;
if (pos4 == TabVarb) TabVarb = 4;
if (pos5 == TabVarb) TabVarb = 5;
if (pos6 == TabVarb) TabVarb = 6;
if (pos7 == TabVarb) TabVarb = 7;

if (pos1 != TabZut && pos2 != TabZut && pos3 != TabZut && pos4 
	 != TabZut && pos5 != TabZut && pos6 != TabZut && pos7 != TabZut) TabZut = 99;
if (pos1 == TabZut) TabZut = 1;
if (pos2 == TabZut) TabZut = 2;
if (pos3 == TabZut) TabZut = 3;
if (pos4 == TabZut) TabZut = 4;
if (pos5 == TabZut) TabZut = 5;
if (pos6 == TabZut) TabZut = 6;
if (pos7 == TabZut) TabZut = 7;

if (pos1 != TabHuell && pos2 != TabHuell && pos3 != TabHuell
	 && pos4 != TabHuell && pos5 != TabHuell && pos6 != TabHuell && pos7 != TabHuell) TabHuell = 99;
if (pos1 == TabHuell) TabHuell = 1;
if (pos2 == TabHuell) TabHuell = 2;
if (pos3 == TabHuell) TabHuell = 3;
if (pos4 == TabHuell) TabHuell = 5;
if (pos5 == TabHuell) TabHuell = 5;
if (pos6 == TabHuell) TabHuell = 6;
if (pos7 == TabHuell) TabHuell = 7;

if (pos1 != TabVerpack && pos2 != TabVerpack && pos3 != TabVerpack 
	 && pos4 != TabVerpack && pos5 != TabVerpack && pos6 != TabVerpack && pos7 != TabVerpack) TabVerpack = 99;
if (pos1 == TabVerpack) TabVerpack = 1;
if (pos2 == TabVerpack) TabVerpack = 2;
if (pos3 == TabVerpack) TabVerpack = 3;
if (pos4 == TabVerpack) TabVerpack = 4;
if (pos5 == TabVerpack) TabVerpack = 5;
if (pos6 == TabVerpack) TabVerpack = 6;
if (pos7 == TabVerpack) TabVerpack = 7;

if (pos1 != TabArbZeit && pos2 != TabArbZeit && pos3 != TabArbZeit
	 && pos4 != TabArbZeit && pos5 != TabArbZeit && pos6 != TabArbZeit && pos7 != TabArbZeit) TabArbZeit = 99;
if (pos1 == TabArbZeit) TabArbZeit = 1;
if (pos2 == TabArbZeit) TabArbZeit = 2;
if (pos3 == TabArbZeit) TabArbZeit = 3;
if (pos4 == TabArbZeit) TabArbZeit = 4;
if (pos5 == TabArbZeit) TabArbZeit = 5;
if (pos6 == TabArbZeit) TabArbZeit = 6;
if (pos7 == TabArbZeit) TabArbZeit = 7;

if (pos1 != TabKost && pos2 != TabKost && pos3 != TabKost && pos4 != TabKost 
	 && pos5 != TabKost && pos6 != TabKost && pos7 != TabKost) TabKost = 99;
if (pos1 == TabKost) TabKost = 1;
if (pos2 == TabKost) TabKost = 2;
if (pos3 == TabKost) TabKost = 3;
if (pos4 == TabKost) TabKost = 4;
if (pos5 == TabKost) TabKost = 5;
if (pos6 == TabKost) TabKost = 6;
if (pos7 == TabKost) TabKost = 7;

if (pos1 != TabQuid && pos2 != TabQuid && pos3 != TabQuid && pos4 != TabQuid 
	 && pos5 != TabQuid && pos6 != TabQuid && pos7 != TabQuid) TabQuid = 99;
if (pos1 == TabQuid) TabQuid = 1;
if (pos2 == TabQuid) TabQuid = 2;
if (pos3 == TabQuid) TabQuid = 3;
if (pos4 == TabQuid) TabQuid = 4;
if (pos5 == TabQuid) TabQuid = 5;
if (pos6 == TabQuid) TabQuid = 6;
if (pos7 == TabQuid) TabQuid = 7;

}