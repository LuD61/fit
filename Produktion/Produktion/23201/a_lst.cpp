#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include <string.h>
#include "comcthlp.h"
#include "itemc.h"
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_menu.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_intp.h"
#include "inflib.h"
#include "spezdlg.h"
#include "enterfunc.h"
#include "a_lst.h"
#include "spezdlg.h"
#include "searchrezv.h"
//#include "searchvarb.h"
// #include "dlg.h"

#define IDM_CANCEL 3001
#ifndef IDM_WRITE
#define IDM_WRITE  3002
#endif

static int StdSize = STDSIZE;

static int dlgsize    = STDSIZE;
static int ltgraysize = STDSIZE;
static int dlgpossize = STDSIZE;
static int cpsize     = 200;


static mfont dlgfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         GRAYCOL,
                         0,
                         NULL};


static ColButton Ctxt    = { "Text",  10, -1,
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, -1, -1,
                             NULL,  0, 0,
                             BLACKCOL,
                             LTGRAYCOL,
                             0,
};
 
struct A_LSTS  A_LST::Lsts;
struct A_LSTS *A_LST::LstTab;
char *A_LST::InfoItem = "a";


CFIELD *A_LST::_fChoise0 [] = {
    new CFIELD ("cancel", " abbrechen ", 10, 0, 0, 0, NULL, "",
                CBUTTON,
                IDM_CANCEL, &dlgfont, 0, 0),
    new CFIELD ("write", " speichern ", 10, 0,12, 0, NULL, "",
                CBUTTON,
                IDM_WRITE, &dlgfont, 0, 0),
    new CFIELD ("show",  "  ansehen  ", 10, 0,24, 0, NULL, "",
                CBUTTON,
                IDM_CHOISE, &dlgfont, 0, 0),
    new CFIELD ("delete","  l�schen  ", 10, 0,36, 0, NULL, "",
                CBUTTON,
                IDM_DELETE, &dlgfont, 0, 0),
    new CFIELD ("insert"," einf�gen  ", 10, 0,48, 0, NULL, "",
                CBUTTON,
                IDM_INSERT, &dlgfont, 0, 0),
    NULL,
};

CFORM A_LST::fChoise0 (5, _fChoise0);

CFIELD *A_LST::_fChoise [] = {
    new CFIELD ("fChoise0", (char *) &fChoise0, 0, 0, -1 , -2, NULL, "",
                CFFORM,
                500, &dlgfont, 0, 0),
    NULL,
};

CFORM A_LST::fChoise (1, _fChoise);

static char *EkTxt = "SK";
static char *PlanEkTxt = "SK";


ITEM A_LST::unr                 ("nr",                 "Nummer",           "", 0);
ITEM A_LST::urez                ("rez",               "Rezeptur",      "", 0);
ITEM A_LST::ua_bz1              ("a_bz1",              "Bezeichnung",      "", 0);
ITEM A_LST::upr_ek              ("pr_ek",              "SK",            "", 0);
ITEM A_LST::upr_plan_ek         ("plan_ek",            "Plan-SK",            "", 0);
ITEM A_LST::uvariante           ("variante",           "Variante",         "", 0);
ITEM A_LST::uakv                ("akv",                "Aktiv",            "", 0);
ITEM A_LST::ufiller             ("",                   " ",                "",0);

ITEM A_LST::iline ("", "1", "", 0);

ITEM A_LST::inr             ("nr",             Lsts.nr,           "", 0);
ITEM A_LST::irez            ("rez",            Lsts.rez,        "", 0);
ITEM A_LST::ia_bz1          ("a_bz1",          Lsts.a_bz1,        "", 0);
ITEM A_LST::ipr_ek          ("pr_ek",          Lsts.pr_ek,        "", 0);
ITEM A_LST::ipr_plan_ek     ("plan_ek",        Lsts.plan_ek,        "", 0);
ITEM A_LST::ivariante       ("variante",       Lsts.variante,     "", 0);
ITEM A_LST::iakv            ("akv",            Lsts.akv     ,     "", 0);

field A_LST::_UbForm [] = {
&unr,      17, 0, 0, 6,  NULL, "", BUTTON, 0, 0, 0,
&urez,     10, 0, 0, 23,  NULL, "", BUTTON, 0, 0, 0,
&ua_bz1,   30, 0, 0, 33,  NULL, "", BUTTON, 0, 0, 0,
&upr_ek,   13, 0, 0,63,  NULL, "", BUTTON, 0, 0, 0,
&upr_plan_ek, 13, 0, 0,76,  NULL, "", BUTTON, 0, 0, 0,
&uvariante, 9, 0, 0,89,  NULL, "", BUTTON, 0, 0, 0,
&uakv,      6, 0, 0,98,  NULL, "", BUTTON, 0, 0, 0,
&ufiller, 120, 0, 0,104,  NULL, "", BUTTON, 0, 0, 0,
};


form A_LST::UbForm = {7, 0, 0, _UbForm, 0, 0, 0, 0, NULL};

field A_LST::_LineForm [] = {
&iline,   1, 0, 0, 23,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0, 33,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0, 63,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0, 76,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0, 89,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0, 98,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0, 104,  NULL, "",  DISPLAYONLY, 0, 0, 0,
};

form A_LST::LineForm = {7, 0, 0, _LineForm,0, 0, 0, 0, NULL};

field A_LST::_DataForm [] = {
&inr,       16, 0, 0,7,   NULL, "%13.0f",   EDIT,        0, 0, 0,
&irez,      10, 0, 0,25,   NULL, "",      DISPLAYONLY, 0, 0, 0,
&ia_bz1,    25, 0, 0,35,   NULL, "",      DISPLAYONLY, 0, 0, 0,
&ipr_ek,     9, 0, 0,63,  NULL, "%7.3f", DISPLAYONLY, 0, 0, 0,
&ipr_plan_ek, 9, 0, 0,76,  NULL, "%7.3f", DISPLAYONLY, 0, 0, 0,
&ivariante,  5, 0, 0,91,  NULL, "", DISPLAYONLY, 0, 0, 0,
&iakv     ,  6, 0, 0,98,  NULL, "", DISPLAYONLY, 0, 0, 0,
};


form A_LST::DataForm = {7, 0, 0, _DataForm,0, 0, 0, 0, NULL};


/**
int A_LST::ubrows [] = {0,
                          1, 1,
                          2,
                          3,
                          4,
                          5,
                          6,
                          7,
                          8,
                          9,
                         -1,
};
********/
int A_LST::ubrows [] = {0,
                          -1,
};


struct CHATTR A_LST::ChAttra [] = {
//                                      "nr",     DISPLAYONLY,EDIT,
                                      NULL,  0,           0};

struct CHATTR *A_LST::ChAttr = ChAttra;
int A_LST::NoRecNrSet = FALSE;


void A_LST::SetPlanEk (BOOL PlanEk)
{
         this->PlanEk = PlanEk;

         if (PlanEk)
         {
                upr_ek.SetFeld (PlanEkTxt);
         }
         else
         {
                upr_ek.SetFeld (EkTxt);
         }
}



BOOL A_LST::CallInfo (HWND hWnd, RECT *rect, char *Item, char *ItemValue, DWORD WinMode)
/**
Einstieg in Infosystem mit Artikel. Abgeleitet von Klasse Info.
**/
{
 		 char buffer [1024];
         BOOL ret;

         sprintf (buffer, "where a = 2");
         ret = Info::CallInfo (hWnd, rect, "a", buffer, WinMode); 
         SetListFocus ();
         return ret;
}


int A_LST::InfoProc (char **Item, char *Value, char *where)
/**
Spezielle Procedure fuer Info-System. Alter Ablauf, ist nicht mehr Aktiv 
**/
{

   *Item = "a";


   sprintf (where, "where a = %.0lf", Lsts.a);
   return 1;
}

void A_LST::CallInfoEx (void)
/*
Info wird aus Dialog aufgerufen.
*/

{
   char where [512];
   
   sprintf (where, "where a = %.0lf", Lsts.a);
   _CallInfoEx (hMainWindow, NULL, InfoItem, where, 0l); 
}

void A_LST::SetButtonSize (int Size)
{
    int fpos;

    fpos = GetItemPos (&DataForm, "chnr");
    if (fpos < 0) return;

    if (Size == SMALL)
    {
         DataForm.mask [fpos].length = 2;
         DataForm.mask [fpos].rows = 1;
    }
    else 
    {
         DataForm.mask [fpos].length = 3;
         DataForm.mask [fpos].rows = 0;
    }
}


void A_LST::GetCfgValues (void)
{
       char cfg_v [512];
       COLORREF Color;

       if (ProgCfg == NULL) return;
	   if (cfgOK) return;

       cfgOK = TRUE;
       if (ProgCfg->GetCfgValue ("3DList", cfg_v) == TRUE)
       {
                    eListe.Set3D (atoi (cfg_v));
	   }
       if (ProgCfg->GetCfgValue ("3DSize", cfg_v) == TRUE)
       {
                    eListe.SetPlus3D (atoi (cfg_v));
	   }
       if (ProgCfg->GetCfgValue ("ListFocus", cfg_v) == TRUE)
       {
                    ListFocus = atoi (cfg_v);
	   }
        if (ProgCfg->GetCfgValue ("rowheight", cfg_v) == TRUE)
		{
			        RowHeight = ratod (cfg_v);
					if (RowHeight < (double) 1.0)
					{
						RowHeight = (double) 1.0;
					}
		}
        if (ProgCfg->GetCfgValue ("ubheight", cfg_v) == TRUE)
		{
			        UbHeight = atoi (cfg_v);
					SetUbHeight ();
		}
		eListe.SetRowHeight (RowHeight);


        if (ProgCfg->GetCfgValue ("NoRecNr", cfg_v) == TRUE)
        {
			        NoRecNr = atoi (cfg_v);
		            if (atoi (cfg_v))
					{
	                        SetNoRecNr ();
					}
		}
        if (ProgCfg->GetCfgValue ("ListColor", cfg_v) == TRUE)
        {
                     GetListColor (&Color, cfg_v);
		             eListe.SetColor (Color);
        }
        if (ProgCfg->GetCfgValue ("ListBkColor", cfg_v) == TRUE)
        {
                     GetListColor (&Color, cfg_v);
		             eListe.SetBkColor (Color);
        }
        if (ProgCfg->GetCfgValue ("ListLine", cfg_v) == TRUE)
        {
		             eListe.SetLines (GetListLine (cfg_v));
        }
        if (ProgCfg->GetCfgValue ("VarbInfo", cfg_v) == TRUE)
        {
                     InfoItem = new char [80];
                     strcpy (InfoItem, cfg_v);
        }
        if (ProgCfg->GetCfgValue ("PreisMeRemoved", cfg_v) == TRUE)
        {
                      if (atoi (cfg_v) != 0)
                      {
                             DestroyListField ("preisme");
                      }
        }
}


void A_LST::TestRemoves (void)
{
        int fpos;

        fpos = GetItemPos (&DataForm, "feld3");
        if (fpos >= 0)
        {
            if (DataForm.mask[fpos].attribut & REMOVED)
            {
                DestroyListField ("feld3");
            }
        }
}

void A_LST::SetNoRecNr (void)
{
	   int i;

	   eListe.SetNoRecNr (TRUE);
	   if (NoRecNrSet) return;

	   for (i = 0; i < dataform->fieldanz; i ++)
	   {
		   dataform->mask[i].pos[1] -= 6;
	   }
	   for (i = 0; i < ubform->fieldanz; i ++)
	   {
		   ubform->mask[i].pos[1] -= 6;
	   }
	   for (i = 0; i < lineform->fieldanz; i ++)
	   {
		   lineform->mask[i].pos[1] -= 6;
	   }
       NoRecNrSet = TRUE;
}


A_LST::A_LST () : KEYS (), LISTENTER () 
{

    ProgCfg = new PROG_CFG ("23201");
	Dlg = NULL;
    cfgOK = FALSE;
	cursor = -1;
    LstTab = new A_LSTS [0x10000];

    dataform  = &DataForm;
    ubform    = &UbForm;
    lineform  = &LineForm;
    if (sys_ben.berecht > 1 && (sys_ben.berecht < 4))
    {
           DelListField ("pr_ek");
           DelListField ("kosten");
    }
    SetFieldLen ("nr", 15);
    eListe.SetUbRows (ubrows); 
	eListe.SetKeys (this);

//    eListe.SetHscrollStart (2);
//    UbForm.ScrollStart = 2;

    LISTENTER::GetCfgValues ();
    GetCfgValues ();
//    GetSysPar ();
    TestRemoves ();
//    if (NoRecNr)
    {
	    SetNoRecNr ();
    }
	fChoise.Setchpos (0, 2);
    work = NULL;
    ChAttr = ChAttra;
    eListe.SetChAttr (ChAttr); 
    Toolbar2 = NULL;
    eListe.SetInfoProc (InfoProc);
    eListe.SetInfo (this);
    ArrDown = NULL;
    ArrDown = LoadBitmap (DLG::hInstance, "ARRDOWN");
    ekds = 0.0;
    PlanEk = FALSE;
    eListe.SetListclipp (TRUE);
    Ctxt.BkColor = GetSysColor (COLOR_3DFACE);
}


A_LST::~A_LST ()
{
	if (work != NULL)
	{
          work->SetListDlg (NULL);
	}
    fChoise.destroy ();
	if (LstTab != NULL)
	{
  	    delete []LstTab;
		LstTab = NULL;
	}
    if (ProgCfg != NULL)
    {
        ProgCfg->CloseCfg ();
        delete ProgCfg;
        ProgCfg = NULL;
    }
    if (work != NULL)
    {
        work->SetA_Lsts (NULL);
    }

}


// Funktionen f�r den Zugriff auf die Daten. Sind als abstrakte Schnittstellen in
// listenter definiert.

unsigned char *A_LST::GetDataRec (void)
{
    return (unsigned char *) &Lsts;
}

int A_LST::GetDataRecLen (void)
{
    return sizeof (struct A_LSTS);
}

char *A_LST::GetListTab (int i)
{
    return (char *) &LstTab[i];
}

// Ende der Schnittstellen.

void A_LST::SetA_Lsts (int i)
{
       memcpy (&Lsts,&LstTab [i], sizeof (Lsts));
       work->SetA_Lsts (&Lsts);
}


void A_LST::Setfeld2Removed (BOOL removed)
{
    if (removed)
    {
            SetFieldAttr ("feld2", REMOVED);
    }
    else
    {
            SetFieldAttr ("feld2", EDIT);
    }
}


void A_LST::Prepare (void)
{
    if (work != NULL)
    {
        cursor = work->PrepareA ();
    }
}

void A_LST::Open (void)
{
	if (cursor == -1)
	{
		Prepare ();
		if (cursor == -1) return;
	}
    if (work != NULL)
    {
        work->OpenA ();
    }
}

int A_LST::Fetch (void)
{
	int dret ;
	if (cursor == -1)
	{
		Prepare ();
		if (cursor == -1) return 100;
	}
    if (work != NULL)
    {
		dret = work->FetchA ();

        return dret;
    }
	return 100;
}


void A_LST::Close (void)
{
	if (cursor != -1)
	{
		cursor = -1;
	}
    if (work != NULL)
    {
        work->CloseA ();
    }
}



BOOL A_LST::BeforeCol (char *colname)
{
         return FALSE;
}
         




BOOL A_LST::AfterCol (char *colname)
{
         eListe.ShowAktRow ();
		 if (atol(Lsts.nr) == 0l) return TRUE;
         return FALSE;
}

int A_LST::AfterRow (int Row)
{

         if (syskey == KEYUP && ratod (Lsts.nr) == 0.0)
         {
                return OnKey7 ();
         }
         memcpy (&LstTab [Row], &Lsts, sizeof (Lsts));
         work->SetA_Lsts (&Lsts);
         return 1;
}

int A_LST::BeforeRow (int Row)
{
         memcpy (&LstTab [Row], &Lsts, sizeof (Lsts));
         return 1;
}


int A_LST::ShowRez (void)
{

        SEARCHREZV SearchRez;
        struct SREZV *srez;
        char query [40];

       SearchRez.SetWindowStyle (WS_POPUP | WS_THICKFRAME);
       SearchRez.SetParams (DLG::hInstance, GetParent (hMainWindow));

        if (Dlg != NULL)
        {
//               SearchRez.SetParams (DLG::hInstance, ((SpezDlg *) Dlg)->GethWnd ());
               SearchRez.Setawin (((SpezDlg *) Dlg)->GethMainWindow ());
        }
            
        sprintf (query, "and rez != %s", prdk_k.rez);
        SearchRez.SetQuery (query);
        SearchRez.Search (prdk_k.mdn);
        SearchRez.SetQuery (NULL);
        if (syskey == KEY5)
        {
            syskey = 0;
            SetListFocus ();
            return 0;
        }
        srez = SearchRez.GetSRez ();
        if (srez == NULL)
        {
            return 0;
        }

        sprintf (LstTab [eListe.GetAktRow ()].nr, "%s", srez->rez);
        memcpy (&Lsts, &LstTab [eListe.GetAktRow ()], sizeof (Lsts));
        eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
        eListe.ShowAktRow ();
        SetListFocus ();
        return 0;
}

int A_LST::GetRecanz (void)
{
    return eListe.GetRecanz ();
}


BOOL A_LST::StopList (void)
{
    int recanz;
    double chg_gew = 0.0;

    if (AfterRow (eListe.GetAktRow ()) == -1)
    {
        return TRUE;
    }

    if (atol (Lsts.nr) == 0)
    {
        OnKey7 ();
    }

    else
    {
    }

    recanz = eListe.GetRecanz ();

//    work->DeleteAllVarbPos ();
//    for (i = 0; i < recanz; i ++)
//    {
//        WriteRec (i);
//        chg_gew += prdk_varb.gew;
//    }
//    work->UpdateChgGew (chg_gew + work->GetZutGew ());
//	eListe.BreakList ();

	return TRUE;
}

BOOL A_LST::OnKeyNext (void)
{
        if (GetKeyState (VK_CONTROL) >= 0)
        {
              if (Dlg != NULL)
              {
                  ((SpezDlg *) Dlg)->NextPage ();
                  return TRUE;
              }
        }
        return FALSE;
}


BOOL A_LST::OnKeyPrior (void)
{
        if (GetKeyState (VK_CONTROL) >= 0)
        {
              if (Dlg != NULL)
              {
                  ((SpezDlg *) Dlg)->PriorPage ();
                  return TRUE;
              }
        }
        return FALSE;
}


BOOL A_LST::OnKey5 (void)
{


    StopList ();
    syskey = KEY5;
	eListe.BreakList ();
	return TRUE;

}

BOOL A_LST::OnKey6 (void)
{
    eListe.InsertLine ();
    return TRUE;
}

BOOL A_LST::OnKeyInsert (void)
{
    return FALSE;
/*
    eListe.InsertLine ();
    return TRUE;
*/
}

BOOL A_LST::OnKey7 (void)
{
    eListe.DeleteLine ();
    return TRUE;
}

BOOL A_LST::OnKeyDel (void)
{
    return FALSE;
}

BOOL A_LST::OnKey8 (void)
{
         return FALSE;
}


BOOL A_LST::OnKey9 (void)
{
    char * Item = eListe.GetAktItem ();
    if (strcmp (Item, "nr") == 0)
    {
                ShowRez ();
     }
     return TRUE;
}


BOOL A_LST::OnKey10 (void)
{
    if (Dlg != NULL)
    {
         work->SetA_Lsts (&Lsts);
// Hier Workfunktion einsetzen
         SetListFocus ();
         return TRUE;
	}
    return FALSE;
}

BOOL A_LST::OnKey11 (void)
{

    if (Dlg != NULL)
    {
        ((SpezDlg *) Dlg)->OnKey11 ();
        SetAktFocus ();
    }
    return FALSE;
}


int A_LST::ToMemory (int i)
{

    LISTENTER::ToMemory (i);
    return 0;
}


void A_LST::InitRow (void)
{

    LISTENTER::InitRow ();


    work->SetA_Lsts (&Lsts);
    work->InitA_Row ();

}

void A_LST::uebertragen (void)
{
    work->SetA_Lsts (&Lsts);
    work->FillA_Row ();
}

void A_LST::WriteRec (int i)
{
    memcpy (&Lsts, &LstTab[i], sizeof (Lsts));
    if (atol (Lsts.nr) == 0l)
    {
        return;
    }
    work->SetA_Lsts (&LstTab[i]);
}

BOOL A_LST::OnKey12 (void)
{
    StopList ();
    syskey = KEY12;
	eListe.BreakList ();
    return TRUE;
}


BOOL A_LST::OnKeyUp (void)
{

    return FALSE;
}

BOOL A_LST::OnKeyDown (void)
{
    if (atol (Lsts.nr) == 0l)
    {
        return TRUE;
    }

    return FALSE;
}


void A_LST::CreateListQuikInfos (HWND hWnd)
{
       if (Toolbar2 == NULL) return;

       Toolbar2->GetCfield ("f5")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("f12")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("f9")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("del")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("insert")->CreateQuikInfos (hWnd);
}


BOOL A_LST::OnCommand (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    if (LOWORD (wParam) == IDM_CHOISE)
    {
        return OnKey9 ();
    }
    if (LOWORD (wParam) == IDM_CANCEL)
    {
        return OnKey5 ();
    }
    if (LOWORD (wParam) == IDM_WRITE)
    {
        return OnKey12 ();
    }
    if (LOWORD (wParam) == VK_DELETE)
    {
        return OnKeyDel ();
    }
    if (LOWORD (wParam) == IDM_DELETE)
    {
        return OnKey7 ();
    }
    if (LOWORD (wParam) == VK_INSERT)
    {
        return OnKeyInsert ();
    }
    if (LOWORD (wParam) == IDM_INSERT)
    {
        return OnKey6 ();
    }
    if (HIWORD (wParam)  == CBN_CLOSEUP)
	{
        eListe.GetFocusText ();
        if (Dlg != NULL)
        {
	           return ((SpezDlg *) Dlg)->OnCloseUp (hWnd, msg, wParam, lParam);
        }
	}
    if (HIWORD (wParam)  == CBN_DROPDOWN)
	{
        if (strcmp (DataForm.mask[eListe.GetAktColumn ()].item->GetItemName (),
            "nr") == 0)
        {
             eListe.CloseDropDown ();
             return TRUE;
        }
	}
    return FALSE;
}

BOOL A_LST::OnPaint (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    HDC hdc;

    if (Modal == FALSE) return FALSE;
    if (this->mamain1 != hWnd) return FALSE; 
    
    hdc = BeginPaint (hWnd, &ps);
    fChoise.display (hWnd, hdc);
    EndPaint (hWnd,&ps);
    return FALSE;
}

BOOL A_LST::OnMove (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{

    if (Modal == FALSE) return FALSE;
    if (this->mamain1 != hWnd) return FALSE; 

    fChoise.MoveWindow ();
    return FALSE;
}








