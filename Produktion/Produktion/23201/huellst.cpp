#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include <string.h>
#include "comcthlp.h"
#include "itemc.h"
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_menu.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_intp.h"
#include "inflib.h"
#include "spezdlg.h"
#include "enterfunc.h"
#include "HuelLst.h"
#include "spezdlg.h"
#include "searchrezv.h"
#include "searchhuel.h"
// #include "dlg.h"

#define IDM_CANCEL 3001
#ifndef IDM_WRITE
#define IDM_WRITE  3002
#endif

static int StdSize = STDSIZE;

static int dlgsize    = STDSIZE;
static int ltgraysize = STDSIZE;
static int dlgpossize = STDSIZE;
static int cpsize     = 200;
static int Suchvariante  = 0;


static mfont dlgfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         GRAYCOL,
                         0,
                         NULL};

struct HUELLSTS  HUELLST::Lsts;
struct HUELLSTS *HUELLST::LstTab;


CFIELD *HUELLST::_fChoise0 [] = {
    new CFIELD ("cancel", " abbrechen ", 10, 0, 0, 0, NULL, "",
                CBUTTON, 
                IDM_CANCEL, &dlgfont, 0, 0),
    new CFIELD ("write", " speichern ", 10, 0,12, 0, NULL, "",
                CBUTTON, 
                IDM_WRITE, &dlgfont, 0, 0),
    new CFIELD ("show",  "  ansehen  ", 10, 0,24, 0, NULL, "",
                CBUTTON, 
                IDM_CHOISE, &dlgfont, 0, 0),
    new CFIELD ("delete","  l�schen  ", 10, 0,36, 0, NULL, "",
                CBUTTON, 
                IDM_DELETE, &dlgfont, 0, 0),
    new CFIELD ("insert"," einf�gen  ", 10, 0,48, 0, NULL, "",
                CBUTTON, 
                IDM_INSERT, &dlgfont, 0, 0),
    NULL,
};

CFORM HUELLST::fChoise0 (5, _fChoise0);

CFIELD *HUELLST::_fChoise [] = {
    new CFIELD ("fChoise0", (char *) &fChoise0, 0, 0, -1 , -2, NULL, "",
                CFFORM, 
                500, &dlgfont, 0, 0),
    NULL,
};

CFORM HUELLST::fChoise (1, _fChoise);


static char *EkTxt = "EK";
static char *PlanEkTxt = "Plan-EK";


ITEM HUELLST::utyp                ("typ",                "Typ"     ,         "", 0);
ITEM HUELLST::unr                 ("nr",                 "Nummer",           "", 0);
ITEM HUELLST::ua_bz1              ("a_bz1",              "Bezeichnung",      "", 0);  
ITEM HUELLST::ukutf               ("kutf",               "Ktf.",             "", 0);
ITEM HUELLST::ufuell_me           ("fuell_me",           "F�llmenge",        "", 0);
ITEM HUELLST::uhuel_me            ("huel_me",            "Menge",            "", 0);
ITEM HUELLST::ume_einh            ("me_einh",            "Einh.",            "", 0);
ITEM HUELLST::ume_buch            ("me_buch",            "MeVb.",            "", 0);
ITEM HUELLST::upr_ek              ("pr_ek",              "Preis",            "", 0);
ITEM HUELLST::ukosten             ("kosten",             "Kosten",           "", 0);
ITEM HUELLST::upreisme            ("preisme",            "Preis x Me",       "", 0);
ITEM HUELLST::ufiller             ("",                   " ",                "",0);

ITEM HUELLST::iline ("", "1", "", 0);

ITEM HUELLST::ityp            ("typ",            Lsts.typ,          "", 0);
ITEM HUELLST::inr             ("nr",             Lsts.nr,           "", 0);
ITEM HUELLST::ia_bz1          ("a_bz1",          Lsts.a_bz1,        "", 0);  
ITEM HUELLST::ikutf           ("kutf",           Lsts.kutf,         "", 0);
ITEM HUELLST::ifuell_me       ("fuell_me",       Lsts.fuell_me,     "", 0);
ITEM HUELLST::ihuel_me        ("huel_me",        Lsts.huel_me,      "", 0);
ITEM HUELLST::ime_einh        ("me_einh",        Lsts.me_einh,      "", 0);
ITEM HUELLST::ipr_ek          ("pr_ek",          Lsts.pr_ek,        "", 0);
ITEM HUELLST::ikosten         ("kosten",         Lsts.kosten,       "", 0);
ITEM HUELLST::ichtyp          ("chtyp",          (char *) &Lsts.chtyp,   "", 0);
ITEM HUELLST::ichnr           ("chnr",           (char *) &Lsts.chnr,    "", 0);
ITEM HUELLST::ipreisme        ("preisme",        Lsts.preisme,      "", 0);
ITEM HUELLST::ime_buch        ("me_buch",        (char *) &Lsts.mebuch,  "", 0);

field HUELLST::_UbForm [] = {
&utyp,      17, 0, 0,  6,  NULL, "", BUTTON, 0, 0, 0,     
&unr,       21, 0, 0, 23,  NULL, "", BUTTON, 0, 0, 0,     
&ua_bz1,    22, 0, 0, 44,  NULL, "", BUTTON, 0, 0, 0, 
&ukutf,      7, 0, 0, 66,  NULL, "", BUTTON, 0, 0, 0,     
&ufuell_me, 12, 0, 0, 73,  NULL, "", BUTTON, 0, 0, 0,     
&uhuel_me,  12, 0, 0, 85,  NULL, "", BUTTON, 0, 0, 0,     
&ume_einh,  10, 0, 0, 97,  NULL, "", BUTTON, 0, 0, 0,     
&ume_buch,   6, 0, 0,107,  NULL, "", BUTTON, 0, 0, 0,     
&upr_ek,    11, 0, 0,113,  NULL, "", BUTTON, 0, 0, 0,     
&ukosten,   11, 0, 0,124,  NULL, "", BUTTON, 0, 0, 0,     
&upreisme,  12, 0, 0,135,  NULL, "", BUTTON, 0, 0, 0,     
&ufiller,  120, 0, 0,147,  NULL, "", BUTTON, 0, 0, 0,
};


form HUELLST::UbForm = {12, 0, 0, _UbForm, 0, 0, 0, 0, NULL};

field HUELLST::_LineForm [] = {
&iline,   1, 0, 0, 23,  NULL, "",  DISPLAYONLY, 0, 0, 0, 
&iline,   1, 0, 0, 44,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0, 66,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0, 73,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0, 85,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0, 97,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,107,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,113,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,124,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,135,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,147,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
};

form HUELLST::LineForm = {11, 0, 0, _LineForm,0, 0, 0, 0, NULL};

field HUELLST::_DataForm [] = {
&ityp,      16,10, 0,  7,    NULL, "",      DISPLAYONLY, 0, 0, 0,     
&inr,       17, 0, 0, 24,    NULL, "%8d",   DISPLAYONLY,        0, 0, 0,     
&ichnr,      2, 1, 0, 41,   NULL, "",      REMOVED,      0, 0, 0,
&ia_bz1,    20, 0, 0, 45,   NULL, "",      DISPLAYONLY, 0, 0, 0, 
&ikutf,      6, 0, 0, 67,   NULL, "%2d",   EDIT,        0, 0, 0,     
&ifuell_me, 10, 0, 0, 74,   NULL, "%8.4f", EDIT,        0, 0, 0,
&ihuel_me,  10, 0, 0, 86,   NULL, "%8.1f", EDIT,        0, 0, 0,
&ime_einh,   8, 0, 0, 98,   NULL, "",      DISPLAYONLY, 0, 0, 0,      
&ime_buch,   2, 1, 0,109,   NULL, "",      CHECKBUTTON, 0, 0, 0,     
&ipr_ek,     9, 0, 0,114,  NULL, "%7.3f", DISPLAYONLY,  0, 0, 0,     
&ikosten,    9, 0, 0,125,  NULL, "%7.3f", DISPLAYONLY, 0, 0, 0,     
&ipreisme,   9, 0, 0,137,  NULL, "%7.3f", DISPLAYONLY, 0, 0, 0,     
};


form HUELLST::DataForm = {12, 0, 0, _DataForm,0, 0, 0, 0, NULL};

char *HUELLST::TypCombo [] = {"Material",
//                              "Kosten",
                              NULL,
};

void HUELLST::FillCombo (HWND hWnd, char *ItemName)
{
    if (strcmp (ItemName, "typ") == 0)
    {
       for (int i = 0; TypCombo [i]; i ++)
       {
               SendMessage (hWnd, CB_ADDSTRING, 0, 
                           (LPARAM) TypCombo[i]);
       }
    }
}


int HUELLST::ubrows [] = {0, 
//                          0,
                          1, 1,
                          2,
                          3, 
                          4,
                          5,
                          6,
                          7,
                          8,
                          9,
                          10,
                          11,
                         -1,
};


struct CHATTR HUELLST::ChAttra [] = {
//                                     "typ",     DISPLAYONLY,COMBOBOX | DROPLIST, 
                                      "nr",     DISPLAYONLY,EDIT,                
                                      "chnr",   REMOVED,BUTTON,                
                                      NULL,  0,           0};
struct CHATTR HUELLST::ChAttrx [] = {
                                     "typ",     DISPLAYONLY,DISPLAYONLY, 
                                      "nr",     DISPLAYONLY,DISPLAYONLY,                
                                      "chnr",   REMOVED,REMOVED,                
                                      "kutf",   EDIT,EDIT,                
									  "fuell_me", DISPLAYONLY,DISPLAYONLY,
									  "huel_me", DISPLAYONLY,DISPLAYONLY,
									  "text",  DISPLAYONLY,DISPLAYONLY,
                                      NULL,  0,           0};


struct CHATTR *HUELLST::ChAttr = ChAttra;
int HUELLST::NoRecNrSet = FALSE;
char *HUELLST::InfoItem = "a";


void HUELLST::SetPlanEk (BOOL PlanEk)
{
         this->PlanEk = PlanEk;
         
         if (PlanEk)
         {
                upr_ek.SetFeld (PlanEkTxt);
				SetFieldAttr ("pr_ek", EDIT);  //#141205
         }
         else
         {
                upr_ek.SetFeld (EkTxt);
         }
}


BOOL HUELLST::CallInfo (HWND hWnd, RECT *rect, char *Item, char *ItemValue, DWORD WinMode)
/**
Einstieg in Infosystem mit Artikel. Abgeleitet von Klasse Info.
**/
{
 		 char buffer [1024];
         BOOL ret;

         sprintf (buffer, "where a = 2");
         ret = Info::CallInfo (hWnd, rect, "a", buffer, WinMode); 
         SetListFocus ();
         return ret;
}


int HUELLST::InfoProc (char **Item, char *Value, char *where)
/**
Spezielle Procedure fuer Info-System. Alter Ablauf, ist nicht mehr Aktiv 
**/
{

   *Item = "a";

   sprintf (where, "where a = 2");
   return 1;
}

void HUELLST::CallInfoEx (void)
/*
Info wird aus Dialog aufgerufen.
*/

{
   char where [512];
   
   sprintf (where, "where a = %.0lf", Lsts.a);
   _CallInfoEx (hMainWindow, NULL, InfoItem, where, 0l); 
}


void HUELLST::SetButtonSize (int Size)
{
    int fpos;

//    return;
    fpos = GetItemPos (&DataForm, "chtyp");
    if (fpos >= 0)
    {
          if (Size == SMALL)
          {
                DataForm.mask [fpos].length = 2;
                DataForm.mask [fpos].rows = 1;
          }
          else 
          {
                DataForm.mask [fpos].length = 3;
                DataForm.mask [fpos].rows = 0;
          }
    }
    fpos = GetItemPos (&DataForm, "chnr");
    if (fpos >= 0)
    {
          if (Size == SMALL)
          {
                DataForm.mask [fpos].length = 2;
                DataForm.mask [fpos].rows = 1;
          }
          else 
          {
                DataForm.mask [fpos].length = 3;
                DataForm.mask [fpos].rows = 0;
          }
    }
    fpos = GetItemPos (&DataForm, "me_buch");
    if (fpos >= 0)
    {
          if (Size == SMALL)
          {
                DataForm.mask [fpos].length = 2;
                DataForm.mask [fpos].rows = 1;
          }
          else 
          {
                DataForm.mask [fpos].length = 3;
                DataForm.mask [fpos].rows = 0;
          }
    }

}


void HUELLST::GetCfgValues (void)
{
       char cfg_v [512];
       COLORREF Color;

       if (ProgCfg == NULL) return;
	   if (cfgOK) return;

       cfgOK = TRUE;
       if (ProgCfg->GetCfgValue ("Suchvariante", cfg_v) == TRUE)
       {
                 Suchvariante =  (atoi (cfg_v));
	   }
       if (ProgCfg->GetCfgValue ("3DList", cfg_v) == TRUE)
       {
                    eListe.Set3D (atoi (cfg_v));
	   }
       if (ProgCfg->GetCfgValue ("3DSize", cfg_v) == TRUE)
       {
                    eListe.SetPlus3D (atoi (cfg_v));
	   }
       if (ProgCfg->GetCfgValue ("ListFocus", cfg_v) == TRUE)
       {
                    ListFocus = atoi (cfg_v);
	   }
        if (ProgCfg->GetCfgValue ("rowheight", cfg_v) == TRUE)
		{
			        RowHeight = ratod (cfg_v);
					if (RowHeight < (double) 1.0)
					{
						RowHeight = (double) 1.0;
					}
		}
        if (ProgCfg->GetCfgValue ("ubheight", cfg_v) == TRUE)
		{
			        UbHeight = atoi (cfg_v);
					SetUbHeight ();
		}
		eListe.SetRowHeight (RowHeight);


        if (ProgCfg->GetCfgValue ("NoRecNr", cfg_v) == TRUE)
        {
			        NoRecNr = atoi (cfg_v);
		            if (atoi (cfg_v))
					{
	                        SetNoRecNr ();
					}
		}
        if (ProgCfg->GetCfgValue ("ListColor", cfg_v) == TRUE)
        {
                     GetListColor (&Color, cfg_v);
		             eListe.SetColor (Color);
        }
        if (ProgCfg->GetCfgValue ("ListBkColor", cfg_v) == TRUE)
        {
                     GetListColor (&Color, cfg_v);
		             eListe.SetBkColor (Color);
        }
        if (ProgCfg->GetCfgValue ("ListLine", cfg_v) == TRUE)
        {
		             eListe.SetLines (GetListLine (cfg_v));
        }
        if (ProgCfg->GetCfgValue ("HuelInfo", cfg_v) == TRUE)
        {
                     InfoItem = new char [80];
                     strcpy (InfoItem, cfg_v);
        }
        if (ProgCfg->GetCfgValue ("PreisMeRemoved", cfg_v) == TRUE)
        {
                      if (atoi (cfg_v) != 0)
                      {
                             DestroyListField ("preisme");
                      }
        }
}


void HUELLST::TestRemoves (void)
{
        int fpos;

        fpos = GetItemPos (&DataForm, "feld3");
        if (fpos >= 0)
        {
            if (DataForm.mask[fpos].attribut & REMOVED)
            {
                DestroyListField ("feld3");
            }
        }
}


void HUELLST::SetNoRecNr (void)
{
	   int i;

	   eListe.SetNoRecNr (TRUE);
	   if (NoRecNrSet) return;

	   for (i = 0; i < dataform->fieldanz; i ++)
	   {
		   dataform->mask[i].pos[1] -= 6;
	   }
	   for (i = 0; i < ubform->fieldanz; i ++)
	   {
		   ubform->mask[i].pos[1] -= 6;
	   }
	   for (i = 0; i < lineform->fieldanz; i ++)
	   {
		   lineform->mask[i].pos[1] -= 6;
	   }
       NoRecNrSet = TRUE;
}


HUELLST::HUELLST () : KEYS (), LISTENTER () 
{

    ProgCfg = new PROG_CFG ("23201");
	Dlg = NULL;
    cfgOK = FALSE;
	cursor = -1;
    LstTab = new HUELLSTS [0x10000];

    dataform  = &DataForm;
    ubform    = &UbForm;
    lineform  = &LineForm;
	SetDisplayonly();
    if (sys_ben.berecht > 1 && (sys_ben.berecht < 4))
    {
           DelListField ("pr_ek");
           DelListField ("preisme");
           DelListField ("kosten");
    }
    SetFieldLen ("typ", 9);
    SetFieldLen ("nr", 15);
    SetFieldLen ("kutf", 6);
    SetFieldLen ("me_einh", 8);
    eListe.SetUbRows (ubrows); 
	eListe.SetKeys (this);
    LISTENTER::GetCfgValues ();
    GetCfgValues ();
//    GetSysPar ();
    TestRemoves ();
    if (NoRecNr)
    {
	    SetNoRecNr ();
    }
	fChoise.Setchpos (0, 2);
    work = NULL;
	if (sys_ben.berecht >= 3)
    {
		ChAttr = ChAttrx;

    }
	else
	{	
		ChAttr = ChAttra;
    }
    eListe.SetChAttr (ChAttr); 
    Toolbar2 = NULL;
    eListe.SetInfoProc (InfoProc);
    eListe.SetInfo (this);
    ArrDown = NULL;
    ArrDown = LoadBitmap (DLG::hInstance, "ARRDOWN");
    huel_me = 0.0;
    PlanEk = FALSE;
}


HUELLST::~HUELLST ()
{
	if (work != NULL)
	{
          work->SetListDlg (NULL);
	}
    fChoise.destroy ();
	if (LstTab != NULL)
	{
  	    delete []LstTab;
		LstTab = NULL;
	}
    if (ProgCfg != NULL)
    {
        ProgCfg->CloseCfg ();
        delete ProgCfg;
        ProgCfg = NULL;
    }
    if (work != NULL)
    {
        work->SetHuelLsts (NULL);
    }
}


// Funktionen f�r den Zugriff auf die Daten. Sind als abstrakte Schnittstellen in
// listenter definiert.

unsigned char *HUELLST::GetDataRec (void)
{
    return (unsigned char *) &Lsts;
}

int HUELLST::GetDataRecLen (void)
{
    return sizeof (struct HUELLSTS);
}

char *HUELLST::GetListTab (int i)
{
    return (char *) &LstTab[i];
}

// Ende der Schnittstellen.


void HUELLST::Setfeld2Removed (BOOL removed)
{
    if (removed)
    {
            SetFieldAttr ("feld2", REMOVED);
    }
    else
    {
            SetFieldAttr ("feld2", EDIT);
    }
}


void HUELLST::Prepare (void)
{
    if (work != NULL)
    {
        cursor = work->PrepareHuel ();
    }
}

void HUELLST::Open (void)
{
	if (cursor == -1)
	{
		Prepare ();
		if (cursor == -1) return;
	}
    if (work != NULL)
    {
        work->OpenHuel ();
    }
}

int HUELLST::Fetch (void)
{
	if (cursor == -1)
	{
		Prepare ();
		if (cursor == -1) return 100;
	}
    if (work != NULL)
    {
        return work->FetchHuel ();
    }
	return 100;
}

void HUELLST::CalcSum (void)
{
    summegew = 0.0;
    ekds = 0.0;
    double kostds = 0.0;
    double wrt = 0;
    double hkwrt = 0;

    for (int i = 0; i < eListe.GetRecanz (); i ++)
    {
        if (strcmp (LstTab[i].me_buch, "J") == 0)
        {
                double posgew = ratod (LstTab[i].huel_me) * LstTab[i].a_gew;
                double posme = ratod (LstTab[i].huel_me);
                summegew += posgew;
                ekds += work->Round(LstTab[i].gewek * posme,3);
                wrt  += work->Round(LstTab[i].prek * posme,3);
                kostds += ratod (LstTab[i].kosten) * posme;
                hkwrt  += work->Round((LstTab[i].prek * posme + 
                           ratod (LstTab[i].kosten) * posme),3);
        }
        else
        {
                double posme = ratod (LstTab[i].huel_me);
                ekds  += work->Round(LstTab[i].gewek * posme,3);
//                wrt   += ratod (LstTab[i].pr_ek) * ratod (LstTab[i].huel_me);
//                hkwrt += (ratod (LstTab[i].pr_ek) * ratod (LstTab[i].huel_me) + 
//                          ratod (LstTab[i].kosten) * ratod (LstTab[i].huel_me));  #090903

				//#090903
                wrt   +=  work->Round((LstTab[i].prek) * ratod (LstTab[i].huel_me),3);
                hkwrt += work->Round(((LstTab[i].prek) * ratod (LstTab[i].huel_me) + 
                          ratod (LstTab[i].kosten) * ratod (LstTab[i].huel_me)),3);

                kostds += ratod (LstTab[i].kosten) * posme;
        }

    }
/*
    double chg_gew = work->GetChgGew ();
    if (chg_gew != 0.0)
    {
        ekds /= chg_gew;
        kostds /= chg_gew;
    }
*/
/* #240403 LuD
    if (summegew != 0.0)
    {
        ekds /= (summegew  + work->GetZutGew () + work->GetVarbGew ());
        kostds /= (summegew  + work->GetZutGew () + work->GetVarbGew ());
    }
*/   
    double chg_gew = summegew + work->GetZutGew () + work->GetVarbGew ();
    if (chg_gew != 0.0)
    {
        ekds /= chg_gew;
        kostds /= chg_gew;
    }

    wrt = work->Round(wrt,3); //#090903
//    hkwrt = work->Round(wrt,3); //#090903
    hkwrt = work->Round(hkwrt,3); //#010604
    work->SetHuelWrt (wrt);
    work->SetHuelHkWrt (hkwrt);

    double SwProz = prdk_k.sw_kalk / 100;
    double SwMe =  ((work->GetVarbGew () - work->GetVarbGewNto ()) + SwProz *  (chg_gew)) * -1;

    ((SpezDlg *) Dlg)->SetSummen (summegew, ekds, kostds, HUEL,chg_gew,SwMe);
}

void HUELLST::ReportFill (void)
{

    for (int i = 0; i < eListe.GetRecanz (); i ++)
    {


	    reportrez.grouprez = 3;
	    reportrez.a = prdk_k.a;
	    strcpy(reportrez.a_bz1, prdk_kf.a_bz1);						
	    strcpy(reportrez.rez ,prdk_k.rez);
	    strcpy(reportrez.rez_bz, prdk_k.rez_bz);
	    reportrez.variante = prdk_k.variante;
	    strcpy(reportrez.variante_bz, prdk_k.variante_bz);
	    if (strcmp(prdk_kf.ek,"J") == 0)  
        {
			strcpy(reportrez.preis_basis, "EK");				
		}
		else
        {
			strcpy(reportrez.preis_basis, "PLAN-EK");			
		}
	    reportrez.schwund = prdk_k.sw_kalk;
	    reportrez.chargengewicht = prdk_k.chg_gew;
	    strcpy(reportrez.datum, "01.01.2003");                      //muss nocht !
	    reportrez.posi = i + 1;
	    reportrez.mat = atoi (LstTab[i].nr);
	    strcpy(reportrez.mat_bz, LstTab[i].a_bz1);
	    reportrez.menge = ratod (LstTab[i].huel_me);
	    strcpy(reportrez.einheit, LstTab[i].me_einh);
	    reportrez.ek = ratod (LstTab[i].pr_ek);
	    reportrez.wert_ek = ratod (LstTab[i].preisme);
	    reportrez.kosten = ratod (LstTab[i].kosten);
	    reportrez.bep = reportrez.ek + reportrez.kosten;
	    reportrez.wert_bep = reportrez.bep * reportrez.menge;
	    strcpy(reportrez.k_a_bez, "");           //Bezeichnung f�r Kosten und Aufschl�ge
		if (strcmp (LstTab[i].me_buch, "J") == 0)   //090207 Gewicht der H�llen 
		{
			reportrez.k_diff = ratod (LstTab[i].huel_me) * LstTab->a_gew;
		}
		else
		{
			reportrez.k_diff = 0.0;
		}
	    reportrez.kostenkg = 0.0;
	    reportrez.kostenart = 0;
	    strcpy(reportrez.kostenart_bz, "");
	    reportrez.mdn = prdk_k.mdn;
	    work->ReportRezUpdate ();
    }
}





void HUELLST::Close (void)
{
	if (cursor != -1)
	{
		cursor = -1;
	}
    if (work != NULL)
    {
        work->CloseHuel ();
    }
    CalcSum ();
}

BOOL HUELLST::BeforeCol (char *colname)
{
         if (strcmp (colname, "fuell_me") == 0)
         {
             fuell_me = ratod (Lsts.fuell_me);
         }
         else if (strcmp (colname, "huel_me") == 0)
         {
             huel_me = ratod (Lsts.huel_me);
         }
         return FALSE;
}
         

BOOL HUELLST::TestNr (void)
{
         char me_einh [5]; 
         double a_gew;
         double pr_ek;
	     double inh_prod;

         switch (Lsts.typ_kz)
         {
               case 1 :
                   work->SetHuelLsts (&Lsts);
                   if (work->GetHuelMatName () == FALSE)
                   {
                       disp_mess ("Falsche Material-Nr", 2);
                       eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
                       eListe.ShowAktRow ();
                       SetListFocus ();
                       return FALSE;
                   }
                   work->GetHuelAbz1 (Lsts.a_bz1, me_einh, &inh_prod, &a_gew, atol (Lsts.nr));
                   work->GetPtab (Lsts.me_einh, "me_einh", me_einh);
				   break;
         }
         if (atoi (me_einh) == 0)
         {
                  strcpy (me_einh, "2");
         }
         work->GetPtab (Lsts.me_einh, "me_einh", me_einh);
         pr_ek = work->GetMatEk (Lsts.a,0,inh_prod,((SpezDlg *) Dlg)->GetPreisHerkunft(),((SpezDlg *) Dlg)->GetPlanPreisHerkunft());
         if (atoi (me_einh) == 2)
         {
                 a_gew = 1.0;
         }
         Lsts.gewek = pr_ek;
         Lsts.prek = pr_ek; 
         Lsts.a_gew = a_gew;
         sprintf (Lsts.pr_ek,     "%.3lf",  pr_ek);
//         sprintf (Lsts.kosten,    "%.3lf",  work->GetKostenEinh ());
         double kosteneinh = work->GetKostenEinh ();
         double kostenkg   = work->GetKostenKg ();
         sprintf (Lsts.kosten,    "%.3lf", kostenkg);
		 /* Bgut-Zeugs: muss raus 
         if (atoi (me_einh) != 2)
         {
                 if (kosteneinh > 0.0)
                 {
                       sprintf (Lsts.kosten,    "%.3lf", kosteneinh);
                 }
                 else if (kostenkg > 0.0)
                 {
                       sprintf (Lsts.kosten,    "%.3lf", kostenkg * a_gew);
                 }
         }
         else
         {
                 if (kostenkg > 0.0)
                 {
                       sprintf (Lsts.kosten,    "%.3lf", kostenkg);
                 }
                 else if (kosteneinh > 0.0)
                 {
                       sprintf (Lsts.kosten,    "%.3lf", kosteneinh / a_gew);
                 }
         }
		 */
         sprintf (Lsts.kutf, "%d", KtfNext ());
         memcpy (&LstTab[eListe.GetAktRow ()], &Lsts, sizeof (Lsts));
//         eListe.ShowAktRow (); //TODO 170805 hier problem , tritt nicht auf , wenn me_buch (checkbox) aus der Liste entfernt wird
         if (eListe.GetAktRow () > 8)
		 {
			eListe.ShowAktRow (3); 
		 }
		 else
		 {
			eListe.ShowAktRow (); 
		 }
         return TRUE;
}


void HUELLST::FillTypKz (int pos)
{
          clipped (Lsts.typ);

          Lsts.typ_kz = 1;
          for (int i = 0; TypCombo[i] != NULL; i ++)
          {
              if (strcmp (Lsts.typ, TypCombo [i]) == 0)
              {
                  Lsts.typ_kz = i + 1;
                  break;
              }
          }
          memcpy (&LstTab[pos], &Lsts, sizeof (Lsts));
}


BOOL HUELLST::KtfExist ()
{
         int Row = eListe.GetAktRow ();
         int Count = eListe.GetRecanz (); 

         long nr = atol (Lsts.nr);
         short kutf = atoi (Lsts.kutf);  
         for (int i = 0; i < Count; i ++)
         {
             if (i == Row) continue;

             if (nr == atol (LstTab[i].nr) &&
                 kutf == atoi (LstTab[i].kutf))
             {
                 return TRUE;
             }
         }
         return FALSE;
}


int HUELLST::KtfNext (void)
{
         int kutf = 0;
         int Count = eListe.GetRecanz (); 
         int Row = eListe.GetAktRow ();

         for (int i = 0; i < Count; i ++)
         {
             if (i == Row) continue;
             if (atoi (LstTab[i].kutf) > kutf)
             {
                 kutf = atoi (LstTab[i].kutf);
             }
         }
         return kutf + 1;
}


BOOL HUELLST::AfterCol (char *colname)
{
/*
         if (strcmp (colname, "chtyp") == 0)
         {
             disp_mess ("Typ gedr�ckt", 2);
             return TRUE;
         }
*/
         
         if (strcmp (colname, "typ") == 0)
         {
             FillTypKz (eListe.GetAktRow ());
             return FALSE;
         }
         else if (strcmp (colname, "chnr") == 0)
         {
             if (eListe.IsNewRec ())
             {
                 switch (Lsts.typ_kz)
                 {
                     case 1 :
                         ShowNr ();
                         break;
                     case 2 :
                         ShowRez ();
                         break;
                 }
             }
             return TRUE;
         }
         else if (strcmp (colname, "nr") == 0)
         {
             if (TestNr () == FALSE)
             {
                 return TRUE;
             }
         }
         else if (strcmp (colname, "kutf") == 0)
         {
             if (KtfExist ())
             {
                 disp_mess ("Nummer mit dieser Kutterfolge existiert schon",2); 
                 return TRUE;
             }
         }
         else if (strcmp (colname, "fuell_me") == 0)
         {
             work->SetHuelLsts (&Lsts);
             if (fuell_me != ratod (Lsts.fuell_me))
             {
                   work->CalcMe ();
                   memcpy (&LstTab[eListe.GetAktRow ()], &Lsts, sizeof (Lsts));
                    sprintf (Lsts.preisme, "%.3lf", ratod (Lsts.pr_ek) *
                                             ratod (Lsts.huel_me));
                   eListe.ShowAktRow ();
             }
         }
         else if (strcmp (colname, "huel_me") == 0)
         {
             work->SetHuelLsts (&Lsts);
             if (huel_me != ratod (Lsts.huel_me))
             {
                    work->CalcFuellMe ();
                    sprintf (Lsts.preisme, "%.3lf", ratod (Lsts.pr_ek) *
                                             ratod (Lsts.huel_me));
                    sprintf (Lsts.preisme, "%.3lf", (work->Round ((Lsts.prek) *
                                             ratod (Lsts.huel_me),3))); //#090903
                    memcpy (&LstTab[eListe.GetAktRow ()], &Lsts, sizeof (Lsts));
                    eListe.ShowAktRow ();
             }
             CalcSum ();
         }
          else if (strcmp (colname, "pr_ek") == 0) //#141205
         {
			 Lsts.gewek = ratod(Lsts.pr_ek);
			 Lsts.prek = ratod(Lsts.pr_ek);
             memcpy (&LstTab [eListe.GetAktRow ()], &Lsts, sizeof (Lsts));
             CalcSum ();
             sprintf (Lsts.preisme, "%.3lf", work->Round(ratod (Lsts.pr_ek) *
                                             ratod (Lsts.huel_me),3)); 
             memcpy (&LstTab [eListe.GetAktRow ()], &Lsts, sizeof (Lsts));
             eListe.ShowAktRow ();
         }
        else if (strcmp (colname, "me_buch") == 0)
         {
             strcpy (Lsts.me_buch, Lsts.mebuch.GetFeld ());
             memcpy (&LstTab[eListe.GetAktRow ()], &Lsts, sizeof (Lsts));
             CalcSum ();
         }
         return FALSE;
}

int HUELLST::AfterRow (int Row)
{
//         form * DataForm; 

         if (syskey == KEYUP && ratod (Lsts.nr) == 0.0)
         {
                return OnKey7 ();
         }
         memcpy (&LstTab [Row], &Lsts, sizeof (Lsts));
         if (KtfExist ())
         {
                 return -1;
         }
         work->SetHuelLsts (&Lsts);
         CalcSum ();
         return 1;
}

int HUELLST::BeforeRow (int Row)
{
         memcpy (&LstTab [Row], &Lsts, sizeof (Lsts));
         return 1;
}

int HUELLST::ShowNr (void)
{

        SEARCHHUEL SearchHuel;
        struct SHUEL *shuel;

       SearchHuel.SetWindowStyle (WS_POPUP | WS_THICKFRAME);
       SearchHuel.SetParams (DLG::hInstance, GetParent (hMainWindow));
       SearchHuel.SetVariante (Suchvariante);

        if (Dlg != NULL)
        {
               SearchHuel.Setawin (((SpezDlg *) Dlg)->GethMainWindow ());
        }
            
        SearchHuel.Search ();
        if (syskey == KEY5)
        {
            syskey = 0;
            SetListFocus ();
            return 0;
        }
        shuel = SearchHuel.GetShuel ();
        if (shuel == NULL)
        {
            return 0;
        }

        sprintf (LstTab [eListe.GetAktRow ()].nr, "%ld", atol (shuel->mat));
        memcpy (&Lsts, &LstTab [eListe.GetAktRow ()], sizeof (Lsts));
        TestNr ();
        eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
        eListe.ShowAktRow ();
        SetListFocus ();
        return 0;
}

int HUELLST::ShowRez (void)
{

        SEARCHREZV SearchRez;
        struct SREZV *srez;
        char query [40];

       SearchRez.SetWindowStyle (WS_POPUP | WS_THICKFRAME);
       SearchRez.SetParams (DLG::hInstance, GetParent (hMainWindow));

        if (Dlg != NULL)
        {
//               SearchRez.SetParams (DLG::hInstance, ((SpezDlg *) Dlg)->GethWnd ());
               SearchRez.Setawin (((SpezDlg *) Dlg)->GethMainWindow ());
        }
            
        sprintf (query, "and rez != %s", prdk_k.rez);
        SearchRez.SetQuery (query);
        SearchRez.Search (prdk_k.mdn);
        SearchRez.SetQuery (NULL);
        if (syskey == KEY5)
        {
            SetListFocus ();
            return 0;
        }
        srez = SearchRez.GetSRez ();
        if (srez == NULL)
        {
            return 0;
        }

        sprintf (LstTab [eListe.GetAktRow ()].nr, "%s", srez->rez);
        memcpy (&Lsts, &LstTab [eListe.GetAktRow ()], sizeof (Lsts));
        TestNr ();
        eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
        eListe.ShowAktRow ();
        SetListFocus ();
        return 0;
}


BOOL HUELLST::StopList (void)
{
    int i;
    int recanz;
    double chg_gew = 0.0;

    if (AfterRow (eListe.GetAktRow ()) == -1)
    {
        return TRUE;
    }

    if (atol (Lsts.nr) == 0)
    {
        OnKey7 ();
    }

    else
    {
    }

    recanz = eListe.GetRecanz ();

    work->DeleteAllHuelPos ();
    for (i = 0; i < recanz; i ++)
    {
        WriteRec (i);
        chg_gew += prdk_huel.gew;
    }
    work->UpdateChgGew (work->GetVarbGew () + work->GetZutGew ());
	return TRUE;
}

BOOL HUELLST::OnKeyNext (void)
{
        if (GetKeyState (VK_CONTROL) >= 0)
        {
              if (Dlg != NULL)
              {
                  ((SpezDlg *) Dlg)->NextPage ();
                  return TRUE;
              }
        }
        return FALSE;
}


BOOL HUELLST::OnKeyPrior (void)
{
        if (GetKeyState (VK_CONTROL) >= 0)
        {
              if (Dlg != NULL)
              {
                  ((SpezDlg *) Dlg)->PriorPage ();
                  return TRUE;
              }
        }
        return FALSE;
}

BOOL HUELLST::OnKey5 (void)
{

    OnKey12 ();
    syskey = KEY5;
//	eListe.BreakList ();
	return TRUE;

}

BOOL HUELLST::OnKey6 (void)
{
    eListe.InsertLine ();
    return TRUE;
}

BOOL HUELLST::OnKeyInsert (void)
{
    return FALSE;
/*
    eListe.InsertLine ();
    return TRUE;
*/
}

BOOL HUELLST::OnKey7 (void)
{
    eListe.DeleteLine ();
    CalcSum ();
    return TRUE;
}

BOOL HUELLST::OnKeyDel (void)
{
    return FALSE;
/*
    eListe.DeleteLine ();
    CalcSum ();
    return TRUE;
*/
}

BOOL HUELLST::OnKey8 (void)
{
         return FALSE;
}


BOOL HUELLST::OnKey9 (void)
{
    char * Item = eListe.GetAktItem ();
    if (strcmp (Item, "typ") == 0)
    {
        eListe.ShowDropDown ();
        return TRUE;
    }
    else if (strcmp (Item, "nr") == 0)
    {
        switch (Lsts.typ_kz)
        {
             case 1 :
                ShowNr ();
                break;
             case 2 :
                ShowRez ();
                break;
        }
        return TRUE;
    }
    return FALSE;
}


BOOL HUELLST::OnKey10 (void)
{
    if (Dlg != NULL)
    {
         work->SetHuelLsts (&Lsts);
// Hier Workfunktion einsetzen
         SetListFocus ();
         return TRUE;
	}
    return FALSE;
}

BOOL HUELLST::OnKey11 (void)
{

    if (Dlg != NULL)
    {
        ((SpezDlg *) Dlg)->OnKey11 ();
        SetAktFocus ();
    }
    return FALSE;
}


int HUELLST::ToMemory (int i)
{

    LISTENTER::ToMemory (i);
    return 0;
}


void HUELLST::InitRow (void)
{

    LISTENTER::InitRow ();


    work->SetHuelLsts (&Lsts);
    work->InitHuelRow ();

/*
    if (ArrDown != NULL && Lsts.chtyp[0] != NULL)
    {
        Lsts.chtyp[0]->SetBitMap (ArrDown);
    }
*/

    if (ArrDown != NULL)
    {
        Lsts.chnr.SetBitMap (ArrDown);
    }

}

void HUELLST::uebertragen (void)
{
    work->SetHuelLsts (&Lsts);
    work->FillHuelRow ();
    if (Lsts.typ_kz <= 0)
    {
          Lsts.typ_kz = 1;
    }
	for (int i = 0; TypCombo[i] != NULL; i ++);
	if (Lsts.typ_kz <= i)
	{
          strcpy (Lsts.typ, TypCombo [Lsts.typ_kz - 1]);
	}
	else
	{
          strcpy (Lsts.typ, TypCombo [0]);
	}

    if (ArrDown != NULL)
    {
        Lsts.chtyp.SetBitMap (ArrDown);
    }
    if (ArrDown != NULL)
    {
        Lsts.chnr.SetBitMap (ArrDown);
    }
}

void HUELLST::WriteRec (int i)
{
    memcpy (&Lsts, &LstTab[i], sizeof (Lsts));
    if (atol (Lsts.nr) == 0l)
    {
        return;
    }
    FillTypKz (i);
    work->SetHuelLsts (&LstTab[i]);
    work->WriteHuelRec ();
}

BOOL HUELLST::OnKey12 (void)
{
	if (sys_ben.berecht >= 3) return TRUE;
    if (KtfExist ())
    {
           disp_mess ("Nummer mit dieser Kutterfolge existiert schon",2); 
           return TRUE;
    }
    CalcSum ();
    StopList ();
    syskey = KEY12;
    return TRUE;
}


BOOL HUELLST::OnKeyUp (void)
{
//    form *DataForm;
//    int fpos;
/*
    if (Leere Zeile)
    {
        return OnKeyDel ();
    }
*/

    return FALSE;
}

BOOL HUELLST::OnKeyDown (void)
{
    if (atol (Lsts.nr) == 0l)
    {
        return TRUE;
    }

    return FALSE;
}


void HUELLST::CreateListQuikInfos (HWND hWnd)
{
       if (Toolbar2 == NULL) return;

       Toolbar2->GetCfield ("f5")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("f12")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("f9")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("del")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("insert")->CreateQuikInfos (hWnd);
}


BOOL HUELLST::OnCommand (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    if (LOWORD (wParam) == IDM_CHOISE)
    {
        return OnKey9 ();
    }
    if (LOWORD (wParam) == IDM_CANCEL)
    {
        return OnKey5 ();
    }
    if (LOWORD (wParam) == IDM_WRITE)
    {
        return OnKey12 ();
    }
    if (LOWORD (wParam) == VK_DELETE)
    {
        return OnKeyDel ();
    }
    if (LOWORD (wParam) == IDM_DELETE)
    {
        return OnKeyDel ();
    }
    if (LOWORD (wParam) == VK_INSERT)
    {
        return OnKeyInsert ();
    }
    if (LOWORD (wParam) == IDM_INSERT)
    {
        return OnKey6 ();
    }
    if (HIWORD (wParam)  == CBN_CLOSEUP)
	{
        eListe.GetFocusText ();
        FillTypKz (eListe.GetAktRow ());
        if (Dlg != NULL)
        {
	           return ((SpezDlg *) Dlg)->OnCloseUp (hWnd, msg, wParam, lParam);
        }
	}
    return FALSE;
}

BOOL HUELLST::OnPaint (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    HDC hdc;

    if (Modal == FALSE) return FALSE;
    if (this->mamain1 != hWnd) return FALSE; 
    
    hdc = BeginPaint (hWnd, &ps);
    fChoise.display (hWnd, hdc);
    EndPaint (hWnd,&ps);
    return FALSE;
}

BOOL HUELLST::OnMove (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{

    if (Modal == FALSE) return FALSE;
    if (this->mamain1 != hWnd) return FALSE; 

    fChoise.MoveWindow ();
    return FALSE;
}

void HUELLST::SetDisplayonly (void)
{
    if (sys_ben.berecht >= 3)
    {
            SetFieldAttr ("typ", DISPLAYONLY);
            SetFieldAttr ("nr", DISPLAYONLY);
            SetFieldAttr ("fuell_me", DISPLAYONLY);
            SetFieldAttr ("huel_me", DISPLAYONLY);
            SetFieldAttr ("me_buch", REMOVED);
            SetFieldAttr ("text", DISPLAYONLY);
    }
}







