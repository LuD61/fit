#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "wmaskc.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "reportrez.h"

struct REPORTREZ reportrez, reportrez_null;

void REPORTREZ_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *)   &reportrez.mdn, 1, 0);
            ins_quest ((char *)   &reportrez.a,   3, 0);
            ins_quest ((char *)   &reportrez.variante,  1, 0);
            ins_quest ((char *)   &reportrez.grouprez,  1, 0);
            ins_quest ((char *)   &reportrez.posi,  1, 0);
    out_quest ((char *) &reportrez.grouprez,1,0);
    out_quest ((char *) &reportrez.a,3,0);
    out_quest ((char *) reportrez.a_bz1,0,25);
    out_quest ((char *) reportrez.rez,0,9);
    out_quest ((char *) reportrez.rez_bz,0,73);
    out_quest ((char *) &reportrez.variante,1,0);
    out_quest ((char *) reportrez.variante_bz,0,25);
    out_quest ((char *) reportrez.preis_basis,0,9);
    out_quest ((char *) &reportrez.schwund,3,0);
    out_quest ((char *) &reportrez.chargengewicht,3,0);
    out_quest ((char *) reportrez.datum,0,11);
    out_quest ((char *) &reportrez.posi,1,0);
    out_quest ((char *) &reportrez.mat,2,0);
    out_quest ((char *) reportrez.mat_bz,0,25);
    out_quest ((char *) &reportrez.menge,3,0);
    out_quest ((char *) reportrez.einheit,0,21);
    out_quest ((char *) &reportrez.ek,3,0);
    out_quest ((char *) &reportrez.wert_ek,3,0);
    out_quest ((char *) &reportrez.kosten,3,0);
    out_quest ((char *) &reportrez.bep,3,0);
    out_quest ((char *) &reportrez.wert_bep,3,0);
    out_quest ((char *) reportrez.k_a_bez,0,13);
    out_quest ((char *) &reportrez.k_diff,3,0);
    out_quest ((char *) &reportrez.kostenkg,3,0);
    out_quest ((char *) &reportrez.kostenart,1,0);
    out_quest ((char *) reportrez.kostenart_bz,0,31);
    out_quest ((char *) &reportrez.mdn,1,0);
            cursor = prepare_sql ("select reportrez.grouprez,  "
"reportrez.a,  reportrez.a_bz1,  reportrez.rez,  reportrez.rez_bz,  "
"reportrez.variante,  reportrez.variante_bz,  reportrez.preis_basis,  "
"reportrez.schwund,  reportrez.chargengewicht,  reportrez.datum,  "
"reportrez.posi,  reportrez.mat,  reportrez.mat_bz,  reportrez.menge,  "
"reportrez.einheit,  reportrez.ek,  reportrez.wert_ek,  "
"reportrez.kosten,  reportrez.bep,  reportrez.wert_bep,  "
"reportrez.k_a_bez,  reportrez.k_diff,  reportrez.kostenkg,  "
"reportrez.kostenart,  reportrez.kostenart_bz,  reportrez.mdn from reportrez "

#line 24 "reportrez.rpp"
                                  "where mdn = ? "
                                  "and   a = ? "
                                  "and   variante = ? "
                                  "and grouprez = ? "
                                  "and posi = ? ");
    ins_quest ((char *) &reportrez.grouprez,1,0);
    ins_quest ((char *) &reportrez.a,3,0);
    ins_quest ((char *) reportrez.a_bz1,0,25);
    ins_quest ((char *) reportrez.rez,0,9);
    ins_quest ((char *) reportrez.rez_bz,0,73);
    ins_quest ((char *) &reportrez.variante,1,0);
    ins_quest ((char *) reportrez.variante_bz,0,25);
    ins_quest ((char *) reportrez.preis_basis,0,9);
    ins_quest ((char *) &reportrez.schwund,3,0);
    ins_quest ((char *) &reportrez.chargengewicht,3,0);
    ins_quest ((char *) reportrez.datum,0,11);
    ins_quest ((char *) &reportrez.posi,1,0);
    ins_quest ((char *) &reportrez.mat,2,0);
    ins_quest ((char *) reportrez.mat_bz,0,25);
    ins_quest ((char *) &reportrez.menge,3,0);
    ins_quest ((char *) reportrez.einheit,0,21);
    ins_quest ((char *) &reportrez.ek,3,0);
    ins_quest ((char *) &reportrez.wert_ek,3,0);
    ins_quest ((char *) &reportrez.kosten,3,0);
    ins_quest ((char *) &reportrez.bep,3,0);
    ins_quest ((char *) &reportrez.wert_bep,3,0);
    ins_quest ((char *) reportrez.k_a_bez,0,13);
    ins_quest ((char *) &reportrez.k_diff,3,0);
    ins_quest ((char *) &reportrez.kostenkg,3,0);
    ins_quest ((char *) &reportrez.kostenart,1,0);
    ins_quest ((char *) reportrez.kostenart_bz,0,31);
    ins_quest ((char *) &reportrez.mdn,1,0);
            sqltext = "update reportrez set "
"reportrez.grouprez = ?,  reportrez.a = ?,  reportrez.a_bz1 = ?,  "
"reportrez.rez = ?,  reportrez.rez_bz = ?,  reportrez.variante = ?,  "
"reportrez.variante_bz = ?,  reportrez.preis_basis = ?,  "
"reportrez.schwund = ?,  reportrez.chargengewicht = ?,  "
"reportrez.datum = ?,  reportrez.posi = ?,  reportrez.mat = ?,  "
"reportrez.mat_bz = ?,  reportrez.menge = ?,  reportrez.einheit = ?,  "
"reportrez.ek = ?,  reportrez.wert_ek = ?,  reportrez.kosten = ?,  "
"reportrez.bep = ?,  reportrez.wert_bep = ?,  reportrez.k_a_bez = ?,  "
"reportrez.k_diff = ?,  reportrez.kostenkg = ?,  "
"reportrez.kostenart = ?,  reportrez.kostenart_bz = ?,  "
"reportrez.mdn = ? "

#line 30 "reportrez.rpp"
                                  "where mdn = ? "
                                  "and   a = ? "
                                  "and   variante = ?"
                                  "and   grouprez = ?"
                                  "and   posi = ?";
            ins_quest ((char *)   &reportrez.mdn, 1, 0);
            ins_quest ((char *)   &reportrez.a,   3, 0);
            ins_quest ((char *)   &reportrez.variante,  1, 0);
            ins_quest ((char *)   &reportrez.grouprez,  1, 0);
            ins_quest ((char *)   &reportrez.posi,  1, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &reportrez.mdn, 1, 0);
            ins_quest ((char *)   &reportrez.a,   3, 0);
            ins_quest ((char *)   &reportrez.variante,  1, 0);
            ins_quest ((char *)   &reportrez.grouprez,  1, 0);
            ins_quest ((char *)   &reportrez.posi,  1, 0);
            test_upd_cursor = prepare_sql ("select a from reportrez "
                                  "where mdn = ? "
                                  "and   a = ? "
                                  "and   variante = ? "
                                  "and   grouprez = ? "
                                  "and   posi = ? "
                                  "for update");

            ins_quest ((char *) &reportrez.mdn, 1, 0);
            ins_quest ((char *)   &reportrez.a,   3, 0);
            ins_quest ((char *)   &reportrez.variante,  1, 0);
            ins_quest ((char *)   &reportrez.grouprez,  1, 0);
            ins_quest ((char *)   &reportrez.posi,  1, 0);
            del_cursor = prepare_sql ("delete from reportrez "
                                  "where mdn = ? "
                                  "and   a = ? "
                                  "and   variante = ?"
                                  "and   grouprez = ? "
                                  "and   posi = ? ");
    ins_quest ((char *) &reportrez.grouprez,1,0);
    ins_quest ((char *) &reportrez.a,3,0);
    ins_quest ((char *) reportrez.a_bz1,0,25);
    ins_quest ((char *) reportrez.rez,0,9);
    ins_quest ((char *) reportrez.rez_bz,0,73);
    ins_quest ((char *) &reportrez.variante,1,0);
    ins_quest ((char *) reportrez.variante_bz,0,25);
    ins_quest ((char *) reportrez.preis_basis,0,9);
    ins_quest ((char *) &reportrez.schwund,3,0);
    ins_quest ((char *) &reportrez.chargengewicht,3,0);
    ins_quest ((char *) reportrez.datum,0,11);
    ins_quest ((char *) &reportrez.posi,1,0);
    ins_quest ((char *) &reportrez.mat,2,0);
    ins_quest ((char *) reportrez.mat_bz,0,25);
    ins_quest ((char *) &reportrez.menge,3,0);
    ins_quest ((char *) reportrez.einheit,0,21);
    ins_quest ((char *) &reportrez.ek,3,0);
    ins_quest ((char *) &reportrez.wert_ek,3,0);
    ins_quest ((char *) &reportrez.kosten,3,0);
    ins_quest ((char *) &reportrez.bep,3,0);
    ins_quest ((char *) &reportrez.wert_bep,3,0);
    ins_quest ((char *) reportrez.k_a_bez,0,13);
    ins_quest ((char *) &reportrez.k_diff,3,0);
    ins_quest ((char *) &reportrez.kostenkg,3,0);
    ins_quest ((char *) &reportrez.kostenart,1,0);
    ins_quest ((char *) reportrez.kostenart_bz,0,31);
    ins_quest ((char *) &reportrez.mdn,1,0);
            ins_cursor = prepare_sql ("insert into reportrez ("
"grouprez,  a,  a_bz1,  rez,  rez_bz,  variante,  variante_bz,  preis_basis,  schwund,  "
"chargengewicht,  datum,  posi,  mat,  mat_bz,  menge,  einheit,  ek,  wert_ek,  kosten,  "
"bep,  wert_bep,  k_a_bez,  k_diff,  kostenkg,  kostenart,  kostenart_bz,  mdn) "

#line 67 "reportrez.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

#line 69 "reportrez.rpp"
}
