#ifndef _VPKLST_DEF
#define _VPKLST_DEF
#include "listenter.h"
#include "listclex.h"
#include "work.h"
#include "mo_progcfg.h"

#define SMALL 0
#define BIG 1
 
class VPKLST : public virtual LISTENTER, 
                public virtual KEYS,
                public virtual Info
{
         private :
                      static CFIELD *_fChoise0[];
                      static CFORM fChoise0;
                      static CFIELD *_fChoise[];
                      static CFORM fChoise;

                      static struct CHATTR ChAttra [];
                      static struct CHATTR ChAttrx [];
                      static struct CHATTR *ChAttr;
                      Work *work;
                      int cursor;
                      CFORM *Toolbar2;
                      BOOL cfgOK;
                      PROG_CFG *ProgCfg;
                      HBITMAP ArrDown;
                      double fuell_me;
                      double vpk_me;
                      double summegew;
                      double ekds;
                      BOOL PlanEk;
                      static int NoRecNrSet;
                      static char *TypCombo [];

         public :

                     static struct VPKLSTS Lsts;
                     static struct VPKLSTS *LstTab;
                     static ITEM utyp;
                     static ITEM unr;
                     static ITEM ua_bz1;
                     static ITEM ukutf;
                     static ITEM ufuell_me;
                     static ITEM uvpk_me;
                     static ITEM ume_einh;
                     static ITEM upr_ek;
                     static ITEM ukosten;
                     static ITEM ufiller;
                     static ITEM upreisme;
                     static field _UbForm[];
                     static form UbForm;

                     static ITEM iline;
                     static field _LineForm[];
                     static form LineForm;

                     static ITEM ityp;
                     static ITEM inr;
                     static ITEM ia_bz1;
                     static ITEM ikutf;
                     static ITEM ifuell_me;
                     static ITEM ivpk_me;
                     static ITEM ime_einh;
                     static ITEM ipr_ek;
                     static ITEM ikosten;
                     static ITEM ipreisme;
                     static ITEM ichtyp;
                     static ITEM ichnr;
                     static field _DataForm[];
                     static form DataForm;
                     static int ubrows[];
                     static char *InfoItem;
                     void SetButtonSize (int);


                     void SetToolbar2 (CFORM *Toolbar2)
                     {
                               this->Toolbar2 = Toolbar2;
                     }
 
                     CFORM *GetToolbar2 (void)
                     {
                               return Toolbar2;
                     }

                     void SetWork (Work *work)
                     {
                         this->work = work;
                         this->work->SetListDlg (this);
                     }

                     Work *GetWork (void)
                     {
                         return work;
                     }

                     VPKLST ();
					 ~VPKLST ();
                     void SetPlanEk (BOOL);
                     void GetSysPar (void);
                     void GetCfgValues (void);
                     void Setfeld2Removed (BOOL);
                     void Prepare (void);
                     void Open (void);
                     int Fetch (void);
                     void Close (void);
                     void CalcSum (void);
                     void SetDisplayonly (void);
                     void ReportFill (void);
                     
 				     unsigned char *GetDataRec (void);
				     int GetDataRecLen (void);
 				     char *GetListTab (int);
                     void WriteRec (int);
                     void InitRow (void);
                     int ToMemory (int);
                     void uebertragen (void);

                     void FillCombo (HWND, char *);
                     void SetNoRecNr (void);
                     BOOL StopList (void);
                     BOOL OnKey5 (void);
                     BOOL OnKey6 (void);
                     BOOL OnKey7 (void);
                     BOOL OnKey8 (void);
                     BOOL OnKey9 (void);
                     BOOL OnKey10 (void);
                     BOOL OnKey11 (void);
                     BOOL OnKey12 (void);
                     BOOL OnKeyDel (void);
                     BOOL OnKeyInsert (void);
                     BOOL OnKeyUp (void);
                     BOOL OnKeyDown (void);
                     BOOL OnKeyNext (void);
                     BOOL OnKeyPrior (void);
                     BOOL OnCommand (HWND, UINT, WPARAM, LPARAM);
                     BOOL OnPaint (HWND, UINT, WPARAM, LPARAM);
                     BOOL OnMove (HWND, UINT, WPARAM, LPARAM);
                     int AfterRow (int);
                     BOOL TestNr (void);
                     void FillTypKz (int);
                     BOOL KtfExist ();
                     int KtfNext (void);
                     int AfterCol (char *);
                     int BeforeCol (char *);
                     int BeforeRow (int);
                     void CreateListQuikInfos (HWND);
                     void TestRemoves (void);
                     int  ShowNr (void);
                     int  ShowRez (void);
                     static int InfoProc (char **, char *, char *);

                     BOOL CallInfo (HWND, RECT *, char *, char *, DWORD);
                     void CallInfoEx (void);
};
#endif