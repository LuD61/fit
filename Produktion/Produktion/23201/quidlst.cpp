#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include <string.h>
#include "comcthlp.h"
#include "itemc.h"
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_menu.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_intp.h"
#include "inflib.h"
#include "spezdlg.h"
#include "enterfunc.h"
#include "quidlst.h"
#include "spezdlg.h"
#include "searchrezv.h"
#include "searchka.h"
// #include "dlg.h"

#define IDM_CANCEL 3001
#ifndef IDM_WRITE
#define IDM_WRITE  3002
#endif

#ifndef MAXCOMBO
#define MAXCOMBO 20
#endif

static int StdSize = STDSIZE;

static int dlgsize    = STDSIZE;
static int ltgraysize = STDSIZE;
static int dlgpossize = STDSIZE;
static int cpsize     = 200;


static mfont dlgfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         GRAYCOL,
                         0,
                         NULL};

struct QUIDLSTS  QUIDLST::Lsts;
struct QUIDLSTS *QUIDLST::LstTab;


CFIELD *QUIDLST::_fChoise0 [] = {
    new CFIELD ("cancel", " abbrechen ", 10, 0, 0, 0, NULL, "",
                CBUTTON, 
                IDM_CANCEL, &dlgfont, 0, 0),
    new CFIELD ("write", " speichern ", 10, 0,12, 0, NULL, "",
                CBUTTON, 
                IDM_WRITE, &dlgfont, 0, 0),
    new CFIELD ("show",  "  ansehen  ", 10, 0,24, 0, NULL, "",
                CBUTTON, 
                IDM_CHOISE, &dlgfont, 0, 0),
    new CFIELD ("delete","  l�schen  ", 10, 0,36, 0, NULL, "",
                CBUTTON, 
                IDM_DELETE, &dlgfont, 0, 0),
    new CFIELD ("insert"," einf�gen  ", 10, 0,48, 0, NULL, "",
                CBUTTON, 
                IDM_INSERT, &dlgfont, 0, 0),
    NULL,
};

CFORM QUIDLST::fChoise0 (5, _fChoise0);

CFIELD *QUIDLST::_fChoise [] = {
    new CFIELD ("fChoise0", (char *) &fChoise0, 0, 0, -1 , -2, NULL, "",
                CFFORM, 
                500, &dlgfont, 0, 0),
    NULL,
};

CFORM QUIDLST::fChoise (1, _fChoise);



ITEM QUIDLST::uquid_kat        ("quid_kat",       "Kategorie",   "", 0); 
ITEM QUIDLST::uquid_nr                ("quid_nr",               "Quid Nr."	,      "", 0);
ITEM QUIDLST::uquid_bez              ("quid_bez",             "Quid Bez",         "", 0);
ITEM QUIDLST::uquid_proz            ("quid_proz",           "Proz.Anteil",       "", 0);

ITEM QUIDLST::ufiller              ("",                   " ",                "",0);

ITEM QUIDLST::iline ("", "1", "", 0);

ITEM QUIDLST::iquid_dummy        ("quid_dummy",       "",   "", 0); 
ITEM QUIDLST::iquid_kat        ("quid_kat",       Lsts.quid_kat,   "", 0); 
ITEM QUIDLST::iquid_nr         ("quid_nr",        Lsts.quid_nr	,  "", 0);
ITEM QUIDLST::iquid_bez              ("quid_bez",             Lsts.quid_bez,         "", 0);
ITEM QUIDLST::iquid_proz            ("quid_proz",           Lsts.quid_proz,       "", 0);
//ITEM QUIDLST::ichtyp          ("chtyp",          (char *) &Lsts.chtyp,         "", 0);
//ITEM QUIDLST::ichnr           ("chnr",           (char *) &Lsts.chnr,          "", 0);

field QUIDLST::_UbForm [] = {
&uquid_kat,     17, 0, 0,  6,  NULL, "", BUTTON, 0, 0, 0,     
&uquid_nr,      11, 0, 0, 23,  NULL, "", BUTTON, 0, 0, 0,     
&uquid_bez,     22, 0, 0, 34,  NULL, "", BUTTON, 0, 0, 0, 
&uquid_proz,    12, 0, 0, 56,  NULL, "", BUTTON, 0, 0, 0,     
&ufiller,      120, 0, 0,120,  NULL, "", BUTTON, 0, 0, 0,
};


form QUIDLST::UbForm = {5, 0, 0, _UbForm, 0, 0, 0, 0, NULL};

field QUIDLST::_LineForm [] = {
&iline,   1, 0, 0, 23,  NULL, "",  DISPLAYONLY, 0, 0, 0, 
&iline,   1, 0, 0, 34,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0, 56,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0, 68,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,120,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
};

form QUIDLST::LineForm = {5, 0, 0, _LineForm,0, 0, 0, 0, NULL};

field QUIDLST::_DataForm [] = {
&iquid_dummy,       1,10, 0,  7,   NULL, "",        EDIT, 0, 0, 0,     
&iquid_kat,       16,10, 0,  8,   NULL, "",        EDIT, 0, 0, 0,     
&iquid_nr,         8, 0, 0, 24,   NULL, "",     EDIT, 0, 0, 0,     
&iquid_bez,       20, 0, 0, 35,   NULL, "",        EDIT, 0, 0, 0, 
&iquid_proz,      10, 0, 0, 57,   NULL, "",  EDIT, 0, 0, 0,     
};


form QUIDLST::DataForm = {5, 0, 0, _DataForm,0, 0, 0, 0, NULL};

char *QUIDLST::ProdSchrittCombo [MAXCOMBO] = {"keine Produktionsschritte",
                                              NULL,
};


void QUIDLST::FillCombo (HWND hWnd, char *ItemName)
{
    static BOOL FillOK = FALSE;

/*
    if (strcmp (ItemName, "prod_schritt") == 0)
    {
       if (work != NULL)
       {
           work->FillComboLong (ProdSchrittCombo, "prod_schritt", MAXCOMBO - 1);
       }
       for (int i = 0; ProdSchrittCombo [i]; i ++)
       {
               SendMessage (hWnd, CB_ADDSTRING, 0, 
                           (LPARAM) ProdSchrittCombo[i]);
       }
       if (strcmp (clipped (Lsts.prod_schritt), " ") <= 0)
       {
              strcpy (Lsts.prod_schritt, ProdSchrittCombo [0]);
       }
    }
	*/
}

int QUIDLST::ubrows [] = {0, 
//                          0,
                          1, 1,
                          2,
                          3, 
                          4,
                          5,
                          6,
                         -1,
};


struct CHATTR QUIDLST::ChAttra [] = {
                                     "prod_schritt",    COMBOBOX,COMBOBOX | DROPLIST, 
//                                      "kost_art",     DISPLAYONLY,EDIT,                
//                                      "chnr",         REMOVED,BUTTON,                
                                      NULL,  0,           0};

struct CHATTR *QUIDLST::ChAttr = ChAttra;
int QUIDLST::NoRecNrSet = FALSE;
char *QUIDLST::InfoItem = "kost_art";



BOOL QUIDLST::CallInfo (HWND hWnd, RECT *rect, char *Item, char *ItemValue, DWORD WinMode)
/**
Einstieg in Infosystem mit Artikel. Abgeleitet von Klasse Info.
**/
{
 		 char buffer [1024];
         BOOL ret;

         sprintf (buffer, "where a = 2");
         ret = Info::CallInfo (hWnd, rect, "a", buffer, WinMode); 
         SetListFocus ();
         return ret;
}


int QUIDLST::InfoProc (char **Item, char *Value, char *where)
/**
Spezielle Procedure fuer Info-System. Alter Ablauf, ist nicht mehr Aktiv 
**/
{

   *Item = "a";

   sprintf (where, "where a = 2");
   return 1;
}

void QUIDLST::CallInfoEx (void)
/*
Info wird aus Dialog aufgerufen.
*/

{
//   char where [512];
   
//   sprintf (where, "where a = %.0lf", prdk_k.a);
//   _CallInfoEx (hMainWindow, NULL, InfoItem, where, 0l); 
}


void QUIDLST::SetButtonSize (int Size)
{
    int fpos;

//    return;
    fpos = GetItemPos (&DataForm, "chtyp");
    if (fpos < 0) return;

    if (Size == SMALL)
    {
         DataForm.mask [fpos].length = 2;
         DataForm.mask [fpos].rows = 1;
    }
    else 
    {
         DataForm.mask [fpos].length = 3;
         DataForm.mask [fpos].rows = 0;
    }
    fpos = GetItemPos (&DataForm, "chnr");
    if (fpos < 0) return;

    if (Size == SMALL)
    {
         DataForm.mask [fpos].length = 2;
         DataForm.mask [fpos].rows = 1;
    }
    else 
    {
         DataForm.mask [fpos].length = 3;
         DataForm.mask [fpos].rows = 0;
    }
}


void QUIDLST::GetCfgValues (void)
{
       char cfg_v [512];
       COLORREF Color;

       if (ProgCfg == NULL) return;
	   if (cfgOK) return;

       cfgOK = TRUE;
       if (ProgCfg->GetCfgValue ("3DList", cfg_v) == TRUE)
       {
                    eListe.Set3D (atoi (cfg_v));
	   }
       if (ProgCfg->GetCfgValue ("3DSize", cfg_v) == TRUE)
       {
                    eListe.SetPlus3D (atoi (cfg_v));
	   }
       if (ProgCfg->GetCfgValue ("ListFocus", cfg_v) == TRUE)
       {
                    ListFocus = atoi (cfg_v);
	   }
        if (ProgCfg->GetCfgValue ("rowheight", cfg_v) == TRUE)
		{
			        RowHeight = ratod (cfg_v);
					if (RowHeight < (double) 1.0)
					{
						RowHeight = (double) 1.0;
					}
		}
        if (ProgCfg->GetCfgValue ("ubheight", cfg_v) == TRUE)
		{
			        UbHeight = atoi (cfg_v);
					SetUbHeight ();
		}
		eListe.SetRowHeight (RowHeight);


        if (ProgCfg->GetCfgValue ("NoRecNr", cfg_v) == TRUE)
        {
			        NoRecNr = atoi (cfg_v);
		            if (atoi (cfg_v))
					{
	                        SetNoRecNr ();
					}
		}
        if (ProgCfg->GetCfgValue ("ListColor", cfg_v) == TRUE)
        {
                     GetListColor (&Color, cfg_v);
		             eListe.SetColor (Color);
        }
        if (ProgCfg->GetCfgValue ("ListBkColor", cfg_v) == TRUE)
        {
                     GetListColor (&Color, cfg_v);
		             eListe.SetBkColor (Color);
        }
        if (ProgCfg->GetCfgValue ("ListLine", cfg_v) == TRUE)
        {
		             eListe.SetLines (GetListLine (cfg_v));
        }
        if (ProgCfg->GetCfgValue ("KostInfo", cfg_v) == TRUE)
        {
                     InfoItem = new char [80];
                     strcpy (InfoItem, cfg_v);
        }
}


void QUIDLST::TestRemoves (void)
{
        int fpos;

        fpos = GetItemPos (&DataForm, "feld3");
        if (fpos >= 0)
        {
            if (DataForm.mask[fpos].attribut & REMOVED)
            {
                DestroyListField ("feld3");
            }
        }
}


void QUIDLST::SetNoRecNr (void)
{
	   int i;

	   eListe.SetNoRecNr (TRUE);
	   if (NoRecNrSet) return;

	   for (i = 0; i < dataform->fieldanz; i ++)
	   {
		   dataform->mask[i].pos[1] -= 6;
	   }
	   for (i = 0; i < ubform->fieldanz; i ++)
	   {
		   ubform->mask[i].pos[1] -= 6;
	   }
	   for (i = 0; i < lineform->fieldanz; i ++)
	   {
		   lineform->mask[i].pos[1] -= 6;
	   }
       NoRecNrSet = TRUE;
}


QUIDLST::QUIDLST () : KEYS (), LISTENTER () 
{

    ProgCfg = new PROG_CFG ("23201");
	Dlg = NULL;
    cfgOK = FALSE;
	cursor = -1;
    LstTab = new QUIDLSTS [0x10000];

    dataform  = &DataForm;
    ubform    = &UbForm;
    lineform  = &LineForm;
    SetFieldAttr ("quid_kat", DISPLAYONLY);
    SetFieldAttr ("quid_nr", DISPLAYONLY);
    SetFieldAttr ("quid_bez", DISPLAYONLY);
    SetFieldAttr ("quid_proz", DISPLAYONLY);
    SetFieldLen ("prod_schritt", 25);
    eListe.SetUbRows (ubrows); 
	eListe.SetKeys (this);
    LISTENTER::GetCfgValues ();
    GetCfgValues ();
//    GetSysPar ();
    TestRemoves ();
    if (NoRecNr)
    {
	    SetNoRecNr ();
    }
	fChoise.Setchpos (0, 2);
    work = NULL;
    ChAttr = ChAttra;
    eListe.SetChAttr (ChAttr); 
    Toolbar2 = NULL;
    eListe.SetInfoProc (InfoProc);
    eListe.SetInfo (this);
    ArrDown = NULL;
    ArrDown = LoadBitmap (DLG::hInstance, "ARRDOWN");
    PlanEk = FALSE;
	eListe.SetPos (0, 0);
}


QUIDLST::~QUIDLST ()
{
	if (work != NULL)
	{
          work->SetListDlg (NULL);
	}
    fChoise.destroy ();
	if (LstTab != NULL)
	{
  	    delete []LstTab;
		LstTab = NULL;
	}
    if (ProgCfg != NULL)
    {
        ProgCfg->CloseCfg ();
        delete ProgCfg;
        ProgCfg = NULL;
    }
    if (work != NULL)
    {
        work->SetKostLsts (NULL);
    }
}


// Funktionen f�r den Zugriff auf die Daten. Sind als abstrakte Schnittstellen in
// listenter definiert.

unsigned char *QUIDLST::GetDataRec (void)
{
    return (unsigned char *) &Lsts;
}

int QUIDLST::GetDataRecLen (void)
{
    return sizeof (struct QUIDLSTS);
}

char *QUIDLST::GetListTab (int i)
{
    return (char *) &LstTab[i];
}

// Ende der Schnittstellen.


void QUIDLST::Setfeld2Removed (BOOL removed)
{
    if (removed)
    {
            SetFieldAttr ("feld2", REMOVED);
    }
    else
    {
            SetFieldAttr ("feld2", EDIT);
    }
}


void QUIDLST::Prepare (void)
{
    work->FillComboLong (ProdSchrittCombo, "prod_schritt", MAXCOMBO - 1);
    if (work != NULL)
    {
        cursor = work->PrepareQuid ();
    }
}

void QUIDLST::Open (void)
{
	if (cursor == -1)
	{
		Prepare ();
		if (cursor == -1) return;
	}
    if (work != NULL)
    {
        work->OpenQuid ();
    }
}

int QUIDLST::Fetch (void)
{
	if (cursor == -1)
	{
		Prepare ();
		if (cursor == -1) return 100;
	}
    if (work != NULL)
    {
        return work->FetchQuid ();
    }
	return 100;
}



void QUIDLST::ReportFill (void)
{

	/*
    for (int i = 0; i < eListe.GetRecanz (); i ++)
    {


	    reportrez.grouprez = 7;
	    reportrez.a = prdk_k.a;
	    strcpy(reportrez.a_bz1, prdk_kf.a_bz1);
	    strcpy(reportrez.rez ,prdk_k.rez);
//	    strcpy(reportrez.rez_bz, prdk_k.rez_bz);
	    strcpy(reportrez.rez_bz, LstTab[i].text);
	    reportrez.variante = prdk_k.variante;
	    strcpy(reportrez.variante_bz, prdk_k.variante_bz);
	    if (strcmp(prdk_kf.ek,"J") == 0)  
        {
			strcpy(reportrez.preis_basis, "EK");				
		}
		else
        {
			strcpy(reportrez.preis_basis, "PLAN-EK");			
		}
	    reportrez.schwund = prdk_k.sw_kalk;
	    reportrez.chargengewicht = prdk_k.chg_gew;
	    strcpy(reportrez.datum, "01.01.2003");                      //muss nocht !
	    reportrez.posi = LstTab[i].lfd;
	    reportrez.mat = LstTab[i].schritt;
	    strcpy(reportrez.mat_bz, "");
	    reportrez.menge = ratod (LstTab[i].su_zeit);
	    strcpy(reportrez.einheit, "");
	    reportrez.ek = ratod (LstTab[i].kosten);
	    reportrez.wert_ek = ratod (LstTab[i].zeit_fix);
	    reportrez.kosten = ratod (LstTab[i].kosten);
	    reportrez.bep = ratod (LstTab[i].zeit_spfix);
	    reportrez.wert_bep = ratod (LstTab[i].zeit_var);
	    strcpy(reportrez.k_a_bez, "");           
	    reportrez.k_diff = 0.0;
	    reportrez.kostenkg = ratod (LstTab[i].su_kost);
	    reportrez.kostenart = 0;
	    strncpy(reportrez.kostenart_bz, LstTab[i].prod_schritt,32);//Bezeichnung der Produktionsschritte
	    reportrez.mdn = prdk_k.mdn;
	    work->ReportRezUpdate ();
    }
	*/
}


void QUIDLST::Close (void)
{
	if (cursor != -1)
	{
		cursor = -1;
	}
    if (work != NULL)
    {
        work->CloseZeit ();
    }
	eListe.SetPos (0, 0);
	for (int i = 0; i < eListe.GetRecanz (); i ++)
	{
		memcpy (&Lsts, &LstTab[i], sizeof (Lsts));
		eListe.SetPos (i, 0);
		CalcSum ();
	}
//	CalcPersKosten ();
	memcpy (&Lsts, &LstTab[0], sizeof (Lsts));
}

BOOL QUIDLST::BeforeCol (char *colname)
{
         return FALSE;
}
         
void QUIDLST::FillProdSchritt (int pos)
{
	/*
          clipped (Lsts.prod_schritt);

          Lsts.schritt = 1;
          for (int i = 0; ProdSchrittCombo[i] != NULL; i ++)
          {
              if (strcmp ((Lsts.prod_schritt), clipped (ProdSchrittCombo [i])) == 0)
              {
                  Lsts.schritt = i + 1;
                  break;
              }
          }
          memcpy (&LstTab[pos], &Lsts, sizeof (Lsts));
		  */
}

BOOL QUIDLST::AfterCol (char *colname)
{
         
         if (strcmp (colname, "prod_schritt") == 0)
         {
             FillProdSchritt (eListe.GetAktRow ());
             return FALSE;
         }
		 else if (strcmp (colname, "text") == 0)
		 {
		 }
		 else
		 {
			 CalcSum ();
             memcpy (&LstTab [eListe.GetAktRow ()], &Lsts, sizeof (Lsts));
          	 eListe.ShowAktRow ();
		 }
         return FALSE;
}

int QUIDLST::AfterRow (int Row)
{

         if (syskey == KEYUP && ratod (Lsts.quid_kat) == 0.0)
         {
                return OnKey7 ();
         }
         memcpy (&LstTab [Row], &Lsts, sizeof (Lsts));
         work->SetQuidLsts (&Lsts);
         CalcSum ();
         return 1;
}

int QUIDLST::BeforeRow (int Row)
{
//         memcpy (&LstTab [Row], &Lsts, sizeof (Lsts));
         return 1;
}

BOOL QUIDLST::StopList (void)
{
    int i;
    int recanz;
    double chg_gew = 0.0;

    if (AfterRow (eListe.GetAktRow ()) == -1)
    {
        return TRUE;
    }

    if (ratod (Lsts.quid_kat) == 0)
    {
        OnKey7 ();
    }

    else
    {
    }

    recanz = eListe.GetRecanz ();

    work->DeleteAllZeitPos ();
    for (i = 0; i < recanz; i ++)
    {
        WriteRec (i);
    }
    return TRUE;
}


BOOL QUIDLST::OnKeyNext (void)
{
        if (GetKeyState (VK_CONTROL) >= 0)
        {
              if (Dlg != NULL)
              {
                  ((SpezDlg *) Dlg)->NextPage ();
                  return TRUE;
              }
        }
        return FALSE;
}


BOOL QUIDLST::OnKeyPrior (void)
{
        if (GetKeyState (VK_CONTROL) >= 0)
        {
              if (Dlg != NULL)
              {
                  ((SpezDlg *) Dlg)->PriorPage ();
                  return TRUE;
              }
        }
        return FALSE;
}

BOOL QUIDLST::OnKey5 (void)
{

    OnKey12 ();
    syskey = KEY5;
    return TRUE;

}

BOOL QUIDLST::OnKey6 (void)
{
    eListe.InsertLine ();
    return TRUE;
}

BOOL QUIDLST::OnKeyInsert (void)
{
    return FALSE;
/*
    eListe.InsertLine ();
    return TRUE;
*/
}

BOOL QUIDLST::OnKey7 (void)
{
    eListe.DeleteLine ();
    CalcSum ();
    return TRUE;
}

BOOL QUIDLST::OnKeyDel (void)
{
    return FALSE;
/*
    eListe.DeleteLine ();
    CalcSum ();
    return TRUE;
*/
}

BOOL QUIDLST::OnKey8 (void)
{
         return FALSE;
}


BOOL QUIDLST::OnKey9 (void)
{
    char * Item = eListe.GetAktItem ();
    if (strcmp (Item, "prod_schritt") == 0)
    {
        eListe.ShowDropDown ();
        return TRUE;
    }
/*
    else if (strcmp (Item, "kost_art") == 0)
    {
        ShowKostArt ();
        return TRUE;
    }
*/
    return FALSE;
}

BOOL QUIDLST::InFirstField ()
{
	if (eListe,GetAktRow () > 0)
	{
		return FALSE;
	}
	if (eListe.GetAktColumn () > 1)
	{
		return FALSE;
	}
	if (eListe.GetAktColumn () == 1 &&
		eListe.GetDataForm ()->mask[0].attribut != DISPLAYONLY)
	{
		return FALSE;
	}
	return TRUE;
}



BOOL QUIDLST::OnKey10 (void)
{
    if (Dlg != NULL)
    {
         work->SetQuidLsts (&Lsts);
// Hier Workfunktion einsetzen
         SetListFocus ();
         return TRUE;
	}
    return FALSE;
}

BOOL QUIDLST::OnKey11 (void)
{

    if (Dlg != NULL)
    {
        ((SpezDlg *) Dlg)->OnKey11 ();
        SetAktFocus ();
    }
    return FALSE;
}


int QUIDLST::ToMemory (int i)
{

    LISTENTER::ToMemory (i);
    return 0;
}


void QUIDLST::InitRow (void)
{

    LISTENTER::InitRow ();


    work->SetQuidLsts (&Lsts);
    work->InitZeitRow ();
	FillProdSchritt (eListe.GetAktRow ());

/*
    if (ArrDown != NULL && Lsts.chtyp[0] != NULL)
    {
        Lsts.chtyp[0]->SetBitMap (ArrDown);
    }
*/


    if (ArrDown != NULL)
    {
        Lsts.chnr.SetBitMap (ArrDown);
    }


}

void QUIDLST::uebertragen (void)
{
    work->SetQuidLsts (&Lsts);
    work->FillQuidRow ();
    if (ArrDown != NULL)
    {
        Lsts.chtyp.SetBitMap (ArrDown);
    }
    if (ArrDown != NULL)
    {
        Lsts.chnr.SetBitMap (ArrDown);
    }
}

void QUIDLST::WriteRec (int i)
{
    memcpy (&Lsts, &LstTab[i], sizeof (Lsts));
    if (ratod (Lsts.quid_kat) == 0)
    {
        return;
    }
    FillProdSchritt (i);
    work->SetQuidLsts (&LstTab[i]);
//    work->WriteZeitRec (i + 1);
}

BOOL QUIDLST::OnKey12 (void)
{
	if (sys_ben.berecht >= 3) return TRUE;
    StopList ();
    syskey = KEY12;
    return TRUE;
}


BOOL QUIDLST::OnKeyUp (void)
{
//    form *DataForm;
//    int fpos;
/*
    if (Leere Zeile)
    {
        return OnKeyDel ();
    }
*/

    return FALSE;
}

BOOL QUIDLST::OnKeyDown (void)
{
    if (ratod (Lsts.quid_kat) == 0.0)
    {
        return TRUE;
    }

    return FALSE;
}


void QUIDLST::CreateListQuikInfos (HWND hWnd)
{
       if (Toolbar2 == NULL) return;

       Toolbar2->GetCfield ("f5")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("f12")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("f9")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("del")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("insert")->CreateQuikInfos (hWnd);
}


BOOL QUIDLST::OnCommand (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    if (LOWORD (wParam) == IDM_CHOISE)
    {
        return OnKey9 ();
    }
    if (LOWORD (wParam) == IDM_CANCEL)
    {
        return OnKey5 ();
    }
    if (LOWORD (wParam) == IDM_WRITE)
    {
        return OnKey12 ();
    }
    if (LOWORD (wParam) == VK_DELETE)
    {
        return OnKeyDel ();
    }
    if (LOWORD (wParam) == IDM_DELETE)
    {
        return OnKey7 ();
    }
    if (LOWORD (wParam) == VK_INSERT)
    {
        return OnKeyInsert ();
    }
    if (LOWORD (wParam) == IDM_INSERT)
    {
        return OnKey6 ();
    }
    if (HIWORD (wParam)  == CBN_CLOSEUP)
	{
		int AktCol = eListe.GetAktColumn ();
		eListe.SetPos (eListe.GetAktRow (), 0);
        eListe.GetFocusText ();
		eListe.SetPos (eListe.GetAktRow (),AktCol);
        FillProdSchritt (eListe.GetAktRow ());
        if (Dlg != NULL)
        {
	           return ((SpezDlg *) Dlg)->OnCloseUp (hWnd, msg, wParam, lParam);
        }
	}
    return FALSE;
}

BOOL QUIDLST::OnPaint (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    HDC hdc;

    if (Modal == FALSE) return FALSE;
    if (this->mamain1 != hWnd) return FALSE; 
    
    hdc = BeginPaint (hWnd, &ps);
    fChoise.display (hWnd, hdc);
    EndPaint (hWnd,&ps);
    return FALSE;
}

BOOL QUIDLST::OnMove (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{

    if (Modal == FALSE) return FALSE;
    if (this->mamain1 != hWnd) return FALSE; 

    fChoise.MoveWindow ();
    return FALSE;
}








