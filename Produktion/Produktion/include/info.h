#ifndef _INFO_DEF
#define _INFO_DEF
#include "inflib.h"

class Info 
{
   public :
     Info ()
     {
     }
     ~Info ()
     {
     }
     virtual BOOL CallInfo (HWND hWnd, RECT *rect, char *Item, char *ItemValue, DWORD WinMode)
     {
         return _CallInfoEx (hWnd, rect, Item, ItemValue, WinMode);
     }
};
#endif  