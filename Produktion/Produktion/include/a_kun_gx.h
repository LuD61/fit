#ifndef A_KUNGXDEF
#define A_KUNGXDEF
#include "dbclass.h"

#include "kun.h"
#include "dbclass.h"

struct A_KUN_GX {
   short     mdn;
   short     fil;
   long      kun;
   double    a;
   char      a_kun[13];
   char      a_bz1[25];
   short     me_einh_kun;
   double    inh;
   char      kun_bran2[3];
   double    tara;
   double    ean;
   char      a_bz2[25];
   short     hbk_ztr;
   long      kopf_text;
   char      pr_rech_kz[2];
   char      modif[2];
   long      text_nr;
   short     devise;
   char      geb_eti[2];
   short     geb_fill;
   long      geb_anz;
   char      pal_eti[2];
   short     pal_fill;
   long      pal_anz;
   char      pos_eti[2];
   short     sg1;
   short     sg2;
   short     pos_fill;
   short     ausz_art;
   long      text_nr2;
   short     cab;
   char      a_bz3[25];
   char      a_bz4[25];
   short     eti_typ;
   long      mhd_text;
   long      freitext1;
   long      freitext2;
   long      freitext3;
   short     sg3;
   short     eti_sum1;
   short     eti_sum2;
   short     eti_sum3;
};
extern struct A_KUN_GX a_kun_gx, a_kun_gx_null;

#line 9 "a_kun_gx.rh"

class A_KUN_GX_CLASS : public KUN_CLASS 
{
       private :
               short cursor_bra;
               void prepare (void);
               int prep_awcursor (void);
               int prep_awcursor (char *);
               void fill_aw (int);

       public :
               A_KUN_GX_CLASS () : KUN_CLASS ()
               {
                       cursor_bra = -1;
               }
               int dbreadfirst (void);
               int dbreadfirst_bra(void);
               int dbread_bra (void);
               int ShowAllBu (HWND hWnd, int ws_flag);
               int ShowBuQuery (HWND hWnd, int ws_flag, char *);
               int QueryBu (HWND hWnd, int ws_flag);
};
#endif
