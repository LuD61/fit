#ifndef _VERBR_TAG_DEF
#define _VERBR_TAG_DEF

struct VERBR_TAG {
   short     mdn;
   char      dat[12];
   double    a;
   double    me_ist;
   double    me_istist;
   double    anz_einh;
   double    zerl_me;
   short     me_einh;
   double    mat_o_b;
   double    hk_vollk;
   double    hk_teilk;
   short     typ;
   short     prdk_stufe;
   double     artikel;
   char      bearb_info[81];
   char		 chargennr[14];       //ist nicht in Tabelle 
};
extern struct VERBR_TAG verbr_tag, verbr_tag_null;


class VERBR_TAG_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int dbcount (void);
               int leseverbr_tag (int);
               int openverbr_tag (void);
               VERBR_TAG_CLASS () : DB_CLASS ()
               {
               }
};
#endif
