// DialogDatum.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "llmfc.h"
#include "DialogDatum.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
extern char datvon[12] ;
extern char datbis[12] ;
bool Datumok = TRUE;

CForm *CDialogDatum::_Dialog [] = 
{
   new CForm (IDC_Datvon, (double *)      &datvon, FRMCHAR, "%s"),
   new CForm (IDC_Datbis, (double *) &datbis, FRMCHAR, "%s"),
   NULL,
};

CVector CDialogDatum::Dialog ((void **) _Dialog);


/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDialogDatum 


CDialogDatum::CDialogDatum(CWnd* pParent /*=NULL*/)
	: CDialog(CDialogDatum::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDialogDatum)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT
}

BOOL CDialogDatum::OnInitDialog()
{
	CDialog::OnInitDialog();
    GetDlgItem (IDC_Datvon)->SetFocus ();
    ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
	Read ();

	return TRUE;
}

void CDialogDatum::EnableDialog (BOOL b)
{


    CForm *Form;

    Dialog.FirstPosition ();
    while ((Form = (CForm *) Dialog.GetNext ()) != NULL)
    {
        Form->Enable (this, b);
    }
	if (b)
	{
	    GetDlgItem (IDC_Datvon)->SetFocus ();
	    ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
    }
}
void CDialogDatum::ToDialog (void)
{
    CForm *Form;

    Dialog.FirstPosition ();
    while ((Form = (CForm *) Dialog.GetNext ()) != NULL)
    {
        Form->ToScreen (this);
    }
}


void CDialogDatum::FromDialog (void)
{
    CForm *Form;

    Dialog.FirstPosition ();

    while ((Form = (CForm *) Dialog.GetNext ()) != NULL)
    {
        Form->FromScreen (this);
    }
}

BOOL CDialogDatum::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Speziellen Code hier einf�gen und/oder Basisklasse aufrufen
CWnd *cWnd;
if (pMsg->message == WM_KEYDOWN)
{
	switch (pMsg->wParam)
	{
	case VK_RETURN : 
	case VK_TAB :
		    cWnd = GetFocus ();
			if (cWnd ==GetDlgItem (IDC_Datvon))
			{
			    Write ();
			    GetDlgItem (IDC_Datbis)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}
			if (cWnd ==GetDlgItem (IDC_Datbis))
			{
			    Write ();
			    GetDlgItem (IDC_Datvon)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}
			if (cWnd ==GetDlgItem (IDCANCEL))
			{
				return CDialog::PreTranslateMessage(pMsg);
			}
			if (cWnd ==GetDlgItem (IDOK))
			{
			    Write ();
				if (Datumok == FALSE)
                { 
					GetDlgItem (IDC_Datvon)->SetFocus ();
					((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
					return TRUE;
                }
				return CDialog::PreTranslateMessage(pMsg);
			}
		    NextDlgCtrl();
		    return TRUE;

	}
}
	return CDialog::PreTranslateMessage(pMsg);
}




BOOL CDialogDatum::Read (void)
{
    ToDialog ();
    EnableDialog (TRUE);
	return TRUE;
	
}

BOOL CDialogDatum::Write (void)
{

    FromDialog ();
return TRUE;
}

void CDialogDatum::OnOK() 
{
	// TODO: Zus�tzliche Pr�fung hier einf�gen
	Write();
	CDialog::OnOK();
}


void CDialogDatum::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDialogDatum)
		// HINWEIS: Der Klassen-Assistent f�gt hier DDX- und DDV-Aufrufe ein
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDialogDatum, CDialog)
	//{{AFX_MSG_MAP(CDialogDatum)
	ON_EN_CHANGE(IDC_Datbis, OnChangeDatbis)
	ON_EN_CHANGE(IDC_Datvon, OnChangeDatvon)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDialogDatum 

void CDialogDatum::OnChangeDatbis() 
{
	// TODO: Wenn es sich hierbei um ein RICHEDIT-Steuerelement handelt, sendet es
	// sendet diese Benachrichtigung nur, wenn die Funktion CDialog::OnInitDialog()
	// �berschrieben wird, um die EM_SETEVENTMASK-Nachricht an das Steuerelement
	// mit dem ENM_CHANGE-Attribut ORed in die Maske lParam zu senden.
	
	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen
	
}

void CDialogDatum::OnChangeDatvon() 
{
	// TODO: Wenn es sich hierbei um ein RICHEDIT-Steuerelement handelt, sendet es
	// sendet diese Benachrichtigung nur, wenn die Funktion CDialog::OnInitDialog()
	// �berschrieben wird, um die EM_SETEVENTMASK-Nachricht an das Steuerelement
	// mit dem ENM_CHANGE-Attribut ORed in die Maske lParam zu senden.
	
	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen
	
}

void CDialogDatum::OnCloseupCombo1() 
{
	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen
	
}
