/**** C/C++ constants and function definitions for L9S.DLL ****/
/****  (c) 1991,92,93,94,95,96,97,98,99,2000,01,02... combit GmbH, Konstanz, Germany  ****/
/****  [build of 2002-11-04 15:11:08] ****/

#ifndef _L9S_H /* include header only once */
#define _L9S_H

#if !defined(WIN32)
  #define WIN32 1
#endif


#ifndef EXPLICIT_TYPES
  #define EXPLICIT_TYPES
  #ifndef INT
    typedef int INT; /* you should comment this out if you have any problems with INT */
  #endif
  #ifndef CHAR
    typedef char CHAR; /* you should comment this out if you have any problems with CHAR */
  #endif
  typedef unsigned char UINT8;
  typedef unsigned short UINT16;
  typedef signed char INT8;
  typedef signed short INT16;
  #ifndef _BASETSD_H_ /* MSVC6 defines these itself in basetsd.h */
    typedef unsigned long UINT32;
    typedef signed long INT32;
  #endif
#endif

#if defined(WIN32) && (!defined(__BORLANDC__) || __BORLANDC__ >= 0x450) && !defined(RC_INVOKED)
 #include <tchar.h>    /* for wide char support - comment this out if you have no 'tchar.h' */
#endif

#ifndef DLLPROC
  #define DLLPROC WINAPI
#endif
#ifndef DLLCPROC
  #ifdef WIN32
    #define DLLCPROC WINAPIV
   #else
    #define DLLCPROC _far _cdecl
  #endif
#endif

#if !defined(_L9S_) && defined(WIN32)
  #define WINL9SAPI DECLSPEC_IMPORT
#else
  #define WINL9SAPI
#endif

#ifndef CMBTLANG_DEFAULT
 #define CMBTLANG_DEFAULT    -1
 #define CMBTLANG_GERMAN      0
 #define CMBTLANG_ENGLISH     1
 #define CMBTLANG_ARABIC      2
 #define CMBTLANG_AFRIKAANS   3
 #define CMBTLANG_ALBANIAN    4
 #define CMBTLANG_BASQUE      5
 #define CMBTLANG_BULGARIAN   6
 #define CMBTLANG_BYELORUSSIAN 7
 #define CMBTLANG_CATALAN     8
 #define CMBTLANG_CHINESE     9
 #define CMBTLANG_CROATIAN    10
 #define CMBTLANG_CZECH       11
 #define CMBTLANG_DANISH      12
 #define CMBTLANG_DUTCH       13
 #define CMBTLANG_ESTONIAN    14
 #define CMBTLANG_FAEROESE    15
 #define CMBTLANG_FARSI       16
 #define CMBTLANG_FINNISH     17
 #define CMBTLANG_FRENCH      18
 #define CMBTLANG_GREEK       19
 #define CMBTLANG_HEBREW      20
 #define CMBTLANG_HUNGARIAN   21
 #define CMBTLANG_ICELANDIC   22
 #define CMBTLANG_INDONESIAN  23
 #define CMBTLANG_ITALIAN     24
 #define CMBTLANG_JAPANESE    25
 #define CMBTLANG_KOREAN      26
 #define CMBTLANG_LATVIAN     27
 #define CMBTLANG_LITHUANIAN  28
 #define CMBTLANG_NORWEGIAN   29
 #define CMBTLANG_POLISH      30
 #define CMBTLANG_PORTUGUESE  31
 #define CMBTLANG_ROMANIAN    32
 #define CMBTLANG_RUSSIAN     33
 #define CMBTLANG_SLOVAK      34
 #define CMBTLANG_SLOVENIAN   35
 #define CMBTLANG_SERBIAN     36
 #define CMBTLANG_SPANISH     37
 #define CMBTLANG_SWEDISH     38
 #define CMBTLANG_THAI        39
 #define CMBTLANG_TURKISH     40
 #define CMBTLANG_UKRAINIAN   41
 #define CMBTLANG_CHINESE_TRADITIONAL   48
#endif

/*--- type declarations ---*/

#ifndef LPCTSTR
  #define LPCTSTR                        const TCHAR FAR *
#endif

#ifndef _PCRECT
  #define _PCRECT                        const RECT FAR *
#endif

#ifndef HLLSTG
  #define HLLSTG                         UINT32
#endif

#ifndef HSTG
  #define HSTG                           UINT32
#endif

#ifndef PHGLOBAL
  #define PHGLOBAL                       HGLOBAL FAR *
#endif


/*--- constant declarations ---*/

#define LL_ERR_BAD_JOBHANDLE           (-1)                 /* bad jobhandle */
#define LL_ERR_TASK_ACTIVE             (-2)                 /* LlDefineLayout() only once in a job */
#define LL_ERR_BAD_OBJECTTYPE          (-3)                 /* nObjType must be one of the allowed values (obsolete constant) */
#define LL_ERR_BAD_PROJECTTYPE         (-3)                 /* nObjType must be one of the allowed values */
#define LL_ERR_PRINTING_JOB            (-4)                 /* print job not opened, no print object */
#define LL_ERR_NO_BOX                  (-5)                 /* LlPrintSetBoxText(...) called when no abort box exists! */
#define LL_ERR_ALREADY_PRINTING        (-6)                 /* LlPrintWithBoxStart(...): another print job is being done, please wait or try LlPrintStart(...) */
#define LL_ERR_NO_PROJECT              (-10)                /* object with requested name does not exist (former ERR_NO_OBJECT) */
#define LL_ERR_NO_PRINTER              (-11)                /* printer couldn't be opened */
#define LL_ERR_PRINTING                (-12)                /* error while printing */
#define LL_ERR_ONLY_ONE_JOB            (-13)                /* only one job per application possible */
#define LL_ERR_NEEDS_VB                (-14)                /* '11...' needs VB.EXE */
#define LL_ERR_BAD_PRINTER             (-15)                /* PrintOptionsDialog(): no printer available */
#define LL_ERR_NO_PREVIEWMODE          (-16)                /* Preview functions: not in preview mode */
#define LL_ERR_NO_PREVIEWFILES         (-17)                /* PreviewDisplay(): no file found */
#define LL_ERR_PARAMETER               (-18)                /* bad parameter (usually NULL pointer) */
#define LL_ERR_BAD_EXPRESSION          (-19)                /* bad expression in LlExprEvaluate() and LlExprType() */
#define LL_ERR_BAD_EXPRMODE            (-20)                /* bad expression mode (LlSetExpressionMode()) */
#define LL_ERR_NO_TABLE                (-21)                /* no table object found to put data in */
#define LL_ERR_CFGNOTFOUND             (-22)                /* on LlPrintStart(), LlPrintWithBoxStart() [not found] */
#define LL_ERR_EXPRESSION              (-23)                /* on LlPrintStart(), LlPrintWithBoxStart() */
#define LL_ERR_CFGBADFILE              (-24)                /* on LlPrintStart(), LlPrintWithBoxStart() [read error, bad format] */
#define LL_ERR_BADOBJNAME              (-25)                /* on LlPrintEnableObject() - not a ':' at the beginning */
#define LL_ERR_NOOBJECT                (-26)                /* on LlPrintEnableObject() - "*" and no object in project */
#define LL_ERR_UNKNOWNOBJECT           (-27)                /* on LlPrintEnableObject() - object with that name not existing */
#define LL_ERR_NO_TABLEOBJECT          (-28)                /* LlPrint...Start() and no list in Project, or: */
#define LL_ERR_NO_OBJECT               (-29)                /* LlPrint...Start() and no object in project */
#define LL_ERR_NO_TEXTOBJECT           (-30)                /* LlPrintGetTextCharsPrinted() and no printable text in Project! */
#define LL_ERR_UNKNOWN                 (-31)                /* LlPrintIsVariableUsed(), LlPrintIsFieldUsed() */
#define LL_ERR_BAD_MODE                (-32)                /* LlPrintFields(), LlPrintIsFieldUsed() called on non-OBJECT_LIST */
#define LL_ERR_CFGBADMODE              (-33)                /* on LlDefineLayout(), LlPrint...Start(): file is in wrong expression mode */
#define LL_ERR_ONLYWITHONETABLE        (-34)                /* on LlDefinePageSeparation(), LlDefineGrouping() */
#define LL_ERR_UNKNOWNVARIABLE         (-35)                /* on LlGetVariableContents() */
#define LL_ERR_UNKNOWNFIELD            (-36)                /* on LlGetFieldContents() */
#define LL_ERR_UNKNOWNSORTORDER        (-37)                /* on LlGetFieldContents() */
#define LL_ERR_NOPRINTERCFG            (-38)                /* on LlPrintCopyPrinterConfiguration() - no or bad file */
#define LL_ERR_SAVEPRINTERCFG          (-39)                /* on LlPrintCopyPrinterConfiguration() - file could not be saved */
#define LL_ERR_RESERVED                (-40)                /* function not yet implemeted */
#define LL_ERR_BUFFERTOOSMALL          (-44)                /* LlXXGetOptionStr() */
#define LL_ERR_USER_ABORTED            (-99)                /* user aborted printing */
#define LL_ERR_BAD_DLLS                (-100)               /* DLLs not up to date (CTL, DWG, UTIL) */
#define LL_ERR_NO_LANG_DLL             (-101)               /* no or out-of-date language resource DLL */
#define LL_ERR_NO_MEMORY               (-102)               /* out of memory */
#define LL_BOXTYPE_NORMALMETER         (0)                 
#define LL_BOXTYPE_BRIDGEMETER         (1)                 
#define LL_BOXTYPE_NORMALWAIT          (2)                 
#define LL_BOXTYPE_BRIDGEWAIT          (3)                 
#define LL_UNITS_MM_DIV_10             (0)                 
#define LL_UNITS_INCH_DIV_100          (1)                 
#define LL_STG_COMPAT4                 (0)                 
#define LL_STG_STORAGE                 (1)                 
#define LL_ERR_STG_NOSTORAGE           (-1000)             
#define LL_ERR_STG_BADVERSION          (-1001)             
#define LL_ERR_STG_READ                (-1002)             
#define LL_ERR_STG_WRITE               (-1003)             
#define LL_ERR_STG_UNKNOWNSYSTEM       (-1004)             
#define LL_ERR_STG_BADHANDLE           (-1005)             
#define LL_ERR_STG_ENDOFLIST           (-1006)             
#define LL_ERR_STG_BADJOB              (-1007)             
#define LL_ERR_STG_ACCESSDENIED        (-1008)             
#define LL_ERR_STG_BADSTORAGE          (-1009)             
#define LL_ERR_STG_CANNOTGETMETAFILE   (-1010)             
#define LL_WRN_STG_UNFAXED_PAGES       (-1100)             
#define LL_STGSYS_OPTION_HAS16BITPAGES (200)                /* has job 16 bit pages? */
#define LL_STGSYS_OPTION_BOXTYPE       (201)                /* wait meter box type */
#define LL_STGSYS_OPTION_UNITS         (203)                /* LL_UNITS_INCH_DIV_100 or LL_UNITS_MM_DIV_10 */
#define LL_STGSYS_OPTION_PRINTERCOUNT  (204)                /* number of printers (1 or 2) */
#define LL_STGSYS_OPTION_ISSTORAGE     (205)                /* returns whether file is STORAGE or COMPAT4 */
#define LL_STGSYS_OPTION_EMFRESOLUTION (206)                /* EMFRESOLUTION used to print the file */
#define LL_STGSYS_OPTION_JOB           (207)                /* returns current job number */
#define LL_STGSYS_OPTION_TOTALPAGES    (208)                /* differs from GetPageCount() if print range in effect */
#define LL_STGSYS_OPTION_PAGESWITHFAXNUMBER (209)                /* NYI */
#define LL_STGSYS_OPTION_PAGENUMBER    (0)                  /* page number of current page */
#define LL_STGSYS_OPTION_COPIES        (1)                  /* number of copies (same for all pages at the moment) */
#define LL_STGSYS_OPTION_PRN_ORIENTATION (2)                  /* orientation (DMORIENT_LANDSCAPE, DMORIENT_PORTRAIT) */
#define LL_STGSYS_OPTION_PHYSPAGE      (3)                  /* is page "physical page" oriented? */
#define LL_STGSYS_OPTION_PRN_PIXELSOFFSET_X (4)                  /* this and the following values are */
#define LL_STGSYS_OPTION_PRN_PIXELSOFFSET_Y (5)                  /* values of the printer that the preview was */
#define LL_STGSYS_OPTION_PRN_PIXELS_X  (6)                  /* created on! */
#define LL_STGSYS_OPTION_PRN_PIXELS_Y  (7)                 
#define LL_STGSYS_OPTION_PRN_PIXELSPHYSICAL_X (8)                 
#define LL_STGSYS_OPTION_PRN_PIXELSPHYSICAL_Y (9)                 
#define LL_STGSYS_OPTION_PRN_PIXELSPERINCH_X (10)                
#define LL_STGSYS_OPTION_PRN_PIXELSPERINCH_Y (11)                
#define LL_STGSYS_OPTION_PRN_INDEX     (12)                 /* printer index of the page (0/1) */
#define LL_STGSYS_OPTION_PRN_PAPERTYPE (13)                
#define LL_STGSYS_OPTION_PRN_PAPERSIZE_X (14)                
#define LL_STGSYS_OPTION_PRN_PAPERSIZE_Y (15)                
#define LL_STGSYS_OPTION_PRN_FORCE_PAPERSIZE (16)                
#define LL_STGSYS_OPTION_PROJECTNAME   (100)                /* name of the original project (not page dependent) */
#define LL_STGSYS_OPTION_JOBNAME       (101)                /* name of the job (WindowTitle of LlPrintWithBoxStart()) (not page dependent) */
#define LL_STGSYS_OPTION_PRTNAME       (102)                /* printer name ("HP Laserjet 4L") */
#define LL_STGSYS_OPTION_PRTDEVICE     (103)                /* printer device ("PSCRIPT") */
#define LL_STGSYS_OPTION_PRTPORT       (104)                /* printer port ("LPT1:" or "\\server\printer") */
#define LL_STGSYS_OPTION_USER          (105)                /* user string (not page dependent) */
#define LL_STGSYS_OPTION_CREATION      (106)                /* creation date (not page dependent) */
#define LL_STGSYS_OPTION_CREATIONAPP   (107)                /* creation application (not page dependent) */
#define LL_STGSYS_OPTION_CREATIONDLL   (108)                /* creation DLL (not page dependent) */
#define LL_STGSYS_OPTION_CREATIONUSER  (109)                /* creation user and computer name (not page dependent) */
#define LL_STGSYS_OPTION_FAXPARA_QUEUE (110)                /* NYI */
#define LL_STGSYS_OPTION_FAXPARA_RECIPNAME (111)                /* NYI */
#define LL_STGSYS_OPTION_FAXPARA_RECIPNUMBER (112)                /* NYI */
#define LL_STGSYS_OPTION_FAXPARA_SENDERNAME (113)                /* NYI */
#define LL_STGSYS_OPTION_FAXPARA_SENDERCOMPANY (114)                /* NYI */
#define LL_STGSYS_OPTION_FAXPARA_SENDERDEPT (115)                /* NYI */
#define LL_STGSYS_OPTION_FAXPARA_SENDERBILLINGCODE (116)                /* NYI */
#define LL_STGSYS_OPTION_FAXPARA_DREMAILADDRESS (117)                /* NYI */
#define LL_STGSYS_OPTION_FAX_AVAILABLEQUEUES (118)                /* NYI, nPageIndex=1 */
#define LL_STGSYS_OPTION_PRINTERALIASLIST (119)                /* alternative printer list (taken from project) */
#define LL_STGSYSPRINTFLAG_FIT         (0x00000001)        
#define LL_STGSYSPRINTFLAG_STACKEDCOPIES (0x00000002)         /* n times page1, n times page2, ... (else n times (page 1...x)) */
#define LL_STGSYSPRINTFLAG_TRYPRINTERCOPIES (0x00000004)         /* first try printer copies, then simulated ones... */
#define LL_STGSYSPRINTFLAG_METER       (0x00000010)        
#define LL_STGSYSPRINTFLAG_ABORTABLEMETER (0x00000020)        
#define LL_STGSYSPRINTFLAG_METERMASK   (0x00000070)         /* allows 7 styles of abort boxes... */

/*--- function declaration ---*/

#ifndef RC_INVOKED

#ifdef __ZTC__ /* Zortech C++ */
#pragma ZTC align 1
#elif __WATCOMC__ > 1000 /* Watcom C++ >= 10.5 */
#pragma pack(push,1)
#elif __BORLANDC__ /* Borland C++ */
#pragma option -a1
#else
#pragma pack(1) /* MS, Watcom <= 10.0, ... */
#endif

#ifdef __cplusplus
extern "C" {
#endif

#ifdef WIN16
WINL9SAPI HLLSTG   DLLPROC  LlStgsysStorageOpen
	(
	LPCSTR               lpszFilename,
	LPCSTR               pszTempPath,
	BOOL                 bReadOnly,
	BOOL                 bOneJobTranslation
	);
#endif // WIN16

#if defined(WIN32) && !defined(_NO_CMBTL9SAPIDEFINES)
   #ifdef UNICODE
       #define LlStgsysStorageOpen LlStgsysStorageOpenW
       #define LlStgsysStorageOpenO LlStgsysStorageOpenA
     #else  // not UNICODE
       #define LlStgsysStorageOpen LlStgsysStorageOpenA
       #define LlStgsysStorageOpenO LlStgsysStorageOpenW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9SAPIDEFINES
#ifdef WIN32
WINL9SAPI HLLSTG   DLLPROC  LlStgsysStorageOpenA
	(
	LPCSTR               lpszFilename,
	LPCSTR               pszTempPath,
	BOOL                 bReadOnly,
	BOOL                 bOneJobTranslation
	);
#endif // WIN32

#ifdef WIN32
WINL9SAPI HLLSTG   DLLPROC  LlStgsysStorageOpenW
	(
	LPCWSTR              lpszFilename,
	LPCWSTR              pszTempPath,
	BOOL                 bReadOnly,
	BOOL                 bOneJobTranslation
	);
#endif // WIN32

WINL9SAPI void     DLLPROC  LlStgsysStorageClose
	(
	HLLSTG               hStg
	);

WINL9SAPI INT      DLLPROC  LlStgsysGetAPIVersion
	(
	HLLSTG               hStg
	);

WINL9SAPI INT      DLLPROC  LlStgsysGetFileVersion
	(
	HLLSTG               hStg
	);

#ifdef WIN16
WINL9SAPI INT      DLLPROC  LlStgsysGetFilename
	(
	HLLSTG               hStg,
	INT                  nJob,
	INT                  nFile,
	LPSTR                pszBuffer,
	UINT                 nBufSize
	);
#endif // WIN16

#if defined(WIN32) && !defined(_NO_CMBTL9SAPIDEFINES)
   #ifdef UNICODE
       #define LlStgsysGetFilename LlStgsysGetFilenameW
       #define LlStgsysGetFilenameO LlStgsysGetFilenameA
     #else  // not UNICODE
       #define LlStgsysGetFilename LlStgsysGetFilenameA
       #define LlStgsysGetFilenameO LlStgsysGetFilenameW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9SAPIDEFINES
#ifdef WIN32
WINL9SAPI INT      DLLPROC  LlStgsysGetFilenameA
	(
	HLLSTG               hStg,
	INT                  nJob,
	INT                  nFile,
	LPSTR                pszBuffer,
	UINT                 nBufSize
	);
#endif // WIN32

#ifdef WIN32
WINL9SAPI INT      DLLPROC  LlStgsysGetFilenameW
	(
	HLLSTG               hStg,
	INT                  nJob,
	INT                  nFile,
	LPWSTR               pszBuffer,
	UINT                 nBufSize
	);
#endif // WIN32

WINL9SAPI INT      DLLPROC  LlStgsysGetJobCount
	(
	HLLSTG               hStg
	);

WINL9SAPI INT      DLLPROC  LlStgsysSetJob
	(
	HLLSTG               hStg,
	INT                  nJob
	);

WINL9SAPI INT      DLLPROC  LlStgsysGetPageCount
	(
	HLLSTG               hStg
	);

WINL9SAPI INT      DLLPROC  LlStgsysGetJobOptionValue
	(
	HLLSTG               hStg,
	INT                  nOption
	);

WINL9SAPI INT      DLLPROC  LlStgsysGetPageOptionValue
	(
	HLLSTG               hStg,
	INT                  nPageIndex,
	INT                  nOption
	);

#ifdef WIN16
WINL9SAPI INT      DLLPROC  LlStgsysGetPageOptionString
	(
	HLLSTG               hStg,
	INT                  nPageIndex,
	INT                  nOption,
	LPSTR                pszBuffer,
	UINT                 nBufSize
	);
#endif // WIN16

#if defined(WIN32) && !defined(_NO_CMBTL9SAPIDEFINES)
   #ifdef UNICODE
       #define LlStgsysGetPageOptionString LlStgsysGetPageOptionStringW
       #define LlStgsysGetPageOptionStringO LlStgsysGetPageOptionStringA
     #else  // not UNICODE
       #define LlStgsysGetPageOptionString LlStgsysGetPageOptionStringA
       #define LlStgsysGetPageOptionStringO LlStgsysGetPageOptionStringW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9SAPIDEFINES
#ifdef WIN32
WINL9SAPI INT      DLLPROC  LlStgsysGetPageOptionStringA
	(
	HLLSTG               hStg,
	INT                  nPageIndex,
	INT                  nOption,
	LPSTR                pszBuffer,
	UINT                 nBufSize
	);
#endif // WIN32

#ifdef WIN32
WINL9SAPI INT      DLLPROC  LlStgsysGetPageOptionStringW
	(
	HLLSTG               hStg,
	INT                  nPageIndex,
	INT                  nOption,
	LPWSTR               pszBuffer,
	UINT                 nBufSize
	);
#endif // WIN32

#ifdef WIN16
WINL9SAPI INT      DLLPROC  LlStgsysSetPageOptionString
	(
	HLLSTG               hStg,
	INT                  nPageIndex,
	INT                  nOption,
	LPCSTR               pszBuffer
	);
#endif // WIN16

#if defined(WIN32) && !defined(_NO_CMBTL9SAPIDEFINES)
   #ifdef UNICODE
       #define LlStgsysSetPageOptionString LlStgsysSetPageOptionStringW
       #define LlStgsysSetPageOptionStringO LlStgsysSetPageOptionStringA
     #else  // not UNICODE
       #define LlStgsysSetPageOptionString LlStgsysSetPageOptionStringA
       #define LlStgsysSetPageOptionStringO LlStgsysSetPageOptionStringW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9SAPIDEFINES
#ifdef WIN32
WINL9SAPI INT      DLLPROC  LlStgsysSetPageOptionStringA
	(
	HLLSTG               hStg,
	INT                  nPageIndex,
	INT                  nOption,
	LPCSTR               pszBuffer
	);
#endif // WIN32

#ifdef WIN32
WINL9SAPI INT      DLLPROC  LlStgsysSetPageOptionStringW
	(
	HLLSTG               hStg,
	INT                  nPageIndex,
	INT                  nOption,
	LPCWSTR              pszBuffer
	);
#endif // WIN32

WINL9SAPI INT      DLLPROC  LlStgsysAppend
	(
	HLLSTG               hStg,
	HLLSTG               hStgToAppend
	);

WINL9SAPI INT      DLLPROC  LlStgsysDeleteJob
	(
	HLLSTG               hStg,
	INT                  nPageIndex
	);

WINL9SAPI INT      DLLPROC  LlStgsysDeletePage
	(
	HLLSTG               hStg,
	INT                  nPageIndex
	);

WINL9SAPI HANDLE   DLLPROC  LlStgsysGetPageMetafile
	(
	HLLSTG               hStg,
	INT                  nPageIndex
	);

WINL9SAPI HANDLE   DLLPROC  LlStgsysGetPageMetafile16
	(
	HLLSTG               hStg,
	INT                  nPageIndex
	);

WINL9SAPI INT      DLLPROC  LlStgsysDestroyMetafile
	(
	HANDLE               hMF
	);

WINL9SAPI INT      DLLPROC  LlStgsysDrawPage
	(
	HLLSTG               hStg,
	HDC                  hDC,
	HDC                  hPrnDC,
	BOOL                 bAskPrinter,
	_PCRECT              pRC,
	INT                  nPageIndex,
	BOOL                 bFit,
	LPVOID               pReserved
	);

WINL9SAPI INT      DLLPROC  LlStgsysGetLastError
	(
	HLLSTG               hStg
	);

WINL9SAPI INT      DLLPROC  LlStgsysDeleteFiles
	(
	HLLSTG               hStg
	);

#ifdef WIN16
WINL9SAPI INT      DLLPROC  LlStgsysPrint
	(
	HLLSTG               hStg,
	LPCSTR               pszPrinterName1,
	LPCSTR               pszPrinterName2,
	INT                  nStartPageIndex,
	INT                  nEndPageIndex,
	INT                  nCopies,
	UINT                 nFlags,
	LPCSTR               pszMessage,
	HWND                 hWndParent
	);
#endif // WIN16

#if defined(WIN32) && !defined(_NO_CMBTL9SAPIDEFINES)
   #ifdef UNICODE
       #define LlStgsysPrint LlStgsysPrintW
       #define LlStgsysPrintO LlStgsysPrintA
     #else  // not UNICODE
       #define LlStgsysPrint LlStgsysPrintA
       #define LlStgsysPrintO LlStgsysPrintW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9SAPIDEFINES
#ifdef WIN32
WINL9SAPI INT      DLLPROC  LlStgsysPrintA
	(
	HLLSTG               hStg,
	LPCSTR               pszPrinterName1,
	LPCSTR               pszPrinterName2,
	INT                  nStartPageIndex,
	INT                  nEndPageIndex,
	INT                  nCopies,
	UINT                 nFlags,
	LPCSTR               pszMessage,
	HWND                 hWndParent
	);
#endif // WIN32

#ifdef WIN32
WINL9SAPI INT      DLLPROC  LlStgsysPrintW
	(
	HLLSTG               hStg,
	LPCWSTR              pszPrinterName1,
	LPCWSTR              pszPrinterName2,
	INT                  nStartPageIndex,
	INT                  nEndPageIndex,
	INT                  nCopies,
	UINT                 nFlags,
	LPCWSTR              pszMessage,
	HWND                 hWndParent
	);
#endif // WIN32

#ifdef WIN16
WINL9SAPI INT      DLLPROC  LlStgsysStoragePrint
	(
	LPCSTR               lpszFilename,
	LPCSTR               pszTempPath,
	LPCSTR               pszPrinterName1,
	LPCSTR               pszPrinterName2,
	INT                  nStartPageIndex,
	INT                  nEndPageIndex,
	INT                  nCopies,
	UINT                 nFlags,
	LPCSTR               pszMessage,
	HWND                 hWndParent
	);
#endif // WIN16

#if defined(WIN32) && !defined(_NO_CMBTL9SAPIDEFINES)
   #ifdef UNICODE
       #define LlStgsysStoragePrint LlStgsysStoragePrintW
       #define LlStgsysStoragePrintO LlStgsysStoragePrintA
     #else  // not UNICODE
       #define LlStgsysStoragePrint LlStgsysStoragePrintA
       #define LlStgsysStoragePrintO LlStgsysStoragePrintW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9SAPIDEFINES
#ifdef WIN32
WINL9SAPI INT      DLLPROC  LlStgsysStoragePrintA
	(
	LPCSTR               lpszFilename,
	LPCSTR               pszTempPath,
	LPCSTR               pszPrinterName1,
	LPCSTR               pszPrinterName2,
	INT                  nStartPageIndex,
	INT                  nEndPageIndex,
	INT                  nCopies,
	UINT                 nFlags,
	LPCSTR               pszMessage,
	HWND                 hWndParent
	);
#endif // WIN32

#ifdef WIN32
WINL9SAPI INT      DLLPROC  LlStgsysStoragePrintW
	(
	LPCWSTR              lpszFilename,
	LPCWSTR              pszTempPath,
	LPCWSTR              pszPrinterName1,
	LPCWSTR              pszPrinterName2,
	INT                  nStartPageIndex,
	INT                  nEndPageIndex,
	INT                  nCopies,
	UINT                 nFlags,
	LPCWSTR              pszMessage,
	HWND                 hWndParent
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9SAPIDEFINES)
   #ifdef UNICODE
       #define LlStgsysGetPagePrinter LlStgsysGetPagePrinterW
       #define LlStgsysGetPagePrinterO LlStgsysGetPagePrinterA
     #else  // not UNICODE
       #define LlStgsysGetPagePrinter LlStgsysGetPagePrinterA
       #define LlStgsysGetPagePrinterO LlStgsysGetPagePrinterW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9SAPIDEFINES
#ifdef WIN32
WINL9SAPI INT      DLLPROC  LlStgsysGetPagePrinterA
	(
	HLLSTG               hStg,
	INT                  nPageIndex,
	LPSTR                pszDeviceName,
	UINT                 nDeviceNameSize,
	PHGLOBAL             phDevMode
	);
#endif // WIN32

#ifdef WIN32
WINL9SAPI INT      DLLPROC  LlStgsysGetPagePrinterW
	(
	HLLSTG               hStg,
	INT                  nPageIndex,
	LPWSTR               pszDeviceName,
	UINT                 nDeviceNameSize,
	PHGLOBAL             phDevMode
	);
#endif // WIN32



#ifdef __cplusplus
}
#endif

#ifdef __ZTC__ /* Zortech C++ */
#pragma ZTC align
#elif __WATCOMC__ > 1000 /* Watcom C++ */
#pragma pack(pop)
#elif __BORLANDC__ /* Borland C++ */
#pragma option -a.
#else
#pragma pack() /* MS C++ */
#endif

#endif  /* #ifndef RC_INVOKED */

#endif  /* #ifndef _L9S_H */

