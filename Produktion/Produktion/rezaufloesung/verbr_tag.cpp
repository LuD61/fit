#include "stdafx.h"
#include "DbClass.h"
#include "verbr_tag.h"
#include "cmbtl9.h"

struct VERBR_TAG verbr_tag, verbr_tag_null;
extern DB_CLASS dbClass;
static long anzzfelder = -1;
short mdn = 1;
short prodabt = 1;
double artikel = 0.0;
char datvon[12] ;
char datbis[12] ;
char artikelbezeichnung[26];
short a_bas_ag ;
short a_bas_dr_folge;
short a_bas_me_einh;
short a_bas_lief_einh;
double a_bas_a_gew;
bool Einzelliste;
bool Labeldruck;
char rez_preis_bz[12];
char a_bz1[25];
char einh_bz[9];
double rez_preis;
extern char Listenname[128];
extern char Labelname[128];
int SollIstVergleich = 0;
int OhnePrintOptions = 0;
char Listenname_vgl[128];
char Listenname_vgleinzel[128];
double artikel_von = 1.0;
double artikel_bis = 9999999999999.0;


int VERBR_TAG_CLASS::dbcount (void)
/**
Tabelle eti lesen.
**/
{

         if (test_upd_cursor == -1)
         {
             prepare ();
         }
         dbClass.sqlopen (count_cursor);
         dbClass.sqlfetch (count_cursor);
         if (sqlstatus == 0)
         {
                      return anzzfelder;
         }
         return sqlstatus; 
}

int VERBR_TAG_CLASS::leseverbr_tag (int fehlercode)
{
	  if (fehlercode == LL_WRN_REPEAT_DATA)
	  {
         return 0;    // Hier kein fetch , da DS nicht mehr auf Seite passt , und nochmal 
					  // ausegeben werden mu�
	  }
	  strcpy(verbr_tag.bearb_info, "");
      int di = dbClass.sqlfetch (readcursor);
	  return di;
}

int VERBR_TAG_CLASS::openverbr_tag (void)
{
         return dbClass.sqlopen (readcursor);
}

void VERBR_TAG_CLASS::prepare (void)
{
	if (artikel > 0.0)
	{
		artikel_von = artikel;
		artikel_bis = artikel;
	}

  
	test_upd_cursor = 1;

     sprintf(Listenname_vgl,"rezaufl_komplett");
if (SollIstVergleich == 1) //�ber prstatchg 
{

	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((char *) datvon, SQLCHAR, 12);
	dbClass.sqlin ((char *) datbis, SQLCHAR, 12);
	dbClass.sqlin ((short *) &prodabt, SQLSHORT, 0);
	dbClass.sqlout ((long *)  &anzzfelder, SQLLONG, 4);


    count_cursor = dbClass.sqlcursor ("select count(*) from prstatchg "
										"where mdn = ? and dat between ? and ? ");
										



	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((char *) datvon, SQLCHAR, 12);
	dbClass.sqlin ((char *) datbis, SQLCHAR, 12);
	dbClass.sqlin ((double *) &artikel_von, SQLDOUBLE, 0);
	dbClass.sqlin ((double *) &artikel_bis, SQLDOUBLE, 0);

	dbClass.sqlout ((char *) verbr_tag.dat, SQLCHAR, 12);
    dbClass.sqlout ((double *) &verbr_tag.artikel,SQLDOUBLE,0);
    dbClass.sqlout ((char *) artikelbezeichnung,SQLCHAR,25);
    dbClass.sqlout ((short *) &a_bas_ag,SQLSHORT,0);
    dbClass.sqlout ((short *) &a_bas_dr_folge,SQLSHORT,0);
    dbClass.sqlout ((double *) &a_bas_a_gew,SQLDOUBLE,0);
    dbClass.sqlout ((char *) einh_bz,SQLCHAR,9);
    dbClass.sqlout ((char *) a_bz1,SQLCHAR,25);
//so wars nicht kummuliert    dbClass.sqlout ((char *) verbr_tag.dat,SQLCHAR,12);
    dbClass.sqlout ((double *) &verbr_tag.a,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &verbr_tag.me_ist,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &verbr_tag.me_istist,SQLDOUBLE,0);
    dbClass.sqlout ((short *) &verbr_tag.typ,SQLSHORT,0);
	dbClass.sqlout ((char *) verbr_tag.chargennr, SQLCHAR, 14);

            readcursor = dbClass.sqlcursor ("select prstatchg.dat,abas.a,abas.a_bz1,a_bas.ag,a_bas.dr_folge, "
											"a_bas.a_gew, ptabn.ptbezk, a_bas.a_bz1, "
											" a_mat.a,  sum(prstatchg.soll_gew),sum(prstatchg.me_ist), "
											" case when prstatchg.mat = prdk_varb.mat then 10    "
											"      when prstatchg.mat = prdk_zut.mat then 20    "
											"      when prstatchg.mat = prdk_huel.mat then 30    "
											"        else 99 end typ, prstatchg.chargennr "

                                  "from prstatchg, a_bas, a_bas abas,  ptabn, a_mat, prdk_k, outer prdk_varb  "
								  ", outer prdk_zut, outer prdk_huel "
								  "where prstatchg.mdn = ? and prstatchg.dat between ? and ? "
								  " and a_mat.mat = prstatchg.mat "
								  "and a_bas.a = a_mat.a "
								  " and abas.a = prstatchg.a "
								  " and abas.a between ? and ? "
								  "and ptabn.ptitem = \"me_einh\" "
								  "and ptabn.ptlfnr = a_bas.me_einh "
									"and prstatchg.mdn = prdk_k.mdn "
									"and prstatchg.a = prdk_k.a "
									"and prdk_k.akv = 1 "

									"and prdk_varb.mdn = prstatchg.mdn "
									"and prdk_varb.rez = prdk_k.rez "
									"and prdk_varb.mat = prstatchg.mat "

									"and prdk_zut.mdn = prstatchg.mdn "	
									"and prdk_zut.rez = prdk_k.rez "
									"and prdk_zut.mat = prstatchg.mat "

									"and prdk_huel.mdn = prstatchg.mdn "
									"and prdk_huel.a =prstatchg.a "
									"and prdk_huel.mat = prstatchg.mat "

								  "group by 1,2,3,4,5,6,7,8,9,12,13 "
								  "order by dat,prstatchg.chargennr,abas.a,typ,a_mat.a ");
			return;

}
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((char *) datvon, SQLCHAR, 12);
	dbClass.sqlin ((char *) datbis, SQLCHAR, 12);
	dbClass.sqlin ((short *) &prodabt, SQLSHORT, 0);
	dbClass.sqlout ((long *)  &anzzfelder, SQLLONG, 4);


    count_cursor = dbClass.sqlcursor ("select count(*) from verbr_tag "
										"where verbr_tag.mdn = ? and verbr_tag.dat between ? and ? "
										"and prodabt = ?");
										

	 //210508 prdk_stufe = 11 Grundbr. wieder rein 
if (Einzelliste == TRUE) //Einzelaufl�sung , pro Produktionsartikelu
{

	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((char *) datvon, SQLCHAR, 12);
	dbClass.sqlin ((char *) datbis, SQLCHAR, 12);
	dbClass.sqlin ((short *) &prodabt, SQLSHORT, 0);

	dbClass.sqlout ((char *) verbr_tag.dat, SQLCHAR, 12);
    dbClass.sqlout ((double *) &verbr_tag.artikel,SQLDOUBLE,0);
    dbClass.sqlout ((char *) artikelbezeichnung,SQLCHAR,25);
    dbClass.sqlout ((short *) &a_bas_ag,SQLSHORT,0);
    dbClass.sqlout ((short *) &a_bas_dr_folge,SQLSHORT,0);
    dbClass.sqlout ((double *) &a_bas_a_gew,SQLDOUBLE,0);
    dbClass.sqlout ((char *) einh_bz,SQLCHAR,9);
    dbClass.sqlout ((char *) a_bz1,SQLCHAR,25);
//so wars nicht kummuliert    dbClass.sqlout ((char *) verbr_tag.dat,SQLCHAR,12);
    dbClass.sqlout ((double *) &verbr_tag.a,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &verbr_tag.me_ist,SQLDOUBLE,0);
    dbClass.sqlout ((short *) &verbr_tag.typ,SQLSHORT,0);
    dbClass.sqlout ((char *) verbr_tag.bearb_info,SQLCHAR,81);

            readcursor = dbClass.sqlcursor ("select verbr_tag.dat,abas.a,abas.a_bz1,a_bas.ag,a_bas.dr_folge,a_bas.a_gew, ptabn.ptbezk, a_bas.a_bz1, "
" verbr_tag.a,  sum(verbr_tag.me_ist), "
" verbr_tag.typ,  "
"verbr_tag.bearb_info "
                                  "from verbr_tag, a_bas, a_bas abas,  ptabn where verbr_tag.mdn = ? and verbr_tag.dat between ? and ? "
								  "and verbr_tag.prodabt = ? "
								  " and a_bas.a = verbr_tag.a "
								  " and abas.a = verbr_tag.artikel "
//								  "and verbr_tag.prdk_stufe <> 11 " 
								  "and ptabn.ptitem = \"me_einh\" "
								  "and ptabn.ptlfnr = a_bas.me_einh "
								  "group by 1,2,3,4,5,6,7,8,9,11,12 "
								  "order by dat,abas.a,typ,verbr_tag.typ,verbr_tag.a "

						 );
}
else if (strcmp (Listenname, Listenname_vgl) == 0) //Gesamtaufl�sung , prdk_stufe kommt raus
{

	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((char *) datvon, SQLCHAR, 12);
	dbClass.sqlin ((char *) datbis, SQLCHAR, 12);
	dbClass.sqlin ((short *) &prodabt, SQLSHORT, 0);

    dbClass.sqlout ((short *) &a_bas_ag,SQLSHORT,0);
    dbClass.sqlout ((short *) &a_bas_dr_folge,SQLSHORT,0);
    dbClass.sqlout ((double *) &a_bas_a_gew,SQLDOUBLE,0);
    dbClass.sqlout ((char *) einh_bz,SQLCHAR,9);
    dbClass.sqlout ((char *) a_bz1,SQLCHAR,25);
//so wars nicht kummuliert    dbClass.sqlout ((char *) verbr_tag.dat,SQLCHAR,12);
    dbClass.sqlout ((double *) &verbr_tag.a,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &verbr_tag.me_ist,SQLDOUBLE,0);
    dbClass.sqlout ((short *) &verbr_tag.typ,SQLSHORT,0);
    dbClass.sqlout ((char *) verbr_tag.bearb_info,SQLCHAR,81);

            readcursor = dbClass.sqlcursor ("select a_bas.ag,a_bas.dr_folge,a_bas.a_gew, ptabn.ptbezk, a_bas.a_bz1, "
" verbr_tag.a,  sum(verbr_tag.me_ist), "
" verbr_tag.typ,  "
"verbr_tag.bearb_info "
                                  "from verbr_tag, a_bas, ptabn where verbr_tag.mdn = ? and verbr_tag.dat between ? and ? "
								  "and verbr_tag.prodabt = ? "
								  " and a_bas.a = verbr_tag.a "
								  "and verbr_tag.prdk_stufe <> 11 "
								  "and ptabn.ptitem = \"me_einh\" "
								  "and ptabn.ptlfnr = a_bas.me_einh "
								  " group by 1,2,3,4,5,6,8,9"
//								   " order by typ,a_bas.ag,a_bas.dr_folge,a"
								   " order by typ,a"

						 );
}
else
{
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((char *) datvon, SQLCHAR, 12);
	dbClass.sqlin ((char *) datbis, SQLCHAR, 12);
	dbClass.sqlin ((short *) &prodabt, SQLSHORT, 0);

    dbClass.sqlout ((short *) &a_bas_ag,SQLSHORT,0);
    dbClass.sqlout ((short *) &a_bas_dr_folge,SQLSHORT,0);
    dbClass.sqlout ((double *) &a_bas_a_gew,SQLDOUBLE,0);
    dbClass.sqlout ((char *) einh_bz,SQLCHAR,9);
    dbClass.sqlout ((char *) a_bz1,SQLCHAR,25);
//so wars nicht kummuliert    dbClass.sqlout ((char *) verbr_tag.dat,SQLCHAR,12);
    dbClass.sqlout ((double *) &verbr_tag.a,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &verbr_tag.me_ist,SQLDOUBLE,0);
    dbClass.sqlout ((short *) &verbr_tag.typ,SQLSHORT,0);
    dbClass.sqlout ((short *) &verbr_tag.prdk_stufe,SQLSHORT,0);
    dbClass.sqlout ((char *) verbr_tag.bearb_info,SQLCHAR,81);

            readcursor = dbClass.sqlcursor ("select a_bas.ag,a_bas.dr_folge,a_bas.a_gew, ptabn.ptbezk, a_bas.a_bz1, "
" verbr_tag.a,  sum(verbr_tag.me_ist), "
" verbr_tag.typ,  "
"verbr_tag.prdk_stufe, verbr_tag.bearb_info "
                                  "from verbr_tag, a_bas, ptabn where verbr_tag.mdn = ? and verbr_tag.dat between ? and ? "
								  "and verbr_tag.prodabt = ? "
								  " and a_bas.a = verbr_tag.a "
								  "and ptabn.ptitem = \"me_einh\" "
								  "and ptabn.ptlfnr = a_bas.me_einh "
								  " group by 1,2,3,4,5,6,8,9,10"
//								   " order by typ,a_bas.ag,a_bas.dr_folge,a"
								   " order by typ,a"


						 );

}
/***

            readcursor = dbClass.sqlcursor ("select a_bas.a_gew, ptabn.ptbezk, a_bas.a_bz1, "
"verbr_tag.dat,  verbr_tag.a,  sum(verbr_tag.me_ist), "
" verbr_tag.typ,  "
"verbr_tag.prdk_stufe, verbr_tag.bearb_info "
                                  "from verbr_tag, a_bas, ptabn where verbr_tag.mdn = ? and verbr_tag.dat between ? and ? "
								  " and a_bas.a = verbr_tag.a "
								  "and ptabn.ptitem = \"me_einh\" "
								  "and ptabn.ptlfnr = a_bas.me_einh "
								  " group by 1,2,3,4,5,7,8,9"
								   " order by dat,typ,a"

						 );

  ********so wars nicht kummuliert*/
}
