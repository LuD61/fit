#include "stdafx.h"
#include "DbClass.h"
#include "a_preis.h"
#include "prstatatag.h"
#include "cmbtl9.h"

struct A_PREIS a_preis, a_preis_null;

void A_PREIS_CLASS::prepare (void)
{

		    sqlin ((long *)   &prstatatag.mdn, SQLLONG, 0);
			sqlin ((long *) &prstatatag.a,SQLLONG,0);
    sqlout ((long *) &a_preis.mdn,SQLLONG,0);
    sqlout ((short *) &a_preis.fil,SQLSHORT,0);
    sqlout ((double *) &a_preis.a,SQLDOUBLE,0);
    sqlout ((short *) &a_preis.prodabt,SQLSHORT,0);
    sqlout ((short *) &a_preis.druckfolge,SQLSHORT,0);
    sqlout ((double *) &a_preis.ek,SQLDOUBLE,0);
    sqlout ((double *) &a_preis.plan_ek,SQLDOUBLE,0);
    sqlout ((double *) &a_preis.bep,SQLDOUBLE,0);
    sqlout ((double *) &a_preis.plan_bep,SQLDOUBLE,0);
    sqlout ((double *) &a_preis.kostenkg,SQLDOUBLE,0);
    sqlout ((double *) &a_preis.kosteneinh,SQLDOUBLE,0);
    sqlout ((short *) &a_preis.flg_bearb_weg,SQLSHORT,0);
    sqlout ((short *) &a_preis.bearb_hk,SQLSHORT,0);
    sqlout ((short *) &a_preis.bearb_p1,SQLSHORT,0);
    sqlout ((short *) &a_preis.bearb_p2,SQLSHORT,0);
    sqlout ((short *) &a_preis.bearb_p3,SQLSHORT,0);
    sqlout ((short *) &a_preis.bearb_p4,SQLSHORT,0);
    sqlout ((short *) &a_preis.bearb_p5,SQLSHORT,0);
    sqlout ((short *) &a_preis.bearb_p6,SQLSHORT,0);
    sqlout ((short *) &a_preis.bearb_vk,SQLSHORT,0);
    sqlout ((double *) &a_preis.sp_sk,SQLDOUBLE,0);
    sqlout ((double *) &a_preis.sp_fil,SQLDOUBLE,0);
    sqlout ((double *) &a_preis.sp_vk,SQLDOUBLE,0);
//    sqlout ((DATE_STRUCT *) &a_preis.dat,SQLDATE,0);
    sqlout ((char *) a_preis.dat,SQLCHAR,11);
    sqlout ((char *) a_preis.zeit,SQLCHAR,6);
    sqlout ((char *) a_preis.pers_nam,SQLCHAR,9);
    sqlout ((long *) &a_preis.inh_ek,SQLLONG,0);
            readcursor = sqlcursor ("select "
"a_preis.mdn,  a_preis.fil,  a_preis.a,  a_preis.prodabt,  "
"a_preis.druckfolge,  a_preis.ek,  a_preis.plan_ek,  a_preis.bep,  "
"a_preis.plan_bep,  a_preis.kostenkg,  a_preis.kosteneinh,  "
"a_preis.flg_bearb_weg,  a_preis.bearb_hk,  a_preis.bearb_p1,  "
"a_preis.bearb_p2,  a_preis.bearb_p3,  a_preis.bearb_p4,  "
"a_preis.bearb_p5,  a_preis.bearb_p6,  a_preis.bearb_vk,  "
"a_preis.sp_sk,  a_preis.sp_fil,  a_preis.sp_vk,  a_preis.dat,  "
"a_preis.zeit,  a_preis.pers_nam,  a_preis.inh_ek from a_preis  "

#line 16 "a_preis.rpp"
                                  "where a_preis.mdn = ? "
                                  "and a_preis.a = ? ");
}

int A_PREIS_CLASS::lesea_preis (void)
{
      if (prstatatag.a <= 1) return 0;

      return sqlfetch (readcursor);
}

int A_PREIS_CLASS::opena_preis (void)
{
         return sqlopen (readcursor);
}

