#include "stdafx.h"
#include "DbClass.h"
#include "a_preis.h"
#include "prstatatag.h"
#include "cmbtl9.h"
#include "datum.h"
#include "strfkt.h"
#include <string.h>
 
extern DB_CLASS dbClass;
struct PRSTATATAG prstatatag, prstatatag_null;
static long anzzfelder = -1;
char dat_von[11];
char dat_bis[11];
char datumTag[11];
char a_bez[25];
char ag_bez[25];
int ag;
DATUM datum;
DATUM datum1;
short prodabt;
char prodabt_bez[17];
short mdn = 1;
double a = 1;
short dummy;
int anzfelder = 1;
double menge[] = {0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 };

short montag;
short dienstag;
short mittwoch;
short donnerstag;
short freitag;
short samstag;
short sonntag;
char ProdAbt [12] = {"a_bas"};



void PRSTATATAG_CLASS::prepare (void)
{

 if (prodabt == 0)
 {
            dbClass.sqlin ((short *)   &mdn, SQLSHORT, 0);
            dbClass.sqlin ((char *) dat_von,SQLCHAR,11);
            dbClass.sqlin ((char *) dat_bis,SQLCHAR,11);
	        dbClass.sqlout ((double *)  &a, SQLDOUBLE, 0);
		    dbClass.sqlout ((char *) &a_bez,SQLCHAR,25);
	        dbClass.sqlout ((short *)  &ag, SQLSHORT, 0);
		    dbClass.sqlout ((char *) &ag_bez,SQLCHAR,25);
		    dbClass.sqlout ((double *) &prstatatag.chg,SQLDOUBLE,0);
            count_cursor = dbClass.sqlcursor ("select a_bas.a, a_bas.a_bz1, a_bas.ag, "
										"ag.ag_bz1, prdk_k.kutter_gew from "
								      " a_bas, outer prstatatag ,ag,prdk_k "
					                  "where prdk_k.mdn = ? "
									  "and prstatatag.a = a_bas.a "
									  "and prdk_k.a = a_bas.a "
									  "and prdk_k.akv = 1 "
									  "and prstatatag.me > 0.01 "
									  "and prstatatag.dat >= ? and prstatatag.dat <= ? "
									  "and a_bas.ag = ag.ag "
									  "group by 1,2,3,4,5 order by 3,1");

   	   test_upd_cursor = 1;


            dbClass.sqlin ((short *)   &mdn, SQLSHORT, 0);
            dbClass.sqlin ((double *) &a,SQLDOUBLE,0);
            dbClass.sqlin ((char *) dat_von,SQLCHAR,11);
            dbClass.sqlin ((char *) dat_bis,SQLCHAR,11);
			dbClass.sqlout ((char *) &a_bez,SQLCHAR,25);
			dbClass.sqlout ((short *) &prstatatag.mdn,SQLSHORT,0);
			dbClass.sqlout ((char *) &prstatatag.dat,SQLCHAR,11);
			dbClass.sqlout ((double *) &prstatatag.a,SQLDOUBLE,0);
			dbClass.sqlout ((char *) prstatatag.rez,SQLCHAR,9);
			dbClass.sqlout ((double *) &prstatatag.me,SQLDOUBLE,0);
			dbClass.sqlout ((double *) &prstatatag.chg,SQLDOUBLE,0);
			dbClass.sqlout ((double *) &prstatatag.mat_o_b,SQLDOUBLE,0);
			dbClass.sqlout ((double *) &prstatatag.hk_teilk,SQLDOUBLE,0);
			dbClass.sqlout ((double *) &prstatatag.hk_vollk,SQLDOUBLE,0);
			dbClass.sqlout ((short *) &prstatatag.planstatus,SQLSHORT,0);
			dbClass.sqlout ((long *) &prstatatag.phase1,SQLLONG,0);
			dbClass.sqlout ((long *) &prstatatag.phase2,SQLLONG,0);
			dbClass.sqlout ((long *) &prstatatag.phase3,SQLLONG,0);
			dbClass.sqlout ((long *) &prstatatag.phase4,SQLLONG,0);
			dbClass.sqlout ((long *) &prstatatag.phase5,SQLLONG,0);
			dbClass.sqlout ((short *) &prstatatag.aufteilung,SQLSHORT,0);
			dbClass.sqlout ((short *) &prstatatag.variante,SQLSHORT,0);
			dbClass.sqlout ((long *) &ag,SQLLONG,0);
			dbClass.sqlout ((char *) &ag_bez,SQLCHAR,25);
            readcursor = dbClass.sqlcursor ("select a_bas.a_bz1, prstatatag.mdn,  "
			"prstatatag.dat,  prstatatag.a,  prstatatag.rez,  prstatatag.me,  "
			"prstatatag.chg,  prstatatag.mat_o_b,  prstatatag.hk_teilk,  "
			"prstatatag.hk_vollk,  prstatatag.planstatus,  prstatatag.phase1,  "
			"prstatatag.phase2,  prstatatag.phase3,  prstatatag.phase4,  "
			"prstatatag.phase5,  prstatatag.aufteilung,  prstatatag.variante, "
			"a_bas.ag, ag.ag_bz1 from a_bas,ag, prstatatag  "

                                  "where prstatatag.mdn = ? "
                                  "and prstatatag.a = ? "
								  "and prstatatag.me > 0.01 "
                                   "and prstatatag.a = a_bas.a "
								   "and a_bas.ag = ag.ag "

								   " and prstatatag.dat >= ? and prstatatag.dat <= ?");
 }
 else
 {
	if (strupcmp (ProdAbt, "a_bas", strlen (ProdAbt)) != 0)
	{ 
            dbClass.sqlin ((short *)   &mdn, SQLSHORT, 0);
            dbClass.sqlin ((short *) &prodabt,SQLSHORT,0);
            dbClass.sqlin ((char *) dat_von,SQLCHAR,11);
            dbClass.sqlin ((char *) dat_bis,SQLCHAR,11);
	        dbClass.sqlout ((double *)  &a, SQLDOUBLE, 0);
		    dbClass.sqlout ((char *) &a_bez,SQLCHAR,25);
	        dbClass.sqlout ((short *)  &ag, SQLSHORT, 0);
		    dbClass.sqlout ((char *) &ag_bez,SQLCHAR,25);
	        dbClass.sqlout ((short *)  &dummy, SQLSHORT, 0);
		    dbClass.sqlout ((double *) &prstatatag.chg,SQLDOUBLE,0);
            count_cursor = dbClass.sqlcursor ("select a_bas.a, a_bas.a_bz1, a_bas.ag, "
										"ag.ag_bz1, a_preis.druckfolge, prdk_k.kutter_gew from "
								      "a_preis, outer prstatatag , a_bas, ag,prdk_k "
					                  "where a_preis.mdn = ? "
						              "and a_preis.prodabt = ? "
									  "and a_bas.a = a_preis.a "
									  "and prstatatag.a = a_bas.a "
									  "and prdk_k.mdn = a_preis.mdn "
									  "and prdk_k.a = a_bas.a "
									  "and prdk_k.akv = 1 "
									  "and prstatatag.me > 0.01 "
									  "and prstatatag.dat >= ? and prstatatag.dat <= ? "
									  "and a_bas.ag = ag.ag "
									  "group by 1,2,3,4,5,6 order by 3,5,1");

   	   test_upd_cursor = 1;


            dbClass.sqlin ((short *)   &mdn, SQLSHORT, 0);
            dbClass.sqlin ((short *) &prodabt,SQLSHORT,0);
            dbClass.sqlin ((double *) &a,SQLDOUBLE,0);
            dbClass.sqlin ((char *) dat_von,SQLCHAR,11);
            dbClass.sqlin ((char *) dat_bis,SQLCHAR,11);
		    dbClass.sqlout ((char *) &a_bez,SQLCHAR,25);
			dbClass.sqlout ((short *) &prstatatag.mdn,SQLSHORT,0);
			dbClass.sqlout ((char *) &prstatatag.dat,SQLCHAR,11);
			dbClass.sqlout ((double *) &prstatatag.a,SQLDOUBLE,0);
			dbClass.sqlout ((char *) prstatatag.rez,SQLCHAR,9);
			dbClass.sqlout ((double *) &prstatatag.me,SQLDOUBLE,0);
			dbClass.sqlout ((double *) &prstatatag.chg,SQLDOUBLE,0);
			dbClass.sqlout ((double *) &prstatatag.mat_o_b,SQLDOUBLE,0);
			dbClass.sqlout ((double *) &prstatatag.hk_teilk,SQLDOUBLE,0);
			dbClass.sqlout ((double *) &prstatatag.hk_vollk,SQLDOUBLE,0);
			dbClass.sqlout ((short *) &prstatatag.planstatus,SQLSHORT,0);
			dbClass.sqlout ((long *) &prstatatag.phase1,SQLLONG,0);
			dbClass.sqlout ((long *) &prstatatag.phase2,SQLLONG,0);
			dbClass.sqlout ((long *) &prstatatag.phase3,SQLLONG,0);
			dbClass.sqlout ((long *) &prstatatag.phase4,SQLLONG,0);
			dbClass.sqlout ((long *) &prstatatag.phase5,SQLLONG,0);
			dbClass.sqlout ((short *) &prstatatag.aufteilung,SQLSHORT,0);
			dbClass.sqlout ((short *) &prstatatag.variante,SQLSHORT,0);
			dbClass.sqlout ((long *) &ag,SQLLONG,0);
			dbClass.sqlout ((char *) &ag_bez,SQLCHAR,25);
            readcursor = dbClass.sqlcursor ("select a_bas.a_bz1, prstatatag.mdn,  "
			"prstatatag.dat,  prstatatag.a,  prstatatag.rez,  prstatatag.me,  "
			"prstatatag.chg,  prstatatag.mat_o_b,  prstatatag.hk_teilk,  "
			"prstatatag.hk_vollk,  prstatatag.planstatus,  prstatatag.phase1,  "
			"prstatatag.phase2,  prstatatag.phase3,  prstatatag.phase4,  "
			"prstatatag.phase5,  prstatatag.aufteilung,  prstatatag.variante, "
			"a_bas.ag, ag.ag_bz1 from a_preis,a_bas,ag, prstatatag  "

                                  "where a_preis.mdn = ? "
                                  "and a_preis.prodabt = ? "
                                  "and prstatatag.a = ? "
                                  "and prstatatag.mdn = a_preis.mdn "
                                   "and prstatatag.a = a_preis.a "
								  "and prstatatag.me > 0.01 "
                                   "and prstatatag.a = a_bas.a "
								   "and a_bas.ag = ag.ag "

								   " and prstatatag.dat >= ? and prstatatag.dat <= ?");
		}
		else
		{
            dbClass.sqlin ((short *)   &mdn, SQLSHORT, 0);
            dbClass.sqlin ((short *) &prodabt,SQLSHORT,0);
            dbClass.sqlin ((char *) dat_von,SQLCHAR,11);
            dbClass.sqlin ((char *) dat_bis,SQLCHAR,11);
	        dbClass.sqlout ((double *)  &a, SQLDOUBLE, 0);
		    dbClass.sqlout ((char *) &a_bez,SQLCHAR,25);
	        dbClass.sqlout ((short *)  &ag, SQLSHORT, 0);
		    dbClass.sqlout ((char *) &ag_bez,SQLCHAR,25);
		    dbClass.sqlout ((double *) &prstatatag.chg,SQLDOUBLE,0);
            count_cursor = dbClass.sqlcursor ("select a_bas.a, a_bas.a_bz1, a_bas.ag, "
										"ag.ag_bz1, prdk_k.kutter_gew from "
								      " a_bas, outer prstatatag ,ag,prdk_k "
					                  "where prdk_k.mdn = ? "
						              "and a_bas.abt = ? "
									  "and prstatatag.a = a_bas.a "
									  "and prdk_k.a = a_bas.a "
									  "and prdk_k.akv = 1 "
									  "and prstatatag.me > 0.01 "
									  "and prstatatag.dat >= ? and prstatatag.dat <= ? "
									  "and a_bas.ag = ag.ag "
									  "group by 1,2,3,4,5 order by 3,1");

   	   test_upd_cursor = 1;


            dbClass.sqlin ((short *)   &mdn, SQLSHORT, 0);
            dbClass.sqlin ((short *) &prodabt,SQLSHORT,0);
            dbClass.sqlin ((double *) &a,SQLDOUBLE,0);
            dbClass.sqlin ((char *) dat_von,SQLCHAR,11);
            dbClass.sqlin ((char *) dat_bis,SQLCHAR,11);
			dbClass.sqlout ((char *) &a_bez,SQLCHAR,25);
			dbClass.sqlout ((short *) &prstatatag.mdn,SQLSHORT,0);
			dbClass.sqlout ((char *) &prstatatag.dat,SQLCHAR,11);
			dbClass.sqlout ((double *) &prstatatag.a,SQLDOUBLE,0);
			dbClass.sqlout ((char *) prstatatag.rez,SQLCHAR,9);
			dbClass.sqlout ((double *) &prstatatag.me,SQLDOUBLE,0);
			dbClass.sqlout ((double *) &prstatatag.chg,SQLDOUBLE,0);
			dbClass.sqlout ((double *) &prstatatag.mat_o_b,SQLDOUBLE,0);
			dbClass.sqlout ((double *) &prstatatag.hk_teilk,SQLDOUBLE,0);
			dbClass.sqlout ((double *) &prstatatag.hk_vollk,SQLDOUBLE,0);
			dbClass.sqlout ((short *) &prstatatag.planstatus,SQLSHORT,0);
			dbClass.sqlout ((long *) &prstatatag.phase1,SQLLONG,0);
			dbClass.sqlout ((long *) &prstatatag.phase2,SQLLONG,0);
			dbClass.sqlout ((long *) &prstatatag.phase3,SQLLONG,0);
			dbClass.sqlout ((long *) &prstatatag.phase4,SQLLONG,0);
			dbClass.sqlout ((long *) &prstatatag.phase5,SQLLONG,0);
			dbClass.sqlout ((short *) &prstatatag.aufteilung,SQLSHORT,0);
			dbClass.sqlout ((short *) &prstatatag.variante,SQLSHORT,0);
			dbClass.sqlout ((long *) &ag,SQLLONG,0);
			dbClass.sqlout ((char *) &ag_bez,SQLCHAR,25);
            readcursor = dbClass.sqlcursor ("select a_bas.a_bz1, prstatatag.mdn,  "
			"prstatatag.dat,  prstatatag.a,  prstatatag.rez,  prstatatag.me,  "
			"prstatatag.chg,  prstatatag.mat_o_b,  prstatatag.hk_teilk,  "
			"prstatatag.hk_vollk,  prstatatag.planstatus,  prstatatag.phase1,  "
			"prstatatag.phase2,  prstatatag.phase3,  prstatatag.phase4,  "
			"prstatatag.phase5,  prstatatag.aufteilung,  prstatatag.variante, "
			"a_bas.ag, ag.ag_bz1 from a_bas,ag, prstatatag  "

                                  "where prstatatag.mdn = ? "
                                  "and a_bas.abt = ? "
                                  "and prstatatag.a = ? "
								  "and prstatatag.me > 0.01 "
                                   "and prstatatag.a = a_bas.a "
								   "and a_bas.ag = ag.ag "

								   " and prstatatag.dat >= ? and prstatatag.dat <= ?");
		}
	  }
	}


int PRSTATATAG_CLASS::dbcount (void)
{
	/************* war nur Test
	  char *datmontag ;
	  char* kw;
	  DATUM dat;
	  strcpy(dat_von, "06.02.2004");
      dat = dat_von;
	  ++dat;
	  datmontag = dat.format("%A, %d. %B %Y"); 
	  kw = dat.format("%W"); 
	  kw = dat.format("%U"); 
	  ***************/


         if (test_upd_cursor == -1)
         {
             prepare ();
         }
         dbClass.sqlopen (count_cursor);
         sqlstatus = dbClass.sqlfetch (count_cursor);
		 if (sqlstatus != 0) return sqlstatus ;
		 anzfelder = 0;
         while (sqlstatus == 0)
         {
			 anzfelder ++;
	         sqlstatus = dbClass.sqlfetch (count_cursor);
         }
         dbClass.sqlopen (count_cursor);
         return anzfelder; 
}

int PRSTATATAG_CLASS::leseprstatatag (int fehlercode)
{

	  if (fehlercode == LL_WRN_REPEAT_DATA)
	  {
         return 0;    // Hier kein fetch , da DS nicht mehr auf Seite passt , und nochmal 
					  // ausgegeben werden mu�
	  }
      fehlercode = dbClass.sqlfetch (count_cursor);
	  if (fehlercode == 0)
      {
        prstatatag.a = a;
		sqlstatus = dbClass.sqlopen (readcursor);
		menge[0] = 0.0;menge[1] = 0.0;menge[2] = 0.0;menge[3] = 0.0;menge[4] = 0.0;
		menge[5] = 0.0;menge[6] = 0.0;menge[7] = 0.0;
        sqlstatus = dbClass.sqlfetch (readcursor);
		while (sqlstatus == 0)
         {
             datum = prstatatag.dat;
			 datum1 = datumTag;
			 if (datum - datum1 == 0) menge[7] = prstatatag.me;
			 menge[datum - dat_von] = prstatatag.me ;
	         sqlstatus = dbClass.sqlfetch (readcursor);
         }

      }
      return fehlercode;
}

int PRSTATATAG_CLASS::openprstatatag (void)
{
         return dbClass.sqlopen (count_cursor);
}

