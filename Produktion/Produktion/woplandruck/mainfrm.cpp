//=============================================================================
//
//  Project: List & Label
//           Copyright (c) 1998-2002 combit GmbH, All Rights Reserved
//
//  Authors: combit Software Team
//
//-----------------------------------------------------------------------------
//
//  Module:  LLMFC - List & Label MFC Sample Application
//
// Diese Applikation z.Z. nur f�r Wochenplandruck (Tabelle prstatatag wird ausgelesen)
// Erstellung : 24.95.2003  LuD

//=============================================================================

// mainfrm.cpp : implementation of the CMainFrame class 


#include "stdafx.h"
#include "llmfc.h"
#include "mainfrm.h"
#include "wmask.h"
#include "DbClass.h"
#include "prstatatag.h"
#include "a_preis.h"
#include "form_feld.h"
#include "systables.h"
#include "syscolumns.h"
#include "Token.h"

#include "cmbtl9.h"
//#include "mo_curso.h"
#include "time.h"
#include "datum.h"
#include <afxdisp.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// GR: Waehlen Sie hier die List & Label Sprache f�r den Designer und die
//     Dialoge aus :
//    (Die entsprechenden Sprachkonstanten entnehmen Sie der Datei cmbtl9.h)

// US: choose your LuL-Language for all designer, dialogs, etc... here:
//    (see cmbtl9.h for other language-constants)

const int LUL_LANGUAGE = CMBTLANG_GERMAN;

//LuD...
//const char *FILENAME_DEFAULT = "c:\\temp\\rezeptur.lst";  // Zeiger auf eine Konstante
const char *FILENAME_DEFAULT = "c:\\user\\fit\\format\\LL\\Wochenplan\\rezeptur.lst";  // Zeiger auf eine Konstante
bool cfgOK;
char Listenname[512];
char Listenname_ohnePreis[512];
char cwotag[20];
int Listenauswahl = 0;   // wenn 1 : Auswahl mehrerer Reports
int Hauptmenue = 0;   // wenn 1 : Auswahl edit oder print �ber Menue
extern short mdn ;
extern double avon ;
extern double abis ;
extern int agvon ;
extern int agbis ;
extern char dat_von[11];
extern char dat_bis[11];
extern char datumTag[11];
extern char a_bez[25];
extern char ag_bez[25];
extern int ag;
extern double menge[];
DATUM datumMontag;
DATUM datumAkt;

extern short montag;
extern short dienstag;
extern short mittwoch;
extern short donnerstag;
extern short freitag;
extern short samstag;
extern short sonntag;

extern short prodabt;
extern char prodabt_bez[17];
int woche = 99;
extern short variantevon;
extern short variantebis;
extern short akvvon;
extern short akvbis;
extern short a_bas_me_einh;
extern short a_bas_lief_einh;
extern double a_bas_inh_lief;
extern double a_bas_a_gew;
extern double rez_schwund;
extern double rez_chargengewicht;
extern char rez_preis_bz[12];
extern double rez_preis;
extern char bearb_info[81];
extern short ohnePreis;
int irez = 0;



  DB_CLASS dbClass;
  PRSTATATAG_CLASS Prstatatag;
  A_PREIS_CLASS APreis;
  FORM_FELD form_feld;
  SYSCOLUMNS syscolumns;
  SYSTABLES systables;

HWND hMainWindow;

int form_feld_curs, tabid_curs, coltyp_curs,  dynacurs ; 
/////////////////////////////////////////////////////////////////////////////
// CMainFrame

// GR: Registrieren eines Nachrichtenkanals f�r List & Label Callbacks
// US: register LuL MessageBase:
UINT CMainFrame::m_uLuLMessageBase = RegisterWindowMessage("cmbtLLMessage");

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_COMMAND(ID_FILE_START_DEBUG, OnStartDebug)
	ON_COMMAND(ID_FILE_STOP_DEBUG, OnStopDebug)
	ON_COMMAND(ID_EDIT_LABEL, OnEditLabel)
	ON_COMMAND(ID_EDIT_LIST, OnEditList)
	ON_UPDATE_COMMAND_UI(ID_FILE_START_DEBUG, OnUpdateStartDebug)
	ON_UPDATE_COMMAND_UI(ID_FILE_STOP_DEBUG, OnUpdateStopDebug)
	ON_COMMAND(ID_PRINT_LABEL, OnPrintLabel)
	ON_COMMAND(ID_PRINT_REPORT, OnPrintReport)
	//}}AFX_MSG_MAP
	ON_REGISTERED_MESSAGE(m_uLuLMessageBase,OnLulMessage)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	m_bDebug = FALSE;
    ProgCfg = new PROG_CFG ("woplandruck");

}

CMainFrame::~CMainFrame()
{
    if (ProgCfg != NULL)
    {
        ProgCfg->CloseCfg ();
        delete ProgCfg;
        ProgCfg = NULL;
    }

}


/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	
	LlAddCtlSupport(m_hWnd,
					LL_CTL_ADDTOSYSMENU | LL_CTL_CONVERTCONTROLS | LL_CTL_ALSOCHILDREN,
					"combit.ini");
	GetCfgValues();
	
	if (Hauptmenue == 0)  //#LuD
	{
		PostMessage(WM_COMMAND,ID_PRINT_REPORT,0l);
	}
		
return 0;
}

void CMainFrame::OnStartDebug()
{
	MessageBox("Make sure that DEBWIN2 had been started before this demo application. "
				"If this doesn't happen you won't see any debug outputs now!",
				"List & Label Sample App", MB_OK | MB_ICONINFORMATION);
//	LlSetDebug(LL_DEBUG_CMBTLL);
	m_bDebug = TRUE;

}

void CMainFrame::OnStopDebug()
{
	LlSetDebug(FALSE);
	m_bDebug = FALSE;
}

void CMainFrame::OnUpdateStopDebug(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bDebug);
}

void CMainFrame::OnUpdateStartDebug(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(!m_bDebug);
}


//=============================================================================
void CMainFrame::OnEditLabel()
//=============================================================================
{
	CHAR szFilename[128+1] = "*.lbl";
	HWND hWnd = m_hWnd;
	HJOB hJob;

	// GR: Initialisieren von List & Label.
	//     Es wird ein Job geoeffnet
	// US: initialize Job
	hJob = LlJobOpen(LUL_LANGUAGE);
	if (hJob==LL_ERR_BAD_JOBHANDLE)
	{
		MessageBox("Job can't be initialized!", "List & Label Sample App", MB_OK|MB_ICONSTOP);
		return;
	}
	else if (hJob==LL_ERR_NO_LANG_DLL)
	{
		MessageBox("Language file not found!\nEnsure that *.lng files can be found in your LuL DLL directory.",
					"List & Label Sample App",
					MB_OK|MB_ICONSTOP);
		return;
	}

	// GR: Setzen der List & Label Optionen
	// US: Setting the List & Label options
	LlSetOption(hJob, LL_OPTION_NEWEXPRESSIONS, TRUE);

	//GR: Exporter aktivieren
	//US: Enable exporter
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cm32l9ex.llx");

	// GR: Auswahl der Projekt-Datei �ber Datei-Auswahl-Dialog
/* ---> GrJ GrJ
	if (LlSelectFileDlgTitleEx(hJob, hWnd, "", LL_PROJECT_LABEL|LL_FILE_ALSONEW
								, szFilename, sizeof(szFilename), NULL) < 0)
    {
        LlJobClose(hJob);
        return;
    }
< ---- */

	// GR: Zur�cksetzen der internen Variablen-Puffer
    LlDefineVariableStart(hJob);

	// GR: Aufruf des Designers

   	sprintf ( szFilename, FILENAME_DEFAULT );

 	if (LlDefineLayout(hJob, hWnd, "Designer", LL_PROJECT_LABEL, szFilename) < 0)
    {
        MessageBox("Error by calling LlDefineLayout", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
        LlJobClose(hJob);
        return;
    }

    // GR: Beenden des List & Label Jobs
    LlJobClose(hJob);
}

long holetyp(char* tab_nam , char* feld_nam)
{
	static char alttabnam[20];
	static long alttabid ;
	if ( strncmp ( tab_nam, alttabnam, strlen(tab_nam)) == 0 )
	{
		// bereits gefunden
	}
	else
	{
		sprintf ( systables.tabname, "%s" , tab_nam );
		dbClass.sqlopen (tabid_curs);	// destroyed den Cursor, sqlopen refreshed
		if (!dbClass.sqlfetch(tabid_curs))	// gefunden
		{
			alttabid = systables.tabid ;
			sprintf ( alttabnam, "%s", tab_nam );
		}
		else

			return LL_TEXT ;

	}
 	sprintf ( syscolumns.colname, "%s" , feld_nam );
 	syscolumns.tabid = alttabid ;
	dbClass.sqlopen (coltyp_curs);	// destroyed den Cursor, sqlopen refreshed
	if (!dbClass.sqlfetch(coltyp_curs))	// gefunden
	{
		switch (syscolumns.coltype)
		{
		case(iDBCHAR):
			return LL_TEXT ;
		case(iDBSMALLINT):	// smallint
		case(iDBINTEGER):	// integer
		case(iDBDECIMAL):	// decimal
			return LL_NUMERIC ;
		case(iDBDATUM):	// Datumsfeld
			return LL_DATE ;
		}
		return LL_TEXT ;
	}
	else
		return LL_TEXT ;
		
}


int GetRecCount()  //#LuD
{
	return Prstatatag.dbcount ();
}


void Felderdefinition (HJOB hJob)
{
	
    LlDefineFieldExt(hJob, "a_nr", "2", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "ag", "1", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "a_bz1",    "Artikelbez.", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "ag_bz1",    "agbez.", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "chg_gew", "100", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "menge.montag",    "1,12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "menge.dienstag",    "1,12.", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "menge.mittwoch",    "1,12.", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "menge.donnerstag",    "1,12.", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "menge.freitag",    "1,12.", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "menge.samstag",    "1,12.", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "menge.sonntag",    "1,12.", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "menge.AktTag",    "1,12.", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "WoTag",      "Dienstag", LL_TEXT, NULL);
	
	
}

void Variablendefinition (HJOB hJob)
{

    LlDefineVariableExt(hJob, "Woche",        "22", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "Abteilung",      "4711", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "Abteilungsbez",      "Abt 1", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "dat_von",      "09.02.2004", LL_DATE_DMY, NULL);
    LlDefineVariableExt(hJob, "dat_bis",      "15.02.2004", LL_DATE_DMY, NULL);
    LlDefineVariableExt(hJob, "AktTag",    "1,12.", LL_DATE_DMY, NULL);
    LlDefineVariableExt(hJob, "WoTag",    "Montag", LL_TEXT, NULL);
//	LlDefineVariableExt(hJob, "Folgeseite", "1", LL_NUMERIC, NULL) ;
}

void VariablenUebergabe ( HJOB hJob, char szTemp2[], int nRecno )
{
		sprintf(szTemp2, "%ld", woche );
		LlDefineVariableExt(hJob, "Woche",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%ld", prodabt );
		LlDefineVariableExt(hJob, "Abteilung",        szTemp2, LL_NUMERIC, NULL);
        LlDefineVariableExt(hJob, "Abteilungsbez",      prodabt_bez, LL_TEXT, NULL);
	    LlDefineVariableExt(hJob, "dat_von",      dat_von, LL_DATE_DMY, NULL);
	    LlDefineVariableExt(hJob, "dat_bis",      dat_bis, LL_DATE_DMY, NULL);
	    LlDefineVariableExt(hJob, "AktTag",      datumTag, LL_DATE_DMY, NULL);
        LlDefineVariableExt(hJob, "WoTag",      cwotag, LL_TEXT, NULL);
//		long ldat = dasc_to_long (dat_von) + 1;
/*
		if (irez > 0 && irez != atoi(reportrez.rez))
		{	
			LlDefineVariableExt(hJob, "Folgeseite", "0", LL_NUMERIC, NULL) ;
		}
		else
		{	
			LlDefineVariableExt(hJob, "Folgeseite", "1", LL_NUMERIC, NULL) ;
		}
		irez = atoi(reportrez.rez);
*/

}

void FelderUebergabe ( HJOB hJob, char szTemp2[], int nRecno )
{

		sprintf(szTemp2, "%.0lf", prstatatag.a );
    LlDefineFieldExt(hJob, "a_nr", szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%ld", ag );
    LlDefineFieldExt(hJob, "ag", szTemp2, LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "a_bz1",    a_bez, LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "ag_bz1",    ag_bez, LL_TEXT, NULL);
		sprintf(szTemp2, "%.0lf", prstatatag.chg );
    LlDefineFieldExt(hJob, "chg_gew", szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.0lf", menge[montag] );
    LlDefineFieldExt(hJob, "menge.montag",    szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.0lf", menge[dienstag] );
    LlDefineFieldExt(hJob, "menge.dienstag",    szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.0lf", menge[mittwoch] );
    LlDefineFieldExt(hJob, "menge.mittwoch",    szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.0lf", menge[donnerstag] );
    LlDefineFieldExt(hJob, "menge.donnerstag",    szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.0lf", menge[freitag] );
    LlDefineFieldExt(hJob, "menge.freitag",    szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.0lf", menge[samstag] );
    LlDefineFieldExt(hJob, "menge.samstag",    szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.0lf", menge[sonntag] );
    LlDefineFieldExt(hJob, "menge.sonntag",    szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.0lf", menge[7] );
    LlDefineFieldExt(hJob, "menge.AktTag",    szTemp2, LL_DATE_DMY, NULL);
    LlDefineFieldExt(hJob, "WoTag",      cwotag, LL_TEXT, NULL);


}


//=============================================================================
void CMainFrame::OnEditList()
//=============================================================================
{
	CHAR szFilename[128+1] = "*.lst";
	HWND hWnd = m_hWnd;
	HJOB hJob;
    char *etc;
	char filename[512];

	etc = getenv ("BWS");
    sprintf (filename, "%s\\format\\LL\\Wochenplan\\%s.lst", etc, Listenname);
	FILENAME_DEFAULT = filename;
	// GR: Initialisieren von List & Label.
	//     Es wird ein Job geoeffnet
	hJob = LlJobOpen(LUL_LANGUAGE);
	if (hJob==LL_ERR_BAD_JOBHANDLE)
	{
		MessageBox("Job can't be initialized!", "List & Label Sample App", MB_OK|MB_ICONSTOP);
		return;
	}
	else if (hJob==LL_ERR_NO_LANG_DLL)
	{
		MessageBox("Language file not found!\nEnsure that *.lng files can be found in your LuL DLL directory.",
					"List & Label Sample App",
					MB_OK|MB_ICONSTOP);
		return;
	}

 // Ich muss wohl noch meine Lizenz setzen , damit Lauff�hig beim Kunden (Siehe redist.txt)
	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "q6GLOw");


	// GR: Setzen der List & Label Optionen
	LlSetOption(hJob, LL_OPTION_NEWEXPRESSIONS, TRUE);

	// GR: Setze den Modus multiple datalines fuer Tabellen (wird nur fuer SAMPLE3.LST benoetigt)
	//     Der Benutzer hat nun mehrere Zeilendefinitionen in Tabellen zu verfuegung.
	LlSetOption(hJob, LL_OPTION_MULTIPLETABLELINES, TRUE);

	LlSetOption(hJob, LL_OPTION_WIZ_FILENEW, TRUE);

	//GR: Exporter aktivieren
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cm32l9ex.llx");


	// GR: Auswahl der Projekt-Datei �ber Datei-Auswahl-Dialog
   	sprintf ( szFilename, FILENAME_DEFAULT );
	if (Listenauswahl == 1)  //#LuD
    {
		if (LlSelectFileDlgTitleEx(hJob, hWnd, "", LL_PROJECT_LIST|LL_FILE_ALSONEW
								, szFilename, sizeof(szFilename), NULL) < 0)
		{
	        LlJobClose(hJob);
			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
			return;
		}
	}

	// GR: Zur�cksetzen der internen Variablen-Puffer
    LlDefineVariableStart(hJob);

    // GR: Zur�cksetzen der internen Feld-Puffer
    LlDefineFieldStart(hJob);

//	dbClass.opendbase ("bws");
    Variablendefinition (hJob);
    Felderdefinition (hJob);

	if (Listenauswahl != 1)  
    {
	   	sprintf ( szFilename, FILENAME_DEFAULT );
	}
	// GR: Aufruf des Designers
    if (LlDefineLayout(hJob, hWnd, "Design Rezeptur", LL_PROJECT_LIST, szFilename) < 0)
    {
        MessageBox("Error by calling LlDefineLayout", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
        LlJobClose(hJob);
		PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
        return;
    }
    // GR: Beenden des List & Label Jobs
    LlJobClose(hJob);
	PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
}


//=============================================================================
void CMainFrame::OnPrintLabel()
//=============================================================================
{
	DoLabelPrint();
}


//=============================================================================
void CMainFrame::OnPrintReport()
//=============================================================================
{
    LPSTR CommandLine = GetCommandLine ();
    CToken Token (CommandLine, " ");

    char *p;
    Token.NextToken ();
	while ((p = Token.NextToken ()) != NULL)
	{
		if (strcmp (p, "-h") == 0 || strcmp (p, "-?") == 0)
		{
              MessageBox ("-mdn           <Mandant>\n"
				          "-prodabt       <Abteilung>\n"
						  "-abtbez        <Bez. der Abt.>\n"
						  "-dat_von       <Datum von>\n" 
				          "-dat_bis       <Datum bis>\n"
				          " ----- m�gliche Aufrufparameter ----- ",
                                MB_OK);
  			  PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
              return;
		}
		if (strcmp (p, "-mdn") == 0)
		{
			p = Token.NextToken ();
			if (p == NULL) break;
            mdn = (atoi (p));
		}
		else if (strcmp (p, "-prodabt") == 0)
		{
			p = Token.NextToken ();
			if (p == NULL) break;
            prodabt = (atoi (p));
		}
		else if (strcmp (p, "-abtbez") == 0)
		{
			p = Token.NextToken ();
			if (p == NULL) break;
			strcpy(prodabt_bez, p);
		}
		else if (strcmp (p, "-kw") == 0)
		{
			p = Token.NextToken ();
			if (p == NULL) break;
            woche = (atoi (p));
		}
		else if (strcmp (p, "-dat_von") == 0)
		{
			p = Token.NextToken ();
			if (p == NULL) break;
			strcpy(dat_von, p);
		}
		else if (strcmp (p, "-dat_bis") == 0)
		{
			p = Token.NextToken ();
			if (p == NULL) break;
			strcpy(dat_bis, p);
		}
		else if (strcmp (p, "-datum") == 0)
		{
			p = Token.NextToken ();
			if (p == NULL) break;
			strcpy(datumTag, p);
		}
		else if (strcmp (p, "-wahl") == 0)
        {  
			Listenauswahl = 1;
        }


	}

	montag = 0;
	dienstag = 1;
	mittwoch = 2;
	donnerstag = 3;
	freitag = 4;
	samstag = 5;
	sonntag = 6;
	datumMontag = dat_von;
	datumAkt = datumTag;
    woche = get_woche (dat_von);
	if (datumAkt - datumMontag == 0) sprintf(cwotag, "Montag"); 
	if (datumAkt - datumMontag == 1) sprintf(cwotag, "Dienstag"); 
	if (datumAkt - datumMontag == 2) sprintf(cwotag, "Mittwoch"); 
	if (datumAkt - datumMontag == 3) sprintf(cwotag, "Donnerstag"); 
	if (datumAkt - datumMontag == 4) sprintf(cwotag, "Freitag"); 
	if (datumAkt - datumMontag == 5) sprintf(cwotag, "Samstag"); 
	if (datumAkt - datumMontag == 6) sprintf(cwotag, "Sonntag"); 


	DoListPrint();
}

//=============================================================================
void CMainFrame::DoLabelPrint()
//=============================================================================
{
    HWND hWnd = m_hWnd;
    CHAR szFilename[128+1] = "*.lbl", szTemp[30], szTemp2[40], szBoxText[200];
	HJOB hJob;
	int  nRet = 0;

	// GR: Initialisieren von List & Label.
	//     Es wird ein Job geoeffnet
	hJob = LlJobOpen(LUL_LANGUAGE);
	if (hJob==LL_ERR_BAD_JOBHANDLE)
	{
		MessageBox("Job can't be initialized!", "List & Label Sample App", MB_OK|MB_ICONSTOP);
		return;
    }
	else if (hJob==LL_ERR_NO_LANG_DLL)
	{
		MessageBox("Language file not found!\nEnsure that *.lng files can be found in your LuL DLL directory.",
					"List & Label Sample App",
					MB_OK|MB_ICONSTOP);
		return;
    }

	//GR: Exporter aktivieren
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cm32l9ex.llx");

	// US: Choose new expression mode with formula support
	LlSetOption(hJob, LL_OPTION_NEWEXPRESSIONS, TRUE);
	LlSetOption(hJob, LL_OPTION_WIZ_FILENEW, TRUE);

	// GR: Auswahl der Projekt-Datei �ber Datei-Auswahl-Dialog

   	sprintf ( szFilename, FILENAME_DEFAULT );
	if (Listenauswahl == 1)  //#LuD
    {
		nRet = LlSelectFileDlgTitleEx(hJob, hWnd, "", LL_PROJECT_LABEL
								, szFilename,sizeof(szFilename), NULL);
		if (nRet < 0)
   		{
	    	if (nRet != LL_ERR_USER_ABORTED)
				MessageBox("Error While Printing", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
			LlJobClose(hJob);
			return;
		}
	}

    // GR: Zur�cksetzen der internen Variablen-Puffer
    LlDefineVariableStart(hJob);

    for (int i=1; i<10; i++)
	{
		sprintf(szTemp, "Field%d", i);
    	LlDefineVariable(hJob, szTemp, szTemp);
   	}

    // GR: Definition der Barcode-Variablen
    //     Normalerweise definiert man nur die Barcode-Typen die man benoetigt

    // GR: Druck starten

	if (LlPrintWithBoxStart(hJob, LL_PROJECT_LABEL, szFilename,
							LL_PRINT_EXPORT,
							LL_BOXTYPE_BRIDGEMETER, hWnd, "Printing...") < 0)
    {
        MessageBox("Error While Printing", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
        LlJobClose(hJob);
        return;
    }

	// GR: Einstellungen bzw. optionen f�r den Drucker-Optionen-Dialog
	LlPrintSetOption(hJob, LL_PRNOPT_COPIES, LL_COPIES_HIDE);
    LlPrintSetOption(hJob, LL_PRNOPT_STARTPAGE, 1);
	LlPrintSetOption(hJob, LL_PRNOPT_OFFSET, 0);
	if (LlPrintOptionsDialog(hJob, hWnd, "Select printing options") < 0)
    {
        LlPrintEnd(hJob,0);
        LlJobClose(hJob);
        return;
    }


	//GR: Druckziel abfragen
	CString sMedia;
	LlPrintGetOptionString(hJob, LL_PRNOPTSTR_EXPORT, sMedia.GetBuffer(256), 256);
	sMedia.ReleaseBuffer();

	char szPrinter[80], szPort[20];
	int  nRecCount = 30, nErrorValue = 0, nRecno, nLastPage;

	LlPrintGetPrinterInfo(hJob, szPrinter, sizeof(szPrinter), szPort, sizeof(szPort));

    nLastPage = LlPrintGetOption(hJob, LL_OPTION_LASTPAGE);
    nRecno = 1;

    // GR: Definition der Barcode-Variablen
    //     Normalerweise definiert man nur die Barcode-Typen die man benoetigt

	// GR: Druckschleife
    //     Diese wird so lange wiederholt, bis saemtliche Daten abgearbeitet wurden, oder
    //     ein Fehler auftritt.
	while (nRecno < nRecCount
			&& (LlPrintGetCurrentPage(hJob) <= nLastPage)
			&& nErrorValue == 0)
	{
		for (i=1; i<10; i++)
		{
			sprintf(szTemp, "Field%d", i);
			if (LlPrintIsVariableUsed(hJob, szTemp))
			{
				sprintf(szTemp2, "contents of Field%d, record %d", i, nRecno);
    			LlDefineVariable(hJob, szTemp, szTemp2);
			}
    	}

    	// GR: Prozentbalken in der Fortschritts-Anzeige setzen
    	sprintf(szBoxText, "printing on %s %s", szPrinter, szPort);
    	nErrorValue = LlPrintSetBoxText(hJob, szBoxText, (100 * nRecno / nRecCount));
        if (nErrorValue == LL_ERR_USER_ABORTED)
    	{
    		LlPrintEnd(hJob,0);
    		LlJobClose(hJob);
    		return;
    	}

    	// GR: Variablen drucken
    	nErrorValue = LlPrint(hJob);

		// GR: gehe zum naechsten Datensatz
    	nRecno++;
	}

	//GR: Druck beenden
	LlPrintEnd(hJob,0);


	//GR: Druckziel = Preview?
	if (sMedia=="PRV")
    {
        // GR: Beim Preview-Druck Preview anzeigen und dann Preview-Datei (.LL) l�schen
        LlPreviewDisplay(hJob, szFilename, "", hWnd);

        LlPreviewDeleteFiles(hJob, szFilename, "");
    }

	// GR: Beenden des List & Label Jobs
	LlJobClose(hJob);
}

//=============================================================================
void CMainFrame::DoListPrint()
//=============================================================================
{
    HWND hWnd = m_hWnd;
    CHAR szFilename[128+1] = "*.lst", szTemp[50], szTemp2[60], szBoxText[200];
	CHAR dmess[128] = "";
	HJOB hJob;
	int  nRet = 0;
	strcpy(szTemp, " ");
	char *etc;
	char filename[512];

	etc = getenv ("BWS");
    sprintf (filename, "%s\\format\\LL\\Wochenplan\\%s.lst", etc, Listenname);
	FILENAME_DEFAULT = filename;


	// GR: Initialisieren von List & Label.
	//     Es wird ein Job geoeffnet
	hJob = LlJobOpen(LUL_LANGUAGE);
	if (hJob==LL_ERR_BAD_JOBHANDLE)
	{
		MessageBox("Job can't be initialized!", "List & Label Sample App", MB_OK|MB_ICONSTOP);
		return;
    }
	else if (hJob==LL_ERR_NO_LANG_DLL)
	{
		MessageBox("Language file not found!\nEnsure that *.lng files can be found in your LuL DLL directory.",
					"List & Label Sample App",
					MB_OK|MB_ICONSTOP);
		return;
    }

 // Ich muss wohl noch meine Lizenz setzen , damit Lauff�hig beim Kunden !?(Siehe redist.txt)
	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "q6GLOw");


	//GR: Exporter aktivieren
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cm32l9ex.llx");

	// GR: Setzen der List & Label Optionen
	LlSetOption(hJob, LL_OPTION_NEWEXPRESSIONS, TRUE);

	// GR: Setze den Modus multiple datalines fuer Tabellen (wird nur fuer SAMPLE3.LST benoetigt)
	//     Der Benutzer hat nun mehrere Zeilendefinitionen in Tabellen zu verfuegung.
	LlSetOption(hJob, LL_OPTION_MULTIPLETABLELINES, TRUE);

	LlSetOption(hJob, LL_OPTION_WIZ_FILENEW, TRUE);

	// GR: Auswahl der Projekt-Datei �ber Datei-Auswahl-Dialog
   	sprintf ( szFilename, FILENAME_DEFAULT );
	if (Listenauswahl == 1)  //#LuD
    {
		nRet = LlSelectFileDlgTitleEx(hJob, hWnd, "", LL_PROJECT_LIST
								, szFilename,sizeof(szFilename), NULL);
		if (nRet < 0)
	   	{
    		if (nRet != LL_ERR_USER_ABORTED)
				MessageBox("Error While Printing", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
			LlJobClose(hJob);
			return;
		}
	}

    // GR: Zur�cksetzen der internen Variablen-Puffer
    LlDefineVariableStart(hJob);

	dbClass.opendbase ("bws");
    Variablendefinition (hJob);
    Felderdefinition (hJob);

    // GR: Druck starten
	if (LlPrintWithBoxStart(hJob, LL_PROJECT_LIST, szFilename,
							LL_PRINT_EXPORT,
							LL_BOXTYPE_BRIDGEMETER, hWnd, "Printing...") < 0)
    {
        MessageBox("Error While Printing", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
        LlJobClose(hJob);
        return;
    }

    // GR: Einstellungen bzw. optionen f�r den Drucker-Optionen-Dialog
	LlPrintSetOption(hJob, LL_PRNOPT_COPIES, 	LL_COPIES_HIDE);
    LlPrintSetOption(hJob, LL_PRNOPT_STARTPAGE, 1);
	if (LlPrintOptionsDialog(hJob, hWnd, "Select printing options") < 0)
    {
        LlPrintEnd(hJob,0);
        LlJobClose(hJob);
		if (Hauptmenue == 0)  //#LuD
		{
			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
		}
		
        return;
    }

	//GR: Druckziel abfragen
	CString sMedia;
	LlPrintGetOptionString(hJob, LL_PRNOPTSTR_EXPORT, sMedia.GetBuffer(256), 256);
	sMedia.ReleaseBuffer();

    int  nRecCount = 1, nErrorValue = 0, nPrintFehler = 0, nLastPage, nRecno;
	char szPrinter[80], szPort[20];

    LlPrintGetPrinterInfo(hJob, szPrinter, sizeof(szPrinter), szPort, sizeof(szPort));

	nLastPage = LlPrintGetOption(hJob, LL_OPTION_LASTPAGE);
    nRecno = 1;

    // GR: Druckschleife
    //     Diese wird so lange wiederholt, bis saemtliche Daten abgearbeitet wurden, oder
    //     ein Fehler auftritt.
	nRecCount = GetRecCount () ;
	sprintf (dmess, "nRecCount = %hd",nRecCount);  //050803
//	MessageBox(dmess, "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
	int di = Prstatatag.leseprstatatag (nErrorValue);   //       1.DATENSATZ lesen f�r Variablen�bergabe
	if (di != 0) 
    {
		sprintf (dmess, "Datensatz nicht gefunden SQL:%hd",di);
		MessageBox (dmess, "", MB_ICONERROR);
    			LlPrintEnd(hJob,0);
    			LlJobClose(hJob);
				if (Hauptmenue == 0)  //#LuD
				{
					PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
				}
    			return;

	}
	Prstatatag.openprstatatag ();   
	while (nRecno <= nRecCount
			&& (LlPrintGetCurrentPage(hJob) <= nLastPage)
			&& (nErrorValue == 0 || nErrorValue == LL_WRN_REPEAT_DATA))
   	{
	    VariablenUebergabe ( hJob, szTemp2, nRecno );

		nErrorValue = LlPrint(hJob);
		while (nRecno <= nRecCount 
			&& (nErrorValue == 0) 
			&& (LlPrintGetCurrentPage(hJob) <= nLastPage)
			&& (Prstatatag.leseprstatatag (nPrintFehler) == 0))  //       DATENSATZ lesen

		{
		    FelderUebergabe ( hJob, szTemp2, nRecno );
    		// GR: Prozentbalken in der Fortschritts-Anzeige setzen
    		sprintf(szBoxText, "printing on %s %s", szPrinter, szPort);
    		nErrorValue = LlPrintSetBoxText(hJob, szBoxText, (100 * nRecno / nRecCount));
        	if (nErrorValue == LL_ERR_USER_ABORTED)
   			{
    			LlPrintEnd(hJob,0);
    			LlJobClose(hJob);
				if (Hauptmenue == 0)  //#LuD
				{
					PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
				}
    			return;
   			}

    		// GR: Drucken der aktuellen Tabellenzeile
    		nErrorValue = LlPrintFields(hJob);
			nPrintFehler = nErrorValue;
//050803
			if (nErrorValue == LL_WRN_REPEAT_DATA)
			{
					// GR: Aktualisieren der seitenabhaengigen Variablen
			    VariablenUebergabe ( hJob, szTemp2, nRecno );
				nErrorValue = LlPrint(hJob);
//			    FelderUebergabe ( hJob, szTemp2, nRecno );
//	    		nErrorValue = LlPrintFields(hJob);
			}


			if (nErrorValue == 0)	// US: everything fine, record could have been printed...
	    	{
				// US: ...but probably the user defined a filter condition!
				//     So before updating time dependent variables we have to check if the record really has been printed:
				if (LlPrintDidMatchFilter(hJob))
				{
					// GR: Aktualisieren der zeitabhaengigen Variable 'FixedVariable2'
					// US: update the time dependent variable 'FixedVariable2'
					//     coming after printig the list(s)
		    		sprintf(szTemp2, "FixedVariable2, record %d", nRecno);
		    		LlDefineVariable(hJob, "FixedVariable2", szTemp2);
		    	}

	    		if (nPrintFehler == 0) nRecno++;	// GR: gehe zum naechsten Datensatz
			}
		}
  	}

	// US: all records have been printed, now flush the table
	//     If footer doesn't fit to this page try again for the next page:

	nErrorValue = LlPrintFieldsEnd(hJob);
	if (nErrorValue == LL_WRN_REPEAT_DATA)
	{
		// GR: Aktualisieren der seitenabhaengigen Variablen
	    VariablenUebergabe ( hJob, szTemp2, nRecno );
	    FelderUebergabe ( hJob, szTemp2, nRecno );

		// US: ... and then try again:
		nErrorValue = LlPrintFieldsEnd(hJob);
	}

	if (nErrorValue == LL_WRN_REPEAT_DATA)	// US: footer still doesn't fit!
		MessageBox("Error because table is too small for footer!", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);

	//GR: Druck beenden
	LlPrintEnd(hJob,0);
	
	//GR: Druckziel = Preview?
	if (sMedia=="PRV")
    {
        // GR: Beim Preview-Druck Preview anzeigen und dann Preview-Datei (.LL) loeschen
        LlPreviewDisplay(hJob, szFilename, "", hWnd);

        LlPreviewDeleteFiles(hJob, szFilename, "");
    }
	// GR: Beenden des List & Label Jobs
	LlJobClose(hJob);
	if (Hauptmenue == 0)  //#LuD
	{
		PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
	}
}

//=============================================================================
LRESULT CMainFrame::OnLulMessage(WPARAM wParam, LPARAM lParam)
//=============================================================================
{
	// GR: Dies ist die List & Label Callback Funktion.
	//     Saemtliche Rueckmeldungen bzw. Events werden dieser Funktion
	//     gemeldet.
	// US: This is the List & Label Callback function.
	//     Is is called for requests an notifications.


	PSCLLCALLBACK	pscCallback = (PSCLLCALLBACK) lParam;
	LRESULT			lRes = TRUE;
	CString			sVariableDescr;

	static CHAR szHelpText[256];

	ASSERT(pscCallback->_nSize == sizeof(scLlCallback));	// GR: Die groesse der Callback Struktur muss stimmen!
															// US: sizes of callback structs must match!

	switch(wParam)
	{
		case LL_CMND_VARHELPTEXT:	// GR: Es wird ein Beschreibungstext f�r eine Variable erfragt.
									// US: Helptext needed for selected variable within designer

				// GR: ( pscCallback->_lParam enth�lt einen LPCSTR des Beschreibungstextes )
				// US: ( pscCallback->_lParam contains a LPCSTR to the name of the selected variable )

				sVariableDescr = (LPCSTR)pscCallback->_lParam;

				if (!sVariableDescr.IsEmpty())
					sprintf(szHelpText,
							"This is the sample field / variable '%s'.",
							(LPCSTR)sVariableDescr);
				else
					strcpy(szHelpText, "No variable or field selected.");

				pscCallback->_lReply = (LPARAM)szHelpText;
				break;

		default:
				pscCallback->_lReply = lRes = FALSE; // GR: Die Nachricht wurde nicht bearbeitet.
													 // US: indicate that message hasn't been processed
	}

	return(lRes);
}


void CMainFrame::GetCfgValues (void)
{
       char cfg_v [512];

       if (ProgCfg == NULL) return;

       cfgOK = TRUE;
	   sprintf(Listenname,"woplan");
       if (ProgCfg->GetCfgValue ("Listenname", cfg_v) == TRUE)
       {
		   sprintf(Listenname,cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("Listenauswahl", cfg_v) == TRUE)
       {
           Listenauswahl = atoi (cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("Hauptmenue", cfg_v) == TRUE)
       {
           Hauptmenue = atoi (cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("ProdAbt", cfg_v) == TRUE)
       {
		   strcpy (ProdAbt, cfg_v);
	   }



/**********
SYSTEMTIME systime ;
CTime zeit;
CTime theTime = CTime::GetCurrentTime();
COleDateTime time(70, 12, 18, 17, 30, 0);

CTime t( 1991, 3, 19, 22, 15, 0 );
// 22:15 am 19. M�rz 1991

CString s = t.Format( "%A, %d. %B %Y" );
// s == "Dienstag, 19. M�rz 1991" 
// Constructors

//	CTime(int nYear, int nMonth, int nDay, int nHour, int nMin, int nSec,
//		int nDST = -1);
//	CTime(const SYSTEMTIME& sysTime, int nDST = -1);
//	CTime(const FILETIME& fileTime, int nDST = -1);
**********/










}


    // GR: Definition der Barcode-Variablen
    //     Normalerweise definiert man nur die Barcode-Typen die man benoetigt
	/************
	LlDefineFieldExt(hJob, "Barcode_EAN13", "44|44444|44444", LL_BARCODE_EAN13, NULL);
	LlDefineFieldExt(hJob, "Barcode_EAN13P2", "44|44444|44444|44", LL_BARCODE_EAN13, NULL);
	LlDefineFieldExt(hJob, "Barcode_EAN13P5", "44|44444|44444|44444", LL_BARCODE_EAN13, NULL);
	LlDefineFieldExt(hJob, "Barcode_EAN128", "EAN128ean128", LL_BARCODE_EAN128, NULL);
	LlDefineFieldExt(hJob, "Barcode_CODE128", "Code 128", LL_BARCODE_CODE128, NULL);
	LlDefineFieldExt(hJob, "Barcode_Codabar", "A123456A", LL_BARCODE_CODABAR, NULL);
	LlDefineFieldExt(hJob, "Barcode_UPCA", "44|44444", LL_BARCODE_EAN8, NULL);
	LlDefineFieldExt(hJob, "Barcode_UPCE", "4|44444|44444", LL_BARCODE_UPCA, NULL);
	LlDefineFieldExt(hJob, "Barcode_3OF9", "*TEST*", LL_BARCODE_3OF9, NULL);
	LlDefineFieldExt(hJob, "Barcode_25IND", "44444", LL_BARCODE_25INDUSTRIAL, NULL);
	LlDefineFieldExt(hJob, "Barcode_25IL", "444444", LL_BARCODE_25INTERLEAVED, NULL);
	LlDefineFieldExt(hJob, "Barcode_25MAT", "44444", LL_BARCODE_25MATRIX, NULL);
	LlDefineFieldExt(hJob, "Barcode_25DL", "44444", LL_BARCODE_25DATALOGIC, NULL);
	LlDefineFieldExt(hJob, "Barcode_POSTNET5", "44444", LL_BARCODE_POSTNET, NULL);
	LlDefineFieldExt(hJob, "Barcode_POSTNET10", "44444-4444", LL_BARCODE_POSTNET, NULL);
	LlDefineFieldExt(hJob, "Barcode_POSTNET12", "44444-444444", LL_BARCODE_POSTNET, NULL);
	LlDefineFieldExt(hJob, "Barcode_FIM", "A", LL_BARCODE_FIM, NULL);
	**********/
void fitapldefines (HJOB hJob)
{

//char tabdotfeld[101];
//char defabeleg [101];
//long ll_typ ;

	// GrJ :spaeter soll hier ne gigantische Schleife alles automatisch generieren
	// Namenskonvention ( Vorschlag 1 : 1. Buchstabe Tabelle/feld grossbuchstabe
	//									2. fuer ptab-Felder ein kleines "c" vor Feldname
	//								d.h. Original-feld mit Schluessel wird immer angeboten,
	//									ptabn-Feld nur, falls angekreuzt(Performance)
	//									3. fuer adress-felder : ein kleines "a"
	//									, danach nummer-feld-name, danach feldname
	//										Bsp.: Kun.aAdr1.Str
	//									Aufloesung nur, wenn angekreuzt

// 		dbClass.opendbase ("bws");
//	}
/* ---> Mustereintrag von Wille ---> 
	dbClass.sqlin ((double *) &a_bas.a, SQLDOUBLE, 0);
	dbClass.sqlout ((char *)  a_bas.a_bz1, SQLCHAR, 25);
	dbClass.sqlout ((char *)  a_bas.a_bz2, SQLCHAR, 25);
    cursor = dbClass.sqlcursor ("select a_bz1, a_bz2 from a_bas where a = ?");
	a = 2.0;
	while (dbClass.sqlfetch (cursor) == 0)
	{
	}
	dbClass.sqlclose (cursor);	// destroyed den Cursor, sqlopen refreshed 
< ---- */

/****************		
		dbClass.sqlin ((long *) &syscolumns.tabid, SQLLONG, 0);
		dbClass.sqlin ((char *)  syscolumns.colname, SQLCHAR, 19);

		dbClass.sqlout ((short *) &syscolumns.coltype, SQLSHORT,0);
    
		coltyp_curs = dbClass.sqlcursor ("select coltype from syscolumns where tabid = ? and colname = ?");
    
		dbClass.sqlin ((char *) systables.tabname, SQLCHAR, 19);

		dbClass.sqlout ((long *)  &systables.tabid, SQLLONG, 0);

		tabid_curs = dbClass.sqlcursor ("select tabid from systables where tabname = ?");

// Hier kommt der Hauptcursor

		dbClass.sqlin ((char *) form_feld.form_nr, SQLCHAR, 25);

		dbClass.sqlout ((char *)  form_feld.agg_funk, SQLCHAR, 2);
		dbClass.sqlout ((char *)  form_feld.feld_id, SQLCHAR, 2);
		dbClass.sqlout ((char *)  form_feld.feld_nam, SQLCHAR, 19);
		dbClass.sqlout ((char *)  form_feld.feld_typ, SQLCHAR, 2);
		dbClass.sqlout ((short *) &form_feld.delstatus, SQLSHORT, 0);
		dbClass.sqlout ((char *)  form_feld.tab_nam, SQLCHAR, 19);
		dbClass.sqlout ((long *)  &form_feld.lfd, SQLLONG, 0);
    
		form_feld_curs = dbClass.sqlcursor 
			("select agg_funk, feld_id, feld_nam, feld_typ, delstatus, tab_nam, lfd from form_feld where form_nr = ?");
    
	}

	sprintf (form_feld.form_nr, "test1");

	dbClass.sqlopen (form_feld_curs);

	while (dbClass.sqlfetch (form_feld_curs) == 0)
	{
		ll_typ = holetyp(form_feld.tab_nam , form_feld.feld_nam );

		form_feld.tab_nam[0] = toupper(form_feld.tab_nam[0]) ;
		form_feld.feld_nam[0] = toupper(form_feld.feld_nam[0]) ;
		CString s = form_feld.tab_nam ;
		s.TrimRight() ;
		strcpy ( form_feld.tab_nam, s ) ;
		s = form_feld.feld_nam ;
		s.TrimRight() ;
		strcpy ( form_feld.feld_nam, s ) ;
		sprintf( tabdotfeld , "%s.%s", form_feld.tab_nam, form_feld.feld_nam);
		sprintf ( defabeleg, "%s", form_feld.feld_nam);
		if (ll_typ == LL_NUMERIC )
		{
			sprintf ( defabeleg, "12345678");
		};
	
			LlDefineFieldExt(hJob, tabdotfeld, defabeleg,ll_typ,NULL);

	}
********************************/
}


