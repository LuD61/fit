#ifndef _PRSTATATAG_DEF
#define _PRSTATATAG_DEF

struct PRSTATATAG {
   short          mdn;
   char           dat[12];
   double         a;
   char           rez[9];
   double         me;
   double         chg;
   double         mat_o_b;
   double         hk_teilk;
   double         hk_vollk;
   short          variante;
   short          planstatus;
   long           phase1;
   long           phase2;
   long           phase3;
   long           phase4;
   long           phase5;
   short          aufteilung;
};
extern struct PRSTATATAG prstatatag, prstatatag_null;
extern char ProdAbt [12];


#line 5 "prstatatag.rh"

class PRSTATATAG_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
			   int leseprstatatag (int);
			   int openprstatatag (void);
			   int dbcount (void);
               PRSTATATAG_CLASS () : DB_CLASS ()
               {
               }
};
#endif
