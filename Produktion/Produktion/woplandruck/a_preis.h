#ifndef _A_PREIS_DEF
#define _A_PREIS_DEF


struct A_PREIS {
   long           mdn;
   short          fil;
   double         a;
   short          prodabt;
   short          druckfolge;
   double         ek;
   double         plan_ek;
   double         bep;
   double         plan_bep;
   double         kostenkg;
   double         kosteneinh;
   short          flg_bearb_weg;
   short          bearb_hk;
   short          bearb_p1;
   short          bearb_p2;
   short          bearb_p3;
   short          bearb_p4;
   short          bearb_p5;
   short          bearb_p6;
   short          bearb_vk;
   double         sp_sk;
   double         sp_fil;
   double         sp_vk;
   char           dat[12];
   char           zeit[6];
   char           pers_nam[9];
   long           inh_ek;
};
extern struct A_PREIS a_preis, a_preis_null;

#line 5 "a_preis.rh"

class A_PREIS_CLASS : public DB_CLASS
{
       private :
       public :
               void prepare (void);
               int lesea_preis (void);
               int opena_preis (void);
               A_PREIS_CLASS () : DB_CLASS ()
               {
               }
};
#endif
