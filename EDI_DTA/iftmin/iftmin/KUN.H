#ifndef _KUN_DEF
#define _KUN_DEF

struct KUN {

short mdn;
short fil;
long kun;
long adr1;
long adr2;  
long adr3; 
char kun_seit[12];
long txt_nr1;
char frei_txt1[65];
char kun_krz1[17];
char kun_bran[2];
char kun_krz2[17];
char kun_krz3[17];
short kun_typ;
long bbn;
short pr_stu;
long pr_lst;
char vereinb[6];
long inka_nr;
long vertr1;
long vertr2;
char statk_period[2];
char a_period[2];
short sprache;
long txt_nr2;
char frei_txt2[65];
char freifeld1[9];
char freifeld2[9];
long tou;
char vers_art[3];
short lief_art;
char fra_ko_ber[3];
short rue_schei;
char form_typ1[3];
short auflage1;
char freifeld3[9];
char freifeld4[9];
short zahl_art;
short zahl_ziel;
char form_typ2[3];
short auflage2;
long txt_nr3;
char frei_txt3[65];
char nr_bei_rech[17];
char rech_st[3];
short sam_rech;
short einz_ausw;
short gut;
char rab_schl[9];
double bonus1;
double bonus2;
double tdm_grenz1;
double tdm_grenz2;
double jr_plan_ums;
char deb_kto[9];
double kred_lim;
short inka_zaehl;
char bank_kun[37];
long blz;
char kto_nr[17];
short hausbank;
short kun_of_po;
short kun_of_lf;
short kun_of_best;
short delstatus;
char kun_bran2[3];
long rech_fuss_txt;
long ls_fuss_txt;
char ust_id[12];
long rech_kopf_txt;
long ls_kopf_txt;
char gn_pkt_kz[2];
char sw_rab[2];
char bbs[9];
long inka_nr2;
short sw_fil_gr;
short sw_fil;
char ueb_kz[2];
char modif[2];
short kun_leer_kz;
char ust_id16[17];
char iln[17];
short waehrung;
short pr_ausw;
char pr_hier[2];
short pr_ausw_ls;
short pr_ausw_re;
short kun_gr1;
short kun_gr2;
short eg_kz;
short bonitaet;
short kred_vers;
long kst;
char edi_typ[2];
long sedas_dta;
char sedas_kz[3];
short sedas_umf;
short sedas_abr;
short sedas_gesch;
short sedas_satz;
short sedas_med;
char sedas_nam[11];
short sedas_abk1;
short sedas_abk2;
short sedas_abk3;
char sedas_nr1[9];
char sedas_nr2[9];
char sedas_nr3[9];
short sedas_vb1;
short sedas_vb2;
short sedas_vb3;
char sedas_iln[17];
long kond_kun;
short kun_schema;
char plattform[17];
char be_log[4];
long stat_kun;
char ust_nummer[25];
char rechiln[17];

};

extern struct KUN kun, kuninka, lesekun, kun_null;

class KUN_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int lesekun (void);
               int openkun (void);
//			   int openallkun(void);

			   long itvon ;
			   long itbis ;

               KUN_CLASS () : DB_CLASS ()
               {
               }
};

extern class KUN_CLASS kun_class ;

struct FIL {

char abr_period[2] ;
long adr;
long adr_lief;
short afl;
char auf_typ[2];
char best_kz[2];
char bli_kz[2];
char dat_ero[12];
short daten_mnp;
short delstatus;
short fil;
char fil_kla[2];
short fil_gr;
double fl_lad;
double fl_nto;
double fl_vk_ges;
short frm;
char iakv[12];
char inv_rht[2];
long kun;
char lief[17];
char lief_rht[2];
long lief_s;
char ls_abgr[2];
char ls_kz[2];
short ls_sum;
short mdn;
char pers[13];
short pers_anz;
char pos_kum[2];
char pr_ausw[2];
char pr_bel_entl[2];
char pr_fil_kz[2];
long pr_lst;
char pr_vk_kz[2];
double reg_bed_theke_lng;
double reg_kt_lng;
double reg_kue_lng;
double reg_lng;
double reg_tks_lng;
double reg_tkt_lng;
char ret_entl[2];
char smt_kz[2];
short sonst_einh;
short sprache;
char sw_kz[2];
long tou;
char umlgr[2];
char verk_st_kz[2];
short vrs_typ;
char inv_akv[2];
double planumsatz;

};

extern struct FIL fil, fil_save, fil_null;

class FIL_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int dbcount (void);
               int lesefil (void);
               int openfil (void);
               FIL_CLASS () : DB_CLASS ()
               {
               }
};



struct KUNBRTXTZU {

char kun_bran2[3] ;
long kopf_txt;
long fuss_txt;

};

extern struct KUNBRTXTZU kunbrtxtzu, kunbrtxtzu_save, kunbrtxtzu_null;

class KUNBRTXTZU_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int lesebrantext (void);
               KUNBRTXTZU_CLASS () : DB_CLASS ()
               {
               }
};



#endif

