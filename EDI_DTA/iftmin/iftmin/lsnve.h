#ifndef _LSNVE_DEF
#define _LSNVE_DEF

struct LSNVE {
    short mdn ;
	short fil ;
	long nve_posi ;
    char nve[31] ;
    char nve_palette[31] ;
	long ls ;
	double a ;
	double me ;
	short me_einh ;
	short gue ;
	short palette_kz ;
	TIMESTAMP_STRUCT tag ;
	TIMESTAMP_STRUCT mhd ;
	double brutto_gew ;
	long verp_art ;
	double ps_art ;
	double ps_stck ;
	double ps_art1 ;
	double ps_stck1 ;
	// neue Felder
	double netto_gew ;
	char masternve[31] ;
	char ls_charge[31] ;
	double lief_me ;

};
extern struct LSNVE lsnve, lsnve_null, lsnve_save, lsnve_savem ;

extern class LSNVE_CLASS lsnve_class ;


class LSNVE_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int lesenveposi (void);
               int opennveposi (void);
               int lesenve0 (void);
               int opennve0 (void);
               int lesenve1 (void);
               int opennve1 (void);
               int lesenvea (void);	// Masternve
               int opennvea (void);
               int lesenveb (void);	// Sandwich-Paletten
			   int opennveb (void);
               int lesenvec (void);	// Kiste auf Palette
               int opennvec (void);
               int lesenvek (void);	// Einzelkiste OHNE Pal-Zuordnung
               int opennvek (void);
               LSNVE_CLASS () : DB_CLASS ()
               {
               }
};
#endif

