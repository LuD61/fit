#ifndef _ZUDAX_DEF
#define _ZUDAX_DEF


struct ZUDAX {
short mdn;
long  kun;
char  be_logd[3];
char  abfert[2];
short lief_art;
char  dfrank [4];
char aufgruppe[3] ;	// 220311
short sped;
};
 
extern struct ZUDAX zudax, zudax_null;
extern class ZUDAX_CLASS zudax_class ;


class ZUDAX_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int lesezudax (void);
               int openzudax (void);
               int writezudax (void);
               int deletezudax (void);
               ZUDAX_CLASS () : DB_CLASS ()
               {
               }
};

#endif

