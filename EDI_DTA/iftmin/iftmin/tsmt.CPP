#include "stdafx.h"
#include "DbClass.h"
#include "tsmt.h"

struct TSMTG tsmtg,  tsmtg_null;
struct TSMTGG tsmtgg,  tsmtgg_null;

extern DB_CLASS dbClass;

class TSMTG_CLASS tsmtg_class ;
class TSMTGG_CLASS tsmtgg_class ;

static int dta_dta ;

int TSMTG_CLASS::lesetsmtg ( )
{

      int di = dbClass.sqlfetch (readcursor);

	  return di;
}


int TSMTGG_CLASS::lesetsmtgg (char kubra )
{

	int di ;

	if ( kubra == 'b' )	// lesen auf "Branche"
	{
      di = dbClass.sqlfetch (readbbeding);
	}
	else
	{
		      di = dbClass.sqlfetch (readabeding);
	}
	  return di;
}


int TSMTG_CLASS::opentsmtg (void)
{

		if ( readcursor < 0 ) prepare ();
		
         return dbClass.sqlopen (readcursor);
}

int TSMTGG_CLASS::opentsmtgg (char kubra,long dtax)
{

		dta_dta = dtax ;
	
		if ( readcursor < 0 ) prepare (dtax);
		
		if ( kubra == 'b' )
		{
			return dbClass.sqlopen (readbbeding);
		}
		else
		{
			return dbClass.sqlopen (readabeding);
		}
}

void TSMTG_CLASS::prepare (void)
{

	dbClass.sqlin  ((short *) &tsmtg.mdn, SQLSHORT, 0);
	dbClass.sqlin  ((long *) &tsmtg.k_tsmt_gr, SQLLONG, 0);

	dbClass.sqlout ((long *) &tsmtg.tsmt_gr, SQLLONG, 0);
    dbClass.sqlout ((long *) &tsmtg.abkommen, SQLLONG, 0);

 
	readcursor =
		(short) dbClass.sqlcursor ("select distinct tsmt_gr, abkommen from tsmtg where mdn = ? "
		" and k_tsmt_gr = ?  order by 1 ") ;
	
}

void TSMTGG_CLASS::prepare (long dtax)
{

	dta_dta = dtax ;
	readcursor = 1 ;	// dummy-marker .....

	dbClass.sqlin  ((long *)  &dta_dta, SQLLONG, 0);
	dbClass.sqlin  ((short *) &tsmtgg.mdn, SQLSHORT, 0);

	dbClass.sqlout ((long *) &tsmtgg.k_tsmt_gr, SQLLONG, 0);
   
 
	readabeding =
		(short) dbClass.sqlcursor ("select distinct k_tsmt_gr from  tsmtgg,kun "
		" where kun.sedas_dta = ? "
		" and kun.kun = kun.inka_nr "
		" and kun.edi_typ = \"E\" "
		" and kun.mdn = ? "
		" and kun.mdn = tsmtgg.mdn "
		" and kun.kun = tsmtgg.kun "
		"and tsmtgg.kun_bran2 = \"0\" "
		" order by 1" ) ;
	
	dbClass.sqlin  ((long *)  &dta_dta, SQLLONG, 0);
	dbClass.sqlin  ((short *) &tsmtgg.mdn, SQLSHORT, 0);

	dbClass.sqlout ((long *) &tsmtgg.k_tsmt_gr, SQLLONG, 0);
   
 
	readbbeding =
		(short) dbClass.sqlcursor ("select distinct k_tsmt_gr from  tsmtgg,kun "
		" where kun.sedas_dta = ? "
		" and kun.kun = kun.inka_nr "
		" and kun.edi_typ = \"E\" "
		" and kun.mdn = ? "
		" and kun.mdn = tsmtgg.mdn "
		" and tsmtgg.kun = 0 "
		"and tsmtgg.kun_bran2 = kun.kun_bran2 "
		" order by 1" ) ;
}









