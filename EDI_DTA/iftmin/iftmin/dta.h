#ifndef _DTA_DEF
#define _DTA_DEF

// Das sind die fit-Typen -> falls jemand ptabs aendern moechte, soll er es nur mal versuchen !!!!!!
// Abgeglichenmit MEINER PTAB am 08.01.2011

#define EDEKA            1 
#define KARSTADT         2
#define KAUFRING         3
#define MARKANT          4
#define METRO            5
#define REWE             6
#define TENGELMANN       7
#define EK_GH_EG		 8
#define ZIMBO            9

#define CHRIST          10
#define SPAR            11
#define KAUFLAND        12	
#define KONSUM			13	
#define WALMART         14

#define SELGROS			21	// z.Z edel-Boesi 
#define BILLA			31	// DESADV

#define NAGEL			51	// IFTMIN ?
#define DACHSER			52	// IFTMIN ?



#define CSB_FG			100		// unterscheiden sich in dta und Dateiname !!!!
#define FG_EDI			101		// Frankengut-orders und -iftmin mit externen Pos-Nummern 

#define SWH_EDI			201		// SWH-Orders und -iftmin mit exteren Pos-Nummern
// alle Nummern zwischen 200 und 299 gehoeren dem Schwarzwaldhof ......

struct DTA {

    long dta ;
    char kun_krz1[17] ;
	long adr ;
    char iln[17] ;
    short typ ;				// METRO,REWE,TENGELMANN ......
    short test_kz ;			// 1 => testkz wird generiert ( INVOICE)
    short waehrung ;		// 2 => EURO ,1 => DM
    char dat_name[17];		// Hilfssegmente Dateiname ( z.B. FG-DESADV)
    long nummer;			// lfd-Nr. Datei (z.B. FG-DESADV)
    short pri_seg;			// Preissegment unterdruecken ( z.B. FG-DESADV)
	short reli ;			// per Rechnungsliste selektieren
	short optiondesadv ;	// 130411 : Optionsschalter aktivieren
};

extern struct DTA dta, dta_null, dta_gelesen ;

extern class DTA_CLASS dta_class ;

class DTA_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int dbcount (void);
               int lesedta (void);
               int lesealldta (void);
               int opendta (void);
               int openalldta (void);
			   int updatedta (void);
			   int insertdta(void);

               DTA_CLASS () : DB_CLASS ()
               {
               }
};
#endif

