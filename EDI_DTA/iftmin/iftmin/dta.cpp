#include "stdafx.h"
#include "DbClass.h"
#include "dta.h"

struct DTA dta,  dta_null , dta_gelesen;

extern DB_CLASS dbClass;

DTA_CLASS dta_class ;

static int anzzfelder ;

int DTA_CLASS::dbcount (void)
/**
Tabelle dta lesen.
**/
{

         if (test_upd_cursor == -1)
         {
             prepare ();
         }
         dbClass.sqlopen (count_cursor);
         dbClass.sqlfetch (count_cursor);
         if (sqlstatus == 0)
         {
                      return anzzfelder;
         }
         return sqlstatus;  
}

int DTA_CLASS::lesedta (void)
{
	long savedta = dta.dta ;	// diese bloeden null-values .......
	memcpy ( &dta,&dta_null, sizeof(struct DTA));

	int di = dbClass.sqlfetch (readcursor);
	dta.dta = savedta ;
	return di;
}

int DTA_CLASS::lesealldta (void)
{
	  memcpy ( &dta,&dta_null, sizeof(struct DTA));
      int di = dbClass.sqlfetch (readallcursor);

	  return di;
}

int DTA_CLASS::opendta (void)
{

		if ( readcursor < 0 ) prepare ();
		
         return dbClass.sqlopen (readcursor);
}

int DTA_CLASS::openalldta (void)
{

		if ( readallcursor < 0 ) prepare ();
		
         return dbClass.sqlopen (readallcursor);
}

int DTA_CLASS::updatedta (void)
{

		if ( upd_cursor < 0 ) prepare ();
         return dbClass.sqlexecute (upd_cursor);
}

int DTA_CLASS::insertdta (void)
{

		if ( ins_cursor < 0 ) prepare ();
         return dbClass.sqlexecute (ins_cursor);
}


void DTA_CLASS::prepare (void)
{

// readcursor

	dbClass.sqlin ((long *) &dta.dta, SQLLONG, 0);

    dbClass.sqlout ((char *)  dta.kun_krz1,	SQLCHAR, 17 ) ;
	dbClass.sqlout ((long *) &dta.adr,		SQLLONG,  0 ) ;
    dbClass.sqlout ((char *)  dta.iln,		SQLCHAR, 17 ) ;
    dbClass.sqlout ((short *)&dta.typ,		SQLSHORT, 0 ) ;			
    dbClass.sqlout ((short *)&dta.test_kz,	SQLSHORT, 0 ) ;		
    dbClass.sqlout ((short *)&dta.waehrung,	SQLSHORT, 0 ) ;	
    dbClass.sqlout ((char *)  dta.dat_name,	SQLCHAR, 17 ) ;
    dbClass.sqlout ((long *) &dta.nummer,	SQLLONG,  0 ) ;	
	dbClass.sqlout ((short *)&dta.pri_seg,	SQLSHORT, 0 ) ;	
	dbClass.sqlout ((short *)&dta.reli,		SQLSHORT, 0 ) ;	
	dbClass.sqlout ((short *)&dta.optiondesadv,SQLSHORT, 0 ) ;	// 130411	
	

		readcursor = (short) dbClass.sqlcursor ("select "

    " kun_krz1, adr, iln, typ, test_kz, waehrung "
	" , dat_name, nummer, pri_seg, reli ,optiondesadv "
	" from dta where dta = ? ") ;

// readallcursor

	dbClass.sqlout ((long *) &dta.dta, SQLLONG, 0);
	dbClass.sqlout ((char *)  dta.kun_krz1,	SQLCHAR, 17 ) ;
	dbClass.sqlout ((long *) &dta.adr,		SQLLONG,  0 ) ;
    dbClass.sqlout ((char *)  dta.iln,		SQLCHAR, 17 ) ;
    dbClass.sqlout ((short *)&dta.typ,		SQLSHORT, 0 ) ;			
    dbClass.sqlout ((short *)&dta.test_kz,	SQLSHORT, 0 ) ;		
    dbClass.sqlout ((short *)&dta.waehrung,	SQLSHORT, 0 ) ;	
    dbClass.sqlout ((char *)  dta.dat_name,	SQLCHAR, 17 ) ;
    dbClass.sqlout ((long *) &dta.nummer,	SQLLONG,  0 ) ;	
	dbClass.sqlout ((short *)&dta.pri_seg,	SQLSHORT, 0 ) ;	
	dbClass.sqlout ((short *)&dta.reli,		SQLSHORT, 0 ) ;
	dbClass.sqlout ((short *)&dta.optiondesadv,	SQLSHORT, 0 ) ;	// 130411

		readallcursor = (short) dbClass.sqlcursor ("select "

    " dta, kun_krz1, adr, iln, typ, test_kz, waehrung "
	" , dat_name, nummer, pri_seg, reli , optiondesadv "
	" from dta order by dta ") ;
/* ----> brauchet mer hier nicht 
// ins_cursor 

	dbClass.sqlin ((long *) &dta.dta, SQLLONG, 0);
	dbClass.sqlin ((char *)  dta.kun_krz1,	SQLCHAR, 17 ) ;
	dbClass.sqlin ((long *) &dta.adr,		SQLLONG,  0 ) ;
    dbClass.sqlin ((char *)  dta.iln,		SQLCHAR, 17 ) ;
    dbClass.sqlin ((short *)&dta.typ,		SQLSHORT, 0 ) ;			

	dbClass.sqlin ((short *)&dta.test_kz,	SQLSHORT, 0 ) ;		
    dbClass.sqlin ((short *)&dta.waehrung,	SQLSHORT, 0 ) ;	
    dbClass.sqlin ((char *)  dta.dat_name,	SQLCHAR, 17 ) ;
    dbClass.sqlin ((long *) &dta.nummer,	SQLLONG,  0 ) ;	
	dbClass.sqlin ((short *)&dta.pri_seg,	SQLSHORT, 0 ) ;	

	dbClass.sqlin ((short *)&dta.reli,		SQLSHORT, 0 ) ;
	dbClass.sqlin ((short *)&dta.optiondesadv,SQLSHORT, 0 ) ;	// 130411

		ins_cursor = (short) dbClass.sqlcursor ("insert into dta (  "

    " dta, kun_krz1, adr, iln, typ "
	" , test_kz, waehrung , dat_name, nummer , pri_seg "
	" , reli , optiondesadv "
	" ) values ( "
	 " ?, ?, ?, ?, ? "
	" , ?, ?, ?, ?, ? "
	" , ?, ? ) " ) ;
< ----- */

// updcursor 


	dbClass.sqlin ((char *)  dta.kun_krz1,	SQLCHAR, 17 ) ;
	dbClass.sqlin ((long *) &dta.adr,		SQLLONG,  0 ) ;
    dbClass.sqlin ((char *)  dta.iln,		SQLCHAR, 17 ) ;
    dbClass.sqlin ((short *)&dta.typ,		SQLSHORT, 0 ) ;			

	dbClass.sqlin ((short *)&dta.test_kz,	SQLSHORT, 0 ) ;		
    dbClass.sqlin ((short *)&dta.waehrung,	SQLSHORT, 0 ) ;	
    dbClass.sqlin ((char *)  dta.dat_name,	SQLCHAR, 17 ) ;
    dbClass.sqlin ((long *) &dta.nummer,	SQLLONG,  0 ) ;	
	dbClass.sqlin ((short *)&dta.pri_seg,	SQLSHORT, 0 ) ;	

	dbClass.sqlin ((short *)&dta.reli,		SQLSHORT, 0 ) ;
	dbClass.sqlin ((short *)&dta.optiondesadv, SQLSHORT, 0 ) ;	// 130411


	dbClass.sqlin ((long *) &dta.dta, SQLLONG, 0);


		upd_cursor = (short) dbClass.sqlcursor ("update dta set  "

    "  kun_krz1 = ? , adr = ? , iln = ? , typ = ? "
	" , test_kz = ? , waehrung = ? , dat_name = ? , nummer = ?  , pri_seg = ?  "
	" , reli = ? , optiondesadv = ? "
	" where dta = ? " ) ;

}

