// iftmin.cpp : Definiert das Klassenverhalten f�r die Anwendung.
//

#include "stdafx.h"
#include "iftmin.h"
#include "iftminDlg.h"
#include "token.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CiftminApp

BEGIN_MESSAGE_MAP(CiftminApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CiftminApp-Erstellung

CiftminApp::CiftminApp()
{
	// TODO: Hier Code zur Konstruktion einf�gen
	// Alle wichtigen Initialisierungen in InitInstance positionieren
}


// Das einzige CiftminApp-Objekt

CiftminApp theApp;


// CiftminApp-Initialisierung

BOOL CiftminApp::InitInstance()
{
	// InitCommonControlsEx() ist f�r Windows XP erforderlich, wenn ein Anwendungsmanifest
	// die Verwendung von ComCtl32.dll Version 6 oder h�her zum Aktivieren
	// von visuellen Stilen angibt. Ansonsten treten beim Erstellen von Fenstern Fehler auf.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Legen Sie dies fest, um alle allgemeinen Steuerelementklassen einzubeziehen,
	// die Sie in Ihrer Anwendung verwenden m�chten.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	// Standardinitialisierung
	// Wenn Sie diese Features nicht verwenden und die Gr��e
	// der ausf�hrbaren Datei verringern m�chten, entfernen Sie
	// die nicht erforderlichen Initialisierungsroutinen.
	// �ndern Sie den Registrierungsschl�ssel, unter dem Ihre Einstellungen gespeichert sind.
	// TODO: �ndern Sie diese Zeichenfolge entsprechend,
	// z.B. zum Namen Ihrer Firma oder Organisation.
	SetRegistryKey(_T("Vom lokalen Anwendungs-Assistenten generierte Anwendungen"));

	CiftminDlg dlg;
	m_pMainWnd = &dlg;

// Kommando-Zeile interpretieren
	// Befehlszeile erlaubt f�r einen Beleg : dta mdn ls 
	CCommandLineInfo cmdInfo;
    char *px;
	LPSTR CommandLine = GetCommandLine ();

	CToken Token (CommandLine, " ");

	char globdta[105] ;
	char globmdn[35] ;
	char globls[35] ;

	sprintf ( globdta, "0" ) ;
	sprintf ( globmdn, "0" ) ;
	sprintf ( globls, "0" ) ;

    Token.NextToken ();
	if ((px = Token.NextToken ()) != NULL)
	{
		if ( px[0] != '\0' && px[0] != ' ' )
				sprintf ( globdta, "%s" , px ) ;

		if ((px = Token.NextToken ()) != NULL)
		{
			if ( px[0] != '\0' && px[0] != ' ' )
				sprintf ( globmdn, "%s" , px ) ;

			if ((px = Token.NextToken ()) != NULL)
			{
				if ( px[0] != '\0' && px[0] != ' ' )
					sprintf ( globls, "%s" , px ) ;

			}
		
		}

	}

	dlg.paras ( globdta, globmdn , globls ) ;


	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: F�gen Sie hier Code ein, um das Schlie�en des
		//  Dialogfelds �ber "OK" zu steuern
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: F�gen Sie hier Code ein, um das Schlie�en des
		//  Dialogfelds �ber "Abbrechen" zu steuern
	}

	// Da das Dialogfeld geschlossen wurde, FALSE zur�ckliefern, sodass wir die
	//  Anwendung verlassen, anstatt das Nachrichtensystem der Anwendung zu starten.
	return FALSE;
}
