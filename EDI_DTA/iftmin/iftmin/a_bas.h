#ifndef _A_BAS_DEF
#define _A_BAS_DEF


// Das ist irgend so ein Stücklistenelement .... 
struct A_STK {
double a ;
double a_stkl ;
double me ;
long stkl ;
double pr_vk ;
short pr_typ ;
};

extern struct A_STK a_stk, a_stk_null;

class A_STK_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int lesea_stk (void);
               int opena_stk (void);
               A_STK_CLASS () : DB_CLASS ()
               {
               }
};

struct A_EAN {
double a ;
short delstatus ;
double ean ;
char ean_bz[25] ;
char h_ean_kz[2] ;
short ean_vk_kz ;
};

extern struct A_EAN a_ean, a_ean_null;

class A_EAN_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int lesea_ean (void);
               int opena_ean (void);
               A_EAN_CLASS () : DB_CLASS ()
               {
               }
};

struct A_BAS {
	double a ;
	short mdn ;
	short fil ;
	char a_bz1[25];
	char a_bz2[25] ;
	double a_gew ;
	short a_typ ;
	short a_typ2 ;
	short abt ;
	long ag ;
	char best_auto[2] ;
	char bsd_kz [2] ;
	char cp_aufschl[2] ;
	short delstatus ;
	short dr_folge ;
	long erl_kto ;
	char hbk_kz [2] ;
	short hbk_ztr ;
	char hnd_gew [2] ;
	short hwg ;
	char kost_kz [3] ;
	short me_einh ;
	char modif [2] ;
	short mwst ;
	short plak_div ;
	char stk_lst_kz [2] ;
	double sw ;
	short teil_smt ;
	long we_kto ;
	short wg ;
	short zu_stoff ;
	char akv [11]; 
	char bearb [11];
	char pers_nam [9] ;
	double prod_zeit ;
	char pers_rab_kz [2];
	double gn_pkt_gbr ;
	long kost_st ;
	char sw_pr_kz[2] ;
	long kost_tr ;
	double a_grund ;
	long kost_st2 ;
	long we_kto2 ;
	long charg_hand ;
	long intra_stat ;
	char qual_kng [5] ; 
	char a_bz3 [25] ;
	short lief_einh ;
	double inh_lief ;
	long erl_kto_1 ;
	long erl_kto_2 ;
	long erl_kto_3 ;
	long we_kto_1 ;
	long we_kto_2 ;
	long we_kto_3 ;
	char skto_f [2] ;
	double sk_vollk ;
	double a_ersatz ;
	short a_ers_kz ;
	short me_einh_abverk ;
	double inh_abverk ;
	char hnd_gew_abverk [2] ;
	
};
extern struct A_BAS a_bas, a_bas_null;
extern class A_BAS_CLASS a_bas_class ;

class A_BAS_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int dbcount (void);
               int lesea_bas (void);
               int opena_bas (void);
               A_BAS_CLASS () : DB_CLASS ()
               {
               }
};


struct A_KUN {

	short mdn ;
	short fil ;
	long kun ;
	double a ; 
	char a_kun[14] ;
	char a_bz1[25] ;
	short me_einh_kun ;
	double inh ;
	char kun_bran2[3] ;
	double tara ;
	double ean ;
	double ean_vk ;
	char a_bz2[25] ;
	short hbk_ztr ; 
	long kopf_text ;
	char pr_rech_kz[2];
	char modif[2] ;
	long text_nr ;
	short devise ;
	char geb_eti[2] ;
	char geb_fill[2] ;
	long geb_anz ;
	char pal_eti[2] ;
	char pal_fill[2] ;
	short pal_anz ; 
	char pos_eti[2] ;
	short sg1 ;
	short sg2 ;
	short pos_fill ;
	short ausz_art ;
	long text_nr2 ;
	short cab ;
	char a_bz3[25] ;
	char a_bz4[25] ;
	char li_a[14] ;         
	double geb_fakt ;

};
extern struct A_KUN a_kun, a_kun_null;

class A_KUN_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
//               int dbcount (void);

			int lesea_kun (void);
			int opena_kun (void);
//			int holea_kun ( short mdn , double a , long kun, char * kun_bran2 ) ;
			   A_KUN_CLASS () : DB_CLASS ()
               {
               }
};
extern class A_KUN_CLASS a_kun_class;

// Struktur nur notwendiges subset ....
struct A_KUN_GX {

	short mdn ;
	short fil ;
	long kun ;
	double a ; 
	char kun_bran2[3] ;
	double ean ;
	double ean1 ;
};

extern struct A_KUN_GX a_kun_gx, a_kun_gx_null;
extern class A_KUN_GX_CLASS a_kun_gx_class ;

class A_KUN_GX_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int open_ean_a_kun_gx (double , double, double);
               int lese_ean_a_kun_gx (void);

               int opena_kun_gx (void);
               int lesea_kun_gx (void);



			   double ia ;
			   char ia_kun[20] ;
			   double iistean ;
			   double ivonean ;
			   double ibisean ;

			   A_KUN_GX_CLASS () : DB_CLASS ()
               {
               }
};


#endif

