// iftminDlg.h : Headerdatei
//

#pragma once
#include "afxwin.h"


// CiftminDlg-Dialogfeld
class CiftminDlg : public CDialog
{
// Konstruktion
public:
	CiftminDlg(CWnd* pParent = NULL);	// Standardkonstruktor

// Dialogfelddaten
	enum { IDD = IDD_DESADV_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV-Unterst�tzung


// Implementierung
protected:
	HICON m_hIcon;

	// Generierte Funktionen f�r die Meldungstabellen
	virtual BOOL OnInitDialog();
	afx_msg BOOL PreTranslateMessage(LPMSG) ;
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CEdit m_mdnnr;
	CString v_mdnnr;
	CEdit m_mdnname;
	CString v_mdnname;
	CEdit m_tour;
	CString v_tour;
	CEdit m_vdat;
	CString v_vdat;
	CEdit m_bdat;
	CString v_bdat;
	afx_msg void OnEnKillfocusMdnnr();
	afx_msg void OnEnKillfocusTour();
	afx_msg void OnEnKillfocusVdat();
	afx_msg void OnEnKillfocusBdat();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedOk();
 
	void ReadMdn (void) ; 
	virtual BOOL OnReturn (void);
	virtual BOOL OnKeyup (void);

	void CiftminDlg::paras ( char * globdta , char * globmdn , char * globls  ) ;
	int CiftminDlg::Dateitesten ( int modus , char * dateiname ) ;
	int CiftminDlg::opendatei ( void ) ;
	int CiftminDlg::schreibekopfsatz ( void ) ;
	int CiftminDlg::schreibepspm (  void ) ;
	int CiftminDlg::schreibenve ( void ) ;
	int CiftminDlg::schreibekpap ( void ) ;
	int CiftminDlg::schreibeschlusssatz (void) ;
	int CiftminDlg::Dateierstellung (void) ;
	int CiftminDlg::holedefaults (void) ;

	CEdit m_melde;
	CString v_melde;
	afx_msg void OnCbnSelchangeCombo1();
	afx_msg void OnCbnKillfocusCombo1();
	CComboBox m_combo1;
	CString v_combo1;
	CString v_meldek;
	CString v_meldep;
	long interndta ;
	short internmdn ;
	long internls ;
	CComboBox m_combo2;
	CString v_combo2;
	afx_msg void OnCbnKillfocusCombo2();
};
