#ifndef _TEMPDRX_DEF
#define _TEMPDRX_DEF

struct TEMPDRX {

	char bearb[11];
	char form_nr[11];
	short lila ;
	long lfd01 ;
	long lfd02 ;
	long lfd03 ;
	long lfd04 ;
	long lfd05 ;
	long lfd06 ;
	long lfd07 ;
	long lfd08 ;
	long lfd09 ;
	long lfd10 ;
	double dfeld01 ;
	double dfeld02 ;
	double dfeld03 ;
	double dfeld04 ;
	double dfeld05 ;
	double dfeld06 ;
	double dfeld07 ;
	double dfeld08 ;
	double dfeld09 ;
	double dfeld10 ;
	double dfeld11 ;
	double dfeld12 ;
	double dfeld13 ;
	double dfeld14 ;
	double dfeld15 ;
	double dfeld16 ;
	double dfeld17 ;
	double dfeld18 ;
	double dfeld19 ;
	double dfeld20 ;

	char cfeld101[17] ;
	char cfeld102[17] ;
	char cfeld103[17] ;
	char cfeld104[17] ;
	char cfeld105[17] ;

	char cfeld201[25] ;
	char cfeld202[25] ;
	char cfeld203[25] ;
	char cfeld204[25] ;
	char cfeld205[25] ;

	char cfeld301[37] ;
	char cfeld302[37] ;
	char cfeld303[37] ;
	char cfeld304[37] ;
	char cfeld305[37] ;
	long lfd ;

};
extern struct TEMPDRX tempdrx, tempdrx_null;


extern class TEMPDRX_CLASS tempdrx_class ;

class TEMPDRX_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int inserttempdrx (void);
               int deletetempdrx (void);
			   int lesetempdrx   (void);
			   int update_lfd10   (void);
               TEMPDRX_CLASS () : DB_CLASS ()
               {
               }
};

#endif

