#include "stdafx.h"
#include "DbClass.h"
#include "zudax.h"

extern DB_CLASS dbClass;

struct ZUDAX zudax,  zudax_null;

class ZUDAX_CLASS zudax_class; 


int ZUDAX_CLASS::lesezudax (void)
{
      int di = dbClass.sqlfetch (readcursor);

	  return di;
}
int ZUDAX_CLASS::openzudax (void)
{

		if ( readcursor < 0 ) prepare ();	
		
         return dbClass.sqlopen (readcursor);
}

int ZUDAX_CLASS::writezudax (void)
{

	if ( readcursor < 0 ) prepare ();	
		
	int di = dbClass.sqlexecute (del_cursor);
        di = dbClass.sqlexecute (ins_cursor);
	return di ;
}


int ZUDAX_CLASS::deletezudax (void)
{

		if ( readcursor < 0 ) prepare ();	
		
         return dbClass.sqlexecute (del_cursor);
}



void ZUDAX_CLASS::prepare (void)
{

// fuer readcursor 

	dbClass.sqlin ((short *) &zudax.mdn, SQLSHORT,0 );
 	dbClass.sqlin ((long  *) &zudax.kun, SQLLONG, 0 );
	dbClass.sqlin ((short *) &zudax.sped,SQLSHORT,0 );

	dbClass.sqlout ((char *)  zudax.be_logd, SQLCHAR ,3 ) ;
	dbClass.sqlout ((char *)  zudax.abfert,  SQLCHAR ,2 ) ;
	dbClass.sqlout ((short *)&zudax.lief_art,SQLSHORT,0 ) ;
	dbClass.sqlout ((char *)  zudax.dfrank,  SQLCHAR ,4 ) ;
	dbClass.sqlout ((char *)  zudax.aufgruppe, SQLCHAR ,3 ) ;	// 220311

    readcursor = (short) dbClass.sqlcursor (
		"select be_logd, abfert, lief_art, dfrank, aufgruppe  "
		" from zudax where mdn = ? and kun = ? and sped = ?");

// fuer ins_cursor
	dbClass.sqlin ((short *) &zudax.mdn, SQLSHORT,0 );
 	dbClass.sqlin ((long  *) &zudax.kun, SQLLONG, 0 );
	dbClass.sqlin ((short *) &zudax.sped,SQLSHORT,0 );

	dbClass.sqlin ((char *)  zudax.be_logd, SQLCHAR ,3 ) ;
	dbClass.sqlin ((char *)  zudax.abfert,  SQLCHAR ,2 ) ;
	dbClass.sqlin ((short *)&zudax.lief_art,SQLSHORT,0 ) ;
	dbClass.sqlin ((char *)  zudax.dfrank,  SQLCHAR ,4 ) ;
	dbClass.sqlin ((char *)  zudax.aufgruppe,  SQLCHAR ,3 ) ;

    ins_cursor = (short) dbClass.sqlcursor (
		"insert into zudax  "
		" ( mdn, kun, sped, be_logd, abfert, lief_art, dfrank, aufgruppe ) "
		" values "
		" (?, ?, ?, ?, ?, ?, ?, ? )"
		);

// fuer del_cursor
	dbClass.sqlin ((short *) &zudax.mdn, SQLSHORT,0 );
 	dbClass.sqlin ((long  *) &zudax.kun, SQLLONG, 0 );
	dbClass.sqlin ((short *) &zudax.sped,SQLSHORT,0 );

    del_cursor = (short) dbClass.sqlcursor (
		"delete from  zudax where mdn = ? and kun = ? and sped = ? "
		);
}

