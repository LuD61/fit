#include "stdafx.h"  

// 1. Runde : Lesen : Teste Struktur und Stammdatenintegritaet
// falls Fehler, dann bitte abbrechen mit entsprechendem Verweis auf ein Protokoll 
// 2. Runde : hole n Auftragsnummern ( n kleine Transaktionen ) 
// 3. Runde schreibe n Auftraege aus dem Array  ( eine grosse Transaktion )
// gebnenfalls rollback oder umbenennen der Quelldatei 



// Struktur ist relativ simpel :

// "019121";"2009-04.16";"003";"00000002204";"010.000";"kg";"vac."

// Anfuehrungszeichen um jedes Feld, Semikolon zwischen Feldern , crlf als Zeilenumbruch
// 1. Spalte :fit-Kunu
// 2. Spalte :Lieferdatum(yyyy-mm.dd)
// 3. Spalte :teilsmt(externes) 
// 4. Spalte :Fit-Art-Nummer
// 5. Spalte :Bestell-Menge
// 6. Spalte :Bestell-ME (St,kg oder X)
// 7. Spalte :Zusatztext-Info
// 8. Spalte : irgendein kenner 
// 9. Spalte :Filkun-Kenner ( "2" = Kunde oder "1" == Filiale )

#include "orderdfw.h"
#include "orderDlg.h"

#include "DbClass.h"
#include "dta.h" 
#include "mdn.h"
#include "kun.h"
#include "tempdr.h"
#include "adr.h"
#include "aufk.h"
#include "aufp.h"
#include "a_bas.h"
#include "prov_satz.h"
#include "ptabn.h"
#include "bsd_buch.h"
#include "sys_par.h"
#include "mo_numme.h"
#include "DllPreise.h"
#include "token.h"

#include "Abarbeit.h"


static int testmade = 0 ;	// 130609 

static int mogel =  0 ;	// gemogelt, damit es erst mal losgeht
static int immerweiter = 1 ; // Normal: ueberspringen und Info generieren  ;  
static int ignoresegment = 0 ; // Normal : abbrechen, Sonderwunsch : nur ewig meckern
static int doption = 0 ;
static int dplattform = 0 ;

static double tausendfaktor = 0.0 ;

static long lfdliste ;	// Listennummer fuer Ausdruck
static long lfdlistennr ;	// lfd in der Liste

extern DB_CLASS dbClass;
extern TEMPDRX_CLASS tempdrx_class ;
extern PTABN_CLASS ptabn_class ;
extern ADR_CLASS adr_class ;
extern AUFK_CLASS aufk_class ;
extern AUFP_CLASS aufp_class ;
extern AUFPNR_CLASS aufpnr_class ;
extern AUFPT_CLASS aufpt_class ;

extern KUN_CLASS kun_class ;
extern FIL_CLASS fil_class ;

extern A_BAS_CLASS a_bas_class ;
extern A_KUN_CLASS a_kun_class ;
extern A_KUN_GX_CLASS a_kun_gx_class ;
extern A_EAN_CLASS a_ean_class ;

extern BSD_BUCH_CLASS bsd_buch_class ;
extern SYS_PAR_CLASS sys_par_class ; 
extern LGR_CLASS lgr_class ;

extern A_HNDW_CLASS a_hndw_class ;
extern A_EIG_CLASS a_eig_class ;
extern A_EIG_DIV_CLASS a_eig_div_class ;

extern TSMTG_CLASS tsmtg_class ;
extern TSMTGG_CLASS tsmtgg_class ;
extern PROV_SATZ_CLASS prov_satz_class ;

CDllPreise DllPreise ;

#define  TRENN_SE        "'"       // Segmentende
#define  TRENN_DE        "+"       // Datenelemente+Segmentbezeichner
#define  TRENN_GD        ":"       // Gruppendatenelemente
#define  TRENN_MA        "?"       // Maskierungs(freigabe-)zeichen
#define	 TRENN_DZ        "."       // Dezimal-Trenner    
#define CIPPUFFLEN      1000      // Laenge Input-Puffer
#define MAXDIMNUM		1000	// maximal 1000 Auftraege in einer Datei 

static char trenn_se[2] ;
static char trenn_de[2] ;
static char trenn_gd[2] ;
static char trenn_ma[2] ;
static char trenn_dz[2] ;

static long num_array[MAXDIMNUM] ;

// Ptrototyping 

void write_belkopf_a ( void ) ;
void write_belpos_a  ( void ) ;

int read_belkopf_a ( void ) ;
int read_belpos_a  ( void ) ;


static int dpos_zahl = 0 ;
static int teilsmtbeacht ;

int teilsmttrennen = 0 ;
int teilsmtnochtrennen = 0 ;


char branche0 [4] ;
char branche1 [4] ;
char branche2 [4] ;
char branche3 [4] ;
char branche4 [4] ;

char teilsort0 [110] ;
char teilsort1 [110] ;
char teilsort2 [110] ;
char teilsort3 [110] ;
char teilsort4 [110] ;

static short dlgr_gr = -1 ;
static short dli_a_edi = 0 ;
static short dlgr_bestand = 0 ;
static char buffer[1000] ;	// hilfsvariable fuer string-handling


long AUF_DATEI ;	// sollte immer identisch mit danz_nachrichten laufen .......

struct BELKOPF
{

long auf_datei ; // beginnt immer bei 1  je Datei
long auf_int ;	// Auftragsnummer aus Nummernverwaltung
char kun_bran2[4] ;	// 2600808
char lieferdat_feld [21] ;	// Lieferdatum ( und gleich komm_dat !!?? )
long int_kun ;	// Kunde aufgeloest -> falls == 0 , dann error == unbekannt 
				// Kunde aufgeloest -> falls == -1, dann error == mehrfach
long int_adr ;
char kun_krz1[37] ;
long tou ;
long vertr ;
char bemerkung[61] ;	// Kommentar/Kurzfehler ( oder ftx-zzz-segment )
long gruppe3	;		// Gruppe aus portal
int filkun ;			// 280910 : kun=0, fil=1 aus Portal oder Dialog 
} ;

struct BELKOPF belkopf , belkopf_null ;
struct BELKOPF * belkopf_a ;

static int belkopfdim ;
static int belkopfmax ;
static int belkopfakt ;


struct BELPOS
{
long auf_datei ;		// die finale referenz ( siehe Kopf )
char bemerkung[61] ;	// Kommentar/Kurzfehler
char * zutext ;			// Zusatztext

// ab hier : felder, die 1:1 in aufp getragen werden muessen
double a ;
double auf_me ;
double auf_vk_pr ;
double auf_lad_pr ;
double prov_satz ;
char auf_me_bz[8]  ;
char lief_me_bz[8] ;
short me_einh_kun ;
short me_einh ;
short sa_kz_sint ;
short teil_smt ;
short dr_folge ;
double a_grund ;
double inh ;
} ;

struct BELPOS belpos , belpos_null ;

struct BELPOS * belpos_a ;
static int belposdim ;
static int belposmax ;
static int belposakt ;


int dtestignore ;			 
int dfilkun ;	// 210910 0 = Kunde 1 = Filiale (neu )
// 280910 : nicht interaktiv, sondern als Schalter ( neue Struktur !! ) je Satz 
int TEST =  0 ;					// 0 * inaktiv

int k ;

FILE * fpin ;		// File-Descriptor der Datei ( oder eben nicht ..... )

int danz_nachrichten ;	// Anzahl Nachrichten


int daufstat  = 0 ;						// 0 =  noch nix
										// Kopf-Infos kaputt


int  dtestmode ;

int  dabbruch = 0 ;
int  dkopfbug = 0 ;
int  dposbug  = 0 ;

/* ---->
field s_p_r     smallint
field s_p_w     char(10)
field s_p_b     char(10)
field dnachkpreis integer
<---- */

int dvertr_int_par  ;

int dwa_pos_txt ;
int dneunr ;
char dateinamex [110] ;


char einlesepuffer[2056] ;	// das sollte immer reichen
char spalte1[24] ;			// Kunde
char spalte2[24] ;			// datum
char spalte3[24] ;			// teilsmt
char spalte4[24] ;			// Artikel-Nummer
char spalte5[24] ;			// bestellmenge
char spalte6[24] ;			// Mengeneinheit
char spalte7[2000] ;		// freier text
char spalte8[24] ;			// numerische dummy-Spalte
char spalte9[24] ;			// filkun-Kenner "1"== Filiale oder "2" == Kunde

int  spaltnr ;				// spaltnr : 0...6 
int  inspalt ;				// pointer innerhalb spalte 
int  spaltmod ;				// spaltmod 0 : vor (ausserhalb)spalte, 1 = inspalte
int  inpuf ;				// lesepointer im quellstring	

long grspalt1 ;		// Kunde
char grspalt2[24] ;	// Datum
long grspalt3  ;	// gruppe 
int  grspalt9 ;		// Kunde/Filiale


// Augekoppelt von Willes Preisholerei aus partyservice\51100
// prinzipiell muss preisewa.dll existieren , anderes geht prinzipiell NICHT los 

void holepreis (void ) 
{
       int boolretco;
       short sa = 0 ;
       double pr_ek = 0.0 ;
       double pr_vk = 0.0 ;
       
	   sqldatdst ( & aufk.lieferdat , belkopf.lieferdat_feld ) ;

//	   WaPreis.SetAufArt (atoi (auf_art));
//	   WaPreis.SetAufArt ( 0 );

//       if (aufpListe.DllPreise.PriceLib != NULL && 
//		   aufpListe.DllPreise.preise_holen != NULL)
//	   {
//				  dsqlstatus = (aufpListe.DllPreise.preise_holen) (mdn.mdn,


       if (DllPreise.PriceLib != NULL && 
		   DllPreise.preise_holen != NULL)
	   {
			if ( belkopf.filkun == 1 )
			{
				 boolretco = (DllPreise.preise_holen) (mdn.mdn, 
                                          0,	// lsk.fil
                                          1 ,	// lsk.kun_fil
                                          fil.fil,	// lsk.kun
                                          a_bas.a,
										  belkopf.lieferdat_feld,
										  &sa, 
										  &pr_ek,
										  &pr_vk);
			}
			else
			{

				 boolretco = (DllPreise.preise_holen) (mdn.mdn, 
                                          0,	// lsk.fil
                                          0,	// lsk.kun_fil
                                          kun.kun,	// lsk.kun
                                          a_bas.a,
										  belkopf.lieferdat_feld,
										  &sa, 
										  &pr_ek,
										  &pr_vk);
			}
	   }
	   else
	   {
		   boolretco = 0 ;
		   sa = 0 ;
		   pr_ek = 0 ;
		   pr_vk = 0 ;
		   
	   } 
/* konventionelle Preisleserei abgeschaltet  
   {
=				dsqlstatus = WaPreis.preise_holen (mdn.mdn,
=                                          0,
=                                          0,	// kun_fil
=                                          kun.kun,
=                                          a_bas.a,
=										  belkopf.dtm_2_feld,
=                                          &sa, 
=										  &pr_ek,
=										  &pr_vk) ;
	   }
< ------ */
       if (boolretco )
       {
			belpos.auf_vk_pr  = pr_ek;
			belpos.auf_lad_pr = pr_vk;
			belpos.sa_kz_sint = sa;

// noch zu tun ?!
//	             strcpy (aufp.kond_art,   WaPreis.GetKondArt ());
//				 aufp.a_grund   =  WaPreis.GetAGrund ();
//				 aufpListe.FillKondArt (aufp.kond_art);
	   }
/* ---> noch zu tun ?!
	   aufpListe.SetAufVkPr (pr_ek);
	   aufpListe.FillWaehrung (pr_ek, pr_vk);
	   aufpListe.SetPreise ();
< ---- */


}




// Ab hier mal wieder die alten string-Handel-funktionen .....

void cr_weg (char *string)

{
 for (; *string; string += 1)
 {
  if (*string == (char) 13)
    break;
  if (*string == (char) 10)
    break;
  if (*string == '\0')
    return ;
 }
 *string = 0;
 return;
}

int next_char_ci (char *string, char tzeichen, int i)
// Naechstes Zeichen != Trennzeichen suchen.

{
       for (;string [i]; i ++)
       {
               if (string[i] != tzeichen)
               {
                                   return (i);
               }
       }
       return (i);
}

char *wort[25] ; // Bereich fuer Worte eines Strings
int strupcmp (char *str1, char *str2, int len)
{
 short i;
 unsigned char upstr1;
 unsigned char upstr2;


 for (i = 0; i < len; i ++, str1 ++, str2 ++)
 {
  if (*str1 == 0)
    return (-1);
  if (*str2 == 0)
    return (1);

  upstr1 = (unsigned char) toupper((int) *str1);
  switch (upstr1)
  {
      case (unsigned char) '�' :
            upstr1 = '�';
			break;
      case (unsigned char) '�' :
            upstr1 = '�';
			break;
      case (unsigned char) '�' :
            upstr1 = '�';
			break;
  }


  upstr2 = (unsigned char) toupper((int) *str2);

  switch (upstr2)
  {
      case (unsigned char) '�' :
            upstr2 = '�';
			break;
      case (unsigned char) '�' :
            upstr2 = '�';
			break;
      case (unsigned char) '�' :
            upstr2 = '�';
			break;
  }

  if (upstr1 < upstr2)
  {
   return(-1);
  }
  else if (upstr1 > upstr2)
  {
   return (1);
  }
 }
 return (0);
}


short split (char *string)
{
 short wz;                  // Wortzaehler  
 int i, j, len;
 static char zeichen = ' ';
//  if (buffer == (char *) 0) buffer = (char *) malloc (0x1000);
 wz = 0;
 j = 0;
 len = (int)strlen (string);
 wz = 1;
 i = next_char_ci (string, zeichen, 0);
 if (i >= len) return (0);
 wort [wz] = buffer;
 wz ++;
 for (; i < len; i ++, j ++)
 {
        if (string [i] == zeichen)
        {
                    i = next_char_ci (string, zeichen, i);
                    if (i >= len) break;
                    buffer [j] = (char) 0;
                    j ++;
                    wort [wz] = &buffer [j];
                    wz ++;
        }
        buffer [j] = string [i];
  }
  buffer [j] = (char) 0;
  return (wz - 1);
}

static char DefWert[256] ;

// Wert aus Bws-default holen.
char *bws_default (char *name)
{         
        char *etc;
        int anz;
        char buffer [512];
        FILE *fp;

        etc = getenv ("BWSETC");
        if (etc == (char *) 0)
        {
                    etc = "C:\\USER\\FIT\\ETC";
        }

        sprintf (buffer, "%s\\bws_defa", etc);
		fp = fopen (buffer, "r");
        if (fp == NULL) return NULL;

// clipped muss in der Quelle realisiert sein .....
//        clipped (name);

        while (fgets (buffer, 511, fp))
        {
                     cr_weg (buffer);
                     anz = split (buffer);
                     if (anz < 2) continue;
                     if (strupcmp (wort[1], name, (int) strlen (name)) == 0)
                     {
                                 strcpy (DefWert, wort [2]);
                                 fclose (fp);
                                 return (char *) DefWert;
                     }
         }
         fclose (fp);
         return NULL;
}

char * fitgroupdef (char *name)
/**
Wert aus fitgroup.def holen.
**/
{         
        char *etc;
        int anz;
        char buffer [512];
        FILE *fp;

// 130609 : testmade reinpopeln

        etc = getenv ("testmade");
        if (etc == (char *) 0)
        {
                    etc = "C:\\USER\\FIT\\ETC";
        }
		else
		{
			testmade = atoi ( etc );
		}


        etc = getenv ("BWSETC");
        if (etc == (char *) 0)
        {
                    etc = "C:\\USER\\FIT\\ETC";
        }

        sprintf (buffer, "%s\\fitgroup.def", etc);
		fp = fopen (buffer, "r");
        if (fp == NULL) return NULL;

// clipped muss in der Quelle realisiert sein .....
//        clipped (name);

        while (fgets (buffer, 511, fp))
        {
                     cr_weg (buffer);
                     anz = split (buffer);
                     if (anz < 2) continue;
                     if (strupcmp (wort[1], name, (int) strlen (name)) == 0)
                     {
                                 strcpy (DefWert, wort [2]);
                                 fclose (fp);
                                 return (char *) DefWert;
                     }
         }
         fclose (fp);
         return NULL;
}



void nextposition (void )
{


	belpos.auf_datei = belkopf.auf_datei ;
	a_bas.a = atof (spalte4) ;
	int retco = a_bas_class.opena_bas () ;
	if ( ! retco ) retco = a_bas_class.lesea_bas () ;
	if ( ! retco )
	{

		belpos.a = a_bas.a ;
		holepreis () ;
		if ( belkopf.filkun == 1 )
		{
			belpos.prov_satz = 0 ;
		}
		else
		{
			belpos.prov_satz = prov_satz_class.prov_satz_hol (
				a_bas.a ,
				a_bas.ag ,
				a_bas.wg ,
				a_bas.hwg ,
				kun.kun ,
				kun.kun_bran2 ,
				kun.vertr1 ,
				kun.mdn ,
				aufp.sa_kz_sint 
				) ;
		}

	}
	else
	{
		sprintf ( belpos.bemerkung , "Art.Error: %1.0f " , a_bas.a ) ;
		a_bas.a = belpos.a = 0.0 ;
		dposbug ++ ;
	}

	belpos.auf_me_bz[0] = spalte6[0] ;
	belpos.auf_me_bz[1] = spalte6[1] ;
	belpos.auf_me_bz[2] = spalte6[2] ;
	belpos.auf_me_bz[3] = spalte6[3] ;
	belpos.auf_me_bz[4] = spalte6[4] ;
	belpos.auf_me_bz[5] = '\0' ;
	if ( (spalte6[0] == 'k' || spalte6[0] == 'K' ) && (spalte6[1] == 'g' || spalte6[0] == 'G' ))
		belpos.me_einh_kun = 2 ;
	else
		if ( spalte6[0] == 'X' || spalte6[0] == 'x' )
			belpos.me_einh_kun = 8 ;
		else
			belpos.me_einh_kun = 1 ;	// alles andere = Stueck 

	belpos.auf_me = atof ( spalte5 ) ;
	if ( belpos.auf_me < -9999.99 ||belpos.auf_me > 99999.99 ) belpos.auf_me = 999.99 ;

	write_belpos_a() ;	// komplettiert die Position
}
void nextbeleg (void )
{
	char jahr[5] ;
	char monat[3] ;
	char tag[3] ;
	int itag ;
	int imonat ;
	int ijahr ;

	memcpy ( &belkopf,&belkopf_null, sizeof(struct BELKOPF));

	daufstat = 0 ;

	jahr[0] = spalte2[0] ;
	jahr[1] = spalte2[1] ;
	jahr[2] = spalte2[2] ;
	jahr[3] = spalte2[3] ;
	jahr[4] = '\0' ;
	ijahr = atoi ( jahr ) ;

	monat[0] = spalte2[5] ;
	monat[1] = spalte2[6] ;
	monat[2] = '\0' ;
	imonat = atoi ( monat ) ;

	tag[0] = spalte2[8] ;
	tag[1] = spalte2[9] ;
	tag[2] = '\0' ;
	itag = atoi ( tag ) ;

	if ( ijahr < 2000 ||ijahr > 2999 )	 daufstat = -33 ;
	if ( itag < 1 ||itag > 31 )	 daufstat = -33 ;
	if ( imonat < 1 ||imonat > 12 )	 daufstat = -33 ;
	if ( itag == 31 && ( imonat == 2 ||imonat == 4 ||imonat == 6 || imonat == 9 ||imonat == 11 )) daufstat = -33 ;
	if ( itag == 30 && imonat == 2 ) daufstat =  -33 ;


	belkopf.bemerkung[0] = '\0' ;

	teilsmtbeacht = 0 ;

	AUF_DATEI ++ ;
	danz_nachrichten ++ ;
	belkopf.gruppe3 = atol ( spalte3 ) ;

	belkopf.auf_datei = AUF_DATEI ;
	if ( daufstat == -33 )
		sprintf ( belkopf.lieferdat_feld, "31.12.1999" ) ; 
	else
		sprintf ( belkopf.lieferdat_feld, "%s.%s.%s", tag,monat,jahr ) ; 

	belkopf.int_kun = atol ( spalte1 ) ;

	belkopf.filkun = atoi ( spalte9 ) ;	// 280910

	if (( belkopf.filkun < 1 ) || ( belkopf.filkun > 2 )) 
		belkopf.filkun = 2 ;	// Notbremse als Kunde

// Umcodierung 1 -> 1 Filiale
// Umcodierung 2 -> 0 Kunde ( oder rest )

	if ( belkopf.filkun == 1 )
		belkopf.filkun = 1 ;
	else
		belkopf.filkun = 0 ;
		
 // 280910 : falls jemals der Dialog zuschlagen sollte : 
// belkopf.filkun = dfilkun	// Einspeisen der Dialog-Eingabe


	if ( belkopf.filkun == 1 )	// 210910
	{
		fil.fil = (short) belkopf.int_kun ;
		fil.mdn = mdn.mdn ;
		int i = fil_class.openfil () ;
		i = fil_class.lesefil () ;
		if ( ! i )
		{
			belkopf.int_kun = fil.fil ;
			belkopf.int_adr = fil.adr ;
			sprintf (belkopf.kun_bran2 ,"" ) ;
			adr.adr = fil.adr ;
			int ii = adr_class.openadr () ;
			ii = adr_class.leseadr () ;
			sprintf (belkopf.kun_krz1 ,"%s", adr.adr_krz ) ;
			belkopf.tou = fil.tou ;
			belkopf.vertr = 0 ;
			// 210910 teilsmtbeacht = tsmtgg_class.ateil_smt_setz() ;
			teilsmtbeacht = 0 ;
			if ( daufstat == -33 )
			{
				sprintf ( belkopf.bemerkung , "Datum ungueltig" ) ;
				dkopfbug ++ ;
			}
		}
		else
		{
			sprintf ( belkopf.bemerkung , "Filial-Nr. fehlt : %d" , belkopf.int_kun ) ;
			dkopfbug ++ ;
			belkopf.int_kun = 0 ;
			daufstat = -3 ;
		}
	}
	else
	{

		kun.kun = belkopf.int_kun ;
		kun.mdn = mdn.mdn ;
		int i = kun_class.openkun () ;
		i = kun_class.lesekun () ;
		if ( ! i )
		{
			belkopf.int_kun = kun.kun ;
			belkopf.int_adr = kun.adr2 ;
			sprintf (belkopf.kun_bran2 ,"%s", kun.kun_bran2 ) ;
			// so will es die Fa. Schiller fuer REWE, rest der Welt hat hier aufk.adr.adr_nam1 rumstehen 
			sprintf (belkopf.kun_krz1 ,"%s", kun.kun_krz1 ) ;
			belkopf.tou = kun.tou ;
			belkopf.vertr = kun.vertr1 ;
			teilsmtbeacht = tsmtgg_class.ateil_smt_setz() ;
			if ( daufstat == -33 )
			{
				sprintf ( belkopf.bemerkung , "Datum ungueltig" ) ;
				dkopfbug ++ ;
			}
		}
		else
		{
			sprintf ( belkopf.bemerkung , "Kunden-Nr. fehlt : %d" , belkopf.int_kun ) ;
			dkopfbug ++ ;
			belkopf.int_kun = 0 ;
			daufstat = -3 ;
		}
	}
	write_belkopf_a() ;
	nextposition() ;

}

int einlesen ( CorderDlg * Cparent, short dmdn , char * quelldatei , int testignnore )
{

// zuerst mal syspars usw. holen

	char * p ;
	p = fitgroupdef ( "lager" ) ;
	if ( p == NULL ) 
		dlgr_gr = -1 ;
	else
		dlgr_gr = atoi ( p  ) ;


	char parname[31] ;
	char parwert[11] ;
	char parbesch[41] ;

	sprintf ( parname, "li_a_edi" ) ;
	if ( ! sys_par_class.sys_par_holen ( parname, parwert, parbesch ))
		dli_a_edi = 0 ;
	else
		dli_a_edi = atoi ( parwert ) ;


	sprintf ( parname, "lgr_bestand" ) ;
	if ( ! sys_par_class.sys_par_holen ( parname, parwert, parbesch ))
		dlgr_bestand = 0 ;
	else
		dlgr_bestand = atoi ( parwert ) ;

	aufk_class.aktual_ipr () ;	// IPR fuer neue Preisfindung scharf machen

// schon passiert ....	memcpy ( &belkopf,&belkopf_null, sizeof(struct BELKOPF));

	AUF_DATEI = 0 ;
	danz_nachrichten = 0 ;
	grspalt1 = grspalt3 = -1 ;
	grspalt9 = -1 ;
	grspalt2[0] = '\0' ;
	spaltnr = inpuf = inspalt = spaltmod = 0 ;
	spalte1[0] = '\0' ;
	spalte2[0] = '\0' ;
	spalte3[0] = '\0' ;
	spalte4[0] = '\0' ;
	spalte5[0] = '\0' ;
	spalte6[0] = '\0' ;
	spalte7[0] = '\0' ;
	spalte8[0] = '\0' ;
	spalte9[0] = '\0' ;

	while ( fgets(einlesepuffer, sizeof(einlesepuffer) ,fpin )!=NULL)
	{
		while ( einlesepuffer[inpuf] != '\0' )
		{

			if ( einlesepuffer[inpuf] == '"' )
			{

				if ( !spaltmod )
				{	// Beginn eines Feldes
					spaltmod = 1 ;
					spaltnr ++ ;
					inspalt = 0 ;
					inpuf ++ ;
					continue ;
				}
				else	// if ( spaltmod )
				{
					// Einbau Maskierung : if ( einlesepuffer[inpuf - 1 ] == '\\' ) else
					{	// Feld ist zuende
						spaltmod = 0 ; 
						inpuf ++ ;
						continue ;
					}
				}
			}
			else
			{
				if ( spaltmod )
				{
					switch ( spaltnr )
					{
					case 1 :
						if ( inspalt > 22 ) break ;
						spalte1[inspalt++] = einlesepuffer[inpuf] ;
						spalte1[inspalt] = '\0' ;
						break ;
					case 2 :
						if ( inspalt > 22 ) break ;
						spalte2[inspalt++] = einlesepuffer[inpuf] ;
						spalte2[inspalt] = '\0' ;
						break ;
					case 3 :
						if ( inspalt > 22 ) break ;
						spalte3[inspalt++] = einlesepuffer[inpuf] ;
						spalte3[inspalt] = '\0' ;
						break ;
					case 4 :
						if ( inspalt > 22 ) break ;
						if ( spalte4[inspalt]== ',' ) spalte4[inspalt]= '.' ;
						spalte4[inspalt++] = einlesepuffer[inpuf] ;
						spalte4[inspalt] = '\0' ;
						break ;
					case 5 :
						if ( inspalt > 22 ) break ;
						if ( spalte5[inspalt]== ',' ) spalte5[inspalt]= '.' ;
						spalte5[inspalt++] = einlesepuffer[inpuf] ;
						spalte5[inspalt] = '\0' ;
						break ;
					case 6 :
						if ( inspalt > 22 ) break ;
						spalte6[inspalt++] = einlesepuffer[inpuf] ;
						spalte6[inspalt] = '\0' ;
						break ;
					case 7 :
						if ( inspalt > 1990 ) break ;
						spalte7[inspalt++] = einlesepuffer[inpuf] ;
						spalte7[inspalt] = '\0' ;
						break ;
					case 8 :	// 280910
						if ( inspalt > 22 ) break ;
						spalte8[inspalt++] = einlesepuffer[inpuf] ;
						spalte8[inspalt] = '\0' ;
						break ;
					case 9 :	// 280910
						if ( inspalt > 22 ) break ;
						spalte9[inspalt++] = einlesepuffer[inpuf] ;
						spalte9[inspalt] = '\0' ;
						break ;
					}
				}
				inpuf ++ ;
				if ( inpuf > sizeof( einlesepuffer ))
				{	// irgend was ganz faul 
					fclose ( fpin ) ;
					return -1 ;
				}
			}
		}		// Zeile zuende, jetzt muss alles da sein ....

		if ( spaltnr < 6 )
		{
			// Fehlerbehandlung
		}
		else
		{

			if ( atol ( spalte1 ) == grspalt1 
				&& atol ( spalte3 ) == grspalt3 
				&& atoi ( spalte9 ) == grspalt9 
				&& ! strncmp ( spalte2 , grspalt2 , 10) )
			{
				nextposition () ;
			}
			else
			{
				grspalt1 = atol ( spalte1 ) ; 
				grspalt3 = atol ( spalte3 ) ; 
				grspalt9 = atoi ( spalte9 ) ; 
				sprintf ( grspalt2 , "%s" , spalte2 );
				nextbeleg () ; 
			
			}
		}
		spaltnr = inpuf = inspalt = spaltmod = 0 ;
		spalte1[0] = '\0' ;
		spalte2[0] = '\0' ;
		spalte3[0] = '\0' ;
		spalte4[0] = '\0' ;
		spalte5[0] = '\0' ;
		spalte6[0] = '\0' ;
		spalte7[0] = '\0' ;
		spalte8[0] = '\0' ;	// 280910
		spalte9[0] = '\0' ;	// 280910

	}

	fclose ( fpin) ;	// ??????
	return 0 ;

}

void ArraysInit () 
{
	// Anfansginit der Arrays
	belkopfakt = belkopfmax = 0 ;
	belkopfdim = 10 ;
	belposakt = belposmax = 0 ;
	belposdim = 50 ;
//	belfussakt = belfussmax = 0 ;
//	belfussdim = 10 ;
	belkopf_a = NULL ;
	belkopf_a = (struct BELKOPF *)calloc(belkopfdim, sizeof(struct BELKOPF ));
	memcpy ( &belkopf,&belkopf_null, sizeof(struct BELKOPF));
	belpos_a = NULL ;
	belpos_a = (struct BELPOS *)calloc(belposdim, sizeof(struct BELPOS ));
	memcpy ( &belpos,&belpos_null, sizeof(struct BELPOS));
	belpos.zutext = NULL ;

//	belfuss_a = NULL ;
//	belfuss_a = (struct BELFUSS *)calloc(belfussdim, sizeof(struct BELFUSS ));
}

void write_belkopf_a(void)
{
	if ( belkopfmax >= belkopfdim )	// neu allozieren
	{
	   belkopfdim += 10 ;
	   belkopf_a = ( struct BELKOPF *)realloc 
					( (void *) belkopf_a,
					      belkopfdim * sizeof(struct BELKOPF));
		if ( belkopf_a == NULL )
		{
// knallt dann sowieso :					int i = 0 ;	// fatal-Error 
		}

	} ;
	memcpy ( belkopf_a + belkopfmax , &belkopf, sizeof ( struct BELKOPF )) ;
	belkopfmax ++ ;
}

int read_belkopf_a(void)
{
	if ( belkopfakt < belkopfmax )
	{
		memcpy ( &belkopf , belkopf_a + belkopfakt , sizeof ( struct BELKOPF )) ;
		belkopfakt ++ ;
		return 0 ;
	}
	return 100 ;
}

char * hole_a_ptbez ( char * ptitem , short valnum )
{

	sprintf ( ptabn.ptitem ,"%s" , ptitem ) ;
	sprintf ( ptabn.ptwert ,"%d" , valnum ) ;
	int retco = ptabn_class.openptabn () ;
	retco = ptabn_class.leseptabn () ;
	if ( retco ) sprintf ( ptabn.ptbezk , "" ) ;
	return ptabn.ptbezk ;

}

/* ****** WIRD EVTL SPAETER MAL GEBRAUCHT 
short hole_me_einh_kun ( void ) 
{

// heben wir mal fuer spaeter auf ..........

// die meisten Klimmzuege entstehen aus dem Kampf gegen schlecht gefuellte Stammdaten

	int qty_einh = 0 ; 
	int qty_hinweis ;
	if ( ! strncmp( belpos.qty_21_einh , "GRM" , 3 ) ) qty_einh = 3 ;	// Faktor 1000
	if ( ! strncmp( belpos.qty_21_einh , "KGM" , 3 ) ) qty_einh = 2 ;	// Faktor 1
	if ( ! strncmp( belpos.qty_21_einh , "PCE" , 3 ) ) qty_einh = 1 ;	// a_kun auswerten
	if ( ! strncmp ( belpos.lin_feld, "28" , 2 ) ) qty_hinweis = 1 ;	// behandeln wie "GRM" , jedoch nur bei O_FEGRO 

	int gefunden = 0 ;
	short retco = a_bas.me_einh ;
	belpos.inh = 1.0 ;

	tausendfaktor = 1.0 ;

	if ( qty_einh == 2 )	// KGM
	{
		retco = 2 ;	// "kg"
		gefunden = 1 ;
	}

	if ( qty_einh == 3 )	// GRM
	{
		retco = 2 ;	// "kg"
		tausendfaktor = 0.001 ;
		gefunden = 1 ;
	}
	if ((! gefunden) && (doption == O_FEGRO) && (qty_hinweis == 1) && qty_einh == 0 ) 
	{
		retco = 2 ;	// "kg"
		tausendfaktor = 0.001 ;
		gefunden = 1 ;
	}
	if ( ! gefunden )
	{
		if ( a_kun.a > 0.01 && a_kun.a == a_bas.a )
		{
			if ( a_kun.me_einh_kun > 0 )
				retco = a_kun.me_einh_kun ;
			if ( a_kun.inh > 0.001 )
				belpos.inh = a_kun.inh ;

			gefunden = 1 ;
		}
		else
		{
			// kein a_kun-Eintrag vorhanden
			if ( a_bas.a_typ == 1 || a_bas.a_typ2 == 1)
			{
				a_hndw.a = a_bas.a ;
				int retco2 = a_hndw_class.opena_hndw () ;
				if ( ! retco2 ) a_hndw_class.lesea_hndw () ;
				if ( ! retco2 )
				{
					if ( a_hndw.me_einh_kun > 0 )
						retco = a_hndw.me_einh_kun ;
					if ( a_hndw.inh > 0.001 )
						belpos.inh = a_hndw.inh ;
					gefunden = 1 ;
				}
			}
			if ( !gefunden )
			{
				if ( a_bas.a_typ == 2 || a_bas.a_typ2 == 2)
				{
					a_eig.a = a_bas.a ;
					int retco2 = a_eig_class.opena_eig () ;
					if ( ! retco2 ) a_eig_class.lesea_eig () ;
					if ( ! retco2 )
					{
						if ( a_eig.me_einh_ek > 0 )
							retco = a_eig.me_einh_ek ;
						if ( a_eig.inh > 0.001 )
							belpos.inh = a_eig.inh ;
						gefunden = 1 ;
					}
				}
			}
			if ( !gefunden )
			{
				if ( a_bas.a_typ == 3 || a_bas.a_typ2 == 3)
				{
					a_eig_div.a = a_bas.a ;
					int retco2 = a_eig_div_class.opena_eig_div () ;
					if ( ! retco2 ) a_eig_div_class.lesea_eig_div () ;
					if ( ! retco2 )
					{
						if ( a_eig_div.me_einh_ek > 0 )
							retco = a_eig_div.me_einh_ek ;
						if ( a_eig_div.inh > 0.001 )
							belpos.inh = a_eig_div.inh ;
						gefunden = 1 ;
					}
				}
			}
		}
	}

	if ( belpos.inh < 0.001 )
		belpos.inh = 1.0 ;
	if ( retco < 1 ) retco = a_bas.me_einh ;
	return retco ;
}
< ------------------- */



// Kundenstamm und a_bas gibt es jedenfalls, a_kun nur eventuell ......
// vk und lad_preis sind schon erledigt 
// jetzt muss koplettiert werden
void write_belpos_a(void)
{
	int wielang = (int) strlen ( spalte7 ) ;
	if ( wielang > 0 )
	{

			char * p = (char *) malloc (wielang + 5 ) ;
			sprintf ( p , "%s", spalte7 ) ;
			belpos.zutext = p ;
	}



	sprintf ( belpos.lief_me_bz ,"%s" , hole_a_ptbez ( "me_einh" , a_bas.me_einh )) ;
	belpos.me_einh = a_bas.me_einh ;

	belpos.inh = 1.00 ;
// bereits in der csv mitgeliefert : belpos.me_einh_kun = hole_me_einh_kun () ;	// stellt auch den tausendfaktor korrekt 

	belpos.a_grund = a_bas.a_grund ;
	if ( belkopf.filkun == 1 )
		belpos.teil_smt = 0 ;
	else
		belpos.teil_smt = tsmtg_class.ateil_smt_hol ( teilsmtbeacht ) ;
	belpos.dr_folge = a_bas.dr_folge ;

/* --->
    bereits in der csv mitgeliefert :	sprintf ( belpos.auf_me_bz ,"%s"  , hole_a_ptbez ( "me_einh" , belpos.me_einh_kun )) ;  

	belpos.auf_me = belpos.qty_21_wert  ;
	belpos.auf_me = belpos.auf_me * tausendfaktor ;

	if ( a_kun.a == a_bas.a && a_kun.a > 0 && a_kun.geb_fakt > 0.001 )
	{

		// positiver Gebindefaktor : Multiplikiton mit der Bestellmenge ergibt interne Bestellmenge 
		// Beispiel : 1 Display enthaelt 4 Schalen  
		belpos.auf_me = belpos.auf_me *  a_kun.geb_fakt ;
	}
	if ( a_kun.a == a_bas.a && a_kun.a > 0 && a_kun.geb_fakt < -0.001 ) 
	{

		// negativer Gebindefaktor : Division mit der Bestellmenge ergibt interne Bestellmenge 
		// Beispiel :  4 bestellte Schalen werden mit 1 Display beliefert
		// hier sollte evtl .gerundet werden ? 
		belpos.auf_me = belpos.auf_me / ( 0 - a_kun.geb_fakt ) ;

	}
< ------ */

// hier gegbnenfalls noch rumrechnen .... 
	if ( belposmax >= belposdim )	// neu allozieren
	{
	   belposdim += 50 ;
	   belpos_a = ( struct BELPOS *)realloc 
					( (void *) belpos_a,
					      belposdim * sizeof(struct BELPOS));
		if ( belpos_a == NULL )
		{
// knallt dann sowieso :					int i = 0 ;	// fatal-Error 
		}

	} ;
	memcpy ( belpos_a + belposmax , &belpos, sizeof ( struct BELPOS )) ;
	belposmax ++ ;
	memcpy ( &belpos,&belpos_null, sizeof(struct BELPOS));
	belpos.zutext = NULL ;
}

int read_belpos_a(void)
{
	if ( belposakt < belposmax )
	{
		memcpy ( &belpos , belpos_a + belposakt , sizeof ( struct BELPOS )) ;
		belposakt ++ ;
		return 0 ;
	}
	return 100 ;
}

void ArraysFree () 
{

	if ( belkopf_a ) { free ( belkopf_a ) ; belkopf_a = NULL ; } ;
	if ( belpos_a )
	{
		for ( int x = 0 ; x < belposmax ; x ++ )
		{
			if ( belpos_a[x].zutext != NULL ) free ( belpos_a[x].zutext ) ; 
		}

		free ( belpos_a ) ; belpos_a  = NULL ;
	}
	belkopfdim = belkopfakt = belkopfmax = 0 ;
	belposdim = belposakt = belposmax = 0 ;
}

/* --->
Felderzuordnung der Tabelle tempdrx
bearb		// Zeitstempel - nach 10 Tagen loeschen
form_nr		// eigentlich dummy "orderdfw"
lila		// eigentlich dummy immer 0 
lfd			// DER Identifier ( range fuer Automatik )

// integer-Felder
lfd01		// 1 = Belegkopf, 2 = Positionen   
lfd02		// 
lfd03		// kun		( Kunden-Nummer )
lfd04		// dp_adr	( Kunden-Adresse )
lfd05		// dtestmode ( 0 = schreibbereit , 1 nur test. NICHT geschrieben 
lfd06		// kopfbug
lfd07		// posbug
lfd08		// dabbruch 
lfd09		//  auf ( interne Auftragsnummer ) 
lfd10		// laufende Nummer : DAS Ordnungskriterium

// decimal-Felder(17,4)
dfeld01		//	Artikel-intern
dfeld02		//  Menge intern
dfeld03		//  
dfeld04		//  
dfeld05		//  
dfeld06		//  
dfeld07		//  
dfeld08		//  
dfeld09		//  
dfeld10		//  
dfeld11		//   
dfeld12		//   
dfeld13		//   
dfeld14		//   
dfeld15		//  
dfeld16		//  
dfeld17		//   
dfeld18		//   
dfeld19		//  
dfeld20		//  

// char-felder(16)
cfeld101	// 
cfeld102	// 
cfeld103	// 
cfeld104	// 
cfeld105	// 
// char-felder(24)
cfeld201	// Lieferdatum  
cfeld202	// Achtung : falls Artikel-Bez. gewuenscht wird, dann "outer a_bas verwenden ( weil im Kopf a= 0 steht !!) 
cfeld203	// 
cfeld204	// auf_ext
cfeld205	// 
char-felder(36)
cfeld301	// Bemerkung ( teil 1 )
cfeld302	// Bemerkung ( teil 2 )
cfeld303	//   
cfeld304	//  
cfeld305	// 
<---- */


void wrilistekopfsatz (void)
{
	memcpy ( &tempdrx , &tempdrx_null, sizeof ( struct TEMPDRX )) ;
	sprintf ( tempdrx.form_nr, "orderdfw") ;	// order-Protokoll-dfw
	tempdrx.lila = 0 ;
	tempdrx.lfd = lfdliste;
	tempdrx.lfd10 = lfdlistennr ;
// 	tempdrx.dfeld05 = dfilkun ;	// 210910
	tempdrx.dfeld05 = belkopf.filkun ;	// 280910
	lfdlistennr ++ ;

// integer-Felder
	tempdrx.lfd01 = 1 ;	// belegkopf
	tempdrx.lfd03 = belkopf.int_kun ;
	tempdrx.lfd04 = belkopf.int_adr ;
	tempdrx.lfd05 = dtestmode ;
	tempdrx.lfd06 = dkopfbug ;
	if ( immerweiter )
		tempdrx.lfd07 = 0  ;
	else
		tempdrx.lfd07 = dposbug  ;
	tempdrx.lfd08 = dabbruch ;
	tempdrx.lfd09 = belkopf.auf_int ;

	sprintf ( tempdrx.cfeld201 , "%s", belkopf.lieferdat_feld ) ;
	sprintf ( tempdrx.cfeld204, "%1.0d", belkopf.gruppe3 ) ; 
	
	if ( (strlen ( belkopf.bemerkung )) > 33 )
	{
		sprintf ( tempdrx.cfeld302 , "%s" , belkopf.bemerkung + 33 ) ;
		belkopf.bemerkung [33] = '\0' ;
		sprintf ( tempdrx.cfeld301 , "%s" , belkopf.bemerkung ) ;
	}
	else
	{
		sprintf ( tempdrx.cfeld301 , "%s" , belkopf.bemerkung ) ;
		sprintf ( tempdrx.cfeld302 , "" ) ;
	}
	tempdrx_class.inserttempdrx() ;
}

void wrilistepossatz (void)
{
	memcpy ( &tempdrx , &tempdrx_null, sizeof ( struct TEMPDRX )) ;
	sprintf ( tempdrx.form_nr, "orderdfw") ;	// orderdfw-Liste
	tempdrx.lila = 0 ;
	tempdrx.lfd = lfdliste;
	tempdrx.lfd10 = lfdlistennr ;
//	tempdrx.dfeld05 = dfilkun ;	// 210910
	tempdrx.dfeld05 = belkopf.filkun ;	// 280910


	lfdlistennr ++ ;

	tempdrx.lfd01 = 2 ;	// belegposition
// zun�chst die Kopf-Infos duplizieren 
	tempdrx.lfd03 = belkopf.int_kun ;
	tempdrx.lfd04 = belkopf.int_adr ;
	tempdrx.lfd05 = dtestmode ;
	tempdrx.lfd06 = dkopfbug ;
	if ( immerweiter )
		tempdrx.lfd07 = 0  ;
	else
		tempdrx.lfd07 = dposbug ;
	tempdrx.lfd08 = dabbruch ;
	tempdrx.lfd09 = belkopf.auf_int ;

	sprintf ( tempdrx.cfeld201 , "%s", belkopf.lieferdat_feld ) ;
	sprintf ( tempdrx.cfeld204, "%1.0d", belkopf.gruppe3 ) ; 
	
// Anschliessend aktuelle Positions-Infos ergaenzen 

	tempdrx.dfeld01 = belpos.a ;
	tempdrx.dfeld02 = belpos.auf_me ;

	if ( (strlen ( belpos.bemerkung )) > 33 )
	{
		sprintf ( tempdrx.cfeld302 , "%s" , belpos.bemerkung + 33 ) ;
		belpos.bemerkung [33] = '\0' ;
		sprintf ( tempdrx.cfeld301 , "%s" , belpos.bemerkung ) ;
	}
	else
	{
		sprintf ( tempdrx.cfeld301 , "%s" , belpos.bemerkung ) ;
		sprintf ( tempdrx.cfeld302 , "" ) ;
	}
	tempdrx_class.inserttempdrx() ;
}


DWORD ProcWaitExec (LPSTR prog, WORD SHOW_MODE, int x, int y, int cx, int cy)
/**
Fenster-Process starten und nicht auf Ende Warten.
**/
{
        STARTUPINFO sti;
        PROCESS_INFORMATION pi;
        DWORD ExitCode; 
        int ret;

        ZeroMemory (&sti, sizeof (sti));
        sti.cb = sizeof (sti);
        if (x != -1)
        {
                     sti.dwFlags |= STARTF_USEPOSITION;
                     sti.dwX = x;
                     sti.dwY = y;
        }

        if (cx != -1)
        {
                     sti.dwFlags |= STARTF_USESIZE;
                     sti.dwXSize = cx;
                     sti.dwYSize = cy;
        }
        sti.wShowWindow = SHOW_MODE;
        sti.dwFlags |= STARTF_USESHOWWINDOW;

        ret = CreateProcess (NULL,
                       prog,
                       NULL,
                       NULL,
                      (int) NULL,
                      (unsigned long) NULL,
                       NULL,
                       NULL,
                       &sti,
                       &pi);
        if (ret)
        {
                  CloseHandle (pi.hThread);
                  WaitForSingleObject (pi.hProcess, INFINITE);
        }
		else
        {
			      return -1;
        }				  
		GetExitCodeProcess (pi.hProcess, &ExitCode);
		while (ExitCode == STILL_ACTIVE)
		{
                  GetExitCodeProcess (pi.hProcess, &ExitCode);
		}
        CloseHandle (pi.hProcess);
        return ExitCode;
}


void VerbucheListe(void)
{
}

// 160609 A
static char wochtag[33] ;
char * wochentag ( TIMESTAMP_STRUCT * liefdat )
{

 time_t timer;
 struct tm *ltime;

 time (&timer);								// Systemzeit holen
 ltime = localtime (&timer);				// ltime initialisieren
 ltime->tm_mday = liefdat->day  ;			// Datum ueberschreiben
 ltime->tm_mon =  liefdat->month - 1 ;		//	-"-
 ltime->tm_year = liefdat->year - 1900 ;	//	-"-
 ltime->tm_hour = 12 ;						//	-"-

 timer = _mkgmtime(ltime) ;					// Datumszeit holen
 ltime = localtime (&timer);				// ltime neu fuellen	

	wochtag[0] = '\0' ;

	switch (ltime->tm_wday)
	{
	case 0 :	sprintf ( wochtag, "Sonntag" );
		break ;
	case 1 :	sprintf ( wochtag, "Montag" );
		break ;
	case 2 :	sprintf ( wochtag, "Dienstag" );
		break ;
	case 3 :	sprintf ( wochtag, "Mittwoch" );
		break ;
	case 4 :	sprintf ( wochtag, "Donnerstag" );
		break ;
	case 5 :	sprintf ( wochtag, "Freitag" );
		break ;
	case 6 :	sprintf ( wochtag, "Samstag" );
		break ;
	}

	return wochtag ;
}
// 160609

void ladeaufk(void )
{
// 	belkopf steht auf aktuellem Wert, belkopfakt zeigt auf naechsten Wert  

	memcpy ( &aufk,&aufk_null, sizeof(struct AUFK));
	aufk.mdn = mdn.mdn ;  
    aufk.fil = 0 ;
	aufk.ang = 0 ;  
	aufk.auf = num_array [belkopfakt - 1 ] ; 
	belkopf_a[belkopfakt - 1].auf_int = aufk.auf ;	// Zurueckschreiben des Auftrages
	aufk.adr = belkopf.int_adr;
//	if ( dfilkun == 1 )	// 210910
	if(  belkopf.filkun == 1 )	// 280910
		aufk.kun_fil = 1 ; 
	else
		aufk.kun_fil = 0 ; 
	aufk.kun = belkopf.int_kun ;
	sqldatdst ( & aufk.lieferdat, belkopf.lieferdat_feld );
// Schiller-spezial Start 
	char hilfe10 [10] ;
//	if ( dfilkun == 1 )	// 210910
	if(  belkopf.filkun == 1 )	// 280910
	{
	}
	else
	{
		sprintf ( hilfe10 ,"%08.0d" , kun.tou ) ;
		if ( kun.tou > 0 )
		{
			sprintf ( aufk.lieferzeit , "%s" , hilfe10 + 5  ) ; // die letzten 3 Stellen
			hilfe10[5] = '\0' ;									// die ersten 5 stellen 
			sprintf ( aufk.hinweis , "%s", hilfe10 ) ;
		}
	}
// erst mal alles platt machen fuer "normal" :
	sprintf ( aufk.lieferzeit , "" ) ;
	sprintf ( aufk.hinweis , "" ) ;
//  Hier wuenscht sich der der Herr Mauer den Wochentag rein ins Hinweisfeld
	sprintf ( aufk.hinweis , wochentag ( & aufk.lieferdat )) ;	// 160609
	

	aufk.auf_stat = 2 ;
	sprintf ( aufk.kun_krz1 , "%s" , belkopf.kun_krz1 ) ;
	sprintf ( aufk.feld_bz1 , "" ) ;
	sprintf ( aufk.feld_bz2 , "" ) ;
	sprintf ( aufk.feld_bz3 , "" ) ;
	aufk.delstatus = 0 ;
	aufk.zeit_dec = 0  ;
	aufk.kopf_txt = 0 ;
	aufk.fuss_txt = 0 ;
//	if (  dfilkun == 1 )	// 210910
	if(  belkopf.filkun == 1 )	// 280910

	{
		aufk.vertr = 0 ;
	}
	else
	{
		aufk.vertr = belkopf.vertr ;
	}
//	sprintf ( aufk.auf_ext , "%s", belkopf.auf_ext ) ;
	sprintf ( aufk.auf_ext , "" ) ;
	aufk.tou = belkopf.tou  ;
	sprintf ( aufk.pers_nam ,"ord.dfw" ) ;
	sqldatdst ( & aufk.komm_dat, belkopf.lieferdat_feld ) ;
	if ( strlen ( belkopf.lieferdat_feld ) > 9 )
		sqldatdst ( & aufk.best_dat, belkopf.lieferdat_feld ) ;
	else
		sqldatdst ( & aufk.best_dat, belkopf.lieferdat_feld ) ;
	aufk.waehrung = 2 ;
	aufk.auf_art = 0  ;
	aufk.gruppe = 0 ;
	aufk.ccmarkt = 0 ;
	aufk.fak_typ = 0 ;
	aufk.tou_nr = belkopf.tou ;
	sprintf ( aufk.ueb_kz , "" ) ;
	sqldatdst ( & aufk.fix_dat, belkopf.lieferdat_feld ) ;
	sprintf ( aufk.komm_name , "ord.dfw" ) ;	// 160609
	sqldatdst ( & aufk.akv, belkopf.lieferdat_feld ) ;
	sprintf ( aufk.akv_zeit , "" ) ;
	sqldatdst ( & aufk.bearb, belkopf.lieferdat_feld ) ;
	sprintf ( aufk.bearb_zeit , "" ) ;
	aufk.psteuer_kz = 0 ;
	dpos_zahl = 1 ;	// Positionscounter 
}

void ladeaufp(void )
{
	// aufk ist bestueckt , belpos zeigt auf dazugeh�rigen Satz
	memcpy ( &aufp,&aufp_null, sizeof(struct AUFP));

	aufp.mdn = mdn.mdn ; 
	aufp.fil = 0  ;
	aufp.auf = aufk.auf ;
	aufp.posi = dpos_zahl * 10  ;
				dpos_zahl ++ ;
	if ( belpos.zutext == NULL )
		aufp.aufp_txt = 0 ;
	else
	{
		aufp.aufp_txt = aufpnr_class.holenummer() ;
		if ( aufp.aufp_txt > 0 )	// 040909 : Errors abfangen und handlen
		{
			if ( strlen ( belpos.zutext ) > 60 )
			{
				belpos.zutext[60] = '\0' ;
			}
			sprintf ( aufpt.txt ,"%s", belpos.zutext) ;
			aufpt.nr = aufp.aufp_txt ;
			aufpt.zei = 10 ;
			aufpt_class.insertaufpt() ;
		}
		else
		{
			aufp.aufp_txt = 0 ;	//  dann doch lieber wieder gar nichts 
		}
	}
	aufp.a = belpos.a ;
	aufp.auf_me = belpos.auf_me  ;
	sprintf ( aufp.auf_me_bz , "%s" , belpos.auf_me_bz ) ;
	aufp.lief_me = 0.0 ;
	sprintf ( aufp.lief_me_bz , "%s" , belpos.lief_me_bz ) ;
	aufp.auf_vk_pr = belpos.auf_vk_pr ;

	aufp.auf_lad_pr = belpos.auf_lad_pr ;
// Auszeichnungspreismechanismus : 
//	falls lad_pr aus der Preisfindung == 0 ,
//	dann muss diese "0"  durchgereicht werden wegen irgendwelcher PAZ-Abl�ufe(Etiketten-Layout)

// falls lad_pr > 0 UND Ausz.Preis > 0 dann wird Preis der Preissuche durch den aktuellen Wert ueberschrieben
/* +++++>

	if ((belpos.auf_lad_pr > 0.001 ) && ( belpos.pri_aae_wert > 0.001 ))
	aufp.auf_lad_pr = belpos.pri_aae_wert ;
< ----- */

	aufp.delstatus = 0 ;
	aufp.sa_kz_sint = belpos.sa_kz_sint ;
	aufp.prov_satz = belpos.prov_satz ;
	aufp.ksys = 0 ;
	aufp.pid = 0 ;
	aufp.auf_klst = 0  ;
	aufp.teil_smt = belpos.teil_smt  ;
	aufp.dr_folge = belpos.dr_folge  ;
	aufp.inh  = belpos.inh ;
	aufp.auf_vk_euro = aufp.auf_vk_pr ;	// zwangsgleichweaehrung
	aufp.auf_vk_fremd = 0.0 ;
	aufp.auf_lad_euro = aufp.auf_lad_pr  ;
	aufp.auf_lad_fremd = 0.0 ;
	aufp.rab_satz = 0.0 ;
	aufp.me_einh_kun = belpos.me_einh_kun ;
	aufp.me_einh = belpos.me_einh ;
	aufp.me_einh_kun1 = 0 ;
	aufp.auf_me1 = 0 ;
	aufp.inh1 = 0 ;
	aufp.me_einh_kun2 = 0 ;
	aufp.auf_me2 = 0 ;
	aufp.inh2 = 0 ;
	aufp.me_einh_kun3 = 0 ;
	aufp.auf_me3 = 0 ;
	aufp.inh3 = 0 ;
	aufp.gruppe = 0 ;
	sprintf (  aufp.kond_art ,"" ) ;
	aufp.a_grund = belpos.a_grund ;
	aufp.ls_pos_kz = 0 ;
	aufp.posi_ext = 0 ;	// evtl. spaeter mal nachbauen ??
	aufp.kun = aufk.kun  ;	// was das auch immer bedueten mag ( insbes. bei filial-LS )
	aufp.a_ers  ;
	sprintf (aufp.ls_charge , "" ) ;
	aufp.aufschlag = 0 ;
	aufp.aufschlag_wert = 0 ;
	aufp.pos_txt_kz = 0 ;

	if ( dlgr_bestand && aufp.auf_me > 0 )
	{
		memcpy ( &bsd_buch,&bsd_buch_null, sizeof(struct BSD_BUCH));
		bsd_buch.nr = aufp.auf ;
        sprintf ( bsd_buch.blg_typ , "A" ) ;
        bsd_buch.mdn = aufp.mdn ;
        bsd_buch.fil = aufp.fil ;	// wohl immer == 0
		if  ( dfilkun == 1 )	// 210910 
			bsd_buch.kun_fil = 0 ; 
		else
			bsd_buch.kun_fil = 0 ; 
        bsd_buch.a = aufp.a ;

		time_t timer;
		struct tm *ltime;
		time (&timer);
 		ltime = localtime (&timer);

		bsd_buch.dat.day = ltime->tm_mday ;
		bsd_buch.dat.month = ltime->tm_mon + 1 ;
		bsd_buch.dat.year = ltime->tm_year + 1900 ;
		bsd_buch.dat.hour = 0 ;
		bsd_buch.dat.minute = 0 ;
		bsd_buch.dat.second = 0 ;
		bsd_buch.dat.fraction = 0 ;

		sprintf ( bsd_buch.zeit , "%02d:%02d:%02d",
			ltime->tm_hour, ltime->tm_min, ltime->tm_sec );

        sprintf ( bsd_buch.pers , "%s" , aufk.pers_nam ) ;
		if ( dlgr_gr > -1 )
		{
			a_lgr.a = aufp.a ;
			lgr.mdn = aufp.mdn ;
			lgr.lgr_gr = dlgr_gr ;
			int retco = lgr_class.lese_a_lager () ;
			if ( ! retco ) 
				sprintf ( bsd_buch.bsd_lgr_ort, "%d", a_lgr.lgr ) ;
			else
				sprintf ( bsd_buch.bsd_lgr_ort, "0" ) ;
		}
		else
			sprintf ( bsd_buch.bsd_lgr_ort ,"0" ) ;

        bsd_buch.me = aufp.auf_me ;
        bsd_buch.bsd_ek_vk = aufp.auf_vk_euro ;
        sprintf ( bsd_buch.chargennr , "" ) ; 
        bsd_buch.auf = aufp.auf ;
        sprintf ( bsd_buch.err_txt , "order:01.01.2010" ) ;
		bsd_buch_class.insertbsd_buch () ;
	}
}

int auswerten (char *quellname ) 
{
// zuerst  Koepfe, danach evtl 0 - n Posten ,
// es koennten aber Posten fehlen 

int statkopf = 0 ;	// 0 = gut gelesen + gut geladen , muss noch geschrieben werden  

char s1[196] ;
char s2[196] ;

// bei immerweiter == 1 wird posbug NICHT als Abbruchkrit. gewertet, jedoch dieser Posten NICHT geschrieben
	if ( !dtestmode && ! dkopfbug && ( ! dposbug || immerweiter == 1)  && ! dabbruch )
	{
		// schreibe alle Auftraege : WICHTIG : leere Koepfe gibbet NICHT !!!! - sonst kommt Unsinn raus 
		dbClass.beginwork () ;
		belkopfakt = belposakt = 0 ;
		statkopf = read_belkopf_a () ;
		if ( !statkopf )
		{
			ladeaufk() ;
			while ( ! read_belpos_a () )
			{
				if ( belpos.auf_datei == belkopf.auf_datei  )
				{
					ladeaufp () ;
					if ( (strlen ( belpos.bemerkung)) == 0  && belpos.auf_me != 0.0 )	// einerseits immerweiter andererseits keine Leermengen  
					{
						if ( testmade == 2 )
						{
							char aussage [ 99 ];
							sprintf ( aussage,"auf:%d ,a:%1.0f,posi:%d",aufp.auf,aufp.a,aufp.posi) ;
							MessageBox(NULL , aussage, "" , MB_OK);
						}
						aufp_class.insertaufp () ;
					}
				}
				else
				{
					if ( testmade == 2 )
					{
						char aussage [ 99 ];
						sprintf ( aussage,"auf:%d ,mdn:%d, fil:%d",aufk.auf,aufk.mdn,aufk.fil) ;
						MessageBox(NULL , aussage, "" , MB_OK);
					}
					aufk_class.insertaufk () ;
					if ( testmade == 2 )
					{
						char aussage [ 99 ];
						sprintf ( aussage,"Kopf geschrieben" ) ;
						MessageBox(NULL , aussage, "" , MB_OK);
					}

					statkopf = read_belkopf_a () ;
					if ( statkopf ) break ;
					ladeaufk () ;
					ladeaufp () ;
					if ( (strlen ( belpos.bemerkung)) == 0 && belpos.auf_me != 0.0 )  // einerseits immerweiter andererseits keine Leermengen
					{
						if ( testmade == 2 )
						{
							char aussage [ 99 ];
							sprintf ( aussage,"auf:%d ,a:%1.0f,posi:%d",aufp.auf,aufp.a,aufp.posi) ;
							MessageBox(NULL , aussage, "" , MB_OK);
						}
						aufp_class.insertaufp()  ;
					}
				}
			}
			if ( !statkopf )
			{ 
				if ( testmade == 2 )
				{
					char aussage [ 99 ];
					sprintf ( aussage,"auf:%d ,mdn:%d ,fil:%d",aufk.auf,aufk.mdn,aufk.fil) ;
					MessageBox(NULL , aussage, "" , MB_OK);
				}
				aufk_class.insertaufk () ;
				if ( testmade == 2 )
				{
					char aussage [ 99 ];
					sprintf ( aussage,"Kopf geschrieben" ) ;
					MessageBox(NULL , aussage, "" , MB_OK);
				}
			}
		}
		dbClass.commitwork() ;
		char zielname[555] ;
		sprintf ( zielname, "%s.sav", quellname ) ;
		unlink( zielname ) ;
		rename ( quellname, zielname ) ;
	}

	// Falls bugs vorhanden sind, dann unbedingt Protokoll aufpoppen,
	// ansonsten kann hier problemlos irgendwann ein nice-to-have-protokoll aktiviert werden
	// -> nur "bemerkung nicht empty" , alle Koepfe  und so weiter als Varianten

//	if ( dkopfbug || dposbug )
	if ( 1 )	// Immer Druckausgabe, kann ja dann abgewaehlt werden 
	{
		sprintf ( tempdrx.form_nr, "orderdfw") ;	// Orders-Protokoll

		tempdrx.lila = 0 ;

		tempdrx_class.deletetempdrx () ;

		sprintf ( s2, "%s\\orderdfw.prm", getenv ( "TMPPATH" ) ) ;

		int kk = tempdrx_class.lesetempdrx ()	;
		tempdrx.lfd ++ ;	// Max + 1
		lfdliste = tempdrx.lfd ;
		lfdlistennr =  0 ;

// Prinzip : kopfsatz gibt es immer, pos-Saetze darunter koennten fehlen 

		belkopfakt = belposakt = 0 ;
		statkopf = read_belkopf_a () ;
		if ( !statkopf )
		{
			wrilistekopfsatz() ;
			while ( ! read_belpos_a () )
			{
				if ( belpos.auf_datei == belkopf.auf_datei )
				{
					wrilistepossatz () ;
				}
				else
				{
					statkopf = read_belkopf_a () ;
					if ( statkopf ) break ;
					wrilistekopfsatz () ;
					wrilistepossatz () ;
				}
			}
		}

		FILE * fp2 ;
		fp2 = fopen ( s2 , "w" ) ;
		if ( fp2 == NULL )
		{
			return -1  ;
		}
	
		sprintf ( s1, "NAME orderdfw\n" ) ; 
	
		CString St = s1;
		St.MakeUpper() ;
		sprintf ( s1 ,"%s", St.GetBuffer(512)) ;
		fputs ( s1 , fp2 );


		fputs ( "LABEL 0\n" , fp2 ) ;

		sprintf ( s1, "DRUCK 1\n"   ) ;
		fputs ( s1 , fp2 ) ;

		sprintf ( s1, "lfd %ld %ld\n", lfdliste , lfdliste   ) ;
		fputs ( s1 , fp2) ;

		fputs ( "ANZAHL 1\n" , fp2 ) ;

		fclose ( fp2) ;

/* ---> das gibbet hier alles nicht
		ModDialog moddialog ;
		int NurProt = 0 ;
		int NurProtaktiv = 1 ;
		int ListeDicht = 0 ;
		int ListeDichtaktiv = 1 ;
		int Details = 1 ;
		int Detailsaktiv = 1 ;
		int VerBuchen = 0 ;
		int VerBuchenaktiv = 0 ;
// im TEXTBLOCK
		moddialog.setzeparas( NurProt,NurProtaktiv
			                  ,ListeDicht,ListeDichtaktiv
							  ,Details,Detailsaktiv
							  ,VerBuchen,VerBuchenaktiv ) ;
		if ( moddialog.DoModal () != IDOK )
		{
			return -1 ;
		}
// im TEXTBLOCK
		moddialog.holeparas ( &NurProt, &ListeDicht, &Details, &VerBuchen ) ;
		if ( Details )
		{
			sprintf ( s1, "dr70001.exe -datei \"%s\" " , s2 );
			int ex_code = ProcWaitExec( (char *)s1, SW_SHOW, -1, 0, -1, 0 );
		}
		if ( ListeDicht )
		{
			sprintf ( s1, "dr70001.exe -datei \"%s\" " , s3 );
			int ex_code = ProcWaitExec( (char *)s1, SW_SHOW, -1, 0, -1, 0 );
		}
// im TEXTBLOCK
		if ( NurProt )
		{
			sprintf ( s1, "dr70001.exe -datei \"%s\" " , s4 );
			int ex_code = ProcWaitExec( (char *)s1, SW_SHOW, -1, 0, -1, 0 );
		}
		if ( VerBuchen )
		{
			VerbucheListe () ;
		}
< ---- */
		sprintf ( s1, "dr70001.exe -datei \"%s\" " , s2 );
		int ex_code = ProcWaitExec( (char *)s1, SW_SHOW, -1, 0, -1, 0 );
	}
	lfdliste = 0 ;

	return dabbruch  ;

}


void InterpretCommandLine(void)
//=============================================================================
{
    LPSTR CommandLine = GetCommandLine ();
    CToken Token (CommandLine, " ");

    char *px;
	char p[299] ;


	Token.NextToken ();
	while ((px = Token.NextToken ()) != NULL)
	{

		CString St = px;
		St.MakeUpper() ;
		sprintf ( p ,"%s", St.GetBuffer(512)) ;

		if (strcmp (p, "-PLATTFORM") == 0)
		{
			dplattform = 1 ;
		}

		if (strcmp (p, "-FEGRO") == 0)
		{
			doption = O_FEGRO ;
		}
		if (strcmp (p, "-METRO") == 0)
		{
			doption = O_METRO ;
		}
		if (strcmp (p, "-REWE") == 0)
		{
			doption = O_REWE ;
			immerweiter = 0 ;
		}
		if (strcmp (p, "-KAUFLAND") == 0)
		{
			doption = O_KAUFLAND ;
		}
		if (strcmp (p, "-EDEKA") == 0)
		{
			doption = O_EDEKA ;
		}
		if (strcmp (p, "-TENGELMANN") == 0)
		{
			doption = O_TENGELMANN ;
		}
	}
}

void ladesmtDatei () 
{
	sprintf ( branche0 , "" ) ;
	sprintf ( branche1 , "" ) ;
	sprintf ( branche2 , "" ) ;
	sprintf ( branche3 , "" ) ;
	sprintf ( branche4 , "" ) ;
	sprintf ( teilsort0 , "" ) ;
	sprintf ( teilsort1 , "" ) ;
	sprintf ( teilsort2 , "" ) ;
	sprintf ( teilsort3 , "" ) ;
	sprintf ( teilsort4 , "" ) ;

	if ( teilsmttrennen  )
	{
		FILE *fp ;
		char *etc;
		char ibuffer [512];

		etc = getenv ("BWSETC");
		if (etc == (char *) 0)
		{
                    etc = "C:\\USER\\FIT\\ETC";
		}

		sprintf (ibuffer, "%s\\dta%d.par", etc , dta.dta ) ;
		fp = fopen (ibuffer, "r");
		if (fp == NULL)	// nichts gefunden , dann wird wohl auch nichts sinnvolles mehr passieren
			return ;

		for ( int i = 1 ; i < 4 ; i++ )
		{
			if ( ! fgets (ibuffer, 511, fp))
			break ;	// letzte Zeile gefunden 

			ibuffer[ 105] = '\0' ;	// Notbremse
			cr_weg ( ibuffer ) ;
// Kundenbranche darf maximal 2 Zeichen lang sein, fuehrende und folgende blanks vor dem "$" ignoriere ich mal grosszuegig

			int ii , ij ;
			ii= ij = 0 ;
			char branbu[5] ;
			branbu[0] = '\0' ;
			while ( ii < 105 )
			{
				if ( ibuffer[ii] == '\0' )
					break ;
				if ( ibuffer[ii] == '$' )
					break ;
				if ( ibuffer[ii] == ' ' )
				{
					ii++ ; 
					continue ;
				}
				branbu[ij] = ibuffer[ii] ;
				ij ++ ;
				branbu[ij] = '\0' ;
				if ( ij > 1 )	break ;	// Branche maximal 2 zeichen lang .....
				ii ++ ;
			}

			if ( i == 1 ) sprintf ( branche0,"%s", branbu ) ;
			if ( i == 2 ) sprintf ( branche1,"%s", branbu ) ;
			if ( i == 3 ) sprintf ( branche2,"%s", branbu ) ;

		// Achtung : ii wird weitergezaehlt !! 

			while ( ii < 105 )
			{
				if ( ibuffer[ii] == '\0' )
					break ;
				if ( ibuffer[ii] == '$' )
				{
					ii ++ ;	// "$" skippen 
					if ( i == 1 ) sprintf ( teilsort0,"%s", ibuffer + ii ) ;
					if ( i == 2 ) sprintf ( teilsort1,"%s", ibuffer + ii ) ;
					if ( i == 3 ) sprintf ( teilsort2,"%s", ibuffer + ii ) ;
					break ;
				}
				ii ++ ;
			}
		}
		fclose (fp) ;
	}
}


int Abarbeiten ( CorderDlg * Cparent , short dmdn , char * quelldatei , int testignore )
{


	InterpretCommandLine () ;


	ArraysInit () ;

	dtestignore = testignore ;
	dfilkun  = testignore ;	// 210910 : alles neu und besser
	dtestmode = 0 ;	// hier wird der testmode eingefangen

	char hilfe[256] ;
	wsprintf( hilfe, "%s", getenv("testmode") );
	TEST = 0 ;
	if ( atoi ( hilfe) == 5 ) TEST =1 ;

	dkopfbug = dposbug = dabbruch = 0 ;

	fpin = fopen ( quelldatei , "r" );
	int i =	 einlesen ( Cparent ,dmdn, quelldatei,  testignore ) ;

// jetzt sind daten da ....
	int o = 0  ;
	int putin = 0 ;
	long numhilfe = 0 ;
	if ( !dtestmode && ! dkopfbug && ( ! dposbug || immerweiter )  && ! dabbruch )
	{
		while ( putin < MAXDIMNUM )
		{
			if ( putin >= danz_nachrichten ) break ;	// Nummern erfolgreich geladen
			dbClass.beginwork () ;
			dbClass.sqlcomm(" set lock mode to wait " ) ;	// 130609
			o = nvsimpel ( mdn.mdn,0, "auf", &numhilfe ) ;

			if ( o )
			{ dbClass.rollbackwork () ;
				dbClass.sqlcomm(" set lock mode to not wait " ) ;	// 130609
				break ;
			}
			if ( numhilfe < 1 )	// 0 und negative Werte sind verboten !!!!
			{ dbClass.rollbackwork () ;
				dbClass.sqlcomm(" set lock mode to not wait " ) ;	// 130609
				break ;
			}
			dbClass.commitwork () ;
			dbClass.sqlcomm(" set lock mode to not wait" ) ;	// 130609
			num_array[putin] = numhilfe ;
			putin ++ ;
		}
		if  ( putin < danz_nachrichten && putin < MAXDIMNUM )
		{
			// Problem mit Nummernvergabe, gegbnenfalls backrollen und beenden
			if ( putin > 0 )
			{
				for ( int i = 0 ; i < putin ; i ++ )
				{
					numhilfe = num_array[i] ;
					if ( numhilfe > 0 )
					{
						dbClass.beginwork () ;
						nveinid ( 1 , 0 , "auf", numhilfe ) ;
						dbClass.commitwork () ;
					}
				}

			}
			MessageBox(NULL, "Problem bei der Nummernvergabe", "" , MB_OK|MB_ICONSTOP);

			dabbruch = 1 ;
		}
	}

	i = auswerten ( quelldatei ) ;

// testmode halt,
// dabbruch : letaler fehler ( Datenbank, )
// dkopfbug : Problem Belegkopf
// dposbug  : Pos-Problem ( Artikel fehlt ?! )

	if ( dtestmode == 1 || dabbruch || ( dposbug && ( ! immerweiter) ) || dkopfbug )
	{
// spaeter : 	MessageBox(NULL, "Diese Daten sind noch nicht in der Datenbank", " ", MB_OK|MB_ICONSTOP);
		ArraysFree() ;
		Cparent->myDisplayMessage( "Diese Daten sind noch nicht in der Datenbank" ) ;

		return 0 ;
	}

	ArraysFree () ;
		sprintf ( hilfe ,"  %d Belege eingelesen " , danz_nachrichten ) ; 
		Cparent->myDisplayMessage( hilfe ) ;

	return i ;
}


