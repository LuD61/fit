#include "stdafx.h"
#include "DbClass.h"
#include "dta.h"

struct DTA dta,  dta_null;

extern DB_CLASS dbClass;

DTA_CLASS dta_class ;

static int anzzfelder ;

int DTA_CLASS::dbcount (void)
/**
Tabelle dta lesen.
**/
{

         if (test_upd_cursor == -1)
         {
             prepare ();
         }
         dbClass.sqlopen (count_cursor);
         dbClass.sqlfetch (count_cursor);
         if (sqlstatus == 0)
         {
                      return anzzfelder;
         }
         return sqlstatus;  
}

int DTA_CLASS::lesedta (void)
{
	long savedta = dta.dta ;	// diese bloeden null-values .......
	memcpy ( &dta,&dta_null, sizeof(struct DTA));

	int di = dbClass.sqlfetch (readcursor);
	dta.dta = savedta ;
	return di;
}

int DTA_CLASS::lesealldta (void)
{
	  memcpy ( &dta,&dta_null, sizeof(struct DTA));
      int di = dbClass.sqlfetch (readallcursor);

	  return di;
}

int DTA_CLASS::opendta (void)
{

		if ( readcursor < 0 ) prepare ();
		
         return dbClass.sqlopen (readcursor);
}

int DTA_CLASS::openalldta (void)
{

		if ( readallcursor < 0 ) prepare ();
		
         return dbClass.sqlopen (readallcursor);
}

void DTA_CLASS::prepare (void)
{


								
	test_upd_cursor = 1;

	dbClass.sqlin ((long *) &dta.dta, SQLLONG, 0);

    dbClass.sqlout ((char *)  dta.kun_krz1,	SQLCHAR, 17 ) ;
	dbClass.sqlout ((long *) &dta.adr,		SQLLONG,  0 ) ;
    dbClass.sqlout ((char *)  dta.iln,		SQLCHAR, 17 ) ;
    dbClass.sqlout ((short *)&dta.typ,		SQLSHORT, 0 ) ;			
    dbClass.sqlout ((short *)&dta.test_kz,	SQLSHORT, 0 ) ;		
    dbClass.sqlout ((short *)&dta.waehrung,	SQLSHORT, 0 ) ;	
    dbClass.sqlout ((char *)  dta.dat_name,	SQLCHAR, 17 ) ;
    dbClass.sqlout ((long *) &dta.nummer,	SQLLONG,  0 ) ;	
	dbClass.sqlout ((short *)&dta.pri_seg,	SQLSHORT, 0 ) ;	
	dbClass.sqlout ((short *)&dta.reli,		SQLSHORT, 0 ) ;	
	
 

		readcursor = (short) dbClass.sqlcursor ("select "

    " kun_krz1, adr, iln, typ, test_kz, waehrung "
	" , dat_name, nummer, pri_seg, reli "
	" from dta where dta = ? ") ;

	dbClass.sqlout ((long *) &dta.dta, SQLLONG, 0);
	dbClass.sqlout ((char *)  dta.kun_krz1,	SQLCHAR, 17 ) ;
	dbClass.sqlout ((long *) &dta.adr,		SQLLONG,  0 ) ;
    dbClass.sqlout ((char *)  dta.iln,		SQLCHAR, 17 ) ;
    dbClass.sqlout ((short *)&dta.typ,		SQLSHORT, 0 ) ;			
    dbClass.sqlout ((short *)&dta.test_kz,	SQLSHORT, 0 ) ;		
    dbClass.sqlout ((short *)&dta.waehrung,	SQLSHORT, 0 ) ;	
    dbClass.sqlout ((char *)  dta.dat_name,	SQLCHAR, 17 ) ;
    dbClass.sqlout ((long *) &dta.nummer,	SQLLONG,  0 ) ;	
	dbClass.sqlout ((short *)&dta.pri_seg,	SQLSHORT, 0 ) ;	
	dbClass.sqlout ((short *)&dta.reli,		SQLSHORT, 0 ) ;
// 130706 : reli dazu
	

		readallcursor = (short) dbClass.sqlcursor ("select "

    " dta, kun_krz1, adr, iln, typ, test_kz, waehrung "
	" , dat_name, nummer, pri_seg, reli "
	" from dta order by dta ") ;


}

