#ifndef _PROV_SATZ_DEF
#define _PROV_SATZ_DEF

struct PROV_SATZ {
short	mdn  ;
short fil ;
long vertr ;
long kun ;
double a ;
short hwg ;
short wg ;
short ag ;
double prov_satz ;
double prov_sa_satz ;
char a_bz1 [25] ;
char kun_bran2 [3] ;

} ;
extern struct PROV_SATZ prov_satz, prov_satz_null;


class PROV_SATZ_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               double prov_satz_hol (	double a ,
									short ag ,
									short wg ,
									short hwg ,
									long kun ,
									char * kun_bran2 ,
									long vertr , 
									short mdn ,
									short sa_kz_sint );
               PROV_SATZ_CLASS () : DB_CLASS ()
               {
               }
};


#endif

