#ifndef _FIL_DEF
#define _FIL_DEF

struct FIL {

char abr_period[2] ;
long adr;
long adr_lief;
short afl;
char auf_typ[2];
char best_kz[2];
char bli_kz[2];
char dat_ero[12];
short daten_mnp;
short delstatus;
short fil;
char fil_kla[2];
short fil_gr;
double fl_lad;
double fl_nto;
double fl_vk_ges;
short frm;
char iakv[12];
char inv_rht[2];
long kun;
char lief[17];
char lief_rht[2];
long lief_s;
char ls_abgr[2];
char ls_kz[2];
short ls_sum;
short mdn;
char pers[13];
short pers_anz;
char pos_kum[2];
char pr_ausw[2];
char pr_bel_entl[2];
char pr_fil_kz[2];
long pr_lst;
char pr_vk_kz[2];
double reg_bed_theke_lng;
double reg_kt_lng;
double reg_kue_lng;
double reg_lng;
double reg_tks_lng;
double reg_tkt_lng;
char ret_entl[2];
char smt_kz[2];
short sonst_einh;
short sprache;
char sw_kz[2];
long tou;
char umlgr[2];
char verk_st_kz[2];
short vrs_typ;
char inv_akv[2];
double planumsatz;

};

extern struct FIL fil, fil_save, fil_null;

class FIL_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int dbcount (void);
               int lesefil (void);
               int openfil (void);
               FIL_CLASS () : DB_CLASS ()
               {
               }
};



#endif

