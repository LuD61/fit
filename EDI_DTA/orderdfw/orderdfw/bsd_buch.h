#ifndef _BSD_BUCH_DEF
#define _BSD_BUCH_DEF

struct BSD_BUCH {
long nr ;
char blg_typ[3] ;
short mdn ;
short fil ;
short kun_fil ;
double a ;
TIMESTAMP_STRUCT dat ;
char zeit [ 9] ;
char pers [13] ;
char bsd_lgr_ort [13] ; 
short qua_status ;
double me ;
double bsd_ek_vk ;
char chargennr [31] ;
char ident_nr [21] ;
char herk_nachw[4] ;
char lief [17];
long auf ;
char verfall [2] ;
TIMESTAMP_STRUCT verf_dat ;
short delstatus ;
char err_txt [17] ;
double me2 ;
short me_einh2 ;
TIMESTAMP_STRUCT lieferdat ;
double a_grund ;
short me_einh ;

};

extern struct BSD_BUCH bsd_buch, bsd_buch_null;

class BSD_BUCH_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int insertbsd_buch (void);
               BSD_BUCH_CLASS () : DB_CLASS ()
               {
               }
};

struct LGR {
short lgr ;
short mdn ;
short fil ;
char lgr_smt_kz [4] ;
char lgr_bz [2] ;
short lgr_kz ;
short lgr_gr ;
char lgr_kla [2] ;
char abr_period [2] ;
long adr ;
short afl ;
char bli_kz [2] ;
TIMESTAMP_STRUCT dat_ero ;
double fl_lad ;
double fl_nto ;
double fl_vk_ges ;
short frm ;
TIMESTAMP_STRUCT iakv ;
char inv_rht [2] ;
char ls_abgr [2] ;
char ls_kz  [2] ;
short ls_sum ;
char pers [13] ;
short pers_anz ;
char pos_kum [2] ;
char pr_ausw [2] ;
char pr_bel_entl [2] ;
char pr_lgr_kz [2] ;
long pr_lst ;
char pr_vk_kz[2] ;
double reg_bed_theke_lng ;
double reg_kt_lng ;
double reg_kue_lng ;
double reg_lng ;
double reg_tks_lng ;
double reg_tkt_lng ;
char smt_kz[2] ; 
short sonst_einh ;
short sprache ;
char sw_kz[2] ;
long tou ;
short vrs_typ ;
char inv_akv[2] ;
short delstatus ;
} ;

struct A_LGR {
double a ;
double a_lgr_kap ;
short delstatus ;
short fil ;
long lgr ;
short lgr_pla_kz ;
short lgr_typ ;
short mdn ;
char haupt_lgr [2] ;
char lgr_platz [11] ;
double min_bestand ;
double meld_bestand ;
double hoechst_bestand ;
long inh_wanne ;
};

extern struct LGR lgr, lgr_null;

extern struct A_LGR a_lgr, a_lgr_null;

class LGR_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int lese_a_lager (void);
               LGR_CLASS () : DB_CLASS ()
               {
               }
};

#endif

