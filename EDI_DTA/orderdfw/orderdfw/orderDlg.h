// orderDlg.h : Headerdatei
//

#pragma once
#include "afxwin.h"


// CorderDlg-Dialogfeld
class CorderDlg : public CDialog
{
// Konstruktion
public:
	CorderDlg(CWnd* pParent = NULL);	// Standardkonstruktor

// Dialogfelddaten
	enum { IDD = IDD_ORDER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV-Unterst�tzung


// Implementierung
protected:
	HICON m_hIcon;

	// Generierte Funktionen f�r die Meldungstabellen
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CEdit m_mandant;
public:
	CString v_mandant;
public:
	CEdit m_mdnname;
public:
	CString v_mdnname;
public:
	CComboBox m_combo1;
public:
	CString v_combo1;
public:
	CEdit m_filename;
public:
	CString v_filename;
public:
	CButton m_check1;
public:
	BOOL v_check1;
public:
	CEdit m_info;
public:
	CString v_info;
public:
	afx_msg void OnBnClickedOk();

public:
	afx_msg BOOL PreTranslateMessage(LPMSG) ;
	afx_msg BOOL NoListCtrl (CWnd *) ;
	afx_msg void myDisplayMessage(char * ) ;
public:
	afx_msg void OnBnClickedCheck1();
public:
	afx_msg void OnEnKillfocusMandant();
public:
	afx_msg void OnEnKillfocusFilename();
public:
	afx_msg void OnBnClickedButton1();
public:
	afx_msg void OnCbnSelchangeCombo1();
public:
	afx_msg void OnCbnKillfocusCombo1();
public:
	CButton m_tsbeacht;
public:
	BOOL b_tsbeacht;
public:
	CEdit m_beding1;
public:
	CString v_beding3;
public:
	CEdit m_beding2;
public:
	CString v_beding2;
public:
	CEdit m_beding3;
public:
	CString v_beding1;
public:
	afx_msg void OnBnClickedTsbeacht();
public:
	void lesesmtDatei (void) ;
	void schreibesmtDatei(void) ;
};
