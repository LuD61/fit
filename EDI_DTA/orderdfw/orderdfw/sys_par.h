#ifndef _SYS_PAR_DEF
#define _SYS_PAR_DEF

struct SYS_PAR {
   char      sys_par_nam[19];
   char      sys_par_wrt[2];
   char      sys_par_besch[33];
   long      zei;
   short     delstatus;
};
extern struct SYS_PAR sys_par, sys_par_null;

class SYS_PAR_CLASS {
		private :
			int cursor;
			void prepare (void);
            int dbreadfirst (void);
            int dbread (void);
        public :
			SYS_PAR_CLASS () : cursor (-1)
            {
            }
			// ACHTUNG : Die Parameterfelder mindestens so gross wie die daba-Felder definieren , sonst Laufzeit-Bugs
			int sys_par_holen ( char * sys_par_nam , char * sys_par_wrt, char * sys_par_besch ) ;
};
#endif

