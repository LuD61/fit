#include "stdafx.h"
#include "DbClass.h"
#include "aufp.h"

struct AUFPT aufpt, aufpt_null;

struct AUFPNR aufpnr, aufpnr_null; 

struct AUFP aufp,  aufp_null;

extern DB_CLASS dbClass;

AUFPT_CLASS   aufpt_class ;

AUFPNR_CLASS  aufpnr_class ;

AUFP_CLASS aufp_class ;

long AUFPNR_CLASS::holenummer (void)
{

	if (ins_cursor < 0 )
	{
		prepare ();
	}

	int retco ;

/* ----> 130609 : das war wohl das problem 
	aufpnr.tag = 47114711 ;
    retco = dbClass.sqlexecute ( ins_cursor ) ;
    if ( ! retco ) retco =dbClass.sqlopen (count_cursor);
    if ( ! retco ) retco = dbClass.sqlfetch (count_cursor);
    if ( ! retco ) retco = dbClass.sqlexecute  (del_cursor) ;
	if ( ! retco )	return ( aufpnr.nr ) ;
	return -1 ;
< --- */

/* ------> 040909 das auch noch weg, hier folgt dann die Wille-Variante 
	long taginput = 47114711 ;
	int test = 0 ;
	retco = 0 ;
	while ( test < 150 )
	{
		aufpnr.tag = taginput ;
		if ( ! retco ) retco = dbClass.sqlopen (count_cursor);
		if ( ! retco ) retco = dbClass.sqlfetch (count_cursor);
		if ( ! retco )
		{
			// da steht noch was altes rum, ganz schnell weg damit hier ....
			dbClass.sql_mode = 2 ;
			retco = dbClass.sqlexecute  (cursor_ausw) ;
			dbClass.sql_mode = 0 ;
			test ++ ;
			taginput ++ ;
			retco = 0 ;	// 140809
			continue ;
		}

		aufpnr.tag = taginput ;
		if ( test < 150 ) dbClass.sql_mode = 2 ;	// 130809 : keine Fehler-Behandlung
		retco = dbClass.sqlexecute ( ins_cursor ) ;	// nur hier kann es eigentlich knallen .....
		if ( retco )	// 130809
		{
			dbClass.sql_mode = 0 ;
			Sleep ( 1000 ) ;
			test ++ ;
			taginput ++ ;
			retco = 0 ;
			continue ;
		}
		if ( ! retco ) retco = dbClass.sqlopen (count_cursor);
		if ( ! retco ) retco = dbClass.sqlfetch (count_cursor);
		if ( ! retco ) retco = dbClass.sqlexecute  (del_cursor) ;
		dbClass.sql_mode = 0 ;	// 130809
		if ( ! retco )	return ( aufpnr.nr ) ;
		return -1 ;
	}
	return -1 ;
< ----- */
// 040909 A ab hier mal der wille-ablauf rein
    time_t timer;
// 210910 red.   int dsqlstatus;
// 210910 red.   int cursor;
//    extern short sql_mode;
//	 short sqls;


	dbClass.sql_mode = 2 ;	
		
	time (&timer);
	long itag = (long) timer ;
    aufpnr.tag = itag ;
//         cursor = DbClass.sqlcursor ("select tag from aufpnr where tag = ?");
	retco = dbClass.sqlopen (count_cursor);
	if ( ! retco ) retco = dbClass.sqlfetch (count_cursor);
	while (retco == 0)
    {
		Sleep (1000); 
        time (&timer);
        if (itag == (long) timer)
        {
            continue;
        } 
        itag = (long) timer;
		aufpnr.tag = itag ;
		retco = dbClass.sqlopen (count_cursor);
		if ( ! retco ) retco = dbClass.sqlfetch (count_cursor);
    }
	if (retco < 0) 
	{
		 dbClass.sql_mode = 0;
			 return -1 ;
	}
	aufpnr.tag = itag ;
//    dsqlstatus = DbClass.sqlcomm ("insert into aufpnr (tag) values (?)");
	retco = dbClass.sqlexecute ( ins_cursor ) ;	// nur hier kann es eigentlich knallen .....
	if (retco < 0) 
	 {
			 dbClass.sql_mode = 0;
			 return -1;
	 }
     
	aufpnr.tag = itag ;
    aufpnr.nr = 0l;
	retco = dbClass.sqlopen (count_cursor);
	if ( ! retco ) retco = dbClass.sqlfetch (count_cursor);
//         dsqlstatus = DbClass.sqlcomm ("select nr from aufpnr where tag = ?");
	if (retco < 0) 
	{
		dbClass.sql_mode = 0 ;
		return -1 ;
	}

 	aufpnr.tag = itag ;
	retco = dbClass.sqlexecute  (cursor_ausw) ;
 //       DbClass.sqlcomm ("delete from aufpnr where tag = ?");

	dbClass.sql_mode = 0 ;
    return aufpnr.nr; 

// 0409090 E

}

int AUFPT_CLASS::insertaufpt (void)
{

         if (ins_cursor < 0 )
         {
             prepare ();
         }
         return ( dbClass.sqlexecute  (ins_cursor)) ;
}

int AUFP_CLASS::insertaufp (void)
{

         if (ins_cursor < 0 )
         {
             prepare ();
         }
         return ( dbClass.sqlexecute  (ins_cursor)) ;
}

void AUFPT_CLASS::prepare (void)
{


	dbClass.sqlin ((long *) &aufpt.nr, SQLLONG, 0 ) ; 
	dbClass.sqlin ((long *) &aufpt.zei, SQLLONG, 0 ) ;
	dbClass.sqlin ((char *) aufpt.txt, SQLCHAR, 61 ) ;

	ins_cursor = (short) dbClass.sqlcursor ("insert into aufpt "
	  " ( nr, zei, txt ) values ( ?,?,? ) " ) ;
}


void AUFPNR_CLASS::prepare (void)
{

	dbClass.sqlin ((long *) &aufpnr.tag, SQLLONG, 0 ) ; 

	ins_cursor = (short) dbClass.sqlcursor ("insert into aufpnr ( tag )"
		"values ( ? ) " ) ;


	dbClass.sqlout ((long *) &aufpnr.tag, SQLLONG, 0 ) ;	// 040909 : tag dazu 
	dbClass.sqlout ((long *) &aufpnr.nr, SQLLONG, 0 ) ; 
	dbClass.sqlin ((long *) &aufpnr.tag, SQLLONG, 0 ) ; 

	count_cursor = (short) dbClass.sqlcursor ("select tag, nr from aufpnr "
		" where tag = ? " ) ;

	dbClass.sqlin ((long *) &aufpnr.nr, SQLLONG, 0 ) ; 
	del_cursor = (short) dbClass.sqlcursor ("delete from aufpnr "
		" where nr = ? " ) ;

	// 130809 : cursor_ausw noch dazu gebastelt

	dbClass.sqlin ((long *) &aufpnr.tag, SQLLONG, 0 ) ; 
	cursor_ausw = (short) dbClass.sqlcursor ("delete from aufpnr "
		" where tag = ? " ) ;

}


void AUFP_CLASS::prepare (void)
{


	dbClass.sqlin ((short *) &aufp.mdn, SQLSHORT, 0 ) ; 
	dbClass.sqlin ((short *) &aufp.fil, SQLSHORT, 0 ) ;
	dbClass.sqlin ((long *) &aufp.auf, SQLLONG, 0 ) ;
	dbClass.sqlin ((long *) &aufp.posi, SQLLONG, 0 ) ;
	dbClass.sqlin ((long *) &aufp.aufp_txt, SQLLONG, 0 ) ;

	dbClass.sqlin ((double *) &aufp.a, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((double *) &aufp.auf_me, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((char *) aufp.auf_me_bz, SQLCHAR ,7 ) ;
	dbClass.sqlin ((double *) &aufp.lief_me, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((char *) aufp.lief_me_bz, SQLCHAR , 7 ) ;

	dbClass.sqlin ((double *) &aufp.auf_vk_pr, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((double *) &aufp.auf_lad_pr, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((short *) &aufp.delstatus, SQLSHORT, 0 ) ;
	dbClass.sqlin ((short *) &aufp.sa_kz_sint, SQLSHORT, 0 ) ;
	dbClass.sqlin ((double *) &aufp.prov_satz, SQLDOUBLE, 0 ) ;

	dbClass.sqlin ((long *) &aufp.ksys, SQLLONG, 0 ) ;
	dbClass.sqlin ((long *) &aufp.pid, SQLLONG, 0 ) ;
	dbClass.sqlin ((long *) &aufp.auf_klst, SQLLONG, 0 ) ;
	dbClass.sqlin ((short *) &aufp.teil_smt, SQLSHORT, 0 ) ;
	dbClass.sqlin ((short *) &aufp.dr_folge, SQLSHORT, 0 ) ;

	dbClass.sqlin ((double *) &aufp.inh, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((double *) &aufp.auf_vk_euro, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((double *) &aufp.auf_vk_fremd, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((double *) &aufp.auf_lad_euro, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((double *) &aufp.auf_lad_fremd, SQLDOUBLE, 0 ) ;

	dbClass.sqlin ((double *) &aufp.rab_satz, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((short *) &aufp.me_einh_kun, SQLSHORT, 0 ) ;
	dbClass.sqlin ((short *) &aufp.me_einh, SQLSHORT, 0 ) ;
	dbClass.sqlin ((short *) &aufp.me_einh_kun1, SQLSHORT, 0 ) ;
	dbClass.sqlin ((double *) &aufp.auf_me1, SQLDOUBLE, 0 ) ;

	dbClass.sqlin ((double *) &aufp.inh1, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((short *) &aufp.me_einh_kun2, SQLSHORT, 0 ) ;
	dbClass.sqlin ((double *) &aufp.auf_me2, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((double *) &aufp.inh2, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((short *) &aufp.me_einh_kun3, SQLSHORT, 0 ) ;

	dbClass.sqlin ((double *) &aufp.auf_me3, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((double *) &aufp.inh3, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((long *) &aufp.gruppe, SQLLONG, 0 ) ;
	dbClass.sqlin ((char *)  aufp.kond_art, SQLCHAR , 5 ) ;
	dbClass.sqlin ((double *) &aufp.a_grund, SQLDOUBLE, 0 ) ;

	dbClass.sqlin ((short *) &aufp.ls_pos_kz, SQLSHORT, 0 ) ;
	dbClass.sqlin ((long *) &aufp.posi_ext, SQLLONG, 0 ) ;
	dbClass.sqlin ((long *) &aufp.kun, SQLLONG, 0 ) ;
	dbClass.sqlin ((double *) &aufp.a_ers, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((char *) aufp.ls_charge, SQLCHAR ,31 ) ;

	dbClass.sqlin ((short *) &aufp.aufschlag, SQLSHORT, 0 ) ;
	dbClass.sqlin ((double *) &aufp.aufschlag_wert, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((short *) &aufp.pos_txt_kz, SQLSHORT, 0 ) ;


	ins_cursor = (short) dbClass.sqlcursor ("insert into aufp ( "
	  " mdn, fil, auf, posi, aufp_txt "
	" , a, auf_me, auf_me_bz, lief_me, lief_me_bz "
	" , auf_vk_pr, auf_lad_pr, delstatus, sa_kz_sint, prov_satz "
	" , ksys, pid, auf_klst, teil_smt, dr_folge "
	" , inh, auf_vk_euro, auf_vk_fremd, auf_lad_euro, auf_lad_fremd "
	" , rab_satz, me_einh_kun, me_einh, me_einh_kun1, auf_me1 "
	" , inh1, me_einh_kun2, auf_me2, inh2, me_einh_kun3 "
	" , auf_me3, inh3, gruppe, kond_art, a_grund "
	" , ls_pos_kz, posi_ext, kun, a_ers, ls_charge "
	" , aufschlag, aufschlag_wert, pos_txt_kz "
	" ) values ( "
	  " ?, ?, ?, ?, ? "
	" , ?, ?, ?, ?, ? "
	" , ?, ?, ?, ?, ? "
	" , ?, ?, ?, ?, ? "
	" , ?, ?, ?, ?, ? "
	" , ?, ?, ?, ?, ? "
	" , ?, ?, ?, ?, ? "
	" , ?, ?, ?, ?, ? "
	" , ?, ?, ?, ?, ? "
	" , ?, ?, ? "
	" ) " ) ;

}

