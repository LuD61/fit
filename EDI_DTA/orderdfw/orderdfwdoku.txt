Status 15.04.09
Absprache mit Herr Mauer :

1. alle Infos aus der csv-Datei gehen 1:1 durch  ( Kunden-Nr, Artikel,Datum ... )
2. Sortierung innerhalb Datei ist so ,dass je Gruppenwechsel neuer Auftrag zu generieren ist
   (spalte1 = Kunde, Spalte2 = Datum Spalte3 = "Sortiment")
3. es gibt eigentlich nur "St.." "kg" "X"  d.h.  Bestell- und fakturier-Einheit sind synchronisiert mit fit

4. Text-Infos sind "simple-Texte" , die als Pos-Texte darzustellen sind









Status 24.07.08 :
alpha-rel. order.exe  zum Einlesen von EDI-Orders-Dateien
Dazu gehoert die LuL-Liste "orderpro", welche nach jeder Uebernahem startet und je nach Verlauf 
die uebernommenen/nicht uebernommenen Belege ausweist und eventuelle Probleme / Fehler dokumentiert 


Start des Programmes ist ohne Optionen oder mit Optionen moeglich

Standard-Funktionalitaet :

0. Preisfindung findet prinzipiell mittels preisewa.dll statt. Fehlt diese dll, bricht das Programm ab.
1. Es werden auf Basis der angegebnen dta-Nummer die Absender-ILN und auf Basis der eignen Mandanten-ILN die Empfaenger-ILN geprueft und die Datei bei Notwendigkeit abgewiesen.
2. Auf Basis des DP-Segments wird der Lieferungsempfaenger bestimmt ( Kunden-ILN bzw. Plattform).Falls Probleme auftreten, wird die Datei abgewiesen.
3. Die zu liefernden Artikel werden mittels LIN-EAN , PIA-BP , PIA-IN oder PIA-SA identifiziert( d.h. EAN , Kunden-Artikelnummer, eigne Artikelnummer). EAN bzw. Kunden-Artikelnummer(alphanumerisch !!) sind in a_kun(18550) zu erfassen. Artikel mit 0-Menge bzw. nicht zuordenbare Artikel werden protokolliert, die Uebernahme in die Daba fuer alle anderen Positionen findet trotzdem statt.
4. Bestelleinheit wird entweder auf Basis der mitgelieferten Einheit ( bei KGM bzw. GRM ) festgelegt oder aus der Tabelle a_kun oder ersatzweise aus a_hndw, a_eig, a_eig_div ermittelt. Notbremse : a_bas.me_einh
5. Falls in a_kun ein gebinde-Faktor (a_kun.geb_fakt) hinterlegt ist, wird dieser zusaetzlich ausgewertet.
6. Falls die Datei mit aktivem Testkennzeichen versehen ist, muss bei Uebernahmewunsch die entsprechende Checkbox angeklickt werden.  
7. PRI+AAE-Segment wird (falls mitgesendet ) als Auszeichnungspreis in den lad_pr geschrieben
8. Systemparameter li_a_edi wird ausgewertet   ( a_kun.li_a wird intern genutzt )
9. Sonderstrukturen wie Schwarzwaldhof bzw. Frankengut mit Positions-ID werden z.Z. nicht unterstuetzt


OPTIONEN :
  "-PLATTFORM"    // bei aktivierter Option wird zur Lieferempfaengeridentifikation das feld kun.plattform ausgwertet,
                  // Standard-Ablauf : Das feld kun.iln wird ausgewertet
	diese Option wird zusaetzlich zu anderen Optionen ausgewertet

	die folgenden Optionen duerfen nur alternativ eingesetzt werden ,die letzte Option erhaelt Gueltigkeit

  "-FEGRO"        // die ean-Aufloesung erfolgt auf Basis a_kun_gx.ean bzw. a_kun_gx.ean1  ( ean-Eintraege immer 12 stellig !)
                  // Standard-Ablauf : Aufloesung auf Basis a_kun.ean bzw. a_kun.ean_vk

  "-METRO"        // EAN-Eintraege werden ignoriert, Aufloesung erfolgt ueber a_kun.a_kun

  "-REWE"         // EAN-Eintraege werden ignoriert, Aufloesung erfolgt uber interne Artikelnummer
                  // falls ein Artikel nicht identifizeirt werden kann, wird die DAtei NICHT akzeptiert

  "-EDEKA"        // falls in der EAN "000000000000" steht, wird zusaetzlich das IMD(A bzw. F) -Feld ausgewertet und im Klartext im Protokoll dargestellt.



10. Zuerst fuer METRO-Real realisiert : Mehrfach-ILN aufloesen (Bedingung : sortimentsreine Bestellungen )

	Die Situation ensteht z.B. typischerweise bei metro1_par : Die Kunden werden von vornherein sortimentsgetrennt behandelt.
	Alternativ sind auch Abl�ufe mit Sortimentstrennung erst bei LS-�bergabe denkbar, das geht dann jedoch genau auf einen Kunden
	und somit nicht in diesem Ablauf

	bei aktivierter Branchentrennung koennen z.Z. bis zu 3 Zuordnungen Branche<-> Sortimente angegeben werden, 
	Diese werden jeweils lokal in der Datei $BWSETC\dtaNNNNNNN.par abgespeichert
 
	fogende Struktur wird unterstuetzt :
	Es gibt je ILN mehrere Kunden ( z.B. wegen Kondition Frische/Fleisch usw. -> MGB-Nummern )

	!!! Je Bestellung muss sortimentsrein (!!!!) bestellt werden

	Das Programm sucht dann mittels dem 1. Artikel heraus, welcher der Kunden angesprochen werden soll
	Poblem : Mehrfach-Zuordnung in der Aufloesug  :
	!!! EAN in erlaubten a_kun.ku_bran2 sollte unique sein   
	!!! a_kun.a_kun  in erlaubten a_kun.kun_bran2 sollte unique sein

	Ansonsten wird der erste passende Eintrag genutzt -> kann zu Fehlentscheidungen fuehren 

	Bemerkung : falls nur Einfach-ILN gefunden, dann wird dieser Kunde genutzt ohne weitere Sortimentsbewertung

















