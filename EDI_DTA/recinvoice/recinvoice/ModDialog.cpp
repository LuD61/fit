// ModDialog.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "recinvoice.h"
#include "ModDialog.h"


// ModDialog-Dialogfeld

IMPLEMENT_DYNAMIC(ModDialog, CDialog)

ModDialog::ModDialog(CWnd* pParent /*=NULL*/)
	: CDialog(ModDialog::IDD, pParent)
	, m_bnurprot(FALSE)
	, m_blistedicht(FALSE)
	, m_bdetails(FALSE)
	, m_bverbuchen(FALSE)
{

}

ModDialog::~ModDialog()
{
}

void ModDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_NURPROT, m_nurprot);
	DDX_Control(pDX, IDC_LISTEDICHT, m_listedicht);
	DDX_Control(pDX, IDC_DETAILS, m_details);
	DDX_Control(pDX, IDC_VERBUCHEN, m_verbuchen);
	DDX_Check(pDX, IDC_NURPROT, m_bnurprot);
	DDX_Check(pDX, IDC_LISTEDICHT, m_blistedicht);
	DDX_Check(pDX, IDC_DETAILS, m_bdetails);
	DDX_Check(pDX, IDC_VERBUCHEN, m_bverbuchen);
}


BEGIN_MESSAGE_MAP(ModDialog, CDialog)
	ON_BN_CLICKED(IDC_NURPROT, &ModDialog::OnBnClickedNurprot)
	ON_BN_CLICKED(IDC_LISTEDICHT, &ModDialog::OnBnClickedListedicht)
	ON_BN_CLICKED(IDC_DETAILS, &ModDialog::OnBnClickedDetails)
	ON_BN_CLICKED(IDC_VERBUCHEN, &ModDialog::OnBnClickedVerbuchen)
	ON_BN_CLICKED(IDOK, &ModDialog::OnBnClickedOk)
END_MESSAGE_MAP()

static int xdefnurprot, xvisnurprot ;
static int xdeflistedicht, xvislistedicht ;
static int xdefdetails, xvisdetails ;
static int xdefverbuchen, xvisverbuchen ;

// ModDialog-Meldungshandler

void ModDialog::setzeparas (
							 int ydefnurprot, int yvisnurprot
							,int ydeflistedicht, int yvislistedicht 
							,int ydefdetails, int yvisdetails 
							,int ydefverbuchen, int yvisverbuchen )
{
	xdefnurprot = ydefnurprot ;
	xvisnurprot = yvisnurprot ;

	xdeflistedicht = ydeflistedicht ;
	xvislistedicht = yvislistedicht ;

	xdefdetails = ydefdetails ;
	xvisdetails = yvisdetails ;

	xdefverbuchen = ydefverbuchen ;
	xvisverbuchen = yvisverbuchen ;
}

void ModDialog::holeparas (
							 int * ydefnurprot
							,int * ydeflistedicht 
							,int * ydefdetails 
							,int * ydefverbuchen )
{
	 * ydefnurprot =  xdefnurprot ;

	 * ydeflistedicht = xdeflistedicht ;

	 * ydefdetails = xdefdetails ;

	 * ydefverbuchen = xdefverbuchen ;
}


BOOL ModDialog::OnInitDialog()
{

	CDialog::OnInitDialog();
	if ( xdefnurprot )
		m_bnurprot = TRUE ;
	else
		m_bnurprot = FALSE ;

	if ( xdeflistedicht )
		m_blistedicht = TRUE ;
	else
		m_blistedicht = FALSE ;

	if ( xdefdetails )
		m_bdetails = TRUE ;
	else
		m_bdetails = FALSE ;

	if ( xdefverbuchen )
		m_bverbuchen = TRUE ;
	else
		m_bverbuchen = FALSE ;


	if ( xvisnurprot )
		m_nurprot.EnableWindow(TRUE) ;
	else
		m_nurprot.EnableWindow(FALSE) ;

	if ( xvislistedicht )
		m_listedicht.EnableWindow(TRUE) ;
	else
		m_listedicht.EnableWindow(FALSE) ;

	if ( xvisdetails )
		m_details.EnableWindow(TRUE) ;
	else
		m_details.EnableWindow(FALSE) ;

	if ( xvisverbuchen )
		m_verbuchen.EnableWindow(TRUE) ;
	else
		m_verbuchen.EnableWindow(FALSE) ;

	UpdateData(FALSE) ;
	return TRUE ;

}



void ModDialog::OnBnClickedNurprot()
{
	if ( m_bnurprot == FALSE )
		 m_bnurprot = TRUE ;
	else
		 m_bnurprot = FALSE ;

	UpdateData(FALSE) ;
}

void ModDialog::OnBnClickedListedicht()
{
	if ( m_blistedicht == FALSE )
		 m_blistedicht = TRUE ;
	else
		 m_blistedicht = FALSE ;


	UpdateData(FALSE) ;
}

void ModDialog::OnBnClickedDetails()
{
	if ( m_bdetails == FALSE )
		 m_bdetails = TRUE ;
	else
		 m_bdetails = FALSE ;

	UpdateData(FALSE) ;
}

void ModDialog::OnBnClickedVerbuchen()
{
	if ( m_bverbuchen == FALSE )
		 m_bverbuchen = TRUE ;
	else
		 m_bverbuchen = FALSE ;
	UpdateData(FALSE) ;
}

void ModDialog::OnBnClickedOk()
{

	if ( m_bverbuchen )
		xdefverbuchen = TRUE ;
	else
		xdefverbuchen = FALSE ;

	if ( m_bdetails )
		xdefdetails = TRUE ;
	else
		xdefdetails = FALSE ;

	if ( m_blistedicht )
		xdeflistedicht = TRUE ;
	else
		xdeflistedicht = FALSE ;

	if ( m_bnurprot )
		xdefnurprot = TRUE ;
	else
		xdefnurprot = FALSE ;

	OnOK();
}
