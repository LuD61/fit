#pragma once
#include "afxwin.h"


// ModDialog-Dialogfeld

class ModDialog : public CDialog
{
	DECLARE_DYNAMIC(ModDialog)

public:
	ModDialog(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~ModDialog();

// Dialogfelddaten
	enum { IDD = IDD_MODDIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnInitDialog();
public:
	afx_msg void OnBnClickedNurprot();
public:
	afx_msg void OnBnClickedListedicht();
public:
	afx_msg void OnBnClickedDetails();
public:
	afx_msg void OnBnClickedVerbuchen();
public:
	CButton m_nurprot;
public:
	CButton m_listedicht;
public:
	CButton m_details;
public:
	CButton m_verbuchen;
public:
	BOOL m_bnurprot;
public:
	BOOL m_blistedicht;
public:
	BOOL m_bdetails;
public:
	BOOL m_bverbuchen;
public:
	void setzeparas(int,int, int,int, int,int, int,int );
public:
	void holeparas(int * , int * ,  int *, int * );
public:
	afx_msg void OnBnClickedOk();
};
