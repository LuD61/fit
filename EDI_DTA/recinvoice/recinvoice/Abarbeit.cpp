#include "stdafx.h"  

#include "recinvoice.h"
#include "recinvoiceDlg.h"

#include "DbClass.h"
#include "dta.h" 
#include "mdn.h"
#include "tempdr.h"
#include "lief.h"
#include "kun.h"
#include "lief_bzg.h"

#include "Abarbeit.h"
#include "ModDialog.h"


static int mogel = 0 ;	// gemogelt, damit es erst mal losgeht
static int gesamtfehlcount ;	// Zeahlt die gemeldeten Fehler
static long lfdliste ;	// Listennummer fuer Ausdruck
static long lfdlistennr ;	// lfd in der Liste
// extern struct DTA dta ;
// extern struct MDN mdn ;

extern TEMPDRX_CLASS tempdrx_class ;
extern LIEF_CLASS lief_class ;
extern FIL_CLASS fil_class ;
extern LIEF_BZG_CLASS lief_bzg_class ;

#define  TRENN_SE        "'"       // Segmentende
#define  TRENN_DE        "+"       // Datenelemente+Segmentbezeichner
#define  TRENN_GD        ":"       // Gruppendatenelemente
#define  TRENN_MA        "?"       // Maskierungs(freigabe-)zeichen
#define	 TRENN_DZ        "."       // Dezimal-Trenner    
#define CIPPUFFLEN      1000      // Laenge Input-Puffer


static char trenn_se[2] ;
static char trenn_de[2] ;
static char trenn_gd[2] ;
static char trenn_ma[2] ;
static char trenn_dz[2] ;

// Ptrototyping 

void write_belkopf_a ( void ) ;
void write_belpos_a  ( void ) ;
void write_belfuss_a ( void ) ;

int read_belkopf_a ( void ) ;
int read_belpos_a  ( void ) ;
int read_belfuss_a ( void ) ;


// Unsere Aufgabe sei NICHT primaer das Finden von Fehlern, sondern von gueltigen Informationen

static long dta_referenz ;

static char cnari[21] ;		// aktuelle Nachricht
static int narityp	;		// z.B. 380,381,393, 325 usw.
static int inblock = 0 ;	// 0 ==> UNB ==> 1
static int inbeleg = 0 ;
							// 0 : ausserhalb Struktur
							// 0 ==> UNH ==> 1
							// 1 ==> BGM ==> 2
							// 2 : bin im Kopf
							// 2 ==> LIN ==> 3
							// 3 : bin in Liste 
							// 3 ==> UNS ==> 4
							// 4 : bin im Fuss
							// 4 ==> UNT ==> 0

static int inposten = 0 ;
							// 0 : ausserhalb Struktur
							// 0/1 ==> LIN ==> 1	// gegbnenfalls Posten weiterschalten ......
							// 1 : bin in Posten
							// 1 ==> UNH,BGM,UNS,UNT usw. ==> 0
							

struct BELKOPF
{
char bgm_typ [5] ;	// z.Z. nur 380, 381  und 393 erlaubt, LS usw. werden ueberlesen
long bgm_nr  ;
char nad_su_feld [21] ;
char nad_by_feld [21] ;
char nad_dp_feld [21] ;
char dtm_35_feld [21] ;	// Lieferdatum
char dtm_137_feld [21] ;	// datum der Dokumenterstellung (== Rechungsdatum )
char rff_abo_feld[21] ;	// rechnungsliste
char rff_on_feld [21] ;	// referenz auf bestellung 
char tax_feld[11] ;
char bemerkung[41] ;	// Kommentar/Kurzfehler
} ;

struct BELKOPF belkopf , belkopf_null ;
struct BELKOPF * belkopf_a ;
static int belkopfdim ;
static int belkopfmax ;
static int belkopfakt ;


struct BELPOS
{
char bgm_typ [5] ;	// z.Z. nur 380, 381 und 393 erlaubt, LS usw. werden ueberlesen
long bgm_nr  ;

char tax_feld[11] ;
char lin_feld[21] ;
char pia_sa_feld[21] ;	// Nummer des Lieferanten ( lief_best )
char pia_bp_feld[21] ;	// Nummer des Kaeufers ( eigne Nr. ) 
char imd_a_feld[70] ;	// Klartext
int imd_in_aktiv ;	// Kodier-Feld :fakturier-Einheit, der Rest ist Mist 
int imd_rf_aktiv ;	// Leergut-Artikel
double qty_47_wert ;
char qty_47_einh[11] ;
double pri_aab_wert ;
char pri_aab_einh[11] ;
double moa_203_wert ;
char bemerkung[41] ;	// Kommentar/Kurzfehler

} ;

struct BELPOS belpos , belpos_null ;

struct BELPOS * belpos_a ;
static int belposdim ;
static int belposmax ;
static int belposakt ;



static char belfusstaxaktiv[11] ;
static int belfusstaxaktpo ;

struct BELFUSS
{
char bgm_typ [5] ;	// z.Z. nur 380, 381 und 393 erlaubt, LS usw. werden ueberlesen
long bgm_nr  ;
double moa_77_wert ;
double moa_79_wert ;
double moa_124_wert ;
double moa_125_wert ;
double moa_131_wert ;
int tax_1_aktiv ;
char tax_f_1[11] ; 
double moa_1_79_wert ;
double moa_1_124_wert ;
double moa_1_125_wert ;
double moa_1_131_wert ;
int tax_2_aktiv ;
char tax_f_2[11] ; 
double moa_2_79_wert ;
double moa_2_124_wert ;
double moa_2_125_wert ;
double moa_2_131_wert ;
char bemerkung[41] ;	// Kommentar/Kurzfehler

} ;

struct BELFUSS belfuss, belfuss_null ;
struct BELFUSS * belfuss_a ;
static int belfussdim ;
static int belfussmax ;
static int belfussakt ;

int maskel ;	// Vorher-Zeichen war trenn_ma ( ==> "?" ) 
int dtestignore ;			// test-kz-ignorier-Schalter 

int TEST =  0 ;					// 0 * inaktiv
                                // 1 * lesesegment

int danz_seg  ;		// Anzahl Segmente einer Nachricht
int k ;

FILE * fpin ;		// File-Descriptor der Datei ( oder eben nicht ..... )

int ref_nr ;			// lfd. Nr in Nachricht 
int danz_nachrichten ;	// Anzahl Nachrichten

char eigen_iln [20] ;	
/* --->
field rechempf_iln      char(16)
field eigen_iln         char(16)
field dta_iln           char(16)
field dasciiopen        smallint
field ddateinames       char(100)
field dteilsmtbeacht    smallint        // 281002
                                        // 0 = nicht beachten
                                        // 2 holen aus tsmtg

field firstprotopen     smallint

field daufwaehr         smallint        // Auftragswaehrung gebnenfalls abweichend
                                         // von Kunden-Waehrung

< ----- */

int daufstat  ;							// 0 =  noch nix
                                        // 1 = bgm=220 erkannt
                                        // 2 = Lieferdatum
                                        // 3 = Liefer-ILN
                                        // 4 = Positions-infos vorhanden

int daufpstat ;							// 0 = neue Position
                                        // 1 = artikel vorhanden
                                        // 2 = Menge vorhanden


int rech_k_stat ;	// Stati innerhalb Kopf
int rech_p_stat ;	// Stati innerhalp Pos

char lin_ean[16] ;

int  testphase ;
int  dtestmode ;
int  dabbruch = 0 ;
int  pia2_gefunden ;
int  dalterstil = 0 ;  

char posmhd [12]    ;    //   date
int poshbkz         ;
char ftx_zzz_1[110] ;	// D1.0b  bis 99 Zeichen 
char ftx_zzz_2[110] ;    
char ftx_zzz_3[119] ;
char ftx_zzz_4[110] ; 
char ftx_zzz_5[110] ; 

/* ---->
field s_p_r     smallint
field s_p_w     char(10)
field s_p_b     char(10)
field dnachkpreis integer
<---- */

int dvertr_int_par  ;

int dwa_pos_txt ;
int dneunr ;
char dateinamex [110] ;

//  inputpuffer ( Datei )                       ippuffer
//      Segmentpuffer                           segpuffer
//          Datenelement                        depuffer
//              Gruppendatenelement             gdpuffer



char  ippuffer[CIPPUFFLEN + 5] ;     // Inputpuffer
int iprpoint ;		// Lesepointer Inputpuffer
int ippufflen ;  

char dsegpuffer [750] ;		// Segmentpuffer aus inputpuffer
int dsegwpoint ;			// Schreibpointer in Segmentpuffer
int dsegpufflen ;

char depuffer [550] ;		// Datenelementpuffer aus segpuffer
int dsegrpoint ;			// Lesepointer aus Datengsegment
int dewpoint ;				// Schreibpointer in Datenelement
int depufflen ;

char gdpuffer[150] ;		// Gruppendaten-Puffer aus depuffer
int derpoint ;			 	// Lesepointer aus depuffer
int gdwpoint ;				// Schreibpointer in Gruppendatenelementpuffer

int decount ;				// Nummer des Datenelements im Segment
int gdcount ;				// Nummer des Gruppendatenelements

// Das Ding mit 2-stelligen Namen ist irgendwie Quatsch , das kann eigentlich nur ein Fehler sein,
// lt. Normung gibt es nur 3-stellige Namen 

char  dsegname3 [10] ;                
int dsegguelt		;		// ob 2 oder 3-stell-Name ( 0/2/3 )
char  dsegname2 [10] ;

/* ---> further usage
= TABLE mart DIMENSION ( CMAXART )
= field a decimal (13,3)
= END
-------------- */

char dstring150 [152] ;
char dstring80  [ 82] ;
char dstring50  [ 52] ;
char dstring20  [ 22] ;

/** Trennzeichen mit aktuellen Werten laden **/

void trennz_laden(void )
{
	// Default-Vorladung, spaeter mal imRahmen des UNA_segments Ueberladung vorstellbar ...
   sprintf ( trenn_se , "%s" , TRENN_SE ) ;
   sprintf ( trenn_de , "%s" , TRENN_DE ) ;
   sprintf ( trenn_gd , "%s" , TRENN_GD ) ;
   sprintf ( trenn_ma , "%s" , TRENN_MA ) ;
   sprintf ( trenn_dz , "%s" , TRENN_DZ ) ;
}

/* ----> further usage ----->
= PROCEDURE martsort
=  field k1 integer
=  field k2 integer
=  table marttau like mart end
=    let k1 = 0
=    while ( ( k1 < (CMAXART - 1) ) and ( mart[k1].a > 0 ) )
=        if ( mart[ k1 + 1 ].a > 0 )
=            if ( mart[k1 + 1].a < mart[k1].a )
=                move table mart[k1] to table marttau
=                move table mart[k1 + 1] to table mart[k1]
=                move table marttau to table mart[k1 + 1]
=                if k1 > 0
=                   let k1 = k1 - 1
=                end
=                continue
=            end
=        end
=        let k1 = k1 + 1
=    end
= END


/* ----> 
    perform sys_par_holen ( "vertr_abr_par" )
    returning ( s_p_r, s_p_w s_p_b )
    if s_p_r = 0
        let s_p_w = "0"
    end
    let dvertr_int_par = s_p_w
        let dlgr_bestand = 0
        perform sys_par_holen ("lgr_bestand" )
                returning ( s_p_r, s_p_w, s_p_b )
        let dlgr_bestand = s_p_w
        if ((s_p_r = FALSE) or (s_p_w = "0"))
                let dlgr_bestand = 0
	end
    init ddateiname
    environment ("EDI", ddateiname )

    perform sys_par_holen ("wa_pos_txt" )
                returning ( dwa_pos_txt, s_p_w, s_p_b )

    if (dwa_pos_txt = FALSE)
        let dwa_pos_txt = 0
    else
        let dwa_pos_txt = s_p_w         // konvertiert implizit ...
    end

    perform sys_par_holen ("neunr" )
              returning ( dneunr, s_p_w, s_p_b )
    let dneunr = s_p_w
    if ((dneunr = FALSE) or (s_p_w = "0"))
        let dneunr = 0
    end

	perform sys_par_holen ("nachkpreis" )
                returning ( dnachkpreis, s_p_w, s_p_b )

    if (dnachkpreis = FALSE)
        let s_p_w = "2"
    end

    let dnachkpreis = 2

    if s_p_w = "3"
        let dnachkpreis = 3
    end
    if s_p_w = "4"
        let dnachkpreis = 4
    end
< ------ */


// binaer : byte fuer byte lesen und noch cr/lf wegfiltern, returnwert ist gleich Anzahl Zeichen im ippuffer

int ippufferread (void )
{
	char buffer [5];
	int ch ;
	ippufflen = 0 ;
	int inpoint = 0 ;
	if ( fpin == NULL ) return 0 ;

	memset ( ippuffer , '\0', sizeof ( ippuffer )) ;

	while ( inpoint < CIPPUFFLEN )
	{
		ch = fgetc( fpin ) ;
		buffer[0] = (char) ch ;
		if ( feof(fpin) == 0 )
		{
			if ( buffer[0] == '\n' || buffer[0] == 0x0D || buffer[0] == 0x0A )
			{
				// gegen ferkels ....
			}
			else
			{
				ippuffer[inpoint++] = buffer[0] ;
			}
			continue ;
		}
		else
		{
			fclose ( fpin ) ;
			fpin = NULL ;	// zu oder fertig 
			break ;
		}
	}
	return  inpoint  ;
}


// ######################################################################
// #### PROC.:  lesesegment
// #### liest naechstes Segment von Start bis Segment-Trenner bzw. crlf
// #### in den Empfangspuffer
// ######################################################################


int lesesegment ( CrecinvoiceDlg * Cparent )
{
char hilfchar[2] ;
    sprintf ( hilfchar , "X") ;
    if ( iprpoint < ippufflen )
	{
        hilfchar[0] = ippuffer[iprpoint] ;
		while (( hilfchar[0] != trenn_se[0]) || maskel )	// alles vor dem Segmenttrenner ....
		{
			if ( maskel )
			{ 
				maskel = 0 ;
			}
			else
			{
				if ( hilfchar[0] == trenn_ma[0]) maskel = 1 ;
			}
            dsegpuffer[dsegwpoint] = hilfchar[0] ;
            dsegwpoint ++ ;
            iprpoint ++ ;
            if ( iprpoint >= ippufflen )
			{
                 ippufflen = ippufferread() ;
				 if ( ippufflen < 1 )
				 {
						maskel = 0 ;	// hier muss ja selbst bei error schluss sein !!!!
						break ;
				 }
				iprpoint = 0 ;
			}
            hilfchar[0]  = ippuffer[iprpoint] ;
		}
        if ( hilfchar[0] == trenn_se[0] )
		{	// maskel gibbet hier niemals 
			maskel = 0 ;
			iprpoint ++ ;
            if ( TEST == 1 )
			{
				MessageBox(NULL, dsegpuffer, " ", MB_OK|MB_ICONSTOP);
			}
			else
			{
	

				Cparent->myDisplayMessage( dsegpuffer ) ;
			}
			return  0 ;
		}
	}
    else
	{
		ippufflen = ippufferread() ;
		if ( ippufflen < 1 )
		{
			maskel = 0 ;
			return -1 ;
		}
		iprpoint = 0 ;
		hilfchar[0]  = ippuffer[iprpoint] ;
		while ((hilfchar[0] != trenn_se[0] )|| maskel )
		{
			if ( maskel )
			{ 
				maskel = 0 ;
			}
			else
			{
				if ( hilfchar[0] == trenn_ma[0]) maskel = 1 ;
			}
			dsegpuffer[dsegwpoint] = hilfchar[0] ;
			dsegwpoint ++ ;
			iprpoint ++ ;
			if ( iprpoint >= ippufflen )
			{
                return lesesegment(Cparent) ;
			}
			hilfchar[0]  = ippuffer[iprpoint] ;
		}
		if ( hilfchar[0] == trenn_se[0] )
		{
			maskel = 0 ;	// maskel gibbet hier niemals ....
			iprpoint ++ ;
			if ( TEST == 1 )
			{
				MessageBox(NULL, dsegpuffer , " ", MB_OK|MB_ICONSTOP);

			}
			else
			{

			Cparent->myDisplayMessage( dsegpuffer ) ;
			}
			return  0 ;
		}
	}
	return  -1 ;    // Syntax-Dummy
}

void UNA_laden (void)
{
	;
}

/* ---->
void UNH_laden (void)
{
	
// wird jetzt auch dataillierter  gehandelt ......
	;
}
< ------ */

void UNZ_laden (void)
{
	;
}

/* ----->
void UNT_laden ( void )
{    
	// wird jetzt detaillierter gehandelt 
  danz_nachrichten ++ ;
}
< ----- */

// Prototyp fuer Handling jedes Segments


/* ----->
// Ausfuehrung allgemeiner Aktionen bei Auftreten eines Segment-Typs

PROCEDURE XXX_laden
        let k = k
END

// alternativ : jedes Gruppendatenelement des Segments einlesen
// und in entsprechend interpretieren und gebnenfalls entsprechende
// Aktivitaeten einleiten

PROCEDURE XXX_arbeiten
field xxx_wert  char(35)
field xxx_typ1  char(5)
field xxx_typ2  char(5)
field xxx_pic   char(10)

    if ((decount = 2 ) and ( gdcount = 1 ))
        let xxx_typ1 = gdpuffer
    elseif (( decount = 2 ) and ( gdcount = 2 ))
        let xxx_wert = gdpuffer
    elseif (( decount = 2 ) and ( gdcount = 3 ))
        let xxx_pic  = gdpuffer

        // wenn alle Werte gelesen sind, startet hier die Interpretation
        // und gegbnenfalls ne entsprechende Aktion

    end
END
< ---- */

static char ftx_wert [110];
static char ftx_typ1 [6] ;

void FTX_arbeiten(void )
{

	if ( ! inblock ) return ;
	if ( ! inbeleg ) return ;
// maximal 5 Felder zu maximal 99 Zeichen

	if ((decount == 2 ) && ( gdcount == 1 ))
	{
        sprintf ( ftx_typ1,"%s", gdpuffer ) ;
		ftx_wert[0] = '\0' ;	// initialisieren
	}
	if (( decount == 5 ) && ( gdcount == 1 ))
	{
		sprintf ( ftx_wert , "%s", gdpuffer ) ;
		if (((strlen( ftx_wert )) > 0 ) && (! strncmp ( ftx_typ1 , "ZZZ",3 )))
		{
			sprintf ( ftx_zzz_1 ,"%s" , ftx_wert ) ;
		}
	}
	if (( decount == 5 ) && ( gdcount == 2 ))
	{
		sprintf ( ftx_wert , "%s" , gdpuffer ) ;
		if (((strlen( ftx_wert )) > 0 ) && (! strncmp ( ftx_typ1 , "ZZZ",3 )))
		{
			sprintf ( ftx_zzz_2 ,"%s" , ftx_wert ) ;
		}
	}
	if (( decount == 5 ) && ( gdcount == 3 ))
	{
		sprintf ( ftx_wert , "%s" , gdpuffer ) ;
		if (((strlen( ftx_wert )) > 0 ) && (! strncmp ( ftx_typ1 , "ZZZ",3 )))
		{
			sprintf ( ftx_zzz_3 ,"%s" , ftx_wert ) ;
		}
	}
	if (( decount == 5 ) && ( gdcount == 4 ))
	{
		sprintf ( ftx_wert , "%s" , gdpuffer ) ;
		if (((strlen( ftx_wert )) > 0 ) && (! strncmp ( ftx_typ1 , "ZZZ",3 )))
		{
			sprintf ( ftx_zzz_4 ,"%s" , ftx_wert ) ;
		}
	}
	if (( decount == 5 ) && ( gdcount == 5 ))
	{
		sprintf ( ftx_wert , "%s" , gdpuffer ) ;
		if (((strlen( ftx_wert )) > 0 ) && (! strncmp ( ftx_typ1 , "ZZZ",3 )))
		{
			sprintf ( ftx_zzz_5 ,"%s" , ftx_wert ) ;
		}
	}
}


static  char dtm_datum[15] ;
static  char dtm_datumb[15] ;
static  char dtm_typ  [15] ;
static  char dtm_wert [25] ;
static  char dtm_pic  [15] ;


void DTM_arbeiten(void)
{
  char hc4j [6] ;
  char hc2m [4] ;
  char hc2t [4] ; 
  char hc10 [20] ;
 
	if ( ! inblock ) return ;
	if ( inbeleg != 2 ) return ;

// Nur im Kopf auswerten : MHD usw. kommen evtl. sapeter 


	if ((decount == 2 ) && ( gdcount == 1 ))
	{
        sprintf ( dtm_typ ,"%s",  gdpuffer ) ;
	}
	if (( decount == 2 ) && ( gdcount == 2 ))
	{
		sprintf ( dtm_wert ,"%s", gdpuffer ) ;
	}
	if (( decount == 2 ) && ( gdcount == 3 ))
	{
		sprintf ( dtm_pic ,"%s", gdpuffer ) ; 
		memset ( dtm_datum , '\0' , sizeof ( dtm_datum )) ;
		memset ( dtm_datumb , '\0' , sizeof ( dtm_datumb )) ;

		if (!strncmp( dtm_pic , "102",3 ) || !strncmp (dtm_pic, "203", 3 ) || !strncmp (dtm_pic, "718", 3 ) )
		{	// 102 : jjjjmmtt ; 203 : jjjjmmtthhmm ; 718 : jjjjmmttjjmmtt
			hc4j[0] = dtm_wert[0] ;
			hc4j[1] = dtm_wert[1] ;
			hc4j[2] = dtm_wert[2] ;
			hc4j[3] = dtm_wert[3] ;
			hc4j[4] = '\0' ;

			hc2m[0] = dtm_wert[4] ;
			hc2m[1] = dtm_wert[5] ;
			hc2m[2] = '\0' ;

			hc2t[0] = dtm_wert[6] ;
			hc2t[1] = dtm_wert[7] ;
			hc2t[2] = '\0' ;
			sprintf (hc10 , "%s.%s.%s", hc2t, hc2m, hc4j );
			sprintf ( dtm_datum, "%s", hc10 ) ;
		}

		if (!strncmp( dtm_pic , "718",3 ) )
		{	// 102 : jjjjmmtt ; 203 : jjjjmmtthhmm ; 718 : jjjjmmttjjmmtt
			hc4j[0] = dtm_wert[8] ;
			hc4j[1] = dtm_wert[9] ;
			hc4j[2] = dtm_wert[10] ;
			hc4j[3] = dtm_wert[11] ;
			hc4j[4] = '\0' ;

			hc2m[0] = dtm_wert[12] ;
			hc2m[1] = dtm_wert[13] ;
			hc2m[2] = '\0' ;

			hc2t[0] = dtm_wert[14] ;
			hc2t[1] = dtm_wert[15] ;
			hc2t[2] = '\0' ;
			sprintf (hc10 , "%s.%s.%s", hc2t, hc2m, hc4j );
			sprintf ( dtm_datumb, "%s", hc10 ) ;
		}

//			if (( (daufstat = 1) or (daufstat = 2) )
//                and (( strlen ( dtm_datum)) >  3))

//				if  (dtm_typ == "361" )	// MHD/Mindest-Restlaufzeit   
//				let posmhd = dtm_datum
// MHD als Haltbarkeitstage
//				if ( dtm_typ = "364" ) and ( dtm_pic = "804")  let poshbkz = dtm_wert
 
//				if ( dtm_typ == "194" )								// geb_dat = dtm_datum 
//				if ( dtm_typ == "347" )								// schl_dat = dtm_datum 
//				if ( ! strncmp ( dtm_typ ,"2",1 ) && dtm_typ[1] == '\0' )	// Gefordertes Lieferdatum 
		if ( ! strncmp ( dtm_typ ,"35",2 ) && dtm_typ[2] == '\0' )	// tats. Lieferdatum
		{
			sprintf ( belkopf.dtm_35_feld, "%s" , dtm_datum) ;
		}

		if ( ! strncmp ( dtm_typ ,"263",3 )  )	// Abrechungszeitraum
		{
			sprintf ( belkopf.dtm_35_feld, "%s" , dtm_datumb) ;	// behelfsweise das Bis-Datum
		}

		//Ich sende immmer zuerst 137 und dann 35 , so wird es dann wohl passen 

		if ( ! strncmp ( dtm_typ , "137" ,3 ))						//aufk.best_dat = dtm_datum ;  aufk.akv = dtm_datum
		{
			sprintf ( belkopf.dtm_137_feld, "%s" , dtm_datum) ;
			if ( narityp == 393 )
			{
				sprintf ( belkopf.dtm_35_feld, "%s" , dtm_datum) ;
			}
		}


// hier gibt es noch unendlich viele andere Datumstypen
	}
}

char imd_typ [10] ;

void IMD_arbeiten  (void) 
{

	char imd_wert[110] ;

	if ( !inposten ) return ;
	if ( inbeleg != 3 ) return ;
	if ( !inblock ) return ;

	if ((decount == 2 ) && ( gdcount == 1 ))
	{
        sprintf ( imd_typ ,"%s",  gdpuffer ) ;
		// A ,B , C , F 
	}
	if (( decount == 4 ) && ( gdcount == 1 ))
	{
		sprintf ( imd_wert ,"%s", gdpuffer ) ;
		if ( ! strncmp( imd_typ, "C" ,1 ) || ! strncmp ( imd_typ, "B" ,1))
		{
			if ( !strncmp ( imd_wert, "IN" , 2 ) && imd_wert[2] == '\0' )
			{
				belpos.imd_in_aktiv = 1 ;
			}
			if ( !strncmp ( imd_wert, "RF" , 2 ) && imd_wert[2] == '\0' )
			{
				belpos.imd_rf_aktiv = 1 ;
			}
		}
	}

	if (( decount == 4 ) && ( gdcount == 4 ))
	{
		if ( ! strncmp( imd_typ, "A" ,1 ) || ! strncmp ( imd_typ, "F" ,1))
		{	// "F" geht wohl noch etwas anders ........
			{
				sprintf ( imd_wert ,"%s", gdpuffer ) ;
				imd_wert[68] = ' ' ;
				imd_wert[69] = '\0';	// das imd_a_feld hat bei mir nur max. 70 zeichen 
				sprintf ( belpos.imd_a_feld , "%s" , imd_wert ) ;
			}
		}
	}

}

void UNB_arbeiten (void) 
{	

  char isend_iln [20] ; 
  char iempf_iln [20] ;

    if (decount == 3  &&  gdcount == 1 )
	{
		memcpy ( &belkopf,&belkopf_null, sizeof(struct BELKOPF));
		dtestmode = 0 ;	// Initialisierung, aber nur am Anfang und nur einmalig .....
		dta_referenz = 0 ;
		inbeleg = 0 ;
		inposten = 0 ;

//		sprintf ( ftx_zzz_1 , "" ) ;
//		sprintf ( ftx_zzz_2 , "" ) ;
//		sprintf ( ftx_zzz_3 , "" ) ;
//		sprintf ( ftx_zzz_4 , "" ) ;
//		sprintf ( ftx_zzz_5 , "" ) ;

//		pia2_gefunden = 0 ; 
		danz_nachrichten = 0 ;

		sprintf ( isend_iln ,"%s", gdpuffer ) ;
		sprintf ( belkopf.nad_su_feld ,"%s", gdpuffer ) ;
		
		if ( strncmp (isend_iln , dta.iln ,13 ) )
		{
			sprintf ( belkopf.bemerkung,"unguelt.Absender" ) ;
		}
		else
		{
			inblock = 1 ;
		}
	}
    if ( decount == 4 && gdcount == 1 )
	{
        sprintf ( iempf_iln ,"%s", gdpuffer ) ;
		sprintf ( belkopf.nad_by_feld ,"%s", gdpuffer ) ;
        if (strncmp ( iempf_iln , eigen_iln ,13 ) )
		{
			sprintf ( belkopf.bemerkung, "unguelt.Empfaenger" ) ;
			inblock = 0 ;
		}
		write_belkopf_a () ;	// alle Infos sind da 1. Satz .......
	}

	if ( decount == 6 && gdcount == 1 )
	{
		dta_referenz = atol ( gdpuffer) ;
	}

	if ( decount == 12 &&  gdcount == 1 )
        if ( gdpuffer[0] == '1' )
            if ( dtestignore == 1 )
                dtestmode = 0 ;
            else
                dtestmode = 1 ;
	if ( mogel ) inblock = 1 ;
}

void UNH_arbeiten (void ) 
{
	if ( !inblock ) return ;	// ganz falsch

	if (decount == 2 && gdcount == 1 )
	{

		sprintf ( cnari,"%s", gdpuffer ) ; 

		if ( inposten )
		{
			sprintf ( belpos.bemerkung, "Notschrieb" ) ;
			write_belpos_a () ;
			inposten = 0 ;
			inbeleg = 0 ;
		}
		if ( inbeleg == 2 )
		{
			sprintf ( belkopf.bemerkung, "Notschrieb" ) ;
			write_belkopf_a () ;
			inbeleg = 0 ;
		}
		if ( inbeleg == 4 )
		{
			sprintf ( belfuss.bemerkung, "Notschrieb" ) ;
			write_belfuss_a () ;
			inbeleg = 0 ;
		}
		inbeleg = 1 ;	// die naechste Nachricht beginnt ...

	}

}

void UNS_arbeiten ( void )
{
	if ( !inblock ) return ;
	if ( inbeleg < 2 ) return ;
    if ( decount == 2 && gdcount == 1 )
	{	// alles nur einmalig .....
		if ( inbeleg == 2 )
		{
			if ( narityp == 393 )
			{
				write_belkopf_a() ;
				inbeleg = 4 ;
			}
			else
			{
				sprintf ( belkopf.bemerkung ,"Notschrieb" ) ;
				inbeleg = 0 ;
				write_belkopf_a() ;
				inposten = 0 ;
				return ;
			}
		}
		if ( inposten )
		{	// das sollte der Normalzustand sein 
			write_belpos_a() ;
			inposten = 0 ;
		}
		inbeleg = 4 ;
		belfusstaxaktpo = 0 ;
		sprintf ( belfusstaxaktiv, "" ) ;
		memcpy ( &belfuss,&belfuss_null, sizeof(struct BELFUSS));
		sprintf ( belfuss.bgm_typ , "%s" , belkopf.bgm_typ ) ;
		belfuss.bgm_nr = belkopf.bgm_nr ;

  	}
}

void UNT_arbeiten (void ) 
{
	
	if ( ! inblock ) return ;
	if ( ! inbeleg ) return ;

	if (decount == 3 && gdcount == 1 )
	{
		if ( inposten )
		{
			if ( strcmp ( gdpuffer,cnari ))	// Es muss gleich sein ,sonst ERROR 
			{
				sprintf ( belpos.bemerkung, "Nari-Error" ) ;
			}
			else
			{
				sprintf ( belpos.bemerkung, "Notschrieb" ) ;
			}
			write_belpos_a () ;
			inposten = 0 ;
			inbeleg = 0 ;
		}
		if ( inbeleg == 2 )
		{

			if ( strcmp ( gdpuffer,cnari ))	// Es muss gleich sein ,sonst ERROR 
			{
				sprintf ( belkopf.bemerkung, "Nari-Error" ) ;
			}
			else
			{
				sprintf ( belkopf.bemerkung, "Notschrieb" ) ;
			}
			write_belkopf_a () ;
			inbeleg =  0;
		}
		if ( inbeleg == 4 )	// das ist regulaer evtl .steht ja nix drin z.B. bei Orders .....
		{
			if ( strcmp ( gdpuffer,cnari ))	// Es muss gleich sein ,sonst ERROR 
			{
				sprintf ( belfuss.bemerkung, "Nari-Error" ) ;
			}
			write_belfuss_a () ;
			inbeleg = 0 ;
		}
	}
}

void BGM_arbeiten (void ) 
{
	if ( ! inblock ) return ;

	// if ( ! inbeleg ) return ; // eigentlich ein problem ,aber das erlauben wir : loch um unh-segment
	
	if (decount == 2 && gdcount == 1 )
	{
		inbeleg = 2 ;
		memcpy ( &belkopf,&belkopf_null, sizeof(struct BELKOPF));
		sprintf ( belkopf.bgm_typ,"%s", gdpuffer );
		narityp = atoi ( gdpuffer ) ;

//		if ( strncmp ( gdpuffer,"380", 3 ))	// Rechnungen,Gutschriften und Rechungsliste auswerten, Rest ignorieren 
		if (( narityp != 380 ) && ( narityp != 381 ) && ( narityp != 393 ))
		{
			inbeleg = 0 ;
			sprintf ( belkopf.bemerkung , "NARI-TYP:%s",gdpuffer ) ;
//			write_belkopf_a () ;
		}
	}
	if ( decount == 3  && gdcount == 1 )
	{
		belkopf.bgm_nr = atol ( gdpuffer ) ;
		if  ( ! inbeleg )
		{
			write_belkopf_a() ;	// getrixter Notschrieb Notschrieb
		}
	}
}

// ACHTUNG z.Z.: werden hier keine Gebinde-Infos gehandelt
// gegbnenfalls fuer abweichende Bestell / Liefereinheit abwickeln
// : hier steht gegebnenfalls immer die Gebinde-ILN d.h. die Bestell-Einheit

// Achtung : fuer Frankengut-Ablauf steht in lin die BP-Nummer anstelle einer ean
// und in PIA eine laufende Nummer , welche wieder zureuck gebeamt werden soll 
// FraGu ist z.Z. nicht durchrealisiert

char lin_eani [25] ;
char lin_pos [25] ;


void LIN_arbeiten ( void )
{
	if ( !inblock ) return ;
	if ( inbeleg < 2 ) return ;
	if ( inbeleg == 2 )
	{
		write_belkopf_a() ;
		inbeleg = 3 ;
	}
    if ( decount == 2 && gdcount == 1 )
	{
		// init je Position nur einmalig
		if ( inposten == 1 )
		{
			write_belpos_a() ;
			memcpy ( &belpos,&belpos_null, sizeof(struct BELPOS));
			belpos.bgm_nr = belkopf.bgm_nr ;
			sprintf ( belpos.bgm_typ ,"%s", belkopf.bgm_typ ) ;
		}
		inposten = 1 ;
        sprintf ( lin_pos , "%s" , gdpuffer ) ;
//        let aufp.posi_ext = lin_pos -> interessant bei referenzierten Orders ( Frankengut ) 
	}
    if ( decount == 4 && gdcount == 1 )
	{
        sprintf ( lin_ean ,"%s" , gdpuffer ) ;
		sprintf (belpos.lin_feld, "%s", lin_ean ) ;
		belpos.bgm_nr = belkopf.bgm_nr ;
		sprintf ( belpos.bgm_typ ,"%s", belkopf.bgm_typ ) ;
		// es gibt durchaus Nachrichtentypen, wo da schon nichts mehr kommt ..... 
 	}
}

void MEA_arbeiten (void)
{
	if ( ! inblock ) return ;
	if ( ! inposten ) return ;
	if ( inbeleg != 3 ) return ;

}

// Struktur der Fuss-MOAS

// Netto       + Rabatt  = EchtesNetto + MwSt auf EchetesNetto = Brutto
// MOA 79      + MOA 131 = MOA 125    + MOA 124               = MOA 77 

// MOA 79 : Summe MOA203

// MOA 131 : i.a. negativ und identisch mit 0 - MOA 8 aus Kopf
// MOA 125 : Summe aus MOA 79 und MOA 131 : Basis der MwSt-Berechnung 
// MOA 77 : Zahlbetrag 

// MOA 79, MOA 124 , MOA 125 und MOA 131 nochmals getrennt je MWST-Satz ausgewiesen 
 

// bei bgm+393 : MOA 86 : Gesamtwert der Nachricht ( like moa77 )
// bei bgm+393 : MOA 9 : Zahlbetrag ( evtl. abweichend wegen Skonto, Rabatten usw. auf die Liste ..... ?? )

// bei bgm+393 : getrennter Ausweis 86 / 124 / 125 je MwSt


static char moa_typ [8] ;
void MOA_arbeiten ( void) 
{
	if ( ! inblock ) return ;

	if ( decount == 2 && gdcount == 1 )
	{
		sprintf ( moa_typ ,"%s", gdpuffer ) ;
	}

	if ( inbeleg == 2 )	// z.B. Abschlaege im Kopf 
	{
	}
	if ( inposten )
	{	// Positions-MOA : 203(AAB) bzw. 66(AAA)
	
		// Brutto- bzw. Netto-Kalkulation 

		if ( decount == 2 && gdcount == 2)
		{	
			if ( ! strncmp ( moa_typ ,"203",3 ) || ! strncmp( moa_typ, "66" , 2 ))
			{
				belpos.moa_203_wert = atof ( gdpuffer ) ;
			}
		}
	}	// ende inposten
	if ( inbeleg == 4 )	// allerlei Fuss-Infos
	{
		if ( decount == 2 && gdcount == 2)
		{	
			if ( ! strncmp ( moa_typ ,"77",2 ) )
			{
				belfuss.moa_77_wert = atof ( gdpuffer ) ;
			}
// Hier die Alternative fuer bgm 393
			if ( ! strncmp ( moa_typ ,"86",2 ) )
			{
				switch ( belfusstaxaktpo )
				{
				case 1 :
					belfuss.moa_1_79_wert = atof ( gdpuffer ) ;
				  break ;
				case 2 :
					belfuss.moa_2_79_wert = atof ( gdpuffer ) ;
				  break ;
				default :
				    belfuss.moa_79_wert = atof ( gdpuffer ) ;
					belfuss.moa_77_wert = atof ( gdpuffer ) ;

				  break ;
				}

			}

			if ( ! strncmp ( moa_typ ,"79",2 ) )
			{
				switch ( belfusstaxaktpo )
				{
				case 1 :
					belfuss.moa_1_79_wert = atof ( gdpuffer ) ;
				  break ;
				case 2 :
					belfuss.moa_2_79_wert = atof ( gdpuffer ) ;
				  break ;
				default :
				    belfuss.moa_79_wert = atof ( gdpuffer ) ;
				  break ;
				}
			}

			if ( ! strncmp ( moa_typ ,"125",3 ) )
			{
				switch ( belfusstaxaktpo )
				{
				case 1 :
					belfuss.moa_1_125_wert = atof ( gdpuffer ) ;
				  break ;
				case 2 :
					belfuss.moa_2_125_wert = atof ( gdpuffer ) ;
				  break ;
				default :
					belfuss.moa_125_wert = atof ( gdpuffer ) ;
				  break ;
				}
			}

			if ( ! strncmp ( moa_typ ,"124",3 ) )
			{
				switch ( belfusstaxaktpo )
				{
				case 1 :
					belfuss.moa_1_124_wert = atof ( gdpuffer ) ;
				  break ;
				case 2 :
					belfuss.moa_2_124_wert = atof ( gdpuffer ) ;
				  break ;
				default :
					belfuss.moa_124_wert = atof ( gdpuffer ) ;
				  break ;
				}
			}

			if ( ! strncmp ( moa_typ ,"131",3 ) )
			{
				switch ( belfusstaxaktpo )
				{
				case 1 :
					belfuss.moa_1_131_wert = atof ( gdpuffer ) ;
				  break ;
				case 2 :
					belfuss.moa_2_131_wert = atof ( gdpuffer ) ;
				  break ;
				default :
					belfuss.moa_131_wert = atof ( gdpuffer ) ;
				  break ;
				}
			}
		}	// 2/2 
	}	// inbeleg == 4
}

char  nad_typ [7] ;
char dpiln [20] ;

void NAD_arbeiten (void ) 
{
	if ( inbeleg != 2 )	return ;	// nur im Kopf benoetigt

// Achtung hier koennte spaeter mal eine weitere rekursion wegen Gutschriften usw. reinkommen ......

	if ( decount == 2 && gdcount == 1 )
	{
        sprintf ( nad_typ ,"%s", gdpuffer ) ;
	}
    if ( decount == 3 && gdcount == 1 )
	{
        sprintf ( dpiln ,"%s" , gdpuffer ) ;
		if ( ! strncmp ( nad_typ , "SU" , 2 ))
		{
			sprintf ( belkopf.nad_su_feld, "%s", dpiln ) ;
		}
		if ( ! strncmp ( nad_typ , "DP" , 2 ))
		{
			sprintf ( belkopf.nad_dp_feld, "%s", dpiln ) ;
		}
        sprintf ( dpiln ,"%s" , gdpuffer ) ;
		if ( ! strncmp ( nad_typ , "BY" , 2 ))
		{
			sprintf ( belkopf.nad_by_feld, "%s", dpiln ) ;
		}

		if ( ! strncmp ( nad_typ , "DP" , 2 ) && daufstat == 2 )
		{
			/* --->
			das gehoert in die Abarbeitungsphase 
			close cursor sucheanschrift
            erase cursor suchanschrift
            prepare cursor suchanschrift from sql
                select #kun.% from kun where kun.mdn = $mdn.mdn
                   and kun.iln = $dpiln
            end
            fetch cursor suchanschrift
            if not sqlstatus
// 150202 Kampf gegen duplikates A
                fetch cursor suchanschrift
                if not sqlstatus
                    if testphase
                         perform protokoll ( 0 , "Liefer-ILN mehrfach : " # dpiln )
                    else
                         let abbruch = 1
                    end
                else
// 150202 Kampf gegen duplikates E 
                    let aufk.kun = kun.kun
                    let aufk.adr = kun.adr2
                    let daufstat = 3
                    if not testphase
                        perform aufnummern ( 2 )
                        perform ateil_smt_setz   // 281002
                    else
                        perform aufnummern ( 1 )
                    end
                end
            else
                if testphase
                    perform protokoll ( 0 , "Liefer-ILN fehlt : " # dpiln )
                else
                    let abbruch = 1
                end
            end
< -------------- */
		}
	}

}

static  char pia_typ1 [12];
static  char pia_typ2 [12];
static  char pia_typ3 [12];
static  char pia_puffer2 [25] ;
static  char pia_puffer3 [25] ;

void PIA_arbeiten ( void ) 
{
	if ( ! inposten ) return ;
	if ( ! inblock ) return ;
	if ( inbeleg != 3 ) return ;

    if ( decount == 2 && gdcount == 1 )
	{
		// "1" oder "5" erwartet 
        sprintf ( pia_typ1 ,"%s", gdpuffer ) ;
        memset ( pia_puffer2 , '\0' , sizeof ( pia_puffer2 )) ;
        memset ( pia_puffer3 , '\0' , sizeof ( pia_puffer3 )) ;
        memset ( pia_typ2 , '\0' , sizeof ( pia_typ2 )) ;
        memset ( pia_typ3 , '\0' , sizeof ( pia_typ3 )) ;
	}
    if ( decount == 3 && gdcount == 1 )
	{
        sprintf ( pia_puffer2 ,"%s", gdpuffer ) ;
	}
	if ( decount == 3 && gdcount == 2 )
	{
        sprintf ( pia_typ2 ,"%s", gdpuffer ) ;
		// Typ zu pia_puffer2 ( z.B. IN, SA oder BP ..... )

		if ( ! strncmp( pia_typ1, "1" , 1 ) || ! strncmp ( pia_typ1 , "5" , 1 ))
		{
			if ( ! strncmp ( pia_typ2 , "IN" ,2 ) || !strncmp ( pia_typ2, "BP" ,2 ))
			{
				sprintf ( belpos.pia_bp_feld, "%s" , pia_puffer2 ) ;	// fit-Artikel-Nummer
			}
			if ( ! strncmp ( pia_typ2 , "SA" ,2 )) 
			{
				sprintf ( belpos.pia_sa_feld, "%s" , pia_puffer2 ) ;	// lief_best-Artikel-Nummer
			}
		}
	}

    if ( decount == 4 && gdcount == 1 )
	{
        sprintf ( pia_puffer3 ,"%s" , gdpuffer ) ;
	}
    if ( decount == 4 && gdcount == 2 )
	{
        sprintf (pia_typ3 ,"%s", gdpuffer );
		// Typ zu pia_puffer3 ( z.B. IN, SA oder BP ..... )

		if ( ! strncmp( pia_typ1, "1" , 1 ) || ! strncmp ( pia_typ1 , "5" , 1 ))
		{
			if ( ! strncmp ( pia_typ3 , "IN" ,2 ) || !strncmp ( pia_typ3, "BP" ,2 ))
			{
				sprintf ( belpos.pia_bp_feld, "%s" , pia_puffer3 ) ;	// fit-Artikel-Nummer
			}
			if ( ! strncmp ( pia_typ3 , "SA" ,2 ) )
			{
				sprintf ( belpos.pia_sa_feld, "%s" , pia_puffer3 ) ;	// lief_best-Artikel-Nummer
			}
		}
	}
 }


static char cux_typ1 [8] ;
static char cux_typ2 [8] ;

void CUX_arbeiten (void )
{
	return ;	// z.Z nur dummy ....
/* ---->
    if ( decount == 2 && gdcount == 1)
	{
        sprintf ( cux_typ1 ,"%s", gdpuffer ) ;
        memset ( cux_typ2 , '\0', sizeof ( cux_typ2 )) ;
	}
    if ( decount == 2 && gdcount == 2 )
	{
        sprintf ( cux_typ2 ,"%s", gdpuffer ) ;
< ----- */

/* ---->
        if (( cux_typ1 = "2" ) and ( cux_typ2 = "DEM" ))
            let daufwaehr = 1
        elseif ((cux_typ1 = "2" ) and ( cux_typ2 = "EUR" ))
            let daufwaehr = 2
        end
< ----- */

// 		}        // elseif verschiedene Felder
}

static char qty_typ [8] ;

void QTY_arbeiten ( void ) 
{

	if ( !inblock ) return ;
	if ( inbeleg != 3 ) return ;
	if ( ! inposten ) return ;

	if ( decount == 2 && gdcount == 1 )
	{
		sprintf ( qty_typ ,"%s", gdpuffer ) ;
	}
    if ( decount == 2 && gdcount == 2)
	{												// 21 : bestellmenge usw. 
        if ( ! strncmp ( qty_typ ,"47",2 ))			// berechnete Menge , anderes wird fuer Rechung ignoriert  ... 
		{
			belpos.qty_47_wert = atof ( gdpuffer ) ;
		}
	}
    if ( decount == 2 && gdcount == 3 )
	{
        if ( ! strncmp ( qty_typ ,"47",2 ))
		{
			sprintf (  belpos.qty_47_einh , "%s" , gdpuffer ) ;
		}
	}
}

static char pri_typ1 [7] ;
static char pri_typ2 [7] ;
static char pri_fakt [7] ;

void PRI_arbeiten (void )
{

	if ( ! inposten ) return ;
	if ( ! inblock ) return ;
	if ( inbeleg != 3 ) return ;
    if (decount == 2 && gdcount == 1 )
	{	// AAA oder AAB requestet
        sprintf ( pri_typ1 ,"%s" ,gdpuffer ) ;
	}
    if ( decount == 2 && gdcount == 2 )
	{
		if ( ! strncmp (pri_typ1 , "AAA" ,3 ) || ! strncmp (pri_typ1 , "AAB" ,3 ))
		{
			belpos.pri_aab_wert = atof ( gdpuffer ) ;
		}
 	}

	if ( decount == 2  && gdcount == 5 )
	{
        sprintf ( pri_fakt ,"%s" , gdpuffer ) ;	// der Faktor sollte hier immer == "1" sein ? !

	}
	if ( decount == 2  && gdcount == 6 )
	{
        if ( ! strncmp ( pri_typ1 ,"AAA" ,3 ) || ! strncmp ( pri_typ1 , "AAB" , 3 ))
		{
			sprintf ( belpos.pri_aab_einh, "%s" ,gdpuffer ) ;
		}
	}
}

static char rff_typ [10] ;

void RFF_arbeiten ( void )
{
	char rff_puffer[25] ;

	if ( ! inblock ) return ;
	if ( ! inbeleg ) return ;

    if (decount == 2 && gdcount == 1 )
	{
        sprintf ( rff_typ ,"%s" ,gdpuffer ) ;
	}
    if ( decount == 2 && gdcount == 2 )
	{
        sprintf ( rff_puffer ,"%s" ,  gdpuffer ) ;

		if ( inbeleg == 2 )
		{
// Achtung hier sind Weiterungen bei Bearbeitung von Gutschriften denkbar 
			if  ( ! strncmp ( rff_typ ,"ON", 2 ) && rff_typ[2] == '\0' )
			{
				sprintf ( belkopf.rff_on_feld, "%s" , rff_puffer ) ;
			}
			if  ( ! strncmp ( rff_typ ,"ABO", 2 ) && rff_typ[2] == '\0' )
			{
				sprintf ( belkopf.rff_abo_feld, "%s" , rff_puffer ) ;
			}
		}
	}
}
static char tax_typ1[10] ;
static char tax_typ2[10] ;
static char tax_wert[10] ;
void TAX_arbeiten ( void )
{
	if ( ! inblock ) return ;
	if ( ! inbeleg ) return ;

    if (decount == 2 && gdcount == 1 )
	{
        sprintf ( tax_typ1 ,"%s" ,gdpuffer ) ;	// immer die "7"
		sprintf ( tax_wert ,"" ) ;				// erst mal init
	}
    if ( decount == 3 && gdcount == 1 )
	{
        sprintf ( tax_typ2 ,"%s" ,  gdpuffer ) ;	// immer "VAT"
	}
	if ( decount == 6 && gdcount == 4 )
	{
        sprintf ( tax_wert ,"%s" ,  gdpuffer ) ;	// hier stehen Prozente
		if ( inbeleg == 2 )
		{
			sprintf ( belkopf.tax_feld , "%s", tax_wert ) ;
		}
		if ( inbeleg == 3 && inposten )
		{
			sprintf ( belpos.tax_feld , "%s", tax_wert ) ;
		}
		if ( inbeleg == 4 )	// Fuss-Umschalter
		{
			sprintf ( belfusstaxaktiv , "%s" , tax_wert ) ;
			if ( ! belfuss.tax_1_aktiv )
			{	// erster Eintrag 
				belfuss.tax_1_aktiv = 1 ;
				sprintf ( belfuss.tax_f_1 , "%s" , tax_wert ) ;
				belfusstaxaktpo = 1 ;
			}
			else
			{	// aktiv aber doppelt ?
				if ( ! strcmp ( belfusstaxaktiv , belfuss.tax_f_1 ))
				{
					belfusstaxaktpo = 1 ;
				}
				else	// Gnadenlos auf 2. Eintrag, das kann natuerlich schiefgehen 
				{
					belfuss.tax_2_aktiv = 1 ;
					sprintf ( belfuss.tax_f_2 , "%s" , tax_wert ) ;
					belfusstaxaktpo = 2 ;
				}
			}
		}	// inbeleg 2,3,4
	}		// 6/5
	if ( decount == 7 && gdcount == 1 )
	{
        sprintf ( tax_wert ,"%s" ,  gdpuffer ) ;	// hier steht das "E" fuer empty 
		if ( inbeleg == 2 )
		{
			sprintf ( belkopf.tax_feld , "%s", tax_wert ) ;
		}
		if ( inbeleg == 3 && inposten )
		{
			sprintf ( belpos.tax_feld , "%s", tax_wert ) ;
		}

		if ( inbeleg == 4 )	// Fuss-Umschalter
		{
			sprintf ( belfusstaxaktiv , "%s" , tax_wert ) ;
			if ( ! belfuss.tax_1_aktiv )
			{	// erster Eintrag 
				belfuss.tax_1_aktiv = 1 ;
				sprintf ( belfuss.tax_f_1 , "%s" , tax_wert ) ;
				belfusstaxaktpo = 1 ;
			}
			else
			{	// aktiv aber doppelt ?
				if ( ! strcmp ( belfusstaxaktiv , belfuss.tax_f_1 ))
				{
					belfusstaxaktpo = 1 ;
				}
				else	// Gnadenlos auf 2. Eintrag, das kann natuerlich schiefgehen 
				{
					belfuss.tax_2_aktiv = 1 ;
					sprintf ( belfuss.tax_f_2 , "%s" , tax_wert ) ;
					belfusstaxaktpo = 2 ;
				}
			}

		}	// inbeleg 2,3,4
	}	// 7/1
}

void schreiben (void )
{
	if ( dsegguelt == 3 )
	{

		if ( ! strncmp ( dsegname3 , "UNH" , 3 ))
		{
                UNH_arbeiten () ;    // Nachrichten-Rahmen-Anfang
                return ;
		}

		if ( ! strncmp ( dsegname3 , "UNT" , 3 ))
		{
                UNT_arbeiten () ;    // NAchrichten-Rahmen Ende
                return ;
		}


		if ( ! strncmp ( dsegname3 , "UNS" , 3 ))
		{
                UNS_arbeiten () ;    // Listenende-Marker
                return ;
		}

		if ( ! strncmp ( dsegname3 , "BGM" , 3 ))
		{
                BGM_arbeiten () ;    // Beleg-Nummer
                return ;
		}
		if ( ! strncmp ( dsegname3 , "DTM" , 3 ))
		{
                 DTM_arbeiten () ;  // Auftrags- u. Lieferdatum
                 return ;
		}
		if ( ! strncmp ( dsegname3 , "IMD" , 3 ))
		{
                IMD_arbeiten () ;    // z.Z. dummy
                return ;
		}
		if ( ! strncmp ( dsegname3 , "LIN" , 3 ))
		{
                LIN_arbeiten ();    // Haupt-Schluessel
                return ;
		}
		if ( ! strncmp ( dsegname3 , "MEA" , 3 ))
		{
                MEA_arbeiten () ;						// 
                return ;
		}
		if ( ! strncmp ( dsegname3 , "MOA"	, 3 ))
		{
			MOA_arbeiten () ;
            return ;
		}
		if ( ! strncmp ( dsegname3 , "NAD" , 3 ))
		{
                NAD_arbeiten () ;						// Anschriften
                return ;
		}
		if ( ! strncmp ( dsegname3 , "PIA" , 3 ))
		{					// interne-Art-Nr (Lief/kun) -> Schluessel
                PIA_arbeiten () ;
                return ;
		}
		if ( ! strncmp ( dsegname3 , "RFF" , 3 ))
		{
			RFF_arbeiten () ;						// Verweise halt .....
            return ;
		}
		if ( ! strncmp ( dsegname3 , "TAX" , 3 ))
		{
			TAX_arbeiten () ;		// MWST-Steuerei 
            return ;
		}
		if ( ! strncmp ( dsegname3 , "QTY" , 3 ))
		{
			QTY_arbeiten () ;		// Menge
            return ;
		}
		if ( ! strncmp ( dsegname3 , "CUX" , 3 ))
		{					// Referenz-Waehrung
			CUX_arbeiten () ;
            return ;
		}

		if ( ! strncmp ( dsegname3 , "FTX" , 3 ))
		{
			FTX_arbeiten () ;		// Pos-Texte 
            return ;
		}
		if ( ! strncmp ( dsegname3 , "PRI" , 3 ))
		{                    // Preis
			PRI_arbeiten () ;
            return ;
		}

		if ( ! strncmp ( dsegname3 , "UNB" , 3 ))
		{  
            UNB_arbeiten () ;	// Adressier-Test
			return ;
		}

//           case "UNH"
//           case "UNA"
//           case "UNT"

                return ;

	}     // dsegguelt3
    if ( dsegguelt == 2 )
	{
		return ;

/* -----> gibbet gar nicht ...
          switch dsegname2
            case "XX"
            case "YY"
            otherwise
                return

        end     // switch dsegname2
< ----- */
	}         // dsegguelt = 2
}

// ######################################################################
// #### PROC.:  lesegd
// #### liest naechstes Datenelement  bis zum naechsten Gruppendaten-
// oder Datenelementtrenner
// #### in den gdpuffer
// ######################################################################

int lesegd(void) 
{
char hilfchar [4] ;
	sprintf ( hilfchar , "X" ) ;
    gdwpoint = 0 ;
    memset ( gdpuffer, '\0', sizeof ( gdpuffer )) ;
    if ( derpoint < depufflen )
	{
		hilfchar[0]  = depuffer[derpoint] ;

        while (hilfchar[0] != trenn_gd[0] ||maskel )
		{
			if ( maskel )
			{ 
				maskel = 0 ;
			}
			else
			{
				if ( hilfchar[0] == trenn_ma[0]) maskel = 1 ;
			}

            gdpuffer[gdwpoint] = hilfchar[0] ;
            gdwpoint ++ ;
            derpoint ++ ;
            if ( derpoint >= depufflen )
			{
				maskel = 0 ;
                gdcount ++ ;
                derpoint ++ ;
                schreiben() ;
                return 0 ;
			}
            hilfchar[0]  = depuffer[derpoint] ;
		}
		if ( hilfchar[0] == trenn_gd[0] )
		{
			// maskel gibbet hier niemals 
			maskel = 0 ;
            gdcount ++ ;
            derpoint ++ ;
            schreiben () ;
            return 0 ;
		}
	}                 // pufferende
    return  -1 ;
}

void mlesegd ( void ) 
{
    memset ( gdpuffer, '\0', sizeof ( gdpuffer )) ;
    gdcount   = 0 ;
    depufflen = (int) strlen ( depuffer ) ;
    derpoint = 0 ;
	while ( ! lesegd ( ) ) ;
}


// ######################################################################
// #### PROC.:  lesede
// #### liest naechstes Datenelement  bis zum naechsten Datenelementtrenner
// #### in den depuffer
// ######################################################################

int lesede ( void ) 
{
char hilfchar [3] ;

    sprintf (hilfchar ,"X" ) ;
    dewpoint = 0 ;
    derpoint = 0 ;
    memset ( depuffer, '\0', sizeof ( depuffer )) ;
    if ( dsegrpoint < dsegpufflen )
	{
		hilfchar[0]  = dsegpuffer[dsegrpoint] ;
		while ((hilfchar[0] != trenn_de[0] ) ||maskel )
		{
			if ( maskel )
			{ 
				maskel = 0 ;
			}
			else
			{
				if ( hilfchar[0] == trenn_ma[0]) maskel = 1 ;
			}

            depuffer[dewpoint] = hilfchar[0] ;
            dewpoint ++ ;
            dsegrpoint ++ ;
            if ( dsegrpoint >= dsegpufflen )
			{
				maskel = 0 ;
				decount ++ ;
                mlesegd () ; 
                return  0 ;
			}
            hilfchar[0]  = dsegpuffer[dsegrpoint] ;
		}
        if ( hilfchar[0] == trenn_de[0] )
		{
			// maskel gibbet hier niemals 
			maskel = 0 ;
            decount ++ ;
            dsegrpoint ++ ;
            mlesegd () ;
            return 0 ;
		}
	}                 // pufferende
    return ( -1 ) ;
}

void mlesede ( void )
{
	depufflen  = 0 ;
    memset ( depuffer, '\0' , sizeof ( depuffer )) ;
    memset ( gdpuffer, '\0' , sizeof ( gdpuffer )) ; 
    gdcount    = 0 ;
    decount    = 0 ;
    dsegrpoint = dsegguelt	; // + 1	Rosi ging erst bei 1 los .... 
//    k = 0 ;
    dsegpufflen = (int) strlen ( dsegpuffer ) ;
    while ( ! lesede () );
}


int auswertesegment ( void ) 
{
    memset ( dsegname3, '\0' , sizeof(dsegname3 )) ;
    memset ( dsegname2, '\0', sizeof (dsegname2 )) ;
    dsegguelt = 0 ;
    gdcount  = 0 ;
    decount  = 0 ;

    dsegname3[0] = dsegpuffer[0] ;
    dsegname2[0] = dsegpuffer[0] ;
    dsegname3[1] = dsegpuffer[1] ;
    dsegname2[1] = dsegpuffer[1] ;
    dsegname3[2] = dsegpuffer[2] ;
    if ( ! strncmp ( dsegname3 ,"UNA",3 ))
	{
        UNA_laden() ;
        memset ( dsegpuffer,'\0', sizeof(dsegpuffer)) ;
        dsegwpoint = 0 ;
        return dabbruch ;
	}

/* ----> detaillierter im modus "schreiben" behandeln 
    if ( ! strncmp ( dsegname3 ,"UNH",3 ))
	{
        UNH_laden() ;
        memset ( dsegpuffer,'\0', sizeof(dsegpuffer)) ;
        dsegwpoint = 0 ;
        return dabbruch ;
	}
    if ( ! strncmp ( dsegname3 ,"UNT",3 ))
	{
        UNT_laden() ;
        memset ( dsegpuffer,'\0', sizeof(dsegpuffer)) ;
        dsegwpoint = 0 ;
        return dabbruch ;
	}
< ----- */
    if ( ! strncmp ( dsegname3 ,"UNZ",3 ))
	{
		UNZ_laden () ;
        memset ( dsegpuffer,'\0', sizeof(dsegpuffer)) ;
        dsegwpoint = 0 ;
        return dabbruch ;
	}

     if (( ! strncmp ( dsegname3, "UNB" , 3 ))
       ||( ! strncmp ( dsegname3, "UNH" , 3 ))
       ||( ! strncmp ( dsegname3, "UNT" , 3 ))
       ||( ! strncmp ( dsegname3, "BGM" , 3 ))
       ||( ! strncmp ( dsegname3, "MOA" , 3 ))
       ||( ! strncmp ( dsegname3, "PIA" , 3 ))
       ||( ! strncmp ( dsegname3, "NAD" , 3 ))
       ||( ! strncmp ( dsegname3, "LIN" , 3 ))
       ||( ! strncmp ( dsegname3, "RFF" , 3 ))
       ||( ! strncmp ( dsegname3, "DTM" , 3 ))
       ||( ! strncmp ( dsegname3, "TAX" , 3 ))
       ||( ! strncmp ( dsegname3, "MEA" , 3 ))
       ||( ! strncmp ( dsegname3, "IMD" , 3 ))
       ||( ! strncmp ( dsegname3, "CNT" , 3 ))
       ||( ! strncmp ( dsegname3, "UNS" , 3 ))
       ||( ! strncmp ( dsegname3, "QTY" , 3 ))
       ||( ! strncmp ( dsegname3, "CUX" , 3 ))
       ||( ! strncmp ( dsegname3, "PRI" , 3 ))
       ||( ! strncmp ( dsegname3, "ALI" , 3 ))
       ||( ! strncmp ( dsegname3, "ALC" , 3 ))
       ||( ! strncmp ( dsegname3, "FTX" , 3 ))
       ||( ! strncmp ( dsegname3, "CTA" , 3 ))
	   ||( ! strncmp ( dsegname3, "COM" , 3 ))
	   ||( ! strncmp ( dsegname3, "PCD" , 3 ))

	  )
	 {
		dsegguelt = 3 ;
        mlesede () ;
        memset ( dsegpuffer , '\0' , sizeof ( dsegpuffer )) ;
        dsegwpoint = 0 ;
        return dabbruch ;
	 }
	 else
	 {
		MessageBox(NULL, "Unbekannter Identifier", dsegname3 , MB_OK|MB_ICONSTOP);
		dabbruch = 1 ;
        return -1 ;
  	 }

	memset ( dsegpuffer , '\0' , sizeof ( dsegpuffer )) ;
    dsegwpoint = 0 ;
    return dabbruch ;
}


int einlesen ( CrecinvoiceDlg * Cparent, short dmdn , char * quelldatei , int testignnore )
{

	inblock = inbeleg = inposten = 0 ;

    trennz_laden () ;

	sprintf ( eigen_iln , "%s", mdn.iln ) ;

    iprpoint   = 0 ;
    dsegwpoint = 0 ;
    dsegrpoint = 0 ;
    dewpoint   = 0 ;
    derpoint   = 0 ;
    gdwpoint   = 0 ;

    memset ( ippuffer,'\0', sizeof(ippuffer))  ;
    ippufflen  = 0 ;

    memset ( dsegpuffer,'\0', sizeof(dsegpuffer)) ;
    dsegpufflen = 0 ; 

    memset ( depuffer,'\0', sizeof(depuffer)) ;
    depufflen  = 0 ;

    memset( gdpuffer,'\0', sizeof(gdpuffer)) ;

    gdcount   = 0 ;
    decount   = 0 ;

    while ( ! lesesegment (Cparent) )
	{
		k = auswertesegment () ;
        if ( k ) return  k ;
	}
	return  0 ; 
}

void ArraysInit () 
{
	// Anfansginit der Arrays
	belkopfakt = belkopfmax = 0 ;
	belkopfdim = 10 ;
	belposakt = belposmax = 0 ;
	belposdim = 50 ;
	belfussakt = belfussmax = 0 ;
	belfussdim = 10 ;
	belkopf_a = NULL ;
	belkopf_a = (struct BELKOPF *)calloc(belkopfdim, sizeof(struct BELKOPF ));
	belpos_a = NULL ;
	belpos_a = (struct BELPOS *)calloc(belposdim, sizeof(struct BELPOS ));
	belfuss_a = NULL ;
	belfuss_a = (struct BELFUSS *)calloc(belfussdim, sizeof(struct BELFUSS ));
}

void write_belkopf_a(void)
{
	// memcpy ( &belkopf,&belkopf_null, sizeof(struct BELKOPF));
	if ( belkopfmax >= belkopfdim )	// neu allozieren
	{
	   belkopfdim += 10 ;
	   belkopf_a = ( struct BELKOPF *)realloc 
					( (void *) belkopf_a,
					      belkopfdim * sizeof(struct BELKOPF));
		if ( belkopf_a == NULL )
		{
// knallt dann sowieso :					int i = 0 ;	// fatal-Error 
		}

	} ;
	memcpy ( belkopf_a + belkopfmax , &belkopf, sizeof ( struct BELKOPF )) ;
	belkopfmax ++ ;
}

int read_belkopf_a(void)
{
	if ( belkopfakt < belkopfmax )
	{
		memcpy ( &belkopf , belkopf_a + belkopfakt , sizeof ( struct BELKOPF )) ;
		belkopfakt ++ ;
		return 0 ;
	}
	return 100 ;
}

void write_belpos_a(void)
{

// memcpy ( &belpos,&belpos_null, sizeof(struct BELPOS));
	if ( belposmax >= belposdim )	// neu allozieren
	{
	   belposdim += 50 ;
	   belpos_a = ( struct BELPOS *)realloc 
					( (void *) belpos_a,
					      belposdim * sizeof(struct BELPOS));
		if ( belpos_a == NULL )
		{
// knallt dann sowieso :					int i = 0 ;	// fatal-Error 
		}

	} ;
	memcpy ( belpos_a + belposmax , &belpos, sizeof ( struct BELPOS )) ;
	belposmax ++ ;
}

int read_belpos_a(void)
{
	if ( belposakt < belposmax )
	{
		memcpy ( &belpos , belpos_a + belposakt , sizeof ( struct BELPOS )) ;
		belposakt ++ ;
		return 0 ;
	}
	return 100 ;
}

void write_belfuss_a(void)
{

// memcpy ( &belfuss,&belfuss_null, sizeof(struct BELFUSS));
	if ( belfussmax >= belfussdim )	// neu allozieren
	{
	   belfussdim += 10 ;
	   belfuss_a = ( struct BELFUSS *)realloc 
					( (void *) belfuss_a,
					      belfussdim * sizeof(struct BELFUSS));
		if ( belfuss_a == NULL )
		{
// knallt dann sowieso :					int i = 0 ;	// fatal-Error 
		}

	} ;
	memcpy ( belfuss_a + belfussmax , &belfuss , sizeof ( struct BELFUSS )) ;
	belfussmax ++ ;
}

int read_belfuss_a(void)
{
	if ( belfussakt < belfussmax )
	{
		memcpy ( &belfuss , belfuss_a + belfussakt, sizeof ( struct BELFUSS )) ;
		belfussakt ++ ;
		return 0 ;
	}
	return 100 ;
}

void ArraysFree () 
{
	belkopfdim = belkopfakt = belkopfmax = 0 ;
	belposdim = belposakt = belposmax = 0 ;
	belfussdim = belfussakt = belfussmax = 0 ;

	if ( belkopf_a ) { free ( belkopf_a ) ; belkopf_a = NULL ; } ;
	if ( belpos_a  ) { free ( belpos_a  ) ; belpos_a  = NULL ; } ;
	if ( belfuss_a ) { free ( belfuss_a ) ; belfuss_a = NULL ; } ;
}

/* --->
Felderzuordnung der Tabelle tempdrx
bearb		// Zeitstempel - nach 10 Tagen loeschen
form_nr		// eigentlich dummy "recinvoice"
lila		// eigentlich dummy immer 0 
lfd			// DER Identifier ( range fuer Automatik )

// integer-Felder
lfd01		//  0 = Gesamt-Kopf , 1 = Belegkopf, 2 = Positionen , 3 = Fuss  
lfd02		// belkopf.bgm_nr
lfd03		// su_adr	( Lieferanten-Adresse )
lfd04		// dp_adr	( Filial-Adresse )
lfd05		// by_adr	( Eigen-Adresse )
lfd06		// belfuss.tax_1_aktiv
lfd07		// belfuss.tax_2_aktiv
lfd08		// dat_referenz ( bel-kopf )
lfd09		// 0 = Original - Normalzustand 
			// > 0  Original mit Kommentaren ( Ausgaben fuer Fehlerprotokoll )
lfd10		// laufende Nummer : DAS Ordnungskriterium

// decimal-Felder(17,4)
dfeld01		//	belpos.tax_feld ( numerisch, "E"-> 0 ) ===== belkopf.tax_feld ( fuer Fuss !!!! )
dfeld02		//  belpos.qty47-Wert ( Anzahl/Gew  )
dfeld03		//  belpos.moa_203    ( Posten-Wert )
dfeld04		//  belpos.pri-Wert   ( Einzelpreis )
dfeld05		//  belfuss.moa_77	  ( Zahl-Betrag )
dfeld06		//  belfuss.moa_79	 ( gesamt-Betrag )
dfeld07		//  belfuss.moa_124
dfeld08		//  belfuss.moa_125
dfeld09		//  belfuss.moa_131
dfeld10		//  belfuss.moa_1_79
dfeld11		//  belfuss.moa_1_124 
dfeld12		//  belfuss.moa_1_125 
dfeld13		//  belfuss.moa_1_131 
dfeld14		//  belfuss.moa_2_79 
dfeld15		//  belfuss.moa_2_124 
dfeld16		//  belfuss.moa_2_125 
dfeld17		//  belfuss.moa_2_131 
dfeld18		//  belfuss.tax_1_feld 
dfeld19		//  belfuss.tax_2_feld
dfeld20		//  

// char-felder(16)
cfeld101	// belkopf.bgm_typ
cfeld102	// belkopf.nad_su_feld   ====== belpos.pia_sa_feld
cfeld103	// belkopf.nad_dp_feld   ====== belpos.lin_ean_feld
cfeld104	// belkopf.nad_by_feld   ====== belpos.pia_bp_feld
cfeld105	// belkopf.on_feld
// char-felder(24)
cfeld201	// belkopf.dat_35 
cfeld202	// belkopf.dat_137
cfeld203	// belkopf.abo_feld
cfeld204	// belpos.Einheit
cfeld205	// 
char-felder(36)
cfeld301	// 
cfeld302	// 
cfeld303	// Zusatzbemerkung  
cfeld304	// belpos: IMD-Bezeichnung 
cfeld305	// bemerkung
<---- */


void wrilistekopfsatz (void)
{
	memcpy ( &tempdrx , &tempdrx_null, sizeof ( struct TEMPDRX )) ;
	sprintf ( tempdrx.form_nr, "recinvoice") ;	// recinvoice-Liste
	tempdrx.lila = 0 ;
	tempdrx.lfd = lfdliste;
	tempdrx.lfd10 = lfdlistennr ;
	lfdlistennr ++ ;

// integer-Felder
	if ( belkopf.bgm_nr == 0 && strlen ( belkopf.bgm_typ )== 0 )
	{
		tempdrx.lfd01 = 0 ;	// Gesamtkopf
		tempdrx.lfd08 = dta_referenz ;	// dta-Referenz eben
	}
	else
		tempdrx.lfd01 = 1 ;	// belegkopf

	tempdrx.lfd02 = belkopf.bgm_nr ;

	// korrekt waere eindeutige Zuordnung Adresse /ILN, wir versuchen es halt mittels Lieferant .... 
	if ( tempdrx.lfd01 == 0 )
		tempdrx.lfd03 = dta.adr ;		// mit etwas Glueck steht hier ja noch der einzige Lieferant drin ....
	else
		tempdrx.lfd03 = 0 ;		// su_adr	( Lieferanten-Adresse )

/* ---
	ZU TUN :
	select lief.adr into tempdrx.lfd03 from lief where lief.iln = $belkopf.nad_su_iln and lief.mdn = mdn.mdn or lief.mdn = 0 
		order by mdn desc 
< ---- */
	if ( (strlen ( belkopf.nad_su_feld )) == 13 )
	{
		sprintf ( lief.iln ,"%s", belkopf.nad_su_feld ) ;
		lief.mdn = mdn.mdn ;
		int di = lief_class.openlief() ;
		di = lief_class.leselief() ;
		if ( !di )
			tempdrx.lfd03 = lief.adr ;
	}
	tempdrx.lfd04 = 0 ;		// dp_adr	( Filial-Adresse )
	if (strlen ( belkopf.nad_dp_feld ) > 11 )
	{
		char filhilfe[10]  ;
//		sprintf (filhilfe, "%s" , belkopf.nad_dp_feld + 7 );
		sprintf (filhilfe, "%s" , belkopf.nad_dp_feld + 9 );	// limitieren fuer 3-stell fil-Nr.
		filhilfe[3] = '\0' ;
/* -->
		ZU TUN :
	select fil.adr from fil where fil.fil = $atoi(filhilfe ) and fil.mdn = mdn.mdn 
< ---- */
		fil.mdn = mdn.mdn ;
		fil.fil = atoi ( filhilfe ) ;
		int di = fil_class.openfil() ;
		di = fil_class.lesefil() ;
		if ( !di )
			tempdrx.lfd04 = fil.adr ;
	}
	tempdrx.lfd05 = mdn.adr ;		// by_adr	( Eigen-Adresse )
	// lfd06		// belfuss.tax_1_aktiv
	// lfd07		// belfuss.tax_2_aktiv
	// lfd08		// belkopf.dta_referenz 
	// lfd09		// 0 = Original - Normalzustand 
					// > Original mit Kommentaren ( Ausgaben fuer Fehlerprotokoll )
	// lfd10		// laufende Nummer : DAS Ordnungskriterium

// decimal-Felder(17,4)
// dfeld01		//	belpos.tax_feld ( numerisch, "E"-> 0 ) ===== belkopf.tax_feld ( fuer Fuss !!!! )
	if ( belkopf.tax_feld[0] == 'E' || strlen ( belkopf.tax_feld)==0 )
		tempdrx.dfeld01 = 0 ;
	else
		tempdrx.dfeld01 = atof ( belkopf.tax_feld ) ;
	// dfeld02		//  belpos.qty47-Wert ( Anzahl/Gew  )
	// dfeld03		//  belpos.moa_203    ( Posten-Wert )
	// dfeld04		//  belpos.pri-Wert   ( Einzelpreis )
	// dfeld05		//  belfuss.moa_77	  ( Zahl-Betrag )
	// dfeld06		//  belfuss.moa_79	 ( gesamt-Betrag )
	// dfeld07		//  belfuss.moa_124
	// dfeld08		//  belfuss.moa_125
	// dfeld09		//  belfuss.moa_131
	// dfeld10		//  belfuss.moa_1_79
	// dfeld11		//  belfuss.moa_1_124 
	// dfeld12		//  belfuss.moa_1_125 
	// dfeld13		//  belfuss.moa_1_131 

	//	dfeld14		//  belfuss.moa_2_79 
	// dfeld15		//  belfuss.moa_2_124 
	// dfeld16		//  belfuss.moa_2_125 
	// dfeld17		//  belfuss.moa_2_131 
	// dfeld18		//  belfuss.tax_1_feld 
	// dfeld19		//  belfuss.tax_2_feld
	// dfeld20		//  

// char-felder(16)
	sprintf ( tempdrx.cfeld101,"%s", belkopf.bgm_typ ) ;
	sprintf ( tempdrx.cfeld102,"%s", belkopf.nad_su_feld ) ;
	sprintf ( tempdrx.cfeld103,"%s", belkopf.nad_dp_feld ) ;
	sprintf ( tempdrx.cfeld104,"%s", belkopf.nad_by_feld ) ;
	sprintf ( tempdrx.cfeld105,"%s", belkopf.rff_on_feld ) ;
// char-felder(24)
	sprintf ( tempdrx.cfeld201,"%s", belkopf.dtm_35_feld ) ; 
	sprintf ( tempdrx.cfeld202,"%s", belkopf.dtm_137_feld ) ;
	sprintf ( tempdrx.cfeld203,"%s", belkopf.rff_abo_feld ) ;
	// cfeld204	// belpos.Einheit
	// cfeld205	// 
// char-felder(36)
	//	cfeld301	// 
	//	cfeld302	// 
	// cfeld303	// Zusatzbemerkung  
	// cfeld304	// belpos: IMD-Bezeichnung 
	if ( strlen ( belkopf.bemerkung ) > 0 ) 
	{
		gesamtfehlcount ++ ;
		tempdrx.lfd09 = gesamtfehlcount ;
	}
	sprintf ( tempdrx.cfeld305, "%s" , belkopf.bemerkung ) ;
	int j = tempdrx_class.inserttempdrx();	
}

void wrilistepossatz (void)
{
	memcpy ( &tempdrx , &tempdrx_null, sizeof ( struct TEMPDRX )) ;
	sprintf ( tempdrx.form_nr, "recinvoice") ;	// recinvoice-Liste
	tempdrx.lila = 0 ;
	tempdrx.lfd = lfdliste;
	tempdrx.lfd10 = lfdlistennr ;
	lfdlistennr ++ ;

	tempdrx.lfd01 = 2 ;		//  0 = Gesamt-Kopf , 1 = Belegkopf, 2 = Positionen , 3 = Fuss  
	tempdrx.lfd02 = belkopf.bgm_nr ;
	// tempdrx.lfd03 = ;		// su_adr	( Lieferanten-Adresse )
	// tempdrx.lfd04 = ;		// dp_adr	( Filial-Adresse )
	tempdrx.lfd05 = mdn.adr ;		// by_adr	( Eigen-Adresse )
	tempdrx.lfd06 = 0 ; 		// belfuss.tax_1_aktiv
	tempdrx.lfd07 = 0 ;		// belfuss.tax_2_aktiv
	//	lfd08	// belkopf.dta_referenz		// 
	tempdrx.lfd09 = 0 ;		// 0 = Original - Normalzustand 
			// 1 = Original mit Kommentaren ( Ausgaben fuer Fehlerprotokoll )

// decimal-Felder(17,4)
	if ( strlen ( belpos.tax_feld ) == 0 )
	{
		sprintf ( belpos.tax_feld ,"%s", belkopf.tax_feld ) ;
	}
	if ( belpos.tax_feld[0] == 'E' || strlen ( belpos.tax_feld)==0 )
		tempdrx.dfeld01 = 0 ;
	else
		tempdrx.dfeld01 = atof ( belpos.tax_feld ) ;
	tempdrx.dfeld02 = belpos.qty_47_wert ;	// belpos -> Anzahl/Gew  
	tempdrx.dfeld03 =  belpos.moa_203_wert ;		// ( Posten-Wert )
	tempdrx.dfeld04	=  belpos.pri_aab_wert ;	// Einzelpreis 
	//	dfeld05		//  belfuss.moa_77	  ( Zahl-Betrag )
	//	dfeld06		//  belfuss.moa_79	 ( gesamt-Betrag )
	//	dfeld07		//  belfuss.moa_124
	//	dfeld08		//  belfuss.moa_125
	//	dfeld09		//  belfuss.moa_131
	//	dfeld10		//  belfuss.moa_1_79
	//	dfeld11		//  belfuss.moa_1_124 
	//	dfeld12		//  belfuss.moa_1_125 
	//	dfeld13		//  belfuss.moa_1_131 
	//	dfeld14		//  belfuss.moa_2_79	
	//	dfeld15		//  belfuss.moa_2_124 
	//	dfeld16		//  belfuss.moa_2_125 
	//	dfeld17		//  belfuss.moa_2_131 
	//	dfeld18		//  belfuss.tax_1_feld 
	//	dfeld19		//  belfuss.tax_2_feld
	//	dfeld20		//  

// char-felder(16)
	sprintf ( tempdrx.cfeld101 ,"%s", belkopf.bgm_typ ) ;
	sprintf ( tempdrx.cfeld102 ,"%s", belpos.pia_sa_feld ) ;	// belkopf.nad_su_feld   ====== belpos.pia_sa_feld
	sprintf ( tempdrx.cfeld103 ,"%s", belpos.lin_feld ) ;	// belkopf.nad_dp_feld   ====== belpos.lin_ean_feld

	int k3k = (int)strlen ( belpos.lin_feld ) ;
	if (k3k == 13 || k3k == 12 || k3k == 8 )
	{
		sprintf ( lief_bzg.ean, "%s", belpos.lin_feld )  ;
		lief_bzg.ean[12]='*' ;	// pruefziffer ignorieren
		lief_bzg.ean[13]='\0' ;
		sprintf ( lief_bzg.lief, "%s", lief.lief ) ;
		lief_bzg.mdn = mdn.mdn ;
		lief_bzg.fil = 0 ;


		double hilfe1 =  0;
		hilfe1 = atof(belpos.pia_bp_feld) ;
		int k4k = lief_bzg_class.openlief_bzg_ean () ;
		k4k = lief_bzg_class.leselief_bzg_ean() ;
		if ( !k4k )
		{
			if ( hilfe1 > 0 )
			{
				if ( hilfe1 != lief_bzg.a )
				{
					sprintf ( belpos.bemerkung,"Art-Nr. lt. EAN : %1.0f" , lief_bzg.a ); 
				}
			}
			else
			{
				sprintf ( belpos.pia_bp_feld,"%1.0f", lief_bzg.a );
			}
		}
		else
		{
					sprintf ( belpos.bemerkung,"EAN nicht gefunden" ); 
		}
	}
	sprintf ( tempdrx.cfeld104 ,"%s", belpos.pia_bp_feld ) ; 	// belkopf.nad_by_feld   ====== belpos.pia_bp_feld
//	sprintf ( tempdrx.cfeld105	// belkopf.on_feld
// char-felder(24)
//	sprintf ( tempdrx.cfeld201	// belkopf.dat_35 
//	sprintf ( tempdrx.cfeld202	// belkopf.dat_137
//	sprintf ( tempdrx.cfeld203	// belkopf.abo_feld
	sprintf ( tempdrx.cfeld204	,"%s", belpos.qty_47_einh ) ;
	//	cfeld205	// 
// char-felder(36)
	//	cfeld301	// 
	//	cfeld302	// 
	//	cfeld303	// Zusatzbemerkung  
	sprintf ( tempdrx.cfeld304,"%s", belpos.imd_a_feld ) ;	// belpos: IMD-Bezeichnung 
	if ( strlen ( belpos.bemerkung ) > 0 ) 
	{
		gesamtfehlcount ++ ;
		tempdrx.lfd09 = gesamtfehlcount ;
	}

	sprintf ( tempdrx.cfeld305,"%s", belpos.bemerkung ) ;

	int j = tempdrx_class.inserttempdrx();	

}

void wrilistefusssatz (void)
{
	memcpy ( &tempdrx , &tempdrx_null, sizeof ( struct TEMPDRX )) ;
	sprintf ( tempdrx.form_nr, "recinvoice") ;	// recinvoice-Liste
	tempdrx.lila = 0 ;
	tempdrx.lfd = lfdliste;
	tempdrx.lfd10 = lfdlistennr ;
	lfdlistennr ++ ;

// integer-Felder
	tempdrx.lfd01 = 3 ;		//  0 = Gesamt-Kopf , 1 = Belegkopf, 2 = Positionen , 3 = Fuss  
	tempdrx.lfd02 = belkopf.bgm_nr ;
	// lfd03		// su_adr	( Lieferanten-Adresse )
	// lfd04		// dp_adr	( Filial-Adresse )
	tempdrx.lfd05 = mdn.adr ;		// by_adr	( Eigen-Adresse )
	tempdrx.lfd06 = belfuss.tax_1_aktiv ;		// belfuss.tax_1_aktiv
	tempdrx.lfd07 = belfuss.tax_2_aktiv ;		// belfuss.tax_2_aktiv
	// lfd08		// belkopf.dta_referenz 
	// lfd09		// 0 = Original - Normalzustand 
			// 1 = Original mit Kommentaren ( Ausgaben fuer Fehlerprotokoll )

// decimal-Felder(17,4)
	if ( belkopf.tax_feld[0] == 'E' || strlen ( belkopf.tax_feld)==0 )
		tempdrx.dfeld01 = 0 ;
	else
		tempdrx.dfeld01 = atof ( belkopf.tax_feld ) ;
	//	dfeld01		//	belpos.tax_feld ( numerisch, "E"-> 0 ) ===== belkopf.tax_feld ( fuer Fuss !!!! )
	// dfeld02		//  belpos.qty47-Wert ( Anzahl/Gew  )
	// dfeld03		//  belpos.moa_203    ( Posten-Wert )
	// dfeld04		//  belpos.pri-Wert   ( Einzelpreis )
	tempdrx.dfeld05 = belfuss.moa_77_wert ;	//	  ( Zahl-Betrag )
	tempdrx.dfeld06 = belfuss.moa_79_wert ;	// ( gesamt-Betrag )
	tempdrx.dfeld07	= belfuss.moa_124_wert ;
	tempdrx.dfeld08	= belfuss.moa_125_wert ;
	tempdrx.dfeld09	= belfuss.moa_131_wert ;
	tempdrx.dfeld10	= belfuss.moa_1_79_wert ;
	tempdrx.dfeld11 = belfuss.moa_1_124_wert ; 
	tempdrx.dfeld12 = belfuss.moa_1_125_wert ;
	tempdrx.dfeld13 = belfuss.moa_1_131_wert ; 
	tempdrx.dfeld14	= belfuss.moa_2_79_wert ; 
	tempdrx.dfeld15 = belfuss.moa_2_124_wert ; 
	tempdrx.dfeld16	= belfuss.moa_2_125_wert ;
	tempdrx.dfeld17	= belfuss.moa_2_131_wert ;

	if ( belfuss.tax_1_aktiv )
	{
		if ( belfuss.tax_f_1[0] == 'E' || strlen ( belfuss.tax_f_1)==0 )
			tempdrx.dfeld18 = 0 ;
		else
			tempdrx.dfeld18 = atof ( belfuss.tax_f_1 ) ;
	}
	if ( belfuss.tax_2_aktiv )
	{
		if ( belfuss.tax_f_2[0] == 'E' || strlen ( belfuss.tax_f_2)==0 )
			tempdrx.dfeld19 = 0 ;
		else
			tempdrx.dfeld19 = atof ( belfuss.tax_f_2 ) ;
	}
	//	dfeld20		//  

// char-felder(16)
	sprintf ( tempdrx.cfeld101 , "%s" , belkopf.bgm_typ ) ;
	if (!strncmp ( belfuss.bgm_typ,"END",3))
	{
		sprintf ( tempdrx.cfeld101 , "END" ) ;
	}
	// cfeld102	// belkopf.nad_su_feld   ====== belpos.pia_sa_feld
	// cfeld103	// belkopf.nad_dp_feld   ====== belpos.lin_ean_feld
	// cfeld104	// belkopf.nad_by_feld   ====== belpos.pia_bp_feld	
	// cfeld105	// belkopf.on_feld
// char-felder(24)
	// cfeld201	// belkopf.dat_35 
	// cfeld202	// belkopf.dat_137
	// cfeld203	// belkopf.abo_feld
	// cfeld204	// belpos.Einheit
	// cfeld205	// 
// char-felder(36)
	// cfeld301	// 
	// cfeld302	// 
// cfeld303	// Zusatzbemerkung  
	// cfeld304	// belpos: IMD-Bezeichnung

	if ( strlen ( belfuss.bemerkung ) > 0 ) 
	{
		gesamtfehlcount ++ ;
		tempdrx.lfd09 = gesamtfehlcount ;
	}

	sprintf ( tempdrx.cfeld305, "%s" , belfuss.bemerkung ) ;	// bemerkung

	int j = tempdrx_class.inserttempdrx();	
}

DWORD ProcWaitExec (LPSTR prog, WORD SHOW_MODE, int x, int y, int cx, int cy)
/**
Fenster-Process starten und nicht auf Ende Warten.
**/
{
        STARTUPINFO sti;
        PROCESS_INFORMATION pi;
        DWORD ExitCode; 
        int ret;

        ZeroMemory (&sti, sizeof (sti));
        sti.cb = sizeof (sti);
        if (x != -1)
        {
                     sti.dwFlags |= STARTF_USEPOSITION;
                     sti.dwX = x;
                     sti.dwY = y;
        }

        if (cx != -1)
        {
                     sti.dwFlags |= STARTF_USESIZE;
                     sti.dwXSize = cx;
                     sti.dwYSize = cy;
        }
        sti.wShowWindow = SHOW_MODE;
        sti.dwFlags |= STARTF_USESHOWWINDOW;

        ret = CreateProcess (NULL,
                       prog,
                       NULL,
                       NULL,
                      (int) NULL,
                      (unsigned long) NULL,
                       NULL,
                       NULL,
                       &sti,
                       &pi);
        if (ret)
        {
                  CloseHandle (pi.hThread);
                  WaitForSingleObject (pi.hProcess, INFINITE);
        }
		else
        {
			      return -1;
        }				  
		GetExitCodeProcess (pi.hProcess, &ExitCode);
		while (ExitCode == STILL_ACTIVE)
		{
                  GetExitCodeProcess (pi.hProcess, &ExitCode);
		}
        CloseHandle (pi.hProcess);
        return ExitCode;
}


void VerbucheListe(void)
{
} ;

// Was soll ich nun auswerten : ?
// 1.Durchlauf : Check, ob Datenfehler falls ja : dann Protokoll und Abbruch

// 2. Durchlauf : Erstellung Liste  ODER schreiben in Datenbank - ich weiss aber noch nicht, wohin

int auswerten () 
{
// zuerst werden Koepfe geschrieben, danach evtl posten und danach evtl. Fuesse 
// d.h genau ein Kopf, danach evtl n Posten und zum schluss evtl 1 Fuss , die Reihenfolge stimmt immer,
// es koenne aber sowohl Posten als auch Fuesse fehlen 

char gruppbgm_typ[12] ;
long gruppbgm_nr ;
int posgeschrieb ;
int fussgeschrieb ;
int retp ;	// status Pos-Satz
int retf ;	// status Fuss-Satz 

char s1[156] ;
char s2[156] ;
char s3[156] ;
char s4[156] ;
 
    sprintf ( tempdrx.form_nr, "recinvoice") ;	// recinvoice-Liste
	tempdrx.lila = 0 ;

	tempdrx_class.deletetempdrx () ;

	sprintf ( s2, "%s\\recinvoice.prm", getenv ( "TMPPATH" ) ) ;
	sprintf ( s3, "%s\\recinvoli.prm", getenv ( "TMPPATH" ) ) ;
	sprintf ( s4, "%s\\recinvopr.prm", getenv ( "TMPPATH" ) ) ;

	gesamtfehlcount = 0  ;

	int kk = tempdrx_class.lesetempdrx ()	;
	tempdrx.lfd ++ ;	// Max + 1
	lfdliste = tempdrx.lfd ;
	lfdlistennr =  0 ;

	posgeschrieb = 1 ;
	fussgeschrieb = 1 ;

// Prinzip : kopfsatz gibt es immer, pos-  bzw. fuss-Saetze darunter koennten fehlen 

	while ( ! read_belkopf_a () )
	{
		sprintf ( gruppbgm_typ , "%s" , belkopf.bgm_typ ) ;
		gruppbgm_nr = belkopf.bgm_nr ;
		wrilistekopfsatz ()  ;

		if ( posgeschrieb )
		{
			retp = read_belpos_a ()	;
			if ( ! retp ) posgeschrieb = 0 ;
		}

		while ( (!retp) && (gruppbgm_nr == belpos.bgm_nr) && (! strncmp ( gruppbgm_typ , belpos.bgm_typ, 3 )) )
		{
			wrilistepossatz () ;
			posgeschrieb = 1 ;
			retp = read_belpos_a () ;
			if ( ! retp ) posgeschrieb = 0 ;
		}
		if ( fussgeschrieb )
		{
			retf = read_belfuss_a () ;
			if ( ! retf ) fussgeschrieb = 0 ;
		}
		if ( (!retf) && (gruppbgm_nr == belfuss.bgm_nr) && (! strncmp ( gruppbgm_typ , belfuss.bgm_typ, 3 )) )
		{
			wrilistefusssatz () ;
			fussgeschrieb = 1 ;
			retf = read_belfuss_a () ;
			if ( ! retf ) fussgeschrieb = 0 ;
		}
	}
	sprintf ( belfuss.bemerkung ," %d Error(s) ", gesamtfehlcount ) ;
	sprintf ( belfuss.bgm_typ ,"END" ) ;
	wrilistefusssatz() ;

		FILE * fp2 ;
		FILE * fp3 ;
		FILE * fp4 ;
		fp2 = fopen ( s2 , "w" ) ;
		if ( fp2 == NULL )
		{
			return -1  ;
		}
		fp3 = fopen ( s3 , "w" ) ;
		if ( fp3 == NULL )
		{
			fclose ( fp2 ) ;
			return -1  ;
		}
		fp4 = fopen ( s4 , "w" ) ;
		if ( fp4 == NULL )
		{
			fclose ( fp2 ) ;
			fclose ( fp3 ) ;
			return -1  ;
		}
	
		sprintf ( s1, "NAME recinvoice\n" ) ; 
	
		CString St = s1;
		St.MakeUpper() ;
		sprintf ( s1 ,"%s", St.GetBuffer(512)) ;
		fputs ( s1 , fp2 );

		sprintf ( s1, "NAME recinvoli\n" ) ; 
		St = s1;
		St.MakeUpper() ;
		sprintf ( s1 ,"%s", St.GetBuffer(512)) ;
		fputs ( s1 , fp3 );

		sprintf ( s1, "NAME recinvopr\n" ) ; 
		St = s1;
		St.MakeUpper() ;
		sprintf ( s1 ,"%s", St.GetBuffer(512)) ;
		fputs ( s1 , fp4 );


		fputs ( "LABEL 0\n" , fp2 ) ;
		fputs ( "LABEL 0\n" , fp3 ) ;
		fputs ( "LABEL 0\n" , fp4 ) ;

		sprintf ( s1, "DRUCK 1\n"   ) ;
		fputs ( s1 , fp2 ) ;
		fputs ( s1 , fp3 ) ;
		fputs ( s1 , fp4 ) ;

		sprintf ( s1, "lfd %ld %ld\n", lfdliste , lfdliste   ) ;
		fputs ( s1 , fp2) ;
		fputs ( s1 , fp3) ;
		fputs ( s1 , fp4) ;

		fputs ( "ANZAHL 1\n" , fp2 ) ;
		fputs ( "ANZAHL 1\n" , fp3 ) ;
		fputs ( "ANZAHL 1\n" , fp4 ) ;

		fclose ( fp2) ;
		fclose ( fp3) ;
		fclose ( fp4) ;

		ModDialog moddialog ;

		int NurProt = 0 ;
		int NurProtaktiv = 1 ;
		int ListeDicht = 0 ;
		int ListeDichtaktiv = 1 ;
		int Details = 1 ;
		int Detailsaktiv = 1 ;
		int VerBuchen = 0 ;
		int VerBuchenaktiv = 0 ;

		moddialog.setzeparas( NurProt,NurProtaktiv
			                  ,ListeDicht,ListeDichtaktiv
							  ,Details,Detailsaktiv
							  ,VerBuchen,VerBuchenaktiv ) ;
		if ( moddialog.DoModal () != IDOK )
		{
			return -1 ;
		}

		moddialog.holeparas ( &NurProt, &ListeDicht, &Details, &VerBuchen ) ;

		if ( Details )
		{
			sprintf ( s1, "dr70001.exe -datei \"%s\" " , s2 );
			int ex_code = ProcWaitExec( (char *)s1, SW_SHOW, -1, 0, -1, 0 );
		}

		if ( ListeDicht )
		{
			sprintf ( s1, "dr70001.exe -datei \"%s\" " , s3 );
			int ex_code = ProcWaitExec( (char *)s1, SW_SHOW, -1, 0, -1, 0 );
		}

		if ( NurProt )
		{
			sprintf ( s1, "dr70001.exe -datei \"%s\" " , s4 );
			int ex_code = ProcWaitExec( (char *)s1, SW_SHOW, -1, 0, -1, 0 );
		}
		if ( VerBuchen )
		{
			VerbucheListe () ;
		}
		lfdliste = 0 ;

	return dabbruch  ;

}

int Abarbeiten ( CrecinvoiceDlg * Cparent , short dmdn , char * quelldatei , int testignore )
{

	ArraysInit () ;

	dtestignore = testignore ;
//	testphase = 2 ;	// immer zuerst eine Testrunde und dann je nach restl. Einstellungen weiterhandeln 
	dtestmode = 0 ;	// hier wird der testmode eingefangen

	char hilfe[256] ;
	wsprintf( hilfe, "%s", getenv("testmode") );
	TEST = 0 ;
	if ( atoi ( hilfe) == 5 ) TEST =1 ;

	dabbruch = 0 ;

	maskel = 0 ;

	fpin = fopen ( quelldatei , "r" );
	int i =	 einlesen ( Cparent ,dmdn, quelldatei,  testignore ) ;

	i = auswerten () ;

	if ( dtestmode == 1 || i )
	{
// spaeter : 	MessageBox(NULL, "Diese Daten sind noch nicht in der Datenbank", " ", MB_OK|MB_ICONSTOP);
		ArraysFree() ;
		Cparent->myDisplayMessage( "Diese Daten sind noch nicht in der Datenbank" ) ;

		return 0 ;
	}

	ArraysFree () ;
		sprintf ( hilfe ,"  %d Belege eingelesen " , danz_nachrichten ) ; 
		Cparent->myDisplayMessage( hilfe ) ;

	return i ;
}


int Abarbeiten_dummy ( CrecinvoiceDlg * Cparent , short dmdn , char * quelldatei , int testignore )
{

	dtestignore = testignore ;
 	testphase = 2 ;	// immer zuerst eine Testrunde und dann je nach restl. Einstellungen weiterhandeln 
	dtestmode = 0 ;	// hier wird der testmode eingefangen

	char hilfe[256] ;
	wsprintf( hilfe, "%s", getenv("testmode") );
	TEST = 0 ;
	if ( atoi ( hilfe) == 5 ) TEST =1 ;

// Abarbeiten_dummy_inaktiv 
	dabbruch = 0 ;

	maskel = 0 ;

// Abarbeiten_dummy_inaktiv 

/*----->
			let daufstat  = 0
            let daufpstat = 0 
            let dtestmode = 0
            let dabbruch  = 0
< ----- */


// Abarbeiten_dummy_inaktiv 

	while ( testphase > -1  )
	{
		testphase -- ;
		if ( ! testphase )
		{	// testphase == 1 :  Analyse runde
			// testpahse == 0 : evtl. Abarbeitung

			if ( dtestmode == 1 )
			{
				MessageBox(NULL, "Diese Daten sind noch nicht in der Datenbank", " ", MB_OK|MB_ICONSTOP);

				return 0 ;
			}
		
//           perform aufnummern (3)
//           perform beginin_work
		}

// Abarbeiten_dummy_inaktiv 


		else
		{
//			perform aufnummern (0)
  		}
		fpin = fopen ( quelldatei , "r" );
		int i =	 einlesen ( Cparent ,dmdn, quelldatei,  testignore ) ;
/* ---->
           if not k
               perform commitin_work
               if not testphase
                   // Erstellung einer Sicherung, Schutz gegen Doppelbuchung
                        link ( ddateiname ddateinames )
                end
                else
                    perform rollbackin_work
                end
                perform disp_fkt ( -1 0 )
                if dabbruch
                   perform disp_msg ( 197 , 1)
                                          // Diese Daten sind noch nicht in der DABA
                   perform disp_msg ( 266, 0 )
                   break
                end
                erase ( 12 1 )
                display pos ( 12 , 1 ) value
                   ("Diese Datei enthaelt ." # danz_nachrichten # " Auftraege" )
            end
            perform aufnummern (4)
            perform protokoll ( 0, "Datei enthielt ." # danz_nachrichten # " Auftraege" )
            perform protokoll ( 1, "" ) // jedenfalls erst mal schliessen
            continue current
< ----- */

// Abarbeiten_dummy_inaktiv 

	}
	return 1 ;	// Syntax-Dummy ?? 
}
/* -------->
// bissel in Abhaengigkeit der Dateistruktur : nach jedem Segmentende
// einen Zeilenumbruch organisieren
// filteri : Original-Datei
// filtero : Interne Datei

PROCEDURE zeilenrein

field outpuffer  char ( 100 )
field outpointer smallint
field hinwert smallint
field inwert     char (1)
field zeipo     smallint
field nichtdoppelt smallint

  open channel filteri mode binary read file (dateinamex)
  if sysstatus
        close channel filteri
        return
  end

  open channel filtero mode binary write file (ddateiname)
  if sysstatus
       create channel filtero mode binary write file (ddateiname)
  end
  if sysstatus
        return
  end
  let nichtdoppelt = 0 
  init outpuffer
  let outpointer = 1 
  while ( 1 )
      init inwert
      select channel filteri
      read ( inwert )
      if sysstatus
           break
      end
      let hinwert = getchar ( inwert 1 )
      if hinwert = 13
          let nichtdoppelt = 0
      end
      if hinwert = 10
         let nichtdoppelt = 0
      end

      if nichtdoppelt
            putchar ( outpuffer , outpointer , 13 )
            let outpointer = outpointer + 1
            if outpointer > 100
                 select channel filtero
                 write ( outpuffer )
                 init outpuffer
                 let outpointer = 1
            end
            putchar ( outpuffer , outpointer , 10 )
            let outpointer = outpointer + 1
            if outpointer > 100
                 select channel filtero
                 write ( clipped ( outpuffer) )
                 init outpuffer
                 let outpointer = 1
            end
            let nichtdoppelt = 0 
      end

      putchar ( outpuffer , outpointer , hinwert )
      if hinwert =   39      // "'"
          let nichtdoppelt = 1
      end
     
      let outpointer = outpointer + 1
      if outpointer > 100
           select channel filtero
           write ( outpuffer) 
           init outpuffer
           let outpointer = 1
      end
  end
  select channel filtero
  if outpointer > 1
        write ( clipped ( outpuffer ))
  end
  flush channel
  close channel
  select channel filteri
  close channel

END
< ------- */

/* --->
PROCEDURE beginin_work
    if in_work = IN_ARBEIT
        perform commit_work
    end
    perform begin_work
    let in_work = IN_ARBEIT
END
< ------ */
/* ------>
PROCEDURE commitin_work
    if in_work = IN_ARBEIT
        perform commit_work
    end
    let in_work = NICHT_IN_ARBEIT
END
< -------- */

/* ---->
PROCEDURE rollbackin_work
    if in_work = IN_ARBEIT
        perform rollback_work
    end
    let in_work = NICHT_IN_ARBEIT
END
< ------ */

/* ----->
PROCEDURE protokoll
PARAMETER
  field imodus smallint
  field dspruch char(75)
END

 field protopen smallint
 field dprotname char (100)

    if imodus
       if protopen
            select channel cprotokoll
            flush channel
            close channel
            let protopen = 0
            let firstprotopen = 1   // war offen in diesem Aufruf
       end
       return
    end

    if protopen
        select channel cprotokoll
    else
        init dprotname
        environment ( "TMPPATH", dprotname )
        putstr ( dprotname , "\order.pro" , 0 , 0 )
        if firstprotopen
            open channel cprotokoll mode append file ( dprotname )
            if not  sysstatus
                let firstprotopen = 1
                let protopen = 1
            else
                let firstprotopen = 0
            end
        end
        if not firstprotopen 
            unlink ( dprotname )

            create channel cprotokoll mode write file ( dprotname )
            if sysstatus
                let dabbruch = 1
                let protopen = 0
                perform disp_msg ( 2068, 1 )
                close channel cprotokoll
                return
            else
                let protopen = 1
                let firstprotopen = 1
            end
        end
    end
    write ( dspruch )
END
< ------ */


/* ---> 
// imodus = 0 : loeschen
// imodus = 1 : hochzaehlen
// imodus = 2 : ausliefern
// imodus = 3 : Nummern aquirieren
// imodus = 4 : Nummern gegebnenfalls freigeben

PROCEDURE aufnummern
PARAMETER
field imodus smallint
END
field gscont  smallint
field incont  smallint
field outcont smallint
field internaufnr like aufk.auf
// mehr als 800 Auftraege je Datei sind sicher nicht normal
field innummern integer DIMENSION (800)

    if imodus = 0
        let incont = 0
        let outcont = 0
        let gscont = 0
        init innummern
        return
    end
    if imodus = 1
       let gscont = gscont + 1
       if gscont > 799
            perform protokoll ( 0, " Mehr als 800 Auftraege" )
            let gscont = 799
            let dabbruch = 1
       end
       return
    end
    if imodus = 2
        let aufk.auf = innummern[outcont]
        let outcont = outcont + 1
        if outcont > 799
            let outcont = 799
        end
        return
    end
    if imodus = 3
        execute from sql set lock mode to wait end
        while incont < gscont
            perform beginin_work
            perform nvholid ( mdn.mdn, 0 , "auf" ) returning ( k )
            if not k
                 let internaufnr = taautonum.nr_nr
                 let innummern[incont] = internaufnr
                 let incont = incont + 1
            end
        end
        perform commitin_work
        execute from sql set lock mode to not wait end
        return
    end
    if imodus = 4
        execute from sql set lock mode to wait end
        while incont < gscont
            let internaufnr = innummern[incont]
            if internaufnr > 0
                perform beginin_work
                perform nveinid ( mdn.mdn ,0 ,"auf" internaufnr )
                                 returning ( k )
            end
            let incont = incont + 1
        end
        perform commitin_work
        execute from sql set lock mode to not wait end
        return
    end
END
< ---- */


