#include "stdafx.h"
#include "DbClass.h"
#include "lief_bzg.h"

extern DB_CLASS dbClass;

struct LIEF_BZG lief_bzg,  lief_bzg_null;

LIEF_BZG_CLASS lief_bzg_class ;

int LIEF_BZG_CLASS::leselief_bzg_a (void)
{
      int di = dbClass.sqlfetch (readcursor);

	  return di;
}

int LIEF_BZG_CLASS::openlief_bzg_a (void)
{

		if ( readcursor < 0 ) prepare ();	
		
         return dbClass.sqlopen (readcursor);
}

int LIEF_BZG_CLASS::leselief_bzg_ean (void)
{
      int di = dbClass.sqlfetch (test_upd_cursor);

	  return di;
}

int LIEF_BZG_CLASS::openlief_bzg_ean (void)
{

		if ( readcursor < 0 ) prepare ();	
		
         return dbClass.sqlopen (test_upd_cursor);
}

void LIEF_BZG_CLASS::prepare (void)
{

	// readcursor : lesen mittels lief_bzg.a
	// test_upd_cursor : lesen mittels lief_bzg.ean 

	dbClass.sqlin ((double *) &lief_bzg.a,  SQLDOUBLE, 0 ) ;
	dbClass.sqlin (( char *)  lief_bzg.lief, SQLCHAR, 20 ) ;
	dbClass.sqlin (( short *) &lief_bzg.mdn, SQLSHORT, 0 ) ;
	dbClass.sqlin (( short *) &lief_bzg.fil, SQLSHORT, 0 ) ;	// immer == 0  jedoch notwendig wegen INDEX ?!


//	dbClass.sqlout (( double *) &lief_bzg.a, SQLDOUBLE , 0 ) ;
	dbClass.sqlout (( char *)    lief_bzg.best_txt1, SQLCHAR , 26 )  ;
	dbClass.sqlout (( char *)    lief_bzg.best_txt2, SQLCHAR , 26 ) ;
//	dbClass.sqlout (( short *)  &lief_bzg.fil, SQLSHORT , 0 ) ;
//	dbClass.sqlout (( char *)    lief_bzg.lief, SQLCHAR , 20 ) ;
	dbClass.sqlout (( char *)    lief_bzg.lief_best, SQLCHAR , 20 ) ;
	dbClass.sqlout (( char *)    lief_bzg.lief_kz, SQLCHAR , 3 ) ;
	dbClass.sqlout (( char *)    lief_bzg.lief_rht, SQLCHAR , 3 ) ;

	dbClass.sqlout (( long *)   &lief_bzg.lief_s, SQLLONG , 0 ) ;
	dbClass.sqlout (( short *)  &lief_bzg.lief_zeit, SQLSHORT , 0 ) ;
	dbClass.sqlout (( short *)  &lief_bzg.me_einh_ek, SQLSHORT , 0 ) ;
	dbClass.sqlout (( short *)  &lief_bzg.mdn, SQLSHORT , 0 ) ;
	dbClass.sqlout (( double *) &lief_bzg.min_best, SQLDOUBLE , 0 ) ;

	dbClass.sqlout (( double *) &lief_bzg.pr_ek, SQLDOUBLE , 0 ) ;
	dbClass.sqlout (( char *)    lief_bzg.me_kz, SQLCHAR ,3 ) ;
	dbClass.sqlout (( TIMESTAMP_STRUCT *) &lief_bzg.dat, SQLTIMESTAMP, 26 )  ;
	dbClass.sqlout (( char *)    lief_bzg.zeit, SQLCHAR , 10 ) ;
	dbClass.sqlout (( char *)    lief_bzg.modif, SQLCHAR , 3 ) ;

	dbClass.sqlout (( double *) &lief_bzg.pr_ek_eur, SQLDOUBLE , 0 ) ;
	dbClass.sqlout (( double *) &lief_bzg.pr_ek_sa, SQLDOUBLE , 0 ) ;
	dbClass.sqlout (( double *) &lief_bzg.pr_ek_sa_eur, SQLDOUBLE , 0 ) ;
	dbClass.sqlout (( char *)    lief_bzg.freifeld1, SQLCHAR , 40 ) ;
	dbClass.sqlout (( char *)    lief_bzg.ean, SQLCHAR , 20 ) ;

	readcursor = (short) dbClass.sqlcursor (
		"select "

		"  best_txt1 ,best_txt2 ,lief_best ,lief_kz ,lief_rht "
		" ,lief_s ,lief_zeit ,me_einh_ek ,mdn ,min_best "
		" ,pr_ek ,me_kz ,dat ,zeit ,modif "
		" ,pr_ek_eur ,pr_ek_sa ,pr_ek_sa_eur ,freifeld1 ,ean "
		
		" from lief_bzg where a = ?  and lief = ? and ( mdn = ? or mdn = 0 ) and fil = ? "
		" order by mdn desc " );


	dbClass.sqlin (( char *)  lief_bzg.ean, SQLCHAR, 20 )  ;
	dbClass.sqlin (( char *)  lief_bzg.lief, SQLCHAR, 20 ) ;
	dbClass.sqlin (( short *) &lief_bzg.mdn, SQLSHORT, 0 ) ;
	dbClass.sqlin (( short *) &lief_bzg.fil, SQLSHORT, 0 ) ;	// immer == 0  jedoch notwendig wegen INDEX ?!

	dbClass.sqlout (( double *) &lief_bzg.a, SQLDOUBLE , 0 ) ;
	dbClass.sqlout (( char *)    lief_bzg.best_txt1, SQLCHAR , 26 )  ;
	dbClass.sqlout (( char *)    lief_bzg.best_txt2, SQLCHAR , 26 ) ;
//	dbClass.sqlout (( short *)  &lief_bzg.fil, SQLSHORT , 0 ) ;
//	dbClass.sqlout (( char *)    lief_bzg.lief, SQLCHAR , 20 ) ;
	dbClass.sqlout (( char *)    lief_bzg.lief_best, SQLCHAR , 20 ) ;
	dbClass.sqlout (( char *)    lief_bzg.lief_kz, SQLCHAR , 3 ) ;
	dbClass.sqlout (( char *)    lief_bzg.lief_rht, SQLCHAR , 3 ) ;

	dbClass.sqlout (( long *)   &lief_bzg.lief_s, SQLLONG , 0 ) ;
	dbClass.sqlout (( short *)  &lief_bzg.lief_zeit, SQLSHORT , 0 ) ;
	dbClass.sqlout (( short *)  &lief_bzg.me_einh_ek, SQLSHORT , 0 ) ;
	dbClass.sqlout (( short *)  &lief_bzg.mdn, SQLSHORT , 0 ) ;
	dbClass.sqlout (( double *) &lief_bzg.min_best, SQLDOUBLE , 0 ) ;

	dbClass.sqlout (( double *) &lief_bzg.pr_ek, SQLDOUBLE , 0 ) ;
	dbClass.sqlout (( char *)    lief_bzg.me_kz, SQLCHAR ,3 ) ;
	dbClass.sqlout (( TIMESTAMP_STRUCT *) &lief_bzg.dat, SQLTIMESTAMP, 26 )  ;
	dbClass.sqlout (( char *)    lief_bzg.zeit, SQLCHAR , 10 ) ;
	dbClass.sqlout (( char *)    lief_bzg.modif, SQLCHAR , 3 ) ;

	dbClass.sqlout (( double *) &lief_bzg.pr_ek_eur, SQLDOUBLE , 0 ) ;
	dbClass.sqlout (( double *) &lief_bzg.pr_ek_sa, SQLDOUBLE , 0 ) ;
	dbClass.sqlout (( double *) &lief_bzg.pr_ek_sa_eur, SQLDOUBLE , 0 ) ;
	dbClass.sqlout (( char *)    lief_bzg.freifeld1, SQLCHAR , 40 ) ;
	dbClass.sqlout (( char *)    lief_bzg.ean, SQLCHAR , 20 ) ;

	// matches "123456789012*"



  test_upd_cursor = (short) dbClass.sqlcursor (
		"select "

		"  a ,best_txt1 ,best_txt2 ,lief_best ,lief_kz ,lief_rht "
		" ,lief_s ,lief_zeit ,me_einh_ek ,mdn ,min_best "
		" ,pr_ek ,me_kz ,dat ,zeit ,modif "
		" ,pr_ek_eur ,pr_ek_sa ,pr_ek_sa_eur ,freifeld1 ,ean "

		" from lief_bzg where ean matches ?  and lief = ? and ( mdn = ? or mdn = 0 ) and fil = ? "
		" order by mdn desc " );

}


