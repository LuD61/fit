#ifndef _DTA_DEF
#define _DTA_DEF

struct DTA {

    long dta ;
    char kun_krz1[17] ;
	long adr ;
    char iln[17] ;
    short typ ;				// METRO,REWE,TENGELMANN ......
    short test_kz ;			// 1 => testkz wird generiert ( INVOICE)
    short waehrung ;		// 2 => EURO ,1 => DM
    char dat_name[17];		// Hilfssegmente Dateiname ( z.B. FG-DESADV)
    long nummer;			// lfd-Nr. Datei (z.B. FG-DESADV)
    short pri_seg;			// Preissegment unterdruecken ( z.B. FG-DESADV)
	short reli ;			// per Rechnungsliste selektieren
};

extern struct DTA dta, dta_null;

class DTA_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int dbcount (void);
               int lesedta (void);
               int lesealldta (void);
               int opendta (void);
               int openalldta (void);
               DTA_CLASS () : DB_CLASS ()
               {
               }
};
#endif

