// recinvoiceDlg.h : Headerdatei
//

#pragma once
#include "afxwin.h"

// CrecinvoiceDlg-Dialogfeld
class CrecinvoiceDlg : public CDialog
{
// Konstruktion
public:
	CrecinvoiceDlg(CWnd* pParent = NULL);	// Standardkonstruktor

// Dialogfelddaten
	enum { IDD = IDD_RECINVOICE_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV-Unterst�tzung


// Implementierung
protected:
	HICON m_hIcon;

	// Generierte Funktionen f�r die Meldungstabellen
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
public:
	afx_msg BOOL PreTranslateMessage(LPMSG) ;	// 080408
	afx_msg BOOL NoListCtrl (CWnd *) ;			// 080408
public:
	afx_msg void OnBnClickedButton1();
public:
	CEdit m_mandant;
public:
	CString v_mandant;
public:
	CEdit m_mdnname;
public:
	CString v_mdnname;
public:
	CEdit m_filename;
public:
	CString v_filename;
public:
	afx_msg void OnEnKillfocusMandant();
public:
	afx_msg void OnEnKillfocusFilename();
public:
	CButton m_check1;
public:
	BOOL v_check1;
public:
	afx_msg void OnBnClickedCheck1();
public:
	CEdit m_info;
public:
	CString v_info;
public:
	afx_msg void OnCbnSelchangeCombo1();
public:
	afx_msg void OnCbnKillfocusCombo1();
public:
	void myDisplayMessage(char*) ;
public:
	CComboBox m_combo1;
public:
	CString v_combo1;
};
