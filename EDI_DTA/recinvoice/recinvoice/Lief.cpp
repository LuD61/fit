#include "stdafx.h"
#include "DbClass.h"
#include "Lief.h"

extern DB_CLASS dbClass;

struct LIEF lief,  lief_null;

LIEF_CLASS lief_class ;

int LIEF_CLASS::leselief (void)
{
      int di = dbClass.sqlfetch (readcursor);
	  return di;
}

int LIEF_CLASS::openlief (void)
{

		if ( readcursor < 0 ) prepare ();	
		
         return dbClass.sqlopen (readcursor);
}

void LIEF_CLASS::prepare (void)
{

	dbClass.sqlin ((char  *)  lief.iln, SQLCHAR, 20);
	dbClass.sqlin ((short *) &lief.mdn, SQLSHORT, 0);

	// 1
	dbClass.sqlout ((char   *)  lief.abr, SQLCHAR, 3) ;
	dbClass.sqlout ((long   *) &lief.adr, SQLLONG, 0) ;
	dbClass.sqlout ((char   *)  lief.bank_nam, SQLCHAR,40) ;	
	dbClass.sqlout ((long   *) &lief.bbn, SQLLONG, 0) ;
	dbClass.sqlout ((short  *) &lief.best_sort,SQLSHORT,0 ) ;
	// 2
	dbClass.sqlout ((char   *)  lief.best_trans_kz, SQLCHAR, 3) ;
	dbClass.sqlout ((long   *) &lief.blz, SQLLONG, 0) ;
	dbClass.sqlout ((char   *)  lief.bonus_kz, SQLCHAR, 3) ;
	dbClass.sqlout ((short  *) &lief.fil,SQLSHORT,0 ) ;	
	dbClass.sqlout ((char   *)  lief.fracht_kz, SQLCHAR, 3) ;
	// 3
	dbClass.sqlout ((char   *)  lief.ink, SQLCHAR, 20 ) ;
	dbClass.sqlout ((long   *) &lief.kreditor, SQLLONG, 0) ;
	dbClass.sqlout ((char   *)  lief.kto, SQLCHAR, 20 ) ;
	dbClass.sqlout ((TIMESTAMP_STRUCT *) &lief.letzt_lief, SQLTIMESTAMP, 26);
	dbClass.sqlout ((char   *)  lief.lief, SQLCHAR, 20 ) ;
	// 4
	dbClass.sqlout ((char   *)  lief.lief_kun, SQLCHAR, 20 ) ;
	dbClass.sqlout ((char   *)  lief.lief_rht, SQLCHAR, 3 ) ;
	dbClass.sqlout ((long   *) &lief.lief_s, SQLLONG, 0) ;
	dbClass.sqlout ((short  *) &lief.lief_typ,SQLSHORT,0 ) ;
	dbClass.sqlout ((short  *) &lief.lief_zeit,SQLSHORT,0 ) ;
	// 5 -> 6 Felder ( incl. lief.mdn !!! )
	dbClass.sqlout ((double *) &lief.lief_zusch, SQLDOUBLE,0 ) ;
	dbClass.sqlout ((long   *) &lief.lst_nr, SQLLONG, 0) ;
	dbClass.sqlout ((short  *) &lief.mahn_stu,SQLSHORT,0 ) ;
 	dbClass.sqlout ((short  *) &lief.mdn,SQLSHORT,0 ) ;
	dbClass.sqlout ((char   *)  lief.me_kz, SQLCHAR, 3 ) ;
	dbClass.sqlout ((char   *)  lief.min_me_kz, SQLCHAR, 3 ) ;
	// 6
	dbClass.sqlout ((double *) &lief.min_zusch, SQLDOUBLE,0 ) ;
	dbClass.sqlout ((double *) &lief.min_zusch_proz, SQLDOUBLE,0 ) ;
	dbClass.sqlout ((char   *)  lief.nach_lief, SQLCHAR, 3) ;
	dbClass.sqlout ((char   *)  lief.ordr_kz, SQLCHAR, 3) ;
	dbClass.sqlout ((char   *)  lief.rab_abl, SQLCHAR, 3) ;
	// 7
	dbClass.sqlout ((char   *)  lief.rab_kz,  SQLCHAR, 3) ;
	dbClass.sqlout ((char   *)  lief.rech_kz, SQLCHAR, 3) ;
	dbClass.sqlout ((double *) &lief.rech_toler, SQLDOUBLE,0 ) ;
	dbClass.sqlout ((short  *) &lief.sdr_nr,SQLSHORT,0 ) ;
	dbClass.sqlout ((long   *) &lief.son_kond, SQLLONG, 0) ;
	// 8
	dbClass.sqlout ((short  *) &lief.sprache,SQLSHORT,0 ) ;
	dbClass.sqlout ((char   *)  lief.steuer_kz, SQLCHAR, 3) ;
	dbClass.sqlout ((char   *)  lief.unt_lief_kz, SQLCHAR, 3) ;
	dbClass.sqlout ((char   *)  lief.vieh_bas, SQLCHAR, 3) ;
	dbClass.sqlout ((char   *)  lief.we_erf_kz, SQLCHAR, 3) ;
	// 9
	dbClass.sqlout ((char   *)  lief.we_kontr, SQLCHAR, 3) ;
	dbClass.sqlout ((char   *)  lief.we_pr_kz, SQLCHAR, 3) ;
	dbClass.sqlout ((double *) &lief.we_toler, SQLDOUBLE,0 ) ;
	dbClass.sqlout ((short  *) &lief.zahl_kond,SQLSHORT,0 ) ;
	dbClass.sqlout ((char   *)  lief.zahlw, SQLCHAR, 3) ;
	// 10
	dbClass.sqlout ((short  *) &lief.hdklpargr,SQLSHORT,0 ) ;
	dbClass.sqlout ((long   *) &lief.vorkgr, SQLLONG, 0) ;
	dbClass.sqlout ((short  *) &lief.delstatus,SQLSHORT,0 ) ;
	dbClass.sqlout ((short  *) &lief.mwst,SQLSHORT,0 ) ;
	dbClass.sqlout ((short  *) &lief.liefart,SQLSHORT,0 ) ;
	// 11
	dbClass.sqlout ((char   *)  lief.modif, SQLCHAR, 3) ;	
	dbClass.sqlout ((char   *)  lief.ust_id, SQLCHAR, 20) ;
	dbClass.sqlout ((short  *) &lief.eg_kz,SQLSHORT,0 ) ;
	dbClass.sqlout ((short  *) &lief.waehrung,SQLSHORT,0 ) ;
// IN-VARIABLE 	dbClass.sqlout ((char   *)  lief.iln, SQLCHAR, 20) ;
	dbClass.sqlout ((short  *) &lief.zertifikat1,SQLSHORT,0 ) ;
	// 12
	dbClass.sqlout ((short  *) &lief.zertifikat2,SQLSHORT,0 ) ;
	dbClass.sqlout ((char   *)  lief.eg_betriebsnr, SQLCHAR, 20) ;
	dbClass.sqlout ((char   *)  lief.iban1, SQLCHAR, 6) ;
	dbClass.sqlout ((char   *)  lief.iban2, SQLCHAR, 6) ;
	dbClass.sqlout ((char   *)  lief.iban3, SQLCHAR, 6) ;
	// 13
	dbClass.sqlout ((char   *)  lief.iban4, SQLCHAR, 6) ;
	dbClass.sqlout ((char   *)  lief.iban5, SQLCHAR, 6) ;
	dbClass.sqlout ((char   *)  lief.iban6, SQLCHAR, 6) ;
	dbClass.sqlout ((char   *)  lief.iban7, SQLCHAR, 6) ;
	dbClass.sqlout ((char   *)  lief.esnum, SQLCHAR, 12) ;
	// 14
	dbClass.sqlout ((char   *)  lief.eznum, SQLCHAR, 12) ;
	dbClass.sqlout ((short  *) &lief.geburt,SQLSHORT,0 ) ;
	dbClass.sqlout ((short  *) &lief.mast,SQLSHORT,0 ) ;
	dbClass.sqlout ((short  *) &lief.schlachtung,SQLSHORT,0 ) ;
	dbClass.sqlout ((short  *) &lief.zerlegung,SQLSHORT,0 ) ;

    readcursor = (short) dbClass.sqlcursor (
	" select abr ,adr ,bank_nam ,bbn ,best_sort " 
	" ,best_trans_kz ,blz ,bonus_kz ,fil ,fracht_kz "
	" ,ink ,kreditor ,kto ,letzt_lief ,lief "
	" ,lief_kun ,lief_rht ,lief_s ,lief_typ ,lief_zeit "
	" ,lief_zusch ,lst_nr ,mahn_stu , mdn, me_kz ,min_me_kz "
	" ,min_zusch ,min_zusch_proz ,nach_lief ,ordr_kz ,rab_abl "
	" ,rab_kz ,rech_kz ,rech_toler ,sdr_nr ,son_kond "
	" ,sprache ,steuer_kz ,unt_lief_kz ,vieh_bas ,we_erf_kz "
	" ,we_kontr ,we_pr_kz ,we_toler ,zahl_kond ,zahlw "
	" ,hdklpargr ,vorkgr ,delstatus ,mwst ,liefart "
	" ,modif ,ust_id ,eg_kz ,waehrung ,zertifikat1 "
	" ,zertifikat2 ,eg_betriebsnr ,iban1 ,iban2 ,iban3 "
	" ,iban4 ,iban5 ,iban6 ,iban7 ,esnum "
	" ,eznum ,geburt ,mast ,schlachtung ,zerlegung "

	"from lief where lief.iln = ?  and ( lief.mdn = ? or lief.mdn = 0 ) " 
	"	order by lief.mdn desc "	) ;

}


