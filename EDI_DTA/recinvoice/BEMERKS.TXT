
einige Voraussetzungen fuer recinvoice( normalerweise nur 1 Lieferant je Relation sinnvoll)
1. dta.iln == Lieferanten-ILN
2. empf-ILN == eigne (Mandanten-)ILN
3. Lieferant-ILN sinnvoll bestueckt, ansonsten wird dta.iln im SU-segment genutzt
4. Lief_bzg.ean MUSS bestueckt sein
5. Filial-ILN fix nach folgender Struktur :

4YXXXXFFFFFP , wobei  4YXXXX der Anfang der Mandanten-ILN
                 und FFFFF gleich der Filial-Nummer ist


dta.adr = lief.adr wegen Aufloesungsreferenz (ordsend)

(dta.adr ist das 2. Feld in der ersten Zeile der dta-Stamm-Pflege) 