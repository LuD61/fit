#pragma once

#include "afxwin.h"
#include "afxcmn.h"
#include "List2Ctrl.h"


// CAutoKonto-Dialogfeld

class CAutoKonto : public CDialog
{
	DECLARE_DYNAMIC(CAutoKonto)

public:
	CAutoKonto(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CAutoKonto();

// Dialogfelddaten
	enum { IDD = IDD_KONTOLISTE_DIALOG };

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	CList2Ctrl m_list2;

 	afx_msg BOOL PreTranslateMessage(LPMSG) ;
	virtual BOOL ReadList(void) ;
	virtual BOOL Write(void) ;
	virtual BOOL OnReturn (void);
	virtual BOOL OnKeyup (void);
//	virtual void DestroyRows(CVector &Rows);
	virtual BOOL InList (DATEV_KTO_CLASS &datev_kto_class) ;	// Input nur dummy ?!

	CVector DbRows;
	CVector ListRows;

	CFillList FillList ;

	afx_msg void OnBnClickedButf6();
	afx_msg void OnBnClickedButf7();
};
