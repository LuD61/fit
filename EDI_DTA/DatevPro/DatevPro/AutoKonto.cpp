// AutoKonto.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "DatevPro.h"

#include "dbclass.h"
#include "datev_kp.h"

#include "Vector.h"
#include "FillList.h"
#include "List2Ctrl.h"


#include "AutoKonto.h"


extern DB_CLASS dbClass;

// CAutoKonto-Dialogfeld

IMPLEMENT_DYNAMIC(CAutoKonto, CDialog)

CAutoKonto::CAutoKonto(CWnd* pParent /*=NULL*/)
	: CDialog(CAutoKonto::IDD, pParent)
{

}

CAutoKonto::~CAutoKonto()
{
}

void CAutoKonto::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST2, m_list2);
}


BEGIN_MESSAGE_MAP(CAutoKonto, CDialog)
	ON_BN_CLICKED(IDOK, &CAutoKonto::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CAutoKonto::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTF6, &CAutoKonto::OnBnClickedButf6)
	ON_BN_CLICKED(IDC_BUTF7, &CAutoKonto::OnBnClickedButf7)
END_MESSAGE_MAP()


// CAutoKonto-Meldungshandler



BOOL CAutoKonto::OnInitDialog()
{
	CDialog::OnInitDialog();


	// Hinzuf�gen des Men�befehls "Info..." zum Systemmen�.

	// IDM_ABOUTBOX muss sich im Bereich der Systembefehle befinden.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Symbol f�r dieses Dialogfeld festlegen. Wird automatisch erledigt
	//  wenn das Hauptfenster der Anwendung kein Dialogfeld ist
//	SetIcon(m_hIcon, TRUE);			// Gro�es Symbol verwenden
//	SetIcon(m_hIcon, FALSE);		// Kleines Symbol verwenden

	// TODO: Hier zus�tzliche Initialisierung einf�gen


	FillList = m_list2;
	FillList.SetStyle (LVS_REPORT);
	if (m_list2.GridLines)
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	}
	else
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT);
	}

	FillList.SetCol (_T(""), 0, 0);
	FillList.SetCol (_T("Konto-Nr."), 1, 100, LVCFMT_RIGHT);

	ReadList();

	return TRUE;

}

BOOL CAutoKonto::ReadList ()
{

  m_list2.DeleteAllItems ();
  m_list2.vSelect.clear ();
  int i = 0;

  memcpy (&datev_kto, &datev_kto_null, sizeof (struct DATEV_KTO));
 
  int sqlret = 100 ;

  datev_kto_class.openalldatev_kto () ;
  sqlret =  datev_kto_class.lesealldatev_kto() ;	
  if ( !sqlret )
  {
		while ( ! sqlret )
		{
		FillList.InsertItem (i, 0);
		CString eRLKTO;
		eRLKTO.Format (_T("%d"), datev_kto.erl_kto );
		FillList.SetItemText (eRLKTO.GetBuffer (), i, m_list2.PosErl_kto);
		
/* --->
		CString wERT;
		m_list1.DoubleToString (a_kun.a, wERT, 0);
		FillList.SetItemText (wERT.GetBuffer (), i, m_list1.PosArtNr);
< ---- */
		sqlret =  datev_kto_class.lesealldatev_kto() ;	
		i ++ ;
	}
  }
	return TRUE;
}

BOOL CAutoKonto::Write ()
{
	m_list2.StopEnter ();

	int count = m_list2.GetItemCount ();

	int retcode = datev_kto_class.deletedatev_kto() ;
	for (int i = 0; i < count; i ++)
	{

			CString Text;
			Text = m_list2.GetItemText (i, m_list2.PosErl_kto);
			datev_kto.erl_kto = atol( Text.GetBuffer());
			if ( datev_kto.erl_kto  > 0 )
			{
				retcode = datev_kto_class.opendatev_kto();
				retcode = datev_kto_class.lesedatev_kto();	// gegen doppelte Eintraege
				if ( retcode ) retcode = datev_kto_class.insertdatev_kto();
			}
	}

	ReadList();
/* --->
	if ( kun.kun > 0 )
		m_kundnr.SetFocus() ;
	else
		m_mdnnr.SetFocus() ;
< ---- */
	return TRUE;
}

BOOL CAutoKonto::InList (DATEV_KTO_CLASS & datev_kto_class)
{

	/* --->
	m_buttkey6.EnableWindow (TRUE) ;
	m_buttkey7.EnableWindow (TRUE) ;
	m_buttkey8.EnableWindow (TRUE) ;
	m_buttkey9.EnableWindow (TRUE) ;
	m_buttkey10.EnableWindow(TRUE) ;
	m_buttkey11.EnableWindow(TRUE) ;
< --- */
	ListRows.FirstPosition ();

   return FALSE;
}

BOOL CAutoKonto::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
//				keyset(TRUE);
				m_list2.OnKeyD (VK_RETURN);
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{

				if (GetFocus () != &m_list2 &&
					GetFocus ()->GetParent () != &m_list2 )
				{

					break;
			    }
//				keyset(TRUE);
				m_list2.OnKeyD (VK_TAB);
				return TRUE;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				else if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CDateTimeCtrl)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
// AAA OOOO					return FALSE ;
				}
//				keyset(TRUE);
				m_list2.OnKeyD (VK_DOWN);
				return TRUE;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				else if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CDateTimeCtrl)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
//				keyset(TRUE);
				m_list2.OnKeyD (VK_UP);
				return TRUE;
			}

			else if (pMsg->wParam == VK_F5)
			{
//				StepBack ();
				OnCancel () ;
				return TRUE;
			}

 			else if (pMsg->wParam == VK_F6)
			{

//				OnInsert ();
//				return TRUE;
				CWnd *Control = GetFocus ();
				if (Control == &m_list2 ||
					Control->GetParent ()== &m_list2 )
				{
					m_list2.OnKeyD (VK_F6);
					return TRUE;
				}

			}

 			else if (pMsg->wParam == VK_F7)
			{
				CWnd *Control = GetFocus ();
				if (Control == &m_list2 ||
					Control->GetParent ()== &m_list2 )
				{
					m_list2.OnKeyD (VK_F7);
					return TRUE;
				}
//				OnDelete ();
//				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Write ();
				return TRUE;
			}

/* ---->
			else if (pMsg->wParam == VK_F8)
			{
				** ---->
				if (Choice != NULL)
				{	if (Choice->IsWindowVisible ())
					{	Choice->ShowWindow (SW_HIDE);
					}
					else
					{	Choice->ShowWindow (SW_SHOWNORMAL);
					}
				}
				else
				{	OnChoice ();
				}
				< ----- **
				OnChoice();	// alternative zur echten selektion 
			}
			else if (pMsg->wParam == VK_F9)
			{	if (GetFocus () == &m_mdn)
				{	OnMdnchoice ();
					return TRUE;
				}
				if (GetFocus () == &m_schlklknr)
				{	OnChoice ();
					return TRUE;
				}

				m_List.OnKey9 ();
				return TRUE;
			}
< ----- */
	}
//return CDbPropertyPage::PreTranslateMessage(pMsg);
//return CFormView::PreTranslateMessage(pMsg);
//	return FALSE ;
return CDialog::PreTranslateMessage(pMsg);
}

BOOL CAutoKonto::OnReturn ()
{
	CWnd *Control = GetFocus ();


	if (Control != &m_list2 &&
		Control->GetParent ()!= &m_list2)
	{
			Control = GetNextDlgTabItem (Control, FALSE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}

	return FALSE;
}

BOOL CAutoKonto::OnKeyup ()
{
	CWnd *Control = GetFocus ();
	if (Control != &m_list2 &&
		Control->GetParent ()!= &m_list2 )
	{
			Control = GetNextDlgTabItem (Control, TRUE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}
	return FALSE;
}

void CAutoKonto::OnBnClickedOk()
{
	Write();
	OnOK();
}

void CAutoKonto::OnBnClickedCancel()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	OnCancel();
}

void CAutoKonto::OnBnClickedButf6()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
					m_list2.OnKeyD (VK_F6);
}

void CAutoKonto::OnBnClickedButf7()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
						m_list2.OnKeyD (VK_F7);

}
