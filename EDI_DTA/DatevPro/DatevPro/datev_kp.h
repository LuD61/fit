#ifndef _DATEV_KP_DEF
#define _DATEV_KP_DEF

struct DATEV_KTO {
    long erl_kto;
};

extern struct DATEV_KTO datev_kto, datev_kto_null;

class DATEV_KTO_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int lesedatev_kto (void);
               int lesealldatev_kto (void);
               int opendatev_kto (void);
               int openalldatev_kto (void);
			   int deletedatev_kto(void);
			   int insertdatev_kto(void);
			   DATEV_KTO_CLASS () : DB_CLASS ()
               {
               }
};

extern class DATEV_KTO_CLASS datev_kto_class ;

struct DATEV_K {
    short mdn;
    long lfd;
	char f01formkz[5];	// char(4),
    long f02version;
    short f03kategorie;
    char f04formname[37];	// char(36),
    long f05formversion;
    TIMESTAMP_STRUCT f06createtime;	// datetime year to second,
    TIMESTAMP_STRUCT f07importtime;	// datetime year to second,
    char f08herkunft[3];	//	char(2),
    char f09expuser [26];	//	char(25),
    char f10impuser [26];	//	char(25),
    long f11berater;
    long f12mandant;
    TIMESTAMP_STRUCT f13wjbeginn;	// date,
    short f14kontolen;
    TIMESTAMP_STRUCT f15vondat;		// date,
    TIMESTAMP_STRUCT f16bisdat;		// date,
    char f17bezeich[31];			// char(30),
    char f18dikkurz[3];				// char(2),
    short f19butyp;
    short f20relezweck;
    long f21res21;
    char f22wkz[4];				// char(3),
    long f23res23;
    long f24res24;
    long f25res25;
    long f26res26;
};

extern struct DATEV_K datev_k, datev_k_null;

class DATEV_K_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int lesedatev_k (void);
               int opendatev_k (void);
			   int updatedatev_k(void);
			   int insertdatev_k(void);
			   long lfdmaxdatev_k(void);
			   DATEV_K_CLASS () : DB_CLASS ()
               {
               }
};

extern class DATEV_K_CLASS datev_k_class ;


struct DATEV_P {

	short mdn;
    long lfd;
	long posi;
    double f001umsatz;	// decimal(12,2),
    char f002solhab[2];
    char f003wkzums[4];
    double f004kurs;	// decimal(10,6),
    double f005bumsatz;	// decimal(12,2),
    char f006wkzbums[4];
    long f007konto;
    long f008gegkonto;
    char f009buschl[3];
    TIMESTAMP_STRUCT f010rech_dat;
    char f011rech_nr1[13];
    char f012rech_nr2[13];
    double f013skonto;	// decimal(10,2),
    char f014butext[61];
    short f015psperre;
    char f016divadr[10];
    short f017partbank;
    char f018sachverhalt[3];
    short f019zsperre;
    char f020bellink[21];
    char f021bia1[21];
    char f022bii1 [211];
    char f023bia2[21];
    char f024bii2 [211];
    char f025bia3 [21];
    char f026bii3 [211];
    char f027bia4 [21];
    char f028bii4 [211];
    char f029bia5 [21];
    char f030bii5 [211];
    char f031bia6 [21];
    char f032bii6 [211];
    char f033bia7 [21];
    char f034bii7 [211];
    char f035bia8 [21];
    char f036bii8 [211];
    char f037kost1 [9];
    char f038kost2 [9];
    double f039kostm;	// decimal(11,2),
    char f040ustid [16];
    double f041euproz;	// decimal(4,2),
    char f042steuart [2];
    short f043sachll ;
    short f044fergll ;
    short f045bu49hft ;
    short f046bu49hfn ;
    short f047bu49fer ;
    char f048zinfa01 [21];
    char f049zinfi01 [211];
    char f050zinfa02 [21];
    char f051zinfi02 [211];
    char f052zinfa03 [21];
    char f053zinfi03 [211];
    char f054zinfa04 [21];
    char f055zinfi04 [211];
    char f056zinfa05 [21];
    char f057zinfi05 [211];
    char f058zinfa06 [21];
    char f059zinfi06 [211];
    char f060zinfa07 [21];
    char f061zinfi07 [211];
    char f062zinfa08 [21];
    char f063zinfi08 [211];
    char f064zinfa09 [21];
    char f065zinfi09 [211];
    char f066zinfa10 [21];
    char f067zinfi10 [211];
    char f068zinfa11 [21];
    char f069zinfi11 [211];
    char f070zinfa12 [21];
    char f071zinfi12 [211];
    char f072zinfa13 [21];
    char f073zinfi13 [211];
    char f074zinfa14 [21];
    char f075zinfi14 [211];
    char f076zinfa15 [21];
    char f077zinfi15 [211];
    char f078zinfa16 [21];
    char f079zinfi16 [211];
    char f080zinfa17 [21];
    char f081zinfi17 [211];
    char f082zinfa18 [21];
    char f083zinfi18 [211];
    char f084zinfa19 [21];
    char f085zinfi19 [211];
    char f086zinfa20 [21];
    char f087zinfi20 [211];
    long f088stueck ;
    double f089gewicht;	// decimal(10,2),
    short f090zahlwei ;
	char f091fordart [11];
    short f092verjahr ;
    TIMESTAMP_STRUCT f093faellig;
    short f094sktotyp ;
    char f095auf_num [31];
    char f096buchtyp [3];
	short f097ustschl ;
    char f098eustaat [3];
    short f099asachll ;
    double f100aeuust;	// decimal(4,2),
    long f101aerlkto ;
    char f102herkkz [3];
    char f103leerfeld [37];
    TIMESTAMP_STRUCT f104kostdat;
    char f105mandref [36];
  };

extern struct DATEV_P datev_p, datev_p_null;

class DATEV_P_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int opendatev_p (void);
			   int insertdatev_p(void);
			   int zuruecksetzen(void);	// 300914
			   DATEV_P_CLASS () : DB_CLASS ()
               {
               }
};

extern class DATEV_P_CLASS datev_p_class ;

#endif

