// DatevOption.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "DatevPro.h"
#include "DatevOption.h"
#include "dbclass.h"
#include "datev_kp.h"

extern char bufh[599] ;
extern BOOL datumcheck(char * bufi ) ;
extern char * SYSTEMDATUM ( TIMESTAMP_STRUCT * idat );
extern char * sqldatdstger(TIMESTAMP_STRUCT *dstakt, char *outpstr);

static int k_retcode = 0;

// DatevOption-Dialogfeld

IMPLEMENT_DYNAMIC(DatevOption, CDialog)

DatevOption::DatevOption(CWnd* pParent /*=NULL*/)
	: CDialog(DatevOption::IDD, pParent)
	, v_1formatkz(_T(""))
	, v_2version(_T(""))
	, v_3kategorie(_T(""))
	, v_4formatname(_T(""))
	, v_5formatversion(_T(""))
	, v_11berater(_T(""))
	, v_12mandant(_T(""))
	, v_13wjbeginn(_T(""))
	, v_14kontenlang(_T(""))
	, v_18diktatkz(_T(""))
{

}

DatevOption::~DatevOption()
{
}

void DatevOption::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_1FORMATKZ, m_1formatkz);
	DDX_Text(pDX, IDC_1FORMATKZ, v_1formatkz);
	DDX_Control(pDX, IDC_2VERSION, m_2version);
	DDX_Text(pDX, IDC_2VERSION, v_2version);
	DDX_Control(pDX, IDC_3KATEGORIE, m_3kategorie);
	DDX_Text(pDX, IDC_3KATEGORIE, v_3kategorie);
	DDX_Control(pDX, IDC_4FORMATNAME, m_4formatname);
	DDX_Text(pDX, IDC_4FORMATNAME, v_4formatname);
	DDX_Control(pDX, IDC_5FORMATVERSION, m_5formatversion);
	DDX_Text(pDX, IDC_5FORMATVERSION, v_5formatversion);
	DDX_Control(pDX, IDC_11BERATER, m_11berater);
	DDX_Text(pDX, IDC_11BERATER, v_11berater);
	DDX_Control(pDX, IDC_12MANDANT, m_12mandant);
	DDX_Text(pDX, IDC_12MANDANT, v_12mandant);
	DDX_Control(pDX, IDC_13WJBEGINN, m_13wjbeginn);
	DDX_Text(pDX, IDC_13WJBEGINN, v_13wjbeginn);
	DDX_Control(pDX, IDC_14KONTENLANG, m_14kontenlang);
	DDX_Text(pDX, IDC_14KONTENLANG, v_14kontenlang);
	DDX_Control(pDX, IDC_18DIKTATKZ, m_18diktatkz);
	DDX_Text(pDX, IDC_18DIKTATKZ, v_18diktatkz);
}


BEGIN_MESSAGE_MAP(DatevOption, CDialog)
	ON_BN_CLICKED(IDOK, &DatevOption::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &DatevOption::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTERW, &DatevOption::OnBnClickedButterw)
	ON_EN_KILLFOCUS(IDC_13WJBEGINN, &DatevOption::OnEnKillfocus13wjbeginn)
END_MESSAGE_MAP()


// DatevOption-Meldungshandler

BOOL DatevOption::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Hinzuf�gen des Men�befehls "Info..." zum Systemmen�.

	// IDM_ABOUTBOX muss sich im Bereich der Systembefehle befinden.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Symbol f�r dieses Dialogfeld festlegen. Wird automatisch erledigt
	//  wenn das Hauptfenster der Anwendung kein Dialogfeld ist
//	SetIcon(m_hIcon, TRUE);			// Gro�es Symbol verwenden
//	SetIcon(m_hIcon, FALSE);		// Kleines Symbol verwenden


	short savemdn = datev_k.mdn;
	long savelfd = datev_k.lfd;

	k_retcode = datev_k_class.opendatev_k();
	k_retcode = datev_k_class.lesedatev_k();

	if ( k_retcode ) 
	{		// urladen

		memcpy( &datev_k, &datev_k_null, sizeof(struct DATEV_K));
		datev_k.mdn = savemdn;
		datev_k.lfd = savelfd;
		sprintf( datev_k.f01formkz,"EXTF" );
		datev_k.f02version = 300;	// z.Z. ( am 29.08.2014)aktuelle Version
		datev_k.f03kategorie = 21;	// Buchungsstapel
		sprintf (datev_k.f04formname,"Buchungsstapel");	// genau dieses Wort
		datev_k.f05formversion = 4;	// aktuelle Version f�r "Buchungsstapel"
//		TIMESTAMP_STRUCT datev_k.f06createtime;	// datetime year to second,
//		TIMESTAMP_STRUCT datev_k.f07importtime;	// datetime year to second,
		sprintf ( datev_k.f08herkunft, "xx");	//	char(2),
		sprintf ( datev_k.f09expuser,"fitfaktura");	//	char(25),
		sprintf ( datev_k.f10impuser, "" );	//	setzt der im�port,
		datev_k.f11berater = 12345;
		datev_k.f12mandant = 23456;
		SYSTEMDATUM (&datev_k.f13wjbeginn);	// Wirtschaftsjahr
		datev_k.f14kontolen = 4;			// zulaessig 4 ... 8
		SYSTEMDATUM (&datev_k.f15vondat);		// date,
		SYSTEMDATUM (&datev_k.f16bisdat);		// date,
		sprintf (datev_k.f17bezeich,"Dummy-Bezeichnung");			// char(30),
		sprintf(datev_k.f18dikkurz,"KK");				// char(2),
		datev_k.f19butyp = -1;					// bei blank wird standardmaessig buchungsstapel gesetzt
		datev_k.f20relezweck = -1;				// 0 oder blank
		datev_k.f21res21 = -1;;
		sprintf(datev_k.f22wkz,"");				// blank wird intern zu "EUR" char(3),
		datev_k.f23res23 = -1;
		datev_k.f24res24 = -1;
		datev_k.f25res25 = -1;
		datev_k.f26res26 = -1;
	}

	v_1formatkz.Format  ("%s", datev_k.f01formkz);
	v_2version.Format   ("%d" ,datev_k.f02version);
	v_3kategorie.Format  ("%d", datev_k.f03kategorie);
	v_4formatname.Format ("%s", datev_k.f04formname);
	v_5formatversion.Format("%d", datev_k.f05formversion);
	v_11berater.Format("%d",datev_k.f11berater);
	v_12mandant.Format ("%d",datev_k.f12mandant);

	char hilfedat[15];
	v_13wjbeginn.Format ( "%s", sqldatdstger( &datev_k.f13wjbeginn,hilfedat));
	v_14kontenlang.Format("%d",datev_k.f14kontolen);
	v_18diktatkz.Format ( "%s",datev_k.f18dikkurz);


	m_1formatkz.SetReadOnly();
	m_1formatkz.ModifyStyle(WS_TABSTOP,0);

	m_2version.SetReadOnly();
	m_2version.ModifyStyle(WS_TABSTOP,0);

	m_3kategorie.SetReadOnly();
	m_3kategorie.ModifyStyle(WS_TABSTOP,0);

	m_4formatname.SetReadOnly();
	m_4formatname.ModifyStyle(WS_TABSTOP,0);

	m_5formatversion.SetReadOnly();
	m_5formatversion.ModifyStyle(WS_TABSTOP,0);
//	m_11berater;
//	m_12mandant;

//	m_13wjbeginn
	m_14kontenlang.SetReadOnly();
	m_14kontenlang.ModifyStyle(WS_TABSTOP,0);
//	m_18diktatkz


	UpdateData(FALSE);

	return TRUE;  // Geben Sie TRUE zur�ck, au�er ein Steuerelement soll den Fokus erhalten
}




void DatevOption::OnBnClickedOk()
{
	UpdateData(TRUE) ;
// Speichern der Eingaben
	
	
//		datev_k.mdn = savemdn;
//		datev_k.lfd = savelfd;
	v_1formatkz.Trim();
	sprintf ( bufh,"%s",v_1formatkz.GetBuffer());
	bufh[4] ='\0';
	sprintf ( datev_k.f01formkz,"%s",bufh);

	v_2version.Trim();
	sprintf ( bufh,"%s",v_2version.GetBuffer());
	bufh[8] ='\0';
	datev_k.f02version = atol (bufh);

	v_3kategorie.Trim();
	sprintf ( bufh,"%s",v_3kategorie.GetBuffer());
	bufh[8] ='\0';
	datev_k.f03kategorie = atoi (bufh);

	v_4formatname.Trim();
	sprintf ( bufh,"%s",v_4formatname.GetBuffer());
	bufh[30] ='\0';
	sprintf (datev_k.f04formname,"%s", bufh);	// genau dieses Wort

	v_5formatversion.Trim();
	sprintf ( bufh,"%s",v_5formatversion.GetBuffer());
	bufh[8] ='\0';
	datev_k.f05formversion = atol (bufh);

	v_11berater.Trim();
	sprintf ( bufh,"%s",v_11berater.GetBuffer());
	bufh[9] ='\0';
	datev_k.f11berater = atol (bufh);

	v_12mandant.Trim();
	sprintf ( bufh,"%s",v_12mandant.GetBuffer());
	bufh[9] ='\0';
	datev_k.f12mandant = atol (bufh);

	v_13wjbeginn.Trim();
	sprintf ( bufh,"%s",v_13wjbeginn.GetBuffer());	// sollte vorher bereits korrekt sein
	sqldatdst( &datev_k.f13wjbeginn, bufh);

	v_14kontenlang.Trim();
	sprintf ( bufh,"%s",v_14kontenlang.GetBuffer());
	bufh[8] ='\0';
	datev_k.f14kontolen = atoi (bufh);

	v_18diktatkz.Trim();
	sprintf ( bufh,"%s",v_18diktatkz.GetBuffer());
	bufh[3] ='\0';
	sprintf (datev_k.f18dikkurz,"%s", bufh);

	if ( k_retcode )
		k_retcode = datev_k_class.insertdatev_k();
	else
		datev_k_class.updatedatev_k();
	OnOK();
}

void DatevOption::OnBnClickedCancel()
{
	OnCancel();
}

void DatevOption::OnBnClickedButterw()
{
	UpdateData(TRUE);

	m_1formatkz.SetReadOnly(0);
	m_1formatkz.ModifyStyle(WS_TABSTOP,1);

	m_2version.SetReadOnly(0);
	m_2version.ModifyStyle(WS_TABSTOP,1);

	m_3kategorie.SetReadOnly(0);
	m_3kategorie.ModifyStyle(WS_TABSTOP,1);

	m_4formatname.SetReadOnly(0);
	m_4formatname.ModifyStyle(WS_TABSTOP,1);

	m_5formatversion.SetReadOnly(0);
	m_5formatversion.ModifyStyle(WS_TABSTOP,1);
//	m_11berater;
//	m_12mandant;

//	m_13wjbeginn
	m_14kontenlang.SetReadOnly(0);
	m_14kontenlang.ModifyStyle(WS_TABSTOP,1);
//	m_18diktatkz

	UpdateData(FALSE);
}
BOOL DatevOption::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				CWnd *xWnd ;
				xWnd = GetFocus ();
			           if(xWnd == GetDlgItem (IDOK)) break ;			// ok - eigentlich nichts machen
// auf den ok-Knopf verlegt ....			           if(xWnd == GetDlgItem (IDC_AUSDRUCK)) break ;	// Ausduck halt
			           if(xWnd == GetDlgItem (IDCANCEL))
					   {
							GetParent ()->DestroyWindow ();
							return TRUE ;	// Abbruch halt ( genau wie ButtonClickedCancel) 
					   }
	
				if (OnReturn ())
				{
					return TRUE;
				}
			}
			else if (pMsg->wParam == VK_TAB)
			{

				break;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (OnKeyup ())
				{
					return TRUE;
				}
			}
			else if (pMsg->wParam == VK_F5)
			{
//				StepBack ();
// ???				OnCancel () ;
				GetParent ()->DestroyWindow ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				return TRUE;
			}

	}

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL DatevOption::OnReturn ()
{
	CWnd *Control = GetFocus ();

	Control = GetNextDlgTabItem (Control, FALSE);

	if (Control != NULL)
	{
		Control->SetFocus ();
	}
	return TRUE;
}

BOOL DatevOption::OnKeyup ()
{
	CWnd *Control = GetFocus ();

	Control = GetNextDlgTabItem (Control, TRUE);
	if (Control != NULL)
	{
		Control->SetFocus ();
	}
	return TRUE;
}


void DatevOption::OnEnKillfocus13wjbeginn()
{

	UpdateData(TRUE) ;
	int	i = m_13wjbeginn.GetLine(0,bufh,500) ;
	bufh[i] = '\0';

	if (i)
	{
		if (datumcheck(bufh))
		{
			v_13wjbeginn.Format("%s",_T(bufh));
		}
		else
			v_13wjbeginn.Format("");
	}
	UpdateData (FALSE) ;
	return ;

}
