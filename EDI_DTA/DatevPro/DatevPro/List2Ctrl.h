#pragma once
#include <vector>
#include "editlistctrl.h"
#include "dbClass.h"
#include "datev_kp.h"
//#include "ChoiceMeEinh.h"

#define MAXLISTROWS 30

class CList2Ctrl :
	public CEditListCtrl
{
protected:
	DECLARE_MESSAGE_MAP()
public:

	enum AUFSCHLAG
	{
		NO	     = 0,
		BUTTON   = 1,
		LIST     = 2,
		ALL      = 3,
	};

/* --->
	enum MODE
	{
		STANDARD = 0,
		TERMIN   = 1,
	};
< ---- */
	enum LISTPOS
	{
		POSERL_KTO	= 1,
	};

	int PosErl_kto;
 

    int *Position[2];



	int Mode;
	int MaxComboEntries;
	BOOL ActiveListRow;
//	short m_mdn;
//	short mdn;

	std::vector<BOOL> vSelect;
//	CVector MeEinhCombo ;
//	CVector PrGrStufCombo;
//	CVector KunPrCombo;
//	CChoiceKun *ChoiceKun;
//	BOOL ModalChoiceKun;
//	BOOL KunChoiceStat;
//	CChoiceIKunPr *ChoiceIKunPr;
//	BOOL ModalChoiceIKunPr;
//	BOOL IKunPrChoiceStat;
//	CChoiceMeEinh *ChoiceMeEinh;
//	BOOL ModalChoiceMeEinh;
//	BOOL MeEinhChoiceStat;
	CVector ListRows;

	CList2Ctrl(void);
	~CList2Ctrl(void);
	virtual void FirstEnter ();
	virtual void StartEnter (int, int);
	virtual void StopEnter ();
	virtual void OnReturn ();
	virtual void NextRow ();
	virtual void PriorRow ();
	virtual void NextCol ();
	virtual void PriorCol ();
	virtual void FormatText (CString&);
	virtual void SetSel (CString&);
	virtual BOOL InsertRow ();
	virtual BOOL DeleteRow ();
	virtual BOOL AppendEmpty ();
//	virtual void RunItemClicked (int);
	virtual void RunCtrlItemClicked (int);
	virtual void RunShiftItemClicked (int);
	virtual void HiLightItem (int);
//	void FillMeEinhCombo( CVector&);
//	void FillPrGrStufCombo (CVector&);
//	void FillKunPrCombo (CVector&);
	void OnChoice ();
//	void OnKunChoice (CString &);
//    void OnIKunPrChoice (CString& Search);
//    void OnPrGrStufChoice (CString& Search);
//	void OnKey9 ();
    BOOL ReadErlKto ();
//    void ReadKunPr ();
//    void ReadPrGrStuf ();
    void GetColValue (int row, int col, CString& Text);
//    void TestIprIndex ();
	void ScrollPositions (int pos);
	BOOL LastCol ();
};
