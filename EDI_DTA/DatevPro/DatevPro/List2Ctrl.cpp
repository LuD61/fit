#include "StdAfx.h"
#include "List2Ctrl.h"
#include "StrFuncs.h"
#include "resource.h"
#include "AutoKonto.h"
#include "dbClass.h"
#include "datev_kp.h"

CList2Ctrl::CList2Ctrl(void)
{
	PosErl_kto		= POSERL_KTO;

	Position[0] = &PosErl_kto;
	Position[1] = NULL;

	MaxComboEntries = 50;

	ListRows.Init ();
}

CList2Ctrl::~CList2Ctrl(void)
{
/* ----- >
	CString *c;
    MeEinhCombo.FirstPosition ();
	while ((c = (CString *) MeEinhCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	MeEinhCombo.Init ();
< ---- */
}

BEGIN_MESSAGE_MAP(CList2Ctrl, CEditListCtrl)
	ON_BN_CLICKED(SEARCHBUTTON ,  OnChoice)
END_MESSAGE_MAP()


void CList2Ctrl::FirstEnter ()
{

	if (GetItemCount () > 0)
	{
		StartEnter (PosErl_kto, 0);
	}
	else
	{
		if (!AppendEmpty ()) return; 
		StartEnter (PosErl_kto, 0);
	}

}

void CList2Ctrl::StartEnter (int col, int row)
{
	if (col == 0) col = 1;
//	if (col == PosArtBez) return;
	if ( col == PosErl_kto )
	{
		CString cErlKto = GetItemText (row, PosErl_kto);
		if (StrToDouble (cErlKto) < 1  )	// Konto nicht gefunden/vorhanden 
		{
//			return ;
		}
		CEditListCtrl::StartEnter (col, row);

	}


	CEditListCtrl::StartEnter (col, row);
/* ---->
	if (col == PosKun_me_einh)
	{
		if (IsWindow (ListComboBox.m_hWnd))
		{
			StopEnter ();
		}

//		CString zUBASIS ;
//		zUBASIS =  FillList.GetItemText (row, PosKun_me_einh);	// == col //
		
//		int nCurSel = ListComboBox.FindString( 0,zUBASIS.GetBuffer(0));

		CEditListCtrl::StartEnterCombo (col, row, &MeEinhCombo);

//		if (nCurSel > 0)
//			ListComboBox.SetCurSel( nCurSel ) ;
//		else
//			ListComboBox.SetCurSel( 0 ) ;
//		oldsel = ListComboBox.GetCurSel ();
		return ;
	}
< ---- */
/* ---->
	if (col == PosZuschlag || col == PosKost_bez)
	{
		CEditListCtrl::StartEnter (col, row);
	}
< ----- */
}

void CList2Ctrl::StopEnter ()
{


	CEditListCtrl::StopEnter ();

	if (IsWindow (ListEdit.m_hWnd))
	{
		CString Text;
		ListEdit.GetWindowText (Text);
		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		ListEdit.DestroyWindow ();
	}
	else if (IsWindow (ListComboBox.m_hWnd))
	{
/* --->
		CString Text;
		int idx = ListComboBox.GetCurSel ();
		if (idx < 0) 
		{
			Text = _T("");
		}
		if (ListComboBox.GetCount () > 0)
		{
			ListComboBox.GetLBText (idx, Text);
			FormatText (Text);
			FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		}
		ListComboBox.DestroyWindow ();
< ---- */
	}
/* --->
	else if (IsWindow (ListCheckBox.m_hWnd))
	{
		int check = ListCheckBox.GetCheck ();
        if (check == BST_CHECKED)
		{
			FillList.SetItemText (_T("X"), EditRow, EditCol);
		}
		else
		{
			FillList.SetItemText (_T(" "), EditRow, EditCol);
		}
		ListCheckBox.DestroyWindow ();
	}
< ---- */

	/* ---->
	else if (IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		int count = GetItemCount ();
		CString Text;
		SearchListCtrl.Edit.GetWindowText (Text);
		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		SearchListCtrl.DestroyWindow ();
	}
	< ----- */
}

void CList2Ctrl::SetSel (CString& Text)
{

   if (EditCol == PosErl_kto )
   {
		ListEdit.SetSel (0, -1);
   }
/* ----->
   else
   {
		Text.TrimRight ();
		int cpos = Text.GetLength ();
//		ListEdit.SetSel (cpos, cpos);
   }
< ------ */
   }

void CList2Ctrl::FormatText (CString& Text)
{

	if (EditCol == PosErl_kto)
	{
		DoubleToString (StrToDouble (Text), Text, 0);
	}

}

void CList2Ctrl::NextRow ()
{
//	CString Zuschlag = GetItemText (EditRow, PosZuschlag);
//	double 	dZuschlag = StrToDouble (Zuschlag) ;

	int count = GetItemCount ();

	BOOL ob = TRUE ;
	if (EditCol == PosErl_kto)
	{
		CString cErlKto = GetItemText (EditRow, PosErl_kto);
		if (StrToDouble (cErlKto) < 1  )	// Konto nicht gefunden/vorhanden 
			ob = FALSE;
	}

	SetEditText ();

	count = GetItemCount ();
	if ( ob == TRUE )
	{
		if (EditRow >= count - 1)
		{
			if (AppendEmpty () == FALSE)
			{
				return;
			}
			StopEnter ();
			EditRow ++;
			EditCol = 0;

		}
		else
		{
			StopEnter ();
			EditRow ++;
		}
	}
	else
	{	// Auf Schluessel stehen bleiben
		EditCol = PosErl_kto ;
	}
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);

	if (GetKeyState (VK_SHIFT) < 0)
	{
		BOOL &b = vSelect[EditRow - 1];
		b = TRUE;
		FillList.SetItemImage (EditRow - 1, 1);
	}
	else if (GetKeyState (VK_CONTROL) == 0)
	{
		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			BOOL& b = vSelect[i];
			b = FALSE;
			FillList.SetItemImage (i,0);
		}
	}
}

void CList2Ctrl::PriorRow ()
{
	if (EditRow <= 0)
	{
			return;
	}
	int count = GetItemCount ();

	BOOL ob = TRUE ;

	if (EditCol == PosErl_kto)
	{
		ob = FALSE ;
		if (IsWindow (ListEdit.m_hWnd))
		{
			CString Text;
			ListEdit.GetWindowText (Text);
			if (atol (Text.GetBuffer()) == 0l)
			{
				DeleteRow() ;
			}
		}
		else
			EditCol = PosErl_kto ;
	}

	if (EditRow == count - 1)
	{
/* --->
		CString Zuschlag = GetItemText (EditRow, PosZuschlag);
		CString Kost_bez = GetItemText (EditRow, PosKost_bez);
		if ((StrToDouble (Zuschlag) == 0.0)
			&& (_tcslen (Kost_bez.Trim()) == 0 ))
		{
	        DeleteItem (EditRow);
		}
< ---- */
	}
	else
	{
		if (EditRow <= 0)
		{
			return;
		}
/* ---->
		if (EditCol == PosKun)
		{
			ReadKunName ();
		}
		else if (EditCol == PosKunPr)
		{
			ReadKunPr ();
		}
		else if (EditCol == PosPrGrStuf)
		{
			ReadPrGrStuf ();
		}
		TestIprIndex ();
< ---- */
	}
	StopEnter ();
 
	EditRow --;
	EnsureVisible (EditRow, FALSE);
	if ( ob == FALSE )
		EditCol = PosErl_kto ;

	StartEnter (EditCol, EditRow);
	for (int i = 0; i < (int) vSelect.size (); i ++)
	{
		BOOL& b = vSelect[i];
		b = FALSE;
		FillList.SetItemImage (i,0);
	}
}


BOOL CList2Ctrl::LastCol ()
{

	if (EditCol < PosErl_kto) return FALSE;

	return TRUE;
}

void CList2Ctrl::OnReturn ()
{
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	int rowCount = GetItemCount ();
	if (EditCol >= colCount - 1 &&
		EditRow >= rowCount - 1)
	{
/* --->
		CString Zuschlag = GetItemText (EditRow, PosZuschlag);
		CString Kost_bez = GetItemText (EditRow, PosKost_bez);
		if ((StrToDouble (Zuschlag) == 0.0 ) 
			&&	(_tcslen(Kost_bez.Trim())== 0 ))
		{
			EditCol --;
			return;
		}
< ----- */

	}

	if (LastCol ())
	{
//		TestIprIndex ();
	    rowCount = GetItemCount ();
		if (EditRow >= rowCount - 1)
		{
			if (AppendEmpty () == FALSE)
			{
				return;
			}


		}
	    StopEnter ();
		CString cErlKto = GetItemText (EditRow, PosErl_kto);
		if (StrToDouble (cErlKto) >0  )	// Konto  gefunden/vorhanden 

		EditRow ++;
		EditCol = 0;
		if (EditRow == rowCount)
		{
			EditCol = PosErl_kto;
		}
		else
		{
			EditCol = PosErl_kto;
		}
	}
	else
	{
	    StopEnter ();
		CString cErlKto = GetItemText (EditRow, PosErl_kto);
		if (StrToDouble (cErlKto) > 0  )	// Konto gefunden/vorhanden 
		EditCol ++;
/* ---->
		if (EditCol == PosArtBez)
		{
			EditCol ++;
		}
		if (EditCol == PosKante)
		{
			EditCol ++;
		}
< --- */
	}
	EnsureColVisible (EditCol);
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
}

void CList2Ctrl::NextCol ()
{
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol >= count - 1)
	{
		return;
	}


	StopEnter ();
	EditCol ++;
	EnsureColVisible (EditCol);
    StartEnter (EditCol, EditRow);
}

void CList2Ctrl::PriorCol ()
{
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol <= 1)
	{
		return;
	}

	StopEnter ();
	EditCol --;
    StartEnter (EditCol, EditRow);
}

BOOL CList2Ctrl::InsertRow ()
{
//	return FALSE ;
/* ---->
	CString Zuschlag = GetItemText (EditRow, PosZuschlag);
	CString Kost_bez = GetItemText (EditRow, PosKost_bez);
	if ((GetItemCount () > 0) && 
		(StrToDouble (Zuschlag) == 0.0) &&
		 (_tcslen(Kost_bez) == 0))
	{
		return FALSE;
	}
< ---- */
	StopEnter ();
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	FillList.InsertItem (EditRow, -1);
		FillList.SetItemText (" ", EditRow, PosErl_kto);
//		FillList.SetItemText (" ", EditRow, PosArtBez);

	StartEnter (0, EditRow);
	return TRUE;
}

BOOL CList2Ctrl::DeleteRow ()
{
	if (!IsWindow (m_hWnd)) return FALSE;
//	return FALSE ;
	return CEditListCtrl::DeleteRow ();
}

BOOL CList2Ctrl::AppendEmpty ()
{
	int rowCount = GetItemCount ();

	if (rowCount > 0)
	{
/* --->
		CString cArtNr = GetItemText (EditRow + 1 , PosArtNr);
		double hilfe = StrToDouble (cArtNr) ;
		if (hilfe < 1 )	// Bereits neue Zeile angelegt
		{
			return FALSE;
		}
< ---- */

	}

	int colCount = GetHeaderCtrl ()->GetItemCount ();
	FillList.InsertItem (rowCount, -1);
		FillList.SetItemText (" ", rowCount, PosErl_kto);
		
	rowCount = GetItemCount ();
	return TRUE;
}

void CList2Ctrl::HiLightItem (int Item)
{

}

/* ---->
void CList1Ctrl::FillMeEinhCombo (CVector& Values)
{
	CString *c;
    MeEinhCombo.FirstPosition ();
	while ((c = (CString *) MeEinhCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	MeEinhCombo.Init ();
	Values.FirstPosition ();
	while ((c = (CString *) Values.GetNext ()) != NULL)
	{
		MeEinhCombo.Add (c);
	}
}
< ---- */


void CList2Ctrl::RunCtrlItemClicked (int Item)
{
/*
	    if (Item == -1) return;
		BOOL& b = vSelect[Item];
		b = TRUE;
		FillList.SetItemImage (Item,1);
*/
}

void CList2Ctrl::RunShiftItemClicked (int Item)
{
/*
	    int start;
		int end;
        int row = -1;

	    if (Item == -1) return;
		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			BOOL& b = vSelect[i];
			if (b)
			{
				row = i;
				break;
			}
		}

		if (row == -1)
		{
			RunItemClicked (Item);
			return;
		}


		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			BOOL& b = vSelect[i];
			b = FALSE;
			FillList.SetItemImage (i,0);
		}

		if (row < Item)
		{
			start = row;
			end = Item;
		}
		else
		{
			start = Item;
			end   = row;
		}
		for (int i = start; i <= end; i ++)
		{
		    BOOL& b = vSelect[i];
			b = TRUE;
			FillList.SetItemImage (i,1);
		}
*/
}

void CList2Ctrl::OnChoice ()
{
	/* -> 
	if (EditCol == PosKun)
	{
		OnKunChoice (CString (_T("")));
	}
	else if (EditCol == PosKunPr)
	{
		OnIKunPrChoice (CString (_T("")));
	}
	else if (EditCol == PosPrGrStuf)
	{
		OnPrGrStufChoice (CString (_T("")));
	}
	< ----- */
}


/* --->
void CList1Ctrl::OnKey9 ()
{
	if (IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		OnChoice ();
	}
}
< ---- */


void CList2Ctrl::GetColValue (int row, int col, CString& Text)
{
}


void CList2Ctrl::ScrollPositions (int pos)
{
	*Position[pos] = -1;
	for (int i = pos + 1; Position[i] != NULL; i ++)
	{
		*Position[i] -= 1;
	}
}


