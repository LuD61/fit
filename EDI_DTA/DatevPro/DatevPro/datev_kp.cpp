#include "stdafx.h"
#include "DbClass.h"
#include "datev_kp.h"

extern DB_CLASS dbClass;

struct DATEV_KTO datev_kto, datev_kto_null ;
struct DATEV_K datev_k, datev_k_null ;
struct DATEV_P datev_p, datev_p_null ;


DATEV_KTO_CLASS  datev_kto_class ;
DATEV_K_CLASS  datev_k_class ;
DATEV_P_CLASS  datev_p_class ;



int DATEV_KTO_CLASS::openalldatev_kto (void)
{

		if ( readcursor < 0 ) prepare ();
         return dbClass.sqlopen (readcursor);
}


int DATEV_KTO_CLASS::lesealldatev_kto (void)
{
      int di = dbClass.sqlfetch (readcursor);
	  return di;
}
int DATEV_KTO_CLASS::opendatev_kto (void)
{

		if ( upd_cursor < 0 ) prepare ();
         return dbClass.sqlopen (upd_cursor);
}


int DATEV_KTO_CLASS::lesedatev_kto (void)
{
	long erlsave ;
	erlsave = datev_kto.erl_kto ;
	int di = dbClass.sqlfetch (upd_cursor);
	datev_kto.erl_kto = erlsave;
	return di;
}

int DATEV_KTO_CLASS::deletedatev_kto (void)
{
	if ( del_cursor < 0 ) prepare ();
      int di = dbClass.sqlexecute (del_cursor);
	  return di;
}
int DATEV_KTO_CLASS::insertdatev_kto (void)
{
	if ( ins_cursor < 0 ) prepare ();
      int di = dbClass.sqlexecute (ins_cursor);
	  return di;
}

void DATEV_KTO_CLASS::prepare (void)
{

// readcursor : lese alle Saetze  

    dbClass.sqlout ((long   *) &datev_kto.erl_kto, SQLLONG , 0 );
	readcursor = (short)dbClass.sqlcursor ("select "
	" erl_kto from datev_kto order by erl_kto "
	) ;

// upd_cursor : lese genau diesen Satz  

    dbClass.sqlin ((long   *) &datev_kto.erl_kto, SQLLONG , 0 );
    dbClass.sqlout ((long   *) &datev_kto.erl_kto, SQLLONG , 0 );

	upd_cursor = (short)dbClass.sqlcursor ("select "
	" erl_kto from datev_kto where erl_kto = ?  "
	) ;

// ins_cursor : inserte genau diesen Satz  

    dbClass.sqlin ((long   *) &datev_kto.erl_kto, SQLLONG , 0 );

	ins_cursor = (short)dbClass.sqlcursor ( "insert into datev_kto "
		" ( erl_kto ) values ( ? )  "
	) ;

// del_cursor : delete alles ( und baue dann neu auf )  

	del_cursor = (short)dbClass.sqlcursor ( "delete from  datev_kto "
		" where erl_kto > -100 "
	) ;


}


long maxlfd;


int DATEV_K_CLASS::lesedatev_k (void)
{
      int di = dbClass.sqlfetch (readcursor);

	  return di;
}

int DATEV_K_CLASS::opendatev_k (void)
{

		if ( readcursor < 0 ) prepare ();
         return dbClass.sqlopen (readcursor);
}

int DATEV_K_CLASS::updatedatev_k (void)
{
		if ( readcursor < 0 ) prepare ();
         return dbClass.sqlexecute (upd_cursor);
}

int DATEV_K_CLASS::insertdatev_k (void)
{
		if ( readcursor < 0 ) prepare ();
         return dbClass.sqlexecute (ins_cursor);
}

long DATEV_K_CLASS::lfdmaxdatev_k (void)
{
		if ( readcursor < 0 ) prepare ();
		maxlfd = 0;
      int di = dbClass.sqlopen (test_upd_cursor);
      di = dbClass.sqlfetch (test_upd_cursor);
	 if (di) maxlfd = -1 ;	// di <>0  kann eigentlich nur ein Fehler sein
	  return maxlfd ;
}

void DATEV_K_CLASS::prepare (void)
{
							

// readcursor : lese DEN -1-Satz oder den max-Satz oder was sonst so gebraucht wird ......  

	dbClass.sqlin ((short  *) &datev_k.mdn , SQLSHORT , 0 ) ;
    dbClass.sqlin ((long   *) &datev_k.lfd, SQLLONG , 0 );

	dbClass.sqlout ((char  *)  datev_k.f01formkz, SQLCHAR , 5);
	dbClass.sqlout ((long  *) &datev_k.f02version, SQLLONG , 0 );
	dbClass.sqlout ((short *) &datev_k.f03kategorie, SQLSHORT , 0 );
	dbClass.sqlout ((char  *)  datev_k.f04formname, SQLCHAR ,37 );
    dbClass.sqlout ((long  *) &datev_k.f05formversion, SQLLONG , 0 );

	dbClass.sqlout ((TIMESTAMP_STRUCT *) &datev_k.f06createtime , SQLTIMESTAMP , 26 );	// datetime year to second,
    dbClass.sqlout ((TIMESTAMP_STRUCT *) &datev_k.f07importtime , SQLTIMESTAMP , 26 );	// datetime year to second,
    dbClass.sqlout ((char  *)  datev_k.f08herkunft, SQLCHAR , 3);	//	char(2),
    dbClass.sqlout ((char  *)  datev_k.f09expuser, SQLCHAR , 26);	//	char(25),
    dbClass.sqlout ((char  *)  datev_k.f10impuser, SQLCHAR , 26);	//	char(25),

	dbClass.sqlout ((long  *) &datev_k.f11berater, SQLLONG , 0 );
    dbClass.sqlout ((long  *) &datev_k.f12mandant, SQLLONG , 0 );
    dbClass.sqlout ((TIMESTAMP_STRUCT *) &datev_k.f13wjbeginn , SQLTIMESTAMP , 26 );	// date,
    dbClass.sqlout ((short *) &datev_k.f14kontolen, SQLSHORT , 0 );
    dbClass.sqlout ((TIMESTAMP_STRUCT *) &datev_k.f15vondat , SQLTIMESTAMP , 26 );		// date,

	dbClass.sqlout ((TIMESTAMP_STRUCT *) &datev_k.f16bisdat , SQLTIMESTAMP , 26 );		// date,
    dbClass.sqlout ((char  *)  datev_k.f17bezeich, SQLCHAR , 31);			// char(30),
    dbClass.sqlout ((char  *)  datev_k.f18dikkurz, SQLCHAR , 3);				// char(2),
    dbClass.sqlout ((short *) &datev_k.f19butyp, SQLSHORT , 0 );
    dbClass.sqlout ((short *) &datev_k.f20relezweck, SQLSHORT , 0 );

	dbClass.sqlout ((long  *) &datev_k.f21res21, SQLLONG , 0);
    dbClass.sqlout ((char  *)  datev_k.f22wkz, SQLCHAR , 4);				// char(3),
    dbClass.sqlout ((long  *) &datev_k.f23res23, SQLLONG , 0);
    dbClass.sqlout ((long  *) &datev_k.f24res24, SQLLONG , 0);
    dbClass.sqlout ((long  *) &datev_k.f25res25, SQLLONG , 0);

	dbClass.sqlout ((long  *) &datev_k.f26res26, SQLLONG , 0);

	readcursor = (short)dbClass.sqlcursor ("select "

	" f01formkz, f02version, f03kategorie, f04formname, f05formversion "
	" , f06createtime, f07importtime, f08herkunft, f09expuser, f10impuser "
	" , f11berater, f12mandant, f13wjbeginn, f14kontolen, f15vondat "
	" , f16bisdat, f17bezeich, f18dikkurz, f19butyp, f20relezweck "
	" , f21res21, f22wkz, datev_k.f23res23, f24res24, f25res25 "
	" , f26res26 "

	" from datev_k  where mdn = ? and lfd= ? "  
	) ;

// ins_cursor :   

	dbClass.sqlin ((short  *) &datev_k.mdn , SQLSHORT , 0 ) ;
    dbClass.sqlin ((long   *) &datev_k.lfd, SQLLONG , 0 );

	dbClass.sqlin ((char   *)  datev_k.f01formkz, SQLCHAR , 5);
	dbClass.sqlin ((long   *) &datev_k.f02version, SQLLONG , 0 );
	dbClass.sqlin ((short  *) &datev_k.f03kategorie, SQLSHORT , 0 );
	dbClass.sqlin ((char   *)  datev_k.f04formname, SQLCHAR ,37 );
    dbClass.sqlin ((long   *) &datev_k.f05formversion, SQLLONG , 0 );

//	dbClass.sqlin ((TIMESTAMP_STRUCT *) &datev_k.f06createtime , SQLTIMESTAMP , 26 );	// datetime year to second,
//  dbClass.sqlin ((TIMESTAMP_STRUCT *) &datev_k.f07importtime , SQLTIMESTAMP , 26 );	// datetime year to second,
    dbClass.sqlin ((char   *)  datev_k.f08herkunft, SQLCHAR , 3);	//	char(2),
    dbClass.sqlin ((char   *)  datev_k.f09expuser, SQLCHAR , 26);	//	char(25),
    dbClass.sqlin ((char   *)  datev_k.f10impuser, SQLCHAR , 26);	//	char(25),

	dbClass.sqlin ((long   *) &datev_k.f11berater, SQLLONG , 0 );
    dbClass.sqlin ((long   *) &datev_k.f12mandant, SQLLONG , 0 );
    dbClass.sqlin ((TIMESTAMP_STRUCT *) &datev_k.f13wjbeginn , SQLTIMESTAMP , 26 );	// date,
    dbClass.sqlin ((short  *) &datev_k.f14kontolen, SQLSHORT , 0 );
    dbClass.sqlin ((TIMESTAMP_STRUCT *) &datev_k.f15vondat , SQLTIMESTAMP , 26 );		// date,

	dbClass.sqlin ((TIMESTAMP_STRUCT *) &datev_k.f16bisdat , SQLTIMESTAMP , 26 );		// date,
    dbClass.sqlin ((char   *)  datev_k.f17bezeich, SQLCHAR , 31);			// char(30),
    dbClass.sqlin ((char   *)  datev_k.f18dikkurz, SQLCHAR , 3);				// char(2),
    dbClass.sqlin ((short  *) &datev_k.f19butyp, SQLSHORT , 0 );
    dbClass.sqlin ((short  *) &datev_k.f20relezweck, SQLSHORT , 0 );

	dbClass.sqlin ((long   *) &datev_k.f21res21, SQLLONG , 0);
    dbClass.sqlin ((char   *)  datev_k.f22wkz, SQLCHAR , 4);				// char(3),
    dbClass.sqlin ((long   *) &datev_k.f23res23, SQLLONG , 0);
    dbClass.sqlin ((long   *) &datev_k.f24res24, SQLLONG , 0);
    dbClass.sqlin ((long   *) &datev_k.f25res25, SQLLONG , 0);

	dbClass.sqlin ((long   *) &datev_k.f26res26, SQLLONG , 0);


	ins_cursor = (short)dbClass.sqlcursor (" insert into datev_k ( "
	" mdn , lfd "
	" , f01formkz, f02version, f03kategorie, f04formname, f05formversion "
	" , f06createtime, f07importtime, f08herkunft, f09expuser, f10impuser "
	" , f11berater, f12mandant, f13wjbeginn, f14kontolen, f15vondat "
	" , f16bisdat, f17bezeich, f18dikkurz, f19butyp, f20relezweck "
	" , f21res21, f22wkz, f23res23, f24res24, f25res25 "
	" , f26res26 "
	" ) values ( "
	" ?,? "
	" , ?, ?, ?, ?, ?  "
	" , current, current, ?, ?, ?  "
	" , ?, ?, ?, ?, ?  "
	" , ?, ?, ?, ?, ?  "
	" , ?, ?, ?, ?, ?  "
	" , ? )  "
	);


// upd_cursor :   

//	dbClass.sqlin ((short  *) &datev_k.mdn , SQLSHORT , 0 ) ;
//	dbClass.sqlin ((long   *) &datev_k.lfd, SQLLONG , 0 );

	dbClass.sqlin ((char   *)  datev_k.f01formkz, SQLCHAR , 5);
	dbClass.sqlin ((long   *) &datev_k.f02version, SQLLONG , 0 );
	dbClass.sqlin ((short  *) &datev_k.f03kategorie, SQLSHORT , 0 );
	dbClass.sqlin ((char   *)  datev_k.f04formname, SQLCHAR ,37 );
    dbClass.sqlin ((long   *) &datev_k.f05formversion, SQLLONG , 0 );

//	dbClass.sqlin ((TIMESTAMP_STRUCT *) &datev_k.f06createtime , SQLTIMESTAMP , 26 );	// datetime year to second,
//  dbClass.sqlin ((TIMESTAMP_STRUCT *) &datev_k.f07importtime , SQLTIMESTAMP , 26 );	// datetime year to second,
    dbClass.sqlin ((char  *)  datev_k.f08herkunft, SQLCHAR , 3);	//	char(2),
    dbClass.sqlin ((char  *)  datev_k.f09expuser, SQLCHAR , 26);	//	char(25),
    dbClass.sqlin ((char  *)  datev_k.f10impuser, SQLCHAR , 26);	//	char(25),

	dbClass.sqlin ((long  *) &datev_k.f11berater, SQLLONG , 0 );
    dbClass.sqlin ((long  *) &datev_k.f12mandant, SQLLONG , 0 );
    dbClass.sqlin ((TIMESTAMP_STRUCT *) &datev_k.f13wjbeginn , SQLTIMESTAMP , 26 );	// date,
    dbClass.sqlin ((short *) &datev_k.f14kontolen, SQLSHORT , 0 );
    dbClass.sqlin ((TIMESTAMP_STRUCT *) &datev_k.f15vondat , SQLTIMESTAMP , 26 );		// date,

	dbClass.sqlin ((TIMESTAMP_STRUCT *) &datev_k.f16bisdat , SQLTIMESTAMP , 26 );		// date,
    dbClass.sqlin ((char  *)  datev_k.f17bezeich, SQLCHAR , 31);			// char(30),
    dbClass.sqlin ((char  *)  datev_k.f18dikkurz, SQLCHAR , 3);				// char(2),
    dbClass.sqlin ((short *) &datev_k.f19butyp, SQLSHORT , 0 );
    dbClass.sqlin ((short *) &datev_k.f20relezweck, SQLSHORT , 0 );

	dbClass.sqlin ((long  *) &datev_k.f21res21, SQLLONG , 0);
    dbClass.sqlin ((char  *)  datev_k.f22wkz, SQLCHAR , 4);				// char(3),
    dbClass.sqlin ((long  *) &datev_k.f23res23, SQLLONG , 0);
    dbClass.sqlin ((long  *) &datev_k.f24res24, SQLLONG , 0);
    dbClass.sqlin ((long  *) &datev_k.f25res25, SQLLONG , 0);

	dbClass.sqlin ((long  *) &datev_k.f26res26, SQLLONG , 0);

	dbClass.sqlin ((short *) &datev_k.mdn , SQLSHORT , 0 ) ;
	dbClass.sqlin ((long  *) &datev_k.lfd, SQLLONG , 0 );

	upd_cursor = (short)dbClass.sqlcursor (" update  datev_k  set "
	"  f01formkz = ?, f02version= ?, f03kategorie= ?, f04formname= ?, f05formversion = ? "
	" , f06createtime = current, f07importtime = current, f08herkunft= ?, f09expuser= ?, f10impuser = ? "
	" , f11berater= ?, f12mandant= ?, f13wjbeginn= ?, f14kontolen= ?, f15vondat= ? "
	" , f16bisdat= ?, f17bezeich= ?, f18dikkurz= ?, f19butyp= ?, f20relezweck= ? "
	" , f21res21= ?, f22wkz= ?, f23res23= ?, f24res24= ?, f25res25= ? "
	" , f26res26= ? "
	" where mdn = ? and lfd = ? "
	);

	dbClass.sqlout ((long   *) &maxlfd, SQLLONG , 0 );
	dbClass.sqlin ((short  *) &datev_k.mdn , SQLSHORT , 0 ) ;

	test_upd_cursor = (short)dbClass.sqlcursor (" select max(lfd) from datev_k where mdn = ? " );

}

int DATEV_P_CLASS::insertdatev_p (void)
{
		if ( ins_cursor < 0 ) prepare ();
         return dbClass.sqlexecute (ins_cursor);
}

int DATEV_P_CLASS::zuruecksetzen (void)
{
		if ( test_upd_cursor < 0 ) prepare ();
         return dbClass.sqlexecute (test_upd_cursor);
}


void DATEV_P_CLASS::prepare (void)
{
	dbClass.sqlin ((short  *) &datev_p.mdn, SQLSHORT,0);
 	dbClass.sqlin ((long   *) &datev_p.lfd , SQLLONG,0);
	dbClass.sqlin ((long   *) &datev_p.posi , SQLLONG,0);
	dbClass.sqlin ((double *) &datev_p.f001umsatz , SQLDOUBLE,0);	// decimal(12,2),
   	dbClass.sqlin ((char   *)  datev_p.f002solhab, SQLCHAR ,2);
   	dbClass.sqlin ((char   *)  datev_p.f003wkzums, SQLCHAR ,4);
   	dbClass.sqlin ((double *) &datev_p.f004kurs , SQLDOUBLE,0);	// decimal(10,6),
   	dbClass.sqlin ((double *) &datev_p.f005bumsatz , SQLDOUBLE,0);	// decimal(12,2),
   	dbClass.sqlin ((char   *)  datev_p.f006wkzbums, SQLCHAR ,4);
   	dbClass.sqlin ((long   *) &datev_p.f007konto , SQLLONG,0);
   	dbClass.sqlin ((long   *) &datev_p.f008gegkonto , SQLLONG,0);
   	dbClass.sqlin ((char   *)  datev_p.f009buschl, SQLCHAR ,3);
   	dbClass.sqlin ((TIMESTAMP_STRUCT  *) &datev_p.f010rech_dat , SQLTIMESTAMP , 26 );
   	dbClass.sqlin ((char   *)  datev_p.f011rech_nr1, SQLCHAR ,13);
   	dbClass.sqlin ((char   *)  datev_p.f012rech_nr2, SQLCHAR ,13);
   	dbClass.sqlin ((double *) &datev_p.f013skonto , SQLDOUBLE,0);	// decimal(10,2),
   	dbClass.sqlin ((char   *)  datev_p.f014butext, SQLCHAR ,61);
   	dbClass.sqlin ((short  *) &datev_p.f015psperre, SQLSHORT,0);
   	dbClass.sqlin ((char   *)  datev_p.f016divadr, SQLCHAR ,10);
   	dbClass.sqlin ((short  *) &datev_p.f017partbank, SQLSHORT,0);
   	dbClass.sqlin ((char   *)  datev_p.f018sachverhalt, SQLCHAR ,3);
   	dbClass.sqlin ((short  *) &datev_p.f019zsperre, SQLSHORT,0);
   	dbClass.sqlin ((char   *)  datev_p.f020bellink, SQLCHAR ,21);
   	dbClass.sqlin ((char   *)  datev_p.f021bia1, SQLCHAR ,21);
   	dbClass.sqlin ((char   *)  datev_p.f022bii1 , SQLCHAR ,211);
   	dbClass.sqlin ((char   *)  datev_p.f023bia2, SQLCHAR ,21);
   	dbClass.sqlin ((char   *)  datev_p.f024bii2 , SQLCHAR ,211);
   	dbClass.sqlin ((char   *)  datev_p.f025bia3 , SQLCHAR ,21);
   	dbClass.sqlin ((char   *)  datev_p.f026bii3 , SQLCHAR ,211);
   	dbClass.sqlin ((char   *)  datev_p.f027bia4 , SQLCHAR ,21);
   	dbClass.sqlin ((char   *)  datev_p.f028bii4 , SQLCHAR ,211);
   	dbClass.sqlin ((char   *)  datev_p.f029bia5 , SQLCHAR ,21);
   	dbClass.sqlin ((char   *)  datev_p.f030bii5 , SQLCHAR ,211);
   	dbClass.sqlin ((char   *)  datev_p.f031bia6 , SQLCHAR ,21);
   	dbClass.sqlin ((char   *)  datev_p.f032bii6 , SQLCHAR ,211);
   	dbClass.sqlin ((char   *)  datev_p.f033bia7 , SQLCHAR ,21);
   	dbClass.sqlin ((char   *)  datev_p.f034bii7 , SQLCHAR ,211);
   	dbClass.sqlin ((char   *)  datev_p.f035bia8 , SQLCHAR ,21);
   	dbClass.sqlin ((char   *)  datev_p.f036bii8 , SQLCHAR ,211);
   	dbClass.sqlin ((char   *)  datev_p.f037kost1 , SQLCHAR ,9);
   	dbClass.sqlin ((char   *)  datev_p.f038kost2 , SQLCHAR ,9);
   	dbClass.sqlin ((double *) &datev_p.f039kostm , SQLDOUBLE,0);	// decimal(11,2),
   	dbClass.sqlin ((char   *)  datev_p.f040ustid , SQLCHAR ,16);
   	dbClass.sqlin ((double *) &datev_p.f041euproz , SQLDOUBLE,0);	// decimal(4,2),
   	dbClass.sqlin ((char   *)  datev_p.f042steuart , SQLCHAR ,2);
   	dbClass.sqlin ((short  *) &datev_p.f043sachll , SQLSHORT,0);
   	dbClass.sqlin ((short  *) &datev_p.f044fergll , SQLSHORT,0);
   	dbClass.sqlin ((short  *) &datev_p.f045bu49hft , SQLSHORT,0);
   	dbClass.sqlin ((short  *) &datev_p.f046bu49hfn , SQLSHORT,0);
   	dbClass.sqlin ((short  *) &datev_p.f047bu49fer , SQLSHORT,0);
   	dbClass.sqlin ((char   *)  datev_p.f048zinfa01 , SQLCHAR ,21);
   	dbClass.sqlin ((char   *)  datev_p.f049zinfi01 , SQLCHAR ,211);
   	dbClass.sqlin ((char   *)  datev_p.f050zinfa02 , SQLCHAR ,21);
   	dbClass.sqlin ((char   *)  datev_p.f051zinfi02 , SQLCHAR ,211);
   	dbClass.sqlin ((char   *)  datev_p.f052zinfa03 , SQLCHAR ,21);
   	dbClass.sqlin ((char   *)  datev_p.f053zinfi03 , SQLCHAR ,211);
   	dbClass.sqlin ((char   *)  datev_p.f054zinfa04 , SQLCHAR ,21);
   	dbClass.sqlin ((char   *)  datev_p.f055zinfi04 , SQLCHAR ,211);
   	dbClass.sqlin ((char   *)  datev_p.f056zinfa05 , SQLCHAR ,21);
   	dbClass.sqlin ((char   *)  datev_p.f057zinfi05 , SQLCHAR ,211);
   	dbClass.sqlin ((char   *)  datev_p.f058zinfa06 , SQLCHAR ,21);
   	dbClass.sqlin ((char   *)  datev_p.f059zinfi06 , SQLCHAR ,211);
   	dbClass.sqlin ((char   *)  datev_p.f060zinfa07 , SQLCHAR ,21);
   	dbClass.sqlin ((char   *)  datev_p.f061zinfi07 , SQLCHAR ,211);
   	dbClass.sqlin ((char   *)  datev_p.f062zinfa08 , SQLCHAR ,21);
   	dbClass.sqlin ((char   *)  datev_p.f063zinfi08 , SQLCHAR ,211);
   	dbClass.sqlin ((char   *)  datev_p.f064zinfa09 , SQLCHAR ,21);
   	dbClass.sqlin ((char   *)  datev_p.f065zinfi09 , SQLCHAR ,211);
   	dbClass.sqlin ((char   *)  datev_p.f066zinfa10 , SQLCHAR ,21);
   	dbClass.sqlin ((char   *)  datev_p.f067zinfi10 , SQLCHAR ,211);
   	dbClass.sqlin ((char   *)  datev_p.f068zinfa11 , SQLCHAR ,21);
   	dbClass.sqlin ((char   *)  datev_p.f069zinfi11 , SQLCHAR ,211);
   	dbClass.sqlin ((char   *)  datev_p.f070zinfa12 , SQLCHAR ,21);
   	dbClass.sqlin ((char   *)  datev_p.f071zinfi12 , SQLCHAR ,211);
   	dbClass.sqlin ((char   *)  datev_p.f072zinfa13 , SQLCHAR ,21);
   	dbClass.sqlin ((char   *)  datev_p.f073zinfi13 , SQLCHAR ,211);
   	dbClass.sqlin ((char   *)  datev_p.f074zinfa14 , SQLCHAR ,21);
   	dbClass.sqlin ((char   *)  datev_p.f075zinfi14 , SQLCHAR ,211);
   	dbClass.sqlin ((char   *)  datev_p.f076zinfa15 , SQLCHAR ,21);
   	dbClass.sqlin ((char   *)  datev_p.f077zinfi15 , SQLCHAR ,211);
   	dbClass.sqlin ((char   *)  datev_p.f078zinfa16 , SQLCHAR ,21);
   	dbClass.sqlin ((char   *)  datev_p.f079zinfi16 , SQLCHAR ,211);
   	dbClass.sqlin ((char   *)  datev_p.f080zinfa17 , SQLCHAR ,21);
   	dbClass.sqlin ((char   *)  datev_p.f081zinfi17 , SQLCHAR ,211);
   	dbClass.sqlin ((char   *)  datev_p.f082zinfa18 , SQLCHAR ,21);
   	dbClass.sqlin ((char   *)  datev_p.f083zinfi18 , SQLCHAR ,211);
   	dbClass.sqlin ((char   *)  datev_p.f084zinfa19 , SQLCHAR ,21);
   	dbClass.sqlin ((char   *)  datev_p.f085zinfi19 , SQLCHAR ,211);
   	dbClass.sqlin ((char   *)  datev_p.f086zinfa20 , SQLCHAR ,21);
   	dbClass.sqlin ((char   *)  datev_p.f087zinfi20 , SQLCHAR ,211);
   	dbClass.sqlin ((long   *) &datev_p.f088stueck , SQLLONG,0);
   	dbClass.sqlin ((double *) &datev_p.f089gewicht , SQLDOUBLE,0);	// decimal(10,2),
   	dbClass.sqlin ((short  *) &datev_p.f090zahlwei , SQLSHORT,0);
	dbClass.sqlin ((char   *)  datev_p.f091fordart , SQLCHAR ,11);
   	dbClass.sqlin ((short  *) &datev_p.f092verjahr , SQLSHORT,0);
   	dbClass.sqlin ((TIMESTAMP_STRUCT  *) &datev_p.f093faellig , SQLTIMESTAMP , 26 );
   	dbClass.sqlin ((short  *) &datev_p.f094sktotyp , SQLSHORT,0);
   	dbClass.sqlin ((char   *)  datev_p.f095auf_num , SQLCHAR ,31);
   	dbClass.sqlin ((char   *)  datev_p.f096buchtyp , SQLCHAR ,3);
	dbClass.sqlin ((short  *) &datev_p.f097ustschl , SQLSHORT,0);
   	dbClass.sqlin ((char   *) &datev_p.f098eustaat , SQLCHAR ,3);
   	dbClass.sqlin ((short  *) &datev_p.f099asachll , SQLSHORT,0);
   	dbClass.sqlin ((double *) &datev_p.f100aeuust , SQLDOUBLE,0);	// decimal(4,2),
   	dbClass.sqlin ((long   *) &datev_p.f101aerlkto , SQLLONG,0);
   	dbClass.sqlin ((char   *)  datev_p.f102herkkz , SQLCHAR ,3);
   	dbClass.sqlin ((char   *)  datev_p.f103leerfeld , SQLCHAR ,37);
   	dbClass.sqlin ((TIMESTAMP_STRUCT  *) &datev_p.f104kostdat , SQLTIMESTAMP , 26 );
   	dbClass.sqlin ((char   *)  datev_p.f105mandref , SQLCHAR ,36);

	ins_cursor = (short)dbClass.sqlcursor (" insert into datev_p ( "
	" mdn, lfd, posi "
    " , f001umsatz, f002solhab, f003wkzums, f004kurs, f005bumsatz "
    " , f006wkzbums, f007konto, f008gegkonto, f009buschl, f010rech_dat "
	" , f011rech_nr1, f012rech_nr2, f013skonto, f014butext, f015psperre "
    " , f016divadr, f017partbank, f018sachverhalt, f019zsperre, f020bellink "
    " , f021bia1, f022bii1, f023bia2, f024bii2, f025bia3 "
    " , f026bii3, f027bia4, f028bii4, f029bia5, f030bii5 "
	" , f031bia6, f032bii6, f033bia7, f034bii7, f035bia8 "
	" , f036bii8, f037kost1, f038kost2, f039kostm, f040ustid "
	" , f041euproz, f042steuart, f043sachll, f044fergll, f045bu49hft "
	" , f046bu49hfn, f047bu49fer, f048zinfa01, f049zinfi01, f050zinfa02 "
	" , f051zinfi02, f052zinfa03, f053zinfi03, f054zinfa04, f055zinfi04 "
	" , f056zinfa05, f057zinfi05, f058zinfa06, f059zinfi06, f060zinfa07 "
	" , f061zinfi07, f062zinfa08, f063zinfi08, f064zinfa09, f065zinfi09 "
	" , f066zinfa10, f067zinfi10, f068zinfa11, f069zinfi11, f070zinfa12 "
    " , f071zinfi12, f072zinfa13, f073zinfi13, f074zinfa14, f075zinfi14 "
	" , f076zinfa15, f077zinfi15, f078zinfa16, f079zinfi16, f080zinfa17 "
	" , f081zinfi17, f082zinfa18, f083zinfi18, f084zinfa19, f085zinfi19 "
	" , f086zinfa20, f087zinfi20, f088stueck, f089gewicht, f090zahlwei "
	" , f091fordart, f092verjahr, f093faellig, f094sktotyp, f095auf_num "
	" , f096buchtyp, f097ustschl, f098eustaat, f099asachll, f100aeuust "
	" , f101aerlkto, f102herkkz, f103leerfeld, f104kostdat, f105mandref "
	" ) values ( "
	" ?, ?, ? "
	" , ?, ?, ?, ?, ? " 
	" , ?, ?, ?, ?, ? " 
	" , ?, ?, ?, ?, ? " 
	" , ?, ?, ?, ?, ? " 
	" , ?, ?, ?, ?, ? " 
	" , ?, ?, ?, ?, ? " 
	" , ?, ?, ?, ?, ? " 
	" , ?, ?, ?, ?, ? " 
	" , ?, ?, ?, ?, ? " 
	" , ?, ?, ?, ?, ? " 
	" , ?, ?, ?, ?, ? " 
	" , ?, ?, ?, ?, ? " 
	" , ?, ?, ?, ?, ? " 
	" , ?, ?, ?, ?, ? " 
	" , ?, ?, ?, ?, ? " 
	" , ?, ?, ?, ?, ? " 
	" , ?, ?, ?, ?, ? " 
	" , ?, ?, ?, ?, ? " 
	" , ?, ?, ?, ?, ? " 
	" , ?, ?, ?, ?, ? " 
	" , ?, ?, ?, ?, ? " 
	" ) "
	);

	test_upd_cursor = (short)dbClass.sqlcursor (" update fibu set buch_dat = \"03.03.1933\" "
		"where fibu.mdn = 1 and rech_nr in "
	"  ( select  f011rech_nr1 "
	"	from datev_p where datev_p.lfd = (select max(lfd) from datev_k) " 
	" and f010rech_dat = fibu.rech_dat ) "
	);

}
