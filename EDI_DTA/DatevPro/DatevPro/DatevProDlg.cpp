// DatevProDlg.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "DatevPro.h"
#include "DatevProDlg.h"
#include "DatevOption.h"

#include "AutoKonto.h"

#include "dbclass.h"
#include "mdn.h"
#include "adr.h"
#include "fibu.h"
#include "datev_kp.h"
#include "kun.h"
#include "sys_par.h"

DB_CLASS dbClass ;	// Hier ist die Heimat der dbClass, alle Tabellen sind in ihrem jeweiligen Modul daheim


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


char bufh[599] ;

BOOL datumcheck(char * bufi )
{
	int i = (int) strlen(bufi) ;
	int hil1, hil2 ;
	int k = 0 ;	// 261007 : Zusatzchecks
	BOOL j = FALSE ;

	char hil5 [5] ;
	if ( i != 6 && i != 8 && i != 10 )
	{
		// 261007 : Zusatzchecks wegen vista, nur noch bedingtes return
		if ( i < 6 ) k = -1 ;
		if ( k == 0 )
		{
			// die ersten beiden Zeichen MUESSEN ZAHLEN sein
			if ( bufi[0] > '9' || bufi[0] < '0' ||bufi[1] > '9' || bufi[1] < '0' )
				k = - 1 ;
		}
		if ( k == 0 )
		{
			if ( bufi[2] > '9' || bufi[2] < '0' )
				k = 2 ;		// Typ 8 oder typ 10 moeglich

		}
		if ( k == 0 )
		{
			// nur noch Typ 6 erlaubt 
			if (    bufi[3] > '9' || bufi[3] < '0'
				 || bufi[4] > '9' || bufi[4] < '0'
				 || bufi[5] > '9' || bufi[5] < '0' )
			{
				k = - 1 ;
			}
			else
			{	// Gueltiger Typ 6  ......
				k = 1 ;
				i = 6 ;
				bufi[6] = '\0' ;
			}
		}
		if ( k == 2 )
		{
			if ( i < 8 )
			{	
				k = -1 ;
			}
			else
			{	// 4.,5.,7.,8. Zeichen MUESSEN ZAHLEN sein, 6.Zeichen MUSS Nicht-Zahl sein
				if (   bufi[3] > '9' || bufi[3] < '0' 
					 ||bufi[4] > '9' || bufi[4] < '0'
				     ||bufi[6] > '9' || bufi[6] < '0'
				     ||bufi[7] > '9' || bufi[7] < '0'
				     || !( bufi[5] >'9' || bufi[5]< '0')
				    )
				k = - 1 ;
			}
		}
		if ( k == 2 )
		{
			if ( bufi[8] > '9' || bufi[8] < '0' )
			{	// gueltiger Typ 8 
				k = 1 ;
				i = 8 ;
				bufi[8] = '\0' ;
			}
		
		}

		if ( k == 2 )
		{
			if ( i < 10 )
			{	
				k = -1 ;
			}
			else
			{	// 9.,10. Zeichen MUESSEN ZAHLEN sein
				if (   bufi[8] > '9' || bufi[8] < '0' 
					 ||bufi[9] > '9' || bufi[9] < '0'
			       )
				{
					k = -1 ;
				}
				else
				{
					k = 1 ;
					i = 10 ;
					bufi[10] = '\0' ;
				}
			}
		}
		if ( k < 1 )
		{
			bufi[0] = '\0' ;
			return TRUE ;
		}
	}
	if ( i == 6 )	// nur ddmmyy erlaubt 
	{
		bufi[10] = '\0' ;
		bufi[ 9] = bufi[5] ;
		bufi[ 8] = bufi[4] ;
		if ( bufi[ 8] > '5' )
		{
			bufi[ 6] = '1' ;
			bufi[ 7] = '9' ;
		}
		else
		{
			bufi[ 6] = '2' ;
			bufi[ 7] = '0' ;
		}
		bufi[ 5] = '.' ;
		bufi[ 4] = bufi[3] ;
		bufi[ 3] = bufi[2] ;
		bufi[ 2] = '.' ;
		i = 10 ;
		j = TRUE ; 
	}
	if ( i == 8 )	// nur dd,mm,yy erlaubt 
	{
		bufi[10] = '\0' ;
		bufi[ 9] = bufi[7] ;
		bufi[ 8] = bufi[6] ;
		if ( bufi[ 8] > '5' )
		{
			bufi[ 6] = '1' ;
			bufi[ 7] = '9' ;
		}
		else
		{
			bufi[ 6] = '2' ;
			bufi[ 7] = '0' ;
		}
		bufi[ 5] = '.' ;
		bufi[ 2] = '.' ;
		i = 10 ;
		j = TRUE ;
	}
	if ( i == 10 )	// hier kommt nix anderes mehr an ....  
	{
		if (   bufi[0] > '9' || bufi[0] < '0' 
			|| bufi[1] > '9' || bufi[1] < '0'
			|| bufi[3] > '9' || bufi[3] < '0'
			|| bufi[4] > '9' || bufi[4] < '0'
			|| bufi[6] > '9' || bufi[6] < '0'
			|| bufi[7] > '9' || bufi[7] < '0'
			|| bufi[8] > '9' || bufi[8] < '0'
			|| bufi[9] > '9' || bufi[9] < '0'
			)
		{

			bufi[0] = '\0' ;
			return TRUE ;
		}
		else
			j = TRUE ;

		hil5[0] = bufi[3] ;
		hil5[1] = bufi[4] ;
		hil5[2] = '\0' ;
		hil1 = atoi (hil5);
		if ( hil1 > 12 || hil1 < 1 )
		{
			bufi[0] = '\0' ;
			return TRUE ;
		}

		hil5[0] = bufi[0] ;
		hil5[1] = bufi[1] ;
		hil5[2] = '\0' ;
		hil2 = atoi (hil5);
		if ( hil2 > 31 || hil1 < 1 )
		{
			bufi[0] = '\0' ;
			return TRUE ;
		}
		switch ( hil1 )
		{
		case  4 :
		case  6 :
		case  9 :
		case 11 :
			if ( hil2 > 30 )
			{
				bufi[0] = '\0' ;
				return TRUE ;
			}
			break ;
		case 2 :

			hil5[0] = bufi[6] ;
			hil5[1] = bufi[7] ;
			hil5[2] = bufi[8] ;
			hil5[3] = bufi[9] ;
			hil5[4] = '\0' ;
			hil1    = atoi(hil5) ;
			if ( hil2 > 28 )	// IM Jahr 2000 geht dieser Check schief .....
			{
				if ( ! ( hil1 % 4) )
				{
					if ( hil2 > 29 )
					{
						bufi[0] = '\0' ;
						return TRUE ;
					}
				}
				else
				{
					bufi[0] = '\0' ;
					return TRUE ;
				};
			}
			break ;

		}

	}
	return j ;

}

// Input ist einerseits eine Datumsstrukur, die aktualisiert wird und ausserdem wird ein
// deutscher Datumsstring mit aktuellem Datum erzeugt

char systemdat[33] ;
char * SYSTEMDATUM ( TIMESTAMP_STRUCT * idat )
{
	time_t timer;
		struct tm *ltime;
		time (&timer);
 		ltime = localtime (&timer);

		idat->day = ltime->tm_mday ;
		idat->month = ltime->tm_mon + 1 ;
		idat->year = ltime->tm_year + 1900 ;
		idat->hour = 0 ;
		idat->minute = 0 ;
		idat->second = 0 ;
		idat->fraction = 0 ;

		sprintf ( systemdat , "%02d.%02d.%04d",
			idat->day, idat->month, idat->year );

	return systemdat ;
}


DWORD ProcWaitExec (LPSTR prog, WORD SHOW_MODE, int x, int y, int cx, int cy)
/**
Fenster-Process starten und nicht auf Ende Warten.
**/
{
        STARTUPINFO sti;
        PROCESS_INFORMATION pi;
        DWORD ExitCode; 
        int ret;

        ZeroMemory (&sti, sizeof (sti));
        sti.cb = sizeof (sti);
        if (x != -1)
        {
                     sti.dwFlags |= STARTF_USEPOSITION;
                     sti.dwX = x;
                     sti.dwY = y;
        }

        if (cx != -1)
        {
                     sti.dwFlags |= STARTF_USESIZE;
                     sti.dwXSize = cx;
                     sti.dwYSize = cy;
        }
        sti.wShowWindow = SHOW_MODE;
        sti.dwFlags |= STARTF_USESHOWWINDOW;

        ret = CreateProcess (NULL,
                       prog,
                       NULL,
                       NULL,
                      (int) NULL,
                      (unsigned long) NULL,
                       NULL,
                       NULL,
                       &sti,
                       &pi);
        if (ret)
        {
                  CloseHandle (pi.hThread);
                  WaitForSingleObject (pi.hProcess, INFINITE);
        }
		else
        {
			      return -1;
        }				  
		GetExitCodeProcess (pi.hProcess, &ExitCode);
		while (ExitCode == STILL_ACTIVE)
		{
                  GetExitCodeProcess (pi.hProcess, &ExitCode);
		}
        CloseHandle (pi.hProcess);
        return ExitCode;
}






// CAboutDlg-Dialogfeld f�r Anwendungsbefehl "Info"

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialogfelddaten
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterst�tzung

// Implementierung
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CDatevProDlg-Dialogfeld



CDatevProDlg::CDatevProDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDatevProDlg::IDD, pParent)
	, v_mdnnr(_T(""))
	, v_mdnname(_T(""))
	, v_vdatum(_T(""))
	, v_bisdatum(_T(""))
	, v_beschreib(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CDatevProDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_MDNNR, m_mdnnr);
	DDX_Text(pDX, IDC_MDNNR, v_mdnnr);
	DDX_Control(pDX, IDC_MDNNAME, m_mdnname);
	DDX_Text(pDX, IDC_MDNNAME, v_mdnname);
	DDX_Control(pDX, IDC_VDATUM, m_vdatum);
	DDX_Text(pDX, IDC_VDATUM, v_vdatum);
	DDX_Control(pDX, IDC_BISDATUM, m_bisdatum);
	DDX_Text(pDX, IDC_BISDATUM, v_bisdatum);
	DDX_Control(pDX, IDC_BESCHREIB, m_beschreib);
	DDX_Text(pDX, IDC_BESCHREIB, v_beschreib);
}

BEGIN_MESSAGE_MAP(CDatevProDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_EN_KILLFOCUS(IDC_MDNNR, &CDatevProDlg::OnEnKillfocusMdnnr)
	ON_EN_KILLFOCUS(IDC_VDATUM, &CDatevProDlg::OnEnKillfocusVdatum)
	ON_EN_KILLFOCUS(IDC_BISDATUM, &CDatevProDlg::OnEnKillfocusBisdatum)
	ON_BN_CLICKED(IDOK, &CDatevProDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CDatevProDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTOPTION, &CDatevProDlg::OnBnClickedButoption)
	ON_BN_CLICKED(IDC_BUTAUTOKONTO, &CDatevProDlg::OnBnClickedButautokonto)
	ON_BN_CLICKED(IDC_BUTNOCHMAL, &CDatevProDlg::OnBnClickedButnochmal)
END_MESSAGE_MAP()


void CDatevProDlg::ReadMdn (void) 
{
	mdn_class.openmdn();
	if (! mdn_class.lesemdn())
	{
		adr.adr = mdn.adr ;
		int i = adr_class.openadr () ;
		i = adr_class.leseadr () ;
		if (!i) 
		{
			v_mdnname.Format("%s",_T(adr.adr_krz));
		}
		else
		{
			v_mdnname.Format("              ");
			MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
//	gibbet nich		PrevDlgCtrl();
		}
	}
	else	// fehlerhafte Eingabe
	{
		v_mdnname.Format("              ");
		MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
// gibbet nich 		PrevDlgCtrl();
	}
		UpdateData (FALSE) ;
}


// CDatevProDlg-Meldungshandler

BOOL CDatevProDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Hinzuf�gen des Men�befehls "Info..." zum Systemmen�.

	// IDM_ABOUTBOX muss sich im Bereich der Systembefehle befinden.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Symbol f�r dieses Dialogfeld festlegen. Wird automatisch erledigt
	//  wenn das Hauptfenster der Anwendung kein Dialogfeld ist
	SetIcon(m_hIcon, TRUE);			// Gro�es Symbol verwenden
	SetIcon(m_hIcon, FALSE);		// Kleines Symbol verwenden

	// TODO: Hier zus�tzliche Initialisierung einf�gen

	dbClass.opendbase (_T("bws"));

	mdn.mdn = 1 ;
	v_mdnnr = "1" ;

	v_vdatum.Format("%s",SYSTEMDATUM(&fibu.vrech_dat)) ; 
	v_bisdatum.Format("%s",SYSTEMDATUM(&fibu.brech_dat)) ; 

 	ReadMdn ();

// Testausgabe : MessageBox(globnutzer, globnutzer, MB_OK|MB_ICONSTOP);


	return TRUE;  // Geben Sie TRUE zur�ck, au�er ein Steuerelement soll den Fokus erhalten
}

void CDatevProDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// Wenn Sie dem Dialogfeld eine Schaltfl�che "Minimieren" hinzuf�gen, ben�tigen Sie 
//  den nachstehenden Code, um das Symbol zu zeichnen. F�r MFC-Anwendungen, die das 
//  Dokument/Ansicht-Modell verwenden, wird dies automatisch ausgef�hrt.

void CDatevProDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Ger�tekontext zum Zeichnen

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Symbol in Clientrechteck zentrieren
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Symbol zeichnen
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// Die System ruft diese Funktion auf, um den Cursor abzufragen, der angezeigt wird, w�hrend der Benutzer
//  das minimierte Fenster mit der Maus zieht.
HCURSOR CDatevProDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CDatevProDlg::OnEnKillfocusMdnnr()
{
	UpdateData (TRUE) ;
	int i = m_mdnnr.GetLine(0,bufh,500);
	bufh[i] = '\0' ;
	if (i)	mdn.mdn = (short) atoi ( bufh );
	else mdn.mdn = -2 ;
	mdn_class.openmdn();
	if (! mdn_class.lesemdn())
	{
		adr.adr = mdn.adr ;
		i = adr_class.openadr () ;
		i = adr_class.leseadr () ;
		if (!i) 
		{
			v_mdnname.Format("%s",_T(adr.adr_krz));
		}
		else
		{
			v_mdnname.Format("              ");
			MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
// gibbet nich			PrevDlgCtrl();
		}
	}
	else	// fehlerhafte Eingabe
	{
		v_mdnname.Format("              ");
		MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
// gibbet nich		PrevDlgCtrl();
	}
	UpdateData (FALSE) ;
}

void CDatevProDlg::OnEnKillfocusVdatum()
{

	UpdateData(TRUE) ;
	int	i = m_vdatum.GetLine(0,bufh,500) ;
	bufh[i] = '\0';

	if (i)
	{
		if (datumcheck(bufh))
		{
			v_vdatum.Format("%s",_T(bufh));
		}
		else
			v_vdatum.Format("");
	}
	UpdateData (FALSE) ;
	return ;
}

void CDatevProDlg::OnEnKillfocusBisdatum()
{
	
	UpdateData(TRUE) ;
	int	i = m_bisdatum.GetLine(0,bufh,500) ;
	bufh[i] = '\0';

	if (i)
	{
		if (datumcheck(bufh))
		{
			v_bisdatum.Format("%s",_T(bufh));
		}
		else
			v_bisdatum.Format("");
	}
	UpdateData (FALSE) ;
	return ;

}

void CDatevProDlg::OnBnClickedOk()
{
	Dateierstellung();
//	OnOK();
}

void CDatevProDlg::OnBnClickedCancel()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	OnCancel();
}

void CDatevProDlg::OnBnClickedButoption()
{

	DatevOption datevoption;
 	datev_k.mdn = mdn.mdn;
	datev_k.lfd = -1;
	datevoption.DoModal();

}



int CDatevProDlg::Dateierstellung (void)
{


	UpdateData (TRUE) ;

// Mandant auf G�ltigkeit pr�fen

	int i = m_mdnnr.GetLine(0,bufh,500);
	bufh[i] = '\0' ;
	if (i)	mdn.mdn = (short) atoi ( bufh );
	else mdn.mdn = -2 ;
	mdn_class.openmdn();
	if ( mdn_class.lesemdn())
	{
		MessageBox("Ung�ltige Eingabe- Mandant!", " ", MB_OK|MB_ICONSTOP);
		return FALSE;
	}

	fibu.mdn = mdn.mdn;	

// Von-Datum auf G�ltigkeit pr�fen

	i = m_vdatum.GetLine(0,bufh,500) ;
	bufh[i] = '\0';

	if (!i)
	{
			MessageBox("Ung�ltige Eingabe- von-Datum!", " ", MB_OK|MB_ICONSTOP);
			return FALSE;
	}
	if ( ! datumcheck(bufh))
	{
		MessageBox("Ung�ltige Eingabe- von-Datum!", " ", MB_OK|MB_ICONSTOP);
		return FALSE;
	}

// Bis-Datum auf G�ltigkeit pr�fen

	i = m_bisdatum.GetLine(0,bufh,500) ;
	bufh[i] = '\0';

	if (!i)
	{
			MessageBox("Ung�ltige Eingabe- bis-Datum!", " ", MB_OK|MB_ICONSTOP);
			return FALSE;
	}
	if ( ! datumcheck(bufh))
	{
		MessageBox("Ung�ltige Eingabe- bis-Datum!", " ", MB_OK|MB_ICONSTOP);
		return FALSE;
	}

	sqldatdst(  & fibu.vrech_dat , v_vdatum.GetBuffer()) ;
	sqldatdst(  & fibu.brech_dat , v_bisdatum.GetBuffer()) ;

	spfibu_barverk_par = atoi ( sys_par_class.sys_par_holen("fibu_barverk_par" )) ;

	int retcode = fibu_class.updatefibu44();

	if (retcode )
	{
		Sleep(1500);
		retcode = fibu_class.updatefibu44();
	}
	if ( retcode )
	{
		sprintf ( bufh, "Fehler %d -Abbruch",retcode);
		MessageBox(bufh , " ", MB_OK|MB_ICONSTOP);
		return FALSE;
	}

	datev_k.mdn = mdn.mdn ;
	datev_k.lfd = -1;
	int k_retcode = datev_k_class.opendatev_k();
	k_retcode = datev_k_class.lesedatev_k();
	if ( k_retcode )
	{
		MessageBox("Keine dafault-Informationen vorhanden!", " ", MB_OK|MB_ICONSTOP);
		return FALSE;
	}

	dbClass.beginwork();

	retcode = fibu_class.openfibu();
	retcode = fibu_class.lesefibu();
	if ( retcode )
	{
		MessageBox("Keine selektierten Daten vorhanden!", " ", MB_OK|MB_ICONSTOP);
		dbClass.rollbackwork();
		return FALSE;
	}

	datev_p.posi = 0;

	while ( !retcode && fibu.satz_art == 0)
	{	// Normal sollte zuerst eine satz_art == 1 ankommen ......
		retcode = fibu_class.lesefibu();
	}
	if ( retcode )	// Nur total-Schrott ?!
	{
		dbClass.rollbackwork();
		return FALSE ;
	}
	memcpy (&fibu_pers_alt,&fibu,sizeof (struct FIBU));
	if ( fibu.buch_art == 0)
	{
		kun.suchrechnr = fibu.rech_nr;
		kun.mdn = fibu.mdn;
		sprintf ( kun.suchblg_typ, "%s", fibu.blg_typ );
		kun_class.openkun();
		kun_class.lesekun();
	}
	if ( fibu.buch_art == 1)
	{
		lief.kreditor = fibu_pers_alt.pers_kto;
		lief.mdn = fibu.mdn;
		lief_class.openlief();
		lief_class.leselief();
	}
	komplettieredatev_k();
	retcode = fibu_class.lesefibu();	// hier sollte jetzt mindestens eine satz_art == 0 folgen 
	while ( !retcode)
	{
		if ( !retcode && 
			( fibu.rech_nr != fibu_pers_alt.rech_nr 
			|| fibu.blg_typ[0] != fibu_pers_alt.blg_typ[0]
			|| fibu.buch_art != fibu_pers_alt.buch_art
			)
		)
		{
			if ( fibu.buch_art == 0)
			{
				kun.suchrechnr = fibu.rech_nr;
				kun.mdn = fibu.mdn;
				sprintf ( kun.suchblg_typ, "%s", fibu.blg_typ );
				kun_class.openkun();
				kun_class.lesekun();
			}
			if ( fibu.buch_art == 1)
			{
				lief.kreditor = fibu_pers_alt.pers_kto;
				lief.mdn = fibu.mdn;
				lief_class.openlief();
				lief_class.leselief();
			}
			memcpy ( &fibu_pers_alt, &fibu, sizeof ( struct FIBU ));
		}
		else	// Zugeh�rige Sachkonten ....
			komplettieredatev_p();

			retcode = fibu_class.lesefibu();
	}

	retcode = fibu_class.updatefibuTOD();
	dbClass.commitwork();

	char s1[299];
	char s2[299];
	char * p ;
	p = getenv ( "TMPPATH" );
	if ( !p)
		sprintf ( s2, "c:\\datevpro.prm" );
	else
		sprintf ( s2, "%s\\datevpro.prm", p );

	FILE * fp;
	fp = fopen ( s2 , "w" ) ;
		if ( fp == NULL )
		{
			return  FALSE ;
		}
	
		sprintf ( s1, "NAME DATEVPRO\n"  ) ; 
	
		CString St = s1;
		St.MakeUpper() ;
		sprintf ( s1 ,"%s", St.GetBuffer(512)) ;
	
		fputs ( s1 , fp );

		fputs ( "LABEL 0\n" , fp ) ;

		sprintf ( s1, "DRUCK 0\n"   ) ;// 1350814 : gnadenlos drucken -> Achtung : Drucker muss gew�hlt sein !!

		fputs ( s1 , fp ) ;


		fputs ( "ANZAHL 1\n" , fp ) ;


		sprintf ( s1, "ALTERNATETYP TXT\n" ) ;
		fputs ( s1 , fp ) ;

		p =getenv ( "FIBUTMP") ;
		if ( !p )
			p =getenv ( "TMPPATH") ;


		sprintf ( s1, "ALTERNATEDIR %s\n" , p ) ;
		fputs ( s1 , fp ) ;
		sprintf ( s1, "ALTERNATENAME EXTF_Buchungen.csv\n" ) ;
		fputs ( s1 , fp ) ;

		sprintf ( s1, "ALTERNATETYP TXT\n" ) ;
		fputs ( s1 , fp ) ;
		sprintf ( s1, "ALTERNATE_SEPACHAR ;\n" ) ;
		fputs ( s1 , fp ) ;
		sprintf ( s1, "ALTERNATE_FRAMECHAR NONE\n" ) ;
		fputs ( s1 , fp ) ;


		sprintf ( s1, "lfd %ld %ld\n", datev_k.lfd , datev_k.lfd  ) ;
		fputs ( s1 , fp ) ;

		sprintf ( s1, "mdn %d %d\n", datev_k.mdn , datev_k.mdn  ) ;
		fputs ( s1 , fp ) ;


		fclose ( fp) ;

		sprintf ( s1, "dr70001.exe -datei \"%s\" " , s2 );
//		if ( minternmdn == 0 )	// 150514 : kein Protokoll ?! anders am 130814
			int ex_code = ProcWaitExec( (char *)s1, SW_SHOW, -1, 0, -1, 0 );



	return TRUE ;
}

void CDatevProDlg::OnBnClickedButautokonto()
{
	CAutoKonto autokonto;
	autokonto.DoModal();

}
BOOL CDatevProDlg::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				CWnd *xWnd ;
				xWnd = GetFocus ();
			           if(xWnd == GetDlgItem (IDOK)) break ;			// ok - eigentlich nichts machen
// auf den ok-Knopf verlegt ....			           if(xWnd == GetDlgItem (IDC_AUSDRUCK)) break ;	// Ausduck halt
			           if(xWnd == GetDlgItem (IDCANCEL))
					   {
							GetParent ()->DestroyWindow ();
							return TRUE ;	// Abbruch halt ( genau wie ButtonClickedCancel) 
					   }
	
				if (OnReturn ())
				{
					return TRUE;
				}
			}
			else if (pMsg->wParam == VK_TAB)
			{

				break;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (OnKeyup ())
				{
					return TRUE;
				}
			}
			else if (pMsg->wParam == VK_F5)
			{
//				StepBack ();
// ???				OnCancel () ;
				GetParent ()->DestroyWindow ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				return TRUE;
			}

	}

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL CDatevProDlg::OnReturn ()
{
	CWnd *Control = GetFocus ();

	Control = GetNextDlgTabItem (Control, FALSE);

	if (Control != NULL)
	{
		Control->SetFocus ();
	}
	return TRUE;
}

BOOL CDatevProDlg::OnKeyup ()
{
	CWnd *Control = GetFocus ();

	Control = GetNextDlgTabItem (Control, TRUE);
	if (Control != NULL)
	{
		Control->SetFocus ();
	}
	return TRUE;
}

void CDatevProDlg::komplettieredatev_k(void)
{
// Der deafult-Satz wurde  bereits vorgelesen

	long maxnummer = datev_k_class.lfdmaxdatev_k();
	if ( maxnummer < 0 )
		maxnummer = 0;

	maxnummer ++ ;
	datev_k.lfd = maxnummer;
	memcpy ( &datev_k.f15vondat, &fibu.vrech_dat, sizeof(TIMESTAMP_STRUCT));
	memcpy ( &datev_k.f16bisdat, &fibu.brech_dat, sizeof(TIMESTAMP_STRUCT));
	v_beschreib.Trim();
	sprintf ( bufh,"%s",v_beschreib.GetBuffer());
	bufh[25]='\0';
	char vonhdat[15];
	char bishdat[15];
	if (( strlen( bufh )) < 1 )
		sprintf ( bufh , "BuSta.%s-%s" ,
		sqldatdstger(&fibu.vrech_dat, vonhdat), sqldatdstger(&fibu.brech_dat,bishdat ));

	sprintf (datev_k.f17bezeich, "%s", bufh);
	datev_k_class.insertdatev_k();

}
void CDatevProDlg::komplettieredatev_p(void)
{


	datev_p.mdn = datev_k.mdn;
    datev_p.lfd = datev_k.lfd;
	datev_p.posi ++;
	datev_p.f001umsatz = fibu.betr_nto_e + fibu.mwst_btr_e;
	if ( datev_p.f001umsatz < 0 )	// dieser Umsatz ist immer positive Zahl
		datev_p.f001umsatz = 0 - datev_p.f001umsatz ;

	if ( fibu_pers_alt.buch_art == 0 )	// WA
	{
		if (fibu_pers_alt.blg_typ[0] == 'G' )
		{
			if ( ( fibu.betr_nto_e + fibu.mwst_btr_e ) <  0.0 )
				sprintf ( datev_p.f002solhab, "H");
			else
				sprintf ( datev_p.f002solhab, "S");
		}
		else	// "R" und "B"
		{
			if ( ( fibu.betr_nto_e + fibu.mwst_btr_e ) <  0.0 )
				sprintf ( datev_p.f002solhab, "S");
			else
				sprintf ( datev_p.f002solhab, "H");
		}
	}
	if ( fibu_pers_alt.buch_art == 1 )	// WE
	{
		if ( fibu_pers_alt.blg_typ[0] == 'G' )
		{
			if ( ( fibu.betr_nto_e + fibu.mwst_btr_e )<  0 )
				sprintf ( datev_p.f002solhab, "S");
			else
				sprintf ( datev_p.f002solhab, "H");
		}
		else	// "R" 
		{
			if ( ( fibu.betr_nto_e + fibu.mwst_btr_e ) <  0 )
				sprintf ( datev_p.f002solhab, "H");
			else
				sprintf ( datev_p.f002solhab, "S");
		}
	}

	sprintf ( datev_p.f003wkzums,"");	// WKZ aus dem Kopfsatz, falls nicht explizit angegeben -> "" ist ok.[4];
    datev_p.f004kurs = -1;	// "0" ist unzul�ssig -> blank in die Datei;	// decimal(10,6),
    datev_p.f005bumsatz= -1;	// keine Basis-W�hrungsmechanik aktiv;	// decimal(12,2),
    sprintf ( datev_p.f006wkzbums,"");	// Pfglicht, falls Basisumsatz aktiviert
	datev_p.f007konto = fibu.erl_kto;
    datev_p.f008gegkonto = fibu_pers_alt.pers_kto ;
	datev_kto.erl_kto = fibu.erl_kto;
	datev_kto_class.opendatev_kto();
	if ( ! datev_kto_class.lesedatev_kto())
		sprintf ( datev_p.f009buschl,"");
	else	//keine Automatik-Konto
	{
		while ( fibu.mwst > 10 )
			fibu.mwst -= 10 ;
		sprintf ( datev_p.f009buschl,"%d",fibu.mwst );	// ich nehme zun�chst einfach den "normalen" Stuerschl�ssel
	}
	memcpy( &datev_p.f010rech_dat, &fibu.rech_dat, sizeof ( TIMESTAMP_STRUCT)) ;
	sprintf ( datev_p.f011rech_nr1,"%d",fibu.rech_nr);
    sprintf ( datev_p.f012rech_nr2,"");
    datev_p.f013skonto = -1 ;	// decimal(10,2),
    sprintf ( datev_p.f014butext, "");	// Buchungstext	[61];
    datev_p.f015psperre = 0 ;			// oder == -1
    sprintf ( datev_p.f016divadr,"");	// div. adress-Nummer = ?
    datev_p.f017partbank = -1;	// Zun�chst mal weg damit
    sprintf ( datev_p.f018sachverhalt,"");	//  nur bei Mahnungen spannend
    datev_p.f019zsperre = -1;	// Zinssperre 
    sprintf ( datev_p.f020bellink,"");
    sprintf ( datev_p.f021bia1,"");
    sprintf ( datev_p.f022bii1,"");
    sprintf ( datev_p.f023bia2,"");
    sprintf ( datev_p.f024bii2,"");
    sprintf ( datev_p.f025bia3,"");
    sprintf ( datev_p.f026bii3,"");
    sprintf ( datev_p.f027bia4,"");
    sprintf ( datev_p.f028bii4,"");
    sprintf ( datev_p.f029bia5,"");
    sprintf ( datev_p.f030bii5,"");
    sprintf ( datev_p.f031bia6,"");
    sprintf ( datev_p.f032bii6,"");
    sprintf ( datev_p.f033bia7,"");
    sprintf ( datev_p.f034bii7,"");
    sprintf ( datev_p.f035bia8,"");
    sprintf ( datev_p.f036bii8,"");
	if ( fibu.kst > 0)  
		sprintf ( datev_p.f037kost1,"%d",fibu.kst);
	else
		sprintf ( datev_p.f037kost1,"");
	sprintf ( datev_p.f038kost2,""); 	// [9];
    datev_p.f039kostm= -1;	// decimal(11,2),
	if ( fibu.buch_art == 0 )
	sprintf ( datev_p.f040ustid, "%s",kun.ust_id16 ) ;	// 2-setll L�nderkuerzel + 13 stell. ust_id
	// Hier ist "Disziplin" gefragt !!!!
	if ( fibu.buch_art == 1 )
	sprintf ( datev_p.f040ustid, "%s",lief.ust_id ) ;	// 2-setll L�nderkuerzel + 13 stell. ust_id

	datev_p.f041euproz = -1 ;	// decimal(4,2),
    sprintf ( datev_p.f042steuart,"");	// Abweichende Versteuerung ist hier m�glich
    datev_p.f043sachll = -1;
    datev_p.f044fergll = -1;
    datev_p.f045bu49hft = -1 ;
    datev_p.f046bu49hfn = -1;
    datev_p.f047bu49fer = -1;
    sprintf ( datev_p.f048zinfa01,"");
    sprintf ( datev_p.f049zinfi01,"");
    sprintf ( datev_p.f050zinfa02,"");
    sprintf ( datev_p.f051zinfi02,"");
    sprintf ( datev_p.f052zinfa03,"");
    sprintf ( datev_p.f053zinfi03,"");
    sprintf ( datev_p.f054zinfa04,"");
    sprintf ( datev_p.f055zinfi04,"");
    sprintf ( datev_p.f056zinfa05,"");
    sprintf ( datev_p.f057zinfi05,"");
    sprintf ( datev_p.f058zinfa06,"");
    sprintf ( datev_p.f059zinfi06,"");
    sprintf ( datev_p.f060zinfa07,"");
    sprintf ( datev_p.f061zinfi07,"");
    sprintf ( datev_p.f062zinfa08,"");
    sprintf ( datev_p.f063zinfi08,"");
    sprintf ( datev_p.f064zinfa09,"");
    sprintf ( datev_p.f065zinfi09,"");
    sprintf ( datev_p.f066zinfa10,"");
    sprintf ( datev_p.f067zinfi10,"");
    sprintf ( datev_p.f068zinfa11,"");
    sprintf ( datev_p.f069zinfi11,"");
    sprintf ( datev_p.f070zinfa12,"");
    sprintf ( datev_p.f071zinfi12,"");
    sprintf ( datev_p.f072zinfa13,"");
    sprintf ( datev_p.f073zinfi13,"");
    sprintf ( datev_p.f074zinfa14,"");
    sprintf ( datev_p.f075zinfi14,"");
    sprintf ( datev_p.f076zinfa15,"");
    sprintf ( datev_p.f077zinfi15,"");
    sprintf ( datev_p.f078zinfa16,"");
    sprintf ( datev_p.f079zinfi16,"");
    sprintf ( datev_p.f080zinfa17,"");
    sprintf ( datev_p.f081zinfi17,"");
    sprintf ( datev_p.f082zinfa18,"");
    sprintf ( datev_p.f083zinfi18,"");
    sprintf ( datev_p.f084zinfa19,"");
    sprintf ( datev_p.f085zinfi19,"");
    sprintf ( datev_p.f086zinfa20,"");
    sprintf ( datev_p.f087zinfi20,"");
    datev_p.f088stueck = -1;
    datev_p.f089gewicht = -1;	// decimal(10,2),
    datev_p.f090zahlwei = -1 ;
	sprintf ( datev_p.f091fordart,"");	// [11];
	datev_p.f092verjahr = fibu.rech_dat.year;
    memcpy ( &datev_p.f093faellig, &fibu.rech_dat, sizeof ( TIMESTAMP_STRUCT));	// erst mal irgendwas rein
    datev_p.f094sktotyp = -1;
    sprintf ( datev_p.f095auf_num, "");	// [31];
    sprintf ( datev_p.f096buchtyp, "");	// [3];
	datev_p.f097ustschl = -1;
    sprintf ( datev_p.f098eustaat, "");		// [3];
    datev_p.f099asachll = -1; ;
    datev_p.f100aeuust = -1;	// decimal(4,2),
    datev_p.f101aerlkto = -1;
    sprintf ( datev_p.f102herkkz,"");	//  [3];
    sprintf ( datev_p.f103leerfeld,"");	// [37];
	memcpy ( &datev_p.f104kostdat, &fibu.rech_dat, sizeof( TIMESTAMP_STRUCT));
    sprintf ( datev_p.f105mandref,"");	// kun.mandatref  [36];

	datev_p_class.insertdatev_p();

}
void CDatevProDlg::OnBnClickedButnochmal()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
		datev_p_class.zuruecksetzen();

}
