#include "stdafx.h"
#include "DbClass.h"
#include "fibu.h"

extern DB_CLASS dbClass;

struct FIBU fibu,  fibu_null, fibu_pers_alt;

short spfibu_barverk_par;

FIBU_CLASS  fibu_class ;

int FIBU_CLASS::lesefibu (void)
{
      int di = dbClass.sqlfetch (readcursor);

	  return di;
}

int FIBU_CLASS::openfibu (void)
{

		if ( readcursor < 0 ) prepare ();
         return dbClass.sqlopen (readcursor);
}

int FIBU_CLASS::updatefibu44 (void)
{
		if ( readcursor < 0 ) prepare ();
         return dbClass.sqlexecute (upd_cursor);
}

int FIBU_CLASS::updatefibuTOD (void)
{
		if ( readcursor < 0 ) prepare ();
         return dbClass.sqlexecute (del_cursor);
}


void FIBU_CLASS::prepare (void)
{
								
// Zuerst alle Saetze auf "04.04.1944" updaten 
// begin work
// dann alles buch_dat = 04.04.1944 lesen
// dann einzeln abarbeiten und in temp-Table schreiben
// dann einzeln auf buch_dat = today setzen
// commit work


// readcursor : lese alle fibu-saetze 
// order by buch_art,blg_typ,rech_nr, satz_art desc where mdn = ? and buch_dat =  "04.04.1944"

	dbClass.sqlin ((short  *) &fibu.mdn , SQLSHORT , 0 ) ;


	dbClass.sqlout ((double *) &fibu.betr_bto , SQLDOUBLE , 0 ) ;
	dbClass.sqlout ((double *) &fibu.betr_nto , SQLDOUBLE , 0 ) ;
	dbClass.sqlout ((double *) &fibu.mwst_betr , SQLDOUBLE , 0 ) ;
	dbClass.sqlout ((short  *) &fibu.mwst , SQLSHORT , 0 ) ;
	dbClass.sqlout ((long   *) &fibu.erl_kto , SQLLONG , 0 ) ;

	dbClass.sqlout ((long   *) &fibu.blg , SQLLONG , 0 ) ;
	dbClass.sqlout ((long   *) &fibu.rech_nr , SQLLONG , 0 ) ;
	dbClass.sqlout((TIMESTAMP_STRUCT *)&fibu.rech_dat , SQLTIMESTAMP , 26 ) ;
	dbClass.sqlout ((long   *) &fibu.pers_kto , SQLLONG , 0 ) ;
	dbClass.sqlout ((long   *) &fibu.kst , SQLLONG , 0 ) ;

	dbClass.sqlout ((short  *) &fibu.zahl_kond , SQLSHORT , 0 ) ;
	dbClass.sqlout ((short  *) &fibu.ziel1 , SQLSHORT , 0 ) ;
	dbClass.sqlout ((double *) &fibu.skto1 , SQLDOUBLE , 0 ) ;
	dbClass.sqlout ((short  *) &fibu.ziel2 , SQLSHORT , 0 ) ;
	dbClass.sqlout ((double *) &fibu.skto2 , SQLDOUBLE , 0 ) ;

	dbClass.sqlout ((short  *) &fibu.ziel3 , SQLSHORT , 0 ) ;
	dbClass.sqlout ((double *) &fibu.skto3 , SQLDOUBLE , 0 ) ;
// macht nur Aerger	dbClass.sqlout((TIMESTAMP_STRUCT *)&fibu.val , SQLTIMESTAMP , 26 ) ;
	dbClass.sqlout ((char   *)  fibu.krz_txt , SQLCHAR , 17 ) ;
//	dbClass.sqlout ((short  *) &fibu.mdn , SQLSHORT , 0 ) ;

	dbClass.sqlout ((short  *) &fibu.fil , SQLSHORT , 0 ) ;
	dbClass.sqlout ((short  *) &fibu.satz_art , SQLSHORT , 0 ) ;
	dbClass.sqlout ((short  *) &fibu.buch_art , SQLSHORT , 0 ) ;
	dbClass.sqlout((TIMESTAMP_STRUCT *)&fibu.buch_dat , SQLTIMESTAMP , 26 ) ;
	dbClass.sqlout ((char   *) fibu.blg_typ , SQLCHAR ,2 ) ;

	dbClass.sqlout ((char   *)  fibu.rech_char , SQLCHAR , 17 ) ;
	dbClass.sqlout ((double *) &fibu.mwst_proz , SQLDOUBLE , 0 ) ;
	dbClass.sqlout ((long   *) &fibu.ktr , SQLLONG , 0 ) ;
	dbClass.sqlout ((double *) &fibu.me , SQLDOUBLE , 0 ) ;
	dbClass.sqlout ((double *) &fibu.pr_vk , SQLDOUBLE , 0 ) ;

	dbClass.sqlout ((char   *)  fibu.buch_period , SQLCHAR , 8 ) ;
	dbClass.sqlout ((short  *) &fibu.scheck , SQLSHORT , 0 ) ;
	dbClass.sqlout ((char   *)  fibu.storno , SQLCHAR , 2 ) ;
	dbClass.sqlout ((char   *)  fibu.ueb_kz , SQLCHAR , 2 ) ;
	dbClass.sqlout ((double *) &fibu.betr_bto_e , SQLDOUBLE , 0 ) ;

	dbClass.sqlout ((double *) &fibu.betr_bto_f , SQLDOUBLE , 0 ) ;
	dbClass.sqlout ((double *) &fibu.betr_nto_e , SQLDOUBLE , 0 ) ;
	dbClass.sqlout ((double *) &fibu.betr_nto_f , SQLDOUBLE , 0 ) ;
	dbClass.sqlout ((double *) &fibu.mwst_btr_e , SQLDOUBLE , 0 ) ;
	dbClass.sqlout ((short  *) &fibu.waehrung , SQLSHORT , 0 ) ;

	dbClass.sqlout ((double *) &fibu.kurs , SQLDOUBLE , 0 ) ;
	dbClass.sqlout ((short  *) &fibu.faktor , SQLSHORT , 0 ) ;
	dbClass.sqlout ((double *) &fibu.pr_vk_e , SQLDOUBLE , 0 ) ;
	dbClass.sqlout ((double *) &fibu.pr_vk_f , SQLDOUBLE , 0 ) ;
	dbClass.sqlout ((double *) &fibu.skto_f , SQLDOUBLE , 0 ) ;

	dbClass.sqlout ((double *) &fibu.skto_f_e , SQLDOUBLE , 0 ) ;
	dbClass.sqlout ((double *) &fibu.skto_f_f , SQLDOUBLE , 0 ) ;

	readcursor = (short)dbClass.sqlcursor ("select "
		
	"  betr_bto ,betr_nto ,mwst_betr ,mwst ,erl_kto "
	" ,blg ,rech_nr ,rech_dat ,pers_kto ,kst "
	" ,zahl_kond ,ziel1 ,skto1 ,ziel2 ,skto2 "
	" ,ziel3 ,skto3 ,krz_txt "
	" ,fil ,satz_art ,buch_art ,buch_dat ,blg_typ "
	" ,rech_char ,mwst_proz ,ktr ,me ,pr_vk "
	" ,buch_period ,scheck ,storno ,ueb_kz ,betr_bto_e "
	" ,betr_bto_f ,betr_nto_e ,betr_nto_f ,mwst_btr_e ,waehrung "
	" ,kurs ,faktor ,pr_vk_e ,pr_vk_f ,skto_f "
	" ,skto_f_e ,skto_f_f "

	" from fibu  where mdn = ? and buch_dat = \"04.04.1944\" and betr_nto_e <> 0 "  
	" order by buch_art,blg_typ,rech_nr, satz_art desc "
	) ;	// Zuerst kommt der Kopfsatz, dann die Positionen

// 1. upd_cursor

	
	dbClass.sqlin ((double *) &fibu.mdn , SQLSHORT , 0 ) ;
	dbClass.sqlin ((double *) &fibu.vrech_dat , SQLTIMESTAMP , 0 ) ;
	dbClass.sqlin ((double *) &fibu.brech_dat , SQLTIMESTAMP , 0 ) ;

	if ( spfibu_barverk_par == 1 )
		upd_cursor = (short)dbClass.sqlcursor ("update fibu  set  "
			" buch_dat = \"04.04.1944\" "
		" where mdn = ?	and fibu.rech_dat between ? and ? and fibu.buch_dat = \"03.03.1933\" "
		" and blg_typ in ( \"R\", \"G\", \"B\" ) " 
		);
	else	// keine Barverkaeufe 
		upd_cursor = (short)dbClass.sqlcursor ("update fibu  set  "
			" buch_dat = \"04.04.1944\" "
		" where mdn = ?	and fibu.rech_dat between ? and ? and fibu.buch_dat = \"03.03.1933\" "
		" and blg_typ in ( \"R\", \"G\" ) " 
		);

// 2. upd_cursor( del_cursor )
	
	dbClass.sqlin ((double *) &fibu.mdn , SQLSHORT , 0 ) ;

	del_cursor = (short)dbClass.sqlcursor (
		"update fibu  set  buch_dat = TODAY "
		" where mdn = ?	and fibu.buch_dat = \"04.04.1944\" " 
	) ;

}

