#pragma once
#include "afxwin.h"


// DatevOption-Dialogfeld

class DatevOption : public CDialog
{
	DECLARE_DYNAMIC(DatevOption)

public:
	DatevOption(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~DatevOption();

// Dialogfelddaten
	enum { IDD = IDD_OPTION_DIALOG };

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	CEdit m_1formatkz;
	CString v_1formatkz;
	CEdit m_2version;
	CString v_2version;
	CEdit m_3kategorie;
	CString v_3kategorie;
	CEdit m_4formatname;
	CString v_4formatname;
	CEdit m_5formatversion;
	CString v_5formatversion;
	CEdit m_11berater;
	CString v_11berater;
	CEdit m_12mandant;
	CString v_12mandant;
	CEdit m_13wjbeginn;
	CString v_13wjbeginn;
	CEdit m_14kontenlang;
	CString v_14kontenlang;
	CEdit m_18diktatkz;
	CString v_18diktatkz;
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedButterw();
 	afx_msg BOOL PreTranslateMessage(LPMSG) ;
	virtual BOOL OnReturn (void);
	virtual BOOL OnKeyup (void);

	afx_msg void OnEnKillfocus13wjbeginn();
};
