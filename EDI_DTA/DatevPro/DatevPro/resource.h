//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by DatevPro.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_DATEVPRO_DIALOG             102
#define IDR_MAINFRAME                   128
#define IDD_DIALOG1                     129
#define IDD_OPTION_DIALOG               129
#define IDD_KONTOLISTE_DIALOG           130
#define IDC_MDNNR                       1000
#define IDC_MDNNAME                     1001
#define IDC_BUTOPTION                   1002
#define IDC_BUTAUTOKONTO                1003
#define IDC_BUTNOCHMAL                  1004
#define IDC_VDATUM                      1005
#define IDC_BISDATUM                    1006
#define IDC_BESCHREIB                   1007
#define IDC_1FORMATKZ                   1008
#define IDC_2VERSION                    1009
#define IDC_3KATEGORIE                  1010
#define IDC_4FORMATNAME                 1011
#define IDC_5FORMATVERSION              1012
#define IDC_11BERATER                   1013
#define IDC_12MANDANT                   1014
#define IDC_13WJBEGINN                  1015
#define IDC_14KONTENLANG                1016
#define IDC_18DIKTATKZ                  1017
#define IDC_BUTTON1                     1018
#define IDC_BUTTERW                     1018
#define IDC_BUTF6                       1018
#define IDC_LIST2                       1020
#define IDC_BUTTON2                     1021
#define IDC_BUTF7                       1021

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1022
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
