// DatevProDlg.h : Headerdatei
//

#pragma once
#include "afxwin.h"


// CDatevProDlg-Dialogfeld
class CDatevProDlg : public CDialog
{
// Konstruktion
public:
	CDatevProDlg(CWnd* pParent = NULL);	// Standardkonstruktor

// Dialogfelddaten
	enum { IDD = IDD_DATEVPRO_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV-Unterst�tzung


// Implementierung
protected:
	HICON m_hIcon;

	// Generierte Funktionen f�r die Meldungstabellen
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CEdit m_mdnnr;
	CString v_mdnnr;
	CEdit m_mdnname;
	CString v_mdnname;
	CEdit m_vdatum;
	CString v_vdatum;
	CEdit m_bisdatum;
	CString v_bisdatum;
	CEdit m_beschreib;
	CString v_beschreib;
	afx_msg void OnEnKillfocusMdnnr();
	afx_msg void OnEnKillfocusVdatum();
	afx_msg void OnEnKillfocusBisdatum();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedButoption();
	afx_msg void OnBnClickedButautokonto();
 	afx_msg BOOL PreTranslateMessage(LPMSG) ;
	virtual BOOL OnReturn (void);
	virtual BOOL OnKeyup (void);
	void ReadMdn(void) ;
	int Dateierstellung (void) ;
	void komplettieredatev_k(void);
	void komplettieredatev_p(void);


	afx_msg void OnBnClickedButnochmal();
};
