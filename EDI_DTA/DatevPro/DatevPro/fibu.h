#ifndef _FIBU_DEF
#define _FIBU_DEF


extern short spfibu_barverk_par ;

struct FIBU {
double 	betr_bto ;
double betr_nto ;
double mwst_betr ;
short mwst ;
long erl_kto ;
long blg ;
long rech_nr ;
TIMESTAMP_STRUCT rech_dat ;
TIMESTAMP_STRUCT vrech_dat ;
TIMESTAMP_STRUCT brech_dat ;
long pers_kto ;
long kst ;
short zahl_kond ;
short ziel1 ;
double skto1 ;
short ziel2 ;
double skto2 ;
short ziel3 ;
double skto3 ;
TIMESTAMP_STRUCT val ;
char krz_txt [17] ;
short mdn ;
short fil ;
short satz_art ;
short buch_art ;
TIMESTAMP_STRUCT buch_dat ;
char blg_typ [2] ;
char rech_char[17] ;
double mwst_proz ;
long ktr ;
double me ;
double pr_vk ;
char buch_period [8] ;
short scheck ;
char storno [2] ;
char ueb_kz [2] ;
double betr_bto_e ;
double betr_bto_f ;
double betr_nto_e ;
double betr_nto_f ;
double mwst_btr_e ;
short waehrung ;
double kurs ;
short faktor ;
double pr_vk_e ;
double pr_vk_f ;
double skto_f ;
double skto_f_e ;
double skto_f_f ;

};

extern struct FIBU fibu, fibu_null, fibu_pers_alt;

class FIBU_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int lesefibu (void);
               int openfibu (void);
			   int updatefibu44(void);
			   int updatefibuTOD(void);
			   			   FIBU_CLASS () : DB_CLASS ()
               {
               }
};

extern class FIBU_CLASS fibu_class ;

struct RECH_MAHN
{
long rech_nr ;
char blg_typ [2] ;
short mdn ;
short mahn_stu ;
short kun_of_po ;
TIMESTAMP_STRUCT faell_dat ;
TIMESTAMP_STRUCT mahn_dat ;
double gbr ;
double gbr_tmp ;
short mahn_stu_tmp ;
short waehrung ;
double gebr_eur ;
double gebr_fremd ;
double gebr_tmp_eur ;
double gebr_tmp_fremd ;

} ;
extern struct RECH_MAHN rech_mahn , rech_mahn_null ;


#endif

