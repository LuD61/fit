#include "stdafx.h"
#include "DbClass.h"
#include "adr.h"

struct ADR adr,  adr_null;
struct ADR adr2, adr3;
extern DB_CLASS dbClass;

ADR_CLASS adr_class ;

int iadr ;
static int anzzfelder ;

int ADR_CLASS::dbcount (void)
/**
Tabelle adr lesen.
**/
{

         if (test_upd_cursor == -1)
         {
             prepare ();
         }
         dbClass.sqlopen (count_cursor);
         dbClass.sqlfetch (count_cursor);
         if (sqlstatus == 0)
         {
                      return anzzfelder;
         }
         return sqlstatus;
}

int ADR_CLASS::leseadr (void)
{
      int di = dbClass.sqlfetch (readcursor);

	  return di;
}

int ADR_CLASS::openadr (void)
{

		if ( readcursor < 0 ) prepare ();	// ob das hier richtig ist ?

         return dbClass.sqlopen (readcursor);
}

void ADR_CLASS::prepare (void)
{


	dbClass.sqlin ((long *) &adr.adr, SQLLONG, 0);
    dbClass.sqlout ((long *)  &anzzfelder, SQLLONG, 0);


    count_cursor = (short) dbClass.sqlcursor ("select count(*) from adr "
										"where adr.adr = ? ");
										
	test_upd_cursor = 1;


	dbClass.sqlin ((long *) &adr.adr, SQLLONG, 0);

	dbClass.sqlout ((long *) &adr.adr,SQLLONG, 0);
	dbClass.sqlout ((char *) adr.adr_krz,SQLCHAR, 17);
	dbClass.sqlout ((char *) adr.adr_nam1, SQLCHAR, 37);
	dbClass.sqlout ((char *) adr.adr_nam2, SQLCHAR, 37);
	dbClass.sqlout ((short *) &adr.adr_typ, SQLSHORT, 0);

	dbClass.sqlout ((char *) adr.adr_verkt, SQLCHAR, 17);
	dbClass.sqlout ((short *) &adr.anr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &adr.delstatus, SQLSHORT, 0);
	dbClass.sqlout ((char *) adr.fax, SQLCHAR, 21);
	dbClass.sqlout ((short *) &adr.fil, SQLSHORT,0);
	
	dbClass.sqlout ((char *) adr.geb_dat, SQLCHAR, 12);
	dbClass.sqlout ((short *) &adr.land, SQLSHORT, 0);
	dbClass.sqlout ((short *) &adr.mdn, SQLSHORT,  0);
	dbClass.sqlout ((char *) adr.merkm_1, SQLCHAR, 3);
	dbClass.sqlout ((char *) adr.merkm_2, SQLCHAR, 3);
	
	dbClass.sqlout ((char *) adr.merkm_3, SQLCHAR, 3);
	dbClass.sqlout ((char *) adr.merkm_4, SQLCHAR, 3);
	dbClass.sqlout ((char *) adr.merkm_5, SQLCHAR, 3);
	dbClass.sqlout ((char *) adr.modem, SQLCHAR, 21);
	dbClass.sqlout ((char *) adr.ort1, SQLCHAR, 37);
	
	dbClass.sqlout ((char *) adr.ort2, SQLCHAR, 37);

	dbClass.sqlout ((char *) adr.partner, SQLCHAR, 37);
	dbClass.sqlout ((char *) adr.pf, SQLCHAR, 17);
	dbClass.sqlout ((char *) adr.plz, SQLCHAR, 9);
	dbClass.sqlout ((short *) &adr.staat, SQLSHORT, 0);
	dbClass.sqlout ((char *) adr.str,SQLCHAR, 37);

	dbClass.sqlout ((char *) adr.tel, SQLCHAR, 21);
	dbClass.sqlout ((char *) adr.telex, SQLCHAR, 21);
	dbClass.sqlout ((long *) &adr.txt_nr, SQLLONG, 0);

	dbClass.sqlout ((char *) adr.plz_postf, SQLCHAR,9);
	dbClass.sqlout ((char *) adr.plz_pf,SQLCHAR, 9);
	dbClass.sqlout ((char *) adr.iln, SQLCHAR, 33 );
	dbClass.sqlout ((char *) adr.email, SQLCHAR, 37);
	dbClass.sqlout ((char *) adr.swift, SQLCHAR, 25);

	readcursor = (short) dbClass.sqlcursor ("select "
	" adr, adr_krz, adr_nam1, adr_nam2, adr_typ "
	" ,adr_verkt, anr, delstatus, fax, fil "
	" ,geb_dat, land, mdn, merkm_1, merkm_2 "
	" ,merkm_3, merkm_4, merkm_5, modem, ort1 "
	" ,ort2 "
	" ,partner, pf, plz, staat, str "
	" ,tel, telex, txt_nr "
	" ,plz_postf, plz_pf ,iln, email, swift "
	" from adr where adr = ? " ) ;
	
	
	/* ---->

	dbClass.sqlout ((long *) &adr.adr,SQLLONG, 0);

	dbClass.sqlout ((char *) adr.adr_krz,SQLCHAR, 17);
	dbClass.sqlout ((char *) adr.adr_nam1, SQLCHAR, 37);
	dbClass.sqlout ((char *) adr.adr_nam2, SQLCHAR, 37);
	dbClass.sqlout ((short *) &adr.adr_typ, SQLSHORT, 0);
	dbClass.sqlout ((char *) adr.adr_verkt, SQLCHAR, 17);
	dbClass.sqlout ((short *) &adr.anr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &adr.delstatus, SQLSHORT, 0);
	dbClass.sqlout ((char *) adr.fax, SQLCHAR, 21);
	dbClass.sqlout ((short *) &adr.fil, SQLSHORT,0);
	dbClass.sqlout ((char *) adr.geb_dat, SQLCHAR, 12);
	dbClass.sqlout ((short *) &adr.land, SQLSHORT, 0);
	dbClass.sqlout ((short *) &adr.mdn, SQLSHORT,  0);
	dbClass.sqlout ((char *) adr.merkm_1, SQLCHAR, 3);
	dbClass.sqlout ((char *) adr.merkm_2, SQLCHAR, 3);
	dbClass.sqlout ((char *) adr.merkm_3, SQLCHAR, 3);
	dbClass.sqlout ((char *) adr.merkm_4, SQLCHAR, 3);
	dbClass.sqlout ((char *) adr.merkm_5, SQLCHAR, 3);
	dbClass.sqlout ((char *) adr.modem, SQLCHAR, 21);
	dbClass.sqlout ((char *) adr.ort1, SQLCHAR, 37);
	dbClass.sqlout ((char *) adr.ort2, SQLCHAR, 37);
	dbClass.sqlout ((char *) adr.partner, SQLCHAR, 37);
	dbClass.sqlout ((char *) adr.pf, SQLCHAR, 17);
	dbClass.sqlout ((char *) adr.plz, SQLCHAR, 9);
	dbClass.sqlout ((short *) &adr.staat, SQLSHORT, 0);
	dbClass.sqlout ((char *) adr.str,SQLCHAR, 37);
	dbClass.sqlout ((char *) adr.tel, SQLCHAR, 21);
	dbClass.sqlout ((char *) adr.telex, SQLCHAR, 21);
	dbClass.sqlout ((long *) &adr.txt_nr, SQLLONG, 0);
	dbClass.sqlout ((char *) adr.plz_postf, SQLLONG,9);
	dbClass.sqlout ((char *) adr.plz_pf,SQLCHAR, 9);
	dbClass.sqlout ((char *) adr.iln, SQLCHAR, 33 );
	dbClass.sqlout ((char *) adr.email, SQLCHAR, 37);
	dbClass.sqlout ((char *) adr.swift, SQLCHAR, 25);


            readcursor = dbClass.sqlcursor ("select "

	" adr, adr_krz, adr_nam1, adr_nam2, adr_typ, adr_verkt, anr, " 
	" delstatus, fax, fil, geb_dat, land, mdn, merkm_1, merkm_2, "
	" merkm_3, merkm_4, merkm_5, modem, ort1, ort2, partner, pf, "
	" plz, staat, str, tel, telex, txt_nr, plz_postf, plz_pf, iln, "
	" email, swift " 
	" from adr where adr = ? " ) ;

< ----- */		

}

