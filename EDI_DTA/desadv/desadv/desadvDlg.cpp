// desadvDlg.cpp : Implementierungsdatei
//

#include "stdafx.h"

#include <direct.h>
#include <fcntl.h>

#include "desadv.h"
#include "desadvDlg.h"
#include "DbClass.h"
#include "kun.h"
#include "mdn.h"
#include "adr.h"
#include "ptabn.h"
#include "ls.h"
#include "lsnve.h"
#include "SpaDesadv.h"
#include "mo_numme.h"
#include "rech.h"
#include "a_bas.h"
#include "dta.h"
#include "tsmt.h"
#include "m_desadvseg.h"
#include "sys_par.h"
#include "tempdr.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#define fenebergohneean 0		// 010913	schaltbar, bis alles geklaert ist

char EchtDateiNameKurz[99] ;	// 090413
char EchtDateiNameKurzMaske[111] ;	// 13014

static char obchep[20] ;	// 080813


CEDIPUT cepu ;	// Das ist die Schreib-Klasse 
 static bool x_checkeinzel ;

static long lfdliste ;		// Listennummer fuer Ausdruck
static long lfdlistennr ;	// lfd in der Liste

static int nvefertig = 1 ;	// 0 => Boesi-style, 1 => komplette 18-stellige NVE

int obaktiv = 1 ;		// Mengenbez. im qty-Segment ausgeben oder nicht

int ref_nr  ;			// globaler counter
long inummer ;			// lfd.Nr-Variable in der dta-ref


long m_abk_nr[20]  ;
long m_mge_lief[20]  ;

int doption = 0  ;
int rheinstetten = 0 ;
long rheinstettendta = -1 ;	// 270313
// 180511
int iCPS ;	// laufende CPS-Nummer
int iBAS ;	// Basis-CPS f�r Sandwichpalette
int iAUF ;	// Basis-CPS f�r Kiste

static int badcount ;
static int gescount ;
static int kbadcount ;
static int kgescount ;


int def_a_kun = 1 ;		// 1:= nutze ean aus a_kun , 0 := baue ean IMMER aus interner Nummer
char protokoll [257] ;
char ddateiname[523] ;
char ddateinamex[523] ;	// 150313
BOOL dateinamefertig;	// 130114

extern char EchtDateinameKurz[99] ;	// 090413
extern char EchtDateinameKurzMaske[111] ;	// 130114

char a_kunfa_kun[50] ;

int sypmetro1_par = 0 ;
int sypwiechmann_par = 0 ;
int sypboes_par = 0 ;
int sypeana_par = 0 ;
int sypeane_par = 0 ;
int sypli_a_edi = 0 ;
int sypli_a_par = 0 ;
int sypnachkpreis = 2 ;
int sypdietzaku = 0 ;
int sypdietzsch_par = 0 ;	// 250613

// mehr als 25 LG-Artikel sollten kaum in Gebrauch sein ...

static char meeinhcode [25][9];
static double meeinharti[25] ;


static char bufh[512] ;

static char tourbeding[512] ;

int gesamtcount ;


// 060511 : Erste Schritte fuer master-NVE-Ablauf
	int aktuelleebene ;	//	0 = MasterNVE
						//	1 = Palette oder Layer
						//	2 = Kiste
						// 3 = ArtikelinKiste


// ######### Prototyping ############## 
void nvekomplett ( char * ausgang , double inwert, char * anfnve ,char * transfer) ;
void erwladeposnve (void ) ;

char *clippedi (char *string) ;
DWORD ProcWaitExec (LPSTR prog, WORD SHOW_MODE, int x, int y, int cx, int cy);


DB_CLASS dbClass ;	// Hier ist die Heimat der dbClass, alle Tabellen sind in ihrem jeweiligen Modul daheim

// CAboutDlg-Dialogfeld f�r Anwendungsbefehl "Info"

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialogfelddaten
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterst�tzung

// Implementierung
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


#define MAXLGZAHL 30

static long lgart[ MAXLGZAHL + 1 ] ;
static char lgname[ MAXLGZAHL + 1 ] [ 10 ];
static char lgchep[ MAXLGZAHL + 1 ] [ 5 ];

char gs1_leergut[20] ;
void ladegs1_leergut()
{
	int i = ptabn_class.openallptabn("gs1_leergut") ;
	if ( !i )
	{
		for ( i = 0 ; i < MAXLGZAHL ; i++ )
		{
			long j = ptabn_class.leseallptabn() ;

			if ( j )
					break ;
			j = atol ( ptabn.ptwer1 ) ;	// der Satz ist zugeordnet
			if ( j > 0 )
			{
				lgart[i] = atol ( ptabn.ptwer1 ) ;
				sprintf ( lgname[i],"%s", clippedi ( ptabn.ptwer2) );
// 040913 A
				a_leer.a = lgart[i];
				a_leer_class.opena_leer();
				int xret = a_leer_class.lesea_leer();
				if ( xret )
					a_leer.chep = 0 ;

				sprintf ( lgchep[i],"" );	// 080813 - keine Info
			// hier noch a_leer.chep auswerten 
				if ( a_leer.chep == 1 )
					sprintf ( lgchep[i],":4E" );	// gemietet -CHEP 080813
				if ( a_leer.chep == 2 )
					sprintf ( lgchep[i],":5E" );	// tauschbar -EURO 080813
				if ( a_leer.chep == 0 )
					sprintf ( lgchep[i],"" );	// 080813 - keine Info

				lgart[i+1] = 0 ;
				sprintf ( lgname[i+1] ,"" );
				sprintf ( lgchep[i+1] ,"" );	// 080813
			}
			else
			{
				if ( i > 0 )
					i -- ;
			}
		}
	}
}

char * lesegs1_leergut(long inta)
{
	int i ;

	sprintf ( obchep, "");	// 080813

	sprintf ( gs1_leergut, "201" )	;	// Notremse
	for ( i = 0 ; i < MAXLGZAHL ; i ++ )
	{
		if ( lgart[i] == 0 )
				break ;
		if (lgart[i] == inta )
		{
			sprintf ( gs1_leergut,"%s", lgname[i] ) ;
			sprintf ( obchep,"%s", lgchep[i] ) ;	// 080813
			break ;
		}
	}
	return gs1_leergut ;
}


void sysparlesen (void)
{

	int i ;
// Wiechmann-spezifika
	sprintf ( sys_par.sys_par_nam, "wiechmann_par" );
	i = sys_par_class.readfirst () ;
	sypwiechmann_par = 0 ;
	if ( !i)
	{
		if ( sys_par.sys_par_wrt[0] == '1' )		 sypwiechmann_par = 1 ;
	}


// Dietz-SL-Umgebung	250613
	sprintf ( sys_par.sys_par_nam, "dietzsch_par" );
	i = sys_par_class.readfirst () ;
	sypdietzsch_par = 0 ;
	if ( !i)
	{
		if ( sys_par.sys_par_wrt[0] == '1' )		 sypdietzsch_par = 1 ;
	}
	
	
// Dietz-spezifika
	sprintf ( sys_par.sys_par_nam, "dietzaku" );
	i = sys_par_class.readfirst () ;
	sypdietzaku = 0 ;
	if ( !i)
	{
		if ( sys_par.sys_par_wrt[0] == '1' )		 sypdietzaku = 1 ;
	}

// Boesinger-spezifika
	sprintf ( sys_par.sys_par_nam, "boes_par" );
	i = sys_par_class.readfirst () ;
	sypboes_par = 0 ;
	if ( !i)
	{
		if ( sys_par.sys_par_wrt[0] == '1' )		 sypboes_par = 1 ;
	}

// metro1_par : mit Teilsortimentstrennung( :=0 ) ODER eigner Inka je MGE-Nummer ( :=1 ) 
	sprintf ( sys_par.sys_par_nam, "metro1_par" );
	i = sys_par_class.readfirst () ;
	sypmetro1_par = 0 ;
	if ( !i)
	{
		if ( sys_par.sys_par_wrt[0] == '1' )		 sypmetro1_par = 1 ;
	}

// nachkpreis : Nachkommastellen im WA 
	sprintf ( sys_par.sys_par_nam, "nachkpreis" );
	i = sys_par_class.readfirst () ;
	sypnachkpreis = 2 ;
	if ( !i)
	{
		if ( sys_par.sys_par_wrt[0] == '3' )		 sypnachkpreis = 3 ;
		if ( sys_par.sys_par_wrt[0] == '4' )		 sypnachkpreis = 4 ;
	}

	sprintf ( sys_par.sys_par_nam, "li_a_edi" );
	i = sys_par_class.readfirst () ;
	sypli_a_edi = 0 ;
	if ( !i)
	{
		if ( sys_par.sys_par_wrt[0] == '1' )		 sypli_a_edi = 1 ;
	}

	sprintf ( sys_par.sys_par_nam, "li_a_par" );
	i = sys_par_class.readfirst () ;
	sypli_a_par = 0 ;
	if ( !i)
	{
		if ( sys_par.sys_par_wrt[0] == '1' )		 sypli_a_par = 1 ;
	}

// 270313
// Rheinstetten-dta
	sprintf ( sys_par.sys_par_nam, "rheinstetten" );
	i = sys_par_class.readfirst () ;
	rheinstettendta = -1 ;
	if ( !i)
	{
		if ( sys_par.sys_par_wrt[0] == '1' )
			rheinstettendta = atol ( sys_par.sys_par_besch ) ;	// da muss der rheinstetten-dta drin stehen 	
	}





// eana_par : Hole ean aus a_kun 
	sypeana_par = 0 ;
	sprintf ( sys_par.sys_par_nam, "eana_par" );
	i = sys_par_class.readfirst () ;
	if ( !i)
	{
		if ( sys_par.sys_par_wrt[0] == '1' )		 sypeana_par = 1 ;
	}

// eane_par : Hole ean aus a_ean 
	sypeana_par = 0 ;
	sprintf ( sys_par.sys_par_nam, "eane_par" );
	i = sys_par_class.readfirst () ;
	if ( !i)
	{
		if ( sys_par.sys_par_wrt[0] == '1' )		 sypeane_par = 1 ;
	}

}

void protschreiben (int typ )
{
	// -1 : Initialisierung
	// 0 : Normal-Verlauf 
	// 1 : bug

	if ( typ == -1 )
	{

		sprintf ( tempdrx.form_nr, "desadvpro") ;	// Orders-Protokoll

		tempdrx.lila = 0 ;

		tempdrx_class.deletetempdrx () ;

		int kk = tempdrx_class.lesetempdrx ()	;
		tempdrx.lfd ++ ;	// Max + 1
		lfdliste = tempdrx.lfd ;
		lfdlistennr =  0 ;
		return ;
	}

	memcpy ( &tempdrx , &tempdrx_null, sizeof ( struct TEMPDRX )) ;
	sprintf ( tempdrx.form_nr, "desadvpro") ;	// desadv-Protokoll
	tempdrx.lila = 0 ;
	tempdrx.lfd = lfdliste;
	tempdrx.lfd10 = lfdlistennr ;
	lfdlistennr ++ ;

	tempdrx.lfd01 = typ ;

// integer-Felder
	tempdrx.lfd02 = inummer ;
	tempdrx.lfd03 = lsk.kun ;
	tempdrx.lfd04 = lsk.ls ;
	tempdrx.lfd05 = lsk.adr ;
	tempdrx.lfd06 = dta.dta ;
	tempdrx.lfd09 = lsk.auf ;
//	sprintf ( tempdrx.cfeld201 , "%s", sqldatdstger ( &lsk.lieferdat , tempdrx.cfeld201)) ;
	sqldatdstger ( &lsk.lieferdat , tempdrx.cfeld201) ;
	char nothilfe[33] ;
	sprintf ( nothilfe,"%s",lsk.auf_ext ) ;
	nothilfe[24] = '\0' ;
	// lsk.auf_ext ist 30 lang, cfeld204 nur 24 ......)
	sprintf ( tempdrx.cfeld204, "%s", nothilfe ) ; 
	sprintf ( tempdrx.cfeld301 , "" ) ;
	sprintf ( tempdrx.cfeld302 , "" ) ;
	sprintf ( tempdrx.cfeld303 , "" ) ;
	if ( typ == 1 )
	{
		if ( (strlen (clippedi( protokoll ))) > 99 )
			protokoll[99] = '\0' ;
		if ( (strlen (clippedi( protokoll ))) > 66 )
				sprintf ( tempdrx.cfeld303 , "%s" , protokoll + 66 ) ;

		if ( (strlen ( clippedi( protokoll ))) > 66 )
			protokoll[66] = '\0' ;
		if ( (strlen ( clippedi( protokoll ))) > 33 )
			sprintf ( tempdrx.cfeld302 , "%s" , protokoll + 33 ) ;

		if ( (strlen ( clippedi( protokoll ))) > 33 )
			protokoll[33] = '\0' ;
		sprintf ( tempdrx.cfeld301 , "%s" , protokoll  ) ;
	}

	int kk = tempdrx_class.inserttempdrx() ;
} 


void protdrucken ( void )
{
	char s1[288] ;
	char s2[299] ;


	sprintf ( s2, "%s\\desadvpro.prm", getenv ( "TMPPATH" ) ) ;

			
	FILE * fp2 ;
	fp2 = fopen ( s2 , "w" ) ;
	if ( fp2 == NULL )
	{
		return  ;
	}
	
	sprintf ( s1, "NAME desadvpro\n" ) ; 
	CString St = s1;
	St.MakeUpper() ;
	sprintf ( s1 ,"%s", St.GetBuffer(512)) ;
	fputs ( s1 , fp2 );

	fputs ( "LABEL 0\n" , fp2 ) ;

	sprintf ( s1, "DRUCK 1\n"   ) ;
	fputs ( s1 , fp2 ) ;

	sprintf ( s1, "lfd %ld %ld\n", lfdliste , lfdliste   ) ;
	fputs ( s1 , fp2) ;

	fputs ( "ANZAHL 1\n" , fp2 ) ;

	fclose ( fp2) ;

	sprintf ( s1, "dr70001.exe -datei \"%s\" " , s2 );

	int ex_code = ProcWaitExec( (char *)s1, SW_SHOW, -1, 0, -1, 0 );
}


// hilfsproceduren zur  stringformatierung 

static char *wort[256];
static char buffer [0x1000] ;
static char DefWert [256] ;

// Naechstes Zeichen != Trennzeichen suchen.
int next_char_ci (char *string, char tzeichen, int i)
{
	for (;string [i]; i ++)
	{
		if (string[i] != tzeichen)
		{
			return (i);
		}
	}
	return (i);
}

//	der 2. Parameter ist dann ein endloser String bis zum #-Trenner oder CR/LF

short splite (char *string)
{
 short wz;                  // Wortzaehler  
 int i, j, len;
 static char zeichen = ' ';
 wz = j = 0;
 len = (int)strlen (string);
 wz = 1;
 i = next_char_ci (string, zeichen, 0);
 if (i >= len) return (0);
 wort [wz] =  buffer ;	
 wz ++;
 for (; i < len; i ++, j ++)
 {
	if (string [i] == zeichen)
	{
		i = next_char_ci (string, zeichen, i);
		if (i >= len) break;
			buffer [j] = (char) 0;
		j ++;
		wort [wz] = &buffer [j];
		wz ++;
		zeichen = '#' ;
	}
	buffer [j] = string [i];
 }
 buffer [j] = (char) 0;
 return (wz - 1);
}

short split (char *string)
{
 short wz;                  // Wortzaehler  
 int i, j, len;
 static char zeichen = ' ';
 wz = j = 0;
 len = (int)strlen (string);
 wz = 1;
 i = next_char_ci (string, zeichen, 0);
 if (i >= len) return (0);
 wort [wz] =  buffer ;	
 wz ++;
 for (; i < len; i ++, j ++)
 {
	if (string [i] == zeichen)
	{
		i = next_char_ci (string, zeichen, i);
		if (i >= len) break;
			buffer [j] = (char) 0;
		j ++;
		wort [wz] = &buffer [j];
		wz ++;
	}
	buffer [j] = string [i];
 }
 buffer [j] = (char) 0;
 return (wz - 1);
}

// vergleiche upper-strings
int strupcmpi (char *str1, char *str2, int len)
{
 short i;
 char upstr1;
 char upstr2;
 for (i = 0; i < len; i ++, str1 ++, str2 ++)
 {
	if (*str1 == 0)
		return (-1);
	if (*str2 == 0)
		return (1);
	upstr1 = toupper((int) *str1);
	upstr2 = toupper((int) *str2);
	if (upstr1 < upstr2)
	{
		return(-1);
	}
	else if (upstr1 > upstr2)
	{
		return (1);
	}
 }
 return (0);
}

// CR am Stringende etfernen.

void cr_weg (char *string)
{
 for (; *string; string += 1)
 {
	if (*string == (char) 13)
		break;
	if (*string == (char) 10)
		break;
 }
 *string = 0;
 return;
}
char *clippedi (char *string)
{
 char *clstring;
 short i,len;

 len = (short) strlen (string);

 if (len == 0) return (string);
 len --;
 clstring = string;
 for (i = len; i >= 0; i --)
 {
  if ((unsigned char) clstring[i] > 0x20)
  {
   break;
  }
 }
 clstring [i + 1] = 0;

 clstring = string;
 len = (short)strlen (clstring);

 for (i = 0; i < len; i ++, clstring +=1)
 {
  if ((unsigned char) *clstring > (unsigned char) 0X20)
  {
   break;
  }
 }
 return (clstring);
}

char *get_defa (char *env)
{         
 int anz;
 char puffer [512];
 FILE *fp;
 sprintf (puffer, "%s\\desadv.cfg",getenv ("BWSETC"));
 fp = fopen (puffer, "r");
 if (fp == (FILE *)NULL) return NULL;
 while (fgets (puffer, 511, fp))
 {
	cr_weg (puffer);
	anz = split (puffer);
	if (anz < 2) continue;
	if (strupcmpi (wort[1], env, (int) strlen (env)) == 0)
	{
		strcpy (DefWert, clippedi (wort [2]));
		fclose (fp);
        return (char *) DefWert;
	}
 }
 fclose (fp);
 return NULL;
}

void fuellefeld ( char * quelle , char * ziel , int solllen , char fuellfeld )
{
	// eineseits auffuellen mit folgeblanks oder nullen , andererseits Laengenlimit fixen 

	char buffl[280] ;
	sprintf ( buffl , "%s", quelle ) ;
	int po = 0 ;
	int gle = (int)strlen ( buffl ) ;
	int pi = 0 ;

	for (po = 0 ; po < solllen; po++ )
	{
		if ( pi < gle )
			ziel[po] = buffl[pi++] ;
		else
			ziel[po] = fuellfeld ;	// Alpha Feld mit " " fuellen, num-felder mit "0" fuellen  
	}
	ziel[po] = '\0';
}


void CdesadvDlg::paras (char * dta , char * mdn , char * ls  )
{
	// Lese die Kdo-Zeilenparameter und schreibe sie in interne Werte
	interndta = atol ( dta ) ;
	internmdn = atoi ( mdn ) ;
	internls  = atol  ( ls ) ;
	if ( ! interndta || ! internmdn || ! internls )
	{
		internmdn = 0 ;
		interndta = internls = 0 ;
	}
}



int CdesadvDlg::holedefaults (void )
{

	sysparlesen () ;

return 0 ;	// zun�chst erts mal dummy, kann bei Bedarf mit Leben erfuellt werden 
/* ---->
// Kundennummer holen z.B. dietz-Nummer
	char *kna =  get_defa ( "K_NR_AUFG" )	;
	if ( kna == NULL )
		return -1  ;
	else
		sprintf ( buffer , "%s" , kna ) ;

	fuellefeld ( buffer , abkda , 8 , '0') ;

// Nummer der Dachser-Niederlassung

	char *kns =  get_defa ( "K_NR_SPED" )	;
	if ( kns == NULL )
		return -1  ;
	else
		sprintf ( buffer , "%s" , kns ) ;

	fuellefeld ( buffer , dtkda , 8 , '0') ;
< ----- */

		return 0 ;
}

BOOL datumcheck(char * bufi )
{
	int i = (int) strlen(bufi) ;
	int hil1, hil2 ;
	int k = 0 ;	// 261007 : Zusatzchecks
	BOOL j = FALSE ;

	char hil5 [5] ;
	if ( i != 6 && i != 8 && i != 10 )
	{
		if ( i < 6 ) k = -1 ;
		if ( k == 0 )
		{
			// die ersten beiden Zeichen MUESSEN ZAHLEN sein
			if ( bufi[0] > '9' || bufi[0] < '0' ||bufi[1] > '9' || bufi[1] < '0' )
				k = - 1 ;
		}
		if ( k == 0 )
		{
			if ( bufi[2] > '9' || bufi[2] < '0' )
				k = 2 ;		// Typ 8 oder typ 10 moeglich

		}
		if ( k == 0 )
		{
			// nur noch Typ 6 erlaubt 
			if (    bufi[3] > '9' || bufi[3] < '0'
				 || bufi[4] > '9' || bufi[4] < '0'
				 || bufi[5] > '9' || bufi[5] < '0' )
			{
				k = - 1 ;
			}
			else
			{	// Gueltiger Typ 6  ......
				k = 1 ;
				i = 6 ;
				bufi[6] = '\0' ;
			}
		}
		if ( k == 2 )
		{
			if ( i < 8 )
			{	
				k = -1 ;
			}
			else
			{	// 4.,5.,7.,8. Zeichen MUESSEN ZAHLEN sein, 6.Zeichen MUSS Nicht-Zahl sein
				if (   bufi[3] > '9' || bufi[3] < '0' 
					 ||bufi[4] > '9' || bufi[4] < '0'
				     ||bufi[6] > '9' || bufi[6] < '0'
				     ||bufi[7] > '9' || bufi[7] < '0'
				     || !( bufi[5] >'9' || bufi[5]< '0')
				    )
				k = - 1 ;
			}
		}
		if ( k == 2 )
		{
			if ( bufi[8] > '9' || bufi[8] < '0' )
			{	// gueltiger Typ 8 
				k = 1 ;
				i = 8 ;
				bufi[8] = '\0' ;
			}
		
		}

		if ( k == 2 )
		{
			if ( i < 10 )
			{	
				k = -1 ;
			}
			else
			{	// 9.,10. Zeichen MUESSEN ZAHLEN sein
				if (   bufi[8] > '9' || bufi[8] < '0' 
					 ||bufi[9] > '9' || bufi[9] < '0'
			       )
				{
					k = -1 ;
				}
				else
				{
					k = 1 ;
					i = 10 ;
					bufi[10] = '\0' ;
				}
			}
		}
		if ( k < 1 )
		{
			bufi[0] = '\0' ;
			return TRUE ;
		}
	}
	if ( i == 6 )	// nur ddmmyy erlaubt 
	{
		bufi[10] = '\0' ;
		bufi[ 9] = bufi[5] ;
		bufi[ 8] = bufi[4] ;
		if ( bufi[ 8] > '5' )
		{
			bufi[ 6] = '1' ;
			bufi[ 7] = '9' ;
		}
		else
		{
			bufi[ 6] = '2' ;
			bufi[ 7] = '0' ;
		}
		bufi[ 5] = '.' ;
		bufi[ 4] = bufi[3] ;
		bufi[ 3] = bufi[2] ;
		bufi[ 2] = '.' ;
		i = 10 ;
		j = TRUE ; 
	}
	if ( i == 8 )	// nur dd,mm,yy erlaubt 
	{
		bufi[10] = '\0' ;
		bufi[ 9] = bufi[7] ;
		bufi[ 8] = bufi[6] ;
		if ( bufi[ 8] > '5' )
		{
			bufi[ 6] = '1' ;
			bufi[ 7] = '9' ;
		}
		else
		{
			bufi[ 6] = '2' ;
			bufi[ 7] = '0' ;
		}
		bufi[ 5] = '.' ;
		bufi[ 2] = '.' ;
		i = 10 ;
		j = TRUE ;
	}
	if ( i == 10 )	// hier kommt nix anderes mehr an ....  
	{
		if (   bufi[0] > '9' || bufi[0] < '0' 
			|| bufi[1] > '9' || bufi[1] < '0'
			|| bufi[3] > '9' || bufi[3] < '0'
			|| bufi[4] > '9' || bufi[4] < '0'
			|| bufi[6] > '9' || bufi[6] < '0'
			|| bufi[7] > '9' || bufi[7] < '0'
			|| bufi[8] > '9' || bufi[8] < '0'
			|| bufi[9] > '9' || bufi[9] < '0'
			)
		{

			bufi[0] = '\0' ;
			return TRUE ;
		}

		hil5[0] = bufi[3] ;
		hil5[1] = bufi[4] ;
		hil5[2] = '\0' ;
		hil1 = atoi (hil5);
		if ( hil1 > 12 || hil1 < 1 )
		{
			bufi[0] = '\0' ;
			return TRUE ;
		}

		hil5[0] = bufi[0] ;
		hil5[1] = bufi[1] ;
		hil5[2] = '\0' ;
		hil2 = atoi (hil5);
		if ( hil2 > 31 || hil1 < 1 )
		{
			bufi[0] = '\0' ;
			return TRUE ;
		}
		switch ( hil1 )
		{
		case  4 :
		case  6 :
		case  9 :
		case 11 :
			if ( hil2 > 30 )
			{
				bufi[0] = '\0' ;
				return TRUE ;
			}
			break ;
		case 2 :

			hil5[0] = bufi[6] ;
			hil5[1] = bufi[7] ;
			hil5[2] = bufi[8] ;
			hil5[3] = bufi[9] ;
			hil5[4] = '\0' ;
			hil1    = atoi(hil5) ;
			if ( hil2 > 28 )	// IM Jahr 2000 geht dieser Check schief .....
			{
				if ( ! ( hil1 % 4) )
				{
					if ( hil2 > 29 )
					{
						bufi[0] = '\0' ;
						return TRUE ;
					}
				}
				else
				{
					bufi[0] = '\0' ;
					return TRUE ;
				};
			}
			break ;

		}

	}
	return j ;

}

DWORD ProcWaitExec (LPSTR prog, WORD SHOW_MODE, int x, int y, int cx, int cy)
/**
Fenster-Process starten und nicht auf Ende Warten.
**/
{
        STARTUPINFO sti;
        PROCESS_INFORMATION pi;
        DWORD ExitCode; 
        int ret;

        ZeroMemory (&sti, sizeof (sti));
        sti.cb = sizeof (sti);
        if (x != -1)
        {
                     sti.dwFlags |= STARTF_USEPOSITION;
                     sti.dwX = x;
                     sti.dwY = y;
        }

        if (cx != -1)
        {
                     sti.dwFlags |= STARTF_USESIZE;
                     sti.dwXSize = cx;
                     sti.dwYSize = cy;
        }
        sti.wShowWindow = SHOW_MODE;
        sti.dwFlags |= STARTF_USESHOWWINDOW;

        ret = CreateProcess (NULL,
                       prog,
                       NULL,
                       NULL,
                      (int) NULL,
                      (unsigned long) NULL,
                       NULL,
                       NULL,
                       &sti,
                       &pi);
        if (ret)
        {
                  CloseHandle (pi.hThread);
                  WaitForSingleObject (pi.hProcess, INFINITE);
        }
		else
        {
			      return -1;
        }				  
		GetExitCodeProcess (pi.hProcess, &ExitCode);
		while (ExitCode == STILL_ACTIVE)
		{
                  GetExitCodeProcess (pi.hProcess, &ExitCode);
		}
        CloseHandle (pi.hProcess);
        return ExitCode;
}

void CdesadvDlg::ReadMdn (void) 
{
	mdn_class.openmdn();
	if (! mdn_class.lesemdn())
	{
		adr.adr = mdn.adr ;
		int i = adr_class.openadr () ;
		i = adr_class.leseadr () ;
		if (!i) 
		{
			v_mdnname.Format("%s",_T(adr.adr_krz));
		}
		else
		{
			v_mdnname.Format("              ");
			MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
//	gibbet nich		PrevDlgCtrl();
		}
	}
	else	// fehlerhafte Eingabe
	{
		v_mdnname.Format("              ");
		MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
// gibbet nich 		PrevDlgCtrl();
	}
		UpdateData (FALSE) ;
}




// CdesadvDlg-Dialogfeld

CdesadvDlg::CdesadvDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CdesadvDlg::IDD, pParent)
	, v_mdnnr(_T(""))
	, v_mdnname(_T(""))
	, v_tour(_T(""))
	, v_vdat(_T(""))
	, v_bdat(_T(""))
	, v_melde(_T(""))
	, v_combo1(_T(""))
	, v_meldek(_T(""))
	, v_meldep(_T(""))
	, v_checkeinzel(FALSE)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CdesadvDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_MDNNR, m_mdnnr);
	DDX_Text(pDX, IDC_MDNNR, v_mdnnr);
	DDV_MaxChars(pDX, v_mdnnr, 8);
	DDX_Control(pDX, IDC_MDNNAME, m_mdnname);
	DDX_Text(pDX, IDC_MDNNAME, v_mdnname);
	DDX_Control(pDX, IDC_TOUR, m_tour);
	DDX_Text(pDX, IDC_TOUR, v_tour);
	DDX_Control(pDX, IDC_VDAT, m_vdat);
	DDX_Text(pDX, IDC_VDAT, v_vdat);
	DDX_Control(pDX, IDC_BDAT, m_bdat);
	DDX_Text(pDX, IDC_BDAT, v_bdat);
	DDX_Control(pDX, IDC_MELDE, m_melde);
	DDX_Text(pDX, IDC_MELDE, v_melde);
	DDX_Control(pDX, IDC_COMBO1, m_combo1);
	DDX_CBString(pDX, IDC_COMBO1, v_combo1);
	DDX_Text(pDX, IDC_MELDEK, v_meldek);
	DDX_Text(pDX, IDC_MELDEP, v_meldep);
	DDX_Control(pDX, IDC_EINZEL, m_check);
	DDX_Check(pDX, IDC_EINZEL, v_checkeinzel);
	DDX_Control(pDX, IDC_STATICTour, m_statictour);
}

BEGIN_MESSAGE_MAP(CdesadvDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_EN_KILLFOCUS(IDC_MDNNR, &CdesadvDlg::OnEnKillfocusMdnnr)
	ON_EN_KILLFOCUS(IDC_TOUR, &CdesadvDlg::OnEnKillfocusTour)
	ON_EN_KILLFOCUS(IDC_VDAT, &CdesadvDlg::OnEnKillfocusVdat)
	ON_EN_KILLFOCUS(IDC_BDAT, &CdesadvDlg::OnEnKillfocusBdat)
	ON_BN_CLICKED(IDCANCEL, &CdesadvDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDOK, &CdesadvDlg::OnBnClickedOk)
	ON_CBN_SELCHANGE(IDC_COMBO1, &CdesadvDlg::OnCbnSelchangeCombo1)
	ON_CBN_KILLFOCUS(IDC_COMBO1, &CdesadvDlg::OnCbnKillfocusCombo1)
	ON_BN_CLICKED(IDC_EINZEL, &CdesadvDlg::OnBnClickedEinzel)
END_MESSAGE_MAP()


// CdesadvDlg-Meldungshandler

BOOL CdesadvDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Hinzuf�gen des Men�befehls "Info..." zum Systemmen�.

	// IDM_ABOUTBOX muss sich im Bereich der Systembefehle befinden.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Symbol f�r dieses Dialogfeld festlegen. Wird automatisch erledigt
	//  wenn das Hauptfenster der Anwendung kein Dialogfeld ist
	SetIcon(m_hIcon, TRUE);			// Gro�es Symbol verwenden
	SetIcon(m_hIcon, FALSE);		// Kleines Symbol verwenden

	// TODO: Hier zus�tzliche Initialisierung einf�gen

	dbClass.opendbase (_T("bws"));


	  // alle dta lesen



	CString szdta ;

	int sqlstat = dta_class.openalldta ();
	sqlstat = dta_class.lesealldta();
	while(!sqlstat)
	{
	
		szdta.Format("%8.0d  %s",dta.dta,dta.kun_krz1);
				// hier haben wir jetzt die dta und k�nnen es in die ListBox einf�gen
		((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->AddString(szdta.GetBuffer(0));
		((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->SetCurSel(0);
	
		sqlstat = dta_class.lesealldta () ;
	}

// m_bisdatum.ShowWindow(SW_HIDE);
//	m_stapnu.ShowWindow(SW_NORMAL);


	if ( internmdn > 0 )
	{
		mdn.mdn = internmdn ;
		v_mdnnr.Format ("%d" , internmdn) ;
	}
	else
	{
		mdn.mdn = 1 ;
		v_mdnnr = "1" ;
	}
 	ReadMdn ();

/* ---> so kann man Systemparameter lesen ......
	dnachkpreis = atoi ( sys_par_class.sys_par_holen ( "nachkpreis" ))  ;
	if ( dnachkpreis > 4 || dnachkpreis < 2 ) dnachkpreis = 2 ;
< -------- */


	if ( internmdn > 0 && internls > 0 && interndta > 0 )
	{


			dta.dta = interndta  ;
			int i = dta_class.opendta();
			i = dta_class.lesedta();
			if ( i )
			{
				v_combo1.Format("                   ");
				MessageBox("Ung�ltige DTA-Eingabe !", " ", MB_OK|MB_ICONSTOP);
				UpdateData(FALSE) ;
				OnBnClickedCancel() ;
				return TRUE ;
			}

			lsk.mdn = internmdn ;
			sprintf ( tourbeding , "and lsk.ls = %d" ,internls ) ;
			sprintf ( kuninka.sedas_nr3 ,"%d", interndta ) ;
			lsk.vondate.fraction = 0 ;
			lsk.vondate.second = 0 ;
			lsk.vondate.minute = 0 ;
			lsk.vondate.hour = 0 ;
			lsk.vondate.day = 1 ;
			lsk.vondate.month = 1 ;
			lsk.vondate.year = 1990 ;

			lsk.bisdate.fraction = 0 ;
			lsk.bisdate.second = 0 ;
			lsk.bisdate.minute = 0 ;
			lsk.bisdate.hour = 0 ;
			lsk.bisdate.day = 31 ;
			lsk.bisdate.month = 12 ;
			lsk.bisdate.year = 2049 ;

			int obdefa = holedefaults () ;

			rheinstetten = 0 ;					// 250613		
			if ( rheinstettendta == dta.dta )	// 250613
				rheinstetten = 1 ;


			Dateierstellung () ;

			OnBnClickedCancel() ;
			return TRUE ;
	}


	x_checkeinzel = FALSE ;

	m_vdat.EnableWindow( TRUE );
	m_bdat.EnableWindow( TRUE );
//	m_statictour.SetDlgItemTextA(IDC_STATICTour,"Tour :") ;


	return TRUE;  // Geben Sie TRUE zur�ck, au�er ein Steuerelement soll den Fokus erhalten
}

void CdesadvDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// Wenn Sie dem Dialogfeld eine Schaltfl�che "Minimieren" hinzuf�gen, ben�tigen Sie 
//  den nachstehenden Code, um das Symbol zu zeichnen. F�r MFC-Anwendungen, die das 
//  Dokument/Ansicht-Modell verwenden, wird dies automatisch ausgef�hrt.

void CdesadvDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Ger�tekontext zum Zeichnen

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Symbol in Clientrechteck zentrieren
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Symbol zeichnen
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// Die System ruft diese Funktion auf, um den Cursor abzufragen, der angezeigt wird, w�hrend der Benutzer
//  das minimierte Fenster mit der Maus zieht.
HCURSOR CdesadvDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CdesadvDlg::OnEnKillfocusMdnnr()
{
	UpdateData (TRUE) ;

	int i = m_mdnnr.GetLine(0,bufh,500);
	bufh[i] = '\0' ;
	if (i)	mdn.mdn = (short) atoi ( bufh );
	else mdn.mdn = -2 ;
	mdn_class.openmdn();
	if (! mdn_class.lesemdn())
	{
		adr.adr = mdn.adr ;
		i = adr_class.openadr () ;
		i = adr_class.leseadr () ;
		if (!i) 
		{
			v_mdnname.Format("%s",_T(adr.adr_krz));
		}
		else
		{
			v_mdnname.Format("              ");
			MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
// gibbet nich			PrevDlgCtrl();
		}
	}
	else	// fehlerhafte Eingabe
	{
		v_mdnname.Format("              ");
		MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
// gibbet nich		PrevDlgCtrl();
	}

	v_melde.Format( " " ) ;
	v_meldek.Format( " " ) ;
	v_meldep.Format( " " ) ;
	UpdateData (FALSE) ;
}

void CdesadvDlg::OnEnKillfocusTour()
{
	UpdateData (TRUE) ;
	v_melde.Format( " " ) ;
	UpdateData (FALSE) ;

}

void CdesadvDlg::OnEnKillfocusVdat()
{
	UpdateData(TRUE) ;
	int	i = m_vdat.GetLine(0,bufh,500) ;
	bufh[i] = '\0' ;

	if (i)
	{
		if (datumcheck(bufh))
		{
			v_vdat.Format("%s",_T(bufh));
		}
	}
	v_melde.Format( " " ) ;
	UpdateData (FALSE) ;
}

void CdesadvDlg::OnEnKillfocusBdat()
{
	UpdateData(TRUE) ;
	int	i = m_bdat.GetLine(0,bufh,500) ;
	bufh[i] = '\0' ;
	if (i)
	{
		if (datumcheck(bufh))
		{
			v_bdat.Format("%s",_T(bufh));
		}
	}
	v_melde.Format( " " ) ;
	UpdateData (FALSE) ;
}

void CdesadvDlg::OnBnClickedCancel()
{
	v_melde.Format( " " ) ;
	v_meldek.Format( " " ) ;
	v_meldep.Format( " " ) ;

	OnCancel();
}

void CdesadvDlg::OnBnClickedOk()
{
	UpdateData(TRUE) ;


// 150313 A
	if ( x_checkeinzel == TRUE )
	{	// es gelten nur noch Mandant, dta und LS-Nummer 
		int i ;
// Dta testen
		bufh[0] = '\0' ;
		int nCurSel = ((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->GetCurSel();
		CString bufx;
		if ( nCurSel > -1 )
		{
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->GetLBText(nCurSel, bufx);

			sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
			i = (int) strlen ( bufh );
// fixe formatierung : 8 stellen und 2 blanks , danach folgt ein text
			if ( i )
			{
				if ( i < 9 )
					bufh[i ] = '\0' ;
				else
					bufh[9] = '\0' ;

				dta.dta = atol ( bufh ) ;
				i = dta_class.opendta();
				i = dta_class.lesedta();
				if ( i )
				{
					v_combo1.Format("                   ");
					MessageBox("Ung�ltige DTA-Eingabe !", " ", MB_OK|MB_ICONSTOP);
					UpdateData(FALSE) ;
					return ;
				}
				else
				{
					// alles ok.
				}
			}
			else
			{
				v_combo1.Format("                   ");
				MessageBox("Ung�ltige DTA-Eingabe !", " ", MB_OK|MB_ICONSTOP);
				UpdateData(FALSE) ;
				return ;
			}
		}
		else
		{
			sprintf ( bufh, "%s", v_combo1 );
			i = (int) strlen ( bufh );
			// fixe formatierung : 8 stellen und 2 blanks , danach folgt ein text
			if ( i )
			{
				if ( i < 9 )
					bufh[i ] = '\0' ;
				else
					bufh[9] = '\0' ;
			}
			long hilfe = atol ( bufh ) ;
			nCurSel = -1 ;
			sprintf ( bufh, "%8.0d", hilfe );
			nCurSel=((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->FindString(nCurSel, bufh);
			if (nCurSel != CB_ERR)
			{
				((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->SetCurSel(nCurSel) ;
				((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->GetLBText(nCurSel, bufx);
				sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
				i = (int) strlen ( bufh );
			// fixe formatierung : 8 stellen und 2 blanks , danach folgt ein text
			// eigentlich kann hier nur noch true ankommen und verarbeitet werden
				if ( i )
				{
					if ( i < 9 )
						bufh[i] = '\0' ;
					else
						bufh[9] = '\0' ;

					dta.dta = atol ( bufh ) ;
					i = dta_class.opendta();
					i = dta_class.lesedta();
					if ( i )
					{
						v_combo1.Format("                   ");
						MessageBox("Ung�ltige DTA-Eingabe !", " ", MB_OK|MB_ICONSTOP);
						UpdateData(FALSE) ;
						return ;
					}
					else
					{
						// alles ok.
					}
				}
				else
				{
					v_combo1.Format("                   ");
					MessageBox("Ung�ltige DTA-Eingabe !", " ", MB_OK|MB_ICONSTOP);
					UpdateData(FALSE) ;
					return ;
				}
			}
			else
			{
				v_combo1.Format("                   ");
				MessageBox("Ung�ltige DTA-Eingabe !", " ", MB_OK|MB_ICONSTOP);
				UpdateData(FALSE) ;
				return ;
			}
		}

		lsk.mdn = mdn.mdn ;

		i = m_tour.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		long longls = atol ( bufh) ;
		if ( longls < 1 )
		{
			MessageBox("Ung�ltige Lieferscheinnummer !", " ", MB_OK|MB_ICONSTOP);
				UpdateData(FALSE) ;
				return ;
		}
		sprintf ( tourbeding , "and lsk.ls = %d" , longls ) ;
		sprintf ( kuninka.sedas_nr3 ,"%d", dta.dta ) ;
		lsk.vondate.fraction = 0 ;
		lsk.vondate.second = 0 ;
		lsk.vondate.minute = 0 ;
		lsk.vondate.hour = 0 ;
		lsk.vondate.day = 1 ;
		lsk.vondate.month = 1 ;
		lsk.vondate.year = 1990 ;

		lsk.bisdate.fraction = 0 ;
		lsk.bisdate.second = 0 ;
		lsk.bisdate.minute = 0 ;
		lsk.bisdate.hour = 0 ;
		lsk.bisdate.day = 31 ;
		lsk.bisdate.month = 12 ;
		lsk.bisdate.year = 2049 ;

		int obdefa = holedefaults () ;

		rheinstetten = 0 ;					// 250613		
		if ( rheinstettendta == dta.dta )	// 250613
			rheinstetten = 1 ;

		int retco = Dateierstellung () ;
		if ((strlen ( ddateiname )) > 3 )
		{
			v_melde.Format( " Datei %s erstellt " , ddateiname ) ;
			v_meldek.Format( " Es wurden %4d von %4d Belegen    verarbeitet " , kgescount - kbadcount , kgescount  ) ;
			v_meldep.Format( " Es wurden %4d von %4d Positionen verarbeitet " , gescount - badcount , gescount ) ;
		}
		else
		{
			v_melde.Format( " keine Datei erstellt " ) ;
			v_meldek.Format ( " " ) ;
			v_meldep.Format ( " " ) ;
		}
		UpdateData(FALSE) ;
			return ;

	}
// 150313 E

	int	i = m_bdat.GetLine(0,bufh,500) ;
	bufh[i] = '\0' ;
	int	j = m_vdat.GetLine(0,bufh,500) ;
	bufh[j] = '\0' ;
	if ( i != 10  && j != 10 )
	{
		MessageBox("Ung�ltige Datumseingabe !", " ", MB_OK|MB_ICONSTOP);
		return ;
	}
	if ( i != 10 )
	{
			v_bdat = v_vdat ;
	}
	else if (j != 10 )
	{
			v_vdat = v_bdat ;

	}
 
	sqldatdst(  & lsk.vondate , v_vdat.GetBuffer()) ;
	sqldatdst(  & lsk.bisdate , v_bdat.GetBuffer()) ;

	int igef , mufo ,iok ;	// es Muss was sinnvolles eingegeben werden, sonst wird gemeckert

	// igef : irgendeine Zahl gefunden 
	// mufo == 0 -> es darf space oder nummer folgen ( vorblanks )
	// mufo == 3 -> es darf space oder nummer folgen (nachkomma) 
	// mufo == 1 -> es darf nummer, space oder komma folgen ( im wert )
	// mufo == 2 -> es darf space oder komma folgen ( nach wert , vor folgekomma )
	//  iok muss == 1 sein und igef muss > 0 und mufo darf nicht == 3 sein 


	i = m_tour.GetLine(0,bufh,500) ;
	bufh[i] = '\0' ;
	igef =  mufo = 0 ;
	iok = 1 ;

	for ( int j = 0 ; j < i ; j++ )
	{
		if (( bufh[j] > '9' ) || ( bufh[j] < '0' ))
		{
	// mufo == 0 -> es darf space oder nummer folgen ( vorblanks )
	// mufo == 3 -> es darf space oder nummer folgen (nachkomma) 
	// mufo == 1 -> es darf nummer, space oder komma folgen ( im wert )
	// mufo == 2 -> es darf space oder komma folgen ( nach wert , vor folgekomma )
			if ( bufh[j] == ' ')
			{
				if ( mufo == 1 )	// Zahl zuende
				{
					mufo = 2 ;
					continue ;
				}
				else
				{
					continue ;	// alle anderen Space-Zustaende
				}
			};				
			if ( bufh[j] == ',')
			{
	// mufo == 0 -> es darf space oder nummer folgen ( vorblanks )
	// mufo == 1 -> es darf nummer, space oder komma folgen ( im wert )
	// mufo == 2 -> es darf space oder komma folgen ( nach wert , vor folgekomma )
	// mufo == 3 -> es darf space oder nummer folgen (nachkomma) 

				if ( mufo == 0 )	// Komma vor erster zahl
				{
					iok = 0 ;
					break ;
				}
				if ( mufo == 1 )	// Komma am Zahlenende
				{
					mufo = 3 ;
					continue ;
				}
				if ( mufo == 2 )	// gueltiges komma nach space 
				{
					mufo = 3 ;
					continue ;
				}
				if ( mufo == 3 )	// Doppelkomma 
				{
					iok = 0 ;
					break ;
				}
			};
			iok = 0 ;
			break ;	// andere Zeichen
		}
		else	// nummer 
		{
			if ( mufo == 0 )	// vorblanks sind vorbei
			{
				mufo = 1 ;
				igef = 1 ;
				 continue ;
			}
			if ( mufo == 1 )	// mitten in einer Zahl
			{
				 continue ;
			}

			if ( mufo == 2 )	// space zwischen 2 Zahlen -> Abbruch
			{
				iok = 0 ;
				 break ;
			}
			if ( mufo == 3 )	// neue Zahl beginnt
			{
				mufo = 1 ;
				 continue ;
			}
		}

	}
	if ( ! iok || ! igef  || mufo == 3 )
	{
		MessageBox("Ung�ltige Toureingabe !", " ", MB_OK|MB_ICONSTOP);
		return ;
	}
	else
		sprintf ( tourbeding , "and lsk.tou in (%s)" ,bufh ) ;


// Dta testen

	bufh[0] = '\0' ;

	int nCurSel = ((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->GetCurSel();
	CString bufx;
	if ( nCurSel > -1 )
	{
		((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->GetLBText(nCurSel, bufx);

		sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
		i = (int) strlen ( bufh );
		// fixe formatierung : 8 stellen und 2 blanks , danach folgt ein text
		if ( i )
		{
			if ( i < 9 )
				bufh[i ] = '\0' ;
			else
				bufh[9] = '\0' ;

			dta.dta = atol ( bufh ) ;
			i = dta_class.opendta();
			i = dta_class.lesedta();
			if ( i )
			{
				v_combo1.Format("                   ");
				MessageBox("Ung�ltige DTA-Eingabe !", " ", MB_OK|MB_ICONSTOP);
				UpdateData(FALSE) ;
				return ;
			}
			else
			{
				// alles ok.
			}
		}
		else
		{
			v_combo1.Format("                   ");
			MessageBox("Ung�ltige DTA-Eingabe !", " ", MB_OK|MB_ICONSTOP);
			UpdateData(FALSE) ;
			return ;
		}
	}
	else
	{
		sprintf ( bufh, "%s", v_combo1 );
		i = (int) strlen ( bufh );
		// fixe formatierung : 8 stellen und 2 blanks , danach folgt ein text
		if ( i )
		{
			if ( i < 9 )
				bufh[i ] = '\0' ;
			else
				bufh[9] = '\0' ;
		}
		long hilfe = atol ( bufh ) ;
		nCurSel = -1 ;
		sprintf ( bufh, "%8.0d", hilfe );
		nCurSel=((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->FindString(nCurSel, bufh);
	
		if (nCurSel != CB_ERR)
		{
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->SetCurSel(nCurSel) ;
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->GetLBText(nCurSel, bufx);
			sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
			i = (int) strlen ( bufh );
			// fixe formatierung : 8 stellen und 2 blanks , danach folgt ein text
			// eigentlich kann hier nur noch true ankommen und verarbeitet werden
			if ( i )
			{
				if ( i < 9 )
					bufh[i] = '\0' ;
				else
					bufh[9] = '\0' ;
				dta.dta = atol ( bufh ) ;
				i = dta_class.opendta();
				i = dta_class.lesedta();
				if ( i )
				{
					v_combo1.Format("                   ");
					MessageBox("Ung�ltige DTA-Eingabe !", " ", MB_OK|MB_ICONSTOP);
					UpdateData(FALSE) ;
					return ;
				}
				else
				{
					// alles ok.
				}
			}
			else
			{
				v_combo1.Format("                   ");
				MessageBox("Ung�ltige DTA-Eingabe !", " ", MB_OK|MB_ICONSTOP);
				UpdateData(FALSE) ;
				return ;
			}
		}
		else
		{
			v_combo1.Format("                   ");
			MessageBox("Ung�ltige DTA-Eingabe !", " ", MB_OK|MB_ICONSTOP);
			UpdateData(FALSE) ;
			return ;
		}
	}

		UpdateData(FALSE) ;

	int obdefa = holedefaults () ;
	if ( obdefa )
	{
		MessageBox("Datei desadv.cfg fehlerhaft !", " ", MB_OK|MB_ICONSTOP);
		return ;
	}
	rheinstetten = 0 ;					// 250613		
	if ( rheinstettendta == dta.dta )	// 250613
		rheinstetten = 1 ;

	int ret = Dateierstellung () ;

	if ((strlen ( ddateiname )) > 3 )
	{
		v_melde.Format( " Datei %s erstellt " , ddateiname ) ;
		v_meldek.Format( " Es wurden %4d von %4d Belegen    verarbeitet " , kgescount - kbadcount , kgescount  ) ;
		v_meldep.Format( " Es wurden %4d von %4d Positionen verarbeitet " , gescount - badcount , gescount ) ;
	}
	else
	{
		v_melde.Format( " keine Datei erstellt " ) ;
		v_meldek.Format ( " " ) ;
		v_meldep.Format ( " " ) ;
	}
	UpdateData(FALSE) ;

//	OnOK();
}
BOOL CdesadvDlg::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				CWnd *xWnd ;
				xWnd = GetFocus ();
			           if(xWnd == GetDlgItem (IDOK)) break ;			// ok - eigentlich nichts machen
// auf den ok-Knopf verlegt ....			           if(xWnd == GetDlgItem (IDC_AUSDRUCK)) break ;	// Ausduck halt
			           if(xWnd == GetDlgItem (IDCANCEL))
					   {
							GetParent ()->DestroyWindow ();
							return TRUE ;	// Abbruch halt ( genau wie ButtonClickedCancel) 
					   }
	
				if (OnReturn ())
				{
					return TRUE;
				}
			}
			else if (pMsg->wParam == VK_TAB)
			{

				break;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (OnKeyup ())
				{
					return TRUE;
				}
			}
			else if (pMsg->wParam == VK_F5)
			{
//				StepBack ();
				OnCancel () ;
//				GetParent ()->DestroyWindow ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				return TRUE;
			}
	}

	return CDialog::PreTranslateMessage(pMsg); 
}

BOOL CdesadvDlg::OnReturn ()
{
	CWnd *Control = GetFocus ();

	Control = GetNextDlgTabItem (Control, FALSE);

	if (Control != NULL)
	{
		Control->SetFocus ();
	}
	return TRUE;
}

BOOL CdesadvDlg::OnKeyup ()
{
	CWnd *Control = GetFocus ();

	Control = GetNextDlgTabItem (Control, TRUE);
	if (Control != NULL)
	{
		Control->SetFocus ();
	}
	return TRUE;
}

//  Ab hier beginnt die Erstellung der Datei :

int dtvorsatz;	// Vorsatz erfolgreich geschrieben -> impliziert, das auch was gefunden wurde

FILE * fpdatei ;


void SysDatum (char * Date)
{
	// hole Systemdatum zwecks Dateiname
 time_t timer;
 struct tm *ltime;

 time (&timer);
 ltime = localtime (&timer);
 sprintf ( Date,"%04d%02d%02d" , ltime->tm_year + 1900,
                                    ltime->tm_mon + 1,
                                    ltime->tm_mday);
}

void DachsDatum (int typ , char * quelle, char * ztag , char * zmon, char * zjahr , char * zjhd , char * zhh, char * zmin )
{

 time_t timer;
 struct tm *ltime;
 char hilfe[5] ;
	if ( typ == 1 )
	{	// hole systemdatum
		time (&timer);
		ltime = localtime (&timer);
// ich gehe spontan davon aus, das wir im 21. Jahrhundert leben .....

		sprintf ( zjahr , "%02d" , ltime->tm_year - 100 ) ;
		sprintf ( zjhd  , "20" ) ;	// das ist dann wohl fix
        sprintf ( zmon  , "%02d" , ltime->tm_mon + 1 ) ;
        sprintf ( ztag  , "%02d" , ltime->tm_mday );
        sprintf ( zhh   , "%02d" , ltime->tm_hour ) ;
		sprintf ( zmin  , "%02d" , ltime->tm_min );
	}
	else	// input ist ein zwingend string der form "dd.mm.yyyy hh:mn" oder "dd.mm.yyyy"
	{
		int gle = (int) strlen ( quelle ) ;

		hilfe[0] = quelle[0] ;
		hilfe[1] = quelle[1] ;
		hilfe[2] = '\0' ;
        sprintf ( ztag  , "%s" , hilfe );

		hilfe[0] = quelle[3] ;
		hilfe[1] = quelle[4] ;
		hilfe[2] = '\0' ;
        sprintf ( zmon  , "%s" , hilfe );

		hilfe[0] = quelle[6] ;
		hilfe[1] = quelle[7] ;
		hilfe[2] = '\0' ;
        sprintf ( zjhd  , "%s" , hilfe );

		hilfe[0] = quelle[8] ;
		hilfe[1] = quelle[9] ;
		hilfe[2] = '\0' ;
        sprintf ( zjahr  , "%s" , hilfe );
		if ( gle < 16 )
		{
			sprintf ( zhh  , "00" );
			sprintf ( zmin  , "00" );
		}
		else
		{
			hilfe[0] = quelle[11] ;
			hilfe[1] = quelle[12] ;
			hilfe[2] = '\0' ;
			sprintf ( zhh  , "%s" , hilfe );

			hilfe[0] = quelle[14] ;
			hilfe[1] = quelle[15] ;
			hilfe[2] = '\0' ;
			sprintf ( zmin  , "%s" , hilfe );
		}
	}
}


int CdesadvDlg::Dateitesten ( int modus , char * dateiname )
{

/* ob wir das noch so brauchen ? ---->

// modus == 0 : Testen  UND Dateiname erstellen
// modus == 1 : Loeschen
// return ==  0 : alles ok : Nix da und schreibbar	+ !!!!! fpdatei ist gueltig  
// return ==  1 : Problem : Datei gibbet bereits
// return == -1 : Problem : Datei nicht schreibbar 
	char basisname[200] ;


	char * p = getenv ( "DACHSER" ) ;
	if ( p == NULL ) 
	{	// Variable "DACHSER" nicht angelegt
		sprintf ( basisname, "%s\\dachser" , getenv ("TMPPATH")) ;
		_mkdir( basisname ) ;	// Prophylaktisch immer Verzeichnis erstellen
	}
	else
	{
		sprintf ( basisname, "%s" , p ) ;
	}

	if ( modus == 0 )
	{

		dbClass.beginwork () ;
		int ix = nvsimpel ( 0,0, "dachser", &vorsatznummer ) ;
		dbClass.commitwork () ;
		// bissel dirty, aber bevor 300 000 Dateien erzeugt wurden, ist bestimmt ein Meteor eingeschlagen

		if ( vorsatznummer == 100000 || vorsatznummer == 200000 )
		{	// die Nummer wird einfach verknallt
			dbClass.beginwork () ;
			ix = nvsimpel ( 0,0, "dachser", &vorsatznummer ) ;
			dbClass.commitwork () ;
		}

// Vorsatznummer darf maximal 5 stellen haben ..... 
		while ( vorsatznummer > 99999 )
		{
			vorsatznummer -= 100000 ;
		}
		long hvorsatznummer ;
		hvorsatznummer = vorsatznummer ;
		while ( hvorsatznummer > 9999 )
		{
			hvorsatznummer -= 10000 ;
		}
		while ( hvorsatznummer > 999 )
		{
			hvorsatznummer -= 1000 ;
		}

// Datei heisst immer "KUNDE&&&" laut Dachser

//		SysDatum ( filename ) ;
//		sprintf ( dateiname,"%s_Kunde.txt" , filename ) ;
		sprintf ( dateiname , "KUNDE%03d", hvorsatznummer ) ;
	}

	sprintf ( filename, "%s\\%s" , basisname , dateiname ) ;

	if ( modus == 1 )
	{
		unlink ( filename ) ;
	}

	fpdatei = fopen (filename, "r");
	if (fpdatei == (FILE *)NULL)
	{	// Schreibtest jedenfalls durchfuehren 

		if ((fpdatei = fopen( filename ,"w+" )) == (FILE *)NULL )
				return -1 ;	// Datei nicht schreibbar (z.B. Pfad oder Permissions )
		fclose ( fpdatei) ;
		unlink ( filename ) ;

// Zum Schluss bleibt die Datei leer, offen und aktiv
		if ((fpdatei = fopen( filename ,"w+" )) == (FILE *)NULL )
				return -1 ;	// Datei nicht schreibbar (z.B. Pfad oder Permissions )
		return 0 ;	// alles i.O.
	}
	else
	{
		fclose ( fpdatei) ;
		if ( modus == 0 )
		{
			return 1 ;	// Datei gibbet schon
		}
		else
			return -1 ;	// Problem beim loeschen oder anlegen 
	};
< ------ */

	return  -1  ;	// Syntax-Dummy "irgend ein Problem
}

int CdesadvDlg::opendatei ( void )
{
	int retcode = 0  ;	// 0 = ok ; 1 Datei existiert ; -1 Zugriffsproblem ;  2 = alles vorbei

	char Dateiname [300] ;

	char ctext [300] ;

	retcode =  Dateitesten(0,Dateiname) ;	// Existenztest
	if ( retcode == -1  )
	{
		sprintf ( ctext , "Zugriffsproblem Datei  %s", Dateiname ) ;
		MessageBox( ctext , " ", MB_OK|MB_ICONSTOP);
		retcode = -2 ;	// Durchmarsch
	}
	if ( retcode == 1 )
	{
		if ( MessageBox("Datei existiert bereits - vorhandene Datei l�schen ?? ", " ", MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2 ) == IDYES )
		{
			retcode =  Dateitesten(1,Dateiname) ;	// bitte loeschen
			if ( retcode == -1  )
			{
				sprintf ( ctext , "Zugriffsproblem Datei  %s", Dateiname ) ;
				MessageBox( ctext , " ", MB_OK|MB_ICONSTOP);
				retcode = -2 ;	// Durchmarsch
			}
		}
		else retcode = -2 ;	// Nicht loeschen - Durchmarsch
	}

	return retcode ;
/* ----->
	if ( retcode == 0 )
	{

		retcode = Dateischreiben (Dateiname) ;	// jetzt steht in retcode die Anzahl gechriebener Saetze

		if ( retcode < 0 )
		{
			Dateitesten (1,Dateiname);
			MessageBox("Datei kann nicht erstellt werden ", " ", MB_OK|MB_ICONSTOP);
		}
		if ( retcode == 0 )
		{
			Dateitesten (1, Dateiname );
			MessageBox("Datensatz nicht vorhanden ", " ", MB_OK|MB_ICONSTOP);
			retcode = -2 ;
		}
		if ( retcode > 0 )
		{
			char spruch [82] ;
			sprintf ( spruch , "Datei mit %d S�tzen wurde erstellt", retcode ) ;
			MessageBox( spruch , " ", MB_OK|MB_ICONSTOP);
			OnOK() ;
		}
	}

< ------ */

}

void holeeinhcode ( double px )
{
	for ( int ii = 0 ; ii < 25 ; ii ++ )
	{

		if ( px > meeinharti[ii] )
			if ( meeinharti[ii] > 0.0 )
				continue ;


		if ( px > meeinharti[ii] )	// schliesst 0 mit ein und ausserdem ist meinharti sortiert
		{	// eintrag existiert noch nicht 
			sprintf ( ptabn.ptitem , "me_einh_leed" ) ;
			sprintf ( ptabn.ptwer1 , "%1.0f" , px ) ;
			int dretpt = ptabn_class.openptabnw1 () ;
			dretpt = ptabn_class.leseptabnw1 () ;
			if ( dretpt )	// darf eigentlich nicht sein
				sprintf ( ptabn.ptwer2 ,"xxxxx" ) ;

			if ( meeinharti[ii] == 0.0 && ii == 0 )
			{	// bin bereits am Ende angekommen
				sprintf ( meeinhcode[ii] ,"%s" , ptabn.ptwer2 ) ;
				meeinharti[ii] = px ;
				return ;
			}

// sortieren und eintragen
			int jj = ii ;
			while ( meeinharti[jj ] > 0.0 )
			{
				jj ++ ;
				if ( jj > 24 ) return ;	// notbremse
			}	// Jetzt zeigt jj auf den ersten freien Platz

			while ( jj > 0 )
			{
				if ( px > meeinharti [ jj -1 ] )
				{
					sprintf ( meeinhcode[jj] ,"%s" , ptabn.ptwer2 ) ;
					meeinharti[jj] = px ;
					return ;
				}
				meeinharti[jj] = meeinharti[jj -1 ] ;
				sprintf( meeinhcode[jj] ,"%s", meeinhcode[jj - 1 ] ) ;
				jj -- ;
			}
			sprintf ( meeinhcode[jj] ,"%s" , ptabn.ptwer2 ) ;
			meeinharti[jj] = px ;
			return ;
		}

		if ( px == meeinharti[ii] )
		{
			sprintf ( ptabn.ptwer2 , "%s" , meeinhcode[ii] ) ;
			return ;
		}
	}
}

static char okstring[33] ;
static char okstring2[33] ;


char * generierenve ( char * innve )
{

int point1 ;
int  wicht ;
int platz ;
char testchars[2] ; 
int summe ;

  sprintf ( okstring , "%s" , innve ) ;
 
  point1 = 0 ;
  summe  = 0 ;
  wicht  = 3 ;

  // dier ersten 18 Stellen werden verknotet
  // falls der input-string kuerzer ist, wird er mit 000 aufgefuellt,
  // falls nicht-nummer-Zeichen drin sind, wird ebenfalls mit "0" aufgefuellt
  testchars[1] = '\0' ;
  while ( point1 < 17 )
  {
      testchars[0] = okstring[point1] ;
	  if ( testchars[0] < '0' || testchars[0] > '9' )
	  {
		  if ( testchars[0] == '\0' )
		  {
			  okstring[point1 + 1 ] = '\0' ;	// stringende fortplanzen
		  }
		  okstring[point1 ] = '0'		;	// Nicht-alpha durch "0" ersetzen bzw. string verl�ngern
		  testchars[0] = '0'			;
	  }
      platz    = atoi ( testchars ) ;
      summe += ( platz * wicht ) ;
      if ( wicht == 1 ) 
		wicht = 3 ;
      else
		wicht = 1 ;
      
      point1 ++ ; 
  }
  platz =  summe % 10 ;            /* Mod 10  */
  if ( platz == 0 )
 	  sprintf ( testchars, "0" ) ;
  else
	  sprintf ( testchars , "%d" , platz ) ; 	

  sprintf ( okstring + 17,  "%s" , testchars ) ;

   return ( okstring ) ;

}

char * nveausiln ( long lfd )
{
// Struktur der NVE :  "3" + mdn.iln[0->6] + lfd[9stellig]

// die "3" ist "frei verf�gbar, aber in der Regel IMMER == "3" ....

// das die Mandanten-ILN korrekt und aktuell ist, sollte vorher klar sein 
	sprintf ( okstring2, "3%s", mdn.iln );
	sprintf ( okstring2 + 8 , "%09ld" , lfd ) ;
	sprintf ( okstring2 , "%s" , generierenve(okstring2 )) ;

	return okstring2 ;
}


char * ohnekomma ( int ges , int nk , double wert )
{
  char maske[22] ;
	sprintf ( maske , "%%0%d.0f" , ges ) ;
	while ( nk > 0 )
	{
		wert *= 10.0 ;
		nk -- ;
	}
	sprintf ( okstring , maske , wert ) ;
	return okstring ;
}


char mge_nr_hilfe[33] ;

// imodus = 0 : Abkommen zusammensuchen
// imodus = 1 : aktuelles Abkommen zuweisen oder kein weiteres vorhanden 

void  mgr_nr_laden (void )
{
	int i ;
// Regulierer-Schleife ueber alles bei METRO mit Teilsortimentstrennung
// alle anderen : sedas_nr1 = Abkommensnummer
//                sedas_nr2 = z.B. MGE-Nummer

// gleich noch der Groesse nach sortieren ?!

	if ( dta.typ == METRO && !sypmetro1_par )
	{ // hole aus der teilsortimentstrennung ...
		for( i = 0 ; i < 20 ; i ++ )
		{
			m_abk_nr[i] = 0 ;
			m_mge_lief[i] = 0 ;
		}
		tsmtgg.mdn = mdn.mdn ;
		tsmtgg_class.opentsmtgg('k', dta.dta );
		i = tsmtgg_class.lesetsmtgg('k' );
		if ( i)	// Nix auf Kundenebene
		{
			tsmtgg.mdn = mdn.mdn ;
			tsmtgg_class.opentsmtgg('b', dta.dta );
			i = tsmtgg_class.lesetsmtgg('b');	// kubra-Ebene suchen
		}
		if ( !i)
		{
			tsmtg.mdn = mdn.mdn ;
			tsmtg.k_tsmt_gr = tsmtgg.k_tsmt_gr ;
			tsmtg_class.opentsmtg();
			int dabhilf = 0 ;
			int dabk = 0 ;
			while ( ! tsmtg_class.lesetsmtg())
			{
				if ((tsmtg.abkommen > 0) && (tsmtg.tsmt_gr > 0 ))
				{
					dabhilf = 0 ;
					while ( dabhilf <= dabk )
					{
						if ( m_abk_nr[dabhilf] == tsmtg.tsmt_gr )
							break ;
						else
							dabhilf ++ ;
					}
					if ( dabhilf > dabk )
					{
						m_abk_nr[dabk] = tsmtg.tsmt_gr ;
						m_mge_lief[dabk] = tsmtg.abkommen ;
						dabk ++ ;
					}
				}
			}	// Abkommen je tsmt-Gruppe
		}	// tsmt-Gruppeneintrag vorhanden
 	}	// gar nix 
}

char * suche_abkommen ( void ) 
{
	int dabhilf ;
	long dergebnis ;
	
    dabhilf = 0 ;
    while ( m_abk_nr[dabhilf] > 0 )
	{
		if ( m_abk_nr[dabhilf] == lsk.teil_smt )
		{
			dergebnis = m_mge_lief[dabhilf] ;
			sprintf ( mge_nr_hilfe, "%d", dergebnis ) ;
			return ( mge_nr_hilfe ) ;
		}
		dabhilf ++ ;
	}
    dergebnis = m_mge_lief[0] ;       // Mogelei, evtl. stimmt es ja doch
	sprintf ( mge_nr_hilfe, "%d", dergebnis ) ;
    return (mge_nr_hilfe) ;
}

int lsk_class_leselsk(void) 
{
	// liefer-ILN muss nach kun.plattform 
	// rechnungs-ILN muss nach kuninka.rechiln

	int dretkun ;
	int dretlsk = lsk_class.leselsk() ;
	if ( dretlsk )
		return dretlsk ;

	if ( lsk.kun != kun.kun )
	{	// Kunde und Inkasso nachladen 
		kun.mdn = mdn.mdn ;
		kun.kun = lsk.kun ;
		dretkun = kun_class.openkun() ;
		dretkun = kun_class.lesekun() ;
		if ( kun.inka_nr != kuninka.kun )
		{	//  Ich arbeite hier OHNE Test auf fehlende Kundeninformationen !!!
			memcpy ( &lesekun, &kun , sizeof (struct KUN)) ;
			kun.mdn = mdn.mdn ;
			kun.kun = kun.inka_nr ;
			dretkun = kun_class.openkun() ;
			dretkun = kun_class.lesekun() ;
			memcpy ( &kuninka, &kun , sizeof (struct KUN)) ;
			memcpy ( &kun, &lesekun , sizeof (struct KUN)) ;

			mgr_nr_laden() ;
		}
// 110208 A :die Frau Hirning hat es halt so gepflegt, jetzt muss nachsortiert werden 

		if ( sypdietzaku > 0 )
		{
			if ( (strlen ( clippedi ( kuninka.rechiln ))) == 13 )
				sprintf ( kun.rechiln ,"%s", kuninka.rechiln ) ;         // hier steht dann rechiln oder einzige iln
			else
				sprintf ( kun.rechiln , "%s" , kun.iln ) ;       // hier steht dann rechiln oder einzige iln

			if ( (strlen ( clippedi ( kun.plattform ))) == 13 )     // Zwischenstatus,solte spaeter mal weg hier
				sprintf ( kun.iln ,"%s" , kun.plattform ) ;
		}
// 110208 E : die Frau Hirning hat es halt so gepflegt
		if ( (strlen ( clippedi (kun.rechiln ))) == 13 )
		{
			// neueste Struktur  bereits aktiv
		}
		else
		{
			if ( (strlen (clippedi (  kuninka.rechiln ))) == 13 ) 
				sprintf ( kun.rechiln ,"%s" ,  kuninka.rechiln  ) ;        // hier steht dann rechiln oder einzige iln
			else
				sprintf ( kun.rechiln ,"%s" , kun.iln ) ;         // hier steht dann rechiln oder einzige iln
		}
		if ((strlen ( clippedi (kun.plattform ))) == 13 )     // Zwischenstatus,solte spaeter mal weg hier
			sprintf ( kun.iln , "%s" , kun.plattform ) ;

// Jetzt haben wir folgende Situation :
//   in kun.rechiln steht ILN fuer "BY
//   in kun.iln     steht ILN fuer "DP"
//   in kun.plattform steht irgend etwas
// 290108 E

	}
	return 0 ;
}

char ean_nr_hilfe[33] ;

char * eange ( char * inean )
{
int point1 ;
int  wicht ;
int platz ;
char testchars[2] ; 
int summe ;

  sprintf ( okstring , "%s" , inean ) ;
 
  point1 = 0 ;
  summe  = 0 ;
  wicht  = 1 ;

  // die ersten 13 Stellen werden verknotet
  // falls der input-string kuerzer ist, wird er mit 000 aufgefuellt,
  // falls nicht-nummer-Zeichen drin sind, wird ebenfalls mit "0" aufgefuellt
  testchars[1] = '\0' ;
  while ( point1 < 12 )
  {
      testchars[0] = okstring[point1] ;
	  if ( testchars[0] < '0' || testchars[0] > '9' )
	  {
		  if ( testchars[0] == '\0' )
		  {
			  okstring[point1 + 1 ] = '\0' ;	// stringende fortplanzen
		  }
		  okstring[point1 ] = '0'		;	// Nicht-alpha durch "0" ersetzen bzw. string verl�ngern
		  testchars[0] = '0'			;
	  }
      platz    = atoi ( testchars ) ;
      summe += ( platz * wicht ) ;
      if ( wicht == 1 ) 
		wicht = 3 ;
      else
		wicht = 1 ;
      
      point1 ++ ; 
  }
  platz =  summe % 10 ;            /* Mod 10  */
  if ( platz == 0 )
 	  sprintf ( testchars, "0" ) ;
  else
	  sprintf ( testchars , "%d" , 10 - platz ) ; 	

  sprintf ( okstring + 12,  "%s" , testchars ) ;

   return ( okstring ) ;
}

char * ean_machen ( double interna  ) 
{

char in_feld[33] ;


// 201100 : pauschale Generierung interne Artikel-Nummer -> 
	while ( interna > 9999999 )
		interna = interna - 10000000 ;
	while ( interna > 999999 )
		interna = interna - 1000000 ;
	while ( interna > 99999 )
		interna = interna - 100000 ;
	if ( interna < 1 )
        interna = 1 ;
   sprintf ( in_feld ,"%s",  cepu.eigen_iln ) ;
   sprintf ( in_feld + 7 ,"%05.0f" , interna ) ;
   return ( eange (in_feld) ) ;
}


char * holeselgrosa ( double interna )
{
int retakun ;
	// 020910 : fuer kun_bran = "15" und die beiden Artikel 227710 und 227750
    // keine Kundenspezifik und 2. ean benutzen 
    if ( sypboes_par == 1  &&  kun.kun_bran2[0] == '1' && kun.kun_bran2[1] == '5' 
		&& ( interna == 227710 || interna == 227750 ))
	{        // Kundencursor ueberspringen
		retakun = 100 ;
	}
	else
	{

		a_kun_gx.mdn = mdn.mdn ;
		a_kun_gx.a = interna ;
		a_kun_gx.kun = kun.kun ;
		sprintf ( a_kun_gx.kun_bran2 , "0" ) ;
		retakun = a_kun_gx_class.opena_kun_gx() ;
		retakun = a_kun_gx_class.lesea_kun_gx() ;
	}     
	while ( ! retakun ) 
	{
		if (a_kun_gx.ean  > 99999999999 && a_kun_gx.ean < 10000000000000 )
			break ;
		if (a_kun_gx.ean1  > 99999999999 && a_kun_gx.ean1 < 10000000000000 )
		{
			a_kun_gx.ean = a_kun_gx.ean1 ;
			break ;
		}
		retakun = a_kun_gx_class.lesea_kun_gx() ;
	}
    if ( retakun ) 
	{
		a_kun_gx.mdn = mdn.mdn ;
		a_kun_gx.a = interna ;
		a_kun_gx.kun = 0 ;
		sprintf ( a_kun_gx.kun_bran2 , kun.kun_bran2 ) ;
		retakun = a_kun_gx_class.opena_kun_gx() ;
		retakun = a_kun_gx_class.lesea_kun_gx() ;
		while ( ! retakun ) 
		{
			if (a_kun_gx.ean  > 99999999999 && a_kun_gx.ean < 10000000000000 )
			{
				if ( sypboes_par == 1 && kun.kun_bran2[0] == '1' && kun.kun_bran2[1] == '5' 
					&& ( interna == 227710 || interna == 227750 ))
				{  
					//  1. ean ignorieren
				}
                else
					break ;
			}
			if (a_kun_gx.ean1  > 99999999999 && a_kun_gx.ean1 < 10000000000000 )
			{
				a_kun_gx.ean = a_kun_gx.ean1 ;
				break ;
			}
			retakun = a_kun_gx_class.lesea_kun_gx() ;
		}
	}
    if ( retakun )        // totaler, eigentlich unzulaessiger Noptnagel
           return ( ean_machen ( interna)) ;

	sprintf ( ean_nr_hilfe, "%1.0f", a_kun_gx.ean  ) ;
    if ( ean_nr_hilfe[0] == '2' && ean_nr_hilfe[1] == '8' )				// "28"
	{
// 020804 : prinzipiell Pruefziffer mit "0" ersetzen lt. Wunsch fuer 28er
		ean_nr_hilfe[12] = '0' ;
		ean_nr_hilfe[13] = '\0' ;
        return  ean_nr_hilfe ;
	}
//    else
	return ( eange( ean_nr_hilfe)) ;
}

int a_kun_komplett (void) 
{
	char iean_nr_hilfe[33] ;

	a_kun.ean = 0.0 ;
	a_kun.ean_vk = 0.0 ;

	sprintf ( a_kun.kun_bran2 , "0" ) ;
	a_kun.kun = kun.kun ;
	a_kun.mdn = mdn.mdn ;
	a_kun.a   = lsp.a ;
	int retakun = a_kun_class.opena_kun () ;
	if ( ! retakun )
		retakun = a_kun_class.lesea_kun () ;
	if ( retakun )
	{
		sprintf ( a_kun.kun_bran2 ,"%s" , kun.kun_bran2 ) ;
		a_kun.kun = 0 ;
		a_kun.mdn = mdn.mdn ;
		a_kun.a   = lsp.a ;
		retakun = a_kun_class.opena_kun () ;
		if ( ! retakun )
			retakun = a_kun_class.lesea_kun () ;
	}

	if ( ! retakun )
	{
		if ( a_kun.ean_vk > 999999999999 && a_kun.ean_vk < 10000000000000 )
			a_kun.ean = a_kun.ean_vk ;

		if ( a_kun.ean > 999999999999 && a_kun.ean < 10000000000000 )
		{
			sprintf ( iean_nr_hilfe, "%1.0f", a_kun.ean  ) ;
			a_kun.ean = atof ( eange ( iean_nr_hilfe )) ;
		}
		else
			a_kun.ean = atof ( ean_machen ( lsp.a )) ;	// doofe Notbremse


		int lang = (int)strlen ( clippedi( a_kun.a_kun)) ;
		wsprintf ( a_kunfa_kun ,"%s", a_kun.a_kun ) ;
		if (dta.typ == FENEBERG )	// 150313
		{
			//                      -123456789012345678-
			wsprintf ( a_kunfa_kun, "000000000000000000" ) ;
			if ( lang > 0 )
				wsprintf ( a_kunfa_kun + 18 - lang , "%s", a_kun.a_kun );
		}


		return 0 ;
	}
// Notbremse ?!
	a_kun.ean = atof ( ean_machen ( lsp.a )) ;

	if (dta.typ == FENEBERG )	// 130613
			wsprintf ( a_kunfa_kun, "" ) ;

	return 1 ;
}



long LIN_senden ( long bel_pos , double exta )
{

	int k ;
// EAN-Artikel
		if ( dta.typ == SELGROS )
			k = cepu.LIN_segment (bel_pos, holeselgrosa( exta)) ;
		else
		{

			if ( def_a_kun )
			{
				
				lsp.a = exta ;	// einschleifen in die Suche
				a_kun_komplett() ;
				char hilfc [22] ;
				sprintf ( hilfc, "%1.0f",a_kun.ean) ;

				if ( dta.typ == FENEBERG && fenebergohneean )	// 010913 : alles zurueck ....
						k = cepu.LIN_segment (bel_pos, "") ;	// 300813 : KEINE EAN f�r Feneberg
				else
				{
					if ( (a_kun.ean  > 9999999 ))	// strlen == 13 )
						k = cepu.LIN_segment ( bel_pos, hilfc ) ;
					else
						k = cepu.LIN_segment (bel_pos, ean_machen( lsp.a)) ;
				}
			}
			else
			{
				if ( dta.typ == FENEBERG && fenebergohneean )	// 010913 : alles zurueck ....
					k = cepu.LIN_segment (bel_pos, "") ;	// 300813 : KEINE EAN f�r Feneberg
				else
					k = cepu.LIN_segment (bel_pos, ean_machen( lsp.a)) ;
			}
		}
		bel_pos ++ ;

return bel_pos ;
}


// a. Unsere Option ist die sortenreine Palette
// b. ( dh. Anzahl Karton je Palette/Artikel ist gleich Anzahl Karton je Palette )
// c. der vorhergehende Spruch ist irrelevant, da die "Palette" ohnehin erst mal die sortenreine Lage ist 

long anzahlkarton ( long bel_pos) 
{

short pme_einh ;
int inicount ;
	inicount = 0 ; 

	lsnve.ls = lsk.ls ;
	lsnve.mdn = lsk.mdn ;
	lsnve.fil = lsk.fil ;

	// lsnve.nve_palette ist vorgef�llt !!!!!

	int retnve1 = lsnve_class.opennve1 () ;
    retnve1 = lsnve_class.lesenve1 () ; 

	double palettbrutto = 0 ;
	double palettnetto  = 0 ;
    while (! retnve1 )
	{
		inicount ++ ;
		palettbrutto += lsnve.brutto_gew ;	// 060511
		palettnetto  += lsnve.netto_gew ;	// 060511
        retnve1 = lsnve_class.lesenve1 () ;
	}

    if ( inicount == 0 ) // Error 
	{
//        let sqlstatus = 100 
		return bel_pos ;
	}
	
    int k = cepu.PAC_segment (inicount, "","", "CT","" ) ;	// (lsnve.verp_art aufloesen ?! Karton oder e-performance oder e2 oder ..............

	if ( rheinstetten )	// 060511 
	{
		// k = cepu.MEA_segment ( "AAA" , palettnetto , 3, "KGM"  )
		// k = cepu.MEA_segment ( "AAB" , palettbrutto , 3, "KGM"  )
	}


	a_bas.a = lsnve.a ;
	k = a_bas_class.opena_bas () ;
	k = a_bas_class.lesea_bas () ;

	lsp.lief_me = inicount ;
    sprintf ( lsp.lief_me_bz , "Stck" ) ;
    lsp.a = lsnve.a ;
	memcpy ( & lsp.hbk_date ,& lsnve.mhd , sizeof( TIMESTAMP_STRUCT )) ; 
	sprintf ( lsp.ls_charge, "%s",lsnve.ls_charge ) ;

     // Positionsnummer      ???????
     // Welche  Nummer soll da folgen
/* ------
     let k = proc LIN_segment (bel_pos, "" ) 
     let bel_pos = bel_pos + 1
< ------ */

// EAN-Artikel
		if ( dta.typ == SELGROS )
			k = cepu.LIN_segment (bel_pos, holeselgrosa( lsp.a)) ;
		else
		{

			if ( def_a_kun )
			{
				a_kun_komplett() ;
				char hilfc [22] ;
				sprintf ( hilfc, "%1.0f",a_kun.ean) ;
				if ( dta.typ == FENEBERG && fenebergohneean )	// 010913 : alles zurueck ....
					k = cepu.LIN_segment (bel_pos, "") ;	// 300813 : KEINE EAN f�r Feneberg
				else
				{
					if ( (a_kun.ean  > 9999999 ))	// strlen == 13 )
						k = cepu.LIN_segment ( bel_pos, hilfc ) ;
					else
						k = cepu.LIN_segment (bel_pos, ean_machen( lsp.a)) ;
				}
			}
			else
			{
				if ( dta.typ == FENEBERG && fenebergohneean )	// 010913 : alles zurueck ....
					k = cepu.LIN_segment (bel_pos, "") ;	// 300813 : KEINE EAN f�r Feneberg
				else
					k = cepu.LIN_segment (bel_pos, ean_machen( lsp.a)) ;
			}
		}
		bel_pos ++ ;

		if ( (	lsp.lief_me_bz[0] == 'K' || lsp.lief_me_bz[0] == 'k' )
			&&( lsp.lief_me_bz[1] == 'G' || lsp.lief_me_bz[1] == 'g' ))
            pme_einh = 2 ;
       else

			pme_einh = 1 ;

		if ( doption == OPTPALETTE )	
		{
			if ( dta.typ != SELGROS )
			{	
				if ( ! rheinstetten )
					k = cepu.PIA_segment (lsp.a, a_kun.a_kun , "BP") ;
				else
				{	// 010612 : rheinstetten-Anforderungen ?!

					k = cepu.PIA_segment (lsp.a, "" , "RS") ;	// Sonderschalter "rheinstetten"
					if ( ( strlen ( a_kun.a_kun)) > 0 )
					k = cepu.PIA_segment (0 , a_kun.a_kun , "RS") ;	// Sonderschalter "rheinstetten"

				}
			}
		}


		k = cepu.QTY_segment ( "12", lsp.lief_me, pme_einh,sypboes_par ) ;

/* 091105 erst mal komplett rausnehmen, alle infos stecken ja bereits in der LIN
// Artikel METRO
// 110604 : welcher Artikel soll da folgen bei selgros
        if dta.typ <> SELGROS 
            let k = proc PIA_segment ("", a_kun.a_kun , "BP")
        end
// Kaufantragsnummer
//       let k = proc PIA_segment ("", a_kun.a_kun , "GB")
< ----- 091105 */

		if ( doption == OPTPALETTE )	// 130411
		{

			if ( dta.typ != FENEBERG )	// 160413
			{
				// 230413 : pci+17 davor 
				if ( ( lsp.hbk_date.year >= lsk.lieferdat.year ) || (strlen ( clippedi(lsp.ls_charge))) > 0 ) 
				{
					int ii = cepu.PCI_segment ( "17" ) ; 
					if ( lsp.hbk_date.year >= lsk.lieferdat.year )	// sinnvoller Wert	// 130411
					{
						k = cepu.DTM_segment ( "361" , lsp.hbk_date , 102 ,"" ) ;
					}
					else
					{	
					}

					if ( dta.typ == KAUFLAND )	// 050913 
					{	// lt. Doku immer die 4 segmente(PCI-DTM-QTY-BX als Sequenz erwartet, kein MHD/Chargen-Mix unterhalb NVE erlaubt
						k = cepu.QTY_segment ( "12", lsp.lief_me, pme_einh,sypboes_par ) ; // dupliziert die obige Menge .........
					}
					if ((strlen ( clippedi(lsp.ls_charge))) > 0 )	// sinnvoller Wert // 130411
					{
						k = cepu.GIN_segment ( "BX" , lsp.ls_charge , "" ,"" ,"","","","","","","") ;
					}
				}
			}
			else
			{
			}
		}

       if ( doption == OPTALLES )        // auch hier wieder gin-segment 
            erwladeposnve () ;

		return bel_pos ;
}


int  anzahlpaletten (void )
{
	// komplett neu am 010411
	// 080813 : Vermutlich kommt man hier NIE vorbei

int inicount = 0 ;

	lsnve.ls = lsk.ls ;
	lsnve.mdn = lsk.mdn ;
	lsnve.fil = lsk.fil ;
	int retnve0 = lsnve_class.opennve0 () ;
    retnve0 = lsnve_class.lesenve0 () ; 
    while (! retnve0 )
	{
		inicount ++ ;  
		retnve0 = lsnve_class.lesenve0 () ;
	}
 
    if ( inicount == 0 ) // Error 
        return  -1 ;
 

	int k = cepu.PAC_segment (inicount, "","",lesegs1_leergut(lsnve.verp_art),"::9" ) ; // Palette -> hier gibbet auch andere Leergut-Typen, bitte immer geclippte strings nutzen
													// 270613 : aus fix "201" wurde lesegs1_leergut ..

	if ( rheinstetten )	// 060511 
	{
		// double gesamtbrutto = berechne_Gesamtbrutto der Lieferung 
		// k = cepu.MEA_segment ( "AAD" , gesamtbrutto , 3, "KGM"  )
	}


// noch mal identico ......
	lsnve.ls = lsk.ls ;
	lsnve.mdn = lsk.mdn ;
	lsnve.fil = lsk.fil ;
	retnve0 = lsnve_class.opennve0 () ;
    retnve0 = lsnve_class.lesenve0 () ; 

	if (! retnve0 )
	{
		a_bas.a = lsnve.a ;
		k = a_bas_class.opena_bas () ;
		k = a_bas_class.lesea_bas () ;
	}

	// erster satz ist vorgelesen

    return  0 ;
}

// Das gehoert zu doption = OPTALLES

void erwladeposnve (void )
{
	// komplett neu am 010411

int ipont ;		// Marker "segment in gin"

double  stanve ;	// Anfang range
char cstanve[40] ;		// char-stanve
double altnve ;		// letztgelesener Wert
char caltnve[40] ;		// char altnve
double aktnve ;		// aktueller wert
char caktnve[40] ;		// char aktnve
// char gesamtnve [30] ;
char anfnve [40] ;
char hilfnve[35] ;

char nve1 [35] ; 
char nve2 [35] ;
char nve3 [35] ;
char nve4 [35] ;
char nve5 [35] ;
char nve6 [35] ;
char nve7 [35] ;
char nve8 [35] ;
char nve9 [35] ;
char nve10[35] ;

sprintf ( nve1 ,"0" ) ;
sprintf ( nve2 ,"0" ) ;
sprintf ( nve3 ,"0" ) ;
sprintf ( nve4 ,"0" ) ;
sprintf ( nve5 ,"0" ) ;
sprintf ( nve6 ,"0" ) ;
sprintf ( nve7 ,"0" ) ;
sprintf ( nve8 ,"0" ) ;
sprintf ( nve9 ,"0" ) ;
sprintf ( nve10,"0" ) ;

// der ganze 10-er Quatsch und die Paerchen-Bildung hat nur Sinn bei fortlaufenden Nummern,
// evtl. koennen das die modernen Systeme das gar  nicht mehr aufloesen 
 
	sprintf ( anfnve , "3%s", mdn.iln ) ;// relevant sind die ersten 1 + 7 Zeichen
	anfnve[8] = '\0' ;
	ipont = 0 ;
	altnve = 0 ;
	aktnve = 0 ;
	stanve = 0 ;
	sprintf ( cstanve, "0" );
	sprintf ( caktnve, "0" );
	sprintf ( caltnve, "0" );
// lsnve.nve_palette sowie mdn,fil und ls sind bereits best�ckt
	int retnve1 = lsnve_class.opennve1 () ;
	retnve1 = lsnve_class.lesenve1 () ;
    while (! retnve1 )
	{
/* ----->
		// hier neu : immer nur 1 Element und gut is ...
		sprintf ( hilfnve ,"%s" , clippedi (lsnve.nve) ) ;
		hilfnve[19] = '\0' ;	// In Vorausschau auf 20-stell nve + Pruefziffer abschneiden 
//        aktnve = lsnve.nve ;
		aktnve = atof ( hilfnve ) ;

		sprintf ( nve1 , "0" ) ;
		sprintf ( nve2 , "0" ) ;
		sprintf ( nve3 , "0" ) ;
		sprintf ( nve4 , "0" ) ;
		sprintf ( nve5 , "0" ) ;
		sprintf ( nve6 , "0" ) ;
		sprintf ( nve7 , "0" ) ;
		sprintf ( nve8 , "0" ) ;
		sprintf ( nve9 , "0" ) ;
		sprintf ( nve10, "0" ) ;
		nvekomplett(nve1 , aktnve,anfnve, clippedi (lsnve.nve)) ;
		retnve1 = lsnve_class.lesenve1 ()  ;
	}	// end while
	return ;
< ------ */

/* ---> das ist doppelquatsch, wurde bereits im select ausgeschlossen ...
        if ( lsnve.nve == lsnve.nve_palette )
		{	// Palettensatz ueberspringen
            retnve1 = fetch cursor cnve1
            continue ;
		}
< ----- */

//		aktnve = lsnve.nve ;
		sprintf ( caktnve ,"%s", clippedi(lsnve.nve) ) ;
		sprintf ( hilfnve ,"%s" , lsnve.nve ) ;
		hilfnve[19] = '\0' ;	// In Vorausschau auf 20-stell nve + Pruefziffer abschneiden 
        sprintf ( caktnve ,"%s", lsnve.nve ) ;
		aktnve = atof ( hilfnve ) ;

		int k ;

        if ( ipont == 10 )
		{
			if ( aktnve == altnve + 1 )
			{
				altnve = aktnve ;
				sprintf ( caltnve,"%s", caktnve) ;
				ipont = 10 ;
			}
            else
			{
                nvekomplett ( nve10, altnve,anfnve,caltnve ) ;
				k = cepu.GIN_segment ("BJ", nve1, nve2, nve3,
                      nve4, nve5, nve6, nve7, nve8, nve9 , nve10 );
                ipont = 0 ;		// neues segment anstarten
			}
		}
  
        if ( ipont == 9 )
		{
            if ( aktnve == altnve + 1 )
			{
				altnve = aktnve ;
				sprintf ( caltnve,"%s", caktnve) ;
				ipont = 9 ;
			}
			else
			{
				sprintf ( nve10 , "0" ) ;
                k = cepu.GIN_segment ("BJ", nve1, nve2, nve3,
                            nve4, nve5, nve6, nve7, nve8, nve9 , nve10  ) ;
				ipont = 0 ;       // Neues segment anstarten
			}
		}
        if ( ipont == 8 )
		{
			if ( aktnve == altnve + 1 )
			{
				altnve = aktnve ;
				sprintf ( caltnve,"%s", caktnve) ;
				ipont = 8 ;
			}
            else
			{
				nvekomplett (nve8, altnve,anfnve,caltnve) ;
                nvekomplett (nve9, aktnve,anfnve,caktnve) ;
                altnve = aktnve ;
  				sprintf ( caltnve,"%s", caktnve) ;
				stanve = aktnve ;
				sprintf ( cstanve,"%s", caktnve) ;
                ipont = 9 ;
			}
		}
        if ( ipont == 7 )
		{
            if ( aktnve == altnve + 1 )
			{
				altnve = aktnve ;
				sprintf ( caltnve,"%s", caktnve) ;
				ipont = 8 ;
			}
            else
			{
				sprintf ( nve8 , "0" ) ;
				nvekomplett (nve9,aktnve,anfnve,caktnve) ;
                altnve = aktnve ;
				sprintf ( caltnve,"%s", caktnve) ;
                stanve = aktnve ;
				sprintf ( cstanve,"%s", caktnve) ;
                ipont = 9;
			}
		}
        if ( ipont == 6 )
		{
            if ( aktnve == altnve + 1 )
			{
				altnve = aktnve ;
				sprintf ( caltnve,"%s", caktnve) ;
				ipont = 6 ;
			}
            else
			{
				nvekomplett(nve6,altnve,anfnve,caltnve ) ;
				nvekomplett(nve7,aktnve,anfnve,caktnve ) ;
				altnve = aktnve ;
				sprintf ( caltnve,"%s", caktnve) ;
				stanve = aktnve ;
				sprintf ( cstanve,"%s", caktnve) ;
				ipont = 7 ;
			}
		}

        if ( ipont == 5 )
		{
			if ( aktnve == altnve + 1 )
			{
				altnve = aktnve ;
				sprintf ( caltnve,"%s", caktnve) ;
				ipont = 6 ;
			}
            else
			{
				sprintf ( nve5 , "0" ) ;
                nvekomplett (nve6,aktnve,anfnve,caktnve) ;
                altnve = aktnve ;
				sprintf ( caltnve,"%s", caktnve) ;
                stanve = aktnve ;
				sprintf ( cstanve,"%s", caktnve) ;
				ipont = 7 ;
			}
		}

        if ( ipont == 4 )
		{
			if ( aktnve == altnve + 1 )
			{
				altnve = aktnve ;
				sprintf ( caltnve,"%s", caktnve) ;
				ipont = 4 ;
			}
            else
			{
				nvekomplett (nve4,altnve,anfnve,caltnve) ;
                nvekomplett (nve5,aktnve,anfnve,caktnve) ;
				altnve = aktnve ;
				sprintf ( caltnve,"%s", caktnve) ;
				stanve = aktnve ;
				sprintf ( cstanve,"%s", caktnve) ;
				ipont = 5 ;
			}
		}
		if ( ipont == 3 )
		{
			if ( aktnve == altnve + 1 )
			{
				altnve = aktnve ;
				sprintf ( caltnve,"%s", caktnve) ;
				ipont = 4 ;
			}
            else
			{
				sprintf ( nve3 , "0" ) ;
				nvekomplett ( nve4, aktnve,anfnve,caktnve) ;
				altnve = aktnve ;
				sprintf ( caltnve,"%s", caktnve) ;
				stanve = aktnve ;
				sprintf ( cstanve,"%s", caktnve) ;
				ipont = 5 ;
			}
		}

        if ( ipont == 2 )
		{
			if ( aktnve == altnve + 1 )
			{
				altnve = aktnve ;
				sprintf ( caltnve,"%s", caktnve) ;
				ipont = 2 ;
			}
            else
			{
				nvekomplett (nve2,altnve,anfnve,caltnve) ;
				nvekomplett (nve3,aktnve,anfnve,caktnve) ;
				altnve = aktnve ;
				sprintf ( caltnve,"%s", caktnve) ;
				stanve = aktnve ;
				sprintf ( cstanve,"%s", caktnve) ;
				ipont = 3 ;
			}
		}
		if ( ipont == 1 )
		{
			if ( aktnve == altnve + 1 )
			{
				altnve = aktnve ;
				sprintf ( caltnve,"%s", caktnve) ;
				ipont = 2 ;
			}
            else
			{
				sprintf ( nve2 , "0" ) ;
				nvekomplett(nve3, aktnve,anfnve,caktnve) ;
				altnve = aktnve ;
				sprintf ( caltnve,"%s", caktnve) ;
				stanve = aktnve ;
				sprintf ( cstanve,"%s", caktnve) ;
				ipont = 3 ;
			}
		}

        if ( ipont == 0 )
		{
			sprintf ( nve1 , "0" ) ;
            sprintf ( nve2 , "0" ) ;
            sprintf ( nve3 , "0" ) ;
            sprintf ( nve4 , "0" ) ;
            sprintf ( nve5 , "0" ) ;
            sprintf ( nve6 , "0" ) ;
            sprintf ( nve7 , "0" ) ;
            sprintf ( nve8 , "0" ) ;
            sprintf ( nve9 , "0" ) ;
            sprintf ( nve10, "0" ) ;
            nvekomplett(nve1 , aktnve,anfnve,caktnve) ;
			altnve = aktnve ;
				sprintf ( caltnve,"%s", caktnve) ;
			stanve = aktnve ;
				sprintf ( cstanve,"%s", caktnve) ;
            ipont = 1 ;
		}
	
		retnve1 = lsnve_class.lesenve1 ()  ;
	}  // while retnve1
    if ( ipont == 1 || ipont == 2 )
	{
        if ( aktnve == stanve )  // nix zu tun , bei 2 doppelmoppel
			;
		else
            nvekomplett (nve2, aktnve,anfnve,caktnve) ;
	}
	if ( ipont == 3 || ipont == 4 ) 
	{
		if ( aktnve == stanve )  // nix zu tun
			;
		else
			nvekomplett(nve4,aktnve,anfnve,caktnve) ;
	}
	if ( ipont == 5 || ipont == 6 )
	{
		if ( aktnve == stanve )  // nix zu tun
		;       
		else
			nvekomplett (nve6,aktnve,anfnve,caktnve) ;
	}
	if ( ipont == 7 || ipont == 8 ) 
	{
        if ( aktnve == stanve )  // nix zu tun
		;
		else
			nvekomplett(nve8,aktnve,anfnve,caktnve) ;
	}
	if ( ipont == 9 || ipont == 10 )
	{
		if ( aktnve = stanve )  // nix zu tun
			;
        else
			nvekomplett ( nve10,aktnve,anfnve,caktnve) ;
	}
    if ( ipont )
        int k = cepu.GIN_segment ("BJ", nve1, nve2, nve3,nve4, nve5, nve6, nve7, nve8, nve9 , nve10 ) ;
}

void ladeposnve(void)
{

int ipont ;		// Marker "segment in gin"

double  stanve ;	// Anfang range
char cstanve[40] ;		// char-stanve
double altnve ;		// letztgelesener Wert
char caltnve[40] ;		// char altnve
double aktnve ;		// aktueller wert
char caktnve[40] ;		// char aktnve
// char gesamtnve [40] ;
char anfnve [40] ;
char hilfnve[35] ;

char nve1 [35] ; 
char nve2 [35] ;
char nve3 [35] ;
char nve4 [35] ;
char nve5 [35] ;
char nve6 [35] ;
char nve7 [35] ;
char nve8 [35] ;
char nve9 [35] ;
char nve10[35] ;

sprintf ( nve1 ,"0" ) ;
sprintf ( nve2 ,"0" ) ;
sprintf ( nve3 ,"0" ) ;
sprintf ( nve4 ,"0" ) ;
sprintf ( nve5 ,"0" ) ;
sprintf ( nve6 ,"0" ) ;
sprintf ( nve7 ,"0" ) ;
sprintf ( nve8 ,"0" ) ;
sprintf ( nve9 ,"0" ) ;
sprintf ( nve10,"0" ) ;

// der ganze 10-er Quatsch und die Paerchen-Bildung hat nur Sinn bei fortlaufenden Nummern,
// evtl. koennen das die modernen Systeme das gar  nicht mehr aufloesen 
 
	sprintf ( anfnve , "3%s", mdn.iln ) ;// relevant sind die ersten 1 + 7 Zeichen
	anfnve[8] = '\0' ;
	ipont = 0 ;
	altnve = 0 ;
	aktnve = 0 ;
	stanve = 0 ;
	sprintf ( cstanve, "0" );
	sprintf ( caktnve, "0" );
	sprintf ( caltnve, "0" );
	lsnve.mdn = lsk.mdn ;
	lsnve.fil = lsk.fil ;
	lsnve.ls = lsk.ls ;
    lsnve.nve_posi = lsp.nve_posi ; 
	lsnve.a = lsp.a ;

    if ( lsp.nve_posi > 0 )
	{
		int retnveposi = lsnve_class.opennveposi () ;
		retnveposi = lsnve_class.lesenveposi () ;
		while (! retnveposi )
		{
/* ----->
		// hier neu : immer nur 1 Element und gut is ...
			sprintf ( hilfnve ,"%s" , clippedi (lsnve.nve) ) ;
			hilfnve[19] = '\0' ;	// In Vorausschau auf 20-stell nve + Pruefziffer abschneiden 
//			aktnve = lsnve.nve ;
			aktnve = atof ( hilfnve ) ;

			sprintf ( nve1 , "0" ) ;
			sprintf ( nve2 , "0" ) ;
			sprintf ( nve3 , "0" ) ;
			sprintf ( nve4 , "0" ) ;
			sprintf ( nve5 , "0" ) ;
			sprintf ( nve6 , "0" ) ;
			sprintf ( nve7 , "0" ) ;
			sprintf ( nve8 , "0" ) ;
			sprintf ( nve9 , "0" ) ;
			sprintf ( nve10, "0" ) ;
			nvekomplett(nve1 , aktnve,anfnve, clippedi (lsnve.nve)) ;
			retnveposi = lsnve_class.lesenveposi ()  ;
		}	// end while
	}
		return ;
< ------ */



			if ( ! strcmp( lsnve.nve , lsnve.nve_palette ))	// Paletten �berlesen .......
			{
				retnveposi = lsnve_class.lesenveposi () ;
				continue ;
			}

//		aktnve = lsnve.nve ;
			sprintf ( caktnve ,"%s", clippedi(lsnve.nve) ) ;
			sprintf ( hilfnve ,"%s" , lsnve.nve ) ;
			hilfnve[19] = '\0' ;	// In Vorausschau auf 20-stell nve + Pruefziffer abschneiden 
			sprintf ( caktnve ,"%s", lsnve.nve ) ;
			aktnve = atof ( hilfnve ) ;

			int k ;

			if ( ipont == 10 )
			{
				if ( aktnve == altnve + 1 )
				{
					altnve = aktnve ;
					sprintf ( caltnve,"%s", caktnve) ;
					ipont = 10 ;
				}
				else
				{
					nvekomplett ( nve10, altnve,anfnve,caltnve ) ;
					k = cepu.GIN_segment ("BJ", nve1, nve2, nve3,
                      nve4, nve5, nve6, nve7, nve8, nve9 , nve10 );
					ipont = 0 ;		// neues segment anstarten
				}
			}
  
			if ( ipont == 9 )
			{
				if ( aktnve == altnve + 1 )
				{
					altnve = aktnve ;
					sprintf ( caltnve,"%s", caktnve) ;
					ipont = 9 ;
				}
				else
				{
					sprintf ( nve10 , "0" ) ;
					k = cepu.GIN_segment ("BJ", nve1, nve2, nve3,
                            nve4, nve5, nve6, nve7, nve8, nve9 , nve10  ) ;
					ipont = 0 ;       // Neues segment anstarten
				}
			}
			if ( ipont == 8 )
			{
				if ( aktnve == altnve + 1 )
				{
					altnve = aktnve ;
					sprintf ( caltnve,"%s", caktnve) ;
					ipont = 8 ;
				}
				else
				{
					nvekomplett (nve8, altnve,anfnve,caltnve) ;
					nvekomplett (nve9, aktnve,anfnve,caktnve) ;
					altnve = aktnve ;
  					sprintf ( caltnve,"%s", caktnve) ;
					stanve = aktnve ;
					sprintf ( cstanve,"%s", caktnve) ;
					ipont = 9 ;
				}
			}
			if ( ipont == 7 )
			{
				if ( aktnve == altnve + 1 )
				{
					altnve = aktnve ;
					sprintf ( caltnve,"%s", caktnve) ;
					ipont = 8 ;
				}
				else
				{
					sprintf ( nve8 , "0" ) ;
					nvekomplett (nve9,aktnve,anfnve,caktnve) ;
					altnve = aktnve ;
					sprintf ( caltnve,"%s", caktnve) ;
					stanve = aktnve ;
					sprintf ( cstanve,"%s", caktnve) ;
					ipont = 9;
				}
			}
			if ( ipont == 6 )
			{
				if ( aktnve == altnve + 1 )
				{
					altnve = aktnve ;
					sprintf ( caltnve,"%s", caktnve) ;
					ipont = 6 ;
				}
				else
				{
					nvekomplett(nve6,altnve,anfnve,caltnve ) ;
					nvekomplett(nve7,aktnve,anfnve,caktnve ) ;
					altnve = aktnve ;
					sprintf ( caltnve,"%s", caktnve) ;
					stanve = aktnve ;
					sprintf ( cstanve,"%s", caktnve) ;
					ipont = 7 ;
				}
			}

			if ( ipont == 5 )
			{
				if ( aktnve == altnve + 1 )
				{
					altnve = aktnve ;
					sprintf ( caltnve,"%s", caktnve) ;
					ipont = 6 ;
				}
				else
				{
					sprintf ( nve5 , "0" ) ;
					nvekomplett (nve6,aktnve,anfnve,caktnve) ;
					altnve = aktnve ;
					sprintf ( caltnve,"%s", caktnve) ;
					stanve = aktnve ;
					sprintf ( cstanve,"%s", caktnve) ;
					ipont = 7 ;
				}
			}

			if ( ipont == 4 )
			{
				if ( aktnve == altnve + 1 )
				{
					altnve = aktnve ;
					sprintf ( caltnve,"%s", caktnve) ;
					ipont = 4 ;
				}
				else
				{
					nvekomplett (nve4,altnve,anfnve,caltnve) ;
					nvekomplett (nve5,aktnve,anfnve,caktnve) ;
					altnve = aktnve ;
					sprintf ( caltnve,"%s", caktnve) ;
					stanve = aktnve ;
					sprintf ( cstanve,"%s", caktnve) ;
					ipont = 5 ;
				}
			}
			if ( ipont == 3 )
			{
				if ( aktnve == altnve + 1 )
				{
					altnve = aktnve ;
					sprintf ( caltnve,"%s", caktnve) ;
					ipont = 4 ;
				}
				else
				{
					sprintf ( nve3 , "0" ) ;
					nvekomplett ( nve4, aktnve,anfnve,caktnve) ;
					altnve = aktnve ;
					sprintf ( caltnve,"%s", caktnve) ;
					stanve = aktnve ;
					sprintf ( cstanve,"%s", caktnve) ;
					ipont = 5 ;
				}
			}

			if ( ipont == 2 )
			{
				if ( aktnve == altnve + 1 )
				{
					altnve = aktnve ;
					sprintf ( caltnve,"%s", caktnve) ;
					ipont = 2 ;
				}
				else
				{
					nvekomplett (nve2,altnve,anfnve,caltnve) ;
					nvekomplett (nve3,aktnve,anfnve,caktnve) ;
					altnve = aktnve ;
					sprintf ( caltnve,"%s", caktnve) ;
					stanve = aktnve ;
					sprintf ( cstanve,"%s", caktnve) ;
					ipont = 3 ;
				}	
			}
			if ( ipont == 1 )
			{
				if ( aktnve == altnve + 1 )
				{
					altnve = aktnve ;
					sprintf ( caltnve,"%s", caktnve) ;
					ipont = 2 ;
				}
				else
				{
					sprintf ( nve2 , "0" ) ;
					nvekomplett(nve3, aktnve,anfnve,caktnve) ;
					altnve = aktnve ;
					sprintf ( caltnve,"%s", caktnve) ;
					stanve = aktnve ;
					sprintf ( cstanve,"%s", caktnve) ;
					ipont = 3 ;
				}
			}

			if ( ipont == 0 )
			{
				sprintf ( nve1 , "0" ) ;
				sprintf ( nve2 , "0" ) ;
				sprintf ( nve3 , "0" ) ;
				sprintf ( nve4 , "0" ) ;
				sprintf ( nve5 , "0" ) ;
				sprintf ( nve6 , "0" ) ;
				sprintf ( nve7 , "0" ) ;
				sprintf ( nve8 , "0" ) ;
				sprintf ( nve9 , "0" ) ;
				sprintf ( nve10, "0" ) ;
				nvekomplett(nve1 , aktnve,anfnve,caktnve) ;
				altnve = aktnve ;
				sprintf ( caltnve,"%s", caktnve) ;
				stanve = aktnve ;
				sprintf ( cstanve,"%s", caktnve) ;
				ipont = 1 ;
			}
	
			retnveposi = lsnve_class.lesenveposi ()  ;
		}  // while retnveposi
		if ( ipont == 1 || ipont == 2 )
		{
			if ( aktnve == stanve )  // nix zu tun , bei 2 doppelmoppel
			;
		else
            nvekomplett (nve2, aktnve,anfnve,caktnve) ;
		}
		if ( ipont == 3 || ipont == 4 ) 
		{
			if ( aktnve == stanve )  // nix zu tun
			;
			else
				nvekomplett(nve4,aktnve,anfnve,caktnve) ;
		}
		if ( ipont == 5 || ipont == 6 )
		{
			if ( aktnve == stanve )  // nix zu tun
			;       
			else
				nvekomplett (nve6,aktnve,anfnve,caktnve) ;
		}
		if ( ipont == 7 || ipont == 8 ) 
		{
			if ( aktnve == stanve )  // nix zu tun
			;
			else
				nvekomplett(nve8,aktnve,anfnve,caktnve) ;
		}
		if ( ipont == 9 || ipont == 10 )
		{
			if ( aktnve = stanve )  // nix zu tun
			;
			else
				nvekomplett ( nve10,aktnve,anfnve,caktnve) ;
		}
		if ( ipont )
			int k = cepu.GIN_segment ("BJ", nve1, nve2, nve3,nve4, nve5, nve6, nve7, nve8, nve9 , nve10 ) ;
	}
}		
		

void nvekomplett ( char * ausgang , double inwert, char * anfnve, char * transfer ) 
{
//field outwert char (20)
// hier wird auch entschieden, ob die NVE selber fertig generiert oder 1:1 durchgereicht wird ......
int point1 ;
int wicht ;
int platz ;
char testchar[2] ;
int summe ;

	if ( nvefertig )
	{
		sprintf ( ausgang, "%s", transfer ) ;
		return ;
	} ;

	sprintf(ausgang,"%s", anfnve ) ;	// Achtung : anfnve MUSS 8 stellen haben
	sprintf ( ausgang + 8 , "%09.0f", inwert )  ;	//  let outwert[9] = PICTURE ( inwert, "&&&&&&&&&" )
	point1 = 0 ;
	summe  = 0 ;
	wicht  = 3 ;
	while ( point1 < 17 )
	{
		testchar[0] = ausgang[point1] ;
		if ( testchar[0] < '0' || testchar[0] > '9' )
		{
		  okstring[point1 ] = '0'		;	// Nicht-alpha durch "0" ersetzen bzw. string verl�ngern
		  testchar[0] = '0'			;
		}

		testchar[1] = '\0' ;
		platz = atoi ( testchar ) ;
		summe += ( platz * wicht ) ;
		if ( wicht == 1 ) 
			wicht = 3 ;
		else
			wicht = 1 ;
		point1 ++ ;
	}
	platz = summe % 10 ; // modul 10 
	if ( platz == 0 )
		sprintf ( testchar, "0" ) ;
	else
		sprintf ( testchar , "%d" , 10 - platz ) ; 
	sprintf ( ausgang + 17,  "%s" , testchar ) ;

}

void  posindatei (int * pbadcount, int * pkbadcount, int * pgescount, int * pkgescount )
{// erster lsp-Satz ist bereits erfolgreich gelesen

	badcount = *pbadcount ;
	gescount = *pgescount ;
	kbadcount = *pkbadcount ;
	kgescount = *pkgescount ;

	int pme_einh ;
	int k ;				// freie Variable 
	int  cpscounter;     // laufende cps-Numerierung
    long bel_pos;         // lfd. im Beleg
	int retlsp;


    bel_pos = 1 ;            // Start je Beleg    

    cpscounter = 1 ;

    kgescount ++ ;
    if ((strlen ( clippedi ( kun.iln ))) < 3 )
	{
		sprintf ( protokoll , "LS: %d  nicht verbucht : fehlende ILN-Nummer" , lsk.ls ) ;
		protschreiben(1) ;
		kbadcount ++ ;
		retlsp = 0 ; // erster satz ist vorgelesen 
        while ( ! retlsp ) 
		{
			gescount ++ ;
			badcount ++ ;
			sprintf ( protokoll , "LS : %d Art: %1.0f Menge: %1.3f"
                    " nicht verbucht : Folgefehler " , lsk.ls, lsp.a , lsp.lief_me  ) ;
			protschreiben(1) ;
            retlsp = lsp_class.leselsp() ;
		}
		*pbadcount =  badcount ;
		*pgescount =  gescount ;
		*pkbadcount = kbadcount ;
		*pkgescount = kgescount ;

        return ;
	}

	retlsp = 0 ; // erster satz ist vorgelesen 

// Basis-CPS-Segment == 1/0 == Lieferung -> fuer NURMENGE und NURKARTON eigentlich nur ein dummy

    k = cepu.CPS_segment (cpscounter, 0 ) ;
    if ( k) return ;

	if ( dta.typ == FENEBERG )
	{
		// hierher muesste dann ein PAC_segment mit irgendeiner Packst�ckzahl ) z.B. "PAC+123'"
	}

    while ( !retlsp )
	{
// Leergut-Infos komplett raus, taucht erst bei NVE-Infos wieder auf ?! 
		if (( a_bas.a_typ == 11 ) && ( ! retlsp ))
		{
            retlsp = lsp_class.leselsp() ;
			continue ;
		}
		gescount ++;


		if ( a_kun_komplett () )	// a_kun komplettieren
		{

			if ( dta.typ == FENEBERG || dta.typ == EDEKA)	// Feneberg : nicht abbrechen !!!!
			{												// 250613 : EDEKA auch nicht abbrechen !!
					sprintf ( protokoll  , "LS : %d  Art: %1.0f Menge: %1.3f"
						" verbucht obwohl a_kun fehlt " , lsk.ls , lsp.a , lsp.lief_me  ) ;
					protschreiben (1) ;

			}
			else
			{
				if ( dta.typ != SELGROS )  // SELGROSS wird bei Boesi in a_kun_gx gepflegt ...
				{
					sprintf ( protokoll  , "LS : %d  Art: %1.0f Menge: %1.3f"
						" nicht verbucht : a_kun fehlt " , lsk.ls , lsp.a , lsp.lief_me  ) ;
					protschreiben (1) ;
					badcount ++ ;
					retlsp = lsp_class.leselsp() ;
					continue ;
				}
			}
		}

// AAAAAAAAAAAAAAAAAAAAAAAAAAAAA posi_ext = ?
/* ----> 110604 issn quark
// Positionsnummer      ???????
// Welche  Nummer soll da folgen
        let k = proc LIN_segment (bel_pos, "" ) 
        let bel_pos = bel_pos + 1
< ----- */

// EAN-Artikel
		if ( dta.typ == SELGROS )
		{
			// 2. Parameter ist ein (ean-)string
			k = cepu.LIN_segment (bel_pos, holeselgrosa( lsp.a)) ;
		}
		else
		{
			if ( def_a_kun )
			{
				if ( dta.typ == FENEBERG && fenebergohneean )	// 010913 : alles zurueck ....
					k = cepu.LIN_segment (bel_pos, "") ;	// 300813 : KEINE EAN f�r Feneberg
				else
				{
					sprintf ( ean_nr_hilfe, "%013.0f", a_kun.ean ) ;
					k = cepu.LIN_segment ( bel_pos, ean_nr_hilfe ) ;
				}
			}
			else
			{
				if ( dta.typ == FENEBERG && fenebergohneean )	// 010913 : alles zurueck ....
					k = cepu.LIN_segment (bel_pos, "") ;	// 300813 : KEINE EAN f�r Feneberg
				else
					k = cepu.LIN_segment (bel_pos, ean_machen( lsp.a)) ;
			}
		}

		bel_pos ++ ;

		if ( ( lsp.lief_me_bz[0] == 'K' || lsp.lief_me_bz[0] == 'k' )
		  && ( lsp.lief_me_bz[1] == 'G' || lsp.lief_me_bz[1] == 'g' ))
			pme_einh = 2 ;
		else
		   // 160913 : Notbremse wegen "leerer" Bezeichnungen 
		   if ( strlen (clippedi( lsp.lief_me_bz)) == 0)
		   {
			   if ( a_bas.me_einh == 2 )
				   pme_einh = 2;
			   else
				   pme_einh = 1;
		   }
		   else

			pme_einh = 1 ;

       if ( dta.typ == SELGROS )
	   {
//		was soll ich da schreiben ??           let k = proc PIA_segment ("", a_kun.a_kun , "BP")
//       let k = proc PIA_segment ("", a_kun.a_kun , "GB")
//	keine eigne Nummer, keine kunspez. Nummer .........
	   }
	   else
	   {
			if ( ! rheinstetten )
				if ( dta.typ == FENEBERG )
					k = cepu.PIA_segment (lsp.a, a_kunfa_kun , "IN") ;	// 160613 "BP"->"IN"
				else
				    if ( dta.typ == METRO )
						k = cepu.PIA_segment (lsp.a, a_kun.a_kun , "BP") ;
					else
						k = cepu.PIA_segment (lsp.a, a_kun.a_kun , "IN") ;	// 160613 "BP"->"IN"
			else
			{	// 010612 : rheinstetten-Anforderungen ?!
				k = cepu.PIA_segment (lsp.a, "" , "RS") ;	// Sonderschalter "Rheinstetten"
				if ( ( strlen ( a_kun.a_kun)) > 0 )
				k = cepu.PIA_segment (0 , a_kun.a_kun , "RS") ;	// Sonderschalter "Rheinstetten"
			}

// Kaufantragsnummer
//       let k = proc PIA_segment (0, a_kun.a_kun , "GB")
	   }
// hier gibt es ja nur optmenge oder optkarton
       if (doption == OPTKARTON )
	   {
			k = cepu.QTY_segment ( "12", lsp.lief_me, pme_einh ,obaktiv) ;
            // qty-segment ist ja auch hier notwendig ;-)
			ladeposnve() ;
	   }
       else
	   {
			if ( doption == OPTMENGE )     // eigentlich gibt es hier nur diese beiden Zweige
			{
				k = cepu.QTY_segment ( "12", lsp.lief_me, pme_einh, obaktiv ) ;
			}
	   }

		if ( dta.typ != FENEBERG )	// 160413
		{	// 230413 : pci+17 davor 
			if ( ( lsp.hbk_date.year >= lsk.lieferdat.year ) || (strlen ( clippedi(lsp.ls_charge))) > 0 ) 
			{
				int ii = cepu.PCI_segment ( "17" ) ; 
				if ( lsp.hbk_date.year >= lsk.lieferdat.year )	// sinnvoller Wert	// 130411
				{
					k = cepu.DTM_segment ( "361" , lsp.hbk_date , 102 ,"" ) ;
				}
				else
				{	
				}

				if ((strlen ( clippedi(lsp.ls_charge))) > 0 )	// sinnvoller Wert // 130411
				{
					k = cepu.GIN_segment ( "BX" , lsp.ls_charge , "" ,"" ,"","","","","","","") ;
				}
			}
		}
		else
		{
		}


	   retlsp = lsp_class.leselsp();
	}		// while pos-fetch

	*pbadcount = badcount ;
	*pgescount = gescount ;
	*pkbadcount = kbadcount ;
	*pkgescount = kgescount ;

	if ( dta.typ == FENEBERG )
		k = cepu.CNT_segment (  "2" , bel_pos - 1 , 0  ) ;
}


static char verpbezeichnung[31] ;
char * holebezeichnung ( long verp_art )
{


	switch ( verp_art )
	{
		// Zun�chst Musterstruktur, sp�ter aus ptab-Eintrag oder sonstwoher 
	case 1	 : sprintf ( verpbezeichnung ,"CT" ) ; break ;
	case 201 : sprintf ( verpbezeichnung ,"DL-R_E1" ) ; break ;
	case 202 : sprintf ( verpbezeichnung ,"DL-R-E2" ) ; break ;
	case 101 : sprintf ( verpbezeichnung ,"DL-W-E1" ) ; break ;
	case 102 : sprintf ( verpbezeichnung ,"DL-W-E2" ) ; break ;
	default  : sprintf ( verpbezeichnung ,"KISTE" )   ; break ;
	}
	return verpbezeichnung ;
}



void  nurnveablauf (int * pbadcount, int * pkbadcount, int * pgescount, int * pkgescount )
{

// 170511 : komplett neuer Ablauf mit Masternve und Struktur und Logik like EDEKA Rheinstetten

    badcount = *pbadcount ;
    kbadcount = *pkbadcount ;
    gescount = *pgescount ;
    kgescount = *pkgescount ;

	int retlsp ;
	short ik ;
    short ime_einh_kun ;
    double iinh	;	// ( 12,3)
    short pme_einh ; 
	int retnve1, retnve2, retnve3  ;	// sql-status der Cursoren 


    long bel_pos ;			// lfd. im Beleg

	bel_pos = 1  ;		// Start je Beleg    

   kgescount ++ ;
	if ((strlen ( clippedi ( kun.iln ))) < 3 )
	{
		sprintf ( protokoll , "LS: %d  nicht verbucht : fehlende ILN-Nummer" , lsk.ls ) ;
		protschreiben(1) ;
    
        kbadcount ++ ;
		retlsp = 0 ; // erster satz ist vorgelesen 
        while ( ! retlsp ) 
		{
			gescount ++ ;
			badcount ++ ;
			sprintf ( protokoll , "LS : %d Art: %1.0f Menge: %1.3f"
                    " nicht verbucht : Folgefehler " , lsk.ls, lsp.a , lsp.lief_me  ) ;
			protschreiben(1) ;
            retlsp = lsp_class.leselsp() ;
		}
		*pbadcount = badcount ;
		*pkbadcount = kbadcount ;
		*pgescount = gescount ;
		*pkgescount = kgescount ;
		return ;
	}

    iCPS = 1 ;	// jetzt geht es richtig los .........
	iBAS = 0 ;
	iAUF = 0 ;

    int k = cepu.CPS_segment (iCPS, 0 ) ;	// erste Ebene "Gesamtlieferung"
	iBAS = iCPS ;
	iAUF = iCPS ;
	iCPS++ ;

	char vorpalette[35] ;

	vorpalette[0] = '\0' ;

    if (( doption == OPTPALETTE ) || ( doption == OPTALLES ))
	{
	// #### erzeugt pac-segment ( n paletten ) #####

		lsnve.mdn = lsk.mdn ;
		lsnve.ls = lsk.ls ;
		lsnve.fil = 0 ;
		double aad = 0 ;
		int pac = 0 ;

		long pacverp_art = 0;	// 080813	// ich gehe mal davon aus, das es nur eine Palettensorte gibt
		int retnvea = lsnve_class.opennvea() ;
		retnvea = lsnve_class.lesenvea () ;
		while ( ! retnvea )
		{
			if ( strlen ( clippedi ( lsnve.masternve )) > 3 )	// Masternve darf es immer nur eine geben
			{
				if ( strcmp ( vorpalette , lsnve.masternve ))
				{
					aad = aad + lsnve.brutto_gew ;
					pac ++ ;
					sprintf ( vorpalette , "%s" , lsnve.masternve ) ;
					if ( lsnve.verp_art > 0 )	// 080813
						pacverp_art = lsnve.verp_art ;
				}
			}
			else	// "normale" Paletten
			{
					aad = aad + lsnve.brutto_gew ;
					pac ++ ;
					if ( lsnve.verp_art > 0 && pacverp_art == 0 )	// 080813
						pacverp_art = lsnve.verp_art ;
			}
			retnvea = lsnve_class.lesenvea () ;
		}
		
		if (rheinstetten )
		{
			k = cepu.PAC_segment( pac , "","","PX-H1", "" ) ;
			k = cepu.MEA_segment ( "AAD" , aad , 3 , "KGM" ) ;
		}
		else	// 080813 
		{
			if ( dta.typ == KAUFLAND )
			{
				k = cepu.PAC_segment( pac , "","","201" /* lesegs1_leergut(pacverp_art)*/,"::9") ;	// Sendung besteht aus "pac" St�ck

			}
		}
		retnvea = lsnve_class.opennvea () ;	// Nochmals die identische Schleife �ber Basis-Ebene
		vorpalette[0] = '\0' ;
		retnvea = lsnve_class.lesenvea () ;
		while ( ! retnvea )
		{
			if ( strlen ( clippedi ( lsnve.masternve )) > 3 )	// Masternve darf es immer nur eine geben
			{
				if ( strcmp ( vorpalette , lsnve.masternve ))
				{	
					sprintf ( vorpalette , "%s" , lsnve.masternve ) ;
					pac = 0 ;
					memcpy ( &lsnve_save , &lsnve , sizeof ( struct LSNVE )) ;
					int retnveb = lsnve_class.opennveb () ;
					retnveb = lsnve_class.lesenveb () ;
// es MUSS dann auch ein master-Satz existieren !!!!!!!!
					while ( ! retnveb )
					{
						pac ++ ;
						if ( ! strcmp ( clippedi(lsnve.masternve) , clippedi (lsnve.nve_palette) ))	// Masterpalette sichern 
						{
							pac -- ;
							memcpy ( &lsnve_savem , &lsnve , sizeof ( struct LSNVE )) ;
						}
						retnveb = lsnve_class.lesenveb () ;

					}

					memcpy ( &lsnve , &lsnve_savem , sizeof ( struct LSNVE )) ;
					k = cepu.CPS_segment ( iCPS , iBAS ) ;
					iBAS = iCPS ;
					iCPS ++ ;
					if ( rheinstetten )
					{

						k = cepu.PAC_segment ( 1 ,":52","", "PX-H1" ,"::92") ;
 						k = cepu.MEA_segment ( "AAB", lsnve.brutto_gew , 3 , "KGM" ) ;
						k =	cepu.MEA_segment ( "LAY", pac , 0 , "PCE" ) ;
					}
					else
					{
						// if ( dta.typ == KAUFLAND )

						lesegs1_leergut(pacverp_art);	// 080813 : Lade obchep
						k = cepu.PAC_segment ( 1 , ":52",obchep,lesegs1_leergut(pacverp_art),"::9" ) ;	
					}
					k = cepu.GIN_segment ( "BJ" , clippedi (lsnve.masternve),"","","","","","","","","") ;
					memcpy ( &lsnve , &lsnve_save , sizeof ( struct LSNVE )) ;
					continue ;	// nochmal auswerten
				}
			}
			else
				iBAS = 1 ;	// Basis ist dann die Gesamtlieferung 

			if ( strlen ( clippedi ( lsnve.masternve )) > 3 )
			{
				if ( ! strcmp ( vorpalette , clippedi ( lsnve.nve_palette) ))
				{	// Masterpalette nicht nochmal abarbeiten
					retnvea = lsnve_class.lesenvea () ;
					continue ;	
				}
			}

			if ( doption == OPTPALETTE )
			{
					k = cepu.CPS_segment ( iCPS , iBAS ) ;
					iCPS ++ ;
					if ( rheinstetten )
					{
						k = cepu.PAC_segment ( 1 , ":52","","PX-H1" , "::9") ;
						k = cepu.MEA_segment ( "AAB", lsnve.brutto_gew , 3 , "KGM" ) ;
					}
					else
					{	// if ( dta.typ ==KAUFLAND)

						obchep,lesegs1_leergut(pacverp_art);	// 080813 : Lade obchep
						k = cepu.PAC_segment ( 1 , ":52",obchep,lesegs1_leergut(pacverp_art) , "::9") ;

					}
					if ( strlen (clippedi ( lsnve.nve_palette)) > 3 )	// 1208 : sonst liegt es direkt auf der Masternve OHNE eigne NVE
						k = cepu.GIN_segment ( "BJ" , clippedi ( lsnve.nve_palette),"","","","","","","","","") ;

					bel_pos = LIN_senden(bel_pos, lsnve.a ) ;	// sortenreine Palette ( Lage ) ist Pflicht
					if ( dta.typ != SELGROS )
					{
						if ( ! rheinstetten )
							k = cepu.PIA_segment (lsnve.a, a_kun.a_kun , "IN") ;	// tabelle a_kun wurde in "LIN_senden"  komplettiert 160613 : "BP"->"IN"
						else
						{	// 010612 : rheinstetten-Anforderungen ?!

							k = cepu.PIA_segment (lsnve.a, "" , "RS") ;	// Sonderschalter "Rheinstetten"
							if ( ( strlen ( a_kun.a_kun)) > 0 )
								k = cepu.PIA_segment (0 , a_kun.a_kun , "RS") ;	// Sonderschalter "Rheinstetten"
						}

					}

					if ( rheinstetten )	// 090611
					{	// rheinstetten : me beinhaltet St�ckzahl in Bestellmengeneinheit, netto_gew ist netto, brutto_gew ist brutto  

						if ( lsnve.brutto_gew < lsnve.netto_gew )	// fehlerhaft oder leer 
							lsnve.brutto_gew = lsnve.netto_gew ;

						k = cepu.MEA_segment ( "AAB" , lsnve.brutto_gew , 3 , "KGM" ) ;
						k = cepu.MEA_segment ( "AAA" , lsnve.netto_gew , 3 , "KGM" ) ;
						if ( lsnve.me > 0 )
							k = cepu.QTY_segment( "12" , lsnve.me , 1 , 0 ) ;
					}
					else	// bis 090611 
						// so hatte ich das auf folgender Basis vermutet :
						// me ist gleich liefermenge und liefermengen-Einheit
						// netto ist netto und brutto ist brutto 
						// es gibt auf jeden Fall Realisierungen, bei denen KEIN Gewicht
						// sondern nur St�ckzahlen angegeben werden
					{
						if ( dta.typ == KAUFLAND )	//080813 : Vermutlich auch f�r andere g�ltig
													// normalerweise in Bestelleinheit
													// Falls nicht egalsierte Ware, dann 
													// zus�tzlich in kg, aber nur auf Strecke ...
						{
								k = cepu.QTY_segment( "12" , lsnve.me , lsnve.me_einh , 1 ) ;
								if ( lsnve.me_einh == 2 && 1 == 2)
								{
									k = cepu.MEA_segment ( "AAA" , lsnve.me , 3 , "KGM" ) ;  // Netto-Gewicht
								}

						}
						else	// 080813 : mal sehen, wer das dann so haben will
						{
							if ( lsnve.me_einh == 2 )
							{
								k = cepu.MEA_segment ( "AAA" , lsnve.me , 3 , "KGM" ) ;
								k = cepu.MEA_segment ( "AAB" , lsnve.netto_gew , 3 , "KGM" ) ;
							}
							else
							{
								k = cepu.QTY_segment( "12" , lsnve.me , 1 , 0 ) ;
							}
						}
					}

					if ( dta.typ != FENEBERG )	// 160413
					{	// 230413/080813 : pci-segment dazu

						if (  ( lsnve.mhd.year  > lsk.lieferdat.year )
							|| ( lsnve.mhd.year  == lsk.lieferdat.year && lsnve.mhd.month > lsk.lieferdat.month )
							|| (lsnve.mhd.year  == lsk.lieferdat.year && lsnve.mhd.month == lsk.lieferdat.month &&	lsnve.mhd.day  >= lsk.lieferdat.day )
							|| (strlen ( clippedi ( lsnve.ls_charge )) > 1))
						{
							k = cepu.PCI_segment ( "17" ) ; 
					
							// MHD nach nur sinnvollem Plausi-Check 
							if ( ( lsnve.mhd.year  > lsk.lieferdat.year ) ||
								( lsnve.mhd.year  == lsk.lieferdat.year && lsnve.mhd.month > lsk.lieferdat.month ) ||
								(lsnve.mhd.year  == lsk.lieferdat.year && lsnve.mhd.month == lsk.lieferdat.month &&
								lsnve.mhd.day  >= lsk.lieferdat.day ))
							{
								k = cepu.DTM_segment ( "361" , lsnve.mhd , 102 , "" ) ;	// MHD
							}

							if ( dta.typ == KAUFLAND )	// 050913 
							{	// lt. Doku immer die 4 segmente(PCI-DTM-QTY-BX als Sequenz erwartet, kein MHD/Chargen-Mix unterhalb NVE erlaubt
								k = cepu.QTY_segment( "12" , lsnve.me , lsnve.me_einh , 1 ) ;// dupliziert die obige Menge .........
							}

							if ( strlen ( clippedi ( lsnve.ls_charge )) > 1 )
							{
								k = cepu.GIN_segment ("BX", lsnve.ls_charge , "","","","","","","","","" ) ;
							}
						}
					}

				gescount ++ ;
			}
			else	// doption == OPTALLES
			{
				//   Bei "Alles " : weitere Hierarchie einfuegen und auch noch Kisten aufloesen 

				k = cepu.CPS_segment ( iCPS , iBAS ) ;
				iAUF = iCPS ;
				iCPS ++ ;
				pac = 0 ;
				memcpy ( &lsnve_save , &lsnve , sizeof ( struct LSNVE )) ;
				int retnvec = lsnve_class.opennvec () ;
				retnvec = lsnve_class.lesenvec () ;
				while ( ! retnvec )
				{
					pac ++ ;	// etwas unscharf, da ich davon ausgehe, dass es nur einen Kistentyp gibt
					if ( ! strcmp ( clippedi(lsnve.nve) , clippedi (lsnve.nve_palette) ))	// Paletten-NVE sichern
					{
						pac -- ;
						memcpy ( &lsnve_savem , &lsnve , sizeof ( struct LSNVE )) ;
					}
						retnvec = lsnve_class.lesenvec () ;
				}

				memcpy ( &lsnve , &lsnve_savem , sizeof ( struct LSNVE )) ;

				if ( rheinstetten )
				{
					k = cepu.PAC_segment ( 1 , ":52","","PX-H1" , "::9") ;
					k = cepu.MEA_segment ( "AAB", lsnve.brutto_gew , 3 , "KGM" ) ;
					k = cepu.QTY_segment ( "12", pac , 1 , 1 ) ;
				}
				else
				{	// if ( dta.typ ==KAUFLAND)

					obchep,lesegs1_leergut(pacverp_art);	// 080813 : Lade obchep
					k = cepu.PAC_segment ( 1 , ":52",obchep,lesegs1_leergut(pacverp_art) , "::9") ;
				}
				if ( strlen (clippedi ( lsnve.nve_palette)) > 3 )	// 1208 : sonst liegt es direkt auf der Masternve OHNE eigne NVE
					k = cepu.GIN_segment ( "BJ" , clippedi ( lsnve.nve_palette),"","","","","","","","","") ;


				retnvec = lsnve_class.opennvec () ;
				retnvec = lsnve_class.lesenvec () ;
				while ( !retnvec )
				{
					if ( ! strcmp ( clippedi( lsnve.nve ) , clippedi ( lsnve.nve_palette) ))
					{	// Palette nicht nochmal abarbeiten
						retnvec = lsnve_class.lesenvec () ;
						continue ;
					}
					k = cepu.CPS_segment ( iCPS , iAUF ) ;
					iCPS ++ ;
					k = cepu.PAC_segment ( 1 , ":52","", holebezeichnung( atol( lesegs1_leergut(lsnve.verp_art))) , "::9" ) ;
					k = cepu.MEA_segment ( "AAB", lsnve.brutto_gew , 3 , "KGM" ) ;
					k = cepu.GIN_segment ( "BJ" , clippedi ( lsnve.nve),"","","","","","","","","") ;
					bel_pos = LIN_senden(bel_pos, lsnve.a ) ;	// sortenreine Kiste ist Pflicht
					if ( dta.typ != SELGROS ) // bei selgros : " alle infos stecken bereits in LIN" 
					{
						if ( !rheinstetten  )
							if ( dta.typ == METRO )
								k = cepu.PIA_segment (lsnve.a, a_kun.a_kun , "BP") ;	// so stehts schon in der rosi ...
							else
								k = cepu.PIA_segment (lsnve.a, a_kun.a_kun , "IN") ;	// a_kun wurde in "LIN_senden"  komplettiert 160613 : "BP"->"IN"
						else
						{	// 010612 : rheinstetten-Anforderungen ?!
							k = cepu.PIA_segment (lsnve.a, "" , "RS") ;	// Sonderschalter "Rheinstetten"
							if ( ( strlen ( a_kun.a_kun ) ) > 0 )
							k = cepu.PIA_segment (0 , a_kun.a_kun , "RS") ;	// Sonderschalter "Rheinstetten"
		
						}
					}

					if ( lsnve.me_einh == 2 )
					{
						k = cepu.MEA_segment ( "AAA" , lsnve.me , 3 , "KGM" ) ;
						k = cepu.MEA_segment ( "AAB" , lsnve.netto_gew , 3 , "KGM" ) ;
					}
					else
					{
						k = cepu.QTY_segment( "12" , lsnve.me , 1 , 0 ) ;
					}
					if ( dta.typ != FENEBERG )
					{
						if ( ( lsp.hbk_date.year >= lsk.lieferdat.year ) || (strlen ( clippedi(lsp.ls_charge))) > 0 )
						{
							int ii = cepu.PCI_segment ( "17" ) ; 

							// MHD nur sinnvollem Plausi-Check 
							if ( ( lsnve.mhd.year  > lsk.lieferdat.year ) ||
								( lsnve.mhd.year  == lsk.lieferdat.year && lsnve.mhd.month > lsk.lieferdat.month ) ||
								(lsnve.mhd.year  == lsk.lieferdat.year && lsnve.mhd.month == lsk.lieferdat.month &&
								lsnve.mhd.day  >= lsk.lieferdat.day ))
							{
								k = cepu.DTM_segment ( "361" , lsnve.mhd , 102 , "" ) ;	// MHD
							}

							if ( dta.typ == KAUFLAND )	// 050913 
							{	// lt. Doku immer die 4 segmente(PCI-DTM-QTY-BX als Sequenz erwartet, kein MHD/Chargen-Mix unterhalb NVE erlaubt
								k = cepu.QTY_segment( "12" , lsnve.me , lsnve.me_einh , 1 ) ;// dupliziert die obige Menge .........
							}

							if ( strlen ( clippedi ( lsnve.ls_charge )) > 1 )
							{
								k = cepu.GIN_segment ("BX", lsnve.ls_charge , "","","","","","","","","" ) ;
							}
						}
					}
					gescount ++ ;
					retnvec = lsnve_class.lesenvec () ;
				}

			}

			retnvea = lsnve_class.lesenvea () ;
		}
	}

	*pbadcount = badcount ;
	*pkbadcount = kbadcount ;
	*pgescount = gescount ;
	*pkgescount = kgescount ;
	k = cepu.CNT_segment (  "2" , bel_pos - 1 , 0  ) ;

}

void  erwposindatei (int * pbadcount, int * pkbadcount, int * pgescount, int * pkgescount )
{
	// 010411 : komplett aktiviert 
	// 080813 : hier kommt man NIE mehr hin ?!
    badcount = *pbadcount ;
    kbadcount = *pkbadcount ;
    gescount = *pgescount ;
    kgescount = *pkgescount ;

	int retlsp ;
	short ik ;
    short ime_einh_kun ;
    double iinh	;	// ( 12,3)
    short pme_einh ; 
	int retnve0, retnve1 ;	// sql-status der Cursoren 

    int cpscounter ;		// laufende CPS-Numerierung

    int ebene0 ;			// merker der aktuellen Ebene 0

    int bel_pos ;			// lfd. im Beleg

// Zwischenvariablen zur NVE-Generierung 
    double aktnve  ;		// aktueller wert
	char gesamtnve [30] ;
    char anfnve [30] ;		// die ersten 10 stellen ....

	bel_pos = 1  ;		// Start je Beleg    

   kgescount ++ ;
	if ((strlen ( clippedi ( kun.iln ))) < 3 )
	{
		sprintf ( protokoll , "LS: %d  nicht verbucht : fehlende ILN-Nummer" , lsk.ls ) ;
		protschreiben(1) ;
    
        kbadcount ++ ;
		retlsp = 0 ; // erster satz ist vorgelesen 
        while ( ! retlsp ) 
		{
			gescount ++ ;
			badcount ++ ;
			sprintf ( protokoll , "LS : %d Art: %1.0f Menge: %1.3f"
                    " nicht verbucht : Folgefehler " , lsk.ls, lsp.a , lsp.lief_me  ) ;
			protschreiben(1) ;
            retlsp = lsp_class.leselsp() ;
		}
		*pbadcount = badcount ;
		*pkbadcount = kbadcount ;
		*pgescount = gescount ;
		*pkgescount = kgescount ;
		return ;
	}

    cpscounter = 1 ;	// jetzt geht es richtig los .........

    int k = cepu.CPS_segment (cpscounter, 0 ) ;
	cpscounter ++ ;

    if ( k)
	{	*pbadcount = badcount ;		*pkbadcount = kbadcount ;		*pgescount = gescount ;		*pkgescount = kgescount ;
         return  ;
	}

    if (( doption == OPTPALETTE ) || ( doption == OPTALLES ))	// was anderes gibbet hier ja gar nicht ...
	{
	// #### erzeugt pac-segment ( n paletten ) #####
        k = anzahlpaletten () ;	//anzahlpaletten fertig bauen
		
		retnve0 = 0 ;
		if ( k == -1 )
			retnve0 = 100 ;
		while  ( ! retnve0 )
		{
			if ( a_bas.a_typ == 11 )
			{
// #### Leergut ueberlesen -> eigentlich redundant, da lg nicht in ls_nve
// #### drin stehen darf

				retnve0 = lsnve_class.lesenve0 () ;
				if (! retnve0 )
				{
					a_bas.a = lsnve.a ;
					a_bas_class.opena_bas () ;
					k = a_bas_class.lesea_bas () ;
					lsp.a = a_bas.a ;
					continue ;
				}
			}
			gescount ++ ;

// CPS_segment je Palette - genauer : 1. Ebene(i.a.Paletten) je Lieferung 
			k = cepu.CPS_segment (cpscounter, 1 ) ;
			cpscounter ++ ;

			sprintf ( anfnve , "3%s" , clippedi ( mdn.iln )); // relevant sind nur die ersten 1 + 7 Zeichen
			anfnve[8] = '\0' ;
			nvekomplett ( gesamtnve ,atof (lsnve.nve_palette ), anfnve, clippedi ( lsnve.nve_palette ) ) ;	
			k = cepu.GIN_segment ("BJ", gesamtnve, "0","0","0","0",
                                               "0","0","0","0","0") ;

			bel_pos = anzahlkarton ( bel_pos) ; // integriert gesamtes
                                                    // Handling dr Palette

			retnve0 = lsnve_class.lesenve0 () ;	// naechste Palette
			if (! retnve0 )
			{
				a_bas.a = lsnve.a ;
				k = a_bas_class.opena_bas () ;
				k = a_bas_class.lesea_bas () ;
				lsp.a = a_bas.a ;
			}
		}
	}
	*pbadcount = badcount ;
	*pkbadcount = kbadcount ;
	*pgescount = gescount ;
	*pkgescount = kgescount ;

}

void holesysdatum( TIMESTAMP_STRUCT * datum )
{

	time_t timer;
	struct tm *ltime;

	time (&timer);
	ltime = localtime (&timer);
/* --->
 sprintf ( Date,"%04d%02d%02d" , ltime->tm_year + 1900,
                                    ltime->tm_mon + 1,
                                    ltime->tm_mday);
<----- */

	datum->year		= ltime->tm_year + 1900 ;
	datum->month	= ltime->tm_mon + 1 ;
	datum->day		= ltime->tm_mday ;
	datum->fraction = 0 ;
	datum->second	= ltime->tm_sec ;
	datum->minute	= ltime->tm_min ;
	datum->hour		= ltime->tm_hour ;

}


int CdesadvDlg::Dateierstellung (void)
{

	int dkopfteil = 0;

	int gescount = 0 ;
	int badcount = 0 ;
	int kgescount = 0 ;
	int kbadcount = 0 ;

	protschreiben ( -1 ) ;

	int dretlsk ;
	int dretlsp ;

	lsk.mdn = mdn.mdn ;
	doption = OPTMENGE ;
	if ( dta.optiondesadv == OPTALLES )
		doption = OPTALLES ;
	if ( dta.optiondesadv == OPTKARTON )
		doption = OPTKARTON ;
	if ( dta.optiondesadv == OPTPALETTE )
		doption = OPTPALETTE ;

	rheinstetten = 0 ;	// 090611 / 270313 : rheinstettendta dazu 
	if (dta.optiondesadv == OPTPALETTE && dta.typ == EDEKA && dta.dta == rheinstettendta  )
		rheinstetten = 1 ;

	if ( dta.optiondesadv == OPTPALETTE || dta.optiondesadv == OPTALLES )
		ladegs1_leergut() ;	// 270613

	ref_nr = 1 ;

	kun.kun = -2 ;
	kuninka.kun = -2 ;

	sprintf ( kuninka.sedas_nr3 ,"%d", dta.dta ) ;

	dretlsk = lsk_class.openlsk(tourbeding ) ;
	if ( ! dretlsk )
		dretlsk =lsk_class_leselsk() ;

	if ( dretlsk )
	{
		// Datensatz nicht vorhanden 
			MessageBox("Keine Daten in Auswahl", " ", MB_OK|MB_ICONSTOP);
		return -1 ;
	}

	int k ;

	inummer = dta.nummer ;
	if (inummer < 1 )
		inummer = 1 ;
	inummer ++ ;
	dta.nummer = inummer ;	// Das ist die naechste freie Referenz-Nummer
	dta_class.updatedta() ;	// es wird zwar alles geupdatet, jedoch eigentlich wird nur die Nummer hochgez�hlt

	inummer -- ;	// das ist die aktuelle Referenz-Nummer
	int leng ;


	dateinamefertig = FALSE ;	// 130114

// 130813 : umorganisiert 

	// 1. desadv.cfg
	// 2. dta.dat_name
	// 3. %DESADV%
	// 4. %BWS%\desadv	// Notbremse


// 130813 : Zielverzeichnis relationspsezifisch aus desadv.cfg holen
	char relatname[ 100];
	wsprintf ( relatname,"DEST%d", dta.dta );
	char *zieldir =  get_defa ( relatname )	;

	if ( zieldir == NULL )
		wsprintf ( ddateiname,"" ) ;
	else
		wsprintf ( ddateiname , "%s" , zieldir ) ;

	if ( strlen ( ddateiname ) < 3 )
	{
		if ( strlen( clippedi( dta.dat_name )) > 3 )	
			wsprintf( ddateiname, "%s", dta.dat_name );	// nur 16 zeichen und systemweit -> bad variante
		else
			wsprintf( ddateiname, "%s", getenv("DESADV") );

		if ( strlen ( ddateiname ) < 3 )
		{
			wsprintf( ddateiname, "%s", getenv("BWS") );
			leng = (int) strlen ( ddateiname ) ;
			wsprintf ( ddateiname + leng , "\\desadv") ;
		}
	}

	_mkdir ( ddateiname ) ;

	leng = (int)strlen ( ddateiname ) ;

	long iinum = inummer ;
	while (iinum > 999999)
		iinum -=  1000000 ;
	while (iinum > 99999)
		iinum -=  100000 ;
	while (iinum > 9999)
		iinum -=  10000 ;

/* --- mal lieber ganz viele Dateinamen 150313 ab heute fuer alle

if ( dta.dta != FENEBERG )
	while (iinum > 999)
		iinum -=  1000 ;
< -----*/



// 010411 : fuer EDEKA sind besonders eklige Namen gew�nscht / 150313 von unten hierher : globalisieren 
	char idatum [7] ;
	char izeit  [5] ;
	cepu.getsysdatum ( idatum, izeit );	// Systemdatum : "yymmdd"


// 230514 : Herr Schick w�nscht sich diese Struktur f�r alle ausser f�r FENEBERG

//	if ( dta.typ == EDEKA )	230415
	if ( dta.typ != FENEBERG )
	{

		// SENDER-GLN_EMPF-GLN_12345678_DESADV_JJJJMMTT.EDI
		wsprintf ( ddateiname + leng , "\\%s_%s_%08d_DESADV_20%s.edi", clippedi(mdn.iln), clippedi(dta.iln), inummer, idatum) ;
		wsprintf ( EchtDateiNameKurz , "%s_%s_%08d_DESADV_20%s.edi", clippedi(mdn.iln), clippedi(dta.iln), inummer, idatum) ;	// 090413
		dateinamefertig = TRUE ;	// 130114
	}
	else
//	if ( dta.typ == FENEBERG )
// 130114 : alles sp�ter : wsprintf ( ddateiname + leng , "\\desadv%d.%04d", dta.dta, iinum) ;
		wsprintf ( EchtDateiNameKurz , "desadv%d.%04d", dta.dta, iinum) ;
		wsprintf ( EchtDateiNameKurzMaske , "desadv%d.%04d", dta.dta, iinum) ;	// 130114
//	else
//		wsprintf ( ddateiname + leng , "\\desadv%d.%03d", dta.dta, iinum) ;


// Anforderung von Herr Schick am 13.01.2014 : Liefer-GLN soll mit in den Namen : eigentlich logik-limitierend(Nur ein Liefer-empf�nger je Datei) , aber es ist der Wunsch des Kunden
	if ( dta.typ == FENEBERG )	
		wsprintf ( EchtDateiNameKurzMaske , "desadv%d_%%s.%04d", dta.dta, iinum) ;	// Masken-Format 130114

// 150313 A : Temp-Datei auch wirklich ins temp-Verzeichnis 

	wsprintf( ddateinamex, "%s", getenv("TMPPATH") );
	leng = (int) strlen ( ddateinamex ) ;
	wsprintf ( ddateinamex + leng , "\\desadv") ;
	_mkdir ( ddateinamex ) ;

	leng = (int)strlen ( ddateinamex ) ;

//	if ( dta.typ == EDEKA )	230415
	if ( dta.typ != FENEBERG )
	{
		// SENDER-GLN_EMPF-GLN_12345678_DESADV_JJJJMMTT.EDIintern
		wsprintf ( ddateinamex + leng , "\\%s_%s_%08d_DESADV_20%s.ediintern", clippedi(mdn.iln), clippedi(dta.iln), inummer, idatum) ;
		dateinamefertig = TRUE ;	// 130114
	}
	else
	{
// 130114 : alles sp�ter :	wsprintf ( ddateinamex + leng , "\\desadv%d.%04dintern", dta.dta, iinum) ;
	}
// 150313 E : Temp-Datei auch wirklich ins temp-Verzeichnis 

	
	while ( !dretlsk )
	{
		lsp.mdn = mdn.mdn ;
		lsp.ls = lsk.ls ;
		dretlsp = lsp_class.openlsp() ;
		if (!dretlsp) 
			dretlsp = lsp_class.leselsp() ;
		if ( dretlsp )	// Keine Positionen vorhanden
		{
			dretlsk =lsk_class_leselsk() ;
			continue ;
		}
		if ( ! dkopfteil )
		{
			if ( !dateinamefertig )	// 1301014 nur einmalig je Datei 
			{
				int leng ;
				// Das Feld kun.iln ist hier bereits sinnvoll best�ckt durch lsk_class_leselsk ....
				leng = (int)strlen ( ddateiname ) ;
				if ( dta.typ == FENEBERG )
				{

					wsprintf ( EchtDateiNameKurz, EchtDateiNameKurzMaske, clippedi (kun.iln) ); 
				}

				wsprintf ( ddateiname + leng , "\\%s", EchtDateiNameKurz) ;


				leng = (int)strlen ( ddateinamex ) ;
				if ( dta.typ == FENEBERG )
				{
					wsprintf ( EchtDateiNameKurz, EchtDateiNameKurzMaske, clippedi (kun.iln) ); 
				}

				wsprintf ( ddateinamex + leng , "\\%sintern", EchtDateiNameKurz) ;
				dateinamefertig = TRUE ;
			}

			k = cepu.Kopfteil(ddateiname ,ddateinamex , inummer )  ;
		}
		dkopfteil = 1 ;
		if ( k ) return ( -1 ) ;

		k = cepu.UNH_segment ( ref_nr, "DESADV" ) ;
		k = cepu.BGM_segment ( "351", lsk.ls, 9 ) ;


		TIMESTAMP_STRUCT genaujetzt ;
		holesysdatum ( &genaujetzt ) ;


		if ( dta.typ == KAUFLAND )
		{
			k = cepu.DTM_segment ( "137", lsk.lieferdat , 102, "" ) ;			// "Ausstellung des Dokuments"
			k = cepu.DTM_segment ( "35", lsk.lieferdat , 102, "" ) ;			// "IST-Lieferdat "alternativ darf auch 17 ..." -geben die in ihrem Beispeil dann selber nicht an ..
			k = cepu.DTM_segment ( "2", lsk.lieferdat , 102, "" ) ;			// "Wunsch-Lieferdat muss gleich datum aus Bestellung ..."
		}
		else
		{
			k = cepu.DTM_segment ( "137", genaujetzt , 102, "" ) ;			// "Ausstellung des Dokuments"
			if ( rheinstetten )	// 010612
			{
				k = cepu.DTM_segment ( "2", lsk.lieferdat ,102, "" ) ;	// Rheinstetten will das so .....
			}
			else
			{
				k = cepu.DTM_segment ( "17", lsk.lieferdat ,102, "" ) ;	// gesch�tztes Lieferdatum
			}
		}


		char hilfwert [43] ;



		if ( dta.typ == METRO )	// nur fuer METRO ( z.B. SELGROS und EDEKA nicht )
		{
			if (sypmetro1_par == 1 )
				sprintf ( hilfwert, "%s" , clippedi( kuninka.sedas_nr2) ) ;
			else
				sprintf ( hilfwert, "%s" , suche_abkommen ()) ;
			k = cepu.RFF_segment ( "API" , hilfwert ) ;
		}


		protschreiben ( 0 ) ;

		if (strlen ( clippedi ( lsk.auf_ext )) > 2 )
		{
			k = cepu.RFF_segment ( "ON" , lsk.auf_ext ) ;

			// eigentlich geh�rt hier lsk.best_dat rein, aber das haben wir ja nur selten aktuell ....
			if ( dta.typ != FENEBERG && dta.typ != KAUFLAND )
			{
				if ( lsk.best_dat.year == lsk.lieferdat.year || lsk.best_dat.year == lsk.lieferdat.year - 1 )
					k = cepu.DTM_segment ( "171", lsk.best_dat ,102 , "" ) ;
				else
					k = cepu.DTM_segment ( "171", lsk.lieferdat ,102 , "" ) ;
			}
		}
		sprintf ( hilfwert , "%d" , lsk.ls ) ;
		k = cepu.RFF_segment ( "DQ" , hilfwert ) ;

		k = cepu.NAD_segment ( "SU" , cepu.eigen_iln , 0  ) ;	// wird in "Kopfteil" gesetzt
		k = cepu.NAD_segment ( "BY" , clippedi(kun.rechiln) , 0  ) ;	// wird beim lsk-lesen gesetzt
		if ( dta.typ == FENEBERG )	// 150313 
		{
			sprintf ( hilfwert, "%s" , clippedi( kuninka.sedas_nr2) ) ;
			if ( strlen ( hilfwert ) > 2)
			k = cepu.RFF_segment ( "API" , hilfwert ) ;
		}

		if ( dta.typ == FENEBERG || dta.typ == KAUFLAND || ( dta.typ == EDEKA && !rheinstetten)  )	// 150313/ 250613
			k = cepu.NAD_segment ( "DP" , clippedi(kun.iln) , lsk.adr  ) ;		// wird beim lsk-lesen gesetzt
		else
			k = cepu.NAD_segment ( "DP" , clippedi(kun.iln) , 0  ) ;		// wird beim lsk-lesen gesetzt


		if ( dta.typ == KAUFLAND )
		{
			sprintf ( hilfwert, "%s" , clippedi( kuninka.sedas_nr2) ) ;
			if ( strlen ( hilfwert ) > 2)
			k = cepu.RFF_segment ( "API" , hilfwert ) ;
		}

// "IT" : kaufland-Lieferanten-Nummer : kun.sedas_nr2 ( like metro1_par !!)
    // "API" : Kundennummer beim Lieferanten ( kun.kun ) -Kann-Segment bei KL 

// (- EDEKA) RFF+DQ : fit-LS-Nummer
// (+ EDEKA) RFF+VN : Referenznummer des Lieferanten f�r Auftrag des Kunden ( d.h. unsere Auftragsnummer oder LS-Nummer ) 
// (+ EDEKA) NAD+IV : Rechnungsempfaenger ( == Buyer ? )

// 060511 : if strlen ( clippedi( lsk.loc)) > 0 )	// noch nicht aktiviert, da das feld lsk.loc noch nicht passt
// 060511 : k = cepu.LOC_sgement ( lsk.loc ) ; 

		if ( dta.typ == EDEKA && (  doption == OPTALLES || doption == OPTPALETTE ) )
			if ( rheinstetten)
				k = cepu.LOC_segment ( "2" ) ;	// fix loc == 2 , das ist aber Kaese und muss bald wieder weg 


		if ( doption == OPTPALETTE || doption == OPTALLES || doption == OPTKARTON )
		{
			if ( doption == OPTKARTON )
				nurnveablauf (& badcount, & kbadcount, & gescount, & kgescount) ;	// nurkarton-Ablauf bisher unfertig
			else
				nurnveablauf (& badcount, & kbadcount, & gescount, & kgescount) ;
		}
		else
		{
// 180511 : Hier kommt man nur noch bei "ohne NVE" hin .......
			if ( doption == OPTPALETTE || doption == OPTALLES)	
				erwposindatei (& badcount, & kbadcount, & gescount, & kgescount) ;	
			else
				posindatei (& badcount, & kbadcount, & gescount, & kgescount) ;
		}
		k = cepu.UNT_segment ( ref_nr ) ;
		ref_nr ++ ;

		dretlsk =lsk_class_leselsk() ;
	}


	if ( dkopfteil )
		k = cepu.Fussteil() ;
	else
		sprintf ( ddateiname , "" ) ;

	if ( internmdn > 0 && internls > 0 && interndta > 0 )
	{
		// 150313
	}
	else
	{
		protdrucken () ;
	}
	return 0 ;
}

void CdesadvDlg::OnCbnSelchangeCombo1()
{
	char bufh[400] ;

	UpdateData(TRUE) ;
//	int i = m_Combo1.GetLine(0,bufh,9) ;

	int nCurSel = ((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->GetCurSel();
	CString bufc;
	((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->GetLBText(nCurSel, bufc);

	sprintf(bufh,"%s",bufc.GetBuffer(0)) ;

	// fixe formatierung : 8 stellen und 2 blanks , danch folgt ein text
	int i = (int)strlen ( bufh) ;
	if ( i )
	{
		if ( i < 9 )
		{
			bufh[i] = '\0' ;
		}
		else
		{
			bufh[9] = '\0' ;
		}
		dta.dta = atol ( bufh ) ;
		i = dta_class.opendta();
		i = dta_class.lesedta();
	}
	
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
}

void CdesadvDlg::OnCbnKillfocusCombo1()
{
	
	UpdateData(TRUE) ;
	bufh[0] = '\0' ;

	int nCurSel = ((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->GetCurSel();
	CString bufx;
	if ( nCurSel > -1 )
	{
		((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->GetLBText(nCurSel, bufx);

		sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
		int i = (int) strlen ( bufh );
		// fixe formatierung : 8 stellen und 2 blanks , danach folgt ein text
		if ( i )
		{
			if ( i < 9 )
			{
				bufh[i ] = '\0' ;
			}
			else
			{
				bufh[9] = '\0' ;
			}
			dta.dta = atol ( bufh ) ;
			i = dta_class.opendta();
			i = dta_class.lesedta();
			if ( i )
			{
				v_combo1.Format("                   ");
			}
			else
			{
				if ( dta.reli == 1 )
				{
				}
				else
				{	
				}
			}

		}
		else
		{
				v_combo1.Format("                   ");
		}
	}
	else
	{
		sprintf ( bufh, "%s", v_combo1 );
		int i = (int) strlen ( bufh );
		// fixe formatierung : 8 stellen und 2 blanks , danach folgt ein text
		if ( i )
		{
			if ( i < 9 )
				bufh[i ] = '\0' ;
			else
				bufh[9] = '\0' ;
		}
		long hilfe = atol ( bufh ) ;
		nCurSel = -1 ;
		sprintf ( bufh, "%8.0d", hilfe );
		nCurSel=((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->FindString(nCurSel, bufh);
	
		if (nCurSel != CB_ERR)
		{
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->SetCurSel(nCurSel) ;
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->GetLBText(nCurSel, bufx);
			sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
			i = (int) strlen ( bufh );
			// fixe formatierung : 8 stellen und 2 blanks , danach folgt ein text
			if ( i )
			{
				if ( i < 9 )
					bufh[i] = '\0' ;
				else
					bufh[9] = '\0' ;
				dta.dta = atol ( bufh ) ;
				i = dta_class.opendta();
				i = dta_class.lesedta();
				if ( i )
				{
					v_combo1.Format("                   ");
				}
				else
				{
					v_combo1.Format(bufx.GetBuffer(0));
					if ( dta.reli == 1 )
					{

					}
					else
					{
					}

				}

			}
			else
			{
				v_combo1.Format("                   ");
			}
		}
		else
		{
				v_combo1.Format("                   ");
		}
	}
	UpdateData(FALSE);

}

void CdesadvDlg::OnBnClickedEinzel()
{

	if ( x_checkeinzel == FALSE )
	{
		x_checkeinzel = TRUE ;
		m_vdat.EnableWindow( FALSE );
		m_bdat.EnableWindow( FALSE );
		m_statictour.SetWindowTextA ("LS.-Nr. :") ;

	}
	else
	{
		x_checkeinzel = FALSE ;
		m_vdat.EnableWindow( TRUE );
		m_bdat.EnableWindow( TRUE );
		m_statictour.SetWindowTextA("Tour :") ;
	}
//	UpdateData(TRUE);
}
