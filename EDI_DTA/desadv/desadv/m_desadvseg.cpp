#include "stdafx.h"
#include "dbClass.h"	// sollte immer ziemlich weit vorne stehen ;-)
#include "m_desadvseg.h"
#include "dta.h"
#include "adr.h"
#include "mdn.h"


extern ADR_CLASS adr_class ;
extern DWORD ProcWaitExec (LPSTR prog, WORD SHOW_MODE, int x, int y, int cx, int cy);
extern char EchtDateiNameKurz[99] ;	// 090413
extern char EchtDateiNameKurzMaske[111] ;	// 130114


/* ---->
= CONSTANT
= CMAXART         value 300               // max 300 versch. Artikel 
= CMAXKTO         value 50                // max 50 versch. Sachkonten-Saetze
= CMAXLIS         value 100                // max 50 versch. Sachkonten-Saetze
= // dta_typ                      Numerierung AK-Handel 2002

= // sedas_abr

= KAUFHALLE       value 1

= EINZELK33       value 33

= END
< ------ */                                                         

// field drelease char (1) value "B"
// ##### "A" fuer Frankengut
// ##### "B" fuer BILLA(REWE-AUSTRIA)
// ##### "C" fuer alle anderen


/* ---->
field waehrung smallint 
field kst      integer  
field erl_kto  integer  
field betr_nto decimal (12,2) 
field mwst     smallint 
field mwst_betr decimal (12,2)

DATA

field psrechpin     integer
field psrechpout    integer
field dsuchen       integer
field dsuchmax      integer

field dabkommensnr  integer
field dmge_lief_nr  integer
field irech_kto_point integer
field m_abk_nr   integer        dimension(20)
field m_mge_lief integer        dimension(20)
field dabk       integer

field dsys_p_r  smallint
field dsys_p_w  char ( 2)
field dsys_p_b  char (20)
field dnachkpreis       smallint
field drewe_sch_par     smallint
field dorewe_sch_par    smallint
field dodtatyp          like dta.typ
field drewe_sch_stat    smallint
field deane_par         smallint
field dwiechmann_par    smallint
field ddietzsch_par     smallint
field dli_a_par         smallint
field dli_a_edi         smallint
field dprahm_par        smallint
field ddfw_par          smallint
field dmetro1_par       smallint
field dmetro_gut        smallint
< ---- */
/* --->
field crust_nummer       char ( 20 )
field cmust_nummer       char ( 20 )
< ----- */


/* ---->

field dateinamex char(120)      // edi-Datei intern
field dateiname  char(120)      // edi_datei extern
field dateinamey char(120)      // edi_datei zum Anzeigen

field danz_seg          integer // Anzahl Segmente einer Nachricht
field k                 smallint        // allg. Variable
field aktion_sel        smallint
field aktion_txt        char(10)
field seite             smallint
field sanfang           smallint dimension(10)
field dta_referenz      integer   // Datei-Nummer
field rechempf_iln      char(16)
field rechempf_kun      integer
field eigen_iln         char(16)
field liefempf_iln      char(16)
field dta_iln           char(16)
field dlsret            smallint
field mwstmark1         char (5)
field mwstmark2         char (5)
field mwstmarkf1        smallint
field mwstmarkf2        smallint
field mwstmarkf6        smallint


field rechen1           decimal (15,2)
field gessumme          decimal (15,2)
field gesmwst1          decimal (15,2)
field gesmwst2          decimal (15,2)
field gessumme1         decimal (15,2)
field gessumme2         decimal (15,2)
field gessumme6         decimal (15,2)
field dreweedi_listnr   integer

field startnummer       integer
field zielnummer        integer

field endproz           decimal (15,2)
field endrabwert        decimal (15,2)
field rabbasis          decimal (15,2)
field dnorm_akt         smallint

field dmeinh             smallint
field dtsmt             smallint

field zrflag           smallint  value 1      // 1 = ZR-Regul-Liste
                                              // 0 = Liste je Regul

field gutflag           smallint value 0        // 0 = Gutschriften werden
                                                // als neg. LS abgearbeitet
                                                // da uns i.a. die Referenz
                                                // zum Ursprungsbeleg fehlt

                                                // 1 = Gutschriften komplett
                                                // handeln
                                                // ( noch nicht realisiert am 07.10.00 )


END

< ---- */

char *clipped (char *string)
{
 char *clstring;
 short i,len;

 len = (short) strlen (string);

 if (len == 0) return (string);
 len --;
 clstring = string;

 for (i = len; i > 0; i --)
 {
  if ((unsigned char) clstring[i] > 0x20)
  {
   break;
  }
 }
 clstring [i + 1] = 0;

 clstring = string;
 return clstring ;
 // Vor-clipp wird mal deaktiviert 
 len = (short) strlen (clstring);

 for (i = 0; i < len; i ++, clstring +=1)
 {
  if ((unsigned char) *clstring > (unsigned char) 0X20)
  {
   break;
  }
 }
 return (clstring);
}

void dispmsg ( int text, int verfahren )
{
	char AusgabeText [256] ;	// hier ist noch was zu tun , erst mal nur ein dummy 
	verfahren++ ;	// syntax-dummy

	if ( ! strlen (clipped(AusgabeText)))
	{
	if ( text== 250 )
			sprintf ( AusgabeText, "Environment \"EDI\" fehlt" );
		
		if ( text== 251 )
			sprintf ( AusgabeText, "Fehler beim Schreiben" ) ;
		if ( text== 17 ) 
//			sprintf ( AusgabeText, "Datei existiert bereits : %s", dateiname );
			sprintf ( AusgabeText, "Datei existiert bereits " );
	
	}
}




/* ------>
PROCEDURE eange
PARAMETER
field eingabe char(12)
END
	
field ausgabe char (13)
field point1 integer
field wicht  integer
field platz  integer
field testchar char(1)
field summe  integer
  init ausgabe
  let ausgabe = eingabe
  if ( strlen ( ausgabe )) < 9
        return ( ausgabe )      // 8 stell. ean NICHT nochmals verknutteln
  end
  let point1 = 1
  let summe  = 0
  let wicht  = 1
  while ( point1 < 13 )
	
      let testchar = eingabe[point1]
      let platz    = testchar
      let summe = summe + ( platz * wicht )
      if wicht = 1 
	   let wicht = 3
      else
	   let wicht = 1
      end
      let point1 = point1 + 1 
  end

  let wicht = summe / 10              // Quotient 
  let platz = summe - ( wicht * 10 )  // modul 10 
  if platz = 0
      let ausgabe [13] = "0"
  else
      let ausgabe [13] = PICTURE ( ( 10 - platz ) , "&" )
  end
  return ( ausgabe )

END
< ----- */

/** Trennzeichen mit aktuellen Werten laden **/

void CEDIPUT::trennz_laden(void ) 
{
   sprintf ( trenn_se ,"%s", TRENN_SE ) ;
   sprintf ( trenn_de ,"%s", TRENN_DE ) ;
   sprintf ( trenn_gd ,"%s", TRENN_GD ) ;
   sprintf ( trenn_ma ,"%s", TRENN_MA ) ;
   sprintf ( trenn_dz ,"%s", TRENN_DZ ) ;
}     // proc trennz_laden

char * CEDIPUT::maskel ( char * eingabestr )
{
// char ausgabestring[110] ;
int iii, jjj, kkk ; 
char hilfchar[2] ;
    iii = 0;
    jjj = 0;
	sprintf ( ausgabestring, "%s", eingabestr );	// wegen const-char-clipped umladen
    kkk = (int) strlen ( clipped( ausgabestring ));
    while ( iii < kkk )
	{
		hilfchar[0] = eingabestr[iii++] ;
		hilfchar[1] = '\0' ;
        if ( ( ! strncmp( hilfchar, trenn_se, 1) )
          || ( ! strncmp( hilfchar, trenn_de, 1) )
          || ( ! strncmp( hilfchar, trenn_gd, 1) )
          || ( ! strncmp( hilfchar, trenn_ma, 1) ) )
		{
//          or ( ! strncmp( hilfchar, trenn_dz, 1) )
            ausgabestring[jjj++] = trenn_ma[0] ;
		}
        ausgabestring[jjj++] = hilfchar[0] ;
	}
	ausgabestring[jjj++] = '\0' ;
	return ( ausgabestring );
//    return ( proc filterg ( ausgabestr ) )
}


/* --->
// das waere ein Filter fuer ohne Umlaute

PROCEDURE filter

PARAMETER field ssssss char (100) END

field outstring char (110)
field ipo integer
field opo integer
field strl integer
field char1 char ( 1 )

  init outstring
  init char1
  let ipo = 1 
  let opo = 1
  let strl = strlen ( ssssss )

    while ( ipo <= strl )

        getstr ( ssssss , char1 , ipo , 1 )
        if char1 = "�"
//            let char1 = "}"
            let char1 = "u"
            putstr ( outstring , char1 , opo , 1 )
            let char1 = "e"

        elseif char1 = "�"
//            let char1 = "{"
            let char1 = "a"
            putstr ( outstring , char1 , opo , 1 )
            let char1 = "e"
        elseif char1 = "�"
//            let char1 = "]"
            let char1 = "U"
            putstr ( outstring , char1 , opo , 1 )
            let char1 = "e"
        elseif char1 = "�"
//            let char1 = "["
            let char1 = "A"
            putstr ( outstring , char1 , opo , 1 )
            let char1 = "e"
        elseif char1 = "�"
//             let char1 = "|"
            let char1 = "o"
            putstr ( outstring , char1 , opo , 1 )
            let char1 = "e"
        elseif char1 = "�"
//             let char1 = "\\" 
            let char1 = "O"
            putstr ( outstring , char1 , opo , 1 )
            let char1 = "e"
        elseif char1 ="�"
//             let char1 = "~"
            let char1 = "s"
            putstr ( outstring , char1 , opo , 1 )
            let char1 = "s"
        elseif char1 = "�"      // ae
//            let char1 = "{"
            let char1 = "a"
            putstr ( outstring , char1 , opo , 1 )
            let char1 = "e"
        elseif char1 = "�"      // Ae
//            let char1 = "["
            let char1 = "A"
            putstr ( outstring , char1 , opo , 1 )
            let char1 = "e"
        elseif char1 = "�"      // ue
//            let char1 = "}"
            let char1 = "u"
            putstr ( outstring , char1 , opo , 1 )
            let char1 = "e"
        elseif char1 = "�"      // Ue
//            let char1 = "]"
            let char1 = "U"
            putstr ( outstring , char1 , opo , 1 )
            let char1 = "e"
        elseif char1 = "�"      // oe
//             let char1 = "|"
            let char1 = "o"
            putstr ( outstring , char1 , opo , 1 )
            let char1 = "e"
        elseif char1 = "�"      // Oe
//             let char1 = "\\" 
            let char1 = "O"
            putstr ( outstring , char1 , opo , 1 )
            let char1 = "e"
        elseif char1 ="�"       // sz
//             let char1 = "~"
            let char1 = "s"
            putstr ( outstring , char1 , opo , 1 )
            let char1 = "s"
	end
        putstr ( outstring , char1 , opo , 1 )
    end
    return (outstring)
END

// das waere ein Filter fuer mit Umlaute like CSB

PROCEDURE filterg

PARAMETER field ssssss char (100) END

field outstring char (110)
field ipo integer
field opo integer
field strl integer
field char1 char ( 1 )

  init outstring
  init char1
  let ipo = 1 
  let opo = 1
  let strl = strlen ( ssssss )

    while ( ipo <= strl )

        getstr ( ssssss , char1 , ipo , 1 )
        if     char1 = "�"
                let char1 = "�"
        elseif char1 = "�"
                let char1 = "�"
        elseif char1 = "�"
                let char1 = "�"
        elseif char1 = "�"
                let char1 = "�"
        elseif char1 = "�"
                let char1 = "�"
        elseif char1 = "�"
                let  char1 = "�"
        elseif char1 ="�"
                let char1 ="�"
	end
        putstr ( outstring , char1 , opo , 1 )
    end
    return (outstring)
END
< ---- */


int CEDIPUT::UNA_segment (void)
{

//    let dstring20 = "UNA:+.?*'"         // "*" lt. DESADV-Beschreibung
//     let dstring20 = "UNA:+.? '"
        sprintf ( dstring20 , "UNA" );
        strcat ( dstring20, trenn_gd) ;
        strcat ( dstring20, trenn_de) ;
        strcat ( dstring20, trenn_dz) ;
        strcat ( dstring20, trenn_ma) ;
        strcat ( dstring20, " ") ;		// oder "*" ???????
        strcat ( dstring20, trenn_se) ;
        strcat ( dstring20, "\n") ;
	
		int k = fprintf ( fpouti ,dstring20 );
		if ( k > 0 ) return 0 ;
		return -1 ;
   
}     // proc UNA_segment

// output soll sysdate im format yymmdd sein und zeit im Format hhmm .... 

void CEDIPUT::getsysdatum( char * datum, char * zeit )
{

	char tmpbuf[128];
    _tzset();

	char ihh[3];
	char imm[3];

    _strtime( tmpbuf );
// printf( "OS time:\t\t\t\t%s\n", tmpbuf );
// OS time:            10:05:36


	ihh[0] = tmpbuf[00] ;
	ihh[1] = tmpbuf[01] ;
	ihh[2] = '\0' ;

	imm[0] = tmpbuf[03] ;
	imm[1] = tmpbuf[04] ;
	imm[2] = '\0' ;

	sprintf ( zeit, "%s%s", ihh,imm );

    _strdate( tmpbuf );
//    printf( "OS date:\t\t\t\t%s\n", tmpbuf );
// OS date:            01/22/99

	char idd[3];
	char iyy[3];

	idd[0] = tmpbuf[3] ;
	idd[1] = tmpbuf[4] ;
	idd[2] = '\0' ;

	imm[0] = tmpbuf[0] ;
	imm[1] = tmpbuf[1] ;
	imm[2] = '\0' ;

	iyy[0] = tmpbuf[6] ;

/* ---->
	if ( iyy[0] == '1' )	// ab Jahr 2000 ;-)
				// braucht es aber gar nicht, Format ist ja korrekt : /mm/dd/yy
	{
		iyy[0] = tmpbuf[7] ;
		iyy[1] = tmpbuf[8] ;
		iyy[2] = '\0' ;
	}
	else
	{
< ------ */
		iyy[1] = tmpbuf[7] ;
		iyy[2] = '\0' ;
//	}
	sprintf ( datum, "%s%s%s", iyy,imm,idd);
}


int CEDIPUT::UNB_segment(long edta_referenz)
{
// char hilfzeit[9] ;

char idatum [7] ;
char izeit  [5] ;
char testfeld [6] ;       // test_kz neu handeln

	dta_referenz = edta_referenz ; 
	getsysdatum ( idatum, izeit );	// Systemdatum : "yymmdd"
										// Systemzeit  : "hhmm"

    if ( dta.test_kz == 0 )
		testfeld[0] = '\0' ;
    else
        sprintf ( testfeld , "%s1" , trenn_de);

// das folgende aehnelt dem edeka-Kram am meisten 
// Frage UNOA oder UNOC -> ich schreibe mal immer "C", entweder wird es akzeptiert oder eben nicht
	sprintf ( dstring200, "UNB%sUNOC%s3%s%s%s14%s%s%s14%s%s%s%s%s%01.0d%s%sDESADV%s%s%sEANCOM%s%s\n"
			/*UNB*/,trenn_de/*UNOA*/,trenn_gd/*"3"*/,trenn_de
			,eigen_iln ,trenn_gd /* "14"*/, trenn_de
			,dta_iln ,trenn_gd /* "14"*/, trenn_de
			,idatum ,trenn_gd ,izeit ,trenn_de
			,dta_referenz , trenn_de /* "DFW:AA*/
			,trenn_de/*DESADV*/,trenn_de,trenn_de,trenn_de/* "EANCOM" */
			, testfeld ,trenn_se);




if ( 1 == 3 )
{
    
	if ( ddfw_par )
	{

// beachte : rauswaerts ilns anders herum als reinwaerts
/* ----> 
		 let dstring200 = "UNB" #  trenn_de # "UNOA" # trenn_gd # "3" # trenn_de
            # clipped ( eigen_iln ) # trenn_gd # "14" # trenn_de
            # clipped ( dta.iln )   # trenn_gd # "14" # trenn_de
//            # clipped ( eigen_iln ) # trenn_de
//            # clipped ( dta.iln )   # trenn_de
            # idatum # trenn_gd # izeit # trenn_de
            # clipped (PICTURE ( dta_referenz , "<--------&" ))

            # trenn_de # "DFW:AA" // Achtung : genau und nur fuer DFW
            # trenn_de # "ORDERS"
            # trenn_de
            # trenn_de
            # trenn_de # "EANCOM"

            # trenn_se
< ------ */

// z.Z. komplett deaktiviert .....
		sprintf ( dstring200, "UNB%sUNOA%s3%s%s%s14%s%s%s14%s%s%s%s%s%01.0d%sDFW:AA%sDESADV%s%s%sEANCOM%s%s\n"
			/*UNB*/,trenn_de/*UNOA*/,trenn_gd/*"3"*/,trenn_de
			,eigen_iln ,trenn_gd /* "14"*/, trenn_de
			,dta_iln ,trenn_gd /* "14"*/, trenn_de
			,idatum ,trenn_gd ,izeit ,trenn_de
			,dta_referenz , trenn_de /* "DFW:AA*/
			,trenn_de/*DESADV*/,trenn_de,trenn_de,trenn_de/* "EANCOM" */
			, testfeld ,trenn_se);
	}
	else  // das hier fuer alle ....
	{

/* ----> Original von orders ....
		sprintf ( dstring200, "UNB%sUNOA%s3%s%s%s14%s%s%s14%s%s%s%s%s%01.0d%s%sDESADV%s%s%sEANCOM%s%s\n"
			**UNB**,trenn_de**UNOA**,trenn_gd **"3"**,trenn_de
			,eigen_iln ,trenn_gd ** "14"**, trenn_de
			,dta_iln ,trenn_gd ** "14"**, trenn_de
			,idatum ,trenn_gd ,izeit ,trenn_de
			,dta_referenz , trenn_de 
			,trenn_de**DESADV**,trenn_de,trenn_de,trenn_de ** "EANCOM" **
			, testfeld ,trenn_se);

< ----- */
		// Versuch, D1.0B zu realisieren
// Beispiel :	UNB+UNOC:3+4013675000004:14+4300175000000:14+040701:0844+20270649+LIEFERANT NAME:AA+DESADV+++EANCOM+1'

// ACHTUNG : METRO schneidet bei "ohne test" direkt nach der dta_referenz mit trenn_se ab,
// ACHTUNG : METRO will auch bei testmodus keinen Eintrag "EANCOM", sondern dann nur leere felder


		sprintf ( dstring200, "UNB%sUNOC%s3%s%s%s14%s%s%s14%s%s%s%s%s%01.0d%s%sDESADV%s%s%sEANCOM%s%s\n"
			/*UNB*/,trenn_de/*UNOC*/,trenn_gd/*"3"*/,trenn_de
			,eigen_iln ,trenn_gd /* "14"*/, trenn_de
			,dta_iln ,trenn_gd /* "14"*/, trenn_de
			,idatum ,trenn_gd ,izeit ,trenn_de
			,dta_referenz , trenn_de 
			,trenn_de/*DESADV*/,trenn_de,trenn_de,trenn_de/* "EANCOM" */
			, testfeld ,trenn_se);
	}

}	// if 1==3 

    int k = fprintf ( fpouti ,dstring200 );
	if ( k > 0 ) return 0 ;
	return -1 ;

}     // proc UNB_segment

/** UNZ : Nutzdaten-Ende-Segment */

int CEDIPUT::UNZ_segment(/*int anz_nachricht*/)
{

	/* --->
    let dstring50 =
       "UNZ" #  trenn_de # clipped (PICTURE (anz_nachricht , "<-------&" ))
	  # trenn_de
      # clipped ( PICTURE ( dta_referenz , "<--------&" ))
      # trenn_se
< ------- */

		sprintf ( dstring50, "UNZ%s%01.0d%s%01.0d%s\n"
			,trenn_de,danz_nachrichten,trenn_de,dta_referenz,trenn_se);

        int k = fprintf ( fpouti ,dstring50 );
		if ( k > 0 ) return 0 ;
		return -1 ;

}     // proc UNZ_segment

/** UNH : Nachrichten-Kopfsegment */

int CEDIPUT::UNH_segment( int ref_nr, char * nach_typ )
{


	danz_seg = 1;    // immer erstes Element der Zaehlerei

//  Beispiel-String : UNH+ME000001+INVOIC:D:01B:UN:EAN007'

	if ( dta.typ == FENEBERG || dta.typ == KAUFLAND )
	{// 96A 
		sprintf ( dstring200 ,"UNH%s%01.0d%s%s%sD%s96A%sUN%sEAN005%s\n"
	   ,trenn_de, ref_nr,trenn_de,nach_typ,trenn_gd,trenn_gd
	   ,trenn_gd,trenn_gd,trenn_se);
	}
	else
	{
		sprintf ( dstring200 ,"UNH%s%01.0d%s%s%sD%s01B%sUN%sEAN007%s\n"
	   ,trenn_de, ref_nr,trenn_de,nach_typ,trenn_gd,trenn_gd
	   ,trenn_gd,trenn_gd,trenn_se);

	}
        int k = fprintf ( fpouti ,dstring200 );
		if ( k > 0 ) return 0 ;
		return -1 ;

}  // proc UNH_segment

/** UNT : Nachrichten-Ende-Segment */

int CEDIPUT::UNT_segment(int ref_nr)
{

    danz_seg ++ ;
	danz_nachrichten ++ ;
 /* ---->
      "UNT" #  trenn_de # clipped ( PICTURE ( danz_seg , "<--------&" ))
      #trenn_de # "ME" # clipped ( PICTURE  (  ref_nr , "<--------&" ))
      # trenn_se
< ------- */


	sprintf ( dstring50, "UNT%s%01.0d%s%01.0d%s\n"
		,trenn_de,danz_seg,trenn_de,ref_nr,trenn_se );

    int k = fprintf ( fpouti ,dstring50 );
	if ( k > 0 ) return 0 ;
	return -1 ;

}     // proc UNT_segment



/** UNT : Nachrichten-Ende-Segment */

int CEDIPUT::DTM_segment( char * ptyp,TIMESTAMP_STRUCT sdatum, int pformat, char * pzeit )
{

char formstring1[20] ;
char idatum[20] ;
sprintf ( idatum , "%02d.%02d.%04d" , sdatum.day , sdatum.month , sdatum.year ) ;  

       danz_seg ++ ;

// datum-input : "dd.mm.yyyy"
//    Ziel-format : yyyymmdd 
//     anderes is z.Z. nicht realisiert
	   if ( pformat == 102 || pformat == 203 )
	   {
           formstring1[0] = idatum[6] ;
           formstring1[1] = idatum[7] ;
           formstring1[2] = idatum[8] ;
           formstring1[3] = idatum[9] ;
           formstring1[4] = idatum[3] ;
		   formstring1[5] = idatum[4] ;
           formstring1[6] = idatum[0] ;
           formstring1[7] = idatum[1] ;
           formstring1[8] = '\0' ;
	   }
	   if ( pformat == 203 )
	   {
		   sprintf ( formstring1 + 8 , "%s" , clipped ( pzeit )) ;
	   }

/* ->
       let dstring50 = "DTM" # trenn_de # ptyp #trenn_gd
        # clipped ( formstring1 ) # trenn_gd # pformat # trenn_se
< ----- */
       sprintf ( dstring50, "DTM%s%s%s%s%s%d%s\n",trenn_de,ptyp
		,trenn_gd ,formstring1, trenn_gd, pformat ,trenn_se );

        int k = fprintf ( fpouti ,dstring50 );
		if ( k > 0 ) return 0 ;
		return -1 ;

}        // proc DTM_segment


// Beginn der Message

int CEDIPUT::BGM_segment( char * ctyp, long blg_nr, int org_kop )
{

    danz_seg ++ ;
	/* ----->
	let  dstring50 =
       "BGM" #  trenn_de # clipped ( ctyp )
//	  # trenn_gd #trenn_gd # trenn_gd               // hier koennte Zusatztext stehen
       # trenn_de # clipped ( PICTURE  (  blg_nr  , "<--------&" ))
       # trenn_de # clipped ( PICTURE  (  org_kop , "<--------&" ))
       # trenn_se
< ----- */

/* --->
	// Beispiel : 	BGM+325:::+87441+9' - ist aber komisch ?!

			sprintf (dstring50, "BGM%s%s:::X%s%01.0d%s%01.0d%s\n"
			,trenn_de ,ctyp ,trenn_de , blg_nr ,trenn_de ,org_kop ,trenn_se );
":::X" zun�chst mal fix  ?!
< ---- */

	if ( dta.typ == FENEBERG )
	{
	// Beispiel : 		BGM+351::9+1234567'

			sprintf (dstring50, "BGM%s%s%s%s%01.0d%s%01.0d%s\n"
			,trenn_de ,ctyp ,trenn_gd, trenn_gd , org_kop, trenn_de, blg_nr, trenn_se );
	}
	else
	{

		if ( dta.typ == KAUFLAND )
		{
			// Beispiel : 		BGM+351+1234567+9'

			sprintf (dstring50, "BGM%s%s%s%01.0d%s9%s\n"
			,trenn_de ,ctyp , trenn_de, blg_nr, trenn_de, trenn_se );
		}
		else
		{
			if ( org_kop > 0 )
			{
				sprintf (dstring50, "BGM%s%s:::X%s%01.0d%s%01.0d%s\n"
				,trenn_de ,ctyp ,trenn_de , blg_nr ,trenn_de ,org_kop ,trenn_se );
			}
			else
			{
				sprintf (dstring50, "BGM%s%s%s%01.0d%%s\n"
				,trenn_de ,ctyp ,trenn_de , blg_nr, trenn_se );
			}
		}
	}


/* ---->
		else	// so ist es bis heute :
		{

			sprintf (dstring50, "BGM%s%s%s%01.0d%s\n"
			,trenn_de ,ctyp ,trenn_de , blg_nr, trenn_se );
		}
< ------ */


		int k = fprintf ( fpouti ,dstring50 );
		if ( k > 0 ) return 0 ;
		return -1 ;

}   // proc BGM_segment



// Referenzsegment

int CEDIPUT::RFF_segment( char * ctyp, char * eblg_nr)
{

// IV : Rechnungsnummer des Verkaeufers(fit)
// VN : Auftragsnummer des Lieferanten(fit)
// ON : z.B. externe Bestellnummer
// DQ : eigner Lieferschein

char blg_nr[129] ;
sprintf ( blg_nr, "%s" , eblg_nr ) ;	// maskel organisieren !!!!!

    danz_seg ++ ;
	/* ---->
       "RFF" #  trenn_de # clipped ( ctyp )
       # trenn_gd # clipped ( blg_nr )
       # trenn_se
	   < ----- */

	char iblg_nr[90] ;
	sprintf ( iblg_nr, "%s", maskel(blg_nr )) ;	

	sprintf ( dstring50 , "RFF%s%s%s%s%s\n"
		,trenn_de ,ctyp ,trenn_gd ,iblg_nr ,trenn_se );

	int k = fprintf ( fpouti ,dstring50 );
	if ( k > 0 ) return 0 ;
	return -1 ;

}   // proc RFF_segment



int CEDIPUT::TDT_segment ( char * cinstring )
{

    danz_seg ++ ;
    sprintf ( dstring50 , "TDT%s%s%s", trenn_de,  cinstring, trenn_se ) ;

	int k = fprintf ( fpouti ,dstring50 );
	if ( k > 0 ) return 0 ;
	return -1 ;

}   // proc TDT_segment

int CEDIPUT::CTA_segment (char * ctyp, char *abtnr )
{

    danz_seg ++ ;
/* ---->
	"CTA" # trenn_de # clipped ( ctyp )
             # trenn_de # clipped ( abtnr )
             # trenn_se
< ----- */
	sprintf ( dstring50, "CTA%s%s%s%s%s\n",
		trenn_de, clipped(ctyp), trenn_de,clipped ( abtnr),trenn_se	);

	int k = fprintf ( fpouti ,dstring50 );
	if ( k > 0 ) return 0 ;
	return -1 ;

}   /* CTA_segment */

int CEDIPUT::NAD_segment( char * ctyp, char * diln, long dadr )
{
//141102 : max 35 Zeichen sind erlaubt, ausserdem maskel-sequenzen kappen

char adr_adr_nam1 [50];
char adr_str [50] ;
char adr_ort1[50] ;  
char adr_plz [50] ;  

    danz_seg ++ ;
	if ( dadr > 0L)
	{

		adr.adr = dadr ;
		int k = adr_class.openadr() ;
		k = adr_class.leseadr() ;
		if ( k )
		{
			sprintf ( adr_adr_nam1 , "ADRESS-NAME %d", dadr );
			dadr = 0 ;
			sprintf ( adr_str , "ADRESS-STRASSE " );
			sprintf ( adr_ort1 , "ADRESS-ORT " );
			sprintf ( adr_plz , "PLZX " );	// PLZ fehlte bisher
		}
		else
		{
	            sprintf ( adr_adr_nam1, "%s", maskel ( adr.adr_nam1 ));
				sprintf ( adr_str, "%s", maskel ( adr.str ));
				sprintf ( adr_ort1, "%s", maskel ( adr.ort1 ));
				sprintf ( adr_plz, "%s", maskel ( adr.plz ));
// string darf max. 35 Zeichen lang sein, wir kappen nach 34 und setzen 1 blank,
				// dadurch werden evtl. end-masken-Sequenzen entschaerft 


				adr_adr_nam1[32] = ' ' ;
				adr_str[32] = ' ' ;
				adr_ort1[32] = ' ' ;
				adr_plz[32] = ' ' ;

				adr_adr_nam1[33] = '\0' ;
				adr_str[33] = '\0' ;
				adr_ort1[33] = '\0' ;
				adr_plz[33] = '\0' ;
		}
	}
    if (! dadr)
	{
/* --->
		"NAD" #  trenn_de # clipped ( ctyp )
           # trenn_de # clipped ( diln )
           # trenn_gd
           # trenn_gd
           # "9"                        // ean
           # trenn_se
< --- */

	sprintf ( dstring200, "NAD%s%s%s%s%s%s9%s\n"
		    ,trenn_de ,ctyp ,trenn_de ,diln ,trenn_gd ,trenn_gd ,trenn_se);


	}
    else
	{
	    if ((strlen( clipped (diln))) > 1 )
		{
/* ---->
          let dstring150 =
           "NAD" #  trenn_de # clipped ( ctyp )
           # trenn_de
           # clipped ( diln )
           # trenn_de
           # trenn_de # clipped ( adr_adr_nam1 ) # " "
           # trenn_gd
           # trenn_gd
           # trenn_gd
           # trenn_gd
           # trenn_de # clipped ( adr_str ) # " "
           # trenn_gd
           # trenn_gd
           # trenn_gd
           # trenn_de # clipped ( adr_ort1 ) # " "
           # trenn_de
           # trenn_de # clipped ( adr.plz )
           # trenn_de # "DE"
           # trenn_se
< ----------------- */
// die folge-Trenngd = ":" sind doch Quatsch : raus damit am  150313 ,aber "::9" nach der iln requested
          sprintf ( dstring200 ,"NAD%s%s" ,trenn_de,ctyp );
		  sprintf ( dstring50, "%s%s%s%s9" ,trenn_de,diln,trenn_gd,trenn_gd );	// "::9" dazu am 150313
		  strcat ( dstring200, dstring50);	
		// 080108 : hier stand bisher "%s%s%s " 16.04.2013 : steht jetzt auch wieder !!!!!!!
 		  sprintf ( dstring50, "%s%s%s " ,trenn_de, trenn_de,adr_adr_nam1 );
		  strcat ( dstring200, dstring50);	

// 150313		  sprintf ( dstring50, "%s%s%s%s%s%s " ,
		  sprintf ( dstring50, "%s%s " ,
		  /* trenn_gd,trenn_gd,trenn_gd,trenn_gd,*/ trenn_de,adr_str );
		  strcat ( dstring200, dstring50);	

//	150313	  sprintf ( dstring50, "%s%s%s%s%s " ,
		  sprintf ( dstring50, "%s%s " ,
		  /* trenn_gd,trenn_gd,trenn_gd,*/ trenn_de,adr_ort1 );
		  strcat ( dstring200, dstring50);	

		  sprintf ( dstring50, "%s%s%s%s%DE%s\n" ,
		  trenn_de,trenn_de,adr_plz,trenn_de,trenn_se );
		  strcat ( dstring200, dstring50);	

		}
        else
		{
/* ----->
			let dstring150 =
           "NAD" #  trenn_de # clipped ( ctyp )
           # trenn_de
           # trenn_de
           # trenn_de # clipped ( adr_adr_nam1 ) # " "
           # trenn_gd
           # trenn_gd
           # trenn_gd
           # trenn_gd
           # trenn_de # clipped ( adr_str ) # " "
           # trenn_gd
           # trenn_gd
           # trenn_gd
           # trenn_de # clipped ( adr_ort1 ) # " "
           # trenn_de
           # trenn_de # clipped ( adr.plz )
           # trenn_de # "DE"
           # trenn_se
  < ------ */

          sprintf ( dstring200 ,"NAD%s%s" ,trenn_de,ctyp );
		  sprintf ( dstring50, "%s%s%s%s"
			  ,trenn_de,trenn_de,trenn_de,adr_adr_nam1 );
		  strcat ( dstring200, dstring50);	

// 150313		  sprintf ( dstring50, "%s%s%s%s%s%s " ,
		  sprintf ( dstring50, "%s%s " ,
		  /* trenn_gd,trenn_gd,trenn_gd,trenn_gd,*/ trenn_de,adr_str );
		  strcat ( dstring200, dstring50);	

//150313		  sprintf ( dstring50, "%s%s%s%s%s " ,
		  sprintf ( dstring50, "%s%s " ,
		  /* trenn_gd,trenn_gd,trenn_gd,*/ trenn_de,adr_ort1 );
		  strcat ( dstring200, dstring50);	


		  sprintf ( dstring50, "%s%s%s%s%DE%s\n" ,
		  trenn_de,trenn_de,adr_plz,trenn_de,trenn_se );
		  strcat ( dstring200, dstring50);	

		}
	}
	int k = fprintf ( fpouti ,dstring200 ) ;
	if ( k > 0 ) return 0 ;
	return -1 ;

}   // proc NAD_segment

int CEDIPUT::TAX_segment( char *taxwert)
{

    danz_seg ++ ;
    if ( taxwert[0] == 'E' )        //  Steuerfrei
	{
 /* ----->
			"TAX" #  trenn_de # "7"
           # trenn_de # "VAT"
           # trenn_de
           # trenn_de
           # trenn_de
           # trenn_de                   // hier koennte noch steuerfrei hin
           # clipped ( taxwert )
           # trenn_se
< ------- */

		sprintf ( dstring50 , "TAX%s7%sVAT%s%s%s%s%s%s\n"
			,trenn_de,trenn_de,trenn_de,trenn_de,trenn_de,
			trenn_de,taxwert,trenn_se);
	}
    else
	{
		/* ------->
           "TAX" #  trenn_de # "7"
           # trenn_de # "VAT"
           # trenn_de
           # trenn_de
           # trenn_de
           # trenn_gd  
		   # trenn_gd
		   # trenn_gd # clipped (taxwert )
           # trenn_se
		   < ----- */
		if ( eanversion == 1 )
		{	// dieses "S" bedeutet Einheitsteuersatz und  ist in der ger-version gefordert ??
			sprintf ( dstring50 , "TAX%s7%sVAT%s%s%s%s%s%s%s%s%sS\n"
			,trenn_de,trenn_de, trenn_de,trenn_de,trenn_de
			,trenn_gd ,trenn_gd ,trenn_gd ,taxwert ,trenn_de,trenn_se);
		}
		else	// EAN 96 A
		{
			sprintf ( dstring50 , "TAX%s7%sVAT%s%s%s%s%s%s%s%s\n"
			,trenn_de,trenn_de, trenn_de,trenn_de,trenn_de
			,trenn_gd ,trenn_gd ,trenn_gd ,taxwert ,trenn_se);
		}
	}
	int k = fprintf ( fpouti ,dstring50 );
	if ( k > 0 ) return 0 ;
	return -1 ;

}   // proc TAX_segment

// Verkettungssegment  : inum1 ist fortaufend in der Lieferung , basnum referenziert auf uebergeordnetes Element ; inum=1 & basnum=0 entspr. Gesamtlieferung
int CEDIPUT::CPS_segment( long inum1 , long basnum )
{
	danz_seg ++ ;

	if ( basnum == 0 )
	{
		sprintf ( dstring50 , "CPS%s%d%s\n" , trenn_de , inum1 , trenn_se ) ;
	}
	else
	{	
//		sprintf (  dstring50 , "CPS%s%d%s%s%d%s\n" , trenn_de , inum1 , trenn_de , trenn_de , basnum , trenn_se ) ; Schrott
		sprintf (  dstring50 , "CPS%s%d%s%d%s\n" , trenn_de , inum1 , trenn_de , basnum , trenn_se ) ;
	}
	int k = fprintf ( fpouti ,dstring50 );
	if ( k > 0 ) return 0 ;
	return -1 ;
}   // proc CPS_segment

int CEDIPUT::PCI_segment  ( char * cinput) 
{
	danz_seg ++ ;

    sprintf ( dstring50 , "PCI%s%s%s\n" , trenn_de , cinput , trenn_se ) ;	// als input bitte immer bereits geclippede strings nutzen

	int k = fprintf ( fpouti ,dstring50 );
	if ( k > 0 ) return 0 ;
	return -1 ;
}   // proc PCI_segment

// Waehrungs-Infos

int CEDIPUT::CUX_segment(void)
{
char  dwaehr [5] ;

// wir nutzen mal simpel-release mit "einfach nur Rechnungswaehrung"
// Prinz. Voraussetzung : Nur eine Waehrung je RELISU, sonst
// organisieren wir uns nur ein Handlings-Problem

    sprintf (dwaehr, "EUR");

/* ---->
    if ( dwaehr_schalter == 1)
	{
         sprintf  dwaehr = "DEM"
	}
< ----- */

    danz_seg ++ ;
/* ---->
       let dstring50 =
           "CUX"
           # trenn_de # "2"
           # trenn_gd # clipped (  dwaehr )
           # trenn_gd # "4"
           # trenn_se
< ----- */

	sprintf ( dstring50, "CUX%s2%s%s%s4%s\n"
			,trenn_de, trenn_gd,dwaehr,trenn_gd,trenn_se );

// dies waere eine Alternative mit euro und dem
//           # trenn_de # "2"
//           # trenn_gd # "EUR"    -> referenzwaehrung
//           # trenn_gd # "4"
//           # trenn_gd
//           # trenn_de # "3"
//           # trenn_gd # "DEM"    // zielwaehrung
//           # trenn_gd # "4"
//           # trenn_gd
//           # trenn_de
//           # trenn_se

	int k = fprintf ( fpouti ,dstring50 );
	if ( k > 0 ) return 0 ;
	return -1 ;

}   // proc CUX_segment

// Positions-Start

int CEDIPUT::LIN_segment( long ibel_pos, char *iart_ean )
{
	// Achtung : plausi-Check und Generierung der ean muss bereits passiert sein !!!!

	danz_seg ++ ;


 	/* 
       let dstring50 =
           "LIN" # trenn_de
           # clipped ( PICTURE ( ibel_pos , "<-------&" ))
           # trenn_de
           # trenn_de
           # clipped ( iart_ean )
           # trenn_gd # "EN"            // fuer EAN

           # trenn_se
< ----------- */

	// Neuerdings heisst der Verein "SRV" und nicht mehr "EN"


	if ( (strlen ( iart_ean )) > 3 )
	{
		sprintf ( dstring50 , "LIN%s%01.0d%s%s%s%sSRV%s\n"
		,trenn_de ,ibel_pos ,trenn_de ,trenn_de ,iart_ean ,trenn_gd ,trenn_se);
	}
	else
	{
		sprintf ( dstring50 , "LIN%s%01.0d%s\n"
		,trenn_de ,ibel_pos ,trenn_se);
	}

	int k = fprintf ( fpouti ,dstring50 );
	if ( k > 0 ) return 0 ;
	return -1 ;

}   // proc LIN_segment

// Positions-Ergaenzung

int CEDIPUT::PIA_segment ( double interna, char * externa, char * ityp )
{					// fit-Nummer, lief_bzg
char bpinfeld[4] ;

// 130411 : long-> double

// folgende Logik : 1. Segment +1+ falls EAN vorhanden und fakultativ
//                  1. Segment +5+ falls keine EAN und dann Pflichtfeld      
// +LIEF-NR:SA::91+ WA : fit-Nummer ; WE  : lief_bzg 
// +BYER-NR:IN::92+ WA : a_kun.a_kun ; WE : fit-Nummer
// +BYER-NR:BP::92+ WA : a_kun.a_kun ; WE : fit-Nummer
    if ((strlen (ityp )) > 0)
        sprintf ( bpinfeld ,"%s" ,ityp) ;
    else
        sprintf ( bpinfeld, "BP" ) ;
//    let bpinfeld = "IN"

    danz_seg ++ ;

// bis 160613 : hier war wohl bis heute verdreht ( wegen WA/WE )
// ab 160613  fuer WA : SA/91 -> fit-Artiklenummer ( interna )
// ab 160613  fuer WA : IN/92 -> a_kun-Artiklenummer ( externa )


    if (( (strlen ( externa)) > 0 ) && ( interna > 0 ))
	{	// sowohl interne als auch externe Nummer vorhanden

/* ----> bis 160613 
      sprintf ( dstring80 , "PIA%s1%s%s%sSA%s%s91%s%1.0f%s%s%s%s92%s\n" 
 			, trenn_de ** "1" ** ,trenn_de ,clipped (externa), trenn_gd **"SA"**
           ,trenn_gd ,trenn_gd ** "91" **,trenn_de ,interna ,trenn_gd ,bpinfeld, trenn_gd, trenn_gd **"92"**, trenn_se ) ;
< ----- */

      sprintf ( dstring80 , "PIA%s1%s%1.0f%sSA%s%s91%s%s%s%s%s%s92%s\n" 
 			, trenn_de /* "1" */ ,trenn_de , interna , trenn_gd /*"SA"*/
           ,trenn_gd ,trenn_gd /* "91" */ ,trenn_de ,  clipped (externa) ,trenn_gd ,bpinfeld, trenn_gd, trenn_gd /*"92"*/, trenn_se ) ;



/* ----> bis 160613
		  "PIA" # trenn_de # "1"
           # trenn_de # clipped (externa)       // Lieferanten-Art-Nr. : ab 160613 : interna
           # trenn_gd # "SA"
           # trenn_gd
           # trenn_gd # "91"
           # trenn_de # clipped ( PICTURE ( interna , "<------------&" )) ab 160613  clipped(externa)
           # trenn_gd # bpinfeld
           # trenn_gd
           # trenn_gd # "92"
           # trenn_se
< ------ */
	}
    else
	{

// Hier ab 160613 : ebenfalls Logik von interna und externa vertauschen
		if ( interna > 0 )
		{	// nur interne Nummer vothanden
				sprintf ( dstring80 , "PIA%s1%s%1.0f%sSA%s%s91%s\n" 
		      /*"PIA"*/ ,trenn_de /*"1"*/,trenn_de ,interna , trenn_gd /*"SA"*/, trenn_gd, trenn_gd /*"91"*/, trenn_se);

// bis 160613			sprintf ( dstring80 ,"PIA%s1%s%1.0f%s%s%s%s92%s\n" 
// bis 160613           /*"PIA"*/ , trenn_de /* "1"*/ ,trenn_de , interna , trenn_gd , bpinfeld , trenn_gd, trenn_gd /*"92"*/, trenn_se ) ;

 /* ---->
         let dstring80 =
           "PIA" # trenn_de # "1"
           # trenn_de # clipped ( PICTURE ( interna , "<------------&" ))
           # trenn_gd # bpinfeld
           # trenn_gd
           # trenn_gd # "92"
           # trenn_se
< ----- */
		}
		else        // nur Lieferanten-Nummer vorhanden ( unwahrsch. Fall ?? )
		{


			sprintf ( dstring80 ,"PIA%s1%s%s%s%s%s%s92%s\n" 
          /*"PIA"*/ , trenn_de /* "1"*/ ,trenn_de , clipped ( externa) , trenn_gd , bpinfeld , trenn_gd, trenn_gd /*"92"*/, trenn_se ) ;
			
// bis 160613			sprintf ( dstring80 , "PIA%s1%s%s%sSA%s%s91%s\n" 
// bis 160613          /*"PIA"*/ ,trenn_de /*"1"*/,trenn_de ,clipped (externa), trenn_gd /*"SA"*/, trenn_gd, trenn_gd /*"91"*/, trenn_se);

/* ---->
			let dstring80 =
           "PIA" # trenn_de # "1"
           # trenn_de # clipped (externa)       // Lieferanten-Art-Nr.
           # trenn_gd # "SA"
           # trenn_gd
           # trenn_gd # "91"
           # trenn_se
< ----- */
		}
	}

// 010612 : Achtung : Rheinstetten : Nummern nur einzeln �bergeben !!!!!
// PIA+1+2059541:SA::91'   - fit- Artikel-Nummer
//	PIA+1+000000:IN::92'   - edeka-Artikelnummer
//	PIA+1+00000:NB::91'    - Chargen-Nummer des produktes - noch nicht realisiert ( z.B. : "WSC"

	if ( bpinfeld[0] == 'R' && bpinfeld[1] == 'S' )	// Rheinstetten-Anforderung
	{
		if ( interna > 0 )
		{	// nur interne Nummer ausgeben
			sprintf ( bpinfeld, "SA" ) ;
			sprintf ( dstring80 ,"PIA%s1%s%1.0f%s%s%s%s91%s\n" 
           /*"PIA"*/ , trenn_de /* "1"*/ ,trenn_de , interna , trenn_gd , bpinfeld , trenn_gd, trenn_gd /*"91"*/, trenn_se ) ;
		}
		else
		{
			if ( strlen ( externa ) > 0 )
			{	// nur externe Nummer ausgeben
				sprintf ( bpinfeld, "IN" ) ;
				sprintf ( dstring80 ,"PIA%s1%s%s%s%s%s%s92%s\n" 
			/*"PIA"*/ , trenn_de /* "1"*/ ,trenn_de , externa , trenn_gd , bpinfeld , trenn_gd, trenn_gd /*"92"*/, trenn_se ) ;
			}
		}
	}

	int k = fprintf ( fpouti ,dstring80 );
	if ( k > 0 ) return 0 ;
	return -1 ;
}   // PIA_segment

int CEDIPUT::CNT_segment (  char * ityp , double iwert , int ink  )
{
// Abstimmsumme 

// ityp : 2		=> Anzahl der Positions-Zeilen( aus bel_pos )
// ityp : 7		=> Gesamtbruttogewicht ==> entweder kumulieren oder von oben merken
// ityp : 11	=> Gesamtzahl Packst�cke ==> Summe der PAC-Zahlen ?!

// Beispiel : CNT+2:3'

	char cintern [33] ;
	char maske[20] ;

	sprintf (maske, "%%01.%01df",ink) ;
	sprintf (cintern, maske,iwert ) ;


	danz_seg ++ ;

	sprintf ( dstring50 ,"CNT%s%s%s%s%s\n"
           /*"CNT"*/ ,trenn_de ,ityp ,trenn_gd, cintern , trenn_se ) ;	//ityp MUSS geklipped sein 

	int k = fprintf ( fpouti ,dstring50 );
	if ( k > 0 ) return 0 ;
	return -1 ;
}   // proc CNT_segment


int CEDIPUT::PAC_segment ( int icont, char * zuIDENT1, char * zuIDENT2, char * iTYP, char * iCODELISTE )

// PAC
// +
// Anzahl
// +
//// :52   oder :50		// zuIDENT1	Art der Ident. NVE(52) oder EAN(50)
//// :4E   oder :5E		// zuIDENT2	Chep(gemietet) oder tauschbar(EURO)
// +iTYP
// ::9		// iCODELISTE
//	'

{
	// die input-strings muessen leer oder geclipped sein !!!
	// zuident und Codeliste bringen eigne trenn_gd mit, sonst klappt es nicht
	// Aufbau : "PAC+icont+zuident1zuident2+iTYPiCODELISTE'"
	
	sprintf ( dstring50 ,"PAC%s%d%s%s%s%s%s%s%s\n"
           /*"PAC"*/ ,trenn_de ,icont ,zuIDENT1, zuIDENT2, trenn_de, trenn_de, iTYP , iCODELISTE ,trenn_se ) ;

			
	int k = fprintf ( fpouti ,dstring50 );
	if ( k > 0 ) return 0 ;
	return -1 ;
}   // proc PAC_segment



// PAC	- physikalische Dimensionen
// +	- n..8	- Anzahl
// +	- Art der Identifikation ( z.B. 50 = EAN8/EAN13 ; 52 : NVE siehe Liste oder auch mal nix )
//		:52   oder :50
// +	- Art der Verpackung : "201" ISO-Palette , "CT" = Karton-Verpackung
//		::9  : falls ean-Identifikation (z.B.201)
// +    - Packagae Type Identifikation ....
// +	- retournable -Verpackung -> siehe PAC_2_segment ??


// 010612 A : jetzt soll es lt. Weilstetten wohl immer so aussehen :
//	PAC+19+:52+PX-H1::92' -> Palette
// oder
// PAC+1+:52+DL-W-E2::92' -> Performance-Kiste


int CEDIPUT::IMD_segment ( char * ityp, char * icode, char * freitext )
{

// A : Freier Langtext
// B : Code + Text
// C : Code
// F : freier text

// ACHTUNG    field freitext char ( 34 )   // max 35 erlaubt

	danz_seg ++ ;

	freitext = maskel ( freitext ) ;
	if (strlen ( clipped(freitext)) > 34 )
	{
		freitext[34] = ' ' ;
		freitext[35] = '\0' ;
	}

    if ( ityp[0] == 'A' || ityp[0] == 'F' )
	{
		/* --->
       let dstring80 =
           "IMD" # trenn_de # clipped ( ityp)
           # trenn_de
           # trenn_de
           # trenn_gd
           # trenn_gd
           # trenn_gd # clipped ( freitext ) # " " 
           # trenn_se
		   < ---- */
			// falls maskel-sequenz durch die Laenge gekappt wird
			// d.h.:  "?" als letztes Zeichen, rettet uns das folge-blank

		   sprintf ( dstring200 ,"IMD%s%s%s%s%s%s%s%s %s\n"
           ,trenn_de ,ityp ,trenn_de ,trenn_de ,trenn_gd ,trenn_gd
		   ,trenn_gd ,clipped (freitext) ,trenn_se );
	}

    if ( ityp[0]  == 'B' )
	{	
/* --->
       let dstring80 =
           "IMD" # trenn_de # clipped ( ityp)
           # trenn_de
           # clipped ( icode )
           # trenn_de
           # trenn_gd
           # trenn_gd
           # trenn_gd # clipped ( freitext ) # " "  
           # trenn_se
< -------*/

		   sprintf ( dstring200 ,"IMD%s%s%s%s%s%s%s%s %s\n"
           ,trenn_de ,ityp ,trenn_de ,icode,trenn_de ,trenn_gd ,trenn_gd
		   ,trenn_gd ,clipped (freitext) ,trenn_se );
	}
	if ( ityp[0] == 'C')
	{
		/* ----->
       let dstring150 =
           "IMD" # trenn_de # clipped ( ityp)
           # trenn_de
           # trenn_de # clipped ( icode ) 
           # trenn_se
< ----------- */
       sprintf ( dstring200, "IMD%s%s%s%s%s%s\n"
		,trenn_de ,ityp ,trenn_de ,trenn_de ,icode ,trenn_se );
	}

	int k = fprintf ( fpouti ,dstring200 );
	if ( k > 0 ) return 0 ;
	return -1 ;
}   // proc IMD_segment

int CEDIPUT::QTY_segment( char * ityp, double menge, short ime_einh ,int obaktiv )
{
 char mengenbez[20] ; 
 char fakmenge [20] ;

    danz_seg ++ ; 

    if ( ime_einh == 0) ime_einh = 2 ;

    if (ime_einh == 2 )	// Kg
	{
        sprintf (  fakmenge, "%01.3f",menge );
        sprintf (mengenbez , "KGM");
	}
    else
	{
		if ( ime_einh == 1 )            // Stueck
		{
			sprintf ( fakmenge, "%01.0f", menge);
			sprintf( mengenbez, "PCE" );
		}
		else
		{// Rest als Stueck ??? !!!
			sprintf (fakmenge,  "%01.0f", menge );
			sprintf ( mengenbez, "PCE");
		}
	}
//    ityp = 21 bestellte Menge
//    ityp = 12 gelieferte Menge
//    ityp = 47 berechnete Menge

/* ---->
	"QTY" # trenn_de # clipped ( ityp)
           # trenn_gd # clipped ( fakmenge )
           # trenn_gd # clipped ( mengenbez )
           # trenn_se
  < ---- */


	if ( obaktiv || ( ityp[0]== '4' && ityp[1] == '7' ) )
	{
		sprintf ( dstring50 , "QTY%s%s%s%s%s%s%s\n"
		,trenn_de ,ityp ,trenn_gd ,fakmenge ,trenn_gd ,mengenbez ,trenn_se);

	}
	else
	{
		sprintf ( dstring50 , "QTY%s%s%s%s%s\n"
		,trenn_de ,ityp ,trenn_gd ,fakmenge , trenn_se);
	}

	int k = fprintf ( fpouti ,dstring50 );
	if ( k > 0 ) return 0 ;
	return -1 ;
}   // proc QTY_segment

int CEDIPUT::MOA_segment( char * ityp, double iwert )
{

	danz_seg ++ ;
/* ---->
       let dstring50 =
           "MOA" # trenn_de # clipped ( ityp)
           # trenn_gd # clipped (PICTURE ( iwert , "<------&.&&" ))
           # trenn_se
< ------ */
       sprintf ( dstring50 ,"MOA%s%s%s%01.2f%s\n"
		,trenn_de ,ityp ,trenn_gd ,iwert ,trenn_se);
	int k = fprintf ( fpouti ,dstring50 );
	if ( k > 0 ) return 0 ;
	return -1 ;

}   // proc MOA_segment

int CEDIPUT::PCD_segment( char * ityp, double iwert )
{

    danz_seg ++ ;
	/* --->
           "PCD" # trenn_de # clipped ( ityp)
           # trenn_gd # clipped (PICTURE ( iwert , "<------&.&&" ))
           # trenn_se
		   < ---- */
       sprintf ( dstring50 ,"PCD%s%s%s%01.2f%s\n"
		,trenn_de ,ityp ,trenn_gd ,iwert ,trenn_se);
	
	int k = fprintf ( fpouti ,dstring50 );
	if ( k > 0 ) return 0 ;
	return -1 ;

}     // proc PCD_segment

int CEDIPUT::ALC_segment( char * abzu, char * absprache, char *kalkstufe, char *rabart)
{

    danz_seg ++;
/* ---->
           "ALC" # trenn_de # clipped ( abzu)
           # trenn_de # clipped ( absprache )
           # trenn_de
           # trenn_de # clipped ( kalkstufe )
           # trenn_de # clipped ( rabart )
           # trenn_se
< ------ */
       sprintf ( dstring50 ,"ALC%s%s%s%s%s%s%s%s%s%s\n"
			,trenn_de ,abzu
			,trenn_de ,absprache 
			,trenn_de
			,trenn_de ,kalkstufe
			,trenn_de ,rabart
			,trenn_se
		   ) ;

	int k = fprintf ( fpouti ,dstring50 );
	if ( k > 0 ) return 0 ;
	return -1 ;

}      // proc ALC_segment

int CEDIPUT::PRI_segment ( char * ityp, double iwert, short ime_einh)
{

  char mengenbez[16];

    danz_seg ++ ;

    if (ime_einh == 0) ime_einh = 2;
    if (ime_einh == 2)	// Kg
	{
        sprintf ( mengenbez, "KGM");
	}
    else
	{
		if ( ime_einh == 1 )             // Stueck
		{
			sprintf ( mengenbez ,"PCE");
		}
		else                            // Rest als Stueck ??? !!!
		{
			sprintf ( mengenbez ,"PCE");
		}
	}

	if ( ime_einh == -1 )
	{
        if ( dnachkpreis == 3 )
		{
           sprintf ( dstring50 , "PRI%s%s%s%01.3f%s" 
		   /*"PRI"*/ ,trenn_de , ityp, trenn_gd ,iwert ,trenn_se ) ;
		}
        else
		{
			if ( dnachkpreis == 4 )
			{
				sprintf ( dstring50 , "PRI%s%s%s%01.4f%s" 
				/*"PRI"*/ ,trenn_de , ityp, trenn_gd ,iwert ,trenn_se ) ;
			}
			else	// default 
			{
				sprintf ( dstring50 , "PRI%s%s%s%01.2f%s" 
				/*"PRI"*/ ,trenn_de , ityp, trenn_gd ,iwert ,trenn_se ) ;
			}
		}
  	}
	else
	{

		// so passt es fuer AAA


		if ( dnachkpreis == 3 )
		{
			if ( dwiechmann_par && (dta.typ == METRO) )
			{

/* ----->
               "PRI" # trenn_de # clipped ( ityp)
               # trenn_gd # clipped (PICTURE ( iwert , "<----&.&&&" ))
               # trenn_gd
               # trenn_gd # "GRP"
               # trenn_gd # "1"
               # trenn_gd # clipped ( mengenbez )

               # trenn_se
 < ----- */

				sprintf ( dstring50, "PRI%s%s%s%01.3f%s%sGRP%s1%s%s%s\n"
				,trenn_de ,ityp ,trenn_gd ,iwert ,trenn_gd, trenn_gd /* "GRP" */
				,trenn_gd /* "1" */
				,trenn_gd ,mengenbez ,trenn_se );

			}

			else       // default
			{
/* --->
			"PRI" # trenn_de # clipped ( ityp)
               # trenn_gd # clipped (PICTURE ( iwert , "<----&.&&&" ))
               # trenn_gd
               # trenn_gd
               # trenn_gd # "1"
               # trenn_gd # clipped ( mengenbez )
               # trenn_se
 < ----- */
				sprintf ( dstring50, "PRI%s%s%s%01.3f%s%s%s1%s%s%s\n"
				,trenn_de ,ityp ,trenn_gd ,iwert ,trenn_gd, trenn_gd 
				,trenn_gd /* "1" */
				,trenn_gd ,mengenbez ,trenn_se );
			}
		}
		else
		{
			if ( dnachkpreis == 4 )
			{	
				if ( dwiechmann_par && ( dta.typ == METRO ))
				{
				sprintf ( dstring50, "PRI%s%s%s%01.4f%s%sGRP%s1%s%s%s\n"
				,trenn_de ,ityp ,trenn_gd ,iwert ,trenn_gd, trenn_gd /* "GRP" */
				,trenn_gd /* "1" */
				,trenn_gd ,mengenbez ,trenn_se );
				}
				else       // default
				{
					sprintf ( dstring50, "PRI%s%s%s%01.4f%s%s%s1%s%s%s\n"
					,trenn_de ,ityp ,trenn_gd ,iwert ,trenn_gd, trenn_gd 
					,trenn_gd /* "1" */
					,trenn_gd ,mengenbez ,trenn_se );
				}
			}
			else		// dnachkpreis == 2 oder undefiniert
			{
				if ( dwiechmann_par && ( dta.typ == METRO ))
				{

// laut Schreiben vom 191001 : Gerr Kaya
// was "GRP" heisst weiss ich vorerst nicht,
//  "SRP" wuerde empf. Einzelhandelspreis heissen
					sprintf ( dstring50, "PRI%s%s%s%01.2f%s%sGRP%s1%s%s%s\n"
					,trenn_de ,ityp ,trenn_gd ,iwert ,trenn_gd, trenn_gd /* "GRP" */
					,trenn_gd /* "1" */
					,trenn_gd ,mengenbez ,trenn_se );
				}
				else       // default
				{
					sprintf ( dstring50, "PRI%s%s%s%01.2f%s%s%s1%s%s%s\n"
					,trenn_de ,ityp ,trenn_gd ,iwert ,trenn_gd, trenn_gd 
					,trenn_gd /* "1" */
					,trenn_gd ,mengenbez ,trenn_se );
				}
			}
		}
	}
	int k = fprintf ( fpouti ,dstring50 );
	if ( k > 0 ) return 0 ;
	return -1 ;

}   // proc PRI_segment

int CEDIPUT::LOC_segment( char * iloc )
{

// ab 060511 : allgemeine Realisierung

// Beispiel : LOC+7+2::92'    // Ladezone "2"

	danz_seg ++ ;

   sprintf ( dstring50 ,"LOC%s7%s%s%s%s92%s\n"
		,trenn_de	// 7 
		,trenn_de 
        ,iloc
        ,trenn_gd
        ,trenn_gd
		,trenn_se
		   );
	int k = fprintf ( fpouti ,dstring50 );
 	if ( k > 0 ) return 0 ;
	return -1 ;

}   // proc LOC_segment

int CEDIPUT::MEA_segment( char * ityp, double iwert, int ink, char * ieinh )
{

// ab 050511 : allgemeine Realisierung, Erweiterung der Parameter

// AAA = Netto-Gewicht
// AAB = Brutto-Gewicht
// AAD = Lieferungsbrutto
// LAY = Layer ( dann immer PCE dabei ..... )

// 050511 : Beispiel EDEKA	MEA+PD+AAB+KGM:631.235'
// PD : physische Abmessung (bestelltes Produkt)
// Einheit : KGM , GRM, PA(Paeckchen), PCE (Stueck) 
// passt vorerst genau fuer Frischware - bis 050511 
    danz_seg ++ ;
	/* ---->
    let dstring50 =
           "MEA" # trenn_de
           # trenn_de # "PD"
           # trenn_de # clipped ( ityp)
           # trenn_gd
           # trenn_gd
           # trenn_gd
           # trenn_de # "GRM"
           # trenn_gd # clipped (PICTURE ( (iwert * 1000) , "<--------&"))
           # trenn_se
< ------ */
/* -----> bis 050511

   sprintf ( dstring50 ,"MEA%s%sPD%s%s%s%s%s%sGRM%s%01.0f%s\n"
		,trenn_de ,trenn_de ** "PD" **
        ,trenn_de ,ityp
           ,trenn_gd
           ,trenn_gd
           ,trenn_gd
           ,trenn_de ** "GRM" **
           ,trenn_gd ,iwert * 1000
           ,trenn_se
		   );
< ----- */
/* ---->
// Das waere eine alternative Verwendung ....
    field cinternwert char(15)
    let danz_seg = danz_seg + 1

    if ( ityp = "AAD" ) // Fuer Bruttogewicht aller Palletten
        let cinternwert = PICTURE ( ROUND ( iwert AT 0), "<-----&" )   
        let dstring50 =
           "MEA" # trenn_de # "PD"
           # trenn_de # clipped ( ityp)
           # trenn_de # "KGM"
           # trenn_gd # clipped (cinternwert)
           # trenn_se

    else
    end
< ---- */

// A ab 050511 


// 050511 : Beispiel EDEKA	MEA+PD+AAB+KGM:631.235'

	char cintern [33] ;
	char maske[20] ;

	sprintf (maske, "%%01.%01df",ink) ;
	sprintf (cintern, maske,iwert ) ;

//   sprintf ( dstring50 ,"MEA%sPD%s%s%s%s%s%s%s%s%s%s\n"
   sprintf ( dstring50 ,"MEA%sPD%s%s%s%s%s%s%s\n"
		,trenn_de  /* "PD" */
        ,trenn_de ,ityp
//           ,trenn_gd	die drei ":" sind ueberfluessig ......
//           ,trenn_gd
//           ,trenn_gd
           ,trenn_de , ieinh /* "GRM" ..... */
           ,trenn_gd ,cintern /*iwert * 1000 */
           ,trenn_se
		   );
	
// E ab 050511

	int k = fprintf ( fpouti ,dstring50 );
 	if ( k > 0 ) return 0 ;
	return -1 ;

}   // proc MEA_segment


int CEDIPUT::GIN_segment( char * ityp , char * iwert1, char * iwert2, char * iwert3, char * iwert4, char * iwert5,
										char * iwert6, char * iwert7, char * iwert8, char * iwert9, char * iwert10)
{
// field ityp char ( 5 )   field iwert1 char ( 20 )   field iwert2 char ( 20 )   field iwert3 char ( 20 )
// field iwert4 char ( 20 )   field iwert5 char ( 20 )   field iwert6 char ( 20 )   field iwert7 char ( 20 )
// field iwert8 char ( 20 )   field iwert9 char ( 20 )   field iwert10 char (20 )

// lt. Spezi 96 A max. 5 derartige Segmente vorgesehen
//    init dstring200

// frueher  gab es mal "SS" als Typ, jetzt wird ueberall "BJ" genutzt
// 5-malige Wiederholung ist nach wie vor erlaubt, mache ich aber wahrsch. nie mehr
// 130411 : nur noch 2 Typen unterstuetzt ( "BJ" und "BX"


	char iityp [10] ;
	sprintf ( iityp, "%s", ityp ) ;

	if (ityp[0] == 'B' && ityp[1] == 'J' && ityp[2] == '\0')
	{
	// Falls Typ ="BJ", dann vorher immer noch ein PCI-33E oder PCI-41G schicken und anschliessend die NVE / GRAI selber
		if ( strlen ( iwert1 ) > 20 )
		{
			sprintf (iityp , "RAG" ) ;
			int k = PCI_segment("41G") ;
		}
		else
		{
			int k = PCI_segment("33E") ;
		}

		danz_seg ++ ;
		sprintf ( dstring200 , "GIN%s%s" , trenn_de, iityp ) ;	// beachte : ityp sollte clipped sein ....

		if ( iwert1[0] != '\0' && iwert1[1] != '\0' )     // erstes Segment wertvoll
		{
			dstring50[0] = '\0' ;
			if ( iwert2[0] != '0'&& iwert2[1] != '\0' )
				sprintf (  dstring50 ,"%s%s%s%s" , trenn_de, iwert1, trenn_gd,  iwert2 ) ;	// iwerte bitte clipped !!
			else
				sprintf ( dstring50 ,"%s%s", trenn_de, iwert1 ) ;
			sprintf ( dstring200 + (int) strlen( dstring200), "%s", dstring50 ) ;
		}

		if ( iwert3[0] != '\0' && iwert3[1] != '\0' )     // zweites Segment wertvoll
		{
			dstring50[0] = '\0' ;
			if ( iwert4[0] != '0'&& iwert4[1] != '\0' )
				sprintf (  dstring50 ,"%s%s%s%s" , trenn_de, iwert3, trenn_gd,  iwert4 ) ;	// iwerte bitte clipped !!
			else
				sprintf ( dstring50 ,"%s%s", trenn_de, iwert3 ) ;
			sprintf ( dstring200 + (int) strlen( dstring200), "%s", dstring50 ) ;
		}
			
		if ( iwert5[0] != '\0' && iwert5[1] != '\0' )     // drittes Segment wertvoll
		{
			dstring50[0] = '\0' ;
			if ( iwert6[0] != '0'&& iwert6[1] != '\0' )
				sprintf (  dstring50 ,"%s%s%s%s" , trenn_de, iwert5, trenn_gd,  iwert6 ) ;	// iwerte bitte clipped !!
			else
				sprintf ( dstring50 ,"%s%s", trenn_de, iwert5 ) ;
			sprintf ( dstring200 + (int) strlen( dstring200), "%s", dstring50 ) ;
		}

		if ( iwert7[0] != '\0' && iwert7[1] != '\0' )     // viertes Segment wertvoll
		{
			dstring50[0] = '\0' ;
			if ( iwert8[0] != '0'&& iwert8[1] != '\0' )
				sprintf (  dstring50 ,"%s%s%s%s" , trenn_de, iwert7, trenn_gd,  iwert8 ) ;	// iwerte bitte clipped !!
			else
				sprintf ( dstring50 ,"%s%s", trenn_de, iwert7 ) ;
			sprintf ( dstring200 + (int) strlen( dstring200), "%s", dstring50 ) ;
		}

		if ( iwert9[0] != '\0' && iwert9[1] != '\0' )     // fuenftes Segment wertvoll
		{
			dstring50[0] = '\0' ;
			if ( iwert10[0] != '0'&& iwert10[1] != '\0' )
				sprintf (  dstring50 ,"%s%s%s%s" , trenn_de, iwert9, trenn_gd,  iwert10 ) ;	// iwerte bitte clipped !!
			else
				sprintf ( dstring50 ,"%s%s", trenn_de, iwert9 ) ;
			sprintf ( dstring200 + (int) strlen( dstring200), "%s", dstring50 ) ;
		}

		sprintf ( dstring200 + (int) strlen( dstring200), "%s\n", trenn_se ) ;
	}

	if (ityp[0] == 'B' && ityp[1] == 'X' && ityp[2] == '\0')
	{
	// Falls Typ ="BX", dann kommt ne Losnummer

		danz_seg ++ ;
		sprintf ( dstring200 , "GIN%s%s%s%s%s\n" , trenn_de, ityp , trenn_de, maskel(iwert1), trenn_se ) ;	// beachte : alles  sollte clipped sein ....
	}
 
	int k = fprintf ( fpouti ,dstring200 );
 	if ( k > 0 ) return 0 ;
	return -1 ;
	
}  // proc GIN_segment

/* ---->
PROCEDURE FTX_segment
PARAMETER
  field textzuordnung char(3) // ZZZ Kopf oder SUR Fuss, AAK fuer edeka 
  field verarb_hinw char(3)     // ist wohl fast immer = 1 
  field schluessel char ( 10 )
  field text1 char(70)
  field text2 char(70)
  field text3 char(70)
  field text4 char(70)
  field text5 char(70)
  field sprache char(3)
END

   let danz_seg = danz_seg + 1

   init dstring200      // bissel sparen, im Original sollte hier
                        // dstring400  hinein .... 

   if ( strlen ( schluessel ) > 1 )

        if ( schluessel = "pre" )
                // das ist der interne Schluessel fuer dresden

            let dstring200 = "FTX"
              # trenn_de # textzuordnung
              # trenn_de # "1"
              # trenn_de # trenn_gd # trenn_gd # "9"
              # trenn_de # clipped ( text1 ) # trenn_se

        else    // Standard-Ablauf mit Standard-texten

            let dstring200 = "FTX" # trenn_de # textzuordnung #trenn_de
                # "1" # trenn_de # clipped ( schluessel ) # trenn_se

        end
   else
       let dstring200 = "FTX" # trenn_de # textzuordnung #trenn_de
         #trenn_de
         #trenn_de
         #clipped(text1)
       if strlen(text2) > 0
          let dstring200 = clipped(dstring200)#trenn_gd#clipped(text2)
       end
       if strlen(text3) > 0
          let dstring200 = clipped(dstring200)#trenn_gd#clipped(text3)
       end
       if strlen(text4) > 0
          let dstring200 = clipped(dstring200)#trenn_gd#clipped(text4)
       end
       if strlen(text5) > 0
          let dstring200 = clipped(dstring200)#trenn_gd#clipped(text5)
       end
        let dstring200 = clipped(dstring200)#trenn_de#clipped(sprache)
                                #trenn_se
    end
    select channel datei_schreiben
    if sysstatus
        return ( sysstatus )
    end
    write ( clipped ( dstring200 ))
    return ( sysstatus )
END     // proc FTX_segment 
< ----- */


int CEDIPUT::UNS_segment( char * ityp )
{
    danz_seg ++ ;

    sprintf ( dstring20, "UNS%s%s%s\n"
//   ,trenn_de ,clipped (ityp) ,trenn_se) ; clipped erzeugt Speicherschutzverletzung ....
           ,trenn_de ,ityp ,trenn_se) ;

		int k = fprintf ( fpouti ,dstring20 );
		if ( k > 0 ) return 0 ;
		return -1 ;

}   // proc UNS_segment

int CEDIPUT::Kopfteil(char * edateiname , char * edateinamex, int dta_referenz)
{

   trennz_laden();
//   mwstsetzen ();
   int k = open_datei (edateiname,edateinamex );
   if (k)  return  k ;
   danz_nachrichten = 0 ;
   sprintf ( eigen_iln, "%s", clipped ( mdn.iln)) ;
   sprintf ( dta_iln,"%s", clipped( dta.iln)) ;
   k = UNA_segment() ;
   if (k) { dispmsg ( 251, 1 ); return ( -1 ); }; 

   k = UNB_segment (dta_referenz);
   if (k) { dispmsg ( 251, 1 );  return ( -1 );};
   return 0 ;
}

int CEDIPUT::Fussteil(void)
{
   if ( fpouti == NULL ) return  -1 ;
/* diesen Mechanismus gibbet gar nicht mehr ... 
   if ( ! dgefunden )
   {
	   if ( fpouti )
	   {
		   fclose(fpouti);
		   fpouti = NULL ;
	   }
	   if ( fpout )
	   {
		   fclose(fpout) ;
		   fpout = NULL ;
	   }
	   return -1 ;
   }
< -------- */
   int k = UNZ_segment (/*danz_nachrichten*/ ) ;
   if (k){dispmsg ( 251, 1 ); return  -1 ;};

   k = close_datei () ;
   if (k){dispmsg ( 251, 1 ); return  -1 ;};
    return 0 ;
}



/* das ist die Vorlage aus dem rosi-Teil

PROCEDURE Kopfteil
PARAMETER
field edatei_name char ( 120 )
field edta_referenz integer
END
    let dta_referenz = edta_referenz
    let dateiname = edatei_name

   perform sys_par_holen ( "nachkpreis" )
   returning ( dsys_p_r, dsys_p_w dsys_p_b )
   if dsys_p_r = 0
       let dsys_p_w = "0"
   end
   let dnachkpreis = dsys_p_w
   let dnachkpreis = 4  // 140806 : fix 4 Nachkommastellen gew�nscht

   perform sys_par_holen ( "li_a_par" )
   returning ( dsys_p_r , dsys_p_w , dsys_p_b )
   if not dsys_p_r let dsys_p_w = "0" end
   let dli_a_par = dsys_p_w

   perform sys_par_holen ( "dfw_par" )
   returning ( dsys_p_r , dsys_p_w , dsys_p_b )
   if not dsys_p_r let dsys_p_w = "0" end
   let ddfw_par = dsys_p_w

//   init crust_nummer
//   perform bws_default_holen( "r_ust_nummer" ) returning ( crust_nummer )
//   init cmust_nummer
//   perform bws_default_holen( "m_ust_nummer" ) returning ( cmust_nummer )

   perform trennz_laden
   perform open_datei returning ( k )
   if k
        return ( k )
   end
   let eigen_iln = mdn.iln
   let dta_iln = dta.iln
   perform UNA_segment returning ( k )
   if k
        perform disp_msg ( 251, 1 )    // Fehler beim Schreiben
        return ( -1 )
    end

   perform UNB_segment returning ( k )
   if k
        perform disp_msg ( 251, 1 )    // Fehler beim Schreiben
        return ( -1 )
    end

    return ( 0 )
END
< ----- */
/* das ist die Vorlage aus dem rosi-teil 
PROCEDURE Fussteil

   if ( not dasciiopen )       // #070201 
        return ( 0 )
   end             
   perform UNZ_segment (danz_nachrichten ) returning ( k )
   if k
        perform disp_msg ( 251, 1 )    // Fehler beim Schreiben
        return ( -1 )
    end

   perform close_datei returning ( k )
   if k
        return ( k )
   end

    return ( 0 )
END
< ---- */

int CEDIPUT::open_datei(char *edateiname, char * edateinamex)
{	// hier wird der Zieldateiname bereits uebergeben

	//	dasciiopen = 0 ;
	fpouti = NULL ;

	wsprintf( dateiname, "%s", edateiname );
	if ( strlen ( clipped ( dateiname)) < 3 )
	{
		dispmsg ( 250 , 1 );
		return -1 ;
	}

// 150313 : Temp-Datei auch wirklich ins temp    sprintf (dateinamex,"%sintern", dateiname );
	wsprintf( dateinamex, "%s", edateinamex );

	fpout = NULL ;
	fpouti = NULL ;
	fpout = fopen( dateiname, "r" );

	if (fpout != NULL)
	{	
		fclose ( fpout);
		fpout = NULL ;
		dispmsg ( 17,1) ;
		return -1 ;	// Datei gibbet schon ......
	}

	fpouti = fopen ( dateinamex, "w" );
	if (fpouti == NULL)
	{
		dispmsg ( 251,1) ;
		return -1 ;	// Datei laesst sich nicht schreiben ......
	}
//	dasciiopen = 1;
    return  0 ;
}

int CEDIPUT::close_datei(void )
{
	if ( fpouti ) fclose ( fpouti ) ;
	zeilenweg ();

// 280313 A : hier kann noch allerlei passieren ( nur bestimmte typen, unterschiedliche bat-files ..... )
// 280313 : wenn die Datei nichtexistiert, passiert einfach NICHTS .....
	char kommando[299] ;


	sprintf ( kommando ,"desadvbat.bat %s" , EchtDateiNameKurz ) ;

	int ex_code = ProcWaitExec( (char *) kommando , SW_SHOW, -1, 0, -1, 0 );

// 280313 E


    return  0 ;
}


/* ---->
// #####################################################################
// ### PROC. mwstsetzen
// ###  init. der entsprechenden Werte und Schluessel
// ###  mwst, Faktoren , mwstfibu
// #####################################################################
PROCEDURE mwstsetzen

    execute from sql
        select #ptabn.% from ptabn where ptwert = "1" and ptitem ="mwst"
    end
    let mwstmark1 = "0"
    if not sqlstatus
        let rechen1 = ptabn.ptwer1
        let rechen1 = rechen1 * 100
        let mwstmark1 = rechen1
    end
    execute from sql
        select #ptabn.% from ptabn where ptwert = "2" and ptitem ="mwst"
    end
    let mwstmark2 = "0"
    if not sqlstatus
        let rechen1 = ptabn.ptwer1
        let rechen1 = rechen1 * 100
        let mwstmark2 = rechen1
    end
    execute from sql
        select #ptabn.% from ptabn where ptwert = "1" and ptitem ="mwst_fibu"
    end
    if not sqlstatus
        let mwstmarkf1 = ptabn.ptwer2
    end
    execute from sql
        select #ptabn.% from ptabn where ptwert = "2" and ptitem ="mwst_fibu"
    end
    if not sqlstatus
        let mwstmarkf2 = ptabn.ptwer2
    end
    execute from sql
        select #ptabn.% from ptabn where ptwert = "6" and ptitem ="mwst_fibu"
    end
    if not sqlstatus
        let mwstmarkf6 = ptabn.ptwer2
    end
END     // proc mwstsetzen
< ----- */



int CEDIPUT::zeilenweg ( void )
{
	fpouti = fopen ( dateinamex , "r" ) ;
	if ( fpouti == NULL ) return -1 ;
	fpout = fopen ( dateiname , "w" ) ;
	if ( fpout == NULL )
	{	fclose ( fpouti ) ;
		dispmsg(251 ,1 ) ;
		return -1 ;
	}
	
	int zeich = fgetc( fpouti ) ;
	while ( zeich != EOF )
	{
		if ( zeich != 0x0d && zeich != 0x0a )
		{
			zeich = fputc( zeich, fpout );
			if ( zeich == EOF )
			{
				fclose ( fpouti ) ;
				fclose ( fpout ) ;
				dispmsg(251 ,1 ) ;
				return -1 ;
			}
		}
		zeich = fgetc(fpouti );
	}
	
	fclose ( fpout ) ; fpout = NULL ;
	fclose ( fpouti ) ; fpouti = NULL ;
			
	return 0 ;

}

/* ---->
  if zeipo < 14
        let zeipo = 14
  elseif zeipo > 16
        let zeipo = 14
  end
  display pos ( zeipo 10 ) value ( " Erzeugte Datei : " # clipped (dateiname) )
  let zeipo = zeipo + 1
< ---- */


/* ----->
PROCEDURE name_druckfile
PARAMETER
field envist char ( 30 )
field teil1  char ( 30 )
field teil2  char ( 30 )
END

field dateinamei char ( 120 )

init dateinamei
     ENVIRONMENT ( envist , dateinamei )

     putstr ( dateinamei , "\" # clipped(teil1), 0, 0 )

     putstr ( dateinamei , "." # clipped(teil2), 0, 0 )
     return ( clipped ( dateinamei ))
END

< ----- */

/* ---->
PROCEDURE loesche_files

// rollback der Transaktion auf die Dateien ausdehnen

unlink ( dateiname )
unlink ( dateinamex )

END
< ----- */
/* ---> further usage
PROCEDURE li_a_suchen
field intern_li_a integer
= let intern_li_a = 0  if (( dli_a_par ) or ( dli_a_edi ))
= perform a_kun_suchen if ( a_kun.li_a > "." )
= if ( is decimal ( a_kun.li_a )) let intern_li_a = a_kun.li_a
= end end end
= return ( intern_li_a )
END
< ---- */

