// desadvDlg.h : Headerdatei
//

#pragma once
#include "afxwin.h"


// CdesadvDlg-Dialogfeld
class CdesadvDlg : public CDialog
{
// Konstruktion
public:
	CdesadvDlg(CWnd* pParent = NULL);	// Standardkonstruktor

// Dialogfelddaten
	enum { IDD = IDD_DESADV_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV-Unterst�tzung


// Implementierung
protected:
	HICON m_hIcon;

	// Generierte Funktionen f�r die Meldungstabellen
	virtual BOOL OnInitDialog();
	afx_msg BOOL PreTranslateMessage(LPMSG) ;
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CEdit m_mdnnr;
	CString v_mdnnr;
	CEdit m_mdnname;
	CString v_mdnname;
	CEdit m_tour;
	CString v_tour;
	CEdit m_vdat;
	CString v_vdat;
	CEdit m_bdat;
	CString v_bdat;
	afx_msg void OnEnKillfocusMdnnr();
	afx_msg void OnEnKillfocusTour();
	afx_msg void OnEnKillfocusVdat();
	afx_msg void OnEnKillfocusBdat();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedOk();
 
	void ReadMdn (void) ; 
	virtual BOOL OnReturn (void);
	virtual BOOL OnKeyup (void);

	void CdesadvDlg::paras ( char * globdta , char * globmdn , char * globls  ) ;
	int CdesadvDlg::Dateitesten ( int modus , char * dateiname ) ;
	int CdesadvDlg::opendatei ( void ) ;
	int CdesadvDlg::schreibekopfsatz ( void ) ;
	int CdesadvDlg::schreibepspm (  void ) ;
	int CdesadvDlg::schreibenve ( void ) ;
	int CdesadvDlg::schreibekpap ( void ) ;
	int CdesadvDlg::schreibeschlusssatz (void) ;
	int CdesadvDlg::Dateierstellung (void) ;
	int CdesadvDlg::holedefaults (void) ;

	CEdit m_melde;
	CString v_melde;
	afx_msg void OnCbnSelchangeCombo1();
	afx_msg void OnCbnKillfocusCombo1();
	CComboBox m_combo1;
	CString v_combo1;
	CString v_meldek;
	CString v_meldep;
	long interndta ;
	short internmdn ;
	long internls ;
	CButton m_check;
	BOOL v_checkeinzel;
	afx_msg void OnBnClickedEinzel();
	CStatic m_statictour;
};
