#include "stdafx.h"
#include "DbClass.h"
#include "tempdr.h"

struct TEMPDRX tempdrx,  tempdrx_null;

extern DB_CLASS dbClass;

TEMPDRX_CLASS tempdrx_class ;


int TEMPDRX_CLASS::inserttempdrx (void)
/**
Tabelle tempdrx inserten
**/
{

         if ( ins_cursor < 0 )
         {
             prepare ();
         }
         int di = dbClass.sqlexecute (ins_cursor);
         return di ;

}

int TEMPDRX_CLASS::deletetempdrx (void)
/**
Tabelle tempdrx deleten
**/
{

         if ( del_cursor < 0 )
         {
             prepare ();
         }
         int di = dbClass.sqlexecute (del_cursor);
         return di ;

}
int TEMPDRX_CLASS::update_lfd10 (void)
/**
Feld lfd10 mit Endwert updaten
**/
{

         if ( upd_cursor < 0 )
         {
             prepare ();
         }
         int di = dbClass.sqlexecute (upd_cursor);
         return di ;

}

int TEMPDRX_CLASS::lesetempdrx (void)
/**
max von lfd holen
**/
{

         if ( readcursor < 0 )
         {
             prepare ();
         }
		 tempdrx.lfd = 0L ;
         int di = dbClass.sqlopen (readcursor);
         di = dbClass.sqlfetch (readcursor);
		 if ( di ) tempdrx.lfd = 0L ;
         return di ;
}

void TEMPDRX_CLASS::prepare (void)
{

// 	dbClass.sqlin ((char *)  tempdrx.bearb	,SQLCHAR ,11); - automatisch

	dbClass.sqlin ((char *)  tempdrx.form_nr,SQLCHAR ,11);
	dbClass.sqlin ((short *)&tempdrx.lila	,SQLSHORT,0) ;
	dbClass.sqlin ((long *) &tempdrx.lfd01	,SQLLONG ,0) ;
	dbClass.sqlin ((long *) &tempdrx.lfd02	,SQLLONG ,0) ;
	dbClass.sqlin ((long *) &tempdrx.lfd03	,SQLLONG ,0) ;

	dbClass.sqlin ((long *) &tempdrx.lfd04	,SQLLONG ,0) ;
	dbClass.sqlin ((long *) &tempdrx.lfd05	,SQLLONG ,0) ;
	dbClass.sqlin ((long *) &tempdrx.lfd06	,SQLLONG ,0) ;
	dbClass.sqlin ((long *) &tempdrx.lfd07	,SQLLONG ,0) ;
	dbClass.sqlin ((long *) &tempdrx.lfd08	,SQLLONG ,0) ;

	dbClass.sqlin ((long *) &tempdrx.lfd09	,SQLLONG ,0) ;
	dbClass.sqlin ((long *) &tempdrx.lfd10	,SQLLONG ,0) ;
	dbClass.sqlin ((double *)&tempdrx.dfeld01 ,SQLDOUBLE ,0) ;
	dbClass.sqlin ((double *)&tempdrx.dfeld02 ,SQLDOUBLE ,0) ;
	dbClass.sqlin ((double *)&tempdrx.dfeld03 ,SQLDOUBLE ,0) ;

	dbClass.sqlin ((double *)&tempdrx.dfeld04 ,SQLDOUBLE ,0) ;
	dbClass.sqlin ((double *)&tempdrx.dfeld05 ,SQLDOUBLE ,0) ;
	dbClass.sqlin ((double *)&tempdrx.dfeld06 ,SQLDOUBLE ,0) ;
	dbClass.sqlin ((double *)&tempdrx.dfeld07 ,SQLDOUBLE ,0) ;
	dbClass.sqlin ((double *)&tempdrx.dfeld08 ,SQLDOUBLE ,0) ;

	dbClass.sqlin ((double *)&tempdrx.dfeld09 ,SQLDOUBLE ,0) ;
	dbClass.sqlin ((double *)&tempdrx.dfeld10 ,SQLDOUBLE ,0) ;
	dbClass.sqlin ((double *)&tempdrx.dfeld11 ,SQLDOUBLE ,0) ;
	dbClass.sqlin ((double *)&tempdrx.dfeld12 ,SQLDOUBLE ,0) ;
	dbClass.sqlin ((double *)&tempdrx.dfeld13 ,SQLDOUBLE ,0) ;

	dbClass.sqlin ((double *)&tempdrx.dfeld14 ,SQLDOUBLE ,0) ;
	dbClass.sqlin ((double *)&tempdrx.dfeld15 ,SQLDOUBLE ,0) ;
	dbClass.sqlin ((double *)&tempdrx.dfeld16 ,SQLDOUBLE ,0) ;
	dbClass.sqlin ((double *)&tempdrx.dfeld17 ,SQLDOUBLE ,0) ;
	dbClass.sqlin ((double *)&tempdrx.dfeld18 ,SQLDOUBLE ,0) ;

	dbClass.sqlin ((double *)&tempdrx.dfeld19 ,SQLDOUBLE ,0) ;
	dbClass.sqlin ((double *)&tempdrx.dfeld20 ,SQLDOUBLE ,0) ;

	dbClass.sqlin ((char *) tempdrx.cfeld101 ,SQLCHAR ,17 ) ;
	dbClass.sqlin ((char *) tempdrx.cfeld102 ,SQLCHAR ,17 ) ;
	dbClass.sqlin ((char *) tempdrx.cfeld103 ,SQLCHAR ,17 ) ;
	dbClass.sqlin ((char *) tempdrx.cfeld104 ,SQLCHAR ,17 ) ;
	dbClass.sqlin ((char *) tempdrx.cfeld105 ,SQLCHAR ,17 ) ;

	dbClass.sqlin ((char *) tempdrx.cfeld201 ,SQLCHAR ,25 ) ;
	dbClass.sqlin ((char *) tempdrx.cfeld202 ,SQLCHAR ,25 ) ;
	dbClass.sqlin ((char *) tempdrx.cfeld203 ,SQLCHAR ,25 ) ;
	dbClass.sqlin ((char *) tempdrx.cfeld204 ,SQLCHAR ,25 ) ;
	dbClass.sqlin ((char *) tempdrx.cfeld205 ,SQLCHAR ,25 ) ;

	dbClass.sqlin ((char *) tempdrx.cfeld301 ,SQLCHAR ,37 ) ;
	dbClass.sqlin ((char *) tempdrx.cfeld302 ,SQLCHAR ,37 ) ;
	dbClass.sqlin ((char *) tempdrx.cfeld303 ,SQLCHAR ,37 ) ;
	dbClass.sqlin ((char *) tempdrx.cfeld304 ,SQLCHAR ,37 ) ;
	dbClass.sqlin ((char *) tempdrx.cfeld305 ,SQLCHAR ,37 ) ;
	dbClass.sqlin ((long *) &tempdrx.lfd  ,SQLLONG ,0 ) ;


	ins_cursor = (short) dbClass.sqlcursor ( "insert into tempdrx "
//		" ( bearb ,form_nr, lila) "

	" ( bearb "
	" ,form_nr ,lila ,lfd01 ,lfd02 ,lfd03 "
	" ,lfd04 ,lfd05  ,lfd06 ,lfd07 ,lfd08 "
	" ,lfd09 ,lfd10 ,dfeld01 ,dfeld02 ,dfeld03 "
	" ,dfeld04 ,dfeld05 ,dfeld06 ,dfeld07 ,dfeld08 "
	" ,dfeld09 ,dfeld10 ,dfeld11 ,dfeld12 ,dfeld13 "
	" ,dfeld14 ,dfeld15 ,dfeld16 ,dfeld17 ,dfeld18 "
	" ,dfeld19 ,dfeld20 "
	" ,cfeld101 ,cfeld102 ,cfeld103 ,cfeld104 ,cfeld105 "
	" ,cfeld201 ,cfeld202 ,cfeld203 ,cfeld204 ,cfeld205 "
	", cfeld301 ,cfeld302 ,cfeld303 ,cfeld304 ,cfeld305 "
	" ,lfd ) "
// " values( TODAY ,?,? )  "

	" values( TODAY "
				",?,?,?,?,? "
				",?,?,?,?,? "
				",?,?,?,?,? "
				",?,?,?,?,? "
				",?,?,?,?,? "
				",?,?,?,?,? "
				",?,? "
				",?,?,?,?,? "
				",?,?,?,?,? "
				",?,?,?,?,? "
				",? )"
			 );


// nicht mehr relevant -> lfd ist es !!	dbClass.sqlin ((char *)  tempdrx.form_nr,SQLCHAR ,11);
// nicht mehr relevant -> lfd ist es !!	dbClass.sqlin ((short *)&tempdrx.lila	,SQLSHORT,0) ;
// nicht mehr relevant -> lfd ist es !!	dbClass.sqlin ((long *) &tempdrx.lfd01	,SQLLONG ,0) ;
// nicht mehr relevant -> lfd ist es !!	dbClass.sqlin ((long *) &tempdrx.lfd02	,SQLLONG ,0) ;

// mal 10 Tage stehen lassen, damit man noch was nachvollziehen/nachdrucken kann 

del_cursor = (short) dbClass.sqlcursor ( "delete  from tempdrx "
// " where bearb < TODAY - 10 or ( form_nr = ? and lila = ? "
// " and lfd01 = ? and lfd02 = ? ) " );

" where bearb < TODAY - 10 " ) ;


dbClass.sqlout ((long *) &tempdrx.lfd ,SQLLONG ,0 ) ;

readcursor = (short) dbClass.sqlcursor ( "select max(lfd)  from tempdrx " );


	dbClass.sqlin ((long *) &tempdrx.lfd10  ,SQLLONG ,0 ) ;

	dbClass.sqlin ((long *) &tempdrx.lfd  ,SQLLONG ,0 ) ;

upd_cursor = (short) dbClass.sqlcursor ( "update tempdrx  set lfd10 = ? "
  " where lfd = ? " ) ;

// ##############################################################
	
}
