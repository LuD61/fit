#ifndef _SPADESADV
#define _SPADESADV


// 180511
// Alles neu : in einer Lieferung koennen ja nur nve ODER nicht-nve-Infos vorhanden sein 
// doption == 1 aus lsk/lsp
// ab doption == 2 : nur nve-Infos sind relevant ,keine Fixierung mehr aauf ls-Positionen
// masternve wird dann auch immer mit ausgewertet
// Geistiges Vorbild sei mal das Muster von edeka Rheinstetten

/* Das ist erst mal die Basis-Struktur-Beschreibung

//   290108 : nur Mengen-Ablauf fuer METRO-Dietz-Schopfloch erweitert
//   060208 : dietzaku als "Dietz-Schopfloch-Schalter" mit auswerten
//   060208 und gegebnenfalls alle erfolgreichen Belege mit ausgeben


060607 : jetzt ist die iln-Struktur komplett
zur Historie :
   1.     zuerst gab es eine KUN.ILN je Kunde
   2.     dann kam KUN.PLATTFORM dazu ( bestellt wurde in orders mit
          anderer iln als auf dem LS und der Rechung standen
          zeitweise wurde die Paarung kun.iln/kun.plattform
           als rechungs/Lieferempfaenger-Plattform benutzt
   3.     schliesslich KUN.RECHILN, damit man auch getrennte iln in den
          Adressen zuordnen kann
          leere iln-Felder generieren dann immer eine Nachrutscherei,
          die man im Fehlerfall durch komplettes  Ausfuellen beheben
          sollte
          Zielstruktur : kun.iln = Liefer-ILN
                         kun.rechiln = Rechmepf-ILN
                         kun.plattform = weitere ILN
                          (z.B. fuer Orders oder cross-docking)


// erst noch einen dta-Typ selgros anlegen ( 21)

DESADV 
Bei uns vorerst : Lieferavis-Nummer ist bei uns gleich LS-Nummer
Erstmal realisiert fuer Boesinger ( d.h.: nach touren, LS-Inhalt usw. )

Option 1 : "dummy-CPS :"  Gesamtlieferung dh. Artikel und Menge je Liefeung
Option 2 : "dummy CPS"    gefolgt von Art+Menge und CPI mit (Karton-)NVE
Option 3 : CPS+1;PAC+n++201;CPS+2+1;PAC+1++201;PCI+30E....
                Artikel verteilt je Palette
Option 4 : komplette Hierarchie


Alle 4 Optionen sind legitim ( siehe z.B. MARKANT-Handout )
1 = Nur Menge ohne NVE
2 = Karton-NVE
3 = Paletten-NVE mit Artikelzuordnung
4 = komplette Hierarchie ( Palette UND Karton )



                PNVE            KNVE
O1               o               o
O2               o               1
O3               1               o
O4               1               1

Wenn ich es richtig begreife, will Metro Option 2 .......
Oder auch Option 4 , das muss noch geklaert werden

Option01     CPS+1               dummy -CPS ( Gesamtlieferung)
             LIN+1+Artnum1:EN    1.Pos (Artnum1)
             QTY+12:5            gelieferte Menge = 5 Stck
             LIN+2+Artnum2:EN    2.Pos (Artnum2)
             QTY+12:6            gelieferte Menge = 6 Stck


Option02     CPS+1               dummy -CPS ( Gesamtlieferung)
             LIN+1+Artnum1:EN    1.Pos (Artnum1)
             QTY+12:5            gelieferte Menge = 5 Stck
             PCI+30E             Markierung folgt
             GIN+SS+0814:0817    NVE
             LIN+2+Artnum2:EN    2.Pos (Artnum2)
             QTY+12:6            gelieferte Menge = 6 Stck
             PCI+30E             Markierung folgt
             GIN+SS+0818:0820    NVE

Option03     CPS+1               1.CPS ( keine uebergeordnete
Ebene)
             PAC+2++201          Anzahl Packstuecke : 2 Paletten

             CPS+2+1             2.CPS 1.ISO-Palette aus CPS+1
             PCI+30E             Markierung folgt
             GIN+SS+0812         Pal-NVE
             PAC+8++CT           Anz. Packstuecke : Palette enthaelt 8 Kartons

             LIN+1+Artnum1:EN    1.Pos (Artnum1)
             QTY+12:5            gelieferte Menge = 5 Stck
             LIN+2+Artnum2:EN    2.Pos (Artnum2)
             QTY+12:3            gelieferte Menge = 3 Stck

             CPS+3+1             3.CPS 2.ISO-Palette aus CPS+1
             PCI+30E             Markierung folgt
             GIN+SS+0811         Pal-NVE
             PAC+7++CT           Anz. Packstuecke : Palette enthaelt 7 Kartons

             LIN+3+Artnum3:EN    3.Pos (Artnum3)
             QTY+12:5            gelieferte Menge = 5 Stck
             LIN+4+Artnum4:EN    4.Pos (Artnum4)
             QTY+12:2            gelieferte Menge = 2 Stck

Option04     CPS+1               1.CPS ( keine uebergeordnete Ebene)
             PAC+2++201          Anzahl Packstuecke : 2 Paletten

             CPS+2+1             2.CPS 1.ISO-Palette aus CPS+1
             PCI+30E             Markierung folgt
             GIN+SS+0812         Pal-NVE
             PAC+8++CT           Anz. Packstuecke : Palette enthaelt 8 Kartons

             LIN+1+Artnum1:EN    1.Pos (Artnum1)
             QTY+12:5            gelieferte Menge = 5 Stck
             PCI+30E             Markierung folgt
             GIN+SS+0813:0817    Kart-NVE

             LIN+2+Artnum2:EN    2.Pos (Artnum2)
             QTY+12:3            gelieferte Menge = 3 Stck
             PCI+30E             Markierung folgt
             GIN+SS+0818:0820    Kart-NVE

             CPS+3+1             2.CPS 2.ISO-Palette aus CPS+1
             PCI+30E             Markierung folgt
             GIN+SS+0811         Pal-NVE
             PAC+7++CT           Anz. Packstuecke : Palette enthaelt 7 Kartons

             LIN+3+Artnum3:EN    3.Pos (Artnum3)
             QTY+12:5            gelieferte Menge = 5 Stck
             PCI+30E             Markierung folgt
             GIN+SS+0821:0825    Kart-NVE
             LIN+4+Artnum4:EN    4.Pos (Artnum4)
             QTY+12:2            gelieferte Menge = 2 Stck
             PCI+30E             Markierung folgt
             GIN+SS+0826:0827    Kart-NVE

< ---- */


/* -----> EDEKA : bei master-NVE erfolgt eine weitere Ebene 
1. auf PAC-segment folgt ( wie bei EDEKA immer) MEA-Segment mit Brutto, danach 
 folgt MEA-Segment mit Anzahl LAYER , danach werden die Layer "normal" aufgeloest
CPS+4+1'
PAC+1+:52++PX-H1::9'
MEA+PD+AAB+KGM:617.250'
MEA+PD+LAY+PCE:3'
Bei Gewichtsartikeln folgen nach LIN und PIA noch 2 MEA-Segmente ( AAA und AAB )

< ----- */


// die werte referenzieren auf ptabn.ptitem == "optiondesadv"

#define OPTMENGE	1
#define OPTKARTON	2
#define OPTPALETTE	3
#define OPTALLES	4

extern int doption ;	// der finale Schalter fuer die Struktur -Tiefe


// 080813 : ganz neu PAC_segment ( long anzahl, char * typ1, char * typ2, char * iTYP )

// PAC
// +
// Anzahl
// +
//// :52
//// :4E   oder :5E		Chep(gemietet) oder tauschbar(EURO)
// +iTYP
// ::9'


#endif
