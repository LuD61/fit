#include "stdafx.h"
#include "DbClass.h"
#include "lsnve.h"

struct LSNVE lsnve,  lsnve_null, lsnve_save, lsnve_savem ;
extern DB_CLASS dbClass;

LSNVE_CLASS lsnve_class ;

int LSNVE_CLASS::lesenve0 (void)
{
      int di = dbClass.sqlfetch (readcursor);

	  return di;
}

int LSNVE_CLASS::opennve0 (void)
{

		if ( readcursor < 0 ) prepare ();
		
         return dbClass.sqlopen (readcursor);
}

int LSNVE_CLASS::lesenvea (void)
{
      int di = dbClass.sqlfetch (readabeding);
	  return di;
}

int LSNVE_CLASS::opennvea (void)
{
		if ( readcursor < 0 ) prepare ();
		return dbClass.sqlopen (readabeding);
}

int LSNVE_CLASS::lesenveb (void)
{
      int di = dbClass.sqlfetch (readbbeding);
	  return di;
}

int LSNVE_CLASS::opennveb (void)
{
		if ( readcursor < 0 ) prepare ();
		return dbClass.sqlopen (readbbeding);
}

int LSNVE_CLASS::lesenvec (void)
{
      int di = dbClass.sqlfetch (readcbeding);
	  return di;
}

int LSNVE_CLASS::opennvec (void)
{
		if ( readcursor < 0 ) prepare ();
		return dbClass.sqlopen (readcbeding);
}


int LSNVE_CLASS::lesenvek (void)
{
      int di = dbClass.sqlfetch (del_cursor);
	  return di;
}

int LSNVE_CLASS::opennvek (void)
{
		if ( readcursor < 0 ) prepare ();
		return dbClass.sqlopen (del_cursor);
}

int LSNVE_CLASS::lesenve1 (void)
{
      int di = dbClass.sqlfetch (test_upd_cursor);

	  return di;
}

int LSNVE_CLASS::opennve1 (void)
{

		if ( readcursor < 0 ) prepare ();
		
         return dbClass.sqlopen (test_upd_cursor);
}


int LSNVE_CLASS::lesenveposi (void)
{
      int di = dbClass.sqlfetch (count_cursor);

	  return di;
}

int LSNVE_CLASS::opennveposi (void)
{

		if ( count_cursor < 0 ) prepare ();
		
         return dbClass.sqlopen (count_cursor);
}


void LSNVE_CLASS::prepare (void)
{

/* ->
geplanter Kaufland-Cursor :

	dbClass.sqlin (( short   *) &lsnve.mdn, SQLSHORT, 0 ) ;
 	dbClass.sqlin (( short   *) &lsnve.fil, SQLSHORT, 0 ) ;
 	dbClass.sqlin (( long    *) &lsnve.ls, SQLLONG, 0 ) ;

	select * from lsnve where mdn= ? and fil = ? and ls = ?

	and ( nve == nve_palette or masternve > "" )
	order by master_nve, nve_palette desc, nve



< - */


// hier geht erst mal alles nach "alter Logik"  nve_palette
//													nve
//													nve


// readcursor Ebene 0 - Paletten  eines LS
 	dbClass.sqlin (( short   *) &lsnve.mdn, SQLSHORT, 0 ) ;
 	dbClass.sqlin (( short   *) &lsnve.fil, SQLSHORT, 0 ) ;
 	dbClass.sqlin (( long    *) &lsnve.ls, SQLLONG, 0 ) ;

 	dbClass.sqlout (( long   *) &lsnve.nve_posi, SQLLONG, 0 ) ;
 	dbClass.sqlout (( char   *)  lsnve.nve, SQLCHAR, 31 ) ;
 	dbClass.sqlout (( char   *)  lsnve.nve_palette, SQLCHAR, 31 ) ;
// 	dbClass.sqlout (( long   *) &lsnve.ls, SQLLONG, 0 ) ;
 	dbClass.sqlout (( double *) &lsnve.a, SQLDOUBLE, 0 ) ;
 	dbClass.sqlout (( double *) &lsnve.me, SQLDOUBLE, 0 ) ;
 
	dbClass.sqlout (( short  *) &lsnve.me_einh, SQLSHORT, 0 ) ;
 	dbClass.sqlout (( short  *) &lsnve.gue, SQLSHORT, 0 ) ;
 	dbClass.sqlout (( short  *) &lsnve.palette_kz, SQLSHORT, 0 ) ;
 	dbClass.sqlout (( TIMESTAMP_STRUCT *) &lsnve.tag, SQLTIMESTAMP,26) ;
 	dbClass.sqlout (( TIMESTAMP_STRUCT *) &lsnve.mhd, SQLTIMESTAMP,26) ;

	dbClass.sqlout (( double *) &lsnve.brutto_gew, SQLDOUBLE, 0 ) ;
	dbClass.sqlout (( long   *) &lsnve.verp_art, SQLLONG, 0 ) ;
 	dbClass.sqlout (( double *) &lsnve.ps_art, SQLDOUBLE, 0 ) ;
 	dbClass.sqlout (( double *) &lsnve.ps_stck, SQLDOUBLE, 0 ) ;
 	dbClass.sqlout (( double *) &lsnve.ps_art1, SQLDOUBLE, 0 ) ;

	dbClass.sqlout (( double *) &lsnve.ps_stck1, SQLDOUBLE, 0 ) ;
	// neue Felder
	dbClass.sqlout (( double *) &lsnve.netto_gew, SQLDOUBLE, 0 ) ;
 	dbClass.sqlout (( char   *)  lsnve.masternve, SQLCHAR,31 ) ;
 	dbClass.sqlout (( char   *)  lsnve.ls_charge, SQLCHAR,31 ) ;
// 	dbClass.sqlout (( double *) &lsnve.lief_me, SQLDOUBLE,0 ) ;	// 180511

	
	readcursor = (short) dbClass.sqlcursor ("select "

// 	" nve_posi , nve , nve_palette , ls , a "
 	" nve_posi , nve , nve_palette ,  a , me "
 	" , me_einh , gue , palette_kz , tag , mhd "
 	" , brutto_gew , verp_art , ps_art , ps_stck , ps_art1 "
 	" , ps_stck1 "
	" , netto_gew , masternve , ls_charge "

	" from lsnve where mdn = ? and fil = ? and ls = ? "
	" and nve = nve_palette and gue = 0 "
	" order by a , nve " ) ;

// test_upd_cursor Ebene 1 - Komponenten einer Palette/Lage

 	dbClass.sqlin (( short   *) &lsnve.mdn, SQLSHORT, 0 ) ;
 	dbClass.sqlin (( short   *) &lsnve.fil, SQLSHORT, 0 ) ;
 	dbClass.sqlin (( long    *) &lsnve.ls, SQLLONG, 0 ) ;
 	dbClass.sqlin (( char    *)  lsnve.nve_palette, SQLCHAR, 31 ) ;

 	dbClass.sqlout (( long   *) &lsnve.nve_posi, SQLLONG, 0 ) ;
 	dbClass.sqlout (( char   *)  lsnve.nve, SQLCHAR, 31 ) ;
// 	dbClass.sqlout (( char   *)  lsnve.nve_palette, SQLCHAR, 31 ) ;
// 	dbClass.sqlout (( long   *) &lsnve.ls, SQLLONG, 0 ) ;
 	dbClass.sqlout (( double *) &lsnve.a, SQLDOUBLE, 0 ) ;
 	dbClass.sqlout (( double *) &lsnve.me, SQLDOUBLE, 0 ) ;
 
	dbClass.sqlout (( short  *) &lsnve.me_einh, SQLSHORT, 0 ) ;
 	dbClass.sqlout (( short  *) &lsnve.gue, SQLSHORT, 0 ) ;
 	dbClass.sqlout (( short  *) &lsnve.palette_kz, SQLSHORT, 0 ) ;
 	dbClass.sqlout (( TIMESTAMP_STRUCT *) &lsnve.tag, SQLTIMESTAMP,26) ;
 	dbClass.sqlout (( TIMESTAMP_STRUCT *) &lsnve.mhd, SQLTIMESTAMP,26) ;

	dbClass.sqlout (( double *) &lsnve.brutto_gew, SQLDOUBLE, 0 ) ;
	dbClass.sqlout (( long   *) &lsnve.verp_art, SQLLONG, 0 ) ;
 	dbClass.sqlout (( double *) &lsnve.ps_art, SQLDOUBLE, 0 ) ;
 	dbClass.sqlout (( double *) &lsnve.ps_stck, SQLDOUBLE, 0 ) ;
 	dbClass.sqlout (( double *) &lsnve.ps_art1, SQLDOUBLE, 0 ) ;

	dbClass.sqlout (( double *) &lsnve.ps_stck1, SQLDOUBLE, 0 ) ;
	// neue Felder
	dbClass.sqlout (( double *) &lsnve.netto_gew, SQLDOUBLE, 0 ) ;
 	dbClass.sqlout (( char   *) lsnve.masternve, SQLCHAR,31 ) ;
 	dbClass.sqlout (( char   *) lsnve.ls_charge, SQLCHAR,31 ) ;
// 	dbClass.sqlout (( double *) &lsnve.lief_me, SQLDOUBLE,0 ) ;	// 180511

	test_upd_cursor = (short) dbClass.sqlcursor ("select "

// 	" nve_posi , nve , nve_palette , ls , a "
 	" nve_posi , nve , a , me"
 	" , me_einh , gue , palette_kz , tag , mhd "
 	" , brutto_gew , verp_art , ps_art , ps_stck , ps_art1 "
 	" , ps_stck1 "
	" , netto_gew , masternve , ls_charge "

	" from lsnve where mdn = ? and fil = ? and ls = ? "
	" and nve_palette = ? "
	" and nve <> nve_palette and gue = 0 "
	" order by nve " ) ;

// count_cursor  Komponenten einer LS-Position ( koennten ja auch mehrere physikalische Paletten je LS-Pos sein 

 	dbClass.sqlin (( short   *) &lsnve.mdn, SQLSHORT, 0 ) ;
 	dbClass.sqlin (( short   *) &lsnve.fil, SQLSHORT, 0 ) ;
 	dbClass.sqlin (( long    *) &lsnve.ls, SQLLONG, 0 ) ;
 	dbClass.sqlin (( long    *) &lsnve.nve_posi, SQLLONG, 0 ) ;
 	dbClass.sqlin (( double  *) &lsnve.a, SQLDOUBLE, 0 ) ;

// 	dbClass.sqlout (( long   *) &lsnve.nve_posi, SQLLONG, 0 ) ;
 	dbClass.sqlout (( char   *)  lsnve.nve, SQLCHAR, 31 ) ;
// 	dbClass.sqlout (( char   *)  lsnve.nve_palette, SQLCHAR, 31 ) ;
// 	dbClass.sqlout (( long   *) &lsnve.ls, SQLLONG, 0 ) ;
// 	dbClass.sqlout (( double *) &lsnve.a, SQLDOUBLE, 0 ) ;
 
 	dbClass.sqlout (( double *) &lsnve.me, SQLDOUBLE, 0 ) ;
	dbClass.sqlout (( short  *) &lsnve.me_einh, SQLSHORT, 0 ) ;
 	dbClass.sqlout (( short  *) &lsnve.gue, SQLSHORT, 0 ) ;
 	dbClass.sqlout (( short  *) &lsnve.palette_kz, SQLSHORT, 0 ) ;
 	dbClass.sqlout (( TIMESTAMP_STRUCT *) &lsnve.tag, SQLTIMESTAMP,26) ;
 	dbClass.sqlout (( TIMESTAMP_STRUCT *) &lsnve.mhd, SQLTIMESTAMP,26) ;

	dbClass.sqlout (( double *) &lsnve.brutto_gew, SQLDOUBLE, 0 ) ;
	dbClass.sqlout (( long   *) &lsnve.verp_art, SQLLONG, 0 ) ;
 	dbClass.sqlout (( double *) &lsnve.ps_art, SQLDOUBLE, 0 ) ;
 	dbClass.sqlout (( double *) &lsnve.ps_stck, SQLDOUBLE, 0 ) ;
 	dbClass.sqlout (( double *) &lsnve.ps_art1, SQLDOUBLE, 0 ) ;

	dbClass.sqlout (( double *) &lsnve.ps_stck1, SQLDOUBLE, 0 ) ;
	// neue Felder
	dbClass.sqlout (( double *) &lsnve.netto_gew, SQLDOUBLE, 0 ) ;
 	dbClass.sqlout (( char   *) lsnve.masternve, SQLCHAR,31 ) ;
 	dbClass.sqlout (( char   *) lsnve.ls_charge, SQLCHAR,31 ) ;
// 	dbClass.sqlout (( double *) &lsnve.lief_me, SQLDOUBLE,0 ) ;	// 180511

	test_upd_cursor = (short) dbClass.sqlcursor ("select "

// 	" nve_posi , nve , nve_palette , ls , a "
 	" me , nve "
 	" , me_einh , gue , palette_kz , tag , mhd "
 	" , brutto_gew , verp_art , ps_art , ps_stck , ps_art1 "
 	" , ps_stck1 "
	" , netto_gew , masternve , ls_charge "

	" from lsnve where mdn = ? and fil = ? and ls = ? "
	" and nve_posi = ? and a = ? "
	" and nve <> nve_palette and gue = 0 "
	" order by nve " ) ;

// 180511 : Logik nur noch aus lsnve-Saetzen, aber daf�r bei Bedarf mit master-NVE

// readabeding Ebene 0 - Paletten UND/ODER Masterpaletten eines LS
 	dbClass.sqlin (( short   *) &lsnve.mdn, SQLSHORT, 0 ) ;
 	dbClass.sqlin (( short   *) &lsnve.fil, SQLSHORT, 0 ) ;
 	dbClass.sqlin (( long    *) &lsnve.ls, SQLLONG, 0 ) ;

 	dbClass.sqlout (( long   *) &lsnve.nve_posi, SQLLONG, 0 ) ;
 	dbClass.sqlout (( char   *)  lsnve.nve, SQLCHAR, 31 ) ;
 	dbClass.sqlout (( char   *)  lsnve.nve_palette, SQLCHAR, 31 ) ;
// 	dbClass.sqlout (( long   *) &lsnve.ls, SQLLONG, 0 ) ;
 	dbClass.sqlout (( double *) &lsnve.a, SQLDOUBLE, 0 ) ;
 	dbClass.sqlout (( double *) &lsnve.me, SQLDOUBLE, 0 ) ;
 
	dbClass.sqlout (( short  *) &lsnve.me_einh, SQLSHORT, 0 ) ;
 	dbClass.sqlout (( short  *) &lsnve.gue, SQLSHORT, 0 ) ;
 	dbClass.sqlout (( short  *) &lsnve.palette_kz, SQLSHORT, 0 ) ;
 	dbClass.sqlout (( TIMESTAMP_STRUCT *) &lsnve.tag, SQLTIMESTAMP,26) ;
 	dbClass.sqlout (( TIMESTAMP_STRUCT *) &lsnve.mhd, SQLTIMESTAMP,26) ;

	dbClass.sqlout (( double *) &lsnve.brutto_gew, SQLDOUBLE, 0 ) ;
	dbClass.sqlout (( long   *) &lsnve.verp_art, SQLLONG, 0 ) ;
 	dbClass.sqlout (( double *) &lsnve.ps_art, SQLDOUBLE, 0 ) ;
 	dbClass.sqlout (( double *) &lsnve.ps_stck, SQLDOUBLE, 0 ) ;
 	dbClass.sqlout (( double *) &lsnve.ps_art1, SQLDOUBLE, 0 ) ;

	dbClass.sqlout (( double *) &lsnve.ps_stck1, SQLDOUBLE, 0 ) ;
	// neue Felder
	dbClass.sqlout (( double *) &lsnve.netto_gew, SQLDOUBLE, 0 ) ;
 	dbClass.sqlout (( char   *)  lsnve.masternve, SQLCHAR,31 ) ;
 	dbClass.sqlout (( char   *)  lsnve.ls_charge, SQLCHAR,31 ) ;
// 	dbClass.sqlout (( double *) &lsnve.lief_me, SQLDOUBLE,0 ) ;	// 180511

	
// Paletten eines LS, sortiert nach MASTER-NVE und darin nach Paletten-NVE
	readabeding = (short) dbClass.sqlcursor ("select "

// 	" nve_posi , nve , nve_palette , ls , a "
 	" nve_posi , nve , nve_palette ,  a , me"
 	" , me_einh , gue , palette_kz , tag , mhd "
 	" , brutto_gew , verp_art , ps_art , ps_stck , ps_art1 "
 	" , ps_stck1 "
	" , netto_gew , masternve , ls_charge  "

	" from lsnve where mdn = ? and fil = ? and ls = ? "
	" and nve = nve_palette and gue = 0 "
	" order by masternve ,  nve " ) ;



// readbbeding Paletten einer Masterpalette

 	dbClass.sqlin (( short   *) &lsnve.mdn, SQLSHORT, 0 ) ;
 	dbClass.sqlin (( short   *) &lsnve.fil, SQLSHORT, 0 ) ;
 	dbClass.sqlin (( long    *) &lsnve.ls, SQLLONG, 0 ) ;
 	dbClass.sqlin (( char    *)  lsnve.masternve, SQLCHAR, 31 ) ;

 	dbClass.sqlout (( long   *) &lsnve.nve_posi, SQLLONG, 0 ) ;
 	dbClass.sqlout (( char   *)  lsnve.nve, SQLCHAR, 31 ) ;
 	dbClass.sqlout (( char   *)  lsnve.nve_palette, SQLCHAR, 31 ) ;
// 	dbClass.sqlout (( long   *) &lsnve.ls, SQLLONG, 0 ) ;
 	dbClass.sqlout (( double *) &lsnve.a, SQLDOUBLE, 0 ) ;
 	dbClass.sqlout (( double *) &lsnve.me, SQLDOUBLE, 0 ) ;
 
	dbClass.sqlout (( short  *) &lsnve.me_einh, SQLSHORT, 0 ) ;
 	dbClass.sqlout (( short  *) &lsnve.gue, SQLSHORT, 0 ) ;
 	dbClass.sqlout (( short  *) &lsnve.palette_kz, SQLSHORT, 0 ) ;
 	dbClass.sqlout (( TIMESTAMP_STRUCT *) &lsnve.tag, SQLTIMESTAMP,26) ;
 	dbClass.sqlout (( TIMESTAMP_STRUCT *) &lsnve.mhd, SQLTIMESTAMP,26) ;

	dbClass.sqlout (( double *) &lsnve.brutto_gew, SQLDOUBLE, 0 ) ;
	dbClass.sqlout (( long   *) &lsnve.verp_art, SQLLONG, 0 ) ;
 	dbClass.sqlout (( double *) &lsnve.ps_art, SQLDOUBLE, 0 ) ;
 	dbClass.sqlout (( double *) &lsnve.ps_stck, SQLDOUBLE, 0 ) ;
 	dbClass.sqlout (( double *) &lsnve.ps_art1, SQLDOUBLE, 0 ) ;

	dbClass.sqlout (( double *) &lsnve.ps_stck1, SQLDOUBLE, 0 ) ;
	// neue Felder
	dbClass.sqlout (( double *) &lsnve.netto_gew, SQLDOUBLE, 0 ) ;
 	dbClass.sqlout (( char   *) lsnve.masternve, SQLCHAR,31 ) ;
 	dbClass.sqlout (( char   *) lsnve.ls_charge, SQLCHAR,31 ) ;
 //	dbClass.sqlout (( double *) &lsnve.lief_me, SQLDOUBLE,0 ) ;	// 180511

// readbbeding ALLE Paletten einer Masterpalette(incl. Masterpalette)

	readbbeding = (short) dbClass.sqlcursor ("select "

 	" nve_posi , nve , nve_palette, a , me"
 	" , me_einh , gue , palette_kz , tag , mhd "
 	" , brutto_gew , verp_art , ps_art , ps_stck , ps_art1 "
 	" , ps_stck1 "
	" , netto_gew , masternve , ls_charge "

	" from lsnve where mdn = ? and fil = ? and ls = ? "
	" and masternve = ? and nve_palette = nve "
	" and gue = 0 "
	" order by nve_palette " ) ;

// readcbeding  Komponenten einer Palette 

 	dbClass.sqlin (( short   *) &lsnve.mdn, SQLSHORT, 0 ) ;
 	dbClass.sqlin (( short   *) &lsnve.fil, SQLSHORT, 0 ) ;
 	dbClass.sqlin (( long    *) &lsnve.ls, SQLLONG, 0 ) ;
 	dbClass.sqlin (( char    *) lsnve.nve_palette, SQLCHAR, 31 ) ;

 	dbClass.sqlout (( long   *) &lsnve.nve_posi, SQLLONG, 0 ) ;
 	dbClass.sqlout (( char   *)  lsnve.nve, SQLCHAR, 31 ) ;
 	dbClass.sqlout (( char   *)  lsnve.nve_palette, SQLCHAR, 31 ) ;
 //	dbClass.sqlout (( long   *) &lsnve.ls, SQLLONG, 0 ) ;
 	dbClass.sqlout (( double *) &lsnve.a, SQLDOUBLE, 0 ) ;
 	dbClass.sqlout (( double *) &lsnve.me, SQLDOUBLE, 0 ) ;
 
	dbClass.sqlout (( short  *) &lsnve.me_einh, SQLSHORT, 0 ) ;
 	dbClass.sqlout (( short  *) &lsnve.gue, SQLSHORT, 0 ) ;
 	dbClass.sqlout (( short  *) &lsnve.palette_kz, SQLSHORT, 0 ) ;
 	dbClass.sqlout (( TIMESTAMP_STRUCT *) &lsnve.tag, SQLTIMESTAMP,26) ;
 	dbClass.sqlout (( TIMESTAMP_STRUCT *) &lsnve.mhd, SQLTIMESTAMP,26) ;

	dbClass.sqlout (( double *) &lsnve.brutto_gew, SQLDOUBLE, 0 ) ;
	dbClass.sqlout (( long   *) &lsnve.verp_art, SQLLONG, 0 ) ;
 	dbClass.sqlout (( double *) &lsnve.ps_art, SQLDOUBLE, 0 ) ;
 	dbClass.sqlout (( double *) &lsnve.ps_stck, SQLDOUBLE, 0 ) ;
 	dbClass.sqlout (( double *) &lsnve.ps_art1, SQLDOUBLE, 0 ) ;

	dbClass.sqlout (( double *) &lsnve.ps_stck1, SQLDOUBLE, 0 ) ;
	// neue Felder
	dbClass.sqlout (( double *) &lsnve.netto_gew, SQLDOUBLE, 0 ) ;
 	dbClass.sqlout (( char   *) lsnve.masternve, SQLCHAR,31 ) ;
 	dbClass.sqlout (( char   *) lsnve.ls_charge, SQLCHAR,31 ) ;
// 	dbClass.sqlout (( double *) &lsnve.lief_me, SQLDOUBLE,0 ) ;	// 180511

	readcbeding = (short) dbClass.sqlcursor ("select "

 	" nve_posi , nve , nve_palette , a , me"
 	" , me_einh , gue , palette_kz , tag , mhd "
 	" , brutto_gew , verp_art , ps_art , ps_stck , ps_art1 "
 	" , ps_stck1 "
	" , netto_gew , masternve , ls_charge  "

	" from lsnve where mdn = ? and fil = ? and ls = ? "
	" and nve_palette = ? "
	" and gue = 0 "
	" order by nve " ) ;

// del_cursor Komponenten einer Lieferung / pure Kistenebene 

 	dbClass.sqlin (( short   *) &lsnve.mdn, SQLSHORT, 0 ) ;
 	dbClass.sqlin (( short   *) &lsnve.fil, SQLSHORT, 0 ) ;
 	dbClass.sqlin (( long    *) &lsnve.ls, SQLLONG, 0 ) ;
 
 	dbClass.sqlout (( long   *) &lsnve.nve_posi, SQLLONG, 0 ) ;
 	dbClass.sqlout (( char   *)  lsnve.nve, SQLCHAR, 31 ) ;
 	dbClass.sqlout (( char   *)  lsnve.nve_palette, SQLCHAR, 31 ) ;
// 	dbClass.sqlout (( long   *) &lsnve.ls, SQLLONG, 0 ) ;
 	dbClass.sqlout (( double *) &lsnve.a, SQLDOUBLE, 0 ) ;
 	dbClass.sqlout (( double *) &lsnve.me, SQLDOUBLE, 0 ) ;
 
	dbClass.sqlout (( short  *) &lsnve.me_einh, SQLSHORT, 0 ) ;
 	dbClass.sqlout (( short  *) &lsnve.gue, SQLSHORT, 0 ) ;
 	dbClass.sqlout (( short  *) &lsnve.palette_kz, SQLSHORT, 0 ) ;
 	dbClass.sqlout (( TIMESTAMP_STRUCT *) &lsnve.tag, SQLTIMESTAMP,26) ;
 	dbClass.sqlout (( TIMESTAMP_STRUCT *) &lsnve.mhd, SQLTIMESTAMP,26) ;

	dbClass.sqlout (( double *) &lsnve.brutto_gew, SQLDOUBLE, 0 ) ;
	dbClass.sqlout (( long   *) &lsnve.verp_art, SQLLONG, 0 ) ;
 	dbClass.sqlout (( double *) &lsnve.ps_art, SQLDOUBLE, 0 ) ;
 	dbClass.sqlout (( double *) &lsnve.ps_stck, SQLDOUBLE, 0 ) ;
 	dbClass.sqlout (( double *) &lsnve.ps_art1, SQLDOUBLE, 0 ) ;

	dbClass.sqlout (( double *) &lsnve.ps_stck1, SQLDOUBLE, 0 ) ;
	// neue Felder
	dbClass.sqlout (( double *) &lsnve.netto_gew, SQLDOUBLE, 0 ) ;
 	dbClass.sqlout (( char   *) lsnve.masternve, SQLCHAR,31 ) ;
 	dbClass.sqlout (( char   *) lsnve.ls_charge, SQLCHAR,31 ) ;
 //	dbClass.sqlout (( double *) &lsnve.lief_me, SQLDOUBLE,0 ) ;	// 180511

	del_cursor = (short) dbClass.sqlcursor ("select "

 	" nve_posi , nve , nve_palette , a , me"
 	" , me_einh , gue , palette_kz , tag , mhd "
 	" , brutto_gew , verp_art , ps_art , ps_stck , ps_art1 "
 	" , ps_stck1 "
	" , netto_gew , masternve , ls_charge "

	" from lsnve where mdn = ? and fil = ? and ls = ? "
	" and nve <> nve_palette and gue = 0 "
	" order by a , nve " ) ;

}

