// desadv.h : Hauptheaderdatei f�r die PROJECT_NAME-Anwendung
//

#pragma once

#ifndef __AFXWIN_H__
	#error "\"stdafx.h\" vor dieser Datei f�r PCH einschlie�en"
#endif

#include "resource.h"		// Hauptsymbole


// CdesadvApp:
// Siehe desadv.cpp f�r die Implementierung dieser Klasse
//

class CdesadvApp : public CWinApp
{
public:
	CdesadvApp();

// �berschreibungen
	public:
	virtual BOOL InitInstance();

// Implementierung

	DECLARE_MESSAGE_MAP()
};

extern CdesadvApp theApp;