//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by dtastamm.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_DTASTAMM_DIALOG             102
#define IDR_MAINFRAME                   128
#define IDC_COMBODTA                    1000
#define IDC_TESTKZ                      1001
#define IDC_COMBOTYP                    1002
#define IDC_KURZBEZ                     1003
#define IDC_DTAILN                      1004
#define IDC_RELI                        1005
#define IDC_STATICDATNAM                1006
#define IDC_DATNAM                      1007
#define IDC_PRIWEG                      1009
#define IDC_ADRNR                       1010
#define IDC_ADRNAM1                     1011
#define IDC_ADR_NAM2                    1012
#define IDC_ADR_NAM3                    1013
#define IDC_STR                         1014
#define IDC_EDIT10                      1015
#define IDC_PLZORT                      1015
#define IDC_EDIT1                       1016
#define IDC_COMBO1                      1017
#define IDC_COMBOOPTION                 1017

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1018
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
