// dtastammDlg.h : Headerdatei
//

#pragma once
#include "afxwin.h"


// CdtastammDlg-Dialogfeld
class CdtastammDlg : public CDialog
{
// Konstruktion
public:
	CdtastammDlg(CWnd* pParent = NULL);	// Standardkonstruktor

// Dialogfelddaten
	enum { IDD = IDD_DTASTAMM_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV-Unterst�tzung


// Implementierung
protected:
	HICON m_hIcon;

	// Generierte Funktionen f�r die Meldungstabellen
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CComboBox m_combodta;
public:
	CString v_combodta;
public:
	CEdit m_adrnr;
public:
	CString v_adrnr;
public:
	CEdit m_kurzbez;
public:
	CString v_kurzbez;
public:
	CComboBox m_combotyp;
public:
	CString v_combotyp;
public:
	CButton m_testkz;
public:
	BOOL v_testkz;
public:
	CEdit m_dtailn;
public:
	CString v_dtailn;
public:
	CEdit m_datnam;
public:
	CString v_datnam;
public:
	CEdit m_listnr;
public:
	//  z.Z. 3 Typen zugelassen ( 0 , 1 oder 2 )
	int v_listnr;
public:
	CButton m_priweg;
public:
	BOOL v_priweg;
public:
	CEdit m_adrnam1;
public:
	CString v_adrnam1;
public:
	CEdit m_adrnam2;
public:
	CString v_adrnam2;
public:
	CEdit m_adrnam3;
public:
	CString v_adrnam3;
public:
	CEdit m_str;
public:
	CString v_str;
public:
	CEdit m_plzort;
public:
	CString v_plzort;
public:
	afx_msg void OnBnClickedTestkz();
public:
	afx_msg void OnBnClickedPriweg();
public:
	afx_msg void OnCbnKillfocusCombodta();
public:
	afx_msg void OnEnKillfocusKurzbez();
public:
	afx_msg void OnCbnKillfocusCombotyp();
public:
	afx_msg void OnCbnSelchangeCombotyp();
public:
	afx_msg void OnEnKillfocusDtailn();
public:
	afx_msg BOOL PreTranslateMessage(LPMSG) ;
	afx_msg BOOL NoListCtrl (CWnd *) ;
public:
	void komplettiereadr ( long internadr ) ;
	void komplettieredta ( int isqlstat ) ;
	void komplettieretyp ( int ityp ) ;
	void komplettiereoption ( int ioption ) ;	// 120411 
	void writedta( void) ;

public:
	afx_msg void OnCbnSelendokCombodta();
public:
	afx_msg void OnBnClickedOk();
public:
	afx_msg void OnEnKillfocusAdrnr();
	afx_msg void OnCbnKillfocusCombooption();
	CComboBox m_combooption;
	CString v_combooption;
};
