// dtastammDlg.cpp : Implementierungsdatei
//


#include "stdafx.h"
#include "dtastamm.h"
#include "dtastammDlg.h"

#include "dbClass.h"
#include "dta.h"
#include "adr.h"
#include "ptabn.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


extern DB_CLASS dbClass ;
extern ADR_CLASS adr_class ;
extern DTA_CLASS dta_class ;
extern PTABN_CLASS ptabn_class ;


static int interntestkz = 0 ;
static int internpriweg = 0 ;
char bufh[500] ;


// CAboutDlg-Dialogfeld f�r Anwendungsbefehl "Info"

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialogfelddaten
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterst�tzung

// Implementierung
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CdtastammDlg-Dialogfeld


CdtastammDlg::CdtastammDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CdtastammDlg::IDD, pParent)
	, v_combodta(_T(""))
	, v_adrnr(_T(""))
	, v_kurzbez(_T(""))
	, v_combotyp(_T(""))
	, v_testkz(FALSE)
	, v_dtailn(_T(""))
	, v_datnam(_T(""))
	, v_listnr(0)
	, v_priweg(FALSE)
	, v_adrnam1(_T(""))
	, v_adrnam2(_T(""))
	, v_adrnam3(_T(""))
	, v_str(_T(""))
	, v_plzort(_T(""))
	, v_combooption(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CdtastammDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBODTA, m_combodta);
	DDX_CBString(pDX, IDC_COMBODTA, v_combodta);
	DDX_Control(pDX, IDC_ADRNR, m_adrnr);
	DDX_Text(pDX, IDC_ADRNR, v_adrnr);
	DDV_MaxChars(pDX, v_adrnr, 8);
	DDX_Control(pDX, IDC_KURZBEZ, m_kurzbez);
	DDX_Text(pDX, IDC_KURZBEZ, v_kurzbez);
	DDV_MaxChars(pDX, v_kurzbez, 16);
	DDX_Control(pDX, IDC_COMBOTYP, m_combotyp);
	DDX_CBString(pDX, IDC_COMBOTYP, v_combotyp);
	DDX_Control(pDX, IDC_TESTKZ, m_testkz);
	DDX_Check(pDX, IDC_TESTKZ, v_testkz);
	DDX_Control(pDX, IDC_DTAILN, m_dtailn);
	DDX_Text(pDX, IDC_DTAILN, v_dtailn);
	DDX_Control(pDX, IDC_DATNAM, m_datnam);
	DDX_Text(pDX, IDC_DATNAM, v_datnam);
	DDX_Control(pDX, IDC_RELI, m_listnr);
	DDX_Text(pDX, IDC_RELI, v_listnr);
	DDV_MinMaxInt(pDX, v_listnr, 0, 2);
	DDX_Control(pDX, IDC_PRIWEG, m_priweg);
	DDX_Check(pDX, IDC_PRIWEG, v_priweg);
	DDX_Control(pDX, IDC_ADRNAM1, m_adrnam1);
	DDX_Text(pDX, IDC_ADRNAM1, v_adrnam1);
	DDX_Control(pDX, IDC_ADR_NAM2, m_adrnam2);
	DDX_Text(pDX, IDC_ADR_NAM2, v_adrnam2);
	DDX_Control(pDX, IDC_ADR_NAM3, m_adrnam3);
	DDX_Text(pDX, IDC_ADR_NAM3, v_adrnam3);
	DDX_Control(pDX, IDC_STR, m_str);
	DDX_Text(pDX, IDC_STR, v_str);
	DDX_Control(pDX, IDC_PLZORT, m_plzort);
	DDX_Text(pDX, IDC_PLZORT, v_plzort);
	DDV_MaxChars(pDX, v_dtailn, 16);
	DDV_MaxChars(pDX, v_datnam, 16);	// 150313 16->80 ( Die Datenbank muss passen !!
										// 150313 : erst mal wieder auf 16 zu�rckgestellt,
										// das wird dann hier die einizge anzupassende Stelle
	DDX_Control(pDX, IDC_COMBOOPTION, m_combooption);
	DDX_CBString(pDX, IDC_COMBOOPTION, v_combooption);
}

BEGIN_MESSAGE_MAP(CdtastammDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_TESTKZ, &CdtastammDlg::OnBnClickedTestkz)
	ON_BN_CLICKED(IDC_PRIWEG, &CdtastammDlg::OnBnClickedPriweg)
	ON_CBN_KILLFOCUS(IDC_COMBODTA, &CdtastammDlg::OnCbnKillfocusCombodta)
	ON_EN_KILLFOCUS(IDC_KURZBEZ, &CdtastammDlg::OnEnKillfocusKurzbez)
	ON_CBN_KILLFOCUS(IDC_COMBOTYP, &CdtastammDlg::OnCbnKillfocusCombotyp)
	ON_CBN_SELCHANGE(IDC_COMBOTYP, &CdtastammDlg::OnCbnSelchangeCombotyp)
	ON_EN_KILLFOCUS(IDC_DTAILN, &CdtastammDlg::OnEnKillfocusDtailn)
	ON_CBN_SELENDOK(IDC_COMBODTA, &CdtastammDlg::OnCbnSelendokCombodta)
	ON_BN_CLICKED(IDOK, &CdtastammDlg::OnBnClickedOk)
	ON_EN_KILLFOCUS(IDC_ADRNR, &CdtastammDlg::OnEnKillfocusAdrnr)
	ON_CBN_KILLFOCUS(IDC_COMBOOPTION, &CdtastammDlg::OnCbnKillfocusCombooption)
END_MESSAGE_MAP()


// CdtastammDlg-Meldungshandler

BOOL CdtastammDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Hinzuf�gen des Men�befehls "Info..." zum Systemmen�.

	// IDM_ABOUTBOX muss sich im Bereich der Systembefehle befinden.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Symbol f�r dieses Dialogfeld festlegen. Wird automatisch erledigt
	//  wenn das Hauptfenster der Anwendung kein Dialogfeld ist
	SetIcon(m_hIcon, TRUE);			// Gro�es Symbol verwenden
	SetIcon(m_hIcon, FALSE);		// Kleines Symbol verwenden

	// TODO: Hier zus�tzliche Initialisierung einf�gen

	// alle dta lesen

	CString szdta ;

	int sqlstat = dta_class.openalldta ();
	sqlstat = dta_class.lesealldta();
	while(!sqlstat)
	{
	
		szdta.Format("%8.0d  %s",dta.dta,dta.kun_krz1);
		// hier haben wir jetzt die dta und k�nnen es in die ListBox einf�gen
		((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBODTA))->AddString(szdta.GetBuffer(0));
		((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBODTA))->SetCurSel(0);
	
		sqlstat = dta_class.lesealldta () ;
	}

// lese alle Typen ( Init der Combobox )

	CString szptabn ;

	sprintf ( ptabn.ptitem ,"dta_typ" ) ;
	sqlstat = ptabn_class.openallptabn ();
	sqlstat = ptabn_class.leseallptabn ();
	while(!sqlstat)
	{
	
		szptabn.Format("%s   %s",ptabn.ptwert, ptabn.ptbez );
				// hier haben wir jetzt die Typen und k�nnen diese in die ListBox einf�gen
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOTYP))->AddString(szptabn.GetBuffer(0));
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOTYP))->SetCurSel(0);
	
		sqlstat = ptabn_class.leseallptabn () ;
	}

	sprintf ( ptabn.ptitem ,"optiondesadv" ) ;
	sqlstat = ptabn_class.openallptabn ();
	sqlstat = ptabn_class.leseallptabn ();
	while(!sqlstat)
	{
	
		szptabn.Format("%s   %s",ptabn.ptwert, ptabn.ptbez );
				// hier haben wir jetzt die Optionen und k�nnen diese in die ListBox einf�gen
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOOPTION))->AddString(szptabn.GetBuffer(0));
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOOPTION))->SetCurSel(0);
	
		sqlstat = ptabn_class.leseallptabn () ;
	}


	static int interntestkz = 0 ;
	static int internpriweg = 0 ;

	return TRUE;  // Geben Sie TRUE zur�ck, au�er ein Steuerelement soll den Fokus erhalten
}

void CdtastammDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// Wenn Sie dem Dialogfeld eine Schaltfl�che "Minimieren" hinzuf�gen, ben�tigen Sie 
//  den nachstehenden Code, um das Symbol zu zeichnen. F�r MFC-Anwendungen, die das 
//  Dokument/Ansicht-Modell verwenden, wird dies automatisch ausgef�hrt.

void CdtastammDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Ger�tekontext zum Zeichnen

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Symbol in Clientrechteck zentrieren
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Symbol zeichnen
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// Die System ruft diese Funktion auf, um den Cursor abzufragen, der angezeigt wird, w�hrend der Benutzer
//  das minimierte Fenster mit der Maus zieht.
HCURSOR CdtastammDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

BOOL CdtastammDlg::PreTranslateMessage(LPMSG lpMsg)
{
	CWnd *cWnd;
	 if (lpMsg->message == WM_KEYDOWN)
	 {
		 switch (lpMsg->wParam)
			 {
		        case VK_RETURN :
					cWnd = GetFocus ();
			           if(cWnd == GetDlgItem (IDOK))
							return CDialog::PreTranslateMessage(lpMsg);

			           if(cWnd == GetDlgItem (IDCANCEL))
					   {
							OnCancel ();
 							return TRUE;
					   }

/* --->
			           if(cWnd == GetDlgItem (IDC_KUNN))
					   {	Read (); return TRUE; }
                       else if (cWnd == GetDlgItem (IDC_MDN))
                       { if (ReadMdn () == FALSE)
                       { return TRUE;
                       } }
 < ----- */
			   NextDlgCtrl();
						return TRUE;

			 case VK_F5 :
                     OnCancel ();
 			         return TRUE;
/* ----->
			 case VK_F7 :
                     OnDelete ();
 			         return TRUE;
             case VK_F9 :
		             if (OnF9 ())
                     {
                         return TRUE;
                     }
                     break;
		     case VK_F6 :
                     OnFree ();
				     return TRUE;
 <----- */

/* ---->
			 case VK_F12 :
					UpdateData( TRUE ) ;
					ladevartodaba();
					if (  tou_class.testupdtou() )
						tou_class.inserttou () ;
					else
						tou_class.updatetou () ;

					v_edithtour = 0 ;
					tou.tou = 0 ;
					UpdateData(FALSE) ;
					m_edithtour.EnableWindow (TRUE) ;
					m_buttonhtour.EnableWindow ( TRUE) ;
					m_edithtour.ModifyStyle (0, WS_TABSTOP,0) ;
					m_buttonhtour.ModifyStyle ( 0, WS_TABSTOP,0) ;
					m_edithtour.SetFocus () ;	


//                     OnOK ();
				     return TRUE;
< ----- */


 		    case VK_DOWN :
                     if (NoListCtrl (GetFocus ()))
                     {
						NextDlgCtrl();
						return TRUE;
					
                     }
				     break;
 		    case VK_UP :
                     if (NoListCtrl (GetFocus ()))
                     {
						PrevDlgCtrl();
	 					cWnd = GetFocus ();
					 	GetNextDlgTabItem(cWnd ,FALSE) ;
						return TRUE;
                     }
				     break;
		 }
	 }

return CDialog::PreTranslateMessage(lpMsg);

}

BOOL CdtastammDlg::NoListCtrl (CWnd *cWnd)
{
    if (((CComboBox *) cWnd->GetParent ())->GetCount () > 1)
    {
        return FALSE;
    }
    else if (((CComboBox *) cWnd)->GetCount () > 1)
    {
        return FALSE;
    }
    else if (((CListBox *) cWnd)->GetCount () > 1)
    {
        return FALSE;
    }
    return TRUE;
}

// Ende pretranslate


void CdtastammDlg::OnBnClickedTestkz()
{
// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	UpdateData ( TRUE ) ;
	if ( v_testkz == TRUE )
	{
		interntestkz = 1 ;
	}
	else
	{
		interntestkz = 0 ;
	}

	UpdateData( FALSE ) ;
}

void CdtastammDlg::OnBnClickedPriweg()
{
// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
		UpdateData ( TRUE ) ;
	if ( v_priweg == TRUE )
	{
		internpriweg = 1 ;
	}
	else
	{
		internpriweg = 0 ;
	}

	UpdateData( FALSE ) ;

}

void CdtastammDlg::komplettiereadr ( long internadr )
{
	
	memcpy ( &adr, & adr_null ,sizeof ( struct ADR )) ;
	v_adrnam1.Format(" " ) ; 
	v_adrnam2.Format(" " ) ; 
	v_adrnam3.Format(" " ) ; 
	v_str.Format(" " ) ; 
	v_plzort.Format(" " ) ; 

	if ( internadr > 0 )
	{
		adr.adr = internadr ;
		int stat = adr_class.openadr () ;
		stat = adr_class.leseadr () ;
		if ( ! stat )
		{
			v_adrnam1.Format("%s", adr.adr_nam1 ) ; 
			v_adrnam2.Format("%s", adr.adr_nam2 ) ; 
			v_adrnam3.Format("%s", adr.adr_nam3 ) ; 
			v_str.Format("%s", adr.str ) ; 
			v_plzort.Format("%s %s", adr.plz , adr.ort1 ) ; 
		}
		else
		{
			adr.adr = 0  ; 
		}
	}
}

void CdtastammDlg::komplettieretyp ( int ityp ) 
{
	if ( ityp > 0 )
	{
		sprintf ( bufh ,"%1.0d" , ityp ) ;
		int nCurSel = -1 ;
		nCurSel=((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOTYP))->FindString(nCurSel, bufh);
		if (nCurSel != CB_ERR)	// sollte dann immer passen ?!
		{
//			CString bufx ;
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOTYP))->SetCurSel(nCurSel) ;
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOTYP))->GetLBText(nCurSel, v_combotyp);
//			sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
		}
	}
	else
	{
			v_combotyp.Format("                   ");
	}	

	if ( ityp >  99 )
	{
//	150313	m_datnam.EnableWindow( TRUE );
		m_listnr.EnableWindow( FALSE );
	}
	else
	{	
// 150313		m_datnam.EnableWindow( FALSE );
		m_listnr.EnableWindow( TRUE );
	}
	if ( ityp >  100 )	// genau CSB-FG (== 100 )ist anders 
	{
		m_priweg.EnableWindow( TRUE );
	}
	else
	{	
		m_priweg.EnableWindow( FALSE );
	}

}


void CdtastammDlg::komplettiereoption ( int ioption ) 
{
	if ( ioption > 0 )
	{
		sprintf ( bufh ,"%1.0d" , ioption ) ;
		int nCurSel = -1 ;
		nCurSel=((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOOPTION))->FindString(nCurSel, bufh);
		if (nCurSel != CB_ERR)	// sollte dann immer passen ?!
		{
//			CString bufx ;
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOOPTION))->SetCurSel(nCurSel) ;
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOOPTION))->GetLBText(nCurSel, v_combooption);
//			sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
		}
	}
	else
	{
			v_combooption.Format("                   ");
	}	
}


void CdtastammDlg::komplettieredta ( int i )
{	// i entspricht dem sqlstatus und es wird immer genau nach einem Neu-Lesen aufgerufen 
	// sollte daher fast in lesedta integriert werden ?! 
	if ( i )
	{
				v_combodta.Format("                   ");
			memcpy ( &dta , &dta_null, sizeof( struct DTA)) ;
	}

	memcpy ( &dta_gelesen , &dta, sizeof ( struct DTA )) ;

	komplettieretyp ( dta_gelesen.typ ) ;
	komplettiereoption ( dta_gelesen.optiondesadv ) ;

// Alle Tabellenfelder in Maskenfelder laden 
	v_dtailn.Format("%s", dta_gelesen.iln);
	v_kurzbez.Format("%s", dta_gelesen.kun_krz1);
	if ( dta_gelesen.adr > 0 )
		v_adrnr.Format("%1.0d", dta_gelesen.adr ) ;
	else
		v_adrnr.Format("") ;

	v_datnam.Format("%s", dta_gelesen.dat_name);

	if ( dta_gelesen.pri_seg )
		v_priweg = TRUE ;
	else
		v_priweg = FALSE ;

	v_listnr = dta_gelesen.reli ;

	if ( dta_gelesen.pri_seg )
		v_priweg = TRUE ;
	else
		v_priweg = FALSE ;

	if ( dta_gelesen.test_kz )
		v_testkz = TRUE ;
	else
		v_testkz = FALSE ;

	dta.waehrung = 2 ;	// fix Euro -> further usage bei der naechsten Waehrungsreform 
			
	komplettiereadr ( dta.adr ) ;
}


void CdtastammDlg::OnCbnKillfocusCombodta()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	UpdateData(TRUE) ;
	bufh[0] = '\0' ;

	int nCurSel = ((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBODTA))->GetCurSel();
	CString bufx;
	if ( nCurSel > -1 )
	{
		((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBODTA))->GetLBText(nCurSel, bufx);
		v_combodta = bufx ;
		sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
		int i = (int) strlen ( bufh );
		// fixe formatierung : 8 stellen und 2 blanks , danach folgt ein text
		if ( i )
		{
			if ( i < 9 )
			{
				bufh[i ] = '\0' ;
			}
			else
			{
				bufh[9] = '\0' ;
			}
			dta.dta = atol ( bufh ) ;
			if ( ( dta.dta == dta_gelesen.dta ) && (dta.dta > 0 ))
			{
				// nicht doppelt lesen, solange innerhlb des identischen Satzes 
			}
			else
			{
				i = dta_class.opendta();
				i = dta_class.lesedta();
				komplettieredta ( i ) ;
			}

		}
		else
		{
			komplettieredta ( 100 ) ;	// d.h. loeschen
		}
	}
	else
	{
		sprintf ( bufh, "%s", v_combodta );
		int i = (int) strlen ( bufh );
		// fixe formatierung : 8 stellen und 2 blanks , danach folgt ein text
		if ( i )
		{
			if ( i < 9 )
			{
				bufh[i ] = '\0' ;
			}
			else
			{
				bufh[9] = '\0' ;
			}
		}
		long hilfe = atol ( bufh ) ;
		nCurSel = -1 ;
		sprintf ( bufh, "%8.0d", hilfe );
		nCurSel=((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBODTA))->FindString(nCurSel, bufh);
	
		if (nCurSel != CB_ERR)
		{
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBODTA))->SetCurSel(nCurSel) ;
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBODTA))->GetLBText(nCurSel, bufx);
			v_combodta = bufx ;
			sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
			i = (int) strlen ( bufh );
			// fixe formatierung : 8 stellen und 2 blanks , danach folgt ein text
			if ( i )
			{
				if ( i < 9 )
				{
					bufh[i] = '\0' ;
				}
			}
			else
			{
				bufh[9] = '\0' ;
			}
			dta.dta = atol ( bufh ) ;
			if ( ( dta.dta == dta_gelesen.dta ) && (dta.dta > 0 ))
			{
				// nicht sopplet innerhalb des identischen Satzes 
			}
			else
			{
				i = dta_class.opendta();
				i = dta_class.lesedta();
				komplettieredta (i) ;
			}
		}
		else
		{
			MessageBox("Neuanlage!", " ", MB_OK|MB_ICONSTOP);

			komplettieredta ( 100 ) ;
			dta.dta = hilfe ;
			v_combodta.Format ( "%8.0d" , hilfe ) ; 

			// Neuanlage ?!
		}
	}
	UpdateData(FALSE);
}

void CdtastammDlg::OnEnKillfocusKurzbez()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
// hier passiert gar nix, gegebnenfalls wird spaeter mal geupdatet ?! 
}

void CdtastammDlg::OnCbnKillfocusCombotyp()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	UpdateData(TRUE) ;
	bufh[0] = '\0' ;

	int nCurSel = ((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOTYP))->GetCurSel();
	CString bufx;
	if ( nCurSel > -1 )
	{
		((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOTYP))->GetLBText(nCurSel, bufx);

		sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
		int i = (int) strlen ( bufh );
		// fixe formatierung : max 3 stellen und danach evtl. blank und Bezeichnung
		if ( i )
		{
			bufh[3] = '\0' ;
			if ( bufh[2] > '9' ||bufh[2] < '0' )
			{
				bufh[2] = '\0' ;
			}	
			if ( bufh[1] > '9' ||bufh[1] < '0' )
			{
				bufh[1] = '\0' ;
			}
			if ( bufh[0] > '9' ||bufh[0] < '0' )
			{
				bufh[0] = '\0' ;
			}
			sprintf ( ptabn.ptwert ,"%s", bufh ) ;
			sprintf ( ptabn.ptitem ,"dta_typ" ) ;
			i = ptabn_class.openptabn();
			i = ptabn_class.leseptabn();
		}
		else
		{

// darf es gar nicht geben			loeschen/ Error-Handling 
		}
	}
	else
	{
		sprintf ( bufh, "%s", v_combotyp );
		int i = (int) strlen ( bufh );	// blank ist dann halt ne Fehleingabe .....
		// fixe formatierung : max 3 stellen und dann blank, danach Text ...
		if ( i )
		{
			bufh[3] = '\0' ;
			if ( bufh[2] > '9' ||bufh[2] < '0' )
			{
				bufh[2] = '\0' ;
			}	
			if ( bufh[1] > '9' ||bufh[1] < '0' )
			{
				bufh[1] = '\0' ;
			}
			if ( bufh[0] > '9' ||bufh[0] < '0' )
			{
				bufh[0] = '\0' ;
			}
		}
//		sprintf ( ptabn.ptwert ,"%s", bufh ) ;
//		sprintf ( ptabn.ptitem ,"dta_typ" ) ;
//		i = ptabn_class.openptabn();
//		i = ptabn_class.leseptabn();
// es darf nur bereits in der Box stehende Werte geben 
		nCurSel = -1 ;
		nCurSel=((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOTYP))->FindString(nCurSel, bufh);
		if (nCurSel != CB_ERR)
		{
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOTYP))->SetCurSel(nCurSel) ;
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOTYP))->GetLBText(nCurSel, bufx);
			v_combotyp = bufx ;
//			sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
		}
		else
		{
			MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
			komplettieretyp ( dta_gelesen.typ ) ;
		}
	}
	
	UpdateData(FALSE);
}

void CdtastammDlg::OnCbnSelchangeCombotyp()
{

	return ;	// killfocus macht das alles viel  besser 
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	UpdateData(TRUE) ;
//	int i = m_Combo1.GetLine(0,bufh,9) ;

	int nCurSel = ((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOTYP))->GetCurSel();
	CString bufc;
	((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOTYP))->GetLBText(nCurSel, bufc);

	sprintf(bufh,"%s",bufc.GetBuffer(0)) ;

	// formatierung : ptwert(max. 3 stellen) , dann ein blank und dann ptbezk
	int i = (int)strlen ( bufh) ;
	if ( i )
	{
		bufh[3] = '\0' ;
		if ( bufh[2] > '9' ||bufh[2] < '0' )
		{
			bufh[2] = '\0' ;
		}
		if ( bufh[1] > '9' ||bufh[1] < '0' )
		{
			bufh[1] = '\0' ;
		}
		if ( bufh[0] > '9' ||bufh[0] < '0' )
		{
			bufh[0] = '\0' ;
		}

		sprintf ( ptabn.ptwert ,"%s", bufh ) ;
		sprintf ( ptabn.ptitem ,"dta_typ" ) ;
		i = ptabn_class.openptabn();
		i = ptabn_class.leseptabn();
	}
}

void CdtastammDlg::OnEnKillfocusDtailn()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	// hier evtl. checksumme pruefen ?!  
}

void CdtastammDlg::OnCbnSelendokCombodta()
{

	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	OnCbnKillfocusCombodta() ;
}

void CdtastammDlg::writedta()
{
	int modif = 0 ;
	int ierror = 0 ;

// ########## dta.adr	
	dta.adr = atol ( v_adrnr ) ;
	if ( dta.adr < 1 ) dta.adr = 0 ;
	if ( dta.adr != dta_gelesen.adr ) modif ++ ;
// ########## dta.dat_name	
	sprintf ( dta.dat_name ,"%s", v_datnam ) ;
	if ( strcmp ( dta.dat_name , dta_gelesen.dat_name ) || (strlen (dta.dat_name )!= strlen ( dta_gelesen.dat_name ))) modif ++ ;
// ##########	dta.dta
	sprintf(bufh,"%s",v_combodta.GetBuffer(0)) ;
	int i = (int) strlen ( bufh );
	if ( i )
	{
		if ( i < 9 )
			bufh[i ] = '\0' ;
		else
				bufh[9] = '\0' ;
		dta.dta = atol ( bufh ) ;
	}
	else
		ierror ++ ;
// ########## dta.iln	
	sprintf ( dta.iln ,"%s",v_dtailn.GetBuffer()) ;
	if ( strcmp ( dta.iln , dta_gelesen.iln ) || (strlen (dta.iln )!= strlen ( dta_gelesen.iln ))) modif ++ ;
// ########## dta.kun_krz1	
	sprintf ( dta.kun_krz1 ,"%s", v_kurzbez.GetBuffer());
	if ( strcmp ( dta.kun_krz1 , dta_gelesen.kun_krz1 ) || (strlen (dta.kun_krz1 )!= strlen ( dta_gelesen.kun_krz1 ))) modif ++ ;
// ########## dta.nummer	
	dta.nummer = dta_gelesen.nummer ;
	if ( dta.nummer < 1 )
			dta.nummer = 1 ;
	if ( dta.nummer != dta_gelesen.nummer ) modif ++ ;
// ########## dta.pri_seg
	if ( v_priweg )
		dta.pri_seg = 1 ;	// Preissegment ignorieren 
	else
		dta.pri_seg = 0 ;
	if ( dta.pri_seg != dta_gelesen.pri_seg ) modif ++ ;
// ########## dta.reli
	dta.reli =  v_listnr ;
	if ( dta.reli > 2 ||dta.reli < 0 ) dta.reli = 0 ;
	if ( dta.reli != dta_gelesen.reli ) modif ++ ;
// ########## dta.test_kz
	if ( v_testkz )
		dta.test_kz = 1 ;
	else
		dta.test_kz = 0 ;
	if ( dta.test_kz != dta_gelesen.test_kz ) modif ++ ;
// ########## dta.typ
	sprintf(bufh,"%s",v_combotyp.GetBuffer(0)) ;
	bufh[3] = '\0' ;
	if ( bufh[2] < '0' ||bufh[2] > '9' ) bufh[2] = '\0' ;
	if ( bufh[1] < '0' ||bufh[1] > '9' ) bufh[1] = '\0' ;
	if ( bufh[0] < '0' ||bufh[0] > '9' ) bufh[0] = '\0' ;
	dta.typ = atoi ( bufh ) ;
	if ( dta.typ != dta_gelesen.typ ) modif ++ ;

// ########## dta.optiondesadv
	sprintf(bufh,"%s",v_combooption.GetBuffer(0)) ;
	bufh[3] = '\0' ;
	if ( bufh[2] < '0' ||bufh[2] > '9' ) bufh[2] = '\0' ;
	if ( bufh[1] < '0' ||bufh[1] > '9' ) bufh[1] = '\0' ;
	if ( bufh[0] < '0' ||bufh[0] > '9' ) bufh[0] = '\0' ;
	dta.optiondesadv = atoi ( bufh ) ;
	if ( dta.optiondesadv != dta_gelesen.optiondesadv ) modif ++ ;

	dta.waehrung = 2 ; // zun�chst fix -> further usage 
	if ( ierror ) 
	{
		MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
		return ;
	}

	if ( !modif ) return ;	// Nichts ge�ndert

	if ( dta.dta == dta_gelesen.dta && dta.dta > 0  )
	{
		modif = dta_class.updatedta () ;

		if ( strcmp ( dta.kun_krz1 , dta_gelesen.kun_krz1 )
			|| (strlen (dta.kun_krz1 )!= strlen ( dta_gelesen.kun_krz1 )))
		{	// dann muss die combodta aktualsiert werden ......
			if ( !modif )
				memcpy ( &dta_gelesen , &dta , sizeof ( struct DTA ));	// erst mal sichern 

			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBODTA))->ResetContent();
			CString szdta ;
			int sqlstat = dta_class.openalldta ();
			sqlstat = dta_class.lesealldta();
			while(!sqlstat)
			{
				szdta.Format("%8.0d  %s",dta.dta,dta.kun_krz1);
				((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBODTA))->AddString(szdta.GetBuffer(0));
				((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBODTA))->SetCurSel(0);
				sqlstat = dta_class.lesealldta () ;
			}

			sprintf ( bufh , "%8.0d",dta_gelesen.dta);
			int nCurSel = -1 ;
			nCurSel=((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBODTA))->FindString(nCurSel, bufh);
			if (nCurSel != CB_ERR)	// sollte dann immer passen ?!
			{
				((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBODTA))->SetCurSel(nCurSel) ;
				((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOTYP))->GetLBText(nCurSel, v_combotyp) ;
				((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOOPTION))->GetLBText(nCurSel, v_combooption) ;
				memcpy ( &dta , &dta_gelesen , sizeof ( struct DTA ));	// und wieder Ausgangszustand 
			}
		}
	}
	else
	{
		modif = dta_class.insertdta () ;
		CString szdta ;	
		szdta.Format("%8.0d  %s",dta.dta,dta.kun_krz1);
		((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBODTA))->AddString(szdta.GetBuffer(0));
//		((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBODTA))->SetCurSel(0);
		int nCurSel = -1 ;
		nCurSel=((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBODTA))->FindString(nCurSel, szdta.GetBuffer(0));
		if (nCurSel != CB_ERR)	// sollte dann immer passen ?!
		{
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBODTA))->SetCurSel(nCurSel) ;
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBODTA))->GetLBText(nCurSel, v_combodta);
	
			// dta.dta passt schon ?! 
			modif = dta_class.opendta();
			modif = dta_class.lesedta();
			komplettieredta ( modif ) ;
		}

	}
	if ( !modif )
		memcpy ( &dta_gelesen , &dta , sizeof ( struct DTA ));
	return ;

}

void CdtastammDlg::OnBnClickedOk()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	UpdateData(TRUE) ;
	writedta () ;
// 	OnOK();
}

void CdtastammDlg::OnEnKillfocusAdrnr()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	UpdateData(TRUE) ;
	sprintf(bufh,"%s",v_adrnr.GetBuffer(0)) ;
	long hilfe = atol ( bufh ) ;
	komplettiereadr ( hilfe ) ;
	UpdateData(FALSE) ;

}

void CdtastammDlg::OnCbnKillfocusCombooption()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	UpdateData(TRUE) ;
	bufh[0] = '\0' ;

	int nCurSel = ((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOOPTION))->GetCurSel();
	CString bufx;
	if ( nCurSel > -1 )
	{
		((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOOPTION))->GetLBText(nCurSel, bufx);

		sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
		int i = (int) strlen ( bufh );
		// fixe formatierung : max 3 stellen und danach evtl. blank und Bezeichnung
		if ( i )
		{
			bufh[3] = '\0' ;
			if ( bufh[2] > '9' ||bufh[2] < '0' )
			{
				bufh[2] = '\0' ;
			}	
			if ( bufh[1] > '9' ||bufh[1] < '0' )
			{
				bufh[1] = '\0' ;
			}
			if ( bufh[0] > '9' ||bufh[0] < '0' )
			{
				bufh[0] = '\0' ;
			}
			sprintf ( ptabn.ptwert ,"%s", bufh ) ;
			sprintf ( ptabn.ptitem ,"optiondesadv" ) ;
			i = ptabn_class.openptabn();
			i = ptabn_class.leseptabn();
		}
		else
		{

// darf es gar nicht geben			loeschen/ Error-Handling 
		}
	}
	else
	{
		sprintf ( bufh, "%s", v_combooption );
		int i = (int) strlen ( bufh );	// blank ist dann halt ne Fehleingabe .....
		// fixe formatierung : max 3 stellen und dann blank, danach Text ...
		if ( i )
		{
			bufh[3] = '\0' ;
			if ( bufh[2] > '9' ||bufh[2] < '0' )
			{
				bufh[2] = '\0' ;
			}	
			if ( bufh[1] > '9' ||bufh[1] < '0' )
			{
				bufh[1] = '\0' ;
			}
			if ( bufh[0] > '9' ||bufh[0] < '0' )
			{
				bufh[0] = '\0' ;
			}
		}
//		sprintf ( ptabn.ptwert ,"%s", bufh ) ;
//		sprintf ( ptabn.ptitem ,"dta_typ" ) ;
//		i = ptabn_class.openptabn();
//		i = ptabn_class.leseptabn();
// es darf nur bereits in der Box stehende Werte geben 
		nCurSel = -1 ;
		nCurSel=((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOOPTION))->FindString(nCurSel, bufh);
		if (nCurSel != CB_ERR)
		{
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOOPTION))->SetCurSel(nCurSel) ;
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOOPTION))->GetLBText(nCurSel, bufx);
			v_combooption = bufx ;
//			sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
		}
		else
		{
			MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
			komplettiereoption ( dta_gelesen.optiondesadv ) ;
		}
	}
	
	UpdateData(FALSE);
}
