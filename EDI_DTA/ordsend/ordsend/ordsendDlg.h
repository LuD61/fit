// ordsendDlg.h : Headerdatei
//

#pragma once
#include "afxwin.h"


// CordsendDlg-Dialogfeld
class CordsendDlg : public CDialog
{
// Konstruktion
public:
	CordsendDlg(CWnd* pParent = NULL);	// Standardkonstruktor

// Dialogfelddaten
	enum { IDD = IDD_ORDSEND_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV-Unterst�tzung


// Implementierung
protected:
	HICON m_hIcon;

	// Generierte Funktionen f�r die Meldungstabellen
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CEdit m_info;
public:
	CString v_info;
public:
	CEdit m_datvon;
public:
	CString v_datvon;
public:
	CEdit m_datbis;
public:
	CString v_datbis;
public:
	CComboBox m_combolief;
public:
	CString v_combolief;
public:
	CEdit m_mdnnr;
public:
	CEdit m_mdnname;
public:
	CString v_mdnname;
public:
	afx_msg void OnEnKillfocusMdnnr();
public:
	afx_msg void OnEnKillfocusDatvon();
public:
	afx_msg void OnEnKillfocusDatbis();
public:
	afx_msg void OnBnClickedOk();
public:
	afx_msg void OnBnClickedCancel();
public:
	afx_msg BOOL PreTranslateMessage(LPMSG) ;
	afx_msg BOOL NoListCtrl (CWnd *) ;	
	virtual int Abarbeiten ( void ) ;

public:
	afx_msg void OnCbnSelchangeCombolief();
public:
	afx_msg void OnCbnKillfocusCombolief();
public:
	short v_mdnnr;
public:
	CEdit m_info2;
public:
	CString v_info2;
};
