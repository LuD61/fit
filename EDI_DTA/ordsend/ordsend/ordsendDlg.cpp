// ordsendDlg.cpp : Implementierungsdatei
//
#include "stdafx.h"
#include "dbClass.h"
#include "ordsend.h"
#include "m_ordseg.h"
#include "adr.h"
#include "mdn.h"
#include "lief.h"
#include "dta.h"
#include "lief_bzg.h"
#include "a_bas.h"
#include "best_bel.h"
#include "ordsendDlg.h"

extern ADR_CLASS adr_class ;
extern MDN_CLASS mdn_class ;
extern LIEF_CLASS lief_class ;
extern BEST_KOPF_CLASS best_kopf_class ;
extern BEST_POS_CLASS best_pos_class ;
extern DTA_CLASS dta_class ;
extern LIEF_BZG_CLASS lief_bzg_class;
extern A_BAS_CLASS a_bas_class;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// Prototyping 

extern char * clipped ( char *) ;

int baudateiname ( char * odateiname  , FILE * * ppdatei) ; 
int schreibedatei (char * odateiname , FILE * * ppdatei, CString v_datvon, CString v_datbis) ;
void posindatei ( FILE * * ppdatei ,int * pbadcount , int * pkbadcount , int * pgescount, int * pkgescount ) ;


char pdateiname[375] ;


CEDIPUT cepu ;

int globgescount ;
int globkgescount ;


static char ausgabe[15] ;

char * eange ( char * eingabe)
{

int point1 ;
int wicht  ; 
int platz  ;
char testchar;
int summe  ;

  sprintf ( ausgabe, "%s",  eingabe );
  point1 = 0;
  summe  = 0;
  wicht  = 1;
  while ( point1 < 12 )
  {

	testchar = eingabe[point1] ;
//  platz    = testchar ;

	switch (testchar)
	{
		case '0' : platz = 0 ; break ;
	    case '1' : platz = 1 ; break ;
	    case '2' : platz = 2 ; break ;
	    case '3' : platz = 3 ; break ;
	    case '4' : platz = 4 ; break ;
	    case '5' : platz = 5 ; break ;
	    case '6' : platz = 6 ; break ;
	    case '7' : platz = 7 ; break ;
	    case '8' : platz = 8 ; break ;
	    case '9' : platz = 9 ; break ;
		default  : platz = 0 ; break ;
	};

      summe = summe + ( platz * wicht );
      if ( wicht == 1 )
	  {
		wicht = 3 ;
	  }
      else
	  {
		wicht = 1 ;
	  }
      point1 ++ ;
  }

  wicht = summe / 10  ;            // Quotient 
  platz = summe - ( wicht * 10 );  // modul 10 
  if ( platz == 0 )
  {
      ausgabe [12] = '0' ;
      ausgabe [13] = '\0' ;
  }
  else
  {
      sprintf ( ausgabe + 12, "%d", ( 10 - platz ));
  }
  return  ausgabe ;
}

DWORD ProcWaitExec (LPSTR prog, WORD SHOW_MODE, int x, int y, int cx, int cy)
/**
Fenster-Process starten und nicht auf Ende Warten.
**/
{
        STARTUPINFO sti;
        PROCESS_INFORMATION pi;
        DWORD ExitCode; 
        int ret;

        ZeroMemory (&sti, sizeof (sti));
        sti.cb = sizeof (sti);
        if (x != -1)
        {
                     sti.dwFlags |= STARTF_USEPOSITION;
                     sti.dwX = x;
                     sti.dwY = y;
        }

        if (cx != -1)
        {
                     sti.dwFlags |= STARTF_USESIZE;
                     sti.dwXSize = cx;
                     sti.dwYSize = cy;
        }
        sti.wShowWindow = SHOW_MODE;
        sti.dwFlags |= STARTF_USESHOWWINDOW;

        ret = CreateProcess (NULL,
                       prog,
                       NULL,
                       NULL,
                      (int) NULL,
                      (unsigned long) NULL,
                       NULL,
                       NULL,
                       &sti,
                       &pi);
        if (ret)
        {
                  CloseHandle (pi.hThread);
                  WaitForSingleObject (pi.hProcess, INFINITE);
        }
		else
        {
			      return -1;
        }				  
		GetExitCodeProcess (pi.hProcess, &ExitCode);
		while (ExitCode == STILL_ACTIVE)
		{
                  GetExitCodeProcess (pi.hProcess, &ExitCode);
		}
        CloseHandle (pi.hProcess);
        return ExitCode;
}


// CAboutDlg-Dialogfeld f�r Anwendungsbefehl "Info"

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialogfelddaten
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterst�tzung

// Implementierung
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CordsendDlg-Dialogfeld




CordsendDlg::CordsendDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CordsendDlg::IDD, pParent)
	, v_info(_T(""))
	, v_datvon(_T(""))
	, v_datbis(_T(""))
	, v_combolief(_T(""))
	, v_mdnname(_T(""))
	, v_mdnnr(0)
	, v_info2(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CordsendDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_INFO, m_info);
	DDX_Text(pDX, IDC_INFO, v_info);
	DDX_Control(pDX, IDC_DATVON, m_datvon);
	DDX_Text(pDX, IDC_DATVON, v_datvon);
	DDX_Control(pDX, IDC_DATBIS, m_datbis);
	DDX_Text(pDX, IDC_DATBIS, v_datbis);
	DDX_Control(pDX, IDC_COMBOLIEF, m_combolief);
	DDX_CBString(pDX, IDC_COMBOLIEF, v_combolief);
	DDX_Control(pDX, IDC_MDNNR, m_mdnnr);
	DDX_Control(pDX, IDC_MDNNAME, m_mdnname);
	DDX_Text(pDX, IDC_MDNNAME, v_mdnname);
	DDX_Text(pDX, IDC_MDNNR, v_mdnnr);
	DDV_MinMaxShort(pDX, v_mdnnr, 0, 9999);
	DDX_Control(pDX, IDC_INFO2, m_info2);
	DDX_Text(pDX, IDC_INFO2, v_info2);
}

BEGIN_MESSAGE_MAP(CordsendDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_EN_KILLFOCUS(IDC_MDNNR, &CordsendDlg::OnEnKillfocusMdnnr)
	ON_EN_KILLFOCUS(IDC_DATVON, &CordsendDlg::OnEnKillfocusDatvon)
	ON_EN_KILLFOCUS(IDC_DATBIS, &CordsendDlg::OnEnKillfocusDatbis)
	ON_BN_CLICKED(IDOK, &CordsendDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CordsendDlg::OnBnClickedCancel)
	ON_CBN_SELCHANGE(IDC_COMBOLIEF, &CordsendDlg::OnCbnSelchangeCombolief)
	ON_CBN_KILLFOCUS(IDC_COMBOLIEF, &CordsendDlg::OnCbnKillfocusCombolief)
END_MESSAGE_MAP()


// CordsendDlg-Meldungshandler

BOOL CordsendDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Hinzuf�gen des Men�befehls "Info..." zum Systemmen�.

	// IDM_ABOUTBOX muss sich im Bereich der Systembefehle befinden.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Symbol f�r dieses Dialogfeld festlegen. Wird automatisch erledigt
	//  wenn das Hauptfenster der Anwendung kein Dialogfeld ist
	SetIcon(m_hIcon, TRUE);			// Gro�es Symbol verwenden
	SetIcon(m_hIcon, FALSE);		// Kleines Symbol verwenden


	CString szlief ;

	lief.mdn = mdn.mdn  ;
	int sqlstat = lief_class.openalllief ();
	sqlstat = lief_class.lesealllief();
	while(!sqlstat)
	{
	
			szlief.Format("%8.0d  %s",lief.lief_s,adr.adr_krz);
				// hier haben wir jetzt die Liefreranten und k�nnen diese in die ListBox einf�gen
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOLIEF))->AddString(szlief.GetBuffer(0));
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOLIEF))->SetCurSel(0);
	
		sqlstat = lief_class.lesealllief () ;
	}

	return TRUE;  // Geben Sie TRUE zur�ck, au�er ein Steuerelement soll den Fokus erhalten
}

void CordsendDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// Wenn Sie dem Dialogfeld eine Schaltfl�che "Minimieren" hinzuf�gen, ben�tigen Sie 
//  den nachstehenden Code, um das Symbol zu zeichnen. F�r MFC-Anwendungen, die das 
//  Dokument/Ansicht-Modell verwenden, wird dies automatisch ausgef�hrt.

void CordsendDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Ger�tekontext zum Zeichnen

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Symbol in Clientrechteck zentrieren
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Symbol zeichnen
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// Die System ruft diese Funktion auf, um den Cursor abzufragen, der angezeigt wird, w�hrend der Benutzer
//  das minimierte Fenster mit der Maus zieht.
HCURSOR CordsendDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

char bufh[256] ;
void CordsendDlg::OnEnKillfocusMdnnr()
{
	short mdnsave = v_mdnnr ;
	UpdateData (TRUE) ;
	int i = m_mdnnr.GetLine(0,bufh,500);
	bufh[i] = '\0' ;	// 161210
	if (i)	mdn.mdn = (short) atoi ( bufh );
	else mdn.mdn = -2 ;
	mdn_class.openmdn();
	if (! mdn_class.lesemdn())
	{
		adr.adr = mdn.adr ;
		i = adr_class.openadr () ;
		i = adr_class.leseadr () ;
		if (!i) 
		{
			v_mdnname.Format("%s",_T(adr.adr_krz));
		}
		else
		{
			v_mdnname.Format("              ");
			MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
			PrevDlgCtrl();
		}
	}
	else	// fehlerhafte Eingabe
	{
		v_mdnname.Format("              ");
		MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
		PrevDlgCtrl();
	}

	if ( mdnsave != v_mdnnr )
	{	// Falls sich was geaendert hat, erst mal alles rein in die Combobox .....

	// Delete every item from the combo box.

		for (int i = ((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOLIEF))->GetCount()-1; i >= 0; i--)
		{
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOLIEF))->DeleteString( i );
		}

		CString szlief ;

		lief.mdn = mdn.mdn  ;
		int sqlstat = lief_class.openalllief ();
		sqlstat = lief_class.lesealllief();

		while(!sqlstat)
		{
			szlief.Format("%8.0d  %s",lief.lief_s,adr.adr_krz);
			// hier haben wir jetzt die Liefreranten und k�nnen diese in die ListBox einf�gen
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOLIEF))->AddString(szlief.GetBuffer(0));
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOLIEF))->SetCurSel(0);

			sqlstat = lief_class.lesealllief () ;
		}
	}
	UpdateData (FALSE) ;

}




BOOL datumcheck(char * bufi )
{
	int i = (int) strlen(bufi) ;
	int hil1, hil2 ;
	BOOL j = FALSE ;

	char hil5 [5] ;
	if ( i != 6 && i != 8 && i != 10 )
	{
		bufi[0] = '\0' ;
		return TRUE ;
	}
	if ( i == 6 )	// nur ddmmyy erlaubt 
	{
		bufi[10] = '\0' ;
		bufi[ 9] = bufi[5] ;
		bufi[ 8] = bufi[4] ;
		if ( bufi[ 8] > '5' )
		{
			bufi[ 6] = '1' ;
			bufi[ 7] = '9' ;
		}
		else
		{
			bufi[ 6] = '2' ;
			bufi[ 7] = '0' ;
		}
		bufi[ 5] = '.' ;
		bufi[ 4] = bufi[3] ;
		bufi[ 3] = bufi[2] ;
		bufi[ 2] = '.' ;
		i = 10 ;
		j = TRUE ; 
	}
	if ( i == 8 )	// nur dd,mm,yy erlaubt 
	{
		bufi[10] = '\0' ;
		bufi[ 9] = bufi[7] ;
		bufi[ 8] = bufi[6] ;
		if ( bufi[ 8] > '5' )
		{
			bufi[ 6] = '1' ;
			bufi[ 7] = '9' ;
		}
		else
		{
			bufi[ 6] = '2' ;
			bufi[ 7] = '0' ;
		}
		bufi[ 5] = '.' ;
		bufi[ 2] = '.' ;
		i = 10 ;
		j = TRUE ;
	}
	if ( i == 10 )	// hier kommt nix anderes mehr an ....  
	{
		if (   bufi[0] > '9' || bufi[0] < '0' 
			|| bufi[1] > '9' || bufi[1] < '0'
			|| bufi[3] > '9' || bufi[3] < '0'
			|| bufi[4] > '9' || bufi[4] < '0'
			|| bufi[6] > '9' || bufi[6] < '0'
			|| bufi[7] > '9' || bufi[7] < '0'
			|| bufi[8] > '9' || bufi[8] < '0'
			|| bufi[9] > '9' || bufi[9] < '0'
			)
		{

			bufi[0] = '\0' ;
			return TRUE ;
		}
		hil5[0] = bufi[3] ;
		hil5[1] = bufi[4] ;
		hil5[2] = '\0' ;
		hil1 = atoi (hil5);
		if ( hil1 > 12 || hil1 < 1 )
		{
			bufi[0] = '\0' ;
			return TRUE ;
		}

		hil5[0] = bufi[0] ;
		hil5[1] = bufi[1] ;
		hil5[2] = '\0' ;
		hil2 = atoi (hil5);
		if ( hil2 > 31 || hil1 < 1 )
		{
			bufi[0] = '\0' ;
			return TRUE ;
		}
		switch ( hil1 )
		{
		case  4 :
		case  6 :
		case  9 :
		case 11 :
			if ( hil2 > 30 )
			{
				bufi[0] = '\0' ;
				return TRUE ;
			}
			break ;
		case 2 :

			hil5[0] = bufi[6] ;
			hil5[1] = bufi[7] ;
			hil5[2] = bufi[8] ;
			hil5[3] = bufi[9] ;
			hil5[4] = '\0' ;
			hil1    = atoi(hil5) ;
			if ( hil2 > 28 )	// IM Jahr 2000 geht dieser Check schief .....
			{
				if ( ! ( hil1 % 4) )
				{
					if ( hil2 > 29 )
					{
						bufi[0] = '\0' ;
						return TRUE ;
					}
				}
				else
				{
					bufi[0] = '\0' ;
					return TRUE ;
				};
			}
			break ;
		}
	}
	return j ;
}


void CordsendDlg::OnEnKillfocusDatvon()
{
	UpdateData(TRUE);
	int i = m_datvon.GetLine(0,bufh,500) ;
	bufh[i] = '\0' ;	// 161210

	if (i)
	{
		if (datumcheck(bufh))
		{
			v_datvon.Format("%s",_T(bufh));
			UpdateData (FALSE) ;
		}
	}

}

void CordsendDlg::OnEnKillfocusDatbis()
{
		UpdateData(TRUE);
	int i = m_datbis.GetLine(0,bufh,500) ;
	bufh[i] = '\0' ;	// 161210
	if (i)
	{
		if (datumcheck(bufh))
		{
			v_datbis.Format("%s",_T(bufh));
			UpdateData (FALSE) ;
		}
	}

}

void CordsendDlg::OnBnClickedOk()
{
	UpdateData(TRUE) ;

	int i = Abarbeiten () ;
	UpdateData(FALSE) ;
//	OnOK();
}

void CordsendDlg::OnBnClickedCancel()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	OnCancel();
}

BOOL CordsendDlg::PreTranslateMessage(LPMSG lpMsg)
{
	CWnd *cWnd;
	 if (lpMsg->message == WM_KEYDOWN)
	 {
		 switch (lpMsg->wParam)
			 {
		        case VK_RETURN :
					cWnd = GetFocus ();
			           if(cWnd == GetDlgItem (IDOK))
							return CDialog::PreTranslateMessage(lpMsg);

			           if(cWnd == GetDlgItem (IDCANCEL))
					   {
							OnCancel ();
 							return TRUE;
					   }

/* --->
			           if(cWnd == GetDlgItem (IDC_KUNN))
					   {	Read (); return TRUE; }
                       else if (cWnd == GetDlgItem (IDC_MDN))
                       { if (ReadMdn () == FALSE)
                       { return TRUE;
                       } }
 < ----- */
			   NextDlgCtrl();
						return TRUE;

			 case VK_F5 :
                     OnCancel ();
 			         return TRUE;
/* ----->
			 case VK_F7 :
                     OnDelete ();
 			         return TRUE;
             case VK_F9 :
		             if (OnF9 ())
                     {
                         return TRUE;
                     }
                     break;
		     case VK_F6 :
                     OnFree ();
				     return TRUE;
 <----- */

/* ---->
			 case VK_F12 :
					UpdateData( TRUE ) ;
					ladevartodaba();
					if (  tou_class.testupdtou() )
						tou_class.inserttou () ;
					else
						tou_class.updatetou () ;

					v_edithtour = 0 ;
					tou.tou = 0 ;
					UpdateData(FALSE) ;
					m_edithtour.EnableWindow (TRUE) ;
					m_buttonhtour.EnableWindow ( TRUE) ;
					m_edithtour.ModifyStyle (0, WS_TABSTOP,0) ;
					m_buttonhtour.ModifyStyle ( 0, WS_TABSTOP,0) ;
					m_edithtour.SetFocus () ;	


//                     OnOK ();
				     return TRUE;
< ----- */


 		    case VK_DOWN :
                     if (NoListCtrl (GetFocus ()))
                     {
						NextDlgCtrl();
						return TRUE;
					
                     }
				     break;

			case VK_UP :
                     if (NoListCtrl (GetFocus ()))
                     {
						PrevDlgCtrl();
	 					cWnd = GetFocus ();
					 	GetNextDlgTabItem(cWnd ,FALSE) ;
						return TRUE;
                     }
				     break;
		 }
	 }

return CDialog::PreTranslateMessage(lpMsg);
}

BOOL CordsendDlg::NoListCtrl (CWnd *cWnd)
{
    if (((CComboBox *) cWnd->GetParent ())->GetCount () > 1)
    {
        return FALSE;
    }
    else if (((CComboBox *) cWnd)->GetCount () > 1)
    {
        return FALSE;
    }
    else if (((CListBox *) cWnd)->GetCount () > 1)
    {
        return FALSE;
    }
    return TRUE;
}


void CordsendDlg::OnCbnSelchangeCombolief()
{
	UpdateData(TRUE) ;

	int nCurSel = ((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOLIEF))->GetCurSel();
	CString bufc;
	((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOLIEF))->GetLBText(nCurSel, bufc);

	sprintf(bufh,"%s",bufc.GetBuffer(0)) ;

	// fixe formatierung : 8 stellen und 2 blanks , danach folgt ein text
	int i = (int)strlen ( bufh) ;
	if ( i )
	{
		if ( i < 9 )
		{
			bufh[i] = '\0' ;
		}
		else
		{
			bufh[9] = '\0' ;
		}
		lief.lief_s = atol ( bufh ) ;
		lief.mdn = mdn.mdn ;
		i = lief_class.openlief();
		i = lief_class.leselief();
	}

}

void CordsendDlg::OnCbnKillfocusCombolief()
{
	UpdateData(TRUE) ;
	bufh[0] = '\0' ;

	int nCurSel = ((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOLIEF))->GetCurSel();
	CString bufx;
	if ( nCurSel > -1 )
	{
		((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOLIEF))->GetLBText(nCurSel, bufx);

		sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
		int i = (int) strlen ( bufh );
		// fixe formatierung : 8 stellen und 2 blanks , danach folgt ein text
		if ( i )
		{
			if ( i < 9 )
			{
				bufh[i ] = '\0' ;
			}
			else
			{
				bufh[9] = '\0' ;
			}
			lief.lief_s = atol ( bufh ) ;
			i = lief_class.openlief();
			i = lief_class.leselief();
			if ( i )
			{
				v_combolief.Format("                   ");
			}
			else
			{
/* -----> bedingte Freischaltung wieterer Dialog-Felder
				if ( dta.reli == 1 )	// 130706
				{
					m_edit2.EnableWindow( TRUE );
					m_edit5.EnableWindow( FALSE);

				}
				else
				{	
						// hier vorerst auch dta.reli == 2 mit dabei ...
					m_edit2.EnableWindow( FALSE );
					m_edit5.EnableWindow( TRUE );
				}
< ----- */
			}

		}
		else
		{
				v_combolief.Format("                   ");
		}
	}
	else
	{
		sprintf ( bufh, "%s", v_combolief );
		int i = (int) strlen ( bufh );
		// fixe formatierung : 8 stellen und 2 blanks , danach folgt ein text
		if ( i )
		{
			if ( i < 9 )
			{
				bufh[i ] = '\0' ;
			}
			else
			{
				bufh[9] = '\0' ;
			}
		}
		long hilfe = atol ( bufh ) ;
		nCurSel = -1 ;
		sprintf ( bufh, "%8.0d", hilfe );
		nCurSel=((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOLIEF))->FindString(nCurSel, bufh);
	
		if (nCurSel != CB_ERR)
		{
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOLIEF))->SetCurSel(nCurSel) ;
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOLIEF))->GetLBText(nCurSel, bufx);
			sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
			i = (int) strlen ( bufh );
			// fixe formatierung : 8 stellen und 2 blanks , danach folgt ein text
			if ( i )
			{
				if ( i < 9 )
				{
					bufh[i] = '\0' ;
				}
			}
			else
			{
				bufh[9] = '\0' ;
			}
			lief.lief_s = atol ( bufh ) ;
			lief.mdn = mdn.mdn ;
			i = lief_class.openlief();
			i = lief_class.leselief();
			if ( i )
			{
				v_combolief.Format("                   ");
			}
			else
			{
				v_combolief.Format(bufx.GetBuffer(0));
/* Aktualisierung bedingetr Dialog-Felder
				if ( dta.reli == 1 )
				{
					m_edit2.EnableWindow( TRUE );
					m_edit5.EnableWindow( FALSE);

				}
				else
				{
						// hier vorerst auch dta.reli == 2 mit dabei
					m_edit2.EnableWindow( FALSE );
					m_edit5.EnableWindow( TRUE );
				}
< ------ */

			}

		}
		else
		{
			v_combolief.Format("                   ");
		}
	}
	UpdateData(FALSE);

}

// Dieses ist die gesamte Abarbeitungsroutine


int CordsendDlg::Abarbeiten ( void )
{

	FILE * prdatei = NULL ;
	FILE * * ppdatei ;
	ppdatei = & prdatei ;

	char odateiname[357] ;

	int retcode = 0 ;

	bufh[0] = '\0' ;
// Zuerst Lieferant und dta lesen 
	int nCurSel = ((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOLIEF))->GetCurSel();
	CString bufx;
	if ( nCurSel > -1 )
	{
		((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOLIEF))->GetLBText(nCurSel, bufx);
		sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
		int i = (int) strlen ( bufh );
		// fixe formatierung : 8 stellen und 2 blanks , danach folgt ein text
		if ( i )
		{
			if ( i < 9 )
			{
				bufh[i ] = '\0' ;
			}
			else
			{
				bufh[9] = '\0' ;
			}
			lief.lief_s = atol ( bufh ) ;
			i = lief_class.openlief();
			i = lief_class.leselief();
			if ( i )
			{
				MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
				v_combolief.Format("                   ");
				return 0 ;
			}
			else
			{
// bedingte Freischaltung weiterer Dialog-Felder
			}

		}
		else
		{
				MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
				v_combolief.Format("                   ");
				return 0 ;
		}
	}
	else
	{
		sprintf ( bufh, "%s", v_combolief );
		int i = (int) strlen ( bufh );
		// fixe formatierung : 8 stellen und 2 blanks , danach folgt ein text
		if ( i )
		{
			if ( i < 9 )
			{
				bufh[i ] = '\0' ;
			}
			else
			{
				bufh[9] = '\0' ;
			}
		}
		long hilfe = atol ( bufh ) ;
		nCurSel = -1 ;
		sprintf ( bufh, "%8.0d", hilfe );
		nCurSel=((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOLIEF))->FindString(nCurSel, bufh);
	
		if (nCurSel != CB_ERR)
		{
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOLIEF))->SetCurSel(nCurSel) ;
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBOLIEF))->GetLBText(nCurSel, bufx);
			sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
			i = (int) strlen ( bufh );
			// fixe formatierung : 8 stellen und 2 blanks , danach folgt ein text
			if ( i )
			{
				if ( i < 9 )
				{
					bufh[i] = '\0' ;
				}
			}
			else
			{
				bufh[9] = '\0' ;
			}
			lief.lief_s = atol ( bufh ) ;
			lief.mdn = mdn.mdn ;
			i = lief_class.openlief();
			i = lief_class.leselief();
			if ( i )
			{
				MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
				v_combolief.Format("                   ");
				return 0 ;
			}
			else
			{
				v_combolief.Format(bufx.GetBuffer(0));
// Aktualisierung bedingetr Dialog-Felder
			}

		}
		else
		{
				MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
				v_combolief.Format("                   ");
				return 0 ;
		}
	}

	retcode = baudateiname (odateiname, ppdatei) ;
	if ( !retcode )
	{
		MessageBox("Datei kann nicht gechrieben werden", " ", MB_OK|MB_ICONSTOP);
		return 0 ;	// Datei kann nicht erstellt werden
	}

	retcode = schreibedatei (odateiname,ppdatei ,v_datvon,v_datbis) ;
// Folgende retcodes : 100 : keine Daten vorhanden
//						<0 : Schreib-Probleme
//						0 : basisdaten ganz falsch
//                      1 : erfolgreich mit Fehlern
//						2 : alles korrekt
	if ( retcode < 0  )
	{
		if ( * ppdatei != NULL ) fclose ( * ppdatei ) ;

		MessageBox("Probleme beim Erstellen der Datei", " ", MB_OK|MB_ICONSTOP);
		v_info.Format( " " ) ;
		v_info2.Format( "Probleme beim Erstellen der Datei" ) ;
		return  0 ;
	}
	if ( retcode == 100 )
	{
		if ( * ppdatei != NULL ) fclose ( * ppdatei ) ;

		MessageBox("Keine Daten vorhanden", " ", MB_OK|MB_ICONSTOP);
		v_info.Format( " " ) ;
		v_info2.Format( "Keine Daten vorhanden" ) ;
		return  0 ;
	}
	if ( retcode == 0 )
	{
		if ( * ppdatei != NULL )
		{
			fclose ( * ppdatei ) ;
			char s1 [256] ;
				sprintf ( s1, "notepad.exe \"%s\" " , pdateiname );
				int ex_code = ProcWaitExec( (char *)s1, SW_SHOW, -1, 0, -1, 0 );
		}
		* ppdatei = NULL ;
		MessageBox("Basisdaten sind nicht korrekt", " ", MB_OK|MB_ICONSTOP);
		v_info.Format (" ") ;
		v_info2.Format ("Basisdaten sind nicht korrekt") ;
		return 0 ;
	}

// if ( retcoode == 1 ||retcode == 2 )
	v_info.Format("%s", odateiname ) ;
	v_info2.Format("%d Beleg(e), %d Position(en) ",globkgescount, globgescount ) ;

	if ( * ppdatei != NULL )
	{
		fclose ( * ppdatei ) ;
		if ( retcode == 1 )
		{
				char s2 [256] ;
				sprintf ( s2, "notepad.exe \"%s\" " , pdateiname );
				int ex_code = ProcWaitExec( (char *)s2, SW_SHOW, -1, 0, -1, 0 );
		}
	}
	* ppdatei = NULL ;

	return 1 ;
}

int baudateiname ( char * odateiname , FILE * * ppdatei ) 
{

// char pdateiname[375] ;

// nur 9999 laufende Nummern im Dateiname, danach geht es wieder von vorne los ?!
long inummer ;
    if ( dta.nummer > 0 )
	{ /* es passt bereits alles .... */ } 
    else       // schliesst null mit ein
			dta.nummer = 1 ;

    inummer = dta.nummer ;

	dta.nummer = inummer + 1 ;	// da steht jetzt schon die neue Nummer

// Achtung : bei den ROSI-DESADV ist der Dateiname immer eins weiter als die dta_referenz-Nummer ;-)
// Achtung : bei den ROSI-DESADV ist die dta-refrenz immer nur bis 999 und geht dann wieder von vorne los 

	while ( inummer > 999999 )
	{
		inummer -= 1000000 ;
	}


	while ( inummer > 99999 )
	{
		inummer -= 100000 ;
	}

    while ( inummer > 9999 )
	{
		inummer -= 10000 ;
	}

    odateiname[0] = '\0' ;
    char * p = getenv ("EDI") ;
	if ( p == NULL )
          p = getenv ( "BWS" ) ;
    sprintf ( odateiname , "%s\\ORDSEND%04d.txt" , p, inummer ) ;

	pdateiname[0] = '\0' ;
    p = getenv ("TMPPATH") ;
	if ( p == NULL )
          p = getenv ( "BWS" ) ;
    sprintf ( pdateiname , "%s\\ORDSEND%04dProt.txt" , p, inummer ) ;

	* ppdatei = fopen ( pdateiname, "w" ) ;
	if ( * ppdatei == NULL )	return 0 ;

	return 1 ;
}

int schreibedatei (char * odateiname , FILE * * ppdatei , CString v_datvon , CString v_datbis)
{

int k ;
int ref_nr ;

int gescount ;
int badcount ;
int kgescount ;
int kbadcount ;

char filhilfe[10] ;
char fililn[20] ;

	gescount  = 0 ;
    badcount  = 0 ;
    kgescount = 0 ;
    kbadcount = 0 ;
	best_kopf.mdn =  mdn.mdn ;
	best_kopf.lief_s = lief.lief_s ;
	best_kopf_class.openbest_kopf( v_datvon, v_datbis ) ;

	int dgefunden = 0 ;

	while ( ! best_kopf_class.lesebest_kopf ()) 
	{
	if ( * ppdatei != NULL )
		fprintf ( * ppdatei , "Auftrag : Fil.: %d Nr .: %d  \n" , best_kopf.fil, best_kopf.best_blg ) ;

		dgefunden ++ ;
		if ( dgefunden == 1 )
		{

			ref_nr = 1  ;
			k = cepu.Kopfteil ( odateiname, dta.nummer - 1  ) ;
			if (k) return -1 ;
			dta_class.updatedta () ; 
		}
		k = cepu.UNH_segment(ref_nr , "ORDERS" ) ; 
        if ( k )	return  -1 ;
        k = cepu.BGM_segment("320",best_kopf.best_blg,0 ) ;

        if ( k ) return -1 ;
		// bestelldatum
		k = cepu.DTM_segment( "137", best_kopf.best_term,102,"" ) ;
 		if ( k ) return -1 ;
		// Lieferdatum
		k = cepu.DTM_segment("2",best_kopf.lief_term,102,"" ) ;
		if ( k ) return -1 ;
        // Byer-ILN
        k = cepu.NAD_segment("BY",mdn.iln /*eigen_iln*/ ,0 ) ;
        if ( k ) return -1 ;
        // Lieferanten-ILN
        if ((strlen ( clipped (lief.iln )) > 10 ))
			k = cepu.NAD_segment("SU",lief.iln , 0 ) ;
        else        // Notbremse
			k = cepu.NAD_segment("SU",dta.iln , 0 ) ;

        if ( k ) return -1 ;
        // Filial-ILN
        sprintf ( fililn , "%s", mdn.iln ) ;	//  eigen_iln
		sprintf ( filhilfe , "%05d" , best_kopf.fil ) ;
		fililn[7] = filhilfe[0] ;	
		fililn[8] = filhilfe[1] ;	
		fililn[9] = filhilfe[2] ;	
		fililn[10] = filhilfe[3] ;	
		fililn[11] = filhilfe[4] ;
		fililn[12] = '\0' ;
        sprintf ( fililn ,"%s", eange ( fililn )) ; // spaeter mal korrekt aus dem Filialstamm lesen

        k = cepu.NAD_segment("DP",fililn ,0 ) ; 
 
/* ---->
        if not k
        // Erst mal fix auf "Strassentransport"  gesetzt
            perform TDT_segment("20++30" ) returning (k)
        end

< ----- */

        if (k) return -1 ;

/* ---->
        perform protokoll ( 0 ,
            "Bestell-Nr:" # PICTURE ( best_kopf.best_blg, "-------&")
          # " zum " # PICTURE(best_kopf.lief_term,"dd.mm.yyyy")
          # " fuer Fil " # PICTURE (best_kopf.fil , "--&" )
           )
< ------ */

         posindatei ( ppdatei , &badcount, &kbadcount, &gescount, &kgescount ) ;
//                 returning ( badcount, kbadcount, gescount, kgescount )

        k = cepu.UNT_segment(ref_nr ) ;
        ref_nr ++ ;
	}
	//  erase ( 12 , 3)
	globgescount = gescount ;
	globkgescount = kgescount ;
	if ( dgefunden )
	{
		k = cepu.Fussteil () ;
	}
	if ( ! dgefunden )
		return 100 ;

	if ( badcount ) return 1 ;	// korrekt mit bugs
	return 2 ;					// total korrekt
}

void posindatei ( FILE * * ppdatei , int * pbadcount , int * pkbadcount , int * pgescount, int * pkgescount ) 
{
    int ik ;
//    field ime_einh_kun  smallint
//    field iinh decimal ( 12,3)
    int pme_einh ;
//    field pauf_einh smallint
//    field cpscounter integer    // laufende cps-Numerierung
    int bel_pos ;         // lfd. im Beleg

//    field GesamtMenge decimal (12,3)
//    field GesamtStueck integer
//    field GesamtTyp   char ( 10 )

	bel_pos = 1 ;             // Start je Beleg    

    (* pkgescount )++ ;
    best_pos_class.openbest_pos () ;
    while ( ! best_pos_class.lesebest_pos() )
	{
		(* pgescount) ++ ;
		lief_bzg.a = best_pos.a ;
		sprintf ( lief_bzg.lief ,"%s", lief.lief ) ;
		lief_bzg.mdn = best_kopf.mdn  ;
		lief_bzg.fil = 0 ;
		a_bas.a = best_pos.a ;

		a_bas_class.opena_bas () ;
		int a_bas_stat = a_bas_class.lesea_bas () ;

		lief_bzg_class.openlief_bzg_a () ;
		int lief_bzg_stat = lief_bzg_class.leselief_bzg_a () ;
			
		if ( (strlen ( clipped (lief_bzg.ean)) > 7) && (! lief_bzg_stat) )	
	       ik = cepu.LIN_segment (bel_pos, lief_bzg.ean) ;
        else
		{
           ik = cepu.LIN_segment (bel_pos, "000") ;      // fix auf Konstante geknipst
			if (  * ppdatei != NULL )
				fprintf (  * ppdatei, "EAN fehlt : Artikel : %1.0f \n" ,best_pos.a ) ;
				(* pbadcount ) ++ ;
		}
        bel_pos ++ ;

        ik = cepu.PIA_segment ((long)best_pos.a, best_pos.lief_best,"") ;
                                 //  kein Typ

        char chilfe [100] ;
		sprintf ( chilfe, "%s %s", clipped(a_bas.a_bz1), clipped (a_bas.a_bz2) ) ;
        ik = cepu.IMD_segment ( "F", "" , chilfe ) ;

        if ( best_pos.me_einh == 2 ) 
			pme_einh = 2 ;
        else
           pme_einh = 1 ;

        ik = cepu.QTY_segment ( "21", best_pos.me, pme_einh ) ;

	}
 
    return ;

   }
