#include "stdafx.h"
#include "DbClass.h"
#include "best_bel.h"

struct BEST_KOPF best_kopf,  best_kopf_null;
struct BEST_POS best_pos,  best_pos_null;

extern DB_CLASS dbClass;

BEST_KOPF_CLASS best_kopf_class ;
BEST_POS_CLASS best_pos_class ;
void setdatum (  TIMESTAMP_STRUCT * ziel , CString quelle )
{
// Input zwingend "dd.mm.yyyy" oder blank

    char tag[] = {"01"};
    char mon[]  = {"01"};
    char jahr[]  = {"2000"};
   	int len = quelle.GetLength ();
	 if (len >=2)
	 {
		 memcpy (tag, quelle.Mid (0, 2), 2);
	 }
	 if (len >=5)
	 {
			 memcpy (mon, quelle.Mid (3, 2), 2);
	 }
	 if (len >=10)
	 {
		 memcpy (jahr, quelle.Mid (6, 4), 4);
	 }
	 else
	 {
		if (len >=10)
		{
			memcpy (jahr + 2 , quelle.Mid (6, 2), 2);
		}
	 }
	 ziel->year = atoi ( jahr ) ; if ( ziel->year > 2099 || ziel->year < 1900 ) ziel->year = 1999 ;
	 ziel->day = atoi ( tag ) ;   if ( ziel->day > 31 || ziel->day < 1 ) ziel->day = 1 ;
	 ziel->month = atoi ( mon ) ; if ( ziel->month > 12 || ziel->month < 1 ) ziel->month = 1 ;
}

int BEST_KOPF_CLASS::lesebest_kopf (void)
{
	int di = dbClass.sqlfetch (readcursor);
	return di;
}

int BEST_KOPF_CLASS::openbest_kopf (CString  von_datum, CString bis_datum )
{

		if ( readcursor < 0 ) prepare ();

	setdatum ( &vondatum , von_datum ) ;
 	setdatum ( &bisdatum , bis_datum ) ;
		
         return dbClass.sqlopen (readcursor);
}


void BEST_KOPF_CLASS::prepare (void)
{

	dbClass.sqlin ((long *) &best_kopf.lief_s, SQLLONG, 0);
	dbClass.sqlin ((TIMESTAMP_STRUCT *) &vondatum, SQLTIMESTAMP, 26);
	dbClass.sqlin ((TIMESTAMP_STRUCT *) &bisdatum, SQLTIMESTAMP, 26);
	dbClass.sqlin ((short *) &best_kopf.mdn, SQLSHORT, 0);

	dbClass.sqlout ((short *) &best_kopf.bearb_stat, SQLSHORT, 0) ;
	dbClass.sqlout ((long *)  &best_kopf.best_blg, SQLLONG, 0) ;
	dbClass.sqlout ((TIMESTAMP_STRUCT *) &best_kopf.best_term, SQLTIMESTAMP, 26) ;  
	dbClass.sqlout ((long *)  &best_kopf.best_txt, SQLLONG, 0) ; 
	dbClass.sqlout ((short *) &best_kopf.fil, SQLSHORT, 0) ; 
	
	dbClass.sqlout ((long *)  &best_kopf.lfd, SQLLONG, 0) ;  
	dbClass.sqlout ((char *)   best_kopf.lief, SQLCHAR ,18 ) ;
	dbClass.sqlout ((long *)  &best_kopf.lief_s, SQLLONG, 0) ;
	dbClass.sqlout ((TIMESTAMP_STRUCT *) &best_kopf.lief_term, SQLTIMESTAMP, 26) ;
	dbClass.sqlout ((short *) &best_kopf.mdn, SQLSHORT, 0) ; 
	
	dbClass.sqlout ((TIMESTAMP_STRUCT *) &best_kopf.we_dat, SQLTIMESTAMP, 26) ;   
	dbClass.sqlout ((char *)   best_kopf.pers_nam, SQLCHAR , 10 ) ;
	dbClass.sqlout ((long *)  &best_kopf.fil_adr, SQLLONG, 0) ;
	dbClass.sqlout ((long *)  &best_kopf.lief_adr, SQLLONG, 0) ;  
	dbClass.sqlout ((char *)   best_kopf.lieferzeit, SQLCHAR , 7);

	dbClass.sqlout ((short *) &best_kopf.waehrung, SQLSHORT, 0) ; 
	dbClass.sqlout ((short *) &best_kopf.delstatus, SQLSHORT, 0) ;
	dbClass.sqlout ((short *) &best_kopf.kontrakt_kz, SQLSHORT, 0) ; 
	dbClass.sqlout ((short *) &best_kopf.kontrakt_nr, SQLSHORT, 0) ;
	dbClass.sqlout ((TIMESTAMP_STRUCT *) &best_kopf.kontrakt_von, SQLTIMESTAMP, 26) ;

	dbClass.sqlout ((TIMESTAMP_STRUCT *) &best_kopf.kontrakt_bis, SQLTIMESTAMP, 26) ;  
	dbClass.sqlout ((char *)   best_kopf.freifeld1, SQLCHAR , 40 ) ;
	dbClass.sqlout ((char *)   best_kopf.freifeld2, SQLCHAR , 40 ) ;
	dbClass.sqlout ((char *)   best_kopf.freifeld3, SQLCHAR , 40 ) ;
	dbClass.sqlout ((char *)   best_kopf.lief_ref_nr, SQLCHAR ,20 ) ;

	dbClass.sqlout ((short *) &best_kopf.zahl_kond, SQLSHORT, 0) ;      
	dbClass.sqlout ((short *) &best_kopf.lgr, SQLSHORT, 0) ;  


		readcursor = (short) dbClass.sqlcursor ("select "
	"  bearb_stat ,best_blg ,best_term ,best_txt , fil "
	" ,lfd ,lief ,lief_s ,lief_term ,mdn "
	" ,we_dat ,pers_nam ,fil_adr ,lief_adr ,lieferzeit "
	" ,waehrung ,delstatus ,kontrakt_kz ,kontrakt_nr ,kontrakt_von "
	" ,kontrakt_bis ,freifeld1 ,freifeld2 ,freifeld3 ,lief_ref_nr "
	" ,zahl_kond ,lgr "
	" from best_kopf where lief_s = ? and lief_term between ? and ?  "
//		" and fil = 0 and mdn = ? order by best_blg ") ;
		" and mdn = ? order by best_blg ") ;

}

int BEST_POS_CLASS::lesebest_pos (void)
{
	int di = dbClass.sqlfetch (readcursor);
	return di;
}

int BEST_POS_CLASS::openbest_pos (void )
{

	if ( readcursor < 0 ) prepare ();
         return dbClass.sqlopen (readcursor);
}

void BEST_POS_CLASS::prepare (void)
{

// Achtung : haengt hier immer am Kopf dran !!!!!!!!!

	dbClass.sqlin((long  *) &best_kopf.best_blg ,SQLLONG , 0 )   ;
	dbClass.sqlin((char  *)  best_kopf.lief ,SQLCHAR , 17 )   ;
	dbClass.sqlin((short *) &best_kopf.mdn ,SQLSHORT , 0 )   ;
	dbClass.sqlin((short *) &best_kopf.fil ,SQLSHORT , 0 )   ;

	dbClass.sqlout((double *) &best_pos.a, SQLDOUBLE,0) ;				//	decimal(13,0)
	dbClass.sqlout((double *) &best_pos.anz_einh, SQLDOUBLE ,0) ;		//	decimal(8,3)
	dbClass.sqlout((short  *) &best_pos.bearb_stat, SQLSHORT ,0) ;
	dbClass.sqlout((long   *) &best_pos.best_blg, SQLLONG ,0 ) ;
	dbClass.sqlout((TIMESTAMP_STRUCT *) &best_pos.best_term, SQLTIMESTAMP,26 ) ;

	dbClass.sqlout((short  *) &best_pos.fil, SQLSHORT ,0) ;
	dbClass.sqlout((double *) &best_pos.inh, SQLDOUBLE,0) ;			//	decimal(8,3)
	dbClass.sqlout((char   *)  best_pos.lief,SQLCHAR, 18 ) ;
	dbClass.sqlout((char   *)  best_pos.lief_best,SQLCHAR,18) ;
	dbClass.sqlout((long   *) &best_pos.lief_s ,SQLLONG ,0) ;

	dbClass.sqlout((TIMESTAMP_STRUCT *) &best_pos.lief_term, SQLTIMESTAMP,26 ) ;
	dbClass.sqlout((short  *) &best_pos.mdn, SQLSHORT ,0) ;
	dbClass.sqlout((double *) &best_pos.me, SQLDOUBLE,0) ;				//	decimal(12,3)
	dbClass.sqlout((double *) &best_pos.me_ist, SQLDOUBLE,0) ;			//	decimal(12,3)
	dbClass.sqlout((short  *) &best_pos.p_num, SQLSHORT ,0) ;

	dbClass.sqlout((double *) &best_pos.pr_ek, SQLDOUBLE,0) ;			//	decimal(8,4)
	dbClass.sqlout((char   *)  best_pos.sa_kz,SQLCHAR,3 ) ;
	dbClass.sqlout((char   *)  best_pos.sti_pro_kz,SQLCHAR, 3) ;
	dbClass.sqlout((short  *) &best_pos.me_einh, SQLSHORT ,0) ;
	dbClass.sqlout((char   *) &best_pos.pr_abw ,SQLCHAR, 3 ) ;

	dbClass.sqlout((double *) &best_pos.fakt, SQLDOUBLE,0) ;			//	decimal(5,2)
	dbClass.sqlout((double *) &best_pos.pr_ek_bto, SQLDOUBLE,0) ;		//	decimal(8,4)
	dbClass.sqlout((long   *) &best_pos.best_txt,SQLLONG ,0 ) ;
	dbClass.sqlout((double *) &best_pos.pr_ek_euro, SQLDOUBLE,0) ;		//	decimal(8,4) 
	dbClass.sqlout((double *) &best_pos.pr_ek_euro_bto, SQLDOUBLE,0) ; //  decimal(8,4)

	dbClass.sqlout((double *) &best_pos.pr_ek_fremd, SQLDOUBLE,0) ;	//	decimal(8,4)
	dbClass.sqlout((double *) &best_pos.pr_ek_fremd_bto, SQLDOUBLE,0) ;//	decimal(8,4)
	dbClass.sqlout((double *) &best_pos.kontrakt_me, SQLDOUBLE,0) ;	//	decimal(12,3)
	dbClass.sqlout((double *) &best_pos.kontrakt_me_soll, SQLDOUBLE,0) ;//	decimal(12,3)
	dbClass.sqlout((double *) &best_pos.paz, SQLDOUBLE,0) ;			//	decimal(8,4)

	dbClass.sqlout((long   *) &best_pos.inh_ek ,SQLLONG ,0 ) ;
	dbClass.sqlout((double *) &best_pos.lakt_rab, SQLDOUBLE,0) ;		//	decimal(5,2)
	dbClass.sqlout((double *) &best_pos.aakt_rab, SQLDOUBLE,0) ;		//	decimal(5,2)
	dbClass.sqlout((double *) &best_pos.aakt_pr, SQLDOUBLE,0) ;		//  decimal(8,4)

		readcursor = (short) dbClass.sqlcursor ("select "

	"  a ,anz_einh ,bearb_stat ,best_blg ,best_term "
	" ,fil ,inh ,lief ,lief_best ,lief_s "
	" ,lief_term ,mdn ,me ,me_ist ,p_num "
	" ,pr_ek ,sa_kz ,sti_pro_kz ,me_einh ,pr_abw "
	" ,fakt ,pr_ek_bto ,best_txt ,pr_ek_euro ,pr_ek_euro_bto "
	" ,pr_ek_fremd ,pr_ek_fremd_bto ,kontrakt_me ,kontrakt_me_soll ,paz "
	" ,inh_ek ,lakt_rab ,aakt_rab ,aakt_pr "
			
		" from best_pos where best_blg = ? and lief = ?  "
		" and mdn = ? and fil = ? order by a ") ;

}