#ifndef _BEST_BEL_DEF
#define _BEST_BEL_DEF

struct BEST_KOPF {
short bearb_stat ;
long best_blg ;
TIMESTAMP_STRUCT best_term ;  
long best_txt ; 
short fil ; 
long lfd ;  
char lief [18] ;
long lief_s ;
TIMESTAMP_STRUCT lief_term ;
short mdn ; 
TIMESTAMP_STRUCT we_dat ;   
char pers_nam [10] ;
long fil_adr ;
long lief_adr ;  
char lieferzeit [7];
short waehrung ; 
short delstatus ;
short kontrakt_kz ; 
short kontrakt_nr ;
TIMESTAMP_STRUCT kontrakt_von ;
TIMESTAMP_STRUCT kontrakt_bis ;  
char freifeld1 [40] ;
char freifeld2 [40] ;
char freifeld3 [40] ;
char lief_ref_nr[20] ;
short zahl_kond ;      
short lgr ;  

};

extern struct BEST_KOPF best_kopf, best_kopf_null;

class BEST_KOPF_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
			   TIMESTAMP_STRUCT vondatum ;
			   TIMESTAMP_STRUCT bisdatum ;

			   int lesebest_kopf (void);
			   int openbest_kopf (CString  vondatum , CString bisdatum );

			   BEST_KOPF_CLASS () : DB_CLASS ()
               {
               }
};

struct BEST_POS {
double a ;				//	decimal(13,0)
double anz_einh ;		//	decimal(8,3)
short bearb_stat ;
long best_blg ;
TIMESTAMP_STRUCT best_term ;
short fil ;
double inh ;			//	decimal(8,3)
char lief [18] ;
char lief_best[18] ;
long lief_s ;
TIMESTAMP_STRUCT lief_term ;
short mdn ;
double me ;				//	decimal(12,3)
double me_ist ;			//	decimal(12,3)
short p_num ;
double pr_ek ;			//	decimal(8,4)
char sa_kz[3] ;
char sti_pro_kz[3] ;
short me_einh ;
char pr_abw [3] ;
double fakt ;			//	decimal(5,2)
double pr_ek_bto ;		//	decimal(8,4)
long best_txt ;
double pr_ek_euro ;		//	decimal(8,4) 
double pr_ek_euro_bto ; //  decimal(8,4)
double pr_ek_fremd ;	//	decimal(8,4)
double pr_ek_fremd_bto ;//	decimal(8,4)
double kontrakt_me ;	//	decimal(12,3)
double kontrakt_me_soll ;//	decimal(12,3)
double paz ;			//	decimal(8,4)
long inh_ek ;
double lakt_rab ;		//	decimal(5,2)
double aakt_rab ;		//	decimal(5,2)
double aakt_pr ;		//  decimal(8,4)
};

extern struct BEST_POS best_pos, best_pos_null;

class BEST_POS_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int lesebest_pos (void);
               int openbest_pos (void );
               BEST_POS_CLASS () : DB_CLASS ()
               {
               }
};


#endif

