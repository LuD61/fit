#ifndef _LIEF_BZG_DEF
#define _LIEF_BZG_DEF

struct LIEF_BZG {
double a ;
char best_txt1[26] ;
char best_txt2[26] ;
short fil ;
char lief[20] ;
char lief_best[20] ;
char lief_kz  [3] ;
char lief_rht [3] ;
long lief_s ;
short lief_zeit ;
short me_einh_ek ;
short mdn ;
double min_best ;
double pr_ek ;
char me_kz[3] ;
TIMESTAMP_STRUCT dat ;
char zeit[10] ;
char modif [3] ;
double pr_ek_eur ;
double pr_ek_sa ;
double pr_ek_sa_eur ;
char freifeld1[40] ;
char ean[17]  ;	// matches "123456789012*" 

};

extern struct LIEF_BZG lief_bzg, lief_bzg_null;

class LIEF_BZG_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int leselief_bzg_ean (void);
               int leselief_bzg_a   (void);
               int openlief_bzg_ean (void);
               int openlief_bzg_a   (void);
               LIEF_BZG_CLASS () : DB_CLASS ()
               {
               }
};

#endif

