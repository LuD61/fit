#ifndef _LIEF_DEF
#define _LIEF_DEF

struct LIEF {
char	abr[3] ;
long    adr ;
char    bank_nam[40] ;
long    bbn ;
short   best_sort ;
char    best_trans_kz [3] ;
long    blz ;
char    bonus_kz [3] ;
short   fil ;
char    fracht_kz [3] ;
char    ink [20] ;
long    kreditor ;
char    kto [20] ;
TIMESTAMP_STRUCT letzt_lief ;
char    lief [20] ;
char    lief_kun [20] ;
char    lief_rht [3] ;
long    lief_s ;
short   lief_typ ;
short   lief_zeit ;
double  lief_zusch ;	// decimal(8,2),
long    lst_nr ;
short   mahn_stu ;
short   mdn ;
char    me_kz [3] ;
char    min_me_kz [3] ;
double  min_zusch ;	// decimal(8,2),
double  min_zusch_proz ;	// decimal(5,2),
char    nach_lief [3] ;
char    ordr_kz [3] ;
char    rab_abl [3] ;
char    rab_kz [3] ;
char    rech_kz [3] ;
double  rech_toler ;
short   sdr_nr ;
long    son_kond ;
short   sprache ;
char    steuer_kz [3] ;
char    unt_lief_kz [3] ;
char    vieh_bas [3] ;
char    we_erf_kz [3] ;
char    we_kontr [3] ;
char    we_pr_kz [3] ;
double  we_toler ;	// decimal(4,1),
short   zahl_kond ;
char    zahlw [3] ;
short   hdklpargr ;
long    vorkgr ;
short   delstatus ;
short   mwst ;
short   liefart ;
char    modif [3] ;
char    ust_id [20] ;
short   eg_kz ;
short   waehrung ;
char    iln [20] ;
short   zertifikat1 ;
short   zertifikat2 ;
char    eg_betriebsnr[20] ;
char    iban1 [6] ;
char    iban2 [6] ;
char    iban3 [6] ;
char    iban4 [6] ;
char    iban5 [6] ;
char    iban6 [6] ;
char    iban7 [6] ;
char    esnum [12] ;
char    eznum [12] ;
short   geburt ;
short   mast ;
short   schlachtung ;
short   zerlegung ;
} ;

extern struct LIEF lief, lief_null;


class LIEF_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int leselief (void);
               int openlief (void);
               int lesealllief (void);
               int openalllief (void);
               LIEF_CLASS () : DB_CLASS ()
               {
               }
};

#endif

