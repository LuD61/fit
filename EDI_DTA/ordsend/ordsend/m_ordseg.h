#ifndef _ORDSEG_DEF
#define _ORDSEG_DEF


#define    TRENN_SE        "'"       // Segmentende
#define    TRENN_DE        "+"       // Datenelemente+Segmentbezeichner
#define    TRENN_GD        ":"       // Gruppendatenelemente
#define    TRENN_MA        "?"       // Maskierungs(freigabe-)zeichen
#define    TRENN_DZ        "."       // Dezimal-Trenner    


// dta_typ                      Numerierung AK-Handel 2002
#define EDEKA           1         //  2
#define KARSTADT        2         //  4    
#define KAUFRING        3         //  5
#define MARKANT         4         //  6
#define METRO           5         //  7
#define REWE            6         //  8
#define TENGELMANN      7         // 10
#define EK_GH_EG        8         //  3
#define ZIMBO           9         
#define CHRIST         10         //  1
#define SPAR           11         //  9
#define WALMART        12         // 11
#define KONSUM         13
//  WAS_ANDERES       999


class CEDIPUT 
{
	private :
               int danz_seg ;
			   int danz_nachrichten ;
	public :
		CEDIPUT () 
        {
			// Not-Initialisierungen , werden je nach Applikation meist uebersteuert oder aktualisiert 
			eanversion = 0 ;
			dnachkpreis = 2 ;
			dwiechmann_par = 0 ;
		};
		~CEDIPUT ()
		{
		};

	char trenn_se[2] ;
	char trenn_de[2] ;
	char trenn_gd[2] ;
	char trenn_ma[2] ;
	char trenn_dz[2] ;
	int eanversion ;	// evtl. spaeter mal umschalten oder variieren fuer neue Normvorschlaege


	int ddfw_par ;
	int dnachkpreis ;
	int dwiechmann_par ;



	char dateiname[257] ;
	char dateinamex[258] ;

	char ausgabestring[1024] ;
	char dstring200 [201] ;
	char dstring80   [81] ; 
	char dstring50   [51] ;
	char dstring20   [21] ;

	char dta_iln[20] ;
	char eigen_iln[20] ;
	long dta_referenz ;

	FILE * fpouti ;
	FILE * fpout  ;

void CEDIPUT::trennz_laden(void ) ; 
char * CEDIPUT::maskel ( char * eingabestr ) ;
void CEDIPUT::getsysdatum( char * datum, char * zeit ) ;

int CEDIPUT::UNA_segment (void) ;
int CEDIPUT::UNB_segment (long edta_referenz) ;
int CEDIPUT::UNZ_segment (void) ;
int CEDIPUT::UNH_segment( int ref_nr, char * nach_typ ) ;
int CEDIPUT::UNT_segment(int ref_nr) ;
int CEDIPUT::DTM_segment( char * ptyp, TIMESTAMP_STRUCT sdatum, int pformat, char * pzeit ) ;
int CEDIPUT::BGM_segment( char * ctyp, long blg_nr, int org_kop ) ;
int CEDIPUT::RFF_segment( char * ctyp, char * blg_nr) ;
int CEDIPUT::TDT_segment ( char * cinstring ) ;
int CEDIPUT::CTA_segment (char * ctyp, char *abtnr ) ; 
int CEDIPUT::NAD_segment( char * ctyp, char * diln, long dadr ) ;
int CEDIPUT::TAX_segment( char *taxwert) ;
int CEDIPUT::CPS_segment( long inum1 , long basnum ) ;
int CEDIPUT::PCI_segment  ( char * cinput) ;
int CEDIPUT::CUX_segment(void) ;
int CEDIPUT::LIN_segment( long ibel_pos, char *iart_ean ) ;
int CEDIPUT::PIA_segment ( long interna, char * externa, char * ityp ) ;
int CEDIPUT::PAC_segment ( int icont , char * ityp ) ;
int CEDIPUT::IMD_segment ( char * ityp, char * icode, char * freitext ) ;
int CEDIPUT::QTY_segment( char * ityp, double menge, short ime_einh ) ;
int CEDIPUT::MOA_segment( char * ityp, double iwert ) ;
int CEDIPUT::PCD_segment( char * ityp, double iwert ) ;
int CEDIPUT::ALC_segment( char * abzu, char * absprache, char *kalkstufe, char *rabart) ;
int CEDIPUT::PRI_segment ( char * ityp, double iwert, short ime_einh) ;
int CEDIPUT::MEA_segment( char * ityp, float iwert ) ;
int CEDIPUT::UNS_segment( char * ityp ) ;

int CEDIPUT::Kopfteil(char * edateiname , int dta_referenz) ;
int CEDIPUT::Fussteil(void) ;
int CEDIPUT::open_datei(char *edateiname) ;
int CEDIPUT::close_datei(void ) ;
int CEDIPUT::zeilenweg ( void ) ;

};

#endif

