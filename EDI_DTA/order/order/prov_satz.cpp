#include "stdafx.h"
#include "DbClass.h"
#include "prov_satz.h"

#include "kun.h" 
#include "a_bas.h"

extern DB_CLASS dbClass;

struct PROV_SATZ prov_satz,  prov_satz_null;

PROV_SATZ_CLASS prov_satz_class ;


/* das war der alte Ablauf,
 der neue Ablauf macht dieses alles in einem Statement und sollte daher hoffentlich 
   trotz unpassender Indizierung effizienter sein 
double PROV_SATZ_CLASS::prov_satz_hol (double i_a ,
									short i_ag ,
									short i_wg ,
									short i_hwg ,
									long i_kun ,
									char * i_kun_bran2 ,
									long i_vertr , 
									short i_mdn ,
									short i_sa_kz_sint )
{
	if ( readcursor < 0 ) prepare () ;
//-	Provisionssaetze sind entsprechend folgender Hierarchie gueltig:
//-	Fuer jede moegliche Hierarchie-Stufe gibt es genau einen Satz
//-	d.h.: unique Index auf diese 8 Felder
//-
//-	Der Zugriff erfolgt absteigender Reihenfolge
//-	
// im TEXTBLOCK
//-	 1.Test =>Provision fuer Artikel dieses Kunden
//-	 2.Test =>Provision fuer Art.Gr. dieses Kunden
//-	 3.Test =>Provision fuer War.Gr. dieses Kunden
//-	 4.Test =>Provision fuer HWG dieses Kunden
//-	 5.Test =>Provision fuer alles von diesem Kunden
//-
//-	 6.Test =>Provision fuer Artikel fuer Kundenbranche dieses Kunden
//-	 7.Test =>Provision fuer Art.Gr. fuer Kundenbranche dieses Kunden
//-	 8.Test =>Provision fuer War.Gr. fuer Kundenbranche dieses Kunden
//-	 9.Test =>Provision fuer HWG fuer Kundenbranche dieses Kunden
//-	10.Test =>Provision fuer alles von dieser Kundenbranche
//-
// im TEXTBLOCK
//-	11.Test =>Provision fuer Artikel aller Kunden
//-	12.Test =>Provision fuer Art.Gr. aller Kunden
//-	13.Test =>Provision fuer War.Gr. aller Kunden
//-	14.Test =>Provision fuer HWG aller Kunden
//-	15.Test =>Provision fuer alles von diesem Vertreter 
//-
//-	mdn + vertr
//-	kun_bran2 Kun_bran = "0" : Prov-Satz gilt fuer alle Kunden-Branchen 
//-	kun	Kunde = 0 : Prov-Satz gilt fuer alle Kunden 
//-	hwg	hwg = 0 : Prov-Satz gilt fuer alle hwg 
//-	wg	wg = 0 : Prov-Satz gilt fuer alle wg 
//-	ag	ag = 0 : Prov-Satz gilt fuer alle ag 
//-	Artikel	Artikel = 0 : Prov-Satz gilt fuer alle Artikel 
//-
// im TEXTBLOCK
//--------------------------------------------------------------------------------
//-	Funktion  : Hierarchische Suche nach Provisionsaetzen 
//-		  : der unique - Index lautet :
//-						-	mdn
//-						-	vertr
//-						-	kun
//-						-	kun_bran2
//-						-	hwg
//-						-	wg
//-						-	ag
//-						-	a
//-
//-		Die Hierarchie ist folgendermassen strukturiert :
//-			1.mdn
//-			2.vertr
//-			3.kun
//-			4.kun_bran2
//-			4.a
//-			5.ag
//-			6.wg
//-			7.hwg
//-		
	int retco = 0  ;
	double retprov_satz = 0.0 ;
 
	if ( readcursor < 0 ) prepare () ;

// im TEXTBLOCK
 //  --------- a pro kunde und Vertreter ------------ 
	prov_satz.mdn = i_mdn ;
	prov_satz.vertr = i_vertr ;
	prov_satz.kun = i_kun ;
	sprintf (prov_satz.kun_bran2 , "0" ) ;
	prov_satz.hwg = 0 ;
	prov_satz.wg = 0 ;
	prov_satz.ag = 0 ;
	prov_satz.a = i_a ;

	retco =  dbClass.sqlopen (readcursor);
	if ( retco ) return 0.0 ;	// boeses error-return
	retco = dbClass.sqlfetch (readcursor);
	if ( retco )
	{
// im TEXTBLOCK
// --------- ag pro kunde und Vertreter ------------ 
		prov_satz.mdn = i_mdn ;
		prov_satz.vertr = i_vertr ;
		prov_satz.kun = i_kun ;
		sprintf (prov_satz.kun_bran2 , "0" ) ;
		prov_satz.hwg = 0 ;
		prov_satz.wg = 0 ;
		prov_satz.ag = i_ag ;
		prov_satz.a = 0.0 ;

		retco =  dbClass.sqlopen (readcursor);
		if ( retco ) return 0.0 ;	// boeses error-return
		retco = dbClass.sqlfetch (readcursor);
	}
	if ( retco )
	{
// im TEXTBLOCK
// --------- wg pro kunde und Vertreter ------------ 
		prov_satz.mdn = i_mdn ;
		prov_satz.vertr = i_vertr ;
		prov_satz.kun = i_kun ;
		sprintf (prov_satz.kun_bran2 , "0" ) ;
		prov_satz.hwg = 0 ;
		prov_satz.wg = i_wg ;
		prov_satz.ag = 0 ;
		prov_satz.a = 0.0 ;

		retco =  dbClass.sqlopen (readcursor);
		if ( retco ) return 0.0 ;	// boeses error-return
		retco = dbClass.sqlfetch (readcursor);
	}
	if ( retco )
	{
// im TEXTBLOCK
// --------- hwg pro kunde und Vertreter ------------ 
		prov_satz.mdn = i_mdn ;
		prov_satz.vertr = i_vertr ;
		prov_satz.kun = i_kun ;
		sprintf (prov_satz.kun_bran2 , "0" ) ;
		prov_satz.hwg = i_hwg ;
		prov_satz.wg = 0 ;
		prov_satz.ag = 0 ;
		prov_satz.a = 0.0 ;

		retco =  dbClass.sqlopen (readcursor);
		if ( retco ) return 0.0 ;	// boeses error-return
		retco = dbClass.sqlfetch (readcursor);
	}
	if ( retco )
	{
// im TEXTBLOCK
// --------- Gesamtsortiment pro kunde und Vertreter ------------ 
		prov_satz.mdn = i_mdn ;
		prov_satz.vertr = i_vertr ;
		prov_satz.kun = i_kun ;
		sprintf (prov_satz.kun_bran2 , "0" ) ;
		prov_satz.hwg = 0 ;
		prov_satz.wg = 0 ;
		prov_satz.ag = 0 ;
		prov_satz.a = 0.0 ;

		retco =  dbClass.sqlopen (readcursor);
		if ( retco ) return 0.0 ;	// boeses error-return
		retco = dbClass.sqlfetch (readcursor);
	}
	if ( retco )
	{
// im TEXTBLOCK
// --------- a pro Kun.-Branche und Vertreter ------------ 
		prov_satz.mdn = i_mdn ;
		prov_satz.vertr = i_vertr ;
		prov_satz.kun = 0 ;
		sprintf (prov_satz.kun_bran2 , "%s" , i_kun_bran2) ;
		prov_satz.hwg = 0 ;
		prov_satz.wg = 0 ;
		prov_satz.ag = 0 ;
		prov_satz.a = i_a ;

		retco =  dbClass.sqlopen (readcursor);
		if ( retco ) return 0.0 ;	// boeses error-return
		retco = dbClass.sqlfetch (readcursor);
	}
	if ( retco )
	{
// im TEXTBLOCK
// --------- ag pro Kun.-Branche und Vertreter ------------ 
		prov_satz.mdn = i_mdn ;
		prov_satz.vertr = i_vertr ;
		prov_satz.kun = 0 ;
		sprintf (prov_satz.kun_bran2 , "%s" , i_kun_bran2) ;
		prov_satz.hwg = 0 ;
		prov_satz.wg = 0 ;
		prov_satz.ag = i_ag ;
		prov_satz.a = 0.0 ;

		retco =  dbClass.sqlopen (readcursor);
		if ( retco ) return 0.0 ;	// boeses error-return
		retco = dbClass.sqlfetch (readcursor);
	}
	if ( retco )
	{
// im TEXTBLOCK
// --------- wg pro Kun.-Branche und Vertreter ------------ 
		prov_satz.mdn = i_mdn ;
		prov_satz.vertr = i_vertr ;
		prov_satz.kun = 0 ;
		sprintf (prov_satz.kun_bran2 , "%s" , i_kun_bran2) ;
		prov_satz.hwg = 0 ;
		prov_satz.wg = i_wg ;
		prov_satz.ag = 0 ;
		prov_satz.a = 0.0 ;

		retco =  dbClass.sqlopen (readcursor);
		if ( retco ) return 0.0 ;	// boeses error-return
		retco = dbClass.sqlfetch (readcursor);
	}
	if ( retco )
	{
// im TEXTBLOCK
// --------- hwg pro Kun.-Branche und Vertreter ------------ 
		prov_satz.mdn = i_mdn ;
		prov_satz.vertr = i_vertr ;
		prov_satz.kun = 0 ;
		sprintf (prov_satz.kun_bran2 , "%s" , i_kun_bran2) ;
		prov_satz.hwg = i_hwg ;
		prov_satz.wg = 0 ;
		prov_satz.ag = 0 ;
		prov_satz.a = 0.0 ;

		retco =  dbClass.sqlopen (readcursor);
		if ( retco ) return 0.0 ;	// boeses error-return
		retco = dbClass.sqlfetch (readcursor);
	}
	if ( retco )
	{
// im TEXTBLOCK
// --------- Gesamtsortiment pro Kun.-Branche und Vertreter ------------ 
		prov_satz.mdn = i_mdn ;
		prov_satz.vertr = i_vertr ;
		prov_satz.kun = 0 ;
		sprintf (prov_satz.kun_bran2 , "%s" , i_kun_bran2) ;
		prov_satz.hwg = 0 ;
		prov_satz.wg = 0 ;
		prov_satz.ag = 0 ;
		prov_satz.a = 0.0 ;

		retco =  dbClass.sqlopen (readcursor);
		if ( retco ) return 0.0 ;	// boeses error-return
		retco = dbClass.sqlfetch (readcursor);
	}
	if ( retco )
	{
// im TEXTBLOCK
// --------- a fuer alle Kunden und diesen Vertreter ------------ 
		prov_satz.mdn = i_mdn ;
		prov_satz.vertr = i_vertr ;
		prov_satz.kun = 0 ;
		sprintf (prov_satz.kun_bran2 , "0" ) ;
		prov_satz.hwg = 0 ;
		prov_satz.wg = 0 ;
		prov_satz.ag = 0 ;
		prov_satz.a = i_a ;

		retco =  dbClass.sqlopen (readcursor);
		if ( retco ) return 0.0 ;	// boeses error-return
		retco = dbClass.sqlfetch (readcursor);
	}
	if ( retco )
	{
// im TEXTBLOCK
// --------- ag fuer alle Kunden und diesen Vertreter ------------ 
		prov_satz.mdn = i_mdn ;
		prov_satz.vertr = i_vertr ;
		prov_satz.kun = 0 ;
		sprintf (prov_satz.kun_bran2 , "0" ) ;
		prov_satz.hwg = 0 ;
		prov_satz.wg = 0 ;
		prov_satz.ag = i_ag ;
		prov_satz.a = 0.0 ;

		retco =  dbClass.sqlopen (readcursor);
		if ( retco ) return 0.0 ;	// boeses error-return
		retco = dbClass.sqlfetch (readcursor);
	}
	if ( retco )
	{
// im TEXTBLOCK
// --------- wg fuer alle Kunden und diesen Vertreter ------------ 
		prov_satz.mdn = i_mdn ;
		prov_satz.vertr = i_vertr ;
		prov_satz.kun = 0 ;
		sprintf (prov_satz.kun_bran2 , "0" ) ;
		prov_satz.hwg = 0 ;
		prov_satz.wg = i_wg ;
		prov_satz.ag = 0 ;
		prov_satz.a = 0.0 ;

		retco =  dbClass.sqlopen (readcursor);
		if ( retco ) return 0.0 ;	// boeses error-return
		retco  = dbClass.sqlfetch (readcursor);
	}
	if ( retco )
	{
// im TEXTBLOCK
// --------- hwg fuer alle Kunden und diesen Vertreter ------------ 
		prov_satz.mdn = i_mdn ;
		prov_satz.vertr = i_vertr ;
		prov_satz.kun = 0 ;
		sprintf (prov_satz.kun_bran2 , "0" ) ;
		prov_satz.hwg = i_hwg ;
		prov_satz.wg = 0 ;
		prov_satz.ag = 0 ;
		prov_satz.a = 0.0 ;

		retco =  dbClass.sqlopen (readcursor);
		if ( retco ) return 0.0 ;	// boeses error-return
		retco = dbClass.sqlfetch (readcursor);
	}
	if ( retco )
	{
// im TEXTBLOCK
// --------- Gesamtsortiment fuer alle Kunden und diesen Vertreter ------------ 
		prov_satz.mdn = i_mdn ;
		prov_satz.vertr = i_vertr ;
		prov_satz.kun = 0 ;
		sprintf (prov_satz.kun_bran2 , "0" ) ;
		prov_satz.hwg = 0 ;
		prov_satz.wg = 0 ;
		prov_satz.ag = 0 ;
		prov_satz.a = 0.0 ;

		retco =  dbClass.sqlopen (readcursor);
		if ( retco ) return 0.0 ;	// boeses error-return
		retco = dbClass.sqlfetch (readcursor);
	}
	if ( retco ) return 0.0 ;	// nix da 
    if ( i_sa_kz_sint > 0 )
		return  prov_satz.prov_sa_satz ;
	else
		return prov_satz.prov_satz ;
}
// im TEXTBLOCK

void PROV_SATZ_CLASS::prepare (void)
{

	dbClass.sqlin (( short *)&prov_satz.mdn, SQLSHORT, 0 ) ;
	dbClass.sqlin (( long *) &prov_satz.vertr , SQLLONG, 0 ) ;
	dbClass.sqlin (( long *) &prov_satz.kun , SQLLONG, 0 ) ;
	dbClass.sqlin (( char *)  prov_satz.kun_bran2 , SQLCHAR, 3 ) ;
	dbClass.sqlin (( short *)&prov_satz.hwg, SQLSHORT, 0 ) ;
	dbClass.sqlin (( short *)&prov_satz.wg, SQLSHORT, 0 ) ;
	dbClass.sqlin (( short *)&prov_satz.ag, SQLSHORT, 0 ) ;
	dbClass.sqlin (( double *)&prov_satz.a, SQLDOUBLE, 0 ) ;

	dbClass.sqlout (( double *) &prov_satz.prov_satz, SQLDOUBLE, 0 ) ;
	dbClass.sqlout (( double *) &prov_satz.prov_sa_satz, SQLDOUBLE, 0 ) ;

// im TEXTBLOCK
	readcursor = (short) dbClass.sqlcursor ("select "
	" prov_satz, prov_sa_satz from prov_satz  " 
	" where mdn = ?  "
	" and vertr = ? "
	" and kun = ? "
	" and kun_bran2 = ? "
	" and hwg = ? "
	" and wg = ? "
	" and ag = ? "
	" and a = ? " ) ;

}
< ------ */

// hier gibt es Diferenzen in der AG : long in a_bas, short in prov_satz 
double PROV_SATZ_CLASS::prov_satz_hol (double i_a ,
									short i_ag ,
									short i_wg ,
									short i_hwg ,
									long i_kun ,
									char * i_kun_bran2 ,
									long i_vertr , 
									short i_mdn ,
									short i_sa_kz_sint )
{
	if ( readcursor < 0 ) prepare () ;
	prov_satz.mdn = i_mdn ;
	prov_satz.vertr = i_vertr ;
	prov_satz.kun = i_kun ;
	sprintf (prov_satz.kun_bran2 , "0" ) ;
	prov_satz.hwg = 0 ;
	prov_satz.wg = 0 ;
	prov_satz.ag = 0 ;
	prov_satz.a = i_a ;

	int retco =  dbClass.sqlopen (readcursor);
	if ( retco ) return 0.0 ;	// boeses error-return
	retco = dbClass.sqlfetch (readcursor);
	if ( retco ) return 0.0 ;	// keinerlei Provision gefunden
// im ersten Satz steht die gueltige Information ....
    if ( i_sa_kz_sint > 0 )
		return  prov_satz.prov_sa_satz ;
	else
		return prov_satz.prov_satz ;
}
	

void PROV_SATZ_CLASS::prepare (void)
{

	dbClass.sqlin (( short *)&prov_satz.mdn, SQLSHORT, 0 ) ;
	dbClass.sqlin (( long *) &prov_satz.vertr , SQLLONG, 0 ) ;
	dbClass.sqlin (( long *) &prov_satz.kun , SQLLONG, 0 ) ;
	dbClass.sqlin (( char *)  prov_satz.kun_bran2 , SQLCHAR, 3 ) ;
	dbClass.sqlin (( short *)&prov_satz.hwg, SQLSHORT, 0 ) ;
	dbClass.sqlin (( short *)&prov_satz.wg, SQLSHORT, 0 ) ;
	dbClass.sqlin (( short *)&prov_satz.ag, SQLSHORT, 0 ) ;
	dbClass.sqlin (( double *)&prov_satz.a, SQLDOUBLE, 0 ) ;

	dbClass.sqlout (( double *) &prov_satz.prov_satz, SQLDOUBLE, 0 ) ;
	dbClass.sqlout (( double *) &prov_satz.prov_sa_satz, SQLDOUBLE, 0 ) ;

	readcursor = (short) dbClass.sqlcursor ("select "
	" prov_satz, prov_sa_satz "
	" , mdn, vertr, kun, kun_bran2, hwg, wg, ag, a "	// nur wegen oder-Bedingung mitlesen .....  
	" from prov_satz where mdn = ?  "
	" and vertr = ? "
	" and ( kun = ? or kun = 0 ) "
	" and ( kun_bran2 = ?  or kun_bran2 = \"0\" ) "
	" and ( hwg = ? or hwg = 0 )  "
	" and ( wg = ? or wg = 0 ) "
	" and ( ag = ? or ag = 0 ) "
	" and ( a = ? or a = 0 )" 
	" order by kun desc , kun_bran2 desc, a desc , ag desc , wg desc, hwg desc " ) ;

// letzte Unschaerfe : kun_bran2 ist alpha-Feld, da waere ein branchenname < "0" denkbar , jedoch sehr unwahrscheinlich 
}
