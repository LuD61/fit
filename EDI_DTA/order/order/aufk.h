#ifndef _AUFK_DEF
#define _AUFK_DEF

struct AUFK {
long mdn ;  
short fil ; 
long ang ;  
long auf ; 
long adr ; 
short kun_fil ; 
long kun ;
TIMESTAMP_STRUCT lieferdat ;
char lieferzeit[6] ;
char hinweis  [49] ;
short auf_stat ;
char kun_krz1[17] ;
char feld_bz1[20] ;
char feld_bz2[12] ;
char feld_bz3[8] ;
short delstatus ;
double zeit_dec ;
long kopf_txt ;
long fuss_txt ;
long vertr ;
char auf_ext [17] ;
long tou  ;
char pers_nam [9] ;
TIMESTAMP_STRUCT komm_dat ;
TIMESTAMP_STRUCT best_dat ;
short waehrung ;
short auf_art ;
short gruppe ;
short ccmarkt ;
short fak_typ ;
long tou_nr ;
char ueb_kz[2] ;
TIMESTAMP_STRUCT fix_dat ;
char komm_name[13] ;
TIMESTAMP_STRUCT akv ;
char akv_zeit [6] ;
TIMESTAMP_STRUCT bearb ;
char bearb_zeit[6] ;
short psteuer_kz ;
};

extern struct AUFK aufk, aufk_null;

class AUFK_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
			   int aktual_ipr (void ) ; 
               int insertaufk (void);
               AUFK_CLASS () : DB_CLASS ()
               {
               }
};
#endif

