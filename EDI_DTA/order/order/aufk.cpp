#include "stdafx.h"
#include "DbClass.h"
#include "aufk.h"

struct AUFK aufk,  aufk_null;

extern DB_CLASS dbClass;

AUFK_CLASS aufk_class ;

int AUFK_CLASS::aktual_ipr (void)
{

		if ( ins_cursor < 0 ) prepare ();
		
        int retco = dbClass.sqlexecute (readabeding);
             retco = dbClass.sqlexecute (readbbeding);
             retco = dbClass.sqlexecute (readcbeding);

		return retco ;
}

int AUFK_CLASS::insertaufk (void)
{

		if ( ins_cursor < 0 ) prepare ();
		
         return dbClass.sqlexecute (ins_cursor);
}


void AUFK_CLASS::prepare (void)
{

	dbClass.sqlin ((long  *) &aufk.mdn, SQLLONG, 0 ) ;  
	dbClass.sqlin ((short *) &aufk.fil, SQLSHORT, 0 ) ; 
	dbClass.sqlin ((long  *) &aufk.ang, SQLLONG, 0 ) ;  
	dbClass.sqlin ((long  *) &aufk.auf, SQLLONG, 0 ) ; 
	dbClass.sqlin ((long  *) &aufk.adr, SQLLONG, 0 ) ; 

	dbClass.sqlin ((short *) &aufk.kun_fil, SQLSHORT, 0 ) ; 
	dbClass.sqlin ((long  *) &aufk.kun, SQLLONG, 0 ) ;
	dbClass.sqlin ((TIMESTAMP_STRUCT *) &aufk.lieferdat, SQLTIMESTAMP, 26 ) ;
	dbClass.sqlin ((char  *)  aufk.lieferzeit, SQLCHAR, 6 ) ;
	dbClass.sqlin ((char  *)  aufk.hinweis, SQLCHAR, 49 ) ;

	dbClass.sqlin ((short *) &aufk.auf_stat, SQLSHORT, 0 ) ;
	dbClass.sqlin ((char  *)  aufk.kun_krz1, SQLCHAR, 17 ) ;
	dbClass.sqlin ((char  *)  aufk.feld_bz1, SQLCHAR, 20 ) ;
	dbClass.sqlin ((char  *)  aufk.feld_bz2, SQLCHAR, 12 ) ;
	dbClass.sqlin ((char  *)  aufk.feld_bz3, SQLCHAR, 8 ) ;

	dbClass.sqlin ((short *) &aufk.delstatus, SQLSHORT, 0 ) ;
	dbClass.sqlin ((double*) &aufk.zeit_dec, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((long  *) &aufk.kopf_txt, SQLLONG, 0 ) ;
	dbClass.sqlin ((long  *) &aufk.fuss_txt, SQLLONG, 0 ) ;
	dbClass.sqlin ((long  *) &aufk.vertr, SQLLONG, 0 ) ;

	dbClass.sqlin ((char  *)  aufk.auf_ext, SQLCHAR, 17 ) ;
	dbClass.sqlin ((long  *) &aufk.tou, SQLLONG, 0 )  ;
	dbClass.sqlin ((char  *)  aufk.pers_nam, SQLCHAR, 9 ) ;
	dbClass.sqlin ((TIMESTAMP_STRUCT *) &aufk.komm_dat, SQLTIMESTAMP, 26 ) ;
	dbClass.sqlin ((TIMESTAMP_STRUCT *) &aufk.best_dat, SQLTIMESTAMP, 26 ) ;

	dbClass.sqlin ((short *) &aufk.waehrung, SQLSHORT, 0 ) ;
	dbClass.sqlin ((short *) &aufk.auf_art, SQLSHORT, 0 ) ;
	dbClass.sqlin ((short *) &aufk.gruppe, SQLSHORT, 0 ) ;
	dbClass.sqlin ((short *) &aufk.ccmarkt, SQLSHORT, 0 ) ;
	dbClass.sqlin ((short *) &aufk.fak_typ, SQLSHORT, 0 ) ;

	dbClass.sqlin ((long  *) &aufk.tou_nr, SQLLONG, 0 ) ;
	dbClass.sqlin ((char  *)  aufk.ueb_kz, SQLCHAR, 2 ) ;
	dbClass.sqlin ((TIMESTAMP_STRUCT *) &aufk.fix_dat, SQLTIMESTAMP, 26 ) ;
	dbClass.sqlin ((char  *)  aufk.komm_name, SQLCHAR, 13 ) ;
	dbClass.sqlin ((TIMESTAMP_STRUCT *) &aufk.akv, SQLTIMESTAMP, 26 ) ;

	dbClass.sqlin ((char  *)  aufk.akv_zeit, SQLCHAR, 6 ) ;
	dbClass.sqlin ((TIMESTAMP_STRUCT *) &aufk.bearb, SQLTIMESTAMP, 26 ) ;
	dbClass.sqlin ((char  *)  aufk.bearb_zeit, SQLCHAR, 6 ) ;
	dbClass.sqlin ((short *) &aufk.psteuer_kz, SQLSHORT, 0 ) ;

	ins_cursor = (short) dbClass.sqlcursor ("insert into aufk ( "
	  " mdn, fil, ang, auf, adr "
	 " , kun_fil, kun, lieferdat, lieferzeit, hinweis " 
	 " , auf_stat, kun_krz1, feld_bz1, feld_bz2, feld_bz3 "
	 " , delstatus, zeit_dec, kopf_txt, fuss_txt, vertr "
	 " , auf_ext, tou, pers_nam, komm_dat, best_dat "
	 " , waehrung, auf_art, gruppe, ccmarkt, fak_typ "
	 " , tou_nr, ueb_kz, fix_dat, komm_name, akv "
	 " , akv_zeit, bearb, bearb_zeit, psteuer_kz "
	 " )  values ( "
 	    " ?, ?, ?, ?, ? " 
	  " , ?, ?, ?, ?, ? " 
	  " , ?, ?, ?, ?, ? " 
	  " , ?, ?, ?, ?, ? " 
	  " , ?, ?, ?, ?, ? " 
	  " , ?, ?, ?, ?, ? " 
	  " , ?, ?, ?, ?, ? " 
	  " , ?, ?, ?, ?  "
	  " ) " ) ;

	readabeding = (short) dbClass.sqlcursor (
		"update ipr set kun = 0 where kun is null " ) ;

	readbbeding = (short) dbClass.sqlcursor (
		"update ipr set vk_pr_eu = vk_pr_i where vk_pr_eu is null " ) ;

	readcbeding = (short) dbClass.sqlcursor (
		"update ipr set ld_pr_eu = ld_pr where ld_pr_eu is null " ) ;

}

