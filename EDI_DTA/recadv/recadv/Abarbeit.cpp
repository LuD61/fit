#include "stdafx.h"  

// ##########################
// Pufferung in Tabelle aufk

// aufk.lieferdat : 137 (Datum der Versendung)
// aufk.best_dat : 50 (Datum der Vereinnahmung)
// aufk.auf_ext   : ext. Auftragsnummer
// aufk.auf       : rff-ls-Nummer

////////////////////// aufk.kun       : K�ufer-ILN ( wahrsch. immerzu Metro )
// aufk.kun       : Kunde aus der Referenz-Lieferschein-Nummer

// aufp.auf_me  :  46 gezaehlte Menge
// aufp.auf_me1 : 180 vereinnahmte Menge
// aufp.lief_me :  12 Liefermenge

// 220605 : Zahlen 2 Stellen groesser
// gezaehlt nicht mehr signifikant

//           111111111 222222222 333333333 444444444 555555555 666666666 77777777
// 123456789012345678901234567890123456789012345678901234567890123456789012345678

//  LS-Nr.  Kun-Nr. Kun-krz1         Lief-Dat  Empf-Dat ext-Auftr
// 12345678 12345678 1234567890123456 dd.mm.yy  dd.mm.yy 1234567890.. 

//          ILN   Art.-Nr.     gelief.   gezaehlt     verein.  
// 1234567890123   12345678  xx12345,12 xx12345,12  xx12345,12



/* ---->
Belegung tempdrx
	char bearb[11];		// fix "TODAY"
	char form_nr[11];	// fix "recadvpro"
	short lila ;		// fix 0
	long lfd01 ;		// 1==Kopf, 2== Position

	long lfd02 ;		// lfd.Nr der Datei
	long lfd03 ;		// int_kun
	long lfd04 ;		// int_adr
	long lfd05 ;		// dta.dta
	long lfd06 ;		// kopfbug-status
	long lfd07 ;		// pos-bug-status
	long lfd08 ;		// dabbruch
	long lfd09 ;		// int_ls ( war mal int_auf )
	long lfd10 ;		// lfd in der Liste ( Order-Feld )
	double dfeld01 ;	// 					Artikel-Nummer
	double dfeld02 ;	// 					Menge 12
	double dfeld03 ;	// 					Menge 180
	double dfeld04 ;	//					Menge 46		
	double dfeld05 ;
	double dfeld06 ;
	double dfeld07 ;
	double dfeld08 ;
	double dfeld09 ;
	double dfeld10 ;
	double dfeld11 ;
	double dfeld12 ;
	double dfeld13 ;
	double dfeld14 ;
	double dfeld15 ;
	double dfeld16 ;
	double dfeld17 ;
	double dfeld18 ;
	double dfeld19 ;
	double dfeld20 ;

	char cfeld101[17] ;
	char cfeld102[17] ;
	char cfeld103[17] ;
	char cfeld104[17] ;
	char cfeld105[17] ;

	char cfeld201[25] ;	// datum 137 ( Datum der Versendung ==Lieferdatum )
	char cfeld202[25] ;
	char cfeld203[25] ;
	char cfeld204[25] ;	// externer Auftrag
	char cfeld205[25] ;

	char cfeld301[37] ;	// Bemerkung Teil1
	char cfeld302[37] ;	// Bemerkung Teil2	
	char cfeld303[37] ;	// Kun-Krz1
	char cfeld304[37] ;	
	char cfeld305[37] ;
	long lfd ;		// Listen-ident

< ---- */





#include "recadv.h"
#include "recadvDlg.h"

#include "DbClass.h"
#include "dta.h" 
#include "mdn.h"
#include "kun.h"
#include "tempdr.h"
#include "adr.h"
#include "aufk.h"
#include "aufp.h"
#include "ls.h"
#include "a_bas.h"
#include "prov_satz.h"
#include "ptabn.h"
#include "bsd_buch.h"
#include "sys_par.h"
#include "mo_numme.h"
#include "DllPreise.h"
#include "token.h"

#include "Abarbeit.h"

static int mogel =  1 ;	// gemogelt, damit es erst mal losgeht
static int immerweiter = 1 ; // Normal: ueberspringen und Info generieren  ;  
static int ignoresegment = 0 ; // Normal : abbrechen, Sonderwunsch : nur ewig meckern
static int doption = 0 ;
static int dplattform = 0 ;

static double tausendfaktor = 0.0 ;

static long lfdliste ;	// Listennummer fuer Ausdruck
static long lfdlistennr ;	// lfd in der Liste

extern DB_CLASS dbClass;
extern TEMPDRX_CLASS tempdrx_class ;
extern PTABN_CLASS ptabn_class ;
extern ADR_CLASS adr_class ;
extern AUFK_CLASS aufk_class ;
extern AUFP_CLASS aufp_class ;
extern KUN_CLASS kun_class ;
extern LSK_CLASS lsk_class;
extern A_BAS_CLASS a_bas_class ;
extern A_KUN_CLASS a_kun_class ;
extern A_KUN_GX_CLASS a_kun_gx_class ;
extern A_EAN_CLASS a_ean_class ;

extern BSD_BUCH_CLASS bsd_buch_class ;
extern SYS_PAR_CLASS sys_par_class ; 
extern LGR_CLASS lgr_class ;

extern A_HNDW_CLASS a_hndw_class ;
extern A_EIG_CLASS a_eig_class ;
extern A_EIG_DIV_CLASS a_eig_div_class ;

extern TSMTG_CLASS tsmtg_class ;
extern TSMTGG_CLASS tsmtgg_class ;
extern PROV_SATZ_CLASS prov_satz_class ;

CDllPreise DllPreise ;

#define  TRENN_SE        "'"       // Segmentende
#define  TRENN_DE        "+"       // Datenelemente+Segmentbezeichner
#define  TRENN_GD        ":"       // Gruppendatenelemente
#define  TRENN_MA        "?"       // Maskierungs(freigabe-)zeichen
#define	 TRENN_DZ        "."       // Dezimal-Trenner    
#define CIPPUFFLEN      1000      // Laenge Input-Puffer
#define MAXDIMNUM		1000	// maximal 1000 Auftraege in einer Datei 


static char trenn_se[2] ;
static char trenn_de[2] ;
static char trenn_gd[2] ;
static char trenn_ma[2] ;
static char trenn_dz[2] ;

static long num_array[MAXDIMNUM] ;

// Ptrototyping 

void write_belkopf_a ( void ) ;
void write_belpos_a  ( void ) ;

int read_belkopf_a ( int ) ;
int read_belpos_a  ( void ) ;

// Unsere Aufgabe sei NICHT primaer das Finden von Fehlern, sondern von gueltigen Informationen


static int dpos_zahl = 0 ;
static int teilsmtbeacht ;

// 260808 Zun�chst folgende Strategie : bei aktiver teilsmt-Trennung erwarte ich erst nur "sortimentsreine" Bestellungen
// 260808 die Entscheidung faellt dann beim ersten Artikel 
// 260808 spaeter ist dann auch noch Parallel-Trennung denkbar 

int teilsmttrennen = 0 ;
int teilsmtnochtrennen = 0 ;
int verdicht = 0 ;


char branche0 [4] ;	// 260808 
char branche1 [4] ;	// 260808 
char branche2 [4] ;	// 260808 
char branche3 [4] ;	// 260808 
char branche4 [4] ;	// 260808 


char teilsort0 [110] ;	// 260808 
char teilsort1 [110] ;	// 260808 
char teilsort2 [110] ;	// 260808 
char teilsort3 [110] ;	// 260808 
char teilsort4 [110] ;	// 260808 


static short dlgr_gr = -1 ;
static short dli_a_edi = 0 ;
static short dlgr_bestand = 0 ;
static char buffer[1000] ;	// hilfsvariable fuer string-handling

static long dta_referenz ;

static char cnari[21] ;		// aktuelle Nachricht
static int narityp	;		// z.B. 380,381,393, 325 usw.

static int inblock = 0 ;	// 0 ==> UNB ==> 1
struct BELKOPF
{
// char bgm_typ [5] ;

char auf_ext[25] ;	// externe Auftragsnummer : ist die finale Referenz !! Es gibt nur bgm == "220"
long auf_int ;
char kun_bran2[4] ;	// 2600808
// char nad_su_feld [21] ;	// nur als Pruef-Test evtl. interessant ( immer ich selber-Lieferant ) 
// char nad_by_feld [21] ;	// nur als Zusatz-Test evtl. interessnat ( immer der Besteller ?!)
char nad_dp_feld [21] ;
char dtm_50_feld [21] ;	 // Datum der Vereinnahmung
char dtm_50_feld_t[10];	 // 291113 : Lieferzeit
char dtm_137_feld [21] ; // datum der Versendung
long int_kun ;	// Kunde aufgeloest -> falls == 0 , dann error == unbekannt 
				// Kunde aufgeloest -> falls == -1, dann error == mehrfach

long int_adr ;
char kun_krz1[37] ;
long tou ;
long vertr ;
char bemerkung[61] ;	// Kommentar/Kurzfehler ( oder ftx-zzz-segment )
char loc_feld [21] ;
char rff_dq_feld[21];	// eigne LS-Nummer ?!
} ;


struct BELKOPF belkopf , belkopf_null ;
struct BELKOPF belkopf1, belkopf2, belkopf3, belkopf4 ; 
struct BELKOPF * belkopf_a ;

static int belkopfdim ;
static int belkopfmax ;
static int belkopfakt ;


struct BELPOS
{
char auf_ext [25] ;	// die finale referenz ( siehe Kopf )
long lin_int ;	// lfd. Nummer der lin im Beleg 
char lin_feld[21] ;	// ean ??!!
char pia_sa_feld[21] ;	// Nummer des Lieferanten ( eigne Nr. )
char pia_bp_feld[21] ;	// Nummer des Kaeufers ( a_kun ) 
char imd_a_feld[70] ;	// Klartext
double qty_12_wert ;
char qty_12_einh[11] ;	// spannend z.B. bei "GRM"

double qty_46_wert ;
char qty_46_einh[11] ;	// spannend z.B. bei "GRM"
double qty_180_wert ;
char qty_180_einh[11] ;	// spannend z.B. bei "GRM"

double pri_aae_wert ;	// Auszeichnungspreis 
char bemerkung[61] ;	// Kommentar/Kurzfehler

// ab hier : felder, die 1:1 in aufp getragen werden muessen
double a ;
double auf_me ;
double auf_vk_pr ;
double auf_lad_pr ;
double prov_satz ;
char auf_me_bz[8]  ;
char lief_me_bz[8] ;
short me_einh_kun ;
short me_einh ;
short sa_kz_sint ;
short teil_smt ;
short dr_folge ;
double a_grund ;
double inh ;


} ;

struct BELPOS belpos , belpos_null ;

struct BELPOS * belpos_a ;
static int belposdim ;
static int belposmax ;
static int belposakt ;


int maskel ;	// Vorher-Zeichen war trenn_ma ( ==> "?" ) 
int dtestignore ;			// test-kz-ignorier-Schalter 

int TEST =  0 ;					// 0 * inaktiv
                                // 1 * lesesegment

int danz_seg  ;		// Anzahl Segmente einer Nachricht
int k ;

FILE * fpin ;		// File-Descriptor der Datei ( oder eben nicht ..... )

int ref_nr ;			// lfd. Nr in Nachricht 
int danz_nachrichten ;	// Anzahl Nachrichten

char eigen_iln [20] ;	

int daufstat  = 0 ;						// 0 =  noch nix
                                        // 1 = bgm=220 erkannt
                                        // 2 = Lieferdatum
                                        // 3 = Liefer-ILN
										// -3 = Liefer-ILN mistig
                                        // 4/-4 = Positions-infos vorhanden
int dauffolgestat = 0 ;					// folgt daufstat -> 3

int daufpstat = 0 ;						// 0 = neue Position
                                        // 1 = artikel vorhanden -1 : Artikel fehlerhaft
                                        // 2 = Menge vorhanden ( d.h. Posten komplett )

//alt und dann weg  :int rech_k_stat ;	// Stati innerhalb Kopf
// alt und dann weg :int rech_p_stat ;	// Stati innerhalp Pos

char lin_ean[16] ;


int  dtestmode ;
int  dabbruch = 0 ;
int  dkopfbug = 0 ;
int  dposbug  = 0 ;


int  pia2_gefunden ;
int  dalterstil = 0 ;  

char posmhd [12]    ;    //   date
int poshbkz         ;
char ftx_zzz_1[110] ;	// D1.0b  bis 99 Zeichen 
char ftx_zzz_2[110] ;    
char ftx_zzz_3[119] ;
char ftx_zzz_4[110] ; 
char ftx_zzz_5[110] ; 

/* ---->
field s_p_r     smallint
field s_p_w     char(10)
field s_p_b     char(10)
field dnachkpreis integer
<---- */

int dvertr_int_par  ;

int dwa_pos_txt ;
int dneunr ;
char dateinamex [110] ;

//  inputpuffer ( Datei )                       ippuffer
//      Segmentpuffer                           segpuffer
//          Datenelement                        depuffer
//              Gruppendatenelement             gdpuffer



char  ippuffer[CIPPUFFLEN + 5] ;     // Inputpuffer
int iprpoint ;		// Lesepointer Inputpuffer
int ippufflen ;  

char dsegpuffer [750] ;		// Segmentpuffer aus inputpuffer
int dsegwpoint ;			// Schreibpointer in Segmentpuffer
int dsegpufflen ;

char depuffer [550] ;		// Datenelementpuffer aus segpuffer
int dsegrpoint ;			// Lesepointer aus Datengsegment
int dewpoint ;				// Schreibpointer in Datenelement
int depufflen ;

char gdpuffer[150] ;		// Gruppendaten-Puffer aus depuffer
int derpoint ;			 	// Lesepointer aus depuffer
int gdwpoint ;				// Schreibpointer in Gruppendatenelementpuffer

int decount ;				// Nummer des Datenelements im Segment
int gdcount ;				// Nummer des Gruppendatenelements

// Das Ding mit 2-stelligen Namen ist irgendwie Quatsch , das kann eigentlich nur ein Fehler sein,
// lt. Normung gibt es nur 3-stellige Namen 

char  dsegname3 [10] ;                
int dsegguelt		;		// ob 2 oder 3-stell-Name ( 0/2/3 )
char  dsegname2 [10] ;

/* ---> further usage
= TABLE mart DIMENSION ( CMAXART )
= field a decimal (13,3)
= END
-------------- */

char dstring150 [152] ;
char dstring80  [ 82] ;
char dstring50  [ 52] ;
char dstring20  [ 22] ;

/** Trennzeichen mit aktuellen Werten laden **/

void trennz_laden(void )
{
	// Default-Vorladung, spaeter mal imRahmen des UNA_segments Ueberladung vorstellbar ...
   sprintf ( trenn_se , "%s" , TRENN_SE ) ;
   sprintf ( trenn_de , "%s" , TRENN_DE ) ;
   sprintf ( trenn_gd , "%s" , TRENN_GD ) ;
   sprintf ( trenn_ma , "%s" , TRENN_MA ) ;
   sprintf ( trenn_dz , "%s" , TRENN_DZ ) ;
}

/* ----> further usage ----->
= PROCEDURE martsort
=  field k1 integer
=  field k2 integer
=  table marttau like mart end
=    let k1 = 0
=    while ( ( k1 < (CMAXART - 1) ) and ( mart[k1].a > 0 ) )
=        if ( mart[ k1 + 1 ].a > 0 )
=            if ( mart[k1 + 1].a < mart[k1].a )
=                move table mart[k1] to table marttau
=                move table mart[k1 + 1] to table mart[k1]
=                move table marttau to table mart[k1 + 1]
=                if k1 > 0
=                   let k1 = k1 - 1
=                end
=                continue
=            end
=        end
=        let k1 = k1 + 1
=    end
= END


/* ----> 
    perform sys_par_holen ( "vertr_abr_par" )
    returning ( s_p_r, s_p_w s_p_b )
    if s_p_r = 0
        let s_p_w = "0"
    end
    let dvertr_int_par = s_p_w
        let dlgr_bestand = 0
        perform sys_par_holen ("lgr_bestand" )
                returning ( s_p_r, s_p_w, s_p_b )
        let dlgr_bestand = s_p_w
        if ((s_p_r = FALSE) or (s_p_w = "0"))
                let dlgr_bestand = 0
	end
    init ddateiname
    environment ("EDI", ddateiname )

    perform sys_par_holen ("wa_pos_txt" )
                returning ( dwa_pos_txt, s_p_w, s_p_b )

    if (dwa_pos_txt = FALSE)
        let dwa_pos_txt = 0
    else
        let dwa_pos_txt = s_p_w         // konvertiert implizit ...
    end

    perform sys_par_holen ("neunr" )
              returning ( dneunr, s_p_w, s_p_b )
    let dneunr = s_p_w
    if ((dneunr = FALSE) or (s_p_w = "0"))
        let dneunr = 0
    end

	perform sys_par_holen ("nachkpreis" )
                returning ( dnachkpreis, s_p_w, s_p_b )

    if (dnachkpreis = FALSE)
        let s_p_w = "2"
    end

    let dnachkpreis = 2

    if s_p_w = "3"
        let dnachkpreis = 3
    end
    if s_p_w = "4"
        let dnachkpreis = 4
    end
< ------ */


// binaer : byte fuer byte lesen und noch cr/lf wegfiltern, returnwert ist gleich Anzahl Zeichen im ippuffer

int ippufferread (void )
{
	char buffer [5];
	int ch ;
	ippufflen = 0 ;
	int inpoint = 0 ;
	if ( fpin == NULL ) return 0 ;

	memset ( ippuffer , '\0', sizeof ( ippuffer )) ;

	while ( inpoint < CIPPUFFLEN )
	{
		ch = fgetc( fpin ) ;
		buffer[0] = (char) ch ;
		if ( feof(fpin) == 0 )
		{
			if ( buffer[0] == '\n' || buffer[0] == 0x0D || buffer[0] == 0x0A )
			{
				// gegen ferkels ....
			}
			else
			{
				ippuffer[inpoint++] = buffer[0] ;
			}
			continue ;
		}
		else
		{
			fclose ( fpin ) ;
			fpin = NULL ;	// zu oder fertig 
			break ;
		}
	}
	return  inpoint  ;
}


// ######################################################################
// #### PROC.:  lesesegment
// #### liest naechstes Segment von Start bis Segment-Trenner bzw. crlf
// #### in den Empfangspuffer
// ######################################################################


int lesesegment ( CrecadvDlg * Cparent )
{
char hilfchar[2] ;
    sprintf ( hilfchar , "X") ;
    if ( iprpoint < ippufflen )
	{
        hilfchar[0] = ippuffer[iprpoint] ;
		while (( hilfchar[0] != trenn_se[0]) || maskel )	// alles vor dem Segmenttrenner ....
		{
			if ( maskel )
			{ 
				maskel = 0 ;
			}
			else
			{
				if ( hilfchar[0] == trenn_ma[0]) maskel = 1 ;
			}
            dsegpuffer[dsegwpoint] = hilfchar[0] ;
            dsegwpoint ++ ;
            iprpoint ++ ;
            if ( iprpoint >= ippufflen )
			{
                 ippufflen = ippufferread() ;
				 if ( ippufflen < 1 )
				 {
						maskel = 0 ;	// hier muss ja selbst bei error schluss sein !!!!
						break ;
				 }
				iprpoint = 0 ;
			}
            hilfchar[0]  = ippuffer[iprpoint] ;
		}
        if ( hilfchar[0] == trenn_se[0] )
		{	// maskel gibbet hier niemals 
			maskel = 0 ;
			iprpoint ++ ;
            if ( TEST == 1 )
			{
				MessageBox(NULL, dsegpuffer, " ", MB_OK|MB_ICONSTOP);
			}
			else
			{
	

				Cparent->myDisplayMessage( dsegpuffer ) ;
			}
			return  0 ;
		}
	}
    else
	{
		ippufflen = ippufferread() ;
		if ( ippufflen < 1 )
		{
			maskel = 0 ;
			return -1 ;
		}
		iprpoint = 0 ;
		hilfchar[0]  = ippuffer[iprpoint] ;
		while ((hilfchar[0] != trenn_se[0] )|| maskel )
		{
			if ( maskel )
			{ 
				maskel = 0 ;
			}
			else
			{
				if ( hilfchar[0] == trenn_ma[0]) maskel = 1 ;
			}
			dsegpuffer[dsegwpoint] = hilfchar[0] ;
			dsegwpoint ++ ;
			iprpoint ++ ;
			if ( iprpoint >= ippufflen )
			{
                return lesesegment(Cparent) ;
			}
			hilfchar[0]  = ippuffer[iprpoint] ;
		}
		if ( hilfchar[0] == trenn_se[0] )
		{
			maskel = 0 ;	// maskel gibbet hier niemals ....
			iprpoint ++ ;
			if ( TEST == 1 )
			{
				MessageBox(NULL, dsegpuffer , " ", MB_OK|MB_ICONSTOP);

			}
			else
			{

			Cparent->myDisplayMessage( dsegpuffer ) ;
			}
			return  0 ;
		}
	}
	return  -1 ;    // Syntax-Dummy
}

void UNA_laden (void)
{
	;
}

// Maximal dummy -Aktion 
void UNH_laden (void)
{
	
	;
}

void UNZ_laden (void)
{
	;
}


void UNT_laden ( void )
{    
  danz_nachrichten ++ ;
  if ( daufstat  /* > 2 */  )
  {
	if ( daufpstat )	// auch fehlerhafte Saetze schreiben !!!!
	{
	  write_belpos_a () ;
	}
  }
  else
  {
	  sprintf ( belkopf.bemerkung , "Unvollst�ndige Nachricht " ) ;
	  dkopfbug ++ ;
  }
  write_belkopf_a () ;
  dauffolgestat = daufstat = 0 ;	// 280808 : dauffolgestat auch reseten 
  daufpstat = 0 ;	// brutaler reset ....
 }

// Prototyp fuer Handling jedes Segments


/* ----->
// Ausfuehrung allgemeiner Aktionen bei Auftreten eines Segment-Typs

PROCEDURE XXX_laden
        let k = k
END

// alternativ : jedes Gruppendatenelement des Segments einlesen
// und in entsprechend interpretieren und gebnenfalls entsprechende
// Aktivitaeten einleiten

PROCEDURE XXX_arbeiten
field xxx_wert  char(35)
field xxx_typ1  char(5)
field xxx_typ2  char(5)
field xxx_pic   char(10)

    if ((decount = 2 ) and ( gdcount = 1 ))
        let xxx_typ1 = gdpuffer
    elseif (( decount = 2 ) and ( gdcount = 2 ))
        let xxx_wert = gdpuffer
    elseif (( decount = 2 ) and ( gdcount = 3 ))
        let xxx_pic  = gdpuffer

        // wenn alle Werte gelesen sind, startet hier die Interpretation
        // und gegbnenfalls ne entsprechende Aktion

    end
END
< ---- */

static char ftx_wert [110];
static char ftx_typ1 [6] ;

void FTX_arbeiten(void )
{
	if ( ! inblock ) return ;
//	if ( ! inbeleg ) return ;
if ( daufstat > 3 || daufstat < -3 )		// keine Pos-Texte auswerten 
	return ;
// maximal 5 Felder zu maximal 99 Zeichen

	if ((decount == 2 ) && ( gdcount == 1 ))
	{
        sprintf ( ftx_typ1,"%s", gdpuffer ) ;
		ftx_wert[0] = '\0' ;	// initialisieren
	}
	if (( decount == 5 ) && ( gdcount == 1 ))
	{
		sprintf ( ftx_wert , "%s", gdpuffer ) ;
		if (((strlen( ftx_wert )) > 0 ) && (! strncmp ( ftx_typ1 , "ZZZ",3 )))
		{
			sprintf ( ftx_zzz_1 ,"%s" , ftx_wert ) ;
		}
	}
	if (( decount == 5 ) && ( gdcount == 2 ))
	{
		sprintf ( ftx_wert , "%s" , gdpuffer ) ;
		if (((strlen( ftx_wert )) > 0 ) && (! strncmp ( ftx_typ1 , "ZZZ",3 )))
		{
			sprintf ( ftx_zzz_2 ,"%s" , ftx_wert ) ;
		}
	}
	if (( decount == 5 ) && ( gdcount == 3 ))
	{
		sprintf ( ftx_wert , "%s" , gdpuffer ) ;
		if (((strlen( ftx_wert )) > 0 ) && (! strncmp ( ftx_typ1 , "ZZZ",3 )))
		{
			sprintf ( ftx_zzz_3 ,"%s" , ftx_wert ) ;
		}
	}
	if (( decount == 5 ) && ( gdcount == 4 ))
	{
		sprintf ( ftx_wert , "%s" , gdpuffer ) ;
		if (((strlen( ftx_wert )) > 0 ) && (! strncmp ( ftx_typ1 , "ZZZ",3 )))
		{
			sprintf ( ftx_zzz_4 ,"%s" , ftx_wert ) ;
		}
	}
	if (( decount == 5 ) && ( gdcount == 5 ))
	{
		sprintf ( ftx_wert , "%s" , gdpuffer ) ;
		if (((strlen( ftx_wert )) > 0 ) && (! strncmp ( ftx_typ1 , "ZZZ",3 )))
		{
			sprintf ( ftx_zzz_5 ,"%s" , ftx_wert ) ;
		}
	}
}

static  char dtm_datum[15] ;
static  char dtm_datumb[15] ;
static  char dtm_typ  [15] ;
static  char dtm_wert [25] ;
static  char dtm_pic  [15] ;


void DTM_arbeiten(void)
{
  char hc4j [6] ;
  char hc2m [4] ;
  char hc2t [4] ; 
  char hc10 [20] ;


  char hc2min[4];
  char hc2hour[4];
  
	if ( ! inblock ) return ;
//	if ( inbeleg != 2 ) return ;
	if (( daufpstat ) ||( daufstat > 2 || daufstat < -2 ))
		return ;
// Nur im Kopf auswerten : MHD usw. kommen evtl. sapeter 

	if ((decount == 2 ) && ( gdcount == 1 ))
	{
        sprintf ( dtm_typ ,"%s",  gdpuffer ) ;
	}
	if (( decount == 2 ) && ( gdcount == 2 ))
	{
		sprintf ( dtm_wert ,"%s", gdpuffer ) ;
	}
	if (( decount == 2 ) && ( gdcount == 3 ))
	{
		sprintf ( dtm_pic ,"%s", gdpuffer ) ; 
		memset ( dtm_datum , '\0' , sizeof ( dtm_datum )) ;
		memset ( dtm_datumb , '\0' , sizeof ( dtm_datumb )) ;

		sprintf( hc2min,"00");	
		sprintf( hc2hour,"00");
		if (!strncmp( dtm_pic , "102",3 ) || !strncmp (dtm_pic, "203", 3 ) || !strncmp (dtm_pic, "718", 3 ) )
		{	// 102 : jjjjmmtt ; 203 : jjjjmmtthhmm ; 718 : jjjjmmttjjmmtt
			hc4j[0] = dtm_wert[0] ;
			hc4j[1] = dtm_wert[1] ;
			hc4j[2] = dtm_wert[2] ;
			hc4j[3] = dtm_wert[3] ;
			hc4j[4] = '\0' ;

			hc2m[0] = dtm_wert[4] ;
			hc2m[1] = dtm_wert[5] ;
			hc2m[2] = '\0' ;

			hc2t[0] = dtm_wert[6] ;
			hc2t[1] = dtm_wert[7] ;
			hc2t[2] = '\0' ;
			sprintf (hc10 , "%s.%s.%s", hc2t, hc2m, hc4j );
			sprintf ( dtm_datum, "%s", hc10 ) ;

			if (!strncmp( dtm_pic , "102",3 ))	// 291113
			{	// 102 : jjjjmmtt ; 203 : jjjjmmtthhmm ; 718 : jjjjmmttjjmmtt
				hc2hour[0] = dtm_wert[8] ;
				hc2hour[1] = dtm_wert[9] ;
				hc2hour[2] = '\0' ;
				hc2min[0] = dtm_wert[10] ;
				hc2min[1] = dtm_wert[11] ;
				hc2hour[2] = '\0' ;
			}
		}

		if (!strncmp( dtm_pic , "718",3 ) )
		{	// 102 : jjjjmmtt ; 203 : jjjjmmtthhmm ; 718 : jjjjmmttjjmmtt
			hc4j[0] = dtm_wert[8] ;
			hc4j[1] = dtm_wert[9] ;
			hc4j[2] = dtm_wert[10] ;
			hc4j[3] = dtm_wert[11] ;
			hc4j[4] = '\0' ;

			hc2m[0] = dtm_wert[12] ;
			hc2m[1] = dtm_wert[13] ;
			hc2m[2] = '\0' ;

			hc2t[0] = dtm_wert[14] ;
			hc2t[1] = dtm_wert[15] ;
			hc2t[2] = '\0' ;
			sprintf (hc10 , "%s.%s.%s", hc2t, hc2m, hc4j );
			sprintf ( dtm_datumb, "%s", hc10 ) ;
		}

//			if (( (daufstat = 1) or (daufstat = 2) )
//                and (( strlen ( dtm_datum)) >  3))

//				if  (dtm_typ == "361" )	// MHD/Mindest-Restlaufzeit   
//				let posmhd = dtm_datum
// MHD als Haltbarkeitstage
//				if ( dtm_typ = "364" ) and ( dtm_pic = "804")  let poshbkz = dtm_wert
 
//				if ( dtm_typ == "194" )								// geb_dat = dtm_datum 
//				if ( dtm_typ == "347" )								// schl_dat = dtm_datum 

//		if ( ! strncmp ( dtm_typ ,"2",1 ) && dtm_typ[1] == '\0' )	// Gefordertes Lieferdatum
//		{
//			sprintf ( belkopf.dtm_2_feld, "%s" , dtm_datum) ;	// genau "2" und nur "2" ......
//			sprintf ( belkopf.dtm_2_feld_t, "%s:%s", hc2hour, hc2min);	// 291113 
//			daufstat = 2 ; 
//		}

//		if ( ! strncmp ( dtm_typ ,"35",2 ) && dtm_typ[2] == '\0' )	// tats. Lieferdatum
//		if ( ! strncmp ( dtm_typ ,"263",3 )  )	// Abrechungszeitraum

		if ( ! strncmp ( dtm_typ , "137" ,3 ))						//Versendungsdatum
		{
			sprintf ( belkopf.dtm_137_feld, "%s" , dtm_datum) ;
		}
		if ( ! strncmp ( dtm_typ , "50" ,2  ) && dtm_typ[2] == '\0') //Datum der Vereinnahmung
		{
			sprintf ( belkopf.dtm_50_feld, "%s" , dtm_datum) ;
		}

// hier gibt es noch unendlich viele andere Datumstypen
	}
}

// 310311 A

char loc_typ[10];

void LOC_arbeiten(void)
{
 
	if ( ! inblock ) return ;
	if (( daufpstat ) ||( daufstat > 2 || daufstat < -2 ))
		return ;
// Nur im Kopf auswerten  ->LOC+7+2::92'<-
	// Location Nr. 2 , vergeben vom Besteller

	if ((decount == 2 ) && ( gdcount == 1 ))
	{
        sprintf ( loc_typ ,"%s",  gdpuffer ) ;
	}
	if (( decount == 3 ) && ( gdcount == 1 ))
	{
		char stelle[21] ;
		sprintf ( stelle ,"%s", gdpuffer ) ;
		if ( ! strncmp ( loc_typ ,"7" ,1 ))
					sprintf ( belkopf.loc_feld, "%s" , stelle) ;
	}
}

// 310311 E

char imd_typ [10] ;

void IMD_arbeiten  (void) 
{

	char imd_wert[110] ;
	if ( !inblock ) return ;
	if ( ! daufstat ) return ;

	if ((decount == 2 ) && ( gdcount == 1 ))
	{
        sprintf ( imd_typ ,"%s",  gdpuffer ) ;
		// A ,B , C , F 
	}
	if (( decount == 4 ) && ( gdcount == 1 ))
	{
		sprintf ( imd_wert ,"%s", gdpuffer ) ;
		if ( ! strncmp( imd_typ, "C" ,1 ) || ! strncmp ( imd_typ, "B" ,1))
		{
			if ( !strncmp ( imd_wert, "IN" , 2 ) && imd_wert[2] == '\0' )
			{
//				belpos.imd_in_aktiv = 1 ;
			}
			if ( !strncmp ( imd_wert, "RF" , 2 ) && imd_wert[2] == '\0' )
			{
//				belpos.imd_rf_aktiv = 1 ;
			}
		}
	}
	if (( decount == 4 ) && ( gdcount == 4 ))
	{
		if ( ! strncmp( imd_typ, "A" ,1 ) || ! strncmp ( imd_typ, "F" ,1))
		{	// "F" ist einfach etwas laenger
			{
				sprintf ( imd_wert ,"%s", gdpuffer ) ;
				imd_wert[68] = ' ' ;
				imd_wert[69] = '\0';	// das imd_a_feld hat bei mir nur max. 70 zeichen 
				sprintf ( belpos.imd_a_feld , "%s" , imd_wert ) ;
			}
		}
	}
}

void UNB_arbeiten (void) 
{	

  char isend_iln [20] ; 
  char iempf_iln [20] ;

    if (decount == 3  &&  gdcount == 1 )
	{
		memcpy ( &belkopf,&belkopf_null, sizeof(struct BELKOPF));
		memcpy ( &belkopf1,&belkopf_null, sizeof(struct BELKOPF));	// 260808
		memcpy ( &belkopf2,&belkopf_null, sizeof(struct BELKOPF));	// 260808
		memcpy ( &belkopf3,&belkopf_null, sizeof(struct BELKOPF));	// 260808
		memcpy ( &belkopf4,&belkopf_null, sizeof(struct BELKOPF));	// 260808
		dtestmode = 0 ;	// Initialisierung, aber nur am Anfang und nur einmalig .....
		dta_referenz = 0 ;
//		inbeleg = 0 ;
//		inposten = 0 ;

//		sprintf ( ftx_zzz_1 , "" ) ;
//		sprintf ( ftx_zzz_2 , "" ) ;
//		sprintf ( ftx_zzz_3 , "" ) ;
//		sprintf ( ftx_zzz_4 , "" ) ;
//		sprintf ( ftx_zzz_5 , "" ) ;

		pia2_gefunden = 0 ; 
		danz_nachrichten = 0 ;

		sprintf ( isend_iln ,"%s", gdpuffer ) ;
//		sprintf ( belkopf.nad_su_feld ,"%s", gdpuffer ) ;
		
		if ( strncmp (isend_iln , dta.iln ,13 ) )
		{
			sprintf ( belkopf.bemerkung,"unguelt.Absender:%s" , isend_iln ) ;
			dkopfbug ++ ;
			write_belkopf_a () ;
			if ( mogel )
				inblock = 1; 
		}
		else
		{
			inblock = 1 ;
		}
	}
    if ( decount == 4 && gdcount == 1 )
	{
        sprintf ( iempf_iln ,"%s", gdpuffer ) ;
//		sprintf ( belkopf.nad_by_feld ,"%s", gdpuffer ) ;
		if (strncmp ( iempf_iln , eigen_iln ,13 ) )
		{
			sprintf ( belkopf.bemerkung, "unguelt.Empfaenger%s", iempf_iln) ;
			dkopfbug ++ ;
			if ( !mogel )
				inblock = 0 ;
			write_belkopf_a () ;	// alle Infos sind da 1. Satz .......
		}
	}

	if ( decount == 6 && gdcount == 1 )
	{
		dta_referenz = atol ( gdpuffer) ;
	}

	if ( decount == 12 &&  gdcount == 1 )
        if ( gdpuffer[0] == '1' )
            if ( dtestignore == 1 )
                dtestmode = 0 ;
            else
                dtestmode = 1 ;
	if ( mogel ) inblock = 1 ;
}

/* nicht mehr genutzt ---->
void UNH_arbeiten (void ) 
{
	if ( !inblock ) return ;	
	if (decount == 2 && gdcount == 1 )
	{
		sprintf ( cnari,"%s", gdpuffer ) ; 
		if ( inposten )
		{
			sprintf ( belpos.bemerkung, "Notschrieb" ) ;
			write_belpos_a () ;
			inposten = 0 ;
			inbeleg = 0 ;
		}
// im TEXTBLOCK
		if ( inbeleg == 2 )
		{
			sprintf ( belkopf.bemerkung, "Notschrieb" ) ;
			write_belkopf_a () ;
			inbeleg = 0 ;
		}
		if ( inbeleg == 4 )
		{
			sprintf ( belfuss.bemerkung, "Notschrieb" ) ;
			write_belfuss_a () ;
			inbeleg = 0 ;
		}
		inbeleg = 1 ;	// die naechste Nachricht beginnt ...
	}
}
< ----- */
void UNS_arbeiten ( void )
{
	return ;
/* ---> nix mehr machen 
	if ( !inblock ) return ;
    if ( decount == 2 && gdcount == 1 )
	{	// alles nur einmalig .....
		if ( inbeleg == 2 )
		{
			if ( narityp == 393 )
			{
				write_belkopf_a() ;
				inbeleg = 4 ;
			}
// im TEXTBLOCK
			else
			{
				sprintf ( belkopf.bemerkung ,"Notschrieb" ) ;
				inbeleg = 0 ;
				write_belkopf_a() ;
				inposten = 0 ;
				return ;
			}
		}
		if ( inposten )
		{	// das sollte der Normalzustand sein 
			write_belpos_a() ;
			inposten = 0 ;
		}
// im TEXTBLOCK
		inbeleg = 4 ;
		belfusstaxaktpo = 0 ;
		sprintf ( belfusstaxaktiv, "" ) ;
		memcpy ( &belfuss,&belfuss_null, sizeof(struct BELFUSS));
		sprintf ( belfuss.bgm_typ , "%s" , belkopf.bgm_typ ) ;
		belfuss.bgm_nr = belkopf.bgm_nr ;

  	}
< ---- */
}

/* nicht mehr aktuell , nur noch UNT_laden wird ausgef�hrt 
void UNT_arbeiten (void ) 
{
	if ( ! inblock ) return ;
	if ( ! inbeleg ) return ;

	if (decount == 3 && gdcount == 1 )
	{
		if ( inposten )
		{
			if ( strcmp ( gdpuffer,cnari ))	// Es muss gleich sein ,sonst ERROR 
			{
				sprintf ( belpos.bemerkung, "Nari-Error" ) ;
			}
			else
			{
				sprintf ( belpos.bemerkung, "Notschrieb" ) ;
			}
			write_belpos_a () ;
			inposten = 0 ;
			inbeleg = 0 ;
		}
// im TEXTBLOCK
		if ( inbeleg == 2 )
		{

			if ( strcmp ( gdpuffer,cnari ))	// Es muss gleich sein ,sonst ERROR 
			{
				sprintf ( belkopf.bemerkung, "Nari-Error" ) ;
			}
			else
			{
				sprintf ( belkopf.bemerkung, "Notschrieb" ) ;
			}
			write_belkopf_a () ;
			inbeleg =  0;
		}
// im TEXTBLOCK
		if ( inbeleg == 4 )	// das ist regulaer evtl .steht ja nix drin z.B. bei Orders .....
		{
			if ( strcmp ( gdpuffer,cnari ))	// Es muss gleich sein ,sonst ERROR 
			{
				sprintf ( belfuss.bemerkung, "Nari-Error" ) ;
			}
			write_belfuss_a () ;
			inbeleg = 0 ;
		}
	}
}
< ---- */

void BGM_arbeiten (void ) 
{
	if ( ! inblock ) return ;
	if (( daufstat /* > 2 */  ) && ( daufpstat ))
	{	// schreibe Kopf und letzte letzte Position des vorherigen Postens
		// darf eigentlich nicht sein , da immer bei unt-segment geschrieben wrden soll ......
		write_belkopf_a () ;
		write_belpos_a () ;
//		memcpy ( &belpos,&belpos_null, sizeof(struct BELPOS));

	}

	// if ( ! inbeleg ) return ; // eigentlich ein problem ,aber das erlauben wir : loch um unh-segment
	
	if (decount == 2 && gdcount == 1 )
	{
		memcpy ( &belkopf,&belkopf_null, sizeof(struct BELKOPF));
		memcpy ( &belkopf1,&belkopf_null, sizeof(struct BELKOPF));	
		memcpy ( &belkopf2,&belkopf_null, sizeof(struct BELKOPF));
		memcpy ( &belkopf3,&belkopf_null, sizeof(struct BELKOPF));
		memcpy ( &belkopf4,&belkopf_null, sizeof(struct BELKOPF));
//		sprintf ( belkopf.bgm_typ,"%s", gdpuffer );
		narityp = atoi ( gdpuffer ) ;

		dauffolgestat = daufstat = 0 ;	// 280808 : dauffolgestat auch reseten

		if ( narityp == 351 )	// RECADV 
		{
		}
		else
		{
			sprintf ( belkopf.bemerkung ," fehlerhafter Nari-Typ : %d" , narityp ) ;
			dkopfbug++ ;
			write_belkopf_a () ;
		}
	}
	if ( decount == 3  && gdcount == 1 )
	{
		sprintf ( belkopf.auf_ext , "%s" ,  gdpuffer ) ;
		sprintf ( belpos.auf_ext , "%s" ,  gdpuffer ) ;
		belkopf.auf_int = 0 ;
		belkopf.bemerkung[0] = '\0' ;
		daufstat = 1 ;
	}
}

// ACHTUNG z.Z.: werden hier keine Gebinde-Infos gehandelt
// gegbnenfalls fuer abweichende Bestell / Liefereinheit abwickeln
// : hier steht gegebnenfalls immer die Gebinde-ILN d.h. die Bestell-Einheit

// Achtung : fuer Frankengut- bzw. Schwarwaldhof-Ablauf steht in lin die BP-Nummer anstelle einer ean
// und in PIA eine laufende Nummer , welche wieder zurueck gebeamt werden soll 
// FraGu/SW-Hof ist hier nicht durchrealisiert

char lin_eani [25] ;
char lin_pos [25] ;

void LIN_arbeiten ( void )
{
	if ( !inblock ) return ;
	if ( daufstat < 4 )
	{
//		write_belkopf_a() ;
		if ( daufstat == -3 )
			daufstat = -4 ;
		else
			daufstat = 4 ;
	}
    if ( decount == 2 && gdcount == 1 )
	{
		// init je Position nur einmalig
		if ( daufpstat == 2 ||daufpstat == -1 )	// korrekte oder fehlerhafte saetze
		{
			write_belpos_a() ;
// red.			memcpy ( &belpos,&belpos_null, sizeof(struct BELPOS));
			sprintf ( belpos.auf_ext  ,"%s" ,  belkopf.auf_ext ) ;

		}
        sprintf ( lin_pos , "%s" , gdpuffer ) ;
//        let aufp.posi_ext = lin_pos -> interessant bei referenzierten Orders ( Frankengut ) 
	}
    if ( decount == 4 && gdcount == 1 )
	{
        sprintf ( lin_ean ,"%s" , gdpuffer ) ;
		sprintf (belpos.lin_feld, "%s", lin_ean ) ;
		sprintf ( belpos.auf_ext ,"%s", belkopf.auf_ext ) ;
 	}
}

void MEA_arbeiten (void)
{
	return ;	// irrelevant fuer Auftrag
/* --->
	if ( ! inblock ) return ;
< ---- */
}


static char moa_typ [8] ;
void MOA_arbeiten ( void) 
{
	return ;	// irrelevant bei Auftrag 
	if ( ! inblock ) return ;
/* ---->
	if ( decount == 2 && gdcount == 1 )
	{
		sprintf ( moa_typ ,"%s", gdpuffer ) ;
	}
	if ( inbeleg == 2 )	// z.B. Abschlaege im Kopf 
	{
	}
// im TEXTBLOCK
	if ( inposten )
	{	// Positions-MOA : 203(AAB) bzw. 66(AAA)
		if ( decount == 2 && gdcount == 2)
		{	
			if ( ! strncmp ( moa_typ ,"203",3 ) || ! strncmp( moa_typ, "66" , 2 ))
			{
				belpos.moa_203_wert = atof ( gdpuffer ) ;
			}
		}
	}	// ende inposten
// im TEXTBLOCK
	if ( inbeleg == 4 )	// allerlei Fuss-Infos
	{
		if ( decount == 2 && gdcount == 2)
		{	
			if ( ! strncmp ( moa_typ ,"77",2 ) )
			{
				belfuss.moa_77_wert = atof ( gdpuffer ) ;
			}
// Hier die Alternative fuer bgm 393
			if ( ! strncmp ( moa_typ ,"86",2 ) )
			{
				switch ( belfusstaxaktpo )
				{
				case 1 :
					belfuss.moa_1_79_wert = atof ( gdpuffer ) ;
				  break ;
				case 2 :
					belfuss.moa_2_79_wert = atof ( gdpuffer ) ;
				  break ;
				default :
				    belfuss.moa_79_wert = atof ( gdpuffer ) ;
					belfuss.moa_77_wert = atof ( gdpuffer ) ;
// im TEXTBLOCK
				  break ;
				}
			}
			if ( ! strncmp ( moa_typ ,"79",2 ) )
			{
				switch ( belfusstaxaktpo )
				{
				case 1 :
					belfuss.moa_1_79_wert = atof ( gdpuffer ) ;
				  break ;
				case 2 :
					belfuss.moa_2_79_wert = atof ( gdpuffer ) ;
				  break ;
				default :
				    belfuss.moa_79_wert = atof ( gdpuffer ) ;
				  break ;
				}
			}
// im TEXTBLOCK
			if ( ! strncmp ( moa_typ ,"125",3 ) )
			{
				switch ( belfusstaxaktpo )
				{
				case 1 :
					belfuss.moa_1_125_wert = atof ( gdpuffer ) ;
				  break ;
				case 2 :
					belfuss.moa_2_125_wert = atof ( gdpuffer ) ;
				  break ;
				default :
					belfuss.moa_125_wert = atof ( gdpuffer ) ;
				  break ;
				}
			}
// im TEXTBLOCK
			if ( ! strncmp ( moa_typ ,"124",3 ) )
			{
				switch ( belfusstaxaktpo )
				{
				case 1 :
					belfuss.moa_1_124_wert = atof ( gdpuffer ) ;
				  break ;
				case 2 :
					belfuss.moa_2_124_wert = atof ( gdpuffer ) ;
				  break ;
				default :
					belfuss.moa_124_wert = atof ( gdpuffer ) ;
				  break ;
				}
			}
// im TEXTBLOCK
			if ( ! strncmp ( moa_typ ,"131",3 ) )
			{
				switch ( belfusstaxaktpo )
				{
				case 1 :
					belfuss.moa_1_131_wert = atof ( gdpuffer ) ;
				  break ;
				case 2 :
					belfuss.moa_2_131_wert = atof ( gdpuffer ) ;
				  break ;
				default :
					belfuss.moa_131_wert = atof ( gdpuffer ) ;
				  break ;
				}
			}
// im TEXTBLOCK
		}	// 2/2 
	}	// inbeleg == 4
< ---- */
}




// returnwerte : 0 = False, >0 = der jeweilige Token 

int kunbranerlaubt ( char testbran[4] )
{
	if ( testbran[0] == branche0 [0] && branche0[0] != '\0' )
	{
		if ( testbran[1] == branche0 [1] )
			return 1 ;
		else
			return 0 ;
	}

	if ( testbran[0] == branche1 [0] && branche1[0] != '\0' )
	{
		if ( testbran[1] == branche1 [1] )
			return 2 ;
		else
			return 0 ;
	}

	if ( testbran[0] == branche2 [0] && branche2[0] != '\0' )
	{
		if ( testbran[1] == branche2 [1] )
			return 3 ;
		else
			return 0 ;
	}

	if ( testbran[0] == branche3 [0] && branche3[0] != '\0' )
	{
		if ( testbran[1] == branche3 [1] )
			return 4 ;
		else
			return 0 ;
	}

	if ( testbran[0] == branche4 [0] && branche4[0] != '\0' )
	{
		if ( testbran[1] == branche4 [1] )
			return 5 ;
		else
			return 0 ;
	}

	return 0 ;
 
}

// returnwerte : 0 = erlaubt, 1 = nix passendes 
// Zusatzfeature : "*" irgendwo in der Sortimentsliste bedeutet : Gesamtsortiment erlaubt 
// d.h. nur die Branche ist noch limitierend

int smtzulaessig ( char testbran[4] )
{
	int token = kunbranerlaubt ( testbran) ;
	if ( !token ) return 1 ;	// nix passendes gefunden

	char * psuch ;
	char vergl[8] ;
	char teilsmt[8] ; 
	int schrp ,suchp ;
	
	vergl[0] = '\0' ;
	vergl[1] = '\0' ;
	vergl[2] = '\0' ;

	sprintf ( teilsmt,"%d" , a_bas.teil_smt ) ;

	if ( token == 1 )	psuch = teilsort0 ;
	if ( token == 2 )	psuch = teilsort1 ;
	if ( token == 3 )	psuch = teilsort2 ;
	if ( token == 4 )	psuch = teilsort3 ;
	if ( token == 5 )	psuch = teilsort4 ;

	schrp = suchp = 0 ;

	while ( psuch[suchp] != '\0' )
	{
		if ( psuch[suchp] == '*' )		// alle Teilsortimente erlaubt 	
			return 0 ;

		if ( psuch[suchp] < '0' || psuch[suchp] > '9' )
		{
			if ( schrp > 0 )
			{
				if ( vergl[0] == teilsmt[0] && vergl[1] == teilsmt[1] )
					return 0 ;
				schrp = 0 ;
				vergl[0] = '\0' ;
				vergl[1] = '\0' ;
				vergl[2] = '\0' ;
			}
		}
		else
		{
			vergl[schrp] = psuch[suchp] ;
			schrp ++ ;
			if ( schrp > 2 )
			{
				if ( vergl[0] == teilsmt[0] && vergl[1] == teilsmt[1] )
					return 0 ;
				schrp = 0 ;
				vergl[0] = '\0' ;
				vergl[1] = '\0' ;
				vergl[2] = '\0' ;
			}

		}
		suchp ++ ;
	}
	if ( schrp  )
	{
		if ( vergl[0] == teilsmt[0] && vergl[1] == teilsmt[1] )
				return 0 ;
	}
	return 1 ;
}

char  nad_typ [7] ;
char dpiln [20] ;

void NAD_arbeiten (void ) 
{

return ;

// Das wird alles nicht mehr ben�tigt, der Kunde kommt aus der LS-Nummer ( rff_on )
	if ( daufstat > 3 || daufstat < -3 )	return ;	// nur im Kopf benoetigt

// Achtung hier koennte spaeter mal eine weitere rekursion wegen Gutschriften usw. reinkommen ......

	if ( decount == 2 && gdcount == 1 )
	{
        sprintf ( nad_typ ,"%s", gdpuffer ) ;
	}
    if ( decount == 3 && gdcount == 1 )
	{
        sprintf ( dpiln ,"%s" , gdpuffer ) ;
		if ( ! strncmp ( nad_typ , "SU" , 2 ))
		{
//			sprintf ( belkopf.nad_su_feld, "%s", dpiln ) ;
		}
 		if ( ! strncmp ( nad_typ , "BY" , 2 ))
		{
//			sprintf ( belkopf.nad_by_feld, "%s", dpiln ) ;
		}

		if ( ! strncmp ( nad_typ , "DP" , 2 ) && daufstat == 2 )
		{
			sprintf ( belkopf.nad_dp_feld, "%s", dpiln ) ;
			teilsmtbeacht = 0 ;
			teilsmtnochtrennen = 0 ;	// 260808
			sprintf ( kun.iln , "%s" , dpiln ) ;
			kun.iln[12] = '*' ;
			kun.iln[13] = '\0' ;
			sprintf ( kun.plattform ,"%s", kun.iln ) ;
			kun.mdn = mdn.mdn ;
			int i = kun_class.openkun (dplattform) ;
			i = kun_class.lesekun (dplattform) ;

            if ( ! i )
			{

				belkopf.int_kun = kun.kun ;
				belkopf.int_adr = kun.adr2 ;
				sprintf (belkopf.kun_bran2 ,"%s", kun.kun_bran2 ) ;
				// so will es die Fa. Schiller fuer REWE, rest der Welt hat hier aufk.adr.adr_nam1 rumstehen 
				sprintf (belkopf.kun_krz1 ,"%s", kun.kun_krz1 ) ;
				belkopf.tou = kun.tou ;
				belkopf.vertr = kun.vertr1 ;

				i = kun_class.lesekun(dplattform) ;
                if ( ! i )
				{
					if ( !teilsmttrennen )	// 260808  
					{
                         sprintf ( belkopf.bemerkung , "Liefer-ILN mehrfach : %s" , dpiln ) ;
						 dkopfbug ++ ;
						 belkopf.int_kun = -1 ;
						 dauffolgestat = daufstat = -3 ;
					}
					else	// 260808 : das ist dann neu 
					{
						while ( !i )
						{
							if ( kunbranerlaubt ( kun.kun_bran2) ) 
							{
								teilsmtnochtrennen = 1 ;
								belkopf1.int_kun = kun.kun ;
								belkopf1.int_adr = kun.adr2 ;
								sprintf (belkopf1.kun_bran2 ,"%s", kun.kun_bran2 ) ;
								sprintf (belkopf1.kun_krz1 ,"%s", kun.kun_krz1 ) ;
								belkopf1.tou = kun.tou ;
								belkopf1.vertr = kun.vertr1 ;
								i = kun_class.lesekun(dplattform) ;
								break ;
							}
							i = kun_class.lesekun(dplattform) ;
						}

						while ( !i )
						{
							if ( kunbranerlaubt ( kun.kun_bran2) ) 
							{
								teilsmtnochtrennen = 2 ;
								belkopf2.int_kun = kun.kun ;
								belkopf2.int_adr = kun.adr2 ;
								sprintf (belkopf2.kun_bran2 ,"%s", kun.kun_bran2 ) ;
								sprintf (belkopf2.kun_krz1 ,"%s", kun.kun_krz1 ) ;
								belkopf2.tou = kun.tou ;
								belkopf2.vertr = kun.vertr1 ;
								i = kun_class.lesekun(dplattform) ;
								break ;
							}
								i = kun_class.lesekun(dplattform) ;
						}

						while ( !i )
						{
							if ( kunbranerlaubt ( kun.kun_bran2) ) 
							{
								teilsmtnochtrennen = 3 ;
								belkopf3.int_kun = kun.kun ;
								belkopf3.int_adr = kun.adr2 ;
								sprintf (belkopf3.kun_bran2 ,"%s", kun.kun_bran2 ) ;
								sprintf (belkopf3.kun_krz1 ,"%s", kun.kun_krz1 ) ;
								belkopf3.tou = kun.tou ;
								belkopf3.vertr = kun.vertr1 ;
								i = kun_class.lesekun(dplattform) ;
								break ;
							}
							i = kun_class.lesekun(dplattform) ;
						}

						while ( !i )
						{
							if ( kunbranerlaubt ( kun.kun_bran2) ) 
							{
								teilsmtnochtrennen = 4 ;
								belkopf4.int_kun = kun.kun ;
								belkopf4.int_adr = kun.adr2 ;
								sprintf (belkopf4.kun_bran2 ,"%s", kun.kun_bran2 ) ;
								sprintf (belkopf4.kun_krz1 ,"%s", kun.kun_krz1 ) ;
								belkopf4.tou = kun.tou ;
								belkopf4.vertr = kun.vertr1 ;
								i = kun_class.lesekun(dplattform) ;
								break ;
							}
							i = kun_class.lesekun(dplattform) ;
						}

						if ( !i)
						{	// mehr als 5 ist immer falsch 
							sprintf ( belkopf.bemerkung , "Liefer-ILN mehrfach : %s" , dpiln ) ;
							dkopfbug ++ ;
							belkopf.int_kun = -1 ;
							dauffolgestat = daufstat = -3 ;
						}
					}
				}
				else	// es gibt einfach nur den einen Kunden
				{
                    dauffolgestat = daufstat = 3 ;
                    teilsmtbeacht = tsmtgg_class.ateil_smt_setz()   ;
				}
			}
			else
			{
				sprintf ( belkopf.bemerkung , "Liefer-ILN fehlt : %s" , dpiln ) ;
				dkopfbug ++ ;
				belkopf.int_kun = 0 ;
				dauffolgestat = daufstat = -3 ;
			}
		}
	}

}

static  char pia_typ1 [12];
static  char pia_typ2 [12];
static  char pia_typ3 [12];
static  char pia_puffer2 [25] ;
static  char pia_puffer3 [25] ;

void PIA_arbeiten ( void ) 
{
	if ( ! inblock ) return ;

    if ( decount == 2 && gdcount == 1 )
	{
		// "1" oder "5" erwartet 
        sprintf ( pia_typ1 ,"%s", gdpuffer ) ;
        memset ( pia_puffer2 , '\0' , sizeof ( pia_puffer2 )) ;
        memset ( pia_puffer3 , '\0' , sizeof ( pia_puffer3 )) ;
        memset ( pia_typ2 , '\0' , sizeof ( pia_typ2 )) ;
        memset ( pia_typ3 , '\0' , sizeof ( pia_typ3 )) ;
	}
    if ( decount == 3 && gdcount == 1 )
	{
        sprintf ( pia_puffer2 ,"%s", gdpuffer ) ;
	}
	if ( decount == 3 && gdcount == 2 )
	{
        sprintf ( pia_typ2 ,"%s", gdpuffer ) ;
		// Typ zu pia_puffer2 ( z.B. IN, SA oder BP ..... )

		if ( ! strncmp( pia_typ1, "1" , 1 ) || ! strncmp ( pia_typ1 , "5" , 1 ))
		{
			if ( ! strncmp ( pia_typ2 , "IN" ,2 ) || !strncmp ( pia_typ2, "BP" ,2 ))
			{
				sprintf ( belpos.pia_bp_feld, "%s" , pia_puffer2 ) ;	// Kunden-Bestellnummer
			}
			if ( ! strncmp ( pia_typ2 , "SA" ,2 )) 
			{
				sprintf ( belpos.pia_sa_feld, "%s" , pia_puffer2 ) ;	// fit-Artikel-Nummer
			}
		}
	}

    if ( decount == 4 && gdcount == 1 )
	{
        sprintf ( pia_puffer3 ,"%s" , gdpuffer ) ;
	}
    if ( decount == 4 && gdcount == 2 )
	{
        sprintf (pia_typ3 ,"%s", gdpuffer );
		// Typ zu pia_puffer3 ( z.B. IN, SA oder BP ..... )

		if ( ! strncmp( pia_typ1, "1" , 1 ) || ! strncmp ( pia_typ1 , "5" , 1 ))
		{
			if ( ! strncmp ( pia_typ3 , "IN" ,2 ) || !strncmp ( pia_typ3, "BP" ,2 ))
			{
				sprintf ( belpos.pia_bp_feld, "%s" , pia_puffer3 ) ;	// Kunden-Artikel-Nummer
			}
			if ( ! strncmp ( pia_typ3 , "SA" ,2 ) )
			{
				sprintf ( belpos.pia_sa_feld, "%s" , pia_puffer3 ) ;	// fit-Artikel-Nummer
			}
		}
	}
 }


static char cux_typ1 [8] ;
static char cux_typ2 [8] ;

void CUX_arbeiten (void )
{
	return ;	// koennte ja DM oder EURO sein ..... z.Z nur dummy ....
/* ---->
    if ( decount == 2 && gdcount == 1)
	{
        sprintf ( cux_typ1 ,"%s", gdpuffer ) ;
        memset ( cux_typ2 , '\0', sizeof ( cux_typ2 )) ;
	}
    if ( decount == 2 && gdcount == 2 )
	{
        sprintf ( cux_typ2 ,"%s", gdpuffer ) ;
< ----- */

/* ---->
        if (( cux_typ1 = "2" ) and ( cux_typ2 = "DEM" ))
            let daufwaehr = 1
        elseif ((cux_typ1 = "2" ) and ( cux_typ2 = "EUR" ))
            let daufwaehr = 2
        end
< ----- */

// 		}        // elseif verschiedene Felder
}

// Augekoppelt von Willes Preisholerei aus partyservice\51100
// prinzipiell muss preisewa.dll existieren , anderes geht prinzipiell NICHT los 

void holepreis (void ) 
{
       int boolretco;
       short sa = 0 ;
       double pr_ek = 0.0 ;
       double pr_vk = 0.0 ;
       
//	   sqldatdst ( & aufk.lieferdat , belkopf.dtm_2_feld ) ;
	   sqldatdst ( & aufk.lieferdat , belkopf.dtm_137_feld ) ; // wegen Syntax

//	   WaPreis.SetAufArt (atoi (auf_art));
//	   WaPreis.SetAufArt ( 0 );

//       if (aufpListe.DllPreise.PriceLib != NULL && 
//		   aufpListe.DllPreise.preise_holen != NULL)
//	   {
//				  dsqlstatus = (aufpListe.DllPreise.preise_holen) (mdn.mdn, 
       if (DllPreise.PriceLib != NULL && 
		   DllPreise.preise_holen != NULL)
	   {
				  boolretco = (DllPreise.preise_holen) (mdn.mdn, 
                                          0,
                                          0,	// kun_fil
                                          kun.kun,
                                          a_bas.a,
										  belkopf.dtm_137_feld,
//										  belkopf.dtm_2_feld,
										  &sa, 
										  &pr_ek,
										  &pr_vk);
	   }
	   else
	   {
		   boolretco = 0 ;
		   sa = 0 ;
		   pr_ek = 0 ;
		   pr_vk = 0 ;
		   
	   } 
/* konventionelle Preisleserei abgeschaltet  
   {
=				dsqlstatus = WaPreis.preise_holen (mdn.mdn,
=                                          0,
=                                          0,	// kun_fil
=                                          kun.kun,
=                                          a_bas.a,
=										  belkopf.dtm_2_feld,
=                                          &sa, 
=										  &pr_ek,
=										  &pr_vk) ;
	   }
< ------ */
       if (boolretco )
       {
			belpos.auf_vk_pr  = pr_ek;
			belpos.auf_lad_pr = pr_vk;
			belpos.sa_kz_sint = sa;

// noch zu tun ?!
//	             strcpy (aufp.kond_art,   WaPreis.GetKondArt ());
//				 aufp.a_grund   =  WaPreis.GetAGrund ();
//				 aufpListe.FillKondArt (aufp.kond_art);
	   }
/* ---> noch zu tun ?!
	   aufpListe.SetAufVkPr (pr_ek);
	   aufpListe.FillWaehrung (pr_ek, pr_vk);
	   aufpListe.SetPreise ();
< ---- */


}
void artikellesen ( void )
{

// 260808 : hier gegbnenfalls den gesamten Auftrag  auf einen Kunden zwicken
// 260808 : Bedingung : Sortimentsreine Bestellung 

	// Artikel aufloesen , a_bas, Preis, Provision, Mengeneinheiten
	int gefunden = 0 ;

	belpos.a = a_kun.a = a_kun_gx.a = a_bas.a = 0.0 ;

	int linlang = (int) strlen ( belpos.lin_feld ) ;
// die felder ean und ean_vk in den Tabellen a_kun und a_kun_gx sind decimal-felders 

	if ( doption == O_EDEKA )
	{	// sonderbehandlung "ohne Nummer"
		if ( linlang < 14 )
		{
			if ( ! strncmp ( belpos.lin_feld, "0000000000000", linlang ))
			{
				if ((strlen ( belpos.imd_a_feld )) > 0 )
				{
					belpos.imd_a_feld[59] = '\0' ;
					sprintf ( belpos.bemerkung ,"%s", belpos.imd_a_feld ) ;
					gefunden = 1 ;
					belpos.a = a_kun_gx.a = a_kun.a = a_bas.a = 0 ;
					dposbug ++ ;
					daufpstat = -1 ;	// fehlerhafte Artikel
				}
			}

		}
	}

	if ( doption == O_REWE ||doption == O_METRO )
	{
		// REWE : im ROSI-Teil steht es folgendermassen drin : LIN-EAN irrelevant, es wird immer mittels SA-Nummer 
			// bestellt , ersatzweise versuche ich dann (falls vorhanden) trotzdem noch die interne Nummer aus der ean 
			// zu extrahieren -> wenn das auch nicht klappt, dann gibbet halt nen Fehler
		// METRO : im ROSI-Teil steht es folgendermassen drin : LIN-EAN  ist kramig ( 28-er , Ersatz-EAN usw.) ,relevant 
			// ist nur BP, ersatzweise wird noch nach SA gesucht

	}
	else
	{
		if ( !gefunden )
		{
			double istean,vonean,bisean ;
			int retco = -1 ;

			if ( linlang == 8 || linlang == 12 || linlang == 13  )
			{
				istean = atof( belpos.lin_feld ) ;
				if ( istean > 99999 )	// 0 und Unsinn wegschubsen
				{
					if ( linlang == 8 )
					{
						istean = vonean = bisean = atof ( belpos.lin_feld ) ;
					}

					if ( linlang == 12 )
					{
						istean = atof ( belpos.lin_feld ) ;
						vonean = istean * 10.0 ;
						bisean = vonean + 9.0 ;
					}
					if ( linlang == 13 )
					{
						istean = atof ( belpos.lin_feld ) ;
						char hilfe20 [20] ; sprintf ( hilfe20, "%s" ,belpos.lin_feld ) ;
						hilfe20[12] = '\0' ;
						istean = atof ( hilfe20 ) ;	// istean ist nur 12 stellig .....
						vonean = istean * 10.0 ;
						bisean = vonean + 9.0 ;
					}
					if ( doption == O_FEGRO)
					{
						retco = a_kun_gx_class.open_ean_a_kun_gx ( istean, vonean,bisean ) ;
						if ( !retco) retco = a_kun_gx_class.lese_ean_a_kun_gx () ;
						if ( !retco )
						{
							a_bas.a = a_kun_gx.a ; 
							retco = a_kun_class.opena_kun(a_bas.a ) ;
							if ( ! retco ) retco = a_kun_class.lesea_kun () ;
							if ( retco )
							{
								a_kun.a = 0.0 ;
								a_kun.li_a[0]  = '\0' ;
								retco = 0 ;
							}
						}
					}
					else
					{

						if ( teilsmtnochtrennen )	// 260808  
						{	
							retco = 1 ;
// 260808 5. Kunde
							if ( teilsmtnochtrennen > 3 )	// Start der Suche von hinten
							{ 
								kun.kun = belkopf4.int_kun ;
								kun.mdn = mdn.mdn ;
								sprintf ( kun.kun_bran2 , "%s" , belkopf4.kun_bran2 ) ;
								retco = a_kun_class.open_ean_a_kun ( istean, vonean,bisean ) ;
								if ( !retco) retco = a_kun_class.lese_ean_a_kun () ;
								if ( !retco )
								{
									a_bas.a = a_kun.a ;
									retco = a_bas_class.opena_bas () ;
									retco = a_bas_class.lesea_bas () ;
									if ( !retco ) 
									{
										retco = smtzulaessig ( kun.kun_bran2 ) ;
									}
									if ( !retco ) 
									{
										memcpy ( &belkopf, &belkopf4, sizeof ( struct BELKOPF )) ;

										kun_class.lesedefkun() ;
										teilsmtbeacht = tsmtgg_class.ateil_smt_setz()   ;
										dauffolgestat = daufstat = 4 ;
 										teilsmtnochtrennen = 0 ;
									}
								}
							}

// 260808 4. Kunde
							if ( teilsmtnochtrennen > 2 )	// Start der Suche von hinten
							{ 
								kun.kun = belkopf3.int_kun ;
								kun.mdn = mdn.mdn ;
								sprintf ( kun.kun_bran2 , "%s" , belkopf3.kun_bran2 ) ;
								retco = a_kun_class.open_ean_a_kun ( istean, vonean,bisean ) ;
								if ( !retco) retco = a_kun_class.lese_ean_a_kun () ;
								if ( !retco )
								{
									a_bas.a = a_kun.a ;
									retco = a_bas_class.opena_bas () ;
									retco = a_bas_class.lesea_bas () ;
									if ( !retco ) 
									{
										retco = smtzulaessig ( kun.kun_bran2 ) ;
									}
									if ( !retco ) 
									{
										memcpy ( &belkopf, &belkopf3, sizeof ( struct BELKOPF )) ;
										kun_class.lesedefkun() ;
										teilsmtbeacht = tsmtgg_class.ateil_smt_setz()   ;
									    dauffolgestat = daufstat = 4 ;
 										teilsmtnochtrennen = 0 ;
									}

								}

							}

// 260808 3. Kunde
							if ( teilsmtnochtrennen > 1 )	// Start der Suche von hinten
							{ 
								kun.kun = belkopf2.int_kun ;
								kun.mdn = mdn.mdn ;
								sprintf ( kun.kun_bran2 , "%s" , belkopf2.kun_bran2 ) ;
								retco = a_kun_class.open_ean_a_kun ( istean, vonean,bisean ) ;
								if ( !retco) retco = a_kun_class.lese_ean_a_kun () ;
								if ( !retco )
								{
									a_bas.a = a_kun.a ;
									retco = a_bas_class.opena_bas () ;
									retco = a_bas_class.lesea_bas () ;
									if ( !retco ) 
									{
										retco = smtzulaessig ( kun.kun_bran2 ) ;
									}
									if ( !retco ) 
									{
										memcpy ( &belkopf, &belkopf2, sizeof ( struct BELKOPF )) ;
										kun_class.lesedefkun() ;
										teilsmtbeacht = tsmtgg_class.ateil_smt_setz()   ;
					                    dauffolgestat = daufstat = 4 ;
										teilsmtnochtrennen = 0 ;
									}
								}
							}
// 260808 2. Kunde
							if ( teilsmtnochtrennen > 0 )	// Start der Suche von hinten
							{ 
								kun.kun = belkopf1.int_kun ;
								kun.mdn = mdn.mdn ;
								sprintf ( kun.kun_bran2 , "%s" , belkopf1.kun_bran2 ) ;
								retco = a_kun_class.open_ean_a_kun ( istean, vonean,bisean ) ;
								if ( !retco) retco = a_kun_class.lese_ean_a_kun () ;
								if ( !retco )
								{
									a_bas.a = a_kun.a ;
									retco = a_bas_class.opena_bas () ;
									retco = a_bas_class.lesea_bas () ;
									if ( !retco ) 
									{
										retco = smtzulaessig ( kun.kun_bran2 ) ;
									}
									if ( !retco ) 
									{
										memcpy ( &belkopf, &belkopf1, sizeof ( struct BELKOPF )) ;
										kun_class.lesedefkun() ;
										teilsmtbeacht = tsmtgg_class.ateil_smt_setz()   ;
										dauffolgestat = daufstat = 4 ;
										teilsmtnochtrennen = 0 ;
									}
								}
							}
// 260808 Notbremse
							if ( teilsmtnochtrennen > 0 )	// nix gefunden, wenigstens konsistent
							{
								kun.kun = belkopf.int_kun ;
								kun.mdn = mdn.mdn ;
								sprintf ( kun.kun_bran2 , "%s" , belkopf.kun_bran2 ) ;
								kun_class.lesedefkun() ;
								teilsmtbeacht = tsmtgg_class.ateil_smt_setz()   ;
								dauffolgestat = daufstat = 4 ;
								retco = a_kun_class.open_ean_a_kun ( istean, vonean,bisean ) ;
								if ( !retco) retco = a_kun_class.lese_ean_a_kun () ;
							 	teilsmtnochtrennen = 0 ;	// dann auf einem doofen Kunden rummachen
							}
						}
						else	// so einfach war das bisher ....
						{
							retco = a_kun_class.open_ean_a_kun ( istean, vonean,bisean ) ;
							if ( !retco) retco = a_kun_class.lese_ean_a_kun () ;
						}
						if ( retco )
						{
							a_kun.a = 0.0 ;
							a_kun.li_a[0]  = '\0' ;
						}
						else
							a_bas.a = a_kun.a ;
					}

					if ( !retco )
					{
						if ( dli_a_edi )
						{
							double hilfa = atof ( a_kun.li_a ) ;
							if ( hilfa > 0.5 )
							{
								a_bas.a = hilfa ;
							}
						}
					}

					if ( !retco )
					{
						retco = a_bas_class.opena_bas () ;
						retco = a_bas_class.lesea_bas () ;
						if ( !retco) gefunden = 1 ;
						else
							a_bas.a = 0.0 ;
					}
					else
						a_kun_gx.a = a_kun.a = 0.0 ;
				}	// Werte groesser 99999  
			}	// Suche nach gueltiger ean
		}	// if ( !gefunden )
	}		// if (O_REWE | O_METRO )

	if ( ! gefunden ) 
	{
		if ((strlen ( belpos.pia_sa_feld )) > 0 )	// eigne Artikelnummer 
		{
			a_bas.a = atof ( belpos.pia_sa_feld ) ;
			int retco = a_bas_class.opena_bas ( ) ;
			if ( !retco ) retco = a_bas_class.lesea_bas () ;
			if ( !retco )
			{
				if ( teilsmtnochtrennen )	// 260808  
				{
					if ( teilsmtnochtrennen > 3 )
					{
						if ( ! smtzulaessig ( belkopf4.kun_bran2 )) 
						{
							kun.mdn = mdn.mdn ;
							kun.kun = belkopf4.int_kun ;
							int retkun = kun_class.lesedefkun() ;
							if ( ! retkun )
							{
								memcpy ( &belkopf, &belkopf4, sizeof ( struct BELKOPF )) ;
								teilsmtbeacht = tsmtgg_class.ateil_smt_setz()   ;
								dauffolgestat = daufstat = 4 ; 
								teilsmtnochtrennen = 0 ;
							}
						}
					}

					if ( teilsmtnochtrennen > 2 )
					{
						if ( ! smtzulaessig ( belkopf3.kun_bran2 )) 
						{
							kun.mdn = mdn.mdn ;
							kun.kun = belkopf3.int_kun ;
							int retkun = kun_class.lesedefkun() ;
							if ( ! retkun )
							{
								memcpy ( &belkopf, &belkopf3, sizeof ( struct BELKOPF )) ;
								teilsmtbeacht = tsmtgg_class.ateil_smt_setz()   ;
								dauffolgestat = daufstat = 4 ; 
								teilsmtnochtrennen = 0 ;
							}
						}
					}
					if ( teilsmtnochtrennen > 1 )
					{
						if ( ! smtzulaessig ( belkopf2.kun_bran2 )) 
						{
							kun.mdn = mdn.mdn ;
							kun.kun = belkopf2.int_kun ;
							int retkun = kun_class.lesedefkun() ;
							if ( ! retkun )
							{
								memcpy ( &belkopf, &belkopf2, sizeof ( struct BELKOPF )) ;
								teilsmtbeacht = tsmtgg_class.ateil_smt_setz()   ;
								dauffolgestat = daufstat = 4 ; 
								teilsmtnochtrennen = 0 ;
							}
						}
					}
					if ( teilsmtnochtrennen > 0 )
					{
						if ( ! smtzulaessig ( belkopf1.kun_bran2 )) 
						{
							kun.mdn = mdn.mdn ;
							kun.kun = belkopf1.int_kun ;
							int retkun = kun_class.lesedefkun() ;
							if ( ! retkun )
							{
								memcpy ( &belkopf, &belkopf1, sizeof ( struct BELKOPF )) ;
								teilsmtbeacht = tsmtgg_class.ateil_smt_setz()   ;
								dauffolgestat = daufstat = 4 ; 
								teilsmtnochtrennen = 0 ;
							}
						}
					}
					if ( teilsmtnochtrennen > 0 )
					{
						kun.mdn = mdn.mdn ;
						kun.kun = belkopf.int_kun ;
						int retkun = kun_class.lesedefkun() ;
						teilsmtbeacht = tsmtgg_class.ateil_smt_setz()   ;
						dauffolgestat = daufstat = 4 ; 
						teilsmtnochtrennen = 0 ;
					}
				}

				retco = a_kun_class.opena_kun( a_bas.a ) ;
				if ( !retco ) retco = a_kun_class.lesea_kun () ;
				if ( retco ) a_kun_gx.a = a_kun.a = 0.0 ;
				gefunden = 1 ;
			}
			else
				a_bas.a = 0.0 ;
		}
	}
	if ( ! gefunden ) 
	{
		if ((strlen ( belpos.pia_bp_feld )) > 0 )	// Kunden-Artikelnummer 
		{
			int retco ;
			if ( teilsmtnochtrennen )	// 260808 
			{	
				retco = 1 ;
// 260808 5. Kunde
				if ( teilsmtnochtrennen > 3 )	// Start der Suche von hinten
				{ 
					kun.kun = belkopf4.int_kun ;
					kun.mdn = mdn.mdn ;
					sprintf ( kun.kun_bran2 , "%s" , belkopf4.kun_bran2 ) ;
					retco = a_kun_class.open_bp_a_kun (belpos.pia_bp_feld ) ;
					if ( !retco ) retco = a_kun_class.lese_bp_a_kun () ;
					if ( !retco )
					{
						a_bas.a = a_kun.a ;
						retco = a_bas_class.opena_bas () ;
						retco = a_bas_class.lesea_bas () ;
						if ( !retco ) 
						{
							retco = smtzulaessig ( kun.kun_bran2 ) ;
						}
						if ( !retco ) 
						{
							memcpy ( &belkopf, &belkopf4, sizeof ( struct BELKOPF )) ;
							kun_class.lesedefkun() ;
							teilsmtbeacht = tsmtgg_class.ateil_smt_setz()   ;
							dauffolgestat = daufstat = 4 ;
							teilsmtnochtrennen = 0 ;
						}
					}
				}

// 260808 4. Kunde
				if ( teilsmtnochtrennen > 2 )	// Start der Suche von hinten
				{ 
					kun.kun = belkopf3.int_kun ;
					kun.mdn = mdn.mdn ;
					sprintf ( kun.kun_bran2 , "%s" , belkopf3.kun_bran2 ) ;
					retco = a_kun_class.open_bp_a_kun (belpos.pia_bp_feld ) ;
					if ( !retco ) retco = a_kun_class.lese_bp_a_kun () ;
					if ( !retco )
					{
						a_bas.a = a_kun.a ;
						retco = a_bas_class.opena_bas () ;
						retco = a_bas_class.lesea_bas () ;
						if ( !retco ) 
						{
							retco = smtzulaessig ( kun.kun_bran2 ) ;
						}
						if ( !retco ) 
						{
							memcpy ( &belkopf, &belkopf3, sizeof ( struct BELKOPF )) ;
							kun_class.lesedefkun() ;
							teilsmtbeacht = tsmtgg_class.ateil_smt_setz()   ;
						    dauffolgestat = daufstat = 4 ;
							teilsmtnochtrennen = 0 ;
						}
					}
				}

// 260808 3. Kunde
				if ( teilsmtnochtrennen > 1 )	// Start der Suche von hinten
				{ 
					kun.kun = belkopf2.int_kun ;
					kun.mdn = mdn.mdn ;
					sprintf ( kun.kun_bran2 , "%s" , belkopf2.kun_bran2 ) ;
					retco = a_kun_class.open_bp_a_kun (belpos.pia_bp_feld ) ;
					if ( !retco) retco = a_kun_class.lese_bp_a_kun () ;
					if ( !retco )
					{
						a_bas.a = a_kun.a ;
						retco = a_bas_class.opena_bas () ;
						retco = a_bas_class.lesea_bas () ;
						if ( !retco ) 
						{
							retco = smtzulaessig ( kun.kun_bran2 ) ;
						}
						if ( !retco ) 
						{
							memcpy ( &belkopf, &belkopf2, sizeof ( struct BELKOPF )) ;
							kun_class.lesedefkun() ;
							teilsmtbeacht = tsmtgg_class.ateil_smt_setz()   ;
			                dauffolgestat = daufstat = 4 ;
							teilsmtnochtrennen = 0 ;
						}
					}
				}
// 260808 2. Kunde
				if ( teilsmtnochtrennen > 0 )	// Start der Suche von hinten
				{ 
					kun.kun = belkopf1.int_kun ;
					kun.mdn = mdn.mdn ;
					sprintf ( kun.kun_bran2 , "%s" , belkopf1.kun_bran2 ) ;
					retco = a_kun_class.open_bp_a_kun (belpos.pia_bp_feld ) ;
					if ( !retco) retco = a_kun_class.lese_bp_a_kun () ;
					if ( !retco )
					{
						a_bas.a = a_kun.a ;
						retco = a_bas_class.opena_bas () ;
						retco = a_bas_class.lesea_bas () ;
						if ( !retco ) 
						{
							retco = smtzulaessig ( kun.kun_bran2 ) ;
						}
						if ( !retco ) 
						{
							memcpy ( &belkopf, &belkopf1, sizeof ( struct BELKOPF )) ;
							kun_class.lesedefkun() ;
							teilsmtbeacht = tsmtgg_class.ateil_smt_setz()   ;
							dauffolgestat = daufstat = 4 ;
							teilsmtnochtrennen = 0 ;
						}
					}
				}
// 260808 Notbremse
				if ( teilsmtnochtrennen > 0 )	// nix gefunden, wenigstens konsistent
				{
					kun.kun = belkopf.int_kun ;
					kun.mdn = mdn.mdn ;
					sprintf ( kun.kun_bran2 , "%s" , belkopf.kun_bran2 ) ;
					kun_class.lesedefkun() ;
					teilsmtbeacht = tsmtgg_class.ateil_smt_setz()   ;
					retco = a_kun_class.open_bp_a_kun (belpos.pia_bp_feld ) ;
					if ( !retco) retco = a_kun_class.lese_bp_a_kun () ;
					teilsmtnochtrennen = 0 ;	// dann auf einem doofen Kunden rummachen
				}
			}
			else
			{
				retco = a_kun_class.open_bp_a_kun (belpos.pia_bp_feld ) ;
				if ( !retco ) retco = a_kun_class.lese_bp_a_kun () ;
			}	
			if ( !retco )
			{
				a_bas.a = a_kun.a ;
				retco = a_bas_class.opena_bas () ;
				retco = a_bas_class.lesea_bas () ;
				if ( !retco)
				{
					gefunden = 1 ;
				}
				else
					a_bas.a = 0.0 ;
			}
			else
				a_kun_gx.a = a_kun.a = 0.0 ;
		}
	}
	if ( !gefunden && doption == O_REWE )	// vorerst nur bei Rewe "Rest of ean" -Mechanismus 
	{
		char hilfe10[10] ;
		if ( linlang > 11 )	// nur komplette ean auswerten .....
		{
			hilfe10[0] = belpos.lin_feld[7] ;
			hilfe10[1] = belpos.lin_feld[8] ;
			hilfe10[2] = belpos.lin_feld[9] ;
			hilfe10[3] = belpos.lin_feld[10] ;
			hilfe10[4] = belpos.lin_feld[11] ;
			hilfe10[5] = '\0' ;
			a_bas.a = atof ( hilfe10 ) ;
			int retco = a_bas_class.opena_bas ( ) ;
			if ( !retco ) retco = a_bas_class.lesea_bas () ;
			if ( !retco )
			{
				retco = a_kun_class.opena_kun( a_bas.a ) ;
				if ( !retco ) retco = a_kun_class.lesea_kun () ;
				if ( retco ) a_kun_gx.a = a_kun.a = 0.0 ;
				gefunden = 1 ;
			}
			else
				a_bas.a = 0.0 ;
		}
	}
	if ( gefunden && a_bas.ghsperre == 1 )	// 011013
	{
		sprintf ( belpos.bemerkung , "Art.Error:Grosshandelsperre %1.0f" , a_bas.a  ) ;	// belpos-Bemerkung f�hrt dazu, das kein aufp-satz entsteht
//		dposbug ++ ;
//		daufpstat = -1 ;	

	}
	else	// 011013 : ab hier bisheriger Zustand
	{
		if ( gefunden )
		{

			belpos.a = a_bas.a ;
			holepreis () ;
			belpos.prov_satz = prov_satz_class.prov_satz_hol (
				a_bas.a ,
				a_bas.ag ,
				a_bas.wg ,
				a_bas.hwg ,
				kun.kun ,
				kun.kun_bran2 ,
				kun.vertr1 ,
				kun.mdn ,
				aufp.sa_kz_sint 
				) ;
			daufpstat = 1 ;
		}
		else
		{
			a_bas.a = belpos.a = 0.0 ;
			sprintf ( belpos.bemerkung , "Art.Error:EAN:%s;BP:%s;SA:%s" , belpos.lin_feld,belpos.pia_bp_feld,belpos.pia_sa_feld ) ;
			dposbug ++ ;
			daufpstat = -1 ;	// fehlerhafte Artikel
		}
	}	// 011013 ende
	if ( dauffolgestat == -3 )
		sprintf ( belpos.bemerkung , "Folge-Pos.Err.:EAN:%s;BP:%s;SA:%s" , belpos.lin_feld,belpos.pia_bp_feld,belpos.pia_sa_feld ) ;

}


static char qty_typ [8] ;

void QTY_arbeiten ( void ) 
{

	// 12  -Liefermenge
	// 46 - gezaehlte Menge
	// 180 - vereinnahmte Menge
	if ( !inblock ) return ;
	if ( daufpstat == 0 )
		artikellesen () ;

	if ( decount == 2 && gdcount == 1 )
	{
		sprintf ( qty_typ ,"%s", gdpuffer ) ;
	}
    if ( decount == 2 && gdcount == 2)
	{												
	    if ( ! strncmp ( qty_typ ,"12",2 ))		// 12 : Liefermenge 
    	{
			belpos.qty_12_wert = atof ( gdpuffer ) ;
			if ( daufpstat > 0 ) daufpstat = 2 ;
		}
	    if ( ! strncmp ( qty_typ ,"46",2 ))		// 46 : gezaehlte Menge 
    	{
			belpos.qty_46_wert = atof ( gdpuffer ) ;
			if ( daufpstat > 0 ) daufpstat = 2 ;
		}
	    if ( ! strncmp ( qty_typ ,"180",3 ))		// 180 : vereinahmte Menge 
    	{
			belpos.qty_180_wert = atof ( gdpuffer ) ;
			if ( daufpstat > 0 ) daufpstat = 2 ;
		}

	}
    if ( decount == 2 && gdcount == 3 )
	{
        if ( ! strncmp ( qty_typ ,"12",2 ))
		{
			sprintf (  belpos.qty_12_einh , "%s" , gdpuffer ) ;
		}
        if ( ! strncmp ( qty_typ ,"46",2 ))
		{
			sprintf (  belpos.qty_46_einh , "%s" , gdpuffer ) ;
		}
        if ( ! strncmp ( qty_typ ,"180",3 ))
		{
			sprintf (  belpos.qty_180_einh , "%s" , gdpuffer ) ;
		}
	}
}

static char pri_typ1 [7] ;
static char pri_typ2 [7] ;
static char pri_wert [20] ;
static char pri_fakt [7] ;

void PRI_arbeiten (void )
{

	if ( ! daufstat ) return ;
    if (decount == 2 && gdcount == 1 )
	{	// AAE  : auszeichnungspreis einzig gefragte Zahl -Initialisierung
        sprintf ( pri_typ1 ,"%s" ,gdpuffer ) ;
		sprintf ( pri_typ2 ,"" ) ;
		sprintf ( pri_fakt , "" ) ;
	}
    if ( decount == 2 && gdcount == 2 )
	{
		if ( ! strncmp (pri_typ1 , "AAE" ,3 )) 
		{
			sprintf ( pri_wert, "%s", gdpuffer ) ;
//			belpos.pri_aae_wert = atof ( gdpuffer ) ;
		}
 	}

	if ( decount == 2  && gdcount == 4 )
	{
        sprintf ( pri_typ2 ,"%s" , gdpuffer ) ;	// typ 2 nix oder RTP d.h. Auszeichnungspreis
       if ( ! strncmp ( pri_typ1 ,"AAE" ,3 ) && ! strncmp ( pri_typ2 , "RTP" , 3 ))
	   {
			belpos.pri_aae_wert = atof ( pri_wert ) ;

	   }
	}

	if ( decount == 2  && gdcount == 5 )
	{
        sprintf ( pri_fakt ,"%s" , gdpuffer ) ;	// der Faktor sollte hier immer == "1" sein ? !

	}
	if ( decount == 2  && gdcount == 6 )
	{
        if ( ! strncmp ( pri_typ1 ,"AAE" ,3 ) && ! strncmp ( pri_typ2 , "RTP" , 3 ))
		{
//			sprintf ( belpos.pri_aae_einh, "%s" ,gdpuffer ) ;
		}
	}
}

static char rff_typ [10] ;

void RFF_arbeiten ( void )
{
// hier steht eigne LS-Nummer !!!!	
	char rff_puffer[25] ;
	if ( ! inblock ) return ;
    if (decount == 2 && gdcount == 1 )
	{
        sprintf ( rff_typ ,"%s" ,gdpuffer ) ;
	}

	if ( decount == 2 && gdcount == 2 )
	{
        sprintf ( rff_puffer ,"%s" ,  gdpuffer ) ;
//		if ( inbeleg == 2 )
		if ( daufstat < 2 )	//Nur im Kopf auswerten ?!
		{
// Achtung hier sind Weiterungen bei Bearbeitung von Gutschriften denkbar 
			if  ( ! strncmp ( rff_typ ,"DQ", 2 ) && rff_typ[2] == '\0' )
			{
				sprintf ( belkopf.rff_dq_feld, "%s" , rff_puffer ) ;
				memcpy ( &lsk,&lsk_null, sizeof(struct LSK));
				memcpy ( &kun,&kun_null, sizeof(struct KUN));
				lsk.mdn = mdn.mdn ;
				lsk.ls = atol ( rff_puffer );
				lsk.fil = 0;
				int di = lsk_class.openlsk() ;
				di = lsk_class.leselsk();
				if ( ! di )
				{
					kun.mdn = mdn.mdn;
					kun.kun = lsk.kun;
					kun.fil = 0;
					di = kun_class.lesedefkun();
					belkopf.auf_int = lsk.ls ;
					belkopf.int_kun = lsk.kun;
					belkopf.int_adr = lsk.adr;
				}
				else
				{
					sprintf ( belkopf.bemerkung , "ung�ltige Lieferscheinreferenz : %s" , rff_puffer ) ;
				}

			}

//			if  ( ! strncmp ( rff_typ ,"ABO", 2 ) && rff_typ[2] == '\0' )
//			{
//				sprintf ( belkopf.rff_abo_feld, "%s" , rff_puffer ) ;
//			}
		}
	}
}

static char tax_typ1[10] ;
static char tax_typ2[10] ;
static char tax_wert[10] ;
void TAX_arbeiten ( void )
{
	return ;	// z.Z. dummy 
/* ---->
	if ( ! inblock ) return ;
    if (decount == 2 && gdcount == 1 )
	{
        sprintf ( tax_typ1 ,"%s" ,gdpuffer ) ;	// immer die "7"
		sprintf ( tax_wert ,"" ) ;				// erst mal init
	}
    if ( decount == 3 && gdcount == 1 )
	{
        sprintf ( tax_typ2 ,"%s" ,  gdpuffer ) ;	// immer "VAT"
	}
// im TEXTBLOCK
	if ( decount == 6 && gdcount == 4 )
	{
        sprintf ( tax_wert ,"%s" ,  gdpuffer ) ;	// hier stehen Prozente
		if ( inbeleg == 2 )
		{
			sprintf ( belkopf.tax_feld , "%s", tax_wert ) ;
		}
		if ( inbeleg == 3 && inposten )
		{
			sprintf ( belpos.tax_feld , "%s", tax_wert ) ;
		}
// im TEXTBLOCK
		if ( inbeleg == 4 )	// Fuss-Umschalter
		{
			sprintf ( belfusstaxaktiv , "%s" , tax_wert ) ;
			if ( ! belfuss.tax_1_aktiv )
			{	// erster Eintrag 
				belfuss.tax_1_aktiv = 1 ;
				sprintf ( belfuss.tax_f_1 , "%s" , tax_wert ) ;
				belfusstaxaktpo = 1 ;
			}
			else
			{	// aktiv aber doppelt ?
				if ( ! strcmp ( belfusstaxaktiv , belfuss.tax_f_1 ))
				{
					belfusstaxaktpo = 1 ;
				}
				else	// Gnadenlos auf 2. Eintrag, das kann natuerlich schiefgehen 
				{
					belfuss.tax_2_aktiv = 1 ;
					sprintf ( belfuss.tax_f_2 , "%s" , tax_wert ) ;
					belfusstaxaktpo = 2 ;
				}
			}
		}	// inbeleg 2,3,4
	}		// 6/5
// im TEXTBLOCK
	if ( decount == 7 && gdcount == 1 )
	{
        sprintf ( tax_wert ,"%s" ,  gdpuffer ) ;	// hier steht das "E" fuer empty 
		if ( inbeleg == 2 )
		{
			sprintf ( belkopf.tax_feld , "%s", tax_wert ) ;
		}
		if ( inbeleg == 3 && inposten )
		{
			sprintf ( belpos.tax_feld , "%s", tax_wert ) ;
		}
// im TEXTBLOCK
		if ( inbeleg == 4 )	// Fuss-Umschalter
		{
			sprintf ( belfusstaxaktiv , "%s" , tax_wert ) ;
			if ( ! belfuss.tax_1_aktiv )
			{	// erster Eintrag 
				belfuss.tax_1_aktiv = 1 ;
				sprintf ( belfuss.tax_f_1 , "%s" , tax_wert ) ;
				belfusstaxaktpo = 1 ;
			}
			else
// im TEXTBLOCK
			{	// aktiv aber doppelt ?
				if ( ! strcmp ( belfusstaxaktiv , belfuss.tax_f_1 ))
				{
					belfusstaxaktpo = 1 ;
				}
				else	// Gnadenlos auf 2. Eintrag, das kann natuerlich schiefgehen 
				{
					belfuss.tax_2_aktiv = 1 ;
					sprintf ( belfuss.tax_f_2 , "%s" , tax_wert ) ;
					belfusstaxaktpo = 2 ;
				}
			}

		}	// inbeleg 2,3,4
	}	// 7/1
< ---- */
}

void schreiben (void )
{
	if ( dsegguelt == 3 )
	{

/* ----> nur noch UNH_laden wird ausgefuehrt 
		if ( ! strncmp ( dsegname3 , "UNH" , 3 ))
		{
                UNH_arbeiten () ;    // Nachrichten-Rahmen-Anfang
                return ;
		}

		if ( ! strncmp ( dsegname3 , "UNT" , 3 ))
		{
                UNT_arbeiten () ;    
                return ;
		}
< ------ */

		if ( ! strncmp ( dsegname3 , "UNS" , 3 ))
		{
                UNS_arbeiten () ;    // nur noch dummy
                return ;
		}

		if ( ! strncmp ( dsegname3 , "BGM" , 3 ))
		{
                BGM_arbeiten () ;    // Beleg-Nummer
                return ;
		}
// 310311 : LOC neu dazu : EDEKA erstmalig genutzt 
		if ( ! strncmp ( dsegname3 , "LOC" , 3 ))
		{
                LOC_arbeiten () ;    // zusaetzliche LOC-INFO ( z.B. EDEKA )
                return ;
		}

		if ( ! strncmp ( dsegname3 , "DTM" , 3 ))
		{
                 DTM_arbeiten () ;  // Auftrags- u. Lieferdatum
                 return ;
		}
		if ( ! strncmp ( dsegname3 , "IMD" , 3 ))
		{
                IMD_arbeiten () ;  
                return ;
		}
		if ( ! strncmp ( dsegname3 , "LIN" , 3 ))
		{
                LIN_arbeiten ();    // Haupt-Schluessel
                return ;
		}
		if ( ! strncmp ( dsegname3 , "MEA" , 3 ))
		{
                MEA_arbeiten () ;	// z. Z. dummy 
                return ;
		}
		if ( ! strncmp ( dsegname3 , "MOA"	, 3 ))
		{
			MOA_arbeiten () ;	// z.Z. dummy
            return ;
		}
		if ( ! strncmp ( dsegname3 , "NAD" , 3 ))
		{
                NAD_arbeiten () ;						// Anschriften
                return ;
		}
		if ( ! strncmp ( dsegname3 , "PIA" , 3 ))
		{					// interne-Art-Nr (Lief/kun) -> Schluessel
                PIA_arbeiten () ;
                return ;
		}
		if ( ! strncmp ( dsegname3 , "RFF" , 3 ))
		{
			RFF_arbeiten () ;						// z.Z. dummy
            return ;
		}
		if ( ! strncmp ( dsegname3 , "TAX" , 3 ))
		{
			TAX_arbeiten () ;		// z.Z. dummy  
            return ;
		}
		if ( ! strncmp ( dsegname3 , "QTY" , 3 ))
		{
			QTY_arbeiten () ;		// Menge
            return ;
		}
		if ( ! strncmp ( dsegname3 , "CUX" , 3 ))
		{					// Referenz-Waehrung
			CUX_arbeiten () ;
            return ;
		}

		if ( ! strncmp ( dsegname3 , "FTX" , 3 ))
		{
			FTX_arbeiten () ;		// Kopf-Text ?! 
            return ;
		}
		if ( ! strncmp ( dsegname3 , "PRI" , 3 ))
		{                    // Preis
			PRI_arbeiten () ;
            return ;
		}

		if ( ! strncmp ( dsegname3 , "UNB" , 3 ))
		{  
            UNB_arbeiten () ;	// Adressier-Test
			return ;
		}

//           case "UNH"
//           case "UNA"
//           case "UNT"

                return ;

	}     // dsegguelt3
    if ( dsegguelt == 2 )
	{
		return ;

/* -----> gibbet gar nicht ...
          switch dsegname2
            case "XX"
            case "YY"
            otherwise
                return

        end     // switch dsegname2
< ----- */
	}         // dsegguelt = 2
}

// ######################################################################
// #### PROC.:  lesegd
// #### liest naechstes Datenelement  bis zum naechsten Gruppendaten-
// oder Datenelementtrenner
// #### in den gdpuffer
// ######################################################################

int lesegd(void) 
{
char hilfchar [4] ;
	sprintf ( hilfchar , "X" ) ;
    gdwpoint = 0 ;
    memset ( gdpuffer, '\0', sizeof ( gdpuffer )) ;
    if ( derpoint < depufflen )
	{
		hilfchar[0]  = depuffer[derpoint] ;

        while (hilfchar[0] != trenn_gd[0] ||maskel )
		{
			if ( maskel )
			{ 
				maskel = 0 ;
			}
			else
			{
				if ( hilfchar[0] == trenn_ma[0]) maskel = 1 ;
			}

            gdpuffer[gdwpoint] = hilfchar[0] ;
            gdwpoint ++ ;
            derpoint ++ ;
            if ( derpoint >= depufflen )
			{
				maskel = 0 ;
                gdcount ++ ;
                derpoint ++ ;
                schreiben() ;
                return 0 ;
			}
            hilfchar[0]  = depuffer[derpoint] ;
		}
		if ( hilfchar[0] == trenn_gd[0] )
		{
			// maskel gibbet hier niemals 
			maskel = 0 ;
            gdcount ++ ;
            derpoint ++ ;
            schreiben () ;
            return 0 ;
		}
	}                 // pufferende
    return  -1 ;
}

void mlesegd ( void ) 
{
    memset ( gdpuffer, '\0', sizeof ( gdpuffer )) ;
    gdcount   = 0 ;
    depufflen = (int) strlen ( depuffer ) ;
    derpoint = 0 ;
	while ( ! lesegd ( ) ) ;
}


// ######################################################################
// #### PROC.:  lesede
// #### liest naechstes Datenelement  bis zum naechsten Datenelementtrenner
// #### in den depuffer
// ######################################################################

int lesede ( void ) 
{
char hilfchar [3] ;

    sprintf (hilfchar ,"X" ) ;
    dewpoint = 0 ;
    derpoint = 0 ;
    memset ( depuffer, '\0', sizeof ( depuffer )) ;
    if ( dsegrpoint < dsegpufflen )
	{
		hilfchar[0]  = dsegpuffer[dsegrpoint] ;
		while ((hilfchar[0] != trenn_de[0] ) ||maskel )
		{
			if ( maskel )
			{ 
				maskel = 0 ;
			}
			else
			{
				if ( hilfchar[0] == trenn_ma[0]) maskel = 1 ;
			}

            depuffer[dewpoint] = hilfchar[0] ;
            dewpoint ++ ;
            dsegrpoint ++ ;
            if ( dsegrpoint >= dsegpufflen )
			{
				maskel = 0 ;
				decount ++ ;
                mlesegd () ; 
                return  0 ;
			}
            hilfchar[0]  = dsegpuffer[dsegrpoint] ;
		}
        if ( hilfchar[0] == trenn_de[0] )
		{
			// maskel gibbet hier niemals 
			maskel = 0 ;
            decount ++ ;
            dsegrpoint ++ ;
            mlesegd () ;
            return 0 ;
		}
	}                 // pufferende
    return ( -1 ) ;
}

void mlesede ( void )
{
	depufflen  = 0 ;
    memset ( depuffer, '\0' , sizeof ( depuffer )) ;
    memset ( gdpuffer, '\0' , sizeof ( gdpuffer )) ; 
    gdcount    = 0 ;
    decount    = 0 ;
    dsegrpoint = dsegguelt	; // + 1	Rosi ging erst bei 1 los .... 
//    k = 0 ;
    dsegpufflen = (int) strlen ( dsegpuffer ) ;
    while ( ! lesede () );
}


int auswertesegment ( void ) 
{
    memset ( dsegname3, '\0' , sizeof(dsegname3 )) ;
    memset ( dsegname2, '\0', sizeof (dsegname2 )) ;
    dsegguelt = 0 ;
    gdcount  = 0 ;
    decount  = 0 ;

    dsegname3[0] = dsegpuffer[0] ;
    dsegname2[0] = dsegpuffer[0] ;
    dsegname3[1] = dsegpuffer[1] ;
    dsegname2[1] = dsegpuffer[1] ;
    dsegname3[2] = dsegpuffer[2] ;
    if ( ! strncmp ( dsegname3 ,"UNA",3 ))
	{
        UNA_laden() ;
        memset ( dsegpuffer,'\0', sizeof(dsegpuffer)) ;
        dsegwpoint = 0 ;
        return dabbruch ;
	}

    if ( ! strncmp ( dsegname3 ,"UNH",3 ))	// kommt nicht mehr in den Modus "schreiben"  
	{
        UNH_laden() ;
        memset ( dsegpuffer,'\0', sizeof(dsegpuffer)) ;
        dsegwpoint = 0 ;
        return dabbruch ;
	}
    if ( ! strncmp ( dsegname3 ,"UNT",3 ))	// kommt nicht mehr in den Modus "schreiben"  
	{
        UNT_laden() ;
        memset ( dsegpuffer,'\0', sizeof(dsegpuffer)) ;
        dsegwpoint = 0 ;
        return dabbruch ;
	}

    if ( ! strncmp ( dsegname3 ,"UNZ",3 ))
	{
		UNZ_laden () ;
        memset ( dsegpuffer,'\0', sizeof(dsegpuffer)) ;
        dsegwpoint = 0 ;
        return dabbruch ;
	}

     if (( ! strncmp ( dsegname3, "UNB" , 3 ))
 //      ||( ! strncmp ( dsegname3, "UNH" , 3 ))
 //      ||( ! strncmp ( dsegname3, "UNT" , 3 ))
       ||( ! strncmp ( dsegname3, "BGM" , 3 ))
       ||( ! strncmp ( dsegname3, "MOA" , 3 ))
       ||( ! strncmp ( dsegname3, "PIA" , 3 ))
       ||( ! strncmp ( dsegname3, "NAD" , 3 ))
       ||( ! strncmp ( dsegname3, "LIN" , 3 ))
       ||( ! strncmp ( dsegname3, "RFF" , 3 ))
       ||( ! strncmp ( dsegname3, "DTM" , 3 ))
       ||( ! strncmp ( dsegname3, "TAX" , 3 ))
       ||( ! strncmp ( dsegname3, "MEA" , 3 ))
       ||( ! strncmp ( dsegname3, "IMD" , 3 ))
       ||( ! strncmp ( dsegname3, "CNT" , 3 ))
       ||( ! strncmp ( dsegname3, "UNS" , 3 ))
       ||( ! strncmp ( dsegname3, "QTY" , 3 ))
       ||( ! strncmp ( dsegname3, "CUX" , 3 ))
       ||( ! strncmp ( dsegname3, "PRI" , 3 ))
       ||( ! strncmp ( dsegname3, "ALI" , 3 ))
       ||( ! strncmp ( dsegname3, "ALC" , 3 ))
       ||( ! strncmp ( dsegname3, "FTX" , 3 ))
       ||( ! strncmp ( dsegname3, "CTA" , 3 ))
	   ||( ! strncmp ( dsegname3, "COM" , 3 ))
	   ||( ! strncmp ( dsegname3, "PCD" , 3 ))
	   ||( ! strncmp ( dsegname3, "LOC" , 3 ))	
	   ||( ! strncmp ( dsegname3, "CPS" , 3 ))	// 041213	
	   ||( ! strncmp ( dsegname3, "PAC" , 3 ))	// 041213



	  )
	 {
		dsegguelt = 3 ;
        mlesede () ;
        memset ( dsegpuffer , '\0' , sizeof ( dsegpuffer )) ;
        dsegwpoint = 0 ;
        return dabbruch ;
	 }
	 else
	 {
		MessageBox(NULL, "Unbekannter Identifier", dsegname3 , MB_OK|MB_ICONSTOP);
		if ( ignoresegment )
		{
			memset ( dsegpuffer,'\0', sizeof(dsegpuffer)) ;
			dsegwpoint = 0 ;
			return dabbruch ;
		}
		else	// Normal-Zustand
		{
			sprintf ( belkopf.bemerkung, "Unbekannter Identifier : %s " , dsegname3 ) ;	// nur einmal im Kopf hinschreiben
			write_belkopf_a () ;
			dabbruch = 1 ;
			return -1 ;
		}
  	 }

	memset ( dsegpuffer , '\0' , sizeof ( dsegpuffer )) ;
    dsegwpoint = 0 ;
    return dabbruch ;
}

// Ab hier mal wieder die alten string-Handel-funktionen .....

void cr_weg (char *string)

{
 for (; *string; string += 1)
 {
  if (*string == (char) 13)
    break;
  if (*string == (char) 10)
    break;
  if (*string == '\0')
    return ;
 }
 *string = 0;
 return;
}

int next_char_ci (char *string, char tzeichen, int i)
// Naechstes Zeichen != Trennzeichen suchen.

{
       for (;string [i]; i ++)
       {
               if (string[i] != tzeichen)
               {
                                   return (i);
               }
       }
       return (i);
}

char *wort[25] ; // Bereich fuer Worte eines Strings
int strupcmp (char *str1, char *str2, int len)
{
 short i;
 unsigned char upstr1;
 unsigned char upstr2;


 for (i = 0; i < len; i ++, str1 ++, str2 ++)
 {
  if (*str1 == 0)
    return (-1);
  if (*str2 == 0)
    return (1);

  upstr1 = (unsigned char) toupper((int) *str1);
  switch (upstr1)
  {
      case (unsigned char) '�' :
            upstr1 = '�';
			break;
      case (unsigned char) '�' :
            upstr1 = '�';
			break;
      case (unsigned char) '�' :
            upstr1 = '�';
			break;
  }


  upstr2 = (unsigned char) toupper((int) *str2);

  switch (upstr2)
  {
      case (unsigned char) '�' :
            upstr2 = '�';
			break;
      case (unsigned char) '�' :
            upstr2 = '�';
			break;
      case (unsigned char) '�' :
            upstr2 = '�';
			break;
  }

  if (upstr1 < upstr2)
  {
   return(-1);
  }
  else if (upstr1 > upstr2)
  {
   return (1);
  }
 }
 return (0);
}


short split (char *string)
{
 short wz;                  // Wortzaehler  
 int i, j, len;
 static char zeichen = ' ';
//  if (buffer == (char *) 0) buffer = (char *) malloc (0x1000);
 wz = 0;
 j = 0;
 len = (int)strlen (string);
 wz = 1;
 i = next_char_ci (string, zeichen, 0);
 if (i >= len) return (0);
 wort [wz] = buffer;
 wz ++;
 for (; i < len; i ++, j ++)
 {
        if (string [i] == zeichen)
        {
                    i = next_char_ci (string, zeichen, i);
                    if (i >= len) break;
                    buffer [j] = (char) 0;
                    j ++;
                    wort [wz] = &buffer [j];
                    wz ++;
        }
        buffer [j] = string [i];
  }
  buffer [j] = (char) 0;
  return (wz - 1);
}

static char DefWert[256] ;

// Wert aus Bws-default holen.
char *bws_default (char *name)
{         
        char *etc;
        int anz;
        char buffer [512];
        FILE *fp;

        etc = getenv ("BWSETC");
        if (etc == (char *) 0)
        {
                    etc = "C:\\USER\\FIT\\ETC";
        }

        sprintf (buffer, "%s\\bws_defa", etc);
		fp = fopen (buffer, "r");
        if (fp == NULL) return NULL;

// clipped muss in der Quelle realisiert sein .....
//        clipped (name);

        while (fgets (buffer, 511, fp))
        {
                     cr_weg (buffer);
                     anz = split (buffer);
                     if (anz < 2) continue;
                     if (strupcmp (wort[1], name, (int) strlen (name)) == 0)
                     {
                                 strcpy (DefWert, wort [2]);
                                 fclose (fp);
                                 return (char *) DefWert;
                     }
         }
         fclose (fp);
         return NULL;
}

char * fitgroupdef (char *name)
/**
Wert aus fitgroup.def holen.
**/
{         
        char *etc;
        int anz;
        char buffer [512];
        FILE *fp;

        etc = getenv ("BWSETC");
        if (etc == (char *) 0)
        {
                    etc = "C:\\USER\\FIT\\ETC";
        }

        sprintf (buffer, "%s\\fitgroup.def", etc);
		fp = fopen (buffer, "r");
        if (fp == NULL) return NULL;

// clipped muss in der Quelle realisiert sein .....
//        clipped (name);

        while (fgets (buffer, 511, fp))
        {
                     cr_weg (buffer);
                     anz = split (buffer);
                     if (anz < 2) continue;
                     if (strupcmp (wort[1], name, (int) strlen (name)) == 0)
                     {
                                 strcpy (DefWert, wort [2]);
                                 fclose (fp);
                                 return (char *) DefWert;
                     }
         }
         fclose (fp);
         return NULL;
}

int einlesen ( CrecadvDlg * Cparent, short dmdn , char * quelldatei , int testignnore )
{

// zuerst mal syspars usw. holen

	char * p ;
	p = fitgroupdef ( "lager" ) ;
	if ( p == NULL ) 
		dlgr_gr = -1 ;
	else
		dlgr_gr = atoi ( p  ) ;


	char parname[31] ;
	char parwert[11] ;
	char parbesch[41] ;

	sprintf ( parname, "li_a_edi" ) ;
	if ( ! sys_par_class.sys_par_holen ( parname, parwert, parbesch ))
		dli_a_edi = 0 ;
	else
		dli_a_edi = atoi ( parwert ) ;


	sprintf ( parname, "lgr_bestand" ) ;
	if ( ! sys_par_class.sys_par_holen ( parname, parwert, parbesch ))
		dlgr_bestand = 0 ;
	else
		dlgr_bestand = atoi ( parwert ) ;



	inblock =  0 ;
	dauffolgestat = daufstat = daufpstat = 0 ;

    trennz_laden () ;

	sprintf ( eigen_iln , "%s", mdn.iln ) ;

    iprpoint   = 0 ;
    dsegwpoint = 0 ;
    dsegrpoint = 0 ;
    dewpoint   = 0 ;
    derpoint   = 0 ;
    gdwpoint   = 0 ;

    memset ( ippuffer,'\0', sizeof(ippuffer))  ;
    ippufflen  = 0 ;

    memset ( dsegpuffer,'\0', sizeof(dsegpuffer)) ;
    dsegpufflen = 0 ; 

    memset ( depuffer,'\0', sizeof(depuffer)) ;
    depufflen  = 0 ;

    memset( gdpuffer,'\0', sizeof(gdpuffer)) ;

    gdcount   = 0 ;
    decount   = 0 ;

// 261110 : macht nur Aerger : aufk_class.aktual_ipr () ;	// IPR fuer neue Preisfindung scharf machen

    while ( ! lesesegment (Cparent) )
	{
		k = auswertesegment () ;
        if ( k ) return  k ;
	}
	return  0 ; 
}

void ArraysInit () 
{
	// Anfansginit der Arrays
	belkopfakt = belkopfmax = 0 ;
	belkopfdim = 10 ;
	belposakt = belposmax = 0 ;
	belposdim = 50 ;
//	belfussakt = belfussmax = 0 ;
//	belfussdim = 10 ;
	belkopf_a = NULL ;
	belkopf_a = (struct BELKOPF *)calloc(belkopfdim, sizeof(struct BELKOPF ));
	memcpy ( &belkopf,&belkopf_null, sizeof(struct BELKOPF));
	belpos_a = NULL ;
	belpos_a = (struct BELPOS *)calloc(belposdim, sizeof(struct BELPOS ));
	memcpy ( &belpos,&belpos_null, sizeof(struct BELPOS));

//	belfuss_a = NULL ;
//	belfuss_a = (struct BELFUSS *)calloc(belfussdim, sizeof(struct BELFUSS ));
}

void write_belkopf_a(void)
{
	// memcpy ( &belkopf,&belkopf_null, sizeof(struct BELKOPF));
	if ( belkopfmax >= belkopfdim )	// neu allozieren
	{
	   belkopfdim += 10 ;
	   belkopf_a = ( struct BELKOPF *)realloc 
					( (void *) belkopf_a,
					      belkopfdim * sizeof(struct BELKOPF));
		if ( belkopf_a == NULL )
		{
// knallt dann sowieso :					int i = 0 ;	// fatal-Error 
		}

	} ;
	memcpy ( belkopf_a + belkopfmax , &belkopf, sizeof ( struct BELKOPF )) ;
	belkopfmax ++ ;
	daufstat = 0  ;
	belkopf.bemerkung[0] = '\0' ;
}

int read_belkopf_a(int modus)
{

	if ( modus == 0 )
	{
		belkopf.int_kun = 0; 
		while ( belkopfakt < belkopfmax )
		{
			memcpy ( &belkopf , belkopf_a + belkopfakt , sizeof ( struct BELKOPF )) ;
			belkopfakt ++ ;
			if ( belkopf.int_kun == 0 )
				continue;

			return 0 ;
		}
	}
	else
	{	// alles lesen
		if ( belkopfakt < belkopfmax )
		{
			memcpy ( &belkopf , belkopf_a + belkopfakt , sizeof ( struct BELKOPF )) ;
			belkopfakt ++ ;
			return 0 ;
		}
	}
	return 100 ;
}

char * hole_a_ptbez ( char * ptitem , short valnum )
{

	sprintf ( ptabn.ptitem ,"%s" , ptitem ) ;
	sprintf ( ptabn.ptwert ,"%d" , valnum ) ;
	int retco = ptabn_class.openptabn () ;
	retco = ptabn_class.leseptabn () ;
	if ( retco ) sprintf ( ptabn.ptbezk , "" ) ;
	ptabn.ptbezk[5] = '\0' ;
	return   ptabn.ptbezk ;

}

short hole_me_einh_kun ( void ) 
{

// die meisten Klimmzuege entstehen aus dem Kampf gegen schlecht gefuellte Stammdaten
// ohnehin nur dummy-Kram
	int qty_einh = 0 ; 
	int qty_hinweis ;
//	if ( ! strncmp( belpos.qty_21_einh , "GRM" , 3 ) ) qty_einh = 3 ;	// Faktor 1000
//	if ( ! strncmp( belpos.qty_21_einh , "KGM" , 3 ) ) qty_einh = 2 ;	// Faktor 1
//	if ( ! strncmp( belpos.qty_21_einh , "PCE" , 3 ) ) qty_einh = 1 ;	// a_kun auswerten
//	if ( ! strncmp ( belpos.lin_feld, "28" , 2 ) ) qty_hinweis = 1 ;	// behandeln wie "GRM" , jedoch nur bei O_FEGRO 

	int gefunden = 0 ;
	short retco = a_bas.me_einh ;
	belpos.inh = 1.0 ;

	tausendfaktor = 1.0 ;

	if ( qty_einh == 2 )	// KGM
	{
		retco = 2 ;	// "kg"
		gefunden = 1 ;
	}

	if ( qty_einh == 3 )	// GRM
	{
		retco = 2 ;	// "kg"
		tausendfaktor = 0.001 ;
		gefunden = 1 ;
	}
	if ((! gefunden) && (doption == O_FEGRO) && (qty_hinweis == 1) && qty_einh == 0 ) 
	{
		retco = 2 ;	// "kg"
		tausendfaktor = 0.001 ;
		gefunden = 1 ;
	}
	if ( ! gefunden )
	{
		if ( a_kun.a > 0.01 && a_kun.a == a_bas.a )
		{
			if ( a_kun.me_einh_kun > 0 )
			{
				retco = a_kun.me_einh_kun ;
				gefunden = 1 ;	// 250610 : erweiterte Suche wegen kumebest und boesi
				// Boesi-Situation : a_kun.me_einh_kun auf "0" setzen ..........
			}
			if ( a_kun.inh > 0.001 )
				belpos.inh = a_kun.inh ;

// 250610 ab hier neu wegen boesi und kumebest
			if ( !gefunden )
			{
				if ( a_bas.a_typ == 1 || a_bas.a_typ2 == 1)
				{
					a_hndw.a = a_bas.a ;
					int retco2 = a_hndw_class.opena_hndw () ;
					if ( ! retco2 ) a_hndw_class.lesea_hndw () ;
					if ( ! retco2 )
					{
						if ( a_hndw.me_einh_kun > 0 )
						{
							retco = a_hndw.me_einh_kun ;
							gefunden = 1 ;
						}
					}
				}
			}
			if ( !gefunden )
			{
				if ( a_bas.a_typ == 2 || a_bas.a_typ2 == 2)
				{
					a_eig.a = a_bas.a ;
					int retco2 = a_eig_class.opena_eig () ;
					if ( ! retco2 ) a_eig_class.lesea_eig () ;
					if ( ! retco2 )
					{
						if ( a_eig.me_einh_ek > 0 )
						{
							retco = a_eig.me_einh_ek ;
							gefunden = 1 ;
						}
					}
				}
			}
			if ( !gefunden )
			{
				if ( a_bas.a_typ == 3 || a_bas.a_typ2 == 3)
				{
					a_eig_div.a = a_bas.a ;
					int retco2 = a_eig_div_class.opena_eig_div () ;
					if ( ! retco2 ) a_eig_div_class.lesea_eig_div () ;
					if ( ! retco2 )
					{
						if ( a_eig_div.me_einh_ek > 0 )
						{
							retco = a_eig_div.me_einh_ek ;
							gefunden = 1 ;
						}
					}
				}
			}

// 250610 bis hier neu wegen boesi und kumebest
			gefunden = 1 ;
		}
		else
		{
			// kein a_kun-Eintrag vorhanden
			if ( a_bas.a_typ == 1 || a_bas.a_typ2 == 1)
			{
				a_hndw.a = a_bas.a ;
				int retco2 = a_hndw_class.opena_hndw () ;
				if ( ! retco2 ) a_hndw_class.lesea_hndw () ;
				if ( ! retco2 )
				{
					if ( a_hndw.me_einh_kun > 0 )
						retco = a_hndw.me_einh_kun ;
					if ( a_hndw.inh > 0.001 )
						belpos.inh = a_hndw.inh ;
					gefunden = 1 ;
				}
			}
			if ( !gefunden )
			{
				if ( a_bas.a_typ == 2 || a_bas.a_typ2 == 2)
				{
					a_eig.a = a_bas.a ;
					int retco2 = a_eig_class.opena_eig () ;
					if ( ! retco2 ) a_eig_class.lesea_eig () ;
					if ( ! retco2 )
					{
						if ( a_eig.me_einh_ek > 0 )
							retco = a_eig.me_einh_ek ;
						if ( a_eig.inh > 0.001 )
							belpos.inh = a_eig.inh ;
						gefunden = 1 ;
					}
				}
			}
			if ( !gefunden )
			{
				if ( a_bas.a_typ == 3 || a_bas.a_typ2 == 3)
				{
					a_eig_div.a = a_bas.a ;
					int retco2 = a_eig_div_class.opena_eig_div () ;
					if ( ! retco2 ) a_eig_div_class.lesea_eig_div () ;
					if ( ! retco2 )
					{
						if ( a_eig_div.me_einh_ek > 0 )
							retco = a_eig_div.me_einh_ek ;
						if ( a_eig_div.inh > 0.001 )
							belpos.inh = a_eig_div.inh ;
						gefunden = 1 ;
					}
				}
			}
		}
	}

	if ( belpos.inh < 0.001 )
		belpos.inh = 1.0 ;
	if ( retco < 1 ) retco = a_bas.me_einh ;
	return retco ;
}

// Kundenstamm und a_bas gibt es jedenfalls, a_kun nur eventuell ......
// vk und lad_preis sind schon erledigt 
// jetzt muss koplettiert werden
void write_belpos_a(void)
{

// memcpy ( &belpos,&belpos_null, sizeof(struct BELPOS));
	sprintf ( belpos.lief_me_bz ,"%s" , hole_a_ptbez ( "me_einh" , a_bas.me_einh )) ;
	belpos.me_einh = a_bas.me_einh ;

	belpos.me_einh_kun = hole_me_einh_kun () ;	// stellt auch den tausendfaktor korrekt 

	belpos.a_grund = a_bas.a_grund ;
	belpos.teil_smt = tsmtg_class.ateil_smt_hol ( teilsmtbeacht ) ;
	belpos.dr_folge = a_bas.dr_folge ;

	sprintf ( belpos.auf_me_bz ,"%s"  , hole_a_ptbez ( "me_einh" , belpos.me_einh_kun )) ;  
	belpos.auf_me = belpos.qty_46_wert;
//	 belpos.qty_46_wert  ;
//	 belpos.qty_180_wert  ;
//	 belpos.qty_12_wert ;

//	belpos.auf_me = belpos.auf_me * tausendfaktor ;

	if ( a_kun.a == a_bas.a && a_kun.a > 0 && a_kun.geb_fakt > 0.001 )
	{

		// positiver Gebindefaktor : Multiplikiton mit der Bestellmenge ergibt interne Bestellmenge 
		// Beispiel : 1 Display enthaelt 4 Schalen  
		belpos.auf_me = belpos.auf_me *  a_kun.geb_fakt ;
	}
	if ( a_kun.a == a_bas.a && a_kun.a > 0 && a_kun.geb_fakt < -0.001 ) 
	{

		// negativer Gebindefaktor : Division mit der Bestellmenge ergibt interne Bestellmenge 
		// Beispiel :  4 bestellte Schalen werden mit 1 Display beliefert
		// hier sollte evtl .gerundet werden ? 
		belpos.auf_me = belpos.auf_me / ( 0 - a_kun.geb_fakt ) ;

	}

// hier gegbnenfalls noch rumrechnen .... 
	if ( belposmax >= belposdim )	// neu allozieren
	{
	   belposdim += 50 ;
	   belpos_a = ( struct BELPOS *)realloc 
					( (void *) belpos_a,
					      belposdim * sizeof(struct BELPOS));
		if ( belpos_a == NULL )
		{
// knallt dann sowieso :					int i = 0 ;	// fatal-Error 
		}

	} ;
	memcpy ( belpos_a + belposmax , &belpos, sizeof ( struct BELPOS )) ;
	belposmax ++ ;
	daufpstat = 0 ;
	memcpy ( &belpos,&belpos_null, sizeof(struct BELPOS));
}

int read_belpos_a(void)
{
	if ( belposakt < belposmax )
	{
		memcpy ( &belpos , belpos_a + belposakt , sizeof ( struct BELPOS )) ;
		belposakt ++ ;
		return 0 ;
	}
	return 100 ;
}

/* ---> gib bet nicht mehr ....
void write_belfuss_a(void)
{
// memcpy ( &belfuss,&belfuss_null, sizeof(struct BELFUSS));
	if ( belfussmax >= belfussdim )	// neu allozieren
	{
	   belfussdim += 10 ;
	   belfuss_a = ( struct BELFUSS *)realloc 
					( (void *) belfuss_a,
					      belfussdim * sizeof(struct BELFUSS));
		if ( belfuss_a == NULL )
		{
// im TEXTBLOCK
	// knallt dann sowieso :					int i = 0 ;	// fatal-Error 
		}
	} ;
	memcpy ( belfuss_a + belfussmax , &belfuss , sizeof ( struct BELFUSS )) ;
	belfussmax ++ ;
}
// im TEXTBLOCK
int read_belfuss_a(void)
{
	if ( belfussakt < belfussmax )
	{
		memcpy ( &belfuss , belfuss_a + belfussakt, sizeof ( struct BELFUSS )) ;
		belfussakt ++ ;
		return 0 ;
	}
	return 100 ;
}
< ---- */

void ArraysFree () 
{
	belkopfdim = belkopfakt = belkopfmax = 0 ;
	belposdim = belposakt = belposmax = 0 ;
//	belfussdim = belfussakt = belfussmax = 0 ;

	if ( belkopf_a ) { free ( belkopf_a ) ; belkopf_a = NULL ; } ;
	if ( belpos_a  ) { free ( belpos_a  ) ; belpos_a  = NULL ; } ;
//	if ( belfuss_a ) { free ( belfuss_a ) ; belfuss_a = NULL ; } ;
}

void wrilistekopfsatz (void)
{
	memcpy ( &tempdrx , &tempdrx_null, sizeof ( struct TEMPDRX )) ;
	sprintf ( tempdrx.form_nr, "recadvpro") ;	// recadv-Protokoll
	tempdrx.lila = 0 ;
	tempdrx.lfd = lfdliste;
	tempdrx.lfd10 = lfdlistennr ;
	lfdlistennr ++ ;

// integer-Felder
	tempdrx.lfd01 = 1 ;	// belegkopf
	tempdrx.lfd02 = dta_referenz ;
	tempdrx.lfd03 = belkopf.int_kun ;
	tempdrx.lfd04 = belkopf.int_adr ;
	tempdrx.lfd05 = dta.dta ;
	tempdrx.lfd06 = dkopfbug ;
	if ( immerweiter )
		tempdrx.lfd07 = 0  ;
	else
		tempdrx.lfd07 = dposbug  ;
	tempdrx.lfd08 = dabbruch ;
	tempdrx.lfd09 = belkopf.auf_int ;

	sprintf ( tempdrx.cfeld201 , "%s", belkopf.dtm_137_feld ) ;	// 041213 : 2->137
	sprintf ( tempdrx.cfeld204, "%s", belkopf.auf_ext ) ; 
	
	if ( (strlen ( belkopf.bemerkung )) > 33 )
	{
		sprintf ( tempdrx.cfeld302 , "%s" , belkopf.bemerkung + 33 ) ;
		belkopf.bemerkung [33] = '\0' ;
		sprintf ( tempdrx.cfeld301 , "%s" , belkopf.bemerkung ) ;
	}
	else
	{
		sprintf ( tempdrx.cfeld301 , "%s" , belkopf.bemerkung ) ;
		sprintf ( tempdrx.cfeld302 , "" ) ;
	}
	tempdrx_class.inserttempdrx() ;
}

void wrilistepossatz (void)
{
	memcpy ( &tempdrx , &tempdrx_null, sizeof ( struct TEMPDRX )) ;
	sprintf ( tempdrx.form_nr, "recadvpro") ;	// recadvpro-Liste
	tempdrx.lila = 0 ;
	tempdrx.lfd = lfdliste;
	tempdrx.lfd10 = lfdlistennr ;
	lfdlistennr ++ ;

	tempdrx.lfd01 = 2 ;	// belegposition
// zun�chst die Kopf-Infos duplizieren 
	tempdrx.lfd02 = dta_referenz ;
	tempdrx.lfd03 = belkopf.int_kun ;
	tempdrx.lfd04 = belkopf.int_adr ;
	tempdrx.lfd05 = dta.dta;
	tempdrx.lfd06 = dkopfbug ;
	if ( immerweiter )
		tempdrx.lfd07 = 0  ;
	else
		tempdrx.lfd07 = dposbug ;
	tempdrx.lfd08 = dabbruch ;
	tempdrx.lfd09 = belkopf.auf_int ;

	sprintf ( tempdrx.cfeld201 , "%s", belkopf.dtm_137_feld ) ;	// 041213 2-> 137
	sprintf ( tempdrx.cfeld204, "%s", belkopf.auf_ext ) ; 
	
// Anschliessend aktuelle Positions-Infos ergaenzen 

	tempdrx.dfeld01 = belpos.a ;
	tempdrx.dfeld02 = belpos.qty_12_wert ;
	tempdrx.dfeld03 = belpos.qty_180_wert ;
	tempdrx.dfeld04 = belpos.qty_46_wert ;

	if ( (strlen ( belpos.bemerkung )) > 33 )
	{
		sprintf ( tempdrx.cfeld302 , "%s" , belpos.bemerkung + 33 ) ;
		belpos.bemerkung [33] = '\0' ;
		sprintf ( tempdrx.cfeld301 , "%s" , belpos.bemerkung ) ;
	}
	else
	{
		sprintf ( tempdrx.cfeld301 , "%s" , belpos.bemerkung ) ;
		sprintf ( tempdrx.cfeld302 , "" ) ;
	}
	tempdrx_class.inserttempdrx() ;
}


DWORD ProcWaitExec (LPSTR prog, WORD SHOW_MODE, int x, int y, int cx, int cy)
/**
Fenster-Process starten und nicht auf Ende Warten.
**/
{
        STARTUPINFO sti;
        PROCESS_INFORMATION pi;
        DWORD ExitCode; 
        int ret;

        ZeroMemory (&sti, sizeof (sti));
        sti.cb = sizeof (sti);
        if (x != -1)
        {
                     sti.dwFlags |= STARTF_USEPOSITION;
                     sti.dwX = x;
                     sti.dwY = y;
        }

        if (cx != -1)
        {
                     sti.dwFlags |= STARTF_USESIZE;
                     sti.dwXSize = cx;
                     sti.dwYSize = cy;
        }
        sti.wShowWindow = SHOW_MODE;
        sti.dwFlags |= STARTF_USESHOWWINDOW;

        ret = CreateProcess (NULL,
                       prog,
                       NULL,
                       NULL,
                      (int) NULL,
                      (unsigned long) NULL,
                       NULL,
                       NULL,
                       &sti,
                       &pi);
        if (ret)
        {
                  CloseHandle (pi.hThread);
                  WaitForSingleObject (pi.hProcess, INFINITE);
        }
		else
        {
			      return -1;
        }				  
		GetExitCodeProcess (pi.hProcess, &ExitCode);
		while (ExitCode == STILL_ACTIVE)
		{
                  GetExitCodeProcess (pi.hProcess, &ExitCode);
		}
        CloseHandle (pi.hProcess);
        return ExitCode;
}


void VerbucheListe(void)
{
} ;

void ladeaufk(void )
{
// 	belkopf steht auf aktuellem Wert, belkopfakt zeigt auf naechsten Wert  

	memcpy ( &aufk,&aufk_null, sizeof(struct AUFK));
	aufk.mdn = mdn.mdn ;  
    aufk.fil = 0 ;
	aufk.ang = 0 ;

//	sprintf ( aufk.loc, "%s" , belkopf.loc_feld ) ;	// 310311 

	aufk.auf = num_array [belkopfakt - 1 ] ; 
// das bitte nicht mehr ;-)	belkopf_a[belkopfakt - 1].auf_int = aufk.auf ;	// Zurueckschreiben des Auftrages
	aufk.adr = belkopf.int_adr; 
	aufk.kun_fil = 0 ; 
	aufk.kun = belkopf.int_kun ;
	sqldatdst ( & aufk.lieferdat, belkopf.dtm_137_feld );
	sqldatdst ( & aufk.best_dat, belkopf.dtm_50_feld );
// Schiller-spezial Start 
	char hilfe10 [10] ;
	sprintf ( hilfe10 ,"%08.0d" , kun.tou ) ;
	sprintf ( aufk.lieferzeit , "%s" , hilfe10 + 5  ) ; // die letzten 3 Stellen
	hilfe10[5] = '\0' ;									// die ersten 5 stellen 
	sprintf ( aufk.hinweis , "%s", hilfe10 ) ;

// 291113 A : zun�chst Schiller �berpinseln, falls Bedarf besteht, dann bei Schiller Sonderablauf schalten

//		sprintf ( aufk.lieferzeit, "%s", belkopf.dtm_2_feld_t);
		aufk.lieferzeit[2]= '.';
		aufk.zeit_dec = atof(aufk.lieferzeit);

//		sprintf ( aufk.lieferzeit, "%s", belkopf.dtm_2_feld_t);

// 291113 E : zun�chst Schiller �berpinseln, falls Bedarf besteht, dann bei Schiller Sonderablauf schalten

	aufk.auf_stat = 2 ;
	sprintf ( aufk.kun_krz1 , "%s" , belkopf.kun_krz1 ) ;
	sprintf ( aufk.feld_bz1 , "" ) ;
	sprintf ( aufk.feld_bz2 , "" ) ;
	sprintf ( aufk.feld_bz3 , "" ) ;
	aufk.delstatus = 0 ;
	aufk.zeit_dec = 0  ;
	aufk.kopf_txt = 0 ;
	aufk.fuss_txt = 0 ;
	aufk.vertr = belkopf.vertr ;
	sprintf ( aufk.auf_ext , "%s", belkopf.auf_ext ) ;
	aufk.tou = belkopf.tou  ;
	sprintf ( aufk.pers_nam ,"order" ) ;
	sqldatdst ( & aufk.komm_dat, belkopf.dtm_50_feld ) ;
/* ---->
	if ( strlen ( belkopf.dtm_137_feld ) > 9 )
		sqldatdst ( & aufk.best_dat, belkopf.dtm_137_feld ) ;
	else
		sqldatdst ( & aufk.best_dat, belkopf.dtm_50_feld ) ;
< --- */

	aufk.waehrung = 2 ;
	aufk.auf_art = 0  ;
	aufk.gruppe = 0 ;
	aufk.ccmarkt = 0 ;
	aufk.fak_typ = 0 ;
	aufk.tou_nr = belkopf.tou ;
	sprintf ( aufk.ueb_kz , "" ) ;
	sqldatdst ( & aufk.fix_dat, belkopf.dtm_137_feld ) ;
	sprintf ( aufk.komm_name , "" ) ;
	sqldatdst ( & aufk.akv, belkopf.dtm_137_feld ) ;
	sprintf ( aufk.akv_zeit , "" ) ;
	sqldatdst ( & aufk.bearb, belkopf.dtm_137_feld ) ;
	sprintf ( aufk.bearb_zeit , "" ) ;
	aufk.psteuer_kz = 0 ;
	dpos_zahl = 1 ;	// Positionscounter 
}

void ladeaufp(void )
{
	// aufk ist bestueckt , belpos zeigt auf dazugeh�rigen Satz
	memcpy ( &aufp,&aufp_null, sizeof(struct AUFP));

	aufp.mdn = mdn.mdn ; 
	aufp.fil = 0  ;
	aufp.auf = aufk.auf ;
	aufp.posi = dpos_zahl * 10  ;
				dpos_zahl ++ ;
	aufp.aufp_txt = 0 ;
	aufp.a = belpos.a ;
	aufp.auf_me = belpos.auf_me  ;
	sprintf ( aufp.auf_me_bz , "%s" , belpos.auf_me_bz ) ;
	aufp.lief_me = 0.0 ;
	sprintf ( aufp.lief_me_bz , "%s" , belpos.lief_me_bz ) ;
	aufp.auf_vk_pr = belpos.auf_vk_pr ;

	aufp.auf_lad_pr = belpos.auf_lad_pr ;
// Auszeichnungspreismechanismus : 
//	falls lad_pr aus der Preisfindung == 0 ,
//	dann muss diese "0"  durchgereicht werden wegen irgendwelcher PAZ-Abl�ufe(Etiketten-Layout)

// falls lad_pr > 0 UND Ausz.Preis > 0 dann wird Preis der Preissuche durch den aktuellen Wert ueberschrieben

	if ((belpos.auf_lad_pr > 0.001 ) && ( belpos.pri_aae_wert > 0.001 ))
	aufp.auf_lad_pr = belpos.pri_aae_wert ;
	aufp.delstatus = 0 ;
	aufp.sa_kz_sint = belpos.sa_kz_sint ;
	aufp.prov_satz = belpos.prov_satz ;
	aufp.ksys = 0 ;
	aufp.pid = 0 ;
	aufp.auf_klst = 0  ;
	aufp.teil_smt = belpos.teil_smt  ;
	aufp.dr_folge = belpos.dr_folge  ;
	aufp.inh  = belpos.inh ;
	aufp.auf_vk_euro = aufp.auf_vk_pr ;	// zwangsgleichweaehrung
	aufp.auf_vk_fremd = 0.0 ;
	aufp.auf_lad_euro = aufp.auf_lad_pr  ;
	aufp.auf_lad_fremd = 0.0 ;
	aufp.rab_satz = 0.0 ;
	aufp.me_einh_kun = belpos.me_einh_kun ;
	aufp.me_einh = belpos.me_einh ;
	aufp.me_einh_kun1 = 0 ;
	aufp.auf_me1 = 0 ;
	aufp.inh1 = 0 ;
	aufp.me_einh_kun2 = 0 ;
	aufp.auf_me2 = 0 ;
	aufp.inh2 = 0 ;
	aufp.me_einh_kun3 = 0 ;
	aufp.auf_me3 = 0 ;
	aufp.inh3 = 0 ;
	aufp.gruppe = 0 ;
	sprintf (  aufp.kond_art ,"" ) ;
	aufp.a_grund = belpos.a_grund ;
	aufp.ls_pos_kz = 0 ;
	aufp.posi_ext = 0 ;	// evtl. spaeter mal nachbauen ??
	aufp.kun = aufk.kun  ;
	aufp.a_ers  ;
	sprintf (aufp.ls_charge , "" ) ;
	aufp.aufschlag = 0 ;
	aufp.aufschlag_wert = 0 ;
	aufp.pos_txt_kz = 0 ;

	if ( dlgr_bestand && aufp.auf_me > 0 )
	{
		memcpy ( &bsd_buch,&bsd_buch_null, sizeof(struct BSD_BUCH));
		bsd_buch.nr = aufp.auf ;
        sprintf ( bsd_buch.blg_typ , "A" ) ;
        bsd_buch.mdn = aufp.mdn ;
        bsd_buch.fil = aufp.fil ;	// wohl immer == 0
        bsd_buch.kun_fil = 0 ;        // immer kunden-Auf
        bsd_buch.a = aufp.a ;

		time_t timer;
		struct tm *ltime;
		time (&timer);
 		ltime = localtime (&timer);

		bsd_buch.dat.day = ltime->tm_mday ;
		bsd_buch.dat.month = ltime->tm_mon + 1 ;
		bsd_buch.dat.year = ltime->tm_year + 1900 ;
		bsd_buch.dat.hour = 0 ;
		bsd_buch.dat.minute = 0 ;
		bsd_buch.dat.second = 0 ;
		bsd_buch.dat.fraction = 0 ;

		sprintf ( bsd_buch.zeit , "%02d:%02d:%02d",
			ltime->tm_hour, ltime->tm_min, ltime->tm_sec );

        sprintf ( bsd_buch.pers , "%s" , aufk.pers_nam ) ;
		if ( dlgr_gr > -1 )
		{
			a_lgr.a = aufp.a ;
			lgr.mdn = aufp.mdn ;
			lgr.lgr_gr = dlgr_gr ;
			int retco = lgr_class.lese_a_lager () ;
			if ( ! retco ) 
				sprintf ( bsd_buch.bsd_lgr_ort, "%d", a_lgr.lgr ) ;
			else
				sprintf ( bsd_buch.bsd_lgr_ort, "0" ) ;
		}
		else
			sprintf ( bsd_buch.bsd_lgr_ort ,"0" ) ;

        bsd_buch.me = aufp.auf_me ;
        bsd_buch.bsd_ek_vk = aufp.auf_vk_euro ;
        sprintf ( bsd_buch.chargennr , "" ) ; 
        bsd_buch.auf = aufp.auf ;
        sprintf ( bsd_buch.err_txt , "order:01.01.2010" ) ;
		bsd_buch_class.insertbsd_buch () ;
	}
}

int auswerten (char *quellname ) 
{
// zuerst  Koepfe, danach evtl 0 - n Posten ,
// es koennten aber Posten fehlen 

int statkopf = 0 ;	// 0 = gut gelesen + gut geladen , muss noch geschrieben werden  

char s1[196] ;
char s2[196] ;

// bei immerweiter == 1 wird posbug NICHT als Abbruchkrit. gewertet, jedoch dieser Posten NICHT geschrieben
		// schreibe alle Auftraege : WICHTIG : leere Koepfe gibbet NICHT !!!! - sonst kommt Unsinn raus 
//		dbClass.beginwork () ;
		belkopfakt = belposakt = 0 ;
		statkopf = read_belkopf_a (0) ;
		if ( !statkopf )
		{
			ladeaufk() ;
			while ( ! read_belpos_a () )
			{
				if (! strncmp ( belpos.auf_ext, belkopf.auf_ext, strlen ( belkopf.auf_ext )) )
				{
					ladeaufp () ;
//					if ( (strlen ( belpos.bemerkung)) == 0  && belpos.auf_me != 0.0 )	// einerseits immerweiter andererseits keine Leermengen  
//						aufp_class.insertaufp () ;
				}
				else
				{
//					aufk_class.insertaufk () ;
					statkopf = read_belkopf_a (0) ;
					if ( statkopf ) break ;
					ladeaufk () ;
					ladeaufp () ;
//					if ( (strlen ( belpos.bemerkung)) == 0 && belpos.auf_me != 0.0 )  // einerseits immerweiter andererseits keine Leermengen
//						aufp_class.insertaufp()  ;
				}
			}
//			if ( !statkopf ) aufk_class.insertaufk () ;
		}
//		dbClass.commitwork() ;

		
	//		char zielname[555] ;
//		sprintf ( zielname, "%s.sav", quellname ) ;
//		unlink( zielname ) ;
//		rename ( quellname, zielname ) ;
//	} nur schreiben, wenn alles ok ....

	// Falls bugs vorhanden sind, dann unbedingt Protokoll aufpoppen,
	// ansonsten kann hier problemlos irgendwann ein nice-to-have-protokoll aktiviert werden
	// -> nur "bemerkung nicht empty" , alle Koepfe  und so weiter als Varianten

	if ( 1 )	// Immer Druckausgabe, kann ja dann abgewaehlt werden 
	{
		sprintf ( tempdrx.form_nr, "recadvpro") ;	// recadv-Protokoll

		tempdrx.lila = 0 ;

		tempdrx_class.deletetempdrx () ;

		sprintf ( s2, "%s\\recadvpro.prm", getenv ( "TMPPATH" ) ) ;

		int kk = tempdrx_class.lesetempdrx ()	;
		tempdrx.lfd ++ ;	// Max + 1
		lfdliste = tempdrx.lfd ;
		lfdlistennr =  0 ;

// Prinzip : kopfsatz gibt es immer, pos-Saetze darunter koennten fehlen 

		belkopfakt = belposakt = 0 ;
		statkopf = read_belkopf_a (1) ;
		if ( !statkopf && belkopf.int_kun == 0 )
		{	// Zusaetzliche Kopf-Meckereien
			wrilistekopfsatz() ;
			statkopf = read_belkopf_a (1) ;
		}
		if ( !statkopf )
		{
			wrilistekopfsatz() ;
			while ( ! read_belpos_a () )
			{
				if (! strncmp ( belpos.auf_ext, belkopf.auf_ext, strlen ( belkopf.auf_ext )) )
				{
					wrilistepossatz () ;
				}
				else
				{
					statkopf = read_belkopf_a (1) ;
					if ( statkopf ) break ;
					wrilistekopfsatz () ;
					wrilistepossatz () ;
				}
			}
		}

		FILE * fp2 ;
		fp2 = fopen ( s2 , "w" ) ;
		if ( fp2 == NULL )
		{
			return -1  ;
		}
	
		sprintf ( s1, "NAME recadvpro\n" ) ; 
	
		CString St = s1;
		St.MakeUpper() ;
		sprintf ( s1 ,"%s", St.GetBuffer(512)) ;
		fputs ( s1 , fp2 );


		fputs ( "LABEL 0\n" , fp2 ) ;

		sprintf ( s1, "DRUCK 1\n"   ) ;
		fputs ( s1 , fp2 ) ;

		fputs ( "ANZAHL 1\n" , fp2 ) ;


		sprintf ( s1, "MITSORTNR %d\n"  , verdicht );
		fputs ( s1 , fp2 ) ;


		sprintf ( s1, "lfd %ld %ld\n", lfdliste , lfdliste   ) ;
		fputs ( s1 , fp2) ;


		fclose ( fp2) ;

/* ---> das gibbet hier alles nicht
		ModDialog moddialog ;
		int NurProt = 0 ;
		int NurProtaktiv = 1 ;
		int ListeDicht = 0 ;
		int ListeDichtaktiv = 1 ;
		int Details = 1 ;
		int Detailsaktiv = 1 ;
		int VerBuchen = 0 ;
		int VerBuchenaktiv = 0 ;
// im TEXTBLOCK
		moddialog.setzeparas( NurProt,NurProtaktiv
			                  ,ListeDicht,ListeDichtaktiv
							  ,Details,Detailsaktiv
							  ,VerBuchen,VerBuchenaktiv ) ;
		if ( moddialog.DoModal () != IDOK )
		{
			return -1 ;
		}
// im TEXTBLOCK
		moddialog.holeparas ( &NurProt, &ListeDicht, &Details, &VerBuchen ) ;
		if ( Details )
		{
			sprintf ( s1, "dr70001.exe -datei \"%s\" " , s2 );
			int ex_code = ProcWaitExec( (char *)s1, SW_SHOW, -1, 0, -1, 0 );
		}
		if ( ListeDicht )
		{
			sprintf ( s1, "dr70001.exe -datei \"%s\" " , s3 );
			int ex_code = ProcWaitExec( (char *)s1, SW_SHOW, -1, 0, -1, 0 );
		}
// im TEXTBLOCK
		if ( NurProt )
		{
			sprintf ( s1, "dr70001.exe -datei \"%s\" " , s4 );
			int ex_code = ProcWaitExec( (char *)s1, SW_SHOW, -1, 0, -1, 0 );
		}
		if ( VerBuchen )
		{
			VerbucheListe () ;
		}
< ---- */
		sprintf ( s1, "dr70001.exe -datei \"%s\" " , s2 );
		int ex_code = ProcWaitExec( (char *)s1, SW_SHOW, -1, 0, -1, 0 );
	}
	lfdliste = 0 ;

	return dabbruch  ;

}



void InterpretCommandLine(void)
//=============================================================================
{
    LPSTR CommandLine = GetCommandLine ();
    CToken Token (CommandLine, " ");

    char *px;
	char p[299] ;


	Token.NextToken ();
	while ((px = Token.NextToken ()) != NULL)
	{

		CString St = px;
		St.MakeUpper() ;
		sprintf ( p ,"%s", St.GetBuffer(512)) ;

		if (strcmp (p, "-PLATTFORM") == 0)
		{
			dplattform = 1 ;
		}

		if (strcmp (p, "-FEGRO") == 0)
		{
			doption = O_FEGRO ;
		}
		if (strcmp (p, "-METRO") == 0)
		{
			doption = O_METRO ;
		}
		if (strcmp (p, "-REWE") == 0)
		{
			doption = O_REWE ;
			immerweiter = 0 ;
		}
		if (strcmp (p, "-KAUFLAND") == 0)
		{
			doption = O_KAUFLAND ;
		}
		if (strcmp (p, "-EDEKA") == 0)
		{
			doption = O_EDEKA ;
		}
		if (strcmp (p, "-TENGELMANN") == 0)
		{
			doption = O_TENGELMANN ;
		}
	}
}

void ladesmtDatei () 
{
	sprintf ( branche0 , "" ) ;
	sprintf ( branche1 , "" ) ;
	sprintf ( branche2 , "" ) ;
	sprintf ( branche3 , "" ) ;
	sprintf ( branche4 , "" ) ;
	sprintf ( teilsort0 , "" ) ;
	sprintf ( teilsort1 , "" ) ;
	sprintf ( teilsort2 , "" ) ;
	sprintf ( teilsort3 , "" ) ;
	sprintf ( teilsort4 , "" ) ;

	if ( teilsmttrennen  )
	{
		FILE *fp ;
		char *etc;
		char ibuffer [512];

		etc = getenv ("BWSETC");
		if (etc == (char *) 0)
		{
                    etc = "C:\\USER\\FIT\\ETC";
		}

		sprintf (ibuffer, "%s\\dta%d.par", etc , dta.dta ) ;
		fp = fopen (ibuffer, "r");
		if (fp == NULL)	// nichts gefunden , dann wird wohl auch nichts sinnvolles mehr passieren
			return ;

		for ( int i = 1 ; i < 4 ; i++ )
		{
			if ( ! fgets (ibuffer, 511, fp))
			break ;	// letzte Zeile gefunden 

			ibuffer[ 105] = '\0' ;	// Notbremse
			cr_weg ( ibuffer ) ;
// Kundenbranche darf maximal 2 Zeichen lang sein, fuehrende und folgende blanks vor dem "$" ignoriere ich mal grosszuegig

			int ii , ij ;
			ii= ij = 0 ;
			char branbu[5] ;
			branbu[0] = '\0' ;
			while ( ii < 105 )
			{
				if ( ibuffer[ii] == '\0' )
					break ;
				if ( ibuffer[ii] == '$' )
					break ;
				if ( ibuffer[ii] == ' ' )
				{
					ii++ ; 
					continue ;
				}
				branbu[ij] = ibuffer[ii] ;
				ij ++ ;
				branbu[ij] = '\0' ;
				if ( ij > 1 )	break ;	// Branche maximal 2 zeichen lang .....
				ii ++ ;
			}

			if ( i == 1 ) sprintf ( branche0,"%s", branbu ) ;
			if ( i == 2 ) sprintf ( branche1,"%s", branbu ) ;
			if ( i == 3 ) sprintf ( branche2,"%s", branbu ) ;

		// Achtung : ii wird weitergezaehlt !! 

			while ( ii < 105 )
			{
				if ( ibuffer[ii] == '\0' )
					break ;
				if ( ibuffer[ii] == '$' )
				{
					ii ++ ;	// "$" skippen 
					if ( i == 1 ) sprintf ( teilsort0,"%s", ibuffer + ii ) ;
					if ( i == 2 ) sprintf ( teilsort1,"%s", ibuffer + ii ) ;
					if ( i == 3 ) sprintf ( teilsort2,"%s", ibuffer + ii ) ;
					break ;
				}
				ii ++ ;
			}
		}
		fclose (fp) ;
	}
}


int Abarbeiten ( CrecadvDlg * Cparent , short dmdn , char * quelldatei , int testignore )
{


	InterpretCommandLine () ;


	ArraysInit () ;

	dtestignore = testignore ;
	dtestmode = 0 ;	// hier wird der testmode eingefangen

	char hilfe[256] ;
	wsprintf( hilfe, "%s", getenv("testmode") );
	TEST = 0 ;
	if ( atoi ( hilfe) == 5 ) TEST =1 ;

	dkopfbug = dposbug = dabbruch = 0 ;

	maskel = 0 ;

	fpin = fopen ( quelldatei , "r" );
	int i =	 einlesen ( Cparent ,dmdn, quelldatei,  testignore ) ;

// jetzt sind daten da und wir schreiben diese nun in die generator-Liste

	i = danz_nachrichten ;
	i = auswerten ( quelldatei ) ;


	ArraysFree () ;
	sprintf ( hilfe ,"  %d Belege bearbeitet " , danz_nachrichten ) ; 
	Cparent->myDisplayMessage( hilfe ) ;

	return i ;
}

