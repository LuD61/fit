
#include "stdafx.h"
#include "recadv.h"
#include "recadvDlg.h"

#include "DbClass.h"
#include "mdn.h"
#include "adr.h"
#include "dta.h"
#include "Abarbeit.h"
#include "DllPreise.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

extern CDllPreise DllPreise ; // eigentlich nur wegen Programmabbruch

extern DB_CLASS dbClass ;
extern MDN_CLASS mdn_class ;
extern ADR_CLASS adr_class ;
extern DTA_CLASS dta_class ;

extern int teilsmttrennen;
extern int verdicht;
extern void cr_weg ( char * );
int testignore ;
long aktivdta;
char bufh [256] ;


// CAboutDlg-Dialogfeld f�r Anwendungsbefehl "Info"

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialogfelddaten
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterst�tzung

// Implementierung
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CrecadvDlg-Dialogfeld


CrecadvDlg::CrecadvDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CrecadvDlg::IDD, pParent)
	, v_mandant(_T(""))
	, v_mdnname(_T(""))
	, v_combo1(_T(""))
	, v_filename(_T(""))
	, v_check1(TRUE)
	, v_info(_T(""))
	, v_check2(FALSE)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CrecadvDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_MANDANT, m_mandant);
	DDX_Text(pDX, IDC_MANDANT, v_mandant);
	DDX_Control(pDX, IDC_MDNNAME, m_mdnname);
	DDX_Text(pDX, IDC_MDNNAME, v_mdnname);
	DDX_Control(pDX, IDC_COMBO1, m_combo1);
	DDX_CBString(pDX, IDC_COMBO1, v_combo1);
	DDX_Control(pDX, IDC_FILENAME, m_filename);
	DDX_Text(pDX, IDC_FILENAME, v_filename);
	DDX_Control(pDX, IDC_CHECK1, m_check1);
	DDX_Check(pDX, IDC_CHECK1, v_check1);
	DDX_Control(pDX, IDC_INFO, m_info);
	DDX_Text(pDX, IDC_INFO, v_info);
	DDX_Check(pDX, IDC_CHECK2, v_check2);
	DDX_Control(pDX, IDC_CHECK2, m_check2);
}

BEGIN_MESSAGE_MAP(CrecadvDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDOK, &CrecadvDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_CHECK1, &CrecadvDlg::OnBnClickedCheck1)
	ON_EN_KILLFOCUS(IDC_MANDANT, &CrecadvDlg::OnEnKillfocusMandant)
	ON_EN_KILLFOCUS(IDC_FILENAME, &CrecadvDlg::OnEnKillfocusFilename)
	ON_BN_CLICKED(IDC_BUTTON1, &CrecadvDlg::OnBnClickedButton1)
	ON_CBN_SELCHANGE(IDC_COMBO1, &CrecadvDlg::OnCbnSelchangeCombo1)
	ON_CBN_KILLFOCUS(IDC_COMBO1, &CrecadvDlg::OnCbnKillfocusCombo1)
	ON_BN_CLICKED(IDC_CHECK2, &CrecadvDlg::OnBnClickedCheck2)
END_MESSAGE_MAP()


void CrecadvDlg::lesesmtDatei () 
{
	if ( dta.dta == -1 )
	{
		// Initialisieren - deaktivieren 

		aktivdta = -1 ;
		teilsmttrennen = 0 ;
		return ;
	}
	if ( aktivdta == dta.dta )
		return ;	// nicht mehrfach lesen bzw. modifizieren

	aktivdta = dta.dta ;

	FILE *fp ;
	char *etc;
    char ibuffer [512];

    etc = getenv ("BWSETC");
    if (etc == (char *) 0)
    {
                    etc = "C:\\USER\\FIT\\ETC";
    }

	sprintf (ibuffer, "%s\\dta%d.par", etc , dta.dta ) ;
	fp = fopen (ibuffer, "r");
    if (fp == NULL)	// nichts gefunden .....
	{
//		m_beding1.EnableWindow (FALSE) ;
//		m_beding2.EnableWindow (FALSE) ;
//		m_beding3.EnableWindow (FALSE) ;
//		b_tsbeacht = FALSE ;
		teilsmttrennen = 0 ;

		return ;
	}

	fclose (fp) ;
}

void CrecadvDlg::schreibesmtDatei () 
{
	if ( dta.dta == -1 )
			return ;

//	if ( b_tsbeacht == FALSE )			return ;

	FILE *fp ;
	char *etc;
    char ibuffer [512];

    etc = getenv ("BWSETC");
	// ist aktivdta == dta.dta oder etwa nicht ? 

    if (etc == (char *) 0)
    {
                    etc = "C:\\USER\\FIT\\ETC";
    }

	sprintf (ibuffer, "%s\\dta%d.par", etc , dta.dta ) ;
	unlink ( ibuffer ) ;
	fp = fopen (ibuffer, "w+");

    if (fp == NULL)	
		return ;	// Problem ?!
	
/* ---
	sprintf(ibuffer,"%s\n",v_beding1.GetBuffer(0)) ; fputs (ibuffer, fp) ;
	sprintf(ibuffer,"%s\n",v_beding2.GetBuffer(0)) ; fputs (ibuffer, fp) ;
	sprintf(ibuffer,"%s\n",v_beding3.GetBuffer(0)) ; fputs (ibuffer, fp) ;
<---- */

    fclose ( fp ) ;

}


// CrecadvDlg-Meldungshandler

BOOL CrecadvDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Hinzuf�gen des Men�befehls "Info..." zum Systemmen�.

	// IDM_ABOUTBOX muss sich im Bereich der Systembefehle befinden.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Symbol f�r dieses Dialogfeld festlegen. Wird automatisch erledigt
	//  wenn das Hauptfenster der Anwendung kein Dialogfeld ist
	SetIcon(m_hIcon, TRUE);			// Gro�es Symbol verwenden
	SetIcon(m_hIcon, FALSE);		// Kleines Symbol verwenden

	// TODO: Hier zus�tzliche Initialisierung einf�gen

/* ----> Preise brauchen wir hier nicht
		if (DllPreise.PriceLib == NULL)
		{
			OnCancel() ;
			return -1 ;
		}
< ----- */

	// alle dta lesen

	teilsmttrennen = 0 ;
	aktivdta = -1 ;

	CString szdta ;

	int sqlstat = dta_class.openalldta ();
	sqlstat = dta_class.lesealldta();
	while(!sqlstat)
	{
	
			szdta.Format("%8.0d  %s",dta.dta,dta.kun_krz1);
				// hier haben wir jetzt die dta und k�nnen es in die ListBox einf�gen
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->AddString(szdta.GetBuffer(0));
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->SetCurSel(0);
	
		sqlstat = dta_class.lesealldta () ;
	}

	testignore = 1 ;	// immer ignorieren, da wir ja hinterher nochmal entscheiden
	v_check1 = TRUE;


	UpdateData ( FALSE ) ;
	((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->SetCurSel(0);

	UpdateData ( TRUE ) ;

	bufh[0] = '\0' ;
	int nCurSel = ((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->GetCurSel();
	CString bufx;
	dta.dta = -1 ;
	if ( nCurSel > -1 )
	{
		((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->GetLBText(nCurSel, bufx);

		sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
		int i = (int) strlen ( bufh );
		// fixe formatierung : 8 stellen und 2 blanks , danach folgt ein text
		if ( i )
		{
			if ( i < 9 )
			{
				bufh[i ] = '\0' ;
			}
			else
			{
				bufh[9] = '\0' ;
			}
			dta.dta = atol ( bufh ) ;
			i = dta_class.opendta();
			i = dta_class.lesedta();
			if ( i )
			{
				dta.dta = -1	;	// grosser Mist  
			}
		}
		else
		{
			dta.dta = -1 ;
		}
	}
// brauchen wir hier nicht 	lesesmtDatei () ;

	v_mandant.Format ( _T("1")) ;
	mdn.mdn = 1;
	mdn_class.openmdn();
	if (! mdn_class.lesemdn())
	{
		adr.adr = mdn.adr ;
		int i = adr_class.openadr () ;
		i = adr_class.leseadr () ;
		if (!i) 
		{
			v_mdnname.Format("%s",_T(adr.adr_krz));
		}
		else
		{
			v_mdnname.Format("              ");
		}
	}
	else	// fehlerhafte Eingabe
	{
		v_mdnname.Format("              ");
	}

	UpdateData ( FALSE ) ;

	return TRUE;  // Geben Sie TRUE zur�ck, au�er ein Steuerelement soll den Fokus erhalten
}

void CrecadvDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// Wenn Sie dem Dialogfeld eine Schaltfl�che "Minimieren" hinzuf�gen, ben�tigen Sie 
//  den nachstehenden Code, um das Symbol zu zeichnen. F�r MFC-Anwendungen, die das 
//  Dokument/Ansicht-Modell verwenden, wird dies automatisch ausgef�hrt.

void CrecadvDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Ger�tekontext zum Zeichnen

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Symbol in Clientrechteck zentrieren
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Symbol zeichnen
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// Die System ruft diese Funktion auf, um den Cursor abzufragen, der angezeigt wird, w�hrend der Benutzer
//  das minimierte Fenster mit der Maus zieht.
HCURSOR CrecadvDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

// Anfang pretranslate

BOOL CrecadvDlg::PreTranslateMessage(LPMSG lpMsg)
{
	CWnd *cWnd;
	 if (lpMsg->message == WM_KEYDOWN)
	 {
		 switch (lpMsg->wParam)
			 {
		        case VK_RETURN :
					cWnd = GetFocus ();
			           if(cWnd == GetDlgItem (IDOK))
							return CDialog::PreTranslateMessage(lpMsg);

			           if(cWnd == GetDlgItem (IDCANCEL))
					   {
							OnCancel ();
 							return TRUE;
					   }

/* --->
			           if(cWnd == GetDlgItem (IDC_KUNN))
					   {	Read (); return TRUE; }
                       else if (cWnd == GetDlgItem (IDC_MDN))
                       { if (ReadMdn () == FALSE)
                       { return TRUE;
                       } }
 < ----- */
			   NextDlgCtrl();
						return TRUE;

			 case VK_F5 :
                     OnCancel ();
 			         return TRUE;
/* ----->
			 case VK_F7 :
                     OnDelete ();
 			         return TRUE;
             case VK_F9 :
		             if (OnF9 ())
                     {
                         return TRUE;
                     }
                     break;
		     case VK_F6 :
                     OnFree ();
				     return TRUE;
 <----- */

/* ---->
			 case VK_F12 :
					UpdateData( TRUE ) ;
					ladevartodaba();
					if (  tou_class.testupdtou() )
						tou_class.inserttou () ;
					else
						tou_class.updatetou () ;

					v_edithtour = 0 ;
					tou.tou = 0 ;
					UpdateData(FALSE) ;
					m_edithtour.EnableWindow (TRUE) ;
					m_buttonhtour.EnableWindow ( TRUE) ;
					m_edithtour.ModifyStyle (0, WS_TABSTOP,0) ;
					m_buttonhtour.ModifyStyle ( 0, WS_TABSTOP,0) ;
					m_edithtour.SetFocus () ;	


//                     OnOK ();
				     return TRUE;
< ----- */


 		    case VK_DOWN :
                     if (NoListCtrl (GetFocus ()))
                     {
						NextDlgCtrl();
						return TRUE;
					
                     }
				     break;
 		    case VK_UP :
                     if (NoListCtrl (GetFocus ()))
                     {
						PrevDlgCtrl();
	 					cWnd = GetFocus ();
					 	GetNextDlgTabItem(cWnd ,FALSE) ;
						return TRUE;
                     }
				     break;
		 }
	 }

return CDialog::PreTranslateMessage(lpMsg);

}

BOOL CrecadvDlg::NoListCtrl (CWnd *cWnd)
{
    if (((CComboBox *) cWnd->GetParent ())->GetCount () > 1)
    {
        return FALSE;
    }
    else if (((CComboBox *) cWnd)->GetCount () > 1)
    {
        return FALSE;
    }
    else if (((CListBox *) cWnd)->GetCount () > 1)
    {
        return FALSE;
    }
    return TRUE;
}

// Ende pretranslate

void CrecadvDlg::OnBnClickedOk()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	int allok = TRUE ;

	UpdateData (TRUE) ;
	int i = m_mandant.GetLine(0,bufh,500);
	bufh [i] = '\0' ;	// 250810
	if (i)	mdn.mdn = (short) atoi ( bufh );
	else mdn.mdn = -2 ;
	mdn_class.openmdn();
	if (! mdn_class.lesemdn())
	{
		adr.adr = mdn.adr ;
		i = adr_class.openadr () ;
		i = adr_class.leseadr () ;
		if (!i) 
		{
			v_mdnname.Format("%s",_T(adr.adr_krz));
		}
		else
		{
			v_mdnname.Format("              ");
			allok = FALSE ;
		}
	}
	else	// fehlerhafte Eingabe
	{
		v_mdnname.Format("              ");
		allok = FALSE ;
	}

	if ( v_check2 == TRUE )
		verdicht = 2 ;
	else
		verdicht =1 ;

// duerfen wir hier gar nicht, ist nur fuer Orders wichtig : schreibesmtDatei() ;

	FILE *fp;

	i = m_filename.GetLine(0,bufh,500);
	bufh [i] = '\0' ;	// 250810
	if (i > 0)
	{

		fp = fopen (bufh, "r");
        if (fp == NULL)
		{
			allok = FALSE ;
			v_filename = "" ;
		}
		else
			fclose (fp) ;
	}
	else
	{
		// gar nichts drin 
		allok = FALSE ;
	}

	UpdateData (FALSE) ;
	if ( ! allok )
	{
		MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);

	}
	else
	{

		// Achtung : bufh wurde beim Test gerade erst sinnvoll gefuellt

		Abarbeiten(  this,  mdn.mdn, bufh , testignore)   ;
	}
// nur mit Cancel kann das Programm verlassen werden : OnOK();
}

void CrecadvDlg::OnBnClickedCheck1()
{

		UpdateData ( TRUE ) ;
	if ( v_check1 == TRUE )
	{
		testignore = 1 ;
	}
	else
	{
		testignore = 0 ;
	}

	UpdateData( FALSE ) ;
}

void CrecadvDlg::OnEnKillfocusMandant()
{
	
	UpdateData (TRUE) ;
	int i = m_mandant.GetLine(0,bufh,500);
	bufh [i] = '\0' ;	// 250810
	if (i)	mdn.mdn = (short) atoi ( bufh );
	else mdn.mdn = -2 ;
	mdn_class.openmdn();
	if (! mdn_class.lesemdn())
	{
		adr.adr = mdn.adr ;
		i = adr_class.openadr () ;
		i = adr_class.leseadr () ;
		if (!i) 
		{
			v_mdnname.Format("%s",_T(adr.adr_krz));
		}
		else
		{
			v_mdnname.Format("              ");
			MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
			PrevDlgCtrl();
		}
	}
	else	// fehlerhafte Eingabe
	{
		v_mdnname.Format("              ");
		MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
		PrevDlgCtrl();
	}
		UpdateData (FALSE) ;
}

void CrecadvDlg::OnEnKillfocusFilename()
{

	UpdateData (TRUE) ;

	FILE *fp;

	int i = m_filename.GetLine(0,bufh,500);
	bufh [i] = '\0' ;	// 250810
	if (i > 0)
	{

		fp = fopen (bufh, "r");
        if (fp == NULL)
		{
			MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
			v_filename = "" ;
			UpdateData (FALSE) ;
			PrevDlgCtrl();
		}
		else
			fclose (fp) ;
	}

	UpdateData (FALSE) ;

}

void CrecadvDlg::OnBnClickedButton1()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.

		OPENFILENAME fnstruct;
		LPCTSTR lpstrFilter = "Alle Dateien\0*.*\0,\0"  ;            // "Info-Datei\0*.inf\0Alle Dateien\0*.*\0,\0";

		char hilfe[256] ;
		wsprintf( hilfe, "%s", getenv("EDIWE") );
		if ( strlen ( hilfe ) < 3 )
		{
			wsprintf( hilfe, "%s", getenv("EDI") );
			if ( strlen ( hilfe ) < 3 )
			{
				wsprintf( hilfe, "%s", getenv("BWS") );
			}
		}
	   
		LPCTSTR lpstrInitialDir = "Alle Dateien\0*.*\0,\0"  ;            // "Info-Datei\0*.inf\0Alle Dateien\0*.*\0,\0";
		char fname [256] ;
		sprintf ( fname, "" ) ;
		
        ZeroMemory (&fnstruct, sizeof (fnstruct));
        fnstruct.lStructSize = sizeof (fnstruct);
		fnstruct.hwndOwner   = this->m_hWnd ;// AktivWindow;
        fnstruct.lpstrFile   = (LPSTR) fname;
        fnstruct.nMaxFile    = 255;
        fnstruct.lpstrFilter = lpstrFilter;
        fnstruct.lpstrInitialDir  = _T(hilfe) ;

        if (GetOpenFileName (&fnstruct) == 0)
        {

		//			v_filename = ""  ; // nach Abbruch bleibt der vorherige Inhalt bestehen ..... 

		}
		else
        {
			wsprintf ( hilfe ,"%s",_T(fnstruct.lpstrFile)) ;
			v_filename = _T (hilfe) ;
        }
		UpdateData(FALSE) ;

}

void CrecadvDlg::OnCbnSelchangeCombo1()
{
		
	char bufh[400] ;
//	traktiv = 1 ;
	UpdateData(TRUE) ;
//	int i = m_Combo1.GetLine(0,bufh,9) ;

	int nCurSel = ((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->GetCurSel();
	CString bufc;
	((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->GetLBText(nCurSel, bufc);

	sprintf(bufh,"%s",bufc.GetBuffer(0)) ;

	// fixe formatierung : 8 stellen und 2 blanks , danch folgt ein text
	int i = (int)strlen ( bufh) ;
	if ( i )
	{
		if ( i < 9 )
		{
			bufh[i] = '\0' ;
		}
		else
		{
			bufh[9] = '\0' ;
		}
		dta.dta = atol ( bufh ) ;
		i = dta_class.opendta();
		i = dta_class.lesedta();
	}
	
}

void CrecadvDlg::OnCbnKillfocusCombo1()
{
	//	traktiv = 1 ;
	UpdateData(TRUE) ;
	bufh[0] = '\0' ;

	int nCurSel = ((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->GetCurSel();
	CString bufx;
	if ( nCurSel > -1 )
	{
		((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->GetLBText(nCurSel, bufx);

		sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
		int i = (int) strlen ( bufh );
		// fixe formatierung : 8 stellen und 2 blanks , danach folgt ein text
		if ( i )
		{
			if ( i < 9 )
			{
				bufh[i ] = '\0' ;
			}
			else
			{
				bufh[9] = '\0' ;
			}
			dta.dta = atol ( bufh ) ;
			i = dta_class.opendta();
			i = dta_class.lesedta();
			if ( i )
			{
				v_combo1.Format("                   ");
			}
			else
			{
/* -----> 
				if ( dta.reli == 1 )
				{
					m_edit2.EnableWindow( TRUE );
					m_edit5.EnableWindow( FALSE);
				}
				else
				{	
						// hier vorerst auch dta.reli == 2 mit dabei ...
					m_edit2.EnableWindow( FALSE );
					m_edit5.EnableWindow( TRUE );
				}
< ------- */
			}

		}
		else
		{
				v_combo1.Format("                   ");
		}
	}
	else
	{
		sprintf ( bufh, "%s", v_combo1 );
		int i = (int) strlen ( bufh );
		// fixe formatierung : 8 stellen und 2 blanks , danach folgt ein text
		if ( i )
		{
			if ( i < 9 )
			{
				bufh[i ] = '\0' ;
			}
			else
			{
				bufh[9] = '\0' ;
			}
		}
		long hilfe = atol ( bufh ) ;
		nCurSel = -1 ;
		sprintf ( bufh, "%8.0d", hilfe );
		nCurSel=((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->FindString(nCurSel, bufh);
	
		if (nCurSel != CB_ERR)
		{
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->SetCurSel(nCurSel) ;
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->GetLBText(nCurSel, bufx);
			sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
			i = (int) strlen ( bufh );
			// fixe formatierung : 8 stellen und 2 blanks , danach folgt ein text
			if ( i )
			{
				if ( i < 9 )
				{
					bufh[i] = '\0' ;
				}
			}
			else
			{
				bufh[9] = '\0' ;
			}
			dta.dta = atol ( bufh ) ;
			i = dta_class.opendta();
			i = dta_class.lesedta();
			if ( i )
			{
				v_combo1.Format("                   ");
				dta.dta = -1 ;				// 260808 
			}
			else
			{
				v_combo1.Format(bufx.GetBuffer(0));
/* ---->
				if ( dta.reli == 1 )
				{
					m_edit2.EnableWindow( TRUE );
					m_edit5.EnableWindow( FALSE);

				}
				else
				{
						// hier vorerst auch dta.reli == 2 mit dabei
					m_edit2.EnableWindow( FALSE );
					m_edit5.EnableWindow( TRUE );
				}
< ------ */
			}

		}
		else
		{
			dta.dta = -1 ;
			v_combo1.Format("                   ");
		}
	}
	lesesmtDatei () ;

	UpdateData(FALSE);

}
void CrecadvDlg::myDisplayMessage(char * message )
{
	// dummes ding, weil innerhalb eines events,  nuetzt das alles leider fast nichts.

	v_info.Format ( "%s" , _T( message) ) ;
	UpdateData(FALSE) ;
}


void CrecadvDlg::OnBnClickedCheck2()
{
	UpdateData ( TRUE ) ;

	UpdateData( FALSE ) ;
}
