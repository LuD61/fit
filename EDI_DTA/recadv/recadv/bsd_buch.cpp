#include "stdafx.h"
#include "DbClass.h"
#include "bsd_buch.h"

struct BSD_BUCH bsd_buch,  bsd_buch_null;
extern DB_CLASS dbClass;

BSD_BUCH_CLASS bsd_buch_class ;

int BSD_BUCH_CLASS::insertbsd_buch (void)
{

	if ( readcursor < 0 ) prepare () ;
         return dbClass.sqlexecute (ins_cursor);
}

void BSD_BUCH_CLASS::prepare (void)
{

	dbClass.sqlin ((long *) &bsd_buch.nr, SQLLONG, 0 ) ;
	dbClass.sqlin ((char *) &bsd_buch.blg_typ, SQLCHAR, 3 ) ;
	dbClass.sqlin ((short *) &bsd_buch.mdn, SQLSHORT, 0 ) ;
	dbClass.sqlin ((short *) &bsd_buch.fil, SQLSHORT, 0 ) ;
	dbClass.sqlin ((short *) &bsd_buch.kun_fil, SQLSHORT, 0 ) ;
	
	dbClass.sqlin ((double *) &bsd_buch.a, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((TIMESTAMP_STRUCT *) &bsd_buch.dat, SQLTIMESTAMP, 26 ) ;
	dbClass.sqlin ((char *) &bsd_buch.zeit, SQLCHAR,  9 ) ;
	dbClass.sqlin ((char *) &bsd_buch.pers, SQLCHAR, 13 ) ;
	dbClass.sqlin ((char *) &bsd_buch.bsd_lgr_ort, SQLCHAR, 13 ) ; 
	
	dbClass.sqlin ((short *) &bsd_buch.qua_status, SQLSHORT, 0 ) ;
	dbClass.sqlin ((double *) &bsd_buch.me, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((double *) &bsd_buch.bsd_ek_vk, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((char *) &bsd_buch.chargennr, SQLCHAR, 31 ) ;
	dbClass.sqlin ((char *) &bsd_buch.ident_nr, SQLCHAR, 21 ) ;
	
	dbClass.sqlin ((char *) &bsd_buch.herk_nachw, SQLCHAR, 4 ) ;
	dbClass.sqlin ((char *) &bsd_buch.lief, SQLCHAR, 17 ) ;
	dbClass.sqlin ((long *) &bsd_buch.auf, SQLLONG, 0 ) ;
	dbClass.sqlin ((char *) &bsd_buch.verfall, SQLCHAR, 2 ) ;
//	dbClass.sqlin ((TIMESTAMP_STRUCT *) &bsd_buch.verf_dat, SQLTIMESTAMP, 26 ) ;
	dbClass.sqlin ((short *) &bsd_buch.delstatus, SQLSHORT, 0 ) ;
	
	dbClass.sqlin ((char *) &bsd_buch.err_txt, SQLCHAR, 17 ) ;
	dbClass.sqlin ((double *) &bsd_buch.me2, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((short *) &bsd_buch.me_einh2, SQLSHORT, 0 ) ;
//	dbClass.sqlin ((TIMESTAMP_STRUCT *) &bsd_buch.lieferdat, SQLTIMESTAMP, 26 ) ;
	dbClass.sqlin ((double *) &bsd_buch.a_grund, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((short *) &bsd_buch.me_einh, SQLSHORT, 0 ) ;

	ins_cursor = (short) dbClass.sqlcursor ("insert into bsd_buch (  "
	 "  nr, blg_typ, mdn, fil, kun_fil " 
	" , a, dat, zeit, pers, bsd_lgr_ort "
	" ,	qua_status, me, bsd_ek_vk, chargennr, ident_nr "
	" , herk_nachw, lief, auf, verfall, delstatus "
	" , err_txt, me2, me_einh2, a_grund, me_einh "
	
	" ) values ( "
	 "  ?, ?, ?, ?, ? " 
	" , ?, ?, ?, ?, ? " 
	" , ?, ?, ?, ?, ? " 
	" , ?, ?, ?, ?, ? " 
	" , ?, ?, ?, ?, ? " 
	" ) " );
									

}

LGR_CLASS lgr_class ;
struct LGR lgr,  lgr_null;
struct A_LGR a_lgr,  a_lgr_null;

int LGR_CLASS::lese_a_lager (void)
{

	if ( readcursor < 0 ) prepare () ;
    return dbClass.sqlfetch (readcursor);
}

void LGR_CLASS::prepare (void)
{
	dbClass.sqlin ((double *) &a_lgr.a, SQLDOUBLE, 0 ) ;

	dbClass.sqlin ((short *) &lgr.lgr_gr, SQLSHORT, 0 ) ;
	dbClass.sqlin ((short *) &lgr.mdn, SQLSHORT, 0 ) ;

	dbClass.sqlout ((long *) &a_lgr.lgr, SQLLONG, 0 ) ;	// beachte : short lgr.lgr ; long a_lgr.lgr 

	readcursor = (short) dbClass.sqlcursor ("select a_lgr.lgr  from a_lgr  "
	" where  a_lgr.a = ?  and a_lgr.lgr in " 
	" ( select lgr.lgr from lgr where  lgr.lgr_gr = ? and  (  lgr.mdn = ? or lgr.mdn = 0 )) "  );
}
