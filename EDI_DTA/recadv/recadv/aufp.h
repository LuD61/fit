#ifndef _AUFP_DEF
#define _AUFP_DEF

struct AUFP {
short mdn ; 
short fil ;
long auf ;
long posi ;
long aufp_txt ;
double a ;
double auf_me ;
char auf_me_bz [7] ;
double lief_me ;
char lief_me_bz [7 ] ;
double auf_vk_pr ;
double auf_lad_pr ;
short delstatus ;
short sa_kz_sint ;
double prov_satz ;
long ksys ;
long pid ;
long auf_klst ;
short teil_smt ;
short dr_folge ;
double inh ;
double auf_vk_euro ;
double auf_vk_fremd ;
double auf_lad_euro ;
double auf_lad_fremd ;
double rab_satz ;
short me_einh_kun ;
short me_einh ;
short me_einh_kun1 ;
double auf_me1 ;
double inh1 ;
short me_einh_kun2 ;
double auf_me2 ;
double inh2 ;
short me_einh_kun3 ;
double auf_me3 ;
double inh3 ;
long gruppe ;
char kond_art [5] ;
double a_grund ;
short ls_pos_kz ;
long posi_ext ;
long kun ;
double a_ers ;
char ls_charge[31] ;
short aufschlag ;
double aufschlag_wert ;
short pos_txt_kz ;
};

extern struct AUFP aufp, aufp_null;

class AUFP_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int insertaufp (void);
               AUFP_CLASS () : DB_CLASS ()
               {
               }
};
#endif

