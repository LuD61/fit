#include "stdafx.h"
#include "DbClass.h"
#include "aufp.h"

struct AUFP aufp,  aufp_null;

extern DB_CLASS dbClass;

AUFP_CLASS aufp_class ;


int AUFP_CLASS::insertaufp (void)
{

         if (ins_cursor < 0 )
         {
             prepare ();
         }
         return ( dbClass.sqlexecute  (ins_cursor)) ;
}


void AUFP_CLASS::prepare (void)
{


	dbClass.sqlin ((short *) &aufp.mdn, SQLSHORT, 0 ) ; 
	dbClass.sqlin ((short *) &aufp.fil, SQLSHORT, 0 ) ;
	dbClass.sqlin ((long *) &aufp.auf, SQLLONG, 0 ) ;
	dbClass.sqlin ((long *) &aufp.posi, SQLLONG, 0 ) ;
	dbClass.sqlin ((long *) &aufp.aufp_txt, SQLLONG, 0 ) ;

	dbClass.sqlin ((double *) &aufp.a, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((double *) &aufp.auf_me, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((char *) aufp.auf_me_bz, SQLCHAR ,7 ) ;
	dbClass.sqlin ((double *) &aufp.lief_me, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((char *) aufp.lief_me_bz, SQLCHAR , 7 ) ;

	dbClass.sqlin ((double *) &aufp.auf_vk_pr, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((double *) &aufp.auf_lad_pr, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((short *) &aufp.delstatus, SQLSHORT, 0 ) ;
	dbClass.sqlin ((short *) &aufp.sa_kz_sint, SQLSHORT, 0 ) ;
	dbClass.sqlin ((double *) &aufp.prov_satz, SQLDOUBLE, 0 ) ;

	dbClass.sqlin ((long *) &aufp.ksys, SQLLONG, 0 ) ;
	dbClass.sqlin ((long *) &aufp.pid, SQLLONG, 0 ) ;
	dbClass.sqlin ((long *) &aufp.auf_klst, SQLLONG, 0 ) ;
	dbClass.sqlin ((short *) &aufp.teil_smt, SQLSHORT, 0 ) ;
	dbClass.sqlin ((short *) &aufp.dr_folge, SQLSHORT, 0 ) ;

	dbClass.sqlin ((double *) &aufp.inh, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((double *) &aufp.auf_vk_euro, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((double *) &aufp.auf_vk_fremd, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((double *) &aufp.auf_lad_euro, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((double *) &aufp.auf_lad_fremd, SQLDOUBLE, 0 ) ;

	dbClass.sqlin ((double *) &aufp.rab_satz, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((short *) &aufp.me_einh_kun, SQLSHORT, 0 ) ;
	dbClass.sqlin ((short *) &aufp.me_einh, SQLSHORT, 0 ) ;
	dbClass.sqlin ((short *) &aufp.me_einh_kun1, SQLSHORT, 0 ) ;
	dbClass.sqlin ((double *) &aufp.auf_me1, SQLDOUBLE, 0 ) ;

	dbClass.sqlin ((double *) &aufp.inh1, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((short *) &aufp.me_einh_kun2, SQLSHORT, 0 ) ;
	dbClass.sqlin ((double *) &aufp.auf_me2, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((double *) &aufp.inh2, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((short *) &aufp.me_einh_kun3, SQLSHORT, 0 ) ;

	dbClass.sqlin ((double *) &aufp.auf_me3, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((double *) &aufp.inh3, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((long *) &aufp.gruppe, SQLLONG, 0 ) ;
	dbClass.sqlin ((char *)  aufp.kond_art, SQLCHAR , 5 ) ;
	dbClass.sqlin ((double *) &aufp.a_grund, SQLDOUBLE, 0 ) ;

	dbClass.sqlin ((short *) &aufp.ls_pos_kz, SQLSHORT, 0 ) ;
	dbClass.sqlin ((long *) &aufp.posi_ext, SQLLONG, 0 ) ;
	dbClass.sqlin ((long *) &aufp.kun, SQLLONG, 0 ) ;
	dbClass.sqlin ((double *) &aufp.a_ers, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((char *) aufp.ls_charge, SQLCHAR ,31 ) ;

	dbClass.sqlin ((short *) &aufp.aufschlag, SQLSHORT, 0 ) ;
	dbClass.sqlin ((double *) &aufp.aufschlag_wert, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((short *) &aufp.pos_txt_kz, SQLSHORT, 0 ) ;


	ins_cursor = (short) dbClass.sqlcursor ("insert into aufp ( "
	  " mdn, fil, auf, posi, aufp_txt "
	" , a, auf_me, auf_me_bz, lief_me, lief_me_bz "
	" , auf_vk_pr, auf_lad_pr, delstatus, sa_kz_sint, prov_satz "
	" , ksys, pid, auf_klst, teil_smt, dr_folge "
	" , inh, auf_vk_euro, auf_vk_fremd, auf_lad_euro, auf_lad_fremd "
	" , rab_satz, me_einh_kun, me_einh, me_einh_kun1, auf_me1 "
	" , inh1, me_einh_kun2, auf_me2, inh2, me_einh_kun3 "
	" , auf_me3, inh3, gruppe, kond_art, a_grund "
	" , ls_pos_kz, posi_ext, kun, a_ers, ls_charge "
	" , aufschlag, aufschlag_wert, pos_txt_kz "
	" ) values ( "
	  " ?, ?, ?, ?, ? "
	" , ?, ?, ?, ?, ? "
	" , ?, ?, ?, ?, ? "
	" , ?, ?, ?, ?, ? "
	" , ?, ?, ?, ?, ? "
	" , ?, ?, ?, ?, ? "
	" , ?, ?, ?, ?, ? "
	" , ?, ?, ?, ?, ? "
	" , ?, ?, ?, ?, ? "
	" , ?, ?, ? "
	" ) " ) ;

}

