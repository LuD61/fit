#include "stdafx.h"
#include "DbClass.h"
#include "sys_par.h"

struct SYS_PAR sys_par, sys_par_null;

extern DB_CLASS dbClass;

SYS_PAR_CLASS sys_par_class ;

void SYS_PAR_CLASS::prepare (void)
{
      dbClass.sqlin ((char *) sys_par.sys_par_nam, 0, 19);

    dbClass.sqlout ((char *) sys_par.sys_par_nam,SQLCHAR,19);
    dbClass.sqlout ((char *) sys_par.sys_par_wrt,SQLCHAR,2);
    dbClass.sqlout ((char *) sys_par.sys_par_besch,SQLCHAR,33);
    dbClass.sqlout ((long *) &sys_par.zei,SQLLONG,0);
    dbClass.sqlout ((char *) &sys_par.delstatus,SQLSHORT,0);

      cursor = dbClass.sqlcursor ("select sys_par.sys_par_nam,  "
"sys_par.sys_par_wrt,  sys_par.sys_par_besch,  sys_par.zei,  "
"sys_par.delstatus from sys_par "

                            "where sys_par_nam = ?");
}

int SYS_PAR_CLASS::sys_par_holen ( char * sys_par_nam , char * sys_par_wrt, char * sys_par_besch )
{
	// Achtung : die Zielfelder muessen die Mindestgroesse der Daba-Felder besitzen : sonst Laufzeit-fehler-problematik !! 
	if (cursor < 0 )
    {
		prepare ();
    }
	sprintf ( sys_par.sys_par_nam , "%s" , sys_par_nam ) ;
	int i = dbClass.sqlopen (cursor);
    if (i) return 0 ;
	i = dbClass.sqlfetch (cursor) ;
	if ( i )
	{
		sprintf ( sys_par_wrt, "0" ) ;
		sprintf ( sys_par_besch, "" ) ;
		return 0 ;
	}
	sprintf ( sys_par_wrt, "%s" , sys_par.sys_par_wrt ) ;
	sprintf ( sys_par_besch, "%s" , sys_par.sys_par_besch) ;
	return 0 ;
  
}

int SYS_PAR_CLASS::dbreadfirst (void)
{
      if (cursor == -1)
      {
                 prepare ();
      }
      int i = dbClass.sqlopen (cursor);
      if (i) return i;
	  return (dbClass.sqlfetch (cursor));
}

int SYS_PAR_CLASS::dbread (void)
{
      return (dbClass.sqlfetch (cursor));
}
  