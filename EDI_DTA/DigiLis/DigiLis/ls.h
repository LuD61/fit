#ifndef _LS_DEF
#define _LS_DEF

struct LSK
{
long	ls;
long	adr;
short	mdn ;
long	auf;
short   kun_fil;
long    kun ;
short   fil ;
char    feld_bz1[20] ;
TIMESTAMP_STRUCT lieferdat;
char    lieferzeit [6] ;
char    hinweis [49] ;
short   ls_stat;
char    kun_krz1 [17] ;
double  auf_sum ;
char    feld_bz2 [12] ;
double  lim_er ;
char    partner [37] ;
long	pr_lst ;
char    feld_bz3 [8] ;
short   pr_stu ;
long    vertr ;
long    tou ;
char    adr_nam1 [37] ;
char    adr_nam2 [37] ;
char    pf [17] ;
char    str [37] ;
char    plz [9] ;
char    ort1 [37] ;
double  of_po ;
short	delstatus ;
long    rech ;
char    blg_typ [2] ;
double	zeit_dec ;
long    kopf_txt ;
long    fuss_txt ;
long    inka_nr ;
char    auf_ext[20] ;
long    teil_smt ;
char    pers_nam [9];
double    brutto ;
TIMESTAMP_STRUCT komm_dat ;
double  of_ek ;
double	of_po_euro ;
double  of_po_fremd ;
double  of_ek_euro ;
double  of_ek_fremd ;
short	waehrung ;
char    ueb_kz [3];
double  gew ;
short   auf_art ;
short   fak_typ ;
short   ccmarkt ;
long    gruppe ;
long    tou_nr ;
short   wieg_kompl ;
TIMESTAMP_STRUCT best_dat ;
char    hinweis2 [32] ;
char    hinweis3 [32] ;
TIMESTAMP_STRUCT fix_dat ;
char    komm_name [13] ;
short   psteuer_kz ;

TIMESTAMP_STRUCT vondate ;
TIMESTAMP_STRUCT bisdate ;
};

extern struct LSK lsk, lsk_null;

extern class LSK_CLASS lsk_class ;


class LSK_CLASS : public DB_CLASS
{
       private :
               void prepare (char *);
       public :
               int openlsk (char *);
               int leselsk (void);

			   LSK_CLASS () : DB_CLASS ()
               {
				   readcursor = -1 ;
               }
};



#endif
