// DigiLisDlg.h : Headerdatei
//

#pragma once
#include "afxwin.h"


// CDigiLisDlg-Dialogfeld
class CDigiLisDlg : public CDialog
{
// Konstruktion
public:
	CDigiLisDlg(CWnd* pParent = NULL);	// Standardkonstruktor

// Dialogfelddaten
	enum { IDD = IDD_DIGILIS_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV-Unterst�tzung


// Implementierung
protected:
	HICON m_hIcon;

	// Generierte Funktionen f�r die Meldungstabellen
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CEdit m_mdn;
	int v_mdn;
	CEdit m_mdnname;
	CString v_mdnname;
	CEdit m_lidat;
	CString v_lidat;
	CEdit m_tour;
	CString v_tour;
	afx_msg void OnEnKillfocusMdn();
	afx_msg void OnEnKillfocusLidat();
	afx_msg void OnEnKillfocusTour();
	afx_msg BOOL PreTranslateMessage(LPMSG) ;
	afx_msg BOOL NoListCtrl (CWnd *) ;
	void CDigiLisDlg::systemdatum ( char * );



	afx_msg void OnBnClickedOk();
};
