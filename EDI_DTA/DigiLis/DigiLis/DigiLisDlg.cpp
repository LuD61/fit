// DigiLisDlg.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "DigiLis.h"
#include "DigiLisDlg.h"
#include "dbClass.h"
#include "mdn.h"
#include "adr.h"
#include "ls.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif



static char tourbeding[512];

static char CDIGILISDIR[256];
static char CALTERNATEDIR[256];
static char CDIGILISAKUNDE[256];
static char CDIGILISNSTART[256];

// CAboutDlg-Dialogfeld f�r Anwendungsbefehl "Info"

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialogfelddaten
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterst�tzung

// Implementierung
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


DWORD ProcWaitExec (LPSTR prog, WORD SHOW_MODE, int x, int y, int cx, int cy)
{
        STARTUPINFO sti;
        PROCESS_INFORMATION pi;
        DWORD ExitCode; 
        int ret;

        ZeroMemory (&sti, sizeof (sti));
        sti.cb = sizeof (sti);
        if (x != -1)
        {
                     sti.dwFlags |= STARTF_USEPOSITION;
                     sti.dwX = x;
                     sti.dwY = y;
        }

        if (cx != -1)
        {
                     sti.dwFlags |= STARTF_USESIZE;
                     sti.dwXSize = cx;
                     sti.dwYSize = cy;
        }
        sti.wShowWindow = SHOW_MODE;
        sti.dwFlags |= STARTF_USESHOWWINDOW;

        ret = CreateProcess (NULL,
                       prog,
                       NULL,
                       NULL,
                      (int) NULL,
                      (unsigned long) NULL,
                       NULL,
                       NULL,
                       &sti,
                       &pi);
        if (ret)
        {
                  CloseHandle (pi.hThread);
                  WaitForSingleObject (pi.hProcess, INFINITE);
        }
		else
        {
			      return -1;
        }				  
		GetExitCodeProcess (pi.hProcess, &ExitCode);
		while (ExitCode == STILL_ACTIVE)
		{
                  GetExitCodeProcess (pi.hProcess, &ExitCode);
		}
        CloseHandle (pi.hProcess);
        return ExitCode;
}



static char *wort[100];	// mehr als 100 Worte braucht es nicht ?!


char *clipped (char *string)
{
 char *clstring;
 short i,len;

 len = (short) strlen (string);

 if (len == 0) return (string);
 len --;
 clstring = string;
 // : damit werden auch Leerstrings korrekt gekappt 
 for (i = len; i >= 0; i --)
 {
  if ((unsigned char) clstring[i] > 0x20)
  {
   break;
  }
 }
 clstring [i + 1] = 0;

 clstring = string;
 /* -----> links-clippen lasse ich mal weg 
 len = (short) strlen (clstring);

 for (i = 0; i < len; i ++, clstring +=1)
 {
  if ((unsigned char) *clstring > (unsigned char) 0X20)
  {
   break;
  }
 }
 < ----- */
 return (clstring);
}

char *uPPER (char *string)
{

   for( char *p = string; p < string + strlen( string ); p++ )
   {
      if( islower( *p ) )
			( p[0] = _toupper( *p ) );
   }
   return string ;
}


// CR am Stringende etfernen.

void cr_weg (char *string)
{
 for (; *string; string += 1)
 {
  if (*string == (char) 13)
    break;
  if (*string == (char) 10)
    break;
 }
 *string = 0;
 return;
}

int strupcmpi (char *str1, char *str2, int len, int len2)
{
 short i;
 char upstr1;
 char upstr2;

 if ( len < len2 ) return -1 ;
 if ( len > len2 ) return  1 ;
 
 for (i = 0; i < len; i ++, str1 ++, str2 ++)
 {
  if (*str1 == 0)
    return (-1);
  if (*str2 == 0)
    return (1);
  upstr1 = toupper((int) *str1);
  upstr2 = toupper((int) *str2);
  if (upstr1 < upstr2)
  {
   return(-1);
  }
  else if (upstr1 > upstr2)
  {
   return (1);
  }
 }
 return (0);
}

static char buffer[1024] ;

static char buffere[1024] ;

int next_char_ci (char *string, char tzeichen, int i)
// Naechstes Zeichen != Trennzeichen suchen.

{
       for (;string [i]; i ++)
       {
               if (string[i] != tzeichen)
               {
                                   return (i);
               }
       }
       return (i);
}

// der 2. Parameter ist dann ein endloser String bis zum #-Trenner oder CR/LF
short splite (char *string)
{

 short wz;                  // Wortzaehler  
 int i, j, len;
 static char zeichen = ' ';
 wz = j = 0;
 len = (int) strlen (string);
 wz = 1;
 i = next_char_ci (string, zeichen, 0);
 if (i >= len) return (0);
 wort [wz] = buffer;
 wz ++;
 for (; i < len; i ++, j ++)
 {
        if (string [i] == zeichen)
        {
                    i = next_char_ci (string, zeichen, i);
                    if (i >= len) break;
                    buffer [j] = (char) 0;
                    j ++;
                    wort [wz] = &buffer [j];
                    wz ++;
 					zeichen ='#' ;
       }
        buffer [j] = string [i];
  }
  buffer [j] = (char) 0;
  return (wz - 1);
}
short splitedoll (char *string)
{

 short wz;                  // Wortzaehler  
 int i, j, len;
 static char zeichen = '$';
 wz = j = 0;
 len = (int) strlen (string);
 wz = 1;
 i = next_char_ci (string, zeichen, 0);
 if (i >= len) return (0);
 wort [wz] = buffer;
 wz ++;
 for (; i < len; i ++, j ++)
 {
        if (string [i] == zeichen)
        {
                    i = next_char_ci (string, zeichen, i);
                    if (i >= len) break;
                    buffer [j] = (char) 0;
                    j ++;
                    wort [wz] = &buffer [j];
                    wz ++;
 					zeichen ='#' ;
       }
        buffer [j] = string [i];
  }
  buffer [j] = (char) 0;
  return (wz - 1);
}

short split (char *string)
{

 short wz;                  // Wortzaehler  
 int i, j, len;
 static char zeichen = ' ';
 wz = j = 0;
 len = (int) strlen (string);
 wz = 1;
 i = next_char_ci (string, zeichen, 0);
 if (i >= len) return (0);
 wort [wz] = buffer;
 wz ++;
 for (; i < len; i ++, j ++)
 {
        if (string [i] == zeichen)
        {
                    i = next_char_ci (string, zeichen, i);
                    if (i >= len) break;
                    buffer [j] = (char) 0;
                    j ++;
                    wort [wz] = &buffer [j];
                    wz ++;
        }
        buffer [j] = string [i];
  }
  buffer [j] = (char) 0;
  return (wz - 1);
}

short splitdoll (char *string)
{

 short wz;                  // Wortzaehler  
 int i, j, len;
 static char zeichen = '$';
 wz = j = 0;
 len = (int) strlen (string);
 wz = 1;
 i = next_char_ci (string, zeichen, 0);
 if (i >= len) return (0);
 wort [wz] = buffer;
 wz ++;
 for (; i < len; i ++, j ++)
 {
        if (string [i] == zeichen)
        {
                    i = next_char_ci (string, zeichen, i);
                    if (i >= len) break;
                    buffer [j] = (char) 0;
                    j ++;
                    wort [wz] = &buffer [j];
                    wz ++;
        }
        buffer [j] = string [i];
  }
  buffer [j] = (char) 0;
  return (wz - 1);
}




void holeparamsausdatei(void)
{
int anz;
char buffer [512];
char token[64] ;
int gefunden ;
gefunden = 0 ;


	sprintf ( buffer,"%s\\digilis.cfg",getenv("BWSETC"));

FILE *fp;

	fp = fopen (buffer, "r");
    if (fp == (FILE *)NULL) return; // eigentlich unm�glich : FALSE;
    while (fgets (buffer, 511, fp))
    {
		cr_weg (buffer);
        anz = splitdoll (buffer);
        if (anz < 2) continue;

		// funktioniert vorerst NICHT mit space-Namen
		sprintf ( token, "ALTERNATEDIR" ); 
        if (strupcmpi (wort[1], token, (int)strlen (token),
							(int)strlen(clipped(wort[1]))) == 0)
		{
			anz = splitedoll(buffer) ;
			strcpy (CALTERNATEDIR, clipped (wort [2]));
			continue ;
		}

		sprintf ( token, "DIGILISDIR" ); 
       if (strupcmpi (wort[1], token, (int)strlen (token),
							(int)strlen(clipped(wort[1]))) == 0)
        {
			anz = splitedoll(buffer) ;
			strcpy (CDIGILISDIR, clipped (wort [2]));
			continue ;
		}
	   sprintf ( token, "DIGILISAKUNDE" ); 
       if (strupcmpi (wort[1], token, (int)strlen (token),
							(int)strlen(clipped(wort[1]))) == 0)
        {
			anz = splitedoll(buffer) ;
			strcpy (CDIGILISAKUNDE, clipped (wort [2]));
			continue ;
		}
	   sprintf ( token, "DIGILISNSTART" ); 
       if (strupcmpi (wort[1], token, (int)strlen (token),
							(int)strlen(clipped(wort[1]))) == 0)
        {
			anz = splitedoll(buffer) ;
			strcpy (CDIGILISNSTART, clipped (wort [2]));
			continue ;
		}
	}
	fclose (fp);
}

bool DateiSetzen(void) 
{

//			MessageBox("Datei drllls.cfgorig oder digilis.cfg fehlt !", " ", MB_OK|MB_ICONSTOP);
	
	FILE * fp ;
	char slls [260] ;
	char sdig [260] ;
	char s2[260] ;
	sprintf ( s2, "%s", getenv ( "BWSETC" ));
	sprintf ( slls, "%s\\drllls.cfgorig",s2 );
	fp = fopen ( slls , "r" ) ;
	if ( fp == NULL )
		return FALSE ;

	fclose(fp) ;
	sprintf ( sdig, "%s\\digilis.cfg",s2 );
	fp = fopen ( sdig , "r" ) ;
	if ( fp == NULL )
		return FALSE ;
	fclose(fp) ;
	sprintf ( slls, "%s\\drllls.cfg",s2 );
	CopyFile( sdig,slls, FALSE  );	// �berpinselt forciert

	holeparamsausdatei();


	return TRUE ;
}

/* -------> MUSTEREINTRAG digilis.cfg
rem zur Aktivierung die kommentare entfernen ;-)

CARAT$1
rem ALTERNATEDIR ist nur Zwischenverzeichnis
ALTERNATETYP$PDF
ALTERNATEBUCHST$0
ALTERNATEMDN$1

rem bitte alle Namen ohne blank
ALTERNATEDIR$c:\user\fit\tmp

DIGILISDIR$c:\user\fit\nagel\pdf\send

DIGILISAKUNDE$BOESINGER
DIGILISNSTART$D78
rem anzahlerlaubt gestattet Auswertung Anzahl Copies
anzahlerlaubt$1
zerlegelfd$0

-------- */

/* ------> Muster fuer die datei digilisbat.bat
rem digilisbat.bat
rem 
rem FTP zu Nagel z.B. Boesinger 
rem 14:14 23.01.2013
rem G. Koenig / J.Greiner


set SEND=c:\user\fit\Nagel\pdf\send
set FTPCMDFILE=c:\user\fit\Nagel\pdf\nagelftp.ftp

set NAGELHOST=ftp.kv-nagel.com
set NAGELUSER=BOESINGER-NICHT
set NAGELPASS=7y13Kv

REM alle account-infos aus einem Schreiben vom 23.12.2012

c:

cd %SEND%
rem del send_ok
rem touch send_ftp

echo OPEN %NAGELHOST%	> %FTPCMDFILE%
echo USER %NAGELUSER%	>> %FTPCMDFILE%
echo %NAGELPASS%	>> %FTPCMDFILE%
echo CD pdf	>> %FTPCMDFILE%
echo CD in	>> %FTPCMDFILE%
echo BINARY	>> %FTPCMDFILE%

echo PUT %1	>> %FTPCMDFILE%

rem echo MPUT aufp*      	>> %FTPCMDFILE%
rem echo PUT send_ftp send_ok	>> %FTPCMDFILE%
rem echo GET send_ok	>> %FTPCMDFILE%
echo rename %1 %2	>> %FTPCMDFILE%  
echo BYE		>> %FTPCMDFILE%

ftp -i -n -s:%FTPCMDFILE% > %SEND%\prot 

if errorlevel 1 goto error_ftp
goto ende

:ende
echo FTP: OK.
exit 0


:error_ftp
echo FTP: ERROR.
exit 1


--------- */


void BelegErstellen( short imdn, long ilsnr )
{
	char kommando[400];
	char kommando2[400];
	char rosipath[400]; 
	char globnutzer[25];
	char zeitstempel[30];

	sprintf ( globnutzer,"fit" );

	sprintf ( rosipath,"%s", getenv( "RSDIR"));

	sprintf ( kommando, "%s\\bin\\rswrun beldr N %d 0 %d %s D" , rosipath, imdn, ilsnr , globnutzer ) ;

	int ex_code = ProcWaitExec( (char *) kommando , SW_SHOW, -1, 0, -1, 0 );
	
	sprintf ( rosipath,"%s\\l%d%08d.PDF", CALTERNATEDIR, imdn, ilsnr) ;	// Mandantennummer ist immer mit dabei ....

	// detaillierte Beschreibung siehe Nagel-Dokumentation 
	// NG#ANAGEL-Kunde #L4711#C4#ND90#Z20100529123145.PDF

	time_t timer;
	struct tm *ltime;
	time (&timer);
 	ltime = localtime (&timer);
	sprintf ( zeitstempel , "%04d%02d%02d%02d%02d%02d",
			ltime->tm_year + 1900, ltime->tm_mon + 1 , ltime->tm_mday
			,ltime->tm_hour, ltime->tm_min, ltime->tm_sec );

	sprintf ( kommando ,"%s\\NG#A%s#L%d%08d#C1#N%s#Z%s.KND", CDIGILISDIR, CDIGILISAKUNDE, imdn, ilsnr, CDIGILISNSTART, zeitstempel ) ;
	
// ich baue mal fix Auflage == 1 ein, kann ja sp�ter angepasst werden 
// #N ( Abgangsrelation ) lasse ich fuer Test erst mal fehlen
// #R ( zustell-Relation) lasse ich fuer test erst mal fehlen

	int retcode = rename ( rosipath, kommando ) ;

// eigentlich derselbe Name mit anderer Extension

	sprintf ( kommando2,"NG#A%s#L%d%08d#C1#N%s#Z%s.PDF", CDIGILISAKUNDE, imdn, ilsnr, CDIGILISNSTART, zeitstempel ) ;
	sprintf ( kommando ,"digilisbat.bat NG#A%s#L%d%08d#C1#N%s#Z%s.KND %s", CDIGILISAKUNDE, imdn, ilsnr, CDIGILISNSTART, zeitstempel, kommando2 ) ;

	ex_code = ProcWaitExec( (char *) kommando , SW_SHOW, -1, 0, -1, 0 );

}

void DateiZurueckSetzen(void) 
{
// Es sit alles prima gelaufen, bitte wieder aufraeumen
	
	char slls [260] ;
	char sdig [260] ;
	char s2[260] ;
	sprintf ( s2, "%s", getenv ( "BWSETC" ));
	sprintf ( sdig, "%s\\drllls.cfgorig",s2 );

	sprintf ( slls, "%s\\drllls.cfg",s2 );
	CopyFile( sdig,slls, FALSE  );	// �berpinselt forciert
}





BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CDigiLisDlg-Dialogfeld




CDigiLisDlg::CDigiLisDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDigiLisDlg::IDD, pParent)
	, v_mdn(0)
	, v_mdnname(_T(""))
	, v_lidat(_T(""))
	, v_tour(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CDigiLisDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_MDN, m_mdn);
	DDX_Text(pDX, IDC_MDN, v_mdn);
	DDV_MinMaxInt(pDX, v_mdn, 0, 9999);
	DDX_Control(pDX, IDC_MDNNAME, m_mdnname);
	DDX_Text(pDX, IDC_MDNNAME, v_mdnname);
	DDX_Control(pDX, IDC_LIDAT, m_lidat);
	DDX_Text(pDX, IDC_LIDAT, v_lidat);
	DDX_Control(pDX, IDC_TOUR, m_tour);
	DDX_Text(pDX, IDC_TOUR, v_tour);
}

BEGIN_MESSAGE_MAP(CDigiLisDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_EN_KILLFOCUS(IDC_MDN, &CDigiLisDlg::OnEnKillfocusMdn)
	ON_EN_KILLFOCUS(IDC_LIDAT, &CDigiLisDlg::OnEnKillfocusLidat)
	ON_EN_KILLFOCUS(IDC_TOUR, &CDigiLisDlg::OnEnKillfocusTour)
	ON_BN_CLICKED(IDOK, &CDigiLisDlg::OnBnClickedOk)
END_MESSAGE_MAP()




static char bufh[512] ;

BOOL datumcheck(char * bufi )
{
	int i = (int) strlen(bufi) ;
	int hil1, hil2 ;
	BOOL j = FALSE ;

	char hil5 [5] ;
	if ( i != 6 && i != 8 && i != 10 )
	{
		bufi[0] = '\0' ;
		return TRUE ;
	}
	if ( i == 6 )	// nur ddmmyy erlaubt 
	{
		bufi[10] = '\0' ;
		bufi[ 9] = bufi[5] ;
		bufi[ 8] = bufi[4] ;
		if ( bufi[ 8] > '5' )
		{
			bufi[ 6] = '1' ;
			bufi[ 7] = '9' ;
		}
		else
		{
			bufi[ 6] = '2' ;
			bufi[ 7] = '0' ;
		}
		bufi[ 5] = '.' ;
		bufi[ 4] = bufi[3] ;
		bufi[ 3] = bufi[2] ;
		bufi[ 2] = '.' ;
		i = 10 ;
		j = TRUE ; 
	}
	if ( i == 8 )	// nur dd,mm,yy erlaubt 
	{
		bufi[10] = '\0' ;
		bufi[ 9] = bufi[7] ;
		bufi[ 8] = bufi[6] ;
		if ( bufi[ 8] > '5' )
		{
			bufi[ 6] = '1' ;
			bufi[ 7] = '9' ;
		}
		else
		{
			bufi[ 6] = '2' ;
			bufi[ 7] = '0' ;
		}
		bufi[ 5] = '.' ;
		bufi[ 2] = '.' ;
		i = 10 ;
		j = TRUE ;
	}
	if ( i == 10 )	// hier kommt nix anderes mehr an ....  
	{
		if (   bufi[0] > '9' || bufi[0] < '0' 
			|| bufi[1] > '9' || bufi[1] < '0'
			|| bufi[3] > '9' || bufi[3] < '0'
			|| bufi[4] > '9' || bufi[4] < '0'
			|| bufi[6] > '9' || bufi[6] < '0'
			|| bufi[7] > '9' || bufi[7] < '0'
			|| bufi[8] > '9' || bufi[8] < '0'
			|| bufi[9] > '9' || bufi[9] < '0'
			)
		{

			bufi[0] = '\0' ;
			return TRUE ;
		}

		hil5[0] = bufi[3] ;
		hil5[1] = bufi[4] ;
		hil5[2] = '\0' ;
		hil1 = atoi (hil5);
		if ( hil1 > 12 || hil1 < 1 )
		{
			bufi[0] = '\0' ;
			return TRUE ;
		}

		hil5[0] = bufi[0] ;
		hil5[1] = bufi[1] ;
		hil5[2] = '\0' ;
		hil2 = atoi (hil5);
		if ( hil2 > 31 || hil2 < 1 )		// 260110 hil1->hil2
		{
			bufi[0] = '\0' ;
			return TRUE ;
		}
		switch ( hil1 )
		{
		case  4 :
		case  6 :
		case  9 :
		case 11 :
			if ( hil2 > 30 )
			{
				bufi[0] = '\0' ;
				return TRUE ;
			}
			break ;
		case 2 :

			hil5[0] = bufi[6] ;
			hil5[1] = bufi[7] ;
			hil5[2] = bufi[8] ;
			hil5[3] = bufi[9] ;
			hil5[4] = '\0' ;
			hil1    = atoi(hil5) ;
			if ( hil2 > 28 )	// IM Jahr 2000 geht dieser Check schief .....
			{
				if ( ! ( hil1 % 4) )	// 25.02.08 : hier stand hil2 und dann hats nicht gefunzt
				{
					if ( hil2 > 29 )
					{
						bufi[0] = '\0' ;
						return TRUE ;
					}
				}
				else
				{
					bufi[0] = '\0' ;
					return TRUE ;
				};
			}
			break ;

		}

	}
	return j ;

}


void CDigiLisDlg::systemdatum ( char *  ziel )
{
// Fuer Platz im "Ziel" ist jeder selber zust�ndig

time_t timer;
struct tm *ltime;

int jrhstart = 70 ;
int jrh1 = 1900 ;
int jrh2 = 2000 ;

 time (&timer);
 ltime = localtime (&timer);

 if (ltime->tm_year > 99 && ltime->tm_year < 200)
 {
	ltime->tm_year -=  100 ;
 }

 if (ltime->tm_year < 100)
 {
             if (ltime->tm_year < jrhstart)
             {
                  ltime->tm_year += jrh2;
             }
             else
             {
                  ltime->tm_year += jrh1;
             }
 }
 sprintf (ziel, "%02d.%02d.%04d", ltime->tm_mday,
                                   ltime->tm_mon + 1,
                                   ltime->tm_year);
}


// CDigiLisDlg-Meldungshandler

BOOL CDigiLisDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Hinzuf�gen des Men�befehls "Info..." zum Systemmen�.

	// IDM_ABOUTBOX muss sich im Bereich der Systembefehle befinden.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Symbol f�r dieses Dialogfeld festlegen. Wird automatisch erledigt
	//  wenn das Hauptfenster der Anwendung kein Dialogfeld ist
	SetIcon(m_hIcon, TRUE);			// Gro�es Symbol verwenden
	SetIcon(m_hIcon, FALSE);		// Kleines Symbol verwenden

	// TODO: Hier zus�tzliche Initialisierung einf�gen

	mdn.mdn = 1 ;
	v_mdn = 1 ;
	mdn_class.openmdn();
	if (! mdn_class.lesemdn())
	{
		adr.adr = mdn.adr ;
		int i = adr_class.openadr () ;
		i = adr_class.leseadr () ;
		if (!i) 
		{
			v_mdnname.Format("%s",_T(adr.adr_krz));
		}
		else
		{
			v_mdnname.Format("MDN NICHT VORHANDEN");

		}
	}
	else	// fehlerhafte Eingabe
	{
		v_mdnname.Format("MDN NICHT VORHANDEN");
		
	}

	char hilfe[20] ;
	systemdatum ( hilfe) ; 
	v_lidat = _T(hilfe);

	UpdateData(FALSE) ;


	return TRUE;  // Geben Sie TRUE zur�ck, au�er ein Steuerelement soll den Fokus erhalten
}

void CDigiLisDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// Wenn Sie dem Dialogfeld eine Schaltfl�che "Minimieren" hinzuf�gen, ben�tigen Sie 
//  den nachstehenden Code, um das Symbol zu zeichnen. F�r MFC-Anwendungen, die das 
//  Dokument/Ansicht-Modell verwenden, wird dies automatisch ausgef�hrt.

void CDigiLisDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Ger�tekontext zum Zeichnen

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Symbol in Clientrechteck zentrieren
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Symbol zeichnen
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// Die System ruft diese Funktion auf, um den Cursor abzufragen, der angezeigt wird, w�hrend der Benutzer
//  das minimierte Fenster mit der Maus zieht.
HCURSOR CDigiLisDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CDigiLisDlg::OnEnKillfocusMdn()
{

	UpdateData (TRUE) ;

	int i = m_mdn.GetLine(0,bufh,500);
	bufh[i] = '\0' ;
	if (i)	mdn.mdn = (short) atoi ( bufh );
	else mdn.mdn = -2 ;
	mdn_class.openmdn();
	if (! mdn_class.lesemdn())
	{
		adr.adr = mdn.adr ;
		i = adr_class.openadr () ;
		i = adr_class.leseadr () ;
		if (!i) 
		{
			v_mdnname.Format("%s",_T(adr.adr_krz));
		}
		else
		{
			v_mdnname.Format("MDN NICHT VORHANDEN");

		}
	}
	else	// fehlerhafte Eingabe
	{
		v_mdnname.Format("MDN NICHT VORHANDEN");
		
	}
		UpdateData (FALSE) ;
}

void CDigiLisDlg::OnEnKillfocusLidat()
{

	UpdateData(TRUE);
	int i = m_lidat.GetLine(0,bufh,500) ;
	bufh[i] = '\0' ;
	if (i)
	{
		if (datumcheck(bufh))
		{
			v_lidat.Format("%s",_T(bufh));
			UpdateData (FALSE) ;
		}
	}
}

void CDigiLisDlg::OnEnKillfocusTour()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
}


// 080408 A
BOOL CDigiLisDlg::PreTranslateMessage(LPMSG lpMsg)
{
	CWnd *cWnd;
	 if (lpMsg->message == WM_KEYDOWN)
	 {
		 switch (lpMsg->wParam)
			 {
		 
		        case VK_RETURN :
					cWnd = GetFocus ();
			           if(cWnd == GetDlgItem (IDOK))
							return CDialog::PreTranslateMessage(lpMsg);
			           if(cWnd == GetDlgItem (IDCANCEL))
					   {
							OnCancel ();
 							return TRUE;
					   }

/* --->
			           if(cWnd == GetDlgItem (IDC_KUNN))
					   {	Read (); return TRUE; }
                       else if (cWnd == GetDlgItem (IDC_MDN))
                       { if (ReadMdn () == FALSE)
                       { return TRUE;
                       } }
 < ----- */
			   NextDlgCtrl();
						return TRUE;

			 case VK_F5 :
                     OnCancel ();
 			         return TRUE;
/* ----->
			 case VK_F7 :
                     OnDelete ();
 			         return TRUE;
             case VK_F9 :
		             if (OnF9 ())
                     {
                         return TRUE;
                     }
                     break;
		     case VK_F6 :
                     OnFree ();
				     return TRUE;
 <----- */

/* ---->
			 case VK_F12 :
					UpdateData( TRUE ) ;
					ladevartodaba();
					if (  tou_class.testupdtou() )
						tou_class.inserttou () ;
					else
						tou_class.updatetou () ;

					v_edithtour = 0 ;
					tou.tou = 0 ;
					UpdateData(FALSE) ;
					m_edithtour.EnableWindow (TRUE) ;
					m_buttonhtour.EnableWindow ( TRUE) ;
					m_edithtour.ModifyStyle (0, WS_TABSTOP,0) ;
					m_buttonhtour.ModifyStyle ( 0, WS_TABSTOP,0) ;
					m_edithtour.SetFocus () ;	


//                     OnOK ();
				     return TRUE;
< ----- */


 		    case VK_DOWN :
                     if (NoListCtrl (GetFocus ()))
                     {
						NextDlgCtrl();
						return TRUE;
					
                     }
				     break;
 		    case VK_UP :
                     if (NoListCtrl (GetFocus ()))
                     {
						PrevDlgCtrl();
	 					cWnd = GetFocus ();
					 	GetNextDlgTabItem(cWnd ,FALSE) ;
						return TRUE;
                     }
				     break;
		 }
	 }

return CDialog::PreTranslateMessage(lpMsg);
}

BOOL CDigiLisDlg::NoListCtrl (CWnd *cWnd)
{
    if (((CComboBox *) cWnd->GetParent ())->GetCount () > 1)
    {
        return FALSE;
    }
    else if (((CComboBox *) cWnd)->GetCount () > 1)
    {
        return FALSE;
    }
    else if (((CListBox *) cWnd)->GetCount () > 1)
    {
        return FALSE;
    }
    return TRUE;
}





void CDigiLisDlg::OnBnClickedOk()
{


	int	i = m_lidat.GetLine(0,bufh,500) ;
	bufh[i] = '\0' ;
	if ( i != 10 )
	{
			MessageBox("Ung�ltige Datumseingabe !", " ", MB_OK|MB_ICONSTOP);
			return ;
	}
	sqldatdst(  & lsk.lieferdat , v_lidat.GetBuffer()) ;

	lsk.mdn = mdn.mdn ;
	lsk.fil = 0 ;

	int igef , mufo ,iok ;	// es Muss was sinnvolles eingegeben werden, sonst wird gemeckert

	// igef : irgendeine Zahl gefunden 
	// mufo == 0 -> es darf space oder nummer folgen ( vorblanks )
	// mufo == 3 -> es darf space oder nummer folgen (nachkomma) 
	// mufo == 1 -> es darf nummer, space oder komma folgen ( im wert )
	// mufo == 2 -> es darf space oder komma folgen ( nach wert , vor folgekomma )
	//  iok muss == 1 sein und igef muss > 0 und mufo darf nicht == 3 sein 


	i = m_tour.GetLine(0,bufh,500) ;
	bufh[i] = '\0' ;
	igef =  mufo = 0 ;
	iok = 1 ;

	for ( int j = 0 ; j < i ; j++ )
	{
		if (( bufh[j] > '9' ) || ( bufh[j] < '0' ))
		{
	// mufo == 0 -> es darf space oder nummer folgen ( vorblanks )
	// mufo == 3 -> es darf space oder nummer folgen (nachkomma) 
	// mufo == 1 -> es darf nummer, space oder komma folgen ( im wert )
	// mufo == 2 -> es darf space oder komma folgen ( nach wert , vor folgekomma )
			if ( bufh[j] == ' ')
			{
				if ( mufo == 1 )	// Zahl zuende
				{
					mufo = 2 ;
					continue ;
				}
				else
				{
					continue ;	// alle anderen Space-Zustaende
				}
			};				
			if ( bufh[j] == ',')
			{
	// mufo == 0 -> es darf space oder nummer folgen ( vorblanks )
	// mufo == 1 -> es darf nummer, space oder komma folgen ( im wert )
	// mufo == 2 -> es darf space oder komma folgen ( nach wert , vor folgekomma )
	// mufo == 3 -> es darf space oder nummer folgen (nachkomma) 

				if ( mufo == 0 )	// Komma vor erster zahl
				{
					iok = 0 ;
					break ;
				}
				if ( mufo == 1 )	// Komma am Zahlenende
				{
					mufo = 3 ;
					continue ;
				}
				if ( mufo == 2 )	// gueltiges komma nach space 
				{
					mufo = 3 ;
					continue ;
				}
				if ( mufo == 3 )	// Doppelkomma 
				{
					iok = 0 ;
					break ;
				}
			};
			iok = 0 ;
			break ;	// andere Zeichen
		}
		else	// nummer 
		{
			if ( mufo == 0 )	// vorblanks sind vorbei
			{
				mufo = 1 ;
				igef = 1 ;
				 continue ;
			}
			if ( mufo == 1 )	// mitten in einer Zahl
			{
				 continue ;
			}

			if ( mufo == 2 )	// space zwischen 2 Zahlen -> Abbruch
			{
				iok = 0 ;
				 break ;
			}
			if ( mufo == 3 )	// neue Zahl beginnt
			{
				mufo = 1 ;
				 continue ;
			}
		}

	}
	if ( ! iok || ! igef  || mufo == 3 )
	{
			MessageBox("Ung�ltige Toureingabe !", " ", MB_OK|MB_ICONSTOP);
			return ;
	}
	else
		sprintf ( tourbeding , "and lsk.tou_nr in (%s)" ,bufh ) ;	// beachte Unterschied zwischen tou und tou_nr !!!!!!!!!


	int dretlsk ;

	dretlsk = lsk_class.openlsk(tourbeding ) ;
	long lsmatrix[390] ;
	int lsmatpoint = 0 ;

	if ( !dretlsk )
		dretlsk =lsk_class.leselsk() ;
	while ( !dretlsk  && lsmatpoint < 385 )
	{
		if ( lsk.ls > 0 )
		{
			lsmatrix[lsmatpoint] = lsk.ls ;
			lsmatpoint ++ ;
			lsmatrix[lsmatpoint] = 0 ;
		}
		dretlsk =lsk_class.leselsk() ;
	}

	if ( lsmatpoint == 0  )
	{
			MessageBox("Keine Daten gefunden !", " ", MB_OK|MB_ICONSTOP);
			return ;
	}

	if ( ! DateiSetzen() ) 
	{
			MessageBox("Datei drllls.cfgorig oder digilis.cfg fehlt !", " ", MB_OK|MB_ICONSTOP);
			return ;
	}

	int arbpoint = 0 ;
	while (  lsmatrix[arbpoint] > 0  )
	{
		BelegErstellen ( mdn.mdn, lsmatrix[arbpoint] ) ;
		arbpoint ++ ;
	}
	DateiZurueckSetzen() ;

	OnOK();
}
