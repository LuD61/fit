// edixDlg.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "edix.h"
#include "edixDlg.h"
#include "dbClass.h"
#include "dta.h"
#include "adr.h"
#include "mdn.h"
#include "rech.h"
#include "m_edix.h"
#include "kun.h"
#include "mo_numme.h"
#include ".\edixdlg.h"
#include "KursDlg.h"	// 140714

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


extern int itestmode ;	// 150514 

extern DB_CLASS dbClass ;
DTA_CLASS dta_class ;
ADR_CLASS adr_class ;
MDN_CLASS mdn_class ;
extern KUN_CLASS kun_class ;
extern RECH_CLASS rech_class ;

// XDATUM extern char * sqldatamger(char *inpstr) ;
// XDATUM extern char * sqldatgeram(char *inpstr) ;

extern void sqldatdst ( TIMESTAMP_STRUCT *dstakt, char*inpstr) ;

extern void ListenAusdruck (short,long,long,long,short ) ;	// 280108 150514 : short == internmdn dazu
extern void namensetzen(void) ;
extern void sysparlesen(void) ;
extern char *clipped (char *string) ;
extern char *atrim   (char *string) ;	// 100209
// CAboutDlg-Dialogfeld f�r Anwendungsbefehl 'Info'


class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialogfelddaten
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterst�tzung

// Implementierung
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CedixDlg Dialogfeld


CedixDlg::CedixDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CedixDlg::IDD, pParent)
	, v_edit1(_T(""))
	, v_edit3(_T(""))
	, v_edit4(_T(""))
	, v_edit5(_T(""))
	, v_edit11(_T(""))
	, v_Combo1(_T(""))
	, v_edit6(_T(""))
	, v_edit2(0)
	, v_nachdruck(FALSE)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CedixDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, m_edit1);
	DDX_Text(pDX, IDC_EDIT1, v_edit1);

	DDX_Control(pDX, IDC_EDIT3, m_edit3);
	DDX_Text(pDX, IDC_EDIT3, v_edit3);

	DDX_Control(pDX, IDC_EDIT4, m_edit4);
	DDX_Text(pDX, IDC_EDIT4, v_edit4);

	DDX_Control(pDX, IDC_EDIT5, m_edit5);
	DDX_Text(pDX, IDC_EDIT5, v_edit5);

	DDX_Control(pDX, IDC_EDIT11, m_edit11);
	DDX_Text(pDX, IDC_EDIT11, v_edit11);

	DDX_Control(pDX, IDC_COMBO1, m_Combo1);
	DDX_CBString(pDX, IDC_COMBO1, v_Combo1);
	DDX_Text(pDX, IDC_EDIT6, v_edit6);

	DDX_Control(pDX, IDC_EDIT2, m_edit2);
	DDX_Text(pDX, IDC_EDIT2, v_edit2);
	DDX_Check(pDX, IDC_CHECK1, v_nachdruck);
	DDX_Control(pDX, IDC_CHECK1, m_nachdruck);
}

BEGIN_MESSAGE_MAP(CedixDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_EN_KILLFOCUS(IDC_EDIT1, OnEnKillFocusEdit1)
	ON_EN_KILLFOCUS(IDC_EDIT3, OnEnKillFocusEdit3)
	ON_EN_KILLFOCUS(IDC_EDIT4, OnEnKillFocusEdit4)
	ON_EN_KILLFOCUS(IDC_EDIT5, OnEnKillFocusEdit5)

	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	ON_CBN_SELCHANGE(IDC_COMBO1, OnCbnSelchangeCombo1)
	ON_CBN_KILLFOCUS(IDC_COMBO1, OnCbnKillfocusCombo1)
	ON_EN_CHANGE(IDC_EDIT3, OnEnChangeEdit3)
	ON_EN_CHANGE(IDC_EDIT5, OnEnChangeEdit5)
	ON_EN_KILLFOCUS(IDC_EDIT2, OnEnKillfocusEdit2)
	ON_BN_CLICKED(IDC_CHECK1, OnBnClickedCheck1)
END_MESSAGE_MAP()

static char bufh[512] ;

BOOL datumcheck(char * bufi )
{
	int i = (int) strlen(bufi) ;
	int hil1, hil2 ;
	BOOL j = FALSE ;

	char hil5 [5] ;
	if ( i != 6 && i != 8 && i != 10 )
	{
		bufi[0] = '\0' ;
		return TRUE ;
	}
	if ( i == 6 )	// nur ddmmyy erlaubt 
	{
		bufi[10] = '\0' ;
		bufi[ 9] = bufi[5] ;
		bufi[ 8] = bufi[4] ;
		if ( bufi[ 8] > '5' )
		{
			bufi[ 6] = '1' ;
			bufi[ 7] = '9' ;
		}
		else
		{
			bufi[ 6] = '2' ;
			bufi[ 7] = '0' ;
		}
		bufi[ 5] = '.' ;
		bufi[ 4] = bufi[3] ;
		bufi[ 3] = bufi[2] ;
		bufi[ 2] = '.' ;
		i = 10 ;
		j = TRUE ; 
	}
	if ( i == 8 )	// nur dd,mm,yy erlaubt 
	{
		bufi[10] = '\0' ;
		bufi[ 9] = bufi[7] ;
		bufi[ 8] = bufi[6] ;
		if ( bufi[ 8] > '5' )
		{
			bufi[ 6] = '1' ;
			bufi[ 7] = '9' ;
		}
		else
		{
			bufi[ 6] = '2' ;
			bufi[ 7] = '0' ;
		}
		bufi[ 5] = '.' ;
		bufi[ 2] = '.' ;
		i = 10 ;
		j = TRUE ;
	}
	if ( i == 10 )	// hier kommt nix anderes mehr an ....  
	{
		if (   bufi[0] > '9' || bufi[0] < '0' 
			|| bufi[1] > '9' || bufi[1] < '0'
			|| bufi[3] > '9' || bufi[3] < '0'
			|| bufi[4] > '9' || bufi[4] < '0'
			|| bufi[6] > '9' || bufi[6] < '0'
			|| bufi[7] > '9' || bufi[7] < '0'
			|| bufi[8] > '9' || bufi[8] < '0'
			|| bufi[9] > '9' || bufi[9] < '0'
			)
		{

			bufi[0] = '\0' ;
			return TRUE ;
		}

		hil5[0] = bufi[3] ;
		hil5[1] = bufi[4] ;
		hil5[2] = '\0' ;
		hil1 = atoi (hil5);
		if ( hil1 > 12 || hil1 < 1 )
		{
			bufi[0] = '\0' ;
			return TRUE ;
		}

		hil5[0] = bufi[0] ;
		hil5[1] = bufi[1] ;
		hil5[2] = '\0' ;
		hil2 = atoi (hil5);
		if ( hil2 > 31 || hil2 < 1 )		// 260110 hil1->hil2
		{
			bufi[0] = '\0' ;
			return TRUE ;
		}
		switch ( hil1 )
		{
		case  4 :
		case  6 :
		case  9 :
		case 11 :
			if ( hil2 > 30 )
			{
				bufi[0] = '\0' ;
				return TRUE ;
			}
			break ;
		case 2 :

			hil5[0] = bufi[6] ;
			hil5[1] = bufi[7] ;
			hil5[2] = bufi[8] ;
			hil5[3] = bufi[9] ;
			hil5[4] = '\0' ;
			hil1    = atoi(hil5) ;
			if ( hil2 > 28 )	// IM Jahr 2000 geht dieser Check schief .....
			{
				if ( ! ( hil1 % 4) )	// 25.02.08 : hier stand hil2 und dann hats nicht gefunzt
				{
					if ( hil2 > 29 )
					{
						bufi[0] = '\0' ;
						return TRUE ;
					}
				}
				else
				{
					bufi[0] = '\0' ;
					return TRUE ;
				};
			}
			break ;

		}

	}
	return j ;

}


// CedixDlg Meldungshandler

BOOL CedixDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Hinzuf�gen des Men�befehls "Info..." zum Systemmen�.

	// IDM_ABOUTBOX muss sich im Bereich der Systembefehle befinden.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Symbol f�r dieses Dialogfeld festlegen. Wird automatisch erledigt
	//  wenn das Hauptfenster der Anwendung kein Dialogfeld ist
	SetIcon(m_hIcon, TRUE);			// Gro�es Symbol verwenden
	SetIcon(m_hIcon, FALSE);		// Kleines Symbol verwenden

	// TODO: Hier zus�tzliche Initialisierung einf�gen

  // alle dta lesen


	CString szdta ;

	int sqlstat = dta_class.openalldta ();
	sqlstat = dta_class.lesealldta();
	while(!sqlstat)
	{
	
			szdta.Format("%8.0d  %s",dta.dta,dta.kun_krz1);
				// hier haben wir jetzt die dta und k�nnen es in die ListBox einf�gen
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->AddString(szdta.GetBuffer(0));
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->SetCurSel(0);
	
		sqlstat = dta_class.lesealldta () ;
	}

// 150514 A
	if ( internmdn > 0 )
	{
		mdn.mdn = internmdn ;
		v_edit1.Format ("%d" , internmdn) ;
	}
	else
	{
		mdn.mdn = 1 ;
		v_edit1.Format ( "1" ) ;
	}
 	ReadMdn ();

	if ( internmdn > 0 && interndta > 0 )
	{
/* ---> so kann man Systemparameter lesen ......
	dnachkpreis = atoi ( sys_par_class.sys_par_holen ( "nachkpreis" ))  ;
	if ( dnachkpreis > 4 || dnachkpreis < 2 ) dnachkpreis = 2 ;
< -------- */


			dta.dta = interndta  ;
			int i = dta_class.opendta();
			i = dta_class.lesedta();
			if ( i )
			{
				v_Combo1.Format("                   ");
				MessageBox("Ung�ltige DTA-Eingabe !", " ", MB_OK|MB_ICONSTOP);
				UpdateData(FALSE) ;
				OnCancel();		// anstelle von : OnBnClickedCancel() ;
				return TRUE ;
			}
			else
			{
//				CString bufx;
				v_Combo1.Format("%8.0d", dta.dta );
				sprintf ( bufh, "%8.0d", dta.dta );
				int nCurSel = -1 ;
				nCurSel=((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->FindString(nCurSel, bufh);
				if (nCurSel != CB_ERR)
				{
					((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->SetCurSel(nCurSel) ;
//					((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->GetLBText(nCurSel, bufx);
				}
			}
			
			OnBnClickedOk();
			OnCancel () ; // anstelle von : OnBnClickedCancel() ;
			return TRUE ;
	}



// 150514 E



	// TODO: Hier zus�tzliche Initialisierung einf�gen
	
	v_nachdruck = FALSE ; 	// 280108

	return TRUE;  // Geben Sie TRUE zur�ck, au�er ein Steuerelement soll den Fokus erhalten
}

void CedixDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// Wenn Sie dem Dialogfeld eine Schaltfl�che "Minimieren" hinzuf�gen, ben�tigen Sie 
//  den nachstehenden Code, um das Symbol zu zeichnen. F�r MFC-Anwendungen, die das 
//  Dokument/Ansicht-Modell verwenden, wird dies automatisch ausgef�hrt.

void CedixDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Ger�tekontext zum Zeichnen

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Symbol in Clientrechteck zentrieren
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Symbol zeichnen
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// Die System ruft diese Funktion auf, um den Cursor abzufragen, der angezeigt wird, w�hrend der Benutzer
//  das minimierte Fenster mit der Maus zieht.
HCURSOR CedixDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

/* further usage --->
void CedixDlg::OnEnChangeEdit1()
{
	// TODO:  Falls dies ein RICHEDIT-Steuerelement ist, wird das Kontrollelement
	// diese Benachrichtigung nicht senden, es sei denn, Sie setzen den CDialog::OnInitDialog() au�er Kraft.
	// Funktion und Aufruf CRichEditCtrl().SetEventMask()
	// mit dem ENM_CHANGE-Flag ORed in der Eingabe.

	// TODO:  F�gen Sie hier Ihren Code f�r die Kontrollbenachrichtigungsbehandlung ein.
}
< ---- */


// 0 == nur check
// 1 == check , ob Liste existiert		( dta.reli == 1 )
// 2 == update und anschliessend check	( dta.reli == 2 )

int CedixDlg::RELICHECK( int imodus )
{


	if ( imodus == 2 )
	{
		rech.mdn = mdn.mdn  ;
		dbClass.beginwork () ;
		int i = nvsimpel ( 0,0, "edilist", &rech.rech_vbd ) ;
		sqldatdst ( &rech.ts_rvdat, crel_datum ) ;
		rech_class.updvbd(dta.dta) ;
		rech_class.openrech ('c');
		int jj = rech_class.leserech('c');	// lesen nach rech_vbd
		if ( jj )
		{
			dbClass.rollbackwork() ;
			rech.rech_vbd = 0L ;
			return 0 ;	// False
		}
		kun.kun = rech.kun ;
		kun.mdn = mdn.mdn ;
		kun_class.openkun();
		jj = kun_class.lesekun();
		if ( jj )
		{
			dbClass.rollbackwork() ;
			rech.rech_vbd = 0L ;
			return 0 ;	// FALSE
		}
		kun.kun	= kun.inka_nr ;
		kun_class.openkun();
		jj = kun_class.lesekun();
		if ( jj )
		{
			dbClass.rollbackwork() ;
			rech.rech_vbd = 0L ;
			return 0 ;	// FALSE
		}
		if ( kun.sedas_dta != dta.dta  )
		{
			dbClass.rollbackwork() ;
			rech.rech_vbd = 0L ;
			return  0  ;	// irgendwas passt da nicht
		}	
// ACHTUNG : hier steht der Inkasso in tabelle kun, wird als Referenz 
// beim Regulierer-Holen noch mal wiederverwendet !!
		dbClass.commitwork() ;
		return  1 ;
	}

// hier fing es bisher �berhaupt erst an an .....

	int i = m_edit2.GetLine(0,bufh,500);
	bufh[i] = '\0' ;	// 250810 
	if (i)	rech.rech_vbd = (short) atoi ( bufh );
	else rech.rech_vbd = -2 ;
	rech.mdn = mdn.mdn  ;
	rech_class.openrech ('c');
	int jj = rech_class.leserech('c');	// lesen nach rech_vbd
	if ( jj )
		return 0 ;	// False

	kun.kun = rech.kun ;
	kun.mdn = mdn.mdn ;
	kun_class.openkun();
	jj = kun_class.lesekun();
	if ( jj )
		return 0 ;	// FALSE
	kun.kun	= kun.inka_nr ;
	kun_class.openkun();
	jj = kun_class.lesekun();
	if ( jj )
		return 0 ;	// FALSE
	if ( kun.sedas_dta != dta.dta  )
				return  0  ;	// irgendwas passt da nicht
	
// ACHTUNG : hier steht der Inkasso in tabelle kun, wird als Referenz 
// beim Regulierer-Holen noch mal wiederverwendet !!

// XDATUM	v_edit5.Format("%s",_T(sqldatamger( rech.rvdat))) ;
// XDATUM	sqldatgeram ( rech.rvdat ) ;
	v_edit5.Format("%s",_T(rech.rvdat)) ;
if ( imodus == 1 )	sprintf ( crel_datum , "%s", rech.rvdat ) ;	// 280109 : dann muss ja schon alles passen !!
	sqldatdst ( &rech.ts_rvdat , rech.rvdat ) ;
	if ( !imodus )
	{
//		v_edit4.Format("%s",_T(sqldatamger( rech.rvdat))) ;
//		sqldatgeram ( rech.rvdat ) ;
//		v_edit3.Format("%s",_T(sqldatamger( rech.rvdat))) ;
//		sqldatgeram ( rech.rvdat ) ;
		v_edit4.Format("%s",_T( rech.rvdat)) ;
		v_edit3.Format("%s",_T( rech.rvdat)) ;

	}

	if ( imodus )	// nur check
	{
	}
	return 1 ;	// TRUE
}

// 080408 A
BOOL CedixDlg::PreTranslateMessage(LPMSG lpMsg)
{
	CWnd *cWnd;
	 if (lpMsg->message == WM_KEYDOWN)
	 {
		 switch (lpMsg->wParam)
			 {
		 
		        case VK_RETURN :
					cWnd = GetFocus ();
			           if(cWnd == GetDlgItem (IDOK))
							return CDialog::PreTranslateMessage(lpMsg);
			           if(cWnd == GetDlgItem (IDCANCEL))
					   {
							OnCancel ();
 							return TRUE;
					   }

/* --->
			           if(cWnd == GetDlgItem (IDC_KUNN))
					   {	Read (); return TRUE; }
                       else if (cWnd == GetDlgItem (IDC_MDN))
                       { if (ReadMdn () == FALSE)
                       { return TRUE;
                       } }
 < ----- */
			   NextDlgCtrl();
						return TRUE;

			 case VK_F5 :
                     OnCancel ();
 			         return TRUE;
/* ----->
			 case VK_F7 :
                     OnDelete ();
 			         return TRUE;
             case VK_F9 :
		             if (OnF9 ())
                     {
                         return TRUE;
                     }
                     break;
		     case VK_F6 :
                     OnFree ();
				     return TRUE;
 <----- */

/* ---->
			 case VK_F12 :
					UpdateData( TRUE ) ;
					ladevartodaba();
					if (  tou_class.testupdtou() )
						tou_class.inserttou () ;
					else
						tou_class.updatetou () ;

					v_edithtour = 0 ;
					tou.tou = 0 ;
					UpdateData(FALSE) ;
					m_edithtour.EnableWindow (TRUE) ;
					m_buttonhtour.EnableWindow ( TRUE) ;
					m_edithtour.ModifyStyle (0, WS_TABSTOP,0) ;
					m_buttonhtour.ModifyStyle ( 0, WS_TABSTOP,0) ;
					m_edithtour.SetFocus () ;	


//                     OnOK ();
				     return TRUE;
< ----- */


 		    case VK_DOWN :
                     if (NoListCtrl (GetFocus ()))
                     {
						NextDlgCtrl();
						return TRUE;
					
                     }
				     break;
 		    case VK_UP :
                     if (NoListCtrl (GetFocus ()))
                     {
						PrevDlgCtrl();
	 					cWnd = GetFocus ();
					 	GetNextDlgTabItem(cWnd ,FALSE) ;
						return TRUE;
                     }
				     break;
		 }
	 }

return CDialog::PreTranslateMessage(lpMsg);

}



BOOL CedixDlg::NoListCtrl (CWnd *cWnd)
{
    if (((CComboBox *) cWnd->GetParent ())->GetCount () > 1)
    {
        return FALSE;
    }
    else if (((CComboBox *) cWnd)->GetCount () > 1)
    {
        return FALSE;
    }
    else if (((CListBox *) cWnd)->GetCount () > 1)
    {
        return FALSE;
    }
    return TRUE;
}

// 080408 E

// Mandantennummer
void CedixDlg::OnEnKillFocusEdit1()
{

	UpdateData (TRUE) ;

	int i = m_edit1.GetLine(0,bufh,500);
	bufh[i] = '\0' ;	// 250810 
	if (i)	mdn.mdn = (short) atoi ( bufh );
	else mdn.mdn = -2 ;
	mdn_class.openmdn();
	if (! mdn_class.lesemdn())
	{
		adr.adr = mdn.adr ;
		i = adr_class.openadr () ;
		i = adr_class.leseadr () ;
		if (!i) 
		{
			v_edit11.Format("%s",_T(adr.adr_krz));
		}
		else
		{
			v_edit11.Format("              ");

		}
	}
	else	// fehlerhafte Eingabe
	{
		v_edit11.Format("              ");
		
	}
		UpdateData (FALSE) ;
	
}


// von-Datum
void CedixDlg::OnEnKillFocusEdit3()
{
	UpdateData(TRUE);
	int i = m_edit3.GetLine(0,bufh,500) ;
	bufh[i] = '\0' ;	// 250810 
	if (i)
	{
		if (datumcheck(bufh))
		{
			v_edit3.Format("%s",_T(bufh));
			UpdateData (FALSE) ;
		}
	}
}
// bis-Datum
void CedixDlg::OnEnKillFocusEdit4()
{
	UpdateData(TRUE);
	int i = m_edit4.GetLine(0,bufh,500) ;
	bufh[i] = '\0' ;	// 250810 
	if (i)
	{
		if (datumcheck(bufh))
		{
			v_edit4.Format("%s",_T(bufh));
			UpdateData (FALSE) ;
		}
	}
}
// Listendatum
void CedixDlg::OnEnKillFocusEdit5()
{
	UpdateData(TRUE);
	int i = m_edit5.GetLine(0,bufh,500) ;
	bufh[i] = '\0' ;	// 250810 
	if (i)
	{
		if (datumcheck(bufh))
		{
			v_edit5.Format("%s",_T(bufh));
			UpdateData (FALSE) ;
		}
	}
}

void CedixDlg::OnBnClickedOk()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	// Nochmal alles checken und bei korrekter Eingabe Generierung starten

	char AusgabeText [256] ;
	int i ;
	UpdateData(TRUE);

	wechselkurs = 1.00 ;	// 140714

	int ok = TRUE ;
	bufh[0] = '\0' ;

//	if ( v_nachdruck == FALSE )	//	280108 : 090609 : immer eingabe erlauben, aber nur bei erstdruck abchecken 
//	{
		i = m_edit3.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;	// 250810 
		if (i)
		{	if (datumcheck(bufh))
			{	v_edit3.Format("%s",_T(bufh));
			}
		}

		if ( strlen ( bufh ) != 10 )
		{
			if ( v_nachdruck == FALSE )	//	090609 
			{
				ok = FALSE ;
			}
		}
		else
		{	sprintf ( cvon_datum , "%s", bufh) ;
			sqldatdst ( &ts_von_datum , cvon_datum ) ;
		}
// 090609	}

	bufh[0] = '\0' ;

//	if ( v_nachdruck == FALSE )	//	280108 : 090609 : immer eingabe erlauben, aber nur bei erstdruck abchecken 
//	{
		i = m_edit4.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;	// 250810 
		if (i)
		{	if (datumcheck(bufh))
			{	v_edit4.Format("%s",_T(bufh));
			}
		}

		if ( strlen ( bufh ) != 10 )
		{
			if ( v_nachdruck == FALSE )	//	090609 
			{
				ok = FALSE ;
			}
		}
		else
		{	sprintf ( cbis_datum , "%s", bufh) ;
			sqldatdst ( &ts_bis_datum , cbis_datum ) ;
		}
// 090609	}

	bufh[0] = '\0' ;

	if ( v_nachdruck == FALSE && dta.reli != 1 )	// bei Nachdruck  kommt man da ja gar nicht mehr sinnvoll hin
	{
		i = m_edit5.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;	// 250810 
		if (i)
		{	if (datumcheck(bufh))
			{	v_edit5.Format("%s",_T(bufh));
			}
		}
		if ( strlen ( bufh ) != 10 )
		{	ok = FALSE ;
		}
		else
		{	sprintf ( crel_datum , "%s", bufh) ;
			sqldatdst ( &ts_rel_datum , crel_datum ) ;
		}
	}

	bufh[0] = '\0' ;

	int nCurSel = ((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->GetCurSel();
	CString bufx;
	if ( nCurSel > -1 )
	{
		((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->GetLBText(nCurSel, bufx);

		sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
		i = (int) strlen ( bufh );
		// fixe formatierung : 8 stellen und 2 blanks , danach folgt ein text
		if ( i )
		{
			if ( i < 9 )
			{
				bufh[i ] = '\0' ;
			}
			else
			{
				bufh[9] = '\0' ;
			}
			dta.dta = atol ( bufh ) ;
			i = dta_class.opendta();
			i = dta_class.lesedta();
			if ( i )
			{
				v_Combo1.Format("                   ");
				ok = FALSE ;
			}
			else
			{
				if ( v_nachdruck == TRUE )	// 280108 : dieser Zweig ist neu  ......
				{
				}
				else
				{
	
					if ( dta.reli == 1 )	// 130706
					{
						m_edit2.EnableWindow( TRUE );
						m_edit5.EnableWindow( FALSE);
						int retcode = RELICHECK( 1 ) ;
						if (!retcode ) ok = FALSE ;
					}
					else
					{
						if ( dta.reli == 2 )
						{
							m_edit2.EnableWindow( FALSE );
							m_edit5.EnableWindow( TRUE );
							int retcode = RELICHECK( 2 ) ;
							if (!retcode ) ok = FALSE ;
						}
						else	// althergebrachte Abl�ufe
						{
							m_edit2.EnableWindow( FALSE );
							m_edit5.EnableWindow( TRUE );
						}
					}
				}	// ende Original oder Nachdruck 
			}

		}
		else
		{
				v_Combo1.Format("                   ");
				ok = FALSE ;
		}
	}
	else
	{
		sprintf ( bufh, "%s", v_Combo1 );
		i = (int) strlen ( bufh );
		// fixe formatierung : 8 stellen und 2 blanks , danach folgt ein text
		if ( i )
		{
			if ( i < 9 )
			{
				bufh[i ] = '\0' ;
			}
			else
			{
				bufh[9] = '\0' ;
			}
		}
		long hilfe = atol ( bufh ) ;
		nCurSel = -1 ;
		sprintf ( bufh, "%8.0d", hilfe );
		nCurSel=((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->FindString(nCurSel, bufh);
	
		if (nCurSel != CB_ERR)
		{
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->SetCurSel(nCurSel) ;
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->GetLBText(nCurSel, bufx);
			sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
			i = (int) strlen ( bufh );
			// fixe formatierung : 8 stellen und 2 blanks , danach folgt ein text
			if ( i )
			{
				if ( i < 9 )
				{
					bufh[i] = '\0' ;
				}
			}
			else
			{
				bufh[9] = '\0' ;
			}
			dta.dta = atol ( bufh ) ;
			i = dta_class.opendta();
			i = dta_class.lesedta();
			if ( i )
			{
				v_Combo1.Format("                   ");
				ok = FALSE ;
			}
			else
			{
				if ( v_nachdruck == TRUE )	// 280108 
				{
				}
				else	// Bisherige Ablaeufe
				{
					if ( dta.reli == 1 )	// 130706
					{
						m_edit2.EnableWindow( TRUE );
						m_edit5.EnableWindow( FALSE);
						int retcode = RELICHECK( 1 ) ;
						if (!retcode ) ok = FALSE ;
					}
					else
					{
						if ( dta.reli == 2 )
						{
							m_edit2.EnableWindow( FALSE );
							m_edit5.EnableWindow( TRUE );
							int retcode = RELICHECK( 2 ) ;
							if (!retcode ) ok = FALSE ;
						}
						else	// althergebrachte Abl�ufe
						{
							m_edit2.EnableWindow( FALSE );
							m_edit5.EnableWindow( TRUE );
						}
					}
				}	// ende Original oder nachdruck
			}

		}
		else
		{
			v_Combo1.Format("                   ");
			ok = FALSE ;
		}
	}
	UpdateData(FALSE);

	if ( ok == FALSE )
	{
		if ( ! internmdn || itestmode )	// 150514 : Ausgaben unterdr�cken
			MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
	}
	else
	{
	
		ok = FALSE ;	// 150514 : ok neu verwenden

		if ( ! internmdn || itestmode )	// 150514 : Ausgaben unterdr�cken
		{

			// 140714 - erst mal abschalten
			if ( ( dta.typ ==METROCZ || dta.typ == METROSK) && v_nachdruck == FALSE && 1 == 2)
			{
				CKursDlg dlgKurs;
				dlgKurs.putkurs(wechselkurs);
				dlgKurs.DoModal();
				wechselkurs = dlgKurs.getkurs();
			}

			if ( MessageBox("Alle Eingaben ok ?? ", " ", MB_YESNO ) == IDYES )
				ok = TRUE ;
		}
		else
			ok = TRUE ;
		
		// 150514 : hier stand fr�her immer ein Dialog ;-)
		if ( ok == TRUE )
		{

			if ( dta.typ >= LIDL && dta.typ <= LIDL + 20 )	// 101111 : funktioniert von 500 bis 520 
			{
				dta_echt = dta.typ ;
				dta.typ = LIDL ;
			}

			if ( v_nachdruck == TRUE )	// 280108
			{
				sysparlesen() ;
				namensetzen () ;
				ListenAusdruck (mdn.mdn,dta.dta, v_edit2 /* dta_referenz */ ,0L, 0) ;	// 150514 : internmdn immer == 0 ?
				// irgendwie Neuerfassung erzwingen
				v_nachdruck = FALSE ;
				v_edit2 = 0 ;
				m_edit2.EnableWindow( FALSE );
				UpdateData(FALSE) ;
			}
			else
			{
				v_edit6 = _T ("Start") ;
				UpdateData(FALSE) ;
				v_edit6 = _T (GeneriereDatei(AusgabeText,internmdn)) ;	// 150514 : internmdn dazu 
				UpdateData(FALSE) ;
			}
			if ( dta.typ == LIDL  )	// 101111
			{
				dta.typ = dta_echt ;
				dta_echt = LIDL ;
			}

		}
	}
// nur mit cancel kann das Programm verlassen werden -> 	OnOK();
	if ( internmdn > 0 )	// 150514
		OnCancel ();
}

void CedixDlg::OnCbnSelchangeCombo1()
{

	char bufh[400] ;
	traktiv = 1 ;
	UpdateData(TRUE) ;
//	int i = m_Combo1.GetLine(0,bufh,9) ;

	int nCurSel = ((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->GetCurSel();
	CString bufc;
	((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->GetLBText(nCurSel, bufc);

	sprintf(bufh,"%s",bufc.GetBuffer(0)) ;

	// fixe formatierung : 8 stellen und 2 blanks , danch folgt ein text
	int i = (int)strlen ( bufh) ;
	if ( i )
	{
		if ( i < 9 )
		{
			bufh[i] = '\0' ;
		}
		else
		{
			bufh[9] = '\0' ;
		}
		dta.dta = atol ( bufh ) ;
		i = dta_class.opendta();
		i = dta_class.lesedta();
	}
	

	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
}


void CedixDlg::OnCbnKillfocusCombo1()
{

	traktiv = 1 ;
	UpdateData(TRUE) ;
	bufh[0] = '\0' ;

	int nCurSel = ((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->GetCurSel();
	CString bufx;
	if ( nCurSel > -1 )
	{
		((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->GetLBText(nCurSel, bufx);

		sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
		int i = (int) strlen ( bufh );
		// fixe formatierung : 8 stellen und 2 blanks , danach folgt ein text
		if ( i )
		{
			if ( i < 9 )
			{
				bufh[i ] = '\0' ;
			}
			else
			{
				bufh[9] = '\0' ;
			}
			dta.dta = atol ( bufh ) ;
			i = dta_class.opendta();
			i = dta_class.lesedta();
			if ( i )
			{
				v_Combo1.Format("                   ");
			}
			else
			{
				if ( dta.reli == 1 )	// 130706
				{
					m_edit2.EnableWindow( TRUE );
					m_edit5.EnableWindow( FALSE);

				}
				else
				{	
						// hier vorerst auch dta.reli == 2 mit dabei ...
					m_edit2.EnableWindow( FALSE );
					m_edit5.EnableWindow( TRUE );
				}
			}

		}
		else
		{
				v_Combo1.Format("                   ");
		}
	}
	else
	{
		sprintf ( bufh, "%s", v_Combo1 );
		int i = (int) strlen ( bufh );
		// fixe formatierung : 8 stellen und 2 blanks , danach folgt ein text
		if ( i )
		{
			if ( i < 9 )
			{
				bufh[i ] = '\0' ;
			}
			else
			{
				bufh[9] = '\0' ;
			}
		}
		long hilfe = atol ( bufh ) ;
		nCurSel = -1 ;
		sprintf ( bufh, "%8.0d", hilfe );
		nCurSel=((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->FindString(nCurSel, bufh);
	
		if (nCurSel != CB_ERR)
		{
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->SetCurSel(nCurSel) ;
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->GetLBText(nCurSel, bufx);
			sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
			i = (int) strlen ( bufh );
			// fixe formatierung : 8 stellen und 2 blanks , danach folgt ein text
			if ( i )
			{
				if ( i < 9 )
				{
					bufh[i] = '\0' ;
				}
			}
			else
			{
				bufh[9] = '\0' ;
			}
			dta.dta = atol ( bufh ) ;
			i = dta_class.opendta();
			i = dta_class.lesedta();
			if ( i )
			{
				v_Combo1.Format("                   ");
			}
			else
			{
				v_Combo1.Format(bufx.GetBuffer(0));
				if ( dta.reli == 1 )	// 130706
				{
					m_edit2.EnableWindow( TRUE );
					m_edit5.EnableWindow( FALSE);

				}
				else
				{
						// hier vorerst auch dta.reli == 2 mit dabei
					m_edit2.EnableWindow( FALSE );
					m_edit5.EnableWindow( TRUE );
				}

			}

		}
		else
		{
			v_Combo1.Format("                   ");
		}
	}
	UpdateData(FALSE);

	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
}

void CedixDlg::OnEnChangeEdit3()
{
	// TODO:  Falls dies ein RICHEDIT-Steuerelement ist, wird das Kontrollelement
	// diese Benachrichtigung nicht senden, es sei denn, Sie setzen den CDialog::OnInitDialog() au�er Kraft.
	// Funktion und Aufruf CRichEditCtrl().SetEventMask()
	// mit dem ENM_CHANGE-Flag ORed in der Eingabe.

	// TODO:  F�gen Sie hier Ihren Code f�r die Kontrollbenachrichtigungsbehandlung ein.
}

void CedixDlg::OnEnChangeEdit5()
{
	// TODO:  Falls dies ein RICHEDIT-Steuerelement ist, wird das Kontrollelement
	// diese Benachrichtigung nicht senden, es sei denn, Sie setzen den CDialog::OnInitDialog() au�er Kraft.
	// Funktion und Aufruf CRichEditCtrl().SetEventMask()
	// mit dem ENM_CHANGE-Flag ORed in der Eingabe.

	// TODO:  F�gen Sie hier Ihren Code f�r die Kontrollbenachrichtigungsbehandlung ein.
}

void CedixDlg::OnEnKillfocusEdit2()
{
	traktiv = 1 ;
	UpdateData(TRUE);

	if ( v_nachdruck == FALSE )	// 280108 : bei Nachdruck NICHT hier pruefen 
	{
		int retcode = RELICHECK( 0 ) ;
		if ( !retcode )
			v_edit2 = 0  ;
	}
	UpdateData (FALSE) ;
}

void CedixDlg::OnBnClickedCheck1()	// 280108
{
	if ( v_nachdruck == FALSE )
	{
		v_nachdruck = TRUE ;
		m_edit2.EnableWindow( TRUE );
//		m_edit5.EnableWindow( TRUE );

	}
	else
	{
		v_nachdruck = FALSE ;
	}
	UpdateData(FALSE);
}

void CedixDlg::paras (char * cdta , char * cmdn , char * vdat ,char * bdat ,char * rdat    )
{

	// Lese die Kdo-Zeilenparameter und schreibe sie in interne Werte
	interndta = atol ( cdta ) ;
	internmdn = atoi ( cmdn ) ;

	if ( vdat[0] == 'X' ) internmdn = 0;
	if ( bdat[0] == 'X' ) internmdn = 0;
	if ( rdat[0] == 'X' ) internmdn = 0;
	if ( internmdn > 0 && interndta > 0  )
	{
		if ( ! datumcheck(vdat) && strlen (vdat)==10)
		{	v_edit3.Format("%s",_T(vdat));
			sprintf ( cvon_datum , "%s", vdat) ;
			sqldatdst ( &ts_von_datum , cvon_datum ) ;
		}
		else
			internmdn = 0 ;
	}

	if ( internmdn > 0 && interndta > 0  )
	{
		if ( ! datumcheck(bdat)&& strlen (bdat)==10)
		{	v_edit4.Format("%s",_T(bdat));
			sprintf ( cbis_datum , "%s", bdat) ;
			sqldatdst ( &ts_bis_datum , cbis_datum ) ;
		}
		else
			internmdn = 0 ;
	}

	if ( internmdn > 0 && interndta > 0  )
	{
		if ( ! datumcheck(rdat)&& strlen (rdat)==10)
		{	v_edit5.Format("%s",_T(rdat));
			sprintf ( crel_datum , "%s", rdat) ;
			sqldatdst ( &ts_rel_datum , crel_datum ) ;
		}
		else
			internmdn = 0 ;
	}
	if ( internmdn > 0 &&  interndta > 0 )
	{
		dta.dta = interndta ; 
		int	i = dta_class.opendta();
		i = dta_class.lesedta();
		if ( i )
		{
			internmdn = 0 ;
			v_Combo1.Format("                   ");
		}
		else
			v_Combo1.Format( "%d   " , interndta );
	}

	if ( ! interndta )
	{
		internmdn = 0 ;	//  das ist jetzt der Marker
	}

	if ( internmdn > 0 )	// 130714

	{
		dbClass.sql_mode = 1 ;	// 140814 : damit es bei sql-fehlern(lock ?!) wenigstens weitergeht

		if ( ts_bis_datum.day == ts_von_datum.day
			 && ts_bis_datum.month == ts_von_datum.month
			 && ts_bis_datum.year == ts_von_datum.year
			)
		{ // einen Monat kleiner werden
			if ( ts_von_datum.day > 28 )
				ts_von_datum.day = 28 ;
			ts_von_datum.month = ts_von_datum.month - 1 ;
			if ( ts_von_datum.month < 1 )
			{
				ts_von_datum.month = 12 ;
				ts_von_datum.year = ts_von_datum.year - 1 ;
			}
			char hilfedat [20];
			sprintf ( hilfedat, "%02d.%02d.%04d", ts_von_datum.day, ts_von_datum.month, ts_von_datum.year);
			v_edit3.Format("%s",_T(hilfedat));
			sprintf ( cvon_datum , "%s", hilfedat) ;
		}

		v_nachdruck = FALSE ;
	}


//	UpdateData(FALSE );

}

void CedixDlg::ReadMdn (void) 
{
	mdn_class.openmdn();
	if (! mdn_class.lesemdn())
	{
		adr.adr = mdn.adr ;
		int i = adr_class.openadr () ;
		i = adr_class.leseadr () ;
		if (!i) 
		{
			v_edit11.Format("%s",_T(adr.adr_krz));
		}
		else
		{
			v_edit11.Format("              ");
			MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
//	gibbet nich		PrevDlgCtrl();
		}
	}
	else	// fehlerhafte Eingabe
	{
		v_edit11.Format("              ");
		MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
// gibbet nich 		PrevDlgCtrl();
	}
		UpdateData (FALSE) ;
}
