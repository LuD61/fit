#ifndef _EDIK_DEF
#define _EDIK_DEF

struct EDIK {

	short mdn ;
	long dta ;
	long listnr ;
	TIMESTAMP_STRUCT ts_rech_dat ;
	char rech_dat[11] ;
	long dta_ref ;
	long abkommen ;
	long mge_lief ;
	double kurs ;	// 140714
};
extern struct EDIK edik, edik_null;

struct EDIP {
	short mdn ;
	long dta ;
	long listnr ;
	long inka_nr ;
	long rech_nr ;
	char blg_typ[2] ;
	TIMESTAMP_STRUCT ts_rech_dat ;
	char rech_dat[11] ;
	char iln[17] ;
	long dta_ref ;
	long abkommen ;
};

extern struct EDIP edip, edip_null;

class EDIK_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int insertedik (void);
               int leseedik (void);
               int openedik (void);
               EDIK_CLASS () : DB_CLASS ()
               {
               }
};

class EDIP_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int insertedip (void);
               int leseedip (void);
               int openedip (void);
               EDIP_CLASS () : DB_CLASS ()
               {
               }
};


#endif

