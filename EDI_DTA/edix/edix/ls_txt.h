#ifndef _LS_TXT_DEF
#define _LS_TXT_DEF
struct LS_TXT {
    long nr ;
 	long zei ;
    char txt[61] ;
};

struct LSPT {
    long nr ;
 	long zei ;
    char txt[61] ;
};


extern struct LS_TXT ls_txt, ls_txt_null;
extern struct LSPT lspt, lspt_null;


class LS_TXT_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int dbcount (void);
               int lesels_txt (void);
               int openls_txt (void);
               LS_TXT_CLASS () : DB_CLASS ()
               {
               }
};


class LSPT_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int leselspt (void);
               int openlspt (void);
               LSPT_CLASS () : DB_CLASS ()
               {
               }
};

#endif

