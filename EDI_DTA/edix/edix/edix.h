// edix.h : Hauptheaderdatei f�r die edix-Anwendung
//

#pragma once

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// Hauptsymbole


// CedixApp:
// Siehe edix.cpp f�r die Implementierung dieser Klasse
//

class CedixApp : public CWinApp
{
public:
	CedixApp();

// �berschreibungen
	public:
	virtual BOOL InitInstance();

// Implementierung

	DECLARE_MESSAGE_MAP()
};

extern CedixApp theApp;
