#include "stdafx.h"
#include "DbClass.h"
#include "kun.h"

struct KUN kun,  kun_null, kuninka;
extern DB_CLASS dbClass;

// int itxt_nr ;

static int anzzfelder ;
static long suchkunde ;
static long findadr2 ;

int KUN_CLASS::dbcount (void)
/**
Tabelle kun lesen.
**/
{

         if (test_upd_cursor == -1)
         {
             prepare ();
         }
         dbClass.sqlopen (count_cursor);
         dbClass.sqlfetch (count_cursor);
         if (sqlstatus == 0)
         {
                      return anzzfelder;
         }
         return sqlstatus;

}


long KUN_CLASS::lesekunadr2(long exsuchkunde)
{

	// suchkunde : adr2 lesen und zur�ckgeben ODER 0 ,falls nix gefunden
	if ( readcursor < 0 ) prepare ();	

	suchkunde = exsuchkunde ;
	// kun.mdn sei mal schon vorher ok. gewesen 
    int retkunde = dbClass.sqlopen (test_upd_cursor);
	if ( retkunde )
	{
		sprintf ( kun.bbs, "" ) ;	// 220211
		return 0L ;
	}
    retkunde = dbClass.sqlfetch (test_upd_cursor);
	if ( retkunde )
	{
		sprintf ( kun.bbs, "" ) ;	// 220211
		return 0L ;
	}
	return ( findadr2 ) ;
}
int KUN_CLASS::lesekun (void)
{
      int di = dbClass.sqlfetch (readcursor);

	  return di;
}

int KUN_CLASS::leseareg (void)
{
      int di = dbClass.sqlfetch (readabeding);

	  return di;
}

int KUN_CLASS::lesebreg (void)
{
      int di = dbClass.sqlfetch (readbbeding);

	  return di;
}

int KUN_CLASS::lesecreg (void)
{
      int di = dbClass.sqlfetch (readcbeding);

	  return di;
}


int KUN_CLASS::openkun (void)
{

		if ( readcursor < 0 ) prepare ();	
		
         return dbClass.sqlopen (readcursor);
}

int KUN_CLASS::openareg (void)
{

		if ( readcursor < 0 ) prepare ();
		
         return dbClass.sqlopen (readabeding);
}

int KUN_CLASS::openbreg (void)
{

		if ( readcursor < 0 ) prepare ();
		
         return dbClass.sqlopen (readbbeding);
}

int KUN_CLASS::opencreg (void)
{

		if ( readcursor < 0 ) prepare ();
		
         return dbClass.sqlopen (readcbeding);
}


void KUN_CLASS::prepare (void)
{


	dbClass.sqlin ((long *) &kun.kun, SQLLONG, 0);
	dbClass.sqlin ((short *) &kun.mdn, SQLSHORT, 0);
    dbClass.sqlout ((long *)  &anzzfelder, SQLLONG, 0);


    count_cursor = (short ) dbClass.sqlcursor ("select count(*) from kun "
										"where kun.kun = ? and kun.mdn = ? ");
										

	dbClass.sqlin ((long *) &suchkunde, SQLLONG, 0);
	dbClass.sqlin ((short *) &kun.mdn, SQLSHORT, 0);
 
	dbClass.sqlout ((long *)  &findadr2, SQLLONG, 0);
	dbClass.sqlout ((char *)   kun.bbs , SQLCHAR, 9);	// 220211 : kun.bbs ueberpunselt !!!!!

    test_upd_cursor = (short) dbClass.sqlcursor ("select adr2 , bbs from kun "
										"where kun.kun = ? and kun.mdn = ? ");
										
	// readcursor zum lesen im Kundenstamm 

	dbClass.sqlin ((long *) &kun.kun, SQLLONG, 0);
	dbClass.sqlin ((short *) &kun.mdn, SQLSHORT, 0);


	dbClass.sqlout ((short *) &kun.mdn, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kun.fil, SQLSHORT, 0);
	dbClass.sqlout ((long *) &kun.kun, SQLLONG, 0);
	dbClass.sqlout ((long *) &kun.adr1, SQLLONG, 0);
	dbClass.sqlout ((long *) &kun.adr2, SQLLONG, 0);  
	dbClass.sqlout ((long *) &kun.adr3, SQLLONG, 0); 
	dbClass.sqlout ((char *) kun.kun_seit, SQLCHAR, 11);
	dbClass.sqlout ((long *) &kun.txt_nr1, SQLLONG, 0);
	dbClass.sqlout ((char *) kun.frei_txt1, SQLCHAR, 65);
	dbClass.sqlout ((char *) kun.kun_krz1, SQLCHAR, 17);

	dbClass.sqlout ((char *) kun.kun_bran, SQLCHAR, 2);
	dbClass.sqlout ((char *) kun.kun_krz2, SQLCHAR, 17);
	dbClass.sqlout ((char *) kun.kun_krz3, SQLCHAR, 17);
	dbClass.sqlout ((short *) &kun.kun_typ, SQLSHORT, 0);
	dbClass.sqlout ((long *) &kun.bbn, SQLLONG, 0);
	dbClass.sqlout ((short *) &kun.pr_stu, SQLSHORT, 0);
	dbClass.sqlout ((long *) &kun.pr_lst, SQLLONG, 0);
	dbClass.sqlout ((char *) kun.vereinb, SQLCHAR, 6);
	dbClass.sqlout ((long *) &kun.inka_nr, SQLLONG, 0);
	dbClass.sqlout ((long *) &kun.vertr1, SQLLONG, 0);
	dbClass.sqlout ((long *) &kun.vertr2, SQLLONG, 0);
	dbClass.sqlout ((char *) kun.statk_period, SQLCHAR, 2);
	dbClass.sqlout ((char *) kun.a_period, SQLCHAR, 2);

	dbClass.sqlout ((short *) &kun.sprache, SQLSHORT, 0);
	dbClass.sqlout ((long *) &kun.txt_nr2, SQLLONG, 0);
	dbClass.sqlout ((char *) kun.frei_txt2, SQLCHAR, 65);
	dbClass.sqlout ((char *) kun.freifeld1, SQLCHAR, 9);
	dbClass.sqlout ((char *) kun.freifeld2, SQLCHAR, 9);
	dbClass.sqlout ((long *) &kun.tou, SQLLONG, 0);
	dbClass.sqlout ((char *) kun.vers_art, SQLCHAR, 3);
	dbClass.sqlout ((short *) &kun.lief_art, SQLSHORT, 0);
	dbClass.sqlout ((char *) kun.fra_ko_ber, SQLCHAR, 3);
	dbClass.sqlout ((short *) &kun.rue_schei, SQLSHORT, 0);
	dbClass.sqlout ((char *) kun.form_typ1, SQLCHAR, 3);
	dbClass.sqlout ((short *) &kun.auflage1, SQLSHORT, 0);
	dbClass.sqlout ((char *) kun.freifeld3, SQLCHAR, 9);
	dbClass.sqlout ((char *) kun.freifeld4, SQLCHAR, 9);
	dbClass.sqlout ((short *) &kun.zahl_art, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kun.zahl_ziel, SQLSHORT, 0);
	dbClass.sqlout ((char *) kun.form_typ2, SQLCHAR, 3);
	dbClass.sqlout ((short *) &kun.auflage2, SQLSHORT, 0);
	dbClass.sqlout ((long *) &kun.txt_nr3, SQLLONG, 0);
	dbClass.sqlout ((char *) kun.frei_txt3, SQLCHAR, 65);
	dbClass.sqlout ((char *) kun.nr_bei_rech, SQLCHAR, 17);
	dbClass.sqlout ((char *) kun.rech_st, SQLCHAR, 3);
	dbClass.sqlout ((short *) &kun.sam_rech, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kun.einz_ausw, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kun.gut, SQLSHORT, 0);
	dbClass.sqlout ((char *) kun.rab_schl, SQLCHAR, 9);

	dbClass.sqlout ((double *) &kun.bonus1, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &kun.bonus2, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &kun.tdm_grenz1, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &kun.tdm_grenz2, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &kun.jr_plan_ums, SQLDOUBLE, 0);
	dbClass.sqlout ((char *) kun.deb_kto, SQLCHAR, 9);
	dbClass.sqlout ((double *) &kun.kred_lim, SQLDOUBLE, 0);
	dbClass.sqlout ((short *) &kun.inka_zaehl, SQLSHORT, 0);
	dbClass.sqlout ((char *) kun.bank_kun, SQLCHAR, 37);
	dbClass.sqlout ((long *) &kun.blz, SQLLONG, 0);
	dbClass.sqlout ((char *) kun.kto_nr, SQLCHAR, 17);
	dbClass.sqlout ((short *) &kun.hausbank, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kun.kun_of_po, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kun.kun_of_lf, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kun.kun_of_best, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kun.delstatus, SQLSHORT, 0);
	dbClass.sqlout ((char *) kun.kun_bran2, SQLCHAR, 3);
	dbClass.sqlout ((long *) &kun.rech_fuss_txt, SQLLONG, 0);
	dbClass.sqlout ((long *) &kun.ls_fuss_txt, SQLLONG, 0);
	dbClass.sqlout ((char *) kun.ust_id, SQLCHAR, 12);
	dbClass.sqlout ((long *) &kun.rech_kopf_txt, SQLLONG, 0);
	dbClass.sqlout ((long *) &kun.ls_kopf_txt, SQLLONG, 0);
	dbClass.sqlout ((char *) kun.gn_pkt_kz, SQLCHAR, 2);
	dbClass.sqlout ((char *) kun.sw_rab, SQLCHAR, 2);

	dbClass.sqlout ((char *) kun.bbs, SQLCHAR, 9);
	dbClass.sqlout ((long *) &kun.inka_nr2, SQLLONG, 0);
	dbClass.sqlout ((short *) &kun.sw_fil_gr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kun.sw_fil, SQLSHORT, 0);
	dbClass.sqlout ((char *) kun.ueb_kz, SQLCHAR, 2);
	dbClass.sqlout ((char *) kun.modif, SQLCHAR, 2);
	dbClass.sqlout ((short *) &kun.kun_leer_kz, SQLSHORT, 0);
	dbClass.sqlout ((char *) kun.ust_id16, SQLCHAR, 17);
	dbClass.sqlout ((char *) kun.iln, SQLCHAR, 17);
	dbClass.sqlout ((short *) &kun.waehrung, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kun.pr_ausw, SQLSHORT, 0);
	dbClass.sqlout ((char *) kun.pr_hier, SQLCHAR, 2);
	dbClass.sqlout ((short *) &kun.pr_ausw_ls, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kun.pr_ausw_re, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kun.kun_gr1, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kun.kun_gr2, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kun.eg_kz, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kun.bonitaet, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kun.kred_vers, SQLSHORT, 0);
	dbClass.sqlout ((long *) &kun.kst, SQLLONG, 0);

	dbClass.sqlout ((char *) kun.edi_typ, SQLCHAR, 2);
	dbClass.sqlout ((long *) &kun.sedas_dta, SQLLONG, 0);
	dbClass.sqlout ((char *) kun.sedas_kz, SQLCHAR, 3);
	dbClass.sqlout ((short *) &kun.sedas_umf, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kun.sedas_abr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kun.sedas_gesch, SQLSHORT, 0);

	dbClass.sqlout ((short *) &kun.sedas_satz, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kun.sedas_med, SQLSHORT, 0);
	dbClass.sqlout ((char *) kun.sedas_nam, SQLCHAR, 11);
	dbClass.sqlout ((short *) &kun.sedas_abk1, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kun.sedas_abk2, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kun.sedas_abk3, SQLSHORT, 0);

	dbClass.sqlout ((char *) kun.sedas_nr1, SQLCHAR, 9);
	dbClass.sqlout ((char *) kun.sedas_nr2, SQLCHAR, 9);
	dbClass.sqlout ((char *) kun.sedas_nr3, SQLCHAR, 9);
	dbClass.sqlout ((short *) &kun.sedas_vb1, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kun.sedas_vb2, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kun.sedas_vb3, SQLSHORT, 0);

	dbClass.sqlout ((char *) kun.sedas_iln, SQLCHAR, 17);
	dbClass.sqlout ((long *) &kun.kond_kun, SQLLONG, 0);
	dbClass.sqlout ((short *) &kun.kun_schema, SQLSHORT, 0);
	dbClass.sqlout ((char *) kun.plattform, SQLCHAR, 17);
	dbClass.sqlout ((char *) kun.be_log, SQLCHAR, 4);
	dbClass.sqlout ((long *) &kun.stat_kun, SQLLONG, 0);
	dbClass.sqlout ((char *) kun.ust_nummer, SQLCHAR, 25);
	dbClass.sqlout ((char *) kun.fak_kz, SQLCHAR, 2);
	dbClass.sqlout ((long *) &kun.fak_nr, SQLLONG, 0);
	dbClass.sqlout ((char *) kun.rechiln, SQLCHAR, 18);

	readcursor = (short) dbClass.sqlcursor ("select "

	" mdn, fil, kun, adr1, adr2, adr3, kun_seit, txt_nr1, frei_txt1, "
	" kun_krz1, kun_bran, kun_krz2, kun_krz3, kun_typ, bbn, pr_stu, "
	" pr_lst, vereinb, inka_nr, vertr1, vertr2, statk_period, a_period, "
	" sprache, txt_nr2, frei_txt2, freifeld1, freifeld2, tou, vers_art, "
	" lief_art, fra_ko_ber, rue_schei, form_typ1, auflage1, freifeld3, "
	" freifeld4, zahl_art, zahl_ziel, form_typ2, auflage2, txt_nr3, "
	" frei_txt3, nr_bei_rech, rech_st, sam_rech, einz_ausw, gut, rab_schl, "
	" bonus1, bonus2, tdm_grenz1, tdm_grenz2, jr_plan_ums, deb_kto, "
	" kred_lim, inka_zaehl, bank_kun, blz, kto_nr, hausbank, kun_of_po, "
	" kun_of_lf, kun_of_best, delstatus, kun_bran2, rech_fuss_txt, "
	" ls_fuss_txt, ust_id, rech_kopf_txt, ls_kopf_txt, gn_pkt_kz, sw_rab, "
	" bbs, inka_nr2, sw_fil_gr, sw_fil, ueb_kz, modif, kun_leer_kz,"
	" ust_id16, iln, waehrung, pr_ausw, pr_hier, pr_ausw_ls, "
	" pr_ausw_re, kun_gr1, kun_gr2, eg_kz, bonitaet, kred_vers, kst, "
	" edi_typ, sedas_dta, sedas_kz, sedas_umf, sedas_abr, sedas_gesch, "
	" sedas_satz, sedas_med, sedas_nam, sedas_abk1, sedas_abk2, sedas_abk3, "
	" sedas_nr1, sedas_nr2, sedas_nr3, sedas_vb1, sedas_vb2, sedas_vb3, "
	" sedas_iln, kond_kun, kun_schema, plattform, be_log, stat_kun, "
	" ust_nummer, fak_kz, fak_nr, rechiln "

	" from kun where kun = ? and mdn = ? " ) ;


// readcursor inkaaread fuer metro mit tsmt-trennung 

	dbClass.sqlin ((long *)	 &kuninka.sedas_dta, SQLLONG,0 );
	dbClass.sqlin ((short *) &kuninka.mdn, SQLSHORT, 0);

	dbClass.sqlout ((short *) &kuninka.mdn, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.fil, SQLSHORT, 0);
	dbClass.sqlout ((long *) &kuninka.kun, SQLLONG, 0);
	dbClass.sqlout ((long *) &kuninka.adr1, SQLLONG, 0);
	dbClass.sqlout ((long *) &kuninka.adr2, SQLLONG, 0);  
	dbClass.sqlout ((long *) &kuninka.adr3, SQLLONG, 0); 
	dbClass.sqlout ((char *) kuninka.kun_seit, SQLCHAR, 11);
	dbClass.sqlout ((long *) &kuninka.txt_nr1, SQLLONG, 0);
	dbClass.sqlout ((char *) kuninka.frei_txt1, SQLCHAR, 65);

	dbClass.sqlout ((char *) kuninka.kun_krz1, SQLCHAR, 17);
	dbClass.sqlout ((char *) kuninka.kun_bran, SQLCHAR, 2);
	dbClass.sqlout ((char *) kuninka.kun_krz2, SQLCHAR, 17);
	dbClass.sqlout ((char *) kuninka.kun_krz3, SQLCHAR, 17);
	dbClass.sqlout ((short *) &kuninka.kun_typ, SQLSHORT, 0);
	dbClass.sqlout ((long *) &kuninka.bbn, SQLLONG, 0);
	dbClass.sqlout ((short *) &kuninka.pr_stu, SQLSHORT, 0);
	dbClass.sqlout ((long *) &kuninka.pr_lst, SQLLONG, 0);
	dbClass.sqlout ((char *) kuninka.vereinb, SQLCHAR, 6);
	dbClass.sqlout ((long *) &kuninka.inka_nr, SQLLONG, 0);
	dbClass.sqlout ((long *) &kuninka.vertr1, SQLLONG, 0);
	dbClass.sqlout ((long *) &kuninka.vertr2, SQLLONG, 0);
	dbClass.sqlout ((char *) kuninka.statk_period, SQLCHAR, 2);
	dbClass.sqlout ((char *) kuninka.a_period, SQLCHAR, 2);
	dbClass.sqlout ((short *) &kuninka.sprache, SQLSHORT, 0);
	dbClass.sqlout ((long *) &kuninka.txt_nr2, SQLLONG, 0);
	dbClass.sqlout ((char *) kuninka.frei_txt2, SQLCHAR, 65);
	dbClass.sqlout ((char *) kuninka.freifeld1, SQLCHAR, 9);
	dbClass.sqlout ((char *) kuninka.freifeld2, SQLCHAR, 9);
	dbClass.sqlout ((long *) &kuninka.tou, SQLLONG, 0);
	dbClass.sqlout ((char *) kuninka.vers_art, SQLCHAR, 3);
	dbClass.sqlout ((short *) &kuninka.lief_art, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.fra_ko_ber, SQLCHAR, 3);
	dbClass.sqlout ((short *) &kuninka.rue_schei, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.form_typ1, SQLCHAR, 3);
	dbClass.sqlout ((short *) &kuninka.auflage1, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.freifeld3, SQLCHAR, 9);
	dbClass.sqlout ((char *) kuninka.freifeld4, SQLCHAR, 9);
	dbClass.sqlout ((short *) &kuninka.zahl_art, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.zahl_ziel, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.form_typ2, SQLCHAR, 3);
	dbClass.sqlout ((short *) &kuninka.auflage2, SQLSHORT, 0);
	dbClass.sqlout ((long *) &kuninka.txt_nr3, SQLLONG, 0);
	dbClass.sqlout ((char *) kuninka.frei_txt3, SQLCHAR, 65);
	dbClass.sqlout ((char *) kuninka.nr_bei_rech, SQLCHAR, 17);
	dbClass.sqlout ((char *) kuninka.rech_st, SQLCHAR, 3);
	dbClass.sqlout ((short *) &kuninka.sam_rech, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.einz_ausw, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.gut, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.rab_schl, SQLCHAR, 9);

	dbClass.sqlout ((double *) &kuninka.bonus1, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &kuninka.bonus2, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &kuninka.tdm_grenz1, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &kuninka.tdm_grenz2, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &kuninka.jr_plan_ums, SQLDOUBLE, 0);
	dbClass.sqlout ((char *) kuninka.deb_kto, SQLCHAR, 9);
	dbClass.sqlout ((double *) &kuninka.kred_lim, SQLDOUBLE, 0);
	dbClass.sqlout ((short *) &kuninka.inka_zaehl, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.bank_kun, SQLCHAR, 37);
	dbClass.sqlout ((long *) &kuninka.blz, SQLLONG, 0);
	dbClass.sqlout ((char *) kuninka.kto_nr, SQLCHAR, 17);
	dbClass.sqlout ((short *) &kuninka.hausbank, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.kun_of_po, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.kun_of_lf, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.kun_of_best, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.delstatus, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.kun_bran2, SQLCHAR, 3);
	dbClass.sqlout ((long *) &kuninka.rech_fuss_txt, SQLLONG, 0);
	dbClass.sqlout ((long *) &kuninka.ls_fuss_txt, SQLLONG, 0);
	dbClass.sqlout ((char *) kuninka.ust_id, SQLCHAR, 12);
	dbClass.sqlout ((long *) &kuninka.rech_kopf_txt, SQLLONG, 0);
	dbClass.sqlout ((long *) &kuninka.ls_kopf_txt, SQLLONG, 0);
	dbClass.sqlout ((char *) kuninka.gn_pkt_kz, SQLCHAR, 2);
	dbClass.sqlout ((char *) kuninka.sw_rab, SQLCHAR, 2);
	dbClass.sqlout ((char *) kuninka.bbs, SQLCHAR, 9);
	dbClass.sqlout ((long *) &kuninka.inka_nr2, SQLLONG, 0);
	dbClass.sqlout ((short *) &kuninka.sw_fil_gr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.sw_fil, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.ueb_kz, SQLCHAR, 2);
	dbClass.sqlout ((char *) kuninka.modif, SQLCHAR, 2);
	dbClass.sqlout ((short *) &kuninka.kun_leer_kz, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.ust_id16, SQLCHAR, 17);
	dbClass.sqlout ((char *) kuninka.iln, SQLCHAR, 17);
	dbClass.sqlout ((short *) &kuninka.waehrung, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.pr_ausw, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.pr_hier, SQLCHAR, 2);
	dbClass.sqlout ((short *) &kuninka.pr_ausw_ls, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.pr_ausw_re, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.kun_gr1, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.kun_gr2, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.eg_kz, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.bonitaet, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.kred_vers, SQLSHORT, 0);
	dbClass.sqlout ((long *) &kuninka.kst, SQLLONG, 0);
	dbClass.sqlout ((char *) kuninka.edi_typ, SQLCHAR, 2);
	dbClass.sqlout ((long *) &kuninka.sedas_dta, SQLLONG, 0);
	dbClass.sqlout ((char *) kuninka.sedas_kz, SQLCHAR, 3);
	dbClass.sqlout ((short *) &kuninka.sedas_umf, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.sedas_abr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.sedas_gesch, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.sedas_satz, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.sedas_med, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.sedas_nam, SQLCHAR, 11);
	dbClass.sqlout ((short *) &kuninka.sedas_abk1, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.sedas_abk2, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.sedas_abk3, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.sedas_nr1, SQLCHAR, 9);
	dbClass.sqlout ((char *) kuninka.sedas_nr2, SQLCHAR, 9);
	dbClass.sqlout ((char *) kuninka.sedas_nr3, SQLCHAR, 9);
	dbClass.sqlout ((short *) &kuninka.sedas_vb1, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.sedas_vb2, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.sedas_vb3, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.sedas_iln, SQLCHAR, 17);
	dbClass.sqlout ((long *) &kuninka.kond_kun, SQLLONG, 0);
	dbClass.sqlout ((short *) &kuninka.kun_schema, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.plattform, SQLCHAR, 17);
	dbClass.sqlout ((char *) kuninka.be_log, SQLCHAR, 4);
	dbClass.sqlout ((long *) &kuninka.stat_kun, SQLLONG, 0);
	dbClass.sqlout ((char *) kuninka.ust_nummer, SQLCHAR, 25);
	dbClass.sqlout ((char *) kuninka.fak_kz, SQLCHAR, 2);
	dbClass.sqlout ((long *) &kuninka.fak_nr, SQLLONG, 0);
	dbClass.sqlout ((char *) kuninka.rechiln, SQLCHAR, 18);

	
	readabeding = (short) dbClass.sqlcursor ("select "

	" mdn, fil, kun, adr1, adr2, adr3, kun_seit, txt_nr1, frei_txt1, "
	" kun_krz1, kun_bran, kun_krz2, kun_krz3, kun_typ, bbn, pr_stu, "
	" pr_lst, vereinb, inka_nr, vertr1, vertr2, statk_period, a_period, "
	" sprache, txt_nr2, frei_txt2, freifeld1, freifeld2, tou, vers_art, "
	" lief_art, fra_ko_ber, rue_schei, form_typ1, auflage1, freifeld3, "
	" freifeld4, zahl_art, zahl_ziel, form_typ2, auflage2, txt_nr3, "
	" frei_txt3, nr_bei_rech, rech_st, sam_rech, einz_ausw, gut, rab_schl, "
	" bonus1, bonus2, tdm_grenz1, tdm_grenz2, jr_plan_ums, deb_kto, "
	" kred_lim, inka_zaehl, bank_kun, blz, kto_nr, hausbank, kun_of_po, "
	" kun_of_lf, kun_of_best, delstatus, kun_bran2, rech_fuss_txt, "
	" ls_fuss_txt, ust_id, rech_kopf_txt, ls_kopf_txt, gn_pkt_kz, sw_rab, "
	" bbs, inka_nr2, sw_fil_gr, sw_fil, ueb_kz, modif, kun_leer_kz,"
	" ust_id16, iln, waehrung, pr_ausw, pr_hier, pr_ausw_ls, "
	" pr_ausw_re, kun_gr1, kun_gr2, eg_kz, bonitaet, kred_vers, kst, "
	" edi_typ, sedas_dta, sedas_kz, sedas_umf, sedas_abr, sedas_gesch,"
	" sedas_satz, sedas_med, sedas_nam, sedas_abk1, sedas_abk2, sedas_abk3, "
	" sedas_nr1, sedas_nr2, sedas_nr3, sedas_vb1, sedas_vb2, sedas_vb3, "
	" sedas_iln, kond_kun, kun_schema, plattform, be_log, stat_kun, "
	" ust_nummer, fak_kz, fak_nr, rechiln "

	" from kun where sedas_dta = ? and mdn = ? and kun = inka_nr and edi_typ = \"E\" "
	" order by sedas_nr1, kun ") ;

//  sedas_nr1 ins order by nur wegen Gesamtleserei fuer alle Abkommen

// readcursor inkabread je Abkommen ( kuninka.sedas_nr1)

	dbClass.sqlin ((long *)	 &kuninka.sedas_dta, SQLLONG,0 );
	dbClass.sqlin ((short *) &kuninka.mdn, SQLSHORT, 0);
	dbClass.sqlin ((char *)   kuninka.sedas_nr1, SQLCHAR, 9);

	dbClass.sqlout ((short *) &kuninka.mdn, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.fil, SQLSHORT, 0);
	dbClass.sqlout ((long *) &kuninka.kun, SQLLONG, 0);
	dbClass.sqlout ((long *) &kuninka.adr1, SQLLONG, 0);
	dbClass.sqlout ((long *) &kuninka.adr2, SQLLONG, 0);  
	dbClass.sqlout ((long *) &kuninka.adr3, SQLLONG, 0); 
	dbClass.sqlout ((char *) kuninka.kun_seit, SQLCHAR, 11);
	dbClass.sqlout ((long *) &kuninka.txt_nr1, SQLLONG, 0);
	dbClass.sqlout ((char *) kuninka.frei_txt1, SQLCHAR, 65);

	dbClass.sqlout ((char *) kuninka.kun_krz1, SQLCHAR, 17);
	dbClass.sqlout ((char *) kuninka.kun_bran, SQLCHAR, 2);
	dbClass.sqlout ((char *) kuninka.kun_krz2, SQLCHAR, 17);
	dbClass.sqlout ((char *) kuninka.kun_krz3, SQLCHAR, 17);
	dbClass.sqlout ((short *) &kuninka.kun_typ, SQLSHORT, 0);
	dbClass.sqlout ((long *) &kuninka.bbn, SQLLONG, 0);
	dbClass.sqlout ((short *) &kuninka.pr_stu, SQLSHORT, 0);
	dbClass.sqlout ((long *) &kuninka.pr_lst, SQLLONG, 0);
	dbClass.sqlout ((char *) kuninka.vereinb, SQLCHAR, 6);
	dbClass.sqlout ((long *) &kuninka.inka_nr, SQLLONG, 0);
	dbClass.sqlout ((long *) &kuninka.vertr1, SQLLONG, 0);
	dbClass.sqlout ((long *) &kuninka.vertr2, SQLLONG, 0);
	dbClass.sqlout ((char *) kuninka.statk_period, SQLCHAR, 2);
	dbClass.sqlout ((char *) kuninka.a_period, SQLCHAR, 2);
	dbClass.sqlout ((short *) &kuninka.sprache, SQLSHORT, 0);
	dbClass.sqlout ((long *) &kuninka.txt_nr2, SQLLONG, 0);
	dbClass.sqlout ((char *) kuninka.frei_txt2, SQLCHAR, 65);
	dbClass.sqlout ((char *) kuninka.freifeld1, SQLCHAR, 9);
	dbClass.sqlout ((char *) kuninka.freifeld2, SQLCHAR, 9);
	dbClass.sqlout ((long *) &kuninka.tou, SQLLONG, 0);
	dbClass.sqlout ((char *) kuninka.vers_art, SQLCHAR, 3);
	dbClass.sqlout ((short *) &kuninka.lief_art, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.fra_ko_ber, SQLCHAR, 3);
	dbClass.sqlout ((short *) &kuninka.rue_schei, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.form_typ1, SQLCHAR, 3);
	dbClass.sqlout ((short *) &kuninka.auflage1, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.freifeld3, SQLCHAR, 9);
	dbClass.sqlout ((char *) kuninka.freifeld4, SQLCHAR, 9);
	dbClass.sqlout ((short *) &kuninka.zahl_art, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.zahl_ziel, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.form_typ2, SQLCHAR, 3);
	dbClass.sqlout ((short *) &kuninka.auflage2, SQLSHORT, 0);
	dbClass.sqlout ((long *) &kuninka.txt_nr3, SQLLONG, 0);
	dbClass.sqlout ((char *) kuninka.frei_txt3, SQLCHAR, 65);
	dbClass.sqlout ((char *) kuninka.nr_bei_rech, SQLCHAR, 17);
	dbClass.sqlout ((char *) kuninka.rech_st, SQLCHAR, 3);
	dbClass.sqlout ((short *) &kuninka.sam_rech, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.einz_ausw, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.gut, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.rab_schl, SQLCHAR, 9);

	dbClass.sqlout ((double *) &kuninka.bonus1, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &kuninka.bonus2, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &kuninka.tdm_grenz1, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &kuninka.tdm_grenz2, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &kuninka.jr_plan_ums, SQLDOUBLE, 0);
	dbClass.sqlout ((char *) kuninka.deb_kto, SQLCHAR, 9);
	dbClass.sqlout ((double *) &kuninka.kred_lim, SQLDOUBLE, 0);
	dbClass.sqlout ((short *) &kuninka.inka_zaehl, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.bank_kun, SQLCHAR, 37);
	dbClass.sqlout ((long *) &kuninka.blz, SQLLONG, 0);
	dbClass.sqlout ((char *) kuninka.kto_nr, SQLCHAR, 17);
	dbClass.sqlout ((short *) &kuninka.hausbank, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.kun_of_po, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.kun_of_lf, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.kun_of_best, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.delstatus, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.kun_bran2, SQLCHAR, 3);
	dbClass.sqlout ((long *) &kuninka.rech_fuss_txt, SQLLONG, 0);
	dbClass.sqlout ((long *) &kuninka.ls_fuss_txt, SQLLONG, 0);
	dbClass.sqlout ((char *) kuninka.ust_id, SQLCHAR, 12);
	dbClass.sqlout ((long *) &kuninka.rech_kopf_txt, SQLLONG, 0);
	dbClass.sqlout ((long *) &kuninka.ls_kopf_txt, SQLLONG, 0);
	dbClass.sqlout ((char *) kuninka.gn_pkt_kz, SQLCHAR, 2);
	dbClass.sqlout ((char *) kuninka.sw_rab, SQLCHAR, 2);
	dbClass.sqlout ((char *) kuninka.bbs, SQLCHAR, 9);
	dbClass.sqlout ((long *) &kuninka.inka_nr2, SQLLONG, 0);
	dbClass.sqlout ((short *) &kuninka.sw_fil_gr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.sw_fil, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.ueb_kz, SQLCHAR, 2);
	dbClass.sqlout ((char *) kuninka.modif, SQLCHAR, 2);
	dbClass.sqlout ((short *) &kuninka.kun_leer_kz, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.ust_id16, SQLCHAR, 17);
	dbClass.sqlout ((char *) kuninka.iln, SQLCHAR, 17);
	dbClass.sqlout ((short *) &kuninka.waehrung, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.pr_ausw, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.pr_hier, SQLCHAR, 2);
	dbClass.sqlout ((short *) &kuninka.pr_ausw_ls, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.pr_ausw_re, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.kun_gr1, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.kun_gr2, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.eg_kz, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.bonitaet, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.kred_vers, SQLSHORT, 0);
	dbClass.sqlout ((long *) &kuninka.kst, SQLLONG, 0);
	dbClass.sqlout ((char *) kuninka.edi_typ, SQLCHAR, 2);
	dbClass.sqlout ((long *) &kuninka.sedas_dta, SQLLONG, 0);
	dbClass.sqlout ((char *) kuninka.sedas_kz, SQLCHAR, 3);
	dbClass.sqlout ((short *) &kuninka.sedas_umf, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.sedas_abr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.sedas_gesch, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.sedas_satz, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.sedas_med, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.sedas_nam, SQLCHAR, 11);
	dbClass.sqlout ((short *) &kuninka.sedas_abk1, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.sedas_abk2, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.sedas_abk3, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.sedas_nr1, SQLCHAR, 9);
	dbClass.sqlout ((char *) kuninka.sedas_nr2, SQLCHAR, 9);
	dbClass.sqlout ((char *) kuninka.sedas_nr3, SQLCHAR, 9);
	dbClass.sqlout ((short *) &kuninka.sedas_vb1, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.sedas_vb2, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.sedas_vb3, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.sedas_iln, SQLCHAR, 17);
	dbClass.sqlout ((long *) &kuninka.kond_kun, SQLLONG, 0);
	dbClass.sqlout ((short *) &kuninka.kun_schema, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.plattform, SQLCHAR, 17);
	dbClass.sqlout ((char *) kuninka.be_log, SQLCHAR, 4);
	dbClass.sqlout ((long *) &kuninka.stat_kun, SQLLONG, 0);
	dbClass.sqlout ((char *) kuninka.ust_nummer, SQLCHAR, 25);
	dbClass.sqlout ((char *) kuninka.fak_kz, SQLCHAR, 2);
	dbClass.sqlout ((long *) &kuninka.fak_nr, SQLLONG, 0);
	dbClass.sqlout ((char *) kuninka.rechiln, SQLCHAR, 18);

	
	readbbeding = (short) dbClass.sqlcursor ("select "

	" mdn, fil, kun, adr1, adr2, adr3, kun_seit, txt_nr1, frei_txt1, "
	" kun_krz1, kun_bran, kun_krz2, kun_krz3, kun_typ, bbn, pr_stu, "
	" pr_lst, vereinb, inka_nr, vertr1, vertr2, statk_period, a_period, "
	" sprache, txt_nr2, frei_txt2, freifeld1, freifeld2, tou, vers_art, "
	" lief_art, fra_ko_ber, rue_schei, form_typ1, auflage1, freifeld3, "
	" freifeld4, zahl_art, zahl_ziel, form_typ2, auflage2, txt_nr3, "
	" frei_txt3, nr_bei_rech, rech_st, sam_rech, einz_ausw, gut, rab_schl, "
	" bonus1, bonus2, tdm_grenz1, tdm_grenz2, jr_plan_ums, deb_kto, "
	" kred_lim, inka_zaehl, bank_kun, blz, kto_nr, hausbank, kun_of_po, "
	" kun_of_lf, kun_of_best, delstatus, kun_bran2, rech_fuss_txt, "
	" ls_fuss_txt, ust_id, rech_kopf_txt, ls_kopf_txt, gn_pkt_kz, sw_rab, "
	" bbs, inka_nr2, sw_fil_gr, sw_fil, ueb_kz, modif, kun_leer_kz,"
	" ust_id16, iln, waehrung, pr_ausw, pr_hier, pr_ausw_ls, "
	" pr_ausw_re, kun_gr1, kun_gr2, eg_kz, bonitaet, kred_vers, kst, "
	" edi_typ, sedas_dta, sedas_kz, sedas_umf, sedas_abr, sedas_gesch,"
	" sedas_satz, sedas_med, sedas_nam, sedas_abk1, sedas_abk2, sedas_abk3, "
	" sedas_nr1, sedas_nr2, sedas_nr3, sedas_vb1, sedas_vb2, sedas_vb3, "
	" sedas_iln, kond_kun, kun_schema, plattform, be_log, stat_kun, "
	" ust_nummer , fak_kz, fak_nr , rechiln "

	" from kun where sedas_dta = ? and mdn = ? and sedas_nr1 = ? and kun = inka_nr and edi_typ = \"E\" "
	" order by kun ") ;


// readcursor inkcread fuer reli == 1

	dbClass.sqlin ((long *)	 &kuninka.sedas_dta, SQLLONG,0 );
	dbClass.sqlin ((short *) &kuninka.mdn, SQLSHORT, 0);
	dbClass.sqlin ((long *) &kuninka.kun, SQLLONG, 0);

	dbClass.sqlout ((short *) &kuninka.mdn, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.fil, SQLSHORT, 0);
	dbClass.sqlout ((long *) &kuninka.kun, SQLLONG, 0);
	dbClass.sqlout ((long *) &kuninka.adr1, SQLLONG, 0);
	dbClass.sqlout ((long *) &kuninka.adr2, SQLLONG, 0);  
	dbClass.sqlout ((long *) &kuninka.adr3, SQLLONG, 0); 
	dbClass.sqlout ((char *) kuninka.kun_seit, SQLCHAR, 11);
	dbClass.sqlout ((long *) &kuninka.txt_nr1, SQLLONG, 0);
	dbClass.sqlout ((char *) kuninka.frei_txt1, SQLCHAR, 65);

	dbClass.sqlout ((char *) kuninka.kun_krz1, SQLCHAR, 17);
	dbClass.sqlout ((char *) kuninka.kun_bran, SQLCHAR, 2);
	dbClass.sqlout ((char *) kuninka.kun_krz2, SQLCHAR, 17);
	dbClass.sqlout ((char *) kuninka.kun_krz3, SQLCHAR, 17);
	dbClass.sqlout ((short *) &kuninka.kun_typ, SQLSHORT, 0);
	dbClass.sqlout ((long *) &kuninka.bbn, SQLLONG, 0);
	dbClass.sqlout ((short *) &kuninka.pr_stu, SQLSHORT, 0);
	dbClass.sqlout ((long *) &kuninka.pr_lst, SQLLONG, 0);
	dbClass.sqlout ((char *) kuninka.vereinb, SQLCHAR, 6);
	dbClass.sqlout ((long *) &kuninka.inka_nr, SQLLONG, 0);
	dbClass.sqlout ((long *) &kuninka.vertr1, SQLLONG, 0);
	dbClass.sqlout ((long *) &kuninka.vertr2, SQLLONG, 0);
	dbClass.sqlout ((char *) kuninka.statk_period, SQLCHAR, 2);
	dbClass.sqlout ((char *) kuninka.a_period, SQLCHAR, 2);
	dbClass.sqlout ((short *) &kuninka.sprache, SQLSHORT, 0);
	dbClass.sqlout ((long *) &kuninka.txt_nr2, SQLLONG, 0);
	dbClass.sqlout ((char *) kuninka.frei_txt2, SQLCHAR, 65);
	dbClass.sqlout ((char *) kuninka.freifeld1, SQLCHAR, 9);
	dbClass.sqlout ((char *) kuninka.freifeld2, SQLCHAR, 9);
	dbClass.sqlout ((long *) &kuninka.tou, SQLLONG, 0);
	dbClass.sqlout ((char *) kuninka.vers_art, SQLCHAR, 3);
	dbClass.sqlout ((short *) &kuninka.lief_art, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.fra_ko_ber, SQLCHAR, 3);
	dbClass.sqlout ((short *) &kuninka.rue_schei, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.form_typ1, SQLCHAR, 3);
	dbClass.sqlout ((short *) &kuninka.auflage1, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.freifeld3, SQLCHAR, 9);
	dbClass.sqlout ((char *) kuninka.freifeld4, SQLCHAR, 9);
	dbClass.sqlout ((short *) &kuninka.zahl_art, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.zahl_ziel, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.form_typ2, SQLCHAR, 3);
	dbClass.sqlout ((short *) &kuninka.auflage2, SQLSHORT, 0);
	dbClass.sqlout ((long *) &kuninka.txt_nr3, SQLLONG, 0);
	dbClass.sqlout ((char *) kuninka.frei_txt3, SQLCHAR, 65);
	dbClass.sqlout ((char *) kuninka.nr_bei_rech, SQLCHAR, 17);
	dbClass.sqlout ((char *) kuninka.rech_st, SQLCHAR, 3);
	dbClass.sqlout ((short *) &kuninka.sam_rech, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.einz_ausw, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.gut, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.rab_schl, SQLCHAR, 9);

	dbClass.sqlout ((double *) &kuninka.bonus1, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &kuninka.bonus2, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &kuninka.tdm_grenz1, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &kuninka.tdm_grenz2, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &kuninka.jr_plan_ums, SQLDOUBLE, 0);
	dbClass.sqlout ((char *) kuninka.deb_kto, SQLCHAR, 9);
	dbClass.sqlout ((double *) &kuninka.kred_lim, SQLDOUBLE, 0);
	dbClass.sqlout ((short *) &kuninka.inka_zaehl, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.bank_kun, SQLCHAR, 37);
	dbClass.sqlout ((long *) &kuninka.blz, SQLLONG, 0);
	dbClass.sqlout ((char *) kuninka.kto_nr, SQLCHAR, 17);
	dbClass.sqlout ((short *) &kuninka.hausbank, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.kun_of_po, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.kun_of_lf, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.kun_of_best, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.delstatus, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.kun_bran2, SQLCHAR, 3);
	dbClass.sqlout ((long *) &kuninka.rech_fuss_txt, SQLLONG, 0);
	dbClass.sqlout ((long *) &kuninka.ls_fuss_txt, SQLLONG, 0);
	dbClass.sqlout ((char *) kuninka.ust_id, SQLCHAR, 12);
	dbClass.sqlout ((long *) &kuninka.rech_kopf_txt, SQLLONG, 0);
	dbClass.sqlout ((long *) &kuninka.ls_kopf_txt, SQLLONG, 0);
	dbClass.sqlout ((char *) kuninka.gn_pkt_kz, SQLCHAR, 2);
	dbClass.sqlout ((char *) kuninka.sw_rab, SQLCHAR, 2);
	dbClass.sqlout ((char *) kuninka.bbs, SQLCHAR, 9);
	dbClass.sqlout ((long *) &kuninka.inka_nr2, SQLLONG, 0);
	dbClass.sqlout ((short *) &kuninka.sw_fil_gr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.sw_fil, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.ueb_kz, SQLCHAR, 2);
	dbClass.sqlout ((char *) kuninka.modif, SQLCHAR, 2);
	dbClass.sqlout ((short *) &kuninka.kun_leer_kz, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.ust_id16, SQLCHAR, 17);
	dbClass.sqlout ((char *) kuninka.iln, SQLCHAR, 17);
	dbClass.sqlout ((short *) &kuninka.waehrung, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.pr_ausw, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.pr_hier, SQLCHAR, 2);
	dbClass.sqlout ((short *) &kuninka.pr_ausw_ls, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.pr_ausw_re, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.kun_gr1, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.kun_gr2, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.eg_kz, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.bonitaet, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.kred_vers, SQLSHORT, 0);
	dbClass.sqlout ((long *) &kuninka.kst, SQLLONG, 0);
	dbClass.sqlout ((char *) kuninka.edi_typ, SQLCHAR, 2);
	dbClass.sqlout ((long *) &kuninka.sedas_dta, SQLLONG, 0);
	dbClass.sqlout ((char *) kuninka.sedas_kz, SQLCHAR, 3);
	dbClass.sqlout ((short *) &kuninka.sedas_umf, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.sedas_abr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.sedas_gesch, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.sedas_satz, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.sedas_med, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.sedas_nam, SQLCHAR, 11);
	dbClass.sqlout ((short *) &kuninka.sedas_abk1, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.sedas_abk2, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.sedas_abk3, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.sedas_nr1, SQLCHAR, 9);
	dbClass.sqlout ((char *) kuninka.sedas_nr2, SQLCHAR, 9);
	dbClass.sqlout ((char *) kuninka.sedas_nr3, SQLCHAR, 9);
	dbClass.sqlout ((short *) &kuninka.sedas_vb1, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.sedas_vb2, SQLSHORT, 0);
	dbClass.sqlout ((short *) &kuninka.sedas_vb3, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.sedas_iln, SQLCHAR, 17);
	dbClass.sqlout ((long *) &kuninka.kond_kun, SQLLONG, 0);
	dbClass.sqlout ((short *) &kuninka.kun_schema, SQLSHORT, 0);
	dbClass.sqlout ((char *) kuninka.plattform, SQLCHAR, 17);
	dbClass.sqlout ((char *) kuninka.be_log, SQLCHAR, 4);
	dbClass.sqlout ((long *) &kuninka.stat_kun, SQLLONG, 0);
	dbClass.sqlout ((char *) kuninka.ust_nummer, SQLCHAR, 25);

	dbClass.sqlout ((char *) kuninka.fak_kz, SQLCHAR, 2);
	dbClass.sqlout ((long *) &kuninka.fak_nr, SQLLONG, 0);
	dbClass.sqlout ((char *) kuninka.rechiln, SQLCHAR, 18);

	readcbeding = (short) dbClass.sqlcursor ("select "

	" mdn, fil, kun, adr1, adr2, adr3, kun_seit, txt_nr1, frei_txt1, "
	" kun_krz1, kun_bran, kun_krz2, kun_krz3, kun_typ, bbn, pr_stu, "
	" pr_lst, vereinb, inka_nr, vertr1, vertr2, statk_period, a_period, "
	" sprache, txt_nr2, frei_txt2, freifeld1, freifeld2, tou, vers_art, "
	" lief_art, fra_ko_ber, rue_schei, form_typ1, auflage1, freifeld3, "
	" freifeld4, zahl_art, zahl_ziel, form_typ2, auflage2, txt_nr3, "
	" frei_txt3, nr_bei_rech, rech_st, sam_rech, einz_ausw, gut, rab_schl, "
	" bonus1, bonus2, tdm_grenz1, tdm_grenz2, jr_plan_ums, deb_kto, "
	" kred_lim, inka_zaehl, bank_kun, blz, kto_nr, hausbank, kun_of_po, "
	" kun_of_lf, kun_of_best, delstatus, kun_bran2, rech_fuss_txt, "
	" ls_fuss_txt, ust_id, rech_kopf_txt, ls_kopf_txt, gn_pkt_kz, sw_rab, "
	" bbs, inka_nr2, sw_fil_gr, sw_fil, ueb_kz, modif, kun_leer_kz,"
	" ust_id16, iln, waehrung, pr_ausw, pr_hier, pr_ausw_ls, "
	" pr_ausw_re, kun_gr1, kun_gr2, eg_kz, bonitaet, kred_vers, kst, "
	" edi_typ, sedas_dta, sedas_kz, sedas_umf, sedas_abr, sedas_gesch,"
	" sedas_satz, sedas_med, sedas_nam, sedas_abk1, sedas_abk2, sedas_abk3, "
	" sedas_nr1, sedas_nr2, sedas_nr3, sedas_vb1, sedas_vb2, sedas_vb3, "
	" sedas_iln, kond_kun, kun_schema, plattform, be_log, stat_kun, "
	" ust_nummer, fak_kz, fak_nr, rechiln "

	" from kun where sedas_dta = ? and mdn = ? and kun = ? and kun = inka_nr "
	" and edi_typ = \"E\" " ) ;

  }
