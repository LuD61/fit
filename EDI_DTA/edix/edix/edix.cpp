// edix.cpp : Definiert das Klassenverhalten f�r die Anwendung.
//

#include "stdafx.h"

#include "edix.h"
#include "edixDlg.h"
#include "dbClass.h"
#include "token.h"	// 150514
// #include "m_edix.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

DB_CLASS dbClass ;

// CedixApp

BEGIN_MESSAGE_MAP(CedixApp, CWinApp)
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()


// CedixApp-Erstellung

CedixApp::CedixApp()
{
	// TODO: Hier Code zur Konstruktion einf�gen
	// Alle wichtigen Initialisierungen in InitInstance positionieren
}


// Das einzige CedixApp-Objekt

CedixApp theApp;


// CedixApp Initialisierung

BOOL CedixApp::InitInstance()
{
	// InitCommonControls() ist f�r Windows XP erforderlich, wenn ein Anwendungsmanifest
	// die Verwendung von ComCtl32.dll Version 6 oder h�her zum Aktivieren
	// von visuellen Stilen angibt. Ansonsten treten beim Erstellen von Fenstern Fehler auf.
	InitCommonControls();

	CWinApp::InitInstance();

	AfxEnableControlContainer();

	CedixDlg dlg ;
	m_pMainWnd = &dlg;

	dbClass.opendbase("bws");

// 150514
	CCommandLineInfo cmdInfo;
    char *px;
	LPSTR CommandLine = GetCommandLine ();

	CToken Token (CommandLine, " ");

	char globdta[105] ;
	char globmdn[35] ;
	char globvdat[35] ;
	char globbdat[35] ;
	char globrdat[35] ;

	sprintf ( globdta, "0" ) ;
	sprintf ( globmdn, "0" ) ;
	sprintf ( globvdat, "X" ) ;
	sprintf ( globbdat, "X" ) ;
	sprintf ( globrdat, "X" ) ;

	// hier muessen dann mindestens 5 Parameter in dieser reihenfolge und diesem Format auftauchen :
	// mdn dta vdat bdat rdat    - die Datums in deutschem Datumsformat : "dd.mm.yyyy"

    Token.NextToken ();
	if ((px = Token.NextToken ()) != NULL)
	{
		if ( px[0] != '\0' && px[0] != ' ' )
				sprintf ( globmdn, "%s" , px ) ;

		if ((px = Token.NextToken ()) != NULL)
		{
			if ( px[0] != '\0' && px[0] != ' ' )
				sprintf ( globdta, "%s" , px ) ;

			if ((px = Token.NextToken ()) != NULL)
			{
				if ( px[0] != '\0' && px[0] != ' ' )
					sprintf ( globvdat, "%s" , px ) ;

				if ((px = Token.NextToken ()) != NULL)
				{
					if ( px[0] != '\0' && px[0] != ' ' )
						sprintf ( globbdat, "%s" , px ) ;

					if ((px = Token.NextToken ()) != NULL)
					{
						if ( px[0] != '\0' && px[0] != ' ' )
							sprintf ( globrdat, "%s" , px ) ;

					}
				}
			}
		
		}

	}

	dlg.paras ( globdta, globmdn , globvdat , globbdat , globrdat ) ;

// 150514 E

	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: F�gen Sie hier Code ein, um das Schlie�en des
		//  Dialogfelds �ber OK zu steuern
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: F�gen Sie hier Code ein, um das Schlie�en des
		//  Dialogfelds �ber "Abbrechen" zu steuern
	}

    dbClass.closedbase ("bws");

	// Da das Dialogfeld geschlossen wurde, FALSE zur�ckliefern, so dass wir die
	//  Anwendung verlassen, anstatt das Nachrichtensystem der Anwendung zu starten.
	return FALSE;
}
