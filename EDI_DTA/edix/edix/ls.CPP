#include "stdafx.h"
#include "DbClass.h"
#include "ls.h"

struct RETK retk,  retk_null;

extern DB_CLASS dbClass;
extern char * sqldatdstger( TIMESTAMP_STRUCT *dstakt, char *outpstr ) ;

static int anzzfelder ;

int RETK_CLASS::dbcount (void)
/**
Tabelle rech lesen.
**/
{

         if (test_upd_cursor == -1)
         {
             prepare ();
         }
         dbClass.sqlopen (count_cursor);
         dbClass.sqlfetch (count_cursor);
         if (sqlstatus == 0)
         {
                      return anzzfelder;
         }
         return sqlstatus;

}


int RETK_CLASS::leseretk (void)
{
	int di = dbClass.sqlfetch (readcursor);
	if ( ! di )
	{
		sqldatdstger ( &retk.ts_ret_dat, retk.ret_dat ) ;
		sqldatdstger ( &retk.ts_lieferdat, retk.lieferdat ) ;
	}
	return di;
}
int RETK_CLASS::openretk (void )
{

	if ( readcursor < 0 ) prepare ();	
// hier NOCH keine Datumskonvertierung notwendig 
	int di =  dbClass.sqlopen (readcursor);
	return di ;
}


void RETK_CLASS::prepare (void)
{

	dbClass.sqlin ((long *)  &retk.rech, SQLLONG, 0);
	dbClass.sqlin ((short *) &retk.mdn, SQLSHORT, 0);
    dbClass.sqlin ((char *) retk.blg_typ, SQLCHAR, 2);

	dbClass.sqlout ((short *) &retk.mdn, SQLSHORT, 0 ) ;
	dbClass.sqlout ((short *) &retk.fil, SQLSHORT, 0 ) ; 
	dbClass.sqlout ((long *)  &retk.ret, SQLLONG, 0 ) ;
	dbClass.sqlout ((long *)  &retk.ls, SQLLONG, 0 ) ;  
	dbClass.sqlout ((short *) &retk.kun_fil, SQLSHORT, 0 ) ; 
	dbClass.sqlout ((char *)   retk.feld_bz1, SQLCHAR, 20 ) ;  
	dbClass.sqlout ((long *)  &retk.kun, SQLLONG, 0 ) ; 
// XDATUM 	dbClass.sqlout ((char *)   retk.ret_dat, SQLCHAR, 11 ) ;
 	dbClass.sqlout ((TIMESTAMP_STRUCT *) &retk.ts_ret_dat, SQLTIMESTAMP, 26 ) ;

// XDATUM 	dbClass.sqlout ((char *)   retk.lieferdat, SQLCHAR, 11 ) ;
 	dbClass.sqlout ((TIMESTAMP_STRUCT *) &retk.ts_lieferdat, SQLTIMESTAMP, 26 ) ;
	dbClass.sqlout ((char *)   retk.hinweis, SQLCHAR, 49 ) ;
	dbClass.sqlout ((short *) &retk.ret_stat, SQLSHORT, 0 ) ;
	dbClass.sqlout ((char *)   retk.feld_bz2, SQLCHAR, 12 ) ;
	dbClass.sqlout ((char *)   retk.kun_krz1, SQLCHAR, 17 ) ;
	dbClass.sqlout ((char *)   retk.feld_bz3, SQLCHAR, 8 ) ;
	dbClass.sqlout ((long *)  &retk.adr, SQLLONG, 0 ) ;
	dbClass.sqlout ((short *) &retk.delstatus, SQLSHORT, 0 ) ;
	dbClass.sqlout ((long *)  &retk.rech, SQLLONG, 0 ) ;
	dbClass.sqlout ((char *)   retk.blg_typ, SQLCHAR, 2 ) ;
	dbClass.sqlout ((long *)  &retk.kopf_txt, SQLLONG, 0 ) ;
	dbClass.sqlout ((long *)  &retk.fuss_txt, SQLLONG, 0 ) ;
	dbClass.sqlout ((long *)  &retk.vertr, SQLLONG, 0 ) ;
	dbClass.sqlout ((long *)  &retk.inka_nr, SQLLONG, 0 ) ;
	dbClass.sqlout ((double *)&retk.of_po, SQLDOUBLE, 0 ) ;
	dbClass.sqlout ((short *) &retk.teil_smt, SQLSHORT, 0 ) ; 
	dbClass.sqlout ((char *)   retk.pers_nam, SQLCHAR, 9 ) ;
	dbClass.sqlout ((double *)&retk.of_ek, SQLDOUBLE, 0 ) ;
	dbClass.sqlout ((double *)&retk.of_po_euro, SQLDOUBLE, 0 ) ;
	dbClass.sqlout ((double *)&retk.of_po_fremd, SQLDOUBLE, 0 ) ;
	dbClass.sqlout ((double *)&retk.of_ek_euro, SQLDOUBLE, 0 ) ;
	dbClass.sqlout ((double *)&retk.of_ek_fremd, SQLDOUBLE, 0 ) ;
	dbClass.sqlout ((short *) &retk.waehrung, SQLSHORT, 0 ) ;
	dbClass.sqlout ((short *) &retk.ret_grund, SQLSHORT, 0 ) ;
	dbClass.sqlout ((long *)  &retk.gruppe, SQLLONG, 0 ) ;
	dbClass.sqlout ((char *)   retk.ueb_kz, SQLCHAR, 2 ) ;


	readcursor = (short) dbClass.sqlcursor ("select "

	" mdn ,fil ,ret ,ls ,kun_fil ,feld_bz1 ,kun ,ret_dat , lieferdat "
	" ,hinweis ,ret_stat ,feld_bz2 ,kun_krz1 ,feld_bz3 ,adr ,delstatus "
	" ,rech ,blg_typ ,kopf_txt ,fuss_txt ,vertr ,inka_nr ,of_po ,teil_smt " 
	" ,pers_nam ,of_ek ,of_po_euro ,of_po_fremd ,of_ek_euro ,of_ek_fremd "
	" ,waehrung ,ret_grund ,gruppe ,ueb_kz "
	" from retk where rech = ? and mdn = ? and blg_typ = ? " );

}

