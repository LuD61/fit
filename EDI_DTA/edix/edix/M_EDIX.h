#ifndef _m_edix_firmen

#define _m_edix_firmen

#define EDEKA            1 
#define KARSTADT         2
#define KAUFRING         3
#define MARKANT          4
#define METRO            5
#define REWE             6
#define TENGELMANN       7
#define EK_GH_EG		 8
#define ZIMBO            9

#define CHRIST          10
#define SPAR            11
#define WALMART      14
// definitionen geglaettet am 070409
#define KAUFLAND        12
#define KONSUM          13

#define FENEBERG		15	// 130313

#define METROSK			530	// 060514
#define METROCZ			531	// 060514


// Lidl dazu am 101111 - bitte ptabn immer synchron halten, sonst spinnt alles .....
#define LIDL			500	// interne ID !!!!!
#define LIDLGERMANY		501	// alpha real.
#define LIDLAUSTRIA		502	// alpha basis-real.
#define LIDLFINLAND		503
#define LIDLDANMARK		504
#define LIDLENGLAND		505
#define LIDLHUNGARY		506	
#define LIDLIRELAND		507
#define LIDLNETHERLAND	508	// alpha
#define LIDLPOLAND		509
#define LIDLSPAIN		510
#define LIDLSWEDEN		511

// 190912
#define LIDLNORDIRE		512		
#define LIDLGREECE		513
#define LIDLCYPRUS		514	
#define LIDLSCHWEIZ		515	





#define NFIT			"fit"
#define NEDEKA          "edek" 
#define NKARSTADT       "kars"
#define NKAUFRING       "kauf"
#define NMARKANT        "mark"
#define NFENEBERG       "fenb"
#define NMETRO          "metr"
#define NREWE           "rewe"
#define NTENGELMANN     "teng"
#define NEK_GH_EG		"ek_g"
#define NZIMBO          "zimb"
#define NKAUFLAND       "kafl"


#define NCHRIST         "chri"
#define NSPAR           "spar"
#define NWALMART        "walm"

#define NLIDL			"lidl"	// interne ID !!!!!
#define NLIDLGERMANY		"li501"		// DE
#define NLIDLAUSTRIA		"li502"		// AT
#define NLIDLFINLAND		"li503"		// FI
#define NLIDLDANMARK		"li504"		// DK
#define NLIDLENGLAND		"li505"		// GB
#define NLIDLHUNGARY		"li506"		// HU
#define NLIDLIRELAND		"li507"		// IE
#define NLIDLNETHERLAND		"li508"		// NL
#define NLIDLPOLAND			"li509"		// PL
#define NLIDLSPAIN			"li510"		// ES
#define NLIDLSWEDEN			"li511"		// SE

// 190912 

#define NLIDLNORDIRE		"li512"		// NI
#define NLIDLGREECE			"li513"		// GR
#define NLIDLCYPRUS			"li514"		// CY
#define NLIDLSCHWEIZ		"li515"		// CH

#define NMETROSK			"li530"
#define NMETROCZ			"li531"




#define WAS_ANDERES     999
#define KAUFHALLE       1

#define EINZELK33       33




#define    TRENN_SE         "'"       // Segmentende
#define    TRENN_DE         "+"       // Datenelemente+Segmentbezeichner
#define    TRENN_GD         ":"       // Gruppendatenelemente
#define    TRENN_MA         "?"       // Maskierungs(freigabe-)zeichen
#define    TRENN_DZ         "."       // Dezimal-Trenner

extern char * GeneriereDatei(char *, short);	// 150514 : internmdn durchreichen

extern int UNA_segment (void);

extern int UNB_segment(void);

extern int UNZ_segment(int anz_nachricht);

extern int UNH_segment( int ref_nr, char * nach_typ );

extern int UNT_segment(int ref_nr);

extern int PAI_segment(char * pai_typ);	// 060514

extern int PAT_segment(char * pat_wert);	// 140714

extern int DTM_segment( char * ptyp, char *pdatum, int pformat );

extern int DTMx_segment( char * ptyp, char *pvdatum, char *pbdatum, int pformat ) ;

extern int BGM_segment( char * ctyp, long blg_nr, int org_kop , char * zutext );	// 081009

extern int RFF_segment( char * ctyp, char * blg_nr);

extern int CTA_segment (char * ctyp, char *abtnr );

extern int NAD_segment( char * ctyp, char * diln, long dadr , char * landkenn);	// 140714 Lšnderkennung dazu

extern int TAX_segment( char *taxwert);

extern int CUX_segment(void);

extern int FII_segment(long);	// 140714

extern int LIN_segment( long ibel_pos, char *iart_ean , long basislin );	// 090508

extern int PIA_segment (double interna, char *externa);

extern int IMD_segment ( char * ityp, char * ityp2, char * freitext , char * marka9 );	// 230408 : 4. Parameter

extern int IMDx_segment ( char * ityp,  char * freitext ) ;		// 140714

extern int QTY_segment( char * ityp, double menge, short ime_einh );

extern int MOA_segment( char * ityp, double iwert );

extern int PCD_segment( char * ityp, double iwert );

extern int ALC_segment( char * abzu, char * absprache, char *kalkstufe, char *rabart);

extern int PRI_segment ( char * ityp, double iwert, short ime_einh);

extern int MEA_segment( char * ityp, float iwert );

extern int FTX_segment ( char * textzuordnung,/* ZZZ Kopf oder SUR Fuss */
				 char * verarb_hinw, char * schluessel,
				 char * text1, char * text2, char * text3,
				 char * text4, char * text5, char * sprache);

extern int UNS_segment( char * ityp );

extern int CNT_segment( long );	// 140714

extern int Kopfteil(void);

extern int Fussteil(void);

extern long dta_echt;	// 101111

extern double wechselkurs;	// 140714

#endif
