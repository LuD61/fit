#include "stdafx.h"
#include "DbClass.h"
#include "rech.h"
#include "kun.h"

struct RECH rech,  rech_null;
struct RECH_P rech_p,  rech_p_null;
struct RECH_VBD rech_vbd,  rech_vbd_null;

extern DB_CLASS dbClass;

extern char * sqldatdstger(TIMESTAMP_STRUCT *dstakt, char *outpstr) ;

char cvon_datum[11];
char cbis_datum[11];
char crel_datum[11];

TIMESTAMP_STRUCT ts_von_datum;
TIMESTAMP_STRUCT ts_bis_datum;
TIMESTAMP_STRUCT ts_rel_datum;

static int anzzfelder ;

int RECH_CLASS::dbcount (void)
/**
Tabelle rech lesen.
**/
{

         if (test_upd_cursor == -1)
         {
             prepare ();
         }
         dbClass.sqlopen (count_cursor);
         dbClass.sqlfetch (count_cursor);
         if (sqlstatus == 0)
         {
                      return anzzfelder;
         }
         return sqlstatus;

}


int RECH_CLASS::leserech (char typ)
{
	int di ;
	switch ( typ)
	{
	case 'a' : di = dbClass.sqlfetch (readabeding);
		break ;
	case 'b' : di = dbClass.sqlfetch (readbbeding);	// eingrenzen nach teil_smt
		break ;
	case 'c' : di = dbClass.sqlfetch (readcbeding);	// was halt zu einer RELI gehoert
		break ;
	default : di = dbClass.sqlfetch (readcursor);
		break ;
	}
	if (!di )
	{
		sqldatdstger( &rech.ts_rech_dat , rech.rech_dat );
		sqldatdstger( &rech.ts_rvdat , rech.rvdat );
	}
	return di;
}
int RECH_VBD_CLASS::leserech_vbd (void)
{
	int di = dbClass.sqlfetch (readcursor);
	if (!di )
	{
// es gibt keine Datumsfelder 
	}
	return di;
}

int RECH_VBD_CLASS::openrech_vbd (void )
{

	if ( readcursor < 0 ) prepare ();	
	int di =  dbClass.sqlopen (readcursor);
	return di ;
}

int RECH_P_CLASS::leserech_p (void)
{
	int di = dbClass.sqlfetch (readcursor);
	if ( ! di )
	{
		sqldatdstger ( &rech_p.ts_lieferdat, rech_p.lieferdat ) ;
	}
		return di;
}

int RECH_P_CLASS::openrech_p (void )
{

	if ( readcursor < 0 ) prepare ();
	int di =  dbClass.sqlopen (readcursor);
	return di ;
}

int RECH_CLASS::updvbd (long ndta )
{
	longdta = ndta ;

	if ( test_upd_cursor < 0 ) prepare ();	
	int di =  dbClass.sqlexecute (test_upd_cursor);
	return di ;
}


int RECH_CLASS::statusupdate (void )
{

	if ( readcursor < 0 ) prepare ();	
	int di =  dbClass.sqlexecute (upd_cursor);
	return di ;
}

int RECH_CLASS::openrech (char typ )
{

		if ( readcursor < 0 ) prepare ();	

		switch ( typ)
		{
		case 'a' :  return dbClass.sqlopen (readabeding);	//  je Inkasso und Zeitraum
		case 'b' :  return dbClass.sqlopen (readbbeding);	// zusaetzl. eingegrenzt nach teil_smt
		case 'c' :  return dbClass.sqlopen (readcbeding);	// zu einer Rechnungsliste
		default  :  return dbClass.sqlopen (readcursor);	// Cursor je Beleg
		}
		return 0 ;	// syntax-dummy
}

void RECH_CLASS::prepare (void)
{

	dbClass.sqlin ((long *) &rech.rech_vbd, SQLLONG, 0);
// XDATUM	dbClass.sqlin ((char *)  rech.rvdat, SQLCHAR, 11);
	dbClass.sqlin ((TIMESTAMP_STRUCT *)  &rech.ts_rvdat, SQLTIMESTAMP, 26);

	dbClass.sqlin ((short *) &rech.mdn, SQLSHORT, 0);
// XDATUM	dbClass.sqlin ((char *)  cvon_datum, SQLCHAR, 11);
// XDATUM   dbClass.sqlin ((char *)  cbis_datum, SQLCHAR, 11);
	dbClass.sqlin ((TIMESTAMP_STRUCT *)  &ts_von_datum, SQLTIMESTAMP, 26);
	dbClass.sqlin ((TIMESTAMP_STRUCT *)  &ts_bis_datum, SQLTIMESTAMP, 26);

	dbClass.sqlin ((short *) &rech.mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &rech.mdn, SQLSHORT, 0);

	dbClass.sqlin ((long *) &longdta, SQLLONG, 0);

	test_upd_cursor = (short) dbClass.sqlcursor ("update rech set rech.rech_vbd = ? , rech.rvdat = ? "
	" where rech.mdn = ? and rech.rech_dat between ? and ? "
	" and rech.blg_typ in ( \"R\" , \"G\" ) "
	" and rech.delstatus = 0 and rech.kun in "
	" ( select kun.kun from kun where kun.mdn = ? "
	"     and kun.fil = 0 and kun.inka_nr in "
	    " ( select kun.kun from kun where kun.mdn = ? "
	    "   and kun.fil = 0 "
	    "   and kun.inka_nr = kun.kun "
	    "   and kun.edi_typ = \"E\" "
	    "   and kun.sedas_dta = ? ) "
	" ) "    );
	
	dbClass.sqlin ((long *)  &rech.rech_nr, SQLLONG, 0);
	dbClass.sqlin ((short *) &rech.mdn, SQLSHORT, 0);
    dbClass.sqlin ((char *)   rech.blg_typ, SQLCHAR, 2);
// XDATUM	dbClass.sqlin ((char *)   rech.rech_dat, SQLCHAR, 11);
	dbClass.sqlin ((TIMESTAMP_STRUCT *)  &rech.ts_rech_dat, SQLTIMESTAMP, 26);

	upd_cursor = (short) dbClass.sqlcursor ("update rech set delstatus = 1 "
	"where rech.rech_nr = ? and rech.mdn = ? and rech.blg_typ = ? and rech.rech_dat = ? ");
	
	dbClass.sqlin ((long *)  &rech.rech_nr, SQLLONG, 0);
	dbClass.sqlin ((short *) &rech.mdn, SQLSHORT, 0);
    dbClass.sqlin ((char *) rech.blg_typ, SQLCHAR, 2);

	dbClass.sqlout ((long *) &anzzfelder, SQLLONG, 0);

    count_cursor = (short) dbClass.sqlcursor ("select count(*) from rech "
					"where rech.rech_nr = ? and rech.mdn = ? and rech.blg_typ = ? ");
										

	dbClass.sqlin ((long *)  &rech.rech_nr, SQLLONG, 0);
	dbClass.sqlin ((short *) &rech.mdn, SQLSHORT, 0);
    dbClass.sqlin ((char *)  rech.blg_typ, SQLCHAR, 2);


	dbClass.sqlout ((short *)	&rech.delstatus,SQLSHORT,0 ) ;
    dbClass.sqlout ((short *)	&rech.fil,SQLSHORT,0 ) ;
    dbClass.sqlout ((long *)	&rech.kun,SQLLONG,0 ) ;
    dbClass.sqlout ((short *)	&rech.mdn,SQLSHORT,0 ) ;
    dbClass.sqlout ((long *)	&rech.rech_nr,SQLLONG,0 ) ;
// XDATUM	dbClass.sqlout ((char *)	 rech.rech_dat,SQLCHAR, 11 ) ;
	dbClass.sqlout ((TIMESTAMP_STRUCT *)  &rech.ts_rech_dat, SQLTIMESTAMP, 26);

    dbClass.sqlout ((long *)	&rech.rech_fuss_txt,SQLLONG,0 ) ;
    dbClass.sqlout ((long *)	&rech.rech_kopf_txt,SQLLONG,0 ) ;
    dbClass.sqlout ((double *)	&rech.rech_sum_bto,SQLDOUBLE,0 ) ;
	dbClass.sqlout ((double *)	&rech.rech_sum_nto,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_end_rab,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_bez,SQLDOUBLE,0) ;
    dbClass.sqlout ((char *)	 rech.rech_stat,SQLCHAR,2);
    dbClass.sqlout ((char *)	 rech.blg_typ,SQLCHAR,2);
    dbClass.sqlout ((char *)	 rech.krz_txt,SQLCHAR,17);
    dbClass.sqlout ((double *)	&rech.skto,SQLDOUBLE,0) ;
    dbClass.sqlout ((short *)	&rech.zahl_art,SQLSHORT,0) ;
    dbClass.sqlout ((char *)	 rech.zahl_dat,SQLCHAR,11);
    dbClass.sqlout ((short *)	&rech.waehrung,SQLSHORT,0) ;
    dbClass.sqlout ((double *)	&rech.rech_bto_eur,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_nto_eur,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_rab_eur,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_bez_eur,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_bto_fremd,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_nto_fremd,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_rab_fremd,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_bez_fremd,SQLDOUBLE,0) ;
    dbClass.sqlout ((char *)	 rech.ueb_kz,SQLCHAR,2) ;
    dbClass.sqlout ((long *)	&rech.geld_kto,SQLLONG,0) ;
    dbClass.sqlout ((long *)	&rech.teil_smt,SQLLONG,0) ;
    dbClass.sqlout ((long *)	&rech.abkommen,SQLLONG,0) ;
    dbClass.sqlout ((double *)	&rech.rerabproz,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_rab,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_rab1,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_rab2,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.netto,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.netto1,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.netto2,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.mwst1,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.mwst2,SQLDOUBLE,0) ;
    dbClass.sqlout ((short *)	&rech.sam_rech,SQLSHORT,0) ;
    dbClass.sqlout ((long *)	&rech.rech_vbd,SQLLONG,0) ;		// 190706 
// XDATUM    dbClass.sqlout ((char *)	 rech.rvdat,SQLCHAR,11) ;		// 190706
	dbClass.sqlout ((TIMESTAMP_STRUCT *)  &rech.ts_rvdat, SQLTIMESTAMP, 26);

	readcursor = (short) dbClass.sqlcursor ("select "
    " delstatus, fil, kun, mdn, rech_nr, rech_dat, rech_fuss_txt, rech_kopf_txt "
    " ,rech_sum_bto, rech_sum_nto, rech_end_rab, rech_bez, rech_stat, blg_typ "
    " ,krz_txt, skto, zahl_art, zahl_dat, waehrung, rech_bto_eur, rech_nto_eur "
    " ,rech_rab_eur, rech_bez_eur, rech_bto_fremd, rech_nto_fremd, rech_rab_fremd "
    " ,rech_bez_fremd, ueb_kz, geld_kto, teil_smt, abkommen, rerabproz, rech_rab "
	" ,rech_rab1, rech_rab2, netto, netto1, netto2, mwst1, mwst2, sam_rech "
	" ,rech_vbd ,rvdat "

	" from rech where rech_nr = ? and mdn = ? and blg_typ = ? " ) ;

// Cursor readabeding : 
// hole Rechnungen und GS je Inkasso und Datumsbereich

// Cursor readbbeding : 
// hole Rechnungen und GS je Inkasso, teil_smt und Datumsbereich

	dbClass.sqlin ((short *) &rech.mdn, SQLSHORT, 0);
    dbClass.sqlin ((long *)  &rech.kun, SQLLONG, 0);
// XDATUM	dbClass.sqlin ((char *)  cvon_datum, SQLCHAR, 11);
// XDATUM    dbClass.sqlin ((char *)  cbis_datum, SQLCHAR, 11);
	dbClass.sqlin ((TIMESTAMP_STRUCT *)  &ts_von_datum, SQLTIMESTAMP, 26);
	dbClass.sqlin ((TIMESTAMP_STRUCT *)  &ts_bis_datum, SQLTIMESTAMP, 26);


    dbClass.sqlout ((short *)	&rech.delstatus,SQLSHORT,0 ) ;
    dbClass.sqlout ((short *)	&rech.fil,SQLSHORT,0 ) ;
    dbClass.sqlout ((long *)	&rech.kun,SQLLONG,0 ) ;
    dbClass.sqlout ((short *)	&rech.mdn,SQLSHORT,0 ) ;
    dbClass.sqlout ((long *)	&rech.rech_nr,SQLLONG,0 ) ;
//    dbClass.sqlout ((char *)	 rech.rech_dat,SQLCHAR, 11 ) ;
	dbClass.sqlout ((TIMESTAMP_STRUCT *)  &rech.ts_rech_dat, SQLTIMESTAMP, 26);

    dbClass.sqlout ((long *)	&rech.rech_fuss_txt,SQLLONG,0 ) ;
    dbClass.sqlout ((long *)	&rech.rech_kopf_txt,SQLLONG,0 ) ;
    dbClass.sqlout ((double *)	&rech.rech_sum_bto,SQLDOUBLE,0 ) ;
    dbClass.sqlout ((double *)	&rech.rech_sum_nto,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_end_rab,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_bez,SQLDOUBLE,0) ;
    dbClass.sqlout ((char *)	 rech.rech_stat,SQLCHAR,2);
    dbClass.sqlout ((char *)	 rech.blg_typ,SQLCHAR,2);
    dbClass.sqlout ((char *)	 rech.krz_txt,SQLCHAR,17);
    dbClass.sqlout ((double *)	&rech.skto,SQLDOUBLE,0) ;
    dbClass.sqlout ((short *)	&rech.zahl_art,SQLSHORT,0) ;
    dbClass.sqlout ((char *)	 rech.zahl_dat,SQLCHAR,11);
    dbClass.sqlout ((short *)	&rech.waehrung,SQLSHORT,0) ;
    dbClass.sqlout ((double *)	&rech.rech_bto_eur,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_nto_eur,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_rab_eur,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_bez_eur,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_bto_fremd,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_nto_fremd,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_rab_fremd,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_bez_fremd,SQLDOUBLE,0) ;
    dbClass.sqlout ((char *)	 rech.ueb_kz,SQLCHAR,2) ;
    dbClass.sqlout ((long *)	&rech.geld_kto,SQLLONG,0) ;
    dbClass.sqlout ((long *)	&rech.teil_smt,SQLLONG,0) ;
    dbClass.sqlout ((long *)	&rech.abkommen,SQLLONG,0) ;
    dbClass.sqlout ((double *)	&rech.rerabproz,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_rab,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_rab1,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_rab2,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.netto,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.netto1,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.netto2,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.mwst1,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.mwst2,SQLDOUBLE,0) ;
    dbClass.sqlout ((short *)	&rech.sam_rech,SQLSHORT,0) ;
    dbClass.sqlout ((long *)	&rech.rech_vbd,SQLLONG,0) ;		// 190706 
// XDATUM    dbClass.sqlout ((char *)	 rech.rvdat,SQLCHAR,11) ;		// 190706
	dbClass.sqlout ((TIMESTAMP_STRUCT *)  &rech.ts_rvdat, SQLTIMESTAMP, 26);

	readabeding = (short) dbClass.sqlcursor ("select "

    " delstatus, fil, kun, mdn, rech_nr, rech_dat, rech_fuss_txt, rech_kopf_txt "
    " ,rech_sum_bto, rech_sum_nto, rech_end_rab, rech_bez, rech_stat, blg_typ "
    " ,krz_txt, skto, zahl_art, zahl_dat, waehrung, rech_bto_eur, rech_nto_eur "
    " ,rech_rab_eur, rech_bez_eur, rech_bto_fremd, rech_nto_fremd, rech_rab_fremd "
    " ,rech_bez_fremd, ueb_kz, geld_kto, teil_smt, abkommen, rerabproz, rech_rab "
	" ,rech_rab1, rech_rab2, netto, netto1, netto2, mwst1, mwst2, sam_rech "
	" ,rech_vbd ,rvdat "
	" from rech "

	" where rech.mdn = ? "
	" and rech.fil = 0 and rech.delstatus = 0 and rech.rech_nto_eur <> 0 "

	" and rech.kun in "
	" ( select distinct kun.kun from kun where kun.inka_nr = ? and kun.mdn = rech.mdn )"

	" and rech.rech_dat between  ? and ? "

	" and ( rech.blg_typ = \"R\" or rech.blg_typ = \"G\") " 
	" order by rech.rech_nr " ) ;

	
// Cursor readbbeding : 

	dbClass.sqlin ((short *) &rech.mdn, SQLSHORT, 0);
    dbClass.sqlin ((long *)  &rech.teil_smt, SQLLONG, 0);
    dbClass.sqlin ((long *)  &rech.kun, SQLLONG, 0);
// XDATUM	dbClass.sqlin ((char *)  cvon_datum, SQLCHAR, 11);
// XDATUM    dbClass.sqlin ((char *)  cbis_datum, SQLCHAR, 11);
	dbClass.sqlin ((TIMESTAMP_STRUCT *)  &ts_von_datum, SQLTIMESTAMP, 26);
	dbClass.sqlin ((TIMESTAMP_STRUCT *)  &ts_bis_datum, SQLTIMESTAMP, 26);


    dbClass.sqlout ((short *)	&rech.delstatus,SQLSHORT,0 ) ;
    dbClass.sqlout ((short *)	&rech.fil,SQLSHORT,0 ) ;
    dbClass.sqlout ((long *)	&rech.kun,SQLLONG,0 ) ;
    dbClass.sqlout ((short *)	&rech.mdn,SQLSHORT,0 ) ;
    dbClass.sqlout ((long *)	&rech.rech_nr,SQLLONG,0 ) ;
// XDATUM    dbClass.sqlout ((char *)	 rech.rech_dat,SQLCHAR, 11 ) ;
	dbClass.sqlout ((TIMESTAMP_STRUCT *)  &rech.ts_rech_dat, SQLTIMESTAMP, 26);

	dbClass.sqlout ((long *)	&rech.rech_fuss_txt,SQLLONG,0 ) ;
    dbClass.sqlout ((long *)	&rech.rech_kopf_txt,SQLLONG,0 ) ;
    dbClass.sqlout ((double *)	&rech.rech_sum_bto,SQLDOUBLE,0 ) ;
    dbClass.sqlout ((double *)	&rech.rech_sum_nto,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_end_rab,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_bez,SQLDOUBLE,0) ;
    dbClass.sqlout ((char *)	 rech.rech_stat,SQLCHAR,2);
    dbClass.sqlout ((char *)	 rech.blg_typ,SQLCHAR,2);
    dbClass.sqlout ((char *)	 rech.krz_txt,SQLCHAR,17);
    dbClass.sqlout ((double *)	&rech.skto,SQLDOUBLE,0) ;
    dbClass.sqlout ((short *)	&rech.zahl_art,SQLSHORT,0) ;
    dbClass.sqlout ((char *)	 rech.zahl_dat,SQLCHAR,11);
    dbClass.sqlout ((short *)	&rech.waehrung,SQLSHORT,0) ;
    dbClass.sqlout ((double *)	&rech.rech_bto_eur,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_nto_eur,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_rab_eur,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_bez_eur,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_bto_fremd,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_nto_fremd,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_rab_fremd,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_bez_fremd,SQLDOUBLE,0) ;
    dbClass.sqlout ((char *)	 rech.ueb_kz,SQLCHAR,2) ;
    dbClass.sqlout ((long *)	&rech.geld_kto,SQLLONG,0) ;
    dbClass.sqlout ((long *)	&rech.teil_smt,SQLLONG,0) ;
    dbClass.sqlout ((long *)	&rech.abkommen,SQLLONG,0) ;
    dbClass.sqlout ((double *)	&rech.rerabproz,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_rab,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_rab1,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_rab2,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.netto,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.netto1,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.netto2,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.mwst1,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.mwst2,SQLDOUBLE,0) ;
    dbClass.sqlout ((short *)	&rech.sam_rech,SQLSHORT,0) ;
    dbClass.sqlout ((long *)	&rech.rech_vbd,SQLLONG,0) ;
// XDATUM    dbClass.sqlout ((char *)	 rech.rvdat,SQLCHAR,11) ;
	dbClass.sqlout ((TIMESTAMP_STRUCT *)  &rech.ts_rvdat, SQLTIMESTAMP, 26);


	readbbeding = (short) dbClass.sqlcursor ("select "

    " delstatus, fil, kun, mdn, rech_nr, rech_dat, rech_fuss_txt, rech_kopf_txt "
    " ,rech_sum_bto, rech_sum_nto, rech_end_rab, rech_bez, rech_stat, blg_typ "
    " ,krz_txt, skto, zahl_art, zahl_dat, waehrung, rech_bto_eur, rech_nto_eur "
    " ,rech_rab_eur, rech_bez_eur, rech_bto_fremd, rech_nto_fremd, rech_rab_fremd "
    " ,rech_bez_fremd, ueb_kz, geld_kto, teil_smt, abkommen, rerabproz, rech_rab "
	" ,rech_rab1, rech_rab2, netto, netto1, netto2, mwst1, mwst2, sam_rech "
	" ,rech_vbd ,rvdat "
	" from rech where rech.mdn = ? "
	" and rech.fil = 0 and rech.delstatus = 0 and rech.rech_nto_eur <> 0 "
	" and rech.teil_smt = ? "
	" and rech.kun in ( select distinct kun.kun from kun " 
		" where kun.inka_nr = ? and kun.mdn = rech.mdn )"
		" and rech.rech_dat between ? and ? "
		" and ( rech.blg_typ = \"R\" or rech.blg_typ = \"G\" ) "  
		" order by rech.rech_nr " )  ;


// Cursor readcbeding : 

	dbClass.sqlin ((short *) &rech.mdn, SQLSHORT, 0);
    dbClass.sqlin ((long *)  &rech.rech_vbd, SQLLONG, 0);
//	dbClass.sqlin ((long *)  &rech.kun, SQLLONG, 0);
//	dbClass.sqlin ((char *)  cvon_datum, SQLCHAR, 11);
//	dbClass.sqlin ((char *)  cbis_datum, SQLCHAR, 11);

    dbClass.sqlout ((short *)	&rech.delstatus,SQLSHORT,0 ) ;
    dbClass.sqlout ((short *)	&rech.fil,SQLSHORT,0 ) ;
    dbClass.sqlout ((long *)	&rech.kun,SQLLONG,0 ) ;
    dbClass.sqlout ((short *)	&rech.mdn,SQLSHORT,0 ) ;
    dbClass.sqlout ((long *)	&rech.rech_nr,SQLLONG,0 ) ;
// XDATUM    dbClass.sqlout ((char *)	 rech.rech_dat,SQLCHAR, 11 ) ;
	dbClass.sqlout ((TIMESTAMP_STRUCT *)  &rech.ts_rech_dat, SQLTIMESTAMP, 26);

	dbClass.sqlout ((long *)	&rech.rech_fuss_txt,SQLLONG,0 ) ;
    dbClass.sqlout ((long *)	&rech.rech_kopf_txt,SQLLONG,0 ) ;
    dbClass.sqlout ((double *)	&rech.rech_sum_bto,SQLDOUBLE,0 ) ;
    dbClass.sqlout ((double *)	&rech.rech_sum_nto,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_end_rab,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_bez,SQLDOUBLE,0) ;
    dbClass.sqlout ((char *)	 rech.rech_stat,SQLCHAR,2);
    dbClass.sqlout ((char *)	 rech.blg_typ,SQLCHAR,2);
    dbClass.sqlout ((char *)	 rech.krz_txt,SQLCHAR,17);
    dbClass.sqlout ((double *)	&rech.skto,SQLDOUBLE,0) ;
    dbClass.sqlout ((short *)	&rech.zahl_art,SQLSHORT,0) ;
    dbClass.sqlout ((char *)	 rech.zahl_dat,SQLCHAR,11);
    dbClass.sqlout ((short *)	&rech.waehrung,SQLSHORT,0) ;
    dbClass.sqlout ((double *)	&rech.rech_bto_eur,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_nto_eur,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_rab_eur,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_bez_eur,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_bto_fremd,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_nto_fremd,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_rab_fremd,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_bez_fremd,SQLDOUBLE,0) ;
    dbClass.sqlout ((char *)	 rech.ueb_kz,SQLCHAR,2) ;
    dbClass.sqlout ((long *)	&rech.geld_kto,SQLLONG,0) ;
    dbClass.sqlout ((long *)	&rech.teil_smt,SQLLONG,0) ;
    dbClass.sqlout ((long *)	&rech.abkommen,SQLLONG,0) ;
    dbClass.sqlout ((double *)	&rech.rerabproz,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_rab,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_rab1,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.rech_rab2,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.netto,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.netto1,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.netto2,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.mwst1,SQLDOUBLE,0) ;
    dbClass.sqlout ((double *)	&rech.mwst2,SQLDOUBLE,0) ;
    dbClass.sqlout ((short *)	&rech.sam_rech,SQLSHORT,0) ;
    dbClass.sqlout ((long *)	&rech.rech_vbd,SQLLONG,0) ;
// XDATUM    dbClass.sqlout ((char *)	rech.rvdat,SQLCHAR,11) ;
	dbClass.sqlout ((TIMESTAMP_STRUCT *)  &rech.ts_rvdat, SQLTIMESTAMP, 26);

	readcbeding = (short) dbClass.sqlcursor ("select "

    " delstatus, fil, kun, mdn, rech_nr, rech_dat, rech_fuss_txt, rech_kopf_txt "
    " ,rech_sum_bto, rech_sum_nto, rech_end_rab, rech_bez, rech_stat, blg_typ "
    " ,krz_txt, skto, zahl_art, zahl_dat, waehrung, rech_bto_eur, rech_nto_eur "
    " ,rech_rab_eur, rech_bez_eur, rech_bto_fremd, rech_nto_fremd, rech_rab_fremd "
    " ,rech_bez_fremd, ueb_kz, geld_kto, teil_smt, abkommen, rerabproz, rech_rab "
	" ,rech_rab1, rech_rab2, netto, netto1, netto2, mwst1, mwst2, sam_rech "
	" ,rech_vbd ,rvdat "
	" from rech where rech.mdn = ? "
	" and rech.fil = 0 and rech.delstatus = 0 and rech.rech_nto_eur <> 0 "
	" and rech.rech_vbd = ? " 
	" and ( rech.blg_typ = \"R\" or rech.blg_typ = \"G\" ) "  
	" order by rech.rech_dat,rech.rech_nr " ) ;

}

void RECH_P_CLASS::prepare (void)
{

	dbClass.sqlin ((long *)  &rech_p.rech_nr, SQLLONG, 0);
	dbClass.sqlin ((short *) &rech_p.mdn, SQLSHORT, 0);
    dbClass.sqlin ((char *) rech_p.blg_typ, SQLCHAR, 2);

    dbClass.sqlout ((short *) &rech_p.waehrung, SQLSHORT, 0) ;
    dbClass.sqlout ((short *) &rech_p.mdn, SQLSHORT, 0 ) ;
    dbClass.sqlout ((short *) &rech_p.fil, SQLSHORT, 0 ) ;
	dbClass.sqlout ((long *)  &rech_p.rech_nr, SQLLONG, 0 ) ;   
	dbClass.sqlout ((char *)   rech_p.blg_typ, SQLCHAR, 2 ) ;
	dbClass.sqlout ((long *)  &rech_p.ls, SQLLONG, 0 ) ;
	dbClass.sqlout ((char *)   rech_p.lblg, SQLCHAR, 2 ) ;
	dbClass.sqlout ((long *)  &rech_p.lkun, SQLLONG, 0 ) ;
	dbClass.sqlout ((long *)  &rech_p.posi, SQLLONG, 0 ) ;
	dbClass.sqlout ((char *)   rech_p.ls_charge, SQLCHAR, 51 ) ;	// Boese Falle 110308 21-> 51 
	dbClass.sqlout ((char *)   rech_p.a_bz1, SQLCHAR, 25 ) ;
	dbClass.sqlout ((char *)   rech_p.a_bz2, SQLCHAR, 25 ) ;
	dbClass.sqlout ((double *)&rech_p.a, SQLDOUBLE, 0 ) ;
	dbClass.sqlout ((char *)   rech_p.a_kun, SQLCHAR, 14 ) ;
	dbClass.sqlout ((double *)&rech_p.lad_pr, SQLDOUBLE, 0 ) ;
	dbClass.sqlout ((double *)&rech_p.vk_pr, SQLDOUBLE, 0 ) ;
	dbClass.sqlout ((double *)&rech_p.netto, SQLDOUBLE, 0 ) ;
	dbClass.sqlout ((double *)&rech_p.bonus_pr, SQLDOUBLE, 0 ) ;
	dbClass.sqlout ((double *)&rech_p.bonus_wrt, SQLDOUBLE, 0 ) ;
	dbClass.sqlout ((double *)&rech_p.rab_pr, SQLDOUBLE, 0 ) ;
	dbClass.sqlout ((double *)&rech_p.rab_wrt, SQLDOUBLE, 0 ) ;
	dbClass.sqlout ((double *)&rech_p.rech_rab1, SQLDOUBLE, 0 ) ;
	dbClass.sqlout ((double *)&rech_p.rech_rabwrt, SQLDOUBLE, 0 ) ;
	dbClass.sqlout ((short *) &rech_p.mwst, SQLSHORT, 0 ) ;
	dbClass.sqlout ((double *)&rech_p.mwst_wert, SQLDOUBLE, 0 ) ;
// XDATUM	dbClass.sqlout ((char *)   rech_p.lieferdat, SQLCHAR, 11 ) ;
 	dbClass.sqlout ((TIMESTAMP_STRUCT *) &rech_p.ts_lieferdat, SQLTIMESTAMP, 26 ) ;

	dbClass.sqlout ((char *)   rech_p.auf_ext, SQLCHAR, 31 ) ;
	dbClass.sqlout ((double *)&rech_p.lief_me, SQLDOUBLE, 0 ) ;
	dbClass.sqlout ((long *)  &rech_p.lsp_txt, SQLLONG, 0 ) ;	// 140910   


	readcursor = (short) dbClass.sqlcursor ("select "
	" waehrung ,mdn ,fil ,rech_nr ,blg_typ ,ls ,lblg ,lkun ,posi "
	" ,ls_charge ,a_bz1 ,a_bz2 ,a ,a_kun ,lad_pr ,vk_pr ,netto "
	" ,bonus_pr ,bonus_wrt ,rab_pr ,rab_wrt ,rech_rab1 ,rech_rabwrt "
	" ,mwst ,mwst_wert ,lieferdat ,auf_ext ,lief_me, lsp_txt "

  	" from rech_p where rech_nr = ? and mdn = ? and blg_typ = ? "
	" and lief_me <> 0 " 
	" order by rech_p.posi ") ;

}

void RECH_VBD_CLASS::prepare (void)
{

	dbClass.sqlin ((long *)  &rech_vbd.rech_nr, SQLLONG, 0);
	dbClass.sqlin ((short *) &rech_vbd.mdn, SQLSHORT, 0);

	dbClass.sqlout ((char *)   rech_vbd.krz_txt, SQLCHAR, 17 ) ;
	dbClass.sqlout ((short *) &rech_vbd.mdn, SQLSHORT, 0 ) ;
	dbClass.sqlout ((short *) &rech_vbd.fil, SQLSHORT, 0 ) ;
	dbClass.sqlout ((long *)  &rech_vbd.inka_nr, SQLLONG, 0 ) ;
	dbClass.sqlout ((long *)  &rech_vbd.kun, SQLLONG, 0 ) ;
	dbClass.sqlout ((short *) &rech_vbd.teil_smt, SQLSHORT, 0 ) ;
	dbClass.sqlout ((long *)  &rech_vbd.rech_vbd, SQLLONG, 0 ) ;
	dbClass.sqlout ((char *)   rech_vbd.blg_typ, SQLCHAR, 2 ) ;
	dbClass.sqlout ((long *)  &rech_vbd.rech_nr, SQLLONG, 0 ) ;
	dbClass.sqlout ((long *)  &rech_vbd.ls, SQLLONG, 0 ) ;
	dbClass.sqlout ((double *)&rech_vbd.rech_sum_nto, SQLDOUBLE, 0 ) ;
	dbClass.sqlout ((double *)&rech_vbd.rech_sum_nto1, SQLDOUBLE, 0 ) ;
	dbClass.sqlout ((double *)&rech_vbd.rech_sum_nto2, SQLDOUBLE, 0 ) ;
	dbClass.sqlout ((double *)&rech_vbd.mwst_betr1, SQLDOUBLE, 0 ) ;
	dbClass.sqlout ((double *)&rech_vbd.mwst_betr2, SQLDOUBLE, 0 ) ;
	dbClass.sqlout ((short *) &rech_vbd.waehrung, SQLSHORT, 0 ) ;
	dbClass.sqlout ((double *)&rech_vbd.rech_nto_eur, SQLDOUBLE, 0 ) ;
	dbClass.sqlout ((double *)&rech_vbd.rech_nto1_eur, SQLDOUBLE, 0 ) ;
	dbClass.sqlout ((double *)&rech_vbd.rech_nto2_eur, SQLDOUBLE, 0 ) ;
	dbClass.sqlout ((double *)&rech_vbd.rech_mwst1_eur, SQLDOUBLE, 0 ) ;
	dbClass.sqlout ((double *)&rech_vbd.rech_mwst2_eur, SQLDOUBLE, 0 ) ;
	

	readcursor = (short) dbClass.sqlcursor ("select "
	" krz_txt ,mdn, fil ,inka_nr , kun ,teil_smt , rech_vbd ,blg_typ "
	" ,rech_nr ,ls ,rech_sum_nto ,rech_sum_nto1 ,rech_sum_nto2 ,mwst_betr1 "
	" ,mwst_betr2 ,waehrung ,rech_nto_eur ,rech_nto1_eur ,rech_nto2_eur "
	" ,rech_mwst1_eur ,rech_mwst2_eur "
	
  	" from rech_vbd where rech_nr = ? and mdn = ? "
	" order by rech_vbd.rech_vbd ") ;

}

