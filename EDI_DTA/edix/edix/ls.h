#ifndef _LS_DEF
#define _LS_DEF


struct RETK {

short	mdn ;
short	fil ; 
long	ret ;
long	ls ;  
short	kun_fil ; 
char	feld_bz1[20] ;  
long	kun ; 
char	ret_dat[11] ;
TIMESTAMP_STRUCT ts_ret_dat ;
char	lieferdat[11] ;
TIMESTAMP_STRUCT ts_lieferdat ;
char	hinweis[49] ;
short	ret_stat ;
char	feld_bz2[12] ;
char	kun_krz1[17] ;
char	feld_bz3[8] ;
long	adr ;
short	delstatus ;
long	rech ;
char	blg_typ[2] ;
long	kopf_txt ;
long	fuss_txt ;
long	vertr ;
long	inka_nr ;
double	of_po ;
short	teil_smt ; 
char	pers_nam[9] ;
double	of_ek ;
double	of_po_euro ;
double	of_po_fremd ;
double	of_ek_euro ;
double	of_ek_fremd ;
short	waehrung ;
short	ret_grund ;
long	gruppe ;
char	ueb_kz[2] ;
};

extern struct RETK retk, retk_null;


class RETK_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int dbcount (void);
               int leseretk (void);
               int openretk (void);
               RETK_CLASS () : DB_CLASS ()
               {
               }
};



#endif

