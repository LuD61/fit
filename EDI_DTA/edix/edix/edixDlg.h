// edixDlg.h : Headerdatei
//

#pragma once
#include "afxwin.h"
#include "dBClass.h"	// 150514

// CedixDlg Dialogfeld
class CedixDlg : public CDialog
{
// Konstruktion
public:
	CedixDlg(CWnd* pParent = NULL);	// Standardkonstruktor

// Dialogfelddaten
	enum { IDD = IDD_EDIX_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV-Unterst�tzung


// Implementierung
protected:

	int traktiv ;

	HICON m_hIcon;

	// Generierte Funktionen f�r die Meldungstabellen
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CEdit m_edit1;
	CString v_edit1;

	CEdit m_edit3;
	CString v_edit3;
	CEdit m_edit4;
	CString v_edit4;

	CEdit m_edit5;
	CString v_edit5;


	afx_msg BOOL PreTranslateMessage(LPMSG) ;
	afx_msg BOOL NoListCtrl (CWnd *) ;

	afx_msg void OnEnKillFocusEdit1();
	afx_msg void OnEnKillFocusEdit3();
	afx_msg void OnEnKillFocusEdit4();
	afx_msg void OnEnKillFocusEdit5();
		    void anzeigen(char * ) ;
			int RELICHECK(int) ;
	CEdit m_edit11;
	CString v_edit11;
	afx_msg void OnBnClickedOk();

	afx_msg void OnCbnSelchangeCombo1();
	CComboBox m_Combo1;
	CString v_Combo1;
	CString v_edit6;
	afx_msg void OnCbnKillfocusCombo1();
	afx_msg void OnEnChangeEdit3();
	afx_msg void OnEnChangeEdit5();
	CEdit m_edit2;
	short v_edit2;
	afx_msg void OnEnKillfocusEdit2();
	BOOL v_nachdruck;
	CButton m_nachdruck;
	afx_msg void OnBnClickedCheck1();

// 150514 
	void paras ( char * globdta , char * globmdn , char * globvdat ,char * globbdat ,char * globrdat ) ;
    void ReadMdn (void) ;
	long interndta ;
	short internmdn ;
	TIMESTAMP_STRUCT internvdat;
	TIMESTAMP_STRUCT internbdat;
	TIMESTAMP_STRUCT internrdat;

};
