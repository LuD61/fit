#include "stdafx.h"
#include "DbClass.h"
#include "edik.h"

struct EDIK edik,  edik_null;
struct EDIP edip,  edip_null;

extern DB_CLASS dbClass;

extern char * sqldatdstger( TIMESTAMP_STRUCT *dstakt, char *outpstr ) ;

int EDIK_CLASS::insertedik (void)
/**
Tabelle edik inserten
**/
{

         if ( readcursor < 0 )
         {
             prepare ();
         }
         int di = dbClass.sqlexecute (ins_cursor);
         return di ;

}

int EDIK_CLASS::openedik (void )
{

	if ( readcursor < 0 ) prepare ();	
	int di =  dbClass.sqlopen (readcursor);
	return di ;
}

int EDIK_CLASS::leseedik (void)
{
	int di = dbClass.sqlfetch (readcursor);
	if ( !di )
	{
		sqldatdstger ( &edik.ts_rech_dat, edik.rech_dat );
	}
	return di ;
}

int EDIP_CLASS::insertedip (void)
{

         if (readcursor == -1)
         {
             prepare ();
         }
         int di = dbClass.sqlexecute (ins_cursor);
         return di ;

}

int EDIP_CLASS::openedip (void )
{

	if ( readcursor < 0 ) prepare ();	
	int di =  dbClass.sqlopen (readcursor);
	return di ;
}

int EDIP_CLASS::leseedip (void)
{
	int di = dbClass.sqlfetch (readcursor);
	if ( !di)
	{
		sqldatdstger ( &edip.ts_rech_dat, edip.rech_dat );
	}
	return di;
}

void EDIK_CLASS::prepare (void)
{

	dbClass.sqlin ((short *) &edik.mdn, SQLSHORT,0) ;
	dbClass.sqlin ((long *)  &edik.dta, SQLLONG,0) ;
	dbClass.sqlin ((long *)  &edik.listnr, SQLLONG,0) ;
// XDATUM	dbClass.sqlin ((char *)   edik.rech_dat, SQLCHAR,11) ;
	dbClass.sqlin ((TIMESTAMP_STRUCT *)  &edik.ts_rech_dat, SQLTIMESTAMP, 26);

	dbClass.sqlin ((long *)  &edik.dta_ref, SQLLONG,0) ;
	dbClass.sqlin ((long *)  &edik.abkommen, SQLLONG,0 ) ;
	dbClass.sqlin ((long *)  &edik.mge_lief, SQLLONG,0 ) ;
	dbClass.sqlin ((double *)  &edik.kurs, SQLDOUBLE,0 ) ;	// 140714


	ins_cursor = (short) dbClass.sqlcursor ( "insert into edik "
		"( mdn,dta,listnr,rech_dat,dta_ref,abkommen,mge_lief, kurs ) " 
		" values( ?,?,?,?,?,?,?,? )");

// ##############################################################
	dbClass.sqlin ((short *) &edik.mdn, SQLSHORT,0) ;
	dbClass.sqlin ((long *)  &edik.dta, SQLLONG,0) ;
	dbClass.sqlin ((long *)  &edik.dta_ref, SQLLONG,0) ;

	dbClass.sqlout ((short *) &edik.mdn, SQLSHORT,0) ;
	dbClass.sqlout ((long *)  &edik.dta, SQLLONG,0) ;
	dbClass.sqlout ((long *)  &edik.listnr, SQLLONG,0) ;
// XDATUM 	dbClass.sqlout ((char *)   edik.rech_dat, SQLCHAR,11) ;
	dbClass.sqlout ((TIMESTAMP_STRUCT *)  &edik.ts_rech_dat, SQLTIMESTAMP, 26);

	dbClass.sqlout ((long *)  &edik.dta_ref, SQLLONG,0) ;
	dbClass.sqlout ((long *)  &edik.abkommen, SQLLONG,0 ) ;
	dbClass.sqlout ((long *)  &edik.mge_lief, SQLLONG,0 ) ;
	dbClass.sqlout ((double *)  &edik.kurs, SQLDOUBLE,0 ) ;	// 140714

	readcursor = (short) dbClass.sqlcursor ( " select "
		" mdn,dta,listnr,rech_dat,dta_ref,abkommen,mge_lief ,kurs " 
		" from edik where mdn = ? and dta = ? and dta_ref = ? "
		" order by listnr ");
	
}

void EDIP_CLASS::prepare (void)
{


	dbClass.sqlin ((short *) &edip.mdn, SQLSHORT, 0 ) ;
	dbClass.sqlin ((long *) &edip.dta, SQLLONG, 0 ) ;
	dbClass.sqlin ((long *) &edip.listnr,SQLLONG, 0 ) ;
	dbClass.sqlin ((long *) &edip.inka_nr, SQLLONG, 0 ) ;
	dbClass.sqlin ((long *) &edip.rech_nr, SQLLONG, 0 ) ;
	dbClass.sqlin ((char *)  edip.blg_typ, SQLCHAR, 2 ) ;
// XDATUM	dbClass.sqlin ((char *) &edip.rech_dat, SQLCHAR, 11 ) ;
	dbClass.sqlin ((TIMESTAMP_STRUCT *)  &edip.ts_rech_dat, SQLTIMESTAMP, 26);

	dbClass.sqlin ((char *)  edip.iln, SQLCHAR, 17 ) ;
	dbClass.sqlin ((long *) &edip.dta_ref, SQLLONG, 0 ) ;
	dbClass.sqlin ((long *) &edip.abkommen, SQLLONG, 0 ) ;

	ins_cursor = (short) dbClass.sqlcursor ( "insert into edip "
		"( mdn,dta,listnr,inka_nr,rech_nr,blg_typ,rech_dat,iln,dta_ref,abkommen ) " 
		" values( ?,?,?,?,?,?,?,?,?,?)");

// ###############################################################################

	dbClass.sqlin ((short *) &edip.mdn, SQLSHORT, 0 ) ;
	dbClass.sqlin ((long *) &edip.dta, SQLLONG, 0 ) ;
	dbClass.sqlin ((long *) &edip.listnr,SQLLONG, 0 ) ;
	dbClass.sqlin ((long *) &edip.dta_ref, SQLLONG, 0 ) ;

	dbClass.sqlout ((short *) &edip.mdn, SQLSHORT, 0 ) ;
	dbClass.sqlout ((long *) &edip.dta, SQLLONG, 0 ) ;
	dbClass.sqlout ((long *) &edip.listnr,SQLLONG, 0 ) ;
	dbClass.sqlout ((long *) &edip.inka_nr, SQLLONG, 0 ) ;
	dbClass.sqlout ((long *) &edip.rech_nr, SQLLONG, 0 ) ;
	dbClass.sqlout ((char *)  edip.blg_typ, SQLCHAR, 2 ) ;
// XDATUM	dbClass.sqlout ((char *) &edip.rech_dat, SQLCHAR, 11 ) ;
	dbClass.sqlout ((TIMESTAMP_STRUCT *)  &edip.ts_rech_dat, SQLTIMESTAMP, 26);

	dbClass.sqlout ((char *)  edip.iln, SQLCHAR, 17 ) ;
	dbClass.sqlout ((long *) &edip.dta_ref, SQLLONG, 0 ) ;
	dbClass.sqlout ((long *) &edip.abkommen, SQLLONG, 0 ) ;

	readcursor = (short) dbClass.sqlcursor ( "select "
		" mdn,dta,listnr,inka_nr,rech_nr,blg_typ,rech_dat,iln,dta_ref,abkommen " 
		" from edip where mdn = ? and dta = ? and listnr = ? and dta_ref = ? "
		" order by inka_nr, rech_nr " );
}

