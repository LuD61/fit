#ifndef _TRAPO_DEF
#define _TRAPO_DEF

struct TRAPOPSPM {
short mdn;
short fil;
long ls;
char blg_typ[2];
long lfd;

double pm;
double pmzahl;
double ps1;
double ps1zahl;
double ps2;
double ps2zahl;
double ps3;
double ps3zahl;
double ps4;
double ps4zahl;
};

extern struct TRAPOPSPM trapopspm, trapopspm_null;
extern class TRAPOPSPM_CLASS trapopspm_class ;

class TRAPOPSPM_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int lesepspm (void);
               int openpspm (void);
               TRAPOPSPM_CLASS () : DB_CLASS ()
               {
               }
};

struct TRAPONVE {

short mdn;
short fil;
long ls;
char blg_typ [2];
long lfd; 

char nve[21];
double a;
};

extern struct TRAPONVE traponve, traponve_null;
extern class TRAPONVE_CLASS traponve_class ;

class TRAPONVE_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int lesenve (void);
               int opennve (void);
               TRAPONVE_CLASS () : DB_CLASS ()
               {
               }
};


#endif

