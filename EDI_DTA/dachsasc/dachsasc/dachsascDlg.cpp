// dachsascDlg.cpp : Implementierungsdatei
//

#include "stdafx.h"

#include <direct.h>
#include <fcntl.h>

#include "dachsasc.h"
#include "dachsascDlg.h"
#include "DbClass.h"
#include "kun.h"
#include "mdn.h"
#include "adr.h"
#include "trapo.h"
#include "ptabn.h"
#include "ls.h"
#include "zudax.h"
#include "SpaDachs.h"
#include "mo_numme.h"
#include "rech.h"
#include "a_bas.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// mehr als 25 LG-Artikel sollten kaum in Gebrauch sein ...

static char meeinhcode [25][9];
static double meeinharti[25] ;


// folgende Logik : "nur fuellen bei eindeutiger Zuordenbarkeit "
// lfd ist immer > 0 
// kpzei wir bei erstem Auftreten zugeordnet, bei mehrfach-Auftreten wieder weggenullt 

struct KPPAZEI
{
long lfd ;
int kpzei ;
} ;

struct KPPAZEI kppazei [15] ;

static double lsgesgewn ;
static double lsgesgewt ;
static double lsgeswert ;

static char filename [451] ;


// 211111 : Temporaer-Datei-Handling wegen ftp-Automatik
char EchtDateiname[512] ;
char TmpDateiname[512] ;

static long lfdoffset ;

static char bufh[512] ;

static char tourbeding[512] ;

int gesamtcount ;

DB_CLASS dbClass ;	// Hier ist die Heimat der dbClass, alle Tabellen sind in ihrem jeweiligen Modul daheim

// CAboutDlg-Dialogfeld f�r Anwendungsbefehl "Info"

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialogfelddaten
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterst�tzung

// Implementierung
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()



// hilfsproceduren zur  stringformatierung 

static char *wort[256];
static char buffer [0x1000] ;
static char DefWert [256] ;

// Naechstes Zeichen != Trennzeichen suchen.
int next_char_ci (char *string, char tzeichen, int i)
{
	for (;string [i]; i ++)
	{
		if (string[i] != tzeichen)
		{
			return (i);
		}
	}
	return (i);
}


//	der 2. Parameter ist dann ein endloser String bis zum #-Trenner oder CR/LF

short splite (char *string)
{
 short wz;                  // Wortzaehler  
 int i, j, len;
 static char zeichen = ' ';
 wz = j = 0;
 len = (int)strlen (string);
 wz = 1;
 i = next_char_ci (string, zeichen, 0);
 if (i >= len) return (0);
 wort [wz] =  buffer ;	
 wz ++;
 for (; i < len; i ++, j ++)
 {
	if (string [i] == zeichen)
	{
		i = next_char_ci (string, zeichen, i);
		if (i >= len) break;
			buffer [j] = (char) 0;
		j ++;
		wort [wz] = &buffer [j];
		wz ++;
		zeichen = '#' ;
	}
	buffer [j] = string [i];
 }
 buffer [j] = (char) 0;
 return (wz - 1);
}

short split (char *string)
{
 short wz;                  // Wortzaehler  
 int i, j, len;
 static char zeichen = ' ';
 wz = j = 0;
 len = (int)strlen (string);
 wz = 1;
 i = next_char_ci (string, zeichen, 0);
 if (i >= len) return (0);
 wort [wz] =  buffer ;	
 wz ++;
 for (; i < len; i ++, j ++)
 {
	if (string [i] == zeichen)
	{
		i = next_char_ci (string, zeichen, i);
		if (i >= len) break;
			buffer [j] = (char) 0;
		j ++;
		wort [wz] = &buffer [j];
		wz ++;
	}
	buffer [j] = string [i];
 }
 buffer [j] = (char) 0;
 return (wz - 1);
}

// vergleiche upper-strings
int strupcmpi (char *str1, char *str2, int len)
{
 short i;
 char upstr1;
 char upstr2;
 for (i = 0; i < len; i ++, str1 ++, str2 ++)
 {
	if (*str1 == 0)
		return (-1);
	if (*str2 == 0)
		return (1);
	upstr1 = toupper((int) *str1);
	upstr2 = toupper((int) *str2);
	if (upstr1 < upstr2)
	{
		return(-1);
	}
	else if (upstr1 > upstr2)
	{
		return (1);
	}
 }
 return (0);
}

// CR am Stringende etfernen.

void cr_weg (char *string)
{
 for (; *string; string += 1)
 {
	if (*string == (char) 13)
		break;
	if (*string == (char) 10)
		break;
 }
 *string = 0;
 return;
}
char *clippedi (char *string)
{
 char *clstring;
 short i,len;

 len = (short) strlen (string);

 if (len == 0) return (string);
 len --;
 clstring = string;
 for (i = len; i >= 0; i --)
 {
  if ((unsigned char) clstring[i] > 0x20)
  {
   break;
  }
 }
 clstring [i + 1] = 0;

 clstring = string;
 len = (short)strlen (clstring);

 for (i = 0; i < len; i ++, clstring +=1)
 {
  if ((unsigned char) *clstring > (unsigned char) 0X20)
  {
   break;
  }
 }
 return (clstring);
}

char *get_defa (char *env)
{         
 int anz;
 char puffer [512];
 FILE *fp;
 sprintf (puffer, "%s\\dachser.cfg",getenv ("BWSETC"));
 fp = fopen (puffer, "r");
 if (fp == (FILE *)NULL) return NULL;
 while (fgets (puffer, 511, fp))
 {
	cr_weg (puffer);
	anz = split (puffer);
	if (anz < 2) continue;
	if (strupcmpi (wort[1], env, (int) strlen (env)) == 0)
	{
		strcpy (DefWert, clippedi (wort [2]));
		fclose (fp);
        return (char *) DefWert;
	}
 }
 fclose (fp);
 return NULL;
}

void fuellefeld ( char * quelle , char * ziel , int solllen , char fuellfeld )
{
	// eineseits auffuellen mit folgeblanks oder nullen , andererseits Laengenlimit fixen 

	char buffl[280] ;
	sprintf ( buffl , "%s", quelle ) ;
	int po = 0 ;
	int gle = (int)strlen ( buffl ) ;
	int pi = 0 ;

	for (po = 0 ; po < solllen; po++ )
	{
		if ( pi < gle )
			ziel[po] = buffl[pi++] ;
		else
			ziel[po] = fuellfeld ;	// Alpha Feld mit " " fuellen, num-felder mit "0" fuellen  
	}
	ziel[po] = '\0';
}

int CdachsascDlg::holedefaults (void )
{

// 220311 : fest als Dietz-Apllikation eisntellen :
	anwender = ANWDIETZ ;

// Kundennummer holen z.B. dietz-Nummer
	char *kna =  get_defa ( "K_NR_AUFG" )	;
	if ( kna == NULL )
		return -1  ;
	else
		sprintf ( buffer , "%s" , kna ) ;

	fuellefeld ( buffer , abkda , 8 , '0') ;

// Nummer der Dachser-Niederlassung

	char *kns =  get_defa ( "K_NR_SPED" )	;
	if ( kns == NULL )
		return -1  ;
	else
		sprintf ( buffer , "%s" , kns ) ;

	fuellefeld ( buffer , dtkda , 8 , '0') ;

		return 0 ;
}


BOOL datumcheck(char * bufi )
{
	int i = (int) strlen(bufi) ;
	int hil1, hil2 ;
	int k = 0 ;	// 261007 : Zusatzchecks
	BOOL j = FALSE ;

	char hil5 [5] ;
	if ( i != 6 && i != 8 && i != 10 )
	{
		if ( i < 6 ) k = -1 ;
		if ( k == 0 )
		{
			// die ersten beiden Zeichen MUESSEN ZAHLEN sein
			if ( bufi[0] > '9' || bufi[0] < '0' ||bufi[1] > '9' || bufi[1] < '0' )
				k = - 1 ;
		}
		if ( k == 0 )
		{
			if ( bufi[2] > '9' || bufi[2] < '0' )
				k = 2 ;		// Typ 8 oder typ 10 moeglich

		}
		if ( k == 0 )
		{
			// nur noch Typ 6 erlaubt 
			if (    bufi[3] > '9' || bufi[3] < '0'
				 || bufi[4] > '9' || bufi[4] < '0'
				 || bufi[5] > '9' || bufi[5] < '0' )
			{
				k = - 1 ;
			}
			else
			{	// Gueltiger Typ 6  ......
				k = 1 ;
				i = 6 ;
				bufi[6] = '\0' ;
			}
		}
		if ( k == 2 )
		{
			if ( i < 8 )
			{	
				k = -1 ;
			}
			else
			{	// 4.,5.,7.,8. Zeichen MUESSEN ZAHLEN sein, 6.Zeichen MUSS Nicht-Zahl sein
				if (   bufi[3] > '9' || bufi[3] < '0' 
					 ||bufi[4] > '9' || bufi[4] < '0'
				     ||bufi[6] > '9' || bufi[6] < '0'
				     ||bufi[7] > '9' || bufi[7] < '0'
				     || !( bufi[5] >'9' || bufi[5]< '0')
				    )
				k = - 1 ;
			}
		}
		if ( k == 2 )
		{
			if ( bufi[8] > '9' || bufi[8] < '0' )
			{	// gueltiger Typ 8 
				k = 1 ;
				i = 8 ;
				bufi[8] = '\0' ;
			}
		
		}

		if ( k == 2 )
		{
			if ( i < 10 )
			{	
				k = -1 ;
			}
			else
			{	// 9.,10. Zeichen MUESSEN ZAHLEN sein
				if (   bufi[8] > '9' || bufi[8] < '0' 
					 ||bufi[9] > '9' || bufi[9] < '0'
			       )
				{
					k = -1 ;
				}
				else
				{
					k = 1 ;
					i = 10 ;
					bufi[10] = '\0' ;
				}
			}
		}
		if ( k < 1 )
		{
			bufi[0] = '\0' ;
			return TRUE ;
		}
	}
	if ( i == 6 )	// nur ddmmyy erlaubt 
	{
		bufi[10] = '\0' ;
		bufi[ 9] = bufi[5] ;
		bufi[ 8] = bufi[4] ;
		if ( bufi[ 8] > '5' )
		{
			bufi[ 6] = '1' ;
			bufi[ 7] = '9' ;
		}
		else
		{
			bufi[ 6] = '2' ;
			bufi[ 7] = '0' ;
		}
		bufi[ 5] = '.' ;
		bufi[ 4] = bufi[3] ;
		bufi[ 3] = bufi[2] ;
		bufi[ 2] = '.' ;
		i = 10 ;
		j = TRUE ; 
	}
	if ( i == 8 )	// nur dd,mm,yy erlaubt 
	{
		bufi[10] = '\0' ;
		bufi[ 9] = bufi[7] ;
		bufi[ 8] = bufi[6] ;
		if ( bufi[ 8] > '5' )
		{
			bufi[ 6] = '1' ;
			bufi[ 7] = '9' ;
		}
		else
		{
			bufi[ 6] = '2' ;
			bufi[ 7] = '0' ;
		}
		bufi[ 5] = '.' ;
		bufi[ 2] = '.' ;
		i = 10 ;
		j = TRUE ;
	}
	if ( i == 10 )	// hier kommt nix anderes mehr an ....  
	{
		if (   bufi[0] > '9' || bufi[0] < '0' 
			|| bufi[1] > '9' || bufi[1] < '0'
			|| bufi[3] > '9' || bufi[3] < '0'
			|| bufi[4] > '9' || bufi[4] < '0'
			|| bufi[6] > '9' || bufi[6] < '0'
			|| bufi[7] > '9' || bufi[7] < '0'
			|| bufi[8] > '9' || bufi[8] < '0'
			|| bufi[9] > '9' || bufi[9] < '0'
			)
		{

			bufi[0] = '\0' ;
			return TRUE ;
		}

		hil5[0] = bufi[3] ;
		hil5[1] = bufi[4] ;
		hil5[2] = '\0' ;
		hil1 = atoi (hil5);
		if ( hil1 > 12 || hil1 < 1 )
		{
			bufi[0] = '\0' ;
			return TRUE ;
		}

		hil5[0] = bufi[0] ;
		hil5[1] = bufi[1] ;
		hil5[2] = '\0' ;
		hil2 = atoi (hil5);
		if ( hil2 > 31 || hil1 < 1 )
		{
			bufi[0] = '\0' ;
			return TRUE ;
		}
		switch ( hil1 )
		{
		case  4 :
		case  6 :
		case  9 :
		case 11 :
			if ( hil2 > 30 )
			{
				bufi[0] = '\0' ;
				return TRUE ;
			}
			break ;
		case 2 :

			hil5[0] = bufi[6] ;
			hil5[1] = bufi[7] ;
			hil5[2] = bufi[8] ;
			hil5[3] = bufi[9] ;
			hil5[4] = '\0' ;
			hil1    = atoi(hil5) ;
			if ( hil2 > 28 )	// IM Jahr 2000 geht dieser Check schief .....
			{
				if ( ! ( hil1 % 4) )
				{
					if ( hil2 > 29 )
					{
						bufi[0] = '\0' ;
						return TRUE ;
					}
				}
				else
				{
					bufi[0] = '\0' ;
					return TRUE ;
				};
			}
			break ;

		}

	}
	return j ;

}

DWORD ProcWaitExec (LPSTR prog, WORD SHOW_MODE, int x, int y, int cx, int cy)
/**
Fenster-Process starten und nicht auf Ende Warten.
**/
{
        STARTUPINFO sti;
        PROCESS_INFORMATION pi;
        DWORD ExitCode; 
        int ret;

        ZeroMemory (&sti, sizeof (sti));
        sti.cb = sizeof (sti);
        if (x != -1)
        {
                     sti.dwFlags |= STARTF_USEPOSITION;
                     sti.dwX = x;
                     sti.dwY = y;
        }

        if (cx != -1)
        {
                     sti.dwFlags |= STARTF_USESIZE;
                     sti.dwXSize = cx;
                     sti.dwYSize = cy;
        }
        sti.wShowWindow = SHOW_MODE;
        sti.dwFlags |= STARTF_USESHOWWINDOW;

        ret = CreateProcess (NULL,
                       prog,
                       NULL,
                       NULL,
                      (int) NULL,
                      (unsigned long) NULL,
                       NULL,
                       NULL,
                       &sti,
                       &pi);
        if (ret)
        {
                  CloseHandle (pi.hThread);
                  WaitForSingleObject (pi.hProcess, INFINITE);
        }
		else
        {
			      return -1;
        }				  
		GetExitCodeProcess (pi.hProcess, &ExitCode);
		while (ExitCode == STILL_ACTIVE)
		{
                  GetExitCodeProcess (pi.hProcess, &ExitCode);
		}
        CloseHandle (pi.hProcess);
        return ExitCode;
}

void CdachsascDlg::ReadMdn (void) 
{
	mdn_class.openmdn();
	if (! mdn_class.lesemdn())
	{
		adr.adr = mdn.adr ;
		int i = adr_class.openadr () ;
		i = adr_class.leseadr () ;
		if (!i) 
		{
			v_mdnname.Format("%s",_T(adr.adr_krz));
		}
		else
		{
			v_mdnname.Format("              ");
			MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
//	gibbet nich		PrevDlgCtrl();
		}
	}
	else	// fehlerhafte Eingabe
	{
		v_mdnname.Format("              ");
		MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
// gibbet nich 		PrevDlgCtrl();
	}
		UpdateData (FALSE) ;
}




// CdachsascDlg-Dialogfeld



CdachsascDlg::CdachsascDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CdachsascDlg::IDD, pParent)
	, v_mdnnr(_T(""))
	, v_mdnname(_T(""))
	, v_tour(_T(""))
	, v_vdat(_T(""))
	, v_bdat(_T(""))
	, v_melde(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CdachsascDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_MDNNR, m_mdnnr);
	DDX_Text(pDX, IDC_MDNNR, v_mdnnr);
	DDV_MaxChars(pDX, v_mdnnr, 8);
	DDX_Control(pDX, IDC_MDNNAME, m_mdnname);
	DDX_Text(pDX, IDC_MDNNAME, v_mdnname);
	DDX_Control(pDX, IDC_TOUR, m_tour);
	DDX_Text(pDX, IDC_TOUR, v_tour);
	DDX_Control(pDX, IDC_VDAT, m_vdat);
	DDX_Text(pDX, IDC_VDAT, v_vdat);
	DDX_Control(pDX, IDC_BDAT, m_bdat);
	DDX_Text(pDX, IDC_BDAT, v_bdat);
	DDX_Control(pDX, IDC_MELDE, m_melde);
	DDX_Text(pDX, IDC_MELDE, v_melde);
}

BEGIN_MESSAGE_MAP(CdachsascDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_EN_KILLFOCUS(IDC_MDNNR, &CdachsascDlg::OnEnKillfocusMdnnr)
	ON_EN_KILLFOCUS(IDC_TOUR, &CdachsascDlg::OnEnKillfocusTour)
	ON_EN_KILLFOCUS(IDC_VDAT, &CdachsascDlg::OnEnKillfocusVdat)
	ON_EN_KILLFOCUS(IDC_BDAT, &CdachsascDlg::OnEnKillfocusBdat)
	ON_BN_CLICKED(IDCANCEL, &CdachsascDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDOK, &CdachsascDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CdachsascDlg-Meldungshandler

BOOL CdachsascDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Hinzuf�gen des Men�befehls "Info..." zum Systemmen�.

	// IDM_ABOUTBOX muss sich im Bereich der Systembefehle befinden.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Symbol f�r dieses Dialogfeld festlegen. Wird automatisch erledigt
	//  wenn das Hauptfenster der Anwendung kein Dialogfeld ist
	SetIcon(m_hIcon, TRUE);			// Gro�es Symbol verwenden
	SetIcon(m_hIcon, FALSE);		// Kleines Symbol verwenden

	// TODO: Hier zus�tzliche Initialisierung einf�gen

	dbClass.opendbase (_T("bws"));

// m_bisdatum.ShowWindow(SW_HIDE);
//	m_stapnu.ShowWindow(SW_NORMAL);

	mdn.mdn = 1 ;
	v_mdnnr = "1" ;

 	ReadMdn ();

/* ---> so kann man Systemparameter lesen ......
	dnachkpreis = atoi ( sys_par_class.sys_par_holen ( "nachkpreis" ))  ;
	if ( dnachkpreis > 4 || dnachkpreis < 2 ) dnachkpreis = 2 ;
< -------- */


	return TRUE;  // Geben Sie TRUE zur�ck, au�er ein Steuerelement soll den Fokus erhalten
}

void CdachsascDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// Wenn Sie dem Dialogfeld eine Schaltfl�che "Minimieren" hinzuf�gen, ben�tigen Sie 
//  den nachstehenden Code, um das Symbol zu zeichnen. F�r MFC-Anwendungen, die das 
//  Dokument/Ansicht-Modell verwenden, wird dies automatisch ausgef�hrt.

void CdachsascDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Ger�tekontext zum Zeichnen

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Symbol in Clientrechteck zentrieren
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Symbol zeichnen
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// Die System ruft diese Funktion auf, um den Cursor abzufragen, der angezeigt wird, w�hrend der Benutzer
//  das minimierte Fenster mit der Maus zieht.
HCURSOR CdachsascDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}




void CdachsascDlg::OnEnKillfocusMdnnr()
{
	UpdateData (TRUE) ;

	int i = m_mdnnr.GetLine(0,bufh,500);
	bufh[i] = '\0' ;
	if (i)	mdn.mdn = (short) atoi ( bufh );
	else mdn.mdn = -2 ;
	mdn_class.openmdn();
	if (! mdn_class.lesemdn())
	{
		adr.adr = mdn.adr ;
		i = adr_class.openadr () ;
		i = adr_class.leseadr () ;
		if (!i) 
		{
			v_mdnname.Format("%s",_T(adr.adr_krz));
		}
		else
		{
			v_mdnname.Format("              ");
			MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
// gibbet nich			PrevDlgCtrl();
		}
	}
	else	// fehlerhafte Eingabe
	{
		v_mdnname.Format("              ");
		MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
// gibbet nich		PrevDlgCtrl();
	}

	v_melde.Format( " " ) ;
	UpdateData (FALSE) ;
}

void CdachsascDlg::OnEnKillfocusTour()
{
	UpdateData (TRUE) ;
	v_melde.Format( " " ) ;
	UpdateData (FALSE) ;

}

void CdachsascDlg::OnEnKillfocusVdat()
{
	UpdateData(TRUE) ;
	int	i = m_vdat.GetLine(0,bufh,500) ;
	bufh[i] = '\0' ;

	if (i)
	{
		if (datumcheck(bufh))
		{
			v_vdat.Format("%s",_T(bufh));
		}
	}
	v_melde.Format( " " ) ;
	UpdateData (FALSE) ;
}

void CdachsascDlg::OnEnKillfocusBdat()
{
	UpdateData(TRUE) ;
	int	i = m_bdat.GetLine(0,bufh,500) ;
	bufh[i] = '\0' ;
	if (i)
	{
		if (datumcheck(bufh))
		{
			v_bdat.Format("%s",_T(bufh));
		}
	}
	v_melde.Format( " " ) ;
	UpdateData (FALSE) ;
}

void CdachsascDlg::OnBnClickedCancel()
{
	v_melde.Format( " " ) ;
	OnCancel();
}

void CdachsascDlg::OnBnClickedOk()
{
	UpdateData(TRUE) ;

	int	i = m_bdat.GetLine(0,bufh,500) ;
	bufh[i] = '\0' ;
	int	j = m_vdat.GetLine(0,bufh,500) ;
	bufh[j] = '\0' ;
	if ( i != 10  && j != 10 )
	{
		MessageBox("Ung�ltige Datumseingabe !", " ", MB_OK|MB_ICONSTOP);
		return ;
	}
	if ( i != 10 )
	{
			v_bdat = v_vdat ;
	}
	else if (j != 10 )
	{
			v_vdat = v_bdat ;

	}
 
	sqldatdst(  & lsk.vondate , v_vdat.GetBuffer()) ;
	sqldatdst(  & lsk.bisdate , v_bdat.GetBuffer()) ;

	int igef , mufo ,iok ;	// es Muss was sinnvolles eingegeben werden, sonst wird gemeckert

	// igef : irgendeine Zahl gefunden 
	// mufo == 0 -> es darf space oder nummer folgen ( vorblanks )
	// mufo == 3 -> es darf space oder nummer folgen (nachkomma) 
	// mufo == 1 -> es darf nummer, space oder komma folgen ( im wert )
	// mufo == 2 -> es darf space oder komma folgen ( nach wert , vor folgekomma )
	//  iok muss == 1 sein und igef muss > 0 und mufo darf nicht == 3 sein 


	i = m_tour.GetLine(0,bufh,500) ;
	bufh[i] = '\0' ;
	igef =  mufo = 0 ;
	iok = 1 ;

	for ( int j = 0 ; j < i ; j++ )
	{
		if (( bufh[j] > '9' ) || ( bufh[j] < '0' ))
		{
	// mufo == 0 -> es darf space oder nummer folgen ( vorblanks )
	// mufo == 3 -> es darf space oder nummer folgen (nachkomma) 
	// mufo == 1 -> es darf nummer, space oder komma folgen ( im wert )
	// mufo == 2 -> es darf space oder komma folgen ( nach wert , vor folgekomma )
			if ( bufh[j] == ' ')
			{
				if ( mufo == 1 )	// Zahl zuende
				{
					mufo = 2 ;
					continue ;
				}
				else
				{
					continue ;	// alle anderen Space-Zustaende
				}
			};				
			if ( bufh[j] == ',')
			{
	// mufo == 0 -> es darf space oder nummer folgen ( vorblanks )
	// mufo == 1 -> es darf nummer, space oder komma folgen ( im wert )
	// mufo == 2 -> es darf space oder komma folgen ( nach wert , vor folgekomma )
	// mufo == 3 -> es darf space oder nummer folgen (nachkomma) 

				if ( mufo == 0 )	// Komma vor erster zahl
				{
					iok = 0 ;
					break ;
				}
				if ( mufo == 1 )	// Komma am Zahlenende
				{
					mufo = 3 ;
					continue ;
				}
				if ( mufo == 2 )	// gueltiges komma nach space 
				{
					mufo = 3 ;
					continue ;
				}
				if ( mufo == 3 )	// Doppelkomma 
				{
					iok = 0 ;
					break ;
				}
			};
			iok = 0 ;
			break ;	// andere Zeichen
		}
		else	// nummer 
		{
			if ( mufo == 0 )	// vorblanks sind vorbei
			{
				mufo = 1 ;
				igef = 1 ;
				 continue ;
			}
			if ( mufo == 1 )	// mitten in einer Zahl
			{
				 continue ;
			}

			if ( mufo == 2 )	// space zwischen 2 Zahlen -> Abbruch
			{
				iok = 0 ;
				 break ;
			}
			if ( mufo == 3 )	// neue Zahl beginnt
			{
				mufo = 1 ;
				 continue ;
			}
		}

	}
	if ( ! iok || ! igef  || mufo == 3 )
	{
		MessageBox("Ung�ltige Toureingabe !", " ", MB_OK|MB_ICONSTOP);
		return ;
	}
	else
		sprintf ( tourbeding , "and lsk.tou in (%s)" ,bufh ) ;

	UpdateData(FALSE) ;

	int obdefa = holedefaults () ;
	if ( obdefa )
	{
		MessageBox("Datei dachser.cfg fehlerhaft !", " ", MB_OK|MB_ICONSTOP);
		return ;
	}
	int ret = Dateierstellung () ;

	if ((strlen ( filename )) > 3 )
		v_melde.Format( " Datei %s erstellt " , filename ) ;
	else
		v_melde.Format( " keine Datei erstellt " ) ;

	UpdateData(FALSE) ;

//	OnOK();
}
BOOL CdachsascDlg::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				CWnd *xWnd ;
				xWnd = GetFocus ();
			           if(xWnd == GetDlgItem (IDOK)) break ;			// ok - eigentlich nichts machen
// auf den ok-Knopf verlegt ....			           if(xWnd == GetDlgItem (IDC_AUSDRUCK)) break ;	// Ausduck halt
			           if(xWnd == GetDlgItem (IDCANCEL))
					   {
							GetParent ()->DestroyWindow ();
							return TRUE ;	// Abbruch halt ( genau wie ButtonClickedCancel) 
					   }
	
				if (OnReturn ())
				{
					return TRUE;
				}
			}
			else if (pMsg->wParam == VK_TAB)
			{

				break;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (OnKeyup ())
				{
					return TRUE;
				}
			}
			else if (pMsg->wParam == VK_F5)
			{
//				StepBack ();
				OnCancel () ;
//				GetParent ()->DestroyWindow ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				return TRUE;
			}

	}

	return CDialog::PreTranslateMessage(pMsg); 
}

BOOL CdachsascDlg::OnReturn ()
{
	CWnd *Control = GetFocus ();

	Control = GetNextDlgTabItem (Control, FALSE);

	if (Control != NULL)
	{
		Control->SetFocus ();
	}
	return TRUE;
}

BOOL CdachsascDlg::OnKeyup ()
{
	CWnd *Control = GetFocus ();

	Control = GetNextDlgTabItem (Control, TRUE);
	if (Control != NULL)
	{
		Control->SetFocus ();
	}
	return TRUE;
}

//  Ab hier beginnt die Erstellung der Datei :

int dtvorsatz;	// Vorsatz erfolgreich geschrieben -> impliziert, das auch was gefunden wurde

FILE * fpdatei ;


void SysDatum (char * Date)
{
	// hole Systemdatum zwecks Dateiname
 time_t timer;
 struct tm *ltime;

 time (&timer);
 ltime = localtime (&timer);
 sprintf ( Date,"%04d%02d%02d" , ltime->tm_year + 1900,
                                    ltime->tm_mon + 1,
                                    ltime->tm_mday);
}

void DachsDatum (int typ , char * quelle, char * ztag , char * zmon, char * zjahr , char * zjhd , char * zhh, char * zmin )
{

 time_t timer;
 struct tm *ltime;
 char hilfe[5] ;
	if ( typ == 1 )
	{	// hole systemdatum
		time (&timer);
		ltime = localtime (&timer);
// ich gehe spontan davon aus, das wir im 21. Jahrhundert leben .....

		sprintf ( zjahr , "%02d" , ltime->tm_year - 100 ) ;
		sprintf ( zjhd  , "20" ) ;	// das ist dann wohl fix
        sprintf ( zmon  , "%02d" , ltime->tm_mon + 1 ) ;
        sprintf ( ztag  , "%02d" , ltime->tm_mday );
        sprintf ( zhh   , "%02d" , ltime->tm_hour ) ;
		sprintf ( zmin  , "%02d" , ltime->tm_min );
	}
	else	// input ist ein zwingend string der form "dd.mm.yyyy hh:mn" oder "dd.mm.yyyy"
	{
		int gle = (int) strlen ( quelle ) ;

		hilfe[0] = quelle[0] ;
		hilfe[1] = quelle[1] ;
		hilfe[2] = '\0' ;
        sprintf ( ztag  , "%s" , hilfe );

		hilfe[0] = quelle[3] ;
		hilfe[1] = quelle[4] ;
		hilfe[2] = '\0' ;
        sprintf ( zmon  , "%s" , hilfe );

		hilfe[0] = quelle[6] ;
		hilfe[1] = quelle[7] ;
		hilfe[2] = '\0' ;
        sprintf ( zjhd  , "%s" , hilfe );

		hilfe[0] = quelle[8] ;
		hilfe[1] = quelle[9] ;
		hilfe[2] = '\0' ;
        sprintf ( zjahr  , "%s" , hilfe );
		if ( gle < 16 )
		{
			sprintf ( zhh  , "00" );
			sprintf ( zmin  , "00" );
		}
		else
		{
			hilfe[0] = quelle[11] ;
			hilfe[1] = quelle[12] ;
			hilfe[2] = '\0' ;
			sprintf ( zhh  , "%s" , hilfe );

			hilfe[0] = quelle[14] ;
			hilfe[1] = quelle[15] ;
			hilfe[2] = '\0' ;
			sprintf ( zmin  , "%s" , hilfe );
		}
	}
}


int CdachsascDlg::Dateitesten ( int modus , char * dateiname )
{

// modus == 0 : Testen  UND Dateiname erstellen
// modus == 1 : Loeschen
// return ==  0 : alles ok : Nix da und schreibbar	+ !!!!! fpdatei ist gueltig  
// return ==  1 : Problem : Datei gibbet bereits
// return == -1 : Problem : Datei nicht schreibbar 
	char basisname[200] ;
	char tmpname[200] ;	// 211111

	sprintf ( tmpname, "%s\\dachser" , getenv ("TMPPATH")) ;	// 211111
	_mkdir( tmpname ) ;	// Prophylaktisch immer Verzeichnis erstellen	// 211111


	char * p = getenv ( "DACHSER" ) ;
	if ( p == NULL ) 
	{	// Variable "DACHSER" nicht angelegt
		sprintf ( basisname, "%s\\dachser" , getenv ("TMPPATH")) ;
		_mkdir( basisname ) ;	// Prophylaktisch immer Verzeichnis erstellen
	}
	else
	{
		sprintf ( basisname, "%s" , p ) ;
	}

	if ( modus == 0 )
	{

		dbClass.beginwork () ;
		int ix = nvsimpel ( 0,0, "dachser", &vorsatznummer ) ;
		dbClass.commitwork () ;
		// bissel dirty, aber bevor 300 000 Dateien erzeugt wurden, ist bestimmt ein Meteor eingeschlagen

		if ( vorsatznummer == 100000 || vorsatznummer == 200000 )
		{	// die Nummer wird einfach verknallt
			dbClass.beginwork () ;
			ix = nvsimpel ( 0,0, "dachser", &vorsatznummer ) ;
			dbClass.commitwork () ;
		}

// Vorsatznummer darf maximal 5 stellen haben ..... 
		while ( vorsatznummer > 99999 )
		{
			vorsatznummer -= 100000 ;
		}
		long hvorsatznummer ;
		hvorsatznummer = vorsatznummer ;
		while ( hvorsatznummer > 9999 )
		{
			hvorsatznummer -= 10000 ;
		}
		while ( hvorsatznummer > 999 )
		{
			hvorsatznummer -= 1000 ;
		}

// Datei heisst immer "KUNDE&&&" laut Dachser

//		SysDatum ( filename ) ;
//		sprintf ( dateiname,"%s_Kunde.txt" , filename ) ;
		sprintf ( dateiname , "KUNDE%03d", hvorsatznummer ) ;
// 050411 : ab jetzt soll die datei ASCIIxxxxx heissen 
		sprintf ( dateiname , "ASCII%05d", vorsatznummer ) ;
		sprintf ( dateiname , "ASCII%05d.FILE", vorsatznummer ) ;	// 181111 : neuer Name

	}

// 211111	sprintf ( filename, "%s\\%s" , basisname , dateiname ) ;
	sprintf ( filename, "%s\\%s" , tmpname , dateiname ) ;	// 211111
	sprintf ( TmpDateiname, "%s\\%s" , tmpname , dateiname ) ;	// 211111
	sprintf ( EchtDateiname, "%s\\%s" , basisname , dateiname ) ;	// 211111

	if ( modus == 1 )
	{
		unlink ( filename ) ;
	}

	fpdatei = fopen (filename, "r");
	if (fpdatei == (FILE *)NULL)
	{	// Schreibtest jedenfalls durchfuehren 

		if ((fpdatei = fopen( filename ,"w+" )) == (FILE *)NULL )
				return -1 ;	// Datei nicht schreibbar (z.B. Pfad oder Permissions )
		fclose ( fpdatei) ;
		unlink ( filename ) ;

// Zum Schluss bleibt die Datei leer, offen und aktiv
		if ((fpdatei = fopen( filename ,"w+" )) == (FILE *)NULL )
				return -1 ;	// Datei nicht schreibbar (z.B. Pfad oder Permissions )
		return 0 ;	// alles i.O.
	}
	else
	{
		fclose ( fpdatei) ;
		if ( modus == 0 )
		{
			return 1 ;	// Datei gibbet schon
		}
		else
			return -1 ;	// Problem beim loeschen oder anlegen 
	};

	return  -1  ;	// Syntax-Dummy "irgend ein Problem
}

int CdachsascDlg::opendatei ( void )
{
	int retcode = 0  ;	// 0 = ok ; 1 Datei existiert ; -1 Zugriffsproblem ;  2 = alles vorbei

	char Dateiname [300] ;

	char ctext [300] ;

	retcode =  Dateitesten(0,Dateiname) ;	// Existenztest
	if ( retcode == -1  )
	{
		sprintf ( ctext , "Zugriffsproblem Datei  %s", Dateiname ) ;
		MessageBox( ctext , " ", MB_OK|MB_ICONSTOP);
		retcode = -2 ;	// Durchmarsch
	}
	if ( retcode == 1 )
	{
		if ( MessageBox("Datei existiert bereits - vorhandene Datei l�schen ?? ", " ", MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2 ) == IDYES )
		{
			retcode =  Dateitesten(1,Dateiname) ;	// bitte loeschen
			if ( retcode == -1  )
			{
				sprintf ( ctext , "Zugriffsproblem Datei  %s", Dateiname ) ;
				MessageBox( ctext , " ", MB_OK|MB_ICONSTOP);
				retcode = -2 ;	// Durchmarsch
			}
		}
		else retcode = -2 ;	// Nicht loeschen - Durchmarsch
	}

	return retcode ;
/* ----->
	if ( retcode == 0 )
	{

		retcode = Dateischreiben (Dateiname) ;	// jetzt steht in retcode die Anzahl gechriebener Saetze

		if ( retcode < 0 )
		{
			Dateitesten (1,Dateiname);
			MessageBox("Datei kann nicht erstellt werden ", " ", MB_OK|MB_ICONSTOP);
		}
		if ( retcode == 0 )
		{
			Dateitesten (1, Dateiname );
			MessageBox("Datensatz nicht vorhanden ", " ", MB_OK|MB_ICONSTOP);
			retcode = -2 ;
		}
		if ( retcode > 0 )
		{
			char spruch [82] ;
			sprintf ( spruch , "Datei mit %d S�tzen wurde erstellt", retcode ) ;
			MessageBox( spruch , " ", MB_OK|MB_ICONSTOP);
			OnOK() ;
		}
	}

< ------ */

}

void holeeinhcode ( double px )
{
	for ( int ii = 0 ; ii < 25 ; ii ++ )
	{

		if ( px > meeinharti[ii] )
			if ( meeinharti[ii] > 0.0 )
				continue ;


		if ( px > meeinharti[ii] )	// schliesst 0 mit ein und ausserdem ist meinharti sortiert
		{	// eintrag existiert noch nicht 
			sprintf ( ptabn.ptitem , "me_einh_leed" ) ;
			sprintf ( ptabn.ptwer1 , "%1.0f" , px ) ;
			int dretpt = ptabn_class.openptabnw1 () ;
			dretpt = ptabn_class.leseptabnw1 () ;
			if ( dretpt )	// darf eigentlich nicht sein
				sprintf ( ptabn.ptwer2 ,"xxxxx" ) ;

			if ( meeinharti[ii] == 0.0 && ii == 0 )
			{	// bin bereits am Ende angekommen
				sprintf ( meeinhcode[ii] ,"%s" , ptabn.ptwer2 ) ;
				meeinharti[ii] = px ;
				return ;
			}

// sortieren und eintragen
			int jj = ii ;
			while ( meeinharti[jj ] > 0.0 )
			{
				jj ++ ;
				if ( jj > 24 ) return ;	// notbremse
			}	// Jetzt zeigt jj auf den ersten freien Platz

			while ( jj > 0 )
			{
				if ( px > meeinharti [ jj -1 ] )
				{
					sprintf ( meeinhcode[jj] ,"%s" , ptabn.ptwer2 ) ;
					meeinharti[jj] = px ;
					return ;
				}
				meeinharti[jj] = meeinharti[jj -1 ] ;
				sprintf( meeinhcode[jj] ,"%s", meeinhcode[jj - 1 ] ) ;
				jj -- ;
			}
			sprintf ( meeinhcode[jj] ,"%s" , ptabn.ptwer2 ) ;
			meeinharti[jj] = px ;
			return ;
		}

		if ( px == meeinharti[ii] )
		{
			sprintf ( ptabn.ptwer2 , "%s" , meeinhcode[ii] ) ;
			return ;
		}


	}
}

int CdachsascDlg::schreibevorsatz ( void )
{
	if ( dtvorsatz ) 
		return 0 ;	// alles gelaufen ....
	if (opendatei ()) 
		return -1 ;	// Dateierstellung fehlgeschlagen

	for ( int ii = 0 ; ii < 25 ; ii++ )
	{	// initialisiere schluesselamtrix 
		meeinharti[ii] = 0.0 ;
		meeinhcode[ii][0] = '\0' ;
	}
	
	
	
	//	Es folgt das Schreiben des Vorsatzes (KVOR_

	satzzahl = 0 ;

	sprintf ( kvor_satza, "KVOR_" )		 ;	// [6] immer "KVOR_"
	sprintf ( kvor_gsber, "01" )		 ;	// [3] immer "01"
	sprintf ( kvor_kvkda , "%s", abkda ) ;	// [9] Absender-Nummer
	sprintf ( kvor_dtkda , "%s", dtkda ) ;	// [9] Dachser-NL
	sprintf ( kvor_kvkvn, "%05d", vorsatznummer )   ;	// [6] Vorsatznummer

/* ----->
	char kvor_d1kvd[3] ;	// Crea-Datum dd
	char kvor_d2kvd[3] ;	// Crea-Datum mm
	char kvor_d3kvd[3] ;	// Crea-Datum yy
	char kvor_d4kvd[3] ;	// Crea-Datum jhd
	char kvor_z1kvd[3] ;	// Crea-Datum hh
	char kvor_z2kvd[3] ;	// Crea-Datum min
< ----- */
	char dummy[3] ;
	DachsDatum( 1 , dummy , kvor_d1kvd , kvor_d2kvd , kvor_d3kvd ,
				kvor_d4kvd , kvor_z1kvd , kvor_z2kvd ) ;

//                          1234567890
//	sprintf ( kvor_kvtnk , "          " ) ;	// [11]  "o"-alpha Trapo-Nummer
	fuellefeld ( "   " , kvor_kvtnk , 10 , ' ' ) ;
//	sprintf ( kvor_kvauf , "00000" ) ;		// [6] 	 "o"-nummer. Anzahl Auftr.
	sprintf ( kvor_kvauf , "%05d" ,gesamtcount ) ;		// [6] 	 "o"-nummer. Anzahl Auftr.
	sprintf ( kvor_kvdst , " " ) ;			// [2] 	 "o"-alpha Reservefeld
/*  056 -> crlf */


	sprintf ( bufh, "%s%s%s%s%s%s%s%s%s%s%s%s%s%s" 
			, kvor_satza
			, kvor_gsber
			, kvor_kvkda
			, kvor_dtkda
			, kvor_kvkvn

			, kvor_d1kvd
			, kvor_d2kvd
			, kvor_d3kvd
			, kvor_d4kvd
			, kvor_z1kvd
			, kvor_z2kvd

			, kvor_kvtnk
			, kvor_kvauf
			, kvor_kvdst
	) ;

	int pointi = 56 ;
	bufh[pointi] = 0x0d ;
	bufh[pointi + 1 ] = 0x0a ;
	bufh[pointi + 2 ] = '\0' ;
	_fmode = _O_BINARY ;
	int	df = fputs ( bufh,fpdatei ) ;
	if ( df == EOF ) 
	{
		_fmode = _O_TEXT ;
//			sprintf(debugtext,"Fehler beim Datei-Schreiben %s ", filenametmp );
//			WriteLog(iBASDIR, debugtext);
		fclose ( fpdatei ) ;
//		unlink ( Dateiname ) ;
//		Sleep (500 ) ;
		return -1 ;
	}

	dtvorsatz = 1 ;
	return 0 ;
}
static char okstring[33] ;
static char okstring2[33] ;


char * generierenve ( char * innve )
{

int point1 ;
int  wicht ;
int platz ;
char testchars[2] ; 
int summe ;

  sprintf ( okstring , "%s" , innve ) ;
 
  point1 = 0 ;
  summe  = 0 ;
  wicht  = 3 ;

  // Beachte : das funzt nur, wenn die ersten 2 Stellen der insgesamt 20 stelligen NVE konstant auf "00" stehen
  // oder zumindest in der Pruefsumme einen Summand von n * 10 ergeben  und daher neutral sind 
  // erlaubte Werte : "00" , "17" , "24" , "31" , "48" , "55" , "62" , "79" , "86" , "93" 
  // dier ersten 18 Stellen werden verknotet
  // falls der input-string kuerzer ist, wird er mit 000 aufgefuellt,
  // falls nicht-nummer-Zeichen drin sind, wird ebenfalls mit "0" aufgefuellt
  testchars[1] = '\0' ;
  while ( point1 < 17 )
  {
      testchars[0] = okstring[point1] ;
	  if ( testchars[0] < '0' || testchars[0] > '9' )
	  {
		  if ( testchars[0] == '\0' )
		  {
			  okstring[point1 + 1 ] = '\0' ;	// stringende fortplanzen
		  }
		  okstring[point1 ] = '0'		;	// Nicht-alpha durch "0" ersetzen bzw. string verl�ngern
		  testchars[0] = '0'			;
	  }
      platz    = atoi ( testchars ) ;
      summe += ( platz * wicht ) ;
      if ( wicht == 1 ) 
		wicht = 3 ;
      else
		wicht = 1 ;
      
      point1 ++ ; 
  }
  platz =  summe % 10 ;            /* Mod 10  */
  if ( platz == 0 )
 	  sprintf ( testchars, "0" ) ;
  else
	  sprintf ( testchars , "%d" , 10 - platz ) ; 		// 220311 : platz -> 10 - platz das isses 

  sprintf ( okstring + 17,  "%s" , testchars ) ;

   return ( okstring ) ;

}

char * nveausiln ( long lwert )
{
// ACHTUNG : long kann nur 8 Stellen transportieren !!!!!!!!!! ACHTUNG

// Struktur der NVE :  "3" + mdn.iln[0->6] + lfd[9stellig]

// die "3" ist "frei verf�gbar, aber in der Regel IMMER == "3" ....

// das die Mandanten-ILN korrekt und aktuell ist, sollte vorher klar sein 
	sprintf ( okstring2, "3%s", mdn.iln );
	sprintf ( okstring2 + 8 , "%09ld" , lwert ) ;
	sprintf ( okstring2 , "%s" , generierenve(okstring2 )) ;

	return okstring2 ;
}


char * ohnekomma ( int ges , int nk , double wert )
{
  char maske[22] ;
	sprintf ( maske , "%%0%d.0f" , ges ) ;
	while ( nk > 0 )
	{
		wert *= 10.0 ;
		nk -- ;
	}
	sprintf ( okstring , maske , wert ) ;
	return okstring ;
}

int CdachsascDlg::schreibekopfsatz ( void )
{
	int dretlsp ;
	int dretkun ;
	int dretzudax ;
	int dretadr ;

	if ( schreibevorsatz())
		return -1 ;

	for ( int ii = 0 ; ii < 15 ; ii ++ )
	{
		kppazei[ii].lfd = 0 ;
		kppazei[ii].kpzei = 0 ;
	}

	kun.mdn = mdn.mdn ;
	kun.kun = lsk.kun ;
	dretkun = kun_class.openkun () ;
	dretkun = kun_class.lesekun () ;	// ich werte hier nix aus, es muss einfach alles da sein .....
	adr.adr = lsk.adr ;
	dretadr = adr_class.openadr () ;
	dretadr = adr_class.leseadr () ;	// ich werte hier nix aus, es muss einfach alles da sein .....

	zudax.mdn = mdn.mdn ;
	zudax.kun = lsk.kun ;
	zudax.sped = DACHSER ;
	dretzudax = zudax_class.openzudax () ;
	dretzudax = zudax_class.lesezudax () ;
	if ( dretzudax )
	{
		zudax.kun = 0 ;
		dretzudax = zudax_class.openzudax () ;
		dretzudax = zudax_class.lesezudax () ;
	}
	if ( dretzudax )
	{
		// Not-defaults ......
		zudax.lief_art = 0 ;	// "normale brutto-rechnung"
		zudax.sped  = 1 ;	// Dachser
	}

// es folgt die Erzeugung von gesgew und geswert auf Basis lsp

	lsp.mdn = lsk.mdn ;
	lsp.ls = lsk.ls ;
	lsp.fil = lsk.fil ;
	dretlsp = lsp_class.openlsp () ;
	dretlsp = lsp_class.leselsp () ;
	lsgesgewn = 0.0 ;
	lsgesgewt = 0.0 ;
	lsgeswert = 0.0 ;
	while ( ! dretlsp ) 
	{
		lsgeswert += lsp.lief_me * lsp.ls_vk_euro ;

		if ( a_bas.a_typ == 11 )
		{
			if ( a_bas.me_einh != 2 )
				lsgesgewt += lsp.lief_me * a_bas.a_gew  ;
			else
				lsgesgewt += lsp.lief_me ;
		}
		else
		{
			if ( a_bas.me_einh != 2 )
				lsgesgewn += lsp.lief_me * a_bas.a_gew  ;
			else
				lsgesgewn += lsp.lief_me ;
		}
	dretlsp = lsp_class.leselsp () ;
	}
	if ( zudax.lief_art == 0 )
	{
	}
	if ( zudax.lief_art == 1 )
	{
		// netto-netto
	}
	if ( zudax.lief_art == 2 )
	{
		// Zuschlagsrechnung
	}


//	Es folgt das Holen von Stammdaten und Schreiben des Kopfsatzes ( KSTA_ )

	sprintf ( ksta_satza, "KSTA_" ) ;	// [6] immer "KSTA_"
	sprintf ( ksta_gsber , "01" ) ;	// [3] immer "01"
	sprintf ( ksta_kvkda , "%s" , abkda ) ;	// [9] Absender-Nummer
	sprintf ( ksta_dtkda , "%s" , dtkda ) ;	// [9] Dachser-NL
	sprintf ( ksta_kvkvn , "%s" , kvor_kvkvn ) ;	// [6] Vorsatznummer

	satzzahl ++ ;
	sprintf ( ksta_sspba , "%05d" , satzzahl );	// [6] lfd.Nr im Vorsatz
	sprintf ( ksta_sskda , "%s" ,abkda ) ;	// [9] Kunu Fakturier-Absender
	sprintf ( ksta_ssik1 , " " ) ;			// [2] "o"-alpha Auf-gruppe

// 220311 : Bei Dietz ist das ein Pflichtfeld 1 == EDEKA/NETTO , 2 = Rest 
	if ( anwender == ANWDIETZ )
		sprintf ( ksta_ssik1 , "2" ) ;			// Default-Rest
// 220311 : aufgruppe dazugebaut 
	zudax.aufgruppe[1] = '\0' ;
	if ( zudax.aufgruppe[0] != '\0' && zudax.aufgruppe[0] != ' ' )
		sprintf ( ksta_ssik1 , zudax.aufgruppe ) ;

	sprintf ( ksta_sssan , " " ) ;			// [2] "o"-alpha Reserve-Feld

	sprintf ( bufh , "%d", lsk.ls ) ;		// auffuellen mit folge-blanks ..

	fuellefeld ( bufh , ksta_ssskd ,  17 , ' ' ) ;	// [18] Sendungsid

	fuellefeld ( adr.adr_nam1 , ksta_ssn1e, 30, ' ' ) ;	// [31] // Name1
	fuellefeld ( adr.adr_nam2 , ksta_ssn2e, 30, ' ' ) ;	// [31] // "o" Name2
	fuellefeld ( " " , ksta_ssn3e, 30, ' ' ) ;			// [31] // "o" Name3
	fuellefeld ( adr.str ,  ksta_sssre, 30 , ' ' ) ;	// [31] // Str+Hausnummer

	if ( adr.staat == 0 )
		sprintf ( ksta_sslde , "D  " ) ;
	else
	{
		sprintf ( ptabn.ptwert , "%d" , adr.staat ) ;
		sprintf ( ptabn.ptitem , "staat" ) ;
		int xxx = ptabn_class.openptabn () ;
		if ( ptabn_class.leseptabn ())
		{	// Notbremse 
			sprintf ( ksta_sslde , "D  " ) ;
		}
		else
		{
			// hier sollte halt mal der postalische code stehen
			fuellefeld ( ptabn.ptwer2 , ksta_sslde, 3 , ' ' ) ;	// [4] // Nation
		}
	}
	fuellefeld ( adr.plz , ksta_ssple , 6 , ' ' ) ;	// [7] 	// PLZ
	fuellefeld (adr.ort1 , ksta_ssore, 26 , ' ' ) ;		//[27] 	// Ort
	fuellefeld ( " " , ksta_sszuh , 30 , ' ' ) ;	// [31] // "o" Adresserg.

	fuellefeld ( "000" ,ksta_ssanr , 3 , '0' ) ;	// [4] 	// "o" Unterrel.
	fuellefeld ( " " , ksta_ssrev , 4 , ' ' ) ;		// [5]	// "m"/"o" Verladerel.
	fuellefeld ( " " , ksta_ssldb , 3 , ' ' ) ;		// [4] 	// "o" Postcode
	fuellefeld ( " " , ksta_ssplb , 6 , ' ' ) ;		// [7] 	// "o" PLZ zu ssldb
	fuellefeld ( " " , ksta_ssorb , 26 , ' ' ) ;		// [27]	// "o" Ort
	fuellefeld ( " " , ksta_sslag , 3 , ' ' ) ;		// [4] 	// "o" Ausland-Gr.
	fuellefeld ( " " , ksta_sspag , 6 , ' ' ) ;		// [7] 	// "o" Ausland-pz-Gr.
	fuellefeld ( " " , ksta_ssoag , 26 , ' ' ) ;		// [27]	// "o" Ausland-ort-Gr.
	fuellefeld ( " " , ksta_sstdo , 1 , ' ' ) ;		// [2] 	// Reserve blank

	sprintf ( ksta_sszgu , "N" ) ;		// [2] // Zollgut J/N -> immer nein
	sprintf ( ksta_sssah , "N" ) ;		// [2] // Selbstabholer == "N"
	sprintf ( ksta_ssheb , "N" ) ;		// [2] // Hebebuehne == "N"
	sprintf ( ksta_sstr1 , "F" ) ;		// [2] // Sparte == "F"	lt. Guggemoos 2004

	sprintf ( ksta_sstr2 , "Y" ) ;		// [2] // Abfertigung "S" oder "Y"  oder "E" lt Guggemoos 2004
	// 2210311 : "Y"->"N" fuer dietz
	if  ( anwender == ANWDIETZ )
		sprintf ( ksta_sstr2 , "N" ) ;		// [2] // Abfertigung 

	if ( zudax.abfert[0] != '\0' && zudax.abfert[0] != ' ' )
		sprintf ( ksta_sstr2 , zudax.abfert ) ;
	sprintf ( ksta_sstr3, " " ) ;		// [2] // Gefahrgut z.B. 1,2,3,4 
	sprintf ( ksta_ssfkt, "999" ) ;		// [4] // Frankatur
	if ( anwender == ANWDIETZ )
		sprintf ( ksta_ssfkt, "031" ) ;		// [4] // 220311 : default fuer Dietz 

	if ( zudax.dfrank[0] != '\0' && zudax.dfrank[0] != ' ' )
		fuellefeld ( zudax.dfrank, ksta_ssfkt , 3 , ' ' ) ; 

	sprintf (  ksta_sssvv , " " ) ;		// [2]  Reservefeld

//	sprintf ( ksta_sswwe , "%s" , ohnekomma ( 9 ,2, 0 ) ) ;	// [10] // "m"/"o" Warenwert fuer Versicherung 

	lsp.mdn = lsk.mdn ;
	lsp.fil = lsk.fil ;
	lsp.ls = lsk.ls ;

//	double lswert = lsp_class.leselswert() ;

	sprintf ( ksta_sswwe , "%s" , ohnekomma ( 9 ,2, lsgeswert ) ) ;	// [10] // "m"/"o" Netto-Warenwert fuer Versicherung 

	sprintf ( ksta_sswhs , "EUR" ) ;						//	[4]	// Waehrung
	fuellefeld ( " " , ksta_ssfwb , 13 , ' ' ) ;			// [14] // Reserve
	fuellefeld ( " " , ksta_ssiws , 9 , ' ' ) ;				// [10] // Reserve

	fuellefeld ( "00" , ksta_smreb, 9 , '0') ;		// Basis-Fuellung [10] // Nachnahme
	sprintf ( ksta_swwco , "00"	) ;		//	[3] m/o : Nachnahme-Code ,steht viel weiter hinten(pos 425 )

	if ( kun.zahl_art == NACHNAHME ) 
	{
		// das folgende funktioniert nur bei Einzelrechnung je Beleg 

		if ( lsk.rech > 0 && lsk.ls_stat > 5 )
		{
			rech.mdn = lsk.mdn ;
			rech.rech_nr = lsk.rech ;
			sprintf ( rech.blg_typ , "%s",  lsk.blg_typ ) ;
			rech.kun = lsk.inka_nr ;
			int dir = rech_class.openrech() ;
			dir = rech_class.leserech () ;
			if ( ! dir )
			{
				sprintf ( ksta_smreb , "%s" , ohnekomma ( 9 ,2, rech.rech_bto_eur ) ) ;	// [10]  
				sprintf ( ksta_swwco , "01"	) ;		//	01 == kassiere bar
													//	02 == kassiere Scheck
													//	03 == kassiere bankbest. Scheck
			}
			else
			{
				// Fehlermeldung  : Rechnung fehlt
			}
		}
		else
		{
				// Fehlermeldung  : Rechnung fehlt
		}
	}

	sprintf ( ksta_smwhs , "EUR" )				;	// [4]	// Waehrung
	fuellefeld ( " " ,  ksta_smfwb , 13 , ' ' ) ;	// [14]	// Reserve
	fuellefeld ( " " ,  ksta_sstvk , 1 , ' ' ) ;	// [2] 	// Reserve

	if ( ksta_sstr2[0] == 'Q' )	// Abfertigung "Fixtermin"
	{
		// nur bei fix-termin-Abfertigung auch ausfuellen, sonst ausnullen
		if ( lsk.fix_dat.year > 2009 )	// schauen, ob was sinnvolles drin steht .....
		{
			sprintf ( ksta_d1ssf , "%02d" , lsk.fix_dat.day )      ;	// 	[3] // Fixtermin dd
			sprintf ( ksta_d2ssf , "%02d" , lsk.fix_dat.month )    ;	//[3] ;	// Fixtermin mm
			sprintf ( ksta_d3ssf , "%02d" , lsk.fix_dat.year - 2000 ) ;	//[3] ;	// Fixtermin yy
			sprintf ( ksta_d4ssf , "20" ) ;								//[3] Fixtermin jhd
		}
		else
		{
			sprintf ( ksta_d1ssf , "%02d" , lsk.lieferdat.day )    ;		// [3] Fixtermin dd
			sprintf ( ksta_d2ssf , "%02d" ,  lsk.lieferdat.month ) ;		// [3] Fixtermin mm
			sprintf ( ksta_d3ssf , "%02d" , lsk.lieferdat.year - 2000  ) ;	// [3] Fixtermin yy
			sprintf ( ksta_d4ssf , "20"	) ;									// [3] Fixtermin jhd
		}
	}
	else	// alles ausnullen .....
	{
			sprintf ( ksta_d1ssf , "00" ) ;		// [3] Fixtermin dd
			sprintf ( ksta_d2ssf , "00" ) ;		// [3] Fixtermin mm
			sprintf ( ksta_d3ssf , "00" ) ;		// [3] Fixtermin yy
			sprintf ( ksta_d4ssf , "00"	) ;		// [3] Fixtermin jhd
	}
	// die folgenden Termin mache ich mal alle platt  .....

	sprintf ( ksta_d1ssv , "00" ) ;		//[3] ;	// Zustellv dd
	sprintf ( ksta_d2ssv , "00" ) ;		//[3] ;	// Zustellv mm
	sprintf ( ksta_d3ssv , "00" ) ;		//[3] ;	// Zustellv yy
	sprintf ( ksta_d4ssv , "00" ) ;		//[3] ;	// Zustellv jhd
	sprintf ( ksta_z1ssv , "00" ) ;		//[3] ;	// Zustellv hh
	sprintf ( ksta_z2ssv , "00" ) ;		//[3] ;	// Zustellv min

	sprintf ( ksta_d1ssb , "00" ) ;		//[3] ;	// Zustellb dd
	sprintf ( ksta_d2ssb , "00" ) ;		//[3] ;	// Zustellb mm
	sprintf ( ksta_d3ssb , "00" ) ;		//[3] ;	// Zustellb yy
	sprintf ( ksta_d4ssb , "00" ) ;		//[3] ;	// Zustellb jhd
	sprintf ( ksta_z1ssb , "00" ) ;		//[3] ;	// Zustellb hh
	sprintf ( ksta_z2ssb , "00" ) ;		//[3] ;	// Zustellb min

	fuellefeld ( kun.iln , ksta_ssiln, 13 , ' ' ) ;	//[14] ;	// Empf-ILN
fuellefeld ( " " ,ksta_ssaua, 2, ' ' ) ;	// [3] 	// Reserve blank
/*  442 -> crlf */


	sprintf ( bufh, "%s%s%s%s%s%s%s%s%s%s" ,

		/*  000 */	ksta_satza ,
		/*  005 */	ksta_gsber ,
		/*  007 */	ksta_kvkda ,
		/*  015 */	ksta_dtkda ,
		/*  023 */	ksta_kvkvn ,

		/*  028 */	ksta_sspba ,
		/*  033 */	ksta_sskda ,	
		/*  041 */	ksta_ssik1 ,
		/*  042 */	ksta_sssan ,
		/*  043 */	ksta_ssskd ) ;

	sprintf ( bufh + 60 , "%s%s%s%s%s%s%s%s%s%s" ,
		/*  060 */	ksta_ssn1e ,
		/*  090 */	ksta_ssn2e ,
		/*  120 */	ksta_ssn3e ,
		/*  150 */	ksta_sssre ,
		/*  180 */	ksta_sslde ,

		/*  183 */	ksta_ssple ,
		/*  189 */	ksta_ssore ,
		/*  215 */	ksta_sszuh ,
		/*  245 */	ksta_ssanr , 
		/*  248 */	ksta_ssrev ) ;

	sprintf ( bufh + 252 , "%s%s%s%s%s%s%s%s%s%s" ,
		/*  252 */	ksta_ssldb ,
		/*  255 */	ksta_ssplb ,
		/*  261 */	ksta_ssorb ,
		/*  287 */	ksta_sslag ,
		/*  290 */	ksta_sspag ,

		/*  296 */	ksta_ssoag ,
		/*  322 */	ksta_sstdo ,
		/*  323 */	ksta_sszgu ,
		/*  324 */	ksta_sssah ,
		/*  325 */	ksta_ssheb ) ;

	sprintf ( bufh + 326 , "%s%s%s%s%s%s%s%s%s%s" ,
		/*  326 */	ksta_sstr1 ,
		/*  327 */	ksta_sstr2 ,
		/*  328 */	ksta_sstr3 ,
		/*  329 */	ksta_ssfkt ,
		/*  332 */	ksta_sssvv ,

		/*  333 */	ksta_sswwe ,
		/*  342 */	ksta_sswhs ,
		/*  345 */	ksta_ssfwb ,
		/*  358 */	ksta_ssiws ,
		/*  367 */	ksta_smreb ) ;

	sprintf ( bufh + 376 , "%s%s%s%s%s%s%s%s%s%s" ,
		/*  376 */	ksta_smwhs ,
		/*  379 */	ksta_smfwb ,
		/*  392 */	ksta_sstvk ,
		/*  393 */	ksta_d1ssf ,
		/*  395 */	ksta_d2ssf ,

		/*  397 */	ksta_d3ssf ,
		/*  399 */	ksta_d4ssf ,
		/*  401 */	ksta_d1ssv , 
		/*  403 */	ksta_d2ssv ,
		/*  405 */	ksta_d3ssv ) ;

	sprintf ( bufh + 407 , "%s%s%s%s%s%s%s%s%s%s" ,
		/*  407 */	ksta_d4ssv ,
		/*  409 */	ksta_z1ssv ,
		/*  411 */	ksta_z2ssv ,
		/*  413 */	ksta_d1ssb ,
		/*  415 */	ksta_d2ssb ,

		/*  417 */	ksta_d3ssb ,
		/*  419 */	ksta_d4ssb ,
		/*  421 */	ksta_z1ssb ,
		/*  423 */	ksta_z2ssb ,
		/*  425 */	ksta_swwco ) ;

	sprintf ( bufh + 427 , "%s%s%" ,
		/*  427 */	ksta_ssiln ,
		/*  440 */	ksta_ssaua ) ;

	int pointi = 442 ;
	bufh[pointi] = 0x0d ;
	bufh[pointi + 1 ] = 0x0a ;
	bufh[pointi + 2 ] = '\0' ;
	_fmode = _O_BINARY ;
	int	df = fputs ( bufh,fpdatei ) ;
	if ( df == EOF ) 
	{
		_fmode = _O_TEXT ;
//			sprintf(debugtext,"Fehler beim Datei-Schreiben %s ", filenametmp );
//			WriteLog(iBASDIR, debugtext);
		fclose ( fpdatei ) ;
//		unlink ( Dateiname ) ;
//		Sleep (500 ) ;
		return -1 ;
	}
	return 0 ;
}

int CdachsascDlg::schreibepspm ( void )
{
// 290911 : R�ckgabe ist poskpap oder nicht .....
	int retcode = 0 ;	// 290911


	if ( !dtvorsatz )	// Datei nicht geoeffnet
		return -1 ;

	double pxzahl ;
	double px ;

	for ( int inpspm = 0 ; inpspm < 4 ; inpspm ++ )
	{

		if (inpspm == 0 )
		{
			if ( trapopspm.ps1zahl < 0.01
				&& trapopspm.ps2zahl < 0.01
				&& trapopspm.ps3zahl < 0.01
				&& trapopspm.ps4zahl < 0.01 )
			{	// lose Packst�cke
				pxzahl = trapopspm.pmzahl ;
				px     = trapopspm.pm ;
				inpspm = 5 ;	// Schleifenende, Marker usw.
			}
			else
			{
				if ( trapopspm.ps1zahl > 0.01 )
				{
					pxzahl = trapopspm.ps1zahl ;
					px     = trapopspm.ps1 ;
				}
				else continue ;
			}
		}
		if (inpspm == 1 )
		{
			if ( trapopspm.ps2zahl > 0.01 )
			{
				pxzahl = trapopspm.ps2zahl ;
				px     = trapopspm.ps2 ;
			}
			else continue ;
		}
		if (inpspm == 2 )
		{
			if ( trapopspm.ps3zahl > 0.01 )
			{
				pxzahl = trapopspm.ps3zahl ;
				px     = trapopspm.ps3 ;
			}
			else continue ;
		}

		if (inpspm == 3 )
		{
			if ( trapopspm.ps4zahl > 0.01 )
			{
				pxzahl = trapopspm.ps3zahl ;
				px     = trapopspm.ps3 ;
			}
			else continue ;
		}

		sprintf ( kzei_satza, "KZEI_" ) ;				// [6] immer KZEI
		sprintf ( kzei_gsber ,"01" ) ;					// immer "01"
		sprintf ( kzei_kvkda ,"%s" , abkda ) ;			// [9] Absender-Nummer
		sprintf ( kzei_dtkda , "%s" , dtkda ) ;			// Dachser-NL
		sprintf ( kzei_kvkvn , "%s" , ksta_kvkvn ) ;	// Vorsatznummer
		sprintf ( kzei_sspba , "%05d" , satzzahl ) ;	// lfd im Vorsatz
		sprintf ( kzei_szazl , "%03d" , posinzei ) ;	//[4] lfd. sendepos(kzei in ksta_)

		if ( trapopspm.lfd  > 0 && trapopspm.lfd  < 15 )
		{
			if ( kppazei[trapopspm.lfd].lfd == 0 )
			{
				kppazei[trapopspm.lfd].lfd = trapopspm.lfd ;
				kppazei[trapopspm.lfd].kpzei = posinzei ;
			}
			else
			{
				kppazei[trapopspm.lfd].kpzei = 0 ;	// mehrfachzuordnung 
			}
		}

		posinzei ++ ;

		sprintf ( kzei_szaps , "%05.0f" , pxzahl  ) ;		// [6] Anz. Verp.
	
		holeeinhcode ( px ) ;
		fuellefeld ( ptabn.ptwer2 + 2 ,  kzei_szvpa , 3 , ' ' ) ; // kzei_szvpa[4] Verp.-Art

		fuellefeld ( "Fleischwaren" , kzei_szinh , 20 , ' ' ) ;	// [21]  Beschr. Packst�ck
		if ( posinzei == 2 )	// Gesamtgewicht in erster Zeile eintragen ........
			fuellefeld ( ohnekomma ( 5, 0, lsgesgewn + lsgesgewt ) , kzei_szgew , 5 , '0' ) ;
		else
			fuellefeld ( "00" , kzei_szgew , 5 , '0' ) ;	//  bruttogew. - nur je gesamte Sendung 
		fuellefeld ( "00" , kzei_sznet , 5 , '0' ) ;	// [6] nettogew. - nie wichtig  
		fuellefeld ( "00" , kzei_szcbm , 7 , '0' ) ;	// [8] // Kubmeter.  ( 7/3 )
		fuellefeld ( "00" , kzei_szldm , 3 , '0' ) ;	// [4] // Lademeter. ( 3/1 )
		fuellefeld ( "  " , kzei_szlkl , 7 , ' ' ) ;	// [8] Reserve blank

		fuellefeld ( "00" , kzei_szgua , 4 , '0' ) ;	// [5] Guterart - immer 0
		fuellefeld ( "00" , kzei_szame , 5 , '0' ) ;	// [6]  Anz. ME WG -immer 0
		fuellefeld ( "05" , kzei_szwgp , 2 , ' ' ) ;	// [3] // WG -  05 = allgemein

		if ( anwender == ANWDIETZ )	// 220311 
			fuellefeld ( "02" , kzei_szwgp , 2 , ' ' ) ;	// WG -  02 = "Rest" lt. Anforderung


		if ( zudax.be_logd [0] != ' ' && zudax.be_logd [0] != '\0' )
		{
			fuellefeld ( zudax.be_logd , kzei_szwgp , 2 , ' ' ) ;	// [3] // WG -  05 = allgemein
		}

		fuellefeld ( " "  , kzei_szkul , 1 , ' ' ) ;	// [2] // Res blank
		fuellefeld ( "  " , kzei_sztpv , 3 , ' ' ) ;	// [4] // Res blank
		fuellefeld ( "  " , kzei_sztpb , 3 , ' ' ) ;	// [4] // Res blank
		fuellefeld ( "  " , kzei_szzei , 17 , ' ') ;	// [18] "o" - Markierung
		fuellefeld ( "00" , kzei_szlng , 5 , '0' ) ;	// [6] laenge in Meter
		fuellefeld ( "00" , kzei_szbre , 5 , '0' ) ;	// [6] breite in Meter
		fuellefeld ( "00" , kzei_szhoh , 5 , '0' ) ;	// [6] hoehe in Meter
		if ( inpspm == 5 )	// es gibt kein zugeordnetes Ladehimi ( keinen kpap-Satz )
			fuellefeld ( "00" , kzei_szpos , 3 , '0' ) ; 
		else
		{
			sprintf ( kzei_szpos , "%03d" ,  poskpap ) ;		// [4]  Packmittelpos.
// 290911 : verladen auf ....			poskpap  ++ ;

			retcode = 1 ;	// 290911
		}
		fuellefeld ( "00" , kzei_szwnr , 9 , '0' ) ;	// [10] Zoll-W-Nr.

		sprintf ( bufh  , "%s%s%s%s%s%s%s%s%s%s" ,
			/*  000 */	kzei_satza ,
			/*  005 */	kzei_gsber ,
			/*  007 */	kzei_kvkda ,
			/*  015 */	kzei_dtkda ,
			/*  023 */	kzei_kvkvn ,

			/*  028 */	kzei_sspba ,
			/*  033 */	kzei_szazl ,
			/*  036 */	kzei_szaps ,
			/*  041 */	kzei_szvpa ,
			/*  044 */	kzei_szinh ) ;

		sprintf ( bufh + 64  , "%s%s%s%s%s%s%s%s%s%s" ,
			/*  064 */	kzei_szgew ,
			/*  069 */	kzei_sznet ,
			/*  074 */	kzei_szcbm ,
			/*  081 */	kzei_szldm ,
			/*  084 */	kzei_szlkl ,
 
			/*  091 */	kzei_szgua ,
			/*  095 */	kzei_szame ,
			/*  100 */	kzei_szwgp ,
			/*  102 */	kzei_szkul ,
			/*  103 */	kzei_sztpv ) ;

		sprintf ( bufh + 106 , "%s%s%s%s%s%s%s" ,
			/*  106 */	kzei_sztpb ,
			/*  109 */	kzei_szzei ,
			/*  126 */	kzei_szlng ,
			/*  131 */	kzei_szbre ,
			/*  136 */	kzei_szhoh ,

			/*  141 */	kzei_szpos ,
			/*  144 */	kzei_szwnr ) ;

		int pointi = 153 ;
		bufh[pointi] = 0x0d ;
		bufh[pointi + 1 ] = 0x0a ;
		bufh[pointi + 2 ] = '\0' ;
		_fmode = _O_BINARY ;
		int	df = fputs ( bufh,fpdatei ) ;
		if ( df == EOF ) 
		{
			_fmode = _O_TEXT ;
//			sprintf(debugtext,"Fehler beim Datei-Schreiben %s ", filenametmp );
//			WriteLog(iBASDIR, debugtext);
			fclose ( fpdatei ) ;
//			unlink ( Dateiname ) ;
//			Sleep (500 ) ;
			return -1 ;
		}
	}	// ende der for-Schleife 
	return retcode ;	// 290911 : returncode anstelle errcode
}

int CdachsascDlg::schreibenve ( void )
{
	if ( !dtvorsatz )	// Datei nicht geoeffnet
		return -1 ;

	// Schreibe NVE-Zeile KPPA_ ( Packstueck-Identifikation ) 

	sprintf ( kppa_satza , "KPPA_" ) ;				// [6] immer "KPPA_"
	sprintf ( kppa_gsber , "01" ) ;					// [3] immer "01"
	sprintf ( kppa_kvkda , abkda ) ;				// [9] Absender-Nummer
	sprintf ( kppa_dtkda ,  dtkda ) ;				// [9] Dachser-NL
	sprintf ( kppa_kvkvn , "%s" , ksta_kvkvn ) ;	// [6] Vorsatznummer

	sprintf ( kppa_ssbpa , "%05d" , satzzahl ) ;		// [6] lfd-Nr
	sprintf ( kppa_kplfd , "%05d%" , poskppa ) ;		// [6] lfd-Nr Packst-ID
	poskppa ++ ;

	char kppikhilfe[88] ;
//	sprintf ( kppikhilfe , "00%s" , nveausiln ( 12973442188 )) ;
	sprintf ( kppikhilfe , "00%s" , generierenve ( traponve.nve + 2 )) ;	// 220311 "+2" wegen 18 stellen, nur TestAblauf
//  100211 : komt bereits fertig in traponve.nve an ( 20 stellen alpha ..... )
	sprintf ( kppikhilfe , "%s" , traponve.nve ) ;

	// lt. Doku wird die PZ ueber alle 20 Stellen gezogen, da es aber geradzahlig 2 Nullen sind, aendert sich nix
	// bei ungeradzahlig anderer Laenge muesste beim rechnen das Startgewicht vertauscht werden,
	// weil formal von rechts nach links gerechnet wird

	fuellefeld ( kppikhilfe , kppa_kppik , 35 , ' ' ) ;	// [36] Packst-ID(NVE) 20 stellen ( 18 + 2 fuehr-Nullen)

	sprintf ( kppa_kpcak, "001" ) ;						// [4] Codeart -immer 001 == NVE


	if (  traponve.lfd < 15 && traponve.lfd  > 0 )
		sprintf ( kppikhilfe , "%03d" , kppazei[traponve.lfd].kpzei ) ; 
	else
		sprintf ( kppikhilfe , "000" ) ; 

	fuellefeld ( kppikhilfe , kppa_kpzei , 3 , '0' ) ;		// [4] auf-zeile ( nur, falls eindeutig zuordenbar ....


	sprintf ( bufh  , "%s%s%s%s%s%s%s%s%s%s" ,
	
	/*  000 */	kppa_satza ,
	/*  005 */	kppa_gsber ,
	/*  007 */	kppa_kvkda , 
	/*  015 */	kppa_dtkda ,
	/*  023 */	kppa_kvkvn ,

	/*  028 */	kppa_ssbpa ,
	/*  033 */	kppa_kplfd ,
	/*  038 */	kppa_kppik ,
	/*  073 */	kppa_kpcak ,
	/*  076 */	kppa_kpzei ) ;

	/*  079 -> crlf */

	int pointi = 79 ;
	bufh[pointi] = 0x0d ;
	bufh[pointi + 1 ] = 0x0a ;
	bufh[pointi + 2 ] = '\0' ;
	_fmode = _O_BINARY ;
	int	df = fputs ( bufh,fpdatei ) ;
	if ( df == EOF ) 
	{
		_fmode = _O_TEXT ;
//		sprintf(debugtext,"Fehler beim Datei-Schreiben %s ", filenametmp );
//		WriteLog(iBASDIR, debugtext);
		fclose ( fpdatei ) ;
//		unlink ( Dateiname ) ;
//		Sleep (500 ) ;
		return -1 ;
	}

	return 0 ;
}


int CdachsascDlg::schreibekpap (void ) 
{
	if ( !dtvorsatz )	// Datei nicht geoeffnet
		return -1 ;


	if ( trapopspm.ps1zahl < 0.01
			&& trapopspm.ps2zahl < 0.01
				&& trapopspm.ps3zahl < 0.01
					&& trapopspm.ps4zahl < 0.01 )
	{	// lose Packst�cke
				return 0 ;
	}

// da order by lfd drin steht, laufen die Saetze automatisch synchron mit dem kzei-Zeiger

	sprintf ( kpap_satza , "KPAP_" ) ;		// [6] immer "KPAP_"
	sprintf ( kpap_gsber , "01" ) ;			// [3] immer "01"
	sprintf ( kpap_kvkda , abkda ) ;		// [9] Absender-Nummer
	sprintf ( kpap_dtkda , dtkda ) ;		// [9] Dachser-NL
	sprintf ( kpap_kvkvn , ksta_kvkvn ) ;	// [6] Vorsatznummer

	sprintf ( kpap_ssbpa , "%05d" , satzzahl ) ;	// [6] lfd-Nr
	sprintf ( kpap_sppos , "%03d" , poskpap ) ;		//	[4]  lfd-Nr Packmittel
	poskpap ++ ;

	sprintf ( kpap_spanp ,"%05.0f" , trapopspm.pmzahl ) ;	// [6] Anz. Packmittel
	holeeinhcode ( trapopspm.pm ) ;
	fuellefeld ( ptabn.ptwer2 + 2 ,  kpap_spvpp , 3 , ' ' ) ; // [4] Verp.-Art

/*  044 -> crlf */

	sprintf ( bufh  , "%s%s%s%s%s%s%s%s%s" ,

		/*  000 */	kpap_satza ,
		/*  005 */	kpap_gsber ,
		/*  007 */	kpap_kvkda ,
		/*  015 */	kpap_dtkda ,
		/*  023 */	kpap_kvkvn ,

		/*  028 */	kpap_ssbpa ,
		/*  033 */	kpap_sppos ,
		/*  036 */	kpap_spanp ,
		/*  041 */	kpap_spvpp ) ;


/*  044 -> crlf */

	int pointi = 44 ;
	bufh[pointi] = 0x0d ;
	bufh[pointi + 1 ] = 0x0a ;
	bufh[pointi + 2 ] = '\0' ;
	_fmode = _O_BINARY ;
	int	df = fputs ( bufh,fpdatei ) ;
	if ( df == EOF ) 
	{
		_fmode = _O_TEXT ;
//		sprintf(debugtext,"Fehler beim Datei-Schreiben %s ", filenametmp );
//		WriteLog(iBASDIR, debugtext);
		fclose ( fpdatei ) ;
//		unlink ( Dateiname ) ;
//		Sleep (500 ) ;
		return -1 ;
	}

	return 0 ;
}
int CdachsascDlg::schreibeschlusssatz (void)
{
	if ( !dtvorsatz )	// Datei nicht geoeffnet
		return -1 ;
// schreibe Satz K999_

	sprintf ( k999_satza , "K999_" ) ;	// [6] immer "K999_"
	sprintf ( k999_daawi , "660113" ) ;	// [7] immer "601113" aetsch -> "660113"
	sprintf ( k999_d1dav , "%s" , kvor_d1kvd ) ;	//  [3]  Erstell-Dat dd
	sprintf ( k999_d2dav , "%s" , kvor_d2kvd ) ;	//  [3]  Erstell-Dat mm
	sprintf ( k999_d3dav , "%s" , kvor_d3kvd ) ;	//  [3]  Erstell-Dat yy
	sprintf ( k999_d4dav , "%s" , kvor_d4kvd ) ;	//  [3]  Erstell-Dat jhd
	sprintf ( k999_z1dav , "%s" , kvor_z1kvd ) ;	//  [3]  Erstell-Dat hh
	sprintf ( k999_z2dav , "%s" , kvor_z2kvd ) ;	//  [3]  Erstell-Dat min
	sprintf ( k999_dastp , "V " ) ;				//  [3]  immer "V "
	fuellefeld ( "3.1 " , k999_dakew , 10 , ' ' ) ;	// [11] ;	// immer "3.1      "

/*  35 -> crlf */

	sprintf ( bufh  , "%s%s%s%s%s%s%s%s%s%s" ,

		/*  000 */	k999_satza ,
		/*  005 */	k999_daawi ,
		/*  011 */	k999_d1dav ,
		/*  013 */	k999_d2dav ,
		/*  015 */	k999_d3dav ,

		/*  017 */	k999_d4dav ,
		/*  019 */	k999_z1dav ,
		/*  021 */	k999_z2dav ,
		/*  023 */	k999_dastp ,
		/*  025 */	k999_dakew  ) ;




/*  35 -> crlf */
	int pointi = 35 ;
	bufh[pointi] = 0x0d ;
	bufh[pointi + 1 ] = 0x0a ;
	bufh[pointi + 2 ] = '\0' ;
	_fmode = _O_BINARY ;
	int	df = fputs ( bufh,fpdatei ) ;
	if ( df == EOF ) 
	{
		_fmode = _O_TEXT ;
//		sprintf(debugtext,"Fehler beim Datei-Schreiben %s ", filenametmp );
//		WriteLog(iBASDIR, debugtext);
		fclose ( fpdatei ) ;
//		unlink ( Dateiname ) ;
//		Sleep (500 ) ;
		return -1 ;
	}

	// schliesse datei 
	fclose ( fpdatei ) ;
	int ergcode = rename ( TmpDateiname, EchtDateiname ) ; 
	dtvorsatz = 0 ;
	return 0 ;
}

int CdachsascDlg::Dateierstellung (void)
{
	int dretlsk ;
	int dretpspm ;
	int dretnve ;

	dtvorsatz = 0 ;
	lsk.mdn = mdn.mdn ;
	dretlsk = lsk_class.openlsk(tourbeding ) ;	// wegen Transaktion alles 2 mal lesen
												// erzeugt auch gleich noch den counter
	gesamtcount = 0 ;

	if ( !dretlsk )
		dretlsk =lsk_class.leselsk() ;
	while ( !dretlsk )
	{
		trapopspm.mdn = lsk.mdn ;
		trapopspm.ls = lsk.ls ;
		sprintf ( trapopspm.blg_typ ,"L" );
		dretpspm = trapopspm_class.openpspm();	// order by lfd
		dretpspm = trapopspm_class.lesepspm() ;
		if ( ! dretpspm )	// Infos zu diesem Beleg vorhanden ......
		{
			gesamtcount ++ ;
		}
		dretlsk =lsk_class.leselsk() ;
	
	}

	if ( gesamtcount > 0 )
	{
		schreibevorsatz () ; 
		dretlsk = lsk_class.openlsk(tourbeding ) ;	// wegen Transaktion alles 2 mal lesen
	}

	if ( !dretlsk )
		dretlsk =lsk_class.leselsk() ;
	while ( !dretlsk )
	{
		poskpap = 1 ;
		posinzei = 1 ;
		trapopspm.mdn = lsk.mdn ;
		trapopspm.ls = lsk.ls ;
		sprintf ( trapopspm.blg_typ ,"L" );

		dretpspm = trapopspm_class.openpspm();	// order by lfd
		dretpspm = trapopspm_class.lesepspm() ;
		lfdoffset = trapopspm.lfd - 1 ;	// 100211 : nicht unbedingt bei 0 beginnend, aber zumindest konsistent
		trapopspm.lfd = trapopspm.lfd  - lfdoffset ;	//100211
		if ( dretpspm )	// keine Infos zu diesem Beleg vorhanden ......
		{
			dretlsk =lsk_class.leselsk() ;
			continue ;
		}

// KSTA_ == Kopfsatz
		if ( schreibekopfsatz() )	// Datei nicht m�glich oder Stammdaten schrott ?!
			break ;
		while ( ! dretpspm )
		{
// KZEI_ == Packst�cke + verladen auf

			poskpap = poskpap + schreibepspm () ;	// 290911 : poskpap hochzaehlen ( oder nicht )

			dretpspm = trapopspm_class.lesepspm() ;
			trapopspm.lfd = trapopspm.lfd  - lfdoffset ;	//100211

		}
		traponve.mdn = mdn.mdn ;
		sprintf ( traponve.blg_typ ,"L" ) ;
		traponve.ls = lsk.ls ;


		poskppa = 1 ;
		dretnve = traponve_class.opennve();	// order by lfd
		dretnve = traponve_class.lesenve() ;
		traponve.lfd = traponve.lfd  - lfdoffset ;	//100211
		
		while ( ! dretnve )
		{
// KPPA_ == NVE-Satz
			schreibenve () ;	// hier sp�ter  noch Fehlerhandling dazu ?
			dretnve = traponve_class.lesenve() ;
			traponve.lfd = traponve.lfd  - lfdoffset ;	//100211

		}
// KPAP_ == Packmittel-Saetze

		trapopspm.mdn = lsk.mdn ;
		trapopspm.ls = lsk.ls ;
		sprintf ( trapopspm.blg_typ ,"L" );

		dretpspm = trapopspm_class.openpspm();	// order by lfd : alles nochmal lesen, aber anders auswerten
		dretpspm = trapopspm_class.lesepspm() ;
		trapopspm.lfd = trapopspm.lfd  - lfdoffset ;	//100211

		poskpap = 1 ;
		while ( ! dretpspm )
		{
			schreibekpap() ; // hier sp�ter  noch Fehlerhandling dazu ?
			dretpspm = trapopspm_class.lesepspm() ;
			trapopspm.lfd = trapopspm.lfd  - lfdoffset ;	//100211

		}
		dretlsk = lsk_class.leselsk() ;
	}
	if ( ! schreibeschlusssatz())
	{
		// Message : Datei xxx erstellt 
	}
	return 0 ;
}
