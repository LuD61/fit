#include "stdafx.h"
#include "DbClass.h"
#include "leitwegtab.h"

extern DB_CLASS dbClass;

struct LEITWEG leitweg,  leitweg_null;

struct B_LEIT b_leit ;

static int anzzfelder ;


LEITWEG_CLASS leitweg_class ;


int LEITWEG_CLASS::deleteleitweg (void)
{
	if ( del_cursor < 0 ) prepare ();	
	int di = dbClass.sqlexecute (del_cursor);
	return di;
}


int LEITWEG_CLASS::insertleitweg (void)
{
	if ( ins_cursor < 0 ) prepare ();	
	int di = dbClass.sqlexecute (ins_cursor);
	return di;
}

int LEITWEG_CLASS::leseallleitweg (void)
{
      int di = dbClass.sqlfetch (readcursor);
	  return di;
}


int LEITWEG_CLASS::openallleitweg (void)
{
		if ( readcursor < 0 ) prepare ();	
         return dbClass.sqlopen (readcursor);
}

void LEITWEG_CLASS::schliessen (void)
{
	if ( readcursor > -1 )
	{	// geht ca. 200 mal gut, dann knallt es wohl 
		dbClass.sqlclose(readcursor ) ;
		readcursor = -1 ;
	}


}



void LEITWEG_CLASS::prepare (void)
{
/* ------>
// lese einen Leitwegsatz .....( test_upd_cursor )-> praktisch ALLE Felder relevant -> ganz anders

dbClass.sqlin (( long *) &leitweg.leitw_typ, SQLLONG, 0 ) ;
dbClass.sqlin (( long *) &leitweg.leitweg,   SQLLONG, 0 ) ;
dbClass.sqlin (( long *) &leitweg.plz_v,     SQLLONG, 0 ) ;
dbClass.sqlin (( long *) &leitweg.plz_b,     SQLLONG, 0 ) ;
	
	dbClass.sqlout (( char *) leitweg.plz_von, SQLCHAR, 9 ) ;
	dbClass.sqlout (( char *) leitweg.plz_bis, SQLCHAR, 9 ) ;

	test_upd_cursor = (short) dbClass.sqlcursor ("select "
	" plz_von , plz_bis "
	
	" from leitweg where leitw_typ = ? and leitweg = ? and plz_v = ? and plz_b = ?  " ) ;
< ------ */


// delcursor .....

	if ( del_cursor < 0 )
	{

		dbClass.sqlin (( long *)&leitweg.leitw_typ, SQLLONG, 0 ) ;

		del_cursor = (short) dbClass.sqlcursor ("delete from leitweg  "
		" where leitw_typ = ?  " );
	}

// Lese alle Saetze eines Leitweg-Typs

	if ( readcursor < 0 )
	{

		dbClass.sqlin (( long *) &leitweg.leitw_typ, SQLLONG, 0 ) ;


		dbClass.sqlout (( long *) &leitweg.leitweg, SQLLONG, 0 ) ;
		dbClass.sqlout (( long *) &leitweg.plz_v, SQLLONG, 0 ) ;
		dbClass.sqlout (( long *) &leitweg.plz_b, SQLLONG, 0 ) ;
		dbClass.sqlout (( char *)  leitweg.plz_von,  SQLCHAR, 9 ) ;
		dbClass.sqlout (( char *)  leitweg.plz_bis,  SQLCHAR, 9 ) ;

		if ( basissort == FALSE )
		{
			readcursor = (short) dbClass.sqlcursor ("select "
			" leitweg, plz_v, plz_b , plz_von , plz_bis "
	
			" from leitweg where leitw_typ = ?  " 
			" order by leitweg,plz_v, plz_b desc " ) ;
		}
		else
		{
			readcursor = (short) dbClass.sqlcursor ("select "
			" leitweg, plz_v, plz_b , plz_von , plz_bis "
	
			" from leitweg where leitw_typ = ?  " 
			" order by plz_v, plz_b desc " ) ;
		}
	}

// insert ....

	if ( ins_cursor < 0 )
	{
		dbClass.sqlin (( long *) &leitweg.leitw_typ, SQLLONG, 0 ) ;
		dbClass.sqlin (( long *) &leitweg.leitweg, SQLLONG, 0 ) ;
		dbClass.sqlin (( long *) &leitweg.plz_v, SQLLONG, 0 ) ;

		dbClass.sqlin (( long *) &leitweg.plz_b, SQLLONG, 0 ) ;
		dbClass.sqlin (( char *)  leitweg.plz_von, SQLCHAR, 9 ) ;
		dbClass.sqlin (( char *)  leitweg.plz_bis, SQLCHAR, 9 ) ;

		ins_cursor = (short) dbClass.sqlcursor ("insert into leitweg  "
		" ( leitw_typ, leitweg, plz_v, plz_b, plz_von, plz_bis "
		" ) values ( "
		"    ? ,? ,? , ? ,? ,? ) "
		) ;
	}

/* ----->
// Updaten : eigentlich nur formal vorhanden

	dbClass.sqlin (( long *) &leitweg.leitw_typ, SQLLONG, 0 );
	dbClass.sqlin (( long *) &leitweg.leitweg,   SQLLONG, 0 ) ;
	dbClass.sqlin (( long *) &leitweg.plz_v,     SQLLONG, 0 ) ;
	dbClass.sqlin (( long *) &leitweg.plz_b,     SQLLONG, 0 ) ;

	dbClass.sqlin (( long *) &leitweg.leitw_typ, SQLLONG, 0 );
	dbClass.sqlin (( long *) &leitweg.leitweg,   SQLLONG, 0 ) ;
	dbClass.sqlin (( long *) &leitweg.plz_v,     SQLLONG, 0 ) ;
	dbClass.sqlin (( long *) &leitweg.plz_b,     SQLLONG, 0 ) ;

	upd_cursor = (short) dbClass.sqlcursor ("update leitweg set "
	" leitw_typ = ? ,leitweg = ? ,plz_v = ? ,plz_b = ? "
	" where leitw_typ = ? and leitweg = ? and plz_v = ? and plz_b = ? " 
	);
< ----- */

}

