// leitwegDlg.h : Headerdatei
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "List1Ctrl.h"


// CMyleitwegDlg-Dialogfeld
class CMyleitwegDlg : public CDialog
{
// Konstruktion
public:
	CMyleitwegDlg(CWnd* pParent = NULL);	// Standardkonstruktor

// Dialogfelddaten
	enum { IDD = IDD_MYLEITWEG_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV-Unterst�tzung


// Implementierung
protected:
	HICON m_hIcon;

	// Generierte Funktionen f�r die Meldungstabellen
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CList1Ctrl m_list1;
public:
	afx_msg void OnHdnBegintrackList1(NMHDR *pNMHDR, LRESULT *pResult);
public:
	afx_msg void OnHdnEndtrackList1(NMHDR *pNMHDR, LRESULT *pResult);
public:
	afx_msg void OnBnClickedOk();
public:
	afx_msg void OnBnClickedCancel();
public:
 	afx_msg BOOL PreTranslateMessage(LPMSG) ;

	int branrekursion ;
	char leitw_typ[5] ;
//	int IMeEinhCursor ;

//	void FillMeEinhCombo() ;
//	void enableleitw_typ() ;
//	void disableleitw_typ() ;
	void setzeleitw_typ3() ;

//	bool ReadMdn() ;
	virtual BOOL Read(void);
	virtual BOOL Write(void);
	virtual BOOL ReadList(void) ;

	virtual BOOL OnReturn (void);
	virtual BOOL OnKeyup (void);
	virtual void DestroyRows(CVector &Rows);
	virtual BOOL InList (LEITWEG_CLASS &leitweg_class) ;	// Input nur dummy ?!

	CVector DbRows;
	CVector ListRows;

	CFillList FillList ;

public:
	afx_msg void OnCbnKillfocusComboleitwtyp();
public:
	afx_msg void OnCbnSelchangeComboleitwtyp();
public:
	CComboBox m_comboleitwtyp;
public:
	CString v_comboleitwtyp;
public:
	afx_msg void OnCbnSetfocusComboleitwtyp();
public:
	CButton m_buttkey6;
	CButton m_buttkey7;
	CButton m_buttkey8;
	CButton m_buttkey9;
	CButton m_buttkey10;
	CButton m_buttkey11;
	void keyset(BOOL) ;
public:
	afx_msg void OnBnClickedBdruck();
	afx_msg void OnBnClickedButtkey7();
	afx_msg void OnBnClickedButtkey6();
	CButton m_check1;
	BOOL v_check1;
	afx_msg void OnBnClickedCheck1();
};
