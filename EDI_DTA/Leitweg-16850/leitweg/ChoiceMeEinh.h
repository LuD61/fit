#ifndef _CHOICEMEEINH_DEF
#define _CHOICEMEEINH_DEF

#include "ChoiceX.h"
#include "MeEinhList.h"
#include "Vector.h"
#include <vector>

class CChoiceMeEinh : public CChoiceX
{
    private :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
        static int Sort4;
      
    public :
		long m_mdn;
		TCHAR ptwert[5] ;
		TCHAR ptbezk[36] ;
		CVector *Rows;
	    std::vector<CMeEinhList *> MeEinhList;
      	CChoiceMeEinh(CWnd* pParent = NULL);   // Standardkonstruktor
      	~CChoiceMeEinh(); 
        virtual void BezLabel (CListCtrl *);
        virtual void NumLabel (CListCtrl *);
        virtual void FillList (void);
        void SearchNum (CListCtrl *,  LPTSTR);
        void SearchMeEinh (CListCtrl *,  LPTSTR);
        void SearchZusBz (CListCtrl *, LPTSTR);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
		CMeEinhList *GetSelectedText ();
        int GetPtBezk (LPTSTR, LPTSTR);
		void DestroyList ();
};
#endif
