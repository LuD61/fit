#pragma once
#include <vector>
#include "editlistctrl.h"
#include "dbClass.h"
#include "leitwegtab.h"
// #include "ChoiceMeEinh.h"

#define MAXLISTROWS 30

class CList1Ctrl :
	public CEditListCtrl
{
protected:
	DECLARE_MESSAGE_MAP()
public:

	enum AUFSCHLAG
	{
		NO	     = 0,
		BUTTON   = 1,
		LIST     = 2,
		ALL      = 3,
	};

	enum LISTPOS
	{
		POSLEITWEG	= 1,
		POSPLZ_V	= 2,
		POSPLZ_B	= 3,
	};

	int PosLeitWeg;
    int PosPlz_v;
	int PosPlz_b;

	int *Position[5];



// void SetWerte ( long , double, long ) ;

// void Zuschlagsetzen ( void ) ;

//	int Aufschlag;
	int Mode;
	int MaxComboEntries;
	BOOL ActiveListRow;
	short m_mdn;
	short mdn;

	std::vector<BOOL> vSelect;
//X	CVector MeEinhCombo ;

//X	CChoiceMeEinh *ChoiceMeEinh;
//X	BOOL ModalChoiceMeEinh;
//X	BOOL MeEinhChoiceStat;

	CVector ListRows;

	CList1Ctrl(void);
	~CList1Ctrl(void);
	virtual void FirstEnter ();
	virtual void StartEnter (int, int);
	virtual void StopEnter ();
	virtual void OnReturn ();
	virtual void NextRow ();
	virtual void PriorRow ();
	virtual void NextCol ();
	virtual void PriorCol ();
	virtual void FormatText (CString&);
	virtual void SetSel (CString&);
	virtual BOOL InsertRow ();
	virtual BOOL DeleteRow ();
	virtual BOOL AppendEmpty ();
//	virtual void RunItemClicked (int);
	virtual void RunCtrlItemClicked (int);
	virtual void RunShiftItemClicked (int);
	virtual void HiLightItem (int);
//X	void FillMeEinhCombo( CVector&);
	void OnChoice ();

	//	void OnKey9 ();
    BOOL ReadArtikel ();

	void GetColValue (int row, int col, CString& Text);
	void ScrollPositions (int pos);
	BOOL LastCol ();
	long lapptest ( CString LW , CString PLZV , CString PLZB ) ;
};
