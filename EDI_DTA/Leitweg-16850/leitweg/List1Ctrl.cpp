#include "StdAfx.h"
#include "List1Ctrl.h"
#include "StrFuncs.h"
#include "resource.h"
#include "leitwegDlg.h"
#include "leitwegtab.h"



CList1Ctrl::CList1Ctrl(void)
{
//	m_mdn = 1;
	PosLeitWeg		= POSLEITWEG;
	PosPlz_v		= POSPLZ_V;
	PosPlz_b		= POSPLZ_B;
	Position[0] = &PosLeitWeg;
	Position[1] = &PosPlz_v;
	Position[2] = &PosPlz_b;
	Position[3] = NULL;

	MaxComboEntries = 50;

	ListRows.Init ();
}

CList1Ctrl::~CList1Ctrl(void)
{
/* ----> keine Combobox in der Liste vorhanden
	CString *c;
    MeEinhCombo.FirstPosition ();
	while ((c = (CString *) MeEinhCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	MeEinhCombo.Init ();
< ----- */
}

BEGIN_MESSAGE_MAP(CList1Ctrl, CEditListCtrl)
	ON_BN_CLICKED(SEARCHBUTTON ,  OnChoice)
END_MESSAGE_MAP()


/* ---->
void CList1Ctrl::SetWerte ( long anzahl, double kaltgew, long schlklknr )
{
	if (( lianzahl != anzahl ) || ( dikaltgew != kaltgew )|| ( lischlklknr != schlklknr ))
	{
		CString wERTKG ;
		CString wERT;
		CString zUBASIS ;
		CString zUSCHLAG ;

		double dizuschlag ;
		int dibasis ;
		double diwert ;
		double diwertkg ;

		dizuabkg = 0.0 ; 

		int i = GetItemCount () ;
		if ( i > 0 )
		{
			lischlklknr = schlklknr ;
		};
		lianzahl = anzahl ;	
		dikaltgew = kaltgew ;
		
		while  ( i > 0 )
		{
			i -- ;

 			zUSCHLAG =  FillList.GetItemText (i, PosZuschlag);
			dizuschlag = CStrFuncs::StrToDouble (zUSCHLAG);
			CString zUBASIS ;
			zUBASIS =  FillList.GetItemText (i, PosZubasis);
			dibasis = CStrFuncs::StrToDouble (zUBASIS);	// Nimmt den Wert bis zum blank ?!
	 		switch ( dibasis )
			{
//			case 0 :	// je kg
			case 1 :	// je Stck.
				diwert = dizuschlag * lianzahl ;
				if ( dikaltgew != 0.0 )
				{
					diwertkg = (dizuschlag *	lianzahl ) / dikaltgew ;
				}
				else
				{
					diwertkg = 0.0 ;
				}
			break ;
			case 2 :	// pauschal
				diwert = dizuschlag ;
				if ( dikaltgew != 0.0 )
				{
					diwertkg = dizuschlag / dikaltgew ;
				}
				else
				{	
					diwertkg = 0.0 ;
				}
			break ;

			default :	// auffang-Linie : je kg
				diwert = dizuschlag * dikaltgew ;
				diwertkg = dizuschlag ;
			break ;
			}

			DoubleToString (diwert, wERT, 2);
			FillList.SetItemText (wERT.GetBuffer (), i, PosWert);

			DoubleToString (diwertkg, wERTKG, 4);
			FillList.SetItemText (wERTKG.GetBuffer (), i, PosWertkg);
			dizuabkg += diwertkg ;

		}
		Zuschlagsetzen  ( ) ;
	}
	else
	{
		lianzahl = anzahl ;
		dikaltgew = kaltgew ;
		lischlklknr = schlklknr ;
	}
}
< ---- */
/* ----->
void CKstArtListCtrl::Zuschlagsetzen (void) 
{
 CSchlakaDlg * CElFenst ;
CElFenst = ( CSchlakaDlg *) GetParent () ;
CElFenst->SetzeEkzerleg (dizuabkg ) ;
}
< ---- */

void CList1Ctrl::FirstEnter ()
{
	if (GetItemCount () > 0)
	{
		StartEnter (PosPlz_v, 0);
	}
	else
	{
		if (!AppendEmpty ()) return; 
		StartEnter (PosLeitWeg, 0);
	}
}

void CList1Ctrl::StartEnter (int col, int row)
{
	if (col == 0) col = 1;
//	if (col == PosArtBez) return;
/* ---> es gibt keine Kriterien mehr ..........
	if ( col == PosArtNr )
	{
		CString cArtNr = GetItemText (row, PosArtNr);
		if (StrToDouble (cArtNr) > 0  )	// Artikel gefunden/vorhanden 
		{
			return ;
		}
		CEditListCtrl::StartEnter (col, row);

	}


/* --->
	if (col == PosAktiv)
	{
		CEditListCtrl::StartEnterCheckBox (col, row);
		return ;
	}
< ---- */

	CEditListCtrl::StartEnter (col, row);
/* ------>
	if (col == PosKun_me_einh)
	{
		if (IsWindow (ListComboBox.m_hWnd))
		{
			StopEnter ();
		}


//		CString zUBASIS ;
//		zUBASIS =  FillList.GetItemText (row, PosKun_me_einh);	// == col //
		
//		int nCurSel = ListComboBox.FindString( 0,zUBASIS.GetBuffer(0));

		CEditListCtrl::StartEnterCombo (col, row, &MeEinhCombo);

//		if (nCurSel > 0)
//			ListComboBox.SetCurSel( nCurSel ) ;
//		else
//			ListComboBox.SetCurSel( 0 ) ;
//		oldsel = ListComboBox.GetCurSel ();
		return ;
	}
< ---- */

/* ---->
	if (col == PosZuschlag || col == PosKost_bez)
	{
		CEditListCtrl::StartEnter (col, row);
	}
< ----- */

}

void CList1Ctrl::StopEnter ()
{

	CEditListCtrl::StopEnter ();

	if (IsWindow (ListEdit.m_hWnd))
	{
		CString Text;
		ListEdit.GetWindowText (Text);
		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		ListEdit.DestroyWindow ();
	}
	else if (IsWindow (ListComboBox.m_hWnd))
	{
		CString Text;
		int idx = ListComboBox.GetCurSel ();
		if (idx < 0) 
		{
			Text = _T("");
		}
		if (ListComboBox.GetCount () > 0)
		{
			ListComboBox.GetLBText (idx, Text);
			FormatText (Text);
			FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		}
		ListComboBox.DestroyWindow ();
	}
/* --->
	else if (IsWindow (ListCheckBox.m_hWnd))
	{
		int check = ListCheckBox.GetCheck ();
        if (check == BST_CHECKED)
		{
			FillList.SetItemText (_T("X"), EditRow, EditCol);
		}
		else
		{
			FillList.SetItemText (_T(" "), EditRow, EditCol);
		}
		ListCheckBox.DestroyWindow ();
	}
< ---- */

	/* ---->
	else if (IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		int count = GetItemCount ();
		CString Text;
		SearchListCtrl.Edit.GetWindowText (Text);
		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		SearchListCtrl.DestroyWindow ();
	}
	< ----- */
}

void CList1Ctrl::SetSel (CString& Text)
{

   if (EditCol == PosLeitWeg )   // EditCol == PosArtNr
   {
		ListEdit.SetSel (0, -1);
   }
/* ----->
   else
   {
		Text.TrimRight ();
		int cpos = Text.GetLength ();
//		ListEdit.SetSel (cpos, cpos);
   }
< ------ */
   else
   {	// alles numerische Felder
		ListEdit.SetSel (0, -1);
   }
}

void CList1Ctrl::FormatText (CString& Text)
{

 if (EditCol == PosLeitWeg)
	{
		DoubleToString (StrToDouble (Text), Text, 0);
	}
 
  if (EditCol == PosPlz_v)
	{
		DoubleToString (StrToDouble (Text), Text, 0);
	}
  if (EditCol == PosPlz_b)
	{
		DoubleToString (StrToDouble (Text), Text, 0);
	}

/* ---->
  if (EditCol == PosKun_inh)
	{
		DoubleToString (StrToDouble (Text), Text, 3);
	}

< ----- */

/* --->
   if (EditCol == PosZuschlag || EditCol == PosZubasis )
   {
	   double dizuschlag ;
	   short dibasis ;
	   if ( EditCol == PosZuschlag )
	   {
			dizuschlag = StrToDouble (Text) ;
			CString zUBASIS ;
			zUBASIS =  FillList.GetItemText (EditRow, PosZubasis);
			dibasis = CStrFuncs::StrToDouble (zUBASIS);	// Nimmt den Wert bis zum blank ?!
	   }
	   else
	   {

 			CString zUSCHLAG =  FillList.GetItemText (EditRow, PosZuschlag);
			dizuschlag = CStrFuncs::StrToDouble (zUSCHLAG);
			CString zUBASIS ;
			zUBASIS =  FillList.GetItemText (EditRow, PosZubasis);
			dibasis = CStrFuncs::StrToDouble (zUBASIS);	// Nimmt den Wert bis zum blank ?!
	   }
		double diposwert, diposwertkg ;
		switch ( dibasis )
		{
//		case 0 :	// je kg
		case 1 :	// je Stck.
			diposwert = dizuschlag * lianzahl ;
			if ( dikaltgew != 0.0 )
			{
			    diposwertkg = (dizuschlag *	lianzahl ) / dikaltgew ;
			}
			else
			{
				diposwertkg = 0.0 ;
			}
			break ;
		case 2 :	// pauschal
			diposwert = dizuschlag ;
			if ( dikaltgew != 0.0 )
			{
			    diposwertkg = dizuschlag / dikaltgew ;
			}
			else
			{
				diposwertkg = 0.0 ;
			}
			break ;

		default :	// auffang-Linie : je kg
		    diposwert = dizuschlag * dikaltgew ;
			diposwertkg = dizuschlag ;
			break ;
		}

		CString wERT;
		DoubleToString (diposwert, wERT, 2);
//		Schlaklkp.schlaklkp.wert = CStrFuncs::StrToDouble (wERT);
// 		FillList.SetItemText (wERT.GetBuffer (), i, m_list1.PosWert);
		FillList.SetItemText (wERT.GetBuffer (), EditRow, PosWert);

		CString wERTKG;
		DoubleToString (diposwertkg, wERTKG, 4);
//		Schlaklkp.schlaklkp.wertkg = CStrFuncs::StrToDouble (wERTKG);
//		FillList.SetItemText (wERTKG.GetBuffer (), i, m_list1.PosWertkg);
		FillList.SetItemText (wERTKG.GetBuffer (), EditRow, PosWertkg);
	}

< ---- */

	/* --->
   CString weRTKG ;
   dizuabkg = 0.0 ; 
 	for (int i = GetItemCount()  ; i > 0 ; )
	{
		i -- ;
		weRTKG =  FillList.GetItemText (i, PosWertkg);
		dizuabkg += CStrFuncs::StrToDouble (weRTKG);
	}

	Zuschlagsetzen  ( ) ;

< ----- */

/*
    else if (EditCol == 4)
	{
		DatFormat (Text, "dd.mm.yyyy");
	}
*/

}

void CList1Ctrl::NextRow ()
{

	int count = GetItemCount ();

	BOOL ob = TRUE ;

	BOOL buggy = FALSE ;

/* -----> keine Kriterien 
	if (EditCol == PosArtNr)
	{
		ob = ReadArtikel ();
	}
< ----- */
/* ->
	else if (EditCol == PosKunPr)
	{
		ReadKunPr ();
	}
	else if (EditCol == PosPrGrStuf)
	{
		ReadPrGrStuf ();
	}
< --- */


    CString Text;
	ListEdit.GetWindowText (Text);

	if ( EditCol == PosLeitWeg )
	{
		if ( atol(Text) < 1 )
		{
			ob = FALSE ;
		}
		else
		{
			CString PLZV = GetItemText (EditRow, PosPlz_v);
			CString PLZB = GetItemText (EditRow, PosPlz_b);
			if (atol (Text.GetBuffer()) < 1  ||
				     StrToDouble ( PLZV ) < 1 || 
					 StrToDouble ( PLZB ) < 1 )
			{
				if ( EditRow >= ( count - 1 ) && StrToDouble( PLZB ) < 1 && StrToDouble ( PLZV ) < 1 )
				{
					// dann ist es erlaubt aber unm�gliche Situation
				}
				else
					buggy = TRUE ;
			}
			else  
			{
				if ( StrToDouble( PLZB ) <  StrToDouble ( PLZV ) && StrToDouble ( PLZV ) > 0  )
				{
					MessageBox (_T("PLZ bis ist kleiner als PLZ von"), NULL, MB_OK | MB_ICONERROR); 
					buggy = TRUE ;
				}
				else
				{
					long lappweg = lapptest ( Text , PLZV , PLZB ) ;
					if ( lappweg )
					{
						char text[95] ;
						sprintf ( text , "Es gibt eine �berlappung mit Leitweg %d ", lappweg ) ;
						MessageBox (_T(text), NULL, MB_OK | MB_ICONERROR); 
						buggy = TRUE ;
					}
				}
			}
		}
	}
	if ( EditCol == PosPlz_v )
	{

		if ( atol(Text) < 1  )
		{
			ob = FALSE ;
		}
		else
		{
			CString PLZB = GetItemText (EditRow, PosPlz_b);
			CString LW = GetItemText (EditRow, PosLeitWeg);
			if ( StrToDouble( PLZB ) < 1 || StrToDouble ( Text ) < 1  || StrToDouble ( LW ) < 1   )
			{
				if ( EditRow >= ( count - 1 ) && StrToDouble( PLZB ) < 1 && StrToDouble ( Text ) < 1 )
				{
					// dann ist es erlaubt
				}
				else
					buggy = TRUE ;
			}
			else if ( StrToDouble( PLZB ) < StrToDouble ( Text ) && StrToDouble ( Text ) > 0  )
			{
				MessageBox (_T("PLZ bis ist kleiner als PLZ von"), NULL, MB_OK | MB_ICONERROR); 
				buggy = TRUE ;
			}
			else 
			{
				long lappweg = lapptest ( LW , Text , PLZB ) ;
				if ( lappweg )
				{
					char text[95] ;
					sprintf ( text , "Es gibt eine �berlappung mit Leitweg %d ", lappweg ) ;
					MessageBox (_T(text), NULL, MB_OK | MB_ICONERROR); 
					buggy = TRUE ;
				}
			}	
		}
	}
	if ( EditCol == PosPlz_b )
	{
		if (  atol(Text) < 1 )
		{
			ob = FALSE ;
		}
		else
		{
			CString PLZV = GetItemText (EditRow, PosPlz_v);
			CString LW = GetItemText (EditRow, PosLeitWeg);
			if ( StrToDouble( Text ) < 1 || StrToDouble ( PLZV ) < 1  || StrToDouble ( LW ) < 1  )
			{
				if ( EditRow >= ( count - 1 ) && StrToDouble( Text ) < 1 && StrToDouble ( PLZV ) < 1 )
				{
					// dann ist es erlaubt
				}
				else
					buggy = TRUE ;
			}
			else if ( StrToDouble( Text ) < StrToDouble ( PLZV ) && StrToDouble ( PLZV ) > 0  )
			{
				MessageBox (_T("PLZ bis ist kleiner als PLZ von"), NULL, MB_OK | MB_ICONERROR); 
				buggy = TRUE ;
			}
			else
			{
				long lappweg = lapptest ( LW , PLZV , Text ) ;
				if ( lappweg )
				{
					char text[95] ;
					sprintf ( text , "Es gibt eine �berlappung mit Leitweg %d ", lappweg ) ;
					MessageBox (_T(text), NULL, MB_OK | MB_ICONERROR); 
					buggy = TRUE ;
				}
			}
		}
	}

	SetEditText ();

	count = GetItemCount ();


	if ( ob == TRUE && buggy == FALSE )
	{
		if (EditRow >= count - 1)
		{
			if (AppendEmpty () == FALSE)
			{
				return;
			}
			StopEnter ();
			EditRow ++;
			EditCol = 0;

		}
		else
		{
			StopEnter ();
			EditRow ++;
		}
	}
	else
	{	// Auf Schluessel stehen bleiben ?!
		EditCol = PosLeitWeg ;
	}
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);

	if (GetKeyState (VK_SHIFT) < 0)
	{
		BOOL &b = vSelect[EditRow - 1];
		b = TRUE;
		FillList.SetItemImage (EditRow - 1, 1);
	}
	else if (GetKeyState (VK_CONTROL) == 0)
	{
		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			BOOL& b = vSelect[i];
			b = FALSE;
			FillList.SetItemImage (i,0);
		}
	}
}

void CList1Ctrl::PriorRow ()
{
	if (EditRow <= 0)
	{
			return;
	}
	int count = GetItemCount ();

	BOOL ob = TRUE ;

	BOOL buggy = FALSE ;

	if (IsWindow (ListEdit.m_hWnd))
	{
		if (EditCol == PosLeitWeg)
		{
			ob = FALSE ;
			CString Text;
			ListEdit.GetWindowText (Text);
			CString PLZV = GetItemText (EditRow, PosPlz_v);
			CString PLZB = GetItemText (EditRow, PosPlz_b);
			if (atol (Text.GetBuffer()) < 1  ||
				     StrToDouble ( PLZV ) < 1 || 
					 StrToDouble ( PLZB ) < 1 )
			{
//				DeleteRow() ;
				if ( EditRow >= ( count - 1 ) && StrToDouble( PLZB ) < 1 && StrToDouble ( PLZV ) < 1 )
				{
					// dann ist es erlaubt
				}
				else
					buggy = TRUE ;
			}
			else  
			{
				if ( StrToDouble( PLZB ) <  StrToDouble ( PLZV ) && StrToDouble ( PLZV ) > 0  )
				{
					MessageBox (_T("PLZ bis ist kleiner als PLZ von"), NULL, MB_OK | MB_ICONERROR); 
					buggy = TRUE ;
				}
				else
				{
					long lappweg = lapptest ( Text , PLZV , PLZB ) ;
					if ( lappweg )
					{
						char text[95] ;
						sprintf ( text , "Es gibt eine �berlappung mit Leitweg %d ", lappweg ) ;
						MessageBox (_T(text), NULL, MB_OK | MB_ICONERROR); 
						buggy = TRUE ;
					}
				}
			}
		}

		if (EditCol == PosPlz_v)
		{
			CString PLZV ;
			ListEdit.GetWindowText (PLZV);
			CString PLZB = GetItemText (EditRow, PosPlz_b);
			CString LW = GetItemText (EditRow, PosLeitWeg);

			if ( StrToDouble( PLZB ) < 1 || StrToDouble ( PLZV ) < 1  || StrToDouble ( LW ) < 1   )
			{
				if ( EditRow >= ( count - 1 ) && StrToDouble( PLZB ) < 1 && StrToDouble ( PLZV ) < 1 )
				{
					// dann ist es erlaubt
				}
				else
					buggy = TRUE ;
			}
			else if ( StrToDouble( PLZB ) < StrToDouble ( PLZV ) && StrToDouble ( PLZV ) > 0  )
			{
				MessageBox (_T("PLZ bis ist kleiner als PLZ von"), NULL, MB_OK | MB_ICONERROR); 
				buggy = TRUE ;
			}
			else 
			{
				long lappweg = lapptest ( LW , PLZV , PLZB ) ;
				if ( lappweg )
				{
					char text[95] ;
					sprintf ( text , "Es gibt eine �berlappung mit Leitweg %d ", lappweg ) ;
					MessageBox (_T(text), NULL, MB_OK | MB_ICONERROR); 
					buggy = TRUE ;
				}
			}
		}

		if (EditCol == PosPlz_b)
		{
			CString PLZB ;
			ListEdit.GetWindowText (PLZB);
			CString PLZV = GetItemText (EditRow, PosPlz_v);
			CString LW = GetItemText (EditRow, PosLeitWeg);
			if ( StrToDouble( PLZB ) < 1 || StrToDouble ( PLZV ) < 1  || StrToDouble ( LW ) < 1  )
			{
				if ( EditRow >= ( count - 1 ) && StrToDouble( PLZB ) < 1 && StrToDouble ( PLZV ) < 1 )
				{
					// dann ist es erlaubt
				}
				else
					buggy = TRUE ;
			}
			else if ( StrToDouble( PLZB ) < StrToDouble ( PLZV ) && StrToDouble ( PLZV ) > 0  )
			{
				MessageBox (_T("PLZ bis ist kleiner als PLZ von"), NULL, MB_OK | MB_ICONERROR); 
				buggy = TRUE ;
			}
			else
			{
				long lappweg = lapptest ( LW , PLZV , PLZB ) ;
				if ( lappweg )
				{
					char text[95] ;
					sprintf ( text , "Es gibt eine �berlappung mit Leitweg %d ", lappweg ) ;
					MessageBox (_T(text), NULL, MB_OK | MB_ICONERROR); 
					buggy = TRUE ;
				}
			}
		}
	}
	else
		EditCol = PosLeitWeg ;

	if (EditRow == count - 1)
	{
/* --->
		CString Zuschlag = GetItemText (EditRow, PosZuschlag);
		CString Kost_bez = GetItemText (EditRow, PosKost_bez);
		if ((StrToDouble (Zuschlag) == 0.0)
			&& (_tcslen (Kost_bez.Trim()) == 0 ))
		{
	        DeleteItem (EditRow);
		}
< ---- */
	}
	else
	{
		if (EditRow <= 0)
		{
			return;
		}
/* ---->
		if (EditCol == PosKun)
		{
			ReadKunName ();
		}
		else if (EditCol == PosKunPr)
		{
			ReadKunPr ();
		}
		else if (EditCol == PosPrGrStuf)
		{
			ReadPrGrStuf ();
		}
		TestIprIndex ();
< ---- */
	}
	StopEnter ();

	if ( buggy == TRUE )
	{
		EditRow ++ ;
		if ( EditCol == PosPlz_b )
			EditCol -- ;
	}

	EditRow --;
	EnsureVisible (EditRow, FALSE);
	if ( ob == FALSE && buggy == FALSE )
		EditCol = PosLeitWeg ;	// PosA_kun

	StartEnter (EditCol, EditRow);
	for (int i = 0; i < (int) vSelect.size (); i ++)
	{
		BOOL& b = vSelect[i];
		b = FALSE;
		FillList.SetItemImage (i,0);
	}
}


BOOL CList1Ctrl::LastCol ()
{

	if (EditCol < PosPlz_b) return FALSE;
	/* --->
	if (Mode == TERMIN && EditCol >= PosLdPr)
	{
		return TRUE;
	}
	if (Aufschlag == LIST || Aufschlag == ALL)
	{
		if (EditCol < PosEkProz) return FALSE;
	}
< ---- */

	return TRUE;
}

void CList1Ctrl::OnReturn ()
{
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	int rowCount = GetItemCount ();

	if (EditCol >= colCount - 1 &&
		EditRow >= rowCount - 1)
	{	// Letzte Stelle der neusten Zeile

/* ----->
		if ((StrToDouble (LeitWeg) < 0.9 ) 
			&&	(StrToDouble (PLZV) < 0.9 )
			&&	(StrToDouble (PLZB) < 0.9 ))
		{
			EditCol --;
			return;
		}

< ----- */
	}

/* --->
	if (EditCol == PosArtNr)
	{
		ReadArtikel ();
	}
< ---- */

/* ---->
	else if (EditCol == PosKunPr)
	{
		ReadKunPr ();
	}
	else if (EditCol == PosPrGrStuf)
	{
		ReadPrGrStuf ();
	}
< ---- */

	if (LastCol ())
	{
//		TestIprIndex ();
	    rowCount = GetItemCount ();
		if (EditRow >= rowCount - 1)
		{
			if (AppendEmpty () == FALSE)
			{
				return;
			}


		}

		
		// letzte Spalte extra handeln 
		CString LW = GetItemText (EditRow, PosLeitWeg);
		CString PLZV = GetItemText (EditRow, PosPlz_v);
	    CString PLZB ;
		ListEdit.GetWindowText (PLZB);
		BOOL ob = FALSE ;

//		CString PLZB = GetItemText (EditRow, PosPlz_b);

		if ( StrToDouble( PLZB ) < 1  )
		{
			ob = TRUE ;
		}
		else if ( StrToDouble( PLZB ) < StrToDouble ( PLZV )  )
		{

			MessageBox (_T("PLZ bis ist kleiner als PLZ von"), NULL, MB_OK | MB_ICONERROR); 
			ob = TRUE ;
		}
		else
		{
			long lappweg = lapptest ( LW , PLZV , PLZB ) ;
			if ( lappweg )
			{	char text[95] ;
				sprintf ( text , "Es gibt eine �berlappung mit Leitweg %d ", lappweg ) ;
				MessageBox (_T(text), NULL, MB_OK | MB_ICONERROR); 
				ob = TRUE ;
			}
		}
		
		StopEnter ();
		if ( ob == TRUE )
		{
//			EditRow -- ;
		}
		else
		{
			EditRow ++;
			EditCol = 0;
		
			if (EditRow == rowCount)
			{
				EditCol = PosLeitWeg ; // PosArtNr
			}
			else
			{
				EditCol = PosLeitWeg ;	// PosA_kun
			}
		}
	}
	else
	{
	    StopEnter ();

		if ( EditCol == PosLeitWeg )
		{
			CString LeitWeg = GetItemText (EditRow, PosLeitWeg);
			if ( StrToDouble( LeitWeg ) < 1 )
				EditCol -- ;
		}
		else if( EditCol == PosPlz_v )
		{
			CString PLZV = GetItemText (EditRow, PosPlz_v);

			if ( StrToDouble( PLZV ) < 1 )
				EditCol -- ;
		}
		else if ( EditCol == PosPlz_b )
		{
			CString PLZV = GetItemText (EditRow, PosPlz_v);
			CString PLZB = GetItemText (EditRow, PosPlz_b);
			if ( StrToDouble( PLZB ) < 1  )
				EditCol -- ;
			else if ( StrToDouble( PLZB ) < StrToDouble ( PLZV )  )
			{
				EditCol -- ;
				EditCol -- ;
			}
		}

		EditCol ++;
/* ---->
		if (EditCol == PosArtBez)
		{
			EditCol ++;
		}
		if (EditCol == PosKante)
		{
			EditCol ++;
		}
< ----- */
	}
	EnsureColVisible (EditCol);
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
}

void CList1Ctrl::NextCol ()
{
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol >= count - 1)
	{
		return;
	}

/* ---->
	if (EditCol == PosArtNr)
	{
		ReadArtikel ();
	}
< ----- */
/* --- >
	else if (EditCol == PosKunPr)
	{
		ReadKunPr ();
	}
	else if (EditCol == PosPrGrStuf)
	{
		ReadPrGrStuf ();
	}
< ---- */

	StopEnter ();
	EditCol ++;
/* --->
	if (EditCol == PosKunName)
	{
		EditCol ++;
	}
< --- */
	EnsureColVisible (EditCol);
    StartEnter (EditCol, EditRow);
}

void CList1Ctrl::PriorCol ()
{
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol <= 1)
	{
		return;
	}
/* ---->
	if (EditCol == PosKun)
	{
		ReadKunName ();
	}
	else if (EditCol == PosKunPr)
	{
		ReadKunPr ();
	}
	else if (EditCol == PosPrGrStuf)
	{
		ReadPrGrStuf ();
	}
< -- */

	StopEnter ();
	EditCol --;
/* ---->
	if (EditCol == PosKunName)
	{
		EditCol --;
	}
< ---- */
    StartEnter (EditCol, EditRow);
}

BOOL CList1Ctrl::InsertRow ()
{
//	return FALSE ;
	CString cLW = GetItemText (EditRow, PosLeitWeg);
	CString cPV = GetItemText (EditRow, PosPlz_v);
	CString cPB = GetItemText (EditRow, PosPlz_b);
	if (   (StrToDouble (cLW ) > 0 )
		&& (StrToDouble (cPV ) < 1 )
		&& (StrToDouble (cPB ) < 1 ) )
	{
		return FALSE;
	}
	StopEnter ();
	// editrow - 1 = ???
	CString AltLeitWeg = GetItemText (EditRow, PosLeitWeg);

	int colCount = GetHeaderCtrl ()->GetItemCount ();
	FillList.InsertItem (EditRow, -1);

	FillList.SetItemText (AltLeitWeg.GetBuffer(), EditRow, PosLeitWeg);
	FillList.SetItemText (" ", EditRow, PosPlz_v);	// Blank editiert sich besser
	FillList.SetItemText (" ", EditRow, PosPlz_b);	// Balnk editiert sich besser

	/* ----->
		FillList.SetItemText (" ", EditRow, PosArtNr);
		FillList.SetItemText (" ", EditRow, PosArtBez);

		CString pKANTE;
		pKANTE = _T("|") ;
		FillList.SetItemText (pKANTE.GetBuffer (), EditRow, PosKante);

		FillList.SetItemText (" " , EditRow, PosLeitWeg );
		FillList.SetItemText (" " , EditRow, PosA_bz1);
		FillList.SetItemText (" " , EditRow, PosA_bz2);

		CString pTWERT ;
		TCHAR ptbezk [37] ; 
		pTWERT.Format (_T("2"));	// Default : Gewichtsartikel ?! 
		CChoiceMeEinh HoleBezk ;
		HoleBezk.GetPtBezk ( pTWERT.GetBuffer(), ptbezk );
		CString zUBASIS;
// das Problem : Formatierung numerischer Wert mit 2 Folgeblanks 
		zUBASIS.Format (_T("2    %s"), ptbezk);
		FillList.SetItemText (zUBASIS.GetBuffer (), EditRow, PosKun_me_einh);

		FillList.SetItemText (" ",EditRow, PosKun_inh);	// blank  editiert sich besser
		FillList.SetItemText (" " , EditRow, PosGeb_fakt);	// blank editiert sich besser
		FillList.SetItemText (" ", EditRow, PosEan);
		FillList.SetItemText (" ", EditRow, PosEan_VK);
		FillList.SetItemText (" ", EditRow, PosLi_a);
< ---- */

	StartEnter (0, EditRow);
	return TRUE;
}

BOOL CList1Ctrl::DeleteRow ()
{
	if (!IsWindow (m_hWnd)) return FALSE;
//	return FALSE ;
	return CEditListCtrl::DeleteRow ();
}

BOOL CList1Ctrl::AppendEmpty ()
{
	int rowCount = GetItemCount ();

	if (rowCount > 0)
	{
		CString cLeitWeg = GetItemText (EditRow + 1 , PosLeitWeg);
		double hilfe = StrToDouble (cLeitWeg) ;
		if (hilfe > 0 )	// Bereits neue Zeile angelegt
		{
			return FALSE;
		}

		CString cPLZV = GetItemText (EditRow  , PosPlz_v);
		CString cPLZB = GetItemText (EditRow  , PosPlz_b);
		double vhilfe = StrToDouble (cPLZV) ;
		double bhilfe = StrToDouble (cPLZB) ;
		if (vhilfe < 1 && bhilfe < 1 )	// Die Zeile ist leer bis auf den Leitweg
		{
			return FALSE;
		}

	}

	CString AltLeitWeg = GetItemText (EditRow, PosLeitWeg);

	int colCount = GetHeaderCtrl ()->GetItemCount ();
	FillList.InsertItem (rowCount, -1);

	FillList.SetItemText (AltLeitWeg.GetBuffer(), rowCount, PosLeitWeg);
	FillList.SetItemText (" ", rowCount, PosPlz_v);	// Blank editiert sich besser
	FillList.SetItemText (" ", rowCount, PosPlz_b);	// Balnk editiert sich besser

/* --->
	CString pOSI ;
	pOSI.Format (_T("%d"), rowCount + 1 );
	FillList.SetItemText (_T(pOSI.GetBuffer()), rowCount, PosPosi);

	FillList.SetItemText (_T(""), rowCount, PosKost_bez);
	FillList.SetItemText (_T("0,00"), rowCount, PosZuschlag);
	FillList.SetItemText (_T(""), rowCount, PosZubasis);
	FillList.SetItemText (_T("0,00"), rowCount, PosWert);
	FillList.SetItemText (_T("0,00"), rowCount, PosWertkg);
< ---- */

	/* ----->
		FillList.SetItemText (" ", rowCount, PosArtNr);
		FillList.SetItemText (" ", rowCount, PosArtBez);

		CString pKANTE;
		pKANTE = _T("|") ;
		FillList.SetItemText (pKANTE.GetBuffer (), rowCount, PosKante);

		FillList.SetItemText (" " , rowCount, PosA_kun);
		FillList.SetItemText (" " , rowCount, PosA_bz1);
		FillList.SetItemText (" " , rowCount, PosA_bz2);

		CString pTWERT ;
		TCHAR ptbezk [37] ; 
		pTWERT.Format (_T("2"));	// Default : Gewichtsartikel ?! 
		CChoiceMeEinh HoleBezk ;
		HoleBezk.GetPtBezk ( pTWERT.GetBuffer(), ptbezk );
		CString zUBASIS;
// das Problem : Formatierung numerischer Wert mit 2 Folgeblanks 
		zUBASIS.Format (_T("2    %s"), ptbezk);
		FillList.SetItemText (zUBASIS.GetBuffer (), rowCount, PosKun_me_einh);

		FillList.SetItemText (" ",rowCount, PosKun_inh); // blank editiert sdich besser
		FillList.SetItemText (" " , rowCount, PosGeb_fakt);	// blank editiert sich besser 
		FillList.SetItemText (" ", rowCount, PosEan);
		FillList.SetItemText (" ", rowCount, PosEan_VK);
		FillList.SetItemText (" ", rowCount, PosLi_a);
	< ---- */	
		
	rowCount = GetItemCount ();
	return TRUE;
}

void CList1Ctrl::HiLightItem (int Item)
{
/*
	    if (Item < 0) return;
		BOOL& b = vSelect[Item];
		b = TRUE;
		FillList.SetItemImage (Item,1);
*/

}

/* --->
void CKstArtListCtrl::FillPrGrStufCombo (CVector& Values)
{
	CString *c;
    PrGrStufCombo.FirstPosition ();
	while ((c = (CString *) PrGrStufCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	PrGrStufCombo.Init ();
	Values.FirstPosition ();
	while ((c = (CString *) Values.GetNext ()) != NULL)
	{
		PrGrStufCombo.Add (c);
	}
}
< ---- */

/* ---> brauchet mer z.Z. nicht
void CList1Ctrl::FillMeEinhCombo (CVector& Values)
{
	CString *c;
    MeEinhCombo.FirstPosition ();
	while ((c = (CString *) MeEinhCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	MeEinhCombo.Init ();
	Values.FirstPosition ();
	while ((c = (CString *) Values.GetNext ()) != NULL)
	{
		MeEinhCombo.Add (c);
	}
}
< ---- */

/* ----->
void CKstArtListCtrl::RunItemClicked (int Item)
{
**
	    if (Item == -1) return;
		BOOL& b = vSelect[Item];
		if (b)
		{
			b = FALSE;
			FillList.SetItemImage (Item,0);
		}
		else
		{
			b = TRUE;
			FillList.SetItemImage (Item,1);
		}

		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			if (i == Item) continue;
			BOOL& b = vSelect[i];
			b = FALSE;
			FillList.SetItemImage (i,0);
		}
**
}
< ----- */


void CList1Ctrl::RunCtrlItemClicked (int Item)
{
/*
	    if (Item == -1) return;
		BOOL& b = vSelect[Item];
		b = TRUE;
		FillList.SetItemImage (Item,1);
*/
}

void CList1Ctrl::RunShiftItemClicked (int Item)
{
/*
	    int start;
		int end;
        int row = -1;

	    if (Item == -1) return;
		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			BOOL& b = vSelect[i];
			if (b)
			{
				row = i;
				break;
			}
		}

		if (row == -1)
		{
			RunItemClicked (Item);
			return;
		}


		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			BOOL& b = vSelect[i];
			b = FALSE;
			FillList.SetItemImage (i,0);
		}

		if (row < Item)
		{
			start = row;
			end = Item;
		}
		else
		{
			start = Item;
			end   = row;
		}
		for (int i = start; i <= end; i ++)
		{
		    BOOL& b = vSelect[i];
			b = TRUE;
			FillList.SetItemImage (i,1);
		}
*/
}

void CList1Ctrl::OnChoice ()
{
	/* -> 
	if (EditCol == PosKun)
	{
		OnKunChoice (CString (_T("")));
	}
	else if (EditCol == PosKunPr)
	{
		OnIKunPrChoice (CString (_T("")));
	}
	else if (EditCol == PosPrGrStuf)
	{
		OnPrGrStufChoice (CString (_T("")));
	}
	< ----- */
}


/* --->
void CList1Ctrl::OnKey9 ()
{
	if (IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		OnChoice ();
	}
}
< ---- */


// Classe::ReadKunNmae () 
// bool : FALSE : lesen ging schief  

/* brauchet mer z.Z nicht 
BOOL CList1Ctrl::ReadArtikel ()
{

	if (EditCol != PosArtNr) return TRUE ;
    memcpy (&a_bas, &a_bas_null, sizeof (A_BAS));
//	memcpy (&KunAdr.adr, &adr_null, sizeof (ADR));
** ---->
	if (!IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		return;
	}
< ---- **

    CString Text;
	ListEdit.GetWindowText (Text);
** ---->
	if (!CStrFuncs::IsDecimal (Text))
	{
		OnKunChoice (Text);
	    SearchListCtrl.Edit.GetWindowText (Text);
		Text.Format (_T("%ld"), atol (Text.GetBuffer ()));
	    SearchListCtrl.Edit.SetWindowText (Text);
		if (!KunChoiceStat)
		{
			EditCol --;
			return;
		}
	}
< ---- **
	if (atol (Text) == 0l)
	{
		MessageBox (_T("eine Eingabe ist erforderlich"), NULL, MB_OK | MB_ICONERROR); 
		EditCol --;
		return FALSE ;
	}
    a_bas.a = atol (Text ) ;
	int i = a_bas_class.opena_bas () ;
	i = a_bas_class.lesea_bas () ;
	a_kun.a = atol (Text);
	if (!i)
    {
//		  KunAdr.adr.adr = Kun.kun.adr1;
//		  KunAdr.dbreadfirst ();
		CString hilfart ;
		double dart ;
		for (int rowco = GetItemCount()  ; rowco > 0 ; )
		{
			rowco -- ;
			hilfart =  FillList.GetItemText (rowco, PosArtNr);
			dart = CStrFuncs::StrToDouble (hilfart) ;  
			if ( ( dart == a_kun.a ) && ( rowco != EditRow ) && dart > 0 )
			{
				MessageBox (_T("Artikel bereits in der Liste"), NULL, MB_OK | MB_ICONERROR); 
				EditCol --;
				return FALSE ;
			}
		}

	}
	else // if (a_bas.a != 0l)
	{
		MessageBox (_T("Artikel nicht gefunden"), NULL, MB_OK | MB_ICONERROR); 
		EditCol --;
		return FALSE ;
	}
    CString cBez;
	cBez.Format (_T("%s %s"), a_bas.a_bz1, a_bas.a_bz2 );
	FillList.SetItemText (cBez.GetBuffer (), EditRow, PosArtBez);
// 020211 : bei Neuanlage zun�chst default vorschlagen 
	cBez.Format (_T("%s"), a_bas.a_bz1 );
	FillList.SetItemText (cBez.GetBuffer (), EditRow, PosA_bz1);
	return TRUE ;

}
< ----- */
void CList1Ctrl::GetColValue (int row, int col, CString& Text)
{
	/* ---->
    CString cText = GetItemText (row, col);
	int pos = 0;
	if (col == PosPrGrStuf)
	{
		Text = cText.Tokenize (_T(" "), pos);
	}
	else if (col == PosKunPr)
	{
		Text = cText.Tokenize (_T(" "), pos);
	}
	else
	{
		Text = cText.Trim ();
	}
	< ---- */
}

/* ---->
void CList1Ctrl::TestIprIndex ()
{
** ---->
	int Items = GetItemCount ();
	if (Items <= 1) return;
	CString Value;
	GetColValue (EditRow, PosPrGrStuf, Value);
	long rPrGrStuf = atol (_T(Value));
	GetColValue (EditRow, PosKunPr, _T(Value));
	long rKunPr = atol (_T(Value));
	GetColValue (EditRow, PosKun, _T(Value));
	long rKun = atol (_T(Value));
	for (int i = 0; i < Items; i ++)
	{
		if (i == EditRow) continue;
		GetColValue (i, PosPrGrStuf, Value);
		long lPrGrStuf = atol (_T(Value));
		GetColValue (i, PosKunPr, Value);
		long lKunPr = atol (_T(Value));
		GetColValue (i, PosKun, Value);
		long lKun = atol (Value);
		if (lKun == rKun && rKun != 0)
		{
			DeleteItem (i);
			InvalidateRect (NULL);
			if ( i < EditRow) EditRow --;
		}
		else if (lKunPr == rKunPr && rKunPr != 0)
		{
			DeleteItem (i);
			InvalidateRect (NULL);
			if ( i < EditRow) EditRow --;
		}
		else if (lPrGrStuf == rPrGrStuf && lKunPr == rKunPr
				&& lKun == rKun)
		{
			DeleteItem (i);
			InvalidateRect (NULL);
			if ( i < EditRow) EditRow --;
		}
	}
	< --- **
}
< ---- */

void CList1Ctrl::ScrollPositions (int pos)
{
	*Position[pos] = -1;
	for (int i = pos + 1; Position[i] != NULL; i ++)
	{
		*Position[i] -= 1;
	}
}


long CList1Ctrl::lapptest ( CString LW , CString PLZV , CString PLZB )
{

//	int rowCount = GetItemCount ();

// Logik : Es wird per sql gesucht : 
//	innerhalb PLZ_V ABSTEIGEND UND DANN DARIN innerhalb PLZ_B AUFSTEIGEND


// wegen Eindeutigkeit darf ein identischer Bereich keinen unterschiedlichen Leitwegen zugeordnet sein

// wegen Eindeutigkeit darf ein Bereich nur vollsaendig in einem anderen enthalten sein,
// sonst bek�me man je nach Suchlogik ein anderes Ergebnis je PLZ
// Eineindeutige Bereiche sind nat�rlich auch erlaubt ;-)
	CString cLW ;
	CString cPLZv ;
	CString cPLZb ;
	long vlw, vplzv, vplzb ;
	long lw, plzv, plzb ;

	lw   = (long)CStrFuncs::StrToDouble (LW);
	plzv = (long)CStrFuncs::StrToDouble (PLZV);
	plzb = (long)CStrFuncs::StrToDouble (PLZB);


	for (int i = GetItemCount()  ; i > 0 ; )
	{
		i -- ;
		cLW =  FillList.GetItemText (i, PosLeitWeg);
		vlw = (long)CStrFuncs::StrToDouble (cLW);

		cPLZv =  FillList.GetItemText (i, PosPlz_v);
		vplzv = (long)CStrFuncs::StrToDouble (cPLZv);
		cPLZb =  FillList.GetItemText (i, PosPlz_b);
		vplzb = (long)CStrFuncs::StrToDouble (cPLZb);

		if ( EditRow == i )	// Eigentest ist wohl Quatsch
			continue ;
		if ( vplzb < plzv )	// Alles kleiner, Rest wurde vorher gepr�ft 
			continue ;
		if ( vplzv > plzb ) // Alles groesser, Rest wurde vorher gepr�ft
			continue ;

		if ( vplzv == plzv ) // Startwert identisch
		{
			if ( vplzb == plzb )	// gleicher Bereich
			{
				if ( vlw == lw )	// Doppelerfassung - redundante Knete
					continue ; 		// nix zu meckern, beim naechsten Aus-/ Einlesen wird alles vernichtet
				else
					return vlw ;	// Bitte meckern : gleicher Bereich mit 2 Leitwegen!!!!
			}
			// Falls Bereichsende groesser oder kleiner ist,
			// dann steckt der eine Bereich im anderen drin
			// es wird dann eindeutig ein Bereich gefunden
			continue ;
		}
		// Rest : ein Bereich muss vollstaenidg im anderen liegen 
		if ( vplzv <= plzv && vplzb >= plzb )	// neu erfasster Bereich steckt komplett drin
			continue ;

		if ( vplzv >= plzv && vplzb <= plzb )	// Bereich aus Liste steckt komplett drin
			continue ;
		if ( vlw == lw &&      ( plzb <= vplzv  || plzv >= vplzb )  ) // gleichnamiger Bereich anschliessend oder disjunkt
			continue ;

		return vlw ;	// Bitte meckern : es passt was nicht
	}
	
	return 0 ;
}
