#ifndef _LB_TAB_DEF
#define _LB_TAB_DEF

extern struct LB lb, lb_null;
extern struct A_TAG_LB a_tag_lb, a_tag_lb_null;
extern struct A_SA_TAG_LB a_sa_tag_lb, a_sa_tag_lb_null;
extern struct WG_TAG_LB wg_tag_lb, wg_tag_lb_null;
extern struct A_WO_LB a_wo_lb, a_wo_lb_null;
extern struct A_SA_WO_LB a_sa_wo_lb, a_sa_wo_lb_null;
extern struct A_MO_LB a_mo_lb, a_mo_lb_null;
extern struct A_SA_MO_LB a_sa_mo_lb, a_sa_mo_lb_null;
extern struct WG_WO_LB wg_wo_lb, wg_wo_lb_null;
extern struct WG_MO_LB wg_mo_lb, wg_mo_lb_null;
//12.03.08
extern struct KONFIG_TAG konfig_tag, konfig_tag_null;
extern struct WAA_SUM_LB waa_sum_lb, waa_sum_lb_null;
extern struct BED_SUM_LB bed_sum_lb, bed_sum_lb_null;
extern struct FREQ_KASE_LB freq_kase_lb, freq_kase_lb_null;
extern struct FREQ_CLS freq_cls, freq_cls_null;
extern struct MWST_SUM_LB mwst_sum_lb, mwst_sum_lb_null;

//12.03.08
class KONFIG_TAG_CLASS
{
       private :
            short cursor_konfig_tag_sel;

            void out_quest_konfig_all (void);
            void prepare_konfig_tag_sel (void);       
       public:
           KONFIG_TAG_CLASS ()
           {
                    cursor_konfig_tag_sel      = -1;
           }
           int lese_konfig_tag (void);

           void close_konfig_tag (void);
};

class LB_CLASS
{
       private :
            short cursor_lb_sel_sys;
            short cursor_lb_sel;
            short cursor_lb_upd;

            void out_quest_all_sys (void);
            void out_quest_all (void);
            void prepare_lb_sel_sys (void);       
            void prepare_lb_sel (void);       
            void prepare_lb_upd (void);       
       public:
           LB_CLASS ()
           {
                    cursor_lb_sel_sys  = -1;
                    cursor_lb_sel      = -1;
                    cursor_lb_upd      = -1;
           }
           int lese_lb_sys (void);
           int lese_lb (void);
           int update_lb (void);

           void close_lese_lb_sys (void);
           void close_lese_lb (void);
           void close_update_lb (void);
};

class A_TAG_LB_CLASS
{
       private :
            short cursor_a_tag_lb_sel;
            short cursor_a_tag_lb_upd;
            short cursor_a_tag_lb_ins;

            void out_quest_all (void);
            void ins_quest_all (void);
            void prepare_a_tag_lb_sel (void);       
            void prepare_a_tag_lb_upd (void);       
            void prepare_a_tag_lb_ins (void);       
       public:
           A_TAG_LB_CLASS ()
           {
                    cursor_a_tag_lb_sel      = -1;
                    cursor_a_tag_lb_upd      = -1;
                    cursor_a_tag_lb_ins      = -1;
           }
           int lese_a_tag_lb (void);
           int update_a_tag_lb (void);
           int insert_a_tag_lb (void);

           void close_lese_a_tag_lb (void);
           void close_update_a_tag_lb (void);
           void close_insert_a_tag_lb (void);
};

class A_SA_TAG_LB_CLASS
{
       private :
            short cursor_a_sa_tag_lb_sel;
            short cursor_a_sa_tag_lb_upd;
            short cursor_a_sa_tag_lb_ins;

            void out_quest_all (void);
            void ins_quest_all (void);
            void prepare_a_sa_tag_lb_sel (void);       
            void prepare_a_sa_tag_lb_upd (void);       
            void prepare_a_sa_tag_lb_ins (void);       
       public:
           A_SA_TAG_LB_CLASS ()
           {
                    cursor_a_sa_tag_lb_sel      = -1;
                    cursor_a_sa_tag_lb_upd      = -1;
                    cursor_a_sa_tag_lb_ins      = -1;
           }
           int lese_a_sa_tag_lb (void);
           int update_a_sa_tag_lb (void);
           int insert_a_sa_tag_lb (void);

           void close_lese_a_sa_tag_lb (void);
           void close_update_a_sa_tag_lb (void);
           void close_insert_a_sa_tag_lb (void);
};

class WG_TAG_LB_CLASS
{
       private :
            short cursor_wg_tag_lb_sel;
            short cursor_wg_tag_lb_upd;
            short cursor_wg_tag_lb_ins;

            void out_quest_all (void);
            void ins_quest_all (void);
            void prepare_wg_tag_lb_sel (void);       
            void prepare_wg_tag_lb_upd (void);       
            void prepare_wg_tag_lb_ins (void);       
       public:
           WG_TAG_LB_CLASS ()
           {
                    cursor_wg_tag_lb_sel      = -1;
                    cursor_wg_tag_lb_upd      = -1;
                    cursor_wg_tag_lb_ins      = -1;
           }
           int lese_wg_tag_lb (void);
           int update_wg_tag_lb (void);
           int insert_wg_tag_lb (void);

           void close_lese_wg_tag_lb (void);
           void close_update_wg_tag_lb (void);
           void close_insert_wg_tag_lb (void);
};


class A_WO_LB_CLASS
{
       private :
            short cursor_a_wo_lb_sel;
            short cursor_a_wo_lb_upd;
            short cursor_a_wo_lb_ins;

            void out_quest_all (void);
            void ins_quest_all (void);
            void prepare_a_wo_lb_sel (void);       
            void prepare_a_wo_lb_upd (void);       
            void prepare_a_wo_lb_ins (void);       
       public:
           A_WO_LB_CLASS ()
           {
                    cursor_a_wo_lb_sel      = -1;
                    cursor_a_wo_lb_upd      = -1;
                    cursor_a_wo_lb_ins      = -1;
           }
           int lese_a_wo_lb (void);
           int update_a_wo_lb (void);
           int insert_a_wo_lb (void);

           void close_lese_a_wo_lb (void);
           void close_update_a_wo_lb (void);
           void close_insert_a_wo_lb (void);
};


class A_SA_WO_LB_CLASS
{
       private :
            short cursor_a_sa_wo_lb_sel;
            short cursor_a_sa_wo_lb_upd;
            short cursor_a_sa_wo_lb_ins;

            void out_quest_all (void);
            void ins_quest_all (void);
            void prepare_a_sa_wo_lb_sel (void);       
            void prepare_a_sa_wo_lb_upd (void);       
            void prepare_a_sa_wo_lb_ins (void);       
       public:
           A_SA_WO_LB_CLASS ()
           {
                    cursor_a_sa_wo_lb_sel      = -1;
                    cursor_a_sa_wo_lb_upd      = -1;
                    cursor_a_sa_wo_lb_ins      = -1;
           }
           int lese_a_sa_wo_lb (void);
           int update_a_sa_wo_lb (void);
           int insert_a_sa_wo_lb (void);

           void close_lese_a_sa_wo_lb (void);
           void close_update_a_sa_wo_lb (void);
           void close_insert_a_sa_wo_lb (void);
};


class A_MO_LB_CLASS
{
       private :
            short cursor_a_mo_lb_sel;
            short cursor_a_mo_lb_upd;
            short cursor_a_mo_lb_ins;

            void out_quest_all (void);
            void ins_quest_all (void);
            void prepare_a_mo_lb_sel (void);       
            void prepare_a_mo_lb_upd (void);       
            void prepare_a_mo_lb_ins (void);       
       public:
           A_MO_LB_CLASS ()
           {
                    cursor_a_mo_lb_sel      = -1;
                    cursor_a_mo_lb_upd      = -1;
                    cursor_a_mo_lb_ins      = -1;
           }
           int lese_a_mo_lb (void);
           int update_a_mo_lb (void);
           int insert_a_mo_lb (void);

           void close_lese_a_mo_lb (void);
           void close_update_a_mo_lb (void);
           void close_insert_a_mo_lb (void);
};


class A_SA_MO_LB_CLASS
{
       private :
            short cursor_a_sa_mo_lb_sel;
            short cursor_a_sa_mo_lb_upd;
            short cursor_a_sa_mo_lb_ins;

            void out_quest_all (void);
            void ins_quest_all (void);
            void prepare_a_sa_mo_lb_sel (void);       
            void prepare_a_sa_mo_lb_upd (void);       
            void prepare_a_sa_mo_lb_ins (void);       
       public:
           A_SA_MO_LB_CLASS ()
           {
                    cursor_a_sa_mo_lb_sel      = -1;
                    cursor_a_sa_mo_lb_upd      = -1;
                    cursor_a_sa_mo_lb_ins      = -1;
           }
           int lese_a_sa_mo_lb (void);
           int update_a_sa_mo_lb (void);
           int insert_a_sa_mo_lb (void);

           void close_lese_a_sa_mo_lb (void);
           void close_update_a_sa_mo_lb (void);
           void close_insert_a_sa_mo_lb (void);
};

class WG_WO_LB_CLASS
{
       private :
            short cursor_wg_wo_lb_sel;
            short cursor_wg_wo_lb_upd;
            short cursor_wg_wo_lb_ins;

            void out_quest_all (void);
            void ins_quest_all (void);
            void prepare_wg_wo_lb_sel (void);       
            void prepare_wg_wo_lb_upd (void);       
            void prepare_wg_wo_lb_ins (void);       
       public:
           WG_WO_LB_CLASS ()
           {
                    cursor_wg_wo_lb_sel      = -1;
                    cursor_wg_wo_lb_upd      = -1;
                    cursor_wg_wo_lb_ins      = -1;
           }
           int lese_wg_wo_lb (void);
           int update_wg_wo_lb (void);
           int insert_wg_wo_lb (void);

           void close_lese_wg_wo_lb (void);
           void close_update_wg_wo_lb (void);
           void close_insert_wg_wo_lb (void);
};

class WG_MO_LB_CLASS
{
       private :
            short cursor_wg_mo_lb_sel;
            short cursor_wg_mo_lb_upd;
            short cursor_wg_mo_lb_ins;

            void out_quest_all (void);
            void ins_quest_all (void);
            void prepare_wg_mo_lb_sel (void);       
            void prepare_wg_mo_lb_upd (void);       
            void prepare_wg_mo_lb_ins (void);       
       public:
           WG_MO_LB_CLASS ()
           {
                    cursor_wg_mo_lb_sel      = -1;
                    cursor_wg_mo_lb_upd      = -1;
                    cursor_wg_mo_lb_ins      = -1;
           }
           int lese_wg_mo_lb (void);
           int update_wg_mo_lb (void);
           int insert_wg_mo_lb (void);

           void close_lese_wg_mo_lb (void);
           void close_update_wg_mo_lb (void);
           void close_insert_wg_mo_lb (void);
};

//12.03.08
class WAA_SUM_LB_CLASS
{
       private :
            short cursor_waa_sum_lb_sel;
            short cursor_waa_sum_lb_upd;
            short cursor_waa_sum_lb_ins;

            void out_quest_all (void);
            void ins_quest_all (void);
            void prepare_waa_sum_lb_sel (void);       
            void prepare_waa_sum_lb_upd (void);       
            void prepare_waa_sum_lb_ins (void);       
       public:
           WAA_SUM_LB_CLASS ()
           {
                    cursor_waa_sum_lb_sel      = -1;
                    cursor_waa_sum_lb_upd      = -1;
                    cursor_waa_sum_lb_ins      = -1;
           }
           int lese_waa_sum_lb (void);
           int update_waa_sum_lb (void);
           int insert_waa_sum_lb (void);

           void close_lese_waa_sum_lb (void);
           void close_update_waa_sum_lb (void);
           void close_insert_waa_sum_lb (void);
};

//12.03.08
class BED_SUM_LB_CLASS
{
       private :
            short cursor_bed_sum_lb_sel;
            short cursor_bed_sum_lb_upd;
            short cursor_bed_sum_lb_ins;

            void out_quest_all (void);
            void ins_quest_all (void);
            void prepare_bed_sum_lb_sel (void);       
            void prepare_bed_sum_lb_upd (void);       
            void prepare_bed_sum_lb_ins (void);       
       public:
           BED_SUM_LB_CLASS ()
           {
                    cursor_bed_sum_lb_sel      = -1;
                    cursor_bed_sum_lb_upd      = -1;
                    cursor_bed_sum_lb_ins      = -1;
           }
           int lese_bed_sum_lb (void);
           int update_bed_sum_lb (void);
           int insert_bed_sum_lb (void);

           void close_lese_bed_sum_lb (void);
           void close_update_bed_sum_lb (void);
           void close_insert_bed_sum_lb (void);
};

//12.03.08
class FREQ_KASE_LB_CLASS
{
       private :
            short cursor_freq_kase_lb_sel;
            short cursor_freq_kase_lb_upd;
            short cursor_freq_kase_lb_ins;

            void out_quest_all (void);
            void ins_quest_all (void);
            void prepare_freq_kase_lb_sel (void);       
            void prepare_freq_kase_lb_upd (void);       
            void prepare_freq_kase_lb_ins (void);       
       public:
           FREQ_KASE_LB_CLASS ()
           {
                    cursor_freq_kase_lb_sel      = -1;
                    cursor_freq_kase_lb_upd      = -1;
                    cursor_freq_kase_lb_ins      = -1;
           }
           int lese_freq_kase_lb (void);
           int update_freq_kase_lb (void);
           int insert_freq_kase_lb (void);

           void close_lese_freq_kase_lb (void);
           void close_update_freq_kase_lb (void);
           void close_insert_freq_kase_lb (void);
};

//12.03.08
class FREQ_CLS_CLASS
{
       private :
            short cursor_freq_cls_sel;

            void out_quest_all (void);
            void prepare_freq_cls_sel (void);       
       public:
           FREQ_CLS_CLASS ()
           {
                    cursor_freq_cls_sel      = -1;
           }
           int lese_freq_cls (void);
           int lese_freq_cls_naechst (void);

           void close_lese_freq_cls (void);
};

//12.03.08
class MWST_SUM_LB_CLASS
{
       private :
            short cursor_mwst_sum_lb_sel;
            short cursor_mwst_sum_lb_upd;
            short cursor_mwst_sum_lb_ins;

            void out_quest_all (void);
            void ins_quest_all (void);
            void prepare_mwst_sum_lb_sel (void);       
            void prepare_mwst_sum_lb_upd (void);       
            void prepare_mwst_sum_lb_ins (void);       
       public:
           MWST_SUM_LB_CLASS ()
           {
                    cursor_mwst_sum_lb_sel      = -1;
                    cursor_mwst_sum_lb_upd      = -1;
                    cursor_mwst_sum_lb_ins      = -1;
           }
           int lese_mwst_sum_lb (void);
           int update_mwst_sum_lb (void);
           int insert_mwst_sum_lb (void);

           void close_lese_mwst_sum_lb (void);
           void close_update_mwst_sum_lb (void);
           void close_insert_mwst_sum_lb (void);
};

#endif