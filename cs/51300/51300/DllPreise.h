#ifndef _DLLPREISE_DEF
#define _DLLPREISE_DEF
#include <windows.h>

class CDllPreise
{
public:
	static HANDLE PriceLib;
    int (*preise_holen)(short dmdn, short dfil, 
		                        short dkun_fil, 
						        int dkun, double da, char *ddatum, 
						        short *sa, double *pr_ek, double *pr_vk);

	int (*SetPriceDbName) (char *);

	CDllPreise ();
};

#endif
