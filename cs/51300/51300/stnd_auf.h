#include "dbclass.h"
#ifndef _STND_AUF_DEF
#define _STND_AUF_DEF

struct STND_AUFK {
   short     mdn;
   short     fil;
   short     kun_fil;
   long      kun;
   char      kun_krz1[17];
   char      stnd_auf_kz[2];
   short     delstatus;
};
extern struct STND_AUFK stnd_aufk, stnd_aufk_null;

#line 6 "stnd_auf.rh"
struct STND_AUFP {
   short     mdn;
   short     fil;
   short     kun_fil;
   long      kun;
   double    a;
   char      me_einh_bz[6];
   long      dat;
   double    pr_vk;
   double    me;
   long      posi;
   short     me_einh;
};
extern struct STND_AUFP stnd_aufp, stnd_aufp_null;

#line 7 "stnd_auf.rh"

class SAUFK_CLASS : public DB_CLASS
{
          private :
               void prepare (void);
          public :
              SAUFK_CLASS () : DB_CLASS ()
              {
              }
              int dbreadfirst (void);
};

class SAUFP_CLASS : public DB_CLASS
{
          private :
               int cursora;
               int cursora0;
               void prepare (void);
               void preparea (void);
               void preparea0 (void);
          public :
              SAUFP_CLASS () : DB_CLASS (), cursora (-1), cursora0 (-1)
              {
              }
              int dbreadfirst (void);
              int dbreadfirsta (void);
              int dbreada (void);
              int dbreadfirsta0 (void);
              int dbreada0 (void);
              int dbclosea (void);
};
#endif