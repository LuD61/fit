#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include "comcthlp.h"
#include "itemc.h"
#include "wmaskc.h"
#include "mo_meld.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_intp.h"
#include "listcl.h"
#include "inflib.h"
#include "mo_arg.h"
#include "mo_curso.h"
#include "a_bas.h"
#include "a_hndw.h"
#include "ptab.h"
#include "mo_aufl.h"
#include "mo_qa.h"
#include "mdn.h"
#include "fil.h"
#include "kun.h"
#include "a_kun.h"
#include "aktion.h"
#include "akt_krz.h"
#include "a_pr.h"
#include "mo_a_pr.h"
#include "mo_preis.h"
#include "sys_par.h"
#include "mo_einh.h"
#include "kumebest.h"
#include "stnd_auf.h"
#include "mo_menu.h"
#include "mo_progcfg.h"
#include "mo_choise.h"
#include "enterfunc.h"
#include "DllPreise.h" //WAL-162

#define MAXLEN 40
#define MAXPOS 5000
#define LPLUS 1

#define MAXME 99999.99
#define MAXPR 9999.99

#define HNDW 1
#define EIG 2
#define EIG_DIV 3

extern HANDLE  hMainInst;

CDllPreise DllPreise; //WAL-162

static HWND hMainWin;
static unsigned char ausgabebuffer [0x1000];
static HWND mamain1;
static HWND eWindow;
static HWND AufMehWnd = NULL;
static HWND AufMehWnd0 = NULL;
static HWND AufGewhWnd = NULL;
static HWND AufGewhWnd0 = NULL;
static HWND BasishWnd;
static PAINTSTRUCT aktpaint;

static COLORREF MessBkCol = DKYELLOWCOL; 
static COLORREF MessCol   = BLACKCOL; 

static ITEM iOK ("", "     OK     ", "", 0);
static ITEM iCA ("", "  Abbrechen ", "", 0);

static int ListFocus = 3;

static int auf_wert_anz = 0;
static int auf_gew_anz = 0;
static int a_kun_smt = 0;
static double auf_vk_pr;
static int preistest;
static double prproz_diff = 50.0;
static int  art_un_tst = 0;
static BOOL  add_me = FALSE;
static BOOL a_kum_par = 0;
static BOOL sacreate = NULL;
static HWND SaWindow = NULL;
static HWND PlusWindow = NULL;
static BOOL add;
static double aufme_old;
static BOOL testmeOK = FALSE;;
static BOOL meoptimize = TRUE;
static int lutz_mdn_par = 0;
static int wa_pos_txt;
static BOOL NoRecNr = FALSE;
static BOOL ld_pr_prim = TRUE;
static int PosSave = 0;
static BOOL PosSaveMess = FALSE;
static int InsCount = 0;
static BOOL RemoveBasisMe = FALSE;

struct AUFPS 
{
       char posi [80];
       char a [80];
       char a_kun [20];
       char a_bz1 [80];
       char a_bz2 [80];
       char auf_me [80];
       char me_bz [80];
       char auf_vk_pr [80];
       char auf_lad_pr [80];
       char basis_me_bz [80];
       char teil_smt [5];
       char me_einh_kun [5];
       char me_einh [5];
       char dat [12];
};

struct AUFPS aufps, aufptab [MAXPOS], aufps_null;

ITEM iposi        ("posi", aufps.posi,             "", 0);
ITEM ia           ("a",          aufps.a,          "", 0);
ITEM ia_kun       ("a_kun",      aufps.a_kun,      "", 0);
ITEM ia_bz1       ("a_bz1",      aufps.a_bz1,      "", 0);
ITEM ia_bz2       ("a_bz2",      aufps.a_bz2,      "", 0);
ITEM iauf_me      ("auf_me",     aufps.auf_me,     "", 0);
ITEM ime_bz       ("me_bz",      aufps.me_bz,      "", 0);
ITEM ipr_vk       ("pr_vk",      aufps.auf_vk_pr,  "", 0);
ITEM ilad_pr      ("ld_pr",      aufps.auf_lad_pr, "", 0);
ITEM ibasis_me_bz ("basis_me_bz",aufps.basis_me_bz,"", 0);
ITEM idat         ("dat",        aufps.dat,        "", 0);


/**
static field  _dataform[30] = {
&iposi,        5, 0, 0, 6, 0, "%4d",     DISPLAYONLY, 0, 0, 0, 
&ia,          14, 0, 0, 12, 0, "",       DISPLAYONLY, 0, 0, 0, 
&ia_kun,      14, 0, 1, 12, 0, "",       REMOVED, 0, 0, 0, 
&ia_bz1,      25, 0, 0, 27, 0, "",       DISPLAYONLY, 0, 0, 0, 
&iauf_me,     11, 0, 0, 54, 0, "%8.3f",  EDIT,        0, 0, 0, 
&ime_bz ,     11, 0, 0, 67, 0, "",       DISPLAYONLY, 0, 0, 0,
&ipr_vk,       9, 0, 0, 80,0, "%6.2f",   EDIT,        0, 0, 0, 
&ilad_pr,      9, 0, 0, 91,0, "%6.2f",   DISPLAYONLY, 0, 0, 0, 
&ibasis_me_bz,11, 0, 0,102, 0, "",       DISPLAYONLY, 0, 0, 0,
&idat,        11, 0, 0,117, 0, "",       DISPLAYONLY, 0, 0, 0,
&ia_bz2,      25, 0, 1, 27, 0, "",       DISPLAYONLY, 0, 0, 0, 
};

static form dataform = {11, 0, 0, _dataform, 0, 0, 0, 0, NULL}; 
**/
static field  _dataform[30] = {
&iposi,        5, 0, 0, 6, 0, "%4d",     DISPLAYONLY, 0, 0, 0, 
&ia,          14, 0, 0, 12, 0, "",       DISPLAYONLY, 0, 0, 0, 
&ia_kun,      14, 0, 1, 12, 0, "",       REMOVED, 0, 0, 0, 
&ia_bz1,      25, 0, 0, 27, 0, "",       DISPLAYONLY, 0, 0, 0, 
&ia_bz2,      25, 0, 0, 54, 0, "",       DISPLAYONLY, 0, 0, 0, 
&iauf_me,     11, 0, 0, 54+27, 0, "%8.3f",  EDIT,        0, 0, 0, 
&ime_bz ,     11, 0, 0, 67+27, 0, "",       DISPLAYONLY, 0, 0, 0,
&ipr_vk,       9, 0, 0, 80+27,0, "%6.2f",   EDIT,        0, 0, 0, 
&ilad_pr,      9, 0, 0, 91+27,0, "%6.2f",   DISPLAYONLY, 0, 0, 0, 
&ibasis_me_bz,11, 0, 0,102+27, 0, "",       DISPLAYONLY, 0, 0, 0,
&idat,        11, 0, 0,117+27, 0, "",       DISPLAYONLY, 0, 0, 0,
};

static form dataform = {11, 0, 0, _dataform, 0, 0, 0, 0, NULL}; 

static BOOL DelLadVK  = TRUE;
static BOOL DelPrVK  = FALSE;
static BOOL DelLastMe = TRUE;

static BOOL FormOK = FALSE;

void addField (form * frm, field * feld, int pos, int len, int space)
{
	int i;
	int plus;

	frm->fieldanz ++;
	plus = len + space;

	feld->pos[1] = frm->mask[pos].pos[1];
	for (i = frm->fieldanz - 1; i > pos; i --)
	{
		memcpy (&frm->mask[i], &frm->mask[i - 1], sizeof (field));
		frm->mask[i].pos[1] += plus;
	}
	memcpy (&frm->mask[i], feld, sizeof (field));
}


int addFieldName (form * frm, field * feld, char *name, int space)
{
	int pos;
	int len;

	len = feld->length;
	pos = GetItemPos (frm, name);
	if (pos == -1)
	{
		return pos;
	}
	addField (frm, feld, pos, len, space);
	return pos;
}


void DelFormField (form *frm, int pos)
/**
Feld aus Dataform herausnehmen.
**/
{
	         int i;
			 int diff;
			 int pos1, pos2;

			 pos1 = frm->mask[pos].pos[1];
			 if (pos < frm->fieldanz - 1)
			 {
				 pos2 = frm->mask[pos + 1].pos[1];
				 diff = max (0, pos2 - pos1);
			 }
			 else
			 {
				 diff = frm->mask[pos].length;
				 if (frm != &dataform) diff ++;
			 }
			 if (diff == 0)
			 {
				 diff = frm->mask[pos].length;
				 if (frm != &dataform) diff ++;
			 }

			 frm->fieldanz --;

			 for (i = pos; i < frm->fieldanz; i ++)
			 {
				 memcpy ((char *) &frm->mask[i], (char *) &frm->mask[i + 1], sizeof (field));
			 }

			 for (i = 0; i < frm->fieldanz; i ++)
			 {
				 if (frm->mask[i].pos[1] > pos1)
				 {
					 frm->mask[i].pos[1] -= diff;
				 }
			 }
}


static int ubrows [] = {0, 
                        1,1,
                        2,3,4,5,6,7,8};


struct CHATTR ChAttra [] = {"a",     DISPLAYONLY, EDIT,
                             NULL,  0,           0};
struct CHATTR ChAttra_kun [] = {"a_kun", DISPLAYONLY, EDIT,
                                 NULL,   0,           0};

struct CHATTR *ChAttr = ChAttra;

ColButton Cuposi = {  "Pos.", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};
ColButton Cua = {
                     "Artikel-Nr", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};
ColButton Cua_bz1 = {
                     "Bezeichnung1", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};

ColButton Cua_bz2 = {
                     "Bezeichnung2", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};

ColButton Cuauf_me = {
                     "A.-Menge", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};

ColButton Cume_bz = {
                     "Best.ME", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};

ColButton Cupr_vk = {
                     "VK", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};

ColButton Cubasis_me_bz = {
                     "Basis-ME", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};


static char *VK        = "  VK    "; 
static char *VK_DM     = " VK DM  ";
static char *VK_EURO   = " VK EURO ";
static char *VK_FREMD  = " VK FREMD ";

static char *LD        = " Ld.VK  "; 
static char *LD_DM     = " LD DM  ";
static char *LD_EURO   = " LD EURO ";
static char *LD_FREMD  = "LD FREMD";


ITEM iuposi        ("posi",       "Pos.",            "", 0);
ITEM iua           ("a",          "Artikel-Nr   ",   "", 0);
ITEM iua_bz1       ("a_bz1",      "Bezeichnung 1",   "", 0);
ITEM iua_bz2       ("a_bz2",      "Bezeichnung 2",   "", 0);
ITEM iuauf_me      ("auf_me",     "Stnd.Menge",      "", 0);
ITEM iume_bz       ("me_bz",      "Best.ME",         "",  0);
ITEM iupr_vk       ("pr_vk",       "VK-Pr.",         "", 0);
ITEM iulad_pr      ("ld_pr",       LD,               "", 0);
ITEM iubasis_me_bz ("basis_me_bz","Basis-ME",        "",  0);
ITEM iudat         ("dat",        "Datum",           "",  0);
ITEM iufiller      ("",                "",           "", 0);


/**
static field  _ubform[30] = {
&iuposi,        5, 0, 0,  6, 0, "",  BUTTON, 0, 0, 0, 
&iua,          15, 0, 0, 11, 0, "",  BUTTON, 0, 0, 0, 
&iua_bz1,      27, 0, 0, 26, 0, "",  BUTTON, 0, 0, 0, 
&iuauf_me,     13, 0, 0, 53, 0, "",  BUTTON, 0, 0, 0, 
&iume_bz,      13, 0, 0, 66, 0, "",  BUTTON, 0, 0, 0,
&iupr_vk,      11, 0, 0, 79,0,  "",  BUTTON, 0, 0, 0, 
&iulad_pr,     11, 0, 0, 90,0,  "",  BUTTON, 0, 0, 0, 
&iubasis_me_bz,15, 0, 0,101, 0, "",  BUTTON, 0, 0, 0,
&iudat,        13, 0, 0,116, 0, "",  BUTTON, 0, 0, 0,
&iufiller,    100, 0, 0,129, 0, "", BUTTON, 0, 0, 0, 
};
*/
static field  _ubform[30] = {
&iuposi,        5, 0, 0,  6, 0, "",  BUTTON, 0, 0, 0, 
&iua,          15, 0, 0, 11, 0, "",  BUTTON, 0, 0, 0, 
&iua_bz1,      27, 0, 0, 26, 0, "",  BUTTON, 0, 0, 0, 
&iua_bz2,      27, 0, 0, 53, 0, "",  BUTTON, 0, 0, 0, 
&iuauf_me,     13, 0, 0, 53+27, 0, "",  BUTTON, 0, 0, 0, 
&iume_bz,      13, 0, 0, 66+27, 0, "",  BUTTON, 0, 0, 0,
&iupr_vk,      11, 0, 0, 79+27,0,  "",  BUTTON, 0, 0, 0, 
&iulad_pr,     11, 0, 0, 90+27,0,  "",  BUTTON, 0, 0, 0, 
&iubasis_me_bz,15, 0, 0,101+27, 0, "",  BUTTON, 0, 0, 0,
&iudat,        13, 0, 0,116+27, 0, "",  BUTTON, 0, 0, 0,
&iufiller,    100, 0, 0,129+27, 0, "", BUTTON, 0, 0, 0, 
};

static form ubform = {10, 0, 0, _ubform, 0, 0, 0, 0, NULL}; 

ITEM iline ("", "1", "", 0);


/***
static field  _lineform[30] = {
&iline,      1, 0, 0, 11, 0, "",  NORMAL, 0, 0, 0, 
&iline,      1, 0, 0, 26, 0, "",  NORMAL, 0, 0, 0, 
&iline,      1, 0, 0, 53, 0, "",  NORMAL, 0, 0, 0, 
&iline,      1, 0, 0, 66, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0, 79, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0, 90, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0,101, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0,116, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0,129, 0, "",  NORMAL, 0, 0, 0,
};

**/
static field  _lineform[30] = {
&iline,      1, 0, 0, 11, 0, "",  NORMAL, 0, 0, 0, 
&iline,      1, 0, 0, 26, 0, "",  NORMAL, 0, 0, 0, 
&iline,      1, 0, 0, 53, 0, "",  NORMAL, 0, 0, 0, 
&iline,      1, 0, 0, 53+27, 0, "",  NORMAL, 0, 0, 0, 
&iline,      1, 0, 0, 66+27, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0, 79+27, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0, 90+27, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0,101+27, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0,116+27, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0,129+27, 0, "",  NORMAL, 0, 0, 0,
};
static form lineform = {11, 0, 0, _lineform, 0, 0, 0, 0, NULL}; 


static ListClassDB eListe;
PTAB_CLASS ptab_class;
WA_PREISE WaPreis;
HNDW_CLASS HndwClass;
SYS_PAR_CLASS sys_par_class;
static QueryClass QClass;
static SAUFP_CLASS Saufp;
static DB_CLASS DbClass;
static EINH_CLASS einh_class;

static PROG_CFG ProgCfg ("51300");
static int plu_size = 4;
static int auf_me_default= 0;
static long aufkunanz = 5;

static BOOL searchadirect = TRUE;
static BOOL searchmodedirect = TRUE;
static double RowHeight = 1.5;
static int UbHeight = 0;
static int bsd_kz = 1;
static int matchcode = 0;

static BOOL NoArtMess = FALSE;

static BOOL ListColors = TRUE;
static BOOL ber_komplett = TRUE;
static COLORREF KompfColor = BLACKCOL;
static COLORREF KompbColor = GRAYCOL;
static COLORREF SafColor   = WHITECOL;
static COLORREF SabColor   = BLACKCOL;


static char KunItem[] = {"kuna"};
static double Akta;
static BOOL auf_me_pr_0 = 1;
static int preis0_mess = 1;
static double inh = 0.0;
static long dauertief = 90l;

static double akt_me;


void DelFormFieldEx (form *frm, int pos, int diff)
/**
Feld aus Dataform herausnehmen.
**/
{
	         int i;
			 int pos1;

			 pos1 = frm->mask[pos].pos[1];
			 frm->fieldanz --;

			 for (i = pos; i < frm->fieldanz; i ++)
			 {
				 memcpy ((char *) &frm->mask[i], (char *) &frm->mask[i + 1], sizeof (field));
			 }

			 for (i = 0; i < frm->fieldanz; i ++)
			 {
				 if (frm->mask[i].pos[1] > pos1)
				 {
					 frm->mask[i].pos[1] -= diff;
				 }
			 }
}


void DelListField (char *Item)
{
	         int pos, diff;

		     diff = 0;
		     pos = GetItemPos (&ubform, Item);
			 if (pos > -1 && pos < ubform.fieldanz - 1)
			 {
					 diff = ubform.mask[pos + 1].pos[1] -
						    ubform.mask[pos].pos[1];
			 }
	         if (pos > -1) DelFormFieldEx (&ubform, pos, diff);
	         if (pos > -1) DelFormFieldEx (&lineform, pos - 1, diff);
		     pos = GetItemPos (&dataform, Item);
	         if (pos > -1) DelFormFieldEx (&dataform, pos, diff);
}


static TEXTMETRIC textm; 

static int EnterBreak ()
{
	 break_enter ();
	 return (0);
}

static int EnterTest (void)
/**
Abfrage in Query-Eingabe.
**/
{
        switch (syskey)
        {
                case KEY5 :
                       break_enter ();
                       return 1;
                case KEY11 :
                case KEY12 :
                       syskey = KEY12;
                       break_enter ();
                       return 1;
        }
		if (syskey == KEYCR)
		{
		        if (current_form->mask[currentfield].BuId == KEY12)
				{
					syskey = KEY12;
					break_enter ();
					return 1;
				}
		        else if (current_form->mask[currentfield].BuId == KEY5)
				{
					syskey = KEY5;
					break_enter ();
					return 1;
				}
		}
        return 0;
}


void SetUbHeight (void)
/**
Hoehe der Listueberschrift setzen.
**/
{
	int i;

	for (i = 0; i < ubform.fieldanz; i ++)
	{
		ubform.mask[i].rows = UbHeight;
	}
}

static int InfoProc (char **Item, char *Value, char *where)
/**
Spezielle Procedure fuer Info-System
**/
{

    clipped (*Item);
//    if (strcmp (*Item, "a") == 0)
    {
        *Item = KunItem;
        sprintf (where, "where kun = %ld and a = %.0lf", 
                         stnd_aufk.kun, ratod (Value));
        return 1;
    }
    return 0;
}

short AUFPLIST::auf_art = 0;

void AUFPLIST::SetMessColors (COLORREF color, COLORREF bkcolor)
/**
Farben fuer Melungen setzen.
**/
{
	 MessCol   = color;
	 MessBkCol = bkcolor;
}

static long mdnprod = 0;


void AUFPLIST::SethMainWindow (HWND hMainWindow)
{
    this->hMainWindow = hMainWindow;
	hMainWin = hMainWindow;
}

AUFPLIST:: AUFPLIST ()
{
    mamainmax = 0;
    mamainmin = 0; 
    eingabesatz = NULL;
    ausgabesatz = ausgabebuffer;
    zlen = 0;
    feld_anz = 0;
    banz = 0;
    PageView = 0;
    LstZiel = NULL;
    LstSatz   = NULL;
    Lstbanz = 0;
    SwRecs = 0;
    AktRow = 0;
    AktColumn = 0;
    scrollpos = 0;
    InfoCaption[0] = (char) 0; 
    WithMenue   = 1;
    WithToolBar = 1;
	preistest = 0;
	add = FALSE;
    lutz_mdn_par = 0;
	liney = 0;
	aufme_old = (double) 0.0;
    dataform.after  = WriteRow; 
    dataform.before = SetRowItem; 
    dataform.mask[1].before = Savea; 
    dataform.mask[1].after  = fetcha; 
    dataform.mask[2].after  = fetcha_kun; 
    dataform.mask[5].before = setkey9me; 
    dataform.mask[5].after  = testme; 
    dataform.mask[7].before  = setkey9basis; 
    ListAktiv = 0;
    inh = (double) 0.0;
    this->hMainWindow = NULL;
	hMainWin = NULL;
}


int AUFPLIST::ShowBasis (void)
/**
Basismenge der aktuellen Bestellmenge anzeigen.
**/
{
	HDC hdc;
	double auf_me;
	double a;
	short me_einh_kun;
	double auf_me_vgl;
    KEINHEIT keinheit;
	HWND hWnd; 
	HWND hMainWindow; 

	static char basis_me[15];
	static char basis_einh[11];


    static mfont anzfont    = {NULL, -1, -1, 1,
                               BLACKCOL,
                               WHITECOL,
                               0};

	static ITEM ibasis_me    ("basis_me", basis_me, "Menge in Basiseinheit :", 0);
	static ITEM ibasis_einh  ("me_einh",  basis_einh, "", 0);
	static ITEM iOK          ("OK",       "OK", "", 0);

	static field _fbasis_me[] = {
		&ibasis_me,      9, 0, 1, 2, 0, "%8.2f", READONLY, 0, 0, 0,
		&ibasis_einh,    8, 0, 1,36, 0, "",      DISPLAYONLY, 0, 0, 0,
		&iOK,           10, 0, 3,17, 0, "",      BUTTON,      0, StopEnter, KEY5,
	};

	static form fbasis_me = {3, 0, 0, _fbasis_me, 0, 0, 0, 0, NULL};

	anzfont.FontName = "                   ";
    GetStdFont (&anzfont);
//	anzfont.FontName = "ARIAL";
    anzfont.FontAttribute = 1;

	auf_me = ratod (aufps.auf_me);
	a  = ratod (aufps.a);
	me_einh_kun = atoi (aufps.me_einh_kun);

    einh_class.AktAufEinh (stnd_aufk.mdn, stnd_aufk.fil,
                           stnd_aufk.kun, a, me_einh_kun);
    einh_class.GetKunEinh (stnd_aufk.mdn, stnd_aufk.fil,
                           stnd_aufk.kun, a, &keinheit);
          
    if (keinheit.me_einh_kun == keinheit.me_einh_bas)
    {
            auf_me_vgl = auf_me;
    }
    else
    {
            if (keinheit.inh <= (double) 0.0)
            {
                     keinheit.inh = (double) 1.0;
            }
            auf_me_vgl = auf_me * keinheit.inh;
    }

	sprintf (basis_me, "%8.2lf", auf_me_vgl);
	strcpy  (basis_einh, keinheit.me_einh_bas_bez);

    break_end ();
	SetButtonTab (TRUE);
	hMainWindow = GetActiveWindow ();
    DisablehWnd (hMainWindow);
    SetBorder (WS_POPUP | WS_VISIBLE| WS_DLGFRAME);
	SethStdWindow ("hListWindow");
    hWnd = OpenWindowChC (5, 46, 10, 14, hMainInst, ""); 
	SethStdWindow ("hStdWindow");
	BasishWnd = hWnd;
    hdc = GetDC (hWnd);
    ReleaseDC (hWnd, hdc);

    SetStaticWhite (TRUE);
    enter_form (hWnd, &fbasis_me, 0, 0);
    SetStaticWhite (FALSE);

	CloseControls (&fbasis_me);
    DestroyWindow (hWnd);
	BasishWnd = NULL;
	AktivWindow = hMainWindow;
	SetButtonTab (FALSE);
    eListe.SetFeldFocus0 (eListe.GetAktRow (), eListe.GetAktColumn ());
	return 0;
}

void AUFPLIST::SetChAttr (int ca)
{
    switch (ca)
    {
          case 0 :
              ChAttr = ChAttra;
              break;
          case 1:
              ChAttr = ChAttra_kun;
              break;
    }
}

void AUFPLIST::SetFieldAttr (char *fname, int attr)
{
         int i;
         char *feldname;

         for (i = 0; i < dataform.fieldanz; i ++)
         {
             feldname = dataform.mask[i].item->GetItemName ();
             if (feldname && strcmp (feldname, fname) == 0)
             {
                   break;
             }
         }
         if (i == dataform.fieldanz) return;

         dataform.mask[i].attribut = attr;
}

int AUFPLIST::GetFieldAttr (char *fname)
{
         int i;
         char *feldname;

         for (i = 0; i < dataform.fieldanz; i ++)
         {
             feldname = dataform.mask[i].item->GetItemName ();
             if (feldname && strcmp (feldname, fname) == 0)
             {
                   break;
             }
         }
         if (i == dataform.fieldanz) return -1;

         return (dataform.mask[i].attribut);
}

void AUFPLIST::Geta_kum_par (void)
{
    static BOOL ParOK = 0;

    if (ParOK) return;

    ParOK = 1;
    a_kum_par = FALSE;
    strcpy (sys_par.sys_par_nam,"a_kum_par");
    if (sys_par_class.dbreadfirst () == 0)
    {
        if (atoi (sys_par.sys_par_wrt)) 
        {
                  a_kum_par = TRUE;
        }
    }
    return;
}


void AUFPLIST::Geta_bz2_par (void)
{
    static BOOL ParOK = 0;

	return; //WAL-109
    if (ParOK) return;

    ParOK = 1;
    SetFieldAttr ("a_bz2", REMOVED);
    return;
    strcpy (sys_par.sys_par_nam,"a_bz2_par");
    if (sys_par_class.dbreadfirst () == 0)
    {
        if (atoi (sys_par.sys_par_wrt)) 
        {
                  SetFieldAttr ("a_bz2", DISPLAYONLY);
                  return;
        }
    }
    SetFieldAttr ("a_bz2", REMOVED);
}


double AUFPLIST::GetAufMeVgl (double a, double auf_me, short me_einh_kun)
/**
Mengeneinheit fuer Kunde und Artikel holen.
**/
{
         KEINHEIT keinheit;
         double auf_me_vgl;

         einh_class.AktAufEinh (stnd_aufk.mdn, stnd_aufk.fil,
                                stnd_aufk.kun, a, me_einh_kun);
         einh_class.GetKunEinh (stnd_aufk.mdn, stnd_aufk.fil,
                                stnd_aufk.kun, a, &keinheit);
          
         if (keinheit.me_einh_kun == keinheit.me_einh_bas)
         {
                      auf_me_vgl = auf_me;
         }
         else
         {
                     if (keinheit.inh <= (double) 0.0)
                     {
                                   keinheit.inh = (double) 1.0;
                     }
                     auf_me_vgl = auf_me * keinheit.inh;
         }
         return auf_me_vgl;
}



int AUFPLIST::SetRowItem (void)
{
	   int pos;
	   double auf_me;

	   pos = eListe.GetAktRow ();
       eListe.SetRowItem ("a", aufptab[pos].a);
	   auf_me = ratod (aufptab[pos].auf_me);
	   if (auf_me == (double) 0.0)
	   {
		   akt_me = (double) 0.0;
	   }
	   else
	   {

           akt_me = GetAufMeVgl (ratod (aufptab[pos].a), auf_me,
			                     atoi (aufptab[pos].me_einh_kun));
	   }
	   auf_vk_pr = ratod (aufps.auf_vk_pr);
       return 0;
}

void AUFPLIST::SetAufVkPr (double pr)
{
	   auf_vk_pr = pr;
}

int AUFPLIST::TestAppend (void)
/**
Testen, ob ein Satz angehaengt werden darf.
**/
{
    if (eListe.GetRecanz () == 0);
    else if (ratod (aufps.a) == (double) 0.0)
    {
        return FALSE;
    }
  
    if (PosSave > 0)
    {
        if (TestInsCount () == FALSE) return 0;
    }
    testmeOK = FALSE; 
	auf_vk_pr = (double) 0.0;
	memcpy (&aufps, &aufps_null, sizeof (struct AUFPS)); 
    eListe.SetPos (eListe.GetAktRow (), eListe.FirstColumn ());
    return TRUE;
}

int AUFPLIST::DeleteLine (void)
/**
Zeile aus Liste loeschen.
**/
{
        eListe.DeleteLine ();
        testmeOK = FALSE; 
        return 0;
}

int AUFPLIST::InsertLine (void)
/**
Zeile aus Liste loeschen.
**/
{
        if (PosSave > 0)
        {
            if (TestInsCount () == FALSE) return 0;
        }
        testmeOK = FALSE; 
        eListe.SetPos (eListe.GetAktRow (), eListe.FirstColumn ());
        eListe.InsertLine ();
        return 0;
}



BOOL AUFPLIST::SavePosis (void)
{
       int recs;
       int i;

       if (PosSave > 1 && PosSaveMess && 
           abfragejn (eListe.Getmamain3 (), 
		             "Positionen speichern ?" , "J") == 0)
       {
           DoBreak ();
           return FALSE;
       }
       memcpy (&aufptab[eListe.GetAktRow ()], &aufps, sizeof (struct AUFPS));
       recs = eListe.GetRecanz ();

       GenNewPosi ();
       for (i = recs - PosSave; i < recs; i ++)
       {
           WritePos (i);
       }
       commitwork ();
       beginwork ();
       InsCount ++;
       return TRUE;
}


BOOL AUFPLIST::TestInsCount (void)
{
        if (InsCount >= PosSave)
        {
            InsCount = 0;
            return SavePosis ();
        }
        InsCount ++;
        return TRUE;
}


int AUFPLIST::AppendLine (void)
/**
Zeile aus Liste loeschen.
**/
{
        if (PosSave > 0)
        {
            if (TestInsCount () == FALSE) return 0;
        }
        testmeOK = FALSE; 
	    auf_vk_pr = (double) 0.0;
        eListe.AppendLine ();
        return 0;
}


int AUFPLIST::WriteRow (void)
/**
Zeile beim Verlassen pruefen.
**/
{
    if (testme () == -1) return -1;
    if (testpr () == -1) return -1;
    set_fkt (NULL, 9);
    SetFkt (9, leer, NULL);
    if (ratod (aufps.a) == (double) 0.0)
    {
        eListe.DeleteLine ();
        return (1);
    }

    return (0);
}

int AUFPLIST::TestRow (void)
/**
Zeile beim Verlassen pruefen.
**/
{
     
    if (ratod (aufps.auf_me) == (double) 0.0) return 0;
    if (testme () == -1) return -1;
    if (testpr () == -1) return -1;
    set_fkt (NULL, 9);
    SetFkt (9, leer, NULL);
    return (0);
}

void AUFPLIST::GenNewPosi (void)
/**
Neue Positionsnummern in 10-er Schritten generieren.
**/
{
    int i;
    int row;
    int recs;
    long posi;

    row  = eListe.GetAktRow ();
    memcpy (&aufptab[row], &aufps, sizeof (struct AUFPS));
    recs = eListe.GetRecanz ();
    posi = 10;
    for (i = 0; i < recs; i ++, posi += 10)
    {
        sprintf (aufptab[i].posi, "%ld", posi);
    }
    eListe.DisplayList ();
    memcpy (&aufps, &aufptab[row], sizeof (struct AUFPS));
}


int AUFPLIST::PosiEnd (long posi)
/**
Positionnummer testen.
**/
{
    int row;
    int recs;
    long nextposi;

    row  = eListe.GetAktRow ();
    recs = eListe.GetRecanz ();

    if (row >= recs - 1)
    {
            return FALSE;
    }

    nextposi = atol (aufptab [row + 1].posi);
    if (nextposi <= posi)
    {
        GenNewPosi ();
        return TRUE;
    }
    return FALSE;
}

void AUFPLIST::TestMessage (void)
{
	MSG msg;

    if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
    {
          TranslateMessage(&msg);
          DispatchMessage(&msg);
    }
}


int AUFPLIST::Querya (void)
/**
Auswahl ueber Artikel.
**/
{
       int ret;

	   if (searchadirect)
	   {
            ret = QClass.searcha (eListe.Getmamain3 (), stnd_aufk.kun_fil);
	   }
       else 
	   {
            ret = QClass.querya (eListe.Getmamain3 (), stnd_aufk.kun_fil);
	   }
       set_fkt (dokey5, 5);
       set_fkt (WriteAllPos, 12);
       if (ret == FALSE)
       {
           eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                                 eListe.GetAktColumn ());
           return 0;
       }
       UpdateWindow (mamain1);
       sprintf (aufptab[eListe.GetAktRow ()].a, "%.0lf", _a_bas.a);
       eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
       eListe.ShowAktRow ();
       eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                             eListe.GetAktColumn ());
       PostMessage (eListe.Getmamain3 (), WM_KEYDOWN, VK_RETURN, 0l);
       return 0;
}
       

int AUFPLIST::Savea (void)
/**
Artikelnummer sichern.
**/
{
       Akta = ratod (aufps.a);
	   if (! numeric (aufps.a))
	   {
		   sprintf (aufps.a, "%13.0lf", 0.0);
           memcpy (&aufptab[eListe.GetAktRow()], &aufps, sizeof (struct AUFPS));
           eListe.ShowAktRow ();
	   }
       set_fkt (Querya, 9);
       SetFkt (9, auswahl, KEY9);
       return 0;
}

int AUFPLIST::ChMeEinh (void)
/**
Auswahl Auftragsmengeneinheit.
**/
{

       kumebest.mdn = stnd_aufk.mdn;
       kumebest.fil = stnd_aufk.fil;
       kumebest.kun = stnd_aufk.kun;
       strcpy (kumebest.kun_bran2, kun.kun_bran2);
       kumebest.a = ratod (aufps.a);
       if (ratod (aufps.a))
       {
           einh_class.AktAufEinh (stnd_aufk.mdn, stnd_aufk.fil, stnd_aufk.kun, 
                                  ratod (aufps.a), atoi (aufps.me_einh_kun));
       }
       else
       {
           einh_class.SetAufEinh (1);
       }
       EnableWindow (mamain1, FALSE);
       einh_class.ChoiseEinh ();
       EnableWindow (mamain1, TRUE);
       _a_bas.a = ratod (aufps.a);
       if (syskey == KEY5) 
	   {
           eListe.SetFeldFocus0 (eListe.GetAktRow (), eListe.GetAktColumn ());
		   return 0;
	   }
       ReadMeEinh ();

       memcpy (&aufptab[eListe.GetAktRow()], &aufps, sizeof (struct AUFPS));
       eListe.ShowAktRow ();
       eListe.SetFeldFocus0 (eListe.GetAktRow (), eListe.GetAktColumn ());
       return 0;
}

int AUFPLIST::setkey9me (void)
/**
Artikelnummer sichern.
**/
{
       testmeOK = FALSE; 
       set_fkt (ChMeEinh, 9);
       SetFkt (9, einhausw, KEY9);
       return 0;
}

int AUFPLIST::setkey9basis (void)
/**
Artikelnummer sichern.
**/
{
	   auf_vk_pr = ratod (aufps.auf_vk_pr);
       set_fkt (ShowBasis, 9);
       SetFkt (9, basisme, KEY9);
       return 0;
}

void AUFPLIST::MovePlus (void)
/**
Fenster Sonderangebot bewegen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;

        if (hMainWin == NULL) return;    
        if (AufMehWnd == NULL) return;    
		if (PlusWindow == NULL) return; 
           
	    memcpy (&tm, &textm, sizeof (tm)); 

        GetClientRect (hMainWin, &rect);
        GetWindowRect (hMainWin, &wrect);

        y = (wrect.top +  9 * tm.tmHeight);
        cx = 3 * tm.tmAveCharWidth + 2;
        x = wrect.left + rect.right - cx + 1;
        cy = 2 * tm.tmHeight;
		MoveWindow (PlusWindow, x, y, cx, cy, TRUE);
}

HWND AUFPLIST::CreatePlus (void)
/**
Fenster fuer Sonderangebot erzeugen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;

        if (hMainWin == NULL) return NULL;    
           
	    memcpy (&tm, &textm, sizeof (tm)); 

        GetClientRect (hMainWin, &rect);
        GetWindowRect (hMainWin, &wrect);


        y = (wrect.top +  0 * tm.tmHeight);
        cx = 3 * tm.tmAveCharWidth + 2;
        x = wrect.left + rect.right - cx + 1;
        cy = 2 * tm.tmHeight;

        PlusWindow       = CreateWindow (
                                       "StaticMess",
                                       "",
                                       WS_BORDER | 
                                       WS_POPUP | WS_VISIBLE,
                                       x, y,
                                       cx, cy,
                                       hMainWin,
                                       NULL,
                                       hMainInst,
                                    NULL);


        return PlusWindow;
}

void AUFPLIST::PaintPlus (HDC hdc)
/**
+ anzeigen.
**/
{
	  RECT rect;
	  SIZE size;
	  int x,y;
	  extern HFONT stdHfont;
	  static char *txt = "+";

	  if (PlusWindow == NULL) return;

	  GetClientRect (PlusWindow, &rect); 
      SelectObject (hdc, stdHfont);
      GetTextExtentPoint32 (hdc, txt, strlen (txt), &size); 
	  x = max (0, (rect.right  - rect.left - size.cx) / 2);
	  y = max (0, (rect.bottom - rect.top  - size.cy) / 2);
	  SetBkMode (hdc, TRANSPARENT);
      SetTextColor (hdc,MessCol);
      SetBkColor (hdc, MessBkCol);
	  TextOut (hdc, x, y, txt, strlen (txt));
}


void AUFPLIST::DestroyPlus (void)
/**
Fenster mit + loeschen.
**/
{
	  if (PlusWindow == NULL)
	  {
		  return;
	  }
	  DestroyWindow (PlusWindow);
	  PlusWindow = NULL;
}


void AUFPLIST::ReadPr (void)
/**
Artikelpreis holen.
**/
{
       char lieferdat [12];
       int dsqlstatus;
       short sa;
       double pr_ek;
       double pr_vk;
       
       sysdate (lieferdat);

// Preise immer mit Filiale 0 lesen.

	   WaPreis.SetAufArt (auf_art);



      if (DllPreise.PriceLib != NULL && 
			  DllPreise.preise_holen != NULL)
	  {
			      dsqlstatus = (DllPreise.preise_holen) (stnd_aufk.mdn,
                                          0,
                                          stnd_aufk.kun_fil,
                                          stnd_aufk.kun,
                                           ratod (aufps.a),
                                          "31.12.9999",
                                          &sa, 
                                          &pr_ek,
                                          &pr_vk);
			      if (dsqlstatus ==1) dsqlstatus = 0;
	  }

/***  WAL-162 nur noch �ber preisewa.dll
       dsqlstatus = WaPreis.preise_holen (stnd_aufk.mdn,
                                          0,
                                          stnd_aufk.kun_fil,
                                          stnd_aufk.kun,
                                           ratod (aufps.a),
                                          "31.12.9999",
                                          &sa, 
                                          &pr_ek,
                                          &pr_vk);
*********/
       if ((NoArtMess == FALSE) && (pr_ek == (double) 0.0))
       {
               if (preis0_mess == 0)
			   {
                      disp_mess ("Achtung !!\nPreis 0 gelesen", 2);
			   }
       }

       if (dsqlstatus == 0)
       {
                 sprintf (aufps.auf_vk_pr, "%6.2lf",  pr_ek);
       }
	   auf_vk_pr = pr_ek;
}

void AUFPLIST::ReadMeEinh (void)
/**
Mengeneinheiten holen.
**/
{
        int dsqlstatus;
        char ptwert [5];

        KEINHEIT keinheit;

        einh_class.GetKunEinh (stnd_aufk.mdn, stnd_aufk.fil, stnd_aufk.kun,
                               _a_bas.a, &keinheit);
        strcpy (aufps.basis_me_bz, keinheit.me_einh_bas_bez);
        strcpy (aufps.me_bz, keinheit.me_einh_kun_bez);
        sprintf (aufps.me_einh_kun, "%hd", keinheit.me_einh_kun);
        sprintf (aufps.me_einh,     "%hd", keinheit.me_einh_bas);
        inh = keinheit.inh;

        return;


        switch (_a_bas.a_typ)
        {
             case HNDW :
                     dsqlstatus = HndwClass.lese_a_hndw (_a_bas.a);
                     break;
             case EIG :
                     dsqlstatus = HndwClass.lese_a_eig (_a_bas.a);
                     a_hndw.me_einh_kun = a_eig.me_einh_ek;
                     break;
             case EIG_DIV :
                     dsqlstatus = HndwClass.lese_a_eig_div (_a_bas.a);
                     a_hndw.me_einh_kun = a_eig_div.me_einh_ek;
                     break;
        }

        sprintf (ptwert, "%hd", _a_bas.me_einh);
        if (ptab_class.lese_ptab ("me_einh", ptwert) == 0)
        {
            strcpy (aufps.basis_me_bz, ptabn.ptbezk);
        }

        if (dsqlstatus) return;

        sprintf (ptwert, "%hd", a_hndw.me_einh_kun);
        if (ptab_class.lese_ptab ("me_einh", ptwert) == 0)
        {
            strcpy (aufps.me_bz, ptabn.ptbezk);
        }
}


int AUFPLIST::testme (void)
/**
Auftragsmeneg pruefen.
**/
{
        KEINHEIT keinheit;

        if (ratod (aufps.a) == (double) 0.0) return 0;

        if (testmeOK) return 0;

		if (add && aufme_old != (double) 0.0)
		{
			stnd_aufp.me = ratod (aufps.auf_me);
			sprintf (aufps.auf_me, "%.3lf", stnd_aufp.me + aufme_old);
            memcpy (&aufptab[eListe.GetAktRow()], 
                            &aufps, sizeof (struct AUFPS));
			eListe.ShowAktRow ();
		}

        if (ratod (aufps.auf_me) == (double) 0.0 && syskey == KEYCR)
        {
            einh_class.NextAufEinh (stnd_aufk.mdn, stnd_aufk.fil, stnd_aufk.kun,
                                 ratod (aufps.a), 
                                 atoi (aufps.me_einh_kun), &keinheit);
            strcpy (aufps.basis_me_bz, keinheit.me_einh_bas_bez);
            strcpy (aufps.me_bz, keinheit.me_einh_kun_bez);
            sprintf (aufps.me_einh_kun, "%hd", keinheit.me_einh_kun);
            inh = keinheit.inh;
            memcpy (&aufptab[eListe.GetAktRow()], 
                            &aufps, sizeof (struct AUFPS));
            eListe.ShowAktRow ();
            eListe.SetPos (eListe.GetAktRow (), 
                              eListe.GetAktColumn ());

  	        add = FALSE;
			DestroyPlus ();
            return -1;
        }

        if (ratod (aufps.auf_me) > MAXME)
        {
            print_mess (2, "Die Auftragsmenge ist zu gross");
            sprintf (aufps.auf_me, "%.3lf", (double) 0.0);
            memcpy (&aufptab[eListe.GetAktRow()], 
                            &aufps, sizeof (struct AUFPS));
            eListe.ShowAktRow ();
            eListe.SetPos (eListe.GetAktRow (), 
                              eListe.GetAktColumn ());
  	        add = FALSE;
			DestroyPlus ();
            return -1;
        }


        memcpy (&aufptab[eListe.GetAktRow()], 
                            &aufps, sizeof (struct AUFPS));
		aufme_old = (double) 0.0;
	    add = FALSE;
		DestroyPlus ();
        if (meoptimize)
        {
             testmeOK = TRUE;
        }
        return 0;
}

int AUFPLIST::TestPrproz_diff (void)
/**
Test, ob die Preisaenderung ueber prproz_diff % ist.
**/
{
	    double oldvk;
	    double diff;
		double diffproz;
		char buffer [256];

		if (auf_vk_pr == (double) 0.0) return 1;
		oldvk = ratod (aufps.auf_vk_pr);
		diff = oldvk - auf_vk_pr;
		if (diff < 0) diff *= -1;
        diffproz = 100 * diff / auf_vk_pr;  
		if (diffproz > prproz_diff)
		{
			sprintf (buffer, "Achtung !! Preis�nderung �ber %.2lf %c.\n"
				             "�nderung OK ?", prproz_diff, '%');
            if (abfragejn (eListe.Getmamain3 (), 
					       buffer , "N") == 0)
			{
				sprintf (aufps.auf_vk_pr, "%.2lf", auf_vk_pr);
                memcpy (&aufptab[eListe.GetAktRow()], 
                            &aufps, sizeof (struct AUFPS));
                eListe.ShowAktRow ();
                eListe.SetPos (eListe.GetAktRow (), 
                              eListe.GetAktColumn ());

			    return 0;
			}
		}
		return 1;
}


int AUFPLIST::testpr (void)
/**
Artikel holen.
**/
{
	    char buffer [256];


		if (ratod (aufps.auf_vk_pr) != auf_vk_pr)
        {
            memcpy (&aufptab[eListe.GetAktRow()], 
                            &aufps, sizeof (struct AUFPS));
        }
	    if (preistest == 1 && auf_vk_pr)
		{
			if (ratod (aufps.auf_vk_pr) != auf_vk_pr)
			{
			        disp_mess ("Achtung !! Der Preis wurde ge�ndert", 2);
			}
			return 0;
		}
		else if (preistest == 4)
		{
			if (TestPrproz_diff () == 0)
			{
               eListe.SetPos (eListe.GetAktRow (), 
                              eListe.GetAktColumn ());
               return (-1);
            }
		}
		else if (preistest == 2 && auf_vk_pr)
		{
			sprintf (aufps.auf_vk_pr, "%2lf", auf_vk_pr);
		}

        if (ratod (aufps.a) == (double) 0.0) return 0;
        if (ratod (aufps.auf_vk_pr) == (double) 0.0)
        {
            if (auf_me_pr_0)
            {
               if ((eListe.IsAppend ()) && (preis0_mess == 1))
			   {
			          sprintf (buffer, "Achtung !! Preis ist 0\n"
				                       "            OK ?");
                      if (abfragejn (eListe.Getmamain3 (), 
					       buffer , "N") == 0)
					  {
                              eListe.SetPos (eListe.GetAktRow (), 
                                             eListe.GetAktColumn ());
							  return -1;
					  }
			  }
              return 0;
            }
            else
            {

               disp_mess ("Der Preis darf nicht 0 sein", 2);
               eListe.SetPos (eListe.GetAktRow (), 
                              eListe.GetAktColumn ());
            }
            return (-1);
        }
        if (ratod (aufps.auf_vk_pr) > MAXPR)
        {
            print_mess (2, "Der Preis ist zu gross");
            sprintf (aufps.auf_vk_pr, "%.3lf", (double) 0.0);
            memcpy (&aufptab[eListe.GetAktRow()], 
                            &aufps, sizeof (struct AUFPS));
            eListe.ShowAktRow ();
            eListe.SetPos (eListe.GetAktRow (), 
                              eListe.GetAktColumn ());
            return -1;
        }
        memcpy (&aufptab[eListe.GetAktRow()], 
                            &aufps, sizeof (struct AUFPS));

        return 0;
}

void AUFPLIST::EanGew (char *eans, BOOL eangew)
/**
Gewicht aus ERAN-Nr holen oder Defaultwert.
**/
{
	   char gews [6];
	   double gew;

	   if (eangew)
	   {
		   memcpy (gews, &eans[7], 5);
		   gews [5] = (char) 0;
		   gew = ratod (gews) / 1000;
		   sprintf (aufps.auf_me, "%.3lf", gew);
	   }
	   else if (auf_me_default)
	   {
		   sprintf (aufps.auf_me, "%d", auf_me_default);
	   }
}
       

int AUFPLIST::ReadEan (double ean)
{
	   double a;
	   char eans [14];
	   int dsqlstatus;
	   char PLU [7];
	   long a_krz;
	   BOOL eangew;

       DbClass.sqlin ((double *) &ean,    3, 0);
       DbClass.sqlout ((double *)  &a, 3, 0);
       dsqlstatus = DbClass.sqlcomm ("select a from a_ean "
                                     "where ean = ?");
	   if (dsqlstatus == 0)
	   {
		   sprintf (aufps.a, "%.0lf", a);
           dsqlstatus = lese_a_bas (ratod (aufps.a));
		   return dsqlstatus;
	   }
	   sprintf (eans, "%.0lf", a);
	   if (strlen (eans) < 13) 
	   {
		   return dsqlstatus;
	   }

	   eangew = FALSE;
	   if (memcmp (eans, "20", 2) == 0);
	   else if (memcmp (eans, "21", 2) == 0);
	   else if (memcmp (eans, "22", 2) == 0);
	   else if (memcmp (eans, "23", 2) == 0);
	   else if (memcmp (eans, "24", 2) == 0);
	   else if (memcmp (eans, "27", 2) == 0)
	   {
		   eangew = TRUE;
	   }
	   else if (memcmp (eans, "28", 2) == 0)
	   {
		   eangew = TRUE;
	   }
	   else if (memcmp (eans, "29", 2) == 0)
	   {
		   eangew = TRUE;
	   }
	   else
	   {
		   return dsqlstatus;
	   }
	   memcpy (PLU, &eans[2], plu_size);    
	   PLU[plu_size] = (char) 0;
	   a_krz = atol (PLU);
       DbClass.sqlin ((long *) &a_krz, 2, 0);
       DbClass.sqlout ((double *)  &a, 3, 0);
       dsqlstatus = DbClass.sqlcomm ("select a from a_krz "
                                     "where a_krz = ?");
	   if (dsqlstatus == 0)
	   {
		   sprintf (aufps.a, "%.0lf", a);
           dsqlstatus = lese_a_bas (ratod (aufps.a));
		   EanGew (eans, eangew);
		   return dsqlstatus;
	   }
	   a = (double) a_krz;

       DbClass.sqlin ((double *) &a,    3, 0);
       DbClass.sqlout ((double *)  &a, 3, 0);
       dsqlstatus = DbClass.sqlcomm ("select a from a_bas "
                                     "where a = ?");
	   if (dsqlstatus == 0)
	   {
		   sprintf (aufps.a, "%.0lf", a);
           dsqlstatus = lese_a_bas (ratod (aufps.a));
		   EanGew (eans, eangew);
	   }
	   return dsqlstatus;
}

int AUFPLIST::Testa_kun (void)
/**
Test, ob fuer den Kunden ein Eintrag in a_kun existiert.
Wenn ja, wird gepr�ft, ob der aktuelle Artikel in a_kun existiert.
**/
{
	   int dsqlstatus;

       DbClass.sqlin ((short *) &stnd_aufk.mdn,    1, 0);
       DbClass.sqlin ((short *) &stnd_aufk.fil,    1, 0);
       DbClass.sqlin ((long *)  &stnd_aufk.kun,   2, 0);
       dsqlstatus = DbClass.sqlcomm ("select a from a_kun "
                                     "where mdn = ? "
                                     "and   fil = ? "
                                     "and   kun = ? ");
	   if (dsqlstatus == 100) return 0;
 	   
       DbClass.sqlin ((short *) &stnd_aufk.mdn,    1, 0);
       DbClass.sqlin ((short *) &stnd_aufk.fil,    1, 0);
       DbClass.sqlin ((long *)  &stnd_aufk.kun,   2, 0);
       DbClass.sqlin ((double*) &_a_bas.a, 3,0);
       DbClass.sqlout ((char *) aufps.a_kun, 0,17);
       dsqlstatus = DbClass.sqlcomm ("select a from a_kun "
                                     "where mdn = ? "
                                     "and   fil = ? "
                                     "and   kun = ? "
                                     "and  a    = ?");
	   if (dsqlstatus == 0) return 0;

	   if (NoArtMess == FALSE)
	   {
              	   print_mess (2, "Der Artikel ist nicht im Sortiment des Kunden");
	   }
	   if (a_kun_smt == 1) 
	   {
		   return 0;
	   }
       return -1;
}



int AUFPLIST::TestNewArt (double a)
/**
Test, ob der Artikel bei diesem Lieferschein existiert.
**/
{
	     int i;
		 int aufpanz;
		 int akt_pos;
		 
		 akt_pos = eListe.GetAktRow ();
         aufpanz = eListe.GetRecanz ();
	     for (i = 0; i < aufpanz; i ++)
		 {
			 if (i == akt_pos) continue;
			 if (a == ratod (aufptab[i].a)) break;
		 }
		 if (i == aufpanz) return -1;
		 return (i);
}


int AUFPLIST::fetchMatchCode (void)	    
{
	   double a;
	   int dsqlstatus;
	   char matchorg [25];
	   char matchupper[25];
	   char matchlower[25];
	   char matchuplow[25];

	   strcpy (matchorg, aufps.a);
       strupcpy (matchupper, aufps.a);
       strlowcpy (matchlower, aufps.a);
       struplowcpy (matchuplow, aufps.a);

	   DbClass.sqlout ((double *) &a, 3, 0);
	   DbClass.sqlin ((char *) matchorg, 0, 13);
	   DbClass.sqlin ((char *) matchupper, 0, 13);
	   DbClass.sqlin ((char *) matchlower, 0, 13);
	   DbClass.sqlin ((char *) matchuplow, 0, 13);
	   dsqlstatus = DbClass.sqlcomm ("select a from a_bas where a_bz3 = ? "
		                                                    "or a_bz3 = ? "
														    "or a_bz3 = ? "
														    "or a_bz3 = ?");
	   if (dsqlstatus == 100)
	   {
		   print_mess (2, "Artikel %s nicht gefunden", aufps.a);
		   sprintf (aufps.a, "%13.0lf", 0.0);
	   }
	   else
	   {
	       sprintf (aufps.a, "%13.0lf", a);
	   }
	   return 0;
}


int AUFPLIST::fetcha (void)
/**
Artikel holen.
**/
{

       char buffer [256];
	   RECT rect;
       int dsqlstatus;
       char wert [5];
       long posi;
       int art_pos;
       int i;


	   clipped (aufps.a);
	   if (syskey != KEYCR && ratod (aufps.a) == (double) 0.0)
	   {
            return 0;
	   }

	   if (!numeric (aufps.a))
	   {
		   if (matchcode == 2)
		   {
			      fetchMatchCode ();
		   }
		   else if (searchmodedirect)
		   {
                  QClass.searcha_direct (eListe.Getmamain3 (),aufps.a, stnd_aufk.kun_fil);
		   }
		   else
		   {
 		          QClass.querya_direct (eListe.Getmamain3 (),aufps.a, stnd_aufk.kun_fil);
		   }
		   if (ratod (aufps.a) == (double) 0.0)
		   {
	           sprintf (aufps.a, "%13.0lf", ratod (aufps.a));
               memcpy (&aufptab[eListe.GetAktRow()], 
				       &aufps, sizeof (struct AUFPS));
			   UpdateWindow (mamain1);
               eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
               eListe.ShowAktRow ();
			   return (-1);
		   }
	   }
	   sprintf (aufps.a, "%13.0lf", ratod (aufps.a));

	   art_pos = TestNewArt (ratod (aufps.a));
	   if (art_pos != -1 && a_kum_par == FALSE && art_un_tst)
	   {
		           if (abfragejn (eListe.Getmamain3 (), 
					        "Artikel bereits im Auftrag, OK?", "J") == 0)
				   { 
                        sprintf (aufptab[eListe.GetAktRow ()].a, "%13.0lf", Akta);
                        memcpy (&aufps, &aufptab[eListe.GetAktRow ()], sizeof (struct AUFPS));
                        eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
                        eListe.ShowAktRow ();
                        return (-1);
				   }
	   }

	   if (art_pos != -1 && a_kum_par && art_un_tst == 1)
	   {
		           if (abfragejn (eListe.Getmamain3 (), 
					        "Der Artikel ist bereits erfasst.\n\n"
							"Artikel bearbeiten ?", "J") == 0)
				   { 
                        sprintf (aufptab[eListe.GetAktRow ()].a, "%13.0lf", Akta);
                        memcpy (&aufps, &aufptab[eListe.GetAktRow ()], sizeof (struct AUFPS));
                        eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
                        eListe.ShowAktRow ();
                        return (-1);
				   }
	   }


	   if (art_pos != -1 && a_kum_par)
	   {
  		           GetClientRect (eListe.Getmamain3 (), &rect);
                   DeleteLine ();
				   if (art_pos > eListe.GetAktRow ()) art_pos --;
                   eListe.SetNewRow (art_pos);
 		           eListe.SetFeldFocus0 (art_pos, 0);
                   InvalidateRect (eListe.Getmamain3 (), &rect, TRUE);
                   UpdateWindow (eListe.Getmamain3 ());
                   memcpy (&aufps, &aufptab[art_pos], sizeof (struct AUFPS));
				   if (add_me)
				   {
				            add = TRUE;
				            aufme_old = ratod (aufps.auf_me);
				            CreatePlus ();
				   }
				   SetRowItem ();
                   return (0);
	   }

       einh_class.SetAufEinh (1);
       SetFkt (9, leer, NULL);
       set_fkt (NULL, 10);
       SetFkt (10, leer, NULL);
       posi = atol (aufps.posi);
       if (posi == 0)
       {
             i = eListe.GetAktRow ();
             if (i == 0)
             {
                 posi = 1;
             }
             else
             {
                 posi = atol (aufptab[i - 1].posi);
                 posi ++;
             }
             if (PosiEnd (posi) == 0)
             {
                 sprintf (aufps.posi, "%ld", posi);
             }
       }

       dsqlstatus = lese_a_bas (ratod (aufps.a));
	   if (dsqlstatus == 100)
	   {
		   dsqlstatus = ReadEan (ratod (aufps.a));
	   }


	   if ((stnd_aufk.kun_fil == 0 && _a_bas.ghsperre == 1) || (stnd_aufk.kun_fil == 1 && _a_bas.filialsperre == 1))  //WAL-109
	   {
		   print_mess (2, "Der  Artikel %.0lf ist gesperrt",  ratod (aufps.a));
           sprintf (aufptab[eListe.GetAktRow ()].a, "%13.0lf", Akta);
           memcpy (&aufps, &aufptab[eListe.GetAktRow ()], sizeof (struct AUFPS));
           eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
           eListe.ShowAktRow ();
           return (-1);
	   }

	   // Umvorhersehbare Ereignisse                

	   if (dsqlstatus < 0)
	   {
		   print_mess (2, "Fehler %d beim Lesen von Artikel %.0lf", dsqlstatus,
			                                                        ratod (aufps.a));
           sprintf (aufptab[eListe.GetAktRow ()].a, "%13.0lf", Akta);
           memcpy (&aufps, &aufptab[eListe.GetAktRow ()], sizeof (struct AUFPS));
           eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
           eListe.ShowAktRow ();
           return (-1);
	   }


       if (dsqlstatus == 100)
       {
           if (syskey != KEYCR &&syskey != KEYTAB 
               && eListe.IsAppend ())
           {
               return (-1);
           }
		   if (NoArtMess == FALSE)
		   {
                   print_mess (2, "Artikel %.0lf nicht gefunden",
                          ratod (aufps.a));
		   }
           sprintf (aufptab[eListe.GetAktRow ()].a, "%13.0lf", Akta);
           memcpy (&aufps, &aufptab[eListe.GetAktRow ()], sizeof (struct AUFPS));
           eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
           eListe.ShowAktRow ();
           return (-1);
       }

	   if (a_kun_smt && Testa_kun () == -1)
	   {
           sprintf (aufptab[eListe.GetAktRow ()].a, "%13.0lf", Akta);
           memcpy (&aufps, &aufptab[eListe.GetAktRow ()], sizeof (struct AUFPS));
           eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
           eListe.ShowAktRow ();
		   return -1;
	   }

       sprintf (aufps.a_bz1,       "%s",      _a_bas.a_bz1);
       sprintf (aufps.a_bz2,       "%s",      _a_bas.a_bz2);
       sprintf (wert, "%hd", _a_bas.me_einh);
       sprintf (aufps.me_einh, "%hd", _a_bas.me_einh);
       dsqlstatus = ptab_class.lese_ptab ("me_einh", wert);  
       strcpy (aufps.basis_me_bz, ptabn.ptbezk);
       sysdate (aufps.dat);

       memcpy (&aufptab[eListe.GetAktRow()], &aufps, sizeof (struct AUFPS));
       eListe.ShowAktRow ();
       if (eListe.IsNewRec ())
       {
                    ReadPr ();
                    if ((ratod (aufps.auf_vk_pr) == 0.0) && (preis0_mess == 1))
					{
			          sprintf (buffer, "Achtung !! Preis 0 gelesen\n"
				                       "            OK ?");
                      if (abfragejn (eListe.Getmamain3 (), 
					       buffer , "N") == 0)
					  {
                              sprintf (aufptab[eListe.GetAktRow ()].a, "%13.0lf", Akta);
                              memcpy (&aufps, &aufptab[eListe.GetAktRow ()], 
								      sizeof (struct AUFPS));
                              eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
                              eListe.ShowAktRow ();
		                      return -1;
					  }
                    }
                    kumebest.mdn = stnd_aufk.mdn;
                    kumebest.fil = stnd_aufk.fil;
                    kumebest.kun = stnd_aufk.kun;
                    strcpy (kumebest.kun_bran2, kun.kun_bran2);
                    kumebest.a = ratod (aufps.a);
                    ReadMeEinh ();
       }

       memcpy (&aufptab[eListe.GetAktRow()], &aufps, sizeof (struct AUFPS));
       
       eListe.ShowAktRow ();
       eListe.SetRowItem ("a", aufptab[eListe.GetAktRow ()].a);
       set_fkt (NULL, 9);
       return 0;
}


int AUFPLIST::fetchkun_bran2(void)
/**
Artikel ueber Kunden-Artikelnummer holen.
**/
{
       int dsqlstatus;
	   char kun_bran2 [3];

       DbClass.sqlin  ((short *) &stnd_aufk.mdn,   1, 0);
       DbClass.sqlin  ((short *) &stnd_aufk.fil,   1, 0);
       DbClass.sqlin  ((long *)  &stnd_aufk.kun,   2, 0);
	   DbClass.sqlout ((char *)  kun_bran2,   0, 3);
	   dsqlstatus = DbClass.sqlcomm ("select kun_bran2 from kun "
                                     "where mdn = ? "
                                     "and   fil = ? "
                                     "and   kun = ?");

	   if (dsqlstatus == 100)
	   {
 		      print_mess (2, "Kundenartikel-Nummer %.0lf nicht gefunden", 
				                                    ratod (aufps.a_kun));
              sprintf (aufptab[eListe.GetAktRow ()].a_kun, "%.0lf", (double) 0);
              memcpy (&aufps, &aufptab[eListe.GetAktRow ()], sizeof (struct AUFPS));
              eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
              eListe.ShowAktRow ();
			  return -1;
	   }

       DbClass.sqlin ((short *) &stnd_aufk.mdn,   1, 0);
       DbClass.sqlin ((short *) &stnd_aufk.fil,   1, 0);
	   DbClass.sqlin ((char *)  kun_bran2,   0, 2);
       DbClass.sqlin ((long *)  &aufps.a_kun, 0, 13);
       DbClass.sqlout ((char *)  aufps.a, 0,14);
       dsqlstatus = DbClass.sqlcomm ("select a from a_kun "
                                     "where mdn = ? "
                                     "and   fil = ? "
                                     "and   kun_bran2 = ? "
                                     "and   a_kun = ?");

       if (dsqlstatus == 100)
       {
 		      print_mess (2, "Kundenartikel-Nummer %.0lf nicht gefunden", 
				                                    ratod (aufps.a_kun));
              sprintf (aufptab[eListe.GetAktRow ()].a_kun, "%.0lf", (double) 0);
              memcpy (&aufps, &aufptab[eListe.GetAktRow ()], sizeof (struct AUFPS));
              eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
              eListe.ShowAktRow ();
			  return -1;
       }
       return fetcha ();
}


int AUFPLIST::fetcha_kun (void)
/**
Artikel ueber Kunden-Artikelnummer holen.
**/
{
       int dsqlstatus;

       DbClass.sqlin ((short *) &stnd_aufk.mdn,    1, 0);
       DbClass.sqlin ((short *) &stnd_aufk.fil,    1, 0);
       DbClass.sqlin ((long *)  &stnd_aufk.kun,   2, 0);
       DbClass.sqlin ((char *)   aufps.a_kun, 0,13);
       DbClass.sqlout ((char *)  aufps.a, 0,14);
       dsqlstatus = DbClass.sqlcomm ("select a from a_kun "
                                     "where mdn = ? "
                                     "and   fil = ? "
                                     "and   kun = ? "
                                     "and  a_kun = ?");

	   if (dsqlstatus == 100)
	   {
		       return (fetchkun_bran2 ());
	   }

       return fetcha ();
}


double AUFPLIST::GetAufMeVgl (void)
/**
Mengeneinheit fuer Kunde und Artikel holen.
**/
{
         KEINHEIT keinheit;
         double auf_me_vgl;

         einh_class.AktAufEinh (stnd_aufp.mdn, stnd_aufp.fil,
                                stnd_aufk.kun, stnd_aufp.a, atoi (aufps.me_einh));
         einh_class.GetKunEinh (stnd_aufp.mdn, stnd_aufp.fil,
                                stnd_aufk.kun, stnd_aufp.a, &keinheit);
          
         if (keinheit.me_einh_kun == keinheit.me_einh_bas)
         {
                      auf_me_vgl = stnd_aufp.me;
         }
         else
         {
                     if (keinheit.inh <= (double) 0.0)
                     {
                                   keinheit.inh = (double) 1.0;
                     }
                     auf_me_vgl = stnd_aufp.me * keinheit.inh;
         }
         return auf_me_vgl;
}

void AUFPLIST::WritePos (int pos)
/**
Position schreiben.
**/
{
       stnd_aufp.posi       = atol  (aufptab[pos].posi);
       stnd_aufp.a          = ratod (aufptab[pos].a);
       stnd_aufp.me         = ratod (aufptab[pos].auf_me);
       stnd_aufp.dat        = dasc_to_long (aufptab[pos].dat);
       stnd_aufp.me_einh     = atoi  (aufptab[pos].me_einh_kun);
       if (stnd_aufp.me == (double) 0) return;

       stnd_aufp.pr_vk      = ratod (aufptab[pos].auf_vk_pr);
       Saufp.dbupdate ();
}


int AUFPLIST::WriteAllPos (void)
/**
Alle Positionen schreiben.
**/
{
    int i;
    int recs;
    int row;

    row     = eListe.GetAktRow ();

    if (TestRow () == -1)
    {
                         
            eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                                  eListe.GetAktColumn ());
            return -1;
    }

    memcpy (&aufptab[row], &aufps, sizeof (struct AUFPS));
    recs = eListe.GetRecanz ();
    DbClass.sqlin ((short *) &stnd_aufk.mdn, 1, 0);
    DbClass.sqlin ((short *) &stnd_aufk.fil, 1, 0);
    DbClass.sqlin ((short *) &stnd_aufk.kun_fil, 1, 0);
    DbClass.sqlin ((long *)  &stnd_aufk.kun, 2, 0);
    DbClass.sqlcomm ("delete from stnd_aufp "
                     "where mdn = ? "
                     "and fil = ? "
                     "and kun_fil = ? "
                     "and kun = ?");
    GenNewPosi ();
    for (i = 0; i < recs; i ++)
    {
        WritePos (i);
    }
    eListe.BreakList ();
    return 0;
}


void AUFPLIST::SaveAuf (void)
{
    save_fkt (5);
    save_fkt (6);
    save_fkt (7);
    save_fkt (8);
    save_fkt (12);
}

void AUFPLIST::SetAuf (void)
{
    set_fkt (dokey5, 5);
    set_fkt (InsertLine, 6);
    set_fkt (DeleteLine, 7);
    set_fkt (AppendLine, 8);
    set_fkt (WriteAllPos, 12);
}

void AUFPLIST::RestoreAuf (void)
{
    restore_fkt (5);
    restore_fkt (6);
    restore_fkt (7);
    restore_fkt (8);
    restore_fkt (12);
}


int AUFPLIST::dokey5 (void)
/**
Listenerfassung abbrechen.
**/
{
	short sql_sav;
	extern short sql_mode;

    if (abfragejn (mamain1, "Positionen speichern ?", "J"))
    {
           WriteAllPos ();
           syskey = KEY12;
    }

	sql_sav = sql_mode;
	sql_mode = 1;
/*
    DbClass.sqlin ((short *) &stnd_aufk.mdn, 1, 0);
    DbClass.sqlin ((short *) &stnd_aufk.fil, 1, 0);
    DbClass.sqlin ((char *)  &stnd_aufk.kun_fil, 0, 2);
    DbClass.sqlin ((long *)  &stnd_aufk.kun, 2, 0);
    DbClass.sqlcomm ("delete from stnd_aufp where mdn = ? and fil = ? "
                     "and kun_fil = ? and kun = ?");
*/
    
	sql_mode = sql_sav; 
    syskey = KEY5;
    RestoreAuf ();

    eListe.SetListFocus (0);
    eListe.SetFeldFocus0 (eListe.GetAktRow (), eListe.GetAktColumn ());

    eListe.BreakList ();
    return 1;
}

void AUFPLIST::DoBreak (void)
{
	short sql_sav;
	extern short sql_mode;

	sql_sav = sql_mode;
	sql_mode = 1;
    
	sql_mode = sql_sav; 
    syskey = KEY5;
    RestoreAuf ();

    DbClass.sqlin ((short *) &stnd_aufk.mdn, 1, 0);
    DbClass.sqlin ((short *) &stnd_aufk.fil, 1, 0);
    DbClass.sqlin ((char *)  stnd_aufk.kun_fil, 0, 2);
    DbClass.sqlin ((long *)  &stnd_aufk.kun, 2, 0);
    DbClass.sqlcomm ("delete from stnd_aufp where mdn = ? and fil = ? "
                     "and kun_fil = ? and kun = ?");
    eListe.SetListFocus (0);
    eListe.SetFeldFocus0 (eListe.GetAktRow (), eListe.GetAktColumn ());

    eListe.BreakList ();
}



void AUFPLIST::InitSwSaetze (void)
{
       int i;

       for (i = 0; i < MAXPOS; i ++)
       {
               SwSaetze [i] = (char *) &aufptab [i];
       }
}

void AUFPLIST::SetRecHeight (void)
/**
Anzahl Zeilen pro Listzeile ermitteln.
**/
{
       int height;
       int i;

       height = 0;
       for (i = 0; i < dataform.fieldanz; i ++)
       {
           if (dataform.mask[i].attribut & REMOVED) continue;
           if (dataform.mask[i].pos[0] > height)
           {
               height = dataform.mask[i].pos[0];
           }
       }
       height ++; 
       eListe.SetRecHeight (height);
}

int AUFPLIST::ToMemory (int pos)
/**
SW-Satz in Ascii-Format Speichern.
**/
{

       memcpy (SwSaetze[pos], ausgabesatz, zlen);
       eListe.SetRecanz (pos + 1);
       return 0;
}


void AUFPLIST::SetStringEnd (char *feldname, int len)
/**
Stringende Setzen.
**/
{

       return;
}


void AUFPLIST::uebertragen (void)
/**
Eingabesatz in ASCII-Struktur uebertragen.
**/
{
	   static int cursor = -1;
        char ptwert [5];

 
	   if (cursor == -1 && (GetFieldAttr ("a_kun") & REMOVED) == 0)
	   {
                 DbClass.sqlin ((short *) &stnd_aufk.mdn,    1, 0);
                 DbClass.sqlin ((short *) &stnd_aufk.fil,    1, 0);
                 DbClass.sqlin ((long *)  &stnd_aufk.kun,   2, 0);
                 DbClass.sqlin ((char *)  aufps.a, 0,14);
                 DbClass.sqlout ((char *) aufps.a_kun, 0,13);
                 cursor = DbClass.sqlcursor ("select a_kun from a_kun "
                                     "where mdn = ? "
                                     "and   fil = ? "
                                     "and   kun = ? "
                                     "and   a = ?");
	   }
       lese_a_bas (stnd_aufp.a);

       sprintf (aufps.posi,        "%4ld",    stnd_aufp.posi);
       sprintf (aufps.a,           "%13.0lf", stnd_aufp.a);
       sprintf (aufps.a_bz1,       "%s",      _a_bas.a_bz1);
       sprintf (aufps.a_bz2,       "%s",      _a_bas.a_bz2);
       sprintf (aufps.auf_me,      "%8.3lf",  stnd_aufp.me);
       sprintf (aufps.me_bz, "%s",            stnd_aufp.me_einh_bz);
       sprintf (aufps.auf_vk_pr,  "%6.2lf",   stnd_aufp.pr_vk);
       sprintf (aufps.me_einh_kun,        "%hd",    stnd_aufp.me_einh);
       dlong_to_asc (stnd_aufp.dat, aufps.dat);

	   strcpy (aufps.a_kun, " ");
	   if (cursor != -1 && (GetFieldAttr ("a_kun") & REMOVED) == 0)
	   {
	           DbClass.sqlopen (cursor);
	           DbClass.sqlfetch (cursor);
	   }

	   if (stnd_aufp.me_einh > 0)
	   {
        sprintf (ptwert, "%hd",stnd_aufp.me_einh);
        if (ptab_class.lese_ptab ("me_einh", ptwert) == 0)
        {
            strcpy (aufps.me_bz, ptabn.ptbezk);
        }
	   }
	   else
	   {
	       ReadMeEinh ();
	   }
}

void AUFPLIST::ShowDB (short mdn, short fil, short kun_fil, long kun)
/**
Auftragspositionen lesen und anzeigen.
**/
{
        int i;
        HCURSOR oldcursor;
        extern short do_exit;
        int dsqlstatus;
        int cursor;


        Saufp.dbreadfirst ();   // Dummy zum Cursor-Prepareieren 
        InitSwSaetze ();
        stnd_aufp.mdn = mdn;
        stnd_aufp.fil = fil;
        stnd_aufp.kun_fil = kun_fil;
        stnd_aufp.kun = kun;
        eListe.SetRecanz (0);
        i = eListe.GetRecanz ();
        eListe.SetUbForm (&ubform);
        oldcursor = SetCursor (LoadCursor (NULL, IDC_WAIT));
        DbClass.sqlin ((short *) &stnd_aufp.mdn, 1, 0);
        DbClass.sqlin ((short *) &stnd_aufp.fil, 1, 0);
        DbClass.sqlin ((short *) &stnd_aufp.kun_fil, 1, 0);
        DbClass.sqlin ((long *) &stnd_aufp.kun, 2, 0);

        DbClass.sqlout ((long *) &stnd_aufp.posi, 2, 0);
        DbClass.sqlout ((double *) &stnd_aufp.a, 3, 0);
        cursor = DbClass.sqlcursor ("select posi, a from stnd_aufp "
                                    "where mdn = ? "
                                    "and   fil = ? "
                                    "and   kun_fil = ? "
                                    "and   kun = ? "
                                    "order by 1, 2");
        dsqlstatus = DbClass.sqlfetch (cursor);
        while (dsqlstatus == 0)
        {
                     dsqlstatus = Saufp.dbreadfirsta ();
                     uebertragen ();
                     if (ToMemory (i) != 0) break;
                     i = eListe.GetRecanz ();
                     if (i >= MAXPOS) break; 
                      dsqlstatus = DbClass.sqlfetch (cursor);
        }

        DbClass.sqlclose (cursor);
        SetRecHeight ();
        SetCursor (oldcursor);
        i = eListe.GetRecanz ();

        eListe.SetRecanz (i);
        SwRecs = eListe.GetRecanz ();
 
        SetFieldAttr ("a", DISPLAYONLY);
        eListe.SetDataForm0 (&dataform, &lineform);
        eListe.SetSaetze (SwSaetze);
        eListe.SetChAttr (ChAttr); 
        eListe.SetUbRows (ubrows); 
        if (i == 0)
        {
			     Posanz = 0;
                 eListe.AppendLine ();
				 AktRow = AktColumn = 0;
                 i = eListe.GetRecanz ();
        }
		else
		{
			     Posanz = i;
		}
        SendMessage (eListe.Getmamain2 (), WM_SIZE, NULL, NULL);
        InvalidateRect (eListe.Getmamain3 (), 0, FALSE);
        UpdateWindow (eListe.Getmamain3 ());
        memcpy (&aufps, &aufptab[0], sizeof (struct AUFPS));
 	    if (! numeric (aufps.a))
		{
		   sprintf (aufps.a, "%.0lf", 0.0);
           memcpy (&aufptab[eListe.GetAktRow()], &aufps, sizeof (struct AUFPS));
		}

}


void AUFPLIST::ReadDB (short mdn, short fil, short kun_fil, long kun)
/**
Auftragspositionen lesen.
**/
{
        ausgabesatz = (unsigned char *) &aufps;
        zlen = sizeof (struct AUFPS);

        PageView = 0;
        eListe.SetPageView (0);
        feld_anz = dataform.fieldanz;
        Lstzlen = zlen;
        LstSatz = ausgabesatz;

        Lstbanz = feld_anz;

        eListe.SetPos (AktRow, AktColumn);
        eListe.Setbanz (feld_anz);
        eListe.Setzlen (zlen);
        eListe.Initscrollpos ();
        eListe.SetAusgabeSatz (ausgabesatz);
}

void AUFPLIST::DestroyWindows (void)
{
       eListe.DestroyListWindow ();
       DestroyMainWindow ();
       InitMax ();
       InitMin ();
       MoveMamain1 ();
       ListAktiv = 0;
       SetFkt (11, leer, NULL);
       set_fkt (NULL, 11);
}

void AUFPLIST::SetSchirm (void)
{
       set_fkt (Schirm, 11);
       SetFkt (11, vollbild, KEY11);
}


void AUFPLIST::SetNoRecNr (void)
{
	   static BOOL SetOK = FALSE;
	   int i;

	   if (SetOK) return;

	   for (i = 0; i < dataform.fieldanz; i ++)
	   {
		   dataform.mask[i].pos[1] -= 6;
	   }
	   for (i = 0; i < ubform.fieldanz; i ++)
	   {
		   ubform.mask[i].pos[1] -= 6;
	   }
	   for (i = 0; i < lineform.fieldanz; i ++)
	   {
		   lineform.mask[i].pos[1] -= 6;
	   }
	   eListe.SetNoRecNr (TRUE);
       SetOK = TRUE;
}


void AUFPLIST::ShowAufp (void)
/**
Auftragsliste bearbeiten.
**/

{
//	   int pos;

       if (ListAktiv) return; 
/*
	   if (kun.waehrung == 0)
	   {
	             pos = GetItemPos (&ubform, "pr_vk");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (VK);
				 }
	             pos = GetItemPos (&ubform, "lad_pr");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (LD);
				 }
	   }
       else if (kun.waehrung == 1)
	   {
	             pos = GetItemPos (&ubform, "pr_vk");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (VK_DM);
				 }
	             pos = GetItemPos (&ubform, "lad_pr");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (LD_DM);
				 }
	   }
	   else if (kun.waehrung == 2)
	   {
	             pos = GetItemPos (&ubform, "pr_vk");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (VK_EURO);
				 }
	             pos = GetItemPos (&ubform, "lad_pr");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (LD_EURO);
				 }
	   }
	   else
	   {
	             pos = GetItemPos (&ubform, "pr_vk");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (VK_FREMD);
				 }
	             pos = GetItemPos (&ubform, "lad_pr");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (LD_FREMD);
				 }
	   }
*/
       Geta_bz2_par ();
       Geta_kum_par ();
	   GetCfgValues ();
	   if (FormOK == FALSE)
	   {
           if (DelPrVK)
		   {
			     DelListField ("pr_vk");
		   }
           if (RemoveBasisMe)
           {
			     DelListField ("basis_me_bz");
		   }
		   FormOK = TRUE;
	   }

   	   add = FALSE;
	   aufme_old = (double) 0.0;
	   DestroyPlus ();
       SetSchirm ();
       eListe.SetInfoProc (InfoProc);
//       sprintf (InfoCaption, "Auftrag %ld", auf);
       mamain1 = CreateMainWindow ();
       eListe.InitListWindow (mamain1);
       ReadDB (stnd_aufk.mdn, stnd_aufk.fil, stnd_aufk.kun_fil, stnd_aufk.kun);

       eListe.SetListFocus (0);
       eListe.Initscrollpos ();
       AktRow = 0;
       AktColumn = 0;
       scrollpos = 0;
       SetAktivWindow (eListe.Getmamain2 ());
       ShowDB (stnd_aufk.mdn, stnd_aufk.fil, stnd_aufk.kun_fil, stnd_aufk.kun);

       eListe.SetRowItem ("a", aufptab[0].a);
       
       ListAktiv = 1;

       return;
}

void AUFPLIST::GetListColor (COLORREF *color, char *cfg_v)
/**
Farbunterlegung fuer Listen uebertragen.
**/
{
	static char *ColTxt[] = {"BLACKCOL",
		                     "WHITECOL",
							 "BLUECOL",
							 "REDCOL",
							 "LTGRAYCOL",
							 "GREENCOL",
							 "YELLOWCOL",
							 "DKYELLOWCOL",
							 NULL};
	
	static COLORREF ColVal[] = {BLACKCOL,
		                        WHITECOL,
					  		    BLUECOL,
							    REDCOL,
							    LTGRAYCOL,
							    GREENCOL,
							    YELLOWCOL,
								DKYELLOWCOL};
	int i;
	int red, blue, green;
	char *ColR;
	int Cols = 0;

	for (i = 0; ColTxt[i]; i ++)
	{
		if (strcmp (ColTxt[i], cfg_v) == 0)
		{
			*color = ColVal[i];
			return;
		}
	}


	ColR = strstr (cfg_v, "red");
	if (ColR)
	{
		red = atoi (&ColR[3]);
		Cols ++;
	}
	ColR = strstr (cfg_v, "blue");
	if (ColR)
	{
		blue = atoi (&ColR[4]);
		Cols ++;
	}
	ColR = strstr (cfg_v, "green");
	if (ColR)
	{
		green = atoi (&ColR[5]);
		Cols ++;
	}
	if (Cols == 3)
	{
		*color = RGB (red, green, blue);
	}

}

void AUFPLIST::SetPreisTest (int mode)
{
	if (mode == 0) return;

	preistest = min (4, max (1, mode));
	if (preistest == 3)
	{
		SetItemAttr (&dataform, "pr_vk", DISPLAYONLY);
	}
}


void AUFPLIST::GetCfgValues (void)
/**
Werte aus 51100.cfg holen.
**/
{
	   static BOOL cfgOK = FALSE;
       char cfg_v [512];

	   if (cfgOK) return;

	    cfgOK = TRUE;
        if (ProgCfg.GetCfgValue ("RemoveBasisMe", cfg_v) == TRUE)
        {
			         RemoveBasisMe = atoi (cfg_v);
        }

        if (ProgCfg.GetCfgValue ("lad_vk", cfg_v) == TRUE)
        {
			         if (atoi (cfg_v))
					 {
						 DelLadVK = FALSE;
					 }

                     if (atoi (cfg_v) == 2)
                     {
   		                  SetItemAttr (&dataform, "ld_pr", EDIT);
                     }
                         
        }
        if (ProgCfg.GetCfgValue ("pr_vk", cfg_v) == TRUE)
        {
			         if (atoi (cfg_v) == 0)
					 {
						 DelPrVK = TRUE;
					 }

                     if (atoi (cfg_v) == 1)
                     {
   		                  SetItemAttr (&dataform, "pr_vk", DISPLAYONLY);
                     }
                         
       }
       if (ProgCfg.GetCfgValue ("3DList", cfg_v) == TRUE)
       {
                    eListe.Set3D (atoi (cfg_v));
	   }
       if (ProgCfg.GetCfgValue ("3DSize", cfg_v) == TRUE)
       {
                    eListe.SetPlus3D (atoi (cfg_v));
	   }
       if (ProgCfg.GetCfgValue ("ListFocus", cfg_v) == TRUE)
       {
                    ListFocus = atoi (cfg_v);
					if (ListFocus == 0) ListFocus = 3; //WAL-109
	   }
       if (ProgCfg.GetCfgValue ("plu_size", cfg_v) ==TRUE)
       {
                    plu_size = atoi (cfg_v);
       }
       else
        {
                    plu_size = 0;
        }
        if (ProgCfg.GetCfgValue ("auf_me_default", cfg_v) == TRUE)
        {
                    auf_me_default = atoi (cfg_v);
        }
        else
        {
                    auf_me_default = 0;
        }
        if (ProgCfg.GetCfgValue ("searchadirect", cfg_v) == TRUE)
		{
			        searchadirect = min (1, atoi (cfg_v));
		}
        if (ProgCfg.GetCfgValue ("searchmodedirect", cfg_v) == TRUE)
		{
			        searchmodedirect = min (1, atoi (cfg_v));
		}
        if (ProgCfg.GetCfgValue ("rowheight", cfg_v) == TRUE)
		{
			        RowHeight = ratod (cfg_v);
					if (RowHeight < (double) 1.0)
					{
						RowHeight = (double) 1.0;
					}
		}
        if (ProgCfg.GetCfgValue ("ubheight", cfg_v) == TRUE)
		{
			        UbHeight = atoi (cfg_v);
					SetUbHeight ();
		}
		eListe.SetRowHeight (RowHeight);
        if (ProgCfg.GetCfgValue ("matchcode", cfg_v) == TRUE)
		{
			         matchcode = atoi (cfg_v);
			         SetMatchCode (atoi (cfg_v));
		}
        if (ProgCfg.GetCfgValue ("auf_wert_anz", cfg_v) == TRUE)
		{
			         auf_wert_anz = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("auf_gew_anz", cfg_v) == TRUE)
		{
			         auf_gew_anz = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("a_kun_smt", cfg_v) == TRUE)
		{
			         a_kun_smt = atoi (cfg_v);
		}

        if (ProgCfg.GetCfgValue ("LISTCOLORS", cfg_v) == TRUE)
        {
		             ListColors =  min (1, max (0, atoi (cfg_v)));
        }

        if (ProgCfg.GetCfgValue ("KOMPFCOLOR", cfg_v) == TRUE)
        {
		             GetListColor (&KompfColor, cfg_v);
        }

        if (ProgCfg.GetCfgValue ("KOMPBCOLOR", cfg_v) == TRUE)
        {
		             GetListColor (&KompbColor, cfg_v);
        }
        if (ProgCfg.GetCfgValue ("SAFCOLOR", cfg_v) == TRUE)
        {
		             GetListColor (&SafColor, cfg_v);
					 MessCol = SafColor;
        }
        if (ProgCfg.GetCfgValue ("SABCOLOR", cfg_v) == TRUE)
        {
		             GetListColor (&SabColor, cfg_v);
					 MessBkCol = SafColor;
        }
        if (ProgCfg.GetCfgValue ("preistest", cfg_v) == TRUE)
        {
		             SetPreisTest (atoi (cfg_v));
        }

        if (ProgCfg.GetCfgValue ("sacreate", cfg_v) == TRUE)
        {
		             sacreate = min (1, max (0, (atoi (cfg_v))));
        }
        if (ProgCfg.GetCfgValue ("pr_vk", cfg_v) == TRUE)
        {
			         if (atoi (cfg_v) == 0)
					 {
						 DelPrVK = TRUE;
					 }

                     if (atoi (cfg_v) == 1)
                     {
   		                  SetItemAttr (&dataform, "pr_vk", DISPLAYONLY);
                     }
                         
        }
        if (ProgCfg.GetCfgValue ("proptimize", cfg_v) == TRUE)
        {
			          WaPreis.SetOptimize (atoi (cfg_v));
        }
        if (ProgCfg.GetGroupDefault ("pr_alarm", cfg_v) == TRUE)
        {
                      prproz_diff = ratod (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("art_un_tst", cfg_v) == TRUE)
        {
		             art_un_tst =  min (1, max (0, atoi (cfg_v)));
        }
        if (ProgCfg.GetCfgValue ("add_me", cfg_v) == TRUE)
        {
		             add_me =  min (1, max (0, atoi (cfg_v)));
        }
        if (ProgCfg.GetCfgValue ("a_kum", cfg_v) == TRUE)
        {
                     a_kum_par =  min (1, max (0, atoi (cfg_v)));
        }
        if (ProgCfg.GetCfgValue ("preis0_mess", cfg_v) == TRUE)
        {
                     preis0_mess = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("NoRecNr", cfg_v) == TRUE)
        {
			        NoRecNr = atoi (cfg_v);
		            if (atoi (cfg_v))
					{
	                        SetNoRecNr ();
					}
		}
        if (ProgCfg.GetCfgValue ("len_a", cfg_v) == TRUE)
        {
		            SetNewLen ("a", atoi (cfg_v));
		}
        if (ProgCfg.GetCfgValue ("picture_a", cfg_v) == TRUE)
        {
		            SetNewPicture ("a", cfg_v);
		}
        if (ProgCfg.GetCfgValue ("len_a_bz1", cfg_v) == TRUE)
        {
//WAL-109		            SetNewLen ("a_bz1", atoi (cfg_v));
		}
        if (ProgCfg.GetCfgValue ("len_me_bz", cfg_v) == TRUE)
        {
		            SetNewLen ("me_bz", atoi (cfg_v));
		}
        if (ProgCfg.GetCfgValue ("PosSave", cfg_v) == TRUE)
		{
			         PosSave = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("PosSaveMess", cfg_v) == TRUE)
		{
			         PosSaveMess = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("meoptimize", cfg_v) == TRUE)
		{
			         meoptimize = atoi (cfg_v);
		}
}


void AUFPLIST::SetNewRow (char *item, int row)
{
        int lenu;
        int len;
        int diff;
        int i;
		int datapos;
		int ipos;


        ipos = GetItemPos (&ubform, item);
		if (ipos <= 0) return;

		for (i = ipos; i < dataform.fieldanz; i ++)
		{
		    datapos = dataform.mask[i].pos[1];
			if (datapos >= ubform.mask[ipos].pos[1]) break;
		}

        diff = row - ubform.mask[ipos].pos[1]; 

		if (i == dataform.fieldanz)
		{
	        datapos = dataform.mask[i].pos[1];
            len  = dataform.mask[i].length + diff;
		}
		else
		{
	        datapos = dataform.mask[i - 1].pos[1];
            len  = dataform.mask[i- 1].length + diff;
		}

        lenu = ubform.mask[ipos - 1].length + diff;

        if (ipos < ubform.fieldanz - 1)
        {
             ubform.mask[ipos].pos[1] = row;
             lineform.mask[ipos - 1].pos[1] = row;

             for ( i = ipos + 1; i < ubform.fieldanz; i ++)
             {
                  ubform.mask[i].pos[1]   += diff;
                  lineform.mask[i - 1].pos[1] += diff;
             }
        }

		for (i = 0; i < dataform.fieldanz; i ++)
		{

			   if (dataform.mask[i].pos[1] == datapos)
			   {
                         dataform.mask[i].length = len;
			   }

			   if (dataform.mask[i].pos[1] > datapos)
			   {
                  dataform.mask[i].pos[1] += diff;
			   }
		}
        ubform.mask[ipos - 1].length = lenu;
}

void AUFPLIST::SetNewPicture (char *item, char *picture)
{
		int ipos;
		char *pic;
        ipos = GetItemPos (&dataform, item);
        if (ipos == -1) return;

		pic = new char (strlen (picture + 2));
		if (pic == NULL) return;

		dataform.mask[ipos].picture = pic;
}


void AUFPLIST::SetNewLen (char *item, int len)
{
        int lenu;
        int diff;
        int i;
		int datapos;
		int ipos;


		if (len == 0) return;
        ipos = GetItemPos (&ubform, item);
        if (ipos == -1) return;

        diff = len - ubform.mask[ipos].length + 2; 
		for (i = ipos; i < dataform.fieldanz; i ++)
		{
		    datapos = dataform.mask[i].pos[1];
			if (datapos >= ubform.mask[ipos].pos[1]) break;
		}

		if (i < dataform.fieldanz)
		{
                 datapos = dataform.mask[i].pos[1];
		}

        lenu = ubform.mask[ipos].length + diff;

        if (ipos < ubform.fieldanz - 1)
        {
             for ( i = ipos + 1; i < ubform.fieldanz; i ++)
             {
                  ubform.mask[i].pos[1]   += diff;
                  lineform.mask[i - 1].pos[1] += diff;
             }
        }

		for (i = 0; i < dataform.fieldanz; i ++)
		{

			   if (dataform.mask[i].pos[1] == datapos)
			   {
                         dataform.mask[i].length = len;
			   }

			   if (dataform.mask[i].pos[1] > datapos)
			   {
                  dataform.mask[i].pos[1] += diff;
			   }
		}
        ubform.mask[ipos].length = lenu;
}


void AUFPLIST::EnterAufp ()
/**
Auftragsliste bearbeiten.
**/

{
       static int initaufp = 0;
//	   int pos;
//	   form *savecurrent;

//	   savecurrent = current_form;

       InsCount = 0;
       strcpy (sys_par.sys_par_nam,"lutz_mdn_par");
       if (sys_par_class.dbreadfirst () == 0)
       {
                 lutz_mdn_par = atoi (sys_par.sys_par_wrt);
       }
/*
	   if (kun.waehrung == 1)
	   {
	             pos = GetItemPos (&ubform, "pr_vk");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (VK_DM);
				 }
	   }
	   else if (kun.waehrung == 2)
	   {
	             pos = GetItemPos (&ubform, "pr_vk");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (VK_EURO);
				 }
	             pos = GetItemPos (&ubform, "lad_pr");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (LD_EURO);
				 }
	   }
	   else
	   {
	             pos = GetItemPos (&ubform, "pr_vk");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (VK_FREMD);
				 }
	   }
*/

       Geta_bz2_par ();
       Geta_kum_par ();
	   GetCfgValues ();
	   if (FormOK == FALSE)
	   {
           if (DelLadVK)
		   {
			     DelListField ("ld_pr");
		   }
           if (RemoveBasisMe)
           {
			     DelListField ("basis_me_bz");
		   }
           FormOK = TRUE;
       }
   	   add = FALSE;
	   aufme_old = (double) 0.0;
	   DestroyPlus ();

	   akt_me = (double) 0.0;
	   strcpy (aufps.auf_me, "0");
	   strcpy (aufptab[0].auf_me, "0");

	   set_fkt (NULL, 8);
	   SetFkt (8, leer, NULL);

       set_fkt (dokey5, 5);
       set_fkt (AppendLine, 6);
       set_fkt (DeleteLine, 7);

       set_fkt (WriteAllPos, 12);
       set_fkt (Schirm, 11);

       SetFkt (6, einfuegen, KEY6);
       SetFkt (7, loeschen, KEY7);
       SetFkt (11, vollbild, KEY11);
       AktRow = 0;
       AktColumn = 0;
       if (initaufp == 0)
       {
                 eListe.SetInfoProc (InfoProc);
                 eListe.SetTestAppend (TestAppend);
//                 sprintf (InfoCaption, "Auftrag %ld", auf);
                 mamain1 = CreateMainWindow ();
                 eListe.InitListWindow (mamain1);
                 ReadDB (stnd_aufk.mdn, stnd_aufk.fil, stnd_aufk.kun_fil, stnd_aufk.kun);
                 initaufp = 1;
       }

       eListe.SetListFocus (ListFocus);
       eListe.Initscrollpos ();
       scrollpos = 0;
       SetAktivWindow (eListe.Getmamain2 ());
       ShowDB (stnd_aufk.mdn, stnd_aufk.fil, stnd_aufk.kun_fil, stnd_aufk.kun);

       eListe.SetRowItem ("a", aufptab[0].a);
       SetRowItem ();

       ListAktiv = 1;
       eListe.ProcessMessages ();

 	   DestroyPlus ();
       InitMax ();
       InitMin ();
       MoveMamain1 ();
       ListAktiv = 0;
       SetFkt (6, leer, NULL);
       SetFkt (7, leer, NULL);
       SetFkt (8, leer, NULL);
       SetFkt (11, leer, NULL);
       set_fkt (NULL, 11);
       eListe.DestroyListWindow ();
       DestroyMainWindow ();
       initaufp = 0;
       return;
}

void AUFPLIST::WorkAufp ()
/**
Auftragsliste bearbeiten.
**/

{

       beginwork ();

	   set_fkt (NULL, 8);
	   SetFkt (8, leer, NULL);

       set_fkt (dokey5, 5);
       set_fkt (InsertLine, 6);
       set_fkt (DeleteLine, 7);


       set_fkt (WriteAllPos, 12);
       SetFkt (6, einfuegen, KEY6);
       SetFkt (7, loeschen, KEY7);


       eListe.SetDataForm0 (&dataform, &lineform);
       eListe.SetChAttr (ChAttr); 
       eListe.SetUbRows (ubrows); 
       eListe.SetListFocus (ListFocus);
       eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                             eListe.GetAktColumn ());
       AktRow = 0;
       AktColumn = 0;
       scrollpos = 0;

       SetAktivWindow (eListe.Getmamain2 ());

       ListAktiv = 1;
       eListe.ProcessMessages ();

       commitwork ();
       if (syskey == KEY5)
       {
                  eListe.Initscrollpos ();
                  ShowDB (stnd_aufk.mdn, stnd_aufk.fil, stnd_aufk.kun_fil, stnd_aufk.kun);
       }
       ListAktiv = 0;
       SetFkt (6, leer, NULL);
       SetFkt (7, leer, NULL);
       SetFkt (8, leer, NULL);
       SetFkt (11, leer, NULL);
       set_fkt (NULL, 11);
       return;
}


void AUFPLIST::DestroyMainWindow (void)
/**
Hauptfenster fuer Liste loeschen.
**/
{
        if (mamain1 == NULL) return;
        DestroyWindow (mamain1);
        mamain1 = NULL;
}


HWND AUFPLIST::CreateMainWindow (void)
/**
Hauptfenster fuer Liste erzeugen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;

        if (hMainWindow == NULL) return NULL;    
           
        if (mamain1) return mamain1;

        eListe.GetTextMetric (&tm);

        GetClientRect (hMainWindow, &rect);
        GetWindowRect (hMainWindow, &wrect);


		if (liney > 0)
		{
			y = liney;
		}
		else
		{
            y = (wrect.bottom - 12 * tm.tmHeight);
		}
        x = wrect.left + 2;
        cy = wrect.bottom - y - 2;
        cx = rect.right;

        mamain1 = CreateWindow ("ListMain",
                                    InfoCaption,
                                    WS_THICKFRAME | 
//                                    WS_CAPTION | 
//                                    WS_CHILD |
                                    WS_POPUP |
                                    WS_SYSMENU |
                                    WS_MINIMIZEBOX |
                                    WS_MAXIMIZEBOX,
                                    x, y,
                                    cx, cy,
                                    hMainWindow,
                                    NULL,
                                    hMainInst,
                                    NULL);
        ShowWindow (mamain1, SW_SHOWNORMAL);
        UpdateWindow (mamain1);
        return mamain1;
}

static int IsMax = 0;
static int IsMin = 0;

void AUFPLIST::SetMax0 (int val)
{
    IsMax = val;
    if (IsMax)
    {
             SetFkt (11, fenster, KEY11);
    }
    else
    {
             SetFkt (11, vollbild, KEY11);
    }
}

void AUFPLIST::SetMin0 (int val)
{
    IsMin = val;
}


int AUFPLIST::Schirm (void)
{
         if (IsMin) return 0;
         
         if (IsMax)
         {
             SendMessage (mamain1, WM_SYSCOMMAND, SC_RESTORE, 0l);
             IsMax = 0;
         }
         else
         {
             SendMessage (mamain1, WM_SYSCOMMAND, SC_MAXIMIZE, 0l);
             IsMax = 1;
         }
         return 0;
}


void AUFPLIST::MoveMamain1 ()
{
        RECT rect;
        RECT wrect;
        int x, y, cx, cy;
        TEXTMETRIC tm;

        if (mamainmin) return;
        eListe.GetTextMetric (&tm);
        GetClientRect (hMainWindow, &rect);
        GetWindowRect (hMainWindow, &wrect);
        if (mamainmax)
        {
                 x = wrect.left + 2;
                 y = wrect.top + 2;
                 cy = wrect.bottom - y - 2;
                 cx = rect.right;
        }
        else
        {
 		         if (liney)
				 {
			            y = liney;
				 }
				 else
				 {
                        y = (wrect.bottom - 12 * tm.tmHeight);
				 }
                 x = wrect.left + 2;
                 cy = wrect.bottom - y - 2;
                 cx = rect.right;
        }
        MoveWindow (mamain1, x,y, cx, cy, TRUE);
}


void AUFPLIST::MaximizeMamain1 ()
{
        RECT rect;
        RECT wrect;
        int x, y, cx, cy;

        GetClientRect (hMainWindow, &rect);
        GetWindowRect (hMainWindow, &wrect);
        x = wrect.left + 2;
        y = wrect.top + 2;
        cy = wrect.bottom - y - 2;
        cx = rect.right;
        MoveWindow (mamain1, x,y, cx, cy, TRUE);
}



static char aufwert [20];
static ITEM iaufwert  ("auf_wert",  aufwert,   "Auftragswert   ", 0);

static field _faufwert [] = {
&iaufwert,        14,  0, 1, 1, 0, "%10.2f", READONLY, 0, 0, 0,
};

static form faufwert = {1, 0, 0, _faufwert, 0, 0, 0, 0, NULL};    

static char aufgew [20];
static ITEM iaufgrw  ("auf_gew",    aufgew,    "Auftragsgew    ", 0);

static field _faufgew [] = {
&iaufgrw,        14,  0, 1, 1, 0, "%11.3f", READONLY, 0, 0, 0,
};

static form faufgew = {1, 0, 0, _faufgew, 0, 0, 0, 0, NULL};    


void AUFPLIST::MoveAufw (void)
/**
Hauptfenster fuer Liste erzeugen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;

        if (hMainWin == NULL) return;    
        if (AufMehWnd == NULL) return;    
           
	    memcpy (&tm, &textm, sizeof (tm)); 

        GetClientRect (hMainWin, &rect);
        GetWindowRect (hMainWin, &wrect);

        y = (wrect.top +  2);
        cx = 34 * tm.tmAveCharWidth + 4;
        x = wrect.left + rect.right - cx + 1;
        cy = 4 * tm.tmHeight + 8;
		MoveWindow (AufMehWnd, x, y, cx, cy, TRUE);
}

void AUFPLIST::MoveAufGew (void)
/**
Hauptfenster fuer Liste erzeugen.
**/
{
        RECT rect;
        RECT wrect;
        RECT merect;
        int x,y,cx, cy;
        TEXTMETRIC tm;

        if (hMainWin == NULL) return;    
        if (AufGewhWnd == NULL) return;    
           
	    memcpy (&tm, &textm, sizeof (tm)); 

        GetClientRect (hMainWin, &rect);
        GetWindowRect (hMainWin, &wrect);


        y = (wrect.top +  2);
        if (AufMehWnd)
        {
             GetWindowRect (AufMehWnd, &merect);
             y = merect.bottom;
        }

        cx = 34 * tm.tmAveCharWidth + 4;
        x = wrect.left + rect.right - cx + 1;
        cy = 4 * tm.tmHeight + 8;
		MoveWindow (AufGewhWnd, x, y, cx, cy, TRUE);
}



void AUFPLIST::EnterPrVk (void)
/**
Laden-VK editieren.
**/
{
       char vk_pr [11]; 
	   EnterF Enter;

       if (ratod (aufps.a) == (double) 0.0)
       {
           return;
       }
	   sprintf (vk_pr, "%s", aufps.auf_vk_pr);
       Enter.SetColor (LTGRAYCOL);
	   Enter.EnterText (hMainWindow, "   VK-Preis ", vk_pr, 10, "%8.2lf");

	   if (syskey == KEY5)
	   {
                eListe.ShowAktRow ();
	            return;
	   }

       sprintf (aufps.auf_vk_pr, "%6.2lf", ratod (vk_pr));
       memcpy (&aufptab[eListe.GetAktRow()], &aufps, sizeof (struct AUFPS));
       eListe.ShowAktRow ();
}

    

HWND AUFPLIST::GetMamain1 (void)
{
       return (mamain1);
}

void AUFPLIST::SethwndTB (HWND hwndTB)
{
         this->hwndTB = hwndTB; 
         eListe.SethwndTB (hwndTB);
}

void AUFPLIST::SetTextMetric (TEXTMETRIC *tm)
{
         memcpy (&textm, tm, sizeof (TEXTMETRIC));
         eListe.SetTextMetric (tm);
}


void AUFPLIST::SetLineRow (int LineRow)
{
         eListe.SetLineRow (0);
}

void AUFPLIST::SetListLines (int i)
{ 
         eListe.SetListLines (i);
}

void AUFPLIST::OnPaint (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	    HDC hdc;

        eListe.OnPaint (hWnd, msg, wParam, lParam);
        if (hWnd == eWindow)  
        {
                    hdc = BeginPaint (eWindow, &aktpaint);
                    EndPaint (eWindow, &aktpaint);
        }
        else if (hWnd == AufMehWnd)  
        {
                    hdc = BeginPaint (AufMehWnd, &aktpaint);
                    EndPaint (AufMehWnd, &aktpaint);
        }
        else if (hWnd == BasishWnd)  
        {
                    hdc = BeginPaint (BasishWnd, &aktpaint);
                    EndPaint (BasishWnd, &aktpaint);
        }
        else if (hWnd == PlusWindow)  
        {
                    hdc = BeginPaint (hWnd, &aktpaint);
                    PaintPlus (hdc); 
                    EndPaint (hWnd, &aktpaint);
        }
}


void AUFPLIST::MoveListWindow (void)
{
        eListe.MoveListWindow ();
}


void AUFPLIST::BreakList (void)
{
        eListe.BreakList ();
}


void AUFPLIST::OnHScroll (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
        eListe.OnHScroll (hWnd, msg,wParam, lParam);
}

void AUFPLIST::OnVScroll (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
        eListe.OnVScroll (hWnd, msg,wParam, lParam);
}

void AUFPLIST::OnSize (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
        if (hWnd == GetMamain1 ())
        {
                    MoveListWindow ();
                    MovePlus ();
        }
}


void AUFPLIST::FunkKeys (WPARAM wParam, LPARAM lParam)
{
        eListe.FunkKeys (wParam, lParam);
}


int AUFPLIST::GetRecanz (void)
{
          return eListe.GetRecanz ();
}


void AUFPLIST::SwitchPage0 (int rows)
{
           eListe.SwitchPage0 (rows);
}


HWND AUFPLIST::Getmamain2 (void)
{
           return eListe.Getmamain2 ();
}


HWND AUFPLIST::Getmamain3 (void)
{
           return eListe.Getmamain3 ();
}


void AUFPLIST::ChoiseFont (mfont *lfont)
{
           eListe.ChoiseFont (lfont);
}

void AUFPLIST::SetFont (mfont *lfont)
{
           eListe.SetFont (lfont);
}

void AUFPLIST::SetListFont (mfont *lfont)
{
           eListe.SetListFont (lfont);
}

void AUFPLIST::FindString (void)
{
           eListe.FindString ();
}


void AUFPLIST::SetLines (int Lines)
{
           eListe.SetLines (Lines);
}


int AUFPLIST::GetAktRow (void)
{
                 return eListe.GetAktRow ();
}

int AUFPLIST::GetAktRowS (void)
{
                 return eListe.GetAktRowS ();
}

void AUFPLIST::SetColors (COLORREF Color, COLORREF BkColor)
{
                 eListe.SetColors (Color, BkColor); 
}

void AUFPLIST::SetListFocus (void)
{
                 eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                                       eListe.GetAktColumn ()); 
}

void AUFPLIST::PaintUb (void)
{
                 eListe.PaintUb (); 
}
