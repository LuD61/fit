#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include "comcthlp.h"
#include "itemc.h"
#include "wmaskc.h"
#include "dbclass.h"
#include "mo_menu.h"
#include "kun.h"
#include "sys_par.h"
#include "mdn.h"
#include "fil.h"
#include "mdnfil.h"
#include "mo_meld.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "listcl.h"
#include "inflib.h"
#include "mo_arg.h"
#include "mo_curso.h"
#include "mo_qa.h"
#include "ls.h"
#include "a_bas.h"
#include "ptab.h"
#include "mo_intp.h"
#include "mo_aufl.h"
#include "bmlib.h"
#include "message.h"
#include "mo_progcfg.h"
#include "mo_inf.h"
#include "mo_ch.h"
#include "mo_chq.h"
#include "mo_chqex.h"
#include "datum.h"
#include "mo_vers.h"
#include "aktion.h"
#include "akt_krz.h"
#include "a_pr.h"
#include "mo_a_pr.h"
#include "mo_preis.h"
#include "stnd_auf.h"
#include "searchptab.h"

#define MAXLEN 40
#define MAXPOS 3000
#define LPLUS 1
 
#define IDM_FRAME     901
#define IDM_REVERSE   902
#define IDM_NOMARK    903
#define IDM_EDITMARK  904
#define IDM_PAGEVIEW  905
#define IDM_LIST      906
#define IDM_PAGE      907
#define IDM_FONT      908
#define IDM_FIND      909
#define IDM_ANZBEST   910
#define IDM_STD       911
#define IDM_BASIS     912
#define IDM_LGR       913
#define IDM_LGRDEF    914
#define IDM_POSTEXT   915
//#define IDM_VINFO     916
#define IDM_LDPR      917
#define IDM_VKPR      918

#define QUERYAUF      920 
#define QUERYKUN      921 
#define QUERYAUFKZ    922 


static BOOL NewStyle = FALSE;

static char *Programm = "51300";

int      PASCAL WinMain(HANDLE,HANDLE,LPSTR,int);
LONG FAR PASCAL WndProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL StaticWProc(HWND,UINT,WPARAM,LPARAM);
static int ProcessMessages(void);
void     InitFirstInstance(HANDLE);
BOOL     InitNewInstance(HANDLE, int);
void     DisplayLines ();
void     MoveButtons (void);
void     PrintButtons (void);
void     ShowDaten (void);
static   int testkun0 (void);
static   void EnableArrows (form *, BOOL);
static int EnterListe (void);

static int dokey5 (void);
static int dokey12 (void);
static int doliste (void);


static BOOL NoClose = FALSE;

extern BOOL ColBorder;

static int  Startsize = 0;
static BOOL auf_art_active = FALSE;
static int zustell_par = 0;
static BOOL akt_preis = TRUE;
static BOOL  lsdirect = FALSE;

HANDLE  hMainInst;
HWND   hMainWindow;
HWND   mamain1;

HWND ForegroundWindow;

void GetForeground (void)
{
	ForegroundWindow = GetForegroundWindow ();
}

void SetForeground (void)
{
	SetForegroundWindow (ForegroundWindow);
}

extern HWND  ftasten;
extern HWND  ftasten2;

static int WithMenue   = 1;
static int WithToolBar = 1;

static   TEXTMETRIC tm;

static int PageView = 0;

static char *TXT_WORK   = "Bearbeiten";
static char *TXT_SHOW   = "Anzeigen";
static char *TXT_DEL    = "L�schen";
static char *TXT_PRINT  = "Drucken";

static char *TXT_ACTIVE = TXT_WORK;


static int IDM_ACTIVE = IDM_WORK;

static PAINTSTRUCT aktpaint;

HMENU hMenu;

// static int CallT1 =  850;
// static int CallT2 =  851;
// static int CallT3 =  852;

#define CallT1  850
#define CallT2  851
#define CallT3  852

struct PMENUE dateimen[] = {
	                        "&1 Bearbeiten", "G",  NULL, IDM_WORK, 
	                        "&2 L�schen",    "G",  NULL, IDM_DEL, 
						    "&3 Drucken",    " ",  NULL, IDM_PRINT,
							"",              "S",  NULL, 0, 
	                        "B&eenden",      " ",  NULL, IDM_EXIT,

                             NULL, NULL, NULL, 0};
struct PMENUE bearbeiten[15] = {
	                        "&Fax-Nummer",                 "G", NULL, CallT1, 
	                        "&Telefon-Nummer",             "G", NULL, CallT2, 
	                        "&Preisgruppen",               "G", NULL, CallT3, 
							"",                            "S",   NULL, 0, 
	                        "A&bbruch F5",                 " ", NULL, KEY5, 
							"Spe&ichern F12",              " ", NULL, KEY12,
							"",                            "S",   NULL, 0, 
	                        "&Suchen",                     "G", NULL, IDM_FIND, 
                             NULL, NULL, NULL, 0};

struct PMENUE changeldpr = {"&Laden-Preis editieren  ALT L","G", NULL, IDM_LDPR}; 
struct PMENUE changevkpr = {"&Verkaufs-Preis editieren  ALT V","G", NULL, IDM_VKPR}; 


struct PMENUE ansicht[] = {
                            "&Basismenge",                 "G", NULL, IDM_BASIS, 
                            "&Fonteinstellung",            "G", NULL, IDM_FONT,
                             NULL, NULL, NULL, 0};

struct PMENUE menuetab[] = {"&Datei",      "M", dateimen,   0, 
                            "&Bearbeiten", "M", bearbeiten, 0, 
                            "&Ansicht",    "M", ansicht,    0, 
                            "&?",          " ", NULL,       IDM_VINFO, 
						     NULL, NULL, NULL, 0};

static int ActiveMark = IDM_FRAME;

static char InfoCaption [80] = {"\0"};


extern HWND hWndToolTip;
HWND hwndTB;
HWND hwndCombo1;
HWND hwndCombo2;

static TBBUTTON tbb[] =
{
 0,               IDM_WORK,   TBSTATE_INDETERMINATE, 
                               TBSTYLE_BUTTON, 
 0, 0, 0, 0,
/*
 1,               IDM_SHOW,   TBSTATE_ENABLED, 
	                          TBSTYLE_BUTTON,
 0, 0, 0, 0,
*/
 2,               IDM_DEL,   TBSTATE_INDETERMINATE, 
                             TBSTYLE_BUTTON,
 0, 0, 0, 0,
 3,               IDM_PRINT,  TBSTATE_INDETERMINATE, 
                              TBSTYLE_BUTTON,
 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED,
                  TBSTYLE_SEP, 
 0, 0, 0, 0,
 /*
 4,               IDM_LIST,   TBSTATE_ENABLED | TBSTATE_CHECKED, 
                  TBSTYLE_CHECKGROUP,
 0, 0, 0, 0,
 */

/*
 5,               IDM_STD,TBSTATE_INDETERMINATE, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,

 5,               IDM_PAGE, TBSTATE_INDETERMINATE, 
                  TBSTYLE_CHECKGROUP,
 0, 0, 0, 0,
 */
 6,               IDM_INFO,   TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
 
 7,               KEYTAB, TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
 8,               KEYSTAB, TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
 9,               KEYDOWN, TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
10 ,               KEYUP, TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,

 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,

 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
};

static char *Version[] = {"  Standardauftr�ge  ",
                          "  Programm    51300", 
                          "  Standard",
						  "  Versions-Nr    1.0",
						   NULL,
};

HWND    QuikInfo = NULL;

void     CreateQuikInfos (void);
static int CallTele1 ();
static int CallTele2 ();
static int CallTele3 ();


static int MoveMain = FALSE;
static int kun_sperr = 1;
static int fen_par = 0;
static int lutz_mdn_par = 0;
static BOOL fil_lief_par = FALSE;
static int KomplettMode = 0;
static HWND DlgWindow = NULL;
static BOOL autokunfil = FALSE;
static int listedirect = 0;
static BOOL PosOK = FALSE;
static BOOL LdVkEnabled = FALSE;
static BOOL PrVkEnabled = FALSE;

HICON telefon1;
HICON fax;

HBITMAP btelefon;
HBITMAP btelefoni;

ColButton CTelefon = { 
//                      "Telefon", -1, -1,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       NULL, 0, 0,
//                       btelefon, 6, 8,
                       btelefon, -1, -1,
                       NULL, 0, 0,
                       RGB (255, 255, 255),
//                       BLUECOL,
                       GRAYCOL,
                       -1};

ColButton CFax =    { "Fax", -1, -1,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       RGB (255, 255, 255),
                       GRAYCOL,
                       -1};
ColButton CGruppen =    { "PG", -1, -1,
                          NULL, 0, 0,
                          NULL, 0, 0,
                          NULL, 0, 0,
                          NULL, 0, 0,
                          RGB (255, 255, 255),
                          GRAYCOL,
                          -1};



static mfont BuFont = {
                      "Courier New", 
                       120, 
                       100, 
                       0, 
                       RGB (-1, 0, 0), 
                       RGB (-1, 0, 0),
                       0, 0};

static BOOL temode = 0;
static ITEM itelefon1 ("", (char *) &CTelefon, "", 0);
static ITEM ifax      ("", (char *) &CFax,     "", 0);
static ITEM iGruppen  ("", (char *) &CGruppen, "", 0);

static ITEM iOK      ("", "    OK     ", "", 0);
static ITEM iCancel  ("", " Abbrechen ", "", 0);

static field _buform [] = {
&ifax,             4, 2, 2,73, 0, "", COLBUTTON, 0, 0,
                                                           CallT1,
&itelefon1,        4, 2, 4,73, 0, "", COLBUTTON, 0, 0,
                                                           CallT2,
&iGruppen,         4, 2, 6,73, 0, "", COLBUTTON, 0, 0,
                                                           CallT3,
};

static form buform = {3, 0, 0, _buform, 0, 0, 0, 0, &BuFont};



static char *qInfo [] = {"bearbeiten",
                         "l�schen",
                         "Drucken",
                         "Funktion f�r aktives Eingabefeld",
                         "S�tze in Listenform anzeigen",
                         "Standardauftrag",
                         "Lager f�r Standort �ndern",
						 "Default-Lager f�r Standort setzen",
                          0, 
                          0};

static UINT qIdfrom [] = {IDM_WORK, IDM_DEL, 
                         IDM_PRINT, IDM_INFO,
                         IDM_LIST, IDM_STD,IDM_LGR, IDM_LGRDEF, 0, 0};

static char *telenr =  "Telefon-Nummer";
static char *telelst = "Telefon-Liste";
static int telepos = 3;
static BOOL telekun = FALSE;

static char *qhWndInfo [] = {"Tabellen-Linien festlegen", 
                             "Farbe f�r Vordergrund und Hintergrund "
                             "festlegen",
                             "Fax-Nummer",
                             telenr,
                             "Preisgruppen",
                             "Abbrechen",
                             "Speichern",
                             0};

static HWND *qhWndFrom [] = {&hwndCombo1, 
                             &hwndCombo2,
                             &buform.mask[0].feldid,
                             &buform.mask[1].feldid,
                             &buform.mask[2].feldid,
                             NULL,
                             NULL,
                             NULL}; 

void SetTeleNr (void)
{
	      qhWndInfo[telepos] = telenr;
}

void SetTeleLst (void)
{
	      qhWndInfo[telepos] = telelst;
}


static char *Combo1 [] = {"wei�e Tabelle" ,
                          "hellgraue Tabelle",
                          "schwarze Tabelle",
                          "graue Tabelle",
                          "vertikal wei�",
                          "vertikal hellgrau",
                          "vertikal schwarz", 
                          "vertikal grau",
                          "horizontal wei�",
                          "horizontal hellgrau",
                          "horizontal schwarz", 
                          "horizontal grau",
                          "keine Linien",
                           NULL};

static char *Combo2 [] = {"wei�er Hintergrund" ,
                          "blauer Hintergrund",
                          "schwarzer Hintergrund",
                          "grauer Hintergrund",
						  "hellgrauer Hintergrund",
                           NULL};

static COLORREF Colors[] = {BLACKCOL,
                            WHITECOL,
                            WHITECOL,
                            WHITECOL,
							BLACKCOL,
};

static COLORREF BkColors[] = {WHITECOL,
                            BLUECOL,
                            BLACKCOL,
                            GRAYCOL,
							LTGRAYCOL,
};

static COLORREF MessBkCol = DKYELLOWCOL; 
static COLORREF MessCol   = BLACKCOL; 

static int combo1pos = 0;
static int combo2pos = 0;

mfont lFont = {
               "Courier New", 
               120, 
               100, 
               0, 
               RGB (-1, 0, 0), 
               RGB (-1, 0, 0),
               0, 0};

static int m3Size = 0;

extern LS_CLASS ls_class;
static WA_PREISE WaPreis;
static KUN_CLASS kun_class;
static AUFPLIST aufpListe;
static MENUE_CLASS menue_class;
static FIL_CLASS fil_class;
static MDN_CLASS mdn_class;
static ADR_CLASS adr_class;
static QueryClass QClass;
static DB_CLASS DbClass;
static PROG_CFG ProgCfg ("51300");
static PTAB_CLASS ptab_class;
static KINFO Kinfo;
static VINFO Vinfo;
SAUFK_CLASS Saufk;
SAUFP_CLASS Saufp;

extern MELDUNG Mess;
extern SYS_PAR_CLASS sys_par_class;



static char mdn [10];
static char fil [10];

/* ****************************************************************/
/* Auftragskopf    
                                              */
static int mench = 0;

static long akt_auf = 0l;
static long akt_tou = 0l;
static long akt_kun = 0l;
static long akt_kun_fil = 0;
static long adr_nr = 0;

char kunfil[2];
char kuns[9];
char k_kurzb[17];
char tele [20];
char stnd_auf_kz [3];
char stnd_auf_kz_bez [37];

static int dsqlstatus;
static int inDaten = 0;
static HBITMAP ArrDown;


static int TestKz (void);
static int TestKun (void);
static int lesekun0 (void);
static int testkunfil (void);
static int leseauf (void);
static int lesekun (void);
static int ReadPtab (void);
static int setkey9kun (void);
static int setkey9ptab (void);


static ITEM ikunfil ("kun_fil",    kunfil,"Kunde/Filiale ", 0); 
static ITEM ikunfil_bz 
                    ("kun_fil_bz","Kunde", "", 0); 
static ITEM iarrdown ("arrdown",  " < ",     "", 0);

static field _aufformk [] = {
&ikunfil,                  2, 0, 3, 1, 0, "%1d", READONLY,   0, 0,0,
&ikunfil_bz,              10, 0, 3,19, 0, "",    DISPLAYONLY,0, 0,0,
};

static form aufformk = {2, 0, 0, _aufformk, 0, 0, 0, 0, NULL};

static ITEM ikuns   ("kun",        kuns,  "Kundennummer  ", 0); 
static ITEM ik_kurzb("kun_krz",    k_kurzb,    "", 0);
static ITEM itele   ("tele",       _adr.tel,   "TelNr ", 0);
static ITEM istnd_auf_kz  
                    ("stnd_auf_kz",  stnd_auf_kz,   
                                          "Kennung       ", 0);
static ITEM istnd_auf_kz_bez  
                    ("stnd_auf_kz_bez",  stnd_auf_kz_bez, "", 0);

static field _aufform [] = {
&ikunfil,                    2, 0, 2,1, 0, "%1d", NORMAL,   0, testkunfil,0,
&ikunfil_bz,                10, 0, 2,19, 0, "",    DISPLAYONLY,0, 0,0,
&ikuns,                      9, 0, 3,1, 0, "", NORMAL,   setkey9kun, 
                                                            lesekun, 0,

&iarrdown,                   2, 0, 3,25, 0, "B", BUTTON,      0, 0, QUERYKUN, 
&ik_kurzb,                  16, 0, 3,27, 0, "",  READONLY, 0, 0,0,
&itele,                     16, 0, 3,45, 0, "",  READONLY, 0, 0,0,
&istnd_auf_kz,               2, 0, 4, 1, 0, "",  UPSHIFT,   setkey9ptab, 
                                                              ReadPtab, 0,
&iarrdown,                   2, 0, 4,18, 0, "B", BUTTON,   0, 0, QUERYAUFKZ, 
&istnd_auf_kz_bez,          17, 0, 4,20, 0, "",  READONLY, 0, 0, 0, 
};

static form aufform = {9, 0, 0, _aufform, 0, 0, 0, 0, NULL};


static int kunfield = 1;

static char *aktart = kunart;
static char *aktzei = a_bz2_on;
static int NewRec = 0;

int CallTele1 (void)
/**
Waehlen ausfuehren.
**/
{
        clipped (_adr.fax);
        if (strcmp (_adr.fax, " ") <= 0)
        {
                  disp_mess ("Keine Fax-Nummer", 0);
        }
        else
        {
                  print_mess (0, "Fax Nummer %s", _adr.fax);
        }
        DestroyWindow (QuikInfo);
        CreateQuikInfos ();
        return 0;
}

int CallTele2 (void)
/**
Waehlen ausfuehren.
**/
{
        return 0;
}

int CallTele3 (void)
/**
Waehlen ausfuehren.
**/
{
	    Kinfo.InfoKunF (hMainInst, hMainWindow, stnd_aufk.mdn, atol (kuns), kun.kun_bran2);
        CreateQuikInfos ();
        return 0;
}

int InfoVersion (void)
/**
Waehlen ausfuehren.
**/
{
	    Vinfo.VInfoF (hMainInst, hMainWindow, Version);
        return 0;
}

void CreateQuikInfos (void)
/**
QuikInfos generieren.
**/
{
     BOOL bSuccess ;
     TOOLINFO ti ;

     if (QuikInfo) DestroyWindow (QuikInfo);
     QuikInfo = CreateWindow (TOOLTIPS_CLASS,
                                "",
                                WS_POPUP,
                                0, 0,
                                0, 0,
                                hMainWindow,
                                NULL,
                                hMainInst,
                                NULL);


     ZeroMemory (&ti, sizeof (TOOLINFO)) ;
     ti.cbSize = sizeof (TOOLINFO) ;
     ti.uFlags = TTF_IDISHWND | TTF_CENTERTIP | TTF_SUBCLASS ;
     ti.hwnd   = mamain1 ;
     ti.uId    = (UINT) (HWND) buform.mask[0].feldid;
     ti.lpszText = LPSTR_TEXTCALLBACK ;
     bSuccess = ToolTip_AddTool (QuikInfo, &ti) ;
     ti.uId    = (UINT) (HWND) buform.mask[1].feldid;
     ti.lpszText = LPSTR_TEXTCALLBACK ;
     bSuccess = ToolTip_AddTool (QuikInfo, &ti) ;
     ti.uId    = (UINT) (HWND) buform.mask[2].feldid;
     ti.lpszText = LPSTR_TEXTCALLBACK ;
     bSuccess = ToolTip_AddTool (QuikInfo, &ti) ;
     SetFktQuikInfo (QuikInfo, &ti);
}


int InfoKun (void)
{
        char where [80];

        if (atoi (kunfil) == 0)
        {
              if (atol (kuns))
              {
                        sprintf (where, "where kun = %ld", atol (kuns));
              }
              else
              {
                        sprintf (where, "where kun > 0");
              }
              _CallInfoEx (hMainWindow, NULL,
							      "kun", 
								  where, 0l);
        }
        else
        {
              if (atol (kuns))
              {
                        sprintf (where, "where fil = %ld", atol (kuns));
              }
              else
              {
                        sprintf (where, "where fil > 0");
              }
              _CallInfoEx (hMainWindow, NULL,
							      "fil", 
								  where, 0l);
        }
        return 0;
}



static void SetKunAttr (void)
/**
Attribut auf Feld Kunde setzen.
**/
{
	   int kunarrowpos;

	   if (kun_sperr == FALSE) return;

	   kunarrowpos = GetItemPos (&aufform, "kun") + 1;
	   if (aufpListe.GetPosanz () == 0)
	   {
	            SetItemAttr (&aufform, "kun", EDIT);
				aufform.mask[kunarrowpos].attribut = BUTTON;
	   }
	   else
	   {
	            SetItemAttr (&aufform, "kun", READONLY);
				aufform.mask[kunarrowpos].attribut = REMOVED;
	   }
}

	       
static void AufToForm (void)
/**
Auftragsdaten in Form uebertragen.
**/
{
          char wert [5];

          clipped (_adr.tel);

          if (strcmp (_adr.tel, " ") > 0)
          {
                   CTelefon.bmp = btelefon;
                   ActivateColButton (mamain1, 
                                      &buform, 1, 0, 1);
                   EnableMenuItem (hMenu, CallT2,   MF_ENABLED);
          }

          clipped (_adr.fax);
          if (strcmp (_adr.fax, " ") > 0)
          {
                   ActivateColButton (mamain1, 
                                      &buform,      0, 0, 1);
                   EnableMenuItem (hMenu, CallT1,   MF_ENABLED);
          }
          ActivateColButton (mamain1, &buform, 2, 0, 1);
          EnableMenuItem (hMenu, CallT3,   MF_ENABLED);
          MoveButtons ();


          strcpy (stnd_auf_kz, stnd_aufk.stnd_auf_kz);
          sprintf (wert, stnd_auf_kz);
          ptabn.ptbezk[0] = 0;
          ptab_class.lese_ptab ("stnd_auf_kz", wert);
          strcpy (stnd_auf_kz_bez, ptabn.ptbezk);
}


static void FormToAuf (short auf_stat)
/**
Form in Auftragsdaten uebertragen.
**/
{
          stnd_aufk.mdn     = atoi (mdn);
          stnd_aufk.fil     = atoi (fil);
          stnd_aufk.kun_fil = atoi (kunfil);
          stnd_aufk.kun = atol (kuns);
          strcpy (stnd_aufk.kun_krz1,k_kurzb);
          strcpy (stnd_aufk.stnd_auf_kz,stnd_auf_kz);
}


static void InitAuf (void)
/**
aufform Initialisieren.
**/
{
          sprintf (kuns,"%8ld", 0l);
          sprintf (kunfil,"%d", 0);
          sprintf (stnd_auf_kz," ");
          sprintf (stnd_auf_kz_bez," ");
          strcpy (k_kurzb, " ");
          strcpy (_adr.tel, " ");
          strcpy (_adr.fax,  " ");
          set_fkt (NULL, 6);
          SetFkt (6, leer, 0);
}


static int leseauf ()
/**
Standardauftrag lesen.
**/
{
	     int dsqlstatus;

         if (testkeys ()) return 0;

		 adr_nr = 0l;
         temode = 1;
         SetTeleNr ();
         stnd_aufk.mdn = atoi (mdn);
         stnd_aufk.fil = atoi (fil);
         stnd_aufk.kun_fil = atoi (kunfil);
         stnd_aufk.kun = atol (kuns);
         dsqlstatus = Saufk.dbreadfirst ();
         if (dsqlstatus == 0)
         {
                AufToForm ();
         }
         return 0;
}

static int testkunfil ()
{
         if (testkeys ()) return 0;

         if (atoi (kunfil) != 0)
         {
             sprintf (kunfil, "%hd", 1);
         }
         stnd_aufk.kun_fil = atoi (kunfil);
         if (atoi (kunfil) == 0)
         {
                       ikunfil_bz.SetFeldPtr ("Kunde");
         }
         else
         {
                       ikunfil_bz.SetFeldPtr ("Filiale");
         }
         display_field (mamain1, &aufform.mask[GetItemPos (&aufform, "kun_fil")], 0, 0);
         display_field (mamain1, &aufform.mask[GetItemPos (&aufform, "kun_fil_bz")], 0, 0);
         return 0;
}


static int TestKz (void)
{
         int dsqlstatus;
         
         dsqlstatus = ptab_class.lese_ptab ("stnd_auf_kz", stnd_auf_kz);
         if (dsqlstatus == 100)
         {
             disp_mess ("Kennzeichen nicht korrekt !!", 2);
             SetCurrentFocus (GetItemPos (&aufform, "stnd_auf_kz"));
             return 0;
         }
         return 1;
}

static int TestKun (void)
{
         int kun_krz_field;
         int kun_field;
 
		 clipped (kuns);
         kun_krz_field = GetItemPos (&aufform, "kun_krz");
         kun_field = GetItemPos (&aufform, "kun");

         if (atol (kuns) == 0)
         {
                  disp_mess ("Kunde 0 ist nicht erlaubt", 2);
                  SetCurrentFocus (GetItemPos (&aufform, "kun"));
                  return 0;
         }

         if (atoi (kunfil) == 0)
         {

// Kunde immer mir Filiale 0 lesen 

                  dsqlstatus = kun_class.lese_kun (atoi (mdn),
                                                         0,
                                                         atol (kuns));
                  if (dsqlstatus)
                  {
                           disp_mess ("Kunde nicht angelegt", 0);
                           SetCurrentFocus (GetItemPos (&aufform, "kun"));
                           return 0;
				  }
                  dsqlstatus = adr_class.lese_adr (kun.adr2);
                  strcpy (k_kurzb, _adr.adr_krz);
         }
         if (atoi (kunfil) == 1)
         {
                  dsqlstatus = fil_class.lese_fil (atoi (mdn),
                                                    atoi (kuns));
                  if (dsqlstatus)
                  {
                                disp_mess ("Filiale nicht angelegt", 0);
                                SetCurrentFocus (GetItemPos (&aufform, "kun"));
                                return 0;
                  }
                  dsqlstatus = adr_class.lese_adr (_fil.adr);
                  strcpy (k_kurzb, _adr.adr_krz);
         }
         return 1;
}

static int lesekun0 ()
{
         int kun_krz_field;
         int kun_field;
 
		 clipped (kuns);
         kun_krz_field = GetItemPos (&aufform, "kun_krz");
         kun_field = GetItemPos (&aufform, "kun");
 	     if (!numeric (kuns))
		 {
			      kun.mdn = atoi (mdn);
			      kun.fil = atoi (fil);
                  QClass.searchkun_direct (mamain1, kuns);
		          sprintf (kuns, "%8ld", atol (kuns));
				  if (atol (kuns) == 0)
				  {
                            current_form = &aufform;
                            SetCurrentField (currentfield);
                            display_field (mamain1, &aufform.mask[kun_field], 0, 0);
							return 0;
				  }
		 }

		 sprintf (kuns, "%8ld", atol (kuns));

         SetFkt (9, leer, 0);
         set_fkt (NULL, 9);

         if (akt_kun_fil != atoi (kunfil));
         else if (akt_kun == atol (kuns)) 
		 {
             display_field (mamain1, &aufform.mask[kun_field], 0, 0);
             display_field (mamain1, &aufform.mask[kun_krz_field], 0, 0);
			 return 0;
		 }

         if (atol (kuns) == 0)
         {
                  disp_mess ("Kunde 0 ist nicht erlaubt", 2);
                  SetCurrentField (currentfield);
                  return 0;
         }

	     if (autokunfil && akt_kun != atol (kuns))
		 {
	              stnd_aufk.kun_fil = 0;
		          strcpy (kunfil, "0");
                  ikunfil_bz.SetFeldPtr ("Kunde");
		 }

         if (atoi (kunfil) == 0)
         {
			      dsqlstatus = 0; 
				  if (dsqlstatus == 0)
				  {

// Kunde immer mir Filiale 0 lesen 

                        dsqlstatus = kun_class.lese_kun (atoi (mdn),
                                                         0,
                                                         atol (kuns));
				  }
			      if (dsqlstatus == 100 && autokunfil)
				  {
					   stnd_aufk.kun_fil = 1;
					   strcpy (kunfil, "1");
                       ikunfil_bz.SetFeldPtr ("Filiale");
                       display_form (mamain1, &aufform, 0, 0);
				  }
				  else
				  {
                       if (dsqlstatus)
					   {
                           disp_mess ("Kunde nicht angelegt", 0);
                           SetCurrentField (currentfield);
                           return 0;
					   }
					   akt_kun = kun.kun;
                       dsqlstatus = adr_class.lese_adr (kun.adr2);
                       strcpy (k_kurzb, _adr.adr_krz);
					   if (autokunfil)
					   {
  				           stnd_aufk.kun_fil = 0;
					       strcpy (kunfil, "0");
                           ikunfil_bz.SetFeldPtr ("Kunde");
                           display_form (mamain1, &aufform, 0, 0);
					   }
				  }
         }
         if (atoi (kunfil) == 1)
         {
		          dsqlstatus = 0;
				  if (dsqlstatus == 0)
				  {
                       dsqlstatus = fil_class.lese_fil (atoi (mdn),
                                                    atoi (kuns));
                       if (dsqlstatus)
					   {
                                disp_mess ("Filiale nicht angelegt", 0);
								if (autokunfil)
								{
					                   stnd_aufk.kun_fil = 0;
					                   strcpy (kunfil, "0");
                                       ikunfil_bz.SetFeldPtr ("Kunde");
                                       display_form (mamain1, &aufformk, 0, 0);
								}
                                SetCurrentField (currentfield);
                                return 0;
					   }
                       else
					   {
                                dsqlstatus = adr_class.lese_adr (_fil.adr);
                                strcpy (k_kurzb, _adr.adr_krz);
					            akt_kun = kun.kun;
					   }
                       kun.einz_ausw = 0;
				  }
         }
         akt_kun = atol (kuns);
         akt_kun_fil = atoi (kunfil);
         if (dsqlstatus == 0)
         {
                   clipped (_adr.tel);
                   if (strcmp (_adr.tel, " ") > 0)
                   {
                          CTelefon.bmp = btelefon;
                          ActivateColButton (mamain1, 
                                      &buform, 1, 0, 1);
                         EnableMenuItem (hMenu, CallT2,   MF_ENABLED);
                   }
                   else
                   {
                          CTelefon.bmp = btelefoni;
                          ActivateColButton (mamain1, 
                                      &buform,      1, -1, 1);
                         EnableMenuItem (hMenu, CallT2,   MF_GRAYED);
                   }

                   clipped (_adr.fax);
                   if (strcmp (_adr.fax, " ") > 0)
                   {
                          ActivateColButton (mamain1, 
                                      &buform,      0, 0, 1);
                         EnableMenuItem (hMenu, CallT1,   MF_ENABLED);
                   }
                   else
                   {
                          ActivateColButton (mamain1, 
                                      &buform,      0, -1, 1);
                         EnableMenuItem (hMenu, CallT1,   MF_GRAYED);
                   }
                   ActivateColButton (mamain1, &buform, 2, 0, 1);
                   EnableMenuItem (hMenu, CallT3,   MF_ENABLED);
                   MoveButtons ();
         }
         MoveButtons ();
         current_form = &aufform;
         display_field (mamain1, &aufform.mask[kun_field], 0, 0);
         display_field (mamain1, &aufform.mask[kun_krz_field], 0, 0);
         return 1;
}

static int lesekun ()
/**
Kunde fuer Kundennummer holen.
**/
{
         if (testkeys ()) return 0;

		 clipped (kuns);
         if (lesekun0 () == 0) 
		 {
             set_fkt (NULL, 6);
             SetFkt (6, leer, 0);
			 return 1;
		 }
         if (mench == 0)
         {
             set_fkt (doliste, 6);
             SetFkt (6, liste, KEY6);
         }
         leseauf ();
         display_form (mamain1, &aufform, 0, 0);
		 if (listedirect == 1)
		 {
			 PostMessage (mamain1, WM_KEYDOWN, VK_F6, 0l);
		 }
		 return 1;
}



int doliste ()
{
         break_enter ();
         return 0;
}

int SwitchArt (void)
/**
Switch zwischen Kundenartikel-Nummer und eigener Atikelnummer.
**/
{
         if (aktart == kunart)
         {
             aktart = eigart;
             aufpListe.SetFieldAttr ("a_kun", DISPLAYONLY);
             aufpListe.SetChAttr (1);
             aufpListe.SetFieldAttr ("a_bz2", DISPLAYONLY);
         }
         else
         {
             aktart = kunart;
             aufpListe.SetFieldAttr ("a_kun", REMOVED);
             aufpListe.SetChAttr (0);
             aufpListe.SetFieldAttr ("a_bz2", REMOVED);
         }
         SetFkt (10, aktart, KEY10);
         aufpListe.DestroyWindows ();
/*
         aufpListe.ShowAufp ();
*/
         return 0;
}


int SwitchZei (void)
/**
Switch zwischen Kundenartikel-Nummer und eigener Atikelnummer.
**/
{
         if (aktzei == a_bz2_on)
         {
             aktzei = a_bz2_off;
             aufpListe.SetFieldAttr ("a_bz2", DISPLAYONLY);
         }
         else
         {
             aktzei = a_bz2_on;
             aufpListe.SetFieldAttr ("a_bz2", REMOVED);
         }
         SetFkt (7, aktzei, KEY7);
         aufpListe.DestroyWindows ();
/*
         aufpListe.ShowAufp (); 
*/
         return 0;
}


void DelAuf (void)
{
         if (abfragejn (mamain1, "Standardauftrag l�schen ?", "J"))
         {
                  if (TestKun () == 0) return;
                  if (TestKz () == 0) return; 
                  FormToAuf (1);
                  Saufk.dbdelete ();
                  DbClass.sqlin ((short *) &stnd_aufk.mdn, 1, 0);
                  DbClass.sqlin ((short *) &stnd_aufk.fil, 1, 0);
                  DbClass.sqlin ((short *) &stnd_aufk.kun_fil, 1, 0);
                  DbClass.sqlin ((long *)  &stnd_aufk.kun, 2, 0);
                  DbClass.sqlcomm ("delete from stnd_aufp "
                                   "where mdn = ? "
                                   "and fil = ? "
                                   "and kun_fil = ? "
                                   "and kun = ?");
                  commitwork ();
                  InitAuf ();
                  break_enter ();
                  set_fkt (NULL, 12);
                  Mess.Message ("Satz wurde gel�scht");
          }
}


void PrintAuf (void)
{
          ProcWaitExecEx ("70001 51300", SW_SHOWNORMAL, -1, 0, -1, 0);
          SetCurrentFocus (currentfield);
}

int dokey12 ()
/**
Taste Key12 behandeln.
**/
{
  		   akt_tou = 0l;
           if (mench == 0)
           {
                 if (TestKun () == 0) return 0;
                 if (TestKz () == 0) return 0;
                 FormToAuf (1);
                 Saufk.dbupdate ();
                 commitwork ();
                 InitAuf ();
                 syskey = KEY12;
                 break_enter ();
                 set_fkt (NULL, 12);
                 Mess.Message ("Satz wurde gespeichert");
           }
           else if (mench == 1)
           {
                 if (abfragejn (mamain1, "Standardauftrag l�schen ?", "J"))
                 {
                        if (TestKun () == 0) return 0;
                        if (TestKz () == 0) return 0;
                        FormToAuf (1);
                        Saufk.dbdelete ();
                        DbClass.sqlin ((short *) &stnd_aufk.mdn, 1, 0);
                        DbClass.sqlin ((short *) &stnd_aufk.fil, 1, 0);
                        DbClass.sqlin ((short *) &stnd_aufk.kun_fil, 1, 0);
                        DbClass.sqlin ((long *)  &stnd_aufk.kun, 2, 0);
                        DbClass.sqlcomm ("delete from stnd_aufp "
                                         "where mdn = ? "
                                         "and fil = ? "
                                         "and kun_fil = ? "
                                         "and kun = ?");
                        commitwork ();
                        InitAuf ();
                        break_enter ();
                        set_fkt (NULL, 12);
                        Mess.Message ("Satz wurde gel�scht");
                 }
           }
           
		   stnd_aufk.mdn = stnd_aufk.fil = 0;
           return 0;
}

void tst_arg (char *arg)
{
          for (; *arg; arg += 1)
          {
                 switch (*arg)
                 {
                            case 'u' :
                                     strcpy (InfoCaption, arg + 1);
                                     return;
                 }
          }
          return;
}


int dokey5 ()
/**
Aktion bei Taste F5
**/
{
        if (inDaten && mench == 0)
        {
            if (abfragejn (mamain1, "Daten speichern ?", "J"))
            {
                  return dokey12 ();
            }
        }
        InitAuf ();
        rollbackwork ();
        break_enter ();
        return 1;
}


BOOL IsComboMsg (MSG *msg)
/**
Test, ob die Meldung fuer die Combobox ist.
**/
{
      if (HIWORD (msg->lParam) == CBN_DROPDOWN)
      {
                 opencombobox = TRUE;
                 return TRUE;
      }
      if (HIWORD (msg->lParam) == CBN_CLOSEUP)
      {
                 opencombobox = FALSE;
                 return TRUE;
      }

      if (msg->hwnd == hwndCombo1)
      {
                 return TRUE;
      }
      if (msg->hwnd == hwndTB)
      {
                 return TRUE;
      }

      return FALSE;
}

void FillCombo1 (void)
{
      int i;

      for (i = 0; Combo1[i]; i ++)
      {
           SendMessage (hwndCombo1, CB_ADDSTRING, 0,
                                   (LPARAM) Combo1 [i]);
      }
      SendMessage (hwndCombo1, CB_SELECTSTRING, 0,
                                   (LPARAM) Combo1 [3]);
      aufpListe.SetLines (3);
}

void FillCombo2 (void)
{
      int i;

      for (i = 0; Combo2[i]; i ++)
      {
           SendMessage (hwndCombo2, CB_ADDSTRING, 0,
                                   (LPARAM) Combo2 [i]);
      }
      SendMessage (hwndCombo2, CB_SELECTSTRING, 0,
                                   (LPARAM) Combo2 [combo2pos]);
      SetComboMess (IsComboMsg);
}

static int StopProc (void)
{
    PostQuitMessage (0);
    return 0;
}


void DisableTB (void)
{
     ToolBar_SetState(hwndTB,IDM_STD,  TBSTATE_INDETERMINATE);
     ToolBar_SetState(hwndTB,IDM_LIST, TBSTATE_INDETERMINATE);
     ToolBar_SetState(hwndTB,KEYLEFT,  TBSTATE_INDETERMINATE);
     ToolBar_SetState(hwndTB,KEYRIGHT, TBSTATE_INDETERMINATE);
     ToolBar_SetState(hwndTB,IDM_FIND, TBSTATE_INDETERMINATE);
}

void EnableTB (void)
{
     ToolBar_SetState(hwndTB,IDM_STD,  TBSTATE_ENABLED);
     ToolBar_SetState(hwndTB,IDM_LIST, TBSTATE_ENABLED);
     ToolBar_SetState(hwndTB,KEYLEFT,  TBSTATE_ENABLED);
     ToolBar_SetState(hwndTB,KEYRIGHT, TBSTATE_ENABLED);
     ToolBar_SetState(hwndTB,IDM_FIND, TBSTATE_ENABLED);

     ToolBar_PressButton(hwndTB, IDM_PAGE, FALSE);
     ToolBar_PressButton(hwndTB, IDM_LIST, TRUE);
}



int EnterPrVk ()
{
        aufpListe.EnterPrVk ();
        return 0;
}


int EnterListe ()
{
    
	PrintButtons ();
	DlgWindow = NULL;
    EnableTB ();
    set_fkt (NULL, 4);
	EnableArrows (&aufform, FALSE);
    EnableMenuItem (hMenu, IDM_FIND,    MF_ENABLED);
    EnableMenuItem (hMenu, IDM_FONT,    MF_ENABLED);
    EnableMenuItem (hMenu, IDM_BASIS,   MF_ENABLED);
    EnableMenuItem (hMenu, IDM_POSTEXT, MF_ENABLED);
    EnableMenuItem (hMenu, IDM_LDPR,    MF_ENABLED);
    EnableMenuItem (hMenu, IDM_VKPR,    MF_ENABLED);

    EnableMenuItem (hMenu, IDM_DEL,   MF_GRAYED);
    ToolBar_SetState(hwndTB,IDM_DEL,  TBSTATE_INDETERMINATE);

    set_fkt (NULL, 9);
    SetFkt (9, leer, 0);
    set_fkt (NULL, 10);
    SetFkt (10, leer, 0);

    if (PrVkEnabled)
    {
         set_men (EnterPrVk, (unsigned char) 'V');
    }

/* Vor dem Aufruf der Liste Kopf abspeichern */

    FormToAuf (1);

    aufpListe.EnterAufp ();

    set_men (NULL, (unsigned char) 'L');
    set_men (NULL, (unsigned char) 'V');

    EnableMenuItem (hMenu, IDM_DEL,   MF_ENABLED);
    ToolBar_SetState(hwndTB,IDM_DEL,  TBSTATE_ENABLED);

    EnableMenuItem (hMenu, IDM_FIND, MF_GRAYED);
    EnableMenuItem (hMenu, IDM_FONT, MF_GRAYED);
    EnableMenuItem (hMenu, IDM_BASIS, MF_GRAYED);
    EnableMenuItem (hMenu, IDM_POSTEXT, MF_GRAYED);
    EnableMenuItem (hMenu, IDM_LDPR,    MF_GRAYED);
    EnableMenuItem (hMenu, IDM_VKPR,    MF_GRAYED);
	EnableArrows (&aufform, TRUE);
    DisableTB ();
    PosOK = TRUE;
    set_fkt (dokey5, 5);
    set_fkt (dokey12, 12);
    SetCurrentFocus (GetItemPos (&aufform, "kun_fil"));
    return 0;
}

void PrintButtons (void)
{
         clipped (_adr.tel);
         if (strcmp (_adr.tel, " ") > 0)
         {
                CTelefon.bmp = btelefon;
                ActivateColButton (mamain1, 
                                  &buform, 1, 0, 1);
                EnableMenuItem (hMenu, CallT2,   MF_ENABLED);
         }
         else
         {
                CTelefon.bmp = btelefoni;
                ActivateColButton (mamain1, 
                &buform, 1, -1, 1);
                EnableMenuItem (hMenu, CallT2,   MF_GRAYED);
         }
         clipped (_adr.fax);
         if (strcmp (_adr.fax, " ") > 0)
         {
                ActivateColButton (mamain1, 
                                   &buform,      0, 0, 1);
                EnableMenuItem (hMenu, CallT1,   MF_ENABLED);
         }
         else
         {
                ActivateColButton (mamain1, 
                                      &buform,      0, -1, 1);
                EnableMenuItem (hMenu, CallT1,   MF_GRAYED);
         }
         ActivateColButton (mamain1, &buform, 2, 0, 1);
         EnableMenuItem (hMenu, CallT3,   MF_ENABLED);
         MoveButtons ();
}


void MoveButtons (void)
/**
Buttons in mamain1 rechtsbuendig.
**/
{
     RECT rect;
     RECT rectb;
     TEXTMETRIC tm;
     HDC hdc;
     int i;
     int x, y;

     GetClientRect (mamain1, &rect); 
     hdc = GetDC (mamain1);
     GetTextMetrics (hdc, &tm);
     ReleaseDC (mamain1, hdc);

     for (i = 0; i < buform.fieldanz; i ++)
     {
		 if (buform.mask[i].feldid == NULL) continue;

         GetClientRect (buform.mask[i].feldid, &rectb );
         x = rect.right - rectb.right - 1;
		 if (BuFont.PosMode == 0)
		 {
                y = buform.mask[i].pos[0] * tm.tmHeight;
		 }
		 else
		 {
                y = buform.mask[i].pos[0];
		 }
         MoveWindow (buform.mask[i].feldid,
                                    x, y,
                                    rectb.right,
                                    rectb.bottom,
                                    TRUE);

		 buform.mask[i].pos[0] = y;
		 buform.mask[i].pos[1] = x;
		 buform.mask[i].length = (short) rectb.right;
		 buform.mask[i].rows   = (short) rectb.bottom;
     }
     if (buform.mask[0].feldid)
	 {
	     BuFont.PosMode = 1;
	 }
     CreateQuikInfos ();
}


int CalcMinusPos (int cy)
{
        TEXTMETRIC tm;
        SIZE size;
        HDC hdc;


        hdc = GetDC (ftasten);
        GetTextMetrics (hdc, &tm);
        GetTextExtentPoint32 (hdc, "X", 1, &size);
        tm.tmAveCharWidth = size.cx;
        ReleaseDC (ftasten, hdc);
        cy -= 3 * tm.tmHeight;
        return cy;
}

int CalcPlusPos (int y)
{
        TEXTMETRIC tm;
        SIZE size;
        HDC hdc;


        hdc = GetDC (ftasten);
        GetTextMetrics (hdc, &tm);
        GetTextExtentPoint32 (hdc, "X", 1, &size);
        tm.tmAveCharWidth = size.cx;
        ReleaseDC (ftasten, hdc);
        y += 1 * tm.tmHeight + 5;
        return y;
}

void MoveMamain1 (void)
/**
Hauptarbeitsfenster oeffnen.
**/
{
     RECT trect;
     RECT frect;
     RECT mrect;
     RECT crect;
     int x,y, cx, cy;

     GetWindowRect (hMainWindow, &mrect);
     GetWindowRect (ftasten, &frect);

     GetClientRect (hMainWindow, &crect);
     GetClientRect (hwndTB, &trect);
     x = 5;
     cx = crect.right - x - 5;

     y = trect.bottom + 5;
     cy = frect.top - mrect.top - y - 50;
     if (ftasten2 == NULL)
     {
         y  = CalcPlusPos (y);
         cy = CalcMinusPos (cy);
     }
     MoveWindow (mamain1, x, y, cx, cy, TRUE);
     MoveFktBitmap ();
     MoveButtons ();
}


void Createmamain1 (void)
/**
Hauptarbeitsfenster oeffnen.
**/
{
     RECT trect;
     RECT frect;
     RECT mrect;
     RECT crect;
     int x,y, cx, cy;


     GetWindowRect (hMainWindow, &mrect);
     GetWindowRect (ftasten, &frect);

     GetClientRect (hMainWindow, &crect);
     GetClientRect (hwndTB, &trect);
     x = 5;
     cx = crect.right - x - 5;

     y = trect.bottom + 5;


     cy = frect.top - mrect.top - y - 50;

     if (ftasten2 == NULL)
     {
         y  = CalcPlusPos (y);
         cy = CalcMinusPos (cy);
     }

     mamain1 = CreateWindowEx (
                              WS_EX_CLIENTEDGE, 
                              "hStdWindow",
                              InfoCaption,
                              WS_CHILD |  
                              WS_VISIBLE,
                              x, y,
                              cx, cy,
                              hMainWindow,
                              NULL,
                              hMainInst,
                              NULL);
}


int ShowFil (void)
/**
Auswahl ueber Filialen.
**/
{
	   _fil.mdn = atoi (mdn);
       fil_class.ShowAllBu (mamain1, 0, stnd_aufk.mdn);
       if (syskey == KEYESC || syskey == KEY5)
       {
           return 0;
       }
       kunfield = GetItemPos (&aufform, "kun");
       sprintf (kuns, "%d", _fil.fil);
       display_field (mamain1, &aufform.mask[kunfield], 0, 0);
       PostMessage (mamain1, WM_KEYDOWN, VK_RETURN, 0l);
       return 0;

}
         

int QueryKun (void)
/**
Auswahl ueber Kunden.
**/
{
       int ret;

       DisablehWnd (hMainWindow);
       DisableListhWnd (mamain1);
	   kun.mdn = atoi (mdn);
//	   kun.fil = atoi (fil);
	   kun.fil = 0;
       ret = QClass.querykun (mamain1);
       set_fkt (dokey5, 5);
       set_fkt (dokey12, 12);
       if (mench == 0) set_fkt (doliste, 6);
       SetFkt (6, liste, KEY6);
       SetFkt (7, aktzei, KEY7);
       SetFkt (9, auswahl, KEY9);
	   SetFkt (8, leer, NULL);
	   set_fkt (NULL, 8);

       if (atoi (kunfil))
       {
             set_fkt (ShowFil, 9);
       }
       else
       {
             set_fkt (QueryKun, 9);
       }

       set_fkt (dokey12, 12);
       SetAktivWindow (mamain1);
       if (ret == FALSE)
       {
           SetCurrentFocus (currentfield);
           return 0;
       }
       kunfield = GetItemPos (&aufform, "kun");
       sprintf (kuns, "%ld", kun.kun);
       display_field (mamain1, &aufform.mask[kunfield], 0, 0);
       SetCurrentFocus (kunfield);
       PostMessage (mamain1, WM_KEYDOWN, VK_RETURN, 0l);
       return 0;
}

      
int ReadPtab0 (void)
{
	  char wert [5];
      int ItemPos;
      int dsqlstatus;

      if (strcmp (aufform.mask[currentfield].item->GetItemName (), "stnd_auf_kz") == 0)
      {
	           sprintf (wert, "%s", stnd_auf_kz);
	           ptabn.ptbezk[0] = 0;
               dsqlstatus = ptab_class.lese_ptab ("stnd_auf_kz", wert);
               if (dsqlstatus == 100)
               {
                   disp_mess ("Falsche Eingabe", 2);
                   SetCurrentFocus (currentfield);
                   return 0;
               }
	           strcpy (stnd_auf_kz_bez, ptabn.ptbezk);
               ItemPos = GetItemPos (&aufform, "stnd_auf_kz_bez");
      }
      if (ItemPos >= 0)
      {
               display_field (mamain1, &aufform.mask[ItemPos], 0, 0);
      }
      return 1;
}

int ReadPtab (void)
{

      if (testkeys ()) return 0;

      if (ReadPtab0 ())
      {
          if (mench == 0)
          {
                 break_enter ();
          }
          else
          {
                 aufpListe.ShowAufp ();
          }
          return 1;
      }
      return 0;
}

int ShowPtab (void)
{
       char wert [80];
       int ItemPos;
       SEARCHPTAB *SearchPtab;
 

       SearchPtab = new SEARCHPTAB (aufform.mask[currentfield].item->GetItemName ());
       SearchPtab->SetParams (hMainInst, hMainWindow); 
       SearchPtab->Setawin (hMainWindow);
                                                  
       SearchPtab->Search ();
       if (SearchPtab->GetKey (wert) == TRUE)
       {
           strcpy (stnd_auf_kz, wert);
           ItemPos = GetItemPos (&aufform, "stnd_auf_kz");
           if (ItemPos >= 0)
           {
               display_field (mamain1, &aufform.mask[ItemPos], 0, 0);
               SetCurrentFocus (ItemPos);
           }
           ReadPtab0 ();
       }
       delete SearchPtab;
       return TRUE;
}



int setkey9ptab ()
/**
Auswahl fuer Kunde setzen.
**/
{
       SetFkt (9, auswahl, KEY9);
       set_fkt (ShowPtab, 9);
       return 0;
}



int setkey9kun ()
/**
Auswahl fuer Kunde setzen.
**/
{
       akt_kun = atol (kuns);
       SetFkt (9, auswahl, KEY9);
       if (atoi (kunfil))
       {
             set_fkt (ShowFil, 9);
       }
       else
       {
             set_fkt (QueryKun, 9);
       }
       return 0;
}

int SwitchKunFil (void)
{
       if (atoi (kunfil) == 0)
       {
           sprintf (kunfil, "1");
           ikunfil_bz.SetFeldPtr ("Filiale");
       }
       else
       {
           sprintf (kunfil, "0");
           ikunfil_bz.SetFeldPtr ("Kunde");
       }
       stnd_aufk.kun_fil = atoi (kunfil);
       display_field (mamain1, &aufformk.mask[2], 0, 0);
       display_field (mamain1, &aufformk.mask[3], 0, 0);
       return 0;
}

void EnableArrows (form *frm, BOOL mode)
/**
Pfeilbuttons aktivieren oder deaktivieren.
**/
{
	   int i;

	   for (i = 0; i < frm->fieldanz; i ++)
	   {
		   if (frm->mask[i].item->GetItemName () && 
			   strcmp (frm->mask[i].item->GetItemName (), "arrdown") == 0)
		   {
			   if (mode == FALSE)
			   {
			             frm->mask[i].picture = "";
						 frm->mask[i].attribut = REMOVED;
						 CloseControl (frm, i);
			   }
			   else
			   {
						 frm->mask[i].attribut = BUTTON;
			             frm->mask[i].picture = "B";
			   }
			   EnableWindow (frm->mask[i].feldid, mode);
		   }
	   }
}


void WorkRights (HMENU hMenu)
{
     EnableMenuItem (hMenu,      IDM_DEL,   MF_ENABLED);
     EnableMenuItem (hMenu, IDM_PRINT, MF_ENABLED);
     ToolBar_SetState(hwndTB,IDM_DEL, TBSTATE_ENABLED);
     ToolBar_SetState(hwndTB,IDM_PRINT, TBSTATE_ENABLED);
}

void MenueRights (HMENU hMenu)
{
     EnableMenuItem (hMenu,      IDM_DEL,   MF_GRAYED);
     EnableMenuItem (hMenu, IDM_PRINT, MF_GRAYED);
     ToolBar_SetState(hwndTB ,IDM_DEL, TBSTATE_INDETERMINATE);
     ToolBar_SetState(hwndTB ,IDM_PRINT, TBSTATE_INDETERMINATE);
}


void EnterAuf (void)
/**
Autrag bearbeiten.
**/
{
	 

       inDaten = TRUE;
       WorkRights (hMenu);
  	   EnableArrows (&aufform, TRUE);
       Mess.CloseMessage ();
       akt_auf = 0l;
       if (autokunfil) strcpy (kunfil, "0"); 
  	   if (atoi (fil) > 0) strcpy (kunfil, "0");
       if (atoi (kunfil) == 0)
       {
           ikunfil_bz.SetFeldPtr ("Kunde");
       }
       else
       {
           ikunfil_bz.SetFeldPtr ("Filiale");
       }
       strcpy (stnd_auf_kz, "F"); 
       ptabn.ptbezk[0] = 0;
       ptab_class.lese_ptab ("stnd_auf_kz", "F");
       strcpy (stnd_auf_kz_bez, ptabn.ptbezk);
       while (TRUE)
       {

                break_end ();
                beginwork ();
                DisplayAfterEnter (TRUE);
                no_break_end ();
				telekun = FALSE;
                enter_form (mamain1, &aufform, 0, 0);
                set_fkt (NULL, 8);
                SetFkt (8, leer, 0);
                set_fkt (NULL, 9);
                SetFkt (9, leer, 0);
                set_fkt (NULL, 10);
                SetFkt (10, leer, 0);
                if (syskey == KEY5 || syskey == KEYESC) break;
                if (syskey == KEY12) break;

				EnableArrows (&aufformk, FALSE);
                EnterListe ();
//                commitwork ();
				EnableArrows (&aufformk,TRUE);
                CTelefon.bmp = btelefoni;
                ActivateColButton (mamain1, 
                                      &buform, 0, -1, 1);
                ActivateColButton (mamain1, 
                                      &buform, 1, -1, 1);
                ActivateColButton (mamain1, 
                                      &buform, 2, -1, 1);
                EnableMenuItem (hMenu, CallT1,   MF_GRAYED);
                EnableMenuItem (hMenu, CallT2,   MF_GRAYED);
                EnableMenuItem (hMenu, CallT3,   MF_GRAYED);

                MoveButtons ();
                set_fkt (NULL, 6);
                set_fkt (NULL, 7);
                set_fkt (NULL, 9);
                SetFkt (6, leer, 0);
                SetFkt (7, leer, 0);
                SetFkt (9, leer, 0);
        }
        aufpListe.DestroyWindows (); 
        CloseControls (&aufform);
        set_fkt (NULL, 6);
        set_fkt (NULL, 7);
        SetFkt (6, leer, 0);
        SetFkt (7, leer, 0);
        Mess.CloseMessage ();
        inDaten = FALSE;
        MenueRights (hMenu);
        return;
}

void DisableMe (void)
{
     ToolBar_SetState(hwndTB,IDM_WORK, TBSTATE_INDETERMINATE);
     EnableMenuItem (hMenu,  IDM_WORK,  MF_GRAYED);

     ToolBar_SetState(hwndTB,IDM_SHOW, TBSTATE_INDETERMINATE);
     EnableMenuItem (hMenu,  IDM_SHOW,  MF_GRAYED);

     ToolBar_SetState(hwndTB,IDM_PRINT, TBSTATE_INDETERMINATE);
     EnableMenuItem (hMenu,  IDM_PRINT,  MF_GRAYED);

}

void EnableMe (void)
{

     ToolBar_SetState(hwndTB, IDM_WORK, TBSTATE_ENABLED);
     EnableMenuItem (hMenu,      IDM_WORK, MF_ENABLED);

     ToolBar_SetState(hwndTB, IDM_SHOW, TBSTATE_ENABLED);
     EnableMenuItem (hMenu,      IDM_SHOW, MF_ENABLED);

     ToolBar_SetState(hwndTB, IDM_PRINT, TBSTATE_ENABLED);
     EnableMenuItem (hMenu,      IDM_PRINT, MF_ENABLED);

}


void ShowBm (void)
{
    RECT rect;
    TEXTMETRIC tm;
    int x, y, cx, cy;
    static int abmp = 0;
    char buffer [80];

    abmp ++;
    if (abmp > 3) abmp = 1;
    GetWindowRect (hMainWindow, &rect);
    stdfont ();
    SetTmFont (hMainWindow, &tm);

    cx = 5;
    cy = 2;
    x = (rect.right - 2) / tm.tmAveCharWidth;
    x -= cx;
    y = rect.top;
    y = (int) (double) ((double) y / 
                                (double)(1 + (double) 1/3));
    y /= tm.tmHeight;
    y += 3;
    sprintf (buffer, "-nm2 -z%d -s%d -h%d -w%d "
             "c:\\user\\fit\\bilder\\artikel\\%d.bmp",
             y, x, cy, cx, abmp);
    _ShowBm (buffer);
}


void EingabeMdnFil (void)
{
       extern MDNFIL cmdnfil;

       Mess.CloseMessage ();
//       DisableMe ();
	   if (fil_lief_par == FALSE)
	   {
                 cmdnfil.SetFilialAttr (REMOVED);
	   }
       cmdnfil.SetNoMdnNull (TRUE);
	   while (TRUE)
       {
           EnableArrows (&aufform, FALSE);
           set_fkt (dokey5, 5);
		   if (cmdnfil.eingabemdnfil (mamain1, mdn, fil) == -1)
           {
			         break;
           }
// Nicht aktiv    ShowBm ();
           switch (mench)
           {
                case 0:
                case 1:
                      set_fkt (dokey12, 12);
                      EnterAuf  ();
                      break;
           }
	   }
       cmdnfil.CloseForm ();
//       EnableMe ();
}


static HWND chwnd;

static int lstcancel (void)
{

    syskey = KEY5;
    break_enter ();
    return 0;
}


static int lstok (void)
{
    syskey = KEY12;
    mench = currentfield;
    break_enter ();
    return 0;
}

static int testwork (void)
{
    if (testkeys ()) return 0;

    if (syskey != KEYCR &&
        syskey != KEY12) return 0;

//    mench = 0;
    break_enter ();
    return 0;
}

static int testshow (void)
{
    if (testkeys ()) return 0;

    if (syskey != KEYCR &&
        syskey != KEY12) return 0;

//    mench = 1;
    break_enter ();
    return 0;
}

static int testdel (void)
{
    if (testkeys ()) return 0;

    if (syskey != KEYCR &&
        syskey != KEY12) return 0;

    break_enter ();
    return 0;
}


static int testprint (void)
{
    if (testkeys ()) return 0;

    if (syskey != KEYCR &&
        syskey != KEY12) return 0;

    break_enter ();
    return 0;
}

static int testfree (void)
{
    if (testkeys ()) return 0;

    if (syskey != KEYCR &&
        syskey != KEY12) return 0;

    break_enter ();
    return 0;
}

static int testcancel (void)
{
    if (testkeys ()) return 0;

    if (syskey != KEYCR &&
        syskey != KEY12) return 0;

	syskey = KEY5;
    break_enter ();
    return 0;
}



        static ColButton LstChoise = {
                              "Auswahl", -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
//                               WHITECOL,
//                               RGB (0,0,120),
                               BLACKCOL,
                               GRAYCOL,
                               -2};
   
        static ColButton cWork = {
                              "Bearbeiten", -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               WHITECOL,
                               BLUECOL,
                               2};

        static ColButton cShow = {
                              "Anzeigen", -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               WHITECOL,
                               BLUECOL,
                               2};

        static ColButton cDel = {
                              "L�schen", -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               WHITECOL,
                               BLUECOL,
                               2};

        static ColButton cPrint = {
                              "Drucken", -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               WHITECOL,
                               BLUECOL,
                               2};

        static ColButton cFree = {
                              "Freigeben", -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               WHITECOL,
                               BLUECOL,
                               2};
							  static ColButton *cButtons [] = {&cWork, &cShow, &cDel, &cPrint, &cFree, NULL}; 



        static ITEM iLstChoise ("", (char *) &LstChoise,  "", 0);

        static ITEM iWork      ("", (char *) &cWork,      "", 0);
        static ITEM iShow      ("", (char *) &cShow,      "", 0);
        static ITEM iDel       ("", (char *) &cDel,       "", 0);
        static ITEM iPrint     ("", (char *) &cPrint,     "", 0);
        static ITEM iFree      ("", (char *) &cFree,      "", 0);

        static ITEM iWorkB      ("", (char *) "Bearbeiten",  "", 0);
        static ITEM iShowB      ("", (char *) "Anzeigen",    "", 0);
        static ITEM iDelB       ("", (char *) "L�schen",     "", 0);
        static ITEM iPrintB     ("", (char *) "Drucken",     "", 0);
        static ITEM iFreeB      ("", (char *) "Freigeben",   "", 0);

        static ITEM vOK     ("",  "    OK     ",  "", 0);
        static ITEM vCancel ("",  " Abbrechen ", "", 0);

        static field _fLstChoise[] = {
          &iLstChoise, 36, 0,  0, 0, 0, "", COLBUTTON, 0, 0, 0};

        static form fLstChoise = {1, 0, 0, _fLstChoise, 
                                  0, 0, 0, 0, NULL};    


        static field _clist [] = {
&iWork,     34, 2, 2, 1, 0, "", COLBUTTON,            0, testwork, 
                                                           KEY12 , 
&iDel,      34, 2, 4, 1, 0, "", COLBUTTON,            0, testdel, 
                                                           KEY12,
&iPrint,    34, 2, 6, 1, 0, "", COLBUTTON,            0, testprint, 
                                                          KEY12,
&vCancel,  12, 0,  7,12, 0, "",  BUTTON,               0, testcancel, 
                                                           KEY5, 
};


        static field _clistB [] = {
&iWorkB,     30, 0, 2, 3, 0, "", BUTTON,            0, testwork, 
                                                          KEY12,
&iDelB,      30, 0, 3, 3, 0, "", BUTTON,            0, testdel, 
                                                           KEY12,
&iPrintB,    30, 0, 4, 3, 0, "", BUTTON,            0, testprint, 
                                                           KEY12,
&vCancel,   12, 0,  7,12, 0, "",  BUTTON,               0, testcancel, 
                                                           KEY5, 
};


static form clist = {4, 0, 0, _clistB, 0, 0, 0, 0, NULL}; 



void StartMenu (void)
/**
Liste auswaehlen und Listengenerator starten
**/
{
     form *sform;
     int  sfield;


     while (TRUE)
     {
        sform = current_form;
        sfield = currentfield;
        save_fkt (5);
        save_fkt (12);

        set_fkt (lstcancel, 5);
        set_fkt (lstok, 12);

        no_break_end ();
        SetButtonTab (TRUE);
//        DisablehWnd (hMainWindow);
        SetBorder (WS_POPUP | WS_DLGFRAME);

        SetAktivWindow (mamain1);
        chwnd = OpenWindowCh (9, 36, 10, 22, hMainInst);
		MoveZeWindow (hMainWindow, chwnd);
        DlgWindow = chwnd;

        display_form (chwnd, &fLstChoise, 0, 0);
        currentfield = 0;
        enter_form (chwnd, &clist, 0, 0);
		mench = currentfield;
        CloseControls (&fLstChoise);
        CloseControls (&clist);
        SetAktivWindow (mamain1);
        DestroyWindow (chwnd);
        chwnd = NULL;
        SetButtonTab (FALSE);
        restore_fkt (5);
        restore_fkt (12);
        if (sform)
        {
                current_form = sform;
                currentfield = sfield;
                SetCurrentFocus (currentfield);
        }

        if (syskey == KEY5) return;
        if (syskey == KEYESC) return;

        switch (mench)
        {
              case 0 :
              case 1 :
                     EingabeMdnFil ();
					 break;
              case 2 :
                     ProcWaitExecEx ("70001 51300", SW_SHOWNORMAL, -1, 0, -1, 0);
                     break;
        }
     }
}


void SetListBkColor (char *coltxt)
/**
Hintergrund f�r Liste setzen.
**/
{
	   int i;
       static char *BkColorsTxt[] = {"WHITECOL",
                                     "BLUECOL",
                                     "BLACKCOL",
                                     "GRAYCOL",
					      		     "LTGRAYCOL",
	   };

       clipped (coltxt);
       for (i = 0; Combo2[i]; i ++)
       {
             if (strcmp (BkColorsTxt[i],coltxt) == 0)
             {
                   aufpListe.SetColors (Colors[i], BkColors[i]);
				   combo2pos = i;
                   break;
             }
	   }
}

void EnableAufArt (void)
{

       SetItemAttr (&aufform, "tele", REMOVED);
       SetItemAttr (&aufform, "auf_art", EDIT);
       SetItemAttr (&aufform, "auf_art_txt", READONLY);
       auf_art_active = TRUE;
}


void EnableLdVkChange ()
/**
Moeglichkeit zur Aenderung des VK's in Menue beaerbeiten einfuegen.
**/
{

       InsertMenueItem (bearbeiten, &changeldpr, 0);
       LdVkEnabled = TRUE;
}

void EnablePrVkChange ()
/**
Moeglichkeit zur Aenderung des VK's in Menue beaerbeiten einfuegen.
**/
{

       InsertMenueItem (bearbeiten, &changevkpr, 0);
       PrVkEnabled = TRUE;
}

void GetCfgValues (void)
/**
Werte aus 51100.cfg holen.
**/
{
	   static BOOL cfgOK = FALSE;
       char cfg_v [20];

	   if (cfgOK) return;

	   cfgOK = TRUE;
       if (ProgCfg.GetCfgValue ("NewStyle", cfg_v) == TRUE)
       {
                    NewStyle = atoi (cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("Startsize", cfg_v) == TRUE)
       {
                    Startsize = atoi (cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("colbutton", cfg_v) == TRUE)
       {
		            if (atoi (cfg_v))
					{
					       clist.mask = _clist;
					}
       }
       if (ProgCfg.GetCfgValue ("lad_vk_edit", cfg_v) == TRUE)
       {
                     if (atoi (cfg_v))
                     {
   		                  EnableLdVkChange ();
                     }
                         
       }
       if (ProgCfg.GetCfgValue ("pr_vk_edit", cfg_v) == TRUE)
       {
                     if (atoi (cfg_v))
                     {
   		                  EnablePrVkChange ();
                     }
                         
       }
       if (ProgCfg.GetCfgValue ("lsdirect", cfg_v) == TRUE)
       {
                     lsdirect = atol (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("searchmodea_bas", cfg_v) == TRUE)
       {
		            SetBasSearchMode (atoi (cfg_v));
       }
       if (ProgCfg.GetCfgValue ("searchfielda_bas", cfg_v) == TRUE)
       {
		            SetBasSearchField (atoi (cfg_v));
       }
       if (ProgCfg.GetCfgValue ("searchmodekun", cfg_v) == TRUE)
       {
		            kun_class.SetSearchModeKun (atoi (cfg_v));
       }
       if (ProgCfg.GetCfgValue ("searchfieldkun", cfg_v) == TRUE)
       {
		           kun_class.SetSearchFieldKun (atoi (cfg_v));
       }

       if (ProgCfg.GetCfgValue ("SAFCOLOR", cfg_v) == TRUE)
       {
		             aufpListe.GetListColor (&MessCol, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("SABCOLOR", cfg_v) == TRUE)
       {
		             aufpListe.GetListColor (&MessBkCol, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("autokunfil", cfg_v) == TRUE)
       {
                     autokunfil = min (max (0, atoi (cfg_v)), 1);
       }
       if (ProgCfg.GetCfgValue ("listedirect", cfg_v) == TRUE)
       {
                     listedirect = atoi (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("lsdirect", cfg_v) == TRUE)
       {
                     lsdirect = atol (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("ListBkColor", cfg_v) == TRUE)
       {
		             SetListBkColor (cfg_v);
	   }
}

void MoveMamain (void)
/**
Koordinaten in $BWSETC lesen.
**/
{
        char *etc;
        char buffer [256];
        FILE *fp;
        int anz;
		static BOOL scrfOK = FALSE;
		RECT rect;
	    int xfull, yfull;



	    if (MoveMain == FALSE) return;
	    if (Startsize > 0) return;
        xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
        yfull = GetSystemMetrics (SM_CYFULLSCREEN);        
	    if (xfull < 900) return;

        etc = getenv ("BWSETC");
        if (etc == NULL)
        {
             etc = "\\user\\fit\\etc";
        }
        sprintf (buffer, "%s\\fit.rct", etc);

        fp = fopen (buffer, "r");
        if (fp == NULL) return;

        if (fgets (buffer, 255, fp) == 0) 
        {
                 fclose (fp);          
                 return;
        }
        if (fgets (buffer, 255, fp) == 0) 
        {
                 fclose (fp);          
                 return;
        }
        fclose (fp);

        anz = wsplit (buffer, " ");
        if (anz < 4) 
        {
            return;
        }

        rect.left   = atoi (wort[0]);
        rect.top    = atoi (wort[1]);
        rect.right  = atoi (wort[2]);
        rect.bottom = atoi (wort[3]);
		rect.left ++; 
		rect.top ++; 
		rect.right  = rect.right  - rect.left - 2;
		rect.bottom = rect.bottom - rect.top - 2;
        MoveWindow (hMainWindow, rect.left, rect.top, rect.right, rect.bottom, TRUE);
        return;
}

void SetcButtons (COLORREF Col, COLORREF BkCol)
/**
Farbe der Button setzen.
**/
{
	    int i;

		for (i = 0; cButtons[i]; i ++)
		{
                 cButtons[i]->Color   = Col;
				 cButtons[i]->BkColor = BkCol;
		}
}

void SaveRect (void)
/**
Aktuelle Windowgroesse sichern.
**/
{
	    char *etc;
		char rectname [512];
		RECT rect;
		FILE *fp;

		etc = getenv ("BWSETC");
		if (etc == NULL) return;

		sprintf (rectname, "%s\\%s.rct", etc,Programm);
		GetWindowRect (hMainWindow, &rect);

		fp = fopen (rectname, "w");
		fprintf (fp, "left    %d\n", rect.left);
		fprintf (fp, "top     %d\n", rect.top);
		fprintf (fp, "right   %d\n", rect.right - rect.left);
		fprintf (fp, "bottom  %d\n", rect.bottom - rect.top);
		fclose (fp);
}


void MoveRect (void)
/**
Fenster auf gesicherte Daten anpassen.
**/
{
	    char *etc;
		char buffer [512];
		RECT rect;
		FILE *fp;
		int x, y, cx, cy;
		int anz;

		etc = getenv ("BWSETC");
		if (etc == NULL) return;

		GetWindowRect (hMainWindow, &rect);
		x = rect.left;
		y = rect.top;
		cx = rect.right;
		cy = rect.bottom;
		sprintf (buffer, "%s\\%s.rct", etc,Programm);
		fp = fopen (buffer, "r");
		if (fp == NULL) return;
		while (fgets (buffer, 511, fp))
		{
			cr_weg (buffer);
			anz = wsplit (buffer, " ");
			if (anz < 2) continue;
			if (strcmp (wort[0], "left") == 0)
			{
				x = atoi (wort[1]);
			}
			else if (strcmp (wort[0], "top") == 0)
			{
				y = atoi (wort[1]);
			}
			else if (strcmp (wort[0], "right") == 0)
			{
				cx = atoi (wort[1]);
			}
			else if (strcmp (wort[0], "bottom") == 0)
			{
				cy = atoi (wort[1]);
			}
		}
		fclose (fp);
		MoveWindow (hMainWindow, x, y, cx, cy, TRUE);
}

BOOL TestMenue (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (LOWORD (wParam) == KEY5)
        {
            SendMessage (hMainWindow, WM_COMMAND, KEY5, 0l);
            return TRUE;
        }
        return FALSE;
}

int    PASCAL WinMain(HANDLE hInstance,HANDLE hPrevInstance,
                        LPSTR lpszCmdLine,int nCmdShow)
{
       int anz; 
       int i;
       char argtab[20][80];
       char *varargs[20];
	   char *mv;
	   char *tr;
    
	   GetForeground ();   
	   SetcButtons (BLACKCOL, LTGRAYCOL);

	   ColBorder = TRUE;
       tr = getenv_default ("COLBORDER");
	   if (tr)
	   {
			ColBorder = max(0, min (1, atoi (tr)));
	   }
	   ArrDown = LoadBitmap (hInstance, "ARRDOWN");
	   iarrdown.SetFeldPtr ((char *) &ArrDown);
       opendbase ("bws");
       menue_class.SetSysBen ();
	   GetCfgValues ();
	   mv = getenv_default ("MOVEMAIN");
	   if (mv)
	   {
		   MoveMain = min (1, max (0, atoi (mv)));
	   }

       memcpy (&sys_par, &sys_par_null,  sizeof (struct SYS_PAR));
       anz = wsplit (lpszCmdLine, " ");
       for (i = 0; i < anz; i ++)
       {
            strcpy (argtab[i], wort[i]);
            varargs[i] = (char *) argtab[i];
       }
       argtst (&anz, varargs, tst_arg);

       SetStdProc (WndProc);
       InitFirstInstance (hInstance);
       InitNewInstance (hInstance, nCmdShow);


       if (WithToolBar)
       {
	               hwndTB = MakeToolBarEx (hInstance, 
		                       hMainWindow,tbb, 53 ,
                               qInfo, qIdfrom,
		                       qhWndInfo, qhWndFrom);

                   hwndCombo1 = MakeToolBarCombo (hInstance,
                                      hMainWindow,
                                      hwndTB, 
                                      10, 
                                      11, 
                                      32);

                   FillCombo1 ();
                   ComboToolTip (hwndTB, hwndCombo1);
 
                   hwndCombo2 = MakeToolBarCombo (hInstance,
                                                  hMainWindow,
                                                  hwndTB, 
                                                  11, 
                                                  33, 
                                                  52);

                   FillCombo2 ();
                   ComboToolTip (hwndTB, hwndCombo2);


                   aufpListe.SethwndTB (hwndTB);
                   DisableTB ();
       }

       if (NewStyle)
       {
             ftasten = OpenFktEx (hInstance);
             ITEM::SetHelpName ("51100.cmd");
             CreateFktBitmap (hInstance, hMainWindow, hwndTB);
             SetFktMenue (TestMenue);
       }
       else
       {
             ftasten = OpenFktM (hInstance);
       }
       Mess.OpenMessage ();
       Createmamain1 ();
       MoveMamain ();
       if (WithMenue)
       {
	               hMenu = MakeMenue (menuetab);
	               SetMenu (hMainWindow, hMenu);
                   aufpListe.SetMenu (hMenu);
       }
	   if (Startsize == 1)
	   {
	               ShowWindow (hMainWindow, SW_SHOWMAXIMIZED);
	   }
	   else if (Startsize == 2)
	   {
	               MoveRect ();
	   }

       SetEnvFont ();
	   GetStdFont (&BuFont);
       aufpListe.SetListFont (&lFont);


       telefon1     = LoadIcon (hMainInst, "tele1");
       fax          = LoadIcon (hMainInst, "fax");

       btelefon       = LoadBitmap (hInstance, "btele");
       btelefoni      = LoadBitmap (hInstance, "btelei");
       CTelefon.bmp   = btelefoni;

       display_form (mamain1, &buform, 0, 0);
       MoveButtons ();
       CreateQuikInfos ();


       CheckMenuItem (hMenu, IDM_PAGEVIEW, MF_UNCHECKED); 
       ToolBar_PressButton(hwndTB, IDM_PAGE, FALSE);
       ToolBar_PressButton(hwndTB, IDM_LIST, TRUE);

       aufpListe.SethMainWindow (mamain1);
//       EingabeMdnFil ();
 
       StartMenu ();

//       ProcessMessages ();
	   SaveRect ();
       SetForeground ();
	   closedbase ();
       return 0;
}


void InitFirstInstance(HANDLE hInstance)
{
        
        WNDCLASS wc;
		HDC hdc;
		int col;

		col = 0;
		hdc = GetDC (NULL);
		col = GetDeviceCaps (hdc, BITSPIXEL);
		ReleaseDC (NULL, hdc);
        if (col < 16) ColBorder = FALSE;

		SetEnvFont ();

		SetSWEnvFont ();

		strcpy (lFont.FontName, FontNameSW);
        lFont.FontHeight = FontHeightSW;
        lFont.FontWidth  = FontWidthSW;       
        lFont.FontAttribute  = FontAttributeSW;       


		if (hbrBackground == NULL)
		{
			hbrBackground = CreateSolidBrush (StdBackCol);
		}


        wc.style         =  CS_BYTEALIGNWINDOW
                            | CS_CLASSDC;
        wc.lpfnWndProc   =  WndProc;
        wc.cbClsExtra    =  0;
        wc.cbWndExtra    =  0;
        wc.hInstance     =  hInstance;
        wc.hIcon         =  LoadIcon (hInstance, "FITICON");
        wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
//        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
//        wc.hbrBackground =  CreateSolidBrush (StdBackCol);
        wc.hbrBackground =  hbrBackground;
        wc.lpszMenuName  =  "";
        wc.lpszClassName =  "hStdWindow";

        RegisterClass(&wc);

        wc.hIcon         =  LoadIcon (hInstance, "NOICON");
        wc.lpszMenuName  =  NULL;
        wc.lpszClassName =  "ListMain";
        RegisterClass(&wc);


        wc.hbrBackground =  GetStockObject (WHITE_BRUSH);
        wc.lpszClassName =  "hListWindow";
        RegisterClass(&wc);

        wc.lpfnWndProc   =  StaticWProc;
        wc.hIcon         =  LoadIcon (hInstance, "NOICON");
        wc.lpszMenuName  =  NULL;
        wc.lpszClassName =  "StaticWhite";
        RegisterClass(&wc);

        wc.lpfnWndProc   =  StaticWProc;
        wc.hIcon         =  LoadIcon (hInstance, "NOICON");
        wc.lpszMenuName  =  NULL;
        wc.hbrBackground =  GetStockObject (GRAY_BRUSH);
        wc.lpszClassName =  "StaticGray";
        RegisterClass(&wc);

        wc.lpfnWndProc   =  WndProc;
        wc.hIcon         =  LoadIcon (hInstance, "NOICON");
        wc.lpszMenuName  =  NULL;
        wc.hbrBackground =  CreateSolidBrush (MessBkCol);
        wc.lpszClassName =  "StaticMess";
        RegisterClass(&wc);
        hMainInst = hInstance;
        return;
}


BOOL InitNewInstance(HANDLE hInstance, int nCmdShow)
{
        HDC hdc;
        HFONT hFont, oldFont;
        char *Caption;
        SIZE size;

        hdc = GetDC (NULL);
        hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
        oldFont = SelectObject (hdc,hFont);
        GetTextMetrics (hdc, &tm);
        GetTextExtentPoint32 (hdc, "X", 1, &size);
        tm.tmAveCharWidth = size.cx;
        ReleaseDC (0, hdc);
        aufpListe.SetTextMetric (&tm);
        if (InfoCaption[0])
        {
            Caption = InfoCaption;
        }
        else
        {
            Caption = "Standardauftr�ge bearbeiten";
        }

        SetBorder (WS_THICKFRAME | WS_CAPTION | 
                   WS_SYSMENU | WS_MINIMIZEBOX |
                   WS_MAXIMIZEBOX);

        hMainWindow = OpenWindowChC (26, 80, 2, 0, hInstance, 
                      Caption);
        return 0; 
}


static int ProcessMessages(void)
{
        MSG msg;

        while (GetMessage (&msg, NULL, 0, 0))
        {
             if (msg.message == WM_KEYDOWN)
             {
                       switch (msg.wParam)
                       {
                              case VK_F5 :
                                  syskey = KEY5;
                                  PostQuitMessage (0);
                                  continue; ;
                              case VK_RETURN :
                                  syskey = KEYCR;
                                  EingabeMdnFil ();
                                  continue; ;
                       }
             }
             TranslateMessage(&msg);
             DispatchMessage(&msg);
        }
        return msg.wParam;
}

void InvalidateLines (void)
{
         RECT rect;
         static TEXTMETRIC tm;
         HDC hdc;

         return;

         hdc = GetDC (hMainWindow);
         GetTextMetrics (hdc, &tm);
         ReleaseDC (hMainWindow, hdc);
         GetClientRect (hMainWindow, &rect);

         rect.top = 2 * tm.tmHeight;
         rect.top = rect.top + rect.top / 3;
         rect.top -= 14;
         rect.bottom = rect.top + 10;
         InvalidateRect (hMainWindow, &rect, TRUE);
}


void PrintLines (HDC hdc)
/**
Linien am Bildschirm anzeigen.
**/
{
         static TEXTMETRIC tm;
         static HPEN hPenG = NULL;
         static HPEN hPenW = NULL;
         static HPEN hPenB = NULL;
         RECT rect;
		 RECT wrect;
         int x, y;

         GetTextMetrics (hdc, &tm);
         y = 2 * tm.tmHeight;
         y = y + y / 3;
		 y -= 14;
         GetClientRect (mamain1, &rect);
         GetWindowRect (mamain1, &wrect);
         x = rect.right - 2;
         if (hPenG == NULL)
         {
                   hPenG = CreatePen (PS_SOLID, 0, GRAYCOL);
                   hPenW = CreatePen (PS_SOLID, 0, WHITECOL);
                   hPenB = CreatePen (PS_SOLID, 0, BLACKCOL);
         }

/* Linie oben                 */

         SelectObject (hdc, hPenG);
         MoveToEx (hdc, 0, y, NULL);
         LineTo (hdc, x + 2, y);

         y ++;
         SelectObject (hdc, hPenW);
         MoveToEx (hdc, 0, y, NULL);
         LineTo (hdc, x + 2, y);

/* Linie mitte                */
/*         
		 y += 6 * tm.tmHeight;
		 y += tm.tmHeight / 2;
         SelectObject (hdc, hPenG);
         MoveToEx (hdc, 0, y, NULL);
         LineTo (hdc, x + 2, y);

         y ++;
         SelectObject (hdc, hPenW);
         MoveToEx (hdc, 0, y, NULL);
         LineTo (hdc, x + 2, y);
		 y -= tm.tmHeight / 2;
*/

/* Linie unten                */
         
		 y += 6 * tm.tmHeight;
         SelectObject (hdc, hPenG);
         MoveToEx (hdc, 0, y, NULL);
         LineTo (hdc, x + 2, y);

         y ++;
         SelectObject (hdc, hPenW);
         MoveToEx (hdc, 0, y, NULL);
         LineTo (hdc, x + 2, y);

		 y += (1 + wrect.top);
		 aufpListe.SetLiney (y);
         aufpListe.MoveMamain1 ();
}

void DisplayLines ()
{
         HDC hdc;

         hdc = GetDC (mamain1);
         PrintLines (hdc);
         ReleaseDC (mamain1, hdc);
}

void ChoiseCombo1 (void)
/**
Aktuell gewaehlten eintrag in Combobox 1 ermitteln.
**/
{
          int i;
          char text [80];

          GetWindowText (hwndCombo1,
                         text,
                         79);
          clipped (text);
          for (i = 0; Combo1[i]; i ++)
          {
              if (strcmp (Combo1[i],text) == 0)
              {
                   aufpListe.SetListLines (i);
                   break;
              }
          }
}

void ChoiseCombo2 (void)
/**
Aktuell gewaehlten eintrag in Combobox 1 ermitteln.
**/
{
          int i;
          char text [80];

          GetWindowText (hwndCombo2,
                         text,
                         79);
          clipped (text);
          for (i = 0; Combo2[i]; i ++)
          {
              if (strcmp (Combo2[i],text) == 0)
              {
                   aufpListe.SetColors (Colors[i], BkColors[i]);
                   break;
              }
          }
}

void disp_forms (void)
{
      if (aufform.mask[0].feldid)  display_form (mamain1, &aufform, 0, 0);
}



LONG FAR PASCAL WndProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        HDC hdc;

        switch(msg)
        {
              case WM_PAINT :
                      if (hWnd == mamain1)
                      {
                              hdc = BeginPaint (hWnd, &aktpaint);
                              PrintLines (hdc);
                              EndPaint (hWnd, &aktpaint);
                      }
                      else 
                      {
                              aufpListe.OnPaint (hWnd, msg, wParam, lParam);
                      }
                      break;
              case WM_MOVE :
                      if (hWnd == hMainWindow)
                      {
                              aufpListe.MoveMamain1 ();
                      }
                      else if (hWnd == mamain1)
                      {
                              aufpListe.MoveMamain1 ();
                      }
                      else
                      {
                              aufpListe.OnSize (hWnd, msg, wParam, lParam);
                      }
                      break;
              case WM_SIZE :
                      if (hWnd == hMainWindow)
                      {
                              MoveFkt ();
                              Mess.MoveMess ();
                              MoveMamain1 ();  
                              hdc = GetDC (hMainWindow);
                              ReleaseDC (hMainWindow, hdc);
                      }
                      else if (hWnd == mamain1)
                      {
						      InvalidateLines ();
                              aufpListe.MoveMamain1 ();
                      }
                      else
                      {
                              aufpListe.OnSize (hWnd, msg, wParam, lParam);
                      }
                      break;

              case WM_SYSCOMMAND:
                      if (hWnd == aufpListe.GetMamain1 ()
                          && wParam == SC_MINIMIZE)
                      {
                                   aufpListe.SetMin ();

                      }
                      else if (hWnd == aufpListe.GetMamain1 ()
                               && wParam == SC_MAXIMIZE)
                      {
                                   aufpListe.SetMax ();
                                   aufpListe.MaximizeMamain1 ();
                                   return 0;

                      }
                      else if (hWnd == aufpListe.GetMamain1 ()
                               && wParam == SC_RESTORE)
                      {
                                   aufpListe.InitMax ();
                                   aufpListe.InitMin ();
                                   ShowWindow (aufpListe.GetMamain1 (), 
                                               SW_SHOWNORMAL);
                                   aufpListe.MoveMamain1 ();
                                   return 0;
                      }
                      else if (hWnd == aufpListe.GetMamain1 ()
                               && wParam == SC_CLOSE)
                      {
                                   if (aufpListe.IsListAktiv ())
                                   {
                                           syskey = KEY5;
                                           SendKey (VK_F5);
                                   }
                                   return 0;
                      }
                      else if (hWnd == hMainWindow && wParam == SC_CLOSE)
                      {
						           if (NoClose)
								   {
						                    disp_mess (
												"Abbruch durch Windowmanager ist nicht m�glich", 
												2);
								            return 0;
								   }
                                   if (abfragejn (hMainWindow, 
                                                  "Bearbeitung abbrechen ?",
                                                  "N") == 0)
                                   {
                                           return 0;
                                   }
                                   rollbackwork ();
								   if (lutz_mdn_par)
								   {
									   sprintf (mdn, "%hd", 0);
									   sprintf (fil, "%hd", 0);
								   }

	                               SetForeground ();
								   SaveRect ();
	                               closedbase ();
                                   ExitProcess (0);
                                   break;
                                   
                      }
                      break;

              case WM_HSCROLL :
                       aufpListe.OnHScroll (hWnd, msg,wParam, lParam);
                       break;
                      
              case WM_VSCROLL :
                       aufpListe.OnVScroll (hWnd, msg, wParam, lParam);
                       break;

              case WM_NOTIFY :
                    {
                      LPNMHDR pnmh = (LPNMHDR) lParam;

                      if (pnmh->code == TTN_NEEDTEXT)
                      {
                           LPTOOLTIPTEXT lpttt = (LPTOOLTIPTEXT) lParam;
                           if (QuickCpy (lpttt->szText, lpttt->hdr.idFrom)
                               == FALSE)
                           {
                                    QuickHwndCpy (lpttt);
                           }
                      }
                      break;
                    }
	
              case WM_DESTROY :
 				      OnDestroy (hWnd); 
                      if (hWnd == hMainWindow)
                      {
                             PostQuitMessage (0);
                             return 0;
                      }
                      break;
              case WM_KEYDOWN :
                      if (aufpListe.GetMamain1 ())
                      {
                               aufpListe.FunkKeys (wParam, lParam);
                      }
                      break;
                  
              case WM_COMMAND :
                    if (LOWORD (wParam) == IDM_EXIT)
                    {
					        if (NoClose)
                            {
						          disp_mess (
										"Abbruch durch Windowmanager ist nicht m�glich", 
										2);
							      return 0;
							 }
                             if (abfragejn (hMainWindow, 
                                            "Bearbeitung abbrechen ?",
                                            "N") == 0)
                            {
                                  return 0;
                            }
                            rollbackwork ();
							if (lutz_mdn_par)
							{
							      sprintf (mdn, "%hd", 0);
							      sprintf (fil, "%hd", 0);
                            }
	                        SetForeground ();
	 			            SaveRect ();
	                        closedbase ();
                            ExitProcess (0);
                            break;
                    }

                    if (LOWORD (wParam) == KEYESC)
                    {
                            syskey = KEYESC;
                            SendKey (VK_ESCAPE);
                            break;
                    }
                    if (LOWORD (wParam) == KEY2)
                    {
                            syskey = KEY2;
                            SendKey (VK_F2);
                            break;
                    }
                    if (LOWORD (wParam) == KEY5)
                    {
                            syskey = KEY5;
                            SendKey (VK_F5);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY6)
                    {
                            syskey = KEY6;
                            SendKey (VK_F6);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY7)
                    {
                            syskey = KEY7;
                            SendKey (VK_F7);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY8)
                    {
                            syskey = KEY8;
                            SendKey (VK_F8);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY9)
                    {
                            syskey = KEY9;
                            SendKey (VK_F9);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY10)
                    {
                            syskey = KEY10;
                            SendKey (VK_F10);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY11)
                    {
                            syskey = KEY11;
                            SendKey (VK_F11);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY12)
                    {
                            syskey = KEY12;
                            SendKey (VK_F12);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYPGD)
                    {
                            syskey = KEYPGD;
                            SendKey (VK_NEXT);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYPGU)
                    {
                            syskey = KEYPGU;
                            SendKey (VK_PRIOR);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYDOWN)
                    {
                            syskey = KEYDOWN;
                            SendKey (VK_DOWN);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYUP)
                    {
                            syskey = KEYUP;
                            SendKey (VK_UP);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYLEFT)
                    {
                            syskey = KEYLEFT;
                            SendKey (VK_LEFT);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYRIGHT)
                    {
                            syskey = KEYRIGHT;
                            SendKey (VK_RIGHT);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYSTAB)
                    {
                            syskey = KEYSTAB;
                            SendKey (VK_LEFT);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYTAB)
                    {
                            syskey = KEYTAB;
                            SendKey (VK_TAB);
                            break;
                    }
                    else if (LOWORD (wParam) == IDM_VINFO)
                    {
						    InfoVersion (); 
                            break;
                    }
                   else if (LOWORD (wParam) == QUERYKUN)
				   {
					        if (atoi (kunfil))
							{
								ShowFil ();
							}
							else
							{
					            QueryKun ();
							}
				   }
                   else if (LOWORD (wParam) == QUERYAUFKZ)
				   {
                            SetCurrentFocus (GetItemPos (&aufform, "stnd_auf_kz")); 
					        ShowPtab ();
				   }
				   else if (LOWORD (wParam) == IDM_FRAME)
                   {
						    CheckMenuItem (hMenu, ActiveMark,
								           MF_UNCHECKED); 
						    CheckMenuItem (hMenu, IDM_FRAME,
								           MF_CHECKED); 
							ActiveMark = IDM_FRAME;
                            syskey = KEY3;
                            SendKey (VK_F3);
                   }
				   else if (LOWORD (wParam) == IDM_REVERSE)
                   {
						    CheckMenuItem (hMenu, ActiveMark,
								           MF_UNCHECKED); 
						    CheckMenuItem (hMenu, IDM_REVERSE,
								           MF_CHECKED); 
							ActiveMark = IDM_REVERSE;
                            syskey = KEY4;
                            SendKey (VK_F4);
                   }
				   else if (LOWORD (wParam) == IDM_NOMARK)
                   {
						    CheckMenuItem (hMenu, ActiveMark,
								           MF_UNCHECKED); 
						    CheckMenuItem (hMenu, IDM_NOMARK,
								           MF_CHECKED); 
							ActiveMark = IDM_NOMARK;
                            syskey = KEY6;
                            SendKey (VK_F6);
                   }
				   else if (LOWORD (wParam) == IDM_EDITMARK)
                   {
						    CheckMenuItem (hMenu, ActiveMark,
								           MF_UNCHECKED); 
						    CheckMenuItem (hMenu, IDM_EDITMARK,
								           MF_CHECKED); 
							ActiveMark = IDM_EDITMARK;
                            syskey = KEY7;
                            SendKey (VK_F7);
                   }
				   else if (LOWORD (wParam) == IDM_PAGEVIEW)
                   {
                            if (aufpListe.GetRecanz () < 1) break;
                            if (PageView)
                            {
						            CheckMenuItem (hMenu, IDM_PAGEVIEW,
								                   MF_UNCHECKED); 
                                    ToolBar_PressButton(hwndTB, IDM_PAGE, FALSE);
                                    ToolBar_PressButton(hwndTB, IDM_LIST, TRUE);
                                    ToolBar_SetState(hwndTB, IDM_PAGE, TBSTATE_ENABLED);
                                    PageView = 0;
                                    aufpListe.SwitchPage0 (aufpListe.GetAktRowS ());
                            }
                            else
                            {
						            CheckMenuItem (hMenu, IDM_PAGEVIEW,
								                   MF_CHECKED); 
                                    ToolBar_PressButton(hwndTB, IDM_PAGE, TRUE);
                                    ToolBar_PressButton(hwndTB, IDM_LIST, FALSE);
                                    ToolBar_SetState(hwndTB, IDM_LIST, TBSTATE_ENABLED);
                                    PageView = 1;
                                    aufpListe.SwitchPage0 (aufpListe.GetAktRow ());
                            }
                            SendMessage (aufpListe.Getmamain2 (), WM_SIZE, NULL, NULL);
                            InvalidateRect (aufpListe.Getmamain3 (), 0, TRUE);
                   }
 				   else if (LOWORD (wParam) == IDM_PAGE)
                   {
                            PageView = 1; 
			                CheckMenuItem (hMenu, IDM_PAGEVIEW,
								                   MF_CHECKED); 
                            ToolBar_PressButton(hwndTB, IDM_PAGE, TRUE);
                            ToolBar_PressButton(hwndTB, IDM_LIST, FALSE);
                            aufpListe.SwitchPage0 (aufpListe.GetAktRow ());
                            SendMessage (aufpListe.Getmamain2 (), WM_SIZE, NULL, NULL);
                            InvalidateRect (aufpListe.Getmamain3 (), 0, TRUE);
                   }
 				   else if (LOWORD (wParam) == IDM_LIST)
                   {
                            PageView = 0; 
			                CheckMenuItem (hMenu, IDM_PAGEVIEW,
								                   MF_UNCHECKED); 
                            ToolBar_PressButton(hwndTB, IDM_LIST, TRUE);
                            ToolBar_PressButton(hwndTB, IDM_PAGE, FALSE);
                            aufpListe.SwitchPage0 (aufpListe.GetAktRowS ());
                            SendMessage (aufpListe.Getmamain2 (), WM_SIZE, NULL, NULL);
                            InvalidateRect (aufpListe.Getmamain3 (), 0, TRUE);
                   }
			       else if (LOWORD (wParam) == IDM_WORK)
                   {
                             EingabeMdnFil ();
                   }
			       else if (LOWORD (wParam) == IDM_DEL)
                   {
                             DelAuf ();
                   }
			       else if (LOWORD (wParam) == IDM_PRINT)
                   {
                             PrintAuf ();
                   }

                   if (HIWORD (wParam) == CBN_CLOSEUP)
                   {
                                if (lParam == (LPARAM) hwndCombo1)
                                {
                                   ChoiseCombo1 ();
                                }
                                if (lParam == (LPARAM) hwndCombo2)
                                {
                                   ChoiseCombo2 ();
                                }

                                if (IsDlgCombobox ((HWND) lParam))
                                {
                                      return 0;
                                }

                                if (aufpListe.Getmamain3 ())
                                {
                                      aufpListe.SetListFocus ();
                                }
                                else
                                {
                                    SetCurrentFocus (currentfield);
                                }
                    }
					else if (LOWORD (wParam) == IDM_FONT)
                    {
                                  aufpListe.ChoiseFont (&lFont);
                                  return 0;
                                   
                    }
					else if (LOWORD (wParam) == IDM_BASIS)
                    {
                                  aufpListe.ShowBasis ();
                                  return 0;
                                   
                    }
					else if (LOWORD (wParam) == IDM_BASIS)
                    {
                                  aufpListe.ShowBasis ();
                                  return 0;
                                   
                    }
                    else if (LOWORD (wParam) == IDM_INFO)
                    {
                                  syskey = KEY4;
                                  SendKey (VK_F4);
                    }
                    else if (LOWORD (wParam) == CallT1)
                    {
                            CallTele1 ();
                            break;
                    }
                    else if (LOWORD (wParam) == CallT2)
                    {
                            CallTele2 ();
                            break;
                    }
                    else if (LOWORD (wParam) == CallT3)
                    {
                            CallTele3 ();
                            break;
                    }
                    else if (LOWORD (wParam) == IDM_VKPR)
                    {
					        aufpListe.EnterPrVk ();
                            break;
                    }
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

LONG FAR PASCAL StaticWProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

