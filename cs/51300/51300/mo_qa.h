#ifndef _MO_AQDEF
#define _MO_AQDEF

class QueryClass
{
private :
public :
	    QueryClass ()
        {
        }
        int querya (HWND,int kun_fil);
        int searcha (HWND hWnd,int kun_fil);
        int querykun (HWND);
        int searchkun_direct (HWND, char *);
        int querya_direct (HWND, char *,int kun_fil);
        int searcha_direct (HWND, char *,int kun_fil);
};
#endif
