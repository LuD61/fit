#include<stdio.h>
#include<stdarg.h>
#include<string.h>
#include<windows.h>
#include<winbase.h>
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_draw.h"
#include "wmaskc.h"
#include "mo_meld.h"
#include "cmask.h"
#include "colbut.h"
#include "mo_choise.h" 
#include "mo_gdruck.h" 

extern BOOL ColBorder;

#define BuOk 1001
#define BuCancel 1002
#define BuUp 760
#define BuDown 761
#define PLBOX 600

#define STDSIZE 120 


static BOOL breakchoise = FALSE;
static BOOL destroyed = FALSE;
static int StdSize = STDSIZE;
static BOOL GraphikMode = FALSE;

static mfont textfont = {"MS SANS SERIF", StdSize, 0, 0,
                             YELLOWCOL,
                             GRAYCOL,
                             0,
                             NULL};

static mfont drkfont = {"MS SANS SERIF", StdSize - 20, 0, 0,
                             YELLOWCOL,
                             GRAYCOL,
                             1,
                             NULL};

static mfont buttonfont = {"MS SANS SERIF", StdSize, 0, 0,
                           RGB (0, 255, 255),
                           BLUECOL,
                           0,
                           NULL};

static mfont rdofont  = {"MS SANS SERIF", StdSize, 0, 0,
                           BLACKCOL,
                           LTGRAYCOL,
                           0,
                           NULL};

static mfont ubfont = {"MS SANS SERIF", 100, 0, 1,
                             BLUECOL,
                             GRAYCOL,
                             0,
                             NULL};

static mfont scrollfont = { "", 100, 0, 1,
                            BLUECOL,
                            BLUECOL,
                            1,
                            NULL};


ColButton ScrollUp    =    {NULL, -1, -1,
                            NULL, 0, 0,
                            NULL, 0, 0,
                            NULL, -1, -1,
                            NULL, 0, 0,
                            RGB (0, 255, 255),
                            BLUECOL,
                            14};

ColButton CTast   = {"0",  -1, -1, 
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     RGB (0, 255, 255),
                     BLUECOL,
                     0};

ColButton CTastC  = {"0",  -1, -1, 
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
//                     BLACKCOL,
                     RGB (0, 255, 255),
                     REDCOL,
                     0};

ColButton ArrUp = {"",  -1, -1,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, -1, -1,
                     NULL, 0, 0,
                     BLACKCOL,
                     LTGRAYCOL,
                     10};

ColButton ArrDown = {"",  -1, -1,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, -1, -1,
                     NULL, 0, 0,
                     BLACKCOL,
                     LTGRAYCOL,
                     10};

ColButton **CTasten;

static CFIELD **_fCButton;
static CFORM *fCButton;
static int buanz = 5;

static CFIELD **_fButton;
static CFORM *fButton;

static CFIELD **_fButton1;
static CFORM *fButton1;

static CFIELD **_fWork;
static CFORM *fWork;

static CFIELD **_fPlist;
static CFORM *fPlist;

static char AktPrinter [80] = {""};
static char ZuPrinter [80] = {""};
static  char **Namen = NULL;
static  char **lnamarr;
static  int lnamanz;
static HWND ListhWnd = NULL;
static int FrmPtr = 0;

CHOISE::CHOISE (int x, int y, int cx, int cy, 
				char *Caption, int Size, char *TextL,char *TextR)
{
			TEXTMETRIC tm;
			HFONT hFont, oldfont;
			SIZE size;
			HDC hdc;
            int i;
            int bx,by, bcx, bcy;
            char name [5];
			int cxs;
			int xfull, yfull;

 	        this->Font = &buttonfont;  

			if (Size > 0) 
			{
				StdSize = Size;
            }
 		    if (StdSize > 150) StdSize = 150;
            xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
            yfull = GetSystemMetrics (SM_CYFULLSCREEN);        

		    if (xfull < 1000) StdSize -= 20;

            textfont.FontHeight = StdSize;
            drkfont.FontHeight = StdSize - 20;
            buttonfont.FontHeight = StdSize;

			this->Caption = NULL;
			if (Caption && Caption[0])
			{
			        this->Caption = Caption; 
			}
			hdc = GetDC (NULL);
            hFont = SetDeviceFont (hdc, Font, &tm);
            oldfont = SelectObject (hdc, hFont);
	        GetTextMetrics (hdc, &tm);
			GetTextExtentPoint32 (hdc, "X", 1, &size);
			DeleteObject (SelectObject (hdc, oldfont));
	

			this->cx = size.cx * cx;
			this->cy = (int) (double) ((double) tm.tmHeight * cy * 1.3);

			this->x = size.cx * x;
			this->y = (int) (double) ((double) tm.tmHeight * y * 1.3);

            bx  = -1;
            by  =  3;
            bcx = 20;
            bcy =  2;

			buanz = 0;
			CTasten   = new ColButton * [buanz];
            _fCButton    = new CFIELD * [buanz];
            for (i = 0; i < buanz; i ++)     
            {
                    sprintf (name, "BU%d", i);
		  	        CTasten[i]  = new ColButton; 
			        memcpy (CTasten[i], &CTast, sizeof (ColButton));
     			    _fCButton[i] = new CFIELD (name, (ColButton *)CTasten[i], bcx, bcy, bx, by, 
				                                  NULL, "", CCOLBUTTON,
                                                  500 + i, Font, 0, 0);
					by += 2;
			}
            fCButton = new CFORM (buanz, _fCButton);

			x = 0;
			if (this->Caption)
			{
			         y = this->cy - 4 * tm.tmHeight;
			}
			else
			{
			         y = this->cy - 3 * tm.tmHeight;
			}
			cx = 12 * size.cx;
			cxs = 4 * size.cx;
			cy = (int) (double) ((double) tm.tmHeight * 2);
			
			_fButton    = new CFIELD * [2];
/*
			_fButton[0] = new CFIELD ("OK", "OK", cx, cy, x, y, 
				                                  NULL, "", CBUTTON,
                                                  BuOk, Font, 1, BS_PUSHBUTTON);				                                   
*/
 
			_fButton[0] = new CFIELD ("Cancel", "Abbruch", 
				                                  cx , cy, x * size.cx, y,
				                                  NULL, "", CBUTTON,
                                                  BuCancel, Font, 1, BS_PUSHBUTTON);				    
			fButton     = new CFORM (1, _fButton);

            _fWork    = new CFIELD * [3];
			_fWork[0] = new CFIELD ("text", (char *) TextL, 0, 0, -1, 0,
				              NULL, "", CDISPLAYONLY,
							  600, &textfont, 0, 0);
			_fWork[1] = new CFIELD ("drk_zu", ZuPrinter, 
				                                         30,0, -1, 1, NULL, "", CREADONLY,
                                                  982, &rdofont, 0, 0);
	//_fWork[2] = new CFIELD ("CButton", fCButton, 0, 0, 0, 0, NULL, "", CFFORM,
	//	                                 980, Font, 0, 0);
			_fWork[2] = new CFIELD ("Button", fButton, 0, 0, -1, 0, NULL, "", CFFORM,
				                                 981, Font, 0, 0);

			fWork   = new CFORM (3, _fWork);
			Texte = NULL;
			textstart = 0;
			textanz   = 0;
			ldruckanz = 0;
			ldruckarr = NULL;
			lnamarr = NULL;
			lnamanz = 0;

			_fButton1    = new CFIELD * [3];
			x = 0;
			_fButton1[0] = new CFIELD ("Up", (ColButton *) &ArrUp, 
				                                  cxs , cy, x, y,
				                                  NULL, "", CCOLBUTTON,
                                                  BuUp, Font, 1, 0);	    
			x = x + cxs + 2 * size.cx;
			_fButton1[1] = new CFIELD ("Wahl", "W�hlen", 
				                                  cx , cy, x, y,
				                                  NULL, "", CBUTTON,
                                                  BuOk, Font, 1, BS_PUSHBUTTON);				    
			x = x + cx + 2 * size.cx;
			_fButton1[2] = new CFIELD ("Down", (ColButton *) &ArrDown, 
				                                  cxs , cy, x, y,
				                                  NULL, "", CCOLBUTTON,
                                                  BuDown, Font, 1, 0);				    
			fButton1     = new CFORM (3, _fButton1);

            _fPlist     = new CFIELD * [4];
			_fPlist[0] = new CFIELD ("drk_text", TextR, 
				                                         0, 0, -1, 0, NULL, "", CDISPLAYONLY,
                                                  984, Font, 0, 0);
			_fPlist[1] = new CFIELD ("drk_wert", AktPrinter, 
				                                         30,0, -1, 1, NULL, "", CREADONLY,
                                                  985, &rdofont, 0, 0);
			_fPlist[2]  = new CFIELD ("Drucker", "Drucker",
				                     30, 12, -1, 3, NULL, "", CLISTBOX,
												    PLBOX, Font, 0, 0);
			_fPlist[3] = new CFIELD ("Button1", fButton1, 0, 0, -1, 0, NULL, "", CFFORM,
				                                 986, Font, 0, 0);

			fPlist      = new CFORM (4, _fPlist);
}

CHOISE::~CHOISE ()
{
        int i;

        fWork->destroy (); 

        for (i = 0; i < buanz; i ++)
        {        
                    delete CTasten [i];
                    delete _fCButton[i];
        }   
        delete CTasten;
        delete _fCButton;
        delete fCButton;   

		delete _fButton[0];
		delete _fButton;
		delete fButton;

		delete _fWork[0];
		delete _fWork[1];
		delete _fWork[2];
//		delete _fWork[3];
		delete _fWork;
		delete fWork;

		delete _fButton1[0];
		delete _fButton1;
		delete fButton1;

		delete _fPlist[0];
		delete _fPlist[1];
		delete _fPlist[2];
		delete _fPlist[3];

		delete _fPlist;
		delete fPlist;
		if (ldruckarr) 
		{
			delete ldruckarr;
		}
		for (i = 0; i < lnamanz; i ++)
		{
			delete lnamarr[i];
		}
		delete lnamarr;
}

void CHOISE::Scroll (BOOL update)
/**
Texte in Button schreiben und bei update = TRUE anzeigen.
**/
{
	    int i, j;

		if (Texte == NULL) return;
		if (buanz== 0) return;

		for (i = textstart,j = 0; Texte[i]; i ++, j ++)
		{
			CTasten[j]->text1 = Texte[i];
		}
		if (update)
		{
			fCButton->Update ();
		}
}


void CHOISE::SetTexte (char **texte, char **namen)
/**
Texte fuer Button setzen.
**/
{

         this->Texte = texte;
         Namen = namen;
		 Scroll (FALSE);
}

void CHOISE::SetCurrentID (DWORD ID)
{
	       if (fWork->SetCurrentID (ID))
		   {
			   currentfield = ID;
		   }
}

void CHOISE::SetCurrentName (char *name)
{
	       if (fWork->SetCurrentName (name))
		   {
			   currentfield = fWork->GetID (name);
		   }
}

void CHOISE::SetCurrentTast (DWORD ID)
{
		int current;
		static int oldcurrent = -1;

	    fCButton->SetCurrentFieldID (ID);
		current = fCButton->GetCurrent ();
		sprintf (ZuPrinter, "  %s", lnamarr[current]);

		if (oldcurrent == current) return;
		if (oldcurrent > -1)
		{
		          CTast.text1 = CTasten[oldcurrent]->text1;
                  memcpy (CTasten[oldcurrent], &CTast, sizeof (ColButton));
	              fCButton->GetCfield () [oldcurrent]->destroy ();
		}

  	    oldcurrent = current;
		CTastC.text1 = CTasten[current]->text1;
        memcpy (CTasten[current], &CTastC, sizeof (ColButton));
	    fCButton->GetCfield () [current]->destroy ();
 	    fWork->Update (1);
	    return;
}

void CHOISE::EnableID (DWORD ID, BOOL flag)
{
	       fWork->EnableID (ID, flag);
}

void CHOISE::EnableName (char *name, BOOL flag)
{
	       fWork->EnableName (name, flag);
}

void CHOISE::CheckID (DWORD ID, BOOL flag)
{
	       fWork->CheckID (ID, flag);
}

void CHOISE::CheckName (char *name, BOOL flag)
{
	       fWork->CheckName (name, flag);
}

BOOL CHOISE::TestButtons (HWND hWndBu)
{
	       int i;

	       if (hWndBu == fWork->GethWndID (BuOk))
		   {
			   SendMessage (hWnd1, WM_COMMAND, MAKELONG (BuOk, VK_RETURN),
				                                    (LPARAM) hWnd); 
			   return TRUE;
		   }
	       if (hWndBu == fWork->GethWndID (BuCancel))
		   {
			   SendMessage (hWnd1, WM_COMMAND, MAKELONG (BuCancel, VK_RETURN),
				                                    (LPARAM) hWnd); 
			   return TRUE;
		   }
		   for (i = 0; i < buanz; i ++)
		   {
			   if (hWndBu == fWork->GethWndID (500 + i))
			   {
//				   print_mess (2, "Button %d gedr�ckt", i);
				   return TRUE;
			   }
		   }
		   return FALSE;
}


void CHOISE::ProcessMessages (void)
{
	      MSG msg;
		  HWND hWnd;
		  static CFORM *AktFrm [] = {fWork, fPlist};

		  FrmPtr = 1;
		  breakchoise = FALSE;
          hWnd = fWork->GethWndID (500);
/*
		  fWork->SetCurrent (2);
		  fCButton->GetCfield () [0]->SetFocus ();
*/
		  fPlist->SetCurrent (2);
          fPlist->SetFirstFocus ();
		  SetCursel (0);
          while (GetMessage (&msg, NULL, 0, 0))
		  {
			  if (breakchoise) break;
			  if (msg.message == WM_KEYDOWN)
			  {
			      if (msg.wParam == VK_TAB)
				  {
/*
                      if (GetKeyState (VK_SHIFT) < 0)
                      {
                              syskey = KEYSTAB;
							  FrmPtr = (FrmPtr + 1) % 2;  
							  if (FrmPtr == 1)
							  {
							          AktFrm[FrmPtr]->SetFirstFocus ();
							  }
							  else
							  {
							          AktFrm[FrmPtr]->SetFocus ();
							  }
					  }
					  else
					  {
					          syskey = KEYTAB;
							  FrmPtr = (FrmPtr + 1) % 2;  
							  if (FrmPtr == 1)
							  {
							          AktFrm[FrmPtr]->SetFirstFocus ();
							  }
							  else
							  {
							          AktFrm[FrmPtr]->SetFocus ();

  
	
	                          
							  }
					  }
					  continue;
*/
				  }
				  else if (msg.wParam == VK_F5)
				  {
					  syskey = KEY5;
					  break;
				  }
				  else if (msg.hwnd == fPlist->GethWndName ("Drucker"));
				  else if (msg.wParam == VK_DOWN)
				  {

					  syskey = KEYDOWN;
                      AktFrm[FrmPtr]->NextField (); 
					  continue;
				  }
				  else if (msg.wParam == VK_UP)
				  {

					  syskey = KEYUP;
                      AktFrm[FrmPtr]->PriorField (); 
					  continue;
				  }
				  else if (msg.wParam == VK_RETURN)
				  {

					  if (TestButtons (msg.hwnd))
					  {
					           continue;
					  }
				  }
			  }
			  else if (msg.message == WM_COMMAND)
			  {
				  if (TestButtons (msg.hwnd))
				  {
					  continue;
				  }
			  }
              TranslateMessage(&msg);
			  DispatchMessage(&msg);
		  }
	      strcpy (ChoiseText, ZuPrinter);
}

DWORD CHOISE::IsTast (DWORD ID)
{

	      if (fCButton->GethWndID (ID))
		  {
			  return ID;
		  }
		  return NULL;
}

void CHOISE::SetPrChoise (HWND hWnd) 
{
	       int idx;
		   char text [512];
		   int anz;

		   idx = SendMessage (hWnd, LB_GETCURSEL, 0, 0l);
		   SendMessage (hWnd, LB_GETTEXT, idx, (LPARAM) text);
           anz = wsplit (text, " ");
		   if (anz > 1)
		   {
			   sprintf (text, "%s  %s", wort[0], wort[1]);
		   }
		   strcpy (AktPrinter, text);
		   fPlist->Update (1);
}

	       
void CHOISE::SetChoise (HWND hWnd) 
{
	       int idx;
		   char text [512];
		   int current;

		   current = fCButton->GetCurrent ();
		   idx = SendMessage (hWnd, LB_GETCURSEL, 0, 0l);
		   SendMessage (hWnd, LB_GETTEXT, idx, (LPARAM) text);
		   strcpy (ZuPrinter, text);
		   fWork->Update (1);
}
	       
void CHOISE::SetChoiseDefault (char *text) 
{
	       ChoiseText = text;
		   strcpy (ZuPrinter, text);
		   fWork->Update (1);
}
	       

CALLBACK CHOISE::CProc (HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        PAINTSTRUCT ps;
		HDC hdc;

        switch(msg)
        {
              case WM_PAINT :
                    hdc = BeginPaint (hWnd, &ps);
                    EndPaint (hWnd, &ps);
					break;
              case WM_DESTROY :
				    breakchoise = TRUE;
				    destroyed = TRUE;
					PostMessage (hWnd, WM_USER, 0, 0l); 
					break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}


CALLBACK CHOISE::CProc1 (HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        PAINTSTRUCT ps;
		HDC hdc;
		DWORD Id;

        switch(msg)
        {
              case WM_PAINT :
                    hdc = BeginPaint (hWnd, &ps);
		            fWork->display (hWnd, hdc);
                    EndPaint (hWnd, &ps);
					break;
              case WM_COMMAND :
                    if (LOWORD (wParam) == BuOk)
                    {
						   syskey = KEYCR;
                           PostQuitMessage (0);
                    }
                    else if (LOWORD (wParam) == BuCancel)
                    {
						   syskey = KEY5;
                           PostQuitMessage (0);
                    }

					else if (Id = IsTast (LOWORD (wParam)))
					{
						   if (HIWORD (wParam) == WM_SETFOCUS)
						   {
							      ListhWnd = NULL;
								  FrmPtr = 0;
                                  SetCurrentTast (Id);
						   }
						   else if (HIWORD (wParam) == WM_KILLFOCUS)
						   {       
	                              SendMessage ((HWND) lParam, WM_DEF, 0, 0l);
						   }
					}
                    break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

CALLBACK CHOISE::CProc2 (HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        PAINTSTRUCT ps;
		HDC hdc;

        switch(msg)
        {
              case WM_PAINT :
                   hdc = BeginPaint (hWnd, &ps);
		           fPlist->display (hWnd, hdc);
                   EndPaint (hWnd, &ps);
				   break;
              case WM_COMMAND :
                   if (LOWORD (wParam) == BuOk)
                   {
	                       SetChoise (fPlist->GethWndID (PLBOX)); 
						   syskey = KEYCR;
                           PostQuitMessage (0);
						   return 0;
                   }
                   else if (LOWORD (wParam) == BuUp)
                   {
					       if (HIWORD (wParam) != WM_LBUTTONDOWN && 
							   HIWORD (wParam) != WM_LBUTTONUP) break; 
					       if (ListhWnd)
						   {
					               PostMessage (ListhWnd, WM_KEYDOWN, VK_UP, 0l);
						   }
						   else
						   {
					               PostMessage (NULL, WM_KEYDOWN, VK_UP, 0l);
						   }
						   return 0;
                   }
                   else if (LOWORD (wParam) == BuDown)           
				   {
					       if (HIWORD (wParam) != WM_LBUTTONDOWN && 
							   HIWORD (wParam) != WM_LBUTTONUP) break; 
					       if (ListhWnd)
						   {
					               PostMessage (ListhWnd, WM_KEYDOWN, VK_DOWN, 0l);
						   }
						   else
						   {
					               PostMessage (NULL, WM_KEYDOWN, VK_DOWN, 0l);
						   }
						   return 0;
                   }
                   else if (LOWORD (wParam) == PLBOX)
				   {
                           if (HIWORD (wParam) == LBN_DBLCLK)
                           {
						          SetChoise ((HWND) lParam); 
						          syskey = KEYCR;
                                  PostQuitMessage (0);
						   }
                           else if (HIWORD (wParam) == VK_RETURN)
                           {
						          SetChoise ((HWND) lParam); 
						          syskey = KEYCR;
                                  PostQuitMessage (0);
						   }
                           else if (HIWORD (wParam) == LBN_SELCHANGE)
                           {
							      SetPrChoise ((HWND) lParam);
		                          fPlist->SetCurrent (2);
						   }
                           else if (HIWORD (wParam) == LBN_SELCHANGE)
                           {
							      SetPrChoise ((HWND) lParam);
		                          fPlist->SetCurrent (2);
						   }
                           else if (HIWORD (wParam) == WM_SETFOCUS)
                           {
								   ListhWnd  = (HWND) lParam;
 								   FrmPtr = 1;
						   }
                           else if (HIWORD (wParam) == WM_KILLFOCUS)
                           {
						   }
				   }
                   break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

void CHOISE::DestroyWindow (void)
{
	    if (destroyed == FALSE)
		{
	       fWork->destroy ();
		   ::DestroyWindow (hWnd);
		}
		hWnd = NULL;
}
						
HWND CHOISE::OpenWindow (HANDLE hInstance, HWND hMainWindow)
{
		  BOOL registered = FALSE;
          WNDCLASS wc;
		  RECT rect, rect1;
		  int xfull, yfull;
		  DWORD style;


  	      ArrUp.bmp   = LoadBitmap (hInstance, "ARRUP");
  	      ArrDown.bmp = LoadBitmap (hInstance, "ARRDOWN");
		  this->hInstance   = hInstance;
		  this->hMainWindow = hMainWindow;
		  SetColBorderM (TRUE);
          xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
          yfull = GetSystemMetrics (SM_CYFULLSCREEN);        

		  if (registered == FALSE)
		  {
                  wc.style =  CS_BYTEALIGNWINDOW | CS_CLASSDC;
                  wc.lpfnWndProc   =  (WNDPROC) CProc;
                  wc.cbClsExtra    =  0;
                  wc.cbWndExtra    =  0;
                  wc.hInstance     =  hInstance;
                  wc.hIcon         =  LoadIcon (hInstance, "FITICON");
                  wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                  wc.hbrBackground =  GetStockObject (GRAY_BRUSH);
                  wc.lpszMenuName  =  "";
                  wc.lpszClassName =  "CChoiseWind0";
                  RegisterClass(&wc);

                  wc.lpfnWndProc   =  (WNDPROC) CProc1;
                  wc.lpszClassName =  "CChoiseWind1";
                  RegisterClass(&wc);

                  wc.lpfnWndProc   =  (WNDPROC) CProc2;
                  wc.lpszClassName =  "CChoiseWind2";

                  RegisterClass(&wc);
				  registered = TRUE;
		  }

		  GetClientRect (hMainWindow, &rect);
		  GetWindowRect (hMainWindow, &rect1);

          if (hMainWindow)
          {
		          this->x  = max (0, (rect.right - cx) / 2)  + rect1.left;
		          this->y  = max (0, (rect.bottom - cy) / 2) + rect1.top;
		  }
		  else
		  {
			  if (this->x < 0)
			  {
		          this->x  = max (0, (xfull - cx) / 2);
			  }
			  if (this->y < 0)
			  {
		          this->y  = max (0, (yfull - cy) / 2);
			  }
		  }

		  if (hMainWindow == NULL)
		  {
			  style = WS_VISIBLE | WS_POPUP | WS_DLGFRAME | WS_CAPTION | WS_SYSMENU
				      | WS_MINIMIZEBOX;
		  }
		  else if (Caption)
		  {
			  style = WS_VISIBLE | WS_POPUP | WS_DLGFRAME | WS_CAPTION;
		  }
		  else
		  {
			  style = WS_VISIBLE | WS_POPUP | WS_DLGFRAME;
		  }

          hWnd = CreateWindowEx (0, 
                                 "CChoiseWind0",
                                 Caption,
                                 style,
                                 x, y,
                                 cx, cy,
                                 hMainWindow,
                                 NULL,
                                 hInstance,
                                 NULL);
          hWnd1 = CreateWindowEx (WS_EX_CLIENTEDGE, 
                                 "CChoiseWind1",
                                 "",
                                 WS_VISIBLE | WS_CHILD, // | WS_DLGFRAME,
                                 0, 0,
                                 cx /2, cy,
                                 hWnd,
                                 NULL,
                                 hInstance,
                                 NULL);
          hWnd2 = CreateWindowEx (WS_EX_CLIENTEDGE, 
                                 "CChoiseWind2",
                                 "",
                                 WS_VISIBLE | WS_CHILD, // | WS_DLGFRAME,
                                 0 + cx / 2, 0,
                                 cx /2, cy,
                                 hWnd,
                                 NULL,
                                 hInstance,
                                 NULL);
		  destroyed = FALSE;
		  ShowWindow (hWnd, SW_SHOWNORMAL);
		  UpdateWindow (hWnd);
		  ShowWindow (hWnd1, SW_SHOWNORMAL);
		  UpdateWindow (hWnd1);
		  ShowWindow (hWnd2, SW_SHOWNORMAL);
		  UpdateWindow (hWnd2);
		  if (ldruckanz)
		  {
		         strcpy (AktPrinter, ldruckarr[0].drucker);
 		         fPlist->Update (1);
		  }
		  VLines ();
		  return hWnd;
}

void CHOISE::MoveWindow (void)
{
		  RECT rect;

		  GetClientRect (hMainWindow, &rect);
          ::MoveWindow (hWnd, rect.left, rect.top, rect.right, rect.bottom, TRUE);


}


void CHOISE::FillListUb (char *Ub)
{
	       HWND hWnd;

		   hWnd = fPlist->GethWndName ("Drucker");
           SendMessage (hWnd, LB_TITLE, -1,  
                                 (LPARAM) (char *) Ub);
		  fPlist->Update (2);
}

void CHOISE::FillListRow (char *text)
{
	       HWND hWnd;

		   hWnd = fPlist->GethWndName ("Drucker");
           SendMessage (hWnd, LB_INSERTSTRING, -1,  
                                 (LPARAM) (char *) text);
		  fPlist->Update (2);
}

void CHOISE::SetCursel (int pos)
{
	      HWND hWnd;

		  hWnd = fPlist->GethWndID (PLBOX);
          SendMessage (hWnd, LB_SETCURSEL, pos, 0L);
	      SetPrChoise (hWnd);
}

void CHOISE::VLines (void)
{
	       HWND hWnd;

		   hWnd = fPlist->GethWndID (PLBOX);
           SendMessage (hWnd, LB_VPOS, 0,  
                                 (LPARAM) (char *) "        ");
}
						

