#ifndef _AKT_IPR_DEF
#define _AKT_IPR_DEF

#include "dbclass.h"

struct AKT_IPR {
   long      mdn;
   long      pr_gr_stuf;
   long      kun_pr;
   double    a;
   double    aki_pr;
   long      aki_von;
   long      aki_bis;
   double    ld_pr;
   short     aktion_nr;
   char      a_akt_kz[2];
   char      modif[2];
   short     waehrung;
   long      kun;
   double    a_grund;
   char      kond_art[5];
};
extern struct AKT_IPR akt_ipr, akt_ipr_null;

#line 7 "akt_ipr.rh"

class AKT_IPR_CLASS : public DB_CLASS 
{
       private :
               int cursor_a;
               int cursor_gr;
               int del_a_curs;
               int del_gr_curs; 
               void prepare (void);
               void prepare_a (char *);
               void prepare_gr (char *);
       public :
               AKT_IPR_CLASS () : DB_CLASS (),
                                  cursor_a (-1) ,
                                  cursor_gr (-1) ,
                                  del_a_curs (-1) ,
                                  del_gr_curs (-1)
               {
               }
               int dbreadfirst (void);
               int dbreadfirst_a (char *);
               int dbread_a (void);
               int dbreadfirst_gr (char *);
               int dbread_gr (void);
               int dbdelete_a (void);
               int dbdelete_gr (void);
               int dbclose_a (void);
               int dbclose_gr (void);
};
#endif
