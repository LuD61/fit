#ifndef _NEWLIST
#define _NEWLST 1
#endif

#ifndef _PFONT
#define _PFONT 1
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include "comcthlp.h"
#include "itemc.h"
#ifndef NINFO
#include "item.h"
class ITEM_CLASS item_class;
#endif
#include "wmaskc.h"
#include "mo_meld.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_intp.h"
#include "listclex.h"
#ifndef NINFO
#include "inflib.h"
#include "info.h"
#endif

#define MAXLEN 40
#define MAXBEZ 500
#define MAXFIND 30


#define LPLUS 1

#ifdef _DEBUG
#include <crtdbg.h>
_CrtMemState memory;
#endif

BOOL (*IsComboMessage) (MSG *) = NULL;

void SetComboMess (BOOL (*CbProg) (MSG *))
{
       IsComboMessage = CbProg;
}


mfont sfont = {
	           "Courier New", 
//	           "MS SAN SERIF", 
                120, 0, 0,
                BLACKCOL,
                LTGRAYCOL,
                0,
                NULL};



ITEM iUbSatzNr ("", "S-Nr", "", 0);

static field _FUbSatzNr[] = {
&iUbSatzNr,          6, 1, 0, 0, 0, "", BUTTON, 0, 0, 0};

static form FUbSatzNr = {1, 0, 0, _FUbSatzNr, 0, 0, 0, 0, NULL};

static char SatzNr [10];
static ITEM iSatzNr ("", SatzNr, "", 0);

static field _fSatzNr[] = {
&iSatzNr,          6, 1, 0, 0, 0, "", EDIT, 0, 0, 0};

static form fSatzNr = {1, 0, 0, _fSatzNr, 0, 0, 0, 0, NULL};


static field iButton = {NULL,  0, 1, 0, 0, 0, "", BUTTON, 0, 0, 0}; 
static field iEdit =   {NULL,  0, 0, 0, 0, 0, "", EDIT, 0, 0, 0}; 

static form IForm = {0, 0, 0, NULL, 0, 0, 0, 0, NULL};

static char *feldbztab [MAXBEZ];
static int fbanz = 0;

static BOOL NoMove = FALSE;
static BOOL FieldDestroy = FALSE;

/* Daten fuer Switch  */

static char *DlgSaetze [1000];
static int DlgAnz = 0;

static FELDER DlgZiel [] = {
"Bezeichnung",     20, "C", 0, 1,
"Wert",        MAXLEN, "C", 0, 1,
};

static form RowUb;
static form RowData;
static form RowLine;

struct DLGS
{
        char bez  [80];
        char wert [80];
};

struct DLGS dlgs, dlgstab [MAXBEZ]; 

static ITEM iDlgBezU  ("", "Bezeichnung", "", 0);
static ITEM iDlgWertU ("", "Wert",        "", 0);

static field _UbDlg[] = {
&iDlgBezU,         20, 1, 0, 6, 0, "", BUTTON, 0, 0, 0,
&iDlgWertU,    MAXLEN, 1, 0,26, 0, "", BUTTON, 0, 0, 0,
};

static form UbDlg = {2, 0, 0, _UbDlg, 0 ,0, 0, 0, NULL};  

static ITEM iDlgBez  ("", dlgs.bez,   "", 0);
static ITEM iDlgWert ("", dlgs.wert,  "", 0);

static field _DataDlg[] = {
&iDlgBez,          20, 1, 0, 6, 0, "", DISPLAYONLY, 0, 0, 0,
&iDlgWert,     MAXLEN, 1, 0,26, 0, "", EDIT, 0, 0, 0,
};

static form DataDlg = {2, 0, 0, _DataDlg, 0 ,0, 0, 0, NULL};  

static ITEM iline ("", "1", "", 0);

static field _LineDlg[] = {
&iline,             1, 1, 0,26, 0, "", DISPLAYONLY, 0, 0, 0,
&iline,             1, 1, 0,66, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form LineDlg = {2, 0, 0, _LineDlg, 0 ,0, 0, 0, NULL};  

static int Dlgzlen = 20 + MAXLEN;
static unsigned char DlgSatzB [20 + MAXLEN + 2];
static unsigned char *DlgSatz = DlgSatzB;

/* Daten fuer Foneingabe           */

HWND choise = NULL;

static char zh[] = {"Zeichenh�he  "};
static char zw[] = {"Zeichenbreite"};

static int FAttr;
static ITEM vHeightT ("" , zh, "",  0);
static ITEM vWidthT  ("" , zw, "",  0);
static ITEM vHeight ("" , "      ", "",   0);
static ITEM vWidth  ("" , "      ", "", 0);
static ITEM vAttrC  ("",  "Fett",  "", 0);
static ITEM vOK     ("",  "OK",  "", 0);
static ITEM vCancel ("",  "Abbrechen", "", 0);


static int testcr (void);

static field _ffont [] = {
&vHeightT, 13, 0, 1, 1, 0, "", DISPLAYONLY, 0, 0, 0, 
&vWidthT,  13, 0, 2, 1, 0, "", DISPLAYONLY, 0, 0, 0, 
&vHeight,   6, 0, 1,15, 0, "%3d", EDIT | UPDOWN, 0, testcr, 0, 
&vWidth,    6, 0, 2,15, 0, "%3d", EDIT | UPDOWN, 0, testcr, 0, 
&vAttrC,    7, 0, 3,15, 0, "", BUTTON | CHECKBUTTON, 0, testcr, KEY6,
&vOK,       4, 0, 5, 4, 0, "", BUTTON | DEFBUTTON, 0, testcr, KEY12,
&vCancel,  11, 0, 5, 9, 0, "", BUTTON, 0, testcr, KEY5, 
};

static form ffont = {7, 0, 0, _ffont, 0, 0, 0, 0, &sfont}; 
// static form ffont = {7, 0, 0, _ffont, 0, 0, 0, 0, NULL}; 

static struct UDTAB udtab[] = {500, 50, 90,
                               200,  0,100};
static int udanz = 2;

static char SearchBuff [256];

static ITEM vSearchBez ("","Suchen",         "", 0);   
static ITEM vSearch    ("", SearchBuff,       "", 0);

static field _ffind [] = {
&vSearchBez,   6, 0,  1, 2,  0, "", DISPLAYONLY, 0, 0, 0,
&vSearch,     20,10,  1, 9,  0, "255", COMBOBOX | SCROLL, 0, 0, 0,     
&vOK,          4, 0,  3, 8,  0, "", BUTTON, 0, 0, KEY12,
&vCancel,     11, 0,  3,13,  0, "", BUTTON, 0, 0, KEY5, 
};

static char findauswahl [MAXFIND][256];
static int findpos = 0;

static combofield  cbfield[] = {1, 256, 0, (char *) findauswahl,
                                 0,  0, 0, NULL};


static form ffind = {4, 0, 0, _ffind, 0, 0, 0, cbfield, NULL}; 

int eBreak (void)
{
       syskey = KEY5;
       break_enter ();
       return 0;
}

int eOk (void)
{
       syskey = KEY12;

       GetWindowText (ffont.mask [2].feldid,
                      ffont.mask [2].item->GetFeldPtr (),
                      ffont.mask [2].length);
       GetWindowText (ffont.mask [3].feldid,
                      ffont.mask [3].item->GetFeldPtr (),
                      ffont.mask [3].length);
       break_enter ();
       return 0;
}

int eAttr (void)
{
       syskey = KEY6;
       if (FAttr == 0)
       {
           FAttr = 1;
       }
       else
       {
           FAttr = 0;
       }
       return 0;
}

static HWND GethMainWindow (HWND hWnd)
/**
Hauptfenster der Anwendung holen.
**/
{
         HWND aktMain;

         while (TRUE)
         {
                 aktMain = GetParent (hWnd);
                 if (aktMain == NULL) break;
                 hWnd = aktMain;
         }
         return hWnd;
}

static void DisableWindows (HWND hWnd)
/**
Hauptfenster der Anwendung holen.
**/
{

         while (TRUE)
         {
                 EnableWindow (hWnd, FALSE);     
                 hWnd = GetParent (hWnd);
                 if (hWnd == NULL) break;
         }
}

static void EnableWindows (HWND hWnd)
/**
Hauptfenster der Anwendung holen.
**/
{

         while (TRUE)
         {
                 EnableWindow (hWnd, TRUE);     
                 hWnd = GetParent (hWnd);
                 if (hWnd == NULL) break;
         }
}


// BOOL ListClass::NoRecNr = FALSE;


ListClass *ListClass::ActiveList = NULL;
int ListClass::pagelen = 66;
int ListClass::pagelen0 = 59;
int ListClass::ublen = 0;
int ListClass::PageStdCols;
int ListClass::PageStart = 0;
int ListClass::PageEnd = 0;
BOOL ListClass::KeySend = FALSE;


ListClass::ListClass ()
{
        Keys = NULL; 
        HANDLE hInstance = (HANDLE) GetWindowLong (mamain2, GWL_HINSTANCE); 
        ArrDown = LoadBitmap (hInstance, "ARRDOWN");
        InvalidateFocusFrame = FALSE;
        IsArrow = FALSE;
        oldcursor = NULL;;
        arrow = NULL;
        aktcursor = LoadCursor (NULL, IDC_ARROW);
        movfield = 0; 
        InMove = 0;
        AktRow = 0;
		SelRow = 0;
		AktColumn = 0;
	    AktRowS = 0;
		AktColumnS = 0;
		WithFocus = 1;
		recanz = 0;
		recHeight = 1;
		banz = 0;
		ListFocus = 4;
		FocusWindow = NULL;
        RowMode = 0;
//                    LstZiel = NULL;
        PageView = 0;
        EditFont = NULL;
        LineColor = 3;
        UseZiel = 0;
        InfoProc = NULL;
        TestAppend = NULL;
        RowProc = NULL;
        paintallways = 0;
        ausgabesatz = NULL;
        Color = RGB (0, 0, 0);;
        BkColor = RGB (255, 255, 255); 
        NoScrollBkColor = LTGRAYCOL; 
        NewRec = 0;
        InAppend = 0;
        ChAttr = NULL;
        UseRowItem = FALSE;
        FrmRow = 0;
        UbRows = NULL;
        fUbSatzNr.mask = _fUbSatzNr;
        _fUbSatzNr[0].feldid = NULL;
		RowHeight = (double) 1.5;
		listclipp = FALSE;
		setsel = TRUE;
		focusframe = TRUE;
		Plus3D = 1;
        print3D = 0;
 		NoRecNr = FALSE;
        EditFont = NULL;
        ListhFont = NULL;
        memcpy (&UbFont,   &listdefaultfont, sizeof (mfont));
        memcpy (&UbSFont,  &listdefaultfont, sizeof (mfont));
        memcpy (&SFont,    &listdefaultfont, sizeof (mfont));
        memcpy (&ListFont, &listdefaultfont, sizeof (mfont));
        AktFont = &ListFont;
        UbForm.fieldanz = 0;
        UbForm.mask = NULL;
        UbForm.font = &UbFont;
        UbRows = NULL;
        HscrollStart = 0;
        NoScrollBackground = FALSE;
        OldList = ActiveList;
        ActiveList = this;
        hwndTB = NULL;
        hTrack = NULL;
        vTrack = NULL;
        cydiff = 0;
        Cinfo = NULL;
        FindEnabled = FALSE;
        NoMove = FALSE;
        PrintHdc = FALSE;
        BkBrush = NULL;
}

ListClass::~ListClass ()
{
    ActiveList = OldList;
    if (BkBrush != NULL)
    {
        DeleteObject (BkBrush);
    }
}

void ListClass::SetNoRecNr (BOOL flag)
/**
FLag NoReNr setzen.
**/
{
       NoRecNr = flag;
}

void ListClass::ChoiseLines (HDC hdc)
{
         static HPEN hPenG = NULL;
         static HPEN hPenW = NULL;
         RECT rect;
         int x, y;
         int cx, cy;

         GetClientRect (choise, &rect);
         x = tm.tmAveCharWidth / 2;
         y = tm.tmHeight / 2;

         cx = rect.right - tm.tmAveCharWidth / 2;
         cy = rect.bottom - tm.tmHeight / 2;
         if (hPenG == NULL)
         {
                   hPenG = CreatePen (PS_SOLID, 0, GRAYCOL);
                   hPenW = CreatePen (PS_SOLID, 0, WHITECOL);
         }
         HPEN oldPen = SelectObject (hdc, hPenG);

         MoveToEx (hdc, x, y, NULL);
         LineTo (hdc, cx, y);

         MoveToEx (hdc, x, y, NULL);
         LineTo (hdc, x, cy);

         MoveToEx (hdc, cx, y, NULL);
         LineTo (hdc, cx, cy);

         MoveToEx (hdc, x, cy, NULL);
         LineTo (hdc, cx, cy);

         SelectObject (hdc, hPenW);


         y ++;
         x ++;
         cx --;
         cy --;
         MoveToEx (hdc, x, y, NULL);
         LineTo (hdc, cx, y);

         MoveToEx (hdc, x, y, NULL);
         LineTo (hdc, x, cy);

         cx += 2;
         cy += 2;
         MoveToEx (hdc, cx, y, NULL);
         LineTo (hdc, cx, cy);

         MoveToEx (hdc, x, cy, NULL);
         LineTo (hdc, cx, cy);
         SelectObject (hdc, oldPen);
         DeleteObject (hPenG);
         DeleteObject (hPenW);
}

int testcr (void)
{
      HWND hWnd;
      
      if (syskey == KEY6)
      {
           eAttr ();
           return 1;
      }
      if (syskey == KEYCR)
      {
           hWnd = GetFocus ();
           if (hWnd == ffont.mask[4].feldid)
           {
               syskey = KEY6; 
               eAttr ();
               return 1;
           }
           else if (hWnd == ffont.mask[6].feldid)
           {
               syskey = KEY5;
           }
           break_enter ();
       }
       else if (syskey == KEY12)
       {
           break_enter ();
       }
       else if (syskey == KEY5)
       {
           break_enter ();
       }
       else if (syskey == KEYESC)
       {
           break_enter ();
       }
       return 0;
}

void ListClass::ChoiseFont (mfont *lFont)
/**
Font eingeben.
**/
{
        HDC hdc; 

        spezfont (&sfont);

        save_fkt (5);
        save_fkt (12);
        save_fkt (6);

        set_fkt (eBreak, 5);
        set_fkt (eOk, 12);
        set_fkt (eAttr, 6);

        udtab[0].Pos = lFont->FontHeight;
        udtab[1].Pos = lFont->FontWidth;
        sprintf (ffont.mask[2].item->GetFeldPtr (), "%3d", 
                 lFont->FontHeight);       

        sprintf (ffont.mask[3].item->GetFeldPtr (), "%3d", 
                 lFont->FontWidth);
        strcpy (ffont.mask[0].item->GetFeldPtr (), zh);
        strcpy (ffont.mask[1].item->GetFeldPtr (), zw);

        if (lFont->FontAttribute)
        {
                 ffont.mask[4].picture = "1";
        }
        else
        {
                 ffont.mask[4].picture = "";
        }

        no_break_end ();
        SetButtonTab (TRUE);
        DisablehWnd (hMainWindow);

        choise  = CreateWindow (
                                 "hStdWindow",
                                 "Font-Daten",
                                 WS_POPUP | 
                                 WS_VISIBLE | 
                                 WS_CAPTION | 
                                 WS_DLGFRAME, 
                                 32, 40,
                                 200, 180,
                                 GetActiveWindow (),
                                 NULL,
                                 hMainInst,
                                 NULL);

        ShowWindow (choise, SW_SHOW);
        UpdateWindow (choise);

        hdc = GetDC (choise);
        ChoiseLines (hdc);
        ReleaseDC (choise, hdc);
        SetUpDownTab (udtab , udanz);
        DisplayAfterEnter (FALSE);
        enter_form (choise, &ffont, 0, 0);
		SetAktivWindow (mamain3);
        DestroyWindow (choise);
        DisplayAfterEnter (TRUE);
        choise = NULL;
        SetButtonTab (FALSE);
        restore_fkt (5);
        restore_fkt (12);
        restore_fkt (6);

        if (syskey == KEY5) return;
        if (syskey == KEYESC) return;

        lFont->FontHeight = atoi (ffont.mask[2].item->GetFeldPtr ());       
        lFont->FontWidth  = atoi (ffont.mask[3].item->GetFeldPtr ());       
        lFont->FontAttribute  = FAttr;       
        SetFont (lFont);
        UbHeight = UbForm.mask[0].rows * tm.tmHeight;
}

void ListClass::SetColors (COLORREF Color, COLORREF BkColor)
{
     
            
        this->Color   = Color;
        this->BkColor = BkColor;
        if (mamain2 == NULL) return;
        SetClassLong (mamain2,GCL_HBRBACKGROUND, 
                                      (long) CreateSolidBrush (BkColor));
        InvalidateRect (mamain3, NULL, TRUE);
}

void ListClass::FindNext (void)
/**
Naechste Uebereinstimmung suchen.
**/
{
        char leer [256];

        if (FindEnabled == FALSE) return;
        memset (leer, ' ', 256);
        leer[255] = (char) 0;
        if (strcmp (SearchBuff,leer) <= 0)
        {
            FindString ();
        }
        else
        {
            this->find (SearchBuff);
        }
}

BOOL ListClass::NewSearchString ()
{
        int i;

        for (i = 0; i < findpos; i ++)
        {
            if (strcmp (SearchBuff, findauswahl[i]) == 0)
            {
                return FALSE;
            }
        }
        return TRUE;
}

void ListClass::FindString (void)
/**
String suchen.
**/
{
        HWND hFind;

        if (FindEnabled == FALSE) return;
        save_fkt (5);
        save_fkt (12);
        save_fkt (6);

        set_fkt (eBreak, 5);
        set_fkt (eOk, 12);

        break_end ();
        DisablehWnd (hMainWindow);
        SetBorder (WS_POPUP | WS_VISIBLE | WS_CAPTION | WS_DLGFRAME);
        hFind = OpenWindowChC (6, 35, 10, 20, hMainInst,
                "Suchen");
        enter_form (hFind, &ffind, 0, 0);
        DestroyFrmWindow ();
        hFind = NULL;
        no_break_end ();
        restore_fkt (5);
        restore_fkt (12);
        restore_fkt (6);
        if (syskey == KEY5) return;

        if (NewSearchString ())
        {
                 strcpy (findauswahl[findpos], SearchBuff);
                 if (findpos < MAXFIND -1) findpos ++;
                 cbfield[0].cbanz = findpos; 
                 fillcombobox (&ffind, 0);
        }
        this->find (SearchBuff);
}

BOOL ListClass::InRec (char *value, int *pos)
{
         int i;

         for (i = *pos; i < DataForm.fieldanz; i ++)
         {
//             if (strstr (DataForm.mask[i].item->GetFeldPtr (),
             if (upstrstr (DataForm.mask[i].item->GetFeldPtr (),
                 value))
             {
                 *pos = i;
                 return TRUE;
             }
         }
         return FALSE;
}


void ListClass::find (char *value)
/**
Begriff suchen.
**/
{
         int i;
         int row;
		 RECT rect;

         if (RowMode) return;

		 GetClientRect (mamain3, &rect);
		 rect.top += UbHeight;
         clipped (value);
         row = AktColumn + 1; 
         for (i = AktRow; i < recanz; i ++)
         {
             memcpy (ausgabesatz, SwSaetze[i], zlen);
             if (InRec (value, &row)) 
             {
                 break;
             }
             row = 0;
         }
         if (i == recanz) 
         {
             disp_mess ("Keine �bereinstimmung gefunden", 2);
             return;
         }

         AktRow = i;
         AktColumn = row;
/*
         if (DataForm.mask[row].attribut != EDIT)
         {
                   row = NextColumn ();
                   AktColumn = row;
         }
*/
		 row = FirstColumn ();
		 AktColumn = row;
         FindRow = i;
         FindColumn = row;
//         SetPos (AktColumn);
         SetVPos (AktRow);
 		 SetFeldFocus0 (AktRow, AktColumn);
         InvalidateRect (mamain3, &rect, TRUE);
         UpdateWindow (mamain3);
}

void ListClass::DestroyListWindow (void)
{
         
//         DestroyFocusWindow ();

         CloseControls (&fSatzNr);
         CloseControls (&UbForm);
         from_frmstack (&fSatzNr);
         from_frmstack (&UbForm);

         if (mamain3)
         {
             DestroyWindow (mamain3);
         }

         if (vTrack)
         {
             DestroyWindow (vTrack);
         }

         if (hTrack)
         {
             DestroyWindow (hTrack);
         }
         if (mamain2)
         {
             DestroyWindow (mamain2);
         }

         if (BkBrush != NULL)
         {
             DeleteObject (BkBrush);
             BkBrush = NULL;
         }
         mamain3 = NULL;
         vTrack  = NULL;
         hTrack  = NULL;
         mamain2 = NULL;
}



HWND ListClass::InitListWindow (HWND hMainWindow)
/**
Listfenster oeffnen.
**/
{
       int x,y;
       int cx,cy;
       RECT rect;


       memcpy (_fUbSatzNr, _FUbSatzNr, sizeof (field));
       memcpy (&fUbSatzNr, &FUbSatzNr, sizeof (form));
       fUbSatzNr.mask[0].feldid = NULL;
       InAppend = NewRec = 0;
       if (mamain2)
       {
           DestroyWindow (mamain2);
       }

       this->hMainWindow = hMainWindow;
       this->hMainInst = (HANDLE) GetWindowLong (hMainWindow, 
                                               GWL_HINSTANCE);
       y = LineRow + 5;
       x = 1;
       GetClientRect (hMainWindow, &rect);
       cy = rect.bottom - y - 5 - cydiff;
       cx = rect.right - 2;

       mamain2 = CreateWindowEx (
                                WS_EX_CLIENTEDGE, 
                                "hListWindow",
                                "",       
                                WS_CHILD | WS_VISIBLE,
                                x, y,
                                cx, cy,
                                hMainWindow,
                                NULL,
                                hMainInst,
                                NULL);

        BkBrush = CreateSolidBrush (BkColor);
        SetClassLong (mamain2,GCL_HBRBACKGROUND, 
                         (long) BkBrush);
        if (mamain3)
        {
           DestroyWindow (mamain3);
        }


        mamain3 = CreateWindowEx (
                                WS_EX_CLIENTEDGE, 
                                "hListWindow",
                                "",       
                                WS_CHILD | WS_VISIBLE,
                                0, 0,
                                0, 0,
                                mamain2,
                                NULL,
                                hMainInst,
                                NULL);


        if (Keys)
        {
            Keys->CreateListQuikInfos (mamain3);
        }

        return mamain2;
}

void ListClass::CopyFonts (mfont *lFont)
/**
Neuen Font fuer Liste setzen.
**/
{
        HFONT hFont, oldFont;
        HDC hdc;
        SIZE size;

        spezfont (lFont);
        memcpy (&UbFont,  lFont, sizeof (mfont));   
        memcpy (&UbSFont, lFont, sizeof (mfont));   
        memcpy (&SFont,   lFont, sizeof (mfont));   
        memcpy (&ListFont,lFont, sizeof (mfont));
        EditFont = NULL;

        hdc = GetDC (NULL);
        hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
        oldFont = SelectObject (hdc,hFont);
		ListhFont = hFont;
        GetTextMetrics (hdc, &tm);
        GetTextExtentPoint32 (hdc, "X", 1, &size);
        tm.tmAveCharWidth = size.cx;
        DeleteObject (SelectObject (hdc, oldFont));
        ReleaseDC (0, hdc);
        SetTextMetric (&tm);
}



void ListClass::SetFont (mfont *lFont)
/**
Neuen Font fuer Liste setzen.
**/
{
        HFONT hFont, oldFont;
        HDC hdc;
        SIZE size;

        spezfont (lFont);
        AktFont = lFont;
        CloseControls (&fUbSatzNr);
        CloseControls (&UbForm);
        memcpy (&UbFont,  lFont, sizeof (mfont));   
        memcpy (&UbSFont, lFont, sizeof (mfont));   
        memcpy (&SFont,   lFont, sizeof (mfont));   
        memcpy (&ListFont,lFont, sizeof (mfont));
        EditFont = NULL;
        DestroyFocusWindow ();
        CloseControls (&fUbSatzNr);
        CloseControls (&UbForm);

        hdc = GetDC (NULL);
        hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
        oldFont = SelectObject (hdc,hFont);
        GetTextMetrics (hdc, &tm);
        GetTextExtentPoint32 (hdc, "X", 1, &size);
        tm.tmAveCharWidth = size.cx;
        DeleteObject (SelectObject (hdc, oldFont));
        ReleaseDC (0, hdc);

		ListhFont = hFont;
        SetTextMetric (&tm);
        if (NoRecNr == FALSE)
        {
                  display_form (mamain3, &fUbSatzNr, 0, 0);
        }

        if (UbForm.mask[0].rows == 0)
        {
            fUbSatzNr.mask[0].rows = 0;
            UbHeight = tm.tmHeight + tm.tmHeight / 3; 
        }
        else 
        {
            fUbSatzNr.mask[0].rows = UbForm.mask[0].rows;
            UbHeight = tm.tmHeight * UbForm.mask[0].rows; 
        }
        display_form (mamain3, &UbForm, 0, 0);
        SendMessage (mamain2, WM_SIZE, NULL, NULL);
        stdfont ();
        InvalidateRect (mamain2, 0, TRUE);
        InvalidateRect (mamain3, 0, TRUE);
        SetTextMetric (&tm);
}


void ListClass::TestMamain3 (void)
{
       RECT rect;
//       HDC hdc;
       
       GetClientRect (mamain2, &rect);

       if (TrackNeeded ())
       {
           rect.bottom -= 16;
       }
       if (VTrackNeeded ())
       {
           rect.right -= 16;
       }

       if (mamain3 != NULL)
       {
           MoveWindow (mamain3, 0, 0, rect.right, rect.bottom, TRUE);


           CloseControls (&UbForm);
           if (UbForm.mask)
           {
               display_form (mamain3, &UbForm, 0, 0);
           }
       }
}

void ListClass::TestTrack (void)
{
       RECT rect;
       int x,y,cx,cy;
	   SCROLLINFO scinfo;

       if (TrackNeeded ())
       {
           CreateTrack ();
       }
       else
       {
           DestroyTrack ();
       }
       if (hTrack == NULL) return;

       GetClientRect (mamain2, &rect);
       x = 0;
       y = rect.bottom - 16;
       cy = 16;
       cx = rect.right;
       MoveWindow (hTrack, x, y, cx, cy, TRUE);
	   scinfo.cbSize = sizeof (SCROLLINFO);
	   scinfo.fMask  = SIF_PAGE;
       scinfo.nPage  = 1;
       SetScrollInfo  (hTrack, SB_CTL, &scinfo, TRUE);
}

void ListClass::TestVTrack (void)
{
       RECT rect;
       int x,y,cx,cy;
	   SCROLLINFO scinfo;

       if (VTrackNeeded ())
       {
           CreateVTrack ();
       }
       else
       {
           DestroyVTrack ();
       }
       if (vTrack == NULL) return;

       GetClientRect (mamain2, &rect);

       x = rect.right - 16;
       cx = 16;
       y = 0;
       cy = rect.bottom;
       if (hTrack)
       {
                 cy -= 16;
       }
       MoveWindow (vTrack, x, y, cx, cy, TRUE);
	   scinfo.cbSize = sizeof (SCROLLINFO);
	   scinfo.fMask  = SIF_PAGE;
       scinfo.nPage  = 1;
       SetScrollInfo  (vTrack, SB_CTL, &scinfo, TRUE);
       SetScrollPos (vTrack, SB_CTL, scrollpos, TRUE);
}


void ListClass::MoveListWindow (void)
/**
Listfenster oeffnen.
**/
{
       int x,y;
       int cx,cy;
       RECT rect;

       if (mamain2 == NULL) return;

       GetPageLen ();
       y = LineRow + 5;
       x = 1;
       GetClientRect (hMainWindow, &rect);
       cy = rect.bottom - y - 5 - cydiff;
       cx = rect.right - 2;

       MoveWindow (mamain2, x, y, cx, cy, TRUE);


       if (hwndTB != NULL) 
       {
              GetClientRect (hwndTB, &rect);
              MoveWindow (hwndTB, 0, 0, cx + 2, rect.bottom, TRUE);
       }

       TestMamain3 ();
       TestTrack ();
       TestVTrack ();
}


BOOL ListClass::TrackNeeded (void)
/**
Test, ob ein Schieberegler benoetigt wird.
**/
{
        RECT frmrect;
        RECT winrect;

		if (banz == 0) return FALSE;
        GetFrmRect (&UbForm, &frmrect);
        GetClientRect (mamain2, &winrect);
        if (frmrect.right > winrect.right)
        {
                    return TRUE;
        }
        return TRUE;
}

void ListClass::CreateTrack (void)
/**
Bildlaufleiste generieren.
**/
{
        int x,y;
        int cx, cy;
        RECT rect;
		SCROLLINFO scinfo;

        if (hTrack) return;

        GetClientRect (mamain2, &rect);

        x = 0;
        y = rect.bottom - 16;
        cy = 16;
        cx = rect.right;

        hTrack = CreateWindow ("scrollbar",
                               NULL,
                               WS_CHILD | WS_VISIBLE | SBS_HORZ,
                               x, y, cx, cy,
                               mamain2,
                               (HMENU) 701,
                               hMainInst,
                               0);


		 scinfo.cbSize = sizeof (SCROLLINFO);
		 scinfo.fMask  = SIF_PAGE | SIF_RANGE;
		 scinfo.nMin   = 0;
		 scinfo.nMax   = banz - 1;
		 scinfo.nPage  = 0;
         SetScrollInfo  (hTrack, SB_CTL, &scinfo, TRUE);
}

void ListClass::DestroyTrack (void)
/**
Bildlaufleiste loeschen.
**/
{
          if (hTrack)
          {
              DestroyWindow (hTrack);
              hTrack = NULL;
          }
}

BOOL ListClass::VTrackNeeded (void)
/**
Test, ob ein Schieberegler benoetigt wird.
**/
{
        RECT winrect;
        int cy;

        GetClientRect (mamain2, &winrect);

// recanz + 1 wegen Ueberschrift 

//      cy = (recanz + 1) * tm.tmHeight * recHeight;

        cy = (int) (double)((double) UbHeight + recanz * 
			         tm.tmHeight * recHeight * RowHeight);
        if (hTrack) 
        {

// untere Bildlaufleiste abziehen

            cy += 16; 
        }
        if (cy > winrect.bottom)
        {
                    return TRUE;
        }
        return FALSE;
}

void ListClass::CreateVTrack (void)
/**
Bildlaufleiste generieren.
**/
{
        int x,y;
        int cx, cy;
        RECT rect;
		SCROLLINFO scinfo;

        if (vTrack) return;
        GetClientRect (mamain2, &rect);

        x = rect.right - 16;
        cx = 16;
        y = 0;
        cy = rect.bottom;
        if (hTrack)
        {
                 cy -= 16;
        }

        vTrack = CreateWindow ("scrollbar",
                               NULL,
                               WS_CHILD | WS_VISIBLE | SBS_VERT,
                               x, y, cx, cy,
                               mamain2,
                               (HMENU) 702,
                               hMainInst,
                               0);
		 scinfo.cbSize = sizeof (SCROLLINFO);
		 scinfo.fMask  = SIF_PAGE | SIF_RANGE;
		 scinfo.nMin   = 0;
		 scinfo.nMax   = recanz - 1;
		 if (lPagelen)
		 {
		     scinfo.nPage  = 1;
		 }
		 else
		 {
			 scinfo.nPage = 0;
		 }
         SetScrollInfo  (vTrack, SB_CTL, &scinfo, TRUE);
}

void ListClass::DestroyVTrack (void)
/**
Bildlaufleiste loeschen.
**/
{
          if (vTrack)
          {
              DestroyWindow (vTrack);
              vTrack = NULL;
          }
}

BOOL ListClass::IsInListArea (MSG *msg)
/**
MousePosition Pruefen unf Focus setzen.
**/
{
	    POINT mousepos;
		RECT rect;
		int x1, y1;
		int x2, y2;
		int i, j;
		int diff;
        int breakstat, h;

//        if (msg->hwnd != mamain3) return FALSE;

        diff = 0;
        if (LineForm.frmstart)
		{
//            diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
//                   fSatzNr.mask[0].length;
			if (NoRecNr == FALSE)
			{
                    diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                           fSatzNr.mask[0].length;
			}
			else
			{
                    diff = LineForm.mask[LineForm.frmstart - 1].pos[1];
			}
/*
            if (HscrollStart > 0)
            {
                diff -= LineForm.mask[HscrollStart - 1].pos[1];
            }
*/
		}
        GetCursorPos (&mousepos);
		GetWindowRect (mamain3, &rect);
//		rect.top += tm.tmHeight;
		rect.top += UbHeight;

		if (mousepos.x < rect.left ||
			mousepos.x > rect.right)
		{
			        return FALSE;
		}
		if (mousepos.y < rect.top ||
			mousepos.y > rect.bottom)
		{
			        return FALSE;
		}

        ScreenToClient (mamain3, &mousepos);

        breakstat = 0;
        for (h = 0; h < recHeight; h ++)
        {
		  for (i = 0; i < recanz; i ++)
          {
			    if (i > lPagelen) break;
//			    y1 = UbHeight + i * tm.tmHeight * recHeight + h * tm.tmHeight;
			    y1 = (int) (double) ((double) UbHeight + 
					        i * tm.tmHeight * recHeight * 
							RowHeight + h * tm.tmHeight);
				y2 = y1 + tm.tmHeight;
				if (mousepos.y < y1 ||
					mousepos.y > y2)
				{
					continue;
				}
				for (j = 0; j < banz; j ++)
				{
                    if (DataForm.mask[j].pos[0] != h) continue;
					x1 = (DataForm.mask[j].pos[1] - diff) *
						 tm.tmAveCharWidth;
					x2 = x1 + (DataForm.mask[j].length * tm.tmAveCharWidth);
 		    		if (mousepos.x > x1 &&
			    		mousepos.x < x2)
					{
                            breakstat = 1;
					        break;
					}
				}
	    		if (mousepos.x > x1 &&
		    		mousepos.x < x2)
				{
					break;
				}
          }
          if (breakstat) break;
		}
   		if (mousepos.x < x1 ||
    		mousepos.x > x2)
		{
				return FALSE;
		}

		if (i + scrollpos >= recanz) return FALSE;
		if (j >= banz) return FALSE;
        if (IsEditColumn (j) == FALSE)
        {
                return TRUE;
        }

		syskey = 0;

		if (FocusWindow)
		{
			 GetFocusText ();
		}

        if (DataForm.mask[j].attribut & COMBOBOX)
        {
                if (AktRow == i + scrollpos && AktColumn == j)
                {
                    return FALSE;
                }
        }

		if (AktColumn != j && IsButtonCol (j) == FALSE)
		{
			    if (TestAfter () == -1) return FALSE;
				if (posset) return TRUE;
		}
        if (AktRow != i + scrollpos)
        {
                TestAfterRow ();
  		        AktRow = i + scrollpos;
                memcpy (ausgabesatz, SwSaetze [AktRow], zlen); 
                TestBeforeRow ();
                if (IsButtonColumn (j))
                {
                      InvalidateRect (mamain3, &FocusRect, TRUE);
	                  UpdateWindow (mamain3);
		              SetFeldFocus0 (AktRow, AktColumn);
                      TestAfter (j);
                      return TRUE;
                }
                else if (IsCheckColumn (j))
                {
                      InvalidateRect (mamain3, &FocusRect, TRUE);
	                  UpdateWindow (mamain3);
		              SetFeldFocus0 (AktRow, AktColumn);
//                      TestAfter (j);
                      if (Keys)
                      {
                              Keys->AfterCol (DataForm.mask[j].item->GetItemName ());
                      }
                      return TRUE;
                }
				if (posset) return TRUE;
        }
        else
        {
 		        AktRow = i + scrollpos;
                if (IsButtonColumn (j))
                {
                      InvalidateRect (mamain3, &FocusRect, TRUE);
	                  UpdateWindow (mamain3);
		              SetFeldFocus0 (AktRow, AktColumn);
                      TestAfter (j);
                      return TRUE;
                }
                else if (IsCheckColumn (j))
                {
                      InvalidateRect (mamain3, &FocusRect, TRUE);
//                      InvalidateRect (mamain3, NULL, TRUE);
	                  UpdateWindow (mamain3);
		              SetFeldFocus0 (AktRow, AktColumn);
//                      TestAfter (j);
                      if (Keys)
                      {
                              Keys->AfterCol (DataForm.mask[j].item->GetItemName ());
                      }
                      return TRUE;
                }
        }
        if (RowMode &&  j == 0)
        {
		        return TRUE;
        }
        AktColumn = j;
        TestBefore ();
        InvalidateRect (mamain3, &FocusRect, TRUE);
	    UpdateWindow (mamain3);
		SetFeldFocus0 (AktRow, AktColumn);
		return TRUE;
}

void ListClass::DestroyFocusWindow ()
/**
Editfenster loeschen.
**/
{
	       if (FocusWindow)
		   {
			   DestroyWindow (FocusWindow);
			   FocusWindow = NULL;
		       if (ListFocus == 4)
               {
 			         InvalidateRect (mamain3, &FocusRect, TRUE);
               }
		   }
}

void ListClass::KillListFocus ()
/**
Editfenster loeschen.
**/
{
           AktFocus = ListFocus;
           DestroyFocusWindow ();
           ListFocus = 0;
}

void ListClass::SetListFocus ()
/**
Editfenster loeschen.
**/
{
           ListFocus = AktFocus;
           SetFeldFocus0 (AktRow, AktColumn);
}

void ListClass::ShowDropDown (void)
{
           if (FocusWindow == NULL)
           {
               return;
           }

           if (DataForm.mask[AktColumn].attribut & COMBOBOX)
           {
                SendMessage (FocusWindow,  CB_SHOWDROPDOWN, TRUE,  NULL);
           }
}

void ListClass::CloseDropDown (void)
{
           if (FocusWindow == NULL)
           {
               return;
           }

           if (DataForm.mask[AktColumn].attribut & COMBOBOX)
           {
                SendMessage (FocusWindow,  CB_SHOWDROPDOWN, FALSE,  NULL);
           }
}


void ListClass::SetFeldEdit (int zeile, int spalte)
/**
Focus in ein Feldsetzen.
**/
{
	      RECT rect;
		  int x,y ,cx, cy;
		  int diff;
		  HANDLE hInstance;
          int ubspalte;
          DWORD style = 0;

          if (banz == 0) return;

          if (DataForm.mask[spalte].attribut == DISPLAYONLY) return;

          ubspalte = GetUbSpalte (spalte);

          hInstance = (HANDLE) GetWindowLong (mamain3, 
                                               GWL_HINSTANCE);
          if (zeile >= recanz) return;
		  if (spalte >= banz) return;
          if (ubspalte < LineForm.frmstart) return; 

          diff = 0;
          if (LineForm.frmstart)
		  {
//            diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
//                   fSatzNr.mask[0].length;
            if (NoRecNr == FALSE)
            {
                 diff = LineForm.mask[LineForm.frmstart - 1 + HscrollStart].pos[1] -
                   fSatzNr.mask[0].length;
            }
            else
            {
                 diff = LineForm.mask[LineForm.frmstart - 1 + HscrollStart].pos[1];
            }

            if (HscrollStart > 0)
            {
                diff -= LineForm.mask[HscrollStart - 1].pos[1];
            }

/*
            if (HscrollStart > 0)
            {
                 if ((ubspalte - UbForm.frmstart) < HscrollStart) return;
            }
*/
		  }


          if (HscrollStart > 0)
          {
                 if ((ubspalte - UbForm.frmstart) < HscrollStart) return;
          }
          if ((UseZiel == 0) && (RowMode == 0))
          {
                   strcpy (AktItem, DataForm.mask[spalte].item->GetItemName ());
          }

          else if ((UseZiel == 0) && (RowMode == 1))
          {
                   strcpy (AktItem, DataRow->mask[zeile].item->GetItemName ());
          }

		  clipped (AktItem);
          memcpy (ausgabesatz, SwSaetze [zeile], zlen);

		  strcpy (AktValue, 
			      DataForm.mask[spalte].item->GetFeldPtr ());

		  rect.top = (int) (double) ((double) UbHeight + (
			                zeile - scrollpos) * 
                             tm.tmHeight * recHeight * RowHeight - 2);
          rect.top += (int) (double) ((double) 
			             DataForm.mask[spalte].pos[0] * tm.tmHeight 
						       * RowHeight);
		  rect.top = GetMidPos (rect.top);
		  rect.bottom = rect.top + tm.tmHeight;
		  rect.left = (DataForm.mask[spalte].pos[1] - diff) *
			          tm.tmAveCharWidth;
		  rect.right = rect.left + (DataForm.mask[spalte].length) 
                       * tm.tmAveCharWidth;  
 		  memcpy (&FocusRect, &rect, sizeof (RECT));

		  y  = (int) (double) ((double) UbHeight + 
		  (zeile - scrollpos) * tm.tmHeight * recHeight * RowHeight);
          y += (int) (double) ((double) 
			  DataForm.mask[spalte].pos[0] * tm.tmHeight * RowHeight);
		  y = GetMidPos (y);
		  if (y < UbHeight) return;


          cy = tm.tmHeight + tm.tmHeight / 3;
		  x  = (DataForm.mask[spalte].pos[1] - diff) *
			   tm.tmAveCharWidth;

          if (RowMode == 0)
          {
		        cx = (DataForm.mask[spalte].length - 1)
                      * tm.tmAveCharWidth;  
               if (strchr (DataForm.mask[spalte].picture, 'f') ||
                   (strchr (DataForm.mask[spalte].picture, 'd') &&
                        strchr (DataForm.mask[spalte].picture, 'y') == NULL))
               {
                        style |= ES_RIGHT;
               }
               else if (numeric (DataForm.mask[spalte].picture))
               {
                        style |= ES_AUTOHSCROLL;
                        cx += tm.tmAveCharWidth;
               }
          }

/*
          else if (RowMode == 1 && UseZiel == 1)
          {
                cx = LstZiel[zeile].feldlen * tm.tmAveCharWidth;
          }
          else if (RowMode == 1 && UseZiel == 0)
          {
                cx = DataRow->mask[zeile].length * tm.tmAveCharWidth;
          }
*/


          if (DataForm.mask[spalte].attribut & COMBOBOX)
          {
                if (DataForm.mask[spalte].attribut & DROPLIST)
                {
                  style |= CBS_DROPDOWNLIST;
                }
                else
                {
                  style |= CBS_DROPDOWN;
                }

                FocusWindow = CreateWindow ("Combobox",
                                    "",
                                    WS_CHILD |
                                    WS_VSCROLL | CBS_DISABLENOSCROLL |
                                    style, 
									x, y - 3, cx + 10 , cy,
                                    mamain3,
                                    NULL,
                                    hInstance,
                                    NULL);
                if (Keys)
                {
                         Keys->FillCombo (FocusWindow, 
                                          DataForm.mask[spalte].item->GetItemName ());
                              
                }
                if (DataForm.mask[spalte].attribut & DROPLIST)
                {
                        SendMessage (FocusWindow, CB_SELECTSTRING, 0,
                                    (LPARAM) clipped 
                                    ((char *) DataForm.mask[spalte].item->GetFeldPtr ()));
                }
          }
          else if (DataForm.mask[spalte].attribut & CHECKBUTTON)
          {
                
                ListButton *lb = (ListButton *) DataForm.mask[spalte].item->GetFeldPtr ();  
                FocusWindow = CreateWindow ("Button",
                                    "",
                                    WS_CHILD |
                                    BS_AUTOCHECKBOX |
                                    WS_BORDER,
                                    lb->GetX (),
                                    lb->GetY (),
                                    lb->GetCX (),
                                    lb->GetCY (),
                                    mamain3,
                                    NULL,
                                    hInstance,
                                    NULL);
                if (lb->IsChecked ())
                {
                    SendMessage (FocusWindow, BM_SETCHECK, BST_CHECKED, 0l);
                }
          }
          else
          {
                FocusWindow = CreateWindowEx (WS_EX_CLIENTEDGE,
                                    "Edit",
                                    "",
                                    WS_CHILD | ES_MULTILINE | style, 
									x, y, cx, cy,
                                    mamain3,
                                    NULL,
                                    hInstance,
                                    NULL);
          }
  		  if (EditFont == NULL)
		  {
	           	spezfont (&ListFont);
		        EditFont = SetWindowFont (mamain3);
                stdfont ();
		  }
          SendMessage (FocusWindow,  WM_SETFONT, (WPARAM) EditFont, 0);

          SendMessage (FocusWindow,  EM_SETMARGINS, 
                                            (WPARAM) EC_LEFTMARGIN,
                                            MAKELONG (0,0));
          SendMessage (FocusWindow,  EM_SETMARGINS, 
                                            (WPARAM) EC_RIGHTMARGIN,
                                            MAKELONG (0,0));

		  ShowWindow (FocusWindow, SW_SHOWNORMAL);
		  UpdateWindow (FocusWindow);
		  AktRow    = zeile;
          AktColumn = spalte;

		  ShowWindowText (rect.left, rect.top);
//          SendMessage (FocusWindow, EM_SETSEL, (WPARAM) 0, MAKELONG (-1, 0));
          TestBefore ();
}


void ListClass::SetFeldEdit0 (int zeile, int spalte, DWORD style)
/**
Focus in ein Feldsetzen.
**/
{
	      RECT rect;
		  int x,y ,cx, cy;
		  int diff;
		  HANDLE hInstance;
          int ubspalte;

          if (banz == 0) return;
          if (DataForm.mask[spalte].attribut == DISPLAYONLY) return;
          ubspalte = GetUbSpalte (spalte);

          hInstance = (HANDLE) GetWindowLong (mamain3, 
                                               GWL_HINSTANCE);
          if (zeile >= recanz) return;
		  if (spalte >= banz) return;
          if (ubspalte < LineForm.frmstart) return; 

          diff = 0;
          if (LineForm.frmstart)
		  {
//            diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
//                   fSatzNr.mask[0].length;
            if (NoRecNr == FALSE)
            {
                 diff = LineForm.mask[LineForm.frmstart - 1 + HscrollStart].pos[1] -
                   fSatzNr.mask[0].length;
            }
            else
            {
                 diff = LineForm.mask[LineForm.frmstart - 1 + HscrollStart].pos[1];
            }

            if (HscrollStart > 0)
            {
                diff -= LineForm.mask[HscrollStart - 1].pos[1];
            }

/*
            if (HscrollStart > 0)
            {
                 if ((ubspalte - UbForm.frmstart) < HscrollStart) return;
            }
*/
		  }

          if (HscrollStart > 0)
          {
                 if ((ubspalte - UbForm.frmstart) < HscrollStart) return;
          }

          if ((UseZiel == 0) && (RowMode == 0))
          {
                   strcpy (AktItem, DataForm.mask[spalte].item->GetItemName ());
          }

          else if ((UseZiel == 0) && (RowMode == 1))
          {
                   strcpy (AktItem, DataRow->mask[zeile].item->GetItemName ());
          }

		  clipped (AktItem);
          memcpy (ausgabesatz, SwSaetze [zeile], zlen);

		  strcpy (AktValue, 
			      DataForm.mask[spalte].item->GetFeldPtr ());
		  rect.top = (int) (double) ((double) UbHeight + (zeile - scrollpos) * 
                     tm.tmHeight * recHeight * RowHeight - 2);
          rect.top += (int) (double) ((double) 
			  DataForm.mask[spalte].pos[0] * tm.tmHeight * RowHeight);
		  rect.top = GetMidPos (rect.top);
		  rect.bottom = rect.top + tm.tmHeight;
		  rect.left = (DataForm.mask[spalte].pos[1] - diff) *
			          tm.tmAveCharWidth;
		  rect.right = rect.left + (DataForm.mask[spalte].length) 
                        * tm.tmAveCharWidth;  
 		  memcpy (&FocusRect, &rect, sizeof (RECT));

		  y  = (int) (double) ((double) UbHeight + 
			          (zeile - scrollpos) * tm.tmHeight * 
                       recHeight * RowHeight);
          y += (int) (double) ((double) 
			  DataForm.mask[spalte].pos[0] * tm.tmHeight * RowHeight);
		  y = GetMidPos (y);
		  if (y < UbHeight) return;


		  cy = tm.tmHeight;
		  x  = (DataForm.mask[spalte].pos[1] - diff) *
			   tm.tmAveCharWidth;
          if (RowMode == 0)
          {
		        cx = (DataForm.mask[spalte].length - 1)
                      * tm.tmAveCharWidth;  
               if (strchr (DataForm.mask[spalte].picture, 'f') ||
                   (strchr (DataForm.mask[spalte].picture, 'd') &&
                        strchr (DataForm.mask[spalte].picture, 'y') == NULL))
               {
                        style |= ES_RIGHT;
               }
               else if (numeric (DataForm.mask[spalte].picture))
               {
                        style |= ES_AUTOHSCROLL;
                        cx += tm.tmAveCharWidth;
               }
          }

          if (DataForm.mask[spalte].attribut & COMBOBOX)
          {
                if (DataForm.mask[spalte].rows != 0)
                {
                    cy *= DataForm.mask[spalte].rows;
                }
                else
                {
                    cy *= 10;
                }
                if (DataForm.mask[spalte].attribut & DROPLIST)
                {
                  style |= CBS_DROPDOWNLIST;
                }
                else
                {
                  style |= CBS_DROPDOWN;
                }

                FocusWindow = CreateWindow ("Combobox",
                                    "",
                                    WS_CHILD |
                                    WS_VSCROLL | CBS_DISABLENOSCROLL |
                                    style, 
									x, y - 3, cx + 10 , cy,
                                    mamain3,
                                    NULL,
                                    hInstance,
                                    NULL);
                if (Keys)
                {
                         Keys->FillCombo (FocusWindow, 
                                          DataForm.mask[spalte].item->GetItemName ());
                              
                }
                if (DataForm.mask[spalte].attribut & DROPLIST)
                {
                        SendMessage (FocusWindow, CB_SELECTSTRING, 0,
                                    (LPARAM) clipped 
                                    ((char *) DataForm.mask[spalte].item->GetFeldPtr ()));
                }
          }
          else if (DataForm.mask[spalte].attribut & CHECKBUTTON)
          {
                
                ListButton *lb = (ListButton *) DataForm.mask[spalte].item->GetFeldPtr ();  
                FocusWindow = CreateWindow ("Button",
                                    "",
                                    WS_CHILD |
                                    BS_AUTOCHECKBOX |
                                    WS_BORDER,
                                    lb->GetX (),
                                    lb->GetY (),
                                    lb->GetCX (),
                                    lb->GetCY (),
                                    mamain3,
                                    NULL,
                                    hInstance,
                                    NULL);
                if (lb->IsChecked ())
                {
                    SendMessage (FocusWindow, BM_SETCHECK, BST_CHECKED, 0l);
                }
 
          }
          else
          {
                FocusWindow = CreateWindow ("Edit",
                                    "",
                                    WS_CHILD | ES_MULTILINE | style, 
									x, y, cx, cy,
                                    mamain3,
                                    NULL,
                                    hInstance,
                                    NULL);
                SendMessage (FocusWindow,  EM_SETMARGINS, 
                                            (WPARAM) EC_LEFTMARGIN,
                                            MAKELONG (0,0));
                SendMessage (FocusWindow,  EM_SETMARGINS, 
                                            (WPARAM) EC_RIGHTMARGIN,
                                            MAKELONG (0,0));
          }
  		  if (EditFont == NULL)
		  {
	           	spezfont (&ListFont);
		        EditFont = SetWindowFont (mamain3);
                stdfont ();
		  }
          SendMessage (FocusWindow,  WM_SETFONT, (WPARAM) EditFont, 0);



		  ShowWindow (FocusWindow, SW_SHOWNORMAL);
		  UpdateWindow (FocusWindow);
		  AktRow    = zeile;
          AktColumn = spalte;

		  ShowWindowText (rect.left, rect.top);
//          SendMessage (FocusWindow, EM_SETSEL, (WPARAM) 0, MAKELONG (-1, 0));
//          TestBefore ();
}

void ListClass::SetFocusFrame (HDC hdc, int zeile, int spalte)
/**
Focus in ein Feldsetzen.
**/
{
	      RECT rect;
		  HBRUSH hBrush;
		  int diff;
          int ubspalte;
		  int plus;


		  if (ListFocus != 4) return;
		  if (focusframe == FALSE) return;

          if (DataForm.mask[spalte].attribut == DISPLAYONLY) return;
          if (DataForm.mask[spalte].attribut & COMBOBOX) return;
//          if (DataForm.mask[spalte].attribut & CHECKBUTTON) return;

          ubspalte = GetUbSpalte (spalte);

          if (zeile >= recanz) return;
		  if (spalte >= banz) return;
          if (ubspalte < LineForm.frmstart) return; 

          diff = 0;
          if (LineForm.frmstart)
		  {
//            diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
//                   fSatzNr.mask[0].length;
            if (NoRecNr == FALSE)
            {
                 diff = LineForm.mask[LineForm.frmstart - 1 + HscrollStart].pos[1] -
                   fSatzNr.mask[0].length;
            }
            else
            {
                 diff = LineForm.mask[LineForm.frmstart - 1 + HscrollStart].pos[1];
            }

            if (HscrollStart > 0)
            {
                diff -= LineForm.mask[HscrollStart - 1].pos[1];
            }

            if (HscrollStart > 0)
            {
                 if (ubspalte <= HscrollStart) return;
            }
		  }
          if (UseZiel == 0)
          {
                   strcpy (AktItem, DataForm.mask[spalte].item->GetItemName ());
          }

		  clipped (AktItem);
          memcpy (ausgabesatz, SwSaetze [zeile], zlen);
		  strcpy (AktValue, 
			      DataForm.mask[spalte].item->GetFeldPtr ());
		  rect.top = (int) (double) ((double) UbHeight + 
			         (zeile - scrollpos) * tm.tmHeight * recHeight * 
					  RowHeight);
          rect.top += (int) (double) ((double) 
			  DataForm.mask[spalte].pos[0] * tm.tmHeight * RowHeight);
//		  rect.top = GetMidPos (rect.top);
//		  rect.bottom = rect.top + tm.tmHeight;
		  rect.bottom = (int) (double) ((double) rect.top + tm.tmHeight * RowHeight);
		  rect.left = (UbForm.mask[ubspalte].pos[1] - diff) *
			          tm.tmAveCharWidth;
          if (RowMode == 0)
          {
		        rect.right = rect.left + (UbForm.mask[ubspalte].length) *
			                 tm.tmAveCharWidth;  
          }
/*
          else
          {
                rect.right = rect.left + LstZiel[zeile].feldlen *
                             tm.tmAveCharWidth;
          }
*/
		  if (RowHeight == (double) 1.0)
		  {
               rect.left -= 1;
               rect.right += 1;
               rect.top -= 1;
               rect.bottom += 1;
			   plus = 1;
		  }
		  else
		  {
               rect.left -= 1;
               rect.right += 1;
               rect.top -= 1;
               rect.bottom += 1;
  			   plus = 2;
		  }


//		  memcpy (&FocusRect, &rect, sizeof (RECT));
          

          if (BkColor == BLUECOL)
          {
                 hBrush = CreateSolidBrush (WHITECOL);
          }
          else if (BkColor == BLACKCOL)
          {
                 hBrush = CreateSolidBrush (WHITECOL);
          }
          else
          {
                 hBrush = CreateSolidBrush (BLACKCOL);
          }
          HBRUSH oldBrush = SelectObject (hdc, GetStockObject (PS_DASH));
          FrameRect (hdc, &rect, hBrush);

          rect.top -= plus;
          rect.left -= plus;
          rect.bottom += plus;
          rect.right += plus;
          FrameRect (hdc, &rect, hBrush);
		  memcpy (&FocusRect, &rect, sizeof (RECT));

          DeleteObject (hBrush);
		  AktRow    = zeile;
          AktColumn = spalte;
          SelectObject (hdc, oldBrush);
          DeleteObject (hBrush);
}


void ListClass::SetFeldFrame (HDC hdc, int zeile, int spalte)
/**
Focus in ein Feldsetzen.
**/
{
	      RECT rect;
		  HBRUSH hBrush;
		  int diff;
		  
          if (zeile >= recanz) return;
		  if (spalte >= banz) return;
          if (spalte < LineForm.frmstart) return; 

          diff = 0;
          if (LineForm.frmstart)
		  {
//            diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
//                   fSatzNr.mask[0].length;
            if (NoRecNr == FALSE)
            {
                 diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                   fSatzNr.mask[0].length;
            }
            else
            {
                 diff = LineForm.mask[LineForm.frmstart - 1].pos[1];
            }
            if (HscrollStart > 0)
            {
                diff -= LineForm.mask[HscrollStart - 1].pos[1];
            }
            if (HscrollStart > 0)
            {
//                 if (ubspalte <= HscrollStart) return;
            }
		  }
          if (UseZiel == 0)
          {
                   strcpy (AktItem, DataForm.mask[spalte].item->GetItemName ());
          }

		  clipped (AktItem);
          memcpy (ausgabesatz, SwSaetze [zeile], zlen);
		  strcpy (AktValue, 
			      DataForm.mask[spalte].item->GetFeldPtr ());

//		  rect.top = (zeile - scrollpos + 1) * tm.tmHeight;
		  rect.top = (int) (double) ((double) UbHeight + 
			          (zeile - scrollpos) * tm.tmHeight * recHeight * RowHeight);
          rect.top += (int) (double) ((double) 
			  DataForm.mask[spalte].pos[0] * tm.tmHeight * RowHeight);
		  rect.top = GetMidPos (rect.top);
		  rect.bottom = rect.top + tm.tmHeight;
		  rect.left = (DataForm.mask[spalte].pos[1] - diff) *
			          tm.tmAveCharWidth;
          if (RowMode == 0)
          {
		        rect.right = rect.left + DataForm.mask[spalte].length *
		                    tm.tmAveCharWidth;  
          }
/*
          else
          {
                rect.right = rect.left + LstZiel[zeile].feldlen *
                             tm.tmAveCharWidth;
          }
*/
		  memcpy (&FocusRect, &rect, sizeof (RECT));


          hBrush = CreateSolidBrush (BLUECOL);

          HBRUSH oldBrush = SelectObject (hdc, hBrush);
          SelectObject (hdc, GetStockObject (NULL_PEN));
          Rectangle (hdc,rect.left, rect.top, rect.right, rect.bottom);
          DeleteObject (hBrush); 
		  AktRow    = zeile;
          AktColumn = spalte;

		  ShowFocusText (hdc, rect.left, rect.top);
          SelectObject (hdc, oldBrush);
          DeleteObject (hBrush);
}


void ListClass::SetFeldFocus (HDC hdc, int zeile, int spalte)
/**
Focus in ein Feldsetzen.
**/
{
	      RECT rect;
		  HBRUSH hBrush;
		  int diff;


		  if (InMove) return;

		  DestroyFocusWindow ();
		  if (ListFocus == 0) return;

          PrintUSlinesSatzNr (hdc);

          DataForm.mask[spalte].item->PrintComment ();
		  if (ListFocus == 9)
		  {
		           SetFeldFrame (hdc, zeile, spalte);
                   PrintSlinesSatzNr (hdc);
		           return;
		  }

		  if (ListFocus == 2)
		  {
		           SetFeldEdit0 (zeile, spalte, NULL);
                   PrintSlinesSatzNr (hdc);
		           return;
		  }
		  else if (ListFocus == 3)
		  {
		           SetFeldEdit (zeile, spalte);
                   PrintSlinesSatzNr (hdc);
		           return;
		  }
		  else if (ListFocus == 4)
		  {

			       if (InvalidateFocusFrame)
				   {
					   InvalidateRect (mamain3, &FocusRect, TRUE);
				   }

		           SetFeldEdit0 (zeile, spalte, NULL);
                   SetFocusFrame (hdc, zeile, spalte);
                   PrintSlinesSatzNr (hdc);
		           return;
		  }
		  else if (ListFocus == 5)
		  {
		           SetFeldEdit0 (zeile, spalte, WS_BORDER);
                   PrintSlinesSatzNr (hdc);
		           return;
		  }

          if (zeile >= recanz) return;
		  if (spalte >= banz) return;
          if (spalte < LineForm.frmstart) return; 

          diff = 0;
          if (LineForm.frmstart)
		  {
//            diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
//                   fSatzNr.mask[0].length;
            if (NoRecNr == FALSE)
            {
                 diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                   fSatzNr.mask[0].length;
            }
            else
            {
                 diff = LineForm.mask[LineForm.frmstart - 1].pos[1];
            }
		  }
          if (UseZiel == 0)
          {
                   strcpy (AktItem, DataForm.mask[spalte].item->GetItemName ());
          }

		  clipped (AktItem);
          memcpy (ausgabesatz, SwSaetze [zeile], zlen);
		  strcpy (AktValue, 
			      DataForm.mask[spalte].item->GetFeldPtr ());
//		  rect.top = (zeile - scrollpos + 1) * tm.tmHeight;
		  rect.top = (int) (double) ((double) UbHeight + 
			                          (zeile - scrollpos) * 
									  tm.tmHeight * recHeight * RowHeight);
          rect.top += (int) (double) ((double) 
			  DataForm.mask[spalte].pos[0] * tm.tmHeight * RowHeight);
		  rect.top = GetMidPos (rect.top);
		  rect.bottom = rect.top + tm.tmHeight;
		  rect.left = (DataForm.mask[spalte].pos[1] - diff) *
			          tm.tmAveCharWidth;
          if (RowMode == 0)
          {
		        rect.right = rect.left + DataForm.mask[spalte].length *
			                 tm.tmAveCharWidth;  
          }
/*
          else
          {
                rect.right = rect.left + LstZiel[zeile].feldlen *
                             tm.tmAveCharWidth;
          }
*/
		  memcpy (&FocusRect, &rect, sizeof (RECT));


          hBrush = CreateSolidBrush (BLUECOL);
          HBRUSH oldBrush = SelectObject (hdc, GetStockObject (PS_DASH));
          FrameRect (hdc, &rect, hBrush);
          DeleteObject (hBrush);
		  AktRow    = zeile;
          AktColumn = spalte;
          PrintSlinesSatzNr (hdc);
          SelectObject (hdc, oldBrush);
          DeleteObject (hBrush);
}

int ListClass::DelRec (void)
/**
Satz aus Liste loeschen.
**/
{
          int i;
 		  SCROLLINFO scinfo;
          
          if (ListFocus == 0) return recanz;

          recanz --;
          for (i = AktRow; i < recanz; i ++)
          {
              SwSaetze[i] = SwSaetze[ i + 1];
          }
          
          if (mamain3)
          {
             SendMessage (mamain2, WM_SIZE, NULL, NULL);
             InvalidateRect (mamain3, 0, TRUE);
             TestVTrack ();
             if (vTrack)
             {
 		          scinfo.cbSize = sizeof (SCROLLINFO);
		          scinfo.fMask  = SIF_PAGE | SIF_RANGE;
		          scinfo.nMin   = 0;
		          scinfo.nMax   = recanz - 1;
		          if (lPagelen)
                  {
		             scinfo.nPage  = 1;
                  }
		          else
                  {
			         scinfo.nPage = 0;
                  }
                  SetScrollInfo  (vTrack, SB_CTL, &scinfo, TRUE);
             }
          }
          
          return recanz;
}

BOOL ListClass::IsEditColumn (int col)
/**
Test, ob die mit der Maus aktivierte Spalte ein Edit-Feld ist.
**/
{
          if (DataForm.mask[col].attribut == EDIT ||
              (DataForm.mask[col].attribut & COMBOBOX))              
          {
              return TRUE;
          }
          if (DataForm.mask[col].attribut == BUTTON)
          {
              return TRUE;
          }
          if (DataForm.mask[col].attribut & CHECKBUTTON)
          {
              return TRUE;
          }
          return FALSE;
}

BOOL ListClass::IsButtonCol (int col)
/**
Test, ob die mit der Maus aktivierte Spalte ein Edit-Feld ist.
**/
{
          if (DataForm.mask[col].attribut == BUTTON)
          {
              return TRUE;
          }
          else if (DataForm.mask[col].attribut & CHECKBUTTON)
          {
              return TRUE;
          }
          return FALSE;
}

BOOL ListClass::IsButtonColumn (int col)
/**
Test, ob die mit der Maus aktivierte Spalte ein Edit-Feld ist.
**/
{
          if (DataForm.mask[col].attribut == BUTTON)
          {
              return TRUE;
          }
          return FALSE;
}


BOOL ListClass::IsCheckColumn (int col)
{
          if (DataForm.mask[col].attribut & CHECKBUTTON)
          {
              CheckButton (DataForm.mask[col].item);
              return TRUE;
          }
          return FALSE;
}


int ListClass::FirstColumn (void)
/**
N�chste Spalte suchen.
**/
{

          int i;
//          int h;

          i = 0;

/*
          for (h = 0; h < recHeight; h ++)
          {
            while (TRUE)
            {

              if (DataForm.mask[i].pos[0] != h) 
              {
                  i ++;
                  continue;
              }
              if (DataForm.mask[i].attribut == EDIT)
              {
                  return i;
              }
              i ++;
              if (i == DataForm.fieldanz)
              {
                  return 0;
              }
            }
          }
*/
          while (TRUE)
          {

              if (DataForm.mask[i].attribut == EDIT ||
                  (DataForm.mask[i].attribut & COMBOBOX) ||
                  (DataForm.mask[i].attribut & CHECKBUTTON))              
              {
                  return i;
              }
              i ++;
              if (i == DataForm.fieldanz)
              {
                  return 0;
              }
          }
          return i;
}

int ListClass::FirstColumnR (void)
/**
N�chste Spalte suchen.
**/
{

          int i;

          i = 0;

          while (TRUE)
          {
              if (DataRow->mask[i].attribut == EDIT ||
                  (DataForm.mask[i].attribut & COMBOBOX) ||
                  (DataForm.mask[i].attribut & CHECKBUTTON))              
              {
                  return i;
              }
              i ++;
              if (i == DataRow->fieldanz)
              {
                  return 0;
              }
          }
          return i;
}

          
int ListClass::PriorColumn (void)
/**
N�chste Spalte suchen.
**/
{

          int i;

          i = AktColumn;

          while (TRUE)
          {
              i --;
              if (i < 0)
              {
                  i = DataForm.fieldanz - 1;
              }
              if (DataForm.mask[i].attribut == EDIT ||
                  (DataForm.mask[i].attribut & COMBOBOX) ||
                  (DataForm.mask[i].attribut & CHECKBUTTON))              
              {
                  return i;
              }
          }
          return i;
}

int ListClass::PriorColumnR (void)
/**
N�chste Spalte suchen.
**/
{

          int i;

          i = AktRow;

          while (TRUE)
          {
              i --;
              if (i < 0)
              {
                  i = DataRow->fieldanz - 1;
              }
              if (DataRow->mask[i].attribut == EDIT ||
                  (DataForm.mask[i].attribut & COMBOBOX) ||
                  (DataForm.mask[i].attribut & CHECKBUTTON))              
              {
                  return i;
              }
          }
          return i;
}


void ListClass::FocusLeft (void)
/**
Focus nachh rechts setzen.
**/
{
          if (RowMode) 
          {
              PriorPageRow ();
              return;
          }

	      AktColumn = PriorColumn ();
		  while (AktColumn < DataForm.frmstart + HscrollStart) 
          {
              ScrollLeft ();
          }
          InvalidateRect (mamain3, &FocusRect, TRUE);
          UpdateWindow (mamain3);
		  SetFeldFocus0 (AktRow, AktColumn);
}

		  
int ListClass::NextColumn (void)
/**
N�chste Spalte suchen.
**/
{

          int i;

          i = AktColumn;

          while (TRUE)
          {
              i ++;
              if (i == DataForm.fieldanz)
              {
                  i = 0;

              }
              if (DataForm.mask[i].attribut == EDIT ||
                  (DataForm.mask[i].attribut & COMBOBOX) ||
                  (DataForm.mask[i].attribut & CHECKBUTTON))
              {
                  return i;
              }
          }
          return i;
}

int ListClass::NextColumnR (void)
/**
N�chste Spalte suchen.
**/
{

          int i;

          i = AktRow;

          while (TRUE)
          {
              i ++;
              if (i == DataRow->fieldanz)
              {
                  i = 0;

              }
              if (DataRow->mask[i].attribut == EDIT ||
                  (DataForm.mask[i].attribut & COMBOBOX) ||
                  (DataForm.mask[i].attribut & CHECKBUTTON))              
              {
                  return i;
              }
          }
          return i;
}


void ListClass::FocusRight (void)
/**
Focus nachh rechts setzen.
**/
{
          RECT rect;
          int x;
          int diff;
          int col;
          int Scrolled;

          if (RowMode)
          {
              NextPageRow ();
              return;
          }

		  if (syskey != KEYCR && AktColumn >= banz - 1) return;

		  col = NextColumn ();
          if (syskey == KEYCR && col <= AktColumn)
          {
/*
                    AktColumn = FirstColumn ();
 		            while (DataForm.frmstart > 0) 
                    {
                              ScrollLeft ();
                    }
*/

                    FocusDown ();
                    return;
          }
             

          AktColumn = col;
		  while (AktColumn < DataForm.frmstart + HscrollStart) 
          {
              ScrollLeft ();
          }

          GetClientRect (mamain3, &rect);

          diff = Scrolled = 0;
          if (LineForm.frmstart)
          {
//                 diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
//                        fSatzNr.mask[0].length;
			       if (NoRecNr == FALSE)
				   {
                            diff = LineForm.mask[LineForm.frmstart - 1 + HscrollStart].pos[1] -
                                              fSatzNr.mask[0].length;
				   }
				   else
				   {
                            diff = LineForm.mask[LineForm.frmstart - 1 + HscrollStart].pos[1];
				   }

                   if (HscrollStart > 0)
                   {
                            diff -= LineForm.mask[HscrollStart - 1].pos[1];
                   }

          }


          x = (DataForm.mask[AktColumn].pos[1] - diff + 
               DataForm.mask[AktColumn].length) *
                   tm.tmAveCharWidth;
          if (x <= rect.right)
          {
              InvalidateRect (mamain3, &FocusRect, TRUE);
	  	      UpdateWindow (mamain3);
 		      SetFeldFocus0 (AktRow, AktColumn);
              return;
          }
          ScrollRight ();
//        diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
//               fSatzNr.mask[0].length;
  		  if (NoRecNr == FALSE)
		  {
                    diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                             fSatzNr.mask[0].length;
		  }
		  else
		  {
                    diff = LineForm.mask[LineForm.frmstart - 1].pos[1];
		  }

/*
          if (HscrollStart > 0)
          {
                    diff -= LineForm.mask[HscrollStart - 1].pos[1];
          }
*/


          x = (DataForm.mask[AktColumn].pos[1] - diff + 
               DataForm.mask[AktColumn].length) *
                   tm.tmAveCharWidth;
          while (x > rect.right)
          {
              ScrollRight ();
//              diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
//                          fSatzNr.mask[0].length;
			  if (NoRecNr == FALSE)
			  {
                       diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                                  fSatzNr.mask[0].length;
			  }
			  else
			  {
                       diff = LineForm.mask[LineForm.frmstart - 1].pos[1];
			  }
/*
              if (HscrollStart > 0)
              {
                        diff -= LineForm.mask[HscrollStart - 1].pos[1];
              }
*/


              x = (DataForm.mask[AktColumn].pos[1] - diff + 
                       DataForm.mask[AktColumn].length) *
                       tm.tmAveCharWidth;
          }
	      SetFeldFocus0 (AktRow, AktColumn);
}
		  
	        
void ListClass::FocusUp (void)
/**
Focus nachh rechts setzen.
**/
{
          RECT rect;
          int Col;
          
		  if (RowMode == 0 && AktRow <= 0) return;

          InAppend = 0;
          NewRec = 0;
          SetInsAttr ();
          Col = FirstColumn ();
          if (AktColumn < Col)
          {
                  SetPos (AktRow, Col);
          }

          GetClientRect (mamain3, &rect);
          rect.top += UbHeight;
		  if (NoRecNr == FALSE)
		  {
                    rect.left += fSatzNr.mask[0].length * tm.tmAveCharWidth;
		  }
          if (RowMode)
          {
                 AktRow = PriorColumnR ();
          }
          else
          {
                 if (TestAfterRow () != -1)
                 {
		                  AktRow --;
                 }
          }
		  if (AktRow < scrollpos)
          {
                    InvalidateRect (mamain3, &rect, TRUE);
                    ScrollUp (); 
          }
          else
          {
                    InvalidateRect (mamain3, &FocusRect, TRUE);
                    UpdateWindow (mamain3);
          }
          if (RowMode)
          {
                 TestBeforeR ();
          }
          else
          {
                 memcpy (ausgabesatz, SwSaetze [AktRow], zlen); 
                 TestBeforeRow ();
          }
		  SetFeldFocus0 (AktRow, AktColumn);
}


int ListClass::TestBeforeRow (void)
/**
Pruefung beim Verlassen der Zeile.
**/
{
         int ret;

         if (Keys)
         {
             if ((ret = Keys->BeforeRow (AktRow)) != 0)
             {
                 return ret;
             }
         }
         if (DataForm.before)
         {
             return (*DataForm.before) ();
         }
         return 0;
}
          

int ListClass::TestAfterRow (void)
/**
Pruefung beim Verlassen der Zeile.
**/
{
         int ret;

         if (TestAfter () == -1)
         {
             return (-1);
         }
         if (Keys)
         {
             if ((ret = Keys->AfterRow (AktRow)) != 0)
             {
                 return ret;
             }
         }

         if (DataForm.after)
         {
             return (*DataForm.after) ();
         }
         return 0;
}

void ListClass::SetFieldAttr (char *fname, int attr)
{
         int i;
         char *feldname;

         for (i = 0; i < DataForm.fieldanz; i ++)
         {
             feldname = DataForm.mask[i].item->GetItemName ();
             if (feldname && strcmp (feldname, fname) == 0)
             {
                   break;
             }
         }
         if (i == DataForm.fieldanz) return;

         DataForm.mask[i].attribut = attr;
}


void ListClass::SetFieldAttr (CHATTR *cha)
{
         int i;
         char *feldname;

         for (i = 0; i < DataForm.fieldanz; i ++)
         {
             feldname = DataForm.mask[i].item->GetItemName ();
             if (feldname && strcmp (feldname, cha->feldname) == 0)
             {
                   break;
             }
         }
         if (i == DataForm.fieldanz) return;

         if (NewRec || InAppend)
         {
                DataForm.mask[i].attribut = cha->InsAttr;
         }
         else
         {
                DataForm.mask[i].attribut = cha->UpdAttr;
         }
}
             
void ListClass::SetInsAttr (void)
/**
Feldattribute wechseln.
**/
{
         int i;

         if (ChAttr == NULL) return;

         for (i = 0; ChAttr[i].feldname; i ++)
         {
             SetFieldAttr (&ChAttr[i]);
         }
}


BOOL ListClass::IsInsButton (char *item)
{
         if (ChAttr == NULL) return FALSE;

         for (int i = 0; ChAttr[i].feldname; i ++)
         {
             if (ChAttr[i].InsAttr != BUTTON)
             {
                 continue;
             }
             if (strcmp (ChAttr[i].feldname, item) == 0)
             {
                      return TRUE;
             }
         }
         return FALSE;
}


BOOL ListClass::InInsAttr (char *item, int pos)
{
         if (ChAttr == NULL) return FALSE;

         for (int i = 0; ChAttr[i].feldname; i ++)
         {
             if (ChAttr[i].InsAttr != BUTTON)
             {
                 continue;
             }
             if (strcmp (ChAttr[i].feldname, item) == 0)
             {
                  if (ChAttr[i].UpdAttr != DISPLAYONLY &&
                      ChAttr[i].UpdAttr != REMOVED)
                  {
                      return FALSE;
                  }
                  else if (pos == AktRow)
                  {
                      return FALSE;
                  }

                  else
                  {
                      return TRUE;
                  }
             }
         }
         return FALSE;
}


void ListClass::FocusDown (void)
/**
Focus nachh unten setzen.
**/
{
          RECT rect;
          int append = 0;


		  if (AktRow >= recanz - 1)
          {

              if (RowMode)
              {
                  if (syskey == KEYCR)
                  {
             	      SetFeldFocus0 (AktRow, AktColumn);
                  }
                  return;
              }
              append = 1;
          }
          else if (InAppend && RowMode == 0)
          {
              append = 1;
          }

          SetInsAttr ();
          GetClientRect (mamain3, &rect);
          rect.top += UbHeight;
		  if (NoRecNr == FALSE)
		  {
                    rect.left += fSatzNr.mask[0].length * tm.tmAveCharWidth;
		  }
          if (RowMode)
          {
                 AktRow = NextColumnR ();
          }
          else
          {
                 if (append)
                 {
                      if (TestAfterRow () != -1)
                      {
                                 AktColumn = FirstColumn ();
   		                         while (DataForm.frmstart > 0) 
                                 {
                                        ScrollLeft ();
                                 }
                                 AppendLine ();
                      }
                 }
                 else
                 {
                      if (TestAfterRow () != -1)
                      {
		                        AktRow ++;
                      }
                      if (syskey == KEYCR)
                      {
                                AktColumn = FirstColumn ();
  		                        while (DataForm.frmstart > 0) 
                                {
                                          ScrollLeft ();
                                }
                      }
                 }
          }

          if (lPagelen < recanz && 
			  AktRow - scrollpos >= lPagelen)
          {
//              InvalidateRect (mamain3, &rect, TRUE);
              InvalidateRect (mamain3, &FocusRect, TRUE);
	   	      UpdateWindow (mamain3);
              ScrollDown ();
          }
          else
          {
              InvalidateRect (mamain3, &FocusRect, TRUE);
	   	      UpdateWindow (mamain3);
          }
          if (RowMode)
          {
                 TestBeforeR ();
          }
          else
          {
                 memcpy (ausgabesatz, SwSaetze [AktRow], zlen); 
                 TestBeforeRow ();
          }
	      SetFeldFocus0 (AktRow, AktColumn);
}
  
    

void ListClass::SetFeldFocus0 (int zeile, int spalte)
/**
Focus in ein Feldsetzen.
**/
{
	    HDC hdc;

        if (DataForm.mask[spalte].attribut != EDIT &&
            DataForm.mask[spalte].attribut != COMBOBOX)
        {
            AktColumn = FirstColumn ();
        }
		hdc = GetDC (mamain3);
        SetFeldFocus (hdc, zeile, spalte);
        ReleaseDC (mamain3, hdc);
        SelRow = AktRow;
}


void ListClass::SetDataStart (void)
/**
frmstart bei DataForms erhoehen.
**/
{

        DataForm.frmstart = UbForm.frmstart;
        LineForm.frmstart = UbForm.frmstart;
}

BOOL ListClass::MustPaint (int zeile)
/**
Feststellen, ob der Bereich neu gezeichnet werden muss.
**/
{
         int py;
         PAINTSTRUCT *ps;
         int Painttop;
         int Paintbottom;

         if (paintallways) return TRUE;

         ps = &aktpaint;

/* Update-Region ermitteln.                              */

         py =  zeile;

         Painttop    = ps->rcPaint.top    -  tm.tmHeight;
         Paintbottom = ps->rcPaint.bottom +  tm.tmHeight;

         if (py < Painttop) return FALSE;
         if (py > Paintbottom) return FALSE;
         return TRUE;
}

void ListClass::PrintUSlinesSatzNr (HDC hdc)
/**
Horizontale Linien zeichnen.
**/
{
        static HPEN hPen;
        static HPEN hPenG;
        static HPEN hPenW;
        HPEN oldPen;
        int i;
        int x, y, y1;
		int RHeight;

        if (NoRecNr) return;

		RHeight = (int) (double) ((double) tm.tmHeight * recHeight * RowHeight + 0.5);

        if (recanz == 0) return;
        if (SelRow >= recanz) return;
        x = fSatzNr.mask[0].length * tm.tmAveCharWidth - 1;

        hPen  = CreatePen (PS_SOLID, 0, LTGRAYCOL);
        hPenG = CreatePen (PS_SOLID, 0, GRAYCOL);
        hPenW = CreatePen (PS_SOLID, 0, WHITECOL);

        oldPen = SelectObject (hdc, hPen);
        i = SelRow - scrollpos;

        SelectObject (hdc, hPenW);

        y = (int) (double) ((double) UbHeight + i * 
			       tm.tmHeight * recHeight * RowHeight);

//        y = UbHeight + i * RHeight;
        y1 = y;
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x, y);

        SelectObject (hdc, hPen);
        y ++;
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x, y);
 
        SelectObject (hdc, hPenG);

        y = (int) (double) ((double) 
			UbHeight + (i + 1) * tm.tmHeight * recHeight * RowHeight 
			       - 1);

//		y += (RHeight - 1);
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x, y);

        SelectObject (hdc, hPen);
        y --;
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x, y);

        SelectObject (hdc, hPen);
        y --;
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x - 1, y);


        SelectObject (hdc, hPenG);
        MoveToEx (hdc, x - 1, y1, NULL);
        LineTo (hdc,   x - 1, y + 1);

        SelectObject (hdc, hPen);
        MoveToEx (hdc, x - 2, y1, NULL);
        LineTo (hdc,   x - 2, y + 1);

        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
        DeleteObject (hPenG);
        DeleteObject (hPenW);
}

void ListClass::PrintSlinesSatzNr (HDC hdc)
/**
Vertikale Linien zeichnen.
**/
{
        static HPEN hPen;
        static HPEN hPenG;
        static HPEN hPenW;
        HPEN oldPen;
        int i;
        int x, y, y1;
		int RHeight;

        if (NoRecNr) return;
		RHeight = (int) (double) ((double) tm.tmHeight * recHeight * RowHeight + 0.5);

        if (recanz == 0) return;
        if (AktRow >= recanz) return;
        x = fSatzNr.mask[0].length * tm.tmAveCharWidth - 1;

        hPen  = CreatePen (PS_SOLID, 0, BLACKCOL);
        hPenG = CreatePen (PS_SOLID, 0, GRAYCOL);
        hPenW = CreatePen (PS_SOLID, 0, WHITECOL);

        oldPen = SelectObject (hdc, hPen);
        i = AktRow - scrollpos;

        SelectObject (hdc, hPenW);

        y = (int) (double) ((double) UbHeight + 
			       i * tm.tmHeight * recHeight * RowHeight);

//        y = UbHeight + i * RHeight;
        y1 = y;
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x, y);

        y ++;
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x - 1, y);
 
        SelectObject (hdc, hPen);

        y = (int) (double) ((double) UbHeight + 
			 (i + 1) * tm.tmHeight * recHeight * RowHeight - 1);

//		y += RHeight;
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x, y);

        y --;
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x, y);

        SelectObject (hdc, hPenG);
        y --;
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x - 1, y);

        SelectObject (hdc, hPen);
        MoveToEx (hdc, x, y1, NULL);
        LineTo (hdc,   x, y + 1);

        SelectObject (hdc, hPen);
        MoveToEx (hdc, x - 1, y1, NULL);
        LineTo (hdc,   x - 1, y + 1);

        SelectObject (hdc, hPenG);
        MoveToEx (hdc, x - 2, y1, NULL);
        LineTo (hdc,   x - 2, y + 1);

         
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
        DeleteObject (hPenG);
        DeleteObject (hPenW);
}

/*
void ListClass::PrintVlineSatzNr (HDC hdc)
/?*
Verikale Linien zeichnen.
*?/
{
        static HPEN hPen;
        HPEN oldPen;
        int x, y;
        hPen = CreatePen (PS_SOLID, 0, BLACKCOL);
        oldPen = SelectObject (hdc, hPen);

        if (NoRecNr) return;

        x = fSatzNr.mask[0].length * tm.tmAveCharWidth - 1;
        y = (int) (double) ((double) UbHeight + 
			recanz * tm.tmHeight * recHeight * RowHeight);
        MoveToEx (hdc, x, tm.tmHeight, NULL);
        LineTo (hdc, x, y);
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);

        hPen = CreatePen (PS_SOLID, 0, GRAYCOL);
        oldPen = SelectObject (hdc, hPen);

        x = fSatzNr.mask[0].length * tm.tmAveCharWidth - 2;
        y = (int) (double) ((double) UbHeight + 
			recanz * tm.tmHeight * recHeight * RowHeight);
        MoveToEx (hdc, x, tm.tmHeight, NULL);
        LineTo (hdc, x, y);
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
}
*/

void ListClass::PrintVlineSatzNr (HDC hdc)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        HPEN oldPen;
        int x, y;


        if (NoRecNr) return;
        hPen = CreatePen (PS_SOLID, 0, BLACKCOL);
        oldPen = SelectObject (hdc, hPen);

        x = fSatzNr.mask[0].length * tm.tmAveCharWidth - 1;
        y = (int) (double) ((double) UbHeight + 
			recanz * tm.tmHeight * recHeight * RowHeight);
        MoveToEx (hdc, x, tm.tmHeight, NULL);
        LineTo (hdc, x, y);
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);

        hPen = CreatePen (PS_SOLID, 0, GRAYCOL);
        oldPen = SelectObject (hdc, hPen);

        x = fSatzNr.mask[0].length * tm.tmAveCharWidth - 2;
        y = (int) (double) ((double) UbHeight + 
			recanz * tm.tmHeight * recHeight * RowHeight);
        MoveToEx (hdc, x, tm.tmHeight, NULL);
        LineTo (hdc, x, y);
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
}


void ListClass::SetListLines (int cnr)
{
        LineColor = cnr;
        if (mamain3 == NULL) return;
        InvalidateRect (mamain3, NULL, TRUE);
        UpdateWindow (mamain3);
}


void ListClass::PrintNoHscroll3DLines (HDC hdc)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        HPEN oldPen;
        int i;
        int x, y;
        int diff;

        static COLORREF lcol[] = {WHITECOL, LTGRAYCOL, BLACKCOL, 
            GRAYCOL, WHITECOL, LTGRAYCOL, BLACKCOL, GRAYCOL};

        PrintNoHscrollLines (hdc);
        return;

        if (LineForm.mask == NULL) return;

        diff = 0;
        hPen = CreatePen (PS_SOLID, 0, WHITECOL);
        oldPen = SelectObject (hdc, hPen);

        for (i = 0; i < HscrollStart; i ++)
        {
              x = (LineForm.mask[i].pos[1] - diff) * 
                   tm.tmAveCharWidth;
			  x -= Plus3D;
              y = (int) (double) ((double) UbHeight + 
				        recanz * tm.tmHeight * recHeight * RowHeight);
              MoveToEx (hdc, x, UbHeight,    NULL);
              LineTo (hdc, x, y);
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);

        hPen = CreatePen (PS_SOLID, 0, LTGRAYCOL);
        oldPen = SelectObject (hdc, hPen);

		if (Plus3D > 1)
		{
          for (i = 0; i < HscrollStart; i ++)
		  {
              x = (LineForm.mask[i].pos[1] - diff) * 
                   tm.tmAveCharWidth;
			  x -= 1;
              y = (int) (double) ((double) UbHeight + 
				        recanz * tm.tmHeight * recHeight * RowHeight);
              MoveToEx (hdc, x, UbHeight,    NULL);
              LineTo (hdc, x, y);
		  }
        }
        for (i = 0; i < HscrollStart; i ++)
        {
              x = (LineForm.mask[i].pos[1] - diff) * 
                   tm.tmAveCharWidth;
              y = (int) (double) ((double) UbHeight + 
				        recanz * tm.tmHeight * recHeight * RowHeight);
              MoveToEx (hdc, x, UbHeight,    NULL);
              LineTo (hdc, x, y);
        }
		if (Plus3D > 1)
		{
          for (i = 0; i < HscrollStart; i ++)
		  {
              x = (LineForm.mask[i].pos[1] - diff) * 
                   tm.tmAveCharWidth;
			  x += 1;
              y = (int) (double) ((double) UbHeight + 
				        recanz * tm.tmHeight * recHeight * RowHeight);
              MoveToEx (hdc, x, UbHeight,    NULL);
              LineTo (hdc, x, y);
		  }
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);

        hPen = CreatePen (PS_SOLID, 0, BLACKCOL);
        oldPen = SelectObject (hdc, hPen);

        for (i = 0; i < HscrollStart; i ++)
        {
              x = (LineForm.mask[i].pos[1] - diff) * 
                   tm.tmAveCharWidth;
			  x += Plus3D;
              y = (int) (double) ((double) UbHeight + 
				        recanz * tm.tmHeight * recHeight * RowHeight);
              MoveToEx (hdc, x, UbHeight,    NULL);
              LineTo (hdc, x, y);
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);

        hPen = CreatePen (PS_SOLID, 0, LTGRAYCOL);
        oldPen = SelectObject (hdc, hPen);

        Print3DHlinesEnd (hdc);
        PrintVlineSatzNr (hdc);

        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
}


void ListClass::Print3DVlines (HDC hdc)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        HPEN oldPen;
        int i;
        int x, y;
        int diff;

        static COLORREF lcol[] = {WHITECOL, LTGRAYCOL, BLACKCOL, 
            GRAYCOL, WHITECOL, LTGRAYCOL, BLACKCOL, GRAYCOL};


        PrintVlineSatzNr (hdc);
        if (LineForm.mask == NULL) return;

        if (HscrollStart > 0)
        {
            PrintNoHscroll3DLines (hdc);
        }

        diff = 0;
        if (LineForm.frmstart)
        {


            diff = LineForm.mask[LineForm.frmstart + HscrollStart - 1].pos[1];

            if (HscrollStart > 0)
            {
                diff -= LineForm.mask[HscrollStart - 1].pos[1];
            }

			else if (NoRecNr == 0)
			{
                diff -= fSatzNr.mask[0].length;
            }

        }
        hPen = CreatePen (PS_SOLID, 0, WHITECOL);
        oldPen = SelectObject (hdc, hPen);

        for (i = LineForm.frmstart + HscrollStart; i < LineForm.fieldanz; i ++)
        {
              x = (LineForm.mask[i].pos[1] - diff) * 
                   tm.tmAveCharWidth;
			  x -= Plus3D;
              y = (int) (double) ((double) UbHeight + 
				        recanz * tm.tmHeight * recHeight * RowHeight);
              MoveToEx (hdc, x, UbHeight,    NULL);
              LineTo (hdc, x, y);
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);

        hPen = CreatePen (PS_SOLID, 0, LTGRAYCOL);
        oldPen = SelectObject (hdc, hPen);

		if (Plus3D > 1)
		{
          for (i = LineForm.frmstart + HscrollStart; i < LineForm.fieldanz; i ++)
		  {
              x = (LineForm.mask[i].pos[1] - diff) * 
                   tm.tmAveCharWidth;
			  x -= 1;
              y = (int) (double) ((double) UbHeight + 
				        recanz * tm.tmHeight * recHeight * RowHeight);
              MoveToEx (hdc, x, UbHeight,    NULL);
              LineTo (hdc, x, y);
		  }
        }
        for (i = LineForm.frmstart + HscrollStart; i < LineForm.fieldanz; i ++)
        {
              x = (LineForm.mask[i].pos[1] - diff) * 
                   tm.tmAveCharWidth;
              y = (int) (double) ((double) UbHeight + 
				        recanz * tm.tmHeight * recHeight * RowHeight);
              MoveToEx (hdc, x, UbHeight,    NULL);
              LineTo (hdc, x, y);
        }
		if (Plus3D > 1)
		{
          for (i = LineForm.frmstart + HscrollStart; i < LineForm.fieldanz; i ++)
		  {
              x = (LineForm.mask[i].pos[1] - diff) * 
                   tm.tmAveCharWidth;
			  x += 1;
              y = (int) (double) ((double) UbHeight + 
				        recanz * tm.tmHeight * recHeight * RowHeight);
              MoveToEx (hdc, x, UbHeight,    NULL);
              LineTo (hdc, x, y);
		  }
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);

        hPen = CreatePen (PS_SOLID, 0, BLACKCOL);
        oldPen = SelectObject (hdc, hPen);

        for (i = LineForm.frmstart + HscrollStart; i < LineForm.fieldanz; i ++)
        {
              x = (LineForm.mask[i].pos[1] - diff) * 
                   tm.tmAveCharWidth;
			  x += Plus3D;
              y = (int) (double) ((double) UbHeight + 
				        recanz * tm.tmHeight * recHeight * RowHeight);
              MoveToEx (hdc, x, UbHeight,    NULL);
              LineTo (hdc, x, y);
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);

        hPen = CreatePen (PS_SOLID, 0, LTGRAYCOL);
        oldPen = SelectObject (hdc, hPen);

        Print3DHlinesEnd (hdc);
        PrintVlineSatzNr (hdc);

        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
}



void ListClass::PrintNoHscrollLines (HDC hdc)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        static HPEN hiPen;
        static HPEN loPen;
        RECT rect;
        HPEN oldPen;
        int i;
        int x, y;
        int diff;

        static COLORREF lcol[] = {WHITECOL, LTGRAYCOL, BLACKCOL, 
            GRAYCOL, WHITECOL, LTGRAYCOL, BLACKCOL, GRAYCOL};

        if (LineColor > 11) return;
        if (LineColor > 7) return;
        if (LineForm.mask == NULL) return;


        diff = 0;
//        hPen = CreatePen (PS_SOLID, 0, lcol[LineColor]);

        hPen = CreatePen (PS_SOLID,  0, GRAYCOL);
        hiPen = CreatePen (PS_SOLID, 0, BLACKCOL);
        loPen = CreatePen (PS_SOLID, 0, WHITECOL);
        oldPen = SelectObject (hdc, hPen);


        for (i = 0; i < HscrollStart; i ++)
        {
              SelectObject (hdc, hPen);
              x = (LineForm.mask[i].pos[1] - diff) * 
                   tm.tmAveCharWidth;
              y = (int) (double) ((double) UbHeight + 
				        recanz * tm.tmHeight * recHeight * RowHeight);
              MoveToEx (hdc, x - 2, UbHeight,    NULL);
              LineTo (hdc, x - 2, y);
              SelectObject (hdc, hiPen);
              MoveToEx (hdc, x - 1, UbHeight,    NULL);
              LineTo (hdc,   x - 1, y);
        }

        GetClientRect (mamain3, &rect);

        for (i = 0; i < recanz; i ++)
        {

              SelectObject (hdc, loPen);
              y = (int) (double) ((double) UbHeight + 
				  i * tm.tmHeight * recHeight * RowHeight) + 1;
              if (MustPaint (y))
              {
                     MoveToEx (hdc, 0, y, NULL);
                     LineTo (hdc, x, y);
              }
              i ++;
              SelectObject (hdc, hPen);
              y = (int) (double) ((double) UbHeight + 
				  i * tm.tmHeight * recHeight * RowHeight) - 1;
              if (MustPaint (y))
              {
                     MoveToEx (hdc, 0, y, NULL);
                     LineTo (hdc, x, y);
              }
              SelectObject (hdc, hiPen);
              y ++;
              if (MustPaint (y))
              {
                     MoveToEx (hdc, 0, y, NULL);
                     LineTo (hdc, x, y);
              }
              i --;
        }


        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
        DeleteObject (hiPen);
        DeleteObject (loPen);
}

     
void ListClass::PrintVlines (HDC hdc)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        HPEN oldPen;
        int i;
        int x, y;
        int diff;

        static COLORREF lcol[] = {WHITECOL, LTGRAYCOL, BLACKCOL, 
            GRAYCOL, WHITECOL, LTGRAYCOL, BLACKCOL, GRAYCOL};

		if (print3D)
		{
                Print3DVlines (hdc);
				return;
		}

        PrintVlineSatzNr (hdc);
        if (LineColor > 11) return;
        if (LineColor > 7) return;
        if (LineForm.mask == NULL) return;

        if (HscrollStart > 0)
        {
            PrintNoHscrollLines (hdc);
        }

        diff = 0;
        if (LineForm.frmstart)
        {


            diff = LineForm.mask[LineForm.frmstart + HscrollStart - 1].pos[1];

            if (HscrollStart > 0)
            {
                diff -= LineForm.mask[HscrollStart - 1].pos[1];
            }

			else if (NoRecNr == 0)
			{
                diff -= fSatzNr.mask[0].length;
            }

        }
        hPen = CreatePen (PS_SOLID, 0, lcol[LineColor]);
        oldPen = SelectObject (hdc, hPen);


        for (i = LineForm.frmstart + HscrollStart; i < LineForm.fieldanz; i ++)
        {
              x = (LineForm.mask[i].pos[1] - diff) * 
                   tm.tmAveCharWidth;
              y = (int) (double) ((double) UbHeight + 
				        recanz * tm.tmHeight * recHeight * RowHeight);
              MoveToEx (hdc, x, UbHeight,    NULL);
              LineTo (hdc, x, y);
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
        PrintVlineSatzNr (hdc);
}

void ListClass::PrintPVlines (HDC hdc, RECT *rect)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        HPEN oldPen;
        int i;
        long x, y;
        int diff;
		SIZE size;

        static COLORREF lcol[] = {WHITECOL, LTGRAYCOL, BLACKCOL, 
            GRAYCOL, WHITECOL, LTGRAYCOL, BLACKCOL, GRAYCOL};


//        PrintVlineSatzNr (hdc);
		GetTextExtentPoint32 (hdc, "X", 1, &size);
        if (LineColor > 11) return;
        if (LineColor > 7) return;
        if (LineForm.mask == NULL) return;

    
    	rect->bottom += 10;
        diff = 0;
        if (LineForm.frmstart)
        {
			if (NoRecNr == 0)
			{
                diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                               fSatzNr.mask[0].length;
			}
			else
			{
                diff = LineForm.mask[LineForm.frmstart - 1].pos[1];
			}
        }
        hPen = CreatePen (PS_SOLID, 0, lcol[LineColor]);
        oldPen = SelectObject (hdc, hPen);

        for (i = 0; i < LineForm.fieldanz; i ++)
        {
              x = (LineForm.mask[i].pos[1] - diff) * 
                   tm.tmAveCharWidth;
              int plen = pagelen0;
              if (recanz - printscrollpos < plen)
              {
                  plen = recanz - printscrollpos;
              }
              y = (int) (double) ((double) UbHeight + 
				        plen * tm.tmHeight * recHeight * RowHeight);
              y += ublen * tm.tmHeight;
//			  if (y > rect->bottom) y = rect->bottom;
//              MoveToEx (hdc, x, UbHeight,    NULL);
              MoveToEx (hdc, x, ublen * tm.tmHeight,    NULL);
              LineTo (hdc, x, y);
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
//        PrintVlineSatzNr (hdc);
}


void ListClass::PrintNoHscroll3DLine (HDC hdc, int z)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        HPEN oldPen;
        int i;
        int x, y;
        int diff;

        static COLORREF lcol[] = {WHITECOL, LTGRAYCOL, BLACKCOL, 
            GRAYCOL, WHITECOL, LTGRAYCOL, BLACKCOL, GRAYCOL};


        PrintNoHscrollLine (hdc, z);
        return;

        if (LineForm.mask == NULL) return;

        diff = 0;
        hPen = CreatePen (PS_SOLID, 0, WHITECOL);
        oldPen = SelectObject (hdc, hPen);

        for (i = 0; i < HscrollStart; i ++)
        {
              x = (LineForm.mask[i].pos[1] - diff) * 
                   tm.tmAveCharWidth;
	          x -= Plus3D;
              y = (int) (double) ((double) UbHeight + 
				  z * tm.tmHeight * recHeight * RowHeight);
              MoveToEx (hdc, x, y, NULL);
              y = (int) (double) ((double) UbHeight + 
				  (z  + 1) * tm.tmHeight * recHeight * RowHeight);
              LineTo (hdc, x, y);
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);

        hPen = CreatePen (PS_SOLID, 0, LTGRAYCOL);
        oldPen = SelectObject (hdc, hPen);

		if (Plus3D > 1)
		{
          for (i = 0; i < HscrollStart; i ++)
		  {
              x = (LineForm.mask[i].pos[1] - diff) * 
                   tm.tmAveCharWidth;
			  x -= 1;
              y = (int) (double) ((double) UbHeight + 
				  z * tm.tmHeight * recHeight * RowHeight);
              MoveToEx (hdc, x, y, NULL);
              y = (int) (double) ((double) UbHeight + 
				  (z  + 1) * tm.tmHeight * recHeight * RowHeight);
              LineTo (hdc, x, y);
		  }
        }
        for (i = 0; i < HscrollStart; i ++)
        {
              x = (LineForm.mask[i].pos[1] - diff) * 
                   tm.tmAveCharWidth;
              y = (int) (double) ((double) UbHeight + 
				  z * tm.tmHeight * recHeight * RowHeight);
              MoveToEx (hdc, x, y, NULL);
              y = (int) (double) ((double) UbHeight + 
				  (z  + 1) * tm.tmHeight * recHeight * RowHeight);
              LineTo (hdc, x, y);
              LineTo (hdc, x, y);
        }
		if (Plus3D > 1)
		{
          for (i = 0; i < HscrollStart; i ++)
		  {
              x = (LineForm.mask[i].pos[1] - diff) * 
                   tm.tmAveCharWidth;
			  x += 1;
              y = (int) (double) ((double) UbHeight + 
				  z * tm.tmHeight * recHeight * RowHeight);
              MoveToEx (hdc, x, y, NULL);
              y = (int) (double) ((double) UbHeight + 
				  (z  + 1) * tm.tmHeight * recHeight * RowHeight);
              LineTo (hdc, x, y);
              LineTo (hdc, x, y);
		  }
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);

        hPen = CreatePen (PS_SOLID, 0, BLACKCOL);
        oldPen = SelectObject (hdc, hPen);

        for (i = 0; i < HscrollStart; i ++)
        {
              x = (LineForm.mask[i].pos[1] - diff) * 
                   tm.tmAveCharWidth;
			  x += Plus3D;
              y = (int) (double) ((double) UbHeight + 
				  z * tm.tmHeight * recHeight * RowHeight);
              MoveToEx (hdc, x, y, NULL);
              y = (int) (double) ((double) UbHeight + 
				  (z  + 1) * tm.tmHeight * recHeight * RowHeight);
              LineTo (hdc, x, y);
              LineTo (hdc, x, y);
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);

        hPen = CreatePen (PS_SOLID, 0, LTGRAYCOL);
        oldPen = SelectObject (hdc, hPen);

        Print3DHlineEnd (hdc, z);
        PrintVlineSatzNr (hdc);

        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
}


void ListClass::Print3DVline (HDC hdc, int z)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        HPEN oldPen;
        int i;
        int x, y;
        int diff;

        static COLORREF lcol[] = {WHITECOL, LTGRAYCOL, BLACKCOL, 
            GRAYCOL, WHITECOL, LTGRAYCOL, BLACKCOL, GRAYCOL};


        PrintVlineSatzNr (hdc);
        if (LineForm.mask == NULL) return;
        if (HscrollStart > 0)
        {
            PrintNoHscroll3DLine (hdc, z);
        }

        diff = 0;
        if (LineForm.frmstart)
        {


            diff = LineForm.mask[LineForm.frmstart + HscrollStart - 1].pos[1];

            if (HscrollStart > 0)
            {
                diff -= LineForm.mask[HscrollStart - 1].pos[1];
            }

			else if (NoRecNr == 0)
			{
                diff -= fSatzNr.mask[0].length;
            }

        }
        hPen = CreatePen (PS_SOLID, 0, WHITECOL);
        oldPen = SelectObject (hdc, hPen);

        for (i = LineForm.frmstart + HscrollStart; i < LineForm.fieldanz; i ++)
        {
              x = (LineForm.mask[i].pos[1] - diff) * 
                   tm.tmAveCharWidth;
	          x -= Plus3D;
              y = (int) (double) ((double) UbHeight + 
				  z * tm.tmHeight * recHeight * RowHeight);
              MoveToEx (hdc, x, y, NULL);
              y = (int) (double) ((double) UbHeight + 
				  (z  + 1) * tm.tmHeight * recHeight * RowHeight);
              LineTo (hdc, x, y);
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);

        hPen = CreatePen (PS_SOLID, 0, LTGRAYCOL);
        oldPen = SelectObject (hdc, hPen);

		if (Plus3D > 1)
		{
          for (i = LineForm.frmstart + HscrollStart; i < LineForm.fieldanz; i ++)
		  {
              x = (LineForm.mask[i].pos[1] - diff) * 
                   tm.tmAveCharWidth;
			  x -= 1;
              y = (int) (double) ((double) UbHeight + 
				  z * tm.tmHeight * recHeight * RowHeight);
              MoveToEx (hdc, x, y, NULL);
              y = (int) (double) ((double) UbHeight + 
				  (z  + 1) * tm.tmHeight * recHeight * RowHeight);
              LineTo (hdc, x, y);
		  }
        }
        for (i = LineForm.frmstart + HscrollStart; i < LineForm.fieldanz; i ++)
        {
              x = (LineForm.mask[i].pos[1] - diff) * 
                   tm.tmAveCharWidth;
              y = (int) (double) ((double) UbHeight + 
				  z * tm.tmHeight * recHeight * RowHeight);
              MoveToEx (hdc, x, y, NULL);
              y = (int) (double) ((double) UbHeight + 
				  (z  + 1) * tm.tmHeight * recHeight * RowHeight);
              LineTo (hdc, x, y);
              LineTo (hdc, x, y);
        }
		if (Plus3D > 1)
		{
          for (i = LineForm.frmstart + HscrollStart; i < LineForm.fieldanz; i ++)
		  {
              x = (LineForm.mask[i].pos[1] - diff) * 
                   tm.tmAveCharWidth;
			  x += 1;
              y = (int) (double) ((double) UbHeight + 
				  z * tm.tmHeight * recHeight * RowHeight);
              MoveToEx (hdc, x, y, NULL);
              y = (int) (double) ((double) UbHeight + 
				  (z  + 1) * tm.tmHeight * recHeight * RowHeight);
              LineTo (hdc, x, y);
              LineTo (hdc, x, y);
		  }
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);

        hPen = CreatePen (PS_SOLID, 0, BLACKCOL);
        oldPen = SelectObject (hdc, hPen);

        for (i = LineForm.frmstart + HscrollStart; i < LineForm.fieldanz; i ++)
        {
              x = (LineForm.mask[i].pos[1] - diff) * 
                   tm.tmAveCharWidth;
			  x += Plus3D;
              y = (int) (double) ((double) UbHeight + 
				  z * tm.tmHeight * recHeight * RowHeight);
              MoveToEx (hdc, x, y, NULL);
              y = (int) (double) ((double) UbHeight + 
				  (z  + 1) * tm.tmHeight * recHeight * RowHeight);
              LineTo (hdc, x, y);
              LineTo (hdc, x, y);
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);

        hPen = CreatePen (PS_SOLID, 0, LTGRAYCOL);
        oldPen = SelectObject (hdc, hPen);

        Print3DHlineEnd (hdc, z);
        PrintVlineSatzNr (hdc);

        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
}

void ListClass::PrintNoHscrollLine (HDC hdc, int z)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        static HPEN hiPen;
        HPEN oldPen;
        int i;
        int x, y;
        int diff;

        static COLORREF lcol[] = {WHITECOL, LTGRAYCOL, BLACKCOL, 
            GRAYCOL, WHITECOL, LTGRAYCOL, BLACKCOL, GRAYCOL};


        if (LineColor > 11) return;
        if (LineColor > 7) return;
        if (LineForm.mask == NULL) return;


        diff = 0;
//        hPen = CreatePen (PS_SOLID, 0, lcol[LineColor]);
        hPen = CreatePen (PS_SOLID, 0, GRAYCOL);
        hiPen = CreatePen (PS_SOLID, 0, BLACKCOL);
        oldPen = SelectObject (hdc, hPen);

        for (i = 0; i < HscrollStart; i ++)
        {
              SelectObject (hdc, hPen);
              x = (LineForm.mask[i].pos[1] - diff) * 
                   tm.tmAveCharWidth;
              y = (int) (double) ((double) UbHeight + 
				  z * tm.tmHeight * recHeight * RowHeight);
              MoveToEx (hdc, x - 2, y, NULL);
              y = (int) (double) ((double) UbHeight + 
				  (z  + 1) * tm.tmHeight * recHeight * RowHeight);
              LineTo (hdc,   x - 2, y);
              SelectObject (hdc, hiPen);
              MoveToEx (hdc, x - 1, y, NULL);
              LineTo (hdc,   x - 1, y);
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
        DeleteObject (hiPen);
        PrintVlineSatzNr (hdc);
}


void ListClass::PrintVline (HDC hdc, int z)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        HPEN oldPen;
        int i;
        int x, y;
        int diff;

        static COLORREF lcol[] = {WHITECOL, LTGRAYCOL, BLACKCOL, 
            GRAYCOL, WHITECOL, LTGRAYCOL, BLACKCOL, GRAYCOL};


		if (print3D)
		{
                Print3DVline (hdc, z);
				return;
		}

        PrintVlineSatzNr (hdc);
        if (LineColor > 11) return;
        if (LineColor > 7) return;
        if (LineForm.mask == NULL) return;

        if (HscrollStart > 0)
        {
            PrintNoHscrollLine (hdc, z);
        }

        diff = 0;
        if (LineForm.frmstart)
        {


            diff = LineForm.mask[LineForm.frmstart + HscrollStart - 1].pos[1];

            if (HscrollStart > 0)
            {
                diff -= LineForm.mask[HscrollStart - 1].pos[1];
            }

			else if (NoRecNr == 0)
			{
                diff -= fSatzNr.mask[0].length;
            }

        }
        hPen = CreatePen (PS_SOLID, 0, lcol[LineColor]);
        oldPen = SelectObject (hdc, hPen);

        for (i = LineForm.frmstart + HscrollStart; i < LineForm.fieldanz; i ++)
        {
              x = (LineForm.mask[i].pos[1] - diff) * 
                   tm.tmAveCharWidth;
              y = (int) (double) ((double) UbHeight + 
				  z * tm.tmHeight * recHeight * RowHeight);
              MoveToEx (hdc, x, y, NULL);
              y = (int) (double) ((double) UbHeight + 
				  (z  + 1) * tm.tmHeight * recHeight * RowHeight);
              LineTo (hdc, x, y);
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
        PrintVlineSatzNr (hdc);
}


void ListClass::PrintHlinesSatzNr (HDC hdc)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        static HPEN hPenG;
        static HPEN hPenW;
        HPEN oldPen;
        int i;
        int x, y;
		int RHeight;

        if (NoRecNr) return;

		RHeight = (int) (double) ((double) tm.tmHeight * recHeight * RowHeight + 0.5);

        x = fSatzNr.mask[0].length * tm.tmAveCharWidth;

        hPen  = CreatePen (PS_SOLID, 0, BLACKCOL);
        hPenG = CreatePen (PS_SOLID, 0, GRAYCOL);
        hPenW = CreatePen (PS_SOLID, 0, WHITECOL);

        oldPen = SelectObject (hdc, hPen);
        for (i = 0; i < recanz; i ++)
        {
              SelectObject (hdc, hPenW);

              y = (int) (double) ((double) UbHeight + 
				  i * tm.tmHeight * recHeight * RowHeight);

              if (MustPaint (y))
              {
                     MoveToEx (hdc, 0, y, NULL);
                     LineTo (hdc, x - 1, y);
              }

              SelectObject (hdc, hPen);

              y = (int) (double) ((double) UbHeight + (i + 1) * 
				  tm.tmHeight * recHeight * RowHeight);

              if (MustPaint (y))
              {
                     MoveToEx (hdc, 0, y, NULL);
                     LineTo (hdc, x, y);
              }

              SelectObject (hdc, hPenG);
              y --;

              if (MustPaint (y))
              {
                     MoveToEx (hdc, 0, y, NULL);
                     LineTo (hdc, x - 1, y);
              }
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
        DeleteObject (hPenG);
        DeleteObject (hPenW);
}


void ListClass::PrintHlineSatzNr (HDC hdc, int i)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        static HPEN hPenG;
        static HPEN hPenW;
        HPEN oldPen;
        int x, y;
		int RHeight;

		RHeight = (int) (double) ((double) tm.tmHeight * recHeight * RowHeight + 0.5);

        x = fSatzNr.mask[0].length * tm.tmAveCharWidth;

        hPen  = CreatePen (PS_SOLID, 0, BLACKCOL);
        hPenG = CreatePen (PS_SOLID, 0, GRAYCOL);
        hPenW = CreatePen (PS_SOLID, 0, WHITECOL);

        oldPen = SelectObject (hdc, hPen);
        SelectObject (hdc, hPenW);

        y = (int) (double) ((double) UbHeight + 
			i * tm.tmHeight * recHeight * RowHeight);

        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x - 1, y);


        SelectObject (hdc, hPen);
        y = (int) (double) ((double) UbHeight + 
			(i + 1) * tm.tmHeight * recHeight * RowHeight);
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x, y);

        SelectObject (hdc, hPenG);
        y --;
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x - 1, y);
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
        DeleteObject (hPenG);
        DeleteObject (hPenW);
}


void ListClass::Print3DHlinesEnd (HDC hdc)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        HPEN oldPen;
        RECT rect;
        int i;
        int xstart;
        int x, y;

        static COLORREF lcol[] = {WHITECOL, LTGRAYCOL, BLACKCOL, 
            GRAYCOL,0,0,0,0, 
            WHITECOL, LTGRAYCOL, BLACKCOL, GRAYCOL};


        PrintHlinesSatzNr (hdc);
        GetClientRect (mamain3, &rect);
        xstart = 0;
        if (HscrollStart > 0)
        {
            xstart = UbForm.mask [HscrollStart].pos[1] * tm.tmAveCharWidth;
        }
        x = rect.right;


        hPen = CreatePen (PS_SOLID, 0, LTGRAYCOL);
        oldPen = SelectObject (hdc, hPen);

		if (Plus3D > 1)
		{
          for (i = 0; i <= recanz; i ++)
		  {

              y = (int) (double) ((double) UbHeight + 
				  i * tm.tmHeight * recHeight * RowHeight);
			  y -= 1;
			  if (y < UbHeight) continue;
              if (MustPaint (y))
              {
                     MoveToEx (hdc, xstart, y, NULL);
                     LineTo (hdc, x, y);
              }
		  }
		}
        for (i = 0; i <= recanz; i ++)
        {

              y = (int) (double) ((double) UbHeight + 
				  i * tm.tmHeight * recHeight * RowHeight);
              if (MustPaint (y))
              {
                     MoveToEx (hdc, xstart, y, NULL);
                     LineTo (hdc, x, y);
              }
        }
		if (Plus3D > 1)
		{
          for (i = 0; i <= recanz; i ++)
		  {

              y = (int) (double) ((double) UbHeight + 
				  i * tm.tmHeight * recHeight * RowHeight);
			  y += 1;
              if (MustPaint (y))
              {
                     MoveToEx (hdc, xstart, y, NULL);
                     LineTo (hdc, x, y);
              }
		  }
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);

        PrintHlinesSatzNr (hdc);
}


void ListClass::Print3DHlines (HDC hdc)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        HPEN oldPen;
        RECT rect;
        int i;
        int xstart;
        int x, y;

        static COLORREF lcol[] = {WHITECOL, LTGRAYCOL, BLACKCOL, 
            GRAYCOL,0,0,0,0, 
            WHITECOL, LTGRAYCOL, BLACKCOL, GRAYCOL};


        PrintHlinesSatzNr (hdc);
        GetClientRect (mamain3, &rect);
        xstart = 0;
        if (HscrollStart > 0)
        {
            xstart = UbForm.mask [HscrollStart].pos[1] * tm.tmAveCharWidth;
        }
        x = rect.right;

        hPen = CreatePen (PS_SOLID, 0, WHITECOL);
        oldPen = SelectObject (hdc, hPen);
        for (i = 0; i <= recanz; i ++)
        {
              y = (int) (double) ((double) UbHeight + 
				  i * tm.tmHeight * recHeight * RowHeight);
			  y -= Plus3D;
			  if (y < UbHeight) continue;
              if (MustPaint (y))
              {
                     MoveToEx (hdc, xstart, y, NULL);
                     LineTo (hdc, x, y);
              }
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);

        hPen = CreatePen (PS_SOLID, 0, LTGRAYCOL);
        oldPen = SelectObject (hdc, hPen);

		if (Plus3D > 1)
		{
          for (i = 0; i <= recanz; i ++)
		  {

              y = (int) (double) ((double) UbHeight + 
				  i * tm.tmHeight * recHeight * RowHeight);
			  y -= 1;
			  if (y < UbHeight) continue;
              if (MustPaint (y))
              {
                     MoveToEx (hdc, xstart, y, NULL);
                     LineTo (hdc, x, y);
              }
		  }
		}
        for (i = 0; i <= recanz; i ++)
        {

              y = (int) (double) ((double) UbHeight + 
				  i * tm.tmHeight * recHeight * RowHeight);
              if (MustPaint (y))
              {
                     MoveToEx (hdc, xstart, y, NULL);
                     LineTo (hdc, x, y);
              }
        }
		if (Plus3D > 1)
		{
          for (i = 0; i <= recanz; i ++)
		  {

              y = (int) (double) ((double) UbHeight + 
				  i * tm.tmHeight * recHeight * RowHeight);
			  y += 1;
              if (MustPaint (y))
              {
                     MoveToEx (hdc, xstart, y, NULL);
                     LineTo (hdc, x, y);
              }
		  }
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);

        hPen = CreatePen (PS_SOLID, 0, BLACKCOL);
        oldPen = SelectObject (hdc, hPen);
        for (i = 0; i <= recanz; i ++)
        {

              y = (int) (double) ((double) UbHeight + 
				  i * tm.tmHeight * recHeight * RowHeight);
			  y += Plus3D;
              if (MustPaint (y))
              {
                     MoveToEx (hdc, xstart, y, NULL);
                     LineTo (hdc, x, y);
              }
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);

        PrintHlinesSatzNr (hdc);
}

void ListClass::PrintLastPHline (HDC hdc)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        HPEN oldPen;
        RECT rect;
        int x, y;

        static COLORREF lcol[] = {WHITECOL, LTGRAYCOL, BLACKCOL, 
            GRAYCOL,0,0,0,0, 
            WHITECOL, LTGRAYCOL, BLACKCOL, GRAYCOL};

        rect.top   = 0;
        rect.left  = 0;
        rect.bottom = GetDeviceCaps (hdc, VERTRES);
        rect.right  = GetDeviceCaps (hdc, HORZRES);

        x = rect.right;

        hPen = CreatePen (PS_SOLID, 0, lcol [LineColor]);
        oldPen = SelectObject (hdc, hPen);
        y = UbHeight + (recanz - scrollpos) * RowHeightP;
        if (y < rect.bottom)
        {
                     MoveToEx (hdc, 0, y, NULL);
                     LineTo (hdc, x, y);
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
}


void ListClass::PrintHlines (HDC hdc)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        HPEN oldPen;
        RECT rect;
        int i;
        int xstart;
        int x, y;

        static COLORREF lcol[] = {WHITECOL, LTGRAYCOL, BLACKCOL, 
            GRAYCOL,0,0,0,0, 
            WHITECOL, LTGRAYCOL, BLACKCOL, GRAYCOL};


		if (print3D)
		{
		         Print3DHlines (hdc);
		         return;
		}

        PrintHlinesSatzNr (hdc);
        if (LineColor > 11) return;
        if (LineColor > 3 && LineColor < 8) return;
        GetClientRect (mamain3, &rect);
        xstart = 0;
        if (HscrollStart > 0)
        {
            xstart = UbForm.mask [HscrollStart].pos[1] * tm.tmAveCharWidth;
        }
        x = rect.right;

        hPen = CreatePen (PS_SOLID, 0, lcol [LineColor]);
        oldPen = SelectObject (hdc, hPen);
        for (i = 0; i <= recanz; i ++)
        {

              y = (int) (double) ((double) UbHeight + 
				  i * tm.tmHeight * recHeight * RowHeight);
              if (MustPaint (y))
              {
                     MoveToEx (hdc, xstart, y, NULL);
                     LineTo (hdc, x, y);
              }
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
        PrintHlinesSatzNr (hdc);
}

void ListClass::PrintPHlines (HDC hdc, RECT *rect)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        HPEN oldPen;
        int i;
        int x, y;

        static COLORREF lcol[] = {WHITECOL, LTGRAYCOL, BLACKCOL, 
            GRAYCOL,0,0,0,0, 
            WHITECOL, LTGRAYCOL, BLACKCOL, GRAYCOL};


//        PrintHlinesSatzNr (hdc);
        if (LineColor > 11) return;

        if (LineColor > 3 && LineColor < 8) 
		{
			PrintLastPHline (hdc);
			return;
		}


        x = rect->right;

        hPen = CreatePen (PS_SOLID, 0, lcol [LineColor]);
        oldPen = SelectObject (hdc, hPen);
        for (i = 0; i <= pagelen0 + 1; i ++)
        {
 
              if (i + printscrollpos > recanz) break;
              y = (int) (double) ((double) UbHeight + 
   		             i * tm.tmHeight * recHeight * RowHeight);
              y += ublen * tm.tmHeight;
              MoveToEx (hdc, 0, y, NULL);
              LineTo (hdc, x, y);
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
//        PrintHlinesSatzNr (hdc);
}

void ListClass::Print3DHline (HDC hdc, int i)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
		RECT rect;
        HPEN oldPen;
        int x, y;
		int p = 0;

        static COLORREF lcol[] = {WHITECOL, LTGRAYCOL, BLACKCOL, 
            GRAYCOL,0,0,0,0, 
            WHITECOL, LTGRAYCOL, BLACKCOL, GRAYCOL};


        PrintHlinesSatzNr (hdc);
        GetClientRect (mamain3, &rect);
        x = rect.right;

        hPen = CreatePen (PS_SOLID, 0, WHITECOL);
        oldPen = SelectObject (hdc, hPen);

        y = (int) (double) ((double) UbHeight + 
				  i * tm.tmHeight * recHeight * RowHeight);
  	    y -= Plus3D;
	    if (y > UbHeight + p)
		{
               MoveToEx (hdc, 0, y, NULL);
               LineTo (hdc, x, y);
		}
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);

        hPen = CreatePen (PS_SOLID, 0, LTGRAYCOL);
        oldPen = SelectObject (hdc, hPen);

		if (Plus3D > 1)
		{

              y = (int) (double) ((double) UbHeight + 
				  i * tm.tmHeight * recHeight * RowHeight);
			  y -= 1;
			  if (y > UbHeight + p)
			  {
                    MoveToEx (hdc, 0, y, NULL);
                    LineTo (hdc, x, y);
			  }
		}

        y = (int) (double) ((double) UbHeight + 
				  i * tm.tmHeight * recHeight * RowHeight);
 	    if (y > UbHeight + 2)
		{
			  if (y > UbHeight + p)
			  {
                    MoveToEx (hdc, 0, y, NULL);
                    LineTo (hdc, x, y);
			  }
		}
		if (Plus3D > 1)
		{

              y = (int) (double) ((double) UbHeight + 
				  i * tm.tmHeight * recHeight * RowHeight);
			  y += 1;
 	          if (y > UbHeight + p)
			  {
                     MoveToEx (hdc, 0, y, NULL);
                     LineTo (hdc, x, y);
			  }
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);

        hPen = CreatePen (PS_SOLID, 0, BLACKCOL);
        oldPen = SelectObject (hdc, hPen);

        y = (int) (double) ((double) UbHeight + 
				  i * tm.tmHeight * recHeight * RowHeight);
		y += Plus3D;
        if (y > UbHeight + p)
		{
               MoveToEx (hdc, 0, y, NULL);
               LineTo (hdc, x, y);
		}
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);

        PrintHlinesSatzNr (hdc);
}

void ListClass::Print3DHlineEnd (HDC hdc, int i)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        HPEN oldPen;
        RECT rect;
        int x, y;

        static COLORREF lcol[] = {WHITECOL, LTGRAYCOL, BLACKCOL, 
            GRAYCOL,0,0,0,0, 
            WHITECOL, LTGRAYCOL, BLACKCOL, GRAYCOL};

        PrintHlinesSatzNr (hdc);
        GetClientRect (mamain3, &rect);
        x = rect.right;


        hPen = CreatePen (PS_SOLID, 0, LTGRAYCOL);
        oldPen = SelectObject (hdc, hPen);

		if (Plus3D > 1)
		{

              y = (int) (double) ((double) UbHeight + 
				  i * tm.tmHeight * recHeight * RowHeight);
			  y -= 1;
              MoveToEx (hdc, 0, y, NULL);
              LineTo (hdc, x, y);
		}

        y = (int) (double) ((double) UbHeight + 
				  i * tm.tmHeight * recHeight * RowHeight);
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x, y);
 		if (Plus3D > 1)
		{
 
              y = (int) (double) ((double) UbHeight + 
				  i * tm.tmHeight * recHeight * RowHeight);
			  y += 1;
              MoveToEx (hdc, 0, y, NULL);
              LineTo (hdc, x, y);
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);

        PrintHlinesSatzNr (hdc);
}



void ListClass::PrintHline (HDC hdc, int i)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        HPEN oldPen;
        RECT rect;
        int x, y;

        static COLORREF lcol[] = {WHITECOL, LTGRAYCOL, BLACKCOL, 
            GRAYCOL,0,0,0,0, 
            WHITECOL, LTGRAYCOL, BLACKCOL, GRAYCOL};


		if (print3D)
		{
		         Print3DHline (hdc, i);
		         return;
		}
        if (LineColor > 11) return;
        if (LineColor > 3 && LineColor < 8) return;
        GetClientRect (mamain3, &rect);
        x = rect.right;

        hPen = CreatePen (PS_SOLID, 0, lcol [LineColor]);
        oldPen = SelectObject (hdc, hPen);

        y = (int) (double) ((double) UbHeight + 
			i * tm.tmHeight * recHeight * RowHeight);
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x, y);
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
}

void ListClass::ToCubFormat (char *dest, field *feld, int tpos)
/**
Feld formatiert in dest uebertragen.
**/
{
        char *text;

        if (feld->attribut & REMOVED)
        {
                    memset (dest, ' ', feld->length);
                    dest[feld->length - 1] = (char) 0;
                    return;
        }

        if ((feld->attribut & COLBUTTON) == 0)
        {
                    return;
        }

        ColButton *Cub = (ColButton *) feld->item->GetFeldPtr ();
        if (tpos == 0)
        {
            text = Cub->text1;
        }
        else if (tpos == 1)
        {
            text = Cub->text2;
        }
        else if (tpos == 2)
        {
            text = Cub->text3;
        }


        if (text == NULL) text = " ";

        if (feld->length == 0)
        {
                      strcpy (dest, text);
        }
        else
        {
                      memset (dest, ' ', feld->length);
                      memcpy (dest, text,strlen (text));
                      dest[feld->length] = 0;
        }
        return;
}


void ListClass::FillCubPrintUb (char *dest, int tpos)
/**
String fuellen.
**/
{
        int i;
        char buffer [1024];
		form *frm;

        memset (dest, 0, 0x1000);
		frm = &UbForm;

        strcat (dest, " ");
        for (i = frm->frmstart; i < frm->fieldanz; i ++)
        {
                 strcpy (buffer , "");
                 ToCubFormat (buffer, &frm->mask[i], tpos);
                 strcat (dest, buffer);           
        }
}

void ListClass::FillPrintUb (char *dest)
/**
String fuellen.
**/
{
        int i;
        char buffer [1024];
		form *frm;

        memset (dest, 0, 0x1000);
		frm = &UbForm;

        strcat (dest, " ");
        for (i = frm->frmstart; i < frm->fieldanz; i ++)
        {
               ToFormat (buffer, &frm->mask[i]);
               strcat (dest, buffer);           
        }
}

void ListClass::FillPrintRow (char *dest, int h)
/**
String fuellen.
**/
{
        int i;
        char buffer [1024];
		form *frm;

        memset (dest, 0, 0x1000);
		frm = &DataForm;

        strcat (dest, " ");
        for (i = frm->frmstart; i < frm->fieldanz; i ++)
        {
                   if (frm->mask[i].pos[0] == h)
                   {
                            ToFormatEx (buffer, &frm->mask[i], &UbForm.mask[i]);
                            strcat (dest, buffer);           
                   }
        }
}


void ListClass::InitListButton (field *Field)
{ 
      ListButton *listButton;
      char *p;

      listButton = ((ListButton *) Field->item->GetFeldPtr ());
      if (listButton == NULL) return;
      listButton->SetFeld (p);
      if (ArrDown != NULL)
      {
            listButton->SetBitMap (ArrDown);
      }
}


HWND ListClass::CreateListButton (int x, int y, int cx, int cy, 
                                  HFONT hFont, char *feld, DWORD bustyle)
{
      ListButton *listButton;
      HBRUSH hBrush;
      HBRUSH oldBrush;
      HPEN hPen, oldPen;
      char *text;
      SIZE sizes;
      int xt,yt;
      HBITMAP hbr;
      HBITMAP  hbmOld;
      HDC    hdcMemory;
      BITMAP bm;
      

      listButton = (ListButton *) feld;
      if (listButton == NULL) return NULL;
      hBrush = CreateSolidBrush (LTGRAYCOL);
      oldBrush = SelectObject (pHdc, hBrush);
      SelectObject (pHdc, GetStockObject (NULL_PEN));
      Rectangle (pHdc, x, y, x + cx, y + cy);
      SelectObject (pHdc, oldBrush);
      DeleteObject (hBrush);

      hPen = CreatePen (PS_SOLID, 0, WHITECOL);
      oldPen = SelectObject (pHdc, hPen);

      MoveToEx (pHdc, x,          y + 1, NULL);
      LineTo   (pHdc, x + cx,     y + 1);

      MoveToEx (pHdc, x,  y + 1, NULL);
      LineTo   (pHdc, x,  y + cy);
      SelectObject (pHdc, oldPen);
      DeleteObject (hPen);

      hPen = CreatePen (PS_SOLID, 0, BLACKCOL);
      oldPen = SelectObject (pHdc, hPen);

      MoveToEx (pHdc, x,          y + cy - 1, NULL);
      LineTo   (pHdc, x + cx,     y + cy - 1);

      MoveToEx (pHdc, x + cx - 1, y + 1, NULL);
      LineTo   (pHdc, x + cx - 1, y + cy - 1);
      SelectObject (pHdc, oldPen);
      DeleteObject (hPen);

      hPen = CreatePen (PS_SOLID, 0, GRAYCOL);
      oldPen = SelectObject (pHdc, oldPen);

      MoveToEx (pHdc, x + 1,      y + cy - 2, NULL);
      LineTo   (pHdc, x + cx - 1, y + cy - 2);

      MoveToEx (pHdc, x + cx - 2, y + 2, NULL);
      LineTo   (pHdc, x + cx - 2, y + cy - 2);
      SelectObject (pHdc, oldPen);
      DeleteObject (hPen);

      hbr = listButton->GetBitMap ();
      text = listButton->GetFeld ();
      if (hbr != NULL)
      {
             GetObject (hbr, sizeof (BITMAP), &bm);
             if (bm.bmWidth > cx - 4) bm.bmWidth = cx - 4;
             if (bm.bmHeight > cy - 4) bm.bmHeight = cy - 4;
             xt = max (2, (cx - bm.bmWidth) / 2);
             yt = max (2, (cy - bm.bmHeight) / 2);
             xt += x;
             yt += y;
             hdcMemory  = CreateCompatibleDC(pHdc);
             hbmOld = SelectObject (hdcMemory, hbr);
             BitBlt (pHdc, xt, yt, bm.bmWidth, bm.bmHeight,
                         hdcMemory,0, 0, SRCCOPY);
             SelectObject (hdcMemory, hbmOld);
             DeleteDC (hdcMemory);        
      }
      else if (text != NULL)
      {
	         GetTextExtentPoint32 (pHdc, text, strlen (text), &sizes);
             yt = max (1, (cy - sizes.cy) /2);
             xt = max (1, (cx - sizes.cx) / 2);
             xt += x;
             yt += y;
             SetTextColor (pHdc,BLACKCOL);
             ::SetBkColor (pHdc,LTGRAYCOL);
             TextOut (pHdc, xt, yt, text, strlen (text));
      }
      return 0;
}

void ListClass::CheckButton (ITEM *Item)
{
      ListButton *listButton;
      char *feld;

      feld = Item->GetFeldPtr ();
      listButton = (ListButton *) feld;
      if (listButton == NULL) return;

      if (listButton->IsChecked ())
      {
          listButton->Check (FALSE);
      }
      else
      {
          listButton->Check (TRUE);
      }

/*
      CreateCheckButton (listButton->GetX (),
                         listButton->GetY (), 
                         listButton->GetCX (), 
                         listButton->GetCY (),
                         ListhFont,
                         feld, NULL);
*/
      memcpy ( SwSaetze [AktRow], ausgabesatz, zlen); 
      ShowAktRow ();
}


HWND ListClass::CreateCheckButton (int x, int y, int cx, int cy, 
                                  HFONT hFont, char *feld, DWORD bustyle)
{
      ListButton *listButton;
      HBRUSH hBrush;
      HBRUSH oldBrush;
      HPEN hPen, oldPen;
      char *text;
      SIZE sizes;
      int xt,yt;
      HBITMAP hbr;
      HBITMAP  hbmOld;
      HDC    hdcMemory;
      BITMAP bm;
      

      listButton = (ListButton *) feld;
      if (listButton == NULL) return NULL;
      listButton->SetRect (x, y, cx, cy);
      hBrush = CreateSolidBrush (WHITECOL);
      oldBrush = SelectObject (pHdc, hBrush);
      SelectObject (pHdc, GetStockObject (NULL_PEN));
      Rectangle (pHdc, x, y, x + cx, y + cy);
      DeleteObject (SelectObject (pHdc, oldBrush));

      hPen = CreatePen (PS_SOLID, 0, BLACKCOL);
      oldPen = SelectObject (pHdc, hPen);

      MoveToEx (pHdc, x,          y + 1, NULL);
      LineTo   (pHdc, x + cx,     y + 1);

      MoveToEx (pHdc, x,  y + 1, NULL);
      LineTo   (pHdc, x,  y + cy);

      MoveToEx (pHdc, x,          y + cy, NULL);
      LineTo   (pHdc, x + cx,     y + cy);

      MoveToEx (pHdc, x + cx,     y + 1, NULL);
      LineTo   (pHdc, x + cx,     y + cy + 1);
      DeleteObject (SelectObject (pHdc, oldPen));


      if (listButton->IsChecked () == FALSE)
      {
          return NULL;
      }


      hbr = listButton->GetBitMap ();
      text = listButton->GetFeld ();
      if (hbr != NULL && listButton->IsChecked ())
      {
             GetObject (hbr, sizeof (BITMAP), &bm);
             if (bm.bmWidth > cx - 4) bm.bmWidth = cx - 4;
             if (bm.bmHeight > cy - 4) bm.bmHeight = cy - 4;
             xt = max (2, (cx - bm.bmWidth) / 2);
             yt = max (2, (cy - bm.bmHeight) / 2);
             xt += x;
             yt += y;
             hdcMemory  = CreateCompatibleDC(pHdc);
             hbmOld = SelectObject (hdcMemory, hbr);
             BitBlt (pHdc, xt, yt, bm.bmWidth, bm.bmHeight,
                         hdcMemory,0, 0, SRCCOPY);
             SelectObject (hdcMemory, hbmOld);
             DeleteDC (hdcMemory);
      }
      else if (text != NULL && text[0] == 'J')
      {
	         GetTextExtentPoint32 (pHdc, text, strlen (text), &sizes);
             yt = max (1, (cy - sizes.cy) /2);
             xt = max (1, (cx - sizes.cx) / 2);
             xt += x;
             yt += y;
             SetTextColor (pHdc,BLACKCOL);
             ::SetBkColor (pHdc,WHITECOL);
             TextOut (pHdc, xt, yt, text, strlen (text));
      }
      return NULL;
}


int ListClass::GetUbFrmlen (void)
{
        int i;
        int len;

        len = 0;
        for (i = 0; i < UbForm.fieldanz; i ++)
        {
            len += UbForm.mask[i].length;
        }
        return len;
}

int ListClass::GetUbFrmlen (int pos)
{
        int i;
        int len;

        len = 0;
        for (i = 0; i < pos; i ++)
        {
            len += UbForm.mask[i].length;
        }
        return len;
}


#ifndef _PFONT

void ListClass::FillFrmRow (char *dest, form *frm, int h)
{
        int i;
        char buffer [1024];
        int ldiff;
        int pos;
        unsigned int frmlen;

        memset (dest, ' ', 0x1000);

        ldiff = 0;

        ldiff = LineForm.mask[LineForm.frmstart - 1].pos[1];


		if (NoRecNr == 0)
		{
			if (LineForm.frmstart)
			{
               ldiff = LineForm.mask[LineForm.frmstart - 1].pos[1];
				                       + fSatzNr.mask[0].length;
			}
			else
			{
				ldiff = fSatzNr.mask[0].length;
			}
		}

        frmlen = 0;
        for (i = frm->frmstart; i < frm->fieldanz; i ++)
        {
            if (frm->mask[i].pos[0] == h)
            {
                   ToFormat (buffer, &frm->mask[i]);

                   pos = frm->mask[i].pos[1];
                   pos = pos - ldiff;
                   if (pos < 0) continue;
                   if (frmlen < pos + strlen (buffer))
                   {
                       frmlen = pos + strlen (buffer);
                   }
                   memcpy (&dest[pos], buffer,
                           strlen (buffer));
            }
        }
        dest[frmlen] = (char) 0;
}

#else


void ListClass::FillNoScrollBackground (HDC hdc, int zeile)
{
          RECT rect; 
          HBRUSH hBrush;
	    

          if (HscrollStart == 0) return;
          if (NoScrollBackground) return;

          rect.top = (int) (double) ((double) UbHeight + zeile * tm.tmHeight * 
			  recHeight * RowHeight); 
          if (PrintHdc)
          {
               rect.top += ublen * tm.tmHeight;
          }
		  rect.bottom = (int) (double) ((double) 
			  rect.top + tm.tmHeight * recHeight * RowHeight + 1); // + 1 zum Test 12.09.2000
          if (NoRecNr == TRUE)
          {
		      rect.left = 0;
          }
          else
          {
		      rect.left = fSatzNr.mask[0].length * tm.tmAveCharWidth;
          }

          rect.right = rect.left + (UbForm.mask[HscrollStart - 1].length + 
                                    UbForm.mask[HscrollStart - 1].pos[1]) * tm.tmAveCharWidth;

          hBrush = CreateSolidBrush (NoScrollBkColor);

          HBRUSH oldBrush = SelectObject (hdc, hBrush);
          SelectObject (hdc, GetStockObject (NULL_PEN));
          Rectangle (hdc,rect.left, rect.top, rect.right, rect.bottom);
          DeleteObject (SelectObject (hdc, oldBrush)); 
}

void ListClass::FillPrintUbBackground (HDC hdc, int zeile)
{
          RECT rect; 
          HBRUSH hBrush;
	    

          rect.top = (int) (double) ((double) (zeile + ublen) * tm.tmHeight); 
		  rect.bottom = (int) (double) ((double) 
			  rect.top + tm.tmHeight * UbForm.mask[0].rows); 
	      rect.left = 0;
          rect.right = GetDeviceCaps (hdc, HORZRES);

          hBrush = CreateSolidBrush (NoScrollBkColor);

          HBRUSH oldBrush = SelectObject (hdc, hBrush);
          HPEN oldPen = SelectObject (hdc, GetStockObject (NULL_PEN));
          Rectangle (hdc,rect.left, rect.top, rect.right, rect.bottom);
          SelectObject (hdc, oldPen);
          SelectObject (hdc, oldBrush);
          DeleteObject (hBrush); 
}

void ListClass::ShowNoScrollArray (HDC hdc, char *dest, form *frm, int zeile, int h)
{
        int i;
        char buffer [1024];
        int ldiff;
        int szdiff;
        int pos;
        unsigned int frmlen;
		int x, y;
        int y0;
        int cx, cy;
		SIZE size;
		SIZE sizes;

        if (HscrollStart == 0) return;

		GetTextExtentPoint32 (hdc, "X", 1, &size);
        tm.tmAveCharWidth = size.cx;
         y0 = (int) (double) ((double) UbHeight + zeile * tm.tmHeight * 
			  recHeight * RowHeight 
			 + tm.tmHeight * h * RowHeight);
         if (PrintHdc)
         {
             y0 += ublen * tm.tmHeight;
         }
		 y = GetMidPos (y0);
        ldiff = 0;

/*        if (LineForm.frmstart > 0)
        {
              ldiff = LineForm.mask[LineForm.frmstart - 1].pos[1];
 		      if (NoRecNr == 0) ldiff -= fSatzNr.mask[0].length;
        }
*/
/*
		if (NoRecNr == 0)
		{
                SetTextColor (hdc,BLACKCOL);
                ::SetBkColor (hdc,LTGRAYCOL);
                TextOut (hdc, 0, y, SatzNr, strlen (SatzNr));
		}
*/

        if (h == 0)
        {
             FillNoScrollBackground (hdc, zeile); 
        }
        frmlen = 0;
        szdiff = 0;
        for (i = 0; i < frm->fieldanz; i ++)
        {
            if (frm->mask[i].pos[1] > frm->mask[HscrollStart - 1].pos[1])
            {
                    continue;
            }

            if (frm->mask[i].pos[0] == h)
            {
                   if ((frm->mask[i].attribut & BUTTON) == 0)
                   {
                        ToFormat (buffer, &frm->mask[i]);
                   }
                   pos = frm->mask[i].pos[1];

                   if (strchr (frm->mask[i].picture, 'f') ||
                       (strchr (frm->mask[i].picture, 'd') &&
                        strchr (frm->mask[i].picture, 'y') == NULL))
                   {
                        clipped (buffer);
                   		GetTextExtentPoint32 (hdc, buffer, strlen (buffer), &sizes);
                        pos = (pos + frm->mask[i].length - 1) * size.cx;
                        pos -= sizes.cx; 
                        pos -= ldiff * size.cx;
                        if (pos < 0) continue;
				        x = pos;
                   } 
                   else

                   {
                        pos = pos - ldiff;
                        if (pos < 0) continue;
				        x = pos * size.cx;
                   }

                   pos -= (szdiff - size.cx);
                   SetTextColor (hdc,Color);
//                   ::SetBkColor (hdc,NoScrollBkColor);
                   ::SetBkColor (hdc,BkColor);
                   if ((frm->mask[i].attribut & BUTTON) != 0 &&
                       (frm->mask[i].attribut & REMOVED) == 0)
                   {
                       if (InInsAttr (frm->mask[i].item->GetItemName (), zeile + scrollpos))
                       {
                           continue;
                       }
                       cx = frm->mask[i].length * size.cx;
                       cy = frm->mask[i].rows * tm.tmHeight;
                       pHdc = hdc;
                       if (cy == 0) 
                       {
                           cy = (int) (double) ((double) RowHeight * tm.tmHeight);
                           frm->mask[i].feldid = 
                                CreateListButton (x, y0 + 1, cx, cy - 2, 
                                     ListhFont, frm->mask[i].item->GetFeldPtr (), NULL);
                       }
                       else
                       {
                            frm->mask[i].feldid = 
                                CreateListButton (x, y, cx, cy, 
                                      ListhFont, frm->mask[i].item->GetFeldPtr (), NULL);
                       }
                       continue;
                   }
                   else if ((frm->mask[i].attribut & CHECKBUTTON) != 0 &&
                       (frm->mask[i].attribut & REMOVED) == 0)
                   {
                       cx = frm->mask[i].length * size.cx;
                       cy = frm->mask[i].rows * tm.tmHeight;
                       pHdc = hdc;
                       if (cy == 0) 
                       {
                           cy = (int) (double) ((double) RowHeight * tm.tmHeight);
                           frm->mask[i].feldid = 
                                CreateCheckButton (x, y0 + 1, cx, cy - 2, 
                                     ListhFont, frm->mask[i].item->GetFeldPtr (), NULL);
                       }
                       else
                       {
                            frm->mask[i].feldid = 
                                CreateCheckButton (x, y, cx, cy, 
                                      ListhFont, frm->mask[i].item->GetFeldPtr (), NULL);
                       }
                       memcpy ( SwSaetze [i], ausgabesatz, zlen); 
                       continue;
                   }
                   
				   memset (buffer, ' ', 80);
				   buffer [79] = 0;
                   clipped (buffer);
                   TextOut (hdc, x, y, buffer, strlen (buffer));

                   if (NoScrollBackground == FALSE)
                   {
                      ::SetBkColor (hdc,NoScrollBkColor);
                   }
                   clipped (buffer);
                   ToFormat (buffer, &frm->mask[i]);
                   clipped (buffer);
                   if (frm->mask[i].length > 0 &&
                       frm->mask[i].length < (int) strlen (buffer))
                   {
                       buffer [frm->mask[i].length] = 0;
                   }

                   TextOut (hdc, x, y, buffer, strlen (buffer));
            }
        }
}

void ListClass::TestTextLen (HDC hdc, char *buffer, int len)
{
		SIZE size;
		SIZE sizes;
        int Xlen;

   		GetTextExtentPoint32 (hdc, "X", 1, &size);
        Xlen = size.cx * len;

        len = strlen (buffer);
        for (int i = len; i >= 0; i --)
        {
              buffer[i] = 0;
   		      GetTextExtentPoint32 (hdc, buffer, strlen (buffer), &sizes);
              if (sizes.cx < Xlen)
              {
                  if (i > 0)
                  {
//                      buffer[i - 1] = 0;
                      buffer[i] = 0;
                  }
                  break;
              }
        }
}


void ListClass::FillFrmRow (HDC hdc, char *dest, form *frm, int zeile, int h)
{
        int i;
        char buffer [1024];
        int ldiff;
        int szdiff;
        int pos;
        unsigned int frmlen;
		int x, y;
        int y0;
        int cx, cy;
		SIZE size;
		SIZE sizes;

		GetTextExtentPoint32 (hdc, "X", 1, &size);
        tm.tmAveCharWidth = size.cx;
         y0 = (int) (double) ((double) UbHeight + zeile * tm.tmHeight * 
			  recHeight * RowHeight 
			 + tm.tmHeight * h * RowHeight);
         if (PrintHdc)
         {
             y0 += ublen * tm.tmHeight;
         }
		 y = GetMidPos (y0);
        ldiff = 0;

		if (LineForm.frmstart)
        {
            if (NoRecNr == FALSE)
            {
                 ldiff = LineForm.mask[LineForm.frmstart - 1 + HscrollStart].pos[1] -
                   fSatzNr.mask[0].length;
            }
            else
            {
                 ldiff = LineForm.mask[LineForm.frmstart - 1 + HscrollStart].pos[1];
            }

            if (HscrollStart > 0)
            {
                ldiff -= LineForm.mask[HscrollStart - 1].pos[1];
            }
        }
		if (NoRecNr == 0)
		{
                SetTextColor (hdc,BLACKCOL);
                ::SetBkColor (hdc,LTGRAYCOL);
                TextOut (hdc, 0, y, SatzNr, strlen (SatzNr));
		}

        frmlen = 0;
        szdiff = 0;
        for (i = frm->frmstart + HscrollStart; i < frm->fieldanz; i ++)
        {
            int ubspalte = GetUbSpalte (i);
            if (HscrollStart > 0)
            {
                    if ((ubspalte - UbForm.frmstart) < HscrollStart)
//                    if (frm->mask[i].pos[1] <= frm->mask[HscrollStart - 1].pos[1])
                    {
                        continue;
                    }
            }

            if (frm->mask[i].pos[0] == h)
            {
                   if ((frm->mask[i].attribut & BUTTON) == 0 &&
                       (frm->mask[i].attribut & CHECKBUTTON) == 0)
                   {
                        ToFormat (buffer, &frm->mask[i]);
                   }
                   pos = frm->mask[i].pos[1];

                   if (strchr (frm->mask[i].picture, 'f') ||
                       (strchr (frm->mask[i].picture, 'd') &&
                        strchr (frm->mask[i].picture, 'y') == NULL))
                   {
                        clipped (buffer);
                   		GetTextExtentPoint32 (hdc, buffer, strlen (buffer), &sizes);
                        pos = (pos + frm->mask[i].length - 1) * size.cx;
                        pos -= sizes.cx; 
                        pos -= ldiff * size.cx;
                        if (pos < 0) continue;
				        x = pos;
                   } 
                   else

                   {
                        pos = pos - ldiff;
                        if (pos < 0) continue;
				        x = pos * size.cx;
                   }

                   pos -= (szdiff - size.cx);
                   SetTextColor (hdc,Color);
                   ::SetBkColor (hdc,BkColor);
                   if ((frm->mask[i].attribut & BUTTON) != 0 &&
                       (frm->mask[i].attribut & REMOVED) == 0)
                   {
                       if (InInsAttr (frm->mask[i].item->GetItemName (), zeile + scrollpos))
                       {
                           continue;
                       }
                       cx = frm->mask[i].length * size.cx;
                       cy = frm->mask[i].rows * tm.tmHeight;
                       pHdc = hdc;
                       if (cy == 0) 
                       {
                           cy = (int) (double) ((double) RowHeight * tm.tmHeight);
                           frm->mask[i].feldid = 
                                CreateListButton (x, y0 + 1, cx, cy - 2, 
                                     ListhFont, frm->mask[i].item->GetFeldPtr (), NULL);
                       }
                       else
                       {
                            frm->mask[i].feldid = 
                                CreateListButton (x, y, cx, cy, 
                                      ListhFont, frm->mask[i].item->GetFeldPtr (), NULL);
                       }
                       continue;
                   }
                   
                   else if ((frm->mask[i].attribut & CHECKBUTTON) != 0 &&
                       (frm->mask[i].attribut & REMOVED) == 0)
                   {
                       cx = frm->mask[i].length * size.cx;
                       cy = frm->mask[i].rows * tm.tmHeight;
                       pHdc = hdc;
                       if (cy == 0) 
                       {
                           cy = (int) (double) ((double) RowHeight * tm.tmHeight);
                           frm->mask[i].feldid = 
                                CreateCheckButton (x, y0 + 1, cx, cy - 2, 
                                     ListhFont, frm->mask[i].item->GetFeldPtr (), NULL);
                       }
                       else
                       {
                            frm->mask[i].feldid = 
                                CreateCheckButton (x, y, cx, cy, 
                                      ListhFont, frm->mask[i].item->GetFeldPtr (), NULL);
                       }
                       memcpy ( SwSaetze [zeile], ausgabesatz, zlen); 
                       continue;
                   }
				   memset (buffer, ' ', 80);
				   buffer [79] = 0;
                   TextOut (hdc, x, y, buffer, strlen (buffer));

                   ToFormat (buffer, &frm->mask[i]);
                   if (frm->mask[i].length > 0)
                   {
                       TestTextLen (hdc, buffer, (int) frm->mask[i].length);
                   }
                   else
                   {
                       TestTextLen (hdc, buffer, (int) strlen (buffer));
                   }
                   TextOut (hdc, x, y, buffer, strlen (buffer));
            }
        }
}

void ListClass::FillUbPFrmRow (HDC hdc, char *dest, form *frm, int zeile)
{
        int i;
        char buffer [1024];
        int ldiff;
        int szdiff;
        int pos;
        unsigned int frmlen;
		int x, y;
		int xplus;
		SIZE size;
		SIZE sizes;

		 GetTextExtentPoint32 (hdc, "X", 1, &size);
//		 y = GetMidPos (y);
         if (NoRecNr == 0)
         {
                  xplus = fSatzNr.mask[0].length * size.cx;
         }
         else
         {
                  xplus = 0;
         }
        memset (dest, ' ', 0x1000);

        ldiff = 0;
        if (NoRecNr == 0)
        {
                   SetTextColor (hdc,BLACKCOL);
                   ::SetBkColor (hdc,LTGRAYCOL);
                   TextOut (hdc, 0, y, " S-Nr        ", strlen (" S-Nr        "));
        }
        if (NoRecNr == 0)
        {
            ldiff = fSatzNr.mask[0].length;
        }
		else
        {
            ldiff = 0;
        }
        frmlen = 0;
        szdiff = 0;
        FillPrintUbBackground (hdc, 0);
        for (i = frm->frmstart; i < frm->fieldanz; i ++)
        {
            y = zeile * RowHeightP; 
            y += ublen * tm.tmHeight;
            for (int h = 0; h < UbForm.mask[0].rows - 1; h ++)
            {
                   if (frm->mask[i].attribut & COLBUTTON)
                   {
                         ToCubFormat (buffer, &frm->mask[i], h);
                   }
                   else
                   {
                         ToFormat (buffer, &frm->mask[i]);
                   }
                   pos = frm->mask[i].pos[1];
                   if (strchr (frm->mask[i].picture, 'f') ||
                       (strchr (frm->mask[i].picture, 'd') &&
                        strchr (frm->mask[i].picture, 'y') == NULL))
                   {
                        clipped (buffer);
                   		GetTextExtentPoint32 (hdc, buffer, strlen (buffer), &sizes);
                        pos = (pos + frm->mask[i].length - 2) * size.cx;
                        pos -= sizes.cx; 
                        pos -= ldiff * size.cx;
                        if (pos < 0) continue;
				        x = xplus + pos;
                   } 
                   else
                   {
                        pos = pos - ldiff;
                        if (pos < 0) continue;
				        x = xplus + pos * size.cx;
                   }
                   pos -= (szdiff - size.cx);
                   SetTextColor (hdc,Color);
                   ::SetBkColor (hdc,NoScrollBkColor);
				   memset (buffer, ' ', 80);
				   buffer [79] = 0;
                   x += size.cx;
                   TextOut (hdc, x, y, buffer, strlen (buffer));
                   if (frm->mask[i].attribut & COLBUTTON)
                   {
                         ToCubFormat (buffer, &frm->mask[i], h);
                   }
                   else
                   {
                         ToFormat (buffer, &frm->mask[i]);
                   }
                   TextOut (hdc, x, y, buffer, strlen (buffer));
                   y += RowHeightP; 
            }
        }
        dest[frmlen] = (char) 0;
}

#endif

void ListClass::ShowSatzNr (HDC hdc, int zeile)
{
          RECT rect; 
          HBRUSH hBrush;
	    

          if (NoRecNr) return;

          rect.top = (int) (double) ((double) UbHeight + zeile * tm.tmHeight * 
			  recHeight * RowHeight); 
		  rect.bottom = (int) (double) ((double) 
			  rect.top + tm.tmHeight * recHeight * RowHeight + 1); // + 1 zum Test 12.09.2000
		  rect.left = 0;
          rect.right = fSatzNr.mask[0].length * tm.tmAveCharWidth;

          hBrush = CreateSolidBrush (LTGRAYCOL);

          HBRUSH oldBrush = SelectObject (hdc, hBrush);
          HPEN oldPen = SelectObject (hdc, GetStockObject (NULL_PEN));
          Rectangle (hdc,rect.left, rect.top, rect.right, rect.bottom);
          SelectObject (hdc, oldPen);
          SelectObject (hdc, oldBrush);
          DeleteObject (hBrush); 
}

int ListClass::GetMidPos (int y)
/**
Mittelposition in Ratserzeile ermitteln.
**/
{
	     int h;

	     if (RowHeight <= 1) return y;

		 h = (int) (double) ((double) tm.tmHeight * RowHeight);
		 y = y + (h - tm.tmHeight) / 2;
		 return y;
}

void ListClass::ShowRowRect (COLORREF bcolor, int zeile)
/**
Zeile farbig hinterlegen.
**/
{
         int x, y, cx, cy;
         HBRUSH hBrush;
		 RECT rect;
		 HDC hdc;


         y = (int) (double) ((double) UbHeight + zeile * tm.tmHeight * 
			  recHeight * RowHeight); 
         x = fSatzNr.mask[0].length * tm.tmAveCharWidth;
		 GetWindowRect (mamain3, &rect);
		 cx = rect.right - rect.left;
		 cy = (int) (double) ((double) tm.tmHeight * recHeight * RowHeight); 
         hBrush = CreateSolidBrush (bcolor);

         HBRUSH oldBrush = SelectObject (hdc, hBrush);
         HPEN oldPen = SelectObject (hdc, GetStockObject (NULL_PEN));
         Rectangle (hdc,x, y, x + cx, y + cy);
         SelectObject (hdc, oldPen);
         SelectObject (hdc, oldBrush);
         DeleteObject (hBrush); 
}
	

void ListClass::ShowFrmRow (HDC hdc, int zeile, int h)
/**
Zeile anzeigen.
**/
{
         int y;
         int x;

         y = (int) (double) ((double) UbHeight + zeile * tm.tmHeight * 
			  recHeight * RowHeight 
			 + tm.tmHeight * h * RowHeight);
		 y = GetMidPos (y);
          if (NoRecNr == 0)
         {
                  x = fSatzNr.mask[0].length * tm.tmAveCharWidth;
         }
         else
         {
                  x = 0;
         }

         if (MustPaint (y))
         {
                 if (NoRecNr == 0)
                 {
                         SetTextColor (hdc,BLACKCOL);
                         ::SetBkColor (hdc,LTGRAYCOL);
                         TextOut (hdc, 0, y, SatzNr, strlen (SatzNr));
                 }

                 SetTextColor (hdc,Color);
                 ::SetBkColor (hdc,BkColor);
                 TextOut (hdc, x, y, frmrow, strlen (frmrow));
         }
}

#ifndef _NEWLST

void ListClass::ShowAktColumn (HDC hdc, int y, int i)
/**
Ein Maskenfeld anzeigen.
**/
{
        int x;
        char buffer [1024];
        int diff;

         diff = 0;
         if (LineForm.frmstart)
         {
			if (NoRecNr)
			{
               diff = 1 + LineForm.mask[LineForm.frmstart - 1].pos[1];
			}
			else
			{
               diff = 1 + LineForm.mask[LineForm.frmstart - 1].pos[1] -
                   fSatzNr.mask[0].length;
			}
         }
         int ubspalte = GetUbSpalte (i);
         if (HscrollStart > 0)
         {
                    if ((ubspalte - UbForm.frmstart) < HscrollStart)
                    {
                        return;;
                    }
         }

         ToFormat (buffer, &DataForm.mask[i]);
         x = (DataForm.mask[i].pos[1] - diff) * tm.tmAveCharWidth;


         SetTextColor (hdc,Color);
         SetBkColor (hdc,BkColor);
         TextOut (hdc, x, y, buffer, strlen (buffer));
}

#else

void ListClass::ShowAktColumn (HDC hdc, int y, int i, int h)
/**
Ein Maskenfeld anzeigen.
**/
{
        int x;
        int cx, cy;
        char buffer [1024];
        int diff;
        int pos;
        SIZE size;
        SIZE sizes;

        if (DataForm.mask[i].pos[0] != h) return;

   		GetTextExtentPoint32 (hdc, "X", 1, &size);
        tm.tmAveCharWidth = size.cx;
        diff = 0;
/*
        if (LineForm.frmstart)
        {
			if (NoRecNr)
			{
               diff = LineForm.mask[LineForm.frmstart - 1].pos[1];
                      
			}
			else
			{
               diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                   fSatzNr.mask[0].length;
			}

        }
*/

		if (LineForm.frmstart)
        {
            if (NoRecNr == FALSE)
            {
                 diff = LineForm.mask[LineForm.frmstart - 1 + HscrollStart].pos[1] -
                   fSatzNr.mask[0].length;
            }
            else
            {
                 diff = LineForm.mask[LineForm.frmstart - 1 + HscrollStart].pos[1];
            }

            if (HscrollStart > 0)
            {
                diff -= LineForm.mask[HscrollStart - 1].pos[1];
            }
        }


        if (NoRecNr); 
        else if ((DataForm.mask[i].pos[1] - diff) <
             fSatzNr.mask[0].length)
        {
            return;
        }

         if ((DataForm.mask[i].attribut & BUTTON) == 0)
         {
               ToFormat (buffer, &DataForm.mask[i]);
         }
#ifndef _PFONT
        x = (DataForm.mask[i].pos[1] - diff) * tm.tmAveCharWidth;
#else

         if (strchr (DataForm.mask[i].picture, 'f') ||
                   (strchr (DataForm.mask[i].picture, 'd') &&
                        strchr (DataForm.mask[i].picture, 'y') == NULL))
         {
                        pos = DataForm.mask[i].pos[1];
                        clipped (buffer);
                   		GetTextExtentPoint32 (hdc, buffer, strlen (buffer), &sizes);
                        pos = (pos + DataForm.mask[i].length - 1) * tm.tmAveCharWidth;
                        pos -= sizes.cx; 
                        pos -= diff * tm.tmAveCharWidth;
                        x = pos;
         } 
         else

         {
                        x = (DataForm.mask[i].pos[1] - diff) * tm.tmAveCharWidth;
         }
#endif

         if ((DataForm.mask[i].attribut & BUTTON) != 0 &&
             (DataForm.mask[i].attribut & REMOVED) == 0)
         {
               if (InInsAttr (DataForm.mask[i].item->GetItemName (), AktRow))
               {
                           return;
               }
               cx = DataForm.mask[i].length * tm.tmAveCharWidth;
               cy = DataForm.mask[i].rows * tm.tmHeight;
               pHdc = hdc;
               if (cy == 0) 
               {
                   cy = (int) (double) ((double) RowHeight * tm.tmHeight);
                   DataForm.mask[i].feldid = 
                           CreateListButton (x, y0 + 1, cx, cy - 2, 
                                ListhFont, DataForm.mask[i].item->GetFeldPtr (), NULL);
               }
               else
               {
                   DataForm.mask[i].feldid = 
                           CreateListButton (x, y, cx, cy, 
                                ListhFont, DataForm.mask[i].item->GetFeldPtr (), NULL);
               }
               return;
         }
         else if ((DataForm.mask[i].attribut & CHECKBUTTON) != 0 &&
                      (DataForm.mask[i].attribut & REMOVED) == 0)
         {
               cx = DataForm.mask[i].length * size.cx;
               cy = DataForm.mask[i].rows * tm.tmHeight;
               pHdc = hdc;
               if (cy == 0) 
               {
                           cy = (int) (double) ((double) RowHeight * tm.tmHeight);
                           DataForm.mask[i].feldid = 
                                CreateCheckButton (x, y0 + 1, cx, cy - 2, 
                                     ListhFont, DataForm.mask[i].item->GetFeldPtr (), NULL);
                }
                else
                {
                            DataForm.mask[i].feldid = 
                                CreateCheckButton (x, y, cx, cy, 
                                      ListhFont, DataForm.mask[i].item->GetFeldPtr (), NULL);
                }
                memcpy ( SwSaetze [i], ausgabesatz, zlen); 
                return;
         }
         SetTextColor (hdc,Color);
         ::SetBkColor (hdc,BkColor);
	     memset (buffer, ' ', 80);
		 buffer [79] = 0;
         TextOut (hdc, x, y, buffer, strlen (buffer));
         ToFormat (buffer, &DataForm.mask[i]);
         if (DataForm.mask[i].length > 0)
         {
                TestTextLen (hdc, buffer, (int) DataForm.mask[i].length);
         }
         else
         {
                TestTextLen (hdc, buffer, (int) strlen (buffer));
         }
         TextOut (hdc, x, y, buffer, strlen (buffer));
}
#endif

void ListClass::ShowAktRow (void)
/**
Aktuelle Zeile anzeigen.
**/
{
	     RECT rect;
         HDC hdc;
         int y;
         int i;
         int z;
         int h;

         if (RowMode) return;

         hdc = GetDC (mamain3);
         for (h = 0; h < recHeight; h ++)
         {
            z = AktRow - scrollpos;
            y = (int) (double) ((double) UbHeight + 
				z * tm.tmHeight * recHeight * RowHeight +
                h * tm.tmHeight * RowHeight);
            y0 = y;
 		    y = GetMidPos (y);
            for (i = UbForm.frmstart; i < DataForm.fieldanz; i ++)
            {
               ShowAktColumn (hdc, y, i, h);
            }
         }

         PrintHline (hdc, z);
         PrintVline (hdc, z);

         ReleaseDC (mamain3, hdc);
         SetFeldFocus0 (AktRow, AktColumn);
         GetClientRect (mamain3, &rect);
		 rect.bottom = UbHeight;
		 InvalidateRect (mamain3, &rect, FALSE);
		 UpdateWindow (mamain3);
}
          
void ListClass::ShowDataForms (HDC hdc)
/**
Seite anzeigen
**/
{
        int i, h;
        int pos;
        SIZE size;

        if (DataForm.mask == NULL) return;

        if (ListhFont)
        {
               SelectObject (hdc, ListhFont);
        }
		GetTextExtentPoint32 (hdc, "X", 1, &size);
        tm.tmAveCharWidth = size.cx;
        for (i = 0, pos = scrollpos; i < lPagelen + 1; i ++, pos ++)
        {
                  if (pos == recanz) break;
                  memcpy (ausgabesatz, SwSaetze [pos], zlen); 
                  sprintf (SatzNr, "%5ld ", pos + 1);

				  ShowSatzNr (hdc, i);
                  for (h = 0; h < recHeight;h ++)
                  {
                         ShowNoScrollArray (hdc, frmrow, &DataForm, i, h);
                         if (h == 0)
                         {
                              sprintf (SatzNr, "%5ld ", pos + 1);
                         }
                         else
                         {
                              sprintf (SatzNr, "      ");
                         }
#ifndef _PFONT
                         FillFrmRow (frmrow, &DataForm, h);
                         ShowFrmRow (hdc, i, h);
#else
                         FillFrmRow (hdc, frmrow, &DataForm, i, h);
#endif
                  }
        }
        memcpy (ausgabesatz, SwSaetze [AktRow], zlen); 
}


void ListClass::ShowPrintForms (HDC hdc, int rows)
/**
Seite anzeigen
**/
{
        int i;
        int pos;

        if (DataForm.mask == NULL) return;

        PrintHdc = TRUE;
        for (i = 0, pos = printscrollpos; i < pagelen0; i ++, pos ++)
        {
                  if (pos == recanz) break;
                  memcpy (ausgabesatz, SwSaetze [pos], zlen); 
                  sprintf (SatzNr, "%5ld ", pos + 1);
				  ShowSatzNr (hdc, i);
                  for (int h = 0; h < rows;h ++)
                  {
                         ShowNoScrollArray (hdc, frmrow, &DataForm, i, h);
                         if (h == 0)
                         {
                              sprintf (SatzNr, "%5ld ", pos + 1);
                         }
                         else
                         {
                              sprintf (SatzNr, "      ");
                         }
                         FillFrmRow (hdc, frmrow, &DataForm, i, h);
                  }
        }
}

void ListClass::GetFocusText (void)
/**
Focus-Text aus Fenster holen.
**/
{
	    char buffer [1024];
		int i;

        int len;
        char *feld;

		i = AktColumn;
        memcpy (ausgabesatz, SwSaetze [AktRow], zlen); 
        if (DataForm.mask [AktColumn].attribut & COMBOBOX)
        {
                int idx = SendMessage (FocusWindow, CB_GETCURSEL, 0, 0);
                if (idx >= 0)
                {
                     SendMessage (FocusWindow, CB_GETLBTEXT, idx, 
                         (LPARAM) DataForm.mask [AktColumn].item->GetFeldPtr ());
                }
        } 
        else if (DataForm.mask [AktColumn].attribut & CHECKBUTTON)
        {
                ListButton *lb = (ListButton *) DataForm.mask[AktColumn].item->GetFeldPtr ();
                if (lb != NULL)
                {
                     if (SendMessage (FocusWindow, BM_GETCHECK, 0, 0l) == BST_CHECKED)
                     {
                         lb->Check (TRUE);
                     }
                     else
                     {
                         lb->Check (FALSE);
                     }
                }
        }
        else if (numeric (DataForm.mask [AktColumn].picture))
        {
            GetWindowText (FocusWindow,
                           DataForm.mask [AktColumn].item->GetFeldPtr (),
                           atoi (DataForm.mask [AktColumn].picture));
        }
        else 
        {
            GetWindowText (FocusWindow,
                           DataForm.mask [AktColumn].item->GetFeldPtr (),
                           DataForm.mask [AktColumn].length);
        }

        if (UseZiel == 0 && RowMode)
        {
                 len = DataRow->mask[AktRow].length;
                 feld = DataRow->mask[AktRow].item->GetFeldPtr();
                 if (len > DataForm.mask[AktColumn].length)
                 {
                          len = DataForm.mask[AktColumn].length;
                 }
                 memcpy (feld,
                         DataForm.mask[AktColumn].item->GetFeldPtr (),
                         len);
                 len = DataRow->mask[AktRow].length;
                 feld[len] = (char) 0;
                 ToFormat (buffer, &DataRow->mask[AktRow]);
        }
        else
        {
                 ToFormat (buffer, &DataForm.mask[AktColumn]);
        }

        if (DataForm.mask [AktColumn].attribut & CHECKBUTTON == 0)
        {
                 strcpy (DataForm.mask[AktColumn].item->GetFeldPtr (), buffer);
                 strcpy (AktValue, 
                         DataForm.mask[AktColumn].item->GetFeldPtr ());
        }
        memcpy (SwSaetze [AktRow], ausgabesatz, zlen); 
}
	 

void ListClass::ShowWindowText (int x, int y)
/**
Focus-Text anzeigen.
**/
{
	    char buffer [1024];
		char dest[1024];

		strcpy (dest, " "); 
        memcpy (ausgabesatz, SwSaetze [AktRow], zlen); 
        ToFormat (buffer, &DataForm.mask[AktColumn]);
        strcat (dest, buffer);

		if (listclipp)
		{
		      clipped (buffer);
		}
        else if (strchr (DataForm.mask[AktColumn].picture, 'f') ||
                   (strchr (DataForm.mask[AktColumn].picture, 'd') &&
                        strchr (DataForm.mask[AktColumn].picture, 'y') == NULL))
        {
              clipped (buffer);
        }
        SetWindowText (FocusWindow, buffer);
		SetFocus (FocusWindow);
		if (setsel == FALSE)
		{
          if (strcmp (buffer, " "))
		  {
                  SendMessage (FocusWindow, EM_SETSEL, (WPARAM) MAKELONG (-1, 0), 
			                                   (LPARAM) strlen (buffer));
		  }
		  return;
		}
		if (strlen (buffer))
		{
          SendMessage (FocusWindow, EM_SETSEL, (WPARAM) 0, MAKELONG (-1, 0));
		}

}

/*
void ListClass::ZielFormat (char *buffer, FELDER *zfeld)
{
        int len;

        len = strlen (DataForm.mask[AktColumn].item->GetFeldPtr ());
        if (len > zfeld->feldlen)
        {
            len = zfeld->feldlen;
        }
        memcpy (buffer,DataForm.mask[AktColumn].item->GetFeldPtr (), 
                len);
        buffer [zfeld->feldlen] = (char) 0;
}
*/

        

void ListClass::ShowFocusText (HDC hdc, int x, int y)
/**
Focus-Text anzeigen.
**/
{
	    char buffer [1024];
		char dest[1024];

		strcpy (dest, " "); 
        memcpy (ausgabesatz, SwSaetze [AktRow], zlen); 
        if (RowMode == 0)
        {
                  ToFormat (buffer, &DataForm.mask[AktColumn]);
        }
/*
        else
        {
                  ZielFormat (buffer, &LstZiel[AktRow]);
        }
*/
		strcat (dest, buffer);
        SetTextColor (hdc,Color);
        ::SetBkColor (hdc,BkColor);
        TextOut (hdc, x, y, dest, strlen (dest) - 1);
}



void ListClass::ScrollLeft (void)
/**
Ein Feld nach rechts scrollen.
**/
{

         RECT rect;

         GetClientRect (mamain3, &rect);
//         rect.left += fSatzNr.mask[0].length * tm.tmAveCharWidth;
         if (NoRecNr == 0)
         {
                    rect.left += fSatzNr.mask[0].length * tm.tmAveCharWidth;
         }
         else
         {
                    rect.left = 0;
         }
         if (HscrollStart > 0)
         {
                    rect.left += UbForm.mask[HscrollStart - 1].length * tm.tmAveCharWidth;
         }
		 rect.top += UbHeight;

         if (UbForm.frmstart > 0)
         {
                     UbForm.frmstart --;
                     SetDataStart ();
                     CloseControls (&UbForm);
                     display_form (mamain3, &UbForm, 0, 0);
                     if (FocusWindow)
                     {
		                       DestroyFocusWindow ();
                     } 
                     InvalidateRect (mamain3, &rect, TRUE);
                     SetScrollPos (hTrack, SB_CTL, UbForm.frmstart,
                                   TRUE);
         }
}

void ListClass::ScrollUbForm (void)
{
        int i;
        HWND Ctab [500];
        
        for (i = 0; i < UbForm.fieldanz; i ++)
        {
            Ctab[i] = UbForm.mask[i].feldid;
            UbForm.mask[i].feldid = NULL;
        }
        display_form (mamain3, &UbForm, 0, 0);

        for (i = 0; i < UbForm.fieldanz; i ++)
        {
            DestroyWindow (Ctab[i]);
        }

}


void ListClass::ScrollRight (void)
/**
Ein Feld nach links scrollen.
**/
{


         RECT rect;

         GetClientRect (mamain3, &rect);
//         rect.left += fSatzNr.mask[0].length * tm.tmAveCharWidth;
         if (NoRecNr == 0)
         {
                    rect.left += fSatzNr.mask[0].length * tm.tmAveCharWidth;
         }
         else
         {
                    rect.left = 0;
         }
         if (HscrollStart > 0)
         {
                    rect.left += UbForm.mask[HscrollStart - 1].length * tm.tmAveCharWidth;
         }
		 rect.top += UbHeight;

         if(UbForm.frmstart < banz - 1)
         {
                     UbForm.frmstart ++;
                     SetDataStart ();
                     CloseControls (&UbForm);
                     display_form (mamain3, &UbForm, 0, 0);
  		             if (FocusWindow)
					 {
					           DestroyFocusWindow ();
					 } 
                     InvalidateRect (mamain3, &rect, TRUE);
                     UpdateWindow (mamain3);
                     SetScrollPos (hTrack, SB_CTL, UbForm.frmstart,
                                   TRUE);
         }
}

void ListClass::ScrollPageUp (void)
/**
Eine Seite nach links scrollen.
**/
{

         RECT rect;

         GetClientRect (mamain3, &rect);
		 if (NoRecNr == 0)
		 {
                     rect.left += fSatzNr.mask[0].length * tm.tmAveCharWidth;
		 }
         if (HscrollStart > 0)
         {
                    rect.left += UbForm.mask[HscrollStart - 1].length * tm.tmAveCharWidth;
         }
		 rect.top += UbHeight;

         UbForm.frmstart -= 5;

		 if (FocusWindow)
		 {
					 DestroyFocusWindow ();
		 }
         if(UbForm.frmstart < 0) 
         {
                     UbForm.frmstart = 0;
         }
         SetDataStart ();

         CloseControls (&UbForm);
         display_form (mamain3, &UbForm, 0, 0);
         InvalidateRect (mamain3, &rect, TRUE);
         SetScrollPos (hTrack, SB_CTL, UbForm.frmstart,
                       TRUE);
}


void ListClass::ScrollPageDown (void)
/**
Eine Seite nach links scrollen.
**/
{

         RECT rect;

         GetClientRect (mamain3, &rect);
		 if (NoRecNr == 0)
		 {
                     rect.left += fSatzNr.mask[0].length * tm.tmAveCharWidth;
		 }
         if (HscrollStart > 0)
         {
                    rect.left += UbForm.mask[HscrollStart - 1].length * tm.tmAveCharWidth;
         }
		 rect.top += UbHeight;

         UbForm.frmstart += 5;

		 if (FocusWindow)
		 {
					 DestroyFocusWindow ();
		 }
         if(UbForm.frmstart > banz - 1) 
         {
                     UbForm.frmstart = banz - 1;
         }
         SetDataStart ();

         CloseControls (&UbForm);
         display_form (mamain3, &UbForm, 0, 0);
         InvalidateRect (mamain3, &rect, TRUE);
         SetScrollPos (hTrack, SB_CTL, UbForm.frmstart,
                       TRUE);
}

void ListClass::SetPos (int pos)
/**
Position setzen.
**/
{
         RECT rect;

         GetClientRect (mamain3, &rect);
         if (NoRecNr == FALSE)
         {
                 rect.left += fSatzNr.mask[0].length * tm.tmAveCharWidth;
         }
         if (HscrollStart > 0)
         {
                    rect.left += UbForm.mask[HscrollStart - 1].length * tm.tmAveCharWidth;
         }
		 rect.top += UbHeight;
 
         if (pos >= 0 && pos <= banz - 1)
         {
                     UbForm.frmstart = pos;
                     SetDataStart ();
                     CloseControls (&UbForm);
                     display_form (mamain3, &UbForm, 0, 0);
                     InvalidateRect (mamain3, &rect, TRUE);
  		             if (FocusWindow)
					 {
					           DestroyFocusWindow ();
					 } 
                     SetScrollPos (hTrack, SB_CTL, UbForm.frmstart,
                                   TRUE);
         }
}
                      
void ListClass::SetTop ()
/**
Position setzen.
**/
{
          
          RECT rect;

          GetClientRect (mamain3, &rect);
		  if (NoRecNr == 0)
		  {
                  rect.left += fSatzNr.mask[0].length * tm.tmAveCharWidth;
		  }
 		  rect.top += UbHeight;

          UbForm.frmstart = 0;
          SetDataStart ();
          CloseControls (&UbForm);
          display_form (mamain3, &UbForm, 0, 0);
		  if (FocusWindow)
		  {
					 DestroyFocusWindow ();
		  }
          InvalidateRect (mamain3, &rect, TRUE);
          SetScrollPos (hTrack, SB_CTL, UbForm.frmstart,
                                   TRUE);
}

                      
void ListClass::SetBottom ()
/**
Position setzen.
**/
{
           
          RECT rect;

          GetClientRect (mamain3, &rect);

          UbForm.frmstart = banz - 1;;
          SetDataStart ();
          CloseControls (&UbForm);
          display_form (mamain3, &UbForm, 0, 0);
    	  if (FocusWindow)
		  {
					 DestroyFocusWindow ();
		  }
          InvalidateRect (mamain3, &rect, TRUE);
          SetScrollPos (hTrack, SB_CTL, UbForm.frmstart,
                                   TRUE);
}
                      

void ListClass::HScroll (WPARAM wParam, LPARAM lParam)
/**
Meldung WM_HSCROLL erhalten.
**/
{
         switch (LOWORD (wParam))
         {
             case SB_LINEDOWN :
             {    
                     ScrollRight ();
                     break;
             }
             case SB_LINEUP :
             {    
                     ScrollLeft ();
                     break;
             }
             case SB_PAGEDOWN :
             {    
                     ScrollPageDown ();
                     break;
             }
             case SB_PAGEUP :
             {    
                     ScrollPageUp ();
                     break;
             }
             case SB_THUMBPOSITION :
                     SetPos (HIWORD (wParam));
                     break;
             case SB_TOP :
                     SetTop ();
                     break;
             case SB_BOTTOM :
                     SetBottom ();
                     break;
         }
}


void ListClass::SetNewRow (int row)
/**
Position setzen.
**/
{
         RECT rect;

		 if (row < scrollpos)
		 {
			 SetVPos (row);
		 }
		 GetPageLen ();
		 if (row > (scrollpos + lPagelen) - 1)
		 {
			 SetVPos (row);
		 }
         GetClientRect (mamain3, &rect);
         rect.top = UbHeight;
		 SetPos (row, 0);
/*
         if (FocusWindow)
		 {
					           DestroyFocusWindow ();
		 } 
         InvalidateRect (mamain3, &rect, TRUE);
         SetScrollPos (vTrack, SB_CTL, scrollpos, TRUE);
*/
}
                      

void ListClass::SetVPos (int pos)
/**
Position setzen.
**/
{
         RECT rect;

         GetClientRect (mamain3, &rect);
         rect.top = UbHeight;

         if (pos >= 0 && pos <= recanz - 1)
         {
                     scrollpos = pos;
  		             if (FocusWindow)
					 {
					           DestroyFocusWindow ();
					 } 
                     InvalidateRect (mamain3, &rect, TRUE);
                     SetScrollPos (vTrack, SB_CTL, scrollpos,
                                   TRUE);
         }
}
                      
void ListClass::EditScroll (void)
/**
Focus nachh rechts setzen.
**/
{
          RECT rect;

          DestroyFocusWindow (); 
          GetClientRect (mamain3, &rect);
          rect.top += UbHeight;
          rect.left += fSatzNr.mask[0].length * tm.tmAveCharWidth;

          InvalidateRect (mamain3, &rect, TRUE);
}
  
void ListClass::ScrollWindow0 (HWND mamain3, int start, int height, RECT *rectv, RECT *rectb)
{
	     int i;
		 int plus;

		 if (height >= 0) 
		 {
			 plus = 1;
		 }
		 else
		 {
			 plus = -1;
			 height *= -1;
		 }

        
		 for (i = 0; i < height; i += 1)
		 {
                     ScrollWindow (mamain3, start, 
											plus,
                                            rectv, rectb);
//					 Sleep (1);
		 }
}
  

void ListClass::ScrollDown (void)
/**
Ein Feld nach unten scrollen.
**/
{
         RECT rect;
         RECT newrows;

         GetClientRect (mamain3, &rect);
         GetClientRect (mamain3, &newrows);
         rect.top = (int) (double) ((double) UbHeight + 1);;

         newrows.top = (int) (double) ((double) 
			 newrows.bottom - 2 * UbHeight * recHeight * RowHeight);
		 GetPageLen ();

         if(scrollpos < recanz - 1)
         {
                     scrollpos ++;
					 if (FocusWindow)
					 {
						 DestroyFocusWindow ();
					 }

                     ScrollWindow (mamain3, 0, 
                                            (int) (double) ((double) 
											-tm.tmHeight * recHeight 
											* RowHeight),
                                            &rect, &rect);


                     InvalidateRect (mamain3, &newrows, FALSE);
                     InvalidateRect (mamain3, &FocusRect, FALSE);
                     UpdateWindow (mamain3);
                     SetScrollPos (vTrack, SB_CTL, scrollpos,
                                   TRUE);
         }
}
         
void ListClass::ScrollUp (void)
/**
Ein Feld nach unten scrollen.
**/
{

         RECT rect;
         RECT newrows;

         GetClientRect (mamain3, &rect);
         GetClientRect (mamain3, &newrows);

         rect.top = (int) (double) ((double) UbHeight + 1);
         newrows.top = UbHeight;
         if (recanz - scrollpos >= lPagelen)
         {
                  newrows.bottom = (int) (double) ((double) 
					  newrows.top + UbHeight * recHeight * RowHeight);
         }

         if(scrollpos > 0)
         {
                     scrollpos --;
					 if (FocusWindow)
					 {
						 DestroyFocusWindow ();
					 }

                     ScrollWindow (mamain3, 0, 
						 (int) (double) ((double) 
						 tm.tmHeight * recHeight * RowHeight),
                                            &rect, &rect);

                     InvalidateRect (mamain3, &newrows, FALSE);
                     InvalidateRect (mamain3, &FocusRect, FALSE);
                     UpdateWindow (mamain3);
                     SetScrollPos (vTrack, SB_CTL, scrollpos,
                                   TRUE);
         }
}

void ListClass::ScrollVPageDown (void)
/**
Ein Feld nach unten scrollen.
**/
{
         RECT rect;

         GetClientRect (mamain3, &rect);
         rect.top = tm.tmHeight;
		 int scrollalt;

		 scrollalt = scrollpos;
         scrollpos += lPagelen - 1;

         if(scrollpos > recanz - 1)
         {
                     scrollpos = recanz - 1;
         }
		 AktRow += scrollpos - scrollalt;
		 if (AktRow > recanz - 1) AktRow = recanz - 1;

		 if (FocusWindow)
		 {
					 DestroyFocusWindow ();
		 }
         InvalidateRect (mamain3, &rect, TRUE);
         InvalidateRect (mamain3, &FocusRect, FALSE);
         UpdateWindow (mamain3);
         SetScrollPos (vTrack, SB_CTL, scrollpos,
                                   TRUE);
}

void ListClass::ScrollVPageUp (void)
/**
Ein Feld nach unten scrollen.
**/
{
         RECT rect;
		 int scrollalt;

		 scrollalt = scrollpos;
         GetClientRect (mamain3, &rect);
         rect.top = tm.tmHeight;

         scrollpos -= lPagelen - 1;

         if(scrollpos < 0)
         {
                     scrollpos = 0;
         }

		 AktRow += scrollpos - scrollalt;
		 if (AktRow < 0) AktRow = 0;
		 if (FocusWindow)
		 {
					 DestroyFocusWindow ();
		 }
         InvalidateRect (mamain3, &rect, TRUE);
         InvalidateRect (mamain3, &FocusRect, FALSE);
         UpdateWindow (mamain3);
         SetScrollPos (vTrack, SB_CTL, scrollpos,
                                   TRUE);
}


void ListClass::VScroll (WPARAM wParam, LPARAM lParam)
/**
Meldung WM_VSCROLL erhalten.
**/
{
         switch (LOWORD (wParam))
         {
             case SB_LINEDOWN :
             {    
                     ScrollDown ();
                     break;
             }
             case SB_LINEUP :
             {    
                     ScrollUp ();
                     break;
             }

             case SB_PAGEDOWN :
             {    
                     ScrollVPageDown ();
                     break;
             }
             case SB_PAGEUP :
             {    
                     ScrollVPageUp ();
                     break;
             }
             case SB_THUMBPOSITION :
                     SetVPos (HIWORD (wParam));
                     break;
             case SB_TOP :
                     SetTop ();
                     break;
             case SB_BOTTOM :
                     SetBottom ();
                     break;
         }
}


int ListClass::TestAfterR (void)
/**
Funktion After fuer aktives Feld testen.
**/
{
         if (DataRow->mask[AktRow].after)
         {
             return (*DataRow->mask[AktRow].after) ();
         }
         return 0;
}

int ListClass::TestAfter (void)
/**
Funktion After fuer aktives Feld testen.
**/
{
         int ret;

	     posset = FALSE;
         
         if (Keys)
         {
             if ((ret = Keys->AfterCol (DataForm.mask[AktColumn].item->GetItemName ())) != 0)
             {
                 memcpy (ausgabesatz, SwSaetze [AktRow], zlen); 
                 return ret;
             }
         }
         if (DataForm.mask[AktColumn].after)
         {
             return (*DataForm.mask[AktColumn].after) ();
         }
         return 0;
}

int ListClass::TestAfter (int Col)
/**
Funktion After fuer aktives Feld testen.
**/
{
         int ret;

	     posset = FALSE;
         
         if (Keys)
         {
             if ((ret = Keys->AfterCol (DataForm.mask[Col].item->GetItemName ())) != 0)
             {
                 memcpy (ausgabesatz, SwSaetze [AktRow], zlen); 
                 return ret;
             }
         }
         if (DataForm.mask[Col].after)
         {
             return (*DataForm.mask[Col].after) ();
         }
         return 0;
}

int ListClass::TestBeforeR (void)
/**
Funktion After fuer aktives Feld testen.
**/
{
         if (DataRow->mask[AktRow].before)
         {
             return (*DataRow->mask[AktRow].before) ();
         }
         return 0;
}

int ListClass::TestBefore (void)
/**
Funktion After fuer aktives Feld testen.
**/
{
         int ret;
         
	     posset = FALSE; 
         if (Keys)
         {
             if ((ret = Keys->BeforeCol (DataForm.mask[AktColumn].item->GetItemName ())) != 0)
             {
                 memcpy (ausgabesatz, SwSaetze [AktRow], zlen); 
                 return ret;
             }
         }
         if (DataForm.mask[AktColumn].before)
         {
             return (*DataForm.mask[AktColumn].before) ();
         }
         return 0;
}

void ListClass::InitListButtons (form *frm)
/**
Form initialisieren.
**/
{
       int i;
       ListButton *listButton;
       
       for (i = 0; i < frm->fieldanz; i ++)
       {
           if ((frm->mask[i].attribut & BUTTON) == FALSE)
           {
                  continue;
           }
           listButton = (ListButton *) frm->mask[i].item->GetFeldPtr ();
           if (listButton != NULL)
           {
           }
       }
}
       

void ListClass::InitForm (form *frm)
/**
Form initialisieren.
**/
{
       int i;
       
       for (i = 0; i < frm->fieldanz; i ++)
       {
           if (frm->mask[i].attribut & BUTTON)
           {
                  InitListButton (&frm->mask[i]);
                  continue;
           }
           else if (strcmp (frm->mask[i].picture, "C"))
           {
                  InitField (&frm->mask[i]);
           }
       }
       if (Keys != NULL)
       {
           Keys->InitRow ();
       }
}
       

void ListClass::InsertLine (void)
/**
Eine Zeile in Liste einfuegen.
**/
{
         int i;

         if (RowMode) return;

         if (TestAppend)
         {
            syskey = KEY6;
            if ((*TestAppend) () == 0)
            {
                return;
            }
         }

         InAppend = 1; 
         SetInsAttr ();
         SetPos (AktRow, FirstColumn ());

         InvalidateRect (mamain3, &FocusRect, TRUE);
         DestroyFocusWindow ();
         for (i = recanz; i > AktRow; i --)
         {
                   memcpy (SwSaetze[i], SwSaetze[i - 1], zlen);
         }

         recanz ++;
         SwRecs = recanz;
         InitForm (&DataForm);
         memcpy (SwSaetze[i], ausgabesatz, zlen);
         GetPageLen ();
         MoveListWindow ();
		 DisplayList ();
/*
         InvalidateRect (mamain3, NULL, TRUE);
         UpdateWindow (mamain3);
*/
         NewRec = 1;
}

void ListClass::AppendLine (void)
/**
Eine Zeile in Liste einfuegen.
**/
{
         int i;
		 RECT rect;

         if (RowMode) return;

         if (TestAppend)
         {
            syskey = KEY8;
            if ((*TestAppend) () == 0)
            {
                return;
            }
         }

         InvalidateRect (mamain3, &FocusRect, TRUE);
         InAppend = 1; 
         NewRec = 1; 
         SetInsAttr ();
         SetPos (AktRow, FirstColumn ());

         DestroyFocusWindow ();
         if (recanz > 0)
         {
             AktRow ++;
         }
         SelRow = AktRow;
         for (i = recanz; i > AktRow; i --)
         {
                   memcpy (SwSaetze[i], SwSaetze[i - 1], zlen);
         }

         recanz ++;
         SwRecs = recanz;
         InitForm (&DataForm);
         memcpy (SwSaetze[i], ausgabesatz, zlen);
         GetPageLen ();
         MoveListWindow ();

		 GetClientRect (mamain3, &rect);
		 rect.top += UbHeight * tm.tmHeight;
		 DisplayList ();
/*
         InvalidateRect (mamain3, NULL, TRUE);
         UpdateWindow (mamain3);
*/
}

void ListClass::DeleteLine (void)
/**
Eine Zeile in Liste loeschen.
**/
{
         int i;
		 RECT rect;

         if (RowMode) return;

         if (TestAppend)
         {
             syskey = KEY7;
            if ((*TestAppend) () == 0)
            {
                return;
            }
         }

         if (recanz <= 1)
         {
	                InvalidateFocusFrame = TRUE;
                    InitForm (&DataForm);
                    memcpy (SwSaetze[0], ausgabesatz, zlen);
                    InAppend = 1;
                    NewRec = 1;
                    SetInsAttr ();
                    SetPos (AktRow, FirstColumn ());
                    ShowAktRow ();
                    SetFeldFocus0 (AktRow, AktColumn);
					InvalidateFocusFrame = FALSE;
                    return;
         }

         InAppend = 0; 
         NewRec = 0;
         SetInsAttr ();
         SetPos (AktRow, FirstColumn ());
         DestroyFocusWindow ();
         memcpy (ausgabesatz, SwSaetze[AktRow], zlen);
//         InitListButtons (&DataForm); 
         for (i = AktRow; i < recanz; i ++)
         {
                   memcpy (SwSaetze[i], SwSaetze[i + 1], zlen);
         }

         recanz --;
         if (AktRow > recanz) AktRow --;
         GetPageLen ();
         MoveListWindow ();
		 GetClientRect (mamain3, &rect);
		 rect.top += UbHeight;
//		 DisplayList ();

         InvalidateRect (mamain3, &rect, TRUE);
         UpdateWindow (mamain3);


         if (AktRow >= recanz && syskey != KEYUP)
         {
             AktRow --;
             SetFeldFocus0 (AktRow, AktColumn); 
         }
	  	 InvalidateFocusFrame = FALSE;
         memcpy (ausgabesatz, SwSaetze[AktRow], zlen);
		 TestBeforeRow ();
}


void ListClass::FunkKeys (WPARAM wParam, LPARAM lParam)
/**
Meldung WM_HSCROLL erhalten.
**/
{
#ifndef NINFO
 		 char where [1024];
         char *Item;
#endif

         KeySend = FALSE;
		 if (mamain3 == NULL) return;

		 if (FocusWindow)
		 {
			 GetFocusText ();
		 }

         if (Keys != NULL)
         {
                if (Keys->OnKey (LOWORD (wParam))) return;
         }

         switch (LOWORD (wParam))
         {
             case VK_F2 :
#ifndef NINFO
  		            sprintf (where, "where %s = %s",
							  GetAktItem (), 
							  GetAktValue ()); 
                    DisableWindows (mamain2);
					_CallInfoEx (hMainWindow, NULL,
							     GetAktItem (), 
								 where, 0l);
                    EnableWindows (mamain2);
#endif
					 break; 
             case VK_F3 :
/*
				     ListFocus = 1;
                     InvalidateRect (mamain3, &FocusRect, TRUE);
	                 UpdateWindow (mamain3);
*/
                     FindNext ();
					 break;
             case VK_F4 :
#ifndef NINFO
                    if (Cinfo != NULL)
                    {
  		                 sprintf (where, "where %s = %s",
							      Item,GetAktValue ()); 
                         Cinfo->CallInfo (hMainWindow, NULL, GetAktItem (), where, 0l);
                         break;
                    }
                    if (InfoProc)
                    {
                         Item = GetAktItem ();
                         (*InfoProc) (&Item, GetAktValue (), where);
                    }
                    else
                    {
                         Item = GetAktItem ();
  		                 sprintf (where, "where %s = %s",
							      Item,GetAktValue ()); 
                    }
                    DisableWindows (mamain2);
				    _CallInfoEx (hMainWindow, NULL,
							     Item, 
								 where, 0l);
                    EnableWindows (mamain2);
#endif
					break;
             case VK_F5 :
                     syskey = KEY5;
                     testkeys ();
					 break;
             case VK_F6 :
                     syskey = KEY6;
                     testkeys ();
					 break;
             case VK_F7 :
                     syskey = KEY7;
                     testkeys ();
					 break;
             case VK_F8 :
                     syskey = KEY8;
                     testkeys ();
					 break;
             case VK_F9 :
                     syskey = KEY9;
                     testkeys ();
					 break;
             case VK_F10 :
                      syskey = KEY10; 
                      testkeys ();
                      break;
             case VK_F11 :
                      syskey = KEY11; 
                      testkeys ();
                      break;
             case VK_F12 :
                      syskey = KEY12; 
                      testkeys ();
                      break;
             case VK_RETURN :
                     syskey = KEYCR;
				     if (ListFocus == 0)
					 {
                            ScrollRight ();
					 }
                     else if (RowMode)
                     {
                            TestAfterR ();
                            FocusDown (); 
                            TestBeforeR ();
                     }
					 else
					 {
                            if (TestAfter () == 0 && posset == FALSE)
                            {
				                     FocusRight ();
                            }
                            else
                            {
                          	         SetFeldFocus0 (AktRow, AktColumn);
                            }
/*
                            TestBefore ();
							if (posset)
							{
                          	         SetFeldFocus0 (AktRow, AktColumn);
							}
*/

					 }
                     break;
             case VK_RIGHT :
                     syskey = KEYRIGHT;
				     if (ListFocus == 0)
					 {
                            ScrollRight ();
					 }
					 else
					 {
				            FocusRight ();
					 }
                     break;
             case VK_LEFT :
                     syskey = KEYLEFT;
				     if (ListFocus == 0)
					 {
                            ScrollLeft ();
					 }
					 else
					 {
				            FocusLeft ();
					 }
                     break;
             case VK_DOWN :
                     syskey = KEYDOWN;
				     if (ListFocus == 0)
					 {
                            ScrollDown ();
					 }
					 else
					 {
				            FocusDown ();
					 }
                     break;
             case VK_UP :
                     syskey = KEYUP;
				     if (ListFocus == 0)
					 {
                            ScrollUp ();
					 }
					 else
					 {
				            FocusUp ();
					 }
                     break;
             case VK_NEXT :
                     syskey = KEYPGD;
                     break;
             case VK_PRIOR :
                     syskey = KEYPGU;
                     ScrollVPageUp ();
                     break;
             case VK_HOME :
                     SetTop ();
                     break;
             case VK_END :
                     SetBottom ();
                     break;
             case VK_TAB :
				     syskey = KEYTAB;
                     if (GetKeyState (VK_SHIFT) < 0)
                     {
                              syskey = KEYLEFT; 
						      FocusLeft ();
                     }
					 else if (syskey == KEYSTAB)
                     {
                              syskey = KEYLEFT; 
						      FocusLeft ();
                     }
					 else
					 {
                            if (TestAfter () == 0 && posset == FALSE)
                            {
                                  syskey = KEYRIGHT; 
 			                      FocusRight ();
                            }
                            else
                            {
                                  syskey = KEYRIGHT; 
                          	      SetFeldFocus0 (AktRow, AktColumn);
                            }
					 }
					 break;
             default :
                     if (GetKeyState (VK_MENU) < 0)
                     {
                         testmenu ((int) wParam);
                     }
                     else
                     {
                         KeySend = TRUE;
                     }
                     break; 
         }
         TestBefore ();
 		 if (posset)
		 {
           	         SetFeldFocus0 (AktRow, AktColumn);
		 }
}

void ListClass::FillUbForm (field *feld, char *feldname, int len, int spalte,
                 int BuId  )
/**
Maskenfeld fuer Ueberschruft fuellen.
**/
{
        memcpy (feld, &iButton, sizeof (field)); 
        clipped (feldname);
        feld->item       = new ITEM ("", feldname, "", 0);
        feld->length     = len;
        feld->pos[0]     = 0;
        feld->pos[1]     = spalte;
        feld->BuId       = BuId;
}


void ListClass::FreeDlgSaetze (void)
{
      int i;

      for (i = 0; i < DlgAnz; i ++)
      {
          if (DlgSaetze[i])
          {
              free (DlgSaetze[i]);
          }
      }
      DlgAnz = 0;
}
       
void ListClass::FromDlgRec (char *rec)
/**
Satz f�r Seitenansicht �bertragen.
**/
{

       return;
}

void ListClass::ToDlgRec (char *rec)
/**
Satz f�r Seitenansicht �bertragen.
**/
{

       return;
}

void ListClass::FromDlgRec0 (char *rec)
/**
Satz f�r Seitenansicht �bertragen.
**/
{
       int i;
       int len, len1;
       char *adr;

       memcpy (ausgabesatz, rec, zlen);
       for (i = 0; i < banz; i ++)
       {
           adr = DataRow->mask[i].item->GetFeldPtr ();
           len1 = DataRow->mask[i].length;
           len = strlen (adr);
           if (len > len1)
           {
               len = len1;
           }
           memcpy (adr, dlgstab[i].wert, len);
           adr [len] = (char) 0;
       }
       memcpy (rec, ausgabesatz, zlen);
}


void ListClass::ToDlgRec0 (char *rec)
/**
Satz f�r Seitenansicht �bertragen.
**/
{
       int i;
       int len;
//       char *adr;
//       char ibez [21];
//       char *bez;
       char *name;

       memcpy (ausgabesatz, rec, zlen);
       for (i = 0; i < banz; i ++)
       {
           DlgSaetze [i] = (char *) &dlgstab[i];

           name = UbForm.mask[i].item->GetFeldPtr ();
        
           len = strlen (name);
           if (len > 19) len = 19;

           strcpy (dlgstab[i].bez, name);

           name = DataForm.mask[i].item->GetFeldPtr ();
        
           len = strlen (name);
           if (len > MAXLEN) len = MAXLEN;

           strcpy (dlgstab[i].wert, name);
       }
       DlgAnz = i;
}


void ListClass::SetPage0 (int pos)
/**
Satz in Seitenansicht.
**/
{
       if (recanz == 0) return;
       if (banz == 0) return;

       if (PageView)
       {
           PageView = 0;
       }
       else
       {
           PageView = 1;
       }

       if (PageView)
       {
                AktRowS     = AktRow;
                AktColumnS  = AktColumn;
                banzS = banz;
                zlenS = zlen;
                LstSatz = ausgabesatz;
                LstSaetze = SwSaetze;
                SwRecs = recanz;
                ToDlgRec0 (SwSaetze [pos]);
                AktRow = FirstColumnR ();
                SetPos (AktRow, 1);
                SelRow = 0;
                ausgabesatz = DlgSatz;
                zlen = sizeof (struct DLGS);
                SetRecanz (banz);
                Setbanz (2);
                SetRowMode (1);
                SetSaetze (DlgSaetze);
                memcpy (&UbForm, &UbDlg, sizeof (form));
                memcpy (&DataForm, &DataDlg, sizeof (form));
       }
       else
       {
                ausgabesatz = LstSatz;
                zlen = zlenS;
                SetPos (AktRowS, AktColumnS);
                SelRow = AktRow;
                Setbanz (banzS);
                SetRowMode (0);
                SetRecanz (SwRecs);
                SetSaetze (LstSaetze);
                FromDlgRec0 (SwSaetze [pos]);
                memcpy (&UbForm, UbRow, sizeof (form));
                memcpy (&DataForm, DataRow, sizeof (form));
       }
}

void ListClass::SetPage (int pos)
/**
Satz in Seitenansicht.
**/
{
       if (recanz == 0) return;
       if (banz == 0) return;

       if (PageView)
       {
           PageView = 0;
       }
       else
       {
           PageView = 1;
       }

       if (PageView)
       {
                AktRowS     = AktRow;
                AktColumnS  = AktColumn;
                banzS = banz;
                zlenS = zlen;
//                LstZiel = ziel;
                LstSatz = ausgabesatz;
                LstSaetze = SwSaetze;
                SwRecs = recanz;
                ToDlgRec (SwSaetze [pos]);
                SetPos (0, 1);
                SelRow = 0;
//                ziel = DlgZiel;
                ausgabesatz = DlgSatz;
                zlen = Dlgzlen;
                SetRecanz (banz);
                Setbanz (2);
                SetRowMode (1);
                SetSaetze (DlgSaetze);
       }
       else
       {
//                ziel = LstZiel;
                ausgabesatz = LstSatz;
                zlen = zlenS;
                SetPos (AktRowS, AktColumnS);
                SelRow = AktRow;
                Setbanz (banzS);
                SetRowMode (0);
                SetRecanz (SwRecs);
                SetSaetze (LstSaetze);
                FromDlgRec (SwSaetze [pos]);
       }
}

void ListClass::SwitchPage (int pos)
/**
Satz in Seitenansicht.
**/
{
       if (recanz == 0) return;
       if (banz == 0) return;

       if (PageView)
       {
           PageView = 0;
       }
       else
       {
           PageView = 1;
       }

       DestroyTrack ();
       DestroyVTrack ();
	   DestroyFocusWindow ();
       FreeUbForm ();
       FreeDataForm ();
       FreeLineForm ();
       if (PageView)
       {
                AktRowS     = AktRow;
                AktColumnS  = AktColumn;
                AktPage     = AktRow;
                scrollposS  = scrollpos;
                banzS = banz;
                zlenS = zlen;
//                LstZiel = ziel;
                LstSatz = ausgabesatz;
                LstSaetze = SwSaetze;
                SwRecs = recanz;
                ToDlgRec (SwSaetze [pos]);
                SetPos (0, 1);
                SelRow = 0;
//                ziel = DlgZiel;
                ausgabesatz = DlgSatz;
                zlen = Dlgzlen;
                SetRecanz (banz);
                Setbanz (2);
                SetRowMode (1);
                this->MakeUbForm ();
                SetSaetze (DlgSaetze);
                Initscrollpos ();
                MakeDataForm0 ();
       }
       else
       {
//                ziel = LstZiel;
                ausgabesatz = LstSatz;
                zlen = zlenS;
                SetPos (AktRowS, AktColumnS);
                SelRow = AktRow;
                Setbanz (banzS);
                SetRowMode (0);
                this->MakeUbForm ();
                SetRecanz (SwRecs);
                SetSaetze (LstSaetze);
                FromDlgRec (SwSaetze [pos]);
                Initscrollpos ();
                MakeDataForm0 ();
                Setscrollpos (scrollposS);
       }
}

void ListClass::NextPageRow (void)
/**
Naechsten Satz im PageView-Mode anzeigen.
**/
{
       if (PageView == 0) return; 
       DestroyFocusWindow ();
       if (UseZiel)
       {
           SetPage (AktRowS);
       }
       else
       {
           SetPage0 (AktRowS);
       }
       if (AktRowS < SwRecs - 1)
       {
           AktRowS = AktRowS + 1;
           AktRow  = AktRowS;
       }
       if (UseZiel)
       {
           SetPage (AktRowS);
       }
       else
       {
           SetPage0 (AktRowS);
       }
       SendMessage (mamain2, WM_SIZE, NULL, NULL);
       InvalidateRect (mamain3, 0, TRUE);
}

void ListClass::PriorPageRow (void)
/**
Naechsten Satz im PageView-Mode anzeigen.
**/
{
       if (PageView == 0) return; 
       DestroyFocusWindow ();
       if (UseZiel)
       {
           SetPage (AktRowS);
       }
       else
       {
           SetPage0 (AktRowS);
       }
       if (AktRowS > 0)
       {
           AktRow = AktRowS - 1;
           AktRowS = AktRow;
       }
       if (UseZiel)
       {
           SetPage (AktRowS);
       }
       else
       {
           SetPage0 (AktRowS);
       }
       SendMessage (mamain2, WM_SIZE, NULL, NULL);
       InvalidateRect (mamain3, 0, TRUE);
}

void ListClass::FillDataForm (field *feld, char *feldname, int len, int spalte)
/**
Maskenfeld fuer Daten fuellen.
**/
{
        return;
}

void ListClass::FillLineForm (field *feld, int spalte)
/**
Maskenfeld fuer Daten fuellen.
**/
{
        memcpy (feld, &iEdit, sizeof (field)); 
        feld->pos[1]     = spalte;
}

void ListClass::GetPageLen (void)
/**
Seitenlaenge ermitteln.
**/
{
        RECT rect;
        int y;

        GetClientRect (mamain2, &rect);
        y = rect.bottom - 16;
        lPagelen = (int) ((double) (y - UbHeight) / tm.tmHeight); 
        lPagelen = (int) (double) ((double) lPagelen / (recHeight * RowHeight));
        if (lPagelen > recanz)
        {
                     lPagelen = recanz;
        }
}

void ListClass::MakeLineForm (form *frm)
/**
Form fuer daten generieren.
**/
{
        
        return;
}

void ListClass::SetDataForm (form *frm)
/**
Form fuer Daten setzen.
**/
{
        int i;
        memcpy (&DataForm, frm, sizeof (form));
        if (DataForm.fieldanz > MAXFIELD)
        {
                  DataForm.fieldanz = MAXFIELD;
        }

        for (i = 0; i < DataForm.fieldanz; i ++)
        {
            memcpy (&_DataForm[i], &DataForm.mask[i], sizeof (field));
        }
        DataForm.mask = _DataForm;
        DataForm.font = &ListFont;
}

void ListClass::SetLineForm (form *frm)
/**
Form fuer Daten setzen.
**/
{
        int i;

        memcpy (&LineForm, frm, sizeof (form));
        if (LineForm.fieldanz > MAXFIELD)
        {
                  LineForm.fieldanz = MAXFIELD;
        }
        for (i = 0; i < LineForm.fieldanz; i ++)
        {
            memcpy (&_LineForm[i], &LineForm.mask[i], sizeof (field));
        }
        LineForm.mask = _LineForm;
        LineForm.font = &ListFont;
}

void ListClass::SetUbForm (form *frm)
/**
Form fuer Daten setzen.
**/
{
        int i, j;

        memcpy (&UbForm, frm, sizeof (form));
        if (UbForm.fieldanz > MAXFIELD)
        {
                  UbForm.fieldanz = MAXFIELD;
        }
        for (i = 0, j = 0; i < UbForm.fieldanz;i ++)
        {
            if (UbForm.mask[i].pos[0] == 0)
            {
                  memcpy (&_UbForm[j], &UbForm.mask[i], sizeof (field));
                  j ++;
            }
        }
        UbForm.fieldanz = j;
        UbForm.mask = _UbForm;
        fUbSatzNr.font = &UbSFont;
//        CloseControls (&fUbSatzNr);
        fSatzNr.font   = &SFont;
        UbForm.font    = &UbFont;
        if (UbForm.mask[0].rows == 0)
        {
            fUbSatzNr.mask[0].rows = 0;
            UbHeight = tm.tmHeight + tm.tmHeight / 3; 
        }
        else 
        {
            fUbSatzNr.mask[0].rows = UbForm.mask[0].rows;
            UbHeight = tm.tmHeight * UbForm.mask[0].rows; 
        }
		if (NoRecNr == FALSE)
		{
                 display_form (mamain3, &fUbSatzNr, 0, 0);
		}
        display_form (mamain3, &UbForm, 0, 0);
        if (TrackNeeded ())
        {
                 CreateTrack ();
        }
        UseZiel = 1;
}


void ListClass::MakeDataForm (form *frm)
/**
Form fuer daten generieren.
**/
{
        
        return;
}

void ListClass::DestroyRmFields (void)
/**
Removefelder aus Maske loeschen.
**/
{
        int i;

        return;
        for (i = 0; i < DataForm.fieldanz;)
        {
            if ((DataForm.mask[i].attribut & REMOVED) != 0)
            {
                    DelListField (DataForm.mask[i].item->GetItemName ());
            }
            else
            {
                    i ++;
            }
        }
}


void ListClass::SetDataForm0 (form *dform, form *lform)
/**
Forms f�r Daten generieren.
**/
{

        GetPageLen ();
        SetDataForm (dform);
        SetLineForm (lform);
        UseZiel = 0;
        DestroyRmFields ();
}



void ListClass::MakeDataForm0 (void)
/**
Forms f�r Daten generieren.
**/
{

        GetPageLen ();
        MakeDataForm (&DataForm);
        MakeLineForm (&LineForm);
        UseZiel = 1;
}


void ListClass::MakeUbForm (void)
/**
Form fuer Ueberschrift generieren.
**/
{

        return;
}



void ListClass::FreeDataForm (void)
/**
Item fuer UbForm freigeben.
**/
{
        int i;

        CloseControls (&DataForm);
        if (DataForm.mask)
        {   
             for (i = 0; i < banz; i ++)
             {
                  if (DataForm.mask[i].item)
                  {
                            delete DataForm.mask[i].item;                 
                            DataForm.mask[i].item = NULL;
                  }
             }
             GlobalFree (DataForm.mask);
        }
        memcpy (&DataForm, &IForm, sizeof (form));
}

void ListClass::FreeLineForm (void)
/**
Item fuer UbForm freigeben.
**/
{
        int i;

        CloseControls (&LineForm);
        if (LineForm.mask)
        {
               for (i = 0; i < banz; i ++)
               {
                  if (LineForm.mask[i].item)
                  {
                            delete LineForm.mask[i].item;                 
                            LineForm.mask[i].item = NULL;
                  }
               }
               GlobalFree (LineForm.mask);
        }
        memcpy (&LineForm, &IForm, sizeof (form));
}

void ListClass::FreeUbForm (void)
/**
Item fuer UbForm freigeben.
**/
{
        int i;

        CloseControls (&UbForm);
        if (UbForm.mask)
        {
               for (i = 0; i < banz; i ++)
               {
                  if (UbForm.mask[i].item)
                  {
                            delete UbForm.mask[i].item;                 
                            UbForm.mask[i].item = NULL;
                  }
               }
               GlobalFree (UbForm.mask);
               
        }
        memcpy (&UbForm, &IForm, sizeof (form));
}

int ListClass::IsUbRow (MSG *msg)
/**
Test, ob der Mousecursor ueber der Feldueberschrift steht.
**/
{
        POINT mousepos;
        RECT rect;
        int i;

        if (UbForm.mask == NULL) return FALSE;

        for (i = 0; i < UbForm.fieldanz; i ++)
        {
            if (msg->hwnd == mamain3)
            {
                break;
            }
            else if (msg->hwnd == UbForm.mask[i].feldid)
            {
                break;
            }
        }
        if (i == UbForm.fieldanz) return FALSE;

//        return TRUE;


        GetCursorPos (&mousepos);
        ScreenToClient (mamain3, &mousepos);
        GetClientRect (mamain3, &rect);

        if (mousepos.y < 0 ||
//            mousepos.y > tm.tmHeight)
            mousepos.y > UbHeight)
        {
                   return FALSE;
        }
        if (mousepos.x > rect.left &&
            mousepos.x < rect.right)
        {
                   return TRUE;
        }
        return FALSE;
}


int ListClass::IsUbEnd (MSG *msg)
/**
Test, ob der Mousecursor ueber dem Ende einer Feldueberschrift steht.
**/
{
        POINT mousepos;
        RECT rect;
        int i;
        int diff;
        int x;

        if (LineForm.mask == NULL) return FALSE;
        if (NoMove) return FALSE;

        for (i = 0; i < UbForm.fieldanz; i ++)
        {

            if (msg->hwnd == mamain3)
            {
                break;
            }
            else if (msg->hwnd == UbForm.mask[i].feldid)
            {
                break;
            }
        }
        if (i == UbForm.fieldanz) return FALSE;

        GetCursorPos (&mousepos);
        ScreenToClient (mamain3, &mousepos);
        GetClientRect (mamain3, &rect);

        diff = 0;
/*
        if (LineForm.frmstart)
        {
            diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                   fSatzNr.mask[0].length;
        }
*/


        if (LineForm.frmstart)
        {


            diff = LineForm.mask[LineForm.frmstart + HscrollStart - 1].pos[1];

            if (HscrollStart > 0)
            {
                diff -= LineForm.mask[HscrollStart - 1].pos[1];
            }

			else if (NoRecNr == 0)
			{
                diff -= fSatzNr.mask[0].length;
            }

        }

        if (mousepos.y < 0 ||
//            mousepos.y > tm.tmHeight)
            mousepos.y > UbHeight)
        {
                   return FALSE;
        }

        if (mousepos.x <= rect.left ||
            mousepos.x >= rect.right)
        {
                   return FALSE;
        }

        for (i = UbForm.frmstart; i < UbForm.fieldanz; i ++)
        {
                   x = (UbForm.mask[i].pos[1] - diff) * tm.tmAveCharWidth;
                   if ((x - mousepos.x) < 4 && 
                       (x - mousepos.x) >= 0) 
                   {
                       if (i == 0)
                       {
                            continue;
                       }
                       else
                       {
                            movfield = i - 1;
                            return TRUE;
                       }
                   }
                   else if ((x - mousepos.x) < 0 && 
                            (x - mousepos.x) > -4) 
                   {
                            movfield = i - 1;
                            return TRUE;
                   }
        }
        x = (UbForm.mask[i - 1].pos[1] + UbForm.mask[i - 1].length 
             - diff) * tm.tmAveCharWidth;
        if ((x - mousepos.x) < 4 && 
            (x - mousepos.x) >= 0) 
        {
                    movfield = i - 1;
                    return TRUE;
        }
        else if ((x - mousepos.x) < 0 && 
                 (x - mousepos.x) > -4) 
        {
                    movfield = i - 1;
                    return TRUE;
        }
        return FALSE;
}


void ListClass::ShowArrow (BOOL mode)
/**
Mousecursor als anzeigen.
**/
{

        if (mode == FALSE && IsArrow == FALSE)
        {
            return;
        }

        if (mode)
        {
                IsArrow = 1;
                if (arrow == 0)
                {
//                            arrow =  LoadCursor (NULL, IDC_SIZEWE);
                            arrow =  LoadCursor (hMainInst, 
                                                 "oarrow");
                }
                oldcursor = SetCursor (arrow);
                aktcursor = arrow;
        }
        else
        {
                IsArrow = 0;
                SetCursor (oldcursor);
                aktcursor = oldcursor;
        }
}

HWND mLine = 0;


void ListClass::StartMove (void)
{
        RECT rect;
        POINT mousepos;

        
		if (FocusWindow)
		{
			DestroyFocusWindow ();
		}
        GetCursorPos (&mousepos);
        ScreenToClient (mamain3, &mousepos);
        InMove = 1; 
        GetClientRect (mamain3, &rect);
        mLine = CreateWindow ("static",
                                 "",
                                WS_CHILD | 
                                WS_VISIBLE |
                                SS_BLACKRECT, 
                                mousepos.x, 0,
                                2, rect.bottom,
                                mamain3,
                                NULL,
                                hMainInst,
                                NULL);
}

void ListClass::MoveLine (void)
/**
Line bewegen.
**/
{

        RECT rect;
        POINT mousepos;
       
        if (mLine == NULL) return;

        GetCursorPos (&mousepos);
        ScreenToClient (mamain3, &mousepos);
        GetClientRect (mamain3, &rect);
        MoveWindow (mLine, mousepos.x, 0, 
                           2, rect.bottom, TRUE);
		UpdateWindow (mamain3);
}

int ListClass::GetFieldPos (char*fieldname)
/**
Position fuer Feld holen.
**/
{
        int i;

        clipped (fieldname);
        for (i = 0; i < DataForm.fieldanz; i ++)
        {
            if (strcmp (fieldname,
                        DataForm.mask[i].item->GetItemName ()) == 0)
            {
                return i;
            }
        }
        return -1;
}


void ListClass::DelFormFieldEx (form *frm, int pos, int diff)
/**
Feld aus Dataform herausnehmen.
**/
{
	         int i;
			 int pos1;

			 pos1 = frm->mask[pos].pos[1];
			 frm->fieldanz --;

			 for (i = pos; i < frm->fieldanz; i ++)
			 {
				 memcpy ((char *) &frm->mask[i], (char *) &frm->mask[i + 1], sizeof (field));
			 }

			 for (i = 0; i < frm->fieldanz; i ++)
			 {
				 if (frm->mask[i].pos[1] > pos1)
				 {
					 frm->mask[i].pos[1] -= diff;
				 }
			 }
}


void ListClass::DelListField (char *Item)
{
	         int pos, diff;

		     diff = 0;
		     pos = GetItemPos (&UbForm, Item);
			 if (pos > -1 && pos < UbForm.fieldanz - 1)
			 {
					 diff = UbForm.mask[pos + 1].pos[1] -
						    UbForm.mask[pos].pos[1];
			 }
             if (pos > -1) DelFormFieldEx (&UbForm, pos, diff);
	         if (pos > -1) DelFormFieldEx (&LineForm, pos - 1, diff);
		     pos = GetItemPos (&DataForm, Item);
	         if (pos > -1) DelFormFieldEx (&DataForm, pos, diff);
}

void ListClass::DestroyField (int movfield)
/**
Ein Feld aus der Maske loeschen.
**/
{
        int i;
        int upos;
        int dpos;
        RECT rect;

		return;
        if (recHeight > 1) return;
        if (UbForm.fieldanz <= 1) return;
		if (NoMove) return;
		if (FieldDestroy == FALSE) return;

        CloseControls (&UbForm);
        GetClientRect (mamain3, &rect);
        rect.top += tm.tmHeight;

        if (UseZiel)
        {
          if (movfield > 0 && UbForm.mask[movfield].item)
          {
                  delete UbForm.mask[movfield].item;
                  UbForm.mask[movfield].item = NULL;
          }
          if (movfield > 0 && DataForm.mask[movfield].item)
          {
                  delete DataForm.mask[movfield].item;
                  DataForm.mask[movfield].item = NULL;
          }


          if (movfield > 1)
          {
                  if (LineForm.mask[movfield - 1].item)
                  {
                           delete LineForm.mask[movfield - 1].item;
                           LineForm.mask[movfield - 1].item = NULL;
                  }
          }
        }


        upos = UbForm.mask[movfield].pos[1];
        dpos = DataForm.mask[movfield].pos[1];
        for (i = movfield; i < UbForm.fieldanz - 1; i ++)
        {
                   if (DataForm.mask[i].pos[0] != FrmRow) continue;
                   memcpy (&UbForm.mask[i], &UbForm.mask[i + 1],
                           sizeof (field));
                   memcpy (&DataForm.mask[i], &DataForm.mask[i + 1],
                           sizeof (field));
                   if (i > 0)
                   {
                       memcpy (&LineForm.mask[i - 1], &LineForm.mask[i],
                               sizeof (field));
                   }

                   UbForm.mask[i].pos[1]   = upos;
                   DataForm.mask[i].pos[1] = dpos;

/*
                   if (i > 0)
                   {
                         LineForm.mask[i - 1].pos[1] = pos;
                   }
*/
                   LineForm.mask[i - 1].pos[1] = upos;

                   upos += UbForm.mask[i].length;
                   dpos += DataForm.mask[i].length;
        }
        LineForm.mask[i - 1].pos[1] = upos;

        if (movfield < UbForm.fieldanz)
        {
                   UbForm.fieldanz --;
        }
        if (movfield < DataForm.fieldanz)
        {
                   DataForm.fieldanz --;
        }
        if (movfield < LineForm.fieldanz)
        {
                   LineForm.fieldanz --;
        }
        if (movfield < banz)
        {
                   banz --;
        }

        if (TrackNeeded ())
        {
           CreateTrack ();
           SetScrollRange (hTrack, SB_CTL, 0,
                                   banz - 1, TRUE);
        }
        else
        {
           DestroyTrack ();
        }
        if (hTrack == NULL) return;

        GetClientRect (mamain2, &rect);
        display_form (mamain3, &UbForm, 0, 0);
        InvalidateRect (mamain3, &rect, TRUE);
}

void ListClass::StopMove (void)
{
        POINT mousepos;
        int x;
        int lenu;
        int len;
        int diff;
        RECT rect;
        int i;
		int datapos;
        static int diff0;
        static int movfield0;


		if (NoMove) return;

        GetCursorPos (&mousepos);
        ScreenToClient (mamain3, &mousepos);
        GetClientRect (mamain3, &rect);
        rect.top += tm.tmHeight;

        diff = 0;
        if (LineForm.frmstart)
        {
            if (NoRecNr == FALSE)
            {
               diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                      fSatzNr.mask[0].length;
            }
            else
            {
               diff = LineForm.mask[LineForm.frmstart - 1].pos[1];
            }
        }

        if (movfield < UbForm.fieldanz - 1)
        {
                 x = (UbForm.mask[movfield + 1].pos[1] - diff) * 
                                 tm.tmAveCharWidth;
        }
        else
        {
                 x = (UbForm.mask[movfield].pos[1] + 
                       UbForm.mask[movfield].length - diff) *
                                 tm.tmAveCharWidth;
        }

        if ((x - mousepos.x) < 4 && 
            (x - mousepos.x) > -4) 
        {
                  return;
        }

        x = mousepos.x / tm.tmAveCharWidth + diff;
		for (i = movfield; i < DataForm.fieldanz; i ++)
		{
		    datapos = DataForm.mask[i].pos[1];
			if (datapos >= UbForm.mask[movfield].pos[1]) break;
		}

        if (movfield < UbForm.fieldanz - 1)
        {
                 diff = x - UbForm.mask[movfield + 1].pos[1]; 
        }
        else
        {
                 diff = x - (UbForm.mask[movfield].pos[1] + 
                               UbForm.mask[movfield].length); 
        }

		if (i == DataForm.fieldanz)
		{
	        datapos = DataForm.mask[i - 1].pos[1];
            len  = DataForm.mask[i - 1].length + diff;
		}
		else
		{
            len  = DataForm.mask[i].length + diff;
		}


        if (FrmRow == 0)
        {
            diff0 = diff;
            movfield0 = movfield;
            lenu = UbForm.mask[movfield].length + diff;
        }
        else
        {
            diff = diff0;
            lenu = UbForm.mask[movfield0].length;
        }


        if (len <= 0)
        {
            if (PageView) return;
    		if (FocusWindow)
			{
			     DestroyFocusWindow ();
			}
            DestroyField (movfield);
            return;
        }


        if (movfield < UbForm.fieldanz - 1)
        {
             if (UbForm.mask[movfield + 1].pos[0] == FrmRow)
             {
                   if (FrmRow == 0)
                   {
                         UbForm.mask[movfield + 1].pos[1] = x;
                         LineForm.mask[movfield].pos[1] = x;
                   }
             }

             for ( i = movfield + 2; i < UbForm.fieldanz; i ++)
             {
                  if (UbForm.mask[i].pos[0] != FrmRow) continue;
                  if (FrmRow == 0)
                  {
                         UbForm.mask[i].pos[1]   += diff;
                         LineForm.mask[i - 1].pos[1] += diff;
                  }
             }
        }

		for (i = 0; i < DataForm.fieldanz; i ++)
		{

			   if (DataForm.mask[i].pos[1] == datapos)
			   {
                         DataForm.mask[i].length = len;
			   }

			   if (DataForm.mask[i].pos[1] > datapos)
			   {
                  DataForm.mask[i].pos[1] += diff;
			   }
		}
/*
        if (FrmRow == 0)
        {
               LineForm.mask[LineForm.fieldanz - 1].pos[1] += diff;
               UbForm.mask[UbForm.fieldanz - 1].pos[1] += diff;
        }
*/
        if (FrmRow == 0)
        {
                UbForm.mask[movfield].length = lenu;
        }
//        DataForm.mask[movfield].length = len;

		if (FocusWindow)
		{
			DestroyFocusWindow ();
		}

        CloseControls (&UbForm);
        display_form (mamain3, &UbForm, 0, 0);
        InvalidateRect (mamain3, NULL, TRUE);
        UpdateWindow (mamain3);
}

void ListClass::TestNextLine (void)
{
        int i;

		return;
        for (i = 0 ; i < DataForm.fieldanz; i ++)
        {
            if (DataForm.mask[i].pos[0] != FrmRow) continue;
            if (DataForm.mask[i].pos[1] == 
                DataForm.mask[movfield].pos[1])
            {
                movfield = i;
                StopMove ();
                return;
            }
        }
}


void ListClass::StopMove0 (void)
{
        int h;

        InMove = 0;


        if (mLine)
        {
            DestroyWindow (mLine);
            mLine = NULL;
        }

        for (h = 0; h < recHeight; h ++)
        {
            FrmRow = h;
            if (h == 0) 
            {
                StopMove ();
            }
            else
            {
//                TestNextLine ();
            }
        }
}


void ListClass::DisplayList (void)
/**
Liste anzeigen.
**/
{
        HDC hdc;
        
        paintallways = 1;
        hdc = GetDC (mamain3);
        ShowDataForms (hdc);
        PrintHlines (hdc);
        PrintVlines (hdc);
	    if (! FocusWindow)
        {
          	     SetFeldFocus (hdc, AktRow, AktColumn);
		}
        else
        {
	             SetFocusFrame (hdc, AktRow, AktColumn );
                 PrintSlinesSatzNr (hdc);
        }
        ReleaseDC (mamain3, hdc);
        paintallways = 0;
}

BOOL ListClass::IsMouseMessage (MSG *msg)
/**
Mouse-Meldungen pruefen.
**/
{
    /*
        if (IsComboMessage && (*IsComboMessage) (msg))
        {
              return FALSE;
        }

        if (opencombobox)
        {
              return FALSE;
        }
    */

        switch (msg->message)
        {
        
              case WM_MOUSEMOVE :
                      if (InMove)
                      {
                            SetCursor (aktcursor);
                            MoveLine ();
                            return TRUE;
                      }
                      if (IsUbEnd (msg))
                      {
                              ShowArrow (TRUE);
                      }
                      else
                      {
                              ShowArrow (FALSE);
                      }
                      return FALSE;
              case WM_LBUTTONDOWN :
                      if (IsUbRow (msg) == FALSE)
                      {
                              return IsInListArea (msg);
                      }
                      if (aktcursor == arrow)
                      {
                                SetCursor (aktcursor);
                                StartMove ();
                                return TRUE;
                      }
                      if (aktcursor)
                      {
                                SetCursor (aktcursor);
                      }
                      return TRUE;
              case WM_LBUTTONDBLCLK :
                      if (IsUbRow (msg) == FALSE)
                      {
                              return FALSE;
                      }
                      if (aktcursor == arrow)
                      {
                                SetCursor (aktcursor);
                                StartMove ();
                                return TRUE;
                      }
                      if (aktcursor)
                      {
                                SetCursor (aktcursor);
                      }
                      return TRUE;
              case WM_LBUTTONUP :
                      if (InMove)
                      {
                                StopMove0 ();
                                return TRUE;
                      }
                      if (aktcursor)
                      {
                                SetCursor (aktcursor);
                      }
                      if (IsUbRow (msg))
                      {
                              return TRUE;
                      }
                      else
                      {
                              return FALSE;
                      }
        }
        return FALSE;
}
void ListClass::PaintUb (void)
{
	     CloseControls (&UbForm);
		 display_form (mamain3, &UbForm, 0, 0);
}

void ListClass::OnPaint (HWND hWnd,UINT msg, WPARAM wParam,LPARAM lParam)
{
         HDC hdc;

         if (hWnd == mamain2)
         {
                    MoveListWindow ();
         }
         else if (hWnd == mamain3)
         {
                    hdc = BeginPaint (mamain3, &aktpaint);
                    ShowDataForms (hdc);
                    PrintHlines (hdc);
                    PrintVlines (hdc);
					if (! FocusWindow)
					{
                	     SetFeldFocus (hdc, AktRow, AktColumn);
					}
                    else
                    {
 		                 SetFocusFrame (hdc, AktRow, AktColumn);
                         PrintSlinesSatzNr (hdc);
                    }
                    EndPaint (mamain3,&aktpaint);
         }
         else if (hWnd == choise)
         {
                    hdc = BeginPaint (choise, &aktpaint);
                    ChoiseLines (hdc); 
                    EndPaint (choise, &aktpaint);
         }
         else
         {
             if (Keys)
             {
                    Keys->OnPaint (hWnd,msg, wParam, lParam);
             }
         }
}

void ListClass::OnNotify (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        LPNMHDR pnmh = (LPNMHDR) lParam;

        if (pnmh->code == TTN_NEEDTEXT)
        {
               LPTOOLTIPTEXT lpttt = (LPTOOLTIPTEXT) lParam;
               if (QuickCpy (lpttt->szText, lpttt->hdr.idFrom)
                              == FALSE)
               {
                        QuickHwndCpy (lpttt);
               }
        }
        return;
}


void ListClass::OnHScroll (HWND hWnd,UINT msg, WPARAM wParam,LPARAM lParam)
{
         if ((HWND) lParam == hTrack)
         {
             HScroll (wParam, lParam);
         }
         if (OldList != NULL)
         {
             OldList->OnHScroll (hWnd, msg, wParam, lParam);
         }
}

void ListClass::OnVScroll (HWND hWnd,UINT msg, WPARAM wParam,LPARAM lParam)
{
         if ((HWND) lParam == vTrack)
         {
             VScroll (wParam, lParam);
         }
         if (OldList != NULL)
         {
             OldList->OnVScroll (hWnd, msg, wParam, lParam);
         }
}


void ListClass::OnSize (HWND hWnd,UINT msg, WPARAM wParam,LPARAM lParam)
{
         return;
}

BOOL ListClass::SystemAction (int Key)
{
        if (Key == VK_DOWN ||
            Key == VK_UP)
        {
            if (FocusWindow != NULL &&
                DataForm.mask[AktColumn].attribut & COMBOBOX)
            {
                    if (SendMessage (FocusWindow, CB_GETDROPPEDSTATE, 0, 0l))
                    {
                        return TRUE;
                    }
            }
        }
        return FALSE;
}

BOOL ListClass::PerformMessage (MSG *m)
{
        MSG msg;
        char KeyState [256];
        PBYTE lpKeyState; 

		if (Keys != NULL)
		{
			if (Keys->PreTranslateMessage (m)) return TRUE;
		}
        memcpy (&msg, m, sizeof (msg));
        if (msg.message == WM_KEYDOWN)
        {
                       switch (msg.wParam)
                       {
                              case VK_F5 :
                              case VK_F3 :
                              case VK_F4 :
                              case VK_F6 :
                              case VK_F7 :
                              case VK_F8 :
                              case VK_F9 :
                              case VK_F10 :
                              case VK_F11 :
                              case VK_F12 :
                              case VK_DOWN :
                              case VK_RETURN:
                              case VK_UP :
                              case VK_NEXT :
                              case VK_PRIOR :
                              case VK_HOME :
                              case VK_END :
                              case VK_TAB :
                              case VK_DELETE :
                                       if (SystemAction (msg.wParam))
                                       {
                                           TranslateMessage(&msg);
                                           DispatchMessage(&msg);
                                           return TRUE;
                                       }

								       FunkKeys (msg.wParam,
										         msg.lParam);
                                       if (KeySend)
                                       {
                                             TranslateMessage(&msg);
                                             DispatchMessage(&msg);
                                       }
									   return TRUE;
                              case 'A' :
                                 if (GetKeyState (VK_CONTROL) < 0)
                                 {
                                       lpKeyState = (PBYTE) KeyState; 
                                       GetKeyboardState (lpKeyState);
                                       lpKeyState [VK_CONTROL] = (char) 0;
                                       SetKeyboardState (lpKeyState);
                                       msg.wParam = '0';
                                       TranslateMessage(&msg);
                                       DispatchMessage(&msg);
                                       msg.wParam = 'X';
                                       TranslateMessage(&msg);
                                       DispatchMessage(&msg);
                                       msg.wParam = 'A';
                                       TranslateMessage(&msg);
                                       DispatchMessage(&msg);
                                       return TRUE;
                                 }
                       }
                       if (GetKeyState (VK_MENU) < 0)
                       {
                                       if (testmenu ((int) msg.wParam))
                                       {
									          return TRUE;
                                       }
                       }
        }
        else if (msg.message == WM_SYSKEYDOWN)
        {
                      switch (msg.wParam)
                      {
                              case VK_F10 :
								       FunkKeys (msg.wParam,
										         msg.lParam);
									   return TRUE;
                      }
                      if (GetKeyState (VK_MENU) < 0)
                      {
                                       if (testmenu ((int) msg.wParam))
                                       {
									          return TRUE;
                                       }
                      }
        }
        if (IsMouseMessage (&msg))
        {
                       return TRUE;
        }
        TranslateMessage(&msg);
        DispatchMessage(&msg);
        return TRUE;  
}


void ListClass::FirstPos ()
{
        AktRow = 0;
        AktColumn = FirstColumn ();
}

int ListClass::ProcessMessages(void)
{
        MSG msg;
        char KeyState [256];
        PBYTE lpKeyState; 

        if (RowMode == 0)
        {
                FirstPos ();
//		        AktRow = 0;
//		        AktColumn = FirstColumn ();
        }
        ListBreak = 0;
		SetFeldFocus0 (AktRow, AktColumn);
        TestBefore ();
  	    if (posset)
		{
           	         SetFeldFocus0 (AktRow, AktColumn);
		}
        while (GetMessage (&msg, NULL, 0, 0))
        {
			 if (Keys != NULL)
			 {
				if (Keys->PreTranslateMessage (&msg)) continue;
			 }
             if (msg.message == WM_KEYDOWN)
             {
                       switch (msg.wParam)
                       {
                              case VK_F5 :
                              case VK_F3 :
                              case VK_F4 :
                              case VK_F6 :
                              case VK_F7 :
                              case VK_F8 :
                              case VK_F9 :
                              case VK_F10 :
                              case VK_F11 :
                              case VK_F12 :
                              case VK_DOWN :
                              case VK_RETURN:
                              case VK_UP :
                              case VK_NEXT :
                              case VK_PRIOR :
                              case VK_HOME :
                              case VK_END :
                              case VK_TAB :
                              case VK_DELETE :
                                       if (SystemAction (msg.wParam))
                                       {
                                           break;
                                       }

								       FunkKeys (msg.wParam,
										         msg.lParam);
                                       if (KeySend)
                                       {
                                             TranslateMessage(&msg);
                                             DispatchMessage(&msg);
                                       }
									   continue;
                              case 'A' :
                                 if (GetKeyState (VK_CONTROL) < 0)
                                 {
                                       lpKeyState = (PBYTE) KeyState; 
                                       GetKeyboardState (lpKeyState);
                                       lpKeyState [VK_CONTROL] = (char) 0;
                                       SetKeyboardState (lpKeyState);
                                       msg.wParam = '0';
                                       TranslateMessage(&msg);
                                       DispatchMessage(&msg);
                                       msg.wParam = 'X';
                                       TranslateMessage(&msg);
                                       DispatchMessage(&msg);
                                       msg.wParam = 'A';
                                       TranslateMessage(&msg);
                                       DispatchMessage(&msg);
                                       continue;
                                 }
                       }
                       if (GetKeyState (VK_MENU) < 0)
                       {
                                       if (testmenu ((int) msg.wParam))
                                       {
									          continue;
                                       }
                       }
                 }
                 else if (msg.message == WM_SYSKEYDOWN)
                 {
                      switch (msg.wParam)
                      {
                              case VK_F10 :
								       FunkKeys (msg.wParam,
										         msg.lParam);
									   continue;
                      }
                      if (GetKeyState (VK_MENU) < 0)
                      {
                                       if (testmenu ((int) msg.wParam))
                                       {
									          continue;
                                       }
                      }
                 }
                 if (IsMouseMessage (&msg))
                 {
                       continue;
                 }
                 TranslateMessage(&msg);
                 DispatchMessage(&msg);
        }

        if (recanz > 1)
        {
             InAppend = 0;
             NewRec = 0;
             SetInsAttr ();
        }
        CloseControls (&fUbSatzNr);
        return msg.wParam;
}


// Ende ListClass


void ListClassDB::FreeBezTab (void)
/**
Speicher fuer feldbztab freigeben.
**/
{
        int i;

        for (i = 0; i < fbanz; i ++)
        {
            if (feldbztab[i])
            {
                GlobalFree (feldbztab[i]);
                feldbztab[i] = NULL;
            }
        }
        fbanz = 0;
}

void ListClassDB::FillUbForm (field *feld, char *feldname, int len, int spalte,
                 int BuId  )
/**
Maskenfeld fuer Ueberschruft fuellen.
**/
{
        char *bez;
        char *name;
//        char ibez [21];

        name = feldname;
        memcpy (feld, &iButton, sizeof (field)); 
        bez = NULL;
//        bez = GetIfItem (name, ibez);
#ifndef NINFO
        if (bez == NULL)
        {
            bez = GetItemName (feldname);
        }
#endif
        if (bez)
        {
                   feldbztab[fbanz] = (char *) GlobalAlloc (0, 21);
                   if (feldbztab[fbanz])
                   {
                       strcpy (feldbztab[fbanz], bez);
                       name = feldbztab [fbanz];
                       if (fbanz < MAXBEZ - 1)
                       {
                           fbanz ++;
                       }
                   }
        }
        clipped (name);
        feld->item       = new ITEM ("", name, "", 0);
        feld->length     = len;
        feld->pos[0]     = 0;
        feld->pos[1]     = spalte;
        feld->BuId       = BuId;
}


void ListClassDB::MakeUbForm (void)
/**
Form fuer Ueberschrift generieren.
**/
{
        return;
}

void ListClassDB::SwitchPage (int pos)
/**
Satz in Seitenansicht.
**/
{
       if (recanz == 0) return;
       if (banz == 0) return;

       if (PageView)
       {
           PageView = 0;
       }
       else
       {
           PageView = 1;
       }

       DestroyTrack ();
       DestroyVTrack ();
	   DestroyFocusWindow ();
       FreeUbForm ();
       FreeDataForm ();
       FreeLineForm ();
       if (PageView)
       {
                AktRowS     = AktRow;
                AktColumnS  = AktColumn;
                AktPage     = AktRow;
                scrollposS  = scrollpos;
                banzS = banz;
                zlenS = zlen;
//                LstZiel = ziel;
                LstSatz = ausgabesatz;
                LstSaetze = SwSaetze;
                SwRecs = recanz;
                ToDlgRec (SwSaetze [pos]);
                SetPos (0, 1);
                SelRow = 0;
//                ziel = DlgZiel;
                ausgabesatz = DlgSatz;
                zlen = Dlgzlen;
                SetRecanz (banz);
                Setbanz (2);
                SetRowMode (1);
                this->MakeUbForm ();
                SetSaetze (DlgSaetze);
                Initscrollpos ();
                MakeDataForm0 ();
       }
       else
       {
//                ziel = LstZiel;
                ausgabesatz = LstSatz;
                zlen = zlenS;
                SetPos (AktRowS, AktColumnS);
                SelRow = AktRow;
                Setbanz (banzS);
                SetRowMode (0);
                this->MakeUbForm ();
                SetRecanz (SwRecs);
                SetSaetze (LstSaetze);
                FromDlgRec (SwSaetze [pos]);
                Initscrollpos ();
                MakeDataForm0 ();
                Setscrollpos (scrollposS);
       }
}


void ListClassDB::SwitchPage0 (int pos)
/**
Satz in Seitenansicht.
**/
{
       if (recanz == 0) return;
       if (banz == 0) return;

       if (PageView)
       {
           PageView = 0;
       }
       else
       {
           PageView = 1;
       }

       DestroyTrack ();
       DestroyVTrack ();
	   DestroyFocusWindow ();
       if (PageView)
       {
                DlgSatz = (unsigned char *) &dlgs;
                AktRowS     = AktRow;
                AktColumnS  = AktColumn;
                AktPage     = AktRow;
                scrollposS  = scrollpos;
                banzS = banz;
                zlenS = zlen;
                LstSatz = ausgabesatz;
                LstSaetze = SwSaetze;
                SwRecs = recanz;
                ToDlgRec0 (SwSaetze [pos]);
                SetPos (0, 1);
                SelRow = 0;
                ausgabesatz = DlgSatz;
                zlen = sizeof (struct DLGS);
                SetRecanz (banz);
                Setbanz (2);
                SetRowMode (1);
                memcpy (&RowUb,  &UbForm,   sizeof (form));
                memcpy (&RowData,&DataForm, sizeof (form));
                memcpy (&RowLine,&LineForm, sizeof (form));
                UbRow   = &RowUb;
                DataRow = &RowData;
                lineRow = &RowLine;
		        AktRow = FirstColumnR ();
                CloseControls (&UbForm);
                this->SetUbForm (&UbDlg);
                SetSaetze (DlgSaetze);
                Initscrollpos ();
                SetDataForm0 (&DataDlg, &LineDlg);
       }
       else
       {
                ausgabesatz = LstSatz;
                zlen = zlenS;
                SetPos (AktRowS, AktColumnS);
                SelRow = AktRow;
                Setbanz (banzS);
                SetRowMode (0);
                CloseControls (&UbForm);
                this->SetUbForm (UbRow);
                SetRecanz (SwRecs);
                SetSaetze (LstSaetze);
                FromDlgRec0 (SwSaetze [pos]);
                Initscrollpos ();
                SetDataForm0 (DataRow, lineRow);
                Setscrollpos (scrollposS);
       }
}

void ListClass::Register (HINSTANCE hMainInst)
{
    static BOOL registered = FALSE;
    WNDCLASS wc;

    if (registered) return;

    this->hMainInst = hMainInst;

    wc.style =  CS_BYTEALIGNWINDOW | CS_CLASSDC |
                              CS_VREDRAW | CS_HREDRAW;
    wc.lpfnWndProc   =  (WNDPROC) CProc;
    wc.cbClsExtra    =  0;
    wc.cbWndExtra    =  0;
    wc.hInstance     =  hMainInst;
    wc.hbrBackground =  CreateSolidBrush (GRAYCOL);
    wc.hIcon         =  LoadIcon (hMainInst, "FITICON");
    wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
    wc.lpszMenuName  =  NULL;
    wc.lpszClassName =  "ListMain";
    RegisterClass(&wc);

    wc.hbrBackground =  GetStockObject (WHITE_BRUSH);
    wc.lpszClassName =  "hListWindow";
    RegisterClass(&wc);
    registered = TRUE;
}


CALLBACK ListClass::CProc (HWND hWnd,UINT msg,
                          WPARAM wParam,LPARAM lParam)
{

        if (ActiveList)
        {
            switch(msg)
            {
              case WM_PAINT :
		            ActiveList->OnPaint (hWnd, msg, wParam, lParam);
		 	        break;
              case WM_NOTIFY :
                    ActiveList->OnNotify (hWnd, msg, wParam, lParam);
                    break; 
              case WM_HSCROLL :
                       ActiveList->OnHScroll (hWnd, msg,wParam, lParam);
					   return TRUE;
              case WM_VSCROLL :
                     ActiveList->OnVScroll (hWnd, msg,wParam, lParam);
					 return TRUE;
              case WM_MOVE :
                     ActiveList->MoveListWindow ();
					 return TRUE;
              case WM_SIZE :

                     ActiveList->MoveListWindow ();
                     if (ActiveList->GetKeys () != NULL)
                     {
                          if (ActiveList->GetKeys ()->OnMove (hWnd, msg, wParam, lParam))
 					      return TRUE;
                     }
                     break;
              case WM_COMMAND :
                    if (ActiveList->GetKeys () != NULL)
                    {
                       if (ActiveList->GetKeys ()->OnCommand (hWnd, msg, wParam, lParam))
                       {
                           return TRUE;
                       }
                    }
                    break;
              case WM_SYSCOMMAND :
                    if (ActiveList->GetKeys () != NULL)
                    {
                       if (ActiveList->GetKeys ()->OnSysCommand (hWnd, msg, wParam, lParam))
                       {
                           return TRUE;
                       }
                    }
                    if (wParam == SC_CLOSE)
                    {
                        PostQuitMessage (0);
                        return TRUE; 
                    }
                    break;
            }
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}


char *ListClassDB::GetItemName (char *feldname)
/**
Bezeichnung aus Tabelle Item zu Feldname holen.
**/
{
#ifndef NINFO
           int dsqlstatus;  
           dsqlstatus = item_class.lese_item (feldname);
           if (dsqlstatus == 0)
           {
               return (_item.bezkurz);
           }
#endif
           return (NULL);
}


