#ifndef _AG_DEF
#define _AG_DEF

#include "dbclass.h"

struct AG {
   short     abt;
   long      ag;
   char      ag_bz1[21];
   char      ag_bz2[21];
   short     a_typ;
   short     bearb_fil;
   short     bearb_lad;
   short     bearb_sk;
   char      best_auto[2];
   char      bsd_kz[2];
   short     delstatus;
   char      durch_fil[2];
   char      durch_sk[2];
   char      durch_sw[2];
   char      durch_vk[2];
   long      erl_kto;
   short     hbk;
   char      hbk_kz[2];
   short     hbk_ztr;
   char      hnd_gew[2];
   char      kost_kz[3];
   short     me_einh;
   short     mwst;
   char      pfa[2];
   char      pr_ueb[2];
   char      reg_eti[2];
   short     sais1;
   short     sais2;
   short     sg1;
   short     sg2;
   char      smt[2];
   double    sp_fil;
   char      sp_kz[2];
   double    sp_sk;
   double    sp_vk;
   char      stk_lst_kz[2];
   double    sw;
   double    tara;
   short     teil_smt;
   char      theke_eti[2];
   char      verk_art[2];
   long      verk_beg;
   long      verk_end;
   char      waren_eti[2];
   long      we_kto;
   short     wg;
   double    gn_pkt_gbr;
   char      a_krz_kz[2];
   char      tier_kz[3];
   long      erl_kto_1;
   long      erl_kto_2;
   long      erl_kto_3;
   long      we_kto_1;
   long      we_kto_2;
   long      we_kto_3;
};
extern struct AG ag, ag_null;

#line 7 "ag.rh"

class AG_CL : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               AG_CL () : DB_CLASS ()
               {
               }
};
#endif
