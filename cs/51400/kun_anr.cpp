#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "a_kun.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "kun_anr.h"

struct KUN_ANR kun_anr, kun_anr_null;

void KUN_ANR_CLASS::prepare (char *order)
{
            char *sqltext;

            ins_quest ((char *) &kun_anr.mdn, 1, 0);
            ins_quest ((char *) &kun_anr.fil, 1, 0);
//            ins_quest ((char *) &kun_anr.kun, 2, 0);

            if (order)
            {
    out_quest ((char *) &kun_anr.mdn,1,0);
    out_quest ((char *) &kun_anr.fil,1,0);
    out_quest ((char *) &kun_anr.wo_tag,1,0);
    out_quest ((char *) kun_anr.uhrzeit,0,6);
    out_quest ((char *) &kun_anr.kun,2,0);
    out_quest ((char *) &kun_anr.tag,2,0);
    out_quest ((char *) kun_anr.pers_nam,0,9);
    out_quest ((char *) kun_anr.tel,0,21);
    out_quest ((char *) &kun_anr.adr,2,0);
    out_quest ((char *) kun_anr.partner,0,37);
                        cursor = prepare_sql ("select "
"kun_anr.mdn,  kun_anr.fil,  kun_anr.wo_tag,  kun_anr.uhrzeit,  "
"kun_anr.kun,  kun_anr.tag,  kun_anr.pers_nam,  kun_anr.tel,  kun_anr.adr,  "
"kun_anr.partner from kun_anr "

#line 31 "kun_anr.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "%s", order);
            }
            else
            {
    out_quest ((char *) &kun_anr.mdn,1,0);
    out_quest ((char *) &kun_anr.fil,1,0);
    out_quest ((char *) &kun_anr.wo_tag,1,0);
    out_quest ((char *) kun_anr.uhrzeit,0,6);
    out_quest ((char *) &kun_anr.kun,2,0);
    out_quest ((char *) &kun_anr.tag,2,0);
    out_quest ((char *) kun_anr.pers_nam,0,9);
    out_quest ((char *) kun_anr.tel,0,21);
    out_quest ((char *) &kun_anr.adr,2,0);
    out_quest ((char *) kun_anr.partner,0,37);
                        cursor = prepare_sql ("select "
"kun_anr.mdn,  kun_anr.fil,  kun_anr.wo_tag,  kun_anr.uhrzeit,  "
"kun_anr.kun,  kun_anr.tag,  kun_anr.pers_nam,  kun_anr.tel,  kun_anr.adr,  "
"kun_anr.partner from kun_anr "

#line 38 "kun_anr.rpp"
                                  "where mdn = ? "
                                  "and   fil = ?");
            }

    ins_quest ((char *) &kun_anr.mdn,1,0);
    ins_quest ((char *) &kun_anr.fil,1,0);
    ins_quest ((char *) &kun_anr.wo_tag,1,0);
    ins_quest ((char *) kun_anr.uhrzeit,0,6);
    ins_quest ((char *) &kun_anr.kun,2,0);
    ins_quest ((char *) &kun_anr.tag,2,0);
    ins_quest ((char *) kun_anr.pers_nam,0,9);
    ins_quest ((char *) kun_anr.tel,0,21);
    ins_quest ((char *) &kun_anr.adr,2,0);
    ins_quest ((char *) kun_anr.partner,0,37);
            sqltext = "update kun_anr set kun_anr.mdn = ?,  "
"kun_anr.fil = ?,  kun_anr.wo_tag = ?,  kun_anr.uhrzeit = ?,  "
"kun_anr.kun = ?,  kun_anr.tag = ?,  kun_anr.pers_nam = ?,  "
"kun_anr.tel = ?,  kun_anr.adr = ?,  kun_anr.partner = ? "

#line 43 "kun_anr.rpp"
                               "where mdn = ? "
                               "and   fil = ? "
                               "and   kun = ? "
                               "and   wo_tag = ? "
                               "and   uhrzeit = ?";
  
            ins_quest ((char *) &kun_anr.mdn, 1, 0);
            ins_quest ((char *) &kun_anr.fil, 1, 0);  
            ins_quest ((char *) &kun_anr.kun, 2, 0);
            ins_quest ((char *) &kun_anr.wo_tag, 1, 0);
            ins_quest ((char *) &kun_anr.uhrzeit,  0, 6);  

            upd_cursor = prepare_sql (sqltext);

    ins_quest ((char *) &kun_anr.mdn,1,0);
    ins_quest ((char *) &kun_anr.fil,1,0);
    ins_quest ((char *) &kun_anr.wo_tag,1,0);
    ins_quest ((char *) kun_anr.uhrzeit,0,6);
    ins_quest ((char *) &kun_anr.kun,2,0);
    ins_quest ((char *) &kun_anr.tag,2,0);
    ins_quest ((char *) kun_anr.pers_nam,0,9);
    ins_quest ((char *) kun_anr.tel,0,21);
    ins_quest ((char *) &kun_anr.adr,2,0);
    ins_quest ((char *) kun_anr.partner,0,37);
            ins_cursor = prepare_sql ("insert into kun_anr ("
"mdn,  fil,  wo_tag,  uhrzeit,  kun,  tag,  pers_nam,  tel,  adr,  partner) "

#line 58 "kun_anr.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?)"); 

#line 60 "kun_anr.rpp"
            ins_quest ((char *) &kun_anr.mdn, 1, 0);
            ins_quest ((char *) &kun_anr.fil, 1, 0);
            ins_quest ((char *) &kun_anr.kun, 2, 0);
            ins_quest ((char *) &kun_anr.wo_tag, 1, 0);
            ins_quest ((char *) &kun_anr.uhrzeit, 0, 6);
            test_upd_cursor = prepare_sql ("select kun from kun_anr "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   kun = ? "
                                  "and   wo_tag = ? "
                                  "and   uhrzeit = ?");
              
            ins_quest ((char *) &kun_anr.mdn, 1, 0);
            ins_quest ((char *) &kun_anr.fil, 1, 0);
            ins_quest ((char *) &kun_anr.kun, 2, 0);
            ins_quest ((char *) &kun_anr.wo_tag, 1, 0);
            ins_quest ((char *) &kun_anr.uhrzeit, 0, 6);
            del_cursor = prepare_sql ("delete from kun_anr "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   kun = ? "
                                  "and   wo_tag = ? "
                                  "and   uhrzeit = ?");
}

int KUN_ANR_CLASS::dbreadfirst (char *order)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare (order);
         }
         return (this->DB_CLASS::dbreadfirst ());
}

/*
int KUN_ANR_CLASS::dbinsert (void)
**
Insert in WIEPRO
**
{
         if (ins_cursor == -1)
         {
                     prepare (NULL);
         }
         execute_curs (ins_cursor);
         return sqlstatus;
}
         
int KUN_ANR_CLASS::dbupdate (void)
**
Insert in WIEPRO
**
{
        return dbinsert ();
}

int KUN_ANR_CLASS::dbclose (void)
**
Insert in WIEPRO
**
{
        if (cursor != -1)
        { 
                 close_sql (cursor);
                 cursor = -1;
        }
        if (ins_cursor != -1)
        { 
                 close_sql (ins_cursor);
                 ins_cursor = -1;
        }
        if (del_cursor != -1)
        { 
                 close_sql (del_cursor);
                 del_cursor = -1;
        }
        if (test_upd_cursor != -1)
        { 
                 close_sql (test_upd_cursor);
                 test_upd_cursor = -1;
        }
        return 0;
}
*/

