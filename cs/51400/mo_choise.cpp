#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include "comcthlp.h"
#include "strfkt.h"
#include "stdfkt.h"
#include "wmaskc.h"
#include "colbut.h"
#include "dbclass.h"
#include "cmask.h"
#include "mo_choise.h"
#include "lbox.h"

#define BuOk     1001
#define BuCancel 1002
#define LIBOX     1003


static mfont mFont = {
                     "MS Sans Serif", 
                      100, 
                      0, 
                      0, 
                      BLACKCOL, 
                      LTGRAYCOL,
                      0, 0};


static CFIELD **_fListe;
static CFORM *fListe;

static CFIELD **_fButton;
static CFORM *fButton;

static CFIELD **_fWork;
static CFORM *fWork;


CHOISE::CHOISE (int cx, int cy)
{
			TEXTMETRIC tm;
			HFONT hFont, oldfont;
			SIZE size;
			HDC hdc;
			int x,y;

 	        this->Font = &mFont;  

			hdc = GetDC (NULL);
            hFont = SetDeviceFont (hdc, Font, &tm);
            oldfont = SelectObject (hdc, hFont);
	        GetTextMetrics (hdc, &tm);
			GetTextExtentPoint32 (hdc, "X", 1, &size);
			DeleteObject (SelectObject (hdc, oldfont));
	

			this->cx = size.cx * cx;
			this->cy = tm.tmHeight * cy;

            _fListe    = new CFIELD * [1];
			_fListe[0] = new CFIELD ("Liste", "Liste",
				                     cx - 2, cy - 4, 1, 1, NULL, "", CLISTBOX,
												 LIBOX, Font, 0, 0);

			fListe     = new CFORM (1, _fListe);

			x = 0;
			y = (cy - 2) * tm.tmHeight;
			cx = 10 * size.cx;
			cy = (int) (double) ((double) tm.tmHeight * 1.3);
			
			_fButton    = new CFIELD * [2];
			_fButton[0] = new CFIELD ("OK", "OK", cx, cy, x, y, 
				                                  NULL, "", CBUTTON,
                                                  BuOk, Font, 1, BS_PUSHBUTTON);				                                   
 
			_fButton[1] = new CFIELD ("Cancel", "Abbruch", 
				                                  cx , cy, x + 12 * size.cx, y,
				                                  NULL, "", CBUTTON,
                                                  BuCancel, Font, 1, BS_PUSHBUTTON);				    
			fButton     = new CFORM (2, _fButton);

            _fWork    = new CFIELD * [2];
			_fWork[0] = new CFIELD ("fListe", fListe, 0, 0, 0, 0, NULL, "", CFFORM,
				                                 980, Font, 0, 0);
			_fWork[1] = new CFIELD ("Button", fButton, 0, 0, -1, 0, NULL, "", CFFORM,
				                                 981, Font, 0, 0);

			fWork   = new CFORM (2, _fWork);
}

CHOISE::~CHOISE ()
{
	        fWork->destroy (); 
            delete _fListe[0];   
            delete _fListe;   

			delete _fButton[0];
			delete _fButton[1];
			delete fButton;

			delete _fWork[0];
			delete _fWork[1];
			delete fWork;
}


void CHOISE::SetCurrentID (DWORD ID)
{
	       if (fWork->SetCurrentID (ID))
		   {
			   currentfield = ID;
		   }
}

void CHOISE::SetCurrentName (char *name)
{
	       if (fWork->SetCurrentName (name))
		   {
			   currentfield = fWork->GetID (name);
		   }
}

void CHOISE::EnableID (DWORD ID, BOOL flag)
{
	       fWork->EnableID (ID, flag);
}

void CHOISE::EnableName (char *name, BOOL flag)
{
	       fWork->EnableName (name, flag);
}

void CHOISE::CheckID (DWORD ID, BOOL flag)
{
	       fWork->CheckID (ID, flag);
}

void CHOISE::CheckName (char *name, BOOL flag)
{
	       fWork->CheckName (name, flag);
}

BOOL CHOISE::TestButtons (HWND hWndBu)
{
	       if (hWndBu == fWork->GethWndID (BuOk))
		   {
			   SendMessage (hWnd, WM_COMMAND, MAKELONG (BuOk, VK_RETURN),
				                                    (LPARAM) hWnd); 
			   return TRUE;
		   }
	       if (hWndBu == fWork->GethWndID (BuCancel))
		   {
			   SendMessage (hWnd, WM_COMMAND, MAKELONG (BuCancel, VK_RETURN),
				                                    (LPARAM) hWnd); 
			   return TRUE;
		   }
		   return FALSE;
}
	       



void CHOISE::ProcessMessages (void)
{
	      MSG msg;
		  HWND hWnd;

          hWnd = fWork->GethWndID (LIBOX);
		  fWork->SetCurrent (0);
		  fListe->GetCfield () [0]->SetFocus ();
          SendMessage (hWnd, LB_SETCURSEL, 0, 0L);
          while (GetMessage (&msg, NULL, 0, 0))
		  {
			  if (msg.message == WM_KEYDOWN)
			  {
			      if (msg.wParam == VK_TAB)
				  {

                      if (GetKeyState (VK_SHIFT) < 0)
                      {
                              syskey = KEYSTAB;
                              fWork->PriorField (); 
					  }
					  else
					  {
					          syskey = KEYTAB;
                              fWork->NextField (); 
					  }
					  continue;
				  }
				  else if (msg.wParam == VK_F5)
				  {
					  syskey = KEY5;
					  break;
				  }
				  else if (msg.hwnd == fWork->GethWndName ("Liste"));
				  else if (msg.wParam == VK_DOWN)
				  {

					  syskey = KEYDOWN;
                      fWork->NextField (); 
					  continue;
				  }
				  else if (msg.wParam == VK_UP)
				  {

					  syskey = KEYUP;
                      fWork->PriorField (); 
					  continue;
				  }
				  else if (msg.wParam == VK_RETURN)
				  {

					  if (TestButtons (msg.hwnd))
					  {
					           continue;
					  }
				  }
			  }
              TranslateMessage(&msg);
			  DispatchMessage(&msg);
		  }
}


CALLBACK CHOISE::CProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        PAINTSTRUCT ps;
		HDC hdc;
//		DWORD Id;

        switch(msg)
        {
              case WM_PAINT :
                    hdc = BeginPaint (hWnd, &ps);
					fWork->display (hWnd, hdc);
                    EndPaint (hWnd, &ps);
					break;
              case WM_COMMAND :
                    if (LOWORD (wParam) == BuOk)
                    {
						   syskey = KEYCR;
                           PostQuitMessage (0);
                    }
                    else if (LOWORD (wParam) == BuCancel)
                    {
						   syskey = KEY5;
                           PostQuitMessage (0);
                    }
                    else if (LOWORD (wParam) == LIBOX)
                    {
                           if (HIWORD (wParam) == LBN_DBLCLK)
                           {
						          syskey = KEYCR;
						          PostQuitMessage (0); 
						   }
                           if (HIWORD (wParam) == VK_RETURN)
                           {
						          syskey = KEYCR;
						          PostQuitMessage (0); 
						   }
                           if (HIWORD (wParam) == LBN_SELCHANGE)
                           {
		                          fWork->SetCurrent (0);
						   }
                    }
                    break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

void CHOISE::InsertCaption (char *buffer)
{
	     HWND hWnd;

         hWnd = fWork->GethWndID (LIBOX);
		 
		 SendMessage (hWnd, LB_TITLE, -1,  
                                 (LPARAM) (char *) buffer);
}



void CHOISE::InsertRecord (char *buffer)
{
	     HWND hWnd;

         hWnd = fWork->GethWndID (LIBOX);

         SendMessage (hWnd, LB_INSERTSTRING, -1,  
                                 (LPARAM) (char *) buffer);
}

void CHOISE::GetText (char *buffer)
{
	     HWND hWnd;
		 int idx;

         hWnd = fWork->GethWndID (LIBOX);

	     idx = SendMessage (hWnd, LB_GETCURSEL, 0, 0l);
	     SendMessage (hWnd, LB_GETTEXT, idx, (LPARAM) buffer);
}

int CHOISE::GetSel (void)
{
	     HWND hWnd;
		 int idx;

         hWnd = fWork->GethWndID (LIBOX);

	     idx = SendMessage (hWnd, LB_GETCURSEL, 0, 0l);
		 return idx;
}

void CHOISE::VLines (char *vpos, int hlines)
{
	       HWND hWnd;

		   hWnd = fWork->GethWndName ("Liste");
           SendMessage (hWnd, LB_CAPTSIZE, 0,  
                                 (LPARAM) 130);
           SendMessage (hWnd, LB_VPOS, hlines,  
                                 (LPARAM) (char *) vpos);
}

void CHOISE::DestroyWindow (void)
{
	       fWork->destroy ();
		   ::DestroyWindow (hWnd);
}
						
void CHOISE::OpenWindow (HANDLE hInstance, HWND hMainWindow)
{
		  int x, y;
		  BOOL registered = FALSE;
          WNDCLASS wc;
		  RECT rect, rect1;

		  this->hInstance   = hInstance;
		  this->hMainWindow = hMainWindow;
//		  SetColBorderM (TRUE);

		  if (registered == FALSE)
		  {
                  wc.style =  CS_BYTEALIGNWINDOW | CS_CLASSDC;
                  wc.lpfnWndProc   =  (WNDPROC) CProc;
                  wc.cbClsExtra    =  0;
                  wc.cbWndExtra    =  0;
                  wc.hInstance     =  hInstance;
                  wc.hIcon         =  LoadIcon (hInstance, "FITICON");
                  wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                  wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
                  wc.lpszMenuName  =  "";
                  wc.lpszClassName =  "CChoiseWind";

                  RegisterClass(&wc);
				  registered = TRUE;
		  }

		  GetClientRect (hMainWindow, &rect);
		  GetWindowRect (hMainWindow, &rect1);
/*
		  cx = rect.right;
		  cy = rect.bottom;
*/

		  x  = max (0, (rect.right - cx) / 2)  + rect1.left;
		  y  = max (0, (rect.bottom - cy) / 2) + rect1.top;

          hWnd = CreateWindowEx (0, 
                                 "CChoiseWind",
                                 "",
                                 WS_VISIBLE | WS_POPUP | WS_DLGFRAME,
                                 x, y,
                                 cx, cy,
                                 hMainWindow,
                                 NULL,
                                 hInstance,
                                 NULL);
		  ShowWindow (hWnd, SW_SHOWNORMAL);
		  UpdateWindow (hWnd);
}

void CHOISE::MoveWindow (void)
{
		  RECT rect;

		  GetClientRect (hMainWindow, &rect);
          ::MoveWindow (hWnd, rect.left, rect.top, rect.right, rect.bottom, TRUE);

}

