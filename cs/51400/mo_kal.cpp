#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include "comcthlp.h"
#include "itemc.h"
#include "wmaskc.h"
#include "mo_meld.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_intp.h"
#include "listcl.h"
#include "inflib.h"
#include "mo_arg.h"
#include "mo_curso.h"
#include "ptab.h"
#include "mo_kal.h"
#include "mo_qkun.h"
#include "mo_qfil.h"
#include "mdn.h"
#include "fil.h"
#include "kun.h"
#include "mo_progcfg.h"
#include "kun_anr.h"
#include "mo_choise.h"
#include "dbclass.h"
#include "mo_menu.h"
#include "searchpartner.h"

#define MAXLEN 40
#define MAXPOS 30000
#define LPLUS 1

extern HANDLE  hMainInst;

static unsigned char ausgabebuffer [0x1000];
static HWND mamain1;

static int ListFocus = 4;
static double RowHeight = 1.5;

static 	int kunsearch;
static long tour_div = 100;
static BOOL kun_test_kz = TRUE;
static BOOL AbrKz = FALSE;

 
struct KUN_ANRS 
{
   char     mdn [6];
   char     fil [6];
   char     kun [10];
   char     kun_krz1 [18];
   char     wo_tag [6];
   char     wo_tags [4];
   char     zeit [7];
   char     partner [38];
   char     tel [22];
   char     pers_nam [9];
   long     adr;
};

struct KUN_ANRS kun_anrs, kun_anrtab [MAXPOS];

ITEM ikun         ("kun",         kun_anrs.kun,       "", 0);
ITEM ikun_krz1    ("kun_krz1",    kun_anrs.kun_krz1,  "", 0);
ITEM iwo_tag      ("wo_tag",      kun_anrs.wo_tag,    "", 0);
ITEM iwo_tags     ("wo_tags",     kun_anrs.wo_tags,    "", 0);
ITEM izeit        ("zeit",        kun_anrs.zeit,      "", 0);
ITEM ipartner     ("partner",     kun_anrs.partner,   "", 0);
ITEM itel         ("tel",         kun_anrs.tel,       "", 0);
ITEM ipers_nam    ("pers_nam",    kun_anrs.pers_nam,  "", 0);

ITEM islash        ("",            "/",                 "", 0);   

static field  _dataform[] = {
&iwo_tag,      2, 0, 0, 8, 0, "%1d",  DISPLAYONLY, 0, 0, 0,
&islash,       1, 0, 0, 9, 0, "C",    DISPLAYONLY, 0, 0, 0,
&iwo_tags,     4, 0, 0,10, 0, "",     DISPLAYONLY, 0, 0, 0,
&izeit,        6, 0, 0,16, 0, "hh:mm",DISPLAYONLY, 0, 0, 0,
&ikun,         9, 0, 0,23, 0, "%8d",  DISPLAYONLY, 0, 0, 0,
&ikun_krz1,   16, 0, 0,33, 0, "",     DISPLAYONLY, 0, 0, 0,
&itel,        20, 0, 0,51, 0, "",     EDIT,        0, 0, 0,
&ipartner,    20, 0, 0,73, 0, "",     DISPLAYONLY, 0, 0, 0,
&ipers_nam,    9, 0, 0,95, 0, "",     EDIT,        0, 0, 0,
};

static form dataform = {9, 0, 0, _dataform, 0, 0, 0, 0, NULL}; 

static int ubrows[] = {0,0,0, 
                       1,
                       2,
                       3,
                       4,
                       5,
					   6,
};

struct CHATTR ChAttr [] = {"kun",    DISPLAYONLY, EDIT,
                           "wo_tag", DISPLAYONLY, EDIT,
                           "zeit",   DISPLAYONLY, EDIT,
                            NULL,  0,           0};

static FRMDB dbkun_anr[] = {
&dataform, 0, (short *) &kun_anr.wo_tag,      1, 0, "%d",
&dataform, 3, (char *)  kun_anr.uhrzeit,      0, 6, "%s",
&dataform, 4, (long *)  &kun_anr.kun,         2, 0, "%ld",
&dataform, 6, (char *)  kun_anr.tel,          0,20, "%s",
&dataform, 7, (char *)  kun_anr.partner,      0,20, "%s",
&dataform, 8, (char *)  kun_anr.pers_nam,     0, 8, "%s",
NULL,      0,          NULL,         0, 0, "",
};


ITEM iukun         ("ukun",         "Kd.Nr",            "", 0);
ITEM iukun_krz1    ("ukun_krz1",    "Name",             "", 0);
ITEM iuwo_tag      ("uwo_tag",      "Tag",              "", 0);
ITEM iuzeit        ("uzeit",        "Zeit",             "", 0);
ITEM iupartner     ("upartner",     "Partner",          "", 0);
ITEM iutel         ("utel",         "Telefon",          "", 0);
ITEM iupers_nam    ("upers_nam",    "Benutzer",         "", 0);
ITEM iufiller      ("",             "",                 "", 0);

static field  _ubform[] = {
&iuwo_tag,      9, 0, 0, 6, 0, "",  BUTTON, 0, 0, 0,
&iuzeit,        7, 0, 0,15, 0, "",  BUTTON, 0, 0, 0,
&iukun,        10, 0, 0,22, 0, "",  BUTTON, 0, 0, 0,
&iukun_krz1,   18, 0, 0,32, 0, "",  BUTTON, 0, 0, 0,
&iutel,        22, 0, 0,50, 0, "",  BUTTON, 0, 0, 0,
&iupartner,    22, 0, 0,72, 0, "",  BUTTON, 0, 0, 0,
&iupers_nam,   10, 0, 0,94, 0, "",  BUTTON, 0, 0, 0,
&iufiller,    100, 0, 0,104, 0, "",  BUTTON, 0, 0, 0, 
};

static form ubform = {8,0, 0, _ubform, 0, 0, 0, 0, NULL}; 


ITEM iline ("", "1", "", 0);


static field  _lineform[] = {
&iline,      1, 0, 0,15, 0, "",  NORMAL, 0, 0, 0, 
&iline,      1, 0, 0,22, 0, "",  NORMAL, 0, 0, 0, 
&iline,      1, 0, 0,32, 0, "",  NORMAL, 0, 0, 0, 
&iline,      1, 0, 0,50, 0, "",  NORMAL, 0, 0, 0, 
&iline,      1, 0, 0,72, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0,94, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0,104, 0, "",  NORMAL, 0, 0, 0,
};

static form lineform = {7, 0, 0, _lineform, 0, 0, 0, 0, NULL}; 


static DB_CLASS DbClass;
static ListClassDB eListe;
static KUN_ANR_CLASS Kunanr;
static QueryKun QClass;
static QueryFil QFClass;
static KUN_CLASS kun_class;
static FIL_CLASS fil_class;
static ADR_CLASS adr_class;
static MENUE_CLASS User;
static PROG_CFG ProgCfg ("wo_tour");


static char KunItem[] = {"kuna"};
static double AktKun;
static BOOL auf_me_pr_0;

static int InfoProc (char **Item, char *Value, char *where)
/**
Spezielle Procedure fuer Info-System
**/
{

    clipped (*Item);
//    if (strcmp (*Item, "a") == 0)
    {
//        *Item = KunItem;
//        sprintf (where, "where kun = %ld and a = %.0lf", 
//                         aufk.kun, ratod (Value));
//        return 1;
    }
    return 0;
}

static int InfoKun (char **Item, char *Value, char *where)
/**
Info auf Feld Kunde.
**/
{

        eListe.GetFocusText ();
        *Item = "kun";
        if (atol (kun_anrs.kun))
        {
            sprintf (where, "where kun = %ld", atol (kun_anrs.kun));
        }
        else
        {
            sprintf (where, "where kun > 0");
        }
        return 1;
}

int KALIST::ShowPartner (void)
{
       SEARCHPARTNER SearchP;
       char Row [512];
       int anz;
 
       SearchP.SetParams (hMainInst, mamain1);
       SearchP.SetKeys   (kun_anr.mdn, 0, atol (kun_anrs.kun));
       SearchP.Setawin (mamain1);
       SearchP.Search ();
       if (SearchP.GetKeyRow (Row) == TRUE)
       {
           anz = wsplit (Row, "|");
           if (anz >= 6)
           {
               strcpy (kun_anrtab[eListe.GetAktRow ()].tel, wort[5]);
               strcpy (kun_anrtab[eListe.GetAktRow ()].partner, wort[1]);
               kun_anrtab[eListe.GetAktRow ()].adr = atol (wort[0]);
               memcpy (&kun_anrs, &kun_anrtab[eListe.GetAktRow ()], sizeof (struct KUN_ANRS));
               eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
               eListe.ShowAktRow ();
               eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                                     eListe.GetAktColumn ());
           }
       }
       return TRUE; 
}


int KALIST::SetKey9Partner (void)
{
       set_fkt (ShowPartner, 9);
       SetFkt (9, fktpartner, KEY9);
       return 0;
}

KALIST::KALIST ()
{
//    int i;

    mamainmax = 0;
    mamainmin = 0; 
    eingabesatz = NULL;
    ausgabesatz = ausgabebuffer;
    zlen = 0;
    feld_anz = 0;
    banz = 0;
    PageView = 0;
    LstZiel = NULL;
    LstSatz   = NULL;
    Lstbanz = 0;
    SwRecs = 0;
    AktRow = 0;
    AktColumn = 0;
    scrollpos = 0;
    InfoCaption[0] = (char) 0; 
    WithMenue   = 1;
    WithToolBar = 1;
	tour_div = 100;
    dataform.after  = WriteRow; 
    dataform.before = SetRowItem; 

	dataform.mask[0].after = Testday;
	dataform.mask[1].after = Testtime;

    dataform.mask[4].before = SaveKun; 
    dataform.mask[4].after  = fetchKun; 

	dataform.mask[6].before = SetKey9Partner;
//    dataform.mask[8].after  = TestKey9Partner; 

	dataform.mask[8].before = Setsysben;
    dataform.mask[8].after  = fetchSysben; 

    ListAktiv = 0;
    this->hMainWindow = NULL;
}



void KALIST::GetCfgValues (void)
/**
Werte aus 51100.cfg holen.
**/
{
	   static BOOL cfgOK = FALSE;
       char cfg_v [5];

	   if (cfgOK) return;

	   cfgOK = TRUE;
       if (ProgCfg.GetCfgValue ("kunsearch", cfg_v) ==TRUE)
       {
                    kunsearch = atoi (cfg_v);
       }
       else
       {
                    kunsearch = 0;
       }
       if (ProgCfg.GetCfgValue ("searchmodekun", cfg_v) == TRUE)
       {
		            kun_class.SetSearchModeKun (atoi (cfg_v));
       }
       if (ProgCfg.GetCfgValue ("searchfieldkun", cfg_v) == TRUE)
       {
		           kun_class.SetSearchFieldKun (atoi (cfg_v));
       }
       if (ProgCfg.GetCfgValue ("kun_test_kz", cfg_v) == TRUE)
       {
		           kun_test_kz = atoi (cfg_v);
       }
}


void KALIST::SetFieldAttr (char *fname, int attr)
{
         int i;
         char *feldname;

         for (i = 0; i < dataform.fieldanz; i ++)
         {
             feldname = dataform.mask[i].item->GetItemName ();
             if (feldname && strcmp (feldname, fname) == 0)
             {
                   break;
             }
         }
         if (i == dataform.fieldanz) return;

         dataform.mask[i].attribut = attr;
}


int KALIST::SetRowItem (void)
{
       return 0;
}

int KALIST::TestAppend (void)
/**
Testen, ob ein Satz angehaengt werden darf.
**/
{

    if (eListe.GetRecanz () == 0);
    else if (atol (kun_anrs.kun) == (double) 0.0)
    {
        return FALSE;
    }

  
    eListe.SetPos (eListe.GetAktRow (), eListe.FirstColumn ());
    return TRUE;
}

int KALIST::DeleteLine (void)
/**
Zeile aus Liste loeschen.
**/
{
        kun_anr.kun = atol (kun_anrs.kun);
        kun_anr.wo_tag = atoi (kun_anrs.wo_tag);
        strcpy (kun_anr.uhrzeit, kun_anrs.zeit);
        if (kun_anr.kun != 0l)
        {
                Kunanr.dbdelete ();
        }
        eListe.DeleteLine ();
        return 0;
}

int KALIST::InsertLine (void)
/**
Zeile aus Liste loeschen.
**/
{
        eListe.SetPos (eListe.GetAktRow (), eListe.FirstColumn ());
        eListe.InsertLine ();
        return 0;
}

int KALIST::AppendLine (void)
/**
Zeile an Liste anfuegen.
**/
{
        eListe.AppendLine ();
        return 0;
}

int KALIST::WriteRow (void)
/**
Zeile beim Verlassen pruefen.
**/
{

    set_fkt (NULL, 9);
    SetFkt (9, leer, NULL);
    if (atol (kun_anrs.kun) == 0l)
    {
        eListe.DeleteLine ();
        return (1);
    }
    return (0);
}

void KALIST::GenNewPosi (void)
/**
Neue Positionsnummern in 10-er Schritten generieren.
**/
{
    int i;
    int row;
    int recs;
    long posi;

    row  = eListe.GetAktRow ();
    memcpy (&kun_anrtab[row], &kun_anrs, sizeof (struct KUN_ANRS));
    recs = eListe.GetRecanz ();
    posi = 10;
    for (i = 0; i < recs; i ++, posi += 10)
    {
//        sprintf (wo_toutab[i].posi, "%ld", posi);
    }
    eListe.DisplayList ();
    memcpy (&kun_anrs, &kun_anrtab[row], sizeof (struct KUN_ANRS));
}


int KALIST::PosiEnd (long posi)
/**
Positionnummer testen.
**/
{
    int row;
    int recs;
    long nextposi;

    row  = eListe.GetAktRow ();
    recs = eListe.GetRecanz ();

    if (row >= recs - 1)
    {
            return FALSE;
    }

//    nextposi = atol (wo_toutab [row + 1].posi);
    if (nextposi <= posi)
    {
        GenNewPosi ();
        return TRUE;
    }
    return FALSE;
}

int KALIST::Querykun (void)
{
       int ret;
	   QClass.SetTestKunProc (NULL);
       ret = QClass.querykun (eListe.Getmamain3 ());
       set_fkt (dokey5, 5);
       set_fkt (InsertLine, 6);
       set_fkt (DeleteLine, 7);
       set_fkt (AppendLine, 8);
       set_fkt (WriteAllPos, 12);
       SetFkt (6, einfuegen, KEY6);
       SetFkt (7, loeschen, KEY7);
       SetFkt (8, anfuegen, KEY8);
       SetFkt (11, vollbild, KEY11);
       if (ret == FALSE)
       {
           eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                                 eListe.GetAktColumn ());
           return 0;
       }
       sprintf (kun_anrtab[eListe.GetAktRow ()].kun, "%ld", kun.kun);
       eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
       eListe.ShowAktRow ();
       eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                             eListe.GetAktColumn ());
       PostMessage (eListe.Getmamain3 (), WM_KEYDOWN, VK_RETURN, 0l);
       return 0;
}

int KALIST::TestKunList (long dkun)
/**
Test, ob der Kunde schon zugeordnet ist.
**/
{
	   int zeile;
	   int recanz;

	   recanz = eListe.GetRecanz ();
	   for (zeile = 0; zeile < recanz; zeile ++)
	   {
                if (dkun == atol (kun_anrtab[zeile].kun))
				{
					return TRUE;
				}
	   }
    
       return FALSE;
}
       
int KALIST::SearchKun (void)
{
	   int zeile;
	   int recanz;

       int ret;

	   if (kunsearch)
	   {
	             QClass.SetTestKunProc (TestKunList);
	   }
       ret = QClass.querykun (eListe.Getmamain3 ());
       set_fkt (dokey5, 5);
       set_fkt (InsertLine, 6);
       set_fkt (DeleteLine, 7);
       set_fkt (AppendLine, 8);
       set_fkt (WriteAllPos, 12);
       SetFkt (6, einfuegen, KEY6);
       SetFkt (7, loeschen, KEY7);
       SetFkt (8, anfuegen, KEY8);
       SetFkt (11, vollbild, KEY11);
       if (ret == FALSE)
       {
           eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                                 eListe.GetAktColumn ());
           return 0;
       }

	   recanz = eListe.GetRecanz ();
	   for (zeile = 0; zeile < recanz; zeile ++)
	   {
                if (kun.kun == atol (kun_anrtab[zeile].kun))
				{
					break;
				}
	   }
   
       if (zeile < recanz)
	   {
                 eListe.SetVPos (zeile);
                 eListe.SetPos (zeile, eListe.FirstColumn ());
                 eListe.ShowAktRow ();
                 eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                             eListe.GetAktColumn ());
	   }
	   else
	   {
	              eListe.AppendLine ();
                  sprintf (kun_anrtab[eListe.GetAktRow ()].kun, "%ld", kun.kun);
                  eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
                  eListe.ShowAktRow ();
                  eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                             eListe.GetAktColumn ());
                  PostMessage (eListe.Getmamain3 (), WM_KEYDOWN, VK_RETURN, 0l);
	   }
       return 0;
}

       
int KALIST::QueryFil (void)
{
       int ret;

       ret = QFClass.queryfil (eListe.Getmamain3 ());
       set_fkt (dokey5, 5);
       set_fkt (InsertLine, 6);
       set_fkt (DeleteLine, 7);
       set_fkt (AppendLine, 8);
       set_fkt (WriteAllPos, 12);
       SetFkt (6, einfuegen, KEY6);
       SetFkt (7, loeschen, KEY7);
       SetFkt (8, anfuegen, KEY8);
       SetFkt (11, vollbild, KEY11);
       if (ret == FALSE)
       {
           eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                                 eListe.GetAktColumn ());
           return 0;
       }
       sprintf (kun_anrtab[eListe.GetAktRow ()].kun, "%hd", _fil.fil);
       eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
       eListe.ShowAktRow ();
       eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                             eListe.GetAktColumn ());
       PostMessage (eListe.Getmamain3 (), WM_KEYDOWN, VK_RETURN, 0l);
       return 0;
}

int KALIST::Showsysben (void)
/**
Auswahl ueber Benutzer.
**/
{
	  int cx, cy;
	  CHOISE *Choise1;
	  char buffer [80];
	  char pers_nam [9];
	  int cursor;


	  DbClass.sqlout ((char *) pers_nam, 0, 9);
	  cursor = DbClass.sqlcursor ("select pers_nam from sys_ben order by pers_nam");

	  cx = 40;
	  cy = 20;
	  Choise1 = new CHOISE (cx, cy);
      Choise1->OpenWindow (hMainInst, eListe.Getmamain3());

	  Choise1->VLines (" ", 0);
	  Choise1->InsertCaption ("Benutzer");

	  while (DbClass.sqlfetch (cursor) == 0)
	  {
  		  sprintf (buffer, "  %s  ", pers_nam);
	      Choise1->InsertRecord (buffer);
	  }
	  DbClass.sqlclose (cursor);

	  Choise1->ProcessMessages ();
	  if (syskey != KEY5)
	  {
	            Choise1->GetText (pers_nam);
	            strcpy (kun_anrtab[eListe.GetAktRow ()].pers_nam, &pers_nam[2]);
                eListe.ShowAktRow ();
                eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                             eListe.GetAktColumn ());
                PostMessage (eListe.Getmamain3 (), WM_KEYDOWN, VK_RETURN, 0l);
	  }
      Choise1->DestroyWindow ();
	  delete Choise1;
	  Choise1 = NULL;
      return 0;
}


int KALIST::SaveKun (void)
{
       AktKun = atol (kun_anrs.kun);
       set_fkt (Querykun, 9);

       SetFkt (9, auswahl, KEY9);
       set_fkt (NULL, 10);
       SetFkt (10, leer, 0);
       eListe.SetInfoProc (InfoKun);
       return 0;
}

int KALIST::Setsysben (void)
{
       set_fkt (Showsysben, 9);

       SetFkt (9, auswahl, KEY9);
       return 0;
}

int KALIST::KunInWoTou (void)
/**
Test, ob schon ein Eintrag fuer den Kunden existiert.
**/
{
	    int i;
		int anz;
		int aktrow;

		return 0;

        aktrow = eListe.GetAktRow ();
        anz = eListe.GetRecanz ();
		for (i = 0; i < anz; i ++)
		{
			if (i == aktrow) continue;
			if (atol (kun_anrs.kun) == atol (kun_anrtab[i].kun))
			{
				break;
			}
		}
		if (i == anz) return 0;

		if (AbrKz)
		{
                  if (abfragejn (mamain1, 
					  "Der Kunde wurde schon angelegt. Eintrag anzeigen ?", "J") == 0)
				  {
			             return 1;
				  }
		}
		else if (kun_test_kz == FALSE)
		{
			      return 0; 
        } 
		DeleteLine ();
		if (aktrow < i && i > 0) i --;
        eListe.SetVPos (i);
        eListe.SetPos (i , eListe.FirstColumn ());
		return 2;
}

void KALIST::fillday (int wotag)
/**
Text fuer Wochentag fuellen.
**/
{
	static char *wtage[] = {"Mon",
		                    "Mon",
					        "Die",
					        "Mit",
					        "Don",
					        "Fre",
					        "Sam",
					        "Son",
					        NULL};

	if (wotag < 1) return;
	strcpy (kun_anrs.wo_tags, wtage[wotag]);
}


int KALIST::Testday (void)
/**
Nummer des Wochentages testen.
**/
{
	  int wotag;
	  
	  wotag = atoi (kun_anrs.wo_tag);
	  if ((wotag < 1) || (wotag > 7))
	  {
		  disp_mess ("Wochentag von 0 bis 6 eingeben", 2);
          eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
          eListe.ShowAktRow ();
          return (-1);
	  }
	  fillday (wotag);
      memcpy (&kun_anrtab[eListe.GetAktRow()], &kun_anrs, sizeof (struct KUN_ANRS));
       
      eListe.ShowAktRow ();
	  return 0;
}


int KALIST::Testtime (void)
/**
Nummer des Wochentages testen.
**/
{
	  return 0;
}


int KALIST::fetchKun (void)
/**
Kunde holen.
**/
{
       int dsqlstatus;
//       long posi;
//       int i;

       SetFkt (9, leer, NULL);

	   switch (KunInWoTou ())
	   {
	         case 0 :
		           break;
			 case 1:
                   if (syskey != KEYCR &&syskey != KEYTAB 
                          && eListe.IsAppend ())
				   {
                            return (-1);
				   }
                   sprintf (kun_anrtab[eListe.GetAktRow ()].kun, "%ld", AktKun);
                   memcpy (&kun_anrs, &kun_anrtab[eListe.GetAktRow ()], sizeof (struct KUN_ANRS));
                   eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
                   eListe.ShowAktRow ();
                   return (-1);
			 case 2:
				   return 0;;
       }
	   
/*
       dsqlstatus = kun_class.lese_kun (kun_anr.mdn, kun_anr.fil, 
                                        atol (kun_anrs.kun));
*/
       dsqlstatus = kun_class.lese_kun (kun_anr.mdn, 0, 
                                        atol (kun_anrs.kun));
       if (dsqlstatus == 100)
       {
           if (syskey != KEYCR &&syskey != KEYTAB 
               && eListe.IsAppend ())
           {
               return (-1);
           }
           print_mess (2, "Kunde %.ld nicht gefunden",
                          atol (kun_anrs.kun));
           sprintf (kun_anrtab[eListe.GetAktRow ()].kun, "%ld", AktKun);
           memcpy (&kun_anrs, &kun_anrtab[eListe.GetAktRow ()], sizeof (struct KUN_ANRS));
           eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
           eListe.ShowAktRow ();
           return (-1);
       }

       sprintf (kun_anrs.kun_krz1,       "%s",     kun.kun_krz1);
       sprintf (kun_anrs.partner,        "%s",     kun.kun_krz1);
       kun_anrs.adr = kun.adr1;
	   dsqlstatus = adr_class.lese_adr (kun.adr1);
	   if (dsqlstatus == 0)
	   {
		   strcpy (kun_anrs.tel, _adr.tel);
           sprintf (kun_anrs.partner,       "%s",     _adr.partner);
	   }
	   else
	   {
		   strcpy (kun_anrs.tel, " ");
		   strcpy (kun_anrs.partner, " ");
	   }
       
       memcpy (&kun_anrtab[eListe.GetAktRow()], &kun_anrs, sizeof (struct KUN_ANRS));
       
       eListe.ShowAktRow ();
//       eListe.SetRowItem ("a", aufptab[eListe.GetAktRow ()].a);
       set_fkt (NULL, 9);
       return 0;
}

int KALIST::fetchFil (void)
/**
Filiale holen.
**/
{
       int dsqlstatus;
//       long posi;
//       int i;

       SetFkt (9, leer, NULL);
/*
       posi = atol (wo_tous.posi);
       if (posi == 0)
       {
             i = eListe.GetAktRow ();
             if (i == 0)
             {
                 posi = 1;
             }
             else
             {
                 posi = atol (wo_toutab[i - 1].posi);
                 posi ++;
             }
             if (PosiEnd (posi) == 0)
             {
                 sprintf (wo_tous.posi, "%ld", posi);
             }
       }
*/

       dsqlstatus = fil_class.lese_fil (kun_anr.mdn, 
                                   (short) atol (kun_anrs.kun));
       if (dsqlstatus == 100)
       {
           if (syskey != KEYCR &&syskey != KEYTAB 
               && eListe.IsAppend ())
           {
               return (-1);
           }
           print_mess (2, " Filiale %.ld nicht gefunden",
                          atol (kun_anrs.kun));
           sprintf (kun_anrtab[eListe.GetAktRow ()].kun, "%ld", AktKun);
           memcpy (&kun_anrs, &kun_anrtab[eListe.GetAktRow ()], sizeof (struct KUN_ANRS));
           eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
           eListe.ShowAktRow ();
           return (-1);
       }

       sprintf (kun_anrs.kun_krz1,       "%s",     _adr.adr_krz);
       
       memcpy (&kun_anrtab[eListe.GetAktRow()], &kun_anrs, sizeof (struct KUN_ANRS));
       
       eListe.ShowAktRow ();
       set_fkt (NULL, 9);
       return 0;
}

int KALIST::fetchSysben (void)
/**
Filiale holen.
**/
{
       int dsqlstatus;

       SetFkt (9, leer, NULL);
	   clipped (kun_anrs.pers_nam);
	   if (strcmp (kun_anrs.pers_nam, " ") <= 0) return 0;

       dsqlstatus = User.Lesesys_ben_pers (kun_anrs.pers_nam);
	   if (dsqlstatus == 0)
	   {
	             return 0;
	   }
	   print_mess (2, "Benutzer %s ist nicht angelegt", kun_anrs.pers_nam);
       eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
       eListe.ShowAktRow ();
       return (-1);
}

void KALIST::WritePos (int pos)
/**
Position schreiben.
**/
{
//	   char datum [12];

       memcpy (&kun_anrs, &kun_anrtab[pos], sizeof (KUN_ANRS));
       FrmtoDB (dbkun_anr);
       kun_anr.adr = kun_anrs.adr; 
//	   sysdate (datum);
//	   kun_anr.tag =  dasc_to_long (datum);
	   kun_anr.tag =  0l;
       if (kun_anr.kun ==  0l)
       {
           return;
       }
       Kunanr.dbupdate ();
}

int KALIST::TestRow (void)
/**
Zeile beim Verlassen pruefen.
**/
{
     
    if (atol (kun_anrs.kun) == 0l) return 0;
    return (0);
}

int KALIST::WriteAllPos (void)
/**
Alle Positionen schreiben.
**/
{
    int i;
    int recs;
    int row;

    row     = eListe.GetAktRow ();
//    if (eListe.TestAfterRow () == -1)
	if (TestRow () == -1)
    {
            eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                                  eListe.GetAktColumn ());
            return -1;
    }
    memcpy (&kun_anrtab[row], &kun_anrs, sizeof (struct KUN_ANRS));
    recs = eListe.GetRecanz ();
    Kunanr.dbdelete ();
//    GenNewPosi ();
    for (i = 0; i < recs; i ++)
    {
        WritePos (i);
    }
    eListe.BreakList ();
    commitwork ();
    return 0;
}


void KALIST::SaveKunanr (void)
{
    save_fkt (5);
    save_fkt (6);
    save_fkt (7);
    save_fkt (8);
    save_fkt (12);
}

void KALIST::SetKunanr (void)
{
    set_fkt (dokey5, 5);
    set_fkt (InsertLine, 6);
    set_fkt (DeleteLine, 7);
    set_fkt (AppendLine, 8);
    set_fkt (WriteAllPos, 12);
}

void KALIST::RestoreKunanr (void)
{
    restore_fkt (5);
    restore_fkt (6);
    restore_fkt (7);
    restore_fkt (8);
    restore_fkt (12);
}

int KALIST::dokey5 (void)
/**
Listenerfassung abbrechen.
**/
{
    if (abfragejn (mamain1, "Positionen speichern ?", "J"))
    {
           WriteAllPos ();
           syskey = KEY12;
    }
    syskey = KEY5;
    RestoreKunanr ();
    eListe.SetListFocus (0);
    eListe.SetFeldFocus0 (eListe.GetAktRow (), eListe.GetAktColumn ());
    eListe.BreakList ();
    rollbackwork ();
    return 1;
}


void KALIST::InitSwSaetze (void)
{
       int i;

       for (i = 0; i < MAXPOS; i ++)
       {
               SwSaetze [i] = (char *) &kun_anrtab [i];
       }
}

void KALIST::SetRecHeight (void)
/**
Anzahl Zeilen pro Listzeile ermitteln.
**/
{
       int height;
       int i;

       height = 0;
       for (i = 0; i < dataform.fieldanz; i ++)
       {
           if (dataform.mask[i].attribut & REMOVED) continue;
           if (dataform.mask[i].pos[0] > height)
           {
               height = dataform.mask[i].pos[0];
           }
       }
       height ++; 
       eListe.SetRecHeight (height);
}

int KALIST::ToMemory (int pos)
/**
SW-Satz in Ascii-Format Speichern.
**/
{

       memcpy (SwSaetze[pos], ausgabesatz, zlen);
       eListe.SetRecanz (pos + 1);
       return 0;
}


void KALIST::SetStringEnd (char *feldname, int len)
/**
Stringende Setzen.
**/
{

       return;
}


void KALIST::uebertragen (void)
/**
Eingabesatz in ASCII-Striktur uebertragen.
**/
{
	   int dsqlstatus;

       DBtoFrm (dbkun_anr); 
       kun_anrs.adr = kun_anr.adr; 
 	   fillday (kun_anr.wo_tag);
/*
       dsqlstatus = kun_class.lese_kun (kun_anr.mdn, kun_anr.fil, 
                                        atol (kun_anrs.kun));
*/
       dsqlstatus = kun_class.lese_kun (kun_anr.mdn, 0, 
                                        atol (kun_anrs.kun));
       if (dsqlstatus == 100)
       {
               return;
       }

       if (kun_anrs.adr <= 0)
       {
           kun_anrs.adr = kun.adr1;
       }
       sprintf (kun_anrs.kun_krz1,       "%s",     kun.kun_krz1);
	   dsqlstatus = adr_class.lese_adr (kun.adr1);
	   if (dsqlstatus == 0)
	   {
		   if (strcmp (kun_anr.tel, " ") <= 0)
		   {
		             strcpy (kun_anrs.tel, _adr.tel);
		   }
		   if (strcmp (kun_anr.partner, " ") <= 0)
           {
                     sprintf (kun_anrs.partner,       "%s",     _adr.partner);
           }
	   }
	   else
	   {
		   strcpy (kun_anrs.tel, " ");
           sprintf (kun_anrs.partner,       "%s",     " ");
	   }
}


void KALIST::ShowDB (void)
/**
Auftragspositionen lesen und anzeigen.
**/
{
        int i;
        HCURSOR oldcursor;
        extern short do_exit;
        int dsqlstatus;


        InitSwSaetze ();
        eListe.SetRecanz (0);
        i = eListe.GetRecanz ();
        oldcursor = SetCursor (LoadCursor (NULL, IDC_WAIT));
        eListe.SetUbForm (&ubform);
        dsqlstatus = Kunanr.dbreadfirst ("order by wo_tag, uhrzeit, kun");
        while (dsqlstatus == 0)
        {
                     uebertragen ();
                     if (ToMemory (i) != 0) break;
                     i = eListe.GetRecanz ();
                     if (i >= MAXPOS) break; 
                     dsqlstatus = Kunanr.dbread ();
        }

        SetRecHeight ();
        SetCursor (oldcursor);
        i = eListe.GetRecanz ();

        eListe.SetRecanz (i);
        SwRecs = eListe.GetRecanz ();

        eListe.SetSaetze (SwSaetze);
        eListe.SetDataForm0 (&dataform, &lineform);
        eListe.SetChAttr (ChAttr); 
        eListe.SetUbRows (ubrows);
        if (i == 0)
        {
                 eListe.AppendLine ();
                 i = eListe.GetRecanz ();
        }
        SendMessage (eListe.Getmamain2 (), WM_SIZE, NULL, NULL);
        InvalidateRect (eListe.Getmamain3 (), 0, TRUE);
}


void KALIST::ReadDB (void)
/**
Auftragspositionen lesen.
**/
{
        ausgabesatz = (unsigned char *) &kun_anrs;
        zlen = sizeof (struct KUN_ANRS);

        PageView = 0;
        eListe.SetPageView (0);
        feld_anz = dataform.fieldanz;
        Lstzlen = zlen;
        LstSatz = ausgabesatz;

        Lstbanz = feld_anz;

        eListe.SetPos (AktRow, AktColumn);
        eListe.Setbanz (feld_anz);
        eListe.Setzlen (zlen);
        eListe.Initscrollpos ();
        eListe.SetAusgabeSatz (ausgabesatz);
}

void KALIST::Enter (short mdn, short fil)
/**
Auftragsliste bearbeiten.
**/

{
       static int initenter = 0;

	   GetCfgValues ();
       beginwork ();
       kun_anr.mdn = mdn;
       kun_anr.fil = fil;

       kun.mdn = mdn;
       _fil.mdn = mdn;

       dataform.mask[2].after  = fetchKun; 

       set_fkt (dokey5, 5);
       set_fkt (InsertLine, 6);
       set_fkt (DeleteLine, 7);
       set_fkt (AppendLine, 8);
       set_fkt (WriteAllPos, 12);
       set_fkt (Schirm, 11);

       SetFkt (6, einfuegen, KEY6);
       SetFkt (7, loeschen, KEY7);
       SetFkt (8, anfuegen, KEY8);
       SetFkt (11, vollbild, KEY11);
       if (initenter == 0)
       {
                 eListe.SetInfoProc (InfoProc);
                 eListe.SetTestAppend (TestAppend);
 //              sprintf (InfoCaption, "Auftrag %ld", auf);
                 mamain1 = CreateMainWindow ();
                 eListe.InitListWindow (mamain1);
                 ReadDB ();
                 initenter = 1;
       }

       eListe.SetListFocus (ListFocus);
	   eListe.SetRowHeight (RowHeight);
       eListe.Initscrollpos ();
       AktRow = 0;
       AktColumn = 0;
       scrollpos = 0;
       SetAktivWindow (eListe.Getmamain2 ());
       ShowDB ();

//       eListe.SetRowItem ("a", aufptab[0].a);
       
       ListAktiv = 1;
       eListe.ProcessMessages ();

       InitMax ();
       InitMin ();
       MoveMamain1 ();

       ListAktiv = 0;
       SetFkt (6, leer, NULL);
       SetFkt (7, leer, NULL);
       SetFkt (8, leer, NULL);
       SetFkt (9, leer, NULL);
       SetFkt (10, leer, NULL);
       SetFkt (11, leer, NULL);
       set_fkt (NULL, 6);
       set_fkt (NULL, 7);
       set_fkt (NULL, 8);
       set_fkt (NULL, 9);
       set_fkt (NULL, 10);
       eListe.DestroyListWindow ();
       DestroyMainWindow ();
       initenter = 0;
       return;
}

void KALIST::Work ()
/**
Liste bearbeiten.
**/

{

       beginwork ();

       set_fkt (dokey5, 5);
       set_fkt (InsertLine, 6);
       set_fkt (DeleteLine, 7);
       set_fkt (AppendLine, 8);
       set_fkt (WriteAllPos, 12);
       SetFkt (6, einfuegen, KEY6);
       SetFkt (7, loeschen, KEY7);
       SetFkt (8, anfuegen, KEY8);

       eListe.SetDataForm0 (&dataform, &lineform);
       eListe.SetChAttr (ChAttr); 
       eListe.SetUbRows (ubrows);
       eListe.SetListFocus (ListFocus);
       eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                             eListe.GetAktColumn ());
       AktRow = 0;
       AktColumn = 0;
       scrollpos = 0;

       SetAktivWindow (eListe.Getmamain2 ());

       ListAktiv = 1;
       eListe.ProcessMessages ();

       commitwork ();
       if (syskey == KEY5)
       {
                  eListe.Initscrollpos ();
                  ShowDB ();
       }
       ListAktiv = 0;
       SetFkt (6, leer, NULL);
       SetFkt (7, leer, NULL);
       SetFkt (8, leer, NULL);
       SetFkt (11, leer, NULL);
       set_fkt (NULL, 11);
       return;
}


void KALIST::DestroyMainWindow (void)
/**
Hauptfenster fuer Liste loeschen.
**/
{
        if (mamain1 == NULL) return;
        DestroyWindow (mamain1);
        mamain1 = NULL;
}


HWND KALIST::CreateMainWindow (void)
/**
Hauptfenster fuer Liste erzeugen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;

        if (hMainWindow == NULL) return NULL;    
           
        if (mamain1) return mamain1;

        eListe.GetTextMetric (&tm);

        GetClientRect (hMainWindow, &rect);
        GetWindowRect (hMainWindow, &wrect);


        y = (wrect.bottom - 20 * tm.tmHeight);
        x = wrect.left + 2;
        cy = wrect.bottom - y - 2;
        cx = rect.right;

        mamain1 = CreateWindow ("ListMain",
                                    InfoCaption,
                                    WS_THICKFRAME | 
                                    WS_POPUP,
                                    x, y,
                                    cx, cy,
                                    hMainWindow,
                                    NULL,
                                    hMainInst,
                                    NULL);
        ShowWindow (mamain1, SW_SHOWNORMAL);
        UpdateWindow (mamain1);
        return mamain1;
}

static int IsMax = 0;
static int IsMin = 0;

void KALIST::SetMax0 (int val)
{
    IsMax = val;
    if (IsMax)
    {
             SetFkt (11, fenster, KEY11);
    }
    else
    {
             SetFkt (11, vollbild, KEY11);
    }
}

void KALIST::SetMin0 (int val)
{
    IsMin = val;
}


int KALIST::Schirm (void)
{
         if (IsMin) return 0;
         
         if (IsMax)
         {
             SendMessage (mamain1, WM_SYSCOMMAND, SC_RESTORE, 0l);
             IsMax = 0;
         }
         else
         {
             SendMessage (mamain1, WM_SYSCOMMAND, SC_MAXIMIZE, 0l);
             IsMax = 1;
         }
         return 0;
}


void KALIST::MoveMamain1 ()
{
        RECT rect;
        RECT wrect;
        int x, y, cx, cy;
        TEXTMETRIC tm;

        if (mamainmin) return;
        eListe.GetTextMetric (&tm);
        GetClientRect (hMainWindow, &rect);
        GetWindowRect (hMainWindow, &wrect);
        if (mamainmax)
        {
                 x = wrect.left + 2;
                 y = wrect.top + 2;
                 cy = wrect.bottom - y - 2;
                 cx = rect.right;
        }
        else
        {
                 y = (wrect.bottom - 20 * tm.tmHeight);
                 x = wrect.left + 2;
                 cy = wrect.bottom - y - 2;
                 cx = rect.right;
        }
        MoveWindow (mamain1, x,y, cx, cy, TRUE);
}


void KALIST::MaximizeMamain1 ()
{
        RECT rect;
        RECT wrect;
        int x, y, cx, cy;

        GetClientRect (hMainWindow, &rect);
        GetWindowRect (hMainWindow, &wrect);
        x = wrect.left + 2;
        y = wrect.top + 2;
        cy = wrect.bottom - y - 2;
        cx = rect.right;
        MoveWindow (mamain1, x,y, cx, cy, TRUE);
}

HWND KALIST::GetMamain1 (void)
{
       return (mamain1);
}

void KALIST::SethwndTB (HWND hwndTB)
{
         this->hwndTB = hwndTB; 
         eListe.SethwndTB (hwndTB);
}

void KALIST::SetTextMetric (TEXTMETRIC *tm)
{
         eListe.SetTextMetric (tm);
}


void KALIST::SetLineRow (int LineRow)
{
         eListe.SetLineRow (0);
}

void KALIST::SetListLines (int i)
{ 
         eListe.SetListLines (i);
}

void KALIST::OnPaint (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
        eListe.OnPaint (hWnd, msg, wParam, lParam);
}


void KALIST::MoveListWindow (void)
{
        eListe.MoveListWindow ();
}


void KALIST::BreakList (void)
{
        eListe.BreakList ();
}


void KALIST::OnHScroll (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
        eListe.OnHScroll (hWnd, msg,wParam, lParam);
}

void KALIST::OnVScroll (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
        eListe.OnVScroll (hWnd, msg,wParam, lParam);
}


void KALIST::FunkKeys (WPARAM wParam, LPARAM lParam)
{
                    eListe.FunkKeys (wParam, lParam);
}


int KALIST::GetRecanz (void)
{
          return eListe.GetRecanz ();
}


void KALIST::SwitchPage0 (int rows)
{
           eListe.SwitchPage0 (rows);
}


HWND KALIST::Getmamain2 (void)
{
           return eListe.Getmamain2 ();
}


HWND KALIST::Getmamain3 (void)
{
           return eListe.Getmamain3 ();
}


void KALIST::ChoiseFont (mfont *lfont)
{
           eListe.ChoiseFont (lfont);
}

void KALIST::SetFont (mfont *lfont)
{
           eListe.SetFont (lfont);
}

void KALIST::SetListFont (mfont *lfont)
{
           eListe.SetListFont (lfont);
}

void KALIST::FindString (void)
{
           eListe.FindString ();
}


void KALIST::SetLines (int Lines)
{
           eListe.SetLines (Lines);
}


int KALIST::GetAktRow (void)
{
                 return eListe.GetAktRow ();
}

int KALIST::GetAktRowS (void)
{
                 return eListe.GetAktRowS ();
}

void KALIST::SetColors (COLORREF Color, COLORREF BkColor)
{
                 eListe.SetColors (Color, BkColor); 
}

void KALIST::SetListFocus (void)
{
                 eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                                       eListe.GetAktColumn ()); 
}

