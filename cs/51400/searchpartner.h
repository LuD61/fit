#ifndef _SEARCHPARTNER_DEF
#define _SEARCHPARTNER_DEF
#include "wmaskc.h"
#include "cmask.h"
#include "mo_chqex.h"
#include "dbclass.h"

class SEARCHPARTNER
{
       private :
           static HINSTANCE hMainInst;
           static HWND hMainWindow;
           static DB_CLASS DbClass; 
           static HWND awin;
           static int idx;
           static long anz;
           static CHQEX *Query;

           int SearchPos;
           int OKPos;
           int CAPos;
           char Key [512];
           static short mdn;
           static short fil;
           static long kun;

        public :
           SEARCHPARTNER ()
           {
                  SearchPos = 8;
                  OKPos = 9;
                  CAPos = 10;
           }

           ~SEARCHPARTNER ()
           {
                   if (Query)
                   {
                       delete Query;
                       Query = NULL;
                   }
           }

/*
           SWG *GetLine (void)
           {
               if (idx == -1) return NULL;
               return &sline;
           }
*/

           void Setawin (HWND awin)
           {
               this->awin = awin;
           }

           static int SearchLst (char *);
           static int ReadLst (char *);
           BOOL GetKey (char *);
           BOOL GetKeyRow (char *);
           void SetKeys (short, short, long);
           void SetParams (HINSTANCE, HWND);
           void Search (void);
};  
#endif