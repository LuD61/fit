#ifndef _PARTNER_DEF
#define _PARTNER_DEF

#include "windows.h"
#include "mo_draw.h"
#include "wmask.h"
#include "dbclass.h"

struct PARTNER {
   short     mdn;
   short     fil;
   long      kun;
   long      adr;
};
extern struct PARTNER partner, partner_null;

#line 10 "partner.rh"

class PARTNER_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               PARTNER_CLASS () : DB_CLASS ()
               {
               }
               ~PARTNER_CLASS ()
               {
                   dbclose ();
               } 
               int dbreadfirst (void);
};
#endif
