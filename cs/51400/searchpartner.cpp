#include <windows.h>
#include "searchpartner.h"
#include "strfkt.h"
#include "mo_wmess.h"
#include "lbox.h"


int SEARCHPARTNER::idx;
long SEARCHPARTNER::anz;
CHQEX *SEARCHPARTNER::Query = NULL;
DB_CLASS SEARCHPARTNER::DbClass; 
HINSTANCE SEARCHPARTNER::hMainInst;
HWND SEARCHPARTNER::hMainWindow;
HWND SEARCHPARTNER::awin;
short SEARCHPARTNER::mdn = 1;
short SEARCHPARTNER::fil = 0;
long SEARCHPARTNER::kun = 0;


int SEARCHPARTNER::SearchLst (char *sebuff)
/**
Nach Kurzname in Liste suchen.
**/
{
	   Query->SearchList ();
	   return 0;
}

int SEARCHPARTNER::ReadLst (char *name)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int cursor;
	  int i;
      WMESS Wmess;
      long adr;
      char adr_krz [17];
      char ort1 [37];
      char tel [21];
      char email [21];


	  anz = 0;

      sprintf (buffer, "select partner.adr, adr.adr_krz, adr.ort1,adr.tel, adr.email "
	 			       "from partner, adr "
                       "where partner.mdn = ? "
                       "and partner.fil = ? "
                       "and partner.kun = ? "
                       "and adr.adr = partner.adr");

      DbClass.sqlin ((short * ) &mdn, 1, 0);
      DbClass.sqlin ((short * ) &fil, 1, 0);
      DbClass.sqlin ((long * )  &kun, 2, 0);
      DbClass.sqlout ((long * )  &adr, 2, 0);
      DbClass.sqlout ((char *) adr_krz, 0, 17);
      DbClass.sqlout ((char *) ort1, 0, 37);
      DbClass.sqlout ((char *) tel, 0, 21);
      DbClass.sqlout ((char *) email, 0, 21);
      cursor = DbClass.sqlcursor (buffer);
      if (cursor < 0) 
      {
          return -1;
      }
  	  i = 0;
/*
      Wmess.Message (hMainInst, hMainWindow, " \nBitte warten.....\n"
  	                                         "Die Daten werden selektiert.\n ");  
*/
	  while (DbClass.sqlfetch (cursor) == 0)
      {
 	      sprintf (buffer, " %8d   |%-16s|  |%36s|   |%20s|  |%20s|", 
                   adr, adr_krz, ort1, tel, email);
	      Query->InsertRecord (buffer);
		  i ++;
      }
      anz = i;
	  DbClass.sqlclose (cursor);
// 	  Wmess.Destroy ();
	  EnableWindow (awin, FALSE);
	  return 0;
}

void SEARCHPARTNER::SetParams (HINSTANCE hMainInst, HWND hMainWindow)
{
      this->hMainInst   = hMainInst;
      this->hMainWindow = hMainWindow;
      awin = hMainWindow;
}

BOOL SEARCHPARTNER::GetKey (char * key)
{
      int anz;
     
      if (idx == -1) return FALSE;
      anz = wsplit (Key, "|");
      if (anz < 1) return FALSE;
      strcpy (key, wort[0]);
      return TRUE;
}

BOOL SEARCHPARTNER::GetKeyRow (char * key)
{
     
      if (idx == -1) return FALSE;
      strcpy (key, Key);

      return TRUE;
}

void SEARCHPARTNER::SetKeys (short mdn, short fil, long kun)
{
      this->mdn = mdn;
      this.fil = fil;
      this.kun  = kun;
}


void SEARCHPARTNER::Search (void)
{
  	  int cx, cy;
	  char buffer [256];

	  idx = -1;
      cx = 48;
      cy = 25;
      Settchar ('|');
      EnableWindow (hMainWindow, FALSE);
      Query = new CHQEX (cx, cy);
      Query->EnableSort (TRUE);
      Query->OpenWindow (hMainInst, hMainWindow);
      sprintf (buffer, "%s %s %s %s %s",
                          "%d",
                          "%s",
                          "%s",
                          "%s",
                          "%s");
      Query->RowAttr (buffer);
	  sprintf (buffer, " %10s  %17s  %37s  %21s  %21s", "1", "1", "1", "1", "1"); 
	  Query->VLines (buffer, 3);
      sprintf (buffer, "1;8 13;16 32;37 71;20 93;20");
      Query->RowPos (buffer);
	  EnableWindow (awin, FALSE);

	  sprintf (buffer, " %10s  %-17s  %-37s  %-21s  %-21s", "Adresse  ", 
                                                           "      Name",
                                                           "                Ort",
                                                           "      Telefon",
                                                           "       E-Mail"); 
	  Query->InsertCaption (buffer);

	  Query->SetFillDb (ReadLst);
	  Query->SetSearchLst (SearchLst);
      ReadLst ("");
      Query->SetSortRow (0, TRUE);
	  Query->ProcessMessages ();
      EnableWindow (hMainWindow, TRUE);
      if (syskey != KEY5)
      {
              idx = Query->GetSel ();
              if (idx != -1)
              {
                     Query->GetText (Key);
              }
      }
	  EnableWindow (awin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (awin);
}

