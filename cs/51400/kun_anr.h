#ifndef _KUN_ANR_DEF
#define _KUN_ANR_DEF

#include "dbclass.h"

struct KUN_ANR {
   short     mdn;
   short     fil;
   short     wo_tag;
   char      uhrzeit[6];
   long      kun;
   long      tag;
   char      pers_nam[9];
   char      tel[21];
   long      adr;
   char      partner[37];
};
extern struct KUN_ANR kun_anr, kun_anr_null;

#line 7 "kun_anr.rh"

class KUN_ANR_CLASS : public DB_CLASS 
{
       private :
               void prepare (char *);
       public :
               KUN_ANR_CLASS () : DB_CLASS ()
               {
               }
               int dbreadfirst (char *);
/*
               int dbinsert (void);
               int dbupdate (void);
               int dbclose (void);
*/
};
#endif
