#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include "itemc.h"
#include "wmaskc.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "mdn.h"
#include "fil.h"
#include "mo_meld.h"
#include "mo_qfil.h"

FIL_CLASS fil_class;

static int testquery (void)
/**
Abfrage in Query-Eingabe.
**/
{
        switch (syskey)
        {
                case KEY5 :
                       break_enter ();
                       return 1;
                case KEY11 :
                case KEY12 :
                       syskey = KEY12;
                       break_enter ();
                       return 1;
        }
        return 0;
}


int QueryFil::queryfil (HWND hWnd)
/**
Query ueber Textnummer.
**/
{

        HANDLE hMainInst;

        static char lfil [41];
        static char ladr_krz [41];
        static char lort1 [41];

        static ITEM ifilval ("fil", 
                              lfil, 
                              "Filial-Nr.....:", 
                              0);

        static ITEM iadr_krzval ("adr_krz", 
                                   ladr_krz, 
                                   "Filial-Name...:", 
                                   0);

        static ITEM ifil_ort1val   ("ort1", 
                                   lort1, 
                                   "Ort...........:", 
                                   0);


        static ITEM iOK ("", "     OK     ", "", 0);
        static ITEM iCA ("", "  Abbrechen ", "", 0);

        static field _qtxtform[] = {
           &ifilval,      40, 0, 1,1, 0, "", NORMAL, 0, testquery,0,
           &iadr_krzval, 40, 0, 2,1, 0, "", NORMAL, 0, testquery,0,
           &ifil_ort1val, 40, 0, 3,1, 0, "", NORMAL, 0, testquery,0,
           &iOK,          15, 0, 5,15, 0, "", BUTTON, 0, 0,KEY12,
           &iCA,          15, 0, 5,32, 0, "", BUTTON, 0, 0,KEY5,
		};

        static form qtxtform = {5, 0, 0, _qtxtform, 
			                    0, 0, 0, 0, NULL};
        
        static char *qnamen[] = {"fil.fil", "adr.adr_krz", 
                                 "adr.ort1",  NULL};

        HWND query;
		int savefield;
		form *savecurrent;

        hMainInst = (HANDLE) GetWindowLong (hWnd, GWL_HINSTANCE);	
        save_fkt (5);
		save_fkt (6);
		save_fkt (7);
		save_fkt (11);
		save_fkt (12);

        set_fkt (NULL, 6);
        SetFkt (6, leer, NULL);
        set_fkt (NULL, 7);
        SetFkt (7, leer, NULL);
        set_fkt (NULL, 9);
        SetFkt (9, leer, NULL);
 
		savefield = currentfield;
		savecurrent = current_form;
        SetAktivWindow (hWnd);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_CAPTION);
        query = OpenWindowChC (9, 60, 11, 10, hMainInst,
                               "Suchkriterien f�r Filialen");
        syskey = 0;
        EnableWindows (hWnd, FALSE); 
        enter_form (query, &qtxtform, 0, 0);

        EnableWindows (hWnd, TRUE); 
        CloseControls (&qtxtform);
        DestroyWindow (query);
		SetAktivWindow (hWnd);
		SetActiveWindow (hWnd);
        if (syskey == KEY12 || syskey == KEYCR)
        {
                  if (fil_class.PrepareQuery (&qtxtform, qnamen) == 0)
                  {
                              fil_class.ShowBuQuery (hWnd, 0);
                  }
                  else
                  {
                      syskey = KEY5;
                  }
        }

		currentfield = savefield;
		restore_fkt (5);
        restore_fkt (11);
        restore_fkt (12);
        restore_fkt (6);
        restore_fkt (7);
        if (syskey == KEY5) return FALSE; 
        return TRUE;
}

