
#ifndef DOS
#ident  "@(#) biz_deb.c		VLUGLOTSA	28.01.2000	by FF"
#endif
/***
--------------------------------------------------------------------------------
-
-       SE.TEC
-       Unternehmensberatung fuer systemische Entwicklung GmbH
-       Zeppelinstr 4
-
-       72189 V�hringen
-
-       Tel.: 07454/9667-0     Fax: 07454/9667-200
-       E-Mail info@setec-gmbh.de
-
--------------------------------------------------------------------------------

 Fuer dieses Programm behalten wir uns alle Rechte, auch fuer den Fall der
 Patenterteilung und der Eintragung eines anderen gewerblichen Schutz-
 rechtes vor. Missbraeuchliche Verwendung, wie insbesondere Vervielfael-
 tigung und Weitergabe an Dritte ist nicht gestattet; sie kann zivil- und
 strafrechtlich geahndet werden.

--------------------------------------------------------------------------------
-
-       Modulname               :       biz_deb.c
-
-       Autor                   :       F.Folmer
-       Erstellungsdatum        :       17.06.97
-       Modifikationsdatum      :       TT.MM.JJ
-
-       Projekt                 :       BIWAK
-       Version                 :       1.00
-       Laendervariante         :       BRD
-
-       Rechner                 :
-       Betriebssystem          :       UNIX/OS4680/DOS
-       Sprache                 :       C
-
-       Modulbeschreibung       :	Debug-Funktionen fuer BIWAK
-
--------------------------------------------------------------------------------
-       Aenderungsjournal       :
-
-       lfd.    Version Datum           Name       Beschreibung
-       ------------------------------------------------------------------------
-       1       2.00    17.06.97	F.Folmer  Grundmodul
--------------------------------------------------------------------------------
***/

#ifdef OS4680
#define SCHAPER 1
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#ifdef OS4680
#include "unixio.h"
//#include "flexif.h"
#else
#include <fcntl.h>
#endif

//#include "biz_allg.h"
#include <time.h>

#ifdef _WINDOWS
#include <windows.h>
#include <share.h>
#include <errno.h>
#endif

#define        WARTEN           "Warten auf Kommando"
#define        AUSFALL          " nach CC Ausfall"

extern short testen;

//short  console = 0;      /* 05.12.01 Dienste FF */
extern char tmppath[];
extern char etc[];
extern char char_zwi[];
extern short testlog;
extern short testen;
extern FILE * flog;
extern int   laenge_log;
extern char  logbuch[];

#ifdef OS4680
extern long   tag_zeit;
extern long   tag_alt;
/* Systemnachrichten */
extern long   ret;
#endif

//#ifdef _WINDOWS
void WriteDTlog (char *Text);
char FileName[64] = {"protokol.dat"};    /* Protokoll-Datei */
//#endif

/***************************************************************************/
/* Procedure :            DEBUG                                            */
/*-------------------------------------------------------------------------*/
/* Funktion :             debug trace output                               */
/*-------------------------------------------------------------------------*/
/* Kurzbeschreibung :                                                      */
/*                                                                         */
/*                                                                         */
/*                                                                         */
/*-------------------------------------------------------------------------*/
/* Aufruf :               DEBUG (level,format, ...)                        */
/* Funktionswert :        ----                                             */
/* Eingabeparameter :     char *format: format string to control           */
/*                                      data format/number of arguments    */
/*                                                                         */
/* Ausgabeparameter :                                                      */
/*                                                                         */
/*-------------------------------------------------------------------------*/

void DEBUG (int tst, char *format, ...)
{
va_list args;
va_start(args, format);

//printf ("in DEBUG\n");
	
if(testen == 0 && testlog == 0)
  return;

//printf ("in DEBUG testen=%d, format=%s\n",testen, format);

if(testen)
       {
       vfprintf(stderr,format, args);
       fflush(stderr);
       }

if(testlog != 0)
       {
       vfprintf(flog,format, args);
       fflush(flog);
       }

va_end(args);

return;
}

#ifdef STEUER_PGM
/******************** bildschirm_meldung **************************/
void bildschirm_meldung (ausgabe, zeit)
char * ausgabe;
long   zeit;
{
#ifdef OS4680
#ifndef ART_ABGL
       if(testen == 0 && testlog == 0)
	      {
              ADX_CSERVE (&ret, 26, ausgabe, strlen (ausgabe));
	      }
       else
#endif
	      {
              DEBUG(1,"MELD %s <%ld sec>\n",ausgabe,zeit);
	      }
       
	   WriteDTlog (ausgabe);

       if(zeit != (long) 0)
	      sleep((long) zeit);
#else
#ifdef _WINDOWS
       if (console == 1 && testen == 1)   /* 05.12.01 FF Dienste */
           printf("%s\n",ausgabe);
       WriteDTlog (ausgabe);
#else
       WriteDTlog (ausgabe);
       DEBUG(1,"MELD %s <%ld sec>\n",ausgabe,zeit);
#endif
       if(zeit != (long) 0)
	   {
#ifdef _WINDOWS
		  Sleep ((long)(zeit * 1000));
#else
	      sleep ((unsigned) zeit);
#endif
	   }

#endif
return;
} /* end of bildschirm_meldung() */
#endif /* STEUER_PGM */


/***
--------------------------------------------------------------------------------
##D8A
	Procedure	: dat_zeit_log()
	In		: char * char_zwi
			  short  parameter   0 Ausgabe ohne Trennzeichen
			                     1 Ausgabe mit Trennzeichen
	Out		: char * char_zwi
	Funktion	: Prozedur liefert Datum und Uhr als String.

##D8E
--------------------------------------------------------------------------------
***/
/* neue version  von swclog.c */
long   uxtime=0;
struct tm * strtime;

char * dat_zeit_log(char * puff,short parameter)
//char * puff;
//short  parameter;
{

#ifdef OS4680
double uxtime;
   time((double *) &uxtime);
   strtime = localtime((const double *) &uxtime); /* aktuelle Zeit holen */
#else
time_t uxtime;
   time((time_t *) &uxtime);
   strtime = localtime((time_t *) &uxtime); /* aktuelle Zeit holen */
#endif

/* Jahr 2000 FF */
if(strtime->tm_year >= 100)
    strtime->tm_year = strtime->tm_year % 100;
/* Jahr 2000 FF */

#ifdef VMS
   if(parameter == 1)
      {
      sprintf(puff,"%2.2u.%2.2u.%2.2u %2.2u:%2.2u:%2.2u ",
      strtime->tm_mday,strtime->tm_mon+1,strtime->tm_year,
      strtime->tm_hour,strtime->tm_min,strtime->tm_sec);
      }
   else
      {
      sprintf(puff,"%2.2u%2.2u%2.2u%2.2u%2.2u%2.2u",
      strtime->tm_mday,strtime->tm_mon+1,strtime->tm_year,
      strtime->tm_hour,strtime->tm_min,strtime->tm_sec);
      }
#else
   if(parameter == 1)
      {
      sprintf(puff,"%2.2hu.%2.2hu.%2.2hu %2.2hu:%2.2hu:%2.2hu ",
      strtime->tm_mday,strtime->tm_mon+1,strtime->tm_year,
      strtime->tm_hour,strtime->tm_min,strtime->tm_sec);
      }
   else
      {
      sprintf(puff,"%2.2hu%2.2hu%2.2hu%2.2hu%2.2hu%2.2hu",
      strtime->tm_mday,strtime->tm_mon+1,strtime->tm_year,
      strtime->tm_hour,strtime->tm_min,strtime->tm_sec);
      }
#endif

return(puff);
} /* end of dat_zeit_log() */

/***
--------------------------------------------------------------------------------
##D8A
	Procedure	: fehler_meldung
	In		:
			  long nr,         0  nur Text 1 Ausgabe
			  long error,      Error
			  char *text,      Text
	Out		: void
	Funktion	: Fehler-Anzeige

##D8E
--------------------------------------------------------------------------------
***/
void fehler_meldung(long nr,long err,char * text)
{
int i=0,len;
int kein_cc;
int ist=1,soll=laenge_log;      /* Satz_nr in logbuch-Datei */
char tmpbuf[81];
char ctmp[81];
char datzeit[32];
FILE * fp;
short neu = 0;

dat_zeit_log(datzeit,(short)1);
len = strlen(datzeit);
datzeit[len - 1] = '|';

/*  Ro 23.04.1998  Test, ob das Netz noch aktiv ist    */
kein_cc = 0;
fp=fopen(logbuch,"r");
if (fp == NULL)
{
    fp=fopen(logbuch,"w+");
    if (fp == NULL)
    {
           kein_cc = 1;
           sprintf(ctmp,"%s%s",datzeit,"Kein CC : Warteschleife");
           printf("%s\n",ctmp);
    }
    else
    {
           ist = 1;
           sprintf(tmpbuf,"%4d",ist);
           fwrite(tmpbuf,(int)strlen(tmpbuf),1,fp);
           //chmod(logbuch,(int)00666);
    }
    i = 1;
    while (fp == NULL)
    {
#ifdef OS4680
           sleep ((long)10);
#else
#ifdef _WINDOWS
		   Sleep ((long)10000);
#else
           sleep ((unsigned)10);
#endif
#endif
           dat_zeit_log(datzeit,(short)1);
           sprintf(ctmp,"%s%s %d0",datzeit,"Kein CC : Warteschleife",i++);
           //bildschirm_meldung(ctmp,(long)0);
           fp=fopen(logbuch,"r");
           if (fp == NULL)
           {
                    fp=fopen(logbuch,"w+");
                    if (fp == NULL)
                       {
                       unlink (logbuch); /* nach Absprache mit F.Ueregi */
                       neu = 1;
                       }
                    else
                       {
                       ist = 1;
                       sprintf(tmpbuf,"%4d",ist);
                       fwrite(tmpbuf,(int)strlen(tmpbuf),1,fp);
#ifdef OS4680
                       /* 28.01.2000 FF */
                       sprintf(ctmp,"adxcsu0l 3 4 %s",logbuch);
                       system(ctmp);
                       /* 28.01.2000 FF */
#endif
                       //chmod(logbuch,(int)00666);
                       }
           }
    }
    if (kein_cc)
    {
#ifdef BIZMAN
           erase_alle_cursor(1);
#endif
           sprintf(ctmp,"%d sek %s%s",((i-1)*10),WARTEN,AUSFALL);
           if (neu == 1)
                strcat (ctmp,": LOG neu");
           //bildschirm_meldung(ctmp,(long)3);
    }
}
/* Ro 23.04.1998                                       */

fclose (fp);

fp=fopen(logbuch,"r");
if(fp == NULL)
    {
    fp=fopen(logbuch,"w+");
    ist = 1;
    sprintf(tmpbuf,"%4d",ist);
    i = fwrite(tmpbuf,(int)strlen(tmpbuf),1,fp);
    fclose(fp);
    /* 28.01.2000 FF */
#ifdef OS4680
    sprintf(ctmp,"adxcsu0l 3 4 %s",logbuch);
    system(ctmp);
#endif
    /* 28.01.2000 FF */
    //chmod(logbuch,(int)00666);
    }
else
    {
    fgets(tmpbuf,128,fp);
    ist = atoi(tmpbuf);
    if(ist > soll)
        ist = 1;
    fclose(fp);
    }
fp=fopen(logbuch,"r+");
fseek(fp,(long)0,SEEK_SET);
if(ist == soll)
   sprintf(tmpbuf,"%4d\n",(soll + 1));
else
   sprintf(tmpbuf,"%4d\n",(ist + 1 + kein_cc));
i = fwrite(tmpbuf,(int)strlen(tmpbuf),1,fp);
#ifndef UNIX
i = fseek(fp,(long)((long)(ist - 1) * (long)81 + (long)6),SEEK_SET);
#else
i = fseek(fp,(long)((long)(ist - 1) * (long)80 + (long)5),SEEK_SET);
#endif

memset(tmpbuf,0x20,79);
strcpy(tmpbuf,datzeit);
len = strlen(tmpbuf);
tmpbuf[len - 1] = '|';
if(nr == 0)
     sprintf(&tmpbuf[len]," %s",text);
else
     sprintf(&tmpbuf[len]," %03ld | %s",err,text);

len = strlen(tmpbuf);
tmpbuf[len] = 0x20;
tmpbuf[79]=0x0A; /* LF */
tmpbuf[80]=0x00;


i = fwrite(tmpbuf,(int)strlen(tmpbuf),1,fp);
if(kein_cc == 1)
   {
   memset(tmpbuf,0x20,79);
   dat_zeit_log(tmpbuf,(short)1);
   len = strlen(tmpbuf);
   tmpbuf[len - 1] = '|';
   sprintf(&tmpbuf[len]," %s",ctmp);
   len = strlen(tmpbuf);
   tmpbuf[len] = 0x20;
   tmpbuf[79]=0x0A; /* LF */
   tmpbuf[80]=0x00;
   i = fwrite(tmpbuf,(int)strlen(tmpbuf),1,fp);
   }
//DEBUG(1,"%s",tmpbuf);
kein_cc = 0;
if(ist < soll)
   {
   memset(tmpbuf,'*',79);
   i=fwrite(tmpbuf,(int)strlen(tmpbuf),1,fp);
   }
fclose(fp);
}/* end of fehler_meldung() */

#ifdef STEUER_PGM

#ifdef OS4680
long zeit_os4680()
{
struct tm * strtime;
long uxtime_zeit=0;

   time((double *) &uxtime_zeit);
   strtime = localtime((const double *) &uxtime_zeit); /* aktuelle Zeit holen */

   /*****
   DEBUG(1,"tag_alt=%ld\n",tag_alt);
   DEBUG(1,"tag_zeit=%ld\n",tag_zeit);
   DEBUG(1,"strtime->tm_mday=%ld\n",(long)strtime->tm_mday);
   DEBUG(1,"strtime->tm_hour=%ld\n",(long)strtime->tm_hour);
   DEBUG(1,"strtime->tm_min=%ld\n",(long)strtime->tm_min);
   DEBUG(1,"strtime->tm_sec=%ld\n",(long)strtime->tm_sec);
   *****/

   if((long)tag_alt != (long) strtime->tm_mday)
	  {
	  tag_alt = (long) strtime->tm_mday;
	  tag_zeit++;
	  }
   uxtime_zeit = (long)((long)tag_zeit * (long)86400 +
		   (long)strtime->tm_hour * (long)3600 +
		   (long)strtime->tm_min * (long)60 +
		   (long)strtime->tm_sec);
   /****
   DEBUG(1,"uxtime_zeit=%ld\n",uxtime_zeit);
   DEBUG(1,"2 tag_alt=%ld\n",tag_alt);
   DEBUG(1,"2 tag_zeit=%ld\n",tag_zeit);
   *****/

return (uxtime_zeit);
} /* end of zeit_os4680() */
#endif

/******************************************************/
#ifdef OS4680
int CopyFile (char *quelle, char *ziel)
/**
Datei kopieren.
**/
{
           FILE *fz;
           FILE *fq;
           int bytes;
           char buffer [0x1000];

           fq = fopen (quelle, "rb");
           if (fq == (FILE *) 0)
           {
                       return (-1);
           }

           fz = fopen (ziel, "wb");
           if (fz == (FILE *) 0)
           {
                       fclose (fq);
                       return (-1);
           }

           while  (bytes = fread (buffer, 1, 0x1000, fq))
           {
                        fwrite (buffer, 1, bytes, fz);
           }
           fclose (fq);
           fclose (fz);
           return (0);
}
#endif /* OS4680 */

/***************************************************************/

//#ifdef _WINDOWS
void WriteFirstLine (char *FileText)
/**
Satz in erste Zeile einer Datei schreiben.
**/
{
	   FILE *in;
       FILE *out; 
	   int pid;
	   int z;
	   char fname [128];
	   char buffer [256];

#ifdef OS4680
	   pid = 777;
       sprintf (fname, "%s%cto_write.%d", tmppath, SLESCH, pid);
#else
	   pid = getpid ();
       sprintf (fname, "touchwrite.%d", pid);
#endif

#ifdef OS4680
	   CopyFile (FileName, fname);  
#else
	   CopyFile (FileName, fname, FALSE);
#endif

#ifdef _WINDOWS
       out = _fsopen (FileName, "w", _SH_DENYNO);
#else
       out = fopen (FileName, "w");
#endif
	   if (out == NULL) return;
	   fprintf (out, "%s\n", FileText);
	   in = fopen (fname, "r");
	   if (in == NULL) 
	   {
		   fclose (out);
		   return;
	   }
	   z = 1;
	   while (fgets (buffer, 255, in))
	   {
		   fputs (buffer, out);
		   if (z >= MAXLINES) break;
		   z ++;
	   }
	   fclose (in);
	   fclose (out);
	   unlink (fname);
}

void WriteDTlog (char *Text)
/**
Text mit Datum, Uhrzeit, Fehlerstatus und Geraetenummer schreiben
**/
{
          char datzeit [32];
	      char FileText [512];
#ifdef ART_ABGL
		  if ((strncmp(Text,"ENDE",4) == 0) || (strncmp(Text,"STAR",4) == 0))
		  {
               dat_zeit_log(datzeit,(short)1);
		  }
		  else
		  {
			  sprintf (datzeit,"                  ");
		  }
#else
          dat_zeit_log(datzeit,(short)1);
#endif
          sprintf (FileText, "%s %s", datzeit, Text);
          WriteFirstLine (FileText);
}

#ifdef _WINDOWS
void pid_schreiben (char * name, int pid)
{
	   char fname [128];
       FILE * fpid;

	   sprintf (fname, "%s%c%s.pid",etc,SLESCH,name);

	   fpid = fopen (fname, "w");
	   //fpid = fopen (fname, "w");
	   if (fpid == NULL)
	   {
		   sprintf(char_zwi,"%s OPEN-ERROR",fname);
		   bildschirm_meldung(char_zwi,(long)0);
		   fehler_meldung((long)1,(long)999,char_zwi);
		   return;
	   }

       sprintf (char_zwi,"%d\n",pid);
	   DEBUG(9,"PID %d schreiben in %s\n",pid,fname);
       fwrite (char_zwi, strlen(char_zwi), 1 , fpid);
	   fclose (fpid);
	   return;
} /* end of pid_schreiben () */


void sirene (int sirene)
{
      short i;
      short j;
      int freq,versuch;
      static int vers [] = {0,1,3,5};

	  versuch = sirene;
      if (versuch > 3) versuch = 3;

      for (i = 0; i < vers [versuch]; i ++) 
      {
                freq = 200;
                for (j = 0; j < 200; j ++)
                {
                           Beep(freq,20);
                           freq += 10;
                }
                for (j = 0; j < 200; j ++)
                {
                           Beep(freq,20);
                           freq -= 10;
                }
      }
} /* end of sirene() */


void doppelte_prozesse_kontrolle (char * name, int pid)
{
	   char fname [128];
       FILE * fpid;
	   int gpid;

	   DEBUG(1,"\nkontrolle ob doppelte Prozesse PID=%d\n",pid);

	   sprintf (fname, "%s%c%s.pid",etc,SLESCH,name);

	   fpid = fopen (fname, "r");
	   //fpid = fopen (fname, "r");
	   if (fpid == NULL)
	   {
		   sprintf(char_zwi,"%s OPEN-ERROR bei Kontrolle",fname);
		   bildschirm_meldung(char_zwi,(long)0);
		   fehler_meldung((long)1,(long)999,char_zwi);
		   return;
	   }

	   _fread (char_zwi, 1 , 255, fpid);
       fclose (fpid);
	   gpid = atoi(char_zwi);
	   if (gpid == 0)
	   {
		   pid_schreiben (name,pid);
		   return;
	   }
	   if (gpid != pid)
	   {
		   sprintf(char_zwi,"ACHTUNG: zwei Prozesse %s aktiv PID=%d und %d",name, pid, gpid);
		   bildschirm_meldung(char_zwi,(long)0);
		   fehler_meldung((long)1,(long)990,char_zwi);
		   if (Signal != 0)
		   {
			   sirene((int)Signal);
		       if (Signal < 3) 
				   Signal++;
			   else
			   {
         		   sprintf(char_zwi,"Prozess %s PID=%d ist beendet",name, pid);
		           bildschirm_meldung(char_zwi,(long)0);
		           fehler_meldung((long)1,(long)999,char_zwi);
        		   pid_schreiben (name,gpid);
				   exit (0);
			   }
		   }
	   }
	   return;
} /* end of doppelte_prozesse_kontrolle	() */

#endif /* _WINDOWS */
//#endif /* _WINDOWS */
#endif /* STEUER_PGM */
