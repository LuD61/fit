
#ifndef DOS
#ident  "@(#) translUC.cpp		VLUGLOTSA	18.09.06	by FF"
#endif
/***
--------------------------------------------------------------------------------
-
-       SE.TEC
-       Unternehmensberatung fuer systemische Entwicklung GmbH
-       Eichendorffstr. 33
-
-       78054 VS-Schwenningen
-
-       Tel.: 07720/840145     Fax: 07720/840190
-       E-Mail gvstec@ibm.net
-
--------------------------------------------------------------------------------

 Fuer dieses Programm behalten wir uns alle Rechte, auch fuer den Fall der
 Patenterteilung und der Eintragung eines anderen gewerblichen Schutz-
 rechtes vor. Missbraeuchliche Verwendung, wie insbesondere Vervielfael-
 tigung und Weitergabe an Dritte ist nicht gestattet; sie kann zivil- und
 strafrechtlich geahndet werden.

--------------------------------------------------------------------------------
-
-       Modulname               :       translUC.c
-
-       Autor                   :       F.Folmer
-       Erstellungsdatum        :       07.07.95
-       Modifikationsdatum      :       24.02.05
-
-       Projekt                 :       BIWAK
-       Version                 :       1.00
-       Laendervariante         :       BRD
-
-       Rechner                 :
-       Betriebssystem          :       Windows
-       Sprache                 :       C
-
-       Modulbeschreibung       :	    FTP fuer FIT, FITSMART
-				
--------------------------------------------------------------------------------
-       Aenderungsjournal       :
-
-       lfd.    Version Datum           Name       Beschreibung
-       ------------------------------------------------------------------------
-       1       2.00    11.02.97	    F.Folmer  Grundmodul
        2       4.20    21.05.01        F.Folmer  DIGI, peri_typ=34
        3       6.00    24.02.05        F.Folmer  MettlerUC, peri_typ=11

        4       1.00    12.07.05        F.Folmer  Aplikation f�r WINDOWS
        4       1.10    10.03.06        F.Folmer  Aplikation f�r WINDOWS FIT
        4       1.12    17.03.06        F.Folmer  �nderungen f�r WINDOWS FIT: rename
        4       1.20    19.04.06        F.Folmer  Status_auswerten f�r OK
        5       1.20    14.06.06        F.Folmer  Zus. Ausgaben
        5       1.50    18.09.06        F.Folmer  Version mit Umsatz-Dateien: INV ...
        5       1.51    28.09.06        F.Folmer  RESET f�r MettlerUC u. andere
        5       1.52    02.10.06        F.Folmer  Interne RESET f�r MettlerUC
        6       1.53    27.10.06        F.Folmer  Klein/Gross-Schreibweise fuer LB-Datei
        7       1.60    15.05.08        F.Folmer  INV, LIE, BES. RET u.s.w
        8       1.70    15.02.10        F.Folmer  Bons ohne Pr�fix abholen (Lieferscheine, Vorbestellung, Warenbon)
        8       1.70    05.03.10        F.Folmer  nur Test
		9       1.80    31.05.10        F.Folmer  Ale Bons im ..\umsXX kopieren Parameter <-k 1> in met_devn.SYS
		-------------------------------------------------------------------------------
***/


#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
//#include <unistd.h>
#include <sys/types.h>
//#include <prototypes.h>
#include <errno.h>



#ifdef OS4680
 #include "unixio.h"
// #include "netdb.h"
 #include "ftpapi.h"
 #include "fopen.h"
#else
 #include <fcntl.h>
 #include <signal.h>
 #include <sys/types.h>
 #include <sys/stat.h>
 #include <windows.h>
 #include <wininet.h>
#endif
//#include "split.h"
//#include "conf_env.h"
#include <time.h>
//#include "biz_allg.h"

#define DATUM  "31.05.10 "
#define VERSION_TRANSLUC (short)1
#define VERSION_NACHKOMMA_TRANSLUC (short)80

#define        LAENGE_LOG       (int)4000      /* Laenge der logbuch-Datei */

#define        WARTE_ZEIT       (int)5
#define        GER_METT_UC       (short)11

//static char OUT_verz[48];
char treiber[48];
FILE * flog;
FILE * fbat;
char tmppath[64];
char sichern[64];
char UCziel[64];
char bin[64];
char etc[64];
char konvert[64];
char v_logbuch[64];
char dev_datei[64];
char char_zwi[256];
char buff[64];
int   tst=0;
int   warte_zeit=0;
short testen=0;
short ohne_aufruf=0;
short testlog=0;
short hilfe=0;
short version=0;
long error=0;
long error_sav=0;
long exit_code=0;
extern struct tm * strtime;
short mode_arg=0;
char  mode_arg_char[10];
char  kennung_arg[8]= {"0"};
short loeschen_arg=0;
short devdataufbau=0;
char  dev_arg[64];
char quelle_dat[64];
char quelle_dat1[64];
int  kopieren;              /* 31.05.10 */

/*  Parameter aus der DEV-Datei */
short netz=0;
int   sys=0;
char  id[20];
char  user[16];
char  passwd[16];
char  input[64];
char  input_quit[64];
char  output[64];
char  rspoutput[64];               /* 17.03.06 FF */
char  output_quit[64];
int   warten_par=0;
char  lbdat[32];
char  logbuch[64];
char  IN_verz[64];
char  TAF_verz[64];
char  OUT_verz[64];
char  home[64];
char  home_waage[64];
char  status_dat[64];
int   laenge_log=LAENGE_LOG;
short ping_prue=0;
short mit_praefix=0;               /* 15.02.10 FF */

/* Prototyp */
long treiber_starten(int,short);
void DEBUG(int tst, char *format, ...);
int localname(char *);
void fehler_meldung(long,long,char*);
char* dat_zeit_log(char*,short);
long dev_dat_lesen (char *, FILE *);
long put_uc (int,int);
long get_uc (int,int);
long get_uc_lb (int,int);
long reset (int,int,short);
long lb_konvertieren(char*,char*,char*);
short get_tag(char*);
int CopyFile(char*, char*);
/*------------------------------------------------------------------*/
/*                     Hilfsfunktionen                              */
/*------------------------------------------------------------------*/

int tst_arg_sw (char * arg)
/**
Argumente nach '-' testen.
**/
{

for (;*arg; arg += 1)
       {
       switch (*arg)
               {
               case 't' :
		       if(testlog == 0)
                            testen = 1;
                       break;
               case 'h' :
                       hilfe = 1;
                       break;
               case 'd' :
                       testlog = 1;
                       testen = 0;
                       break;
               case 'v' :
                       version = VERSION_TRANSLUC;
                       break;
               case 'o' :
                       ohne_aufruf = 1;
                       break;
               case 'c' :
                       devdataufbau = 1;
                       break;
               /*****
			   case 'l' :
                       loeschen = 0;
                       break;
               ****/
               case 'w' :
                       warte_zeit = (int) atoi (arg + 1);
                       break;
               default:
                       break;
               }
       }
return(0);
}

/***********************************************************/
int copy (char *ziel, char *quelle, BOOL par)
/**
Datei kopieren. wenn par = FALSE mit system()
**/
{
	       int err = 0;
           FILE *fz;
           FILE *fq;
           //int bytes,zbytes;
           size_t bytes,zbytes;
		   char buffer[1024];
           
		   if (testen)
               printf("COPY: %s nach %s\n", quelle,ziel);
		   
		   if (par == FALSE)
		   {
		       sprintf (char_zwi,"copy %s %s",quelle,ziel);
		       DEBUG(1,"SYSTEM: %s\n",char_zwi);
		       err = system(char_zwi);
		       DEBUG(1,"err=%d\n",err);
		       return (err);
		   }

		   unlink(ziel);
           fq = fopen (quelle, "rb");
           if (fq == (FILE *) 0)
           {
                       sprintf (buff,"%s kann nicht ge�ffnet werden", quelle);
                       //fehler_meldung ((long)1, (long)99, buff);
                       DEBUG(1,"%s\n",buff);
                       return (99);
           }
		   else
			   DEBUG(1,"Dat %s zum Lesen geoeffnet\n",quelle);

           fz = fopen (ziel, "w+b");
           if (fz == (FILE *) 0)
           {
                       fclose (fq);
                       sprintf (buff,"%s kann nicht ge�ffnet werden", ziel);
                       //fehler_meldung ((long)1, (long)98, buff);
                       DEBUG(1,"%s\n",buff);
                       return (98);
           }
		   else
			   DEBUG(1,"Dat %s zum Schreiben geoeffnet\n",ziel);

           while  (bytes = fread (buffer, 1, 0x1000, fq))
           {
			            DEBUG(1,"fread bytes=%d\n",bytes);
                        zbytes=fwrite (buffer, 1, bytes, fz);
                        //zbytes=fwrite (buffer, (size_t)bytes, (size_t)1, fz);
			            DEBUG(1,"fwrite zbytes=%d\n",zbytes);
           }
           fclose (fq);
           fclose (fz);

           return (0);
}


/**********************************************************************/
void cr_weg (char * string)
/**
CR aus String entfernen.
**/
{
 
        for (;*string; string += 1)
        {
                   if ((*string == 10) ||
                       (*string == 13))
                   {
                            *string = 0;
                             break;
                   }
        }
}

/**********************************************************************/
int localname (char * tname)
/**
Netzkonoten fuer Lesezugriff ignorieren.
**/
{
          char dname [256];
          char *kpos;

          kpos = strstr (tname, "::");
          if (kpos == (char *) 0) return (0);
          kpos += 2;
          strcpy (dname, kpos);
          strcpy (tname, dname);
          return (0);
}
/**********************************************************************/



/**************** usage() ******************************/
void usage()
{
   //printf("usage: version=%d hilfe=%d devdataufbau=%d\n",version, hilfe, devdataufbau);
   sprintf(char_zwi,"----------------------------------------------------------\n");
   if(version != 0 || hilfe != 0)
   {
	printf("\ntranslUC: Version %d.%02d  vom %s fuer WINDOWS\n",
	VERSION_TRANSLUC,VERSION_NACHKOMMA_TRANSLUC,DATUM);
	printf("Copyright (c)   SE.TEC   2005/08/10\n");
    if(hilfe == 0)
	        {
                printf("\n");
		        if(testen == 0 && testlog == 0)
	            exit(0);
	        }
    else
	        {
                printf("\n\n                               ENTER-Taste");
		        getchar();
                printf("\n\n\n\n\n\n");
	        }
   }
   if(hilfe != 0)
   {
   printf("\n\nAufruf:  translUC [-vhtdowX] DEV MODE [KENNUNG] [LOESCHEN]\n");
   printf("%s",char_zwi);
   printf("Parameter: -vh: Version/Hilfe\n");
   printf("           -t : Testausgaben auf dem Bildschirm\n");
   printf("           -d : Testausgaben in die Datei <TMPPATH\\translUC.tst>\n");
   printf("           -o : ohne Aufruf <nur Testen>\n");
   printf("           -c : Aufbau der Device-Datei\n");
   //printf("           -wX: Wartezeit in sec nach der Uebertragung <DIGI>\n");
   printf("           -wX: Timeout in sec <MettlerUC>\n");
   printf("DEV           : Device-Datei mit dem Pfad\n");
   printf("MODE          : 1 = put/Senden Stammdaten\n");
   printf("              : 2 = get/Holen  Langbons mit Praefix\n");
   printf("              : 3 = get/Holen  Langbons ohne Praefix\n");
   printf("              : 4 = get/Holen  Umsatz   <in der Bearbeitung>\n");
   printf("              : 99= Reset der Waage <mit Loeschen>\n");
   printf("KENNUNG       : 0     LB-Abverkauf  <Default>\n");
   printf("              : INV   LB-Inventur\n");
   printf("              : RET   LB-Retouren\n");
   printf("LOESCHEN      : 0=Daten werden geloescht <Default>\n");
   printf("              : 1=ohne Loeschen in der Waagen\n");
   printf("%s",char_zwi);

   printf("Die Parameter kann man auch als Environment PAR_TRANSLUC definieren\n");
   printf("\n\n                               ENTER-Taste");
   getchar();
   printf("\n");
   printf("\nLOG-Datei:    Bsp.: TMPPATH\\met_bat.SYS  <in DEV-Datei definiert>\n");
   printf("Exit-Datei:   TMPPATH\\exit_ftp.SYS\n");
   if(devdataufbau != 0)
	        {
                printf("\n                               ENTER-Taste");
		        getchar();
                printf("\n\n\n\n\n\n");
	        }
   }

   if (devdataufbau == 1)
   {
	   printf("Aufbau der Device-Datei   <S. Arg2>\n");
       printf("%s",char_zwi);
	   printf("       Beispiel\n");
       printf("%s",char_zwi);
       printf("-n 0                          <0-FTP, 1-WINDOWS-Netz, Default=0>\n");
       printf("-s 6                          <Sys-Nummer>\n");
       printf("-d 10.1.45.45                 <IP-Nummer, nur FTP>\n");
       printf("-u anonymous                  <user>\n");
       printf("-p anonymous                  <Password>\n");
       printf("-i TMPPATH\\metinuc.6          <Local-INPUT Datei>\n");
       printf("-j cmducart.xml               <Remote-INPUT Datei>\n");
       printf("-o TMPPATH\\metoutuc.6         <Local-OUTPUT Datei>\n");
       printf("-a TMPPATH\\RSPmetoutuc.6      <Local-RSPOUTPUT Datei, Antwort-Datei>\n");
       printf("-q rspucart.xml               <Remote-OUTPUT Datei>\n");
       printf("-w 60                         <Wartezeit in sec>\n");
       printf("-b *.xml                      <LB-Datei zu cmd ls>\n");
       printf("-l TMPPATH\\met_bat.6          <LOG-Datei>\n");
       printf("-x IN                         <Waagen-Verz: Stammdaten>\n");
       printf("-y TAF                        <Waagen-Verz: Langbon>\n");
       printf("-z OUT                        <Waagen-Verz: Umsatz/Quittungen>\n");
       printf("-h c:\\fitsmart                <HOME-Verz fuer WWS>\n");
       printf("-v m:                         <HOME-Verz fuer MettlerUC, nur WINDOWS-Netz>\n");
       printf("-r 1                          <PING pruefen 0/1,  Default=1>\n");
       printf("-f 9999                       <Anzal Zeilen in LOG,  Default=4000>\n");
       printf("-k 1                          <Alle Bons im umsXX kopieren>\n");
       printf("%s",char_zwi);
   }

   exit(0);
}/* end of usage() */

/**********************************************************/                     
int dat_ping_pruefen (char * dat)
{
	int err=1;
	FILE * fp;
    char buff[255];
    char * p;
    char * q;
    char * q1;

	DEBUG(1,"DAT=%s\n",dat);
	fp = fopen (dat,"r");
	if (fp == NULL)
	{
        error = 59;
	    if (error_sav == 0)
			  error_sav = error;
        sprintf (char_zwi,"Sys=%d: ERR bei ping",sys);
        fehler_meldung((long)1,(long)error,char_zwi);
        DEBUG(1,"%s\n",char_zwi);
		return 2;
	}

	p = fgets (buff, 255, fp);
	while (p != NULL)
	{
		printf ("%s",buff);
		q = strstr(buff,"Antwort");
		q1 = strstr(buff,"Bytes");
		if (q != NULL && q1 != NULL)
		{
			err = 0;
			break;
		}
     	p = fgets (buff, 255, fp);
	}
	fclose (fp);

    return err;
} /* end of dat_ping_pruefen () */

/**************************************************************/
int ping_pruefen (char * kanal)
{
	int err=0,ex=0;
    char buffer[128];
	char ping_dat[64];
	int anz=2;

    if (ping_prue == 1)
	{
         sprintf (buffer,"Sys=%d: PING an %s",sys,kanal);
         fehler_meldung ((long)1, (long)0, buffer);
         DEBUG(1,"%s\n",buffer);
         sprintf (ping_dat,"%s\\ping_dat.%d",tmppath,sys);
		 unlink(ping_dat);
         sprintf (buffer,"ping %s > %s", kanal,ping_dat);
         DEBUG(1,"%s\n",buffer);
         while (anz != 0)
		 {
              ex = (int)system (buffer);
              ex = dat_ping_pruefen(ping_dat);
	          DEBUG(1,"PING: ex=%d,  anz=%d\n",(int)ex,anz);
              if (ex == 0)
		           break;
	          anz--;
		 }

         if (ex != 0)
		 {
              error = 59;
			  if (error_sav == 0)
				  error_sav = error;
              sprintf (char_zwi,"Sys=%d kanal=%s ERR bei ping",sys,kanal);
              fehler_meldung((long)1,(long)error,char_zwi);
              DEBUG(1,"%s\n",char_zwi);
              return (1);
		 }
         else
		 {
	          sprintf(char_zwi,"Sys=%d: %s PING OK",sys,kanal);
	          fehler_meldung((long)1,(long)0,char_zwi);
	          DEBUG(1,"%s\n",char_zwi);
		 }
	}
    else
		DEBUG(1,"ping_prue nicht aktiv\n");

    return err;
} /* end of ping_pruefen () */

/**********************************
Headerfile wininet.h
Bibliothek wininet.lib




    WIN32_FIND_DATA fileinfo;


    HINTERNET hInternet = InternetOpen(Programmname,
                                       INTERNET_OPEN_TYPE_PRECONFIG ,
                                       NULL,
                                       NULL,
 				       0);

    HINTERNET hConnect = InternetConnect(hInternet,
                                      IP-Adresse,
 			  	      INTERNET_INVALID_PORT_NUMBER,
                                      username,
                                      password,
                                      INTERNET_SERVICE_FTP ,
                                      INTERNET_FLAG_PASSIVE ,
						  	  0);

    
    HINTERNET Dir = FtpFindFirstFile(hConnect,
                                     "D:\\Mettler\\up\\*",
                                     &fileinfo, 
                                     INTERNET_FLAG_RELOAD, 
                                     0);

    while (InternetFindNextFile (Dir, &fileinfo)) 
    {
    }
    InternetCloseHandle (Dir);


    BOOL ret = FtpPutFile(hConnect,
                          quelle,
                          ziel (remote),
                          FTP_TRANSFER_TYPE_ASCII/FTP_TRANSFER_TYPE_BINARY  | INTERNET_FLAG_RELOAD,
			  1);

    BOOL ret = FtpGetFile(hConnect,
                          quelle (remote),
                          ziel,
                          FALSE (failExists)
                          FTP_TRANSFER_TYPE_ASCII/FTP_TRANSFER_TYPE_BINARY  | INTERNET_FLAG_RELOAD,
			  1);

   BOOL ret = FtpDeleteFile (hConnect, File);

	
   InternetCloseHandle (hConnect);
   InternetCloseHandle (hInternet);
**********************************/
long status_auswerten (char * dat, int sys)
{
	char zwi_dat[64];
	long err=0;
	int anz = 0;       /* 19.04.06 */
	FILE * fp;
    char status[36];

    sprintf (zwi_dat,"%s\\met_sta.%d",tmppath,sys);
	DEBUG(1,"status_auswerten Dat=%s zwi_dat=%s\n",dat,zwi_dat);
  
    sprintf(char_zwi,"%s\\readxml %s\\ucmetstatus.if %s\\ucmetstatus.ifx %s %s",
                     bin,
					 konvert,
					 konvert,
                     zwi_dat,
					 dat);
    DEBUG(9,"%s\n",char_zwi); 
    err = system(char_zwi);
	DEBUG(1,"err=%ld\n",err);
    if (err != 0)
    {
                err = 713;
                sprintf (char_zwi, "ERR readxml %s",dat);
                fehler_meldung ((long)1, err, char_zwi);
                DEBUG(1,"err=%ld %s\n",err,char_zwi);
                return (err);
    }

	fp = fopen (zwi_dat, "r");
	if (fp == NULL)
	{
                err = 714;
                sprintf (char_zwi, "ERR read %s",zwi_dat);
                fehler_meldung ((long)1, err, char_zwi);
                DEBUG(1,"err=%ld %s\n",err,char_zwi);
                return (err);
	}
    anz = fread (status, 1, 36, fp);
	/* 19.04.06 A */
	if (anz == 0)
	{
		err = 0;
	    sprintf (char_zwi,"Sys=%d: anz=%d",sys, anz);
        //fehler_meldung ((long)1, err, char_zwi);
		DEBUG(1,"%s\n",char_zwi);
	}
	else
	/* 19.04.06 E */
	{
        err = (long) atoi (status);
        cr_weg(status);
	    status[35] = 0x00;
	    sprintf (char_zwi,"Sys=%d: anz=%d",sys, anz);
        //fehler_meldung ((long)1, err, char_zwi);
		DEBUG(1,"%s\n",char_zwi);
	}


	if (err != 0)
	{
	    sprintf (char_zwi,"Status=%s", &status[5]);
        fehler_meldung ((long)1, err, char_zwi);
        printf ("%s\n",char_zwi);
	}
	else
	{
	    sprintf (char_zwi,"Status OK");
        fehler_meldung ((long)1, err, char_zwi);
        printf ("%s\n",char_zwi);
	}
	
	fclose (fp);
	return(err);
}
/*************************************************************************/
long put_uc(int sys,int len)
{
	short merker=0;
	int warten=0;
    long error=0;
    long error1=0;
	char zwi_dat[64];
    char quit_dat[64];
	char quelle_dat[64];
    char metinzwi[64];
    char metinquelle[64];
    //char * p;
    FILE * fp;
	int  anz_loe=0;
	int  anz_bearb=0;
	int  anz_gelesen=0;
    WIN32_FIND_DATA fileinfo;
    HINTERNET Dir;
	HINTERNET hInternet;
    HINTERNET hConnect;
    BOOL Ret;
    BOOL ret_get;
    BOOL ret_put;
    BOOL ret_del;

	sprintf (char_zwi,"Sys=%d: Daten senden: Kennung=%s",sys,kennung_arg);
	fehler_meldung ((long)1, error, char_zwi);
	DEBUG(9,"%s\n",char_zwi);

	switch (netz)
	{
	case 0:      /****************** FTP ********************************/
        DEBUG(1,"put_uc: sys=%ld len=%d   FTP\n",sys,len);

        /* Pruefen ob INPUT vorhanden */
        fp = fopen (input, "r");
		if (fp == NULL)
		{
			error = 60;
			sprintf (char_zwi,"INPUT=%s nicht vorhanden", input);
		    DEBUG(1,"error=%ld: %s\n",error,char_zwi);
			fehler_meldung ((long)1, error, char_zwi);
			error = 0;
			sprintf (char_zwi,"Keine �bertragung");
		    DEBUG(1,"error=%ld: %s\n",error,char_zwi);
			fehler_meldung ((long)1, error, char_zwi);
            break;
		}
		fclose (fp);

		/* PING-Pruefen */
		error = (long)ping_pruefen(id);
		if (error != 0)
		{
			sprintf (char_zwi,"Waagen nicht im Netz o. Waagen ausgeschaltet");
		    DEBUG(1,"error=%ld: %s\n",error,char_zwi);
			error = 59;
			fehler_meldung ((long)1, error, char_zwi);
            break;
		}

		/* OPEN */
		//DEBUG(1,"vor InternetOpen\n");
        hInternet = InternetOpen("transluc",
                                 INTERNET_OPEN_TYPE_PRECONFIG ,
                                 NULL,
                                 NULL,
 		                         0);

		if (hInternet == NULL)
		{
			error = 999;
			//DEBUG(1,"OPEN-INTERNET false\n");
			sprintf (char_zwi,"OPEN-INTERNET false");
		    DEBUG(1,"error=%ld: %s\n",error,char_zwi);
			fehler_meldung ((long)1, error, char_zwi);
			break;
		}
        else
		    DEBUG(1,"nach InternetOpen OK\n");

        /* Connect */
        DEBUG(1,"id=%s user=%s passwd=%s\n",id,user,passwd);
		hConnect = InternetConnect(hInternet,
                                   id,
 		                           INTERNET_INVALID_PORT_NUMBER,
                                   user,
                                   passwd,
                                   INTERNET_SERVICE_FTP ,
                                   INTERNET_FLAG_PASSIVE ,
			  	                   0);
		if (hConnect == NULL)
		{
			error = 999;
			sprintf (char_zwi,"CONNECT-INTERNET false");
		    DEBUG(1,"error=%ld: %s\n",error,char_zwi);
			fehler_meldung ((long)1, error, char_zwi);
			break;
		}
        else
		    DEBUG(1,"nach InternetConnect OK\n");

		sprintf (quelle_dat,"\\%s",IN_verz);
		DEBUG(1,"vor FtpSetCurrentDirectory: quelle_dat=%s\n",quelle_dat);

        Ret = FtpSetCurrentDirectory (hConnect,quelle_dat);

		Dir = FtpFindFirstFile(hConnect,
			                   quelle_dat,
                               &fileinfo, 
                               INTERNET_FLAG_RELOAD, 
                               0);

	    /* 02.10.09 FF Interne RESET A */
		printf("RESET\n");
        error = reset(sys,len,(short)1);
	
        DEBUG (1,"Interne RESET: error=%ld\n",error);
        if (error != 0)
		{
                 sprintf (char_zwi,"Sys=%d: Error bei RESET",sys);
                 fehler_meldung ((long)1, error, char_zwi);
		}
	    /* 02.10.09 FF Interne RESET A */

	    if (Dir == NULL)
		{
			 DEBUG(1,"Dir=FALSE: fur %s\n",quelle_dat);
             Dir = FtpFindFirstFile(hConnect,
                                    NULL,
                                    &fileinfo, 
                                    INTERNET_FLAG_RELOAD, 
                                     0);
		}
		else
			 DEBUG(1,"Dir=TRUE: fur %s\n",quelle_dat);

        
        DEBUG(1,"input=%s\n",input);
		sprintf (quelle_dat,"%s\\%s\\%s",home_waage,IN_verz,input_quit);
		DEBUG(1,"1:quelle_dat=%s\n",quelle_dat);
		sprintf (quit_dat,"%s\\%s\\%s",home_waage,OUT_verz,output_quit);
		DEBUG(1,"1:quit_dat=%s\n",quit_dat);
		sprintf (metinzwi,"metinzwi.%d",sys);
		DEBUG(1,"1:metinzwi=%s\n",metinzwi);
		sprintf (metinquelle,"%s\\%s",tmppath,input_quit);
		DEBUG(1,"1:metinquelle=%s\n",metinquelle);

		/* BEARBEITEN */
		/* Daten Konvertieren */
        /* in Treiber nicht vorgesehen */

		sprintf (char_zwi,"SENDEN");
        fehler_meldung ((long)1, error, char_zwi);
        DEBUG(1,"%s\n",char_zwi);

		/* �bertragung */
		while (TRUE)
		{
			/* pruefen ob Datei vorhanden */
             DEBUG(1,"vor FtpGetFile metinquelle=%s quelle_dat=%s\n",metinquelle,quelle_dat);
             ret_get = FtpGetFile(hConnect,
                             quelle_dat,
                             metinquelle,
							 FALSE,
                             FTP_TRANSFER_TYPE_BINARY  | INTERNET_FLAG_RELOAD,
			                 1,
							 0);
		     if (ret_get == TRUE)
			 {
			       DEBUG(1,"get_ret=TRUE\n");
                   sprintf(char_zwi,"Dat=%s in der Waage nicht bearbeitet",quelle_dat);
                   error = 704;
                   fehler_meldung((long)1,(long)error,char_zwi);
                   DEBUG(9,"ERR=%ld: %s\n",(long)error,char_zwi);
                   if (loeschen_arg == 0 || testen == 1)
				   {
					   DEBUG(1,"BEMERKUNG: Datei %s wird geloescht\n",quelle_dat);
				        ret_del = FtpDeleteFile (hConnect, quelle_dat);
		                if (ret_del == TRUE)
						{
                             sprintf(char_zwi,"*** Dat=%s in der Waage geloescht ************",quelle_dat);
                             error = 0;
                             fehler_meldung((long)1,(long)error,char_zwi);
			                 DEBUG(1,"ret_del=TRUE  %s geloescht\n",quelle_dat);
						}
		                else
						{
                             sprintf(char_zwi,"*** Dat=%s nicht geloescht *****************",quelle_dat);
                             error = 704;
                             fehler_meldung((long)1,(long)error,char_zwi);
			                 DEBUG(1,"ret_del=FALSE fuer Dat=%s\n",quelle_dat);
                             break;
						}
				   }
				   else
                        break;
             }
		     else
			 {
			       DEBUG(1,"get_ret=FALSE fuer quelle_dat=%s metinquelle=%s\n",quelle_dat,metinquelle);
                   //break;
			 }

			 /* Datei als Zwischendatei �bertragen */
             DEBUG(1,"vor FtpPutFile input=%s metinzwi=%s\n",input,metinzwi);

             ret_put = FtpPutFile(hConnect,
                             input,
                             metinzwi,
                             FTP_TRANSFER_TYPE_BINARY  | INTERNET_FLAG_RELOAD,
			                 1);
		     if (ret_put == TRUE)
			 {
			       DEBUG(1,"ret_put=TRUE: ZWI %s gesendet\n",metinzwi);
                   sprintf (char_zwi, "Dat=%s gesendet",input);
                   fehler_meldung((long)1,(long)error,char_zwi);
                   DEBUG(9,"%s\n",char_zwi);
             }
		     else
			 {
			       DEBUG(1,"get_put=FALSE fuer input=%s\n",input);
                   error = 705;
                   sprintf (char_zwi, "Uebertragungsfehler put: %s",input);
                   fehler_meldung((long)1,(long)error,char_zwi);
                   DEBUG(9,"%s\n",char_zwi);
                   break;
			 }
			 /* Zwischendatei als OUTPUT-Datei umbenennen */
             DEBUG(1,"1:vor FtpRenameFile metinzwi=%s input_quit=%s\n",metinzwi,input_quit);
             Ret = FtpRenameFile(hConnect,
                             metinzwi,
                             input_quit);
		     if (Ret == TRUE)
			 {
			       DEBUG(1,"Ret=TRUE: %s umbenannt als %s\n",metinzwi,input_quit);
			 }
		     else
			 {
			       DEBUG(1,"Ret=FALSE fuer REN %s %s\n",metinzwi,input_quit);
                   error = 706;
                   sprintf (char_zwi, "ERR bei RENAME %s %s",metinzwi,input_quit);
                   fehler_meldung((long)1,(long)error,char_zwi);
                   DEBUG(9,"%s\n",char_zwi);
                   break;
			 }

			 /* �bertragung Pruefen und Status auswerten */
			 /* Verzeichnis OUT */
			 DEBUG(1,"RSP Holen\n");
		     sprintf (quelle_dat,"..\\%s",OUT_verz);
		     DEBUG(1,"vor FtpSetCurrentDirectory: quelle_dat=%s\n",quelle_dat);
             Ret = FtpSetCurrentDirectory (hConnect,quelle_dat);
		     if (Ret == TRUE)
			 {
			       DEBUG(1,"Ret=TRUE: bei cd %s\n",quelle_dat);
			 }
		     else
			 {
			       DEBUG(1,"Ret=FALSE bei cd %s\n",quelle_dat);
                   error = 999;
                   sprintf (char_zwi, "ERR bei cd %s",quelle_dat);
                   fehler_meldung((long)1,(long)error,char_zwi);
                   DEBUG(9,"%s\n",char_zwi);
                   break;
			 }

             /* RSP-Datei holen und in der Waage loeschen */
			 sprintf (quelle_dat,"%s\\%s\\%s",home_waage,OUT_verz,output_quit);
			 //sprintf (quelle_dat,"%s",output_quit);
		     DEBUG(1,"quelle_dat=%s\n",quelle_dat);

             DEBUG(1,"vor FtpGetFile output=%s quelle_dat=%s\n",rspoutput,quelle_dat);
			 unlink (rspoutput);
		     DEBUG(1,"unlink %s\n",rspoutput);

			 DEBUG(1,"WARTEN: warten=%d, warten_par=%d\n",warten,warten_par);
			 printf ("TIMEOUT=%d\n",warten_par);
             merker = 0;
             warten = 0;
             while (warten <= warten_par)  
             {
                 ret_get = FtpGetFile(hConnect,
                             quelle_dat,
                             rspoutput,
							 FALSE,
                             FTP_TRANSFER_TYPE_BINARY  | INTERNET_FLAG_RELOAD,
			                 1,
							 0);
		         if (ret_get == TRUE)
				 {
			         DEBUG(1,"get_ret=TRUE, Datei %s geholt\n",rspoutput);
					 merker = 1;
					 break;
				 }
		         else
				 {
					 warten++;
					 Sleep(1000);
					 if (warten%10 == 0)
					     printf("%d",warten);
					 else
					     printf(".");
				 }
             }
			 printf ("\n");
			 DEBUG(1,"merker=%d\n",merker);
             /* Datei RSP.xml in der Waage loeschen */
			 if (merker == 1)
			 {
				 if (loeschen_arg == 0)
				 {
					DEBUG(1,"Datei %s loeschen\n",quelle_dat);
				    ret_del = FtpDeleteFile (hConnect, quelle_dat);
		            if (ret_del == TRUE)
					{
			             DEBUG(1,"ret_del=TRUE  %s geloescht\n",quelle_dat);
                         sprintf(char_zwi,"RSP %s geloescht",quelle_dat);
                         DEBUG(9,"%s\n",char_zwi);
                         fehler_meldung((long)1,(long)0,char_zwi);
					}
		            else
					{
			             DEBUG(1,"ret_del=FALSE fuer Dat=%s\n",quelle_dat);
                         error = 707;
                         sprintf(char_zwi,"RSP %s nicht geloescht",quelle_dat);
                         DEBUG(9,"%s\n",char_zwi);
                         fehler_meldung((long)1,(long)error,char_zwi);
						 break;
					}
				 }
				 else
				 {
					 DEBUG(1,"Datei %s nicht geloescht, loeschen_arg=%d\n",quelle_dat,loeschen_arg);
				 }
			 }
			 else
			 {
                    error = 708;
                    sprintf (char_zwi, "Timeout RSP-Holen: %s",quelle_dat);
                    fehler_meldung((long)1,(long)error,char_zwi);
                    DEBUG(9,"\n%s\n",char_zwi);
                    //19.09.06 wegen Synchronisation 
					/***
					if (loeschen_arg == 0)
					{
						printf ("Warten %d sec und loeschen %s\n",warten_par,quelle_dat);
					    Sleep(warten_par * 1000);
				        ret_del = FtpDeleteFile (hConnect, quelle_dat);
                    }
					***/
                    //break;
			 }

             /* STATUS-Datei holen, auswerten und in der Waage loeschen */
			 DEBUG(1,"STATUS Holen\n");
			 sprintf (quelle_dat,"%s\\%s\\%s",home_waage,OUT_verz,status_dat);
		     DEBUG(1,"quelle_dat=%s\n",quelle_dat);

		     //sprintf (metinzwi,"%s\\metout_status.%d",tmppath,sys);
		     sprintf (metinzwi,"%s\\RSP_staxml.%d",tmppath,sys);
		     DEBUG(1,"metinzwi=%s\n",metinzwi);

		     DEBUG(1,"vor FtpGetFile metinzwi=%s quelle_dat=%s\n",metinzwi,quelle_dat);
			 unlink (metinzwi);
		     DEBUG(1,"unlink %s\n",metinzwi);

			 DEBUG(1,"WARTEN: warten_par=%d warten=%d\n",warten_par,warten);
			 printf ("TIMEOUT=%d\n",warten_par);
             merker = 0;
			 warten = 0;
             while (warten <= warten_par)  
             {
                 ret_get = FtpGetFile(hConnect,
                             quelle_dat,
                             metinzwi,
							 FALSE,
                             FTP_TRANSFER_TYPE_BINARY  | INTERNET_FLAG_RELOAD,
			                 1,
							 0);
		         if (ret_get == TRUE)
				 {
			         DEBUG(1,"get_ret=TRUE\n");
					 merker = 1;
					 break;
				 }
		         else
				 {
					 warten++;
					 Sleep(1000);
			         //DEBUG(1,"get_ret=FALSE fuer quelle_dat=%s output=%s\n",quelle_dat,output);
					 if (warten%10 == 0)
					     printf("%d",warten);
					 else
					     printf(".");
				 }
             }
			 printf ("\n");
			 DEBUG(1,"merker=%d\n",merker);
             /* Status-Datei .xml in der Waage loeschen */
			 if (merker == 1)
			 {
				 if (loeschen_arg == 0)
				 {
					DEBUG(1,"Datei %s loeschen\n",quelle_dat);
				    ret_del = FtpDeleteFile (hConnect, quelle_dat);
		            if (ret_del == TRUE)
					{
			             DEBUG(1,"ret_del=TRUE  %s geloescht\n",quelle_dat);
                         sprintf(char_zwi,"STA %s geloescht",quelle_dat);
                         DEBUG(9,"%s\n",char_zwi);
                         fehler_meldung((long)1,(long)0,char_zwi);
					}
		            else
					{
			             DEBUG(1,"ret_del=FALSE fuer Dat=%s\n",quelle_dat);
                         error = 707;
                         sprintf(char_zwi,"STA %s nicht geloescht",quelle_dat);
                         DEBUG(9,"%s\n",char_zwi);
                         fehler_meldung((long)1,(long)error,char_zwi);
						 break;
					}
				 }
				 else
				 {
					 DEBUG(1,"Datei %s nicht geloescht, loeschen_arg=%d\n",quelle_dat,loeschen_arg);
				 }
			 }
			 else
			 {
                    error = 708;
                    sprintf (char_zwi, "Timeout STATUS-Holen: %s",quelle_dat);
                    fehler_meldung((long)1,(long)error,char_zwi);
                    DEBUG(9,"\n%s\n",char_zwi);
                    break;
			 }

			 /* STATUS auswerten */
			 error = status_auswerten(metinzwi,(int)sys);

   		     break;
		}/* end of while(TRUE) �bertragung */

        InternetCloseHandle (Dir);
        InternetCloseHandle (hConnect);
        InternetCloseHandle (hInternet);
		if (error == 0)
		{
                   sprintf (char_zwi, "Datei=%s OK gesendet",input);
                   fehler_meldung((long)1,(long)error,char_zwi);
                   DEBUG(9,"%s\n",char_zwi);
		}
		break;


	case 1:   /**************** WINDOWS-NETZ **********************/

        DEBUG(1,"put_uc: sys=%ld len=%d   WINDOWS-Netz\n",sys,len);
        /* Pruefen ob INPUT vorhanden */
        fp = fopen (input, "r");
		if (fp == NULL)
		{
			error = 60;
			sprintf (char_zwi,"INPUT=%s nicht vorhanden", input);
		    DEBUG(1,"error=%ld: %s\n",error,char_zwi);
			fehler_meldung ((long)1, error, char_zwi);
			error = 0;
			sprintf (char_zwi,"Keine �bertragung");
		    DEBUG(1,"error=%ld: %s\n",error,char_zwi);
			fehler_meldung ((long)1, error, char_zwi);
            break;
		}
		fclose (fp);

		/* Pruefen ob Waage im Netz */
		sprintf (zwi_dat,"%s\\null",tmppath);
		sprintf (char_zwi,"dir %s > %s",home_waage,zwi_dat);
		DEBUG(1,"SYS: %s\n",char_zwi);
		error=system(char_zwi);
		if (error != 0)
		{
			sprintf (char_zwi,"Waagen nicht im Netz o. Waagen ausgeschaltet");
		    DEBUG(1,"error=%ld: %s\n",error,char_zwi);
			error = 59;
			fehler_meldung ((long)1, error, char_zwi);
            break;
		}

		/* 02.10.09 FF Interne RESET A */
        error = reset(sys,len,(short)1);
	
        DEBUG (1,"Interne RESET: error=%ld\n",error);
        if (error != 0)
		{
                 sprintf (char_zwi,"Sys=%d: Error bei RESET",sys);
                 fehler_meldung ((long)1, error, char_zwi);
		}
	    /* 02.10.09 FF Interne RESET A */
		
		unlink (zwi_dat);
		DEBUG(1,"unlink %s\n",zwi_dat);
		
		/* BEARBEITEN */
		/* Daten Konvertieren */
        /* in Treiber nicht vorgesehen */


		/* Datei senden */
		while (TRUE)
		{
		    sprintf (quelle_dat,"%s\\%s\\%s",home_waage,IN_verz,input_quit);
		    DEBUG(1,"ZIEL=quelle_dat=%s\n",quelle_dat);
		    sprintf (metinzwi,"%s\\%s\\metinzwi.%d",home_waage,IN_verz,sys);
		    DEBUG(1,"metinzwi=%s\n",metinzwi);

		    /* Pruefen ob alte Datei bearbeitet */
		    fp = fopen (quelle_dat, "r");
		    if (fp != NULL)
			{
				   fclose (fp);
                   sprintf(char_zwi,"Dat=%s in der Waage nicht bearbeitet",quelle_dat);
                   error = 704;
                   fehler_meldung((long)1,(long)error,char_zwi);
                   DEBUG(9,"ERR=%ld: %s\n",(long)error,char_zwi);
                   if (loeschen_arg == 0 || testen == 1)
				   {
					    DEBUG(1,"BEMERKUNG: Datei %s wird geloescht\n",quelle_dat);
				        error = unlink(quelle_dat);
		                if (error == 0)
						{
                             sprintf(char_zwi,"*** Dat=%s in der Waage geloescht ***********",quelle_dat);
                             error = 0;
                             fehler_meldung((long)1,(long)error,char_zwi);
			                 DEBUG(1,"ret_del=TRUE  %s geloescht\n",quelle_dat);
						}
		                else
						{
                             sprintf(char_zwi,"*** Dat=%s nicht geloescht *********",quelle_dat);
                             error = 704;
                             fehler_meldung((long)1,(long)error,char_zwi);
                             break;
						}
				   }
				   else
                        break;
/* TESTEN 
                   if (testen)
				   {
				       unlink (quelle_dat);
					   DEBUG(1,"Datei %s geloescht\n",quelle_dat);
                       sprintf(char_zwi,"Dat=%s in der Waage geloescht",quelle_dat);
                       error = 0;
                       fehler_meldung((long)1,(long)error,char_zwi);
				   }
				   else
                       break;
   TESTEN */
			}

			unlink (metinzwi);
			DEBUG(1,"%s geloescht\n",metinzwi);

		    error = copy (metinzwi,input,FALSE);
		    if (error != 0)
			{
				if (error == 99)
				{
					sprintf (char_zwi,"%s kann nicht ge�ffnet werden",input);
			        fehler_meldung ((long)1, error, char_zwi);
					DEBUG(1,"%s\n",char_zwi);
				}
				if (error == 98)
				{
					sprintf (char_zwi,"%s kann nicht ge�ffnet werden",metinzwi);
			        fehler_meldung ((long)1, error, char_zwi);
					DEBUG(1,"%s\n",char_zwi);
				}
			    error = 61;
			    sprintf(char_zwi,"ERR bei copy %s an die Waage",input);
			    fehler_meldung ((long)1, error, char_zwi);
			}
			error1 = rename (metinzwi,quelle_dat);
		    if (error1 != 0)
			{
			    error = 62;
			    sprintf(char_zwi,"ERR bei rename %s in der Waage",input);
			    fehler_meldung ((long)1, error, char_zwi);
			}

			if (error == 0)
			{
                   sprintf (char_zwi, "Dat=%s gesendet",input);
                   fehler_meldung((long)1,(long)error,char_zwi);
                   DEBUG(9,"%s\n",char_zwi);
            }
		    else
			{
			       DEBUG(1,"get_put=FALSE fuer input=%s\n",input);
                   error = 705;
                   sprintf (char_zwi, "Uebertragungsfehler put: %s",input);
                   fehler_meldung((long)1,(long)error,char_zwi);
                   DEBUG(9,"%s\n",char_zwi);
				   break;
			}


             /* RSP-Datei holen und in der Waage loeschen */
            DEBUG(1,"RSP holen\n");
			sprintf (quelle_dat,"%s\\%s\\%s",home_waage,OUT_verz,output_quit);
			DEBUG(1,"quelle_dat=%s\n",quelle_dat);

            DEBUG(1,"vor HOLEN rspoutput=%s quelle_dat=%s\n",rspoutput,quelle_dat);
		    unlink (rspoutput);
		    DEBUG(1,"unlink %s\n",rspoutput);

			DEBUG(1,"WARTEN: warten=%d, warten_par=%d\n",warten,warten_par);
			printf ("TIMEOUT=%d\n",warten_par);
            merker = 0;
            warten = 0;
            while (warten <= warten_par)  
            {
                 error = copy (rspoutput, quelle_dat, FALSE);
				 if (error == 0)
				 {
			         DEBUG(1,"Datei %s geholt\n",rspoutput);
					 merker = 1;
					 break;
				 }
		         else
				 {
					 warten++;
					 Sleep(1000);
					 if (warten%10 == 0)
					     printf("%d",warten);
					 else
					     printf(".");
				 }
            } /* end of while () */
			printf ("\n");

			if (error == 99)
			{
					sprintf (char_zwi,"%s kann nicht ge�ffnet werden",quelle_dat);
			        fehler_meldung ((long)1, error, char_zwi);
					DEBUG(1,"%s\n",char_zwi);
			}
			if (error == 98)
			{
					sprintf (char_zwi,"%s kann nicht ge�ffnet werden",output);
			        fehler_meldung ((long)1, error, char_zwi);
					DEBUG(1,"%s\n",char_zwi);
			}

			DEBUG(1,"\nmerker=%d\n",merker);
            
			/* Datei RSP.xml in der Waage loeschen */
			if (merker == 1)
			{
				 if (loeschen_arg == 0)
				 {
					DEBUG(1,"Datei %s loeschen\n",quelle_dat);
				    error = unlink (quelle_dat);
		            if (error == 0)
					{
			             DEBUG(1,"%s geloescht\n",quelle_dat);
                         sprintf(char_zwi,"RSP %s geloescht",quelle_dat);
                         DEBUG(9,"%s\n",char_zwi);
                         fehler_meldung((long)1,(long)0,char_zwi);
					}
		            else
					{
			             DEBUG(1,"ERR unlink Dat=%s\n",quelle_dat);
                         error = 707;
                         sprintf(char_zwi,"RSP %s nicht geloescht",quelle_dat);
                         DEBUG(9,"%s\n",char_zwi);
                         fehler_meldung((long)1,(long)error,char_zwi);
						 break;
					}
				 }
				 else
				 {
					 DEBUG(1,"Datei %s nicht geloescht, loeschen_arg=%d\n",quelle_dat,loeschen_arg);
				 }
			}
			else
			{
                    error = 708;
                    sprintf (char_zwi, "Timeout RSP-Holen: %s",quelle_dat);
                    fehler_meldung((long)1,(long)error,char_zwi);
                    DEBUG(9,"\n%s\n",char_zwi);
					/* wegen Synchronisation 19.09.06 FF */
					/******
				    if (loeschen_arg == 0)
					{
						printf ("Warten %d sec und loeschen %s\n",warten_par,quelle_dat);
					    Sleep(warten_par * 1000);
				        error = unlink (quelle_dat);
                    }
					*****/
                    //break;
			}


             /* Status-Datei holen und in der Waage loeschen */
            DEBUG(1,"STATUS holen\n");
			sprintf (quelle_dat,"%s\\%s\\%s",home_waage,OUT_verz,status_dat);
		    DEBUG(1,"quelle_dat=%s\n",quelle_dat);

		    sprintf (metinzwi,"%s\\RSP_staxml.%d",tmppath,sys);
		    DEBUG(1,"metinzwi=%s\n",metinzwi);

		    DEBUG(1,"vor HOLEN metinzwi=%s quelle_dat=%s\n",metinzwi,quelle_dat);
			unlink (metinzwi);
		    DEBUG(1,"unlink %s\n",metinzwi);

			DEBUG(1,"WARTEN: warten_par=%d warten=%d\n",warten_par,warten);
			printf ("TIMEOUT=%d\n",warten_par);
            merker = 0;
			warten = 0;
            while (warten <= warten_par)  
            {
                 error = copy (metinzwi, quelle_dat, FALSE);
				 if (error == 0)
				 {
			         DEBUG(1,"Datei %s geholt\n",output);
					 merker = 1;
					 break;
				 }
		         else
				 {
					 warten++;
					 Sleep(1000);
					 if (warten%10 == 0)
					     printf("%d",warten);
					 else
					     printf(".");
				 }
            } /* end of while () */
		    printf("\n");

			if (error == 99)
			{
					sprintf (char_zwi,"%s kann nicht ge�ffnet werden",quelle_dat);
			        fehler_meldung ((long)1, error, char_zwi);
					DEBUG(1,"%s\n",char_zwi);
			}
			if (error == 98)
			{
					sprintf (char_zwi,"%s kann nicht ge�ffnet werden",metinzwi);
			        fehler_meldung ((long)1, error, char_zwi);
					DEBUG(1,"%s\n",char_zwi);
			}

			DEBUG(1,"\nmerker=%d\n",merker);
            
			/* STATUS.xml in der Waage loeschen */
			if (merker == 1)
			{
				 if (loeschen_arg == 0)
				 {
					DEBUG(1,"Datei %s loeschen\n",quelle_dat);
				    error = unlink (quelle_dat);
		            if (error == 0)
					{
			             DEBUG(1,"%s geloescht\n",quelle_dat);
                         sprintf(char_zwi,"STA %s geloescht",quelle_dat);
                         DEBUG(9,"%s\n",char_zwi);
                         fehler_meldung((long)1,(long)0,char_zwi);
					}
		            else
					{
			             DEBUG(1,"ERR unlink Dat=%s\n",quelle_dat);
                         error = 707;
                         sprintf(char_zwi,"STA %s nicht geloescht",quelle_dat);
                         DEBUG(9,"%s\n",char_zwi);
                         fehler_meldung((long)1,(long)error,char_zwi);
						 break;
					}
				 }
				 else
				 {
					 DEBUG(1,"Datei %s nicht geloescht, loeschen_arg=%d\n",quelle_dat,loeschen_arg);
				 }
			}
			else
			{
                    error = 708;
                    sprintf (char_zwi, "Timeout STATUS-Holen: %s",quelle_dat);
                    fehler_meldung((long)1,(long)error,char_zwi);
                    DEBUG(9,"\n%s\n",char_zwi);
                    break;
			}


			/* STATUS auswerten */
			error = status_auswerten(metinzwi,(int)sys);


	        break;
		} /* end of while (TRUE) */

		if (error == 0)
		{
                   sprintf (char_zwi, "Datei=%s OK gesendet",input);
                   fehler_meldung((long)1,(long)error,char_zwi);
                   DEBUG(9,"%s\n",char_zwi);
		}
		break;

	default:
		sprintf (char_zwi,"Netz=%d ist nicht erlaubt",netz);
		error=751;
		fehler_meldung((long)1,error,char_zwi);
	}
    
	/*****
	if (error == 0)
	{
        sprintf (char_zwi, "Sys=%d: �bertragung OK beendet",sys,input);
        fehler_meldung((long)1,(long)error,char_zwi);
        DEBUG(9,"%s\n",char_zwi);
	}
	*****/

	return error;
} /* end of put_uc () */


/*************************************************************************/
long lb_konvertieren (char * lb_dat, char * lb_zwi, char * metout)
{
	long err=0;

			 DEBUG(1,"lb_konvertieren: lb_dat=%s lb_zwi=%s metout=%s\n",lb_dat, lb_zwi, metout);

             sprintf(char_zwi,"%s\\readxml %s\\ucmet512.if %s\\ucmet512.ifx %s %s",
                     bin,
					 konvert,
					 konvert,
                     lb_zwi,
					 lb_dat);
             DEBUG(9,"%s\n",char_zwi); 
             err = system(char_zwi);
             if (err != 0)
             {
                err = 713;
                sprintf (char_zwi, "ERR readxml %s",lb_dat);
                fehler_meldung ((long)1, err, char_zwi);
                DEBUG(1,"err=%ld %s\n",err,char_zwi);
                return (err);
             }
             sprintf(char_zwi,"%s\\sxascii /br /i%s /o%s %s\\ucmet512.if",
                     bin,
                     lb_zwi, 
                     metout,
					 konvert);
             DEBUG(9,"%s\n",char_zwi); 
             err = system(char_zwi);
             if (err != 0)
             {
                err = 714;
                sprintf (char_zwi, "ERR dtod %s",lb_zwi);
                fehler_meldung ((long)1, err, char_zwi);
                DEBUG(1,"err=%ld %s\n",err,char_zwi);
                //return (err);
             }

	return err;
}

/*************************************************************************/
long get_uc(int sys,int len)
{
	char zwi_dat[64];
	char ls_get[64];
    long error=0;
    char datei[64];
    char lb_dat[64];
    char lb_zwi[64];
	char quelle_dat[64];
    char metout[64];
    char * p;
    FILE * fp;
	int  anz_loe=0;
	int  anz_bearb=0;
	int  anz_gelesen=0;
    WIN32_FIND_DATA fileinfo;
    HINTERNET Dir;
	HINTERNET hInternet;
    HINTERNET hConnect;
    BOOL Ret;
    BOOL ret_get;
    BOOL ret_del;
	
	sprintf (char_zwi,"Sys=%d: Umsatz-Daten holen",sys);
	fehler_meldung ((long)1, error, char_zwi);

	sprintf (char_zwi,"Umsatz-Daten holen, Prozedure nicht aktiv",sys);
	fehler_meldung ((long)1, error, char_zwi);
	return 0;
	

	//18.09.06 FF
	if (strcmp(kennung_arg,"0") == 0)
	{
	    sprintf (lb_dat, "%s\\lb_%d.xml",tmppath,sys);
        sprintf (lb_zwi, "%s\\lb_512.%d",tmppath,sys);
	}
	else
	{
	    sprintf (lb_dat, "%s\\lb%s_%d.xml",tmppath,kennung_arg,sys);
        sprintf (lb_zwi, "%s\\lb%s_512.%d",tmppath,kennung_arg,sys);
	}
    sprintf (metout, "%s",output);
    DEBUG(9,"lb_dat=%s, lb_zwi=%s metout=%s\n",lb_dat,lb_zwi,metout);
    unlink(lb_dat);
    unlink(lb_zwi);
    unlink(metout);

	sprintf (char_zwi,"Sys=%d: Umsatz-Daten holen",sys);
	fehler_meldung ((long)1, error, char_zwi);

	switch (netz)
	{
	case 0:      /****************** FTP ********************************/
        DEBUG(1,"get_uc: sys=%ld len=%d   FTP\n",sys,len);

		/* PING-Pruefen */
		error = (long)ping_pruefen(id);
		if (error != 0)
		{
			sprintf (char_zwi,"Waagen nicht im Netz o. Waagen ausgeschaltet");
		    DEBUG(1,"error=%ld: %s\n",error,char_zwi);
			error = 59;
			fehler_meldung ((long)1, error, char_zwi);
            break;
		}

		/* OPEN */
		//DEBUG(1,"vor InternetOpen\n");
        hInternet = InternetOpen("transluc",
                                 INTERNET_OPEN_TYPE_PRECONFIG ,
                                 NULL,
                                 NULL,
 		                         0);

		if (hInternet == NULL)
		{
			error = 999;
			sprintf (char_zwi,"OPEN-INTERNET false");
		    DEBUG(1,"error=%ld: %s\n",error,char_zwi);
			error = 999;
			fehler_meldung ((long)1, error, char_zwi);
			break;
		}
        else
		    DEBUG(1,"nach InternetOpen OK\n");

        /* Connect */
		hConnect = InternetConnect(hInternet,
                                   id,
 		                           INTERNET_INVALID_PORT_NUMBER,
                                   user,
                                   passwd,
                                   INTERNET_SERVICE_FTP ,
                                   INTERNET_FLAG_PASSIVE ,
			  	                   0);
		if (hConnect == NULL)
		{
			error = 999;
			DEBUG(1,"CONNECT-INTERNET false\n");
			sprintf (char_zwi,"CONNECT-INTERNET false");
		    DEBUG(1,"error=%ld: %s\n",error,char_zwi);
			fehler_meldung ((long)1, error, char_zwi);
			break;
		}
        else
		    DEBUG(1,"nach InternetConnect OK\n");

		sprintf (quelle_dat,"\\%s",TAF_verz);
		DEBUG(1,"vor FtpSetCurrentDirectory: quelle_dat=%s\n",quelle_dat);

        //Ret = FtpSetCurrentDirectory (hConnect,"c:\\taf");
        Ret = FtpSetCurrentDirectory (hConnect,quelle_dat);

		Dir = FtpFindFirstFile(hConnect,
			                   quelle_dat,
                               &fileinfo, 
                               INTERNET_FLAG_RELOAD, 
                               0);

		if (Dir == NULL)
		{
			 DEBUG(1,"Dir=FALSE: fur %s\n",quelle_dat);
             Dir = FtpFindFirstFile(hConnect,
                                    NULL,
                                    &fileinfo, 
                                    INTERNET_FLAG_RELOAD, 
                                     0);
		}
		else
			 DEBUG(1,"Dir=TRUE: fur %s\n",quelle_dat);

        DEBUG(1,"*-------------------------------------- anz_bearb=%d anz_gelesen=%d\n",anz_bearb, anz_gelesen);
        DEBUG(1,"*1:lb_dat=%s\n",lb_dat);
		sprintf (quelle_dat,"%s\\%s\\%s",home_waage,TAF_verz,fileinfo.cFileName);
		DEBUG(1,"*1:quelle_dat=%s\n",quelle_dat);

		/* BEARBEITEN */
        DEBUG(1,"*1:vor FtpGetFile lb_dat=%s quelle_dat=%s\n",lb_dat,quelle_dat);
        ret_get = FtpGetFile(hConnect,
                             quelle_dat,
                             lb_dat,
							 FALSE,
                             FTP_TRANSFER_TYPE_BINARY  | INTERNET_FLAG_RELOAD,
			                 1,
							 0);
		if (ret_get == TRUE)
		{
			DEBUG(1,"1:get_ret=TRUE\n");
            anz_gelesen++;
		}
		else
		{
			DEBUG(1,"1:get_ret=FALSE fuer quelle_dat=%s lb_dat=%s\n",quelle_dat,lb_dat);
		}

		if(ret_get == TRUE)
		{
		     /* Konventieren */
		     DEBUG(1,"1 Bearb: Datei=%s\n",fileinfo.cFileName);
			 DEBUG(1,"KONV: lb_dat=%s lb_zwi=%s metout=%s\n",lb_dat, lb_zwi, metout);
             error = lb_konvertieren(lb_dat,lb_zwi,metout);
             if (error != 0)
             {
                  error = 0;
             }
		     anz_bearb++;

		     /* LOESCHEN */
		     if (loeschen_arg == 0)
                  ret_del = FtpDeleteFile (hConnect, fileinfo.cFileName);
		          if (ret_del == TRUE)
				  {
			          DEBUG(1,"ret_del=TRUE\n");
                      anz_loe++;
				  }
		          else
				  {
			          DEBUG(1,"ret_del=FALSE fuer Dat=%s\n",fileinfo.cFileName);
				  }
		}

        DEBUG(1,"WHILE: anz_bearb=%d anz_gelesen=%d\n",anz_bearb, anz_gelesen);
        while (InternetFindNextFile (Dir, &fileinfo)) 
		{
              /* 15.02.10 Praefix-Kontrolle */
              if (mit_praefix == 1)
              {
		          //DEBUG(1,"Dir-Datei=%s\n",fileinfo.cFileName);
		          /* 27.10.06 Klein/Gross Schreibung */
			      if (strncmp(quelle_dat,fileinfo.cFileName,2) != 0 &&
				      strncmp(quelle_dat1,fileinfo.cFileName,2) != 0)
				  {
		              //sprintf (char_zwi,"Keine Datei %s gefunden",quelle_dat);
		              sprintf (char_zwi,"Datei %s ohne/falschem Praefix",quelle_dat);
                      DEBUG(1,"%s\n",char_zwi);
                      //fehler_meldung((long)1,(long)0,char_zwi);
                      //error = 0;
				      //keine_daten = 1;
			          //break;
					  continue;
				  }
				  else
				  {
                      sprintf (char_zwi, "Datei %s wird bearbeitet",fileinfo.cFileName);
                      DEBUG(1,"%s\n",char_zwi);
				  }
              }
              else
              {
                  /* ohne Praefix */
			      if (quelle_dat,fileinfo.cFileName[0] >= 0x30 &&
					  quelle_dat,fileinfo.cFileName[0] <= 0x39)
				  {
                      sprintf (char_zwi, "Datei %s wird bearbeitet",fileinfo.cFileName);
                      DEBUG(1,"%s\n",char_zwi);
				  }
				  else
				  {
                      sprintf (char_zwi, "Datei %s nicht ohne Praefix",fileinfo.cFileName);
					  DEBUG(1,"%s\n",char_zwi);
					  continue;
				  }
              }
             /* 15.02.10 FF E */

              DEBUG(1,"--------------------------------------- anz_bearb=%d anz_gelesen=%d\n",anz_bearb, anz_gelesen);
		      sprintf (quelle_dat,"%s\\%s\\%s",home_waage,TAF_verz,fileinfo.cFileName);
		      DEBUG(1,"fileinfo.FileNamet=%s\n",fileinfo.cFileName);
			  //sprintf (lb_dat,"%s\\%s",tmppath,fileinfo.cFileName);
		      DEBUG(1,"quelle_dat=%s lb_dat=%s\n",quelle_dat,lb_dat);

			  /* BEARBEITEN */
              DEBUG(1,"unlink lb_dat=ziel=%s\n",lb_dat);
              unlink (lb_dat);
              DEBUG(1,"vor FtpGetFile: quelle=%s ziel=%s\n",quelle_dat,lb_dat);
			  ret_get = FtpGetFile(hConnect,
                                    quelle_dat,
                                    lb_dat,
									FALSE,
                                    FTP_TRANSFER_TYPE_BINARY  | INTERNET_FLAG_RELOAD,
			                        1,
									0);
		      DEBUG(1,"nach FtpGetFile: fileinfo.cFileNamet=%s\n",fileinfo.cFileName);
		      if (ret_get == TRUE)
			  {
			       DEBUG(1,"get_ret=TRUE\n");
                   anz_gelesen++;
			  }
		      else
			  {
			       DEBUG(1,"get_ret=FALSE fuer quelle_dat=%s: continue\n",quelle_dat);
                   continue;
              }

		      if(ret_get == TRUE)
			  {
       			   /* Konventieren */
		           DEBUG(1,"Bearb: QUELLE-Datei=%s\n",fileinfo.cFileName);
			       DEBUG(1,"KONV: lb_dat=%s lb_zwi=%s metout=%s\n",lb_dat, lb_zwi, metout);
                   error = lb_konvertieren(lb_dat,lb_zwi,metout);
                   if (error != 0)
				   {
                        error = 0;
                        continue;
				   }
			       anz_bearb++;

			       /* LOESCHEN */
			       if (loeschen_arg == 0)
				   {
				        ret_del = FtpDeleteFile (hConnect, fileinfo.cFileName);
		                if (ret_del == TRUE)
						{
			                 DEBUG(1,"ret_del=TRUE  %s geloescht\n",fileinfo.cFileName);
                             anz_loe++;
						}
		                else
						{
			                 DEBUG(1,"ret_del=FALSE fuer Dat=%s\n",fileinfo.cFileName);
                             continue;
						}
				   }
			  }
              DEBUG(1,"WHILE: anz_bearb=%d anz_gelesen=%d\n",anz_bearb, anz_gelesen);
		} /* end of while() */

        InternetCloseHandle (Dir);

        InternetCloseHandle (hConnect);
        InternetCloseHandle (hInternet);

		sprintf (char_zwi,"gelesen:    %d Bons/Dateien",anz_gelesen);
        DEBUG(9,"%s\n",char_zwi);
        fehler_meldung((long)1,(long)0,char_zwi);

		sprintf (char_zwi,"bearbeitet: %d Bons/Dateien",anz_bearb);
        DEBUG(9,"%s\n",char_zwi);
        fehler_meldung((long)1,(long)0,char_zwi);

        if (loeschen_arg == 0)
		{
	         sprintf (char_zwi,"geloescht:  %d Bons/Dateien",anz_loe);
             DEBUG(9,"%s\n",char_zwi);
             fehler_meldung((long)1,(long)0,char_zwi);
		}
		else
			DEBUG(1,"keine Loeschung loeschen_arg=%d\n",loeschen_arg);

		/* Endsatz Schreiben */
		DEBUG(1,"vor Endsatz error=%ld\n",error);
        if (error != 0)
		{
             DEBUG(1,"get_uc: error=%ld %s\n",error,char_zwi);
             fehler_meldung ((long)1, error, char_zwi);
		}
        else
		{
             sprintf(char_zwi,"%s\\sxascii /br /o%s %s\\ucmetend.if",
                     bin,
					 metout,
					 konvert);
             DEBUG(9,"%s\n",char_zwi); 
             error = system(char_zwi);
             if (error != 0)
             {
                error = 715;
                sprintf (char_zwi, "ERR dtod %s ENDOFSATZ",lb_zwi);
                fehler_meldung ((long)1, error, char_zwi);
                //error = 0;
             }
		}

		break;

	case 1:      /************** WINDOWS-NETZ ******************/
        DEBUG(1,"get_uc: sys=%ld len=%d   WINDOWS-Netz\n",sys,len);

		/* Pruefen ob Waage im Netz */
		sprintf (zwi_dat,"%s\\null",tmppath);
		sprintf (char_zwi,"dir %s > %s",home_waage,zwi_dat);
		DEBUG(1,"SYS: %s\n",char_zwi);
		error=system(char_zwi);
		if (error != 0)
		{
			sprintf (char_zwi,"Waagen nicht im Netz o. Waagen ausgeschaltet");
		    DEBUG(1,"error=%ld: %s\n",error,char_zwi);
			error = 59;
			fehler_meldung ((long)1, error, char_zwi);
            break;
		}
		unlink (zwi_dat);
		DEBUG(1,"unlink %s\n",zwi_dat);

		/* Dateien Liste */
		sprintf (ls_get,"%s\\%s%ld",tmppath,"ls_get.",sys);
		unlink (ls_get);
		DEBUG(1,"unlink %s\n",ls_get);

		sprintf (char_zwi,"dir /b %s\\%s\\%s > %s",home_waage,TAF_verz,lbdat,ls_get);
		DEBUG(1,"SYS: %s\n",char_zwi);
		error=system(char_zwi);
		if (error != 0)
		{
			sprintf (char_zwi,"Keine Bons/Dateien <%s> in Waagen vorh.",lbdat);
		    DEBUG(1,"error=%ld: %s\n",error,char_zwi);
			fehler_meldung ((long)1, error, char_zwi);
            error = 0;
            break;
		}

		/* Dateien aus der DIR-Liste bearbeiten */
        fp = fopen (ls_get, "r");
        if (fp == NULL)
        {
             sprintf (char_zwi,"OPEN-ERROR %s",ls_get);
             error = 710;
             DEBUG(9,"error=710: %s\n",char_zwi);
			 fehler_meldung ((long)1, error, char_zwi);
             break;
        }
         
        while (TRUE)
        {
             DEBUG(1,"---------------------------------------\n");
			 p = fgets (datei,32,fp);
             if (p == NULL)
             {
                DEBUG(9,"INPUT-Ende\n");
                break;
             }
			 else
				 DEBUG(1,"Datei=%s\n",datei);
             cr_weg(datei);
             DEBUG(9,"Holen: Datei=%s\n",datei);

             /* Holen */
			 sprintf (quelle_dat,"%s\\%s\\%s",home_waage,TAF_verz,datei);
			 unlink(lb_dat);
			 DEBUG(1,"unlink lb_dat=ziel=%s\n",lb_dat);
             DEBUG(9,"HOLEN: Z=%s Q=%s\n",lb_dat,quelle_dat);
             error = (long) copy (lb_dat,quelle_dat, FALSE);           
             if (error != 0)
             {
                error = 712;
                sprintf (char_zwi, "ERR Holen %s",quelle_dat);
                fehler_meldung ((long)1, error, char_zwi);
				DEBUG(1,"%s\n",char_zwi);
                error = 0;
                continue;
             }
             sprintf (char_zwi, "Sys=%d: Holen %s",sys,quelle_dat);
             if (testen)
			      fehler_meldung ((long)1, error, char_zwi);
			 DEBUG(1,"%s\n",char_zwi);
			 anz_gelesen++;

             /* Konvertierung */
			 DEBUG(1,"KONV: lb_dat=%s lb_zwi=%s metout=%s\n",lb_dat, lb_zwi, metout);
             error = lb_konvertieren(lb_dat,lb_zwi,metout);
             if (error != 0)
             {
                error = 0;
                continue;
             }
			 anz_bearb++;

             /* Loeschen */
             if (error == 0 && loeschen_arg == 0)
             {
                sprintf (char_zwi,"Sys=%d: Loeschen %s ",sys,quelle_dat);
                DEBUG(9,"%s\n",char_zwi);
                if (testen) 
					fehler_meldung((long)1,(long)error,char_zwi);
				error=(long)unlink(quelle_dat);
                DEBUG(9,"delete: %s error=%ld\n",quelle_dat,error);
                if (error != 0)
                {
                     sprintf(char_zwi,"%s nicht geloescht",quelle_dat);
                     DEBUG(9,"%s\n",char_zwi);
                     fehler_meldung((long)1,(long)720,char_zwi);
                     error = 0;
                }
				else
				{
					anz_loe++;
					//DEBUG(1,"anz_loe=%d\n",anz_loe);
				}
             }
             else
			 {
				DEBUG(1,"error=%ld loeschen_arg=%d\n",error,loeschen_arg);
                continue;
			 }
        } /* end of while() holen ... */
		
	    sprintf (char_zwi,"gelesen:    %d Bons/Dateien",anz_gelesen);
        DEBUG(9,"%s\n",char_zwi);
        fehler_meldung((long)1,(long)0,char_zwi);

		sprintf (char_zwi,"bearbeitet: %d Bons/Dateien",anz_bearb);
        DEBUG(9,"%s\n",char_zwi);
        fehler_meldung((long)1,(long)0,char_zwi);

        if (loeschen_arg == 0)
		{
	         sprintf (char_zwi,"geloescht:  %d Bons/Dateien",anz_loe);
             DEBUG(9,"%s\n",char_zwi);
             fehler_meldung((long)1,(long)0,char_zwi);
		}

        fclose(fp);
        //unlink(zwi_dat);

		/* Endsatz Schreiben */
		DEBUG(1,"vor Endsatz error=%ld\n",error);
        if (error != 0)
		{
             DEBUG(1,"get_uc: error=%ld %s\n",error,char_zwi);
             fehler_meldung ((long)1, error, char_zwi);
		}
        else
		{
             sprintf(char_zwi,"%s\\sxascii /br /o%s %s\\ucmetend.if",
                     bin,
					 metout,
					 konvert);
             DEBUG(9,"%s\n",char_zwi); 
             error = system(char_zwi);
             if (error != 0)
             {
                error = 715;
                sprintf (char_zwi, "ERR dtod %s ENDOFSATZ",lb_zwi);
                fehler_meldung ((long)1, error, char_zwi);
                //error = 0;
             }
		}

		break;
	default:
		sprintf (char_zwi,"Netz=%d ist nicht erlaubt",netz);
		error=751;
		fehler_meldung((long)1,error,char_zwi);
	}
	if (error == 0)
	{
		if (strcmp(kennung_arg,"0") == 0)
             sprintf (char_zwi, "LB-�bertragung OK beendet");
		else
             sprintf (char_zwi, "%sLB-�bertragung OK beendet",kennung_arg);
        fehler_meldung((long)1,(long)error,char_zwi);
        DEBUG(9,"%s\n",char_zwi);
	}
	return error;
} /* end of get_uc () */

/*************************************************************************/
long get_uc_lb(int sys,int len)
{
	char zwi_dat[64];
	char ls_get[64];
    long error=0;
    int  cp_error=0;
    char datei[64];
    char lb_dat[64];
    char lb_zwi[64];
	char uc_quelle_dat[64];
//	char quelle_dat1[64];              Global
    char metout[64];
    char * p;
    FILE * fp;
	int  anz_loe=0;
	int  anz_bearb=0;
	int  anz_gelesen=0;
	int  keine_daten=0;
    int  anz_kopieren=0;        /* 31.05.10 */
    WIN32_FIND_DATA fileinfo;
    HINTERNET Dir;
	HINTERNET hInternet;
    HINTERNET hConnect;
    BOOL Ret;
    BOOL ret_get;
    BOOL ret_del;
	
	//18.09.06 FF
	if (strcmp(kennung_arg,"0") == 0)
	{
	    sprintf (lb_dat, "%s\\lb_%d.xml",tmppath,sys);
        sprintf (lb_zwi, "%s\\lb_512.%d",tmppath,sys);
    	sprintf (char_zwi,"Sys=%d: LB-Daten holen",sys);
	}
	else
	{
	    sprintf (lb_dat, "%s\\lb%s_%d.xml",tmppath,kennung_arg,sys);
        sprintf (lb_zwi, "%s\\lb%s_512.%d",tmppath,kennung_arg,sys);
		/* 15.02.10 */
    	if (strcmp(kennung_arg,"ALL") == 0)
    	    sprintf (char_zwi,"Sys=%d: *.xml Dateien holen",sys);
        else
		    sprintf (char_zwi,"Sys=%d: %sLB-Daten holen",sys,kennung_arg);
	}
	fehler_meldung ((long)1, error, char_zwi);

    sprintf (metout, "%s",output);
    DEBUG(9,"lb_dat=%s, lb_zwi=%s metout=%s\n",lb_dat,lb_zwi,metout);
    unlink(lb_dat);
    unlink(lb_zwi);
    unlink(metout);

	switch (netz)
	{
	case 0:      /****************** FTP ********************************/
        DEBUG(1,"get_uc_lb: sys=%ld len=%d   FTP\n",sys,len);

		/* PING-Pruefen */
		error = (long)ping_pruefen(id);
		if (error != 0)
		{
			sprintf (char_zwi,"Waagen nicht im Netz o. Waagen ausgeschaltet");
		    DEBUG(1,"error=%ld: %s\n",error,char_zwi);
			error = 59;
			fehler_meldung ((long)1, error, char_zwi);
            break;
		}

		/* OPEN */
		//DEBUG(1,"vor InternetOpen\n");
        hInternet = InternetOpen("transluc",
                                 INTERNET_OPEN_TYPE_PRECONFIG ,
                                 NULL,
                                 NULL,
 		                         0);

		if (hInternet == NULL)
		{
			error = 999;
			sprintf (char_zwi,"OPEN-INTERNET false");
		    DEBUG(1,"error=%ld: %s\n",error,char_zwi);
			error = 999;
			fehler_meldung ((long)1, error, char_zwi);
			break;
		}
        else
		    DEBUG(1,"nach InternetOpen OK\n");

        /* Connect */
		hConnect = InternetConnect(hInternet,
                                   id,
 		                           INTERNET_INVALID_PORT_NUMBER,
                                   user,
                                   passwd,
                                   INTERNET_SERVICE_FTP ,
                                   INTERNET_FLAG_PASSIVE ,
			  	                   0);
		if (hConnect == NULL)
		{
			error = 999;
			DEBUG(1,"CONNECT-INTERNET false\n");
			sprintf (char_zwi,"CONNECT-INTERNET false");
		    DEBUG(1,"error=%ld: %s\n",error,char_zwi);
			fehler_meldung ((long)1, error, char_zwi);
			break;
		}
        else
		    DEBUG(1,"nach InternetConnect OK\n");

		sprintf (uc_quelle_dat,"\\%s",TAF_verz);
		DEBUG(1,"vor FtpSetCurrentDirectory: uc_quelle_dat=%s\n",uc_quelle_dat);

        //Ret = FtpSetCurrentDirectory (hConnect,"c:\\taf");
        Ret = FtpSetCurrentDirectory (hConnect,uc_quelle_dat);

		/* 15.02.10 */
		if (mit_praefix == 1)
		{
            DEBUG(1,"lb_dat=%s\n",lb_dat);
		    sprintf (quelle_dat,"%s",lbdat);
		    /* 27.10.06 Klein/Gross Schreibung */
		    sprintf (quelle_dat1,"%s",lbdat);
            quelle_dat1[0] = quelle_dat1[0] | 0x20;
            quelle_dat1[1] = quelle_dat1[1] | 0x20;
		    DEBUG(1,"0:quelle_dat1=%s\n",quelle_dat1);
		}
		else
		{
		    sprintf (quelle_dat,"*.xml");
		}

		DEBUG(1,"0:quelle_dat=%s\n",quelle_dat);

		Dir = FtpFindFirstFile(hConnect,
			                   quelle_dat,
                               &fileinfo, 
                               INTERNET_FLAG_RELOAD, 
                               0);

		if (Dir == NULL)
		{
			 DEBUG(1,"1Dir=FALSE: fur %s\n",quelle_dat);
             Dir = FtpFindFirstFile(hConnect,
                                    NULL,
                                    &fileinfo, 
                                    INTERNET_FLAG_RELOAD, 
                                     0);
			 //28.09.06 FF A
			 if (Dir == NULL)
			 {
			      DEBUG(1,"2Dir=FALSE: fur %s\n",quelle_dat);
		          sprintf (char_zwi,"Keine Datei %s im %s gefunden",quelle_dat,TAF_verz);
                  DEBUG(1,"%s\n",char_zwi);
                  fehler_meldung((long)1,(long)0,char_zwi);
                  error = 0;
				  keine_daten = 1;
				  //break;
			 }
			 else
			 {
			      DEBUG(1,"2Dir=TRUE: fur %s\n",quelle_dat);
				  DEBUG(1,"Dir-Datei=%s\n",fileinfo.cFileName);
		          /* 27.10.06 Klein/Gross Schreibung */
				  if (strncmp(quelle_dat,fileinfo.cFileName,2) != 0 &&
					  strncmp(quelle_dat1,fileinfo.cFileName,2) != 0)
				  {
		              sprintf (char_zwi,"Keine Datei %s im %s gefunden",quelle_dat,TAF_verz);
		              //sprintf (char_zwi,"Keine Datei %s gefunden",quelle_dat);
                      DEBUG(1,"%s\n",char_zwi);
                      fehler_meldung((long)1,(long)0,char_zwi);
                      error = 0;
				      keine_daten = 1;
					  //break;
				  }

			 }
			 //28.09.06 FF E
		}
		else
		{
			 DEBUG(1,"1Dir=TRUE: fur %s\n",quelle_dat);
             /* 15.02.10 FF A */
             if (mit_praefix == 1)
             {
		          DEBUG(1,"Dir-Datei=%s\n",fileinfo.cFileName);
		          /* 27.10.06 Klein/Gross Schreibung */
			      if (strncmp(quelle_dat,fileinfo.cFileName,2) != 0 &&
				      strncmp(quelle_dat1,fileinfo.cFileName,2) != 0)
				  {
		              //sprintf (char_zwi,"Keine Datei %s gefunden",quelle_dat);
		              sprintf (char_zwi,"Keine Datei %s im %s gefunden",quelle_dat,TAF_verz);
                      DEBUG(1,"%s\n",char_zwi);
                      fehler_meldung((long)1,(long)0,char_zwi);
                      error = 0;
				      keine_daten = 1;
			          //break;
				  }
             }
             else
             {
                  /* ohne Praefix */
			      if (quelle_dat,fileinfo.cFileName[0] >= 0x30 &&
					  quelle_dat,fileinfo.cFileName[0] <= 0x39)
				  {
                      sprintf (char_zwi, "Datei %s gefunden",fileinfo.cFileName);
                      DEBUG(1,"%s\n",char_zwi);
				  }
				  else
				  {
                      sprintf (char_zwi, "Datei %s nicht ohne Praefix",fileinfo.cFileName);
					  DEBUG(1,"%s\n",char_zwi);
				      keine_daten = 1;
				  }
             }
             /* 15.02.10 FF E */
		}

    DEBUG(1,"keine_daten=%d\n",keine_daten);
	if (keine_daten == 0)
    {  /* 28.09.06 WICHTIG FF */
        DEBUG(1,"--------------------------------------- anz_bearb=%d anz_gelesen=%d\n",anz_bearb, anz_gelesen);
        DEBUG(1,"lb_dat=%s\n",lb_dat);
		sprintf (uc_quelle_dat,"%s\\%s\\%s",home_waage,TAF_verz,fileinfo.cFileName);
		DEBUG(1,"1:uc_quelle_dat=%s\n",quelle_dat);

		/* BEARBEITEN */
        DEBUG(1,"1:vor FtpGetFile lb_dat=%s quelle_dat=%s\n",lb_dat,uc_quelle_dat);
        ret_get = FtpGetFile(hConnect,
                             uc_quelle_dat,
                             lb_dat,
							 FALSE,
                             FTP_TRANSFER_TYPE_BINARY  | INTERNET_FLAG_RELOAD,
			                 1,
							 0);
		if (ret_get == TRUE)
		{
			DEBUG(1,"1:get_ret=TRUE\n");
            anz_gelesen++;
		}
		else
		{
			DEBUG(1,"1:get_ret=FALSE fuer uc_quelle_dat=%s lb_dat=%s\n",uc_quelle_dat,lb_dat);
		}

		if(ret_get == TRUE)
		{
		     DEBUG(1,"1 Bearb: Datei=%s\n",fileinfo.cFileName);
			 // 31.05.10 FF
			 if (kopieren)
			 {
				 sprintf (UCziel,"%s\\%s", sichern, fileinfo.cFileName);
				 DEBUG (1,"\ncopy quelle: %s ziel: %s\n",lb_dat, UCziel);
                 //error = (long) copy (UCziel, lb_dat, FALSE);
                 cp_error = (int) CopyFile (lb_dat, UCziel, FALSE);
				 DEBUG (1,"cp_error=%d\n",cp_error);
				 if (cp_error != 0)
					 anz_kopieren++;
			 }

			 /* Konventieren */
			 DEBUG(1,"KONV: lb_dat=%s lb_zwi=%s metout=%s\n",lb_dat, lb_zwi, metout);
             error = lb_konvertieren(lb_dat,lb_zwi,metout);
             if (error != 0)
             {
                  error = 0;
             }
		     anz_bearb++;

		     /* LOESCHEN */
		     if (loeschen_arg == 0)
                  ret_del = FtpDeleteFile (hConnect, fileinfo.cFileName);
		          if (ret_del == TRUE)
				  {
			          DEBUG(1,"ret_del=TRUE\n");
                      anz_loe++;
				  }
		          else
				  {
			          DEBUG(1,"ret_del=FALSE fuer Dat=%s\n",fileinfo.cFileName);
				  }
		}

        DEBUG(1,"WHILE: anz_bearb=%d anz_gelesen=%d\n",anz_bearb, anz_gelesen);
        while (InternetFindNextFile (Dir, &fileinfo)) 
		{
			  DEBUG(1,"--------------------------------------- anz_bearb=%d anz_gelesen=%d\n",anz_bearb, anz_gelesen);
		      DEBUG(1,"fileinfo.FileNamet=%s\n",fileinfo.cFileName);
              /* 15.02.10 Praefix-Kontrolle */
              if (mit_praefix == 1)
              {
		          //DEBUG(1,"Dir-Datei=%s\n",fileinfo.cFileName);
		          /* 27.10.06 Klein/Gross Schreibung */
			      if (strncmp(quelle_dat,fileinfo.cFileName,2) != 0 &&
				      strncmp(quelle_dat1,fileinfo.cFileName,2) != 0)
				  {
		              //sprintf (char_zwi,"Keine Datei %s gefunden",quelle_dat);
		              sprintf (char_zwi,"Datei %s ohne/mit falschem Praefix",quelle_dat);
                      DEBUG(1,"%s\n",char_zwi);
                      //fehler_meldung((long)1,(long)0,char_zwi);
                      //error = 0;
				      //keine_daten = 1;
			          //break;
					  continue;
				  }
				  else
				  {
                      sprintf (char_zwi, "Datei %s wird bearbeitet",fileinfo.cFileName);
                      DEBUG(1,"%s\n",char_zwi);
				  }
              }
              else
              {
                  /* ohne Praefix */
			      if (quelle_dat,fileinfo.cFileName[0] >= 0x30 &&
					  quelle_dat,fileinfo.cFileName[0] <= 0x39)
				  {
                      sprintf (char_zwi, "Datei %s wird bearbeitet",fileinfo.cFileName);
                      DEBUG(1,"%s\n",char_zwi);
				  }
				  else
				  {
                      sprintf (char_zwi, "Datei %s nicht ohne Praefix",fileinfo.cFileName);
					  DEBUG(1,"%s\n",char_zwi);
					  continue;
				  }
              }
              /* 15.02.10 FF E */

		      sprintf (uc_quelle_dat,"%s\\%s\\%s",home_waage,TAF_verz,fileinfo.cFileName);
			  //sprintf (lb_dat,"%s\\%s",tmppath,fileinfo.cFileName);
		      DEBUG(1,"uc_quelle_dat=%s lb_dat=%s\n",uc_quelle_dat,lb_dat);

			  /* BEARBEITEN */
              DEBUG(1,"unlink lb_dat=ziel=%s\n",lb_dat);
              unlink (lb_dat);
              DEBUG(1,"vor FtpGetFile: quelle=%s ziel=%s\n",uc_quelle_dat,lb_dat);
			  ret_get = FtpGetFile(hConnect,
                                    uc_quelle_dat,
                                    lb_dat,
									FALSE,
                                    FTP_TRANSFER_TYPE_BINARY  | INTERNET_FLAG_RELOAD,
			                        1,
									0);
		      DEBUG(1,"nach FtpGetFile: fileinfo.cFileNamet=%s\n",fileinfo.cFileName);
		      if (ret_get == TRUE)
			  {
			       DEBUG(1,"get_ret=TRUE\n");
                   anz_gelesen++;
			  }
		      else
			  {
			       DEBUG(1,"get_ret=FALSE fuer uc_quelle_dat=%s: continue\n",uc_quelle_dat);
                   continue;
              }

		      if(ret_get == TRUE)
			  {
       			   /* Konventieren */
		           DEBUG(1,"Bearb: QUELLE-Datei=%s\n",fileinfo.cFileName);
			       DEBUG(1,"KONV: lb_dat=%s lb_zwi=%s metout=%s\n",lb_dat, lb_zwi, metout);
                   error = lb_konvertieren(lb_dat,lb_zwi,metout);
                   if (error != 0)
				   {
                        error = 0;
                        continue;
				   }
			       anz_bearb++;
       			   // 31.05.10 FF
			       if (kopieren)
				   {
				         sprintf (UCziel,"%s\\%s", sichern, fileinfo.cFileName);
				         DEBUG (1,"\ncopy quelle: %s ziel: %s\n",lb_dat, UCziel);
                         //error = (long) copy (UCziel, lb_dat, FALSE);           
                         cp_error = (int) CopyFile (lb_dat, UCziel, FALSE);
						 DEBUG (1,"cp_error=%d\n",cp_error);
						 if (cp_error != 0)
							 anz_kopieren++;
				   }

			       /* LOESCHEN */
			       if (loeschen_arg == 0)
				   {
				        ret_del = FtpDeleteFile (hConnect, fileinfo.cFileName);
		                if (ret_del == TRUE)
						{
			                 DEBUG(1,"ret_del=TRUE  %s geloescht\n",fileinfo.cFileName);
                             anz_loe++;
						}
		                else
						{
			                 DEBUG(1,"ret_del=FALSE fuer Dat=%s\n",fileinfo.cFileName);
                             continue;
						}
				   }
			  }
        DEBUG(1,"WHILE: anz_bearb=%d anz_gelesen=%d\n",anz_bearb, anz_gelesen);
		} /* end of while() */
    }  /* 28.09.06 WICHTIG FF end of if (keine_daten == 0) */

		sprintf (char_zwi,"gelesen:    %d Bons/Dateien",anz_gelesen);
        DEBUG(9,"%s\n",char_zwi);
        fehler_meldung((long)1,(long)0,char_zwi);

		sprintf (char_zwi,"bearbeitet: %d Bons/Dateien",anz_bearb);
        DEBUG(9,"%s\n",char_zwi);
        fehler_meldung((long)1,(long)0,char_zwi);

        if (loeschen_arg == 0)
		{
	         sprintf (char_zwi,"geloescht:  %d Bons/Dateien",anz_loe);
             DEBUG(9,"%s\n",char_zwi);
             fehler_meldung((long)1,(long)0,char_zwi);
		}
		else
			DEBUG(1,"keine Loeschung loeschen_arg=%d\n",loeschen_arg);

		if (kopieren)
		{
		    sprintf (char_zwi, "kopiert:    %d Bons/Dateien",anz_kopieren);
            DEBUG(9,"%s\n",char_zwi);
            fehler_meldung((long)1,(long)0,char_zwi);
		}

        InternetCloseHandle (Dir);

        InternetCloseHandle (hConnect);
        InternetCloseHandle (hInternet);

		/* Endsatz Schreiben */
		DEBUG(1,"vor Endsatz error=%ld\n",error);
        if (error != 0)
		{
             DEBUG(1,"get_uc_lb: error=%ld %s\n",error,char_zwi);
             fehler_meldung ((long)1, error, char_zwi);
		}
        else
		{
             sprintf(char_zwi,"%s\\sxascii /br /o%s %s\\ucmetend.if",
                     bin,
					 metout,
					 konvert);
             DEBUG(9,"%s\n",char_zwi); 
             error = system(char_zwi);
             if (error != 0)
             {
                error = 715;
                sprintf (char_zwi, "ERR dtod %s ENDOFSATZ",lb_zwi);
                fehler_meldung ((long)1, error, char_zwi);
                //error = 0;
             }
		}

		break;

	case 1:      /************** WINDOWS-NETZ ******************/
        DEBUG(1,"get_uc_lb: sys=%ld len=%d   WINDOWS-Netz\n",sys,len);

		/* Pruefen ob Waage im Netz */
		sprintf (zwi_dat,"%s\\null",tmppath);
		sprintf (char_zwi,"dir %s > %s",home_waage,zwi_dat);
		DEBUG(1,"SYS: %s\n",char_zwi);
		error=system(char_zwi);
		if (error != 0)
		{
			sprintf (char_zwi,"Waagen nicht im Netz o. Waagen ausgeschaltet");
		    DEBUG(1,"error=%ld: %s\n",error,char_zwi);
			error = 59;
			fehler_meldung ((long)1, error, char_zwi);
            break;
		}
		unlink (zwi_dat);
		DEBUG(1,"unlink %s\n",zwi_dat);

		/* Dateien Liste */
		sprintf (ls_get,"%s\\%s%ld",tmppath,"ls_get.",sys);
		unlink (ls_get);
		DEBUG(1,"unlink %s\n",ls_get);

		/* 15.02.10 */
		if (mit_praefix == 0)
		{
		    sprintf (lbdat,"*.xml");
		}

		//sprintf (char_zwi,"dir /b %s\\%s\\%s > %s",home_waage,TAF_verz,lbdat,ls_get);
		sprintf (char_zwi,"dir /b %s\\%s\\%s > %s",home_waage,TAF_verz,lbdat,ls_get);
		DEBUG(1,"SYS: %s\n",char_zwi);
		error=system(char_zwi);
		if (error != 0)
		{
			sprintf (char_zwi,"Keine Bons/Dateien <%s> in Waagen vorh.",lbdat);
		    DEBUG(1,"error=%ld: %s\n",error,char_zwi);
			fehler_meldung ((long)1, error, char_zwi);
            error = 0;
            break;
		}

		/* Dateien aus der DIR-Liste bearbeiten */
        fp = fopen (ls_get, "r");
        if (fp == NULL)
        {
             sprintf (char_zwi,"OPEN-ERROR %s",ls_get);
             error = 710;
             DEBUG(9,"error=710: %s\n",char_zwi);
			 fehler_meldung ((long)1, error, char_zwi);
             break;
        }
         
        while (TRUE)
        {
             DEBUG(1,"---------------------------------------\n");
			 p = fgets (datei,32,fp);
             if (p == NULL)
             {
                DEBUG(9,"INPUT-Ende\n");
                break;
             }
			 else
				 DEBUG(1,"Datei=%s, lbdat=%s\n",datei, lbdat);

             cr_weg(datei);
              /* 15.02.10 Praefix-Kontrolle */
              if (mit_praefix == 1)
              {
		          /* 27.10.06 Klein/Gross Schreibung */
			      if (strncmp(lbdat,datei,2) != 0 &&
				      strncmp(lbdat,datei,2) != 0)
				  {
		              //sprintf (char_zwi,"Keine Datei %s gefunden",quelle_dat);
		              sprintf (char_zwi,"Datei %s ohne/ mit falschem Praefix",quelle_dat);
                      DEBUG(1,"%s\n",char_zwi);
					  continue;
				  }
				  else
				  {
                      sprintf (char_zwi, "Datei %s wird bearbeitet",datei);
                      DEBUG(1,"%s\n",char_zwi);
				  }
              }
              else
              {
                  /* ohne Praefix */
			      if (datei[0] >= 0x30 &&
					  datei[0] <= 0x39)
				  {
                      sprintf (char_zwi, "Datei %s wird bearbeitet",datei);
                      DEBUG(1,"%s\n",char_zwi);
				  }
				  else
				  {
                      sprintf (char_zwi, "Datei %s nicht ohne Praefix",datei);
					  DEBUG(1,"%s\n",char_zwi);
					  continue;
				  }
              }
              /* 15.02.10 FF E */


             DEBUG(9,"Holen: Datei=%s\n",datei);

             /* Holen */
			 sprintf (uc_quelle_dat,"%s\\%s\\%s",home_waage,TAF_verz,datei);
			 unlink(lb_dat);
			 DEBUG(1,"unlink lb_dat=ziel=%s\n",lb_dat);
             DEBUG(9,"HOLEN: Z=%s Q=%s\n",lb_dat,uc_quelle_dat);
             error = (long) copy (lb_dat,uc_quelle_dat, FALSE);           
             if (error != 0)
             {
                error = 712;
                sprintf (char_zwi, "ERR Holen %s",uc_quelle_dat);
                fehler_meldung ((long)1, error, char_zwi);
				DEBUG(1,"%s\n",char_zwi);
                error = 0;
                continue;
             }
			 // 31.05.10 FF
			 if (kopieren)
			 {
				 sprintf (UCziel,"%s\\%s", sichern, datei);
				 DEBUG (1,"SICHERN  %s\n", UCziel);
                 //cp_error = (int) copy (UCziel, uc_quelle_dat,  FALSE);           
                 cp_error = (int) CopyFile (uc_quelle_dat, UCziel, FALSE);
				 DEBUG (1,"cp_error=%d\n",cp_error);
				 if (cp_error != 0)
					 anz_kopieren++;
			 }

             sprintf (char_zwi, "Sys=%d: Holen %s",sys,uc_quelle_dat);
             if (testen)
			      fehler_meldung ((long)1, error, char_zwi);
			 DEBUG(1,"%s\n",char_zwi);
			 anz_gelesen++;

             /* Konvertierung */
			 DEBUG(1,"KONV: lb_dat=%s lb_zwi=%s metout=%s\n",lb_dat, lb_zwi, metout);
             error = lb_konvertieren(lb_dat,lb_zwi,metout);
             if (error != 0)
             {
                error = 0;
                continue;
             }
			 anz_bearb++;

             /* Loeschen */
             if (error == 0 && loeschen_arg == 0)
             {
                sprintf (char_zwi,"Sys=%d: Loeschen %s ",sys,uc_quelle_dat);
                DEBUG(9,"%s\n",char_zwi);
                if (testen) 
					fehler_meldung((long)1,(long)error,char_zwi);
				error=(long)unlink(uc_quelle_dat);
                DEBUG(9,"delete: %s error=%ld\n",uc_quelle_dat,error);
                if (error != 0)
                {
                     sprintf(char_zwi,"%s nicht geloescht",uc_quelle_dat);
                     DEBUG(9,"%s\n",char_zwi);
                     fehler_meldung((long)1,(long)720,char_zwi);
                     error = 0;
                }
				else
				{
					anz_loe++;
					//DEBUG(1,"anz_loe=%d\n",anz_loe);
				}
             }
             else
			 {
				DEBUG(1,"error=%ld loeschen_arg=%d\n",error,loeschen_arg);
                continue;
			 }
        } /* end of while() holen ... */
		
	    sprintf (char_zwi,"gelesen:    %d Bons/Dateien",anz_gelesen);
        DEBUG(9,"%s\n",char_zwi);
        fehler_meldung((long)1,(long)0,char_zwi);

		sprintf (char_zwi,"bearbeitet: %d Bons/Dateien",anz_bearb);
        DEBUG(9,"%s\n",char_zwi);
        fehler_meldung((long)1,(long)0,char_zwi);

        if (loeschen_arg == 0)
		{
	         sprintf (char_zwi,"geloescht:  %d Bons/Dateien",anz_loe);
             DEBUG(9,"%s\n",char_zwi);
             fehler_meldung((long)1,(long)0,char_zwi);
		}
	    if (kopieren)
		{
	         sprintf (char_zwi,"kopiert:    %d Bons/Dateien",anz_kopieren);
             DEBUG(9,"%s\n",char_zwi);
             fehler_meldung((long)1,(long)0,char_zwi);
		}

        fclose(fp);
        //unlink(zwi_dat);

		/* Endsatz Schreiben */
		DEBUG(1,"vor Endsatz error=%ld\n",error);
        if (error != 0)
		{
             DEBUG(1,"get_uc_lb: error=%ld %s\n",error,char_zwi);
             fehler_meldung ((long)1, error, char_zwi);
		}
        else
		{
             sprintf(char_zwi,"%s\\sxascii /br /o%s %s\\ucmetend.if",
                     bin,
					 metout,
					 konvert);
             DEBUG(9,"%s\n",char_zwi); 
             error = system(char_zwi);
             if (error != 0)
             {
                error = 715;
                sprintf (char_zwi, "ERR dtod %s ENDOFSATZ",lb_zwi);
                fehler_meldung ((long)1, error, char_zwi);
                //error = 0;
             }
		}

		break;


	default:
		sprintf (char_zwi,"Netz=%d ist nicht erlaubt",netz);
		error=751;
		fehler_meldung((long)1,error,char_zwi);

	}  /* end of switch (netz) */

	if (error == 0)
	{
		if (strcmp(kennung_arg,"0") == 0)
             sprintf (char_zwi, "LB-�bertragung OK beendet");
		else
		{
			/* 15.02.10 */
			if (strcmp(kennung_arg,"ALL") == 0)
                sprintf (char_zwi, "�bertragung OK beendet");
			else
                sprintf (char_zwi, "%sLB-�bertragung OK beendet",kennung_arg);
		}
		fehler_meldung((long)1,(long)error,char_zwi);
        DEBUG(9,"%s\n",char_zwi);
	}

	return error;
} /* end of get_uc_lb () */

/*************************************************************************/
long reset(int sys,int len, short intern)
/* 02.10.06 intern = 1 aufruf von put_uc() f�r interne RESET */
{
	short merker=0;
	int warten=0;
    long error=0;
    long error1=0;
	char zwi_dat[64];
    char quit_dat[64];
	char quelle_dat[64];
    char metinzwi[64];
//    char metinquelle[64];
//    char * p;
//    FILE * fp;
//	int  anz_loe=0;
//	int  anz_bearb=0;
//	int  anz_gelesen=0;
//  WIN32_FIND_DATA fileinfo;
    HINTERNET Dir;
	HINTERNET hInternet;
    HINTERNET hConnect;
//    BOOL Ret;
    BOOL ret_get;
    BOOL ret_del;
	
    if (intern == 0)
        sprintf (char_zwi,"Sys=%d: Reset der Waage",sys);
	else
        sprintf (char_zwi,"Intern Reset der Waage");
	fehler_meldung ((long)1, error, char_zwi);


switch (netz)
{
case 0:      /****************** FTP ********************************/
        DEBUG(1,"reset: sys=%ld len=%d   FTP\n",sys,len);
    if (intern == 0)
	{/* 02.10.06 FF */
		/* PING-Pruefen */
		error = (long)ping_pruefen(id);
		if (error != 0)
		{
			sprintf (char_zwi,"Waagen nicht im Netz o. Waagen ausgeschaltet");
		    DEBUG(1,"error=%ld: %s\n",error,char_zwi);
			error = 59;
			fehler_meldung ((long)1, error, char_zwi);
            break;
		}

		/* OPEN */
		//DEBUG(1,"vor InternetOpen\n");
        hInternet = InternetOpen("transluc",
                                 INTERNET_OPEN_TYPE_PRECONFIG ,
                                 NULL,
                                 NULL,
 		                         0);

		if (hInternet == NULL)
		{
			error = 999;
			sprintf (char_zwi,"OPEN-INTERNET false");
		    DEBUG(1,"error=%ld: %s\n",error,char_zwi);
			error = 999;
			fehler_meldung ((long)1, error, char_zwi);
			break;
		}
        else
		    DEBUG(1,"nach InternetOpen OK\n");

        /* Connect */
		hConnect = InternetConnect(hInternet,
                                   id,
 		                           INTERNET_INVALID_PORT_NUMBER,
                                   user,
                                   passwd,
                                   INTERNET_SERVICE_FTP ,
                                   INTERNET_FLAG_PASSIVE ,
			  	                   0);
		if (hConnect == NULL)
		{
			error = 999;
			DEBUG(1,"CONNECT-INTERNET false\n");
			sprintf (char_zwi,"CONNECT-INTERNET false");
		    DEBUG(1,"error=%ld: %s\n",error,char_zwi);
			fehler_meldung ((long)1, error, char_zwi);
			break;
		}
        else
		    DEBUG(1,"nach InternetConnect OK\n");
	} /* end of if (intern == 0) */

    DEBUG(1,"input=%s\n",input);
	sprintf (quelle_dat,"%s\\%s\\%s",home_waage,IN_verz,input_quit);
	DEBUG(1,"1:quelle_dat=%s\n",quelle_dat);
	sprintf (quit_dat,"%s\\%s\\%s",home_waage,OUT_verz,output_quit);
	DEBUG(1,"1:quit_dat=%s\n",quit_dat);
	//sprintf (metinquelle,"%s\\%s",tmppath,input_quit);
	//DEBUG(1,"1:metinquelle=%s\n",metinquelle);

	/* BEARBEITEN */

	/* �bertragung */
	while (TRUE)
	{
			 /* �bertragung Pruefen und Status auswerten */
			 DEBUG(1,"RSP Holen\n");

	 
             /* RSP-Datei holen und in der Waage loeschen */
			 sprintf (quelle_dat,"%s\\%s\\%s",home_waage,OUT_verz,output_quit);
			 //sprintf (quelle_dat,"%s",output_quit);
		     DEBUG(1,"quelle_dat=%s\n",quelle_dat);

             DEBUG(1,"vor FtpGetFile output=%s quelle_dat=%s\n",rspoutput,quelle_dat);
			 unlink (rspoutput);
		     DEBUG(1,"unlink %s\n",rspoutput);

			 printf ("TIMEOUT=%d\n",2);
             merker = 0;
             warten = 0;
             while (warten <= 2)  
             {
                 ret_get = FtpGetFile(hConnect,
                             quelle_dat,
                             rspoutput,
							 FALSE,
                             FTP_TRANSFER_TYPE_BINARY  | INTERNET_FLAG_RELOAD,
			                 1,
							 0);
		         if (ret_get == TRUE)
				 {
			         DEBUG(1,"get_ret=TRUE, Datei %s geholt\n",rspoutput);
					 merker = 1;
					 break;
				 }
		         else
				 {
					 warten++;
					 Sleep(1000);
					 if (warten%10 == 0)
					     printf("%d",warten);
					 else
					     printf(".");
				 }
             }
			 printf ("\n");
			 DEBUG(1,"merker=%d\n",merker);
             /* Datei RSP.xml in der Waage loeschen */
			 if (merker == 1)
			 {
				 if (loeschen_arg == 0)
				 {
					DEBUG(1,"Datei %s loeschen\n",quelle_dat);
				    ret_del = FtpDeleteFile (hConnect, quelle_dat);
		            if (ret_del == TRUE)
					{
			             DEBUG(1,"ret_del=TRUE  %s geloescht\n",quelle_dat);
                         sprintf(char_zwi,"RSP %s geloescht",quelle_dat);
                         DEBUG(9,"%s\n",char_zwi);
                         fehler_meldung((long)1,(long)0,char_zwi);
					}
		            else
					{
			             DEBUG(1,"ret_del=FALSE fuer Dat=%s\n",quelle_dat);
                         error = 707;
                         sprintf(char_zwi,"RSP %s nicht geloescht",quelle_dat);
                         DEBUG(9,"%s\n",char_zwi);
                         fehler_meldung((long)1,(long)error,char_zwi);
						 break;
					}
				 }
				 else
				 {
					 DEBUG(1,"Datei %s nicht geloescht, loeschen_arg=%d\n",quelle_dat,loeschen_arg);
				 }
			 }
			 else
			 {
                    error = 0;
                    sprintf (char_zwi, "RSP-Datei: %s nicht vorhanden",quelle_dat);
                    fehler_meldung((long)1,(long)error,char_zwi);
                    DEBUG(9,"\n%s\n",char_zwi);
			 }

             /* STATUS-Datei holen, auswerten und in der Waage loeschen */
			 DEBUG(1,"STATUS Holen\n");
			 sprintf (quelle_dat,"%s\\%s\\%s",home_waage,OUT_verz,status_dat);
		     DEBUG(1,"quelle_dat=%s\n",quelle_dat);

		     //sprintf (metinzwi,"%s\\metout_status.%d",tmppath,sys);
		     sprintf (metinzwi,"%s\\RSP_staxml.%d",tmppath,sys);
		     DEBUG(1,"metinzwi=%s\n",metinzwi);

		     DEBUG(1,"vor FtpGetFile metinzwi=%s quelle_dat=%s\n",metinzwi,quelle_dat);
			 unlink (metinzwi);
		     DEBUG(1,"unlink %s\n",metinzwi);

			 printf ("TIMEOUT=%d\n",2);
             merker = 0;
			 warten = 0;
             while (warten <= 2)  
             {
                 ret_get = FtpGetFile(hConnect,
                             quelle_dat,
                             metinzwi,
							 FALSE,
                             FTP_TRANSFER_TYPE_BINARY  | INTERNET_FLAG_RELOAD,
			                 1,
							 0);
		         if (ret_get == TRUE)
				 {
			         DEBUG(1,"get_ret=TRUE\n");
					 merker = 1;
					 break;
				 }
		         else
				 {
					 warten++;
					 Sleep(1000);
			         //DEBUG(1,"get_ret=FALSE fuer quelle_dat=%s output=%s\n",quelle_dat,output);
					 if (warten%10 == 0)
					     printf("%d",warten);
					 else
					     printf(".");
				 }
             }
			 printf ("\n");
			 DEBUG(1,"merker=%d\n",merker);
             /* Status-Datei .xml in der Waage loeschen */
			 if (merker == 1)
			 {
				 if (loeschen_arg == 0)
				 {
					DEBUG(1,"Datei %s loeschen\n",quelle_dat);
				    ret_del = FtpDeleteFile (hConnect, quelle_dat);
		            if (ret_del == TRUE)
					{
			             DEBUG(1,"ret_del=TRUE  %s geloescht\n",quelle_dat);
                         sprintf(char_zwi,"STA %s geloescht",quelle_dat);
                         DEBUG(9,"%s\n",char_zwi);
                         fehler_meldung((long)1,(long)0,char_zwi);
					}
		            else
					{
			             DEBUG(1,"ret_del=FALSE fuer Dat=%s\n",quelle_dat);
                         error = 707;
                         sprintf(char_zwi,"STA %s nicht geloescht",quelle_dat);
                         DEBUG(9,"%s\n",char_zwi);
                         fehler_meldung((long)1,(long)error,char_zwi);
						 break;
					}
				 }
				 else
				 {
					 DEBUG(1,"Datei %s nicht geloescht, loeschen_arg=%d\n",quelle_dat,loeschen_arg);
				 }
			 }
			 else
			 {
                    error = 0;
                    sprintf (char_zwi, "STA-Datei: %s nicht vorhanden",quelle_dat);
                    fehler_meldung((long)1,(long)error,char_zwi);
                    DEBUG(9,"\n%s\n",char_zwi);
                    break;
			 }
			 break;

	} /* end of while() */

    if (intern == 0)
	{/* 02.10.06 FF */
        InternetCloseHandle (Dir);

        InternetCloseHandle (hConnect);
        InternetCloseHandle (hInternet);
	}
	break;

case 1:      /************** WINDOWS-NETZ ******************/
        DEBUG(1,"reset: sys=%ld len=%d   WINDOWS-Netz\n",sys,len);

		/* Pruefen ob Waage im Netz */
		sprintf (zwi_dat,"%s\\null",tmppath);
		sprintf (char_zwi,"dir %s > %s",home_waage,zwi_dat);
		DEBUG(1,"SYS: %s\n",char_zwi);
		error=system(char_zwi);
		if (error != 0)
		{
			sprintf (char_zwi,"Waagen nicht im Netz o. Waagen ausgeschaltet");
		    DEBUG(1,"error=%ld: %s\n",error,char_zwi);
			error = 59;
			fehler_meldung ((long)1, error, char_zwi);
            break;
		}

		/* BEARBEITEN */


		while (TRUE)
		{
             /* RSP-Datei holen und in der Waage loeschen */
            DEBUG(1,"RSP holen\n");
			sprintf (quelle_dat,"%s\\%s\\%s",home_waage,OUT_verz,output_quit);
			DEBUG(1,"quelle_dat=%s\n",quelle_dat);

            DEBUG(1,"vor HOLEN rspoutput=%s quelle_dat=%s\n",rspoutput,quelle_dat);
		    unlink (rspoutput);
		    DEBUG(1,"unlink %s\n",rspoutput);

			printf ("TIMEOUT=%d\n",2);
            merker = 0;
            warten = 0;
            while (warten <= 2)  
            {
                 error = copy (rspoutput, quelle_dat, FALSE);
				 if (error == 0)
				 {
			         DEBUG(1,"Datei %s geholt\n",rspoutput);
					 merker = 1;
					 break;
				 }
		         else
				 {
					 warten++;
					 Sleep(1000);
					 if (warten%10 == 0)
					     printf("%d",warten);
					 else
					     printf(".");
				 }
            } /* end of while () */
			printf ("\n");

			if (error == 99)
			{
					sprintf (char_zwi,"%s kann nicht ge�ffnet werden",quelle_dat);
			        fehler_meldung ((long)1, error, char_zwi);
					DEBUG(1,"%s\n",char_zwi);
			}
			if (error == 98)
			{
					sprintf (char_zwi,"%s kann nicht ge�ffnet werden",output);
			        fehler_meldung ((long)1, error, char_zwi);
					DEBUG(1,"%s\n",char_zwi);
			}

			DEBUG(1,"\nmerker=%d\n",merker);
            
			/* Datei RSP.xml in der Waage loeschen */
			if (merker == 1)
			{
				 if (loeschen_arg == 0)
				 {
					DEBUG(1,"Datei %s loeschen\n",quelle_dat);
				    error = unlink (quelle_dat);
		            if (error == 0)
					{
			             DEBUG(1,"%s geloescht\n",quelle_dat);
                         sprintf(char_zwi,"RSP %s geloescht",quelle_dat);
                         DEBUG(9,"%s\n",char_zwi);
                         fehler_meldung((long)1,(long)0,char_zwi);
					}
		            else
					{
			             DEBUG(1,"ERR unlink Dat=%s\n",quelle_dat);
                         error = 707;
                         sprintf(char_zwi,"RSP %s nicht geloescht",quelle_dat);
                         DEBUG(9,"%s\n",char_zwi);
                         fehler_meldung((long)1,(long)error,char_zwi);
						 break;
					}
				 }
				 else
				 {
					 DEBUG(1,"Datei %s nicht geloescht, loeschen_arg=%d\n",quelle_dat,loeschen_arg);
				 }
			}
			else
			{
                    error = 0;
                    sprintf (char_zwi, "STA-Datei: %s nicht vorhanden",quelle_dat);
                    fehler_meldung((long)1,(long)error,char_zwi);
                    DEBUG(9,"\n%s\n",char_zwi);
			}


             /* Status-Datei holen und in der Waage loeschen */
            DEBUG(1,"STATUS holen\n");
			sprintf (quelle_dat,"%s\\%s\\%s",home_waage,OUT_verz,status_dat);
		    DEBUG(1,"quelle_dat=%s\n",quelle_dat);

		    sprintf (metinzwi,"%s\\RSP_staxml.%d",tmppath,sys);
		    DEBUG(1,"metinzwi=%s\n",metinzwi);

		    DEBUG(1,"vor HOLEN metinzwi=%s quelle_dat=%s\n",metinzwi,quelle_dat);
			unlink (metinzwi);
		    DEBUG(1,"unlink %s\n",metinzwi);

			printf ("TIMEOUT=%d\n",2);
            merker = 0;
			warten = 0;
            while (warten <= 2)  
            {
                 error = copy (metinzwi, quelle_dat, FALSE);
				 if (error == 0)
				 {
			         DEBUG(1,"Datei %s geholt\n",output);
					 merker = 1;
					 break;
				 }
		         else
				 {
					 warten++;
					 Sleep(1000);
					 if (warten%10 == 0)
					     printf("%d",warten);
					 else
					     printf(".");
				 }
            } /* end of while () */
		    printf("\n");

			if (error == 99)
			{
					sprintf (char_zwi,"%s kann nicht ge�ffnet werden",quelle_dat);
			        fehler_meldung ((long)1, error, char_zwi);
					DEBUG(1,"%s\n",char_zwi);
			}
			if (error == 98)
			{
					sprintf (char_zwi,"%s kann nicht ge�ffnet werden",metinzwi);
			        fehler_meldung ((long)1, error, char_zwi);
					DEBUG(1,"%s\n",char_zwi);
			}

			DEBUG(1,"\nmerker=%d\n",merker);
            
			/* STATUS.xml in der Waage loeschen */
			if (merker == 1)
			{
				 if (loeschen_arg == 0)
				 {
					DEBUG(1,"Datei %s loeschen\n",quelle_dat);
				    error = unlink (quelle_dat);
		            if (error == 0)
					{
			             DEBUG(1,"%s geloescht\n",quelle_dat);
                         sprintf(char_zwi,"STA %s geloescht",quelle_dat);
                         DEBUG(9,"%s\n",char_zwi);
                         fehler_meldung((long)1,(long)0,char_zwi);
					}
		            else
					{
			             DEBUG(1,"ERR unlink Dat=%s\n",quelle_dat);
                         error = 707;
                         sprintf(char_zwi,"STA %s nicht geloescht",quelle_dat);
                         DEBUG(9,"%s\n",char_zwi);
                         fehler_meldung((long)1,(long)error,char_zwi);
						 break;
					}
				 }
				 else
				 {
					 DEBUG(1,"Datei %s nicht geloescht, loeschen_arg=%d\n",quelle_dat,loeschen_arg);
				 }
			}
			else
			{
                    error = 0;
                    sprintf (char_zwi, "STA-Datei: %s nicht vorhanden",quelle_dat);
                    fehler_meldung((long)1,(long)error,char_zwi);
                    DEBUG(9,"\n%s\n",char_zwi);
                    break;
			}

	        break;
		} /* end of while (TRUE) */

		
		break;

default:
		sprintf (char_zwi,"Netz=%d ist nicht erlaubt",netz);
		error=751;
		fehler_meldung((long)1,error,char_zwi);
		break;
} /* end of switch(mode) */

if (error == 0)
{
    sprintf (char_zwi, "Reset in der Waage OK beendet",kennung_arg);
    fehler_meldung((long)1,(long)error,char_zwi);
    DEBUG(9,"%s\n",char_zwi);
}
return error;

} /* end of reset () */

/*********************************************/
long dev_dat_lesen(char * dat,FILE * fp)
{
	long error = 0;
	char zwi[8];
    char * p;
    char * q;
	char code;

	if (testen)
		printf ("dev_dat_lesen: dat=%s kennung_arg=%s\n",dat);

	while (1)
	{
        p = fgets(char_zwi,255,fbat);

        if (p == NULL)  break;
        cr_weg(char_zwi);

		//if (testen)
		//	printf ("%s\n",char_zwi);

		code = char_zwi[1];
		switch (code)
		{
	        case 's':
                sys = (short)atoi(&char_zwi[3]);
				break;
	        case 'n':
                netz = (short)atoi(&char_zwi[3]);
				break;
	        case 'd':
				sprintf (id,"%s",&char_zwi[3]);
				break;
	        case 'p':
				sprintf (passwd,"%s",&char_zwi[3]);
				break;
	        case 'u':
				sprintf (user,"%s",&char_zwi[3]);
				break;
	        case 'i':
				sprintf (input,"%s",&char_zwi[3]);
				break;
	        case 'j':
				sprintf (input_quit,"%s",&char_zwi[3]);
				//sprintf (status_dat,"%s",input_quit);
				//q=strstr(status_dat,".xml");
				//sprintf (q,"_status.xml");
				//if (testen) printf("status_dat=%s\n",status_dat);

				break;
	        case 'k':
                kopieren = (short)atoi(&char_zwi[3]);
				break;
	        case 'o':
				if (strcmp(kennung_arg,"0") == 0)
  				    sprintf (output,"%s",&char_zwi[3]);
                else
				{
				    sprintf (output,"%s",&char_zwi[3]);
				    q=strstr(output,".");
				    sprintf (q,"%s",kennung_arg);
					sprintf (zwi,".%d",sys);
					strcat (output,zwi);
				}
				break;
	        case 'a':
				sprintf (rspoutput,"%s",&char_zwi[3]);
     			break;
	        case 'q':
				sprintf (output_quit,"%s",&char_zwi[3]);
				
				sprintf (status_dat,"%s",output_quit);
				q=strstr(status_dat,".xml");
				sprintf (q,"_Status.xml");
				if (testen) printf("status_dat=%s\n",status_dat);

				break;
	        case 'b':
				if (strcmp(kennung_arg,"0") == 0)
					sprintf (lbdat,"%s",&char_zwi[3]);
                else
					sprintf (lbdat,"%s%s",kennung_arg,&char_zwi[3]);
				break;
	        case 'w':
                warten_par = (short)atoi(&char_zwi[3]);
				break;
	        case 'f':
                laenge_log = (short)atoi(&char_zwi[3]);
				break;
	        case 'r':
                ping_prue = (short)atoi(&char_zwi[3]);
				break;
	        case 'l':
				sprintf (logbuch,"%s",&char_zwi[3]);
				break;
	        case 'x':
				sprintf (IN_verz,"%s",&char_zwi[3]);
				break;
	        case 'y':
				sprintf (TAF_verz,"%s",&char_zwi[3]);
				break;
	        case 'z':
				sprintf (OUT_verz,"%s",&char_zwi[3]);
				break;
	        case 'h':
				sprintf (home,"%s",&char_zwi[3]);
				break;
	        case 'v':
				sprintf (home_waage,"%s",&char_zwi[3]);
				break;
		    default:
				printf ("COD <%s> ignoriert\n",char_zwi);
			    break;
		}
	}

	sprintf (bin,"%s\\bin",home);
	sprintf (etc,"%s\\etc",home);
	sprintf (tmppath,"%s\\tmp",home);
	sprintf (konvert,"%s\\konvert",home);

	if (testen)
	{
		printf ("netz=%d\n",netz);
		printf ("sys=%d\n",sys);
		printf ("id=%s:\n",id);
		printf ("user=%s:\n",user);
		printf ("passwd=%s:\n",passwd);
		printf ("input=%s:\n",input);
		printf ("input_quit=%s:\n",input_quit);
		printf ("output=%s:\n",output);
		printf ("rspoutput=%s:\n",rspoutput);
		printf ("output_quit=%s:\n",output_quit);
		printf ("warten_par=%d\n",warten_par);
		printf ("lbdat=%s:\n",lbdat);
		printf ("logbuch=%s:\n",logbuch);
		printf ("IN_verz=%s:\n",IN_verz);
		printf ("TAF_verz=%s:\n",TAF_verz);
		printf ("OUT_verz=%s:\n",OUT_verz);
		printf ("home=%s:\n",home);
		printf ("home_waage=%s:\n",home_waage);
		printf ("VERZEICHNISSE:\nbin=%s:\n",bin);
		printf ("etc=%s:\n",etc);
		printf ("konvert=%s:\n",konvert);
		printf ("tmppath=%s:\n",tmppath);
		printf ("laenge_log=%d\n",laenge_log);
		printf ("ping_prue=%d\n",ping_prue);
		printf ("---------------ENDE %s----------------\n",dat);
	}

	return (error);
} /* end of dev_dat_lesen () */

/**********************************************************/                     
int scroll_arg (char ** arg, int pos, int anz)
/**
Argumenete nach links scrollen.
**/
{
anz --;
for (;pos < anz; pos ++)
       {
       arg [pos] = arg [pos + 1];
       }
return (anz);
}

/******************* main ********************/
int main (int argc,char * argv[])
{
    int i;
    int len=0;
	int sich_tag;
    short anzahl=0;
//	char datum[12];
    char * p;
    int peri_typ=0;   
    FILE * fp;
    long   uxtime=0;
    struct tm * strtime;
//    time_t uxtime;


         /** Parameter **/
    if(argc >= 2)
	{
        for (i = 1; i < argc; i ++)
        {
            if (argv [i] [0] == '-')
            {
                tst_arg_sw (&argv[i][1]);
                argc = scroll_arg (argv,i,argc);
                i --;
            }
        }
	}

    for (i=0;i<argc;i++)
	{
		if (i == 0)
		{
            if (testen)
		        printf ("argc=%d\n",argc);
		}
        if (testen)
		    printf ("argv[%d] = %s\n",i,argv[i]);
	}

    if(version != 0 || hilfe != 0 || devdataufbau != 0)
        usage();

    sprintf (dev_arg,"%s", argv[1]);
	mode_arg = (short)atoi(argv[2]);
    if (argc >= 4)
	   sprintf (kennung_arg,"%s",argv[3]);
	else
	   sprintf (kennung_arg,"0");

    if (argc >= 5)
	   loeschen_arg = (short)atoi(argv[4]);
	else
	   loeschen_arg = (short)0;

	if (testen) 
	{
		//printf ("laenge_log=%d\n",laenge_log);
		printf ("mode_arg=%d, kennung_arg=%s loeschen_arg=%d\n",mode_arg,kennung_arg,loeschen_arg);
		printf ("dev_arg=%s\n",dev_arg);
	}

    /* PARAMETER als Umgebungsvariable */
    if(getenv("PAR_TRANSLUC") != NULL)
    {
        sprintf(char_zwi,"%s",getenv("PAR_TRANSLUC"));
        tst_arg_sw(char_zwi);
        if (testen) printf ("char_zwi=%s\n",char_zwi);
    }


    if(getenv("testmode") != NULL)
    {
        sprintf(char_zwi,"%s   ACHTUNG testmode ist gesetzt","translUC");
        tst=(short)atoi(getenv("testmode"));
        if(tst > 1)
		{
            testlog= 1;
            testen = 0;
		}
        else
		{
            if(tst == 1)
			{
               testlog= 0;
               testen = 1;
			}
	        else
			{
               testlog= 0;
               testen = 0;
			}
		}
    }

	sprintf(dev_datei,"%s",dev_arg);
	//if (testen) printf ("dev_datei=%s\n",dev_datei);
    fbat=fopen(dev_datei,"r");
    if(fbat == NULL)
    {
        error=10;
        if(error_sav == 0)
	        error_sav = error;
        sprintf(char_zwi,"translUC: %s nicht vorhanden",dev_datei);
        //fehler_meldung((long)1,(long)error,char_zwi);
	    printf ("%s\n",char_zwi);
		return (10);
    }

	error = dev_dat_lesen(dev_datei, fbat);
	if (error != 0)
	{
        if(error_sav == 0)
	        error_sav = error;
        sprintf(char_zwi,"translUC: Fehler=%ld bei Lesen %s",error,dev_datei);
        //fehler_meldung((long)1,(long)error,char_zwi);
	    printf ("%s\n",char_zwi);
		return (error);
	}
	fclose (fbat);

    if(testlog)
	{
        sprintf(char_zwi,"%s",logbuch);
		sprintf(buff,".");
		p=strrchr(char_zwi,92);
		//printf ("buff=%s p=%s\n",buff,p);
		if (p != NULL)
			strcpy(p,"\\translUC.tst");
        if (testen) printf ("LOG: char_zwi=%s\n",char_zwi);

        printf ("OPEN %s\n",char_zwi);
        flog = fopen(char_zwi,"w+");
	    if(flog != NULL)
              DEBUG(1,"TEST-LOG ist geoeffnet\n");
	}
	if (kopieren == 1)
	{
        time((time_t *) &uxtime);
        strtime = localtime((time_t *) &uxtime); /* aktuelle Zeit holen */
        sich_tag = strtime->tm_mday;
        sprintf (sichern, "%s\\ums%02d", tmppath, sich_tag);
		if (testen)
		{
			printf ("kopieren=%d\n",kopieren);
	    	printf ("sichern=%s\n",sichern);
		}
	}

    sprintf(char_zwi,">>>>> START: translUC: Vers.%2d.%02d vom %s: WINDOWS >>>>>>>>",
                  VERSION_TRANSLUC,VERSION_NACHKOMMA_TRANSLUC,DATUM);
    fehler_meldung((long)0,(long)0,char_zwi);

    error = 0;

    switch (mode_arg)
	{
	    case 1:
		    sprintf (mode_arg_char,"%s","put Daten");
		    break;
	    case 2:
                    /* 15.02.10 mit Pr�fix */
                    mit_praefix = 1;
		    if (strcmp(kennung_arg,"0") == 0)
		        sprintf (mode_arg_char,"%s","get LB   ");
		    else
		        //sprintf (mode_arg_char,"%s","get INVLB");
		        sprintf (mode_arg_char,"%s %s","get", kennung_arg);
		    break;
	    case 3:
                    /* 15.02.10 ohne Pr�fix */
                    mit_praefix = 0;
		    sprintf (mode_arg_char,"%s","get *.xml");
		    break;
	    case 4:
		    sprintf (mode_arg_char,"%s","get Ums  ");
		    break;
       
	    case 99:
			//28.09.06 FF
		    sprintf (mode_arg_char,"%s","RESET");
			loeschen_arg = 0;  /* Reset nur mit loeschen !!! FF */
		    break;

	    default:
		    sprintf (mode_arg_char,"%s","null");
            error = 700;
            if(error_sav == 0)
	             error_sav = error;
			sprintf(char_zwi,"Mode %d ist nicht erlaubt",mode_arg);
            fehler_meldung((long)1,(long)error,char_zwi);
		    break;
	}
	DEBUG(1,"\nMODE=%s mit_praefix=%d\n",mode_arg_char, mit_praefix);

    if (argc < 3)
	{
		error = 999;
        if(error_sav == 0)
	        error_sav = error;
		sprintf (char_zwi, "Anzahl Parameter falsch: ist=%d, soll min=3", argc);
	}
    else
	{
        sprintf(char_zwi,"%s: Sys=%d",mode_arg_char,sys);
		if (error == 0)
		{
			if (loeschen_arg == 0)
				sprintf (buff,"  mit Loeschen");
			else
				sprintf (buff,"  ohne Loeschen");
			//10.03.06 FF
			//if (mode_arg == 2)
			    strcat (char_zwi,buff);
			strcat (char_zwi," gestartet");
		}
		if (netz == 0)
			strcat (char_zwi,",        FTP");
		if (netz == 1)
			strcat (char_zwi,", WINDOWS-NETZ");

        fehler_meldung((long)1,(long)0,char_zwi);
        sprintf(char_zwi,"   DEV-Dat %s",dev_arg);
	}
    fehler_meldung((long)1,(long)error,char_zwi);

    //18.09.06 FF
	if (mode_arg == 1)
	{
	     sprintf(char_zwi,"   INPUT   %s",input);
         fehler_meldung((long)1,(long)error,char_zwi);
	     sprintf(char_zwi,"   OUTPUT  %s",rspoutput);
         fehler_meldung((long)1,(long)error,char_zwi);
	     sprintf (buff,"%s\\RSP_staxml.%d",tmppath,sys);
	     sprintf(char_zwi,"   STATUS  %s",buff);
         fehler_meldung((long)1,(long)error,char_zwi);
	}
    else if (mode_arg == 2)
	{
		 if (netz == 0)
		 {
	       sprintf (buff,"%s\\%s",TAF_verz,lbdat);
	       sprintf(char_zwi,"   LB-File %s",buff);
		 }
 		 if (netz == 1)
		 {
	       sprintf (buff,"%s\\%s\\%s",home_waage,TAF_verz,lbdat);
	       sprintf(char_zwi,"   LB-File %s",buff);
		 }
         fehler_meldung((long)1,(long)error,char_zwi);
	     sprintf(char_zwi,"   OUTPUT  %s",output);
         fehler_meldung((long)1,(long)error,char_zwi);
	}
    else if (mode_arg == 3)
	{
             /* 15.02.10 FF */
		 if (netz == 0)
		 {
	       sprintf (buff,"%s\\*.xml",TAF_verz);
	       sprintf(char_zwi,"   LB-File %s",buff);
		 }
 		 if (netz == 1)
		 {
	       sprintf (buff,"%s\\%s\\*.xml",home_waage,TAF_verz);
	       sprintf(char_zwi,"   LB-File %s",buff);
		 }
         fehler_meldung((long)1,(long)error,char_zwi);
	     sprintf(char_zwi,"   OUTPUT  %s",output);
         fehler_meldung((long)1,(long)error,char_zwi);
	}
    else if (mode_arg == 4)
	{
	     sprintf(char_zwi,"   OUTPUT  %s",output);
         fehler_meldung((long)1,(long)error,char_zwi);
	}
    else if (mode_arg == 99)
	{
	     sprintf(char_zwi,"   RESET Verz: %s in der Waage",OUT_verz);
         fehler_meldung((long)1,(long)error,char_zwi);
	}

	if (warte_zeit == 0 && peri_typ != GER_METT_UC)
        warte_zeit = WARTE_ZEIT;

    sprintf(v_logbuch,"%s\\exit_ftp.%d",tmppath,sys);
    DEBUG(1,"V_LOG=%s\n",v_logbuch);
    unlink(v_logbuch);

	//DEBUG(1,"argc=%d: sys=%d peri_typ=%d\ndev_datei=%s error=%ld\n",argc,sys,peri_typ,dev_datei,error);
    DEBUG(1,"vor der Bearbeitung\n");

/* Bearbeitungsschleife */
while (error == 0)
{
        /***** Ersetzung der Parameter $1 ... *****/
        strcpy(treiber,mode_arg_char);
        DEBUG(1,"treiber=%s:\n",treiber);
        len = strlen(passwd);
        DEBUG (1,"len passwd=%d\n",len);

        switch (mode_arg)
		{
        case 1:
            /************ put *****************/
            DEBUG(1,"Aktion: %s: mode_arg=%d\n",treiber,mode_arg);
            if (len == 0)
                DEBUG(1,"ftpput(%s %s NULL NULL %s %s T_BINARY)\n",
                id,passwd,input,input_quit);
            else
                DEBUG(1,"ftpput(%s %s %s NULL %s %s T_BINARY)\n",
                id,user,passwd,input,input_quit);
#ifdef _WINDOWS
            if (ohne_aufruf == 0)
			{
                if (warte_zeit == 0)
                     warte_zeit = warten_par;
                DEBUG(1,"warte_zeit=%d\n",warte_zeit);
                error = put_uc(sys,len);
			}
            else
			{
                DEBUG(9,"ohne Aufruf\n");
			}

            DEBUG (1,"error=%ld\n",error);
            if (error != 0)
			{
                 //sprintf (char_zwi,"Uebertragungsfehler <ftperrno=%ld>: ftp%s, sys %d",
                 //(long)ftperrno,treiber,sys);
                 sprintf (char_zwi,"Sys=%d: �bertragungsfehler",sys);
			}
            else
			{
                 sprintf (char_zwi,"Sys=%d: �bertragung OK",sys);
			}
#else /* _WINDOWS */
            sprintf (char_zwi,"Uebertragung : %s, sys %d",treiber,sys);
#endif /* _WINDOWS */

            
			
			DEBUG(1,"error=%ld: %s\n",error,char_zwi);
            fehler_meldung ((long)1, error, char_zwi);
            break;
        case 2:
        case 3:
            /************ get LANGBON *****************/
            /* 15.02.10 ohne Pr�fix A */
//            mit_praefix = 0;
//            if (mode_arg = 2)
//                mit_praefix = 1;
            DEBUG(1,"Aktion: %s mode_arg=%d, mit_praefix=%d\n",treiber,mode_arg, mit_praefix);
            /* 15.02.10 ohne Pr�fix E */

            if (len == 0)
                DEBUG(1,"ftpget(%s %s NULL NULL %s %s w T_BINARY)\n",
                id,user,output,output_quit);
            else
                DEBUG(1,"ftpget(%s %s %s NULL %s %s w T_BINARY)\n",
                id,user,passwd,output,output_quit);

#ifdef _WINDOWS
            if (ohne_aufruf == 0)
			{
                 if (warte_zeit == 0)
                     warte_zeit = atoi(&output_quit[1]);
                 error = get_uc_lb(sys,len);
			}
            else
			{
                 DEBUG(9,"ohne Aufruf\n");
			}

            DEBUG (1,"error=%ld\n",error);
            if (error != 0)
			{
                 //sprintf (char_zwi,"Uebertragungsfehler <ftperrno=%ld>: ftp%s, sys %d",
                 //(long)ftperrno,treiber,sys);
                 sprintf (char_zwi,"Sys=%d: Uebertragungsfehler",sys);
                 //error = (long)ftperrno;
			}
            else
			{
                 //ftplogoff();
				if (strcmp(kennung_arg,"0") == 0)
                    sprintf (char_zwi,"Sys=%d: LB-Uebertragung OK",sys);
				else
				{
					/* 15.02.10 */
					if (mode_arg == 3)
                        sprintf (char_zwi,"Sys=%d: Uebertragung OK",sys);
                    else
                        sprintf (char_zwi,"Sys=%d: %sLB-Uebertragung OK",sys,kennung_arg);
				}
			}

#else /* _WINDOWS */
            sprintf (char_zwi,"Uebertragung : %s, sys=%d",treiber,sys);
#endif /* _WINDOWS */
            DEBUG(1,"error=%ld: %s\n",error,char_zwi);
            fehler_meldung ((long)1, error, char_zwi);


            break;

        case 4:
            /************ get Umsatz *****************/
            DEBUG(1,"Aktion: %s mode_arg=%d\n",treiber,mode_arg);

			
			//18.09.06 FF
			sprintf (char_zwi,"Umsatz-Daten holen, Prozedure nicht aktiv, in Bearbeitung",sys);
	        fehler_meldung ((long)1, error, char_zwi);
            break;
            



	        if (len == 0)
                DEBUG(1,"ftpget(%s %s NULL NULL %s %s w T_BINARY)\n",
                id,user,output,output_quit);
            else
                DEBUG(1,"ftpget(%s %s %s NULL %s %s w T_BINARY)\n",
                id,user,passwd,output,output_quit);

#ifdef _WINDOWS
            if (ohne_aufruf == 0)
			{
                 if (warte_zeit == 0)
                     warte_zeit = atoi(&output_quit[1]);
                 error = get_uc(sys,len);
			}
            else
			{
                 DEBUG(9,"ohne Aufruf\n");
			}

            DEBUG (1,"error=%ld\n",error);
            if (error != 0)
			{
                 //sprintf (char_zwi,"Uebertragungsfehler <ftperrno=%ld>: ftp%s, sys %d",
                 //(long)ftperrno,treiber,sys);
                 sprintf (char_zwi,"Sys=%d: Uebertragungsfehler",sys);
                 //error = (long)ftperrno;
			}
            else
			{
                 //ftplogoff();
                 sprintf (char_zwi,"Sys=%d: Umsatz-Uebertragung OK",sys);
			}
#else /* _WINDOWS */
            sprintf (char_zwi,"Uebertragung : %s, sys=%d",treiber,sys);
#endif /* _WINDOWS */
            DEBUG(1,"error=%ld: %s\n",error,char_zwi);
            fehler_meldung ((long)1, error, char_zwi);


            break;

        case 99:
            /************ RESET Export   28.09.06 FF*****************/
            DEBUG(1,"Aktion: %s mode_arg=%d\n",treiber,mode_arg);
#ifdef _WINDOWS
            if (ohne_aufruf == 0)
			{
                 if (warte_zeit == 0)
                     warte_zeit = atoi(&output_quit[1]);
                 error = reset(sys,len,(short)0);
			}
            else
			{
                 DEBUG(9,"ohne Aufruf\n");
			}

            DEBUG (1,"error=%ld\n",error);
            if (error != 0)
			{
                 sprintf (char_zwi,"Sys=%d: Error bei %s",sys,treiber);
			}
            else
			{
                 sprintf (char_zwi,"Sys=%d: Reset OK",sys,kennung_arg);
			}
#else /* _WINDOWS */
            sprintf (char_zwi,"%s, sys=%d",treiber,sys);
#endif /* _WINDOWS */
            DEBUG(1,"error=%ld: %s\n",error,char_zwi);
            fehler_meldung ((long)1, error, char_zwi);

            break;

        default:
            error = 700;
            if(error_sav == 0)
	            error_sav = error;
            sprintf(char_zwi,"Falsche Aktion %s",treiber);
            fehler_meldung((long)1,(long)error,char_zwi);
	        DEBUG(1,"%s\n",char_zwi);
            break;
		} /* end of switch (mode_arg) */

		DEBUG(1,"sleep 1\n");
#ifdef _WINDOWS
        Sleep ((long) 1000);
#else
        sleep ((long) 1);
#endif
		DEBUG(1,"nach: sleep 1\n");


  break; /* von while() */
} /* end of while () */

#ifdef OS4680
if (error == 0 || ftperrno == FTPCONNECT || error == FTPCONNECT)
{
  /************************
  if (error != 0)
  {
       DEBUG(1,"PING\n");
       hp = gethostbyname(netz);
       if (hp)
          {
          memcpy ((char *) &addr, hp->h_addr, hp->h_length);
          i = ping(addr,256);
          DEBUG(1,"ping reply %d msec\n",i);
          }
       else
          {
          sprintf (char_zwi,"unknown host %s",netz);
          fehler_meldung((long)1,(long)201,char_zwi);
          }
  }
  **********************/
 
  DEBUG(1,"CLOSE\n");
  ftplogoff();
}
#else
  DEBUG(1,"WINDOWS ftp exit\n");
#endif

if (error_sav == 0)
   error_sav = error;

if(error_sav != 0)
   error = error_sav;

memset(char_zwi,'<',54);
memcpy(&char_zwi[24]," ENDE ",6);
if(error != 0)
     memcpy(&char_zwi[34]," exit(1) ",9);
char_zwi[54] = '\0';
fehler_meldung((long)1,(long)error,char_zwi);

DEBUG(1,"ENDE %s error=%ld\n","translUC",error);
fp = fopen (v_logbuch, "w");
sprintf (char_zwi,"%ld",error);
fwrite (char_zwi,strlen(char_zwi),1,fp);
fclose(fp);
if (testlog)
    fclose (flog);
if(error == 0)
    return(0);
return (1);
} /* end of main */

/************************** main ***********************/
