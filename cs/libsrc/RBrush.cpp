#include "RBrush.h"

RBrush::RBrush ()
{
	color = NULL;
	hBrush = NULL;
	Instances = 0;
}

RBrush::RBrush (COLORREF color, HBRUSH hBrush)
{
	this->color = color;
	this->hBrush = hBrush;
	Instances = 0;
}


RBrush::~RBrush ()
{
}

BOOL RBrush::operator== (RBrush& RBrush)
{
	if (hBrush == NULL)
	{
		return FALSE;
	}
	if (RBrush.GetColor () != color)
	{
		return FALSE;
	}
	return TRUE;
}

RBrush& RBrush::operator=(RBrush& RBrush)
{
	color= RBrush.GetColor ();
	return *this;
}

void RBrush::SetColor (COLORREF color)
{
	this->color = color;
}

COLORREF RBrush::GetColor ()
{
	return color;
}


void RBrush::SetBrush (HBRUSH hBrush)
{
	this->hBrush = hBrush;
}

HBRUSH RBrush::GetBrush ()
{
	return hBrush;
}

void RBrush::inc ()
{
	Instances ++;
}

void RBrush::dec ()
{
	if (Instances > 0)
	{
	     Instances --;
	}
}

int RBrush::GetInstances ()
{
	return Instances;
}