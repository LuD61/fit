#include <windows.h>
#include <stdio.h>
#include "Work.h"
#include "strfkt.h"
#include "mo_menu.h"
//#include "mo_wdebug.h"

extern char LONGNULL[];

#define MAXTRIES 100

long Work::LongNull = 0x80000000;


void Work::ExitError (BOOL b)
{
    if (b)
    {
        disp_mess ("Speicher kann nicht zugeornet werden", 2);
        ExitProcess (1);
    }
}

void Work::TestNullDat (long *dat)
{
    if (*dat == 0l)
    {
        memcpy (dat, (long *) LONGNULL, sizeof (long));
    }
}

   

void Work::FillRow (void)
{

    if (Lsts == NULL) return;
}


void Work::InitRow (void)
{
    if (Lsts == NULL) return;
}

int Work::GetPtab (char *dest, char *item, char *wert)
{
    int dsqlstatus;

    dest[0] = 0;
    dsqlstatus = Ptab.lese_ptab (item, wert);  
    if (dsqlstatus == 100)
    {
        return -1;
    }
    strcpy (dest, ptabn.ptbezk);
    return 0;
}

int Work::ShowPtab (char *dest, char *item)
{
    int dsqlstatus;

    dsqlstatus = Ptab.Show (item);  
    if (syskey == KEY5)
    {
        return -1;
    }
    strcpy (dest, ptabn.ptwert);
    return 0;
}

int Work::ShowPtabEx (HWND hMainWindow, char *dest, char *item)
{
       char wert [80];
       SEARCHPTAB *SearchPtab;
	   HINSTANCE hMainInst;


       hMainInst = (HINSTANCE) GetWindowLong (hMainWindow, GWL_HINSTANCE);
       SearchPtab = new SEARCHPTAB (item);
       SearchPtab->SetParams (hMainInst, hMainWindow); 
       SearchPtab->Setawin (hMainWindow);
                                                  
       SearchPtab->Search ();
       if (SearchPtab->GetKey (wert) == TRUE)
       {
           delete SearchPtab;
           strcpy (dest, wert);
		   return 0;
       }
       delete SearchPtab;
       return -1;
}


int Work::GetMdnName (char *dest, short mdn)
{
    int dsqlstatus;

    if (mdn == 0)
    {
        strcpy (dest, "Zentrale");
        return 0;
    }
    dest[0] = 0;
    DbClass.sqlin ((short *) &mdn, 1, 0);
    DbClass.sqlout ((char *) dest, 0, 17);
    dsqlstatus =  DbClass.sqlcomm ("select adr.adr_krz from mdn,adr "
                                   "where mdn.mdn = ? " 
                                   "and adr.adr = mdn.adr");
    if (dsqlstatus == 100)
    {
        return 100;
    }
    return Mdn.lese_mdn (mdn);
}


int Work::GetFilName (char *dest, short mdn, short fil)
{

    if (mdn == 0 || fil == 0)
    {
        strcpy (dest, "Mandant");
        return 0;
    }
    dest[0] = 0;
    DbClass.sqlin ((short *) &mdn, 1, 0);
    DbClass.sqlin ((short *) &fil, 1, 0);
    DbClass.sqlout ((char *) dest, 0, 17);
    return DbClass.sqlcomm ("select adr.adr_krz from fil,adr "
                             "where fil.mdn = ? " 
                             "and   fil.fil = ? " 
                             "and adr.adr = fil.adr");
}

int Work::GetHwgBez (char *dest, short hwg)
{
    int dsqlstatus;

    if (hwg == 0) return 0;
    dest[0] = 0;
    DbClass.sqlin ((short *) &hwg, 1, 0);
    DbClass.sqlout ((char *) dest, 0, 17);
    dsqlstatus =  DbClass.sqlcomm ("select hwg_bz1 from hwg "
                                   "where hwg = ?"); 
    if (dsqlstatus == 100)
    {
        return 100;
    }
    return 0;
}


void Work::BeginWork (void)
{
    ::beginwork ();
}

void Work::CommitWork (void)
{
    ::commitwork ();
}

void Work::RollbackWork (void)
{
    ::rollbackwork ();
}

void Work::InitRec (void)
{
}



void Work::DeleteAllPos (void)
{
}


void Work::PrepareLst (void)
{
}

void Work::OpenLst (void)
{
	if (LstCursor == -1)
	{
		PrepareLst ();
		if (LstCursor == -1) return;
	}
}

int Work::FetchLst (void)
{
	if (LstCursor == -1)
	{
		PrepareLst ();
		if (LstCursor == -1) return 100;
	}
    return 0;
}

void Work::CloseLst (void)
{
	if (LstCursor != -1)
	{
		LstCursor = -1;
	}
}

long Work::GetHwgAnz (short hwg)
{
    long anz;

    MatrixTyp = ARTIKEL;
    DbClass.sqlin ((short *) &hwg, 1, 0);
    cursor = DbClass.sqlcursor ("select a from a_bas where hwg = ? and a > 0 and delstatus = 0");
    anz = 0;
    while (DbClass.sqlfetch (cursor) == 0)
    {
           if (anz >= MAXA) break;
           anz ++;
    }
    DbClass.sqlclose (cursor);
    cursor = -1;
    return anz;
}


long Work::HwgFirst (short hwg)
{
    
    DbClass.sqlin ((short *) &hwg, 1, 0);
    DbClass.sqlout ((double *) &_a_bas.a, 3, 0);
    cursor = DbClass.sqlcursor ("select a from a_bas where hwg = ? " 
                                "and a > 0 "
                                "and delstatus = 0 "
                                "order by a");
    int dsqlstatus = DbClass.sqlfetch (cursor);
    if (dsqlstatus == 0)
    {
        dsqlstatus = lese_a_bas (_a_bas.a);
    }
    return dsqlstatus;
}

long Work::HwgNext (void)
{

    if (cursor == -1) 
    {
        return 100;
    }
    int dsqlstatus = DbClass.sqlfetch (cursor);
    if (dsqlstatus == 0)
    {
        dsqlstatus = lese_a_bas (_a_bas.a);
    }
    else
    {
        DbClass.sqlclose (cursor);
        cursor = -1;
    }
    return dsqlstatus;
}

long Work::GetLiefAnz (short hwg)
{
    long anz;

    this->hwg = hwg;
    _a_bas.hwg = hwg;
    MatrixTyp = LIEFERANT;
    cursor = DbClass.sqlcursor ("select distinct (lief.lief) from lief, lief_bzg "
                                "where lief.lief > \"               \" "
								"and lief_bzg.lief = lief.lief "
                                "and lief.lief != \"-1\"");
    anz = 0;
    while (DbClass.sqlfetch (cursor) == 0)
    {
           if (anz >= MAXA) break;
           anz ++;
    }
    DbClass.sqlclose (cursor);
    cursor = -1;
    return anz;
}


long Work::LiefFirst (void)
{
    
    DbClass.sqlout ((char *) lief_bzg.lief, 0, 17);
    DbClass.sqlout ((char *) _adr.adr_krz, 0, 17);
    cursor = DbClass.sqlcursor ("select distinct (lief.lief), adr.adr_krz from lief,adr,lief_bzg " 
                                "where lief.lief > \"               \" "
                                "and lief.lief != \"-1\" "
                                "and adr.adr = lief.adr "
								"and lief.lief = lief_bzg.lief "
                                "order by lief.lief");
    int dsqlstatus = DbClass.sqlfetch (cursor);
    return dsqlstatus;
}

long Work::LiefNext (void)
{

    if (cursor == -1) 
    {
        return 100;
    }
    int dsqlstatus = DbClass.sqlfetch (cursor);
    if (dsqlstatus == 100)
    {
        DbClass.sqlclose (cursor);
        cursor = -1;
    }
    return dsqlstatus;
}

BOOL Work::GetABz1 (double a, char *a_bz1)
{
	  strcpy (a_bz1, "");
	  if (lese_a_bas (a) == 0)
	  {
		  strcpy (a_bz1, _a_bas.a_bz1);
		  return TRUE;
	  }
	  return FALSE;
}


int Work::PrepareLief (void)
{
 
     DbClass.sqlin ((short *)  &hwg, 1, 0);
     DbClass.sqlout ((double *) &_a_bas.a, 3, 0);
     DbClass.sqlout ((char *)   _a_bas.a_bz1, 0, 25);

     cursor = DbClass.sqlcursor ("select distinct (a_bas.a), a_bas.a_bz1 from a_bas,lief_bzg "
		                        "where a_bas.hwg = ? "
								"and a_bas.a = lief_bzg.a "
                                "and a_bas.a > 0 "
                                "order by a_bas.a");
     DbClass.sqlin ((char *)   lief_bzg.lief, 0, 17);
     DbClass.sqlin ((short *)  &lief_bzg.mdn, 1,0);
     DbClass.sqlin ((double *) &_a_bas.a, 3,0);
     DbClass.sqlout ((double *) &lief_bzg.pr_ek_eur, 3,0);
     cursor_a = DbClass.sqlcursor ("select pr_ek_eur from lief_bzg "
                                   "where lief = ? "
                                   "and mdn = ? "
                                   "and a = ?");
     return cursor;
}

     
int Work::Prepare (void)
{

     if (MatrixTyp == LIEFERANT)
     {
         return PrepareLief ();
     }
/*
     DbClass.sqlout ((char *)   lief_bzg.lief, 0, 17);
     DbClass.sqlin ((short *)  &lief_bzg.mdn, 1,0);
     cursor = DbClass.sqlcursor ("select lief from lief_bzg where mdn = ? order by lief");
*/
     DbClass.sqlout ((char *)   lief_bzg.lief, 0, 17);
     DbClass.sqlout ((char *) _adr.adr_krz, 0, 17);
//     DbClass.sqlin ((char *)   lief_bzg.lief, 0, 17);
     cursor = DbClass.sqlcursor ("select lief, adr_krz from lief,adr "
                                 "where lief > \"-1\" "
                                 "and adr.adr = lief.adr "
                                 "order by lief");
     DbClass.sqlin ((char *)   lief_bzg.lief, 0, 17);
     DbClass.sqlin ((short *)  &lief_bzg.mdn, 1,0);
     DbClass.sqlin ((double *) &_a_bas.a, 3,0);
     DbClass.sqlout ((double *) &lief_bzg.pr_ek_eur, 3,0);
     cursor_a = DbClass.sqlcursor ("select pr_ek_eur from lief_bzg "
                                   "where lief = ? "
                                   "and mdn = ? "
                                   "and a = ?");
     return cursor;
}

void Work::Open (void)
{
	if (cursor == -1)
	{
		Prepare ();
		if (cursor == -1) return;
	}
    DbClass.sqlopen (cursor);
}

int Work::Fetch (void)
{
	if (cursor == -1)
	{
		Prepare ();
		if (cursor == -1) return 100;
	}
    int dsqlstatus = DbClass.sqlfetch (cursor);
/*
    if (dsqlstatus == 0)
    {
        DbClass.sqlopen (cursor_lief);
        DbClass.sqlfetch (cursor_lief);
    }
*/
	return dsqlstatus;
}

int Work::FetchLiefBzg ()
{
	 if (cursor == -1)
	 {
		 Prepare ();
	 }
     lief_bzg.pr_ek_eur = 0.0;
     open_sql (cursor_a);
     return fetch_sql (cursor_a);
}

void Work::Close (void)
{

	if (cursor != -1)
	{
        DbClass.sqlclose (cursor);
        DbClass.sqlclose (cursor_lief);
        DbClass.sqlclose (cursor_a);
		cursor = -1;
		cursor_lief = -1;
		cursor_a = -1;
	}
}

int Work::WriteRec (form *UbForm, int Feldanz)
{
    LIEF_BZG_CLASS LiefBzg;
    double pr_ek;
    char ek_old [20];
    char ek_new [20];

    if (Lsts == NULL) return 0;

    if (MatrixTyp == LIEFERANT)
    {
         return WriteRecLief (UbForm, Feldanz);
    }

    strcpy (lief_bzg.lief, Lsts[0]->GetFeld ());

    for (int i = 1; i < Feldanz; i ++)
    {
        pr_ek = ratod (Lsts[i]->GetFeld ());
        if (UbForm->mask[i].attribut == BUTTON)
        {
            lief_bzg.a = ratod (UbForm->mask[i].item->GetFeldPtr ());
        }
        else if (UbForm->mask[i].attribut == COLBUTTON)
        {
            ColButton *Cub = (ColButton *) UbForm->mask[i].item->GetFeldPtr ();
            lief_bzg.a = ratod (Cub->text1);
        }
        lief_bzg.pr_ek = 0.0;
        if (lief_bzg.a == 0.0) continue;
        int dsqlstatus = LiefBzg.dbreadfirst ();
        if (dsqlstatus != 0) 
        {
            if (pr_ek == 0.0)
            {
                   continue;
            }
            sprintf (lief_bzg.lief_best, "%.0lf", lief_bzg.a);
        }

        sprintf (ek_old, "%.4lf", lief_bzg.pr_ek_eur);
        sprintf (ek_new, "%.4lf", pr_ek);
        if (strcmp (ek_old, ek_new) == 0) continue;
        lief_bzg.pr_ek_eur = pr_ek;
        lief_bzg.pr_ek = pr_ek * 1.95583;
        dsqlstatus = LiefBzg.dbupdate ();
        if (dsqlstatus != 0)
        {
            return dsqlstatus;
        }
    }
    return 0;
}


int Work::WriteRecLief (form *UbForm, int Feldanz)
{
    LIEF_BZG_CLASS LiefBzg;
    double pr_ek;
    char ek_old [20];
    char ek_new [20];
    lief_bzg.a = ratod (Lsts[0]->GetFeld ());

    for (int i = 1; i < Feldanz; i ++)
    {
        pr_ek = ratod (Lsts[i]->GetFeld ());
        if (UbForm->mask[i].attribut == BUTTON)
        {
            strcpy (lief_bzg.lief, UbForm->mask[i].item->GetFeldPtr ());
        }
        else if (UbForm->mask[i].attribut == COLBUTTON)
        {
            ColButton *Cub = (ColButton *) UbForm->mask[i].item->GetFeldPtr ();
            strcpy (lief_bzg.lief, Cub->text1);
        }
        lief_bzg.pr_ek = 0.0;
        if (lief_bzg.a == 0.0) continue;
        int dsqlstatus = LiefBzg.dbreadfirst ();
        if (dsqlstatus != 0) 
        {
            if (pr_ek == 0.0)
            {
                   continue;
            }
            sprintf (lief_bzg.lief_best, "%.0lf", lief_bzg.a);
        }

        sprintf (ek_old, "%.4lf", lief_bzg.pr_ek_eur);
        sprintf (ek_new, "%.4lf", pr_ek);
        if (strcmp (ek_old, ek_new) == 0) continue;
        lief_bzg.pr_ek_eur = pr_ek;
        lief_bzg.pr_ek = pr_ek * 1.95583;
        dsqlstatus = LiefBzg.dbupdate ();
        if (dsqlstatus != 0)
        {
            return dsqlstatus;
        }
    }
    return 0;
}

