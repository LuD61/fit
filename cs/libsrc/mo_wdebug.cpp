#include <stdio.h>
#include <stdarg.h>
#include <windows.h>
#include "strfkt.h"
#include "mo_intp.h"
#include "wmaskc.h"

#define IdEdit 501
#define MAXBUFF 2000


static HWND hWndDeb = NULL;
static HWND hWndEdit = NULL;
static HWND DebugMain = NULL;
static HANDLE  hInstance;
static int zeile = 0;
static TEXTMETRIC tm;

char zbuff [MAXBUFF] [81];
int z0 = 0;
int z = 0;

void disp_deb (HDC hdc)
{
          int i;
          int y;
          int ze;

          GetTextMetrics (hdc, &tm);

          for (i = z0, ze = 0; i < z; i ++, ze ++)
          {
                    y = ze * tm.tmHeight;
                    SetTextColor (hdc,BLACKCOL);
                    SetBkColor   (hdc,WHITECOL);
                    TextOut (hdc, 0, y,zbuff[i] , strlen (zbuff[i]));
          }
}


LONG FAR PASCAL DebProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        PAINTSTRUCT pm;
        HDC hdc;

        switch(msg)
        {
              case WM_PAINT :
                    hdc = BeginPaint (hWnd, &pm);
                    disp_deb (hdc);
                    EndPaint (hdc, &pm);
                    break;
              case WM_SIZE :
                    break;
              case WM_HSCROLL :
                    break;
              case WM_VSCROLL :
                    break;
              case WM_DESTROY :
                    break;
              case WM_KEYDOWN :
                    switch (wParam)
                    {
                            case VK_RETURN :
                                 PostQuitMessage (0);
                                 return 0;
                    }
                    break;
              case WM_COMMAND :
                    break;
              case WM_SYSCOMMAND:
                      if (wParam == SC_CLOSE)
                      {
                                 PostQuitMessage (0);
                                 DestroyWindow (hWndDeb);
                                 hWndDeb = NULL;
                                 return 0;
                      }
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

void debprintf (char *format, ...)
{
          va_list args;
          char logtext [0x1000];
          HDC hdc;
          int y;
          RECT rect;
          va_start (args, format);
          vsprintf (logtext, format, args);
          va_end (args);

          hdc = GetDC (hWndDeb);
          GetTextMetrics (hdc, &tm);
          y = zeile * tm.tmHeight;
          GetClientRect (hWndDeb, &rect);

          SetTextColor (hdc,BLACKCOL);
                              SetBkColor   (hdc,WHITECOL);
          TextOut (hdc, 0, y, logtext, strlen (logtext));
          ReleaseDC (hWndDeb, hdc);
          
          GetClientRect (hWndDeb, &rect);
          strcpy (zbuff[z], logtext);
}

void ScrollBuff (void)
{
      int i;

      for (i = 0; i < MAXBUFF - 1; i ++)
      {
               strcpy (zbuff[i], zbuff[i + 1]);
      }
}

void addzeile (void)
{
      RECT rect;
      int y;

      GetClientRect (hWndDeb, &rect);

      y = zeile * tm.tmHeight;
      if (y < (rect.bottom - tm.tmHeight))
      {
                       zeile ++;
      }
      else
      {
                       z0 ++;
                       ScrollWindow (hWndDeb, 0, -tm.tmHeight, NULL, NULL);
      }
      if (z < MAXBUFF - 1)
      {
             z ++;
      }
      else
      {
             ScrollBuff ();
      }
}

int OpenEdit (void)
{
      int y;
      RECT rect;

      GetClientRect (hWndDeb, &rect);

      addzeile ();
      y = zeile * tm.tmHeight;
      
      hWndEdit = CreateWindow ("Edit",
                               "",       
                               WS_CHILD | WS_VISIBLE | ES_AUTOHSCROLL,
                               0, y,
                               rect.right, tm.tmHeight,
                               hWndDeb,
                               (HMENU) IdEdit,
                               hInstance,
                               NULL);
      SetFocus (hWndEdit);
      return 0;
}

int IsDebMess (MSG *msg)
{
        switch (msg->message)
        {
               case WM_KEYDOWN :
                  switch (msg->wParam)
                  {
                     case VK_RETURN :
                         SendMessage (hWndDeb, msg->message,
                                      msg->wParam, msg->lParam);
                         return TRUE;
                  }
        }
        return FALSE;
}


int debgets (char *buffer)
{
        MSG msg;
        int fret;

        OpenEdit ();
        while (GetMessage (&msg, NULL, 0, 0))
        {
             fret = IsDebMess (&msg);
             if (fret == 0)
             {
                      TranslateMessage(&msg);
                      DispatchMessage(&msg);
             }
        }
        GetWindowText (hWndEdit, buffer, 79);
        DestroyWindow (hWndEdit);
        hWndEdit = NULL;
        debprintf (buffer);
        return 0;
}

static void RegisterDebug (void)
{
        WNDCLASS wc;
        static int registered = 0;

        if (registered) return;

        hInstance = (HANDLE) GetWindowLong (DebugMain, GWL_HINSTANCE);
        registered = 1;
        wc.style         =  CS_BYTEALIGNWINDOW
                            | CS_CLASSDC;
        wc.lpfnWndProc   =  DebProc;
        wc.cbClsExtra    =  0;
        wc.cbWndExtra    =  0;
        wc.hInstance     =  hInstance;
        wc.hIcon         =  LoadIcon (hInstance, "ROSIICON");
        wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
        wc.hbrBackground =  GetStockObject (WHITE_BRUSH);
        wc.lpszMenuName  =  "";
        wc.lpszClassName =  "DebWindow";

        RegisterClass(&wc);
}

void CreateDebug (void)
/**
Debug-Fenster oeffnen.
**/
{
        RegisterDebug ();

        hWndDeb = CreateWindow ("DebWindow",
                                 "DEBUG",
                                 WS_THICKFRAME | WS_CAPTION | 
                                 WS_SYSMENU, 
                                 CW_USEDEFAULT, CW_USEDEFAULT,
                                 CW_USEDEFAULT, CW_USEDEFAULT,
                                 DebugMain,
                                 NULL,
                                 hInstance,
                                 NULL);

        ShowWindow (hWndDeb, SW_SHOWNORMAL);
        UpdateWindow (hWndDeb);
}

static int show_feld (char *anweisung)
/**
Variableninhalt anzeigen.
**/
{
 short anz;
 char wert2 [1024];

 while (memcmp (anweisung, "dis", 3) == 0)
 {
  anz = split (anweisung);
  if (anz < 2)
    return (-1);
  cr_weg (wort [2]);
  getwert (wort [2], wert2,  (short) strlen (wort [2]));
  addzeile ();
  debprintf ("%s", wert2);
  debgets (anweisung);
 }
 return (0);
}



int wcode_debug (iff *ifs, short mit_debug)
{
 char anweisung [80];

 if (mit_debug == 0)
   return (0);

 if (hWndDeb == NULL)
 {
        CreateDebug ();
 }  

 if (ifs->bedingung)
   debprintf ("%s ", ifs->bedingung);
 if (ifs->anweisung)
   debprintf ("%s ", ifs->anweisung);
// debprintf ("%hd\n", ifs->bflag); 

 debgets (anweisung);

 if (memcmp (anweisung, "dis", 3) == 0)
 {
  show_feld (anweisung);
 }


 if (memcmp (anweisung, "con", 3) == 0)
 {
       mit_debug = 0;
 }

 if (hWndDeb == NULL) 
 {
        mit_debug = 0;
        set_dprog (NULL);
 }

 return (mit_debug);
}

void SetDebug (void)
{
       set_dprog (wcode_debug);
}

void StopDebug (void)
{
       if (hWndDeb)
       {
           DestroyWindow (hWndDeb);
           hWndDeb = NULL;
       }
}

void SetDebugMain (HWND MainhWnd)
{
         DebugMain = MainhWnd;
}

