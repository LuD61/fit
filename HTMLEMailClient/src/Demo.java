import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import net.atlanticbb.tantlinger.io.IOUtils;
import net.atlanticbb.tantlinger.shef.HTMLEditorPane;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

import java.awt.event.KeyEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.border.EtchedBorder;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.SwingConstants;
import javax.swing.JFormattedTextField;
 
/**
 *
 * @author Bob Tantlinger
 */


public class Demo {
	
	private File file = null;
	private String text = "";
	private JFormattedTextField frmtdtxtfldProgrammGeffnet = null;
 
    public Demo() {   	
    	
    	
    	frmtdtxtfldProgrammGeffnet = new JFormattedTextField();    	
        JFrame frame = new JFrame();
        frame.setResizable(false);
        JMenuBar menuBar = new JMenuBar();
        frame.setJMenuBar(menuBar);
        final HTMLEditorPane editor = new HTMLEditorPane();
 
        frame.setTitle("HTML Editor Demo");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(900, 730);        
        frame.setLocation(400, 200);
        
        JMenu mnNewMenu = new JMenu("Datei");
        menuBar.add(mnNewMenu);
        
        JMenuItem mntmNewMenuItem_1 = new JMenuItem("HTML Vorlage \u00F6ffnen");
        mntmNewMenuItem_1.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
        		
				if ( text.equals(editor.getText()) ) {
					
					openFile(editor);					
					
				} else {
					
					int x = JOptionPane.showConfirmDialog(null, "M�chten Sie die vorgenommenen �nderungen an der ge�ffneten Datei speichern?", "�nderungen noch nicht gespeichert"
							, JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
        			if ( x == JOptionPane.YES_OPTION ) {
        				try {
							IOUtils.write(file, editor.getText());
						} catch (FileNotFoundException e1) {
							e1.printStackTrace();
						} catch (IOException e1) {
							e1.printStackTrace();
						}	
        			}
        			
        			openFile(editor);
					
				}
        	}
        });
        
        JMenuItem mntmNeu = new JMenuItem("Neu");
        mntmNeu.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		
        		if ( file == null ) {
        			editor.setText("");
        			text = editor.getText();
        		} else {
        			int x = JOptionPane.showConfirmDialog(null, "M�chten Sie die vorhandene Datei schlie�en?", "Achtung", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
        			if ( x == JOptionPane.YES_OPTION ) {
        				editor.setText("");
        				text = editor.getText();
        			}
        		}
        		
        		
        	}
        });
        mnNewMenu.add(mntmNeu);
        
        mntmNewMenuItem_1.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F8, 0));
        mnNewMenu.add(mntmNewMenuItem_1);
        
        JMenuItem saveMenuItem = new JMenuItem("HTML Vorlage speichern");
        saveMenuItem.addActionListener(new ActionListener() {
        	
        	public void actionPerformed(ActionEvent arg0) {
        		
        		if ( file != null ) {
        			
        			try {
						IOUtils.write(file, editor.getText());
						text = editor.getText();
						frmtdtxtfldProgrammGeffnet.setText(getTimeStamp() + " | " + file.getName());
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
        			
        		} else {        			
        			saveAs(editor);        			
        			text = editor.getText();       			
        		}        		
        	}
        });
        saveMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F12, 0));
        mnNewMenu.add(saveMenuItem);
        
        JMenuItem mntmNewMenuItem_2 = new JMenuItem("Beenden");
        mntmNewMenuItem_2.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
    			
        		int x = JOptionPane.showConfirmDialog(null, "M�chten Sie das Programm jetzt beenden?", "Programm schlie�en", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
    			if ( x == JOptionPane.YES_OPTION ) {
    				System.exit(0);	
    			}
        	}
        });
        
        JMenuItem mntmHtmlVorlageSpeichern = new JMenuItem("HTML Vorlage speichern unter");
        mntmHtmlVorlageSpeichern.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        		saveAs(editor);        			
    			text = editor.getText();
        	}
        });
        mnNewMenu.add(mntmHtmlVorlageSpeichern);
        
        mntmNewMenuItem_2.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0));
        mnNewMenu.add(mntmNewMenuItem_2);
               
        frame.getContentPane().setLayout(null);
               
               
               editor.setBounds(0, 0, 886, 649);
               editor.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
                      
               menuBar.add(editor.getEditMenu());
               menuBar.add(editor.getFormatMenu());
               menuBar.add(editor.getInsertMenu());
                      
        frame.getContentPane().add(editor);
        
        frmtdtxtfldProgrammGeffnet.setText(getTimeStamp());
        frmtdtxtfldProgrammGeffnet.setHorizontalAlignment(SwingConstants.TRAILING);
        frmtdtxtfldProgrammGeffnet.setBounds(0, 652, 886, 27);
        frame.getContentPane().add(frmtdtxtfldProgrammGeffnet);
        
        frame.setVisible(true);       
    }
    
    
    
    private void openFile(final HTMLEditorPane editor) {
		
		InputStream in = null;
		file = null;
	    JFileChooser fc = new JFileChooser();
	    fc.setCurrentDirectory(new File(System.getenv("BWS")));	        	    
	    fc.setFileFilter( new FileNameExtensionFilter("Plaintext: txt, html", "txt", "html", "htm") );        
        fc.removeChoosableFileFilter(fc.getAcceptAllFileFilter());

	    int state = fc.showOpenDialog( null );

	    if ( state == JFileChooser.APPROVE_OPTION )
	    {
	      file = fc.getSelectedFile();
	      try {
			in = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		}       	    
	    
	    if ( file != null ) {
	    	
			try {
	            editor.setText(IOUtils.read(in));
	            text = editor.getText();
	            frmtdtxtfldProgrammGeffnet.setText(getTimeStamp() + " | " + file.getName());
			} catch(IOException ex) {
	            ex.printStackTrace();
			} finally {
	            IOUtils.close(in);
			}
	    	
	    }	
	
    }
    
    
    
    private void saveAs(final HTMLEditorPane editor) {
		
		InputStream in = null;
		file = null;
	    JFileChooser fc = new JFileChooser();
	    fc.setDialogType(JFileChooser.SAVE_DIALOG);
	    fc.setDialogTitle("Speichern unter ...");
	    fc.setCurrentDirectory(new File(System.getenv("BWS")));
	    
        FileNameExtensionFilter markUpFilter = new FileNameExtensionFilter( 
                "Markup: html", "html"); 
        
        fc.removeChoosableFileFilter(fc.getAcceptAllFileFilter()); 
        fc.setFileFilter(markUpFilter);
        fc.setSelectedFile(new File(".html"));
        
	    int state = fc.showSaveDialog(null);

	    if ( state == JFileChooser.APPROVE_OPTION )
	    {
	    	String pfad = fc.getSelectedFile().toString();	    	
	    	File newFile = new File (pfad);
	    	
	    	if (markUpFilter.accept(newFile)) {
		    	try {
					IOUtils.write(newFile, editor.getText());
					file = newFile;
					frmtdtxtfldProgrammGeffnet.setText(getTimeStamp() + " | " + file.getName());					
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} 
	    	} else {
	    		System.out.println(pfad + " ist der falsche Dateityp.");
	    	}	    		 

	    	fc.setVisible(false);
	    	
	    } else {
	    	
	    	fc.setVisible(false);
	    }
	
    }
    
    
    private String getTimeStamp() {
    	
    	final Date now = new java.sql.Date(new Date().getTime());
    	final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd. MMM yyyy HH:mm");
    	final String now_String = simpleDateFormat.format(now);
    	
    	return now_String;
    }
    
    
 
    public static void main(String args[]) {
    	
 
        try {
        	UIManager.setLookAndFeel( "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel" );
            //UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch(Exception ex){}
        
         
        SwingUtilities.invokeLater(new Runnable() {
 
            public void run() {
               new Demo();
            }
        });
    }
}
