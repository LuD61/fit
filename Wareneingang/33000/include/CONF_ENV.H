/***
--------------------------------------------------------------------------------
-
-       BIZERBA
-       Vorstadtstr. 59
-       72351 Geislingen
-
-       Tel.: 07433/96800     Fax: 07433/968030
-
--------------------------------------------------------------------------------

 Fuer dieses Programm behalten wir uns alle Rechte, auch fuer den Fall der
 Patenterteilung und der Eintragung eines anderen gewerblichen Schutz-
 rechtes vor. Missbraeuchliche Verwendung, wie insbesondere Vervielfael-
 tigung und Weitergabe an Dritte ist nicht gestattet; sie kann zivil- und
 strafrechtlich geahndet werden.

--------------------------------------------------------------------------------
-
-       Modulname               :       conf_env.h
-
-       Autor                   :       F.Folmer
-       Erstellungsdatum        :       07.07.95
-       Modifikationsdatum      :       TT.MM.JJ
-
-       Projekt                 :       Schaper
-       Version                 :       1.00
-       Laendervariante         :       BRD
-
-       Rechner                 :       UNISYS 386
-       Betriebssystem          :       XENIX  386
-       Sprache                 :       C
-
-       Aenderungsjournal       :
-
-       lfd.    Version Datum           Name       Beschreibung
-       ------------------------------------------------------------------------
-       1       1.00    09.01.96	F.Folmer  Grundmodul
--------------------------------------------------------------------------------
-
-       Modulbeschreibung       :	Hilfsprogramm
                                        Definition der Environmentvariablen
					fuer OS4680
-				
--------------------------------------------------------------------------------
***/

#define   getenv  config_environment

char * config_environment(char *);
