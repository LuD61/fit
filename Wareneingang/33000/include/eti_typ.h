struct ETI_TYP {
   long      eti;
   char      eti_bz[37];
   short     max_zei_pos;
   short     sg01;
   short     ze01;
   short     sg02;
   short     ze02;
   short     sg03;
   short     ze03;
   short     sg04;
   short     ze04;
   short     sg05;
   short     ze05;
   short     sg06;
   short     ze06;
   short     sg07;
   short     ze07;
   short     sg08;
   short     ze08;
   short     sg09;
   short     ze09;
   short     sg10;
   short     ze10;
   short     sg11;
   short     ze11;
   short     sg12;
   short     ze12;
   short     sg13;
   short     ze13;
   short     sg14;
   short     ze14;
   short     sg15;
   short     ze15;
   short     sg16;
   short     ze16;
   short     sg17;
   short     ze17;
   short     sg18;
   short     ze18;
   short     sg19;
   short     ze19;
   short     sg20;
   short     ze20;
   short     sg_std;
};


extern struct ETI_TYP eti_typ, eti_typ_null;

class ETI_TYP_CLASS 
{
       private :
               short cursor;
               short test_upd_cursor;
               short upd_cursor;
               short ins_cursor;
               short del_cursor;
               void prepare (void);

       public :
               ETI_TYP_CLASS ()
               {
                         cursor       = -1;
                         test_upd_cursor = -1;
                         upd_cursor      = -1;
                         ins_cursor      = -1;
                         del_cursor      = -1;
               }

               int lese_eti_typ (long);
               int lese_eti_typ (void);
               int update_eti_typ (long);
               int delete_eti_typ (long);
               void close_eti_typ (void);
               int ShowAll (void);
               int ShowAllBu (HWND, int);
};

class ETIITEM : virtual public ITEM
{
         private :
   	         int GetDBItem (char *item);
         public :
			 ETIITEM (char *itname, char *value, 
				      char *ittext, int itnum) : 
                  ITEM (itname, value, ittext, itnum)
				  {
				  }	   
             void CallInfo ();
};
