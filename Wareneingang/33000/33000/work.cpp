#include <windows.h>
#include <stdio.h>
#include "Work.h"
#include "strfkt.h"
#include "ptab.h"
#include "searchlgr.h"
#include "spezdlg.h"


#define TLEN 60


static DB_CLASS DbClass;
struct AUSWAHL _auswahl;
int Work::ReadLgr (short mdn, short lgr_nr)
{
    int dsqlstatus = 0;

    DbClass.sqlout ((char *)  _auswahl.lgr_bz, 0, 25);
 //   DbClass.sqlin  ((short *)  &mdn, 1, 0);
    DbClass.sqlin  ((long *) &lgr_nr, 2, 0);
    dsqlstatus = DbClass.sqlcomm ("select lgr_bz from vempf_lgr "
                                  "where lgr = ?");
	if (dsqlstatus == 100) strcpy (_auswahl.lgr_bz, "�ber alle L�ger");
    return 0;
}
int Work::CountBest (short mdn, long lgr_nr,
					 long best_term_von, long best_term_bis,
					 long lief_term_von, long lief_term_bis,
					 long lief_von, long lief_bis,
					 long best_blg_von, long best_blg_bis, char* gbest)
{
    int dsqlstatus = 0;
	short dbearb_stat = 1;
	long dcount = 0;
	char lief_v [17];
	char lief_b [17];
	sprintf(lief_v,"%ld",lief_von);
	sprintf(lief_b,"%ld",lief_bis);

	if (strcmp(gbest, "J") == 0) dbearb_stat = 99;
	if (lgr_nr > 0) 
	{
	    DbClass.sqlout ((long *)  &dcount, 2, 0);
	    DbClass.sqlin  ((short *)  &mdn, 1, 0);
	    DbClass.sqlin  ((long *) &lgr_nr, 2, 0);
//	    DbClass.sqlin  ((long *) &best_term_von, 2, 0);
//	    DbClass.sqlin  ((long *) &best_term_bis, 2, 0);
	    DbClass.sqlin  ((long *) &lief_term_von, 2, 0);
	    DbClass.sqlin  ((long *) &lief_term_bis, 2, 0);
	    DbClass.sqlin  ((char *) lief_v, 0, 16);
	    DbClass.sqlin  ((char *) lief_b, 0, 16);
	    DbClass.sqlin  ((long *) &best_blg_von, 2, 0);
	    DbClass.sqlin  ((long *) &best_blg_bis, 2, 0);
	    DbClass.sqlin  ((short *) &dbearb_stat, 1, 0);
	    dsqlstatus = DbClass.sqlcomm ("select count(*) from best_kopf "
                                  "where mdn = ? and lgr = ? "
								  "and lief_term >= ? and lief_term <= ? "
								  "and lief >= ? and lief <= ? "
								  "and best_blg >= ? and best_blg <= ? "
								  "and bearb_stat <= ? "
								  );
	}
	else
	{
	    DbClass.sqlout ((long *)  &dcount, 2, 0);
	    DbClass.sqlin  ((short *)  &mdn, 1, 0);
//	    DbClass.sqlin  ((long *) &best_term_von, 2, 0);
//	    DbClass.sqlin  ((long *) &best_term_bis, 2, 0);
	    DbClass.sqlin  ((long *) &lief_term_von, 2, 0);
	    DbClass.sqlin  ((long *) &lief_term_bis, 2, 0);
	    DbClass.sqlin  ((char *) lief_v, 0, 16);
	    DbClass.sqlin  ((char *) lief_b, 0, 16);
	    DbClass.sqlin  ((long *) &best_blg_von, 2, 0);
	    DbClass.sqlin  ((long *) &best_blg_bis, 2, 0);
	    DbClass.sqlin  ((short *) &dbearb_stat, 1, 0);
	    dsqlstatus = DbClass.sqlcomm ("select count(*) from best_kopf "
                                  "where mdn = ? "
								  "and lief_term >= ? and lief_term <= ? "
								  "and lief >= ? and lief <= ? "
								  "and best_blg >= ? and best_blg <= ? "
								  "and bearb_stat <= ? "
								  );

	}


	if (dsqlstatus == 100) dcount = 0;
    return dcount;
}

int Work::StatusVorbereiten (short mdn, long lgr_nr,
					 long best_term_von, long best_term_bis,
					 long lief_term_von, long lief_term_bis,
					 long lief_von, long lief_bis,
					 long best_blg_von, long best_blg_bis)
{
    int dsqlstatus = 0;
	int cursor ;
	int cursorupd;
	long lgr_von = 0;
	long lgr_bis = 9999;
	short dbearb_stat = 1;
	long best_blg_akt;
	long lief_akt;
	short blg_z = 0;
	short blg_fz = 0;
	short sqlm;
	extern short sql_mode;
    sqlm = sql_mode;
    sql_mode = 1;
	char lief_v [17];
	char lief_b [17];
	sprintf(lief_v,"%ld",lief_von);
	sprintf(lief_b,"%ld",lief_bis);
	if (lgr_nr > 0) 
	{
		lgr_von = lgr_bis = lgr_nr;
	    DbClass.sqlin  ((short *)  &mdn, 1, 0);
	    DbClass.sqlin  ((long *) &lgr_von, 2, 0);
	    DbClass.sqlin  ((long *) &lgr_bis, 2, 0);
//	    DbClass.sqlin  ((long *) &best_term_von, 2, 0);
//	    DbClass.sqlin  ((long *) &best_term_bis, 2, 0);
	    DbClass.sqlin  ((long *) &lief_term_von, 2, 0);
	    DbClass.sqlin  ((long *) &lief_term_bis, 2, 0);
	    DbClass.sqlin  ((char *) lief_v, 0, 16);
	    DbClass.sqlin  ((char *) lief_b, 0, 16);
	    DbClass.sqlin  ((long *) &best_blg_von, 2, 0);
	    DbClass.sqlin  ((long *) &best_blg_bis, 2, 0);
	    DbClass.sqlout  ((long *) &best_blg_akt, 2, 0);
	    DbClass.sqlout  ((long *) &lief_akt, 2, 0);
	    cursor = DbClass.sqlcursor ("select  best_blg,lief from best_kopf "
                                  "where mdn = ? and lgr >= ? and lgr <= ? "
								  "and lief_term >= ? and lief_term <= ? "
								  "and lief >= ? and lief <= ? "
								  "and best_blg >= ? and best_blg <= ? "
								  "and bearb_stat <= 1 "
								  );
	}
	else
	{
	    DbClass.sqlin  ((short *)  &mdn, 1, 0);
//	    DbClass.sqlin  ((long *) &best_term_von, 2, 0);
//	    DbClass.sqlin  ((long *) &best_term_bis, 2, 0);
	    DbClass.sqlin  ((long *) &lief_term_von, 2, 0);
	    DbClass.sqlin  ((long *) &lief_term_bis, 2, 0);
	    DbClass.sqlin  ((char *) lief_v, 0, 16);
	    DbClass.sqlin  ((char *) lief_b, 0, 16);
	    DbClass.sqlin  ((long *) &best_blg_von, 2, 0);
	    DbClass.sqlin  ((long *) &best_blg_bis, 2, 0);
	    DbClass.sqlout  ((long *) &best_blg_akt, 2, 0);
	    DbClass.sqlout  ((long *) &lief_akt, 2, 0);
	    cursor = DbClass.sqlcursor ("select  best_blg,lief  from best_kopf "
                                  "where mdn = ? "
								  "and lief_term >= ? and lief_term <= ? "
								  "and lief >= ? and lief <= ? "
								  "and best_blg >= ? and best_blg <= ? "
								  "and bearb_stat <= 1 "
								  );
	}
	/*
	    DbClass.sqlin  ((long *) &best_blg_akt, 2, 0);
	    DbClass.sqlin  ((long *) &lief_akt, 2, 0);
	    DbClass.sqlin  ((short *)  &mdn, 1, 0);
	    cursorupd = DbClass.sqlcursor ("update best_kopf set mdn = -1 "
                                  "where best_blg = ? and lief_s = ? "
								  "and mdn = ? and fil = 0 "
								  );
*/


	   dsqlstatus = 0;
	   dsqlstatus = DbClass.sqlopen (cursor);
	   dsqlstatus = DbClass.sqlfetch (cursor);
       while (dsqlstatus == 0)
	   {
	    DbClass.sqlin  ((long *) &best_blg_akt, 2, 0);
	    DbClass.sqlin  ((long *) &lief_akt, 2, 0);
	    DbClass.sqlin  ((short *)  &mdn, 1, 0);
		dsqlstatus =  DbClass.sqlcomm ("update best_kopf set  delstatus = 2 "
                      "where best_blg = ? and lief_s = ? "
					  "and mdn = ? and fil = 0 "
					  );
				if (dsqlstatus != 0)
				{
                    print_mess (2,"Beleg %d ist noch in Bearbeitung und kann noch nicht gedruckt werden",best_blg_akt);
					blgf_tab[blg_fz] = best_blg_akt; blg_fz++;
				}
				else
				{
					blg_tab[blg_z] = best_blg_akt; 
					lief_tab[blg_z] = lief_akt; 
					blg_z++;
				}
				dsqlstatus = DbClass.sqlfetch (cursor);
	   }
	    blganz = blg_z;
	 	DbClass.sqlclose (cursor);
	 	DbClass.sqlclose (cursorupd);
		sql_mode = sqlm;


    return blganz;
}


int Work::StatusAendern (short mdn, long lgr_nr,
					 long best_term_von, long best_term_bis,
					 long lief_term_von, long lief_term_bis,
					 long lief_von, long lief_bis,
					 long best_blg_von, long best_blg_bis)
{
 	extern short sql_mode;
	int i;
    int dsqlstatus;
	long best_blg_akt;
	long lief_akt;
	short sqlm;
    sqlm = sql_mode;
    sql_mode = 1;

	if (blganz == 0) return 0;
    for (i = 0; i < blganz; i ++)
	{
        lief_akt = lief_tab[i];
        best_blg_akt = blg_tab[i];
	    DbClass.sqlin  ((long *) &best_blg_akt, 2, 0);
		DbClass.sqlin  ((long *) &lief_akt, 2, 0);
		DbClass.sqlin  ((short *)  &mdn, 1, 0);
		dsqlstatus = DbClass.sqlcomm ("update best_kopf set bearb_stat = 2, delstatus = 0 "
                      "where best_blg = ? and lief_s = ? "
					  "and mdn = ? and fil = 0 "
					  );
	}

	for (i = 0; i < blganz; i ++)
	{
        lief_tab[i] = 0;
        blg_tab[i] = 0;
	}
	sql_mode = sqlm;
	return 0;
}


int Work::ReadFrm (short mdn, short frm_nr)
{
    int dsqlstatus = 0;

    return 0;
}

int Work::GetPtab (char *dest, char *item, char *wert)
{
    int dsqlstatus;

    ptabn.ptlfnr = 1;
    dest[0] = 0;
    dsqlstatus = Ptab.lese_ptab (item, wert);  
    if (dsqlstatus == 100)
    {
        return -1;
    }
    strcpy (dest, ptabn.ptbezk);
    return 0;
}

char *Work::GetPtWert (char *item, short lfd)
{
    int dsqlstatus;

    DbClass.sqlout ((char *)  ptabn.ptwert, 0, 4);
    DbClass.sqlin  ((char *)  item, 0, 20);
    DbClass.sqlin  ((short *) &lfd, 1, 0);
    dsqlstatus = DbClass.sqlcomm ("select ptwert from ptabn "
                                  "where ptitem = ? and ptlfnr = ?");
    if (dsqlstatus == 0)
    {
        return ptabn.ptwert;
    }
    return "";
}

int Work::ShowPtab (char *dest, char *item)
{
    int dsqlstatus;

    dsqlstatus = Ptab.Show (item);  
    if (syskey == KEY5)
    {
        return -1;
    }
    strcpy (dest, ptabn.ptwert);
    return 0;
}


void Work::FillRow (char *buffer)
{
/*
         sprintf (buffer, "%8ld     |%-36s|   |%-20s|   |%-36s|",
                      maschienen.adr, _adr.adr_nam1, _adr.tel,
                      _adr.ort1);
*/
}

void Work::FillCombo (char **Combo, char *Item, int anz)
{
    int dsqlstatus;
    char *anr;
    
    int i = 0;
    dsqlstatus = Ptab.lese_ptab_all (Item);
    while (dsqlstatus == 0)
    {
        anr = new char [20];
        if (anr == NULL) return;
        strcpy (anr, ptabn.ptbezk);
        Combo[i] = anr;
        i ++;
        if (i ==anz) break;
        dsqlstatus = Ptab.lese_ptab_all ();
    }
    Combo[i] = NULL;
}

void Work::FillComboLong (char **Combo, char *Item, int anz)
{
    int dsqlstatus;
    char *anr;
    
    int i = 0;
    dsqlstatus = Ptab.lese_ptab_all (Item);
    while (dsqlstatus == 0)
    {
        anr = new char [256];
        if (anr == NULL) return;
        strcpy (anr, ptabn.ptbez);
        Combo[i] = anr;
        i ++;
        if (i ==anz) break;
        dsqlstatus = Ptab.lese_ptab_all ();
    }
    Combo[i] = NULL;
}


int Work::GetMdnName (char *dest, short mdn)
{
    int dsqlstatus;

    _mdn.konversion = 1.95583;
    if (mdn == 0)
    {
        strcpy (dest, "Zentrale");
        return 0;
    }
    dest[0] = 0;
    DbClass.sqlin ((short *) &mdn, 1, 0);
    DbClass.sqlout ((char *) dest, 0, 17);
    dsqlstatus =  DbClass.sqlcomm ("select adr.adr_krz from mdn,adr "
                                   "where mdn.mdn = ? " 
                                   "and adr.adr = mdn.adr");
    if (dsqlstatus == 100)
    {
        return 100;
    }
    Mdn.lese_mdn (mdn);
    if (_mdn.konversion == 0)
    {
        _mdn.konversion = 1.0;
    }
    return 0;
}

int Work::CutLines (char *txt, char *txtout, int zei, int cursor)
{
	   char *params [5];
	   char zeile[5];
	   char tlen[5];

	   if (strlen (txt) <= TLEN)
	   {
		   cr_weg (txtout);
	       strcpy (txtout, txt);
           DbClass.sqlexecute (cursor);
		   return (zei + 1);
	   }

	   params[0] = txt;
	   params[1] = txtout;
	   sprintf (zeile, "1");
	   sprintf (tlen, "%d", TLEN);
	   params[2] = tlen;
	   params[3] = zeile;
	   params[4] = NULL;
	   while (getline (params))
	   {
		   cr_weg (txtout);
           DbClass.sqlexecute (cursor);
		   zei ++;
		   sprintf (zeile, "%d", atoi (zeile) + 1);
	   }
	   return zei;
}


void Work::BeginWork (void)
{
    ::beginwork ();
}

void Work::CommitWork (void)
{
    ::commitwork ();
}

void Work::RollbackWork (void)
{
    ::rollbackwork ();
}

void Work::InitRec (void)
{
//    memcpy (&kost_art, &kost_art_null, sizeof (kost_art));
}