#include<stdio.h>
#include<stdarg.h>
#include<string.h>
#include<windows.h>
#include "stdfkt.h"
#include "strfkt.h"
#include "SpezDlg.h" 
#include "mo_gdruck.h" 
#include "colbut.h"
#include "help.h"
#include "lbox.h"
#include "searchmdn.h"
#include "searchlgr.h"
#include "searchfrm.h"
#include "mo_llgen.h"
#include "ptab.h"
#ifdef BIWAK
#include "conf_env.h"
#endif
#include "mo_progcfg.h"


static int StdSize = STDSIZE;
static int flgMinute = 0;

static int dlgsize    = STDSIZE;
static int ltgraysize = STDSIZE;
static int dlgpossize = STDSIZE;
static int cpsize     = 200;
SEARCHFRM SearchFrm;
 

static mfont dlgfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         GRAYCOL,
                         0,
                         NULL};

static mfont ltgrayfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         LTGRAYCOL,
                        0,
                         NULL};

static mfont checkfonth = {
                           "ARIAL", STDSIZE, 0, 0,
                            BLACKCOL,
                            LTGRAYCOL,
                            0,
                            NULL};

static mfont dlgposfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         LTGRAYCOL,
                         0,
                         NULL};


static mfont cpfont = {
                        "Times New Roman", 200, 0, 1,
                         BLUECOL,
                         GRAYCOL,
                         0,
                         NULL};

static mfont *Font = &dlgposfont;

struct AUSWAHLF
{
      char mdn [7];
      char mdn_krz [20];
      char lgr_nr [11];
      char lgr_bz [25];
	  char lief_von [12];
	  char lief_bis [12];
	  char best_blg_von [12];
	  char best_blg_bis [12];
	  char best_term_von [15];
	  char best_term_bis [15];
	  char lief_term_von [15];
	  char lief_term_bis [15];
	  char gedruckt [12];
	  char frm_typ [12];
	  char frm_bz [50];
	  char best_anzahl [12];
} auswahlf, auswahlf_save;




CFIELD *_fHead [] = {
                     new CFIELD ("mdn_txt", "Mandant",  0, 0, 1, 0,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("mdn", auswahlf.mdn,  6, 0, 20, 0,  NULL, "%4d", CEDIT,
                                 MDN_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("mdn_choise", "", 2, 0, 26, 0, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("mdn_krz", auswahlf.mdn_krz, 19, 0, 30, 0,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, 0),

/*
                     new CFIELD ("lgr_txt", "Empfangslager :",  0, 0, 1, 2,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("lgr_nr", auswahlf.lgr_nr, 6, 0, 20, 2,  NULL, "%4ld", CEDIT,
                                 LGR_NR_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("lgr_choise", "", 2, 0, 26, 2, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
*/
                     new CFIELD ("anz_txt", "Anzahl Bestellungen :",  0, 0, 65, 4,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("best_anzahl", auswahlf.best_anzahl,4, 0, 83, 4,  NULL, "%ld", CREADONLY,
                                 BEST_ANZ_CTL, Font, 0, 0),
                     NULL,
};

CFORM fHead (6, _fHead);


CFIELD *_fPos [] = {
                     new CFIELD ("pos_frame", "",    87,13, 1, 4,  NULL, "",
                                 CBORDER,   
                                 500, Font, 0, TRANSPARENT),    

                     new CFIELD ("von_txt", "von",  0, 0, 30, 5,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("bis_txt", "bis",  0, 0, 50, 5,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
/**
                     new CFIELD ("lgr_bz", auswahlf.lgr_bz,25, 0, 30, 2,  NULL, "", CREADONLY,
                                 LGR_BZ_CTL, Font, 0, 0),

**/
/****
                     new CFIELD ("vkost_wt_txt", "Bestelltermin",  0, 0, 5, 6,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                      new CFIELD ("best_term_von", auswahlf.best_term_von, 11, 0, 30, 6,  NULL, "dd.mm.yyyy", CEDIT,
                                 BEST_TVON_CTL, Font, 0, ES_RIGHT),
                      new CFIELD ("best_term_bis", auswahlf.best_term_bis, 11, 0, 50, 6,  NULL, "dd.mm.yyyy", CEDIT,
                                 BEST_TBIS_CTL, Font, 0, ES_RIGHT),

  *****/

                     new CFIELD ("vkost_wt_txt", "Liefertermin",  0, 0, 5, 7,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                      new CFIELD ("lief_term_von", auswahlf.lief_term_von, 11, 0, 30, 7,  NULL, "dd.mm.yyyy", CEDIT,
                                 LIEF_TVON_CTL, Font, 0, ES_RIGHT),
                      new CFIELD ("lief_term_bis", auswahlf.lief_term_bis, 11, 0, 50, 7,  NULL, "dd.mm.yyyy", CEDIT,
                                 LIEF_TBIS_CTL, Font, 0, ES_RIGHT),


                     new CFIELD ("vkost_wt_txt", "Lieferant",  0, 0, 5, 8,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                      new CFIELD ("lief_von", auswahlf.lief_von, 10, 0, 30, 8,  NULL, "%ld", CEDIT,
                                 LIEF_VON_CTL, Font, 0, ES_RIGHT),
                      new CFIELD ("lief_bis", auswahlf.lief_bis, 10, 0, 50, 8,  NULL, "%8d", CEDIT,
                                 LIEF_BIS_CTL, Font, 0, ES_RIGHT),


                    new CFIELD ("vkost_wt_txt", "Bestellbelegnummer",  0, 0, 5, 9,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                      new CFIELD ("best_blg_von", auswahlf.best_blg_von, 10, 0, 30, 9,  NULL, "%8ld", CEDIT,
                                 BEST_VON_CTL, Font, 0, ES_RIGHT),
                      new CFIELD ("best_blg_bis", auswahlf.best_blg_bis, 10, 0, 50, 9,  NULL, "%8ld", CEDIT,
                                 BEST_BIS_CTL, Font, 0, ES_RIGHT),


                     new CFIELD ("gedruckt", "gedruckte Bestellungen  :", auswahlf.gedruckt,
                                  27, 0, 5, 10,  NULL, "", CBUTTON,
                                 GBEST_CTL, Font, 0, BS_AUTOCHECKBOX | BS_LEFTTEXT),


                     new CFIELD ("vkost_wt_txt", "Formulartyp",  0, 0, 5, 12,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("frm_typ", auswahlf.frm_typ,  12, 0, 30, 12,  NULL, "%8ld", CEDIT,
                                 FRM_TYP_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("frm_typ_choise", "", 2, 0, 42, 12, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("frm_bz", auswahlf.frm_bz, 41, 0, 46, 12,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, 0),


                     new CFIELD ("dp1_txt", ":",  0, 0, 25, 6,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("dp2_txt", ":",  0, 0, 25, 7,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("dp3_txt", ":",  0, 0, 25, 8,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("dp3_txt", ":",  0, 0, 25, 9,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("dp5_txt", ":",  0, 0, 25, 12,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     NULL,
};


CFORM fPos (25, _fPos);

CFIELD *_fList [] = {
       			     new CFIELD ("auswahllst", "",
				                     87,9, 1,14, NULL, "", CLISTBOX,
												  AUSWAHL_LST_CTL, Font, 0, 0),
                     NULL,
};

CFORM fList (1, _fList);
    

CFIELD *_fFoot [] = {
                     new CFIELD ("ok",      "    OK    ", 10, 0, 0, 0,  NULL, "", 
                                 CBUTTON,
                                 OK_CTL, Font, 0, 0),
                     new CFIELD ("cancel", " Abbruch  ", 10, 0,12, 0,  NULL, "", 
                                 CBUTTON,
                                 CANCEL_CTL, Font, 0, 0),
                     NULL,
};


CFORM fFoot (2, _fFoot);


CFIELD *_fauswahl [] = {
                     new CFIELD ("fauswahl1", (CFORM *) &fHead, 0, 0, 0, 0, NULL, "", CFFORM,
                                   HEADCTL, Font, 0, 0),
                     new CFIELD ("fauswahl2", (CFORM *) &fPos,  0, 0, 0, 0, NULL, "", CFFORM,
                                   POSCTL, Font, 0, 0),
                     NULL,                      
};

CFORM fauswahl (2, _fauswahl);

CFIELD *_fauswahl0 [] = { 
                     new CFIELD ("fauswahl", (CFORM *) &fauswahl, 0, 0, -1, -1, NULL, "", CFFORM,
                                   500, Font, 0, 0),
                     NULL,
};
CFORM fauswahl0 (1, _fauswahl0);

Work SpezDlg::work;
int SpezDlg::ListRows = 0;
SEARCHLGR SpezDlg::Searchlgr;
BOOL SpezDlg::InsMode = FALSE;
BOOL SpezDlg::WriteOK = FALSE;
char *SpezDlg::HelpName = "33000.cmd";
SpezDlg *SpezDlg::ActiveSpezDlg = NULL;
HBITMAP SpezDlg::SelBmp;
BitImage SpezDlg::ImageDelete;
BitImage SpezDlg::ImageInsert;
COLORREF SpezDlg::SysBkColor = LTGRAYCOL;
char *SpezDlg::lastfield = "frm_typ";
int SpezDlg::pos = 0;
char SpezDlg::ptwert[5];


void SpezDlg::SetListDimension (int cx, int cy)
{
         _fList[0]->SetCX (cx);
         _fList[0]->SetCY (cy);
}

void SpezDlg::DiffListDimension (int cx, int cy)
{
         _fList[0]->SetCX (_fList[0]->GetCXorg () + cx);
         _fList[0]->SetCY (_fList[0]->GetCYorg () + cy);
}


int SpezDlg::ReadMdn (void)
{
         if (syskey == 0) return 0;
         if (syskey == KEY9) return 0;
         if (syskey == KEY5) return 0;

//         GetPosText (); 
         FromForm (dbheadfields);
         work.GetMdnName (auswahlf.mdn_krz,   atoi (auswahlf.mdn));
         if (atoi (auswahlf.mdn) == 0)
         {
             disp_mess ("Mandant <> 0 eingeben", 2);
             fHead.SetCurrentName ("mdn");
         }
         if (syskey == KEYUP)
         {
                    fHead.SetCurrentName ("mdn");
         }
         fHead.SetText ();
         ToForm (dbfields); 

         SetCombos ();

         fPos.SetText ();

         return 0;
}

int SpezDlg::EnableFaktor (void)
{
         if (syskey == 0) return 0;
         if (syskey == KEY9) return 0;
         if (syskey == KEY5) return 0;

         FromForm (dbheadfields);
         GetCombos ();
         return 0;
}

int SpezDlg::EnterPos (void)
{
	char cdatum[15];
         switch (syskey)
         {
               case KEYDOWN :
               case KEYTAB :
                   fHead.SetCurrentName ("mdn");
                   return TRUE;
         }
        
         if (DLG::MousePressed)
         {
             return FALSE;
         }

         if (syskey != KEYCR)
         {
             return FALSE;
         }
         
         fHead.GetText ();
		 _auswahl.lief_von = 1;
		 _auswahl.lief_bis = 99999999;
		 _auswahl.best_blg_von = 1;
		 _auswahl.best_blg_bis = 99999999;
		 sysdate(cdatum);
//		 _auswahl.best_term_von = dasc_to_long (cdatum) ;
//		 _auswahl.best_term_bis = dasc_to_long (cdatum);
//		 _auswahl.lief_term_von = _auswahl.best_term_von + 1;
//		 _auswahl.lief_term_bis = _auswahl.best_term_bis + 1 ;
		 _auswahl.lief_term_von = dasc_to_long (cdatum) ;
		 _auswahl.lief_term_bis = dasc_to_long (cdatum) + 10 ;
		 strcpy(auswahlf.frm_typ,"33000");
		 strcpy(auswahlf.frm_bz,SearchFrm.ReadFrmBz(auswahlf.frm_typ));
		 strcpy(auswahlf.lgr_nr,"0");
         ToForm (dbfields); 


         FromForm (dbheadfields);

         work.BeginWork ();
             FillEnterList (0);
             ToolBar_SetState (ActiveDlg->GethwndTB (),   IDM_DELALL,   TBSTATE_ENABLED);
             EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELALL,   MF_ENABLED);
         Enable (&fauswahl, EnableHead, FALSE); 
         Enable (&fauswahl, EnablePos,  TRUE); 
		 CountBest();
//         fPos.SetCurrentName ("best_term_von");
         fPos.SetCurrentName ("lief_term_von");

         return 0;
}

int SpezDlg::ListChanged (WPARAM wParam, LPARAM lParam)
{
/*
        int idx;

        if (LOWORD (wParam) != PARTNER_LST_CTL) return 0; 
        InsMode = FALSE;
        if (WriteOK) return 0;

        fMasch.GetText ();
        if (atol (maschf.adr) == 0) return 0;
        FromForm (dbfields); 
        GetCombos ();

        _adr.geb_dat = dasc_to_long (maschf.geb_dat);
        work.WriteAdr ();

        idx = SendMessage (_fList[0]->GethWnd (), LB_GETCURSEL, 0, 0l);
        if (idx != -1)
        {
            FillEnterList (idx);
        }
*/
        return TRUE;
}

void SpezDlg::FillEnterList (int idx)
{
/*
         char text [512];
         int anz;
         HWND lBox;

         lBox = _fList[0]->GethWnd ();
         if (lBox == NULL) return;

	     SendMessage (lBox, LB_GETTEXT, idx, (LPARAM) text);
         anz = wsplit (text, " ");
         if (anz == 0) return;
         sprintf (maschf.adr, "%8ld", atol (wort[0]));
         fPos.SetText ();
         ReadAdr ();
*/
}

         
int SpezDlg::ReadLgr (void)
{
         int dsqlstatus;
          
         fHead.GetText ();

         WriteOK = FALSE;
         dsqlstatus = work.ReadLgr 
             (atoi(auswahlf.mdn), atoi (auswahlf.lgr_nr));
         ToForm (dbfields); 

         SetCombos ();

         fPos.SetText ();
         return TRUE;
}
int SpezDlg::CountBest (void)
{
          
         fHead.GetText ();

         WriteOK = FALSE;

         FromForm (dbfields); 
		 if (strcmp(clipped(auswahlf_save.lgr_nr),clipped(auswahlf.lgr_nr)) != 0  ||
             strcmp(clipped(auswahlf_save.lief_von),clipped(auswahlf.lief_von)) != 0   ||  
             strcmp(clipped(auswahlf_save.lief_bis),clipped(auswahlf.lief_bis)) != 0   ||  
             strcmp(clipped(auswahlf_save.best_blg_von),clipped(auswahlf.best_blg_von)) != 0   ||  
             strcmp(clipped(auswahlf_save.best_blg_bis),clipped(auswahlf.best_blg_bis)) != 0   ||  
//             strcmp(clipped(auswahlf_save.best_term_von),clipped(auswahlf.best_term_von)) != 0   ||  
//             strcmp(clipped(auswahlf_save.best_term_bis),clipped(auswahlf.best_term_bis)) != 0   ||  
             strcmp(clipped(auswahlf_save.lief_term_von),clipped(auswahlf.lief_term_von)) != 0   ||  
             strcmp(clipped(auswahlf_save.lief_term_bis),clipped(auswahlf.lief_term_bis)) != 0   ||  
             strcmp(clipped(auswahlf_save.gedruckt),clipped(auswahlf.gedruckt)) != 0) 
		 {

				_auswahl.best_anzahl = work.CountBest 
	             (atoi(auswahlf.mdn), atoi (auswahlf.lgr_nr),
				dasc_to_long(auswahlf.best_term_von),dasc_to_long(auswahlf.best_term_bis),
				dasc_to_long(auswahlf.lief_term_von),dasc_to_long(auswahlf.lief_term_bis),
				atoi(auswahlf.lief_von), atoi (auswahlf.lief_bis),
				atoi(auswahlf.best_blg_von), atoi (auswahlf.best_blg_bis),
				auswahlf.gedruckt);
				memcpy (&auswahlf_save, &auswahlf, sizeof (struct AUSWAHLF));
				SetCombos ();
		 }

		 ToForm (dbfields); 
         fHead.SetText ();
         return TRUE;
}
int SpezDlg::ReadFrm (void)
{
         int dsqlstatus;
          
         fPos.GetText ();

         WriteOK = FALSE;
         FromForm (dbfields); 
         dsqlstatus = work.ReadFrm 
             (atoi(auswahlf.mdn), atoi (auswahlf.frm_typ));
         ToForm (dbfields); 

         SetCombos ();

//         fPos.SetText ();

         return TRUE;
}

int SpezDlg::GetPtab (void)
{
//         char wert [5];
         CFIELD *Cfield;


         fPos.GetText ();

         Cfield = ActiveDlg->GetCurrentCfield ();
/*
         if (strcmp (Cfield->GetName (), "me_einh_ek") == 0)
         {
             sprintf (wert, "%d", atoi (bzgf.me_einh_ek));
             work.GetPtab (bzgf.me_einh_ek_bez, "me_einh_ek", wert);
             fPos.GetCfield ("me_einh_ek_bez")->SetText ();         
         }
*/
         return 0;
}


ItProg *SpezDlg::After [] = {
                                   new ItProg ("mdn",         ReadMdn),
                                   new ItProg ("lgr_nr",         ReadLgr),
                                   new ItProg ("frm_typ",         ReadFrm),
                                   new ItProg ("lief_von",         CountBest),
                                   new ItProg ("lief_bis",         CountBest),
                                   new ItProg ("best_blg_von",         CountBest),
                                   new ItProg ("best_blg_bis",         CountBest),
//                                   new ItProg ("best_term_von",         CountBest),
//                                   new ItProg ("best_term_bis",         CountBest),
                                   new ItProg ("lief_term_von",         CountBest),
                                   new ItProg ("lief_term_bis",         CountBest),
                                   new ItProg ("gedruckt",                 CountBest),
                                   NULL,
};

ItProg *SpezDlg::Before [] = {NULL,
};

ItFont *SpezDlg::Font [] = {
//                                  new ItFont ("a_txt",           &ltgrayfont),
                                  NULL
};

FORMFIELD *SpezDlg::dbheadfields [] = {
         new FORMFIELD ("mdn",         auswahlf.mdn,         (short *)   &_auswahl.mdn,        FSHORT,   NULL),
//         new FORMFIELD ("lgr_nr",       auswahlf.lgr_nr ,      (long *)    &_auswahl.lgr_nr,       FLONG,    NULL),
};

FORMFIELD *SpezDlg::dbfields [] = {
//         new FORMFIELD ("lgr_bz",  auswahlf.lgr_bz,  (char *)   _auswahl.lgr_bz,      FCHAR,    NULL),
        new FORMFIELD ("lief_von",  auswahlf.lief_von,  (long *)   &_auswahl.lief_von,      FLONG,    NULL),
         new FORMFIELD ("lief_bis",  auswahlf.lief_bis,  (long *)   &_auswahl.lief_bis,      FLONG,    NULL),
         new FORMFIELD ("best_blg_von",  auswahlf.best_blg_von,  (long *)   &_auswahl.best_blg_von,      FLONG,    NULL),
         new FORMFIELD ("best_blg_bis",  auswahlf.best_blg_bis,  (long *)   &_auswahl.best_blg_bis,      FLONG,    NULL),
//         new FORMFIELD ("best_term_von",  auswahlf.best_term_von,  (long *)   &_auswahl.best_term_von,      FDATE,    NULL),
//         new FORMFIELD ("best_term_bis",  auswahlf.best_term_bis,  (long *)   &_auswahl.best_term_bis,      FDATE,    NULL),
         new FORMFIELD ("lief_term_von",  auswahlf.lief_term_von,  (long *)   &_auswahl.lief_term_von,      FDATE,    NULL),
         new FORMFIELD ("lief_term_bis",  auswahlf.lief_term_bis,  (long *)   &_auswahl.lief_term_bis,      FDATE,    NULL),
         new FORMFIELD ("best_anzahl",  auswahlf.best_anzahl,  (long *)   &_auswahl.best_anzahl,      FLONG,    NULL),
         NULL,
};



char *SpezDlg::EnableHead [] = {
                                "mdn",
                                "mdn_choise",
//                                "lgr_nr",
//                                "lgr_choise",
                                 NULL
};


char *SpezDlg::EnablePos[] = {
                                  "lief_von", 
                                  "lief_bis", 
                                  "best_blg_von", 
                                  "best_blg_bis", 
//                                  "best_term_von", 
//                                  "best_term_bis",
                                  "lief_term_von", 
                                  "lief_term_bis",
                                  "gbest",
								  "frm_typ",
								  "frm_typ_choise",
                                  NULL,
};

char *SpezDlg::EnableMinute[] = {
                                  "faktor", 
                                  NULL,
};


char *SpezDlg::RechbCombo[200]; 

                          
SpezDlg::SpezDlg (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel) :
         DLG (x, y, cx, cy, Caption, Size, Pixel)
{
          
             BorderType = HIGHCOLBORDER;
             Init (x, y, cx, cy, Caption, Size, Pixel);
}

SpezDlg::SpezDlg (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel, 
                  int BorderType) :
         DLG (x, y, cx, cy, Caption, Size, Pixel)
{
          
             this->BorderType = BorderType;
             Init (x, y, cx, cy, Caption, Size, Pixel);
}

SpezDlg::SpezDlg (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel, 
                  int BorderType, int EnterM) :
         DLG (x, y, cx, cy, Caption, Size, Pixel)
{
          
             this->BorderType = BorderType;
//             EnterMode = EnterM;
             Init (x, y, cx, cy, Caption, Size, Pixel);
}


void SpezDlg::Init (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel)
{
             PROG_CFG ProgCfg ("33000");
//             char cfg_v [256];
             int i;
             int xfull, yfull;

//             Work.Prepare ();

             ImageDelete.Image    = BMAP::LoadBitmap (hInstance, "DEL",       "DELMASK",      SysBkColor);
             ImageDelete.Inactive = BMAP::LoadBitmap (hInstance, "DELIA",     "DELMASK",      SysBkColor);
             ImageInsert.Image    = BMAP::LoadBitmap (hInstance, "INSERT",    "INSERTMASK",   SysBkColor);
             ImageInsert.Inactive = BMAP::LoadBitmap (hInstance, "INSERTIA",  "INSERTMASK",   SysBkColor);

             ActiveSpezDlg = this; 
             xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
             yfull = GetSystemMetrics (SM_CYFULLSCREEN);        


             StdSize = Size;
             dlgfont.FontHeight    = Size;
             dlgposfont.FontHeight = Size;
             ltgrayfont.FontHeight = Size;
             cpfont.FontHeight     = cpsize;


             if (xfull < 1000) 
             {
                 dlgfont.FontHeight = 90;
                 dlgposfont.FontHeight = 90;
                 ltgrayfont.FontHeight = 90;
                 checkfonth.FontHeight = 90;
                 cpfont.FontHeight = 150;
             }

             if (xfull < 800) 
             {

                 dlgfont.FontHeight = 70;
                 dlgposfont.FontHeight = 70;
                 ltgrayfont.FontHeight = 70;
                 checkfonth.FontHeight = 70;
                 cpfont.FontHeight = 120;
             }

//             fPos.SetCfield (_fPos1); 
             fHead.SetFieldanz ();
             fPos.SetFieldanz ();
             fFoot.SetFieldanz ();
             fauswahl.SetFieldanz ();

            fHead.GetCfield ("mdn_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
            fPos.GetCfield ("frm_typ_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
//            fHead.GetCfield ("lgr_choise")->SetBitmap 
//                 (LoadBitmap (hInstance, "ARRDOWN"));

             fHead.GetCfield ("mdn_choise")->SetTabstop (FALSE); 
             fPos.GetCfield ("frm_typ_choise")->SetTabstop (FALSE); 
//             fHead.GetCfield ("lgr_choise")->SetTabstop (FALSE); 

             for (i = 0; i < fHead.GetFieldanz (); i ++)
             {
//                       fHead.GetCfield ()[i]->SetFont (&dlgfont); 
//                       fHead.GetCfield ()[i]->SetPosFont (&dlgposfont); 
             }

             SetTmFont (&dlgfont);
             for (i = 0; Before[i] != NULL; i ++)
             {
                 Before[i]->SetBefore (&fauswahl);
             }

             for (i = 0; After[i] != NULL; i ++)
             {
                 After[i]->SetAfter (&fauswahl);
             }


             if (BorderType == HIGHCOLBORDER)
             {

                for (i = 0; Font[i] != NULL; i ++)
                {
                 Font[i]->SetFont (&fauswahl);
                }

                fPos.GetCfield ("pos_frame")->SetBorder 
                 (new FILLEDBORDER (BLACKCOL, WHITECOL, HIGH, 0, LTGRAYCOL)); 
             }
             else if (BorderType == LOWCOLBORDER)
             {

                for (i = 0; Font[i] != NULL; i ++)
                {
                 Font[i]->SetFont (&fauswahl);
                }
                fPos.GetCfield ("pos_frame")->SetBorder 
                 (new FILLEDBORDER (BLACKCOL, WHITECOL, LOW, 0, LTGRAYCOL)); 
             }
             else if (BorderType == HIGHBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, HIGH); 
             }
             else if (BorderType == LOWBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, LOW); 
             }
             else if (BorderType == RAISEDBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, RAISED); 
             }
             else if (BorderType == LINEBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, LINE); 
             }
			 /*
             work.FillCombo (RechbCombo, "vkost_rechb", 20);
             if (RechbCombo [0] != NULL)
             {
                     strcpy (auswahlf.vkost_rechb, RechbCombo [0]);
             }
             fPos.GetCfield ("vkost_rechb")->SetCombobox (RechbCombo); 
			 */

             Enable (&fauswahl, EnableHead, TRUE); 
             Enable (&fauswahl, EnablePos,  FALSE);

             _fList[0]->Setchsize (0, 1);
              SetDialog (&fauswahl0);
}

/*
void SpezDlg::FillHeadfields (void)
{
        int dsqlstatus;

        memcpy (&maschf, &maschf_null, sizeof (struct MASCHF));
        sprintf (maschf.mdn,  "%4d", masch.mdn);  
        sprintf (maschf.fil,  "%4d", masch.fil);  
        sprintf (maschf.kun,  "%ld", masch.kun);  
        sprintf (maschf.adr,  "%ld", masch.adr);  
        dsqlstatus = work.ReadKun (masch.mdn, masch.fil, masch.kun); 
        if (dsqlstatus == 0)
        {
               sprintf (maschf.mdn,  "%4d", masch.mdn);  
               sprintf (maschf.fil,  "%4d", masch.fil);  
        }
        else
        {
               masch.mdn = atoi (maschf.mdn);  
               masch.fil = atoi (maschf.fil);  
        }

        work.GetMdnName (maschf.mdn_krz,   atoi (maschf.mdn));
        work.GetFilName (maschf.fil_krz,   atoi (maschf.mdn), atoi (maschf.fil));
        work.GetKunName (maschf.kun_krz,   masch.mdn, masch.kun);

        ToForm (dbfields); 
        masch.kun =  atol (maschf.kun);  
}
*/

BOOL SpezDlg::OnClicked (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{       
        HWND hWndBu = (HWND) lParam; 
        if (hWndBu == fWork->GetCfield ("gedruckt")->GethWnd ())
		{
			fPos.GetText();
			CountBest();
			return FALSE;
		}
		return FALSE;
}

BOOL SpezDlg::OnKeyReturn (void)
{
        HWND hWnd;
        
        hWnd = GetFocus ();
        if (hWnd == fFoot.GetCfield ("cancel")->GethWnd ())
        {
            return OnKey5 ();
        }
        else if (hWnd == fFoot.GetCfield ("ok")->GethWnd ())
        {
            return OnKey12 ();
        }
        else if (hWnd == fWork->GetCfield ("mdn")->GethWnd ())
        {

            syskey = KEYCR;
            EnterPos ();
            return TRUE;
        }
		/*
        else if (hWnd == fWork->GetCfield ("lgr_nr")->GethWnd ())
        {

            syskey = KEYCR;
            if (ReadLgr () == FALSE)
            {
                return TRUE;
            }
            EnterPos ();
            return TRUE;
        }
		*/
        else if (hWnd == fWork->GetCfield ("lief_von")->GethWnd ())
        {
	        fPos.GetCfield ("lief_von")->GetText ();
			if (atoi(auswahlf.lief_von) > 1) strcpy(auswahlf.lief_bis,auswahlf.lief_von);
			if (atoi(auswahlf.lief_von) <= 1) strcpy(auswahlf.lief_bis,"99999999");
	        fPos.GetCfield ("lief_bis")->SetText ();
		}
        else if (hWnd == fWork->GetCfield ("lief_bis")->GethWnd ())
        {
	        fPos.GetCfield ("lief_bis")->GetText ();
			if (atoi(auswahlf.lief_von) > atoi(auswahlf.lief_bis))
			{
				strcpy(auswahlf.lief_bis,auswahlf.lief_von);
		        fPos.GetCfield ("lief_bis")->SetText ();
		        fPos.SetCurrentName ("lief_bis");
				return TRUE;
			}
		}
        else if (hWnd == fWork->GetCfield ("best_blg_von")->GethWnd ())
        {
	        fPos.GetCfield ("best_blg_von")->GetText ();
			if (atoi(auswahlf.best_blg_von) > 1) strcpy(auswahlf.best_blg_bis,auswahlf.best_blg_von);
			if (atoi(auswahlf.best_blg_von) <= 1) strcpy(auswahlf.best_blg_bis,"99999999");
	        fPos.GetCfield ("best_blg_bis")->SetText ();
		}
        else if (hWnd == fWork->GetCfield ("best_blg_bis")->GethWnd ())
        {
	        fPos.GetCfield ("best_blg_bis")->GetText ();
			if (atoi(auswahlf.best_blg_von) > atoi(auswahlf.best_blg_bis))
			{
				strcpy(auswahlf.best_blg_bis,auswahlf.best_blg_von);
		        fPos.GetCfield ("best_blg_bis")->SetText ();
		        fPos.SetCurrentName ("best_blg_bis");
				return TRUE;
			}
		}
		/*
        else if (hWnd == fWork->GetCfield ("best_term_von")->GethWnd ())
        {
	        fPos.GetCfield ("best_term_von")->GetText ();
			if (ratod(auswahlf.best_term_von) != ratod(auswahlf_save.best_term_von)) strcpy(auswahlf.best_blg_bis,auswahlf.best_blg_von);
	        fPos.GetCfield ("best_term_bis")->SetText ();
		}
        else if (hWnd == fWork->GetCfield ("best_term_bis")->GethWnd ())
        {
	        fPos.GetCfield ("best_term_bis")->GetText ();
			if (ratod(auswahlf.best_term_von) > ratod(auswahlf.best_term_bis))
			{
				strcpy(auswahlf.best_term_bis,auswahlf.best_term_von);
		        fPos.GetCfield ("best_term_bis")->SetText ();
		        fPos.SetCurrentName ("best_term_bis");
				return TRUE;
			}
		}
		*/
        else if (hWnd == fWork->GetCfield ("frm_typ")->GethWnd ())
        {

            syskey = KEYCR;
            if (ReadFrm () == FALSE)
            {
                return TRUE;
            }
	         fPos.SetCurrentName ("lief_term_von");
            return TRUE;
        }
        return FALSE;
}

void SpezDlg::GetCombos (void)
{
/*
        pos = fPos.GetCfield ("vkost_rechb")->GetComboPos ();
        strcpy(kost_art.vkost_rechb, work.GetPtWert ("vkost_rechb", pos + 1)); 
		*/
}


void SpezDlg::SetCombos (void)
{

	/*
		 sprintf (ptwert, "%s", kost_art.vkost_rechb); 
		 
         work.GetPtab (auswahlf.vkost_rechb, "vkost_rechb", ptwert);
         fPos.GetCfield ("vkost_rechb")->SetPosCombo (ptabn.ptlfnr - 1);
		 */

}

int SpezDlg::WriteRow (void)
{

        return 0;
}

         

int SpezDlg::DeleteRow (void)
{
        return 0;
}

void SpezDlg::FillRow (void)
{
/*
        char buffer [512];
        int idx;

        if (_fList[0]->GethWnd () == NULL) return;

        idx = SendMessage (_fList[0]->GethWnd (), LB_GETCURSEL, 0, 0l);
        if (idx < 0) return;

        work.FillRow (buffer);

        SendMessage (_fList[0]->GethWnd (), LB_INSERTSTRING, idx,  
                           (LPARAM) (char *) buffer);
*/
}


void SpezDlg::InsertRow (void)
{
/*
        int i;
        int anz;
        char buffer [512];
        HWND lBox;
        long adr;

        if (InsMode) return;

        memcpy (&masch, &masch_null, sizeof (struct PARTNER));
        memcpy (&_adr, &_adr_null, sizeof (struct ADR));
        ToForm (dbfields);
        SetCombos ();
        if (_adr.geb_dat == 0)
        {
            strcpy (maschf.geb_dat, "");
        }
        else
        {
            dlong_to_asc (_adr.geb_dat, maschf.geb_dat);
        }
        FromForm (dbheadfields);
        adr = work.GenAdrNrEx ();
        masch.adr = adr;
        sprintf (maschf.adr, "%ld", adr);
        fPos.GetCfield ("adr")->SetText ();
        lBox = _fList[0]->GethWnd ();
        if (lBox == NULL) return;

        ListRows = SendMessage (lBox, LB_GETCOUNT, 0, 0l);

        for (i = 0; i < ListRows; i ++)
        {

   	           SendMessage (lBox, LB_GETTEXT, i, (LPARAM) buffer);
               anz = wsplit (buffer, " ");
               if (anz == 0) continue;
               adr = atol (wort[0]);
               if (atol (maschf.adr) < adr) break;
        }
        work.FillRow (buffer);

        fPos.SetText ();
        ScrollList (lBox, i, SCINSERT);
        InvalidateRect (lBox, NULL, TRUE);
        UpdateWindow (lBox);
        SendMessage (lBox, LB_INSERTSTRING, i, (LPARAM) (char *) buffer);
        SendMessage (lBox, LB_SETCURSEL, (WPARAM) i, (LPARAM) 0l);
        InsMode = TRUE;
*/
}

void SpezDlg::SelectRow (void)
{
/*
        int i;
        int anz;
        char buffer [512];
        HWND lBox;
        int ListPos;
        long adr;

        lBox = _fList[0]->GethWnd ();
        if (lBox == NULL) return;


        ListRows = SendMessage (lBox, LB_GETCOUNT, 0, 0l);
        ListPos  = SendMessage (_fList[0]->GethWnd (), LB_GETCURSEL, 0, 0l);
         
        for (i = 0; i < ListRows; i ++)
        {

   	           SendMessage (lBox, LB_GETTEXT, i, (LPARAM) buffer);
               anz = wsplit (buffer, " ");
               if (anz == 0) continue;
               adr = atol (wort[0]);
               if (atol (maschf.adr) == adr) break;
        }
                
        if (i == ListPos) return;

        SendMessage (lBox, LB_SETCURSEL, (WPARAM) i, (LPARAM) 0l);
*/
}


/*
BOOL SpezDlg::OnKeyDown (void)
{
        HWND hWnd;
        CFIELD *Cfield;

        hWnd == GetFocus ();

        if (hWnd == _fList[0]->GethWnd ())
        {
            return FALSE;
        }

        Cfield = GetCurrentCfield ();
        if (Cfield != NULL)
        {
            if (Cfield->GetAttribut () & CCOMBOBOX != 0)
            {
                return FALSE;
            }
        }

        if (fHead.GetCfield ("kun")->IsDisabled () == FALSE)
        {
            return FALSE;
        }
        if (_fList[0]->GethWnd () == NULL) 
        {
            return FALSE;
        }

        if (WriteRow () == -1)
        {
                 return TRUE;
        }

        syskey = KEYDOWN;
        ListRows = SendMessage (hWnd, LB_GETCOUNT, 0, 0l);
        {
                  SendMessage (_fList[0]->GethWnd (), WM_KEYDOWN, VK_DOWN, 0l);
        }

        fPos.SetCurrentName ("adr_nam1");
        fPos.SetCurrentName ("adr_krz");
        LockWindowUpdate (NULL);
        return TRUE;
}


BOOL SpezDlg::OnKeyUp (void)
{
        HWND hWnd;
        int idx;
        CFIELD *Cfield;

        hWnd == GetFocus ();

        if (hWnd == _fList[0]->GethWnd ())
        {
            return FALSE;
        }

        Cfield = GetCurrentCfield ();
        if (Cfield != NULL)
        {
            if (Cfield->GetAttribut () & CCOMBOBOX != 0)
            {
                return FALSE;
            }
        }
 
        fPos.GetText ();
        if (atol (maschf.adr) <= 0)
        {
            return TRUE;
        }

        idx = SendMessage (_fList[0]->GethWnd (), LB_GETCURSEL, 0, 0l);

        if (idx <= 0)
        {
            return TRUE;
        }

        if (fHead.GetCfield ("kun")->IsDisabled () == FALSE)
        {
            return FALSE;
        }
        if (_fList[0]->GethWnd () == NULL) 
        {
            return FALSE;
        }


        if (WriteRow () == -1)
        {
                 return TRUE;
        }


        LockWindowUpdate (hWnd);
        syskey = KEYUP;
        SendMessage (_fList[0]->GethWnd (), WM_KEYDOWN, VK_UP, 0l);

        fPos.SetCurrentName ("adr_nam1");
        fPos.SetCurrentName ("adr_krz");
        LockWindowUpdate (NULL);
        return TRUE;
}
*/

BOOL SpezDlg::OnKey2 ()
{
        return TRUE;
}



BOOL SpezDlg::OnKeyHome ()
{
        return TRUE;
}

BOOL SpezDlg::OnKeyEnd ()
{
        return TRUE;

}
BOOL SpezDlg::OnKey1 ()
{
        syskey = KEY1;
        Help ();
        return TRUE;
}

BOOL SpezDlg::OnKey3 ()
{
        syskey = KEY3;
        return TRUE;
}

BOOL SpezDlg::OnKey4 ()
{
        syskey = KEY4;
        CallInfo ();
        return TRUE;
}

BOOL SpezDlg::OnKey5 ()
{

        syskey = KEY5;
        InsMode = FALSE;
        syskey = KEY5;
        if (fHead.GetCfield ("mdn")->IsDisabled () == FALSE)
        {
                  DestroyWindow ();
                  return TRUE;
        }
        Enable (&fauswahl, EnableHead, TRUE); 
        Enable (&fauswahl, EnablePos,  FALSE);

//        SendMessage (_fList[0]->GethWnd (), LB_RESETCONTENT, 0, 0l);
        work.RollbackWork ();
        work.InitRec ();
        ToForm (dbfields);
        SetCombos ();
//        fPos.SetText ();
        fHead.SetCurrentName ("mdn");
        ToolBar_SetState (ActiveDlg->GethwndTB (),   IDM_DELALL,   TBSTATE_INDETERMINATE);
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELETE,   MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELALL,   MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      VK_INSERT,    MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      VK_F10,       MF_GRAYED);
        if (ActiveSpezDlg->GetToolbar2 () != NULL)
        {
                 ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("del")->GetFeld ())->bmp = 
					 ImageDelete.Inactive;
                 ((ColButton *)Toolbar2->GetCfield ("insert")->GetFeld ())->bmp = 
                     ImageInsert.Inactive;
                 if  (ActiveSpezDlg->GetToolbar2 ()->GetCfield ("Texte") != NULL)
                 {
                      ActiveSpezDlg->GetToolbar2 ()->GetCfield ("Texte")->Enable (FALSE);
                 }
                 ActiveSpezDlg->GetToolbar2 ()->display ();
        }
        return TRUE;
}


BOOL SpezDlg::OnKey12 ()
{
        syskey = KEY12;

        InsMode = FALSE;
        fWork->GetText ();
        FromForm (dbfields);
        GetCombos ();
        if (fHead.GetCfield ("mdn")->IsDisabled ())
        {

			if (strcmp(auswahlf.gedruckt, "J") == 0) 
			{
				SchreibeAuswahl ();
			}
			else
			{

				int dsqlstatus = work.StatusVorbereiten 
	             (atoi(auswahlf.mdn), atoi (auswahlf.lgr_nr),
				dasc_to_long(auswahlf.best_term_von),dasc_to_long(auswahlf.best_term_bis),
				dasc_to_long(auswahlf.lief_term_von),dasc_to_long(auswahlf.lief_term_bis),
				atoi(auswahlf.lief_von), atoi (auswahlf.lief_bis),
				atoi(auswahlf.best_blg_von), atoi (auswahlf.best_blg_bis));

			    if (dsqlstatus > 0 ) SchreibeAuswahl ();

				dsqlstatus = work.StatusAendern 
	             (atoi(auswahlf.mdn), atoi (auswahlf.lgr_nr),
				dasc_to_long(auswahlf.best_term_von),dasc_to_long(auswahlf.best_term_bis),
				dasc_to_long(auswahlf.lief_term_von),dasc_to_long(auswahlf.lief_term_bis),
				atoi(auswahlf.lief_von), atoi (auswahlf.lief_bis),
				atoi(auswahlf.best_blg_von), atoi (auswahlf.best_blg_bis));
			}

//                if (dsqlstatus) print_mess 
//					(2, "Status kann nicht auf gedruckt gesetzt werden (Fehler %ld)", dsqlstatus); 


               Enable (&fauswahl, EnableHead, TRUE); 
               Enable (&fauswahl, EnablePos,  FALSE);

               work.CommitWork ();
               work.InitRec ();
               ToForm (dbfields);
               SetCombos ();

               fHead.SetCurrentName ("mdn");
               ToolBar_SetState (ActiveDlg->GethwndTB (),   IDM_DELALL, TBSTATE_INDETERMINATE);
               EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELETE,   MF_GRAYED);
               EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELALL,   MF_GRAYED);
               EnableMenuItem (ActiveDlg->GethMenu (),      VK_INSERT,    MF_GRAYED);
               EnableMenuItem (ActiveDlg->GethMenu (),      VK_F10,       MF_GRAYED);
               if (ActiveSpezDlg->GetToolbar2 () != NULL)
               {
                 ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("del")->GetFeld ())->bmp = 
					 ImageDelete.Inactive;
                 ((ColButton *)Toolbar2->GetCfield ("insert")->GetFeld ())->bmp = 
                     ImageInsert.Inactive;
                 if  (ActiveSpezDlg->GetToolbar2 ()->GetCfield ("Texte") != NULL)
                 {
                      ActiveSpezDlg->GetToolbar2 ()->GetCfield ("Texte")->Enable (FALSE);
                 }
                 ActiveSpezDlg->GetToolbar2 ()->display ();
               }
        }
        return TRUE;
}


BOOL SpezDlg::OnKey10 (void)
{
/*
  	  int cx, cy;
	  char textnr [10];
      long text_nr;


      if (fPos.GetCfield ("adr_krz")->IsDisabled ()) 
      {
          return TRUE;
      }
      text_nr = work.GenTxtNr ();
      cx = 80;
      cy = 20;
      Mtxt = new MTXT (cx, cy, "Bemerkungen", "", NULL);
	  Mtxt->SetTextNr (LongToChar (textnr, "%ld", text_nr));
      Mtxt->OpenWindow (DLG::hInstance, hWnd);
	  EnableWindows (hWnd, FALSE);
      work.ReadTxt (text_nr, Mtxt); 

	  if (Mtxt->ProcessMessages ())
	  {
		  work.WriteTxt (atol (Mtxt->GetTextNr ()), Mtxt);
	  }
      else
      {
          if (_adr.txt_nr == 0l)
          {
              work.FreeTxtNr (text_nr);
          }
      }

	  SetActiveWindow (hWnd);
	  EnableWindows (hWnd, TRUE);
      Mtxt->DestroyWindow ();
	  delete Mtxt;
      ActiveDlg->SetCurrentFocus ();
*/
      return TRUE;
}


BOOL SpezDlg::OnKeyDelete ()
{
/*        
        if (GetKeyState (VK_CONTROL) < 0)
        {
            return OnRowDelete ();
        }
*/
        return FALSE;
}


BOOL SpezDlg::OnRowDelete ()
{
/*
        CFIELD *Cfield;  

        if (_fList[0]->GethWnd () == NULL) return FALSE;

        ListRows = SendMessage (_fList[0]->GethWnd (), LB_GETCOUNT, 0, 0l);
        if (ListRows == NULL) return FALSE;

        if (abfragejn (hWnd, "Position l�schen ?", "J"))
        {
            DeleteRow ();
        }
        Cfield = ActiveDlg->GetCurrentCfield ();
        if (Cfield != NULL)
        {
            Cfield->SetFocus ();
        }
*/
        return TRUE;
}


BOOL SpezDlg::OnKeyInsert ()
{
/*
        InsertRow ();
        fPos.SetCurrentName ("adr_krz");
*/
        return TRUE;
}


BOOL SpezDlg::OnKeyPrior ()
{

        if (vTrack != NULL)
        {
             SendMessage (hWnd, WM_VSCROLL, SB_PAGEUP, 0l);
             return TRUE;
        }
        return FALSE;
}

BOOL SpezDlg::OnKeyNext ()
{
        if (vTrack != NULL)
        {
             SendMessage (hWnd, WM_VSCROLL, SB_PAGEDOWN, 0l);
             return TRUE;
        }
        return FALSE;
}


BOOL SpezDlg::OnKey6 ()
{
        return TRUE;
} 


BOOL SpezDlg::OnKey7 ()
{
        return FALSE;
}


BOOL SpezDlg::ShowMdn (void)
{
             char mdn_nr [6];
             SEARCHMDN SearchMdn;
 
             SearchMdn.SetParams (DLG::hInstance, ActiveDlg->GethWnd ());
             SearchMdn.Setawin (ActiveDlg->GethMainWindow ());
             SearchMdn.Search ();
             if (SearchMdn.GetKey (mdn_nr) == TRUE)
             {
                 sprintf (auswahlf.mdn, "%4d", atoi (mdn_nr));
                 work.GetMdnName (auswahlf.mdn_krz,   atoi (auswahlf.mdn));
                 fHead.SetText ();
                 fHead.SetCurrentName ("mdn");
             }
             return TRUE;
}

BOOL SpezDlg::ShowFrm (void)
{
             char frm_nr [11];
             char bez [129];
 
             SearchFrm.SetParams (DLG::hInstance, ActiveDlg->GethWnd ());
             SearchFrm.Setawin (ActiveDlg->GethMainWindow ());
             SearchFrm.Search ();
             if (SearchFrm.GetKey (frm_nr, bez ) == TRUE)
             {
                 strcpy (auswahlf.frm_typ, frm_nr);
                 strncpy (auswahlf.frm_bz, bez,40);
//                 work.GetFrmName (auswahlf.frm_bz,   atoi (auswahlf.frm_typ));
//                 fPos.SetText ();
		         fPos.GetCfield ("frm_bz")->SetText ();
		         fPos.GetCfield ("frm_typ")->SetText ();
                 fPos.SetCurrentName ("frm_nr");
             }
             return TRUE;
}


BOOL SpezDlg::Showlgr (void)
{
             short lgr_nr ;
             SEARCHLGR Searchlgr;
 
             Searchlgr.SetParams (DLG::hInstance, ActiveDlg->GethWnd ());
             Searchlgr.Setawin (ActiveDlg->GethMainWindow ());
             Searchlgr.Search ();
             if (Searchlgr.GetKey (&lgr_nr) == TRUE)
             {
                 sprintf (auswahlf.lgr_nr, "%4d", lgr_nr);
                 fHead.SetText ();
                 fHead.SetCurrentName ("lgr_nr");
             }
             return TRUE;
}



BOOL SpezDlg::OnKey9 ()
{
         CFIELD *Cfield;
         HWND hWnd;

         syskey = KEY9;
         hWnd = GetFocus ();


         if (hWnd == fHead.GetCfield ("mdn_choise")->GethWnd ())
         {
             return ShowMdn ();
         }

         if (hWnd == fPos.GetCfield ("frm_typ_choise")->GethWnd ())
         {
             return ShowFrm ();
         }
         if (hWnd == fHead.GetCfield ("lgr_choise")->GethWnd ())
         {
             return Showlgr ();
         }

         Cfield = ActiveDlg->GetCurrentCfield ();

         if (strcmp (Cfield->GetName (), "mdn") == 0)
         {
             return ShowMdn ();
         }
         if (strcmp (Cfield->GetName (), "frm_typ") == 0)
         {
             return ShowFrm ();
         }

		 if (strcmp (Cfield->GetName (), "lgr_nr") == 0)
         {
             return Showlgr ();
         }
         else if (Cfield->GetAttribut () == CCOMBOBOX)
         {
             SendMessage (Cfield->GethWnd (),
                     CB_SHOWDROPDOWN, TRUE,  NULL);
             return TRUE;
         }

         return FALSE;
}


BOOL SpezDlg::OnPaint (HWND hWnd,HDC hdc, UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (fWork && this->hWnd == hWnd)
        {
             PrintMess (MessText);
             fWork->display (hWnd, hdc);
             return TRUE;
        }
        return DLG::OnPaint (hWnd,hdc, msg, wParam,lParam);
}


BOOL SpezDlg::OnActivate (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (LOWORD (wParam) == WA_ACTIVE ||
                LOWORD (wParam) == WA_CLICKACTIVE)
        {
              if (WithTabStops)
              {
                  SetTabFocus (); 
              }
              else
              {
                  fWork->SetFocus ();
              }
              return TRUE;
        }
        return FALSE;
}


BOOL SpezDlg::OnCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (fWork)
        {

              if (LOWORD (wParam) == CANCEL_CTL)
              {
                  return OnKey5 ();
              }
              if (LOWORD (wParam) == OK_CTL)
              {
                  return OnKey12 ();
              }
              if (LOWORD (wParam) == IDM_CHOISE)
              {
                  return OnKey9 ();
              }

              if (HIWORD (wParam) == WM_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              if (HIWORD (wParam) == EN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == CBN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == WM_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == LBN_SELCHANGE)
              {
                  return ListChanged (wParam, lParam);
              }
              else if (HIWORD (wParam) == EN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == CBN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == BN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == BN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == BN_CLICKED)
              {
                  OnClicked (hWnd, msg, wParam, lParam);
              }
        }
        return DLG::OnCommand (hWnd, msg, wParam, lParam);
}


BOOL SpezDlg::OnSysCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        
        if (wParam == SC_CLOSE)
        {
            if (abfragejn (hWnd, "Verarbeitung abbrechen ?", "N") == 0)
            {
                ActiveDlg->SetCurrentFocus ();
                return TRUE;
            }
            syskey = KEY5;
        }
        return DLG::OnSysCommand (hWnd, msg, wParam, lParam); 
}

BOOL SpezDlg::OnDestroy (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{

        if (hWnd == this->hWnd)
        {
             fauswahl.RemoveCfield ("kost_artlst");
             PostQuitMessage (0);
             fauswahl.destroy ();
        }
        return TRUE;
}

void SpezDlg::SetWinBackground (COLORREF Col)
{
          dlgfont.FontBkColor = Col;
          cpfont.FontBkColor = Col;
          DLG::SetWinBackground (Col);
}


void SpezDlg::ToClipboard (void)
{
          CFIELD *Cfield;
          HGLOBAL hb;
          LPVOID p;
          char *text;

          Cfield = GetCurrentCfield ();
          Cfield->GetText ();
          text = (char *) Cfield->GetFeld ();
          if (text == NULL) return;
          hb = GlobalAlloc (GHND, strlen (text) + 1);
          if (hb == NULL) return;
          p = GlobalLock (hb);
          strcpy ((char *) p, text);
          GlobalUnlock (hb);
          OpenClipboard (hWnd);
          EmptyClipboard ();
          SetClipboardData (CF_TEXT, hb);
          CloseClipboard ();
}

void SpezDlg::FromClipboard (void)
{
          CFIELD *Cfield;
          HGLOBAL hb;
          LPVOID p;
          char *text;

          Cfield = GetCurrentCfield ();
          text = (char *) Cfield->GetFeld ();
          if (text == NULL) return;
          if (IsClipboardFormatAvailable (CF_TEXT) == FALSE) return;
          OpenClipboard (hWnd);
          hb = GetClipboardData (CF_TEXT);
          if (hb == NULL)
          {
                 CloseClipboard ();
                 return;
          }
          p = GlobalLock (hb);
          if (strlen ((char *) p) > (unsigned int ) Cfield->GetLength ())
          {
                 GlobalUnlock (hb);
                 CloseClipboard ();
                 return;
          }
          strcpy (text, (char *) p);
          GlobalUnlock (hb);
          CloseClipboard ();
          Cfield->SetText ();
}

void SpezDlg::Help (void)
{
          CFIELD *Cfield;
          HELP *Help; 
          char *Item;

          Cfield = GetCurrentCfield ();
          if (Cfield == NULL) return;

          Item = Cfield->GetName ();

          Help = new HELP (HelpName, Item, hInstance,hMainWindow);
          delete Help;
          Cfield->SetFocus ();
}


BOOL SpezDlg::FillListRows (void)
{
/*
          char buffer [512];
          HWND lBox;
          int dsqlstatus;

          lBox = _fList[0]->GethWnd ();
          if (lBox == NULL) return FALSE;
          dsqlstatus = work.ReadKun 
             (masch.mdn, masch.fil, atol (maschf.kun));
          if (dsqlstatus < 0)
          {
                    disp_mess ("Der Kunde wird gerade bearbeitet", 2);
                    return FALSE;
          }
          while (dsqlstatus == 0)
          {
                   dsqlstatus = work.ReadKun ();
                   if (dsqlstatus < 0)
                   {
                         disp_mess ("Der Kunde wird gerade bearbeitet", 2);
                         return FALSE;
                   }
          }

          dsqlstatus = work.ReadKun 
             (masch.mdn, masch.fil, atol (maschf.kun));
          while (dsqlstatus == 0)
          {
                   work.FillRow (buffer);

                   SendMessage (lBox, LB_INSERTSTRING, -1,  
                                 (LPARAM) (char *) buffer);
                   dsqlstatus = work.ReadKun ();
          }
          ListRows = SendMessage (lBox, LB_GETCOUNT, 0, 0l);
          if (ListRows > 0)
          {
                   SendMessage (lBox, LB_SETCURSEL, 0, 0l);
          }
*/
          return TRUE;
}


void SpezDlg::FillListCaption (void)
{
/*
          char buffer [512];
          HWND lBox;

          lBox = _fList[0]->GethWnd ();
          if (lBox == NULL) return;

          sprintf (buffer, "%10s  %-36s  %-21s  %-36s", 
                           "Adresse", "Name", "Telefon-Nr", "Ort");
          SendMessage (lBox, LB_TITLE, -1,  
                                 (LPARAM) (char *) buffer);
           sprintf (buffer, "%11s%38s%22s%38s%", 
                           "1","1", "1", "1", "1");
          SendMessage (lBox, LB_VPOS, 3, (LPARAM) buffer);  
          sprintf (buffer, "0;8 13;36 52;20 75;36");
          SendMessage (lBox, LB_RPOS, 0, (LPARAM) buffer);  
          sprintf (buffer, "%s %s %s %s %s", "%d","%s","%s","%s");
          SendMessage (lBox, LB_ROWATTR, 0,  
                                 (LPARAM) (char *) buffer);
*/
}


HWND SpezDlg::OpenWindow (HINSTANCE hInstance, HWND hMainWindow)
{
          HWND hWnd;

          Settchar ('|');
          hWnd = DLG::OpenWindow (hInstance, hMainWindow);
          FillListCaption ();
          return hWnd;
}

HWND SpezDlg::OpenScrollWindow (HINSTANCE hInstance, HWND hMainWindow)
{
          HWND hWnd;

          Settchar ('|');
          hWnd = DLG::OpenScrollWindow (hInstance, hMainWindow);
          FillListCaption ();
          return hWnd;
}

void SpezDlg::PrintComment (char *Name)
{
          HELP *Help; 
          char Comment [256];

          Help = new HELP (HelpName, Name, hInstance,hMainWindow, INITCOMM);
          Help->GetItem (Name, Comment);
          PrintMess (Comment);
          delete Help;
}

void SpezDlg::CallInfo (void)
{
          DLG::CallInfo ();
}

int SpezDlg::Druckell (void)
{

  char datenam[22] ;
  char buffer [512] ;
  char *PrintCommand = "dr70001";
  sprintf ( datenam , "%s.llf", auswahlf.frm_typ ) ;

  if (getenv ("TMPPATH"))
  {
    sprintf (buffer, "%s -name %s -datei %s\\%s",PrintCommand,auswahlf.frm_typ, getenv("TMPPATH"), datenam);
  }
  else
  {
    strcpy (buffer, datenam );
  }
  return ProcExec (buffer, SW_SHOWNORMAL, -1, 0, -1, 0);  
}


int SpezDlg::SchreibeAuswahl (void)
{
//  char datenam[22] ;
//  char buffer [512] ;
//  char komma[128] 

//  char zeilbuffer[80] ;
  short mit_druckwahl = 1;
//  char range_name[60];
  char range_von[60];
  char range_bis[60];
//  FILE *lst ;

  LeseFormat (clipped(auswahlf.frm_typ));
  /*
  sprintf ( datenam , "%s.llf", auswahlf.frm_typ ) ;

  if (getenv ("TMPPATH"))
  {
    sprintf (buffer, "%s\\%s", getenv("TMPPATH"), datenam);
  }
  else
  {
    strcpy (buffer, datenam );
  }
  remove (buffer) ;
  if ( (lst = fopen (buffer, "at" )) == NULL ) 
  {
                     print_mess (2, "%s kann nicht ge�ffnet werden", buffer);
                     return -1;
  }
  */


  //===================== Formatname =====================
//        memset ( zeilbuffer, '\0', 128 ) ;
//        sprintf ( zeilbuffer, "NAME $%s" , clipped(auswahlf.frm_typ)) ;
//        fprintf ( lst , "%s\n" , zeilbuffer ) ;
  //===================== mit Druckwahl =====================
 //         memset ( zeilbuffer, '\0', 80 ) ;
  //        sprintf ( zeilbuffer, "DRUCK %hd" , mit_druckwahl) ;
  //        fprintf ( lst , "%s\n" , zeilbuffer ) ;
  //===================== Liste, nicht LAbel =====================
 //         memset ( zeilbuffer, '\0', 80 ) ;
 //         sprintf ( zeilbuffer, "LABEL %hd" , 0) ;
 //         fprintf ( lst , "%s\n" , zeilbuffer ) ;
  //===================== Range =====================
	if (strcmp(clipped(auswahlf.lgr_nr), "0") == 0) 
	{
		sprintf ( range_von, "%s" , "0") ;
		sprintf ( range_bis, "%s" , "99999") ;
	}
	else
	{
		sprintf ( range_von, "%s" , clipped(auswahlf.lgr_nr)) ;
		sprintf ( range_bis, "%s" , clipped(auswahlf.lgr_nr)) ;
		FillFormFeld ("best_kopf.lgr", range_von, range_bis);
	}
//	FillFormFeld ("best_kopf.best_term", auswahlf.best_term_von, auswahlf.best_term_bis);
	FillFormFeld ("best_kopf.lief_term", auswahlf.lief_term_von, auswahlf.lief_term_bis);
	FillFormFeld ("best_kopf.lief", auswahlf.lief_von, auswahlf.lief_bis);
	FillFormFeld ("best_kopf.best_blg", auswahlf.best_blg_von, auswahlf.best_blg_bis);
	if (strcmp(auswahlf.gedruckt, "J") == 0) 
	{
		sprintf ( range_bis, "%s" , "99") ;
		sprintf ( range_von, "%s" , "0") ;
	}
	else
	{
	    FillFormFeld ("best_kopf.delstatus", "2", "2");
		sprintf ( range_bis, "%s" , "1") ;
		sprintf ( range_von, "%s" , "0") ;
	}
	FillFormFeld ("best_kopf.bearb_stat", range_von, range_bis);
    DruckeFile();

/*************

		  strcpy(range_name,"lgr");
          sprintf ( range_von, "%s" , auswahlf.lgr_nr) ;
          sprintf ( range_bis, "%s" , auswahlf.lgr_nr) ;
          memset ( zeilbuffer, '\0', 80 ) ;
          sprintf ( zeilbuffer, "%s %s %s",clipped(range_name),clipped(range_von),clipped(range_bis));
          fprintf ( lst , "%s\n" , zeilbuffer ) ;

		  strcpy(range_name,"best_term");
          sprintf ( range_von, "%s" , auswahlf.best_term_von) ;
          sprintf ( range_bis, "%s" , auswahlf.best_term_bis) ;
          memset ( zeilbuffer, '\0', 80 ) ;
          sprintf ( zeilbuffer, "%s %s %s",clipped(range_name),clipped(range_von),clipped(range_bis));
          fprintf ( lst , "%s\n" , zeilbuffer ) ;

		  strcpy(range_name,"lief_term");
          sprintf ( range_von, "%s" , auswahlf.lief_term_von) ;
          sprintf ( range_bis, "%s" , auswahlf.lief_term_bis) ;
          memset ( zeilbuffer, '\0', 80 ) ;
          sprintf ( zeilbuffer, "%s %s %s",clipped(range_name),clipped(range_von),clipped(range_bis));
          fprintf ( lst , "%s\n" , zeilbuffer ) ;

		  strcpy(range_name,"lief");
          sprintf ( range_von, "%s" , auswahlf.lief_von) ;
          sprintf ( range_bis, "%s" , auswahlf.lief_bis) ;
          memset ( zeilbuffer, '\0', 80 ) ;
          sprintf ( zeilbuffer, "%s %s %s",clipped(range_name),clipped(range_von),clipped(range_bis));
          fprintf ( lst , "%s\n" , zeilbuffer ) ;

		  strcpy(range_name,"best_blg");
          sprintf ( range_von, "%s" , auswahlf.best_blg_von) ;
          sprintf ( range_bis, "%s" , auswahlf.best_blg_bis) ;
          memset ( zeilbuffer, '\0', 80 ) ;
          sprintf ( zeilbuffer, "%s %s %s",clipped(range_name),clipped(range_von),clipped(range_bis));
          fprintf ( lst , "%s\n" , zeilbuffer ) ;

		  strcpy(range_name,"bearb_stat");
          sprintf ( range_von, "%s" , "0") ;
  	      if (strcmp(auswahlf.gedruckt, "J") == 0) 
		  {
				sprintf ( range_bis, "%s" , "99") ;
		  }
		  else
		  {
				sprintf ( range_bis, "%s" , "1") ;
		  }
          
          memset ( zeilbuffer, '\0', 80 ) ;
          sprintf ( zeilbuffer, "%s %s %s",clipped(range_name),clipped(range_von),clipped(range_bis));
          fprintf ( lst , "%s\n" , zeilbuffer ) ;
		  fclose (lst);
  if (getenv ("TMPPATH"))
  {
    sprintf (komma, "%s\\%s", getenv("TMPPATH"), datenam);
  }
  else
  {
    strcpy (komma, datenam );
  }
  remove (komma) ;
  ***********************/

  return 0;
}



