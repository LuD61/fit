#ifndef _WORK_DEF
#define _WORK_DEF
#include "dbclass.h"
#include "ptab.h"
#include "mdn.h"
#include "mo_auto.h"
#include "auto_nr.h"
#include "mo_txt.h"

class Work
{
    private :
        PTAB_CLASS Ptab;
        MDN_CLASS Mdn;
        int lgr_cursor;
		long blg_tab [2000];
		long blgf_tab [2000];
		long lief_tab [2000];
		long lieff_tab [2000];
		short blganz ;
    public :
        Work () : lgr_cursor (-1)
        {
        }
        void BeginWork (void);
        void CommitWork (void);
        void RollbackWork (void);
        void InitRec (void);
        int ReadLgr (short,short);
        int CountBest (short,long,long,long,long,long,long,long,long,long,char*);
        int StatusVorbereiten (short,long,long,long,long,long,long,long,long,long);
        int StatusAendern (short,long,long,long,long,long,long,long,long,long);
        int ReadFrm (short,short);
        int GetPtab (char *, char *, char *);
        char *GetPtWert (char *, short);
        int ShowPtab (char *dest, char *item);
        void FillRow (char *);
        void FillCombo (char **, char *, int);
        void FillComboLong (char **, char *, int);
        int GetMdnName (char *, short);
        int  CutLines (char *, char *, int, int);
};

#endif