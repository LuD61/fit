#ifndef _SPEZDLG_DEF
#define _SPEZDLG_DEF
#include "dlg.h"
#include "itprog.h"
#include "itfont.h"
#include "formfield.h"
#include "searchlgr.h"
#define LGR_NR_CTL 1803

#define MDN_CTL 1804
#define LGR_BZ_CTL 1805
#define LIEF_VON_CTL 1806
#define LIEF_BIS_CTL 1807
#define BEST_VON_CTL 1808
#define BEST_BIS_CTL 1809
#define BEST_TVON_CTL 18010
#define BEST_TBIS_CTL 18011
#define LIEF_TVON_CTL 18012
#define LIEF_TBIS_CTL 18013
#define GBEST_CTL 18014
#define FRM_TYP_CTL 18015
#define BEST_ANZ_CTL 18016
#include "Work.h"

#define AUSWAHL_LST_CTL 1817

#define IDM_CHOISE 2002

#define HEADCTL 700
#define POSCTL 701
#define FOOTCTL 702

#define RAISEDBORDER 0
#define HIGHBORDER 1
#define LOWBORDER 2
#define HIGHCOLBORDER 3
#define LOWCOLBORDER 4
#define LINEBORDER 5

#define ENTERHEAD 0
#define ENTERA 1
#define ENTERADATA 2

#define IDM_DELALL 5000


struct AUSWAHL
{
      short mdn;
      char mdn_krz [20];
      int lgr_nr;
      char lgr_bz [25];
	  long lief_von;
	  long lief_bis;
	  long best_blg_von ;
	  long best_blg_bis ;
	  long best_term_von ;
	  long best_term_bis ;
	  long lief_term_von ;
	  long lief_term_bis ;
	  char gedruckt [2];
	  char frm_typ [12];
	  long best_anzahl;

};
extern struct AUSWAHL _auswahl ;

class SpezDlg : virtual public DLG 
{
          private :
             static ItProg *After [];
             static ItProg *Before [];
             static ItFont *Font [];
             static FORMFIELD *dbfields [];
             static FORMFIELD *dbheadfields [];
             static char *EnableHead[];
             static char *EnableHeadFil[];
             static char *EnablePos [];
             static char *EnableMinute [];
			 static BitImage ImageDelete;
			 static BitImage ImageInsert;
//             static char *ProdAbtCombo[]; 
             static char *RechbCombo[]; 
             static char *lastfield;
             int    BorderType;
             static Work work;
             static int ListRows;
             static BOOL InsMode;
             static BOOL WriteOK;
             static char *HelpName;
             static SEARCHLGR Searchlgr;
             CFORM *Toolbar2;
             MTXT *Mtxt;

             static SpezDlg *ActiveSpezDlg;
             static int pos;
             static char ptwert[];
          public :

            static HBITMAP SelBmp;
            static COLORREF SysBkColor;

            void SetToolbar2 (CFORM *Toolbar2)
            {
                this->Toolbar2 = Toolbar2;
            }

            CFORM *GetToolbar2 (void)
            {
                return Toolbar2;
            }
 	        SpezDlg (int, int, int, int, char *, int, BOOL);
 	        SpezDlg (int, int, int, int, char *, int, BOOL, int);
 	        SpezDlg (int, int, int, int, char *, int, BOOL, int, int);
 	        void Init (int, int, int, int, char *, int, BOOL);
//            BOOL OnKeyDown (void);
//            BOOL OnKeyUp (void);
            BOOL OnKey1 (void);
            BOOL OnKey2 (void);
            BOOL OnKey3 (void);
            BOOL OnKey4 (void);
            BOOL OnKey5 (void);
            BOOL OnKey6 (void);
            BOOL OnKey7 (void);
            BOOL OnKey9 (void);
            BOOL OnKey10 (void);
            BOOL OnKey12 (void);
            BOOL OnRowDelete (void);
            BOOL OnKeyDelete (void);
            BOOL OnKeyInsert (void);
            BOOL OnKeyPrior (void);
            BOOL OnKeyNext (void);
            BOOL OnKeyHome (void);
            BOOL OnKeyEnd (void);
            BOOL OnKeyReturn (void);
            BOOL OnPaint (HWND,HDC,UINT,WPARAM,LPARAM);
            BOOL OnActivate (HWND,UINT,WPARAM,LPARAM);
            BOOL OnCommand (HWND,UINT,WPARAM,LPARAM);
            BOOL OnSysCommand (HWND,UINT,WPARAM,LPARAM);
            BOOL OnDestroy (HWND,UINT,WPARAM,LPARAM);
            BOOL OnClicked (HWND,UINT,WPARAM,LPARAM);
            void PrintComment (char *);
            HWND OpenWindow (HANDLE, HWND);
            HWND OpenScrollWindow (HANDLE, HWND);
            void FillListCaption (void);
            void SetWinBackground (COLORREF);
            void ToClipboard (void);
            void FromClipboard (void);
            void Help (void);
            int Druckell (void);
            BOOL Showlgr (void);
            BOOL ShowMdn (void);
            BOOL ShowFrm (void);
            void SetListDimension (int, int);
            void DiffListDimension (int, int);
            void CallInfo (void);
            static int WriteRow (void);
            static int SchreibeAuswahl (void);
            static int DeleteRow (void);
            static void FillRow (void);
            static void InsertRow (void);
            static void SelectRow (void);
            static void FillEnterList (int);
            static int ListChanged (WPARAM, LPARAM);
            static BOOL FillListRows (void);
            static int ReadLgr (void);
            static int CountBest (void);
            static int ReadFrm (void);
            static int ReadMdn (void);
            static int EnterPos (void);
            static int EnableFaktor (void);
            static int GetPtab (void);
            static void GetCombos (void);
            static void SetCombos (void);
};
#endif
