#include <windows.h>
#include "searchfrm.h"
#include "strfkt.h"
#include "mo_wmess.h"
#include "lbox.h"


int SEARCHFRM::idx;
long SEARCHFRM::anz;
CHQEX *SEARCHFRM::Query = NULL;
DB_CLASS SEARCHFRM::DbClass; 
HINSTANCE SEARCHFRM::hMainInst;
HWND SEARCHFRM::hMainWindow;
HWND SEARCHFRM::awin;


int SEARCHFRM::SearchLst (char *sebuff)
/**
Nach Kurzname in Liste suchen.
**/
{
	   Query->SearchList ();
	   return 0;
}
char * SEARCHFRM::ReadFrmBz (char *frm_typ)
{
      static char frm_ube [129];
	  int dsqlstatus;

      DbClass.sqlin ((char *) frm_typ, 0, 10);
      DbClass.sqlout ((char *) frm_ube, 0, 40);
	 dsqlstatus = DbClass.sqlcomm ("select nform_hea.form_ube "
	 			       "from nform_hea "
                       "where form_nr = ? and lila = 0 ");
	 if (dsqlstatus == 0) return frm_ube;
	 return "";

}

int SEARCHFRM::ReadLst (char *name)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int cursor;
	  int i;
      WMESS Wmess;
      char frm_nr[11];
      char frm_ube [129];


	  anz = 0;
      clipped (name);
      if (strcmp (name, " ") == 0) name[0] = 0;

      if (strlen (name) == 0 ||
          name[0] <= ' ')
      {
	          sprintf (buffer, "select nform_hea.form_nr, nform_hea.form_ube "
	 			       "from nform_hea "
                       "where form_nr in (select form_nr from nform_tab  "
					                       "where tab_nam = \"best_kopf\") "
		                "and lila = 0 "
                       "order by form_nr");
      }
      else
      {
	          sprintf (buffer, "select nform_hea.form_nr, nform_hea.form_ube "
	 			       "from nform_hea "
                       "where form_nr in (select form_nr from nform_tab  "
					                       "where tab_nam = \"best_kopf\") "
		               "and nform_ube matches \"%s\" and lila = 0", name);
      }
      DbClass.sqlout ((char *) frm_nr, 0, 10);
      DbClass.sqlout ((char *) frm_ube, 0, 40);
      cursor = DbClass.sqlcursor (buffer);
      if (cursor < 0) 
      {
          return -1;
      }
  	  i = 0;
      Wmess.Message (hMainInst, hMainWindow, " \nBitte warten.....\n"
  	                                         "Die Daten werden selektiert.\n ");  
	  while (DbClass.sqlfetch (cursor) == 0)
      {
 	      sprintf (buffer, "%10s|%-40s|", frm_nr, frm_ube);
	      Query->InsertRecord (buffer);
		  i ++;
      }
      anz = i;
	  DbClass.sqlclose (cursor);
 	  Wmess.Destroy ();
	  EnableWindow (awin, FALSE);
	  return 0;
}

void SEARCHFRM::SetParams (HINSTANCE hMainInst, HWND hMainWindow)
{
      this->hMainInst   = hMainInst;
      this->hMainWindow = hMainWindow;
      awin = hMainWindow;
}

BOOL SEARCHFRM::GetKey (char * key, char * bez)
{
      int anz, i;
     
      if (idx == -1) return FALSE;
      anz = wsplit (Key, "|");
      if (anz < 1) return FALSE;
      strcpy (key, wort[0]);
      strcpy (bez, wort[1]);
	  for (i = 2; i < anz; i++)
	  {
		strcat (bez, wort[i]);
	  }
      return TRUE;
}


void SEARCHFRM::Search (void)
{
  	  int cx, cy;
	  char buffer [256];

	  idx = -1;
      cx = 48;
      cy = 25;
      Settchar ('|');
      EnableWindow (hMainWindow, FALSE);
      Query = new CHQEX (cx, cy);
      Query->EnableSort (TRUE);
      Query->OpenWindow (hMainInst, hMainWindow);
      sprintf (buffer, "%s %s",
                          "%s",
                          "%s");
      Query->RowAttr (buffer);
	  sprintf (buffer, "    %6s", "1"); 
	  Query->VLines (buffer, 3);
      sprintf (buffer, "4;12 19;26");
      Query->RowPos (buffer);
	  EnableWindow (awin, FALSE);

	  sprintf (buffer, "%6s     %-11s", "Format", "Bezeichnung"); 
	  Query->InsertCaption (buffer);

	  Query->SetFillDb (ReadLst);
	  Query->SetSearchLst (SearchLst);
      ReadLst ("");
      Query->SetSortRow (1, TRUE);
	  Query->ProcessMessages ();
      EnableWindow (hMainWindow, TRUE);
      if (syskey != KEY5)
      {
              idx = Query->GetSel ();
              if (idx != -1)
              {
                     Query->GetText (Key);
              }
      }
	  EnableWindow (awin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (awin);
}

