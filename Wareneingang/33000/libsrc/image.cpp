#include <windows.h>
#include "stdio.h"
#include "image.h"

IMG::IMG (HWND hWnd, char *bname, char *mname)
{
          HDC hdc;
 
          this->hWnd = hWnd;
          hdc = GetDC (hWnd);
          if (bname) bmp.ReadBitmap (hdc, bname);
          if (mname) msk.ReadBitmap (hdc, mname);
		  ReleaseDC (hWnd, hdc);
}

IMG::IMG (HWND hWnd, HBITMAP hbmp, HBITMAP hmsk)
{
          HDC hdc;
 
          this->hWnd = hWnd;
          hdc = GetDC (hWnd);
          if (hbmp)  bmp.SetBitmap (hbmp);
          if (hmsk) msk.SetBitmap (hmsk);
}

void IMG::Print (HWND hWnd, int x, int y)
{
          HDC hdc;
          hdc = GetDC (hWnd);
	      if (msk.GetBitmap ())
		  {
                 msk.DrawBitmapEx (hdc, x, y, SRCAND);
                 bmp.DrawBitmapEx (hdc, x, y, SRCPAINT);
		  }
		  else
		  {
                 bmp.DrawBitmapEx (hdc, x, y, SRCCOPY);
		  }
		  ReleaseDC (hWnd, hdc);
}

char *IMG::getImageNr (char *str, int nr)
{
            sprintf (str, "#%d", nr);
            return str;
}

         