#ifndef _MO_KOMPL_DEF
#define _MO_KOMPL_DEF


class KompBest
{
        private :
            short mdntab[1000]; 
            short filtab[1000];
            long auftab[1000];
            int aufanz;
            short mdntablock[1000]; 
            short filtablock[1000];
            long auftablock[1000];
            int aufanzlock;
            long drklstab [1000];
            int drklsanz;
            int auf_a_sort; 
			static char *llformat;
			static char drformat [];
			static char drfaxformat [];
			static char kontraktformat [];
            BOOL KontraktIsSet;
            BOOL kontrakt_kz; 
			BOOL IsGroup;
			long max_best;
			static HANDLE Pid;
			static int prnret;
        public :
            KompBest () : auf_a_sort (0), IsGroup (FALSE), aufanz (0), aufanzlock (0)
            {
                kontrakt_kz = FALSE; 
                KontraktIsSet = FALSE;
            }

            void SetKontraktKz (BOOL kz)
            {
                kontrakt_kz = kz;
            }     

			void SetDrFormat (char *format)
			{
				strcpy (drformat, format);
                if (KontraktIsSet == FALSE)
                {
         				strcpy (kontraktformat, format);
                }
			}

			void SetDrFaxFormat (char *format)
			{
				strcpy (drfaxformat, format);
			}

			void SetKontraktFormat (char *format)
			{
				strcpy (kontraktformat, format);
                KontraktIsSet = TRUE;
			}

			long Getmax_best (void)
			{
				return max_best;
			}

            static int BreakPrn (int);
            static int BreakPrn0 (int);
			static int BreakAufb (int);
            void SetPrintMode (int);
            void SetKompDefault (int);
            void SetDrkLsSperr (BOOL);
            void SetLiefMePar (int);
            void SetLog (int);
            void SetKlstPar (int);
            void SetSplitLs (int);
            void BearbKomplett (void);
            int  SetKomplett (HWND);
            void ShowLocks (HWND);
            void ShowNoDrk (HWND hWnd);
			void InitBestanz (void);
			void KompParams (void);
            void Getauf_a_sort (void);
            void K_Liste_Direct ();
            void K_Liste_Fax ();
            void K_Liste ();
            void K_ListeGroup (short, short, short, short,long, long,
							   char *, char *, short, short);
            void FreeBest (void);
            void LockBest (void);
            void TestFree (void);
			int PrintBest (HWND, short, short, long);
			int FaxBest (HWND, short, short, long);
			static BOOL IsLlFormat ();
            void K_LlListe ();
/*
            BOOL BsdArtikel (double);
            void BucheBsd (double, double,  double);
            void SetBsdKz (int);
*/

};
#endif
