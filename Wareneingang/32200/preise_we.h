#ifndef _PREISE_WE_DEF
#define _PREISE_WE_DEF

#include "dbclass.h"

struct PREISE_WE {
   short     mdn;
   double    a;
   char      lief[17];
   long      best_blg;
   long      best_term;
   double    pr_ek_b;
   short     me_einh_b;
   char      lief_rech_nr[17];
   long      we_dat;
   double    pr_ek_w;
   short     me_einh_w;
};
extern struct PREISE_WE preise_we, preise_we_null;

#line 7 "preise_we.rh"

class PREISE_WE_CL : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               PREISE_WE_CL () : DB_CLASS ()
               {
               }
};
#endif
