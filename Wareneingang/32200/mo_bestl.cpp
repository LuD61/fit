//220709 Artikel vervollst�ndigen
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include "comcthlp.h"
#include "itemc.h"
#include "wmaskc.h"
#include "mo_meld.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_intp.h"
#include "listcl.h"
#include "inflib.h"
#include "mo_arg.h"
#include "mo_curso.h"
#include "a_hndw.h"
#include "a_bas.h"
#include "best_kopf.h"
#include "best_pos.h"
#include "preise_we.h"
#include "ptab.h"
#include "mo_bestl.h"
#include "mo_qa.h"
#include "mdn.h"
#include "fil.h"
#include "lief.h"
#include "lief_bzg.h"
#include "mo_wepr.h"
#include "sys_par.h"
#include "mo_einh.h"
#include "mo_atxtl.h"
#include "mo_nr.h"
#include "best_bdf.h"
#include "best_res.h"
#include "mo_menu.h"
#include "bsd_buch.h"
#include "mo_progcfg.h"
#include "liefmebest.h"
#include "bestpt.h"
#include "mo_stdac.h"
#include "mo_liefch.h"
#include "LiefBzgDlg.h"
#include "AktionDlg.h"
#include "searchbzg.h"
#include "searchpreise_we.h"
#include "lbek.h"

#define MAXTRIES 100
#define MAXLEN 40
#define MAXPOS 5000
#define LPLUS 1

#define MAXME 9999999.99
#define MAXPR 999999.99

#define TLEN 60

#define HNDW 1
#define EIG 2
#define EIG_DIV 3

extern HANDLE  hMainInst;

static HWND hMainWin;
static unsigned char ausgabebuffer [0x1000];
static HWND mamain1;
static HWND eWindow;
static HWND AufMehWnd = NULL;
static HWND AufMehWnd0 = NULL;
static HWND BasishWnd;
static PAINTSTRUCT aktpaint;

static COLORREF MessBkCol = DKYELLOWCOL; 
static COLORREF MessCol   = BLACKCOL; 

static ITEM iOK ("", "     OK     ", "", 0);
static ITEM iCA ("", "  Abbrechen ", "", 0);

static int ListFocus = 3;

static long akt_lager;

static int rab_prov_kz = 0;
static int auf_wert_anz = 0;
static int a_kun_smt = 0;
static double pr_ek;
static double pr_ek_bto;
static double pr_ek_bto1;
static int preistest;
static double prproz_diff = 50.0;
static int  art_un_tst = 0;
static BOOL  add_me = FALSE;
static BOOL a_kum_par = 0;
static BOOL sacreate = NULL;
static HWND SaWindow = NULL;
static HWND PlusWindow = NULL;
static BOOL add;
static double aufme_old;
static BOOL pr0test = FALSE;
static BOOL textinpmode = TRUE;
static int Stndmode = 1;
static int we_neu_a_par = 0;
static BOOL PrInBestEinh = FALSE;
int AnzTagePreise = 0;

struct AUFPS 
{
       char posi [80];
       char sa_kz_sint [80];
       char fix_kz_sint [20];
       char a [80];
       char lief_best [20];
       char a_bz1 [80];
       char a_bz2 [80];
	   char last_me[80];
       char auf_me [80];
       char me_bz [80];
       char pr_ek [80];
       char pr_ek_bto [80];
       char basis_me_bz [80];
       char teil_smt [5];
       char me_einh_lief [5];
       char me_einh [5];
       char best_txt [9];
       char lief_me [80];
       char pr_ek_dm [80];
       char pr_ek_bto_dm [80];
       char pr_ek_euro [80];
       char pr_ek_bto_euro [80];
       char pr_ek_fremd [80];
       char pr_ek_dm_bto [80];
       char pr_ek_euro_bto [80];
       char pr_ek_fremd_bto [80];
       char pos_wrt [80];
       char me_kz [10];
       double inh;
       double min_best;
};

struct AUFPS bestps, bestptab [MAXPOS], bestps_null;

ITEM iposi        ("posi",       bestps.posi,             "", 0);
ITEM isa_kz_sint  ("sa_kz_sint", bestps.sa_kz_sint, "", 0);
ITEM ifix_kz_sint  ("fix_kz_sint", bestps.fix_kz_sint, "", 0);
ITEM ia           ("a",          bestps.a,          "", 0);
ITEM ilief_best   ("a_kun",      bestps.lief_best,  "", 0);
ITEM ia_bz1       ("a_bz1",      bestps.a_bz1,      "", 0);
ITEM ia_bz2       ("a_bz2",      bestps.a_bz2,      "", 0);
ITEM ilast_me     ("last_me",    bestps.last_me,    "", 0);
ITEM iauf_me      ("auf_me",     bestps.auf_me,     "", 0);
ITEM ime_bz       ("me_bz",      bestps.me_bz,      "", 0);
ITEM ipr_ek       ("pr_ek",      bestps.pr_ek,  "", 0);
ITEM ipr_ek_bto   ("pr_ek_bto",  bestps.pr_ek_bto, "", 0);
ITEM ibasis_me_bz ("basis_me_bz",bestps.basis_me_bz,"", 0);
ITEM ipos_wrt     ("pos_wrt",    bestps.pos_wrt, "", 0);


static field  _dataform[] = {
&iposi,        4, 0, 0,  7, 0, "%4d",     DISPLAYONLY, 0, 0, 0, 
//&isa_kz_sint,  3, 0, 0, 13, 0, "%1d",    DISPLAYONLY, 0, 0, 0, 
&ifix_kz_sint,  3, 0, 0, 13, 0, "%1d",    DISPLAYONLY, 0, 0, 0, 
&ia,          16, 0, 0, 18, 0, "",       DISPLAYONLY, 0, 0, 0, 
&ilief_best,  16, 0, 1, 18, 0, "",       REMOVED, 0, 0, 0, 
&ia_bz1,      24, 0, 0, 36, 0, "",       DISPLAYONLY, 0, 0, 0, 
&ilast_me,    12, 0, 0, 61, 0, "%10.3f", DISPLAYONLY, 0, 0, 0, 
&iauf_me,     12, 0, 0, 75, 0, "%10.3f", EDIT,        0, 0, 0, 
&ime_bz ,     11, 0, 0, 89, 0, "",       DISPLAYONLY, 0, 0, 0,
&ipr_ek_bto,  10, 0, 0,100,0, "%6.2f",   EDIT, 0, 0, 0, 
&ipr_ek,      10, 0, 0,112,0, "%6.2f",   DISPLAYONLY, 0, 0, 0, 
&ipos_wrt,    12, 0, 0,124, 0,"%8.2f",   DISPLAYONLY, 0, 0, 0,
&ibasis_me_bz,11, 0, 0,138, 0, "",       DISPLAYONLY, 0, 0, 0,
&ia_bz2,      24, 0, 1, 36, 0, "",       DISPLAYONLY, 0, 0, 0, 
};

static int ubrows [] = {0, 
                        1,
                        2,2,
                        3,4,5,6,7,8,9,
						10,10,
};

static form dataform = {13, 0, 0, _dataform, 0, 0, 0, 0, NULL}; 

static BOOL DelLadVK  = TRUE;
static BOOL DelLastMe = TRUE;
static BOOL DelPosWrt = TRUE;

static BOOL FormOK = FALSE;

void DelFormField (form *frm, int pos)
/**
Feld aus Dataform herausnehmen.
**/
{
	         int i;
			 int diff;
			 int pos1, pos2;

			 pos1 = frm->mask[pos].pos[1];
			 if (pos < frm->fieldanz - 1)
			 {
				 pos2 = frm->mask[pos + 1].pos[1];
				 diff = max (0, pos2 - pos1);
			 }
			 else
			 {
				 diff = frm->mask[pos].length;
				 if (frm != &dataform) diff ++;
			 }
			 if (diff == 0)
			 {
				 diff = frm->mask[pos].length;
				 if (frm != &dataform) diff ++;
			 }

			 frm->fieldanz --;

			 for (i = pos; i < frm->fieldanz; i ++)
			 {
				 memcpy ((char *) &frm->mask[i], (char *) &frm->mask[i + 1], sizeof (field));
			 }

			 for (i = 0; i < frm->fieldanz; i ++)
			 {
				 if (frm->mask[i].pos[1] > pos1)
				 {
					 frm->mask[i].pos[1] -= diff;
				 }
			 }
}



struct CHATTR ChAttra [] = {"a",     DISPLAYONLY, EDIT,
                             NULL,  0,           0};
struct CHATTR ChAttrlief_best [] = {"a_kun", DISPLAYONLY, EDIT,
                                     NULL,   0,           0};

struct CHATTR *ChAttr = ChAttra;

ColButton Cuposi = {  "Pos.", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};
ColButton Cusa_kz_sint = {
                     "S", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};
ColButton Cufix_kz_sint = {
                     "F", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};
ColButton Cua = {
                     "Artikel-Nr", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};
ColButton Cua_bz1 = {
                     "Bezeichnung1", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};

ColButton Cua_bz2 = {
                     "Bezeichnung2", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};

ColButton Cuauf_me = {
                     "A.-Menge", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};

ColButton Cume_bz = {
                     "Best.ME", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};

ColButton Cupr_vk = {
                     "EK", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};

ColButton Cubasis_me_bz = {
                     "Basis-ME", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};


static char *EK        = "  EK    "; 
static char *EK_DM     = "  EK    ";
static char *EK_EURO   = " EK EURO ";
static char *EK_FREMD  = " EK FREMD ";

static char *EB        = " EK.Bto "; 
static char *EB_DM     = " EB DM  ";
static char *EB_EURO   = " EB EURO ";
static char *EB_FREMD  = "EB FREMD";


ITEM iuposi        ("posi",       "Pos.",            "", 0);
ITEM iusa_kz_sint  ("sa_kz_sint", "S",               "", 0);
ITEM iufix_kz_sint  ("sa_kz_sint", "F",               "", 0);
ITEM iua           ("a",          "Artikel-Nr   ",   "", 0);
ITEM iua_bz1       ("a_bz1",      "Bezeichnung 1",   "", 0);
ITEM iua_bz2       ("a_bz2",      "Bezeichnung 2",   "", 0);
ITEM iulast_me     ("last_me",    "Letz.Bst",        "", 0);
ITEM iuauf_me      ("auf_me",     "A.-Menge",        "", 0);
ITEM iume_bz       ("me_bz",      "Best.ME",         "",  0);
ITEM iupr_ek       ("pr_ek",       EK,               "", 0);
ITEM iupr_ek_bto   ("pr_ek_bto",   EB,               "", 0);
ITEM iubasis_me_bz ("basis_me_bz","Basis-ME",        "",  0);
ITEM iupos_wrt     ("pos_wrt",    "Pos.Wert",        "",  0);

static field  _ubform[] = {
&iuposi,        6, 0, 0,  6, 0, "",  BUTTON, 0, 0, 0, 
//&iusa_kz_sint,  5, 0, 0, 12, 0, "",  BUTTON, 0, 0, 0, 
&iufix_kz_sint,  5, 0, 0, 12, 0, "",  BUTTON, 0, 0, 0, 
&iua,          18, 0, 0, 17, 0, "",  BUTTON, 0, 0, 0, 
&iua_bz1,      25, 0, 0, 35, 0, "",  BUTTON, 0, 0, 0, 
// &iua_bz2,      25, 0, 1, 35, 0, "",  BUTTON, 0, 0, 0, 
&iulast_me,    14, 0, 0, 60, 0, "",  BUTTON, 0, 0, 0, 
&iuauf_me,     14, 0, 0, 74, 0, "",  BUTTON, 0, 0, 0, 
&iume_bz,      13, 0, 0, 88, 0, "",  BUTTON, 0, 0, 0,
&iupr_ek_bto,  12, 0, 0, 99,0,  "",  BUTTON, 0, 0, 0, 
&iupr_ek,      12, 0, 0,111,0,  "",  BUTTON, 0, 0, 0, 
&iupos_wrt,    14, 0, 0,123,0,  "",  BUTTON, 0, 0, 0, 
&iubasis_me_bz,13, 0, 0,137, 0, "",  BUTTON, 0, 0, 0,
};


static form ubform = {11, 0, 0, _ubform, 0, 0, 0, 0, NULL}; 

ITEM iline ("", "1", "", 0);


static field  _lineform[] = {
&iline,      1, 0, 0, 12, 0, "",  NORMAL, 0, 0, 0, 
&iline,      1, 0, 0, 17, 0, "",  NORMAL, 0, 0, 0, 
&iline,      1, 0, 0, 35, 0, "",  NORMAL, 0, 0, 0, 
&iline,      1, 0, 0, 60, 0, "",  NORMAL, 0, 0, 0, 
&iline,      1, 0, 0, 74, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0, 88, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0, 99, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0,111, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0,123, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0,137, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0,149, 0, "",  NORMAL, 0, 0, 0,
};

static form lineform = {11, 0, 0, _lineform, 0, 0, 0, 0, NULL}; 


static ListClassDB eListe;
PTAB_CLASS ptab_class;
WE_PREISE WePreis;
HNDW_CLASS HndwClass;
SYS_PAR_CLASS sys_par_class;
static QueryClass QClass;
// static BEST_KOPF_CLASS bestk_class;
static BEST_POS_CLASS bestp_class;
static PREISE_WE_CL preise_we_class;
static DB_CLASS DbClass;
static EINH_CLASS einh_class;
static AUFPTLIST TListe;
static AutoNrClass AutoNr;
static BSD_BUCH_CLASS BsdBuch;

static BEST_BDF_CLASS best_bdf_class;
static BEST_RES_CLASS best_res_class;
static PROG_CFG ProgCfg ("32200");
//static SMTG_CLASS Smtg;
//static PROV Prov;
static int plu_size = 4;
static int auf_me_default= 0;
static long aufkunanz = 5;

//static StndAuf StndAuf;
static BOOL searchadirect = TRUE;
static BOOL searchmodedirect = TRUE;
static double RowHeight = 1.5;
static int UbHeight = 0;
static int bsd_kz = 1;

static BOOL NoArtMess = FALSE;

static BOOL ListColors = TRUE;
static BOOL ber_komplett = TRUE;
static COLORREF KompfColor = BLACKCOL;
static COLORREF KompbColor = GRAYCOL;
static COLORREF SafColor   = WHITECOL;
static COLORREF SabColor   = BLACKCOL;


static char LiefItem[] = {"liefa"};
static double Akta;
static BOOL best_me_pr_0 = 0;
static int preis0_mess = 1;
static double inh = 0.0;
static double pr_ek_einh = 0.0;
static BOOL RemovePr = FALSE;

static double akt_me;
static double AktKontraktMe;
static double AktBsdMe;
static BOOL LstBestEk = FALSE;
static int Artikeloffset1 = 0;
static int Artikeloffset2 = 0;


static TEXTMETRIC textm; 

static int EnterBreak ()
{
	 break_enter ();
	 return (0);
}

static int EnterTest (void)
/**
Abfrage in Query-Eingabe.
**/
{
        switch (syskey)
        {
                case KEY5 :
                       break_enter ();
                       return 1;
                case KEY11 :
                case KEY12 :
                       syskey = KEY12;
                       break_enter ();
                       return 1;
        }
		if (syskey == KEYCR)
		{
		        if (current_form->mask[currentfield].BuId == KEY12)
				{
					syskey = KEY12;
					break_enter ();
					return 1;
				}
		        else if (current_form->mask[currentfield].BuId == KEY5)
				{
					syskey = KEY5;
					break_enter ();
					return 1;
				}
		}
        return 0;
}


void SetUbHeight (void)
/**
Hoehe der Listueberschrift setzen.
**/
{
	int i;

	for (i = 0; i < ubform.fieldanz; i ++)
	{
		ubform.mask[i].rows = UbHeight;
	}
}

static int InfoProc (char **Item, char *Value, char *where)
/**
Spezielle Procedure fuer Info-System
**/
{

    clipped (*Item);
//    if (strcmp (*Item, "a") == 0)
    {
        *Item = LiefItem;
        sprintf (where, "where mdn = %hd and fil = %hd and "
			"            lief = %s and a = %.0lf", 
                         best_kopf.mdn, 
                         best_kopf.fil, 
						 best_kopf.lief, 
						 ratod (Value));
        return 1;
    }
    return 0;
}

BOOL BESTPLIST::textinpmode = TRUE;
int BESTPLIST:: TestKontraktSoll = 1;
BOOL BESTPLIST::WithSearchBest = FALSE;
BOOL BESTPLIST::CompareMode = Artikel;

int (*BESTPLIST::SetLief) (char *) = NULL;

void BESTPLIST::SetMessColors (COLORREF color, COLORREF bkcolor)
/**
Farben fuer Melungen setzen.
**/
{
	 MessCol   = color;
	 MessBkCol = bkcolor;
}


void BESTPLIST::SethMainWindow (HWND hMainWindow)
{
    this->hMainWindow = hMainWindow;
	hMainWin = hMainWindow;
}

BESTPLIST:: BESTPLIST ()
{
    mamainmax = 0;
    mamainmin = 0; 
    eingabesatz = NULL;
    ausgabesatz = ausgabebuffer;
    zlen = 0;
    feld_anz = 0;
    banz = 0;
    PageView = 0;
    LstZiel = NULL;
    LstSatz   = NULL;
    Lstbanz = 0;
    SwRecs = 0;
    AktRow = 0;
    AktColumn = 0;
    scrollpos = 0;
    InfoCaption[0] = (char) 0; 
    WithMenue   = 1;
    WithToolBar = 1;
	preistest = 0;
	add = FALSE;
	aufme_old = (double) 0.0;
    dataform.after  = WriteRow; 
    dataform.before = SetRowItem; 
    dataform.mask[2].before = Savea; 
    dataform.mask[2].after  = fetcha; 
    dataform.mask[3].after  = fetchlief_best; 
    dataform.mask[6].before = setkey9me; 
    dataform.mask[6].after  = testme; 
    dataform.mask[8].before  = setkey9basis; 
    dataform.mask[8].after   = testekbto; 
	CompareMode = Artikel;
    ListAktiv = 0;
    inh = (double) 0.0;
    this->hMainWindow = NULL;
	hMainWin = NULL;
}

void BESTPLIST::SetLager (long lgr)
{
	akt_lager = lgr;
}

int BESTPLIST::ShowLief (void)
/**
Lieferanten zum Artikel anzeigen.
**/
{
    LIEFAC *Liefac;
	char liefc [17];
	int ret;

	if (ratod (bestps.a) == 0.0)
	{
		disp_mess ("Keine Artikel-Nummer vorhanden", 2);
        return 0;
	}

	FuellePreise_we();
    EnableWindows (eListe.Getmamain3 (), FALSE); 
	Liefac = new LIEFAC;
    ret = Liefac->Show (hMainWin, best_kopf.mdn, best_kopf.fil, ratod (bestps.a), liefc,AnzTagePreise);
    EnableWindows (eListe.Getmamain3 (), TRUE); 
	delete Liefac;
    eListe.SetFeldFocus0 (eListe.GetAktRow (), eListe.GetAktColumn ());
	if (ret == 0)
	{
		return 1;
	}
	if (SetLief)
	{
		strcpy (best_pos.lief, liefc);
		(*SetLief) (liefc);
		strcpy (best_kopf.lief, liefc); 
		ReadPr ();

        liefmebest.mdn = best_kopf.mdn;
        liefmebest.fil = best_kopf.fil;
        strcpy (liefmebest.lief, best_kopf.lief);
        liefmebest.a = ratod (bestps.a);
        ReadMeEinh (bestps.lief_best);

        memcpy (&bestptab[eListe.GetAktRow()], &bestps, sizeof (struct AUFPS));
        eListe.ShowAktRow ();
	}
	return 1;
}


int BESTPLIST::ShowBasis (void)
/**
Basismenge der aktuellen Bestellmenge anzeigen.
**/
{

/*
	HDC hdc;
	double auf_me;
	double a;
	short me_einh_kun;
	double auf_me_vgl;
    KEINHEIT keinheit;
	HWND hWnd; 
	HWND hMainWindow; 

	static char basis_me[15];
	static char basis_einh[11];


    static mfont anzfont    = {NULL, -1, -1, 1,
                               BLACKCOL,
                               WHITECOL,
                               0};

	static ITEM ibasis_me    ("basis_me", basis_me, "Menge in Basiseinheit :", 0);
	static ITEM ibasis_einh  ("me_einh",  basis_einh, "", 0);
	static ITEM iOK          ("OK",       "OK", "", 0);

	static field _fbasis_me[] = {
		&ibasis_me,      9, 0, 1, 2, 0, "%8.2f", READONLY, 0, 0, 0,
		&ibasis_einh,    8, 0, 1,36, 0, "",      DISPLAYONLY, 0, 0, 0,
		&iOK,           10, 0, 3,17, 0, "",      BUTTON,      0, StopEnter, KEY5,
	};

	static form fbasis_me = {3, 0, 0, _fbasis_me, 0, 0, 0, 0, NULL};


	anzfont.FontName = "                   ";
    GetStdFont (&anzfont);
    anzfont.FontAttribute = 1;

	auf_me = ratod (bestps.auf_me);
	a  = ratod (bestps.a);
	me_einh_kun = atoi (bestps.me_einh_kun);

    einh_class.AktAufEinh (angk.mdn, angk.fil,
                                angk.kun, a, me_einh_kun);
    einh_class.GetKunEinh (angk.mdn, angk.fil,
                              angk.kun, a, &keinheit);
          
    if (keinheit.me_einh_kun == keinheit.me_einh_bas)
    {
            auf_me_vgl = auf_me;
    }
    else
    {
            if (keinheit.inh <= (double) 0.0)
            {
                     keinheit.inh = (double) 1.0;
            }
            auf_me_vgl = auf_me * keinheit.inh;
    }

	sprintf (basis_me, "%8.2lf", auf_me_vgl);
	strcpy  (basis_einh, keinheit.me_einh_bas_bez);

    break_end ();
	SetButtonTab (TRUE);
	hMainWindow = GetActiveWindow ();
    DisablehWnd (hMainWindow);
    SetBorder (WS_POPUP | WS_VISIBLE| WS_DLGFRAME);
	SethStdWindow ("hListWindow");
    hWnd = OpenWindowChC (5, 46, 10, 14, hMainInst, ""); 
	SethStdWindow ("hStdWindow");
	BasishWnd = hWnd;
    hdc = GetDC (hWnd);
    ChoiseLines (hWnd, hdc);
    ReleaseDC (hWnd, hdc);

    SetStaticWhite (TRUE);
    enter_form (hWnd, &fbasis_me, 0, 0);
    SetStaticWhite (FALSE);

	CloseControls (&fbasis_me);
    DestroyWindow (hWnd);
	BasishWnd = NULL;
	AktivWindow = hMainWindow;
	SetButtonTab (FALSE);
    eListe.SetFeldFocus0 (eListe.GetAktRow (), eListe.GetAktColumn ());
*/
	return 0;
}

void BESTPLIST::SetChAttr (int ca)
{
    switch (ca)
    {
          case 0 :
              ChAttr = ChAttra;
              break;
          case 1:
              ChAttr = ChAttrlief_best;
              break;
    }
}

void BESTPLIST::SetFieldAttr (char *fname, int attr)
{
         int i;
         char *feldname;

         for (i = 0; i < dataform.fieldanz; i ++)
         {
             feldname = dataform.mask[i].item->GetItemName ();
             if (feldname && strcmp (feldname, fname) == 0)
             {
                   break;
             }
         }
         if (i == dataform.fieldanz) return;

         dataform.mask[i].attribut = attr;
}

int BESTPLIST::GetFieldAttr (char *fname)
{
         int i;
         char *feldname;

         for (i = 0; i < dataform.fieldanz; i ++)
         {
             feldname = dataform.mask[i].item->GetItemName ();
             if (feldname && strcmp (feldname, fname) == 0)
             {
                   break;
             }
         }
         if (i == dataform.fieldanz) return -1;

         return (dataform.mask[i].attribut);
}

int BESTPLIST::Getib (void)
{
    static BOOL ParOK = 0;
    static int IB;

    if (ParOK) return IB;

    ParOK = 1;
    IB = 0;
    strcpy (sys_par.sys_par_nam,"ib");
    if (sys_par_class.dbreadfirst () == 0)
    {
        if (atoi (sys_par.sys_par_wrt)) 
        {
                  IB = 1;
        }
    }
    return IB;
}

void BESTPLIST::GetWeANeuPar (void)
{
    static BOOL ParOK = 0;

    if (ParOK) return;

    ParOK = 1;
    we_neu_a_par = 0;
    strcpy (sys_par.sys_par_nam,"we_neu_a_par");
    if (sys_par_class.dbreadfirst () == 0)
    {
        if (atoi (sys_par.sys_par_wrt)) 
        {
                  we_neu_a_par = 1;
        }
    }
}

void BESTPLIST::Geta_kum_par (void)
{
    static BOOL ParOK = 0;

    if (ParOK) return;

    ParOK = 1;
    a_kum_par = TRUE;
    strcpy (sys_par.sys_par_nam,"a_kum_we_par");
    if (sys_par_class.dbreadfirst () == 0)
    {
        if (atoi (sys_par.sys_par_wrt)) 
        {
                  a_kum_par = TRUE;
        }
    }
    return;
}


void BESTPLIST::Geta_bz2_par (void)
{
    static BOOL ParOK = 0;

    if (ParOK) return;

    ParOK = 1;
    SetFieldAttr ("a_bz2", REMOVED);
    return;
    strcpy (sys_par.sys_par_nam,"a_bz2_par");
    if (sys_par_class.dbreadfirst () == 0)
    {
        if (atoi (sys_par.sys_par_wrt)) 
        {
                  SetFieldAttr ("a_bz2", DISPLAYONLY);
                  return;
        }
    }
    SetFieldAttr ("a_bz2", REMOVED);
//    eListe.DestroyField (eListe.GetFieldPos ("a_bz2")); 
}

/*
void BESTPLIST::Getbest_me_pr0 (void)
{
    BOOL ParOK = 0;

    if (ParOK) return;

    ParOK = 1;
    best_me_pr_0 = 0;
    strcpy (sys_par.sys_par_nam,"best_me_pr_0");
    if (sys_par_class.dbreadfirst () != 0)
    {
        return;
    }
    best_me_pr_0 = atoi (sys_par.sys_par_wrt);
}
*/

/*
void BESTPLIST::Getwa_pos_txt (void)
{
    BOOL ParOK = 0;

    if (ParOK) return;

    ParOK = 1;
	wa_pos_txt = FALSE;
    strcpy (sys_par.sys_par_nam,"wa_pos_txt");
    if (sys_par_class.dbreadfirst () == 0)
	{
                 wa_pos_txt = atoi (sys_par.sys_par_wrt); 
	}
}
*/

double BESTPLIST::GetBestMeVgl (double a,char *lief_best, double best_me, short me_einh_lief)
/**
Mengeneinheit fuer Lieferant und Artikel holen.
**/
{
         KEINHEIT keinheit;
         double best_me_vgl;

         einh_class.AktBestEinh (best_kopf.mdn, best_kopf.fil,
                                best_kopf.lief, a,lief_best, me_einh_lief);
         einh_class.GetLiefEinh (best_kopf.mdn, best_kopf.fil,
                                 best_kopf.lief, a,lief_best, &keinheit);
          
         if (keinheit.me_einh_lief == keinheit.me_einh_bas)
         {
                      best_me_vgl = best_me;
         }
         else
         {
                     if (keinheit.inh <= (double) 0.0)
                     {
                                   keinheit.inh = (double) 1.0;
                     }
                     best_me_vgl = best_me * keinheit.inh;
         }
         return best_me_vgl;
}

int BESTPLIST::SetRowItem (void)
{
	   int pos;
	   double auf_me;

	   pos = eListe.GetAktRow ();
       eListe.SetRowItem ("a", bestptab[pos].a);
	   auf_me = ratod (bestptab[pos].auf_me);
	   if (auf_me == (double) 0.0)
	   {
		   akt_me = (double) 0.0;
	   }
	   else
	   {

           AktBsdMe = GetBestMeVgl (ratod (bestptab[pos].a),bestptab[pos].lief_best, auf_me,
			                      atoi (bestptab[pos].me_einh_lief));

           akt_me = auf_me;
	   }
	   pr_ek = ratod (bestps.pr_ek);
//       TestSaPr ();
       TestFixPr ();
       return 0;
}

int BESTPLIST::TestAppend (void)
/**
Testen, ob ein Satz angehaengt werden darf.
**/
{
    if (eListe.GetRecanz () == 0);
    else if (ratod (bestps.a) == (double) 0.0)
    {
        return FALSE;
    }
  
	pr_ek = (double) 0.0;
	memcpy (&bestps, &bestps_null, sizeof (struct AUFPS)); 
    eListe.SetPos (eListe.GetAktRow (), eListe.FirstColumn ());
    return TRUE;
}

int BESTPLIST::DeleteLine (void)
/**
Zeile aus Liste loeschen.
**/
{
/*
   	    BucheBsd (ratod (bestps.a), (double) 0.0, atoi (bestps.me_einh_kun),
			      ratod (bestps.auf_vk_pr));
*/
        eListe.DeleteLine ();
 	    AnzBestWert ();
        return 0;
}



int BESTPLIST::InsertLine (void)
/**
Zeile aus Liste loeschen.
**/
{
        eListe.SetPos (eListe.GetAktRow (), eListe.FirstColumn ());
        eListe.InsertLine ();
        return 0;
}


int BESTPLIST::PosRab (void)
/**
Zeile aus Liste loeschen.
**/
{

//	    EnterPosRab ();
        return 0;
}

int BESTPLIST::AppendLine (void)
/**
Zeile aus Liste loeschen.
**/
{
	    pr_ek     = (double) 0.0;
	    pr_ek_bto = (double) 0.0;
        eListe.AppendLine ();
        return 0;
}


BOOL BESTPLIST::BsdArtikel (double a)
/**
Bestandskennzeichen in a_bas pruefen.
**/
{
	     char bsd_kz [2];

		 strcpy (bsd_kz, "N");
		 DbClass.sqlin ((double *) &a, 3, 0);
		 DbClass.sqlout ((char *) bsd_kz, 0, 2);
		 DbClass.sqlcomm ("select bsd_kz from a_bas where a = ?");
		 if (bsd_kz[0] == 'J') return TRUE;
		 return FALSE;
}


void BESTPLIST::BucheBsd (double a, double auf_me, short me_einh_lief,char *lief_best, double pr_ek_bto)
/**
Bestandsbuchung vorbereiten.
**/
{
	double best_me_vgl;
	double buchme;
	char datum [12];

	if (bsd_kz == 0) return;
	if (BsdArtikel (a) == FALSE) return;
    best_me_vgl = GetBestMeVgl (a,lief_best, auf_me,me_einh_lief);
	if (best_me_vgl == AktBsdMe) return;
	buchme = best_me_vgl - AktBsdMe;

	bsd_buch.nr  = best_kopf.best_blg;
	strcpy (bsd_buch.blg_typ, "B");   // Achtung Kennzeichen B noch �berpr�fen
 	bsd_buch.mdn = best_kopf.mdn;
	bsd_buch.fil = best_kopf.fil;
	bsd_buch.kun_fil = 0;
	bsd_buch.a   = a;
	sysdate (datum);
	bsd_buch.dat = dasc_to_long (datum);
	systime (bsd_buch.zeit);
	strcpy (bsd_buch.pers, sys_ben.pers);

    sprintf (bsd_buch.bsd_lgr_ort, "%ld", akt_lager);

	bsd_buch.qua_status = 0;
	bsd_buch.me = buchme;
	bestps.min_best = (bestps.min_best <= 0.0) ? 1.0 : bestps.min_best;
	bsd_buch.bsd_ek_vk = pr_ek / bestps.min_best;
    strcpy (bsd_buch.chargennr, "");
    strcpy (bsd_buch.ident_nr, "");
    strcpy (bsd_buch.herk_nachw, "");
    sprintf (bsd_buch.lief, "%s", best_kopf.lief);
    bsd_buch.auf = best_kopf.best_blg;
    strcpy  (bsd_buch.verfall, "");
    bsd_buch.delstatus = 0;
    strcpy  (bsd_buch.err_txt, "");
	BsdBuch.dbinsert ();
}


int BESTPLIST::UpdateKontrakt (double a)
{
       double auf_me, kontrakt_me_soll; 
       double me;
       short sql_s;
       char buffer [256];
       extern short sql_mode;

	   if (best_kopf.kontrakt_nr == 0l) return 0;
       if (TestKontraktSoll == 0) return 0;

       sql_s = sql_mode;
	   DbClass.sqlin ((short *) &best_kopf.mdn, 1, 0);
	   DbClass.sqlin ((short *) &best_kopf.fil, 1, 0);
	   DbClass.sqlin ((char *) best_kopf.lief, 0, 17);
	   DbClass.sqlin ((long *) &best_kopf.kontrakt_nr, 2, 0);
	   DbClass.sqlin ((double *) &a, 3, 0);
	   DbClass.sqlout ((double *) &auf_me, 3, 0);
	   DbClass.sqlout ((double *) &kontrakt_me_soll, 3, 0);
	   int dsqlstatus = DbClass.sqlcomm ("select me,kontrakt_me_soll from best_pos "
		                                 "where mdn = ? " 
						                 "and fil = ? "
						                 "and lief = ? "
						                 "and best_blg = ? "
						                 "and a = ?");
       if (dsqlstatus == 100)
	   {
		   print_mess (2, "Artikel %.0lf ist nicht im Kontrakt %ld",
			              a, best_kopf.kontrakt_nr);
		   return -1;
	   }
       me = ratod (bestps.auf_me) - akt_me;
       if (me > (auf_me - kontrakt_me_soll))
       {
           if (TestKontraktSoll == 2)
           {
               sprintf (buffer, "Bestellmenge %.3lf ist zu gro�.\n"
                                "Restmenge im Kontrakt : %.3lf\n"
                                "Speichern ?",
                                 me, (auf_me - kontrakt_me_soll));
              if (abfragejn (eListe.Getmamain3 (), 
  					          buffer , "N") == 0)
              {
                     akt_me = 0.0;
                     sprintf (bestptab[eListe.GetAktRow ()].auf_me, "%lf", akt_me);
                     eListe.ShowAktRow ();
                     int pos = GetItemPos (&dataform, "auf_me");
                     if (pos != -1)
                     {
                                eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                                                      pos);
                     }
                     return -1;
              }
           }
           else 
           {
               print_mess (2, "Bestellmenge %.3lf ist zu gro�.\n"
                              "Restmenge im Kontrakt : %.3lf",
                               me, (auf_me - kontrakt_me_soll));
               akt_me = 0.0;
               sprintf (bestptab[eListe.GetAktRow ()].auf_me, "%lf", akt_me);
               eListe.ShowAktRow ();
               int pos = GetItemPos (&dataform, "auf_me");
               if (pos != -1)
               {
                  eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                                        pos);
               }
               return -1;
           }

       }
       kontrakt_me_soll += me;
	   DbClass.sqlin ((double *) &kontrakt_me_soll, 3, 0);
	   DbClass.sqlin ((short *) &best_kopf.mdn, 1, 0);
	   DbClass.sqlin ((short *) &best_kopf.fil, 1, 0);
	   DbClass.sqlin ((char *) best_kopf.lief, 0, 17);
	   DbClass.sqlin ((long *) &best_kopf.kontrakt_nr, 2, 0);
	   DbClass.sqlin ((double *) &a, 3, 0);
       int cursor = DbClass.sqlcursor ("update best_pos set kontrakt_me_soll = ? "
		                               "where mdn = ? " 
						               "and fil = ? "
						               "and lief = ? "
						               "and best_blg = ? "
                                       "and a = ?");
       int tries = 0;
       dsqlstatus = DbClass.sqlexecute (cursor); 
       while (dsqlstatus < 0)
       {
           tries ++;
           if (tries == MAXTRIES) 
           {
               disp_mess ("Kontrakt konnte nicht korrigiert werden", 2);
               return -1;
           }
           Sleep (10);
           dsqlstatus = DbClass.sqlexecute (cursor); 
       }
	   return 0;
}

int BESTPLIST::WriteRow (void)
/**
Zeile beim Verlassen pruefen.
**/
{

    if (testme () == -1) return -1;
    if (testpr () == -1) return -1;
	pr_ek_bto = 0;
    set_fkt (NULL, 9);
    SetFkt (9, leer, NULL);
    if (ratod (bestps.a) == (double) 0.0)
    {
        eListe.DeleteLine ();
        return (1);
    }

    if (UpdateKontrakt (ratod (bestps.a)) == -1)
    {
        return -1;
    }

	BucheBsd (ratod (bestps.a), ratod (bestps.auf_me), atoi (bestps.me_einh_lief),bestps.lief_best,
		      ratod (bestps.pr_ek_bto));


    return (0);
}

int BESTPLIST::TestRow (void)
/**
Zeile beim Verlassen pruefen.
**/
{
     
    if (ratod (bestps.auf_me) == (double) 0.0) return 0;
    if (testme () == -1) return -1;
    if (testpr () == -1) return -1;
    set_fkt (NULL, 9);
    SetFkt (9, leer, NULL);
    if (UpdateKontrakt (ratod (bestps.a)) == -1)
    {
        return -1;
    }

	BucheBsd (ratod (bestps.a), ratod (bestps.auf_me) - akt_me, atoi (bestps.me_einh_lief),bestps.lief_best,
		      ratod (bestps.pr_ek_bto));
    return (0);
}

void BESTPLIST::GenNewPosi (void)
/**
Neue Positionsnummern in 10-er Schritten generieren.
**/
{
    int i;
    int row;
    int recs;
    long posi;

    row  = eListe.GetAktRow ();
    memcpy (&bestptab[row], &bestps, sizeof (struct AUFPS));
    recs = eListe.GetRecanz ();
    posi = 10;
    for (i = 0; i < recs; i ++, posi += 10)
    {
        sprintf (bestptab[i].posi, "%ld", posi);
    }
    eListe.DisplayList ();
    memcpy (&bestps, &bestptab[row], sizeof (struct AUFPS));
}


int BESTPLIST::PosiEnd (long posi)
/**
Positionnummer testen.
**/
{
    int row;
    int recs;
    long nextposi;

    row  = eListe.GetAktRow ();
    recs = eListe.GetRecanz ();

    if (row >= recs - 1)
    {
            return FALSE;
    }

    nextposi = atol (bestptab [row + 1].posi);
    if (nextposi <= posi)
    {
        GenNewPosi ();
        return TRUE;
    }
    return FALSE;
}

void BESTPLIST::TestMessage (void)
{
	MSG msg;

    if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
    {
          TranslateMessage(&msg);
          DispatchMessage(&msg);
    }
}


int BESTPLIST::doStd (void)
/*
Standardauftrag.
*/
{
         double a;
         double me;
         double pr_vk;
         int ret;
         int row;
         int lrow;
		 int SaveRow, SaveColumn;
		 STDAC *Stdac = NULL;
		 struct STD_A *stda;


         save_fkt (5);
         save_fkt (6);
         save_fkt (7);
         save_fkt (8);
         save_fkt (9);
         save_fkt (10);
         save_fkt (11);
         save_fkt (12);
		 SaveRow     = eListe.GetAktRow ();
		 SaveColumn  = eListe.GetAktColumn ();

/*
		 if (Stndmode == 0)
		 {
                 ret = StndAuf.StdAuftrag (eListe.Getmamain3 (), 
                                           angk.mdn, angk.fil,
                                           angk.kun, angk.kun_fil);
		 }
		 else
*/
		 {
			     EnableWindows (eListe.Getmamain3 (), FALSE); 
			     Stdac = new STDAC;
				 ret = Stdac->Show (hMainWin,
                                    best_kopf.mdn, best_kopf.fil,
                                    best_kopf.lief);
			     EnableWindows (eListe.Getmamain3 (), TRUE); 
		 }
         restore_fkt (5);
         restore_fkt (6);
         restore_fkt (7);
         restore_fkt (8);
         restore_fkt (9);
         restore_fkt (10);
         restore_fkt (11);
         restore_fkt (12);
         if (ret == 0)
         {
		         eListe.SetPos (SaveRow, eListe.FirstColumn ());
                 eListe.SetVPos (SaveRow);
                 eListe.ShowAktRow ();
 	             if (Stdac) delete Stdac; 
                 return 0;
         }
         if (ret == 1 && Stndmode == 0);
/*
         {

                  a = StndAuf.GetStda (); 
                  me = StndAuf.GetStdme (); 
                  pr_vk = StndAuf.GetStdpr_vk (); 
                  sprintf (bestps.a,  "%.0lf", a);
                  sprintf (bestps.auf_me, "%.3lf", me); 
                  sprintf (bestps.auf_vk_pr, "%.2lf", pr_vk); 
                  memcpy (&bestptab [eListe.GetAktRow ()], &bestps,
                          sizeof (struct AUFPS));
                  fetcha ();
		 }
*/
		 else if (ret == 1 && Stndmode)
		 {
				  eListe.DestroyFocusWindow ();
                  InvalidateRect (eListe.Getmamain3 (), 0, TRUE);
                  UpdateWindow (eListe.Getmamain3 ());
			      NoArtMess = TRUE;
                  row = 0;
                  lrow = eListe.GetAktRow ();
                  while (stda = Stdac->GetStda (row))
                  {
					    if (stda->nstatus == 0)
						{
							row ++;
							continue;
						}
					    a = ratod (stda->a);
					    me = ratod (stda->me);
					    pr_vk = ratod (stda->pr_vk);

                        eListe.SetVPos (lrow);
	                    if (ratod (bestptab[lrow].a) ||
                            lrow >= eListe.GetRecanz ())
                        {
							  eListe.SetRecanz (lrow);
                        }

                        sprintf (bestps.a,  "%.0lf", a);
                        sprintf (bestps.auf_me, "%.3lf", me); 
                        row ++;
                        sprintf (bestps.pr_ek_bto, "%lf", pr_vk); 
 				        sprintf (bestps.sa_kz_sint, "%1d", 0);
                        memcpy (&bestptab [lrow], &bestps,
                                sizeof (struct AUFPS));
                        fetchaDirect (lrow);
                        lrow ++;
                  }
                  if (ratod (bestptab[lrow].a) ||
                           lrow >= eListe.GetRecanz ())
                  {
							  eListe.SetRecanz (lrow);
                  }
			      NoArtMess = FALSE;
                  AppendLine ();
                  syskey = KEYUP;
                  eListe.FocusUp ();
                  InvalidateRect (eListe.Getmamain3 (), 0, TRUE);
         }
         else if (ret == 2 && Stndmode == 0);
/*
         {
				  eListe.DestroyFocusWindow ();
                  InvalidateRect (eListe.Getmamain3 (), 0, TRUE);
                  UpdateWindow (eListe.Getmamain3 ());
			      NoArtMess = TRUE;
                  row = 0;
                  lrow = eListe.GetAktRow ();
                  while (StndAuf.GetStdRow (row, &a, &me, &pr_vk))
                  {
                        eListe.SetVPos (lrow);
	                    if (ratod (bestptab[lrow].a) ||
                            lrow >= eListe.GetRecanz ())
                        {
							  eListe.SetRecanz (lrow);
                        }

                        sprintf (bestps.a,  "%.0lf", a);
                        sprintf (bestps.auf_me, "%.3lf", me); 
                        row ++;
                        sprintf (bestps.pr_ek, "%.2lf", pr_vk); 
 				        sprintf (bestps.sa_kz_sint, "%1d", 0);
                        memcpy (&bestptab [lrow], &bestps,
                          sizeof (struct AUFPS));
                        fetchaDirect (lrow);
                        lrow ++;
                  }
                  if (ratod (bestptab[lrow].a) ||
                           lrow >= eListe.GetRecanz ())
                  {
							  eListe.SetRecanz (lrow);
                  }
			      NoArtMess = FALSE;
                  AppendLine ();
                  syskey = KEYUP;
                  eListe.FocusUp ();
                  InvalidateRect (eListe.Getmamain3 (), 0, TRUE);
         }
*/
		 else if (ret == 2 && Stndmode)
		 {
				  eListe.DestroyFocusWindow ();
                  InvalidateRect (eListe.Getmamain3 (), 0, TRUE);
                  UpdateWindow (eListe.Getmamain3 ());
			      NoArtMess = TRUE;
                  row = 0;
                  lrow = eListe.GetAktRow ();
                  while (stda = Stdac->GetStda (row))
                  {
					    a = ratod (stda->a);
					    me = ratod (stda->me);
					    pr_vk = ratod (stda->pr_vk);

                        eListe.SetVPos (lrow);
	                    if (ratod (bestptab[lrow].a) ||
                            lrow >= eListe.GetRecanz ())
                        {
							  eListe.SetRecanz (lrow);
                        }

                        sprintf (bestps.a,  "%.0lf", a);
                        sprintf (bestps.auf_me, "%.3lf", me); 
                        row ++;
                        sprintf (bestps.pr_ek, "%lf", pr_vk); 
 				        sprintf (bestps.sa_kz_sint, "%1d", 0);
                        memcpy (&bestptab [lrow], &bestps,
                                sizeof (struct AUFPS));
                        fetchaDirect (lrow);
                        lrow ++;
                  }
                  if (ratod (bestptab[lrow].a) ||
                           lrow >= eListe.GetRecanz ())
                  {
							  eListe.SetRecanz (lrow);
                  }
			      NoArtMess = FALSE;
                  AppendLine ();
                  syskey = KEYUP;
                  eListe.FocusUp ();
                  InvalidateRect (eListe.Getmamain3 (), 0, TRUE);
         }
         memcpy (&bestps, &bestptab [SaveRow], sizeof (struct AUFPS));
		 eListe.SetPos (SaveRow, eListe.FirstColumn ());
         eListe.SetVPos (SaveRow);
         eListe.ShowAktRow ();
 	     if (Stdac) delete Stdac; 
         return 0;
}


int BESTPLIST::ShowBzg ()
{
        struct SBZG *sbzg;
        SEARCHBZG SearchBzg; 
        char buffer [256];

        sprintf (buffer, "lief_bzg.lief = \"%s\" and (lief_bzg.mdn = %hd or lief_bzg.mdn = 0) "
			             "and (lief_bzg.fil = %hd or lief_bzg.fil = 0)", 
						 clipped (best_kopf.lief), best_kopf.mdn, best_kopf.fil);

        SearchBzg.SetQuery (buffer); 
        SearchBzg.SetWindowStyle (WS_POPUP | WS_THICKFRAME);
        SearchBzg.SetParams (hMainInst, hMainWin);
        SearchBzg.Setawin (GetParent (hMainWin));
        SearchBzg.Search (best_kopf.mdn);
        if (syskey == KEY5)
        {
           eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                                 eListe.GetAktColumn ());
            return 0;
        }
        sbzg = SearchBzg.GetSBzg ();
        if (sbzg == NULL)
        {
            return 0;
        }

        sprintf (bestptab[eListe.GetAktRow ()].a, "%.0lf", _a_bas.a);
        eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
        eListe.ShowAktRow ();
        eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                             eListe.GetAktColumn ());
        strcpy (bestptab[eListe.GetAktRow ()].a, sbzg->a);
        strcpy (bestptab[eListe.GetAktRow ()].lief_best, sbzg->lief_best);
        eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
        eListe.ShowAktRow ();
        PostMessage (eListe.Getmamain3 (), WM_KEYDOWN, VK_RETURN, 0l);
        return 0;
}
int BESTPLIST::ShowBzg (char* ca)
{
        struct SBZG *sbzg;
        SEARCHBZG SearchBzg; 
        char buffer [256];

        sprintf (buffer, "lief_bzg.lief = \"%s\" and (lief_bzg.mdn = %hd or lief_bzg.mdn = 0) "
			             "and (lief_bzg.fil = %hd or lief_bzg.fil = 0)", 
						 clipped (best_kopf.lief), best_kopf.mdn, best_kopf.fil);

        SearchBzg.SetQuery (buffer); 
        SearchBzg.SetWindowStyle (WS_POPUP | WS_THICKFRAME);
        SearchBzg.SetParams (hMainInst, hMainWin);
        SearchBzg.Setawin (GetParent (hMainWin));
        SearchBzg.Search (best_kopf.mdn,ca);
        if (syskey == KEY5)
        {
           eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                                 eListe.GetAktColumn ());
            return 0;
        }
        sbzg = SearchBzg.GetSBzg ();
        if (sbzg == NULL)
        {
            return 0;
        }

        sprintf (bestptab[eListe.GetAktRow ()].a, "%.0lf", _a_bas.a);
        eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
        eListe.ShowAktRow ();
        eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                             eListe.GetAktColumn ());
        strcpy (bestptab[eListe.GetAktRow ()].a, sbzg->a);
        strcpy (bestptab[eListe.GetAktRow ()].lief_best, sbzg->lief_best);
        eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
        eListe.ShowAktRow ();
        PostMessage (eListe.Getmamain3 (), WM_KEYDOWN, VK_RETURN, 0l);
        return 0;
}

int BESTPLIST::ShowPreise_we ()
{
//        struct SPREISE_WE *spreise_we;
        SEARCHPREISE_WE SearchPreise_we; 
        char buffer [256];

		if (ratod (bestps.a) == 0.0)
		{
			disp_mess ("Keine Artikel-Nummer vorhanden", 2);
			return 0;
		}
        sprintf (buffer, "lief_bzg.lief = \"%s\"", clipped (best_kopf.lief));
        SearchPreise_we.SetQuery (buffer); 
        SearchPreise_we.SetWindowStyle (WS_POPUP | WS_THICKFRAME);
        SearchPreise_we.SetParams (hMainInst, hMainWin);
        SearchPreise_we.Setawin (GetParent (hMainWin));
        SearchPreise_we.Search (best_kopf.mdn);
        if (syskey == KEY5)
        {
           eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                                 eListe.GetAktColumn ());
            return 0;
        }
        return 0;
}


int BESTPLIST::Querya (void)
/**
Auswahl ueber Artikel.
**/
{
       int ret;

       if (WithSearchBest)
       {
            ShowBzg ();
            return 0;
       }
	   if (searchadirect)
	   {
            ret = QClass.searcha (eListe.Getmamain3 ());
	   }
       else 
	   {
            ret = QClass.querya (eListe.Getmamain3 ());
	   }
       set_fkt (dokey5, 5);
       set_fkt (WriteAllPos, 12);
       if (ret == FALSE)
       {
           eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                                 eListe.GetAktColumn ());
           return 0;
       }
       UpdateWindow (mamain1);
       sprintf (bestptab[eListe.GetAktRow ()].a, "%.0lf", _a_bas.a);
       eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
       eListe.ShowAktRow ();
       eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                             eListe.GetAktColumn ());
       PostMessage (eListe.Getmamain3 (), WM_KEYDOWN, VK_RETURN, 0l);
       return 0;
}
       

int BESTPLIST::Savea (void)
/**
Artikelnummer sichern.
**/
{
       Akta = ratod (bestps.a);
       set_fkt (Querya, 9);
       SetFkt (9, auswahl, KEY9);

       set_fkt (doStd, 10);
       SetFkt (10, standardauf, KEY10);

       return 0;
}

int BESTPLIST::ChMeEinh (void)
/**
Auswahl Auftragsmengeneinheit.
**/
{
       int me_einh; 


       me_einh = atoi (bestps.me_einh);
       liefmebest.mdn = best_kopf.mdn;
       liefmebest.fil = best_kopf.fil;
       strcpy (liefmebest.lief, best_kopf.lief);
       liefmebest.a = ratod (bestps.a);

       if (ratod (bestps.a))
       {
           einh_class.AktBestEinh (best_kopf.mdn, best_kopf.fil, best_kopf.lief, 
                                  ratod (bestps.a),bestps.lief_best, atoi (bestps.me_einh_lief));
       }
       else
       {
           einh_class.SetBestEinh (1);
       }
       EnableWindow (mamain1, FALSE);
       einh_class.ChoiseEinh ();
       EnableWindow (mamain1, TRUE);
       _a_bas.a = ratod (bestps.a);
       if (syskey == KEY5) 
	   {
           eListe.SetFeldFocus0 (eListe.GetAktRow (), eListe.GetAktColumn ());
		   return 0;
	   }

       ReadMeEinh (bestps.lief_best);
       if ((atoi (bestps.me_einh) != me_einh) && PrInBestEinh)
       {
/*
           if (atoi (bestps.me_einh) == lief_bzg.me_einh_ek)
           {
             sprintf (bestps.pr_ek_bto, "%.4lf", lief_bzg.pr_ek);
             bestps.min_best = lief_bzg.min_best;
           }
           else
*/
           {
//ToDo Preise
             sprintf (bestps.pr_ek_bto, "%.4lf", (double) ratod (bestps.pr_ek_bto) / 
                                                                   bestps.min_best);
             sprintf (bestps.pr_ek_bto, "%.4lf", (double) ratod (bestps.pr_ek_bto) * 
                                                                   bestps.inh);

             if (pr_ek_einh != 0.0)
             {
                  sprintf (bestps.pr_ek_bto, "%.4lf", pr_ek_einh);
             }

	         CalcPrNto (ratod (bestps.pr_ek_bto));
             bestps.min_best = bestps.inh;
             if (bestps.min_best == 0.0)
             {
                 bestps.min_best = lief_bzg.min_best;
             }
           }
       }

       memcpy (&bestptab[eListe.GetAktRow()], &bestps, sizeof (struct AUFPS));
       eListe.ShowAktRow ();
       eListe.SetFeldFocus0 (eListe.GetAktRow (), eListe.GetAktColumn ());
       return 0;
}


long BESTPLIST::GenBestpTxt0 (void)
/**
Nummer fuer Lieferschein-Positionstexte generieren.
**/
{
	int count; 
	long nr;

	nr = 0l;
    nr = AutoNr.GetNrBest (nr);
	count = 0;
	while (nr == 0l)
	{
		         Sleep (10);
		         count ++;
				 if (count == 10) break;
                 nr = AutoNr.GetNrBest (nr);
	}
	return nr;
}


BOOL BESTPLIST::TxtNrExist (long nr)
{
	   int dsqlstatus;
	   
	   DbClass.sqlin ((long *)  &nr, 2, 0);
	   dsqlstatus = DbClass.sqlcomm ("select nr from bestpt where nr = ?");
	   if (dsqlstatus == 100) return FALSE;
	   return TRUE;
}


long BESTPLIST::GenBestpTxt (void)
/**
Nummer fuer Lieferschein-Positionstexte generieren.
**/
{
	 long nr;

	 while (TRUE)
	 {
		 nr = GenBestpTxt0 ();
		 if (TxtNrExist (nr) == FALSE) break;
		 if (nr > 99999999) 
		 {
			 return 0l;
		 }
	 }
	 return nr;
}



int BESTPLIST::Texte (void)
/**
Freie Artikeltexte erfassen.
**/
{
    long best_txt; 

    set_fkt (NULL, 9);
    SetFkt (9, leer, 0);
    set_fkt (NULL, 10);
    SetFkt (10, leer, 0);
    set_fkt (NULL, 11);
    SetFkt (11, leer, 0);
    TListe.SethMainWindow (mamain1);
    TListe.SetListLines (12);
//    TListe.SetTextMetric (&textm);
    EnableWindow (mamain1, FALSE);
    best_txt = atol (bestps.best_txt);

	if (best_txt == 0l) best_txt = GenBestpTxt ();

	if (best_txt)
	{
		    if (textinpmode)
			{
				    
			         InputTxt ("Psoitionstext",best_txt);
			}
            else 
			{
                     TListe.EnterAufp (best_kopf.mdn , best_kopf.fil, best_kopf.best_blg, 
                                       atol (bestps.posi), best_txt);
			}
	}
	else
	{
		     syskey = KEY5;
    }
    EnableWindow (mamain1, TRUE);
    SetActiveWindow (mamain1);
    
    if (syskey != KEY5)
    {
             sprintf (bestps.best_txt, "%ld", best_txt);
             memcpy (&bestptab[eListe.GetAktRow()], 
                     &bestps, sizeof (struct AUFPS));
    }
    set_fkt (dokey5, 5);
    set_fkt (InsertLine, 6);
    set_fkt (DeleteLine, 7);

//    if (rab_prov_kz) set_fkt (PosRab, 8);

    set_fkt (WriteAllPos, 12);
    set_fkt (Schirm, 11);

    SetFkt (6, einfuegen, KEY6);
    SetFkt (7, loeschen, KEY7);

//    if (rab_prov_kz) SetFkt (8, posrab, KEY8);

    SetFkt (11, vollbild, KEY11);
    set_fkt (ChMeEinh, 9);
    SetFkt (9, einhausw, KEY9);
    set_fkt (Texte, 10);
    SetFkt (10, texte, KEY10);
    eListe.SetFeldFocus0 (eListe.GetAktRow (), eListe.GetAktColumn ());
    return 0;
}

int BESTPLIST::setkey9me (void)
/**
Artikelnummer sichern.
**/
{
       set_fkt (ChMeEinh, 9);
       SetFkt (9, einhausw, KEY9);
       set_fkt (Texte, 10);
       SetFkt (10, texte, KEY10);
//       set_fkt (ShowPreise_we, 10); //TESTTEST
       return 0;
}

int BESTPLIST::setkey9basis (void)
/**
Artikelnummer sichern.
**/
{
	   pr_ek = ratod (bestps.pr_ek);
	   pr_ek_bto1 = ratod (bestps.pr_ek_bto);
       set_fkt (ShowBasis, 9);
       SetFkt (9, basisme, KEY9);
       return 0;
}

//static ITEM iSaTxt  ("",  "Sonderangebot", "", 0);
static ITEM iSaTxt  ("",  "Fester Preis", "", 0);

static field _fSaTxt [] = {
&iSaTxt,        13,  0, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form fSaTxt = {1, 0, 0, _fSaTxt, 0, 0, 0, 0, NULL};    


void BESTPLIST::MoveSaW (void)
/**
Fenster Sonderangebot bewegen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;

        if (hMainWin == NULL) return;    
        if (AufMehWnd == NULL) return;    
        if (SaWindow == NULL) return;    
           
	    memcpy (&tm, &textm, sizeof (tm)); 

        GetClientRect (hMainWin, &rect);
        GetWindowRect (hMainWin, &wrect);

        y = (wrect.top +  11 * tm.tmHeight);
        cx = 15 * tm.tmAveCharWidth + 2;
        x = wrect.left + rect.right - cx + 1;
        cy = 2 * tm.tmHeight;
		MoveWindow (SaWindow, x, y, cx, cy, TRUE);
}


HWND BESTPLIST::CreateSaW (void)
/**
Fenster fuer Sonderangebot erzeugen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;

        if (hMainWin == NULL) return NULL;    
           
	    memcpy (&tm, &textm, sizeof (tm)); 

        GetClientRect (hMainWin, &rect);
        GetWindowRect (hMainWin, &wrect);


        y = (wrect.top +  11 * tm.tmHeight);
        cx = 15 * tm.tmAveCharWidth + 2;
        x = wrect.left + rect.right - cx + 1;
        cy = 2 * tm.tmHeight;

        SaWindow       = CreateWindow (
                                       "StaticMess",
                                       "",
                                       WS_BORDER | 
                                       WS_POPUP | WS_VISIBLE,
                                       x, y,
                                       cx, cy,
                                       hMainWin,
                                       NULL,
                                       hMainInst,
                                    NULL);


        return SaWindow;
}

void BESTPLIST::PaintSa (HDC hdc)
/**
Text anzeigen.
**/
{
	  RECT rect;
	  SIZE size;
	  int x,y;
	  extern HFONT stdHfont;
//	  static char *txt = "Sonderangebot";
	  static char *txt = "Fester Preis";

	  if (SaWindow == NULL) return;

	  GetClientRect (SaWindow, &rect); 
      SelectObject (hdc, stdHfont);
      GetTextExtentPoint32 (hdc, txt, strlen (txt), &size); 
	  x = max (0, (rect.right  - rect.left - size.cx) / 2);
	  y = max (0, (rect.bottom - rect.top  - size.cy) / 2);
	  SetBkMode (hdc, TRANSPARENT);
      SetTextColor (hdc,MessCol);
      SetBkColor (hdc, MessBkCol);
	  TextOut (hdc, x, y, txt, strlen (txt));
}

void BESTPLIST::CreateSa (void)
/**
Fenster Sonderpreis anzeigen.
**/
{
	   if (SaWindow) return;

	   SaWindow = CreateSaW ();
}

void BESTPLIST::DestroySa (void)
/**
Fenster mit Sonderpreis loeschen.
**/
{
	  if (SaWindow == NULL)
	  {
		  return;
	  }
	  CloseControls (&fSaTxt);
	  DestroyWindow (SaWindow);
	  SaWindow = NULL;
}

void BESTPLIST::TestSaPr (void)
{
       short sa;

       if (ratod (bestps.pr_ek) == 0)
	   {
		   DestroySa ();
		   return;
	   }

	   sa = atoi( bestps.sa_kz_sint);
	   if (sa)
	   {
		   CreateSa ();
	   }
	   else
	   {
		   DestroySa ();
	   }
}

void BESTPLIST::CreateFix (void)
/**
Fenster Sonderpreis anzeigen.
**/
{
	   if (SaWindow) return;

	   SaWindow = CreateSaW ();
}

void BESTPLIST::DestroyFix (void)
/**
Fenster mit Sonderpreis loeschen.
**/
{
	  if (SaWindow == NULL)
	  {
		  return;
	  }
	  CloseControls (&fSaTxt);
	  DestroyWindow (SaWindow);
	  SaWindow = NULL;
}


void BESTPLIST::TestFixPr (void)
{
       short sa;

       if (ratod (bestps.pr_ek) == 0)
	   {
		   DestroyFix ();
		   return;
	   }

	   sa = atoi( bestps.fix_kz_sint);
	   if (sa)
	   {
		   CreateFix ();
	   }
	   else
	   {
		   DestroyFix ();
	   }
}

void BESTPLIST::MovePlus (void)
/**
Fenster Sonderangebot bewegen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;

        if (hMainWin == NULL) return;    
        if (AufMehWnd == NULL) return;    
		if (PlusWindow == NULL) return; 
           
	    memcpy (&tm, &textm, sizeof (tm)); 

        GetClientRect (hMainWin, &rect);
        GetWindowRect (hMainWin, &wrect);

        y = (wrect.top +  9 * tm.tmHeight);
        cx = 3 * tm.tmAveCharWidth + 2;
        x = wrect.left + rect.right - cx + 1;
        cy = 2 * tm.tmHeight;
		MoveWindow (PlusWindow, x, y, cx, cy, TRUE);
}

HWND BESTPLIST::CreatePlus (void)
/**
Fenster fuer Sonderangebot erzeugen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;

        if (hMainWin == NULL) return NULL;    
           
	    memcpy (&tm, &textm, sizeof (tm)); 

        GetClientRect (hMainWin, &rect);
        GetWindowRect (hMainWin, &wrect);


        y = (wrect.top +  9 * tm.tmHeight);
        cx = 3 * tm.tmAveCharWidth + 2;
        x = wrect.left + rect.right - cx + 1;
        cy = 2 * tm.tmHeight;

        PlusWindow       = CreateWindow (
                                       "StaticMess",
                                       "",
                                       WS_BORDER | 
                                       WS_POPUP | WS_VISIBLE,
                                       x, y,
                                       cx, cy,
                                       hMainWin,
                                       NULL,
                                       hMainInst,
                                    NULL);


        return PlusWindow;
}

void BESTPLIST::PaintPlus (HDC hdc)
/**
+ anzeigen.
**/
{
	  RECT rect;
	  SIZE size;
	  int x,y;
	  extern HFONT stdHfont;
	  static char *txt = "+";

	  if (PlusWindow == NULL) return;

	  GetClientRect (PlusWindow, &rect); 
      SelectObject (hdc, stdHfont);
      GetTextExtentPoint32 (hdc, txt, strlen (txt), &size); 
	  x = max (0, (rect.right  - rect.left - size.cx) / 2);
	  y = max (0, (rect.bottom - rect.top  - size.cy) / 2);
	  SetBkMode (hdc, TRANSPARENT);
      SetTextColor (hdc,MessCol);
      SetBkColor (hdc, MessBkCol);
	  TextOut (hdc, x, y, txt, strlen (txt));
}


void BESTPLIST::DestroyPlus (void)
/**
Fenster mit + loeschen.
**/
{
	  if (PlusWindow == NULL)
	  {
		  return;
	  }
	  DestroyWindow (PlusWindow);
	  PlusWindow = NULL;
}

void BESTPLIST::FillDM (double pr_ek, double pr_ek_bto)
/**
Felder fuer die verschiedenen Waehrungen fuellen. Kundenwaehrung ist DM
**/
{
	  double pr_ek_euro;
	  double pr_ek_euro_bto;
	  double kurs;
	

	  kurs = _mdn.konversion;
	  if (kurs == 0.0) kurs = 1.0;

      if (atoi (_mdn.waehr_prim) == 1)
	  {
/* Basiswaehrung ist DM                  */
	           pr_ek_euro     = pr_ek / kurs;
	           pr_ek_euro_bto = pr_ek_bto / kurs;
	  }
	  else if (atoi (_mdn.waehr_prim) == 2)
	  {
/* Basiswaehrung ist EURO                   */
	           pr_ek_euro     = pr_ek * kurs;
	           pr_ek_euro_bto = pr_ek_bto * kurs;
	  }
	  else
	  {
	           pr_ek_euro     = pr_ek;
	           pr_ek_euro_bto = pr_ek_bto;
	  }
      sprintf (bestps.pr_ek,          "%lf",    pr_ek);
      sprintf (bestps.pr_ek_bto,      "%lf",    pr_ek_bto);
      sprintf (bestps.pr_ek_dm,       "%lf",    pr_ek);
      sprintf (bestps.pr_ek_dm_bto,   "%lf",    pr_ek_bto);
      sprintf (bestps.pr_ek_euro,     "%lf",    pr_ek_euro);
      sprintf (bestps.pr_ek_euro_bto, "%lf",    pr_ek_euro_bto);
}

void BESTPLIST::FillEURO (double pr_ek, double pr_ek_bto)
/**
Felder fuer die verschiedenen Waehrungen fuellen. Kunden waehrung ist EURO
**/
{
	  double pr_ek_dm;
	  double pr_ek_dm_bto;
	  double kurs;
	
	  kurs = _mdn.konversion;
	  if (kurs == 0.0) kurs = 1.0;

	  if (atoi (_mdn.waehr_prim) == 1)
/* Basiswaehrung ist DM                  */
	  {
	          pr_ek_dm     = pr_ek * kurs;
	          pr_ek_dm_bto = pr_ek_bto * kurs;
	  }
	  else if (atoi (_mdn.waehr_prim) == 2)
	  {
/* Basiswaehrung ist EURO                   */
	           pr_ek_dm     = pr_ek * kurs;
	           pr_ek_dm_bto = pr_ek_bto * kurs;
	  }
      sprintf (bestps.pr_ek_dm,       "%lf",    pr_ek_dm);
      sprintf (bestps.pr_ek_dm_bto,   "%lf",    pr_ek_dm_bto);
      sprintf (bestps.pr_ek_euro,     "%lf",    pr_ek);
      sprintf (bestps.pr_ek_euro_bto, "%lf",    pr_ek_bto);
      sprintf (bestps.pr_ek,          "%lf",    pr_ek);
      sprintf (bestps.pr_ek_bto,      "%lf",    pr_ek_bto);
}

void BESTPLIST::FillFremd (double pr_ek, double pr_ek_bto)
/**
Felder fuer die verschiedenen Waehrungen fuellen. Kunden waehrung ist DM
**/
{
	  double pr_ek_dm;
	  double pr_ek_dm_bto;
	  double pr_ek_euro;
	  double pr_ek_euro_bto;
	  double kurs1;
	  double kurs2;
	  short  faktor;
	  int dsqlstatus;
	
	  kurs1 = _mdn.konversion;
	  if (kurs1 == 0.0) kurs1 = 1.0;

      DbClass.sqlin ((short *)   &best_kopf.mdn, 1, 0);
      DbClass.sqlin ((short *)   &best_kopf.waehrung, 1, 0);
	  DbClass.sqlout ((double *) &kurs2, 3, 0);
	  DbClass.sqlout ((short *)  &faktor, 1, 0);
	  dsqlstatus = DbClass.sqlcomm ("select kurs, faktor from devise "
		                            "where mdn = ? "
									"and devise_nr = ?");
	  if (dsqlstatus != 0) 
	  {
            if (atoi (_mdn.waehr_prim) == 1)
			{
				pr_ek_dm = pr_ek;
				pr_ek_dm = pr_ek_bto;
				pr_ek_euro     = pr_ek / kurs1;
				pr_ek_euro_bto = pr_ek_bto / kurs1;
			}
			else
			{
				pr_ek_dm = pr_ek * kurs1;
				pr_ek_dm = pr_ek_bto * kurs1;
				pr_ek_euro = pr_ek;
				pr_ek_euro = pr_ek_bto;
			}
	  }
	  else
	  {
		    if (atoi (_mdn.waehr_prim) == 1)
			{
	            pr_ek_dm       = pr_ek * kurs2 / faktor;
	            pr_ek_dm_bto   = pr_ek_bto * kurs2 / faktor;
				pr_ek_euro     = pr_ek_dm / kurs1;
				pr_ek_euro_bto = pr_ek_dm_bto / kurs1;
			}
			else
			{
                pr_ek_euro     = pr_ek * kurs2 / faktor; 
                pr_ek_euro_bto = pr_ek * kurs2 / faktor; 
				pr_ek_dm       = pr_ek_euro / kurs1;
				pr_ek_dm_bto   = pr_ek_euro_bto / kurs1;
			}
	  }
      sprintf (bestps.pr_ek_dm,        "%lf", pr_ek_dm);
      sprintf (bestps.pr_ek_dm_bto,    "%lf", pr_ek_dm_bto);
      sprintf (bestps.pr_ek_euro,      "%lf", pr_ek_euro);
      sprintf (bestps.pr_ek_euro_bto,  "%lf", pr_ek_euro_bto);
      sprintf (bestps.pr_ek_fremd,     "%lf", pr_ek);
      sprintf (bestps.pr_ek_fremd_bto, "%lf", pr_ek_bto);
      sprintf (bestps.pr_ek,           "%lf", pr_ek);
      sprintf (bestps.pr_ek_bto,       "%lf", pr_ek_bto);
}


void BESTPLIST::InitWaehrung (void)
{
	   sprintf (bestps.pr_ek_euro,      "%lf", 0.0);   
	   sprintf (bestps.pr_ek_euro_bto,  "%lf", 0.0);   
	   sprintf (bestps.pr_ek_fremd,     "%lf", 0.0);   
	   sprintf (bestps.pr_ek_fremd_bto, "%lf", 0.0);   
}

void BESTPLIST::FillWaehrung (double pr_ek, double pr_vk)
/**
Felder fuer die verschiedenen Waehrungen fuellen.
**/
{
	   InitWaehrung ();
	   switch (best_kopf.waehrung )
	   {
	         case 1 :
				 FillDM (pr_ek, pr_vk);
				 break;
	         case 2 :
				 FillEURO (pr_ek, pr_vk);
				 break;
            case 3 :
				 FillFremd (pr_ek, pr_vk);
				 break;
            default :
				 FillDM (pr_ek, pr_vk);
				 break;
	   }
}

void BESTPLIST::FillAktWaehrung ()
/**
Anzeigefelder fuer EK und VK fuellen.
**/
{
	   if (best_kopf.waehrung == 1 || best_kopf.waehrung == 0)
	   {
           sprintf (bestps.pr_ek,       "%lf",    best_pos.pr_ek);
           sprintf (bestps.pr_ek_bto,   "%lf",    best_pos.pr_ek_bto);
	   }
	   else if (best_kopf.waehrung == 2)
	   {
           sprintf (bestps.pr_ek,         "%lf",    best_pos.pr_ek_euro);
           sprintf (bestps.pr_ek_bto,     "%lf",    best_pos.pr_ek_euro_bto);
	   }
	   else if (best_kopf.waehrung == 3)
	   { 
           sprintf (bestps.pr_ek,        "%lf",    best_pos.pr_ek_fremd);
           sprintf (bestps.pr_ek_bto,    "%lf",    best_pos.pr_ek_fremd_bto);
	   }
	   else
	   {
           sprintf (bestps.pr_ek,       "%lf",    best_pos.pr_ek);
           sprintf (bestps.pr_ek_bto,   "%lf",    best_pos.pr_ek_bto);
	   }
}

void BESTPLIST::CalcPrNto (double pr_ek_bto0)
/**
Netto-Preis ausrechnen.
**/
{
	   double pr_ek0;

	   lief_bzg.pr_ek = pr_ek_bto0;
	   if (lief_bzg.pr_ek > 0.0)
	   {
              pr_ek0  = pr_ek_bto0 - (double) WePreis.GetRabEk 
				                                      (best_pos.mdn, best_pos.fil, 
			                                           best_kopf.lief, 
	  						                           1, pr_ek_bto0, _a_bas.me_einh,_a_bas.a,_a_bas.ag,_a_bas.wg);
/*
			  char lieferdat [12];
			  dlong_to_asc (best_kopf.lief_term, lieferdat);

		      int dsqlstatus = WePreis.preis_holen (best_kopf.mdn,
                                          best_kopf.fil,
                                          best_kopf.lief,
										  lieferdat,
                                          ratod (bestps.a));
*/
	   }
	   else
	   {
			  pr_ek0 = (double) 0.0;
	   }
       if ((NoArtMess == FALSE) && (pr_ek0 == (double) 0.0))
       {

               if (preis0_mess != 0)
			   {
                      disp_mess ("Achtung !!\nPreis 0 gelesen", 2);
			   }
       }

       sprintf (bestps.pr_ek,     "%lf",  pr_ek0);
       sprintf (bestps.pr_ek_bto, "%lf", pr_ek_bto0);
	   pr_ek = pr_ek0;
	   FillWaehrung (pr_ek0, pr_ek_bto0);
}

int BESTPLIST::testekbto ()
{
	   if (pr_ek_bto1 != ratod(bestps.pr_ek_bto))
	   {
		   if (atoi(bestps.fix_kz_sint) == 0)
		   {
				if (abfragejn (eListe.Getmamain3 (), 
					"Preis auf Fix setzen, OK?", "J") == 1)
				{
					strcpy(bestps.fix_kz_sint,"1");
				}
		   }
		   else
		   {
				if (abfragejn (eListe.Getmamain3 (), 
					"Preis wieder auf variabel, OK?", "J") == 1)
				{
					strcpy(bestps.fix_kz_sint,"0");
				}
		   }
	   }
	   else
	   {
//		   strcpy(bestps.fix_kz_sint,"0");
	   }
	   CalcPrNto (ratod (bestps.pr_ek_bto));
       memcpy (&bestptab[eListe.GetAktRow()], 
                            &bestps, sizeof (struct AUFPS));
       eListe.ShowAktRow ();
       TestFixPr ();
       return 0;
}

double BESTPLIST::PrAktionChoise (void)
{
       AktionDlg *aktionDlg;

       aktionDlg = new AktionDlg (-1, -1, 40, 8, "Preisauswahl", 105, FALSE);
       aktionDlg->SetWinBackground (GetSysColor (COLOR_3DFACE));
       aktionDlg->SetStyle (WS_VISIBLE | WS_POPUP | WS_CAPTION | WS_DLGFRAME | WS_SYSMENU );
       aktionDlg->SetPreise (lief_bzg.pr_ek_eur, lief_bzg.pr_ek_sa_eur);
       HWND ChWindow = aktionDlg->OpenScrollWindow (hMainInst, hMainWin);
	   aktionDlg->ProcessMessages ();
       double pr_ek = aktionDlg->GetPreis ();
       aktionDlg->DestroyWindow ();
       delete aktionDlg;
       return pr_ek;
}


void BESTPLIST::ReadPr (void)
/**
Artikelpreis holen.
**/
{
       char lieferdat [12];
       int dsqlstatus;
       short sa;
	   double pr_ek0;
	   double pr_ek_bto0;
	   double pr_ek0_euro;
	   double pr_ek_bto0_euro;
       double lastek, lastek_euro;
       LbEk lbEk;
       
	   sa = 0;
       dlong_to_asc (best_kopf.best_term, lieferdat);

       if ((GetFieldAttr ("a_kun") & REMOVED) == 0 && strcmp (bestps.lief_best, " ") > 0)
	   {

	        dsqlstatus = WePreis.preis_holen (best_kopf.mdn,
		                                      best_kopf.fil,
			                                  best_kopf.lief,
											  lieferdat,
					                          bestps.lief_best);
	   }
	   else
	   {

			dsqlstatus = WePreis.preis_holen (best_kopf.mdn,
                                          best_kopf.fil,
                                          best_kopf.lief,
										  lieferdat,
                                          ratod (bestps.a));
	   }


        if (DbClass.IsDoublenull (_a_bas.inh_ek)) _a_bas.inh_ek = 1;
        if (_a_bas.inh_ek == 0) _a_bas.inh_ek = 1;

		lief_bzg.pr_ek_sa /= _a_bas.inh_ek;
		lief_bzg.pr_ek /= _a_bas.inh_ek;
		lief_bzg.pr_ek_sa_eur /= _a_bas.inh_ek;
		lief_bzg.pr_ek_eur /= _a_bas.inh_ek;


       if (lief_bzg.pr_ek_sa > 0.0)
       {
	           pr_ek_bto0      = lief_bzg.pr_ek_sa; 
       }
       else
       {
	           pr_ek_bto0      = lief_bzg.pr_ek; 
       }

       if (lief_bzg.pr_ek_sa_eur > 0.0)
       {
               if (LstBestEk)
               {
           	           pr_ek_bto0_euro = lief_bzg.pr_ek_sa_eur; 
               }
               else
               {
	                   pr_ek_bto0_euro = PrAktionChoise ();
               }
               if (pr_ek_bto0_euro == lief_bzg.pr_ek_sa_eur)
               {
 	                      pr_ek_bto0 = lief_bzg.pr_ek_sa; 
               }
               else
               {
	                      pr_ek_bto0      = lief_bzg.pr_ek; 
               }
       }
       else
       {
	           pr_ek_bto0_euro = lief_bzg.pr_ek_eur; 
               pr_ek_bto0      = lief_bzg.pr_ek; 
       }

       if (LstBestEk)
       {
              lastek_euro = lbEk.LastEk (best_kopf.mdn, best_kopf.fil, best_kopf.lief, 
                                        ratod (bestps.a),
                                        &lastek_euro,
                                        &lastek);
              if (lastek_euro != 0.0)
              {
                  pr_ek_bto0_euro    = lastek_euro;
                  lief_bzg.pr_ek_eur = lastek_euro;
              }
              if (lastek != 0.0)
              {
                  pr_ek_bto0     = lastek;
                  lief_bzg.pr_ek = lastek;
              }
       }

	   if (lief_bzg.pr_ek > 0.0)
	   {
              pr_ek0  = pr_ek_bto0 - (double) WePreis.GetRabEk 
				                                      (best_pos.mdn, best_pos.fil, 
			                                           best_kopf.lief, 
	  						                           1, pr_ek_bto0, _a_bas.me_einh,_a_bas.a,_a_bas.ag,_a_bas.wg);
	   }
	   else
	   {
			  pr_ek0 = (double) 0.0;
	   }
	   if (lief_bzg.pr_ek_eur > 0.0)
	   {
              pr_ek0_euro  = pr_ek_bto0_euro - (double) WePreis.GetRabEk 
				                                      (best_pos.mdn, best_pos.fil, 
			                                           best_kopf.lief, 
	  						                           1, pr_ek_bto0_euro, _a_bas.me_einh,_a_bas.a,_a_bas.ag,_a_bas.wg);
	   }
	   else
	   {
			  pr_ek0 = (double) 0.0;
	   }


       if ((NoArtMess == FALSE) && (pr_ek0 == (double) 0.0))
       {

               if (preis0_mess != 0)
			   {
                      disp_mess ("Achtung !!\nPreis 0 gelesen", 2);
			   }
       }

       if (dsqlstatus == 0)
       {
                 sprintf (bestps.pr_ek_dm,     "%lf",  pr_ek0);
                 sprintf (bestps.pr_ek_bto_dm, "%lf", pr_ek_bto0);
                 sprintf (bestps.pr_ek_euro,     "%lf",  pr_ek0_euro);
                 sprintf (bestps.pr_ek_bto_euro, "%lf", pr_ek_bto0_euro);
				 sprintf (bestps.sa_kz_sint, "%1d", sa);
                 strcpy  (bestps.me_kz, lief_bzg.me_kz); 
                 strcpy  (bestps.lief_best, lief_bzg.lief_best);
                 bestps.min_best = lief_bzg.min_best; 
       }

	   pr_ek = pr_ek0;
       if (best_kopf.waehrung == 2)
       {
                 sprintf (bestps.pr_ek,     "%lf",  pr_ek0_euro);
                 sprintf (bestps.pr_ek_bto, "%lf", pr_ek_bto0_euro);
	             FillWaehrung (pr_ek0_euro, pr_ek_bto0_euro);
       }
       else
       {
                 sprintf (bestps.pr_ek,     "%lf",  pr_ek0);
                 sprintf (bestps.pr_ek_bto, "%lf", pr_ek_bto0);
	             FillWaehrung (pr_ek0, pr_ek_bto0);
       }
	   if (sa)
	   {
		   CreateSa ();
	   }
	   else
	   {
		   DestroySa ();
	   }
}


void BESTPLIST::ReadAktMeEinh (int me_einh_lief, char* lief_best)
/**
Mengeneinheiten holen.
**/
{
        int dsqlstatus;
        char ptwert [5];

        KEINHEIT keinheit;

        einh_class.AktBestEinh (best_kopf.mdn, best_kopf.fil,
                               best_kopf.lief, _a_bas.a,lief_best, me_einh_lief);
        einh_class.GetLiefEinh (best_kopf.mdn, best_kopf.fil, best_kopf.lief,
                               _a_bas.a,lief_best, &keinheit);
        strcpy (bestps.me_bz, keinheit.me_einh_lief_bez);
        sprintf (bestps.me_einh_lief, "%hd", keinheit.me_einh_lief);
//        sprintf (bestps.me_einh,     "%hd", keinheit.me_einh_bas);
        sprintf (bestps.me_einh,     "%hd", keinheit.me_einh_lief);
        inh = keinheit.inh;
        pr_ek_einh = keinheit.pr_ek;
/*
        if (atoi (bestps.me_kz) == BESTELLPREIS)
        {
                 inh = keinheit.inh / bestps.min_best;
        }
        else if (atoi (bestps.me_kz) == VERKAUFSPREIS)
        {
                 inh = keinheit.inh;
        }
*/

        bestps.inh = inh;
		//130509
        sprintf (ptwert, "%hd", _a_bas.me_einh);
        if (ptab_class.lese_ptab ("me_einh", ptwert) == 0)
        {
            strcpy (bestps.basis_me_bz, ptabn.ptbezk);
        }
        return;


        switch (_a_bas.a_typ)
        {
             case HNDW :
                     dsqlstatus = HndwClass.lese_a_hndw (_a_bas.a);
                     break;
             case EIG :
                     dsqlstatus = HndwClass.lese_a_eig (_a_bas.a);
                     a_hndw.me_einh_kun = a_eig.me_einh_ek;
                     break;
             case EIG_DIV :
                     dsqlstatus = HndwClass.lese_a_eig_div (_a_bas.a);
                     a_hndw.me_einh_kun = a_eig_div.me_einh_ek;
                     break;
        }

        sprintf (ptwert, "%hd", _a_bas.me_einh);
        if (ptab_class.lese_ptab ("me_einh", ptwert) == 0)
        {
            strcpy (bestps.basis_me_bz, ptabn.ptbezk);
        }

        if (dsqlstatus) return;

        sprintf (ptwert, "%hd", a_hndw.me_einh_kun);
        if (ptab_class.lese_ptab ("me_einh", ptwert) == 0)
        {
            strcpy (bestps.me_bz, ptabn.ptbezk);
        }
}


void BESTPLIST::ReadMeEinh (char* lief_best)
/**
Mengeneinheiten holen.
**/
{
        int dsqlstatus;
        char ptwert [5];

        KEINHEIT keinheit;

        einh_class.GetLiefEinh (best_kopf.mdn, best_kopf.fil, best_kopf.lief,
                               _a_bas.a,lief_best, &keinheit);
        strcpy (bestps.me_bz, keinheit.me_einh_lief_bez);
        sprintf (bestps.me_einh_lief, "%hd", keinheit.me_einh_lief);
//        sprintf (bestps.me_einh,     "%hd", keinheit.me_einh_bas);
        sprintf (bestps.me_einh,     "%hd", keinheit.me_einh_lief);
        inh = keinheit.inh;
        pr_ek_einh = keinheit.pr_ek;
/*
        if (atoi (bestps.me_kz) == BESTELLPREIS)
        {
                 inh = keinheit.inh / bestps.min_best;
        }
        else if (atoi (bestps.me_kz) == VERKAUFSPREIS)
        {
                 inh = keinheit.inh;
        }
*/

        bestps.inh = inh;
		//130509
        sprintf (ptwert, "%hd", _a_bas.me_einh);
        if (ptab_class.lese_ptab ("me_einh", ptwert) == 0)
        {
            strcpy (bestps.basis_me_bz, ptabn.ptbezk);
        }


        return;


        switch (_a_bas.a_typ)
        {
             case HNDW :
                     dsqlstatus = HndwClass.lese_a_hndw (_a_bas.a);
                     break;
             case EIG :
                     dsqlstatus = HndwClass.lese_a_eig (_a_bas.a);
                     a_hndw.me_einh_kun = a_eig.me_einh_ek;
                     break;
             case EIG_DIV :
                     dsqlstatus = HndwClass.lese_a_eig_div (_a_bas.a);
                     a_hndw.me_einh_kun = a_eig_div.me_einh_ek;
                     break;
        }

        sprintf (ptwert, "%hd", _a_bas.me_einh);
        if (ptab_class.lese_ptab ("me_einh", ptwert) == 0)
        {
            strcpy (bestps.basis_me_bz, ptabn.ptbezk);
        }

        if (dsqlstatus) return;

        sprintf (ptwert, "%hd", a_hndw.me_einh_kun);
        if (ptab_class.lese_ptab ("me_einh", ptwert) == 0)
        {
            strcpy (bestps.me_bz, ptabn.ptbezk);
        }
}


void BESTPLIST::rechne_liefme (char* lief_best)
/**
Basismenge der aktuellen Bestellmenge anzeigen.
**/
{
	double best_me;
	double a;
	short me_einh_lief;
	double best_me_vgl;
    KEINHEIT keinheit;


	best_me = ratod (bestps.auf_me);
	a  = ratod (bestps.a);
	me_einh_lief = atoi (bestps.me_einh_lief);

    einh_class.AktBestEinh (best_kopf.mdn, best_kopf.fil,
                                best_kopf.lief, a,lief_best, me_einh_lief);
    einh_class.GetLiefEinh (best_kopf.mdn, best_kopf.fil,
                              best_kopf.lief, a,lief_best, &keinheit);
          
    if (keinheit.me_einh_lief == keinheit.me_einh_bas)
    {
            best_me_vgl = best_me;
    }
    else
    {
            if (keinheit.inh <= (double) 0.0)
            {
                     keinheit.inh = (double) 1.0;
            }
            best_me_vgl = best_me * keinheit.inh;
    }

	sprintf (bestps.lief_me, "%8.2lf", best_me_vgl);
}



int BESTPLIST::testme (void)
/**
Auftragsmeneg pruefen.
**/
{
        KEINHEIT keinheit;
        int me_einh;

        me_einh = atoi (bestps.me_einh);
        if (ratod (bestps.a) == (double) 0.0) return 0;

		if (add && aufme_old != (double) 0.0)
		{
			best_pos.me = ratod (bestps.auf_me);
			sprintf (bestps.auf_me, "%.3lf", best_pos.me + aufme_old);
            memcpy (&bestptab[eListe.GetAktRow()], 
                            &bestps, sizeof (struct AUFPS));
			eListe.ShowAktRow ();
		}

        if (ratod (bestps.auf_me) == (double) 0.0 && syskey == KEYCR)
        {
            einh_class.NextBestEinh (best_kopf.mdn, best_kopf.fil, best_kopf.lief,
                                     ratod (bestps.a),bestps.lief_best, 
                                     atoi (bestps.me_einh_lief), &keinheit);
            strcpy (bestps.basis_me_bz, keinheit.me_einh_bas_bez);
            strcpy (bestps.me_bz, keinheit.me_einh_lief_bez);
            sprintf (bestps.me_einh_lief, "%hd", keinheit.me_einh_lief);
            inh = keinheit.inh;

//ToDo Preise
            ReadMeEinh (bestps.lief_best); 
            if ((atoi (bestps.me_einh) != me_einh) && PrInBestEinh)
            {
                  sprintf (bestps.pr_ek_bto, "%.4lf", (double) ratod (bestps.pr_ek_bto) / 
                                                                   bestps.min_best);
                  sprintf (bestps.pr_ek_bto, "%.4lf", (double) ratod (bestps.pr_ek_bto) * 
                                                                   bestps.inh);

                  if (pr_ek_einh != 0.0)
                  {
                          sprintf (bestps.pr_ek_bto, "%.4lf", pr_ek_einh);
                  }

	              CalcPrNto (ratod (bestps.pr_ek_bto));
                  bestps.min_best = bestps.inh;
                  if (bestps.min_best == 0.0)
                  {
                          bestps.min_best = lief_bzg.min_best;
                  }
            }
			sprintf (bestps.pos_wrt, "%.2lf", 0.0);

            memcpy (&bestptab[eListe.GetAktRow()], 
                            &bestps, sizeof (struct AUFPS));
            eListe.ShowAktRow ();
            eListe.SetPos (eListe.GetAktRow (), 
                              eListe.GetAktColumn ());

  	        add = FALSE;
			DestroyPlus ();
            return -1;
        }

        if (ratod (bestps.auf_me) > MAXME)
        {
            print_mess (2, "Die Bestellmenge ist zu gross");
            sprintf (bestps.auf_me, "%.3lf", (double) 0.0);
			sprintf (bestps.pos_wrt, "%.2lf", 0.0);
            memcpy (&bestptab[eListe.GetAktRow()], 
                            &bestps, sizeof (struct AUFPS));
            eListe.ShowAktRow ();
            eListe.SetPos (eListe.GetAktRow (), 
                              eListe.GetAktColumn ());
  	        add = FALSE;
			DestroyPlus ();
            return -1;
        }


		rechne_liefme (bestps.lief_best);
		double pos_wrt = 0.0;
		pos_wrt = ratod (bestps.auf_me) * ratod (bestps.pr_ek);
		if (bestps.me_kz[0] == '1' && !PrInBestEinh)
		{
			pos_wrt *= bestps.inh;
		}
		sprintf (bestps.pos_wrt, "%.2lf", pos_wrt);
        memcpy (&bestptab[eListe.GetAktRow()], 
                            &bestps, sizeof (struct AUFPS));
        eListe.ShowAktRow ();
        AnzBestWert ();
		aufme_old = (double) 0.0;
	    add = FALSE;
		DestroyPlus ();
        return 0;

}

int BESTPLIST::TestPrproz_diff (void)
/**
Test, ob die Preisaenderung ueber prproz_diff % ist.
**/
{
	    double oldek;
	    double diff;
		double diffproz;
		char buffer [256];

		if (pr_ek_bto == (double) 0.0) return 1;
		oldek = ratod (bestps.pr_ek_bto);
		diff = oldek - pr_ek_bto;
		if (diff < 0) diff *= -1;
        diffproz = 100 * diff / pr_ek_bto;  
		if (diffproz > prproz_diff)
		{
//			print_mess (2, "Achtung !! Preis�nderung �ber %.2lf %c", prproz_diff, '%');
			sprintf (buffer, "Achtung !! Preis�nderung �ber %.2lf %c.\n"
				             "�nderung OK ?", prproz_diff, '%');
            if (abfragejn (eListe.Getmamain3 (), 
					       buffer , "N") == 0)
			{
				sprintf (bestps.pr_ek, "%lf", pr_ek);
                memcpy (&bestptab[eListe.GetAktRow()], 
                            &bestps, sizeof (struct AUFPS));
                eListe.ShowAktRow ();
                eListe.SetPos (eListe.GetAktRow (), 
                              eListe.GetAktColumn ());

		 	    return 0;
			}
		}
		return 1;
}


int BESTPLIST::testpr (void)
/**
Artikel holen.
**/
{
	    char buffer [256];

	    if (preistest == 1 && pr_ek_bto)
		{
			if (ratod (bestps.pr_ek_bto) != pr_ek_bto)
			{
			        disp_mess ("Achtung !! Der Preis wurde ge�ndert", 2);
			}
			return 0;
		}
		else if (preistest == 4)
		{
			if (TestPrproz_diff () == 0)
			{
               eListe.SetPos (eListe.GetAktRow (), 
                              eListe.GetAktColumn ());
               return (-1);
            }
		}
		else if (preistest == 2 && pr_ek_bto)
		{
			sprintf (bestps.pr_ek_bto, "%lf", pr_ek_bto);
		}

        if (ratod (bestps.a) == (double) 0.0) return 0;
        if (ratod (bestps.pr_ek_bto) == (double) 0.0)
        {
            if (best_me_pr_0)
            {
               if ((eListe.IsAppend ()) && (preis0_mess == 1))
			   {
			          sprintf (buffer, "Achtung !! Preis ist 0\n"
				                       "            OK ?");
                      if (abfragejn (eListe.Getmamain3 (), 
					       buffer , "N") == 0)
					  {
                              eListe.SetPos (eListe.GetAktRow (), 
                                             eListe.GetAktColumn ());
							  return -1;
					  }
			  }
              return 0;
            }
            else
            {
               disp_mess ("Der Preis darf nicht 0 sein", 2);
               eListe.SetPos (eListe.GetAktRow (), 
                              eListe.GetAktColumn ());
            }
            return (-1);
        }
        if (ratod (bestps.pr_ek_bto) > MAXPR)
        {
            print_mess (2, "Der Preis ist zu gross");
            sprintf (bestps.pr_ek_bto, "%lf", (double) 0.0);
            memcpy (&bestptab[eListe.GetAktRow()], 
                            &bestps, sizeof (struct AUFPS));
            eListe.ShowAktRow ();
            eListe.SetPos (eListe.GetAktRow (), 
                              eListe.GetAktColumn ());
            return -1;
        }
        memcpy (&bestptab[eListe.GetAktRow()], 
                            &bestps, sizeof (struct AUFPS));
        AnzBestWert ();

        return 0;
}

void BESTPLIST::EanGew (char *eans, BOOL eangew)
/**
Gewicht aus EAN-Nr holen oder Defaultwert.
**/
{
	   char gews [6];
	   double gew;

	   if (eangew)
	   {
		   memcpy (gews, &eans[7], 5);
		   gews [5] = (char) 0;
		   gew = ratod (gews) / 1000;
		   sprintf (bestps.auf_me, "%.3lf", gew);
	   }
	   else if (auf_me_default)
	   {
		   sprintf (bestps.auf_me, "%d", auf_me_default);
	   }
}
       

int BESTPLIST::ReadEan (double ean)
{
	   double a;
	   char eans [14];
	   int dsqlstatus;
	   char PLU [7];
	   long a_krz;
	   BOOL eangew;

       DbClass.sqlin ((double *) &ean,    3, 0);
       DbClass.sqlout ((double *)  &a, 3, 0);
       dsqlstatus = DbClass.sqlcomm ("select a from a_ean "
                                     "where ean = ?");
	   if (dsqlstatus == 0)
	   {
		   sprintf (bestps.a, "%.0lf", a);
           dsqlstatus = lese_a_bas (ratod (bestps.a));
		   return dsqlstatus;
	   }
	   sprintf (eans, "%.0lf", a);
	   if (strlen (eans) < 13) 
	   {
		   return dsqlstatus;
	   }

	   eangew = FALSE;
	   if (memcmp (eans, "20", 2) == 0);
	   else if (memcmp (eans, "21", 2) == 0);
	   else if (memcmp (eans, "22", 2) == 0);
	   else if (memcmp (eans, "23", 2) == 0);
	   else if (memcmp (eans, "24", 2) == 0);
	   else if (memcmp (eans, "27", 2) == 0)
	   {
		   eangew = TRUE;
	   }
	   else if (memcmp (eans, "28", 2) == 0)
	   {
		   eangew = TRUE;
	   }
	   else if (memcmp (eans, "29", 2) == 0)
	   {
		   eangew = TRUE;
	   }
	   else
	   {
		   return dsqlstatus;
	   }
	   memcpy (PLU, &eans[2], plu_size);    
	   PLU[plu_size] = (char) 0;
	   a_krz = atol (PLU);
       DbClass.sqlin ((long *) &a_krz, 2, 0);
       DbClass.sqlout ((double *)  &a, 3, 0);
       dsqlstatus = DbClass.sqlcomm ("select a from a_krz "
                                     "where a_krz = ?");
	   if (dsqlstatus == 0)
	   {
		   sprintf (bestps.a, "%.0lf", a);
           dsqlstatus = lese_a_bas (ratod (bestps.a));
		   EanGew (eans, eangew);
		   return dsqlstatus;
	   }
	   a = (double) a_krz;

       DbClass.sqlin ((double *) &a,    3, 0);
       DbClass.sqlout ((double *)  &a, 3, 0);
       dsqlstatus = DbClass.sqlcomm ("select a from a_bas "
                                     "where a = ?");
	   if (dsqlstatus == 0)
	   {
		   sprintf (bestps.a, "%.0lf", a);
           dsqlstatus = lese_a_bas (ratod (bestps.a));
		   EanGew (eans, eangew);
	   }
	   return dsqlstatus;
}

BOOL BESTPLIST::EnterLiefBzgA ()
{
	   if (ratod (bestps.a) == 0.0)
	   {
		   return FALSE;
	   }
       if (CreateLiefBzg () != -1)
	   {
		   ReadPr ();
           memcpy (&bestptab[eListe.GetAktRow ()], &bestps, 
 							      sizeof (struct AUFPS));
           eListe.ShowAktRow ();
	   }
	   return TRUE;
}

BOOL BESTPLIST::ReReadPr ()
{
	   if (ratod (bestps.a) == 0.0)
	   {
		   return FALSE;
	   }

	   ReadPr ();
	   testme ();
       memcpy (&bestptab[eListe.GetAktRow ()], &bestps, 
					      sizeof (struct AUFPS));
       eListe.ShowAktRow ();

	   return TRUE;
}


int BESTPLIST::CreateLiefBzg (void)
{
       LiefBzgDlg *Dlg;

       lief_bzg.mdn = best_kopf.mdn;
       lief_bzg.fil = best_kopf.fil;
       strcpy (lief_bzg.lief, best_kopf.lief);
       DLG::hInstance = hMainInst;
       Dlg = new LiefBzgDlg (-1, -1, 89, 16, "Lieferanten-Bezugsquellen",105, FALSE,
                             RAISEDBORDER, ENTERADATA);
//       Dlg = new LiefBzgDlg (-1, -1, 89, 25, "Lieferanten-Bezugsquellen", 116, FALSE,
//                             HIGHCOLBORDER, ENTERHEAD);
       Dlg->SetStyle (WS_VISIBLE | WS_POPUP | WS_THICKFRAME | WS_CAPTION | WS_SYSMENU);
       Dlg->SetStyleEx (NULL);
       Dlg->SetWinBackground (LTGRAYCOL);
//       Dlg->SetWinBackground (RGB (0,128,192));
       EnableWindows (eListe.Getmamain3 (), FALSE);
       EnableWindows (hMainWin, FALSE);
       Dlg->OpenScrollWindow (hMainInst, GetParent (hMainWin));
	   Dlg->ProcessMessages ();
       delete Dlg;
       EnableWindows (hMainWin, TRUE);
       EnableWindows (eListe.Getmamain3 (), TRUE);
       if (syskey == KEY5)
       {
                 return -1;
       }
       return 0; 
}

int BESTPLIST::TestLief_bzg (void)
{
     lief_bzg.a = ratod (bestps.a);
	 strcpy(lief_bzg.lief_best,bestps.lief_best);
     return TestLief_bzg (lief_bzg.a,lief_bzg.lief_best);
}

int BESTPLIST::TestLief_bzg (double a, char *lief_best)
/**
Test, ob fuer den Lieferanten ein Eintrag in lief_bzg existiert.
**/
{
	char ca [30];
	char clief_best [17];

	   int dsqlstatus;
       LIEF_BZG_CLASS lief_bzg_class;

       lief_bzg.mdn = best_kopf.mdn;
       lief_bzg.fil = best_kopf.fil;
       strcpy (lief_bzg.lief, best_kopf.lief);
       strcpy (lief_bzg.lief_best, lief_best);
       lief_bzg.a = a;
       dsqlstatus = lief_bzg_class.dbreadfirst ();
       while (dsqlstatus == 100)
       {
                  if (lief_bzg.fil > 0)
                  {
                      lief_bzg.fil = 0;
                  }
                  else if (lief_bzg.mdn > 0)
                  {
                      lief_bzg.mdn = 0;
                  }
                  else
                  {
                      break;
                  }
                  dsqlstatus = lief_bzg_class.dbreadfirst ();
       }
       if (dsqlstatus == 0) return 0; 


				   strcpy (ca,bestps.a);    //ToDO Nur eine Kr�cke , Nach der MessageBox verliert bestps die akt. Werte !!!??
				   strcpy (clief_best,bestps.lief_best);
                   if (abfragejn (NULL,
                       "Der Artikel ist nicht im Sortiment des Lieferanten\n"
                       "Artikel anlegen ?", "J"))
                   {

                       if (CreateLiefBzg () == 0)
                       {
						   strcpy (bestps.a,ca);    //ToDO Nur eine Kr�cke , Nach der MessageBox verliert bestps die akt. Werte !!!??
						   strcpy (bestps.lief_best,lief_bzg.lief_best);
                           return TestLief_bzg (lief_bzg.a,bestps.lief_best);
                       }
                       return -1;
                   }
       return -1;
}

/********************
int BESTPLIST::TestLief_bzg (void)
{
     lief_bzg.a = ratod (bestps.a);
     return TestLief_bzg (lief_bzg.a);
}

int BESTPLIST::TestLief_bzg (double a)
{
	char ca [30];
	char cpos [20];

	   int dsqlstatus;
       LIEF_BZG_CLASS lief_bzg_class;

       lief_bzg.mdn = best_kopf.mdn;
       lief_bzg.fil = best_kopf.fil;
       strcpy (lief_bzg.lief, best_kopf.lief);
       lief_bzg.a = a;
       dsqlstatus = lief_bzg_class.dbreadfirst ();
       while (dsqlstatus == 100)
       {
                  if (lief_bzg.fil > 0)
                  {
                      lief_bzg.fil = 0;
                  }
                  else if (lief_bzg.mdn > 0)
                  {
                      lief_bzg.mdn = 0;
                  }
                  else
                  {
                      break;
                  }
                  dsqlstatus = lief_bzg_class.dbreadfirst ();
       }
       if (dsqlstatus == 0) return 0; 

	   if (NoArtMess == FALSE)
	   {
                   if (we_neu_a_par == 0)
                   {
                       disp_mess ("Der Artikel ist nicht im Sortiment des Lieferanten.", 2);
                       return -1;
                   }

				   strcpy (ca,bestps.a);    // Nur eine Kr�cke , Nach der MessageBox verliert bestps die akt. Werte !!!??
				   strcpy (cpos,bestps.posi);
                   if (abfragejn (eListe.Getmamain3 (),
                       "Der Artikel ist nicht im Sortiment des Lieferanten\n"
                       "Artikel anlegen ?", "J"))
                   {

                       if (CreateLiefBzg () == 0)
                       {
						   strcpy (bestps.a,ca);    // Nur eine Kr�cke , Nach der MessageBox verliert bestps die akt. Werte !!!??
						   strcpy (bestps.posi,cpos);
                           return TestLief_bzg (lief_bzg.a);
                       }
                       return -1;
                   }

//              	   print_mess (2, "Der Artikel ist nicht im Sortiment des Lieferanten");
	   }

       return -1;

}

  ********************/

int BESTPLIST::TestKontrakt (double a)
{
	   if (best_kopf.kontrakt_nr == 0l) return 0;

	   DbClass.sqlin ((short *) &best_kopf.mdn, 1, 0);
	   DbClass.sqlin ((short *) &best_kopf.fil, 1, 0);
	   DbClass.sqlin ((char *) best_kopf.lief, 0, 17);
	   DbClass.sqlin ((long *) &best_kopf.kontrakt_nr, 2, 0);
	   DbClass.sqlin ((double *) &a, 3, 0);
	   int dsqlstatus = DbClass.sqlcomm ("select a from best_pos "
		                                 "where mdn = ? " 
						                 "and fil = ? "
						                 "and lief = ? "
						                 "and best_blg = ? "
						                 "and a = ?");
       if (dsqlstatus == 100)
	   {
		   print_mess (2, "Artikel %.0lf ist nicht im Kontrakt %ld",
			              a, best_kopf.kontrakt_nr);
		   return -1;
	   }
	   return 0;
}

void BESTPLIST::GetLastMeBest (double a)
/**
Letzte Bestellmenge des Lieferanten holen.
**/
{
	   int cursork;
	   int cursorp;
	   long best_blg;
	   double auf_me;
	   double auf_me_ges;
	   char bdat[12];

	   if (DelLastMe) return;

	   DbClass.sqlin ((short *) &best_kopf.mdn, 1, 0);
	   DbClass.sqlin ((short *) &best_kopf.fil, 1, 0);
	   DbClass.sqlin ((long *)  &best_kopf.lief, 0, 17);
	   DbClass.sqlout ((long *) &best_blg, 2, 0);
	   DbClass.sqlout ((char *) bdat, 0, 11);
	   cursork = DbClass.sqlcursor ("select best_blg, best_term from best_kopf where mdn = ? "
		                                                  "and fil = ? "
														  "and lief = ? "
														  "order by best_term desc, "
														  "best_blg desc");

	   DbClass.sqlin ((short *) &best_kopf.mdn, 1, 0);
	   DbClass.sqlin ((short *) &best_kopf.fil, 1, 0);
	   DbClass.sqlin ((long *)  &best_blg, 2, 0);
	   DbClass.sqlin ((double *)  &a, 3, 0);
	   DbClass.sqlout ((double *) &auf_me, 3, 0);
	   cursorp = DbClass.sqlcursor ("select me from best_pos "
		                            "where mdn = ? "
									"and fil = ? "
									"and best_blg = ? "
									"and a = ?");
	   auf_me_ges = (double) 0.0;

       while (DbClass.sqlfetch (cursork) == 0)
	   {
		   if (DbClass.sqlopen (cursorp)) break;
		   while (DbClass.sqlfetch (cursorp) == 0)
		   {
			   auf_me_ges += auf_me;
		   }
		   if (auf_me_ges != (double) 0.0) break;
	   }
	   DbClass.sqlclose (cursorp);
	   DbClass.sqlclose (cursork);
	   sprintf (bestps.last_me, "%.3lf", auf_me_ges);
}


void BESTPLIST::GetLastMe (double a)
/**
Letzte Bestellmenge des Lieferanten holen.
**/
{
	   return;
/*
	   long auf;
	   double auf_me_ges;

	   if (DelLastMe) return;

	   auf_me_ges = 0;
	   aufkun.auf_me = (double) 0.0;
	   aufkun.mdn     = angk.mdn;
	   aufkun.fil     = angk.fil;
	   aufkun.kun     = angk.kun;
	   aufkun.a       = a;
	   aufkun.kun_fil = angk.kun_fil;
	   AufKun.dbreadlast ();
	   auf = aufkun.auf;
	   auf_me_ges = aufkun.auf_me;
	   while (AufKun.dbreadnextlast () == 0)
	   {
		   if (auf != aufkun.auf) break;
		   auf_me_ges += aufkun.auf_me;
	   }
	   if (auf_me_ges == (double) 0.0)
	   {
		   GetLastMeAuf (a);
	   }
	   else
	   {
           sprintf (bestps.last_me, "%.3lf", auf_me_ges);
	   }
*/
}


int BESTPLIST::TestNewArt (double a)
/**
Test, ob der Artikel bei diesem Lieferschein existiert.
**/
{
	     int i;
		 int angpanz;
		 int akt_pos;
		 
		 akt_pos = eListe.GetAktRow ();
         angpanz = eListe.GetRecanz ();
	     for (i = 0; i < angpanz; i ++)
		 {
			 if (i == akt_pos) continue;
			 /* CompareMode ist irrelevant, es muss immer beides verglichen werden !!! //LuD 29.02.2012
			 if (CompareMode == Artikel)
			 {
				if (a == ratod (bestptab[i].a)) break;
			 }
			 else if (CompareMode == LiefBest)
			 {
				if (a == ratod (bestptab[i].a) &&
					 strcmp (bestps.lief_best,bestptab[i].lief_best) == 0) break;
			 }
			 ****/
			if (a == ratod (bestptab[i].a) &&
				 strcmp (bestps.lief_best,bestptab[i].lief_best) == 0) break;
		 }
		 if (i == angpanz) return -1;
		 return (i);
}


int BESTPLIST::fetchaDirect (int lrow)
/**
Artikel holen.
**/
{

       int dsqlstatus;
       char wert [5];
       long posi;
       int i;


	   clipped (bestps.a);
	   sprintf (bestps.a, "%13.0lf", ratod (bestps.a));

       einh_class.SetBestEinh (1);
       sprintf (bestps.best_txt, "%ld", 0l);
       SetFkt (9, leer, NULL);
       set_fkt (NULL, 10);
       SetFkt (10, leer, NULL);
       posi = atol (bestps.posi);
       if (posi == 0)
       {
             i = lrow;
             if (i == 0)
             {
                 posi = 1;
             }
             else
             {
                 posi = atol (bestptab[i - 1].posi);
                 posi ++;
             }
             if (PosiEnd (posi) == 0)
             {
                 sprintf (bestps.posi, "%ld", posi);
             }
       }

       dsqlstatus = lese_a_bas (ratod (bestps.a));
       if (dsqlstatus == 100)
       {
           if (syskey != KEYCR &&syskey != KEYTAB 
               && eListe.IsAppend ())
           {
               return (-1);
           }
           sprintf (bestptab[lrow].a, "%.0lf", Akta);
           memcpy (&bestps, &bestptab[lrow], sizeof (struct AUFPS));
           return (-1);
       }

	   GetLastMe (ratod (bestps.a));

       sprintf (bestps.a_bz1,       "%s",      _a_bas.a_bz1);
       sprintf (bestps.a_bz2,       "%s",      _a_bas.a_bz2);
       sprintf (wert, "%hd", _a_bas.me_einh);
       sprintf (bestps.me_einh, "%hd", _a_bas.me_einh);
       dsqlstatus = ptab_class.lese_ptab ("me_einh", wert);  
       strcpy (bestps.basis_me_bz, ptabn.ptbezk);

       memcpy (&bestptab[lrow], &bestps, sizeof (struct AUFPS));
       if (eListe.IsNewRec ())
       {
           if (ratod (bestps.pr_ek_bto) == (double) 0.0)
           {
                    ReadPr ();
           }

           liefmebest.mdn = best_kopf.mdn;
           liefmebest.fil = best_kopf.fil;
           strcpy (liefmebest.lief, best_kopf.lief);
           liefmebest.a = ratod (bestps.a);

           ReadMeEinh (bestps.lief_best);
           if (bestps.min_best == 0.0)
           {
                bestps.min_best = lief_bzg.min_best;
           }
       }
	   else if (ratod (bestps.pr_ek_bto) == (double) 0.0)
	   {
		           ReadPr ();
	   }
       memcpy (&bestptab[lrow], &bestps, sizeof (struct AUFPS));
       return 0;
}

	    
int BESTPLIST::fetcha (void)
/**
Artikel holen.
**/
{

	   char buffer [25];
	   RECT rect;
       int dsqlstatus;
       char wert [5];
       long posi;
       int art_pos;
       int i;
	   double artikel = 0.0; //220709
	   int offset = 0;




	   clipped (bestps.a);
	   artikel = ratod(bestps.a);
	   if (best_kopf.abteilung == 1) offset = Artikeloffset1;
	   if (best_kopf.abteilung == 2) offset = Artikeloffset2;
	   if (artikel < 100000 && offset > 0)
	   {
		   artikel += offset;
		   sprintf(bestps.a,"%.0lf",artikel);
	   }
	   if (syskey != KEYCR && ratod (bestps.a) == (double) 0.0)
	   {
            return 0;
	   }

	   if (!numeric (bestps.a))
	   {
		   if (searchmodedirect)
		   {
                  QClass.searcha_direct (eListe.Getmamain3 (),bestps.a);
		   }
		   else
		   {
 		          QClass.querya_direct (eListe.Getmamain3 (),bestps.a);
		   }
		   if (ratod (bestps.a) == (double) 0.0)
		   {
               memcpy (&bestptab[eListe.GetAktRow()], 
				       &bestps, sizeof (struct AUFPS));
			   UpdateWindow (mamain1);
               eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
               eListe.ShowAktRow ();
			   return (-1);
		   }
	   }
	   sprintf (bestps.a, "%13.0lf", ratod (bestps.a));


	   if (strlen(clipped(bestps.lief_best)) == 0)
	   {
		if (HoleAnzLief_best(ratod(bestps.a)) > 1)
		{
			ShowBzg (bestps.a);
		}
	   }



	   art_pos = TestNewArt (ratod (bestps.a));
	   if (art_pos != -1 && a_kum_par == FALSE && art_un_tst)
	   {
		           if (abfragejn (eListe.Getmamain3 (), 
					        "Artikel bereits im Auftrag, OK?", "J") == 0)
				   { 
                        sprintf (bestptab[eListe.GetAktRow ()].a, "%.0lf", Akta);
                        memcpy (&bestps, &bestptab[eListe.GetAktRow ()], sizeof (struct AUFPS));
                        eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
                        eListe.ShowAktRow ();
                        return (-1);
				   }
	   }

	   if (art_pos != -1 && a_kum_par && art_un_tst == 1)
	   {
		           if (abfragejn (eListe.Getmamain3 (), 
					        "Der Artikel ist bereits erfasst.\n\n"
							"Artikel bearbeiten ?", "J") == 0)
				   { 
                        sprintf (bestptab[eListe.GetAktRow ()].a, "%.0lf", Akta);
                        memcpy (&bestps, &bestptab[eListe.GetAktRow ()], sizeof (struct AUFPS));
                        eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
                        eListe.ShowAktRow ();
                        return (-1);
				   }
	   }

	   if (art_pos != -1 && a_kum_par)
	   {
  		           GetClientRect (eListe.Getmamain3 (), &rect);
                   DeleteLine ();
				   if (art_pos > eListe.GetAktRow ()) art_pos --;
                   eListe.SetNewRow (art_pos);
 		           eListe.SetFeldFocus0 (art_pos, eListe.FirstColumn ());
                   InvalidateRect (eListe.Getmamain3 (), &rect, TRUE);
                   UpdateWindow (eListe.Getmamain3 ());
                   memcpy (&bestps, &bestptab[art_pos], sizeof (struct AUFPS));
				   if (add_me)
				   {
				            add = TRUE;
				            aufme_old = ratod (bestps.auf_me);
				            CreatePlus ();
				   }
				   SetRowItem ();
                   return (0);
	   }

	   GetLastMe (ratod (bestps.a));
       einh_class.SetBestEinh (1);
       sprintf (bestps.best_txt, "%ld", 0l);
       SetFkt (9, leer, NULL);
       set_fkt (NULL, 10);
       SetFkt (10, leer, NULL);
       posi = atol (bestps.posi);
       if (posi == 0)
       {
             i = eListe.GetAktRow ();
             if (i == 0)
             {
                 posi = 1;
             }
             else
             {
                 posi = atol (bestptab[i - 1].posi);
                 posi ++;
             }
             if (PosiEnd (posi) == 0)
             {
                 sprintf (bestps.posi, "%ld", posi);
             }
       }

       dsqlstatus = lese_a_bas (ratod (bestps.a));
	   if (dsqlstatus == 100)
	   {
		   dsqlstatus = ReadEan (ratod (bestps.a));
	   }

// Umvorhersehbare Ereignisse                

	   if (dsqlstatus < 0)
	   {
		   print_mess (2, "Fehler %d beim Lesen von Artikel %.0lf", dsqlstatus,
			                                                        ratod (bestps.a));
           sprintf (bestptab[eListe.GetAktRow ()].a, "%.0lf", Akta);
           memcpy (&bestps, &bestptab[eListe.GetAktRow ()], sizeof (struct AUFPS));
           eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
           eListe.ShowAktRow ();
           return (-1);
	   }


       if (dsqlstatus == 100)
       {
           if (syskey != KEYCR &&syskey != KEYTAB 
               && eListe.IsAppend ())
           {
               return (-1);
           }
		   if (NoArtMess == FALSE)
		   {
                   print_mess (2, "Artikel %.0lf nicht gefunden",
                          ratod (bestps.a));
		   }
           sprintf (bestptab[eListe.GetAktRow ()].a, "%.0lf", Akta);
           memcpy (&bestps, &bestptab[eListe.GetAktRow ()], sizeof (struct AUFPS));
           eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
           eListe.ShowAktRow ();
           return (-1);
       }

//	   if (a_kun_smt && TestLief_bzg () == -1)
	   if (TestLief_bzg () == -1)
	   {
           sprintf (bestptab[eListe.GetAktRow ()].a, "%.0lf", Akta);
           memcpy (&bestps, &bestptab[eListe.GetAktRow ()], sizeof (struct AUFPS));
           eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
           eListe.ShowAktRow ();
		   return -1;
	   }

	   if (TestKontrakt (ratod (bestps.a)) == -1)
	   {
           sprintf (bestptab[eListe.GetAktRow ()].a, "%.0lf", Akta);
           memcpy (&bestps, &bestptab[eListe.GetAktRow ()], sizeof (struct AUFPS));
           eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
           eListe.ShowAktRow ();
		   return -1;
	   }
       sprintf (bestps.a_bz1,       "%s",      _a_bas.a_bz1);
       sprintf (bestps.a_bz2,       "%s",      _a_bas.a_bz2);
       sprintf (wert, "%hd", _a_bas.me_einh);
       sprintf (bestps.me_einh, "%hd", _a_bas.me_einh);
       dsqlstatus = ptab_class.lese_ptab ("me_einh", wert);  
       strcpy (bestps.basis_me_bz, ptabn.ptbezk);

       memcpy (&bestptab[eListe.GetAktRow()], &bestps, sizeof (struct AUFPS));
       eListe.ShowAktRow ();
       if (eListe.IsNewRec ())
       {
//           if (ratod (aufps.auf_vk_pr) == (double) 0.0)
           {
                    ReadPr ();
                    if ((ratod (bestps.pr_ek_bto) == 0.0) && (preis0_mess == 1))
					{
			          sprintf (buffer, "Achtung !! Preis 0 gelesen\n"
				                       "            OK ?");
                      if (abfragejn (eListe.Getmamain3 (), 
					       buffer , "N") == 0)
					  {
                              sprintf (bestptab[eListe.GetAktRow ()].a, "%.0lf", Akta);
                              memcpy (&bestps, &bestptab[eListe.GetAktRow ()], 
								      sizeof (struct AUFPS));
                              eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
                              eListe.ShowAktRow ();
		                      return -1;
					  }
			   }
           }
           liefmebest.mdn = best_kopf.mdn;
           liefmebest.fil = best_kopf.fil;
           strcpy (liefmebest.lief, best_kopf.lief);
           liefmebest.a = ratod (bestps.a);
//ToDo Preise
           ReadMeEinh (bestps.lief_best);
           if (lief_bzg.me_kz[0] == '1' && PrInBestEinh)
           {
             sprintf (bestps.pr_ek_bto, "%lf", (double) ratod (bestps.pr_ek_bto) * 
                                                                   bestps.inh);
             if (pr_ek_einh != 0.0)
             {
                  sprintf (bestps.pr_ek_bto, "%lf", pr_ek_einh);
             }
	         CalcPrNto (ratod (bestps.pr_ek_bto));
           }
           bestps.min_best = bestps.inh;
           if (bestps.min_best == 0.0)
           {
                bestps.min_best = lief_bzg.min_best;
           }
       }
       memcpy (&bestptab[eListe.GetAktRow()], &bestps, sizeof (struct AUFPS));
       pr_ek_bto = ratod (bestps.pr_ek_bto); 
       
       eListe.ShowAktRow ();
       eListe.SetRowItem ("a", bestptab[eListe.GetAktRow ()].a);
       set_fkt (NULL, 9);
       return 0;
}


int BESTPLIST::HoleAnzLief_best (double art)
{
	static int cursor1 = -1;
	static int cursor2 = -1;
	static long anzahl;
	static double a;

	if (cursor1 == -1)
	{
	   DbClass.sqlin ((short *) &lief_bzg.mdn, 1, 0);
	   DbClass.sqlin ((short *) &lief_bzg.fil, 1, 0);
	   DbClass.sqlin ((char *) best_kopf.lief, 0, 17);
	   DbClass.sqlin ((double *) &a, 3, 0);
	   DbClass.sqlout ((long *) &anzahl, 2, 0);
	   cursor1 = DbClass.sqlcursor ("select count(*) from lief_bzg "
		                                 "where mdn = ? " 
						                 "and fil = ? "
						                 "and lief = ? "
						                 "and a = ?");

      DbClass.sqlin ((short *) &lief_bzg.mdn, 1, 0);
   	  DbClass.sqlin ((short *) &lief_bzg.fil, 1, 0);
	  DbClass.sqlin ((char *) best_kopf.lief, 0, 17);
	  DbClass.sqlin ((double *) &a, 3, 0);
	  DbClass.sqlout ((char *) &lief_bzg.lief_best, 0, 17);
	  cursor2 = DbClass.sqlcursor ("select lief_best from lief_bzg "
		                     "where mdn = ? " 
			                 "and fil = ? "
			                 "and lief = ? "
			                 "and a = ?");

	}

	a = art;
	lief_bzg.mdn = best_kopf.mdn;
	lief_bzg.fil = best_kopf.fil;
	anzahl = 0;
    
    DbClass.sqlopen (cursor1);
    DbClass.sqlfetch (cursor1);
    while (anzahl == 0)
       {
                  if (lief_bzg.fil > 0)
                  {
                      lief_bzg.fil = 0;
                  }
                  else if (lief_bzg.mdn > 0)
                  {
                      lief_bzg.mdn = 0;
                  }
                  else
                  {
                      break;
                  }
				  DbClass.sqlopen (cursor1);
				  DbClass.sqlfetch (cursor1);
       }
   	   if (anzahl == 1)
	   {
		    DbClass.sqlopen (cursor2);
			int dsql = DbClass.sqlfetch (cursor2);
		    if (dsql == 0) strcpy (bestps.lief_best,lief_bzg.lief_best);
		}


	   return anzahl;
}



int BESTPLIST::fetchlief_best (void)
/**
Artikel ueber Lieferanten-Artikelnummer holen.
**/
{
       int dsqlstatus;
	   int cursor;
	   short mdn, fil;

       DbClass.sqlin ((short *) &mdn,    1, 0);
       DbClass.sqlin ((short *) &fil,    1, 0);
       DbClass.sqlin ((long *)  &best_kopf.lief,   0, 17);
       DbClass.sqlin ((char *)   bestps.lief_best, 0,17);
       DbClass.sqlout ((char *)  bestps.a, 0,14);
       cursor = DbClass.sqlcursor ("select a from lief_bzg "
                                   "where mdn = ? "
                                   "and   fil = ? "
                                   "and   lief = ? "
                                   "and  lief_best = ?");
	   mdn = best_kopf.mdn;
	   fil = best_kopf.fil;
       while ((dsqlstatus = DbClass.sqlfetch (cursor)) == 100)
	   {
		   if (fil > 0)
		   {
			   fil = 0;
		   }
		   else if (mdn > 0)
		   {
			   mdn = 0;
		   }
		   else
		   {
			   break;
		   }
		   DbClass.sqlopen (cursor);
	   }
	   DbClass.sqlclose (cursor);
	   if (dsqlstatus)
	   {
              	   print_mess (2, "Der Artikel ist nicht im Sortiment des Lieferanten");
                   return dsqlstatus;
	   }
	   CompareMode = LiefBest;
       int ret = fetcha ();
	   CompareMode = Artikel;
	   return ret;
}


/*
void BESTPLIST::DeleteBsd (void)
/?**
Bestand updaten.
**?/
{
	    return;
        int dsqlstatus; 
        if (angk.kun_fil == 0)
        {
            memcpy (&best_res, &best_res_null, sizeof (best_res));
            best_res.mdn       = aufp.mdn;
            best_res.fil       = aufp.fil;
            best_res.auf       = aufp.auf;
            dsqlstatus = best_res_class.delete_auf ();
        }
}
*/

double BESTPLIST::GetBestMeVgl (void)
/**
Mengeneinheit fuer Kunde und Artikel holen.
**/
{
         KEINHEIT keinheit;
         double best_me_vgl;

         einh_class.AktBestEinh (best_pos.mdn, best_pos.fil,
                                best_kopf.lief, best_pos.a,best_pos.lief_best, best_pos.me_einh);
         einh_class.GetLiefEinh (best_pos.mdn, best_pos.fil,
                                best_pos.lief, best_pos.a,best_pos.lief_best, &keinheit);
          
         if (keinheit.me_einh_lief == keinheit.me_einh_bas)
         {
                      best_me_vgl = best_pos.me;
         }
         else
         {
                     if (keinheit.inh <= (double) 0.0)
                     {
                                   keinheit.inh = (double) 1.0;
                     }
                     best_me_vgl = best_pos.me * keinheit.inh;
         }
         return best_me_vgl;
}

/*
void BESTPLIST::UpdateBsd (void)
/?**
Bestand updaten.
**?/
{
        char melde_term [12];
        int dsqlstatus; 

		return;
        sysdate (melde_term); 
        memcpy (&best_res, &best_res_null, sizeof (best_res));
        best_res.mdn       = angp.mdn;
        best_res.fil       = angp.fil;
        best_res.a         = angp.a;
        best_res.auf       = angp.auf;
        best_res.lief_term = angk.lieferdat;
        best_res.melde_term = dasc_to_long (melde_term);
        dsqlstatus = best_res_class.dbreadfirst ();
        best_res.me += GetAufMeVgl ();
        best_res.kun_fil = angk.kun_fil;
        best_res.kun = angk.kun;
        best_res_class.dbupdate ();
}
*/


void BESTPLIST::WritePos (int pos)
/**
Position schreiben.
**/
{
//	   KEINHEIT keinheit;

       best_pos.p_num      = atoi  (bestptab[pos].posi);
       best_pos.pr_fix = atoi  (bestptab[pos].fix_kz_sint);
       best_pos.a          = ratod (bestptab[pos].a);
       best_pos.me         = ratod (bestptab[pos].auf_me);
       strcpy (best_pos.lief_best, bestptab[pos].lief_best);
       if (best_pos.me == (double) 0) return;

//       best_pos.rab_satz   = ratod (bestptab[pos].rab_satz);
//       best_pos.prov_satz  = ratod (bestptab[pos].prov_satz);
//       sprintf (angp.auf_me_bz,"%s",bestptab[pos].me_bz);
/*
       angp.auf_vk_pr  = ratod (bestptab[pos].auf_vk_pr);
       angp.auf_lad_pr  = ratod (bestptab[pos].auf_lad_pr);
       angp.auf_vk_pr  = ratod (bestptab[pos].auf_vk_dm);
*/
	   memcpy (&bestps, &bestptab[pos], sizeof (AUFPS));
       best_pos.pr_ek_bto        = ratod (bestptab[pos].pr_ek_bto);
	   CalcPrNto (best_pos.pr_ek_bto);
	   memcpy (&bestptab[pos], &bestps, sizeof (AUFPS));
       best_pos.pr_ek            = ratod (bestptab[pos].pr_ek_dm);
       best_pos.pr_ek_euro       = ratod (bestptab[pos].pr_ek_euro);
       best_pos.pr_ek_bto        = ratod (bestptab[pos].pr_ek_dm_bto);
       best_pos.pr_ek_euro_bto   = ratod (bestptab[pos].pr_ek_euro_bto);
       best_pos.pr_ek_fremd      = ratod (bestptab[pos].pr_ek_fremd);
       best_pos.pr_ek_fremd_bto  = ratod (bestptab[pos].pr_ek_fremd_bto);
//       sprintf (angp.lief_me_bz,"%s",bestptab[pos].basis_me_bz);
       if (best_pos.a == (double) 0)
       {
           return;
       }
       best_pos.me_einh          = atoi (bestptab[pos].me_einh_lief);


       best_pos.me_einh     = atoi (bestptab[pos].me_einh);
       best_pos.best_txt    = atol (bestptab[pos].best_txt);
       best_pos.inh         = bestptab[pos].min_best;
       strcpy (best_pos.lief, best_kopf.lief);
       clipped (best_pos.lief);
 	   if (numeric (best_pos.lief) && strlen (best_pos.lief) < 9)
       {
			  best_pos.lief_s = atol (best_pos.lief);
       }
	   bestp_class.dbupdate ();

	   preise_we.mdn = best_pos.mdn;
	   preise_we.a = best_pos.a;
       strcpy (preise_we.lief, best_kopf.lief);
	   preise_we.best_blg = best_kopf.best_blg;
	   strcpy(preise_we.lief_rech_nr,"");
	   if (preise_we_class.dbreadfirst () != 0)
	   {
			preise_we.best_blg = best_kopf.best_blg;
	        strcpy (preise_we.lief, best_kopf.lief);
			strcpy (preise_we.lief_rech_nr,"");
			preise_we.we_dat = 0;
		    preise_we.pr_ek_w = 0;
		    preise_we.me_einh_w = 0;
	   }
	   preise_we.best_term = best_kopf.best_term;
	   preise_we.pr_ek_b = best_pos.pr_ek;
	   preise_we.me_einh_b = best_pos.me_einh;
	   preise_we_class.dbupdate ();

}


int BESTPLIST::WriteAllPos (void)
/**
Alle Positionen schreiben.
**/
{
    int i;
    int recs;
    int row;

    row     = eListe.GetAktRow ();

    if (TestRow () == -1)
    {
                         
            eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                                  eListe.GetAktColumn ());
            return -1;
    }

    memcpy (&bestptab[row], &bestps, sizeof (struct AUFPS));
    recs = eListe.GetRecanz ();
	bestp_class.dbdeletebest ();
    GenNewPosi ();
    for (i = 0; i < recs; i ++)
    {
        WritePos (i);
    }
    eListe.BreakList ();
    return 0;
}


int BESTPLIST::HeadWriteAllPos (void)
/**
Alle Positionen schreiben.
**/
{
    int i;
    int recs;

    recs = eListe.GetRecanz ();
	bestp_class.dbdeletebest ();
    for (i = 0; i < recs; i ++)
    {
        WritePos (i);
    }
    return 0;
}



void BESTPLIST::SaveBest (void)
{
    save_fkt (5);
    save_fkt (6);
    save_fkt (7);
    save_fkt (8);
    save_fkt (12);
}

void BESTPLIST::SetBest (void)
{
    set_fkt (dokey5, 5);
    set_fkt (InsertLine, 6);
    set_fkt (DeleteLine, 7);
    set_fkt (AppendLine, 8);
    set_fkt (WriteAllPos, 12);
}

void BESTPLIST::RestoreBest (void)
{
    restore_fkt (5);
    restore_fkt (6);
    restore_fkt (7);
    restore_fkt (8);
    restore_fkt (12);
}


int BESTPLIST::dokey5 (void)
/**
Listenerfassung abbrechen.
**/
{
	short sql_sav;
	extern short sql_mode;

    if (abfragejn (mamain1, "Positionen speichern ?", "J"))
    {
           WriteAllPos ();
           syskey = KEY12;
    }

	sql_sav = sql_mode;
	sql_mode = 1;
    DbClass.sqlin ((short *) &best_kopf.mdn, 1, 0);
    DbClass.sqlin ((short *) &best_kopf.fil, 1, 0);
    DbClass.sqlin ((long *)  &best_kopf.best_blg, 2, 0);
    DbClass.sqlcomm ("delete from best_pos where mdn = ? and fil = ? "
                     "and best_blg = ? and me = 0");
    
	sql_mode = sql_sav; 
    syskey = KEY5;
    RestoreBest ();

    eListe.SetListFocus (0);
    eListe.SetFeldFocus0 (eListe.GetAktRow (), eListe.GetAktColumn ());

    eListe.BreakList ();
    return 1;
}


void BESTPLIST::InitSwSaetze (void)
{
       int i;

       for (i = 0; i < MAXPOS; i ++)
       {
               SwSaetze [i] = (char *) &bestptab [i];
       }
}

void BESTPLIST::SetRecHeight (void)
/**
Anzahl Zeilen pro Listzeile ermitteln.
**/
{
       int height;
       int i;

       height = 0;
       for (i = 0; i < dataform.fieldanz; i ++)
       {
           if (dataform.mask[i].attribut & REMOVED) continue;
           if (dataform.mask[i].pos[0] > height)
           {
               height = dataform.mask[i].pos[0];
           }
       }
       height ++; 
       eListe.SetRecHeight (height);
}

int BESTPLIST::ToMemory (int pos)
/**
SW-Satz in Ascii-Format Speichern.
**/
{

       memcpy (SwSaetze[pos], ausgabesatz, zlen);
       eListe.SetRecanz (pos + 1);
       return 0;
}


void BESTPLIST::SetStringEnd (char *feldname, int len)
/**
Stringende Setzen.
**/
{

       return;
}


void BESTPLIST::uebertragen (void)
/**
Eingabesatz in ASCII-Striktur uebertragen.
**/
{
	   static int cursor = -1;
	   short mdn, fil;

/*
	   if (cursor == -1 && (GetFieldAttr ("a_best") & REMOVED) == 0)
	   {
                 DbClass.sqlin ((short *) &mdn,    1, 0);
                 DbClass.sqlin ((short *) &fil,    1, 0);
                 DbClass.sqlin ((long *)  &best_kopf.lief,   0, 17);
                 DbClass.sqlin ((char *)  bestps.a, 0,14);
                 DbClass.sqlout ((char *) bestps.lief_best, 0,17);
                 cursor = DbClass.sqlcursor ("select lief_best from a_best "
                                     "where mdn = ? "
                                     "and   fil = ? "
                                     "and   lief = ? "
                                     "and   a = ?");
	   }
*/
       lese_a_bas (best_pos.a);

	   if (best_pos.pr_ek_bto > 0.0)
	   {
				best_pos.pr_ek  = best_pos.pr_ek_bto - (double) WePreis.GetRabEk 
				                                      (best_pos.mdn, best_pos.fil, 
			                                           best_kopf.lief, 
	  						                           1, best_pos.pr_ek_bto, _a_bas.me_einh,_a_bas.a,_a_bas.ag,_a_bas.wg);
	   }

	   if (best_pos.pr_ek_euro_bto > 0.0)
	   {
				best_pos.pr_ek_euro  = best_pos.pr_ek_euro_bto - (double) WePreis.GetRabEk 
				                                      (best_pos.mdn, best_pos.fil, 
			                                           best_kopf.lief, 
	  						                           1, best_pos.pr_ek_euro_bto, _a_bas.me_einh,_a_bas.a,_a_bas.ag,_a_bas.wg);
	   }

       sprintf (bestps.posi,        "%4ld",    best_pos.p_num);
//       sprintf (bestps.sa_kz_sint,  "%1hd",    best_pos.sa_kz_sint);
       sprintf (bestps.fix_kz_sint,  "%1hd",    best_pos.pr_fix);
       sprintf (bestps.a,           "%13.0lf", best_pos.a);
       sprintf (bestps.lief_best,  "%s",       best_pos.lief_best);
       sprintf (bestps.a_bz1,       "%s",      _a_bas.a_bz1);
       sprintf (bestps.a_bz2,       "%s",      _a_bas.a_bz2);
       sprintf (bestps.last_me,     "%8.3lf",  0,0);
       sprintf (bestps.auf_me,      "%8.3lf",  best_pos.me);
       sprintf (bestps.pr_ek_dm,        "%lf",   best_pos.pr_ek);
       sprintf (bestps.pr_ek_dm_bto,    "%lf",   best_pos.pr_ek_bto);
       sprintf (bestps.pr_ek_euro,      "%lf",   best_pos.pr_ek_euro);
       sprintf (bestps.pr_ek_euro_bto,  "%lf",   best_pos.pr_ek_euro_bto);
       sprintf (bestps.pr_ek_fremd,     "%lf",   best_pos.pr_ek_fremd);
       sprintf (bestps.pr_ek_fremd_bto, "%lf",   best_pos.pr_ek_fremd_bto);
	   FillAktWaehrung ();
       sprintf (bestps.me_einh,     "%hd",     best_pos.me_einh);
       sprintf (bestps.best_txt,    "%ld",     best_pos.best_txt);

	   GetLastMe (ratod (bestps.a));
	   if (cursor != -1 && (GetFieldAttr ("a_best") & REMOVED) == 0)
	   {
	           DbClass.sqlopen (cursor);
	           while (DbClass.sqlfetch (cursor) == 100)
			   {
				   if (fil > 0)
				   {
					   fil = 0;
				   }
				   else if (mdn > 0)
				   {
					   mdn = 0;
				   }
				   else
				   {
					   break;
				   }
				   DbClass.sqlopen (cursor);
			   }
	   }
       liefmebest.mdn = best_kopf.mdn;
       liefmebest.fil = best_kopf.fil;
       strcpy (liefmebest.lief, best_kopf.lief);
       liefmebest.a = ratod (bestps.a);
       bestps.min_best = 0.0;
       ReadAktMeEinh (best_pos.me_einh,best_pos.lief_best);
       if (bestps.min_best == 0.0)
       {
                bestps.min_best = bestps.inh;
       }
       if (bestps.min_best == 0.0)
       {
                bestps.min_best = lief_bzg.min_best;
       }

// Wegen me_kz in lief_bzg lesen


	   char lieferdat [12];
	   dlong_to_asc (best_kopf.lief_term, lieferdat);

       int dsqlstatus = WePreis.preis_holen (best_kopf.mdn,
                                          best_kopf.fil,
                                          best_kopf.lief,
										  lieferdat,
                                          ratod (bestps.a));
	   strcpy (bestps.me_kz, lief_bzg.me_kz);

	   double pos_wrt = 0.0;
	   pos_wrt = ratod (bestps.auf_me) * ratod (bestps.pr_ek);
 	   if (bestps.me_kz[0] == '1' && !PrInBestEinh)
	   {
			pos_wrt *= bestps.inh;
	   }
	   sprintf (bestps.pos_wrt, "%.2lf", pos_wrt);
}

void BESTPLIST::ShowDB (short mdn, short fil, long best_blg, char *lief)
/**
Bestellpositionen lesen und anzeigen.
**/
{
        int i;
        HCURSOR oldcursor;
        extern short do_exit;
        int dsqlstatus;


        InitSwSaetze ();
        eListe.SetRecanz (0);
        i = eListe.GetRecanz ();
        eListe.SetUbForm (&ubform);
        oldcursor = SetCursor (LoadCursor (NULL, IDC_WAIT));
		best_pos.mdn = mdn;
		best_pos.fil = fil;
		best_pos.best_blg = best_blg;
		strcpy (best_pos.lief, lief);
		dsqlstatus = bestp_class.dbreadfirst_o ("order by p_num,a");
        while (dsqlstatus == 0)
        {
                     uebertragen ();
                     if (ToMemory (i) != 0) break;
                     i = eListe.GetRecanz ();
                     if (i >= MAXPOS) break; 
		             dsqlstatus = bestp_class.dbread_o ();
        }

        SetRecHeight ();
        SetCursor (oldcursor);
        i = eListe.GetRecanz ();

        eListe.SetRecanz (i);
        SwRecs = eListe.GetRecanz ();
 
        SetFieldAttr ("a", DISPLAYONLY);
        eListe.SetDataForm0 (&dataform, &lineform);
        eListe.SetSaetze (SwSaetze);
        eListe.SetChAttr (ChAttr); 
        eListe.SetUbRows (ubrows); 
        if (i == 0)
        {
			     Posanz = 0;
                 eListe.AppendLine ();
				 AktRow = AktColumn = 0;
                 eListe.SetPos (AktRow, AktColumn);
                 i = eListe.GetRecanz ();
        }
		else
		{
			     Posanz = i;
		}
        SendMessage (eListe.Getmamain2 (), WM_SIZE, NULL, NULL);
        InvalidateRect (eListe.Getmamain3 (), 0, TRUE);
        memcpy (&bestps, &bestptab[0], sizeof (struct AUFPS));
}


void BESTPLIST::ReadDB (short mdn, short fil, long best_blg)
/**
Auftragspositionen lesen.
**/
{
        ausgabesatz = (unsigned char *) &bestps;
        zlen = sizeof (struct AUFPS);

        PageView = 0;
        eListe.SetPageView (0);
        feld_anz = dataform.fieldanz;
        Lstzlen = zlen;
        LstSatz = ausgabesatz;

        Lstbanz = feld_anz;

        eListe.SetPos (AktRow, AktColumn);
        eListe.Setbanz (feld_anz);
        eListe.Setzlen (zlen);
        eListe.Initscrollpos ();
        eListe.SetAusgabeSatz (ausgabesatz);
}

void BESTPLIST::DestroyWindows (void)
{
       eListe.DestroyListWindow ();
       DestroyMainWindow ();
       InitMax ();
       InitMin ();
       MoveMamain1 ();
       ListAktiv = 0;
       SetFkt (11, leer, NULL);
       set_fkt (NULL, 11);
}

void BESTPLIST::SetSchirm (void)
{
       set_fkt (Schirm, 11);
       SetFkt (11, vollbild, KEY11);
       set_fkt (ShowPreise_we, 2); //TESTTEST
}

void BESTPLIST::ShowBestp (short mdn, short fil, long best_blg, char *lief, char *bdat)
/**
Auftragsliste bearbeiten.
**/

{
	   int pos;

       if (ListAktiv) return; 
	   if (best_kopf.waehrung == 0)
	   {
	             pos = GetItemPos (&ubform, "pr_ek");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (EK);
				 }
	             pos = GetItemPos (&ubform, "pr_ek_bto");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (EB);
				 }
	   }
	   else if (best_kopf.waehrung == 1)
	   {
	             pos = GetItemPos (&ubform, "pr_ek");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (EK_DM);
				 }
	             pos = GetItemPos (&ubform, "pr_vk_bto");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (EB_DM);
				 }
	   }
	   else if (best_kopf.waehrung == 2)
	   {
	             pos = GetItemPos (&ubform, "pr_ek");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (EK_EURO);
				 }
	             pos = GetItemPos (&ubform, "pr_ek_bto");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (EB_EURO);
				 }
	   }
	   else if (best_kopf.waehrung == 3)
	   {
	             pos = GetItemPos (&ubform, "pr_ek");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (EK_FREMD);
				 }
	             pos = GetItemPos (&ubform, "pr_ek_bto");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (EB_FREMD);
				 }
	   }
	   else
	   {
	             pos = GetItemPos (&ubform, "pr_ek");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (EK_DM);
				 }
	             pos = GetItemPos (&ubform, "pr_vk_bto");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (EB_DM);
				 }
	   }

       Geta_bz2_par ();
       Geta_kum_par ();
       GetWeANeuPar ();
	   GetCfgValues ();
	   if (FormOK == FALSE)
	   {

/*
           if (DelLadVK)
		   {
			     int upos = GetItemPos (&ubform, "pr_ek"); 
			     int ipos = GetItemPos (&dataform, "pr_ek"); 
				 if (upos != -1 && ipos != -1)
				 {
					DelFormField (&ubform, upos);
					DelFormField (&dataform, ipos);
					DelFormField (&lineform, upos - 1);
					lineform.mask[lineform.fieldanz - 1].pos[1] =
                             lineform.mask[lineform.fieldanz - 2].pos[1] + 
                             ubform.mask[ubform.fieldanz - 1].length;
				 }
		   }
*/

           if (DelLastMe)
		   {
		         DelFormField (&ubform, 4);
		         DelFormField (&dataform, 5);
		         DelFormField (&lineform, 4);
		         lineform.mask[lineform.fieldanz - 1].pos[1] =
                             lineform.mask[lineform.fieldanz - 2].pos[1] + 
                             ubform.mask[ubform.fieldanz - 1].length;
		   }
           if (DelPosWrt)
		   {
			     int upos = GetItemPos (&ubform, "pos_wrt"); 
			     int ipos = GetItemPos (&dataform, "pos_wrt"); 
				 if (upos != -1 && ipos != -1)
				 {
					DelFormField (&ubform, upos);
					DelFormField (&dataform, ipos);
					DelFormField (&lineform, upos - 1);
					lineform.mask[lineform.fieldanz - 1].pos[1] =
                             lineform.mask[lineform.fieldanz - 2].pos[1] + 
                             ubform.mask[ubform.fieldanz - 1].length;
				 }
		   }
           if (RemovePr)
		   {
			     int upos = GetItemPos (&ubform, "pr_ek_bto"); 
			     int ipos = GetItemPos (&dataform, "pr_ek_bto"); 
				 if (upos != -1 && ipos != -1)
				 {
					DelFormField (&ubform, upos);
					DelFormField (&dataform, ipos);
					DelFormField (&lineform, upos - 1);
					lineform.mask[lineform.fieldanz - 1].pos[1] =
                             lineform.mask[lineform.fieldanz - 2].pos[1] + 
                             ubform.mask[ubform.fieldanz - 1].length;
				 }
			     upos = GetItemPos (&ubform, "pr_ek"); 
			     ipos = GetItemPos (&dataform, "pr_ek"); 
				 if (upos != -1 && ipos != -1)
				 {
					DelFormField (&ubform, upos);
					DelFormField (&dataform, ipos);
					DelFormField (&lineform, upos - 1);
					lineform.mask[lineform.fieldanz - 1].pos[1] =
                             lineform.mask[lineform.fieldanz - 2].pos[1] + 
                             ubform.mask[ubform.fieldanz - 1].length;
				 }
		   }
		   FormOK = TRUE;
	   }
       best_kopf.mdn = mdn;
       best_kopf.fil = fil;
       best_kopf.best_blg = best_blg;
       strcpy (best_kopf.lief, lief);
       best_kopf.best_term = dasc_to_long (bdat);

   	   add = FALSE;
	   aufme_old = (double) 0.0;
	   DestroyPlus ();
       SetSchirm ();
       eListe.SetInfoProc (InfoProc);
       sprintf (InfoCaption, "Bestellung %ld", best_blg);
       mamain1 = CreateMainWindow ();
       eListe.InitListWindow (mamain1);
       ReadDB (mdn, fil, best_blg);

       eListe.SetListFocus (0);
       eListe.Initscrollpos ();
       AktRow = 0;
       AktColumn = 0;
       scrollpos = 0;
       SetAktivWindow (eListe.Getmamain2 ());
       ShowDB (mdn, fil, best_blg, lief);

       eListe.SetRowItem ("a", bestptab[0].a);
       
       ListAktiv = 1;

       return;
}

void BESTPLIST::GetListColor (COLORREF *color, char *cfg_v)
/**
Farbunterlegung fuer Listen uebertragen.
**/
{
	static char *ColTxt[] = {"BLACKCOL",
		                     "WHITECOL",
							 "BLUECOL",
							 "REDCOL",
							 "LTGRAYCOL",
							 "GREENCOL",
							 "YELLOWCOL",
							 "DKYELLOWCOL",
							 NULL};
	
	static COLORREF ColVal[] = {BLACKCOL,
		                        WHITECOL,
					  		    BLUECOL,
							    REDCOL,
							    LTGRAYCOL,
							    GREENCOL,
							    YELLOWCOL,
								DKYELLOWCOL};
	int i;
	int red, blue, green;
	char *ColR;
	int Cols = 0;

	for (i = 0; ColTxt[i]; i ++)
	{
		if (strcmp (ColTxt[i], cfg_v) == 0)
		{
			*color = ColVal[i];
			return;
		}
	}


	ColR = strstr (cfg_v, "red");
	if (ColR)
	{
		red = atoi (&ColR[3]);
		Cols ++;
	}
	ColR = strstr (cfg_v, "blue");
	if (ColR)
	{
		blue = atoi (&ColR[4]);
		Cols ++;
	}
	ColR = strstr (cfg_v, "green");
	if (ColR)
	{
		green = atoi (&ColR[5]);
		Cols ++;
	}
	if (Cols == 3)
	{
		*color = RGB (red, green, blue);
	}

}

void BESTPLIST::SetPreisTest (int mode)
{
	if (mode == 0) return;

	preistest = min (4, max (1, mode));
	if (preistest == 3)
	{
		SetItemAttr (&dataform, "pr_ek_bto", DISPLAYONLY);
	}
}

void BESTPLIST::SetNoRecNr (void)
{
	   static BOOL SetOK = FALSE;
	   int i;

	   if (SetOK) return;

	   for (i = 0; i < dataform.fieldanz; i ++)
	   {
		   dataform.mask[i].pos[1] -= 6;
	   }
	   for (i = 0; i < ubform.fieldanz; i ++)
	   {
		   ubform.mask[i].pos[1] -= 6;
	   }
	   for (i = 0; i < lineform.fieldanz; i ++)
	   {
		   lineform.mask[i].pos[1] -= 6;
	   }
	   eListe.SetNoRecNr (TRUE);
       SetOK = TRUE;
}


void BESTPLIST::GetCfgValues (void)
/**
Werte aus 51100.cfg holen.
**/
{
	   static BOOL cfgOK = FALSE;
       char cfg_v [512];

	   if (cfgOK) return;

	   cfgOK = TRUE;
       if (ProgCfg.GetCfgValue ("plu_size", cfg_v) ==TRUE)
       {
                    plu_size = atoi (cfg_v);
       }
       else
        {
                    plu_size = 0;
        }
       if (ProgCfg.GetCfgValue ("AnzTagePreise", cfg_v) ==TRUE)
       {
                    AnzTagePreise = atoi (cfg_v);
       }
       else
        {
                    AnzTagePreise = 365;
        }
        if (ProgCfg.GetCfgValue ("auf_me_default", cfg_v) == TRUE)
        {
                    auf_me_default = atoi (cfg_v);
        }
        else
        {
                    auf_me_default = 0;
        }
        if (ProgCfg.GetCfgValue ("searchadirect", cfg_v) == TRUE)
		{
			        searchadirect = min (1, atoi (cfg_v));
		}
        if (ProgCfg.GetCfgValue ("searchmodedirect", cfg_v) == TRUE)
		{
			        searchmodedirect = min (1, atoi (cfg_v));
		}
        if (ProgCfg.GetCfgValue ("rowheight", cfg_v) == TRUE)
		{
			        RowHeight = ratod (cfg_v);
					if (RowHeight < (double) 1.0)
					{
						RowHeight = (double) 1.0;
					}
		}
        if (ProgCfg.GetCfgValue ("ubheight", cfg_v) == TRUE)
		{
			        UbHeight = atoi (cfg_v);
					SetUbHeight ();
		}
		eListe.SetRowHeight (RowHeight);
        if (ProgCfg.GetCfgValue ("listfocus", cfg_v) == TRUE)
		{
			        ListFocus = min (4, atoi (cfg_v));
					ListFocus = max (3, ListFocus);
		}
        if (ProgCfg.GetCfgValue ("matchcode", cfg_v) == TRUE)
		{
			         SetMatchCode (atoi (cfg_v));
		}
        if (ProgCfg.GetCfgValue ("bsd_kz", cfg_v) == TRUE)
		{
			         bsd_kz = atoi (cfg_v);

		}
        if (ProgCfg.GetCfgValue ("rab_prov_kz", cfg_v) == TRUE)
		{
			         rab_prov_kz = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("auf_wert_anz", cfg_v) == TRUE)
		{
			         auf_wert_anz = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("a_kun_smt", cfg_v) == TRUE)
		{
			         a_kun_smt = atoi (cfg_v);
		}

        if (ProgCfg.GetCfgValue ("LISTCOLORS", cfg_v) == TRUE)
        {
		             ListColors =  min (1, max (0, atoi (cfg_v)));
        }

        if (ProgCfg.GetCfgValue ("KOMPFCOLOR", cfg_v) == TRUE)
        {
		             GetListColor (&KompfColor, cfg_v);
        }

        if (ProgCfg.GetCfgValue ("KOMPBCOLOR", cfg_v) == TRUE)
        {
		             GetListColor (&KompbColor, cfg_v);
        }
        if (ProgCfg.GetCfgValue ("SAFCOLOR", cfg_v) == TRUE)
        {
		             GetListColor (&SafColor, cfg_v);
					 MessCol = SafColor;
        }
        if (ProgCfg.GetCfgValue ("SABCOLOR", cfg_v) == TRUE)
        {
		             GetListColor (&SabColor, cfg_v);
					 MessBkCol = SafColor;
        }
/*
        if (ProgCfg.GetCfgValue ("sortstnd", cfg_v) == TRUE)
        {
		             StndAuf.SetSortMode (atoi (cfg_v));
        }
*/
        if (ProgCfg.GetCfgValue ("preistest", cfg_v) == TRUE)
        {
		             SetPreisTest (atoi (cfg_v));
        }

        if (ProgCfg.GetCfgValue ("sacreate", cfg_v) == TRUE)
        {
		             sacreate = min (1, max (0, (atoi (cfg_v))));
        }
        if (ProgCfg.GetCfgValue ("lad_vk", cfg_v) == TRUE)
        {
			         if (atoi (cfg_v))
					 {
						 DelLadVK = FALSE;
					 }
        }
        if (ProgCfg.GetCfgValue ("last_me", cfg_v) == TRUE)
        {
			         if (atoi (cfg_v))
					 {
						 DelLastMe = FALSE;
					 }
        }
        if (ProgCfg.GetCfgValue ("pos_wrt", cfg_v) == TRUE)
        {
			         if (atoi (cfg_v))
					 {
						 DelPosWrt = FALSE;
					 }
        }
/*
        if (ProgCfg.GetCfgValue ("proptimize", cfg_v) == TRUE)
        {
			          WaPreis.SetOptimize (atoi (cfg_v));
        }
*/
        if (ProgCfg.GetGroupDefault ("pr_alarm", cfg_v) == TRUE)
        {
                      prproz_diff = ratod (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("art_un_tst", cfg_v) == TRUE)
        {
		             art_un_tst =  min (1, max (0, atoi (cfg_v)));
        }
        if (ProgCfg.GetCfgValue ("add_me", cfg_v) == TRUE)
        {
		             add_me =  min (1, max (0, atoi (cfg_v)));
        }
        if (ProgCfg.GetCfgValue ("a_kum", cfg_v) == TRUE)
        {
                     a_kum_par =  min (1, max (0, atoi (cfg_v)));
 					 a_kum_par = TRUE;  // beim Speichern wirds eh kummuliert !!!
        }
        if (ProgCfg.GetCfgValue ("preis0_mess", cfg_v) == TRUE)
        {
                     preis0_mess = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("best_me_pr_0", cfg_v) == TRUE)
        {
                     best_me_pr_0 = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("RemovePr", cfg_v) == TRUE)
        {
                     RemovePr = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("textinpmode", cfg_v) == TRUE)
        {
                     textinpmode = atol (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("PrInBestEinh", cfg_v) == TRUE)
        {
                     PrInBestEinh = atol (cfg_v);
        }
       if (ProgCfg.GetCfgValue ("3DList", cfg_v) == TRUE)
       {
                    eListe.Set3D (atoi (cfg_v));
	   }
       if (ProgCfg.GetCfgValue ("3DSize", cfg_v) == TRUE)
       {
                    eListe.SetPlus3D (atoi (cfg_v));
	   }
       if (ProgCfg.GetCfgValue ("ListFocus", cfg_v) == TRUE)
       {
                    ListFocus = atoi (cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("LstBestEk", cfg_v) == TRUE)
       {
                     LstBestEk = atoi (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("NoRecNr", cfg_v) == TRUE)
       {
		            if (atoi (cfg_v))
					{
	                        SetNoRecNr ();
					}
	   }
       if (ProgCfg.GetCfgValue ("TestKontrakt", cfg_v) == TRUE)
       {
                    SetTestKontrakt (atoi (cfg_v));
	   }

       if (ProgCfg.GetCfgValue ("Artikeloffset1", cfg_v) == TRUE)
       {
                    Artikeloffset1 =  atoi (cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("Artikeloffset2", cfg_v) == TRUE)
       {
                    Artikeloffset2 =  atoi (cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("PrNachKomma", cfg_v) == TRUE)
       {
                    int nk        = atoi (cfg_v);
                    int ekpos     = GetItemPos (&dataform, "pr_ek");
                    int ek_btopos = GetItemPos (&dataform, "pr_ek_bto");
                    switch (nk)
                    {
                          case 2 :
                              if (ekpos >= 0)
                              {
                                  dataform.mask[ekpos].picture = "%6.2f";
                              }
                              if (ek_btopos >= 0)
                              {
                                  dataform.mask[ek_btopos].picture = "%6.2f";
                              }
                              break;
                          case 3 :
                              if (ekpos >= 0)
                              {
                                  dataform.mask[ekpos].picture = "%7.3f";
                              }
                              if (ek_btopos >= 0)
                              {
                                  dataform.mask[ek_btopos].picture = "%7.3f";
                              }
                              break;
                          case 4 :
                              if (ekpos >= 0)
                              {
                                  dataform.mask[ekpos].picture = "%8.4f";
                              }
                              if (ek_btopos >= 0)
                              {
                                  dataform.mask[ek_btopos].picture = "%8.4f";
                              }
                              break;       
                    }
	   }
}


static char prabval [10];
static char pprovval [10];

static ITEM iprab  ("rab_satz",  prabval,   "Rabatt......:", 0);
static ITEM ipprov ("prov_satz", pprovval,  "Provision...:", 0);

static field _prab1 [] = {
&iprab,       8,  0, 1,10, 0, "%5.2f", EDIT, 0, 0, 0,
&iOK,        15, 0,  3, 4, 0, "", BUTTON, 0,EnterTest ,KEY12,
&iCA,        15, 0,  3,21, 0, "", BUTTON, 0,EnterTest ,KEY5,
};

static form prab1 = {3, 0, 0, _prab1, 0, 0, 0, 0, NULL};    

static field _prab2 [] = {
&ipprov,      8,  0, 1,10, 0, "%5.2f", EDIT, 0, 0, 0,
&iOK,        15, 0,  3, 4, 0, "", BUTTON, 0,EnterTest ,KEY12,
&iCA,        15, 0,  3,21, 0, "", BUTTON, 0,EnterTest ,KEY5,
};

static form prab2 = {3, 0, 0, _prab2, 0, 0, 0, 0, NULL};    

static field _prab3 [] = {
&iprab,       8,  0, 1, 10, 0, "%5.2f", EDIT, 0, 0, 0,
&ipprov,      8,  0, 2, 10, 0, "%5.2f", EDIT, 0, 0, 0,
&iOK,        15, 0,  4,  4, 0, "", BUTTON, 0,EnterTest ,KEY12,
&iCA,        15, 0,  4, 21, 0, "", BUTTON, 0,EnterTest ,KEY5,
};

static form prab3 = {4, 0, 0, _prab3, 0, 0, 0, 0, NULL};    

static form *prab;

void BESTPLIST::ChoiseLines (HWND eWindow, HDC hdc)
{
         static HPEN hPenG = NULL;
         static HPEN hPenW = NULL;
         TEXTMETRIC tm;
         RECT rect;
         int x, y;
         int cx, cy;

 		 if (eWindow == NULL) return;

		 memcpy (&tm, &textm, sizeof (tm)); 
         GetClientRect (eWindow, &rect);
         x = tm.tmAveCharWidth / 2;
         y = tm.tmHeight / 2;

         cx = rect.right - tm.tmAveCharWidth / 2;
         cy = rect.bottom - tm.tmHeight / 2;
         if (hPenG == NULL)
         {
                   hPenG = CreatePen (PS_SOLID, 0, GRAYCOL);
                   hPenW = CreatePen (PS_SOLID, 0, WHITECOL);
         }
         SelectObject (hdc, hPenG);

         MoveToEx (hdc, x, y, NULL);
         LineTo (hdc, cx, y);

         MoveToEx (hdc, x, y, NULL);
         LineTo (hdc, x, cy);

         MoveToEx (hdc, cx, y, NULL);
         LineTo (hdc, cx, cy);

         MoveToEx (hdc, x, cy, NULL);
         LineTo (hdc, cx, cy);

         SelectObject (hdc, hPenW);


         y ++;
         x ++;
         cx --;
         cy --;
         MoveToEx (hdc, x, y, NULL);
         LineTo (hdc, cx, y);

         MoveToEx (hdc, x, y, NULL);
         LineTo (hdc, x, cy);

         cx += 2;
         cy += 2;
         MoveToEx (hdc, cx, y, NULL);
         LineTo (hdc, cx, cy);

         MoveToEx (hdc, x, cy, NULL);
         LineTo (hdc, cx, cy);
}


HWND BESTPLIST::CreateEnter (void)
/**
Hauptfenster fuer Liste erzeugen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;
        HDC hdc;

        if (hMainWin == NULL) return NULL;    
           
        if (eWindow) return eWindow;

//        eListe.GetTextMetric (&tm);
	    memcpy (&tm, &textm, sizeof (tm)); 

        GetClientRect (hMainWin, &rect);
        GetWindowRect (hMainWin, &wrect);
       
        y = (wrect.bottom - 16 * tm.tmHeight);
        cx = 40 * tm.tmAveCharWidth;
        x = wrect.left + 2 + (rect.right - cx) / 2;
		if (rab_prov_kz < 3)
		{
                  cy = 7 * tm.tmHeight;
		}
		else
		{
                  cy = 9 * tm.tmHeight;
		}

        eWindow       = CreateWindow (
                                       "ListMain",
//                                       "hListWindow", 
                                       "",
                                       WS_DLGFRAME | 
                                       WS_POPUP,
                                       x, y,
                                       cx, cy,
                                       hMainWin,
                                       NULL,
                                       hMainInst,
                                    NULL);
        ShowWindow (eWindow, SW_SHOWNORMAL);
        UpdateWindow (eWindow);
        hdc = GetDC (eWindow);
        ChoiseLines (eWindow, hdc);
        ReleaseDC (eWindow, hdc);
        return eWindow;
}



long BESTPLIST::EnterPosRab (void)
/**
Nummer Eingeben.
*/
{
	     return 0l;
/*		 
          int currentf;

		  save_fkt (5);
		  save_fkt (6);
		  save_fkt (7);
		  save_fkt (8);
		  save_fkt (9);
		  save_fkt (10);
		  save_fkt (11);
		  save_fkt (12);
          set_fkt (EnterBreak, 5);
		  CreateEnter ();
          currentf = currentfield;
		  switch (rab_prov_kz)
		  {
		        case 1 :
			        prab = &prab1;
					break;
		        case 2 :
			        prab = &prab2;
					break;
		        case 3 :
			        prab = &prab3;
					break;
		  }
          sprintf (prabval , "%.2lf", (double) ratod (bestps.rab_satz));          
          sprintf (pprovval, "%.2lf", (double) ratod (bestps.prov_satz));          
          break_end ();
		  EnableWindows (hMainWin, FALSE);
          enter_form (eWindow, prab, 0, 0);
		  EnableWindows (hMainWin, TRUE);
		  DestroyWindow (eWindow);
		  eWindow = NULL;
          no_break_end ();
          currentfield = currentf;
          eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                             eListe.GetAktColumn ());
		  if (syskey != KEY5)
		  {
                   sprintf (bestps.rab_satz, "%.2lf", (double) ratod (prabval));          
                   sprintf (bestps.prov_satz, "%.2lf", (double) ratod (pprovval));          
                   memcpy (&bestptab[eListe.GetAktRow ()], &bestps, sizeof (struct AUFPS));
		  }
		  restore_fkt (5);
		  restore_fkt (6);
		  restore_fkt (7);
		  restore_fkt (8);
		  restore_fkt (9);
		  restore_fkt (10);
		  restore_fkt (11);
		  restore_fkt (12);
          return 0l;
*/
}
      

void BESTPLIST::EnterBestp (short mdn, short fil, long best_blg,
                            char * lief, char *bdat)
/**
Bestellliste bearbeiten.
**/

{
	   int pos;
       static int initbestp = 0;
//	   form *savecurrent;

//	   savecurrent = current_form;

	   if (best_kopf.waehrung == 0)
	   {
	             pos = GetItemPos (&ubform, "pr_ek");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (EK);
				 }
	             pos = GetItemPos (&ubform, "pr_ek_bto");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (EB);
				 }
	   }
	   else if (best_kopf.waehrung == 1)
	   {
	             pos = GetItemPos (&ubform, "pr_ek");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (EK_DM);
				 }
	             pos = GetItemPos (&ubform, "pr_vk_bto");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (EB_DM);
				 }
	   }
	   else if (best_kopf.waehrung == 2)
	   {
	             pos = GetItemPos (&ubform, "pr_ek");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (EK_EURO);
				 }
	             pos = GetItemPos (&ubform, "pr_ek_bto");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (EB_EURO);
				 }
	   }
	   else if (best_kopf.waehrung == 3)
	   {
	             pos = GetItemPos (&ubform, "pr_ek");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (EK_FREMD);
				 }
	             pos = GetItemPos (&ubform, "pr_ek_bto");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (EB_FREMD);
				 }
	   }
	   else
	   {
	             pos = GetItemPos (&ubform, "pr_ek");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (EK_DM);
				 }
	             pos = GetItemPos (&ubform, "pr_vk_bto");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (EB_DM);
				 }
	   }

	   best_pos.best_term = best_kopf.best_term;
	   best_pos.lief_term = best_kopf.lief_term;
       Geta_bz2_par ();
       Geta_kum_par ();
//       Getbest_me_pr0 ();
       GetWeANeuPar ();
	   GetCfgValues ();
   	   add = FALSE;
	   aufme_old = (double) 0.0;
	   DestroyPlus ();
	   if (FormOK == FALSE)
	   {
           if (DelLadVK)
		   {
		         DelFormField (&ubform, 9);
		         DelFormField (&dataform, 9);
		         DelFormField (&lineform, 8);
		         lineform.mask[lineform.fieldanz - 1].pos[1] =
                             lineform.mask[lineform.fieldanz - 2].pos[1] + 
                             ubform.mask[ubform.fieldanz - 1].length;
		   }
           if (DelLastMe)
		   {
		         DelFormField (&ubform, 5);
		         DelFormField (&dataform, 5);
		         DelFormField (&lineform, 4);
		         lineform.mask[lineform.fieldanz - 1].pos[1] =
                             lineform.mask[lineform.fieldanz - 2].pos[1] + 
                             ubform.mask[ubform.fieldanz - 1].length;
		   }
           if (DelPosWrt)
		   {
			     int upos = GetItemPos (&ubform, "pos_wrt"); 
			     int ipos = GetItemPos (&dataform, "pos_wrt"); 
				 if (upos != -1 && ipos != -1)
				 {
					DelFormField (&ubform, upos);
					DelFormField (&dataform, ipos);
					DelFormField (&lineform, upos - 1);
					lineform.mask[lineform.fieldanz - 1].pos[1] =
                             lineform.mask[lineform.fieldanz - 2].pos[1] + 
                             ubform.mask[ubform.fieldanz - 1].length;
				 }
		   }
		   FormOK = TRUE;
	   }

	   akt_me = (double) 0.0;
	   strcpy (bestps.auf_me, "0");
	   strcpy (bestptab[0].auf_me, "0");
       best_kopf.mdn = mdn;
       best_kopf.fil = fil;
       best_kopf.best_blg = best_blg;
       clipped (lief);
       strcpy (best_kopf.lief, lief);
       best_kopf.best_term = dasc_to_long (bdat);

	   set_fkt (NULL, 8);

       set_fkt (dokey5, 5);
       set_fkt (AppendLine, 6);
       set_fkt (DeleteLine, 7);
       set_fkt (ShowLief, 8);
//       set_fkt (ShowPreise_we, 8);

//       if (rab_prov_kz) set_fkt (PosRab, 8);

       set_fkt (WriteAllPos, 12);
       set_fkt (Schirm, 11);

       SetFkt (6, einfuegen, KEY6);
       SetFkt (7, loeschen, KEY7);
       SetFkt (8, lieferanten, KEY8);
       SetFkt (11, vollbild, KEY11);
       AktRow = 0;
       AktColumn = 0;
       if (initbestp == 0)
       {
                 eListe.SetInfoProc (InfoProc);
                 eListe.SetTestAppend (TestAppend);
                 sprintf (InfoCaption, "Bestellung %ld", best_blg);
                 mamain1 = CreateMainWindow ();
                 eListe.InitListWindow (mamain1);
                 ReadDB (mdn, fil, best_blg);
                 initbestp = 1;
       }

       eListe.SetListFocus (ListFocus);
       eListe.Initscrollpos ();
       scrollpos = 0;
       SetAktivWindow (eListe.Getmamain2 ());
       ShowDB (mdn, fil, best_blg, lief);

       eListe.SetRowItem ("a", bestptab[0].a);
       SetRowItem ();

	   AnzBestWert ();
       ListAktiv = 1;
       eListe.ProcessMessages ();

       InitMax ();
       InitMin ();
       MoveMamain1 ();
       ListAktiv = 0;
       SetFkt (6, leer, NULL);
       SetFkt (7, leer, NULL);
       SetFkt (8, leer, NULL);
       SetFkt (11, leer, NULL);
       set_fkt (NULL, 11);
	   CloseAufw ();
	   DestroySa ();
       eListe.DestroyListWindow ();
       DestroyMainWindow ();
       initbestp = 0;
//	   current_form = savecurrent;
       return;
}

void BESTPLIST::WorkBestp ()
/**
Bestellliste bearbeiten.
**/

{

       beginwork ();

	   set_fkt (NULL, 8);
	   SetFkt (8, leer, NULL);

       set_fkt (dokey5, 5);
       set_fkt (InsertLine, 6);
       set_fkt (DeleteLine, 7);

       set_fkt (WriteAllPos, 12);
       SetFkt (6, einfuegen, KEY6);
       SetFkt (7, loeschen, KEY7);

//       if (rab_prov_kz) SetFkt (8, posrab, KEY8);

       eListe.SetDataForm0 (&dataform, &lineform);
       eListe.SetChAttr (ChAttr); 
       eListe.SetUbRows (ubrows); 
       eListe.SetListFocus (ListFocus);
       eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                             eListe.GetAktColumn ());
       AktRow = 0;
       AktColumn = 0;
       scrollpos = 0;

       SetAktivWindow (eListe.Getmamain2 ());

       ListAktiv = 1;
	   AnzBestWert ();
       eListe.ProcessMessages ();

       commitwork ();
       if (syskey == KEY5)
       {
                  eListe.Initscrollpos ();
                  ShowDB (best_kopf.mdn, best_kopf.fil, best_kopf.best_blg, best_kopf.lief);
       }
       ListAktiv = 0;
       SetFkt (6, leer, NULL);
       SetFkt (7, leer, NULL);
       SetFkt (8, leer, NULL);
       SetFkt (11, leer, NULL);
       set_fkt (NULL, 11);
       return;
}


void BESTPLIST::DestroyMainWindow (void)
/**
Hauptfenster fuer Liste loeschen.
**/
{
        if (mamain1 == NULL) return;
        DestroyWindow (mamain1);
        mamain1 = NULL;
}


HWND BESTPLIST::CreateMainWindow (void)
/**
Hauptfenster fuer Liste erzeugen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;

        if (hMainWindow == NULL) return NULL;    
           
        if (mamain1) return mamain1;

        eListe.GetTextMetric (&tm);

        GetClientRect (hMainWindow, &rect);
        GetWindowRect (hMainWindow, &wrect);


		if (liney > 0)
		{
			y = liney;
		}
		else
		{
            y = (wrect.bottom - 12 * tm.tmHeight);
		}
        x = wrect.left + 2;
        cy = wrect.bottom - y - 2;
        cx = rect.right;

        mamain1 = CreateWindow ("ListMain",
                                    InfoCaption,
                                    WS_THICKFRAME | 
//                                    WS_CAPTION | 
//                                    WS_CHILD |
                                    WS_POPUP |
                                    WS_SYSMENU |
                                    WS_MINIMIZEBOX |
                                    WS_MAXIMIZEBOX,
                                    x, y,
                                    cx, cy,
                                    hMainWindow,
                                    NULL,
                                    hMainInst,
                                    NULL);
        ShowWindow (mamain1, SW_SHOWNORMAL);
        UpdateWindow (mamain1);
        return mamain1;
}

static int IsMax = 0;
static int IsMin = 0;

void BESTPLIST::SetMax0 (int val)
{
    IsMax = val;
    if (IsMax)
    {
             SetFkt (11, fenster, KEY11);
    }
    else
    {
             SetFkt (11, vollbild, KEY11);
    }
}

void BESTPLIST::SetMin0 (int val)
{
    IsMin = val;
}


int BESTPLIST::Schirm (void)
{
         if (IsMin) return 0;
         
         if (IsMax)
         {
             SendMessage (mamain1, WM_SYSCOMMAND, SC_RESTORE, 0l);
             IsMax = 0;
         }
         else
         {
             SendMessage (mamain1, WM_SYSCOMMAND, SC_MAXIMIZE, 0l);
             IsMax = 1;
         }
         return 0;
}


void BESTPLIST::MoveMamain1 ()
{
        RECT rect;
        RECT wrect;
        int x, y, cx, cy;
        TEXTMETRIC tm;

        if (mamainmin) return;
        eListe.GetTextMetric (&tm);
        GetClientRect (hMainWindow, &rect);
        GetWindowRect (hMainWindow, &wrect);
        if (mamainmax)
        {
                 x = wrect.left + 2;
                 y = wrect.top + 2;
                 cy = wrect.bottom - y - 2;
                 cx = rect.right;
        }
        else
        {
 		         if (liney)
				 {
			            y = liney;
				 }
				 else
				 {
                        y = (wrect.bottom - 12 * tm.tmHeight);
				 }
                 x = wrect.left + 2;
                 cy = wrect.bottom - y - 2;
                 cx = rect.right;
        }
        MoveWindow (mamain1, x,y, cx, cy, TRUE);
}


void BESTPLIST::MaximizeMamain1 ()
{
        RECT rect;
        RECT wrect;
        int x, y, cx, cy;

        GetClientRect (hMainWindow, &rect);
        GetWindowRect (hMainWindow, &wrect);
        x = wrect.left + 2;
        y = wrect.top + 2;
        cy = wrect.bottom - y - 2;
        cx = rect.right;
        MoveWindow (mamain1, x,y, cx, cy, TRUE);
}



static char aufwert [20];

static ITEM iaufwert  ("auf_wert",  aufwert,   "Auftragswert   ", 0);

static field _faufwert [] = {
&iaufwert,        14,  0, 1, 1, 0, "%10.2f", READONLY, 0, 0, 0,
};

static form faufwert = {1, 0, 0, _faufwert, 0, 0, 0, 0, NULL};    


void BESTPLIST::MoveAufw (void)
/**
Hauptfenster fuer Liste erzeugen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;

        if (hMainWin == NULL) return;    
        if (AufMehWnd == NULL) return;    
           
	    memcpy (&tm, &textm, sizeof (tm)); 

        GetClientRect (hMainWin, &rect);
        GetWindowRect (hMainWin, &wrect);

/*		
        y = (wrect.top +  2);
        cx = 34 * tm.tmAveCharWidth;
        x = wrect.left + rect.right - cx + 1;
        cy = 4 * tm.tmHeight;
*/

        y = (wrect.top +  2);
        cx = 34 * tm.tmAveCharWidth + 4;
        x = wrect.left + rect.right - cx + 1;
        cy = 4 * tm.tmHeight + 8;
		MoveWindow (AufMehWnd, x, y, cx, cy, TRUE);
}


HWND BESTPLIST::CreateAufw (void)
/**
Hauptfenster fuer Liste erzeugen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;
        HDC hdc;
		HWND eWindow;

        if (hMainWin == NULL) return NULL;    
           
	    memcpy (&tm, &textm, sizeof (tm)); 

        GetClientRect (hMainWin, &rect);
        GetWindowRect (hMainWin, &wrect);

/*		
        y = (wrect.top +  2);
        cx = 34 * tm.tmAveCharWidth;
        x = wrect.left + rect.right - cx + 1;
        cy = 4 * tm.tmHeight;
*/

        y = (wrect.top +  2);
        cx = 34 * tm.tmAveCharWidth + 4;
        x = wrect.left + rect.right - cx + 1;
        cy = 4 * tm.tmHeight + 8;

        eWindow       = CreateWindow (
                                       "StaticWhite",
                                       "",
                                       WS_DLGFRAME | 
                                       WS_POPUP,
                                       x, y,
                                       cx, cy,
                                       hMainWin,
                                       NULL,
                                       hMainInst,
                                    NULL);
        ShowWindow (eWindow, SW_SHOWNORMAL);
        UpdateWindow (eWindow);

        AufMehWnd0      = CreateWindowEx (
			                           WS_EX_CLIENTEDGE, 
                                       "StaticWhite",
                                       "",
									   WS_CHILD | WS_VISIBLE,
                                       2, 4,
                                       cx - 11, cy - 14,
                                       eWindow,
                                       NULL,
                                       hMainInst,
                                       NULL);

        hdc = GetDC (eWindow);
//        ChoiseLines (eWindow, hdc);
        ReleaseDC (eWindow, hdc);
        return eWindow;
}


void BESTPLIST::CloseAufw (void)
{
        if (auf_wert_anz == 0) return;

        if (AufMehWnd)
		{
			CloseControls (&faufwert);
            if (AufMehWnd0)
			{
			         DestroyWindow (AufMehWnd0);
 			         AufMehWnd0 = NULL;
			}
			DestroyWindow (AufMehWnd);
			AufMehWnd = NULL;
		}
}
			

void BESTPLIST::AnzBestWert (void)
/**
Auftragswert anzeigen.
**/
{
          int currentf;
		  int i;
		  double wert;
		  int recanz;

      
          if (auf_wert_anz == 0) return;

		  recanz = eListe.GetRecanz ();
		  if (recanz == 0)
		  {
			  if (AufMehWnd) 
			  {
				  DestroyWindow (AufMehWnd);
				  AufMehWnd = NULL;
			  }
			  return;
		  }
          if (AufMehWnd == NULL)
		  {
		            AufMehWnd = CreateAufw ();
					if (AufMehWnd == NULL) return;
		  }
		  wert = 0;
/*
		  for (i = 0; i < recanz; i ++)
		  {
			      wert = wert + ratod (bestptab[i].lief_me) *
					            ratod (bestptab[i].pr_ek_bto);
          }
*/

		  for (i = 0; i < recanz; i ++)
		  {
           if (bestptab[i].me_kz[0] == '1' && !PrInBestEinh)
           {
			      wert = wert + ratod (bestptab[i].auf_me) *
					            ratod (bestptab[i].pr_ek) * bestptab[i].inh;
		   }
		   else
		   {
			      wert = wert + ratod (bestptab[i].auf_me) *
					            ratod (bestptab[i].pr_ek);
		   }
          }
		  SetStaticWhite (TRUE);
		  sprintf (aufwert, "%10.2lf", wert);
		  display_form (AufMehWnd0, &faufwert, 0, 0); 
		  SetStaticWhite (FALSE);

          currentfield = currentf;
}
      

HWND BESTPLIST::GetMamain1 (void)
{
       return (mamain1);
}

void BESTPLIST::SethwndTB (HWND hwndTB)
{
         this->hwndTB = hwndTB; 
         eListe.SethwndTB (hwndTB);
}

void BESTPLIST::SetTextMetric (TEXTMETRIC *tm)
{
         memcpy (&textm, tm, sizeof (TEXTMETRIC));
         eListe.SetTextMetric (tm);
}


void BESTPLIST::SetLineRow (int LineRow)
{
         eListe.SetLineRow (0);
}

void BESTPLIST::SetListLines (int i)
{ 
//         TListe.SetListLines (i);
         eListe.SetListLines (i);
}

void BESTPLIST::OnPaint (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	    HDC hdc;

        TListe.OnPaint (hWnd, msg, wParam, lParam);
        eListe.OnPaint (hWnd, msg, wParam, lParam);
        if (hWnd == eWindow)  
        {
                    hdc = BeginPaint (eWindow, &aktpaint);
                    ChoiseLines (eWindow, hdc); 
                    EndPaint (eWindow, &aktpaint);
        }
        else if (hWnd == AufMehWnd)  
        {
                    hdc = BeginPaint (AufMehWnd, &aktpaint);
//                    ChoiseLines (AufMehWnd, hdc); 
                    EndPaint (AufMehWnd, &aktpaint);
        }
        else if (hWnd == BasishWnd)  
        {
                    hdc = BeginPaint (BasishWnd, &aktpaint);
                    ChoiseLines (BasishWnd, hdc); 
                    EndPaint (BasishWnd, &aktpaint);
        }
        else if (hWnd == SaWindow)  
        {
                    hdc = BeginPaint (hWnd, &aktpaint);
                    PaintSa (hdc); 
                    EndPaint (hWnd, &aktpaint);
        }
        else if (hWnd == PlusWindow)  
        {
                    hdc = BeginPaint (hWnd, &aktpaint);
                    PaintPlus (hdc); 
                    EndPaint (hWnd, &aktpaint);
        }
}

MTXT *BESTPLIST::Mtxt;
long BESTPLIST::TextNr;

void BESTPLIST::GetKopfTexte (void)
/**
Texte aus EditFeld holen.
**/
{
	   char *Text;
	   char *txt;
	   static char seps [] = "\n";


	   Text = Mtxt->GetText ();
	   txt = strtok (Text, seps);
	   while (txt)
	   {
		   disp_mess (txt, 2);
	       txt = strtok (NULL, seps);
	   }
}

int BESTPLIST::ReadTxt (long txtnr)
{

       BESTPT_CLASS bestpt_class;
	   int dsqlstatus;
	   
	   Mtxt->InitText ();
	   bestpt.nr = txtnr;
       dsqlstatus = bestpt_class.dbreadfirst ();
	   while (dsqlstatus == 0)
	   {
		    clipped (bestpt.txt);
			Mtxt->AddText (bestpt.txt);
			dsqlstatus = bestpt_class.dbread ();
	   }
	   Mtxt->SetText ();

	   return 0;
}



int BESTPLIST::CutLines (char *txt, int zei, BESTPT_CLASS *bestpt_class)
{
	   char *params [5];
	   char zeile[5];
	   char tlen[5];

	   if (strlen (txt) <= TLEN)
	   {
		   bestpt.zei = zei;
		   cr_weg (txt);
		   strcpy (bestpt.txt, txt);
           bestpt_class->dbupdate ();
		   return (zei + 1);
	   }

	   params[0] = txt;
	   params[1] = bestpt.txt;
	   sprintf (zeile, "1");
	   sprintf (tlen, "%d", TLEN);
	   params[2] = tlen;
	   params[3] = zeile;
	   params[4] = NULL;
	   while (getline (params))
	   {
		   bestpt.zei = zei;
		   cr_weg (bestpt.txt);
           bestpt_class->dbupdate ();
		   zei ++;
		   sprintf (zeile, "%d", atoi (zeile) + 1);
	   }
	   return zei;
}


int BESTPLIST::WriteTxt (long txtnr)
{
       BESTPT_CLASS bestpt_class;
	   int zei;
	   char *p;
	   char txt [512];
	   
	   zei = 1;
	   bestpt.nr = txtnr;
	   bestpt_class.dbreadfirst ();
       bestpt_class.delete_bestpposi ();
	   p = Mtxt->FirstRow (txt, 511);
       while (p)
	   {
		   clipped (txt);
		   zei = CutLines (txt, zei, &bestpt_class);
	       p = Mtxt->NextRow (txt, 511);
		   zei ++;
	   }
	   return 0;
}


void BESTPLIST::InputTxt (char *Label, long text_nr)
{
  	  int cx, cy;
	  form *scurrent;
	  char textnr [10];

	  scurrent = current_form;
      cx = 80;
      cy = 20;
      Mtxt = new MTXT (cx, cy, Label, "", ReadTxt);
	  Mtxt->SetTextNr (LongToChar (textnr, "%ld", text_nr));
      Mtxt->OpenWindow (hMainInst, hMainWin);
	  EnableWindow (hMainWin, FALSE);
      ReadTxt (text_nr); 

	  if (Mtxt->ProcessMessages ())
	  {
		  WriteTxt (atol (Mtxt->GetTextNr ()));
	  }

	  SetActiveWindow (hMainWin);
	  EnableWindow (hMainWin, TRUE);
      Mtxt->DestroyWindow ();
	  delete Mtxt;
	  current_form = scurrent;
}



void BESTPLIST::MoveListWindow (void)
{
        TListe.MoveListWindow ();
        eListe.MoveListWindow ();
}


void BESTPLIST::BreakList (void)
{
        eListe.BreakList ();
}


void BESTPLIST::OnHScroll (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
        TListe.OnHScroll (hWnd, msg,wParam, lParam);
        eListe.OnHScroll (hWnd, msg,wParam, lParam);
}

void BESTPLIST::OnVScroll (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
        TListe.OnVScroll (hWnd, msg,wParam, lParam);
        eListe.OnVScroll (hWnd, msg,wParam, lParam);
}

void BESTPLIST::OnSize (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
        if (hWnd == GetMamain1 ())
        {
                    MoveListWindow ();
                    MoveAufw ();
                    MoveSaW ();
                    MovePlus ();
        }
        else if (hWnd == TListe.GetMamain1 ())
        {
                    TListe.MoveListWindow ();
        }
}


void BESTPLIST::StdAngebot (void)
{
         if (eListe.Getmamain3 () == NULL) return; 
//         doStd ();
}

void BESTPLIST::FunkKeys (WPARAM wParam, LPARAM lParam)
{
	                if (TListe.Getmamain3())
					{
                            TListe.FunkKeys (wParam, lParam);
					}
					else
					{
                            eListe.FunkKeys (wParam, lParam);
					}
}


int BESTPLIST::GetRecanz (void)
{
          return eListe.GetRecanz ();
}


void BESTPLIST::SwitchPage0 (int rows)
{
           eListe.SwitchPage0 (rows);
}


HWND BESTPLIST::Getmamain2 (void)
{
           return eListe.Getmamain2 ();
}


HWND BESTPLIST::Getmamain3 (void)
{
           return eListe.Getmamain3 ();
}


void BESTPLIST::ChoiseFont (mfont *lfont)
{
           eListe.ChoiseFont (lfont);
}

void BESTPLIST::SetFont (mfont *lfont)
{
           TListe.SetFont (lfont);
           eListe.SetFont (lfont);
}

void BESTPLIST::SetListFont (mfont *lfont)
{
           TListe.SetListFont (lfont);
           eListe.SetListFont (lfont);
}

void BESTPLIST::FindString (void)
{
           eListe.FindString ();
}


void BESTPLIST::SetLines (int Lines)
{
           TListe.SetLines (Lines);
           eListe.SetLines (Lines);
}


int BESTPLIST::GetAktRow (void)
{
                 return eListe.GetAktRow ();
}

int BESTPLIST::GetAktRowS (void)
{
                 return eListe.GetAktRowS ();
}

void BESTPLIST::SetColors (COLORREF Color, COLORREF BkColor)
{
                 TListe.SetColors (Color, BkColor); 
                 eListe.SetColors (Color, BkColor); 
}

void BESTPLIST::SetListFocus (void)
{
                 eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                                       eListe.GetAktColumn ()); 
}

void BESTPLIST::PaintUb (void)
{
                 eListe.PaintUb (); 
}
void BESTPLIST::FuellePreise_we (void)
/**
preise_we f�llen, falls leer
**/
{
   int cursork, cursorp, cursorwek, cursorwep;
   long we_dat,best_term, best_blg;
   double pr_ek_w, pr_ek_b;
   short me_einh_w, me_einh_b;
   char datum [12];
   char lief_rech_nr [16];
   long ddatum ; 

	if (DbClass.sqlcomm ("select a from preise_we ") == 100)
	{
		sysdate (datum);
		ddatum = dasc_to_long (datum) - AnzTagePreise  ;

	   DbClass.sqlin ((short *) &best_kopf.mdn, 1, 0);
	   DbClass.sqlin ((short *) &best_kopf.fil, 1, 0);
	   DbClass.sqlin ((long *)  &ddatum, 2, 0);
	   DbClass.sqlout ((long *) &best_blg, 2, 0);
	   DbClass.sqlout ((long *) &best_term, 2, 0);
	   DbClass.sqlout ((char *) preise_we.lief, 0, 16);
	   cursork = DbClass.sqlcursor ("select best_blg, best_term,lief from best_kopf where mdn = ? "
		                                                  "and fil = ? "
														  "and best_term  > ? "
														  "order by best_term desc, "
														  "best_blg desc");

	   DbClass.sqlin ((short *) &best_kopf.mdn, 1, 0);
	   DbClass.sqlin ((short *) &best_kopf.fil, 1, 0);
	   DbClass.sqlin ((long *)  &best_blg, 2, 0);
	   DbClass.sqlout ((double *) &preise_we.a, 3, 0);
	   DbClass.sqlout ((double *) &pr_ek_b, 3, 0);
	   DbClass.sqlout ((short *) &me_einh_b, 1, 0);
	   cursorp = DbClass.sqlcursor ("select a,pr_ek,me_einh from best_pos "
		                            "where mdn = ? "
									"and fil = ? "
									"and best_blg = ? ");

       while (DbClass.sqlfetch (cursork) == 0)
	   {
		   if (DbClass.sqlopen (cursorp)) break;
		   while (DbClass.sqlfetch (cursorp) == 0)
		   {
				preise_we.mdn = best_kopf.mdn;
				preise_we.best_blg = best_blg;
				strcpy(preise_we.lief_rech_nr,"");
				if (preise_we_class.dbreadfirst () != 0)
				{
					preise_we.best_blg = best_blg;
					strcpy (preise_we.lief, best_kopf.lief);
					strcpy (preise_we.lief_rech_nr,"");
					preise_we.we_dat = 0;
					preise_we.pr_ek_w = 0;
					preise_we.me_einh_w = 0;
				}
			
				preise_we.best_term = best_term;
				preise_we.pr_ek_b = pr_ek_b;
				preise_we.me_einh_b = me_einh_b;
				preise_we_class.dbupdate ();
		   }
	   }

	   DbClass.sqlin ((short *) &best_kopf.mdn, 1, 0);
	   DbClass.sqlin ((short *) &best_kopf.fil, 1, 0);
	   DbClass.sqlin ((long *)  &ddatum, 2, 0);

	   DbClass.sqlout ((char *) &lief_rech_nr, 0, 16);
	   DbClass.sqlout ((long *) &preise_we.best_blg, 2, 0);
	   DbClass.sqlout ((long *) &we_dat, 2, 0);
	   DbClass.sqlout ((char *) preise_we.lief, 0, 16);
	   cursorwek = DbClass.sqlcursor ("select lief_rech_nr, koresp_best0, we_dat,lief from we_kopf where mdn = ? "
		                                                  "and fil = ? "
														  "and we_dat  > ? and blg_typ <> \"X\" "
														  "order by we_dat desc, "
														  "lief_rech_nr desc");

	   DbClass.sqlin ((short *) &best_kopf.mdn, 1, 0);
	   DbClass.sqlin ((short *) &best_kopf.fil, 1, 0);
	   DbClass.sqlin ((char *)  &lief_rech_nr, 0, 16);
	   DbClass.sqlin ((char *) preise_we.lief, 0, 16);

	   DbClass.sqlout ((double *) &preise_we.a, 3, 0);
	   DbClass.sqlout ((double *) &pr_ek_w, 3, 0);
	   DbClass.sqlout ((short *) &me_einh_w, 1, 0);
	   cursorwep = DbClass.sqlcursor ("select a,pr_ek,me_einh from we_jour "
		                            "where mdn = ? "
									"and fil = ? "
									"and lief_rech_nr = ? "
									"and lief = ?");

       while (DbClass.sqlfetch (cursorwek) == 0)
	   {
		   if (DbClass.sqlopen (cursorwep)) break;
		   while (DbClass.sqlfetch (cursorwep) == 0)
		   {
				preise_we.mdn = best_kopf.mdn;
				strcpy (preise_we.lief_rech_nr,lief_rech_nr);
				if (preise_we_class.dbreadfirst () != 0)
				{
					preise_we.best_blg = 0;
					preise_we.best_term = 0;
					preise_we.pr_ek_b = 0;
					preise_we.me_einh_b = 0;
				}
			
				strcpy (preise_we.lief_rech_nr,lief_rech_nr);
				preise_we.we_dat = we_dat;
				preise_we.pr_ek_w = pr_ek_w;
				preise_we.me_einh_w = me_einh_w;
				preise_we_class.dbupdate ();
				preise_we.best_blg = 0;
		   }
	   }

	   DbClass.sqlclose (cursorp);
	   DbClass.sqlclose (cursork);
	   DbClass.sqlclose (cursorwep);
	   DbClass.sqlclose (cursorwek);
	}
}
