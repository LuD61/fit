#ifndef _AKTIONDLG_DEF
#define _AKTIONDLG_DEF
#include "dlg.h"
#include "itprog.h"
#include "itfont.h"
#include "formfield.h"

#define NORMALPREIS_CTL 2001 
#define AKTIONPREIS_CTL 2002
#define CHNORMAL_CTL 2003 
#define CHAKTION_CTL 2004 

#ifndef HEADCTL
#define HEADCTL 3001
#endif


#define RAISEDBORDER 0
#define HIGHBORDER 1
#define LOWBORDER 2
#define HIGHCOLBORDER 3
#define LOWCOLBORDER 4
#define LINEBORDER 5


class AktionDlg : virtual public DLG 
{
          private :
             static mfont *Font;
             static CFIELD *_fHead [];
             static CFIELD *_fDlg [];
             static CFORM fHead;
             static CFIELD *_fFoot [];
             static CFORM fFoot;
             static CFORM fDlg;
             static CFIELD *_fDlg0[]; 
             static CFORM fDlg0;

             static ItProg *After [];
             static ItProg *Before [];
             static ItFont *Fonts [];
             static FORMFIELD *dbfields [];
             static char *EnableHead[];
             static AktionDlg *ActiveAktionDlg; 
             static char NormalPreis [];
             static char AktionPreis [];
             static char ChNormal [];
             static char ChAktion [];
             static char *HelpName;

          public :
            static COLORREF SysBkColor;

            AktionDlg (int, int, int, int, char *, int, BOOL);
            void Init (int, int, int, int, char *, int, BOOL);
/*
            BOOL OnKeyDown (void);
            BOOL OnKeyUp (void);
            BOOL OnKey1 (void);
            BOOL OnKey2 (void);
            BOOL OnKey3 (void);
            BOOL OnKey4 (void);
            BOOL OnKey6 (void);
            BOOL OnKey7 (void);
            BOOL OnKey9 (void);
            BOOL OnKey10 (void);
*/
            BOOL OnKey5 (void);
            BOOL OnKey12 (void);
            BOOL OnKeyReturn (void);
/*
            BOOL OnKeyDelete (void);
            BOOL OnKeyPrior (void);
            BOOL OnKeyNext (void);
            BOOL OnKeyHome (void);
            BOOL OnKeyEnd (void);
            BOOL OnPaint (HWND,HDC,UINT,WPARAM,LPARAM);
*/
            BOOL OnActivate (HWND,UINT,WPARAM,LPARAM);
            BOOL OnCommand (HWND,UINT,WPARAM,LPARAM);
            BOOL OnSysCommand (HWND,UINT,WPARAM,LPARAM);

//            BOOL OnDestroy (HWND,UINT,WPARAM,LPARAM);
            void PrintComment (char *);

            void SetWinBackground (COLORREF);

            void ToClipboard (void);
            void FromClipboard (void);
            void Help (void);
            void CallInfo (void);
            void SetPreise (double, double);
            double GetPreis (void);  
};
#endif
