#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include "comcthlp.h"
#include "itemc.h"
#include "wmaskc.h"
#include "mo_meld.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_intp.h"
#include "listcl.h"
#include "inflib.h"
#include "mo_arg.h"
#include "mo_curso.h"
#include "ls.h"
#include "a_bas.h"
#include "mdn.h"
#include "fil.h"
#include "bestpt.h"
#include "mo_atxtl.h"
#include "sys_par.h"

#define MAXLEN 40
#define MAXPOS 3000
#define LPLUS 1

#define HNDW 1
#define EIG 2
#define EIG_DIV 3


static mfont txtfont = {
	           "Courier New", 
//	           "MS SAN SERIF", 
                90, 100, 0,
                BLACKCOL,
                LTGRAYCOL,
                0,
                NULL};


extern HANDLE  hMainInst;

static unsigned char ausgabebuffer [0x1000];
static HWND mamain1;
static HWND eWindow;

static int ListFocus = 4;

struct AUFP_TXTS 
{
       char zei [80];
       char txt [80];
};

struct AUFP_TXTS aufp_txts, aufp_txttab [MAXPOS];

ITEM izei         ("zei",        aufp_txts.zei,             "", 0);
ITEM itxt         ("txt",        aufp_txts.txt,             "", 0);

static field  _dataform[] = {
&itxt,        60, 0, 0, 1, 0, "",        EDIT,        0, 0, 0, 
};

static form dataform = {1, 0, 0, _dataform, 0, 0, 0, 0, NULL}; 

static int ubrows [] = {0}; 




ITEM iutxt         ("txt",         "Positionstext",            "", 0);
ITEM iuspace       ("space",       " ",            "", 0);

static field  _ubform[] = {
&iutxt,        61, 1, 0, 0, 0, "",  BUTTON, 0, 0, 0, 
&iuspace,     260, 1, 0,61, 0, "",  BUTTON, 0, 0, 0, 
};


static form ubform = {2, 0, 0, _ubform, 0, 0, 0, 0, NULL}; 

static ITEM iline ("", "1", "", 0);

static field  _lineform[] = {
&iline,      1, 0, 0,66, 0, "",  NORMAL, 0, 0, 0, 
};

static form lineform = {1, 0, 0, _lineform, 0, 0, 0, 0, NULL}; 

static char TextNr [10];

static ITEM iNummer ("txt_nr", TextNr,   "Text_nr...:", 0);

static field _ftext [] = {
&iNummer,     9,  0, 1, 1, 0, "", EDIT, 0, 0, 0
};

static form ftext = {1, 0, 0, _ftext, 0, 0, 0, 0, NULL};    

static int auf_text_par;
static int wa_pos_txt;

static ListClassDB eListe;
static SYS_PAR_CLASS sys_par_class;
static BESTPT_CLASS bestpt_class;


AUFPTLIST:: AUFPTLIST ()
{
    mamainmax = 0;
    mamainmin = 0; 
    eingabesatz = NULL;
    ausgabesatz = ausgabebuffer;
    zlen = 0;
    feld_anz = 0;
    banz = 0;
    PageView = 0;
    LstZiel = NULL;
    LstSatz   = NULL;
    Lstbanz = 0;
    SwRecs = 0;
    AktRow = 0;
    AktColumn = 0;
    scrollpos = 0;
    InfoCaption[0] = (char) 0; 
    WithMenue   = 1;
    WithToolBar = 1;
    dataform.after  = WriteRow; 
    ListAktiv = 0;
    this->hMainWindow = NULL;
}

void AUFPTLIST::SetFieldAttr (char *fname, int attr)
{
         int i;
         char *feldname;

         for (i = 0; i < dataform.fieldanz; i ++)
         {
             feldname = dataform.mask[i].item->GetItemName ();
             if (feldname && strcmp (feldname, fname) == 0)
             {
                   break;
             }
         }
         if (i == dataform.fieldanz) return;

         dataform.mask[i].attribut = attr;
}

int AUFPTLIST::TestAppend (void)
/**
Testen, ob ein Satz angehaengt werden darf.
**/
{
    eListe.SetPos (eListe.GetAktRow (), eListe.FirstColumn ());
    return TRUE;
}

int AUFPTLIST::DeleteLine (void)
/**
Zeile aus Liste loeschen.
**/
{
        eListe.DeleteLine ();
        return 0;
}

int AUFPTLIST::InsertLine (void)
/**
Zeile aus Liste loeschen.
**/
{
        eListe.SetPos (eListe.GetAktRow (), eListe.FirstColumn ());
        eListe.InsertLine ();
        return 0;
}

int AUFPTLIST::AppendLine (void)
/**
Zeile aus Liste loeschen.
**/
{
        eListe.AppendLine ();
        return 0;
}

int AUFPTLIST::WriteRow (void)
/**
Zeile beim Verlassen pruefen.
**/
{
    char str [61];
    
    memset (str, ' ', 60);
    str[60] = (char) 0;
/*
    if (strcmp (aufp_txts.txt, str) <= 0 &&
        syskey == KEYUP)
    {
        eListe.DeleteLine ();
        return (1);
    }
*/
    return (0);
}

int AUFPTLIST::TestRow (void)
/**
Zeile beim Verlassen pruefen.
**/
{
    set_fkt (NULL, 9);
    SetFkt (9, leer, NULL);
    return (0);
}

void AUFPTLIST::WritePos (int pos)
/**
Position schreiben.
**/
{
      bestpt.zei       = (long) pos;
      strcpy (bestpt.txt, aufp_txttab[pos].txt);
      bestpt_class.dbupdate ();
}


int AUFPTLIST::WriteAllPos (void)
/**
Alle Positionen schreiben.
**/
{
    int i;
    int recs;
    int row;
    char str [61];
    
    memset (str, ' ', 60);
    str[60] = (char) 0;

    row     = eListe.GetAktRow ();

    if (TestRow () == -1)
    {
            return -1;
    }

    memcpy (&aufp_txttab[row], &aufp_txts, sizeof (struct AUFP_TXTS));
    recs = eListe.GetRecanz ();
	for (i = recs; i > 0; i --)
	{
		if (strcmp (aufp_txttab[i - 1].txt, str)) break;

	}

	recs = i;
    bestpt_class.delete_bestpposi ();
    for (i = 0; i < recs - 1; i ++)
    {
        WritePos (i);
    }
    if (strcmp (aufp_txttab[i].txt, str))
    {
        WritePos (i);
    }
    eListe.BreakList ();
    return 0;
}


void AUFPTLIST::SaveAuf (void)
{
    save_fkt (5);
    save_fkt (6);
    save_fkt (7);
    save_fkt (8);
    save_fkt (12);
}

void AUFPTLIST::SetAuf (void)
{
    set_fkt (dokey5, 5);
    set_fkt (InsertLine, 6);
    set_fkt (DeleteLine, 7);
    set_fkt (AppendLine, 8);
    set_fkt (WriteAllPos, 12);
}

void AUFPTLIST::RestoreAuf (void)
{
    restore_fkt (5);
    restore_fkt (6);
    restore_fkt (7);
    restore_fkt (8);
    restore_fkt (12);
}

int AUFPTLIST::dokey5 (void)
/**
Listenerfassung abbrechen.
**/
{
    if (abfragejn (mamain1, "Texte speichern ?", "J"))
    {
           WriteAllPos ();
           syskey = KEY12;
    }
    else
    {
           syskey = KEY5;
    }
    RestoreAuf ();

    eListe.SetListFocus (0);
    eListe.SetFeldFocus0 (eListe.GetAktRow (), eListe.GetAktColumn ());

    eListe.BreakList ();
    return 1;
}


void AUFPTLIST::InitSwSaetze (void)
{
       int i;

       for (i = 0; i < MAXPOS; i ++)
       {
               SwSaetze [i] = (char *) &aufp_txttab [i];
       }
}

void AUFPTLIST::SetRecHeight (void)
/**
Anzahl Zeilen pro Listzeile ermitteln.
**/
{
       int height;
       int i;

       height = 0;
       for (i = 0; i < dataform.fieldanz; i ++)
       {
           if (dataform.mask[i].attribut & REMOVED) continue;
           if (dataform.mask[i].pos[0] > height)
           {
               height = dataform.mask[i].pos[0];
           }
       }
       height ++; 
       eListe.SetRecHeight (height);
}

int AUFPTLIST::ToMemory (int pos)
/**
SW-Satz in Ascii-Format Speichern.
**/
{

       memcpy (SwSaetze[pos], ausgabesatz, zlen);
       eListe.SetRecanz (pos + 1);
       return 0;
}


void AUFPTLIST::SetStringEnd (char *feldname, int len)
/**
Stringende Setzen.
**/
{

       return;
}


void AUFPTLIST::uebertragen (void)
/**
Eingabesatz in ASCII-Striktur uebertragen.
**/
{
         sprintf (aufp_txts.zei,     "%8ld",    bestpt.zei);
         sprintf (aufp_txts.txt,     "%s",      bestpt.txt);
}

void AUFPTLIST::ShowDB (void)
/**
Auftragspositionen lesen und anzeigen.
**/
{
        int i;
        HCURSOR oldcursor;
        extern short do_exit;
        int dsqlstatus;


        InitSwSaetze ();
        eListe.SetRecanz (0);
        i = eListe.GetRecanz ();
        eListe.SetUbForm (&ubform);
        oldcursor = SetCursor (LoadCursor (NULL, IDC_WAIT));
        dsqlstatus = bestpt_class.dbreadfirst ();
        while (dsqlstatus == 0)
        {
                     uebertragen ();
                     if (ToMemory (i) != 0) break;
                     i = eListe.GetRecanz ();
                     if (i >= MAXPOS) break; 
                     dsqlstatus = bestpt_class.dbread ();
        }

        SetRecHeight ();
        SetCursor (oldcursor);
        i = eListe.GetRecanz ();

        eListe.SetRecanz (i);
        SwRecs = eListe.GetRecanz ();
 
        eListe.SetDataForm0 (&dataform, &lineform);
        eListe.SetSaetze (SwSaetze);
//        eListe.SetChAttr (ChAttr); 
        eListe.SetUbRows (ubrows); 
        if (i == 0)
        {
                 eListe.AppendLine ();
                 i = eListe.GetRecanz ();
        }
	    SetFont (&txtfont);
        SendMessage (eListe.Getmamain2 (), WM_SIZE, NULL, NULL);
        InvalidateRect (eListe.Getmamain3 (), 0, TRUE);
        memcpy (&aufp_txts, &aufp_txttab[0], sizeof (struct AUFP_TXTS));
}

void AUFPTLIST::ReadDB (void)
/**
Auftragspositionen lesen.
**/
{
        ausgabesatz = (unsigned char *) &aufp_txts;
        zlen = sizeof (struct AUFP_TXTS);

        PageView = 0;
        eListe.SetPageView (0);
        feld_anz = dataform.fieldanz;
        Lstzlen = zlen;
        LstSatz = ausgabesatz;

        Lstbanz = feld_anz;

        eListe.SetPos (AktRow, AktColumn);
        eListe.Setbanz (feld_anz);
        eListe.Setzlen (zlen);
        eListe.Initscrollpos ();
        eListe.SetAusgabeSatz (ausgabesatz);
}

void AUFPTLIST::DestroyWindows (void)
{
       eListe.DestroyListWindow ();
       DestroyMainWindow ();
       InitMax ();
       InitMin ();
       MoveMamain1 ();
       ListAktiv = 0;
       SetFkt (11, leer, NULL);
       set_fkt (NULL, 11);
}


long AUFPTLIST::EnterNr (long nr)
/**
Nummer Eingeben.
**/
{
          sprintf (TextNr, "%ld", nr);
          break_end ();
          enter_form (eWindow, &ftext, 0, 0);
          display_form (eWindow, &ftext, 0, 0);
          no_break_end ();
          return atol (TextNr);
}
      

void AUFPTLIST::EnterAufp (short mdn, short fil, long auf, long posi,
                           long nr)
/**
Auftragsliste bearbeiten.
**/

{
       static int initaufp = 0;

       strcpy (sys_par.sys_par_nam,"auf_text_par");
       if (sys_par_class.dbreadfirst () == 0)
	   {
                 auf_text_par = atoi (sys_par.sys_par_wrt); 
	   }
       strcpy (sys_par.sys_par_nam,"wa_pos_txt");
       if (sys_par_class.dbreadfirst () == 0)
	   {
                 wa_pos_txt = atoi (sys_par.sys_par_wrt); 
	   }

       eListe.SetListclipp (TRUE);
       eListe.SetSetsel (FALSE);
       eListe.SetFocusframe (FALSE);
	   eListe.SetNoRecNr (TRUE);
       bestpt.nr = nr;
       set_fkt (dokey5, 5);
       set_fkt (InsertLine, 6);
       set_fkt (DeleteLine, 7);
       set_fkt (AppendLine, 8);
       set_fkt (WriteAllPos, 12);

       SetFkt (6, einfuegen, KEY6);
       SetFkt (7, loeschen, KEY7);
       SetFkt (8, anfuegen, KEY8);

       if (initaufp == 0)
       {
                 eListe.SetTestAppend (TestAppend);
                 sprintf (InfoCaption, "Auftrag %ld", auf);
                 mamain1 = CreateMainWindow ();
                 eListe.InitListWindow (mamain1);
                 ReadDB ();
                 initaufp = 1;
       }

        
       eListe.SetListFocus (ListFocus);
       eListe.Initscrollpos ();
       AktRow = 0;
       AktColumn = 0;
       scrollpos = 0;
       SetAktivWindow (eListe.Getmamain2 ());
       ShowDB ();
     
       ListAktiv = 1;
       eListe.ProcessMessages ();

       InitMax ();
       InitMin ();
       MoveMamain1 ();
       ListAktiv = 0;
       SetFkt (6, leer, NULL);
       SetFkt (7, leer, NULL);
       SetFkt (8, leer, NULL);
       SetFkt (11, leer, NULL);
       set_fkt (NULL, 11);
       eListe.DestroyListWindow ();
       DestroyMainWindow ();
       CloseControls (&ftext);
       initaufp = 0;
	   eListe.SetNoRecNr (FALSE);
       return;
}


void AUFPTLIST::DestroyMainWindow (void)
/**
Hauptfenster fuer Liste loeschen.
**/
{
        if (mamain1 == NULL) return;
        DestroyWindow (mamain1);
        mamain1 = NULL;
        if (eWindow == NULL) return;
        DestroyWindow (eWindow);
        eWindow = NULL;
}

HWND AUFPTLIST::CreateEnter (void)
/**
Hauptfenster fuer Liste erzeugen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;

        if (hMainWindow == NULL) return NULL;    
           
        if (eWindow) return eWindow;

        eListe.GetTextMetric (&tm);

        GetClientRect (hMainWindow, &rect);
        GetWindowRect (hMainWindow, &wrect);


        y = (wrect.bottom - 16 * tm.tmHeight);
        x = wrect.left + 2 + 4 * tm.tmAveCharWidth;
        cy = 4 * tm.tmHeight;
        cx = rect.right - 8 * tm.tmAveCharWidth;

        eWindow       = CreateWindow (
                                       "ListMain",
//                                       "hListWindow", 
                                       InfoCaption,
                                       WS_DLGFRAME | 
                                       WS_POPUP,
                                       x, y,
                                       cx, cy,
                                       hMainWindow,
                                       NULL,
                                       hMainInst,
                                    NULL);
        ShowWindow (eWindow, SW_SHOWNORMAL);
        UpdateWindow (eWindow);
        return eWindow;
}


HWND AUFPTLIST::CreateMainWindow (void)
/**
Hauptfenster fuer Liste erzeugen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;

        if (hMainWindow == NULL) return NULL;    
           
        if (mamain1) return mamain1;

        eListe.GetTextMetric (&tm);

        GetClientRect (hMainWindow, &rect);
        GetWindowRect (hMainWindow, &wrect);


        y = (wrect.bottom - 12 * tm.tmHeight);
        x = wrect.left + 2 + 4 * tm.tmAveCharWidth;
        cy = wrect.bottom - y - 2;
        cx = rect.right - 8 * tm.tmAveCharWidth;

        mamain1 = CreateWindow ("ListMain",
                                    InfoCaption,
                                    WS_THICKFRAME | 
//                                    WS_CAPTION | 
//                                    WS_CHILD |
                                    WS_POPUP,
//                                    WS_SYSMENU |
//                                    WS_MINIMIZEBOX |
//                                    WS_MAXIMIZEBOX,
                                    x, y,
                                    cx, cy,
                                    hMainWindow,
                                    NULL,
                                    hMainInst,
                                    NULL);
        ShowWindow (mamain1, SW_SHOWNORMAL);
        UpdateWindow (mamain1);
        return mamain1;
}

static int IsMax = 0;
static int IsMin = 0;

void AUFPTLIST::SetMax0 (int val)
{
    IsMax = val;
    if (IsMax)
    {
             SetFkt (11, fenster, KEY11);
    }
    else
    {
             SetFkt (11, vollbild, KEY11);
    }
}

void AUFPTLIST::SetMin0 (int val)
{
    IsMin = val;
}


int AUFPTLIST::Schirm (void)
{
         if (IsMin) return 0;
         
         if (IsMax)
         {
             SendMessage (mamain1, WM_SYSCOMMAND, SC_RESTORE, 0l);
             IsMax = 0;
         }
         else
         {
             SendMessage (mamain1, WM_SYSCOMMAND, SC_MAXIMIZE, 0l);
             IsMax = 1;
         }
         return 0;
}

void AUFPTLIST::MoveeWindow ()
{
        RECT rect;
        RECT wrect;
        int x, y, cx, cy;
        TEXTMETRIC tm;

        if (mamainmin) return;
        eListe.GetTextMetric (&tm);
        GetClientRect (mamain1, &rect);
        GetWindowRect (mamain1, &wrect);
        y = (wrect.top - 4 * tm.tmHeight);
        x = wrect.left;
        cy = 4 * tm.tmHeight;
        cx = wrect.right - wrect.left;
        MoveWindow (eWindow, x,y, cx, cy, TRUE);
}


void AUFPTLIST::MoveMamain1 ()
{
        RECT rect;
        RECT wrect;
        int x, y, cx, cy;
        TEXTMETRIC tm;

        if (mamainmin) return;
        eListe.GetTextMetric (&tm);
        GetClientRect (hMainWindow, &rect);
        GetWindowRect (hMainWindow, &wrect);
        if (mamainmax)
        {
                 x = wrect.left + 2;
                 y = wrect.top + 2;
                 cy = wrect.bottom - y - 2;
                 cx = rect.right;
        }
        else
        {
/*
                 y = (wrect.bottom - 12 * tm.tmHeight);
                 x = wrect.left + 2;
                 cy = wrect.bottom - y - 2;
                 cx = rect.right;
*/
                 y = (wrect.bottom - 12 * tm.tmHeight);
                 x = wrect.left + 2 + 4 * tm.tmAveCharWidth;
                 cy = wrect.bottom - y - 2;
                 cx = rect.right - 8 * tm.tmAveCharWidth;
        }
        MoveWindow (mamain1, x,y, cx, cy, TRUE);
        MoveeWindow ();
}


void AUFPTLIST::MaximizeMamain1 ()
{
        RECT rect;
        RECT wrect;
        int x, y, cx, cy;

        GetClientRect (hMainWindow, &rect);
        GetWindowRect (hMainWindow, &wrect);
        x = wrect.left + 2;
        y = wrect.top + 2;
        cy = wrect.bottom - y - 2;
        cx = rect.right;
        MoveWindow (mamain1, x,y, cx, cy, TRUE);
}

HWND AUFPTLIST::GetMamain1 (void)
{
       return (mamain1);
}

void AUFPTLIST::SethwndTB (HWND hwndTB)
{
         this->hwndTB = hwndTB; 
         eListe.SethwndTB (hwndTB);
}

void AUFPTLIST::SetTextMetric (TEXTMETRIC *tm)
{
         eListe.SetTextMetric (tm);
}


void AUFPTLIST::SetLineRow (int LineRow)
{
         eListe.SetLineRow (0);
}

void AUFPTLIST::SetListLines (int i)
{ 
         eListe.SetListLines (i);
}

void AUFPTLIST::OnPaint (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
        eListe.OnPaint (hWnd, msg, wParam, lParam);
}


void AUFPTLIST::MoveListWindow (void)
{
        MoveeWindow ();
        eListe.MoveListWindow ();
}


void AUFPTLIST::BreakList (void)
{
        eListe.BreakList ();
}


void AUFPTLIST::OnHScroll (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
        eListe.OnHScroll (hWnd, msg,wParam, lParam);
}

void AUFPTLIST::OnVScroll (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
        eListe.OnVScroll (hWnd, msg,wParam, lParam);
}


void AUFPTLIST::FunkKeys (WPARAM wParam, LPARAM lParam)
{
                    eListe.FunkKeys (wParam, lParam);
}


int AUFPTLIST::GetRecanz (void)
{
          return eListe.GetRecanz ();
}


void AUFPTLIST::SwitchPage0 (int rows)
{
           eListe.SwitchPage0 (rows);
}


HWND AUFPTLIST::Getmamain2 (void)
{
           return eListe.Getmamain2 ();
}


HWND AUFPTLIST::Getmamain3 (void)
{
           return eListe.Getmamain3 ();
}


void AUFPTLIST::ChoiseFont (mfont *lfont)
{
           eListe.ChoiseFont (lfont);
}

void AUFPTLIST::SetFont (mfont *lfont)
{
           eListe.SetFont (lfont);
}

void AUFPTLIST::SetListFont (mfont *lfont)
{
           eListe.SetListFont (lfont);
}

void AUFPTLIST::FindString (void)
{
           eListe.FindString ();
}


void AUFPTLIST::SetLines (int Lines)
{
           eListe.SetLines (Lines);
}


int AUFPTLIST::GetAktRow (void)
{
                 return eListe.GetAktRow ();
}

int AUFPTLIST::GetAktRowS (void)
{
                 return eListe.GetAktRowS ();
}

void AUFPTLIST::SetColors (COLORREF Color, COLORREF BkColor)
{
                 eListe.SetColors (Color, BkColor); 
}

void AUFPTLIST::SetListFocus (void)
{
                 eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                                       eListe.GetAktColumn ()); 
}

