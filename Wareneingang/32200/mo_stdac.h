#ifndef _MO_STDAC_DEF
#define _MO_STDAC_DEF
#include "stnd_best.h"
#include "dbclass.h"
#include "mo_ch.h"
#include "image.h"

struct STD_A
{
	     char a [14];
	     char a_bz1 [26];
	     char me_einh_bz [12];
	     char pr_vk [11];
           char dat [13];
           char me [13]; 
           char status [9];
         short nstatus; 
	     int  sortidx;
};

class STDAC
{
           private :
			    int SortMode;
                static struct STD_A stda;
                static struct STD_A *stdatab;
				static long stdaanz;
                static  CH *Choise1;
                static SBEST_CLASS sbest_class;
                static DB_CLASS DbClass;
		        HBITMAP Sel;
		        HBITMAP Msk;
           public :
               STDAC ()
               {
                      stdatab = NULL; 
					  SortMode = 0;
               }
               ~STDAC ()
               {
                      if (stdatab)
                      {
                          delete stdatab;
                      }
               }
			   static long Getstdaanz (void);
               static struct STD_A *GetStdaTab (void);
               static struct STD_A *GetStda (int);
               static int AllArt (int);
               static int ArtOK (int);
               static int StdaOK (int);
               int Show (HWND, int, int, char *);
};
#endif

