#ifndef _LGR_DEF
#define _LGR_DEF

#include "windows.h"
#include "mo_draw.h"
#include "wmask.h"
#include "dbclass.h"

struct LGR {
   short     lgr;
   short     mdn;
   short     fil;
   char      lgr_smt_kz[4];
   char      lgr_bz[25];
   short     lgr_kz;
   short     lgr_gr;
   char      lgr_kla[2];
   char      abr_period[2];
   long      adr;
   short     afl;
   char      bli_kz[2];
   long      dat_ero;
   double    fl_lad;
   double    fl_nto;
   double    fl_vk_ges;
   short     frm;
   long      iakv;
   char      inv_rht[2];
   char      ls_abgr[2];
   char      ls_kz[2];
   short     ls_sum;
   char      pers[13];
   short     pers_anz;
   char      pos_kum[2];
   char      pr_ausw[2];
   char      pr_bel_entl[2];
   char      pr_lgr_kz[2];
   long      pr_lst;
   char      pr_vk_kz[2];
   double    reg_bed_theke_lng;
   double    reg_kt_lng;
   double    reg_kue_lng;
   double    reg_lng;
   double    reg_tks_lng;
   double    reg_tkt_lng;
   char      smt_kz[2];
   short     sonst_einh;
   short     sprache;
   char      sw_kz[2];
   long      tou;
   short     vrs_typ;
   char      inv_akv[2];
   short     delstatus;
};
extern struct LGR lgr, lgr_null;

#line 10 "lgr.rh"

class LGR_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               LGR_CLASS () : DB_CLASS ()
               {
               }
               ~LGR_CLASS ()
               {
                   dbclose ();
               } 
};
#endif
