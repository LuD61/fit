#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include "liefmebest.h"

struct LIEFMEBEST liefmebest, liefmebest_null;

void LIEFMEBEST_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &liefmebest.mdn, 1, 0);
            ins_quest ((char *) &liefmebest.fil, 1, 0);
            ins_quest ((char *) &liefmebest.lief, 0, 17);
            ins_quest ((char *) &liefmebest.a, 3, 0);
    out_quest ((char *) &liefmebest.mdn,1,0);
    out_quest ((char *) &liefmebest.fil,1,0);
    out_quest ((char *) liefmebest.lief,0,17);
    out_quest ((char *) &liefmebest.a,3,0);
    out_quest ((char *) &liefmebest.tara0,3,0);
    out_quest ((char *) &liefmebest.me_einh0,1,0);
    out_quest ((char *) &liefmebest.inh0,3,0);
    out_quest ((char *) &liefmebest.me_einh1,1,0);
    out_quest ((char *) &liefmebest.inh1,3,0);
    out_quest ((char *) &liefmebest.tara1,3,0);
    out_quest ((char *) &liefmebest.me_einh2,1,0);
    out_quest ((char *) &liefmebest.inh2,3,0);
    out_quest ((char *) &liefmebest.tara2,3,0);
    out_quest ((char *) &liefmebest.me_einh3,1,0);
    out_quest ((char *) &liefmebest.inh3,3,0);
    out_quest ((char *) &liefmebest.tara3,3,0);
    out_quest ((char *) &liefmebest.me_einh4,1,0);
    out_quest ((char *) &liefmebest.inh4,3,0);
    out_quest ((char *) &liefmebest.tara4,3,0);
    out_quest ((char *) &liefmebest.me_einh5,1,0);
    out_quest ((char *) &liefmebest.inh5,3,0);
    out_quest ((char *) &liefmebest.tara5,3,0);
    out_quest ((char *) &liefmebest.me_einh6,1,0);
    out_quest ((char *) &liefmebest.inh6,3,0);
    out_quest ((char *) &liefmebest.tara6,3,0);
    out_quest ((char *) &liefmebest.knuepf1,1,0);
    out_quest ((char *) &liefmebest.knuepf2,1,0);
    out_quest ((char *) &liefmebest.knuepf3,1,0);
    out_quest ((char *) &liefmebest.knuepf4,1,0);
    out_quest ((char *) &liefmebest.knuepf5,1,0);
    out_quest ((char *) &liefmebest.knuepf6,1,0);
    out_quest ((char *) &liefmebest.ean_su1,3,0);
    out_quest ((char *) &liefmebest.ean_su2,3,0);
    out_quest ((char *) &liefmebest.pr_ek0,3,0);
    out_quest ((char *) &liefmebest.pr_ek1,3,0);
    out_quest ((char *) &liefmebest.pr_ek2,3,0);
    out_quest ((char *) &liefmebest.pr_ek3,3,0);
    out_quest ((char *) &liefmebest.pr_ek4,3,0);
    out_quest ((char *) &liefmebest.pr_ek5,3,0);
    out_quest ((char *) &liefmebest.pr_ek6,3,0);
            cursor = prepare_sql ("select liefmebest.mdn,  "
"liefmebest.fil,  liefmebest.lief,  liefmebest.a,  liefmebest.tara0,  "
"liefmebest.me_einh0,  liefmebest.inh0,  liefmebest.me_einh1,  "
"liefmebest.inh1,  liefmebest.tara1,  liefmebest.me_einh2,  "
"liefmebest.inh2,  liefmebest.tara2,  liefmebest.me_einh3,  "
"liefmebest.inh3,  liefmebest.tara3,  liefmebest.me_einh4,  "
"liefmebest.inh4,  liefmebest.tara4,  liefmebest.me_einh5,  "
"liefmebest.inh5,  liefmebest.tara5,  liefmebest.me_einh6,  "
"liefmebest.inh6,  liefmebest.tara6,  liefmebest.knuepf1,  "
"liefmebest.knuepf2,  liefmebest.knuepf3,  liefmebest.knuepf4,  "
"liefmebest.knuepf5,  liefmebest.knuepf6,  liefmebest.ean_su1,  "
"liefmebest.ean_su2,  liefmebest.pr_ek0,  liefmebest.pr_ek1,  "
"liefmebest.pr_ek2,  liefmebest.pr_ek3,  liefmebest.pr_ek4,  "
"liefmebest.pr_ek5,  liefmebest.pr_ek6 from liefmebest "

#line 19 "liefmebest.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   lief = ? "
                                  "and   a = ?");

    ins_quest ((char *) &liefmebest.mdn,1,0);
    ins_quest ((char *) &liefmebest.fil,1,0);
    ins_quest ((char *) liefmebest.lief,0,17);
    ins_quest ((char *) &liefmebest.a,3,0);
    ins_quest ((char *) &liefmebest.tara0,3,0);
    ins_quest ((char *) &liefmebest.me_einh0,1,0);
    ins_quest ((char *) &liefmebest.inh0,3,0);
    ins_quest ((char *) &liefmebest.me_einh1,1,0);
    ins_quest ((char *) &liefmebest.inh1,3,0);
    ins_quest ((char *) &liefmebest.tara1,3,0);
    ins_quest ((char *) &liefmebest.me_einh2,1,0);
    ins_quest ((char *) &liefmebest.inh2,3,0);
    ins_quest ((char *) &liefmebest.tara2,3,0);
    ins_quest ((char *) &liefmebest.me_einh3,1,0);
    ins_quest ((char *) &liefmebest.inh3,3,0);
    ins_quest ((char *) &liefmebest.tara3,3,0);
    ins_quest ((char *) &liefmebest.me_einh4,1,0);
    ins_quest ((char *) &liefmebest.inh4,3,0);
    ins_quest ((char *) &liefmebest.tara4,3,0);
    ins_quest ((char *) &liefmebest.me_einh5,1,0);
    ins_quest ((char *) &liefmebest.inh5,3,0);
    ins_quest ((char *) &liefmebest.tara5,3,0);
    ins_quest ((char *) &liefmebest.me_einh6,1,0);
    ins_quest ((char *) &liefmebest.inh6,3,0);
    ins_quest ((char *) &liefmebest.tara6,3,0);
    ins_quest ((char *) &liefmebest.knuepf1,1,0);
    ins_quest ((char *) &liefmebest.knuepf2,1,0);
    ins_quest ((char *) &liefmebest.knuepf3,1,0);
    ins_quest ((char *) &liefmebest.knuepf4,1,0);
    ins_quest ((char *) &liefmebest.knuepf5,1,0);
    ins_quest ((char *) &liefmebest.knuepf6,1,0);
    ins_quest ((char *) &liefmebest.ean_su1,3,0);
    ins_quest ((char *) &liefmebest.ean_su2,3,0);
    ins_quest ((char *) &liefmebest.pr_ek0,3,0);
    ins_quest ((char *) &liefmebest.pr_ek1,3,0);
    ins_quest ((char *) &liefmebest.pr_ek2,3,0);
    ins_quest ((char *) &liefmebest.pr_ek3,3,0);
    ins_quest ((char *) &liefmebest.pr_ek4,3,0);
    ins_quest ((char *) &liefmebest.pr_ek5,3,0);
    ins_quest ((char *) &liefmebest.pr_ek6,3,0);
            sqltext = "update liefmebest set "
"liefmebest.mdn = ?,  liefmebest.fil = ?,  liefmebest.lief = ?,  "
"liefmebest.a = ?,  liefmebest.tara0 = ?,  liefmebest.me_einh0 = ?,  "
"liefmebest.inh0 = ?,  liefmebest.me_einh1 = ?,  "
"liefmebest.inh1 = ?,  liefmebest.tara1 = ?,  "
"liefmebest.me_einh2 = ?,  liefmebest.inh2 = ?,  "
"liefmebest.tara2 = ?,  liefmebest.me_einh3 = ?,  "
"liefmebest.inh3 = ?,  liefmebest.tara3 = ?,  "
"liefmebest.me_einh4 = ?,  liefmebest.inh4 = ?,  "
"liefmebest.tara4 = ?,  liefmebest.me_einh5 = ?,  "
"liefmebest.inh5 = ?,  liefmebest.tara5 = ?,  "
"liefmebest.me_einh6 = ?,  liefmebest.inh6 = ?,  "
"liefmebest.tara6 = ?,  liefmebest.knuepf1 = ?,  "
"liefmebest.knuepf2 = ?,  liefmebest.knuepf3 = ?,  "
"liefmebest.knuepf4 = ?,  liefmebest.knuepf5 = ?,  "
"liefmebest.knuepf6 = ?,  liefmebest.ean_su1 = ?,  "
"liefmebest.ean_su2 = ?,  liefmebest.pr_ek0 = ?,  "
"liefmebest.pr_ek1 = ?,  liefmebest.pr_ek2 = ?,  "
"liefmebest.pr_ek3 = ?,  liefmebest.pr_ek4 = ?,  "
"liefmebest.pr_ek5 = ?,  liefmebest.pr_ek6 = ? "

#line 25 "liefmebest.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   lief = ? "
                                  "and   a = ?";

            ins_quest ((char *) &liefmebest.mdn, 1, 0);
            ins_quest ((char *) &liefmebest.fil, 1, 0);
            ins_quest ((char *) &liefmebest.lief, 0, 17);
            ins_quest ((char *) &liefmebest.a, 3, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &liefmebest.mdn, 1, 0);
            ins_quest ((char *) &liefmebest.fil, 1, 0);
            ins_quest ((char *) &liefmebest.lief, 0, 17);
            ins_quest ((char *) &liefmebest.a, 3, 0);
            test_upd_cursor = prepare_sql ("select a from liefmebest "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   lief = ? "
                                  "and   a = ? for update");
            ins_quest ((char *) &liefmebest.mdn, 1, 0);
            ins_quest ((char *) &liefmebest.fil, 1, 0);
            ins_quest ((char *) &liefmebest.lief, 0, 17);
            ins_quest ((char *) &liefmebest.a, 3, 0);
            del_cursor = prepare_sql ("delete from liefmebest "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   lief = ? "
                                  "and   a = ?");
    ins_quest ((char *) &liefmebest.mdn,1,0);
    ins_quest ((char *) &liefmebest.fil,1,0);
    ins_quest ((char *) liefmebest.lief,0,17);
    ins_quest ((char *) &liefmebest.a,3,0);
    ins_quest ((char *) &liefmebest.tara0,3,0);
    ins_quest ((char *) &liefmebest.me_einh0,1,0);
    ins_quest ((char *) &liefmebest.inh0,3,0);
    ins_quest ((char *) &liefmebest.me_einh1,1,0);
    ins_quest ((char *) &liefmebest.inh1,3,0);
    ins_quest ((char *) &liefmebest.tara1,3,0);
    ins_quest ((char *) &liefmebest.me_einh2,1,0);
    ins_quest ((char *) &liefmebest.inh2,3,0);
    ins_quest ((char *) &liefmebest.tara2,3,0);
    ins_quest ((char *) &liefmebest.me_einh3,1,0);
    ins_quest ((char *) &liefmebest.inh3,3,0);
    ins_quest ((char *) &liefmebest.tara3,3,0);
    ins_quest ((char *) &liefmebest.me_einh4,1,0);
    ins_quest ((char *) &liefmebest.inh4,3,0);
    ins_quest ((char *) &liefmebest.tara4,3,0);
    ins_quest ((char *) &liefmebest.me_einh5,1,0);
    ins_quest ((char *) &liefmebest.inh5,3,0);
    ins_quest ((char *) &liefmebest.tara5,3,0);
    ins_quest ((char *) &liefmebest.me_einh6,1,0);
    ins_quest ((char *) &liefmebest.inh6,3,0);
    ins_quest ((char *) &liefmebest.tara6,3,0);
    ins_quest ((char *) &liefmebest.knuepf1,1,0);
    ins_quest ((char *) &liefmebest.knuepf2,1,0);
    ins_quest ((char *) &liefmebest.knuepf3,1,0);
    ins_quest ((char *) &liefmebest.knuepf4,1,0);
    ins_quest ((char *) &liefmebest.knuepf5,1,0);
    ins_quest ((char *) &liefmebest.knuepf6,1,0);
    ins_quest ((char *) &liefmebest.ean_su1,3,0);
    ins_quest ((char *) &liefmebest.ean_su2,3,0);
    ins_quest ((char *) &liefmebest.pr_ek0,3,0);
    ins_quest ((char *) &liefmebest.pr_ek1,3,0);
    ins_quest ((char *) &liefmebest.pr_ek2,3,0);
    ins_quest ((char *) &liefmebest.pr_ek3,3,0);
    ins_quest ((char *) &liefmebest.pr_ek4,3,0);
    ins_quest ((char *) &liefmebest.pr_ek5,3,0);
    ins_quest ((char *) &liefmebest.pr_ek6,3,0);
            ins_cursor = prepare_sql ("insert into liefmebest ("
"mdn,  fil,  lief,  a,  tara0,  me_einh0,  inh0,  me_einh1,  inh1,  tara1,  me_einh2,  inh2,  "
"tara2,  me_einh3,  inh3,  tara3,  me_einh4,  inh4,  tara4,  me_einh5,  inh5,  tara5,  "
"me_einh6,  inh6,  tara6,  knuepf1,  knuepf2,  knuepf3,  knuepf4,  knuepf5,  knuepf6,  "
"ean_su1,  ean_su2,  pr_ek0,  pr_ek1,  pr_ek2,  pr_ek3,  pr_ek4,  pr_ek5,  pr_ek6) "

#line 55 "liefmebest.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"); 

#line 57 "liefmebest.rpp"
}
int LIEFMEBEST_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

