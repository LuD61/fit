#include<stdio.h>
#include<stdarg.h>
#include<string.h>
#include<windows.h>
#include "stdfkt.h"
#include "strfkt.h"
#include "combodlg.h" 

static int StdSize = STDSIZE;
static int InfoSize = 150;

static int dlgsize    = STDSIZE;
static int ltgraysize = STDSIZE;
static int dlgpossize = STDSIZE;
static int cpsize     = 200;


mfont ComboDlg::dlgfont = {
                         "ARIAL", 100, 0, 0,
                         BLACKCOL,
                         LTGRAYCOL,
                         0,
                         NULL};

mfont ComboDlg::dlgposfont = {
                         "ARIAL", 100, 0, 0,
                         BLACKCOL,
                         LTGRAYCOL,
                         0,
                         NULL};



mfont *ComboDlg::Font = &dlgposfont;


char ComboDlg::combotext [80];

CFIELD *ComboDlg::_fCombo0 [] = {
                                new CFIELD ("", "Einheit f�r Bestellmenge",
                                            0, 0, 0, 0, NULL, "", CDISPLAYONLY,
                                            500, Font, 0, 0),
                                new CFIELD ("combobox", combotext,
                                            27, 5, 0,  1,  NULL, "", CCOMBOBOX,
                                            COMBOBOX_CTL, Font, 0,    CBS_DROPDOWNLIST |
                                                                      WS_VSCROLL |
                                                                      CBS_DISABLENOSCROLL),
                                NULL,
};

CFORM ComboDlg::fCombo0 (2, _fCombo0);

CFIELD *ComboDlg::_fCombo [] = {
                                new CFIELD ("comboform", (CFORM *)   &fCombo0,
                                            0, 0, -1,  1,  NULL, "", CFFORM,
                                            500, Font, 0, 0),
                                NULL,

};

CFORM ComboDlg::fCombo (1, _fCombo);
                          
ComboDlg::ComboDlg (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel) :
         DLG (x, y, cx, cy, Caption, Size, Pixel)
{
          
             Init (x, y, cx, cy, Caption, Size, Pixel);
}

ComboDlg::~ComboDlg ()
{
         ActiveDlg = OldDlg;
}


void ComboDlg::Init (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel)
{
//             int i;
//             int xfull, yfull;

             SetDimension (19, 5); 
             SetDialog (&fCombo);
             ActiveDlg = this;
             fCombo.SetFieldanz ();
             CbClosed = FALSE;
}


void ComboDlg::SetComboBox (char **Combobox)
{
             fCombo.GetCfield ("combobox")->SetCombobox (Combobox); 
}

BOOL ComboDlg::OnKeyReturn (void)
{
       fCombo.GetText ();
       DestroyWindow ();
       return TRUE;
}

BOOL ComboDlg::OnKey5 ()
{
       syskey = KEY5;
       fCombo.GetText ();
       DestroyWindow ();
       return TRUE;
}


BOOL ComboDlg::OnCloseUp (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
       PostMessage (hWnd, WM_KEYDOWN, VK_RETURN, 0l); 
       return FALSE;
}

BOOL ComboDlg::OnCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
         
        if (fWork)
        {
	          if (HIWORD (wParam)  == CBN_CLOSEUP)
              {
		           return OnCloseUp (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == WM_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == EN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == CBN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == WM_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == LBN_SELCHANGE)
              {
              }
              else if (HIWORD (wParam) == EN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == CBN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == BN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == BN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
        }
        return DLG::OnCommand (hWnd, msg, wParam, lParam);
}


BOOL ComboDlg::OnSysCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        
        if (wParam == SC_CLOSE)
        {
            if (abfragejn (hWnd, "Verarbeitung abbrechen ?", "N") == 0)
            {
                ActiveDlg->SetCurrentFocus ();
                return TRUE;
            }
            ExitProcess (0);
            syskey = KEY5;
        }
        return DLG::OnSysCommand (hWnd, msg, wParam, lParam); 
}


void ComboDlg::SetWinBackground (COLORREF Col)
{
          DLG::SetWinBackground (Col);
}

void ComboDlg::ToClipboard (void)
{
          CFIELD *Cfield;
          HGLOBAL hb;
          LPVOID p;
          char *text;

          Cfield = GetCurrentCfield ();
          Cfield->GetText ();
          text = (char *) Cfield->GetFeld ();
          if (text == NULL) return;
          hb = GlobalAlloc (GHND, strlen (text) + 1);
          if (hb == NULL) return;
          p = GlobalLock (hb);
          strcpy ((char *) p, text);
          GlobalUnlock (hb);
          OpenClipboard (hWnd);
          EmptyClipboard ();
          SetClipboardData (CF_TEXT, hb);
          CloseClipboard ();
}

void ComboDlg::FromClipboard (void)
{
          CFIELD *Cfield;
          HGLOBAL hb;
          LPVOID p;
          char *text;

          Cfield = GetCurrentCfield ();
          text = (char *) Cfield->GetFeld ();
          if (text == NULL) return;
          if (IsClipboardFormatAvailable (CF_TEXT) == FALSE) return;
          OpenClipboard (hWnd);
          hb = GetClipboardData (CF_TEXT);
          if (hb == NULL)
          {
                 CloseClipboard ();
                 return;
          }
          p = GlobalLock (hb);
          if (strlen ((char *) p) > (unsigned int ) Cfield->GetLength ())
          {
                 GlobalUnlock (hb);
                 CloseClipboard ();
                 return;
          }
          strcpy (text, (char *) p);
          GlobalUnlock (hb);
          CloseClipboard ();
          Cfield->SetText ();
}

