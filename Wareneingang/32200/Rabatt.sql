select * from lief_r_kte
where mdn = 0
and fil = 0
and lief = "1001"
and 
( 
(von_dat <= today and bis_dat >= today) 
or
(von_dat <= today and bis_dat is null) 
or
(von_dat is null and bis_dat >= today) 
or
(von_dat is null and bis_dat is null)
or
(von_dat <= today and bis_dat <= "01.01.1900") 
or
(von_dat <= "01.01.1900" and bis_dat >= today) 
or
(von_dat <= "01.01.1900" and bis_dat <= "01.01.1900")
)
order by lfd;
