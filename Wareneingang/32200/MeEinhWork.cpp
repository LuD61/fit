#include <windows.h>
#include <stdio.h>
#include "MeEinhWork.h"
#include "a_bas.h"



int MeEinhWork::ReadA (void)
{
    int dsqlstatus;

    memcpy (&_a_bas, &_a_bas_null, sizeof (struct A_BAS));
    dsqlstatus = lese_a_bas (liefmebest.a);
    if (dsqlstatus != 0)
    {
        return -1;
    }
    dsqlstatus = LiefMeBest.dbreadfirst ();
    return dsqlstatus;
}

BOOL MeEinhWork::TestLiefBzg (void)
{
    int dsqlstatus;

    dsqlstatus = LiefBzg.dbreadfirst ();
    if (dsqlstatus == 100)
    {
        return FALSE;
    }
    return TRUE;
}

int MeEinhWork::ReadA (short mdn, short fil, char *lief_nr, double a)
{
    int dsqlstatus;

    memcpy (&_a_bas, &_a_bas_null, sizeof (struct A_BAS));
    memcpy (&liefmebest, &liefmebest_null, sizeof (struct LIEFMEBEST));
    dsqlstatus = lese_a_bas (a);
    if (dsqlstatus == 100)
    {
        return -1;
    }
    liefmebest.mdn = mdn;
    liefmebest.fil = fil;
    strcpy (liefmebest.lief, lief_nr);
    liefmebest.a = a;
    dsqlstatus = LiefMeBest.dbreadfirst ();
/*
    if (dsqlstatus == 100)
    {
        GetLiefDefaults ();
    }
*/
    return dsqlstatus;
}

void MeEinhWork::TestLiefPr (void)
{
    if (lief_bzg.me_kz[0] == '0' &&
        liefmebest.pr_ek1 == 0.0)
    {
            liefmebest.pr_ek1   = lief_bzg.pr_ek_eur;
    }
    if (lief_bzg.me_kz[0] == '1' &&
        liefmebest.pr_ek0 == 0.0)
    {
            liefmebest.pr_ek0   = lief_bzg.pr_ek_eur;
    }
    if (liefmebest.inh0 == 0.0)
    {
            liefmebest.inh0 = 1.0;
    }
    if (liefmebest.inh1 == 0.0)
    {
            liefmebest.inh1 = lief_bzg.min_best;
    }
    liefmebest.me_einh1 = lief_bzg.me_einh_ek;
}

int MeEinhWork::GetLiefDefaults (void)
{
    int dsqlstatus = 0;

    liefmebest.me_einh0 = _a_bas.me_einh;
    liefmebest.inh0     = 1.0;
    liefmebest.me_einh1 = lief_bzg.me_einh_ek;
    liefmebest.inh1     = lief_bzg.min_best;
    if (lief_bzg.me_kz[0] == '0')
    {
            liefmebest.pr_ek1   = lief_bzg.pr_ek_eur;
    }
    else
    {
            liefmebest.pr_ek0   = lief_bzg.pr_ek_eur;
    }
    return dsqlstatus; 
}


double MeEinhWork::GetLiefBzgInh (double inh, int knuepf)
{

    switch (knuepf)
    {
       case 0 :
           return inh;
       case 1 :
           if (liefmebest.knuepf1 == 1)
           {
               return inh;
           }
           return inh * GetLiefBzgInh (liefmebest.inh1, liefmebest.knuepf1);
       case 2 :
           if (liefmebest.knuepf2 == 2)
           {
               return inh;
           }
           return inh * GetLiefBzgInh (liefmebest.inh2, liefmebest.knuepf2);
       case 3 :
           if (liefmebest.knuepf3 == 3)
           {
               return inh;
           }
           return inh * GetLiefBzgInh (liefmebest.inh3, liefmebest.knuepf3);
       case 4 :
           if (liefmebest.knuepf4 == 4)
           {
               return inh;
           }
           return inh * GetLiefBzgInh (liefmebest.inh4, liefmebest.knuepf4);
       case 5 :
           if (liefmebest.knuepf5 == 5)
           {
               return inh;
           }
           return inh * GetLiefBzgInh (liefmebest.inh5, liefmebest.knuepf5);
       case 6 :
           if (liefmebest.knuepf6 == 6)
           {
               return inh;
           }
           return inh * GetLiefBzgInh (liefmebest.inh6, liefmebest.knuepf6);
    }
    return inh;
}


int MeEinhWork::WriteA (void)
{

    LiefMeBest.dbupdate ();
    lief_bzg.mdn = liefmebest.mdn;
    lief_bzg.fil = liefmebest.fil;
    strcpy (lief_bzg.lief, liefmebest.lief);
    lief_bzg.a   = liefmebest.a;
    if (lief_bzg.me_kz[0] == '0')
    {
           lief_bzg.pr_ek_eur = liefmebest.pr_ek1;
           lief_bzg.pr_ek     = liefmebest.pr_ek1 * 1.95583;
    }
    else
    {
           lief_bzg.pr_ek_eur = liefmebest.pr_ek0;
           lief_bzg.pr_ek     = liefmebest.pr_ek1 * 1.95583;
    }
    lief_bzg.me_einh_ek  = liefmebest.me_einh1;
    lief_bzg.min_best    = GetLiefBzgInh (liefmebest.inh1, liefmebest.knuepf1);
    LiefBzg.dbupdate ();
    return 0;
}

int MeEinhWork::DeleteLief (void)
{
    int dsqlstatus;
    
    LiefMeBest.sqlin ((short *)  &liefmebest.mdn, 1, 0);
    LiefMeBest.sqlin ((short *)  &liefmebest.fil, 1, 0);
    LiefMeBest.sqlin ((char *)   liefmebest.lief, 0, 17);
    LiefMeBest.sqlin ((double *) &liefmebest.a, 3, 0);
    dsqlstatus = LiefMeBest.sqlcomm ("delete from liefmebest "
                                  "where mdn = ? "
                                  "and fil = ? "
                                  "and lief = ? "
                                  "and a = ?");
    return dsqlstatus;
}


int MeEinhWork::DeleteA (void)
{
    LiefMeBest.dbdelete ();
    return 0;
}

int MeEinhWork::GetPtab (char *dest, char *item, char *wert)
{
    int dsqlstatus;

    for (; *wert <= ' '; wert += 1)
    {
        if (*wert == 0) break;
    }
    dest[0] = 0;
    dsqlstatus = Ptab.lese_ptab (item, wert);  
    if (dsqlstatus == 100)
    {
        return -1;
    }
    strcpy (dest, ptabn.ptbezk);
    return 0;
}

int MeEinhWork::ShowPtab (char *dest, char *item)
{
    int dsqlstatus;

    dsqlstatus = Ptab.Show (item);  
    if (syskey == KEY5)
    {
        return -1;
    }
    strcpy (dest, ptabn.ptwert);
    return 0;
}

int MeEinhWork::ShowPtabEx (HWND hMainWindow, char *dest, char *item)
{
       char wert [80];
       SEARCHPTAB *SearchPtab;
	   HINSTANCE hMainInst;


       hMainInst = (HINSTANCE) GetWindowLong (hMainWindow, GWL_HINSTANCE);
       SearchPtab = new SEARCHPTAB (item);
       SearchPtab->SetParams (hMainInst, hMainWindow); 
       SearchPtab->Setawin (hMainWindow);
       SearchPtab->SetNumWert (TRUE);                                             
       SearchPtab->Search ();
       if (SearchPtab->GetKey (wert) == TRUE)
       {
           delete SearchPtab;
           strcpy (dest, wert);
		   return 0;
       }
       delete SearchPtab;
       return -1;
}


int MeEinhWork::GetMdnName (char *dest, short mdn)
{
    int dsqlstatus;

    if (mdn == 0)
    {
        strcpy (dest, "Zentrale");
        return 0;
    }
    dest[0] = 0;
    LiefMeBest.sqlin ((short *) &mdn, 1, 0);
    LiefMeBest.sqlout ((char *) dest, 0, 17);
    dsqlstatus =  LiefMeBest.sqlcomm ("select adr.adr_krz from mdn,adr "
                                   "where mdn.mdn = ? " 
                                   "and adr.adr = mdn.adr");
    if (dsqlstatus == 100)
    {
        return 100;
    }
    return Mdn.lese_mdn (mdn);
}

int MeEinhWork::GetFilName (char *dest, short mdn, short fil)
{

    if (mdn == 0 || fil == 0)
    {
        strcpy (dest, "Mandant");
        return 0;
    }
    dest[0] = 0;
    LiefMeBest.sqlin ((short *) &mdn, 1, 0);
    LiefMeBest.sqlin ((short *) &fil, 1, 0);
    LiefMeBest.sqlout ((char *) dest, 0, 17);
    return LiefMeBest.sqlcomm ("select adr.adr_krz from fil,adr "
                             "where fil.mdn = ? " 
                             "and   fil.fil = ? " 
                             "and adr.adr = fil.adr");
}

int MeEinhWork::GetLiefName (char *dest, short mdn, char *lief_nr)
{
    int dsqlstatus;
    int cursor;

    dest[0] = 0;


    LiefMeBest.sqlin ((short *) &mdn, 1, 0);
    LiefMeBest.sqlin ((char *)  lief_nr, 0, 17);
    LiefMeBest.sqlout ((char *) dest, 0, 17);

    cursor = LiefMeBest.sqlcursor ("select adr.adr_krz from lief,adr "
                                "where lief.mdn = ? " 
                                "and   lief.lief = ? "
                                "and adr.adr = lief.adr");
    dsqlstatus = LiefMeBest.sqlfetch (cursor);
    while (dsqlstatus == 100)
    {
        if (mdn > 0)
        {
            mdn = 0;
        }
        else
        {
            break;
        }
        dsqlstatus = LiefMeBest.sqlopen (cursor);
        dsqlstatus = LiefMeBest.sqlfetch (cursor);
    }
    LiefMeBest.sqlclose (cursor);
    if (dsqlstatus == 0)
    {
        lief.mdn = mdn;
        strcpy (lief.lief, lief_nr);
        Lief.dbreadfirst ();
    }
    return dsqlstatus;
}


void MeEinhWork::BeginWork (void)
{
    ::beginwork ();
}

void MeEinhWork::CommitWork (void)
{
    ::commitwork ();
}

void MeEinhWork::RollbackWork (void)
{
    ::rollbackwork ();
}

void MeEinhWork::InitRec (void)
{
    memcpy (&liefmebest, &liefmebest_null, sizeof (struct LIEFMEBEST));
}