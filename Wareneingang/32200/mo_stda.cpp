#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include "itemc.h"
#include "wmaskc.h"
#include "dbfunc.h"
#include "mo_meld.h"
#include "datum.h"
#include "strfkt.h"
#include "mo_curso.h"
#include "mo_stda.h"
#include "stnd_best.h"
#include "a_bas.h"
#include "dbclass.h"

#define MAXSORT 1000

static SBEST_CLASS sbest_class;
static DB_CLASS DbClass;


struct STD_AUF
{
	     char a [14];
	     char a_bz1 [26];
	     char me_einh_bz [12];
	     char pr_vk [11];
           char dat [13];
           char me [13]; 
	     int  sortidx;
};

static struct STD_AUF std_auf, std_auftab[MAXSORT];
static int std_awanz;
static int sort_idx;

static ITEM ia                  ("a",          std_auf.a,                 "", 0);
static ITEM ia_bz1              ("a_bz1",      std_auf.a_bz1,             "", 0);
static ITEM ime_einh_bz         ("me_einh_bz", std_auf.me_einh_bz,        "", 0);
static ITEM ipr_vk              ("pr_vk",      std_auf.pr_vk,             "", 0);
static ITEM idat                ("dat",        std_auf.dat,               "", 0);
static ITEM ime                 ("me",         std_auf.me,                "", 0);
static ITEM ifillub   ("", " ",             "", 0); 

static field _fstd_auf[] = {
&ia,         14, 1, 0,  1, 0,  "%13f",     NORMAL, 0, 0, 0,
&ia_bz1,     25, 1, 0, 16,0,  "",         NORMAL, 0, 0, 0,
&ime_einh_bz,11, 1, 0, 42,0,  "",         NORMAL, 0, 0, 0,      
&ipr_vk,      8, 1, 0, 54,0,  "%6.2f",    NORMAL, 0, 0, 0,      
&idat,       11, 1, 0, 63,0,  "",         NORMAL, 0, 0, 0,      
&ime,        12, 1, 0, 75,0,  "%10.2f",   NORMAL, 0, 0, 0,      
// &ifillub,    80, 1, 0, 81, 0, "",         NORMAL, 0, 0, 0,
};

static form fstd_auf = {6, 0, 0, _fstd_auf, 0, 0, 0, 0, NULL};

static ITEM isort1ub    ("", "a",             "", 0);
static ITEM isort2ub    ("", "Bezeichnung",   "", 0);
static ITEM isort3ub    ("", "Einh",          "", 0);
static ITEM isort4ub    ("", "DM/Einh",       "", 0);
static ITEM isort5ub    ("", "Datum",         "", 0);
static ITEM isort6ub    ("", "Std Menge",     "", 0);

static field _fsort_awub[] = {
&isort1ub,       16, 1, 0,  0, 0, "", BUTTON, 0, 0,    0,
&isort2ub,       26, 1, 0, 15, 0, "", BUTTON, 0, 0,    0,
&isort3ub,       12, 1, 0, 41, 0, "", BUTTON, 0, 0,    0,
&isort4ub,        9, 1, 0, 53, 0, "", BUTTON, 0, 0,    0,
&isort5ub,       12, 1, 0, 62, 0, "", BUTTON, 0, 0,    0,
&isort6ub,       13, 1, 0, 74, 0, "", BUTTON, 0, 0,    0,
&ifillub,        80, 1, 0, 87, 0, "", BUTTON, 0, 0, 0};

static form fsort_awub = {7, 0, 0, _fsort_awub, 0, 0, 0, 0, NULL};

static ITEM isortl ("", "1", "", 0);

static field _fsort_awl[] = {
&isortl,          1, 1, 0, 15, 0, "", NORMAL, 0, 0, 0,
&isortl,          1, 1, 0, 41, 0, "", NORMAL, 0, 0, 0,
&isortl,          1, 1, 0, 53, 0, "", NORMAL, 0, 0, 0,
&isortl,          1, 1, 0, 62, 0, "", NORMAL, 0, 0, 0,
&isortl,          1, 1, 0, 74, 0, "", NORMAL, 0, 0, 0,
&isortl,          1, 1, 0, 87, 0, "", NORMAL, 0, 0, 0,
};

static form fsort_awl = {6, 0, 0, _fsort_awl, 0, 0, 0, 0, NULL};

static struct LISTBU ListBu[] = {
    "   Artikel  ", 15, BT_OK, 0,
    " F8 Auftrag ", 15, KEY8,  0,
    " F5 Abbruch ", 15, KEY5, 0,
    NULL,         0, 0, 0};

static void IsAwClck (int idx)
/**
Reaktion auf Doppelclick in der Liste.
**/
{
        sort_idx = idx;
        break_list ();
        return;
}

static int endsort (void)
/**
Liste beenden.
**/
{
        break_list ();
        return (1);
}

static int dokey5 ()
{
        break_list ();
        syskey = KEY5;
        return (1);
}


static int dokey8 ()
{
        break_list ();
        syskey = KEY8;
        return (1);
}


int StndAuf::StdAuftrag (HWND hWnd, short mdn, short fil, char *lief)
/**
Standardauftraege anzeigen.
**/
{
        HWND eWindow;
        int dsqlstatus;
        int i;
        char caption [80];
		int cursor;
		short ag;
		short wg;
		char a_bz1 [25];
		char kun_bran2 [4];

		DbClass.sqlin ((short *) &mdn, 1, 0);
		DbClass.sqlin ((short *) &fil, 1, 0);
		DbClass.sqlin ((long *)  lief, 0, 17);

		DbClass.sqlout ((short *) &best_stnd.mdn, 1, 0);
		DbClass.sqlout ((short *) &best_stnd.fil, 1, 0);
		DbClass.sqlout ((short *) &best_stnd.lief, 0, 17);
		DbClass.sqlout ((double *)&best_stnd.a, 3, 0);
		DbClass.sqlout ((short *) &ag, 1, 0);
		DbClass.sqlout ((short *) &wg, 1, 0);
		DbClass.sqlout ((char *)  &a_bz1, 0, 25);

		if (SortMode == 0)
		{
           		cursor = DbClass.sqlcursor ("select best_stnd.mdn, best_stnd.fil, "
			                        "best_stnd.lief, "
									"best_stnd.a, a_bas.ag, a_bas.wg, "
									"a_bas.a_bz1 "
			                        "from best_stnd,a_bas "
									"where best_stnd.mdn = ? "
									"and   best_stnd.fil = ? "  
									"and   best_stnd.lief = ? "  
									"and a_bas.a = best_stnd.a "
									"order by 3,4");  
		}
		else if (SortMode == 1)
		{
           		cursor = DbClass.sqlcursor ("select best_stnd.mdn, best_stnd.fil, "
			                        "best_stnd.lief, "
									"best_stnd.a, a_bas.ag, a_bas.wg, "
									"a_bas.a_bz1 "
			                        "from best_stnd,a_bas "
									"where best_stnd.mdn = ? "
									"and   best_stnd.fil = ? "  
									"and   best_stnd.lief = ? "  
									"and a_bas.a = best_stnd.a "
									"order by 5,4");  
		}
		else if (SortMode == 2)
		{
           		cursor = DbClass.sqlcursor ("select best_stnd.mdn, best_stnd.fil, "
			                        "best_stnd.lief, "
									"best_stnd.a, a_bas.ag, a_bas.wg, "
									"a_bas.a_bz1 "
			                        "from best_stnd,a_bas "
									"where best_stnd.mdn = ? "
									"and   best_stnd.fil = ? "  
									"and   best_stnd.lief = ? "  
									"and a_bas.a = best_stnd.a "
									"order by 6,5");  
		}
		else if (SortMode == 3)
		{
           		cursor = DbClass.sqlcursor ("select best_stnd.mdn, best_stnd.fil, "
			                        "best_stnd.lief, "
									"best_stnd.a, a_bas.ag, a_bas.wg, "
									"a_bas.a_bz1 "
			                        "from best_stnd,a_bas "
									"where best_stnd.mdn = ? "
									"and   best_stnd.fil = ? "  
									"and   best_stnd.lief = ? "  
									"and a_bas.a = best_stnd.a "
									"order by 5,7");  
		}
		else if (SortMode == 4)
		{
           		cursor = DbClass.sqlcursor ("select best_stnd.mdn, best_stnd.fil, "
			                        "best_stnd.lief, "
									"best_stnd.a, a_bas.ag, a_bas.wg, "
									"a_bas.a_bz1 "
			                        "from best_stnd,a_bas "
									"where best_stnd.mdn = ? "
									"and   best_stnd.fil = ? "  
									"and   best_stnd.lief = ? "  
									"and a_bas.a = best_stnd.a "
									"order by 6,7");  
		}
			                      

        hMainInst = (HANDLE) GetWindowLong (hWnd, GWL_HINSTANCE);	
        save_fkt (5);
  	    save_fkt (6);
	    save_fkt (7);
	    save_fkt (8);
	    save_fkt (11);
	    save_fkt (12);

        set_fkt (NULL, 6);
        set_fkt (NULL, 7);
        set_fkt (NULL, 9);
        set_fkt (dokey8, 8);
        best_stnd.mdn = mdn;        
        best_stnd.fil = fil;        
        best_stnd.kun = kun;        
        best_stnd.kun_fil = kun_fil;
        i = 0;

//        dsqlstatus = saufp_class.dbreadfirst ();
        dsqlstatus = DbClass.sqlopen (cursor); 
        dsqlstatus = DbClass.sqlfetch (cursor);

	    if (dsqlstatus == 100)
		{
			    if (kun_fil != 0) return dsqlstatus;

                DbClass.sqlin  ((short *) &mdn, 1, 0);
                DbClass.sqlin  ((short *) &fil, 1, 0);  
                DbClass.sqlin  ((long *) &kun, 2, 0);
                DbClass.sqlout ((char *) kun_bran2, 0, 4);
				dsqlstatus = DbClass.sqlcomm ("select kun_bran2 from kun "
					                  "where mdn = ? "
									  "and fil = ? "
									  "and kun = ?");
				if (dsqlstatus == 100) return dsqlstatus;
				kun = atol (kun_bran2);
				kun_fil = 2;
                best_stnd.kun = kun;        
                best_stnd.kun_fil = kun_fil;
                dsqlstatus = DbClass.sqlopen (cursor); 
                dsqlstatus = DbClass.sqlfetch (cursor); 
		}

        while (dsqlstatus == 0)
        {
			         
                     saufp_class.dbreadfirsta ();
                     sprintf (std_auftab[i].a, "%.0lf", best_stnd.a); 
                     dsqlstatus = lese_a_bas (best_stnd.a);
                     if (dsqlstatus == 0)
                     {
                                 strcpy (std_auftab[i].a_bz1, _a_bas.a_bz1); 
                     }
                     else
                     {
                                 strcpy (std_auftab[i].a_bz1, " "); 
                     }
                     strcpy (std_auftab[i].me_einh_bz, best_stnd.me_einh_bz); 
                     sprintf (std_auftab[i].pr_vk, "%6.2lf", best_stnd.pr_vk); 
                     dlong_to_asc (best_stnd.dat, std_auftab[i].dat); 
                     sprintf (std_auftab[i].me, "%10.2lf", best_stnd.me); 
//                     dsqlstatus = saufp_class.dbread ();
                     dsqlstatus = DbClass.sqlfetch (cursor); 
                     i ++;
        }
		DbClass.sqlclose (cursor);
        std_awanz = i;          
        set_fkt (endsort, 5);
        SetDblClck (IsAwClck, 1);
        SetAktivWindow (hWnd);
        SetListEWindow (1);
 	    SetHLines (1);
	    SetVLines (TRUE);
        SetListButtons (ListBu);
        sprintf (caption, "Standardauftrag f�r Kunden %ld", kun);
        SetCaption (caption); 
        eWindow = OpenListWindowEnF (10, 80, 8, 2, 0);
        EnableWindows (hWnd, FALSE);
        ElistVl (&fsort_awl);
        ElistUb (&fsort_awub);
	    Setlistenter (1);
        ShowElist ((char *) std_auftab,
                    std_awanz,
                   (char *) &std_auf,
                   (int) sizeof (struct STD_AUF),
                   &fstd_auf);
        EnterElist (eWindow, (char *) std_auftab,
                             std_awanz,
                             (char *) &std_auf,
                             (int) sizeof (struct STD_AUF),
                             &fstd_auf);
   	    restore_fkt (5); 
 	    restore_fkt (6);
	    restore_fkt (7);
	    restore_fkt (8);
	    restore_fkt (11);
	    restore_fkt (12);

        EnableWindows (hWnd, TRUE);
        SetActiveWindow (hWnd);
        CloseUbControls (); 
        CloseEWindow (eWindow);
        SetListButtons (FALSE);
        SetListFont (FALSE);
        SetDblClck (NULL, 1);
        if (syskey == KEYESC || syskey == KEY5)
        {
                     return 0;
        }
        if (syskey == KEY8)
        {
                     return 2;
        }
        return 1;
}
   
double StndAuf::GetStda (void)
/**
Ausgewaehlten Standardartikel holen.
**/
{
        return ratod (std_auftab[sort_idx].a);
}
    
double StndAuf::GetStdme (void)
/**
Ausgewaehlten Standardartikel holen.
**/
{
        return ratod (std_auftab[sort_idx].me);
}
    
double StndAuf::GetStdpr_vk (void)
/**
Ausgewaehlten Standardartikel holen.
**/
{
        return ratod (std_auftab[sort_idx].pr_vk);
}

BOOL StndAuf::GetStdRow (int row, double *a, 
                         double *me, double *pr_vk)
/**
Standardwerte fuer eine Position holen.
**/
{
         if (row >= std_awanz)
         {
             return FALSE;
         }
         if (row < 0)
         {
             return FALSE;
         }

         *a     = ratod (std_auftab[row].a);
         *me    = ratod (std_auftab[row].me);
         *pr_vk = ratod (std_auftab[row].pr_vk);
         return TRUE;
}
