#ifndef _MEEINHDLG_DEF
#define _MEEINHDLG_DEF
#include "dlg.h"
#include "itprog.h"
#include "itfont.h"
#include "formfield.h"

#define MDN_CTL 1801
#define FIL_CTL 1802
#define LIEF_CTL 1803
#define A_CTL 1804

#define ME_EINH0_CTL 1901
#define INH0_CTL 1902
#define PR_EK0_CTL 1903

#define ME_EINH1_CTL 1905
#define INH1_CTL 1906
#define KNUEPF1_CTL 1907
#define PR_EK1_CTL 1908

#define ME_EINH2_CTL 1909
#define INH2_CTL 1910
#define KNUEPF2_CTL 1911
#define PR_EK2_CTL 1912

#define ME_EINH3_CTL 1913
#define INH3_CTL 1914
#define KNUEPF3_CTL 1915
#define PR_EK3_CTL 1916

#define ME_EINH4_CTL 1917
#define INH4_CTL 1918
#define KNUEPF4_CTL 1919
#define PR_EK4_CTL 1920

#define ME_EINH5_CTL 1921
#define INH5_CTL 1922
#define KNUEPF5_CTL 1923
#define PR_EK5_CTL 1924

#define ME_EINH6_CTL 1926
#define INH6_CTL 1927
#define KNUEPF6_CTL 1928
#define PR_EK6_CTL 1929

#define IDM_CHOISE 2002

#define HEADCTL 700
#define POSCTL 701
#define FOOTCTL 702

#define RAISEDBORDER 0
#define HIGHBORDER 1
#define LOWBORDER 2
#define HIGHCOLBORDER 3
#define LOWCOLBORDER 4
#define LINEBORDER 5
#include "MeEinhWork.h"
#include "searcha.h"

#define ENTERHEAD 0
#define ENTERA 1
#define ENTERADATA 2

#define LISTBORDER 1
#define LISTLOW 2
#define LISTHI 3

#define IDM_DELALL 5000


class MeEinhDlg : virtual public DLG 
{
          private :
             static char *MeKzCombo[]; 
             static mfont *Font;
             static CFIELD *_fHead [];
             static CFORM fHead;
             static CFIELD* _fPos [];
             static CFORM fPos;
             static CFIELD *_fFoot [];
             static CFORM fFoot;
             static CFIELD *_fMeEinh [];
             static CFORM fMeEinh;
             static CFIELD *_fMeEinh0[]; 
             static CFORM fMeEinh0;

             static ItProg *After [];
             static ItProg *Before [];
             static ItFont *Fonts [];
             static FORMFIELD *dbfields [];
             static FORMFIELD *dbfieldseuro [];
             static FORMFIELD *dbheadfields [];
             static char *EnableHead[];
             static char *EnableHeadFil[];
             static char *EnablePos [];
             static char *MeEinhTab [];
             static char *MeEinhChoiseTab [];
             static char *MeEinhBzTab [];
             int    BorderType;
             static MeEinhWork meEinhWork;
             static int EnterMode;
             static SEARCHA SearchA;
             static char *HelpName;
             static double saveda;
			 static BitImage ImageDelete;
             CFORM *Toolbar2;

             static MeEinhDlg *ActiveMeEinh;
          public :

            static int ListStyle;
            static HBITMAP SelBmp;
	        static COLORREF SysBkColor;

            void SetToolbar2 (CFORM *Toolbar2)
            {
                this->Toolbar2 = Toolbar2;
            }

            CFORM *GetToolbar2 (void)
            {
                return Toolbar2;
            }
 	        MeEinhDlg (int, int, int, int, char *, int, BOOL);
 	        MeEinhDlg (int, int, int, int, char *, int, BOOL, int);
 	        MeEinhDlg (int, int, int, int, char *, int, BOOL, int, int);
 	        void Init (int, int, int, int, char *, int, BOOL);
            void OrPosAttr (DWORD);
            void OrPosAttrEx (DWORD);
            BOOL FillHeadfields (void);
            BOOL OnKeyDown (void);
            BOOL OnKeyUp (void);
            BOOL OnKey1 (void);
            BOOL OnKey2 (void);
            BOOL OnKey3 (void);
            BOOL OnKey4 (void);
            BOOL OnKey5 (void);
            BOOL OnKey6 (void);
            BOOL OnKey7 (void);
            BOOL OnKey9 (void);
            BOOL OnKey10 (void);
            BOOL OnKey12 (void);
            BOOL OnKeyDelete (void);
            BOOL OnKeyPrior (void);
            BOOL OnKeyNext (void);
            BOOL OnKeyHome (void);
            BOOL OnKeyEnd (void);
            BOOL OnKeyReturn (void);
            BOOL OnPaint (HWND,HDC,UINT,WPARAM,LPARAM);
            BOOL OnActivate (HWND,UINT,WPARAM,LPARAM);
            BOOL OnCommand (HWND,UINT,WPARAM,LPARAM);
            BOOL OnSysCommand (HWND,UINT,WPARAM,LPARAM);
            BOOL OnDestroy (HWND,UINT,WPARAM,LPARAM);
            void PrintComment (char *);
            HWND OpenWindow (HANDLE, HWND);
            HWND OpenScrollWindow (HANDLE, HWND);
            void SetWinBackground (COLORREF);
            void ToClipboard (void);
            void FromClipboard (void);
            void Help (void);
            BOOL ShowPtab (char *, char *, char *);
            BOOL ShowMdn (void);
            BOOL ShowFil (void);
            BOOL ShowLief (void);
            BOOL ShowA (void);
            void CallInfo (void);

            static int DeleteRow (void);
            static int CreateLiefBzg (void);
            static BOOL TestLief_bzg (void);
            static int ReadA (void);
            static int ReadMdn (void);
            static int ReadFil (void);
            static int ReadLief (void);
            static int EnterPos (void);
            static int GetPtab (void);
};
#endif
