#ifndef _LBEK_DEF
#define _LBEK_DEF

class LbEk
{
  protected :
     double pr_ek_akt;

  public : 
    LbEk ()
    {
    }

    LbEk (double pr_ek_akt)
    {
        this->pr_ek_akt = pr_ek_akt;
    }
    ~LbEk ()
    {
    }

    void SetPrEkAkt (double pr_ekakt)
    {
        this->pr_ek_akt = pr_ek_akt;
    }
//    void UpdatePr (short, short, double);
    double LastEk (short, short, char *, double, double *, double *);
};
#endif