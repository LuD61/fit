#include "lgr.h"

struct LGR lgr, lgr_null;

void LGR_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &lgr.lgr, 1, 0);
    out_quest ((char *) &lgr.lgr,1,0);
    out_quest ((char *) &lgr.mdn,1,0);
    out_quest ((char *) &lgr.fil,1,0);
    out_quest ((char *) lgr.lgr_smt_kz,0,4);
    out_quest ((char *) lgr.lgr_bz,0,25);
    out_quest ((char *) &lgr.lgr_kz,1,0);
    out_quest ((char *) &lgr.lgr_gr,1,0);
    out_quest ((char *) lgr.lgr_kla,0,2);
    out_quest ((char *) lgr.abr_period,0,2);
    out_quest ((char *) &lgr.adr,2,0);
    out_quest ((char *) &lgr.afl,1,0);
    out_quest ((char *) lgr.bli_kz,0,2);
    out_quest ((char *) &lgr.dat_ero,2,0);
    out_quest ((char *) &lgr.fl_lad,3,0);
    out_quest ((char *) &lgr.fl_nto,3,0);
    out_quest ((char *) &lgr.fl_vk_ges,3,0);
    out_quest ((char *) &lgr.frm,1,0);
    out_quest ((char *) &lgr.iakv,2,0);
    out_quest ((char *) lgr.inv_rht,0,2);
    out_quest ((char *) lgr.ls_abgr,0,2);
    out_quest ((char *) lgr.ls_kz,0,2);
    out_quest ((char *) &lgr.ls_sum,1,0);
    out_quest ((char *) lgr.pers,0,13);
    out_quest ((char *) &lgr.pers_anz,1,0);
    out_quest ((char *) lgr.pos_kum,0,2);
    out_quest ((char *) lgr.pr_ausw,0,2);
    out_quest ((char *) lgr.pr_bel_entl,0,2);
    out_quest ((char *) lgr.pr_lgr_kz,0,2);
    out_quest ((char *) &lgr.pr_lst,2,0);
    out_quest ((char *) lgr.pr_vk_kz,0,2);
    out_quest ((char *) &lgr.reg_bed_theke_lng,3,0);
    out_quest ((char *) &lgr.reg_kt_lng,3,0);
    out_quest ((char *) &lgr.reg_kue_lng,3,0);
    out_quest ((char *) &lgr.reg_lng,3,0);
    out_quest ((char *) &lgr.reg_tks_lng,3,0);
    out_quest ((char *) &lgr.reg_tkt_lng,3,0);
    out_quest ((char *) lgr.smt_kz,0,2);
    out_quest ((char *) &lgr.sonst_einh,1,0);
    out_quest ((char *) &lgr.sprache,1,0);
    out_quest ((char *) lgr.sw_kz,0,2);
    out_quest ((char *) &lgr.tou,2,0);
    out_quest ((char *) &lgr.vrs_typ,1,0);
    out_quest ((char *) lgr.inv_akv,0,2);
    out_quest ((char *) &lgr.delstatus,1,0);
            cursor = prepare_sql ("select lgr.lgr,  lgr.mdn,  "
"lgr.fil,  lgr.lgr_smt_kz,  lgr.lgr_bz,  lgr.lgr_kz,  lgr.lgr_gr,  "
"lgr.lgr_kla,  lgr.abr_period,  lgr.adr,  lgr.afl,  lgr.bli_kz,  lgr.dat_ero,  "
"lgr.fl_lad,  lgr.fl_nto,  lgr.fl_vk_ges,  lgr.frm,  lgr.iakv,  lgr.inv_rht,  "
"lgr.ls_abgr,  lgr.ls_kz,  lgr.ls_sum,  lgr.pers,  lgr.pers_anz,  "
"lgr.pos_kum,  lgr.pr_ausw,  lgr.pr_bel_entl,  lgr.pr_lgr_kz,  lgr.pr_lst,  "
"lgr.pr_vk_kz,  lgr.reg_bed_theke_lng,  lgr.reg_kt_lng,  "
"lgr.reg_kue_lng,  lgr.reg_lng,  lgr.reg_tks_lng,  lgr.reg_tkt_lng,  "
"lgr.smt_kz,  lgr.sonst_einh,  lgr.sprache,  lgr.sw_kz,  lgr.tou,  "
"lgr.vrs_typ,  lgr.inv_akv,  lgr.delstatus from lgr "

#line 11 "lgr.rpp"
                                  "where lgr = ?");

    ins_quest ((char *) &lgr.lgr,1,0);
    ins_quest ((char *) &lgr.mdn,1,0);
    ins_quest ((char *) &lgr.fil,1,0);
    ins_quest ((char *) lgr.lgr_smt_kz,0,4);
    ins_quest ((char *) lgr.lgr_bz,0,25);
    ins_quest ((char *) &lgr.lgr_kz,1,0);
    ins_quest ((char *) &lgr.lgr_gr,1,0);
    ins_quest ((char *) lgr.lgr_kla,0,2);
    ins_quest ((char *) lgr.abr_period,0,2);
    ins_quest ((char *) &lgr.adr,2,0);
    ins_quest ((char *) &lgr.afl,1,0);
    ins_quest ((char *) lgr.bli_kz,0,2);
    ins_quest ((char *) &lgr.dat_ero,2,0);
    ins_quest ((char *) &lgr.fl_lad,3,0);
    ins_quest ((char *) &lgr.fl_nto,3,0);
    ins_quest ((char *) &lgr.fl_vk_ges,3,0);
    ins_quest ((char *) &lgr.frm,1,0);
    ins_quest ((char *) &lgr.iakv,2,0);
    ins_quest ((char *) lgr.inv_rht,0,2);
    ins_quest ((char *) lgr.ls_abgr,0,2);
    ins_quest ((char *) lgr.ls_kz,0,2);
    ins_quest ((char *) &lgr.ls_sum,1,0);
    ins_quest ((char *) lgr.pers,0,13);
    ins_quest ((char *) &lgr.pers_anz,1,0);
    ins_quest ((char *) lgr.pos_kum,0,2);
    ins_quest ((char *) lgr.pr_ausw,0,2);
    ins_quest ((char *) lgr.pr_bel_entl,0,2);
    ins_quest ((char *) lgr.pr_lgr_kz,0,2);
    ins_quest ((char *) &lgr.pr_lst,2,0);
    ins_quest ((char *) lgr.pr_vk_kz,0,2);
    ins_quest ((char *) &lgr.reg_bed_theke_lng,3,0);
    ins_quest ((char *) &lgr.reg_kt_lng,3,0);
    ins_quest ((char *) &lgr.reg_kue_lng,3,0);
    ins_quest ((char *) &lgr.reg_lng,3,0);
    ins_quest ((char *) &lgr.reg_tks_lng,3,0);
    ins_quest ((char *) &lgr.reg_tkt_lng,3,0);
    ins_quest ((char *) lgr.smt_kz,0,2);
    ins_quest ((char *) &lgr.sonst_einh,1,0);
    ins_quest ((char *) &lgr.sprache,1,0);
    ins_quest ((char *) lgr.sw_kz,0,2);
    ins_quest ((char *) &lgr.tou,2,0);
    ins_quest ((char *) &lgr.vrs_typ,1,0);
    ins_quest ((char *) lgr.inv_akv,0,2);
    ins_quest ((char *) &lgr.delstatus,1,0);
            sqltext = "update lgr set lgr.lgr = ?,  lgr.mdn = ?,  "
"lgr.fil = ?,  lgr.lgr_smt_kz = ?,  lgr.lgr_bz = ?,  lgr.lgr_kz = ?,  "
"lgr.lgr_gr = ?,  lgr.lgr_kla = ?,  lgr.abr_period = ?,  lgr.adr = ?,  "
"lgr.afl = ?,  lgr.bli_kz = ?,  lgr.dat_ero = ?,  lgr.fl_lad = ?,  "
"lgr.fl_nto = ?,  lgr.fl_vk_ges = ?,  lgr.frm = ?,  lgr.iakv = ?,  "
"lgr.inv_rht = ?,  lgr.ls_abgr = ?,  lgr.ls_kz = ?,  lgr.ls_sum = ?,  "
"lgr.pers = ?,  lgr.pers_anz = ?,  lgr.pos_kum = ?,  lgr.pr_ausw = ?,  "
"lgr.pr_bel_entl = ?,  lgr.pr_lgr_kz = ?,  lgr.pr_lst = ?,  "
"lgr.pr_vk_kz = ?,  lgr.reg_bed_theke_lng = ?,  lgr.reg_kt_lng = ?,  "
"lgr.reg_kue_lng = ?,  lgr.reg_lng = ?,  lgr.reg_tks_lng = ?,  "
"lgr.reg_tkt_lng = ?,  lgr.smt_kz = ?,  lgr.sonst_einh = ?,  "
"lgr.sprache = ?,  lgr.sw_kz = ?,  lgr.tou = ?,  lgr.vrs_typ = ?,  "
"lgr.inv_akv = ?,  lgr.delstatus = ? "

#line 14 "lgr.rpp"
                                  "where lgr = ?";

            ins_quest ((char *) &lgr.lgr, 1, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &lgr.lgr, 1, 0);
            test_upd_cursor = prepare_sql ("select lgr from lgr "
                                  "where lgr = ?");
            ins_quest ((char *) &lgr.lgr, 1, 0);
            del_cursor = prepare_sql ("delete from lgr "
                                  "where lgr = ?");
    ins_quest ((char *) &lgr.lgr,1,0);
    ins_quest ((char *) &lgr.mdn,1,0);
    ins_quest ((char *) &lgr.fil,1,0);
    ins_quest ((char *) lgr.lgr_smt_kz,0,4);
    ins_quest ((char *) lgr.lgr_bz,0,25);
    ins_quest ((char *) &lgr.lgr_kz,1,0);
    ins_quest ((char *) &lgr.lgr_gr,1,0);
    ins_quest ((char *) lgr.lgr_kla,0,2);
    ins_quest ((char *) lgr.abr_period,0,2);
    ins_quest ((char *) &lgr.adr,2,0);
    ins_quest ((char *) &lgr.afl,1,0);
    ins_quest ((char *) lgr.bli_kz,0,2);
    ins_quest ((char *) &lgr.dat_ero,2,0);
    ins_quest ((char *) &lgr.fl_lad,3,0);
    ins_quest ((char *) &lgr.fl_nto,3,0);
    ins_quest ((char *) &lgr.fl_vk_ges,3,0);
    ins_quest ((char *) &lgr.frm,1,0);
    ins_quest ((char *) &lgr.iakv,2,0);
    ins_quest ((char *) lgr.inv_rht,0,2);
    ins_quest ((char *) lgr.ls_abgr,0,2);
    ins_quest ((char *) lgr.ls_kz,0,2);
    ins_quest ((char *) &lgr.ls_sum,1,0);
    ins_quest ((char *) lgr.pers,0,13);
    ins_quest ((char *) &lgr.pers_anz,1,0);
    ins_quest ((char *) lgr.pos_kum,0,2);
    ins_quest ((char *) lgr.pr_ausw,0,2);
    ins_quest ((char *) lgr.pr_bel_entl,0,2);
    ins_quest ((char *) lgr.pr_lgr_kz,0,2);
    ins_quest ((char *) &lgr.pr_lst,2,0);
    ins_quest ((char *) lgr.pr_vk_kz,0,2);
    ins_quest ((char *) &lgr.reg_bed_theke_lng,3,0);
    ins_quest ((char *) &lgr.reg_kt_lng,3,0);
    ins_quest ((char *) &lgr.reg_kue_lng,3,0);
    ins_quest ((char *) &lgr.reg_lng,3,0);
    ins_quest ((char *) &lgr.reg_tks_lng,3,0);
    ins_quest ((char *) &lgr.reg_tkt_lng,3,0);
    ins_quest ((char *) lgr.smt_kz,0,2);
    ins_quest ((char *) &lgr.sonst_einh,1,0);
    ins_quest ((char *) &lgr.sprache,1,0);
    ins_quest ((char *) lgr.sw_kz,0,2);
    ins_quest ((char *) &lgr.tou,2,0);
    ins_quest ((char *) &lgr.vrs_typ,1,0);
    ins_quest ((char *) lgr.inv_akv,0,2);
    ins_quest ((char *) &lgr.delstatus,1,0);
            ins_cursor = prepare_sql ("insert into lgr (lgr,  "
"mdn,  fil,  lgr_smt_kz,  lgr_bz,  lgr_kz,  lgr_gr,  lgr_kla,  abr_period,  adr,  afl,  "
"bli_kz,  dat_ero,  fl_lad,  fl_nto,  fl_vk_ges,  frm,  iakv,  inv_rht,  ls_abgr,  ls_kz,  "
"ls_sum,  pers,  pers_anz,  pos_kum,  pr_ausw,  pr_bel_entl,  pr_lgr_kz,  pr_lst,  "
"pr_vk_kz,  reg_bed_theke_lng,  reg_kt_lng,  reg_kue_lng,  reg_lng,  "
"reg_tks_lng,  reg_tkt_lng,  smt_kz,  sonst_einh,  sprache,  sw_kz,  tou,  vrs_typ,  "
"inv_akv,  delstatus) "

#line 26 "lgr.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?)"); 

#line 28 "lgr.rpp"
}


