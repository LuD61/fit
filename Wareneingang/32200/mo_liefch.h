#ifndef _MO_LIEFAC_DEF
#define _MO_LIEFAC_DEF
#include "lief_bzg.h"
#include "dbclass.h"
#include "mo_chq.h"
#include "image.h"

struct LIEF_A
{
         char mdn[5];
         char fil[5];
	     char lief [17];
	     char adr_krz [17];
	     char pr_ek [11];
	     int  sortidx;
};

class LIEFAC
{
           private :
			    int SortMode;
                static struct LIEF_A liefa;
                static struct LIEF_A *liefatab;
				static long liefaanz;
                static  CHQ *Choise1;
                static DB_CLASS DbClass;
		        HBITMAP Sel;
		        HBITMAP Msk;
           public :
               LIEFAC ()
               {
                      liefatab = NULL; 
					  SortMode = 0;
               }
               ~LIEFAC ()
               {
                      if (liefatab)
                      {
                          delete liefatab;
                          liefatab = NULL;  
                      }
               }
			   static long Getliefaanz (void);
               static struct LIEF_A *GetLiefaTab (void);
               static struct LIEF_A *GetLiefa (int);
               static int AllArt (int);
               static int ArtOK (int);
               static int LiefaOK (int);
               int Show (HWND, short, short, double, char *, short);
};
#endif

