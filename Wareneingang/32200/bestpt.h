#ifndef _ANGPT_DEF
#define _ANGPT_DEF

#include "dbclass.h"

struct BESTPT {
   long      nr;
   long      zei;
   char      txt[61];
};
extern struct BESTPT bestpt, bestpt_null;

#line 7 "bestpt.rh"

class BESTPT_CLASS : public DB_CLASS 
{
       private :
               int del_cursor_posi;
               void prepare (void);
       public :
               BESTPT_CLASS () : DB_CLASS ()
               {
                        del_cursor_posi = -1;
               }

               ~BESTPT_CLASS ()
               {
	                    this->dbclose ();
               }
               int dbreadfirst (void);
               int delete_bestpposi (void); 
		       void dbclose ();
};
#endif
