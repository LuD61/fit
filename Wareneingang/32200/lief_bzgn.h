#ifndef _LIEF_BZGN_DEF
#define _LIEF_BZGN_DEF

#include "dbclass.h"

struct LIEF_BZGN {
   double    a;
   short     fil;
   char      lief[17];
   char      lief_best[17];
   long      lief_s;
   short     mdn;
   double    pr_ek;
   long      dat;
   char      zeit[7];
   double    pr_ek_eur;
};
extern struct LIEF_BZGN lief_bzgn, lief_bzgn_null;

#line 7 "lief_bzgn.rh"

class LIEF_BZGN_CLASS : public DB_CLASS 
{
       private :
	       int cursor_best;		
	       int cursor_dat;		
	       int cursor_a;		
               void prepare (void);
       public :
               LIEF_BZGN_CLASS () 
               {
                   cursor_best = -1;
                   cursor_dat = -1;
                   cursor_a = -1;
 	       }
               int dbreadfirst_a (); 
               int dbread_a (); 
               int dbreadfirst_best (); 
               int dbread_best (); 
               int dbreadfirst_dat (); 
               int dbread_dat (); 
};
#endif
