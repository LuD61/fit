#include <windows.h>
#include "wmaskc.h"
#include "searchbzg.h"
#include "strfkt.h"
#include "mo_wmess.h"
#include "lbox.h"
#include "dlglst.h"
#include "a_bas.h"
#include "lbox.h"


struct SBZG *SEARCHBZG::sbzgtab = NULL;
struct SBZG SEARCHBZG::sbzg;
int SEARCHBZG::idx = -1;
long SEARCHBZG::anz;
CHQEX *SEARCHBZG::Query = NULL;
DB_CLASS SEARCHBZG::DbClass; 
HINSTANCE SEARCHBZG::hMainInst;
HWND SEARCHBZG::hMainWindow;
HWND SEARCHBZG::awin;
int SEARCHBZG::SearchField = 1;
int SEARCHBZG::soa = 1; 
int SEARCHBZG::soa_bz1 = 1; 
int SEARCHBZG::solief_best = 1; 
int SEARCHBZG::sopr_ek = 1; 
int SEARCHBZG::some_einh = 1; 
short SEARCHBZG::mdn = 0; 
PTAB_CLASS SEARCHBZG::Ptab;


ITEM SEARCHBZG::ua         ("a",         "Artikel",      "", 0);  
ITEM SEARCHBZG::ua_bz1     ("a_bz1",     "Bezeichnung",  "", 0);  
ITEM SEARCHBZG::ulief_best ("lief_best", "Bestellnr.",   "", 0);  
ITEM SEARCHBZG::upr_ek     ("pr_ek",     "EK",           "", 0);  
ITEM SEARCHBZG::ume_einh   ("me_einh",  "Einheit",      "", 0);  


field SEARCHBZG::_UbForm[] = {
&ua,         15, 0, 0,  0,  NULL, "", BUTTON, 0, 0, 0,     
&ua_bz1,     46, 0, 0, 15 , NULL, "", BUTTON, 0, 0, 0,     
&ulief_best, 18, 0, 0, 61 , NULL, "", BUTTON, 0, 0, 0,     
&upr_ek,      9, 0, 0, 79 , NULL, "", BUTTON, 0, 0, 0,     
&ume_einh,   10, 0, 0, 88 , NULL, "", BUTTON, 0, 0, 0,     
};

form SEARCHBZG::UbForm = {5, 0, 0, _UbForm, 0, 0, 0, 0, NULL};


ITEM SEARCHBZG::ia         ("a",         sbzg.a,          "", 0);  
ITEM SEARCHBZG::ia_bz1     ("a_bz1",     sbzg.a_bz1,      "", 0);  
ITEM SEARCHBZG::ilief_best ("lief_best", sbzg.lief_best,  "", 0);  
ITEM SEARCHBZG::ipr_ek     ("pr_ek",     sbzg.pr_ek,      "", 0);  
ITEM SEARCHBZG::ime_einh   ("me_einh",   sbzg.me_einh,    "", 0);  

field SEARCHBZG::_DataForm[] = {
&ia,        13, 0, 0,  1,  NULL, "%13.0f", DISPLAYONLY, 0, 0, 0,     
&ia_bz1,    44, 0, 0, 16 , NULL, "",       DISPLAYONLY, 0, 0, 0,     
&ilief_best,16, 0, 0, 62 , NULL, "",       DISPLAYONLY, 0, 0, 0,     
&ipr_ek,     6, 0, 0, 80 , NULL, "%6.2f",  DISPLAYONLY, 0, 0, 0,     
&ime_einh,   8, 0, 0, 89 , NULL, "",       DISPLAYONLY, 0, 0, 0,     
};

form SEARCHBZG::DataForm = {5, 0, 0, _DataForm, 0, 0, 0, 0, NULL};

ITEM SEARCHBZG::iline ("", "1", "", 0);

field SEARCHBZG::_LineForm[] = {
&iline,       1, 0, 0, 15 , NULL, "", DISPLAYONLY, 0, 0, 0,     
&iline,       1, 0, 0, 61 , NULL, "", DISPLAYONLY, 0, 0, 0,     
&iline,       1, 0, 0, 79 , NULL, "", DISPLAYONLY, 0, 0, 0,     
&iline,       1, 0, 0, 88 , NULL, "", DISPLAYONLY, 0, 0, 0,     
};

form SEARCHBZG::LineForm = {4, 0, 0, _LineForm, 0, 0, 0, 0, NULL};

char *SEARCHBZG::query = NULL;


int SEARCHBZG::sorta (const void *elem1, const void *elem2)
{
	      struct SBZG *el1; 
	      struct SBZG *el2; 

		  el1 = (struct SBZG *) elem1;
		  el2 = (struct SBZG *) elem2;
          clipped (el1->a);
          clipped (el2->a);

          if (ratod (el1->a) > ratod (el2->a))
          {
              return 1 * soa;
          }
          else if (ratod (el1->a) < ratod (el2->a))
          {
              return -1 * soa;
          }

          return 0;
}

void SEARCHBZG::SortA (HWND hWnd)
{
   	   qsort (sbzgtab, anz, sizeof (struct SBZG),
				   sorta);
       soa *= -1;
}

int SEARCHBZG::sorta_bz1 (const void *elem1, const void *elem2)
{
	      struct SBZG *el1; 
	      struct SBZG *el2; 

		  el1 = (struct SBZG *) elem1;
		  el2 = (struct SBZG *) elem2;
          clipped (el1->a_bz1);
          clipped (el2->a_bz1);
          if (strlen (el1->a_bz1) == 0 &&
              strlen (el2->a_bz1) == 0)
          {
              return 0;
          }
          if (strlen (el1->a_bz1) == 0)
          {
              return -1;
          }
          if (strlen (el2->a_bz1) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->a_bz1,el2->a_bz1) * soa_bz1);
}

void SEARCHBZG::SortABz1 (HWND hWnd)
{
   	   qsort (sbzgtab, anz, sizeof (struct SBZG),
				   sorta_bz1);
       soa_bz1 *= -1;
}

int SEARCHBZG::sortlief_best (const void *elem1, const void *elem2)
{
	      struct SBZG *el1; 
	      struct SBZG *el2; 

		  el1 = (struct SBZG *) elem1;
		  el2 = (struct SBZG *) elem2;
          clipped (el1->lief_best);
          clipped (el2->lief_best);
          if (strlen (el1->lief_best) == 0 &&
              strlen (el2->lief_best) == 0)
          {
              return 0;
          }
          if (strlen (el1->lief_best) == 0)
          {
              return -1;
          }
          if (strlen (el2->lief_best) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->lief_best,el2->lief_best) * solief_best);
}

void SEARCHBZG::SortLiefBest (HWND hWnd)
{
   	   qsort (sbzgtab, anz, sizeof (struct SBZG),
				   sortlief_best);
       solief_best *= -1;
}


int SEARCHBZG::sortpr_ek (const void *elem1, const void *elem2)
{
	      struct SBZG *el1; 
	      struct SBZG *el2; 

		  el1 = (struct SBZG *) elem1;
		  el2 = (struct SBZG *) elem2;
          clipped (el1->pr_ek);
          clipped (el2->pr_ek);

          if (ratod (el1->pr_ek) > ratod (el2->pr_ek))
          {
              return 1 * sopr_ek;
          }
          else if (ratod (el1->pr_ek) < ratod (el2->pr_ek))
          {
              return -1 * sopr_ek;
          }

          return 0;
}

void SEARCHBZG::SortPrEk (HWND hWnd)
{
   	   qsort (sbzgtab, anz, sizeof (struct SBZG),
				   sortpr_ek);
       sopr_ek *= -1;
}

int SEARCHBZG::sortme_einh (const void *elem1, const void *elem2)
{
	      struct SBZG *el1; 
	      struct SBZG *el2; 

		  el1 = (struct SBZG *) elem1;
		  el2 = (struct SBZG *) elem2;
          clipped (el1->me_einh);
          clipped (el2->me_einh);
          if (strlen (el1->me_einh) == 0 &&
              strlen (el2->me_einh) == 0)
          {
              return 0;
          }
          if (strlen (el1->me_einh) == 0)
          {
              return -1;
          }
          if (strlen (el2->me_einh) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->me_einh,el2->me_einh) * some_einh);
}

void SEARCHBZG::SortMeEinh (HWND hWnd)
{
   	   qsort (sbzgtab, anz, sizeof (struct SBZG),
				   sortme_einh);
       some_einh *= -1;
}


void SEARCHBZG::SortLst (int Col, HWND hWnd)
/**
Procedure zum Sortiern der Liste.
**/
{
       switch (Col)
       {
              case 0 :
                  SortA (hWnd);
                  SearchField = 0;
                  break;
              case 1 :
                  SortABz1 (hWnd);
                  SearchField = 1;
                  break;
              case 2 :
                  SortLiefBest (hWnd);
                  SearchField = 2;
                  break;
              case 3 :
                  SortPrEk (hWnd);
                  SearchField = 3;
                  break;
              case 4 :
                  SortMeEinh (hWnd);
                  SearchField = 4;
                  break;
              default :
                  return;
       }
       UpdateList ();
}

void SEARCHBZG::FillFormat (char *buffer, int size)
{
        DlgLst::FillFrmAttr (&DataForm, buffer, size);
}


void SEARCHBZG::FillCaption (char *buffer, int size)
{
        DlgLst::FillFrmTitle (&UbForm, buffer, 0, size);
}

void SEARCHBZG::FillVlines (char *buffer, int size) 
{
        DlgLst::FillFrmLines (&LineForm, buffer, 0, size);
}


void SEARCHBZG::FillRec (char *buffer, int i, int size)
{
          DlgLst::FillFrmData (&DataForm, buffer, 0, size);
}


void SEARCHBZG::UpdateList (void)
{
       int i;
 	   char buffer [512];

       for (i = 0; i < anz; i ++)
       {
		  memcpy (&sbzg, &sbzgtab[i],  sizeof (struct SBZG));
          FillRec (buffer, i, 512);
	      Query->UpdateRecord (buffer, i);
       }
}

int SEARCHBZG::SearchLst (char *sebuff)
/**
Nach Kurzname in Liste suchen.
**/
{
/*
	   Query->SearchList ();
	   return 0;
*/

	   int i;
	   int len;
       char sabuff [80];
       char *pos;

	   if (sbzgtab == NULL) return 0;
	   if (strlen (sebuff) == 0) 
	   {
	       Query->SetSel (0);
		   return 0;
	   }

	   for (i = 0; i < anz; i ++)
	   {
		   len = min (16, strlen (sebuff));
           if (SearchField == 0)
           {
                   strcpy (sabuff, sbzgtab[i].a);
                   for (pos = sabuff; *pos <= ' ' && *pos != 0 ; pos ++);
                   pos[len] = 0;
                   strcpy (sabuff, pos);
                   if (ratod (sebuff) == ratod (sabuff)) break;
           }
           else if (SearchField == 1)
           {
		           if (strupcmp (sebuff, sbzgtab[i].a_bz1, len) == 0) break;
           }
           else if (SearchField == 2)
           {
		           if (strupcmp (sebuff, sbzgtab[i].lief_best, len) == 0) break;
           }
           else if (SearchField == 3)
           {
                   strcpy (sabuff, sbzgtab[i].pr_ek);
                   for (pos = sabuff; *pos <= ' ' && *pos != 0 ; pos ++);
                   pos[len] = 0;
                   strcpy (sabuff, pos);
                   if (ratod (sebuff) == ratod (sabuff)) break;
           }
           else if (SearchField == 4)
           {
		           if (strupcmp (sebuff, sbzgtab[i].me_einh, len) == 0) break;
           }
	   }
	   if (i == anz) return 0;
	   Query->SetSel (i);
	   return 0;
}


int SEARCHBZG::Read (char *squery)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  char bufa [40];
	  int cursor;
      double akt_a = 0.0;
      double a = 0.0;
	  int i;
      WMESS Wmess;


	  if (sbzgtab) 
	  {
           delete sbzgtab;
           sbzgtab = NULL;
	  }

      idx = -1;
	  anz = 0;
      SearchField = 1;
      Query->SetSBuff (NULL);
      clipped (squery);
	  if (numeric (squery))
	  {
		  a = ratod(squery);
		  strcpy (squery,"");
	  }
      if (strcmp (squery, " ") == 0) squery[0] = 0;

      if (anz == 0) anz = 0x10000;

	  sbzgtab = new struct SBZG [anz];
		 if (squery == NULL || 
          strlen (squery) == 0)
      {
             sprintf (buffer, "select lief_bzg.a, a_bas.a_bz1,a_bas.a_bz2, lief_bzg.lief_best, "
                              "pr_ek_eur,  me_einh_ek, lief_bzg.mdn, lief_bzg.fil "
	 			       "from lief_bzg,a_bas "
                       "where a_bas.a = lief_bzg.a");
              if (query != NULL && strlen (query) > 0)
              {
                  strcat (buffer, " and ");
                  strcat (buffer, query);
				  if (a > double(0))
				  {
					  sprintf (bufa," and lief_bzg.a = %.0f",a);
					  strcat (buffer,bufa);
				  }

              }

              strcat (buffer, " order by lief_bzg.a, lief_bzg.mdn desc, lief_bzg.fil desc"); 
      }
      else
      {
             sprintf (buffer, "select lief_bzg.a, a_bas.a_bz1,a_bas.a_bz2, lief_bzg.lief_best, "
                              "pr_ek_eur,  me_einh_ek, lief_bzg.mdn, lief_bzg.fil "
	 			       "from lief_bzg,a_bas "
                       "where a_bas.a = lief_bzg.a "
                       "and a_bas.a_bz1 matches \"%s*\"", squery);
              if (query != NULL && strlen (query) > 0)
              {
                  strcat (buffer, " and ");
                  strcat (buffer, query);
              }
              strcat (buffer, " order by lief_bzg.a, lief_bzg.mdn desc, lief_bzg.fil desc"); 
      }
      DbClass.sqlout ((char *) sbzg.a, 0, 14);
      DbClass.sqlout ((char *) sbzg.a_bz1, 0, 25);
      DbClass.sqlout ((char *) sbzg.a_bz2, 0, 25);
      DbClass.sqlout ((char *) sbzg.lief_best, 0, 17);
      DbClass.sqlout ((char *) sbzg.pr_ek, 0, 12);
      DbClass.sqlout ((char *) sbzg.me_einh, 0, 11);
      cursor = DbClass.sqlcursor (buffer);
      if (cursor < 0) 
      {
          return -1;
      }

  	  i = 0;
	  while (DbClass.sqlfetch (cursor) == 0)
      {
/*
          strcpy (_a_bas.a_bz1, "");
          lese_a_bas (ratod (sbzg.a));
          strcpy (sbzg.a_bz1, _a_bas.a_bz1);
*/
/* //070406
          if (akt_a == ratod (sbzg.a))
          {
              continue;
          }
*/

          akt_a = ratod (sbzg.a);
          Ptab.lese_ptab ("me_einh_ek", clipped (sbzg.me_einh));
          strcpy (sbzg.me_einh, ptabn.ptbezk);
		  sprintf (sbzg.a_bz1,"%s %s",clipped(sbzg.a_bz1), clipped(sbzg.a_bz2)); 
		  memcpy (&sbzgtab[i], &sbzg, sizeof (struct SBZG));
          FillRec (buffer, i, 512);
	      Query->InsertRecord (buffer);
		  i ++;
	  }
      anz = i;
	  DbClass.sqlclose (cursor);

      soa = 1;
      if (anz > 1)
      {
             SortA (Query->GethWnd ());
      }
      UpdateList ();

	  EnableWindow (awin, FALSE);
	  return 0;
}

void SEARCHBZG::FillBox (void)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int i;

      for (i = 0; i < anz; i ++)
      {
		  memcpy (&sbzg, &sbzgtab[i],  sizeof (struct SBZG));
          FillRec (buffer, i, 512);
	      Query->InsertRecord (buffer);
      }
      if (idx > -1)
      {
	      Query->SetSel (idx);
      }
}


void SEARCHBZG::SetParams (HINSTANCE hMainInst, HWND hMainWindow)
{
      this->hMainInst   = hMainInst;
      this->hMainWindow = hMainWindow;
      awin = hMainWindow;
}


void SEARCHBZG::Search (short mdn)
{
  	  int cx, cy;
	  char buffer [512];
      RECT rect;
      BOOL doret = FALSE; 

      if (mdn == 0)
      {
          disp_mess ("Mandant 0 ist nicht erlaubt", 2);
          syskey = KEY5;
          return;
      }
      this->mdn = mdn;
      Settchar ('|');
      GetClientRect (hMainWindow, &rect);
      cx = 100;
      cy = 25;
      SetSortProc (SortLst);
      if (Query == NULL)
      {
        idx = -1;
        if (query != NULL && strlen (query) > 0)
        {
               Query = new CHQEX (cx, cy);
        }
        else
        {
               Query = new CHQEX (cx, cy);
//               Query = new CHQEX (cx, cy, "Name", "");
        }
        Query->EnableSort (TRUE);
        if (SetStyle)
        {
            Query->SetWindowStyle (WindowStyle);
        }
        Query->OpenWindow (hMainInst, hMainWindow, TRUE);
        FillFormat (buffer, 512);
        Query->RowAttr (buffer);
        FillVlines (buffer, 512);
	    Query->VLines (buffer, 3);
	    EnableWindow (awin, FALSE);

        FillCaption (buffer, 512);
	    Query->InsertCaption (buffer);

	    Query->SetFillDb (Read);
	    Query->SetSearchLst (SearchLst);
//        if (query != NULL && strlen (query) > 0)
        {
              Read ("");
              if (anz == 0)
              {
                  doret = TRUE;
              }
        }
      }
      else
      {
        Query->RestorefWork ();
        if (SetStyle)
        {
            Query->SetWindowStyle (WindowStyle);
        }
        Query->OpenWindow (hMainInst, hMainWindow, TRUE);
        FillFormat (buffer, 512);
        Query->RowAttr (buffer);
        FillVlines (buffer, 512);
	    Query->VLines (buffer, 3);
	    EnableWindow (awin, FALSE);

        FillCaption (buffer, 512);
	    Query->InsertCaption (buffer);

	    Query->SetFillDb (Read);
	    Query->SetSearchLst (SearchLst);
        FillBox ();
      }
      if (doret == FALSE)
      {
          Query->ProcessMessages ();
          idx = Query->GetSel ();
      }
      else
      {
          idx = -1;
      }
      query = NULL;
      EnableWindow (awin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (awin);
      if (sbzgtab == NULL)
      {
          delete Query;
          Query = NULL;
      }
	  if (idx == -1) return;
	  memcpy (&sbzg, &sbzgtab[idx], sizeof (sbzg));
      SetSortProc (NULL);
      if (sbzgtab != NULL)
      {
          Query->SavefWork ();
      }
}

void SEARCHBZG::Search (short mdn, char* ca)
{
  	  int cx, cy;
	  char buffer [512];
      RECT rect;
      BOOL doret = FALSE; 

      if (mdn == 0)
      {
          disp_mess ("Mandant 0 ist nicht erlaubt", 2);
          syskey = KEY5;
          return;
      }
      this->mdn = mdn;
      Settchar ('|');
      GetClientRect (hMainWindow, &rect);
      cx = 80;
      cy = 25;
      SetSortProc (SortLst);
      if (Query == NULL)
      {
        idx = -1;
        if (query != NULL && strlen (query) > 0)
        {
               Query = new CHQEX (cx, cy);
        }
        else
        {
               Query = new CHQEX (cx, cy);
//               Query = new CHQEX (cx, cy, "Name", "");
        }
        Query->EnableSort (TRUE);
        if (SetStyle)
        {
            Query->SetWindowStyle (WindowStyle);
        }
        Query->OpenWindow (hMainInst, hMainWindow, TRUE);
        FillFormat (buffer, 512);
        Query->RowAttr (buffer);
        FillVlines (buffer, 512);
	    Query->VLines (buffer, 3);
	    EnableWindow (awin, FALSE);

        FillCaption (buffer, 512);
	    Query->InsertCaption (buffer);

	    Query->SetFillDb (Read);
	    Query->SetSearchLst (SearchLst);
//        if (query != NULL && strlen (query) > 0)
        {
              Read (ca);
              if (anz == 0)
              {
                  doret = TRUE;
              }
        }
      }
      if (doret == FALSE)
      {
          Query->ProcessMessages ();
          idx = Query->GetSel ();
      }
      else
      {
          idx = -1;
      }
      query = NULL;
      EnableWindow (awin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (awin);
      if (sbzgtab == NULL)
      {
          delete Query;
          Query = NULL;
      }
	  if (idx == -1) return;
	  memcpy (&sbzg, &sbzgtab[idx], sizeof (sbzg));
      SetSortProc (NULL);
      if (sbzgtab != NULL)
      {
          Query->SavefWork ();
      }
}

