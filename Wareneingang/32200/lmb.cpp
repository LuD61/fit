#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "lmb.h"
#include "dbclass.h"
#include "dbfunc.h"


static char *sqltext;

void LMB_CLASS::prepare (void)
{
         LIEFMEBEST_CLASS::prepare ();

         ins_quest ((char *) &liefmebest.mdn, 1, 0);
         ins_quest ((char *) &liefmebest.fil, 1, 0);
         ins_quest ((char *) &liefmebest.a, 3, 0);
    out_quest ((char *) &liefmebest.mdn,1,0);
    out_quest ((char *) &liefmebest.fil,1,0);
    out_quest ((char *) liefmebest.lief,0,17);
    out_quest ((char *) &liefmebest.a,3,0);
    out_quest ((char *) &liefmebest.tara0,3,0);
    out_quest ((char *) &liefmebest.me_einh0,1,0);
    out_quest ((char *) &liefmebest.inh0,3,0);
    out_quest ((char *) &liefmebest.me_einh1,1,0);
    out_quest ((char *) &liefmebest.inh1,3,0);
    out_quest ((char *) &liefmebest.tara1,3,0);
    out_quest ((char *) &liefmebest.me_einh2,1,0);
    out_quest ((char *) &liefmebest.inh2,3,0);
    out_quest ((char *) &liefmebest.tara2,3,0);
    out_quest ((char *) &liefmebest.me_einh3,1,0);
    out_quest ((char *) &liefmebest.inh3,3,0);
    out_quest ((char *) &liefmebest.tara3,3,0);
    out_quest ((char *) &liefmebest.me_einh4,1,0);
    out_quest ((char *) &liefmebest.inh4,3,0);
    out_quest ((char *) &liefmebest.tara4,3,0);
    out_quest ((char *) &liefmebest.me_einh5,1,0);
    out_quest ((char *) &liefmebest.inh5,3,0);
    out_quest ((char *) &liefmebest.tara5,3,0);
    out_quest ((char *) &liefmebest.me_einh6,1,0);
    out_quest ((char *) &liefmebest.inh6,3,0);
    out_quest ((char *) &liefmebest.tara6,3,0);
    out_quest ((char *) &liefmebest.knuepf1,1,0);
    out_quest ((char *) &liefmebest.knuepf2,1,0);
    out_quest ((char *) &liefmebest.knuepf3,1,0);
    out_quest ((char *) &liefmebest.knuepf4,1,0);
    out_quest ((char *) &liefmebest.knuepf5,1,0);
    out_quest ((char *) &liefmebest.knuepf6,1,0);
    out_quest ((char *) &liefmebest.ean_su1,3,0);
    out_quest ((char *) &liefmebest.ean_su2,3,0);
    out_quest ((char *) &liefmebest.pr_ek0,3,0);
    out_quest ((char *) &liefmebest.pr_ek1,3,0);
    out_quest ((char *) &liefmebest.pr_ek2,3,0);
    out_quest ((char *) &liefmebest.pr_ek3,3,0);
    out_quest ((char *) &liefmebest.pr_ek4,3,0);
    out_quest ((char *) &liefmebest.pr_ek5,3,0);
    out_quest ((char *) &liefmebest.pr_ek6,3,0);
         cursor_a = prepare_sql ("select liefmebest.mdn,  "
"liefmebest.fil,  liefmebest.lief,  liefmebest.a,  liefmebest.tara0,  "
"liefmebest.me_einh0,  liefmebest.inh0,  liefmebest.me_einh1,  "
"liefmebest.inh1,  liefmebest.tara1,  liefmebest.me_einh2,  "
"liefmebest.inh2,  liefmebest.tara2,  liefmebest.me_einh3,  "
"liefmebest.inh3,  liefmebest.tara3,  liefmebest.me_einh4,  "
"liefmebest.inh4,  liefmebest.tara4,  liefmebest.me_einh5,  "
"liefmebest.inh5,  liefmebest.tara5,  liefmebest.me_einh6,  "
"liefmebest.inh6,  liefmebest.tara6,  liefmebest.knuepf1,  "
"liefmebest.knuepf2,  liefmebest.knuepf3,  liefmebest.knuepf4,  "
"liefmebest.knuepf5,  liefmebest.knuepf6,  liefmebest.ean_su1,  "
"liefmebest.ean_su2,  liefmebest.pr_ek0,  liefmebest.pr_ek1,  "
"liefmebest.pr_ek2,  liefmebest.pr_ek3,  liefmebest.pr_ek4,  "
"liefmebest.pr_ek5,  liefmebest.pr_ek6 from liefmebest "

#line 27 "lmb.rpp"
                               "where mdn = ? "
                               "and   fil = ? "
                               "and   (lief <= \" \" "
							   "or    lief is NULL) "
                               "and   a   = ? ");
}

int LMB_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return LIEFMEBEST_CLASS::dbreadfirst ();
}


int LMB_CLASS::dbreadfirst_a (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         open_sql (cursor_a);
         fetch_sql (cursor_a);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int LMB_CLASS::dbread_a (void)
/**
Naechsten Satz aus Tabelle lesen.
**/
{
         fetch_sql (cursor_a);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}
 
 
