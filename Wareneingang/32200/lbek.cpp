#include <windows.h>
// #include "a_kalkhndw.h"
#define CONSOLE 1
#include "dbclass.h"
#include "lbek.h"

// Wird im Moment nicht benutzt.

/*
void LbEk::UpdatePr (short mdn, short fil, double a)
{
    A_KALKHNDW_CLASS AKalkHndw;

    a_kalkhndw.mdn = mdn;
    a_kalkhndw.fil = fil;
    a_kalkhndw.a   = a;
    int dsqlstatus = AKalkHndw.dbreadfirst ();
    while (dsqlstatus == 100)
    {
        if (a_kalkhndw.fil > 0)
        {
            a_kalkhndw.fil = 0;
        }
        else if (a_kalkhndw.mdn > 0)
        {
            a_kalkhndw.mdn = 0;
        }
        else
        {
            break;
        }
        int dsqlstatus = AKalkHndw.dbreadfirst ();
    }
    if (dsqlstatus < 0)
    {
        return;
    }

    if (dsqlstatus == 0)
    {
        a_kalkhndw.pr_ek3 = a_kalkhndw.pr_ek2;
        a_kalkhndw.pr_ek2 = a_kalkhndw.pr_ek1;
    }
    a_kalkhndw.pr_ek1 = pr_ek_akt;
    AKalkHndw.dbupdate ();
}
*/

double LbEk::LastEk (short mdn, short fil, char *lief, double a, double *pr_ek_euro,
                     double *pr_ek)
{
    DB_CLASS DbClass;
    double pr_ek_bto = 0.0;
    double pr_ek_euro_bto = 0.0;

    DbClass.sqlin ((short *) &mdn, 1, 0);
    DbClass.sqlin ((short *) &fil, 1, 0);
    DbClass.sqlin ((char *)  lief, 0, 17);
    DbClass.sqlin ((double *) &a, 3, 0);
    DbClass.sqlout ((double *) &pr_ek_euro_bto, 3, 0);
    DbClass.sqlout ((double *) &pr_ek_bto, 3, 0);
    int dsqlstatus = DbClass.sqlcomm ("select best_pos.pr_ek_euro_bto, best_pos.pr_ek_bto, "
                                      "best_kopf.best_term "
                                      "from best_kopf, best_pos "
                                      "where best_kopf.mdn = ? "
                                      "and best_kopf.fil = ? "
                                      "and best_kopf.lief = ? "
                                      "and best_pos.a = ? "
                                      "and best_pos.mdn = best_kopf.mdn "
                                      "and best_pos.lief = best_kopf.lief "
                                      "and best_pos.best_blg = best_kopf.best_blg "
                                      "order by best_term desc");
    *pr_ek_euro = pr_ek_euro_bto;
    *pr_ek      = pr_ek_bto;
    return pr_ek_euro_bto;
}





