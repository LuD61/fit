#include <windows.h>
#include "searchlgr.h"
#include "strfkt.h"
#include "mo_wmess.h"
#include "lbox.h"


int SEARCHLGR::idx;
long SEARCHLGR::anz;
CHQEX *SEARCHLGR::Query = NULL;
DB_CLASS SEARCHLGR::DbClass; 
HINSTANCE SEARCHLGR::hMainInst;
HWND SEARCHLGR::hMainWindow;
HWND SEARCHLGR::awin;
short SEARCHLGR::mdn_nr = 0;


int SEARCHLGR::SearchLst (char *sebuff)
/**
Nach Kurzname in Liste suchen.
**/
{
	   Query->SearchList ();
	   return 0;
}

int SEARCHLGR::ReadLst (char *name)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int cursor;
	  int i;
      WMESS Wmess;
      long lgr;
      char lgr_bz [25];


	  anz = 0;
      clipped (name);
      if (strcmp (name, " ") == 0) name[0] = 0;

      if (strlen (name) == 0 ||
          name[0] <= ' ')
      {
	          sprintf (buffer, "select lgr, lgr.lgr_bz "
	 			       "from lgr "
                       "where lgr > 0");
      }
      else
      {
	          sprintf (buffer, "select lgr, lgr.lgr_bz "
	 			       "from lgr "
                       "where lgr_bz matches \"%s\" ",
                        name);
      }
      DbClass.sqlout ((short *) &lgr, 2, 0);
      DbClass.sqlout ((char *) lgr_bz, 0, 25);
      cursor = DbClass.sqlcursor (buffer);
      if (cursor < 0) 
      {
          return -1;
      }
  	  i = 0;
      Wmess.Message (hMainInst, hMainWindow, " \nBitte warten.....\n"
  	                                         "Die Daten werden selektiert.\n ");  
	  while (DbClass.sqlfetch (cursor) == 0)
      {
 	      sprintf (buffer, "    %8d  |%-24s|", lgr, lgr_bz);
	      Query->InsertRecord (buffer);
		  i ++;
      }
      anz = i;
	  DbClass.sqlclose (cursor);
 	  Wmess.Destroy ();
	  EnableWindow (awin, FALSE);
	  return 0;
}

void SEARCHLGR::SetParams (HINSTANCE hMainInst, HWND hMainWindow)
{
      this->hMainInst   = hMainInst;
      this->hMainWindow = hMainWindow;
      awin = hMainWindow;
}

BOOL SEARCHLGR::GetKey (short *key1)
{
      int anz;
     
      if (idx == -1) return FALSE;
      anz = wsplit (Key, "|");
      if (anz < 2) return FALSE;
      *key1 = atoi (wort[0]);
      return TRUE;
}


void SEARCHLGR::Search (void)
{
  	  int cx, cy;
	  char buffer [256];

	  idx = -1;
      cx = 48;
      cy = 25;
      Settchar ('|');
      EnableWindow (hMainWindow, FALSE);
      Query = new CHQEX (cx, cy);
      Query->EnableSort (TRUE);
      Query->OpenWindow (hMainInst, hMainWindow);
      sprintf (buffer, "%s %s",
                          "%d" 
                          "%s");
      Query->RowAttr (buffer);
	  sprintf (buffer, "    %10s", "1"); 
	  Query->VLines (buffer, 3);
      sprintf (buffer, "8;8 11;24");
      Query->RowPos (buffer);
	  EnableWindow (awin, FALSE);

	  sprintf (buffer, "    %8s  %-26s", "LgrNr", "Bezeichnung"); 
	  Query->InsertCaption (buffer);

	  Query->SetFillDb (ReadLst);
	  Query->SetSearchLst (SearchLst);
      ReadLst ("");
      Query->SetSortRow (1, TRUE);
	  Query->ProcessMessages ();
      EnableWindow (hMainWindow, TRUE);
      if (syskey != KEY5)
      {
              idx = Query->GetSel ();
              if (idx != -1)
              {
                     Query->GetText (Key);
              }
      }
	  EnableWindow (awin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (awin);
}

