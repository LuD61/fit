[Runtime]
version = 5.0
elistw = scrolled, bordered  
push30 = reverse
push29 = reverse
funktionstasten = -red0 -green0 -blue120 reverse
fktinfo = reverse
seite =  reverse
fktstandard = -red255 -green0 -blue0  reverse
;fktstandard = reverse
blau = -red0 -green0 -blue120
KEY4 = " INFO "
KEY29 = ":"
KEY30 = "."
KEY6 = "6"
KEY7 = "7"
KEY8 = "8"
KEY9 = "9"
KEY10 = "10"
KEY11 = "11"
KEYHELP = "   Hilfe   "
KEYNEXT = ">"
KEYPREV = "<"
KEY5 = "  Abbruch "
KEY17 = "  Flucht   "
KEY12 = " Ausf�hren"
KEY2 = "  Zur�ck  "
;KEYPREV = "  Zur�ck  "
KEY3 = "   Vor    "
;KEYNEXT = "   Vor    "
;mamain1 = scrolled
wiakti = -red255 -green0 -blue0  
tasklines = 25
cellwidth = 10
;cellwidth = 12
;cellheight = 29
cellheight = 23
;cellheight = 18
tcptimeout = -3
compatmode=2
;font=ms sans serif
font=courier
rowadjust=0
[Debugger]

[Printer]
cellheight = 16
reverse=,-s
underline=,-u
highlight=,-b
blink=,-i
yellow=, 10 -v1
magenta=,8 -v3
red=,6 -v7
cyan=, *10
green=,8
blue=,8 -v5
pmode0=Courier,12
pmode1=,10 -v1
pmode2=,8 -v3
pmode3=,6 -v4
pmode4=, *10
pmode5=Helvetica
pmode6=Times New Roman
pmode7=Arial
pmode8=MS Sans Serif

[HP DeskJet Series v2.1]
pmode0=Courier,12 -red255 -green255 -blue255
white=, -red0 -green0 -blue0
red=, -red255 -green0 -blue0
green=,  -red0 -green255 -blue0
blue=,  -red0 -green0 -blue255
yellow= , -red0 -green255 -blue255

[wl]
; 
; Kommando-Interpreter
; ====================
;
; Windows 95
;
;shell=command.com /E:10240 /C
;
; Windows NT
;
;shell=cmd.exe /c
;
;
; Schriftart und Geometrie
; ========================
;
;
font=courier
;
; 1024x768
;
;cellWidth=                     10
;cellHeight=                    23
;
; 800x600
;
cellHeight=                     18
cellWidth=                      9
;
; Hintergrundfarben, Rahmen und Scroll-Bars
; =========================================
;
KwMenu=-r192 -g192 -b192
HistMenu= bordered, -r192 -g192 -b192
ProjektMenu=bordered, -r192 -g192 -b192
w_suchkriterium=bordered
w_projektneu=bordered
wd_get_string=bordered
wd_get_smallint=bordered
ParameterMenu=bordered
CompParaMenu=bordered
LinkParaMenu=bordered
RuntimeParaMenu=bordered
MsgParaMenu=bordered
define_strings=bordered
DiensteMenu= bordered, -r192 -g192 -b192
OptionenMenu= bordered, -r192 -g192 -b192
WlHaupt=scrolled,bordered
WlHaupt_disp=scrolled,bordered
w_matrixmenu=bordered,scrolled
fehler_window=scrolled
ExtEditorMatrix=bordered
ExtEditor=bordered
;
; nachfolgende Spezifikationen NICHT �ndern !!!!
;                              =====
tasklines=23
;
; Button-Spezifikationen
; ======================
;
CommentWindow=reverse,-r192 -g192 -b192
KeyWindow=reverse,-r192 -g192 -b192
fkeys_mail=reverse, -r192 -g192 -b192

;
; Funktionstasten-Button-Definitionen
; ===================================
;
keyhelp=        " ? "
keyesc=         " Zur�ck ,Zur�ck, Abbruch ,ESC"
key2=           " O.K., Fehlerdatei Editieren , �bernehmen , Brennen ,F2"
keyhome=        " 1. "
keyend=         " n. "
keyup=          " <-- , Hoch , auf ,auf"
keydown=        " --> , Runter , ab , ab"
keycr=          " * , Eingabe , Auswahl , Abschlu� , pos. , sel. , n�chst.Par. , n�chste Tab. , Weiter , Ref. , Referenzen , Toggle , �ndern "
key4=           " Dateiauswahl , Verzeichnisauswahl "
key5=           " Editieren , Pos auf "
key6=           " Generieren , Pos ab "
key7=           " Start , Ausf�hren "
key8=           " Parameter "
key9=           " Projekt "
keyright=       " Rechts , Ebene + , Stufe tiefer ,-->"
keyleft=        " Links , Ebene - , Stufe h�her ,<--"
keyprev=        " vorige Seite , Seite - ,Seite -"
keynext=        " n�chste Seite , Seite + ,Seite +"
keyinschr=      " Datei Einf�gen , Einf�gen , Eintragen "
keydelchr=      " Datei Entfernen , L�schen , Entfernen "

[color]
rowadjust=2
wr0g128b255=-r0 -g128 -b255
wr192g192b192=-r192 -g192 -b192
wr128g128b128=-r128 -g128 -b128
wr0g0b0=-r0 -g0 -b0
wr0g0b128=-r0 -g0 -b128
wr0g128b0=-r0 -g128 -b0
wr128g0b0=-r128 -g0 -b0
wr0g128b128=-r0 -g128 -b128
wr128g0b128=-r128 -g0 -b128
wr128g128b0=-r128 -g128 -b0
wr0g0b255=-r0 -g0 -b255
wr0g255b0=-r0 -g255 -b0
wr255g0b0=-r255 -g0 -b0
wr0g255b255=-r0 -g255 -b255
wr255g0b255=-r255 -g0 -b255
wr255g255b0=-r255 -g255 -b0
wrs128gs128b255=-rs128 -gs128 -b255
wrs128g255bs128=-r128 -g255 -b128
wr255g128b128=-r255 -g128 -b128
wr128g255b255=-r128 -g255 -b255
wr255g128b255=-r255 -g128 -b255
wr255g255b128=-r255 -g255 -b128

[rsdbs]
;1024x768
;cellWidth=                     10
;cellHeight=                    23
;800x600
cellHeight=                     18
cellWidth=                      9
tasklines = 25
compatmode = 2
font=Courier
;fontadjust=2
ausgabe = scrolled, bordered
ausgabe_kopf  = bordered
DateiAuswahl = scrolled
objekt = scrolled
fortsetzen = bordered
KeyWindow = reverse
LogonWindow = reverse
dbneuanlegen = reverse
KEYESC = "ESC, ESC , Zur�ck , Abbruch " 
KEYHELP = "F1, F1 , ? "
KEYCR = " Auswahl "
KEY2 = "F2, F2 , O.K., Speichern "
KEY3 = "F3, F3 , Verzeichnis <=> Dateiliste "
KEY7 = "F7, F7 , Liste "
KEY8 = "Transaktionsmodus,AutoCommit"
KEYUP   = " auf "
KEYDOWN = " ab "
KEYRIGHT = "-->"
KEYLEFT = "<--"
KEYNEXT = "NEXT, n�chste Seite , Bild ab "
KEYPREV = "PREV, vorige Seite , Bild auf "

