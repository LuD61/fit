#include<stdio.h>
#include<stdarg.h>
#include<string.h>
#include<windows.h>
#include <time.h>
#include <direct.h>
#include "stdfkt.h"
#include "strfkt.h"
#include "wmaskc.h"
#include "mo_meld.h"
#include "message.h"
#include "best_kopf.h"
#include "ls.h"
#include "kun.h"
#include "mdn.h"
#include "fil.h"
// #include "mo_numme.h"
#include "mo_kompl.h"
#include "mo_einh.h"
#include "mo_curso.h"
#include "sys_par.h"
#include "dbclass.h"
#include "mo_lsgen.h"
#include "best_pos.h"
#include "auto_nr.h"
#include "mo_auto.h"
#include "mo_nr.h"
#include "70001.h"
#include "bsd_buch.h"
#include "mo_menu.h"
#include "a_bas.h"
#include "mo_gdruck.h"
#include "mo_cancel.h"
#include "bestpt.h"
#include "mo_llgen.h"

#define MAXWAIT 60

LS_CLASS ls_class;
static EINH_CLASS einh_class;
static SYS_PAR_CLASS sys_par_class;
static DB_CLASS DbClass;
static BESTPT_CLASS bestpt_class;
static AUTO_CLASS AutoClass;
static ADR_CLASS adr_class;
static AutoNrClass AutoNr;
static BSD_BUCH_CLASS BsdBuch;
extern MELDUNG Mess;

/**
Auftrag auf komplett setzen.
**/

struct AP
{
	short mdn;
	short fil;
	long auf;
	long posi;
	double a;
};

static struct AP *best_postab = NULL;
static long best_postanz;

LLGEN_CLASS Llgen;

static int bsd_kz = 1;
static BOOL klst_dr_par = FALSE;
static BOOL drk_ls_sperr = 0;
static int lief_me_par = 0;
static int PrintMode = 0;
static int LskOK;
static int AufkOK;

static int LsKomplett     = 0;
static int aufKliste      = 0;
static int aufUeandDrk    = 0;
static int aufLsUeb       = 0;
static int aufLsKomp      = 0;
static int drwahl         = 0;

static int *aufbuttons [] = {&aufKliste, &aufUeandDrk, &aufLsUeb, &aufLsKomp, &drwahl, NULL};

static int SetLsKomp0 (void);
static int SetLsUeb0 (void);
static int SetKlst0 (void);
static int SetUeandDrk0 (void);

static int (*SetDefTab[]) () = {SetKlst0, SetUeandDrk0, SetLsUeb0, SetLsKomp0};

static int SetKlst (void);
static int SetUeandDrk (void);
static int SetLsUeb (void);
static int SetLsKomp (void);
static int BreakBox (void);
static int BoxBreak (void);

static int IsDrChoise;

static ITEM iKliste   ("",  "K.-Liste",    "", 0);
static ITEM iUeandDrk ("",  "�berg&Drk",   "", 0);
static ITEM iLsUeb    ("",  "LS�bergabe",  "", 0);
static ITEM iLsKomp   ("",  "LSKomplett",  "", 0);
static ITEM  iOK      ("", "    OK      ",  "", 0);
static ITEM  iCancel  ("", "  Abbruch   ",  "", 0);
static ITEM  iDW      ("", "Druckerwahl ",  "", 0);


static field _cbox [] = {
&iKliste,                19, 2, 0,  6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             SetKlst, 101,
&iUeandDrk,              19, 2, 2,  6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             SetUeandDrk, 102,
&iLsUeb,                 19, 2, 4,  6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             SetLsUeb, 103,
&iLsKomp,                19, 2, 6,  6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             SetLsKomp, 104,
&iOK,                    15, 0, 9,  6,  0, "", BUTTON, 0, BreakBox, 107,
&iCancel,                15, 0, 9, 23, 0, "",  BUTTON, 0, BoxBreak, KEY5,
&iDW,                    15, 0, 9, 40, 0, "",  BUTTON, 0, BoxBreak, KEY7};

form cbox = {7, 0, 0, _cbox, 0, SetKlst0, 0, 0, NULL};

static int auf_text_par;
static int wa_pos_txt;
static BOOL with_log = TRUE;

static char *LogName = NULL;
static char *LstName = NULL;

static void CreateLogfile (void)
/**
Log-Datei oeffnen.
**/
{
	      char buffer [512];
		  char count [80];
		  int c;
		  char *tmp;
		  char datum [12];
		  char zeit  [12];
		  FILE *fp;
          time_t timer;
          struct tm *ltime;

 	      static char *days [] = {"Son", "Mon", "Die", "Mit", "Don", "Fre", "Sam", NULL};

		  if (with_log == FALSE) return;

		  sysdate (datum);
		  systime (zeit);
		  tmp = getenv ("TMPPATH");
		  if (tmp == NULL) return;

          time (&timer);
          ltime = localtime (&timer);

          sprintf (buffer, "%s\\%s", tmp, days [ltime->tm_wday]);
          _mkdir (buffer);

          strcat (buffer, "\\");
		  strcat (buffer, "count");
		  c = 0;
		  fp = fopen (buffer, "r");
		  if (fp)
		  {
			  if (fgets (count, 79, fp))
			  {
				  c = atoi (count);
			  }
			  fclose (fp);
		  }
		  if (LogName) delete LogName;
          LogName = new char [512];
		  if (LogName == NULL) return;
		  sprintf (LogName, "%s\\%s\\32200.%d", tmp, days [ltime->tm_wday], c);
		  fp = fopen (LogName, "w");
		  if (fp == NULL)
		  {
			  delete LogName;
			  LogName = NULL;
              return;
		  }
		  fprintf (fp, "Log-Datei Auftragserfassung erzeugt am %s %s\n", datum, 
			                                                             zeit);
		  fclose (fp);
		  c ++;
		  if (c == 20) c = 0;
		  fp = fopen (buffer, "w");
		  fprintf (fp, "%d", c);
		  fclose (fp);
		  if (LstName) delete LstName;
		  LstName = new char [512];
		  sprintf (LstName, "%s\\%s\\liste.%d", tmp, days [ltime->tm_wday], atoi (count));
}

void CopyListe (void)
/**
Liste sichern.
**/
{
	  char quelle [512];
	  char ziel [512];

	  if (with_log == FALSE) return;
	  if (LstName == NULL) return;
	  char *tmp;
	  
	  tmp = getenv ("TMPPATH");
	  if (tmp == NULL) return;
	  sprintf (quelle, "%s\\32200.lst", tmp);
	  sprintf (ziel, "%s", LstName);
	  CopyFile (quelle, ziel, FALSE);
}


void CloseLogfile (void)
/**
Dateiname fuer Logfile abschliessen.
**/
{
		  if (with_log == FALSE) return;
	      if (LogName == NULL) return;

	      delete LogName;
		  LogName = NULL;

	      if (LstName == NULL) return;

	      delete LstName;
		  LstName = NULL;
}


static void WriteLogfile (char * format, ...)
/**
Debugausgabe.
**/
{
          va_list args;
          FILE *logfile;

		  if (with_log == FALSE) return;
		  if (LogName == NULL) 
		  {
			  CreateLogfile ();
		  }

		  logfile = fopen (LogName, "a");
		  if (logfile == NULL) return;

          va_start (args, format);
          vfprintf (logfile, format, args);
          va_end (args);
		  fclose (logfile);
}


int KomplettEnd (void)
/**
EnterButton abbrechen.
**/
{
           if (syskey == KEY5)
           {
                    break_enter ();
           }
           return 1;
}

void UnsetBox (int pos)
/**
Button auf selectierte setzen. (ausser pos);
**/
{
        int i;

        for (i = 0; aufbuttons [i]; i ++)
        {
                  if (i == pos) continue;

                  SendMessage (cbox.mask[i].feldid, BM_SETCHECK, 0, 0l);
                  *aufbuttons [i] = 0;
        }
}


int SetKlst (void)
/**
K-Liste setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (cbox.mask[0].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       aufKliste = 0;
          }
          else
          {
                       aufKliste = 1;
          }
          if (syskey == KEYCR)
          {
                       aufKliste = 1;
                       SendMessage (cbox.mask[0].feldid, BM_SETCHECK,
                       aufKliste, 0l);
                       break_enter ();
          }
          UnsetBox (0);
          return 1;
}

int SetUeandDrk (void)
/**
Uebergabe und Druck setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (cbox.mask[1].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                        aufUeandDrk = 0;
          }
          else
          {
                        aufUeandDrk  = 1;
          }
          if (syskey == KEYCR)
          {
                       aufUeandDrk  = 1;
                       SendMessage (cbox.mask[1].feldid, BM_SETCHECK,
                       aufUeandDrk, 0l);
                       break_enter ();
          }
          UnsetBox (1);
          return 1;
}

int SetLsUeb (void)

/**
Uebergabe an Lieferschein setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (cbox.mask[2].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       aufLsUeb = 0;
          }
          else
          {
                       aufLsUeb = 1;
          }
          if (syskey == KEYCR)
          {
                       aufLsUeb = 1;
                       SendMessage (cbox.mask[2].feldid, BM_SETCHECK,
                       aufLsUeb, 0l);
                       break_enter ();
          }
          UnsetBox (2);
          return 1;
}

int SetLsKomp (void)

/**
Uebergabe an Lieferschein setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (cbox.mask[2].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       aufLsKomp = 0;
          }
          else
          {
                       aufLsKomp = 1;
          }
          if (syskey == KEYCR)
          {
                       aufLsKomp = 1;
                       SendMessage (cbox.mask[3].feldid, BM_SETCHECK,
                       aufLsUeb, 0l);
                       break_enter ();
          }
          UnsetBox (3);
          return 1;
}

int SetLsUeb0 (void)

/**
Uebergabe an Lieferschein setzen.
**/
{
          aufLsUeb = 1;
          SendMessage (cbox.mask[2].feldid, BM_SETCHECK,
                       aufLsUeb, 0l);
          SetFocus (cbox.mask[2].feldid);
          SetCurrentField (2);
          UnsetBox (2);
          return 1;
}

int SetKlst0 (void)
/**
Uebergabe an Lieferschein setzen.
**/
{
          aufKliste = 1;
          SendMessage (cbox.mask[0].feldid, BM_SETCHECK,
                       aufKliste, 0l);
          SetFocus (cbox.mask[0].feldid);
          SetCurrentField (0);
          UnsetBox (0);
          return 1;
}

int SetLsKomp0 (void)
/**
Uebergabe an Lieferschein setzen.
**/
{
          aufLsKomp = 1;
          SendMessage (cbox.mask[3].feldid, BM_SETCHECK,
                       aufLsKomp, 0l);
          SetFocus (cbox.mask[3].feldid);
          SetCurrentField (3);
          UnsetBox (3);
          return 1;
}

int SetUeandDrk0 (void)
/**
Uebergabe und Druck setzen.
**/
{
          aufUeandDrk  = 1;
          SendMessage (cbox.mask[1].feldid, BM_SETCHECK,
                       aufUeandDrk, 0l);
          SetFocus (cbox.mask[1].feldid);
          SetCurrentField (1);
          UnsetBox (1);
          return 1;
}


int BreakBox (void)
/**
EnterButton abbrechen.
**/
{
          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;
          syskey = KEYCR;
          break_enter ();
          return 1;
}

int BoxBreak (void)
/**
EnterButton abbrechen.
**/
{
	      int savecurrent;
		  HWND hWndA;
		  HWND hWndB;

		  savecurrent = currentfield;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;


          switch (syskey)
		  {
                case KEY5 :
                       break_enter ();
                       return 1;
                case KEY11 :
                case KEY12 :
                       syskey = KEY12;
                       break_enter ();
                       return 1;
				case KEY7 :
					   if (KompBest::IsLlFormat ())
					   {
						   Llgen.SetDrkWahl (TRUE);
						   return 1;
					   }
                       hWndA = GetActiveWindow ();					   
					   hWndB = AktivWindow;
  					   SetCurrentField (0);
					   ChoisePrinter0 ();
					   AktivWindow = hWndB;
					   SetActiveWindow (hWndA);
					   EnableWindow (GetParent (AktivWindow), FALSE);
 		               SetButtonTab (TRUE);
					   SetCurrentField (savecurrent);
					   SetCurrentFocus (savecurrent);
					   return 1;
		  }

          if (current_form->mask[currentfield].BuId == KEY7)
		  {
				    if (KompBest::IsLlFormat ())
					{
					   Llgen.SetDrkWahl (TRUE);
					   return 1;
					}
					syskey = KEY7;
                    hWndA = GetActiveWindow ();					   
			        hWndB = AktivWindow;
 					SetCurrentField (0);
					ChoisePrinter0 ();
				    AktivWindow = hWndB;
					SetActiveWindow (hWndA);
					EnableWindow (GetParent (AktivWindow), FALSE);
	                SetButtonTab (TRUE);
 					SetCurrentField (savecurrent);
					SetCurrentFocus (savecurrent);
					return 1;
		  }
		  else if (current_form->mask[currentfield].BuId == KEY5)
		  {
					syskey = KEY5;
					break_enter ();
					return 1;
		  }
		  else if (current_form->mask[currentfield].BuId == 107)
		  {
					syskey = KEY12;
					break_enter ();
					return 1;
		  }
		
          return 1;
}


void SelectAktCtr (void)
/**
Aktiven Button in EnterButton selectieren.
**/
{
        syskey = KEYCR;
        if (cbox.mask[currentfield].after)
        {
                       (*cbox.mask[currentfield].after) ();
        }
        syskey = KEYCR;
}

char *KompBest::llformat;
char KompBest::drformat [20] = {"32200"};
char KompBest::drfaxformat [20] = {"32200"};
char KompBest::kontraktformat [20] = {"32200"};

void KompBest::SetPrintMode (int mode)
{
	       PrintMode = mode;
}


void KompBest::SetKlstPar (int par)
/**
Parameter fuer Liefermenge setzen.
**/
{
	       klst_dr_par = min (1, par);
}

void KompBest::SetLog (int par)
/**
Parameter fuer Liefermenge setzen.
**/
{
	       with_log = min (1, par);
}

void KompBest::SetKompDefault (int def_nr)
/**
Default fuer Auswahlmaske setzen.
**/
{
	       def_nr = min (max (0, def_nr), 3);
		   cbox.before = SetDefTab[def_nr];
}	       

void KompBest::SetDrkLsSperr (BOOL sp)
{
	       drk_ls_sperr = min (1, max (0, sp));
}

void KompBest::BearbKomplett (void)
/**
Auswahl aus Komplett bearbeiten.
**/
{
/*
          if (aufKliste)
          {
              K_ListeTS ();
          }
*/
          return;
}
                   

int KompBest::SetKomplett (HWND hWnd)
/**
Komplett setzen.
**/
{
 
  	     KompParams ();
		 aufKliste = TRUE;
		 BearbKomplett ();
		 return (0);
/*
         save_fkt (5);
         save_fkt (7);
		 set_fkt (NULL, 7);
         set_fkt (KomplettEnd, 5);

         aufKliste = aufUeandDrk = 0;
         aufLsUeb = 0;
         EnableWindows (hWnd, FALSE);
         SetBorder (WS_POPUP | WS_VISIBLE | WS_CAPTION | WS_DLGFRAME);
         SetCaption ("Auswahl f�r Komplett");
         if (EnterButtonWindow (hWnd, &cbox, 14, 9, 11, 62) == -1)
         {
			            SetActiveWindow (hWnd);  
                        SetAktivWindow (hWnd);  
                        EnableWindows (hWnd, TRUE);
                        restore_fkt (5);
                        return 0;
         }

         SetActiveWindow (hWnd);  
         SetAktivWindow (hWnd);  
         EnableWindows (hWnd, TRUE);
         BearbKomplett ();
         restore_fkt (5);
         restore_fkt (7);
         return 0;
*/
}

void KompBest::ShowLocks (HWND hWnd)
/**
Auftraege, die nicht freigeben oder gedruckt werden konnten, anzeigen.
**/
{
	int wlen; 
	int i;
	char buffer [80];

    if (aufanzlock == 0) return;

    SetHLines (0);
    SetVLines (0);
	SetMenSelect (FALSE);
	wlen = 12;
	if (aufanzlock < 12) wlen = aufanzlock; 

	EnableWindows (hWnd, FALSE);
	OpenListWindowBu (hWnd, wlen, 40, 6, 20, 1);
    InsertListUb (NULL);
	SetWindowText (GethListBox (), "Nicht bearbeitete Bestellungen");
	for (i = 0; i < aufanzlock; i ++)
	{
		sprintf (buffer,"    Auftrag %ld", auftablock [i]);
        InsertListRow (buffer);
	}
    EnterQueryListBox ();
	EnableWindows (hWnd, TRUE);
    SetMenSelect (TRUE);
}

void KompBest::ShowNoDrk (HWND hWnd)
/**
Auftraege, die beim Freigeben noch nicht gedruckt waren, anzeigen.
**/
{
	int wlen; 
	int i;
	char buffer [80];

    if (drklsanz == 0) return;

    SetHLines (0);
    SetVLines (0);
	SetMenSelect (FALSE);
	wlen = 12;
	if (drklsanz < 12) wlen = drklsanz; 

	EnableWindows (hWnd, FALSE);
	OpenListWindowBu (hWnd, wlen, 40, 6, 20, 1);
    InsertListUb (NULL);
	SetWindowText (GethListBox (), "Nicht gedruckte Bestellungen");
	for (i = 0; i < drklsanz; i ++)
	{
		sprintf (buffer,"    Bestellung %ld", drklstab [i]);
        InsertListRow (buffer);
	}
    EnterQueryListBox ();
	EnableWindows (hWnd, TRUE);
    SetMenSelect (TRUE);
}


void KompBest::InitBestanz (void)
/**
aufanz auf 0 setzen.
**/
{
	aufanz = 0;
	aufanzlock = 0;
}

void KompBest::KompParams (void)
/**
Systemparameter lesen.
**/
{
	return;
/*
	static BOOL params_ok = FALSE;

	if (params_ok) return;

	params_ok = TRUE;
    strcpy (sys_par.sys_par_nam,"auf_text_par");
    if (sys_par_class.dbreadfirst () == 0)
	{
                 auf_text_par = atoi (sys_par.sys_par_wrt); 
	}
    strcpy (sys_par.sys_par_nam,"wa_pos_txt");
    if (sys_par_class.dbreadfirst () == 0)
	{
                 wa_pos_txt = atoi (sys_par.sys_par_wrt); 
	}
*/
}

void KompBest::LockBest (void)
/**
Auftraege sperren.
**/
{
	int i;

    for (i = 0; i < aufanz; i ++)
	{
 			          best_kopf.mdn = mdntab[i];
			          best_kopf.fil = filtab[i];
			          best_kopf.best_blg = auftab[i];
                      DbClass.sqlin ((short *) &best_kopf.mdn, 1, 0); 
                      DbClass.sqlin ((short *) &best_kopf.fil, 1, 0); 
                      DbClass.sqlin ((long *)  &best_kopf.best_blg, 2, 0); 
					  DbClass.sqlcomm ("update best_kopf set delstatus = -1 "
						               "where mdn = ? "
									   "and   fil = ? "
									   "and best_blg   = ?");
	}
}


void KompBest::TestFree (void)
/**
Testen, ob freie Auftraege inzwischen gesperrt sind.
**/
{
	int i;
    short mdntab0[1000]; 
    short filtab0[1000];
    long auftab0[1000];
    int dsqlstatus;
	int j;

	if (aufanz == 0) return;
    for (i = 0, j = 0; i < aufanz; i ++)
	{
 			          best_kopf.mdn = mdntab[i];
			          best_kopf.fil = filtab[i];
			          best_kopf.best_blg = auftab[i];
                      DbClass.sqlin ((short *) &best_kopf.mdn, 1, 0); 
                      DbClass.sqlin ((short *) &best_kopf.fil, 1, 0); 
                      DbClass.sqlin ((long *)  &best_kopf.best_blg, 2, 0); 
					  dsqlstatus = DbClass.sqlcomm ("select best_blg from best_kopf "
						                            "where mdn = ? "
									                "and   fil = ? "
									                "and best_blg   = ? "
									                "and delstatus = 0");
					  if (dsqlstatus == 0)
					  {
						  mdntab0[j]  = mdntab[i];
						  filtab0[j]  = filtab[i];
						  auftab0[j]  = auftab[i];
						  j ++;
					  }
					  else
					  {
		                  mdntablock[aufanzlock] = mdntab[i];
		                  filtablock[aufanzlock] = filtab[i];
		                  auftablock[aufanzlock] = auftab[i];
						  aufanzlock ++;
					  }
	}
	if (j == aufanz) return;
	aufanz = j;
	for (i = 0; i < aufanz; i ++)
	{
			  mdntab[i]  = mdntab0[i];
			  filtab[i]  = filtab0[i];
			  auftab[i]  = auftab0[i];
	}
}

		  
void KompBest::FreeBest (void)
/**
Gesperrte Auftraege freigeben.
**/
{
	int i;
	int dsqlstatus;
	extern short sql_mode;

    sql_mode = 1;
    for (i = 0; i < aufanz; i ++)
	{
 			          best_kopf.mdn = mdntab[i];
			          best_kopf.fil = filtab[i];
			          best_kopf.best_blg = auftab[i];
                      DbClass.sqlin ((short *) &best_kopf.mdn, 1, 0); 
                      DbClass.sqlin ((short *) &best_kopf.fil, 1, 0); 
                      DbClass.sqlin ((long *)  &best_kopf.best_blg, 2, 0); 
					  dsqlstatus = DbClass.sqlcomm ("update best_kopf set delstatus = 0 "
						                            "where mdn = ? "
									                "and   fil = ? "
									                "and best_blg   = ?");
					if (dsqlstatus < 0)
					{
						print_mess (2, "Achtung !!\n"
							           "Bestellung %ld kann nicht zur�ckgesetzt werden",
									    best_kopf.best_blg);
					}
	}
	sql_mode = 0;
	aufanz = 0;
}

void KompBest::Getauf_a_sort (void)
{
    static BOOL ParOK = 0;

    if (ParOK) return;

    ParOK = 1;
    auf_a_sort = 0;
    strcpy (sys_par.sys_par_nam,"auf_a_sort");
    if (sys_par_class.dbreadfirst () == 0)
    {
        auf_a_sort = atoi (sys_par.sys_par_wrt);
    }
}

void KompBest::K_Liste_Direct (void)
/**
Kommissionierliste ohne Listgenerator drucken.
**/
{
}
     
void KompBest::K_Liste_Fax (void)
/**
Kommissionierliste drucken.
**/
{
    char valfrom [20];
    char valto [20];
	HWND akthWnd;

	if (IsLlFormat ())
	{
		  if (strcmp (drfaxformat, drformat) == 0)
		  {
			  Llgen.SetDrkWahl (FALSE);
		  }
		  llformat = drfaxformat;
		  K_LlListe ();
          return;
	}

	akthWnd = GetActiveWindow ();
	SetAusgabe (FAX_OUT);
    if (syskey == KEY5) return;
    if (IsGroup == FALSE) commitwork ();
    if (LeseFormat (drformat) == FALSE)
    {
		           return;
    }

    sprintf (valfrom, "%hd", best_kopf.mdn);
    sprintf (valto, "%hd", best_kopf.mdn);
    FillFormFeld ("best_kopf.mdn", valfrom, valto);

    sprintf (valfrom, "%hd", best_kopf.fil);
    sprintf (valto, "%hd", best_kopf.fil);
    FillFormFeld ("best_kopf.fil", valfrom, valto);

    sprintf (valfrom, "%ld", best_kopf.best_blg);
    sprintf (valto, "%ld", best_kopf.best_blg);
    FillFormFeld ("best_kopf.best_blg", valfrom, valto);
    StartPrint ();
	SetActiveWindow (akthWnd);
	CreateLogfile ();
	WriteLogfile ("Bestellung %ld wurde gedruckt\n", best_kopf.best_blg);
	CloseLogfile ();
    DbClass.sqlin ((short *) &best_kopf.mdn, 1, 0);
    DbClass.sqlin ((short *) &best_kopf.fil, 1, 0);
    DbClass.sqlin ((long *)  &best_kopf.best_blg, 2, 0);
    DbClass.sqlcomm ("update best_kopf set bearb_stat = 2 "
                          "where mdn = ? "
                          "and fil = ? "
                          "and best_blg = ? ");
}

void KompBest::K_LlListe (void)
/**
Kommissionierliste drucken.
**/
{
    char valfrom [20];
    char valto [20];

	SetAusgabe (PRINTER_OUT);
    if (syskey == KEY5) return;
    if (IsGroup == FALSE) commitwork ();
    if (Llgen.LeseFormat (llformat) == FALSE)
	{
           return;
	}

    sprintf (valfrom, "%hd", best_kopf.mdn);
    sprintf (valto, "%hd", best_kopf.mdn);
    Llgen.FillFormFeld ("best_kopf.mdn", valfrom, valto);

    sprintf (valfrom, "%hd", best_kopf.fil);
    sprintf (valto, "%hd", best_kopf.fil);
    Llgen.FillFormFeld ("best_kopf.fil", valfrom, valto);

    sprintf (valfrom, "%ld", best_kopf.best_blg);
    sprintf (valto, "%ld", best_kopf.best_blg);
    Llgen.FillFormFeld ("best_kopf.best_blg", valfrom, valto);
    Llgen.StartPrint ();

	CreateLogfile ();
	WriteLogfile ("Bestellung %ld wurde gedruckt\n", best_kopf.best_blg);
	CloseLogfile ();
    DbClass.sqlin ((short *) &best_kopf.mdn, 1, 0);
    DbClass.sqlin ((short *) &best_kopf.fil, 1, 0);
    DbClass.sqlin ((long *)  &best_kopf.best_blg, 2, 0);
    DbClass.sqlcomm ("update best_kopf set bearb_stat = 2 "
                          "where mdn = ? "
                          "and fil = ? "
                          "and best_blg = ? ");
    if (IsGroup == FALSE) Mess.Message ("Die Bestellung wurde gedruckt");
}


BOOL KompBest::IsLlFormat ()
{
	static int LlCursor = -1;
	static BOOL IsLlFormat = FALSE;
	static BOOL readed = FALSE;

	if (readed)
	{
		return IsLlFormat;
	}

    readed = TRUE;
	if (LlCursor == -1)
	{
		DbClass.sqlin (drformat, 0, sizeof (drformat));
//		DbClass.sqlin (aufbformat, 0, sizeof (aufbformat));
		LlCursor = DbClass.sqlcursor ("select * from nform_hea where form_nr =?");
	}
	if (LlCursor != -1)
	{
		DbClass.sqlopen (LlCursor);
		if (DbClass.sqlfetch (LlCursor) == 0)
		{
			IsLlFormat = TRUE;
		}
	}
    return IsLlFormat;
}

void KompBest::K_Liste (void)
/**
Kommissionierliste drucken.
**/
{
    char valfrom [20];
    char valto [20];
	HWND akthWnd;


	if (IsLlFormat ())
	{
		  llformat = drformat;
		  K_LlListe ();
          return;
	}

	akthWnd = GetActiveWindow ();
	SetAusgabe (PRINTER_OUT);
    if (syskey == KEY5) return;
    if (IsGroup == FALSE) commitwork ();

	if (klst_dr_par && !kontrakt_kz)
	{
		K_Liste_Direct ();
	}
	else
	{
        if (!kontrakt_kz && LeseFormat (drformat) == FALSE)
		{
		           return;
		}
        if (kontrakt_kz && LeseFormat (kontraktformat) == FALSE)
		{
		           return;
		}

        sprintf (valfrom, "%hd", best_kopf.mdn);
        sprintf (valto, "%hd", best_kopf.mdn);
        FillFormFeld ("best_kopf.mdn", valfrom, valto);

        sprintf (valfrom, "%hd", best_kopf.fil);
        sprintf (valto, "%hd", best_kopf.fil);
        FillFormFeld ("best_kopf.fil", valfrom, valto);

        sprintf (valfrom, "%ld", best_kopf.best_blg);
        sprintf (valto, "%ld", best_kopf.best_blg);
        FillFormFeld ("best_kopf.best_blg", valfrom, valto);
        StartPrint ();
	}
	SetActiveWindow (akthWnd);
	CreateLogfile ();
	WriteLogfile ("Bestellung %ld wurde gedruckt\n", best_kopf.best_blg);
	CloseLogfile ();
    DbClass.sqlin ((short *) &best_kopf.mdn, 1, 0);
    DbClass.sqlin ((short *) &best_kopf.fil, 1, 0);
    DbClass.sqlin ((long *)  &best_kopf.best_blg, 2, 0);
    DbClass.sqlcomm ("update best_kopf set bearb_stat = 2 "
                          "where mdn = ? "
                          "and fil = ? "
                          "and best_blg = ? ");
    if (IsGroup == FALSE) Mess.Message ("Die Bestellung wurde gedruckt");
}


void KompBest::K_ListeGroup (short mdn_von, short mdn_bis,
							short fil_von, short fil_bis,
							long best_von, long best_bis,
							char *dat_von, char *dat_bis,
							short stat_von, short stat_bis)
/**
Kommissionierliste drucken.
**/
{
    char valfrom [20];
    char valto [20];
	long ldat_von, ldat_bis;
	int cursor; 
//	int cursor_pos;
	short mdn, fil;
	long best_blg;
	int i;
	int dsqlstatus;
	char datum [11];
	char zeit [11];
	extern short sql_mode;

	HWND akthWnd;

	akthWnd = GetForegroundWindow ();
	CreateLogfile ();

    sysdate (datum);
	systime (zeit);
    WriteLogfile ("Bereichsdruck Bestellungen %s %s\n", datum, zeit);

	Mess.WaitWindow ("Die Bestellungen werden gedruckt");

	max_best = best_bis;
    ldat_von = dasc_to_long (dat_von);
    ldat_bis = dasc_to_long (dat_bis);


    DbClass.sqlin ((short *) &mdn_von, 1, 0);
    DbClass.sqlin ((short *) &mdn_bis, 1, 0);
    DbClass.sqlin ((short *) &fil_von, 1, 0);
    DbClass.sqlin ((short *) &fil_bis, 1, 0);
    DbClass.sqlin ((long *)  &best_von, 2, 0);
    DbClass.sqlin ((long *)  &best_bis, 2, 0);
    DbClass.sqlin ((long *)  &ldat_von, 2, 0);
    DbClass.sqlin ((long *)  &ldat_bis, 2, 0);
    DbClass.sqlin ((short *) &stat_von, 1, 0);
    DbClass.sqlin ((short *) &stat_bis, 1, 0);

    DbClass.sqlout ((long *) &best_kopf.mdn, 1, 0);
    DbClass.sqlout ((long *) &best_kopf.fil, 1, 0);
    DbClass.sqlout ((long *) &best_kopf.best_blg, 2, 0);
    cursor = DbClass.sqlcursor ("select best_kopf.mdn, best_kopf.fil, best_kopf.best_blg from best_kopf "
                          "where best_kopf.mdn between ? and ?"
                          "and best_kopf.fil between ? and ? "
                          "and best_kopf.best_blg between  ? and ? "
                          "and best_kopf.best_term between  ? and ? "
                          "and best_kopf.bearb_stat between  ? and ? "
						  "and delstatus = 0");

// Gesperrte Bestellungen merken 

    DbClass.sqlin ((short *) &mdn_von, 1, 0);
    DbClass.sqlin ((short *) &mdn_bis, 1, 0);
    DbClass.sqlin ((short *) &fil_von, 1, 0);
    DbClass.sqlin ((short *) &fil_bis, 1, 0);
    DbClass.sqlin ((long *)  &best_von, 2, 0);
    DbClass.sqlin ((long *)  &best_bis, 2, 0);
    DbClass.sqlin ((long *)  &ldat_von, 2, 0);
    DbClass.sqlin ((long *)  &ldat_bis, 2, 0);
    DbClass.sqlin ((short *) &stat_von, 1, 0);
    DbClass.sqlin ((short *) &stat_bis, 1, 0);

    DbClass.sqlout ((long *) &mdn, 1, 0);
    DbClass.sqlout ((long *) &fil, 1, 0);
    DbClass.sqlout ((long *) &best_blg, 2, 0);
    cursor = DbClass.sqlcursor ("select best_kopf.mdn, best_kopf.fil, best_kopf.best_blg from best_kopf "
                          "where best_kopf.mdn between ? and ?"
                          "and best_kopf.fil between ? and ? "
                          "and best_kopf.best_blg between  ? and ? "
                          "and best_kopf.best_term between  ? and ? "
                          "and best_kopf.bearb_stat between  ? and ? "
						  "and delstatus != 0");
	aufanzlock = 0;
	DbClass.sqlopen (cursor);
	dsqlstatus = DbClass.sqlfetch (cursor);
	if (dsqlstatus == 0)
	{
		    WriteLogfile  ("Gesperrte Bestellungen\n");
	}
	while (dsqlstatus == 0)
	{
		    mdntablock[aufanzlock] = mdn;
		    filtablock[aufanzlock] = fil;
		    auftablock[aufanzlock] = best_blg;
			WriteLogfile ("   Bestellung %ld\n", best_blg);
			if (aufanz == 999)
			{
				disp_mess ("Maximale Anzahl Bestellungen �berschritten", 2);
				break;
			}
	        dsqlstatus = DbClass.sqlfetch (cursor);
			aufanzlock ++;
	}
	DbClass.sqlclose (cursor);


// Freie Bestellungen merken

    DbClass.sqlin ((short *) &mdn_von, 1, 0);
    DbClass.sqlin ((short *) &mdn_bis, 1, 0);
    DbClass.sqlin ((short *) &fil_von, 1, 0);
    DbClass.sqlin ((short *) &fil_bis, 1, 0);
    DbClass.sqlin ((long *)  &best_von, 2, 0);
    DbClass.sqlin ((long *)  &best_bis, 2, 0);
    DbClass.sqlin ((long *)  &ldat_von, 2, 0);
    DbClass.sqlin ((long *)  &ldat_bis, 2, 0);
    DbClass.sqlin ((short *) &stat_von, 1, 0);
    DbClass.sqlin ((short *) &stat_bis, 1, 0);

    DbClass.sqlout ((long *) &mdn, 1, 0);
    DbClass.sqlout ((long *) &fil, 1, 0);
    DbClass.sqlout ((long *) &best_blg, 2, 0);
    cursor = DbClass.sqlcursor ("select best_kopf.mdn, best_kopf.fil, best_kopf.best_blg from best_kopf "
                          "where best_kopf.mdn between ? and ?"
                          "and best_kopf.fil between ? and ? "
                          "and best_kopf.best_blg between  ? and ? "
                          "and best_kopf.best_term between  ? and ? "
                          "and best_kopf.bearb_stat between  ? and ? "
						  "and delstatus = 0");

	aufanz = 0;
	DbClass.sqlopen (cursor);
	dsqlstatus = DbClass.sqlfetch (cursor);
	if (dsqlstatus == 0)
	{
		    WriteLogfile  ("Nicht gesperrte Bestellungen\n");
	}
	while (dsqlstatus == 0)
	{
		    mdntab[aufanz] = mdn;
		    filtab[aufanz] = fil;
		    auftab[aufanz] = best_blg;
			WriteLogfile ("   Bestellung %ld\n", best_blg);
			if (aufanz == 999)
			{
 			    WriteLogfile ("Maximale Anzahl Bestellungen �berschritten\n");
				disp_mess ("Maximale Anzahl Bestellungen �berschritten", 2);
				break;
			}
	        dsqlstatus = DbClass.sqlfetch (cursor);
			aufanz ++;
	}
	DbClass.sqlclose (cursor);

	SetAusgabe (PRINTER_OUT);
//    ChoisePrinter0 ();
    if (syskey == KEY5) return;
    commitwork ();

    if (LeseFormat (drformat) == FALSE)
	{
  	    Mess.CloseWaitWindow ();
		return;
	}

	WriteLogfile ("�bergabewerte an Listgenerator\n");

    sprintf (valfrom, "%hd", mdn_von);
    sprintf (valto, "%hd", mdn_bis);
    FillFormFeld ("aufk.mdn", valfrom, valto);
	WriteLogfile (" Mandant von %hd bis %hd\n", mdn_von, mdn_bis); 

    sprintf (valfrom, "%hd", fil_von);
    sprintf (valto, "%hd", fil_bis);
    FillFormFeld ("aufk.fil", valfrom, valto);
	WriteLogfile (" Filiale von %hd bis %hd\n", fil_von, fil_bis); 


    sprintf (valfrom, "%ld", best_von);
    sprintf (valto, "%ld",   best_bis);
    FillFormFeld ("best_kopf.best_blg", valfrom, valto);
	WriteLogfile (" Bestellung von %ld bis %ld\n", best_von, best_bis); 

    sprintf (valfrom, "%s", dat_von);
    sprintf (valto, "%s", dat_bis);
    FillFormFeld ("best_kopf.best_term", valfrom, valto);
	WriteLogfile (" Bestelltermin von %s bis %s\n", dat_von, dat_bis); 

    sprintf (valfrom, "%hd", stat_von);
    sprintf (valto, "%hd", stat_bis);
    FillFormFeld ("best_kopf.bearb_stat", valfrom, valto);
	WriteLogfile (" Status von %hd bis %hd\n", stat_von, stat_bis); 

    StartPrint ();
	SetForegroundWindow (akthWnd);
	SetActiveWindow (akthWnd);
	SetAktivWindow (akthWnd);
	sql_mode = 1;
	for (i = 0; i < aufanz; i ++)
	{
		    mdn = mdntab[i];
			fil = filtab[i];
			best_blg = auftab[i];
/*
		    DbClass.sqlopen (cursor_pos);
		    if (DbClass.sqlfetch (cursor_pos) == 0)
*/
			{
 	                beginwork ();
                    DbClass.sqlin ((long *) &mdn, 1, 0);
                    DbClass.sqlin ((long *) &fil, 1, 0);
                    DbClass.sqlin ((long *) &best_blg, 2, 0);
                    dsqlstatus = DbClass.sqlcomm ("update best_kopf set bearb_stat = 4 "
                             "where mdn = ? "
                             "and fil = ? "
                             "and best_blg = ? "
							 "and bearb_stat < 4");
					if (dsqlstatus < 0)
					{
						print_mess (2, "Achtung !!\n"
							           "Bestellung %ld kann nicht auf Status\n"
									   "gedruckt gesetzt werden", best_blg);
					}
	                commitwork ();
			}
	}
	sql_mode = 0;
//	DbClass.sqlclose (cursor_pos);
	Mess.CloseWaitWindow ();
    Mess.Message ("Die Bestellungen wurden gedruckt");
	CopyListe ();
	CloseLogfile ();
}


extern int WindowOk;


HANDLE KompBest::Pid = NULL;

int KompBest::prnret = 0;

int KompBest::BreakPrn (int)
/**
Abbruchprocedure f�r Aufbereitung.
**/
{
	      if (abfragejn (NULL, "Druck abbrechen ?", "N"))
		  {
			  SetPrnBreak (TRUE);
			  prnret = -1;
		  }
		  return 0;
}

int KompBest::BreakPrn0 (int)
/**
Abbruchprocedure f�r Aufbereitung.
**/
{
	      if (abfragejn (NULL, "Druck abbrechen ?", "N"))
		  {
	              TerminateProcess (Pid, 99);
				  prnret = 99;
		  }
		  return 0;
}


int KompBest::BreakAufb (int)
/**
Abbruchprocedure f�r Aufbereitung.
**/
{
	      if (abfragejn (NULL, "Aufbereiten abbrechen ?", "N"))
		  {
	              TerminateProcess (Pid, 99);
				  prnret = 99;
		  }
		  return 0;
}



int KompBest::PrintBest (HWND hMainWindow, short mdn, short fil, long ang)
/**
Bestellung ausdrucken.
**/
{
	     char command [512];
		 char *tmp;
		 DWORD Ecode;
		 CA_CLASS Cancel;
		 int ret;


		 prnret = 0;
		 if (PrintMode == 0)
		 {
			 K_Liste ();
			 return 0;

/*		     Cancel.SetCaFunc (BreakPrn0);
		     EnableWindow (hMainWindow, FALSE);
             Cancel.Execute (hMainWindow, 28, 4,
			                          " \nBitte warten.....\n"
									  "Die Bestellung wird gedruckt.",
									   NULL);
			 sprintf (command, "fitprint %s %s\\%s.lst", drformat, sys_ben.pers_nam, tmp);
	         Pid = ProcExecPid (command, SW_SHOWNORMAL, -1, 0, -1, 0); 
	         Ecode = WaitPid (Pid); 
		     EnableWindow (hMainWindow, TRUE);
		     if (Ecode == 99)
			 {
                     return -1;
			 }
		     if (prnret == 99)
			 {
                     return -1;
			 }
		     if (Ecode != 0)
			 {
			         disp_mess ("Fehler beim Drucken", 2);
			         return -1;
			 }
			 return prnret;
*/
         }

         prnret = 0;
		 SetpDevMode (NULL);
		 WindowOk = TRUE;
		 tmp = getenv ("TMPPATH");
		 if (tmp == NULL)
		 {
			 tmp = "C:";
		 }

		 sprintf (command, "bws_asc -os\"where mdn = %hd and fil = %hd "
			              "and best_blg = %ld\" %s\\32200.lst 32200.if",
						   mdn, fil, best_kopf.best_blg, tmp);

		 EnableWindow (hMainWindow, FALSE);
		 Cancel.SetCaFunc (BreakAufb);
 	     Cancel.Execute (hMainWindow, 41, 4, " \nBitte warten.....\n"
								 "Die Bestellung wird zum Drucken vorbereitet.",
								 NULL);
	     Pid = ProcExecPid (command, SW_MINIMIZE, -1, 0, -1, 0); 
	     Ecode = WaitPid (Pid); 
		 EnableWindow (hMainWindow, TRUE);
		 Cancel.Destroy ();
		 if (Ecode == 99)
		 {
             return -1;
		 }
		 if (prnret == 99)
		 {
             return -1;
		 }

		 if (Ecode != 0)
		 {
			 disp_mess ("Fehler beim Aufbereiten der Druckdaten", 2);
			 return -1;
		 }

		 if (PrintMode == 1)
		 {
		       sprintf (command, "%s\\32200.lst", tmp);
		       Cancel.SetAbrProc (BreakPrn);
		       EnableWindow (hMainWindow, FALSE);
               Cancel.Execute (hMainWindow, 28, 4,
			                          " \nBitte warten.....\n"
									  "Die Bestellung wird gedruckt.",
									   NULL);
               ret = Gdruck (command);
		       EnableWindow (hMainWindow, TRUE);
			   if (prnret) return prnret;
			   return ret;
		 }

		 if (PrintMode == 2)
		 {
		       sprintf (command, "%s\\32200.lst", tmp);
               if (SelectPrinter ())
			   {
				      InvalidateRect (hMainWindow, NULL, TRUE);
				      UpdateWindow (hMainWindow);
		              Cancel.SetAbrProc (BreakPrn);
		              EnableWindow (hMainWindow, FALSE);
	                  Cancel.Execute (hMainWindow, 28, 4,
			                          " \nBitte warten.....\n"
									  "Die Bestellung wird gedruckt.",
									   NULL);
                      ret = Gdruck (command);
		              EnableWindow (hMainWindow, TRUE);
			          if (prnret) return prnret;
			          return ret;
			   }
		       EnableWindow (hMainWindow, TRUE);
			   return -1;
		 }
		 else if (PrintMode == 3)
		 {
		       sprintf (command, "%s\\32200.lst", tmp);
			   if (SetPage ())
			   {
				      InvalidateRect (hMainWindow, NULL, TRUE);
				      UpdateWindow (hMainWindow);
		              Cancel.SetAbrProc (BreakPrn);
		              EnableWindow (hMainWindow, FALSE);
	                  Cancel.Execute (hMainWindow, 28, 4,
			                          " \nBitte warten.....\n"
									  "Die Bestellung wird gedruckt.",
									   NULL);
                      ret = Gdruck (command);
		              EnableWindow (hMainWindow, TRUE);
			          if (prnret) return prnret;
			          return ret;
			   }
		       EnableWindow (hMainWindow, TRUE);
			   return -1;
		 }
         EnableWindow (hMainWindow, TRUE);
		 disp_mess ("Fehler bim Drucken", 2);
		 return -1;
}

int KompBest::FaxBest (HWND hMainWindow, short mdn, short fil, long ang)
/**
Bestellung ausdrucken.
**/
{
	     char command [512];
		 char *tmp;
		 DWORD Ecode;
		 CA_CLASS Cancel;
		 int ret;


		 prnret = 0;
		 if (PrintMode == 0)
		 {
			 K_Liste_Fax ();
			 return 0;

/*		     Cancel.SetCaFunc (BreakPrn0);
		     EnableWindow (hMainWindow, FALSE);
             Cancel.Execute (hMainWindow, 28, 4,
			                          " \nBitte warten.....\n"
									  "Die Bestellung wird gedruckt.",
									   NULL);
			 sprintf (command, "fitprint %s %s\\%s.lst", drformat, sys_ben.pers_nam, tmp);
	         Pid = ProcExecPid (command, SW_SHOWNORMAL, -1, 0, -1, 0); 
	         Ecode = WaitPid (Pid); 
		     EnableWindow (hMainWindow, TRUE);
		     if (Ecode == 99)
			 {
                     return -1;
			 }
		     if (prnret == 99)
			 {
                     return -1;
			 }
		     if (Ecode != 0)
			 {
			         disp_mess ("Fehler beim Drucken", 2);
			         return -1;
			 }
			 return prnret;
*/
         }

         prnret = 0;
		 SetpDevMode (NULL);
		 WindowOk = TRUE;
		 tmp = getenv ("TMPPATH");
		 if (tmp == NULL)
		 {
			 tmp = "C:";
		 }

		 sprintf (command, "bws_asc -os\"where mdn = %hd and fil = %hd "
			              "and best_blg = %ld\" %s\\32200.lst 32200.if",
						   mdn, fil, best_kopf.best_blg, tmp);

		 EnableWindow (hMainWindow, FALSE);
		 Cancel.SetCaFunc (BreakAufb);
 	     Cancel.Execute (hMainWindow, 41, 4, " \nBitte warten.....\n"
								 "Die Bestellung wird zum Drucken vorbereitet.",
								 NULL);
	     Pid = ProcExecPid (command, SW_MINIMIZE, -1, 0, -1, 0); 
	     Ecode = WaitPid (Pid); 
		 EnableWindow (hMainWindow, TRUE);
		 Cancel.Destroy ();
		 if (Ecode == 99)
		 {
             return -1;
		 }
		 if (prnret == 99)
		 {
             return -1;
		 }

		 if (Ecode != 0)
		 {
			 disp_mess ("Fehler beim Aufbereiten der Druckdaten", 2);
			 return -1;
		 }

		 if (PrintMode == 1)
		 {
		       sprintf (command, "%s\\32200.lst", tmp);
		       Cancel.SetAbrProc (BreakPrn);
		       EnableWindow (hMainWindow, FALSE);
               Cancel.Execute (hMainWindow, 28, 4,
			                          " \nBitte warten.....\n"
									  "Die Bestellung wird gedruckt.",
									   NULL);
               ret = Gdruck (command);
		       EnableWindow (hMainWindow, TRUE);
			   if (prnret) return prnret;
			   return ret;
		 }

		 if (PrintMode == 2)
		 {
		       sprintf (command, "%s\\32200.lst", tmp);
               if (SelectPrinter ())
			   {
				      InvalidateRect (hMainWindow, NULL, TRUE);
				      UpdateWindow (hMainWindow);
		              Cancel.SetAbrProc (BreakPrn);
		              EnableWindow (hMainWindow, FALSE);
	                  Cancel.Execute (hMainWindow, 28, 4,
			                          " \nBitte warten.....\n"
									  "Die Bestellung wird gedruckt.",
									   NULL);
                      ret = Gdruck (command);
		              EnableWindow (hMainWindow, TRUE);
			          if (prnret) return prnret;
			          return ret;
			   }
		       EnableWindow (hMainWindow, TRUE);
			   return -1;
		 }
		 else if (PrintMode == 3)
		 {
		       sprintf (command, "%s\\32200.lst", tmp);
			   if (SetPage ())
			   {
				      InvalidateRect (hMainWindow, NULL, TRUE);
				      UpdateWindow (hMainWindow);
		              Cancel.SetAbrProc (BreakPrn);
		              EnableWindow (hMainWindow, FALSE);
	                  Cancel.Execute (hMainWindow, 28, 4,
			                          " \nBitte warten.....\n"
									  "Die Bestellung wird gedruckt.",
									   NULL);
                      ret = Gdruck (command);
		              EnableWindow (hMainWindow, TRUE);
			          if (prnret) return prnret;
			          return ret;
			   }
		       EnableWindow (hMainWindow, TRUE);
			   return -1;
		 }
         EnableWindow (hMainWindow, TRUE);
		 disp_mess ("Fehler bim Drucken", 2);
		 return -1;
}

