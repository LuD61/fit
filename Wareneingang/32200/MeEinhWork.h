#ifndef _MEEINHWORK_DEF
#define _MEEINHWORK_DEF
#include "searchptab.h"
#include "liefmebest.h"
#include "lief_bzg.h"
#include "lief.h"
#include "ptab.h"
#include "mdn.h"

class MeEinhWork
{
    private :
        LIEFMEBEST_CLASS LiefMeBest;
        LIEF_BZG_CLASS LiefBzg;
        PTAB_CLASS Ptab;
        LIEF_CLASS Lief;
        MDN_CLASS Mdn;
        int liefmebest_cursor;

    public :
        MeEinhWork () : liefmebest_cursor (-1)
        {
        }

        void BeginWork (void);
        void CommitWork (void);
        void RollbackWork (void);
        void InitRec (void);
        int ReadLief (short, short, char *);
        int ReadLief (void);
        int DeleteLief (void);
        BOOL TestLiefBzg (void);
        int ReadA (short, short, char *, double);
        int ReadA (void);
        int WriteA (void);
        int DeleteA (void);
        int GetPtab (char *, char *, char *);
        int ShowPtab (char *dest, char *item);
        int ShowPtabEx (HWND, char *, char *);
        int GetMdnName (char *, short);
        int GetFilName (char *, short, short);
        int GetLiefName (char *, short, char *);
        void TestLiefPr (void);
        int GetLiefDefaults (void);
        double GetLiefBzgInh (double, int);
};
#endif