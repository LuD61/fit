/**
Test der an das Programm uebergebenen Argumente
**/

int scroll_arg (char **arg, short pos, int anz)
/**
Argumenete nach links scrollen.
**/
{

       anz --;
       for (;pos < anz; pos ++)
       {
                  arg [pos] = arg [pos + 1];
       }
       return (anz);
}

void argtst (int *arganz, char **arg, void (*tst_arg) (char *))
{

       int anz;
       int i;

       anz = *arganz;
       for (i = 0; i < anz; i ++)
       {
              if (arg [i] [0] == '-')
              {
                            (*tst_arg) (&arg[i][1]);
                            anz = scroll_arg (arg,i,anz);
                            i --;
              }
       }
       *arganz = anz;
}
