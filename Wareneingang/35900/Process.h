#ifndef _PROCESS_DEF
#define _PROCESS_DEF
#include "Text.h"

class CProcess
{
  private :
        HANDLE Pid;
        DWORD ExitCode; 
        STARTUPINFO sti;
        PROCESS_INFORMATION pi;
		Text Command;
		int x, y, cx, cy;
		WORD ShowMode;

  public :
	 CProcess ();
	 CProcess (LPSTR);
	 CProcess (Text&);
	 ~CProcess ();
	 void Init ();
	 void SetCommand (LPSTR);
	 void SetCommand (Text&);
	 void SetSize (int,int,int,int);
     void SetShowMode (WORD);
	 HANDLE Start ();
	 HANDLE Start (WORD);
     DWORD WaitForEnd ();
	 HANDLE GetPid ();
	 BOOL Stop ();
	 BOOL IsActive ();
};
#endif 