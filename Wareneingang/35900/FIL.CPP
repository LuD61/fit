#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#ifndef CONSOLE
#include "wmask.h"
#include "mo_meld.h"
#include "dbfunc.h"
#endif
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "fil.h"

struct FIL _fil, _fil_null;

static void FillUb  (char *buffer)
/**
Ueberschrift fuer Auswahl fuellen.
**/
{
         sprintf (buffer, "%7s %-16s",
                          "Filiale", "Name");
}

static void FillValues  (char *buffer)
/**
Werte fuer Auswahl fuellen.
**/
{
         sprintf (buffer, "%7hd %-16.16s",
                          _fil.fil, _adr.adr_krz);
}


int FIL_CLASS::lese_fil (short mdn, short fil)
/**
Tabelle mdn fuer Mandant mdn lesen.
**/
{
         if (cursor_fil == -1)
         {
                ins_quest ((char *) &_fil.mdn, 1, 0);
                ins_quest ((char *) &_fil.fil, 1, 0);
                out_quest ((char *) _adr.adr_krz, 0, 17);
                out_quest ((char *) &_fil.fil_gr, 1, 0);
                out_quest ((char *) _fil.smt_kz, 0, 2);
                cursor_fil =
                    prepare_sql ("select adr.adr_krz, fil.fil_gr, smt_kz "
                                 "from fil,adr "
                                 "where fil.mdn = ? "
                                 "and fil.fil = ? "
                                 "and fil.adr = adr.adr");
         }
         _fil.mdn = mdn;
         _fil.fil = fil;
         open_sql (cursor_fil);
         fetch_sql (cursor_fil);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int FIL_CLASS::lese_fil (void)
/**
Naechsten Satz aus Tabelle mdn lesen.
**/
{
         if (cursor_fil != -1) fetch_sql (cursor_fil);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int FIL_CLASS::lese_fil_gr (short mdn, short fil_gr)
/**
Tabelle fil fuer Filialgruppe mdn lesen.
**/
{
         if (cursor_fil_gr == -1)
         {
                ins_quest ((char *) &_fil.mdn, 1, 0);
                ins_quest ((char *) &_fil.fil_gr, 1, 0);
                out_quest ((char *) _adr.adr_krz, 0, 17);
                out_quest ((char *) &_fil.fil, 1, 0);
                cursor_fil_gr =
                    prepare_sql ("select adr.adr_krz, fil.fil "
                                 "from fil,adr "
                                 "where fil.mdn = ? "
                                 "and fil.fil_gr = ? "
                                 "and fil.adr = adr.adr");
         }
         _fil.mdn = mdn;
         _fil.fil_gr = fil_gr;
         open_sql (cursor_fil_gr);
         fetch_sql (cursor_fil_gr);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int FIL_CLASS::lese_fil_gr (void)
/**
Naechsten Satz aus Tabelle mdn lesen.
**/
{
         if (cursor_fil_gr != -1) fetch_sql (cursor_fil_gr);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}


void FIL_CLASS::close_fil (void)
/**
Cursor fil schliessen.
**/
{
          if (cursor_fil == -1) return;
          close_sql (cursor_fil);
          cursor_fil = -1;
}


void FIL_CLASS::close_fil_gr (void)
/**
Cursor fil_gr schliessen.
**/
{
          if (cursor_fil_gr == -1) return;
          close_sql (cursor_fil_gr);
          cursor_fil_gr = -1;
}

#ifndef CONSOLE

int FIL_CLASS::ShowAllFil (short mdn)
/**
Auswahl ueber Mandanten anzeigen.
**/
{
         int cursor_ausw;
         int i;

         ins_quest ((char *) &_fil.mdn, 1, 0);
         out_quest ((char *) &_fil.fil, 1, 0);
         out_quest (_adr.adr_krz, 0, 16);

         cursor_ausw = prepare_scroll ("select fil.fil, adr.adr_krz from "
                                       "fil,adr "
                                       "where fil.fil > 0 "
                                       "and fil.mdn = ? "
                                       "and adr.adr = fil.adr "
                                       "order by fil");
         if (sqlstatus)
         {
                     return (-1);
         }
         _fil.mdn = mdn;
         i = DbAuswahl (cursor_ausw, FillUb,
                                     FillValues);
         UpdateFkt ();
         SetFocus (current_form->mask[currentfield].feldid);

         SetCurrentField (currentfield);

         if (syskey == KEYESC || syskey == KEY5)
         {
                     close_sql (cursor_ausw);
                     return 0;
         }
         fetch_scroll (cursor_ausw, DBABSOLUTE, i + 1);
         close_sql (cursor_ausw);
         return 0;
}

#endif // CONSOLE
