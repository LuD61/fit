#ifndef MO_A_PR_OK
#define MO_A_PR_OK 1
class AB_PREISE
{
            private :
              int tst;
              int dsqlstatus;
              int mit_flb;
              short  k_mgruppe;
              short k_mandant;
              short k_fgruppe;
              short k_filiale;
              double k_artikel;
              char k_datum[11];
              double  ek_preis;
              double  vk_preis;
              double  ek_preis_euro;
              double  vk_preis_euro;
              char gueltig [2];
              static char waehr_prim [];

              MDN_CLASS mdn_class;
              FIL_CLASS fil_class;
              A_PR_CLASS a_pr_class;
              AKT_KRZ_CLASS akt_krz_class;
              AKTION_CLASS aktion_class;

              void fetch_waehr_prim (short);
              short fetch_mdn_gr (short);
              short fetch_fil_gr (short, short);
              short hole_aktion (short, double *,double *);
              short hole_gueltig (double *, double *);
              int fetch_akt_dat (void);
              fetch_akt_kopf_dat (short, short, short, short,
                                  double, char *);
              int akt_comp (void);
              int debug (char *, ...);

            public :
              AB_PREISE () 
              {
                       tst = 0;
                       mit_flb = 1;
              }

              void mitfilbelosa (void)
              {
                          mit_flb = 1;
              }

              void ohnefilbelosa ()
              {
                          mit_flb = 0;
              }
              int fetch_a_pr (short, short, short, short, double);
              int wr_a_pr (void);
              int fetch_preis_lad (short,short,short,short,double,
                                   double *, double *);
              int fetch_preis_tag (short,short,short,short,double,
                                   char *,
                                   double *, double *);
};
#endif
