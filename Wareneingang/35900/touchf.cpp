#include<stdio.h>
#include<stdarg.h>
#include<string.h>
#include<windows.h>
#include<winbase.h>
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_draw.h"
#include "wmask.h"
#include "mo_meld.h"
#include "touchf.h" 
#include "Text.h" 

/** Ab hier Auswahl fuer Preiseingabe 
**/

static HWND eFunkWindow;
static break_funk_enter = 0;
static HANDLE    hInstance = NULL;
static HANDLE    hMainInst = NULL;
static HWND      hMainWindow = NULL;
static HWND      ParentWindow;

static WNDPROC FunkEProc = NULL;
static NUBLOCK NuBlock;

static void (*WriteParamKiste) (int) = NULL;
static void (*WriteParamEZ) (int)    = NULL;
static void (*WriteParamEV) (int)    = NULL;
static void (*WriteParamArt) (int)   = NULL;
static void (*Bu5Proc) (int)   = NULL;

static mfont FunkTextFont = {"Arial", 200, 0, 1, RGB (0, 0, 0),
                                         RGB (255, 255, 255),
                                         1,
                                         NULL};


static mfont funkfont   = {"", 120, 0, 1,
                     RGB (255, 0, 0),
                     BLUECOL,
                     1,
                     NULL};


static ColButton Bu1    =   {"&Kiste", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         RGB (0,   0,   255),
                         2};

static ColButton Bu2    =   {"&EZ", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         RGB (0,   0,   255),
                        2};

static ColButton Bu3    =   {"E&V", -1, -1,
                          NULL, 0, 0,
                          NULL, 0, 0,
                          NULL, 0, 0,
                          NULL, 0, 0,
                          RGB (255, 255, 255),
                          RGB (0,   0,   255),
                          2};
static ColButton Bu4    =   {"&Artikel", -1, -1,
                          NULL, 0, 0,
                          NULL, 0, 0,
                          NULL, 0, 0,
                          NULL, 0, 0,
                          RGB (255, 255, 255),
                          RGB (0,   0,   255),
                          2};
static ColButton Bu5    =   {"&Button", -1, -1,
                          NULL, 0, 0,
                          NULL, 0, 0,
                          NULL, 0, 0,
                          NULL, 0, 0,
                          RGB (255, 255, 255),
                          RGB (0,   0,   255),
                          2};

ColButton BuBack =   {"&Zur�ck", -1, -1,
                          NULL, 0, 0,
                          NULL, 0, 0,
                          NULL, 0, 0,
                          NULL, 0, 0,
                          RGB (255, 255, 255),
                          RGB (255,   0,   0),
                          2};


static HWND fhWnd;
static int pnbss0 = 80;
static int pny    = 3;
static int pnx    = 1;
static int pcbss0 = 100;
static int pcbss  = 64; 
static int pcbsz  = 64;
static int pcbsz0 = 100;

static int dobu1 (void);
static int dobu2 (void);
static int dobu3 (void);
static int dobu4 (void);
static int doback (void);
static int dobu5 (void);


static field _Funk1[] = {
(char *) &BuBack,        pcbss0, pcbsz0, pny, pnx + 4 * pcbss0 , 0, "",
                                                           COLBUTTON, 0,
                                                           doback, 0,
(char *) &Bu1,           pcbss0, pcbsz0, pny, pnx,        0, "",
                                                           COLBUTTON, 0,
                                                           dobu1, 0,
(char *) &Bu2,           pcbss0, pcbsz0, pny, pnx + 1 * pcbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           dobu2, 0,
(char *) &Bu3,           pcbss0, pcbsz0, pny, pnx + 2 * pcbss0 , 0, "",
                                                           COLBUTTON, 0,
                                                           dobu3, 0,
(char *) &Bu4,           pcbss0, pcbsz0, pny, pnx + 3 * pcbss0 , 0, "",
                                                           COLBUTTON, 0,
                                                           dobu4, 0,
};

static field _Funk2[] = {
(char *) &BuBack,        pcbss0, pcbsz0, pny, pnx + 5 * pcbss0 , 0, "",
                                                           COLBUTTON, 0,
                                                           doback, 0,
(char *) &Bu1,           pcbss0, pcbsz0, pny, pnx,        0, "",
                                                           COLBUTTON, 0,
                                                           dobu1, 0,
(char *) &Bu2,           pcbss0, pcbsz0, pny, pnx + 1 * pcbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           dobu2, 0,
(char *) &Bu3,           pcbss0, pcbsz0, pny, pnx + 2 * pcbss0 , 0, "",
                                                           COLBUTTON, 0,
                                                           dobu3, 0,
(char *) &Bu4,           pcbss0, pcbsz0, pny, pnx + 3 * pcbss0 , 0, "",
                                                           COLBUTTON, 0,
                                                           dobu4, 0,
(char *) &Bu5,           pcbss0, pcbsz0, pny, pnx + 4 * pcbss0 , 0, "",
                                                           COLBUTTON, 0,
                                                           dobu5, 0,
};

static form Funk = {5, 0, 0, _Funk1, 0, 0, 0, 0, &funkfont};

static char funkbuff [256];

static field _FunkTextForm [] = {
funkbuff,             0, 0, 0, 0, 0, "", DISPLAYONLY, 0, 0, 0};

static form FunkTextForm = {1, 0, 0, _FunkTextForm, 0, 0, 0, 0, &FunkTextFont};


int GetBuStat (ColButton *CuB)
/**
Status des ColButton holen.
**/
{
	     return CuB->aktivate;
}


void FunkFocus (int pos)
/**
Naechsten Button aktivieren.
**/
{
         int AktButton;
		 int run;

         AktButton = pos; 
		 run = 0;
		 while (TRUE)
		 {
			  if (GetBuStat ((ColButton *) Funk.mask[AktButton].feld) != -1) break;
              if (AktButton == Funk.fieldanz - 1)
			  {
				     if (run == 1) break;
					 run = 1;
                     AktButton = 0;
			  }
              else
			  {
                     AktButton ++;
			  }
			  if (GetBuStat ((ColButton *) Funk.mask[AktButton].feld) != -1) break;
         }
         SetFocus (Funk.mask [AktButton].feldid);
         fhWnd = GetFocus ();
}

int GetAktFocus (void)
/**
Aktiven Button holen.
**/
{
        HWND hWnd;
        int i;

        hWnd = GetFocus ();

        for (i = 0; i < Funk.fieldanz; i ++)
        {
                    if (hWnd == Funk.mask[i].feldid)
                    {
                               return (i);
                    }
        }
        return (0);
}

int dobu1 (void)
{

        char anz [20];
        int AktButton;

		syskey = 0;
		AktButton = GetAktFocus ();
		sprintf (anz, "%8d", 1);
		if (TOUCHF::EnterAnz)
		{
				NuBlock.SetParent (ParentWindow);
                NuBlock.EnterCalcBox (eFunkWindow,"  Anzahl  ", anz, 9, "%8d", FunkEProc);
		}
		if (WriteParamKiste && syskey != KEY5)
		{
		         (*WriteParamKiste )(atoi (anz));
		}
        SetActiveWindow (eFunkWindow);
		FunkFocus (AktButton);
        return 0;
}


int dobu2 (void)
{
        char anz [20];
        int AktButton;

		syskey = 0;
		AktButton = GetAktFocus ();
		sprintf (anz, "%d", 1);
		if (TOUCHF::EnterAnz)
		{
			     NuBlock.SetParent (ParentWindow);
                 NuBlock.EnterCalcBox (eFunkWindow,"  Anzahl  ", anz, 9, "%8d", FunkEProc);
		}
		if (WriteParamEZ && syskey != KEY5)
		{
		         (*WriteParamEZ )(atoi (anz));
		}
        SetActiveWindow (eFunkWindow);
		FunkFocus (AktButton);
        return 0;
}


int dobu3 (void)
{
        char anz [20];
        int AktButton;

		syskey = 0;
		AktButton = GetAktFocus ();
		sprintf (anz, "%d", 1);
		if (TOUCHF::EnterAnz)
		{
			     NuBlock.SetParent (ParentWindow);
                 NuBlock.EnterCalcBox (eFunkWindow,"  Anzahl  ", anz, 9, "%8d", FunkEProc);
		}
		if (WriteParamEV && syskey != KEY5)
		{
		         (*WriteParamEV )(atoi (anz));
		}
        SetActiveWindow (eFunkWindow);
		FunkFocus (AktButton);
		syskey = 0;
        return 0;
}


int dobu4 (void)
{
        char anz [20];
        int AktButton;

		syskey = 0;
		AktButton = GetAktFocus ();
		sprintf (anz, "%d", 1);
		if (TOUCHF::EnterAnz)
		{
  			      NuBlock.SetParent (ParentWindow);
                  NuBlock.EnterCalcBox (eFunkWindow,"  Anzahl  ", anz, 9, "%8d", FunkEProc);
		}
		if (WriteParamArt && syskey != KEY5)
		{
		         (*WriteParamArt )(atoi (anz));
		}
        SetActiveWindow (eFunkWindow);
		FunkFocus (AktButton);
        return 0;
}


int dobu5 (void)
{
        int AktButton;

		syskey = 0;
		AktButton = GetAktFocus ();
		if (Bu5Proc != NULL)
		{
		         (*Bu5Proc )(0);
		}
        SetActiveWindow (eFunkWindow);
		FunkFocus (AktButton);
        return 0;
}


int doback (void)
{
        break_funk_enter = 1;
        return 0;
}


BOOL TOUCHF::EnterAnz = FALSE;

TOUCHF::TOUCHF ()
{
       Funk.mask[0].after = doback;        
       Funk.mask[1].after = dobu1;        
       Funk.mask[2].after = dobu2;        
       Funk.mask[3].after = dobu3;        
       Funk.mask[4].after = dobu4;        
	   etianz = 4;
       lastbu = 3;
}

void TOUCHF::SetBuText (ColButton& Bu, char *text)
{
	Token t;

	t.SetSep ("\n");
	t = text;
	char *p = strtok (text, "\n");
	if (p != NULL)
	{
        Bu.text1 = p;
		p = strtok (NULL, "\n");
	}

	if (p != NULL)
	{
         Bu.text2 = p;
 		 p = strtok (NULL, "\n");
	}

	if (p != NULL)
	{
         Bu.	text3 = p;
	}

	if (t.GetAnzToken () == 1)
	{
		Bu.ty1 = -1;
		Bu.ty2 = 0;
		Bu.text2 = "";
		Bu.ty3 = 0;
		Bu.text3 = "";
	}
	else if (t.GetAnzToken () == 2)
	{
		Bu.ty1 = 20;
		Bu.ty2 = 55;
		Bu.tx2 = -1;
		Bu.ty3 = 0;
		Bu.text3 = "";
	}
	else if (t.GetAnzToken () == 3)
	{
		Bu.ty1 = 12;
		Bu.ty2 = 40;
		Bu.tx2 = -1;
		Bu.ty3 = 68;
		Bu.tx3 = -1;
	}
}

void TOUCHF::SetEtiText (char *kiste, char *ez, char *ev, char *art)
{
    SetBuText (Bu1, kiste);
	SetBuText (Bu2, ez);
	SetBuText (Bu3, ev);
	SetBuText (Bu4, art);
    Funk.fieldanz = 5;
	Funk.mask = _Funk1;
}

void TOUCHF::SetEtiText (char *kiste, char *ez, char *ev, char *art, char *text5)
{
    SetBuText (Bu1, kiste);
	SetBuText (Bu2, ez);
	SetBuText (Bu3, ev);
	SetBuText (Bu4, art);
	SetBuText (Bu5, text5);
    Funk.fieldanz = 6;
	Funk.mask = _Funk2;
}

void TOUCHF::SetEtiAttr (char *kiste, char *ez, char *ev, char *art)
/**
Attribute fuer EtikettenButton setzen.
**/
{
	   etianz = 4; 
	   lastbu = 3;
	   int actbutab [] = {1,1,1,1};

	   if (kiste == NULL)
	   {
              ActivateColButton (NULL, &Funk, 1, -1, 0);
			  etianz --;
	          actbutab[0] = 0;;
	   }
	   else
	   {
              ActivateColButton (NULL, &Funk, 1, 2, 0);
	   }
	   if (ez == NULL)
	   {
              ActivateColButton (NULL, &Funk, 2, -1, 0);
			  etianz --;
	          actbutab[1] = 0;;
	   }
	   else
	   {
              ActivateColButton (NULL, &Funk, 2, 2, 0);
	   }
	   if (ev == NULL)
	   {
              ActivateColButton (NULL, &Funk, 3, -1, 0);
			  etianz --;
	          actbutab[2] = 0;;
	   }
	   else
	   {
              ActivateColButton (NULL, &Funk, 3, 2, 0);
	   }
	   if (art == NULL)
	   {
              ActivateColButton (NULL, &Funk, 4, -1, 0);
			  etianz --;
	          actbutab[3] = 0;;
	   }
	   else
	   {
              ActivateColButton (NULL, &Funk, 4, 2, 0);
	   }
       for (int i = 0; i < 4; i ++)
	   {
		   if (actbutab[i] == 1)
		   {
			   lastbu = i;
		       break;
		   }
	   }
}



int TOUCHF::FunkEnterBreak ()
/**
Endeflag fuer Erfassung setzen.
**/
{
        break_funk_enter = 1;
		return 0;
}

void TOUCHF::SetWriteParams (void (*wpki) (int),
			                   void (*wpez) (int),
							   void (*wpev) (int),
							   void (*wpar) (int))
{
	  WriteParamKiste = wpki;
	  WriteParamEZ    = wpez;
	  WriteParamEV    = wpev;
      WriteParamArt   = wpar;
      Funk.fieldanz = 5;
	  Funk.mask = _Funk1;
}

void TOUCHF::SetWriteParams (void (*wpki) (int),
			                   void (*wpez) (int),
							   void (*wpev) (int),
							   void (*wpar) (int),
							   void (*wp5) (int)
							   )
{
	  WriteParamKiste = wpki;
	  WriteParamEZ    = wpez;
	  WriteParamEV    = wpev;
      WriteParamArt   = wpar;
      Bu5Proc         =wp5; 
      Funk.fieldanz = 6;
	  Funk.mask = _Funk2;
}

HWND TOUCHF::IsChild (POINT *mousepos)
/**
Test, ob sich der Maus-Cursor in einem Kindfenster befindet.
**/
{
        int i;

        for (i = 0; i < Funk.fieldanz; i ++)
        {
                   if (MouseinWindow (Funk.mask[i].feldid,
                                      mousepos))
                   {
                                 return (Funk.mask[i].feldid);
                   }
        }

        return NULL;
}


int TOUCHF::OnPaint (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (eFunkWindow == NULL) return 0;
		if (hWnd == eFunkWindow)
		{
                 display_form (eFunkWindow, &Funk, 0, 0);
                 SetFocus (fhWnd);
		}
		else
		{
                 NuBlock.OnPaint (hWnd, msg, wParam, lParam);
		}
		return 0;
}

int TOUCHF::OnButton (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
/*
        POINT mousepos;
        HWND enchild;
*/

        if (eFunkWindow == NULL) return 0;
/*
        GetCursorPos (&mousepos);
        enchild = IsChild(&mousepos);
        MouseTest (enchild, msg);
        if (enchild)
        {
                  SendMessage (enchild, msg, wParam, lParam);
                  return TRUE;
        }
		else
*/
		{
                  return NuBlock.OnButton (hWnd, msg, wParam, lParam);
		}
        return TRUE;
}


void TOUCHF::RegisterFunk (WNDPROC FunkProc)
/**
Fenster fuer Preis registrieren.
**/
{
        static int registered = 0;
        WNDCLASS wc;

        if (registered == 0)
        {
                   wc.style =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                               | CS_OWNDC;
                   wc.cbClsExtra    =  0;
                   wc.cbWndExtra    =  0;
                   wc.hInstance     =  hMainInst;
                   wc.hIcon         =  LoadIcon (hMainInst, "FITICON");
                   wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                   wc.lpszMenuName  =  "";
                   wc.lpfnWndProc   =  FunkProc;
                   wc.lpszMenuName  =  "";
                   wc.hbrBackground =  CreateSolidBrush (LTGRAYCOL);
                   wc.lpszClassName =  "EnterFunk";
                   RegisterClass(&wc);
                   registered = 1;
        }
}

int TOUCHF::GetAktButton (void)
/**
Aktiven Button holen.
**/
{
        HWND hWnd;
        int i;

        hWnd = GetFocus ();

        for (i = 0; i < Funk.fieldanz; i ++)
        {
                    if (hWnd == Funk.mask[i].feldid)
                    {
                               return (i);
                    }
        }
        return (0);
}


void TOUCHF::PrevKey (void)
/**
Vorhergenden Button aktivieren.
**/
{
         int AktButton;
		 int run;

         AktButton = GetAktButton (); 
		 run = 0;
		 while (TRUE)
		 {
              if (AktButton == 0)
			  {
                     if (run == 1) break;
					 run = 1;
				     AktButton = Funk.fieldanz - 1;
			  }
              else
			  {
                     AktButton --;
			  }
			  if (GetBuStat ((ColButton *) Funk.mask[AktButton].feld) != -1) break;
		 }
         SetFocus (Funk.mask [AktButton].feldid);
         fhWnd = GetFocus ();
}


void TOUCHF::NextKey (void)
/**
Naechsten Button aktivieren.
**/
{
         int AktButton;
		 int run;

         AktButton = GetAktButton (); 
		 run = 0;
		 while (TRUE)
		 {
              if (AktButton == Funk.fieldanz - 1)
			  {
				     if (run == 1) break;
					 run = 1;
                     AktButton = 0;
			  }
              else
			  {
                     AktButton ++;
			  }
			  if (GetBuStat ((ColButton *) Funk.mask[AktButton].feld) != -1) break;
         }
         SetFocus (Funk.mask [AktButton].feldid);
         fhWnd = GetFocus ();
}

void TOUCHF::ActionKey (void)
/**
Button-Aktion ausfuehren.
**/
{
         int AktButton;
         field *feld;

         AktButton = GetAktButton ();
         feld =  &Funk.mask[AktButton];

         if (feld->after != (int (*) (void)) 0)
         {
                    (*feld->after) ();
         }
         else if (feld->BuId != 0)
         {
                    SendMessage (GetParent (feld->feldid), WM_COMMAND,
                                            MAKELONG (feld->BuId,
                                            feld->feldid),
                                            0l);
         }
}
         
int TOUCHF::IsFunkKey (MSG *msg)
/**
HotKey Testen.
**/
{
         static char keypressed = 0;
         static HWND keyhwnd = 0;
         ColButton *ColBut;
         int taste;
         int i;
         char *pos;
         
         switch (msg->message)
         {
              case WM_KEYDOWN :
              {
                     if (keypressed)
                     {
                                  return FALSE;
                     }
                     switch (msg->wParam)
                     {
                            case VK_F5 :
                               FunkEnterBreak ();
                               syskey = KEY5;
                               return TRUE;
                            case  VK_UP :
                               PrevKey ();
                               return TRUE;
                            case VK_DOWN :
                               NextKey ();
                               return TRUE;
                            case VK_RIGHT :
                               NextKey ();
                               return TRUE;
                            case VK_LEFT :
                               PrevKey ();
                               return TRUE;
                            case VK_TAB :
                               if (GetKeyState (VK_SHIFT) < 0)
                               {
                                      PrevKey ();
                                      return TRUE;
                               }
                               NextKey ();
                               return TRUE;
                             case VK_RETURN :
                               ActionKey ();
                               return TRUE;
                     }
                     taste = (int) msg->wParam;
                     for (i = 0; i < 3; i ++)
                     {
                        ColBut = (ColButton *) Funk.mask[i].feld;
                        if (ColBut->text1)
                        {
                           if ((pos = strchr (ColBut->text1, '&')) &&
                               ((UCHAR) toupper (*(pos + 1)) == taste))
                           {
                                        keypressed = 1;
                                        keyhwnd = Funk.mask[i].feldid;
                                        SendMessage (keyhwnd,
                                                WM_LBUTTONDOWN,
                                                0l,
                                                0l);
                                        return TRUE;
                            }
                       }
                     }
                     return FALSE;
              }
              case WM_KEYUP :
              {
                     taste = (int) msg->wParam;
                     for (i = 0; i < 3; i ++)
                     {
                         if (keyhwnd == Funk.mask[i].feldid)
                         {
                                   keypressed = 0;
                                   SendMessage (keyhwnd,
                                                WM_LBUTTONUP,
                                                0l,
                                                0l);
                                   keyhwnd = NULL;
                                   return TRUE;
                         }
                     }
                     return FALSE;
              }
          }
          return FALSE;
}

void TOUCHF::MainInstance (HANDLE MainInst, HWND hWnd)
{
	    hInstance   = MainInst;
	    hMainInst   = MainInst;
		hMainWindow = hWnd;
		NuBlock.MainInstance (MainInst, hWnd);
}

void TOUCHF::ScreenParam (double scrx, double scry)
/**
Paren-Fenster setzen.
**/
{
	     NuBlock.ScreenParam (scrx, scry);
}

void TOUCHF::SetParent (HWND Parent)
/**
Paren-Fenster setzen.
**/
{
	     ParentWindow = Parent;
}

void TOUCHF::StartDirect ()
{
            break_funk_enter = TRUE;
			switch (lastbu)
			{
			case 0:
					dobu1 ();
					return;
			case 1:
					dobu2 ();
					return;
			case 2:
					dobu3 ();
					return;
			case 3:
					dobu4 ();
					return;
			}
}

void TOUCHF::EnterFunkBox (HWND hWnd, int px, int py, WNDPROC fproc)
/**
Fenster fuer Preisauswahl oeffnen.
**/
{
        RECT rect;
        MSG msg;
        int x, y, cx, cy;
        HCURSOR oldcursor;
        static int BitMapOK = 0;

        FunkEProc = fproc;
        RegisterFunk (fproc);
        GetWindowRect (hWnd, &rect);
        cx = Funk.fieldanz * pcbss0 + 7;
        cy = pcbsz0 + 10;
        if (px == -1)
        {
                  x = rect.left + (rect.right - rect.left - cx) / 2;
        }
        else
        {
                  x = px;
        }
        if (py == -1)
        {
                  y = (rect.bottom - cy) / 2;
        }
        else
        {
                  y = py;
        }

        eFunkWindow  = CreateWindowEx (
                              WS_EX_CLIENTEDGE,
                              "EnterFunk",
                              "",
                              WS_POPUP,
                              x, y,
                              cx, cy,
                              hWnd,
                              NULL,
                              hMainInst,
                              NULL);

        ShowWindow (eFunkWindow, SW_SHOW);
        UpdateWindow (eFunkWindow);
        oldcursor = SetCursor (LoadCursor (NULL, IDC_ARROW));
        FunkFocus (1);
        fhWnd = GetFocus ();

		EnableWindows (hWnd, FALSE); 
		EnableWindow (hMainWindow, FALSE); 
        break_funk_enter = 0;
		if (etianz == 1)
		{
 		      PostMessage (eFunkWindow, WM_DIRECT, 0, 0l);
		}
        while (GetMessage (&msg, NULL, 0, 0))
        {
              if (IsFunkKey (&msg));
              else
              {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
              }
              if (break_funk_enter) break;
        }
		EnableWindows (hWnd, TRUE); 
		EnableWindow (hMainWindow, TRUE); 
        CloseControls (&Funk);
        DestroyWindow (eFunkWindow);
        eFunkWindow = NULL;
        SetCursor (oldcursor);
}
