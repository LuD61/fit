/*   Datenbankfunktionen                                  */

struct A_BAS {
   double    a;
   short     mdn;
   short     fil;
   char      a_bz1[25];
   char      a_bz2[25];
   double    a_gew;
   short     a_typ;
   short     a_typ2;
   short     abt;
   short     ag;
   char      best_auto[2];
   char      bsd_kz[2];
   char      cp_aufschl[2];
   short     delstatus;
   short     dr_folge;
   long      erl_kto;
   char      hbk_kz[2];
   short     hbk_ztr;
   char      hnd_gew[2];
   short     hwg;
   char      kost_kz[3];
   short     me_einh;
   char      modif[2];
   short     mwst;
   short     plak_div;
   char      stk_lst_kz[2];
   double    sw;
   short     teil_smt;
   long      we_kto;
   short     wg;
   short     zu_stoff;
   long      akv;
   long      bearb;
   char      pers_nam[9];
   double    prod_zeit;
   char      pers_rab_kz[2];
   double    gn_pkt_gbr;
   long      kost_st;
   char      sw_pr_kz[2];
   long      kost_tr;
   double    a_grund;
   long      kost_st2;
   long      we_kto2;
   long      charg_hand;
   long      intra_stat;
   char      qual_kng[5];
   short     zerl_eti; 
   short	sais;
   short	bereich;
};

extern struct A_BAS _a_bas, _a_bas_null;
extern char ag_tier_kz[3];   // GK 25.06.2013

int lese_a (char *);
int lese_a (void);
int lese_a_bas (double);
int Auswahla_basQuery ();
int Auswahla_bas ();
int Showa_bas (char *);
