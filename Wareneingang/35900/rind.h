#ifndef _RIND_DEF
#define _RIND_DEF

#include "windows.h"
#include "mo_draw.h"
#include "wmask.h"
#include "dbclass.h"

struct RIND {
   double    a;
   char      ls_ident[21];
   char      ls_charge[21];
   char      ls_ez[21];
   char      ls_es[21];
   char      ls_land[21];
   short     gebland;
   short     mastland;
   short     schlaland;
   short     zerlland;
   char      geblandk[3];
   char      mastlandk[3];
   char      schlalandk[3];
   char      zerllandk[3];
};
extern struct RIND rind, rind_null;

#line 10 "rind.rh"

class RIND_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               RIND_CLASS () : DB_CLASS ()
               {
               }
               ~RIND_CLASS ()
               {
               } 
};
#endif
