#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
#include "Sounds.h"


CSounds::CSounds ()
{
		this->len  = 1;
		this->wait = 1;
		this->type = MB_ICONASTERISK;
		SoundName = "";
}


CSounds::CSounds (int len, int wait, UINT type)
{
		this->len  = len;
		this->wait = wait;
		this->type = type;
		SoundName = "";
}

CSounds::CSounds (LPSTR SoundName)
{
		this->len  = 1;
		this->wait = 1;
		this->type = MB_ICONASTERISK;
		char *etc = getenv ("BWSETC");
		if (etc != NULL)
		{
  		         this->SoundName.Format ("%s\\%s", etc,SoundName);
		}
		else
		{
			     this->SoundName = SoundName;
		}
}


void CSounds::Run ()
{
	    MSG msg;
		
		for (int i = 0; i < len; i ++)
		{
			MessageBeep (type);
            if (GetMessage (&msg, NULL, 0, 0))
			{
                 TranslateMessage(&msg);
                 DispatchMessage(&msg);
			}
			Sleep (wait);
		}
}

void CSounds::Play ()
{  
	    if (SoundName.GetLen () == 0)
		{
			 Run ();
			 return;
		}
		FILE *fp;
		fp = fopen (SoundName.GetBuffer (), "r");
		if (fp == NULL)
		{
			MessageBeep (type);
			return;
		}
		fclose (fp);
		if (PlaySound ((LPCSTR) SoundName.GetBuffer (), NULL, SND_FILENAME) == FALSE)
		{
			 len = 2;
			 Run ();
		}
}


void CSounds::Play (int Wave)
{
		if (PlaySound (MAKEINTRESOURCE (Wave), NULL, SND_RESOURCE) == FALSE)
		{
			 len = 2;
			 Run ();
		}
}

