#ifndef _PROG_CFG
#define _PROG_CGF


class PROG_CFG 
         {
          private :
                char progname [80];
                FILE *progfp;
          public :
              PROG_CFG (char *progname) : progfp (NULL)
              {
                       if (progname)
                       {
                               strcpy (this->progname, progname);
                       }
              }
              void SetProgName (char *progname)
              {
                       if (progname)
                       {
                               strcpy (this->progname, progname);
                       }
              }
              BOOL OpenCfg (void);
              void CloseCfg (void);
              BOOL GetCfgValue (char *, char *);
              BOOL ReadCfgItem (char *, char *, char *, char **, char *);
              BOOL ReadCfgValues (char **, char *);
              BOOL ReadCfgHelp (char *);
              BOOL  GetGroupDefault (char *, char *);
};
#endif         
                          
                               

