#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "wmaskc.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "a_best.h" 

struct A_BEST a_best, a_best_null;

void A_BEST_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &a_best.mdn, 1, 0);
            ins_quest ((char *) &a_best.fil, 1, 0);
            ins_quest ((char *) &a_best.a, 3, 0);
    out_quest ((char *) &a_best.a,3,0);
    out_quest ((char *) &a_best.best_anz,1,0);
    out_quest ((char *) a_best.best_auto,0,2);
    out_quest ((char *) &a_best.bsd_min,3,0);
    out_quest ((char *) &a_best.bsd_min_emp,3,0);
    out_quest ((char *) &a_best.bsd_max_emp,3,0);
    out_quest ((char *) &a_best.delstatus,1,0);
    out_quest ((char *) a_best.erf_gew_kz,0,2);
    out_quest ((char *) &a_best.fil,1,0);
    out_quest ((char *) a_best.letzt_lief_nr,0,17);
    out_quest ((char *) a_best.lief,0,17);
    out_quest ((char *) a_best.lief_best,0,17);
    out_quest ((char *) &a_best.lief_s,2,0);
    out_quest ((char *) &a_best.mdn,1,0);
    out_quest ((char *) &a_best.me_einh_ek,1,0);
    out_quest ((char *) a_best.umk_kz,0,2);
            cursor = prepare_sql ("select a_best.a,  "
"a_best.best_anz,  a_best.best_auto,  a_best.bsd_min,  "
"a_best.bsd_min_emp,  a_best.bsd_max_emp,  a_best.delstatus,  "
"a_best.erf_gew_kz,  a_best.fil,  a_best.letzt_lief_nr,  a_best.lief,  "
"a_best.lief_best,  a_best.lief_s,  a_best.mdn,  a_best.me_einh_ek,  "
"a_best.umk_kz from a_best "

#line 22 "a_best.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   a = ?");

    ins_quest ((char *) &a_best.a,3,0);
    ins_quest ((char *) &a_best.best_anz,1,0);
    ins_quest ((char *) a_best.best_auto,0,2);
    ins_quest ((char *) &a_best.bsd_min,3,0);
    ins_quest ((char *) &a_best.bsd_min_emp,3,0);
    ins_quest ((char *) &a_best.bsd_max_emp,3,0);
    ins_quest ((char *) &a_best.delstatus,1,0);
    ins_quest ((char *) a_best.erf_gew_kz,0,2);
    ins_quest ((char *) &a_best.fil,1,0);
    ins_quest ((char *) a_best.letzt_lief_nr,0,17);
    ins_quest ((char *) a_best.lief,0,17);
    ins_quest ((char *) a_best.lief_best,0,17);
    ins_quest ((char *) &a_best.lief_s,2,0);
    ins_quest ((char *) &a_best.mdn,1,0);
    ins_quest ((char *) &a_best.me_einh_ek,1,0);
    ins_quest ((char *) a_best.umk_kz,0,2);
            sqltext = "update a_best set a_best.a = ?,  "
"a_best.best_anz = ?,  a_best.best_auto = ?,  a_best.bsd_min = ?,  "
"a_best.bsd_min_emp = ?,  a_best.bsd_max_emp = ?,  "
"a_best.delstatus = ?,  a_best.erf_gew_kz = ?,  a_best.fil = ?,  "
"a_best.letzt_lief_nr = ?,  a_best.lief = ?,  a_best.lief_best = ?,  "
"a_best.lief_s = ?,  a_best.mdn = ?,  a_best.me_einh_ek = ?,  "
"a_best.umk_kz = ? "

#line 27 "a_best.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   a = ?";

            ins_quest ((char *) &a_best.mdn, 1, 0);
            ins_quest ((char *) &a_best.fil, 1, 0);
            ins_quest ((char *) &a_best.a, 3, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &a_best.mdn, 1, 0);
            ins_quest ((char *) &a_best.fil, 1, 0);
            ins_quest ((char *) &a_best.a, 3, 0);
            test_upd_cursor = prepare_sql ("select a from a_best "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   a = ?");
            ins_quest ((char *) &a_best.mdn, 1, 0);
            ins_quest ((char *) &a_best.fil, 1, 0);
            ins_quest ((char *) &a_best.a, 3, 0);
            del_cursor = prepare_sql ("delete from a_best "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   a = ?");
    ins_quest ((char *) &a_best.a,3,0);
    ins_quest ((char *) &a_best.best_anz,1,0);
    ins_quest ((char *) a_best.best_auto,0,2);
    ins_quest ((char *) &a_best.bsd_min,3,0);
    ins_quest ((char *) &a_best.bsd_min_emp,3,0);
    ins_quest ((char *) &a_best.bsd_max_emp,3,0);
    ins_quest ((char *) &a_best.delstatus,1,0);
    ins_quest ((char *) a_best.erf_gew_kz,0,2);
    ins_quest ((char *) &a_best.fil,1,0);
    ins_quest ((char *) a_best.letzt_lief_nr,0,17);
    ins_quest ((char *) a_best.lief,0,17);
    ins_quest ((char *) a_best.lief_best,0,17);
    ins_quest ((char *) &a_best.lief_s,2,0);
    ins_quest ((char *) &a_best.mdn,1,0);
    ins_quest ((char *) &a_best.me_einh_ek,1,0);
    ins_quest ((char *) a_best.umk_kz,0,2);
            ins_cursor = prepare_sql ("insert into a_best (a,  "
"best_anz,  best_auto,  bsd_min,  bsd_min_emp,  bsd_max_emp,  delstatus,  "
"erf_gew_kz,  fil,  letzt_lief_nr,  lief,  lief_best,  lief_s,  mdn,  me_einh_ek,  "
"umk_kz) "

#line 51 "a_best.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?)"); 

#line 53 "a_best.rpp"
}
int A_BEST_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

