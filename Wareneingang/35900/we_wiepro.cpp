#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "a_kun.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "we_wiepro.h"

struct WE_WIEPRO we_wiepro, we_wiepro_null;

void WE_WIEPRO_CLASS::prepare (void)
{
//            char *sqltext;

            ins_quest ((char *) &we_wiepro.mdn, 1, 0);
            ins_quest ((char *) &we_wiepro.fil, 1, 0);
    out_quest ((char *) &we_wiepro.mdn,1,0);
    out_quest ((char *) &we_wiepro.fil,1,0);
    out_quest ((char *) &we_wiepro.a,3,0);
    out_quest ((char *) &we_wiepro.best_blg,2,0);
    out_quest ((char *) we_wiepro.blg_typ,0,2);
    out_quest ((char *) we_wiepro.lief_rech_nr,0,17);
    out_quest ((char *) &we_wiepro.lief_s,2,0);
    out_quest ((char *) &we_wiepro.me,3,0);
    out_quest ((char *) &we_wiepro.tara,3,0);
    out_quest ((char *) we_wiepro.zeit,0,7);
    out_quest ((char *) &we_wiepro.dat,2,0);
    out_quest ((char *) &we_wiepro.sys,2,0);
    out_quest ((char *) we_wiepro.pers,0,17);
    out_quest ((char *) we_wiepro.erf_kz,0,2);
    out_quest ((char *) &we_wiepro.anzahl,2,0);
    out_quest ((char *) we_wiepro.qua_kz,0,2);
    out_quest ((char *) we_wiepro.lief,0,17);
    out_quest ((char *) we_wiepro.ls_charge,0,21);
    out_quest ((char *) we_wiepro.ls_ident,0,21);
    out_quest ((char *) &we_wiepro.es_nr,2,0);
            cursor = prepare_sql ("select we_wiepro.mdn,  "
"we_wiepro.fil,  we_wiepro.a,  we_wiepro.best_blg,  we_wiepro.blg_typ,  "
"we_wiepro.lief_rech_nr,  we_wiepro.lief_s,  we_wiepro.me,  "
"we_wiepro.tara,  we_wiepro.zeit,  we_wiepro.dat,  we_wiepro.sys,  "
"we_wiepro.pers,  we_wiepro.erf_kz,  we_wiepro.anzahl,  "
"we_wiepro.qua_kz,  we_wiepro.lief,  we_wiepro.ls_charge,  "
"we_wiepro.ls_ident,  we_wiepro.es_nr from we_wiepro "

#line 27 "we_wiepro.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? ");

    ins_quest ((char *) &we_wiepro.mdn,1,0);
    ins_quest ((char *) &we_wiepro.fil,1,0);
    ins_quest ((char *) &we_wiepro.a,3,0);
    ins_quest ((char *) &we_wiepro.best_blg,2,0);
    ins_quest ((char *) we_wiepro.blg_typ,0,2);
    ins_quest ((char *) we_wiepro.lief_rech_nr,0,17);
    ins_quest ((char *) &we_wiepro.lief_s,2,0);
    ins_quest ((char *) &we_wiepro.me,3,0);
    ins_quest ((char *) &we_wiepro.tara,3,0);
    ins_quest ((char *) we_wiepro.zeit,0,7);
    ins_quest ((char *) &we_wiepro.dat,2,0);
    ins_quest ((char *) &we_wiepro.sys,2,0);
    ins_quest ((char *) we_wiepro.pers,0,17);
    ins_quest ((char *) we_wiepro.erf_kz,0,2);
    ins_quest ((char *) &we_wiepro.anzahl,2,0);
    ins_quest ((char *) we_wiepro.qua_kz,0,2);
    ins_quest ((char *) we_wiepro.lief,0,17);
    ins_quest ((char *) we_wiepro.ls_charge,0,21);
    ins_quest ((char *) we_wiepro.ls_ident,0,21);
    ins_quest ((char *) &we_wiepro.es_nr,2,0);
            ins_cursor = prepare_sql ("insert into we_wiepro ("
"mdn,  fil,  a,  best_blg,  blg_typ,  lief_rech_nr,  lief_s,  me,  tara,  zeit,  dat,  sys,  pers,  "
"erf_kz,  anzahl,  qua_kz,  lief,  ls_charge,  ls_ident,  es_nr) "

#line 31 "we_wiepro.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?)"); 

#line 33 "we_wiepro.rpp"
}
int WE_WIEPRO_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int WE_WIEPRO_CLASS::dbinsert (void)
/**
Insert in we_wiepro
**/
{
         if (ins_cursor == -1)
         {
                     prepare ();
         }
         execute_curs (ins_cursor);
         return sqlstatus;
}
         
int WE_WIEPRO_CLASS::dbupdate (void)
/**
Insert in we_wiepro
**/
{
        return dbinsert ();
}

int WE_WIEPRO_CLASS::dbdelete (void)
/**
Insert in we_wiepro
**/
{
        return 0;
}

int WE_WIEPRO_CLASS::dbclose (void)
/**
Insert in we_wiepro
**/
{
        if (cursor != -1)
        { 
                 close_sql (cursor);
                 cursor = -1;
        }
        if (ins_cursor != -1)
        { 
                 close_sql (ins_cursor);
                 cursor = -1;
        }
        return 0;
}

