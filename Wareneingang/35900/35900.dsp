# Microsoft Developer Studio Project File - Name="35900" - Package Owner=<4>
# Von Microsoft Developer Studio generierte Erstellungsdatei, Format Version 5.00
# ** NICHT BEARBEITEN **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=35900 - Win32 Debug
!MESSAGE Dies ist kein g�ltiges Makefile. Zum Erstellen dieses Projekts mit\
 NMAKE
!MESSAGE verwenden Sie den Befehl "Makefile exportieren" und f�hren Sie den\
 Befehl
!MESSAGE 
!MESSAGE NMAKE /f "35900.mak".
!MESSAGE 
!MESSAGE Sie k�nnen beim Ausf�hren von NMAKE eine Konfiguration angeben
!MESSAGE durch Definieren des Makros CFG in der Befehlszeile. Zum Beispiel:
!MESSAGE 
!MESSAGE NMAKE /f "35900.mak" CFG="35900 - Win32 Debug"
!MESSAGE 
!MESSAGE F�r die Konfiguration stehen zur Auswahl:
!MESSAGE 
!MESSAGE "35900 - Win32 Release" (basierend auf  "Win32 (x86) Application")
!MESSAGE "35900 - Win32 Debug" (basierend auf  "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "35900 - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /W3 /GX /O2 /I "d:\informix\incl\esql" /I "d:\inf" /I "$(INFORMIXDIR)\incl\esql723tc9" /I "$(INFORMIXDIR)\incl\esql" /I "..\include" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o NUL /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o NUL /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib comctl32.lib isqlt07c.lib winmm.lib /nologo /subsystem:windows /machine:I386 /out:"d:\user\fit\bin\35900.exe" /libpath:"d:\informix\lib"

!ELSEIF  "$(CFG)" == "35900 - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /W3 /Gm /GX /Zi /Od /I "d:\inf" /I "$(INFORMIXDIR)\incl\esql723tc9" /I "$(INFORMIXDIR)\incl\esql" /I "..\include" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /FR /YX /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o NUL /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o NUL /win32
# ADD BASE RSC /l 0x407 /d "_DEBUG"
# ADD RSC /l 0x407 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib comctl32.lib isqlt07c.lib winmm.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept /libpath:"$(INFORMIXDIR)\lib"

!ENDIF 

# Begin Target

# Name "35900 - Win32 Release"
# Name "35900 - Win32 Debug"
# Begin Source File

SOURCE=.\35900.cpp
# End Source File
# Begin Source File

SOURCE=.\35900.rc

!IF  "$(CFG)" == "35900 - Win32 Release"

!ELSEIF  "$(CFG)" == "35900 - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\A_BAS.CPP
# End Source File
# Begin Source File

SOURCE=.\a_best.cpp
# End Source File
# Begin Source File

SOURCE=.\A_HNDW.CPP
# End Source File
# Begin Source File

SOURCE=.\a_kalk_mat.cpp
# End Source File
# Begin Source File

SOURCE=.\a_kalkhndw.cpp
# End Source File
# Begin Source File

SOURCE=.\a_kalkpreis.cpp
# End Source File
# Begin Source File

SOURCE=.\a_kalkpreis.h
# End Source File
# Begin Source File

SOURCE=.\a_lgr.cpp
# End Source File
# Begin Source File

SOURCE=.\a_lgr.h
# End Source File
# Begin Source File

SOURCE=.\A_PR.CPP
# End Source File
# Begin Source File

SOURCE=.\AKT_KRZ.CPP
# End Source File
# Begin Source File

SOURCE=.\AKTION.CPP
# End Source File
# Begin Source File

SOURCE=.\best_kopf.cpp
# End Source File
# Begin Source File

SOURCE=.\best_pos.cpp
# End Source File
# Begin Source File

SOURCE=.\bsd_buch.cpp
# End Source File
# Begin Source File

SOURCE=.\DATUM.C
# End Source File
# Begin Source File

SOURCE=.\dbclass.cpp
# End Source File
# Begin Source File

SOURCE=.\DBFUNC.CPP
# End Source File
# Begin Source File

SOURCE=..\lib\etubidll.lib
# End Source File
# Begin Source File

SOURCE=.\fhs.ico
# End Source File
# Begin Source File

SOURCE=.\FIL.CPP
# End Source File
# Begin Source File

SOURCE=.\fit.ico
# End Source File
# Begin Source File

SOURCE=.\fit0.ico
# End Source File
# Begin Source File

SOURCE=.\fit01.ico
# End Source File
# Begin Source File

SOURCE=.\fit02.ico
# End Source File
# Begin Source File

SOURCE=.\FNB.H
# End Source File
# Begin Source File

SOURCE=.\FNB.LIB
# End Source File
# Begin Source File

SOURCE=.\infmacro.cpp
# End Source File
# Begin Source File

SOURCE=.\infmacro.h
# End Source File
# Begin Source File

SOURCE=.\Integer.cpp
# End Source File
# Begin Source File

SOURCE=.\Integer.h
# End Source File
# Begin Source File

SOURCE=.\kumebest.cpp
# End Source File
# Begin Source File

SOURCE=.\lbek.cpp
# End Source File
# Begin Source File

SOURCE=.\lbek.h
# End Source File
# Begin Source File

SOURCE=.\lgr.cpp
# End Source File
# Begin Source File

SOURCE=.\lief.cpp
# End Source File
# Begin Source File

SOURCE=.\lief_bzg.cpp
# End Source File
# Begin Source File

SOURCE=.\lief_bzgn.cpp
# End Source File
# Begin Source File

SOURCE=.\lief_bzgn.h
# End Source File
# Begin Source File

SOURCE=.\LS.CPP
# End Source File
# Begin Source File

SOURCE=..\lib\macro.lib
# End Source File
# Begin Source File

SOURCE=.\MDN.CPP
# End Source File
# Begin Source File

SOURCE=.\MO_A_PR.CPP
# End Source File
# Begin Source File

SOURCE=.\MO_CURSO.CPP
# End Source File
# Begin Source File

SOURCE=.\MO_DRAW.CPP
# End Source File
# Begin Source File

SOURCE=.\MO_EXPL.CPP
# End Source File
# Begin Source File

SOURCE=.\mo_gdruck.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_geb.cpp
# End Source File
# Begin Source File

SOURCE=.\MO_MELD.CPP
# End Source File
# Begin Source File

SOURCE=.\MO_MENU.CPP
# End Source File
# Begin Source File

SOURCE=.\mo_nr.cpp
# End Source File
# Begin Source File

SOURCE=.\MO_NUMME.CPP
# End Source File
# Begin Source File

SOURCE=.\MO_PASSW.CPP
# End Source File
# Begin Source File

SOURCE=.\mo_progcfg.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_qm.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_wepr.cpp
# End Source File
# Begin Source File

SOURCE=.\NOTE.ICO
# End Source File
# Begin Source File

SOURCE=.\nublock.cpp
# End Source File
# Begin Source File

SOURCE=.\pht_wg.cpp
# End Source File
# Begin Source File

SOURCE=.\Process.cpp
# End Source File
# Begin Source File

SOURCE=.\Process.h
# End Source File
# Begin Source File

SOURCE=.\prp_user.cpp
# End Source File
# Begin Source File

SOURCE=.\PTAB.CPP
# End Source File
# Begin Source File

SOURCE=.\qswekopf.cpp
# End Source File
# Begin Source File

SOURCE=.\qswepos.cpp
# End Source File
# Begin Source File

SOURCE=.\rind.cpp
# End Source File
# Begin Source File

SOURCE=.\scanean.cpp
# End Source File
# Begin Source File

SOURCE=.\scanean.h
# End Source File
# Begin Source File

SOURCE=.\Sounds.cpp
# End Source File
# Begin Source File

SOURCE=.\STDFKT.CPP
# End Source File
# Begin Source File

SOURCE=.\STRFKT.CPP
# End Source File
# Begin Source File

SOURCE=.\sys_par.cpp
# End Source File
# Begin Source File

SOURCE=.\SYS_PERI.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\Text.cpp
# End Source File
# Begin Source File

SOURCE=.\textblock.cpp
# End Source File
# Begin Source File

SOURCE=.\touchf.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\Vector.cpp
# End Source File
# Begin Source File

SOURCE=..\include\Vector.h
# End Source File
# Begin Source File

SOURCE=.\we_jour.cpp
# End Source File
# Begin Source File

SOURCE=.\we_kopf.cpp
# End Source File
# Begin Source File

SOURCE=.\we_pos.cpp
# End Source File
# Begin Source File

SOURCE=.\we_txt.cpp
# End Source File
# Begin Source File

SOURCE=.\we_txt.h
# End Source File
# Begin Source File

SOURCE=.\we_wiepro.cpp
# End Source File
# Begin Source File

SOURCE=.\WMASK.CPP
# End Source File
# Begin Source File

SOURCE=.\zerldaten.cpp
# End Source File
# Begin Source File

SOURCE=.\zerldaten.h
# End Source File
# End Target
# End Project
