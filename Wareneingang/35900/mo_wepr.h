#ifndef _MO_WEPR_DEF
#define _MO_WEPR_DEF

class WE_PREISE
{
         private :
              double rab_proz;
              double proz_sum;
			  int rmdn;
			  int rfil;
         public :
              WE_PREISE ()
              {
              }
              int preis_holen (short, short, char *, double);
              int preis_holen (short, short, char *, char *, double);
              double GetRabEk (short, short, char *, double, double, int,double,int,short);
              double GetRabAEk (short, short, char *, double, int, short, double, double, int, int);
              int fetchrab (int);
};
#endif
