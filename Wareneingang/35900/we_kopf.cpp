#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "a_kun.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "we_kopf.h"

struct WE_KOPF we_kopf, we_kopf_null;

void WE_KOPF_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &we_kopf.mdn, 1, 0);
            ins_quest ((char *) &we_kopf.fil, 1, 0);
            ins_quest ((char *) &we_kopf.lief_rech_nr, 0, 17);
            ins_quest ((char *) &we_kopf.lief, 0, 17);
            ins_quest ((char *) &we_kopf.lfd, 2, 0);
            ins_quest ((char *) &we_kopf.blg_typ, 0, 2);
    out_quest ((char *) we_kopf.a_best_kz,0,2);
    out_quest ((char *) &we_kopf.abw_lief_zusch,3,0);
    out_quest ((char *) &we_kopf.abw_rab_betr,3,0);
    out_quest ((char *) &we_kopf.abw_rab_proz,3,0);
    out_quest ((char *) &we_kopf.abw_zusch_betr,3,0);
    out_quest ((char *) &we_kopf.abw_zusch_proz,3,0);
    out_quest ((char *) &we_kopf.akv,2,0);
    out_quest ((char *) we_kopf.bsd_verb_kz,0,2);
    out_quest ((char *) &we_kopf.anz_alarm,2,0);
    out_quest ((char *) &we_kopf.anz_mang,2,0);
    out_quest ((char *) &we_kopf.anz_pos,2,0);
    out_quest ((char *) &we_kopf.bearb,2,0);
    out_quest ((char *) &we_kopf.best0_term,2,0);
    out_quest ((char *) &we_kopf.best1_term,2,0);
    out_quest ((char *) &we_kopf.best2_term,2,0);
    out_quest ((char *) &we_kopf.best3_term,2,0);
    out_quest ((char *) &we_kopf.best4_term,2,0);
    out_quest ((char *) &we_kopf.best5_term,2,0);
    out_quest ((char *) &we_kopf.best6_term,2,0);
    out_quest ((char *) &we_kopf.best7_term,2,0);
    out_quest ((char *) &we_kopf.best8_term,2,0);
    out_quest ((char *) &we_kopf.best9_term,2,0);
    out_quest ((char *) we_kopf.blg_typ,0,2);
    out_quest ((char *) &we_kopf.fil,1,0);
    out_quest ((char *) we_kopf.koresp_best0,0,17);
    out_quest ((char *) we_kopf.koresp_best1,0,17);
    out_quest ((char *) we_kopf.koresp_best2,0,17);
    out_quest ((char *) we_kopf.koresp_best3,0,17);
    out_quest ((char *) we_kopf.koresp_best4,0,17);
    out_quest ((char *) we_kopf.koresp_best5,0,17);
    out_quest ((char *) we_kopf.koresp_best6,0,17);
    out_quest ((char *) we_kopf.koresp_best7,0,17);
    out_quest ((char *) we_kopf.koresp_best8,0,17);
    out_quest ((char *) we_kopf.koresp_best9,0,17);
    out_quest ((char *) &we_kopf.lfd,2,0);
    out_quest ((char *) we_kopf.lief,0,17);
    out_quest ((char *) &we_kopf.lief0_term,2,0);
    out_quest ((char *) &we_kopf.lief1_term,2,0);
    out_quest ((char *) &we_kopf.lief2_term,2,0);
    out_quest ((char *) &we_kopf.lief3_term,2,0);
    out_quest ((char *) &we_kopf.lief4_term,2,0);
    out_quest ((char *) &we_kopf.lief5_term,2,0);
    out_quest ((char *) &we_kopf.lief6_term,2,0);
    out_quest ((char *) &we_kopf.lief7_term,2,0);
    out_quest ((char *) &we_kopf.lief8_term,2,0);
    out_quest ((char *) &we_kopf.lief9_term,2,0);
    out_quest ((char *) we_kopf.lief_rech_nr,0,17);
    out_quest ((char *) &we_kopf.lief_s,2,0);
    out_quest ((char *) &we_kopf.lief_wrt_ab,3,0);
    out_quest ((char *) &we_kopf.lief_wrt_nto,3,0);
    out_quest ((char *) &we_kopf.lief_wrt_o,3,0);
    out_quest ((char *) &we_kopf.lief_zusch,3,0);
    out_quest ((char *) &we_kopf.mdn,1,0);
    out_quest ((char *) &we_kopf.mwst_ges,3,0);
    out_quest ((char *) we_kopf.pers_nam,0,9);
    out_quest ((char *) we_kopf.pr_erf_kz,0,2);
    out_quest ((char *) &we_kopf.rab_eff,3,0);
    out_quest ((char *) &we_kopf.rab_faeh_betr,3,0);
    out_quest ((char *) &we_kopf.skto,3,0);
    out_quest ((char *) &we_kopf.skto_faeh_betr,3,0);
    out_quest ((char *) we_kopf.skto_kz,0,2);
    out_quest ((char *) &we_kopf.skto_proz,3,0);
    out_quest ((char *) &we_kopf.txt_nr,2,0);
    out_quest ((char *) &we_kopf.we_dat,2,0);
    out_quest ((char *) &we_kopf.we_status,1,0);
    out_quest ((char *) &we_kopf.zusch,3,0);
    out_quest ((char *) &we_kopf.fil_adr,2,0);
    out_quest ((char *) &we_kopf.lief_adr,2,0);
    out_quest ((char *) &we_kopf.blg_eing_dat,2,0);
    out_quest ((char *) we_kopf.lief_ink,0,17);
    out_quest ((char *) &we_kopf.lief_ink_s,2,0);
    out_quest ((char *) &we_kopf.ink_proz,3,0);
    out_quest ((char *) &we_kopf.ink_zusch,3,0);
    out_quest ((char *) &we_kopf.vk_wrt,3,0);
    out_quest ((char *) &we_kopf.buch_kz,1,0);
    out_quest ((char *) &we_kopf.rab_eff_lief,3,0);
    out_quest ((char *) &we_kopf.rab_eff_a,3,0);
    out_quest ((char *) &we_kopf.fil_ek_wrt,3,0);
            cursor = prepare_sql ("select we_kopf.a_best_kz,  "
"we_kopf.abw_lief_zusch,  we_kopf.abw_rab_betr,  "
"we_kopf.abw_rab_proz,  we_kopf.abw_zusch_betr,  "
"we_kopf.abw_zusch_proz,  we_kopf.akv,  we_kopf.bsd_verb_kz,  "
"we_kopf.anz_alarm,  we_kopf.anz_mang,  we_kopf.anz_pos,  "
"we_kopf.bearb,  we_kopf.best0_term,  we_kopf.best1_term,  "
"we_kopf.best2_term,  we_kopf.best3_term,  we_kopf.best4_term,  "
"we_kopf.best5_term,  we_kopf.best6_term,  we_kopf.best7_term,  "
"we_kopf.best8_term,  we_kopf.best9_term,  we_kopf.blg_typ,  "
"we_kopf.fil,  we_kopf.koresp_best0,  we_kopf.koresp_best1,  "
"we_kopf.koresp_best2,  we_kopf.koresp_best3,  we_kopf.koresp_best4,  "
"we_kopf.koresp_best5,  we_kopf.koresp_best6,  we_kopf.koresp_best7,  "
"we_kopf.koresp_best8,  we_kopf.koresp_best9,  we_kopf.lfd,  "
"we_kopf.lief,  we_kopf.lief0_term,  we_kopf.lief1_term,  "
"we_kopf.lief2_term,  we_kopf.lief3_term,  we_kopf.lief4_term,  "
"we_kopf.lief5_term,  we_kopf.lief6_term,  we_kopf.lief7_term,  "
"we_kopf.lief8_term,  we_kopf.lief9_term,  we_kopf.lief_rech_nr,  "
"we_kopf.lief_s,  we_kopf.lief_wrt_ab,  we_kopf.lief_wrt_nto,  "
"we_kopf.lief_wrt_o,  we_kopf.lief_zusch,  we_kopf.mdn,  "
"we_kopf.mwst_ges,  we_kopf.pers_nam,  we_kopf.pr_erf_kz,  "
"we_kopf.rab_eff,  we_kopf.rab_faeh_betr,  we_kopf.skto,  "
"we_kopf.skto_faeh_betr,  we_kopf.skto_kz,  we_kopf.skto_proz,  "
"we_kopf.txt_nr,  we_kopf.we_dat,  we_kopf.we_status,  we_kopf.zusch,  "
"we_kopf.fil_adr,  we_kopf.lief_adr,  we_kopf.blg_eing_dat,  "
"we_kopf.lief_ink,  we_kopf.lief_ink_s,  we_kopf.ink_proz,  "
"we_kopf.ink_zusch,  we_kopf.vk_wrt,  we_kopf.buch_kz,  "
"we_kopf.rab_eff_lief,  we_kopf.rab_eff_a,  we_kopf.fil_ek_wrt from we_kopf "

#line 31 "we_kopf.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   lief_rech_nr = ? "
                                  "and   lief = ? "
                                  "and   lfd = ? "
                                  "and   blg_typ = ?");

    ins_quest ((char *) we_kopf.a_best_kz,0,2);
    ins_quest ((char *) &we_kopf.abw_lief_zusch,3,0);
    ins_quest ((char *) &we_kopf.abw_rab_betr,3,0);
    ins_quest ((char *) &we_kopf.abw_rab_proz,3,0);
    ins_quest ((char *) &we_kopf.abw_zusch_betr,3,0);
    ins_quest ((char *) &we_kopf.abw_zusch_proz,3,0);
    ins_quest ((char *) &we_kopf.akv,2,0);
    ins_quest ((char *) we_kopf.bsd_verb_kz,0,2);
    ins_quest ((char *) &we_kopf.anz_alarm,2,0);
    ins_quest ((char *) &we_kopf.anz_mang,2,0);
    ins_quest ((char *) &we_kopf.anz_pos,2,0);
    ins_quest ((char *) &we_kopf.bearb,2,0);
    ins_quest ((char *) &we_kopf.best0_term,2,0);
    ins_quest ((char *) &we_kopf.best1_term,2,0);
    ins_quest ((char *) &we_kopf.best2_term,2,0);
    ins_quest ((char *) &we_kopf.best3_term,2,0);
    ins_quest ((char *) &we_kopf.best4_term,2,0);
    ins_quest ((char *) &we_kopf.best5_term,2,0);
    ins_quest ((char *) &we_kopf.best6_term,2,0);
    ins_quest ((char *) &we_kopf.best7_term,2,0);
    ins_quest ((char *) &we_kopf.best8_term,2,0);
    ins_quest ((char *) &we_kopf.best9_term,2,0);
    ins_quest ((char *) we_kopf.blg_typ,0,2);
    ins_quest ((char *) &we_kopf.fil,1,0);
    ins_quest ((char *) we_kopf.koresp_best0,0,17);
    ins_quest ((char *) we_kopf.koresp_best1,0,17);
    ins_quest ((char *) we_kopf.koresp_best2,0,17);
    ins_quest ((char *) we_kopf.koresp_best3,0,17);
    ins_quest ((char *) we_kopf.koresp_best4,0,17);
    ins_quest ((char *) we_kopf.koresp_best5,0,17);
    ins_quest ((char *) we_kopf.koresp_best6,0,17);
    ins_quest ((char *) we_kopf.koresp_best7,0,17);
    ins_quest ((char *) we_kopf.koresp_best8,0,17);
    ins_quest ((char *) we_kopf.koresp_best9,0,17);
    ins_quest ((char *) &we_kopf.lfd,2,0);
    ins_quest ((char *) we_kopf.lief,0,17);
    ins_quest ((char *) &we_kopf.lief0_term,2,0);
    ins_quest ((char *) &we_kopf.lief1_term,2,0);
    ins_quest ((char *) &we_kopf.lief2_term,2,0);
    ins_quest ((char *) &we_kopf.lief3_term,2,0);
    ins_quest ((char *) &we_kopf.lief4_term,2,0);
    ins_quest ((char *) &we_kopf.lief5_term,2,0);
    ins_quest ((char *) &we_kopf.lief6_term,2,0);
    ins_quest ((char *) &we_kopf.lief7_term,2,0);
    ins_quest ((char *) &we_kopf.lief8_term,2,0);
    ins_quest ((char *) &we_kopf.lief9_term,2,0);
    ins_quest ((char *) we_kopf.lief_rech_nr,0,17);
    ins_quest ((char *) &we_kopf.lief_s,2,0);
    ins_quest ((char *) &we_kopf.lief_wrt_ab,3,0);
    ins_quest ((char *) &we_kopf.lief_wrt_nto,3,0);
    ins_quest ((char *) &we_kopf.lief_wrt_o,3,0);
    ins_quest ((char *) &we_kopf.lief_zusch,3,0);
    ins_quest ((char *) &we_kopf.mdn,1,0);
    ins_quest ((char *) &we_kopf.mwst_ges,3,0);
    ins_quest ((char *) we_kopf.pers_nam,0,9);
    ins_quest ((char *) we_kopf.pr_erf_kz,0,2);
    ins_quest ((char *) &we_kopf.rab_eff,3,0);
    ins_quest ((char *) &we_kopf.rab_faeh_betr,3,0);
    ins_quest ((char *) &we_kopf.skto,3,0);
    ins_quest ((char *) &we_kopf.skto_faeh_betr,3,0);
    ins_quest ((char *) we_kopf.skto_kz,0,2);
    ins_quest ((char *) &we_kopf.skto_proz,3,0);
    ins_quest ((char *) &we_kopf.txt_nr,2,0);
    ins_quest ((char *) &we_kopf.we_dat,2,0);
    ins_quest ((char *) &we_kopf.we_status,1,0);
    ins_quest ((char *) &we_kopf.zusch,3,0);
    ins_quest ((char *) &we_kopf.fil_adr,2,0);
    ins_quest ((char *) &we_kopf.lief_adr,2,0);
    ins_quest ((char *) &we_kopf.blg_eing_dat,2,0);
    ins_quest ((char *) we_kopf.lief_ink,0,17);
    ins_quest ((char *) &we_kopf.lief_ink_s,2,0);
    ins_quest ((char *) &we_kopf.ink_proz,3,0);
    ins_quest ((char *) &we_kopf.ink_zusch,3,0);
    ins_quest ((char *) &we_kopf.vk_wrt,3,0);
    ins_quest ((char *) &we_kopf.buch_kz,1,0);
    ins_quest ((char *) &we_kopf.rab_eff_lief,3,0);
    ins_quest ((char *) &we_kopf.rab_eff_a,3,0);
    ins_quest ((char *) &we_kopf.fil_ek_wrt,3,0);
            sqltext = "update we_kopf set "
"we_kopf.a_best_kz = ?,  we_kopf.abw_lief_zusch = ?,  "
"we_kopf.abw_rab_betr = ?,  we_kopf.abw_rab_proz = ?,  "
"we_kopf.abw_zusch_betr = ?,  we_kopf.abw_zusch_proz = ?,  "
"we_kopf.akv = ?,  we_kopf.bsd_verb_kz = ?,  we_kopf.anz_alarm = ?,  "
"we_kopf.anz_mang = ?,  we_kopf.anz_pos = ?,  we_kopf.bearb = ?,  "
"we_kopf.best0_term = ?,  we_kopf.best1_term = ?,  "
"we_kopf.best2_term = ?,  we_kopf.best3_term = ?,  "
"we_kopf.best4_term = ?,  we_kopf.best5_term = ?,  "
"we_kopf.best6_term = ?,  we_kopf.best7_term = ?,  "
"we_kopf.best8_term = ?,  we_kopf.best9_term = ?,  "
"we_kopf.blg_typ = ?,  we_kopf.fil = ?,  we_kopf.koresp_best0 = ?,  "
"we_kopf.koresp_best1 = ?,  we_kopf.koresp_best2 = ?,  "
"we_kopf.koresp_best3 = ?,  we_kopf.koresp_best4 = ?,  "
"we_kopf.koresp_best5 = ?,  we_kopf.koresp_best6 = ?,  "
"we_kopf.koresp_best7 = ?,  we_kopf.koresp_best8 = ?,  "
"we_kopf.koresp_best9 = ?,  we_kopf.lfd = ?,  we_kopf.lief = ?,  "
"we_kopf.lief0_term = ?,  we_kopf.lief1_term = ?,  "
"we_kopf.lief2_term = ?,  we_kopf.lief3_term = ?,  "
"we_kopf.lief4_term = ?,  we_kopf.lief5_term = ?,  "
"we_kopf.lief6_term = ?,  we_kopf.lief7_term = ?,  "
"we_kopf.lief8_term = ?,  we_kopf.lief9_term = ?,  "
"we_kopf.lief_rech_nr = ?,  we_kopf.lief_s = ?,  "
"we_kopf.lief_wrt_ab = ?,  we_kopf.lief_wrt_nto = ?,  "
"we_kopf.lief_wrt_o = ?,  we_kopf.lief_zusch = ?,  we_kopf.mdn = ?,  "
"we_kopf.mwst_ges = ?,  we_kopf.pers_nam = ?,  "
"we_kopf.pr_erf_kz = ?,  we_kopf.rab_eff = ?,  "
"we_kopf.rab_faeh_betr = ?,  we_kopf.skto = ?,  "
"we_kopf.skto_faeh_betr = ?,  we_kopf.skto_kz = ?,  "
"we_kopf.skto_proz = ?,  we_kopf.txt_nr = ?,  we_kopf.we_dat = ?,  "
"we_kopf.we_status = ?,  we_kopf.zusch = ?,  we_kopf.fil_adr = ?,  "
"we_kopf.lief_adr = ?,  we_kopf.blg_eing_dat = ?,  "
"we_kopf.lief_ink = ?,  we_kopf.lief_ink_s = ?,  "
"we_kopf.ink_proz = ?,  we_kopf.ink_zusch = ?,  we_kopf.vk_wrt = ?,  "
"we_kopf.buch_kz = ?,  we_kopf.rab_eff_lief = ?,  "
"we_kopf.rab_eff_a = ?,  we_kopf.fil_ek_wrt = ? "

#line 39 "we_kopf.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   lief_rech_nr = ? "
                                  "and   lief = ? "
                                  "and   lfd = ? "
                                  "and   blg_typ = ?";

            ins_quest ((char *) &we_kopf.mdn, 1, 0);
            ins_quest ((char *) &we_kopf.fil, 1, 0);
            ins_quest ((char *) &we_kopf.lief_rech_nr, 0, 17);
            ins_quest ((char *) &we_kopf.lief, 0, 17);
            ins_quest ((char *) &we_kopf.lfd, 2, 0);
            ins_quest ((char *) &we_kopf.blg_typ, 0, 2);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &we_kopf.mdn, 1, 0);
            ins_quest ((char *) &we_kopf.fil, 1, 0);
            ins_quest ((char *) &we_kopf.lief_rech_nr, 0, 17);
            ins_quest ((char *) &we_kopf.lief, 0, 17);
            ins_quest ((char *) &we_kopf.lfd, 2, 0);
            ins_quest ((char *) &we_kopf.blg_typ, 0, 2);
            test_upd_cursor = prepare_sql ("select lief_rech_nr from we_kopf "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   lief_rech_nr = ? "
                                  "and   lief = ? "
                                  "and   lfd = ? "
                                  "and   blg_typ = ?");
            ins_quest ((char *) &we_kopf.mdn, 1, 0);
            ins_quest ((char *) &we_kopf.fil, 1, 0);
            ins_quest ((char *) &we_kopf.lief_rech_nr, 0, 17);
            ins_quest ((char *) &we_kopf.lief, 0, 17);
            ins_quest ((char *) &we_kopf.lfd, 2, 0);
            ins_quest ((char *) &we_kopf.blg_typ, 0, 2);
            del_cursor = prepare_sql ("delete from we_kopf "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   lief_rech_nr = ? "
                                  "and   lief = ? "
                                  "and   lfd = ? "
                                  "and   blg_typ = ?");
    ins_quest ((char *) we_kopf.a_best_kz,0,2);
    ins_quest ((char *) &we_kopf.abw_lief_zusch,3,0);
    ins_quest ((char *) &we_kopf.abw_rab_betr,3,0);
    ins_quest ((char *) &we_kopf.abw_rab_proz,3,0);
    ins_quest ((char *) &we_kopf.abw_zusch_betr,3,0);
    ins_quest ((char *) &we_kopf.abw_zusch_proz,3,0);
    ins_quest ((char *) &we_kopf.akv,2,0);
    ins_quest ((char *) we_kopf.bsd_verb_kz,0,2);
    ins_quest ((char *) &we_kopf.anz_alarm,2,0);
    ins_quest ((char *) &we_kopf.anz_mang,2,0);
    ins_quest ((char *) &we_kopf.anz_pos,2,0);
    ins_quest ((char *) &we_kopf.bearb,2,0);
    ins_quest ((char *) &we_kopf.best0_term,2,0);
    ins_quest ((char *) &we_kopf.best1_term,2,0);
    ins_quest ((char *) &we_kopf.best2_term,2,0);
    ins_quest ((char *) &we_kopf.best3_term,2,0);
    ins_quest ((char *) &we_kopf.best4_term,2,0);
    ins_quest ((char *) &we_kopf.best5_term,2,0);
    ins_quest ((char *) &we_kopf.best6_term,2,0);
    ins_quest ((char *) &we_kopf.best7_term,2,0);
    ins_quest ((char *) &we_kopf.best8_term,2,0);
    ins_quest ((char *) &we_kopf.best9_term,2,0);
    ins_quest ((char *) we_kopf.blg_typ,0,2);
    ins_quest ((char *) &we_kopf.fil,1,0);
    ins_quest ((char *) we_kopf.koresp_best0,0,17);
    ins_quest ((char *) we_kopf.koresp_best1,0,17);
    ins_quest ((char *) we_kopf.koresp_best2,0,17);
    ins_quest ((char *) we_kopf.koresp_best3,0,17);
    ins_quest ((char *) we_kopf.koresp_best4,0,17);
    ins_quest ((char *) we_kopf.koresp_best5,0,17);
    ins_quest ((char *) we_kopf.koresp_best6,0,17);
    ins_quest ((char *) we_kopf.koresp_best7,0,17);
    ins_quest ((char *) we_kopf.koresp_best8,0,17);
    ins_quest ((char *) we_kopf.koresp_best9,0,17);
    ins_quest ((char *) &we_kopf.lfd,2,0);
    ins_quest ((char *) we_kopf.lief,0,17);
    ins_quest ((char *) &we_kopf.lief0_term,2,0);
    ins_quest ((char *) &we_kopf.lief1_term,2,0);
    ins_quest ((char *) &we_kopf.lief2_term,2,0);
    ins_quest ((char *) &we_kopf.lief3_term,2,0);
    ins_quest ((char *) &we_kopf.lief4_term,2,0);
    ins_quest ((char *) &we_kopf.lief5_term,2,0);
    ins_quest ((char *) &we_kopf.lief6_term,2,0);
    ins_quest ((char *) &we_kopf.lief7_term,2,0);
    ins_quest ((char *) &we_kopf.lief8_term,2,0);
    ins_quest ((char *) &we_kopf.lief9_term,2,0);
    ins_quest ((char *) we_kopf.lief_rech_nr,0,17);
    ins_quest ((char *) &we_kopf.lief_s,2,0);
    ins_quest ((char *) &we_kopf.lief_wrt_ab,3,0);
    ins_quest ((char *) &we_kopf.lief_wrt_nto,3,0);
    ins_quest ((char *) &we_kopf.lief_wrt_o,3,0);
    ins_quest ((char *) &we_kopf.lief_zusch,3,0);
    ins_quest ((char *) &we_kopf.mdn,1,0);
    ins_quest ((char *) &we_kopf.mwst_ges,3,0);
    ins_quest ((char *) we_kopf.pers_nam,0,9);
    ins_quest ((char *) we_kopf.pr_erf_kz,0,2);
    ins_quest ((char *) &we_kopf.rab_eff,3,0);
    ins_quest ((char *) &we_kopf.rab_faeh_betr,3,0);
    ins_quest ((char *) &we_kopf.skto,3,0);
    ins_quest ((char *) &we_kopf.skto_faeh_betr,3,0);
    ins_quest ((char *) we_kopf.skto_kz,0,2);
    ins_quest ((char *) &we_kopf.skto_proz,3,0);
    ins_quest ((char *) &we_kopf.txt_nr,2,0);
    ins_quest ((char *) &we_kopf.we_dat,2,0);
    ins_quest ((char *) &we_kopf.we_status,1,0);
    ins_quest ((char *) &we_kopf.zusch,3,0);
    ins_quest ((char *) &we_kopf.fil_adr,2,0);
    ins_quest ((char *) &we_kopf.lief_adr,2,0);
    ins_quest ((char *) &we_kopf.blg_eing_dat,2,0);
    ins_quest ((char *) we_kopf.lief_ink,0,17);
    ins_quest ((char *) &we_kopf.lief_ink_s,2,0);
    ins_quest ((char *) &we_kopf.ink_proz,3,0);
    ins_quest ((char *) &we_kopf.ink_zusch,3,0);
    ins_quest ((char *) &we_kopf.vk_wrt,3,0);
    ins_quest ((char *) &we_kopf.buch_kz,1,0);
    ins_quest ((char *) &we_kopf.rab_eff_lief,3,0);
    ins_quest ((char *) &we_kopf.rab_eff_a,3,0);
    ins_quest ((char *) &we_kopf.fil_ek_wrt,3,0);
            ins_cursor = prepare_sql ("insert into we_kopf ("
"a_best_kz,  abw_lief_zusch,  abw_rab_betr,  abw_rab_proz,  "
"abw_zusch_betr,  abw_zusch_proz,  akv,  bsd_verb_kz,  anz_alarm,  anz_mang,  "
"anz_pos,  bearb,  best0_term,  best1_term,  best2_term,  best3_term,  "
"best4_term,  best5_term,  best6_term,  best7_term,  best8_term,  best9_term,  "
"blg_typ,  fil,  koresp_best0,  koresp_best1,  koresp_best2,  koresp_best3,  "
"koresp_best4,  koresp_best5,  koresp_best6,  koresp_best7,  koresp_best8,  "
"koresp_best9,  lfd,  lief,  lief0_term,  lief1_term,  lief2_term,  lief3_term,  "
"lief4_term,  lief5_term,  lief6_term,  lief7_term,  lief8_term,  lief9_term,  "
"lief_rech_nr,  lief_s,  lief_wrt_ab,  lief_wrt_nto,  lief_wrt_o,  "
"lief_zusch,  mdn,  mwst_ges,  pers_nam,  pr_erf_kz,  rab_eff,  rab_faeh_betr,  "
"skto,  skto_faeh_betr,  skto_kz,  skto_proz,  txt_nr,  we_dat,  we_status,  zusch,  "
"fil_adr,  lief_adr,  blg_eing_dat,  lief_ink,  lief_ink_s,  ink_proz,  "
"ink_zusch,  vk_wrt,  buch_kz,  rab_eff_lief,  rab_eff_a,  fil_ek_wrt) "

#line 81 "we_kopf.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?)"); 

#line 83 "we_kopf.rpp"
}
int WE_KOPF_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

