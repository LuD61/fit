
struct AUTO_NR {
   short     mdn;
   short     fil;
   char      nr_nam[11];
   long      nr_nr;
   short     satz_kng;
   long      max_wert;
   long      min_wert;  /* GK 26.06.2013 */
   char      nr_char[11];
   long      nr_char_lng;
   char      fest_teil[11];
   char      nr_komb[21];
   short     delstatus;
};

extern struct AUTO_NR auto_nr, auto_nr_null;

int nvreset (short, short, char *);
int nvholid (short, short, char *);
int nveinid (short, short, char *, long);
int nvanmprf (short, short, char *,long, long, long, char *);
int freinummer ();
int nummernspeicher ();
int nvdata_gen ();

