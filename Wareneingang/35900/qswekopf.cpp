#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "wmask.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "a_kun.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "qswekopf.h"

struct QSWEKOPF qswekopf, qswekopf_null;

void QSWEKOPF_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &qswekopf.mdn, 1, 0);
            ins_quest ((char *) &qswekopf.fil, 1, 0);
            ins_quest ((char *) qswekopf.lief, 0, 17);
            ins_quest ((char *) qswekopf.lief_rech_nr, 0, 17);
            ins_quest ((char *) &qswekopf.lfd, 2, 0);
    out_quest ((char *) &qswekopf.mdn,1,0);
    out_quest ((char *) &qswekopf.fil,1,0);
    out_quest ((char *) &qswekopf.lfd,2,0);
    out_quest ((char *) qswekopf.lief_rech_nr,0,17);
    out_quest ((char *) qswekopf.lief,0,17);
    out_quest ((char *) qswekopf.txt,0,61);
    out_quest ((char *) &qswekopf.wrt,1,0);
    out_quest ((char *) &qswekopf.dat,2,0);
    out_quest ((char *) qswekopf.pers_nam,0,9);
    out_quest ((char *) qswekopf.txt2,0,61);
    out_quest ((char *) qswekopf.txt3,0,61);
    out_quest ((char *) qswekopf.txt4,0,61);
    out_quest ((char *) qswekopf.txt5,0,61);
    out_quest ((char *) qswekopf.txt6,0,61);
    out_quest ((char *) qswekopf.txt7,0,61);
    out_quest ((char *) qswekopf.txt8,0,61);
    out_quest ((char *) qswekopf.txt9,0,61);
    out_quest ((char *) qswekopf.txt10,0,61);
    out_quest ((char *) &qswekopf.wrt2,1,0);
    out_quest ((char *) &qswekopf.wrt3,1,0);
    out_quest ((char *) &qswekopf.wrt4,1,0);
    out_quest ((char *) &qswekopf.wrt5,1,0);
    out_quest ((char *) &qswekopf.wrt6,1,0);
    out_quest ((char *) &qswekopf.wrt7,1,0);
    out_quest ((char *) &qswekopf.wrt8,1,0);
    out_quest ((char *) &qswekopf.wrt9,1,0);
    out_quest ((char *) &qswekopf.wrt10,1,0);
            cursor = prepare_sql ("select qswekopf.mdn,  "
"qswekopf.fil,  qswekopf.lfd,  qswekopf.lief_rech_nr,  qswekopf.lief,  "
"qswekopf.txt,  qswekopf.wrt,  qswekopf.dat,  qswekopf.pers_nam,  "
"qswekopf.txt2,  qswekopf.txt3,  qswekopf.txt4,  qswekopf.txt5,  "
"qswekopf.txt6,  qswekopf.txt7,  qswekopf.txt8,  qswekopf.txt9,  "
"qswekopf.txt10,  qswekopf.wrt2,  qswekopf.wrt3,  qswekopf.wrt4,  "
"qswekopf.wrt5,  qswekopf.wrt6,  qswekopf.wrt7,  qswekopf.wrt8,  "
"qswekopf.wrt9,  qswekopf.wrt10 from qswekopf "

#line 30 "qswekopf.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   lief = ? "
                                  "and   lief_rech_nr = ? "
                                  "and lfd = ?");
    ins_quest ((char *) &qswekopf.mdn,1,0);
    ins_quest ((char *) &qswekopf.fil,1,0);
    ins_quest ((char *) &qswekopf.lfd,2,0);
    ins_quest ((char *) qswekopf.lief_rech_nr,0,17);
    ins_quest ((char *) qswekopf.lief,0,17);
    ins_quest ((char *) qswekopf.txt,0,61);
    ins_quest ((char *) &qswekopf.wrt,1,0);
    ins_quest ((char *) &qswekopf.dat,2,0);
    ins_quest ((char *) qswekopf.pers_nam,0,9);
    ins_quest ((char *) qswekopf.txt2,0,61);
    ins_quest ((char *) qswekopf.txt3,0,61);
    ins_quest ((char *) qswekopf.txt4,0,61);
    ins_quest ((char *) qswekopf.txt5,0,61);
    ins_quest ((char *) qswekopf.txt6,0,61);
    ins_quest ((char *) qswekopf.txt7,0,61);
    ins_quest ((char *) qswekopf.txt8,0,61);
    ins_quest ((char *) qswekopf.txt9,0,61);
    ins_quest ((char *) qswekopf.txt10,0,61);
    ins_quest ((char *) &qswekopf.wrt2,1,0);
    ins_quest ((char *) &qswekopf.wrt3,1,0);
    ins_quest ((char *) &qswekopf.wrt4,1,0);
    ins_quest ((char *) &qswekopf.wrt5,1,0);
    ins_quest ((char *) &qswekopf.wrt6,1,0);
    ins_quest ((char *) &qswekopf.wrt7,1,0);
    ins_quest ((char *) &qswekopf.wrt8,1,0);
    ins_quest ((char *) &qswekopf.wrt9,1,0);
    ins_quest ((char *) &qswekopf.wrt10,1,0);
            sqltext = "update qswekopf set qswekopf.mdn = ?,  "
"qswekopf.fil = ?,  qswekopf.lfd = ?,  qswekopf.lief_rech_nr = ?,  "
"qswekopf.lief = ?,  qswekopf.txt = ?,  qswekopf.wrt = ?,  "
"qswekopf.dat = ?,  qswekopf.pers_nam = ?,  qswekopf.txt2 = ?,  "
"qswekopf.txt3 = ?,  qswekopf.txt4 = ?,  qswekopf.txt5 = ?,  "
"qswekopf.txt6 = ?,  qswekopf.txt7 = ?,  qswekopf.txt8 = ?,  "
"qswekopf.txt9 = ?,  qswekopf.txt10 = ?,  qswekopf.wrt2 = ?,  "
"qswekopf.wrt3 = ?,  qswekopf.wrt4 = ?,  qswekopf.wrt5 = ?,  "
"qswekopf.wrt6 = ?,  qswekopf.wrt7 = ?,  qswekopf.wrt8 = ?,  "
"qswekopf.wrt9 = ?,  qswekopf.wrt10 = ? "

#line 36 "qswekopf.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   lief = ? "
                                  "and   lief_rech_nr = ? "
                                  "and lfd = ?";

            ins_quest ((char *) &qswekopf.mdn, 1, 0);
            ins_quest ((char *) &qswekopf.fil, 1, 0);
            ins_quest ((char *) qswekopf.lief, 0, 17);
            ins_quest ((char *) qswekopf.lief_rech_nr, 0, 17);
            ins_quest ((char *) &qswekopf.lfd, 2, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &qswekopf.mdn, 1, 0);
            ins_quest ((char *) &qswekopf.fil, 1, 0);
            ins_quest ((char *) qswekopf.lief, 0, 17);
            ins_quest ((char *) qswekopf.lief_rech_nr, 0, 17);
            ins_quest ((char *) &qswekopf.lfd, 2, 0);
            test_upd_cursor = prepare_sql ("select lief from qswekopf "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   lief = ? "
                                  "and   lief_rech_nr = ? "
                                  "and lfd = ?");
            ins_quest ((char *) &qswekopf.mdn, 1, 0);
            ins_quest ((char *) &qswekopf.fil, 1, 0);
            ins_quest ((char *) qswekopf.lief, 0, 17);
            ins_quest ((char *) qswekopf.lief_rech_nr, 0, 17);
            ins_quest ((char *) &qswekopf.lfd, 2, 0);
            del_cursor = prepare_sql ("delete from qswekopf "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   lief = ? "
                                  "and   lief_rech_nr = ? "
                                  "and lfd = ?");
    ins_quest ((char *) &qswekopf.mdn,1,0);
    ins_quest ((char *) &qswekopf.fil,1,0);
    ins_quest ((char *) &qswekopf.lfd,2,0);
    ins_quest ((char *) qswekopf.lief_rech_nr,0,17);
    ins_quest ((char *) qswekopf.lief,0,17);
    ins_quest ((char *) qswekopf.txt,0,61);
    ins_quest ((char *) &qswekopf.wrt,1,0);
    ins_quest ((char *) &qswekopf.dat,2,0);
    ins_quest ((char *) qswekopf.pers_nam,0,9);
    ins_quest ((char *) qswekopf.txt2,0,61);
    ins_quest ((char *) qswekopf.txt3,0,61);
    ins_quest ((char *) qswekopf.txt4,0,61);
    ins_quest ((char *) qswekopf.txt5,0,61);
    ins_quest ((char *) qswekopf.txt6,0,61);
    ins_quest ((char *) qswekopf.txt7,0,61);
    ins_quest ((char *) qswekopf.txt8,0,61);
    ins_quest ((char *) qswekopf.txt9,0,61);
    ins_quest ((char *) qswekopf.txt10,0,61);
    ins_quest ((char *) &qswekopf.wrt2,1,0);
    ins_quest ((char *) &qswekopf.wrt3,1,0);
    ins_quest ((char *) &qswekopf.wrt4,1,0);
    ins_quest ((char *) &qswekopf.wrt5,1,0);
    ins_quest ((char *) &qswekopf.wrt6,1,0);
    ins_quest ((char *) &qswekopf.wrt7,1,0);
    ins_quest ((char *) &qswekopf.wrt8,1,0);
    ins_quest ((char *) &qswekopf.wrt9,1,0);
    ins_quest ((char *) &qswekopf.wrt10,1,0);
            ins_cursor = prepare_sql ("insert into qswekopf ("
"mdn,  fil,  lfd,  lief_rech_nr,  lief,  txt,  wrt,  dat,  pers_nam,  txt2,  txt3,  txt4,  txt5,  "
"txt6,  txt7,  txt8,  txt9,  txt10,  wrt2,  wrt3,  wrt4,  wrt5,  wrt6,  wrt7,  wrt8,  wrt9,  wrt10) "

#line 72 "qswekopf.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"); 

#line 74 "qswekopf.rpp"
}
int QSWEKOPF_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

