#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "formfeld.h"
#include "dbfunc.h"

struct FORM_FELD _form_feld;
struct FORM_FRM _form_frm ;
struct FORM_HEAD form_head, form_head_null;

int FORMFELD_CLASS::lese_formfeld (char *form_nr)
/**
Tabelle form_feld fuer form_feld lesen
**/
{
         if (cursor_formfeld == -1)
         {
         /* tab_nam auch noch lesen JG */
                    ins_quest (_form_feld.form_nr, 0, 6);
                    out_quest (_form_feld.tab_nam, 0, 19 );
                    out_quest (_form_feld.feld_nam, 0, 19);
                    out_quest (_form_feld.feld_id, 0, 2);
                    out_quest ((char *) &_form_feld.lfd, 2, 0);
                    cursor_formfeld = prepare_sql
                       ("select tab_nam, feld_nam, feld_id,lfd from form_feld "
                               "where form_nr = ? "
                               "and (feld_id = 2 or "
                                    "feld_id = 3 or "
                                    "feld_id = 4 or "
                                    "feld_id = 5) "
                                    "order by lfd");
                    if (sqlstatus) return (-1);
         }
         strcpy (_form_feld.form_nr, form_nr);
         open_sql (cursor_formfeld);
         fetch_sql (cursor_formfeld);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int FORMFELD_CLASS::lese_formfeld (void)
/**
Naechsten Satz aus Tabelle a_pr lesen.
**/
{
         fetch_sql (cursor_formfeld);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

void FORMFELD_CLASS::erase_formfeld (void)
/**
Cursor freigeben.
**/
{
         if (cursor_formfeld == -1) return;
         close_sql (cursor_formfeld);
         cursor_formfeld = -1;
}

int FORMFELD_CLASS::lese_feld (char *form_nr, char *tab_nam, char *feld_nam)
/**
Tabelle form_feld fuer form_feld lesen
**/
{
         if (cursor_feld == -1)
         {
                    ins_quest (_form_feld.form_nr, 0, 6);
                    ins_quest (_form_feld.tab_nam, 0, 19 );
                    ins_quest (_form_feld.feld_nam, 0, 19);
                    out_quest (_form_feld.feld_id, 0, 2);
                    out_quest ((char *) &_form_feld.lfd, 2, 0);
                    cursor_feld = prepare_sql
                       ("select feld_id,lfd from form_feld "
                               "where form_nr = ? "
                               "and tab_nam = ? "
                               "and feld_nam = ? "
                               "and (feld_id = 2 or "
                                    "feld_id = 3 or "
                                    "feld_id = 4 or "
                                    "feld_id = 5) "
                                    "order by lfd");
                    if (sqlstatus) return (-1);
         }
         strcpy (_form_feld.form_nr,  form_nr);
         strcpy (_form_feld.tab_nam,  tab_nam);
         strcpy (_form_feld.feld_nam, feld_nam);
         open_sql (cursor_feld);
         fetch_sql (cursor_feld);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

void FORMFELD_CLASS::erase_feld (void)
/**
Cursor freigeben.
**/
{
         if (cursor_feld == -1) return;
         close_sql (cursor_feld);
         cursor_feld = -1;
}

int FORMFRM_CLASS::lese_formfrm (char *form_nr)
/**
Tabelle form_frm fuer form_frm lesen
**/
{
         if (cursor_formfrm == -1)
         {
             ins_quest (_form_frm.form_nr, 0, 6);
             out_quest (_form_frm.form_nr, 0, 6);
             cursor_formfrm = prepare_sql
                               ("select form_nr from form_frm "
                               "where form_nr = ? "
                               );
                    if (sqlstatus) return (-1);
         }
         strcpy ( _form_frm.form_nr, form_nr);
         open_sql (cursor_formfrm);
         fetch_sql (cursor_formfrm);
         if ( sqlstatus == 0 )
         {
                    return 0;
         }
         return 100;
}

int FORMFRM_CLASS::lese_formfrm (void)
/**
Naechsten Satz aus Tabelle form_frm lesen.
**/
{
         fetch_sql (cursor_formfrm);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

void FORMFRM_CLASS::erase_formfrm (void)
/**
Cursor freigeben.
**/
{
         if (cursor_formfrm == -1) return;
         close_sql (cursor_formfrm);
}


FORMHEAD_CLASS::FORMHEAD_CLASS ()
{
         cursor = -1;
}

void FORMHEAD_CLASS::prepare (void)
{
    ins_quest ((char *) form_head.form_nr,0,6);
    out_quest ((char *) &form_head.delstatus,1,0);
    out_quest ((char *) form_head.form_nr,0,6);
    out_quest ((char *) form_head.form_ube,0,129);
    out_quest ((char *) form_head.form_vers,0,4);
    out_quest ((char *) form_head.form_distinct,0,2);
    out_quest ((char *) form_head.lst_par,0,21);
    out_quest ((char *) form_head.sort,0,25);
    out_quest ((char *) &form_head.zei_bis,1,0);
    out_quest ((char *) &form_head.zei_von,1,0);
    out_quest ((char *) &form_head.zei_von1,1,0);
    out_quest ((char *) &form_head.zei_bis1,1,0);
cursor = prepare_sql ("select form_head.delstatus,  "
"form_head.form_nr,  form_head.form_ube,  form_head.form_vers,  "
"form_head.form_distinct,  form_head.lst_par,  form_head.sort,  "
"form_head.zei_bis,  form_head.zei_von,  form_head.zei_von1,  "
"form_head.zei_bis1 from form_head where form_nr = ?");
}

int FORMHEAD_CLASS::dbreadfirst (void)
{
         if (cursor == -1)
         {
                    prepare ();
         }
         open_sql (cursor);
         fetch_sql (cursor);
         if ( sqlstatus == 0 )
         {
                    return 0;
         }
         return 100;
}

int FORMHEAD_CLASS::dbread (void)
{
         fetch_sql (cursor);
         if ( sqlstatus == 0 )
         {
                    return 0;
         }
         return 100;
}

int FORMHEAD_CLASS::dbclose (void)
{
         if (cursor != -1)
         {
                   close_sql (cursor);
                   cursor = -1;
         }
         return 0;
}
 

