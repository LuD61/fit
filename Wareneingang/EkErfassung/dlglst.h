#ifndef _DLGLST_DEF
#define _DLGLST_DEF
#include "dlg.h"
#include "itprog.h"
#include "itfont.h"
#include "formfield.h"

#define RAISEDBORDER 0
#define HIGHBORDER 1
#define LOWBORDER 2
#define HIGHCOLBORDER 3
#define LOWCOLBORDER 4
#define LINEBORDER 5
#include "searcha.h"

class DlgLst : virtual public DLG 
{
          protected :
             static int ListRows;
             static BOOL InsMode;
			 static BOOL WriteOK;
			 static char *HelpName;
             CFORM *Toolbar2;
			 void *Dlg;
             int    BorderType;

          public :
            void SetDlg (void *Dlg)
			{
					 this->Dlg = Dlg;
			}

            void SetToolbar2 (CFORM *Toolbar2)
            {
                this->Toolbar2 = Toolbar2;
            }

            CFORM *GetToolbar2 (void)
            {
                return Toolbar2;
            }
            DlgLst (int, int, int, int, char *, int, BOOL);
 	        void Init (int, int, int, int, char *, int, BOOL);
            virtual int WriteRow (void);
            virtual void InitRec (void);
            virtual void ToForm (void);
            virtual void FromForm (void);
            virtual void FillRow (char *);
            virtual BOOL InsPosOk (char *);
            virtual BOOL SelPosOk (char *);
            virtual BOOL OnKeyDown (void);
            virtual BOOL OnKeyUp (void);
            virtual BOOL OnKey1 (void);
            virtual BOOL OnKey3 (void);
            virtual BOOL OnKey4 (void);
            virtual BOOL OnKey5 (void);
            virtual BOOL OnKey12 (void);
            virtual BOOL OnKeyDelete (void);
            virtual BOOL OnCommand (HWND,UINT,WPARAM,LPARAM);
            virtual void FillListCaption (void);
            virtual void FillRow (void);
            virtual void InsertRow (void);
            virtual int DeleteRow (void);
            virtual void SelectRow (void);
            virtual void FillEnterList (int);
            virtual void FillEnterList (char *);
            virtual int ListChanged (WPARAM, LPARAM);
            virtual void FillListRows (void);
			virtual void Scroll (int);

            BOOL OnKeyPrior (void);
            BOOL OnKeyNext (void);
            BOOL OnPaint (HWND,HDC,UINT,WPARAM,LPARAM);
            BOOL OnActivate (HWND,UINT,WPARAM,LPARAM);
            BOOL OnSysCommand (HWND,UINT,WPARAM,LPARAM);
            BOOL OnDestroy (HWND,UINT,WPARAM,LPARAM);
            void PrintComment (char *);
            HWND OpenWindow (HANDLE, HWND);
            HWND OpenScrollWindow (HANDLE, HWND);
//            void SetWinBackground (COLORREF);
            void ToClipboard (void);
            void FromClipboard (void);
            void Help (void);
            void SetListDimension (int, int);
            void DiffListDimension (int, int);
            static void FillFrmTitle (form *, char *, int, int);
            static void FillFrmLines (form *, char *, int, int);
            static void FillFrmPos (form *, char *, int, int);
            static void FillFrmAttr (form *, char *, int);
            static void FillFrmData (form *, char *, int, int);
            static int NextFieldPos (form *, int, int, int);
            static int AddSepAnz (char *, int);
			int GetAktRow (void);
			int GetListRows (void);
};
#endif
