#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "a_kun.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "fleisch_ek.h"

struct FLEISCH_EK fleisch_ek, fleisch_ek_null;

void FLEISCH_EK_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &fleisch_ek.mdn, 1, 0);
            ins_quest ((char *) &fleisch_ek.dat, 2, 0);
            ins_quest ((char *) &fleisch_ek.a, 3, 0);
            ins_quest ((char *) &fleisch_ek.rowid, 2, 0);
    out_quest ((char *) &fleisch_ek.mdn,1,0);
    out_quest ((char *) &fleisch_ek.dat,2,0);
    out_quest ((char *) &fleisch_ek.gue_ab,2,0);
    out_quest ((char *) &fleisch_ek.a,3,0);
    out_quest ((char *) &fleisch_ek.preis,3,0);
    out_quest ((char *) &fleisch_ek.sk,3,0);
    out_quest ((char *) &fleisch_ek.fil_ek,3,0);
    out_quest ((char *) &fleisch_ek.me_einh,1,0);
    out_quest ((char *) &fleisch_ek.delstatus,1,0);
    out_quest ((char *) &fleisch_ek.posi,2,0);
            cursor = prepare_sql ("select fleisch_ek.mdn,  "
"fleisch_ek.dat,  fleisch_ek.gue_ab,  fleisch_ek.a,  fleisch_ek.preis,  "
"fleisch_ek.sk,  fleisch_ek.fil_ek,  fleisch_ek.me_einh,  "
"fleisch_ek.delstatus,  fleisch_ek.posi from fleisch_ek "

#line 29 "fleisch_ek.rpp"
                                  "where mdn = ? "
                                  "and   dat = ? "
                                  "and   a = ?"
								   "and rowid = ?");

    ins_quest ((char *) &fleisch_ek.mdn,1,0);
    ins_quest ((char *) &fleisch_ek.dat,2,0);
    ins_quest ((char *) &fleisch_ek.gue_ab,2,0);
    ins_quest ((char *) &fleisch_ek.a,3,0);
    ins_quest ((char *) &fleisch_ek.preis,3,0);
    ins_quest ((char *) &fleisch_ek.sk,3,0);
    ins_quest ((char *) &fleisch_ek.fil_ek,3,0);
    ins_quest ((char *) &fleisch_ek.me_einh,1,0);
    ins_quest ((char *) &fleisch_ek.delstatus,1,0);
    ins_quest ((char *) &fleisch_ek.posi,2,0);
            sqltext = "update fleisch_ek set "
"fleisch_ek.mdn = ?,  fleisch_ek.dat = ?,  fleisch_ek.gue_ab = ?,  "
"fleisch_ek.a = ?,  fleisch_ek.preis = ?,  fleisch_ek.sk = ?,  "
"fleisch_ek.fil_ek = ?,  fleisch_ek.me_einh = ?,  "
"fleisch_ek.delstatus = ?,  fleisch_ek.posi = ? "

#line 35 "fleisch_ek.rpp"
                                  "where mdn = ? "
                                  "and   dat = ? "
                                  "and   a = ?";


            ins_quest ((char *) &fleisch_ek.mdn, 1, 0);
            ins_quest ((char *) &fleisch_ek.dat, 2, 0);
            ins_quest ((char *) &fleisch_ek.a, 3, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &fleisch_ek.mdn, 1, 0);
            ins_quest ((char *) &fleisch_ek.dat, 2, 0);
            ins_quest ((char *) &fleisch_ek.a, 3, 0);
            test_upd_cursor = prepare_sql ("select a from fleisch_ek "
                                  "where mdn = ? "
                                  "and   dat = ? "
                                  "and   a = ?");

            ins_quest ((char *) &fleisch_ek.mdn, 1, 0);
            ins_quest ((char *) &fleisch_ek.dat, 2, 0);
            ins_quest ((char *) &fleisch_ek.a, 3, 0);
            del_cursor = prepare_sql ("delete from fleisch_ek "
                                  "where mdn = ? "
                                  "and   dat = ? "
                                  "and   a = ?");

            ins_quest ((char *) &fleisch_ek.mdn, 1, 0);
            ins_quest ((char *) &fleisch_ek.dat, 2, 0);
            del_best_curs = prepare_sql ("delete from fleisch_ek "
                                         "where mdn = ? "
                                         "and   dat = ? ");

    ins_quest ((char *) &fleisch_ek.mdn,1,0);
    ins_quest ((char *) &fleisch_ek.dat,2,0);
    ins_quest ((char *) &fleisch_ek.gue_ab,2,0);
    ins_quest ((char *) &fleisch_ek.a,3,0);
    ins_quest ((char *) &fleisch_ek.preis,3,0);
    ins_quest ((char *) &fleisch_ek.sk,3,0);
    ins_quest ((char *) &fleisch_ek.fil_ek,3,0);
    ins_quest ((char *) &fleisch_ek.me_einh,1,0);
    ins_quest ((char *) &fleisch_ek.delstatus,1,0);
    ins_quest ((char *) &fleisch_ek.posi,2,0);
            ins_cursor = prepare_sql ("insert into fleisch_ek ("
"mdn,  dat,  gue_ab,  a,  preis,  sk,  fil_ek,  me_einh,  delstatus,  posi) "

#line 68 "fleisch_ek.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?)");

#line 70 "fleisch_ek.rpp"
}

void FLEISCH_EK_CLASS::prepare_o (char *order)
{

            ins_quest ((char *) &fleisch_ek.mdn, 1, 0);
            ins_quest ((char *) &fleisch_ek.dat, 2, 0);
            if (order)
            {
    out_quest ((char *) &fleisch_ek.mdn,1,0);
    out_quest ((char *) &fleisch_ek.dat,2,0);
    out_quest ((char *) &fleisch_ek.gue_ab,2,0);
    out_quest ((char *) &fleisch_ek.a,3,0);
    out_quest ((char *) &fleisch_ek.preis,3,0);
    out_quest ((char *) &fleisch_ek.sk,3,0);
    out_quest ((char *) &fleisch_ek.fil_ek,3,0);
    out_quest ((char *) &fleisch_ek.me_einh,1,0);
    out_quest ((char *) &fleisch_ek.delstatus,1,0);
    out_quest ((char *) &fleisch_ek.posi,2,0);
                    cursor_o = prepare_sql ("select "
"fleisch_ek.mdn,  fleisch_ek.dat,  fleisch_ek.gue_ab,  fleisch_ek.a,  "
"fleisch_ek.preis,  fleisch_ek.sk,  fleisch_ek.fil_ek,  "
"fleisch_ek.me_einh,  fleisch_ek.delstatus,  fleisch_ek.posi from fleisch_ek "

#line 80 "fleisch_ek.rpp"
                                          "where mdn = ? "
                                          "and   dat = ? %s", order);
            }
            else
            {
    out_quest ((char *) &fleisch_ek.mdn,1,0);
    out_quest ((char *) &fleisch_ek.dat,2,0);
    out_quest ((char *) &fleisch_ek.gue_ab,2,0);
    out_quest ((char *) &fleisch_ek.a,3,0);
    out_quest ((char *) &fleisch_ek.preis,3,0);
    out_quest ((char *) &fleisch_ek.sk,3,0);
    out_quest ((char *) &fleisch_ek.fil_ek,3,0);
    out_quest ((char *) &fleisch_ek.me_einh,1,0);
    out_quest ((char *) &fleisch_ek.delstatus,1,0);
    out_quest ((char *) &fleisch_ek.posi,2,0);
                    cursor_o = prepare_sql ("select "
"fleisch_ek.mdn,  fleisch_ek.dat,  fleisch_ek.gue_ab,  fleisch_ek.a,  "
"fleisch_ek.preis,  fleisch_ek.sk,  fleisch_ek.fil_ek,  "
"fleisch_ek.me_einh,  fleisch_ek.delstatus,  fleisch_ek.posi from fleisch_ek "

#line 86 "fleisch_ek.rpp"
                                          "where mdn = ? "
                                          "and   dat = ? ");
           }
}

int FLEISCH_EK_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int FLEISCH_EK_CLASS::dbreadfirst_o (char *order)
/**
Ersten Satz aus Tabelle lesen.
**/
{

         if (cursor == -1)
         {
                this->prepare ();
         }
         if (cursor_o == -1)
         {
                this->prepare_o (order);
         }
         open_sql (cursor_o);
         fetch_sql (cursor_o);
         return (sqlstatus);
}

int FLEISCH_EK_CLASS::dbread_o ()
/**
Ersten Satz aus Tabelle lesen.
**/
{

         fetch_sql (cursor_o);
         return (sqlstatus);
}

int FLEISCH_EK_CLASS::dbdeletebest ()
/**
Alles Positionen einer Bestellung loeschen.
**/
{
         if (del_best_curs == -1)
         {
                     prepare ();
         }
         return execute_curs (del_best_curs);
}

int FLEISCH_EK_CLASS::dbclose_o ()
/**
Cursor schliessen.
**/
{
         if (del_best_curs)
         {
                      close_sql (del_best_curs);
                      del_best_curs = -1;
         }
         this->DB_CLASS::dbclose ();
         return 0;
}
int FLEISCH_EK_CLASS::dbinsert (void)
/**
In fleisch_ek einfuegen.
**/
{
        int dsqlstatus;
        if (ins_cursor == -1)
        {
                       prepare ();
        }
        dsqlstatus = execute_curs (ins_cursor);
        return dsqlstatus;
}


