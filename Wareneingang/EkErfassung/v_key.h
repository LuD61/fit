#ifndef V_KEY_DEF
#define V_KEY_DEF
class V_KEY
{
          private :
              HWND hWnd;
			  int vkey;
			  DWORD ID;
			  int (*funk) (void);
          public :
			  V_KEY ()
			  {
				  hWnd = NULL;
				  ID = (DWORD) 0;
				  funk = NULL;
			  }

			  void SethWnd (HWND hWnd)
			  {
				  this->hWnd = hWnd;
			  }
			  void SetVkey (int vkey)
			  {
				  this->vkey = vkey;
			  }
			  void SetBuId (DWORD ID)
			  {
				  this->ID = ID;
			  }
			  void SetFunk (int (*funk) (void))
			  {
				  this->funk = funk;
			  }
			  BOOL DoVkey (int vkey)
			  {
				  if (vkey != this->vkey) return FALSE;

			      if (ID)
				  {
			              SendMessage (hWnd, WM_COMMAND, MAKELONG (ID, VK_RETURN),
				                                    (LPARAM) hWnd); 
				          return TRUE;
				  }

			      if (funk)
				  {
				          (*funk) ();
				          return TRUE;
				  }
			  return FALSE;
			  }
};
#endif
