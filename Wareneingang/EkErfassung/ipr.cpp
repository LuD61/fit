#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "ipr.h"

struct IPR ipr, ipr_null;

void IPR_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &ipr.mdn, 2, 0);
            ins_quest ((char *) &ipr.a, 3, 0);
            ins_quest ((char *) &ipr.pr_gr_stuf, 1, 0);
            ins_quest ((char *) &ipr.kun_pr, 2, 0);
            ins_quest ((char *) &ipr.kun, 2, 0);
    out_quest ((char *) &ipr.mdn,1,0);
    out_quest ((char *) &ipr.pr_gr_stuf,2,0);
    out_quest ((char *) &ipr.kun_pr,2,0);
    out_quest ((char *) &ipr.a,3,0);
    out_quest ((char *) &ipr.vk_pr_i,3,0);
    out_quest ((char *) &ipr.vk_pr_eu,3,0);
    out_quest ((char *) &ipr.ld_pr,3,0);
    out_quest ((char *) &ipr.ld_pr_eu,3,0);
    out_quest ((char *) &ipr.aktion_nr,1,0);
    out_quest ((char *) ipr.a_akt_kz,0,2);
    out_quest ((char *) ipr.modif,0,2);
    out_quest ((char *) &ipr.waehrung,1,0);
    out_quest ((char *) &ipr.kun,2,0);
    out_quest ((char *) &ipr.a_grund,3,0);
    out_quest ((char *) ipr.kond_art,0,5);
    out_quest ((char *) &ipr.add_ek_proz,3,0);
    out_quest ((char *) &ipr.add_ek_abs,3,0);
            cursor = prepare_sql ("select ipr.mdn,  "
"ipr.pr_gr_stuf,  ipr.kun_pr,  ipr.a,  ipr.vk_pr_i,  ipr.vk_pr_eu,  "
"ipr.ld_pr,  ipr.ld_pr_eu,  ipr.aktion_nr,  ipr.a_akt_kz,  ipr.modif,  "
"ipr.waehrung,  ipr.kun,  ipr.a_grund,  ipr.kond_art,  ipr.add_ek_proz,  "
"ipr.add_ek_abs from ipr "

#line 29 "ipr.rpp"
                                  "where mdn = ? "
                                  "and   a   = ? "
                                  "and   pr_gr_stuf = ? "
                                  "and kun_pr = ? "
                                  "and kun = ?"); 



    ins_quest ((char *) &ipr.mdn,1,0);
    ins_quest ((char *) &ipr.pr_gr_stuf,2,0);
    ins_quest ((char *) &ipr.kun_pr,2,0);
    ins_quest ((char *) &ipr.a,3,0);
    ins_quest ((char *) &ipr.vk_pr_i,3,0);
    ins_quest ((char *) &ipr.vk_pr_eu,3,0);
    ins_quest ((char *) &ipr.ld_pr,3,0);
    ins_quest ((char *) &ipr.ld_pr_eu,3,0);
    ins_quest ((char *) &ipr.aktion_nr,1,0);
    ins_quest ((char *) ipr.a_akt_kz,0,2);
    ins_quest ((char *) ipr.modif,0,2);
    ins_quest ((char *) &ipr.waehrung,1,0);
    ins_quest ((char *) &ipr.kun,2,0);
    ins_quest ((char *) &ipr.a_grund,3,0);
    ins_quest ((char *) ipr.kond_art,0,5);
    ins_quest ((char *) &ipr.add_ek_proz,3,0);
    ins_quest ((char *) &ipr.add_ek_abs,3,0);
            sqltext = "update ipr set ipr.mdn = ?,  "
"ipr.pr_gr_stuf = ?,  ipr.kun_pr = ?,  ipr.a = ?,  ipr.vk_pr_i = ?,  "
"ipr.vk_pr_eu = ?,  ipr.ld_pr = ?,  ipr.ld_pr_eu = ?,  "
"ipr.aktion_nr = ?,  ipr.a_akt_kz = ?,  ipr.modif = ?,  "
"ipr.waehrung = ?,  ipr.kun = ?,  ipr.a_grund = ?,  ipr.kond_art = ?,  "
"ipr.add_ek_proz = ?,  ipr.add_ek_abs = ? "

#line 38 "ipr.rpp"
                               "where mdn = ? "
                               "and   a = ? "
                               "and   pr_gr_stuf = ? "
                               "and   kun_pr = ? "
                               "and   kun = ?";
  
            ins_quest ((char *) &ipr.mdn, 2, 0);
            ins_quest ((char *) &ipr.a, 3, 0);
            ins_quest ((char *) &ipr.pr_gr_stuf, 1, 0);
            ins_quest ((char *) &ipr.kun_pr,  2, 0);  
            ins_quest ((char *) &ipr.kun,  2, 0);  

            upd_cursor = prepare_sql (sqltext);

    ins_quest ((char *) &ipr.mdn,1,0);
    ins_quest ((char *) &ipr.pr_gr_stuf,2,0);
    ins_quest ((char *) &ipr.kun_pr,2,0);
    ins_quest ((char *) &ipr.a,3,0);
    ins_quest ((char *) &ipr.vk_pr_i,3,0);
    ins_quest ((char *) &ipr.vk_pr_eu,3,0);
    ins_quest ((char *) &ipr.ld_pr,3,0);
    ins_quest ((char *) &ipr.ld_pr_eu,3,0);
    ins_quest ((char *) &ipr.aktion_nr,1,0);
    ins_quest ((char *) ipr.a_akt_kz,0,2);
    ins_quest ((char *) ipr.modif,0,2);
    ins_quest ((char *) &ipr.waehrung,1,0);
    ins_quest ((char *) &ipr.kun,2,0);
    ins_quest ((char *) &ipr.a_grund,3,0);
    ins_quest ((char *) ipr.kond_art,0,5);
    ins_quest ((char *) &ipr.add_ek_proz,3,0);
    ins_quest ((char *) &ipr.add_ek_abs,3,0);
            ins_cursor = prepare_sql ("insert into ipr (mdn,  "
"pr_gr_stuf,  kun_pr,  a,  vk_pr_i,  vk_pr_eu,  ld_pr,  ld_pr_eu,  aktion_nr,  "
"a_akt_kz,  modif,  waehrung,  kun,  a_grund,  kond_art,  add_ek_proz,  add_ek_abs) "

#line 53 "ipr.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?)"); 

#line 55 "ipr.rpp"
            ins_quest ((char *) &ipr.mdn, 2, 0);
            ins_quest ((char *) &ipr.a, 3, 0);
            ins_quest ((char *) &ipr.pr_gr_stuf, 1, 0);
            ins_quest ((char *) &ipr.kun_pr,  2, 0);  
            ins_quest ((char *) &ipr.kun,  2, 0);  
            test_upd_cursor = prepare_sql ("select a from ipr "
                                  "where mdn = ? "
                                  "and   a   = ? "
                                  "and   pr_gr_stuf = ? "
                                  "and kun_pr = ? "
                                  "and kun = ?"); 
              
            ins_quest ((char *) &ipr.mdn, 2, 0);
            ins_quest ((char *) &ipr.a, 3, 0);
            ins_quest ((char *) &ipr.pr_gr_stuf, 1, 0);
            ins_quest ((char *) &ipr.kun_pr,  2, 0);  
            ins_quest ((char *) &ipr.kun,  2, 0);  
            del_cursor = prepare_sql ("delete from ipr "
                                  "where mdn = ? "
                                  "and   a   = ? "
                                  "and   pr_gr_stuf = ? "
                                  "and kun_pr = ? "
                                  "and kun = ?"); 
}

void IPR_CLASS::prepare_a (char *order)
{
	if (ipr.mdn == 0)
	{
            ins_quest ((char *) &ipr.a, 3, 0);
            if (order)
            {
    out_quest ((char *) &ipr.mdn,1,0);
    out_quest ((char *) &ipr.pr_gr_stuf,2,0);
    out_quest ((char *) &ipr.kun_pr,2,0);
    out_quest ((char *) &ipr.a,3,0);
    out_quest ((char *) &ipr.vk_pr_i,3,0);
    out_quest ((char *) &ipr.vk_pr_eu,3,0);
    out_quest ((char *) &ipr.ld_pr,3,0);
    out_quest ((char *) &ipr.ld_pr_eu,3,0);
    out_quest ((char *) &ipr.aktion_nr,1,0);
    out_quest ((char *) ipr.a_akt_kz,0,2);
    out_quest ((char *) ipr.modif,0,2);
    out_quest ((char *) &ipr.waehrung,1,0);
    out_quest ((char *) &ipr.kun,2,0);
    out_quest ((char *) &ipr.a_grund,3,0);
    out_quest ((char *) ipr.kond_art,0,5);
    out_quest ((char *) &ipr.add_ek_proz,3,0);
    out_quest ((char *) &ipr.add_ek_abs,3,0);
                        cursor_a = prepare_sql ("select "
"ipr.mdn,  ipr.pr_gr_stuf,  ipr.kun_pr,  ipr.a,  ipr.vk_pr_i,  ipr.vk_pr_eu,  "
"ipr.ld_pr,  ipr.ld_pr_eu,  ipr.aktion_nr,  ipr.a_akt_kz,  ipr.modif,  "
"ipr.waehrung,  ipr.kun,  ipr.a_grund,  ipr.kond_art,  ipr.add_ek_proz,  "
"ipr.add_ek_abs from ipr "

#line 88 "ipr.rpp"
                                  "where a = ? "
                                  "%s", order);
            }
            else
            {
    out_quest ((char *) &ipr.mdn,1,0);
    out_quest ((char *) &ipr.pr_gr_stuf,2,0);
    out_quest ((char *) &ipr.kun_pr,2,0);
    out_quest ((char *) &ipr.a,3,0);
    out_quest ((char *) &ipr.vk_pr_i,3,0);
    out_quest ((char *) &ipr.vk_pr_eu,3,0);
    out_quest ((char *) &ipr.ld_pr,3,0);
    out_quest ((char *) &ipr.ld_pr_eu,3,0);
    out_quest ((char *) &ipr.aktion_nr,1,0);
    out_quest ((char *) ipr.a_akt_kz,0,2);
    out_quest ((char *) ipr.modif,0,2);
    out_quest ((char *) &ipr.waehrung,1,0);
    out_quest ((char *) &ipr.kun,2,0);
    out_quest ((char *) &ipr.a_grund,3,0);
    out_quest ((char *) ipr.kond_art,0,5);
    out_quest ((char *) &ipr.add_ek_proz,3,0);
    out_quest ((char *) &ipr.add_ek_abs,3,0);
                        cursor_a = prepare_sql ("select "
"ipr.mdn,  ipr.pr_gr_stuf,  ipr.kun_pr,  ipr.a,  ipr.vk_pr_i,  ipr.vk_pr_eu,  "
"ipr.ld_pr,  ipr.ld_pr_eu,  ipr.aktion_nr,  ipr.a_akt_kz,  ipr.modif,  "
"ipr.waehrung,  ipr.kun,  ipr.a_grund,  ipr.kond_art,  ipr.add_ek_proz,  "
"ipr.add_ek_abs from ipr "

#line 94 "ipr.rpp"
                                  "where a = ? ");
            }
            ins_quest ((char *) &ipr.a, 3, 0);
            del_a_curs = prepare_sql ("delete from ipr "
                                  "where a = ? ");
	}
	else
	{
            ins_quest ((char *) &ipr.mdn, 2, 0);
            ins_quest ((char *) &ipr.a, 3, 0);
            if (order)
            {
    out_quest ((char *) &ipr.mdn,1,0);
    out_quest ((char *) &ipr.pr_gr_stuf,2,0);
    out_quest ((char *) &ipr.kun_pr,2,0);
    out_quest ((char *) &ipr.a,3,0);
    out_quest ((char *) &ipr.vk_pr_i,3,0);
    out_quest ((char *) &ipr.vk_pr_eu,3,0);
    out_quest ((char *) &ipr.ld_pr,3,0);
    out_quest ((char *) &ipr.ld_pr_eu,3,0);
    out_quest ((char *) &ipr.aktion_nr,1,0);
    out_quest ((char *) ipr.a_akt_kz,0,2);
    out_quest ((char *) ipr.modif,0,2);
    out_quest ((char *) &ipr.waehrung,1,0);
    out_quest ((char *) &ipr.kun,2,0);
    out_quest ((char *) &ipr.a_grund,3,0);
    out_quest ((char *) ipr.kond_art,0,5);
    out_quest ((char *) &ipr.add_ek_proz,3,0);
    out_quest ((char *) &ipr.add_ek_abs,3,0);
                        cursor_a = prepare_sql ("select "
"ipr.mdn,  ipr.pr_gr_stuf,  ipr.kun_pr,  ipr.a,  ipr.vk_pr_i,  ipr.vk_pr_eu,  "
"ipr.ld_pr,  ipr.ld_pr_eu,  ipr.aktion_nr,  ipr.a_akt_kz,  ipr.modif,  "
"ipr.waehrung,  ipr.kun,  ipr.a_grund,  ipr.kond_art,  ipr.add_ek_proz,  "
"ipr.add_ek_abs from ipr "

#line 107 "ipr.rpp"
                                  "where mdn = ? "
                                  "and   a   = ? "
                                  "%s", order);
            }
            else
            {
    out_quest ((char *) &ipr.mdn,1,0);
    out_quest ((char *) &ipr.pr_gr_stuf,2,0);
    out_quest ((char *) &ipr.kun_pr,2,0);
    out_quest ((char *) &ipr.a,3,0);
    out_quest ((char *) &ipr.vk_pr_i,3,0);
    out_quest ((char *) &ipr.vk_pr_eu,3,0);
    out_quest ((char *) &ipr.ld_pr,3,0);
    out_quest ((char *) &ipr.ld_pr_eu,3,0);
    out_quest ((char *) &ipr.aktion_nr,1,0);
    out_quest ((char *) ipr.a_akt_kz,0,2);
    out_quest ((char *) ipr.modif,0,2);
    out_quest ((char *) &ipr.waehrung,1,0);
    out_quest ((char *) &ipr.kun,2,0);
    out_quest ((char *) &ipr.a_grund,3,0);
    out_quest ((char *) ipr.kond_art,0,5);
    out_quest ((char *) &ipr.add_ek_proz,3,0);
    out_quest ((char *) &ipr.add_ek_abs,3,0);
                        cursor_a = prepare_sql ("select "
"ipr.mdn,  ipr.pr_gr_stuf,  ipr.kun_pr,  ipr.a,  ipr.vk_pr_i,  ipr.vk_pr_eu,  "
"ipr.ld_pr,  ipr.ld_pr_eu,  ipr.aktion_nr,  ipr.a_akt_kz,  ipr.modif,  "
"ipr.waehrung,  ipr.kun,  ipr.a_grund,  ipr.kond_art,  ipr.add_ek_proz,  "
"ipr.add_ek_abs from ipr "

#line 114 "ipr.rpp"
                                  "where mdn = ? "
                                  "and   a   = ?");
            }
            ins_quest ((char *) &ipr.mdn, 2, 0);
            ins_quest ((char *) &ipr.a, 3, 0);
            del_a_curs = prepare_sql ("delete from ipr "
                                  "where mdn = ? "
                                  "and   a   = ?");
   }
}

void IPR_CLASS::prepare_gr (char *order)
{
            ins_quest ((char *) &ipr.mdn, 2, 0);
            ins_quest ((char *) &ipr.pr_gr_stuf, 1, 0);
            ins_quest ((char *) &ipr.kun_pr, 2, 0);
            if (order)
            {
    out_quest ((char *) &ipr.mdn,1,0);
    out_quest ((char *) &ipr.pr_gr_stuf,2,0);
    out_quest ((char *) &ipr.kun_pr,2,0);
    out_quest ((char *) &ipr.a,3,0);
    out_quest ((char *) &ipr.vk_pr_i,3,0);
    out_quest ((char *) &ipr.vk_pr_eu,3,0);
    out_quest ((char *) &ipr.ld_pr,3,0);
    out_quest ((char *) &ipr.ld_pr_eu,3,0);
    out_quest ((char *) &ipr.aktion_nr,1,0);
    out_quest ((char *) ipr.a_akt_kz,0,2);
    out_quest ((char *) ipr.modif,0,2);
    out_quest ((char *) &ipr.waehrung,1,0);
    out_quest ((char *) &ipr.kun,2,0);
    out_quest ((char *) &ipr.a_grund,3,0);
    out_quest ((char *) ipr.kond_art,0,5);
    out_quest ((char *) &ipr.add_ek_proz,3,0);
    out_quest ((char *) &ipr.add_ek_abs,3,0);
                        cursor_gr = prepare_sql ("select "
"ipr.mdn,  ipr.pr_gr_stuf,  ipr.kun_pr,  ipr.a,  ipr.vk_pr_i,  ipr.vk_pr_eu,  "
"ipr.ld_pr,  ipr.ld_pr_eu,  ipr.aktion_nr,  ipr.a_akt_kz,  ipr.modif,  "
"ipr.waehrung,  ipr.kun,  ipr.a_grund,  ipr.kond_art,  ipr.add_ek_proz,  "
"ipr.add_ek_abs from ipr "

#line 133 "ipr.rpp"
                                  "where mdn = ? "
                                  "and   pr_gr_stuf   = ? "
                                  "and   kun_pr   = ? "
                                  "%s", order);
            }
            else
            {
    out_quest ((char *) &ipr.mdn,1,0);
    out_quest ((char *) &ipr.pr_gr_stuf,2,0);
    out_quest ((char *) &ipr.kun_pr,2,0);
    out_quest ((char *) &ipr.a,3,0);
    out_quest ((char *) &ipr.vk_pr_i,3,0);
    out_quest ((char *) &ipr.vk_pr_eu,3,0);
    out_quest ((char *) &ipr.ld_pr,3,0);
    out_quest ((char *) &ipr.ld_pr_eu,3,0);
    out_quest ((char *) &ipr.aktion_nr,1,0);
    out_quest ((char *) ipr.a_akt_kz,0,2);
    out_quest ((char *) ipr.modif,0,2);
    out_quest ((char *) &ipr.waehrung,1,0);
    out_quest ((char *) &ipr.kun,2,0);
    out_quest ((char *) &ipr.a_grund,3,0);
    out_quest ((char *) ipr.kond_art,0,5);
    out_quest ((char *) &ipr.add_ek_proz,3,0);
    out_quest ((char *) &ipr.add_ek_abs,3,0);
                        cursor_gr = prepare_sql ("select "
"ipr.mdn,  ipr.pr_gr_stuf,  ipr.kun_pr,  ipr.a,  ipr.vk_pr_i,  ipr.vk_pr_eu,  "
"ipr.ld_pr,  ipr.ld_pr_eu,  ipr.aktion_nr,  ipr.a_akt_kz,  ipr.modif,  "
"ipr.waehrung,  ipr.kun,  ipr.a_grund,  ipr.kond_art,  ipr.add_ek_proz,  "
"ipr.add_ek_abs from ipr "

#line 141 "ipr.rpp"
                                  "where mdn = ? "
                                  "and   pr_gr_stuf   = ? "
                                  "and   kun_pr   = ?");
            }
            ins_quest ((char *) &ipr.mdn, 2, 0);
            ins_quest ((char *) &ipr.pr_gr_stuf, 1, 0);
            ins_quest ((char *) &ipr.kun_pr, 2, 0);
            del_gr_curs = prepare_sql ("delete from ipr "
                                  "where mdn = ? "
                                  "and   pr_gr_stuf   = ? "
                                  "and   kun_pr   = ?");
}

int IPR_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int IPR_CLASS::dbreadfirst_a (char *order)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor_a == -1)
         {
                this->prepare_a (order);
         }
         open_sql (cursor_a);
         return fetch_sql (cursor_a);
}

int IPR_CLASS::dbread_a (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         return fetch_sql (cursor_a);
}

int IPR_CLASS::dbreadfirst_gr (char *order)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor_gr == -1)
         {
                this->prepare_gr (order);
         }
         open_sql (cursor_gr);
         return fetch_sql (cursor_gr);
}

int IPR_CLASS::dbread_gr (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         return fetch_sql (cursor_gr);
}

int IPR_CLASS::dbdelete_gr (void)
{
         if (del_gr_curs == -1)
         {
                this->prepare_a (NULL);
         }
         return execute_curs (del_gr_curs);
}
       

int IPR_CLASS::dbdelete_a (void)
{
         if (del_a_curs == -1)
         {
                this->prepare_a (NULL);
         }
         return execute_curs (del_a_curs);
}
       


int IPR_CLASS::dbclose_a (void)
{
        if (cursor_a != -1)
        { 
                 close_sql (cursor_a);
                 cursor_a = -1;
        }
        return 0;
}


int IPR_CLASS::dbclose_gr (void)
{
        if (cursor_gr != -1)
        { 
                 close_sql (cursor_gr);
                 cursor_gr = -1;
        }
        return 0;
}

