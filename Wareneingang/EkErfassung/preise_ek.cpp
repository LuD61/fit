#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "a_kun.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "preise_ek.h" 

struct PREISE_EK preise_ek, preise_ek_null;

void PREISE_EK_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &preise_ek.mdn, 1, 0);
            ins_quest ((char *) &preise_ek.dat, 2, 0);
            ins_quest ((char *) &preise_ek.a, 3, 0);
            ins_quest ((char *) &preise_ek.rowid, 2, 0);
    out_quest ((char *) &preise_ek.mdn,1,0);
    out_quest ((char *) &preise_ek.dat,2,0);
    out_quest ((char *) &preise_ek.gue_ab,2,0);
    out_quest ((char *) &preise_ek.a,3,0);
    out_quest ((char *) &preise_ek.preis,3,0);
    out_quest ((char *) &preise_ek.me_einh,1,0);
    out_quest ((char *) &preise_ek.delstatus,1,0);
    out_quest ((char *) &preise_ek.posi,2,0);
    out_quest ((char *) &preise_ek.sk,3,0);
    out_quest ((char *) &preise_ek.fil_ek,3,0);
            cursor = prepare_sql ("select preise_ek.mdn,  "
"preise_ek.dat,  preise_ek.gue_ab,  preise_ek.a,  preise_ek.preis,  "
"preise_ek.me_einh,  preise_ek.delstatus,  preise_ek.posi,  "
"preise_ek.sk,  preise_ek.fil_ek from preise_ek "

#line 29 "preise_ek.rpp"
                                  "where mdn = ? "
                                  "and   dat = ? "
                                  "and   a = ?"
								   "and rowid = ?");

    ins_quest ((char *) &preise_ek.mdn,1,0);
    ins_quest ((char *) &preise_ek.dat,2,0);
    ins_quest ((char *) &preise_ek.gue_ab,2,0);
    ins_quest ((char *) &preise_ek.a,3,0);
    ins_quest ((char *) &preise_ek.preis,3,0);
    ins_quest ((char *) &preise_ek.me_einh,1,0);
    ins_quest ((char *) &preise_ek.delstatus,1,0);
    ins_quest ((char *) &preise_ek.posi,2,0);
    ins_quest ((char *) &preise_ek.sk,3,0);
    ins_quest ((char *) &preise_ek.fil_ek,3,0);
            sqltext = "update preise_ek set "
"preise_ek.mdn = ?,  preise_ek.dat = ?,  preise_ek.gue_ab = ?,  "
"preise_ek.a = ?,  preise_ek.preis = ?,  preise_ek.me_einh = ?,  "
"preise_ek.delstatus = ?,  preise_ek.posi = ?,  preise_ek.sk = ?,  "
"preise_ek.fil_ek = ? "

#line 35 "preise_ek.rpp"
                                  "where mdn = ? "
                                  "and   dat = ? "
                                  "and   a = ?";


            ins_quest ((char *) &preise_ek.mdn, 1, 0);
            ins_quest ((char *) &preise_ek.dat, 2, 0);
            ins_quest ((char *) &preise_ek.a, 3, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &preise_ek.mdn, 1, 0);
            ins_quest ((char *) &preise_ek.dat, 2, 0);
            ins_quest ((char *) &preise_ek.a, 3, 0);
            test_upd_cursor = prepare_sql ("select a from preise_ek "
                                  "where mdn = ? "
                                  "and   dat = ? "
                                  "and   a = ?");

            ins_quest ((char *) &preise_ek.mdn, 1, 0);
            ins_quest ((char *) &preise_ek.dat, 2, 0);
            ins_quest ((char *) &preise_ek.a, 3, 0);
            del_cursor = prepare_sql ("delete from preise_ek "
                                  "where mdn = ? "
                                  "and   dat = ? "
                                  "and   a = ?");

            ins_quest ((char *) &preise_ek.mdn, 1, 0);
            ins_quest ((char *) &preise_ek.dat, 2, 0);
            del_best_curs = prepare_sql ("delete from preise_ek "
                                         "where mdn = ? "
                                         "and   dat = ? ");

    ins_quest ((char *) &preise_ek.mdn,1,0);
    ins_quest ((char *) &preise_ek.dat,2,0);
    ins_quest ((char *) &preise_ek.gue_ab,2,0);
    ins_quest ((char *) &preise_ek.a,3,0);
    ins_quest ((char *) &preise_ek.preis,3,0);
    ins_quest ((char *) &preise_ek.me_einh,1,0);
    ins_quest ((char *) &preise_ek.delstatus,1,0);
    ins_quest ((char *) &preise_ek.posi,2,0);
    ins_quest ((char *) &preise_ek.sk,3,0);
    ins_quest ((char *) &preise_ek.fil_ek,3,0);
            ins_cursor = prepare_sql ("insert into preise_ek ("
"mdn,  dat,  gue_ab,  a,  preis,  me_einh,  delstatus,  posi,  sk,  fil_ek) "

#line 68 "preise_ek.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?)");

#line 70 "preise_ek.rpp"
}

void PREISE_EK_CLASS::prepare_o (char *order)
{

            ins_quest ((char *) &preise_ek.mdn, 1, 0);
            ins_quest ((char *) &preise_ek.dat, 2, 0);
            if (order)
            {
    out_quest ((char *) &preise_ek.mdn,1,0);
    out_quest ((char *) &preise_ek.dat,2,0);
    out_quest ((char *) &preise_ek.gue_ab,2,0);
    out_quest ((char *) &preise_ek.a,3,0);
    out_quest ((char *) &preise_ek.preis,3,0);
    out_quest ((char *) &preise_ek.me_einh,1,0);
    out_quest ((char *) &preise_ek.delstatus,1,0);
    out_quest ((char *) &preise_ek.posi,2,0);
    out_quest ((char *) &preise_ek.sk,3,0);
    out_quest ((char *) &preise_ek.fil_ek,3,0);
                    cursor_o = prepare_sql ("select "
"preise_ek.mdn,  preise_ek.dat,  preise_ek.gue_ab,  preise_ek.a,  "
"preise_ek.preis,  preise_ek.me_einh,  preise_ek.delstatus,  "
"preise_ek.posi,  preise_ek.sk,  preise_ek.fil_ek from preise_ek "

#line 80 "preise_ek.rpp"
                                          "where mdn = ? "
                                          "and   dat = ? %s", order);
            }
            else
            {
    out_quest ((char *) &preise_ek.mdn,1,0);
    out_quest ((char *) &preise_ek.dat,2,0);
    out_quest ((char *) &preise_ek.gue_ab,2,0);
    out_quest ((char *) &preise_ek.a,3,0);
    out_quest ((char *) &preise_ek.preis,3,0);
    out_quest ((char *) &preise_ek.me_einh,1,0);
    out_quest ((char *) &preise_ek.delstatus,1,0);
    out_quest ((char *) &preise_ek.posi,2,0);
    out_quest ((char *) &preise_ek.sk,3,0);
    out_quest ((char *) &preise_ek.fil_ek,3,0);
                    cursor_o = prepare_sql ("select "
"preise_ek.mdn,  preise_ek.dat,  preise_ek.gue_ab,  preise_ek.a,  "
"preise_ek.preis,  preise_ek.me_einh,  preise_ek.delstatus,  "
"preise_ek.posi,  preise_ek.sk,  preise_ek.fil_ek from preise_ek "

#line 86 "preise_ek.rpp"
                                          "where mdn = ? "
                                          "and   dat = ? ");
           }
}

int PREISE_EK_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int PREISE_EK_CLASS::dbreadfirst_o (char *order)
/**
Ersten Satz aus Tabelle lesen.
**/
{

         if (cursor == -1)
         {
                this->prepare ();
         }
         if (cursor_o == -1)
         {
                this->prepare_o (order);
         }
         open_sql (cursor_o);
         fetch_sql (cursor_o);
         return (sqlstatus);
}

int PREISE_EK_CLASS::dbread_o ()
/**
Ersten Satz aus Tabelle lesen.
**/
{

         fetch_sql (cursor_o);
         return (sqlstatus);
}

int PREISE_EK_CLASS::dbdeletebest ()
/**
Alles Positionen einer Bestellung loeschen.
**/
{
         if (del_best_curs == -1)
         {
                     prepare ();
         }
         return execute_curs (del_best_curs);
}

int PREISE_EK_CLASS::dbclose_o ()
/**
Cursor schliessen.
**/
{
         if (del_best_curs)
         {
                      close_sql (del_best_curs);
                      del_best_curs = -1;
         }
         this->DB_CLASS::dbclose ();
         return 0;
}
int PREISE_EK_CLASS::dbinsert (void)
/**
In preise_ek einfuegen.
**/
{
        int dsqlstatus;
        if (ins_cursor == -1)
        {
                       prepare ();
        }
        dsqlstatus = execute_curs (ins_cursor);
        return dsqlstatus;
}


