#ifndef _FIL_INV_DEF
#define _FIL_INV_DEF

#include "dbclass.h"

struct PREISE_EK {
   short     mdn;
   long      dat;
   long      gue_ab;
   double    a;
   double    preis;
   short     me_einh;
   short     delstatus;
   long      posi;
   double    sk;
   double    fil_ek;
   long rowid;
};
extern struct PREISE_EK preise_ek, preise_ek_null;

#line 7 "preise_ek.rh"

class PREISE_EK_CLASS : public DB_CLASS
{
       private :
               int cursor_o;
               int del_best_curs;
               void prepare (void);
               void prepare_o (char *);
       public :
               PREISE_EK_CLASS () : DB_CLASS (), cursor_o (-1), del_best_curs (-1)
               {
               }
               int dbreadfirst (void);
               int dbreadfirst_o (char *);
               int dbread_o (void);
               int dbdeletebest (void);
               int dbclose (void);
               int dbclose_o (void);
               int dbinsert (void);

};
#endif
