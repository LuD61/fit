#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "a_kun.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "a_kalkn.h"

struct A_KALKN a_kalkn, a_kalkn_null;

void A_KALKN_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &a_kalkn.mdn, 1, 0);
            ins_quest ((char *) &a_kalkn.fil, 1, 0);
            ins_quest ((char *) &a_kalkn.a, 3, 0);
            ins_quest ((char *) &a_kalkn.gue_ab, 2, 0);
    out_quest ((char *) &a_kalkn.mdn,1,0);
    out_quest ((char *) &a_kalkn.fil,1,0);
    out_quest ((char *) &a_kalkn.a,3,0);
    out_quest ((char *) &a_kalkn.gue_ab,2,0);
    out_quest ((char *) &a_kalkn.ek,3,0);
    out_quest ((char *) &a_kalkn.sk,3,0);
    out_quest ((char *) &a_kalkn.fil_ek,3,0);
            cursor = prepare_sql ("select a_kalkn.mdn,  "
"a_kalkn.fil,  a_kalkn.a,  a_kalkn.gue_ab,  a_kalkn.ek,  a_kalkn.sk,  "
"a_kalkn.fil_ek from a_kalkn "

#line 29 "a_kalkn.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   a = ?"
								   "and gue_ab = ?");

    ins_quest ((char *) &a_kalkn.mdn,1,0);
    ins_quest ((char *) &a_kalkn.fil,1,0);
    ins_quest ((char *) &a_kalkn.a,3,0);
    ins_quest ((char *) &a_kalkn.gue_ab,2,0);
    ins_quest ((char *) &a_kalkn.ek,3,0);
    ins_quest ((char *) &a_kalkn.sk,3,0);
    ins_quest ((char *) &a_kalkn.fil_ek,3,0);
            sqltext = "update a_kalkn set a_kalkn.mdn = ?,  "
"a_kalkn.fil = ?,  a_kalkn.a = ?,  a_kalkn.gue_ab = ?,  a_kalkn.ek = ?,  "
"a_kalkn.sk = ?,  a_kalkn.fil_ek = ? "

#line 35 "a_kalkn.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   a = ? "
                                  "and   gue_ab = ?";


            ins_quest ((char *) &a_kalkn.mdn, 1, 0);
            ins_quest ((char *) &a_kalkn.fil, 1, 0);
            ins_quest ((char *) &a_kalkn.a, 3, 0);
            ins_quest ((char *) &a_kalkn.gue_ab, 2, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &a_kalkn.mdn, 1, 0);
            ins_quest ((char *) &a_kalkn.fil, 1, 0);
            ins_quest ((char *) &a_kalkn.a, 3, 0);
            ins_quest ((char *) &a_kalkn.gue_ab, 2, 0);
            test_upd_cursor = prepare_sql ("select a from a_kalkn "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   a = ? "
                                  "and   gue_ab = ?");

            ins_quest ((char *) &a_kalkn.mdn, 1, 0);
            ins_quest ((char *) &a_kalkn.fil, 1, 0);
            ins_quest ((char *) &a_kalkn.a, 3, 0);
            ins_quest ((char *) &a_kalkn.gue_ab, 2, 0);
            del_cursor = prepare_sql ("delete from a_kalkn "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   a = ? "
                                  "and   gue_ab = ?");

    ins_quest ((char *) &a_kalkn.mdn,1,0);
    ins_quest ((char *) &a_kalkn.fil,1,0);
    ins_quest ((char *) &a_kalkn.a,3,0);
    ins_quest ((char *) &a_kalkn.gue_ab,2,0);
    ins_quest ((char *) &a_kalkn.ek,3,0);
    ins_quest ((char *) &a_kalkn.sk,3,0);
    ins_quest ((char *) &a_kalkn.fil_ek,3,0);
            ins_cursor = prepare_sql ("insert into a_kalkn ("
"mdn,  fil,  a,  gue_ab,  ek,  sk,  fil_ek) "

#line 68 "a_kalkn.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?)");

#line 70 "a_kalkn.rpp"
}


int A_KALKN_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int A_KALKN_CLASS::dbinsert (void)
/**
In a_kalkn einfuegen.
**/
{
        int dsqlstatus;
        if (ins_cursor == -1)
        {
                       prepare ();
        }
        dsqlstatus = execute_curs (ins_cursor);
        return dsqlstatus;
}


