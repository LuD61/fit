#ifndef _A_BEST_DEF
#define _A_BEST_DEF

#include "windows.h"
#include "mo_draw.h"
#include "wmask.h"
#include "dbclass.h"

struct A_BEST {
   double    a;
   short     best_anz;
   char      best_auto[2];
   double    bsd_min;
   double    bsd_min_emp;
   double    bsd_max_emp;
   short     delstatus;
   char      erf_gew_kz[2];
   short     fil;
   char      letzt_lief_nr[17];
   char      lief[17];
   char      lief_best[17];
   long      lief_s;
   short     mdn;
   short     me_einh_ek;
   char      umk_kz[2];
};
extern struct A_BEST a_best, a_best_null;

#line 10 "a_best.rh"

class A_BEST_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               A_BEST_CLASS () : DB_CLASS ()
               {
               }
               ~A_BEST_CLASS ()
               {
                   dbclose ();
               } 
               int dbreadfirst (void);
};
#endif
