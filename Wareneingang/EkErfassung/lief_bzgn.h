#ifndef _LIEF_BZGN_DEF
#define _LIEF_BZGN_DEF

#include "dbclass.h"

struct LIEF_BZGN {
   double    a;
   short     fil;
   char      lief[17];
   char      lief_best[17];
   long      lief_s;
   short     mdn;
   double    pr_ek;
   long      dat;
   char      zeit[7];
   double    pr_ek_eur;
};
extern struct LIEF_BZGN lief_bzgn, lief_bzgn_null;

#line 7 "lief_bzgn.rh"

class LIEF_BZGN_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
};
#endif
