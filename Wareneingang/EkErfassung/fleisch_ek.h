#ifndef _FLEISCH_EK_DEF
#define _FLEISCH_EK_DEF

#include "dbclass.h"

struct FLEISCH_EK {
   short     mdn;
   long      dat;
   long      gue_ab;
   double    a;
   double    preis;
   double    sk;
   double    fil_ek;
   short     me_einh;
   short     delstatus;
   long      posi;
   long 	rowid;
};
extern struct FLEISCH_EK fleisch_ek, fleisch_ek_null;

#line 7 "fleisch_ek.rh"

class FLEISCH_EK_CLASS : public DB_CLASS
{
       private :
               int cursor_o;
               int del_best_curs;
               void prepare (void);
               void prepare_o (char *);
       public :
               FLEISCH_EK_CLASS () : DB_CLASS (), cursor_o (-1), del_best_curs (-1)
               {
               }
               int dbreadfirst (void);
               int dbreadfirst_o (char *);
               int dbread_o (void);
               int dbdeletebest (void);
               int dbclose (void);
               int dbclose_o (void);
               int dbinsert (void);

};
#endif
