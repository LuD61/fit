#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include "itemc.h"
#include "wmaskc.h"
#include "dbfunc.h"
#include "a_bas.h"
#include "preise_ek.h"
#include "lief.h"
#include "mo_meld.h"
#include "mo_qa.h"

static ITEM iOK ("", "     OK     ", "", 0);
static ITEM iCA ("", "  Abbrechen ", "", 0);

//static A_PREISM_CLASS  bestk_class;

static int testquery (void)
/**
Abfrage in Query-Eingabe.
**/
{
        switch (syskey)
        {
                case KEY5 :
                       break_enter ();
                       return 1;
                case KEY11 :
                case KEY12 :
                       syskey = KEY12;
                       break_enter ();
                       return 1;
        }
		if (syskey == KEYCR)
		{
		        if (current_form->mask[currentfield].BuId == KEY12)
				{
					syskey = KEY12;
					break_enter ();
					return 1;
				}
		        else if (current_form->mask[currentfield].BuId == KEY5)
				{
					syskey = KEY5;
					break_enter ();
					return 1;
				}
		}
        return 0;
}


int QueryClass::querya (HWND hWnd)
/**
Query ueber Textnummer.
**/
{

        HANDLE hMainInst;

        static char aval[41];
        static char a_bz1val[41];
        static char hwgval[41];
        static char wgval[41];
        static char agval[41];
        static char a_typval[41];

        static ITEM iaval ("a", 
                           aval, 
                             "Artikel-Nr.......:", 
                           0);

        static ITEM ia_bz1val ("a_bz1", 
                                a_bz1val, 
                             "Bezeichnung......:", 
                                   0);
        static ITEM ihwgval ("hwg", 
                             hwgval, 
                             "Hauptwarengruppe.:", 
                             0);

        static ITEM iwgval ("wg", 
                            wgval, 
                             "Warengruppe......:", 

                            0);
        static ITEM iagval ("ag", 
                            agval, 
                             "Artikelgruppe....:", 
                             0);


        static ITEM ia_typval ("a_typ", 
                                a_typval, 
                             "Artikel-Typ......:", 
                                   0);

        static field _qtxtform[] = {
           &iaval,      40, 0, 1,1, 0, "", NORMAL, 0, testquery,0,
           &ia_bz1val,  40, 0, 2,1, 0, "", NORMAL, 0, testquery,0,
           &ihwgval,    40, 0, 3,1, 0, "", NORMAL, 0, testquery,0,
           &iwgval,     40, 0, 4,1, 0, "", NORMAL, 0, testquery,0,
           &iagval,     40, 0, 5,1, 0, "", NORMAL, 0, testquery,0,
           &ia_typval,  40, 0, 6,1, 0, "", NORMAL, 0, testquery,0,
           &iOK,        15, 0, 8,15, 0, "", BUTTON, 0,testquery ,KEY12,
           &iCA,        15, 0, 8,32, 0, "", BUTTON, 0,testquery ,KEY5,
		};

        static form qtxtform = {8, 0, 0, _qtxtform, 
			                    0, 0, 0, 0, NULL};
        
        static char *qnamen[] = {"a", "a_bz1", "hwg", "wg", "ag",
                                 "a_typ",
                                  NULL};

        HWND query;
		int savefield;
		form *savecurrent;
        
        hMainInst = (HANDLE) GetWindowLong (hWnd, GWL_HINSTANCE);	
        save_fkt (5);
		save_fkt (6);
		save_fkt (7);
		save_fkt (8);
		save_fkt (11);
		save_fkt (12);

        set_fkt (NULL, 6);
        set_fkt (NULL, 8);
        set_fkt (NULL, 7);
        set_fkt (NULL, 9);

        set_fkt (testquery, 5);
        set_fkt (testquery, 11);
        set_fkt (testquery, 12);

		savefield = currentfield;
		savecurrent = current_form;
        SetAktivWindow (hWnd);
		SetButtonTab (TRUE);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_CAPTION);
        query = OpenWindowChC (11, 62, 9, 10, hMainInst,
                               "Suchkriterien f�r Artikel");
        enter_form (query, &qtxtform, 0, 0);

		SetButtonTab (FALSE);
        CloseControls (&qtxtform);
        DestroyWindow (query);
		SetAktivWindow (hWnd);
        if (syskey != KEY5 && syskey != KEY10) 
        {
                  Preparea_basQuery (&qtxtform, qnamen);

                  Showa_basQuery (hWnd, 1);
        }

		currentfield = savefield;
		restore_fkt (5);
        restore_fkt (11);
        restore_fkt (12);
        restore_fkt (6);
        restore_fkt (7);
        restore_fkt (8);
        if (syskey == KEY5) return FALSE; 
        return TRUE;
}


int QueryClass::querya_direct (HWND hWnd, char *a_bz1)
/**
Query ueber Textnummer.
**/
{

        HANDLE hMainInst;

        static char a_bz1val[41];

        static ITEM ia_bz1val ("a_bz1", 
                                a_bz1val, 
                             "Bezeichnung......:", 
                                   0);

        static field _qtxtform[] = {
           &ia_bz1val,  40, 0, 2,1, 0, "", NORMAL, 0, testquery,0,
		};

        static form qtxtform = {1, 0, 0, _qtxtform, 
			                    0, 0, 0, 0, NULL};
        
        static char *qnamen[] = {"a_bz1",
                                  NULL};

 	    int savefield;
		form *savecurrent;
        
        hMainInst = (HANDLE) GetWindowLong (hWnd, GWL_HINSTANCE);	
		save_fkt (6);
		save_fkt (7);
		save_fkt (8);
		save_fkt (11);
		save_fkt (12);

        set_fkt (NULL, 6);
        set_fkt (NULL, 8);
        set_fkt (NULL, 7);
        set_fkt (NULL, 9);


		savefield = currentfield;
		savecurrent = current_form;
        SetAktivWindow (hWnd);

		SetAktivWindow (hWnd);
		strcpy (a_bz1val, a_bz1);
        Preparea_basQuery_Bez (&qtxtform, qnamen);

        Showa_basQuery_Bez (hWnd, 1);
		currentfield = savefield;
		strcpy (a_bz1val, "");
        restore_fkt (11);
        restore_fkt (12);
        restore_fkt (6);
        restore_fkt (7);
        restore_fkt (8);
        if (syskey == KEY5)
		{
			strcpy (a_bz1, "0");
			return FALSE;
		}
		sprintf (a_bz1, "%13.0lf", _a_bas.a);
        return TRUE;
}

int QueryClass::searcha_direct (HWND hWnd, char *a_bz1)
/**
Query ueber Textnummer.
**/
{
 	    int savefield;
		form *savecurrent;

        HANDLE hMainInst;

        
        hMainInst = (HANDLE) GetWindowLong (hWnd, GWL_HINSTANCE);	
		save_fkt (8);
		save_fkt (6);
		save_fkt (7);
		save_fkt (11);
		save_fkt (12);

        set_fkt (NULL, 8);
        set_fkt (NULL, 6);
        set_fkt (NULL, 7);
        set_fkt (NULL, 9);

        set_fkt (testquery, 11);
        set_fkt (testquery, 12);

		savefield = currentfield;
		savecurrent = current_form;
        SetAktivWindow (hWnd);
		SetAktivWindow (hWnd);

		SetCharBuffa_bas (a_bz1);
        Preparea_basQuery ();
        Showa_basQuery (hWnd, 1);

		currentfield = savefield;
        restore_fkt (11);
        restore_fkt (12);
        restore_fkt (6);
        restore_fkt (7);
        restore_fkt (8);
        if (syskey == KEY5)
		{
			strcpy (a_bz1, "0");
			return FALSE;
		}
		sprintf (a_bz1, "%13.0lf", _a_bas.a);
        return TRUE;
}


int QueryClass::searcha (HWND hWnd)
/**
Query ueber Textnummer.
**/
{

        HANDLE hMainInst;

		int savefield;
		form *savecurrent;
        
        hMainInst = (HANDLE) GetWindowLong (hWnd, GWL_HINSTANCE);	
		save_fkt (8);
        save_fkt (5);
		save_fkt (6);
		save_fkt (7);
		save_fkt (11);
		save_fkt (12);

        set_fkt (NULL, 8);
        set_fkt (NULL, 6);
        set_fkt (NULL, 7);
        set_fkt (NULL, 9);

        set_fkt (testquery, 5);
        set_fkt (testquery, 11);
        set_fkt (testquery, 12);

		savefield = currentfield;
		savecurrent = current_form;
        SetAktivWindow (hWnd);
		SetAktivWindow (hWnd);

        Preparea_basQuery ();
        Showa_basQuery (hWnd, 1);

		currentfield = savefield;
		restore_fkt (5);
        restore_fkt (11);
        restore_fkt (12);
        restore_fkt (6);
        restore_fkt (7);
        restore_fkt (8);
        if (syskey == KEY5) return FALSE; 
        return TRUE;
}

