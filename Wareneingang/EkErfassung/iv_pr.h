#ifndef _IV_PR_DEF
#define _IV_PR_DEF

#include "dbclass.h"

struct IV_PR {
   short     mdn;
   long      pr_gr_stuf;
   long      kun_pr;
   double    a;
   double    vk_pr_i;
   double    vk_pr_eu;
   double    ld_pr;
   double    ld_pr_eu;
   long      gue_ab;
   short     waehrung;
   long      kun;
};
extern struct IV_PR iv_pr, iv_pr_null;

#line 7 "iv_pr.rh"

class IV_PR_CLASS : public DB_CLASS
{
       private :
               int cursor_a;
               int cursor_gr;
               int del_a_curs;
               int del_gr_curs;
               void prepare (void);
               void prepare_a (char *);
               void prepare_gr (char *);
       public :
               IV_PR_CLASS () : DB_CLASS (),
                                  cursor_a (-1) ,
                                  cursor_gr (-1) ,
                                  del_a_curs (-1) ,
                                  del_gr_curs (-1)
               {
               }
               int dbreadfirst (void);
               int dbreadfirst_a (char *);
               int dbread_a (void);
               int dbreadfirst_gr (char *);
               int dbread_gr (void);
               int dbdelete_a (void);
               int dbdelete_gr (void);
               int dbclose_a (void);
               int dbclose_gr (void);
};
#endif
