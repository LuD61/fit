#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "a_pr.h"

struct A_PR a_pr, a_pr_null;

void A_PR_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &a_pr.a, 3, 0);
            ins_quest ((char *) &a_pr.mdn_gr, 1, 0);
            ins_quest ((char *) &a_pr.mdn, 1, 0);
            ins_quest ((char *) &a_pr.fil_gr, 1, 0);
            ins_quest ((char *) &a_pr.fil, 1, 0);
    out_quest ((char *) &a_pr.a,3,0);
    out_quest ((char *) &a_pr.akt,1,0);
    out_quest ((char *) &a_pr.delstatus,1,0);
    out_quest ((char *) &a_pr.fil,1,0);
    out_quest ((char *) &a_pr.fil_gr,1,0);
    out_quest ((char *) &a_pr.key_typ_dec13,3,0);
    out_quest ((char *) &a_pr.key_typ_sint,1,0);
    out_quest ((char *) a_pr.lad_akv,0,2);
    out_quest ((char *) a_pr.lief_akv,0,2);
    out_quest ((char *) &a_pr.mdn,1,0);
    out_quest ((char *) &a_pr.mdn_gr,1,0);
    out_quest ((char *) a_pr.modif,0,2);
    out_quest ((char *) &a_pr.pr_ek,3,0);
    out_quest ((char *) &a_pr.pr_vk,3,0);
    out_quest ((char *) &a_pr.bearb,2,0);
    out_quest ((char *) a_pr.pers_nam,0,9);
    out_quest ((char *) &a_pr.pr_vk1,3,0);
    out_quest ((char *) &a_pr.pr_vk2,3,0);
    out_quest ((char *) &a_pr.pr_vk3,3,0);
    out_quest ((char *) &a_pr.pr_ek_euro,3,0);
    out_quest ((char *) &a_pr.pr_vk_euro,3,0);
            cursor = prepare_sql ("select a_pr.a,  a_pr.akt,  "
"a_pr.delstatus,  a_pr.fil,  a_pr.fil_gr,  a_pr.key_typ_dec13,  "
"a_pr.key_typ_sint,  a_pr.lad_akv,  a_pr.lief_akv,  a_pr.mdn,  "
"a_pr.mdn_gr,  a_pr.modif,  a_pr.pr_ek,  a_pr.pr_vk,  a_pr.bearb,  "
"a_pr.pers_nam,  a_pr.pr_vk1,  a_pr.pr_vk2,  a_pr.pr_vk3,  "
"a_pr.pr_ek_euro,  a_pr.pr_vk_euro from a_pr "

#line 29 "a_pr.rpp"
                                  "where   a   = ? "
                                  "and   mdn_gr   = ? "
                                  "and   mdn   = ? "
                                  "and   fil_gr   = ? "
                                  "and   fil = ? "
                                  );



    ins_quest ((char *) &a_pr.a,3,0);
    ins_quest ((char *) &a_pr.akt,1,0);
    ins_quest ((char *) &a_pr.delstatus,1,0);
    ins_quest ((char *) &a_pr.fil,1,0);
    ins_quest ((char *) &a_pr.fil_gr,1,0);
    ins_quest ((char *) &a_pr.key_typ_dec13,3,0);
    ins_quest ((char *) &a_pr.key_typ_sint,1,0);
    ins_quest ((char *) a_pr.lad_akv,0,2);
    ins_quest ((char *) a_pr.lief_akv,0,2);
    ins_quest ((char *) &a_pr.mdn,1,0);
    ins_quest ((char *) &a_pr.mdn_gr,1,0);
    ins_quest ((char *) a_pr.modif,0,2);
    ins_quest ((char *) &a_pr.pr_ek,3,0);
    ins_quest ((char *) &a_pr.pr_vk,3,0);
    ins_quest ((char *) &a_pr.bearb,2,0);
    ins_quest ((char *) a_pr.pers_nam,0,9);
    ins_quest ((char *) &a_pr.pr_vk1,3,0);
    ins_quest ((char *) &a_pr.pr_vk2,3,0);
    ins_quest ((char *) &a_pr.pr_vk3,3,0);
    ins_quest ((char *) &a_pr.pr_ek_euro,3,0);
    ins_quest ((char *) &a_pr.pr_vk_euro,3,0);
            sqltext = "update a_pr set a_pr.a = ?,  "
"a_pr.akt = ?,  a_pr.delstatus = ?,  a_pr.fil = ?,  a_pr.fil_gr = ?,  "
"a_pr.key_typ_dec13 = ?,  a_pr.key_typ_sint = ?,  a_pr.lad_akv = ?,  "
"a_pr.lief_akv = ?,  a_pr.mdn = ?,  a_pr.mdn_gr = ?,  a_pr.modif = ?,  "
"a_pr.pr_ek = ?,  a_pr.pr_vk = ?,  a_pr.bearb = ?,  a_pr.pers_nam = ?,  "
"a_pr.pr_vk1 = ?,  a_pr.pr_vk2 = ?,  a_pr.pr_vk3 = ?,  "
"a_pr.pr_ek_euro = ?,  a_pr.pr_vk_euro = ? "

#line 39 "a_pr.rpp"
                                  "where   a   = ? "
                                  "and   mdn_gr   = ? "
                                  "and   mdn   = ? "
                                  "and   fil_gr   = ? "
                                  "and   fil = ? ";

            ins_quest ((char *) &a_pr.a, 3, 0);
            ins_quest ((char *) &a_pr.mdn_gr, 1, 0);
            ins_quest ((char *) &a_pr.mdn, 1, 0);
            ins_quest ((char *) &a_pr.fil_gr, 1, 0);
            ins_quest ((char *) &a_pr.fil, 1, 0);

            upd_cursor = prepare_sql (sqltext);

    ins_quest ((char *) &a_pr.a,3,0);
    ins_quest ((char *) &a_pr.akt,1,0);
    ins_quest ((char *) &a_pr.delstatus,1,0);
    ins_quest ((char *) &a_pr.fil,1,0);
    ins_quest ((char *) &a_pr.fil_gr,1,0);
    ins_quest ((char *) &a_pr.key_typ_dec13,3,0);
    ins_quest ((char *) &a_pr.key_typ_sint,1,0);
    ins_quest ((char *) a_pr.lad_akv,0,2);
    ins_quest ((char *) a_pr.lief_akv,0,2);
    ins_quest ((char *) &a_pr.mdn,1,0);
    ins_quest ((char *) &a_pr.mdn_gr,1,0);
    ins_quest ((char *) a_pr.modif,0,2);
    ins_quest ((char *) &a_pr.pr_ek,3,0);
    ins_quest ((char *) &a_pr.pr_vk,3,0);
    ins_quest ((char *) &a_pr.bearb,2,0);
    ins_quest ((char *) a_pr.pers_nam,0,9);
    ins_quest ((char *) &a_pr.pr_vk1,3,0);
    ins_quest ((char *) &a_pr.pr_vk2,3,0);
    ins_quest ((char *) &a_pr.pr_vk3,3,0);
    ins_quest ((char *) &a_pr.pr_ek_euro,3,0);
    ins_quest ((char *) &a_pr.pr_vk_euro,3,0);
            ins_cursor = prepare_sql ("insert into a_pr (a,  "
"akt,  delstatus,  fil,  fil_gr,  key_typ_dec13,  key_typ_sint,  lad_akv,  "
"lief_akv,  mdn,  mdn_gr,  modif,  pr_ek,  pr_vk,  bearb,  pers_nam,  pr_vk1,  pr_vk2,  "
"pr_vk3,  pr_ek_euro,  pr_vk_euro) "

#line 54 "a_pr.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?)");

#line 56 "a_pr.rpp"
            ins_quest ((char *) &a_pr.a, 3, 0);
            ins_quest ((char *) &a_pr.mdn_gr, 1, 0);
            ins_quest ((char *) &a_pr.mdn, 1, 0);
            ins_quest ((char *) &a_pr.fil_gr, 1, 0);
            ins_quest ((char *) &a_pr.fil, 1, 0);
            test_upd_cursor = prepare_sql ("select a from a_pr "
                                  "where   a   = ? "
                                  "and   mdn_gr   = ? "
                                  "and   mdn   = ? "
                                  "and   fil_gr   = ? "
                                  "and   fil = ? ");

            ins_quest ((char *) &a_pr.a, 3, 0);
            ins_quest ((char *) &a_pr.mdn_gr, 1, 0);
            ins_quest ((char *) &a_pr.mdn, 1, 0);
            ins_quest ((char *) &a_pr.fil_gr, 1, 0);
            ins_quest ((char *) &a_pr.fil, 1, 0);
            del_cursor = prepare_sql ("delete from a_pr "
                                  "where   a   = ? "
                                  "and   mdn_gr   = ? "
                                  "and   mdn   = ? "
                                  "and   fil_gr   = ? "
                                  "and   fil = ? ");
}



int A_PR_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

