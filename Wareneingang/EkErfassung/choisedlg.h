#ifndef _CHOISEDLG_DEF
#define _CHOISEDLG_DEF
#include "dlg.h"
#include "itprog.h"
#include "itfont.h"
#include "formfield.h"
#include "cmask.h"

class ChoiseDlg : virtual public DLG 
{
          private :
            static  mfont dlgfont;
            static  mfont dlgposfont;
           static mfont *Font;
		   BOOL Exclusive;
           DLG *Parent; 

          public :

            void SetExclusive (BOOL b)
			{
				Exclusive = b;
			}

	        static COLORREF SysBkColor;

            ChoiseDlg (int, int, int, int, char *, int, BOOL);
            ~ChoiseDlg ();

 	        void Init (int, int, int, int, char *, int, BOOL);
            void SetParent (DLG *Parent)
            {
                 this->Parent = Parent;
            }
            DLG *GetParent (void)
            {
                 return Parent;
            } 
            
//            BOOL OnDestroy (HWND,UINT,WPARAM,LPARAM);
            BOOL OnKey5 (void);
            BOOL OnKey12 (void);
            BOOL OnKeyReturn (void);
            BOOL TestCheck (HWND,UINT,WPARAM,LPARAM);
            BOOL OnCommand (HWND,UINT,WPARAM,LPARAM);
            BOOL OnSysCommand (HWND,UINT,WPARAM,LPARAM);

            void SetWinBackground (COLORREF);
            void ToClipboard (void);
            void FromClipboard (void);
};
#endif
