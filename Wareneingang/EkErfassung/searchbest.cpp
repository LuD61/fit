#include <windows.h>
#include "searchbest.h"
#include "strfkt.h"
#include "mo_wmess.h"
#include "lbox.h"


int SEARCHBEST::idx;
long SEARCHBEST::anz;
CHQEX *SEARCHBEST::Query = NULL;
DB_CLASS SEARCHBEST::DbClass; 
HINSTANCE SEARCHBEST::hMainInst;
HWND SEARCHBEST::hMainWindow;
HWND SEARCHBEST::awin;


int SEARCHBEST::SearchLst (char *sebuff)
/**
Nach Wert in Liste suchen.
**/
{
	   Query->SearchList ();
	   return 0;
}

int SEARCHBEST::ReadLst (char *name)
/**
Query-Liste fuellen. 
**/
{
	/**
	  char buffer [512];
	  int cursor;
	  int i;
      WMESS Wmess;
      long best_blg;
      char best_term [12];
      char lief_term [12];


	  anz = 0;
      clipped (name);
      if (strcmp (name, " ") == 0) name[0] = 0;

      if (strlen (name) == 0 ||
          name[0] <= ' ')
      {
	          sprintf (buffer, "select dat "
	 			       "from fil_invges "
                       "where mdn = %hd "
                       "and fil = %hd "
                       "and lief = %s",
                       best_kopf.mdn, best_kopf.fil, best_kopf.lief);
      }
      else
      {
	          sprintf (buffer, "select best_blg, best_term, lief_term "
	 			       "from best_kopf "
                       "where mdn = %hd "
                       "and fil = %hd "
                       "and lief = %s "
                       "%s",
                       best_kopf.mdn, best_kopf.fil, best_kopf.lief, name);
      }
      DbClass.sqlout  ((char *)  best_term, 0, 11); 
      cursor = DbClass.sqlcursor (buffer);
      if (cursor < 0) 
      {
          return -1;
      }
  	  i = 0;
//      Wmess.Message (hMainInst, hMainWindow, " \nBitte warten.....\n"
//  	                                         "Die Daten werden selektiert.\n ");  
	  while (DbClass.sqlfetch (cursor) == 0)
      {
 	      sprintf (buffer, "  |%8ld|    |%-10s|    |%-19s|", best_blg, best_term, lief_term);
	      Query->InsertRecord (buffer);
		  i ++;
      }
      anz = i;
	  DbClass.sqlclose (cursor);
// 	  Wmess.Destroy ();
	  EnableWindow (awin, FALSE);
	  ****/
	  return 0;
}

void SEARCHBEST::InsertRecord (char *buffer)
{
      if (Query == NULL) return;
      Query->InsertRecord (buffer);
}

void SEARCHBEST::SetParams (HINSTANCE hMainInst, HWND hMainWindow)
{
      this->hMainInst   = hMainInst;
      this->hMainWindow = hMainWindow;
      awin = hMainWindow;
}

BOOL SEARCHBEST::GetKey (char * key)
{
      int anz;
     
      if (idx == -1) return FALSE;
      anz = wsplit (Key, "|");
      if (anz < 1) return FALSE;
      strcpy (key, wort[1]);
      return TRUE;
}

BOOL SEARCHBEST::GetKey (char * key, int pos)
{
      int anz;
     
      if (idx == -1) return FALSE;
      anz = wsplit (Key, "|");
      if (anz <= pos) return FALSE;
      strcpy (key, wort[pos]);
      return TRUE;
}

char * SEARCHBEST::GetKey (void)
{
     
      if (idx == -1) return NULL;
      return Key;
}


void SEARCHBEST::Search (void)
{
  	  int cx, cy;
	  char buffer [256];

	  idx = -1;
      cx = 48;
      cy = 25;
      Settchar ('|');
      EnableWindow (hMainWindow, FALSE);
      Query = new CHQEX (cx, cy);
      Query->EnableSort (TRUE);
      Query->OpenWindow (hMainInst, hMainWindow);
      sprintf (buffer, "%s %s %s",
                          "%d",
                          "%s",
                          "%s");
      Query->RowAttr (buffer);
	  sprintf (buffer, " %11s  %13s %13s", "1", "1", "1"); 
	  Query->VLines (buffer, 3);
      sprintf (buffer, "3;8 17;10 33;10");
      Query->RowPos (buffer);
	  EnableWindow (awin, FALSE);

	  sprintf (buffer, " %-11s  %-13s   %-13s", "Bset.Nr.", "Best.Term.", "Lief.Term"); 
	  Query->InsertCaption (buffer);

	  Query->SetFillDb (ReadLst);
	  Query->SetSearchLst (SearchLst);

      ReadLst ("and bearb_stat <= 2");

      Query->SetSortRow (0, TRUE);
	  Query->ProcessMessages ();
      EnableWindow (hMainWindow, TRUE);
      if (syskey != KEY5)
      {
              idx = Query->GetSel ();
              if (idx != -1)
              {
                     Query->GetText (Key);
              }
      }
	  EnableWindow (awin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (awin);
}

void SEARCHBEST::Search (char *Caption, char *Vlines, char *RowPos, char *RowAttr,
                         int (*ReadLst)(char *), char *Search)
{
  	  int cx, cy;

	  Sel = LoadBitmap (hMainInst, "Sel2");
	  Msk = LoadBitmap (hMainInst, "Msk2");
	  idx = -1;
      cx = 48;
      cy = 25;
      Settchar ('|');
      EnableWindow (hMainWindow, FALSE);
      Query = new CHQEX (cx, cy);
      Query->EnableSort (TRUE);
	  Query->AddImage (Sel, Msk);
      Query->OpenWindow (hMainInst, hMainWindow, TRUE);
      Query->RowAttr (RowAttr);
	  Query->VLines (Vlines, 3);
      Query->RowPos (RowPos);
	  EnableWindow (awin, FALSE);

	  Query->InsertCaption (Caption);

	  Query->SetFillDb (ReadLst);
	  Query->SetSearchLst (SearchLst);

      ReadLst ("and bearb_stat <= 2");

      Query->SetSortRow (0, TRUE);
	  if (Search != NULL)
	  {
	       Query->SearchList (Search);
	  }
	  Query->ProcessMessages ();
      EnableWindow (hMainWindow, TRUE);
      if (syskey != KEY5)
      {
              idx = Query->GetSel ();
              if (idx != -1)
              {
                     Query->GetText (Key);
              }
      }
	  EnableWindow (awin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (awin);
}

