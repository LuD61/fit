#ifndef _MO_CHQ_DEF
#define _MO_CHQ_DEF
#include "v_key.h"


class CHQ
{
          private :
			  HWND hMainWindow;
			  HINSTANCE hInstance;
			  HWND hWnd;
              mfont *Font;
			  V_KEY Vkeys[20];
			  int vkidx;
			  DWORD currentfield;
                    static int listpos;
			  static int (*OkFunc) (int); 
			  static int (*OkFuncE) (char *); 
			  static int (*DialFunc) (int); 
              static int (*FillDb) (char *); 
			  static char EBuff [80];
              int cx, cy;
              char Dialtext [21];
          public :
			  CHQ (int, int, char *, char *);
			  CHQ (int, int);
			  CHQ (int, int, char *);
			  ~CHQ ();
			  int GetListpos (void)
			  {
				  return listpos;
			  }
 		      void ProcessMessages (void);
              static void ShowDlg (HDC, form *);
              static CALLBACK CProc(HWND,UINT, WPARAM,LPARAM);
			  static void SetOkFunc (int (*) (int));
			  static void SetOkFuncE (int (*) (char *));
			  static void SetDialFunc (int (*) (int));
			  static void SetFillDb (int (*) (char *));
              static int  GetSel (void);
              static void  SetSel (int);
              static void MoveWindow (void);
              void AddAccelerator (int, int, int (*) (void));
              void OpenWindow (HANDLE, HWND);
              void DestroyWindow (void);
			  void SetCurrentID (DWORD);
			  void SetCurrentName (char *);
			  void EnableID (DWORD, BOOL);
			  void EnableName (char *, BOOL);
              void EnableSort (BOOL);
			  void CheckID (DWORD, BOOL);
			  void CheckName (char *, BOOL);
              void VLines (char *, int);
			  void InsertCaption (char *);
			  void InsertRecord  (char *);
			  void UpdateRecord  (char *, int);
              void GetText (char *);
              BOOL TestButtons (HWND);
              BOOL TestEdit (HWND);
              BOOL IsVKey (int);
              BOOL TestVKeys (int);
              void NextField (void);
              void PriorField (void);
};
#endif

