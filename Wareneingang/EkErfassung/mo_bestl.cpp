#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include "comcthlp.h"
#include "itemc.h"
#include "wmaskc.h"
#include "message.h"
#include "mo_meld.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_intp.h"
#include "listcl.h"
#include "inflib.h"
#include "mo_arg.h"
#include "mo_curso.h"
#include "a_hndw.h"
#include "a_bas.h"
#include "fleisch_ek.h"
#include "ptab.h"
#include "mo_bestl.h"
#include "mo_qa.h"
#include "mdn.h"
#include "fil.h"
#include "sys_par.h"
#include "mo_einh.h"
#include "mo_nr.h"
#include "mo_menu.h"
#include "mo_progcfg.h"
#include "mo_auto.h"
#include "auto_nr.h"
#include "lief_bzgn.h"
#include "a_kalkn.h"
//#include "AktionDlg.h"


#define MAXLEN 40
#define MAXPOS 5000
#define LPLUS 1

#define MAXME 99999.99
#define MAXPR 9999.99

#define TLEN 60

#define HNDW 1
#define EIG 2
#define EIG_DIV 3

extern HANDLE  hMainInst;
extern MELDUNG Mess;

static HWND hMainWin;
static unsigned char ausgabebuffer [0x1000];
static HWND mamain1;
static HWND eWindow;
static HWND AufMehWnd = NULL;
static HWND AufMehWnd0 = NULL;
static HWND BasishWnd;
static PAINTSTRUCT aktpaint;

static COLORREF MessBkCol = DKYELLOWCOL; 
static COLORREF MessCol   = BLACKCOL; 

static ITEM iOK ("", "     OK     ", "", 0);
static ITEM iCA ("", "  Abbrechen ", "", 0);

static int ListFocus = 3;

static long akt_lager;
static long defaultdat = 0;
static long heute = 0;

static int rab_prov_kz = 0;
static int auf_wert_anz = 0;
static int a_kun_smt = 0;
static double pr_ek;
static double pr_ek_bto;
static long InvDat = 0;
static int preistest;
static double prproz_diff = 50.0;
static int  art_un_tst = 0;
static BOOL  add_me = FALSE;
static BOOL a_kum_par = 0;
static BOOL sacreate = NULL;
static HWND SaWindow = NULL;
static HWND PlusWindow = NULL;
static BOOL add;
static double aufme_old;
static BOOL pr0test = FALSE;
static BOOL textinpmode = TRUE;
static int Stndmode = 1;
static jrhstart = 70;
static jrh1 = 1900;
static jrh2 = 2000;

struct INVPOS 
{
       char posi [20];
       char a [20];
       char a_bz1 [80];
       char a_bz2 [80];
	   char last_pr[20];
	   char last_gue[20];
       char me_bz [20];
       char pr_ek [20];
       char pr_vollk [20];
       char pr_sk [20];
       char pr_fil_ek [20];
       char gue_ab [20];
       char me_einh [5];
};


bool einstieg_default;
LIEF_BZGN_CLASS Lief_bzgn;
A_KALKN_CLASS A_kalkn;
struct INVPOS invpos, invpostab [MAXPOS], invpos_null;

ITEM iposi        ("posi",       invpos.posi,          "", 0);
ITEM ia           ("a",          invpos.a,          "", 0);
ITEM ia_bz1       ("a_bz1",      invpos.a_bz1,      "", 0);
ITEM ia_bz2       ("a_bz2",      invpos.a_bz2,      "", 0);
ITEM ime_bz       ("me_bz",      invpos.me_bz,      "", 0);
ITEM ilast_pr     ("last_pr",    invpos.last_pr,    "", 0);
ITEM ipr_ek       ("pr_ek",      invpos.pr_ek,  "", 0);
ITEM ipr_sk       ("pr_sk",      invpos.pr_sk,  "", 0);
ITEM ipr_fil_ek   ("pr_fil_ek",  invpos.pr_fil_ek,  "", 0);
ITEM igue_ab     ("gue_ab",    invpos.gue_ab,  "", 0);
ITEM ilast_gue     ("last_gue", invpos.last_gue,  "", 0);


static field  _dataform[] = {
//&iposi,       10, 0, 0, 6,  0, "",       DISPLAYONLY, 0, 0, 0, 
&ia,          16, 0, 0, 6,  0, "",       DISPLAYONLY, 0, 0, 0, 
&ia_bz1,      27, 0, 0, 25, 0, "",       DISPLAYONLY, 0, 0, 0, 
//&ia_bz2,      24, 0, 0, 50, 0, "",       DISPLAYONLY, 0, 0, 0, 
&ime_bz ,     11, 0, 0, 54, 0, "",       DISPLAYONLY, 0, 0, 0,
&ilast_gue,      10, 0, 0, 65, 0, "",       DISPLAYONLY, 0, 0, 0, 
&ilast_pr,     8, 0, 0, 78, 0, "%6.4f",   DISPLAYONLY, 0, 0, 0, 
&ipr_ek,       8, 0, 0, 90, 0, "%6.4f",   EDIT, 0, 0, 0, 
&ipr_sk,       8, 0, 0, 102, 0, "%6.4f",   DISPLAYONLY, 0, 0, 0, 
&ipr_fil_ek,   8, 0, 0, 114, 0, "%6.4f",   DISPLAYONLY, 0, 0, 0, 
};


static form dataform = {8, 0, 0, _dataform, 0, 0, 0, 0, NULL}; 

static BOOL DelLadVK  = TRUE;
static BOOL DelLastMe = TRUE;

static BOOL FormOK = FALSE;

static BOOL DisplayonlyPrEK = FALSE;

/* prototyp */
short get_jahr(char *);
short get_monat(char *);

void DelFormField (form *frm, int pos)
/**
Feld aus Dataform herausnehmen.
**/
{
	         int i;
			 int diff;
			 int pos1, pos2;

			 pos1 = frm->mask[pos].pos[1];
			 if (pos < frm->fieldanz - 1)
			 {
				 pos2 = frm->mask[pos + 1].pos[1];
				 diff = max (0, pos2 - pos1);
			 }
			 else
			 {
				 diff = frm->mask[pos].length;
				 if (frm != &dataform) diff ++;
			 }
			 if (diff == 0)
			 {
				 diff = frm->mask[pos].length;
				 if (frm != &dataform) diff ++;
			 }

			 frm->fieldanz --;

			 for (i = pos; i < frm->fieldanz; i ++)
			 {
				 memcpy ((char *) &frm->mask[i], (char *) &frm->mask[i + 1], sizeof (field));
			 }

			 for (i = 0; i < frm->fieldanz; i ++)
			 {
				 if (frm->mask[i].pos[1] > pos1)
				 {
					 frm->mask[i].pos[1] -= diff;
				 }
			 }
}


static int ubrows [] = {0, 
                        1,
                        2,2,
                        3,4,5,6,7,8,9};


struct CHATTR ChAttra [] = {"a",     DISPLAYONLY, EDIT,
                             NULL,  0,           0};
struct CHATTR ChAttrlief_best [] = {"a_kun", DISPLAYONLY, EDIT,
                                     NULL,   0,           0};

struct CHATTR *ChAttr = ChAttra;

ColButton Cuposi = {  "Pos.", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};
ColButton Cusa_kz_sint = {
                     "S", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};
ColButton Cua = {
                     "Artikel-Nr", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};
ColButton Cua_bz1 = {
                     "Bezeichnung1", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};

ColButton Cua_bz2 = {
                     "Bezeichnung2", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};

ColButton Cuauf_me = {
                     "A.-Menge", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};

ColButton Cume_bz = {
                     "Best.ME", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};

ColButton Cupr_vk = {
                     "EK", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};

ColButton Cubasis_me_bz = {
                     "Basis-ME", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};


static char *EK        = "  EK    "; 
static char *EK_DM     = " EK DM  ";
static char *EK_EURO   = " EK EURO ";
static char *EK_FREMD  = " EK FREMD ";

static char *EB        = " EK.Bto "; 
static char *EB_DM     = " EB DM  ";
static char *EB_EURO   = " EB EURO ";
static char *EB_FREMD  = "EB FREMD";


//ITEM iuposi        ("posi",       "Pos ",   "", 0);
ITEM iua           ("a",          "Artikel-Nr   ",   "", 0);
ITEM iua_bz1       ("a_bz1",      "Bezeichnung  ",   "", 0);
ITEM iua_bz2       ("a_bz2",      "Bezeichnung 2",   "", 0);
ITEM iume_bz       ("me_bz",      "Einh.",         "",  0);
ITEM iupr_ek       ("pr_ek",       EK,               "", 0);
ITEM iupr_sk       ("pr_sk",       "SK",               "", 0);
ITEM iupr_fil_ek       ("pr_fil_ek", "FIL-EK",               "", 0);
ITEM iulast_pr       ("last_pr",     "lezter EK",        "", 0);
ITEM iulast_gue       ("last_gue",     "letztes Datum",        "", 0);

static field  _ubform[] = {
&iua,          18, 0, 0, 6, 0, "",  BUTTON, 0, 0, 0, 
&iua_bz1,      28, 0, 0, 24, 0, "",  BUTTON, 0, 0, 0, 
//&iua_bz2,      25, 0, 0, 49, 0, "",  BUTTON, 0, 0, 0, 
&iume_bz,      13, 0, 0, 52, 0, "",  BUTTON, 0, 0, 0,
&iulast_gue,     13, 0, 0, 64, 0, "",  BUTTON, 0, 0, 0, 
&iulast_pr,      12, 0, 0, 77 ,0,  "",  BUTTON, 0, 0, 0, 
&iupr_ek,        13, 0, 0, 89 ,0,  "",  BUTTON, 0, 0, 0, 
&iupr_sk,        12, 0, 0, 101 ,0,  "",  BUTTON, 0, 0, 0, 
&iupr_fil_ek,    12, 0, 0, 113 ,0,  "",  BUTTON, 0, 0, 0, 
};



static form ubform = {8, 0, 0, _ubform, 0, 0, 0, 0, NULL}; 

ITEM iline ("", "1", "", 0);


static field  _lineform[] = {
&iline,      1, 0, 0, 24, 0, "",  NORMAL, 0, 0, 0, 
&iline,      1, 0, 0, 52, 0, "",  NORMAL, 0, 0, 0, 
&iline,      1, 0, 0, 64, 0, "",  NORMAL, 0, 0, 0, 
&iline,      1, 0, 0, 77, 0, "",  NORMAL, 0, 0, 0, 
&iline,      1, 0, 0, 89, 0, "",  NORMAL, 0, 0, 0, 
&iline,      1, 0, 0, 101, 0, "",  NORMAL, 0, 0, 0, 
&iline,      1, 0, 0, 113, 0, "",  NORMAL, 0, 0, 0, 
&iline,      1, 0, 0, 125, 0, "",  NORMAL, 0, 0, 0, 
};

static form lineform = {8, 0, 0, _lineform, 0, 0, 0, 0, NULL}; 


static ListClassDB eListe;
PTAB_CLASS ptab_class;
HNDW_CLASS HndwClass;
SYS_PAR_CLASS sys_par_class;
static QueryClass QClass;
static FLEISCH_EK_CLASS fleisch_ek_class;
static DB_CLASS DbClass;
static EINH_CLASS einh_class;
static AutoNrClass AutoNr;
static AUTO_CLASS AutoClass;
static ADR_CLASS adr_class;

static PROG_CFG ProgCfg ("EkErfassung");
//static SMTG_CLASS Smtg;
//static PROV Prov;
static int plu_size = 4;
static int LiefBzgnSchreiben = 0;
static int lese_a_kalkpreis = 0;
static int auf_me_default= 0;
static long aufkunanz = 5;
static double auf_sk = 0.0;
static short mit_kosten = 0;
static double auf_fil_ek = 0.0;

//static StndAuf StndAuf;
static BOOL searchadirect = TRUE;
static BOOL searchmodedirect = TRUE;
static double RowHeight = 1.5;
static int UbHeight = 0;
static int bsd_kz = 1;

static BOOL NoArtMess = FALSE;

static BOOL ListColors = TRUE;
static BOOL ber_komplett = TRUE;
static COLORREF KompfColor = BLACKCOL;
static COLORREF KompbColor = GRAYCOL;
static COLORREF SafColor   = WHITECOL;
static COLORREF SabColor   = BLACKCOL;


static char LiefItem[] = {"liefa"};
static double Akta;
static BOOL best_me_pr_0;
static int preis0_mess = 1;
static double inh = 0.0;

static double akt_me;


static TEXTMETRIC textm; 

static int EnterBreak ()
{
	 break_enter ();
	 return (0);
}

static int EnterTest (void)
/**
Abfrage in Query-Eingabe.
**/
{
        switch (syskey)
        {
                case KEY5 :
                       break_enter ();
                       return 1;
                case KEY11 :
                case KEY12 :
                       syskey = KEY12;
                       break_enter ();
                       return 1;
        }
		if (syskey == KEYCR)
		{
		        if (current_form->mask[currentfield].BuId == KEY12)
				{
					syskey = KEY12;
					break_enter ();
					return 1;
				}
		        else if (current_form->mask[currentfield].BuId == KEY5)
				{
					syskey = KEY5;
					break_enter ();
					return 1;
				}
		}
        return 0;
}


void SetUbHeight (void)
/**
Hoehe der Listueberschrift setzen.
**/
{
	int i;

	for (i = 0; i < ubform.fieldanz; i ++)
	{
		ubform.mask[i].rows = UbHeight;
	}
}

static int InfoProc (char **Item, char *Value, char *where)
/**
Spezielle Procedure fuer Info-System
**/
{

    clipped (*Item);
//    if (strcmp (*Item, "a") == 0)
    {
        *Item = LiefItem;
        sprintf (where, "where mdn = %hd and "
			"            dat = %hd ", 
                         fleisch_ek.mdn, 
						 ratod (Value));
        return 1;
    }
    return 0;
}

BOOL BESTPLIST::textinpmode = TRUE;

int (*BESTPLIST::SetLief) (char *) = NULL;

void BESTPLIST::SetMessColors (COLORREF color, COLORREF bkcolor)
/**
Farben fuer Melungen setzen.
**/
{
	 MessCol   = color;
	 MessBkCol = bkcolor;
}


void BESTPLIST::SethMainWindow (HWND hMainWindow)
{
    this->hMainWindow = hMainWindow;
	hMainWin = hMainWindow;
}

BESTPLIST:: BESTPLIST ()
{
	liney = 0;
    mamainmax = 0;
    mamainmin = 0; 
    eingabesatz = NULL;
    ausgabesatz = ausgabebuffer;
    zlen = 0;
    feld_anz = 0;
    banz = 0;
    PageView = 0;
    LstZiel = NULL;
    LstSatz   = NULL;
    Lstbanz = 0;
    SwRecs = 0;
    AktRow = 0;
    AktColumn = 0;
    scrollpos = 0;
    InfoCaption[0] = (char) 0; 
    WithMenue   = 1;
    WithToolBar = 1;
	preistest = 0;
	add = FALSE;
	aufme_old = (double) 0.0;
    dataform.after  = WriteRow; 
    dataform.before = SetRowItem; 
    dataform.mask[0].before = Savea; 
    dataform.mask[0].after  = fetcha; 
    dataform.mask[5].after  = rechnepreis; 
//    dataform.mask[3].before = setkey9me; 
//    dataform.mask[3].after  = testme; 
    ListAktiv = 0;
    inh = (double) 0.0;
    this->hMainWindow = NULL;
	hMainWin = NULL;
}

void BESTPLIST::SetLager (long lgr)
{
	akt_lager = lgr;
}

void BESTPLIST::SetHeute (long ddat)
{
	heute = ddat;
}


void BESTPLIST::SetChAttr (int ca)
{
    switch (ca)
    {
          case 0 :
              ChAttr = ChAttra;
              break;
          case 1:
              ChAttr = ChAttrlief_best;
              break;
    }
}

void BESTPLIST::SetFieldAttr (char *fname, int attr)
{
         int i;
         char *feldname;

         for (i = 0; i < dataform.fieldanz; i ++)
         {
             feldname = dataform.mask[i].item->GetItemName ();
             if (feldname && strcmp (feldname, fname) == 0)
             {
                   break;
             }
         }
         if (i == dataform.fieldanz) return;

         dataform.mask[i].attribut = attr;
}

int BESTPLIST::GetFieldAttr (char *fname)
{
         int i;
         char *feldname;

         for (i = 0; i < dataform.fieldanz; i ++)
         {
             feldname = dataform.mask[i].item->GetItemName ();
             if (feldname && strcmp (feldname, fname) == 0)
             {
                   break;
             }
         }
         if (i == dataform.fieldanz) return -1;

         return (dataform.mask[i].attribut);
}

int BESTPLIST::Getib (void)
{
    static BOOL ParOK = 0;
    static int IB;

    if (ParOK) return IB;

    ParOK = 1;
    IB = 0;
    strcpy (sys_par.sys_par_nam,"ib");
    if (sys_par_class.dbreadfirst () == 0)
    {
        if (atoi (sys_par.sys_par_wrt)) 
        {
                  IB = 1;
        }
    }
    return IB;
}

void BESTPLIST::Geta_kum_par (void)
{
    static BOOL ParOK = 0;

    if (ParOK) return;

    ParOK = 1;
    a_kum_par = FALSE;
    strcpy (sys_par.sys_par_nam,"a_kum_par");
    if (sys_par_class.dbreadfirst () == 0)
    {
        if (atoi (sys_par.sys_par_wrt)) 
        {
                  a_kum_par = TRUE;
        }
    }
    return;
}


void BESTPLIST::Geta_bz2_par (void)
{
    static BOOL ParOK = 0;

    if (ParOK) return;

    ParOK = 1;
//    SetFieldAttr ("a_bz2", REMOVED);
    return;
    strcpy (sys_par.sys_par_nam,"a_bz2_par");
    if (sys_par_class.dbreadfirst () == 0)
    {
        if (atoi (sys_par.sys_par_wrt)) 
        {
                  SetFieldAttr ("a_bz2", DISPLAYONLY);
                  return;
        }
    }
    SetFieldAttr ("a_bz2", REMOVED);
//    eListe.DestroyField (eListe.GetFieldPos ("a_bz2")); 
}

/*
void BESTPLIST::Getbest_me_pr0 (void)
{
    BOOL ParOK = 0;

    if (ParOK) return;

    ParOK = 1;
    best_me_pr_0 = 0;
    strcpy (sys_par.sys_par_nam,"best_me_pr_0");
    if (sys_par_class.dbreadfirst () != 0)
    {
        return;
    }
    best_me_pr_0 = atoi (sys_par.sys_par_wrt);
}
*/

/*
void BESTPLIST::Getwa_pos_txt (void)
{
    BOOL ParOK = 0;

    if (ParOK) return;

    ParOK = 1;
	wa_pos_txt = FALSE;
    strcpy (sys_par.sys_par_nam,"wa_pos_txt");
    if (sys_par_class.dbreadfirst () == 0)
	{
                 wa_pos_txt = atoi (sys_par.sys_par_wrt); 
	}
}
*/


int BESTPLIST::SetRowItem (void)
{
       return 0;
}

int BESTPLIST::TestAppend (void)
/**
Testen, ob ein Satz angehaengt werden darf.
**/
{
    if (eListe.GetRecanz () == 0);
    else if (ratod (invpos.a) == (double) 0.0)
    {
        return FALSE;
    }
  
	pr_ek = (double) 0.0;
	memcpy (&invpos, &invpos_null, sizeof (struct INVPOS)); 
    eListe.SetPos (eListe.GetAktRow (), eListe.FirstColumn ());
    return TRUE;
}

int BESTPLIST::DeleteLine (void)
/**
Zeile aus Liste loeschen.
**/
{
/*
   	    BucheBsd (ratod (invpos.a), (double) 0.0, atoi (invpos.me_einh_kun),
			      ratod (invpos.auf_vk_pr));
*/
// soll generell gehen 12.12.2007	    if (ratod(invpos.pr_ek) != (double) 0)
//		{
			eListe.DeleteLine ();
 			AnzBestWert ();
//		}
        return 0;
}

int BESTPLIST::InsertLine (void)
/**
Zeile aus Liste loeschen.
**/
{
        eListe.SetPos (eListe.GetAktRow (), eListe.FirstColumn ());
        eListe.InsertLine ();
        return 0;
}


int BESTPLIST::PosRab (void)
/**
Zeile aus Liste loeschen.
**/
{

//	    EnterPosRab ();
        return 0;
}

int BESTPLIST::AppendLine (void)
/**
Zeile aus Liste loeschen.
**/
{
	    pr_ek     = (double) 0.0;
	    pr_ek_bto = (double) 0.0;
        eListe.AppendLine ();
        return 0;
}


BOOL BESTPLIST::BsdArtikel (double a)
/**
Bestandskennzeichen in a_bas pruefen.
**/
{
	     char bsd_kz [2];

		 strcpy (bsd_kz, "N");
		 DbClass.sqlin ((double *) &a, 3, 0);
		 DbClass.sqlout ((char *) bsd_kz, 0, 2);
		 DbClass.sqlcomm ("select bsd_kz from a_bas where a = ?");
		 if (bsd_kz[0] == 'J') return TRUE;
		 return FALSE;
}


void BESTPLIST::BucheBsd (double a, double auf_me, short me_einh_lief, double pr_ek_bto)
/**
Bestandsbuchung vorbereiten.
**/
{
	/********
	double best_me_vgl;
	double buchme;
	char datum [12];

	if (bsd_kz == 0) return;
	if (BsdArtikel (a) == FALSE) return;
	if (auf_me == akt_me) return;
	buchme = auf - akt_me;

	bsd_buch.nr  = best_kopf.best_blg;
	strcpy (bsd_buch.blg_typ, "B");   // Achtung Kennzeichen B noch �berpr�fen
 	bsd_buch.mdn = best_kopf.mdn;
	bsd_buch.fil = best_kopf.fil;
	bsd_buch.kun_fil = 0;
	bsd_buch.a   = a;
	sysdate (datum);
	bsd_buch.dat = dasc_to_long (datum);
	systime (bsd_buch.zeit);
	strcpy (bsd_buch.pers, sys_ben.pers);

    sprintf (bsd_buch.bsd_lgr_ort, "%ld", akt_lager);

	bsd_buch.qua_status = 0;
	bsd_buch.me = buchme;
	bsd_buch.bsd_ek_vk = pr_ek;
    strcpy (bsd_buch.chargennr, "");
    strcpy (bsd_buch.ident_nr, "");
    strcpy (bsd_buch.herk_nachw, "");
    sprintf (bsd_buch.lief, "%s", best_kopf.lief);
    bsd_buch.auf = best_kopf.best_blg;
    strcpy  (bsd_buch.verfall, "");
    bsd_buch.delstatus = 0;
    strcpy  (bsd_buch.err_txt, "");
	BsdBuch.dbinsert ();
  *******/
}




int BESTPLIST::WriteRow (void)
/**
Zeile beim Verlassen pruefen.
**/
{
    Mess.Message("");
    if (testme () == -1) return -1;
    if (testpr () == -1) return -1;
    set_fkt (NULL, 9);
    SetFkt (9, leer, NULL);
    if (ratod (invpos.a) == (double) 0.0)
    {
        eListe.DeleteLine ();
        return (1);
    }


/*
	BucheBsd (ratod (invpos.a), ratod (invpos.auf_me), atoi (invpos.me_einh_lief),
		      ratod (invpos.pr_ek_bto));
*/


    return (0);
}

int BESTPLIST::TestRow (void)
/**
Zeile beim Verlassen pruefen.
**/
{
     
    if (testme () == -1) return -1;
    if (testpr () == -1) return -1;
    set_fkt (NULL, 9);
    SetFkt (9, leer, NULL);
    return (0);
}

void BESTPLIST::GenNewPosi (void)
/**
Neue Positionsnummern in 10-er Schritten generieren.
**/
{
    int i;
    int row;
    int recs;
    long posi;

    row  = eListe.GetAktRow ();
    memcpy (&invpostab[row], &invpos, sizeof (struct INVPOS));
    recs = eListe.GetRecanz ();
    posi = 10;
    for (i = 0; i < recs; i ++, posi += 10)
    {
        sprintf (invpostab[i].posi, "%ld", posi);
    }
    eListe.DisplayList ();
    memcpy (&invpos, &invpostab[row], sizeof (struct INVPOS));
}


int BESTPLIST::PosiEnd (long posi)
/**
Positionnummer testen.
**/
{
    int row;
    int recs;
    long nextposi;

    row  = eListe.GetAktRow ();
    recs = eListe.GetRecanz ();

    if (row >= recs - 1)
    {
            return FALSE;
    }

    nextposi = atol (invpostab [row + 1].posi);
    if (nextposi <= posi)
    {
        GenNewPosi ();
        return TRUE;
    }
    return FALSE;
}

void BESTPLIST::TestMessage (void)
{
	MSG msg;

    if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
    {
          TranslateMessage(&msg);
          DispatchMessage(&msg);
    }
}



int BESTPLIST::Querya (void)
/**
Auswahl ueber Artikel.
**/
{
       int ret;

	   if (searchadirect)
	   {
            ret = QClass.searcha (eListe.Getmamain3 ());
	   }
       else 
	   {
            ret = QClass.querya (eListe.Getmamain3 ());
	   }
       set_fkt (dokey5, 5);
       set_fkt (WriteAllPos, 12);
       if (ret == FALSE)
       {
           eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                                 eListe.GetAktColumn ());
           return 0;
       }
       UpdateWindow (mamain1);
       sprintf (invpostab[eListe.GetAktRow ()].a, "%.0lf", _a_bas.a);
       eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
       eListe.ShowAktRow ();
       eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                             eListe.GetAktColumn ());
       PostMessage (eListe.Getmamain3 (), WM_KEYDOWN, VK_RETURN, 0l);
       return 0;
}
       

int BESTPLIST::Savea (void)
/**
Artikelnummer sichern.
**/
{
       Akta = ratod (invpos.a);
       set_fkt (Querya, 9);
       SetFkt (9, auswahl, KEY9);
       Mess.Message("");
       return 0;
}






int BESTPLIST::setkey9me (void)
/**
Artikelnummer sichern.
**/
{
       return 0;
}

int BESTPLIST::setkey9basis (void)
/**
Artikelnummer sichern.
**/
{
	   pr_ek = ratod (invpos.pr_ek);
//       set_fkt (ShowBasis, 9);
//       SetFkt (9, basisme, KEY9);
       return 0;
}

static ITEM iSaTxt  ("",  "Sonderangebot", "", 0);

static field _fSaTxt [] = {
&iSaTxt,        13,  0, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form fSaTxt = {1, 0, 0, _fSaTxt, 0, 0, 0, 0, NULL};    


void BESTPLIST::MoveSaW (void)
/**
Fenster Sonderangebot bewegen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;

        if (hMainWin == NULL) return;    
        if (AufMehWnd == NULL) return;    
        if (SaWindow == NULL) return;    
           
	    memcpy (&tm, &textm, sizeof (tm)); 

        GetClientRect (hMainWin, &rect);
        GetWindowRect (hMainWin, &wrect);

        y = (wrect.top +  11 * tm.tmHeight);
        cx = 15 * tm.tmAveCharWidth + 2;
        x = wrect.left + rect.right - cx + 1;
        cy = 2 * tm.tmHeight;
		MoveWindow (SaWindow, x, y, cx, cy, TRUE);
}


HWND BESTPLIST::CreateSaW (void)
/**
Fenster fuer Sonderangebot erzeugen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;

        if (hMainWin == NULL) return NULL;    
           
	    memcpy (&tm, &textm, sizeof (tm)); 

        GetClientRect (hMainWin, &rect);
        GetWindowRect (hMainWin, &wrect);


        y = (wrect.top +  11 * tm.tmHeight);
        cx = 15 * tm.tmAveCharWidth + 2;
        x = wrect.left + rect.right - cx + 1;
        cy = 2 * tm.tmHeight;

        SaWindow       = CreateWindow (
                                       "StaticMess",
                                       "",
                                       WS_BORDER | 
                                       WS_POPUP | WS_VISIBLE,
                                       x, y,
                                       cx, cy,
                                       hMainWin,
                                       NULL,
                                       hMainInst,
                                    NULL);


        return SaWindow;
}

void BESTPLIST::PaintSa (HDC hdc)
/**
Text anzeigen.
**/
{
	  RECT rect;
	  SIZE size;
	  int x,y;
	  extern HFONT stdHfont;
	  static char *txt = "Sonderangebot";

	  if (SaWindow == NULL) return;

	  GetClientRect (SaWindow, &rect); 
      SelectObject (hdc, stdHfont);
      GetTextExtentPoint32 (hdc, txt, strlen (txt), &size); 
	  x = max (0, (rect.right  - rect.left - size.cx) / 2);
	  y = max (0, (rect.bottom - rect.top  - size.cy) / 2);
	  SetBkMode (hdc, TRANSPARENT);
      SetTextColor (hdc,MessCol);
      SetBkColor (hdc, MessBkCol);
	  TextOut (hdc, x, y, txt, strlen (txt));
}

void BESTPLIST::CreateSa (void)
/**
Fenster Sonderpreis anzeigen.
**/
{
	   if (SaWindow) return;

	   SaWindow = CreateSaW ();
}

void BESTPLIST::DestroySa (void)
/**
Fenster mit Sonderpreis loeschen.
**/
{
	  if (SaWindow == NULL)
	  {
		  return;
	  }
	  CloseControls (&fSaTxt);
	  DestroyWindow (SaWindow);
	  SaWindow = NULL;
}


void BESTPLIST::MovePlus (void)
/**
Fenster Sonderangebot bewegen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;

        if (hMainWin == NULL) return;    
        if (AufMehWnd == NULL) return;    
		if (PlusWindow == NULL) return; 
           
	    memcpy (&tm, &textm, sizeof (tm)); 

        GetClientRect (hMainWin, &rect);
        GetWindowRect (hMainWin, &wrect);

        y = (wrect.top +  9 * tm.tmHeight);
        cx = 3 * tm.tmAveCharWidth + 2;
        x = wrect.left + rect.right - cx + 1;
        cy = 2 * tm.tmHeight;
		MoveWindow (PlusWindow, x, y, cx, cy, TRUE);
}

HWND BESTPLIST::CreatePlus (void)
/**
Fenster fuer Sonderangebot erzeugen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;

        if (hMainWin == NULL) return NULL;    
           
	    memcpy (&tm, &textm, sizeof (tm)); 

        GetClientRect (hMainWin, &rect);
        GetWindowRect (hMainWin, &wrect);


        y = (wrect.top +  9 * tm.tmHeight);
        cx = 3 * tm.tmAveCharWidth + 2;
        x = wrect.left + rect.right - cx + 1;
        cy = 2 * tm.tmHeight;

        PlusWindow       = CreateWindow (
                                       "StaticMess",
                                       "",
                                       WS_BORDER | 
                                       WS_POPUP | WS_VISIBLE,
                                       x, y,
                                       cx, cy,
                                       hMainWin,
                                       NULL,
                                       hMainInst,
                                    NULL);


        return PlusWindow;
}

void BESTPLIST::PaintPlus (HDC hdc)
/**
+ anzeigen.
**/
{
	  RECT rect;
	  SIZE size;
	  int x,y;
	  extern HFONT stdHfont;
	  static char *txt = "+";

	  if (PlusWindow == NULL) return;

	  GetClientRect (PlusWindow, &rect); 
      SelectObject (hdc, stdHfont);
      GetTextExtentPoint32 (hdc, txt, strlen (txt), &size); 
	  x = max (0, (rect.right  - rect.left - size.cx) / 2);
	  y = max (0, (rect.bottom - rect.top  - size.cy) / 2);
	  SetBkMode (hdc, TRANSPARENT);
      SetTextColor (hdc,MessCol);
      SetBkColor (hdc, MessBkCol);
	  TextOut (hdc, x, y, txt, strlen (txt));
}


void BESTPLIST::DestroyPlus (void)
/**
Fenster mit + loeschen.
**/
{
	  if (PlusWindow == NULL)
	  {
		  return;
	  }
	  DestroyWindow (PlusWindow);
	  PlusWindow = NULL;
}


/**
double BESTPLIST::PrAktionChoise (void)
{
       AktionDlg *aktionDlg;

       aktionDlg = new AktionDlg (-1, -1, 40, 8, "Preisauswahl", 105, FALSE);
       aktionDlg->SetWinBackground (GetSysColor (COLOR_3DFACE));
       aktionDlg->SetStyle (WS_VISIBLE | WS_POPUP | WS_CAPTION | WS_DLGFRAME | WS_SYSMENU );
       HWND ChWindow = aktionDlg->OpenScrollWindow (hMainInst, hMainWin);
	   aktionDlg->ProcessMessages ();
       double pr_ek = aktionDlg->GetPreis ();
       aktionDlg->DestroyWindow ();
       delete aktionDlg;
       return pr_ek;
}
**/


void BESTPLIST::ReadPr (void)
/**
Artikelpreis holen.
**/
{
	int dsqlstatus = 0;
	if (dsqlstatus == 100)
	{
	    Mess.Message ("Kein Preis gefunden");
	}
}




void BESTPLIST::ReadMeEinh (void)
/**
Mengeneinheiten holen.
**/
{
//      int dsqlstatus;
//      char ptwert [5];

        KEINHEIT keinheit;

        einh_class.GetBasEinh (_a_bas.a, &keinheit);
        strcpy (invpos.me_bz, keinheit.me_einh_bas_bez);
        sprintf (invpos.me_einh,     "%hd", keinheit.me_einh_bas_bez);
        inh = keinheit.inh;

        return;

		/*
        switch (_a_bas.a_typ)
        {
             case HNDW :
                     dsqlstatus = HndwClass.lese_a_hndw (_a_bas.a);
                     break;
             case EIG :
                     dsqlstatus = HndwClass.lese_a_eig (_a_bas.a);
                     a_hndw.me_einh_kun = a_eig.me_einh_ek;
                     break;
             case EIG_DIV :
                     dsqlstatus = HndwClass.lese_a_eig_div (_a_bas.a);
                     a_hndw.me_einh_kun = a_eig_div.me_einh_ek;
                     break;
        }

        sprintf (ptwert, "%hd", _a_bas.me_einh);
        if (ptab_class.lese_ptab ("me_einh", ptwert) == 0)
        {
            strcpy (invpos.basis_me_bz, ptabn.ptbezk);
        }

        if (dsqlstatus) return;

        sprintf (ptwert, "%hd", a_hndw.me_einh_kun);
        if (ptab_class.lese_ptab ("me_einh", ptwert) == 0)
        {
            strcpy (invpos.me_bz, ptabn.ptbezk);
        }
		*******/
}


void BESTPLIST::rechne_liefme (void)
/**
Basismenge der aktuellen Bestellmenge anzeigen.
**/
{
	/******
	double best_me;
	double a;
	short me_einh_lief;
	double best_me_vgl;
    KEINHEIT keinheit;


	best_me = ratod (invpos.auf_me);
	a  = ratod (invpos.a);
	me_einh_lief = atoi (invpos.me_einh_lief);

    einh_class.AktBestEinh (best_kopf.mdn, best_kopf.fil,
                                best_kopf.lief, a, me_einh_lief);
    einh_class.GetLiefEinh (best_kopf.mdn, best_kopf.fil,
                              best_kopf.lief, a, &keinheit);
          
    if (keinheit.me_einh_lief == keinheit.me_einh_bas)
    {
            best_me_vgl = best_me;
    }
    else
    {
            if (keinheit.inh <= (double) 0.0)
            {
                     keinheit.inh = (double) 1.0;
            }
            best_me_vgl = best_me * keinheit.inh;
    }

	sprintf (invpos.lief_me, "%8.4lf", best_me_vgl);
	******/
}



int BESTPLIST::testme (void)
/**
Auftragsmeneg pruefen.
**/
{
//        KEINHEIT keinheit;

	return 0;
	    Mess.Message("  ");
//        if (ratod (invpos.a) == (double) 0.0) return 0;

//		if (add && aufme_old != (double) 0.0)
//		{
//			fil_inv.me = ratod (invpos.last_me);
//			sprintf (invpos.last_me, "%.3lf", fil_inv.me + aufme_old);
//          memcpy (&invpostab[eListe.GetAktRow()], 
//                            &invpos, sizeof (struct INVPOS));
//			eListe.ShowAktRow ();
//		}

//        memcpy (&invpostab[eListe.GetAktRow()], 
//                            &invpos, sizeof (struct INVPOS));
//        AnzBestWert ();
//		aufme_old = (double) 0.0;
//	    add = FALSE;
//		DestroyPlus ();
 //      return 0;

}

int BESTPLIST::TestPrproz_diff (void)
/**
Test, ob die Preisaenderung ueber prproz_diff % ist.
**/
{
	    double oldek;
	    double diff;
		double diffproz;
		char buffer [256];

		if (pr_ek == (double) 0.0) return 1;
		oldek = ratod (invpos.pr_ek);
		diff = oldek - pr_ek;
		if (diff < 0) diff *= -1;
      diffproz = 100 * diff / pr_ek;  
		if (diffproz > prproz_diff)
		{
			sprintf (buffer, "Achtung !! Preis�nderung �ber %.4lf %c.\n"
				             "�nderung OK ?", prproz_diff, '%');
            if (abfragejn (eListe.Getmamain3 (), 
					       buffer , "N") == 0)
			{
				sprintf (invpos.pr_ek, "%lf", pr_ek);
                memcpy (&invpostab[eListe.GetAktRow()], 
                            &invpos, sizeof (struct INVPOS));
                eListe.ShowAktRow ();
                eListe.SetPos (eListe.GetAktRow (), 
                              eListe.GetAktColumn ());

		 	    return 0;
			}
		}
		return 1;
}


int BESTPLIST::testpr (void)
/**
Artikel holen.
**/
{
//	    char buffer [256];

	    if (preistest == 1 && pr_ek)
		{
			if (ratod (invpos.pr_ek) != pr_ek)
			{
			        disp_mess ("Achtung !! Der Preis wurde ge�ndert", 2);
			}
			return 0;
		}
		else if (preistest == 4)
		{
			if (TestPrproz_diff () == 0)
			{
               eListe.SetPos (eListe.GetAktRow (), 
                              eListe.GetAktColumn ());
               return (-1);
            }
		}
		else if (preistest == 2 && pr_ek)
		{
			sprintf (invpos.pr_ek, "%4lf", pr_ek);
		}

        if (ratod (invpos.a) == (double) 0.0) return 0;
/********
        if (ratod (invpos.pr_ek) == (double) 0.0)
        {
//            if (best_me_pr_0)
//            {
               if ((eListe.IsAppend ()) && (preis0_mess == 1))
			   {
			          sprintf (buffer, "Achtung !! Preis ist 0\n"
				                       "            OK ?");
                      if (abfragejn (eListe.Getmamain3 (), 
					       buffer , "N") == 0)
					  {
                              eListe.SetPos (eListe.GetAktRow (), 
                                             eListe.GetAktColumn ());
							  return -1;
					  }
			  }
              return 0;
            }
            else
            {
               disp_mess ("Der Preis darf nicht 0 sein", 2);
               eListe.SetPos (eListe.GetAktRow (), 
                              eListe.GetAktColumn ());
            }
            return (-1);
        }
********/
        if (ratod (invpos.pr_ek) > MAXPR)
        {
            print_mess (2, "Der Preis ist zu gross");
            sprintf (invpos.pr_ek, "%.4lf", (double) 0.0);
            memcpy (&invpostab[eListe.GetAktRow()], 
                            &invpos, sizeof (struct INVPOS));
            eListe.ShowAktRow ();
            eListe.SetPos (eListe.GetAktRow (), 
                              eListe.GetAktColumn ());
            return -1;
        }
        memcpy (&invpostab[eListe.GetAktRow()], 
                            &invpos, sizeof (struct INVPOS));
        AnzBestWert ();

        return 0;
}




int BESTPLIST::TestNewArt (double a)
/**
Test, ob der Artikel bei diesem Lieferschein existiert.
**/
{
	     int i;
		 int angpanz;
		 int akt_pos;
		 
		 akt_pos = eListe.GetAktRow ();
         angpanz = eListe.GetRecanz ();
	     for (i = 0; i < angpanz; i ++)
		 {
			 if (i == akt_pos) continue;
			 if (a == ratod (invpostab[i].a)) break;
		 }
		 if (i == angpanz) return -1;
		 return (i);
}


int BESTPLIST::fetchaDirect (int lrow)
/**
Artikel holen.
**/
{

       int dsqlstatus;
       char wert [5];
       long posi;
       int i;


	   clipped (invpos.a);
	   sprintf (invpos.a, "%13.0lf", ratod (invpos.a));

       einh_class.SetBestEinh (1);
       SetFkt (9, leer, NULL);
       set_fkt (NULL, 10);
       SetFkt (10, leer, NULL);
       posi = atol (invpos.posi);
       if (posi == 0)
       {
             i = lrow;
             if (i == 0)
             {
                 posi = 1;
             }
             else
             {
                 posi = atol (invpostab[i - 1].posi);
                 posi ++;
             }
             if (PosiEnd (posi) == 0)
             {
                 sprintf (invpos.posi, "%ld", posi);
             }
       }

       dsqlstatus = lese_a_bas (ratod (invpos.a));
       if (dsqlstatus == 100)
       {
           if (syskey != KEYCR &&syskey != KEYTAB 
               && eListe.IsAppend ())
           {
               return (-1);
           }
           sprintf (invpostab[lrow].a, "%.0lf", Akta);
           memcpy (&invpos, &invpostab[lrow], sizeof (struct INVPOS));
           return (-1);
       }


       sprintf (invpos.a_bz1,       "%s %s",      clipped(_a_bas.a_bz1),_a_bas.a_bz2);
//       sprintf (invpos.a_bz1,       "%s",      _a_bas.a_bz1);
       sprintf (invpos.a_bz2,       "%s",      _a_bas.a_bz2);
       sprintf (wert, "%hd", _a_bas.me_einh);
       sprintf (invpos.me_einh, "%hd", _a_bas.me_einh);
       dsqlstatus = ptab_class.lese_ptab ("me_einh", wert);  
       strcpy (invpos.me_bz, ptabn.ptbezk);

       memcpy (&invpostab[lrow], &invpos, sizeof (struct INVPOS));
       if (eListe.IsNewRec ())
       {
           if (ratod (invpos.pr_ek) == (double) 0.0)
           {
                    ReadPr ();
           }

           ReadMeEinh ();
       }
	   else if (ratod (invpos.pr_ek) == (double) 0.0)
	   {
		           ReadPr ();
	   }
       memcpy (&invpostab[lrow], &invpos, sizeof (struct INVPOS));
       return 0;
}

int BESTPLIST::rechnepreis (void)
/**
Artikel holen.
**/
{
	  double kosten = HoleKosten (ratod(invpos.a));
	 double vollk = ratod(invpos.pr_ek) + kosten;
	 double sk = ratod(invpos.pr_ek) + auf_sk + kosten;
	 double fil_ek = sk + auf_fil_ek;
	 sprintf (invpos.pr_vollk,"%.4lf",vollk);
	 sprintf (invpos.pr_sk,"%.4lf",sk);
	 sprintf (invpos.pr_fil_ek,"%.4lf",fil_ek);
     memcpy (&invpostab[eListe.GetAktRow()], &invpos, sizeof (struct INVPOS));
     eListe.ShowAktRow ();
	return 0;
}
double BESTPLIST::HoleKosten (double a)
{
	short a_typ = 0;
	short a_typ2 = 0;
	double kost = 0.0;
	if (mit_kosten == 0) return 0.0;
	   DbClass.sqlin ((double *)   &a, 3, 0);
	   DbClass.sqlout ((short *)  &a_typ, 1, 0);
	   DbClass.sqlout ((short *)  &a_typ2, 2, 0);
	   DbClass.sqlcomm ("select a_typ,a_typ2 from a_bas where a = ? ");
	   if (a_typ == 2 || a_typ2 == 2)
	   {
			DbClass.sqlin ((double *)   &a, 3, 0);
			DbClass.sqlout ((double *)  &kost, 3, 0);
			DbClass.sqlcomm ("select kost from a_kalk_eig where a = ?  ");
	   }
	   else if (a_typ == 5 || a_typ2 == 5)
	   {
			DbClass.sqlin ((double *)   &a, 3, 0);
			DbClass.sqlout ((double *)  &kost, 3, 0);
			DbClass.sqlcomm ("select kost from a_kalk_mat where a = ?  ");
	   }


	return kost;
}
	    
int BESTPLIST::fetcha (void)
/**
Artikel holen.
**/
{

//	   char buffer [25];
//	   RECT rect;
       int dsqlstatus;
       char wert [5];
       long posi;
       int art_pos;
       int i;

	   clipped (invpos.a);
	   if (syskey != KEYCR && ratod (invpos.a) == (double) 0.0)
	   {
            return 0;
	   }
	   if (!numeric (invpos.a))
	   {
		   if (searchmodedirect)
		   {
                  QClass.searcha_direct (eListe.Getmamain3 (),invpos.a);
		   }
		   else
		   {
 		          QClass.querya_direct (eListe.Getmamain3 (),invpos.a);
		   }
		   if (ratod (invpos.a) == (double) 0.0)
		   {
               memcpy (&invpostab[eListe.GetAktRow()], 
				       &invpos, sizeof (struct INVPOS));
			   UpdateWindow (mamain1);
               eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
               eListe.ShowAktRow ();
			   return (-1);
		   }
	   }
	   sprintf (invpos.a, "%13.0lf", ratod (invpos.a));

	   art_pos = TestNewArt (ratod (invpos.a));
/**
	   if (art_pos != -1 && a_kum_par == FALSE && art_un_tst)
	   {
		           if (abfragejn (eListe.Getmamain3 (), 
					        "Artikel bereits im Auftrag, OK?", "J") == 0)
				   { 
                        sprintf (invpostab[eListe.GetAktRow ()].a, "%.0lf", Akta);
                        memcpy (&invpos, &invpostab[eListe.GetAktRow ()], sizeof (struct INVPOS));
                        eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
                        eListe.ShowAktRow ();
                        return (-1);
				   }
	   }
**/
/**********
	   if (art_pos != -1 && a_kum_par && art_un_tst == 1)
	   {
		           if (abfragejn (eListe.Getmamain3 (), 
					        "Der Artikel ist bereits erfasst.\n\n"
							"Artikel bearbeiten ?", "J") == 0)
				   { 
                        sprintf (invpostab[eListe.GetAktRow ()].a, "%.0lf", Akta);
                        memcpy (&invpos, &invpostab[eListe.GetAktRow ()], sizeof (struct INVPOS));
                        eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
                        eListe.ShowAktRow ();
                        return (-1);
				   }
	   }

	   if (art_pos != -1 && a_kum_par)
	   {
  		           GetClientRect (eListe.Getmamain3 (), &rect);
                   DeleteLine ();
				   if (art_pos > eListe.GetAktRow ()) art_pos --;
                   eListe.SetNewRow (art_pos);
 		           eListe.SetFeldFocus0 (art_pos, eListe.FirstColumn ());
                   InvalidateRect (eListe.Getmamain3 (), &rect, TRUE);
                   UpdateWindow (eListe.Getmamain3 ());
                   memcpy (&invpos, &invpostab[art_pos], sizeof (struct INVPOS));
				   if (add_me)
				   {
				            add = TRUE;
				            aufme_old = ratod (invpos.auf_me);
				            CreatePlus ();
				   }
				   SetRowItem ();
                   return (0);
	   }
	   ****************/
 
       einh_class.SetBestEinh (1);
       SetFkt (9, leer, NULL);
       set_fkt (NULL, 10);
       SetFkt (10, leer, NULL);
       posi = atol (invpos.posi);
       if (posi == 0)
       {
             i = eListe.GetAktRow ();
             if (i == 0)
             {
                 posi = 1;
             }
             else
             {
                 posi = atol (invpostab[i - 1].posi);
                 posi ++;
             }
             if (PosiEnd (posi) == 0)
             {
                 sprintf (invpos.posi, "%ld", posi);
             }
       }

       dsqlstatus = lese_a_bas (ratod (invpos.a));

// Umvorhersehbare Ereignisse                

	   if (dsqlstatus < 0)
	   {
		   print_mess (2, "Fehler %d beim Lesen von Artikel %.0lf", dsqlstatus,
			                                                        ratod (invpos.a));
           sprintf (invpostab[eListe.GetAktRow ()].a, "%.0lf", Akta);
           memcpy (&invpos, &invpostab[eListe.GetAktRow ()], sizeof (struct INVPOS));
           eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
           eListe.ShowAktRow ();
           return (-1);
	   }


       if (dsqlstatus == 100)
       {
           if (syskey != KEYCR &&syskey != KEYTAB 
               && eListe.IsAppend ())
           {
               return (-1);
           }
		   if (NoArtMess == FALSE)
		   {
                   print_mess (2, "Artikel %.0lf nicht gefunden",
                          ratod (invpos.a));
		   }
           sprintf (invpostab[eListe.GetAktRow ()].a, "%.0lf", Akta);
           memcpy (&invpos, &invpostab[eListe.GetAktRow ()], sizeof (struct INVPOS));
           eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
           eListe.ShowAktRow ();
           return (-1);
       }

//       sprintf (invpos.a_bz1,       "%s",      _a_bas.a_bz1);
       sprintf (invpos.a_bz1,       "%s %s",      clipped(_a_bas.a_bz1),_a_bas.a_bz2);
       sprintf (invpos.a_bz2,       "%s",      _a_bas.a_bz2);
       sprintf (wert, "%hd", _a_bas.me_einh);
       sprintf (invpos.me_einh, "%hd", _a_bas.me_einh);
       dsqlstatus = ptab_class.lese_ptab ("me_einh", wert);  
       strcpy (invpos.me_bz, ptabn.ptbezk);
       sprintf (invpos.posi, "%d", posi);

       memcpy (&invpostab[eListe.GetAktRow()], &invpos, sizeof (struct INVPOS));
       eListe.ShowAktRow ();
       if (eListe.IsNewRec ())
       {
//           if (ratod (INVPOS.auf_vk_pr) == (double) 0.0)
           {
/***
			        if (ShowLief () == 0)
					{
						      strcpy (invpos.lief, " ");
                              sprintf (invpostab[eListe.GetAktRow ()].a, "%.0lf", Akta);
                              memcpy (&invpos, &invpostab[eListe.GetAktRow ()], 
								      sizeof (struct INVPOS));
                              eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
                              eListe.ShowAktRow ();
		                      return -1;
					}
//                    ReadPr ();

                    if ((ratod (invpos.pr_ek_bto) == 0.0) && (preis0_mess == 1))
					{
			          sprintf (buffer, "Achtung !! Preis 0 gelesen\n"
				                       "            OK ?");
                      if (abfragejn (eListe.Getmamain3 (), 
					       buffer , "N") == 0)
					  {
                              sprintf (invpostab[eListe.GetAktRow ()].a, "%.0lf", Akta);
                              memcpy (&invpos, &invpostab[eListe.GetAktRow ()], 
								      sizeof (struct INVPOS));
                              eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
                              eListe.ShowAktRow ();
		                      return -1;
					  }
			   }
*/
           }
           if (ratod (invpos.pr_ek) == (double) 0.0)
           {
                    ReadPr ();
           }
           ReadMeEinh ();
       }
       memcpy (&invpostab[eListe.GetAktRow()], &invpos, sizeof (struct INVPOS));
       
       eListe.ShowAktRow ();
       eListe.SetRowItem ("a", invpostab[eListe.GetAktRow ()].a);
       set_fkt (NULL, 9);
       return 0;
}



void BESTPLIST::DellAllPos (void)
/**
Position l�schen
**/
{
	   extern short sql_mode;
	   short sql_modes;
//	   int cursor;
	   int delete_pos;


	   sql_modes = sql_mode;
	   sql_mode = 1;

	   if (einstieg_default == TRUE)
	   {
		DbClass.sqlin ((short *)  &fleisch_ek.mdn, 1, 0);
		DbClass.sqlin ((long *)   &defaultdat, 2, 0);
		delete_pos = DbClass.sqlcursor ("delete from fleisch_ek "
		                               "where mdn = ? "
						               "and dat = ?");

	    DbClass.sqlexecute (delete_pos);
	   }

	   DbClass.sqlin ((short *)  &fleisch_ek.mdn, 1, 0);
	   DbClass.sqlin ((long *)   &fleisch_ek.gue_ab, 2, 0);
	   delete_pos = DbClass.sqlcursor ("delete from fleisch_ek "
		                               "where mdn = ? "
						               "and gue_ab = ? ");

	   DbClass.sqlexecute (delete_pos);

/********
	   DbClass.sqlin ((short *)  &fleisch_ek.mdn, 1, 0);
	   DbClass.sqlin ((long *)   &fleisch_ek.gue_ab, 2, 0);
	   DbClass.sqlout ((long *)  &fleisch_ek.gue_ab, 2, 0);
	   cursor = DbClass.sqlcursor ("select gue_ab from fleisch_ek "
		                           "where mdn = ? "
						           "and dat = ? ");
								   

	   DbClass.sqlin ((short *)  &fleisch_ek.mdn, 1, 0);
	   DbClass.sqlin ((long *)   &fleisch_ek.gue_ab, 2, 0);
	   delete_pos = DbClass.sqlcursor ("delete from fleisch_ek "
		                               "where mdn = ? "
						               "and gue_ab = ?");

	   while (DbClass.sqlfetch (cursor) == 0)
	   {
		   DbClass.sqlexecute (delete_pos);
	   }
	   DbClass.sqlclose (delete_pos);
	   DbClass.sqlclose (cursor);

	   if (fleisch_ek.dat == heute)
	   {
		DbClass.sqlin ((short *)  &fleisch_ek.mdn, 1, 0);
		DbClass.sqlin ((long *)   &defaultdat, 2, 0);
		DbClass.sqlout ((long *)  &fleisch_ek.dat, 2, 0);
		cursor = DbClass.sqlcursor ("select dat from fleisch_ek "
		                           "where mdn = ? "
						           "and dat = ? ");
								   

		DbClass.sqlin ((short *)  &fleisch_ek.mdn, 1, 0);
		DbClass.sqlin ((long *)   &defaultdat, 2, 0);
		delete_pos = DbClass.sqlcursor ("delete from fleisch_ek "
		                               "where mdn = ? "
						               "and dat = ?");

		while (DbClass.sqlfetch (cursor) == 0)
		{
			DbClass.sqlexecute (delete_pos);
		}
		DbClass.sqlclose (delete_pos);
		DbClass.sqlclose (cursor);
	   }
*********/
	   sql_mode = sql_modes;
}

void BESTPLIST::SchreibeA_Kalkn (void)
/**
Position l�schen
**/
{
	a_kalkn.mdn = fleisch_ek.mdn;
	a_kalkn.fil = 0;
	a_kalkn.a = fleisch_ek.a;
	a_kalkn.gue_ab = fleisch_ek.gue_ab;
	a_kalkn.ek = fleisch_ek.preis;
	a_kalkn.sk = fleisch_ek.sk;
	a_kalkn.fil_ek = fleisch_ek.fil_ek;
	A_kalkn.dbupdate();
}

void BESTPLIST::SchreibeLiefBzgn (void)
/**
Position l�schen
**/
{
	int cursor , cursor_j;
	bool gefunden = FALSE;
	   DbClass.sqlin ((short *)  &fleisch_ek.mdn, 1, 0);
	   DbClass.sqlin ((double *)   &fleisch_ek.a, 3, 0);
	   DbClass.sqlout ((short *)  &lief_bzgn.mdn, 1, 0);
	   DbClass.sqlout ((char *)  lief_bzgn.lief, 0, 16);
	   DbClass.sqlout ((long *)  &lief_bzgn.lief_s, 2, 0);
	   DbClass.sqlout ((char *)  lief_bzgn.lief_best, 0, 16);
	   cursor_j = DbClass.sqlcursor ("select mdn,lief,lief_s,lief_best from lief_bzg "
		                           "where (mdn = ? or mdn = 0)  "
						           "and a = ?  and lief_kz = \"J\" order by mdn desc");
	   while (DbClass.sqlfetch (cursor_j) == 0)
	   {
		   gefunden = TRUE;
		   lief_bzgn.fil = 0;
		   lief_bzgn.a = fleisch_ek.a;
		   lief_bzgn.dat = fleisch_ek.dat;
		   lief_bzgn.pr_ek = fleisch_ek.preis;
		   lief_bzgn.pr_ek_eur = fleisch_ek.preis;
		   Lief_bzgn.dbupdate();
	   }
	   DbClass.sqlclose (cursor_j);
	   if (gefunden == FALSE)
	   {
		   DbClass.sqlin ((short *)  &fleisch_ek.mdn, 1, 0);
		   DbClass.sqlin ((double *)   &fleisch_ek.a, 3, 0);
		   DbClass.sqlout ((short *)  &lief_bzgn.mdn, 1, 0);
		   DbClass.sqlout ((char *)  lief_bzgn.lief, 0, 16);
		   DbClass.sqlout ((long *)  &lief_bzgn.lief_s, 2, 0);
		   DbClass.sqlout ((char *)  lief_bzgn.lief_best, 0, 16);
		   cursor = DbClass.sqlcursor ("select mdn,lief,lief_s,lief_best from lief_bzg "
		                           "where (mdn = ? or mdn = 0)  "
						           "and a = ? order by mdn desc");
		   if (DbClass.sqlfetch (cursor) == 0)
			{
			   lief_bzgn.fil = 0;
			   lief_bzgn.a = fleisch_ek.a;
			   lief_bzgn.dat = fleisch_ek.gue_ab;
			   lief_bzgn.pr_ek = fleisch_ek.preis;
			   lief_bzgn.pr_ek_eur = fleisch_ek.preis;
			   if (DbClass.sqlfetch (cursor) == 0)
			   {
				   //nicht eindeutig, hier kein Eintrag in lief_bzgn
			   }
			   else
			   {
					Lief_bzgn.dbupdate();
			   }
		   }
		   DbClass.sqlclose (cursor);
	   }
}

void BESTPLIST::WritePos (int pos)
/**
Position schreiben.
**/
{
	   long gue_ab;
	   KEINHEIT keinheit;
	   InvDat = heute ;

	   gue_ab = fleisch_ek.gue_ab;
	   fleisch_ek.mdn          = fleisch_ek.mdn;
	   fleisch_ek.dat          = InvDat;
       fleisch_ek.a            = ratod (invpostab[pos].a);
//       fleisch_ek.posi            = atoi (invpostab[pos].posi);
	   fleisch_ek.posi = pos;

       fleisch_ek.preis       = ratod (invpostab[pos].pr_ek);
//       fleisch_ek.pr_vollk       = ratod (invpostab[pos].pr_vollk);
       fleisch_ek.sk          = ratod (invpostab[pos].pr_sk);
       fleisch_ek.fil_ek      = ratod (invpostab[pos].pr_fil_ek);

       if (fleisch_ek.a == (double) 0)
       {
           return;
       }
       if (fleisch_ek.preis == (double) 0)
       {
//		   if (InvDat >= heute)
//		   {
				fleisch_ek.dat = defaultdat;
		        fleisch_ek.preis       = ratod (invpostab[pos].last_pr);
		        fleisch_ek.gue_ab = dasc_to_long (invpostab[pos].last_gue);
				fleisch_ek_class.dbupdate (); 
				fleisch_ek.dat = heute;
				fleisch_ek.gue_ab = gue_ab;
//			}
           return;
       }
	   lese_a_bas(fleisch_ek.a);
	   einh_class.GetBasEinh (fleisch_ek.a, &keinheit);
	   fleisch_ek.me_einh = keinheit.me_einh_bas;
	   if (LiefBzgnSchreiben) SchreibeLiefBzgn ();
	   SchreibeA_Kalkn ();
	   
	   fleisch_ek_class.dbupdate (); 
	   if (InvDat >= heute)
	   {
		   fleisch_ek.dat = defaultdat;
		   fleisch_ek_class.dbupdate (); 
		   fleisch_ek.dat = heute;
	   }

}


int BESTPLIST::sortlist (const void *elem1, const void *elem2)
/**
Element aus Liste nach Lieferant und Artikelnummer sortieren.
**/
{
    struct INVPOS *el1; 
    struct INVPOS *el2;
	double a1, a2;

	el1 = (struct INVPOS *) elem1;
	el2 = (struct INVPOS *) elem2;
	a1 = ratod (el1->a) ;
	a2 = ratod (el2->a) ;
	if (a1 == a2) 
	{
		return 0;
	}
	if (a1 > a2)
	{
		return 1;
	}
    return -1;
}

BOOL BESTPLIST::BestNrOK (void)
/**
generierte Angebotsnummer testen.
**/
{
	/*
       char buffer [256];

       if (auto_nr.nr_nr == 0l) return FALSE;

       sprintf (buffer, "select best_blg from best_kopf where mdn = %d "
                                              "and   fil = %d "
                                              "and best_blg =   %ld",
                          best_kopf.mdn, best_kopf.fil, auto_nr.nr_nr);

       if (DbClass.sqlcomm (buffer) != 100) return FALSE;
	   */
       return TRUE;
}
      

void BESTPLIST::GenBestNr (void)
/**
Bestellnummer generieren.
**/
{
	/*
       extern short sql_mode; 
       int sql_mode_s;
	   int dsqlstatus;
	   int i;
	   static int MAXWAIT = 100;
       MSG msg;

       commitwork ();
       beginwork ();
	   auto_nr.nr_nr = 0l;
       sql_mode_s = sql_mode;
       sql_mode = 1;
	   i = 0;
       while (TRUE)
       {
                dsqlstatus = AutoClass.nvholid (best_kopf.mdn, 0, "best");

				if (dsqlstatus == -1)
				{
					        DbClass.sqlin ((short *) &best_kopf.mdn, 1, 0); 
					        DbClass.sqlin ((short *) &best_kopf.fil, 1, 0); 
							DbClass.sqlcomm ("delete from auto_nr where nr_nam = \"best\" "
								              "and mdn = ? and fil = ?");
							dsqlstatus = 100;
				}
					        
					   
                if (dsqlstatus == 100)
                {
                           dsqlstatus = AutoClass.nvanmprf (best_kopf.mdn,
                                                  0,
                                                  "best",
                                                  (long) 1,
                                                  (long) 999999,
                                                  (long) 10,
                                                  "");

                           if (dsqlstatus == 0)
                           {
                                  dsqlstatus = AutoClass.nvholid (best_kopf.mdn,
                                                        0,
                                                        "best");
                           }
                }

                if (dsqlstatus == 0 && BestNrOK ()) break;
				Sleep (50);
                if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
                {
                          TranslateMessage(&msg);
                          DispatchMessage(&msg);
                }

				i ++;
				if (i > MAXWAIT) break;
       }
       sql_mode = sql_mode_s;
       commitwork ();
       beginwork ();
	   */
}

int BESTPLIST::WriteAllPos (void)
/**
Alle Positionen schreiben.
**/
{
    int i;
    int recs;
    int row;
	char akt_lief[17];
//	int dsqlstatus;

    row     = eListe.GetAktRow ();
	fleisch_ek.mdn = fleisch_ek.mdn;

    if (TestRow () == -1)
    {
                         
            eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                                  eListe.GetAktColumn ());
            return -1;
    }

    memcpy (&invpostab[row], &invpos, sizeof (struct INVPOS));
    if (ratod (invpostab[row].a) == 0.0)
    {
            eListe.DeleteLine ();
    }
    recs = eListe.GetRecanz ();
//	qsort (invpostab, recs, sizeof (struct INVPOS), sortlist);

	strcpy (akt_lief, " "); 

	DellAllPos ();
	for (i = 0; i < recs; i ++)
    {
		WritePos (i);

    }
    eListe.BreakList ();
	commitwork ();
    return 0;
}


void BESTPLIST::SaveBest (void)
{
    save_fkt (5);
    save_fkt (6);
    save_fkt (7);
    save_fkt (8);
    save_fkt (12);
}

void BESTPLIST::SetBest (void)
{
    set_fkt (dokey5, 5);
    set_fkt (InsertLine, 6);
    set_fkt (DeleteLine, 7);
    set_fkt (AppendLine, 8);
    set_fkt (WriteAllPos, 12);
}

void BESTPLIST::RestoreBest (void)
{
    restore_fkt (5);
    restore_fkt (6);
    restore_fkt (7);
    restore_fkt (8);
    restore_fkt (12);
}


int BESTPLIST::dokey5 (void)
/**
Listenerfassung abbrechen.
**/
{
	short sql_sav;
	extern short sql_mode;

    if (abfragejn (mamain1, "Positionen speichern ?", "J"))
    {
           WriteAllPos ();
           syskey = KEY12;
    }

	sql_sav = sql_mode;
	sql_mode = 1;
/*
    DbClass.sqlin ((short *) &best_kopf.mdn, 1, 0);
    DbClass.sqlin ((short *) &best_kopf.fil, 1, 0);
    DbClass.sqlin ((long *)  &best_kopf.best_blg, 2, 0);
    DbClass.sqlcomm ("delete from best_pos where mdn = ? and fil = ? "
                     "and best_blg = ? and me = 0");
    
	sql_mode = sql_sav; 
    syskey = KEY5;
    RestoreBest ();

    eListe.SetListFocus (0);
    eListe.SetFeldFocus0 (eListe.GetAktRow (), eListe.GetAktColumn ());
*/

	rollbackwork ();
    eListe.BreakList ();
    return 1;
}


void BESTPLIST::InitSwSaetze (void)
{
       int i;

       for (i = 0; i < MAXPOS; i ++)
       {
               SwSaetze [i] = (char *) &invpostab [i];
       }
}

void BESTPLIST::SetRecHeight (void)
/**
Anzahl Zeilen pro Listzeile ermitteln.
**/
{
       int height;
       int i;

       height = 0;
       for (i = 0; i < dataform.fieldanz; i ++)
       {
           if (dataform.mask[i].attribut & REMOVED) continue;
           if (dataform.mask[i].pos[0] > height)
           {
               height = dataform.mask[i].pos[0];
           }
       }
       height ++; 
       eListe.SetRecHeight (height);
}

int BESTPLIST::ToMemory (int pos)
/**
SW-Satz in Ascii-Format Speichern.
**/
{

       memcpy (SwSaetze[pos], ausgabesatz, zlen);
       eListe.SetRecanz (pos + 1);
       return 0;
}


void BESTPLIST::SetStringEnd (char *feldname, int len)
/**
Stringende Setzen.
**/
{

       return;
}


void BESTPLIST::uebertragen (void)
/**
Eingabesatz in ASCII-Striktur uebertragen.
**/
{
	   static int cursor = -1;
//	   short mdn, fil;
//       int dsqlstatus;

/*
	   if (cursor == -1 && (GetFieldAttr ("a_best") & REMOVED) == 0)
	   {
                 DbClass.sqlin ((short *) &mdn,    1, 0);
                 DbClass.sqlin ((short *) &fil,    1, 0);
                 DbClass.sqlin ((long *)  &best_kopf.lief,   0, 17);
                 DbClass.sqlin ((char *)  invpos.a, 0,14);
                 DbClass.sqlout ((char *) invpos.lief_best, 0,17);
                 cursor = DbClass.sqlcursor ("select lief_best from a_best "
                                     "where mdn = ? "
                                     "and   fil = ? "
                                     "and   lief = ? "
                                     "and   a = ?");
	   }
*/
       lese_a_bas (fleisch_ek.a);

       sprintf (invpos.posi,        "%hd", fleisch_ek.posi);
       sprintf (invpos.a,           "%13.0lf", fleisch_ek.a);
//       sprintf (invpos.a_bz1,       "%s",      _a_bas.a_bz1);
       sprintf (invpos.a_bz1,       "%s %s",      clipped(_a_bas.a_bz1),_a_bas.a_bz2);
       sprintf (invpos.a_bz2,       "%s",      _a_bas.a_bz2);
       sprintf (invpos.last_pr,      "%lf",   fleisch_ek.preis);
       dlong_to_asc (fleisch_ek.gue_ab, invpos.last_gue);
	   if (lese_a_kalkpreis == 0)
	   {
			sprintf (invpos.pr_ek,      "%lf",   0.0);
			sprintf (invpos.pr_sk,      "%lf",   0.0);
			sprintf (invpos.pr_fil_ek,      "%lf",   0.0);
	   }
	   else
	   {
	        sprintf (invpos.pr_ek,      "%lf",   fleisch_ek.preis);
//	        sprintf (invpos.pr_vollk,      "%lf",   fleisch_ek.pr_vollk);
	        sprintf (invpos.pr_sk,      "%lf",   fleisch_ek.sk);
	        sprintf (invpos.pr_fil_ek,   "%lf",   fleisch_ek.fil_ek);
	   }
       sprintf (invpos.me_einh,     "%hd",     fleisch_ek.me_einh);

       ReadMeEinh ();
}

void BESTPLIST::InitBestList (void)
/**
Ram fuer Liste initialisieren.
**/
{
  	    strcpy (invpos.last_pr, "0");
  	    strcpy (invpos.last_gue, "0");
	    strcpy (invpostab[0].last_pr, "0");
	    strcpy (invpostab[0].last_gue, "0");
        InitSwSaetze ();
        eListe.SetRecanz (0);
}

void BESTPLIST::ShowDB (short mdn, short fil, long invdat)
/**
Bestellpositionen lesen und anzeigen.
**/
{
        int i;
        HCURSOR oldcursor;
        extern short do_exit;
        int dsqlstatus;
		int cursor;
		char cheute [12];

		sysdate (cheute);

        InitSwSaetze ();
        eListe.SetUbForm (&ubform);
        oldcursor = SetCursor (LoadCursor (NULL, IDC_WAIT));
        i = eListe.GetRecanz ();
		if (i == 0)
		{
          InitBestList ();
		  fleisch_ek.gue_ab = invdat;
		  fleisch_ek.dat = dasc_to_long (cheute);
		  InvDat = invdat;
		  fleisch_ek.mdn = mdn;
		  DbClass.sqlin ((short *) &fleisch_ek.mdn, 1, 0);
		  DbClass.sqlin ((long *) &fleisch_ek.gue_ab, 2, 0);
		  cursor = DbClass.sqlcursor ("select mdn "
			                        "from fleisch_ek "
									"where fleisch_ek.mdn = ? "
									"and fleisch_ek.gue_ab = ? ");
		  dsqlstatus = DbClass.sqlfetch (cursor);

		  if (lese_a_kalkpreis == 1 ) dsqlstatus = 100;

		  DbClass.sqlclose (cursor);

		  DbClass.sqlin ((short *) &fleisch_ek.mdn, 1, 0);

		  if (dsqlstatus)
		  {
			    einstieg_default = TRUE;
				if (lese_a_kalkpreis == 1)
				{
				    DbClass.sqlin ((long *) &fleisch_ek.gue_ab, 2, 0);
					DbClass.sqlout ((double *) &fleisch_ek.dat, 2, 0);
					DbClass.sqlout ((double *) &fleisch_ek.a, 3, 0);
					DbClass.sqlout ((double *) &fleisch_ek.preis, 3, 0);
					DbClass.sqlout ((double *) &fleisch_ek.sk, 3, 0);
					DbClass.sqlout ((double *) &fleisch_ek.fil_ek, 3, 0);
					cursor = DbClass.sqlcursor ("select a_kalkpreis.dat, a_kalkpreis.a, a_kalkpreis.mat_o_b,a_kalkpreis.hk_teilk, a_kalkpreis.sk_vollk "
			                        "from a_kalkpreis "
									"where a_kalkpreis.mdn = ? "
									"and a_kalkpreis.dat = ? "
									"order by a_kalkpreis.a");
				}
				else
				{
				    DbClass.sqlin ((long *) &defaultdat, 2, 0);
					DbClass.sqlout ((double *) &fleisch_ek.dat, 2, 0);
					DbClass.sqlout ((double *) &fleisch_ek.a, 3, 0);
					DbClass.sqlout ((long *) &fleisch_ek.posi, 2, 0);
					DbClass.sqlout ((long *) &fleisch_ek.rowid, 2, 0);
					cursor = DbClass.sqlcursor ("select fleisch_ek.dat, fleisch_ek.a, fleisch_ek.posi,fleisch_ek.rowid "
			                        "from fleisch_ek "
									"where fleisch_ek.mdn = ? "
									"and fleisch_ek.dat = ? "
									"order by fleisch_ek.a");
				}
		  }
		  else
		  {
			  einstieg_default = FALSE;
			  DbClass.sqlin ((long *) &fleisch_ek.gue_ab, 2, 0);
			  DbClass.sqlin ((long *) &defaultdat, 2, 0);
			  DbClass.sqlout ((double *) &fleisch_ek.dat, 2, 0);
			  DbClass.sqlout ((double *) &fleisch_ek.a, 3, 0);
			  DbClass.sqlout ((long *) &fleisch_ek.posi, 2, 0);
			  DbClass.sqlout ((long *) &fleisch_ek.rowid, 2, 0);
			  cursor = DbClass.sqlcursor ("select fleisch_ek.dat, fleisch_ek.a, fleisch_ek.posi,fleisch_ek.rowid "
			                        "from fleisch_ek "
									"where fleisch_ek.mdn = ? "
									"and fleisch_ek.gue_ab = ? and fleisch_ek.dat <> ?"
									"order by fleisch_ek.a");
		  }



		  dsqlstatus = DbClass.sqlfetch (cursor);
          while (dsqlstatus == 0)
		  {
					if (lese_a_kalkpreis == 0)  dsqlstatus = fleisch_ek_class.dbreadfirst ();
                     uebertragen ();
                     if (ToMemory (i) != 0) break;
                     i = eListe.GetRecanz ();
                     if (i >= MAXPOS) break; 
               		 dsqlstatus = DbClass.sqlfetch (cursor);
		  }
		  DbClass.sqlclose (cursor);
		}

        SetRecHeight ();
        SetCursor (oldcursor);
        i = eListe.GetRecanz ();

		if (i == 1 && ratod (invpostab[0].a) == 0.0)
		{
			i = 0;
		}

        eListe.SetRecanz (i);
        SwRecs = eListe.GetRecanz ();
 
//150606        SetFieldAttr ("a", DISPLAYONLY);
        eListe.SetDataForm0 (&dataform, &lineform);
        eListe.SetSaetze (SwSaetze);
        eListe.SetChAttr (ChAttr); 
        eListe.SetUbRows (ubrows); 
        if (i == 0)
        {
			     Posanz = 0;
                 eListe.AppendLine ();
				 AktRow = AktColumn = 0;
                 eListe.SetPos (AktRow, AktColumn);
                 i = eListe.GetRecanz ();
        }
		else
		{
			     Posanz = i;
		}
        SendMessage (eListe.Getmamain2 (), WM_SIZE, NULL, NULL);
        memcpy (&invpos, &invpostab[0], sizeof (struct INVPOS));
        InvalidateRect (eListe.Getmamain3 (), 0, TRUE);
}


void BESTPLIST::ReadDB (short mdn, short fil, char *bdat)
/**
Auftragspositionen lesen.
**/
{
        ausgabesatz = (unsigned char *) &invpos;
        zlen = sizeof (struct INVPOS);

        PageView = 0;
        eListe.SetPageView (0);
        feld_anz = dataform.fieldanz;
        Lstzlen = zlen;
        LstSatz = ausgabesatz;

        Lstbanz = feld_anz;

        eListe.SetPos (AktRow, AktColumn);
        eListe.Setbanz (feld_anz);
        eListe.Setzlen (zlen);
        eListe.Initscrollpos ();
        eListe.SetAusgabeSatz (ausgabesatz);
}

void BESTPLIST::DestroyWindows (void)
{
       eListe.DestroyListWindow ();
       DestroyMainWindow ();
       InitMax ();
       InitMin ();
       MoveMamain1 ();
       ListAktiv = 0;
       SetFkt (11, leer, NULL);
       set_fkt (NULL, 11);
}

void BESTPLIST::SetSchirm (void)
{
       set_fkt (Schirm, 11);
       SetFkt (11, vollbild, KEY11);
}

void BESTPLIST::ShowBestp (short mdn, short fil, char *ldat)
/**
Auftragsliste bearbeiten.
**/

{
//	   int pos;

       if (ListAktiv) return; 

       Geta_bz2_par ();
       Geta_kum_par ();
	   GetCfgValues ();
	   /*
	   if (FormOK == FALSE)
	   {
           if (DelLadVK)
		   {
		         DelFormField (&ubform, 9);
		         DelFormField (&dataform, 9);
		         DelFormField (&lineform, 8);
		         lineform.mask[lineform.fieldanz - 1].pos[1] =
                             lineform.mask[lineform.fieldanz - 2].pos[1] + 
                             ubform.mask[ubform.fieldanz - 1].length;
		   }
           if (DelLastMe)
		   {
		         DelFormField (&ubform, 5);
		         DelFormField (&dataform, 5);
		         DelFormField (&lineform, 4);
		         lineform.mask[lineform.fieldanz - 1].pos[1] =
                             lineform.mask[lineform.fieldanz - 2].pos[1] + 
                             ubform.mask[ubform.fieldanz - 1].length;
		   }
		   FormOK = TRUE;
	   }
	   */
       fleisch_ek.mdn = mdn;
       fleisch_ek.dat = dasc_to_long (ldat);

   	   add = FALSE;
	   aufme_old = (double) 0.0;
	   DestroyPlus ();
       SetSchirm ();
       eListe.SetInfoProc (InfoProc);
       sprintf (InfoCaption, "Bestellung vom %s", ldat);
       mamain1 = CreateMainWindow ();
       eListe.InitListWindow (mamain1);
       ReadDB (mdn, fil, ldat);

       eListe.SetListFocus (0);
       eListe.Initscrollpos ();
       AktRow = 0;
       AktColumn = 0;
       scrollpos = 0;
       SetAktivWindow (eListe.Getmamain2 ());
       ShowDB (mdn, fil, fleisch_ek.dat);

       eListe.SetRowItem ("a", invpostab[0].a);
       
       ListAktiv = 1;

       return;
}

void BESTPLIST::GetListColor (COLORREF *color, char *cfg_v)
/**
Farbunterlegung fuer Listen uebertragen.
**/
{
	static char *ColTxt[] = {"BLACKCOL",
		                     "WHITECOL",
							 "BLUECOL",
							 "REDCOL",
							 "LTGRAYCOL",
							 "GREENCOL",
							 "YELLOWCOL",
							 "DKYELLOWCOL",
							 NULL};
	
	static COLORREF ColVal[] = {BLACKCOL,
		                        WHITECOL,
					  		    BLUECOL,
							    REDCOL,
							    LTGRAYCOL,
							    GREENCOL,
							    YELLOWCOL,
								DKYELLOWCOL};
	int i;
	int red, blue, green;
	char *ColR;
	int Cols = 0;

	for (i = 0; ColTxt[i]; i ++)
	{
		if (strcmp (ColTxt[i], cfg_v) == 0)
		{
			*color = ColVal[i];
			return;
		}
	}


	ColR = strstr (cfg_v, "red");
	if (ColR)
	{
		red = atoi (&ColR[3]);
		Cols ++;
	}
	ColR = strstr (cfg_v, "blue");
	if (ColR)
	{
		blue = atoi (&ColR[4]);
		Cols ++;
	}
	ColR = strstr (cfg_v, "green");
	if (ColR)
	{
		green = atoi (&ColR[5]);
		Cols ++;
	}
	if (Cols == 3)
	{
		*color = RGB (red, green, blue);
	}

}

void BESTPLIST::SetPreisTest (int mode)
{
	if (mode == 0) return;

	preistest = min (4, max (1, mode));
	if (preistest == 3)
	{
		SetItemAttr (&dataform, "pr_vk", DISPLAYONLY);
	}
}

void BESTPLIST::SetNoRecNr (void)
{
	   static BOOL SetOK = FALSE;
	   int i;

	   if (SetOK) return;

	   for (i = 0; i < dataform.fieldanz; i ++)
	   {
		   dataform.mask[i].pos[1] -= 6;
	   }
	   for (i = 0; i < ubform.fieldanz; i ++)
	   {
		   ubform.mask[i].pos[1] -= 6;
	   }
	   for (i = 0; i < lineform.fieldanz; i ++)
	   {
		   lineform.mask[i].pos[1] -= 6;
	   }
	   eListe.SetNoRecNr (TRUE);
       SetOK = TRUE;
}


void BESTPLIST::GetCfgValues (void)
/**
Werte aus 51100.cfg holen.
**/
{
	   static BOOL cfgOK = FALSE;
       char cfg_v [512];

	   if (cfgOK) return;

	   cfgOK = TRUE;
       if (ProgCfg.GetCfgValue ("LiefBzgnSchreiben", cfg_v) ==TRUE)
       {
                    LiefBzgnSchreiben = atoi (cfg_v);
       }
       else
       {
                   LiefBzgnSchreiben = 0;
       }
       if (ProgCfg.GetCfgValue ("lese_a_kalkpreis", cfg_v) ==TRUE)
       {
                    lese_a_kalkpreis = atoi (cfg_v);
       }
       else
       {
                   LiefBzgnSchreiben = 0;
       }
        if (ProgCfg.GetCfgValue ("aufschlag_sk", cfg_v) == TRUE)
        {
                    auf_sk = ratod (cfg_v);
        }
        else
        {
                    auf_sk = 0.0;
        }
        if (ProgCfg.GetCfgValue ("mit_kosten", cfg_v) == TRUE)
        {
                    mit_kosten = atoi (cfg_v);
        }
        else
        {
                    mit_kosten = 0;
        }
        if (ProgCfg.GetCfgValue ("aufschlag_fil_ek", cfg_v) == TRUE)
        {
                    auf_fil_ek = ratod (cfg_v);
        }
        else
        {
                    auf_fil_ek = 0.0;
        }
       if (ProgCfg.GetCfgValue ("plu_size", cfg_v) ==TRUE)
       {
                    plu_size = atoi (cfg_v);
       }
       else
        {
                    plu_size = 0;
        }
        if (ProgCfg.GetCfgValue ("auf_me_default", cfg_v) == TRUE)
        {
                    auf_me_default = atoi (cfg_v);
        }
        else
        {
                    auf_me_default = 0;
        }
        if (ProgCfg.GetCfgValue ("searchadirect", cfg_v) == TRUE)
		{
			        searchadirect = min (1, atoi (cfg_v));
		}
        if (ProgCfg.GetCfgValue ("searchmodedirect", cfg_v) == TRUE)
		{
			        searchmodedirect = min (1, atoi (cfg_v));
		}
        if (ProgCfg.GetCfgValue ("rowheight", cfg_v) == TRUE)
		{
			        RowHeight = ratod (cfg_v);
					if (RowHeight < (double) 1.0)
					{
						RowHeight = (double) 1.0;
					}
		}
        if (ProgCfg.GetCfgValue ("ubheight", cfg_v) == TRUE)
		{
			        UbHeight = atoi (cfg_v);
					SetUbHeight ();
		}
		eListe.SetRowHeight (RowHeight);
        if (ProgCfg.GetCfgValue ("listfocus", cfg_v) == TRUE)
		{
			        ListFocus = min (4, atoi (cfg_v));
					ListFocus = max (3, ListFocus);
		}
        if (ProgCfg.GetCfgValue ("matchcode", cfg_v) == TRUE)
		{
			         SetMatchCode (atoi (cfg_v));
		}
        if (ProgCfg.GetCfgValue ("bsd_kz", cfg_v) == TRUE)
		{
			         bsd_kz = atoi (cfg_v);

		}
        if (ProgCfg.GetCfgValue ("rab_prov_kz", cfg_v) == TRUE)
		{
			         rab_prov_kz = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("auf_wert_anz", cfg_v) == TRUE)
		{
//			         auf_wert_anz = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("a_kun_smt", cfg_v) == TRUE)
		{
			         a_kun_smt = atoi (cfg_v);
		}

        if (ProgCfg.GetCfgValue ("LISTCOLORS", cfg_v) == TRUE)
        {
		             ListColors =  min (1, max (0, atoi (cfg_v)));
        }

        if (ProgCfg.GetCfgValue ("KOMPFCOLOR", cfg_v) == TRUE)
        {
		             GetListColor (&KompfColor, cfg_v);
        }

        if (ProgCfg.GetCfgValue ("KOMPBCOLOR", cfg_v) == TRUE)
        {
		             GetListColor (&KompbColor, cfg_v);
        }
        if (ProgCfg.GetCfgValue ("SAFCOLOR", cfg_v) == TRUE)
        {
		             GetListColor (&SafColor, cfg_v);
					 MessCol = SafColor;
        }
        if (ProgCfg.GetCfgValue ("SABCOLOR", cfg_v) == TRUE)
        {
		             GetListColor (&SabColor, cfg_v);
					 MessBkCol = SafColor;
        }
/*
        if (ProgCfg.GetCfgValue ("sortstnd", cfg_v) == TRUE)
        {
		             StndAuf.SetSortMode (atoi (cfg_v));
        }
*/
        if (ProgCfg.GetCfgValue ("preistest", cfg_v) == TRUE)
        {
		             SetPreisTest (atoi (cfg_v));
        }

        if (ProgCfg.GetCfgValue ("sacreate", cfg_v) == TRUE)
        {
		             sacreate = min (1, max (0, (atoi (cfg_v))));
        }
        if (ProgCfg.GetCfgValue ("lad_vk", cfg_v) == TRUE)
        {
			         if (atoi (cfg_v))
					 {
						 DelLadVK = FALSE;
					 }
        }
        if (ProgCfg.GetCfgValue ("last_me", cfg_v) == TRUE)
        {
			         if (atoi (cfg_v))
					 {
						 DelLastMe = FALSE;
					 }
        }
        if (ProgCfg.GetCfgValue ("pr_ek", cfg_v) == TRUE)
        {
			         if (atoi (cfg_v))
					 {
						 DisplayonlyPrEK = TRUE;
					 }
        }
/*
        if (ProgCfg.GetCfgValue ("proptimize", cfg_v) == TRUE)
        {
			          WaPreis.SetOptimize (atoi (cfg_v));
        }
*/
        if (ProgCfg.GetGroupDefault ("pr_alarm", cfg_v) == TRUE)
        {
                      prproz_diff = ratod (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("art_un_tst", cfg_v) == TRUE)
        {
		             art_un_tst =  min (1, max (0, atoi (cfg_v)));
        }
        if (ProgCfg.GetCfgValue ("add_me", cfg_v) == TRUE)
        {
		             add_me =  min (1, max (0, atoi (cfg_v)));
        }
        if (ProgCfg.GetCfgValue ("a_kum", cfg_v) == TRUE)
        {
                     a_kum_par =  min (1, max (0, atoi (cfg_v)));
        }
        if (ProgCfg.GetCfgValue ("preis0_mess", cfg_v) == TRUE)
        {
                     preis0_mess = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("textinpmode", cfg_v) == TRUE)
        {
                     textinpmode = atol (cfg_v);
        }
       if (ProgCfg.GetCfgValue ("3DList", cfg_v) == TRUE)
       {
                    eListe.Set3D (atoi (cfg_v));
	   }
       if (ProgCfg.GetCfgValue ("3DSize", cfg_v) == TRUE)
       {
                    eListe.SetPlus3D (atoi (cfg_v));
	   }
       if (ProgCfg.GetCfgValue ("ListFocus", cfg_v) == TRUE)
       {
                    ListFocus = atoi (cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("NoRecNr", cfg_v) == TRUE)
       {
		            if (atoi (cfg_v))
					{
	                        SetNoRecNr ();
					}
	   }
	   /*********
       if (ProgCfg.GetCfgValue ("PrNachKomma", cfg_v) == TRUE)
       {
                    int nk        = atoi (cfg_v);
                    int ekpos     = GetItemPos (&dataform, "pr_ek");
                    int ek_btopos = GetItemPos (&dataform, "pr_ek_bto");
                    switch (nk)
                    {
                          case 2 :
                              if (ekpos >= 0)
                              {
                                  dataform.mask[ekpos].picture = "%6.2f";
                              }
                              if (ek_btopos >= 0)
                              {
                                  dataform.mask[ek_btopos].picture = "%6.2f";
                              }
                              break;
                          case 3 :
                              if (ekpos >= 0)
                              {
                                  dataform.mask[ekpos].picture = "%7.3f";
                              }
                              if (ek_btopos >= 0)
                              {
                                  dataform.mask[ek_btopos].picture = "%7.3f";
                              }
                              break;
                          case 4 :
                              if (ekpos >= 0)
                              {
                                  dataform.mask[ekpos].picture = "%8.4f";
                              }
                              if (ek_btopos >= 0)
                              {
                                  dataform.mask[ek_btopos].picture = "%8.4f";
                              }
                              break;       
                    }
       }
	   ********/
}


static char prabval [10];
static char pprovval [10];

static ITEM iprab  ("rab_satz",  prabval,   "Rabatt......:", 0);
static ITEM ipprov ("prov_satz", pprovval,  "Provision...:", 0);

static field _prab1 [] = {
&iprab,       8,  0, 1,10, 0, "%5.2f", EDIT, 0, 0, 0,
&iOK,        15, 0,  3, 4, 0, "", BUTTON, 0,EnterTest ,KEY12,
&iCA,        15, 0,  3,21, 0, "", BUTTON, 0,EnterTest ,KEY5,
};

static form prab1 = {3, 0, 0, _prab1, 0, 0, 0, 0, NULL};    

static field _prab2 [] = {
&ipprov,      8,  0, 1,10, 0, "%5.2f", EDIT, 0, 0, 0,
&iOK,        15, 0,  3, 4, 0, "", BUTTON, 0,EnterTest ,KEY12,
&iCA,        15, 0,  3,21, 0, "", BUTTON, 0,EnterTest ,KEY5,
};

static form prab2 = {3, 0, 0, _prab2, 0, 0, 0, 0, NULL};    

static field _prab3 [] = {
&iprab,       8,  0, 1, 10, 0, "%5.2f", EDIT, 0, 0, 0,
&ipprov,      8,  0, 2, 10, 0, "%5.2f", EDIT, 0, 0, 0,
&iOK,        15, 0,  4,  4, 0, "", BUTTON, 0,EnterTest ,KEY12,
&iCA,        15, 0,  4, 21, 0, "", BUTTON, 0,EnterTest ,KEY5,
};

static form prab3 = {4, 0, 0, _prab3, 0, 0, 0, 0, NULL};    

static form *prab;

void BESTPLIST::ChoiseLines (HWND eWindow, HDC hdc)
{
         static HPEN hPenG = NULL;
         static HPEN hPenW = NULL;
         TEXTMETRIC tm;
         RECT rect;
         int x, y;
         int cx, cy;

 		 if (eWindow == NULL) return;

		 memcpy (&tm, &textm, sizeof (tm)); 
         GetClientRect (eWindow, &rect);
         x = tm.tmAveCharWidth / 2;
         y = tm.tmHeight / 2;

         cx = rect.right - tm.tmAveCharWidth / 2;
         cy = rect.bottom - tm.tmHeight / 2;
         if (hPenG == NULL)
         {
                   hPenG = CreatePen (PS_SOLID, 0, GRAYCOL);
                   hPenW = CreatePen (PS_SOLID, 0, WHITECOL);
         }
         SelectObject (hdc, hPenG);

         MoveToEx (hdc, x, y, NULL);
         LineTo (hdc, cx, y);

         MoveToEx (hdc, x, y, NULL);
         LineTo (hdc, x, cy);

         MoveToEx (hdc, cx, y, NULL);
         LineTo (hdc, cx, cy);

         MoveToEx (hdc, x, cy, NULL);
         LineTo (hdc, cx, cy);

         SelectObject (hdc, hPenW);


         y ++;
         x ++;
         cx --;
         cy --;
         MoveToEx (hdc, x, y, NULL);
         LineTo (hdc, cx, y);

         MoveToEx (hdc, x, y, NULL);
         LineTo (hdc, x, cy);

         cx += 2;
         cy += 2;
         MoveToEx (hdc, cx, y, NULL);
         LineTo (hdc, cx, cy);

         MoveToEx (hdc, x, cy, NULL);
         LineTo (hdc, cx, cy);
}


HWND BESTPLIST::CreateEnter (void)
/**
Hauptfenster fuer Liste erzeugen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;
        HDC hdc;

        if (hMainWin == NULL) return NULL;    
           
        if (eWindow) return eWindow;

//        eListe.GetTextMetric (&tm);
	    memcpy (&tm, &textm, sizeof (tm)); 

        GetClientRect (hMainWin, &rect);
        GetWindowRect (hMainWin, &wrect);
       
        y = (wrect.bottom - 16 * tm.tmHeight);
        cx = 40 * tm.tmAveCharWidth;
        x = wrect.left + 2 + (rect.right - cx) / 2;
		if (rab_prov_kz < 3)
		{
                  cy = 7 * tm.tmHeight;
		}
		else
		{
                  cy = 9 * tm.tmHeight;
		}

        eWindow       = CreateWindow (
                                       "ListMain",
//                                       "hListWindow", 
                                       "",
                                       WS_DLGFRAME | 
                                       WS_POPUP,
                                       x, y,
                                       cx, cy,
                                       hMainWin,
                                       NULL,
                                       hMainInst,
                                    NULL);
        ShowWindow (eWindow, SW_SHOWNORMAL);
        UpdateWindow (eWindow);
        hdc = GetDC (eWindow);
        ChoiseLines (eWindow, hdc);
        ReleaseDC (eWindow, hdc);
        return eWindow;
}



long BESTPLIST::EnterPosRab (void)
/**
Nummer Eingeben.
*/
{
	     return 0l;
/*		 
          int currentf;

		  save_fkt (5);
		  save_fkt (6);
		  save_fkt (7);
		  save_fkt (8);
		  save_fkt (9);
		  save_fkt (10);
		  save_fkt (11);
		  save_fkt (12);
          set_fkt (EnterBreak, 5);
		  CreateEnter ();
          currentf = currentfield;
		  switch (rab_prov_kz)
		  {
		        case 1 :
			        prab = &prab1;
					break;
		        case 2 :
			        prab = &prab2;
					break;
		        case 3 :
			        prab = &prab3;
					break;
		  }
          sprintf (prabval , "%.2lf", (double) ratod (invpos.rab_satz));          
          sprintf (pprovval, "%.2lf", (double) ratod (invpos.prov_satz));          
          break_end ();
		  EnableWindows (hMainWin, FALSE);
          enter_form (eWindow, prab, 0, 0);
		  EnableWindows (hMainWin, TRUE);
		  DestroyWindow (eWindow);
		  eWindow = NULL;
          no_break_end ();
          currentfield = currentf;
          eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                             eListe.GetAktColumn ());
		  if (syskey != KEY5)
		  {
                   sprintf (invpos.rab_satz, "%.2lf", (double) ratod (prabval));          
                   sprintf (invpos.prov_satz, "%.2lf", (double) ratod (pprovval));          
                   memcpy (&invpostab[eListe.GetAktRow ()], &invpos, sizeof (struct INVPOS));
		  }
		  restore_fkt (5);
		  restore_fkt (6);
		  restore_fkt (7);
		  restore_fkt (8);
		  restore_fkt (9);
		  restore_fkt (10);
		  restore_fkt (11);
		  restore_fkt (12);
          return 0l;
*/
}
      

void BESTPLIST::EnterBestp (short mdn, short fil, char *invdat)
/**
Bestellliste bearbeiten.
**/

{
       static int initbestp = 0;
       if (DisplayonlyPrEK == TRUE)
	   {
			SetFieldAttr ("pr_ek", DISPLAYONLY);    
	   }
       Geta_bz2_par ();
       Geta_kum_par ();
//       Getbest_me_pr0 ();
	   GetCfgValues ();
   	   add = FALSE;
	   aufme_old = (double) 0.0;
	   DestroyPlus ();

	   akt_me = (double) 0.0;
       fleisch_ek.mdn = mdn;
       fleisch_ek.dat = dasc_to_long (invdat);

// Hier wird nur gelesen, um die Cursor zu preparieren.
	   
//  	   fil_invges_class.dbreadfirst ();
	   fleisch_ek_class.dbreadfirst ();

	   set_fkt (NULL, 8);

       set_fkt (dokey5, 5);
       set_fkt (AppendLine, 6);
       set_fkt (DeleteLine, 7);

//       if (rab_prov_kz) set_fkt (PosRab, 8);

       set_fkt (WriteAllPos, 12);
       set_fkt (Schirm, 11);

       SetFkt (6, einfuegen, KEY6);
       SetFkt (7, loeschen, KEY7);
       SetFkt (11, vollbild, KEY11);
       AktRow = 0;
       AktColumn = 0;
       if (initbestp == 0)
       {
                 eListe.SetInfoProc (InfoProc);
                 eListe.SetTestAppend (TestAppend);
                 sprintf (InfoCaption, "Inventur vom %s", invdat);
                 mamain1 = CreateMainWindow ();
                 eListe.InitListWindow (mamain1);
                 ReadDB (mdn, fil, invdat);
                 initbestp = 1;
       }

       eListe.SetListFocus (ListFocus);
       eListe.Initscrollpos ();
       scrollpos = 0;
       SetAktivWindow (eListe.Getmamain2 ());
       ShowDB (mdn, fil, fleisch_ek.dat);

       eListe.SetRowItem ("a", invpostab[0].a);
       SetRowItem ();

	   AnzBestWert ();
       ListAktiv = 1;
	   beginwork ();
       eListe.ProcessMessages ();

       InitMax ();
       InitMin ();
       MoveMamain1 ();
       ListAktiv = 0;
       SetFkt (6, leer, NULL);
       SetFkt (7, leer, NULL);
       SetFkt (8, leer, NULL);
       SetFkt (11, leer, NULL);
       set_fkt (NULL, 11);
	   CloseAufw ();
	   DestroySa ();
       eListe.DestroyListWindow ();
       DestroyMainWindow ();
       initbestp = 0;
//	   current_form = savecurrent;
       return;
}



void BESTPLIST::DestroyMainWindow (void)
/**
Hauptfenster fuer Liste loeschen.
**/
{
        if (mamain1 == NULL) return;
        DestroyWindow (mamain1);
        mamain1 = NULL;
}


HWND BESTPLIST::CreateMainWindow (void)
/**
Hauptfenster fuer Liste erzeugen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;

        if (hMainWindow == NULL) return NULL;    
           
        if (mamain1) return mamain1;

        eListe.GetTextMetric (&tm);

        GetClientRect (hMainWindow, &rect);
        GetWindowRect (hMainWindow, &wrect);


		if (liney > 0)
		{
			y = liney;
		}
		else
        {			 
            y = (wrect.bottom - 15 * tm.tmHeight);
        }
        x = wrect.left + 2;
        cy = wrect.bottom - y - 2;
        cx = rect.right;

        mamain1 = CreateWindow ("ListMain",
                                    InfoCaption,
                                    WS_THICKFRAME | 
//                                    WS_CAPTION | 
//                                    WS_CHILD |
                                    WS_POPUP |
                                    WS_SYSMENU |
                                    WS_MINIMIZEBOX |
                                    WS_MAXIMIZEBOX,
                                    x, y,
                                    cx, cy,
                                    hMainWindow,
                                    NULL,
                                    hMainInst,
                                    NULL);
        ShowWindow (mamain1, SW_SHOWNORMAL);
        UpdateWindow (mamain1);
        return mamain1;
}

static int IsMax = 0;
static int IsMin = 0;

void BESTPLIST::SetMax0 (int val)
{
    IsMax = val;
    if (IsMax)
    {
             SetFkt (11, fenster, KEY11);
    }
    else
    {
             SetFkt (11, vollbild, KEY11);
    }
}

void BESTPLIST::SetMin0 (int val)
{
    IsMin = val;
}


int BESTPLIST::Schirm (void)
{
         if (IsMin) return 0;
         
         if (IsMax)
         {
             SendMessage (mamain1, WM_SYSCOMMAND, SC_RESTORE, 0l);
             IsMax = 0;
         }
         else
         {
             SendMessage (mamain1, WM_SYSCOMMAND, SC_MAXIMIZE, 0l);
             IsMax = 1;
         }
         return 0;
}


void BESTPLIST::MoveMamain1 ()
{
        RECT rect;
        RECT wrect;
        int x, y, cx, cy;
        TEXTMETRIC tm;

        if (mamainmin) return;
        eListe.GetTextMetric (&tm);
        GetClientRect (hMainWindow, &rect);
        GetWindowRect (hMainWindow, &wrect);
        if (mamainmax)
        {
                 x = wrect.left + 2;
                 y = wrect.top + 2;
                 cy = wrect.bottom - y - 2;
                 cx = rect.right;
        }
        else
        {
 		         if (liney)
				 {
			            y = liney;
				 }
				 else
				 {
                        y = (wrect.bottom - 15 * tm.tmHeight);
				 }
                 x = wrect.left + 2;
                 cy = wrect.bottom - y - 2;
                 cx = rect.right;
        }
        MoveWindow (mamain1, x,y, cx, cy, TRUE);
}


void BESTPLIST::MaximizeMamain1 ()
{
        RECT rect;
        RECT wrect;
        int x, y, cx, cy;

        GetClientRect (hMainWindow, &rect);
        GetWindowRect (hMainWindow, &wrect);
        x = wrect.left + 2;
        y = wrect.top + 2;
        cy = wrect.bottom - y - 2;
        cx = rect.right;
        MoveWindow (mamain1, x,y, cx, cy, TRUE);
}



static char aufwert [20];

static ITEM iaufwert  ("auf_wert",  aufwert,   "Inventurwert   ", 0);

static field _faufwert [] = {
&iaufwert,       14,  0, 1, 1, 0, "%10.2f", READONLY, 0, 0, 0,
};

static form faufwert = {1, 0, 0, _faufwert, 0, 0, 0, 0, NULL};    


void BESTPLIST::MoveAufw (void)
/**
Hauptfenster fuer Liste erzeugen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;

        if (hMainWin == NULL) return;    
        if (AufMehWnd == NULL) return;    
           
	    memcpy (&tm, &textm, sizeof (tm)); 

        GetClientRect (hMainWin, &rect);
        GetWindowRect (hMainWin, &wrect);

/*		
        y = (wrect.top +  2);
        cx = 34 * tm.tmAveCharWidth;
        x = wrect.left + rect.right - cx + 1;
        cy = 4 * tm.tmHeight;
*/

        y = (wrect.top +  2);
        cx = 40 * tm.tmAveCharWidth + 4;
        x = wrect.left + rect.right - cx + 1;
        cy = 4 * tm.tmHeight + 8;
		MoveWindow (AufMehWnd, x, y, cx, cy, TRUE);
}


HWND BESTPLIST::CreateAufw (void)
/**
Hauptfenster fuer Liste erzeugen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;
        HDC hdc;
		HWND eWindow;

        if (hMainWin == NULL) return NULL;    
           
	    memcpy (&tm, &textm, sizeof (tm)); 

        GetClientRect (hMainWin, &rect);
        GetWindowRect (hMainWin, &wrect);

/*		
        y = (wrect.top +  2);
        cx = 34 * tm.tmAveCharWidth;
        x = wrect.left + rect.right - cx + 1;
        cy = 4 * tm.tmHeight;
*/

        y = (wrect.top +  2);
        cx = 34 * tm.tmAveCharWidth + 4;
        x = wrect.left + rect.right - cx + 1;
        cy = 4 * tm.tmHeight + 8;

        eWindow       = CreateWindow (
                                       "StaticWhite",
                                       "",
                                       WS_DLGFRAME | 
                                       WS_POPUP,
                                       x, y,
                                       cx, cy,
                                       hMainWin,
                                       NULL,
                                       hMainInst,
                                    NULL);
        ShowWindow (eWindow, SW_SHOWNORMAL);
        UpdateWindow (eWindow);

        AufMehWnd0      = CreateWindowEx (
			                           WS_EX_CLIENTEDGE, 
                                       "StaticWhite",
                                       "",
									   WS_CHILD | WS_VISIBLE,
                                       2, 4,
                                       cx - 11, cy - 14,
                                       eWindow,
                                       NULL,
                                       hMainInst,
                                       NULL);

        hdc = GetDC (eWindow);
//        ChoiseLines (eWindow, hdc);
        ReleaseDC (eWindow, hdc);
        return eWindow;
}


void BESTPLIST::CloseAufw (void)
{
        if (auf_wert_anz == 0) return;

        if (AufMehWnd)
		{
			CloseControls (&faufwert);
            if (AufMehWnd0)
			{
			         DestroyWindow (AufMehWnd0);
 			         AufMehWnd0 = NULL;
			}
			DestroyWindow (AufMehWnd);
			AufMehWnd = NULL;
		}
}
			

void BESTPLIST::AnzBestWert (void)
/**
Auftragswert anzeigen.
**/
{
          int currentf;
		  int i;
		  double wert;
		  int recanz;

      
          if (auf_wert_anz == 0) return;

		  recanz = eListe.GetRecanz ();
		  if (recanz == 0)
		  {
			  if (AufMehWnd) 
			  {
				  DestroyWindow (AufMehWnd);
				  AufMehWnd = NULL;
			  }
			  return;
		  }
          if (AufMehWnd == NULL)
		  {
		            AufMehWnd = CreateAufw ();
					if (AufMehWnd == NULL) return;
		  }
		  wert = 0;
		  for (i = 0; i < recanz; i ++)
		  {
//			      wert = wert + ratod (invpostab[i].last_me) *
//					            ratod (invpostab[i].pr_ek);
          }

		  SetStaticWhite (TRUE);
		  sprintf (aufwert, "%08.4lf", wert);
		  display_form (AufMehWnd0, &faufwert, 0, 0); 
		  SetStaticWhite (FALSE);

          currentfield = currentf;
}
      

HWND BESTPLIST::GetMamain1 (void)
{
       return (mamain1);
}

void BESTPLIST::SethwndTB (HWND hwndTB)
{
         this->hwndTB = hwndTB; 
         eListe.SethwndTB (hwndTB);
}

void BESTPLIST::SetTextMetric (TEXTMETRIC *tm)
{
         memcpy (&textm, tm, sizeof (TEXTMETRIC));
         eListe.SetTextMetric (tm);
}


void BESTPLIST::SetLineRow (int LineRow)
{
         eListe.SetLineRow (0);
}

void BESTPLIST::SetListLines (int i)
{ 
//         TListe.SetListLines (i);
         eListe.SetListLines (i);
}

void BESTPLIST::OnPaint (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	    HDC hdc;

        eListe.OnPaint (hWnd, msg, wParam, lParam);
        if (hWnd == eWindow)  
        {
                    hdc = BeginPaint (eWindow, &aktpaint);
                    ChoiseLines (eWindow, hdc); 
                    EndPaint (eWindow, &aktpaint);
        }
        else if (hWnd == AufMehWnd)  
        {
                    hdc = BeginPaint (AufMehWnd, &aktpaint);
//                    ChoiseLines (AufMehWnd, hdc); 
                    EndPaint (AufMehWnd, &aktpaint);
        }
        else if (hWnd == BasishWnd)  
        {
                    hdc = BeginPaint (BasishWnd, &aktpaint);
                    ChoiseLines (BasishWnd, hdc); 
                    EndPaint (BasishWnd, &aktpaint);
        }
        else if (hWnd == SaWindow)  
        {
                    hdc = BeginPaint (hWnd, &aktpaint);
                    PaintSa (hdc); 
                    EndPaint (hWnd, &aktpaint);
        }
        else if (hWnd == PlusWindow)  
        {
                    hdc = BeginPaint (hWnd, &aktpaint);
                    PaintPlus (hdc); 
                    EndPaint (hWnd, &aktpaint);
        }
}




void BESTPLIST::MoveListWindow (void)
{
        eListe.MoveListWindow ();
}


void BESTPLIST::BreakList (void)
{
        eListe.BreakList ();
}


void BESTPLIST::OnHScroll (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
        eListe.OnHScroll (hWnd, msg,wParam, lParam);
}

void BESTPLIST::OnVScroll (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
        eListe.OnVScroll (hWnd, msg,wParam, lParam);
}

void BESTPLIST::OnSize (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
        if (hWnd == GetMamain1 ())
        {
                    MoveListWindow ();
                    MoveAufw ();
                    MoveSaW ();
                    MovePlus ();
        }
}


void BESTPLIST::StdAngebot (void)
{
         if (eListe.Getmamain3 () == NULL) return; 
}

void BESTPLIST::FunkKeys (WPARAM wParam, LPARAM lParam)
{
	/*
	                if (TListe.Getmamain3())
					{
                            TListe.FunkKeys (wParam, lParam);
					}
					else
					{
                            eListe.FunkKeys (wParam, lParam);
					}
*/                          eListe.FunkKeys (wParam, lParam);

}


int BESTPLIST::GetRecanz (void)
{
          return eListe.GetRecanz ();
}


void BESTPLIST::SwitchPage0 (int rows)
{
           eListe.SwitchPage0 (rows);
}


HWND BESTPLIST::Getmamain2 (void)
{
           return eListe.Getmamain2 ();
}


HWND BESTPLIST::Getmamain3 (void)
{
           return eListe.Getmamain3 ();
}


void BESTPLIST::ChoiseFont (mfont *lfont)
{
           eListe.ChoiseFont (lfont);
}

void BESTPLIST::SetFont (mfont *lfont)
{
           eListe.SetFont (lfont);
}

void BESTPLIST::SetListFont (mfont *lfont)
{
           eListe.SetListFont (lfont);
}

void BESTPLIST::FindString (void)
{
           eListe.FindString ();
}


void BESTPLIST::SetLines (int Lines)
{
           eListe.SetLines (Lines);
}


int BESTPLIST::GetAktRow (void)
{
                 return eListe.GetAktRow ();
}

int BESTPLIST::GetAktRowS (void)
{
                 return eListe.GetAktRowS ();
}

void BESTPLIST::SetColors (COLORREF Color, COLORREF BkColor)
{
                 eListe.SetColors (Color, BkColor); 
}

void BESTPLIST::SetListFocus (void)
{
                 eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                                       eListe.GetAktColumn ()); 
}

void BESTPLIST::PaintUb (void)
{
                 eListe.PaintUb (); 
}


/***************************************/
/*    Funkton liefert Jahr (short)     */
/*                  Jahr 4 Stellen */
/*-------------------------------------*/
/*    Format des Datums: TTMMJJJJ      */
/*                       TT.MM.JJJJ    */
/***************************************/
short BESTPLIST::get_jahr( char* datum)
{
char  dat[16];
strcpy(dat,datum);
if(strlen(dat) == 8)
      return((short)(atoi(&dat[4])));
else
      return((short)(atoi(&dat[6])));
}

/***************************************/
/*    Funkton liefert monat(short)     */
/***************************************/
short get_monat(char *datum)
{
char  dat[16];
strcpy(dat,datum);
dat[5]='\0';
return((short)(atoi(&dat[3])));
}

