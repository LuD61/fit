#ifndef _A_PR_DEF
#define _A_PR_DEF

#include "dbclass.h"

struct A_PR {
   double    a;
   short     akt;
   short     delstatus;
   short     fil;
   short     fil_gr;
   double    key_typ_dec13;
   short     key_typ_sint;
   char      lad_akv[2];
   char      lief_akv[2];
   short     mdn;
   short     mdn_gr;
   char      modif[2];
   double    pr_ek;
   double    pr_vk;
   long      bearb;
   char      pers_nam[9];
   double    pr_vk1;
   double    pr_vk2;
   double    pr_vk3;
   double    pr_ek_euro;
   double    pr_vk_euro;
};
extern struct A_PR a_pr, a_pr_null;

#line 7 "a_pr.rh"

class A_PR_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               A_PR_CLASS () : DB_CLASS ()
               {
               }
               int dbreadfirst (void);
};
#endif
