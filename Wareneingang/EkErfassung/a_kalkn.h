#ifndef _A_KALKN_DEF
#define _A_KALKN_DEF

#include "dbclass.h"

struct A_KALKN {
   short     mdn;
   short     fil;
   double    a;
   long      gue_ab;
   double    ek;
   double    sk;
   double    fil_ek;
};
extern struct A_KALKN a_kalkn, a_kalkn_null;

#line 7 "a_kalkn.rh"

class A_KALKN_CLASS : public DB_CLASS
{
       private :
               int cursor_o;
               int del_best_curs;
               void prepare (void);
       public :
               A_KALKN_CLASS () : DB_CLASS ()
               {
               }
               int dbreadfirst (void);
               int dbclose (void);
               int dbinsert (void);

};
#endif
