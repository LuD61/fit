#ifndef _MO_MTXT_DEF
#define _MO_MTXT_DEF

class MTXT
{
          private :
			  HWND hMainWindow;
			  HINSTANCE hInstance;
			  HWND hWnd;
              mfont *Font;
			  DWORD currentfield;
                    static int listpos;
			  static int (*OkFunc) (int); 
			  static int (*OkFuncE) (char *); 
			  static int (*DialFunc) (int); 
              static int (*FillDb) (char *); 
			  static char *EBuff;
			  static char *buffer;
			  static char *EBuff0;
              static long aktnr;
			  static int textidx;
              static int (*Read) (long);
              int cx, cy;
          public :
			  MTXT (int, int, char *, char *, char *, int (*) (long));
			  MTXT (int, int, char *, char *, int (*) (long));
			  ~MTXT ();
               HWND GethWnd (void)
               {
                  return hWnd;
               } 
 		      BOOL ProcessMessages (void);
              static char *strtok (char *, char *);
              static char *FirstRow (char *, int);
              static char *NextRow (char *, int);
              static int SaveTxt (void);
              static int ReadTxt (void);
              static void ShowDlg (HDC, form *);
              static CALLBACK CProc(HWND,UINT, WPARAM,LPARAM);
			  static void SetOkFunc (int (*) (int));
			  static void SetOkFuncE (int (*) (char *));
			  static void SetDialFunc (int (*) (int));
			  static void SetFillDb (int (*) (char *));
              static int  GetSel (void);
              static void  SetSel (int);
              static char *GetText (void);
              static void  SetTextNr (char *);
			  static char *GetTextNr (void);
              static void InitText (void);
              static void AddText (char *);
              static void ReplaceText (char *, BOOL);
              static void SetText (void);
              static void MoveWindow (void);
              void OpenWindow (HANDLE, HWND);
              void DestroyWindow (void);
			  void SetCurrentID (DWORD);
			  void SetCurrentName (char *);
			  void EnableID (DWORD, BOOL);
			  void EnableName (char *, BOOL);
			  void CheckID (DWORD, BOOL);
			  void CheckName (char *, BOOL);
              void VLines (char *, int);
			  void InsertCaption (char *);
			  void InsertRecord  (char *);
			  void UpdateRecord  (char *, int);
              void GetText (char *);
              BOOL TestButtons (HWND);
              BOOL TestEdit (HWND);
			  void SetRead (int (*) (long));
};
#endif

