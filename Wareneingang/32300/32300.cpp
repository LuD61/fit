#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include "comcthlp.h"
#include "itemc.h"
#include "wmaskc.h"
#include "mo_intp.h"
#include "dbclass.h"
#include "mo_menu.h"
#include "sys_par.h"
#include "mdn.h"
#include "fil.h"
#include "lief.h"
#include "mo_auto.h"
#include "mdnfil.h"
#include "mo_meld.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "listcl.h"
#include "inflib.h"
#include "mo_arg.h"
#include "mo_curso.h"
#include "mo_qa.h"
#include "best_kopf.h"
#include "best_pos.h"
#include "a_bas.h"
#include "ptab.h"
#include "mo_bestl.h"
#include "bmlib.h"
#include "message.h"
#include "auto_nr.h"
#include "mo_progcfg.h"
#include "mo_lsgen.h"
#include "mo_inf.h"
#include "mo_chq.h"
#include "mo_chqex.h"
#include "mo_vers.h"
#include "mo_kf.h"
#include "mo_kompl.h"
#include "ls_txt.h"
#include "mo_txt.h"
#include "lbox.h"
#include "mo_wmess.h"
#include "mo_gdruck.h"

#define MAXLEN 40
#define MAXPOS 3000
#define LPLUS 1
 
#define IDM_FRAME     901
#define IDM_REVERSE   902
#define IDM_NOMARK    903
#define IDM_EDITMARK  904
#define IDM_PAGEVIEW  905
#define IDM_LIST      906
#define IDM_PAGE      907
#define IDM_FONT      908
//#define IDM_DELETE    909
#define IDM_ANZBEST   910
#define IDM_STD       911
#define IDM_BASIS     912
#define IDM_LGR       913
#define IDM_LGRDEF    914
#define IDM_POSTEXT   915
//#define IDM_VINFO     916
#define IDM_FIND      917
//#define IDM_PRCHOISE  918

#define QUERYBEST      920 
#define QUERYLIEF      921 

#define TLEN 60 

static BOOL NewStyle = FALSE;

int      PASCAL WinMain(HANDLE,HANDLE,LPSTR,int);
LONG FAR PASCAL WndProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL StaticWProc(HWND,UINT,WPARAM,LPARAM);
static int ProcessMessages(void);
void     InitFirstInstance(HANDLE);
BOOL     InitNewInstance(HANDLE, int);
void     DisplayLines ();
void     MoveButtons (void);
void     PrintButtons (void);
void     ShowDaten (void);
static   void FreeBestk (void);
static   int testlief0 (void);
static   void 	EnableArrows (form *, BOOL);
static void InputTxt (char *, long);
static void EnablePrn (void);
static void DisablePrn (void);

static int dokey5 (void);
static int dokey12 (void);
static int doliste (void);
static int KopfTexte (void);
static int FussTexte (void);
static int InputAdr (void);
static void IncDatum (char *);

static char stat_defp_von [3] = {"1"};
static char stat_defp_bis [3] = {"3"};;
static char stat_deff_von [3] = {"1"};
static char stat_deff_bis [3] = {"4"};;
static char tou_def_von [10]  = {"1"};
static char tou_def_bis [10]  = {"9999"};
static char abt_def_von [6]   = {"0"};
static char abt_def_bis [6]   = {"9999"};


static BOOL NoClose      = FALSE;
static BOOL NoStartMenue = FALSE;
static BOOL InStartMenue = FALSE;

extern BOOL ColBorder;


HANDLE  hMainInst;
HWND   hMainWindow;
HWND   mamain1;

static long  datum_plus = 0;
static long  ldatdiff = 365;
static BOOL  lsdirect = FALSE;
static BOOL gen_div_adr = FALSE;
static long  bdatdiff = 365;

static BOOL StartMen = TRUE;
static BOOL PrintMode = 0;
static int  Startsize = 0;


HWND ForegroundWindow;

void GetForeground (void)
{
	ForegroundWindow = GetForegroundWindow ();
}

void SetForeground (void)
{
	SetForegroundWindow (ForegroundWindow);
}

extern HWND  ftasten;
extern HWND  ftasten2;

static int WithMenue   = 1;
static int WithToolBar = 1;

static   TEXTMETRIC tm;

static int PageView = 0;

static char *TXT_WORK   = "Bearbeiten";
static char *TXT_SHOW   = "Anzeigen";
static char *TXT_DEL    = "L�schen";
static char *TXT_PRINT  = "Drucken";

static char *TXT_ACTIVE = TXT_WORK;


static int IDM_ACTIVE = IDM_WORK;

static PAINTSTRUCT aktpaint;

HMENU hMenu;

#define CallT1  850
#define CallT2  851
#define CallT3  852

struct PMENUE dateimen[] = {
	                        "&1 Bearbeiten", " ",  NULL, IDM_WORK, 
                            "&2 Anzeigen",   " ",  NULL, IDM_SHOW,
						    "&3 L�schen",    " ",  NULL, IDM_DELETE,
						    "&4 Drucken",    " ",  NULL, IDM_PRINT,
							"",              "S",  NULL, 0, 
						    "&5 Druckerwahl"," ",  NULL, IDM_PRCHOISE,
							"",              "S",  NULL, 0, 
	                        "B&eenden",      " ",  NULL, KEY5,

                             NULL, NULL, NULL, 0};
struct PMENUE bearbeiten[] = {
	                        "A&bbruch F5",                 " ", NULL, KEY5, 
							"Spe&ichern F12",              " ", NULL, KEY12,
							"",                            "S",   NULL, 0, 
	                        "&Fax-Nummer",                 "G", NULL, CallT1, 
	                        "&Telefon-Nummer",             "G", NULL, CallT2, 
	                        "&Preisgruppen",               "G", NULL, CallT3, 
							"",                            "S",   NULL, 0, 
	                        "&Suchen",                     "G", NULL, IDM_FIND, 
                            "&Positionstexte",             "G", NULL, IDM_POSTEXT, 
                             NULL, NULL, NULL, 0};


struct PMENUE ansicht[] = {
                            "&Fonteinstellung",            "G", NULL, IDM_FONT,
                             NULL, NULL, NULL, 0};

struct PMENUE menuetab[] = {"&Datei",      "M", dateimen,   0, 
                            "&Bearbeiten", "M", bearbeiten, 0, 
                            "&Ansicht",    "M", ansicht,    0, 
                            "&?",          " ", NULL,       IDM_VINFO, 
						     NULL, NULL, NULL, 0};

static int ActiveMark = IDM_FRAME;

static char InfoCaption [80] = {"\0"};


extern HWND hWndToolTip;
HWND hwndTB;
HWND hwndCombo1;
HWND hwndCombo2;

static TBBUTTON tbb[] =
{
 0,               IDM_WORK,   TBSTATE_ENABLED | TBSTATE_CHECKED, 
                              TBSTYLE_CHECKGROUP, 
 0, 0, 0, 0,
 1,               IDM_SHOW,   TBSTATE_ENABLED, 
	                          TBSTYLE_CHECKGROUP,
 0, 0, 0, 0,
 2,               IDM_DELETE,   TBSTATE_INDETERMINATE, 
                                TBSTYLE_CHECKGROUP,
 0, 0, 0, 0,
 3,               IDM_PRINT,  TBSTATE_ENABLED, 
                              TBSTYLE_BUTTON,
 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED,
                  TBSTYLE_SEP, 
 0, 0, 0, 0,
 6,               IDM_INFO,   TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
 
 7,               KEYTAB, TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
 8,               KEYSTAB, TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
 9,               KEYDOWN, TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
10 ,              KEYUP, TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,

 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,

 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
11 ,              IDM_LGR, TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
12 ,              IDM_LGRDEF, TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
};


static char *Version[] = {"  Bestellungen  ",
                          "  Programm    32300", 
                          "  Standard",
						  "  Versions-Nr    1.0",
						   NULL,
};

HWND    QuikInfo = NULL;

void     CreateQuikInfos (void);
static int CallTele1 ();
static int CallTele2 ();
static int CallTele3 ();


static int MoveMain = FALSE;
static HWND DlgWindow = NULL;

static int lief_sperr = 1;
static long akt_lager = 0;
static BOOL bestdatum = FALSE;
static BOOL use_kopf_txt = FALSE;
static int KomplettMode = 0;
static BOOL autosysdat = FALSE;
static BOOL textinpmode = TRUE;


HICON telefon1;
HICON fax;

HBITMAP btelefon;
HBITMAP btelefoni;

ColButton CTelefon = { 
                       NULL, 0, 0,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       btelefon, -1, -1,
                       NULL, 0, 0,
                       RGB (255, 255, 255),
                       GRAYCOL,
                       -1};

ColButton CFax =    { "Fax", -1, -1,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       RGB (255, 255, 255),
                       GRAYCOL,
                       -1};
ColButton CGruppen =    { "PG", -1, -1,
                          NULL, 0, 0,
                          NULL, 0, 0,
                          NULL, 0, 0,
                          NULL, 0, 0,
                          RGB (255, 255, 255),
                          GRAYCOL,
                          -1};

static ITEM itelefon1 ("", (char *) &CTelefon, "", 0);
static ITEM ifax      ("", (char *) &CFax,     "", 0);
static ITEM iGruppen  ("", (char *) &CGruppen, "", 0);

static ITEM iOK      ("", "    OK     ", "", 0);
static ITEM iCancel  ("", " Abbrechen ", "", 0);

static field _buform [] = {
&ifax,             4, 2, 2,73, 0, "", COLBUTTON, 0, 0,
                                                           CallT1,
&itelefon1,        4, 2, 4,73, 0, "", COLBUTTON, 0, 0,
                                                           CallT2,
&iGruppen,         4, 2, 6,73, 0, "", REMOVED, 0, 0,
                                                           CallT3,
};

static form buform = {0, 0, 0, _buform, 0, 0, 0, 0, NULL};



static char *qInfo [] = {"Bearbeiten",
                         "Anzeigen",
                         "L�schen",
                         "Drucken",
                         "Funktion f�r aktives Eingabefeld",
                         "S�tze in Listenform anzeigen",
                          0, 
                          0};

static UINT qIdfrom [] = {IDM_WORK, IDM_SHOW, IDM_DELETE,
                         IDM_PRINT, IDM_INFO,
                         IDM_LIST, IDM_STD,IDM_LGR, IDM_LGRDEF, 0, 0};

static char *qhWndInfo [] = {"Tabellen-Linien festlegen", 
                             "Farbe f�r Vordergrund und Hintergrund "
                             "festlegen",
                             "Fax-Nummer",
                             "Telefon-Nummer",
                             "Preisgruppen",
                             "Abbrechen",
                             "Speichern",
                             0};

static HWND *qhWndFrom [] = {&hwndCombo1, 
                             &hwndCombo2,
                             &buform.mask[0].feldid,
                             &buform.mask[1].feldid,
                             &buform.mask[2].feldid,
                             NULL,
                             NULL,
                             NULL}; 


static char *Combo1 [] = {"wei�e Tabelle" ,
                          "hellgraue Tabelle",
                          "schwarze Tabelle",
                          "graue Tabelle",
                          "vertikal wei�",
                          "vertikal hellgrau",
                          "vertikal schwarz", 
                          "vertikal grau",
                          "horizontal wei�",
                          "horizontal hellgrau",
                          "horizontal schwarz", 
                          "horizontal grau",
                          "keine Linien",
                           NULL};

static char *Combo2 [] = {"wei�er Hintergrund" ,
                          "blauer Hintergrund",
                          "schwarzer Hintergrund",
                          "grauer Hintergrund",
                           NULL};

static COLORREF Colors[] = {BLACKCOL,
                            WHITECOL,
                            WHITECOL,
                            WHITECOL};

static COLORREF BkColors[] = {WHITECOL,
                            BLUECOL,
                            BLACKCOL,
                            GRAYCOL};

static COLORREF MessBkCol = DKYELLOWCOL; 
static COLORREF MessCol   = BLACKCOL; 

static int combo1pos = 0;
static int combo2pos = 0;

mfont lFont = {
               "Courier New", 
               120, 
               100, 
               0, 
               RGB (-1, 0, 0), 
               RGB (-1, 0, 0),
               0, 0};

static int m3Size = 0;

static BEST_KOPF_CLASS bestk_class;
static BEST_POS_CLASS  bestp_class;
static KompBest BestKompl;
static LIEF_CLASS lief_class;
static BESTPLIST bestpListe;
static MENUE_CLASS menue_class;
static FIL_CLASS fil_class;
static ADR_CLASS adr_class;
static QueryClass QClass;
static DB_CLASS DbClass;
static AUTO_CLASS AutoClass;
static PROG_CFG ProgCfg ("32300");
static PTAB_CLASS ptab_class;
static KINFO Kinfo;
static VINFO Vinfo;
static WMESS Wmess;
static LSTXTLIST lstxtlist;

extern MELDUNG Mess;
extern SYS_PAR_CLASS sys_par_class;

static char mdn [10] = {"0"};
static char fil [10] = {"0"};

static BOOL FreeBestNr = TRUE;

/* ****************************************************************/
/* Angebotsskopf    
                                              */
static int mench = 0;

static BOOL ldat_ok = FALSE;
static long akt_best = 0l;
static long akt_tou = 0l;
static char akt_lief [17] = {" "};
static long adr_nr = 0;

char akt_ldat[12];
char akt_bdat[12];

static char *lief_blank = "                ";

char best_nr[9] = "0";
char liefs[17];
char bdat[12] = {" "};
char ldat[12];
char best_stat[2];
char best_stat_txt[40];
char lf_auftrag[17];
char l_kurzb[17];
char tele[17];
char htext [80];

static int dsqlstatus;
static int inDaten = 0;
static HBITMAP ArrDown;

static int lesebest (void);
static int leselief (void);
static int setkey9lief (void);
static int savebdat (void);
static int saveldat (void);
static int testbdat (void);
static int testldat (void);
static int setkey10_11 (void);
static int testhtext (void);
static int SwitchArt (void);
static int QueryBest (void);


static ITEM ibest_nr ("best_nr", best_nr,     "Bestell-Nr    ", 0); 
static ITEM ikunfil_bz 
                    ("",           "Kunde", "", 0); 
static ITEM iarrdown ("arrdown",  " < ",     "", 0);

static field _bestformk [] = {
&ibest_nr,                 9, 0, 2, 1, 0, "%8d", EDIT,       0, lesebest, 0,
&iarrdown,                 2, 0, 2,25, 0, "B",    BUTTON,     0, 0, QUERYBEST, 
};

static form bestformk = {2, 0, 0, _bestformk, 0, 0, 0, 0, NULL};


static ITEM iliefs   ("lief",      liefs, "Lieferant     ", 0); 
static ITEM ibdat   ("best_term",  bdat,   "Bestelldatum  ", 0);
static ITEM ildat   ("we_dat",     ldat,   "WE-Datum      ", 0);
static ITEM ibest_stat
                    ("bearb_stat", best_stat, "Status     ", 0);
static ITEM ibest_stat_txt
                    ("best_stat_txt", best_stat_txt, "", 0);
static ITEM ilf_auftrag
                    ("lf_auftrag", lf_auftrag, "Lf-Auftrag ", 0); 
static ITEM il_kurzb ("lief_krz", l_kurzb,    "", 0);
static ITEM itele   ("tele",       _adr.tel,   "TelNr ", 0);

static ITEM ihtext  ("",           htext,  "Hinweistext   ", 0); 

static field _bestform [] = {
&iliefs,                     17, 0, 4,1, 0, "", REMOVED,   setkey9lief, 
                                                            leselief, 0,

&iarrdown,                    2, 0, 4,33, 0, "B",    REMOVED,     0, 0, QUERYLIEF, 
&ibdat,                      11, 0, 2,1, 0, "dd.mm.yyyy", NORMAL,
                                                            savebdat, testbdat,0,
&ildat,                      11, 0, 3,1, 0, "dd.mm.yyyy", NORMAL,
                                                            saveldat, testldat,0,
&ihtext,                    49, 0, 4,1, 0, "", NORMAL,      setkey10_11, testhtext,0,
&ibest_stat,                  2, 0, 2,40, 0, "", REMOVED, 0, testkeys,0,
&ibest_stat_txt,             13, 0, 2,55, 0, "", REMOVED, 0, 0, 0, 
&il_kurzb,                   16, 0, 4,35, 0, "", REMOVED, 0, testkeys,0,
&itele,                      16, 0, 4,52, 0, "", REMOVED, 0, testkeys,0,
};

static form bestform = {9, 0, 0, _bestform, 0, 0, 0, 0, NULL};

struct AARROW
{
	char item[21];
	int pos;
	BOOL active;
};

struct AARROW activearrows[] = {"lief",  1,  TRUE,
 							    "",     -1,  FALSE};

static int lieffield = 0;

static char *aktart = liefart;
static char *aktzei = a_bz2_on;
static int NewRec = 0;

static int  anzart = 20;
static char AnzBest [10];

static int testanzb (void)
{
	if (syskey == KEY5 || syskey == KEY12)
	{
		break_enter ();
	}

	if (atoi (AnzBest) > 100)
	{
		strcpy (AnzBest, "100");
	}
	else if (atoi (AnzBest) < 5)
	{
		strcpy (AnzBest, "5");
	}
	return 0;
}

static ITEM iAnzBestT ("anzbest",  "Anzahl Bestellartikel", "", 0);
static ITEM iAnzBest ("anzbest",   AnzBest, "", 0);

// static ITEM iAnzBest ("anzbest",   AnzBest, "Anzahl Bestellartikel", 0);

static field _fAnzBest [] = {
    &iAnzBestT,  22, 0, 1, 1, 0, "", DISPLAYONLY, 0, 0, 0,
    &iAnzBest,    7, 0, 1,24, 0, "%4d", EDIT | UPDOWN, 0, testanzb, 0,
    &iOK,          12, 0, 3, 5, 0, "",  BUTTON,   0,0, KEY12,
    &iCancel,      12, 0, 3,19, 0, "",  BUTTON,   0,0, KEY5};

static form fAnzBest = {4,0, 0, _fAnzBest, 0, 0, 0, 0, NULL};

static struct UDTAB udtab[] = {100, 5, 10};
static int udanz = 1;

void SetAnzBest (void)
/**
String suchen.
**/
{
        HWND hAnzBest;
        int end_break;
        int currents;

        udtab[0].Pos = anzart;
        currents = currentfield;
        end_break = GetBreakEnd ();
        break_end ();
        sprintf (AnzBest, "%d", anzart);
        EnableWindows (mamain1, FALSE);
        SetBorder (WS_POPUP | WS_VISIBLE | WS_DLGFRAME);
        hAnzBest = OpenWindowChC (5,36,10, 20, hMainInst,
                   "");
        SetUpDownTab (udtab , udanz);
        enter_form (hAnzBest, &fAnzBest, 0, 0);
		if (syskey != KEY5)
		{
                 anzart = atoi (AnzBest);
		}
        EnableWindows (mamain1, TRUE);
        DestroyWindow (hAnzBest);
        SetBreakEnd (end_break);
        SetAktivWindow (mamain1);
        SetActiveWindow (mamain1);
        currentfield = currents;
        SetCurrentFocus (currentfield);
}


int CallTele1 (void)
/**
Waehlen ausfuehren.
**/
{
        clipped (_adr.fax);
        if (strcmp (_adr.fax, " ") <= 0)
        {
                  disp_mess ("Keine Fax-Nummer", 0);
        }
        else
        {
                  print_mess (0, "Fax Nummer %s", _adr.fax);
        }
        DestroyWindow (QuikInfo);
        CreateQuikInfos ();
        return 0;
}

int CallTele2 (void)
/**
Waehlen ausfuehren.
**/
{
        clipped (_adr.tel);
        if (strcmp (_adr.tel, " ") <= 0)
        {
                  disp_mess ("Keine Telefon-Nummer", 0);
        }
        else
        {
                  print_mess (0, "Telefon Nummer %s", _adr.tel);
        }
        CreateQuikInfos ();
        return 0;
}

int CallTele3 (void)
/**
Waehlen ausfuehren.
**/
{
	    Kinfo.InfoKunF (hMainInst, hMainWindow, best_kopf.mdn, best_kopf.mdn,NULL);
        CreateQuikInfos ();
        return 0;
}

int InfoVersion (void)
/**
Waehlen ausfuehren.
**/
{
	    Vinfo.VInfoF (hMainInst, hMainWindow, Version);
        return 0;
}

void CreateQuikInfos (void)
/**
QuikInfos generieren.
**/
{
     BOOL bSuccess ;
     TOOLINFO ti ;

     if (QuikInfo) DestroyWindow (QuikInfo);
     QuikInfo = CreateWindow (TOOLTIPS_CLASS,
                                "",
                                WS_POPUP,
                                0, 0,
                                0, 0,
                                hMainWindow,
                                NULL,
                                hMainInst,
                                NULL);


     ZeroMemory (&ti, sizeof (TOOLINFO)) ;
     ti.cbSize = sizeof (TOOLINFO) ;
     ti.uFlags = TTF_IDISHWND | TTF_CENTERTIP | TTF_SUBCLASS ;
     ti.hwnd   = mamain1 ;
     ti.uId    = (UINT) (HWND) buform.mask[0].feldid;
     ti.lpszText = LPSTR_TEXTCALLBACK ;
     bSuccess = ToolTip_AddTool (QuikInfo, &ti) ;
     ti.uId    = (UINT) (HWND) buform.mask[1].feldid;
     ti.lpszText = LPSTR_TEXTCALLBACK ;
     bSuccess = ToolTip_AddTool (QuikInfo, &ti) ;
     ti.uId    = (UINT) (HWND) buform.mask[2].feldid;
     ti.lpszText = LPSTR_TEXTCALLBACK ;
     bSuccess = ToolTip_AddTool (QuikInfo, &ti) ;
     SetFktQuikInfo (QuikInfo, &ti);
}


int InfoLief (void)
{
        char where [80];

        if (strcmp (liefs, " "))
		{
                                     sprintf (where, "where lief = %s", liefs);
        }
        else
        {
                        sprintf (where, "where lief > \" \"");
        }
        _CallInfoEx (hMainWindow, NULL,
							      "lief", 
								  where, 0l);
        return 0;
}


BOOL Lock_Bestk (void)
/**
Angebot sperren.
**/
{
    short sqlm;
    extern short sql_mode;

    sqlm = sql_mode;
    sql_mode = 1;

	best_kopf.delstatus = -1;

	dsqlstatus = bestk_class.dbupdate ();

	best_kopf.delstatus = 0;
    if (dsqlstatus < 0)
    {
	    print_mess (2, "Bestellung %ld wird im Moment bearbeitet", best_kopf.best_blg);
        sql_mode = sqlm;
        return FALSE;
    }
    sql_mode = sqlm;
    return TRUE;
}


void GetBeststatTxt (char *beststxt)
/**
Klartext fuer Auftragsstatus holen.
**/
{
	  char wert [5];

	  sprintf (wert, "%hd", best_kopf.bearb_stat);
	  ptabn.ptbez[0] = 0;
      ptab_class.lese_ptab ("bearb_stat", wert);
	  strcpy (beststxt, ptabn.ptbez);
}


static void SetLiefAttr (void)
/**
Atribut auf Feld Kunde setzen.
**/
{
	   return;
/*	   
	   int liefarrowpos;

	   if (lief_sperr == FALSE) return;

	   liefarrowpos = GetItemPos (&bestform, "lief") + 1;
	   if (bestpListe.GetPosanz () == 0)
	   {
	            SetItemAttr (&bestform, "lief", EDIT);
				bestform.mask[liefarrowpos].attribut = BUTTON;
	   }
	   else
	   {
	            SetItemAttr (&bestform, "lief", READONLY);
				bestform.mask[liefarrowpos].attribut = REMOVED;
	   }
*/
}

	       
static void BesttoForm (void)
/**
Bestellsdaten in Form uebertragen.
**/
{

          sprintf (liefs,   "%s", best_kopf.lief);
		  lief.mdn = atoi (mdn);
		  strcpy (lief.lief, best_kopf.lief);
          dsqlstatus = lief_class.dbreadfirst ();
		  if (sqlstatus == 100 && lief.mdn > 0)
		  {
			        lief.mdn = 0;
                    dsqlstatus = lief_class.dbreadfirst ();
		  }
          if (dsqlstatus == 0)
		  {
                    dsqlstatus = adr_class.lese_adr (best_kopf.lief_adr);
		  }

          best_kopf.waehrung = atoi (_mdn.waehr_prim);
          strcpy (l_kurzb, _adr.adr_krz);
          clipped (_adr.tel);

/*
          if (strcmp (_adr.tel, " ") > 0)
          {
                   CTelefon.bmp = btelefon;
                   ActivateColButton (mamain1, 
                                      &buform, 1, 0, 1);
                   EnableMenuItem (hMenu, CallT2,   MF_ENABLED);
          }

          clipped (_adr.fax);
          if (strcmp (_adr.fax, " ") > 0)
          {
                   ActivateColButton (mamain1, 
                                      &buform,      0, 0, 1);
                   EnableMenuItem (hMenu, CallT1,   MF_ENABLED);
          }
          ActivateColButton (mamain1, &buform, 2, 0, 1);
          EnableMenuItem (hMenu, CallT3,   MF_ENABLED);
          MoveButtons ();
*/

          dlong_to_asc (best_kopf.best_term, bdat);
          dlong_to_asc (best_kopf.we_dat,    ldat);
          sprintf (best_stat, "%1d", best_kopf.bearb_stat);
		  GetBeststatTxt (best_stat_txt);
}


static void FormToBest (short best_stat)
/**
Form in Angebotssdaten uebertragen.
**/
{
	      char datum [12];

		  sysdate (datum);
          best_kopf.mdn       = atoi (mdn);
          best_kopf.fil       = atoi (fil);
          best_kopf.best_blg  = atol (best_nr);
          strcpy (best_kopf.lief, liefs);
		  if (numeric (liefs) && strlen (liefs) < 9)
		  {
			  best_kopf.lief_s = atol (liefs);
		  }
          best_kopf.best_term = dasc_to_long (bdat); 
          best_kopf.we_dat     = dasc_to_long (ldat); 
          best_kopf.bearb_stat = best_stat;
/*
          if (best_kopf.bearb_stat == 0)
          {
              best_kopf.bearb_stat = 1;
          }
*/
		  strcpy (best_kopf.pers_nam, sys_ben.pers_nam);
		  if (strcmp (best_kopf.pers_nam, " ") <= 0)
		  {
			  strcpy (best_kopf.pers_nam, "leer");
		  }
}


void IncDatum (char *datum)
/**
Systemdatum um dat_plsu erhoehen.
**/
{
	    long ldat;
		
        if (datum_plus == 0l) return;

        ldat = dasc_to_long (datum);
        ldat += datum_plus;
        dlong_to_asc (ldat, datum);
}


void SetBestdat (void)
/**
Defaultwert fuer Bestelldatum setzen.
**/
{
         char datum [12];

		 if (strcmp (bdat, "          ") <= 0 || autosysdat) 
		 {
                    sysdate (datum);
					IncDatum (datum);
                    strcpy (bdat, datum);
		 }
}


void SetLiefdat (void)
/**
Defaultwert fuer Lieferdatumdatum setzen.
**/
{
         char datum [12];

		 if (strcmp (ldat, "          ") <= 0 || autosysdat) 
		 {
                    sysdate (datum);
					IncDatum (datum);
                    strcpy (ldat, datum);
		 }
}


static void InitBest (void)
/**
bestform Initialisieren.
**/
{
          sprintf (liefs,"%16.16s", " ");
          strcpy (l_kurzb, " ");
          strcpy (_adr.tel, " ");
          strcpy (_adr.fax,  " ");
          SetBestdat ();
          SetLiefdat ();
          sprintf (best_stat, "%1d", 0);
}

void InsBestp (long best_nr, double a, long posi)
/**
Satz in Bestellsposition einfuegen.
**/
{
	       best_pos.mdn  = best_kopf.mdn;
	       best_pos.fil  = best_kopf.fil;
	       best_pos.best_blg  = best_nr;
	       best_pos.a    = a;
	       best_pos.p_num = (short) posi;
           if (bestp_class.dbreadfirst () != 0)
           {
               return;
           }
		   bestp_class.dbupdate ();
}


static int lesebest ()
/**
Angebot lesen.
**/
{
	     int dsqlstatus;

         if (testkeys ()) return 0;

         if (mench > 0 && akt_best && 
             (atol (best_nr) == akt_best)) 
         {
             return 0;
         }

		 adr_nr = 0l;
         bestpListe.DestroyWindows (); 
		 best_kopf.mdn = atoi (mdn);
		 best_kopf.fil = atoi (fil);
		 best_kopf.best_blg = atol (best_nr);
         dsqlstatus = bestk_class.dbreadfirst ();
         if (best_kopf.delstatus != 0) dsqlstatus = 100;
         if (dsqlstatus == 0 && mench == 0)
         {
                       if (Lock_Bestk () == FALSE) return 0;
         }
		 else if (mench == 2 && dsqlstatus == 0)
		 {
                       if (Lock_Bestk () == FALSE) return 0;
		 }

         if (dsqlstatus == 0 && mench == 0)
         {
                       NewRec = 0;
         }
         else if (mench == 0)
         {
                       NewRec = 1;
         }

         if (dsqlstatus == 0)
         {
			           adr_nr = best_kopf.lief_adr; 
                       BesttoForm ();  
         }

         else if (mench != 0 || best_kopf.best_blg != auto_nr.nr_nr)
         {
                       print_mess (2, "Bestellung %ld nicht gefunden",
                                   atol (best_nr));
					   if (mench != 1)
					   {
						   rollbackwork ();
						   beginwork ();
					   }
                       InitBest ();
					   if (mench == 0) 
					   {
						   sprintf (best_nr, "%ld",auto_nr.nr_nr);
					   }
					   else
					   {
                            strcpy (best_nr, "0");
					   }
                       display_form (mamain1, &bestformk, 0, 0);
                       return 0;
         }
         else
         {
			           best_kopf.bearb_stat = 0; 
                       if (Lock_Bestk () == FALSE) return 0;
                       memcpy (&best_kopf, &best_kopf_null, sizeof (struct BEST_KOPF));
                       InitBest ();
                       best_kopf.mdn = atoi (mdn);
                       best_kopf.fil = atoi (fil);
                       best_kopf.best_blg = atol (best_nr);
         }

         if (mench != 0)
         {
             ShowDaten ();
         }
         else
         {
             break_enter ();
         }
         akt_best = atol (best_nr);
         return 0;
}


static int testlief0 ()
{
         int lief_krz_field; 
		 int lief_field;
         int tele_field;
         
         lief_field = GetItemPos (&bestform, "lief");
         lief_krz_field = GetItemPos (&bestform, "lief_krz");
         tele_field = GetItemPos (&bestform, "tele");
         if (strcmp (liefs, lief_blank) <= 0)
         {
                  disp_mess ("Lieferant <= \" \" ist nicht erlaubt", 2);
                  SetCurrentField (lief_field);
                  return FALSE;
         }
		 lief.mdn = atoi (mdn);
		 strcpy (lief.lief, liefs);
         dsqlstatus = lief_class.dbreadfirst ();
		 if (dsqlstatus == 100 && lief.mdn > 0)
		 {
		         lief.mdn = 0;
                 dsqlstatus = lief_class.dbreadfirst ();
		 }
         if (dsqlstatus)
		 { 
                 disp_mess ("Lieferant nicht angelegt", 0);
                 SetCurrentField (lief_field);
                 return FALSE;
		 }
         dsqlstatus = adr_class.lese_adr (lief.adr);
         best_kopf.lief_adr = lief.adr;
         best_kopf.waehrung = atoi (_mdn.waehr_prim);
         strcpy (l_kurzb, _adr.adr_krz);
         best_kopf.waehrung = lief.waehrung;
         display_field (mamain1, &bestform.mask[tele_field], 0, 0);
		 return TRUE;
}


static int leselief0 ()
{
         int lief_krz_field;
         int lief_field;
 
		 clipped (liefs);
         lief_krz_field = GetItemPos (&bestform, "lief_krz");
         lief_field = GetItemPos (&bestform, "lief");
 	     if (!numeric (liefs))
		 {
			      lief.mdn = atoi (mdn);
                  QClass.searchlief_direct (mamain1, liefs);
		          sprintf (liefs, "%s", lief.lief);
				  if (strcmp (liefs, "0") <= 0)
				  {
                            current_form = &bestform;
                            SetCurrentField (currentfield);
                            display_field (mamain1, &bestform.mask[lief_field], 0, 0);
							return 0;
				  }
		 }


         SetFkt (9, leer, 0);
         set_fkt (NULL, 9);

//		 sprintf (kuns, "%8ld", atol (kuns));

/*
	     if (_adr.adr_typ != 25)
		 {
                 SetFkt (9, leer, 0);
                 set_fkt (NULL, 9);
		 }
*/

//         if (akt_kun == atol (kuns)) return 0;
         if (strcmp (liefs, "                ") == 0)
         {
                  disp_mess ("Lieferanten-Nummer eingeben", 2);
                  SetCurrentField (currentfield);
                  return 0;
         }
		 lief.mdn = atoi (mdn);
		 strcpy (lief.lief, liefs);
         dsqlstatus = lief_class.dbreadfirst ();
		 if (dsqlstatus == 100 && lief.mdn > 0)
		 {
		         lief.mdn = 0;
                 dsqlstatus = lief_class.dbreadfirst ();
		 }


         if (dsqlstatus)
		 {
                 disp_mess ("Lieferant nicht angelegt", 0);
                 SetCurrentField (currentfield);
                 return 0;
		 }
         dsqlstatus = adr_class.lese_adr (lief.adr);
		 {
				       best_kopf.lief_adr = _adr.adr;
		 }
         best_kopf.waehrung = atoi (_mdn.waehr_prim);
// 		  best_kopf.waehrung = kun.waehrung;  
         strcpy (l_kurzb, _adr.adr_krz);
//         akt_kun = atol (kuns);
         if (dsqlstatus == 0)
         {
                   clipped (_adr.tel);
/*
                   if (strcmp (_adr.tel, " ") > 0)
                   {
                          CTelefon.bmp = btelefon;
                          ActivateColButton (mamain1, 
                                      &buform, 1, 0, 1);
                         EnableMenuItem (hMenu, CallT2,   MF_ENABLED);
                   }
                   else
                   {
                          CTelefon.bmp = btelefoni;
                          ActivateColButton (mamain1, 
                                      &buform,      1, -1, 1);
                         EnableMenuItem (hMenu, CallT2,   MF_GRAYED);
                   }

                   clipped (_adr.fax);
                   if (strcmp (_adr.fax, " ") > 0)
                   {
                          ActivateColButton (mamain1, 
                                      &buform,      0, 0, 1);
                         EnableMenuItem (hMenu, CallT1,   MF_ENABLED);
                   }
                   else
                   {
                          ActivateColButton (mamain1, 
                                      &buform,      0, -1, 1);
                         EnableMenuItem (hMenu, CallT1,   MF_GRAYED);
                   }
                   ActivateColButton (mamain1, &buform, 2, 0, 1);
                   EnableMenuItem (hMenu, CallT3,   MF_ENABLED);
                   MoveButtons ();
*/
         }

//         MoveButtons ();

         current_form = &bestform;
         display_field (mamain1, &bestform.mask[lief_field], 0, 0);
         display_field (mamain1, &bestform.mask[lief_krz_field], 0, 0);
         return 1;
}

static int leselief ()
/**
Lieferant fuer Lieferantennummer holen.
**/
{
         if (testkeys ()) return 0;

		 clipped (liefs);
         if (testlief0 () == FALSE) return 1;
         return leselief0 ();
}

static int SetLief (char *liefc)
/**
Procedure wird aus der Listenerfassung aufgerufen und setzt den 
Lieferanten neu.
**/
{
	      if (strcmp (liefc, liefs) == 0)
		  {
			  return 0;
		  }
 		  strcpy (liefs, liefc);
          return leselief0 ();
}

long KopfTextGen (void)
/**
Kopftexte erfassen.
**/
{
           long text_nr;
           extern short sql_mode;


           sql_mode = 1;
           dsqlstatus = AutoClass.nvholid (0, 0, "ls_txt");
		   if (dsqlstatus == -1)
		   {
							DbClass.sqlcomm ("delete from auto_nr where nr_nam = \"ls_txt\" "
								              "and mdn = 0 and fil = 0");
							dsqlstatus = 100;
			}
					        
            if (dsqlstatus == 100)
            {
                           dsqlstatus = AutoClass.nvanmprf (0,
                                                  0,
                                                  "ls_txt",
                                                  (long) 1,
                                                  (long) 999999,
                                                  (long) 10,
                                                  "");

                           if (dsqlstatus == 0)
                           {
                                  dsqlstatus = AutoClass.nvholid (0,
                                                        0,
                                                        "ls_txt");
                           }
             }
             sql_mode = 0;
             if (auto_nr.nr_nr == 0l)
             {
                    disp_mess ("Es konnte keine text-Nr generiert werden", 2);
                    return 0l;
             }
			 text_nr = auto_nr.nr_nr;
			 return text_nr;
}


BOOL KopfTextExist (short mdn, short fil, char *lief, long kopt_txt)
/**
Test, ob die Nummer schon benutzt wird.
**/
{
	       int dsqlstatus;

	       DbClass.sqlin ((short *) &mdn, 1, 0); 
	       DbClass.sqlin ((short *) &fil, 1, 0); 
	       DbClass.sqlin ((long *)  &lief, 0, 17); 
	       DbClass.sqlin ((long *)  &kopt_txt, 2, 0); 
		   dsqlstatus = DbClass.sqlcomm ("select best_blg from best_kopf "
			                             "where mdn = ? "
										 "and fil = ? "
										 "and lief != ? "
										 "and best_txt = ?");
		   if (dsqlstatus == 0) return TRUE;
		   return FALSE;
}


int KopfTexte (void)
/**
Kopftexte erfassen.
**/
{

	       long nr_nr;
           long text_nr;

           nr_nr = auto_nr.nr_nr;
		   text_nr = best_kopf.best_txt;
		   if (text_nr == 0l)
		   {
		      while (TRUE)
			  {
			     text_nr = KopfTextGen ();
				 if (text_nr == 0l) return 0l;
			     if (KopfTextExist (best_kopf.mdn, best_kopf.fil, best_kopf.lief, text_nr) == FALSE) 
				 {
				                   break;
				 }
			  }

              auto_nr.nr_nr = 0;
               
//        Die Transtion wird an dieser Stelle geschlossen.

              FormToBest (0);
              bestk_class.dbupdate ();   
              FreeBestNr = FALSE; 
              commitwork ();

              if (best_kopf.best_blg != nr_nr)
			  {
                       beginwork ();
                       dsqlstatus = AutoClass.nveinid (atoi (mdn), 0,
                                            "best",  nr_nr);
                       commitwork ();
			  }
              beginwork ();
		   }
           best_kopf.best_txt = text_nr;

           set_fkt (NULL, 9);
           SetFkt (9, leer, 0);
           set_fkt (NULL, 10);
           SetFkt (10, leer, 0);
           set_fkt (NULL, 11);
           SetFkt (11, leer, 0);
           lstxtlist.SethMainWindow (mamain1);
           lstxtlist.SetListLines (12);
		   EnableWindow (bestpListe.GetMamain1 (), FALSE);
		   if (textinpmode == 0)
		   {
                     lstxtlist.EnterAufp ("Kopftext", text_nr);
		   }
		   else
		   {
			         InputTxt ("Kopftext",text_nr);
		   }
	       EnableWindow (bestpListe.GetMamain1 (), TRUE);
           SetActiveWindow (mamain1);
           SetCurrentFocus (currentfield);
    
           set_fkt (dokey5, 5);
           set_fkt (doliste, 6);
           set_fkt (dokey12, 12);
           set_fkt (NULL, 7);
           SetFkt (6, liste, KEY6);
           SetFkt (7, leer, 0);
           set_fkt (KopfTexte, 10);
//           set_fkt (FussTexte, 11);
           SetFkt (10, kopftext, KEY10);
//           SetFkt (11, fusstext, KEY11);
           return 0;
}

/*
int FussTexte (void)
/?*
Fusstexte erfassen.
*?/
{
           long nr_nr;
           long text_nr;
           extern short sql_mode;


           nr_nr = auto_nr.nr_nr;

           if (angk.fuss_txt == 0)
           {
                sql_mode = 1;
                dsqlstatus = AutoClass.nvholid (0, 0, "ls_txt");
				if (dsqlstatus == -1)
				{
							DbClass.sqlcomm ("delete from auto_nr where nr_nam = \"ls_txt\" "
								              "and mdn = 0 and fil = 0");
							dsqlstatus = 100;
				}
					        
                if (dsqlstatus == 100)
                {
                           dsqlstatus = AutoClass.nvanmprf (0,
                                                  0,
                                                  "ls_txt",
                                                  (long) 1,
                                                  (long) 999999,
                                                  (long) 10,
                                                  "");

                           if (dsqlstatus == 0)
                           {
                                  dsqlstatus = AutoClass.nvholid (0,
                                                        0,
                                                        "ls_txt");
                           }
                }
                sql_mode = 0;
                if (auto_nr.nr_nr == 0l)
                {
                    disp_mess ("Es konnte keine text-Nr generiert werden", 2);
                    return 0;
                }

                text_nr = auto_nr.nr_nr;

                auto_nr.nr_nr = 0;
               
        Die Transtion wird an dieser Stelle geschlossen.

                 FormToAng (1);
				 angk_class.dbupdate (); 
                 commitwork ();

                 if (angk.ang != nr_nr)
                 {
                       beginwork ();
                       dsqlstatus = AutoClass.nveinid (atoi (mdn), 0,
                                            "ang",  nr_nr);
                       commitwork ();
                 }
                 beginwork ();
                 angk.fuss_txt = text_nr;
           }
           else
           {
                  text_nr = angk.fuss_txt;
           }

           set_fkt (NULL, 9);
           SetFkt (9, leer, 0);
           set_fkt (NULL, 10);
           SetFkt (10, leer, 0);
           set_fkt (NULL, 11);
           SetFkt (11, leer, 0);
           lstxtlist.SethMainWindow (mamain1);
           lstxtlist.SetListLines (12);
		   EnableWindow (angpListe.GetMamain1 (), FALSE);
		   if (textinpmode == 0)
		   {
                     lstxtlist.EnterAufp ("Fu�text", text_nr);
		   }
		   else
		   {
			         InputTxt ("Fu�text", text_nr);
		   }
		   EnableWindow (angpListe.GetMamain1 (), TRUE);
           SetActiveWindow (mamain1);
           SetCurrentFocus (currentfield);
    
           set_fkt (dokey5, 5);
           set_fkt (doliste, 6);
           set_fkt (dokey12, 12);
           set_fkt (NULL, 7);
           SetFkt (6, liste, KEY6);
           SetFkt (7, leer, 0);
           set_fkt (KopfTexte, 10);
           set_fkt (FussTexte, 11);
           SetFkt (10, kopftext, KEY10);
           SetFkt (11, fusstext, KEY11);
           return 0;
}
*/

int setkey10_11 (void)
{
//       set_fkt (KopfTexte, 10);
//       set_fkt (FussTexte, 11);
//       SetFkt (10, kopftext, KEY10);
//       SetFkt (11, fusstext, KEY11);
       return 0;
}

static int BreakKomm (void)
{
       break_enter ();
       return 1;
}

static int SearchPos = 8;
static int OKPos = 9;
static int CAPos = 10;

struct SADR
{
	  long adr;
	  char adr_krz [17];
	  char adr_nam1 [37];
	  char adr_nam2 [37];
	  char pf [17];
	  char plz [9];
	  char str [37];
	  char ort1 [37];
	  char ort2 [37];
	  short adr_typ;
};

static struct SADR *sadrtab = NULL;
static struct SADR sadr;
static int idx;
static long adranz;
static CHQEX *Query;
static HWND adrwin;
static short adr_typ;
static long akt_adr;
static int testadr (void);

int saveadr (void)
/**
Aktuelle Adressnumer in akt_adr sichern.
**/
{
	  akt_adr = atol (current_form->mask[0].item->GetFeldPtr ());
	  return 0;
}

int readadr (void)
/**
Adresse lesen, wenn Adressnummer geaendert wurde.
**/
{
	  int dsqlstatus;

	  if (testadr ()) return 0;

	  sadr.adr = atol (current_form->mask[0].item->GetFeldPtr ());
	  if (sadr.adr == akt_adr) return 0;

      DbClass.sqlin ((long *) &sadr.adr, 2, 0);
      DbClass.sqlout ((char *) sadr.adr_krz, 0, 37);
      DbClass.sqlout ((char *) sadr.adr_nam1, 0, 37);
      DbClass.sqlout ((char *) sadr.adr_nam2, 0, 37);
      DbClass.sqlout ((char *) sadr.pf, 0, 17);
      DbClass.sqlout ((char *) sadr.plz, 0, 9);
      DbClass.sqlout ((char *) sadr.str, 0, 37);
      DbClass.sqlout ((char *) sadr.ort1, 0, 37);
      DbClass.sqlout ((char *) sadr.ort2, 0, 37);
      DbClass.sqlout ((short *) &sadr.adr_typ, 1, 0);

	  dsqlstatus = DbClass.sqlcomm ("select adr_krz,"
		                                   "adr_nam1,"   
		                                   "adr_nam2,"   
		                                   "pf,"   
		                                   "plz,"   
		                                   "str,"   
		                                   "ort1,"   
		                                   "ort2,"
										   "adr_typ "
										   "from adr where adr = ?");
	  if (dsqlstatus == 100) return 0;

	  if (sadr.adr_typ != 20)
	  {
		  disp_mess ("Falscher Adress-Typ", 2);
 	      sprintf (current_form->mask[0].item->GetFeldPtr (), "%ld",  akt_adr); 
	      display_form (adrwin, current_form);
          SetCurrentField (currentfield);
		  return -1;
	  }

	  sprintf (current_form->mask[1].item->GetFeldPtr (), "%s",  sadr.adr_nam1); 
	  sprintf (current_form->mask[2].item->GetFeldPtr (), "%s",  sadr.adr_nam2); 
	  sprintf (current_form->mask[3].item->GetFeldPtr (), "%s",  sadr.pf); 
	  sprintf (current_form->mask[4].item->GetFeldPtr (), "%s",  sadr.str); 
	  sprintf (current_form->mask[5].item->GetFeldPtr (), "%s",  sadr.plz); 
	  sprintf (current_form->mask[6].item->GetFeldPtr (), "%s",  sadr.ort1); 
	  sprintf (current_form->mask[7].item->GetFeldPtr (), "%s",  sadr.ort2); 
	  adr_typ = sadr.adr_typ;
      display_form (adrwin, current_form);
	  return 0;
}
	   
int SearchLst (char *sebuff)
/**
Nach Kurzname in Liste suchen.
**/
{
	   int i;
	   int len;

	   if (sadrtab == NULL) return 0;
	   if (strlen (sebuff) == 0) 
	   {
	       Query->SetSel (0);
		   return 0;
	   }

	   for (i = 0; i < adranz; i ++)
	   {
		   len = min (16, strlen (sebuff));
		   if (strupcmp (sebuff, sadrtab[i].adr_krz, len) == 0) break;
	   }
	   if (i == adranz) return 0;
	   Query->SetSel (i);
	   return 0;
}


int ReadAdr (char *adr_name)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int cursor;
	  int i;

	  if (sadrtab) 
	  {
		  delete sadrtab;
		  sadrtab = NULL;
	  }

	  adranz = 0;
	  sprintf (buffer, "select count (*) from adr where adr_krz "
		            "matches \"%s*\" and adr_typ between 20 and 25", adr_name);
      DbClass.sqlout ((int *) &adranz, 2, 0);
	  DbClass.sqlcomm (buffer);
	  if (adranz == 0) return 0;

	  sadrtab = new struct SADR [adranz + 2];
	  sprintf (buffer, "select adr, adr_krz, "
		               "adr_nam1, adr_nam2, pf, plz, "
					   "str, ort1, ort2, adr_typ from adr where adr_krz "
		               "matches \"%s*\"  and adr_typ between 20 and 25 "
					   "order by adr_nam1", adr_name);
      DbClass.sqlout ((long *) &sadr.adr, 2, 0);
      DbClass.sqlout ((char *) sadr.adr_krz, 0, 37);
      DbClass.sqlout ((char *) sadr.adr_nam1, 0, 37);
      DbClass.sqlout ((char *) sadr.adr_nam2, 0, 37);
      DbClass.sqlout ((char *) sadr.pf, 0, 17);
      DbClass.sqlout ((char *) sadr.plz, 0, 9);
      DbClass.sqlout ((char *) sadr.str, 0, 37);
      DbClass.sqlout ((char *) sadr.ort1, 0, 37);
      DbClass.sqlout ((char *) sadr.ort2, 0, 37);
      DbClass.sqlout ((short *) &sadr.adr_typ, 1, 0);
	  cursor = DbClass.sqlcursor (buffer);
	  i = 0;
	  while (DbClass.sqlfetch (cursor) == 0)
	  {
		  memcpy (&sadrtab[i], &sadr, sizeof (sadr));
// 	      sprintf (buffer, " %-8ld \"%-36s\" \"%-36s\"", sadr.adr, sadr.adr_nam1, sadr.ort1); 
 	      sprintf (buffer, " %-8ld $%-36s$ $%-36s$", sadr.adr, sadr.adr_nam1, sadr.ort1); 
	      Query->InsertRecord (buffer);
		  i ++;
	  }
	  DbClass.sqlclose (cursor);
	  return 0;
}



static void SearchAdr (void)
{
  	  int cx, cy;
	  char buffer [256];
	  char tchar;
	  form *scurrent;

	  tchar = Gettchar ();
	  Settchar ('$');
	  scurrent = current_form;
	  idx = -1;
      cx = 80;
      cy = 20;
//      Query = new CHQ (cx, cy, "Name", "");
      Query = new CHQEX (cx, cy, "Name", "");
      Query->OpenWindow (hMainInst, hMainWindow);
	  sprintf (buffer, " %8s %36s %36s", "1", "1", "1"); 
	  Query->VLines (buffer, 0);
	  EnableWindow (hMainWindow, FALSE);

	  sprintf (buffer, " %-8s %-36s %-36s", "AdrNr", "Name", "Ort"); 
	  Query->InsertCaption (buffer);

	  Query->SetSearchLst (SearchLst);
	  Query->SetFillDb (ReadAdr);
	  Query->ProcessMessages ();
      idx = Query->GetSel ();
      Query->DestroyWindow ();
	  Settchar (tchar);
	  if (idx == -1) return;
	  memcpy (&sadr, &sadrtab[idx], sizeof (sadr));
	  if (sadrtab) delete sadrtab;
	  sadrtab = NULL;
	  current_form = scurrent;
	  if (syskey == KEY5) return;
	  sprintf (current_form->mask[0].item->GetFeldPtr (), "%ld", sadr.adr); 
	  sprintf (current_form->mask[1].item->GetFeldPtr (), "%s",  sadr.adr_nam1); 
	  sprintf (current_form->mask[2].item->GetFeldPtr (), "%s",  sadr.adr_nam2); 
	  sprintf (current_form->mask[3].item->GetFeldPtr (), "%s",  sadr.pf); 
	  sprintf (current_form->mask[4].item->GetFeldPtr (), "%s",  sadr.str); 
	  sprintf (current_form->mask[5].item->GetFeldPtr (), "%s",  sadr.plz); 
	  sprintf (current_form->mask[6].item->GetFeldPtr (), "%s",  sadr.ort1); 
	  sprintf (current_form->mask[7].item->GetFeldPtr (), "%s",  sadr.ort2); 
	  adr_typ = sadr.adr_typ;
	  display_form (adrwin, current_form);
}


static int testadr (void)
/**
Abfrage in Query-Eingabe.
**/
{
        switch (syskey)
        {
                case KEY5 :
                       break_enter ();
                       return 1;
                case KEY9 :
 					   SearchAdr ();
                       return 1;
                case KEY11 :
                case KEY12 :
                       syskey = KEY12;
                       break_enter ();
                       return 1;
                case KEYCR :
					   if (currentfield == GetItemPos (current_form, "OK"))
					   {
                             syskey = KEY12;
                             break_enter ();
                             return 1;
					   }
					   else if (currentfield == GetItemPos (current_form, "CA"))
					   {
                             syskey = KEY5;
                             break_enter ();
                             return 1;
					   }
					   else if (currentfield == GetItemPos (current_form, "Search"))
					   {
                             syskey = KEY9;
							 SearchAdr ();
                             return 1;
					   }
         }
        return 0;
}


void AdrInput (long adr_nr)
/**
Eingabemaske fuer Adresse diverser Kunde.
**/
{
        HANDLE hMainInst;
		HWND hWnd;

        static char adrval[41];
        static char adrz1val [41];
        static char adrz2val [41];
        static char pfval [41];
        static char strval [41];
        static char plzval [41];
        static char ort1val [41];
        static char ort2val [41];

        static ITEM iOK     ("OK",     "     OK     ", "", 0);
        static ITEM iCA     ("CA",     "  Abbrechen ", "", 0);
        static ITEM iSearch ("Search", "    Suchen  ", "", 0);


        static ITEM iadr ("adr", 
                           adrval, 
                             "Adress-Nr.......:", 
                           0);

        static ITEM iadrz1 ("adr_nam1", 
                                adrz1val, 
                             "Adressz. 1......:", 
                                   0);
        static ITEM iadrz2 ("adr_nam1", 
                                adrz2val, 
                             "Adressz. 2......:", 
                                   0);
        static ITEM ipf ("pf", 
                             pfval, 
                             "Postfach........:", 
                             0);

        static ITEM istr ("str", 
                            strval, 
                             "Strasse.........:", 
                            0);
        static ITEM iplz ("plz", 
                            plzval, 
                             "Postleitzahl....:", 
                             0);
        static ITEM iort1 ("ort1", 
                                ort1val, 
                             "Ort 1.Zei.......:", 
                                   0);
        static ITEM iort2 ("ort2", 
                                ort2val, 
                             "Ort 2.Zei.......:", 
                                   0);


        static field _adrform[] = {
           &iadr,     9, 0, 1,1, 0, "%8d", NORMAL, saveadr, readadr,0,
           &iadrz1,  37, 0, 2,1, 0, "",    NORMAL, 0,       testadr,0,
           &iadrz2,  37, 0, 3,1, 0, "",    NORMAL, 0,       testadr,0,
           &ipf,     16, 0, 4,1, 0, "",    NORMAL, 0,       testadr,0,
           &istr,    37, 0, 5,1, 0, "",    NORMAL, 0,       testadr,0,
           &iplz,     7, 0, 6,1, 0, "",    NORMAL, 0,       testadr,0,
           &iort1,   37, 0, 7,1, 0, "",    NORMAL, 0,       testadr,0,
           &iort2,   37, 0, 8,1, 0, "",    NORMAL, 0,       testadr,0,
           &iSearch, 15, 0,10, 5, 0, "",   BUTTON, 0,       testadr,KEY9,
           &iOK,     15, 0,10,22, 0, "",   BUTTON, 0,       testadr,KEY12,
           &iCA,     15, 0,10,39, 0, "",   BUTTON, 0,       testadr,KEY5,
		};

        static form adrform = {11, 0, 0, _adrform, 
			                    0, 0, 0, 0, NULL};
        

		int savefield;
		form *savecurrent;

		adr_typ = _adr.adr_typ;
        hWnd = AktivWindow;
        hMainInst = (HANDLE) GetWindowLong (hWnd, GWL_HINSTANCE);	
        save_fkt (5);
		save_fkt (6);
		save_fkt (7);
		save_fkt (9);
		save_fkt (11);
		save_fkt (12);

        set_fkt (NULL, 6);
        set_fkt (NULL, 7);
        set_fkt (NULL, 9);

        set_fkt (testadr, 5);
        set_fkt (testadr, 9);
        set_fkt (testadr, 11);
        set_fkt (testadr, 12);

 		sprintf (adrval, "%ld", adr_nr);
		strcpy (adrz1val,_adr.adr_nam1);
		strcpy (adrz2val,_adr.adr_nam2);
		strcpy (pfval,_adr.pf);
		strcpy (strval,_adr.str);
		strcpy (plzval,_adr.plz);
		strcpy (ort1val,_adr.ort1);
		strcpy (ort2val,_adr.ort2);

		savefield = currentfield;
		savecurrent = current_form;
        SetAktivWindow (hWnd);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_CAPTION);
        adrwin = OpenWindowChC (13, 62, 9, 10, hMainInst,
                               "Anschrift : Diverser Kunde");
		EnableWindow (bestpListe.GetMamain1 (), FALSE);
		EnableWindow (hMainWindow, FALSE);
        SetButtonTab (TRUE);
        enter_form (adrwin, &adrform, 0, 0);

        SetButtonTab (FALSE);
        CloseControls (&adrform);
		EnableWindow (bestpListe.GetMamain1 (), TRUE);
		EnableWindow (hMainWindow, TRUE);
        DestroyWindow (adrwin);
		current_form = savecurrent;
		currentfield = savefield;
        restore_fkt (5);
		restore_fkt (6);
		restore_fkt (7);
		restore_fkt (9);
		restore_fkt (11);
		restore_fkt (12);

		if (syskey == KEY5) return;

		adr_nr = atol (adrval);
		best_kopf.lief_adr = adr_nr;
		_adr.adr = best_kopf.lief_adr;
		strcpy (_adr.adr_nam1, adrz1val);
		strcpy (_adr.adr_nam2, adrz2val);
		strcpy (_adr.pf,       pfval);
		strcpy (_adr.str,      strval);
		strcpy (_adr.plz,      plzval);
		strcpy (_adr.ort1,     ort1val);
		strcpy (_adr.ort2,     ort2val);
		_adr.adr_typ = adr_typ;
		adr_class.dbupdate ();
}

// Werte f�r Adressbereiche  

static long adr_von  = 1;
static long adr_bis = 99999999;

// Werte f�r gesch�tzte Adressbereiche  

static long sadr_von = 90000000;
static long sadr_bis = 99999999;


void fillsadr (void)
/**
Gesch�tzte Adressbereiche f�llen.
**/
{
	  static BOOL adrber_ok = FALSE;
	  int dsqlstatus;
	  
	  if (adrber_ok) return;

	  adrber_ok = TRUE;
	  memcpy (&ptabn, &ptabn_null, sizeof (ptabn));
      dsqlstatus = ptab_class.lese_ptab ("adr", "1");
	  if (dsqlstatus) 
	  {
		  return;
	  }
	  adr_von = atol (ptabn.ptwer1);
	  adr_bis = atol (ptabn.ptwer2);
	  memcpy (&ptabn, &ptabn_null, sizeof (ptabn));
      dsqlstatus = ptab_class.lese_ptab ("adr", "2");
	  if (dsqlstatus) 
	  {
		  return;
	  }
	  sadr_von = atol (ptabn.ptwer2) + 1;
	  sadr_bis = adr_bis;
}

long GenAdr (void)
/**
Adresse fuer diverse Kunden generieren.
**/
{
           extern short sql_mode;

           sql_mode = 1;
           dsqlstatus = AutoClass.nvholid (0, 0, "adr");
		   if (dsqlstatus == -1)
		   {
							DbClass.sqlcomm ("delete from auto_nr where nr_nam = \"adr\" "
								              "and mdn = 0 and fil = 0");
							dsqlstatus = 100;
			}
					        
           if (dsqlstatus == 100)
           {
                           dsqlstatus = AutoClass.nvanmprf (0,
                                                  0,
                                                  "adr",
                                                  (long) sadr_von,
                                                  (long) sadr_bis,
                                                  (long) 10,
                                                  "");

                           if (dsqlstatus == 0)
                           {
                                  dsqlstatus = AutoClass.nvholid (0,
                                                        0,
                                                        "adr");
                           }
            }
            sql_mode = 0;
/*
            if (auto_nr.nr_nr == 0l)
            {
                    disp_mess ("Es konnte keine Adress-Nr generiert werden", 2);
                    return 0l;
            }
*/

            return auto_nr.nr_nr;

}

long AdrExist (long adr_nr)
/**
Testen, ob die Adress schon existiert.
**/
{
	         int dsqlstatus;

	         DbClass.sqlin ((long *) &adr_nr, 2, 0);
			 dsqlstatus = DbClass.sqlcomm ("select adr from adr where adr = ?");
			 if (dsqlstatus == 0) return TRUE;
			 return FALSE;
}

static BOOL AdrGen (char *lief_nr)
/**
Test, ob eine ADressnummer generiert werden soll.
**/
{

	        if (adr_nr == 0l)
			{
                   return TRUE;
			}
			return FALSE;
}
	        

int InputAdr (void)
/**
Adresse fuer diverse Kunden eingeben.
**/
{
            long nr_nr;
			int count;


		    fillsadr ();
			gen_div_adr = AdrGen (liefs);
		    if (gen_div_adr == FALSE)
			{
                    dsqlstatus = adr_class.lese_adr (adr_nr);
					if (dsqlstatus == 100)
					{
                             dsqlstatus = adr_class.lese_adr (lief.adr);
					}
			        AdrInput (adr_nr);
                    SetCurrentFocus (currentfield);
                    return 0;
			}
  		    adr_nr = 0l;
			count = 0;
            nr_nr = auto_nr.nr_nr;
			while (TRUE)
			{
                    adr_nr = GenAdr ();
					if (adr_nr == 0l)
					{
                        count ++;
						Sleep (5);
					}
					else if (AdrExist (adr_nr) == FALSE)
					{
						break;
					}
			}

            if (auto_nr.nr_nr == 0l)
            {
                    disp_mess ("Es konnte keine Adress-Nr generiert werden", 2);
                    return 0;
            }

            adr_nr = auto_nr.nr_nr;

            auto_nr.nr_nr = 0;
               
//        Die Transakttion wird an dieser Stelle geschlossen.

            FormToBest (0);
			bestk_class.dbupdate ();
            FreeBestNr = FALSE; 
            commitwork ();

            if (best_kopf.best_blg != nr_nr)
            {
                       beginwork ();
                       dsqlstatus = AutoClass.nveinid (atoi (mdn), 0,
                                            "best",  nr_nr);
                       commitwork ();
            }
            beginwork ();
			AdrInput (adr_nr);
            SetCurrentFocus (currentfield);
    	    gen_div_adr = FALSE;
            return 0;
}

int saveldat (void)
/**
Lieferdatum merken.
**/
{
	  strcpy (akt_ldat, ldat);
/*
	  if (_adr.adr_typ == 25)
	  {
		  gen_div_adr = TRUE;
          set_fkt (InputAdr, 9);
		  SetFkt (9, adresse, KEY9);
	  }
*/
	  return 0;
}

int savebdat (void)
/**
Lieferdatum merken.
**/
{
	  strcpy (akt_bdat, bdat);
	  return 0;
}


int testhtext (void)
{
      if (testkeys ()) return 0;

//      set_fkt (SwitchArt, 10);
//      SetFkt (10, aktart, KEY10);
//      bestpListe.SetSchirm ();
      return 0;
}

int doliste ()
{
           break_enter ();
           return 0;
}

int SwitchArt (void)
/**
Switch zwischen Kundenartikel-Nummer und eigener Atikelnummer.
**/
{
         if (aktart == liefart)
         {
             aktart = eigart;
             bestpListe.SetFieldAttr ("a_kun", DISPLAYONLY);
             bestpListe.SetChAttr (1);
             bestpListe.SetFieldAttr ("a_bz2", DISPLAYONLY);
         }
         else
         {
             aktart = liefart;
             bestpListe.SetFieldAttr ("a_kun", REMOVED);
             bestpListe.SetChAttr (0);
             bestpListe.SetFieldAttr ("a_bz2", REMOVED);
         }
         SetFkt (10, aktart, KEY10);
         bestpListe.DestroyWindows ();
         bestpListe.ShowBestp (atoi (mdn) , atoi (fil), bdat);
         return 0;
}


int SwitchZei (void)
/**
Switch zwischen 2 Artikelzeilen und 1 Artikelzeile.
**/
{
         if (aktzei == a_bz2_on)
         {
             aktzei = a_bz2_off;
             bestpListe.SetFieldAttr ("a_bz2", DISPLAYONLY);
         }
         else
         {
             aktzei = a_bz2_on;
             bestpListe.SetFieldAttr ("a_bz2", REMOVED);
         }
         SetFkt (7, aktzei, KEY7);
         bestpListe.DestroyWindows ();
         bestpListe.ShowBestp (atoi (mdn) , atoi (fil), ldat);
         return 0;
}


void DelBest (void)
/**
Bestellung l�schen.
**/
{
	       bestk_class.dbdelete (); 
		   bestp_class.dbdeletebest ();
           Mess.Message ("Satz wurde gel�scht");
           return;
}
           

int dokey12 ()
/**
Taste Key12 behandeln.
**/
{
           if (mench == 0)
           {

 	             if (bestpListe.GetPosanz () == 0)
				 {
					 disp_mess ("Es wurden kein Positionen erfasst", 2);
					 SetCurrentField (currentfield);
					 SetCurrentFocus (currentfield);
					 return (1);
				 }
				 if (testlief0 () == FALSE)
				 {
					 return 1;
				 }
                 FormToBest (1);
				 if (best_kopf.bearb_stat == 0)
				 {
					 best_kopf.bearb_stat = 1;
				 }
				 bestk_class.dbupdate (); 
                 commitwork ();

                 if (best_kopf.best_blg != auto_nr.nr_nr)
                 {
                       beginwork ();
                       dsqlstatus = AutoClass.nveinid (atoi (mdn), 0,
                                            "best",  auto_nr.nr_nr);
                       commitwork ();
                 }
                 InitBest ();
                 break_enter ();
                 set_fkt (NULL, 12);
                 Mess.Message ("Satz wurde geschrieben");
           }
           else if (mench == 2)
           {
                 if (abfragejn (mamain1, "Bestellung l�schen ?", "J"))
                 {
                        DelBest ();
                        commitwork ();
                        InitBest ();
                        break_enter ();
                        set_fkt (NULL, 12);
                        Mess.Message ("Satz wurde gel�scht");
                 }
           }
           
           return 0;
}


/* Ende Eingabe Kopfdaten                                         */
/******************************************************************/

void tst_arg (char *arg)
{
          for (; *arg; arg += 1)
          {
                 switch (*arg)
                 {
                            case 'u' :
                                     strcpy (InfoCaption, arg + 1);
                                     return;
                 }
          }
          return;
}


void rollbackbestk (void)
/**
delstatus in angk auf 0 setzen.
Bei neuem Angebot Satz loeschen.
**/
{
	    int best_stat = 0;
		int dsqlstatus;

		DbClass.sqlin ((short *) &best_kopf.mdn, 1, 0);
		DbClass.sqlin ((short *) &best_kopf.fil, 1, 0);
		DbClass.sqlin ((short *) &best_kopf.best_blg, 2, 0);
		DbClass.sqlout ((short *) &best_stat, 1, 0);
		dsqlstatus = DbClass.sqlcomm ("select bearb_stat from best_kopf "
			                          "where mdn = ? "
									  "and fil = ? "
									  "and best_blg = ?");
		if (dsqlstatus != 0) return;

		if (best_stat == 0)
		{
  		           DbClass.sqlin ((short *) &best_kopf.mdn, 1, 0);
		           DbClass.sqlin ((short *) &best_kopf.fil, 1, 0);
		           DbClass.sqlin ((short *) &best_kopf.best_blg, 2, 0);
		           dsqlstatus = DbClass.sqlcomm ("select best_blg from best_pos "
			                          "where mdn = ? "
									  "and fil = ? "
									  "and best_blg = ?");

				   if (dsqlstatus == 0)
				   {
  		                     DbClass.sqlin ((short *) &best_kopf.mdn, 1, 0);
		                     DbClass.sqlin ((short *) &best_kopf.fil, 1, 0);
		                     DbClass.sqlin ((short *) &best_kopf.best_blg, 2, 0);
		                     dsqlstatus = DbClass.sqlcomm ("delete from best_pos "
			                                               "where mdn = ? "
									                       "and fil = ? "
									                       "and best_blg = ?");
				   }
  		           DbClass.sqlin ((short *) &best_kopf.mdn, 1, 0);
		           DbClass.sqlin ((short *) &best_kopf.fil, 1, 0);
		           DbClass.sqlin ((short *) &best_kopf.best_blg, 2, 0);
		           dsqlstatus = DbClass.sqlcomm ("delete from best_kopf "
			                          "where mdn = ? "
									  "and fil = ? "
									  "and best_blg = ?");
		}
		else
		{
  		           DbClass.sqlin ((short *) &best_kopf.mdn, 1, 0);
		           DbClass.sqlin ((short *) &best_kopf.fil, 1, 0);
		           DbClass.sqlin ((short *) &best_kopf.best_blg, 2, 0);
		           dsqlstatus = DbClass.sqlcomm ("update best_kopf set delstatus = 0 "
			                          "where mdn = ? "
									  "and fil = ? "
									  "and best_blg = ?");
		}
}



int dokey5 ()
/**
Aktion bei Taste F5
**/
{
		if (mench == 0 ||
			mench == 2) 
		{
                 rollbackwork ();
		}
        break_enter ();
        DisplayAfterEnter (0);
        set_fkt (NULL, 12);
        return 1;
}


BOOL IsComboMsg (MSG *msg)
/**
Test, ob die Meldung fuer die Combobox ist.
**/
{
      if (HIWORD (msg->lParam) == CBN_DROPDOWN)
      {
                 opencombobox = TRUE;
                 return TRUE;
      }
      if (HIWORD (msg->lParam) == CBN_CLOSEUP)
      {
                 opencombobox = FALSE;
                 return TRUE;
      }

      if (msg->hwnd == hwndCombo1)
      {
                 return TRUE;
      }
      if (msg->hwnd == hwndTB)
      {
                 return TRUE;
      }

      return FALSE;
}

void FillCombo1 (void)
{
      int i;

      for (i = 0; Combo1[i]; i ++)
      {
           SendMessage (hwndCombo1, CB_ADDSTRING, 0,
                                   (LPARAM) Combo1 [i]);
      }
      SendMessage (hwndCombo1, CB_SELECTSTRING, 0,
                                   (LPARAM) Combo1 [3]);
      bestpListe.SetLines (3);
}

void FillCombo2 (void)
{
      int i;

      for (i = 0; Combo2[i]; i ++)
      {
           SendMessage (hwndCombo2, CB_ADDSTRING, 0,
                                   (LPARAM) Combo2 [i]);
      }
      SendMessage (hwndCombo2, CB_SELECTSTRING, 0,
                                   (LPARAM) Combo2 [combo2pos]);
      SetComboMess (IsComboMsg);
}

static int StopProc (void)
{
    PostQuitMessage (0);
    return 0;
}


void DisableTB (void)
{
     ToolBar_SetState(hwndTB,IDM_STD,  TBSTATE_INDETERMINATE);
     ToolBar_SetState(hwndTB,IDM_LIST, TBSTATE_INDETERMINATE);
     ToolBar_SetState(hwndTB,KEYLEFT,  TBSTATE_INDETERMINATE);
     ToolBar_SetState(hwndTB,KEYRIGHT, TBSTATE_INDETERMINATE);
     ToolBar_SetState(hwndTB,IDM_FIND, TBSTATE_INDETERMINATE);
}

void EnableTB (void)
{
     ToolBar_SetState(hwndTB,IDM_STD,  TBSTATE_ENABLED);
     ToolBar_SetState(hwndTB,IDM_LIST, TBSTATE_ENABLED);
     ToolBar_SetState(hwndTB,KEYLEFT,  TBSTATE_ENABLED);
     ToolBar_SetState(hwndTB,KEYRIGHT, TBSTATE_ENABLED);
     ToolBar_SetState(hwndTB,IDM_FIND, TBSTATE_ENABLED);

     ToolBar_PressButton(hwndTB, IDM_PAGE, FALSE);
     ToolBar_PressButton(hwndTB, IDM_LIST, TRUE);
}


int EnterListe ()
{
//	int ftyp;
    
//	PrintButtons ();
//    if (testlief0 () == FALSE) return 0;
	DlgWindow = NULL;
    EnableTB ();
    set_fkt (NULL, 4);
	EnableArrows (&bestform, FALSE);
    EnableMenuItem (hMenu, IDM_FIND,    MF_ENABLED);
    EnableMenuItem (hMenu, IDM_FONT,    MF_ENABLED);
    EnableMenuItem (hMenu, IDM_BASIS,   MF_ENABLED);
    EnableMenuItem (hMenu, IDM_POSTEXT, MF_ENABLED);
	DisablePrn ();

    set_fkt (NULL, 9);
    SetFkt (9, leer, 0);
    set_fkt (NULL, 10);
    SetFkt (10, leer, 0);
    best_kopf.lief_term = dasc_to_long (ldat);	
	bestpListe.SetLiefProc (SetLief);
    bestpListe.EnterBestp (atoi (mdn) , atoi (fil), bdat);
	bestpListe.SetLiefProc (NULL);

    EnableMenuItem (hMenu, IDM_FIND, MF_GRAYED);
    EnableMenuItem (hMenu, IDM_FONT, MF_GRAYED);
    EnableMenuItem (hMenu, IDM_BASIS, MF_GRAYED);
    EnableMenuItem (hMenu, IDM_POSTEXT, MF_GRAYED);
	EnableArrows (&bestform, TRUE);
    DisableTB ();
//	EnablePrn ();

//	if (GroupOK ())
//	{
            if (KomplettMode < 2) 
			{
//				set_fkt (SetKomplett, 8);
//                SetFkt (8, komplett, KEY8);
			}
//	}
    return 0;
}

void PrintButtons (void)
{
         clipped (_adr.tel);
/*
         if (strcmp (_adr.tel, " ") > 0)
         {
                CTelefon.bmp = btelefon;
                ActivateColButton (mamain1, 
                                  &buform, 1, 0, 1);
                EnableMenuItem (hMenu, CallT2,   MF_ENABLED);
         }
         else
         {
                CTelefon.bmp = btelefoni;
                ActivateColButton (mamain1, 
                &buform, 1, -1, 1);
                EnableMenuItem (hMenu, CallT2,   MF_GRAYED);
         }
         clipped (_adr.fax);
         if (strcmp (_adr.fax, " ") > 0)
         {
                ActivateColButton (mamain1, 
                                   &buform,      0, 0, 1);
                EnableMenuItem (hMenu, CallT1,   MF_ENABLED);
         }
         else
         {
                ActivateColButton (mamain1, 
                                      &buform,      0, -1, 1);
                EnableMenuItem (hMenu, CallT1,   MF_GRAYED);
         }
         ActivateColButton (mamain1, &buform, 2, 0, 1);
         EnableMenuItem (hMenu, CallT3,   MF_ENABLED);
         MoveButtons ();
*/
}


void MoveButtons (void)
/**
Buttons in mamain1 rechtsbuendig.
**/
{

     return;

/*
     RECT rect;
     RECT rectb;
     TEXTMETRIC tm;
     HDC hdc;
     int i;
     int x, y;

     GetClientRect (mamain1, &rect); 
     hdc = GetDC (mamain1);
     GetTextMetrics (hdc, &tm);
     ReleaseDC (mamain1, hdc);

     for (i = 0; i < buform.fieldanz; i ++)
     {
         GetClientRect (buform.mask[i].feldid, &rectb );
         x = rect.right - rectb.right - 1;
         y = buform.mask[i].pos[0] * tm.tmHeight;
         MoveWindow (buform.mask[i].feldid,
                                    x, y,
                                    rectb.right,
                                    rectb.bottom,
                                    TRUE);
     }
     CreateQuikInfos ();
*/
}

int CalcMinusPos (int cy)
{
        TEXTMETRIC tm;
        SIZE size;
        HDC hdc;


        hdc = GetDC (ftasten);
        GetTextMetrics (hdc, &tm);
        GetTextExtentPoint32 (hdc, "X", 1, &size);
        tm.tmAveCharWidth = size.cx;
        ReleaseDC (ftasten, hdc);
        cy -= 3 * tm.tmHeight;
        return cy;
}

int CalcPlusPos (int y)
{
        TEXTMETRIC tm;
        SIZE size;
        HDC hdc;


        hdc = GetDC (ftasten);
        GetTextMetrics (hdc, &tm);
        GetTextExtentPoint32 (hdc, "X", 1, &size);
        tm.tmAveCharWidth = size.cx;
        ReleaseDC (ftasten, hdc);
        y += 1 * tm.tmHeight + 5;
        return y;
}

void MoveMamain1 (void)
/**
Hauptarbeitsfenster oeffnen.
**/
{
     RECT trect;
     RECT frect;
     RECT mrect;
     RECT crect;
     int x,y, cx, cy;

     GetWindowRect (hMainWindow, &mrect);
     GetWindowRect (ftasten, &frect);

     GetClientRect (hMainWindow, &crect);
     GetClientRect (hwndTB, &trect);
     x = 5;
     cx = crect.right - x - 5;

     y = trect.bottom + 5;
     cy = frect.top - mrect.top - y - 50;
     if (ftasten2 == NULL)
     {
         y  = CalcPlusPos (y);
         cy = CalcMinusPos (cy);
     }
     MoveWindow (mamain1, x, y, cx, cy, TRUE);
     MoveFktBitmap ();
     MoveButtons ();
}


void Createmamain1 (void)
/**
Hauptarbeitsfenster oeffnen.
**/
{
     RECT trect;
     RECT frect;
     RECT mrect;
     RECT crect;
     int x,y, cx, cy;


     GetWindowRect (hMainWindow, &mrect);
     GetWindowRect (ftasten, &frect);

     GetClientRect (hMainWindow, &crect);
     GetClientRect (hwndTB, &trect);
     x = 5;
     cx = crect.right - x - 5;

     y = trect.bottom + 5;
     cy = frect.top - mrect.top - y - 50;

     if (ftasten2 == NULL)
     {
         y  = CalcPlusPos (y);
         cy = CalcMinusPos (cy);
     }
     mamain1 = CreateWindowEx (
                              WS_EX_CLIENTEDGE, 
                              "hStdWindow",
                              InfoCaption,
                              WS_CHILD |  
                              WS_VISIBLE,
                              x, y,
                              cx, cy,
                              hMainWindow,
                              NULL,
                              hMainInst,
                              NULL);
}

/*
void MoveMamain1 (void)
{
     RECT trect;
     RECT frect;
     RECT mrect;
     RECT crect;
     int x,y, cx, cy;

     GetWindowRect (hMainWindow, &mrect);
     GetWindowRect (ftasten, &frect);

     GetClientRect (hMainWindow, &crect);
     GetClientRect (hwndTB, &trect);
     x = 5;
     cx = crect.right - x - 5;

     y = trect.bottom + 5;
     cy = frect.top - mrect.top - y - 50;
     MoveWindow (mamain1, x, y, cx, cy, TRUE);
     MoveButtons ();
}


void Createmamain1 (void)
{
     RECT trect;
     RECT frect;
     RECT mrect;
     RECT crect;
     int x,y, cx, cy;


     GetWindowRect (hMainWindow, &mrect);
     GetWindowRect (ftasten, &frect);

     GetClientRect (hMainWindow, &crect);
     GetClientRect (hwndTB, &trect);
     x = 5;
     cx = crect.right - x - 5;

     y = trect.bottom + 5;
     cy = frect.top - mrect.top - y - 50;

     mamain1 = CreateWindowEx (
                              WS_EX_CLIENTEDGE, 
                              "hStdWindow",
                              InfoCaption,
                              WS_CHILD |  
                              WS_VISIBLE,
                              x, y,
                              cx, cy,
                              hMainWindow,
                              NULL,
                              hMainInst,
                              NULL);
}
*/

void ShowDaten (void)
/**
Daten fuer Angebot anzeigen.
**/
{
        Mess.CloseMessage ();
        display_form (mamain1, &bestform, 0, 0);

        bestpListe.ShowBestp (atoi (mdn) , atoi (fil), bdat);
        current_form = &bestformk;
        set_fkt (dokey12, 12);
        display_form (mamain1, &bestformk, 0, 0);

}


void eingabedaten (void)
/**
Artikel bearbeiten.
**/
{

       inDaten = 1;
       Mess.CloseMessage ();
       while (TRUE)
       {
                set_fkt (InfoLief, 4);
                set_fkt (doliste, 6);
                SetFkt (6, liste, KEY6);
                set_fkt (SwitchZei, 7);
                SetFkt (7, aktzei, KEY7);
//                set_fkt (SwitchArt, 10);
//                SetFkt (10, aktart, KEY10);
//                set_fkt (dokey12, 12);
                no_break_end ();

                bestpListe.ShowBestp (atoi (mdn) , atoi (fil), bdat);

				SetLiefAttr ();
                DisplayAfterEnter (FALSE);
				currentfield = 0;
                enter_form (mamain1, &bestform, 0, 0);
                DisplayLines ();
                if (syskey == KEYESC || syskey == KEY5 ||
                    syskey == KEY12)
                {
                                 DisplayAfterEnter (TRUE);
                                 break;
                }
                current_form = &bestform;
                display_form (mamain1, &bestform, 0, 0);
                bestpListe.DestroyWindows (); 
                EnterListe ();
                set_fkt (dokey5, 5);
                set_fkt (doliste, 6);
                set_fkt (NULL, 7);
                SetFkt (6, liste, KEY6);
                SetFkt (7, leer, 0);
                SetFkt (12, leer, 0);
                set_fkt (NULL, 12);
        }
        bestpListe.DestroyWindows (); 
        CloseControls (&bestform);
        set_fkt (NULL, 6);
        set_fkt (NULL, 7);
        SetFkt (6, leer, 0);
        SetFkt (7, leer, 0);
        inDaten = 0;
        set_fkt (NULL, 8);
        SetFkt (8, leer, NULL);
        return;
}


int QueryLief (void)
/**
Auswahl ueber Touren.
**/
{
       int ret;

	   lief.mdn = atoi (mdn);
       DisablehWnd (hMainWindow);
       DisableListhWnd (mamain1);
       ret = QClass.querylief (mamain1);
       set_fkt (dokey5, 5);
       set_fkt (doliste, 6);
       SetFkt (6, liste, KEY6);
       SetFkt (7, aktzei, KEY7);
       SetFkt (9, auswahl, KEY9);
	   SetFkt (8, leer, NULL);
	   set_fkt (NULL, 8);
	   set_fkt (NULL, 12);

       set_fkt (QueryLief, 9);

//       if (GroupOK ())
//       {
            if (KomplettMode < 2) 
			{
//				set_fkt (SetKomplett, 8);
//                SetFkt (8, komplett, KEY8);
			}
//       }
	   EnablePrn ();
       set_fkt (dokey12, 12);
       SetAktivWindow (mamain1);
       if (ret == FALSE)
       {
           SetCurrentFocus (currentfield);
           return 0;
       }
       lieffield = GetItemPos (&bestform, "lief");
       sprintf (liefs, "%s", lief.lief);
       display_field (mamain1, &bestform.mask[lieffield], 0, 0);
       PostMessage (mamain1, WM_KEYDOWN, VK_RETURN, 0l);
       return 0;
}


int setkey9lief ()
/**
Auswahl fuer Kunde setzen.
**/
{
       strcpy (akt_lief, liefs);
       SetFkt (9, auswahl, KEY9);
       set_fkt (QueryLief, 9);
       return 0;
}


int testldat (void)
/**
Lieferdatum testen.
**/
{
		 char ldatum [12];
		 long dat_von, dat_bis, lidat;

         if (testkeys ()) return 0;

		 sysdate (ldatum);
		 dat_von = dasc_to_long (ldatum) - ldatdiff;
		 dat_bis = dasc_to_long (ldatum) + ldatdiff;
		 lidat = dasc_to_long (ldat);

		 if ((lidat < dat_von) || (lidat > dat_bis))
		 {
			 disp_mess ("Abweichung beim Lieferdatum ist zu gro�", 2);
		     SetCurrentField (currentfield);
			 return 0;
		 }

		 if (strcmp (ldat, akt_ldat) == 0)
		 {
		              return 0;
		 }

		 return 0;
}

int testbdat (void)
/**
Lieferdatum testen.
**/
{
		 char bdatum [12];
		 long dat_von, dat_bis, lidat;
		 static BOOL ListOK = FALSE; 

         if (testkeys ()) return 0;

		 sysdate (bdatum);
		 dat_von = dasc_to_long (bdatum) - bdatdiff;
		 dat_bis = dasc_to_long (bdatum) + bdatdiff;
		 lidat = dasc_to_long (bdat);

		 if ((lidat < dat_von) || (lidat > dat_bis))
		 {
			 disp_mess ("Abweichung beim Bestelldatum ist zu gro�", 2);
		     SetCurrentField (currentfield);
			 return 0;
		 }

		 if (ListOK && strcmp (bdat, akt_bdat) == 0)
		 {
                      bestpListe.ShowBestp (atoi (mdn) , atoi (fil), bdat);
		              return 0;
		 }

         bestpListe.DestroyWindows (); 
		 bestpListe.SetListAktiv (FALSE);
         bestpListe.InitBestList ();
         bestpListe.ShowBestp (atoi (mdn) , atoi (fil), bdat);
         ListOK = TRUE;
		 return 0;
}


int QueryBest (void)
/**
Auswahl ueber Artikel.
**/
{
       int ret;

       DisablehWnd (hMainWindow);
       DisableListhWnd (mamain1);
       ret = QClass.querybest (mamain1);
       set_fkt (dokey5, 5);
       SetAktivWindow (mamain1);
       if (ret == FALSE)
       {
           SetCurrentFocus (currentfield);
           return 0;
       }
       sprintf (best_nr, "%ld", best_kopf.best_blg);
       display_field (mamain1, &bestformk.mask[0], 0, 0);
       PostMessage (mamain1, WM_KEYDOWN, VK_RETURN, 0l);
       return 0;
}

  
void ShowBest (void)
/**
Autrag anzeigen.
**/
{
       extern short sql_mode; 

       Mess.CloseMessage ();
       akt_best = 0l;
       sprintf (best_nr, "0");
       no_break_end ();
       set_fkt (QueryBest, 10);
       SetFkt (10, auswahl, KEY10);
	   if (mench == 2) beginwork ();
       enter_form (mamain1, &bestformk, 0, 0);
	   if (mench == 2) commitwork ();
       bestpListe.DestroyWindows (); 
       CloseControls (&bestform);
       CloseControls (&bestformk);
       set_fkt (NULL, 12);
/*
       CTelefon.bmp = btelefoni;
       ActivateColButton (mamain1, 
                                      &buform, 0, -1, 1);
       ActivateColButton (mamain1, 
                                      &buform, 1, -1, 1);
       ActivateColButton (mamain1, 
                                      &buform, 2, -1, 1);

       EnableMenuItem (hMenu, CallT1,    MF_GRAYED);
       EnableMenuItem (hMenu, CallT2,    MF_GRAYED);
       EnableMenuItem (hMenu, CallT3,    MF_GRAYED);

       MoveButtons ();
*/
       return;
}

BOOL BestNrOK (void)
/**
generierte Angebotsnummer testen.
**/
{
       char buffer [256];

       if (auto_nr.nr_nr == 0l) return FALSE;

       sprintf (buffer, "select best_blg from best_kopf where mdn = %d "
                                              "and   fil = %d "
                                              "and best_blg =   %ld",
                          atoi (mdn), atoi (fil), auto_nr.nr_nr);

       if (DbClass.sqlcomm (buffer) != 100) return FALSE;
       return TRUE;
}
      

void GenBestNr (void)
/**
Bestellnummer generieren.
**/
{
       extern short sql_mode; 
	   int i;
	   static int MAXWAIT = 100;
       MSG msg;

       beginwork ();
       sql_mode = 1;
	   i = 0;
       while (TRUE)
       {
                dsqlstatus = AutoClass.nvholid (atoi (mdn), 0, "best");

				if (dsqlstatus == -1)
				{
					        DbClass.sqlin ((short *) &best_kopf.mdn, 1, 0); 
					        DbClass.sqlin ((short *) &best_kopf.fil, 1, 0); 
							DbClass.sqlcomm ("delete from auto_nr where nr_nam = \"best\" "
								              "and mdn = ? and fil = ?");
							dsqlstatus = 100;
				}
					        
					   
                if (dsqlstatus == 100)
                {
                           dsqlstatus = AutoClass.nvanmprf (atoi (mdn),
                                                  0,
                                                  "best",
                                                  (long) 1,
                                                  (long) 999999,
                                                  (long) 10,
                                                  "");

                           if (dsqlstatus == 0)
                           {
                                  dsqlstatus = AutoClass.nvholid (atoi (mdn),
                                                        0,
                                                        "best");
                           }
                }

                if (dsqlstatus == 0 && BestNrOK ()) break;
				Sleep (50);
                if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
                {
                          TranslateMessage(&msg);
                          DispatchMessage(&msg);
                }

				i ++;
				if (i > MAXWAIT) break;
       }
       sql_mode = 0;
       commitwork ();
       FreeBestNr = TRUE;
}

void SetAktiveArrow (char *item, BOOL active)
{
	  int i;
	  
	  for (i = 0; activearrows[i].pos > -1; i ++)
	  {
		  if (strcmp (activearrows[i].item, item) == 0)
		  {
                   activearrows[i].active = active;
				   break;
		  }
	  }
}  

BOOL ArrowActive (int pos)
{
	  int i;
	  
	  for (i = 0; activearrows[i].pos > -1; i ++)
	  {
		  if (activearrows[i].pos == pos)
		  {
			  return activearrows[i].active;
		  }
	  }
	  return TRUE;
}


void EnableArrows (form *frm, BOOL mode)
/**
Pfeilbuttons aktivieren oder deaktivieren.
**/
{
	   int i;

	   for (i = 0; i < frm->fieldanz; i ++)
	   {
		   if (frm->mask[i].item->GetItemName () && 
			   strcmp (frm->mask[i].item->GetItemName (), "arrdown") == 0)
		   {
		       if (!ArrowActive (i)) continue;
			   if (mode == FALSE)
			   {
			             frm->mask[i].picture = "";
						 frm->mask[i].attribut = REMOVED;
						 CloseControl (frm, i);
			   }
			   else
			   {
						 frm->mask[i].attribut = BUTTON;
			             frm->mask[i].picture = "B";
			   }
			   EnableWindow (frm->mask[i].feldid, mode);
		   }
	   }
}


void EnterBest (void)
/**
Autrag bearbeiten.
**/
{
       break_end ();
       SetBestdat ();
       SetLiefdat ();
	   bestpListe.SetListAktiv (FALSE);
       bestpListe.InitBestList ();
       eingabedaten ();
/*

       Mess.CloseMessage ();
       akt_best = 0l;
       while (TRUE)
       {
				DisablePrn ();
                break_end ();
                GenBestNr ();
				if (auto_nr.nr_nr == 0l)
				{
					disp_mess ("Es konnte keine Bestellsnummer generiert werden", 2);
					return;
				}
                sprintf (best_nr, "%ld", auto_nr.nr_nr);
                beginwork ();
                set_fkt (QueryBest, 10);
                SetFkt (10, auswahl, KEY10);
                DisplayAfterEnter (TRUE);
                no_break_end ();
                enter_form (mamain1, &bestformk, 0, 0);
                set_fkt (NULL, 8);
                SetFkt (8, leer, 0);
                set_fkt (NULL, 10);
                SetFkt (10, leer, 0);
                if (syskey == KEY5 || syskey == KEYESC) break;

                if (NewRec == 0)
                {
                        if (KomplettMode < 2) 
						{
						}
						EnablePrn ();
                }
				EnableArrows (&bestformk, FALSE);
                eingabedaten ();
                commitwork ();
				EnableArrows (&bestformk,TRUE);
                CTelefon.bmp = btelefoni;
                ActivateColButton (mamain1, 
                                      &buform, 0, -1, 1);
                ActivateColButton (mamain1, 
                                      &buform, 1, -1, 1);
                ActivateColButton (mamain1, 
                                      &buform, 2, -1, 1);
                EnableMenuItem (hMenu, CallT1,   MF_GRAYED);
                EnableMenuItem (hMenu, CallT2,   MF_GRAYED);
                EnableMenuItem (hMenu, CallT3,   MF_GRAYED);

                MoveButtons ();
                set_fkt (NULL, 6);
                set_fkt (NULL, 7);
                set_fkt (NULL, 9);
                SetFkt (6, leer, 0);
                SetFkt (7, leer, 0);
                SetFkt (9, leer, 0);
        }
*/
        set_fkt (NULL, 6);
        set_fkt (NULL, 7);
        SetFkt (6, leer, 0);
        SetFkt (7, leer, 0);
        Mess.CloseMessage ();
        return;
}

void DisableMe (void)
{
     ToolBar_SetState(hwndTB,IDM_WORK, TBSTATE_INDETERMINATE);
     EnableMenuItem (hMenu,  IDM_WORK,  MF_GRAYED);

     ToolBar_SetState(hwndTB,IDM_SHOW, TBSTATE_INDETERMINATE);
     EnableMenuItem (hMenu,  IDM_SHOW,  MF_GRAYED);

     ToolBar_SetState(hwndTB,IDM_DELETE, TBSTATE_INDETERMINATE);
     EnableMenuItem (hMenu,  IDM_DELETE,  MF_GRAYED);

     ToolBar_SetState(hwndTB,IDM_PRINT, TBSTATE_INDETERMINATE);
     EnableMenuItem (hMenu,  IDM_PRINT,  MF_GRAYED);

}

void EnableMe (void)
{

     ToolBar_SetState(hwndTB, IDM_WORK, TBSTATE_ENABLED);
     EnableMenuItem (hMenu,      IDM_WORK, MF_ENABLED);
	 CheckMenuItem (hMenu,        IDM_WORK, MF_UNCHECKED);

     ToolBar_SetState(hwndTB, IDM_SHOW, TBSTATE_ENABLED);
     EnableMenuItem (hMenu,      IDM_SHOW, MF_ENABLED);
	 CheckMenuItem (hMenu,        IDM_SHOW, MF_UNCHECKED);

     ToolBar_SetState(hwndTB, IDM_DELETE, TBSTATE_ENABLED);
     EnableMenuItem (hMenu,      IDM_DELETE, MF_ENABLED);
	 CheckMenuItem (hMenu,        IDM_DELETE, MF_UNCHECKED);

     ToolBar_SetState(hwndTB, IDM_PRINT, TBSTATE_ENABLED);
     EnableMenuItem (hMenu,      IDM_PRINT, MF_ENABLED);


     switch  (mench)
	 {
	     case 0 :
            ToolBar_SetState(hwndTB, IDM_WORK, TBSTATE_ENABLED | TBSTATE_CHECKED);
            EnableMenuItem (hMenu,      IDM_WORK, MF_ENABLED);
	        CheckMenuItem (hMenu,        IDM_WORK, MF_CHECKED);
			break;
	     case 1 :
            ToolBar_SetState(hwndTB, IDM_SHOW, TBSTATE_ENABLED | TBSTATE_CHECKED);
            EnableMenuItem (hMenu,      IDM_SHOW, MF_ENABLED);
	        CheckMenuItem (hMenu,        IDM_SHOW, MF_CHECKED);
			break;
	     case 2 :
            ToolBar_SetState(hwndTB, IDM_DELETE, TBSTATE_ENABLED | TBSTATE_CHECKED);
            EnableMenuItem (hMenu,      IDM_DELETE, MF_ENABLED);
	        CheckMenuItem (hMenu,        IDM_DELETE, MF_CHECKED);
			break;
	 }
}

void DisablePrn (void)
{
     ToolBar_SetState(hwndTB,IDM_PRINT, TBSTATE_INDETERMINATE);
     EnableMenuItem (hMenu,  IDM_PRINT,  MF_GRAYED);
}

void EnablePrn (void)
{
     ToolBar_SetState(hwndTB, IDM_PRINT, TBSTATE_ENABLED);
     EnableMenuItem (hMenu,      IDM_PRINT, MF_ENABLED);

}


void ShowBm (void)
{
    RECT rect;
    TEXTMETRIC tm;
    int x, y, cx, cy;
    static int abmp = 0;
    char buffer [80];

    abmp ++;
    if (abmp > 3) abmp = 1;
    GetWindowRect (hMainWindow, &rect);
    stdfont ();
    SetTmFont (hMainWindow, &tm);

    cx = 5;
    cy = 2;
    x = (rect.right - 2) / tm.tmAveCharWidth;
    x -= cx;
    y = rect.top;
    y = (int) (double) ((double) y / 
                                (double)(1 + (double) 1/3));
    y /= tm.tmHeight;
    y += 3;
    sprintf (buffer, "-nm2 -z%d -s%d -h%d -w%d "
             "c:\\user\\fit\\bilder\\artikel\\%d.bmp",
             y, x, cy, cx, abmp);
    _ShowBm (buffer);
}


void EingabeMdnFil (void)
{
       extern MDNFIL cmdnfil;

       Mess.CloseMessage ();
//       DisableMe ();
       cmdnfil.SetFilialAttr (REMOVED);
       cmdnfil.SetNoMdnNull (TRUE);
	   while (TRUE)
       {
		   EnableMe ();
		   StartMen = TRUE;
           set_fkt (dokey5, 5);
		   if (cmdnfil.eingabemdnfil (mamain1, mdn, fil) == -1)
           {
			         break;
           }
// Nicht aktiv    ShowBm ();
           DisableMe ();
		   StartMen = FALSE;
           switch (mench)
           {
                case 0:
                      EnterBest  ();
                      break;
                case 1 :
                case 2 :
                case 3 :
                      ShowBest  ();
                      CTelefon.bmp = btelefoni;
                      break;
           }
	   }
       cmdnfil.CloseForm ();
       EnableMe ();
}


static int testber (void)
/**
Abfrage in Query-Eingabe.
**/
{
	    int savecurrent;

		savecurrent = currentfield;
        switch (syskey)
        {
                case KEY5 :
                       break_enter ();
                       return 1;
                case KEY11 :
                case KEY12 :
                       syskey = KEY12;
                       break_enter ();
                       return 1;
				case KEY7 :
					   ChoisePrinter0 ();
 		               SetButtonTab (TRUE);
					   SetCurrentField (savecurrent);
					   SetCurrentFocus (savecurrent);
					   return 1;
        }

		if (syskey == KEYCR)
		{
		        if (current_form->mask[currentfield].BuId == KEY12)
				{
					syskey = KEY12;
					break_enter ();
					return 1;
				}
		        else if (current_form->mask[currentfield].BuId == KEY5)
				{
					syskey = KEY5;
					break_enter ();
					return 1;
				}
		        else if (current_form->mask[currentfield].BuId == KEY7)
				{
					   syskey = KEY7; 
					   ChoisePrinter0 ();
 		               SetButtonTab (TRUE);
					   SetCurrentField (savecurrent);
					   SetCurrentFocus (savecurrent);
					   return 1;
				}
		}
        return 0;
}

BOOL LockBereich (char *where)
/**
Druckbereich sperren.
Bereich fuer Drucken sperren. 
**/
{
	char sqls [1028];
	int cursor;
//	int cursor_pos;
	int cursorupd;
	int dsqlstatus;
	extern short sql_mode;

	DbClass.sqlout ((short *) &best_kopf.mdn, 1, 0);
	DbClass.sqlout ((short *) &best_kopf.fil, 1, 0);
	DbClass.sqlout ((long *) &best_kopf.best_blg, 2, 0);
	sprintf (sqls, "select mdn, fil, best_blg from best_kopf %s and delstatus = 0", where);
	cursor = DbClass.sqlcursor (sqls);

	DbClass.sqlin ((short *) &best_kopf.mdn, 1, 0);
	DbClass.sqlin ((short *) &best_kopf.fil, 1, 0);
	DbClass.sqlin ((long *) &best_kopf.best_blg, 2, 0);
	cursorupd = DbClass.sqlcursor ("select best_blg from best_kopf where mdn = ? "
		                            "and fil = ? "
									"and best_blg = ? for update");

	beginwork ();
    sql_mode = 1;
	while (DbClass.sqlfetch (cursor) == 0)
	{
		 DbClass.sqlopen (cursorupd);
		 dsqlstatus = DbClass.sqlfetch (cursorupd);
		 if (dsqlstatus < 0)
		 {
			 print_mess (2, "Bestellung %ld wird im Moment bearbeitet", best_kopf.best_blg);
			 break;
		 }
	}
	DbClass.sqlclose (cursor);
	DbClass.sqlclose (cursorupd);
	commitwork ();
    sql_mode = 0;
    if (dsqlstatus < 0)
	{
		  return FALSE;
	}
	return TRUE;
}


void BereichsAuswahl (void)
/**
Bereich fuer Druck oder Lieferscheinuebergabe eingeben.
**/
{
        HANDLE hMainInst;
		HWND hWnd;
		char where [1028];

        static char mdn_von [5];
        static char mdn_bis [5];
        static char fil_von [5];
        static char fil_bis [5];
        static char best_von [9];
        static char best_bis [9];
        static char stat_von [3];
        static char stat_bis [3];
        static char dat_von [12];
        static char dat_bis [12];
		long max_best;


        static ITEM iOK ("", "     OK     ", "", 0);
        static ITEM iCA ("", "  Abbrechen ", "", 0);
        static ITEM iDW ("", "Druckerwahl ",  "", 0);


		static ITEM ivon     ("", "von", "", 0);
		static ITEM ibis     ("", "von", "", 0);
		static ITEM iMdn     ("", "Mandant.........:", "", 0);
		static ITEM iFil     ("", "Filiale.........:", "", 0);
		static ITEM iBest    ("", "Bestellung .....:", "", 0);
		static ITEM iStat    ("", "Status..........:", "", 0);
		static ITEM iDat     ("", "Bestelldatum....:", "", 0);

        static ITEM imdn_von ("mdn_von", mdn_von,  "", 0);
        static ITEM imdn_bis ("mdn_bis", mdn_bis,  "", 0);
        static ITEM ifil_von ("fil_von", fil_von,  "", 0);
        static ITEM ifil_bis ("fil_bis", fil_bis,  "", 0);
        static ITEM ibest_von ("best_von", best_von,  "", 0);
        static ITEM ibest_bis ("best_bis", best_bis,  "", 0);
        static ITEM istat_von ("stat_von", stat_von,  "", 0);
        static ITEM istat_bis ("stat_bis", stat_bis,  "", 0);
        static ITEM idat_von ("dat_von", dat_von,  "", 0);
        static ITEM idat_bis ("dat_bis", dat_bis,  "", 0);


        static field _berform[] = {
           &ivon,     0, 0, 1,18,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &ibis,     0, 0, 1,40,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iMdn,     0, 0, 1, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iFil,     0, 0, 3, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iBest,     0, 0, 5, 1,0, "",   DISPLAYONLY, 0, 0 ,0, 
           &iStat,    0, 0, 7, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iDat,     0, 0, 9, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 

           &imdn_von, 5, 0, 1,18, 0, "%4d",    EDIT, 0 ,testber,0, 
           &imdn_bis, 5, 0, 1,40, 0, "%4d",    EDIT, 0 ,testber,0, 
           &ifil_von, 5, 0, 3,18, 0, "%4d",    EDIT, 0 ,testber,0, 
           &ifil_bis, 5, 0, 3,40, 0, "%4d",    EDIT, 0 ,testber,0, 
           &ibest_von, 9, 0, 5,18, 0, "%8d",    EDIT, 0 ,testber,0, 
           &ibest_bis, 9, 0, 5,40, 0, "%8d",    EDIT, 0 ,testber,0, 
           &istat_von, 2, 0, 7,18, 0, "%1d",    EDIT, 0 ,testber,0, 
           &istat_bis, 2, 0, 7,40, 0, "%1d",    EDIT, 0 ,testber,0, 
           &idat_von, 11, 0, 9,18, 0, "dd.mm.yyyy",    EDIT, 0 ,testber,0, 
           &idat_bis, 11, 0, 9,40, 0, "dd.mm.yyyy",    EDIT, 0 ,testber,0, 

           &iOK,     13, 0,14,15, 0, "", BUTTON, 0, testber,KEY12,
           &iCA,     13, 0,14,32, 0, "", BUTTON, 0, testber,KEY5,
		};

        static form berform = {19, 0, 0, _berform, 
			                    0, 0, 0, 0, NULL};
        
        static field _berformdr[] = {
           &ivon,     0, 0, 1,18,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &ibis,     0, 0, 1,40,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iMdn,     0, 0, 1, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iFil,     0, 0, 3, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iBest,     0, 0, 5, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iStat,    0, 0, 7, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 

           &imdn_von, 5, 0, 1,18, 0, "%4d",    EDIT, 0 ,testber,0, 
           &imdn_bis, 5, 0, 1,40, 0, "%4d",    EDIT, 0 ,testber,0, 
           &ifil_von, 5, 0, 3,18, 0, "%4d",    EDIT, 0 ,testber,0, 
           &ifil_bis, 5, 0, 3,40, 0, "%4d",    EDIT, 0 ,testber,0, 
           &ibest_von, 9, 0, 5,18, 0, "%8d",    EDIT, 0 ,testber,0, 
           &ibest_bis, 9, 0, 5,40, 0, "%8d",    EDIT, 0 ,testber,0, 
           &istat_von, 2, 0, 7,18, 0, "%1d",    EDIT, 0 ,testber,0, 
           &istat_bis, 2, 0, 7,40, 0, "%1d",    EDIT, 0 ,testber,0, 

           &iOK,     15, 0,10, 6, 0, "", BUTTON, 0, testber,KEY12,
           &iCA,     15, 0,10,23, 0, "", BUTTON, 0, testber,KEY5,
           &iDW,     15, 0,10,40, 0, "", BUTTON, 0, testber,KEY7,
		};

        static form berformdr = {17, 0, 0, _berformdr, 
			                    0, 0, 0, 0, NULL};

        HWND berwin;
		int savefield;
		form *savecurrent;

        NoClose = TRUE;
        hWnd = AktivWindow;
        hMainInst = (HANDLE) GetWindowLong (hWnd, GWL_HINSTANCE);	
        save_fkt (5);
		save_fkt (6);
		save_fkt (7);
		save_fkt (9);
		save_fkt (11);
		save_fkt (12);

        set_fkt (NULL, 6);
        set_fkt (NULL, 7);
        set_fkt (NULL, 9);

        set_fkt (testber, 5);
        set_fkt (testber, 11);
        set_fkt (testber, 12);

		sysdate (dat_von);
		sysdate (dat_bis);
		strcpy (mdn_von, "1");
		strcpy (mdn_bis, "9999");
		strcpy (fil_von, "0");
		strcpy (fil_bis, "9999");
		strcpy (best_von, "1");
		strcpy (best_bis, "99999999");
        strcpy (stat_von, stat_defp_von);
        strcpy (stat_bis, stat_defp_bis);

		savefield = currentfield;
		savecurrent = current_form;
        SetAktivWindow (hWnd);
		SetButtonTab (TRUE);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_CAPTION);

        berwin = OpenWindowChC (13, 62, 6, 10, hMainInst,
                               "Druck Bestellungen");
        MoveZeWindow (hMainWindow, berwin);
        enter_form (berwin, &berformdr, 0, 0);


		SetButtonTab (FALSE);
        CloseControls (&berform);
        DestroyWindow (berwin);
        SetAktivWindow (hWnd);
		current_form = savecurrent;
		currentfield = savefield;
        restore_fkt (5);
		restore_fkt (6);
		restore_fkt (7);
		restore_fkt (9);
		restore_fkt (11);
		restore_fkt (12);

		if (syskey == KEY5) 
		{
            NoClose = FALSE;
			return;
		}

		BestKompl.InitBestanz ();
		sprintf (where, "where best_kopf.mdn between %d and %d "
			            "and   best_kopf.fil between %d and %d "
						"and   best_kopf.best_blg between %ld and %ld "
						"and   best_kopf.best_term between \"%s\" and \"%s\" "
						"and   best_kopf.bearb_stat  between %d and %d ",
						atoi (mdn_von), atoi (mdn_bis),
						atoi (fil_von), atoi (fil_bis),
						atol (best_von), atol (best_bis),
						clipped (dat_von), clipped (dat_bis),
						atoi (stat_von), atoi (stat_bis));
		EnableWindow (hMainWindow, FALSE);
        if (LockBereich (where) == FALSE) return;
        BestKompl.K_ListeGroup (atoi (mdn_von), atoi (mdn_bis),
						        atoi (fil_von), atoi (fil_bis),
						        atol (best_von), atol (best_bis),
 			                    clipped (dat_von), clipped (dat_bis),
						        atoi (stat_von), atoi (stat_bis));
  	    max_best = BestKompl.Getmax_best ();
 	    if (max_best > atol (best_bis))
		{
					   sprintf (best_bis, "%8ld", max_best);
		}
				 

 	    BestKompl.FreeBest ();
	    BestKompl.ShowLocks (hWnd);

        NoClose = FALSE;
		EnableWindow (hMainWindow, TRUE);
}


static HWND chwnd;

static int lstcancel (void)
{

    syskey = KEY5;
    break_enter ();
    return 0;
}


static int lstok (void)
{
    syskey = KEY12;
    mench = currentfield;
    break_enter ();
    return 0;
}


static int testwork (void)
{
    if (testkeys ()) return 0;

    if (syskey != KEYCR &&
        syskey != KEY12) return 0;

//    mench = 0;
    break_enter ();
    return 0;
}

static int testshow (void)
{
    if (testkeys ()) return 0;

    if (syskey != KEYCR &&
        syskey != KEY12) return 0;

//    mench = 1;
    break_enter ();
    return 0;
}

static int testdel (void)
{
    if (testkeys ()) return 0;

    if (syskey != KEYCR &&
        syskey != KEY12) return 0;

    break_enter ();
    return 0;
}


static int testprint (void)
{
    if (testkeys ()) return 0;

    if (syskey != KEYCR &&
        syskey != KEY12) return 0;

    break_enter ();
    return 0;
}

static int testfree (void)
{
    if (testkeys ()) return 0;

    if (syskey != KEYCR &&
        syskey != KEY12) return 0;

    break_enter ();
    return 0;
}

static int testcancel (void)
{
    if (testkeys ()) return 0;

    if (syskey != KEYCR &&
        syskey != KEY12) return 0;

	syskey = KEY5;
    break_enter ();
    return 0;
}

        static ColButton LstChoise = {
                              "Auswahl", -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
//                               WHITECOL,
//                               RGB (0,0,120),
                               BLACKCOL,
                               GRAYCOL,
                               -2};
   
        static ColButton cWork = {
                              "Bearbeiten", -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               WHITECOL,
                               BLUECOL,
                               2};

        static ColButton cShow = {
                              "Anzeigen", -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               WHITECOL,
                               BLUECOL,
                               2};

        static ColButton cDel = {
                              "L�schen", -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               WHITECOL,
                               BLUECOL,
                               2};

        static ColButton cPrint = {
                              "Drucken", -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               WHITECOL,
                               BLUECOL,
                               -1};

        static ColButton cFree = {
                              "Freigeben", -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               WHITECOL,
                               BLUECOL,
                               2};
							  static ColButton *cButtons [] = {&cWork, &cShow, &cDel, &cPrint, &cFree, NULL}; 



        static ITEM iLstChoise ("", (char *) &LstChoise,  "", 0);

        static ITEM iWork      ("", (char *) &cWork,      "", 0);
        static ITEM iShow      ("", (char *) &cShow,      "", 0);
        static ITEM iDel       ("", (char *) &cDel,       "", 0);
        static ITEM iPrint     ("", (char *) &cPrint,     "", 0);
        static ITEM iFree      ("", (char *) &cFree,      "", 0);

        static ITEM iWorkB      ("", (char *) "Bearbeiten",  "", 0);
        static ITEM iShowB      ("", (char *) "Anzeigen",    "", 0);
        static ITEM iDelB       ("", (char *) "L�schen",     "", 0);
        static ITEM iPrintB     ("", (char *) "Drucken",     "", 0);
        static ITEM iFreeB      ("", (char *) "Freigeben",   "", 0);

        static ITEM vOK     ("",  "    OK     ",  "", 0);
        static ITEM vCancel ("",  " Abbrechen ", "", 0);

        static field _fLstChoise[] = {
          &iLstChoise, 36, 0,  0, 0, 0, "", COLBUTTON, 0, 0, 0};

        static form fLstChoise = {1, 0, 0, _fLstChoise, 
                                  0, 0, 0, 0, NULL};    

/*
        static field _clist [] = {
&iWork,     34, 2, 2, 1, 0, "", COLBUTTON,            0, testwork, 
                                                           KEY12 , 
&iShow,     34, 2, 4, 1, 0, "", COLBUTTON,            0, testshow, 
                                                          KEY12,
&iDel,      34, 2, 6, 1, 0, "", COLBUTTON,            0, testdel, 
                                                           KEY12,
&iPrint,    34, 2, 8, 1, 0, "", COLBUTTON,            0, testprint, 
                                                           KEY12,
&vCancel,  12, 0, 9, 12, 0, "",  BUTTON,               0, testcancel, 
                                                           KEY5, 
};
*/

        static field _clist [] = {
&iWork,     16, 3, 0,  0, 0,"", COLBUTTON,            0, testwork, 
                                                           KEY12 , 
&iShow,     16, 3, 0, 18, 0,"", COLBUTTON,            0, testshow, 
                                                          KEY12,
&iDel,      16, 3, 0, 36, 0,"", COLBUTTON,            0, testdel, 
                                                           KEY12,
&iPrint,    16, 3, 0, 54, 0, "", COLBUTTON,           0, testprint, 
                                                           KEY12,
&vCancel,   12, 0, 0, 12, 0, "",  REMOVED,           0, testcancel, 
                                                           KEY5, 
};

        static field _clistB [] = {
&iWorkB,     30, 0, 2, 3, 0, "", BUTTON,            0, testwork, 
                                                           KEY12 , 
&iShowB,     30, 0, 3, 3, 0, "", BUTTON,            0, testshow, 
                                                          KEY12,
&iDelB,      30, 0, 4, 3, 0, "", BUTTON,            0, testdel, 
                                                           KEY12,
&iPrintB,    30, 0, 5, 3, 0, "", BUTTON,            0, testprint, 
                                                           KEY12,
// &iFreeB,     30, 0, 6, 3, 0, "", BUTTON,            0, testfree, 
//                                                            KEY12,
&vCancel,   12, 0,  7,12, 0, "",  BUTTON,               0, testcancel, 
                                                           KEY5, 
};


static form clist = {5, 0, 0, _clistB, 0, 0, 0, 0, NULL}; 


void StartMenu (void)
/**
Liste auswaehlen und Listengenerator starten
**/
{
     form *sform;
     int  sfield;


	 if (NoStartMenue)
	 {
	         EnableMe ();
             mench = 0;
             EingabeMdnFil ();
         	 return;
	 }

	 DisableMe ();
     while (TRUE)
     {
        sform = current_form;
        sfield = currentfield;
        save_fkt (5);
        save_fkt (12);

        set_fkt (lstcancel, 5);
        set_fkt (lstok, 12);

        no_break_end ();
        SetButtonTab (TRUE);

        SetAktivWindow (mamain1);
        if (clist.mask == _clist)
		{
                   InStartMenue = TRUE;
		           InvalidateRect (mamain1, NULL, TRUE);
                   SetBorder (WS_POPUP);
                   chwnd = OpenWindowCh (4, 54 + 18, 0, 2, hMainInst);
		           MoveTopWindow (mamain1, chwnd);
		}
		else
		{
                   SetBorder (WS_POPUP | WS_DLGFRAME);
                   chwnd = OpenWindowCh (10, 36, 10, 22, hMainInst);
		           MoveZeWindow (mamain1, chwnd);
		}
        DlgWindow = chwnd;

        if (clist.mask != _clist)
		{
                  display_form (chwnd, &fLstChoise, 0, 0);
		}
        currentfield = 0;
        enter_form (chwnd, &clist, 0, 0);
		mench = currentfield;
		CloseControls (&fLstChoise);
        CloseControls (&clist);
        SetAktivWindow (mamain1);
        DestroyWindow (chwnd);
        InStartMenue = FALSE;
		InvalidateRect (mamain1, NULL, FALSE);
        chwnd = NULL;
        SetButtonTab (FALSE);
        restore_fkt (5);
        restore_fkt (12);
        if (sform)
        {
                current_form = sform;
                currentfield = sfield;
                SetCurrentFocus (currentfield);
        }

        if (syskey == KEY5) return;
        if (syskey == KEYESC) return;

        switch (mench)
        {
              case 0 :
              case 1 :
              case 2 :
                     EingabeMdnFil ();
					 break;
              case 3 :
			         ProcWaitExecEx ("rswrun 33000", SW_SHOWNORMAL, -1, 0, -1, 0);
//                     BereichsAuswahl ();
					 break;
        }
     }
}


void SetListBkColor (char *coltxt)
/**
Hintergrund f�r Liste setzen.
**/
{
	   int i;
       static char *BkColorsTxt[] = {"WHITECOL",
                                     "BLUECOL",
                                     "BLACKCOL",
                                     "GRAYCOL",
					      		     "LTGRAYCOL",
	   };

       clipped (coltxt);
       for (i = 0; Combo2[i]; i ++)
       {
             if (strcmp (BkColorsTxt[i],coltxt) == 0)
             {
                   bestpListe.SetColors (Colors[i], BkColors[i]);
				   combo2pos = i;
                   break;
             }
	   }
}


void GetCfgValues (void)
/**
Werte aus 51100.cfg holen.
**/
{
	   static BOOL cfgOK = FALSE;
       char cfg_v [20];

	   if (cfgOK) return;

	   cfgOK = TRUE;
       if (ProgCfg.GetCfgValue ("NewStyle", cfg_v) == TRUE)
       {
                    NewStyle = atoi (cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("Startsize", cfg_v) == TRUE)
       {
                    Startsize = atoi (cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("colbutton", cfg_v) == TRUE)
       {
		            if (atoi (cfg_v))
					{
					       clist.mask = _clist;
					}
       }
       if (ProgCfg.GetCfgValue ("searchmodea_bas", cfg_v) == TRUE)
       {
		            SetBasSearchMode (atoi (cfg_v));
       }
       if (ProgCfg.GetCfgValue ("searchfielda_bas", cfg_v) == TRUE)
       {
		            SetBasSearchField (atoi (cfg_v));
       }
       if (ProgCfg.GetCfgValue ("searchmodelief", cfg_v) == TRUE)
       {
		            lief_class.SetSearchModeLief (atoi (cfg_v));
       }
       if (ProgCfg.GetCfgValue ("searchfieldlief", cfg_v) == TRUE)
       {
		           lief_class.SetSearchFieldLief (atoi (cfg_v));
       }

       if (ProgCfg.GetCfgValue ("stat_defp_von", cfg_v) == TRUE)
       {
                     strcpy (stat_defp_von, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("stat_defp_bis", cfg_v) == TRUE)
       {
                     strcpy (stat_defp_bis, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("stat_deff_von", cfg_v) == TRUE)
       {
                     strcpy (stat_deff_von, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("stat_deff_bis", cfg_v) == TRUE)
       {
                     strcpy (stat_deff_bis, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("abt_def_von", cfg_v) == TRUE)
       {
                     strcpy (abt_def_von, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("abt_def_bis", cfg_v) == TRUE)
       {
                     strcpy (abt_def_bis, cfg_v);
       }

       if (ProgCfg.GetCfgValue ("lief_sperr", cfg_v) == TRUE)
       {
                     lief_sperr = min (max (0, atoi (cfg_v)), 1);
       }
       if (ProgCfg.GetCfgValue ("use_kopf_txt", cfg_v) == TRUE)
       {
                     use_kopf_txt = min (max (0, atoi (cfg_v)), 1);
       }

       if (ProgCfg.GetCfgValue ("datum_plus", cfg_v) == TRUE)
       {
                     datum_plus = atol (cfg_v);
       }

/*
       if (ProgCfg.GetCfgValue ("wo_tag_plus", cfg_v) == TRUE)
       {
                     QTClass.SetWotagplus (atol (cfg_v));
       }

       if (ProgCfg.GetCfgValue ("satour", cfg_v) == TRUE)
       {
                     QTClass.SetSatour (atol (cfg_v));
       }
       if (ProgCfg.GetCfgValue ("sotour", cfg_v) == TRUE)
       {
                     QTClass.SetSotour (atol (cfg_v));
       }
*/
       if (ProgCfg.GetCfgValue ("format", cfg_v) == TRUE)
       {
		             BestKompl.SetDrFormat (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("SAFCOLOR", cfg_v) == TRUE)
       {
		             bestpListe.GetListColor (&MessCol, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("SABCOLOR", cfg_v) == TRUE)
       {
		             bestpListe.GetListColor (&MessBkCol, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("with_log", cfg_v) == TRUE)
       {
		             BestKompl.SetLog (atoi (cfg_v));
       }
       if (ProgCfg.GetCfgValue ("PrintMode", cfg_v) == TRUE)
       {
		             PrintMode = atoi (cfg_v);
		             BestKompl.SetPrintMode (atoi (cfg_v));
       }
       if (ProgCfg.GetCfgValue ("komplettmode", cfg_v) == TRUE)
       {
                     KomplettMode = min (max (0, atoi (cfg_v)), 2);
       }
       if (ProgCfg.GetCfgValue ("autodatum", cfg_v) == TRUE)
       {
                     autosysdat = min (max (0, atoi (cfg_v)), 1);
       }
       if (ProgCfg.GetCfgValue ("ldatdiff", cfg_v) == TRUE)
       {
                     ldatdiff = atol (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("bdatdiff", cfg_v) == TRUE)
       {
                     bdatdiff = atol (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("textinpmode", cfg_v) == TRUE)
       {
                     textinpmode = atol (cfg_v);
                     bestpListe.SetTxtMode (textinpmode); 
	   }
       if (ProgCfg.GetCfgValue ("NoStartMenue", cfg_v) == TRUE)
       {
                     NoStartMenue = min (1, max (0, atol (cfg_v)));
	   }
/*
       if (ProgCfg.GetCfgValue ("lieferdatum", cfg_v) == TRUE)
       {
                     lieferdatum = min (1, max (0, atol (cfg_v)));
	   }
*/
       if (ProgCfg.GetCfgValue ("ListBkColor", cfg_v) == TRUE)
       {
		             SetListBkColor (cfg_v);
	   }
}

void MoveMamain (void)
/**
Koordinaten in $BWSETC lesen.
**/
{
        char *etc;
        char buffer [256];
        FILE *fp;
        int anz;
		static BOOL scrfOK = FALSE;
		RECT rect;
	    int xfull, yfull;

	    if (MoveMain == FALSE) return;
        xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
        yfull = GetSystemMetrics (SM_CYFULLSCREEN);        
	    if (xfull < 900) return;

        etc = getenv ("BWSETC");
        if (etc == NULL)
        {
             etc = "\\user\\fit\\etc";
        }
        sprintf (buffer, "%s\\fit.rct", etc);

        fp = fopen (buffer, "r");
        if (fp == NULL) return;

        if (fgets (buffer, 255, fp) == 0) 
        {
                 fclose (fp);          
                 return;
        }
        if (fgets (buffer, 255, fp) == 0) 
        {
                 fclose (fp);          
                 return;
        }
        fclose (fp);

        anz = wsplit (buffer, " ");
        if (anz < 4) 
        {
            return;
        }

        rect.left   = atoi (wort[0]);
        rect.top    = atoi (wort[1]);
        rect.right  = atoi (wort[2]);
        rect.bottom = atoi (wort[3]);
		rect.left ++; 
		rect.top ++; 
		rect.right  = rect.right  - rect.left - 2;
		rect.bottom = rect.bottom - rect.top - 2;
        MoveWindow (hMainWindow, rect.left, rect.top, rect.right, rect.bottom, TRUE);
        return;
}

void SetcButtons (COLORREF Col, COLORREF BkCol)
/**
Farbe der Button setzen.
**/
{
	    int i;

		for (i = 0; cButtons[i]; i ++)
		{
                 cButtons[i]->Color   = Col;
				 cButtons[i]->BkColor = BkCol;
		}
}

int    PASCAL WinMain(HANDLE hInstance,HANDLE hPrevInstance,
                        LPSTR lpszCmdLine,int nCmdShow)
{
       int anz; 
       int i;
       char argtab[20][80];
       char *varargs[20];
	   char *mv;
	   char *tr;

	   LoadLibrary ("RICHED32.DLL");
	   GetForeground ();   
	   SetcButtons (BLACKCOL, GRAYCOL);

	   ColBorder = TRUE;
       tr = getenv_default ("COLBORDER");
	   if (tr)
	   {
			ColBorder = max(0, min (1, atoi (tr)));
	   }
	   ArrDown = LoadBitmap (hInstance, "ARRDOWN");
	   iarrdown.SetFeldPtr ((char *) &ArrDown);
       opendbase ("bws");
       menue_class.SetSysBen ();
	   GetCfgValues ();
	   mv = getenv_default ("MOVEMAIN");
	   if (mv)
	   {
		   MoveMain = min (1, max (0, atoi (mv)));
	   }


/*
	   if (lieferdatum == FALSE)
	   {
		                 SetItemAttr (&angform, "lieferdat", REMOVED);
		                 SetItemAttr (&angform, "lieferzeit", REMOVED);
	   }
*/


       anz = wsplit (lpszCmdLine, " ");
       for (i = 0; i < anz; i ++)
       {
            strcpy (argtab[i], wort[i]);
            varargs[i] = (char *) argtab[i];
       }
       argtst (&anz, varargs, tst_arg);

       SetStdProc (WndProc);
       InitFirstInstance (hInstance);
       InitNewInstance (hInstance, nCmdShow);


       if (WithToolBar)
       {
	               hwndTB = MakeToolBarEx (hInstance, 
		                       hMainWindow,tbb, 56 ,
                               qInfo, qIdfrom,
		                       qhWndInfo, qhWndFrom);

                   hwndCombo1 = MakeToolBarCombo (hInstance,
                                      hMainWindow,
                                      hwndTB, 
                                      10, 
                                      11, 
                                      32);

                   FillCombo1 ();
                   ComboToolTip (hwndTB, hwndCombo1);
 
                   hwndCombo2 = MakeToolBarCombo (hInstance,
                                                  hMainWindow,
                                                  hwndTB, 
                                                  11, 
                                                  33, 
                                                  52);

                   FillCombo2 ();
                   ComboToolTip (hwndTB, hwndCombo2);


                   bestpListe.SethwndTB (hwndTB);
                   DisableTB ();
       }

	   if (PrintMode > 1)
	   {
			      EnableMenuItem (hMenu,  IDM_PRCHOISE,  MF_GRAYED);
	   }
       if (NewStyle)
       {
             ftasten = OpenFktEx (hInstance);
             ITEM::SetHelpName ("32300.cmd");
             CreateFktBitmap (hInstance, hMainWindow, hwndTB);
//             SetFktMenue (TestMenue);
       }
       else
       {
             ftasten = OpenFktM (hInstance);
       }
       Mess.OpenMessage ();
       Createmamain1 ();
       MoveMamain ();
       if (WithMenue)
       {
	               hMenu = MakeMenue (menuetab);
	               SetMenu (hMainWindow, hMenu);
       }

	   if (Startsize == 1)
	   {
	               ShowWindow (hMainWindow, SW_SHOWMAXIMIZED);
	   }

       SetEnvFont ();
       lstxtlist.SetListFont (&lFont);
       bestpListe.SetListFont (&lFont);


       telefon1     = LoadIcon (hMainInst, "tele1");
       fax          = LoadIcon (hMainInst, "fax");

       btelefon       = LoadBitmap (hInstance, "btele");
       btelefoni      = LoadBitmap (hInstance, "btelei");
       CTelefon.bmp   = btelefoni;

       display_form (mamain1, &buform, 0, 0);
       MoveButtons ();
       CreateQuikInfos ();


       CheckMenuItem (hMenu, IDM_PAGEVIEW, MF_UNCHECKED); 
       ToolBar_PressButton(hwndTB, IDM_PAGE, FALSE);
       ToolBar_PressButton(hwndTB, IDM_LIST, TRUE);

       bestpListe.SethMainWindow (mamain1);
//       EingabeMdnFil ();
 
       StartMenu ();

	   FreeBestk ();
	   SetForeground ();
	   closedbase ();
       return 0;
}


void InitFirstInstance(HANDLE hInstance)
{
        
        WNDCLASS wc;
		HDC hdc;
		int col;

		col = 0;
		hdc = GetDC (NULL);
		col = GetDeviceCaps (hdc, BITSPIXEL);
		ReleaseDC (NULL, hdc);
        if (col < 16) ColBorder = FALSE;

		SetEnvFont ();

		SetSWEnvFont ();

		strcpy (lFont.FontName, FontNameSW);
        lFont.FontHeight = FontHeightSW;
        lFont.FontWidth  = FontWidthSW;       
        lFont.FontAttribute  = FontAttributeSW;       


		if (hbrBackground == NULL)
		{
			hbrBackground = CreateSolidBrush (StdBackCol);
		}


        wc.style         =  CS_BYTEALIGNWINDOW
                            | CS_CLASSDC;
        wc.lpfnWndProc   =  WndProc;
        wc.cbClsExtra    =  0;
        wc.cbWndExtra    =  0;
        wc.hInstance     =  hInstance;
        wc.hIcon         =  LoadIcon (hInstance, "FITICON");
        wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
//        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
//        wc.hbrBackground =  CreateSolidBrush (StdBackCol);
        wc.hbrBackground =  hbrBackground;
        wc.lpszMenuName  =  "";
        wc.lpszClassName =  "hStdWindow";

        RegisterClass(&wc);

        wc.hIcon         =  LoadIcon (hInstance, "NOICON");
        wc.lpszMenuName  =  NULL;
        wc.lpszClassName =  "ListMain";
        RegisterClass(&wc);


        wc.hbrBackground =  GetStockObject (WHITE_BRUSH);
        wc.lpszClassName =  "hListWindow";
        RegisterClass(&wc);

        wc.lpfnWndProc   =  StaticWProc;
        wc.hIcon         =  LoadIcon (hInstance, "NOICON");
        wc.lpszMenuName  =  NULL;
        wc.lpszClassName =  "StaticWhite";
        RegisterClass(&wc);

        wc.lpfnWndProc   =  WndProc;
        wc.hIcon         =  LoadIcon (hInstance, "NOICON");
        wc.lpszMenuName  =  NULL;
        wc.hbrBackground =  CreateSolidBrush (MessBkCol);
        wc.lpszClassName =  "StaticMess";
        RegisterClass(&wc);
        hMainInst = hInstance;
        return;
}


BOOL InitNewInstance(HANDLE hInstance, int nCmdShow)
{
        HDC hdc;
        HFONT hFont, oldFont;
        char *Caption;
        SIZE size;

        hdc = GetDC (NULL);
        hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
        oldFont = SelectObject (hdc,hFont);
        GetTextMetrics (hdc, &tm);
        GetTextExtentPoint32 (hdc, "X", 1, &size);
        tm.tmAveCharWidth = size.cx;
        ReleaseDC (0, hdc);
        bestpListe.SetTextMetric (&tm);
        if (InfoCaption[0])
        {
            Caption = InfoCaption;
        }
        else
        {
            Caption = "Bestellungen erfassen";
        }

        SetBorder (WS_THICKFRAME | WS_CAPTION | 
                   WS_SYSMENU | WS_MINIMIZEBOX |
                   WS_MAXIMIZEBOX);

        hMainWindow = OpenWindowChC (26, 80, 2, 0, hInstance, 
                      Caption);
        return 0; 
}


static int ProcessMessages(void)
{
        MSG msg;

        while (GetMessage (&msg, NULL, 0, 0))
        {
             if (msg.message == WM_KEYDOWN)
             {
                       switch (msg.wParam)
                       {
                              case VK_F5 :
                                  syskey = KEY5;
                                  PostQuitMessage (0);
                                  continue; ;
                              case VK_RETURN :
                                  syskey = KEYCR;
                                  EingabeMdnFil ();
                                  continue; ;
                       }
             }
             TranslateMessage(&msg);
             DispatchMessage(&msg);
        }
        return msg.wParam;
}

void InvalidateLines (void)
{
         RECT rect;
         static TEXTMETRIC tm;
         HDC hdc;

         return;

         hdc = GetDC (hMainWindow);
         GetTextMetrics (hdc, &tm);
         ReleaseDC (hMainWindow, hdc);
         GetClientRect (hMainWindow, &rect);

         rect.top = 2 * tm.tmHeight;
         rect.top = rect.top + rect.top / 3;
         rect.top -= 14;
         rect.bottom = rect.top + 10;
         InvalidateRect (hMainWindow, &rect, TRUE);
}


void PrintLines (HDC hdc)
/**
Linien am Bildschirm anzeigen.
**/
{
         static TEXTMETRIC tm;
         static HPEN hPenG = NULL;
         static HPEN hPenW = NULL;
         static HPEN hPenB = NULL;
         RECT rect;
		 RECT wrect;
         int x, y;

         HFONT hFont, oldfont;

         stdfont ();
         hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
         oldfont = SelectObject (hdc,hFont);
         GetTextMetrics (hdc, &tm);
         DeleteObject (SelectObject (hdc, oldfont));
         y = 2 * tm.tmHeight;
         y = y + y / 3;
		 y -= 14;
         GetClientRect (mamain1, &rect);
         GetWindowRect (mamain1, &wrect);
         x = rect.right - 2;
         if (hPenG == NULL)
         {
                   hPenG = CreatePen (PS_SOLID, 0, GRAYCOL);
                   hPenW = CreatePen (PS_SOLID, 0, WHITECOL);
                   hPenB = CreatePen (PS_SOLID, 0, BLACKCOL);
         }

/* Linie oben                 */

		 if (InStartMenue == FALSE)
		 {
         SelectObject (hdc, hPenG);
         MoveToEx (hdc, 0, y, NULL);
         LineTo (hdc, x + 2, y);

         y ++;
         SelectObject (hdc, hPenW);
         MoveToEx (hdc, 0, y, NULL);
         LineTo (hdc, x + 2, y);
		 }

/* Linie mitte                */
         
		 y += 5 * tm.tmHeight;
		 y += tm.tmHeight / 2;
         SelectObject (hdc, hPenG);
         MoveToEx (hdc, 0, y, NULL);
         LineTo (hdc, x + 2, y);

         y ++;
         SelectObject (hdc, hPenW);
         MoveToEx (hdc, 0, y, NULL);
         LineTo (hdc, x + 2, y);
		 y -= tm.tmHeight / 2;


/* Linie unten                */
/*         
		 y += 6 * tm.tmHeight;
         SelectObject (hdc, hPenG);
         MoveToEx (hdc, 0, y, NULL);
         LineTo (hdc, x + 2, y);

         y ++;
         SelectObject (hdc, hPenW);
         MoveToEx (hdc, 0, y, NULL);
         LineTo (hdc, x + 2, y);
*/

		 y += (2 + wrect.top);
		 bestpListe.SetLiney (y);
         bestpListe.MoveMamain1 ();
}

void DisplayLines ()
{
         HDC hdc;

         hdc = GetDC (mamain1);
         PrintLines (hdc);
         ReleaseDC (mamain1, hdc);
}

void ChoiseCombo1 (void)
/**
Aktuell gewaehlten eintrag in Combobox 1 ermitteln.
**/
{
          int i;
          char text [80];

          GetWindowText (hwndCombo1,
                         text,
                         79);
          clipped (text);
          for (i = 0; Combo1[i]; i ++)
          {
              if (strcmp (Combo1[i],text) == 0)
              {
                   bestpListe.SetListLines (i);
                   break;
              }
          }
}

void ChoiseCombo2 (void)
/**
Aktuell gewaehlten eintrag in Combobox 1 ermitteln.
**/
{
          int i;
          char text [80];

          GetWindowText (hwndCombo2,
                         text,
                         79);
          clipped (text);
          for (i = 0; Combo2[i]; i ++)
          {
              if (strcmp (Combo2[i],text) == 0)
              {
                   bestpListe.SetColors (Colors[i], BkColors[i]);
                   break;
              }
          }
}

void disp_forms (void)
{
      if (bestformk.mask[0].feldid) display_form (mamain1, &bestformk, 0, 0);
      if (bestform.mask[0].feldid)  display_form (mamain1, &bestform, 0, 0);
}

int PrintAng (void)
{
	      int ret;
		  FORM *scurrent;
		  int   sfield;

		  scurrent = current_form;
		  sfield   = currentfield;
/*
	      if (StartMen == TRUE)
		  {
			         mench = 3;
                     BereichsAuswahl ();
					 mench = 0;
					 EnableMe ();
		             current_form = scurrent;
		             currentfield = sfield;
					 return 0;
		  }
*/
	      if (StartMen == TRUE)
		  {
			         ProcWaitExecEx ("rswrun 33000", SW_SHOWNORMAL, -1, 0, -1, 0);
		             current_form = scurrent;
		             currentfield = sfield;
					 return 0;
		  }

	      NoClose = TRUE;
          if (bestpListe.GetPosanz () == 0)
		  {
			         NoClose = FALSE;
					 disp_mess ("Es wurden kein Positionen erfasst", 2);
					 SetCurrentField (currentfield);
		             current_form = scurrent;
		             currentfield = sfield;
					 return (1);
		  }
 		  if (testlief0 () == FALSE)
		  {
			         NoClose = FALSE;
		             current_form = scurrent;
		             currentfield = sfield;
					 return 1;
		  }
	      if (abfragejn (hMainWindow, "Bestellung ausdrucken ?", "J") == FALSE)
		  {
			         NoClose = FALSE;
		             current_form = scurrent;
		             currentfield = sfield;
					 return 1;
		  }
          bestpListe.DestroyWindows ();
          FormToBest (3);
		  best_kopf.mdn = atoi (mdn);
		  best_kopf.fil = atoi (fil);
		  best_kopf.best_blg = atoi (best_nr);
		  bestk_class.dbupdate ();
          if (best_kopf.best_blg != auto_nr.nr_nr)
          {

                       dsqlstatus = AutoClass.nveinid (atoi (mdn), 0,
                                            "best",  auto_nr.nr_nr);
          }
		  ret = BestKompl.PrintBest (hMainWindow, best_kopf.mdn, best_kopf.fil, 
			                                      best_kopf.best_blg);
          syskey = KEY12;
          break_enter ();
          NoClose = FALSE;
          if (ret == 0)
		  {
			  Mess.Message ("Die Bestellung wurde gedruckt");
		  }
          current_form = scurrent;
          currentfield = sfield;
          return 0;
}


void ShowPrinters (void)
{
	    if (PrintMode == 0)
		{
			   ChoisePrinter0 ();
		}
		else if (PrintMode == 1)
		{
			   ChoisePrinter1 ();
		}
}


LONG FAR PASCAL WndProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        HDC hdc;
        char cfg_v [20];

        switch(msg)
        {
              case WM_PAINT :
                      if (hWnd == mamain1)
                      {
                              hdc = BeginPaint (hWnd, &aktpaint);
                              PrintLines (hdc);
                              EndPaint (hWnd, &aktpaint);
                      }
                      else 
                      {
                              bestpListe.OnPaint (hWnd, msg, wParam, lParam);
                              lstxtlist.OnPaint (hWnd, msg, wParam, lParam);
                      }
                      break;
              case WM_MOVE :
                      if (hWnd == hMainWindow)
                      {
                              bestpListe.MoveMamain1 ();
                              lstxtlist.MoveMamain1 ();
                      }
                      else if (hWnd == mamain1)
                      {
                              bestpListe.MoveMamain1 ();
                              lstxtlist.MoveMamain1 ();
                      }
                      else
                      {
                              bestpListe.OnSize (hWnd, msg, wParam, lParam);
                              lstxtlist.OnSize (hWnd, msg, wParam, lParam);
                      }
                      break;
              case WM_SIZE :
                      if (hWnd == hMainWindow)
                      {
                              MoveFkt ();
                              Mess.MoveMess ();
                              MoveMamain1 ();  
                              hdc = GetDC (hMainWindow);
                              ReleaseDC (hMainWindow, hdc);
                      }
                      else if (hWnd == mamain1)
                      {
                              bestpListe.MoveMamain1 ();
                              lstxtlist.MoveMamain1 ();
                      }
                      else
                      {
                              bestpListe.OnSize (hWnd, msg, wParam, lParam);
                              lstxtlist.OnSize (hWnd, msg, wParam, lParam);
                      }
                      break;

              case WM_SYSCOMMAND:
                      if (hWnd == bestpListe.GetMamain1 ()
                          && wParam == SC_MINIMIZE)
                      {
                                   bestpListe.SetMin ();

                      }
                      else if (hWnd == bestpListe.GetMamain1 ()
                               && wParam == SC_MAXIMIZE)
                      {
                                   bestpListe.SetMax ();
                                   bestpListe.MaximizeMamain1 ();
                                   return 0;

                      }
                      else if (hWnd == bestpListe.GetMamain1 ()
                               && wParam == SC_RESTORE)
                      {
                                   bestpListe.InitMax ();
                                   bestpListe.InitMin ();
                                   ShowWindow (bestpListe.GetMamain1 (), 
                                               SW_SHOWNORMAL);
                                   bestpListe.MoveMamain1 ();
                                   return 0;
                      }
                      else if (hWnd == bestpListe.GetMamain1 ()
                               && wParam == SC_CLOSE)
                      {
                                   if (bestpListe.IsListAktiv ())
                                   {
                                           syskey = KEY5;
                                           SendKey (VK_F5);
                                   }
                                   return 0;
                      }
                      else if (hWnd == hMainWindow && wParam == SC_CLOSE)
                      {
						           if (NoClose)
								   {
						                    disp_mess (
												"Abbruch durch Windowmanager ist nicht m�glich", 
												2);
								            return 0;
								   }
                                   if (abfragejn (hMainWindow, 
                                                  "Bearbeitung abbrechen ?",
                                                  "N") == 0)
                                   {
                                           return 0;
                                   }
                                   rollbackwork ();
                                   beginwork ();
                                   dsqlstatus = AutoClass.nveinid (atoi (mdn), 0,
                                                "best",  auto_nr.nr_nr);
                                   commitwork ();
	                               FreeBestk ();
	                               SetForeground ();
	                               closedbase ();
                                   ExitProcess (0);
                                   break;
                                   
                      }
                      break;

              case WM_HSCROLL :
                       bestpListe.OnHScroll (hWnd, msg,wParam, lParam);
                       lstxtlist.OnHScroll (hWnd, msg,wParam, lParam);
                       break;
                      
              case WM_VSCROLL :
                       bestpListe.OnVScroll (hWnd, msg, wParam, lParam);
                       lstxtlist.OnVScroll (hWnd, msg, wParam, lParam);
                       break;

              case WM_NOTIFY :
                    {
                      LPNMHDR pnmh = (LPNMHDR) lParam;

                      if (pnmh->code == TTN_NEEDTEXT)
                      {
                           LPTOOLTIPTEXT lpttt = (LPTOOLTIPTEXT) lParam;
                           if (QuickCpy (lpttt->szText, lpttt->hdr.idFrom)
                               == FALSE)
                           {
                                    QuickHwndCpy (lpttt);
                           }
                      }
                      break;
                    }
	
              case WM_DESTROY :
				  OnDestroy (hWnd); 
                      if (hWnd == hMainWindow)
                      {
                             PostQuitMessage (0);
                             return 0;
                      }
                      break;
              case WM_KEYDOWN :
                      if (bestpListe.GetMamain1 ())
                      {
                               bestpListe.FunkKeys (wParam, lParam);
                      }
                      break;
                  
              case WM_COMMAND :
                    if (LOWORD (wParam) == KEYESC)
                    {
                            syskey = KEYESC;
                            SendKey (VK_ESCAPE);
                            break;
                    }
                    if (LOWORD (wParam) == KEY2)
                    {
                            syskey = KEY2;
                            SendKey (VK_F2);
                            break;
                    }
                    if (LOWORD (wParam) == KEY5)
                    {
                            syskey = KEY5;
                            SendKey (VK_F5);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY6)
                    {
                            syskey = KEY6;
                            SendKey (VK_F6);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY7)
                    {
                            syskey = KEY7;
                            SendKey (VK_F7);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY8)
                    {
                            syskey = KEY8;
                            SendKey (VK_F8);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY9)
                    {
                            syskey = KEY9;
                            SendKey (VK_F9);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY10)
                    {
                            syskey = KEY10;
                            SendKey (VK_F10);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY11)
                    {
                            syskey = KEY11;
                            SendKey (VK_F11);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY12)
                    {
                            syskey = KEY12;
                            SendKey (VK_F12);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYPGD)
                    {
                            syskey = KEYPGD;
                            SendKey (VK_NEXT);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYPGU)
                    {
                            syskey = KEYPGU;
                            SendKey (VK_PRIOR);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYDOWN)
                    {
                            syskey = KEYDOWN;
                            SendKey (VK_DOWN);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYUP)
                    {
                            syskey = KEYUP;
                            SendKey (VK_UP);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYLEFT)
                    {
                            syskey = KEYLEFT;
                            SendKey (VK_LEFT);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYRIGHT)
                    {
                            syskey = KEYRIGHT;
                            SendKey (VK_RIGHT);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYSTAB)
                    {
                            syskey = KEYSTAB;
                            SendKey (VK_LEFT);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYTAB)
                    {
                            syskey = KEYTAB;
                            SendKey (VK_TAB);
                            break;
                    }
                    else if (LOWORD (wParam) == IDM_VINFO)
                    {
						    InfoVersion (); 
                            break;
                    }
                   else if (LOWORD (wParam) == QUERYBEST)
				   {
					        QueryBest ();
				   }
                   else if (LOWORD (wParam) == QUERYLIEF)
				   {
				            QueryLief ();
				   }
				   else if (LOWORD (wParam) == IDM_FRAME)
                   {
						    CheckMenuItem (hMenu, ActiveMark,
								           MF_UNCHECKED); 
						    CheckMenuItem (hMenu, IDM_FRAME,
								           MF_CHECKED); 
							ActiveMark = IDM_FRAME;
                            syskey = KEY3;
                            SendKey (VK_F3);
                   }
				   else if (LOWORD (wParam) == IDM_REVERSE)
                   {
						    CheckMenuItem (hMenu, ActiveMark,
								           MF_UNCHECKED); 
						    CheckMenuItem (hMenu, IDM_REVERSE,
								           MF_CHECKED); 
							ActiveMark = IDM_REVERSE;
                            syskey = KEY4;
                            SendKey (VK_F4);
                   }
				   else if (LOWORD (wParam) == IDM_NOMARK)
                   {
						    CheckMenuItem (hMenu, ActiveMark,
								           MF_UNCHECKED); 
						    CheckMenuItem (hMenu, IDM_NOMARK,
								           MF_CHECKED); 
							ActiveMark = IDM_NOMARK;
                            syskey = KEY6;
                            SendKey (VK_F6);
                   }
				   else if (LOWORD (wParam) == IDM_EDITMARK)
                   {
						    CheckMenuItem (hMenu, ActiveMark,
								           MF_UNCHECKED); 
						    CheckMenuItem (hMenu, IDM_EDITMARK,
								           MF_CHECKED); 
							ActiveMark = IDM_EDITMARK;
                            syskey = KEY7;
                            SendKey (VK_F7);
                   }
				   else if (LOWORD (wParam) == IDM_PAGEVIEW)
                   {
                            if (bestpListe.GetRecanz () < 1) break;
                            if (PageView)
                            {
						            CheckMenuItem (hMenu, IDM_PAGEVIEW,
								                   MF_UNCHECKED); 
                                    ToolBar_PressButton(hwndTB, IDM_PAGE, FALSE);
                                    ToolBar_PressButton(hwndTB, IDM_LIST, TRUE);
                                    ToolBar_SetState(hwndTB, IDM_PAGE, TBSTATE_ENABLED);
                                    PageView = 0;
                                    bestpListe.SwitchPage0 (bestpListe.GetAktRowS ());
                            }
                            else
                            {
						            CheckMenuItem (hMenu, IDM_PAGEVIEW,
								                   MF_CHECKED); 
                                    ToolBar_PressButton(hwndTB, IDM_PAGE, TRUE);
                                    ToolBar_PressButton(hwndTB, IDM_LIST, FALSE);
                                    ToolBar_SetState(hwndTB, IDM_LIST, TBSTATE_ENABLED);
                                    PageView = 1;
                                    bestpListe.SwitchPage0 (bestpListe.GetAktRow ());
                            }
                            SendMessage (bestpListe.Getmamain2 (), WM_SIZE, NULL, NULL);
                            InvalidateRect (bestpListe.Getmamain3 (), 0, TRUE);
                   }
 				   else if (LOWORD (wParam) == IDM_PAGE)
                   {
                            PageView = 1; 
			                CheckMenuItem (hMenu, IDM_PAGEVIEW,
								                   MF_CHECKED); 
                            ToolBar_PressButton(hwndTB, IDM_PAGE, TRUE);
                            ToolBar_PressButton(hwndTB, IDM_LIST, FALSE);
                            bestpListe.SwitchPage0 (bestpListe.GetAktRow ());
                            SendMessage (bestpListe.Getmamain2 (), WM_SIZE, NULL, NULL);
                            InvalidateRect (bestpListe.Getmamain3 (), 0, TRUE);
                   }
/*
 				   else if (LOWORD (wParam) == IDM_STD)
                   {
                             bestpListe.StdBest ();
                             bestpListe.SetListFocus ();
                   }
*/
 				   else if (LOWORD (wParam) == IDM_LIST)
                   {
                            PageView = 0; 
			                CheckMenuItem (hMenu, IDM_PAGEVIEW,
								                   MF_UNCHECKED); 
                            ToolBar_PressButton(hwndTB, IDM_LIST, TRUE);
                            ToolBar_PressButton(hwndTB, IDM_PAGE, FALSE);
                            bestpListe.SwitchPage0 (bestpListe.GetAktRowS ());
                            SendMessage (bestpListe.Getmamain2 (), WM_SIZE, NULL, NULL);
                            InvalidateRect (bestpListe.Getmamain3 (), 0, TRUE);
                   }
			       else if (LOWORD (wParam) == IDM_WORK)
                   {
					         mench = 0;
							 EnableMe ();
                   }
			       else if (LOWORD (wParam) == IDM_SHOW)
                   {
					         mench = 1;
							 EnableMe ();
                 }
			       else if (LOWORD (wParam) == IDM_DELETE)
                   {
					         mench = 2;
							 EnableMe ();
                   }
			       else if (LOWORD (wParam) == IDM_PRINT)
                   {
					         PrintAng ();
							 SetCurrentFocus (currentfield);
                   }
			       else if (LOWORD (wParam) == IDM_PRCHOISE)
                   {
					         ShowPrinters ();
							 SetCurrentFocus (currentfield);
                   }

                   if (HIWORD (wParam) == CBN_CLOSEUP)
                   {
                                if (lParam == (LPARAM) hwndCombo1)
                                {
                                   ChoiseCombo1 ();
                                }
                                if (lParam == (LPARAM) hwndCombo2)
                                {
                                   ChoiseCombo2 ();
                                }

                                if (IsDlgCombobox ((HWND) lParam))
                                {
                                      return 0;
                                }

                                if (bestpListe.Getmamain3 ())
                                {
                                      bestpListe.SetListFocus ();
                                }
                                else
                                {
                                    SetCurrentFocus (currentfield);
                                }
                    }
					else if (LOWORD (wParam) == IDM_FONT)
                    {
                                  bestpListe.ChoiseFont (&lFont);
                                  lstxtlist.SetListFont (&lFont);
                                  return 0;
                                   
                    }
					else if (LOWORD (wParam) == IDM_BASIS)
                    {
                                  bestpListe.ShowBasis ();
                                  return 0;
                                   
                    }
					else if (LOWORD (wParam) == IDM_POSTEXT)
                    {
                                  bestpListe.Texte ();
                                  return 0;
                                   
                    }
					else if (LOWORD (wParam) == IDM_ANZBEST)
                    {
//                                  SetAnzBest ();
                                  return 0;
                                   
                    }
					else if (LOWORD (wParam) == IDM_LGRDEF)
					{
                                  if (ProgCfg.GetGroupDefault ("lager", cfg_v) == TRUE)
								  {
                                             akt_lager = atol (cfg_v);
											 bestpListe.SetLager (akt_lager);
								  }
					}
					else if (LOWORD (wParam) == IDM_LGR)
					{
								  bestpListe.SetLager (akt_lager);
					}
                    else if (LOWORD (wParam) == IDM_INFO)
                    {
                                  syskey = KEY4;
                                  SendKey (VK_F4);
                    }
                    else if (LOWORD (wParam) == IDM_TEXT)
                    {
                    }
                    else if (LOWORD (wParam) == IDM_ETI)
                    {
                    }
                    else if (LOWORD (wParam) == CallT1)
                    {
                            CallTele1 ();
                            break;
                    }
                    else if (LOWORD (wParam) == CallT2)
                    {
                            CallTele2 ();
                            break;
                    }
                    else if (LOWORD (wParam) == CallT3)
                    {
                            CallTele3 ();
                            break;
                    }
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

LONG FAR PASCAL StaticWProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

void FreeBestk (void)
/**
Beim Verlassen des Programms wird versucht alle Saetze in best_kopf mit
delstatus -1 auf delstatus 0 zu setzen.
Bei Saetze, die von einem anderen Benutzer bearbeitet werden bleibt der 
delstatus auf -1;
**/
{
	    extern short sql_mode; 
		short sql_s;
		int cursor;
		int upd_cursor;

		sql_s = sql_mode;
		sql_mode = 1;

		DbClass.sqlout ((short *) &best_kopf.mdn, 1, 0);
		DbClass.sqlout ((short *) &best_kopf.fil, 1, 0);
		DbClass.sqlout ((long *)  &best_kopf.best_blg, 2, 0);
		cursor = DbClass.sqlcursor ("select mdn,fil,best_blg from best_kopf "
			                        "where delstatus = -1");
		if (cursor < 0) return;

		DbClass.sqlin ((short *) &best_kopf.mdn, 1, 0);
		DbClass.sqlin ((short *) &best_kopf.fil, 1, 0);
		DbClass.sqlin ((long *)  &best_kopf.best_blg, 2, 0);
		upd_cursor = DbClass.sqlcursor ("update best_kopf set delstatus = 0"
			                        "where mdn = ? "
									"and fil   = ? "
									"and best_blg = ?");
		if (upd_cursor < 0) return;

		beginwork ();
        while (DbClass.sqlfetch (cursor) == 0)
		{
			DbClass.sqlexecute (upd_cursor);
		}
		commitwork ();
		DbClass.sqlclose (upd_cursor);
		DbClass.sqlclose (cursor);
		sql_mode = sql_s;
}

static MTXT *Mtxt;
static long TextNr;

void GetKopfTexte (void)
/**
Texte aus EditFeld holen.
**/
{
	   char *Text;
	   char *txt;
	   static char seps [] = "\n";


	   Text = Mtxt->GetText ();
	   txt = strtok (Text, seps);
	   while (txt)
	   {
		   disp_mess (txt, 2);
	       txt = strtok (NULL, seps);
	   }
}

static int ReadTxt (long txtnr)
{
       LS_TXT_CLASS ls_txt_class;
	   int dsqlstatus;
	   
	   Mtxt->InitText ();
	   ls_txt.nr = txtnr;
       dsqlstatus = ls_txt_class.dbreadfirst ();
	   while (dsqlstatus == 0)
	   {
		    clipped (ls_txt.txt);
			Mtxt->AddText (ls_txt.txt);
			dsqlstatus = ls_txt_class.dbread ();
	   }
	   Mtxt->SetText ();
	   return 0;
}

static int CutLines (char *txt, int zei, LS_TXT_CLASS *ls_txt_class)
{
	   char *params [5];
	   char zeile[5];
	   char tlen[5];

	   if (strlen (txt) <= TLEN)
	   {
		   ls_txt.zei = zei;
		   cr_weg (txt);
		   strcpy (ls_txt.txt, txt);
           ls_txt_class->dbupdate ();
		   return (zei + 1);
	   }

	   params[0] = txt;
	   params[1] = ls_txt.txt;
	   sprintf (zeile, "1");
	   sprintf (tlen, "%d", TLEN);
	   params[2] = tlen;
	   params[3] = zeile;
	   params[4] = NULL;
	   while (getline (params))
	   {
		   ls_txt.zei = zei;
		   cr_weg (ls_txt.txt);
           ls_txt_class->dbupdate ();
		   zei ++;
		   sprintf (zeile, "%d", atoi (zeile) + 1);
	   }
	   return zei;
}


static int WriteTxt (long txtnr)
{
       LS_TXT_CLASS ls_txt_class;
	   int zei;
	   char *p;
	   char txt [512];
	   
	   zei = 1;
	   ls_txt.nr = txtnr;
	   ls_txt_class.dbreadfirst ();
       ls_txt_class.delete_aufpposi ();
	   p = Mtxt->FirstRow (txt, 511);
       while (p)
	   {
		   clipped (txt);
		   zei = CutLines (txt, zei, &ls_txt_class);
	       p = Mtxt->NextRow (txt, 511);
		   zei ++;
	   }
	   return 0;
}

static void InputTxt (char *Label, long text_nr)
{
  	  int cx, cy;
	  form *scurrent;
	  char textnr [10];

	  scurrent = current_form;
	  idx = -1;
      cx = 80;
      cy = 20;
      Mtxt = new MTXT (cx, cy, Label, "Text-Nr ", "", ReadTxt);
	  Mtxt->SetTextNr (LongToChar (textnr, "%ld", text_nr));
      Mtxt->OpenWindow (hMainInst, hMainWindow);
	  EnableWindow (hMainWindow, FALSE);
      ReadTxt (text_nr); 

	  if (Mtxt->ProcessMessages ())
	  {
		  WriteTxt (atol (Mtxt->GetTextNr ()));
	  }

	  SetActiveWindow (hMainWindow);
	  EnableWindow (hMainWindow, TRUE);
      Mtxt->DestroyWindow ();
	  delete Mtxt;
	  current_form = scurrent;
}


