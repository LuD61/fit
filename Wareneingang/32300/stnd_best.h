#include "dbclass.h"
#ifndef _STND_BEST_DEF
#define _STND_BEST_DEF

struct BEST_STND {
   double    a;
   short     delstatus;
   short     fil;
   char      lief[17];
   short     mdn;
   double    me;
   long      zuord_dat;
};
extern struct BEST_STND best_stnd, best_stnd_null;

#line 6 "stnd_best.rh"


class SBEST_CLASS : public DB_CLASS
{
          private :
               int cursora;
               void prepare (void);
               void preparea (void);
          public :
              SBEST_CLASS () : DB_CLASS (), cursora (-1)
              {
              }
              int dbreadfirst (void);
              int dbreadfirsta (void);
              int dbreada (void);
              int dbclosea (void);
};
#endif