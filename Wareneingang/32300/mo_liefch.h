#ifndef _MO_LIEFAC_DEF
#define _MO_LIEFAC_DEF
#include "lief_bzg.h"
#include "dbclass.h"
#include "mo_chq.h"
#include "image.h"

struct LIEF_A
{
         char mdn[5];
         char fil[5];
	     char lief [17];
	     char adr_krz [17];
	     char lief_best [17];
	     char pr_ek [11];
	     char pr_ek_sa [11];
	     int  sortidx;
};

class LIEFAC
{
           private :
			    int SortMode;
                static struct LIEF_A liefa;
                static struct LIEF_A *liefatab;
				static long liefaanz;
                static  CHQ *Choise1;
                static DB_CLASS DbClass;
                static HANDLE KondPid;
                static double art;
		        HBITMAP Sel;
		        HBITMAP Msk;
				char lief_best [17];
           public :
               LIEFAC ()
               {
                      liefatab = NULL; 
					  SortMode = 0;
               }
               ~LIEFAC ()
               {
                      if (liefatab)
                      {
                          delete liefatab;
                          liefatab = NULL;  
                      }
               }
			   char *getLief_best ()
			   {
				   return this->lief_best;
			   }
			   void setLief_best (char *lief_best)
			   {
				   strcpy (this->lief_best, lief_best);
			   }
			   static long Getliefaanz (void);
               static struct LIEF_A *GetLiefaTab (void);
               static struct LIEF_A *GetLiefa (int);
               static int AllArt (int);
               static int ArtOK (int);
               static int LiefaOK (int);
               static int PreisInfo (int);
               static BOOL TestProcess (HANDLE);
               static void TestKondPid (void);
               int Show (HWND, short, short, double, char *);
};
#endif

