#ifndef _LIEFBZGWORK_DEF
#define _LIEFBZGWORK_DEF
#include "lief_bzg.h"
#include "liefmebest.h"
#include "a_best.h"
#include "lief.h"
#include "ptab.h"
#include "mdn.h"

class LiefBzgWork
{
    private :
        LIEF_BZG_CLASS LiefBzg;
        LIEFMEBEST_CLASS LiefMeBest;
        PTAB_CLASS Ptab;
        LIEF_CLASS Lief;
        MDN_CLASS Mdn;
        int liefbzg_cursor;
        char default_lief_kz [4];
        char old_me_kz [5];
    public :
        LiefBzgWork () : liefbzg_cursor (-1)
        {
            strcpy (default_lief_kz, "N");
        }

        void SetDefaultLiefKz (char *lief_kz)
        {
            strcpy (default_lief_kz, lief_kz);
        }

        void SetOldMeKz (char *old_me_kz)
        {
            strcpy (this->old_me_kz, old_me_kz);
        }

        char *GetOldMeKz (void)
        {
            return old_me_kz;
        }

        void BeginWork (void);
        void CommitWork (void);
        void RollbackWork (void);
        void InitRec (void);
        int ReadLief (short, short, char *);
        int ReadLief (void);
        int DeleteLief (void);
        int ReadA (short, short, char *, double);
        int ReadA (void);
        BOOL LiefmebestExist (void);
        void WriteLiefmebest (char *);
        int WriteA (void);
        int DeleteA (void);
        int GetPtab (char *, char *, char *);
        int ShowPtab (char *dest, char *item);
        int GetMdnName (char *, short);
        int GetFilName (char *, short, short);
        int GetLiefName (char *, short, char *);
        int GetLiefDefaults (void);
        void FillRow (char *);
};

#endif