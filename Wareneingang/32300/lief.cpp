#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "message.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
// #include "a_kun.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "lief.h"

struct LIEF lief, lief_null;

extern HWND hMainWindow;
extern HWND mamain1;
static MELDUNG Mess;

void LIEF_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &lief.mdn, 1, 0);
            ins_quest ((char *) lief.lief, 0, 17);
    out_quest ((char *) lief.abr,0,2);
    out_quest ((char *) &lief.adr,2,0);
    out_quest ((char *) lief.bank_nam,0,37);
    out_quest ((char *) &lief.bbn,2,0);
    out_quest ((char *) &lief.best_sort,1,0);
    out_quest ((char *) lief.best_trans_kz,0,2);
    out_quest ((char *) &lief.blz,2,0);
    out_quest ((char *) lief.bonus_kz,0,2);
    out_quest ((char *) &lief.fil,1,0);
    out_quest ((char *) lief.fracht_kz,0,2);
    out_quest ((char *) lief.ink,0,17);
    out_quest ((char *) &lief.kreditor,2,0);
    out_quest ((char *) lief.kto,0,17);
    out_quest ((char *) &lief.letzt_lief,2,0);
    out_quest ((char *) lief.lief,0,17);
    out_quest ((char *) lief.lief_kun,0,17);
    out_quest ((char *) lief.lief_rht,0,2);
    out_quest ((char *) &lief.lief_s,2,0);
    out_quest ((char *) &lief.lief_typ,1,0);
    out_quest ((char *) &lief.lief_zeit,1,0);
    out_quest ((char *) &lief.lief_zusch,3,0);
    out_quest ((char *) &lief.lst_nr,2,0);
    out_quest ((char *) &lief.mahn_stu,1,0);
    out_quest ((char *) &lief.mdn,1,0);
    out_quest ((char *) lief.me_kz,0,2);
    out_quest ((char *) lief.min_me_kz,0,2);
    out_quest ((char *) &lief.min_zusch,3,0);
    out_quest ((char *) &lief.min_zusch_proz,3,0);
    out_quest ((char *) lief.nach_lief,0,2);
    out_quest ((char *) lief.ordr_kz,0,2);
    out_quest ((char *) lief.rab_abl,0,2);
    out_quest ((char *) lief.rab_kz,0,2);
    out_quest ((char *) lief.rech_kz,0,2);
    out_quest ((char *) &lief.rech_toler,3,0);
    out_quest ((char *) &lief.sdr_nr,1,0);
    out_quest ((char *) &lief.son_kond,2,0);
    out_quest ((char *) &lief.sprache,1,0);
    out_quest ((char *) lief.steuer_kz,0,2);
    out_quest ((char *) lief.unt_lief_kz,0,2);
    out_quest ((char *) lief.vieh_bas,0,2);
    out_quest ((char *) lief.we_erf_kz,0,2);
    out_quest ((char *) lief.we_kontr,0,2);
    out_quest ((char *) lief.we_pr_kz,0,2);
    out_quest ((char *) &lief.we_toler,3,0);
    out_quest ((char *) &lief.zahl_kond,1,0);
    out_quest ((char *) lief.zahlw,0,2);
    out_quest ((char *) &lief.hdklpargr,1,0);
    out_quest ((char *) &lief.vorkgr,2,0);
    out_quest ((char *) &lief.delstatus,1,0);
    out_quest ((char *) &lief.mwst,1,0);
    out_quest ((char *) &lief.liefart,1,0);
    out_quest ((char *) lief.modif,0,2);
    out_quest ((char *) lief.ust_id,0,17);
    out_quest ((char *) lief.eg_betriebsnr,0,21);
    out_quest ((char *) &lief.eg_kz,1,0);
    out_quest ((char *) &lief.waehrung,1,0);
    out_quest ((char *) lief.iln,0,17);
    out_quest ((char *) &lief.zertifikat1,1,0);
    out_quest ((char *) &lief.zertifikat2,1,0);
            cursor = prepare_sql ("select lief.abr,  lief.adr,  "
"lief.bank_nam,  lief.bbn,  lief.best_sort,  lief.best_trans_kz,  "
"lief.blz,  lief.bonus_kz,  lief.fil,  lief.fracht_kz,  lief.ink,  "
"lief.kreditor,  lief.kto,  lief.letzt_lief,  lief.lief,  lief.lief_kun,  "
"lief.lief_rht,  lief.lief_s,  lief.lief_typ,  lief.lief_zeit,  "
"lief.lief_zusch,  lief.lst_nr,  lief.mahn_stu,  lief.mdn,  lief.me_kz,  "
"lief.min_me_kz,  lief.min_zusch,  lief.min_zusch_proz,  "
"lief.nach_lief,  lief.ordr_kz,  lief.rab_abl,  lief.rab_kz,  "
"lief.rech_kz,  lief.rech_toler,  lief.sdr_nr,  lief.son_kond,  "
"lief.sprache,  lief.steuer_kz,  lief.unt_lief_kz,  lief.vieh_bas,  "
"lief.we_erf_kz,  lief.we_kontr,  lief.we_pr_kz,  lief.we_toler,  "
"lief.zahl_kond,  lief.zahlw,  lief.hdklpargr,  lief.vorkgr,  "
"lief.delstatus,  lief.mwst,  lief.liefart,  lief.modif,  lief.ust_id,  "
"lief.eg_betriebsnr,  lief.eg_kz,  lief.waehrung,  lief.iln,  "
"lief.zertifikat1,  lief.zertifikat2 from lief "

#line 32 "lief.rpp"
                                  "where mdn = ? "
                                  "and   lief = ?");

    ins_quest ((char *) lief.abr,0,2);
    ins_quest ((char *) &lief.adr,2,0);
    ins_quest ((char *) lief.bank_nam,0,37);
    ins_quest ((char *) &lief.bbn,2,0);
    ins_quest ((char *) &lief.best_sort,1,0);
    ins_quest ((char *) lief.best_trans_kz,0,2);
    ins_quest ((char *) &lief.blz,2,0);
    ins_quest ((char *) lief.bonus_kz,0,2);
    ins_quest ((char *) &lief.fil,1,0);
    ins_quest ((char *) lief.fracht_kz,0,2);
    ins_quest ((char *) lief.ink,0,17);
    ins_quest ((char *) &lief.kreditor,2,0);
    ins_quest ((char *) lief.kto,0,17);
    ins_quest ((char *) &lief.letzt_lief,2,0);
    ins_quest ((char *) lief.lief,0,17);
    ins_quest ((char *) lief.lief_kun,0,17);
    ins_quest ((char *) lief.lief_rht,0,2);
    ins_quest ((char *) &lief.lief_s,2,0);
    ins_quest ((char *) &lief.lief_typ,1,0);
    ins_quest ((char *) &lief.lief_zeit,1,0);
    ins_quest ((char *) &lief.lief_zusch,3,0);
    ins_quest ((char *) &lief.lst_nr,2,0);
    ins_quest ((char *) &lief.mahn_stu,1,0);
    ins_quest ((char *) &lief.mdn,1,0);
    ins_quest ((char *) lief.me_kz,0,2);
    ins_quest ((char *) lief.min_me_kz,0,2);
    ins_quest ((char *) &lief.min_zusch,3,0);
    ins_quest ((char *) &lief.min_zusch_proz,3,0);
    ins_quest ((char *) lief.nach_lief,0,2);
    ins_quest ((char *) lief.ordr_kz,0,2);
    ins_quest ((char *) lief.rab_abl,0,2);
    ins_quest ((char *) lief.rab_kz,0,2);
    ins_quest ((char *) lief.rech_kz,0,2);
    ins_quest ((char *) &lief.rech_toler,3,0);
    ins_quest ((char *) &lief.sdr_nr,1,0);
    ins_quest ((char *) &lief.son_kond,2,0);
    ins_quest ((char *) &lief.sprache,1,0);
    ins_quest ((char *) lief.steuer_kz,0,2);
    ins_quest ((char *) lief.unt_lief_kz,0,2);
    ins_quest ((char *) lief.vieh_bas,0,2);
    ins_quest ((char *) lief.we_erf_kz,0,2);
    ins_quest ((char *) lief.we_kontr,0,2);
    ins_quest ((char *) lief.we_pr_kz,0,2);
    ins_quest ((char *) &lief.we_toler,3,0);
    ins_quest ((char *) &lief.zahl_kond,1,0);
    ins_quest ((char *) lief.zahlw,0,2);
    ins_quest ((char *) &lief.hdklpargr,1,0);
    ins_quest ((char *) &lief.vorkgr,2,0);
    ins_quest ((char *) &lief.delstatus,1,0);
    ins_quest ((char *) &lief.mwst,1,0);
    ins_quest ((char *) &lief.liefart,1,0);
    ins_quest ((char *) lief.modif,0,2);
    ins_quest ((char *) lief.ust_id,0,17);
    ins_quest ((char *) lief.eg_betriebsnr,0,21);
    ins_quest ((char *) &lief.eg_kz,1,0);
    ins_quest ((char *) &lief.waehrung,1,0);
    ins_quest ((char *) lief.iln,0,17);
    ins_quest ((char *) &lief.zertifikat1,1,0);
    ins_quest ((char *) &lief.zertifikat2,1,0);
            sqltext = "update lief set lief.abr = ?,  "
"lief.adr = ?,  lief.bank_nam = ?,  lief.bbn = ?,  lief.best_sort = ?,  "
"lief.best_trans_kz = ?,  lief.blz = ?,  lief.bonus_kz = ?,  "
"lief.fil = ?,  lief.fracht_kz = ?,  lief.ink = ?,  lief.kreditor = ?,  "
"lief.kto = ?,  lief.letzt_lief = ?,  lief.lief = ?,  "
"lief.lief_kun = ?,  lief.lief_rht = ?,  lief.lief_s = ?,  "
"lief.lief_typ = ?,  lief.lief_zeit = ?,  lief.lief_zusch = ?,  "
"lief.lst_nr = ?,  lief.mahn_stu = ?,  lief.mdn = ?,  lief.me_kz = ?,  "
"lief.min_me_kz = ?,  lief.min_zusch = ?,  lief.min_zusch_proz = ?,  "
"lief.nach_lief = ?,  lief.ordr_kz = ?,  lief.rab_abl = ?,  "
"lief.rab_kz = ?,  lief.rech_kz = ?,  lief.rech_toler = ?,  "
"lief.sdr_nr = ?,  lief.son_kond = ?,  lief.sprache = ?,  "
"lief.steuer_kz = ?,  lief.unt_lief_kz = ?,  lief.vieh_bas = ?,  "
"lief.we_erf_kz = ?,  lief.we_kontr = ?,  lief.we_pr_kz = ?,  "
"lief.we_toler = ?,  lief.zahl_kond = ?,  lief.zahlw = ?,  "
"lief.hdklpargr = ?,  lief.vorkgr = ?,  lief.delstatus = ?,  "
"lief.mwst = ?,  lief.liefart = ?,  lief.modif = ?,  lief.ust_id = ?,  "
"lief.eg_betriebsnr = ?,  lief.eg_kz = ?,  lief.waehrung = ?,  "
"lief.iln = ?,  lief.zertifikat1 = ?,  lief.zertifikat2 = ? "

#line 36 "lief.rpp"
                                  "where mdn = ? "
                                  "and   lief = ?";


            ins_quest ((char *) &lief.mdn, 1, 0);
            ins_quest ((char *) lief.lief, 0, 17);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &lief.mdn, 1, 0);
            ins_quest ((char *) lief.lief, 0, 17);
            test_upd_cursor = prepare_sql ("select lief from lief "
                                  "where mdn = ? "
                                  "and   lief = ?");
            ins_quest ((char *) &lief.mdn, 1, 0);
            ins_quest ((char *) lief.lief, 0, 17);
            del_cursor = prepare_sql ("delete from lief "
                                  "where mdn = ? "
                                  "and   lief = ?");
    ins_quest ((char *) lief.abr,0,2);
    ins_quest ((char *) &lief.adr,2,0);
    ins_quest ((char *) lief.bank_nam,0,37);
    ins_quest ((char *) &lief.bbn,2,0);
    ins_quest ((char *) &lief.best_sort,1,0);
    ins_quest ((char *) lief.best_trans_kz,0,2);
    ins_quest ((char *) &lief.blz,2,0);
    ins_quest ((char *) lief.bonus_kz,0,2);
    ins_quest ((char *) &lief.fil,1,0);
    ins_quest ((char *) lief.fracht_kz,0,2);
    ins_quest ((char *) lief.ink,0,17);
    ins_quest ((char *) &lief.kreditor,2,0);
    ins_quest ((char *) lief.kto,0,17);
    ins_quest ((char *) &lief.letzt_lief,2,0);
    ins_quest ((char *) lief.lief,0,17);
    ins_quest ((char *) lief.lief_kun,0,17);
    ins_quest ((char *) lief.lief_rht,0,2);
    ins_quest ((char *) &lief.lief_s,2,0);
    ins_quest ((char *) &lief.lief_typ,1,0);
    ins_quest ((char *) &lief.lief_zeit,1,0);
    ins_quest ((char *) &lief.lief_zusch,3,0);
    ins_quest ((char *) &lief.lst_nr,2,0);
    ins_quest ((char *) &lief.mahn_stu,1,0);
    ins_quest ((char *) &lief.mdn,1,0);
    ins_quest ((char *) lief.me_kz,0,2);
    ins_quest ((char *) lief.min_me_kz,0,2);
    ins_quest ((char *) &lief.min_zusch,3,0);
    ins_quest ((char *) &lief.min_zusch_proz,3,0);
    ins_quest ((char *) lief.nach_lief,0,2);
    ins_quest ((char *) lief.ordr_kz,0,2);
    ins_quest ((char *) lief.rab_abl,0,2);
    ins_quest ((char *) lief.rab_kz,0,2);
    ins_quest ((char *) lief.rech_kz,0,2);
    ins_quest ((char *) &lief.rech_toler,3,0);
    ins_quest ((char *) &lief.sdr_nr,1,0);
    ins_quest ((char *) &lief.son_kond,2,0);
    ins_quest ((char *) &lief.sprache,1,0);
    ins_quest ((char *) lief.steuer_kz,0,2);
    ins_quest ((char *) lief.unt_lief_kz,0,2);
    ins_quest ((char *) lief.vieh_bas,0,2);
    ins_quest ((char *) lief.we_erf_kz,0,2);
    ins_quest ((char *) lief.we_kontr,0,2);
    ins_quest ((char *) lief.we_pr_kz,0,2);
    ins_quest ((char *) &lief.we_toler,3,0);
    ins_quest ((char *) &lief.zahl_kond,1,0);
    ins_quest ((char *) lief.zahlw,0,2);
    ins_quest ((char *) &lief.hdklpargr,1,0);
    ins_quest ((char *) &lief.vorkgr,2,0);
    ins_quest ((char *) &lief.delstatus,1,0);
    ins_quest ((char *) &lief.mwst,1,0);
    ins_quest ((char *) &lief.liefart,1,0);
    ins_quest ((char *) lief.modif,0,2);
    ins_quest ((char *) lief.ust_id,0,17);
    ins_quest ((char *) lief.eg_betriebsnr,0,21);
    ins_quest ((char *) &lief.eg_kz,1,0);
    ins_quest ((char *) &lief.waehrung,1,0);
    ins_quest ((char *) lief.iln,0,17);
    ins_quest ((char *) &lief.zertifikat1,1,0);
    ins_quest ((char *) &lief.zertifikat2,1,0);
            ins_cursor = prepare_sql ("insert into lief (abr,  "
"adr,  bank_nam,  bbn,  best_sort,  best_trans_kz,  blz,  bonus_kz,  fil,  fracht_kz,  "
"ink,  kreditor,  kto,  letzt_lief,  lief,  lief_kun,  lief_rht,  lief_s,  lief_typ,  "
"lief_zeit,  lief_zusch,  lst_nr,  mahn_stu,  mdn,  me_kz,  min_me_kz,  min_zusch,  "
"min_zusch_proz,  nach_lief,  ordr_kz,  rab_abl,  rab_kz,  rech_kz,  rech_toler,  "
"sdr_nr,  son_kond,  sprache,  steuer_kz,  unt_lief_kz,  vieh_bas,  we_erf_kz,  "
"we_kontr,  we_pr_kz,  we_toler,  zahl_kond,  zahlw,  hdklpargr,  vorkgr,  "
"delstatus,  mwst,  liefart,  modif,  ust_id,  eg_betriebsnr,  eg_kz,  waehrung,  iln,  "
"zertifikat1,  zertifikat2) "

#line 55 "lief.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"); 

#line 57 "lief.rpp"
}
int LIEF_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}


/* Auswahlfenster                                   */

static int searchmode = 0;
static int searchfield = 0;
static char CharBuff [20] = {"\0"};
static BOOL CharBuffSet = FALSE;
static BOOL CharMess = TRUE;

static BOOL DisplayList = TRUE;




/* Variablen fuer Auswahl mit Buttonueberschrift    */

#include "itemc.h"
#define MAXSORT 20000
static long MaxSort;

int (*LiefTestProc) (char *) = NULL;

static char *sqltext;

static int dosort1 ();
static int dosort2 ();
static int dosort3 ();
static int dosort4 ();
static int dosort5 ();
static int dosort6 ();

struct SORT_AW
{
         char sort1 [40];
		 char sort2 [40];
		 char sort3 [40];
		 char sort4 [11];
		 char sort5 [40];
		 char sort6 [40];
		 int  sortidx;
};

static int sortl1 = 18;
static int sortl2 = 39;
static int sortl3 = 39;
static int sortl4 = 10;
static int sortl5 = 39;
static int sortl6 = 18;



//static struct SORT_AW sort_aw, sort_awtab[MAXSORT];
//static int sortidxtab[MAXSORT];
static struct SORT_AW sort_aw, *sort_awtab;
static int *sortidxtab;


static void SortMalloc (void) 
/**
Speicher fuer Listbereich zuordnen.
**/
{
    DB_CLASS DbClass;
	static BOOL SortOK = FALSE;
    long anz;

//	return;
	if (SortOK) return;


	DbClass.sqlout ((long *) &anz, 2, 0);
	DbClass.sqlcomm ("select count (*) from lief");

	if (anz == 0l) return;

    sort_awtab = (struct SORT_AW *) GlobalAlloc (GMEM_FIXED, 
		                                   ((anz + 1) * sizeof (struct SORT_AW))); 
	while (sort_awtab == NULL)
	{
		if (anz == 0l) break;
		anz --;
        sort_awtab = (struct SORT_AW *) GlobalAlloc (GMEM_FIXED, 
		                                   (anz * sizeof (struct SORT_AW))); 
	}
	MaxSort = anz + 1;
	if (anz == 0l)
	{
		print_mess (2, "Es konnte kein Speicher fuer die Lieferantenauswahl zugewiesen werden");
		return;
	}
    sortidxtab = (int *) GlobalAlloc (GMEM_FIXED, 
		                                   ((anz + 1) * sizeof (int))); 
	if (sortidxtab == NULL)
	{
		print_mess (2, "Es konnte kein Speicher fuer die Lieferantenauswahl zugewiesen werden");
		GlobalFree (sort_awtab);
  	    MaxSort = 0;
		return;
	}
	SortOK = TRUE;

}


static int sort_awanz;
static int sort1 = 1;
static int sort2 = 1;
static int sort3 = 1;
static int sort4 = 1;
static int sort5 = 1;
static int sort6 = 1;
static int sort_idx = 0;

static ITEM isort1    ("", sort_aw.sort1,    "", 0);
static ITEM isort2    ("", sort_aw.sort2, "", 0);
static ITEM isort3    ("", sort_aw.sort3, "", 0);
static ITEM isort4    ("", sort_aw.sort4, "", 0);
static ITEM isort5    ("", sort_aw.sort5, "", 0);
static ITEM isort6    ("", sort_aw.sort6, "", 0);

static field _fsort_aw[] = {
&isort1,     16, 1, 0, 0, 0,  "",    NORMAL, 0, 0, 0,
&isort2,     39, 1, 0, 19, 0, "",    NORMAL, 0, 0, 0,
&isort4,      9, 1, 0, 57, 0, "",    NORMAL, 0, 0, 0,
&isort3,     37, 1, 0, 67, 0, "",    NORMAL, 0, 0, 0,
&isort5,     37, 1, 0,105, 0, "",    NORMAL, 0, 0, 0,
&isort6,     17, 1, 0,143, 0, "",    NORMAL, 0, 0, 0,
};

static form fsort_aw = {6, 0, 0, _fsort_aw, 0, 0, 0, 0, NULL};

static ITEM isort1ub    ("", "Lieferanten-Nr","", 0);
static ITEM isort2ub    ("", "Name",     "", 0);
static ITEM isort3ub    ("", "Ort",      "", 0);
static ITEM isort4ub    ("", "PLZ",      "", 0);
static ITEM isort5ub    ("", "Strasse",  "", 0);
static ITEM isort6ub    ("", "Kurz-Name","", 0);
static ITEM ifillub     ("", " ",             "", 0); 

static field _fsort_awub[] = {
&isort1ub,       18, 1, 0,  0, 0, "", BUTTON, 0, dosort1,    102,
&isort2ub,       38, 1, 0, 18, 0, "", BUTTON, 0, dosort2,    103,
&isort4ub,       10, 1, 0, 56, 0, "", BUTTON, 0, dosort4,    105,
&isort3ub,       38, 1, 0, 66, 0, "", BUTTON, 0, dosort3,    104,
&isort5ub,       38, 1, 0,104, 0, "", BUTTON, 0, dosort5,    106,
&isort6ub,       18, 1, 0,142, 0, "", BUTTON, 0, dosort6,    107,
&ifillub,        80, 1, 0,160, 0, "", BUTTON, 0, 0, 0};

static form fsort_awub = {7, 0, 0, _fsort_awub, 0, 0, 0, 0, NULL};

static ITEM isortl ("", "1", "", 0);

static field _fsort_awl[] = {
&isortl,          1, 1, 0, 18, 0, "", NORMAL, 0, 0, 0,
&isortl,          1, 1, 0, 56, 0, "", NORMAL, 0, 0, 0,
&isortl,          1, 1, 0, 66, 0, "", NORMAL, 0, 0, 0,
&isortl,          1, 1, 0,104, 0, "", NORMAL, 0, 0, 0,
&isortl,          1, 1, 0,142, 0, "", NORMAL, 0, 0, 0,
&isortl,          1, 1, 0,160, 0, "", NORMAL, 0, 0, 0,
};

static form fsort_awl = {6, 0, 0, _fsort_awl, 0, 0, 0, 0, NULL};


static void SortInit (char *sort, int len)
/**
**/
{
	       memset (sort, ' ', len);
		   sort [len] = 0;
}
          

static void InitSort (int pos)
{
	       SortInit (sort_awtab [pos].sort1, sortl1);
	       SortInit (sort_awtab [pos].sort2, sortl2);
	       SortInit (sort_awtab [pos].sort3, sortl3);
	       SortInit (sort_awtab [pos].sort4, sortl4);
	       SortInit (sort_awtab [pos].sort5, sortl5);
	       SortInit (sort_awtab [pos].sort6, sortl6);
}

static int dosort10 (const void *elem1, const void *elem2)
{
	      struct SORT_AW *el1; 
	      struct SORT_AW *el2; 

		  el1 = (struct SORT_AW *) elem1;
		  el2 = (struct SORT_AW *) elem2;
	        return strcmp(el1->sort1, el2->sort1) * sort1;
}


int dosort1 ()
{
	if (searchmode == 1)
	{
		SetSearchField (0);
        SetCharBuffTxt (fsort_awub.mask[0].item->GetFeldPtr (), 1);
        if (CharMess) SetCharBuffMess (" ");
	}
	SetFocus (Getlbox());
	qsort (sort_awtab, sort_awanz, sizeof (struct SORT_AW),
				   dosort10);
	if (DisplayList)
	{
	          sort1 *= -1;
              ShowNewElist ((char *) sort_awtab,
                           sort_awanz,
                           (int) sizeof (struct SORT_AW));
	}
	return 0;
}


static int dosort20 (const void *elem1, const void *elem2)
{
	      struct SORT_AW *el1; 
	      struct SORT_AW *el2; 

		  el1 = (struct SORT_AW *) elem1;
		  el2 = (struct SORT_AW *) elem2;
	      return (strcmp (el1->sort2,el2->sort2) * sort2);
}


int dosort2 ()
{
	if (searchmode == 1)
	{
		SetSearchField (1);
        SetCharBuffTxt (fsort_awub.mask[1].item->GetFeldPtr (), 1);
        if (CharMess) SetCharBuffMess (" ");
	}
	SetFocus (Getlbox());
	qsort (sort_awtab, sort_awanz, sizeof (struct SORT_AW),
				   dosort20);
	if (DisplayList)
	{
         	    if (searchmode != 1)
				{
	                        sort2 *= -1;
				}
                ShowNewElist ((char *) sort_awtab,
                           sort_awanz,
                           (int) sizeof (struct SORT_AW));
	}
	return 0;
}

static int dosort30 (const void *elem1, const void *elem2)
{
	      struct SORT_AW *el1; 
	      struct SORT_AW *el2; 

		  el1 = (struct SORT_AW *) elem1;
		  el2 = (struct SORT_AW *) elem2;
	      return (strcmp (el1->sort3,el2->sort3) * sort3);
}


int dosort3 ()
{
	if (searchmode == 1)
	{
		SetSearchField (3);
        SetCharBuffTxt (fsort_awub.mask[3].item->GetFeldPtr (), 1);
        if (CharMess) SetCharBuffMess (" ");
	}
	SetFocus (Getlbox());
	qsort (sort_awtab, sort_awanz, sizeof (struct SORT_AW),
				   dosort30);
	if (DisplayList)
	{
 	            if (searchmode != 1)
				{
	                     sort3 *= -1;
				}
                ShowNewElist ((char *) sort_awtab,
                           sort_awanz,
                           (int) sizeof (struct SORT_AW));
	}
	return 0;
}

static int dosort40 (const void *elem1, const void *elem2)
{
	      struct SORT_AW *el1; 
	      struct SORT_AW *el2; 

		  el1 = (struct SORT_AW *) elem1;
		  el2 = (struct SORT_AW *) elem2;
	      return (strcmp (el1->sort4,el2->sort4) * sort4);
}


int dosort4 ()
{
	if (searchmode == 1)
	{
		SetSearchField (2);
        SetCharBuffTxt (fsort_awub.mask[2].item->GetFeldPtr (), 1);
        if (CharMess) SetCharBuffMess (" ");
	}
	SetFocus (Getlbox());
	qsort (sort_awtab, sort_awanz, sizeof (struct SORT_AW),
				   dosort40);
	if (DisplayList)
	{
         	        if (searchmode != 1)
					{
	                        sort4 *= -1;
					}
                    ShowNewElist ((char *) sort_awtab,
                           sort_awanz,
                           (int) sizeof (struct SORT_AW));
	}
	return 0;
}

static int dosort50 (const void *elem1, const void *elem2)
{
	      struct SORT_AW *el1; 
	      struct SORT_AW *el2; 

		  el1 = (struct SORT_AW *) elem1;
		  el2 = (struct SORT_AW *) elem2;

	      return (strcmp (el1->sort5,el2->sort5) * sort5);
}


int dosort5 ()
{
	if (searchmode == 1)
	{
		SetSearchField (4);
        SetCharBuffTxt (fsort_awub.mask[4].item->GetFeldPtr (), 1);
        if (CharMess) SetCharBuffMess (" ");
	}
	SetFocus (Getlbox());
	qsort (sort_awtab, sort_awanz, sizeof (struct SORT_AW),
				   dosort50);
	if (DisplayList)
	{
         	       if (searchmode != 1)
				   {
	                        sort5 *= -1;
				   }
                   ShowNewElist ((char *) sort_awtab,
                           sort_awanz,
                           (int) sizeof (struct SORT_AW));
	}
	return 0;
}

static int dosort60 (const void *elem1, const void *elem2)
{
	      struct SORT_AW *el1; 
	      struct SORT_AW *el2; 

		  el1 = (struct SORT_AW *) elem1;
		  el2 = (struct SORT_AW *) elem2;
		  if (strcmp (el1->sort6, el2->sort6) == 0)
		  {
	                      return (strcmp (el1->sort2,el2->sort2) * sort6);
		  }
	      return (strcmp (el1->sort6,el2->sort6) * sort6);
}


int dosort6 ()
{
	if (searchmode == 1)
	{
		SetSearchField (5);
        SetCharBuffTxt (fsort_awub.mask[5].item->GetFeldPtr (), 1);
        if (CharMess) SetCharBuffMess (" ");
	}
	SetFocus (Getlbox());
	qsort (sort_awtab, sort_awanz, sizeof (struct SORT_AW),
				   dosort60);
	if (DisplayList)
	{
         	    if (searchmode != 1)
				{
	                      sort6 *= -1;
				}
                ShowNewElist ((char *) sort_awtab,
                           sort_awanz,
                           (int) sizeof (struct SORT_AW));
	}
	return 0;
}

static void IsAwClck (int idx)
/**
Reaktion auf Doppelclick in der Liste.
**/
{
        sort_idx = idx;
        break_list ();
        return;
}

static int endsort (void)
/**
Liste beenden.
**/
{
        break_list ();
        return (1);
}

void LIEF_CLASS::SetTestLiefProc (int (*TkProc)(char *))
/**
TestKunProc setzen.
**/
{
	     LiefTestProc = TkProc;
}

void LIEF_CLASS::ListToMamain1 (HWND eWindow)
/**
Fenster an mamain1 anpassen.
**/
{
	    RECT rect;

		GetWindowRect (mamain1, &rect);

		rect.left ++;
		rect.top ++;
		rect.right = rect.right - rect.left - 1;
		rect.bottom = rect.bottom - rect.top - 1;
		MoveWindow (eWindow, rect.left, rect.top, rect.right, rect.bottom, TRUE);
}


int LIEF_CLASS::prep_awcursor (char *sqlstring)
/**
Cursor fuer Auswahl vorbereiten.
**/
{
         int cursor_ausw;

         cursor_ausw = prepare_scroll (sqlstring);
         if (sqlstatus)
         {
                     return (-1);
         }
         return (cursor_ausw);
}



int LIEF_CLASS::ShowBuQuery (HWND hWnd, int ws_flag) 
/**
Auswahl ueber Gruppen anzeigen.
**/
{
		HWND eWindow;
	    form *savecurrent;
		static struct LST lst;

		SortMalloc ();
	    savecurrent = current_form;
		SetCaption ("Auswahl �ber Lieferanten");
		if (searchmode == 1)
		{
                  SetCharBuffTxt (fsort_awub.mask[searchfield].item->GetFeldPtr (), 0);
                  SetBorder (WS_VISIBLE | WS_CAPTION |WS_THICKFRAME);
                  eWindow = OpenListWindowChoise (17, 73, 6, 2, 0);
		}
		else
		{
                  eWindow = OpenListWindowEnF (17, 73, 6, 2, 0);
		}
		ListToMamain1 (eWindow);
        if (qschange)
        {
 	        Mess.WaitWindow ("Die Lieferanten werden eingelesen ");
   		    sort_awanz = 0;
		    while (fetch_scroll (cursor_ausw, NEXT) == 0)
            {
				     InitSort (sort_awanz);
			         sprintf (sort_awtab[sort_awanz].sort1, "%s",
						      lief.lief);
					 strcpy (sort_awtab[sort_awanz].sort2, 
						     _adr.adr_nam1);
					 strcpy (sort_awtab[sort_awanz].sort3, 
						     _adr.ort1);
					 strcpy (sort_awtab[sort_awanz].sort4, 
						     _adr.plz);
					 strcpy (sort_awtab[sort_awanz].sort5, 
						     _adr.str);
					 strcpy (sort_awtab[sort_awanz].sort6, 
						     _adr.adr_krz);
					 sort_awtab[sort_awanz].sortidx = sort_awanz + 1;

					 memcpy (&sort_aw, &sort_awtab [sort_awanz],
                                       (int) sizeof (struct SORT_AW));
 //                    SetElistRow (&lst.menue, &fsort_aw, sort_awanz);

					 sort_awanz ++;
					 if (sort_awanz == MaxSort) 
					 {
						      print_mess (2, "Es konnten nicht alle Lieferanten eingelesen werden");       
						      break;
					 }
					 if (sort_awanz == MAXSORT) break;
            }
 	        Mess.CloseWaitWindow ();

 		    if (searchmode == 1)
			{
				   CharMess = FALSE;
				   DisplayList = FALSE;
			      (*fsort_awub.mask[searchfield].after) ();
				   DisplayList = TRUE;
				   CharMess = TRUE;
			}
        }
		else
		{
		    SetAktLst (&lst);
		}

		save_fkt (5); 
        set_fkt (endsort, 5);
        SetDblClck (IsAwClck, 1);
        SetAktivWindow (hWnd);
		SetSearchField (searchfield);
		SetSearchMode (searchmode);
        SetListEWindow (1);
		SetHLines (ws_flag);
		SetVLines (TRUE);
		SetMouseLock (TRUE);
        ElistVl (&fsort_awl);
        ElistUb (&fsort_awub);
		Setlistenter (1);


		if (qschange)
		{

/*
            ShowElistMen (&lst.menue, (char *) sort_awtab,
                    sort_awanz,
                   (char *) &sort_aw,
                   (int) sizeof (struct SORT_AW),
                   &fsort_aw);
*/


            ShowElist ((char *) sort_awtab,
                    sort_awanz,
                   (char *) &sort_aw,
                   (int) sizeof (struct SORT_AW),
                   &fsort_aw);
                   SetCharBuff (CharBuff);
				   CharBuffSet = FALSE;

		}
		else
		{
			 SetAktLst (&lst);

			 if (CharBuffSet)
			 {
                   SetCharBuff (CharBuff);
				   CharBuffSet = FALSE;
			 }

 		     if (searchmode == 1)
			 {
				   CharMess = FALSE;
			      (*fsort_awub.mask[searchfield].after) ();
				   CharMess = TRUE;
			 }
	

		}

        SetSaveList (&lst);

        EnterElist (eWindow, (char *) sort_awtab,
                             sort_awanz,
                             (char *) &sort_aw,
                             (int) sizeof (struct SORT_AW),
                             &fsort_aw);
        SetCharBuffTxt (NULL, 0);
	    current_form = savecurrent;
		restore_fkt (5); 
		SetMouseLock (FALSE);
        CloseUbControls (); 
        CloseEWindow (eWindow);
        SetListFont (FALSE);
        SetDblClck (NULL, 1);
        if (syskey == KEYESC || syskey == KEY5)
        {
                     close_sql (cursor_ausw);
                     return 0;
         }
//		 sort_idx = sort_awtab[sort_idx].sortidx - 1;
//         fetch_scroll (cursor_ausw, DBABSOLUTE, sort_idx);
		 strcpy (lief.lief, sort_awtab[sort_idx].sort1);
		 strcpy (_adr.adr_krz, sort_awtab[sort_idx].sort6);
//         close_sql (cursor_ausw);
         return 0;
}

void LIEF_CLASS::SetNew (void)
{
	     strcpy (qstring0, "Start");
}

int LIEF_CLASS::PrepareQueryMdn (form *qform, char *qnamen[])
/**
Query-Eingabe bearbeiten.
**/
{
         extern short sql_mode;
         short old_mode;
	     char sqlstring [1000]; 
         char *order; 

         ReadQuery (qform, qnamen); 
         if (strcmp (qstring0, qstring) == 0)
         {
                   qschange = 0;
         }
         else
         {
                   qschange = 1;
                   strcpy (qstring0, qstring);
         }

         close_sql (cursor_ausw);

         if (StatusQuery ())
         {
			       if (strstr (qstring, "adr_nam1"))
				   {
					   order = "order by adr.adr_nam1";
				   }
				   else if (searchmode != 0)
				   {
					   order = "order by adr.adr_nam1";
				   }
				   else
				   {
					   order = "order by lief.lief";

				   }
                   sprintf (sqlstring ,
                            "select lief.mdn, lief.fil, lief.lief, "
                            "adr.adr_nam1, adr.ort1, adr.plz, adr.str, adr.adr_krz "
							"from lief, adr "
                            "where lief  > \" \""
                             "and adr.adr = lief.adr "
                             "and %s and lief.delstatus = 0 %s",
							  qstring, order);
         }
         else
         {
                   strcpy (sqlstring ,
                            "select lief.mdn, lief.fil, lief.lief, "
                            "adr.adr_nam1, adr.ort1, adr.plz, adr.str, adr.adr_krz "
							"from lief, adr "
                            "where lief > 0 and adr.adr = lief.adr and lief.delstatus = 0 "
                            "order by adr.adr_nam1");
         }
         out_quest ((char *) &lief.mdn, 1, 0);
         out_quest ((char *) &lief.fil, 1, 0);
         out_quest ((char *) lief.lief, 0, 17);
         out_quest ((char *) _adr.adr_nam1, 0, 37);
         out_quest ((char *) _adr.ort1, 0, 37);
         out_quest ((char *) _adr.plz, 0, 9);
         out_quest ((char *) _adr.str, 0, 37);
         out_quest ((char *) _adr.adr_krz, 0, 17);
         old_mode = sql_mode;
         sql_mode = 2;
         cursor_ausw = prep_awcursor (sqlstring);
         sql_mode = old_mode;
         if (cursor_ausw == -1)
         {
                       return (-1);
         }
         return (0);
}

int LIEF_CLASS::PrepareQuery (form *qform, char *qnamen[])
/**
Query-Eingabe bearbeiten.
**/
{
         extern short sql_mode;
         short old_mode;
	     char sqlstring [1000]; 
         char *order; 

         ReadQuery (qform, qnamen); 
         if (strcmp (qstring0, qstring) == 0)
         {
                   qschange = 0;
         }
         else
         {
                   qschange = 1;
                   strcpy (qstring0, qstring);
         }

         close_sql (cursor_ausw);

         if (StatusQuery ())
         {
			       if (strstr (qstring, "adr_nam1"))
				   {
					   order = "order by adr.adr_nam1";
				   }
				   else if (searchmode != 0)
				   {
					   order = "order by adr.adr_nam1";
				   }
				   else
				   {
					   order = "order by lief.lief";

				   }
                   sprintf (sqlstring ,
                            "select lief.mdn, lief.fil, lief.lief, "
                            "adr.adr_nam1, adr.ort1, adr.plz, adr.str, adr.adr_krz "
							"from lief, adr "
                            "where lief  > \" \""
                             "and adr.adr = lief.adr "
                             "and %s and lief.delstatus = 0 %s",
							  qstring, order);
         }
         else
         {
                   strcpy (sqlstring ,
                            "select lief.mdn, lief.fil, lief.lief, "
                            "adr.adr_nam1, adr.ort1, adr.plz, adr.str, adr.adr_krz "
							"from lief, adr "
                            "where lief > \" \" and adr.adr = lief.adr and lief.delstatus = 0 "
                            "order by adr.adr_nam1");
         }

         out_quest ((char *) &lief.mdn, 1, 0);
         out_quest ((char *) &lief.fil, 1, 0);
         out_quest ((char *) lief.lief, 0,17);
         out_quest ((char *) _adr.adr_nam1, 0, 37);
         out_quest ((char *) _adr.ort1, 0, 37);
         out_quest ((char *) _adr.plz, 0, 9);
         out_quest ((char *) _adr.str, 0, 37);
         out_quest ((char *) _adr.adr_krz, 0, 17);
         old_mode = sql_mode;
         sql_mode = 2;
         cursor_ausw = prep_awcursor (sqlstring);
         sql_mode = old_mode;
         if (cursor_ausw == -1)
         {
                       return (-1);
         }
         return (0);
}

int LIEF_CLASS::PrepareQueryMdn (void)
/**
Query-Eingabe bearbeiten.
**/
{
         extern short sql_mode;
         short old_mode;
	     char sqlstring [1000]; 
		 static short mdn = -1;
		 static short fil = -1;

         qstring[0] = (char) 0; 
         if (strcmp (qstring0, qstring) == 0)
         {
                   qschange = 0;
         }
         else
         {
                   qschange = 1;
         }

		 if (qschange == 0 && lief.mdn != mdn)
		 {
			       qschange = 1;
		 }
		 if (qschange == 0 && lief.fil != fil)
		 {
			       qschange = 1;
		 }

		 mdn = lief.mdn;
		 fil = lief.fil;

         close_sql (cursor_ausw);
         strcpy (qstring0, qstring);

         strcpy (sqlstring ,
                   "select lief.mdn, lief.fil, lief.lief, "
                   "adr.adr_nam1, adr.ort1, adr.plz, adr.str, adr.adr_krz "
				   "from lief, adr "
                   "where  (lief.mdn = ? or lief.mdn = 0) and "
				   "lief > \" \" and adr.adr = lief.adr and lief.delstatus = 0"
                   "order by adr.adr_krz, adr.adr_nam1");

         ins_quest ((char *) &lief.mdn, 1, 0);
//         ins_quest ((char *) &lief.fil, 1, 0);

         out_quest ((char *) &lief.mdn, 1, 0);
         out_quest ((char *) &lief.fil, 1, 0);
         out_quest ((char *) lief.lief, 0, 17);
         out_quest ((char *) _adr.adr_nam1, 0, 37);
         out_quest ((char *) _adr.ort1, 0, 37);
         out_quest ((char *) _adr.plz, 0, 9);
         out_quest ((char *) _adr.str, 0, 37);
         out_quest ((char *) _adr.adr_krz, 0, 17);
         old_mode = sql_mode;
         sql_mode = 2;
         cursor_ausw = prep_awcursor (sqlstring);
         sql_mode = old_mode;
         if (cursor_ausw == -1)
         {
                       return (-1);
         }
         return (0);
}


int LIEF_CLASS::PrepareQuery (void)
/**
Query-Eingabe bearbeiten.
**/
{
         extern short sql_mode;
         short old_mode;
	     char sqlstring [1000]; 

         qstring[0] = (char) 0; 
         if (strcmp (qstring0, qstring) == 0)
         {
                   qschange = 0;
         }
         else
         {
                   qschange = 1;
         }

         close_sql (cursor_ausw);
         strcpy (qstring0, qstring);

         strcpy (sqlstring ,
                   "select lief.mdn, lief.fil, lief.lief, "
                   "adr.adr_nam1, adr.ort1, adr.plz, adr.str, adr.adr_krz "
				   "from lief, adr "
                   "where  "
				   "(lief.mdn = ? or lief.mdn = 0) and "
				   "lief > \"  \" and adr.adr = lief.adr and lief.delstatus = 0 "
                   "order by adr.adr_krz, adr.adr_nam1");

         ins_quest ((char *) &lief.mdn, 1, 0);

         out_quest ((char *) &lief.mdn, 1, 0);
         out_quest ((char *) &lief.fil, 1, 0);
         out_quest ((char *) lief.lief, 0, 17);
         out_quest ((char *) _adr.adr_nam1, 0, 37);
         out_quest ((char *) _adr.ort1, 0, 37);
         out_quest ((char *) _adr.plz, 0, 9);
         out_quest ((char *) _adr.str, 0, 37);
         out_quest ((char *) _adr.adr_krz, 0, 17);
         old_mode = sql_mode;
         sql_mode = 2;
         cursor_ausw = prep_awcursor (sqlstring);
         sql_mode = old_mode;
         if (cursor_ausw == -1)
         {
                       return (-1);
         }
         return (0);
}


int LIEF_CLASS::ShowBuQueryEx (HWND hWnd, int ws_flag) 
/**
Auswahl ueber Gruppen anzeigen.
**/
{
		HWND eWindow;
	    form *savecurrent;
		static struct LST lst;
		int dbpos;


		SortMalloc ();
	    savecurrent = current_form;
		SetCaption ("Auswahl �ber Lieferanten");
		if (searchmode == 1)
		{
                  SetBorder (WS_VISIBLE | WS_CAPTION |WS_THICKFRAME);
                  eWindow = OpenListWindowChoise (17, 73, 6, 2, 0);
		          ListToMamain1 (eWindow);
		}
		else
		{
                  eWindow = OpenListWindowEnF (17, 73, 6, 2, 0);
		          ListToMamain1 (eWindow);
		}
        if (qschangeEx)
        {
 	        Mess.WaitWindow ("Die Lieferanten werden eingelesen ");
   		    sort_awanz = dbpos = 0;
		    while (fetch_scroll (cursor_ausw, NEXT) == 0)
            {
				     if (LiefTestProc && (*LiefTestProc) (lief.lief) == FALSE)
					 {
 					     dbpos ++;
						 continue;
					 }
				     InitSort (sort_awanz);
			         sprintf (sort_awtab[sort_awanz].sort1, "%ld",
						      lief.lief);
					 strcpy (sort_awtab[sort_awanz].sort2, 
						     _adr.adr_nam1);
					 strcpy (sort_awtab[sort_awanz].sort3, 
						     _adr.ort1);
					 strcpy (sort_awtab[sort_awanz].sort4, 
						     _adr.plz);
					 strcpy (sort_awtab[sort_awanz].sort5, 
						     _adr.str);
					 strcpy (sort_awtab[sort_awanz].sort6, 
						     _adr.adr_krz);
					 sortidxtab[sort_awanz] = dbpos + 1;

					 memcpy (&sort_aw, &sort_awtab [sort_awanz],
                                       (int) sizeof (struct SORT_AW));
//                     SetElistRow (&lst.menue, &fsort_aw, sort_awanz);
                    
					 dbpos ++; 
					 sort_awanz ++;
					 if (sort_awanz == MaxSort) 
					 {
						      print_mess (2, "Es konnten nicht alle Lieferanten eingelesen werden");       
						      break;
					 }
					 if (sort_awanz == MAXSORT) break;
            }
 	        Mess.CloseWaitWindow ();
 		    if (searchmode == 1)
			{
				   DisplayList = FALSE;
			      (*fsort_awub.mask[searchfield].after) ();
				   DisplayList = FALSE;
			}
        }
		else
		{
			 SetAktLst (&lst);
		}

		save_fkt (5); 
        set_fkt (endsort, 5);
        SetDblClck (IsAwClck, 1);
        SetAktivWindow (hWnd);
		SetSearchField (searchfield);
		SetSearchMode (searchmode);
        SetListEWindow (1);
		SetHLines (ws_flag);
		SetVLines (TRUE);
		SetMouseLock (TRUE);
        ElistVl (&fsort_awl);
        ElistUb (&fsort_awub);
		Setlistenter (1);


		if (qschangeEx)
		{
/*
            ShowElistMen (&lst.menue, (char *) sort_awtab,
                    sort_awanz,
                   (char *) &sort_aw,
                   (int) sizeof (struct SORT_AW),
                   &fsort_aw);
*/

            ShowElist ((char *) sort_awtab,
                    sort_awanz,
                   (char *) &sort_aw,
                   (int) sizeof (struct SORT_AW),
                   &fsort_aw);
                   SetCharBuff (CharBuff);
				   CharBuffSet = FALSE;

		}
		else
		{
			 SetAktLst (&lst);
/*
			 if (CharBuffSet)
			 {
                   SetCharBuff (CharBuff);
				   CharBuffSet = FALSE;
			 }
*/
		}

        SetSaveList (&lst);
        EnterElist (eWindow, (char *) sort_awtab,
                             sort_awanz,
                             (char *) &sort_aw,
                             (int) sizeof (struct SORT_AW),
                             &fsort_aw);
	    current_form = savecurrent;
		restore_fkt (5); 
		SetMouseLock (FALSE);
        CloseUbControls (); 
        CloseEWindow (eWindow);
        SetListFont (FALSE);
        SetDblClck (NULL, 1);
        if (syskey == KEYESC || syskey == KEY5)
        {
                     close_sql (cursor_ausw);
                     return 0;
         }
		 sort_idx = sortidxtab[sort_idx];
         fetch_scroll (cursor_ausw, DBABSOLUTE, sort_idx);
//         close_sql (cursor_ausw);
         return 0;
}
void LIEF_CLASS::SetNewEx (void)
{
	     strcpy (qstring0Ex, "Start");
}

int LIEF_CLASS::PrepareQueryEx (form *qform, char *qnamen[])
/**
Query-Eingabe bearbeiten.
**/
{
         extern short sql_mode;
         short old_mode;
	     char sqlstring [1000]; 
         char *order; 

         ReadQuery (qform, qnamen); 
         if (strcmp (qstring0Ex, qstring) == 0)
         {
                   qschangeEx = 0;
         }
         else
         {
                   qschangeEx = 1;
                   strcpy (qstring0Ex, qstring);
         }

         close_sql (cursor_ausw);

         if (StatusQuery ())
         {
			       if (strstr (qstring, "adr_nam1"))
				   {
					   order = "order by adr.adr_nam1";
				   }
				   else if (searchmode != 0)
				   {
					   order = "order by adr.adr_nam1";
				   }
				   else
				   {
					   order = "order by lief.lief";

				   }
                   sprintf (sqlstring ,
                            "select lief.mdn, lief.fil, lief.lief, "
                            "adr.adr_nam1, adr.ort1, adr.plz, adr.str, adr.adr_krz "
							"from lief, adr "
                            "where lief  > \" \" "
                             "and adr.adr = lief.adr "
                             "and %s and lief.delstatus = 0 %s",
							  qstring, order);
         }
         else
         {
                   strcpy (sqlstring ,
                            "select lief.mdn, lief.fil, lief.lief, "
                            "adr.adr_nam1, adr.ort1, adr.plz, adr.str, adr.adr_krz "
							"from lief, adr "
                            "where lief > \" \" and adr.adr = lief.adr and lief.delstatus = 0"
                            "order by adr.adr_nam1");
         }

         out_quest ((char *) &lief.mdn, 1, 0);
         out_quest ((char *) &lief.fil, 1, 0);
         out_quest ((char *) &lief.lief, 0, 17);
         out_quest ((char *) _adr.adr_nam1, 0, 37);
         out_quest ((char *) _adr.ort1, 0, 37);
         out_quest ((char *) _adr.plz, 0, 9);
         out_quest ((char *) _adr.str, 0, 37);
         out_quest ((char *) _adr.adr_krz, 0, 17);
         old_mode = sql_mode;
         sql_mode = 2;
         cursor_ausw = prep_awcursor (sqlstring);
         sql_mode = old_mode;
         if (cursor_ausw == -1)
         {
                       return (-1);
         }
         return (0);
}

void LIEF_CLASS::SetSearchModeLief (int mode)
{
	      searchmode = min (2,mode);
}


void LIEF_CLASS::SetSearchFieldLief (int fieldnr)
{
	      searchfield = fieldnr;
}

void LIEF_CLASS::SetCharBuffLief (char *buffer)
{
	     strcpy (CharBuff, buffer);
		 clipped (CharBuff);
		 CharBuffSet = TRUE;
}


