#include<stdio.h>
#include<stdarg.h>
#include<string.h>
#include<windows.h>
#include "stdfkt.h"
#include "strfkt.h"
#include "AktionDlg.h"
#include "mo_progcfg.h"
#include "colbut.h"
#include "help.h"


static int StdSize = STDSIZE;
static int InfoSize = 150;

static int dlgsize    = STDSIZE;
static int ltgraysize = STDSIZE;
static int dlgpossize = STDSIZE;
static int cpsize     = 200;


static mfont dlgfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         GRAYCOL,
                         0,
                         NULL};


static mfont ltgrayfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         LTGRAYCOL,
                        0,
                         NULL};

static mfont PrFont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         WHITECOL,
                        0,
                         NULL};

static mfont dlgposfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         LTGRAYCOL,
                         0,
                         NULL};


char AktionDlg::NormalPreis [20];
char AktionDlg::AktionPreis [20];
char AktionDlg::ChNormal [5];
char AktionDlg::ChAktion [5];

void AktionDlg::SetPreise (double np, double ap)
{
    sprintf (NormalPreis, "%lf", np);
    sprintf (AktionPreis, "%lf", ap);
    strcpy (ChNormal, "J");
    strcpy (ChAktion, "N");
}

double AktionDlg::GetPreis (void)
{
    if (ChAktion[0] == 'J')
    {
        return ratod (AktionPreis);
    }
    return ratod (NormalPreis);
}


char *AktionDlg::HelpName = "32200.cmd";

mfont *AktionDlg::Font = &dlgposfont;

CFIELD *AktionDlg::_fHead [] = {
                     new CFIELD ("pos_frame", "",    36,5, 0, 0,  NULL, "",
                                 CBORDER,   
                                 500, Font, 0, TRANSPARENT),    
                     new CFIELD ("normalpreis_txt", "Normalpreis",  0, 0, 1, 1,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("normalpreis", NormalPreis,  8, 0, 15, 1,  NULL, "%8.4lf", CREADONLY,
                                 NORMALPREIS_CTL, Font, 0, ES_RIGHT | WS_BORDER | TRANSPARENT),

                     new CFIELD ("chnormal", "", ChNormal,
                                 2, 0, 25, 1,  NULL, "", CBUTTON,
                                 CHNORMAL_CTL, Font, 0, BS_AUTORADIOBUTTON),
                     new CFIELD ("aktionpreis_txt", "Aktionspreis",  0, 0, 1, 2,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("aktionpreis", AktionPreis,  8, 0, 15, 2,  NULL, "%8.4lf", CREADONLY,
                                 AKTIONPREIS_CTL, Font, 0, ES_RIGHT | WS_BORDER | TRANSPARENT),

                     new CFIELD ("chaktion", "", ChAktion,
                                 2, 0, 25, 2,  NULL, "", CBUTTON,
                                 CHAKTION_CTL, Font, 0, BS_AUTORADIOBUTTON),
                     NULL,
};

CFORM AktionDlg::fHead (6, _fHead);


CFIELD *AktionDlg::_fFoot [] = {
                     new CFIELD ("ok",      "    OK    ", 10, 0, 0, 0,  NULL, "", 
                                 CBUTTON,
                                 OK_CTL, Font, 0, 0),
//                     new CFIELD ("cancel", " Abbruch  ", 10, 0,12, 0,  NULL, "", 
//                                 CBUTTON,
//                                 CANCEL_CTL, Font, 0, 0),
                     NULL,
};

CFORM AktionDlg::fFoot (2, _fFoot);


CFIELD *AktionDlg::_fDlg [] = {
                     new CFIELD ("fDlg1", (CFORM *) &fHead, 0, 0, 0, 0, NULL, "", CFFORM,
                                   HEADCTL, Font, 0, 0),
                     new CFIELD ("fDlg2", (CFORM *) &fFoot, 0, 0, -1, -2, NULL, "", CFFORM,
                                   HEADCTL, Font, 0, 0),
                     NULL,                      
};

CFORM AktionDlg::fDlg (2, _fDlg);

CFIELD *AktionDlg::_fDlg0 [] = { 
                     new CFIELD ("fDlg", (CFORM *) &fDlg, 0, 0, -1, -1, NULL, "", CFFORM,
                                   500, Font, 0, 0),
                     NULL,
};
CFORM AktionDlg::fDlg0 (1, _fDlg0);

AktionDlg *AktionDlg::ActiveAktionDlg = NULL;
COLORREF AktionDlg::SysBkColor = LTGRAYCOL;


ItProg *AktionDlg::After [] = {
                                   NULL,
};

ItProg *AktionDlg::Before [] = {
                                   NULL,
};

ItFont *AktionDlg::Fonts [] = {
                                  NULL
};


FORMFIELD *AktionDlg::dbfields [] = {
         NULL,
};



char *AktionDlg::EnableHead [] = {
                                 NULL
};




AktionDlg::AktionDlg (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel) :
         DLG (x, y, cx, cy, Caption, Size, Pixel)
{
          
//             BorderType = HIGHCOLBORDER;
             Init (x, y, cx, cy, Caption, Size, Pixel);
}



void AktionDlg::Init (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel)
{
             int i;
             int xfull, yfull;


             xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
             yfull = GetSystemMetrics (SM_CYFULLSCREEN);        


             StdSize = Size;
             dlgfont.FontHeight    = Size;
             dlgposfont.FontHeight = Size;
             ltgrayfont.FontHeight = Size;


             if (xfull < 1000) 
             {
                 dlgfont.FontHeight = 90;
                 dlgposfont.FontHeight = 90;
                 ltgrayfont.FontHeight = 90;
             }

             if (xfull < 800) 
             {

                 dlgfont.FontHeight = 70;
                 dlgposfont.FontHeight = 70;
                 ltgrayfont.FontHeight = 70;
             }

             fHead.SetFieldanz ();
             fFoot.SetFieldanz ();
             fDlg.SetFieldanz ();


             for (i = 0; i < fHead.GetFieldanz (); i ++)
             {
//                       fHead.GetCfield ()[i]->SetFont (&dlgfont); 
//                       fHead.GetCfield ()[i]->SetPosFont (&dlgposfont); 
             }

             PrFont.FontHeight = dlgfont.FontHeight;
             fHead.GetCfield ("normalpreis")->SetFont (&PrFont);
             fHead.GetCfield ("aktionpreis")->SetFont (&PrFont);
             SetTmFont (&dlgfont);
             for (i = 0; Before[i] != NULL; i ++)
             {
                 Before[i]->SetBefore (&fDlg);
             }

             for (i = 0; After[i] != NULL; i ++)
             {
                 After[i]->SetAfter (&fDlg);
             }

             Enable (&fDlg, EnableHead, TRUE); 
             SetDialog (&fDlg0);
}


BOOL AktionDlg::OnKey5 ()
{
        syskey = KEY5;
        fHead.GetText ();
        DestroyWindow ();;
        return TRUE;
}


BOOL AktionDlg::OnKeyReturn ()
{
        syskey = KEY5;
        fHead.GetText ();
        DestroyWindow ();;
        return TRUE;
}

BOOL AktionDlg::OnKey12 ()
{
        syskey = KEY5;
        fHead.GetText ();
        DestroyWindow ();;
        return TRUE;
}


BOOL AktionDlg::OnActivate (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (LOWORD (wParam) == WA_ACTIVE ||
                LOWORD (wParam) == WA_CLICKACTIVE)
        {
              if (WithTabStops)
              {
                  SetTabFocus (); 
              }
              else
              {
                  fWork->SetFocus ();
              }
              return TRUE;
        }
        return FALSE;
}


BOOL AktionDlg::OnCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (fWork)
        {


              if (LOWORD (wParam) == OK_CTL)
              {
                  return OnKeyReturn ();
              }

              if (HIWORD (wParam) == WM_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              if (HIWORD (wParam) == EN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == CBN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == WM_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == EN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == CBN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == BN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == BN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
        }
        return DLG::OnCommand (hWnd, msg, wParam, lParam);
}


/*
BOOL AktionDlg::OnDestroy (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        return FALSE; 

}
*/

BOOL AktionDlg::OnSysCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        
        if (wParam == SC_CLOSE)
        {
            fHead.GetText ();
        }
        return DLG::OnSysCommand (hWnd, msg, wParam, lParam); 
}


void AktionDlg::SetWinBackground (COLORREF Col)
{
          dlgfont.FontBkColor = Col;
          DLG::SetWinBackground (Col);
}


void AktionDlg::ToClipboard (void)
{
          CFIELD *Cfield;
          HGLOBAL hb;
          LPVOID p;
          char *text;

          Cfield = GetCurrentCfield ();
          Cfield->GetText ();
          text = (char *) Cfield->GetFeld ();
          if (text == NULL) return;
          hb = GlobalAlloc (GHND, strlen (text) + 1);
          if (hb == NULL) return;
          p = GlobalLock (hb);
          strcpy ((char *) p, text);
          GlobalUnlock (hb);
          OpenClipboard (hWnd);
          EmptyClipboard ();
          SetClipboardData (CF_TEXT, hb);
          CloseClipboard ();
}

void AktionDlg::FromClipboard (void)
{
          CFIELD *Cfield;
          HGLOBAL hb;
          LPVOID p;
          char *text;

          Cfield = GetCurrentCfield ();
          text = (char *) Cfield->GetFeld ();
          if (text == NULL) return;
          if (IsClipboardFormatAvailable (CF_TEXT) == FALSE) return;
          OpenClipboard (hWnd);
          hb = GetClipboardData (CF_TEXT);
          if (hb == NULL)
          {
                 CloseClipboard ();
                 return;
          }
          p = GlobalLock (hb);
          if (strlen ((char *) p) > (unsigned int ) Cfield->GetLength ())
          {
                 GlobalUnlock (hb);
                 CloseClipboard ();
                 return;
          }
          strcpy (text, (char *) p);
          GlobalUnlock (hb);
          CloseClipboard ();
          Cfield->SetText ();
}

void AktionDlg::Help (void)
{
          CFIELD *Cfield;
          HELP *Help; 
          char *Item;

          Cfield = GetCurrentCfield ();
          if (Cfield == NULL) return;

          Item = Cfield->GetName ();

          Help = new HELP (HelpName, Item, hInstance,hMainWindow);
          delete Help;
          Cfield->SetFocus ();
}


void AktionDlg::PrintComment (char *Name)
{
          HELP *Help; 
          char Comment [256];

          Help = new HELP (HelpName, Name, hInstance,hMainWindow, INITCOMM);
          Help->GetItem (Name, Comment);
          PrintMess (Comment);
          delete Help;
}

void AktionDlg::CallInfo (void)
{
          DLG::CallInfo ();
}








