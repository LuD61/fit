#ifndef _MO_ANGL_DEF
#define _MO_ANGL_DEF
#include "mo_txt.h"
#include "angpt.h"

#define MAXLEN 40

class ANGPLIST
{
            private :
                int mamainmax;
                int mamainmin;
                HWND    hwndTB;
                char *eingabesatz;
                unsigned char *ausgabesatz;
                int zlen;
                int feld_anz;
                int banz;
                PAINTSTRUCT aktpaint;
                char *SwSaetze[30000];
                int PageView;
                unsigned char DlgSatz [20 + MAXLEN + 2];
                FELDER *LstZiel;
                unsigned char *LstSatz;
                int Lstzlen;
                int Lstbanz;
                int SwRecs;
                int AktRow;
                int AktColumn;
                int scrollpos;
                TEXTMETRIC tm;
                char InfoCaption [80];
                int WithMenue;
                int WithToolBar;
                BOOL ListAktiv;
				HWND hMainWindow;
				int Posanz;
                static BOOL textinpmode;
                static MTXT *Mtxt;
                static long TextNr;


             public :
                static void SetMax0 (int);
                static void SetMin0 (int);
                ANGPLIST ();

				void SetTxtMode (BOOL mode)
				{
					textinpmode = mode;
				}

                void SetMenue (int with)
                {
                    WithMenue = with;
                }

                void SetToolMenue (int with)
                {
                    WithToolBar = with;
                }

                void SetMax (void)
                {
                    mamainmax = 1;
                    SetMax0 (mamainmax);
                }

                void SetMin (void)
                {
                    mamainmin = 1;
                    SetMin0 (mamainmin);
                }

                void InitMax (void)
                {
                    mamainmax = 0;
                    SetMax0 (mamainmax);
                }

                void InitMin (void)
                {
                    mamainmin = 0;
                    SetMin0 (mamainmin);
                }

				void SetLager (long);

                BOOL IsListAktiv (void)
                {
                    return ListAktiv;
                }

				int GetPosanz (void)
				{
					return Posanz;
				}

                void SethMainWindow (HWND);
                HWND GetMamain1 (void);
                static int ShowBasis (void);
                static int TestAppend (void);
                static int DeleteLine (void);
                static int InsertLine (void);
                static int AppendLine (void);
                static int PosRab (void);
                static int SearchA (void);
                static void PaintSa (HDC hdc);
                static HWND CreateSaW (void);
                static void CreateSa (void);
				static void DestroySa (void);
                static HWND CreatePlus (void);
				static void DestroyPlus (void);
                void   PaintPlus (HDC);
                static void TestSaPr (void);
                static void FillDM (double, double);
                static void FillEURO (double, double);
                static void FillFremd (double, double);
				static void InitWaehrung (void);
                static void FillAktWaehrung (void);
                static void FillWaehrung (double, double);
                static void ReadPr (void);
                static void ReadMeEinh (void);

                static void rechne_liefme (void);

                static int testme (void);
                static int TestPrproz_diff (void);
                static int testpr (void);
                static void EanGew (char *, BOOL);
                static int ReadEan (double);
                static int Testa_kun (void);
                static void GetLastMeAuf (double);
                static void GetLastMe (double);
                static int TestNewArt (double);
                static int fetcha (void);
                static int fetchaDirect (int);
                static int fetcha_kun (void);
                static int fetchkun_bran2 (void);
                static int ChMeEinh (void);
                static long GenAngpTxt0 (void);
			    static BOOL TxtNrExist (long);
                static long GenAngpTxt (void);
                static int Texte (void);
                static int Querya (void);
                static int doStd (void);
                static void TestMessage (void);
                static int setkey9me (void);
                static int setkey9basis (void);
                static int Savea (void);
                static double GetAngMeVgl (void);
                static double GetAngMeVgl (double, double, short);
//                static BOOL BsdArtikel (double);
//                static void BucheBsd (double, double, short, double);
//                static void UpdateBsd (void);
//                static void DeleteBsd (void);
                static void WriteAngkun (void);
                static void WritePos (int);
                static int WriteAllPos (void);
                static int dokey5 (void);
                static int WriteRow (void);
                static int TestRow (void);
                static void GenNewPosi (void);
                static int PosiEnd (long);
                static void SaveAuf (void);
                static void SetAuf (void);
                static void RestoreAuf (void);
                static int Schirm (void);
                static int SetRowItem (void);
                static int  Getib (void);
                static void ChoiseLines (HWND, HDC);
                static HWND CreateEnter (void);
                static long EnterPosRab (void);
                static HWND CreateAufw (void);
                static void AnzAngWert (void);

                void GetKopfTexte (void);
                static int ReadTxt (long);
                static int CutLines (char *, int, ANGPT_CLASS *);
                static int WriteTxt (long);
                static void InputTxt (char *, long);

                void MoveSaW (void);
                void MovePlus (void);
                void MoveAufw (void);
                void CloseAufw (void);
                void InitSwSaetze (void);
                int ToMemory (int pos);
                void SetStringEnd (char *, int);
                void uebertragen (void);
                void ShowDB (short, short, long);
                void ReadDB (short, short, long);
                void SetSchirm (void);
                void GetListColor (COLORREF *, char *);
                void SetPreisTest (int);
                void GetCfgValues (void);
                void EnterAngp (short, short, long,
                                short, long,  char *);
                void ShowAngp (short, short, long,
                                short, long,  char *);
                void DestroyWindows (void);
                void WorkAngp (void);
                void DestroyMainWindow (void);
                HWND CreateMainWindow (void);
                void MoveMamain1 ();
                void MaximizeMamain1 ();
                void SetMessColors (COLORREF, COLORREF);

                void SetChAttr (int);
                void SetFieldAttr (char *, int);
                int  GetFieldAttr (char *);
                void Geta_bz2_par (void);
                void Geta_kum_par (void);
                void Getauf_me_pr0 (void);
                void Getwa_pos_txt (void);
                void SetRecHeight (void);

                void SethwndTB (HWND);
                void SetTextMetric (TEXTMETRIC *);
                void SetLineRow (int);
                void SetListLines (int); 
                void OnPaint (HWND, UINT, WPARAM, LPARAM);
                void MoveListWindow (void);
                void BreakList (void);
                void OnHScroll (HWND, UINT, WPARAM, LPARAM);
                void OnVScroll (HWND, UINT, WPARAM, LPARAM);
                void OnSize (HWND, UINT, WPARAM, LPARAM);
                void StdAngebot (void);
                void FunkKeys (WPARAM, LPARAM);
                int  GetRecanz (void);
                void SwitchPage0 (int);
                HWND Getmamain2 (void);
                HWND Getmamain3 (void);
                void SetFont (mfont *); 
                void SetListFont (mfont *);
                void ChoiseFont (mfont *);
                void FindString (void);
                void SetLines (int);
                int GetAktRow (void);
                int GetAktRowS (void);
                void SetColors (COLORREF, COLORREF);
                void SetListFocus (void);
 			    void PaintUb (void);
 
};
#endif