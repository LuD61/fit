#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#ifndef CONSOLE
#include "wmaskc.h"
#include "mo_meld.h"
#include "dbfunc.h"
#endif
#include "mo_curso.h"
#include "strfkt.h"
#include "a_bas.h"
#include "a_hndw.h"
#include "lief.h"
#include "ptab.h"
#include "mo_einh.h"
#include "lmb.h"
#include "lief_bzg.h"

static LIEF_CLASS lief_class;
static HNDW_CLASS hndw_class;
static PTAB_CLASS ptab_class;
static LMB_CLASS kmb_class;
static LIEF_BZG_CLASS lief_bzg_class;

static int KmbOK = 0;
static int dsqlstatus;
short knleer = 0;

        static short *me_einh[] = {&liefmebest.me_einh0,
                                  &liefmebest.me_einh1,
                                  &liefmebest.me_einh2,
                                  &liefmebest.me_einh3,
                                  &liefmebest.me_einh4,
                                  &liefmebest.me_einh5,
                                  &liefmebest.me_einh6};
                               
         static short *knuepf[] = {&knleer,
                                  &liefmebest.knuepf1,
                                  &liefmebest.knuepf2,
                                  &liefmebest.knuepf3,
                                  &liefmebest.knuepf4,
                                  &liefmebest.knuepf5,
                                  &liefmebest.knuepf6};

         static double *inh[]    = {&liefmebest.inh0,
                                   &liefmebest.inh1,
                                   &liefmebest.inh2,
                                   &liefmebest.inh3,
                                   &liefmebest.inh4,
                                   &liefmebest.inh5,
                                   &liefmebest.inh6};

void EINH_CLASS::GetBasEinh (double a, KEINHEIT *keinheit)
/**
Basis-Einheit aus a_bas holen.
**/
{
        char wert [5];

        keinheit->me_einh_bas = _a_bas.me_einh;
        memcpy (&ptabn, &ptabn_null, sizeof (struct PTABN));
        sprintf (wert, "%hd", _a_bas.me_einh);

        dsqlstatus = ptab_class.lese_ptab ("me_einh", wert);

        if (dsqlstatus == 0)
        {
                   strcpy (keinheit->me_einh_bas_bez, ptabn.ptbezk);
        }
}
        
void EINH_CLASS::GetLiefEinhBas (double a, KEINHEIT *keinheit)
/**
Kunden-Einheit aus a_hndw, a_eig oder a_eig_div holen.
**/
{
         char wert [5];

         switch (_a_bas.a_typ)
         {
                     case 1 :
                           dsqlstatus = hndw_class.lese_a_hndw (a);
                           return;  // Keine Handelsware f�r Einkauf
                           break;
                     case 2 :
                           dsqlstatus = hndw_class.lese_a_eig (a);
                           break;
                     case 3 :
                           dsqlstatus = hndw_class.lese_a_eig_div (a);
                           break;
                     default :
                           return;
         }

         if (dsqlstatus)
         {
                     return;
         }

         switch (_a_bas.a_typ)
         {
                     case 2 :
                           a_hndw.me_einh_kun = a_eig.me_einh_ek;
                           a_hndw.inh         = a_eig.inh;
                           break;
                     case 3 :
                           a_hndw.me_einh_kun = a_eig_div.me_einh_ek;
                           a_hndw.inh         = a_eig_div.inh;
                           break;
          }

          keinheit->me_einh_lief = a_hndw.me_einh_kun;
          keinheit->inh          = a_hndw.inh;

          memcpy (&ptabn, &ptabn_null, sizeof (struct PTABN));
          sprintf (wert, "%hd", a_hndw.me_einh_kun);

          dsqlstatus = ptab_class.lese_ptab ("me_einh_kun", wert);

          if (dsqlstatus == 0)
          {
                   strcpy (keinheit->me_einh_lief_bez, ptabn.ptbezk);
          }
}
 
int EINH_CLASS::ReadKmb (void)
/**
Combofelder fuellen.
 **/
{
        int dsqlstatus;



        dsqlstatus = kmb_class.dbreadfirst ();
        if (dsqlstatus != 0)
        {
            dsqlstatus = kmb_class.dbreadfirst_a ();
        }
        return dsqlstatus;
}

double EINH_CLASS::GetInh (int pos, double inh0)
/**
Inhalt in Basiseinheit ermitteln.
**/
{

        double inh;

        switch (pos)
        {
        case 0 :
                return inh0;
        case 1 :
                if (liefmebest.knuepf1 == 1) return inh0; 
                inh = liefmebest.inh1 * inh0;
                return GetInh (liefmebest.knuepf1, inh);
        case 2 :
                if (liefmebest.knuepf2 == 2) return inh0; 
                inh = liefmebest.inh2 * inh0;
                return GetInh (liefmebest.knuepf2, inh);
        case 3 :
                if (liefmebest.knuepf3 == 3) return inh0; 
                inh = liefmebest.inh3 * inh0;
                return GetInh (liefmebest.knuepf3, inh);
        case 4 :
                if (liefmebest.knuepf4 == 4) return inh0; 
                inh = liefmebest.inh4 * inh0;
                return GetInh (liefmebest.knuepf4, inh);
        case 5 :
                if (liefmebest.knuepf5 == 5) return inh0; 
                inh = liefmebest.inh5 * inh0;
                return GetInh (liefmebest.knuepf5, inh);
        case 6 :
                if (liefmebest.knuepf6 == 6) return inh0; 
                inh = liefmebest.inh6 * inh0;
                return GetInh (liefmebest.knuepf6, inh);
        }
        return inh0;

}

void EINH_CLASS::FillGeb0 (KEINHEIT *keinheit, int pos)
/**
Erste Gebindeebene holen.
**/
{
    char wert [5];         
    int i;

    i = pos;
    if (*me_einh[i] == 10) return;
    sprintf (wert, "%hd", *me_einh[i]);
    dsqlstatus = ptab_class.lese_ptab ("me_einh", wert);
    clipped (ptabn.ptwer1);
    if (strcmp(ptabn.ptwer1, "G") == 0) return;
    keinheit->me_einh1 = *me_einh[i];
    keinheit->inh1     = *inh[i];
    return;
}

void EINH_CLASS::FillGeb1P (KEINHEIT *keinheit, int pos)
/**
Mittlere und hoechste Gebindeebene holen.
**/
{
    char wert [5];         
    int i;

    for (i = pos + 1; i < 6; i ++)
    {
        if (*knuepf[i] == pos)
        {
            sprintf (wert, "%hd", *me_einh[i]);
            dsqlstatus = ptab_class.lese_ptab ("me_einh", wert);
            clipped (ptabn.ptwer1);
            if (*me_einh[i] == 10) return;
            if (strcmp(ptabn.ptwer1, "G") != 0)
            {
                    return;
            }
            keinheit->me_einh2 = *me_einh[i];
            keinheit->inh2     = *inh[i];
            FillGeb2 (keinheit, BestEinh + 1);
            return;
        }
    }
    for (i = 0; i < pos; i ++)
    {
        if (*knuepf[i] == pos)
        {
            sprintf (wert, "%hd", *me_einh[i]);
            dsqlstatus = ptab_class.lese_ptab ("me_einh", wert);
            clipped (ptabn.ptwer1);
            if (*me_einh[i] == 10) return;
            if (strcmp(ptabn.ptwer1, "G") != 0)
            {
                    return;
            }
            keinheit->me_einh2 = *me_einh[i];
            keinheit->inh2     = *inh[i];
            FillGeb2 (keinheit, BestEinh);
            return;
        }
    }
}


void EINH_CLASS::FillGeb1M (KEINHEIT *keinheit, int pos)
/**
Mittlere und erste Gebindeebene holen.
**/
{
    char wert [5];         
    int i;

    i = pos;
    if (*me_einh[i] == 10) return;
    sprintf (wert, "%hd", *me_einh[i]);
    dsqlstatus = ptab_class.lese_ptab ("me_einh", wert);
    clipped (ptabn.ptwer1);
    if (strcmp(ptabn.ptwer1, "G") != 0) return;
    keinheit->me_einh2 = *me_einh[i];
    keinheit->inh2     = *inh[i];
    FillGeb0 (keinheit, *knuepf[i]);
    return;
}


void EINH_CLASS::FillGeb2 (KEINHEIT *keinheit, int pos)
/**
Hoechste Gebindeebene holen.
**/
{
    char wert [5];         
    int i;

    for (i = pos + 1; i < 6; i ++)
    {
        if (*knuepf[i] == pos)
        {
            sprintf (wert, "%hd", *me_einh[i]);
            dsqlstatus = ptab_class.lese_ptab ("me_einh", wert);
            clipped (ptabn.ptwer1);
            if (*me_einh[i] == 10);
            else if (strcmp(ptabn.ptwer1, "G") == 0);
            else
            {
                    return;
            }
            keinheit->me_einh3 = *me_einh[i];
            keinheit->inh3     = *inh[i];
            return;
        }
    }

    for (i = 0; i < pos; i ++)
    {
        if (*knuepf[i] == pos)
        {
            sprintf (wert, "%hd", *me_einh[i]);
            dsqlstatus = ptab_class.lese_ptab ("me_einh", wert);
            clipped (ptabn.ptwer1);
            if (*me_einh[i] == 10);
            else if (strcmp(ptabn.ptwer1, "G") == 0);
            else
            {
                    return;
            }
            keinheit->me_einh3 = *me_einh[i];
            keinheit->inh3     = *inh[i];
            return;
        }
    }
}


void EINH_CLASS::FillGeb (KEINHEIT *keinheit)
/**
Gebindeeinheiten aus liefmebest fuellen.
**/
{
         char wert [5];         
         short is_palette = 0;
         short is_gebinde = 0;

         keinheit->me_einh1 = 0;
         keinheit->inh1 = (double) 0.0;
         keinheit->me_einh2 = 0;
         keinheit->inh2 = (double) 0.0;
         keinheit->me_einh3 = 0;
         keinheit->inh3 = (double) 0.0;

         if (*me_einh[BestEinh] == 10)
         {
                    is_palette = 1;
         }
         else
         {
                     sprintf (wert, "%hd", *me_einh[BestEinh]);
                     dsqlstatus = ptab_class.lese_ptab ("me_einh", wert);
                     clipped (ptabn.ptwer1);
                     if (strcmp(ptabn.ptwer1, "G") == 0)
                     {
                         is_gebinde = 1;
                     }
          }
          if (is_gebinde)
          {
                     FillGeb0 (keinheit, *knuepf[BestEinh]);
                     keinheit->me_einh2 = *me_einh[BestEinh];
                     keinheit->inh2     = *inh[BestEinh];
                     FillGeb2 (keinheit, BestEinh);
          }
          else if (is_palette)
          {
                keinheit->me_einh3 = *me_einh[BestEinh];
                keinheit->inh3     = *inh[BestEinh];
                FillGeb1M (keinheit, *knuepf[BestEinh]);
          }
          else
          {
                keinheit->me_einh1 = *me_einh[BestEinh];
                keinheit->inh1     = *inh[BestEinh];
                FillGeb1P (keinheit, BestEinh);
          }
}

            
void EINH_CLASS::FillKmb (KEINHEIT *keinheit)
/**
Einheit aus liefmebest fuellen.
**/
{
        char wert [5];

		if (liefmebest.me_einh1 == 0)
		{
			BestEinh = 0;
		}

        switch (BestEinh)
        {
             case 0 :
                   keinheit->me_einh_lief = liefmebest.me_einh0;
                   keinheit->inh      = liefmebest.inh0;
                   keinheit->pr_ek    = liefmebest.pr_ek0;
                   sprintf (wert, "%hd", liefmebest.me_einh0);
                   break;
             case 1 :
                   keinheit->me_einh_lief = liefmebest.me_einh1;
                   keinheit->inh      = GetInh (liefmebest.knuepf1, 
                                                liefmebest.inh1);

                   keinheit->pr_ek    = liefmebest.pr_ek1;
                   sprintf (wert, "%hd", liefmebest.me_einh1);
                   break;
             case 2 :
                   keinheit->me_einh_lief = liefmebest.me_einh2;
                   keinheit->inh      = GetInh (liefmebest.knuepf2, 
                                                liefmebest.inh2);
                   keinheit->pr_ek    = liefmebest.pr_ek2;
                   sprintf (wert, "%hd", liefmebest.me_einh2);
                   break;
             case 3 :
                   keinheit->me_einh_lief = liefmebest.me_einh3;
                   keinheit->inh      = GetInh (liefmebest.knuepf3, 
                                                liefmebest.inh3);
                   keinheit->pr_ek    = liefmebest.pr_ek3;
                   sprintf (wert, "%hd", liefmebest.me_einh3);
                   break;
             case 4 :
                   keinheit->me_einh_lief = liefmebest.me_einh4;
                   keinheit->inh      = GetInh (liefmebest.knuepf4, 
                                                liefmebest.inh4);
                   keinheit->pr_ek    = liefmebest.pr_ek4;
                   sprintf (wert, "%hd", liefmebest.me_einh4);
                   break;
             case 5 :
                   keinheit->me_einh_lief = liefmebest.me_einh5;
                   keinheit->inh      = GetInh (liefmebest.knuepf5, 
                                                liefmebest.inh5);
                   keinheit->pr_ek    = liefmebest.pr_ek5;
                   sprintf (wert, "%hd", liefmebest.me_einh5);
                   break;
             case 6 :
                   keinheit->me_einh_lief = liefmebest.me_einh6;
                   keinheit->inh      = GetInh (liefmebest.knuepf6, 
                                                liefmebest.inh6);
                   keinheit->pr_ek    = liefmebest.pr_ek6;
                   sprintf (wert, "%hd", liefmebest.me_einh6);
                   break;
        }

        memcpy (&ptabn, &ptabn_null, sizeof (struct PTABN));
        dsqlstatus = ptab_class.lese_ptab ("me_einh_kun", wert);
        if (dsqlstatus == 0)
        {
                    strcpy (keinheit->me_einh_lief_bez,
                                      ptabn.ptbezk);
        }
}
         
void EINH_CLASS::AktBestEinh (short mdn, short fil, char *lief_nr,
                               double a, int me_einh_lief)
/**
Mengeneinheit als Klartext fuer Kunde und Artikel holen.
**/
{


        lief.mdn = mdn;
        lief.fil = fil;
        strcpy (lief.lief,  lief_nr);
        dsqlstatus = lief_class.dbreadfirst (); 
		while (dsqlstatus == 100)
		{
			if (lief.fil > 0)
			{
				lief.fil = 0;
			}
			else if (lief.mdn > 0)
			{
				lief.mdn = 0;
			}
			else
			{
				break;
			}
            dsqlstatus = lief_class.dbreadfirst (); 
		}

        liefmebest.mdn = mdn;
        liefmebest.fil = fil;
        strcpy (liefmebest.lief,  lief_nr);
        liefmebest.a   = a;
        if (ReadKmb () == 0)
        {
                KmbOK = 1;
                if (liefmebest.me_einh0 == me_einh_lief)
                {
                    BestEinh = 0;
                }
                else if (liefmebest.me_einh1 == me_einh_lief)
                {
                    BestEinh = 1;
                }
                else if (liefmebest.me_einh2 == me_einh_lief)
                {
                    BestEinh = 2;
                }
                else if (liefmebest.me_einh3 == me_einh_lief)
                {
                    BestEinh = 3;
                }
                else if (liefmebest.me_einh4 == me_einh_lief)
                {
                    BestEinh = 4;
                }
                else if (liefmebest.me_einh5 == me_einh_lief)
                {
                    BestEinh = 5;
                }
                else if (liefmebest.me_einh6 == me_einh_lief)
                {
                    BestEinh = 6;
                }
                else
                {
                    BestEinh = 1;
                }
                return;
        }

        KmbOK = 0;

        lief_bzg.mdn = mdn;
        lief_bzg.fil = fil;
        strcpy (lief_bzg.lief, lief_nr);
        lief_bzg.a = a;
        dsqlstatus = lief_bzg_class.dbreadfirst ();
        while (dsqlstatus == 100)
        {
                  if (lief_bzg.fil > 0)
                  {
                      lief_bzg.fil = 0;
                  }
                  else if (lief_bzg.mdn > 0)
                  {
                      lief_bzg.mdn = 0;
                  }
                  else
                  {
                      break;
                  }
                  dsqlstatus = lief_bzg_class.dbreadfirst ();
        }

        BestEinh = 0;
        if (dsqlstatus == 0)
        {
               if (me_einh_lief == lief_bzg.me_einh_ek)
               {
                            BestEinh = 1;
               }
               else
               {
                           BestEinh = 0;
               }
               return;
        }
/*
        BestEinh = 0;
        dsqlstatus = lese_a_bas (a);
        if (dsqlstatus)
        {
                     return;
        }
        switch (_a_bas.a_typ)
         {
                     case 1 :
                           dsqlstatus = hndw_class.lese_a_hndw (a);
                           break;
                     case 2 :
                           dsqlstatus = hndw_class.lese_a_eig (a);
                           break;
                     case 3 :
                           dsqlstatus = hndw_class.lese_a_eig_div (a);
                           break;
                     default :
                           return;
         }

         switch (_a_bas.a_typ)
         {
                     case 2 :
                           a_hndw.me_einh_kun = a_eig.me_einh_ek;
                           break;
                     case 3 :
                           a_hndw.me_einh_kun = a_eig_div.me_einh_ek;
                           break;
         }
         if (me_einh_lief == a_hndw.me_einh_kun)
         {
                     BestEinh = 1;
         }
*/
}


void EINH_CLASS::NextBestEinh (short mdn, short fil, char *lief,
                              double a, int me_einh_lief, KEINHEIT *keinheit)
/**
Naechste Mengeneinheit als Klartext fuer Kunde und Artikel holen.
**/
{
        AktBestEinh (mdn, fil, lief, a, me_einh_lief);
        BestEinh ++;
        if (KmbOK)
        {
            if (BestEinh > 6) BestEinh = 0;
            switch (BestEinh)
            {
                 case 1 :
                         if (liefmebest.me_einh1 == 0)
                             BestEinh = 0;
                             break;
                 case 2 :
                         if (liefmebest.me_einh2 == 0)
                             BestEinh = 0;
                             break;
                 case 3 :
                         if (liefmebest.me_einh3 == 0)
                             BestEinh = 0;
                             break;
                 case 4 :
                         if (liefmebest.me_einh4 == 0)
                             BestEinh = 0;
                             break;
                 case 5 :
                         if (liefmebest.me_einh5 == 0)
                             BestEinh = 0;
                             break;
                 case 6 :
                         if (liefmebest.me_einh6 == 0)
                             BestEinh = 0;
                             break;
            }
        }
        else
        {
                if (BestEinh == 2) BestEinh = 0;
        }
        GetLiefEinh (mdn, fil, lief,a, keinheit);
}

void EINH_CLASS::GetLiefEinh (short mdn, short fil, char *lief_nr,
                            double a, KEINHEIT *keinheit)
/**
Mengeneinheit als Klartext fuer Lieferant und Artikel holen.
**/
{
        char wert [5];
		int dsqlstatus;

        keinheit->me_einh_lief = 0;

        liefmebest.mdn = mdn;
        liefmebest.fil = fil;
        strcpy (liefmebest.lief, lief_nr);
        liefmebest.a   = a;

		lief.mdn = mdn;
		lief.fil = fil;
		strcpy (lief.lief, lief_nr);
        dsqlstatus = lief_class.dbreadfirst (); 
		while (dsqlstatus != 0)
		{
			if (lief.fil > 0)
			{
				lief.fil = 0;
			}
			else if (lief.mdn > 0)
			{
				lief.mdn = 0;
			}
			else
			{
				break;
			}
            dsqlstatus = lief_class.dbreadfirst (); 
		}


        if (ReadKmb () == 0)
        {
                  GetBasEinh (a, keinheit);
                  FillKmb (keinheit);
                  FillGeb (keinheit);
                  return;
        }

        dsqlstatus = lese_a_bas (a);

        lief_bzg.mdn = mdn;
        lief_bzg.fil = fil;
        strcpy (lief_bzg.lief, lief_nr);
        lief_bzg.a = a;
        dsqlstatus = lief_bzg_class.dbreadfirst ();
        while (dsqlstatus == 100)
        {
                  if (lief_bzg.fil > 0)
                  {
                      lief_bzg.fil = 0;
                  }
                  else if (lief_bzg.mdn > 0)
                  {
                      lief_bzg.mdn = 0;
                  }
                  else
                  {
                      break;
                  }
                  dsqlstatus = lief_bzg_class.dbreadfirst ();
        }

        if (dsqlstatus == 0 && lief_bzg.me_einh_ek > 0)
        {
                  keinheit->me_einh_lief = lief_bzg.me_einh_ek;
//                  keinheit->inh      = a_kun.inh;
                  keinheit->inh      = lief_bzg.min_best; 
                  keinheit->pr_ek    = 0.0; 
                  memcpy (&ptabn, &ptabn_null, sizeof (struct PTABN));
                  sprintf (wert, "%hd", lief_bzg.me_einh_ek);

                  dsqlstatus = ptab_class.lese_ptab ("me_einh_ek",
                                           wert);

                  if (dsqlstatus == 0)
                  {
                             strcpy (keinheit->me_einh_lief_bez,
                                     ptabn.ptbezk);
                  }
                  GetBasEinh (a, keinheit);
                  if (BestEinh == 0)
                  {
                              keinheit->inh = 1.0;
                              keinheit->me_einh_lief = keinheit->me_einh_bas;
                              strcpy (keinheit->me_einh_lief_bez,
                                      keinheit->me_einh_bas_bez); 
                  }
                  if (keinheit->me_einh_lief == 0)
				  {
                              keinheit->me_einh_lief = keinheit->me_einh_bas;
                              strcpy (keinheit->me_einh_lief_bez,
                                      keinheit->me_einh_bas_bez); 
				  }
                  return;
        }


//        GetLiefEinhBas (a, keinheit);

        GetBasEinh (a, keinheit);
        if (BestEinh == 0)
        {
                  keinheit->me_einh_lief = keinheit->me_einh_bas;
                  strcpy (keinheit->me_einh_lief_bez,
                          keinheit->me_einh_bas_bez); 
        }
        if (keinheit->me_einh_lief == 0)
		{
                  keinheit->me_einh_lief = keinheit->me_einh_bas;
                  strcpy (keinheit->me_einh_lief_bez,
                          keinheit->me_einh_bas_bez); 
		}
}                                   


static char vEinh [31];

static ITEM iEinhTxt      ("", "Einheit f�r Bestell-Menge",  "", 0);   
static ITEM iEinh         ("", vEinh,          "", 0);   

static ITEM vOK     ("",  " OK ",  "", 0);
static ITEM vCancel ("",  "Abbrechen", "", 0);

static int testcombo (void);
static int testOK (void);
static int testCancel (void);

static field _fDrkw [] = {
&iEinhTxt,       37, 0,  1, 2,  0, "", DISPLAYONLY, 0, 0, 0,
&iEinh,          37,10,  2, 2,  0, "DROPDOWN", COMBOBOX | DROPLIST, 0, testcombo, 0,     
// &vOK,            11, 0,  4, 5,  0, "", BUTTON, 0, testOK,     KEY12,
// &vCancel,        11, 0,  4,18,  0, "", BUTTON, 0, testCancel, KEY5, 
&vCancel,           11, 0,  4,11,  0, "", BUTTON, 0, testCancel, KEY5, 
};

static char Otab [10][81];
static short *kmbtab[] = 
{
                          &liefmebest.me_einh0,
                          &liefmebest.me_einh1, 
                          &liefmebest.me_einh2, 
                          &liefmebest.me_einh3, 
                          &liefmebest.me_einh4, 
                          &liefmebest.me_einh5, 
                          &liefmebest.me_einh6,
};

static combofield  cbfield[] = {1,  81, 0, (char *) Otab,
                                0,  0, 0, NULL};


static form fDrkw = {3, 0, 0, _fDrkw, 0, 0, 0, cbfield, NULL}; 


static int testcombo (void)
{
       if (testkeys ()) return 0;

       if (syskey == KEYCR)
       {
           syskey = KEY12;
           break_enter ();
       }
       return 0;
}


static int testCancel (void)
{
       if (testkeys ()) return 0;

       switch (syskey)
       {
          case KEYDOWN :
          case KEYUP :
          case KEYTAB :
              return 0;
       }
       syskey = KEY5;
       break_enter ();
       return 0;
}

static int testOK (void)
{
       if (testkeys ()) return 0;

       switch (syskey)
       {
          case KEYDOWN :
          case KEYUP :
          case KEYTAB :
              return 0;
       }

       syskey = KEY12;
       break_enter ();
       return 0;
}

static int eBreak (void)
{
       switch (syskey)
       {
          case KEYDOWN :
          case KEYUP :
          case KEYTAB :
              return 0;
       }
       syskey = KEY5;
       break_enter ();
       return 0;
}

static int eOk (void)
{
       switch (syskey)
       {
          case KEYDOWN :
          case KEYUP :
          case KEYTAB :
              return 0;
       }
       syskey = KEY12;
       break_enter ();
       return 0;
}

void EINH_CLASS::FillOtab (int i)
/**
Text fuer Menegeneinheit fuellen.
**/
{
       short me_einh;
       char wert [4];
       int dsqlstatus;
	   int knpf;
	   char buffer [256];
	   char bu1[256];

       me_einh = *kmbtab[i];
       sprintf (wert, "%hd", me_einh);
       dsqlstatus = ptab_class.lese_ptab ("me_einh", wert);
       if (dsqlstatus == 0)
       {
                   sprintf (bu1, "%hd %s", me_einh, 
                                               clipped (ptabn.ptbezk));
       }
	   else
	   {
		           return;
	   }
	   if (i == 0)
	   {
	           sprintf (Otab [i], "%-10.10s", bu1);
			   return;
	   }
	   knpf = *knuepf[i];
	   if (knpf < 0 || knpf > 7) return;
	   me_einh = *kmbtab[knpf];
       sprintf (wert, "%hd", me_einh);
       dsqlstatus = ptab_class.lese_ptab ("me_einh", wert);
       if (dsqlstatus != 0)
	   {
	           sprintf (Otab [i], "%-10.10s", bu1);
			   return;
	   }
       sprintf (buffer, "  = %7.2lf %s", *inh[i], clipped (ptabn.ptbezk));
	   sprintf (Otab [i], "%-10.10s %s", bu1, buffer);
}


void EINH_CLASS::InitCombo (void)
/**
Combofelder fuellen.
**/
{
        static BOOL ComboOK = FALSE;
        int dsqlstatus;
        int i;


        dsqlstatus = kmb_class.dbreadfirst ();
        if (dsqlstatus != 0)
        {
            dsqlstatus = kmb_class.dbreadfirst_a ();
        }
        if (dsqlstatus == 0)
        {
            for (i = 0; i < 7; i ++)
            {
                    if  (*kmbtab[i] == 0) break;
                    FillOtab (i);
            }
            cbfield[0].cbanz = i;
            strcpy (vEinh,   Otab[0]);
            fDrkw.mask[1].rows = 10;
            if (i < 8) 
            {
                fDrkw.mask[1].rows = i + 2;
            }
            return;
        }
 
        strcpy (Otab[0], "Basis-Artikeleinheit");
        strcpy (Otab[1], "Lieferanten-Bestelleinheit");
        strcpy (vEinh,   Otab[BestEinh]);
        cbfield[0].cbanz = 2;
        fDrkw.mask[1].rows = 4;
}

void EINH_CLASS::ChoiseCombo (void)
/**
Ausgewaehlte Combofelder auswerten.
**/
{
        int i;

        clipped (vEinh);
        for (i = 0; i < cbfield[0].cbanz; i ++)
        {
            if (strcmp (vEinh,clipped (Otab[i])) == 0)
            {
                BestEinh = i;
                break;
            }
        }
}

void EINH_CLASS::ChoiseEinh (void)
/**
String suchen.
**/
{
        HWND hFind;
        HWND ahWnd;
        HWND hMainInst;

        hMainInst = (HANDLE) 
            GetWindowLong (GetActiveWindow (),  GWL_HINSTANCE);
        save_fkt (5);
        save_fkt (12);
        save_fkt (6);

        InitCombo ();
        strcpy (vEinh, Otab[BestEinh]);
        set_fkt (eBreak, 5);
        set_fkt (eOk, 12);

        no_break_end ();
        ahWnd = AktivWindow;
        EnableWindows (GetActiveWindow (), FALSE);
        SetBorder (WS_POPUP | WS_VISIBLE | WS_CAPTION | WS_DLGFRAME);
        hFind = OpenWindowChC (7, 45, 10, 20, hMainInst,
//                  "");
                "Einheit f�r die Bestellsmenge");
        SetButtonTab (TRUE);
        SetCurrentField (0);
        enter_form (hFind, &fDrkw,0, 0);
        SetButtonTab (FALSE);
        AktivWindow = ahWnd;
        EnableWindows (AktivWindow, TRUE);
        SetActiveWindow (AktivWindow);
		CloseControls (&fDrkw);
        DestroyWindow (hFind);
        hFind = NULL;
        no_break_end ();
        restore_fkt (5);
        restore_fkt (12);
        restore_fkt (6);
        if (syskey == KEY5) return;
        ChoiseCombo ();
}



