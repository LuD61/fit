#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include "comcthlp.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "stdfkt.h"
#include "wmaskc.h"
#include "colbut.h"
#include "cmask.h"
#include "lbox.h"
#include "mo_stdac.h"
#include "a_bas.h"


struct STD_A STDAC::stda;
struct STD_A *STDAC::stdatab;
long STDAC::stdaanz = 0;
CH *STDAC::Choise1;
SBEST_CLASS STDAC::sbest_class;
DB_CLASS STDAC::DbClass;

long STDAC::Getstdaanz (void)
{
	    return stdaanz;
}

struct STD_A *STDAC::GetStdaTab (void)
{
	   return stdatab;
}

struct STD_A *STDAC::GetStda (int pos)
{
	   if (pos >= stdaanz) return NULL;
	   return &stdatab[pos];
}

int STDAC::AllArt (int pos)
/**
Kunde auf erledigt setzen.
**/
{
	   int i;
 	   char buffer [256];

	   for (i = 0; i < stdaanz; i ++)
	   {
	             strcpy (stdatab[i].status, "  #1");
				 stdatab[i].nstatus = 1;
	             memcpy (&stda, &stdatab[i], sizeof (STD_A));
/*
                 sprintf (buffer, " %-8s %-15s $%-25s$ $%-11s$ %-8s $%-11s$ %-13s",
		                stda.status,   
                        stda.a, 
				        stda.a_bz1,
		                stda.me_einh_bz, 
		                stda.pr_vk, 
						stda.dat,
						stda.me);
*/
                 sprintf (buffer, " %-8s %-15s $%-25s$ %-13s",
		                stda.status,   
                        stda.a, 
				        stda.a_bz1,
						stda.me);
	             Choise1->UpdateRecord (buffer, i);
	   }
	   return 0;
}


int STDAC::ArtOK (int pos)
/**
Kunde auf erledigt setzen.
**/
{
 	   char buffer [256];

	   if (stdatab[pos].nstatus)
	   {
	             strcpy (stdatab[pos].status, " ");
				 stdatab[pos].nstatus = 0;
	   }
	   else
	   {
	             strcpy (stdatab[pos].status, "  #1");
				 stdatab[pos].nstatus = 1;
	   }
	   memcpy (&stda, &stdatab[pos], sizeof (STD_A));
/*
       sprintf (buffer, " %-8s %-15s $%-25s$ $%-11s$ %-8s $%-11s$ %-13s",
		                stda.status,   
                        stda.a, 
				        stda.a_bz1,
		                stda.me_einh_bz, 
		                stda.pr_vk, 
						stda.dat,
						stda.me);
*/
                 sprintf (buffer, " %-8s %-15s $%-25s$ %-13s",
		                stda.status,   
                        stda.a, 
				        stda.a_bz1,
						stda.me);
	   Choise1->UpdateRecord (buffer, pos);
	   return 0;
}

int STDAC::StdaOK (int pos)
/**
Telefon-Nummer w�hlen.
**/
{
	    syskey = KEY12;
		PostQuitMessage (0);
		return 0;
}

int STDAC::Show (HWND hWnd, int mdn, int fil, char *lief)
/**
Standardauftr�ge auswaehlen
**/
{
	  int cx, cy;
	  char buffer [256];
	  int cursor;
	  int i;
  	  short ag;
	  short wg;
	  char a_bz1 [25];
	  int dsqlstatus;
	  char tchar;
	  HINSTANCE hMainInst; 
	  int ret;

	  tchar = Gettchar ();
	  Settchar ('$');

	  hMainInst = (HINSTANCE) GetClassLong (hWnd, GCL_HMODULE); 
	  Sel = LoadBitmap (hMainInst, "Sel");
	  Msk = LoadBitmap (hMainInst, "Msk");
	  stdaanz = 0;
	  DbClass.sqlin ((short *) &mdn, 1, 0);
	  DbClass.sqlin ((short *) &fil, 1, 0);
	  DbClass.sqlin ((char *)  lief, 0, 17);
	  DbClass.sqlout ((short *) &stdaanz, 2, 0);
	  cursor = DbClass.sqlcursor ("select count (*) "   
			                        "from best_stnd "
									"where mdn = ? "
									"and fil = ? "
									"and lief = ?");

      dsqlstatus = DbClass.sqlopen (cursor); 
      dsqlstatus = DbClass.sqlfetch (cursor); 
      DbClass.sqlclose (cursor);
  	  if (dsqlstatus == 100) 
	  {
		  return 0;
	  }

	  if (stdaanz == 0)
	  {
		  disp_mess ("Keine Standardbestellungen vorhanden",2);
		  return 0;
	  }
									



	  stdatab = new struct STD_A [stdaanz + 2];
	  if (stdatab == NULL)
	  {
		  disp_mess ("Fehler bei der Speicherzuornung", 2);
		  return 0;
	  }


	  cx = 80;
	  cy = 20;
	  Choise1 = new CH (cx, cy, "Alle", "W�hlen", "OK", "Abbruch");
	  Choise1->AddImage (Sel, Msk);
      Choise1->OpenWindow (hMainInst, hWnd);
      Choise1->AddAccelerator (VK_F12, 1, NULL);
      Choise1->AddAccelerator (VK_F8,  2, NULL);
      Choise1->AddAccelerator (VK_F5,  4, NULL);
//	  sprintf (buffer, " %8s %15s %25s %11s %8s %11s %13s", "1", "1", "1", "1", "1", "1", "1"); 
	  sprintf (buffer, " %8s %15s %25s %13s", "1", "1", "1", "1"); 
	  Choise1->VLines (buffer, 3);
	  EnableWindows (hWnd, FALSE);
/*
	  sprintf (buffer, " %-8s  %-15s %-25s %-11s %-8s %-11s %-13s", 
		                                            "Status",
		                                            "Artikel", 
													"Bezeichnung",
		                                            "Einh", 
		                                            "DM/Einh", 
													"Datum",
													"Std Menge");
*/
	  sprintf (buffer, " %-8s  %-15s %-25s %-13s", 
		                                            "Status",
		                                            "Artikel", 
													"Bezeichnung",
													"Std Menge");

	  Choise1->InsertCaption (buffer);

	  DbClass.sqlin ((short *)   &mdn, 1, 0);
	  DbClass.sqlin ((short *)   &fil, 1, 0);
	  DbClass.sqlin ((char *)    lief, 0, 17);
	  DbClass.sqlout ((short *)  &best_stnd.mdn, 1, 0);
	  DbClass.sqlout ((short *)  &best_stnd.fil, 1, 0);
	  DbClass.sqlout ((char *)   best_stnd.lief, 0, 17);
	  DbClass.sqlout ((double *) &best_stnd.a, 3, 0);
	  DbClass.sqlout ((short *)  &ag, 1, 0);
	  DbClass.sqlout ((short *)  &wg, 1, 0);
	  DbClass.sqlout ((char *)   &a_bz1, 0, 25);

	  if (SortMode == 0)
	  {
           		cursor = DbClass.sqlcursor ("select best_stnd.mdn, best_stnd.fil, "
			                        "best_stnd.lief, "
									"best_stnd.a, a_bas.ag, a_bas.wg, "
									"a_bas.a_bz1 "
			                        "from best_stnd,a_bas "
									"where best_stnd.mdn = ? "
									"and   best_stnd.fil = ? "  
									"and   best_stnd.lief = ? "  
									"and a_bas.a = best_stnd.a "
									"order by 3,4");  
	  }
	  else if (SortMode == 1)
	  {
           		cursor = DbClass.sqlcursor ("select best_stnd.mdn, best_stnd.fil, "
			                        "best_stnd.lief, "
									"best_stnd.a, a_bas.ag, a_bas.wg, "
									"a_bas.a_bz1 "
			                        "from best_stnd,a_bas "
									"where best_stnd.mdn = ? "
									"and   best_stnd.fil = ? "  
									"and   best_stnd.lief = ? "  
									"and a_bas.a = best_stnd.a "
									"order by 5,4");  
	  }
	  else if (SortMode == 2)
	  {
           		cursor = DbClass.sqlcursor ("select best_stnd.mdn, best_stnd.fil, "
			                        "best_stnd.lief, "
									"best_stnd.a, a_bas.ag, a_bas.wg, "
									"a_bas.a_bz1 "
			                        "from best_stnd,a_bas "
									"where best_stnd.mdn = ? "
									"and   best_stnd.fil = ? "  
									"and   best_stnd.lief = ? "  
									"and a_bas.a = best_stnd.a "
									"order by 6,4");  
	  }
	  else if (SortMode == 3)
	  {
           		cursor = DbClass.sqlcursor ("select best_stnd.mdn, best_stnd.fil, "
			                        "best_stnd.lief, "
									"best_stnd.a, a_bas.ag, a_bas.wg, "
									"a_bas.a_bz1 "
			                        "from best_stnd,a_bas "
									"where best_stnd.mdn = ? "
									"and   best_stnd.fil = ? "  
									"and   best_stnd.lief = ? "  
									"and a_bas.a = best_stnd.a "
									"order by 5,7");  
	  }
	  else if (SortMode == 4)
	  {
           		cursor = DbClass.sqlcursor ("select best_stnd.mdn, best_stnd.fil, "
			                        "best_stnd.lief, "
									"best_stnd.a, a_bas.ag, a_bas.wg, "
									"a_bas.a_bz1 "
			                        "from best_stnd,a_bas "
									"where best_stnd.mdn = ? "
									"and   best_stnd.fil = ? "  
									"and   best_stnd.lief = ? "  
									"and a_bas.a = best_stnd.a "
									"order by 6,7");  
	  }
			                      


      best_stnd.mdn = mdn;        
      best_stnd.fil = fil;        
      strcpy (best_stnd.lief, lief);        
      i = 0;

	  while (DbClass.sqlfetch (cursor) == 0)
	  {
                     sbest_class.dbreadfirsta ();
                     sprintf (stdatab[i].a, "%.0lf", best_stnd.a); 
                     dsqlstatus = lese_a_bas (best_stnd.a);
                     if (dsqlstatus == 0)
                     {
                                 strcpy (stdatab[i].a_bz1, _a_bas.a_bz1); 
                     }
                     else
                     {
                                 strcpy (stdatab[i].a_bz1, " "); 
                     }
                     strcpy (stdatab[i].me_einh_bz, ""); 
                     sprintf (stdatab[i].pr_vk, "%6.2lf", 0.0); 
					 strcpy (stdatab[i].dat, " "); 
					 strcpy (stdatab[i].status, " "); 
				      stdatab[i].nstatus = 0;
                     dlong_to_asc (best_stnd.zuord_dat, stdatab[i].dat); 
                     sprintf (stdatab[i].me, "%.2lf", best_stnd.me); 
					 memcpy (&stda, &stdatab[i], sizeof (struct STD_A));
/*
                     sprintf (buffer, " %8s %-15s $%-25s$ $%-11s$ %-8s $%-11s$ %-13s",
		                              stda.status,   
                                      stda.a, 
				                      stda.a_bz1,
		                              stda.me_einh_bz, 
		                              stda.pr_vk, 
						              stda.dat,
						              stda.me);
*/
                     sprintf (buffer, " %8s %-15s $%-25s$ %-13s",
		                              stda.status,   
                                      stda.a, 
				                      stda.a_bz1,
						              stda.me);
	                 Choise1->InsertRecord (buffer);
		             i ++;
	  }

	  stdaanz = i;
	  DbClass.sqlclose (cursor);

	  Choise1->SetOkFunc (StdaOK);
	  Choise1->SetDialFunc (ArtOK);
	  Choise1->SetSpezFunc (AllArt);
	  Choise1->ProcessMessages ();
	  Settchar (tchar);
	  if (syskey != KEY5)
	  {
	            Choise1->GetText (buffer);
				ret = 1;
	  }
	  else
	  {
		        ret = 0;
	  }
	  EnableWindows (hWnd, TRUE);
      Choise1->DestroyWindow ();
	  delete Choise1;
	  Choise1 = NULL;
	  currentfield = 0;
      return ret;
}

