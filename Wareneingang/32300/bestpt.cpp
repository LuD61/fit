#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "a_kun.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "bestpt.h"

struct BESTPT bestpt, bestpt_null;

void BESTPT_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &bestpt.nr, 2, 0);
    out_quest ((char *) &bestpt.nr,2,0);
    out_quest ((char *) &bestpt.zei,2,0);
    out_quest ((char *) bestpt.txt,0,61);
            cursor = prepare_sql ("select bestpt.nr,  "
"bestpt.zei,  bestpt.txt from bestpt "

#line 26 "bestpt.rpp"
                                  "where nr = ? "
                                  "order by zei");

    ins_quest ((char *) &bestpt.nr,2,0);
    ins_quest ((char *) &bestpt.zei,2,0);
    ins_quest ((char *) bestpt.txt,0,61);
            sqltext = "update bestpt set bestpt.nr = ?,  "
"bestpt.zei = ?,  bestpt.txt = ? "

#line 30 "bestpt.rpp"
                                  "where nr = ? "
                                  "and zei  = ?";

            ins_quest ((char *) &bestpt.nr, 2, 0);
            ins_quest ((char *) &bestpt.zei, 2, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &bestpt.nr, 2, 0);
            ins_quest ((char *) &bestpt.zei, 2, 0);
            test_upd_cursor = prepare_sql ("select nr from bestpt "
                                  "where nr = ? "
                                  "and   zei  = ?");

            ins_quest ((char *) &bestpt.nr, 2, 0);
            ins_quest ((char *) &bestpt.zei, 2, 0);
            del_cursor = prepare_sql ("delete from bestpt "
                                  "where nr = ? "
                                  "and   zei  = ?");

            ins_quest ((char *) &bestpt.nr, 2, 0);
            del_cursor_posi = prepare_sql ("delete from bestpt "
                                  "where  nr = ?");

    ins_quest ((char *) &bestpt.nr,2,0);
    ins_quest ((char *) &bestpt.zei,2,0);
    ins_quest ((char *) bestpt.txt,0,61);
            ins_cursor = prepare_sql ("insert into bestpt ("
"nr,  zei,  txt) "

#line 54 "bestpt.rpp"
                                      "values "
                                      "(?,?,?)"); 

#line 56 "bestpt.rpp"
}
int BESTPT_CLASS::dbreadfirst (void)
/**

Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int BESTPT_CLASS::delete_bestpposi (void)
/**
Alle text fuer eine Position in aufp loeschen
**/
{
         if (del_cursor_posi == -1)
         {  
                this->prepare ();
         }
         execute_curs (del_cursor_posi);
         return (sqlstatus);
}

void BESTPT_CLASS::dbclose (void)
{
         if (del_cursor_posi != -1)
	   {
		     close_sql (del_cursor_posi);
		     del_cursor_posi = -1;
	   }
	   DB_CLASS::dbclose ();
}
