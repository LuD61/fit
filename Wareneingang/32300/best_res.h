#ifndef _BEST_RES_DEF
#define _BEST_RES_DEF

#include "dbclass.h"

struct BEST_RES {
   double    a;
   long      auf;
   short     fil;
   long      kun;
   long      lief_term;
   short     mdn;
   double    me;
   long      melde_term;
   short     stat;
   short     kun_fil;
};
extern struct BEST_RES best_res, best_res_null;

#line 7 "best_res.rh"

class BEST_RES_CLASS : public DB_CLASS 
{
       private :
               int del_cursor_a;
               int del_cursor_auf;
               void prepare (void);
       public :
               BEST_RES_CLASS () : DB_CLASS (), del_cursor_a (-1),del_cursor_auf (-1)
               {
               }
               int dbreadfirst (void);
               int delete_a (void);
               int delete_auf (void);
};
#endif
