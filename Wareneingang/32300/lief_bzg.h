#ifndef _LIEF_BZG_DEF
#define _LIEF_BZG_DEF

#include "windows.h"
#include "mo_draw.h"
#include "wmask.h"
#include "dbclass.h"

struct LIEF_BZG {
   double    a;
   char      best_txt1[25];
   char      best_txt2[25];
   short     fil;
   char      lief[17];
   char      lief_best[17];
   char      lief_kz[2];
   char      lief_rht[2];
   long      lief_s;
   short     lief_zeit;
   short     me_einh_ek;
   short     mdn;
   double    min_best;
   double    pr_ek;
   char      me_kz[2];
   long      dat;
   char      zeit[7];
   char      modif[2];
   double    pr_ek_eur;
   double    pr_ek_sa;
   double    pr_ek_sa_eur;
   char      freifeld1[37];
};
extern struct LIEF_BZG lief_bzg, lief_bzg_null;

#line 10 "lief_bzg.rh"

class LIEF_BZG_CLASS : public DB_CLASS
{
       private :
               int cursor_a;
               void prepare (void);
       public :
               LIEF_BZG_CLASS () : DB_CLASS (), cursor_a (-1)
               {
               }
               ~LIEF_BZG_CLASS ()
               {
                   dbclose ();
               }
               int dbreadfirst (void);
               int dbreadfirsta (void);
               int dbreada (void);
               void dbclose (void)
               {
                      if (cursor_a != -1)
                      {
                              DB_CLASS::sqlclose (cursor_a);
                              cursor_a = -1;
                      }
                      DB_CLASS::dbclose ();
               }
};
#endif
