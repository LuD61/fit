#include<stdio.h>
#include<stdarg.h>
#include<string.h>
#include<windows.h>
#include "stdfkt.h"
#include "strfkt.h"
#include "LiefBzgDlg.h"
#include "MeEinhDlg.h" 
#include "mo_progcfg.h"
#include "mo_gdruck.h" 
#include "colbut.h"
#include "help.h"
#include "lbox.h"
#include "liefmebest.h"
#include "a_bas.h"
#include "searchmdn.h"
#include "searchfil.h"
#include "searchlief.h"
#include "searcha.h"
#ifdef BIWAK
#include "conf_env.h"
#endif


static int StdSize = STDSIZE;
static int InfoSize = 150;

static int dlgsize    = STDSIZE;
static int ltgraysize = STDSIZE;
static int dlgpossize = STDSIZE;
static int cpsize     = 200;


static mfont dlgfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         GRAYCOL,
                         0,
                         NULL};

static mfont InfoFont = {
                         "ARIAL", InfoSize, 0, 0,
                         BLUECOL,
                         LTGRAYCOL,
                         10,
                         NULL};

static mfont ltgrayfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         LTGRAYCOL,
                        0,
                         NULL};

static mfont dlgposfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         LTGRAYCOL,
                         0,
                         NULL};


static mfont cpfont = {
                        "Times New Roman", 200, 0, 1,
                         BLUECOL,
                         GRAYCOL,
                         0,
                         NULL};


struct MEEINHF
{
      char mdn [7];
      char mdn_krz [20];
      char fil [7];
      char fil_krz [18];
      char lief [18];
      char lief_krz [20];
      char a [15];
      char a_bz1 [26];
      char a_bz2 [26];
      char me_kz [80];

      char tara0 [10];
      char me_einh0 [5];
      char me_einh0_bz [11];
      char inh0 [10];
      char pr_ek0 [10];

      char tara1 [10];
      char me_einh1 [5];
      char me_einh1_bz [11];
      char inh1 [10];
      char pr_ek1 [10];

      char tara2 [10];
      char me_einh2 [5];
      char me_einh2_bz [11];
      char inh2 [10];
      char pr_ek2 [10];

      char tara3 [10];
      char me_einh3 [5];
      char me_einh3_bz [11];
      char inh3 [10];
      char pr_ek3 [10];

      char tara4 [10];
      char me_einh4 [5];
      char me_einh4_bz [11];
      char inh4 [10];
      char pr_ek4 [10];

      char tara5 [10];
      char me_einh5 [5];
      char me_einh5_bz [11];
      char inh5 [10];
      char pr_ek5 [10];

      char tara6 [10];
      char me_einh6 [5];
      char me_einh6_bz [11];
      char inh6 [10];
      char pr_ek6 [10];

      char knuepf1 [5];
      char knuepf2 [5];
      char knuepf3 [5];
      char knuepf4 [5];
      char knuepf5 [5];
      char knuepf6 [5];

} meeinhf, meeinhf_null;

mfont *MeEinhDlg::Font = &dlgposfont;

CFIELD *MeEinhDlg::_fHead [] = {
                     new CFIELD ("mdn_txt", "Mandant",  0, 0, 1, 0,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("mdn", meeinhf.mdn,  6, 0, 10, 0,  NULL, "%4d", CEDIT,
                                 MDN_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("mdn_choise", "", 2, 0, 16, 0, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("mdn_krz", meeinhf.mdn_krz, 19, 0, 30, 0,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, 0),
                     new CFIELD ("fil_txt", "Filiale",  0, 0, 50, 0,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("fil", meeinhf.fil,  6, 0, 58, 0,  NULL, "%4d", CEDIT,
                                 FIL_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("fil_choise", "", 2, 0, 64, 0, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("fil_krz", meeinhf.fil_krz, 17, 0, 67, 0,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, 0),
                     new CFIELD ("lief_txt", "Lieferant",  0, 0, 1, 2,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("lief", meeinhf.lief, 17, 0, 10, 2,  NULL, "", CEDIT,
                                 LIEF_CTL, Font, 0, 0),
                     new CFIELD ("lief_choise", "", 2, 0, 27, 2, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("lief_krz", meeinhf.lief_krz, 17, 0, 30, 2,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, 0),
                     new CFIELD ("a_txt", "Artikel",  0, 0, 1, 3,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("a", meeinhf.a, 14, 0, 10, 3,  NULL, "%13.0lf", CEDIT,
                                 A_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("a_choise", "", 2, 0, 24, 3, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("a_bz1", meeinhf.a_bz1, 25, 0, 30, 3,  NULL, "", CREADONLY,
                                 500, Font, 0, 0),
                     new CFIELD ("a_bz2", meeinhf.a_bz2, 25, 0, 30, 4,  NULL, "", CREADONLY,
                                 500, Font, 0, 0),
                     new CFIELD ("me_kz_txt", "Mengen-KZ", 0, 5, 62, 2,  NULL, "", DISPLAYONLY,
                                 500, Font, 0, 0),
                     new CFIELD ("me_kz", meeinhf.me_kz,  15, 0, 62, 3,  NULL, "", CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER),
                     NULL,
};

CFORM MeEinhDlg::fHead (19, _fHead);	// alt : 6


CFIELD *MeEinhDlg::_fPos [] = {
                     new CFIELD ("pos_frame", "",    85,15, 1, 6,  NULL, "",
                                 CBORDER,   
                                   500, Font, 0, TRANSPARENT),    

                     new CFIELD ("ebene", "Ebene",  10, 0, 2, 7,  NULL, "",  CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_DLGFRAME),
                     new CFIELD ("einh_txt", "Einheit", 18, 0, 12, 7,  NULL, "", CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_DLGFRAME),
                     new CFIELD ("inh1_txt", "Inhalt", 11, 0, 30, 7,  NULL, "", CREADONLY,
                                  500, Font, 0, TRANSPARENT | WS_DLGFRAME),
                     new CFIELD ("ek_txt", "EK-Preis",  11, 0, 41, 7,  NULL, "", CREADONLY,
                                  500, Font, 0, TRANSPARENT | WS_DLGFRAME),
                     new CFIELD ("knuepf_txt", "Verkn�pfung", 11, 0, 52, 7,  NULL, "", CREADONLY,
                                  500, Font, 0, TRANSPARENT | WS_DLGFRAME),


                     new CFIELD ("me_einh0_txt", "Einheit0",  0, 0, 2, 8,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("me_einh0", meeinhf.me_einh0, 3, 0, 12, 8,  NULL, "%2d", CEDIT,
                                 ME_EINH0_CTL, Font, 0, ES_RIGHT, 0),
                     new CFIELD ("me_einh0_choise", "", 2, 0, 15, 8, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("me_einh0_bz", meeinhf.me_einh0_bz, 10, 0, 18, 8, NULL, "", CREADONLY,
                                  500, Font, 0, TRANSPARENT),
                     new CFIELD ("inh0", meeinhf.inh0, 10, 0, 30, 8,  NULL, "%8.3lf", CEDIT,
                                  INH0_CTL, Font, 0, ES_RIGHT, 0),
                     new CFIELD ("pr_ek0", meeinhf.pr_ek0, 10, 0, 41, 8,  NULL, "%8.4lf", CEDIT,
                                  PR_EK0_CTL, Font, 0, ES_RIGHT, 0),

                     new CFIELD ("me_einh1_txt", "Einheit1",  0, 0, 2, 9,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("me_einh1", meeinhf.me_einh1, 3, 0, 12, 9,  NULL, "%2d", CEDIT,
                                 ME_EINH1_CTL, Font, 0, ES_RIGHT, 0),
                     new CFIELD ("me_einh1_choise", "", 2, 0, 15, 9, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("me_einh1_bz", meeinhf.me_einh1_bz, 10, 0, 18, 9, NULL, "", CREADONLY,
                                  500, Font, 0, TRANSPARENT),
                     new CFIELD ("inh1", meeinhf.inh1, 10, 0, 30, 9,  NULL, "%8.3lf", CEDIT,
                                  INH1_CTL, Font, 0, ES_RIGHT, 0),
                     new CFIELD ("pr_ek1", meeinhf.pr_ek1, 10, 0, 41, 9,  NULL, "%8.4lf", CEDIT,
                                  PR_EK1_CTL, Font, 0, ES_RIGHT, 0),
                     new CFIELD ("knuepf1", meeinhf.knuepf1, 2, 0, 52, 9,  NULL, "%1d", CEDIT,
                                  KNUEPF1_CTL, Font, 0, ES_RIGHT, 0),

                     new CFIELD ("me_einh2_txt", "Einheit2",  0, 0, 2, 10,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("me_einh2", meeinhf.me_einh2, 3, 0, 12, 10,  NULL, "%2d", CEDIT,
                                 ME_EINH2_CTL, Font, 0, ES_RIGHT, 0),
                     new CFIELD ("me_einh2_choise", "", 2, 0, 15, 10, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("me_einh2_bz", meeinhf.me_einh2_bz, 10, 0, 18, 10, NULL, "", CREADONLY,
                                  500, Font, 0, TRANSPARENT),
                     new CFIELD ("inh2", meeinhf.inh2, 10, 0, 30, 10,  NULL, "%8.3lf", CEDIT,
                                  INH2_CTL, Font, 0, ES_RIGHT, 0),
                     new CFIELD ("pr_ek2", meeinhf.pr_ek2, 10, 0, 41, 10,  NULL, "%8.4lf", CEDIT,
                                  PR_EK2_CTL, Font, 0, ES_RIGHT, 0),
                     new CFIELD ("knuepf2", meeinhf.knuepf2, 2, 0, 52, 10,  NULL, "%1d", CEDIT,
                                  KNUEPF2_CTL, Font, 0, ES_RIGHT, 0),


                     new CFIELD ("me_einh3_txt", "Einheit3",  0, 0, 2, 11,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("me_einh3", meeinhf.me_einh3, 3, 0, 12, 11,  NULL, "%2d", CEDIT,
                                 ME_EINH3_CTL, Font, 0, ES_RIGHT, 0),
                     new CFIELD ("me_einh3_choise", "", 2, 0, 15, 11, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("me_einh3_bz", meeinhf.me_einh3_bz, 10, 0, 18, 11, NULL, "", CREADONLY,
                                  500, Font, 0, TRANSPARENT),
                     new CFIELD ("inh3", meeinhf.inh3, 10, 0, 30, 11,  NULL, "%8.3lf", CEDIT,
                                  INH3_CTL, Font, 0, ES_RIGHT, 0),
                     new CFIELD ("pr_ek3", meeinhf.pr_ek3, 10, 0, 41, 11,  NULL, "%8.4lf", CEDIT,
                                  PR_EK3_CTL, Font, 0, ES_RIGHT, 0),
                     new CFIELD ("knuepf3", meeinhf.knuepf3, 2, 0, 52, 11,  NULL, "%1d", CEDIT,
                                  KNUEPF3_CTL, Font, 0, ES_RIGHT, 0),

                     new CFIELD ("me_einh4_txt", "Einheit4",  0, 0, 2, 12,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("me_einh4", meeinhf.me_einh4, 3, 0, 12, 12,  NULL, "%2d", CEDIT,
                                 ME_EINH4_CTL, Font, 0, ES_RIGHT, 0),
                     new CFIELD ("me_einh4_choise", "", 2, 0, 15, 12, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("me_einh4_bz", meeinhf.me_einh4_bz, 10, 0, 18, 12, NULL, "", CREADONLY,
                                  500, Font, 0, TRANSPARENT),
                     new CFIELD ("inh4", meeinhf.inh4, 10, 0, 30, 12,  NULL, "%8.3lf", CEDIT,
                                  INH4_CTL, Font, 0, ES_RIGHT, 0),
                     new CFIELD ("pr_ek4", meeinhf.pr_ek4, 10, 0, 41, 12,  NULL, "%8.4lf", CEDIT,
                                  PR_EK4_CTL, Font, 0, ES_RIGHT, 0),
                     new CFIELD ("knuepf4", meeinhf.knuepf4, 2, 0, 52, 12,  NULL, "%1d", CEDIT,
                                  KNUEPF4_CTL, Font, 0, ES_RIGHT, 0),


                     new CFIELD ("me_einh5_txt", "Einheit5",  0, 0, 2, 13,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("me_einh5", meeinhf.me_einh5, 3, 0, 12, 13,  NULL, "%2d", CEDIT,
                                 ME_EINH5_CTL, Font, 0, ES_RIGHT, 0),
                     new CFIELD ("me_einh5_choise", "", 2, 0, 15, 13, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("me_einh5_bz", meeinhf.me_einh5_bz, 10, 0, 18, 13, NULL, "", CREADONLY,
                                  500, Font, 0, TRANSPARENT),
                     new CFIELD ("inh5", meeinhf.inh5, 10, 0, 30, 13,  NULL, "%8.3lf", CEDIT,
                                  INH5_CTL, Font, 0, ES_RIGHT, 0),
                     new CFIELD ("pr_ek5", meeinhf.pr_ek5, 10, 0, 41, 13,  NULL, "%6.2lf", CEDIT,
                                  PR_EK5_CTL, Font, 0, ES_RIGHT, 0),
                     new CFIELD ("knuepf5", meeinhf.knuepf5, 2, 0, 52, 13,  NULL, "%1d", CEDIT,
                                  KNUEPF5_CTL, Font, 0, ES_RIGHT, 0),

                     new CFIELD ("me_einh6_txt", "Einheit4",  0, 0, 2, 14,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("me_einh6", meeinhf.me_einh6, 3, 0, 12, 14,  NULL, "%2d", CEDIT,
                                 ME_EINH6_CTL, Font, 0, ES_RIGHT, 0),
                     new CFIELD ("me_einh6_choise", "", 2, 0, 15, 14, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("me_einh6_bz", meeinhf.me_einh6_bz, 10, 0, 18, 14, NULL, "", CREADONLY,
                                  500, Font, 0, TRANSPARENT),
                     new CFIELD ("inh6", meeinhf.inh6, 10, 0, 30, 14,  NULL, "%8.3lf", CEDIT,
                                  INH6_CTL, Font, 0, ES_RIGHT, 0),
                     new CFIELD ("pr_ek6", meeinhf.pr_ek6, 10, 0, 41, 14,  NULL, "%6.2lf", CEDIT,
                                  PR_EK6_CTL, Font, 0, ES_RIGHT, 0),
                     new CFIELD ("knuepf6", meeinhf.knuepf6, 2, 0, 52, 14,  NULL, "%1d", CEDIT,
                                  KNUEPF6_CTL, Font, 0, ES_RIGHT, 0),
                     NULL,
};


CFORM MeEinhDlg::fPos (55, _fPos);	// alt 29


CFIELD *MeEinhDlg::_fFoot [] = {
                     new CFIELD ("ok",      "    OK    ", 10, 0, 0, 0,  NULL, "", 
                                 CBUTTON,
                                 OK_CTL, Font, 0, 0),
                     new CFIELD ("cancel", " Abbruch  ", 10, 0,12, 0,  NULL, "", 
                                 CBUTTON,
                                 CANCEL_CTL, Font, 0, 0),
                     NULL,
};


CFORM MeEinhDlg::fFoot (2, _fFoot);


CFIELD *MeEinhDlg::_fMeEinh [] = {
                     new CFIELD ("fMeEinh1", (CFORM *) &fHead, 0, 0, 0, 0, NULL, "", CFFORM,
                                   HEADCTL, Font, 0, 0),
                     new CFIELD ("fMeEinh2", (CFORM *) &fPos,  0, 0, 0, 0, NULL, "", CFFORM,
                                   POSCTL, Font, 0, 0),
                     new CFIELD ("fMeEinh3", (CFORM *) &fFoot, 0, 0, -1, -2, NULL, "", CFFORM,
                                   FOOTCTL, Font, 0, 0),
                     NULL,                      
};

CFORM MeEinhDlg::fMeEinh (3, _fMeEinh);

CFIELD *MeEinhDlg::_fMeEinh0 [] = { 
                     new CFIELD ("fMeEinh", (CFORM *) &fMeEinh, 0, 0, -1, -1, NULL, "", CFFORM,
                                   500, Font, 0, 0),
                     NULL,
};
CFORM MeEinhDlg::fMeEinh0 (1, _fMeEinh0);

MeEinhWork MeEinhDlg::meEinhWork;
int MeEinhDlg::EnterMode = ENTERHEAD;
SEARCHA MeEinhDlg::SearchA;
double MeEinhDlg::saveda = 0.0;
char *MeEinhDlg::HelpName = "12900.cmd";
MeEinhDlg *MeEinhDlg::ActiveMeEinh = NULL;
HBITMAP MeEinhDlg::SelBmp;
BitImage MeEinhDlg::ImageDelete;
COLORREF MeEinhDlg::SysBkColor = LTGRAYCOL;
int MeEinhDlg::ListStyle = 0;

int MeEinhDlg::ReadMdn (void)
{
         if (syskey == 0) return 0;
         if (syskey == KEY9) return 0;

         fPos.GetText ();
         FromForm (dbheadfields);
         if (meEinhWork.GetMdnName (meeinhf.mdn_krz,   atoi (meeinhf.mdn)) == 100)
         {
             print_mess (2, "Mandant %hd ist nicht angelegt", atoi (meeinhf.mdn));
             fHead.SetCurrentName ("mdn");
             return 0;
         }
         if (atoi (meeinhf.mdn) == 0)
         {
             sprintf (meeinhf.fil, "%4d", 0);
             meEinhWork.GetFilName (meeinhf.fil_krz,   atoi (meeinhf.mdn), atoi (meeinhf.fil));
             Enable (&fMeEinh, EnableHeadFil, FALSE); 
             fHead.SetCurrentName ("lief");
         }
         else
         {
             if (fHead.GetCfield ("fil")->IsDisabled ())
             {
                    Enable (&fMeEinh, EnableHeadFil, TRUE); 
                    fHead.SetCurrentName ("fil");
             }
         }
         if (syskey == KEYUP)
         {
                    fHead.SetCurrentName ("lief");
         }
         fHead.SetText ();
         return 0;
}

int MeEinhDlg::ReadFil (void)
{
          
         if (syskey == 0) return 0;
         if (syskey == KEY9) return 0;
         fPos.GetText ();
         FromForm (dbheadfields);
         if (meEinhWork.GetFilName (meeinhf.fil_krz,   atoi (meeinhf.mdn), atoi (meeinhf.fil)) == 100)
         {
             print_mess (2, "Filiale %hd ist nicht angelegt", atoi (meeinhf.fil));
             fHead.SetCurrentName ("fil");
             return 0;
         }
         fHead.SetText ();
         return 0;
}

int MeEinhDlg::ReadLief (void)
{
        
         if (DLG::MousePressed)
         {
             return FALSE;
         }

         if (syskey != KEYCR)
         {
             return FALSE;
         }

         
         if (syskey == 0) return 0;
         if (syskey == KEY9) return 0;

         fHead.GetText ();
         if (meeinhf.lief[0] == 0)
         {
             return 0;
         }
         FromForm (dbheadfields);
         if (meEinhWork.GetLiefName (meeinhf.lief_krz, atoi (meeinhf.mdn), 
                                     meeinhf.lief) == 100)
         {
             disp_mess ("Lieferant ist nicht angelegt", 2);
             fHead.SetCurrentName ("lief");
             return TRUE;
         }

         fHead.SetText ();
         return 0;
}

int MeEinhDlg::EnterPos (void)
{
/*
         switch (syskey)
         {
               case KEYDOWN :
               case KEYTAB :
                   fHead.SetCurrentName ("mdn");
                   return TRUE;
         }
        
         if (DLG::MousePressed)
         {
             return FALSE;
         }

         if (syskey != KEYCR)
         {
             return FALSE;
         }
*/
         
         fHead.GetText ();


         FromForm (dbheadfields);

         Enable (&fMeEinh, EnableHead, FALSE); 
         Enable (&fMeEinh, EnablePos,  TRUE); 
         EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELETE,   MF_ENABLED);
         ToolBar_SetState (ActiveDlg->GethwndTB (),   IDM_DELETE, TBSTATE_ENABLED);
         if (ActiveMeEinh->GetToolbar2 () != NULL)
         {
                 ((ColButton *)ActiveMeEinh->GetToolbar2 ()->GetCfield ("del")->GetFeld ())->bmp = 
                     ImageDelete.Image;
                  ActiveMeEinh->GetToolbar2 ()->display ();
         }
         fPos.SetCurrentName ("me_einh0");
         return TRUE;
}

int MeEinhDlg::CreateLiefBzg (void)
{
       LiefBzgDlg *Dlg;
       HWND hWnd;

       hWnd = ActiveDlg->GethWnd ();
       lief_bzg.mdn = atoi (meeinhf.mdn);
       lief_bzg.fil = atoi (meeinhf.fil);
       strcpy (lief_bzg.lief, meeinhf.lief);
       Dlg = new LiefBzgDlg (-1, -1, 89, 16, "Lieferanten-Bezugsquellen",105, FALSE,
                             RAISEDBORDER, ENTERADATA);
       Dlg->SetStyle (WS_VISIBLE | WS_POPUP | WS_THICKFRAME | WS_CAPTION | WS_SYSMENU);
       Dlg->SetStyleEx (NULL);
       Dlg->SetWinBackground (LTGRAYCOL);
       EnableWindows (hWnd, FALSE);
       Dlg->OpenScrollWindow (DLG::hInstance, hWnd);
	   Dlg->ProcessMessages ();
       delete Dlg;
       EnableWindows (hWnd, TRUE);
       if (syskey == KEY5)
       {
                 return -1;
       }
       return 0; 
}


BOOL MeEinhDlg::TestLief_bzg (void)
/**
Test, ob fuer den Lieferanten ein Eintrag in lief_bzg existiert.
**/
{
       BOOL OK; 

       lief_bzg.mdn = atoi (meeinhf.mdn);
       lief_bzg.fil = atoi (meeinhf.fil);
       strcpy (lief_bzg.lief, meeinhf.lief);
       lief_bzg.a = ratod (meeinhf.a);
       
       OK = meEinhWork.TestLiefBzg ();
       while (!OK)
       {
                  break; 
                  if (lief_bzg.fil > 0)
                  {
                      lief_bzg.fil = 0;
                  }
                  else if (lief_bzg.mdn > 0)
                  {
                      lief_bzg.mdn = 0;
                  }
                  else
                  {
                      break;
                  }
       }
       if (OK) return TRUE; 

       if (abfragejn (ActiveDlg->GethWnd (),
                      "Der Artikel ist nicht im Sortiment des Lieferanten\n"
                       "Artikel anlegen ?", "J"))
       {
              if (CreateLiefBzg () == 0)
              {
                       return TestLief_bzg ();
              }
              return FALSE;
       }
       return FALSE;
}
        
int MeEinhDlg::ReadA (void)
{
         int dsqlstatus;
         char wert[5];
         int i;
          
         fHead.GetText ();
         if (ratod (meeinhf.a) == 0.0)
         {
             print_mess (2, "Artikel 0 ist nicht erlaubt");
             fHead.SetCurrentName ("a");
             return -1;
         }

         sprintf (liefmebest.lief, "%s",  meeinhf.lief);  
         dsqlstatus = meEinhWork.ReadA 
             (atoi (meeinhf.mdn), atoi (meeinhf.fil), meeinhf.lief, ratod (meeinhf.a));
         if (dsqlstatus == -1)
         {
             print_mess (2, "Artikel %.0lf ist nicht angelegt", ratod (meeinhf.a));
             fHead.SetCurrentName ("a");
             return -1;
         }
/*
         if (dsqlstatus == -2)
         {
             print_mess (2, "Artikel %.0lf ist nicht in den\n"
                            "Lieferanten-Bezugsquellen angelegt", ratod (meeinhf.a));
             fHead.SetCurrentName ("a");
             return -1;
         }
*/
         meEinhWork.BeginWork ();
         if (TestLief_bzg () == FALSE)
         {
             fHead.SetCurrentName ("a");
             meEinhWork.RollbackWork ();
             return -1;
         }

         if (dsqlstatus != 0)
         {
             meEinhWork.GetLiefDefaults ();
         }
         else
         {
             meEinhWork.TestLiefPr ();
         }

         ToForm (dbheadfields);
         for (i = 0; MeKzCombo[i] != NULL; i ++); 
         if (atoi (lief_bzg.me_kz) < i &&
             atoi (lief_bzg.me_kz) >= 0)
         {
                  strcpy (meeinhf.me_kz, MeKzCombo [atoi (lief_bzg.me_kz)]);
         }
         else
         {
                  strcpy (meeinhf.me_kz, MeKzCombo [0]);
         }
         ToForm (dbfields); 
         sprintf (liefmebest.lief, "%s",  meeinhf.lief);  

         for (i = 0; MeEinhTab[i] != NULL; i ++)
         {
               sprintf (wert, "%d", 
                        atoi ((char *) fPos.GetCfield (MeEinhTab[i])->GetFeld ()));
                        meEinhWork.GetPtab (fPos.GetCfield (MeEinhBzTab[i])->GetFeld (), 
                                                    "me_einh_ek", wert);
         }

         fHead.SetText ();
         fPos.SetText ();
         return 0;
}

int MeEinhDlg::GetPtab (void)
{
         char wert [5];
         CFIELD *Cfield;
         int i;


         fPos.GetText ();

         Cfield = ActiveDlg->GetCurrentCfield ();

         for (i = 0; MeEinhTab[i] != NULL; i ++)
         {
                 if (strcmp (Cfield->GetName (), 
                       fPos.GetCfield (MeEinhTab[i])->GetName ()) == 0)
                   {
                           sprintf (wert, "%d", 
                               atoi ((char *) fPos.GetCfield (MeEinhTab[i])->GetFeld ()));
                           meEinhWork.GetPtab (MeEinhBzTab[i], MeEinhTab[i], wert);
                           fPos.GetCfield (MeEinhBzTab[i])->SetText ();         
                   }
         }
         return 0;
}



ItProg *MeEinhDlg::After [] = {
                                   new ItProg ("mdn",         ReadMdn),
                                   new ItProg ("fil",         ReadFil),
                                   new ItProg ("lief",        ReadLief),
//                                   new ItProg ("a",           ReadA),
//                                   new ItProg ("me_einh0",    GetPtab),
                                   NULL,
};

ItProg *MeEinhDlg::Before [] = {
                                   NULL,
};

ItFont *MeEinhDlg::Fonts [] = {
                                  NULL
};

FORMFIELD *MeEinhDlg::dbheadfields [] = {
         new FORMFIELD ("mdn",       meeinhf.mdn,       (short *)   &liefmebest.mdn,       FSHORT,   NULL),
         new FORMFIELD ("fil",       meeinhf.fil,       (short *)   &liefmebest.fil,       FSHORT,   NULL),
         new FORMFIELD ("lief",      meeinhf.lief,      (char *)    liefmebest.lief,       FCHAR,    NULL),
         new FORMFIELD ("a",         meeinhf.a,         (double *)  &liefmebest.a,         FDOUBLE,  NULL),
         new FORMFIELD ("a_bz1",     meeinhf.a_bz1,     (char *)    _a_bas.a_bz1,          FCHAR,    NULL),
         new FORMFIELD ("a_bz2",     meeinhf.a_bz2,     (char *)    _a_bas.a_bz2,          FCHAR,    NULL),
         NULL,
};

FORMFIELD *MeEinhDlg::dbfields [] = {
         new FORMFIELD ("me_einh0",  meeinhf.me_einh0,  (short *)  &liefmebest.me_einh0, FSHORT,    NULL),
         new FORMFIELD ("inh0",      meeinhf.inh0,      (double *) &liefmebest.inh0,     FDOUBLE,  NULL),
         new FORMFIELD ("pr_ek0",    meeinhf.pr_ek0,    (double *) &liefmebest.pr_ek0,   FDOUBLE,    NULL),

         new FORMFIELD ("me_einh1",  meeinhf.me_einh1,  (short *)  &liefmebest.me_einh1, FSHORT,    NULL),
         new FORMFIELD ("inh1",      meeinhf.inh1,      (double *) &liefmebest.inh1,     FDOUBLE,  NULL),
         new FORMFIELD ("knuepf1",   meeinhf.knuepf1,   (short *)  &liefmebest.knuepf1,  FSHORT,    NULL),
         new FORMFIELD ("pr_ek1",    meeinhf.pr_ek1,    (double *) &liefmebest.pr_ek1,   FDOUBLE,    NULL),


         new FORMFIELD ("me_einh2",  meeinhf.me_einh2,  (short *)  &liefmebest.me_einh2, FSHORT,    NULL),
         new FORMFIELD ("inh2",      meeinhf.inh2,     (double *) &liefmebest.inh2,     FDOUBLE,  NULL),
         new FORMFIELD ("knuepf2",   meeinhf.knuepf2,  (short *)  &liefmebest.knuepf2,  FSHORT,    NULL),
         new FORMFIELD ("pr_ek2",    meeinhf.pr_ek2,   (double *) &liefmebest.pr_ek2,   FDOUBLE,    NULL),

         new FORMFIELD ("me_einh3",  meeinhf.me_einh3, (short *)  &liefmebest.me_einh3, FSHORT,    NULL),
         new FORMFIELD ("inh3",      meeinhf.inh3,     (double *) &liefmebest.inh3,     FDOUBLE,  NULL),
         new FORMFIELD ("knuepf3",   meeinhf.knuepf3,   (short *)  &liefmebest.knuepf3,  FSHORT,    NULL),
         new FORMFIELD ("pr_ek3",    meeinhf.pr_ek3,    (double *) &liefmebest.pr_ek3,   FDOUBLE,    NULL),

         new FORMFIELD ("me_einh4",  meeinhf.me_einh4,  (short *)  &liefmebest.me_einh4, FSHORT,    NULL),
         new FORMFIELD ("inh4",      meeinhf.inh4,      (double *) &liefmebest.inh4,     FDOUBLE,  NULL),
         new FORMFIELD ("knuepf4",   meeinhf.knuepf4,   (short *)  &liefmebest.knuepf4,  FSHORT,    NULL),
         new FORMFIELD ("pr_ek4",    meeinhf.pr_ek4,    (double *) &liefmebest.pr_ek4,   FDOUBLE,    NULL),

         new FORMFIELD ("me_einh5",  meeinhf.me_einh5,  (short *)  &liefmebest.me_einh5, FSHORT,    NULL),
         new FORMFIELD ("inh5",      meeinhf.inh5,      (double *) &liefmebest.inh5,     FDOUBLE,  NULL),
         new FORMFIELD ("knuepf5",   meeinhf.knuepf5,   (short *)  &liefmebest.knuepf5,  FSHORT,    NULL),
         new FORMFIELD ("pr_ek5",    meeinhf.pr_ek5,    (double *) &liefmebest.pr_ek5,   FDOUBLE,    NULL),

         new FORMFIELD ("me_einh6",  meeinhf.me_einh6,  (short *)  &liefmebest.me_einh6, FSHORT,    NULL),
         new FORMFIELD ("inh6",      meeinhf.inh6,      (double *) &liefmebest.inh6,     FDOUBLE,  NULL),
         new FORMFIELD ("knuepf6",   meeinhf.knuepf6,   (short *)  &liefmebest.knuepf6,  FSHORT,    NULL),
         new FORMFIELD ("pr_ek6",    meeinhf.pr_ek6,    (double *) &liefmebest.pr_ek6,   FDOUBLE,    NULL),
         NULL,
};



char *MeEinhDlg::EnableHead [] = {
                                "mdn",
                                "mdn_choise",
                                "fil",
                                "fil_choise",
                                "lief",
                                "lief_choise",
                                "a",
                                "a_choise",
                                 NULL
};

char *MeEinhDlg::EnableHeadFil [] = {
                                     "fil",
                                     NULL
};


char *MeEinhDlg::EnablePos[] = {
                                "me_einh0",
                                "me_einh0_choise",
                                "inh0", 
                                "pr_ek0",

                                "me_einh1",
                                "me_einh1_choise",
                                "inh1", 
                                "knuepf1",
                                "pr_ek1",

                                "me_einh2",
                                "me_einh2_choise",
                                "inh2", 
                                "knuepf2",
                                "pr_ek2",

                                "me_einh3",
                                "me_einh3_choise",
                                "inh3", 
                                "knuepf3",
                                "pr_ek3",

                                "me_einh4",
                                "me_einh4_choise",
                                "inh4", 
                                "knuepf4",
                                "pr_ek4",

                                "me_einh5",
                                "me_einh5_choise",
                                "inh5", 
                                "knuepf5",
                                "pr_ek5",

                                "me_einh6",
                                "me_einh6_choise",
                                "inh6", 
                                "knuepf6",
                                "pr_ek6",

                                 NULL,
};
char *MeEinhDlg::MeEinhTab [] = {
                                 "me_einh0",
                                 "me_einh1",
                                 "me_einh2",
                                 "me_einh3",
                                 "me_einh4",
                                 "me_einh5",
                                 "me_einh6",
                                 NULL,
};

char *MeEinhDlg::MeEinhChoiseTab [] = {
                                      "me_einh0_choise",
                                      "me_einh1_choise",
                                      "me_einh2_choise",
                                      "me_einh3_choise",
                                      "me_einh4_choise",
                                      "me_einh5_choise",
                                      "me_einh6_choise",
                                       NULL,
};

char *MeEinhDlg::MeEinhBzTab [] = {
                                   "me_einh0_bz",
                                   "me_einh1_bz",
                                   "me_einh2_bz",
                                   "me_einh3_bz",
                                   "me_einh4_bz",
                                   "me_einh5_bz",
                                   "me_einh6_bz",
                                    NULL,
};

char *MeEinhDlg::MeKzCombo[] = {"Bestelleinheit", 
                                 "Verkaufseinheit" , 
                                  NULL};

MeEinhDlg::MeEinhDlg (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel) :
         DLG (x, y, cx, cy, Caption, Size, Pixel)
{
          
             BorderType = HIGHCOLBORDER;
             Init (x, y, cx, cy, Caption, Size, Pixel);
}

MeEinhDlg::MeEinhDlg (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel, 
                  int BorderType) :
         DLG (x, y, cx, cy, Caption, Size, Pixel)
{
          
             this->BorderType = BorderType;
             Init (x, y, cx, cy, Caption, Size, Pixel);
}

MeEinhDlg::MeEinhDlg (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel, 
                  int BorderType, int EnterM) :
         DLG (x, y, cx, cy, Caption, Size, Pixel)
{
          
             this->BorderType = BorderType;
             EnterMode = EnterM;
             Init (x, y, cx, cy, Caption, Size, Pixel);
}


void MeEinhDlg::Init (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel)
{
             int i;
             int xfull, yfull;


             ActiveMeEinh = this; 
             xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
             yfull = GetSystemMetrics (SM_CYFULLSCREEN);        


             ImageDelete.Image    = BMAP::LoadBitmap (hInstance, "DEL",       "DELMASK", SysBkColor);
             ImageDelete.Inactive = BMAP::LoadBitmap (hInstance, "DELIA",     "DELMASK", SysBkColor);

             StdSize = Size;
             dlgfont.FontHeight    = Size;
             dlgposfont.FontHeight = Size;
             ltgrayfont.FontHeight = Size;
             cpfont.FontHeight     = cpsize;
             InfoFont.FontHeight    = InfoSize;


             if (xfull < 1000) 
             {
                 dlgfont.FontHeight = 90;
                 InfoFont.FontHeight    = InfoSize - 30;
                 dlgposfont.FontHeight = 90;
                 ltgrayfont.FontHeight = 90;
                 cpfont.FontHeight = 150;
             }

             if (xfull < 800) 
             {

                 dlgfont.FontHeight = 70;
                 InfoFont.FontHeight    = InfoSize - 50;
                 dlgposfont.FontHeight = 70;
                 ltgrayfont.FontHeight = 70;
                 cpfont.FontHeight = 120;
             }

             fHead.SetFieldanz ();
             fPos.SetFieldanz ();
             fFoot.SetFieldanz ();
             fMeEinh.SetFieldanz ();

             fHead.GetCfield ("mdn_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fHead.GetCfield ("fil_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fHead.GetCfield ("lief_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fHead.GetCfield ("a_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));

             fHead.GetCfield ("mdn_choise")->SetTabstop (FALSE); 
             fHead.GetCfield ("fil_choise")->SetTabstop (FALSE); 
             fHead.GetCfield ("lief_choise")->SetTabstop (FALSE); 
             fHead.GetCfield ("a_choise")->SetTabstop (FALSE); 

             for (i = 0; MeEinhChoiseTab[i] != NULL; i ++)
             {
                   fPos.GetCfield (MeEinhChoiseTab[i])->SetBitmap 
                                (LoadBitmap (hInstance, "ARRDOWN"));
                   fPos.GetCfield (MeEinhChoiseTab[i])->SetTabstop (FALSE); 
             }

             for (i = 0; i < fHead.GetFieldanz (); i ++)
             {
//                       fHead.GetCfield ()[i]->SetFont (&dlgfont); 
//                       fHead.GetCfield ()[i]->SetPosFont (&dlgposfont); 
             }

             fHead.GetCfield ("me_kz")->SetFont (&InfoFont);

             SetTmFont (&dlgfont);
             for (i = 0; Before[i] != NULL; i ++)
             {
                 Before[i]->SetBefore (&fMeEinh);
             }

             for (i = 0; After[i] != NULL; i ++)
             {
                 After[i]->SetAfter (&fMeEinh);
             }


             if (BorderType == HIGHCOLBORDER)
             {

                for (i = 0; Fonts[i] != NULL; i ++)
                {
                     Fonts[i]->SetFont (&fMeEinh);
                }

                fPos.GetCfield ("pos_frame")->SetBorder 
                 (new FILLEDBORDER (BLACKCOL, WHITECOL, HIGH, 0, LTGRAYCOL)); 
             }
             else if (BorderType == LOWCOLBORDER)
             {

                for (i = 0; Fonts[i] != NULL; i ++)
                {
                   Fonts[i]->SetFont (&fMeEinh);
                }
                fPos.GetCfield ("pos_frame")->SetBorder 
                 (new FILLEDBORDER (BLACKCOL, WHITECOL, LOW, 0, LTGRAYCOL)); 
             }
             else if (BorderType == HIGHBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, HIGH); 
             }
             else if (BorderType == LOWBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, LOW); 
             }
             else if (BorderType == RAISEDBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, RAISED); 
             }
             else if (BorderType == LINEBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, LINE); 
             }

             
             if (EnterMode != ENTERHEAD)
             {
                     FillHeadfields ();
                     Enable (&fMeEinh, EnableHead, FALSE); 
                     Enable (&fMeEinh, EnablePos,  TRUE); 
                     SetDialog (&fMeEinh);
             }
             else
             {
                     Enable (&fMeEinh, EnableHead, TRUE); 
                     Enable (&fMeEinh, EnablePos,  FALSE); 
                     fMeEinh.RemoveCfield ("fMeEinh3");
                     SetDialog (&fMeEinh0);
             }
             if (EnterMode == ENTERADATA)
             {
                     SetDialog (&fMeEinh);
             }
             if (ListStyle == LISTBORDER)
             {
                   OrPosAttr (WS_BORDER);
             }
             else if (ListStyle == LISTLOW)
             {
                   OrPosAttrEx (WS_EX_CLIENTEDGE);
             }
             else if (ListStyle == LISTHI)
             {
                   OrPosAttr (WS_DLGFRAME);
             }
}


void MeEinhDlg::OrPosAttr (DWORD Attr)
{
        int i; 

        for (i = 0; EnablePos[i] != NULL; i ++)
        {
            fPos.GetCfield (EnablePos[i])->SetBkMode 
             (fPos.GetCfield (EnablePos[i])->GetBkMode () | Attr);     
        }
}

void MeEinhDlg::OrPosAttrEx (DWORD Attr)
{
        int i; 

        for (i = 0; EnablePos[i] != NULL; i ++)
        {
            fPos.GetCfield (EnablePos[i])->SetBkModeEx 
             (fPos.GetCfield (EnablePos[i])->GetBkModeEx () | Attr);     
        }
}

BOOL MeEinhDlg::FillHeadfields (void)
{
        PROG_CFG ProgCfg ("12900");
        char cfg_v [256];
        char wert [5];
        int dsqlstatus;
        int i;
 
        if (ProgCfg.GetCfgValue ("ListStyle", cfg_v) == TRUE)
        {
                    ListStyle = atoi (cfg_v);
        }
        ProgCfg.CloseCfg ();
        memcpy (&meeinhf, &meeinhf_null, sizeof (struct MEEINHF));
        sprintf (meeinhf.mdn,  "%4d", liefmebest.mdn);  
        sprintf (meeinhf.fil,  "%4d", liefmebest.fil);  
        sprintf (meeinhf.lief, "%s",  liefmebest.lief);  
        sprintf (meeinhf.a,    "%13.0lf", liefmebest.a);  
        dsqlstatus = meEinhWork.ReadA (liefmebest.mdn, liefmebest.fil, meeinhf.lief,
                                       liefmebest.a); 

        sprintf (meeinhf.mdn,  "%4d", liefmebest.mdn);  
        sprintf (meeinhf.fil,  "%4d", liefmebest.fil);  

        lief_bzg.mdn = liefmebest.mdn;
        lief_bzg.fil = liefmebest.fil;
        strcpy (lief_bzg.lief, liefmebest.lief);
        lief_bzg.a   = liefmebest.a;
        if (meEinhWork.TestLiefBzg () == 100) return FALSE;
        if (dsqlstatus != 0)
        {
             meEinhWork.GetLiefDefaults ();
        }
        else
        {
             meEinhWork.TestLiefPr ();
        }
        meEinhWork.GetMdnName (meeinhf.mdn_krz,   atoi (meeinhf.mdn));
        meEinhWork.GetFilName (meeinhf.fil_krz,   atoi (meeinhf.mdn), atoi (meeinhf.fil));
        meEinhWork.GetLiefName (meeinhf.lief_krz, liefmebest.mdn, liefmebest.lief);

        for (i = 0; MeKzCombo[i] != NULL; i ++); 
        if (atoi (lief_bzg.me_kz) < i &&
             atoi (lief_bzg.me_kz) >= 0)
        {
                  strcpy (meeinhf.me_kz, MeKzCombo [atoi (lief_bzg.me_kz)]);
        }
        else
        {
                  strcpy (meeinhf.me_kz, MeKzCombo [0]);
        }
        ToForm (dbheadfields); 
        ToForm (dbfields); 
        for (i = 0; MeEinhTab[i] != NULL; i ++)
        {
               sprintf (wert, "%d", 
                        atoi ((char *) fPos.GetCfield (MeEinhTab[i])->GetFeld ()));
                        meEinhWork.GetPtab (fPos.GetCfield (MeEinhBzTab[i])->GetFeld (), 
                                                    "me_einh_ek", wert);
        }
        sprintf (liefmebest.lief, "%s",  meeinhf.lief);  
        return TRUE;
}


BOOL MeEinhDlg::OnKeyReturn (void)
{
        HWND hWnd;
        char wert [5];
        int i;
        
        syskey = KEYCR;

        hWnd = GetFocus ();
        if (hWnd == fFoot.GetCfield ("cancel")->GethWnd ())
        {
            return OnKey5 ();
        }
        else if (hWnd == fFoot.GetCfield ("ok")->GethWnd ())
        {
            return OnKey12 ();
        }
        else if (hWnd == fHead.GetCfield ("a")->GethWnd ())
        {
            if (ReadA () == -1) return TRUE;
            return EnterPos ();
        }
        else
        {
             for (i = 0; MeEinhTab[i] != NULL; i ++)
             {
                   if (hWnd == fPos.GetCfield (MeEinhTab[i])->GethWnd ())
                   {
                           fPos.GetCfield (MeEinhTab[i])->GetText ();         
                           sprintf (wert, "%d", 
                               atoi ((char *) fPos.GetCfield (MeEinhTab[i])->GetFeld ()));
                           meEinhWork.GetPtab (fPos.GetCfield (MeEinhBzTab[i])->GetFeld (), 
                                                    "me_einh_ek", wert);
                           fPos.GetCfield (MeEinhBzTab[i])->SetText ();         
                   }
             }
        }
        return FALSE;
}

int MeEinhDlg::DeleteRow (void)
{
         return 0;
}


BOOL MeEinhDlg::OnKeyDown (void)
{
        return FALSE;
}


BOOL MeEinhDlg::OnKeyUp (void)
{
        return FALSE;
}


BOOL MeEinhDlg::OnKey2 ()
{
        return TRUE;
}



BOOL MeEinhDlg::OnKeyHome ()
{
        return TRUE;
}

BOOL MeEinhDlg::OnKeyEnd ()
{
        return TRUE;

}
BOOL MeEinhDlg::OnKey1 ()
{
        syskey = KEY1;
        Help ();
        return TRUE;
}

BOOL MeEinhDlg::OnKey3 ()
{
        syskey = KEY3;
        return TRUE;
}

BOOL MeEinhDlg::OnKey4 ()
{
        syskey = KEY4;
        CallInfo ();
        return TRUE;
}

BOOL MeEinhDlg::OnKey5 ()
{
        int i;

        syskey = KEY5;
        if (fHead.GetCfield ("lief")->IsDisabled () == FALSE)
        {
                  DestroyWindow ();
                  return TRUE;
        }
        else if (EnterMode != ENTERHEAD)
        {
                  DestroyWindow ();
                  return TRUE;
        }
        Enable (&fMeEinh, EnableHead, TRUE); 
        if (atoi (meeinhf.mdn) == 0)
        {
             Enable (&fMeEinh, EnableHeadFil, FALSE); 
        }
        Enable (&fMeEinh, EnablePos,  FALSE);
        meEinhWork.RollbackWork ();
        meEinhWork.InitRec ();
        ToForm (dbfields);
        for (i = 0; MeEinhBzTab [i] != 0; i ++)
        {
            strcpy (fPos.GetCfield (MeEinhBzTab [i])->GetFeld (), "");
        }
        fPos.SetText ();
        fHead.SetCurrentName ("lief");
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELETE,   MF_GRAYED);
        ToolBar_SetState (ActiveDlg->GethwndTB (),   IDM_DELETE, TBSTATE_INDETERMINATE);
        if (ActiveMeEinh->GetToolbar2 () != NULL)
        {
                 ((ColButton *)ActiveMeEinh->GetToolbar2 ()->GetCfield ("del")->GetFeld ())->bmp = 
                     ImageDelete.Inactive;
                 ActiveMeEinh->GetToolbar2 ()->display ();
        }
        return TRUE;
}

BOOL MeEinhDlg::OnKey12 ()
{
        int i;
        syskey = KEY12;

        fWork->GetText ();
        FromForm (dbfields); 
        if (EnterMode != ENTERHEAD)
        {
               if (meEinhWork.WriteA () == -1)
               {
                   syskey = KEY5;
               }
               DestroyWindow ();
               return TRUE;
        }
        if (fHead.GetCfield ("lief")->IsDisabled ())
        {
               meEinhWork.WriteA ();
               Enable (&fMeEinh, EnableHead, TRUE); 
               if (atoi (meeinhf.mdn) == 0)
               {
                        Enable (&fMeEinh, EnableHeadFil, FALSE); 
               }
               Enable (&fMeEinh, EnablePos,  FALSE);
               meEinhWork.CommitWork ();
               meEinhWork.InitRec ();
               ToForm (dbfields);
               for (i = 0; MeEinhBzTab [i] != 0; i ++)
               {
                          strcpy (fPos.GetCfield (MeEinhBzTab [i])->GetFeld (), "");
               }
               fPos.SetText ();
               fHead.SetCurrentName ("lief");
               EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELETE,   MF_GRAYED);
               ToolBar_SetState (ActiveDlg->GethwndTB (),   IDM_DELETE, TBSTATE_INDETERMINATE);
               if (ActiveMeEinh->GetToolbar2 () != NULL)
               {
                 ((ColButton *)ActiveMeEinh->GetToolbar2 ()->GetCfield ("del")->GetFeld ())->bmp = 
                     ImageDelete.Inactive;
                 ActiveMeEinh->GetToolbar2 ()->display ();
               }
        }
        return TRUE;
}

BOOL MeEinhDlg::OnKeyDelete ()
{
//        CFIELD *Cfield;  
        int i;

        if (EnterMode != ENTERHEAD) return FALSE;

        if (fHead.GetCfield ("lief")->IsDisabled () == FALSE)
        {
                return FALSE;
        }

        if (EnterMode == ENTERHEAD && 
                abfragejn (hWnd, "Eintrag l�schen ?", "N") == 0)
            {
               SetCurrentFocus ();
               return TRUE;
        }

        meEinhWork.DeleteA ();
        Enable (&fMeEinh, EnableHead, TRUE); 
        if (atoi (meeinhf.mdn) == 0)
        {
               Enable (&fMeEinh, EnableHeadFil, FALSE); 
        }
        Enable (&fMeEinh, EnablePos,  FALSE);
        meEinhWork.CommitWork ();
        meEinhWork.InitRec ();
        ToForm (dbfields);
        for (i = 0; MeEinhBzTab [i] != 0; i ++)
        {
               strcpy (fPos.GetCfield (MeEinhBzTab [i])->GetFeld (), "");
        }
        fPos.SetText ();
        fHead.SetCurrentName ("lief");
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELETE,   MF_GRAYED);
        if (ActiveMeEinh->GetToolbar2 () != NULL)
        {
              ((ColButton *)ActiveMeEinh->GetToolbar2 ()->GetCfield ("del")->GetFeld ())->bmp = 
                     ImageDelete.Inactive;
                 ActiveMeEinh->GetToolbar2 ()->display ();
        }
        return TRUE;
}


BOOL MeEinhDlg::OnKeyPrior ()
{

        if (vTrack != NULL)
        {
             SendMessage (hWnd, WM_VSCROLL, SB_PAGEUP, 0l);
             return TRUE;
        }
        return FALSE;
}

BOOL MeEinhDlg::OnKeyNext ()
{
        if (vTrack != NULL)
        {
             SendMessage (hWnd, WM_VSCROLL, SB_PAGEDOWN, 0l);
             return TRUE;
        }
        return FALSE;
}


BOOL MeEinhDlg::OnKey6 ()
{
        return TRUE;
} 


BOOL MeEinhDlg::OnKey7 ()
{
        return FALSE;
}


BOOL MeEinhDlg::ShowMdn (void)
{
             char mdn_nr [6];
             SEARCHMDN SearchMdn;
 
             SearchMdn.SetParams (DLG::hInstance, ActiveDlg->GethWnd ());
             SearchMdn.Setawin (ActiveDlg->GethMainWindow ());
             SearchMdn.Search ();
             if (SearchMdn.GetKey (mdn_nr) == TRUE)
             {
                 sprintf (meeinhf.mdn, "%4d", atoi (mdn_nr));
                 meEinhWork.GetMdnName (meeinhf.mdn_krz,   atoi (meeinhf.mdn));
                 fHead.SetText ();
                 fHead.SetCurrentName ("mdn");
             }
             return TRUE;
}


BOOL MeEinhDlg::ShowFil (void)
{
             char fil_nr [6];
             SEARCHFIL SearchFil;
 
             if (atoi (meeinhf.mdn) == 0) return TRUE;

             SearchFil.SetParams (DLG::hInstance, ActiveDlg->GethWnd (), 
                                                  atoi (meeinhf.mdn));
             SearchFil.Setawin (ActiveDlg->GethMainWindow ());
             SearchFil.Search ();
             if (SearchFil.GetKey (fil_nr) == TRUE)
             {
                 sprintf (meeinhf.fil, "%4d", atoi (fil_nr));
                 meEinhWork.GetFilName (meeinhf.fil_krz,   atoi (meeinhf.mdn), atoi (meeinhf.fil));
                 fHead.SetText ();
                 fHead.SetCurrentName ("fil");
             }
             return TRUE;
}

BOOL MeEinhDlg::ShowLief (void)
{
             char lief [17];
             SEARCHLIEF SearchLief;
 
             SearchLief.SetParams (DLG::hInstance, ActiveDlg->GethWnd ()); 
             SearchLief.Setawin (ActiveDlg->GethMainWindow ());
                                                  
             SearchLief.Search ();
             if (SearchLief.GetKey (lief) == TRUE)
             {
                 sprintf (meeinhf.lief, "%s", lief);
                 meEinhWork.GetLiefName (meeinhf.lief_krz,  atoi (meeinhf.mdn), meeinhf.lief);
                 fHead.SetText ();
                 fHead.SetCurrentName ("lief");
             }
             return TRUE;
}


int MeEinhDlg::ShowA ()
{
        struct SA *sa;

        SearchA.SetParams (DLG::hInstance, ActiveDlg->GethWnd ());
        SearchA.Setawin (ActiveDlg->GethMainWindow ());
        SearchA.SearchA ();
//        ActiveDlg->Validate ();
        if (syskey == KEY5)
        {
            return 0;
        }
        sa = SearchA.GetSa ();
        if (sa == NULL)
        {
            return 0;
        }
        strcpy (meeinhf.a, sa->a);
        fHead.GetCfield ("a")->SetText ();
        ReadA ();
        return 0;
}

BOOL MeEinhDlg::ShowPtab (char *ptitem, char *item, char *item_bz)
{
             if (meEinhWork.ShowPtabEx (hWnd, (char *) fPos.GetCfield (item)->GetFeld (),
				                        ptitem) == -1)
			 {
				 ActiveDlg->SetCurrentFocus ();
				 return TRUE;
			 }
			 fPos.GetCfield (item)->SetText ();
			 meEinhWork.GetPtab (fPos.GetCfield (item_bz)->GetFeld (),
				                 ptitem,  
					   		     fPos.GetCfield (item)->GetFeld ());
			 fPos.GetCfield (item_bz)->SetText ();
			 return TRUE;
}


BOOL MeEinhDlg::OnKey9 ()
{
         CFIELD *Cfield;
         HWND hWnd;
         int i;

         syskey = KEY9;
         hWnd = GetFocus ();

         if (hWnd == fHead.GetCfield ("mdn_choise")->GethWnd ())
         {
             return ShowMdn ();
         }

         if (hWnd == fHead.GetCfield ("fil_choise")->GethWnd ())
         {
             return ShowFil ();
         }

         if (hWnd == fHead.GetCfield ("lief_choise")->GethWnd ())
         {
             return ShowLief ();
         }

         if (hWnd == fHead.GetCfield ("a_choise")->GethWnd ())
         {
             return ShowA ();
         }
         else 
         {
             for (i = 0; MeEinhChoiseTab[i] != NULL; i ++)
             {
                   if (hWnd == fPos.GetCfield (MeEinhChoiseTab[i])->GethWnd ())
                   {
                           return ShowPtab ("me_einh_ek", MeEinhTab[i], MeEinhBzTab[i]);
                   }
             }
         }


         Cfield = ActiveDlg->GetCurrentCfield ();

         if (strcmp (Cfield->GetName (), "mdn") == 0)
         {
             return ShowMdn ();
         }

         if (strcmp (Cfield->GetName (), "fil") == 0)
         {
             return ShowFil ();
         }

         if (strcmp (Cfield->GetName (), "lief") == 0)
         {
             return ShowLief ();
         }

         if (strcmp (Cfield->GetName (), "a") == 0)
         {
             return ShowA ();
         }
         else 
         {
             for (i = 0; MeEinhChoiseTab[i] != NULL; i ++)
             {
                   if (strcmp (Cfield->GetName (), 
                       fPos.GetCfield (MeEinhTab[i])->GetName ()) == 0)
                   {
                           return ShowPtab ("me_einh_ek", MeEinhTab[i], MeEinhBzTab[i]);
                   }
             }
         }

         return FALSE;
}

BOOL MeEinhDlg::OnKey10 ()
{
         return FALSE; 
}

BOOL MeEinhDlg::OnPaint (HWND hWnd,HDC hdc, UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (fWork && this->hWnd == hWnd)
        {
             PrintMess (MessText);
             fWork->display (hWnd, hdc);
             return TRUE;
        }
        return DLG::OnPaint (hWnd,hdc, msg, wParam,lParam);
}


BOOL MeEinhDlg::OnActivate (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (LOWORD (wParam) == WA_ACTIVE ||
                LOWORD (wParam) == WA_CLICKACTIVE)
        {
              if (WithTabStops)
              {
                  SetTabFocus (); 
              }
              else
              {
                  fWork->SetFocus ();
              }
              return TRUE;
        }
        return FALSE;
}


BOOL MeEinhDlg::OnCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (fWork)
        {

              if (LOWORD (wParam) == CANCEL_CTL)
              {
                  return OnKey5 ();
              }
              if (LOWORD (wParam) == OK_CTL)
              {
                  return OnKey12 ();
              }
              if (LOWORD (wParam) == IDM_CHOISE)
              {
                  return OnKey9 ();
              }

              if (LOWORD (wParam) == IDM_DELETE)
              {
                  return OnKeyDelete ();
              }
              if (HIWORD (wParam) == WM_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              if (HIWORD (wParam) == EN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == CBN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == WM_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == EN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == CBN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == BN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == BN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
        }
        return DLG::OnCommand (hWnd, msg, wParam, lParam);
}


BOOL MeEinhDlg::OnSysCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        
        if (wParam == SC_CLOSE)
        {
            if (EnterMode == ENTERHEAD && 
                abfragejn (hWnd, "Verarbeitung abbrechen ?", "N") == 0)
            {
                ActiveDlg->SetCurrentFocus ();
                return TRUE;
            }
            syskey = KEY5;
        }
        return DLG::OnSysCommand (hWnd, msg, wParam, lParam); 
}

BOOL MeEinhDlg::OnDestroy (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{

        if (hWnd == this->hWnd)
        {
             if (NextWindow != NULL)
             {
                     EnableWindows (NextWindow, TRUE);
                     SetWindowPos (NextWindow, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
             }
             PostQuitMessage (0);
             fMeEinh.destroy ();
        }
        return TRUE;
}

void MeEinhDlg::SetWinBackground (COLORREF Col)
{
          dlgfont.FontBkColor = Col;
          cpfont.FontBkColor = Col;
          DLG::SetWinBackground (Col);
}


void MeEinhDlg::ToClipboard (void)
{
          CFIELD *Cfield;
          HGLOBAL hb;
          LPVOID p;
          char *text;

          Cfield = GetCurrentCfield ();
          Cfield->GetText ();
          text = (char *) Cfield->GetFeld ();
          if (text == NULL) return;
          hb = GlobalAlloc (GHND, strlen (text) + 1);
          if (hb == NULL) return;
          p = GlobalLock (hb);
          strcpy ((char *) p, text);
          GlobalUnlock (hb);
          OpenClipboard (hWnd);
          EmptyClipboard ();
          SetClipboardData (CF_TEXT, hb);
          CloseClipboard ();
}

void MeEinhDlg::FromClipboard (void)
{
          CFIELD *Cfield;
          HGLOBAL hb;
          LPVOID p;
          char *text;

          Cfield = GetCurrentCfield ();
          text = (char *) Cfield->GetFeld ();
          if (text == NULL) return;
          if (IsClipboardFormatAvailable (CF_TEXT) == FALSE) return;
          OpenClipboard (hWnd);
          hb = GetClipboardData (CF_TEXT);
          if (hb == NULL)
          {
                 CloseClipboard ();
                 return;
          }
          p = GlobalLock (hb);
          if (strlen ((char *) p) > (unsigned int ) Cfield->GetLength ())
          {
                 GlobalUnlock (hb);
                 CloseClipboard ();
                 return;
          }
          strcpy (text, (char *) p);
          GlobalUnlock (hb);
          CloseClipboard ();
          Cfield->SetText ();
}

void MeEinhDlg::Help (void)
{
          CFIELD *Cfield;
          HELP *Help; 
          char *Item;

          Cfield = GetCurrentCfield ();
          if (Cfield == NULL) return;

          Item = Cfield->GetName ();

          Help = new HELP (HelpName, Item, hInstance,hMainWindow);
          delete Help;
          Cfield->SetFocus ();
}



HWND MeEinhDlg::OpenWindow (HINSTANCE hInstance, HWND hMainWindow)
{
          HWND hWnd;

          Settchar ('|');
          hWnd = DLG::OpenWindow (hInstance, hMainWindow);
          return hWnd;
}

HWND MeEinhDlg::OpenScrollWindow (HINSTANCE hInstance, HWND hMainWindow)
{
          HWND hWnd;

          Settchar ('|');
          hWnd = DLG::OpenScrollWindow (hInstance, hMainWindow);
          return hWnd;
}

void MeEinhDlg::PrintComment (char *Name)
{
          HELP *Help; 
          char Comment [256];

          Help = new HELP (HelpName, Name, hInstance,hMainWindow, INITCOMM);
          Help->GetItem (Name, Comment);
          PrintMess (Comment);
          delete Help;
}

void MeEinhDlg::CallInfo (void)
{
          DLG::CallInfo ();
}







