#include <windows.h>
#include "searchfil.h"
#include "strfkt.h"
#include "mo_wmess.h"
#include "lbox.h"


int SEARCHFIL::idx;
long SEARCHFIL::anz;
CHQEX *SEARCHFIL::Query = NULL;
DB_CLASS SEARCHFIL::DbClass; 
HINSTANCE SEARCHFIL::hMainInst;
HWND SEARCHFIL::hMainWindow;
HWND SEARCHFIL::awin;
short SEARCHFIL::mdn_nr = 0;


int SEARCHFIL::SearchLst (char *sebuff)
/**
Nach Kurzname in Liste suchen.
**/
{
	   Query->SearchList ();
	   return 0;
}

int SEARCHFIL::ReadLst (char *name)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int cursor;
	  int i;
      WMESS Wmess;
      short fil_nr;
      char adr_krz [17];


	  anz = 0;
      clipped (name);
      if (strcmp (name, " ") == 0) name[0] = 0;

      if (strlen (name) == 0 ||
          name[0] <= ' ')
      {
	          sprintf (buffer, "select fil.fil, adr.adr_krz "
	 			       "from fil, adr "
                       "where adr.adr = fil.adr "
                       "and fil.mdn = %hd "
                       "order by fil", mdn_nr);
      }
      else
      {
	          sprintf (buffer, "select fil.fil, adr.adr_krz "
	 			       "from fil,adr "
                       "where adr_krz matches \"%s\" "
                       "and fil.mdn = %hd "
                       "and adr.adr_krz = fil.adr_krz", name, mdn_nr);
      }
      DbClass.sqlout ((short *) &fil_nr, 1, 0);
      DbClass.sqlout ((char *) adr_krz, 0, 17);
      cursor = DbClass.sqlcursor (buffer);
      if (cursor < 0) 
      {
          return -1;
      }
  	  i = 0;
      Wmess.Message (hMainInst, hMainWindow, " \nBitte warten.....\n"
  	                                         "Die Daten werden selektiert.\n ");  
	  while (DbClass.sqlfetch (cursor) == 0)
      {
 	      sprintf (buffer, "    %4d  |%-16s|", fil_nr, adr_krz);
	      Query->InsertRecord (buffer);
		  i ++;
      }
      anz = i;
	  DbClass.sqlclose (cursor);
 	  Wmess.Destroy ();
	  EnableWindow (awin, FALSE);
	  return 0;
}

void SEARCHFIL::SetParams (HINSTANCE hMainInst, HWND hMainWindow, short mdn_nr)
{
      this->hMainInst   = hMainInst;
      this->hMainWindow = hMainWindow;
      awin = hMainWindow;
      this->mdn_nr = mdn_nr;
}

BOOL SEARCHFIL::GetKey (char * key)
{
      int anz;
     
      if (idx == -1) return FALSE;
      anz = wsplit (Key, "|");
      if (anz < 1) return FALSE;
      strcpy (key, wort[0]);
      return TRUE;
}


void SEARCHFIL::Search (void)
{
  	  int cx, cy;
	  char buffer [256];

	  idx = -1;
      cx = 48;
      cy = 25;
      Settchar ('|');
      EnableWindow (hMainWindow, FALSE);
      Query = new CHQEX (cx, cy);
      Query->EnableSort (TRUE);
      Query->OpenWindow (hMainInst, hMainWindow);
      sprintf (buffer, "%s %s",
                          "%d",
                          "%s");
      Query->RowAttr (buffer);
	  sprintf (buffer, "    %6s", "1"); 
	  Query->VLines (buffer, 3);
      sprintf (buffer, "4;4 11;16");
      Query->RowPos (buffer);
	  EnableWindow (awin, FALSE);

	  sprintf (buffer, "    %4s  %-16s", "Fil", "Name"); 
	  Query->InsertCaption (buffer);

	  Query->SetFillDb (ReadLst);
	  Query->SetSearchLst (SearchLst);
      ReadLst ("");
      Query->SetSortRow (1, TRUE);
	  Query->ProcessMessages ();
      EnableWindow (hMainWindow, TRUE);
      if (syskey != KEY5)
      {
              idx = Query->GetSel ();
              if (idx != -1)
              {
                     Query->GetText (Key);
              }
      }
	  EnableWindow (awin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (awin);
}

