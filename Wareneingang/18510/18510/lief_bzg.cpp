#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "wmask.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "a_kun.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "lief_bzg.h"

struct LIEF_BZG lief_bzg, lief_bzg_null;

void LIEF_BZG_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &lief_bzg.mdn, 1, 0);
            ins_quest ((char *) &lief_bzg.fil, 1, 0);
            ins_quest ((char *) &lief_bzg.lief, 0, 17);
            ins_quest ((char *) &lief_bzg.lief_best, 0, 17);
    out_quest ((char *) &lief_bzg.a,3,0);
    out_quest ((char *) lief_bzg.best_txt1,0,25);
    out_quest ((char *) lief_bzg.best_txt2,0,25);
    out_quest ((char *) &lief_bzg.fil,1,0);
    out_quest ((char *) lief_bzg.lief,0,17);
    out_quest ((char *) lief_bzg.lief_best,0,17);
    out_quest ((char *) lief_bzg.lief_kz,0,2);
    out_quest ((char *) lief_bzg.lief_rht,0,2);
    out_quest ((char *) &lief_bzg.lief_s,2,0);
    out_quest ((char *) &lief_bzg.lief_zeit,1,0);
    out_quest ((char *) &lief_bzg.me_einh_ek,1,0);
    out_quest ((char *) &lief_bzg.mdn,1,0);
    out_quest ((char *) &lief_bzg.min_best,3,0);
    out_quest ((char *) &lief_bzg.pr_ek,3,0);
    out_quest ((char *) lief_bzg.me_kz,0,2);
    out_quest ((char *) &lief_bzg.dat,2,0);
    out_quest ((char *) lief_bzg.zeit,0,7);
    out_quest ((char *) lief_bzg.modif,0,2);
    out_quest ((char *) &lief_bzg.pr_ek_eur,3,0);
    out_quest ((char *) &lief_bzg.pr_ek_sa,3,0);
    out_quest ((char *) &lief_bzg.pr_ek_sa_eur,3,0);
    out_quest ((char *) lief_bzg.freifeld1,0,37);
    out_quest ((char *) lief_bzg.ean,0,17);	
 // 200114
	out_quest ((char *) &lief_bzg.vme,3,0);
    out_quest ((char *) &lief_bzg.vme1,3,0);
    out_quest ((char *) &lief_bzg.vme2,3,0);
    out_quest ((char *) &lief_bzg.vjahr,1,0);
    out_quest ((char *) &lief_bzg.vjahr1,1,0);
    out_quest ((char *) &lief_bzg.vjahr2,1,0);
    out_quest ((char *) &lief_bzg.vme_einh,1,0);

	
	cursor = prepare_sql ("select lief_bzg.a,  "
"lief_bzg.best_txt1,  lief_bzg.best_txt2,  lief_bzg.fil,  "
"lief_bzg.lief,  lief_bzg.lief_best,  lief_bzg.lief_kz,  "
"lief_bzg.lief_rht,  lief_bzg.lief_s,  lief_bzg.lief_zeit,  "
"lief_bzg.me_einh_ek,  lief_bzg.mdn,  lief_bzg.min_best,  "
"lief_bzg.pr_ek,  lief_bzg.me_kz,  lief_bzg.dat,  lief_bzg.zeit,  "
"lief_bzg.modif,  lief_bzg.pr_ek_eur,  lief_bzg.pr_ek_sa,  "
"lief_bzg.pr_ek_sa_eur,  lief_bzg.freifeld1 , lief_bzg.ean "

",lief_bzg.vme,  lief_bzg.vme1 , lief_bzg.vme2 "
",lief_bzg.vjahr, lief_bzg.vjahr1, lief_bzg.vjahr2, lief_bzg.vme_einh "


" from lief_bzg "

#line 29 "lief_bzg.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   lief = ? "
                                  "and lief_best = ?");
            ins_quest ((char *) &lief_bzg.mdn, 1, 0);
            ins_quest ((char *) &lief_bzg.fil, 1, 0);
            ins_quest ((char *) &lief_bzg.lief, 0, 17);
            ins_quest ((char *) &lief_bzg.a, 3, 0);

    out_quest ((char *) &lief_bzg.a,3,0);
    out_quest ((char *) lief_bzg.best_txt1,0,25);
    out_quest ((char *) lief_bzg.best_txt2,0,25);
    out_quest ((char *) &lief_bzg.fil,1,0);
    out_quest ((char *) lief_bzg.lief,0,17);
    out_quest ((char *) lief_bzg.lief_best,0,17);
    out_quest ((char *) lief_bzg.lief_kz,0,2);
    out_quest ((char *) lief_bzg.lief_rht,0,2);
    out_quest ((char *) &lief_bzg.lief_s,2,0);
    out_quest ((char *) &lief_bzg.lief_zeit,1,0);
    out_quest ((char *) &lief_bzg.me_einh_ek,1,0);
    out_quest ((char *) &lief_bzg.mdn,1,0);
    out_quest ((char *) &lief_bzg.min_best,3,0);
    out_quest ((char *) &lief_bzg.pr_ek,3,0);
    out_quest ((char *) lief_bzg.me_kz,0,2);
    out_quest ((char *) &lief_bzg.dat,2,0);
    out_quest ((char *) lief_bzg.zeit,0,7);
    out_quest ((char *) lief_bzg.modif,0,2);
    out_quest ((char *) &lief_bzg.pr_ek_eur,3,0);
    out_quest ((char *) &lief_bzg.pr_ek_sa,3,0);
    out_quest ((char *) &lief_bzg.pr_ek_sa_eur,3,0);
    out_quest ((char *) lief_bzg.freifeld1,0,37);
 
	out_quest ((char *) lief_bzg.ean,0,17);
 // 200114
	out_quest ((char *) &lief_bzg.vme,3,0);
    out_quest ((char *) &lief_bzg.vme1,3,0);
    out_quest ((char *) &lief_bzg.vme2,3,0);
    out_quest ((char *) &lief_bzg.vjahr,1,0);
    out_quest ((char *) &lief_bzg.vjahr1,1,0);
    out_quest ((char *) &lief_bzg.vjahr2,1,0);
    out_quest ((char *) &lief_bzg.vme_einh,1,0);

	cursor_a = prepare_sql ("select lief_bzg.a,  "
"lief_bzg.best_txt1,  lief_bzg.best_txt2,  lief_bzg.fil,  "
"lief_bzg.lief,  lief_bzg.lief_best,  lief_bzg.lief_kz,  "
"lief_bzg.lief_rht,  lief_bzg.lief_s,  lief_bzg.lief_zeit,  "
"lief_bzg.me_einh_ek,  lief_bzg.mdn,  lief_bzg.min_best,  "
"lief_bzg.pr_ek,  lief_bzg.me_kz,  lief_bzg.dat,  lief_bzg.zeit,  "
"lief_bzg.modif,  lief_bzg.pr_ek_eur,  lief_bzg.pr_ek_sa,  "
"lief_bzg.pr_ek_sa_eur,  lief_bzg.freifeld1, lief_bzg.ean "

",lief_bzg.vme,  lief_bzg.vme1 , lief_bzg.vme2 "
",lief_bzg.vjahr, lief_bzg.vjahr1, lief_bzg.vjahr2, lief_bzg.vme_einh "

"from lief_bzg "

#line 38 "lief_bzg.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   lief = ? "
                                  "and   a = ? order by lief_best");


    ins_quest ((char *) &lief_bzg.a,3,0);
    ins_quest ((char *) lief_bzg.best_txt1,0,25);
    ins_quest ((char *) lief_bzg.best_txt2,0,25);
    ins_quest ((char *) &lief_bzg.fil,1,0);
    ins_quest ((char *) lief_bzg.lief,0,17);
    ins_quest ((char *) lief_bzg.lief_best,0,17);
    ins_quest ((char *) lief_bzg.lief_kz,0,2);
    ins_quest ((char *) lief_bzg.lief_rht,0,2);
    ins_quest ((char *) &lief_bzg.lief_s,2,0);
    ins_quest ((char *) &lief_bzg.lief_zeit,1,0);
    ins_quest ((char *) &lief_bzg.me_einh_ek,1,0);
    ins_quest ((char *) &lief_bzg.mdn,1,0);
    ins_quest ((char *) &lief_bzg.min_best,3,0);
    ins_quest ((char *) &lief_bzg.pr_ek,3,0);
    ins_quest ((char *) lief_bzg.me_kz,0,2);
    ins_quest ((char *) &lief_bzg.dat,2,0);
    ins_quest ((char *) lief_bzg.zeit,0,7);
    ins_quest ((char *) lief_bzg.modif,0,2);
    ins_quest ((char *) &lief_bzg.pr_ek_eur,3,0);
    ins_quest ((char *) &lief_bzg.pr_ek_sa,3,0);
    ins_quest ((char *) &lief_bzg.pr_ek_sa_eur,3,0);
    ins_quest ((char *) lief_bzg.freifeld1,0,37);
    ins_quest ((char *) lief_bzg.ean, 0,17);	// GrJ

 // 200114
	ins_quest ((char *) &lief_bzg.vme,3,0);
    ins_quest ((char *) &lief_bzg.vme1,3,0);
    ins_quest ((char *) &lief_bzg.vme2,3,0);
    ins_quest ((char *) &lief_bzg.vjahr,1,0);
    ins_quest ((char *) &lief_bzg.vjahr1,1,0);
    ins_quest ((char *) &lief_bzg.vjahr2,1,0);
    ins_quest ((char *) &lief_bzg.vme_einh,1,0);
	
	
	sqltext = "update lief_bzg set lief_bzg.a = ?,  "
"lief_bzg.best_txt1 = ?,  lief_bzg.best_txt2 = ?,  lief_bzg.fil = ?,  "
"lief_bzg.lief = ?,  lief_bzg.lief_best = ?,  lief_bzg.lief_kz = ?,  "
"lief_bzg.lief_rht = ?,  lief_bzg.lief_s = ?,  "
"lief_bzg.lief_zeit = ?,  lief_bzg.me_einh_ek = ?,  "
"lief_bzg.mdn = ?,  lief_bzg.min_best = ?,  lief_bzg.pr_ek = ?,  "
"lief_bzg.me_kz = ?,  lief_bzg.dat = ?,  lief_bzg.zeit = ?,  "
"lief_bzg.modif = ?,  lief_bzg.pr_ek_eur = ?,  "
"lief_bzg.pr_ek_sa = ?,  lief_bzg.pr_ek_sa_eur = ?,  "
"lief_bzg.freifeld1 = ?, lief_bzg.ean = ? "

",lief_bzg.vme = ? ,  lief_bzg.vme1 = ? , lief_bzg.vme2 = ? "
",lief_bzg.vjahr = ? , lief_bzg.vjahr1 = ? , lief_bzg.vjahr2 = ? , lief_bzg.vme_einh = ? "


#line 45 "lief_bzg.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   lief = ? "
                                  "and lief_best = ?";

            ins_quest ((char *) &lief_bzg.mdn, 1, 0);
            ins_quest ((char *) &lief_bzg.fil, 1, 0);
            ins_quest ((char *) &lief_bzg.lief, 0, 17);
            ins_quest ((char *) &lief_bzg.lief_best, 0, 17);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &lief_bzg.mdn, 1, 0);
            ins_quest ((char *) &lief_bzg.fil, 1, 0);
            ins_quest ((char *) &lief_bzg.lief, 0, 17);
            ins_quest ((char *) &lief_bzg.lief_best, 0, 17);
            test_upd_cursor = prepare_sql ("select a from lief_bzg "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   lief = ? "
                                  "and lief_best = ?");
            ins_quest ((char *) &lief_bzg.mdn, 1, 0);
            ins_quest ((char *) &lief_bzg.fil, 1, 0);
            ins_quest ((char *) &lief_bzg.lief, 0, 17);
            ins_quest ((char *) &lief_bzg.lief_best, 0, 17);
            del_cursor = prepare_sql ("delete from lief_bzg "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   lief = ? "
                                  "and lief_best = ?");
    ins_quest ((char *) &lief_bzg.a,3,0);
    ins_quest ((char *) lief_bzg.best_txt1,0,25);
    ins_quest ((char *) lief_bzg.best_txt2,0,25);
    ins_quest ((char *) &lief_bzg.fil,1,0);
    ins_quest ((char *) lief_bzg.lief,0,17);
    ins_quest ((char *) lief_bzg.lief_best,0,17);
    ins_quest ((char *) lief_bzg.lief_kz,0,2);
    ins_quest ((char *) lief_bzg.lief_rht,0,2);
    ins_quest ((char *) &lief_bzg.lief_s,2,0);
    ins_quest ((char *) &lief_bzg.lief_zeit,1,0);
    ins_quest ((char *) &lief_bzg.me_einh_ek,1,0);
    ins_quest ((char *) &lief_bzg.mdn,1,0);
    ins_quest ((char *) &lief_bzg.min_best,3,0);
    ins_quest ((char *) &lief_bzg.pr_ek,3,0);
    ins_quest ((char *) lief_bzg.me_kz,0,2);
    ins_quest ((char *) &lief_bzg.dat,2,0);
    ins_quest ((char *) lief_bzg.zeit,0,7);
    ins_quest ((char *) lief_bzg.modif,0,2);
    ins_quest ((char *) &lief_bzg.pr_ek_eur,3,0);
    ins_quest ((char *) &lief_bzg.pr_ek_sa,3,0);
    ins_quest ((char *) &lief_bzg.pr_ek_sa_eur,3,0);
    ins_quest ((char *) lief_bzg.freifeld1,0,37);
    ins_quest ((char *) lief_bzg.ean,0,17);	

// 200114
	ins_quest ((char *) &lief_bzg.vme,3,0);
    ins_quest ((char *) &lief_bzg.vme1,3,0);
    ins_quest ((char *) &lief_bzg.vme2,3,0);
    ins_quest ((char *) &lief_bzg.vjahr,1,0);
    ins_quest ((char *) &lief_bzg.vjahr1,1,0);
    ins_quest ((char *) &lief_bzg.vjahr2,1,0);
    ins_quest ((char *) &lief_bzg.vme_einh,1,0);


            ins_cursor = prepare_sql ("insert into lief_bzg ("
"a, best_txt1, best_txt2, fil, lief, lief_best, lief_kz, lief_rht, lief_s, "
"lief_zeit, me_einh_ek, mdn, min_best, pr_ek, me_kz, dat, zeit, modif, "
" pr_ek_eur,  pr_ek_sa,  pr_ek_sa_eur,  freifeld1, ean " 
",vme, vme1, vme2, vjahr, vjahr1, vjahr2, vme_einh " 

" ) "

#line 75 "lief_bzg.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?, ? "
",?,?,?,?,?,?,? "
")");

#line 77 "lief_bzg.rpp"
}
int LIEF_BZG_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int LIEF_BZG_CLASS::dbreadfirsta (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor_a == -1)
         {
                this->prepare ();
         }
         open_sql (cursor_a);
         fetch_sql (cursor_a);
         return sqlstatus;
}

int LIEF_BZG_CLASS::dbreada (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         fetch_sql (cursor_a);
         return sqlstatus;
}
