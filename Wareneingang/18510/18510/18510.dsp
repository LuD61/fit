# Microsoft Developer Studio Project File - Name="18510" - Package Owner=<4>
# Von Microsoft Developer Studio generierte Erstellungsdatei, Format Version 5.00
# ** NICHT BEARBEITEN **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=18510 - Win32 Debug
!MESSAGE Dies ist kein g�ltiges Makefile. Zum Erstellen dieses Projekts mit\
 NMAKE
!MESSAGE verwenden Sie den Befehl "Makefile exportieren" und f�hren Sie den\
 Befehl
!MESSAGE 
!MESSAGE NMAKE /f "18510.mak".
!MESSAGE 
!MESSAGE Sie k�nnen beim Ausf�hren von NMAKE eine Konfiguration angeben
!MESSAGE durch Definieren des Makros CFG in der Befehlszeile. Zum Beispiel:
!MESSAGE 
!MESSAGE NMAKE /f "18510.mak" CFG="18510 - Win32 Debug"
!MESSAGE 
!MESSAGE F�r die Konfiguration stehen zur Auswahl:
!MESSAGE 
!MESSAGE "18510 - Win32 Release" (basierend auf  "Win32 (x86) Application")
!MESSAGE "18510 - Win32 Debug" (basierend auf  "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "18510 - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /W3 /GX /O2 /I "d:\informix\incl\esql" /I "..\include" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o NUL /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o NUL /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib comctl32.lib isqlt07c.lib /nologo /subsystem:windows /machine:I386 /out:"d:\user\fit\bin\18510.exe" /libpath:"d:\informix\lib"

!ELSEIF  "$(CFG)" == "18510 - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "18510___"
# PROP BASE Intermediate_Dir "18510___"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "DEBUG"
# PROP Intermediate_Dir "DEBUG"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /W3 /Gm /GX /Zi /Od /I "d:\informix\incl\esql" /I "..\include" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /FR /YX /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o NUL /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o NUL /win32
# ADD BASE RSC /l 0x407 /d "_DEBUG"
# ADD RSC /l 0x407 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib comctl32.lib isqlt07c.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept /libpath:"d:\informix\lib"

!ENDIF 

# Begin Target

# Name "18510 - Win32 Release"
# Name "18510 - Win32 Debug"
# Begin Source File

SOURCE=.\18510.cpp
# End Source File
# Begin Source File

SOURCE=.\18510.rc

!IF  "$(CFG)" == "18510 - Win32 Release"

!ELSEIF  "$(CFG)" == "18510 - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\libsrc\A_BAS.CPP
# End Source File
# Begin Source File

SOURCE=.\a_best.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\cmask.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\colbut.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\dbclass.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\DBFUNC.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\dlg.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\enterfunc.cpp
# End Source File
# Begin Source File

SOURCE=.\f5.bmp
# End Source File
# Begin Source File

SOURCE=..\libsrc\FIL.CPP
# End Source File
# Begin Source File

SOURCE=.\fit0.ico
# End Source File
# Begin Source File

SOURCE=.\fit01.ico
# End Source File
# Begin Source File

SOURCE=.\fit02.ico
# End Source File
# Begin Source File

SOURCE=..\libsrc\FORMFELD.CPP
# End Source File
# Begin Source File

SOURCE=..\lib\gprintl.lib
# End Source File
# Begin Source File

SOURCE=..\libsrc\help.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\image.cpp
# End Source File
# Begin Source File

SOURCE=..\lib\inflib.lib
# End Source File
# Begin Source File

SOURCE=.\IprDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\IprDialog.h
# End Source File
# Begin Source File

SOURCE=..\libsrc\itemc.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\lbox.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\lief.cpp
# End Source File
# Begin Source File

SOURCE=.\lief_bzg.cpp
# End Source File
# Begin Source File

SOURCE=.\lief_bzgn.cpp
# End Source File
# Begin Source File

SOURCE=.\LiefBzgDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\LiefBzgWork.cpp
# End Source File
# Begin Source File

SOURCE=.\liefmebest.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\MDN.CPP
# End Source File
# Begin Source File

SOURCE=.\MeEinhDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\MeEinhWork.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\message.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\mo_arg.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\mo_chqex.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\MO_CURSO.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\MO_DRAW.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\mo_enter.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\mo_inf.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\MO_MELD.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\MO_MENU.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\mo_progcfg.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\mo_vers.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\mo_wmess.cpp
# End Source File
# Begin Source File

SOURCE=.\oarrow.cur
# End Source File
# Begin Source File

SOURCE=..\libsrc\PTAB.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\RBrush.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\rdonly.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\RFont.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\RPen.cpp
# End Source File
# Begin Source File

SOURCE=.\searcha.cpp
# End Source File
# Begin Source File

SOURCE=.\searchfil.cpp
# End Source File
# Begin Source File

SOURCE=.\searchlief.cpp
# End Source File
# Begin Source File

SOURCE=.\searchmdn.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\searchptab.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\STDFKT.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\STRFKT.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\Text.cpp
# End Source File
# Begin Source File

SOURCE=.\textdlg.cpp
# End Source File
# Begin Source File

SOURCE=.\textwork.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\VBrush.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\Vector.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\VFont.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\VPen.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\wmaskc.cpp
# End Source File
# End Target
# End Project
