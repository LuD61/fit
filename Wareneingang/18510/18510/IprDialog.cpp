// IprDialog.cpp: Implementierung der Klasse CIprDialog.
//
//////////////////////////////////////////////////////////////////////

#include "IprDialog.h"
#include "Text.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

CIprDialog::CIprDialog()
{
	Text IprModul;
	char *bws = getenv ("BWS");
	if (bws != NULL)
	{
		IprModul = bws;
		IprModul += "\\bin\\";
	}
	else
	{
		IprModul = "";
	}
	IprModul += "IprModul.dll";
    IprDlg = NULL;
    dOpenDbase = NULL; 
	IprDialog = LoadLibrary (IprModul.GetBuffer ());
	if (IprDialog != NULL)
	{
			IprDlg = (void (*) (char *params[]))
					  GetProcAddress ((HMODULE) IprDialog, "Start");
			dOpenDbase = (BOOL (*) (LPSTR))
					  GetProcAddress ((HMODULE) IprDialog, "OpenDbase");
	}
}

CIprDialog::~CIprDialog()
{

}

void CIprDialog::Start(short mdn, double a, double ek)
{
	if (IprDlg == NULL) return;

	char *params[4];
	Text cMdn;
	Text cA;
	Text cEk;
	cMdn.Format ("mdn = %hd", mdn);
	cA.Format ("a=%.0lf", a);
	cEk.Format ("ek=%.4lf", ek);
	params[0] = cMdn.GetBuffer ();
	params[1] = cA.GetBuffer ();
	params[2] = cEk.GetBuffer ();
	params[3] = NULL;
	(*IprDlg) (params);
}


