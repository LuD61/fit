#include <windows.h>
#include <stdio.h>
#include "LiefBzgWork.h"
#include "a_bas.h"
#include "strfkt.h"
#include "mo_curso.h"


const char *LiefBzgWork::ErrorMsg[] = {"OK",
									   "Die Lieferantenbestellnummer ist schon einem\n"
									   "anderen Artikel zugeorndet",
									   "Die Position kann nicht gespeichert werden!!\n"
									   "Sie wird wahrscheinlich von einem anderen Benutzer gesperrt"
}; 

int LiefBzgWork::ReadLief (short mdn, short fil, char *lief_nr)
{
    int dsqlstatus;

    if (liefbzg_cursor != -1)
    {
        LiefBzg.sqlclose (liefbzg_cursor);
    }

    LiefBzg.sqlin ((short *) &lief_bzg.mdn, 1, 0);
    LiefBzg.sqlin ((short *) &lief_bzg.fil, 1, 0);
    LiefBzg.sqlin ((char *)  lief_bzg.lief, 0, 17);
    LiefBzg.sqlout ((double *)  &lief_bzg.a, 3, 0);
    LiefBzg.sqlout ((char *)  lief_bzg.lief_best, 0, 17);
    liefbzg_cursor = LiefBzg.sqlcursor ("select a,lief_best from lief_bzg "
                                        "where mdn = ? "
                                        "and fil = ? "
                                        "and lief = ? "
                                        "order by a,lief_best");
    dsqlstatus = LiefBzg.sqlfetch (liefbzg_cursor);
    if (dsqlstatus == 0)
    {
        return ReadA ();
    }

    return dsqlstatus;
}

int LiefBzgWork::ReadLief (void)
{
    int dsqlstatus;

    if (liefbzg_cursor == -1)
    {
        return 100;
    }
    dsqlstatus = LiefBzg.sqlfetch (liefbzg_cursor);
    if (dsqlstatus == 0)
    {
        return ReadA ();
    }
    return dsqlstatus;
}

int LiefBzgWork::ReadBzgn (void)
{
	memcpy (&lief_bzgn, &lief_bzgn_null, sizeof (lief_bzgn));
	lief_bzgn.mdn = lief_bzg.mdn;
	lief_bzgn.fil = lief_bzg.fil;
	lief_bzgn.a   = lief_bzg.a;
	strcpy (lief_bzgn.lief, lief_bzg.lief);
	strcpy (lief_bzgn.lief_best, lief_bzg.lief_best);
//	lief_bzgn.dat = dasc_to_long (Termin);
	int dsqlstatus = LiefBzgn.dbreadfirst_best ();
	lief_bzg.pr_ek_sa_eur = lief_bzgn.pr_ek_eur;
	return dsqlstatus;
}

int LiefBzgWork::ReadA (void)
{
    int dsqlstatus;

    memcpy (&_a_bas, &_a_bas_null, sizeof (struct A_BAS));
    lese_a_bas (lief_bzg.a);

    dsqlstatus = LiefBzg.dbreadfirst ();
    if (dsqlstatus == 0)
    {
        if (IsDoublenull (lief_bzg.pr_ek_sa))
        {
            lief_bzg.pr_ek_sa = 0.0;
            lief_bzg.pr_ek_sa_eur = 0.0;
        }
        if (IsDoublenull (lief_bzg.pr_ek_sa_eur))
        {
            lief_bzg.pr_ek_sa = 0.0;
            lief_bzg.pr_ek_sa_eur = 0.0;
        }
    }
    if (dsqlstatus == 0)
    {
        ReadBzgn ();
    }
    if (dsqlstatus == 0)
    {
        ReadTxt ();
    }
    return dsqlstatus;
}


int LiefBzgWork::ReadA (short mdn, short fil, char *lief_nr, double a, char *lief_best)
{
    int dsqlstatus;

	char datumsthilfe[12];
	short datumhilfe;
	sysdate(datumsthilfe);
	datumhilfe = atoi( datumsthilfe + 6 );


    memcpy (&_a_bas, &_a_bas_null, sizeof (struct A_BAS));
    memcpy (&lief_bzg, &lief_bzg_null, sizeof (struct LIEF_BZG));
    lese_a_bas (a);

    lief_bzg.mdn = mdn;
    lief_bzg.fil = fil;
    strcpy (lief_bzg.lief, lief_nr);
    strcpy (lief_bzg.lief_best, lief_best);
    dsqlstatus = LiefBzg.dbreadfirst ();
    while (dsqlstatus == 100)
    {
        if (lief_bzg.fil > 0)
        {
            lief_bzg.fil = 0;
        }
        else if (lief_bzg.mdn > 0)
        {
            lief_bzg.mdn = 0;
        }
        else
        {
            break;
        }
        dsqlstatus = LiefBzg.dbreadfirst ();
    }
    if (dsqlstatus == 100)
    {
        strcpy (lief_bzg.best_txt1, _a_bas.a_bz1);
        strcpy (lief_bzg.best_txt2, _a_bas.a_bz2);
		sprintf ( lief_bzg.ean ,"                ") ;
		lief_bzg.vme = lief_bzg.vme1 = lief_bzg.vme2 = 0.0;
		lief_bzg.vjahr = datumhilfe;
		lief_bzg.vjahr1 = lief_bzg.vjahr - 1 ;
		lief_bzg.vjahr2 = lief_bzg.vjahr1 - 1 ;

        GetLiefDefaults ();
		lief_bzg.a = a;
    }
    else
    {
        if (IsDoublenull (lief_bzg.pr_ek_sa))
        {
            lief_bzg.pr_ek_sa = 0.0;
            lief_bzg.pr_ek_sa_eur = 0.0;
        }
        if (IsDoublenull (lief_bzg.pr_ek_sa_eur))
        {
            lief_bzg.pr_ek_sa = 0.0;
            lief_bzg.pr_ek_sa_eur = 0.0;
        }
    }
    if (dsqlstatus == 0)

    {
		
		if ( lief_bzg.vjahr < 2000 || lief_bzg.vjahr > 2050 )
		{
			lief_bzg.vjahr = datumhilfe;
			lief_bzg.vjahr1 = lief_bzg.vjahr - 1;
			lief_bzg.vjahr2 = lief_bzg.vjahr1 - 1;
			lief_bzg.vme = lief_bzg.vme1 = lief_bzg.vme2 = 0.0;
		}
		else	// gegebnenfalls weiterschieben 
		{
			if ( lief_bzg.vjahr < datumhilfe )
			{	// bitte schieben
				if ( (lief_bzg.vjahr2 < (datumhilfe - 2 )) && ( lief_bzg.vjahr2 < lief_bzg.vjahr1))
				{ 
					lief_bzg.vjahr2 = lief_bzg.vjahr1;
					lief_bzg.vme2 = lief_bzg.vme1;
					if ( (lief_bzg.vjahr1 < (datumhilfe - 1 )) && ( lief_bzg.vjahr1 < lief_bzg.vjahr))
					{ 
						lief_bzg.vjahr1 = lief_bzg.vjahr;
						lief_bzg.vme1 = lief_bzg.vme;
					}
				
				}
				lief_bzg.vjahr = datumhilfe; lief_bzg.vme = 0.0;
			}

		}

	}
    if (dsqlstatus == 0)
    {
        ReadBzgn ();
    }
    if (dsqlstatus == 0)
    {
        ReadTxt ();
    }
    return dsqlstatus;
}

int LiefBzgWork::ReadArt (short mdn, short fil, char *lief_nr, double a)
{
    int dsqlstatus;
	char datumsthilfe[12];
	short datumhilfe;
	sysdate(datumsthilfe);
	datumhilfe = atoi( datumsthilfe + 6 );

    memcpy (&_a_bas, &_a_bas_null, sizeof (struct A_BAS));
    memcpy (&lief_bzg, &lief_bzg_null, sizeof (struct LIEF_BZG));
    lese_a_bas (a);

    lief_bzg.mdn = mdn;
    lief_bzg.fil = fil;
    strcpy (lief_bzg.lief, lief_nr);
	lief_bzg.a = a;
    dsqlstatus = LiefBzg.dbreadfirsta ();
    while (dsqlstatus == 100)
    {
        if (lief_bzg.fil > 0)
        {
            lief_bzg.fil = 0;
        }
        else if (lief_bzg.mdn > 0)
        {
            lief_bzg.mdn = 0;
        }
        else
        {
            break;
        }
        dsqlstatus = LiefBzg.dbreadfirsta ();
    }
    if (dsqlstatus == 100)
    {
        strcpy (lief_bzg.best_txt1, _a_bas.a_bz1);
        strcpy (lief_bzg.best_txt2, _a_bas.a_bz2);
		sprintf ( lief_bzg.ean ," ") ;
		lief_bzg.vme = lief_bzg.vme1 = lief_bzg.vme2 = 0.0;
		lief_bzg.vjahr = datumhilfe;
		lief_bzg.vjahr1 = lief_bzg.vjahr - 1 ;
		lief_bzg.vjahr2 = lief_bzg.vjahr1 - 1 ;

        GetLiefDefaults ();
        sprintf (lief_bzg.lief_best, "%.0lf", a);
    }
    else
    {
        if (IsDoublenull (lief_bzg.pr_ek_sa))
        {
            lief_bzg.pr_ek_sa = 0.0;
            lief_bzg.pr_ek_sa_eur = 0.0;
        }
        if (IsDoublenull (lief_bzg.pr_ek_sa_eur))
        {
            lief_bzg.pr_ek_sa = 0.0;
            lief_bzg.pr_ek_sa_eur = 0.0;
        }
    }
	if (dsqlstatus == 0)
    {
		if ( lief_bzg.vjahr < 2000 || lief_bzg.vjahr > 2050 )
		{
			lief_bzg.vjahr = datumhilfe;
			lief_bzg.vjahr1 = lief_bzg.vjahr - 1;
			lief_bzg.vjahr2 = lief_bzg.vjahr1 - 1;
			lief_bzg.vme = lief_bzg.vme1 = lief_bzg.vme2 = 0.0;
		}
		else	// gegebnenfalls weiterschieben 
		{
			if ( lief_bzg.vjahr < datumhilfe )
			{	// bitte schieben
				if ( (lief_bzg.vjahr2 < (datumhilfe - 2 )) && ( lief_bzg.vjahr2 < lief_bzg.vjahr1))
				{ 
					lief_bzg.vjahr2 = lief_bzg.vjahr1;
					lief_bzg.vme2 = lief_bzg.vme1;
					if ( (lief_bzg.vjahr1 < (datumhilfe - 1 )) && ( lief_bzg.vjahr1 < lief_bzg.vjahr))
					{ 
						lief_bzg.vjahr1 = lief_bzg.vjahr;
						lief_bzg.vme1 = lief_bzg.vme;
					}
				
				}
				lief_bzg.vjahr = datumhilfe; lief_bzg.vme = 0.0;
			}

		}
	}
  
	if (dsqlstatus == 0)
    {
        ReadBzgn ();
    }
    if (dsqlstatus == 0)
    {
        ReadTxt ();
    }
    return dsqlstatus;
}


int LiefBzgWork::GetLiefDefaults (void)
{
    int dsqlstatus;

    LiefBzg.sqlin ((short *) &lief_bzg.mdn, 1, 0); 
    LiefBzg.sqlin ((short *) &lief_bzg.fil, 1, 0); 
    LiefBzg.sqlin ((char *)  lief_bzg.lief, 0, 17);
    LiefBzg.sqlout ((short *) &lief_bzg.lief_zeit, 1, 0); 
    LiefBzg.sqlout ((char *)  lief_bzg.lief_rht, 0, 2);
    dsqlstatus = LiefBzg.sqlcomm ("select lief_zeit, lief_rht from lief where mdn = ? "
                                                          "and fil = ? "
                                                          "and lief = ?");
    strcpy  (lief_bzg.lief_kz, default_lief_kz);
    if (lief_bzg.lief_rht[0] <= ' ')
    {
               strcpy  (lief_bzg.lief_rht, "M");
    }
//    sprintf (lief_bzg.lief_best, "%.0lf", lief_bzg.a); ZUTUN
/*
    LiefBzg.sqlin ((double *)  &lief_bzg.a, 3, 0);
    if (LiefBzg.sqlcomm ("select a from a_best where a = ?") == 0)
    {
              strcpy (lief_bzg.lief_kz, "J");
    }
*/
    lief_bzg.min_best = 1;
    lief_bzg.me_einh_ek = 1;
    return dsqlstatus; 
}

BOOL LiefBzgWork::LiefmebestExist (void)
{
    int dsqlstatus;

    liefmebest.mdn = lief_bzg.mdn;
    liefmebest.fil = lief_bzg.fil;
    strcpy (liefmebest.lief, lief_bzg.lief);
    liefmebest.a   = lief_bzg.a;
    dsqlstatus = LiefMeBest.dbreadfirst ();
    return dsqlstatus == 100 ? FALSE : TRUE;
}

void LiefBzgWork::WriteLiefmebest (char *me_kz)
{
	extern short sql_mode;
    int dsqlstatus;
    sql_mode = 1;


    liefmebest.mdn = lief_bzg.mdn;
    liefmebest.fil = lief_bzg.fil;
    strcpy (liefmebest.lief, lief_bzg.lief);
    liefmebest.a   = lief_bzg.a;
    dsqlstatus = LiefMeBest.dbreadfirst ();
    if (dsqlstatus != 0) 
    {
        strcpy (old_me_kz, me_kz); 
		sql_mode = 0;
        return;
    }

    if (atoi (me_kz) != atoi (old_me_kz))
    {
        liefmebest.pr_ek0 = 0.0;
        liefmebest.pr_ek1 = 0.0;
        liefmebest.pr_ek2 = 0.0;
        liefmebest.pr_ek3 = 0.0;
        liefmebest.pr_ek4 = 0.0;
        liefmebest.pr_ek5 = 0.0;
        liefmebest.pr_ek6 = 0.0;
    }

    if (atoi (me_kz) == 0)
    {
        liefmebest.pr_ek1 = lief_bzg.pr_ek;
    }
    else if (atoi (me_kz) == 1)
    {
        liefmebest.pr_ek0 = lief_bzg.pr_ek;
    }
    liefmebest.me_einh1 = lief_bzg.me_einh_ek;
    dsqlstatus = LiefMeBest.dbupdate ();
	sql_mode = 0;
	if (dsqlstatus != 0)
	{
		return;
	}
    strcpy (old_me_kz, me_kz); 
}

int LiefBzgWork::WriteBzgn (void)
{
	extern short sql_mode;
	sql_mode = 1;
	if (!TerminEnter)
    {
		return 0;
	}
	if (lief_bzgn.pr_ek_eur == 0.0)
	{
		return 0;
	}

	lief_bzgn.mdn = lief_bzg.mdn;
	lief_bzgn.fil = lief_bzg.fil;
	lief_bzgn.a   = lief_bzg.a;
	strcpy (lief_bzgn.lief, lief_bzg.lief);
	strcpy (lief_bzgn.lief_best, lief_bzg.lief_best);
	lief_bzgn.dat, dasc_to_long (Termin);
	lief_bzgn.dat = dasc_to_long (Termin);
	int dsqlstatus = LiefBzgn.dbupdate ();
	sql_mode = 0;
	if (dsqlstatus != 0)
	{
		error = RecordIsLocked;
		return -1;
	}
	return 0;
}

int LiefBzgWork::WriteA (void)
{
    int dsqlstatus;
	extern short sql_mode;
    A_BEST_CLASS ABest;

	sql_mode = 1;
    LiefBzg.sqlin ((short *)  &lief_bzg.mdn, 1, 0);
    LiefBzg.sqlin ((short *)  &lief_bzg.fil, 1, 0);
    LiefBzg.sqlin ((char *)   lief_bzg.lief, 0, 17);
    LiefBzg.sqlin ((char *)   lief_bzg.lief_best, 0, 17);
    LiefBzg.sqlin ((double *) &lief_bzg.a, 3, 0);
    dsqlstatus = LiefBzg.sqlcomm ("select a from lief_bzg "
                                  "where mdn = ? "
                                  "and fil = ? "
                                  "and lief = ? "
                                  "and lief_best = ? "
                                  "and a <> ?");
	error = NoError;
    if (dsqlstatus == 0)
    {
		sql_mode = 0;
		error = LiefBestIsDouble;
        return -1;
    }

	if (TerminEnter)
	{
		sql_mode = 0;
		return WriteBzgn ();
	}

    clipped (lief_bzg.lief);
    if (numeric (lief_bzg.lief) && strlen (lief_bzg.lief) < 9)
    {
        lief_bzg.lief_s = atol (lief_bzg.lief);
    }

	if (lief_bzg.min_best == 0.0) lief_bzg.min_best = 1.0;

    dsqlstatus = LiefBzg.dbupdate ();
	if (dsqlstatus < 0)
	{
			sql_mode = 0;
			error = RecordIsLocked;
			return -1;
	}
    if (lief_bzg.lief_kz[0] == 'J')
    {
        a_best.mdn = lief_bzg.mdn;
        a_best.fil = lief_bzg.fil;
        a_best.a   = lief_bzg.a;
        dsqlstatus = ABest.dbreadfirst ();
        strcpy (a_best.lief, lief_bzg.lief);
        strcpy (a_best.lief_best, lief_bzg.lief_best);
        if (dsqlstatus == 100)
        {
             a_best.best_anz = 1;
             strcpy (a_best.best_auto, "N");
             a_best.me_einh_ek = lief_bzg.me_einh_ek;
             strcpy (a_best.umk_kz, "N");
             strcpy (a_best.erf_gew_kz, "N");
        }
        dsqlstatus = ABest.dbupdate ();
		if (dsqlstatus < 0)
		{
			sql_mode = 0;
			return -1;
		}
    }
	sql_mode = 0;
    WriteTxt ();
    return 0;
}

int LiefBzgWork::DeleteLief (void)
{
    int dsqlstatus;
    
    LiefBzg.sqlin ((short *)  &lief_bzg.mdn, 1, 0);
    LiefBzg.sqlin ((short *)  &lief_bzg.fil, 1, 0);
    LiefBzg.sqlin ((char *)   lief_bzg.lief, 0, 17);
    dsqlstatus = LiefBzg.sqlcomm ("delete from lief_bzg "
                                  "where mdn = ? "
                                  "and fil = ? "
                                  "and lief = ?");
    return dsqlstatus;
}


int LiefBzgWork::DeleteA (void)
{
    LiefBzg.dbdelete ();
    return 0;
}

int LiefBzgWork::GetPtab (char *dest, char *item, char *wert)
{
    int dsqlstatus;

    dest[0] = 0;
    dsqlstatus = Ptab.lese_ptab (item, wert);  
    if (dsqlstatus == 100)
    {
        return -1;
    }
    strcpy (dest, ptabn.ptbezk);
    return 0;
}

int LiefBzgWork::ShowPtab (char *dest, char *item)
{
    int dsqlstatus;

    dsqlstatus = Ptab.Show (item);  
    if (syskey == KEY5)
    {
        return -1;
    }
    strcpy (dest, ptabn.ptwert);
    return 0;
}


int LiefBzgWork::ShowPtabEx (HWND hMainWindow, char *dest, char *item)
{
       char wert [80];
       SEARCHPTAB *SearchPtab;
	   HINSTANCE hMainInst;


       hMainInst = (HINSTANCE) GetWindowLong (hMainWindow, GWL_HINSTANCE);
       SearchPtab = new SEARCHPTAB (item);
       SearchPtab->SetParams (hMainInst, hMainWindow); 
       SearchPtab->Setawin (hMainWindow);
       SearchPtab->SetNumWert (TRUE);                                             
       SearchPtab->Search ();
       if (SearchPtab->GetKey (wert) == TRUE)
       {
           delete SearchPtab;
           strcpy (dest, wert);
		   return 0;
       }
       delete SearchPtab;
       return -1;
}


int LiefBzgWork::GetMdnName (char *dest, short mdn)
{
    int dsqlstatus;

 //   _mdn.konversion = 1.95583;
    _mdn.konversion = 1.0;
    if (mdn == 0)
    {
        strcpy (dest, "Zentrale");
        return 0;
    }
    dest[0] = 0;
    LiefBzg.sqlin ((short *) &mdn, 1, 0);
    LiefBzg.sqlout ((char *) dest, 0, 17);
    dsqlstatus =  LiefBzg.sqlcomm ("select adr.adr_krz from mdn,adr "
                                   "where mdn.mdn = ? " 
                                   "and adr.adr = mdn.adr");
    if (dsqlstatus == 100)
    {
        return 100;
    }
    Mdn.lese_mdn (mdn);
    if (_mdn.konversion == 0)
    {
        _mdn.konversion = 1.0;
    }
    _mdn.konversion = 1.0;
    return 0;
}

int LiefBzgWork::GetFilName (char *dest, short mdn, short fil)
{

    if (mdn == 0 || fil == 0)
    {
        strcpy (dest, "Mandant");
        return 0;
    }
    dest[0] = 0;
    LiefBzg.sqlin ((short *) &mdn, 1, 0);
    LiefBzg.sqlin ((short *) &fil, 1, 0);
    LiefBzg.sqlout ((char *) dest, 0, 17);
    return LiefBzg.sqlcomm ("select adr.adr_krz from fil,adr "
                             "where fil.mdn = ? " 
                             "and   fil.fil = ? " 
                             "and adr.adr = fil.adr");
}

int LiefBzgWork::GetLiefName (char *dest, short mdn, char *lief_nr)
{
/*
    dest[0] = 0;


    LiefBzg.sqlin ((short *) &mdn, 1, 0);
    LiefBzg.sqlin ((char *)  lief, 0, 17);
    LiefBzg.sqlout ((char *) dest, 0, 17);

    return LiefBzg.sqlcomm ("select adr.adr_krz from lief,adr "
                             "where lief.mdn = ? " 
                             "and   lief.lief = ? "
                             "and adr.adr = lief.adr");
*/
    int dsqlstatus;
    int cursor;

    dest[0] = 0;


    LiefBzg.sqlin ((short *) &mdn, 1, 0);
    LiefBzg.sqlin ((char *)  lief_nr, 0, 17);
    LiefBzg.sqlout ((char *) dest, 0, 17);

    cursor = LiefBzg.sqlcursor ("select adr.adr_krz from lief,adr "
                                "where lief.mdn = ? " 
                                "and   lief.lief = ? "
                                "and adr.adr = lief.adr");
    dsqlstatus = LiefBzg.sqlfetch (cursor);
    while (dsqlstatus == 100)
    {
        if (mdn > 0)
        {
            mdn = 0;
        }
        else
        {
            break;
        }
        dsqlstatus = LiefBzg.sqlopen (cursor);
        dsqlstatus = LiefBzg.sqlfetch (cursor);
    }
    LiefBzg.sqlclose (cursor);
    if (dsqlstatus == 0)
    {
        lief.mdn = mdn;
        strcpy (lief.lief, lief_nr);
        Lief.dbreadfirst ();
    }
    return dsqlstatus;
}


void LiefBzgWork::FillRow (char *buffer)
{
    char sa_dat [12];

	strcpy (sa_dat, "");
	if (lief_bzgn.pr_ek_eur != 0.0)
	{
		dlong_to_asc (lief_bzgn.dat, sa_dat);
	}


    if (lief.waehrung == 2)
    {
/*
        sprintf (buffer, "%13.0lf  |%-24s|  |%-16s|  %8.4lf   %8.4lf",
                         lief_bzg.a, _a_bas.a_bz1, lief_bzg.lief_best,
                         lief_bzg.pr_ek_eur, lief_bzg.pr_ek_sa_eur);
*/
        sprintf (buffer, "%13.0lf  |%-24s|  |%-16s|  %8.4lf   %8.4lf   %-10s   %10.2lf",
                         lief_bzg.a, _a_bas.a_bz1, lief_bzg.lief_best,
                         lief_bzg.pr_ek_eur, lief_bzgn.pr_ek_eur, sa_dat, lief_bzg.vme);

    }
    else
    {
/*
        sprintf (buffer, "%13.0lf  |%-24s|  |%-16s|  %8.4lf   %8.4lg",
                         lief_bzg.a, _a_bas.a_bz1, lief_bzg.lief_best,
                         lief_bzg.pr_ek, lief_bzg.pr_ek_sa);
*/
        sprintf (buffer, "%13.0lf  |%-24s|  |%-16s|  %8.4lf   %8.4lf   %-10s   %10.2lf",
                         lief_bzg.a, _a_bas.a_bz1, lief_bzg.lief_best,
                         lief_bzg.pr_ek, lief_bzgn.pr_ek, sa_dat, lief_bzg.vme);
    }
}

void LiefBzgWork::BeginWork (void)
{
    ::beginwork ();
}

void LiefBzgWork::CommitWork (void)
{
    ::commitwork ();
}

void LiefBzgWork::RollbackWork (void)
{
    ::rollbackwork ();
}

void LiefBzgWork::InitRec (void)
{
    memcpy (&lief_bzg, &lief_bzg_null, sizeof (struct LIEF_BZG));
}

BOOL LiefBzgWork::GenSqlout (char *Tab, char *Feld)
{
    char Feldname [256];
    int i;

    sprintf (Feldname, "%s.%s", Tab, Feld);
    for (i = 0; i < textWork->GetFieldanz (); i ++)
    {
        if (strcmp (Feldname, textWork->GetFeldnamen () [i]) == 0)
        {
            LiefBzg.sqlout ((char *) textWork->GetFelder () [i], 0, 
                                     textWork->GetLength () [i]);
            return TRUE;
        }
    }
    return FALSE;
}

int LiefBzgWork::ReadTxt (void)
{
    char **Selects;
    char sql [0x1000];
    int anz;
    int i;

    if (textWork == NULL)
    {
        return 0;
    }

    Selects = textWork->GetSelects ();
    if (Selects[0] == NULL)
    {
        return 0;
    }
    anz = wsplit (Selects[0], ";");
    if (anz < 2) return 0;

    strcpy (sql, "select ");

    for (i = 1; i < anz; i ++)
    {
        if (GenSqlout (wort[0], wort[i]) == FALSE) continue;
        if (i > 1)
        {
            strcat (sql, ",");
        }
        strcat (sql, wort[i]);
    }

    strcat (sql, " from ");
    strcat (sql, wort [0]);
    strcat (sql, " where mdn = ? and fil = ? and lief = ? and a = ?");

    LiefBzg.sqlin ((short *) &lief_bzg.mdn, 1, 0);
    LiefBzg.sqlin ((short *) &lief_bzg.fil, 1, 0);
    LiefBzg.sqlin ((char *)  lief_bzg.lief, 0, 17);
    LiefBzg.sqlin ((double *)  &lief_bzg.a, 3, 0);
    LiefBzg.sqlcomm (sql);
    return 0;
}

BOOL LiefBzgWork::GenSqlin (char *Tab, char *Feld)
{
    char Feldname [256];
    int i;

    sprintf (Feldname, "%s.%s", Tab, Feld);
    for (i = 0; i < textWork->GetFieldanz (); i ++)
    {
        if (strcmp (Feldname, textWork->GetFeldnamen () [i]) == 0)
        {
            LiefBzg.sqlin ((char *) textWork->GetFelder () [i], 0, 
                                    textWork->GetLength () [i]);
            return TRUE;
        }
    }
    return FALSE;
}


int LiefBzgWork::WriteTxt (void)
{
    char **Updates;
    char sql [0x1000];
    int anz;
    int i;

    if (textWork == NULL)
    {
        return 0;
    }

    Updates = textWork->GetUpdates ();
    if (Updates[0] == NULL)
    {
        return 0;
    }
    anz = wsplit (Updates[0], ";");
    if (anz < 2) return 0;

    sprintf (sql, "update %s set ", wort [0]);

    for (i = 1; i < anz; i ++)
    {
        if (GenSqlin (wort[0], wort[i]) == FALSE) continue;
        if (i > 1)
        {
            strcat (sql, ",");
        }
        strcat (sql, wort[i]);
        strcat (sql, " = ?");
    }

    strcat (sql, " where mdn = ? and fil = ? and lief = ? and a = ?");
         

    LiefBzg.sqlin ((short *) &lief_bzg.mdn, 1, 0);
    LiefBzg.sqlin ((short *) &lief_bzg.fil, 1, 0);
    LiefBzg.sqlin ((char *)  lief_bzg.lief, 0, 17);
    LiefBzg.sqlin ((double *)  &lief_bzg.a, 3, 0);
    LiefBzg.sqlcomm (sql);
    return 0;
}


void LiefBzgWork::FillTerminCombo (char *TerminCombo[], int maxcombo)
{
	int cursor;

	char termin [12];
    LiefBzg.sqlin ((short *) &lief_bzg.mdn, 1, 0);
    LiefBzg.sqlin ((short *) &lief_bzg.fil, 1, 0);
    LiefBzg.sqlin ((char *)  lief_bzg.lief, 0, 17);
	LiefBzg.sqlout ((char *) termin, 0, 11);
    cursor = LiefBzg.sqlcursor ("select distinct (dat) from lief_bzgn "
		                        "where mdn = ? "
								"and fil = ? "
								"and lief = ? "
								"and dat > today "
								"order by dat");
	int i = 0;
	while ((fetch_sql (cursor) == 0) && (i < maxcombo))
	{
		TerminCombo[i] = new char [20];
		strcpy (TerminCombo[i], termin);
		i ++;
	}
    TerminCombo[i] = NULL;
	LiefBzg.sqlclose (cursor);
}

void LiefBzgWork::FreeTerminCombo (char *TerminCombo[])
{
	for (int i = 0; TerminCombo[i] != NULL; i ++)
	{
		delete TerminCombo[i];
		TerminCombo[i] = NULL;
	}
}

