// IprDialog.h: Schnittstelle f�r die Klasse CIprDialog.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_IPRDIALOG_H__5EC694C7_8B78_44A0_A51E_01B7F7EFDB79__INCLUDED_)
#define AFX_IPRDIALOG_H__5EC694C7_8B78_44A0_A51E_01B7F7EFDB79__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
#include "windows.h"

class CIprDialog  
{
protected:
	 HANDLE IprDialog;
	 void (*IprDlg)(char *params[]); 
	 BOOL (*dOpenDbase)(LPSTR); 
public:
	CIprDialog();
	virtual ~CIprDialog();
	void Start (short mdn, double a, double ek);
};

#endif // !defined(AFX_IPRDIALOG_H__5EC694C7_8B78_44A0_A51E_01B7F7EFDB79__INCLUDED_)
