#include <windows.h>
#include "searchlief.h"
#include "strfkt.h"
#include "mo_wmess.h"
#include "lbox.h"


int SEARCHLIEF::idx;
long SEARCHLIEF::anz;
CHQEX *SEARCHLIEF::Query = NULL;
DB_CLASS SEARCHLIEF::DbClass; 
HINSTANCE SEARCHLIEF::hMainInst;
HWND SEARCHLIEF::hMainWindow;
HWND SEARCHLIEF::awin;
short SEARCHLIEF::mdn_nr = 0;


int SEARCHLIEF::SearchLst (char *sebuff)
/**
Nach Kurzname in Liste suchen.
**/
{
	   Query->SearchList ();
	   return 0;
}

int SEARCHLIEF::ReadLst (char *name)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int cursor;
	  int i;
      WMESS Wmess;
      short mdn;
      char lief [17];
      char adr_krz [17];


	  anz = 0;
      clipped (name);
      if (strcmp (name, " ") == 0) name[0] = 0;

      if (strlen (name) == 0 ||
          name[0] <= ' ')
      {
	          sprintf (buffer, "select lief.mdn, lief.lief, adr.adr_krz "
	 			       "from LIEF, adr "
                       "where adr.adr = lief.adr "
                       "order by lief");
      }
      else
      {
	          sprintf (buffer, "select lief.mdn, lief.lief, adr.adr_krz "
	 			       "from lief,adr "
                       "where adr_krz matches \"%s\" "
                       "and adr.adr_krz = lief.adr_krz", name);
      }
      DbClass.sqlout ((short *) &mdn, 1, 0);
      DbClass.sqlout ((char *) lief, 0, 17);
      DbClass.sqlout ((char *) adr_krz, 0, 17);
      cursor = DbClass.sqlcursor (buffer);
      if (cursor < 0) 
      {
          return -1;
      }
  	  i = 0;
      Wmess.Message (hMainInst, hMainWindow, " \nBitte warten.....\n"
  	                                         "Die Daten werden selektiert.\n ");  
	  while (DbClass.sqlfetch (cursor) == 0)
      {
 	      sprintf (buffer, "    %4d  |%-16s|  |%-16s|", mdn, lief, adr_krz);
	      Query->InsertRecord (buffer);
		  i ++;
      }
      anz = i;
	  DbClass.sqlclose (cursor);
 	  Wmess.Destroy ();
	  EnableWindow (awin, FALSE);
	  return 0;
}

void SEARCHLIEF::SetParams (HINSTANCE hMainInst, HWND hMainWindow)
{
      this->hMainInst   = hMainInst;
      this->hMainWindow = hMainWindow;
      awin = hMainWindow;
}

BOOL SEARCHLIEF::GetKey (char * key)
{
      int anz;
     
      strcpy (key, " ");
      if (idx == -1) return FALSE;
      anz = wsplit (Key, "|");
      if (anz < 2) return FALSE;
      strcpy (key, clipped (wort[1]));
      return TRUE;
}


void SEARCHLIEF::Search (void)
{
  	  int cx, cy;
	  char buffer [256];

	  idx = -1;
      cx = 48;
      cy = 25;
      Settchar ('|');
      EnableWindow (hMainWindow, FALSE);
      Query = new CHQEX (cx, cy);
      Query->EnableSort (TRUE);
      Query->OpenWindow (hMainInst, hMainWindow);
      sprintf (buffer, "%s %s %s",
                          "%d" 
                          "%s",
                          "%s");
      Query->RowAttr (buffer);
	  sprintf (buffer, "    %6s%18s", "1", "1"); 
	  Query->VLines (buffer, 3);
      sprintf (buffer, "4;4 11;16 31;16");
      Query->RowPos (buffer);
	  EnableWindow (awin, FALSE);

	  sprintf (buffer, "    %4s  %-16s  %-16s", "Mdn", "Lieferant", "Name"); 
	  Query->InsertCaption (buffer);

	  Query->SetFillDb (ReadLst);
	  Query->SetSearchLst (SearchLst);
      ReadLst ("");
      Query->SetSortRow (2, TRUE);
	  Query->ProcessMessages ();
      EnableWindow (hMainWindow, TRUE);
      if (syskey != KEY5)
      {
              idx = Query->GetSel ();
              if (idx != -1)
              {
                     Query->GetText (Key);
              }
      }
	  EnableWindow (awin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (awin);
}

void SEARCHLIEF::Search (char *stext)
{
  	  int cx, cy;
	  char buffer [256];

	  idx = -1;
      cx = 48;
      cy = 25;
      Settchar ('|');
      EnableWindow (hMainWindow, FALSE);
      Query = new CHQEX (cx, cy);
      Query->EnableSort (TRUE);
      Query->OpenWindow (hMainInst, hMainWindow);
      sprintf (buffer, "%s %s %s",
                          "%d" 
                          "%s",
                          "%s");
      Query->RowAttr (buffer);
	  sprintf (buffer, "    %6s%18s", "1", "1"); 
	  Query->VLines (buffer, 3);
      sprintf (buffer, "4;4 11;16 31;16");
      Query->RowPos (buffer);
	  EnableWindow (awin, FALSE);

	  sprintf (buffer, "    %4s  %-16s  %-16s", "Mdn", "Lieferant", "Name"); 
	  Query->InsertCaption (buffer);

	  Query->SetFillDb (ReadLst);
	  Query->SetSearchLst (SearchLst);
      ReadLst ("");
      Query->SetSortRow (2, TRUE);
      if (stext != NULL && stext[0] != 0)
      {
             Query->SetSBuff (stext);
      }
	  Query->ProcessMessages ();
      EnableWindow (hMainWindow, TRUE);
      if (syskey != KEY5)
      {
              idx = Query->GetSel ();
              if (idx != -1)
              {
                     Query->GetText (Key);
              }
      }
	  EnableWindow (awin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (awin);
}

