#ifndef _LIEFMEBEST_DEF
#define _LIEFMEBEST_DEF
#include <windows.h>
#include "wmaskc.h"
#include "dbclass.h"

struct LIEFMEBEST {
   short     mdn;
   short     fil;
   char      lief[17];
   double    a;
   double    tara0;
   short     me_einh0;
   double    inh0;
   short     me_einh1;
   double    inh1;
   double    tara1;
   short     me_einh2;
   double    inh2;
   double    tara2;
   short     me_einh3;
   double    inh3;
   double    tara3;
   short     me_einh4;
   double    inh4;
   double    tara4;
   short     me_einh5;
   double    inh5;
   double    tara5;
   short     me_einh6;
   double    inh6;
   double    tara6;
   short     knuepf1;
   short     knuepf2;
   short     knuepf3;
   short     knuepf4;
   short     knuepf5;
   short     knuepf6;
   double    ean_su1;
   double    ean_su2;
   double    pr_ek0;
   double    pr_ek1;
   double    pr_ek2;
   double    pr_ek3;
   double    pr_ek4;
   double    pr_ek5;
   double    pr_ek6;
};
extern struct LIEFMEBEST liefmebest, liefmebest_null;

#line 8 "liefmebest.rh"

class LIEFMEBEST_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               LIEFMEBEST_CLASS () : DB_CLASS ()
               {
               }
               ~LIEFMEBEST_CLASS ()
               {
                   dbclose ();
               } 
               int dbreadfirst (void);
};
#endif
