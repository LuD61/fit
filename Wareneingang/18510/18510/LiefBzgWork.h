#ifndef _LIEFBZGWORK_DEF
#define _LIEFBZGWORK_DEF
#include "lief_bzg.h"
#include "lief_bzgn.h"
#include "liefmebest.h"
#include "a_best.h"
#include "lief.h"
#include "ptab.h"
#include "mdn.h"
#include "textwork.h"
#include "searchptab.h"

class LiefBzgWork
{
    private :
        LIEF_BZG_CLASS LiefBzg;
        LIEF_BZGN_CLASS LiefBzgn;
        LIEFMEBEST_CLASS LiefMeBest;
        PTAB_CLASS Ptab;
        LIEF_CLASS Lief;
        MDN_CLASS Mdn;
        int liefbzg_cursor;
        char default_lief_kz [4];
        char old_me_kz [5];
        TextWork *textWork;
		char Termin [20];
		BOOL TerminEnter;
		int error;
    public :
		static const char *ErrorMsg[];
		enum 
		{
			NoError = 0,
			LiefBestIsDouble = 1,
            RecordIsLocked = 2,
		};

		int Error ()
		{
			return error;
		}

		void SetTerminEnter (BOOL TerminEnter)
		{
			this->TerminEnter = TerminEnter;
		}

		BOOL GetTerminEnter ()
		{
			return TerminEnter;
		}
		void SetTermin (char *Termin)
		{
			strcpy (this->Termin, Termin);
		}

		char *GetTermin ()
		{
			return Termin;
		}

        LiefBzgWork () : liefbzg_cursor (-1)
        {
            strcpy (default_lief_kz, "N");
        }

        void SetDefaultLiefKz (char *lief_kz)
        {
            strcpy (default_lief_kz, lief_kz);
        }

        void SetOldMeKz (char *old_me_kz)
        {
            strcpy (this->old_me_kz, old_me_kz);
        }

        char *GetOldMeKz (void)
        {
            return old_me_kz;
        }

        void SetTextWork (TextWork *textWork)
        {
            this->textWork = textWork;
        }

        TextWork *GetTextWork (void)
        {
            return textWork;
        }

        void BeginWork (void);
        void CommitWork (void);
        void RollbackWork (void);
        void InitRec (void);
        int ReadLief (short, short, char *);
        int ReadLief (void);
        int DeleteLief (void);
        int ReadA (short, short, char *, double, char *);
        int ReadArt (short, short, char *, double);
        int ReadA (void);
        int ReadBzgn (void);
        BOOL LiefmebestExist (void);
        void WriteLiefmebest (char *);
        int WriteBzgn (void);
        int WriteA (void);
        int DeleteA (void);
        int GetPtab (char *, char *, char *);
        int ShowPtab (char *dest, char *item);
        int ShowPtabEx (HWND, char *, char *);
        int GetMdnName (char *, short);
        int GetFilName (char *, short, short);
        int GetLiefName (char *, short, char *);
        int GetLiefDefaults (void);
        void FillRow (char *);
        BOOL GenSqlout (char *, char *);
        int  ReadTxt (void);
        BOOL GenSqlin (char *, char *);
        int  WriteTxt (void);
        void FillTerminCombo (char *[], int);
        void FreeTerminCombo (char *[]);
};

#endif