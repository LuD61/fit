#ifndef _TEXTWORK_DEF
#define _TEXTWORK_DEF

class TextWork 
{
          private :
              char *Selects[100];
              char *Updates[100];
              char *Keys[100];
              int Wsize [2];
              char **Feldnamen;
              char **Texte;
              char **Felder;
              char **Namen;
              int *Rows;
              int *Columns;
              int *Length;
              int headfieldanz;
              int fieldanz;
              int Startrow;
              int Textlen;
              COLORREF WinColor;
          public :
              char **GetUpdates (void)
              {
                     return Updates;
              }

              char **GetSelects (void)
              {
                     return Selects;
              }

              char **GetKeys (void)
              {
                     return Keys;
              }

              int *GetWsize (void)
              {
                     return Wsize;
              }

              char **GetFeldnamen (void)
              {
                     return Feldnamen;
              }

              char **GetTexte (void)
              {
                     return Texte;
              }      
              char **GetFelder (void)
              {
                     return Felder;
              }      
              char **GetNamen (void)
              {
                     return Namen;
              }      

              int *GetRows (void)
              {
                     return Rows;
              }

              int *GetColumns (void)
              {
                     return Columns;
              }

              int *GetLength (void)
              {
                     return Length;
              }

              int GetFieldanz (void)
              {
                     return fieldanz;
              } 

              int GetTextlen (void)
              {
                     return Textlen;
              }

              COLORREF GetWinColor (void)
              {
                  return WinColor;
              }

              TextWork (char *); 
              ~TextWork ();
};
#endif
