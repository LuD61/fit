#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include <time.h>
#include "comcthlp.h"
#include "wmaskc.h"
#include "itemc.h"
#include "mo_meld.h"
#include "strfkt.h"
#include "stdfkt.h"
#include "mo_gdruck.h"

static BOOL GraphikMode = FALSE;
static char vOutput [21];
static char vDrkChoise [21];


extern HANDLE  hMainInst;
static HWND hMainWindow = NULL;
static HWND  mamain1 = NULL;
static HFONT StdFont = NULL;


static char akt_drucker [40] [64];
static int drkanz = 0;

void BelegeGdiDrucker (void)
/**
Drucker Win.ini holen
**/
{
       int i;
	   char *pr;
	   char AktDevice [80];
       char ziel [256];
       char quelle [256];
       char *frmenv;

       frmenv = getenv ("FRMPATH");
       if (frmenv == NULL) return;

       GetAllPrinters ();
     
       i = 0;
       while (TRUE)
       {
                pr = GetNextPrinter ();
                if (pr == NULL) break;
                strcpy (akt_drucker[i], pr);
                i ++;
       }
       drkanz = i;

	   if (GetPrinter (AktDevice))
	   {
		        strcpy (vDrkChoise, AktDevice);      
	   }
       sprintf (quelle, "%s\\%s", getenv ("FRMPATH"), "gdi.cfg");
       sprintf (ziel, "%s\\%s", getenv ("FRMPATH"), "drucker.akt");
       CopyFile (quelle, ziel, FALSE);
       sprintf (ziel, "%s\\%s.%s", getenv ("FRMPATH"),
                                 clipped (sys_ben.pers_nam),
                                 "akt");
       CopyFile (quelle, ziel, FALSE);
}


void BelegeDrucker (void)
/**
Drucker aus Formatverzeichnis holen.
**/
{
      char frmdir [256];
      char ziel [256];
      char quelle [256];
      char *frmenv;
      WIN32_FIND_DATA lpffd;
      HANDLE hFile;
      char *p;
      int i;

      GraphikMode = IsGdiPrint ();

      if (GraphikMode)
      {
               BelegeGdiDrucker ();
               return;
      }
      frmenv = getenv ("FRMPATH");
      if (frmenv == NULL) return;

      sprintf (frmdir, "%s\\*.cfg", frmenv);

      hFile = FindFirstFile (frmdir, &lpffd);

      if (hFile == INVALID_HANDLE_VALUE)
      {
                      return;
      }

      i = 0;
      p = strchr (lpffd.cFileName, '.');
      if (p) *p = (char) 0;

      sprintf (quelle, "%s\\%s.cfg", getenv ("FRMPATH"), lpffd.cFileName);
      sprintf (ziel, "%s\\%s", getenv ("FRMPATH"), "drucker.akt");
      strcpy (akt_drucker[i], lpffd.cFileName);
      i ++;

      while (FindNextFile (hFile, &lpffd))
      {
               p = strchr (lpffd.cFileName, '.');
               if (p) *p = (char) 0;
               strcpy (akt_drucker[i], lpffd.cFileName);
               i ++;
      }
      FindClose (hFile);

      drkanz = i;
}

void drucker_akt (char *dname)
/**
Ausgewaehlten Drucker in drucker.akt_uebertragen.
**/
{
        char ziel [256];
        char quelle [256];

		if (GraphikMode)
		{
		        SetPrinter (clipped (dname));
				return;
		}
        sprintf (quelle, "%s\\%s.cfg", getenv ("FRMPATH"), dname);
        sprintf (ziel, "%s\\%s", getenv ("FRMPATH"), "drucker.akt");
        CopyFile (quelle, ziel, FALSE);
        clipped (sys_ben.pers_nam);
        if (strlen (sys_ben.pers_nam))
        {
                  sprintf (ziel, "%s\\%s.%s", getenv ("FRMPATH"),
                                 sys_ben.pers_nam,
                                 "akt");
        }
        else
        {
                  sprintf (ziel, "%s\\%s.%s", getenv ("FRMPATH"),
                                  "drucker",
                                 "akt");
        }
        CopyFile (quelle, ziel, FALSE);
}

void InitCombo0 (void)
/**
Combofelder fuellen.
**/
{
        static BOOL ComboOK0 = FALSE;
        int i;

        if (ComboOK0) return;

        BelegeDrucker ();
        for (i = 0; i < drkanz; i ++)
        {
               strcpy (Dtab[i], akt_drucker[i]);
        }
        cbfield0[0].cbanz = i;
//        strcpy (vDrkChoise, akt_drucker[0]);
        ComboOK0 = TRUE;
}

void InitCombo1 (void)
/**
Combofelder fuellen.
**/
{
        static BOOL ComboOK0 = FALSE;
        int i;

        if (ComboOK0) return;

        BelegeGdiDrucker ();
        for (i = 0; i < drkanz; i ++)
        {
               strcpy (Dtab[i], akt_drucker[i]);
        }
        cbfield0[0].cbanz = i;
//        strcpy (vDrkChoise, akt_drucker[0]);
        ComboOK0 = TRUE;
}

void ChoiseCombo (void)
/**
Ausgewaehlte Combofelder auswerten.
**/
{
        int i;
        static int Output [] = {PRINTER_OUT, SCREEN_OUT, FILE_OUT};

        clipped (vOutput);
        for (i = 0; i < 3; i ++)
        {
            if (strcmp (vOutput,Otab[i]) == 0)
            {
                ausgabe = Output[i];
                break;
            }
        }
        clipped (vDrkChoise);
        if (strlen (vDrkChoise) == 0) return; 
        drucker_akt (vDrkChoise);
}


void ChoisePrinter (void)
/**
Ausgabe und Drucker w�hlen.
**/
{
        HWND hFind;
        HWND ahWnd;

        save_fkt (5);
        save_fkt (12);
        save_fkt (6);

        InitCombo ();
        set_fkt (eBreak, 5);
        set_fkt (eOk, 12);

        no_break_end ();
        ahWnd = AktivWindow;
        EnableWindows (GetActiveWindow (), FALSE);
        SetBorder (WS_POPUP | WS_VISIBLE | WS_CAPTION | WS_DLGFRAME);
        hFind = OpenWindowChC (8, 55, 10, 13, hMainInst,
                "Parameter f�r Liste");
        SetButtonTab (TRUE);
        enter_form (hFind, &fDrkw,0, 0);
        SetButtonTab (FALSE);
        AktivWindow = ahWnd;
        EnableWindows (AktivWindow, TRUE);
        SetActiveWindow (AktivWindow);
        DestroyWindow (hFind);
        hFind = NULL;
        no_break_end ();
        restore_fkt (5);
        restore_fkt (12);
        restore_fkt (6);
       if (syskey == KEY5) return;
       ChoiseCombo ();
}

void ChoiseCombo0 (void)
/**
Ausgewaehlte Combofelder auswerten.
**/
{
        clipped (vDrkChoise);
        if (strlen (vDrkChoise) == 0) return; 
        drucker_akt (vDrkChoise);
}

static void ChoiseCombo1 (void)
/**
Ausgewaehlte Combofelder auswerten.
**/
{
        clipped (vDrkChoise);
        if (strlen (vDrkChoise) == 0) return; 
        SetPrinter (clipped (vDrkChoise));
}


void ChoisePrinter0 (void)
/**
Drucker w�hlen.
**/
{
        HWND hFind;
        HWND ahWnd;
		FORM *scurrent;
		int   sfield;

		scurrent = current_form;
		sfield   = currentfield;
        save_fkt (5);
        save_fkt (12);
        save_fkt (6);

        ausgabe = PRINTER_OUT;
        InitCombo0 ();
        set_fkt (eBreak, 5);
        set_fkt (eOk, 12);

        no_break_end ();
        ahWnd = AktivWindow;
		ahWnd = GetActiveWindow ();
        EnableWindows (GetActiveWindow (), FALSE);
        SetBorder (WS_POPUP | WS_VISIBLE | WS_CAPTION | WS_DLGFRAME);
        hFind = OpenWindowChC (6, 55, 10, 13, hMainInst,
                "Auswahl Drucker");
        SetButtonTab (TRUE);
        enter_form (hFind, &fDrkw0,0, 0);
        SetButtonTab (FALSE);
        AktivWindow = ahWnd;
        EnableWindows (AktivWindow, TRUE);
        SetActiveWindow (AktivWindow);
        DestroyWindow (hFind);
		current_form = scurrent;
		currentfield = sfield;
        hFind = NULL;
        no_break_end ();
        restore_fkt (5);
        restore_fkt (12);
        restore_fkt (6);
       if (syskey == KEY5) return;
       ChoiseCombo0 ();
}


void ChoisePrinter1 (void)
/**
Drucker w�hlen.
**/
{
        HWND hFind;
        HWND ahWnd;
		FORM *scurrent;
		int   sfield;

		scurrent = current_form;
		sfield   = currentfield;
        save_fkt (5);
        save_fkt (12);
        save_fkt (6);

        ausgabe = PRINTER_OUT;
        InitCombo1 ();
        set_fkt (eBreak, 5);
        set_fkt (eOk, 12);

        no_break_end ();
        ahWnd = AktivWindow;
		ahWnd = GetActiveWindow ();
        EnableWindows (GetActiveWindow (), FALSE);
        SetBorder (WS_POPUP | WS_VISIBLE | WS_CAPTION | WS_DLGFRAME);
        hFind = OpenWindowChC (6, 55, 10, 13, hMainInst,
                "Auswahl Drucker");
        SetButtonTab (TRUE);
        enter_form (hFind, &fDrkw0,0, 0);
        SetButtonTab (FALSE);
        AktivWindow = ahWnd;
        EnableWindows (AktivWindow, TRUE);
        SetActiveWindow (AktivWindow);
        DestroyWindow (hFind);
		current_form = scurrent;
		currentfield = sfield;
        hFind = NULL;
        no_break_end ();
        restore_fkt (5);
        restore_fkt (12);
        restore_fkt (6);
       if (syskey == KEY5) return;
       ChoiseCombo1 ();
}
