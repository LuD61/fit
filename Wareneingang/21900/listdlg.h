#ifndef _LISTDLG_DEF
#define _LISTDLG_DEF
#include "listenter.h"
#include "listclex.h"
#include "work.h"
#include "mo_progcfg.h"

#define SMALL 0
#define BIG 1
 
class LISTDLG : public virtual LISTENTER, 
                public virtual KEYS,
                public virtual Info
{
         private :
                      static CFIELD *_fChoise0[];
                      static CFORM fChoise0;
                      static CFIELD *_fChoise[];
                      static CFORM fChoise;

                      static struct CHATTR ChAttra [];
                      static struct CHATTR *ChAttr;
           			  ZERM_CLASS Zerm;
           			  ZERLDATEN_CLASS Zerldaten;
		              Work *work;
                      int cursor;
                      CFORM *Toolbar2;
                      BOOL cfgOK;
                      PROG_CFG *ProgCfg;
                      HBITMAP ArrDown;

         public :

                     static struct LSTS Lsts;
                     static struct LSTS *LstTab;
                     static ITEM ufeld1;
                     static ITEM ufeld2;
                     static ITEM ufeld3;
                     static ITEM ufeld4;
                     static ITEM ufeld5;
                     static ITEM ufiller;
                     static field _UbForm[];
                     static form UbForm;

                     static ITEM iline;
                     static field _LineForm[];
                     static form LineForm;

                     static ITEM ifeld1;
                     static ITEM ifeld2;
                     static ITEM ifeld3;
                     static ITEM ifeld4;
                     static ITEM ifeld5;
                     static ITEM ichoise;
                     static field _DataForm[];
                     static form DataForm;
                     static int ubrows[];
                     void SetButtonSize (int);

                     void SetToolbar2 (CFORM *Toolbar2)
                     {
                               this->Toolbar2 = Toolbar2;
                     }
 
                     CFORM *GetToolbar2 (void)
                     {
                               return Toolbar2;
                     }

                     void SetWork (Work *work)
                     {
                         this->work = work;
                         this->work->SetListDlg (this);
                     }

                     Work *GetWork (void)
                     {
                         return work;
                     }

                     LISTDLG ();
					 ~LISTDLG ();
                     void GetSysPar (void);
                     void GetCfgValues (void);
                     void Setfeld2Removed (BOOL);
                     void Prepare (void);
                     void Open (void);
                     int Fetch (void);
                     void Close (void);
                     
 				     unsigned char *GetDataRec (void);
				     int GetDataRecLen (void);
 				     char *GetListTab (int);
                     void WriteRec (int);
                     void InitRow (void);
                     int ToMemory (int);
                     void uebertragen (void);

                     BOOL OnKey5 (void);
                     BOOL OnKey6 (void);
                     BOOL OnKey7 (void);
                     BOOL OnKey8 (void);
                     BOOL OnKey9 (void);
                     BOOL OnKey10 (void);
                     BOOL OnKey11 (void);
                     BOOL OnKey12 (void);
		             BOOL ShowA (char *);
                     BOOL OnKeyDel (void);
                     BOOL OnKeyInsert (void);
                     BOOL OnKeyUp (void);
                     BOOL OnKeyDown (void);
                     BOOL OnCommand (HWND, UINT, WPARAM, LPARAM);
                     BOOL OnPaint (HWND, UINT, WPARAM, LPARAM);
                     BOOL OnMove (HWND, UINT, WPARAM, LPARAM);
                     BOOL SchreibeZerlege(void);
                     int AfterRow (int);
                     int AfterCol (char *);
                     int BeforeRow (int);
                     void CreateListQuikInfos (HWND);
                     void TestRemoves (void);
                     static int InfoProc (char **, char *, char *);

                     BOOL CallInfo (HWND, RECT *, char *, char *, DWORD);
};
#endif