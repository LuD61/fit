#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "wmaskc.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "zerldaten.h"

struct ZERLDATEN zerldaten, zerldaten_null;

void ZERLDATEN_CLASS::prepare_lfd (void)
{
            ins_quest ((char *)   &zerldaten.lfd_i, 2, 0);
    out_quest ((char *) &zerldaten.mdn,1,0);
    out_quest ((char *) &zerldaten.fil,1,0);
    out_quest ((char *) &zerldaten.partie,2,0);
    out_quest ((char *) zerldaten.lief,0,17);
    out_quest ((char *) zerldaten.lief_rech_nr,0,17);
    out_quest ((char *) zerldaten.ident_extern,0,21);
    out_quest ((char *) zerldaten.ident_intern,0,21);
    out_quest ((char *) &zerldaten.a,3,0);
    out_quest ((char *) &zerldaten.a_gt,3,0);
    out_quest ((char *) zerldaten.kategorie,0,4);
    out_quest ((char *) &zerldaten.schnitt,1,0);
    out_quest ((char *) &zerldaten.gebland,1,0);
    out_quest ((char *) &zerldaten.mastland,1,0);
    out_quest ((char *) &zerldaten.schlaland,1,0);
    out_quest ((char *) &zerldaten.zerland,1,0);
    out_quest ((char *) zerldaten.esnum,0,11);
    out_quest ((char *) zerldaten.eznum1,0,11);
    out_quest ((char *) zerldaten.eznum2,0,11);
    out_quest ((char *) zerldaten.eznum3,0,11);
    out_quest ((char *) &zerldaten.dat,2,0);
    out_quest ((char *) &zerldaten.anz_gedruckt,1,0);
    out_quest ((char *) &zerldaten.anz_entnommen,1,0);
    out_quest ((char *) &zerldaten.lfd,2,0);
    out_quest ((char *) &zerldaten.lfd_i,2,0);
    out_quest ((char *) &zerldaten.zerlegt,1,0);
    out_quest ((char *) &zerldaten.aktiv,1,0);
    out_quest ((char *) zerldaten.eunum1,0,13);
    out_quest ((char *) zerldaten.eunum2,0,13);
            cursor_lfd = prepare_sql ("select zerldaten.mdn,  "
"zerldaten.fil,  zerldaten.partie,  zerldaten.lief,  "
"zerldaten.lief_rech_nr,  zerldaten.ident_extern,  "
"zerldaten.ident_intern,  zerldaten.a,  zerldaten.a_gt,  "
"zerldaten.kategorie,  zerldaten.schnitt,  zerldaten.gebland,  "
"zerldaten.mastland,  zerldaten.schlaland,  zerldaten.zerland,  "
"zerldaten.esnum,  zerldaten.eznum1,  zerldaten.eznum2,  "
"zerldaten.eznum3,  zerldaten.dat,  zerldaten.anz_gedruckt,  "
"zerldaten.anz_entnommen,  zerldaten.lfd,  zerldaten.lfd_i,  "
"zerldaten.zerlegt,  zerldaten.aktiv,  zerldaten.eunum1,  "
"zerldaten.eunum2 from zerldaten "

#line 18 "zerldaten.rpp"
                                  "where lfd_i = ? ");
}

void ZERLDATEN_CLASS::prepare (void)
{
            char *sqltext;
            ins_quest ((char *)   zerldaten.ident_intern, 0, 20);
            ins_quest ((char *)   &zerldaten.a, 3, 0);
            ins_quest ((char *)   &zerldaten.a_gt, 3, 0);
            ins_quest ((char *)   &zerldaten.mdn, 1, 0);
            ins_quest ((char *)   &zerldaten.fil, 1, 0);
            ins_quest ((char *)   zerldaten.ident_extern, 0, 20);
    out_quest ((char *) &zerldaten.mdn,1,0);
    out_quest ((char *) &zerldaten.fil,1,0);
    out_quest ((char *) &zerldaten.partie,2,0);
    out_quest ((char *) zerldaten.lief,0,17);
    out_quest ((char *) zerldaten.lief_rech_nr,0,17);
    out_quest ((char *) zerldaten.ident_extern,0,21);
    out_quest ((char *) zerldaten.ident_intern,0,21);
    out_quest ((char *) &zerldaten.a,3,0);
    out_quest ((char *) &zerldaten.a_gt,3,0);
    out_quest ((char *) zerldaten.kategorie,0,4);
    out_quest ((char *) &zerldaten.schnitt,1,0);
    out_quest ((char *) &zerldaten.gebland,1,0);
    out_quest ((char *) &zerldaten.mastland,1,0);
    out_quest ((char *) &zerldaten.schlaland,1,0);
    out_quest ((char *) &zerldaten.zerland,1,0);
    out_quest ((char *) zerldaten.esnum,0,11);
    out_quest ((char *) zerldaten.eznum1,0,11);
    out_quest ((char *) zerldaten.eznum2,0,11);
    out_quest ((char *) zerldaten.eznum3,0,11);
    out_quest ((char *) &zerldaten.dat,2,0);
    out_quest ((char *) &zerldaten.anz_gedruckt,1,0);
    out_quest ((char *) &zerldaten.anz_entnommen,1,0);
    out_quest ((char *) &zerldaten.lfd,2,0);
    out_quest ((char *) &zerldaten.lfd_i,2,0);
    out_quest ((char *) &zerldaten.zerlegt,1,0);
    out_quest ((char *) &zerldaten.aktiv,1,0);
    out_quest ((char *) zerldaten.eunum1,0,13);
    out_quest ((char *) zerldaten.eunum2,0,13);
            cursor = prepare_sql ("select zerldaten.mdn,  "
"zerldaten.fil,  zerldaten.partie,  zerldaten.lief,  "
"zerldaten.lief_rech_nr,  zerldaten.ident_extern,  "
"zerldaten.ident_intern,  zerldaten.a,  zerldaten.a_gt,  "
"zerldaten.kategorie,  zerldaten.schnitt,  zerldaten.gebland,  "
"zerldaten.mastland,  zerldaten.schlaland,  zerldaten.zerland,  "
"zerldaten.esnum,  zerldaten.eznum1,  zerldaten.eznum2,  "
"zerldaten.eznum3,  zerldaten.dat,  zerldaten.anz_gedruckt,  "
"zerldaten.anz_entnommen,  zerldaten.lfd,  zerldaten.lfd_i,  "
"zerldaten.zerlegt,  zerldaten.aktiv,  zerldaten.eunum1,  "
"zerldaten.eunum2 from zerldaten "

#line 31 "zerldaten.rpp"
                                  "where ident_intern = ? and a_gt = ? and a = ? and lief = ? and dat = ? and mdn = ? and fil = ? ");
    ins_quest ((char *) &zerldaten.mdn,1,0);
    ins_quest ((char *) &zerldaten.fil,1,0);
    ins_quest ((char *) &zerldaten.partie,2,0);
    ins_quest ((char *) zerldaten.lief,0,17);
    ins_quest ((char *) zerldaten.lief_rech_nr,0,17);
    ins_quest ((char *) zerldaten.ident_extern,0,21);
    ins_quest ((char *) zerldaten.ident_intern,0,21);
    ins_quest ((char *) &zerldaten.a,3,0);
    ins_quest ((char *) &zerldaten.a_gt,3,0);
    ins_quest ((char *) zerldaten.kategorie,0,4);
    ins_quest ((char *) &zerldaten.schnitt,1,0);
    ins_quest ((char *) &zerldaten.gebland,1,0);
    ins_quest ((char *) &zerldaten.mastland,1,0);
    ins_quest ((char *) &zerldaten.schlaland,1,0);
    ins_quest ((char *) &zerldaten.zerland,1,0);
    ins_quest ((char *) zerldaten.esnum,0,11);
    ins_quest ((char *) zerldaten.eznum1,0,11);
    ins_quest ((char *) zerldaten.eznum2,0,11);
    ins_quest ((char *) zerldaten.eznum3,0,11);
    ins_quest ((char *) &zerldaten.dat,2,0);
    ins_quest ((char *) &zerldaten.anz_gedruckt,1,0);
    ins_quest ((char *) &zerldaten.anz_entnommen,1,0);
    ins_quest ((char *) &zerldaten.lfd_i,2,0);
    ins_quest ((char *) &zerldaten.zerlegt,1,0);
    ins_quest ((char *) &zerldaten.aktiv,1,0);
    ins_quest ((char *) zerldaten.eunum1,0,13);
    ins_quest ((char *) zerldaten.eunum2,0,13);
            sqltext = "update zerldaten set "
"zerldaten.mdn = ?,  zerldaten.fil = ?,  zerldaten.partie = ?,  "
"zerldaten.lief = ?,  zerldaten.lief_rech_nr = ?,  "
"zerldaten.ident_extern = ?,  zerldaten.ident_intern = ?,  "
"zerldaten.a = ?,  zerldaten.a_gt = ?,  zerldaten.kategorie = ?,  "
"zerldaten.schnitt = ?,  zerldaten.gebland = ?,  "
"zerldaten.mastland = ?,  zerldaten.schlaland = ?,  "
"zerldaten.zerland = ?,  zerldaten.esnum = ?,  zerldaten.eznum1 = ?,  "
"zerldaten.eznum2 = ?,  zerldaten.eznum3 = ?,  zerldaten.dat = ?,  "
"zerldaten.anz_gedruckt = ?,  zerldaten.anz_entnommen = ?,  "
"zerldaten.lfd_i = ?,  zerldaten.zerlegt = ?,  "
"zerldaten.aktiv = ?,  zerldaten.eunum1 = ?,  zerldaten.eunum2 = ? "

#line 33 "zerldaten.rpp"
                                  "where ident_intern = ? and lief_rech_nr = ? and a = ? and a_gt = ? and mdn = ? and fil = ? and "
				   " ident_extern = ?";
                                  
            ins_quest ((char *)   zerldaten.ident_intern, 0, 20);
            ins_quest ((char *)   zerldaten.lief_rech_nr, 0, 18);
            ins_quest ((char *)   &zerldaten.a, 3, 0);
            ins_quest ((char *)   &zerldaten.a_gt, 3, 0);
            ins_quest ((char *)   &zerldaten.mdn, 1, 0);
            ins_quest ((char *)   &zerldaten.fil, 1, 0);
            ins_quest ((char *)   zerldaten.ident_extern, 0, 20);
            upd_cursor = prepare_sql (sqltext);
            
            ins_quest ((char *)   &zerldaten.aktiv, 2, 0);
            ins_quest ((char *)   &zerldaten.lfd, 2, 0);
            upd_cursor_lfd = prepare_sql ("update zerldaten set aktiv = ? where lfd = ? ");
            

            ins_quest ((char *)   zerldaten.ident_intern, 0, 20);
            ins_quest ((char *)   zerldaten.lief_rech_nr, 0, 16);
            ins_quest ((char *)   &zerldaten.a, 3, 0);
            ins_quest ((char *)   &zerldaten.a_gt, 3, 0);
            ins_quest ((char *)   &zerldaten.mdn, 1, 0);
            ins_quest ((char *)   &zerldaten.fil, 1, 0);
            ins_quest ((char *)   zerldaten.ident_extern, 0, 20);
            test_upd_cursor = prepare_sql ("select mdn from zerldaten "
                                  "where ident_intern = ? and lief_rech_nr = ? and a = ? and a_gt = ? and mdn = ? and fil = ? "
								  " and ident_extern = ?");

            ins_quest ((char *)   zerldaten.ident_intern, 0, 20);
            ins_quest ((char *)   zerldaten.lief_rech_nr, 0, 16);
            ins_quest ((char *)   &zerldaten.a, 3, 0);
            ins_quest ((char *)   &zerldaten.a_gt, 3, 0);
            ins_quest ((char *)   &zerldaten.mdn, 1, 0);
            ins_quest ((char *)   &zerldaten.fil, 1, 0);
            ins_quest ((char *)   zerldaten.ident_extern, 0, 20);
            del_cursor = prepare_sql ("delete from zerldaten "
                                  "where ident_intern = ? and lief_rech_nr = ? and a = ? and a_gt = ? and mdn = ? and fil = ? "
								  " and ident_extern = ?");
    ins_quest ((char *) &zerldaten.mdn,1,0);
    ins_quest ((char *) &zerldaten.fil,1,0);
    ins_quest ((char *) &zerldaten.partie,2,0);
    ins_quest ((char *) zerldaten.lief,0,17);
    ins_quest ((char *) zerldaten.lief_rech_nr,0,17);
    ins_quest ((char *) zerldaten.ident_extern,0,21);
    ins_quest ((char *) zerldaten.ident_intern,0,21);
    ins_quest ((char *) &zerldaten.a,3,0);
    ins_quest ((char *) &zerldaten.a_gt,3,0);
    ins_quest ((char *) zerldaten.kategorie,0,4);
    ins_quest ((char *) &zerldaten.schnitt,1,0);
    ins_quest ((char *) &zerldaten.gebland,1,0);
    ins_quest ((char *) &zerldaten.mastland,1,0);
    ins_quest ((char *) &zerldaten.schlaland,1,0);
    ins_quest ((char *) &zerldaten.zerland,1,0);
    ins_quest ((char *) zerldaten.esnum,0,11);
    ins_quest ((char *) zerldaten.eznum1,0,11);
    ins_quest ((char *) zerldaten.eznum2,0,11);
    ins_quest ((char *) zerldaten.eznum3,0,11);
    ins_quest ((char *) &zerldaten.dat,2,0);
    ins_quest ((char *) &zerldaten.anz_gedruckt,1,0);
    ins_quest ((char *) &zerldaten.anz_entnommen,1,0);
    ins_quest ((char *) &zerldaten.lfd_i,2,0);
    ins_quest ((char *) &zerldaten.zerlegt,1,0);
    ins_quest ((char *) &zerldaten.aktiv,1,0);
    ins_quest ((char *) zerldaten.eunum1,0,13);
    ins_quest ((char *) zerldaten.eunum2,0,13);
            ins_cursor = prepare_sql ("insert into zerldaten ("
"mdn,  fil,  partie,  lief,  lief_rech_nr,  ident_extern,  ident_intern,  a,  a_gt,  "
"kategorie,  schnitt,  gebland,  mastland,  schlaland,  zerland,  esnum,  eznum1,  "
"eznum2,  eznum3,  dat,  anz_gedruckt,  anz_entnommen,  lfd_i,  zerlegt,  aktiv,  "
"eunum1,  eunum2) "

#line 72 "zerldaten.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

#line 74 "zerldaten.rpp"
}


int ZERLDATEN_CLASS::dbreadfirst_lfd (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor_lfd == -1)
         {
                this->prepare_lfd ();
         }
         open_sql (cursor_lfd);
         return fetch_sql (cursor_lfd);
}

int ZERLDATEN_CLASS::dbclose_lfd (void)
{
        if (cursor_lfd != -1)
        {
                 close_sql (cursor_lfd);
                 cursor_lfd = -1;
        }
        return 0;
}

int ZERLDATEN_CLASS::dbupdate_lfd (void)
{
         if (upd_cursor_lfd == -1)
         {
                this->prepare ();
         }
            execute_curs (upd_cursor_lfd);
			return sqlstatus;
}

int ZERLDATEN_CLASS::dbupdate (void)
/**
Tabelle eti Updaten.
**/
{

         zerldaten.aktiv = 1;
         if (test_upd_cursor == -1)
         {
             prepare ();
         }
         open_sql (test_upd_cursor);
         fetch_sql (test_upd_cursor);
         if (sqlstatus == 100)
         {
                   execute_curs (ins_cursor);
         }  
         else if (sqlstatus == 0)
         {
//                    execute_curs (upd_cursor);
         }  
          
         return sqlstatus;
} 
