//100407 abspeichern mit F12 externe identnumer soll nicht in die interne Identnummer �bernommen werden 
 //16.4.07 wieder r�ckg�ngig  gemacht 
#include<stdio.h>
#include<stdarg.h>
#include<string.h>
#include<windows.h>
#include "stdfkt.h"
#include "strfkt.h"
#include "spezdlg.h" 
#include "mo_gdruck.h" 
#include "colbut.h"
#include "help.h"
#include "lbox.h"
#include "searchmdn.h"
#include "searchfil.h"
#include "searchzerldaten.h"
#include "searchlief.h"
#include "searcha.h"
#include "etubi.h"
#include "ptab.h"
#include "bsd_buch.h"
#ifdef BIWAK
#include "conf_env.h"
#endif
#include "mo_progcfg.h"

BOOL RFE_Lohmann; // 24.06.2013 GK

char cbem [12];
bool ComboGelesen = FALSE;
bool NachdruckAktiv = FALSE;
int anzComboStaat = 0;
static int StdSize = STDSIZE;

static int dlgsize    = STDSIZE;
static int ltgraysize = STDSIZE;
static int dlgpossize = STDSIZE;
static int cpsize     = 200;
bool inpos = FALSE;
static struct PMENUE MenuePosEnter = {"Positionen erfassen F6",      "G",  NULL, IDM_LIST}; 
static struct PMENUE MenueInsert   = {"einf�gen F6",                 "G",  NULL, IDM_INSERT}; 

static char eti_schlacht [80] = {"ddorf_zerl.etk"} ;
static char eti_schl_drk [80] = {"drucker"} ;
static int eti_schl_typ = 5 ;
static int eti_testen = 0 ;
static int flg_we = 0 ;
static int flg_kat_zwang = 0 ;
static int Mandant = 0 ;
static int FontSizeIdentNr = 150;
static int lief_es_ez = 0 ;
static BOOL we_vorhanden = FALSE ;

static char eti_zerleg [80] = {"ddorf_zerl.etk"} ;
static char eti_zerl_drk [80] = {"drucker"} ;
static int eti_zerl_typ = 5 ;
char **SpezDlg::ComboStaat1 = NULL;
char **SpezDlg::ComboStaat2 = NULL;

SEARCHLIEF SearchLief;

static mfont dlgfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         GRAYCOL,
                         0,
                         NULL};

static mfont ltgrayfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         LTGRAYCOL,
                        0,
                         NULL};

static mfont checkfonth = {
                           "ARIAL", STDSIZE, 0, 0,
                            BLACKCOL,
                            LTGRAYCOL,
                            0,
                            NULL};

static mfont dlgposfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         LTGRAYCOL,
                         0,
                         NULL};


static mfont cpfont = {
                        "Times New Roman", 200, 0, 1,
                         BLUECOL,
                         GRAYCOL,
                         0,
                         NULL};


struct ZERLDATENF zerldatenf, zerldatenf_null;
ZERLDATEN_CLASS ZerldatenClass;

COLORREF SpezDlg::Colors[] = {BLACKCOL,
                            WHITECOL,
                            WHITECOL,
                            WHITECOL};

COLORREF SpezDlg::BkColors[] = {WHITECOL,
                            BLUECOL,
                            BLACKCOL,
                            GRAYCOL};

HBITMAP SpezDlg::ListBitmap  = NULL;
HBITMAP SpezDlg::ListBitmapi = NULL;
BitImage SpezDlg::ImageDelete;
BitImage SpezDlg::ImageInsert;
BitImage SpezDlg::ImageList;
COLORREF SpezDlg::SysBkColor = LTGRAYCOL;
BOOL SpezDlg::ListButtons = FALSE;
int SpezDlg::ButtonSize = SMALL;
char cwhere [256];

mfont *SpezDlg::Font = &dlgposfont;

inline void NULLERROR (void *Ptr) 
{
        if (Ptr == NULL) 
        {
            disp_mess ("Fehler bei dr Speicherzuordnung", 2);
            ExitProcess (1);
        }
}

char eznum2 [11];

CFIELD *SpezDlg::_fHead [] = {
// Hier Kopffelder eintragen
// Mandant 
                     new CFIELD ("mdn_txt", "Mandant",  0, 0, 1, 1,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("mdn", zerldatenf.mdn,  6, 0, 10, 1,  NULL, "%4d", CEDIT,
                                 MDN_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("mdn_choise", "", 2, 0, 16, 1, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("mdn_krz", zerldatenf.mdn_krz, 19, 0, 19, 1,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, 0),
//Filiale
                     new CFIELD ("fil_txt", "Filiale",  0, 0, 50, 1,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("fil", zerldatenf.fil,  6, 0, 58, 1,  NULL, "%4d", CEDIT,
                                 FIL_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("fil_choise", "", 2, 0, 64, 1, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("fil_krz", zerldatenf.fil_krz, 19, 0, 67, 1,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, 0),
//Datum
                     new CFIELD ("dat_txt", "Datum", 18,	0, 50,2,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("datum", zerldatenf.dat, 11, 0,67,2,  NULL, "dd.mm.yyyy", 
                                 CEDIT,
                                 DAT_CTL, Font, 0, 0),

//Partie
                     new CFIELD ("partienr_txt", "Tagespartie",  18, 0, 50, 3,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("partienr", zerldatenf.partie, 9, 0, 67, 3,  NULL, "%ld", 
                                 CREADONLY,
                                 500, Font, 0, 0),
//Lieferant 
                     new CFIELD ("lief_txt", "Lieferant",  0, 0, 1, 3,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("lief", zerldatenf.lief, 17, 0, 10, 3,  NULL, "", CEDIT,
                                 LIEF_CTL, Font, 0, 0),
                     new CFIELD ("lief_choise", "", 2, 0, 27, 3, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("lief_krz", zerldatenf.lief_krz, 18, 0, 30, 3,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, 0),
//Lief/Rech
                     new CFIELD ("lief_rech_nr_txt", "Lief/Rech",  0, 0, 1, 4,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("lief_rech_nr", zerldatenf.lief_rech_nr, 17, 0, 10, 4,  NULL, "", 
					             CEDIT,
                                 LIEF_RECH_NR_CTL, Font, 0, 0),
                     new CFIELD ("lief_rech_nr_choise", "", 2, 0, 27, 4, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
//Artikel 
                     new CFIELD ("a_txt", "Artikel",  0, 0, 1, 6,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("a_gt", zerldatenf.a_gt, 17, 0, 10, 6,  NULL, "", CEDIT,
                                 ARTIKEL_CTL, Font, 0, 0),
                     new CFIELD ("a_choise", "", 2, 0, 27, 6, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("a_bez", zerldatenf.a_bez, 25, 0, 30, 6,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, 0),
//externe Identnummer
                     new CFIELD ("ident_extern_txt", "Identnummer",  0, 0, 57, 6,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("ident_extern", zerldatenf.ident_extern, 20, 0, 68, 6,  NULL, "", 
					             CEDIT,
                                 IDENT_EXTERN_CTL, Font, 0, 0),
                     new CFIELD ("ident_extern_choise", "", 2, 0, 88, 6, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     NULL,
};
CFORM SpezDlg::fHead (26, _fHead);


CFIELD *SpezDlg::_fPos [] = {
//interne Identnummer
	                     new CFIELD ("pos_frame", "",    90,16, 1, 8,  NULL, "",
                                 CBORDER,   
                                 500, Font, 0, TRANSPARENT),    
	                     new CFIELD ("pos_frame1", "",    0,0, 0, 2,  NULL, "",
                                 CREMOVED,   
                                 500, Font, 0, TRANSPARENT),    

                     new CFIELD ("ident_intern_txt", "Interne Identnummer",  0, 0, 50, 4,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("ident_intern", zerldatenf.ident_intern, 20, 0, 67, 4,  NULL, "", 
					             CREADONLY,
                                 IDENT_INTERN_CTL, Font, 0, 0),
//Kategorie
                     new CFIELD ("tier_kz_txt", "Kategorie",  0, 0, 44, 9,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("tier_kz", zerldatenf.tier_kz, 4, 0, 55, 9,  NULL, "", CEDIT ,
                                 KATEGORIE_CTL, Font, 0, ES_UPPERCASE),
                     new CFIELD ("tier_kz_choise", "", 2, 0, 59, 9, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("tier_kz_krz", zerldatenf.tier_kz_krz, 19, 0, 67, 9,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, 0),
//Geburt
                     new CFIELD ("geburt_txt", "Geburt",  0, 0, 34, 11,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("geburt", zerldatenf.gebland, 16, 10, 45, 11,  NULL, "", CCOMBOBOX,
                                 GEBURT_CTL, Font, 0, CBS_DROPDOWNLIST | WS_VSCROLL),


//Mast
                     new CFIELD ("mast_txt", "Mast",  0, 0, 34, 12,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("mast", zerldatenf.mastland, 16, 10, 45, 12,  NULL, "", CCOMBOBOX,
                                 MAST_CTL, Font, 0, CBS_DROPDOWNLIST | WS_VSCROLL),
//Schlachtung
                     new CFIELD ("schlachtung_txt", "Schlachtung",  0, 0, 34, 13,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("schlachtung", zerldatenf.schlaland, 16, 10, 45, 13,  NULL, "", CCOMBOBOX,
                                 SCHLACHT_CTL, Font, 0, CBS_DROPDOWNLIST | WS_VSCROLL),
//ES-Nr
                     new CFIELD ("esnum_txt", "ES-Nr.",  0, 0, 34, 15,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("esnum", zerldatenf.esnum, 10, 0, 45, 15,  NULL, "", CEDIT,
                                 ES_CTL, Font, 0, 0),

//Zerlegung
                     new CFIELD ("zerlegung_txt", "Zerlegung",  0, 0, 65, 11,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("zerlegung", zerldatenf.zerland, 16, 10, 74, 11,  NULL, "", CCOMBOBOX,
                                 ZERLEGUNG_CTL, Font, 0, CBS_DROPDOWNLIST | WS_VSCROLL),

//EZ1-Nr
                     new CFIELD ("ez1_txt", "EZ-Nr.",  0, 0, 65, 13,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("eznum1", zerldatenf.eznum1, 10, 0, 74, 13,  NULL, "", CEDIT,
                                 EZ1_CTL, Font, 0, 0),
//EZ2-Nr
                     new CFIELD ("ez2_txt", "EZ-Nr.",  0, 0, 65, 14,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("eznum2", eznum2, 10, 0, 74, 14,  NULL, "", CREADONLY,
                                 EZ2_CTL, Font, 0, 0),
//EZ3-Nr
//                     new CFIELD ("ez3_txt", "EZ-Nr.",  0, 0, 65, 15,  NULL, "", 
//                                 CDISPLAYONLY,
//                                 500, Font, 0, TRANSPARENT),
//                     new CFIELD ("eznum3", zerldatenf.eznum3, 10, 0, 74, 15,  NULL, "", CREADONLY,
//                                 EZ3_CTL, Font, 0, 0),

                     new CFIELD ("eu1_txt", "EU-Nr1",  0, 0, 65, 15,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("eunum1", zerldatenf.eunum1, 12, 0, 74, 15,  NULL, "", CEDIT,
                                 EU1_CTL, Font, 0, 0),

                     new CFIELD ("eu2_txt", "EU-Nr2",  0, 0, 65, 16,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("eunum2", zerldatenf.eunum2, 12, 0, 74, 16,  NULL, "", CEDIT,
                                 EU2_CTL, Font, 0, 0),
//Anzahl Etiketten
                     new CFIELD ("anz_eti_txt", "Anzahl Etiketten",  0, 0, 45, 18,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("anz_eti", zerldatenf.anz_gedruckt,  6, 0, 65, 18,  NULL, "%3d", CEDIT,
                                 ANZ_ETI_CTL, Font, 0, ES_RIGHT),
//                     new CFIELD ("datum", zerldatenf.dat, 17, 0, 67, 3,  NULL, "dd.mm.yyyy", 
//                                 CREADONLY,
//                                 500, Font, 0, 0),
//                     new CFIELD ("test_frame", "",    90,-1, 1, 5,  NULL, "",
//                                 CBORDER,   
//                                 500, Font, 0, TRANSPARENT),    

//                     new CFIELD ("lfd", zerldatenf.lfd,  6, 0, 88, 12,  NULL, "%6d", CREADONLY,
//                                 LFD_CTL, Font, 0, ES_RIGHT),

// Hier Positionsfelder eintragen

                     NULL,
};



CFORM SpezDlg::fPos (27, _fPos);


CFIELD *SpezDlg::_fcField [] = {
                     new CFIELD ("fcField1", (CFORM *) &fHead, 0, 0, 0, 0, NULL, "", CFFORM,
                                   HEADCTL, Font, 0, 0),
                     new CFIELD ("fcField2", (CFORM *) &fPos,  0, 0, 0, 0, NULL, "", CFFORM,
                                   POSCTL, Font, 0, 0),
                     NULL,                      
};

CFORM SpezDlg::fcForm (3, _fcField);

CFIELD *SpezDlg::_fcField0 [] = { 
                     new CFIELD ("fcForm", (CFORM *) &fcForm, 0, 0, -1, 1, NULL, "", CFFORM,
                                   500, Font, 0, 0),
                     NULL,
};
CFORM SpezDlg::fcForm0 (1, _fcField0);

Work SpezDlg::work;
char *SpezDlg::HelpName = "defaultlist.cmd";
SpezDlg *SpezDlg::ActiveSpezDlg = NULL;
HBITMAP SpezDlg::SelBmp;
int SpezDlg::pos = 0;
char SpezDlg::ptwert[5];


void SpezDlg::ChangeF6 (DWORD Id)
{

    if (Id == IDM_LIST)
    {
         if (DeleteMenu (ActiveDlg->GethMenu (), IDM_LIST , MF_BYCOMMAND)) 
         {
                InsertMenu (ActiveDlg->GethMenu (), IDM_DOCKLIST, MF_BYCOMMAND | MF_STRING, 
                            MenueInsert.menid, MenueInsert.text);
                            
         }
    }
    else if (Id == IDM_INSERT)
    {
         if (DeleteMenu (ActiveDlg->GethMenu (), IDM_INSERT , MF_BYCOMMAND))
         {
                 InsertMenu (ActiveDlg->GethMenu (), IDM_DOCKLIST, MF_BYCOMMAND | MF_STRING, 
                            MenuePosEnter.menid, MenuePosEnter.text);
         }
    }
}


int SpezDlg::ReadIdent (void) //Einstieg in den ZerldatenSatz todo
{
         if (syskey == 0) return 0;
		 AktiviereSchlacht(TRUE);
		 AktiviereZerlege(TRUE);
         fHead.SetText (); 
         fPos.SetText (); 
         fHead.GetText (); 
         fPos.GetText (); 
         FromForm (dbheadfields);
         FromForm (dbfields);


		 if ( RFE_Lohmann ) 
		 {
            PostMessage (NULL, WM_KEYDOWN, VK_F7, 0l);
			 return 0; 
		 }
		 
		 if (strlen(clipped(zerldatenf.ident_extern)) == 0)
		 {
	            disp_mess ("Bitte erst die Identnummer erfassen!", 0);
                fHead.SetCurrentName ("ident_extern");
				return 0;
		 }

		 if (NachdruckAktiv == FALSE)
		 {
			Enable (&fcForm, EnableHead, FALSE); 
			if (Mandant == 0) Enable (&fcForm, EnableHeadMdn, FALSE); 
			Enable (&fcForm, EnableHead2, FALSE); 
			inpos = TRUE;
			Enable (&fcForm, EnablePos, TRUE); 
			fPos.SetCurrentName ("tier_kz");
		 }
		 ReadLief(); //190407
		 
		 return 0;
/***
         if (syskey == KEY9) return 0;
	     if (strcmp (zerldatenf.ohrmarke, "") == 0) return 0;

         fPos.GetText ();
         work.InitRec ();
         FromForm (dbheadfields);
         if (work.ReadErzeug (zerldatenf.ohrmarke))
		 {
			 ShowIdent(zerldatenf.ohrmarke);
			 return 0;
		 }
		 AktiviereSchlacht(TRUE);
         ToForm (dbheadfields);
         ToForm (dbfields);
         work.GetPtab (zerldatenf.tier_kz_krz, "tier_kz", zerldatenf.tier_kz);
         dlong_to_asc (zerldaten.schl_dat, zerldatenf.datum);
         fHead.SetText ();
         fPos.SetText ();
		 AktiviereZerlege(TRUE);
         return 0;
*******/
}

int SpezDlg::ReadTier_kz (void)
{
         if (syskey == 0) return 0;
         if (syskey == KEY9) return 0;

         fPos.GetText ();
         FromForm (dbfields);
         if (work.GetPtab (zerldatenf.tier_kz_krz, "tier_kz", zerldatenf.tier_kz) == 0)
		 {
			 AktiviereZerlege(TRUE);
		 }
		 else
		 {
			 AktiviereZerlege(TRUE);
		 }
         fPos.SetText ();
         return 0;
}
int SpezDlg::ReadWe (void)
{
         if (syskey == 0) return 0;
         if (syskey == KEY9) return 0;
		 if (flg_we == 0) return 0;

         fPos.GetText ();
         FromForm (dbfields);
		 if (we_vorhanden == TRUE)
		 {
			if (work.readwe() == 0)
			{
				we_vorhanden = TRUE;
			}
			else
			{
	            disp_mess ("Dieser Artikel ist nicht im Wareneingang", 0);
				if (flg_we == 2)
				{
                      fHead.SetCurrentName ("a_gt");
				}

			}
		 }
         fPos.SetText ();
         return 0;
}
int SpezDlg::ReadWeKopf (void)
{
	if (strlen(clipped(zerldatenf.lief_rech_nr)) == 0) return 0;
	if (flg_we != 0)
	{
		if (work.readwekopf() == 0)
		{
			we_vorhanden = TRUE;
		}
		else
		{
			we_vorhanden = FALSE;
			disp_mess ("Wareneingang nicht gefunden", 0);
			if (flg_we == 2)
			{
                      fHead.SetCurrentName ("lief_rech_nr");
			}
		}
	}
	return 0;
}
int SpezDlg::ReadLief (void)
{
         if (syskey == 0) return 0;
         if (syskey == KEY9) return 0;
	     if (strcmp (zerldatenf.lief, "") == 0) return 0;

         fHead.GetText ();
         work.InitRec ();
         FromForm (dbheadfields);
         if (!numeric (zerldatenf.lief))
         {
             if (ActiveSpezDlg->ShowLief (zerldatenf.lief) == FALSE)
             {
                      fHead.SetCurrentName ("lief");
                      return 0;
             }
         }
         if (work.ReadLief (zerldatenf.lief, zerldatenf.lief_krz))
		 {
                    fHead.SetCurrentName ("lief");
					return 1;

		 }
		 if (lief_es_ez == 1)
		 {
			 strcpy(zerldatenf.esnum, lief.esnum);  
			 strcpy(zerldatenf.eznum1, lief.eznum);
		 }
		 zerldaten.gebland = lief.geburt;
		 zerldaten.mastland = lief.mast;
		 zerldaten.schlaland = lief.schlachtung;
		 zerldaten.zerland = lief.zerlegung;
		 if (zerldaten.gebland < 0 || zerldaten.gebland > anzComboStaat) zerldaten.gebland = 0;
		 if (zerldaten.mastland < 0 || zerldaten.mastland > anzComboStaat) zerldaten.mastland = 0;
		 if (zerldaten.schlaland < 0 || zerldaten.schlaland > anzComboStaat) zerldaten.schlaland = 0;
		 if (zerldaten.zerland < 0 || zerldaten.zerland > anzComboStaat) zerldaten.zerland = 0;
         if (!RFE_Lohmann) 		 sysdate (zerldatenf.dat);
		 work.PruefePartie();

         fHead.SetText ();
			 fPos.GetCfield ("geburt")->SetPosCombo (zerldaten.gebland); //todo
			 fPos.GetCfield ("mast")->SetPosCombo (zerldaten.mastland); 
			 fPos.GetCfield ("schlachtung")->SetPosCombo (zerldaten.schlaland);
			 fPos.GetCfield ("zerlegung")->SetPosCombo (zerldaten.zerland);
         fPos.SetText ();

         return 0;
}
int SpezDlg::ReadA (void)
{
         if (syskey == 0) return 0;
         if (syskey == KEY9) return 0;
	     if (strcmp (clipped(zerldatenf.a_gt), "") == 0) return 0;

         fHead.GetText ();
         work.InitRec ();
         FromForm (dbheadfields);
		 if (NachdruckAktiv == TRUE)
		 {
			 //Auswahl f�r Nachdruck
			 /* auch hier a_bez und tier_kz initialisieren! GK 20130624 */
	         if (!numeric (zerldatenf.a_gt))
			{
	             if (ActiveSpezDlg->ShowA (zerldatenf.a_gt) == FALSE)
	             {
	                      fHead.SetCurrentName ("a_gt");
	                      return 0;
	             }
	         }
	         if (work.ReadA (ratod(zerldatenf.a_gt), zerldatenf.a_bez, zerldatenf.tier_kz))
			 {
	                    fHead.SetCurrentName ("a_gt");
						return 1;
	
			 }
		 }
		 else
		 {
	         if (!numeric (zerldatenf.a_gt))
			{
	             if (ActiveSpezDlg->ShowA (zerldatenf.a_gt) == FALSE)
	             {
	                      fHead.SetCurrentName ("a_gt");
	                      return 0;
	             }
	         }
	         if (work.ReadA (ratod(zerldatenf.a_gt), zerldatenf.a_bez, zerldatenf.tier_kz))
			 {
	                    fHead.SetCurrentName ("a_gt");
						return 1;
	
			 }
		 }
         fHead.SetText ();
         fPos.SetText ();
		 ReadTier_kz();
		 ReadWe();
         return 0;
}
int SpezDlg::SetAnz (void)
{
         if (syskey == 0) return 0;
         if (syskey == KEY9) return 0;

         fPos.SetText ();
		 zerldaten.anz_gedruckt = atoi(zerldatenf.anz_gedruckt);
		 AktiviereSchlacht(TRUE);
         return 0;
}
int SpezDlg::ReadMdn (void)
{
         if (syskey == 0) return 0;
         if (syskey == KEY9) return 0;

         fPos.GetText ();
         FromForm (dbheadfields);
         if (work.GetMdnName (zerldatenf.mdn_krz,eznum2,  atoi (zerldatenf.mdn)))
         {
                    fHead.SetCurrentName ("mdn");
					return 1;
         }

         if (atoi (zerldatenf.mdn) == 0)
         {
             sprintf (zerldatenf.fil, "%4d", 0);
             work.GetFilName (zerldatenf.fil_krz,   atoi (zerldatenf.mdn), atoi (zerldatenf.fil));
             Enable (&fcForm, EnableHeadFil, FALSE); 
             fHead.SetCurrentName ("mdn");
         }
         else
         {
             if (fHead.GetCfield ("fil")->IsDisabled ())
             {
                    if (Mandant == 0) Enable (&fcForm, EnableHeadFil, TRUE); 
                    fHead.SetCurrentName ("fil");
             }
         }
         if (syskey == KEYUP)
         {
                    fHead.SetCurrentName ("mdn");
         }
         fHead.SetText ();
         return 0;
}

int SpezDlg::ReadFil (void)
{
          
         if (syskey == 0) return 0;
         if (syskey == KEY9) return 0;
         fPos.GetText ();
         FromForm (dbheadfields);
         if (work.GetFilName (zerldatenf.fil_krz,   atoi (zerldatenf.mdn), atoi (zerldatenf.fil)))
         {
			 if (atoi(zerldatenf.fil) != 0)
			 {
                    fHead.SetCurrentName ("fil");
					return 1;
			 }
         }
         fHead.SetText ();
         return 0;
}

int SpezDlg::EnterPos (void)
{

//Hier Postionsdialog eintragen, falls vorhanden.
/*
         switch (syskey)
         {
               case KEYDOWN :
               case KEYTAB :
                   fHead.SetCurrentName ("mdn");
                   return TRUE;
         }
        
         if (DLG::MousePressed)
         {
             return FALSE;
         }

         if (syskey != KEYCR)
         {
             return FALSE;
         }

         
         fHead.GetText ();

         FromForm (dbheadfields);

         work.BeginWork ();
*/

         ToolBar_SetState (ActiveDlg->GethwndTB (),   IDM_DELALL, TBSTATE_ENABLED);
         EnableMenuItem (ActiveDlg->GethMenu (),      IDM_LIST,     MF_ENABLED);
         EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELETE,   MF_ENABLED);
         EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELALL,   MF_ENABLED);
         EnableMenuItem (ActiveDlg->GethMenu (),      VK_INSERT,    MF_ENABLED);
         EnableMenuItem (ActiveDlg->GethMenu (),      VK_F3,       MF_ENABLED);
         EnableMenuItem (ActiveDlg->GethMenu (),      VK_F6,       MF_ENABLED);
         if (ActiveSpezDlg->GetToolbar2 () != NULL)
         {
                 if  (ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f6") != NULL)
                 {
                      ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f6")->GetFeld ())->bmp = 
                                   ImageList.Image;
                 }
                 ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("del")->GetFeld ())->bmp = 
					 ImageDelete.Image;
                 ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("insert")->GetFeld ())->bmp = 
					 ImageInsert.Image;
                 if  (ActiveSpezDlg->GetToolbar2 ()->GetCfield ("Texte") != NULL)
                 {
                      ActiveSpezDlg->GetToolbar2 ()->GetCfield ("Texte")->Enable (TRUE);
                 }
                 ActiveSpezDlg->GetToolbar2 ()->display ();
         }
// Bei zus�tzlichen Positionseingaben Positionsfelder aktiv setzen
//         Enable (&fcForm, EnableHead, FALSE); 
//         Enable (&fcForm, EnablePos,  TRUE); 
// Erstes Positionsfeld setzen. 
//         fPos.SetCurrentName ("adr_krz");

         return 0;
}


int SpezDlg::GetPtab (void)
{
//         char wert [5];
         CFIELD *Cfield;


         fPos.GetText ();

         Cfield = ActiveDlg->GetCurrentCfield ();
         return 0;
}


ItProg *SpezDlg::After [] = {
                                   new ItProg ("tier_kz",         ReadTier_kz),
                                   new ItProg ("ident_extern",         ReadIdent),
                                   new ItProg ("lief_rech_nr",         ReadWeKopf),
//                                   new ItProg ("ident_intern",         ReadIdent),
                                   new ItProg ("lief",         ReadLief),
                                   new ItProg ("anz_eti",         SetAnz),
                                   new ItProg ("mdn",         ReadMdn),
                                   new ItProg ("fil",         ReadFil),
                                   new ItProg ("a_gt",         ReadA),
//                                   new ItProg ("gadr1",         SetAnz),
//                                   new ItProg ("gstr",         SetAnz),
//                                   new ItProg ("gplz",         SetAnz),
//                                   new ItProg ("gort",         SetAnz),
                                   NULL,
};

ItProg *SpezDlg::Before [] = {
								NULL,
};

ItFont *SpezDlg::itFont [] = {
//                                  new ItFont ("a_txt",           &ltgrayfont),
                                  NULL
};

FORMFIELD *SpezDlg::dbheadfields [] = {
         new FORMFIELD ("mdn",            zerldatenf.mdn,            (short *)   &zerldaten.mdn,            FSHORT,   NULL),
         new FORMFIELD ("fil",            zerldatenf.fil,            (short *)   &zerldaten.fil,            FSHORT,   NULL),
         new FORMFIELD ("lief",           zerldatenf.lief,           (char *)    zerldaten.lief,            FCHAR,   NULL),
         new FORMFIELD ("lief_rech_nr",   zerldatenf.lief_rech_nr ,  (char *)   zerldaten.lief_rech_nr,     FCHAR,   NULL),
         new FORMFIELD ("ident_extern",   zerldatenf.ident_extern ,  (char *)    zerldaten.ident_extern,    FCHAR,    NULL),
         new FORMFIELD ("partienr",       zerldatenf.partie ,        (long *)    &zerldaten.partie,         FLONG,    NULL),
         new FORMFIELD ("a_gt",           zerldatenf.a_gt ,          (double *)    &zerldaten.a_gt,         FDOUBLE,    NULL),
           NULL,
};


FORMFIELD *SpezDlg::dbfields [] = {
         new FORMFIELD ("tier_kz",         zerldatenf.tier_kz ,        (char *)    zerldaten.kategorie,     FCHAR,    NULL),
         new FORMFIELD ("anz_eti",    zerldatenf.anz_gedruckt ,   (short *)   &zerldaten.anz_gedruckt, FSHORT,    NULL),
//         new FORMFIELD ("datum",       zerldatenf.dat ,            (long *)    zerldaten.dat,           FDATE,    "dd.mm.yyyy"),
          new FORMFIELD ("ident_intern",    zerldatenf.ident_intern ,   (char *)    zerldaten.ident_intern,  FCHAR,    NULL),
         new FORMFIELD ("geburt",          zerldatenf.gebland ,        (short *)    &zerldaten.gebland,       FSHORT,    NULL),
         new FORMFIELD ("mast",            zerldatenf.mastland ,       (short *)    &zerldaten.mastland,      FSHORT,    NULL),
         new FORMFIELD ("schlachtung",     zerldatenf.schlaland ,      (short *)    &zerldaten.schlaland,     FSHORT,    NULL),
         new FORMFIELD ("zerlegung",       zerldatenf.zerland ,        (short *)    &zerldaten.zerland,       FSHORT,    NULL),
         new FORMFIELD ("esnum",           zerldatenf.esnum ,          (char *)    zerldaten.esnum,         FCHAR,    NULL),
         new FORMFIELD ("eznum1",          zerldatenf.eznum1 ,         (char *)    zerldaten.eznum1,        FCHAR,    NULL),
         new FORMFIELD ("eunum1",          zerldatenf.eunum1 ,         (char *)    zerldaten.eunum1,        FCHAR,    NULL),
         new FORMFIELD ("eunum2",          zerldatenf.eunum2 ,         (char *)    zerldaten.eunum2,        FCHAR,    NULL),
//         new FORMFIELD ("eznum2",          zerldatenf.eznum2 ,         (char *)    zerldaten.eznum2,        FCHAR,    NULL),
//         new FORMFIELD ("eznum3",          zerldatenf.eznum3 ,         (char *)    zerldaten.eznum3,        FCHAR,    NULL),
         NULL,
};



char *SpezDlg::EnableHead [] = {
                                "lief",
                                "lief_rech_nr",
                                 NULL
};
char *SpezDlg::EnableHeadMdn [] = {
							    "mdn",
							    "fil",
                                 NULL
};
char *SpezDlg::EnableHead2 [] = {
							    "a_gt",
							    "ident_extern",
                                 NULL
};
char *SpezDlg::EnableNachdruck [] = {
								 "datum",
							    "a_gt",
								"a_bez",
							    "ident_extern", 
                                 NULL
};

char *SpezDlg::EnableHeadFil [] = {
                                     "fil",
                                     NULL
};
char *SpezDlg::EnableDatum [] = {
                                     "datum",
                                    "a_bez",
                                     NULL
};


char *SpezDlg::EnablePos[] = {
								"tier_kz",
								"geburt",
								"mast",
								"schlachtung",
								"zerlegung",
								"esnum",
								"eznum1",
								"eunum1",
								"eunum2",
//								"eznum2",
//								"eznum3",
//								"ident_intern",
								"anz_eti",
                               NULL,
};

                         
SpezDlg::SpezDlg (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel) :
         DLG (x, y, cx, cy, Caption, Size, Pixel)
{
          
             BorderType = HIGHCOLBORDER;
             Init (x, y, cx, cy, Caption, Size, Pixel);
}

SpezDlg::SpezDlg (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel, 
                  int BorderType) :
         DLG (x, y, cx, cy, Caption, Size, Pixel)
{
          
             this->BorderType = BorderType;
             Init (x, y, cx, cy, Caption, Size, Pixel);
}

SpezDlg::SpezDlg (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel, 
                  int BorderType, int EnterM) :
         DLG (x, y, cx, cy, Caption, Size, Pixel)
{
          
             this->BorderType = BorderType;
//             EnterMode = EnterM;
             Init (x, y, cx, cy, Caption, Size, Pixel);
}


void SpezDlg::Init (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel)
{
//             SEARCHLIEF SearchLief;
             PROG_CFG ProgCfg ("21900");
             char cfg_v [256];
             int i;
             int xfull, yfull;


             if (ProgCfg.GetCfgValue ("FontSizeIdentNr", cfg_v) == TRUE)
             {
					FontSizeIdentNr = atoi(cfg_v);
             }
             if (ProgCfg.GetCfgValue ("Mandant", cfg_v) == TRUE)
             {
					Mandant = atoi(cfg_v);
             }
             if (ProgCfg.GetCfgValue ("Kategoriezwang", cfg_v) == TRUE)
             {
					flg_kat_zwang = atoi(cfg_v);
             }
             if (ProgCfg.GetCfgValue ("lief_es_ez", cfg_v) == TRUE)
             {
					lief_es_ez = atoi(cfg_v);
             }
             if (ProgCfg.GetCfgValue ("Wareneingang", cfg_v) == TRUE)
             {
					flg_we = atoi(cfg_v);
             }
             if (ProgCfg.GetCfgValue ("eti_schlacht", cfg_v) == TRUE)
             {
			        strcpy (eti_schlacht, cfg_v);
             }
             if (ProgCfg.GetCfgValue ("eti_schl_drk", cfg_v) == TRUE)
             {
			        strcpy (eti_schl_drk, cfg_v);
             }
             if (ProgCfg.GetCfgValue ("eti_schl_typ", cfg_v) == TRUE)
             {
					eti_schl_typ = atoi(cfg_v);
             }
             if (ProgCfg.GetCfgValue ("eti_testen", cfg_v) == TRUE)
             {
					eti_testen = atoi(cfg_v);
             }
             if (ProgCfg.GetCfgValue ("eti_zerleg", cfg_v) == TRUE)
             {
			        strcpy (eti_zerleg, cfg_v);
             }
             if (ProgCfg.GetCfgValue ("eti_zerl_drk", cfg_v) == TRUE)
             {
			        strcpy (eti_zerl_drk, cfg_v);
             }
             if (ProgCfg.GetCfgValue ("eti_zerl_typ", cfg_v) == TRUE)
             {
					eti_zerl_typ = atoi(cfg_v);
             }
             if (ProgCfg.GetCfgValue ("liefvon", cfg_v) == TRUE)
             {
					SearchLief.SetLiefVon (atoi(cfg_v));
             }
             if (ProgCfg.GetCfgValue ("liefbis", cfg_v) == TRUE)
             {
					SearchLief.SetLiefBis (atoi(cfg_v));
             }

//             Work.Prepare ();
			 strcpy (cwhere," ");

             ImageDelete.Image    = BMAP::LoadBitmap (hInstance, "DEL",       "DELMASK",      SysBkColor);
             ImageDelete.Inactive = BMAP::LoadBitmap (hInstance, "DELIA",     "DELMASK",      SysBkColor);
             ImageInsert.Image    = BMAP::LoadBitmap (hInstance, "INSERT",    "INSERTMASK",   SysBkColor);
             ImageInsert.Inactive = BMAP::LoadBitmap (hInstance, "INSERTIA",  "INSERTMASK",   SysBkColor);
             ImageList.Image      = BMAP::LoadBitmap (hInstance, "ARRDOWND",  "ARRDOWNDMASK", SysBkColor);
             ImageList.Inactive   = BMAP::LoadBitmap (hInstance, "ARRDOWNDI","ARRDOWNDMASK", SysBkColor);
             hwndCombo1 = NULL;
             hwndCombo2 = NULL;
             Combo1 = NULL;
             Combo2 = NULL;
             ColorSet = FALSE;
             LineSet = FALSE;
             Toolbar2 = NULL;
             DockList = TRUE;
             ListDlg = NULL;
             hwndList = NULL;

             ActiveSpezDlg = this; 
             xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
             yfull = GetSystemMetrics (SM_CYFULLSCREEN);        


             StdSize = Size;
             dlgfont.FontHeight    = Size;
             dlgposfont.FontHeight = Size;
             ltgrayfont.FontHeight = Size;
             cpfont.FontHeight     = cpsize;


             if (xfull < 1000) 
             {
                 dlgfont.FontHeight = 90;
                 dlgposfont.FontHeight = 90;
                 ltgrayfont.FontHeight = 90;
                 checkfonth.FontHeight = 90;
                 cpfont.FontHeight = 150;
             }

             if (xfull < 800) 
             {

                 dlgfont.FontHeight = 70;
                 dlgposfont.FontHeight = 70;
                 ltgrayfont.FontHeight = 70;
                 checkfonth.FontHeight = 70;
                 cpfont.FontHeight = 120;
             }

             fHead.SetFieldanz ();
             fHead.SetFieldanz ();
             fPos.SetFieldanz ();
             fcForm.SetFieldanz ();

             fHead.GetCfield ("mdn_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fHead.GetCfield ("fil_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fHead.GetCfield ("mdn_choise")->SetBitmap 
			      (LoadBitmap (hInstance, "ARRDOWN"));
             fPos.GetCfield ("tier_kz_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fHead.GetCfield ("ident_extern_choise")->SetBitmap    //todo
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fHead.GetCfield ("lief_rech_nr_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));

             fHead.GetCfield ("lief_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fHead.GetCfield ("a_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));

             fHead.GetCfield ("ident_extern_choise")->SetTabstop (FALSE);  //todo
             fHead.GetCfield ("lief_rech_nr_choise")->SetTabstop (FALSE);  
             fPos.GetCfield ("tier_kz_choise")->SetTabstop (FALSE); 
             fHead.GetCfield ("lief_choise")->SetTabstop (FALSE); 
             fHead.GetCfield ("mdn_choise")->SetTabstop (FALSE); 
             fHead.GetCfield ("fil_choise")->SetTabstop (FALSE); 
             fHead.GetCfield ("a_choise")->SetTabstop (FALSE);  


             if (ComboStaat1 == NULL)
             {
                 anzComboStaat = work.GetStaatAnz ();
                 ComboStaat1 = new char *[anzComboStaat + 1];
                 NULLERROR (ComboStaat1);   
                 for (i = 0; i < anzComboStaat; i ++)
                 {
                     ComboStaat1[i] = new char [20];
                     NULLERROR (ComboStaat1[i]);
                 }
                 ComboStaat1[i] = NULL;
			 }
             if (ComboStaat2 == NULL)
             {
                 anzComboStaat = work.GetStaatAnz ();
                 ComboStaat2 = new char *[anzComboStaat + 1];
                 NULLERROR (ComboStaat2);   
                 for (i = 0; i < anzComboStaat; i ++)
                 {
                     ComboStaat2[i] = new char [20];
                     NULLERROR (ComboStaat2[i]);
                 }
                 ComboStaat2[i] = NULL;
			 }
             work.FillStaatCombo (ComboStaat1, 0, anzComboStaat);
             work.FillStaatCombo (ComboStaat2, 0, anzComboStaat);
             fPos.GetCfield ("geburt")->SetCombobox (ComboStaat1); 
             fPos.GetCfield ("mast")->SetCombobox (ComboStaat1); 
             fPos.GetCfield ("schlachtung")->SetCombobox (ComboStaat1); 
             fPos.GetCfield ("zerlegung")->SetCombobox (ComboStaat1); 




             for (i = 0; i < fHead.GetFieldanz (); i ++)
             {
//                       fHead.GetCfield ()[i]->SetFont (&dlgfont); 
//                       fHead.GetCfield ()[i]->SetPosFont (&dlgposfont); 
             }

             SetTmFont (&dlgfont);
             for (i = 0; Before[i] != NULL; i ++)
             {
                 Before[i]->SetBefore (&fcForm);
             }

             for (i = 0; After[i] != NULL; i ++)
             {
                 After[i]->SetAfter (&fcForm);
             }


             if (BorderType == HIGHCOLBORDER)
             {

                for (i = 0; itFont[i] != NULL; i ++)
                {
                         itFont[i]->SetFont (&fcForm);
                }

                fPos.GetCfield ("pos_frame")->SetBorder 
                 (new FILLEDBORDER (BLACKCOL, WHITECOL, HIGH, 0, LTGRAYCOL)); 
             }
             else if (BorderType == LOWCOLBORDER)
             {

                for (i = 0; itFont[i] != NULL; i ++)
                {
                         itFont[i]->SetFont (&fcForm);
                }
                fPos.GetCfield ("pos_frame")->SetBorder 
                 (new FILLEDBORDER (BLACKCOL, WHITECOL, LOW, 0, LTGRAYCOL)); 
             }
             else if (BorderType == HIGHBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, HIGH); 
             }
             else if (BorderType == LOWBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, LOW); 
             }
             else if (BorderType == RAISEDBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, RAISED); 
             }
             else if (BorderType == LINEBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, LINE); 
             }

             Enable (&fcForm, EnableHead, TRUE); 
             if (Mandant > 0) 
			 {
				 Enable (&fcForm, EnableHeadMdn, FALSE); 
				 sprintf(zerldatenf.mdn,"%ld",Mandant);
				 syskey = KEYCR;
				 ReadMdn();
			 }
             Enable (&fcForm, EnablePos,  FALSE);
             Enable (&fcForm, EnableDatum,  FALSE);
			 inpos = FALSE;

			 if (!RFE_Lohmann) sysdate (zerldatenf.dat);

             SetDialog (&fcForm0);
}


void SpezDlg::ZeigeListe(void)
{
		 if (work.PruefeZerm(zerldatenf.tier_kz) == TRUE)
		 {
              ShowList ();
			 ListDlg->InitList();
		     ListDlg->DestroyWindows ();
		 }
		 else
		 {
		 }

}


BOOL SpezDlg::OnKeyReturn (void)
{
        HWND hWnd;
        
        hWnd = GetFocus ();
//		ZeigeListe();
        return FALSE;
}


BOOL SpezDlg::OnKeyDown (void)
{
        return FALSE;
}


BOOL SpezDlg::OnKeyUp (void)
{
        return FALSE;
}


BOOL SpezDlg::OnKey2 ()
{
        return TRUE;
}

BOOL SpezDlg::OnKeyHome ()
{
        return TRUE;
}

BOOL SpezDlg::OnKeyEnd ()
{
        return TRUE;

}
BOOL SpezDlg::OnKey1 ()
{
        syskey = KEY1;
        Help ();
        return TRUE;
}

BOOL SpezDlg::OnKey3 ()
{
	char text [40];
	if (flg_kat_zwang)
	{
	if (strcmp (clipped(zerldatenf.tier_kz),"") == 0)
	{
			disp_mess("Bitte zuerst die Kategorie erfassen!",2);
            fPos.SetCurrentName ("tier_kz");
			return TRUE;
	}
	}

	sprintf(text,"Jetzt %d Etiketten drucken ?",atoi(zerldatenf.anz_gedruckt));
    if (abfragejn (hMainWindow, text, "J"))
    {
		WriteParamSchlacht(atoi(zerldatenf.anz_gedruckt));
		ComboGelesen = FALSE;
        syskey = KEY5;
		OnKey5();
	}

        return TRUE;
}

BOOL SpezDlg::OnKey4 ()
{
        syskey = KEY4;
        CallInfo ();
        return TRUE;
}
void SpezDlg::SaveRect (void)
/**
Aktuelle Windowgroesse sichern.
**/
{
	    char *etc;
		char rectname [512];
		RECT rect;
		FILE *fp;

		etc = getenv ("BWSETC");
		if (etc == NULL) return;

		sprintf (rectname, "%s\\%s.rct", etc,"21900");
		GetWindowRect (ActiveDlg->GethMainWindow (), &rect);

		fp = fopen (rectname, "w");
		fprintf (fp, "left    %d\n", rect.left);
		fprintf (fp, "top     %d\n", rect.top);
		fprintf (fp, "right   %d\n", rect.right - rect.left);
		fprintf (fp, "bottom  %d\n", rect.bottom - rect.top);
		fclose (fp);
}


BOOL SpezDlg::OnKey5 ()
{

	syskey = KEY5;
	    SaveRect();
        if (fHead.GetCfield ("ident_extern")->IsDisabled () == FALSE)
        {
		      work.CommitWork ();
//            work.RollbackWork ();
		      ExitProcess (0);
              return TRUE;
        }
        Enable (&fcForm, EnableHead, TRUE); 
        if (Mandant == 0) Enable (&fcForm, EnableHeadMdn, TRUE); 
        Enable (&fcForm, EnableHead2, TRUE);  
        Enable (&fcForm, EnablePos,  FALSE);
			inpos = FALSE;

		if (RFE_Lohmann) //LOH-3
		{
			Enable (&fcForm, EnableNachdruck, TRUE);
			Enable (&fcForm, EnableHead, FALSE); 
		}

        work.CommitWork ();
//        work.RollbackWork ();
        work.InitRec ();
        ToForm (dbfields);
        fPos.SetText ();
        fHead.SetCurrentName ("a_gt");
        ToolBar_SetState (ActiveDlg->GethwndTB (),   
			
			IDM_DELALL,   TBSTATE_INDETERMINATE);
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_LIST,     MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELETE,   MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELALL,   MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      VK_INSERT,    MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      VK_F3,       MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      VK_F6,       MF_GRAYED);
        if (ActiveSpezDlg->GetToolbar2 () != NULL)
        {
                 ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f6")->GetFeld ())->bmp = 
					 ImageDelete.Inactive;
            //     ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("del")->GetFeld ())->bmp = 
			//test		 ImageDelete.Inactive;
                 ((ColButton *)Toolbar2->GetCfield ("insert")->GetFeld ())->bmp = 
                     ImageInsert.Inactive;
                 if  (ActiveSpezDlg->GetToolbar2 ()->GetCfield ("Texte") != NULL)
                 {
                      ActiveSpezDlg->GetToolbar2 ()->GetCfield ("Texte")->Enable (FALSE);
                 }
                 ActiveSpezDlg->GetToolbar2 ()->display ();
        }
        return TRUE;
}

BOOL SpezDlg::OnKey12 ()
{
        syskey = KEY12;

        fWork->GetText ();
        FromForm (dbfields); 
        if (fHead.GetCfield ("ident_extern")->IsDisabled ())
        {
               Enable (&fcForm, EnableHead, TRUE); 
               if (Mandant == 0) Enable (&fcForm, EnableHeadMdn, TRUE); 
               Enable (&fcForm, EnableHead2, TRUE); 
               Enable (&fcForm, EnablePos,  FALSE);
				inpos = FALSE;

			   zerldaten.anz_gedruckt = 99;
			   strcpy(zerldatenf.anz_gedruckt,"99");
				zerldaten.a = zerldaten.a_gt;
/*100407*/				strcpy (zerldatenf.ident_intern,zerldaten.ident_extern);
/*100407*/				strcpy (zerldaten.ident_intern,zerldaten.ident_extern);

			   ActiveSpezDlg->Schreibe (FALSE);
			   ActiveSpezDlg->SchreibeLief ();

               work.CommitWork ();
//               work.InitRec ();
               ToForm (dbfields);

               fPos.SetText ();
               fHead.SetCurrentName ("lief");
               ToolBar_SetState (ActiveDlg->GethwndTB (),   IDM_DELALL, TBSTATE_INDETERMINATE);
               EnableMenuItem (ActiveDlg->GethMenu (),      IDM_LIST,     MF_GRAYED);
               EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELETE,   MF_GRAYED);
               EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELALL,   MF_GRAYED);
               EnableMenuItem (ActiveDlg->GethMenu (),      VK_INSERT,    MF_GRAYED);
               EnableMenuItem (ActiveDlg->GethMenu (),      VK_F3,       MF_GRAYED);
               EnableMenuItem (ActiveDlg->GethMenu (),      VK_F6,       MF_GRAYED);
               if (ActiveSpezDlg->GetToolbar2 () != NULL)
               {
             //    ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f6")->GetFeld ())->bmp = 
			//test		 ImageDelete.Inactive;
                 ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("del")->GetFeld ())->bmp = 
					 ImageDelete.Inactive;
                 ((ColButton *)Toolbar2->GetCfield ("insert")->GetFeld ())->bmp = 
                     ImageInsert.Inactive;
                 if  (ActiveSpezDlg->GetToolbar2 ()->GetCfield ("Texte") != NULL)
                 {
                      ActiveSpezDlg->GetToolbar2 ()->GetCfield ("Texte")->Enable (FALSE);
                 }
                 ActiveSpezDlg->GetToolbar2 ()->display ();
               }
        }
        return TRUE;
}


BOOL SpezDlg::OnKey10 (void)
{
      return FALSE;
}


BOOL SpezDlg::OnKeyDelete (void)
{
            if (abfragejn (hWnd, "Identnummer l�schen ?", "N") > 0)
            {
               work.BeginWork ();
			   work.DeleteAllPos();
               work.CommitWork ();
			   if (inpos == TRUE)
			   {
				OnKey5();
			   }
			}
		   
        return FALSE;
}

BOOL SpezDlg::OnKeyInsert ()
{
        return FALSE;
}


BOOL SpezDlg::OnKeyPrior ()
{
        return FALSE;
}

BOOL SpezDlg::OnKeyNext ()
{
        return FALSE;
}

BOOL SpezDlg::OnKey6 (short i)
{
	    if (i == 1)
		{
			if (ListDlg->SchreibeZerlege() == FALSE)
			{
				fPos.SetCurrentName ("tier_kz");
				return TRUE;
			}
		    return OnKey5();
//			return TRUE;
		}
		return OnKey6();
}

BOOL SpezDlg::OnKey6 ()
{
	if (flg_kat_zwang)
	{
    if (strcmp (clipped(zerldatenf.tier_kz),"") == 0)  
	{
		disp_mess("Bitte zuerst die Kategorie erfassen!",2);
            fPos.SetCurrentName ("tier_kz");
			return TRUE;
	}
	}
	    work.SetAnzahl(atoi(zerldatenf.anz_gedruckt));
	    ShowList();
        if (ListDlg == NULL)
        {
            return TRUE;
        }
        ListDlg->SetWork (&work);
// Button, wenn vorhanden Initialisieren
        if (work.GetLsts () != NULL)
        {
              work.GetLsts ()->choise [0] = NULL;
        }
// Liste ist schon vorhanden, Liste l�schen und dann Editiermodus mit ListEnter
//        ListDlg->DestroyWindows ();

// Liste ist schon vorhanden, Wechsel in Editiermodus 
        if (DockList)
        {
              ListDlg->WorkList ();
        }
        else
        {
              ListDlg->EnterList ();
        }

// Liste nicht vorhanden, Editiermodus mit EnterListe
//        ListDlg->EnterList ();

// Liste nach EnterList wieder anzeigen, wie in ShowList
//      if (DockList)
//        {
//              ListDlg->ShowList ();
//        }

			 ListDlg->InitList();
		     ListDlg->DestroyWindows ();
        fPos.SetCurrentName ("tier_kz");
        return TRUE;
} 

BOOL SpezDlg::OnKey8 ()
{
	syskey = KEY8;
    FromForm (dbheadfields);
    FromForm (dbfields);
	work.PruefePartie();
         fHead.SetText ();
         fPos.SetText ();
		fHead.SetCurrentName ("lief_rech_nr");
	return TRUE;
} 

BOOL SpezDlg::OnKey7 ()
{

	if (RFE_Lohmann)
	{
         fHead.SetText (); 
         fPos.SetText (); 
         fHead.GetText (); 
         fPos.GetText (); 
         FromForm (dbheadfields);
         FromForm (dbfields);
	}

	char cw [50];
	char tier_kz [20];
		if (NachdruckAktiv == TRUE)
		{
			strcpy (cwhere, "");
			if (strlen(clipped(zerldatenf.a_gt)) > 0)
			{
				sprintf (cw, "and zerldaten.a = %13.0lf ",ratod(zerldatenf.a_gt));
				strcat (cwhere,cw);
			}
			if (strlen(zerldatenf.dat) > 7)
			{
				sprintf (cw, "and zerldaten.dat = \"%s\" ",zerldatenf.dat);
				strcat (cwhere,cw);
			}
			if (strlen(clipped(zerldatenf.a_bez)) > 0)
			{
				sprintf (cw, "and a_bas.a_bz1 matches \"*%s*\" ",zerldatenf.a_bez);
				strcat (cwhere,cw);
			}
			if (strlen(clipped(zerldatenf.ident_extern)) > 0)
			{
				sprintf (cw, "and ident_extern matches \"%s*\" ",zerldatenf.ident_extern);
				strcat (cwhere,cw);
			}
			ShowIdent (cwhere,"");
			if (syskey != KEY5)
			{
				work.ReadA (ratod(zerldatenf.a_gt), zerldatenf.a_bez, tier_kz);
				work.ReadLief (zerldatenf.lief, zerldatenf.lief_krz);
                work.GetMdnName (zerldatenf.mdn_krz,eznum2,  atoi (zerldatenf.mdn));
				Enable (&fcForm, EnableNachdruck, FALSE);
				Enable (&fcForm, EnableHead, FALSE); 
				if (Mandant == 0) Enable (&fcForm, EnableHeadMdn, FALSE); 
				Enable (&fcForm, EnableHead2, FALSE); 
				Enable (&fcForm, EnablePos, TRUE); 
				inpos = TRUE;

				sprintf (zerldatenf.a_gt,"%.0lf",ratod(zerldatenf.a_gt));
				fPos.SetCurrentName ("tier_kz");
				AktiviereSchlacht(TRUE);
				AktiviereZerlege(TRUE);
                fHead.SetText ();
			}
			else
			{
				Enable (&fcForm, EnableNachdruck, FALSE);
				Enable (&fcForm, EnableHead, TRUE); 
				if (Mandant == 0) Enable (&fcForm, EnableHeadMdn, TRUE); 
				Enable (&fcForm, EnableHead2, TRUE); 
				Enable (&fcForm, EnablePos, FALSE); 
				inpos = FALSE;
				if (RFE_Lohmann) //LOH-3
				{
					Enable (&fcForm, EnableNachdruck, TRUE);
					Enable (&fcForm, EnableHead, FALSE); 
				}
	
				fHead.SetCurrentName ("mdn");
			}

			OnKey6();
			if (!RFE_Lohmann) NachdruckAktiv = FALSE; //LOH-3
		}
		else
		{
			Enable (&fcForm, EnablePos, FALSE);
			Enable (&fcForm, EnableHead, FALSE);
			if (Mandant == 0) Enable (&fcForm, EnableHeadMdn, FALSE);
			Enable (&fcForm, EnableHead2, FALSE);
			Enable (&fcForm, EnableNachdruck, TRUE);
			NachdruckAktiv = TRUE;
			inpos = TRUE;

	        fHead.SetCurrentName ("a_gt");
            fHead.SetFocus (); //LOH-3
		}
        return FALSE;
}

BOOL SpezDlg::ShowTierKz (void)
{
             ShowPtab ("tier_kz", "tier_kz", "tier_kz_krz");
             fPos.SetCurrentName ("tier_kz");

             return TRUE;
}

BOOL SpezDlg::ShowPtab (char *ptitem, char *item, char *item_bz)
{
             if (work.ShowPtabEx (hWnd, (char *) fPos.GetCfield (item)->GetFeld (),
				                        ptitem) == -1)
			 {
				 ActiveDlg->SetCurrentFocus ();
				 return TRUE;
			 }
			 fPos.GetCfield (item)->SetText ();
			 work.GetPtab (fPos.GetCfield (item_bz)->GetFeld (),
				                 ptitem,  
					   		     fPos.GetCfield (item)->GetFeld ());
			 fPos.GetCfield (item_bz)->SetText ();
			 return TRUE;
}

BOOL SpezDlg::ShowIdent (void)
{

             char lfd [21];
             SEARCHZERLDATEN SearchZerldaten;
 
             SearchZerldaten.SetParams (DLG::hInstance, ActiveDlg->GethWnd ());
             SearchZerldaten.Setawin (ActiveDlg->GethMainWindow ());
             SearchZerldaten.Search ();
             if (SearchZerldaten.GetKey (lfd) == TRUE)
             {
                 sprintf (zerldatenf.lfd, "%s", lfd); //todo Einstieg �ber lfd
//                 fHead.SetText ();
 			     fHead.GetCfield ("lfd")->SetText ();
                 fHead.SetCurrentName ("lfd");
				 AktiviereSchlacht(TRUE);
             }
			 else
			 {
				 AktiviereSchlacht(FALSE);
             }
             return TRUE;
}
BOOL SpezDlg::ShowIdent (char * inr)
{
             char lfd [21];
             SEARCHZERLDATEN SearchZerldaten;
 
             SearchZerldaten.SetParams (DLG::hInstance, ActiveDlg->GethWnd ());
             SearchZerldaten.Setawin (ActiveDlg->GethMainWindow ());
             SearchZerldaten.Search (inr);
             if (SearchZerldaten.GetKey (lfd) == TRUE)
             {
                 sprintf (zerldatenf.lfd, "%s", lfd);//todo Einstieg �ber lfd
//                 fHead.SetText ();
 			     fHead.GetCfield ("lfd")->SetText ();
                 fHead.SetCurrentName ("lfd");
             }
             return TRUE;
}
void SpezDlg::ZerldatenToZerldatenf (void)
{

	  sprintf (zerldatenf.lfd, "%d", zerldaten.lfd_i);
	  sprintf (zerldatenf.mdn, "%d", zerldaten.mdn);
	  sprintf (zerldatenf.fil, "%d", zerldaten.fil);
	  sprintf (zerldatenf.lief, "%s", zerldaten.lief);
	  sprintf (zerldatenf.lief_rech_nr, "%s", zerldaten.lief_rech_nr);
	  sprintf (zerldatenf.partie, "%d", zerldaten.partie);
	  sprintf (zerldatenf.anz_gedruckt, "%d", zerldaten.anz_gedruckt);
	  sprintf (zerldatenf.a_gt, "%13.0lf", zerldaten.a_gt);
	  sprintf (zerldatenf.gebland, "%d", zerldaten.gebland);
	  sprintf (zerldatenf.mastland, "%d", zerldaten.mastland);
	  sprintf (zerldatenf.schlaland, "%d", zerldaten.schlaland);
	  sprintf (zerldatenf.zerland, "%d", zerldaten.zerland);
	  sprintf (zerldatenf.esnum, "%s", zerldaten.esnum);
	  sprintf (zerldatenf.eznum1, "%s", zerldaten.eznum1);
	  sprintf (zerldatenf.eunum1, "%s", zerldaten.eunum1);
	  sprintf (zerldatenf.eunum2, "%s", zerldaten.eunum2);
//	  sprintf (zerldatenf.eznum2, "%s", zerldaten.eznum2);
//	  sprintf (zerldatenf.eznum3, "%s", zerldaten.eznum3);
	  sprintf (zerldatenf.tier_kz, "%s", zerldaten.kategorie);
	  sprintf (zerldatenf.ident_extern, "%s", zerldaten.ident_extern);
	  sprintf (zerldatenf.ident_intern, "%s", zerldaten.ident_intern);
	  dlong_to_asc (zerldaten.dat,zerldatenf.dat);
         ToForm (dbheadfields);
         ToForm (dbfields);
         work.GetPtab (zerldatenf.tier_kz_krz, "tier_kz", zerldatenf.tier_kz);
         fHead.SetText ();
         fPos.SetText ();
			fPos.SetCurrentName ("ident_extern");
//			Enable (&fcForm, EnableHead, FALSE); 
//			Enable (&fcForm, EnableHead2, FALSE); 
//			Enable (&fcForm, EnablePos, TRUE); 
			NachdruckAktiv == FALSE;
	  /*
      char mdn_krz [20];
      char fil_krz [20];
      char lief_krz [20];
	  char a_bez [25];
	  char mast [4];
	  char staat_mast [33];
	  char schlaland [20];
      char tier_kz_krz [33];

	  char geburt [4];
	  char staat_geburt [33];
	  char schlachtung [4];
	  char staat_schlachtung [33];
	  char zerlegung [4];
	  char staat_zerlegung [33];
	  */
}


BOOL SpezDlg::ShowIdent (char * where, char * inr)
{
             char lfd [21];
             SEARCHZERLDATEN SearchZerldaten;
 
             SearchZerldaten.SetParams (DLG::hInstance, ActiveDlg->GethWnd ());
             SearchZerldaten.Setawin (ActiveDlg->GethMainWindow ());
             SearchZerldaten.Search (where,inr, FontSizeIdentNr);
             if (SearchZerldaten.GetKey (lfd) == TRUE)
             {
                 sprintf (zerldatenf.lfd, "%s", lfd);//todo Einstieg �ber lfd
				 zerldaten.lfd = atoi(lfd);
				 zerldaten.lfd_i = atoi(lfd);

// 			     fHead.GetCfield ("ident_extern")->SetText ();
//                 fHead.SetCurrentName ("ident_extern");
				 ZerldatenClass.dbreadfirst_lfd();
				 ZerldatenToZerldatenf ();
             }
             return TRUE;
}
BOOL SpezDlg::ShowLief (void)
{
             char lief [11];
//             SEARCHLIEF SearchLief;
 
             SearchLief.SetParams (DLG::hInstance, ActiveDlg->GethWnd ());
             SearchLief.Setawin (ActiveDlg->GethMainWindow ());
             SearchLief.Search ();
             if (SearchLief.GetKey (lief) == TRUE)
             {
                 sprintf (zerldatenf.lief, "%s", lief);
//                 fHead.SetText ();
 			     fHead.GetCfield ("lief")->SetText ();
                 fHead.SetCurrentName ("lief");
             }
             return TRUE;
}
BOOL SpezDlg::ShowLief (char *stext)
{
             char lief [17];
//             SEARCHLIEF SearchLief;
 
             fHead.SetCurrentName ("lief");
             SearchLief.SetParams (DLG::hInstance, ActiveDlg->GethWnd ()); 
             SearchLief.Setawin (ActiveDlg->GethMainWindow ());
                                       
             clipped (stext);
             SearchLief.Search (stext);
             if (SearchLief.GetKey (lief) == TRUE)
             {
                 sprintf (zerldatenf.lief, "%s", lief);
//                 work.GetLiefName (zerldatenf.lief_krz,  atoi (zerldatenf.mdn), zerldatenf.lief);
                 fHead.SetText ();
                 PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
                 return TRUE;
             }
             return FALSE;
}
BOOL SpezDlg::ShowA (void)
{
             char artikel [14];
             SEARCHA SearchA;
 
             SearchA.SetParams (DLG::hInstance, ActiveDlg->GethWnd (),we_vorhanden);
             SearchA.Setawin (ActiveDlg->GethMainWindow ());
             SearchA.Search ();
             if (SearchA.GetKey (artikel) == TRUE)
             {
                 sprintf (zerldatenf.a_gt, "%13.0lf", ratod(artikel));
 			     fHead.GetCfield ("a_gt")->SetText ();
                 fHead.SetCurrentName ("a_gt");
             }
             return TRUE;
}
BOOL SpezDlg::ShowA (char *stext)
{
             char artikel [14];
             SEARCHA SearchA;
 
             fHead.SetCurrentName ("a_gt");
             SearchA.SetParams (DLG::hInstance, ActiveDlg->GethWnd (),we_vorhanden); 
             SearchA.Setawin (ActiveDlg->GethMainWindow ());
                                       
             clipped (stext);
             SearchA.Search (stext);
             if (SearchA.GetKey (artikel) == TRUE)
             {
                 sprintf (zerldatenf.a_gt, "%s", artikel);
//                 work.GetLiefName (zerldatenf.lief_krz,  atoi (zerldatenf.mdn), zerldatenf.lief);
                 fHead.SetText ();
                 PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
                 return TRUE;
             }
             return FALSE;
}

BOOL SpezDlg::ShowMdn (void)
{
             char mdn_nr [6];
             SEARCHMDN SearchMdn;
 
             SearchMdn.SetParams (DLG::hInstance, ActiveDlg->GethWnd ());
             SearchMdn.Setawin (ActiveDlg->GethMainWindow ());
             SearchMdn.Search ();
             if (SearchMdn.GetKey (mdn_nr) == TRUE)
             {
                 sprintf (zerldatenf.mdn, "%4d", atoi (mdn_nr));
                 work.GetMdnName (zerldatenf.mdn_krz,eznum2,   atoi (zerldatenf.mdn));
                 fHead.SetText ();
                 fHead.SetCurrentName ("mdn");
             }
             return TRUE;
}


BOOL SpezDlg::ShowFil (void)
{
             char fil_nr [6];
             SEARCHFIL SearchFil;
 
             if (atoi (zerldatenf.mdn) == 0) return TRUE;

             SearchFil.SetParams (DLG::hInstance, ActiveDlg->GethWnd (), 
                                                  atoi (zerldatenf.mdn));
             SearchFil.Setawin (ActiveDlg->GethMainWindow ());
             SearchFil.Search ();
             if (SearchFil.GetKey (fil_nr) == TRUE)
             {
                 sprintf (zerldatenf.fil, "%4d", atoi (fil_nr));
                 work.GetFilName (zerldatenf.fil_krz,   atoi (zerldatenf.mdn), atoi (zerldatenf.fil));
                 fHead.SetText ();
                 fHead.SetCurrentName ("fil");
             }
             return TRUE;
}


BOOL SpezDlg::OnKey9 ()
{
         CFIELD *Cfield;
		char tier_kz [20];
         HWND hWnd;

         syskey = KEY9;
         hWnd = GetFocus ();

         if (hWnd == fHead.GetCfield ("mdn_choise")->GethWnd ())
         {
             return ShowMdn ();
         }

         if (hWnd == fHead.GetCfield ("fil_choise")->GethWnd ())
         {
             return ShowFil ();
         }

         if (hWnd == fPos.GetCfield ("tier_kz_choise")->GethWnd ())
         {
             return ShowTierKz ();
         }

         if (hWnd == fHead.GetCfield ("ident_extern_choise")->GethWnd ())
         {
             char cw[50];
			 strcpy( cw, "" );

			 if ( numeric( zerldatenf.a_gt ) )  /* GK 25.06.2013 */
			 {
				 sprintf( cw, " and a_gt=%s", zerldatenf.a_gt );
			 }
			 BOOL ret = ShowIdent (cw,"");
			 if (syskey == KEY5) return ret;

			if (syskey != KEY5)
			{
				work.ReadA (ratod(zerldatenf.a_gt), zerldatenf.a_bez, tier_kz);
				work.ReadLief (zerldatenf.lief, zerldatenf.lief_krz);
                work.GetMdnName (zerldatenf.mdn_krz,eznum2,  atoi (zerldatenf.mdn));
				Enable (&fcForm, EnableNachdruck, FALSE);
				Enable (&fcForm, EnableHead, FALSE); 
				if (Mandant == 0) Enable (&fcForm, EnableHeadMdn, FALSE); 
				Enable (&fcForm, EnableHead2, FALSE); 
				Enable (&fcForm, EnablePos, TRUE); 
				inpos = TRUE;

				sprintf (zerldatenf.a_gt,"%.0lf",ratod(zerldatenf.a_gt));
				fPos.SetCurrentName ("tier_kz");
				AktiviereSchlacht(TRUE);
				AktiviereZerlege(TRUE);
                fHead.SetText ();
			}
			else
			{
				Enable (&fcForm, EnableNachdruck, FALSE);
				Enable (&fcForm, EnableHead, TRUE); 
				if (Mandant == 0) Enable (&fcForm, EnableHeadMdn, FALSE); 
				Enable (&fcForm, EnableHead2, TRUE); 
				Enable (&fcForm, EnablePos, FALSE); 
				inpos = FALSE;
	
				fHead.SetCurrentName ("mdn");
			}

			OnKey6();

			 return ret;
//             return ShowIdent (cwhere,"");
         }
         if (hWnd == fHead.GetCfield ("lief_choise")->GethWnd ())
         {
             return ShowLief (zerldatenf.lief);
         }
         if (hWnd == fHead.GetCfield ("a_choise")->GethWnd ())
         {
             return ShowA (zerldatenf.a_gt);
         }

         Cfield = ActiveDlg->GetCurrentCfield ();
         if (strcmp (Cfield->GetName (), "mdn") == 0)
         {
             return ShowMdn ();
         }

         if (strcmp (Cfield->GetName (), "fil") == 0)
         {
             return ShowFil ();
         }
         if (strcmp (Cfield->GetName (), "tier_kz") == 0)
         {
             return ShowTierKz ();
         }

         if (strcmp (Cfield->GetName (), "ident_extern") == 0)
         {
             if (RFE_Lohmann) OnKey7 (); //LOH-3
			 if ( numeric( zerldatenf.a_gt ) )
			 {
				 sprintf( cwhere + strlen(cwhere), " and a_gt=%s", zerldatenf.a_gt );
			 }
             return ShowIdent (cwhere,"");
         }
         if (strcmp (Cfield->GetName (), "lief") == 0)
         {
             return ShowLief (zerldatenf.lief);
         }
         if (strcmp (Cfield->GetName (), "a_gt") == 0)
         {
             return ShowA (zerldatenf.a_gt);
         }
         return FALSE;
}


BOOL SpezDlg::OnPaint (HWND hWnd,HDC hdc, UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (fWork && this->hWnd == hWnd)
        {
             PrintMess (MessText);
             fWork->display (hWnd, hdc);
             return TRUE;
        }
        return DLG::OnPaint (hWnd,hdc, msg, wParam,lParam);
}


BOOL SpezDlg::OnMove (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (hWnd != this->hMainWindow) return FALSE; 
        DLG::OnMove (hWnd,msg,wParam,lParam); 
        if (ListDlg != NULL)
        {
            ListDlg->MoveMamain1 ();
        }
        return FALSE;
}

BOOL SpezDlg::OnActivate (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (LOWORD (wParam) == WA_ACTIVE ||
                LOWORD (wParam) == WA_CLICKACTIVE)
        {
              if (WithTabStops)
              {
                  SetTabFocus (); 
              }
              else
              {
                  fWork->SetFocus ();
              }
              return TRUE;
        }
        return FALSE;
}


BOOL SpezDlg::OnCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (fWork)
        {

              if (LOWORD (wParam) == CANCEL_CTL)
              {
                  return OnKey5 ();
              }
              if (LOWORD (wParam) == OK_CTL)
              {
                  return OnKey12 ();
              }
              if (LOWORD (wParam) == IDM_CHOISE)
              {
                  return OnKey9 ();
              }

              if (HIWORD (wParam) == WM_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              if (HIWORD (wParam) == EN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == CBN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == WM_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == LBN_SELCHANGE)
              {
//                  return ListChanged (wParam, lParam);
              }
              else if (HIWORD (wParam) == EN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == CBN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == BN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == BN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
        }
        return DLG::OnCommand (hWnd, msg, wParam, lParam);
}


BOOL SpezDlg::OnSysCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        
        if (wParam == SC_CLOSE)
        {
            if (abfragejn (hWnd, "Verarbeitung abbrechen ?", "N") == 0)
            {
                ActiveDlg->SetCurrentFocus ();
                return TRUE;
            }
            syskey = KEY5;
        }
        return DLG::OnSysCommand (hWnd, msg, wParam, lParam); 
}

BOOL SpezDlg::OnDestroy (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{

        if (hWnd == this->hWnd)
        {
             PostQuitMessage (0);
             fcForm.destroy ();
        }
        return TRUE;
}

void SpezDlg::SetWinBackground (COLORREF Col)
{
          dlgfont.FontBkColor = Col;
          cpfont.FontBkColor = Col;
          DLG::SetWinBackground (Col);
}


void SpezDlg::ToClipboard (void)
{
          CFIELD *Cfield;
          HGLOBAL hb;
          LPVOID p;
          char *text;

          Cfield = GetCurrentCfield ();
          Cfield->GetText ();
          text = (char *) Cfield->GetFeld ();
          if (text == NULL) return;
          hb = GlobalAlloc (GHND, strlen (text) + 1);
          if (hb == NULL) return;
          p = GlobalLock (hb);
          strcpy ((char *) p, text);
          GlobalUnlock (hb);
          OpenClipboard (hWnd);
          EmptyClipboard ();
          SetClipboardData (CF_TEXT, hb);
          CloseClipboard ();
}

void SpezDlg::FromClipboard (void)
{
          CFIELD *Cfield;
          HGLOBAL hb;
          LPVOID p;
          char *text;

          Cfield = GetCurrentCfield ();
          text = (char *) Cfield->GetFeld ();
          if (text == NULL) return;
          if (IsClipboardFormatAvailable (CF_TEXT) == FALSE) return;
          OpenClipboard (hWnd);
          hb = GetClipboardData (CF_TEXT);
          if (hb == NULL)
          {
                 CloseClipboard ();
                 return;
          }
          p = GlobalLock (hb);
          if (strlen ((char *) p) > (unsigned int ) Cfield->GetLength ())
          {
                 GlobalUnlock (hb);
                 CloseClipboard ();
                 return;
          }
          strcpy (text, (char *) p);
          GlobalUnlock (hb);
          CloseClipboard ();
          Cfield->SetText ();
}

void SpezDlg::Help (void)
{
          CFIELD *Cfield;
          HELP *Help; 
          char *Item;

          Cfield = GetCurrentCfield ();
          if (Cfield == NULL) return;

          Item = Cfield->GetName ();

          Help = new HELP (HelpName, Item, hInstance,hMainWindow);
          delete Help;
          Cfield->SetFocus ();
}

HWND SpezDlg::OpenWindow (HINSTANCE hInstance, HWND hMainWindow)
{
          HWND hWnd;

          Settchar ('|');
          hWnd = DLG::OpenWindow (hInstance, hMainWindow);
//          FillListCaption ();
          EnterPos ();
          return hWnd;
}

void SpezDlg::DestroyPos (void)
{
    fPos.destroy ();
    fcForm.SetFieldanz (fcForm.GetFieldanz () - 1);
}

void SpezDlg::SetPos (void)
{
    fcForm.SetFieldanz (2);
    fcForm0.Invalidate (TRUE);
    fcForm0.MoveWindow ();
    UpdateWindow (hWnd);
}

HWND SpezDlg::OpenScrollWindow (HINSTANCE hInstance, HWND hMainWindow)
{
          HWND hWnd;

          Settchar ('|');
          hWnd = DLG::OpenScrollWindow (hInstance, hMainWindow);
          EnterPos ();
          ListDlg = new LISTDLG ();
          return hWnd;
}

void SpezDlg::ShowList (void)
{
          int DockY;
          RECT rect;

		  ListDlg->SetDlg (this);
          ListDlg->SetWork (&work);
          if (DockList == FALSE)
          {
                 ListDlg->SetWinStyle (ListDlg->GetWinStyle () | WS_CAPTION | 
                                                                 WS_SYSMENU |
                                                                 WS_MINIMIZEBOX |
                                                                 WS_MAXIMIZEBOX);
                ListDlg->SetDockWin (FALSE);
                ListDlg->SetDimension (750, 400);
                ListDlg->SetModal (TRUE);
          }
          else
          {
                if (DockMode)
                {
                   ListDlg->SetWinStyle (WS_THICKFRAME | WS_CHILD | WS_CLIPSIBLINGS);
                   Enable (&fcForm, EnablePos, TRUE); 
//                DestroyPos ();
                }
                GetWindowRect (hWnd, &rect);
                DockY = fPos.GetCfield ("pos_frame")->GetYP () + rect.top;
//                DockY = 309;
                ListDlg->SetDockY (DockY);
          }

          ListDlg->SethMenu (hMenu);
          ListDlg->SethwndTB (hwndTB);
          ListDlg->SethMainWindow (hWnd);
          ListDlg->SetTextMetric (&DlgTm);
          ListDlg->SetLineRow (100);
// Liste gleich anzeigen
          if (DockList)
          {
               ListDlg->ShowList ();
          }
}

void SpezDlg::PrintComment (char *Name)
{
          HELP *Help; 
          char Comment [256];

          Help = new HELP (HelpName, Name, hInstance,hMainWindow, INITCOMM);
          Help->GetItem (Name, Comment);
          PrintMess (Comment);
          delete Help;
}

void SpezDlg::CallInfo (void)
{
          DLG::CallInfo ();
}



void SpezDlg::SetListLines (int ListLines)
{
          this->ListLines = ListLines;
          if (ListClass::ActiveList != NULL)
          {
		      ListClass::ActiveList->SetListLines (ListLines);
          }
          LineSet         = TRUE;
}

void SpezDlg::SetListColors (COLORREF ListColor, COLORREF ListBkColor)
{
          this->ListColor   = ListColor;
          this->ListBkColor = ListBkColor;
          if (ListClass::ActiveList != NULL)
          {
		      ListClass::ActiveList->SetColors (ListColor,ListBkColor);
          }
          ColorSet          = TRUE;

}


HWND SpezDlg::SetToolCombo (int Id, int tbn1, int tbn2, int ComboPtr)
{
         HWND hwndCombo; 

		 hwndCombo = DLG::SetToolCombo (Id, tbn1, tbn2);

		 switch (ComboPtr)
		 {
		     case 0:
                   hwndCombo1 = hwndCombo;
				   break;
			 case 1:
                   hwndCombo2 = hwndCombo;
				   break;
		 }
         return hwndCombo;
}

void SpezDlg::SetComboTxt (HWND hwndCombo, char **Combo, int Select, int ComboPtr)
{

	  DLG::SetComboTxt (hwndCombo, Combo, Select);

 	  switch (ComboPtr)
	  {
		     case 0:
                   Combo1 = Combo;
				   break;
			 case 1:
                   Combo2 = Combo;
				   break;
	  }
}


BOOL SpezDlg::OnCloseUp (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
	    if ((HWND) lParam == hwndCombo1)
		{
			ChoiseCombo1 ();
			return TRUE;
		}
		else if ((HWND) lParam == hwndCombo2)
		{
			ChoiseCombo2 ();
			return TRUE;
		}
		return FALSE;
}


void SpezDlg::ChoiseCombo1 (void)
/**
Aktuell gewaehlten eintrag in Combobox 1 ermitteln.
**/
{
          int i;
          char text [80];

          GetWindowText (hwndCombo1,
                         text,
                         79);
          clipped (text);
          for (i = 0; Combo1[i]; i ++)
          {
              if (strcmp (Combo1[i],text) == 0)
              {
                   SetListLines (i);
                   break;
              }
          }
          if (ActiveDlg != NULL && 
              ActiveDlg->GetCurrentCfield ()->IsDisabled == FALSE)
          {
              ActiveDlg->SetCurrentFocus ();
          }
}

void SpezDlg::ChoiseCombo2 (void)
/**
Aktuell gewaehlten eintrag in Combobox 1 ermitteln.
**/
{
          int i;
          char text [80];

          GetWindowText (hwndCombo2,
                         text,
                         79);
          clipped (text);
          for (i = 0; Combo2[i]; i ++)
          {
              if (strcmp (Combo2[i],text) == 0)
              {
                   SetListColors (Colors[i], BkColors[i]);
                   break;
              }
          }
          if (ActiveDlg != NULL && 
              ActiveDlg->GetCurrentCfield ()->IsDisabled == FALSE)
          {
              ActiveDlg->SetCurrentFocus ();
          }
}


void SpezDlg::ProcessMessages (void)
{
//test          ShowList ();
          DLG::ProcessMessages ();
}


void SpezDlg::AktiviereZerlege (bool akt)
{

	if (ActiveSpezDlg->GetToolbar2 () != NULL)
         {

                 if  (ActiveSpezDlg->GetToolbar2 ()->GetCfield ("absch1") != NULL)
                 {
					if (akt == TRUE)
					{
						if (work.PruefeZerm(zerldatenf.tier_kz) == TRUE)
						{
		                      ActiveSpezDlg->GetToolbar2 ()->GetCfield ("absch1")->Enable (akt);
						}
						else
						{
		                      ActiveSpezDlg->GetToolbar2 ()->GetCfield ("absch1")->Enable (TRUE);
						}
					}
					else
					{
		                      ActiveSpezDlg->GetToolbar2 ()->GetCfield ("absch1")->Enable (TRUE);
					}
                 }

                 ActiveSpezDlg->GetToolbar2 ()->display ();
         }
}
void SpezDlg::AktiviereSchlacht (bool akt)
{
         if (ActiveSpezDlg->GetToolbar2 () != NULL)
         {
                 if  (ActiveSpezDlg->GetToolbar2 ()->GetCfield ("absch") != NULL)
                 {
					 if (atoi(zerldatenf.anz_gedruckt) > 0)
					 {
	                      ActiveSpezDlg->GetToolbar2 ()->GetCfield ("absch")->Enable (akt);
					 }
					 else
					 {
	                      ActiveSpezDlg->GetToolbar2 ()->GetCfield ("absch")->Enable (FALSE);
					 }
                 }

                 ActiveSpezDlg->GetToolbar2 ()->display ();
         }
}


static char EtiPath [512];

void SpezDlg::WriteParamSchlacht (int etianz)
/**
Parameter-Datei fuer Schlachtetikettschreiben.
**/
{
	    char *tmp;
		char *etc;
	    char dname [512];
	    char dmess [512];
		FILE *pa;

		tmp = getenv ("TMPPATH");
		if (tmp == NULL) return;
		etc = getenv ("BWSETC");
		if (etc == NULL) return;


	zerldaten.a = zerldaten.a_gt;
	if (etianz > 0)
	{
/*100407*/		strcpy (zerldatenf.ident_intern,zerldatenf.ident_extern);
/*100407*/		strcpy (zerldaten.ident_intern,zerldaten.ident_extern);
	}
	ActiveSpezDlg->Schreibe (FALSE);
	ActiveSpezDlg->SchreibeLief ();

	if (etianz == 0) return ;

	if (work.GetComboGelesen() == FALSE)
	{
		 sprintf (zerldatenf.geburt,"%d", fPos.GetCfield ("geburt")->GetComboPos ());
         // GK 30.06.2013 work.GetPtab (zerldatenf.staat_geburt, "staat", zerldatenf.geburt);
         work.GetPtab (zerldatenf.staat_geburt, zerldatenf.staatk_geburt, "staat", zerldatenf.geburt);

		 sprintf (zerldatenf.mast,"%d", fPos.GetCfield ("mast")->GetComboPos ());
         // GK 30.06.2013 work.GetPtab (zerldatenf.staat_mast, "staat", zerldatenf.mast);
         work.GetPtab (zerldatenf.staat_mast, zerldatenf.staatk_mast, "staat", zerldatenf.mast);

		 sprintf (zerldatenf.schlachtung,"%d", fPos.GetCfield ("schlachtung")->GetComboPos ());
         // GK 30.06.2013 work.GetPtab (zerldatenf.staat_schlachtung, "staat", zerldatenf.schlachtung);
         work.GetPtab (zerldatenf.staat_schlachtung, zerldatenf.staatk_schlachtung, "staat", zerldatenf.schlachtung);

		 sprintf (zerldatenf.zerlegung,"%d", fPos.GetCfield ("zerlegung")->GetComboPos ());
         // GK 30.06.2013 work.GetPtab (zerldatenf.staat_zerlegung, "staat", zerldatenf.zerlegung);
         work.GetPtab (zerldatenf.staat_zerlegung, zerldatenf.staatk_zerlegung, "staat", zerldatenf.zerlegung);

		 work.SetComboGelesen(TRUE);
	}

	//LOH-34
    if (strlen (clipped(zerldatenf.tier_kz_krz)) == 0)  
	{
		work.GetPtab (zerldatenf.tier_kz_krz, "tier_kz", zerldatenf.tier_kz); //LOH-34
	}
    if (strlen (clipped(zerldatenf.tier_kz_krz)) == 0)  
	{
			disp_mess("Es kann nicht gedruckt werden, da keine Kategorie(Bez) vorhanden ist",2);
			return;
	}


		sprintf (dname, "%s\\parafile", tmp);
		pa = fopen (dname, "w");
		if (pa == NULL) return;
		fprintf (pa, "$$KATEGORIE;%s\n",    zerldatenf.tier_kz_krz);
		fprintf (pa, "$$INTERN_ID;%s\n",    zerldatenf.ident_intern);
		fprintf (pa, "$$EXTERN_ID;%s\n",    zerldatenf.ident_extern);
		fprintf (pa, "$$GLAND;%s\n",    zerldatenf.staat_geburt);
//		fprintf (pa, "$$MLAND;%.2lf\n",    zerldatenf.staat_mast);
		fprintf (pa, "$$MLAND;%s\n",    zerldatenf.staat_mast);
		fprintf (pa, "$$SLAND;%s\n",    zerldatenf.staat_schlachtung);
		fprintf (pa, "$$ZLAND;%s\n",    zerldatenf.staat_zerlegung);

		/* GK 30.06.2013 */
		fprintf (pa, "$$GLANK;%s\n",    zerldatenf.staatk_geburt);
		fprintf (pa, "$$MLANK;%s\n",    zerldatenf.staatk_mast);
		fprintf (pa, "$$SLANK;%s\n",    zerldatenf.staatk_schlachtung);
		fprintf (pa, "$$ZLANK;%s\n",    zerldatenf.staatk_zerlegung);

		fprintf (pa, "$$ESNUM;%s\n",    zerldatenf.esnum);
		fprintf (pa, "$$EZNUM1;%s\n",    zerldatenf.eznum1);
		fprintf (pa, "$$EUNUM1;%s\n",    zerldatenf.eunum1);
		fprintf (pa, "$$EUNUM2;%s\n",    zerldatenf.eunum2);
		fprintf (pa, "$$EZNUM2;%s\n",    eznum2);
		fprintf (pa, "$$EZNUM3;%s\n",    zerldatenf.eznum3);
		fprintf (pa, "$$ARTNUM;%.0lf\n",    zerldaten.a);
		fprintf (pa, "$$ABEZ1;%s\n",    zerldatenf.a_bez);  
		if (strcmp(clipped(zerldatenf.tier_kz),"KA"))
		{
			fprintf (pa, "$$KALBTEXT;%s\n",    "");
		}
		else
		{
			fprintf (pa, "$$KALBTEXT;%s\n",    "Schlachtalter bis 8 Monate");
		}

		fclose (pa);

        sprintf (EtiPath, "%s\\%s", etc, eti_schlacht);

		int ret = 0;
		if (eti_testen == 1) strcpy (cbem,"Drucke   :");
		if (eti_testen == 2) strcpy (cbem,"nur Test :");
		if (eti_testen > 0)
		{
			sprintf (dmess, "%s   Anzahl: %s\n\nDrucker: %s   Datei: %s\n"
							"Artikel %.0lf %s",
							 cbem,zerldatenf.anz_gedruckt,eti_schl_drk, EtiPath, zerldaten.a,zerldatenf.a_bez);
            disp_mess (dmess, 0);
		}
		if (eti_testen < 2)
		{
	        ret = PutEti (eti_schl_drk, EtiPath, dname, etianz, eti_schl_typ) ;
	        if (ret == -1)
		    {
	            disp_mess ("Etikett kann nicht gedruckt werden", 2);
				sprintf (dmess, "Drucker: %s   Datei: %s   Anzahl: %s Artikel %.0lf", eti_schl_drk, EtiPath, zerldatenf.anz_gedruckt,zerldaten.a);
				disp_mess (dmess, 2);
			}
		}

		ActiveSpezDlg->BucheBsd ("ZE",atoi(zerldatenf.anz_gedruckt));
}

void SpezDlg::WriteZerlegedaten (LSTS lsts)
/**
Parameter-Datei fuer Zerlegeetikettschreiben.
**/
{
		/* GK 30.06.2013 */
	if (work.GetComboGelesen() == FALSE)
	{
		 sprintf (zerldatenf.geburt,"%d", fPos.GetCfield ("geburt")->GetComboPos ());
         // GK 30.06.2013 work.GetPtab (zerldatenf.staat_geburt, "staat", zerldatenf.geburt);
         work.GetPtab (zerldatenf.staat_geburt, zerldatenf.staatk_geburt, "staat", zerldatenf.geburt);

		 sprintf (zerldatenf.mast,"%d", fPos.GetCfield ("mast")->GetComboPos ());
         // GK 30.06.2013 work.GetPtab (zerldatenf.staat_mast, "staat", zerldatenf.mast);
         work.GetPtab (zerldatenf.staat_mast, zerldatenf.staatk_mast, "staat", zerldatenf.mast);

		 sprintf (zerldatenf.schlachtung,"%d", fPos.GetCfield ("schlachtung")->GetComboPos ());
         // GK 30.06.2013 work.GetPtab (zerldatenf.staat_schlachtung, "staat", zerldatenf.schlachtung);
         work.GetPtab (zerldatenf.staat_schlachtung, zerldatenf.staatk_schlachtung, "staat", zerldatenf.schlachtung);

		 sprintf (zerldatenf.zerlegung,"%d", fPos.GetCfield ("zerlegung")->GetComboPos ());
         // GK 30.06.2013 work.GetPtab (zerldatenf.staat_zerlegung, "staat", zerldatenf.zerlegung);
         work.GetPtab (zerldatenf.staat_zerlegung, zerldatenf.staatk_zerlegung, "staat", zerldatenf.zerlegung);

		 work.SetComboGelesen(TRUE);
	}
	zerldaten.anz_gedruckt = atoi(lsts.anzahl);
	zerldaten.a = ratod(lsts.mat);
	ActiveSpezDlg->Schreibe (TRUE);
	ActiveSpezDlg->BucheBsd ("ZA",zerldaten.anz_gedruckt);
}
void SpezDlg::WriteParamZerleg (LSTS lsts, bool jetzt )
/**
Parameter-Datei fuer Zerlegeetikettschreiben.
**/
{
   
	    char *tmp;
		char *etc;
	    char dname [512];
	    char dmess [512];
		FILE *pa;

		tmp = getenv ("TMPPATH");
		if (tmp == NULL) return;
		etc = getenv ("BWSETC");
		if (etc == NULL) return;



	if (work.GetComboGelesen() == FALSE)
	{
		 sprintf (zerldatenf.geburt,"%d", fPos.GetCfield ("geburt")->GetComboPos ());
         // GK 30.06.2013 work.GetPtab (zerldatenf.staat_geburt, "staat", zerldatenf.geburt);
         work.GetPtab (zerldatenf.staat_geburt, zerldatenf.staatk_geburt, "staat", zerldatenf.geburt);

		 sprintf (zerldatenf.mast,"%d", fPos.GetCfield ("mast")->GetComboPos ());
         // GK 30.06.2013 work.GetPtab (zerldatenf.staat_mast, "staat", zerldatenf.mast);
         work.GetPtab (zerldatenf.staat_mast, zerldatenf.staatk_mast, "staat", zerldatenf.mast);

		 sprintf (zerldatenf.schlachtung,"%d", fPos.GetCfield ("schlachtung")->GetComboPos ());
         // GK 30.06.2013 work.GetPtab (zerldatenf.staat_schlachtung, "staat", zerldatenf.schlachtung);
         work.GetPtab (zerldatenf.staat_schlachtung, zerldatenf.staatk_schlachtung, "staat", zerldatenf.schlachtung);

		 sprintf (zerldatenf.zerlegung,"%d", fPos.GetCfield ("zerlegung")->GetComboPos ());
         // GK 30.06.2013 work.GetPtab (zerldatenf.staat_zerlegung, "staat", zerldatenf.zerlegung);
         work.GetPtab (zerldatenf.staat_zerlegung, zerldatenf.staatk_zerlegung, "staat", zerldatenf.zerlegung);

		 work.SetComboGelesen(TRUE);
	}

		sprintf (dname, "%s\\parafile", tmp);
		pa = fopen (dname, "w");
		if (pa == NULL) return;
		fprintf (pa, "$$KATEGORIE;%s\n",    zerldatenf.tier_kz_krz);
		fprintf (pa, "$$INTERN_ID;%s\n",    zerldatenf.ident_intern);
		fprintf (pa, "$$EXTERN_ID;%s\n",    zerldatenf.ident_extern);
		fprintf (pa, "$$GLAND;%s\n",    zerldatenf.staat_geburt);
		fprintf (pa, "$$MLAND;%s\n",    zerldatenf.staat_mast);
		fprintf (pa, "$$SLAND;%s\n",    zerldatenf.staat_schlachtung);
		fprintf (pa, "$$ZLAND;%s\n",    zerldatenf.staat_zerlegung);

		/* GK 30.06.2013 */
		fprintf (pa, "$$GLANK;%s\n",    zerldatenf.staatk_geburt);
		fprintf (pa, "$$MLANK;%s\n",    zerldatenf.staatk_mast);
		fprintf (pa, "$$SLANK;%s\n",    zerldatenf.staatk_schlachtung);
		fprintf (pa, "$$ZLANK;%s\n",    zerldatenf.staatk_zerlegung);

		fprintf (pa, "$$ESNUM;%s\n",    zerldatenf.esnum);
		fprintf (pa, "$$EZNUM1;%s\n",    zerldatenf.eznum1);
		fprintf (pa, "$$EZNUM2;%s\n",    eznum2);
		fprintf (pa, "$$EUNUM1;%s\n",    zerldatenf.eunum1);
		fprintf (pa, "$$EUNUM2;%s\n",    zerldatenf.eunum2);
		fprintf (pa, "$$EZNUM3;%s\n",    zerldatenf.eznum3);
		fprintf (pa, "$$ARTNUM;%s\n",    lsts.mat);
		fprintf (pa, "$$ABEZ1;%s\n",    lsts.a_bez);
		if (strcmp(clipped(zerldatenf.tier_kz),"KA"))
		{
			fprintf (pa, "$$KALBTEXT;%s\n",    "");
		}
		else
		{
			fprintf (pa, "$$KALBTEXT;%s\n",    "Schlachtalter bis 8 Monate");
		}

		fclose (pa);

        sprintf (EtiPath, "%s\\%s", etc, eti_schlacht);


		int ret = 0;
		if (eti_testen == 1) strcpy (cbem,"Drucke   :");
		if (eti_testen == 2) strcpy (cbem,"nur Test :");
		if (eti_testen > 0 && jetzt == TRUE && atoi(lsts.anzahl) > 0)
		{
			sprintf (dmess, "%s   Anzahl: %s\n\nDrucker: %s   Datei: %s\n"
							"Artikel %s %s",
							 cbem,lsts.anzahl,eti_schl_drk, EtiPath, lsts.mat,lsts.a_bez);
            disp_mess (dmess, 0);
		}
		if (eti_testen < 2 && jetzt == TRUE && atoi(lsts.anzahl) > 0)
		{
	        ret = PutEti (eti_schl_drk, EtiPath, dname, atoi(lsts.anzahl), eti_schl_typ) ;
	        if (ret == -1)
		    {
	            disp_mess ("Etikett kann nicht gedruckt werden", 2);
				sprintf (dmess, "Drucker: %s   Datei: %s   Anzahl: %s Artikel %.0lf", eti_schl_drk, EtiPath, lsts.anzahl,lsts.mat);
				disp_mess (dmess, 2);
			}
		}
/***
		if (ret == 0)
		{
			zerldaten.anz_gedruckt = atoi(lsts.anzahl);
			zerldaten.a = ratod(lsts.mat);
			ActiveSpezDlg->Schreibe (TRUE);
			ActiveSpezDlg->BucheBsd ("ZA",zerldaten.anz_gedruckt);
		}
********/
}
void SpezDlg::Schreibe (bool flgZerleg)
{
         fHead.SetText ();
         fPos.GetText ();
               work.BeginWork ();
			    work.PruefeZerldaten ();
				if (flgZerleg == TRUE)
				{
					zerldaten.zerlegt = 1;
					if (strlen(eznum2) > 1)
					{
			   			sprintf (zerldaten.eznum2, "%s", eznum2);
					}
					else
					{
			   			sprintf (zerldaten.eznum2, "%s", "sn014");
					}
				}
				else
				{
			   	    sprintf (zerldaten.eznum2, "%s", "");
					zerldaten.zerlegt = 0;

				}

				ZerldatenClass.dbupdate ();
				

               work.CommitWork ();
}

void SpezDlg::SchreibeLief (void)
{
	strcpy(lief.esnum, zerldatenf.esnum);
	strcpy(lief.eznum, zerldatenf.eznum1);
	lief.geburt = fPos.GetCfield ("geburt")->GetComboPos ();
	lief.mast = fPos.GetCfield ("mast")->GetComboPos ();
	lief.schlachtung = fPos.GetCfield ("schlachtung")->GetComboPos ();
	lief.zerlegung = fPos.GetCfield ("zerlegung")->GetComboPos ();
	work.SchreibeLief (); 
}


void SpezDlg::BucheBsd (char *blg_typ, int anzahl)
/**
Bestandsbuchung vorbereiten.
**/
{
   BSD_BUCH_CLASS BsdBuch;

    zerldaten.dat = dasc_to_long (zerldatenf.dat);
	bsd_buch.nr  = zerldaten.partie;
	strcpy (bsd_buch.blg_typ, blg_typ);   
    bsd_buch.mdn = zerldaten.mdn;
	bsd_buch.fil = zerldaten.fil;
	bsd_buch.kun_fil = 0;
	bsd_buch.a   = zerldaten.a;
	bsd_buch.dat = zerldaten.dat;
	bsd_buch.lieferdat = zerldaten.dat;
//	strcpy (bsd_buch.zeit, prstatchg.zeit);
//	strcpy (bsd_buch.pers, prstatchg.ptbenutz);

	bsd_buch.qua_status = 0;
	bsd_buch.me = (double) anzahl;
	bsd_buch.bsd_ek_vk = 0;
    strcpy (bsd_buch.ident_nr, zerldaten.ident_intern);
    strcpy (bsd_buch.chargennr, zerldaten.ident_intern);
    strcpy (bsd_buch.herk_nachw, zerldaten.ident_extern);
    sprintf (bsd_buch.lief, "%s", zerldaten.lief);
    bsd_buch.auf = 0l;
    bsd_buch.verf_dat = 0; 
    strcpy  (bsd_buch.verfall, "");
    bsd_buch.delstatus = 0;
    strcpy  (bsd_buch.err_txt, "");
	if (bsd_buch.me != 0.0)
	{
		beginwork();
		BsdBuch.dbinsert ();
		commitwork();
	}
}

