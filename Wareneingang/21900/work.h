#ifndef _WEWORK_DEF
#define _WEWORK_DEF
#include "ptab.h"
#include "searchptab.h"
#include "mdn.h"
#include "zerldaten.h"
#include "zerm.h"
#include "dlg.h"
#include "listenter.h"
#include "dbclass.h"

extern BOOL RFE_Lohmann; // 24.06.2013 GK

struct LIEF
{
	char lief [16];
	char esnum [11];
	char eznum [11];
	short geburt;
	short mast;
	short schlachtung;
	short zerlegung;
};
extern struct LIEF lief;

struct LSTS
{
    char schnitt[80];
    char mdn[80];
    char mat[80];
    char aus_mat[80];
    char a_bez[80];
    char anzahl[80];
//    ListButton mebuch;
    char me_buch[10];
    char aktiv[80];
    char lfd[80];
    ListButton *choise[1];
};
struct ZERLDATENF
{
	  char lfd [20];
      char mdn [7];
      char mdn_krz [20];
      char fil [7];
      char fil_krz [20];
      char lief [18];
      char lief_krz [20];
	  char lief_rech_nr [20];
	  char partie [20];
	  char anz_gedruckt [10];
	  char a_gt [20];
	  char a_bez [27];       /* wegen Ueberlauf 25 --> 27 GK 27.06.2013 */
	  char gebland [20];
	  char mastland [20];
	  char mast [4];
	  char staat_mast [33];
	  char staatk_mast [33];
	  char schlaland [20];
	  char zerland [20];
	  char esnum [11];
	  char eznum1 [11];
	  char eznum2 [11];
	  char eznum3 [11];
	  char eunum1 [13];
	  char eunum2 [13];
	
      char tier_kz [7];
      char tier_kz_krz [33];
      char ident_extern [22];
      char ident_intern [22];
      char dat [12];

	  char geburt [4];
	  char staat_geburt [33];
	  char staatk_geburt [33];
	  char schlachtung [4];
	  char staat_schlachtung [33];
	  char staatk_schlachtung [33];
	  char zerlegung [4];
	  char staat_zerlegung [33];
	  char staatk_zerlegung [33];
	  double such_a;
};
extern struct ZERLDATENF zerldatenf, zerldatenf_null;
           

class Work
{
    private :
        PTAB_CLASS Ptab;
        ZERLDATEN_CLASS Zerldaten;
        MDN_CLASS Mdn;

        struct LSTS *Lsts;
        DB_CLASS DbClass;

        static long LongNull;
        DLG *Dlg;
        LISTENTER *ListDlg;
        int LstCursor;
		bool ComboGelesen;
		int AnzahlDrk;

    public :

        void SetDlg (DLG *Dlg)
        {
            this->Dlg = Dlg;
        }

        DLG *GetDlg (void)
        {
            return Dlg;
        }

        void SetListDlg (LISTENTER *ListDlg)
        {
            this->ListDlg = ListDlg;
        }

        void SetComboGelesen (bool gelesen)
        {
            this->ComboGelesen = gelesen;
        }
        bool GetComboGelesen (void)
        {
            return this->ComboGelesen;
        }

        LISTENTER *GetListDlg (void)
        {
            return ListDlg;
        }

        void SetLsts (struct LSTS *Lsts)
        {
            this->Lsts = Lsts;
        }

        struct LSTS *GetLsts (void)
        {
            return Lsts;
        }
		void SetAnzahl (int anzahl)
		{
			this->AnzahlDrk = anzahl;
		}
		int GetAnzahl (void)
		{
			return this->AnzahlDrk;
		}


        Work ()
        {
            Lsts = NULL;
            Dlg = NULL;
            ListDlg = NULL;
			LstCursor = -1;
        }


        void ExitError (BOOL);
        void TestNullDat (long *);
        void TestAllDat (void);
        void BeginWork (void);
        void CommitWork (void);
        void RollbackWork (void);
        void InitRec (void);
        void FillRow (void);
        int GetPtab (char *, char *, char *);
        int GetPtab (char *, char *, char *, char *); /* GK 30.06.2013 */
        int ShowPtab (char *dest, char *item);
        int ShowPtabEx (HWND, char *dest, char *item);
        int GetMdnName (char *,char *, short);
        int GetFilName (char *, short, short);
        void FillRow (char *);
        void DeleteAllPos (void);
        void DeleteAllZerm (void);
        void PrepareLst (void);
        void OpenLst (void);
        int FetchLst (void);
        void CloseLst (void);
        void InitRow (void);
        int ReadZerldaten (char *,double,double,char *, long,short,short);
        bool PruefeZerm (char *);
        int GetStaatAnz (void);
        int ReadLief (char *, char *);
        int ReadA (double, char *, char *);
        double TestABez (char *);
        void FillStaatCombo (char **, int, int);
		void SchreibeLief (void);
		void PruefeZerldaten (void);
		void PruefePartie (void);
		double GetArtikel  (int);
		int readwe  (void);
		int readwekopf  (void);
};

#endif