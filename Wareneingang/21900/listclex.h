#ifndef _eListDef
#define _eListDef
#include "info.h"
#include "itemc.h"
#define MAXFIELD 200

static mfont listdefaultfont = {"Arial", 100, 0, 0,
                                 BLACKCOL,
                                 GRAYCOL,
                                 0,
                                 NULL};

struct CHATTR
{
    char *feldname;
    int  UpdAttr;
    int  InsAttr;
};

class ListButton
{
     private :
         char *feld;
         HBITMAP BitMap;
         HBITMAP BitMapChecked;
         HWND Button;
         int x;
         int y;
         int cx;
         int cy;
         int typ;
         BOOL checked;
         char feldb [2];
     public :
         ListButton ()
         {

             Button = NULL;
             BitMap = NULL;
             typ = 0;
             checked = FALSE;
             strcpy (feldb, "N");
             feld = feldb;
         }

         ListButton (char *feld, HBITMAP BitMap, HWND Button)
         {
             SetFeld (feld);
             this->BitMap = BitMap;
             this->Button = Button;
             typ = 0;
             strcpy (feldb, feld);
         }

         ListButton (char *feld, HBITMAP BitMap, HWND Button, int typ)
         {
             SetFeld (feld);
             this->BitMap = BitMap;
             this->Button = Button;
             this->typ = typ;
             strcpy (feldb, feld);
         }

         ~ListButton ()
         {

             if (Button != NULL)
             {
                 DestroyWindow (Button);
             }

         }

         void SetTyp (int typ)
         {
             this->typ = typ;
         }

         int GetTyp (void)
         {
             return typ;
         }

         void SetFeld (char *feld)
         {

             strncpy (feldb, feld, 1);
             feldb[1] = 0;
             this->feld = feldb;

             if (feld[0] == 'J')
             {
                 checked = TRUE;
             }
             else
             {
                 checked = FALSE;
             }
         }

         void Check (BOOL checked)
         {
             this->checked = checked;
             if (checked)
             {
//TESTTEST todo                 strcpy (feld, "J");
             }
             else
             {
//TESTTEST todo                 strcpy (feld, "N");
             }
         }

         BOOL IsChecked (void)
         {
             return checked;
         }

         char *GetFeld (void)
         {
             return feld;
         }

         char *GetFeld (char *feld)
         {
             strcpy (feld, this->feld);
             return feld;
         }

         char GetFeldChar (void)
         {
             return feld[0];
         }

         BOOL GetFeldStat (void)
         {
             if (feld[0] == 'J')
             {
                 return TRUE;
             }
             return FALSE;
         }

         void SetBitMap (HBITMAP Bitmap)
         {
             this->BitMap = Bitmap;
         }

         HBITMAP GetBitMap (void)
         {
             return BitMap;
         }

         void SetButton (HWND Button)
         {
             this->Button = Button;
         }

         HWND GetButton (void)
         {
             return Button;
         }

         void SetRect (int x, int y, int cx, int cy)
         {
             this->x = x;
             this->y = y;
             this->cx = cx;
             this->cy = cy;
         }

         int GetX (void)
         {
             return x;
         }

         int GetY (void)
         {
             return y;
         }

         int GetCX (void)
         {
             return cx;
         }

         int GetCY (void)
         {
             return cy;
         }

         void Destroy (void)
         {
            if (Button != NULL)
             {
                 DestroyWindow (Button);
                 Button = NULL;
             }
          }
};

void SetComboMess (BOOL (*) (MSG *));

class KEYS 
{
   public :
      KEYS ()
      {
      }

      ~KEYS ()
      {
      }

      virtual BOOL OnKey1 (void)
      {
          return FALSE;
      }

      virtual BOOL OnKey2 (void)
      {
          return FALSE;
      }

      virtual BOOL OnKey3 (void)
      {
          return FALSE;
      }

      virtual BOOL OnKey4 (void)
      {
          return FALSE;
      }

      virtual BOOL OnKey5 (void)
      {
          return FALSE;
      }

      virtual BOOL OnKey6 (void)
      {
          return FALSE;
      }

      virtual BOOL OnKey7 (void)
      {
          return FALSE;
      }

      virtual BOOL OnKey8 (void)
      {
          return FALSE;
      }

      virtual BOOL OnKey9 (void)
      {
          return FALSE;
      }

      virtual BOOL OnKey10 (void)
      {
          return FALSE;
      }

      virtual BOOL OnKey11 (void)
      {
          return FALSE;
      }

      virtual BOOL OnKey12 (void)
      {
          return FALSE;
      }
      virtual BOOL OnKeyDel (void)
      {
          return FALSE;
      }
      virtual BOOL OnKeyInsert (void)
      {
          return FALSE;
      }
      virtual BOOL OnKeyUp (void)
      {
          return FALSE;
      }
      virtual BOOL OnKeyDown (void)
      {
          return FALSE;
      }
      virtual BOOL OnKeyNext (void)
      {
          return FALSE;
      }
      virtual BOOL OnKeyPrior (void)
      {
          return FALSE;
      }

      virtual BOOL OnKey (int Key)
      {
          switch (Key)
          {
              case VK_F1 :
                  return OnKey1 ();
              case VK_F2 :
                  return OnKey2 ();
              case VK_F3 :
                  return OnKey3 ();
              case VK_F4 :
                  return OnKey4 ();
              case VK_F5 :
                  return OnKey5 ();
              case VK_F6 :
                  return OnKey6 ();
              case VK_F7 :
                  return OnKey7 ();
              case VK_F8 :
                  return OnKey8 ();
              case VK_F9 :
                  return OnKey9 ();
              case VK_F10 :
                  return OnKey10 ();
              case VK_F11 :
                  return OnKey11 ();
              case VK_F12 :
                  return OnKey12 ();
              case VK_DELETE :
                  return OnKeyDel ();
              case VK_INSERT :
                  return OnKeyInsert ();
              case VK_UP :
                  return OnKeyUp ();
              case VK_DOWN :
                  return OnKeyDown ();
              case VK_NEXT :
                  return OnKeyNext ();
              case VK_PRIOR :
                  return OnKeyPrior ();
          }
          return FALSE;
      }

      virtual BOOL OnSysCommand (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
      {
          return FALSE;
      }
      virtual BOOL OnCommand (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
      {
          return FALSE;
      }

      virtual BOOL OnNotify (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
      {
          return FALSE;
      }

      virtual BOOL OnPaint (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
      {
          return FALSE;
      }

      virtual BOOL OnMove (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
      {
          return FALSE;
      }

      virtual int AfterRow (int Row)
      {
          return FALSE;
      }
      virtual int AfterCol (char *Colname)
      {
          return FALSE;
      }
      virtual int BeforeRow (int Row)
      {
          return FALSE;
      }
      virtual int BeforeCol (char *Colname)
      {
          return FALSE;
      }
      virtual void CreateListQuikInfos (HWND hWnd)
      {
      }
      virtual void FillCombo (HWND hWnd, char *ItemName)
      {
      }
      virtual void InitRow (void)
      {
      }
};


class ListClass
{
      private :
		     BOOL InvalidateFocusFrame; 
             int ListBreak; 
             char *eingabesatz;
             int NoScroll;
             int lPagelen;
             int LineRow;
             HWND    hwndTB;
             HWND    hTrack;
             HWND    vTrack;
             HDC     pHdc;
             int     y0;

             char frmrow [0x1000];
             TEXTMETRIC tm;
             PAINTSTRUCT aktpaint;

             BOOL IsArrow;
             HCURSOR oldcursor;
             HCURSOR arrow;
             HCURSOR aktcursor;
             int movfield; 
             int InMove;
			 RECT FocusRect;
             HFONT EditFont;
             HFONT ListhFont;
             mfont UbFont;
             mfont UbSFont;
             mfont SFont;
             mfont ListFont;
             mfont *AktFont;
             int LineColor;
             int InAppend;
             int NewRec;
             int paintallways;
             COLORREF Color;
             COLORREF BkColor;
             COLORREF Color0;
             COLORREF BkColor0;
             COLORREF NoScrollBkColor;
             struct CHATTR *ChAttr;
             int UbHeight;
			 double RowHeight;
			 BOOL listclipp;
			 BOOL setsel;
			 BOOL focusframe;
             BOOL NoRecNr;
			 int    RowHeightP;
			 int    UbRowsP;
             int    Plus3D;
             BOOL posset;
             KEYS *Keys;
             ListClass *OldList;
             HMENU hMenu;
             int cydiff;
             Info *Cinfo;
             BOOL FindEnabled;
             BOOL NoMove;
             BOOL PrintBackground;
             HBITMAP ArrDown;
             HBRUSH BkBrush;
      protected :
                 

      public :
             static ListClass *ActiveList;
             static int pagelen;
             static int pagelen0;
             static int ublen;
             static int PageStdCols;
             static int PageStart;
             static int PageEnd;
             static BOOL KeySend;
             unsigned char *ausgabesatz;
             int SwRecs;
             char **SwSaetze;
             char **LstSaetze;
             unsigned char *LstSatz;
//             FELDER *LstZiel;
             int zlenS;
             int scrollpos;
             int scrollposS;
             int printscrollpos;
             int recanz;
             int recHeight ;
             int PageView;
             int HscrollStart;
             int NoScrollBackground;
             HANDLE  hMainInst;
             HWND    hMainWindow; 
             HWND    mamain2; 
             HWND    mamain3; 
             field _UbForm [MAXFIELD];
             form UbForm;
             field _DataForm [MAXFIELD];
             form DataForm;
             field _LineForm [MAXFIELD];
             form LineForm;
             field _fUbSatzNr[1];
             form fUbSatzNr;
             form *UbRow;
             form *DataRow;
             form *lineRow;
             int *UbRows;
             int banz;
             int zlen;
             int banzS;
			 int WithFocus;
			 int  AktRow;
			 int  SelRow;
			 int AktColumn;
			 int  AktRowS;
             int AktPage;
			 int AktColumnS;
             int FindRow;
             int FindColumn;
			 char AktItem [21];
			 char AktValue [80];
			 char RowItem [21];
			 char RowValue [80];
             BOOL UseRowItem;
			 int  ListFocus;
			 int  AktFocus;
			 HWND FocusWindow;
             int RowMode;
             int UseZiel;
             int FrmRow;
             int (*InfoProc) (char **, char *, char *);
             int (*TestAppend) (void);
			 void (*RowProc) (COLORREF *, COLORREF *);
			 BOOL print3D;
             BOOL PrintHdc;

             char *Getfrmrow (void)
             {
                 return (char *) frmrow;
             }

             void SetListTextMetrics (TEXTMETRIC *tm)
             {
                 memcpy (&this->tm , &tm, sizeof (TEXTMETRIC));
             }

             TEXTMETRIC *GetListTextMetrics (void)
             {
                 return &tm;
             }

             void SetUbHeight (int UbHeight)
             {
                 this->UbHeight = UbHeight;
             }

             int GetUbHeight (void)
             {
                 return UbHeight;
             }

             void SetRowHeightP (int RowHeightP)
             {
                 this->RowHeightP = RowHeightP;
             }

             int GetRowHeightP (void)
             {
                 return RowHeightP;
             }

             mfont *GetAktFont (void)
             {
                 return AktFont;
             }

             void SetNoMove (BOOL b)
             {
                 NoMove = b;
             }

             BOOL GetNoMove (void)
             {
                 return NoMove;
             }


             void SetInfo (Info *Cinfo)
             {
                 this->Cinfo = Cinfo;
             }

             Info *GetInfo (void)
             {
                 return Cinfo;
             }


             void SetLines (int LineColor)
             {
                 this->LineColor = LineColor;
             }

             int GetLines (void)
             {
                 return LineColor;
             }

             void SetColor (COLORREF Color)
             {
                 this->Color = Color;
             }

             COLORREF GetColor (void)
             {
                 return Color;
             }

             void SetBkColor (COLORREF BkColor)
             {
                 this->BkColor = BkColor;
             }

             COLORREF GetBkColor (void)
             {
                 return BkColor;
             }

             void SetNoScrollBkColor (COLORREF NoScrollBkColor)
             {
                 this->NoScrollBkColor = NoScrollBkColor;
             }
             void SetCYDiff (int cydiff)
             {
                 this->cydiff = cydiff;
             }

             int GetCYDiff (void)
             {
                 return cydiff;
             }


             HWND GethMainWindow (void)
             {
                 return hMainWindow;
             }

             HMENU GethMenu (void)
             {
                return hMenu;
             }

             void SethMenu (HMENU hMenu)
             {
                this->hMenu = hMenu;
             }

             HWND GethwndTB (void)
             {
                return hwndTB;
             }

			 void SetPlus3D (int plus)
			 {
				 Plus3D = min (2, (max (plus,1)));
			 }

			 void Set3D (BOOL flag)
			 {
				 print3D = flag;
			 }

             void SetListFont (mfont *lFont)
             {
				 CopyFonts (lFont);
             }

             BOOL IsNewRec (void)
             {
                 return NewRec;
             }

             BOOL IsAppend (void)
             {
                 return InAppend;
             }

             void SetAppend (BOOL b)
             {
                 InAppend = b;
             }

             void SetInfoProc (int (*Proc) (char **, char *, char *))
             {
                 InfoProc = Proc;
             }

             void SetTestAppend (int (*Proc) (void))
             {
                 TestAppend = Proc;
             }

             void SetRowProc (void (*Proc) (COLORREF *, COLORREF *))
             {
                 RowProc = Proc;
             }

             form * GetUbForm (void)
             {
                 return &UbForm;
             }

             form * GetLineForm (void)
             {
                 return &LineForm;
             }

             form * GetDataForm (void)
             {
                 return &DataForm;
             }

			 char *GetAktItem (void)
			 {
                 if (UseRowItem)
                 {
                     return RowItem;

                 }
				 return AktItem;
			 }

			 char *GetAktValue (void)
			 {
                 if (UseRowItem)
                 {
                     return RowValue;

                 }
				 return AktValue;
			 }

             void SetChAttr (struct CHATTR *cha)
             {
                 ChAttr = cha;
             }

             void SetKeys (KEYS *Keys)
             {
                     this->Keys = Keys;
             }

             KEYS *GetKeys (void)
             {
                     return Keys;
             }

             ListClass ();
             ~ListClass ();

			 void SetRowHeight (double Height)
			 {
				    RowHeight = Height;
			 }

             void SetHscrollStart (int HscrollStart)
             {
                    this->HscrollStart = HscrollStart;
             }

             void SetNoScrollBackground (BOOL NoScrollBackground)
             {
                    this->NoScrollBackground = NoScrollBackground;
             }

			 void SetListclipp (BOOL flag)
			 {
				 listclipp = flag;
			 }

			 void SetSetsel (BOOL flag)
			 {
				 setsel = flag;
			 }

			 void SetFocusframe (BOOL flag)
			 {
				 focusframe = flag;
			 }

             void SetUbRows (int *ubr)
             {
                    UbRows = ubr;
             }

             int *GetUbRows (void)
             {
                    return UbRows;
             }

             int GetUbSpalte (int spalte)
             {
                    if (UbRows)
                    {
                        return UbRows[spalte];
                    }
                    return spalte;
             }

             void SetRowItem (char *name, char *value)
             {
                     strcpy (RowItem, name);
                     strcpy (RowValue, value);
                     UseRowItem = TRUE;
             }

             void InitRowItem (void)
             {
                     UseRowItem = FALSE;
             }

             void SetAusgabeSatz (unsigned char *satz)
             {
                    ausgabesatz = satz;
             }

             void BreakList (void)
             {
                    ListBreak = 1;
                    PostQuitMessage (0);
             }

             void SetListFocus (int focus)
             {
                    ListFocus = focus;
             }


             void SetPageView (int pg)
             {
                     PageView = pg;
             }

/*
             void SetLstZiel (FELDER *zl)
             {
                     LstZiel = zl;
             }
*/

             void SetRowMode (int mode)
             {
                    RowMode = mode;
             }

             void SetPos (int zeile, int spalte)
             {
					AktRow = zeile;
					AktColumn = spalte;
					posset = TRUE;
             }

             void Initscrollpos (void)
             {
                    scrollpos = 0;
             }

             void Setscrollpos (int pos)
             {
                 scrollpos = pos;
             }

             int Getscrollpos (void)
             {
                 return scrollpos;
             }


             void InitRecanz (void)
             {
                 recanz = 0;
             }

             int  GetRecanz (void)
             {
                 return recanz;
             }

             int GetRecHeight (void)
             {
                 return recHeight;
             }

             int  GetAktRow (void)
             {
                 return AktRow;
             }

             int  GetAktRowS (void)
             {
                 return AktRowS;
             }

             int  GetAktColumn (void)
             {
                 return AktColumn;
             }

             void SethwndTB (HWND hwndTB)
             {
                 this->hwndTB = hwndTB;
             }

             void SetSaetze (char **Saetze)
             {
                 SwSaetze = Saetze;
             }

             void Setbanz (int banz)
             {
                 this->banz = banz;
             }

             void Setzlen (int zlen)
             {
                 this->zlen = zlen;
             }

             void SetRecanz (int anz)
             {
                 recanz = anz;
             }

             void SetRecHeight (int height)
             {
                 recHeight = height;
             }

             void SetPagelen (int Pagelen)
             {
                 lPagelen = Pagelen;
             }

             void SetLineRow (int LineRow)
             {
                 this->LineRow = LineRow;
             }

             void SetTextMetric (TEXTMETRIC *ttm)
             {
                 memcpy (&tm, ttm, sizeof (tm));
				 if (RowHeight == (double) 0.0) 
				 {
					 RowHeight = (double) 2;
				 }
             }

             void GetTextMetric (TEXTMETRIC *ttm)
             {
                 memcpy (ttm, &tm, sizeof (tm));
             }
             void SetAktPaint (PAINTSTRUCT *pm)
             {
                 memcpy (&aktpaint, pm, sizeof (aktpaint));
             }

             HWND Getmamain2 (void)
             {
                  return mamain2;
             }

             HWND Getmamain3 (void)
             {
                  return mamain3;
             }

             HWND GethTrack (void)
             {
                  return  hTrack;
             }

             HWND GetvTrack (void)
             {
                  return  vTrack;
             }

             BOOL IsRowMode (void)
             {
                 return RowMode;
             }

             HWND     InitListWindow (HWND);
             void     GetPageLen (void);
             BOOL     TrackNeeded (void);
             void     CreateTrack (void);
             void     DestroyTrack (void); 

             BOOL     VTrackNeeded (void);
             void     CreateVTrack (void);
             void     DestroyVTrack (void);
             void     TestTrack (void);
             void     TestVTrack (void);
             void     TestMamain3 (void);
             void     MoveListWindow (void);
             void     SetDataStart (void);
             BOOL     MustPaint (int);
             void     PrintUSlinesSatzNr (HDC);
             void     PrintSlinesSatzNr (HDC);
             void     PrintVlineSatzNr (HDC);
             void     PrintNoHscroll3DLine (HDC, int);
             void     Print3DVlines (HDC);
             void     PrintNoHscrollLines (HDC);
             void     PrintVlines (HDC);
             void     PrintPVlines (HDC, RECT *);
             void     PrintNoHscroll3DLines (HDC);
             void     Print3DVline (HDC, int);
             void     PrintNoHscrollLine (HDC, int);
             void     PrintVline (HDC, int);
             void     PrintHlinesSatzNr (HDC);
             void     PrintHlineSatzNr (HDC, int);
             void     Print3DHlinesEnd (HDC);
             void     Print3DHlines (HDC);
             void     PrintHlines (HDC);
             void     PrintPHlines (HDC, RECT *);
             void     PrintLastPHline (HDC);
             void     Print3DHline (HDC, int);
             void     Print3DHlineEnd (HDC, int);
             void     PrintHline (HDC, int);
             void     ToCubFormat (char *, field *, int);
             void     FillCubPrintUb (char *, int);
             void     FillPrintUb (char *);
             void     FillPrintRow (char *, int);
             void     FillPrintUbBackground (HDC, int);
			 void     ShowFocusText (HDC, int, int);
			 void     GetFocusText (void);
			 void     ShowWindowText (int, int);
			 void     DestroyFocusWindow (void);
             void     ShowDropDown (void);
             void     CloseDropDown (void);
             void     SetFeldEdit (int, int);
             void     SetFocusFrame (HDC, int, int);
             void     SetFeldEdit0 (int, int, DWORD);
			 void     SetFeldFrame (HDC, int, int);
			 void     SetFeldFocus (HDC, int, int);
			 void     SetFeldFocus0 (int, int);
             int      DelRec (void); 
             void     InitListButton (field *);
             HWND     CreateListButton (int, int, int, int, HFONT, char *, DWORD);
             void     CheckButton (ITEM *);
             HWND     CreateCheckButton (int, int, int, int, HFONT, char *, DWORD);
             int      GetUbFrmlen (void);
             int      GetUbFrmlen (int);
#ifndef _PFONT
             void     FillFrmRow (char *, form *, int);
#else
             void     FillNoScrollBackground (HDC, int);
             void     ShowNoScrollArray (HDC, char *, form *, int, int);
             void     FillFrmRow (HDC, char *, form *, int, int);
             void     FillUbPFrmRow (HDC, char *, form *, int);
#endif
             int      GetMidPos (int y);
             void     ShowRowRect (COLORREF, int);
             void     TestTextLen (HDC, char *, int);
             void     ShowFrmRow (HDC, int, int);
             void     ShowSatzNr (HDC, int);
             void     ShowDataForms (HDC);
             void     ShowPrintForms (HDC, int);
             void     ScrollLeft (void);
             void     ScrollRight (void);
             void     ScrollPageDown (void);
             void     ScrollPageUp (void);
             void     SetPos (int);
             void     SetTop (void);
             void     SetBottom (void);
             void     HScroll (WPARAM, LPARAM);
             void     SetNewRow (int);
             void     SetVPos (int);
             void     ScrollWindow0 (HWND, int, int, RECT *, RECT *);
             void     ScrollDown (void);
             void     ScrollUp (void);
             void     ScrollVPageDown (void);
             void     ScrollVPageUp (void);
             void     VScroll (WPARAM, LPARAM);
			 void     FocusLeft (void);
			 void     FocusRight (void);
			 void     FocusUp (void);
			 void     FocusDown (void);
//             void     ZielFormat (char *, FELDER *);
             BOOL     IsInListArea (MSG *);
             void     OnNotify (HWND,UINT,WPARAM,LPARAM);
             void     OnPaint (HWND, UINT, WPARAM, LPARAM);
             void     OnSize (HWND, UINT, WPARAM, LPARAM);
             void     OnHScroll (HWND, UINT, WPARAM, LPARAM);
             void     OnVScroll (HWND, UINT, WPARAM, LPARAM);
             void     FunkKeys (WPARAM, LPARAM);
             void     FillUbForm (field *, char *, int, int,int);
             void     FillDataForm (field *, char *, int, int);
             void     FillLineForm (field *, int);
             void     MakeLineForm (form *);
             void     MakeDataForm (form *);
             void     MakeUbForm (void);
             void     MakeDataForm0 (void);

             void     SetLineForm (form *);
             void     SetDataForm (form *);
             void     SetUbForm (form *);
             void     SetDataForm0 (form *, form *);

             void     FreeLineForm (void);
             void     FreeDataForm (void);
             void     FreeUbForm (void);
             BOOL     IsMouseMessage (MSG *);
             void     StopMove (void);
             void     StopMove0 (void);
             void     StartMove (void);
             void     MoveLine (void);
             void     TestNextLine (void);
             int      GetFieldPos (char *);
             void     DestroyField (int);
             void     DelFormFieldEx (form *, int, int);
             void     DelListField (char *);
             void     ShowArrow (BOOL);
             int      IsUbEnd (MSG *);
             int      IsUbRow (MSG *);
			 void     EditScroll (void);
             void     ToDlgRec (char *);
             void     FromDlgRec (char *);
             void     ToDlgRec0 (char *);
             void     FromDlgRec0 (char *);
             void     FreeDlgSaetze (void);
             void     SwitchPage (int);
             void     SetPage (int);
             void     SetPage0 (int);
             void     PriorPageRow (void);
             void     NextPageRow (void);
             void     SetFont (mfont *);
             void     CopyFonts (mfont *);
             void     ChoiseFont (mfont *);
             void     ChoiseLines (HDC);
             BOOL     InRec (char *, int *);
             void     find (char *);
             void     FindString (void);
             void     FindNext (void);
             BOOL     NewSearchString ();
             void     SetListLines (int); 
             BOOL     SystemAction (int);
             void     FirstPos (void);
             BOOL     PerformMessage (MSG *);
             int      ProcessMessages(void);
             void     SetColors (COLORREF, COLORREF);

             BOOL    IsCheckColumn (int);
             BOOL    IsButtonColumn (int);
             BOOL    IsButtonCol (int);
             BOOL    IsEditColumn (int);
             int     FirstColumn (void);
             int     PriorColumn (void);
             int     NextColumn (void);
             int     FirstColumnR (void);
             int     PriorColumnR (void);
             int     NextColumnR (void);
             int     TestAfter (int);
             int     TestAfter (void);
             int     TestBefore (void);
             int     TestAfterR (void);
             int     TestBeforeR (void);
             int     TestAfterRow (void);
             int     TestBeforeRow (void);
             void    InitListButtons (form *);
             void    InitForm (form *);
             void    InsertLine (void);
             void    AppendLine (void);
             void    DeleteLine (void);
             void    ShowAktRow (void);
             void    ShowAktRow (short);
             void    ShowAktColumn (HDC, int, int, int);
             void    DisplayList (void);
             void    KillListFocus (void);
             void    SetListFocus (void);
             void    SetInsAttr (void);
             BOOL    IsInsButton (char *);
             BOOL    InInsAttr (char *, int);
             void    SetFieldAttr (char *name, int attr);
             void    SetFieldAttr (CHATTR *);
             void    DestroyRmFields (void);
             void    DestroyListWindow (void);
             void    ScrollUbForm (void);
			 void    PaintUb (void);
             void    SetNoRecNr (BOOL);
             void    Register (HINSTANCE);
             static  CALLBACK CProc(HWND,UINT, WPARAM,LPARAM);
};

class ListClassDB : public ListClass
{
     public :
            ListClassDB () : ListClass ()
            {
            }
            void     FreeBezTab (void);
            void     FillUbForm (field *, char *, int, int,int);
            void     MakeUbForm (void);
            void     SwitchPage (int);
            void     SwitchPage0 (int);
            char *   GetItemName (char *); 
};

#endif 
