#include <windows.h>
#include "searchzerldaten.h"
#include "strfkt.h"
#include "work.h"
#include "mo_wmess.h"
#include "lbox.h"


int SEARCHZERLDATEN::idx;
long SEARCHZERLDATEN::anz;
CHQEX *SEARCHZERLDATEN::Query = NULL;
DB_CLASS SEARCHZERLDATEN::DbClass; 
HINSTANCE SEARCHZERLDATEN::hMainInst;
HWND SEARCHZERLDATEN::hMainWindow;
HWND SEARCHZERLDATEN::awin;
short SEARCHZERLDATEN::mdn_nr = 0;
extern struct ZERLDATENF zerldatenf, zerldatenf_null;


int SEARCHZERLDATEN::SearchLst (char *sebuff)
/**
Nach Kurzname in Liste suchen.
**/
{
	   Query->SearchList ();
	   return 0;
}

int SEARCHZERLDATEN::ReadLst (char *name)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  char buffer1 [512];
	  int cursor;
	  int i;
      WMESS Wmess;
      //short mdn;
      //char adr_krz [17];

	  char cdat [11];
	  long lfd_i, dat;
	  long ab_datum;
	  double a;
	  char a_bz1[25];
	  char ident_extern[21];
	  char ident_intern[21];
	  char lief [17];
	  char lief_rech_nr [17];

	  anz = 0;
	  sysdate (cdat);
	  ab_datum = dasc_to_long (cdat) - 700;

      clipped (name);
/*
	          sprintf (buffer, "select lfd_i,dat,zerldaten.a,a_bas.a_bz1,ident_extern,ident_intern,lief,lief_rech_nr "
	 			       "from zerldaten,a_bas "
                       "where zerldaten.a = a_bas.a and zerldaten.a_gt = zerldaten.a %s order by zerldaten.lfd_i desc");
*/

	          sprintf (buffer, "select lfd_i,dat,zerldaten.a,a_bas.a_bz1,ident_extern,ident_intern,lief,lief_rech_nr "
	 			       "from zerldaten,a_bas "
                       "where zerldaten.a = a_bas.a and zerldaten.a_gt = zerldaten.a %s order by zerldaten.lfd_i desc"
                       , name);

	          sprintf (buffer1, "select lfd_i,dat,zerldaten.a,a_bas.a_bz1,ident_extern,ident_intern,lief,lief_rech_nr "
	 			       "from zerldaten,a_bas "
                       "where zerldaten.a = a_bas.a %s order by zerldaten.lfd_i desc"
                       , name);
	  /*
	          sprintf (buffer, "select lfd,dat,zerldaten.a,a_bas.a_bz1,ident_extern,ident_intern,lief,lief_rech_nr "
	 			       "from zerldaten,a_bas "
                       "where zerldaten.a = a_bas.a ");
					   */

 	  zerldatenf.such_a = 0.0;
      DbClass.sqlout ((long *) &lfd_i, 2, 0);
      DbClass.sqlout ((long *) &dat, 2, 0);
      DbClass.sqlout ((double *) &a, 3, 0);
      DbClass.sqlout ((char *) a_bz1, 0, 24);
      DbClass.sqlout ((char *) ident_extern, 0, 20);
      DbClass.sqlout ((char *) ident_intern, 0, 20);
      DbClass.sqlout ((char *) lief, 0, 16);
      DbClass.sqlout ((char *) lief_rech_nr, 0, 16);
      cursor = DbClass.sqlcursor (buffer);
      if (cursor < 0) 
      {
          return -1;
      }
  	  i = 0;
      Wmess.Message (hMainInst, hMainWindow, " \nBitte warten.....\n"
  	                                         "Die Daten werden selektiert.\n ");  
	  int dz = 0;
	  while (DbClass.sqlfetch (cursor) == 0)
      {
		  dz++;
		  if (dat > 0 ) dlong_to_asc (dat,cdat);
 //	      sprintf (buffer, "    %4d  |%-16s|  |%-16s|", mdn, lief, adr_krz);
	      sprintf (buffer, "    %4d  |%-10s|  %13.0lf |%-24s| |%-20s| |%-20s| |%-16s| |%-16s|", lfd_i, cdat, a,a_bz1,ident_extern,ident_intern,lief,lief_rech_nr);
	      Query->InsertRecord (buffer);
		  i ++;
      }
      anz = i;
	  DbClass.sqlclose (cursor);
	  if (dz == 0)
	  {
	    DbClass.sqlout ((long *) &lfd_i, 2, 0);
		DbClass.sqlout ((long *) &dat, 2, 0);
	    DbClass.sqlout ((double *) &a, 3, 0);
		DbClass.sqlout ((char *) a_bz1, 0, 24);
		DbClass.sqlout ((char *) ident_extern, 0, 20);
		DbClass.sqlout ((char *) ident_intern, 0, 20);
		DbClass.sqlout ((char *) lief, 0, 16);
		DbClass.sqlout ((char *) lief_rech_nr, 0, 16);
		cursor = DbClass.sqlcursor (buffer1);
	      if (cursor < 0) 
		  {
			return -1;
		}
  		i = 0;
		int dz = 0;
		while (DbClass.sqlfetch (cursor) == 0)
		{
			zerldatenf.such_a = a; 
			dz++;
			if (dat > 0 ) dlong_to_asc (dat,cdat);
			sprintf (buffer, "    %4d  |%-10s|  %13.0lf |%-24s| |%-20s| |%-20s| |%-16s| |%-16s|", lfd_i, cdat, a,a_bz1,ident_extern,ident_intern,lief,lief_rech_nr);
			Query->InsertRecord (buffer);
			i ++;
		}
		anz = i;
	  }

 	  Wmess.Destroy ();
	  EnableWindow (awin, FALSE);
	  return 0;
}

void SEARCHZERLDATEN::SetParams (HINSTANCE hMainInst, HWND hMainWindow)
{
      this->hMainInst   = hMainInst;
      this->hMainWindow = hMainWindow;
      awin = hMainWindow;
}

BOOL SEARCHZERLDATEN::GetKey (char * key)
{
      int anz;
     
      strcpy (key, " ");
      if (idx == -1) return FALSE;
      anz = wsplit (Key, "|");
      if (anz < 2) return FALSE;
      strcpy (key, clipped (wort[0]));
      return TRUE;
}


void SEARCHZERLDATEN::Search (void)
{
  	  int cx, cy;
	  char buffer [256];

	  idx = -1;
      cx = 48;
      cy = 25;
      Settchar ('|');
      EnableWindow (hMainWindow, FALSE);
      Query = new CHQEX (cx, cy);
      Query->EnableSort (TRUE);
      Query->OpenWindow (hMainInst, hMainWindow);
      sprintf (buffer, "%s %s %s",
                          "%d" 
                          "%s",
                          "%s");
      Query->RowAttr (buffer);
	  sprintf (buffer, "    %6s%18s", "1", "1"); 
	  Query->VLines (buffer, 3);
      sprintf (buffer, "4;4 11;16 31;16");
      Query->RowPos (buffer);
	  EnableWindow (awin, FALSE);

	  sprintf (buffer, "    %4s  %-16s  %-16s", "Mdn", "Lieferant", "Name"); 
	  Query->InsertCaption (buffer);

	  Query->SetFillDb (ReadLst);
	  Query->SetSearchLst (SearchLst);
      ReadLst ("");
      Query->SetSortRow (2, TRUE);
	  Query->ProcessMessages ();
      EnableWindow (hMainWindow, TRUE);
      if (syskey != KEY5)
      {
              idx = Query->GetSel ();
              if (idx != -1)
              {
                     Query->GetText (Key);
              }
      }
	  EnableWindow (awin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (awin);
}

void SEARCHZERLDATEN::Search (char *stext)
{
  	  int cx, cy;
	  char buffer [256];

	  idx = -1;
      cx = 48;
      cy = 25;
      Settchar ('|');
      EnableWindow (hMainWindow, FALSE);
      Query = new CHQEX (cx, cy);
      Query->EnableSort (TRUE);
      Query->OpenWindow (hMainInst, hMainWindow);
      sprintf (buffer, "%s %s %s",
                          "%d" 
                          "%s",
                          "%s");
      Query->RowAttr (buffer);
	  sprintf (buffer, "    %6s%18s", "1", "1"); 
	  Query->VLines (buffer, 3);
      sprintf (buffer, "4;4 11;16 31;16");
      Query->RowPos (buffer);
	  EnableWindow (awin, FALSE);

	  sprintf (buffer, "    %4s  %-16s  %-16s", "Mdn", "Lieferant", "Name"); 
	  Query->InsertCaption (buffer);

	  Query->SetFillDb (ReadLst);
	  Query->SetSearchLst (SearchLst);
      ReadLst ("");
      Query->SetSortRow (2, TRUE);
      if (stext != NULL && stext[0] != 0)
      {
             Query->SetSBuff (stext);
      }
	  Query->ProcessMessages ();
      EnableWindow (hMainWindow, TRUE);
      if (syskey != KEY5)
      {
              idx = Query->GetSel ();
              if (idx != -1)
              {
                     Query->GetText (Key);
              }
      }
	  EnableWindow (awin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (awin);
}

void SEARCHZERLDATEN::Search (char *where, char *stext, int FontSizeIdentNr)
{
  	  int cx, cy;
	  char buffer [256];

	  idx = -1;
      cx = 118;
      cy = 25;
      Settchar ('|');
	  
      EnableWindow (hMainWindow, FALSE);
      Query = new CHQEX (cx, cy, FontSizeIdentNr);
	  Query->SetFontSize(FontSizeIdentNr);
      Query->EnableSort (TRUE);
      Query->OpenWindow (hMainInst, hMainWindow);
      sprintf (buffer, "%s %s %s %s %s %s %s %s",
                          "%d" 
                          "%s",
                          "%13.0f" 
                          "%s",
                          "%s",
                          "%s",
                          "%s",
                          "%s");
      Query->RowAttr (buffer);
	  sprintf (buffer, "    %6s%12s%15s%24s%20s%20s%16s%16s", "1","1","1","1","1", "1","1","1"); 
	  Query->VLines (buffer, 3);
      sprintf (buffer, "4;4 11;12 31;16 47;16 ");
      Query->RowPos (buffer);
	  EnableWindow (awin, FALSE);

	  sprintf (buffer, "    %4s  %-12s  %-15s %-24s %-20s %-20s %-16s %-16s", "lfd_i", "Datum", "Artikel", "Bezeichnung","Ident-Extern.","Ident-Intern.","Lieferant","Lief/Rech" ); 
	  Query->InsertCaption (buffer);

	  Query->SetFillDb (ReadLst);
	  Query->SetSearchLst (SearchLst);
      ReadLst (where);
      Query->SetSortRow (1, TRUE);
      if (stext != NULL && stext[0] != 0)
      {
             Query->SetSBuff (stext);
      }
	  Query->ProcessMessages ();
      EnableWindow (hMainWindow, TRUE);
      if (syskey != KEY5)
      {
              idx = Query->GetSel ();
              if (idx != -1)
              {
                     Query->GetText (Key);
              }
      }
	  EnableWindow (awin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (awin);
}
