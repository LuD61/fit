#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "wmaskc.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "zerm.h"

struct ZERM zerm, zerm_null;
 
void ZERM_CLASS::prepare (void)
{
            char *sqltext;

//            init_sqlin ();
            ins_quest ((char *)   &zerm.mdn, 1, 0);
            ins_quest ((char *)   &zerm.zerm_aus_mat, 2, 0);
    out_quest ((char *) &zerm.mdn,1,0);
    out_quest ((char *) &zerm.schnitt,1,0);
    out_quest ((char *) &zerm.zerm_aus_mat,2,0);
    out_quest ((char *) &zerm.zerm_mat,2,0);
    out_quest ((char *) zerm.a_kz,0,3);
    out_quest ((char *) zerm.fix,0,2);
    out_quest ((char *) &zerm.fwp,3,0);
    out_quest ((char *) &zerm.zerm_gew,3,0);
    out_quest ((char *) &zerm.zerm_gew_ant,3,0);
    out_quest ((char *) zerm.zerm_teil,0,2);
    out_quest ((char *) &zerm.mat_o_b,3,0);
    out_quest ((char *) &zerm.hk_vollk,3,0);
    out_quest ((char *) &zerm.hk_teilk,3,0);
    out_quest ((char *) &zerm.delstatus,1,0);
    out_quest ((char *) &zerm.vollkosten_abs,3,0);
    out_quest ((char *) &zerm.vollkosten_proz,3,0);
    out_quest ((char *) &zerm.teilkosten_abs,3,0);
    out_quest ((char *) &zerm.teilkosten_proz,3,0);
    out_quest ((char *) &zerm.dm_100,3,0);
    out_quest ((char *) &zerm.a_z_f,2,0);
    out_quest ((char *) &zerm.vk_pr,3,0); 
    out_quest ((char *) &zerm.a_bez,0,25);
    out_quest ((char *) &zerm.a_bz2,0,25);   /* GK 02.07.2013 */
            cursor = prepare_sql ("select zerm.mdn,  "
"zerm.schnitt,  zerm.zerm_aus_mat,  zerm.zerm_mat,  zerm.a_kz,  zerm.fix,  "
"zerm.fwp,  zerm.zerm_gew,  zerm.zerm_gew_ant,  zerm.zerm_teil,  "
"zerm.mat_o_b,  zerm.hk_vollk,  zerm.hk_teilk,  zerm.delstatus,  "
"zerm.vollkosten_abs,  zerm.vollkosten_proz,  zerm.teilkosten_abs,  "
"zerm.teilkosten_proz,  zerm.dm_100,  zerm.a_z_f,  zerm.vk_pr, a_bas.a_bz1,  a_bas.a_bz2  "
"from zerm, outer a_bas "

#line 22 "zerm.rpp"
                                  "where zerm.zerm_mat = a_bas.a and zerm.mdn = ? and zerm.zerm_aus_mat = ? ");
		    ins_quest ((char *) &zerm.a_z_f,2,0);
            ins_quest ((char *)   &zerm.mdn, 1, 0);
            ins_quest ((char *)   &zerm.zerm_aus_mat, 2, 0);
            ins_quest ((char *)   &zerm.zerm_mat, 2, 0);
            sqltext = "update zerm set zerm.a_z_f = ? "

                                  "where mdn = ? and zerm_aus_mat = ? and zerm_mat = ?";
            ins_quest ((char *)   &zerm.mdn, 1, 0);
            ins_quest ((char *)   &zerm.zerm_aus_mat, 3, 0);
            ins_quest ((char *)   &zerm.zerm_mat, 2, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *)   &zerm.mdn, 1, 0);
            ins_quest ((char *)   &zerm.zerm_aus_mat, 2, 0);
            ins_quest ((char *)   &zerm.zerm_mat, 2, 0);
            test_upd_cursor = prepare_sql ("select schnitt from zerm "
                                  "where mdn = ? and zerm_aus_mat = ? and zerm_mat = ?");

            ins_quest ((char *)   &zerm.mdn, 1, 0);
            ins_quest ((char *)   &zerm.zerm_aus_mat, 2, 0);
            ins_quest ((char *)   &zerm.zerm_mat, 2, 0);
            del_cursor = prepare_sql ("delete from zerm "
                                  "where mdn = ? and zerm_aus_mat = ? and zerm_mat = ? ");
    ins_quest ((char *) &zerm.mdn,1,0);
    ins_quest ((char *) &zerm.schnitt,1,0);
    ins_quest ((char *) &zerm.zerm_aus_mat,2,0);
    ins_quest ((char *) &zerm.zerm_mat,2,0);
    ins_quest ((char *) zerm.a_kz,0,3);
    ins_quest ((char *) zerm.fix,0,2);
    ins_quest ((char *) &zerm.fwp,3,0);
    ins_quest ((char *) &zerm.zerm_gew,3,0);
    ins_quest ((char *) &zerm.zerm_gew_ant,3,0);
    ins_quest ((char *) zerm.zerm_teil,0,2);
    ins_quest ((char *) &zerm.mat_o_b,3,0);
    ins_quest ((char *) &zerm.hk_vollk,3,0);
    ins_quest ((char *) &zerm.hk_teilk,3,0);
    ins_quest ((char *) &zerm.delstatus,1,0);
    ins_quest ((char *) &zerm.vollkosten_abs,3,0);
    ins_quest ((char *) &zerm.vollkosten_proz,3,0);
    ins_quest ((char *) &zerm.teilkosten_abs,3,0);
    ins_quest ((char *) &zerm.teilkosten_proz,3,0);
    ins_quest ((char *) &zerm.dm_100,3,0);
    ins_quest ((char *) &zerm.a_z_f,2,0);
    ins_quest ((char *) &zerm.vk_pr,3,0);
            ins_cursor = prepare_sql ("insert into zerm (mdn,  "
"schnitt,  zerm_aus_mat,  zerm_mat,  a_kz,  fix,  fwp,  zerm_gew,  zerm_gew_ant,  "
"zerm_teil,  mat_o_b,  hk_vollk,  hk_teilk,  delstatus,  vollkosten_abs,  "
"vollkosten_proz,  teilkosten_abs,  teilkosten_proz,  dm_100,  a_z_f,  vk_pr) "

#line 39 "zerm.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?)");

#line 41 "zerm.rpp"
}
