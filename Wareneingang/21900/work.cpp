#include <windows.h>
#include <stdio.h>
#include "Work.h"
#include "strfkt.h"
#include "a_bas.h"
#include "mo_menu.h"
#include "datum.h"
//#include "mo_wdebug.h"
#include "wmaskc.h"

extern char LONGNULL[];
struct LIEF lief;

#define MAXTRIES 100

long Work::LongNull = 0x80000000;


void Work::ExitError (BOOL b)
{
    if (b)
    {
        disp_mess ("Speicher kann nicht zugeornet werden", 2);
        ExitProcess (1);
    }
}

void Work::TestNullDat (long *dat)
{
    if (*dat == 0l)
    {
        memcpy (dat, (long *) LONGNULL, sizeof (long));
    }
}

   

void Work::FillRow (void)
{

	short dmdn = 0;
	double da_gt = 0.0;
	double da = 0.0;
	long daktiv = 0;
	long dlfd = 0;

    if (Lsts == NULL) return;
    strcpy (Lsts->a_bez,  zerm.a_bez);
	if ( zerm.a_bz2[0] > ' ' )  // GK 02.07.2013
	{
	    clipped ( Lsts->a_bez );
	    clipped ( zerm.a_bz2 );
		strcat( Lsts->a_bez, " " ); 
		strcat( Lsts->a_bez, zerm.a_bz2 ); 
	}
	if (AnzahlDrk == 0) AnzahlDrk = 1;
    sprintf (Lsts->anzahl, "%d", zerm.a_z_f * AnzahlDrk);
    sprintf (Lsts->schnitt, "%d", zerm.schnitt);
    sprintf (Lsts->mdn, "%d", zerm.mdn);
//    sprintf (Lsts->mat, "%.0lf", GetArtikel(zerm.zerm_mat));
    sprintf (Lsts->mat, "%d", zerm.zerm_mat);
    sprintf (Lsts->aus_mat, "%.0lf", GetArtikel(zerm.zerm_aus_mat));
	strcpy (Lsts->me_buch, "J");
	if (zerldatenf.such_a >0)
	{
	
		if (zerldatenf.such_a != double(zerm.zerm_mat))
		{
			strcpy (Lsts->me_buch, "N");
		}
	}
	strcpy (Lsts->aktiv, " ");
	strcpy (Lsts->lfd, "0");

	dmdn = atoi(zerldatenf.mdn);
	da = zerm.zerm_mat;
	da_gt = ratod(zerldatenf.a_gt);
    DbClass.sqlin ((short *) &dmdn, 1, 0);
    DbClass.sqlin ((char *) &zerldatenf.ident_intern, 0, 20);
    DbClass.sqlin ((double *) &da, 3, 0);
    DbClass.sqlin ((double *) &da_gt, 3, 0);
    DbClass.sqlout ((long *) &daktiv, 2, 0);
    DbClass.sqlout ((long *) &dlfd, 2, 0);
    int dsqlstatus =  DbClass.sqlcomm ("select aktiv,lfd from zerldaten where mdn = ? and fil = 0 " 
										"and ident_intern = ? and a = ? and a_gt = ?");
	if (dsqlstatus == 0) 
	{
	    sprintf (Lsts->lfd, "%d", dlfd);
		if (daktiv == 0)
		{
			strcpy (Lsts->aktiv, "X");
		}
	}


//    Lsts->mebuch.SetTyp (0); 
//    Lsts->mebuch.SetFeld (Lsts->me_buch); 
//    Lsts->mebuch.SetBitMap (BMAP::LoadBitmap (DLG::hInstance, "SEL", "MSK", WHITECOL));


} 

double Work::GetArtikel (int material)
{
	char buffer [128];
	double artikel = -1;
    DbClass.sqlin ((int *) &material, 2, 0);
    DbClass.sqlout ((double *) &artikel, 3, 0);
    int dsqlstatus =  DbClass.sqlcomm ("select a from a_mat where mat = ? ");
	if (dsqlstatus != 0) 
	{
		sprintf(buffer,"Kein Artikel zum Material %d vorhanden",material);
//		disp_mess (buffer,2);   Hier Access violation warum ?? todo
		artikel = -1;
	}

	return artikel;
 }
int Work::readwe (void)
{
	short dmdn = 0;
	double da_gt = 0.0;
	short dGebLand = 0;
	short dMastLand = 0;
	short dZerLand = 0;
	short dSchlaLand = 0;

	dmdn = atoi(zerldatenf.mdn);
	da_gt = ratod(zerldatenf.a_gt);
    DbClass.sqlin ((char *) &zerldatenf.lief_rech_nr, 0, 20);
    DbClass.sqlin ((char *) &zerldatenf.lief, 0, 18);
    DbClass.sqlin ((double *) &da_gt, 3, 0);
    DbClass.sqlin ((short *) &dmdn, 1, 0);

    DbClass.sqlout ((char *) &zerldatenf.ident_extern, 0, 20);
    DbClass.sqlout ((short *) &dGebLand, 1, 0);
    DbClass.sqlout ((short *) &dMastLand, 1, 0);
    DbClass.sqlout ((short *) &dZerLand, 1, 0);
    DbClass.sqlout ((short *) &dSchlaLand, 1, 0);
    DbClass.sqlout ((char *) &zerldatenf.esnum, 0, 11);
    DbClass.sqlout ((char *) &zerldatenf.eznum1, 0, 11);
    DbClass.sqlout ((char *) &zerldatenf.eunum1, 0, 12);
    DbClass.sqlout ((char *) &zerldatenf.eunum2, 0, 12);
/**
    int dsqlstatus =  DbClass.sqlcomm ("select we_jour.ls_ident,rind.ls_land,rind.ls_es,rind.ls_ez from we_jour, outer rind "
										"where we_jour.lief_rech_nr  = ? "
										"and we_jour.lief = ? and we_jour.a = ? and we_jour.mdn = ? and we_jour.fil = 0 "
										"and we_jour.blg_typ = \"L\" and we_jour.ls_ident = rind.ls_ident ");
***/
    int dsqlstatus =  DbClass.sqlcomm ("select we_jour.ls_ident,zerldaten.gebland,zerldaten.mastland, "
										"zerldaten.zerland,zerldaten.schlaland,zerldaten.esnum,zerldaten.eznum1, "
										"zerldaten.eunum1, zerldaten.eunum2 from we_jour, outer zerldaten "
										"where we_jour.lief_rech_nr  = ? "
										"and we_jour.lief = ? and we_jour.a = ? and we_jour.mdn = ? and we_jour.fil = 0 "
										"and we_jour.blg_typ = \"L\" " 
										"and we_jour.ls_ident = zerldaten.ident_extern "
										"and we_jour.mdn = zerldaten.mdn "
										"and we_jour.fil = zerldaten.fil "
										"and we_jour.lief = zerldaten.lief "
										"and we_jour.lief_rech_nr = zerldaten.lief_rech_nr ");

	return dsqlstatus;
 }
int Work::readwekopf (void)
{
	short dmdn = 0;

	dmdn = atoi(zerldatenf.mdn);
    DbClass.sqlin ((char *) &zerldatenf.lief_rech_nr, 0, 20);
    DbClass.sqlin ((char *) &zerldatenf.lief, 0, 18);
    DbClass.sqlin ((short *) &dmdn, 1, 0);

    DbClass.sqlout ((short *) &dmdn, 1, 0);
    int dsqlstatus =  DbClass.sqlcomm ("select mdn from we_kopf "
										"where lief_rech_nr  = ? "
										"and lief = ? and mdn = ? and fil = 0 "
										"and blg_typ = \"L\" ");

	return dsqlstatus;
 }


void Work::InitRow (void)
{
    if (Lsts == NULL) return;

}

int Work::GetPtab (char *dest, char *item, char *wert)
{
    int dsqlstatus;

    dest[0] = 0;
    dsqlstatus = Ptab.lese_ptab (item, wert);  
    if (dsqlstatus == 100)
    {
        return -1;
    }
    strcpy (dest, ptabn.ptbez);
    return 0;
}

/* GK 30.06.2013 */
int Work::GetPtab (char *dest, char *destk ,char *item, char *wert)
{
    int dsqlstatus;

    dest[0] = 0;
    dsqlstatus = Ptab.lese_ptab (item, wert);  
    if (dsqlstatus == 100)
    {
        return -1;
    }
    strcpy (dest, ptabn.ptbez);
	strcpy (destk, ptabn.ptbezk );
    return 0;
}

int Work::ShowPtab (char *dest, char *item)
{
    int dsqlstatus;

    dsqlstatus = Ptab.Show (item);  
    if (syskey == KEY5)
    {
        return -1;
    }
    strcpy (dest, ptabn.ptwert);
    return 0;
}

int Work::ShowPtabEx (HWND hMainWindow, char *dest, char *item)
{
       char wert [80];
       SEARCHPTAB *SearchPtab;
	   HINSTANCE hMainInst;


       hMainInst = (HINSTANCE) GetWindowLong (hMainWindow, GWL_HINSTANCE);
       SearchPtab = new SEARCHPTAB (item);
       SearchPtab->SetParams (hMainInst, hMainWindow); 
       SearchPtab->Setawin (hMainWindow);
                                                  
       SearchPtab->Search ();
       if (SearchPtab->GetKey (wert) == TRUE)
       {
           delete SearchPtab;
           strcpy (dest, wert);
		   return 0;
       }
       delete SearchPtab;
       return -1;
}


int Work::GetMdnName (char *dest,char *ez, short mdn)
{
    int dsqlstatus;

    if (mdn == 0)
    {
        strcpy (dest, "Zentrale");
        return 0;
    }
    dest[0] = 0;
    DbClass.sqlin ((short *) &mdn, 1, 0);
    DbClass.sqlout ((char *) ez, 0, 10);
    DbClass.sqlout ((char *) dest, 0, 17);
    dsqlstatus =  DbClass.sqlcomm ("select ez_nr,adr.adr_krz from mdn,adr "
                                   "where mdn.mdn = ? " 
                                   "and adr.adr = mdn.adr");
    if (dsqlstatus == 100)
    {
        return 100;
    }
    return Mdn.lese_mdn (mdn);
}

int Work::ReadZerldaten (char *ident_extern, double a_gt, double a, char *lief,long dat, short mdn, short fil)
{
    int dsqlstatus;

    strcpy (zerldaten.ident_extern, ident_extern);
	zerldaten.a_gt = a_gt;
	zerldaten.a = a;
    strcpy (zerldaten.lief, lief);
	zerldaten.dat = dat;
	zerldaten.mdn = mdn;
	zerldaten.fil = fil;
    dsqlstatus = Zerldaten.dbreadfirst ();
    return dsqlstatus;
}

int Work::ReadLief (char *lieferant, char *lief_krz)
{
    int cursor;
    int dsqlstatus;
	char buffer [512];

    sprintf (buffer, "select  lief,adr.adr_krz, esnum,eznum,geburt,mast,schlachtung,zerlegung "
	 			       "from lief, adr "
                       "where adr.adr = lief.adr "
                       "and lief = \"%s\"", lieferant );
      DbClass.sqlout ((char *) lief.lief, 0, 16);
      DbClass.sqlout ((char *) lief_krz, 0, 17);
      DbClass.sqlout ((char *) lief.esnum, 0, 11);
      DbClass.sqlout ((char *) lief.eznum, 0, 11);
      DbClass.sqlout ((short *) &lief.geburt, 1, 0);
      DbClass.sqlout ((short *) &lief.mast, 1, 0);
      DbClass.sqlout ((short *) &lief.schlachtung, 1, 0);
      DbClass.sqlout ((short *) &lief.zerlegung, 1, 0);
      cursor = DbClass.sqlcursor (buffer);
      if (cursor < 0) 
      {
          return -1;
      }
  	  dsqlstatus = DbClass.sqlfetch (cursor) ;
	  DbClass.sqlclose (cursor);
    return dsqlstatus;
}

int Work::ReadA (double artikel, char *a_bez, char *kategorie)
{
    int cursor;
    int dsqlstatus;
	char buffer [512];

    sprintf (buffer, "select  a_bas.a_bz1,ag.tier_kz from a_bas,ag where a_bas.a = %13.0lf and a_bas.ag = ag.ag",artikel);
      DbClass.sqlout ((char *) a_bez, 0, 25);
      DbClass.sqlout ((char *) kategorie, 0, 3);
      cursor = DbClass.sqlcursor (buffer);
      if (cursor < 0) 
      {
          return -1;
      }
  	  dsqlstatus = DbClass.sqlfetch (cursor) ;
	  DbClass.sqlclose (cursor);
    return dsqlstatus;
}
double Work::TestABez (char *a_bez)  //Testet , ob die Artikelbezeichnung eindeutig ist
{
    int cursor;
    int dsqlstatus;
	char buffer [512];
	bool eindeutig = TRUE;
	double a;

	if (strlen (clipped(a_bez)) < 2) return FALSE ;

	  sprintf (buffer, "select  a_bas.a from a_bas where a_bas.a_bz1 = \"%s\" and (charg_hand = 9 or zerl_eti = 1) ",a_bez);
      DbClass.sqlout ((double *) &a, 3, 0);
      cursor = DbClass.sqlcursor (buffer);
      if (cursor < 0) 
      {
          return 0.0;
      }
  	  dsqlstatus = DbClass.sqlfetch (cursor) ;
	  if (dsqlstatus != 0) eindeutig = FALSE;
	if (eindeutig == TRUE)
	{
  	  dsqlstatus = DbClass.sqlfetch (cursor) ;
	  if (dsqlstatus == 0 ) eindeutig = FALSE; 
	}

	  DbClass.sqlclose (cursor);

    if (eindeutig == FALSE) a = 0.0;
    return a;
}

int Work::GetFilName (char *dest, short mdn, short fil)
{

    if (mdn == 0 || fil == 0)
    {
        strcpy (dest, "Mandant");
        return 0;
    }
    dest[0] = 0;
    DbClass.sqlin ((short *) &mdn, 1, 0);
    DbClass.sqlin ((short *) &fil, 1, 0);
    DbClass.sqlout ((char *) dest, 0, 17);
    return DbClass.sqlcomm ("select adr.adr_krz from fil,adr "
                             "where fil.mdn = ? " 
                             "and   fil.fil = ? " 
                             "and adr.adr = fil.adr");
}


void Work::BeginWork (void)
{
    ::beginwork ();
}

void Work::CommitWork (void)
{
    ::commitwork ();
}

void Work::RollbackWork (void)
{
    ::rollbackwork ();
}

void Work::InitRec (void)
{
 //   memcpy (&zerldaten, &zerldaten_null, sizeof (struct ZERLDATEN));
}



void Work::DeleteAllPos (void)
{
	    DbClass.sqlin ((char *)   zerldaten.ident_intern, 0, 20);
	    DbClass.sqlin ((double *)   &zerldaten.a_gt, 3, 0);
	    DbClass.sqlin ((char *)   zerldaten.ident_extern, 0, 20);
	    DbClass.sqlin ((short *)   &zerldaten.mdn, 1, 0);
	    DbClass.sqlin ((short *)   &zerldaten.fil, 1, 0);
		int dsqlstatus = DbClass.sqlcomm ("delete from zerldaten "
                                      "where ident_intern = ? and a_gt = ? and ident_extern = ? "
									  "and mdn = ? and fil = ?");
	
}
void Work::DeleteAllZerm (void)
{
	int dmat = (long) zerldaten.a_gt;
	    DbClass.sqlin ((short *)   &zerldaten.mdn, 1, 0);
	    DbClass.sqlin ((int *)   &dmat, 2, 0);
		int dsqlstatus = DbClass.sqlcomm ("delete from zerm "
                                      "where mdn = ? and zerm_aus_mat = ?");
	
}


void Work::PrepareLst (void)
{
}

void Work::OpenLst (void)
{
	if (LstCursor == -1)
	{
		PrepareLst ();
		if (LstCursor == -1) return;
	}
}

int Work::FetchLst (void)
{
	if (LstCursor == -1)
	{
		PrepareLst ();
		if (LstCursor == -1) return 100;
	}
    return 0;
}

void Work::CloseLst (void)
{
	if (LstCursor != -1)
	{
		LstCursor = -1;
	}
}

bool Work::PruefeZerm (char * kat)
{
	short mdn ;
	int dsqlstatus;
	    DbClass.sqlin ((double *)   &zerldaten.a_gt, 3, 0);
		DbClass.sqlout ((long *) &mdn, 1, 0);
		dsqlstatus = DbClass.sqlcomm ("select mdn from zerm "
                                      "where zerm_aus_mat = ? ");
		if (dsqlstatus == 0) return TRUE;

		return FALSE;
}


int Work::GetStaatAnz (void)
{
    PTAB_CLASS Ptab;
    int dsqlstatus;

    ptabk.ptanzpos = 0;
    dsqlstatus = Ptab.lese_ptab_all ("staat");
    return ptabk.ptanzpos;
}

void Work::FillStaatCombo (char **Combo, int plus, int anz)
{
    PTAB_CLASS Ptab;
    int dsqlstatus;
    int i = 0;
 
    dsqlstatus = Ptab.lese_ptab_all ("staat");

    while (dsqlstatus == 0)
    {
        if (Combo[i] == NULL) break;
        sprintf (Combo[i], "%d   (%s)", atoi (ptabn.ptwert) + plus, clipped (ptabn.ptbezk));
        dsqlstatus = Ptab.lese_ptab_all ();
        i ++;
        if (i == anz) break;
    }
    Combo[i] = NULL;
}
void Work::SchreibeLief (void)
{
	if ( RFE_Lohmann ) return; /* Schreibe Lief? Nein! GK 25.06.2013 */

	    BeginWork();
	    DbClass.sqlin ((char *)   zerldatenf.esnum, 0, 3);
	    DbClass.sqlin ((char *)   zerldatenf.eznum1, 0, 3);
	    DbClass.sqlin ((short *)  &lief.geburt, 1, 0);
	    DbClass.sqlin ((short *)  &lief.mast, 1, 0);
	    DbClass.sqlin ((short *)  &lief.schlachtung, 1, 0);
	    DbClass.sqlin ((short *)  &lief.zerlegung, 1, 0);
	    DbClass.sqlin ((char *)   lief.lief, 0, 16);
		DbClass.sqlcomm ("update lief set esnum = ?, eznum = ?,geburt = ?,mast = ?, "
										 " schlachtung = ?, zerlegung = ? "
                                      "where lief = ? ");

		CommitWork();

}

void Work::PruefeZerldaten (void)
{
	    DbClass.sqlout ((long *)   &zerldaten.lfd_i, 2, 0);
		if (DbClass.sqlcomm ("select lfd_i from zerldaten order by lfd_i desc") == 100)
		{
			zerldaten.lfd_i = 0;
		}
		else
		{
			zerldaten.lfd_i++;
		}
		if (zerldaten.a == 0.0) zerldaten.a = zerldaten.a_gt;
        zerldaten.dat = dasc_to_long (zerldatenf.dat);

		zerldaten.mdn = atoi(zerldatenf.mdn);
		zerldaten.fil = atoi(zerldatenf.fil);
		sprintf (zerldaten.lief, "%s", zerldatenf.lief);
		sprintf (zerldaten.lief_rech_nr, "%s", zerldatenf.lief_rech_nr);
		zerldaten.partie = atoi (zerldatenf.partie);
		zerldaten.anz_gedruckt = atoi (zerldatenf.anz_gedruckt);
		zerldaten.a_gt = ratod(zerldatenf.a_gt);
		zerldaten.gebland = atoi (zerldatenf.gebland);
		zerldaten.mastland = atoi (zerldatenf.mastland);
		zerldaten.schlaland = atoi (zerldatenf.schlaland);
		zerldaten.zerland = atoi (zerldatenf.zerland);
	  sprintf (zerldaten.esnum, "%s", zerldatenf.esnum);
	  sprintf (zerldaten.eznum1, "%s", zerldatenf.eznum1);
	  sprintf (zerldaten.eunum1, "%s", zerldatenf.eunum1);
	  sprintf (zerldaten.eunum2, "%s", zerldatenf.eunum2);
//	  sprintf (zerldaten.eznum2, "%s", eznum2);
	  sprintf (zerldaten.eznum3, "%s", zerldatenf.eznum3);
	  sprintf (zerldaten.kategorie, "%s", zerldatenf.tier_kz);
	  sprintf (zerldaten.ident_extern, "%s", zerldatenf.ident_extern);
	  sprintf (zerldaten.ident_intern, "%s", zerldatenf.ident_intern);

}

void Work::PruefePartie (void)
{
	char cIdent [10];

	if ( RFE_Lohmann ) return; /* Pruefe Partie? Nein! GK 25.06.2013 */
    
        zerldaten.dat = dasc_to_long (zerldatenf.dat);
	    DbClass.sqlin ((short *)   &zerldaten.mdn, 1, 0);
	    DbClass.sqlin ((long *)   &zerldaten.dat, 2, 0);
	    DbClass.sqlin ((char *)   &zerldaten.lief, 0, 16);
	    DbClass.sqlout ((long *)   &zerldaten.partie, 2, 0);
	    DbClass.sqlout ((char *)   zerldaten.ident_intern, 0, 20);
		if (DbClass.sqlcomm ("select partie,ident_intern from zerldaten where mdn = ? and dat = ? and lief = ? "
			"order by partie desc") == 100 || syskey == KEY8)
		{
			DbClass.sqlin ((short *)   &zerldaten.mdn, 1, 0);
		    DbClass.sqlin ((long *)   &zerldaten.dat, 2, 0);
			DbClass.sqlout ((long *)   &zerldaten.partie, 2, 0);
		    DbClass.sqlout ((char *)   zerldaten.ident_intern, 0, 20);
			if (DbClass.sqlcomm ("select partie,ident_intern from zerldaten where mdn = ? and dat = ? order by partie desc") == 100)
			{
				zerldaten.partie = 1;
				sprintf(cIdent,"%02d%02d%02d%02d",get_jahr(zerldatenf.dat),get_monat(zerldatenf.dat),get_tag(zerldatenf.dat),zerldaten.partie);
				strcpy (zerldatenf.ident_intern,cIdent);
				strcpy (zerldaten.ident_intern,cIdent);
			}
			else
			{
				zerldaten.partie++;
				sprintf(cIdent,"%02d%02d%02d%02d",get_jahr(zerldatenf.dat),get_monat(zerldatenf.dat),get_tag(zerldatenf.dat),zerldaten.partie);
				strcpy (zerldatenf.ident_intern,cIdent);
				strcpy (zerldaten.ident_intern,cIdent);
			}
		}
		else
		{
				sprintf(cIdent,"%02d%02d%02d%02d",get_jahr(zerldatenf.dat),get_monat(zerldatenf.dat),get_tag(zerldatenf.dat),zerldaten.partie);
				strcpy (zerldatenf.ident_intern,cIdent);
				strcpy (zerldaten.ident_intern,cIdent);
		}
		sprintf(zerldatenf.partie,"%ld",zerldaten.partie);
}
