#ifndef _ZERLDATEN_DEF
#define _ZERLDATEN_DEF

#include "windows.h"
#include "mo_draw.h"
#include "wmask.h"
#include "dbclass.h"

struct ZERLDATEN {
   short     mdn;
   short     fil;
   long      partie;
   char      lief[17];
   char      lief_rech_nr[17];
   char      ident_extern[21];
   char      ident_intern[21];
   double    a;
   double    a_gt;
   char      kategorie[4];
   short     schnitt;
   short     gebland;
   short     mastland;
   short     schlaland;
   short     zerland;
   char      esnum[11];
   char      eznum1[11];
   char      eznum2[11];
   char      eznum3[11];
   long      dat;
   short     anz_gedruckt;
   short     anz_entnommen;
   long      lfd;
   long      lfd_i;
   short     zerlegt;
   short     aktiv;
   char      eunum1[13];
   char      eunum2[13];
};
extern struct ZERLDATEN zerldaten, zerldaten_null;

#line 10 "zerldaten.rh"

class ZERLDATEN_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
               void prepare_lfd (void);
               int cursor_lfd;
               int upd_cursor_lfd;
       public :
               ZERLDATEN_CLASS () : DB_CLASS (),
                                  cursor_lfd (-1)
               {
               }
               int dbreadfirst_lfd (void);
               int dbclose_lfd (void);
               int dbupdate_lfd (void);
               int dbupdate (void);
};
#endif
