#ifndef _SEARCHMDN_DEF
#define _SEARCHMDN_DEF
#include "wmaskc.h"
#include "cmask.h"
#include "mo_chqex.h"
#include "dbclass.h"

class SEARCHMDN
{
       private :
           static HINSTANCE hMainInst;
           static HWND hMainWindow;
           static DB_CLASS DbClass; 
           static HWND awin;
           static int idx;
           static long anz;
           static CHQEX *Query;

           int SearchPos;
           int OKPos;
           int CAPos;
           char Key [512];

        public :
           SEARCHMDN ()
           {
                  SearchPos = 8;
                  OKPos = 9;
                  CAPos = 10;
           }

           ~SEARCHMDN ()
           {
                   if (Query)
                   {
                       delete Query;
                       Query = NULL;
                   }
           }

/*
           SWG *GetLine (void)
           {
               if (idx == -1) return NULL;
               return &sline;
           }
*/

           void Setawin (HWND awin)
           {
               this->awin = awin;
           }

           static int SearchLst (char *);
           static int ReadLst (char *);
           BOOL GetKey (char *);
           void SetParams (HINSTANCE, HWND);
           void Search (void);
};  
#endif