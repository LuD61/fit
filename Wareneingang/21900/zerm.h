#ifndef _ZERM_DEF
#define _ZERM_DEF

#include "windows.h"
#include "mo_draw.h"
#include "wmask.h"
#include "dbclass.h"

struct ZERM {
   short     mdn;
   short     schnitt;
   long      zerm_aus_mat;
   long      zerm_mat;
   char      a_kz[3];
   char      fix[2];
   double    fwp;
   double    zerm_gew;
   double    zerm_gew_ant;
   char      zerm_teil[2];
   double    mat_o_b;
   double    hk_vollk;
   double    hk_teilk;
   short     delstatus;
   double    vollkosten_abs;
   double    vollkosten_proz;
   double    teilkosten_abs;
   double    teilkosten_proz;
   double    dm_100;
   long      a_z_f;
   double    vk_pr;
   char      a_bez[25];
   char      a_bz2[25];  /* GK 02.07.2013 */
};
extern struct ZERM zerm, zerm_null;

#line 10 "zerm.rh"

class ZERM_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               ZERM_CLASS () : DB_CLASS ()
               {
               }
};
#endif
