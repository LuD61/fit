#include <windows.h>
#include "searcha.h"
#include "strfkt.h"
#include "mo_wmess.h"
#include "lbox.h"
#include "work.h"


int SEARCHA::idx;
long SEARCHA::anz;
CHQEX *SEARCHA::Query = NULL;
DB_CLASS SEARCHA::DbClass; 
HINSTANCE SEARCHA::hMainInst;
HWND SEARCHA::hMainWindow;
HWND SEARCHA::awin;
short SEARCHA::mdn_nr = 0;
BOOL SEARCHA::we_vorhanden = 0;


int SEARCHA::SearchLst (char *sebuff)
/**
Nach Kurzname in Liste suchen.
**/
{
	   Query->SearchList ();
	   return 0;
}

int SEARCHA::ReadLst (char *name)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int cursor;
	  int i;
      WMESS Wmess;
      double a;
      char a_bz1 [25];
      char a_bz2 [25];
      char a_bez [52];


	  anz = 0;
      clipped (name);
      if (strcmp (name, " ") == 0) name[0] = 0;

	  if (we_vorhanden == TRUE)
	  {
		if (strlen (name) == 0 ||
			name[0] <= ' ')
		{
				sprintf (buffer, "select a_bas.a,a_bas.a_bz1,a_bas.a_bz2 "
	 			       "from a_bas,we_jour where a_bas.a = we_jour.a and "
					   "we_jour.lief_rech_nr = \"%s\" "
					   "and we_jour.lief = \"%s\" "
					   "and we_jour.mdn = \"%s\" "
					   "and we_jour.blg_typ = \"L\" "
					   "and we_jour.fil = 0 and we_jour.ls_ident > \"\" "
                       "and a_bas.a > 0 "
					   "order by a_bas.a", zerldatenf.lief_rech_nr,zerldatenf.lief,zerldatenf.mdn);
		}
		else
		{	
				sprintf (buffer, "select a_bas.a,a_bas.a_bz1,a_bas.a_bz2 "
	 			       "from a_bas,we_jour where a_bas.a = we_jour.a and "
					   "we_jour.lief_rech_nr = \"%s\" "
					   "and we_jour.lief = \"%s\" "
					   "and we_jour.mdn = \"%d\" "
					   "and we_jour.blg_typ = \"L\" "
					   "and we_jour.fil = 0 and we_jour.ls_ident > \"\" "
                       "and a_bas.a > 0 "
                       "and a_bas.a_bz1 matches \"%s\"",zerldatenf.lief_rech_nr,zerldatenf.lief,zerldatenf.mdn, name);
		}
	  }
	  else
	  {
		if (strlen (name) == 0 ||
			name[0] <= ' ')
		{
				sprintf (buffer, "select a,a_bz1,a_bz2 "
	 			       "from a_bas where (charg_hand = 9 or zerl_eti = 1)  "
                       "and a_bas.a > 0 "
                       "order by a");
		}
		else
		{	
	          sprintf (buffer, "select a,a_bz1,a_bz2 "
	 			       "from a_bas "
                       "where (charg_hand = 9 or zerl_eti = 1) and a_bas.a > 0 and a_bz1 matches \"%s\"", name);
		}
	  }
      DbClass.sqlout ((double *) &a, 3, 0);
      DbClass.sqlout ((char *) a_bz1, 0, 24);
      DbClass.sqlout ((char *) a_bz2, 0, 24);
      cursor = DbClass.sqlcursor (buffer);
      if (cursor < 0) 
      {
          return -1;
      }
  	  i = 0;
      Wmess.Message (hMainInst, hMainWindow, " \nBitte warten.....\n"
  	                                         "Die Daten werden selektiert.\n ");  
	  while (DbClass.sqlfetch (cursor) == 0)
      {
		   strcpy (a_bez,clipped(a_bz1));
		   strcat (a_bez, " ");
		   strcat (a_bez, clipped(a_bz2));
 	      sprintf (buffer, "         %13.0lf  |%-50s|", a, a_bez);
	      Query->InsertRecord (buffer);
		  i ++;
      }
      anz = i;
	  DbClass.sqlclose (cursor);
 	  Wmess.Destroy ();
	  EnableWindow (awin, FALSE);
	  return 0;
}

void SEARCHA::SetParams (HINSTANCE hMainInst, HWND hMainWindow, BOOL we_vorhanden)
{
      this->hMainInst   = hMainInst;
      this->hMainWindow = hMainWindow;
      awin = hMainWindow;
	  this->we_vorhanden = we_vorhanden;
}

BOOL SEARCHA::GetKey (char * key)
{
      int anz;
     
      strcpy (key, " ");
      if (idx == -1) return FALSE;
      anz = wsplit (Key, "|");
      if (anz < 2) return FALSE;
      strcpy (key, clipped (wort[0]));
      return TRUE;
}


void SEARCHA::Search (void)
{
  	  int cx, cy;
	  char buffer [256];

	  idx = -1;
      cx = 52;
      cy = 25;
      Settchar ('|');
      EnableWindow (hMainWindow, FALSE);
      Query = new CHQEX (cx, cy);
      Query->EnableSort (TRUE);
      Query->OpenWindow (hMainInst, hMainWindow);
      sprintf (buffer, "%s %s ",
                          "%d" 
                          "%s");
      Query->RowAttr (buffer);
	  sprintf (buffer, "    %20s", "1"); 
	  Query->VLines (buffer, 3);
      sprintf (buffer, "13;13 25;37");
      Query->RowPos (buffer);
	  EnableWindow (awin, FALSE);

	  sprintf (buffer, "    %20s  %-24s", "Artikel", "Bezeichnung"); 
	  Query->InsertCaption (buffer);

	  Query->SetFillDb (ReadLst);
	  Query->SetSearchLst (SearchLst);
      ReadLst ("");
	  if (syskey == KEY9)
	  {
		Query->SetSortRow (0, TRUE);
	  }
	  else
	  {
		Query->SetSortRow (1, TRUE);
	  }

	  Query->ProcessMessages ();
      EnableWindow (hMainWindow, TRUE);
      if (syskey != KEY5)
      {
              idx = Query->GetSel ();
              if (idx != -1)
              {
                     Query->GetText (Key);
              }
      }
	  EnableWindow (awin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (awin);
}

void SEARCHA::Search (char *stext)
{
  	  int cx, cy;
	  char buffer [256];

	  idx = -1;
      cx = 82;
      cy = 25;
      Settchar ('|');
      EnableWindow (hMainWindow, FALSE);
      Query = new CHQEX (cx, cy);
      Query->EnableSort (TRUE);
      Query->OpenWindow (hMainInst, hMainWindow);
      sprintf (buffer, "%s %s ",
                          "%d" 
                          "%s");
      Query->RowAttr (buffer);
	  sprintf (buffer, "    %20s", "1"); 
	  Query->VLines (buffer, 3);
      sprintf (buffer, "13;13 25;37");
      Query->RowPos (buffer);
	  EnableWindow (awin, FALSE);

	  sprintf (buffer, "    %20s  %-24s", "Artikel", "Bezeichnung"); 
	  Query->InsertCaption (buffer);

	  Query->SetFillDb (ReadLst);
	  Query->SetSearchLst (SearchLst);
      ReadLst ("");
      Query->SetSortRow (1, TRUE);
      if (stext != NULL && stext[0] != 0)
      {
             Query->SetSBuff (stext);
      }
	  Query->ProcessMessages (); 
      EnableWindow (hMainWindow, TRUE);
      if (syskey != KEY5)
      {
              idx = Query->GetSel ();
              if (idx != -1)
              {
                     Query->GetText (Key);
              }
      }
	  EnableWindow (awin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (awin);
}

