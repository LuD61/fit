#ifndef _SPEZDLG_DEF
#define _SPEZDLG_DEF
#include "dlg.h"
#include "itprog.h"
#include "itfont.h"
#include "formfield.h"
#include "listdlg.h"
//#include "mo_txt.h"
#include "Work.h"
#include "cmask.h"

#define MDN_CTL 1802
#define FIL_CTL 1801
#define LFD_CTL 1803

#define KATEGORIE_CTL 1804
#define IDENT_EXTERN_CTL 1805
#define IDENT_INTERN_CTL 1806
#define LIEF_CTL 1807
#define LIEF_RECH_NR_CTL 1808
#define GEBURT_CTL 1809
#define MAST_CTL 1810
#define SCHLACHT_CTL 1811
#define ZERLEGUNG_CTL 1812
#define ES_CTL 1813
#define EZ1_CTL 1814
#define EZ2_CTL 1815
#define EZ3_CTL 1816
#define ANZ_ETI_CTL 1817
#define ARTIKEL_CTL 1818
#define TEXT4_CTL 1819
#define TEXT5_CTL 1820
#define DAT_CTL 1821
#define EU1_CTL 1822
#define EU2_CTL 1823

#define IDM_CHOISE 2002
#define IDM_QUIK   2003

#define HEADCTL 700
#define POSCTL 701

#define RAISEDBORDER 0
#define HIGHBORDER 1
#define LOWBORDER 2
#define HIGHCOLBORDER 3
#define LOWCOLBORDER 4
#define LINEBORDER 5



#define IDM_LIST 2001
#define IDM_DOCKLIST 2003
#define IDM_INSERT 2004
#define IDM_DELALL 5000
#define IDM_CHOISEPRINTER 2016

#define ENTERHEAD 0

class SpezDlg : virtual public DLG 
{
          private :
             static mfont *Font;
             static CFIELD *_fHead [];
             static CFIELD *_fPos [];
             static CFORM fHead;
             static CFORM fPos;
             static CFIELD *_fcField [];
             static CFORM fcForm;
             static CFIELD *_fcField0 [];
             static CFORM fcForm0;

             static ItProg *After [];
             static ItProg *Before [];
             static ItFont *itFont [];
             static FORMFIELD *dbfields [];
             static FORMFIELD *dbheadfields [];
             static char *EnableHead[];
             static char *EnableHeadMdn[];
             static char *EnableHead2[];
             static char *EnableNachdruck[];
             static char *EnableHeadFil[];
             static char *EnableDatum[];
             static char *EnablePos [];
 			 static BitImage ImageDelete;
			 static BitImage ImageInsert;
             int    BorderType;
             static Work work;
             static BOOL WriteOK;
             static char *HelpName;
             static BOOL NewRec;
             static HBITMAP ListBitmap;
             static HBITMAP ListMask;
             static HBITMAP ListBitmapi;
			 static BitImage ImageList;
			 static COLORREF Colors[];
			 static COLORREF BkColors[];
             CFORM *Toolbar2;
             static char **ComboStaat1;
             static char **ComboStaat2;

             static SpezDlg *ActiveSpezDlg;
             static int pos;
             static char ptwert[];
             LISTDLG *ListDlg;
             COLORREF ListColor;
             COLORREF ListBkColor;
             BOOL ColorSet;
             int ListLines;
             BOOL LineSet;
             HWND hwndCombo1;
             HWND hwndCombo2;
             char **Combo1;
             char **Combo2;
             HWND *hwndList;
             BOOL DockList;
             BOOL DockMode;

          public :

            static BOOL ListButtons;
            static int ButtonSize;
            static HBITMAP SelBmp;
            static int MeKzDefault;
            static COLORREF SysBkColor;

            void SethwndList (HWND *hwndList)
            {
                this->hwndList = hwndList;
            }

            HWND *GethwndList (void)
            {
                return hwndList;
            }

            void SetToolbar2 (CFORM *Toolbar2)
            {
                this->Toolbar2 = Toolbar2;
            }

            CFORM *GetToolbar2 (void)
            {
                return Toolbar2;
            }

            void SetDockList (BOOL DockList)
            {
                this->DockList = DockList;
            }

            BOOL GetDockList (void)
            {
                return DockList;
            }

            void SetDockMode (BOOL DockMode)
            {
                this->DockMode = DockMode;
            }

            BOOL GetDockMode (void)
            {
                return DockMode;
            }

 	        SpezDlg (int, int, int, int, char *, int, BOOL);
 	        SpezDlg (int, int, int, int, char *, int, BOOL, int);
 	        SpezDlg (int, int, int, int, char *, int, BOOL, int, int);
 	        void Init (int, int, int, int, char *, int, BOOL);
            BOOL OnKeyDown (void);
            BOOL OnKeyUp (void);
            BOOL OnKey1 (void);
            BOOL OnKey2 (void);
            BOOL OnKey3 (void);
            BOOL OnKey4 (void);
            BOOL OnKey5 (void);
            void SaveRect (void);
            BOOL OnKey6 (void);
            BOOL OnKey6 (short);
            BOOL OnKey7 (void);
            BOOL OnKey8 (void);
            BOOL OnKey9 (void);
            BOOL OnKey10 (void);
            BOOL OnKey12 (void);
            BOOL OnKeyDelete (void);
            BOOL OnKeyInsert (void);
            BOOL OnKeyPrior (void);
            BOOL OnKeyNext (void);
            BOOL OnKeyHome (void);
            BOOL OnKeyEnd (void);
            BOOL OnKeyReturn (void);
            BOOL OnPaint (HWND,HDC,UINT,WPARAM,LPARAM);
            BOOL OnActivate (HWND,UINT,WPARAM,LPARAM);
            BOOL OnCommand (HWND,UINT,WPARAM,LPARAM);
            BOOL OnSysCommand (HWND,UINT,WPARAM,LPARAM);
            BOOL OnDestroy (HWND,UINT,WPARAM,LPARAM);
            BOOL OnMove (HWND,UINT,WPARAM,LPARAM);
            BOOL OnCloseUp (HWND,UINT,WPARAM,LPARAM);
            void PrintComment (char *);
            HWND OpenWindow (HANDLE, HWND);
            HWND OpenScrollWindow (HANDLE, HWND);
            void SetWinBackground (COLORREF);
            void ToClipboard (void);
            void FromClipboard (void);
            void Help (void);
            BOOL ShowMdn (void);
            BOOL ShowFil (void);
            BOOL ShowTierKz (void);
            BOOL ShowLief (void);
            BOOL ShowLief (char *);
            BOOL ShowA (void);
            BOOL ShowA (char *);
            BOOL ShowIdent (void);
            static void ZerldatenToZerldatenf (void);
            static BOOL ShowIdent (char *);
            static BOOL ShowIdent (char *,char *);
            void CallInfo (void);
            void SetListColors (COLORREF, COLORREF);
            static int ReadMdn (void);
            static int ReadFil (void);
            BOOL ShowPtab (char *, char *, char *);
            static int ReadIdent (void);
            static int ReadTier_kz (void);
            static int ReadWe (void);
            static int ReadWeKopf (void);
            void ZeigeListe (void);
            static int ReadLief (void);
            static int ReadA (void);
            static int SetAnz (void);
            static int SetAdr (void);
            static int EnterPos (void);
            static int GetPtab (void);
            HWND SetToolCombo (int, int, int, int);
            void SetComboTxt (HWND, char **, int, int);
			void ChoiseCombo1 (void);
			void ChoiseCombo2 (void);
            void SetListLines (int);
            void ChangeF6 (DWORD);
            void DestroyPos (void);
            void SetPos (void);
            void ShowList (void);
            static void AktiviereZerlege (bool);
            static void AktiviereSchlacht (bool);
			static void WriteParamSchlacht(int);
			static void WriteParamZerleg(LSTS,bool);
			static void WriteZerlegedaten(LSTS);
            void ProcessMessages (void);
			void Schreibe (bool);
			void SchreibeLief (void);
			void BucheBsd (char *, int);
};
#endif
