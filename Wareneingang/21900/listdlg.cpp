//190407 
//040407  zerldaten auch schreiben , wenn drucken auf "N" steht 
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include <string.h>
#include "comcthlp.h"
#include "itemc.h"
#include "wmaskc.h"
#include "mo_meld.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_intp.h"
#include "listdlg.h"
#include "inflib.h"
#include "spezdlg.h"
#include "enterfunc.h"
#include "ListDlg.h"
#include "dlg.h"
#include "searcha.h"

#define IDM_CANCEL 3001
#ifndef IDM_WRITE
#define IDM_WRITE  3002
#endif

static int StdSize = STDSIZE;

static int dlgsize    = STDSIZE;
static int ltgraysize = STDSIZE;
static int dlgpossize = STDSIZE;
static int cpsize     = 200;


bool druckejetzt = FALSE;
static mfont dlgfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         GRAYCOL,
                         0,
                         NULL};

struct LSTS  LISTDLG::Lsts;
struct LSTS *LISTDLG::LstTab;


CFIELD *LISTDLG::_fChoise0 [] = {
    new CFIELD ("cancel", " abbrechen ", 10, 0, 0, 0, NULL, "",
                CBUTTON, 
                IDM_CANCEL, &dlgfont, 0, 0),
    new CFIELD ("write", " speichern ", 10, 0,12, 0, NULL, "",
                CBUTTON, 
                IDM_WRITE, &dlgfont, 0, 0),
    new CFIELD ("show",  "  ansehen  ", 10, 0,24, 0, NULL, "",
                CBUTTON, 
                IDM_CHOISE, &dlgfont, 0, 0),
    new CFIELD ("delete","  l�schen  ", 10, 0,36, 0, NULL, "",
                CBUTTON, 
                IDM_DELETE, &dlgfont, 0, 0),
    new CFIELD ("insert"," einf�gen  ", 10, 0,48, 0, NULL, "",
                CBUTTON, 
                IDM_INSERT, &dlgfont, 0, 0),
    NULL,
};

CFORM LISTDLG::fChoise0 (5, _fChoise0);

CFIELD *LISTDLG::_fChoise [] = {
    new CFIELD ("fChoise0", (char *) &fChoise0, 0, 0, -1 , -2, NULL, "",
                CFFORM, 
                500, &dlgfont, 0, 0),
    NULL,
};

CFORM LISTDLG::fChoise (1, _fChoise);
//bool dInsert = FALSE;


ITEM LISTDLG::ufeld1              ("feld1",              "Material",         "", 0);
ITEM LISTDLG::ufeld2              ("feld2",              "Anzahl",         "", 0);
ITEM LISTDLG::ufeld3              ("feld3",              "Auswahl",         "", 0);
ITEM LISTDLG::ufeld4              ("feld4",              "Archiv",         "", 0);
ITEM LISTDLG::ufeld5              ("feld5",              "Spalte 5",         "", 0);
ITEM LISTDLG::ufiller         ("",               " ",               "",0);

ITEM LISTDLG::iline ("", "1", "", 0);

ITEM LISTDLG::ifeld1          ("Bezeichnung",          Lsts.a_bez,        "", 0);
ITEM LISTDLG::ifeld2          ("Anzahl",          Lsts.anzahl,        "", 0);
ITEM LISTDLG::ifeld3          ("mebuch",          (char *) &Lsts.me_buch,        "", 0);
ITEM LISTDLG::ifeld4          ("aktiv",          Lsts.aktiv,        "", 0);
//ITEM LISTDLG::ifeld5          ("feld5",          Lsts.feld5,        "", 0);
ITEM LISTDLG::ichoise         ("choise",        (char *) Lsts.choise,  "", 0);

field LISTDLG::_UbForm [] = {
&ufeld1,   26, 0, 0,  6,  NULL, "", BUTTON, 0, 0, 0,     
&ufeld2,   8, 0, 0, 32,  NULL, "", BUTTON, 0, 0, 0,     
&ufeld3,   10, 0, 0, 40,  NULL, "", BUTTON, 0, 0, 0,     
&ufeld4,   10, 0, 0, 50,  NULL, "", BUTTON, 0, 0, 0,     
//&ufeld5,   12, 0, 0, 54,  NULL, "", BUTTON, 0, 0, 0,     
&ufiller, 110, 0, 0, 50,  NULL, "", BUTTON, 0, 0, 0,
};


form LISTDLG::UbForm = {4, 0, 0, _UbForm, 0, 0, 0, 0, NULL};

field LISTDLG::_LineForm [] = {
&iline,   1, 0, 0, 32,  NULL, "",  DISPLAYONLY, 0, 0, 0, 
&iline,   1, 0, 0, 40,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0, 50,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0, 60,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
};

form LISTDLG::LineForm = {4, 0, 0, _LineForm,0, 0, 0, 0, NULL};

field LISTDLG::_DataForm [] = {
&ifeld1,     25, 0, 0, 7,  NULL, "",     DISPLAYONLY,      0, 0, 0,     
//&ichoise,    2, 1, 0,15,  NULL, "",     BUTTON,    0, 0, 0,
&ifeld2,      6, 0, 0,33,  NULL, "%d",     DISPLAYONLY,      0, 0, 0,     
&ifeld3,    3, 1, 0,41,  NULL, "",     EDIT,      0, 0, 0,     
&ifeld4,    3, 0, 0,51,  NULL, "",     EDIT,      0, 0, 0,     
//&ifeld5,    10, 0, 0,55,  NULL, "",     EDIT,      0, 0, 0,     
};

form LISTDLG::DataForm = {4, 0, 0, _DataForm,0, 0, 0, 0, NULL};


int LISTDLG::ubrows [] = {0, 
                          1,
                          2,
                          3, 
                          4, 
                         -1,
};


struct CHATTR LISTDLG::ChAttra [] = {"Bezeichnung",     DISPLAYONLY, EDIT,
                                      "Anzahl",     EDIT,EDIT,                
                                      "mebuch",     EDIT,EDIT,                
                                      "aktiv",     EDIT,EDIT,                
                                      NULL,  0,           0};

struct CHATTR *LISTDLG::ChAttr = ChAttra;


BOOL LISTDLG::CallInfo (HWND hWnd, RECT *rect, char *Item, char *ItemValue, DWORD WinMode)
/**
Einstieg in Infosystem mit Artikel. Abgeleitet von Klasse Info.
**/
{
 		 char buffer [1024];
         BOOL ret;

         sprintf (buffer, "where a = 2");
         ret = Info::CallInfo (hWnd, rect, "a", buffer, WinMode); 
         SetListFocus ();
         return ret;
}


int LISTDLG::InfoProc (char **Item, char *Value, char *where)
/**
Spezielle Procedure fuer Info-System. Alter Ablauf, ist nicht mehr Aktiv 
**/
{

   *Item = "a";

   sprintf (where, "where a = 2");
   return 1;
}

void LISTDLG::SetButtonSize (int Size)
{
    int fpos;

    return;
    fpos = GetItemPos (&DataForm, "choise");
    if (fpos < 0) return;

    if (Size == SMALL)
    {
         DataForm.mask [fpos].length = 2;
         DataForm.mask [fpos].rows = 1;
    }
    else 
    {
         DataForm.mask [fpos].length = 3;
         DataForm.mask [fpos].rows = 0;
    }
}


void LISTDLG::GetCfgValues (void)
{
       char cfg_v [512];
       COLORREF Color;

       if (ProgCfg == NULL) return;
	   if (cfgOK) return;

       cfgOK = TRUE;
        if (ProgCfg->GetCfgValue ("Wareneingang", cfg_v) == TRUE)
		{
			        flg_we = atoi (cfg_v);
		}
       if (ProgCfg->GetCfgValue ("3DList", cfg_v) == TRUE)
       {
                    eListe.Set3D (atoi (cfg_v));
	   }
       if (ProgCfg->GetCfgValue ("3DSize", cfg_v) == TRUE)
       {
                    eListe.SetPlus3D (atoi (cfg_v));
	   }
       if (ProgCfg->GetCfgValue ("ListFocus", cfg_v) == TRUE)
       {
                    ListFocus = atoi (cfg_v);
	   }
        if (ProgCfg->GetCfgValue ("rowheight", cfg_v) == TRUE)
		{
			        RowHeight = ratod (cfg_v);
					if (RowHeight < (double) 1.0)
					{
						RowHeight = (double) 1.0;
					}
		}
        if (ProgCfg->GetCfgValue ("ubheight", cfg_v) == TRUE)
		{
			        UbHeight = atoi (cfg_v);
					SetUbHeight ();
		}
		eListe.SetRowHeight (RowHeight);

/*
        if (ProgCfg->GetCfgValue ("NoRecNr", cfg_v) == TRUE)
        {
			        NoRecNr = atoi (cfg_v);
		            if (atoi (cfg_v))
					{
	                        SetNoRecNr ();
					}
		}
		*/
        NoRecNr = 1;
        SetNoRecNr ();


        if (ProgCfg->GetCfgValue ("ListColor", cfg_v) == TRUE)
        {
                     GetListColor (&Color, cfg_v);
		             eListe.SetColor (Color);
        }
        if (ProgCfg->GetCfgValue ("ListBkColor", cfg_v) == TRUE)
        {
                     GetListColor (&Color, cfg_v);
		             eListe.SetBkColor (Color);
        }
        if (ProgCfg->GetCfgValue ("ListLine", cfg_v) == TRUE)
        {
		             eListe.SetLines (GetListLine (cfg_v));
        }
}


void LISTDLG::TestRemoves (void)
{
        int fpos;

        fpos = GetItemPos (&DataForm, "feld3");
        if (fpos >= 0)
        {
            if (DataForm.mask[fpos].attribut & REMOVED)
            {
                DestroyListField ("feld3");
            }
        }
}


LISTDLG::LISTDLG () : KEYS (), LISTENTER () 
{

    ProgCfg = new PROG_CFG ("21900");
	Dlg = NULL;
    cfgOK = FALSE;
	cursor = -1;
    LstTab = new LSTS [0x10000];

    dataform  = &DataForm;
    ubform    = &UbForm;
    lineform  = &LineForm;
    eListe.SetUbRows (ubrows); 
	eListe.SetKeys (this);

    LISTENTER::GetCfgValues ();
    GetCfgValues ();
//    GetSysPar ();
    TestRemoves ();
    if (NoRecNr)
    {
	    SetNoRecNr ();
    }
	fChoise.Setchpos (0, 2);
    work = NULL;
    ChAttr = ChAttra;
    eListe.SetChAttr (ChAttr); 
    Toolbar2 = NULL;
    eListe.SetInfoProc (InfoProc);
    eListe.SetInfo (this);
    ArrDown = NULL;
    ArrDown = LoadBitmap (DLG::hInstance, "ARRDOWN");
}


LISTDLG::~LISTDLG ()
{
    int i, anz;
	if (work != NULL)
	{
          work->SetListDlg (NULL);
	}
    fChoise.destroy ();
	if (LstTab != NULL)
	{

        anz =   eListe.GetRecanz ()	;
        for (i = 0; i < anz; i ++)
        {
            if (LstTab[i].choise[0] != NULL)
            {
                delete LstTab[i].choise[0];
            }
        }

  	    delete LstTab;
		LstTab = NULL;
	}
    if (ProgCfg != NULL)
    {
        ProgCfg->CloseCfg ();
        delete ProgCfg;
        ProgCfg = NULL;
    }
}


// Funktionen f�r den Zugriff auf die Daten. Sind als abstrakte Schnittstellen in
// listenter definiert.

unsigned char *LISTDLG::GetDataRec (void)
{
    return (unsigned char *) &Lsts;
}

int LISTDLG::GetDataRecLen (void)
{
    return sizeof (struct LSTS);
}

char *LISTDLG::GetListTab (int i)
{
    return (char *) &LstTab[i];
}

// Ende der Schnittstellen.


void LISTDLG::Setfeld2Removed (BOOL removed)
{
	return;
    if (removed)
    {
            SetFieldAttr ("Bezeichnung", DISPLAYONLY);
    }
    else
    {
            SetFieldAttr ("Bezeichnung", EDIT);
    }
}


void LISTDLG::Prepare (void)
{
	Prepare();
}

void LISTDLG::Open (void)
{
	if (cursor == -1)
	{
		Prepare ();
		if (cursor == -1) return;
	}
}

int LISTDLG::Fetch (void)
{
	if (cursor == -1)
	{
		Prepare ();
		if (cursor == -1) return 100;
	}
	return 100;
}

void LISTDLG::Close (void)
{
	if (cursor != -1)
	{
		cursor = -1;
	}
}


BOOL LISTDLG::AfterCol (char *colname)
{
	char cmat[30];
         if (strcmp (colname, "Bezeichnung") == 0)// && dInsert == TRUE)
         {
			 if (syskey == KEYCR)
			 {
		         if (!numeric (Lsts.a_bez))
				 {
					if (work->TestABez(Lsts.a_bez) == 0.0) ShowA (Lsts.a_bez);
				 }
				 else
				 {
					 strcpy(cmat,Lsts.a_bez);
					if (work->ReadA (ratod(Lsts.a_bez), Lsts.a_bez, "")) return TRUE;
					strcpy(Lsts.mat,clipped(cmat));
			         memcpy (&LstTab [eListe.GetAktRow ()], &Lsts, sizeof (struct LSTS));
					work->SetLsts (&Lsts);


				 }
			 }

	         if (strcmp (clipped (Lsts.a_bez), " ") <= 0)
			 {
				return OnKeyDel ();
			 }
             return FALSE;
         }
         else if (strcmp (colname, "mebuch") == 0)
         {
			 if (strcmp (clipped(Lsts.me_buch),"j") == 0)  strcpy (Lsts.me_buch, "J"); 
			 if (strcmp (clipped(Lsts.me_buch),"1") == 0)  strcpy (Lsts.me_buch, "J"); 

			 if (strcmp (clipped(Lsts.me_buch),"J") != 0)  strcpy (Lsts.me_buch, ""); 
             memcpy (&LstTab[eListe.GetAktRow ()], &Lsts, sizeof (Lsts));
         }
         else if (strcmp (colname, "aktiv") == 0)
         {
			 if (strcmp (clipped(Lsts.aktiv),"x") == 0)  strcpy (Lsts.aktiv, "X"); 
			 if (strcmp (clipped(Lsts.aktiv),"a") == 0)  strcpy (Lsts.aktiv, "A"); 

			 if (strcmp (clipped(Lsts.aktiv),"X") != 0 && strcmp (clipped(Lsts.aktiv),"A") != 0) 
			 {
				 if (strcmp (clipped(Lsts.aktiv),"") != 0)
				 {
	 				disp_mess("mit X archivieren, mit A wieder aktivieren",0);
					strcpy (Lsts.aktiv, ""); 
				 }
			 }
             memcpy (&LstTab[eListe.GetAktRow ()], &Lsts, sizeof (Lsts));
         }


         return FALSE;
}

int LISTDLG::AfterRow (int Row)
{
//         form * DataForm; 

         if (syskey == KEYUP && strcmp (clipped (Lsts.a_bez), " ") <= 0)
         {
                return OnKeyDel ();

         }
         if (syskey == KEYCR && strcmp (clipped (Lsts.a_bez), " ") <= 0)
         {
                return OnKeyDel ();

         }
         memcpy (&LstTab [Row], &Lsts, sizeof (struct LSTS));
         work->SetLsts (&Lsts);
         return 1;
}

int LISTDLG::BeforeRow (int Row)
{
         memcpy (&LstTab [Row], &Lsts, sizeof (struct LSTS));
		 if (strlen(Lsts.a_bez) > 0) 
		 {
			 Setfeld2Removed(TRUE);
		 }
		 else
		 {
			 Setfeld2Removed(FALSE);
		 }

         return 1;
}

/*
int LISTDLG::ShowA ()
{

        struct SA *sa;

        if (Modal)
        {  
               SearchA.SetWindowStyle (WS_POPUP | 
                                       WS_THICKFRAME |
                                       WS_CAPTION |
                                       WS_SYSMENU |
                                       WS_MINIMIZEBOX |
                                       WS_MAXIMIZEBOX);
               SearchA.SetParams (DLG::hInstance, hMainWindow);
        }
        else
        {  
               SearchA.SetWindowStyle (WS_POPUP | WS_THICKFRAME);
               SearchA.SetParams (DLG::hInstance, hMainWindow);
        }
        SearchA.Setawin (eListe.GethMainWindow ());
        SearchA.SearchA ();
        if (Modal)
        {  7
               EnableWindows (GetParent (eListe.GethMainWindow ()), FALSE);
        }
        if (syskey == KEY5)
        {
            SetListFocus ();
            return 0;
        }
        sa = SearchA.GetSa ();
        if (sa == NULL)
        {
            return 0;
        }

        strcpy (LstTab [eListe.GetAktRow ()].a, sa->a);
        eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
        eListe.ShowAktRow ();
        SetListFocus ();
        return 0;
}
*/

BOOL LISTDLG::OnKey5 (void)
{
/*
    if (abfragejn (hMainWindow, "Positionen speichern ?", "J"))
    {
        return OnKey12 ();
    }
*/
	eListe.BreakList ();
	return TRUE;
}

BOOL LISTDLG::OnKey6 (void)
{
//    eListe.InsertLine ();

    if (Dlg != NULL)
    {
        ((SpezDlg *) Dlg)->OnKey6 (1);
        SetAktFocus ();
    }
    return TRUE;
}

BOOL LISTDLG::OnKeyInsert (void)
{
//    eListe.InsertLine ();
    return TRUE;
}

BOOL LISTDLG::OnKey7 (void)
{
//    eListe.DeleteLine ();
//	  dInsert = TRUE;
	    eListe.InsertLine ();
    return TRUE;
}

BOOL LISTDLG::OnKeyDel (void)
{
    eListe.DeleteLine ();
    return TRUE;
}
		
BOOL LISTDLG::OnKey8 (void)
{
         return FALSE;
}


BOOL LISTDLG::OnKey9 (void)
{
    char * Item = eListe.GetAktItem ();
    if (strcmp (Item, "Bezeichnung") == 0)
    {
		ShowA (Lsts.a_bez);

        return TRUE;
    }
    return FALSE;
}


BOOL LISTDLG::OnKey10 (void)
{
    if (Dlg != NULL)
    {
         work->SetLsts (&Lsts);
// Hier Workfunktion einsetzen
         SetListFocus ();
         return TRUE;
	}
    return FALSE;
}

BOOL LISTDLG::OnKey11 (void)
{

    return FALSE;
}


int LISTDLG::ToMemory (int i)
{

    LISTENTER::ToMemory (i);
    return 0;
}


void LISTDLG::InitRow (void)
{

    LISTENTER::InitRow ();


    work->SetLsts (&Lsts);

    if (ArrDown != NULL && Lsts.choise[0] != NULL)
    {
        Lsts.choise[0]->SetBitMap (ArrDown);
    }

}

void LISTDLG::uebertragen (void)
{
    work->SetLsts (&Lsts);
    work->FillRow ();
    if (ArrDown != NULL && Lsts.choise[0] != NULL)
    {
        Lsts.choise[0]->SetBitMap (ArrDown);
    }
}

void LISTDLG::WriteRec (int i)
{
    memcpy (&Lsts, &LstTab [i], sizeof (struct LSTS));
	if (strlen(Lsts.a_bez) < 1) return;

	int anzdrk = work->GetAnzahl();
	if (anzdrk == 0) anzdrk = 1;

	zerm.a_z_f = atoi(Lsts.anzahl) / anzdrk;
	zerm.mdn = zerldaten.mdn;
//	zerm.schnitt = atoi(Lsts.schnitt);
	zerm.zerm_mat = atoi(Lsts.mat);
	zerm.zerm_aus_mat = (long) zerldaten.a_gt;
	if (zerm.zerm_mat > 0)
	{
		beginwork();
		Zerm.dbupdate ();
		zerldaten.lfd = atoi(Lsts.lfd);
		zerldaten.aktiv = 1;
		if (strcmp (clipped(Lsts.aktiv),"X") == 0)
		{
			zerldaten.aktiv = 0;
			Zerldaten.dbupdate_lfd();
		}
		if (strcmp (clipped(Lsts.aktiv),"A") == 0)
		{
			zerldaten.aktiv = 1;
			Zerldaten.dbupdate_lfd();
		}
		commitwork();
	}

	commitwork();

}

BOOL LISTDLG::OnKey12 (void)
{
    int i;
    int recanz;
//    int dsqlstatus;

    if (AfterRow (eListe.GetAktRow ()) == -1)
    {
        return TRUE;
    }
	if (strcmp (clipped (Lsts.a_bez), " ") <= 0)
    {
        OnKeyDel ();
    }
    else
    {
    }

    recanz = eListe.GetRecanz ();

    work->DeleteAllZerm ();
    for (i = 0; i < recanz; i ++)
    {
        WriteRec (i);
    }
	eListe.BreakList ();
    return TRUE;
}


BOOL LISTDLG::OnKeyUp (void)
{
//    form *DataForm;
//    int fpos;
	if (strcmp (clipped (Lsts.a_bez), " ") <= 0)
    {
        return OnKeyDel ();
    }

    return FALSE;
}

BOOL LISTDLG::OnKeyDown (void)
{
//    form *DataForm;
//    int fpos;

	if (strcmp (clipped (Lsts.a_bez), " ") <= 0)
    {
        return TRUE;
    }

    return FALSE;
}


void LISTDLG::CreateListQuikInfos (HWND hWnd)
{
       if (Toolbar2 == NULL) return;

       Toolbar2->GetCfield ("f5")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("f12")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("f9")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("del")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("insert")->CreateQuikInfos (hWnd);
}


BOOL LISTDLG::OnCommand (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    if (LOWORD (wParam) == IDM_CHOISE)
    {
        return OnKey9 ();
    }
    if (LOWORD (wParam) == IDM_CANCEL)
    {
        return OnKey5 ();
    }
    if (LOWORD (wParam) == IDM_WRITE)
    {
        return OnKey12 ();
    }
    if (LOWORD (wParam) == VK_DELETE)
    {
        return OnKeyDel ();
    }
    if (LOWORD (wParam) == IDM_DELETE)
    {
        return OnKeyDel ();
    }
    if (LOWORD (wParam) == VK_INSERT)
    {
        return OnKeyInsert ();
    }
    if (LOWORD (wParam) == IDM_INSERT)
    {
        return OnKey6 ();
    }
    if (HIWORD (wParam)  == CBN_CLOSEUP)
	{
       if (Dlg != NULL)
	   {
	           return ((SpezDlg *) Dlg)->OnCloseUp (hWnd, msg, wParam, lParam);
	   }
	}
    return FALSE;
}

BOOL LISTDLG::OnPaint (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    HDC hdc;

    if (Modal == FALSE) return FALSE;
    if (this->mamain1 != hWnd) return FALSE; 
    
    hdc = BeginPaint (hWnd, &ps);
    fChoise.display (hWnd, hdc);
    EndPaint (hWnd,&ps);
    return FALSE;
}

BOOL LISTDLG::OnMove (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{

    if (Modal == FALSE) return FALSE;
    if (this->mamain1 != hWnd) return FALSE; 

    fChoise.MoveWindow ();
    return FALSE;
}

BOOL LISTDLG::SchreibeZerlege()
{
	int i;
	int danz ;
	int danzAusw;
	char text [40];

	//LOH-34
    if (strlen (clipped(zerldatenf.tier_kz)) == 0)  
	{
			disp_mess("Es kann nicht gedruckt werden, da keine Kategorie vorhanden ist",2);
			return FALSE;
	}
	work->GetPtab (zerldatenf.tier_kz_krz, "tier_kz", zerldatenf.tier_kz); //LOH-34
    if (strlen (clipped(zerldatenf.tier_kz_krz)) == 0)  
	{
			disp_mess("Es kann nicht gedruckt werden, da keine Kategorie(Bez) vorhanden ist",2);
			return FALSE;
	}
    if (Dlg != NULL)
    {
		/* Ausnahme RFE_Lohmann ergaenzt. GK 25.06.2013 */ 
	    if ( !RFE_Lohmann && strcmp (clipped(zerldatenf.ident_intern),clipped(zerldatenf.ident_extern)) == 0)  
		{
			disp_mess("Keine interne Identnummer!",2);
			return TRUE;
		}

	    int recanz = eListe.GetRecanz ();
		danz = danzAusw = 0;

/** LOH-3 nicht n�tig
		sprintf(text,"Zerlegedaten jetzt schreiben ?");
		if (! abfragejn (hMainWindow, text, "J"))
		{
			work->SetComboGelesen(FALSE);
			work->SchreibeLief ();
//   			eListe.BreakList ();
			return FALSE;
		}
****/
		for (i = 0; i < recanz; i ++)
	    {
//180407			if (atoi(LstTab[i].anzahl) > 0)
//180407			{
				((SpezDlg *) Dlg)->WriteZerlegedaten (LstTab[i]);
				danz+= atoi(LstTab[i].anzahl);
//				if (LstTab[i].mebuch.IsChecked ())
				if (strcmp (clipped(LstTab[i].me_buch),"J") == 0)
				{
					danzAusw+= atoi(LstTab[i].anzahl);
				}
//180407			}
	    }

     druckejetzt = FALSE;
	 if (danzAusw == 0) danzAusw = danz;

	sprintf(text,"Jetzt %d Etiketten drucken ?",danzAusw);
    if (abfragejn (hMainWindow, text, "J"))
    {
		druckejetzt = TRUE;
	}
		((SpezDlg *) Dlg)->WriteParamSchlacht (0);
		for (i = 0; i < recanz; i ++)
	    {
		    work->SetLsts (&LstTab[i]);
//210307 auch bei anzahl = 0!!			if (atoi(LstTab[i].anzahl) > 0)
//			{
//				if (LstTab[i].mebuch.IsChecked () || danz == danzAusw)
				if (strcmp (clipped(LstTab[i].me_buch),"J") == 0 || danz == danzAusw)
				{
					((SpezDlg *) Dlg)->WriteParamZerleg (LstTab[i],druckejetzt);
				}


//			}
	    }
		work->SetComboGelesen(FALSE);
		work->SchreibeLief ();
   	    eListe.BreakList ();
	}
  return TRUE;
}



BOOL LISTDLG::ShowA (char *stext)
{
             char artikel [14];
             char tier_kz [14];
             SEARCHA SearchA;
 
//             fHead.SetCurrentName ("a_gt");
             SearchA.SetParams (DLG::hInstance, hMainWindow,FALSE); 
             SearchA.Setawin (hMainWindow);
                                       
             clipped (stext);
             SearchA.Search (stext);
             if (SearchA.GetKey (artikel) == TRUE)
             {
				 if (syskey == KEY5)
				 {
				 }
				 else
				 {
					work->ReadA (ratod(artikel), Lsts.a_bez, tier_kz);
					strcpy(Lsts.mat,clipped(artikel));
			         memcpy (&LstTab [eListe.GetAktRow ()], &Lsts, sizeof (struct LSTS));
					work->SetLsts (&Lsts);

				 }
                 return TRUE;
             }
             return FALSE;
}




