#ifndef _SEARCHZERLDATEN_DEF
#define _SEARCHZERLDATEN_DEF
#include "wmaskc.h"
#include "cmask.h"
#include "mo_chqex.h"
#include "dbclass.h"

class SEARCHZERLDATEN
{
       private :
           static HINSTANCE hMainInst;
           static HWND hMainWindow;
           static DB_CLASS DbClass; 
           static HWND awin;
           static int idx;
           static long anz;
           static CHQEX *Query;
           static short mdn_nr;

           int SearchPos;
           int OKPos;
           int CAPos;
           char Key [512];

        public :
           SEARCHZERLDATEN ()
           {
                  SearchPos = 8;
                  OKPos = 9;
                  CAPos = 10;
           }

           ~SEARCHZERLDATEN ()
           {
                   if (Query)
                   {
                       delete Query;
                       Query = NULL;
                   }
           }

           void Setawin (HWND awin)
           {
               this->awin = awin;
           }

           static int SearchLst (char *);
           static int ReadLst (char *);
           BOOL GetKey (char *);
           void SetParams (HINSTANCE, HWND);
           void Search (void);
           void Search (char *);
           void Search (char *, char *);
           void Search (char *, char *, int);
};  
#endif