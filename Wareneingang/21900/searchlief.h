#ifndef _SEARCHLIEF_DEF
#define _SEARCHLIEF_DEF
#include "wmaskc.h"
#include "cmask.h"
#include "mo_chqex.h"
#include "dbclass.h"

class SEARCHLIEF
{
       private :
           static HINSTANCE hMainInst;
           static HWND hMainWindow;
           static DB_CLASS DbClass; 
           static HWND awin;
           static int idx;
           static long anz;
           static CHQEX *Query;
           static short mdn_nr;
		   static long liefvon;
		   static long liefbis;

           int SearchPos;
           int OKPos;
           int CAPos;
           char Key [512];

        public :
           SEARCHLIEF ()
           {
                  SearchPos = 8;
                  OKPos = 9;
                  CAPos = 10;
				  liefvon = 1;
				  liefbis = 99999999;
           }

           ~SEARCHLIEF ()
           {
                   if (Query)
                   {
                       delete Query;
                       Query = NULL;
                   }
           }

           void Setawin (HWND awin)
           {
               this->awin = awin;
           }
           void SetLiefVon (long liefvon)
           {
               this->liefvon = liefvon;
           }
           void SetLiefBis (long liefbis)
           {
               this->liefbis = liefbis;
           }

           static int SearchLst (char *);
           static int ReadLst (char *);
           BOOL GetKey (char *);
           void SetParams (HINSTANCE, HWND);
           void Search (void);
           void Search (char *);
};  
#endif