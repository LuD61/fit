#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "dbclass.h"
#include "preise_we.h"

struct PREISE_WE preise_we, preise_we_null;

void PREISE_WE_CL::prepare (void)
{
            char *sqltext;

            sqlin ((short *)   &preise_we.mdn,  SQLSHORT, 0);
            sqlin ((double *)   &preise_we.a,  SQLDOUBLE, 0);
            sqlin ((char *)   &preise_we.lief,  SQLCHAR, 16);
            sqlin ((char *)   &preise_we.lief_rech_nr,  SQLCHAR, 16);
            sqlin ((long *)   &preise_we.best_blg,  SQLLONG, 0);
    out_quest ((char *) &preise_we.mdn,1,0);
    out_quest ((char *) &preise_we.a,3,0);
    out_quest ((char *) preise_we.lief,0,17);
    out_quest ((char *) &preise_we.best_blg,2,0);
    out_quest ((char *) &preise_we.best_term,2,0);
    out_quest ((char *) &preise_we.pr_ek_b,3,0);
    out_quest ((char *) &preise_we.me_einh_b,1,0);
    out_quest ((char *) preise_we.lief_rech_nr,0,17);
    out_quest ((char *) &preise_we.we_dat,2,0);
    out_quest ((char *) &preise_we.pr_ek_w,3,0);
    out_quest ((char *) &preise_we.me_einh_w,1,0);
            cursor = sqlcursor ("select preise_we.mdn,  "
"preise_we.a,  preise_we.lief,  preise_we.best_blg,  "
"preise_we.best_term,  preise_we.pr_ek_b,  preise_we.me_einh_b,  "
"preise_we.lief_rech_nr,  preise_we.we_dat,  preise_we.pr_ek_w,  "
"preise_we.me_einh_w from preise_we "

#line 22 "preise_we.rpp"
                                  "where mdn = ? and a = ? and lief = ? and (lief_rech_nr = ? or (best_blg = ? and best_blg <> 0)) ");
    ins_quest ((char *) &preise_we.mdn,1,0);
    ins_quest ((char *) &preise_we.a,3,0);
    ins_quest ((char *) preise_we.lief,0,17);
    ins_quest ((char *) &preise_we.best_blg,2,0);
    ins_quest ((char *) &preise_we.best_term,2,0);
    ins_quest ((char *) &preise_we.pr_ek_b,3,0);
    ins_quest ((char *) &preise_we.me_einh_b,1,0);
    ins_quest ((char *) preise_we.lief_rech_nr,0,17);
    ins_quest ((char *) &preise_we.we_dat,2,0);
    ins_quest ((char *) &preise_we.pr_ek_w,3,0);
    ins_quest ((char *) &preise_we.me_einh_w,1,0);
            sqltext = "update preise_we set "
"preise_we.mdn = ?,  preise_we.a = ?,  preise_we.lief = ?,  "
"preise_we.best_blg = ?,  preise_we.best_term = ?,  "
"preise_we.pr_ek_b = ?,  preise_we.me_einh_b = ?,  "
"preise_we.lief_rech_nr = ?,  preise_we.we_dat = ?,  "
"preise_we.pr_ek_w = ?,  preise_we.me_einh_w = ? "

#line 24 "preise_we.rpp"
                                  "where mdn = ? and a = ? and lief = ? and (lief_rech_nr = ? or (best_blg = ? and best_blg <> 0)) ";
            sqlin ((short *)   &preise_we.mdn,  SQLSHORT, 0);
            sqlin ((double *)   &preise_we.a,  SQLDOUBLE, 0);
            sqlin ((char *)   &preise_we.lief,  SQLCHAR, 16);
            sqlin ((char *)   &preise_we.lief_rech_nr,  SQLCHAR, 16);
            sqlin ((long *)   &preise_we.best_blg,  SQLLONG, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &preise_we.mdn,  SQLSHORT, 0);
            sqlin ((double *)   &preise_we.a,  SQLDOUBLE, 0);
            sqlin ((char *)   &preise_we.lief,  SQLCHAR, 16);
            sqlin ((char *)   &preise_we.lief_rech_nr,  SQLCHAR, 16);
            sqlin ((long *)   &preise_we.best_blg,  SQLLONG, 0);
            test_upd_cursor = sqlcursor ("select a from preise_we "
                                  "where mdn = ? and a = ? and lief = ? and (lief_rech_nr = ? or (best_blg = ? and best_blg <> 0)) ");
            sqlin ((short *)   &preise_we.mdn,  SQLSHORT, 0);
            sqlin ((double *)   &preise_we.a,  SQLDOUBLE, 0);
            sqlin ((char *)   &preise_we.lief,  SQLCHAR, 16);
            sqlin ((char *)   &preise_we.lief_rech_nr,  SQLCHAR, 16);
            sqlin ((long *)   &preise_we.best_blg,  SQLLONG, 0);
            del_cursor = sqlcursor ("delete from preise_we "
                                  "where mdn = ? and a = ? and lief = ? and (lief_rech_nr = ? or (best_blg = ? and best_blg <> 0)) ");
    ins_quest ((char *) &preise_we.mdn,1,0);
    ins_quest ((char *) &preise_we.a,3,0);
    ins_quest ((char *) preise_we.lief,0,17);
    ins_quest ((char *) &preise_we.best_blg,2,0);
    ins_quest ((char *) &preise_we.best_term,2,0);
    ins_quest ((char *) &preise_we.pr_ek_b,3,0);
    ins_quest ((char *) &preise_we.me_einh_b,1,0);
    ins_quest ((char *) preise_we.lief_rech_nr,0,17);
    ins_quest ((char *) &preise_we.we_dat,2,0);
    ins_quest ((char *) &preise_we.pr_ek_w,3,0);
    ins_quest ((char *) &preise_we.me_einh_w,1,0);
            ins_cursor = sqlcursor ("insert into preise_we ("
"mdn,  a,  lief,  best_blg,  best_term,  pr_ek_b,  me_einh_b,  lief_rech_nr,  we_dat,  "
"pr_ek_w,  me_einh_w) "

#line 47 "preise_we.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?)");

#line 49 "preise_we.rpp"
}
