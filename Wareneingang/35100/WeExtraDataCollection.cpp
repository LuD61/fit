// WeExtraDataCollection.cpp: Implementierung der Klasse CWeExtraDataCollection.
//
//////////////////////////////////////////////////////////////////////

#include "WeExtraDataCollection.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

CWeExtraDataCollection::CWeExtraDataCollection ()
{

}

CWeExtraDataCollection::~CWeExtraDataCollection ()
{
}


void CWeExtraDataCollection::Destroy ()
{
	if (Collection.Arr != NULL)
	{
		delete Collection.Arr;
		Collection.Arr = NULL;
	}
}


void CWeExtraDataCollection::Add (CWeExtraData Element)
{
	Collection.Add (Element);
}

void CWeExtraDataCollection::Add (CWeExtraData *Element)
{
	Collection.Add (Element);
}

CWeExtraData *CWeExtraDataCollection::Get (int idx)
{
	return Collection.Get (idx);
}

CWeExtraData *CWeExtraDataCollection::Find (int ListPos)
{
	CWeExtraData *d; 
	Collection.pos = 0;
	while ((d = Collection.GetNext ()) != NULL)
	{
		if (d->ListPos == ListPos)
		{
			return d;
		}
	}
	return NULL;
}


void CWeExtraDataCollection::Drop (CWeExtraData Element)
{
	CWeExtraData *d; 
	int idx = 0;
	Collection.pos = 0;
	while ((d = Collection.GetNext ()) != NULL)
	{
		if (d->ListPos == Element.ListPos) break;
		idx ++;
	}
	if (d != NULL)
	{
		Collection.Drop (idx);
	}
}
