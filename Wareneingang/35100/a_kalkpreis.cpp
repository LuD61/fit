#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "wmaskc.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "a_kalkpreis.h"

struct A_KALKPREIS a_kalkpreis, a_kalkpreis_null;

void A_KALKPREIS_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &a_kalkpreis.mdn, 1, 0);
            ins_quest ((char *) &a_kalkpreis.fil, 1, 0);
            ins_quest ((char *) &a_kalkpreis.a, 3, 0);
    out_quest ((char *) &a_kalkpreis.mdn,2,0);
    out_quest ((char *) &a_kalkpreis.fil,1,0);
    out_quest ((char *) &a_kalkpreis.a,3,0);
    out_quest ((char *) &a_kalkpreis.mat_o_b,3,0);
    out_quest ((char *) &a_kalkpreis.hk_vollk,3,0);
    out_quest ((char *) &a_kalkpreis.sk_vollk,3,0);
    out_quest ((char *) &a_kalkpreis.fil_ek_vollk,3,0);
    out_quest ((char *) &a_kalkpreis.fil_vk_vollk,3,0);
    out_quest ((char *) &a_kalkpreis.hk_teilk,3,0);
    out_quest ((char *) &a_kalkpreis.sk_teilk,3,0);
    out_quest ((char *) &a_kalkpreis.fil_ek_teilk,3,0);
    out_quest ((char *) &a_kalkpreis.fil_vk_teilk,3,0);
    out_quest ((char *) &a_kalkpreis.dat,2,0);
    out_quest ((char *) a_kalkpreis.zeit,0,6);
    out_quest ((char *) a_kalkpreis.programm,0,13);
    out_quest ((char *) a_kalkpreis.version,0,13);
            cursor = prepare_sql ("select a_kalkpreis.mdn,  "
"a_kalkpreis.fil,  a_kalkpreis.a,  a_kalkpreis.mat_o_b,  "
"a_kalkpreis.hk_vollk,  a_kalkpreis.sk_vollk,  "
"a_kalkpreis.fil_ek_vollk,  a_kalkpreis.fil_vk_vollk,  "
"a_kalkpreis.hk_teilk,  a_kalkpreis.sk_teilk,  "
"a_kalkpreis.fil_ek_teilk,  a_kalkpreis.fil_vk_teilk,  "
"a_kalkpreis.dat,  a_kalkpreis.zeit,  a_kalkpreis.programm,  "
"a_kalkpreis.version from a_kalkpreis "

#line 22 "a_kalkpreis.rpp"
                                  "where mdn = ? "
                                  "and fil = ? "
                                  "and a = ?");
    ins_quest ((char *) &a_kalkpreis.mdn,2,0);
    ins_quest ((char *) &a_kalkpreis.fil,1,0);
    ins_quest ((char *) &a_kalkpreis.a,3,0);
    ins_quest ((char *) &a_kalkpreis.mat_o_b,3,0);
    ins_quest ((char *) &a_kalkpreis.hk_vollk,3,0);
    ins_quest ((char *) &a_kalkpreis.sk_vollk,3,0);
    ins_quest ((char *) &a_kalkpreis.fil_ek_vollk,3,0);
    ins_quest ((char *) &a_kalkpreis.fil_vk_vollk,3,0);
    ins_quest ((char *) &a_kalkpreis.hk_teilk,3,0);
    ins_quest ((char *) &a_kalkpreis.sk_teilk,3,0);
    ins_quest ((char *) &a_kalkpreis.fil_ek_teilk,3,0);
    ins_quest ((char *) &a_kalkpreis.fil_vk_teilk,3,0);
    ins_quest ((char *) &a_kalkpreis.dat,2,0);
    ins_quest ((char *) a_kalkpreis.zeit,0,6);
    ins_quest ((char *) a_kalkpreis.programm,0,13);
    ins_quest ((char *) a_kalkpreis.version,0,13);
            sqltext = "update a_kalkpreis set "
"a_kalkpreis.mdn = ?,  a_kalkpreis.fil = ?,  a_kalkpreis.a = ?,  "
"a_kalkpreis.mat_o_b = ?,  a_kalkpreis.hk_vollk = ?,  "
"a_kalkpreis.sk_vollk = ?,  a_kalkpreis.fil_ek_vollk = ?,  "
"a_kalkpreis.fil_vk_vollk = ?,  a_kalkpreis.hk_teilk = ?,  "
"a_kalkpreis.sk_teilk = ?,  a_kalkpreis.fil_ek_teilk = ?,  "
"a_kalkpreis.fil_vk_teilk = ?,  a_kalkpreis.dat = ?,  "
"a_kalkpreis.zeit = ?,  a_kalkpreis.programm = ?,  "
"a_kalkpreis.version = ? "

#line 26 "a_kalkpreis.rpp"
                                  "where mdn = ? "
                                  "and fil = ? "
                                  "and a = ?";
            ins_quest ((char *) &a_kalkpreis.mdn, 1, 0);
            ins_quest ((char *) &a_kalkpreis.fil, 1, 0);
            ins_quest ((char *) &a_kalkpreis.a, 3, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &a_kalkpreis.mdn, 1, 0);
            ins_quest ((char *) &a_kalkpreis.fil, 1, 0);
            ins_quest ((char *) &a_kalkpreis.a, 3, 0);
            test_upd_cursor = prepare_sql ("select a from a_kalkpreis "
                                  "where mdn = ? "
                                  "and fil = ? "
                                  "and a = ?");
            ins_quest ((char *) &a_kalkpreis.mdn, 1, 0);
            ins_quest ((char *) &a_kalkpreis.fil, 1, 0);
            ins_quest ((char *) &a_kalkpreis.a, 3, 0);
            del_cursor = prepare_sql ("delete from a_kalkpreis "
                                  "where mdn = ? "
                                  "and fil = ? "
                                  "and a = ?");
    ins_quest ((char *) &a_kalkpreis.mdn,2,0);
    ins_quest ((char *) &a_kalkpreis.fil,1,0);
    ins_quest ((char *) &a_kalkpreis.a,3,0);
    ins_quest ((char *) &a_kalkpreis.mat_o_b,3,0);
    ins_quest ((char *) &a_kalkpreis.hk_vollk,3,0);
    ins_quest ((char *) &a_kalkpreis.sk_vollk,3,0);
    ins_quest ((char *) &a_kalkpreis.fil_ek_vollk,3,0);
    ins_quest ((char *) &a_kalkpreis.fil_vk_vollk,3,0);
    ins_quest ((char *) &a_kalkpreis.hk_teilk,3,0);
    ins_quest ((char *) &a_kalkpreis.sk_teilk,3,0);
    ins_quest ((char *) &a_kalkpreis.fil_ek_teilk,3,0);
    ins_quest ((char *) &a_kalkpreis.fil_vk_teilk,3,0);
    ins_quest ((char *) &a_kalkpreis.dat,2,0);
    ins_quest ((char *) a_kalkpreis.zeit,0,6);
    ins_quest ((char *) a_kalkpreis.programm,0,13);
    ins_quest ((char *) a_kalkpreis.version,0,13);
            ins_cursor = prepare_sql ("insert into a_kalkpreis ("
"mdn,  fil,  a,  mat_o_b,  hk_vollk,  sk_vollk,  fil_ek_vollk,  fil_vk_vollk,  "
"hk_teilk,  sk_teilk,  fil_ek_teilk,  fil_vk_teilk,  dat,  zeit,  programm,  "
"version) "

#line 49 "a_kalkpreis.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?)"); 

#line 51 "a_kalkpreis.rpp"
}

