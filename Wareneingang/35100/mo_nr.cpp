#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <process.h>
#include <time.h>
#include "wmaskc.h"
#include "mo_curso.h"
#include "dbclass.h"
#include "strfkt.h"
#include "mo_nr.h"

DB_CLASS DbClass;
static long nr;
static long tag;


long AutoNrClass::GetNr (long akt_nr)
/**
Naechste Textnummer holen.
**/
{
         time_t timer;
         int dsqlstatus;
         int cursor;

         if (akt_nr) return akt_nr;

         time (&timer);
         tag = (long) timer;
         DbClass.sqlin ((long *) &tag, 2, 0);
         cursor = DbClass.sqlcursor ("select tag from aufpnr where tag = ?");
         dsqlstatus = DbClass.sqlfetch (cursor); 
         while (dsqlstatus == 0)
         {
                      Sleep (1000); 
                      time (&timer);
                      if (tag == (long) timer)
                      {
                                  continue;
                      } 
                      tag = (long) timer;
                      dsqlstatus = DbClass.sqlfetch (cursor); 
         }
         DbClass.sqlclose (cursor);
         DbClass.sqlin ((long *) &tag, 2, 0);
         DbClass.sqlcomm ("insert into aufpnr (tag) values (?)");
         DbClass.sqlin ((long *) &tag, 2, 0);
         DbClass.sqlout ((long *) &nr, 2, 0);
         nr = 0l;
         dsqlstatus = DbClass.sqlcomm ("select nr from aufpnr where tag = ?");
         DbClass.sqlin ((long *) &tag, 2, 0);
         DbClass.sqlcomm ("delete from aufpnr where tag = ?");
         return nr; 
}

long AutoNrClass::GetGJ (short mdn, short fil, char *nummername)
/**
Naechste Textnummer aus (gj<nummername> holen.
**/
{
	     char datum [12];
		 long dat;
         int dsqlstatus;
         int cursor;
		 char tabname [21];
		 char buffer [256];
		 long nummero;

		 long pid;

//		 pid = getpid ();
		 pid = (long) 1001;
         sysdate (datum);
		 dat = dasc_to_long (datum);
		 clipped (nummername);
		 sprintf (tabname, "gj%s", nummername);
         DbClass.sqlin ((short *) &mdn, 1, 0);
         DbClass.sqlin ((short *) &fil, 1, 0);
         DbClass.sqlin ((long *)  &dat, 2, 0);
         DbClass.sqlin ((long *)  &pid, 2, 0);
		 sprintf (buffer, "select nummero from %s "
			              "where mdn = ? "
						  "and fil = ? "
						  "and datum = ? "
			              "and pid = ?",
			               tabname);
		 cursor = DbClass.sqlcursor (buffer);
         dsqlstatus = DbClass.sqlfetch (cursor); 
         while (dsqlstatus == 0)
         {
                      Sleep (50); 
                      dsqlstatus = DbClass.sqlopen (cursor); 
                      dsqlstatus = DbClass.sqlfetch (cursor); 
         }
         DbClass.sqlclose (cursor);

         DbClass.sqlin ((short *) &mdn, 1, 0);
         DbClass.sqlin ((short *) &fil, 1, 0);
         DbClass.sqlin ((long *)  &dat, 2, 0);
         DbClass.sqlin ((long *)  &pid, 2, 0);
		 sprintf (buffer, "insert into %s (mdn,fil,datum,pid) "
			              "values "
						  "(?,?,?,?)",
						  tabname);
         DbClass.sqlcomm (buffer);

         DbClass.sqlin ((short *) &mdn, 1, 0);
         DbClass.sqlin ((short *) &fil, 1, 0);
         DbClass.sqlin ((long *)  &dat, 2, 0);
         DbClass.sqlin ((long *)  &pid, 2, 0);
         DbClass.sqlout ((long *) &nummero, 2, 0);
         nummero = 0l;
		 sprintf (buffer, "select nummero from %s "
			              "where mdn = ? "
						  "and fil = ? "
						  "and datum = ? "
						  "and pid = ?",
						  tabname);
         dsqlstatus = DbClass.sqlcomm (buffer);

         DbClass.sqlin ((short *) &mdn, 1, 0);
         DbClass.sqlin ((short *) &fil, 1, 0);
         DbClass.sqlin ((long *)  &dat, 2, 0);
         DbClass.sqlin ((long *)  &pid, 2, 0);
		 sprintf (buffer, "delete from %s "
			              "where mdn = ? "
						  "and fil = ? "
						  "and datum = ? "
						  "and pid = ?",
						  tabname);
         dsqlstatus = DbClass.sqlcomm (buffer);

         return nummero; 
}
