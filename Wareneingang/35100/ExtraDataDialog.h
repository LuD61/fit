// ExtraDataDialog.h: Schnittstelle f�r die Klasse CExtraDataDialog.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_EXTRADATADIALOG_H__09A8AF58_70C4_48BF_A445_84673F857645__INCLUDED_)
#define AFX_EXTRADATADIALOG_H__09A8AF58_70C4_48BF_A445_84673F857645__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "ExtraData.h"

class CExtraDataDialog  
{
protected:
	HANDLE Extra;
	CExtraData *ExtraD;
	CExtraData *ExtraDResult;
	CExtraData * (*ExtraData)(CExtraData *); 
public:
	CExtraDataDialog();
	virtual ~CExtraDataDialog();
	void SetData (CExtraData *Data);
	CExtraData *GetData ();
	BOOL DoModal ();

};

#endif // !defined(AFX_EXTRADATADIALOG_H__09A8AF58_70C4_48BF_A445_84673F857645__INCLUDED_)
