#ifndef _SPEZDLG_DEF
#define _SPEZDLG_DEF
#include "dlg.h"
#include "itprog.h"
#include "itfont.h"
#include "formfield.h"

#define MDN_CTL 1801
#define FIL_CTL 1802
#define LIEF_CTL 1803

#define A_CTL 1804
#define A_BZ1_CTL 1805
#define ME_KZ_CTL 1806
#define BEST_TXT1_CTL 1807

#define LIEF_BEST_CTL 1808
#define PR_EK_CTL 1809
#define A_BZ2_CTL 1810
#define BEST_TXT2_CTL 1811
#define MIN_BEST_CTL 1812
#define ME_EINH_EK_CTL 1813
#define LIEF_ZEIT_CTL 1814
#define LIEF_RHT_CTL 1815
#define LIEF_KZ_CTL 1816
#define BZG_LST_CTL 1817
#define DEFAULT_LIEF_KZ_CTL 1818
#define DEFAULT_ME_KZ_CTL 1819

#define IDM_CHOISE 2002
#define IDM_QUIK   2003
#define IDM_LIEFMEBEST 2004

#define HEADCTL 700
#define POSCTL 701
#define FOOTCTL 702

#define RAISEDBORDER 0
#define HIGHBORDER 1
#define LOWBORDER 2
#define HIGHCOLBORDER 3
#define LOWCOLBORDER 4
#define LINEBORDER 5
#include "LiefBzgWork.h"
#include "searcha.h"
#include "searchlief_best.h"

#define ENTERHEAD 0
#define ENTERA 1
#define ENTERADATA 2

#define IDM_DELALL 5000

class LiefBzgDlg : virtual public DLG 
{
          private :
             static char **MeKzCombo; 
             static char *MeKzComboOrg[]; 
             static char *MeKzCombo0[]; 
             static char *MeKzCombo1[]; 
             static ItProg *BzgAfter [];
             static ItProg *BzgBefore [];
             static ItFont *BzgFont [];
             static FORMFIELD *dbfields [];
             static FORMFIELD *dbfieldseuro [];
             static FORMFIELD *dbheadfields [];
             static char *EnableHead[];
             static char *EnableHeadFil[];
             static char *EnablePos [];
             static char *EnableQuik [];
             static char *EnablePosA [];
             int    BorderType;
             static LiefBzgWork liefBzgWork;
             static int EnterMode;
             static int ListRows;
             static SEARCHA SearchA;
             static SEARCHLIEF_BEST SearchLief_best;
             static BOOL InsMode;
             static BOOL WriteOK;
             static char *HelpName;
             static double saveda;
             static BOOL QuikEnter;
             CFORM *Toolbar2;

             static LiefBzgDlg *ActiveLiefBzg;
          public :

            static HBITMAP SelBmp;
            static int MeKzDefault;

            void SetToolbar2 (CFORM *Toolbar2)
            {
                this->Toolbar2 = Toolbar2;
            }

            CFORM *GetToolbar2 (void)
            {
                return Toolbar2;
            }
 	        LiefBzgDlg (int, int, int, int, char *, int, BOOL);
 	        LiefBzgDlg (int, int, int, int, char *, int, BOOL, int);
 	        LiefBzgDlg (int, int, int, int, char *, int, BOOL, int, int);
 	        void Init (int, int, int, int, char *, int, BOOL);
            void FillHeadfields (void);
            BOOL OnKeyDown (void);
            BOOL OnKeyUp (void);
            BOOL OnKey1 (void);
            BOOL OnKey2 (void);
            BOOL OnKey3 (void);
            BOOL OnKey4 (void);
            BOOL OnKey5 (void);
            BOOL OnKey6 (void);
            BOOL OnKey7 (void);
            BOOL OnKey8 (void);
            BOOL OnKey9 (void);
            BOOL OnKey10 (void);
            BOOL OnKey12 (void);
            BOOL OnKeyDelete (void);
            BOOL OnKeyPrior (void);
            BOOL OnKeyNext (void);
            BOOL OnKeyHome (void);
            BOOL OnKeyEnd (void);
            BOOL OnKeyReturn (void);
            BOOL OnPaint (HWND,HDC,UINT,WPARAM,LPARAM);
            BOOL OnActivate (HWND,UINT,WPARAM,LPARAM);
            BOOL OnCommand (HWND,UINT,WPARAM,LPARAM);
            BOOL OnSysCommand (HWND,UINT,WPARAM,LPARAM);
            BOOL OnDestroy (HWND,UINT,WPARAM,LPARAM);
            void PrintComment (char *);
            HWND OpenWindow (HANDLE, HWND);
            HWND OpenScrollWindow (HANDLE, HWND);
            void FillListCaption (void);
            void SetWinBackground (COLORREF);
            void ToClipboard (void);
            void FromClipboard (void);
            void Help (void);
            BOOL ShowMdn (void);
            BOOL ShowFil (void);
            BOOL ShowLief (void);
            BOOL ShowA (void);
            BOOL ShowLief_best (void);
            BOOL Showme_einh_ek (void);
            BOOL Showlief_rht (void);
            int DeleteLief (void);
            void SetListDimension (int, int);
            void DiffListDimension (int, int);
            void CallInfo (void);
            BOOL EnterLiefmebest (void);
            BOOL ChangeQuikEnter (void);

            static int WriteRow (void);
            static int DeleteRow (void);
            static void FillRow (void);
            static void InsertRow (void);
            static void SelectRow (void);
            static void FillEnterList (int);
            static int ListChanged (WPARAM, LPARAM);
            static void FillListRows (void);
            static int SaveA (void);
            static int ReadBzgA (void);
            static int ReadBzgMdn (void);
            static int ReadBzgFil (void);
            static int ReadBzgLief (void);
            static int EnterPos (void);
            static int TestLiefKz (void);
            static int EnableKey10 (void);
            static int DisableKey10 (void);
            static int GetPtab (void);
            static int WrPrEk (void);
            static int TestMinBest (void);
};
#endif
