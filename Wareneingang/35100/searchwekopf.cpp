#include <windows.h>
#include "searchwekopf.h"
#include "strfkt.h"
#include "mo_wmess.h"
#include "lbox.h"
#include "we_kopf.h"


int SEARCHWEKOPF::idx;
long SEARCHWEKOPF::anz;
CHQEX *SEARCHWEKOPF::Query = NULL;
DB_CLASS SEARCHWEKOPF::DbClass; 
HINSTANCE SEARCHWEKOPF::hMainInst;
HWND SEARCHWEKOPF::hMainWindow;
HWND SEARCHWEKOPF::awin;


int SEARCHWEKOPF::SearchLst (char *sebuff)
/**
Nach Kurzname in Liste suchen.
**/
{
	   Query->SearchList ();
	   return 0;
}

int SEARCHWEKOPF::ReadLst (char *name)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int cursor;
	  int i;
      WMESS Wmess;
      char lief_rech_nr [18];
      char blg_typ [3];
      char we_dat [12];


	  anz = 0;
      clipped (name);
      if (strcmp (name, " ") == 0) name[0] = 0;

      if (strlen (name) == 0 ||
          name[0] <= ' ')
      {
	          sprintf (buffer, "select lief_rech_nr, blg_typ, we_dat "
	 			       "from we_kopf "
                       "where mdn = %hd "
                       "and fil = %hd "
                       "and lief = \"%s\" and we_status < 5 order by we_dat desc ",
                       we_kopf.mdn, we_kopf.fil, we_kopf.lief);
      }
      else
      {
	          sprintf (buffer, "select lief_rech_nr, blg_typ, we_dat "
	 			       "from we_kopf "
                       "where mdn = %hd "
                       "and fil = %hd "
                       "and lief = \"%s\" and we_status < 5 order by we_dat desc ",
                       we_kopf.mdn, we_kopf.fil, we_kopf.lief);
      }
      DbClass.sqlout ((char *) lief_rech_nr, 0, 17);
      DbClass.sqlout ((char *) blg_typ, 0, 2);
      DbClass.sqlout ((char *) we_dat, 0, 11);
      cursor = DbClass.sqlcursor (buffer);
      if (cursor < 0) 
      {
          return -1;
      }
  	  i = 0;
      Wmess.Message (hMainInst, hMainWindow, " \nBitte warten.....\n"
  	                                         "Die Daten werden selektiert.\n ");  
	  while (DbClass.sqlfetch (cursor) == 0)
      {
 	      sprintf (buffer, "  |%-16s|    |%-4s|    |%-12s|", lief_rech_nr, blg_typ, we_dat);
	      Query->InsertRecord (buffer);
		  i ++;
      }
      anz = i;
	  DbClass.sqlclose (cursor);
 	  Wmess.Destroy ();
	  EnableWindow (awin, FALSE);
	  return 0;
}

void SEARCHWEKOPF::SetParams (HINSTANCE hMainInst, HWND hMainWindow)
{
      this->hMainInst   = hMainInst;
      this->hMainWindow = hMainWindow;
      awin = hMainWindow;
}

BOOL SEARCHWEKOPF::GetKey (char * key)
{
      int anz;
     
      if (idx == -1) return FALSE;
      anz = wsplit (Key, "|");
      if (anz < 1) return FALSE;
      strcpy (key, wort[1]);
      return TRUE;
}


void SEARCHWEKOPF::Search (void)
{
  	  int cx, cy;
	  char buffer [256];

	  idx = -1;
      cx = 48;
      cy = 25;
      Settchar ('|');
      EnableWindow (hMainWindow, FALSE);
      Query = new CHQEX (cx, cy);
      Query->EnableSort (TRUE);
      Query->OpenWindow (hMainInst, hMainWindow);
      sprintf (buffer, "%s %s %s",
                          "%s",
                          "%s",
                          "%s");
      Query->RowAttr (buffer);
	  sprintf (buffer, " %18s  %8s %15s", "1", "1", "1"); 
	  Query->VLines (buffer, 3);
      sprintf (buffer, "3;16 25;2 35;10");
      Query->RowPos (buffer);
	  EnableWindow (awin, FALSE);

	  sprintf (buffer, " %-16s  %8s   %-15s", "Lief./Rech-Nr.", "Blg.Typ", "WE-Datum"); 
	  Query->InsertCaption (buffer);

	  Query->SetFillDb (ReadLst);
	  Query->SetSearchLst (SearchLst);
      ReadLst ("");
//      Query->SetSortRow (0, TRUE);
      Query->SetSortRow (0, FALSE);
	  Query->ProcessMessages ();
      EnableWindow (hMainWindow, TRUE);
      if (syskey != KEY5)
      {
              idx = Query->GetSel ();
              if (idx != -1)
              {
                     Query->GetText (Key);
              }
      }
	  EnableWindow (awin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (awin);
}

