// WeExtraData.h: Schnittstelle f�r die Klasse CWeExtraData.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WEEXTRADATA_H__7A3E8C62_6D47_4DE9_9909_77801331B21D__INCLUDED_)
#define AFX_WEEXTRADATA_H__7A3E8C62_6D47_4DE9_9909_77801331B21D__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "ExtraData.h"

class CWeExtraData  
{
public:
	int ListPos;
	CExtraData Data;
	CWeExtraData();
	virtual ~CWeExtraData();
};

#endif // !defined(AFX_WEEXTRADATA_H__7A3E8C62_6D47_4DE9_9909_77801331B21D__INCLUDED_)
