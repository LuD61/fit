#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "stnd_best.h"
#include "dbclass.h"
#include "dbfunc.h"

struct BEST_STND best_stnd, best_stnd_null;

static char *sqltext;
 
void SBEST_CLASS::prepare (void)
{
         ins_quest ((char *) &best_stnd.mdn, 1, 0);
         ins_quest ((char *) &best_stnd.fil, 1, 0);  
         ins_quest ((char *) best_stnd.lief, 0, 17);

    out_quest ((char *) &best_stnd.a,3,0);
    out_quest ((char *) &best_stnd.delstatus,1,0);
    out_quest ((char *) &best_stnd.fil,1,0);
    out_quest ((char *) best_stnd.lief,0,17);
    out_quest ((char *) &best_stnd.mdn,1,0);
    out_quest ((char *) &best_stnd.me,3,0);
    out_quest ((char *) &best_stnd.zuord_dat,2,0);
         cursor = prepare_sql ("select best_stnd.a,  "
"best_stnd.delstatus,  best_stnd.fil,  best_stnd.lief,  best_stnd.mdn,  "
"best_stnd.me,  best_stnd.zuord_dat from best_stnd "

#line 26 "stnd_best.rpp"
                               "where mdn = ? "
                               "and   fil = ? "
                               "and   lief = ?");

    ins_quest ((char *) &best_stnd.a,3,0);
    ins_quest ((char *) &best_stnd.delstatus,1,0);
    ins_quest ((char *) &best_stnd.fil,1,0);
    ins_quest ((char *) best_stnd.lief,0,17);
    ins_quest ((char *) &best_stnd.mdn,1,0);
    ins_quest ((char *) &best_stnd.me,3,0);
    ins_quest ((char *) &best_stnd.zuord_dat,2,0);
         sqltext = "update best_stnd set best_stnd.a = ?,  "
"best_stnd.delstatus = ?,  best_stnd.fil = ?,  best_stnd.lief = ?,  "
"best_stnd.mdn = ?,  best_stnd.me = ?,  best_stnd.zuord_dat = ? "

#line 31 "stnd_best.rpp"
                               "where mdn = ? "
                               "and   fil = ? "
                               "and   lief = ?";
  
         ins_quest ((char *) &best_stnd.mdn, 1, 0);
         ins_quest ((char *) &best_stnd.fil, 1, 0);  
         ins_quest ((char *) best_stnd.lief, 0, 17);

         upd_cursor = prepare_sql (sqltext);

    ins_quest ((char *) &best_stnd.a,3,0);
    ins_quest ((char *) &best_stnd.delstatus,1,0);
    ins_quest ((char *) &best_stnd.fil,1,0);
    ins_quest ((char *) best_stnd.lief,0,17);
    ins_quest ((char *) &best_stnd.mdn,1,0);
    ins_quest ((char *) &best_stnd.me,3,0);
    ins_quest ((char *) &best_stnd.zuord_dat,2,0);
         ins_cursor = prepare_sql ("insert into best_stnd (a,  "
"delstatus,  fil,  lief,  mdn,  me,  zuord_dat) "

#line 42 "stnd_best.rpp"
                                   "values "
                                   "(?,?,?,?,?,?,?)");

#line 44 "stnd_best.rpp"

         ins_quest ((char *) &best_stnd.mdn, 1, 0);
         ins_quest ((char *) &best_stnd.fil, 1, 0);  
         ins_quest ((char *) best_stnd.lief, 0, 17);
         del_cursor = prepare_sql ("delete from best_stnd "
                               "where mdn = ? "
                               "and   fil = ? "
                               "and   lief = ?");

         ins_quest ((char *) &best_stnd.mdn, 1, 0);
         ins_quest ((char *) &best_stnd.fil, 1, 0);  
         ins_quest ((char *) best_stnd.lief, 0, 17);
         test_upd_cursor = prepare_sql ("select a from best_stnd "
                               "where mdn = ? "
                               "and   fil = ? "
                               "and   lief = ?");
}

void SBEST_CLASS::preparea (void)
{
         ins_quest ((char *) &best_stnd.mdn, 1, 0);
         ins_quest ((char *) &best_stnd.fil, 1, 0);  
         ins_quest ((char *) best_stnd.lief, 0, 17);
         ins_quest ((char *) &best_stnd.a, 3, 0);
  

    out_quest ((char *) &best_stnd.a,3,0);
    out_quest ((char *) &best_stnd.delstatus,1,0);
    out_quest ((char *) &best_stnd.fil,1,0);
    out_quest ((char *) best_stnd.lief,0,17);
    out_quest ((char *) &best_stnd.mdn,1,0);
    out_quest ((char *) &best_stnd.me,3,0);
    out_quest ((char *) &best_stnd.zuord_dat,2,0);
         cursora = prepare_sql ("select best_stnd.a,  "
"best_stnd.delstatus,  best_stnd.fil,  best_stnd.lief,  best_stnd.mdn,  "
"best_stnd.me,  best_stnd.zuord_dat from best_stnd "

#line 71 "stnd_best.rpp"
                               "where mdn = ? "
                               "and   fil = ? "
                               "and   lief = ? "
                               "and   a = ?");
}


int SBEST_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
	     int dsqlstatus;
         if (cursor == -1)
         {
                this->prepare ();
         }
         dsqlstatus =  this->DB_CLASS::dbreadfirst ();
 	   return dsqlstatus;
}


int SBEST_CLASS::dbreadfirsta (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         int dsqlstatus;

         if (cursora == -1)
         {
                this->preparea ();
         }
         dsqlstatus = open_sql (cursora);
         dsqlstatus = fetch_sql (cursora);
	   return dsqlstatus;
}


int SBEST_CLASS::dbreada (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
	   int dsqlstatus;

         fetch_sql (cursora);
	   return dsqlstatus;
}

int SBEST_CLASS::dbclosea (void)
{
         if (cursora > -1)
         {
                  close_sql (cursora);
                  cursora = -1;
         }
         return 0;
}
