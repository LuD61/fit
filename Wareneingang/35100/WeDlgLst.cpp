//240409
#include<stdio.h>
#include<stdarg.h>
#include<string.h>
#include<windows.h>
#include "stdfkt.h"
#include "strfkt.h"
#include "wmaskc.h"
#include "mo_meld.h"
#include "ListDlg.h"
#include "WeDlgLst.h"
#include "lief_bzg.h"
#include "a_bas.h"
#include "lbox.h"
#include "wedlg.h"
#include "inflib.h"
#include "sys_par.h"
#include "mo_liefch.h"
#include "searchpreise_we.h"
#include "scanean.h"
#include "Integer.h"
#include "wework.h"


int WeDlgLst::StdSize = STDSIZE;
double HoleInhaltLiefmebest (void);
static LIEF_BZG_CLASS LiefBzg;
void InitScanFlags (void);
void PruefeScanFlags (void);


WeWork Wework;
int WeDlgLst::dlgsize    = STDSIZE;
int WeDlgLst::ltgraysize = STDSIZE;
int WeDlgLst::dlgpossize = STDSIZE;
int WeDlgLst::cpsize     = 200;
int we_fil_bel_par = 0;
short AnzTagePreise = 365;
short add_me = 1; //Defaultm��ig wird addiert
bool flgLoesch;
BOOL GehezuScanfeld = FALSE;

WeDlgLst * WeDlgLst::Instance = NULL;

static mfont infofonta = {
                         "ARIAL", 300, 0, 0,
                         REDCOL,
                         GetSysColor (COLOR_3DFACE),
                         0,
                         NULL};

static mfont infofontme = {
                         "ARIAL", 300, 0, 0,
                         REDCOL,
                         GetSysColor (COLOR_3DFACE),
                         0,
                         NULL};

static mfont infofontchg = {
                         "ARIAL", 300, 0, 0,
                         REDCOL,
                         GetSysColor (COLOR_3DFACE),
                         0,
                         NULL};
static mfont infofontX = {
                         "ARIAL", 1100, 0, 0,
                         REDCOL,
                         GetSysColor (COLOR_3DFACE),
                         0,
                         NULL};

static mfont infofontmhd = {
                         "ARIAL", 150, 0, 0,
                         REDCOL,
                         GetSysColor (COLOR_3DFACE),
                         0,
                         NULL};

mfont WeDlgLst::dlgfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         GRAYCOL,
                         0,

                         NULL};

mfont WeDlgLst::ltgrayfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         LTGRAYCOL,
                         0,
                         NULL};

mfont WeDlgLst::dlgposfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         LTGRAYCOL,
                         0,
                         NULL};

mfont *WeDlgLst::Font = &dlgposfont;

static char A_INFO [30] = {" \0"};
static char ME_INFO [30] = {" \0"};
static char CHG_INFO [30] = {" \0"};
static char ACHTUNG [10] = {"\0"};
//static char MHD_INFO [30] = {" \0"};

CFIELD *WeDlgLst::_fPos [] = {
                     new CFIELD ("pos_frame", "",    120,11, 1, 0,  NULL, "",
                                 CBORDER,
                                 500, Font, 0, TRANSPARENT),

					 new CFIELD ("a_info", A_INFO, 15, 0, 96, 0,  NULL, "", CREADONLY,
                                 LA_INFOCTL, Font, 0, TRANSPARENT),

					 new CFIELD ("me_info", ME_INFO, 15, 0, 96, 2,  NULL, "", CREADONLY,
                                 LME_INFOCTL, Font, 0, TRANSPARENT),

					 new CFIELD ("chg_info", CHG_INFO, 15, 0, 96, 5,  NULL, "", CREADONLY,
                                 LCHG_INFOCTL, Font, 0, TRANSPARENT),
					 new CFIELD ("achtung", ACHTUNG, 1, 0, 83, 0,  NULL, "", CREADONLY,
                                 500, Font, 0, TRANSPARENT),

//					 new CFIELD ("mhd_info", MHD_INFO, 10, 0, 86, 7,  NULL, "", CREADONLY,
//                                 LMHD_INFOCTL, Font, 0, TRANSPARENT),


                     new CFIELD ("scan_txt", "Scannen", 0, 0, 2, 1,  NULL, "",
					             CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("scanfeld", LISTDLG::Lsts.scanfeld, 70, 0, 10, 1,  NULL, "", CEDIT,
                                 LSCANFELD_CTL, Font, 0, 0),

                     new CFIELD ("a_txt", "Artikel",  0, 0, 2, 2,  NULL, "",
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
					 new CFIELD ("a", LISTDLG::Lsts.a, 14, 0, 2, 3,  NULL, "%13.0lf", CEDIT,
                                 LA_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("a_choise", "", 2, 0, 16, 3, NULL, "", CBUTTON,
                                  IDM_CHOISE, Font, 0, BS_BITMAP),

                     new CFIELD ("lager_txt", "Lager", 0,0, 2, 6,  NULL, "",
					             CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("lager_bez", LISTDLG::Lsts.lager_bez, 18, 0, 2,7,  NULL, "", CEDIT,
                                 500, Font, 0,    0),
                     new CFIELD ("lager_choise", "", 2, 0, 20, 7, NULL, "", CBUTTON,
                                  IDM_CH_LAGER, Font, 0, BS_BITMAP),


/*
                     new CFIELD ("a_bz1_txt", "Artikeltext1", 0, 0, 2, 3,  NULL, "",
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
*/
                     new CFIELD ("a_bz1", LISTDLG::Lsts.a_bz1, 25, 0, 2, 4,  NULL, "", CREADONLY,
                                 LA_BZ1_CTL, Font, 0, 0),
/*
                     new CFIELD ("a_bz2_txt", "Artikeltext2", 0, 0, 2, 5,  NULL, "",
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
*/
                     new CFIELD ("a_bz2", LISTDLG::Lsts.a_bz2, 25, 0, 2, 5,  NULL, "", CREADONLY,
                                 LA_BZ2_CTL, Font, 0, 0),

                     new CFIELD ("me_txt", "Menge",  0, 0, 19, 2,  NULL, "",
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("me", LISTDLG::Lsts.me, 17, 0, 19,3,  NULL, "%12.3lf", CEDIT,
                                 LME_CTL, Font, 0,    ES_RIGHT),

                     new CFIELD ("me_einh_txt", "Einheit",  0, 0, 37, 2,  NULL, "",
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("me_einh_bez", LISTDLG::Lsts.me_einh_bez, 9, 0, 37,3,  NULL, "", CREADONLY,
                                 500, Font, 0,    0),
                     new CFIELD ("me_einh_choise", "", 2, 0, 46, 3, NULL, "", CBUTTON,
                                  IDM_CH_MEEINH, Font, 0, BS_BITMAP),

                     new CFIELD ("pr_ek_txt", "EK-Preis",  0, 0, 50, 2,  NULL, "",
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("pr_ek", LISTDLG::Lsts.pr_ek, 10, 0, 50, 3,  NULL, "%8.4lf", CEDIT,
                                 PR_EK_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("preise_choise", "", 2, 0, 60, 3, NULL, "", CBUTTON,
                                  IDM_CH_PREISE, Font, 0, BS_BITMAP),

                     new CFIELD ("pr_netto_ek_txt", "Netto-EK",  0, 0, 63, 2,  NULL, "",
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("netto_ek", LISTDLG::Lsts.netto_ek, 10, 0, 63, 3,  NULL, "%8.4lf", CREADONLY,
                                 500, Font, 0, ES_RIGHT),
                     new CFIELD ("inh_txt", "Inhalt",  0, 0, 74, 2,  NULL, "",
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("inh", LISTDLG::Lsts.inh, 9, 0, 74, 3,  NULL, "%8.3lf", CREADONLY,
                                 500, Font, 0, ES_RIGHT),
                     new CFIELD ("best_me_txt", "Bestell-Menge", 0 , 0, 37, 4,  NULL, "",
					             CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("best_me", LISTDLG::Lsts.best_me, 12, 0, 37, 5,  NULL,
                                 "%12.3lf",  CREADONLY,
                                 BEST_ME_CTL, Font, 0, ES_RIGHT),

                     new CFIELD ("me_einh_txt", "Best.Einh",  0, 0, 50, 4,  NULL, "",
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
								 new CFIELD ("best_me_einh_bez", LISTDLG::Lsts.best_me_einh_bez,
								                              10, 0, 50,5,  NULL, "", CREADONLY,
                                 500, Font, 0,    0),

                     new CFIELD ("lief_best_txt", "Liefer.Bestellnr.", 0, 0, 63, 4,  NULL, "",
					             CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),

                     new CFIELD ("lief_best", LISTDLG::Lsts.lief_best, 14, 0, 63, 5,  NULL, "", CEDIT,
                                 LLIEF_BEST_CTL, Font, 0, 0),

                     new CFIELD ("tara_txt", "Tara",  0, 0, 77, 4,  NULL, "",
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("tara", LISTDLG::Lsts.tara, 6, 0, 77, 5,  NULL, "%5.3lf", CEDIT,
                                 LTARA_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("ls_ident_txt", "Ident-Nummer", 0, 0, 22, 6,  NULL, "",
					             CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("ls_ident", LISTDLG::Lsts.ls_ident, 21, 0, 22, 7,  NULL, "", CEDIT,
                                 LIDENT_CTL, Font, 0, 0),
                     new CFIELD ("ls_charge_txt", "Chargen-Nummer", 0, 0, 44, 6,  NULL, "",
					             CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("ls_charge", LISTDLG::Lsts.ls_charge, 28, 0, 44, 7,  NULL, "", CEDIT,
                                 LCHARGE_CTL, Font, 0, 0),
                     new CFIELD ("mhd_txt", "MHD",  0, 0, 72, 6,  NULL, "",
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
//                     new CFIELD ("mhd", LISTDLG::Lsts.mhd, 9, 0, 72, 7,  NULL, "%4d", CEDIT,
//                                 LMHD_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("mhd", LISTDLG::Lsts.hbk_dat, 11, 0,72,07,  NULL, "dd.mm.yyyy", 
                                 CEDIT,
                                 HBKDAT_CTL, Font, 0, 0),

                     NULL,
};

CFORM WeDlgLst::fPos (41, _fPos);




CFIELD *WeDlgLst::_fList [] = {
       			     new CFIELD ("Liste", "",
				                     85,14, 1,8, NULL, "", CLISTBOX,
												  BZG_LST_CTL, Font, 0, 0),
                     NULL,
};

CFORM WeDlgLst::fList (1, _fList);

CFIELD *WeDlgLst::_fFoot [] = {
                     new CFIELD ("ok",      "    OK    ", 10, 0, 0, 0,  NULL, "",
                                 CBUTTON,
                                 OK_CTL, Font, 0, 0),
                     new CFIELD ("cancel", " Abbruch  ", 10, 0,12, 0,  NULL, "",
                                 CBUTTON,
                                 CANCEL_CTL, Font, 0, 0),
                     NULL,
};


CFORM WeDlgLst::fFoot (2, _fFoot);

CFIELD *WeDlgLst::_fWeLst [] = {
                     new CFIELD ("fPos", (CFORM *) &fPos,  0, 0, 0, 0, NULL, "", CFFORM,
                                   POSCTL, Font, 0, 0),
                     new CFIELD ("fFoot", (CFORM *) &fFoot, 0, 0, -1, -2, NULL, "", CFFORM,
                                   FOOTCTL, Font, 0, 0),
                     NULL,
};

CFORM WeDlgLst::fWeLst (2, _fWeLst);

CFIELD *WeDlgLst::_fWeLst0 [] = {
                     new CFIELD ("fWeLst", (CFORM *) &fWeLst, 0, 0, 1, 1, NULL, "", CFFORM,
                                   500, Font, 0, 0),
                     NULL,
};
CFORM WeDlgLst::fWeLst0 (1, _fWeLst0);


ItFont *WeDlgLst::weLstFont [] = {
                                  new ItFont ("a_txt",           &ltgrayfont),
                                  new ItFont ("a_bz1_txt",       &ltgrayfont),
                                  new ItFont ("me_txt",          &ltgrayfont),
                                  NULL
};

ItFont *WeDlgLst::InfoFont [] = {
                                  new ItFont ("a_info",  &infofonta),
                                  new ItFont ("chg_info",  &infofontchg),
                                  new ItFont ("me_info",  &infofontme),
                                  new ItFont ("achtung",  &infofontX),
//                                  new ItFont ("mhd_info",  &infofontmhd),
                                  NULL
};

ItProg *WeDlgLst::After [] = {
                                   new ItProg ("a",           AfterA),
//                                   new ItProg ("scanfeld",    AfterScanfeld),
//                                   new ItProg ("me",          AfterMe
                                   new ItProg ("pr_ek",       AfterPrEk),
                                   new ItProg ("lager",       AfterLager),
                                   new ItProg ("ls_ident",       AfterLsIdent),
//                                   new ItProg ("mhd",       AfterMHD),
//                                   new ItProg ("ls_charge",   AfterLsCharge),
                                   NULL,
};
ItProg *WeDlgLst::Before [] = {
                                   new ItProg ("a",           BeforeA),
                                   NULL,
};

WeDlgLst *WeDlgLst::ActiveWeDlgLst = NULL;


//=======================================================================================
//=================== SCANPROZEDUREN    SCANNING =======================================
//=======================================================================================
static int dsqlstatus;
static BOOL SmallScann = TRUE;
static SCANEAN Scanean ((Text) "scanean.cfg");
static long scangew = 0l;
CEan128Date Ean128Date;
static BOOL Scannen = FALSE;
static int Scanlen = 14;
static BOOL ScanMode = FALSE;
static BOOL BreakScan = FALSE;
static BOOL ScanAFromEan = TRUE;
static int Ean128Eanlen = 12;
char Ean128EanPruefziffer[2] = " ";

static BOOL ChargeZwang = TRUE;
static BOOL ScanCharge = FALSE;
static BOOL MultiTemperature = TRUE;
int ArtikelUnvollstaendig = 0;
BOOL ChargeVorhanden = TRUE;
BOOL MengeVorhanden = TRUE;
BOOL ArtikelVorhanden = TRUE;
BOOL MHDVorhanden = TRUE;

int artikelOK (char *buffer, BOOL DispMode=TRUE);
int ArtikelFromScann (char *buffer, BOOL DispMode);

extern int _WMDEBUG ;
void GetWMDEBUG (void)
{
      if (getenv ("WMDEBUG"))
      {
            _WMDEBUG = atoi (getenv ("WMDEBUG"));
      }
}



int artikelOK (double a, BOOL DispMode=TRUE)
{
	char buffer [20];
	sprintf (buffer, "%.0lf", a);
	return ArtikelFromScann (buffer, TRUE);

}



int ChangeChar (int key)
{

	CInteger Key (key);
	Scanean.AiInfos.FirstPosition ();
    AiInfo *ai;
	while ((ai = (AiInfo *) Scanean.AiInfos.GetNext ()) != NULL)
	{
		ai->EndChar.FirstPosition ();
		CInteger *ec;
		while ((ec = (CInteger *) ai->EndChar.GetNext ()) != NULL)
		{
			if (Key == *ec)
			{
				return ai->EndCharEdit;
			}
		}
	}
	return key;
}


int ScanEan128Charge (Text& Ean128, BOOL DispMode=TRUE)
{
		Text Charge = "";
		Text *charge = Scanean.GetEan128Ai (Text ("10"), Ean128);

		if (ChargeZwang && _a_bas.charg_hand == 1 && charge == NULL && strlen(LISTDLG::Lsts.ls_charge) == 0)
		{
//		        disp_mess ("Fehler bei Ean128 : Charge", 2);
			    ArtikelUnvollstaendig = 1;
				ArtikelVorhanden = FALSE;
					strcpy(LISTDLG::Lsts.scanfeld,"Chargennummer fehlt, bitte einscannen oder im Chargenfeld eingeben!");
				return 100;
		}

		if (charge != NULL)
		{
					
			    if (_a_bas.charg_hand == 9)
				{
					strcpy (LISTDLG::Lsts.ls_ident, charge->GetBuffer ());
				}
				else
				{
					Charge = *charge;
					strcpy (LISTDLG::Lsts.ls_charge, Charge.GetBuffer ());
				}
		}
		else
		{
			    if (_a_bas.charg_hand == 9)
				{
					strcpy (LISTDLG::Lsts.ls_ident, "");
				}
				else
				{
					strcpy (LISTDLG::Lsts.ls_charge, "");
				}
		}
//== MHD ===============================================
	    char datum [12];
		long hbk_dat;
	    long heute; 
		Text Mhd = "";
		Text *mhd = Scanean.GetEan128Ai (Text ("15"), Ean128);
		if (mhd != NULL)
		{
				Text Mhd = *mhd;
				Ean128Date = Mhd;
				strncpy(datum ,Ean128Date.ToString ().GetBuffer (),12);
				strncpy(LISTDLG::Lsts.hbk_dat ,Ean128Date.ToString ().GetBuffer (),12);
				hbk_dat = dasc_to_long(datum);
				sysdate(datum);
				heute = dasc_to_long(datum);
			// Haltbarketstage aus Haltbarkeitsdatum berechnen
//			if (we_jour.hbk_dat >= ldat)
//			{
			      sprintf (LISTDLG::Lsts.mhd, "%hd", hbk_dat - heute);
//			}

				delete mhd;
		}
		else
		{
			Ean128Date = Mhd;
		}


		return 0;
}


int ScanEan128Ai (Text& Ean128, BOOL DispMode=TRUE)
{
//    	ReadNve *readNve;
//		Text nve;

/* Teil f�r allgemeine Ean128verarbeitung */

//	    char buffer [256]; 
        double ean1 = 0.0;
		double Gew = 0.0;
		double Stk = 0.0;

//== EAN ============================================
		if (lese_a_bas(_a_bas.a) != 0)
		{
			//Artikel noch nicht vorhanden, muss gelesen werden
			Text *ean = Scanean.GetEan128Ai (Text ("01"), Ean128);
			if (ean == NULL)
			{
				ean = Scanean.GetEan128Ai (Text ("02"), Ean128);
				if (ean == NULL)
				{
						strcpy(LISTDLG::Lsts.scanfeld,"Strichcode nicht lesbar, Bezeichner (01) oder (02) fehlen!");
						ArtikelUnvollstaendig = 99;
						ArtikelVorhanden = FALSE;

//						dsqlstatus = 99;
//						return 100;
				}
			}

			if (ean != NULL)
			{
				Text Ean = ean->SubString (1, Ean128Eanlen);
				Text Pruefziffer = ean->SubString (Ean128Eanlen+1,1);
				delete ean;
				double a = ratod (Ean.GetBuffer ());
				strncpy(Ean128EanPruefziffer , Pruefziffer.GetBuffer(),1);
				ean1 = a;
				if (artikelOK (a, FALSE) != 0)
				{
					return 100;
				}
	
				sprintf (LISTDLG::Lsts.a, "%.0lf", _a_bas.a);
				BreakScan = FALSE;
			}
		}

//== Gewicht ============================================
		// 310x ist Nettogewicht in kg (x zeigt die anzahl nachkommastellen an)
	if (ratod(LISTDLG::Lsts.me) < 0.001) 
	{

		Text *gew = Scanean.GetEan128Ai (Text ("3103"), Ean128);
		int GewFaktor = 1000;
		if (gew == NULL)
		{
			gew = Scanean.GetEan128Ai (Text ("3102"), Ean128); //Detlef 031208 
			GewFaktor = 100;
		}
		if (gew == NULL)
		{
			gew = Scanean.GetEan128Ai (Text ("3101"), Ean128); //Detlef 031208 
			GewFaktor = 10;
		}
		if (gew == NULL)
		{
			gew = Scanean.GetEan128Ai (Text ("3100"), Ean128); //Detlef 031208 
			GewFaktor = 1;
		}
		if (gew == NULL)
		{
			gew = Scanean.GetEan128Ai (Text ("3104"), Ean128); //Detlef 031208 
			GewFaktor = 10000;
		}
		if (gew == NULL)
		{
			gew = Scanean.GetEan128Ai (Text ("3105"), Ean128); //Detlef 031208 
			GewFaktor = 100000;
		}
		if (gew == NULL)
		{
			gew = Scanean.GetEan128Ai (Text ("3106"), Ean128); //Detlef 031208 
			GewFaktor = 1000000;
		}
		if (gew == NULL)
		{
		}
		else
		{
			Gew = ratod (gew->GetBuffer ());
			delete gew;
		}

//== St�ck ============================================
		Text *stk = Scanean.GetEan128Ai (Text ("30"), Ean128);
		if (stk == NULL)
		{
			stk = Scanean.GetEan128Ai (Text ("37"), Ean128);
		}
		if (stk == NULL)
		{
		}
		else
		{
			Stk = ratod (stk->GetBuffer ()) ;
			delete stk;
		}

		sprintf (LISTDLG::Lsts.a, "%.0lf", _a_bas.a);
		if (lief_bzg.me_einh_ek == 2) //kg
		{
			scangew = (long) (Gew);
			if (scangew != 0l)
			{
				sprintf (LISTDLG::Lsts.me, "%.3lf", (double) scangew / GewFaktor);
			}
			else
			{
				scangew = (long) (Stk);
				if (scangew != 0l)
				{
					//In diesem Fall wird aus lefmebest die Gr��e Einheit (Palette genommen) #31.12.2008
					double Inhalt = Wework.GetMaxInh (we_kopf.mdn, we_kopf.fil, we_kopf.lief, _a_bas.a);
					if (Inhalt > 0.0) 
					{
						sprintf (LISTDLG::Lsts.me, "%.3lf", (double) scangew * Inhalt);
					}
					else
					{
							strcpy(LISTDLG::Lsts.scanfeld,"Im Ean ist nur eine St�ckzahl codiert, Artikel ist aber als Gewichtsartikel in den Bezugsquellen hinterlegt!");
						    ArtikelUnvollstaendig = 3;
							MengeVorhanden = FALSE;
					}
				}
				else
				{
//						strcpy(LISTDLG::Lsts.scanfeld,"Im Ean fehlt das Gewicht!");
					    ArtikelUnvollstaendig = 2;
						MengeVorhanden = FALSE;
				}
			}
		}
		else // als St�ck hinterlegt 
		{
			scangew = (long) (Stk);
			if (scangew != 0l)
			{
	            sprintf (LISTDLG::Lsts.me, "%.3lf", (double) scangew );
			}
			else
			{
				scangew = (long) (Gew);
				if (scangew != 0l)
				{
		            sprintf (LISTDLG::Lsts.me, "%.3lf", (double) scangew / lief_bzg.min_best / GewFaktor);
				}
				else
				{
//						strcpy(LISTDLG::Lsts.scanfeld,"Im Ean fehlt das Gewicht oder die St�ckzahl!");
						ArtikelUnvollstaendig = 2;
						MengeVorhanden = FALSE;
				}

			}
		}
	} //if (ratod(LISTDLG::Lsts.me) < 0.001)




//== MHD ===============================================
	    char datum [12];
		long hbk_dat;
	    long heute; 
		Text Mhd = "";
		Text *mhd = Scanean.GetEan128Ai (Text ("15"), Ean128);
		if (mhd != NULL)
		{
				Text Mhd = *mhd;
				Ean128Date = Mhd;
				strncpy(datum ,Ean128Date.ToString ().GetBuffer (),12);
				strncpy(LISTDLG::Lsts.hbk_dat ,Ean128Date.ToString ().GetBuffer (),12);
				hbk_dat = dasc_to_long(datum);
				sysdate(datum);
				heute = dasc_to_long(datum);
			// Haltbarketstage aus Haltbarkeitsdatum berechnen
//			if (we_jour.hbk_dat >= ldat)
//			{
			      sprintf (LISTDLG::Lsts.mhd, "%hd", hbk_dat - heute);
//			}

				delete mhd;
		}
		else
		{
			Ean128Date = Mhd;
		}

//== Charge ============================================
	if (strlen(clipped(LISTDLG::Lsts.ls_charge)) < 2) 
	{
		Text Charge = "";
		Text *charge = Scanean.GetEan128Ai (Text ("10"), Ean128);

		if (ChargeZwang && _a_bas.charg_hand == 1 && charge == NULL  )
		{
//		        disp_mess ("Fehler bei Ean128 : Charge", 2);
			    ArtikelUnvollstaendig = 1;
				ChargeVorhanden  = FALSE;
//					strcpy(LISTDLG::Lsts.scanfeld,"Chargennummer fehlt, bitte einscannen oder im Chargenfeld eingeben!");
				return 100;
		}

		if (charge != NULL)
		{
					
			    if (_a_bas.charg_hand == 9)
				{
					strcpy (LISTDLG::Lsts.ls_ident, charge->GetBuffer ());
				}
				else
				{
					Charge = *charge;
					strcpy (LISTDLG::Lsts.ls_charge, Charge.GetBuffer ());
				}
		}
	}
        return 0;
}

	

int ScanEan128 (Text& Ean128, BOOL DispMode)
{
	return ScanEan128Ai (Ean128, DispMode);
}

double HoleInhaltLiefmebest (void)
{
	return (double) -1;
}

int lese_lief_bzg (char *buffer, double *a)
{

	     memcpy (&lief_bzg, &lief_bzg_null, sizeof (struct LIEF_BZG));
	     lief_bzg.mdn = we_kopf.mdn;
	     lief_bzg.fil = we_kopf.fil;
		 strcpy (lief_bzg.lief, we_kopf.lief);
		 strncpy (lief_bzg.lief_best, buffer,16);
//		 return WeDlgLst::Readlief_best();
		 dsqlstatus = LiefBzg.dbreadfirst ();
		 while (dsqlstatus == 100)
		 {
			 if (lief_bzg.fil)
			 {
				 lief_bzg.fil = 0;
			 }
			 else if (lief_bzg.mdn)
			 {
				 lief_bzg.mdn = 0;
			 }
			 else
			 {
				 break;
			 }
  		     dsqlstatus = LiefBzg.dbreadfirst ();
		 }
		 if (dsqlstatus == 0)
		 {
			 *a = lief_bzg.a;
	         dsqlstatus = lese_a_bas (*a);
		 }
		 else
		 {
			 *a = (double) 0;
		 }
		 if (dsqlstatus == 0) return 0;

// jetzt Suche �ber lief_bzg.ean
	     lief_bzg.mdn = we_kopf.mdn;
	     lief_bzg.fil = we_kopf.fil;
		 strcpy (lief_bzg.lief, we_kopf.lief);
		 strncpy (lief_bzg.ean, buffer,16);
		 if (strlen(clipped(buffer)) > 9 && strlen(clipped(buffer)) <= 14)
		 {
			sprintf (lief_bzg.ean, "*%s*", clipped(buffer));
		 }
		 dsqlstatus = LiefBzg.dbreadfirstean ();
		 while (dsqlstatus == 100)
		 {
			 if (lief_bzg.fil)
			 {
				 lief_bzg.fil = 0;
			 }
			 else if (lief_bzg.mdn)
			 {
				 lief_bzg.mdn = 0;
			 }
//			 else if (strlen(clipped(lief_bzg.ean)) <  ( unsigned)Ean128Eanlen + 2 )
//			 {
				 // nicht mehr n�tig
//			     lief_bzg.mdn = we_kopf.mdn;
//				 lief_bzg.fil = we_kopf.fil;
//				 strncpy (lief_bzg.ean, buffer,12);
//				 strcat(lief_bzg.ean,Ean128EanPruefziffer);
//				 sprintf (lief_bzg.ean, "*%s*", clipped(lief_bzg.ean));
//			 }
			 else
			 {
				 break;
			 }
  		     dsqlstatus = LiefBzg.dbreadfirstean ();
		 }
		 if (dsqlstatus == 0)
		 {
			 *a = lief_bzg.a;
	         dsqlstatus = lese_a_bas (*a);
		 }
		 else
		 {
			 *a = (double) 0;
		 }
		 return dsqlstatus;
}


static double ean_a = 0l;
static double ean = 0l;

int ScanArtikel (double a)
{

	      DB_CLASS DbClass;

        Text Ean;
        Text EanDb;
        Text EanA;
        STRUCTEAN *EanStruct;
		static short cursor_ean = -1;
        double ean = 0l;

		writelog ("Scanartikel %.0lf\n", a);
        scangew = 0l;
        Ean.Format ("%13.0lf", a);
		if (Ean.GetLength () >= 12)
		{
			EanStruct = Scanean.GetStructean (Ean);
		}
		else
		{
			EanStruct = NULL;
		}
		writelog ("EanStruct %ld\n", EanStruct);
		char *sc;
        if (EanStruct != NULL)
        {
            switch (Scanean.GetType (Ean))
            {
            case EANGEW :
				if (Ean.GetLength () < 13)
				{
					break;
				}
                EanDb = Scanean.GetEanForDb (Ean);
                ean = ratod (EanDb.GetBuffer ());
				sc = Scanean.GetEanGew (Ean);
				if (sc != NULL)
				{
					scangew = atol (Scanean.GetEanGew (Ean));
				}
				else
				{
					EanStruct = NULL;
				}
                break;
            case EANPR :
                EanDb = Scanean.GetEanForDb (Ean);
                ean = ratod (EanDb.GetBuffer ());
                BreakScan = TRUE;
                break;
            default :
                ean = ratod (Ean.GetBuffer ());
                BreakScan = TRUE;
/*
                if (ScanKunEan (ean) == 0)
                {
                    return 0;
                }
*/
            }
        }
/*
        else
        {
                ean = ratod (Ean.GetBuffer ());
				writelog ("ScanEan : EAN %.0lf", ean);
                BreakScan = TRUE;
                if (ScanKunEan (ean) == 0)
                {
				    writelog ("ScanKunEan OK gefunden: a %.0lf", _a_bas.a);
                    return 0;
                }
			    writelog ("ScanKunEan OK nicht gefunden");
        }
*/

        if (cursor_ean == -1)
		{
              DbClass.sqlout ((double *) &ean_a, 3, 0);
              DbClass.sqlin  ((double *) &ean, 3, 0);
              cursor_ean = DbClass.sqlcursor ("select a from a_ean where ean = ?");
		}
        ean = ratod (Ean.GetBuffer ());
        writelog ("ScanEan: EAN %.0lf", ean);
		DbClass.sqlopen (cursor_ean);
		dsqlstatus = DbClass.sqlfetch (cursor_ean);

        if (dsqlstatus == 0)
        {
			     writelog ("ScanEan OK gefunden: a %.0lf", _a_bas.a);
                 dsqlstatus = lese_a_bas (ean_a);
        }
        if (dsqlstatus == 0)
        {
                 return dsqlstatus;
        }
        if (EanStruct != NULL && EanStruct->GetAFromEan () == TRUE)
//            ((EanStruct->GetType ().GetBuffer ())[0] == 'G' ||
//            (EanStruct->GetType ().GetBuffer ())[0] != 'P'))
        {
                 EanA = Ean.SubString (2, EanStruct->GetLen ());
                 a = ratod (EanA.GetBuffer ());
                 dsqlstatus = lese_a_bas (a);
        }
        return dsqlstatus;
}


int ScanLiefEan (char *buffer, double *a)
{
		  dsqlstatus = lese_lief_bzg (buffer, a);

	  if (dsqlstatus != 0)
	  {
 			dsqlstatus = lese_a_bas (ratod (buffer));
			*a = ratod (buffer);
			if (dsqlstatus == 100)
			{
						dsqlstatus = ScanArtikel (*a);
			}
	  }
	  else
	  {
 			dsqlstatus = lese_a_bas (*a);
	  }

	  if (dsqlstatus == 100)
	  {
			sprintf(LISTDLG::Lsts.scanfeld, "EAN %s nicht gefunden" ,buffer);

	  }
	  return dsqlstatus;
}


int ArtikelFromScann (char *buffer, BOOL DispMode)
{

    double a;

//    if (strlen (buffer) >= 16)
    if (DispMode == FALSE)   // wenn TRUE, kommts aus artikelOk()
	{
			 ScanEan128 (Text (buffer), DispMode);
	}
	else
	{
			 ScanLiefEan (buffer, &a);
	}
    return dsqlstatus;
}











WeDlgLst::WeDlgLst (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel) :
          DlgLst (x, y, cx, cy, Caption, Size, Pixel),
          DLG (x, y, cx, cy, Caption, Size, Pixel)
{
             int xfull, yfull;
			 int i;

			 Instance = this;
             LISTDLG::LstTab = new LSTS [0x10000];
		     ProgCfg = new PROG_CFG ("35100");
	         Scanean.Read ();
		     GetCfgValues ();
		     GetWMDEBUG ();

             strcpy (DefaultIdent,  "");
             strcpy (DefaultCharge, "");

		     GetSysPar ();
             xfull = GetSystemMetrics (SM_CXFULLSCREEN);
             yfull = GetSystemMetrics (SM_CYFULLSCREEN);

             StdSize = Size;
             dlgfont.FontHeight    = Size;
             ltgrayfont.FontHeight    = Size;
             dlgposfont.FontHeight = Size;

             if (xfull < 1000)
             {
                 dlgfont.FontHeight = 90;
                 ltgrayfont.FontHeight = 90;
                 dlgposfont.FontHeight = 90;
             }

             if (xfull < 800)
             {

                 dlgfont.FontHeight = 70;
                 ltgrayfont.FontHeight = 70;
                 dlgposfont.FontHeight = 70;
             }

             for (i = 0; After[i] != NULL; i ++)
             {
                 After[i]->SetAfter (&fWeLst0);
             }
             for (i = 0; Before[i] != NULL; i ++)
             {
                 Before[i]->SetBefore (&fWeLst0);
             }
             for (i = 0; InfoFont[i] != NULL; i ++)
             {
                         InfoFont[i]->SetFont (&fPos);
             }



/*
             if (BorderType == HIGHCOLBORDER)
             {

                for (i = 0; weLstFont[i] != NULL; i ++)
                {
                 weLstFont[i]->SetFont (&fPos);
                }

                fPos.GetCfield ("pos_frame")->SetBorder
                 (new FILLEDBORDER (BLACKCOL, WHITECOL, HIGH, 0, GetSysColor (COLOR_3DFACE)));
             }
             else if (BorderType == LOWCOLBORDER)
             {

                for (i = 0; weLstFont[i] != NULL; i ++)
                {
                 weLstFont[i]->SetFont (&fPos);
                }
                fPos.GetCfield ("pos_frame")->SetBorder
                 (new FILLEDBORDER (BLACKCOL, WHITECOL, LOW, 0, GetSysColor (COLOR_3DFACE)));
             }
             else if (BorderType == HIGHBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, HIGH);
             }
             else if (BorderType == LOWBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, LOW);
             }
             else if (BorderType == RAISEDBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, RAISED);
             }
             else if (BorderType == LINEBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, LINE);
             }
*/

             fPos.SetFieldanz ();
             fFoot.SetFieldanz ();
             fWeLst.SetFieldanz ();

             fPos.GetCfield ("a_choise")->SetBitmap
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fPos.GetCfield ("a_choise")->SetTabstop (FALSE);
             fPos.GetCfield ("me_einh_choise")->SetBitmap
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fPos.GetCfield ("me_einh_choise")->SetTabstop (FALSE);
             fPos.GetCfield ("lager_choise")->SetBitmap
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fPos.GetCfield ("lager_choise")->SetTabstop (FALSE);
             fPos.GetCfield ("preise_choise")->SetBitmap
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fPos.GetCfield ("preise_choise")->SetTabstop (FALSE);
             fList.GetCfield ("Liste")->SetTabstop (FALSE);

             fPos.GetCfield ("a_info")->SetTabstop (FALSE);
             fPos.GetCfield ("me_info")->SetTabstop (FALSE);
             fPos.GetCfield ("chg_info")->SetTabstop (FALSE);
             fPos.GetCfield ("achtung")->SetTabstop (FALSE);
//             fPos.GetCfield ("mhd_info")->SetTabstop (FALSE);


             if (fWeLst.GetCfield ("fFoot") != NULL)
             {
                      fWeLst.InsertCfieldAt (_fList[0], "fFoot");
                      fWeLst.RemoveCfield ("fFoot");
             }
	         _fList[0]->Setchsize (-2, -2);
			 SetDialog (&fWeLst0);
			 Work = NULL;
             WithSearchBest = FALSE;
			 ActiveWeDlgLst = this;
}


WeDlgLst::~WeDlgLst ()
{
	if (LISTDLG::LstTab != NULL)
	{
		delete []LISTDLG::LstTab;
		LISTDLG::LstTab = NULL;
	}
    if (ProgCfg != NULL)
    {
        ProgCfg->CloseCfg ();
        delete ProgCfg;
        ProgCfg = NULL;
    }
}


void WeDlgLst::SetBorderType (int BorderType)
{
     int i;

     this->BorderType = BorderType;
     if (BorderType == HIGHCOLBORDER)
     {

            for (i = 0; weLstFont[i] != NULL; i ++)
            {
                 weLstFont[i]->SetFont (&fPos);
            }
            fPos.GetCfield ("pos_frame")->SetBorder
                 (new FILLEDBORDER (BLACKCOL, WHITECOL, HIGH, 0, GetSysColor (COLOR_3DFACE)));
             }
             else if (BorderType == LOWCOLBORDER)
             {

                for (i = 0; weLstFont[i] != NULL; i ++)
                {
                 weLstFont[i]->SetFont (&fPos);
                }
                fPos.GetCfield ("pos_frame")->SetBorder
                 (new FILLEDBORDER (BLACKCOL, WHITECOL, LOW, 0, GetSysColor (COLOR_3DFACE)));
             }
             else if (BorderType == HIGHBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, HIGH);
             }
             else if (BorderType == LOWBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, LOW);
             }
             else if (BorderType == RAISEDBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, RAISED);
             }
             else if (BorderType == LINEBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, LINE);
             }
}



BOOL WeDlgLst::OnCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (fWork)
        {

              if (LOWORD (wParam) == IDM_CHOISE)
              {
                  return OnKey9 ();
              }
              if (LOWORD (wParam) == IDM_CH_MEEINH)
              {
			      return ShowMeEinh ();
			  }
              if (LOWORD (wParam) == IDM_CH_LAGER)
              {
			      return ShowLager (_a_bas.a);
			  }
              if (LOWORD (wParam) == IDM_CH_PREISE)
              {
			      return ShowLief ();
			  }
              if (LOWORD (wParam) == IDM_QM)
			  {
                  return OnKey10 ();
			  }
			  if (LOWORD (wParam) == IDM_EXTRA_DATA)
			  {
			 	  return OnExtraData ();
			  }
              else if (LOWORD (wParam) == IDM_INFO)
			  {
                  PostMessage (NULL, WM_KEYDOWN, VK_F4, 0l);
                  return TRUE;
			  }
              else if (LOWORD (wParam) == IDM_DELETE)
			  {
                   return OnRowDelete ();
			  }
              else if (LOWORD (wParam) == VK_INSERT)
			  {
                   return OnKey6 ();
			  }
              if (HIWORD (wParam) == WM_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              if (HIWORD (wParam) == EN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == CBN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == WM_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == LBN_SELCHANGE)
              {
                  return ListChanged (wParam, lParam);
              }
              else if (HIWORD (wParam) == EN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == CBN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == BN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == BN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
			  else if (OldDlg != 0)
			  {
				  return OldDlg->OnCommand (hWnd, msg, wParam, lParam);
			  }
        }
        return DLG::OnCommand (hWnd, msg, wParam, lParam);
}

void WeDlgLst::FillListCaption (void)
{
		char dest [0x1000];
		form *frm;
		int diff = 6;
		HWND lBox;


		diff = WeWork::ListDiff;
        lBox = _fList[0]->GethWnd ();
        if (lBox == NULL) return;

		frm = &LISTDLG::UbForm;
        FillFrmTitle (frm, dest, diff, 0x1000);
        SendMessage (lBox, LB_TITLE, -1,
                                (LPARAM) (char *) dest);

		frm = &LISTDLG::LineForm;
        FillFrmLines (frm, dest, diff, 0x1000);
        SendMessage (lBox, LB_VPOS, 3, (LPARAM) dest);

		frm = &LISTDLG::DataForm;
        FillFrmPos (frm, dest, diff, 0x1000);
        SendMessage (lBox, LB_RPOS, 0, (LPARAM) dest);


		frm = &LISTDLG::DataForm;
        FillFrmAttr (frm, dest, 0x1000);
        SendMessage (lBox, LB_ROWATTR, 0,
                           (LPARAM) (char *) dest);
		ReadPos ();
		fWork->SetText ();
}

void WeDlgLst::FillRow (int idx)
{
		char dest [0x1000];
		form *datafrm;
		int diff = 6;

		diff = WeWork::ListDiff;
 	    datafrm  = &LISTDLG::DataForm;
        FillFrmData (datafrm, dest, diff, 0x1000);
        if (Work != NULL)
        {
                Work->SetAktMe (ratod (LISTDLG::Lsts.me));
//240409                Work->SetAktBsdMe (Work->GetMeVgl (
//240409                                   ratod (LISTDLG::Lsts.a),LISTDLG::Lsts.lief_best, ratod (LISTDLG::Lsts.me), atoi (LISTDLG::Lsts.me_einh)));
				//290409 ist n�tig zum best�cken von _a_bas z.b.  
                Work->GetMeVgl (ratod (LISTDLG::Lsts.a),LISTDLG::Lsts.lief_best, ratod (LISTDLG::Lsts.me), atoi (LISTDLG::Lsts.me_einh));

        }
        SendMessage (_fList[0]->GethWnd (), LB_INSERTSTRING, idx,
                           (LPARAM) (char *) dest);

		//Detlef 011208
		   if (_a_bas.charg_hand == 1)
		   {
              fPos.GetCfield ("ls_charge")->Enable (TRUE);
		   }
		   else
		   {
              fPos.GetCfield ("ls_charge")->Enable (FALSE);
		   }
		   if (_a_bas.charg_hand == 9 ||_a_bas.zerl_eti == 1)
		   {
              fPos.GetCfield ("ls_ident")->Enable (TRUE);
		   }
		   else
		   {
              fPos.GetCfield ("ls_ident")->Enable (FALSE);
		   }

//        Work->ReadA (ratod (LISTDLG::Lsts.a));
	    if (_a_bas.charg_hand == 1 || _a_bas.charg_hand == 9 || _a_bas.zerl_eti == 1)
		{
            EnableMenuItem (ActiveDlg->GethMenu (),   IDM_EXTRA_DATA, MF_ENABLED);
		}
	    else
		{
            EnableMenuItem (ActiveDlg->GethMenu (),   IDM_EXTRA_DATA, MF_GRAYED);
		}
}

void WeDlgLst::FillRow (char *dest)
{
		form *datafrm;
		int diff = 6;

		diff = WeWork::ListDiff;
 	    datafrm  = &LISTDLG::DataForm;
        FillFrmData (datafrm, dest, diff, 0x1000);
}

void WeDlgLst::ReadPos (void)
{
	   int i;
       char datum [12];
       long ldat;
	   static struct LSTS LstsNull;

	   if (Work == NULL) return;

	   i = 0;
       AppendMode = InsMode = FALSE;
	   Work->OpenLst ();
	   while (Work->FetchLst () == 0)
	   {
		   uebertragen ();
           sysdate (datum);
           ldat = dasc_to_long (datum);
// Default f�r Haltbarketstage aus a_bas
           sprintf (LISTDLG::Lsts.mhd, "%hd", _a_bas.hbk_ztr);
// Haltbarketstage aus Haltbarkeitsdatum berechnen
           if (we_jour.hbk_dat >= ldat)
           {
                  sprintf (LISTDLG::Lsts.mhd, "%hd", we_jour.hbk_dat - ldat);
				  dlong_to_asc (we_jour.hbk_dat, LISTDLG::Lsts.hbk_dat);
           }
		   memcpy (&LISTDLG::LstTab[i], &LISTDLG::Lsts, sizeof (LISTDLG::Lsts));
		 

		   if (_a_bas.charg_hand == 9 ||_a_bas.zerl_eti == 1)
		   LadeExtraData(i);//Detlef 011208 ExtraDaten f�llen 
		   FillRow (-1);
		   i ++;
	   }

       if (i == 0)
       {
 		      memcpy (&LISTDLG::Lsts, &LstsNull, sizeof (LISTDLG::Lsts));
              memcpy (&LISTDLG::LstTab[i],
		              &LISTDLG::Lsts, sizeof (LISTDLG::Lsts));
	          InsertRow ();
       	      AppendMode = TRUE;
              fPos.GetCfield ("a")->Enable (TRUE);
              fPos.GetCfield ("scanfeld")->Enable (TRUE);
              fPos.GetCfield ("a_choise")->Enable (TRUE);

       }
       else
       {
		//Detlef 011208
        if (ratod (LISTDLG::Lsts.a) == 0.0)  //240309
        {
                 fPos.GetCfield ("a")->Enable (TRUE);
                 fPos.GetCfield ("scanfeld")->Enable (TRUE);
                 fPos.GetCfield ("a_choise")->Enable (TRUE);
 		      memcpy (&LISTDLG::Lsts, &LstsNull, sizeof (LISTDLG::Lsts));
              memcpy (&LISTDLG::LstTab[i],
		              &LISTDLG::Lsts, sizeof (LISTDLG::Lsts));
	          InsertRow ();
       	      AppendMode = TRUE;
                 fPos.SetCurrentName ("scanfeld");
				 return;
        }
		   if (_a_bas.charg_hand == 1)
		   {
              fPos.GetCfield ("ls_charge")->Enable (TRUE);
		   }
		   else
		   {
              fPos.GetCfield ("ls_charge")->Enable (FALSE);
		   }
		   if (_a_bas.charg_hand == 9 ||_a_bas.zerl_eti == 1)
		   {
              fPos.GetCfield ("ls_ident")->Enable (TRUE);
		   }
		   else
		   {
              fPos.GetCfield ("ls_ident")->Enable (FALSE);
		   }

              SelectRow ();
              memcpy (&LISTDLG::Lsts, &LISTDLG::LstTab[0], sizeof (LISTDLG::Lsts));
	          FillRow (0);
			fPos.GetCfield ("a")->Enable (FALSE);
			fPos.GetCfield ("scanfeld")->Enable (FALSE);
			fPos.GetCfield ("a_choise")->Enable (FALSE);
       }

// TODO	   if _a_bas
}

void WeDlgLst::uebertragen (void)
{
    Work->SetLsts (&LISTDLG::Lsts);
    Work->FillRow ();
    if (strcmp (clipped (LISTDLG::Lsts.ls_ident), " ") <= 0 &&
        strcmp (DefaultIdent, " ") > 0)
    {
        strcpy (LISTDLG::Lsts.ls_ident, DefaultIdent);
    }
    if (strcmp (clipped (LISTDLG::Lsts.ls_charge), " ") <= 0 &&
        strcmp (DefaultCharge, " ") > 0)
    {
        strcpy (LISTDLG::Lsts.ls_charge, DefaultCharge);
    }
	//240409
         Work->BucheBsd (ratod (LISTDLG::Lsts.a),
							LISTDLG::Lsts.lief_best,
                         ratod (LISTDLG::Lsts.me ) * -1,
                         atoi (LISTDLG::Lsts.me_einh),
                         ratod (LISTDLG::Lsts.pr_ek),
						 LISTDLG::Lsts.ls_charge,
						 LISTDLG::Lsts.ls_ident,
                         LISTDLG::Lsts.lager
						 );

}

void WeDlgLst::IdentCharge (void)
{
    Work->SetLsts (&LISTDLG::Lsts);
    if (strcmp (clipped (LISTDLG::Lsts.ls_ident), " ") <= 0 &&
        strcmp (DefaultIdent, " ") > 0)
    {
        strcpy (LISTDLG::Lsts.ls_ident, DefaultIdent);
    }
    if (strcmp (clipped (LISTDLG::Lsts.ls_charge), " ") <= 0 &&
        strcmp (DefaultCharge, " ") > 0)
    {
        strcpy (LISTDLG::Lsts.ls_charge, DefaultCharge);
    }
}

int WeDlgLst::WriteRow (void)
{
  	    static struct LSTS LstsNull;

        HWND lBox = fWork->GetCfield ("Liste")->GethWnd ();
        if (lBox == NULL) return 0;

        FillOk = TRUE;
	    InsMode = FALSE;
        if ((syskey == KEYCR || syskey == KEYDOWN) &&
             AppendMode)
        {
            FillRow ();
            int idx   = SendMessage (fWork->GetCfield ("Liste")->GethWnd (), LB_GETCURSEL, 0, 0l);
            AppendRow (idx);
 	        memcpy (&LISTDLG::Lsts, &LstsNull, sizeof (LISTDLG::Lsts));
            memcpy (&LISTDLG::LstTab[idx + 1],
		              &LISTDLG::Lsts, sizeof (LISTDLG::Lsts));
            FillRow (idx + 1);
            fPos.SetText ();
             fPos.SetCurrentName ("scanfeld"); //160109
//            InsertRow ();
            return 0;
        }
        AppendMode = FALSE;
        FillRow ();
        if (ratod (LISTDLG::Lsts.a) == 0.0)  //240309
		{
			fPos.GetCfield ("a")->Enable (FALSE);
			fPos.GetCfield ("scanfeld")->Enable (FALSE);
			fPos.GetCfield ("a_choise")->Enable (FALSE);
		}
        WriteOK = FALSE;
        if (FillOk == FALSE)
        {
            return -1;
        }
        return 0;
}

void WeDlgLst::FillRow (void)
{
        int idx;

		fPos.GetText ();
        if (fWork->GetCfield ("Liste")->GethWnd () == NULL) return;

        idx = SendMessage (fWork->GetCfield ("Liste")->GethWnd (), LB_GETCURSEL, 0, 0l);
        if (idx < 0) return;

 	    memcpy (&LISTDLG::LstTab[idx], &LISTDLG::Lsts, sizeof (LISTDLG::Lsts));
        if (Work != NULL)
        {
           Work->SetLsts (&LISTDLG::Lsts);

           if (Work->UpdateKontrakt (ratod (LISTDLG::Lsts.a)) == -1)
           {
                sprintf (LISTDLG::Lsts.me, "%lf", Work->GetAktMe ());
                memcpy (&LISTDLG::LstTab [idx], &LISTDLG::Lsts, sizeof (LISTDLG::Lsts));
                fPos.GetCfield ("me")->SetText ();
                fPos.SetCurrentName ("me");
                FillOk = FALSE;
                return;
           }
         }
		/***** 240409
         Work->BucheBsd (ratod (LISTDLG::Lsts.a),
							LISTDLG::Lsts.lief_best,
                         ratod (LISTDLG::Lsts.me),
                         atoi (LISTDLG::Lsts.me_einh),
                         ratod (LISTDLG::Lsts.pr_ek),
						 LISTDLG::Lsts.ls_charge,
						 LISTDLG::Lsts.ls_ident);
        ***********/
		 FillRow (idx);
}

void WeDlgLst::FillEnterList (int idx)
{
        if (fWork->GetCfield ("Liste")->GethWnd () == NULL) return;
 	    memcpy (&LISTDLG::Lsts, &LISTDLG::LstTab[idx], sizeof (LISTDLG::Lsts));
		FillRow (idx);
		fWork->SetText ();
        fWork->SetCurrentName ("Liste");
        if (AppendMode)
        {
//             fPos.SetCurrentName ("a");
             fPos.SetCurrentName ("scanfeld"); //160109
        }
        else
        {
             fPos.SetCurrentName ("me");
        }
}

BOOL WeDlgLst::NewArt (void)
{
	   int i;
       int ListRows;

//       return TRUE;
       HWND lBox = fWork->GetCfield ("Liste")->GethWnd ();
       if (lBox == NULL) return FALSE;
       int idx = SendMessage (fWork->GetCfield ("Liste")->GethWnd (), LB_GETCURSEL, 0, 0l);

	   ListRows = GetListRows ();
	   for (i = 0; i < ListRows; i ++)
	   {
		   if ( ratod (LISTDLG::LstTab[i].a) == ratod (LISTDLG::Lsts.a)  && 
			    strcmp (clipped(LISTDLG::LstTab[i].lief_best), clipped(LISTDLG::Lsts.lief_best)) == 0 &&
			    strcmp (clipped(LISTDLG::LstTab[i].ls_charge), clipped(LISTDLG::Lsts.ls_charge)) == 0 &&
			    strcmp (clipped(LISTDLG::LstTab[i].ls_ident), clipped(LISTDLG::Lsts.ls_ident)) == 0 &&
				idx != i
				)
		   {
/*** soll nicht dazu addiert werden (man kanns evt noch einstellbar machen TODO */
//	         if (abfragejn (hMainWindow, "Charge schon vorhanden , Menge dazu addieren ?", "J"))
//			 {
			 if (add_me == 1)
			 {
				 /**** 240409
		         Work->BucheBsd (ratod (LISTDLG::Lsts.a),   //140409
							LISTDLG::Lsts.lief_best,
                         ratod (LISTDLG::Lsts.me),
                         atoi (LISTDLG::Lsts.me_einh),
                         ratod (LISTDLG::Lsts.pr_ek),
						 LISTDLG::Lsts.ls_charge,
						 LISTDLG::Lsts.ls_ident);
                ***************/
			      double menge = ratod(LISTDLG::LstTab[i].me) + ratod(LISTDLG::Lsts.me);
				  sprintf(LISTDLG::LstTab[i].me, "%.3lf",menge);
				  sprintf(LISTDLG::Lsts.me, "%.3lf",menge);
				  fPos.GetText(); 

                  SendMessage (lBox, LB_SETCURSEL, (WPARAM) i, (LPARAM) 0l);
                  SendMessage (lBox, LB_SETCURSEL, (WPARAM) ListRows - 1, (LPARAM) 0l);
		          OnRowDelete ();
                  return FALSE;
			 }
/***/
		   }
	   }
	   return TRUE;
}

void WeDlgLst::Scroll (int mode, int idx)
{
       int ListRows;
	   int AktRow;
	   int i;

//       AktRow   = GetAktRow ();
       AktRow = idx;
	   if (AktRow == -1) return;
	   ListRows = GetListRows ();
	   if (mode == SCINSERT)
	   {
              for (i = ListRows; i > AktRow; i --)
			  {
                  memcpy (&LISTDLG::LstTab[i], &LISTDLG::LstTab[i - 1],
					      sizeof (LISTDLG::Lsts));
			  }
              memcpy (&LISTDLG::LstTab[i], &LISTDLG::Lsts, sizeof (LISTDLG::Lsts));
	   }
	   if (mode == SCDELETE)
	   {
              for (i = AktRow; i < ListRows; i ++)
			  {
                  memcpy (&LISTDLG::LstTab[i], &LISTDLG::LstTab[i + 1],
					      sizeof (LISTDLG::Lsts));
			  }
              AktRow   = GetAktRow ();
              if (AktRow >= 0)
              {
                        memcpy (&LISTDLG::Lsts, &LISTDLG::LstTab[AktRow],
                            sizeof (LISTDLG::Lsts));
                        ActiveWeDlgLst->FillRow (ActiveWeDlgLst->GetAktRow ());
              }
	          fWork->SetText ();
	   }
}

int WeDlgLst::AfterLsCharge (void)
{
/*
       if (_fList[0]->GethWnd () != NULL)
       {
                PostMessage (ActiveDlg->GethWnd (), WM_KEYDOWN, VK_DOWN, 0l);
                return 1;
       }
*/
	   InsMode = FALSE;
	   ActiveWeDlgLst->OnKeyDown ();
       return 0;
}

BOOL WeDlgLst::PruefeRec (int i)
{
    Work->SetLsts (&LISTDLG::LstTab[i]);
    if (ratod (LISTDLG::LstTab[i].me) == 0.0 && ratod (LISTDLG::LstTab[i].a) != 0.0) //260109
    {
        return FALSE;
    }
	return TRUE;
}


void WeDlgLst::ReadExtraData (int ListPos)
{
	ExtraDataItem = ExtraData.Find (ListPos);
	if (ExtraDataItem == NULL) 
	{
		return;
	}

//    SCharge *Charge = ExtraDataItem->Data.Get (0);
    Work->WriteExtraData (&ExtraDataItem->Data);
}

void WeDlgLst::ReadExtraData (void)   //Detlef 011208 
{
    int ListPos   = SendMessage (fWork->GetCfield ("Liste")->GethWnd (), LB_GETCURSEL, 0, 0l);
	ExtraDataItem = ExtraData.Find (ListPos);
	if (ExtraDataItem == NULL) 
	{
		return;
	}
	strcpy (LISTDLG::Lsts.ls_ident, ExtraDataItem->Data.IdentNr);
    fPos.SetText ();

}





void WeDlgLst::WriteRec (int i)
{
    Work->SetLsts (&LISTDLG::LstTab[i]);
    if (ratod (LISTDLG::LstTab[i].a) == 0.0 ) //260109
    {
        return;
    }
    if (ratod (LISTDLG::LstTab[i].me) == 0.0 && flgLoesch == TRUE)
    {
        return;
    }

	//240409
         Work->BucheBsd (ratod (LISTDLG::LstTab[i].a),
							LISTDLG::LstTab[i].lief_best,
                         ratod (LISTDLG::LstTab[i].me),
                         atoi (LISTDLG::LstTab[i].me_einh),
                         ratod (LISTDLG::LstTab[i].pr_ek),
						 LISTDLG::LstTab[i].ls_charge,
						 LISTDLG::LstTab[i].ls_ident,
                         LISTDLG::Lsts.lager
						 );


	ReadExtraData (i);
    Work->WriteWeJour (&Wejour, i);
    we_kopf.fil_ek_wrt += we_jour.pr_fil_ek * we_jour.me * we_jour.inh;
    we_kopf.fil_ek_wrt_eu += we_jour.pr_fil_ek_eu * we_jour.me * we_jour.inh;
    we_kopf.vk_wrt += we_jour.pr_vk * we_jour.me * we_jour.inh;
    we_kopf.vk_wrt_eu += we_jour.pr_vk_eu * we_jour.me * we_jour.inh;
}

BOOL WeDlgLst::OnKeyUp ()
{
      HWND hWnd;

      hWnd = GetFocus ();

      if (hWnd == fPos.GetCfield ("a")->GethWnd ())
	  {
          fPos.GetText ();
          if (ratod (LISTDLG::Lsts.a) == 0.0)
		  {
//           disp_mess ("Artikel-Nummer > 0 eingeben", 2);
//           fWork->SetCurrentName ("Liste");
           fPos.SetCurrentName ("scanfeld");
           return TRUE;
		  }
	  }
      if (hWnd == fPos.GetCfield ("ls_charge")->GethWnd ())
	  {
          fPos.GetText ();
          if (ratod (LISTDLG::Lsts.a) == 0.0)
		  {
//           disp_mess ("Artikel-Nummer > 0 eingeben", 2);
//           fWork->SetCurrentName ("Liste");
           fPos.SetCurrentName ("scanfeld");
           return TRUE;
		  }
	      if ( ChargeZwang && strlen(clipped(LISTDLG::Lsts.ls_charge)) == 0.0)
		  {
            fPos.SetCurrentName ("ls_charge");
            return TRUE;
		  }

	  }

        HWND lBox = fWork->GetCfield ("Liste")->GethWnd ();
        if (lBox == NULL) return 0;

        syskey = KEYUP;
        int idx = SendMessage (lBox, LB_GETCURSEL, (WPARAM) 0, (LPARAM) 0l);
        if (idx == 0)
        {
                    AppendMode = FALSE;
	//             fPos.SetCurrentName ("a");
		         fPos.SetCurrentName ("scanfeld"); //160109
                    return TRUE;
        }
        else if (AppendMode)
        {
            AppendMode = FALSE;
	        if (ratod (LISTDLG::Lsts.a) == 0.0)  //240309
			{
				fPos.GetCfield ("a")->Enable (FALSE);
				fPos.GetCfield ("scanfeld")->Enable (FALSE);
				fPos.GetCfield ("a_choise")->Enable (FALSE);
	            fPos.SetCurrentName ("me");
			}
            fPos.SetCurrentName ("me");
            if (ratod (LISTDLG::Lsts.a) == 0.0)
            {
                   DeleteRow ();
            }
            return TRUE;
        }

        int ret = DlgLst::OnKeyUp ();

        return ret;
}

BOOL WeDlgLst::OnKeyDown ()
{
        if (ratod (LISTDLG::Lsts.a) == 0.0)
        {
            fPos.SetCurrentName ("me");
//             fPos.SetCurrentName ("a");
             fPos.SetCurrentName ("scanfeld"); //160109
            return TRUE;
        }
        if ( ChargeZwang && strlen(clipped(LISTDLG::Lsts.ls_charge)) == 0.0)
        {
	        fPos.GetText ();
            fPos.SetCurrentName ("ls_charge");
            return TRUE;
        }
		/* 100406
        if ( ratod (LISTDLG::Lsts.me) == 0.0)
        {
            disp_mess ("Menege > 0 eingeben", 2);
            fPos.SetCurrentName ("me");
            return TRUE;
        }
		*/
        if (AppendMode == FALSE)
        {
              int idx   = SendMessage (fWork->GetCfield ("Liste")->GethWnd (), LB_GETCURSEL, 0, 0l);
              int count = SendMessage (fWork->GetCfield ("Liste")->GethWnd (), LB_GETCOUNT, 0, 0l);
              if (idx >= count - 1)
              {
                  AppendMode = TRUE;
                  fPos.GetCfield ("a")->Enable (TRUE);
                  fPos.GetCfield ("scanfeld")->Enable (TRUE);
                  fPos.GetCfield ("a_choise")->Enable (TRUE);
              }
        }
        int ret = DlgLst::OnKeyDown ();
//             fPos.SetCurrentName ("a");
             fPos.SetCurrentName ("scanfeld"); //160109

        return ret;
}

BOOL WeDlgLst::OnKey4 ()
{
        syskey = KEY4;
        CallInfo ();
        return TRUE;
}

BOOL WeDlgLst::OnKey5 ()
{
        InsMode = FALSE;
        syskey = KEY5;
		InitScanFlags();
        if (abfragejn (hMainWindow, "Positionen speichern ?", "J"))
		{
              return OnKey12 ();
		}
		DestroyWindow ();
        return FALSE;
}


BOOL WeDlgLst::OnKey6 ()
{
 	    static struct LSTS LstsNull;

        if (ratod (LISTDLG::Lsts.a) == 0.0)
        {
            fPos.SetCurrentName ("me");
//             fPos.SetCurrentName ("a");
             fPos.SetCurrentName ("scanfeld"); //160109
            return TRUE;
        }
		/* 100406
        if ( ratod (LISTDLG::Lsts.me) == 0.0)
        {
            disp_mess ("Menege > 0 eingeben", 2);
            fPos.SetCurrentName ("me");
            return TRUE;
        }
		*/
        int idx   = SendMessage (fWork->GetCfield ("Liste")->GethWnd (), LB_GETCURSEL, 0, 0l);
        InsertRow (idx);
        memcpy (&LISTDLG::Lsts, &LstsNull, sizeof (LISTDLG::Lsts));
        memcpy (&LISTDLG::LstTab[idx],
		              &LISTDLG::Lsts, sizeof (LISTDLG::Lsts));
        FillRow (idx);
        fPos.SetText ();
        AppendMode = TRUE;
        fPos.GetCfield ("a")->Enable (TRUE);
        fPos.GetCfield ("scanfeld")->Enable (TRUE);
        fPos.GetCfield ("a_choise")->Enable (TRUE);
//             fPos.SetCurrentName ("a");
             fPos.SetCurrentName ("scanfeld"); //160109
        return TRUE;
}

BOOL WeDlgLst::OnKey7 ()
{
        syskey = KEY7;
        return OnRowDelete ();
}

BOOL WeDlgLst::OnKey10 (void)
{
	if (Dlg == NULL) return FALSE;

	fPos.GetText ();
    if (ratod (LISTDLG::Lsts.a) == 0.0)
	{
			 return FALSE;
	}
    if (atoi (LISTDLG::Lsts.p_nr) == 0)
    {
             sprintf (LISTDLG::Lsts.p_nr, "%d", GetAktRow ());
    }
    Work->SetLsts (&LISTDLG::Lsts);
    ((WeDlg *) Dlg)->EnterQmPos (hWnd);
    GetCurrentCfield ()->SetFocus ();
    return FALSE;
}


BOOL WeDlgLst::OnPreTranslateMessage (MSG *msg)    //TODO : Massageschleife zum Ersetzen 
{
     switch (msg->message)
     {


	 case WM_KEYDOWN:
 // Detlef RoW 22.02.2005 Ean128 mit Endezeichen TODO
			  if (msg->message == WM_KEYDOWN )
			  {

				  int key = ChangeChar (msg->wParam);
					if (key != (int) msg->wParam)
					{
						SendMessage (msg->hwnd, WM_CHAR, key, 0l);
					}
			  }
// Detlef RoW 22.02.2005 Ean128 mit Endezeichen Ende

		 if (msg->wParam == 'Z' || msg->wParam == 'z')
		 {
                  if (GetKeyState (VK_CONTROL) < 0)
				  {
					  OnExtraData ();
					  return TRUE;

				  }
		 }
	 }
	 return FALSE;
}

void WeDlgLst::LadeExtraData (void) //Detlef 011208
{
	if (strlen(clipped(LISTDLG::Lsts.ls_ident)) == 0) return;
    int ListPos   = SendMessage (fWork->GetCfield ("Liste")->GethWnd (), LB_GETCURSEL, 0, 0l);
	ExtraDataItem = ExtraData.Find (ListPos);
    fPos.GetText ();
	if (ExtraDataItem != NULL)
	{
//			ExtraData.Drop (*ExtraDataItem);
		strcpy (ExtraDataItem->Data.IdentNr,LISTDLG::Lsts.ls_ident);
		strcpy (ExtraDataItem->Data.ESNr,lief.esnum);
		strcpy (ExtraDataItem->Data.EZNr,lief.eznum);
		ExtraData.Add (ExtraDataItem);

	}
	else
	{
		CWeExtraData NewExtraDataItem;
		NewExtraDataItem.ListPos = ListPos;
       		if (Work->ReadExtraData ())
		{
			strcpy (NewExtraDataItem.Data.IdentNr,zerldaten.ident_extern);
			strcpy (NewExtraDataItem.Data.ESNr,zerldaten.esnum);
			strcpy (NewExtraDataItem.Data.EZNr,zerldaten.eznum1);
			Work->ReadCountries ();
			strcpy (NewExtraDataItem.Data.Countries,rind.ls_land);
		}
		else
		{
			strcpy (NewExtraDataItem.Data.IdentNr,LISTDLG::Lsts.ls_ident);
			strcpy (NewExtraDataItem.Data.ESNr,lief.esnum);
			strcpy (NewExtraDataItem.Data.EZNr,lief.eznum);
		}
		ExtraData.Add (NewExtraDataItem);

	}

}
void WeDlgLst::LadeExtraData (int ListPos) //Detlef 011208
{
	if (strlen(clipped(LISTDLG::Lsts.ls_ident)) == 0) return;
	ExtraDataItem = ExtraData.Find (ListPos);
	if (ExtraDataItem != NULL)
	{
//			ExtraData.Drop (*ExtraDataItem);
		strcpy (ExtraDataItem->Data.IdentNr,LISTDLG::Lsts.ls_ident);
		strcpy (ExtraDataItem->Data.ESNr,lief.esnum);
		strcpy (ExtraDataItem->Data.EZNr,lief.eznum);
		ExtraData.Add (ExtraDataItem);

	}
	else
	{
		CWeExtraData NewExtraDataItem;
		NewExtraDataItem.ListPos = ListPos;
        	if (Work->ReadExtraData ())
		{
			strcpy (NewExtraDataItem.Data.IdentNr,zerldaten.ident_extern);
			strcpy (NewExtraDataItem.Data.ESNr,zerldaten.esnum);
			strcpy (NewExtraDataItem.Data.EZNr,zerldaten.eznum1);
			Work->ReadCountries ();
			strcpy (NewExtraDataItem.Data.Countries,rind.ls_land);
		}
		else
		{
			strcpy (NewExtraDataItem.Data.IdentNr,LISTDLG::Lsts.ls_ident);
			strcpy (NewExtraDataItem.Data.ESNr,lief.esnum);
			strcpy (NewExtraDataItem.Data.EZNr,lief.eznum);
		}
		ExtraData.Add (NewExtraDataItem);

	}
}

BOOL WeDlgLst::OnExtraData ()
{
	if (Dlg == NULL) return FALSE;

	fPos.GetText ();
    if (ratod (LISTDLG::Lsts.a) == 0.0)
	{
			 return FALSE;
	}
	if (_a_bas.charg_hand != 1 && _a_bas.charg_hand != 9 && _a_bas.zerl_eti != 1)
	{
			 return FALSE;
	}
    int ListPos   = SendMessage (fWork->GetCfield ("Liste")->GethWnd (), LB_GETCURSEL, 0, 0l);
	ExtraDataItem = ExtraData.Find (ListPos);
	if (ExtraDataItem != NULL)
	{
		ExtraDataDialog.SetData (&ExtraDataItem->Data);		
		if (!ExtraDataDialog.DoModal ())
		{
			ExtraData.Drop (*ExtraDataItem);
		}
	}
	else
	{
		CWeExtraData NewExtraDataItem;
		NewExtraDataItem.ListPos = ListPos;
		ExtraDataDialog.SetData (&NewExtraDataItem.Data);		
		if (ExtraDataDialog.DoModal ())
		{
			ExtraData.Add (NewExtraDataItem);
		}
	}
	ReadExtraData();
	return TRUE;
}

BOOL WeDlgLst::OnKey12 (void)
{
    int i;
    int recanz;
    int dsqlstatus;
	char text [256];

    fPos.GetText ();
	InitScanFlags();
    i = SendMessage (fWork->GetCfield ("Liste")->GethWnd (), LB_GETCURSEL, 0, 0l);
    if (i >= 0)
	{
 	    memcpy (&LISTDLG::LstTab[i], &LISTDLG::Lsts, sizeof (LISTDLG::Lsts));
	}
    if (ratod (LISTDLG::Lsts.a) == 0.0)
    {
        OnKeyDelete ();
    }
    else
    {
		/*********** 240409
         Work->BucheBsd (ratod (LISTDLG::Lsts.a),
							LISTDLG::Lsts.lief_best,
                         ratod (LISTDLG::Lsts.me),
                         atoi (LISTDLG::Lsts.me_einh),
                         ratod (LISTDLG::Lsts.pr_ek),
						 LISTDLG::Lsts.ls_charge,
						 LISTDLG::Lsts.ls_ident);
         ***************/
        memcpy (&lief_bzg, &lief_bzg_null, sizeof (struct LIEF_BZG));
        lief_bzg.mdn = we_kopf.mdn;
        lief_bzg.fil = we_kopf.fil;
        strcpy (lief_bzg.lief, we_kopf.lief);
        dsqlstatus = Work->ReadA (ratod (LISTDLG::Lsts.a));
        if (dsqlstatus == 100)
        {
              OnKeyDelete ();
        }
    }

    recanz = GetListRows ();
	int danz0 = 0;
    for (i = 0; i < recanz; i ++)
    {
        if (PruefeRec (i) == FALSE) danz0++;
    }
	flgLoesch = FALSE;
	if (danz0 > 0)
	{
		sprintf (text,"Es gibt %d Position(en) mit Menge 0\nSollen diese gel�scht werden ?",danz0);
	    if (abfragejn (hMainWindow, text, "J"))
		{
			flgLoesch = TRUE;
		}
	}

    Work->DeleteAllPos (we_kopf.mdn,
                        we_kopf.fil,
                        we_kopf.lief,
                        we_kopf.lief_rech_nr,
                        we_kopf.blg_typ);
    we_kopf.rab_eff = 0.0;
    we_kopf.rab_eff_eu = 0.0;
    we_kopf.fil_ek_wrt = 0.0;
    we_kopf.fil_ek_wrt_eu = 0.0;
    we_kopf.vk_wrt = 0.0;
    we_kopf.vk_wrt_eu = 0.0;
    for (i = 0; i < recanz; i ++)
    {
        WriteRec (i);
    }
/*
	if (Emb !=NULL)
	{
		DestroyEmb ();
	}
*/
    DestroyWindow ();
    return TRUE;
}

BOOL WeDlgLst::OnRowDelete ()
{
//        CFIELD *Cfield;
 	    static struct LSTS LstsNull;


        syskey = KEY7;
		InitScanFlags();
		/**** 240409
         Work->SetAktBsdMe (0.0);  
         Work->BucheBsd (ratod (LISTDLG::Lsts.a),
							LISTDLG::Lsts.lief_best,
                         ratod (LISTDLG::Lsts.me) * -1,
                         atoi (LISTDLG::Lsts.me_einh),
                         ratod (LISTDLG::Lsts.pr_ek),
						 LISTDLG::Lsts.ls_charge,
						 LISTDLG::Lsts.ls_ident);
	  **************/

        DlgLst::OnRowDelete ();
        int idx   = SendMessage (fWork->GetCfield ("Liste")->GethWnd (), LB_GETCURSEL, 0, 0l);
        if (idx > -1)
        {
             memcpy (&LISTDLG::LstTab[idx], &LISTDLG::Lsts,
		                       sizeof (LISTDLG::Lsts));
        }
        fPos.SetText ();
        if (SendMessage (fWork->GetCfield ("Liste")->GethWnd (), LB_GETCOUNT, 0, 0l) == 0)
        {
                   memcpy (&LISTDLG::Lsts, &LstsNull, sizeof (LISTDLG::Lsts));
                   fWork->SetText ();
                   InsertRow ();
        }
/***** 240309
        if (ratod (LISTDLG::Lsts.a) == 0.0)
        {
                 fPos.GetCfield ("a")->Enable (TRUE);
                 fPos.GetCfield ("scanfeld")->Enable (TRUE);
                 fPos.GetCfield ("a_choise")->Enable (TRUE);
//                 fPos.SetCurrentName ("a");
                 fPos.SetCurrentName ("scanfeld");
        }
        else
        {
                 fPos.SetCurrentName ("me");
                 fPos.GetCfield ("a")->Enable (FALSE);
                 fPos.GetCfield ("scanfeld")->Enable (FALSE);
                 fPos.GetCfield ("a_choise")->Enable (FALSE);
        }
****************/
//        AppendMode = FALSE;
        AppendMode = TRUE;
        return TRUE;
}


int WeDlgLst::ShowA ()
{
        struct SA *sa;

		LISTDLG::SearchA.SetWindowStyle (WS_POPUP | WS_THICKFRAME);
        LISTDLG::SearchA.SetParams (DLG::hInstance, hMainWindow);
//        LISTDLG::SearchA.Setawin (hWnd);
        LISTDLG::SearchA.Setawin (GetParent (GetParent (hMainWindow)));
        LISTDLG::SearchA.SearchA ();
        if (syskey == KEY5)
        {
            GetCurrentCfield ()->SetFocus ();
            return 0;
        }
        sa = LISTDLG::SearchA.GetSa ();
        if (sa == NULL)
        {
            GetCurrentCfield ()->SetFocus ();
            return 0;
        }

        strcpy (LISTDLG::Lsts.a, sa->a);
	    fPos.SetText ();
//        GetCurrentCfield ()->SetFocus ();
        fPos.SetCurrentName ("a");
        PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
        return 0;
}



int WeDlgLst::ShowBzg ()
{
        struct SBZG *sbzg;
        SEARCHBZG SearchBzg;
        char buffer [256];

//        sprintf (buffer, "lief_bzg.lief = \"%s\"", clipped (we_kopf.lief));
        sprintf (buffer, "lief_bzg.lief = \"%s\" and lief_bzg.mdn = %hd "
			             "and lief_bzg.fil = %hd", 
						 clipped (we_kopf.lief), we_kopf.mdn, we_kopf.fil);
        SearchBzg.SetQuery (buffer);
        SearchBzg.SetWindowStyle (WS_POPUP | WS_THICKFRAME);
        SearchBzg.SetParams (DLG::hInstance, hMainWindow);
//        SearchBzg.Setawin (hWnd);
        SearchBzg.Setawin (GetParent (GetParent (hMainWindow)));
        SearchBzg.Search (we_kopf.mdn);
        if (syskey == KEY5)
        {
            GetCurrentCfield ()->SetFocus ();
            return 0;
        }
        sbzg = SearchBzg.GetSBzg ();
        if (sbzg == NULL)
        {
            return 0;
        }
        Validate ();
        strcpy (LISTDLG::Lsts.a, sbzg->a);
        strcpy (LISTDLG::Lsts.lief_best, sbzg->lief_best);
	    fPos.SetText ();
//        GetCurrentCfield ()->SetFocus ();
        fPos.SetCurrentName ("a");
        PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
        return 0;
}



BOOL WeDlgLst::ShowMeEinh (void)
{
    Work->SetLsts (&LISTDLG::Lsts);
    EnableWindow (hWnd, FALSE);
    Work->ChMeEinh (hWndMain);
    EnableWindow (hWnd, TRUE);
    fPos.SetText ();
    GetCurrentCfield ()->SetFocus ();
    return 0;
}

BOOL WeDlgLst::ShowLager (double a)
{
    Work->SetLsts (&LISTDLG::Lsts);
    EnableWindow (hWnd, FALSE);
    Work->ShowLager (hWndMain,a);
    EnableWindow (hWnd, TRUE);
    fPos.SetText ();
    GetCurrentCfield ()->SetFocus ();
    return 0;
}

BOOL WeDlgLst::OnKey9 (void)
{
    CFIELD *Cfield;
    HWND hWnd;

	syskey = KEY9;
    hWnd = GetFocus ();
    if (hWnd == fPos.GetCfield ("a_choise")->GethWnd ())
    {
        if (fPos.GetCfield ("a")->IsDisabled ())
        {
            return TRUE;
        }
        if (WithSearchBest)
        {
            ShowBzg ();
        }
        else
        {
            ShowA ();
        }
        return TRUE;
    }

    Cfield = ActiveDlg->GetCurrentCfield ();
    if (strcmp (Cfield->GetName (), "a") == 0)
    {
        if (fPos.GetCfield ("a")->IsDisabled ())
        {
            return TRUE;
        }
        if (WithSearchBest)
        {
            ShowBzg ();
        }
        else
        {
            ShowA ();
        }
        return TRUE;
    }

    if (strcmp (Cfield->GetName (), "lief_best") == 0)
    {
        if (fPos.GetCfield ("lief_best")->IsDisabled ())
        {
            return TRUE;
        }
        ShowBzg ();
        return TRUE;
    }
    if (strcmp (Cfield->GetName (), "pr_ek") == 0)
    {
		ShowLief();
		return TRUE;
	}
    if (strcmp (Cfield->GetName (), "lager") == 0)
    {
		ShowLager(_a_bas.a);
		return TRUE;
	}

    return ShowMeEinh ();
}



int WeDlgLst::OnKeyTab (void)
{
	  return OnKeyReturn ();
}


void InitScanFlags (void)
{
	ArtikelUnvollstaendig = 0;
	ArtikelVorhanden = TRUE;
	MengeVorhanden = TRUE;
	ChargeVorhanden = TRUE;
	MHDVorhanden = TRUE;
	strcpy(A_INFO,"        ");
	strcpy(ME_INFO,"       ");
	strcpy(CHG_INFO,"      ");
	strcpy(ACHTUNG," ");
//	strcpy(MHD_INFO,"      ");
}
void PruefeScanFlags (void)
{
	ArtikelVorhanden = TRUE;
	MengeVorhanden = TRUE;
	ChargeVorhanden = TRUE;
	MHDVorhanden = TRUE;
	strcpy(A_INFO,"        ");
	strcpy(ME_INFO,"       ");
	strcpy(CHG_INFO,"      ");
	strcpy(ACHTUNG," ");
//	strcpy(MHD_INFO,"      ");
	if (strlen(clipped(LISTDLG::Lsts.ls_charge)) < 2) 
	{
		ChargeVorhanden = FALSE;
		strcpy(CHG_INFO,"Charge !");
		strcpy(ACHTUNG,"X");
	}
	if (lese_a_bas (ratod(LISTDLG::Lsts.a)) != 0)
	{
		ArtikelVorhanden = FALSE;
		strcpy(A_INFO,"Artikel !");
		strcpy(ACHTUNG,"X");
	}
	if (ratod(LISTDLG::Lsts.me) < 0.001) 
	{
		MengeVorhanden = FALSE;
		strcpy(ME_INFO,"Menge !");
		strcpy(ACHTUNG,"X");
	}
/****
	if (strlen(clipped(LISTDLG::Lsts.mhd)) < 1) 
	{
		MHDVorhanden = FALSE;
		strcpy(MHD_INFO,"MHD fehlt!");
	}
	****/

}

int WeDlgLst::OnKeyReturn (void)
{

      HWND hWnd;
	  int dret = 0;

      hWnd = GetFocus ();

	  GehezuScanfeld = FALSE;
      if (hWnd == fPos.GetCfield ("a")->GethWnd ())
	  {
          fPos.GetText ();
          if (ratod (LISTDLG::Lsts.a) == 0.0)
		  {
//           disp_mess ("Artikel-Nummer > 0 eingeben", 2);
//           fWork->SetCurrentName ("Liste");
           fPos.SetCurrentName ("scanfeld");
           return TRUE;
		  }
	  }
      if (hWnd == fPos.GetCfield ("scanfeld")->GethWnd ())
	  {
          fPos.GetText ();
          if (strlen(clipped(LISTDLG::Lsts.scanfeld)) == 0)
		  {
	           fPos.SetCurrentName ("a");
		       return TRUE;
		  }
          if (strlen(clipped(LISTDLG::Lsts.scanfeld)) < 10)
		  {
	           fPos.SetCurrentName ("scanfeld");
		       return TRUE;
		  }

		  fPos.SetText ();
		  if (ArtikelUnvollstaendig )
		  {
			  /***
			  if (ArtikelUnvollstaendig == 1)
			  {
	  			ArtikelUnvollstaendig = 0;
				dret = AfterScanfeldCharge();
				if (dret == 1)
				{
						AfterLsCharge ();
					  PruefeScanFlags();
				      fPos.SetText ();
					fPos.SetCurrentName ("scanfeld");
					return TRUE;
				}
				if (dret == 100)  //160109
				{
	//			        AfterLsCharge ();
					fPos.SetCurrentName ("ls_charge");
					return TRUE;
				}
			  }
			  else
			  {
			  *********/
				  ArtikelUnvollstaendig = 0;
				  AfterScanfeld();
				  PruefeScanFlags();
				  fPos.SetText ();
				if (ArtikelUnvollstaendig == 99)
				{
					fPos.SetCurrentName ("scanfeld");
					ArtikelUnvollstaendig = 0;
					return TRUE;
				}
				if (ArtikelUnvollstaendig == 1)
				{
//					fPos.SetCurrentName ("ls_charge");
					fPos.SetCurrentName ("scanfeld");
					ArtikelUnvollstaendig = 0;
					return TRUE;
				}
				if (ArtikelUnvollstaendig == 2)
				{
//					fPos.SetCurrentName ("me");
					fPos.SetCurrentName ("scanfeld");
					ArtikelUnvollstaendig = 0;
					return TRUE;
				}
				if (ArtikelUnvollstaendig == 3)
				{
//					fPos.SetCurrentName ("me");
					fPos.SetCurrentName ("scanfeld");
					ArtikelUnvollstaendig = 0;
					return TRUE;
				}
//			  }
		  }
		  else
		  {
			  AfterScanfeld();
			  PruefeScanFlags();
			  fPos.SetText ();
		  }
		  syskey = KEYCR;
		  if (ArtikelUnvollstaendig) 
		  {
	           fPos.GetCfield ("scanfeld")->SetText ();
		       fPos.SetCurrentName ("scanfeld");
			  return TRUE;
		  }
		   if (ArtikelUnvollstaendig == 0)
			{
//				Work->BucheBsd (ratod (LISTDLG::Lsts.a),
//                       ratod ("0"),
//                       atoi (LISTDLG::Lsts.me_einh),
//                       ratod (LISTDLG::Lsts.pr_ek));
		   }
		  fPos.SetText ();

		  if (InsMode == FALSE)
		  {
		        AfterLsCharge ();
	           fPos.SetCurrentName ("scanfeld");
		  }
		  else
		  {
			    WriteRow ();
		  }
           fPos.SetCurrentName ("scanfeld");

		   return TRUE;
	  }

      if (hWnd == fPos.GetCfield ("me")->GethWnd ())
	  {
          fPos.GetText ();
  	      ArtikelUnvollstaendig = 0;

          if (ratod (LISTDLG::Lsts.me) == 0.0)
		  {
           fPos.SetCurrentName ("me");
           return TRUE;
		  }
		  PruefeScanFlags();
	      fPos.SetText ();
	  }
      if (hWnd == fPos.GetCfield ("tara")->GethWnd ())
	  {
          fPos.GetText ();
          if (_a_bas.charg_hand == 9 || _a_bas.zerl_eti == 1)
		  {
           fPos.SetCurrentName ("ls_ident");
           return TRUE;
		  }
	  }

      if (hWnd == fPos.GetCfield ("ls_charge")->GethWnd ())
	  {
          fPos.GetText ();
	      if ( ChargeZwang && strlen(clipped(LISTDLG::Lsts.ls_charge)) == 0.0)
		  {
			    fPos.SetCurrentName ("ls_charge");
	            return TRUE;
		  }
		  if (ArtikelUnvollstaendig == 1)
		  {
		 	    ArtikelUnvollstaendig = 0;
			  if (syskey == KEYTAB && GetKeyState (VK_SHIFT) < 0)
			  {
				    return FALSE;
			  }
			  if (InsMode == FALSE)
			  {
			       AfterLsCharge ();
			    	return TRUE;
			  }
			  else
			  {
					syskey = KEYCR;
			 	    WriteRow ();
			  }
				GehezuScanfeld = TRUE;
		      PruefeScanFlags();
		      fPos.SetText ();
		  }
	  }
		



//      if (hWnd == fPos.GetCfield ("ls_charge")->GethWnd ())
      if (hWnd == fPos.GetCfield ("mhd")->GethWnd ())
	  {
		  if (syskey == KEYTAB && GetKeyState (VK_SHIFT) < 0)
		  {
			    return FALSE;
		  }
		  if (InsMode == FALSE)
		  {
		        AfterLsCharge ();
		        return TRUE;
		  }
		  else
		  {
			    WriteRow ();
		  }
 			GehezuScanfeld = TRUE;
		  PruefeScanFlags();
	      fPos.SetText ();
	  }
	  return FALSE;
}

int WeDlgLst::AfterLsIdent (void) //Detlef 011208
{
	ActiveWeDlgLst->LadeExtraData ();
	return 0;

}
int WeDlgLst::AfterMHD (void) 
{
	   long ldat, lsysdat;
	   char datum[12];
       if (strlen(clipped(LISTDLG::Lsts.hbk_dat)) < 6)
	   {
         if (!abfragejn (ActiveWeDlgLst->GethWnd (), "Es ist Kein Datum erfasst worden \nWeiter ?", "N"))
		  {
			  fPos.SetText ();
//              fWork->SetCurrentName ("Liste");
			  fPos.SetCurrentName ("mhd");
			  return TRUE;
		  }
	   }

	   sysdate(datum);
       lsysdat = dasc_to_long (datum);
       ldat = dasc_to_long (LISTDLG::Lsts.hbk_dat);
	   ldat = ldat - lsysdat;
	   sprintf (LISTDLG::Lsts.mhd, "%.d", ldat);
	return 0;

}

int WeDlgLst::AfterPrEk (void)
{
	   WeWork *Work;
	   CFORM *fWork;
	   Work = ActiveWeDlgLst->GetWork ();
	   fWork = &fWeLst0;
       Work->SetLsts (&LISTDLG::Lsts);

	   if (ratod (LISTDLG::Lsts.pr_ek) == double(0)  && we_fil_bel_par)
	   {
			          if (!abfragejn (ActiveWeDlgLst->GethWnd (), "Achtung ! Preis ist 0\nOK ?", "N"))
					  {
							  fPos.SetText ();
				              fWork->SetCurrentName ("Liste");
							  fPos.SetCurrentName ("pr_ek");
							  return TRUE;
					  }
					  else
					  {
							  fPos.SetText ();
				              fWork->SetCurrentName ("Liste");
							  fPos.SetCurrentName ("lief_best");
							  return FALSE;
					  }
	   }
       ActiveWeDlgLst->GetWork ()->SetLsts (&LISTDLG::Lsts);
       ActiveWeDlgLst->GetWork ()->CalcEkBto ();
       if (ActiveWeDlgLst->GetAktRow () < 0)
       {
           return 0;
       }
       memcpy (&LISTDLG::LstTab [ActiveWeDlgLst->GetAktRow ()],
		       &LISTDLG::Lsts, sizeof (LISTDLG::Lsts));
	   fPos.SetText ();
       return 0;
}
int WeDlgLst::AfterLager (void)
{
	   WeWork *Work;
	   CFORM *fWork;
	   Work = ActiveWeDlgLst->GetWork ();
	   fWork = &fWeLst0;
       Work->SetLsts (&LISTDLG::Lsts);

	   if (ratod (LISTDLG::Lsts.pr_ek) == double(0)  && we_fil_bel_par)
	   {
			          if (!abfragejn (ActiveWeDlgLst->GethWnd (), "Achtung ! Preis ist 0\nOK ?", "N"))
					  {
							  fPos.SetText ();
				              fWork->SetCurrentName ("Liste");
							  fPos.SetCurrentName ("pr_ek");
							  return TRUE;
					  }
					  else
					  {
							  fPos.SetText ();
				              fWork->SetCurrentName ("Liste");
							  fPos.SetCurrentName ("lief_best");
							  return FALSE;
					  }
	   }
       ActiveWeDlgLst->GetWork ()->SetLsts (&LISTDLG::Lsts);
       ActiveWeDlgLst->GetWork ()->CalcEkBto ();
       if (ActiveWeDlgLst->GetAktRow () < 0)
       {
           return 0;
       }
       memcpy (&LISTDLG::LstTab [ActiveWeDlgLst->GetAktRow ()],
		       &LISTDLG::Lsts, sizeof (LISTDLG::Lsts));
	   fPos.SetText ();
       return 0;
}

int WeDlgLst::BeforeA (void)
{
	if (GehezuScanfeld == TRUE)
	{
           fPos.SetCurrentName ("scanfeld");
	}
	GehezuScanfeld = FALSE;
	return TRUE;
}
int WeDlgLst::AfterA (void)
{
       int dsqlstatus;
	   WeWork *Work;
	   CFORM *fWork;

	   if (syskey == KEY5) return FALSE;
	   if (AktId == KEY5) return FALSE;
	   if (syskey == KEYUP) return FALSE;
	   if (syskey == KEY7) return FALSE;

       if (ratod (LISTDLG::Lsts.a) == 0.0)
	   {
		   return FALSE;
	   }
       if (ArtikelUnvollstaendig == 1)
	   {
		   return TRUE;
	   }
	   if (ActiveWeDlgLst->NewArt () == FALSE)
	   {
		   return FALSE;
	   }
	   Work = ActiveWeDlgLst->GetWork ();
	   fWork = &fWeLst0;
       sysdate (LISTDLG::Lsts.akv);
       Work->SetLsts (&LISTDLG::Lsts);
       if (ratod (LISTDLG::Lsts.a) == 0.0)
       {
//           disp_mess ("Artikel-Nummer > 0 eingeben", 2);
//           fWork->SetCurrentName ("Liste");
	       fPos.SetCurrentName ("scanfeld");
           return TRUE;
       }
	   if (strlen(clipped(LISTDLG::Lsts.lief_best)) == 0)
	   {
		if (Instance->HoleAnzLief_best(ratod(LISTDLG::Lsts.a)) > 1)
		{
			Instance->ShowBzg (LISTDLG::Lsts.a);
		}
	   }
       memcpy (&lief_bzg, &lief_bzg_null, sizeof (struct LIEF_BZG));
       lief_bzg.mdn = we_kopf.mdn;
       lief_bzg.fil = we_kopf.fil;
       strcpy (lief_bzg.lief, we_kopf.lief);
       dsqlstatus = Work->ReadA (ratod (LISTDLG::Lsts.a));
       if (dsqlstatus == 100)
       {
           print_mess (2, "Artikel %.0lf nicht gefunden", ratod (LISTDLG::Lsts.a));
       }

       if (dsqlstatus == -2)
       {
           disp_mess ("Falscher Artikeltyp", 2);
       }

       if (dsqlstatus != 0)
       {
		   sprintf (LISTDLG::Lsts.a, "%.0lf", 0.0);
		   fPos.SetText ();
           fWork->SetCurrentName ("Liste");
           fPos.SetCurrentName ("a");
           return TRUE;
       }

	   if (_a_bas.charg_hand == 1 || _a_bas.charg_hand == 9 || _a_bas.zerl_eti == 1)
	   {
            EnableMenuItem (ActiveDlg->GethMenu (),   IDM_EXTRA_DATA, MF_ENABLED);
	   }
	   else
	   {
            EnableMenuItem (ActiveDlg->GethMenu (),   IDM_EXTRA_DATA, MF_GRAYED);
	   }

//	   TestEmb ();
//       TestCharge ();
       strcpy (LISTDLG::Lsts.a_bz1, _a_bas.a_bz1);
       sprintf (LISTDLG::Lsts.pr_ek, "%.4lf", Work->GetPrEkBto ());//310804 LuD %3lf ge�ndert in %4lf
       sprintf (LISTDLG::Lsts.netto_ek, "%.4lf", Work->GetPrEk ());//310804 LuD %3lf ge�ndert in %4lf
       sprintf (LISTDLG::Lsts.pr_fil_ek, "%.4lf", Work->GetPrFilEk ());//310804 LuD %3lf ge�ndert in %4lf
       sprintf (LISTDLG::Lsts.pr_vk, "%.4lf", Work->GetPrVk ());
       strcpy  (LISTDLG::Lsts.lief_best, lief_bzg.lief_best);
	   sprintf (LISTDLG::Lsts.me, "%.3lf", 0.0);
	   sprintf (LISTDLG::Lsts.tara, "%.3lf", 0.0);
	   sprintf (LISTDLG::Lsts.mhd, "%.d", 0);
	   strcpy  (LISTDLG::Lsts.ls_ident,  "");
	   strcpy  (LISTDLG::Lsts.ls_charge, "");
	   sprintf  (LISTDLG::Lsts.mhd, "%hd", _a_bas.hbk_ztr);
       LISTDLG::Lsts.lager = Work->HoleLager (_a_bas.a,we_kopf.mdn);
	   sprintf  (LISTDLG::Lsts.lager_bez, "%s", Work->HoleLagerBez (LISTDLG::Lsts.lager,we_kopf.mdn));
	   long ldat;
	   char datum[12];

		    sysdate(datum);
           ldat = dasc_to_long (datum);
		   ldat = ldat + _a_bas.hbk_ztr;
		   dlong_to_asc (ldat,LISTDLG::Lsts.hbk_dat);

	   ActiveWeDlgLst->IdentCharge ();
       if (lief_bzg.me_kz[0] == '1')
       {
             sprintf (LISTDLG::Lsts.pr_ek, "%.4lf", (double) ratod (LISTDLG::Lsts.pr_ek) *
                                                                    LISTDLG::Lsts.inh_einh);
             if (WeWork::pr_ek_einh != 0.0)
             {
                 sprintf (LISTDLG::Lsts.pr_ek, "%.4lf", WeWork::pr_ek_einh);
             }
             Work->CalcEkBto ();
       }
       sprintf (LISTDLG::Lsts.inh, "%.3lf", LISTDLG::Lsts.inh_einh);
//       TestIdentChargeDefaults ();
	   fPos.SetText ();
/*
       if (ActiveWeDlgLst->AppendMode)
       {
               ActiveWeDlgLst->InsertRow ();
       }
*/
       memcpy (&LISTDLG::LstTab[ActiveWeDlgLst->GetAktRow ()],
		                 &LISTDLG::Lsts, sizeof (LISTDLG::Lsts));
	   ActiveWeDlgLst->FillRow (ActiveWeDlgLst->GetAktRow ());
	   fWork->SetText ();
//       fWork->SetCurrentName ("Liste");
       fPos.SetCurrentName ("me");
       return FALSE;
}

int WeDlgLst::AfterScanfeldCharge (void)
{
	int dret = 0;
		dret = ScanEan128Charge (Text (LISTDLG::Lsts.scanfeld));
	    if (dret == 100) return 100; 
	   if (ArtikelUnvollstaendig == 0)
		{
//			Work->BucheBsd (ratod (LISTDLG::Lsts.a),
//                      ratod ("0"),
//                      atoi (LISTDLG::Lsts.me_einh),
//                      ratod (LISTDLG::Lsts.pr_ek));
	   }

		if (ActiveWeDlgLst->NewArt () == FALSE)
		{
		     return 1;
		}
		return 0;

}

int WeDlgLst::AfterScanfeld (void)
{
       int dsqlstatus;
	   WeWork *Work;
	   CFORM *fWork;

	   if (syskey == KEY5) return FALSE;
	   if (AktId == KEY5) return FALSE;
	   if (syskey == KEYUP) return FALSE;
	   if (syskey == KEY7) return FALSE;


	   if (ratod (LISTDLG::Lsts.a) == 0.0)
	   {
			strncpy(LISTDLG::Lsts.a,LISTDLG::Lsts.scanfeld,13);
	   }

       if (ratod (LISTDLG::Lsts.a) == 0.0 && ArtikelVorhanden == FALSE)
	   {
		   ArtikelUnvollstaendig = 99;
		   return FALSE;
	   }
//	   sprintf (LISTDLG::Lsts.me, "%.3lf", 0.0);
//	   sprintf (LISTDLG::Lsts.mhd, "%.d", 0);
//	   strcpy  (LISTDLG::Lsts.ls_charge, "");

	   if (ArtikelFromScann(LISTDLG::Lsts.scanfeld,FALSE) > 0) 
	   {
            fPos.GetCfield ("scanfeld")->SetText ();
//			DeleteRow();
			ArtikelUnvollstaendig = 99; //260109
           fPos.SetCurrentName ("scanfield");
			return TRUE;
	   }


	   Work = ActiveWeDlgLst->GetWork ();
	   fWork = &fWeLst0;
       sysdate (LISTDLG::Lsts.akv);
       Work->SetLsts (&LISTDLG::Lsts);
       if (ratod (LISTDLG::Lsts.a) == 0.0)
       {
           fWork->SetCurrentName ("Liste");
           fPos.SetCurrentName ("scanfield");
           return TRUE;
       }
	   /***
	   if (strlen(clipped(LISTDLG::Lsts.lief_best)) == 0)
	   {
		if (Instance->HoleAnzLief_best(ratod(LISTDLG::Lsts.a)) > 1)
		{
			Instance->ShowBzg (LISTDLG::Lsts.a);
		}
	   }
	   *******/
       lief_bzg.mdn = we_kopf.mdn;
       lief_bzg.fil = we_kopf.fil;
       strcpy (lief_bzg.lief, we_kopf.lief);
       strcpy  (LISTDLG::Lsts.lief_best, lief_bzg.lief_best);
       dsqlstatus = Work->ReadA (ratod (LISTDLG::Lsts.a));
       if (dsqlstatus == 100)
       {
           print_mess (2, "Artikel %.0lf nicht gefunden", ratod (LISTDLG::Lsts.a));
       }

       if (dsqlstatus == -2)
       {
           disp_mess ("Falscher Artikeltyp", 2);
       }

       if (dsqlstatus != 0)
       {
		   sprintf (LISTDLG::Lsts.a, "%.0lf", 0.0);
		   fPos.SetText ();
           fWork->SetCurrentName ("Liste");
           fPos.SetCurrentName ("scanfield");
           return TRUE;
       }

	   if (_a_bas.charg_hand == 1 || _a_bas.charg_hand == 9 || _a_bas.zerl_eti == 1)
	   {
            EnableMenuItem (ActiveDlg->GethMenu (),   IDM_EXTRA_DATA, MF_ENABLED);
	   }
	   else
	   {
            EnableMenuItem (ActiveDlg->GethMenu (),   IDM_EXTRA_DATA, MF_GRAYED);
	   }

//	   TestEmb ();
//       TestCharge ();
       strcpy (LISTDLG::Lsts.a_bz1, _a_bas.a_bz1);
       sprintf (LISTDLG::Lsts.pr_ek, "%.4lf", Work->GetPrEkBto ());//310804 LuD %3lf ge�ndert in %4lf
       sprintf (LISTDLG::Lsts.netto_ek, "%.4lf", Work->GetPrEk ());//310804 LuD %3lf ge�ndert in %4lf
       sprintf (LISTDLG::Lsts.pr_fil_ek, "%.4lf", Work->GetPrFilEk ());//310804 LuD %3lf ge�ndert in %4lf
       sprintf (LISTDLG::Lsts.pr_vk, "%.4lf", Work->GetPrVk ());
       strcpy  (LISTDLG::Lsts.lief_best, lief_bzg.lief_best);
	   sprintf (LISTDLG::Lsts.tara, "%.3lf", 0.0);
	   strcpy  (LISTDLG::Lsts.ls_ident,  "");
	   sprintf  (LISTDLG::Lsts.mhd, "%hd", _a_bas.hbk_ztr);
       LISTDLG::Lsts.lager = Work->HoleLager (_a_bas.a,we_kopf.mdn);
	   sprintf  (LISTDLG::Lsts.lager_bez, "%s", Work->HoleLagerBez (LISTDLG::Lsts.lager,we_kopf.mdn));
	   ActiveWeDlgLst->IdentCharge ();

	   if (ActiveWeDlgLst->NewArt () == FALSE)
	   {
		   return FALSE;
	   }
       if (lief_bzg.me_kz[0] == '1')
       {
             sprintf (LISTDLG::Lsts.pr_ek, "%.4lf", (double) ratod (LISTDLG::Lsts.pr_ek) *
                                                                    LISTDLG::Lsts.inh_einh);
             if (WeWork::pr_ek_einh != 0.0)
             {
                 sprintf (LISTDLG::Lsts.pr_ek, "%.4lf", WeWork::pr_ek_einh);
             }
             Work->CalcEkBto ();
       }
       sprintf (LISTDLG::Lsts.inh, "%.3lf", LISTDLG::Lsts.inh_einh);
//       TestIdentChargeDefaults ();
	   fPos.SetText ();
/*
       if (ActiveWeDlgLst->AppendMode)
       {
               ActiveWeDlgLst->InsertRow ();
       }
*/
       memcpy (&LISTDLG::LstTab[ActiveWeDlgLst->GetAktRow ()],
		                 &LISTDLG::Lsts, sizeof (LISTDLG::Lsts));
	   ActiveWeDlgLst->FillRow (ActiveWeDlgLst->GetAktRow ());
//	   if (ArtikelUnvollstaendig != 0)
//	   {
//240409            Work->SetAktBsdMe (0.0);  
//	   }
	   fWork->SetText ();
//       fWork->SetCurrentName ("Liste");
       fPos.SetCurrentName ("scanfeld");
       return FALSE;
}

int WeDlgLst::AfterBzgPressed (void)
{
       int dsqlstatus;
	   WeWork *Work;
	   CFORM *fWork;

	   if (syskey == KEY5) return FALSE;
	   if (AktId == KEY5) return FALSE;
	   if (syskey == KEYUP) return FALSE;
	   if (syskey == KEY7) return FALSE;

       if (ratod (LISTDLG::Lsts.a) == 0.0)
	   {
		   return FALSE;
	   }
	   Work = ActiveWeDlgLst->GetWork ();
	   fWork = &fWeLst0;
       sysdate (LISTDLG::Lsts.akv);
       Work->SetLsts (&LISTDLG::Lsts);
       memcpy (&lief_bzg, &lief_bzg_null, sizeof (struct LIEF_BZG));
       lief_bzg.mdn = we_kopf.mdn;
       lief_bzg.fil = we_kopf.fil;
       strcpy (lief_bzg.lief, we_kopf.lief);
       dsqlstatus = Work->ReadA (ratod (LISTDLG::Lsts.a));

       if (dsqlstatus != 0)
       {
           return TRUE;
       }
       sprintf (LISTDLG::Lsts.pr_ek, "%.4lf", Work->GetPrEkBto ());//310804 LuD %3lf ge�ndert in %4lf
       sprintf (LISTDLG::Lsts.netto_ek, "%.4lf", Work->GetPrEk ());//310804 LuD %3lf ge�ndert in %4lf
       sprintf (LISTDLG::Lsts.pr_fil_ek, "%.4lf", Work->GetPrFilEk ());//310804 LuD %3lf ge�ndert in %4lf
       sprintf (LISTDLG::Lsts.pr_vk, "%.4lf", Work->GetPrVk ());
       LISTDLG::Lsts.lager = Work->HoleLager (_a_bas.a,we_kopf.mdn);
	   sprintf  (LISTDLG::Lsts.lager_bez, "%s", Work->HoleLagerBez (LISTDLG::Lsts.lager,we_kopf.mdn));
       strcpy  (LISTDLG::Lsts.lief_best, lief_bzg.lief_best);
       if (lief_bzg.me_kz[0] == '1')
       {
             sprintf (LISTDLG::Lsts.pr_ek, "%.4lf", (double) ratod (LISTDLG::Lsts.pr_ek) *
                                                                    LISTDLG::Lsts.inh_einh);
             if (WeWork::pr_ek_einh != 0.0)
             {
                 sprintf (LISTDLG::Lsts.pr_ek, "%.4lf", WeWork::pr_ek_einh);
             }
             Work->CalcEkBto ();
       }
       sprintf (LISTDLG::Lsts.inh, "%.3lf", LISTDLG::Lsts.inh_einh);
	   fPos.SetText ();
       memcpy (&LISTDLG::LstTab[ActiveWeDlgLst->GetAktRow ()],
		                 &LISTDLG::Lsts, sizeof (LISTDLG::Lsts));
	   ActiveWeDlgLst->FillRow (ActiveWeDlgLst->GetAktRow ());
	   fWork->SetText ();
       fPos.SetCurrentName ("me");
       return FALSE;
}

void WeDlgLst::CallInfo (void)
{
  	      char where [256];

          CFIELD *aCfield = fPos.GetCfield ("a");
          aCfield->GetText ();
          sprintf (where, "where mdn = %hd "
                              "and fil = %hd "
                              "lief = %s "
                              "and a = %.0lf",
                              we_kopf.mdn,
                              we_kopf.fil,
                              we_kopf.lief,
                              ratod ((char *) aCfield->GetFeld ()));
     	  _CallInfoEx (hWndMain, NULL, "liefa", where, 0l);
}
void WeDlgLst::GetSysPar (void)
{
       SYS_PAR_CLASS SysPar;

       strcpy (sys_par.sys_par_nam, "we_fil_bel_par");
       if (SysPar.dbreadfirst () == 0)
       {
					we_fil_bel_par = atoi (sys_par.sys_par_wrt);
       }

       SysPar.dbclose ();
}
int WeDlgLst::ShowLief (void)
/**
Lieferanten zum Artikel anzeigen.
**/
{
    LIEFAC *Liefac;
	char liefc [17];
	int ret;

	if (ratod (LISTDLG::Lsts.a) == 0.0)
	{
		disp_mess ("Keine Artikel-Nummer vorhanden", 2);
        return 0;
	}

	WeWork::FuellePreise_we(AnzTagePreise);
//    EnableWindows (eListe.Getmamain3 (), FALSE); 
	Liefac = new LIEFAC;
    EnableWindow (hWnd, FALSE);
    ret = Liefac->Show (GetParent (GetParent (hMainWindow)), we_kopf.mdn, we_kopf.fil, ratod (LISTDLG::Lsts.a), liefc,AnzTagePreise);
//    EnableWindows (eListe.Getmamain3 (), TRUE); 
    EnableWindow (hWnd, TRUE);
	delete Liefac;
    GetCurrentCfield ()->SetFocus ();
	return 1;
}

void WeDlgLst::GetCfgValues (void)
{
       char cfg_v [512];
	   static BOOL cfgOK = FALSE;

	   if (cfgOK) return;


       if (ProgCfg == NULL) return;
	   if (cfgOK) return;

       cfgOK = TRUE;
       if (ProgCfg->GetCfgValue ("AnzTagePreise", cfg_v) == TRUE)
       {
                    AnzTagePreise =  (atoi (cfg_v));
	   }
       if (ProgCfg->GetCfgValue ("add_me", cfg_v) == TRUE)
       {
                    add_me =  (atoi (cfg_v));
	   }
}

int WeDlgLst::HoleAnzLief_best (double a)
{

	lief_bzg.mdn = we_kopf.mdn;
	lief_bzg.fil = we_kopf.fil;
	int anzahl = 0;
	   Wejour.sqlin ((short *) &we_kopf.mdn, 1, 0);
	   Wejour.sqlin ((short *) &we_kopf.fil, 1, 0);
	   Wejour.sqlin ((char *) we_kopf.lief, 0, 17);
	   Wejour.sqlin ((double *) &a, 3, 0);
	   Wejour.sqlout ((long *) &anzahl, 2, 0);
	   Wejour.sqlcomm ("select count(*) from lief_bzg "
		                                 "where mdn = ? " 
						                 "and fil = ? "
						                 "and lief = ? "
						                 "and a = ?");
       while (anzahl == 0)
       {
                  if (lief_bzg.fil > 0)
                  {
                      lief_bzg.fil = 0;
                  }
                  else if (lief_bzg.mdn > 0)
                  {
                      lief_bzg.mdn = 0;
                  }
                  else
                  {
                      break;
                  }
				 Wejour.sqlin ((short *) &lief_bzg.mdn, 1, 0);
				Wejour.sqlin ((short *) &lief_bzg.fil, 1, 0);
				Wejour.sqlin ((char *) we_kopf.lief, 0, 17);
				Wejour.sqlin ((double *) &a, 3, 0);
				Wejour.sqlout ((long *) &anzahl, 2, 0);
				Wejour.sqlcomm ("select count(*) from lief_bzg "
		                                 "where mdn = ? " 
						                 "and fil = ? "
						                 "and lief = ? "
						                 "and a = ?");
       }
   	   if (anzahl == 1)
	   {
		    Wejour.sqlin ((short *) &lief_bzg.mdn, 1, 0);
			Wejour.sqlin ((short *) &lief_bzg.fil, 1, 0);
	        Wejour.sqlin ((char *) we_kopf.lief, 0, 17);
		    Wejour.sqlin ((double *) &a, 3, 0);
		    Wejour.sqlout ((char *) &lief_bzg.lief_best, 0, 17);
		    int dsql = Wejour.sqlcomm ("select lief_best from lief_bzg "
		                     "where mdn = ? " 
			                 "and fil = ? "
			                 "and lief = ? "
			                 "and a = ?");
		    if (dsql == 0) strcpy (LISTDLG::Lsts.lief_best,lief_bzg.lief_best);
		}


	   return anzahl;
}


int WeDlgLst::ShowBzg (char* ca)
{
        struct SBZG *sbzg;
        SEARCHBZG SearchBzg; 
        char buffer [256];

        sprintf (buffer, "lief_bzg.lief = \"%s\" and (lief_bzg.mdn = %hd or lief_bzg.mdn = 0) "
			             "and (lief_bzg.fil = %hd or lief_bzg.fil = 0)", 
						 clipped (we_kopf.lief), we_kopf.mdn, we_kopf.fil);
        SearchBzg.SetQuery (buffer); 
        SearchBzg.SetWindowStyle (WS_POPUP | 
                                  WS_THICKFRAME |
                                  WS_CAPTION |
                                  WS_SYSMENU |
                                  WS_MINIMIZEBOX |
                                  WS_MAXIMIZEBOX);
        SearchBzg.SetParams (DLG::hInstance, hMainWindow);

        SearchBzg.Setawin (GetParent (GetParent (hMainWindow)));
        SearchBzg.Search (we_kopf.mdn,ca);
        EnableWindows (GetParent (hWnd), TRUE);
        if (syskey == KEY5)
        {

            return 0;
        }
        sbzg = SearchBzg.GetSBzg ();
        if (sbzg == NULL)
        {
            return 0;
        }

        strcpy (LISTDLG::Lsts.a, sbzg->a);
        strcpy (LISTDLG::Lsts.lief_best, sbzg->lief_best);
        return 0;
}


BOOL WeDlgLst::Readlief_best (void)
{
       int dsqlstatus;

       memcpy (&lief_bzg, &lief_bzg_null, sizeof (struct LIEF_BZG));
       lief_bzg.mdn = we_kopf.mdn;
       lief_bzg.fil = we_kopf.fil;
       strcpy (lief_bzg.lief, we_kopf.lief);
//TODO       dsqlstatus = Work->ReadLief_best (Lsts.lief_best);
       if (dsqlstatus == 100)
       {
           print_mess (2, "Bestell-Nummer %s nicht gefunden", LISTDLG::Lsts.lief_best);
//TODO           eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
//TODO           eListe.ShowAktRow ();
//TODO           SetListFocus ();
           return TRUE;
       }
       sprintf (LISTDLG::Lsts.a, "%.0lf", lief_bzg.a);
//       return Reada ();
	   return 0;
}
