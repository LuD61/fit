#ifndef _LISTDLG_DEF
#define _LISTDLG_DEF
#include "listenter.h"
#include "listclex.h"
#include "we_kopf.h"
#include "we_pos.h"
#include "searcha.h"
#include "searchbzg.h"
#include "wework.h"
#include "EmbDlg.h"
#include "mo_progcfg.h"
#include "WeExtraDataCollection.h"
#include "ExtraDataDialog.h"

#define SMALL 0
#define BIG 1
 
class LISTDLG : public virtual LISTENTER, 
                public virtual KEYS,
                public virtual Info
{
         private :
                      static CFIELD *_fChoise0[];
                      static CFORM fChoise0;
                      static CFIELD *_fChoise[];
                      static CFORM fChoise;

                      static struct CHATTR ChAttra [];
                      static struct CHATTR ChAttrlief_best [];
                      static struct CHATTR ChAttra_krz [];
                      static struct CHATTR *ChAttr;
					  WE_KOPF_CLASS Wekopf;
					  WE_POS_CLASS Wepos;
					  WE_JOUR_CLASS Wejour;
                      WeWork *Work;
					  EmbDlg * Emb;
                      int cursor;
                      CFORM *Toolbar2;
                      BOOL cfgOK;
                      PROG_CFG *ProgCfg;
                      BOOL WithLiefBest;
                      BOOL WithSearchBest;
                      BOOL WithAkrz;
                      BOOL WithEk;
                      BOOL WithPosEk;
                      char *ChargeMacro;
                      BOOL ls_charge_par;
                      BOOL lsc2_par;
					  short we_vk_par;
					  short flg_vk_par;
                      BOOL ABasCharge;
                      char DefaultIdent  [22];
                      char DefaultCharge [22];
                      HBITMAP ArrDown;
                      short akt_lager;
                      int bsd_kz;
                      int TestKontraktMe;
					  CWeExtraDataCollection ExtraData;
					  CWeExtraData * ExtraDataItem;
					  CExtraDataDialog ExtraDataDialog;
			 		  static LISTDLG *ActiveListDlg;
					  HMENU hMenu;

         public :

		           int ShowLief (void);
					static BOOL InEnterPos ;
                    static struct LSTS Lsts;
                     static struct LSTS *LstTab;
                     static ITEM ua;
		             static ITEM uint_pos;
                     static ITEM ua_bz1;
                     static ITEM ua_bz2;
                     static ITEM ubest_me;
                     static ITEM ume;
                     static ITEM ume_einh_bez;
                     static ITEM uinh;
                     static ITEM utara;
                     static ITEM upr_ek;
                     static ITEM unetto_ek;
                     static ITEM upos_wert;
                     static ITEM uhbk_ztr;
                     static ITEM upr_vk;
                     static ITEM uspanne;
                     static ITEM uls_ident;
                     static ITEM uls_charge;
                     static ITEM ufiller;
                     static field _UbForm[];
                     static form UbForm;

                     static ITEM iline;
                     static field _LineForm[];
                     static form LineForm;

                     static ITEM iint_pos;
                     static ITEM ia;
                     static ITEM ilief_best;
                     static ITEM ia_krz;
                     static ITEM ia_bz1;
                     static ITEM ia_bz2;
                     static ITEM ibest_me;
                     static ITEM ime;
                     static ITEM ime_einh_bez;
                     static ITEM ime_choise;
                     static ITEM ipr_ek_choise;
                     static ITEM iinh;
                     static ITEM itara;
                     static ITEM ipr_ek;
                     static ITEM inetto_ek;
                     static ITEM ipos_wert;
                     static ITEM ihbk_ztr;
                     static ITEM ipr_vk;
                     static ITEM ifil_ek;
                     static ITEM ispanne;
                     static ITEM ils_ident;
                     static ITEM ils_charge;
                     static field _DataForm[];
                     static form DataForm;
                     static int ubrows[];

                     static field _DataForm0[];
                     static form DataForm0;
                     static field _UbForm0[];
                     static form UbForm0;
                     static field _LineForm0[];
                     static form LineForm0;
                     static int ubrows0[];

                     static SEARCHA SearchA;

                     void SetWithLiefBest (BOOL);
                     void SetWithSearchBest (BOOL);
                     void SetWithAkrz (BOOL);
                     void SetWithPosEk (BOOL);
                     void SetButtonSize (int);

                    void SethMenu (HMENU hMenu)
					{
						this->hMenu = hMenu;
					}

					HMENU gethMenu ()
					{
						return hMenu;
					}

			        double GetAktArtikel (void)
					{
						return ratod (Lsts.a);
					}
			        char* GetAktLief_best (void)
					{
					    static char cLiefBest [17];
						strcpy (cLiefBest,Lsts.lief_best);
						return cLiefBest;
					}

                     void SetDefaultIdent (char *Ident)
                     {
                         strcpy (DefaultIdent, Ident);
                     }

                     char *GetDefaultIdent (void)
                     {
                         return DefaultIdent;
                     }

                     void SetDefaultCharge (char *Charge)
                     {
                         strcpy (DefaultCharge, Charge);
                     }

                     char *GetDefaultCharge (void)
                     {
                         return DefaultCharge;
                     }

                     BOOL GetWithLiefBest (void)
                     {
                         return WithLiefBest;
                     }
                     BOOL GetWithAkrz (void)
                     {
                         return WithAkrz;
                     }
                     void SetToolbar2 (CFORM *Toolbar2)
                     {
                               this->Toolbar2 = Toolbar2;
                     }
 
                     CFORM *GetToolbar2 (void)
                     {
                               return Toolbar2;
                     }

                     void SetWork (WeWork *weWork)
                     {
                         this->Work = weWork;
                         this->Work->SetListDlg (this);
                         this->Work->SetLager (akt_lager);
                         this->Work->SetBsdKz (bsd_kz);
                         this->Work->SetTestKontrakt (TestKontraktMe);
                     }

                     WeWork *GetWork (void)
                     {
                         return Work;
                     }

                     LISTDLG ();
					 ~LISTDLG ();
                     void GetSysPar (void);
                     void GetCfgValues (void);
                     void Seta_bz2Removed (BOOL);
                     void Setlief_bestRemoved (BOOL);
                     void Seta_krzRemoved (BOOL);
                     void Prepare (void);
                     void Open (void);
                     int Fetch (void);
                     void Close (void);
                     int ShowA (void); 
                     int ShowBzg (void); 
                     int ShowBzg (char*); 
		             int HoleAnzLief_best (double);
                     BOOL ShowMeEinh (void);
                     void SetPrFieldText (BOOL);
                     void SetInhFieldText (BOOL);
                     
 				     unsigned char *GetDataRec (void);
				     int GetDataRecLen (void);
 				     char *GetListTab (int);
					 void ReadExtraData (int i);
			   	     void ReadExtraData (void);
					 void LadeExtraData (void);
					 void LadeExtraData (int i);
                     void WriteRec (int);
                     BOOL PruefeRec (int);
                     void InitRow (void);
                     int ToMemory (int);
                     void uebertragen (void);
                     void StartList (void);

				    virtual BOOL PreTranslateMessage (MSG *msg);
                     BOOL OnKey5 (void);
                     BOOL OnKey6 (void);
                     BOOL OnKey7 (void);
                     BOOL OnKey8 (void);
                     BOOL OnKey9 (void);
                     BOOL OnKey10 (void);
                     BOOL OnKey11 (void);
                     BOOL OnMHDData (void);
                     BOOL OnKey12 (void);
                     BOOL OnKeyDel (void);
                     BOOL OnRowDelete (void);
                     BOOL OnKeyInsert (void);
                     BOOL OnKeyUp (void);
                     BOOL OnKeyDown (void);
                     BOOL OnCommand (HWND, UINT, WPARAM, LPARAM);
                     BOOL OnPaint (HWND, UINT, WPARAM, LPARAM);
                     BOOL OnMove (HWND, UINT, WPARAM, LPARAM);
					 BOOL OnExtraData (void);
                     int AfterRow (int);
                     int AfterCol (char *);
                     int BeforeRow (int);
                     void TestCharge (void);
                     void TestEmb (void);
					 void DestroyEmb ();
                     BOOL TestEkBto (void);
                     BOOL TestMe (void);
                     int Schreibea_pr (void);
                     BOOL ReadaDirect (void);
                     BOOL Reada (void);
                     BOOL Readlief_best (void);
                     BOOL Reada_krz(void);
                     BOOL TestGenCharge (void);
                     void SetBaseForms ();
                     void CreateListQuikInfos (HWND);
                     void TestRemoves (void);
                     void TestIdentChargeDefaults (void);
                     static int InfoProc (char **, char *, char *);
			         int AfterBzgPressed (void);

                     BOOL CallInfo (HWND, RECT *, char *, char *, DWORD);
};
#endif