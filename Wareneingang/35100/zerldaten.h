#ifndef _ZERLDATEN_DEF
#define _ZERLDATEN_DEF

#include "dbclass.h"

struct ZERLDATEN {
   short     mdn;
   short     fil;
   long      partie;
   char      lief[17];
   char      lief_rech_nr[17];
   char      ident_extern[21];
   char      ident_intern[21];
   double    a;
   double    a_gt;
   char      kategorie[4];
   short     schnitt;
   short     gebland;
   short     mastland;
   short     schlaland;
   short     zerland;
   char      esnum[11];
   char      eznum1[11];
   char      eznum2[11];
   char      eznum3[11];
   long      dat;
   short     anz_gedruckt;
   short     anz_entnommen;
   long      lfd;
   long      lfd_i;
   short     aktiv;
   short     zerlegt;
};
extern struct ZERLDATEN zerldaten, zerldaten_null;

#line 7 "zerldaten.rh"

class ZERLDATEN_CLASS : public DB_CLASS 
{
       private:
	       int test_upd_cursor2;	
       public :
               ZERLDATEN_CLASS () : DB_CLASS ()
               {
		    test_upd_cursor2 = -1;	
               }
       virtual void prepare (void);
       virtual int dbinsert ();
       virtual int dbmodify ();
       virtual int dbtestinsert ();  
};
#endif
