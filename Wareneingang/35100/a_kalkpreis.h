#ifndef _A_KALKPREIS_DEF
#define _A_KALKPREIS_DEF

#include "windows.h"
#include "mo_draw.h"
#include "wmask.h"
#include "dbclass.h"

struct A_KALKPREIS {
   long      mdn;
   short     fil;
   double    a;
   double    mat_o_b;
   double    hk_vollk;
   double    sk_vollk;
   double    fil_ek_vollk;
   double    fil_vk_vollk;
   double    hk_teilk;
   double    sk_teilk;
   double    fil_ek_teilk;
   double    fil_vk_teilk;
   long      dat;
   char      zeit[6];
   char      programm[13];
   char      version[13];
};
extern struct A_KALKPREIS a_kalkpreis, a_kalkpreis_null;

#line 10 "a_kalkpreis.rh"

class A_KALKPREIS_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               A_KALKPREIS_CLASS () : DB_CLASS ()
               {
               }
               ~A_KALKPREIS_CLASS ()
               {
               } 
};
#endif
