#include <windows.h>
#include "wmaskc.h"
#include "searchpreise_we.h"
#include "strfkt.h"
#include "mo_wmess.h"
#include "lbox.h"
#include "dlglst.h"
#include "a_bas.h"
#include "lbox.h"


struct SPREISE_WE *SEARCHPREISE_WE::spreise_wetab = NULL;
struct SPREISE_WE SEARCHPREISE_WE::spreise_we;
int SEARCHPREISE_WE::idx = -1;
long SEARCHPREISE_WE::anz;
CHQEX *SEARCHPREISE_WE::Query = NULL;
DB_CLASS SEARCHPREISE_WE::DbClass;
HINSTANCE SEARCHPREISE_WE::hMainInst;
HWND SEARCHPREISE_WE::hMainWindow;
HWND SEARCHPREISE_WE::awin;
int SEARCHPREISE_WE::SearchField = 1;
int SEARCHPREISE_WE::sobest_term = 1; 
int SEARCHPREISE_WE::sowe_dat = 1; 
int SEARCHPREISE_WE::soa_bz1 = 1;
int SEARCHPREISE_WE::solief_best = 1;
int SEARCHPREISE_WE::solief_rech_nr = 1;
int SEARCHPREISE_WE::sopr_ek_b = 1;
int SEARCHPREISE_WE::sopr_ek_w = 1;
int SEARCHPREISE_WE::some_einh_b = 1;
int SEARCHPREISE_WE::some_einh_w = 1;
short SEARCHPREISE_WE::mdn = 0;
PTAB_CLASS SEARCHPREISE_WE::Ptab;


ITEM SEARCHPREISE_WE::ubest_term         ("best_term",         "Bestelldatum",      "", 0);
ITEM SEARCHPREISE_WE::uwe_dat         ("we_dat",         "WE-Datum",      "", 0);
ITEM SEARCHPREISE_WE::ulief_rech_nr         ("lief_rech_nr",         "RechnungsNr.",      "", 0);
ITEM SEARCHPREISE_WE::ua_bz1     ("a_bz1",     "Lieferant",  "", 0);
ITEM SEARCHPREISE_WE::ulief_best ("lief_best", "BestNr",   "", 0);
ITEM SEARCHPREISE_WE::upr_ek     ("pr_ek",     "EK ",           "", 0);
ITEM SEARCHPREISE_WE::ume_einh   ("me_einh",  "Einheit",      "", 0);
ITEM SEARCHPREISE_WE::upr_ek_we     ("pr_ek_we",     "EK ",           "", 0);
ITEM SEARCHPREISE_WE::ume_einh_we   ("me_einh_we",  "Einheit",      "", 0);


field SEARCHPREISE_WE::_UbForm[] = {
&ua_bz1,     15, 0, 0, 0 , NULL, "", BUTTON, 0, 0, 0,
&ubest_term, 12, 0, 0, 16,  NULL, "", BUTTON, 0, 0, 0,
&upr_ek,      9, 0, 0, 29 , NULL, "", BUTTON, 0, 0, 0,
&ume_einh,   10, 0, 0, 37 , NULL, "", BUTTON, 0, 0, 0,
&uwe_dat,    11, 0, 0, 47,  NULL, "", BUTTON, 0, 0, 0,
&upr_ek_we,   9, 0, 0, 59 , NULL, "", BUTTON, 0, 0, 0,
&ume_einh_we,10, 0, 0, 68 , NULL, "", BUTTON, 0, 0, 0,
&ulief_best,  8, 0, 0, 78 , NULL, "", BUTTON, 0, 0, 0,
&ulief_rech_nr, 16, 0, 0, 85 , NULL, "", BUTTON, 0, 0, 0,
};

form SEARCHPREISE_WE::UbForm = {9, 0, 0, _UbForm, 0, 0, 0, 0, NULL};


ITEM SEARCHPREISE_WE::ia_bz1       ("a_bz1",       spreise_we.a_bz1,      "", 0);
ITEM SEARCHPREISE_WE::ibest_term   ("best_term",   spreise_we.best_term,          "", 0);
ITEM SEARCHPREISE_WE::ipr_ek       ("pr_ek",       spreise_we.pr_ek_b,      "", 0);
ITEM SEARCHPREISE_WE::ime_einh     ("me_einh",     spreise_we.me_einh_b,    "", 0);
ITEM SEARCHPREISE_WE::iwe_dat      ("we_dat",      spreise_we.we_dat,          "", 0);
ITEM SEARCHPREISE_WE::ipr_ek_we    ("pr_ek_we",    spreise_we.pr_ek_w,      "", 0);
ITEM SEARCHPREISE_WE::ime_einh_we  ("me_einh_we",   spreise_we.me_einh_w,    "", 0);
ITEM SEARCHPREISE_WE::ilief_best   ("lief_best",   spreise_we.lief_best,  "", 0);
ITEM SEARCHPREISE_WE::ilief_rech_nr ("lief_rech_nr", spreise_we.lief_rech_nr,  "", 0);

field SEARCHPREISE_WE::_DataForm[] = {
&ia_bz1,      16, 0, 0,  1 , NULL, "",       DISPLAYONLY, 0, 0, 0,
&ibest_term,  10, 0, 0,  18,  NULL, "",       DISPLAYONLY, 0, 0, 0,
&ipr_ek,       6, 0, 0, 30 , NULL, "",  DISPLAYONLY, 0, 0, 0,
&ime_einh,     8, 0, 0, 39 , NULL, "",       DISPLAYONLY, 0, 0, 0,
&iwe_dat,     10, 0, 0, 49,  NULL, "",       DISPLAYONLY, 0, 0, 0,
&ipr_ek_we,    6, 0, 0, 61 , NULL, "",  DISPLAYONLY, 0, 0, 0,
&ime_einh_we,  8, 0, 0, 70 , NULL, "",       DISPLAYONLY, 0, 0, 0,
&ilief_best,   8, 0, 0, 80 , NULL, "",       DISPLAYONLY, 0, 0, 0,
&ilief_rech_nr, 16, 0, 0, 90 , NULL, "",       DISPLAYONLY, 0, 0, 0,
};
 
form SEARCHPREISE_WE::DataForm = {9, 0, 0, _DataForm, 0, 0, 0, 0, NULL};

ITEM SEARCHPREISE_WE::iline ("", "1", "", 0);

field SEARCHPREISE_WE::_LineForm[] = {
&iline,       1, 0, 0, 16 , NULL, "", DISPLAYONLY, 0, 0, 0,
&iline,       1, 0, 0, 27 , NULL, "", DISPLAYONLY, 0, 0, 0,
&iline,       1, 0, 0, 36 , NULL, "", DISPLAYONLY, 0, 0, 0,
&iline,       1, 0, 0, 47 , NULL, "", DISPLAYONLY, 0, 0, 0,
&iline,       1, 0, 0, 57 , NULL, "", DISPLAYONLY, 0, 0, 0,
&iline,       1, 0, 0, 67 , NULL, "", DISPLAYONLY, 0, 0, 0,
&iline,       1, 0, 0, 77 , NULL, "", DISPLAYONLY, 0, 0, 0,
&iline,       1, 0, 0, 86 , NULL, "", DISPLAYONLY, 0, 0, 0,
};

form SEARCHPREISE_WE::LineForm = {8, 0, 0, _LineForm, 0, 0, 0, 0, NULL};

char *SEARCHPREISE_WE::query = NULL;


int SEARCHPREISE_WE::sortbest_term (const void *elem1, const void *elem2)
{
          struct SPREISE_WE *el1;
          struct SPREISE_WE *el2;

          el1 = (struct SPREISE_WE *) elem1;
          el2 = (struct SPREISE_WE *) elem2;
          clipped (el1->best_term);
          clipped (el2->best_term);

          if (dasc_to_long (el1->best_term) > dasc_to_long (el2->best_term))
          {
              return 1 * sobest_term;
          }
          else if (dasc_to_long (el1->best_term) < dasc_to_long (el2->best_term))
          {
              return -1 * sobest_term;
          }

          return 0;
}


void SEARCHPREISE_WE::SortBest_term (HWND hWnd)
{
       qsort (spreise_wetab, anz, sizeof (struct SPREISE_WE),
				   sortbest_term);
       sobest_term *= -1;
}

int SEARCHPREISE_WE::sortwe_dat (const void *elem1, const void *elem2)
{
          struct SPREISE_WE *el1;
          struct SPREISE_WE *el2;

          el1 = (struct SPREISE_WE *) elem1;
          el2 = (struct SPREISE_WE *) elem2;
          clipped (el1->we_dat);
          clipped (el2->we_dat);

          if (dasc_to_long (el1->we_dat) > dasc_to_long (el2->we_dat))
          {
              return 1 * sowe_dat;
          }
          else if (dasc_to_long (el1->we_dat) < dasc_to_long (el2->we_dat))
          {
              return -1 * sowe_dat;
          }

          return 0;
}
void SEARCHPREISE_WE::SortWE_dat (HWND hWnd)
{
       qsort (spreise_wetab, anz, sizeof (struct SPREISE_WE),
				   sortwe_dat);
       sowe_dat *= -1;
}


int SEARCHPREISE_WE::sorta_bz1 (const void *elem1, const void *elem2)
{
          struct SPREISE_WE *el1;
          struct SPREISE_WE *el2;

          el1 = (struct SPREISE_WE *) elem1;
          el2 = (struct SPREISE_WE *) elem2;
          clipped (el1->a_bz1);
          clipped (el2->a_bz1);
          if (strlen (el1->a_bz1) == 0 &&
              strlen (el2->a_bz1) == 0)
          {
              return 0;
          }
          if (strlen (el1->a_bz1) == 0)
          {
              return -1;
          }
          if (strlen (el2->a_bz1) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->a_bz1,el2->a_bz1) * soa_bz1);
}

void SEARCHPREISE_WE::SortABz1 (HWND hWnd)
{
       qsort (spreise_wetab, anz, sizeof (struct SPREISE_WE),
				   sorta_bz1);
       soa_bz1 *= -1;
}

int SEARCHPREISE_WE::sortlief_best (const void *elem1, const void *elem2)
{
          struct SPREISE_WE *el1;
          struct SPREISE_WE *el2;

          el1 = (struct SPREISE_WE *) elem1;
          el2 = (struct SPREISE_WE *) elem2;
          clipped (el1->lief_best);
          clipped (el2->lief_best);
          if (strlen (el1->lief_best) == 0 &&
              strlen (el2->lief_best) == 0)
          {
              return 0;
          }
          if (strlen (el1->lief_best) == 0)
          {
              return -1;
          }
          if (strlen (el2->lief_best) == 0)
          {
              return 1;
          }
		  if (ratod(el1->lief_best) > 0 && ratod(el2->lief_best) > 0)
		  {
	        if (ratod (el1->lief_best) > ratod (el2->lief_best))
			{
				return 1 * solief_best;
			}
			else if (ratod (el1->lief_best) < ratod (el2->lief_best))
			{
				return -1 * solief_best;
			}
		  }

	      return (strcmp (el1->lief_best,el2->lief_best) * solief_best);
}

void SEARCHPREISE_WE::SortLiefBest (HWND hWnd)
{
       qsort (spreise_wetab, anz, sizeof (struct SPREISE_WE),
				   sortlief_best);
       solief_best *= -1;
}

int SEARCHPREISE_WE::sortlief_rech_nr (const void *elem1, const void *elem2)
{
          struct SPREISE_WE *el1;
          struct SPREISE_WE *el2;

          el1 = (struct SPREISE_WE *) elem1;
          el2 = (struct SPREISE_WE *) elem2;
          clipped (el1->lief_rech_nr);
          clipped (el2->lief_rech_nr);
          if (strlen (el1->lief_rech_nr) == 0 &&
              strlen (el2->lief_rech_nr) == 0)
          {
              return 0;
          }
          if (strlen (el1->lief_rech_nr) == 0)
          {
              return -1;
          }
          if (strlen (el2->lief_rech_nr) == 0)
          {
              return 1;
          }

		  if (ratod(el1->lief_rech_nr) > 0 && ratod(el2->lief_rech_nr) > 0)
		  {
	        if (ratod (el1->lief_rech_nr) > ratod (el2->lief_rech_nr))
			{
				return 1 * solief_rech_nr;
			}
			else if (ratod (el1->lief_rech_nr) < ratod (el2->lief_rech_nr))
			{
				return -1 * solief_rech_nr;
			}
		  }

	      return (strcmp (el1->lief_rech_nr,el2->lief_rech_nr) * solief_rech_nr);
}

void SEARCHPREISE_WE::SortLiefRechNr (HWND hWnd)
{
       qsort (spreise_wetab, anz, sizeof (struct SPREISE_WE),
				   sortlief_rech_nr);
       solief_rech_nr *= -1;
}


int SEARCHPREISE_WE::sortpr_ek_b (const void *elem1, const void *elem2)
{
          struct SPREISE_WE *el1;
          struct SPREISE_WE *el2;

          el1 = (struct SPREISE_WE *) elem1;
          el2 = (struct SPREISE_WE *) elem2;
          clipped (el1->pr_ek_b);
          clipped (el2->pr_ek_b);

          if (ratod (el1->pr_ek_b) > ratod (el2->pr_ek_b))
          {
              return 1 * sopr_ek_b;
          }
          else if (ratod (el1->pr_ek_b) < ratod (el2->pr_ek_b))
          {
              return -1 * sopr_ek_b;
          }

          return 0;
}

void SEARCHPREISE_WE::SortPrEkB (HWND hWnd)
{
       qsort (spreise_wetab, anz, sizeof (struct SPREISE_WE),
				   sortpr_ek_b);
       sopr_ek_b *= -1;
}
int SEARCHPREISE_WE::sortpr_ek_w (const void *elem1, const void *elem2)
{
          struct SPREISE_WE *el1;
          struct SPREISE_WE *el2;

          el1 = (struct SPREISE_WE *) elem1;
          el2 = (struct SPREISE_WE *) elem2;
          clipped (el1->pr_ek_w);
          clipped (el2->pr_ek_w);

          if (ratod (el1->pr_ek_w) > ratod (el2->pr_ek_w))
          {
              return 1 * sopr_ek_w;
          }
          else if (ratod (el1->pr_ek_w) < ratod (el2->pr_ek_w))
          {
              return -1 * sopr_ek_w;
          }

          return 0;
}

void SEARCHPREISE_WE::SortPrEkW (HWND hWnd)
{
       qsort (spreise_wetab, anz, sizeof (struct SPREISE_WE),
				   sortpr_ek_w);
       sopr_ek_w *= -1;
}

int SEARCHPREISE_WE::sortme_einh_b (const void *elem1, const void *elem2)
{
          struct SPREISE_WE *el1;
          struct SPREISE_WE *el2;

          el1 = (struct SPREISE_WE *) elem1;
          el2 = (struct SPREISE_WE *) elem2;
          clipped (el1->me_einh_b);
          clipped (el2->me_einh_b);
          if (strlen (el1->me_einh_b) == 0 &&
              strlen (el2->me_einh_b) == 0)
          {
              return 0;
          }
          if (strlen (el1->me_einh_b) == 0)
          {
              return -1;
          }
          if (strlen (el2->me_einh_b) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->me_einh_b,el2->me_einh_b) * some_einh_b);
}

void SEARCHPREISE_WE::SortMeEinhB (HWND hWnd)
{
       qsort (spreise_wetab, anz, sizeof (struct SPREISE_WE),
				   sortme_einh_b);
       some_einh_b *= -1;
}
int SEARCHPREISE_WE::sortme_einh_w (const void *elem1, const void *elem2)
{
          struct SPREISE_WE *el1;
          struct SPREISE_WE *el2;

          el1 = (struct SPREISE_WE *) elem1;
          el2 = (struct SPREISE_WE *) elem2;
          clipped (el1->me_einh_w);
          clipped (el2->me_einh_w);
          if (strlen (el1->me_einh_w) == 0 &&
              strlen (el2->me_einh_w) == 0)
          {
              return 0;
          }
          if (strlen (el1->me_einh_w) == 0)
          {
              return -1;
          }
          if (strlen (el2->me_einh_w) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->me_einh_w,el2->me_einh_w) * some_einh_w);
}

void SEARCHPREISE_WE::SortMeEinhW (HWND hWnd)
{
       qsort (spreise_wetab, anz, sizeof (struct SPREISE_WE),
				   sortme_einh_w);
       some_einh_w *= -1;
}


void SEARCHPREISE_WE::SortLst (int Col, HWND hWnd)
/**
Procedure zum Sortiern der Liste.
**/
{
       switch (Col)
       {
              case 0 :
                  SortABz1 (hWnd);
                  SearchField = 0;
                  break;
              case 1 :
                  SortBest_term (hWnd);
                  SearchField = 1;
                  break;
              case 2 :
                  SortPrEkB (hWnd);
                  SearchField = 2;
                  break;
              case 3 :
                  SortMeEinhB (hWnd);
                  SearchField = 3;
                  break;
              case 4 :
                  SortWE_dat(hWnd);
                  SearchField = 4;
                  break;
              case 5 :
                  SortPrEkW (hWnd);
                  SearchField = 5;
                  break;
              case 6 :
                  SortMeEinhW (hWnd);
                  SearchField = 6;
                  break;
              case 7 :
                  SortLiefBest (hWnd);
                  SearchField = 7;
                  break;
              case 8 :
                  SortLiefRechNr (hWnd);
                  SearchField = 8;
                  break;
              default :
                  return;
       }
       UpdateList ();
}

void SEARCHPREISE_WE::FillFormat (char *buffer, int size)
{
        DlgLst::FillFrmAttr (&DataForm, buffer, size);
}


void SEARCHPREISE_WE::FillCaption (char *buffer, int size)
{
        DlgLst::FillFrmTitle (&UbForm, buffer, 0, size);
}

void SEARCHPREISE_WE::FillVlines (char *buffer, int size)
{
        DlgLst::FillFrmLines (&LineForm, buffer, 0, size);
}


void SEARCHPREISE_WE::FillRec (char *buffer, int i, int size)
{
          DlgLst::FillFrmData (&DataForm, buffer, 0, size);
}


void SEARCHPREISE_WE::UpdateList (void)
{
       int i;
 	   char buffer [512];

       for (i = 0; i < anz; i ++)
       {
          memcpy (&spreise_we, &spreise_wetab[i],  sizeof (struct SPREISE_WE));
          FillRec (buffer, i, 512);
	      Query->UpdateRecord (buffer, i);
       }
}

int SEARCHPREISE_WE::SearchLst (char *sebuff)
/**
Nach Kurzname in Liste suchen.
**/
{
/*
	   Query->SearchList ();
	   return 0;
*/

	   int i;
	   int len;
       char sabuff [80];
       char *pos;

       if (spreise_wetab == NULL) return 0;
	   if (strlen (sebuff) == 0)
	   {
	       Query->SetSel (0);
		   return 0;
	   }

	   for (i = 0; i < anz; i ++)
	   {
		   len = min (16, strlen (sebuff));
           if (SearchField == 0)
           {
                   if (strupcmp (sebuff, spreise_wetab[i].a_bz1, len) == 0) break;
           }
           else if (SearchField == 2)
           {
                   strcpy (sabuff, spreise_wetab[i].pr_ek_b);
                   for (pos = sabuff; *pos <= ' ' && *pos != 0 ; pos ++);
                   pos[len] = 0;
                   strcpy (sabuff, pos);
                   if (ratod (sebuff) == ratod (sabuff)) break;
           }
           else if (SearchField == 3)
           {
                   if (strupcmp (sebuff, spreise_wetab[i].me_einh_b, len) == 0) break;
           }
           else if (SearchField == 6)
           {
                   if (strupcmp (sebuff, spreise_wetab[i].me_einh_w, len) == 0) break;
           }
           else if (SearchField == 7)
           {
                   if (strupcmp (sebuff, spreise_wetab[i].lief_best, len) == 0) break;
           }
	   }
	   if (i == anz) return 0;
	   Query->SetSel (i);
	   return 0;
}


int SEARCHPREISE_WE::Read (char *squery)
/**
Query-Liste fuellen.
**/
{
	  char buffer [512];
	  char q_mdn [128];
	  int cursor;
      double akt_a = 0.0;
	  int i;
      WMESS Wmess;


      if (spreise_wetab)
	  {
           delete spreise_wetab;
           spreise_wetab = NULL;
	  }


      idx = -1;
	  anz = 0;
      SearchField = 1;
      Query->SetSBuff (NULL);
      clipped (squery);
      if (strcmp (squery, " ") == 0) squery[0] = 0;

      if (anz == 0) anz = 0x10000;

      spreise_wetab = new struct SPREISE_WE [anz];
      if (squery == NULL ||
          strlen (squery) == 0)
      {
		   sprintf (q_mdn, " and preise_we.mdn = %d and (lief.mdn = 0 or lief.mdn = %d)",mdn,mdn);
            sprintf (buffer,"select "
			"preise_we.lief,adr.adr_krz,  preise_we.best_blg,  "
			"preise_we.best_term,  preise_we.pr_ek_b,  preise_we.me_einh_b  "
			", preise_we.lief_rech_nr,  preise_we.we_dat,  preise_we.pr_ek_w,preise_we.me_einh_w  "
			"from preise_we,lief,adr "
			"where preise_we.lief = lief.lief and lief.adr = adr.adr");
			strcat (buffer, q_mdn);
			 
              if (query != NULL && strlen (query) > 0)
              {
                  strcat (buffer, " and ");
                  strcat (buffer, query);
              }

              strcat (buffer, " order by preise_we.best_term desc");
      }
      else
      {
		  // dieser Zweig wird hier nicht angesprochen
      }
      DbClass.sqlout ((char *) spreise_we.a, 0, 14);
      DbClass.sqlout ((char *) spreise_we.a_bz1, 0, 25);
      DbClass.sqlout ((char *) spreise_we.lief_best, 0, 9);
      DbClass.sqlout ((char *) spreise_we.best_term, 0, 12);
      DbClass.sqlout ((char *) spreise_we.pr_ek_b, 0, 12);
      DbClass.sqlout ((char *) spreise_we.me_einh_b, 0, 11);
      DbClass.sqlout ((char *) spreise_we.lief_rech_nr, 0, 17);
      DbClass.sqlout ((char *) spreise_we.we_dat, 0, 12);
      DbClass.sqlout ((char *) spreise_we.pr_ek_w, 0, 12);
      DbClass.sqlout ((char *) spreise_we.me_einh_w, 0, 11);
      cursor = DbClass.sqlcursor (buffer);
      if (cursor < 0)
      {
          return -1;
      }

  	  i = 0;
	  while (DbClass.sqlfetch (cursor) == 0)
      {
		  if (atoi (spreise_we.me_einh_b) == 0)
		  {
			  strcpy (spreise_we.me_einh_b,"");
		  } else
		  {
			Ptab.lese_ptab ("me_einh_ek", clipped (spreise_we.me_einh_b));
			strcpy (spreise_we.me_einh_b, ptabn.ptbezk);
		  }
		  if (atoi (spreise_we.me_einh_w) == 0)
		  {
			  strcpy (spreise_we.me_einh_w,"");
		  } else
		  {
	          Ptab.lese_ptab ("me_einh_ek", clipped (spreise_we.me_einh_w));
		      strcpy (spreise_we.me_einh_w, ptabn.ptbezk);
		  }

		  if (dasc_to_long (spreise_we.best_term) == 0) strcpy (spreise_we.best_term,"");
		  if (dasc_to_long (spreise_we.we_dat) == 0) strcpy (spreise_we.we_dat,"");
		  if (atoi (spreise_we.lief_best) == 0 ) strcpy (spreise_we.lief_best,"");
		  double dek = ratod(spreise_we.pr_ek_b);
		  int ek = (int) (dek * 100);
		  if (ek == 0) 
		  {
			  strcpy (spreise_we.pr_ek_b,"");
		  } else
		  {
			  sprintf(spreise_we.pr_ek_b,"%6.2f",dek);
		  }
		  dek = ratod(spreise_we.pr_ek_w);
		  ek = (int) (dek * 100);
		  if (ek == 0) 
		  {
			  strcpy (spreise_we.pr_ek_w,"");
		  } else
		  {
			  sprintf(spreise_we.pr_ek_w,"%6.2f",dek);
		  }


          memcpy (&spreise_wetab[i], &spreise_we, sizeof (struct SPREISE_WE));
          FillRec (buffer, i, 512);
	      Query->InsertRecord (buffer);
		  i ++;
	  }
      anz = i;
	  DbClass.sqlclose (cursor);

      sobest_term = 1;
      if (anz > 1)
      {
             SortWE_dat (Query->GethWnd ());
             SortWE_dat (Query->GethWnd ());
      }
      UpdateList ();

	  EnableWindow (awin, FALSE);
	  return 0;
}

void SEARCHPREISE_WE::FillBox (void)
/**
Query-Liste fuellen.
**/
{
	  char buffer [512];
	  int i;

      for (i = 0; i < anz; i ++)
      {
          memcpy (&spreise_we, &spreise_wetab[i],  sizeof (struct SPREISE_WE));
          FillRec (buffer, i, 512);
	      Query->InsertRecord (buffer);
      }
      if (idx > -1)
      {
	      Query->SetSel (idx);
      }
}


void SEARCHPREISE_WE::SetParams (HINSTANCE hMainInst, HWND hMainWindow)
{
      this->hMainInst   = hMainInst;
      this->hMainWindow = hMainWindow;
      awin = hMainWindow;
}


void SEARCHPREISE_WE::Search (short mdn)
{
  	  int cx, cy;
	  char buffer [512];
      RECT rect;
      BOOL doret = FALSE;

      if (mdn == 0)
      {
          disp_mess ("Mandant 0 ist nicht erlaubt", 2);
          syskey = KEY5;
          return;
      }
      this->mdn = mdn;
      Settchar ('|');
      GetClientRect (hMainWindow, &rect);
      cx = 80;
      cy = 25;
      SetSortProc (SortLst);
      if (Query == NULL)
      {
        idx = -1;
        if (query != NULL && strlen (query) > 0)
        {
               Query = new CHQEX (cx, cy);
        }
        else
        {
               Query = new CHQEX (cx, cy);
//               Query = new CHQEX (cx, cy, "Name", "");
        }
        Query->EnableSort (TRUE);
        if (SetStyle)
        {
            Query->SetWindowStyle (WindowStyle);
        }
        Query->OpenWindow (hMainInst, hMainWindow, TRUE);
        FillFormat (buffer, 512);
        Query->RowAttr (buffer);
        FillVlines (buffer, 512);
	    Query->VLines (buffer, 3);
	    EnableWindow (awin, FALSE);

        FillCaption (buffer, 512);
	    Query->InsertCaption (buffer);

	    Query->SetFillDb (Read);
	    Query->SetSearchLst (SearchLst);
//        if (query != NULL && strlen (query) > 0)
        {
              Read ("");
              if (anz == 0)
              {
                  doret = TRUE;
              }
        }
      }
      else
      {
        Query->RestorefWork ();
        if (SetStyle)
        {
            Query->SetWindowStyle (WindowStyle);
        }
        Query->OpenWindow (hMainInst, hMainWindow, TRUE);
        FillFormat (buffer, 512);
        Query->RowAttr (buffer);
        FillVlines (buffer, 512);
	    Query->VLines (buffer, 3);
	    EnableWindow (awin, FALSE);

        FillCaption (buffer, 512);
	    Query->InsertCaption (buffer);

	    Query->SetFillDb (Read);
	    Query->SetSearchLst (SearchLst);
        FillBox ();
      }
      if (doret == FALSE)
      {
          Query->ProcessMessages ();
          idx = Query->GetSel ();
      }
      else
      {
          idx = -1;
      }
      query = NULL;
      EnableWindow (awin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (awin);
      if (spreise_wetab == NULL)
      {
          delete Query;
          Query = NULL;
      }
	  if (idx == -1) return;
      memcpy (&spreise_we, &spreise_wetab[idx], sizeof (spreise_we));
      SetSortProc (NULL);
      if (spreise_wetab != NULL)
      {
          Query->SavefWork ();
      }
}



