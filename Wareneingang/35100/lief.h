#ifndef _LIEF_DEF
#define _LIEF_DEF

#include "dbclass.h"

struct LIEF {
   char      abr[2];
   long      adr;
   char      bank_nam[37];
   long      bbn;
   short     best_sort;
   char      best_trans_kz[2];
   long      blz;
   char      bonus_kz[2];
   short     fil;
   char      fracht_kz[2];
   char      ink[17];
   long      kreditor;
   char      kto[17];
   long      letzt_lief;
   char      lief[17];
   char      lief_kun[17];
   char      lief_rht[2];
   long      lief_s;
   short     lief_typ;
   short     lief_zeit;
   double    lief_zusch;
   long      lst_nr;
   short     mahn_stu;
   short     mdn;
   char      me_kz[2];
   char      min_me_kz[2];
   double    min_zusch;
   double    min_zusch_proz;
   char      nach_lief[2];
   char      ordr_kz[2];
   char      rab_abl[2];
   char      rab_kz[2];
   char      rech_kz[2];
   double    rech_toler;
   short     sdr_nr;
   long      son_kond;
   short     sprache;
   char      steuer_kz[2];
   char      unt_lief_kz[2];
   char      vieh_bas[2];
   char      we_erf_kz[2];
   char      we_kontr[2];
   char      we_pr_kz[2];
   double    we_toler;
   short     zahl_kond;
   char      zahlw[2];
   short     hdklpargr;
   long      vorkgr;
   short     delstatus;
   short     mwst;
   short     liefart;
   char      modif[2];
   char      ust_id[17];
   char      eg_betriebsnr[21];
   short     eg_kz;
   short     waehrung;
   char      iln[17];
   short     zertifikat1;
   short     zertifikat2;
   char      iban1[5];
   char      iban2[5];
   char      iban3[5];
   char      iban4[5];
   char      iban5[5];
   char      iban6[5];
   char      iban7[4];
   char      esnum[11];
   char      eznum[11];
   short     geburt;
   short     mast;
   short     schlachtung;
   short     zerlegung;
};
extern struct LIEF lief, lief_null;

#line 7 "lief.rh"

class LIEF_CLASS : public DB_CLASS 
{
       private :
               char qstring0[0x1000];
               char qstring0Ex[0x1000];
               int qschange;
               int qschangeEx;
               void prepare (void);
               int cursor_ausw;
               int prep_awcursor (char *);
       public :
               LIEF_CLASS () : DB_CLASS ()
               {
                    strcpy (qstring0, "Start");
                    strcpy (qstring0Ex, "Start");
 			  cursor_ausw = -1;
               }
               int dbreadfirst (void);
               int PrepareQueryMdn (form *, char *[]);
               int PrepareQuery (form *, char *[]);
               int PrepareQuery (void);
               int PrepareQueryMdn (void);
               int ShowBuQuery (HWND, int); 
               void SetTestLiefProc (int (*)(char *));
		       void SetNew (void);
		       void SetNewEx (void);
               int PrepareQueryEx (form *, char *[]);
               int ShowBuQueryEx (HWND, int); 
               void SetSearchModeLief (int mode);
               void SetSearchFieldLief (int);
               void SetCharBuffLief (char *);
		       void ListToMamain1 (HWND);
};
#endif
