#ifndef _ABSCHLWORK_DEF
#define _ABSCHLWORK_DEF
#include "dlg.h"
#include "itprog.h"
#include "itfont.h"
#include "formfield.h"
#include "cmask.h"
#include "we_kopf.h"
#include "we_jour.h"
#include "lief.h"
#include "ptab.h"
#include "a_bas.h"

struct ABSCHL
{
          char anz_pos [10];
          char anz_mang [10];
          char vk_wrt_eu [20];
          char fil_ek_wrt_eu [20];
          char sp_ek_vk [20];
          char lief_wrt_o_eu [20];
          char rab_eff_eu [20];
          char rab_eff_a_eu [20];
          char rab_eff_lief_eu [20];
          char lief_wrt_ab_eu [20];
          char lief_wrt_nto_eu [20];
          char mwst_ges_eu [20];
          char zusch_eu [20];
          char lief_zusch_eu [20];
          char ink_zusch_eu [20];
          char lief_wrt_eu [20];
          char mwst_sl1 [20];
          char mwst_sl2 [20];
          char mwst_sl3 [20];
          char mwst_val1 [20];
          char mwst_val2 [20];
          char mwst_val3 [20];
		  char lfd [10];
};

class AbschlWork
{
          private :
             WE_JOUR_CLASS WeJourClass;
             PTAB_CLASS Ptab;
			 BOOL stichtag;
			 BOOL stichtagOK;
             char tag [33];

          public :
             static BOOL Weldr; 
             AbschlWork ()
             {
				 stichtag = 0;
				 stichtagOK = FALSE;
				 strcpy (tag, "01.01.2007");
             }
             ~AbschlWork ()
             {
             }

             void FillMwstSlCombo (char **, int, int);
			 BOOL Stichtag ();
             int  GetMwstAnz (void);
             double GetMwstFaktor (int mwst);
             double CalcVSteuer (void);
             void WriteWeKopf (void);
};
#endif