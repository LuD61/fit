#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "wmaskc.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "rind.h"

struct RIND rind, rind_null;

void RIND_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) rind.ls_ident, 0, sizeof (rind.ls_ident));
    out_quest ((char *) &rind.a,3,0);
    out_quest ((char *) rind.ls_ident,0,21);
    out_quest ((char *) rind.ls_charge,0,21);
    out_quest ((char *) rind.ls_ez,0,21);
    out_quest ((char *) rind.ls_es,0,21);
    out_quest ((char *) rind.ls_land,0,21);
    out_quest ((char *) &rind.gebland,1,0);
    out_quest ((char *) &rind.mastland,1,0);
    out_quest ((char *) &rind.schlaland,1,0);
    out_quest ((char *) &rind.zerlland,1,0);
            cursor = prepare_sql ("select rind.a,  "
"rind.ls_ident,  rind.ls_charge,  rind.ls_ez,  rind.ls_es,  rind.ls_land,  "
"rind.gebland,  rind.mastland,  rind.schlaland,  rind.zerlland from rind "

#line 20 "rind.rpp"
                                  "where ls_ident = ?");
    ins_quest ((char *) &rind.a,3,0);
    ins_quest ((char *) rind.ls_ident,0,21);
    ins_quest ((char *) rind.ls_charge,0,21);
    ins_quest ((char *) rind.ls_ez,0,21);
    ins_quest ((char *) rind.ls_es,0,21);
    ins_quest ((char *) rind.ls_land,0,21);
    ins_quest ((char *) &rind.gebland,1,0);
    ins_quest ((char *) &rind.mastland,1,0);
    ins_quest ((char *) &rind.schlaland,1,0);
    ins_quest ((char *) &rind.zerlland,1,0);
            sqltext = "update rind set rind.a = ?,  "
"rind.ls_ident = ?,  rind.ls_charge = ?,  rind.ls_ez = ?,  "
"rind.ls_es = ?,  rind.ls_land = ?,  rind.gebland = ?,  "
"rind.mastland = ?,  rind.schlaland = ?,  rind.zerlland = ? "

#line 22 "rind.rpp"
                                  "where ls_ident = ?";
            ins_quest ((char *) rind.ls_ident, 0, sizeof (rind.ls_ident));
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) rind.ls_ident, 0, sizeof (rind.ls_ident));
            test_upd_cursor = prepare_sql ("select a from rind "
                                  "where ls_ident = ?");
            ins_quest ((char *) rind.ls_ident, 0, sizeof (rind.ls_ident));
            del_cursor = prepare_sql ("delete from rind "
                                  "where ls_ident = ?");
    ins_quest ((char *) &rind.a,3,0);
    ins_quest ((char *) rind.ls_ident,0,21);
    ins_quest ((char *) rind.ls_charge,0,21);
    ins_quest ((char *) rind.ls_ez,0,21);
    ins_quest ((char *) rind.ls_es,0,21);
    ins_quest ((char *) rind.ls_land,0,21);
    ins_quest ((char *) &rind.gebland,1,0);
    ins_quest ((char *) &rind.mastland,1,0);
    ins_quest ((char *) &rind.schlaland,1,0);
    ins_quest ((char *) &rind.zerlland,1,0);
            ins_cursor = prepare_sql ("insert into rind (a,  "
"ls_ident,  ls_charge,  ls_ez,  ls_es,  ls_land,  gebland,  mastland,  schlaland,  "
"zerlland) "

#line 33 "rind.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?)"); 

#line 35 "rind.rpp"
}

