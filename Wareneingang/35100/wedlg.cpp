//050809 damit es beim Scannen sauber l�uft
#include<stdio.h>
#include<stdarg.h>
#include<string.h>
#include<windows.h>
#include "stdfkt.h"
#include "strfkt.h"
#include "wedlg.h" 
#include "mo_gdruck.h" 
#include "colbut.h"
#include "help.h"
#include "lbox.h"
#include "lief_bzg.h"
#include "we_kopf.h"
#include "a_bas.h"
#include "searchmdn.h"
#include "searchfil.h"
#include "searchlief.h"
#include "searchwekopf.h"
#include "mo_nr.h"
#include "listdlg.h"
#include "listclex.h"
#include "wedlglst.h"
#include "searchbest.h"
#include "textdlg.h"
#include "abschldlg.h"
#include "70001.h"
#include "mo_lsgen.h"
#include "choisedlg.h"
#include "best_kopf.h"
#include "mo_menu.h"
#ifdef BIWAK
#include "conf_env.h"
#endif


#ifdef _DEBUG
#include <crtdbg.h>
extern _CrtMemState memory;
#endif

static BOOL DatError;
int WeDlg::StdSize = STDSIZE;
int WeDlg::InfoSize = 150;

int WeDlg::dlgsize    = STDSIZE;
int WeDlg::ltgraysize = STDSIZE;
int WeDlg::dlgpossize = STDSIZE;
int WeDlg::cpsize     = 200;


mfont WeDlg::dlgfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         GRAYCOL,
                         0,
                         NULL};

mfont WeDlg::InfoFont = {
                         "ARIAL", InfoSize, 0, 0,
                         BLUECOL,
                         LTGRAYCOL,
                         10,
                         NULL};

COLORREF WeDlg::InfShadow = GRAYCOL;

mfont WeDlg::ltgrayfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         LTGRAYCOL,
                        0,
                         NULL};

mfont WeDlg::dlgposfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         LTGRAYCOL,
                         0,
                         NULL};


mfont WeDlg::cpfont = {
                        "Times New Roman", 200, 0, 1,
                         BLUECOL,
                         GRAYCOL,
                         0,
                         NULL};

mfont *WeDlg::Font = &dlgposfont;

struct WEKOPFF
{
      char mdn [7];
      char mdn_krz [20];
      char fil [7];
      char fil_krz [20];
      char lief [18];
      char lief_krz [20];
	  char lief_rech_nr [20];
	  char blg_typ [4];
	  char blg_typ_bz [38];
	  char a_best_info [38];
	  char embinh [15];
	  char we_status [6];
	  char we_status_bz [38];
	  char a_best_kz [10];
	  char a_best_kz_bz [38];
	  char bsd_verb_kz [5];
	  char we_dat [15];
	  char blg_eing_dat [15];
	  char lfd [10];
	  char koresp_best0 [18];
	  char pr_erf_kz [5];
	  char pr_erf_kz_bz [38];
      char ls_charge [31];
      char ls_ident [21];

} wekopff, wekopff_null;


CFIELD *WeDlg::_fHead [] = {
                     new CFIELD ("mdn_txt", "Mandant",  0, 0, 1, 1,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("mdn", wekopff.mdn,  6, 0, 10, 1,  NULL, "%4d", CEDIT,
                                 MDN_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("mdn_choise", "", 2, 0, 16, 1, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("mdn_krz", wekopff.mdn_krz, 19, 0, 19, 1,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, 0),
                     new CFIELD ("fil_txt", "Filiale",  0, 0, 50, 1,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("fil", wekopff.fil,  6, 0, 58, 1,  NULL, "%4d", CEDIT,
                                 FIL_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("fil_choise", "", 2, 0, 64, 1, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("fil_krz", wekopff.fil_krz, 19, 0, 67, 1,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, 0),
                     new CFIELD ("lief_txt", "Lieferant",  0, 0, 1, 3,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("lief", wekopff.lief, 17, 0, 10, 3,  NULL, "", CEDIT,
                                 LIEF_CTL, Font, 0, 0),
                     new CFIELD ("lief_choise", "", 2, 0, 27, 3, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("lief_krz", wekopff.lief_krz, 19, 0, 30, 3,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, 0),
                     new CFIELD ("lief_rech_nr_txt", "Lief/Rech",  0, 0, 1, 4,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("lief_rech_nr", wekopff.lief_rech_nr, 17, 0, 10, 4,  NULL, "", 
					             CEDIT,
                                 LIEF_RECH_NR_CTL, Font, 0, 0),
                     new CFIELD ("lief_rech_nr_choise", "", 2, 0, 27, 4, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("blg_typ_txt", "Belegtyp", 0, 0, 1, 5,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("blg_typ", wekopff.blg_typ, 3, 0,24, 5,  NULL, "", 
                                 CEDIT,
                                 BLG_TYP_CTL, Font, 0, ES_UPPERCASE),
                     new CFIELD ("blg_typ_choise", "", 2, 0,27, 5,  NULL, "", CBUTTON, 
                                 VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("blg_typ_bz", wekopff.blg_typ_bz, 19, 0, 30, 5,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, 0),
                     new CFIELD ("we_erf_info_txt", "Erfassen �ber", 0, 0, 60, 4, NULL, "",
					              CDISPLAYONLY,
							     500, Font, 0, TRANSPARENT),
                     new CFIELD ("we_erf_info", wekopff.a_best_info, 16, 0, 60, 5, NULL, "",
					              CREADONLY,
							  500, Font, 0, TRANSPARENT | WS_BORDER),
//                     new CFIELD ("we_erf_info", wekopff.a_best_info, 16, 0, 60, 5, NULL, "",
//					              CDISPLAYONLY,
//							  500, Font, 0, TRANSPARENT),
                     NULL,
};

CFORM WeDlg::fHead (6, _fHead);


CFIELD *WeDlg::_fPos [] = {
                     new CFIELD ("pos_frame", "",    85,16, 1, 7,  NULL, "",
                                 CBORDER,   
                                 500, Font, 0, TRANSPARENT),    
                     new CFIELD ("we_status_txt", "Status", 0, 0, 2, 8, NULL, "",
					              CDISPLAYONLY,
								  500, Font, 0, 0),
                     new CFIELD ("we_status", wekopff.we_status, 3, 0, 14, 8, NULL, "",
					              CREADONLY,
								  500, Font, 0, 0),
//                     new CFIELD ("we_status_bz", wekopff.we_status_bz, 20, 0, 20, 8,  NULL, "", 
//                                 CREADONLY,
//                                 500, Font, 0, 0),
                     new CFIELD ("we_erf_kz_txt", "Art./Best", 0, 0, 2, 9, NULL, "",
					              CDISPLAYONLY,
								  500, Font, 0, 0),
                     new CFIELD ("we_erf_kz", wekopff.a_best_kz, 3, 0, 14, 9, NULL, "%1d",
					              CEDIT,
								  A_BEST_KZ_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("we_erf_kz_choise", "", 2, 0,17, 9,  NULL, "", CBUTTON, 
                                 VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("we_erf_kz_bz", wekopff.a_best_kz_bz, 20, 0, 20, 9,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, 0),
                     new CFIELD ("bsd_verb_kz_txt", "Best.�nd.", 3, 0, 2,10, NULL, "",
					              CDISPLAYONLY,
								  500, Font, 0, 0),
                     new CFIELD ("bsd_verb_kz", "    ", wekopff.bsd_verb_kz,
                                 3, 0, 14, 10,  NULL, "", CBUTTON,
                                 DEFAULT_LIEF_KZ_CTL, Font, 0, BS_AUTOCHECKBOX),
/*
                     new CFIELD ("bsd_verb_kz", wekopff.bsd_verb_kz, 3, 0, 14, 10, NULL, "",
					              CEDIT,
								  BSD_VERB_KZ_CTL, Font, 0, ES_UPPERCASE),
*/
                     new CFIELD ("we_dat_txt", "Eing.Datum", 0,	0, 2,11,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("we_dat", wekopff.we_dat, 11, 0,14,11,  NULL, "dd.mm.yyyy", 
                                 CEDIT,
                                 WE_DAT_CTL, Font, 0, 0),
                     new CFIELD ("blg_eing_dat_txt", "Belegdatum", 0,	0, 2,12,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("blg_eing_dat", wekopff.blg_eing_dat, 11, 0,14,12,  NULL, 
					             "dd.mm.yyyy", 
                                 CEDIT,
                                 BLG_EING_DAT_CTL, Font, 0, 0),
                     new CFIELD ("lfd_txt", "WE-Nummer", 0,	0, 2,13,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("lfd", wekopff.lfd, 10, 0,14,13,  NULL, 
					             "%8d", 
                                 CREADONLY,
                                 500, Font, 0, ES_RIGHT),
                     new CFIELD ("koresp_best0_txt", "BestellNr", 0,	0, 2,14,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("koresp_best0", wekopff.koresp_best0, 10, 0,14,14,  NULL, 
					             "%8d", 
                                 CREADONLY,
                                 KORESP_BEST0_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("we_pr_kz_txt", "EK-Erfass.", 0,	0, 2,15,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("we_pr_kz", wekopff.pr_erf_kz, 3, 0,14,15,  NULL, 
					             "", 
                                 CEDIT,
                                 PR_ERF_KZ_CTL, Font, 0, 0),
                     new CFIELD ("we_pr_kz_choise", "", 2, 0,17, 15,  NULL, "", CBUTTON, 
                                 VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("we_pr_kz_bz", wekopff.pr_erf_kz_bz, 20, 0, 20, 15,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, 0),
                     new CFIELD ("liste", ". . .", 26, 2, 14, 17,  NULL, "", CBUTTON, 
                                 VK_F6, Font, 0, BS_BITMAP),
                     new CFIELD ("ls_ident_txt", "Ident-Nr", 0, 0,45, 9, NULL, "",
					              CDISPLAYONLY,
								  500, Font, 0, 0),
                     new CFIELD ("ls_ident", wekopff.ls_ident, 21, 0, 59, 9, NULL, "",
					              CEDIT,
								  LS_IDENT_CTL, Font, 0, 0),
                     new CFIELD ("ls_charge_txt", "Chargen-Nr", 0, 0,45,10, NULL, "",
					              CDISPLAYONLY,
								  500, Font, 0, 0),
                     new CFIELD ("ls_charge", wekopff.ls_charge, 31, 0, 59,10, NULL, "",
					              CEDIT,
								  LS_IDENT_CTL, Font, 0, 0),
                     NULL,
};

CFORM WeDlg::fPos (6, _fPos);



CFIELD *WeDlg::_fWeKopf [] = {
                     new CFIELD ("fBzg1", (CFORM *) &fHead, 0, 0, 0, 0, NULL, "", CFFORM,
                                   HEADCTL, Font, 0, 0),
                     new CFIELD ("fBzg2", (CFORM *) &fPos,  0, 0, 0, 0, NULL, "", CFFORM,
                                   POSCTL, Font, 0, 0),
                     NULL,                      
};

CFORM WeDlg::fWeKopf (3, _fWeKopf);

CFIELD *WeDlg::_fWeKopf0 [] = { 
                     new CFIELD ("fWeKopf", (CFORM *) &fWeKopf, 0, 0, 1, 1, NULL, "", CFFORM,
                                   500, Font, 0, 0),
                     NULL,
};
CFORM WeDlg::fWeKopf0 (1, _fWeKopf0);



CFIELD *WeDlg::_fPrintFoot [] = {
                     new CFIELD ("ok",      "    OK    ", 10, 0, 0, 0,  NULL, "", 
                                 CBUTTON,
                                 OK_CTL, Font, 0, 0),
                     new CFIELD ("cancel", " Abbruch  ", 10, 0,12, 0,  NULL, "", 
                                 CBUTTON,
                                 CANCEL_CTL, Font, 0, 0),
                     NULL,
};

CFORM WeDlg::fPrintFoot (2, _fPrintFoot);

CFIELD *WeDlg::_fPrintChoise0[] = {
                     new CFIELD ("beleg", "Kontrollbeleg", Beleg,
                                 18, 0, -1, 1,  NULL, "", CBUTTON,
                                 BELEG_CTL, Font, 0, BS_AUTOCHECKBOX),
                     new CFIELD ("journal", "WE-Journal", Journal,
                                 18, 0, -1, 3,  NULL, "", CBUTTON,
                                 JOURNAL_CTL, Font, 0, BS_AUTOCHECKBOX),
                     new CFIELD ("fPrintFoot", (CFORM *) &fPrintFoot, 0, 0, -1, -2, NULL, "", 
					              CFFORM,
                                  500, Font, 0, 0),
                     NULL,
};

CFIELD *WeDlg::_fPrintChoise1[] = {
                     new CFIELD ("beleg", "Kontrollbeleg",  Beleg,
                                 18, 0, -1, 1,  NULL, "", CBUTTON,
                                 BELEG_CTL, Font, 0, 0),
                     new CFIELD ("journal", "WE-Journal", Journal,
                                 18, 0, -1, 3,  NULL, "", CBUTTON,
                                 JOURNAL_CTL, Font, 0, 0),
                     new CFIELD ("fPrintFoot", (CFORM *) &fPrintFoot, 0, 0, -1, -2, NULL, "", 
					              CFFORM,
                                  500, Font, 0, 0),
                     NULL,
};


CFIELD **WeDlg::_fPrintChoise = _fPrintChoise0;

CFORM WeDlg::fPrintChoise (3, _fPrintChoise);


void WeDlg::SetPrintChoiseMode (int mode)
{
	if (mode == 1)
	{
		_fPrintChoise = _fPrintChoise1;
	}
}

COLORREF WeDlg::Colors[] = {BLACKCOL,
                            WHITECOL,
                            WHITECOL,
                            WHITECOL};

COLORREF WeDlg::BkColors[] = {WHITECOL,
                            BLUECOL,
                            BLACKCOL,
                            GRAYCOL};

COLORREF WeDlg::QmTextBkColor = LTGRAYCOL;

WeDlg *WeDlg::ActiveDlg = NULL;

int WeDlg::LsChargeAttr = 2;
int WeDlg::Ruecksprung = 2; //R�cksprung auf lief

int WeDlg::LsIdentAttr = 2;
char *WeDlg::ChargeMacro = NULL;
BOOL WeDlg::ListButtons = TRUE;
int WeDlg::ButtonSize = SMALL;
char WeDlg::Format [20] = {"35502"};
char WeDlg::JoFormat [20] = {"35501"};
BOOL WeDlg::PrChoise = FALSE;
char WeDlg::Beleg [2];
char WeDlg::Journal [2];
BOOL WeDlg::ReadBest = FALSE;
BOOL WeDlg::AutoBest = FALSE;
int  WeDlg::ListType = 0;
BOOL WeDlg::WithSearchBest = FALSE;
BOOL WeDlg::InEnterPos = FALSE;
BOOL WeDlg::SysParKreditor = TRUE;
BOOL WeDlg::RwKopf = TRUE;
int WeDlg::we_dat_plus = 1;
int WeDlg::we_dat_minus = 20;
int WeDlg::blg_eing_dat_plus = 1;
int WeDlg::blg_eing_dat_minus = 10;
int WeDlg::flg_Storno = FALSE;


void WeDlg::TestAttr (void)
{
    switch (LsChargeAttr)
    {
          case 0 :
              fPos.GetCfield ("ls_charge_txt")->SetAttribut (CREMOVED);
              fPos.GetCfield ("ls_charge")->SetAttribut (CREMOVED);
              break;
          case 1 :
              fPos.GetCfield ("ls_charge_txt")->SetAttribut (CDISPLAYONLY);
              fPos.GetCfield ("ls_charge")->SetAttribut (CREADONLY);
              break;
          case 2 :
              fPos.GetCfield ("ls_charge_txt")->SetAttribut (CDISPLAYONLY);
              fPos.GetCfield ("ls_charge")->SetAttribut (CEDIT);
              break;
    }
    switch (LsIdentAttr)
    {
          case 0 :
              fPos.GetCfield ("ls_ident_txt")->SetAttribut (CREMOVED);
              fPos.GetCfield ("ls_ident")->SetAttribut (CREMOVED);
              fPos.GetCfield ("ls_charge_txt")->SetY 
                            (fPos.GetCfield ("ls_charge_txt")->GetYorg () - 1);
              fPos.GetCfield ("ls_charge")->SetY 
                            (fPos.GetCfield ("ls_charge")->GetYorg () - 1);
              break;
          case 1 :
              fPos.GetCfield ("ls_ident_txt")->SetAttribut (CDISPLAYONLY);
              fPos.GetCfield ("ls_ident")->SetAttribut (CREADONLY);
              break;
          case 2 :
              fPos.GetCfield ("ls_ident_txt")->SetAttribut (CDISPLAYONLY);
              fPos.GetCfield ("ls_ident")->SetAttribut (CEDIT);
              break;
    }
}


void WeDlg::DestroyHead (void)
{
    fHead.destroy ();
    fWeKopf.SetFieldanz (fWeKopf.GetFieldanz () - 1);
}

void WeDlg::DestroyPos (void)
{
    fPos.destroy ();
    fWeKopf.SetFieldanz (fWeKopf.GetFieldanz () - 1);
}

void WeDlg::SetPos (void)
{
    fWeKopf.SetFieldanz (2);
    fWeKopf0.Invalidate (TRUE);
    fWeKopf0.MoveWindow ();
    UpdateWindow (hWnd);
    if (hwndList != NULL)
    {
             *hwndList = fPos.GetCfield ("liste")->GethWnd ();
    }
    fPos.GetCfield ("liste")->CreateQuikInfos (hMainWindow);
    SendMessage (fPos.GetCfield ("liste")->GethWnd (),
                      BM_SETIMAGE, (WPARAM) IMAGE_BITMAP, 
					  (LPARAM) ListBitmap);
}



static struct PMENUE MenuePosEnter = {"Positionen erfassen F6",      "G",  NULL, IDM_LIST}; 
static struct PMENUE MenueInsert   = {"einf�gen F6",                 "G",  NULL, IDM_INSERT}; 

WeWork WeDlg::weWork;
int WeDlg::ListRows = 0;
BOOL WeDlg::InsMode = FALSE;
BOOL WeDlg::WriteOK = FALSE;
BOOL WeDlg::NewRec = FALSE;
HBITMAP WeDlg::ListBitmap  = NULL;
//HBITMAP WeDlg::ListMask    = NULL;
HBITMAP WeDlg::ListBitmapi = NULL;
BitImage WeDlg::ImageDelete;
BitImage WeDlg::ImageInsert;
BitImage WeDlg::ImageList;
COLORREF WeDlg::SysBkColor = LTGRAYCOL;

char *WeDlg::HelpName = "35100.cmd";


void WeDlg::ChangeF6 (DWORD Id)
{

    if (Id == IDM_LIST)
    {
         if (DeleteMenu (ActiveDlg->GethMenu (), IDM_LIST , MF_BYCOMMAND)) 
         {
                InsertMenu (ActiveDlg->GethMenu (), IDM_DOCKLIST, MF_BYCOMMAND | MF_STRING, 
                            MenueInsert.menid, MenueInsert.text);
                            
         }
    }
    else if (Id == IDM_INSERT)
    {
         if (DeleteMenu (ActiveDlg->GethMenu (), IDM_INSERT , MF_BYCOMMAND))
         {
                 InsertMenu (ActiveDlg->GethMenu (), IDM_DOCKLIST, MF_BYCOMMAND | MF_STRING, 
                            MenuePosEnter.menid, MenuePosEnter.text);
         }
    }
}


int WeDlg::ReadBzgMdn (void)
{
         if (syskey == 0) return 0;
         if (syskey == KEY5) return 0;
         if (syskey == KEY9) return 0;

         fHead.GetText ();
         if (atoi (wekopff.mdn) == 0)
         {
             disp_mess ("Mandant > 0 eingeben", 2);
             fHead.SetCurrentName ("mdn");
             return 0;
         }
         FromForm (dbheadfields);
         if (weWork.GetMdnName (wekopff.mdn_krz,   atoi (wekopff.mdn)) != 0)
         {
             disp_mess ("Mandant nicht gefunden", 2);
             fHead.SetCurrentName ("mdn");
             return 0;
         }
         if (atoi (wekopff.mdn) == 0)
         {
             sprintf (wekopff.fil, "%4d", 0);
             weWork.GetFilName (wekopff.fil_krz,   atoi (wekopff.mdn), atoi (wekopff.fil));
             Enable (&fWeKopf, EnableHeadFil, FALSE); 
             fHead.SetCurrentName ("lief");
         }
         else
         {
             if (fHead.GetCfield ("fil")->IsDisabled ())
             {
                    Enable (&fWeKopf, EnableHeadFil, TRUE); 
                    fHead.SetCurrentName ("fil");
             }
         }
         if (syskey == KEYUP)
         {
                    fHead.SetCurrentName ("lief");
         }
         fHead.SetText ();
         EnableMenuItem (ActiveDlg->GethMenu (), IDM_CHOISE,   MF_GRAYED);
         return 0;
}

int WeDlg::ReadBzgFil (void)
{
          
         if (syskey == 0) return 0;
         if (syskey == KEY9) return 0;
         fPos.GetText ();
         FromForm (dbheadfields);
         if (weWork.GetFilName (wekopff.fil_krz,   atoi (wekopff.mdn), atoi (wekopff.fil)) != 0)
         {
             disp_mess ("Filiale nicht gefunden", 2);
             fHead.SetCurrentName ("mdn");
             return 0;
         }
         fHead.SetText ();
         EnableMenuItem (ActiveDlg->GethMenu (), IDM_CHOISE,   MF_GRAYED);
         return 0;
}

int WeDlg::ReadBzgLief (void)
{
        
         if (DLG::MousePressed)
         {
             return FALSE;
         }

         if (syskey != KEYCR)
         {
             return FALSE;
         }

         
         if (syskey == 0) return 0;
         if (syskey == KEY9) return 0;

         fHead.GetText ();
         if (wekopff.lief[0] == 0)
         {
             return 0;
         }
         if (SysParKreditor && !numeric (wekopff.lief))
         {
             if (ActiveDlg->ShowLief (wekopff.lief) == FALSE)
             {
                      fHead.SetCurrentName ("lief");
                      return 0;
             }
         }
         FromForm (dbheadfields);
         if (weWork.GetLiefName (wekopff.lief_krz, we_kopf.mdn, we_kopf.lief) == 100)
         {
             disp_mess ("Lieferant ist nicht angelegt", 2);
             fHead.SetCurrentName ("lief");
             SetLiefRechNr (FALSE);
             return TRUE;
         }

         fHead.SetText ();
         EnableMenuItem (ActiveDlg->GethMenu (), IDM_CHOISE,   MF_GRAYED);
         SetLiefRechNr (TRUE);
//         if (AutoBest)
//         {
//             ActiveDlg->ShowBest ();  Hier zu gef�hrlich!!!
//         }
         return 0;
}

int WeDlg::ReadWe (void)
{


         fHead.GetText ();
		 if (wekopff.blg_typ[0] <= ' ')
		 {
			 strcpy (wekopff.blg_typ, "L");
		 }
         weWork.GetPtab (wekopff.blg_typ_bz, "blg_typ",  
		  			     wekopff.blg_typ);
         fHead.GetCfield ("blg_typ_bz")->SetText ();
         EnableMenuItem (ActiveDlg->GethMenu (), IDM_CHOISE,   MF_GRAYED);
		 return 0;
}


void WeDlg::TestEnablePrint (void)
{
         if (weWork.GetWePosAnz () > 0)
         {
                 ToolBar_SetState (ActiveDlg->GethwndTB (), IDM_PRINT,   TBSTATE_ENABLED);
                 EnableMenuItem (ActiveDlg->GethMenu (),    IDM_PRINT,   MF_ENABLED);
         }
}

		 

int WeDlg::EnterPos (void)
{
         int dsqlstatus;
         char datum [12];

         AutoNrClass AutoNr;

		 flg_Storno = FALSE;
         switch (syskey)
         {
               case KEYDOWN :
               case KEYTAB :
                   fHead.SetCurrentName ("mdn");
                   return TRUE;
         }
        
         if (DLG::MousePressed)
         {
             return FALSE;
         }

         if (syskey != KEYCR)
         {
             return FALSE;
         }
         
         fHead.GetText ();
         clipped (wekopff.lief_rech_nr);
         if (strcmp (wekopff.lief_rech_nr, " ") == 0)
         {
                  wekopff.lief_rech_nr[0] = 0;
         }

         if (wekopff.lief_rech_nr[0] == 0)
         {
             disp_mess ("Lieferscheinnummer ung�ltig", 2);
			 fHead.SetCurrentName ("lief_rech_nr");
             return 0;
         }

		 if (weWork.GetPtab (wekopff.blg_typ_bz,
				             "blg_typ",  
							 wekopff.blg_typ) == -1)
		 {
			 disp_mess ("Falsche Eingabe", 2);
			 fHead.SetCurrentName ("blg_typ");
			 return 0;
		 }

         ToolBar_SetState (ActiveDlg->GethwndTB (), IDM_PRINT,   TBSTATE_INDETERMINATE);
         EnableMenuItem (ActiveDlg->GethMenu (),    IDM_PRINT,   MF_GRAYED);
		 fHead.GetCfield ("blg_typ_bz")->SetText ();


         FromForm (dbheadfields);
         sysdate (datum);

		 we_kopf.lfd = 0;
		 dsqlstatus = weWork.ReadWeKopf ();
         if (AutoBest && we_kopf.we_status == 0)  //TESTTESTTEST
         {
             ActiveDlg->ShowBest ();
         }
		 /*
		 if (dsqlstatus == 0 && we_kopf.we_status > 3)
		 {
             disp_mess ("Der Wareneingang wurde schon abgeschlossen", 2);
			 fHead.SetCurrentName ("lief");
			 return 0;
		 }
		 */
       HWND hWnd;

       hWnd = GetFocus ();
		if (dsqlstatus == 0 && we_kopf.we_status > 3)
		{
			if (abfragejn (hWnd, "Der Wareneingang wurde schon abgeschlossen\n\nStatus zur�cksetzen und erneut bearbeiten ? ", "N") == 0)
			{
				fHead.SetCurrentName ("lief");
				return 0;
			}
			else
			{
				we_kopf.we_status = 1;
				flg_Storno = TRUE;
			}
		}

		 if (dsqlstatus == 0)
		 {
             NewRec = FALSE;
			 ToForm (dbheadfields);
			 ToForm (dbfields);
             dlong_to_asc (we_kopf.we_dat, wekopff.we_dat);
             dlong_to_asc (we_kopf.blg_eing_dat, wekopff.blg_eing_dat);
             TestEnablePrint ();
		 }
         else if (dsqlstatus == 100)
         {
             NewRec = TRUE;
             disp_mess ("Neuer Wareneingang", 1);
             we_kopf.we_status = 0;
             if (wekopff.a_best_kz[0] < '1')
             {
                   strcpy (wekopff.a_best_kz, "1");
             }
             strcpy (we_kopf.bsd_verb_kz, "J");
             sysdate (wekopff.we_dat);
             we_kopf.we_dat = dasc_to_long (wekopff.we_dat);
             sysdate (wekopff.blg_eing_dat);
             strcpy (wekopff.pr_erf_kz, "3");
             we_kopf.akv = dasc_to_long (datum);

             weWork.BeginWork ();
             we_kopf.lfd = AutoNr.GetGJ (we_kopf.mdn, we_kopf.fil, "we_nr");
             weWork.CommitWork ();


             sprintf (wekopff.lfd, "%ld", we_kopf.lfd);
	         strcpy (we_kopf.a_best_kz, lief.we_erf_kz);
	         strcpy (we_kopf.pr_erf_kz, lief.we_pr_kz);
			 ToForm (dbfields);
         }

         we_kopf.bearb = dasc_to_long (datum);

 	     weWork.GetPtab (wekopff.we_status_bz, "we_status",  
						 wekopff.we_status);
 	     weWork.GetPtab (wekopff.a_best_kz_bz, "we_erf_kz",  
						 wekopff.a_best_kz);
         strcpy (wekopff.a_best_info, ptabn.ptbez);
         fHead.GetCfield ("we_erf_info")->Invalidate (TRUE);
         fHead.GetCfield ("we_erf_info")->SetText ();
 	     weWork.GetPtab (wekopff.pr_erf_kz_bz, "we_pr_kz",  
						 wekopff.pr_erf_kz);
         weWork.BeginWork ();
		 if (weWork.LockWeKopf () == FALSE)
		 {
				 disp_mess ("Der Lieferschein wird von einem anderen Benutzer bearbeitet",2);
				 weWork.RollbackWork ();
			     fHead.SetCurrentName ("lief");
				 return 0;
		 }
		if (flg_Storno == TRUE)
		{
		        char rosipath [512];
				char progname [512];
	                if (getenv (RSDIR) == NULL)
				{
  						flg_Storno = FALSE;
				}
				else
				{
					flg_Storno = TRUE;
					BOOL testweldr = FALSE;
			        if(getenv("testweldr") != NULL)
					{
						if (atoi(getenv("testweldr")) == 1)
						{
							testweldr = TRUE;
						}
					}

					strcpy (rosipath, getenv (RSDIR));
	                weWork.WriteWeKopf ();
	                weWork.CommitWork ();
			        if(getenv("testmode") != NULL)
					{
						if (testweldr = TRUE)
						{
							sprintf (progname, "%s\\BIN\\rswrun -q c:\\temp\\weldr351.log weldr351 %s "
                                   "%hd %hd %s %s %s",
                                   rosipath, we_kopf.blg_typ,
                                   we_kopf.mdn, we_kopf.fil, we_kopf.lief,
							 we_kopf.lief_rech_nr, "S");
						}
						else
						{
							sprintf (progname, "%s\\BIN\\rswrun weldr351 %s "
                                   "%hd %hd %s %s %s",
                                   rosipath, we_kopf.blg_typ,
                                   we_kopf.mdn, we_kopf.fil, we_kopf.lief,
							 we_kopf.lief_rech_nr, "S");
						}
					}
					else
					{
						sprintf (progname, "%s\\BIN\\rswrun weldr351 %s "
                                   "%hd %hd %s %s %s",
                                   rosipath, we_kopf.blg_typ,
                                   we_kopf.mdn, we_kopf.fil, we_kopf.lief,
							 we_kopf.lief_rech_nr, "S");
					}
						ProcWaitExecEx (progname, SW_SHOWMINIMIZED, 0, 40, -1, 0);
			        weWork.BeginWork ();
				}
		}


         fWeKopf.SetText ();
         Enable (&fWeKopf, EnableHead, FALSE); 
         Enable (&fWeKopf, EnablePos,  TRUE); 

         SendMessage (fPos.GetCfield ("liste")->GethWnd (),
                      BM_SETIMAGE, (WPARAM) IMAGE_BITMAP, 
					  (LPARAM) ListBitmap);
         if (ActiveDlg->GetToolbar2 () != NULL)
         {
                 if  (ActiveDlg->GetToolbar2 ()->GetCfield ("f6") != NULL)
                 {
                      ((ColButton *)ActiveDlg->GetToolbar2 ()->GetCfield ("f6")->GetFeld ())->bmp = 
                                   ImageList.Image;
                 }
                 if  (ActiveDlg->GetToolbar2 ()->GetCfield ("qs") != NULL)
                 {
                      ActiveDlg->GetToolbar2 ()->GetCfield ("qs")->Enable (TRUE);
                 }
                 if  (ActiveDlg->GetToolbar2 ()->GetCfield ("best") != NULL)
                 {
                      ActiveDlg->GetToolbar2 ()->GetCfield ("best")->Enable (TRUE);
                 }
                 ActiveDlg->GetToolbar2 ()->display ();
         }

         EnableMenuItem (ActiveDlg->GethMenu (),    IDM_LIST,     MF_ENABLED);
         EnableMenuItem (ActiveDlg->GethMenu (),    VK_F12,       MF_ENABLED);
         EnableMenuItem (ActiveDlg->GethMenu (),    IDM_DELALL,   MF_ENABLED);
         EnableMenuItem (ActiveDlg->GethMenu (),    IDM_QM,       MF_ENABLED);
         EnableMenuItem (ActiveDlg->GethMenu (),    IDM_BEST,     MF_ENABLED);
         if (ActiveDlg->GettextWork () != NULL)
         {
                EnableMenuItem (ActiveDlg->GethMenu (),    IDM_TXTDLG,   MF_ENABLED);
         }

         ToolBar_SetState (ActiveDlg->GethwndTB (), IDM_DELALL,   TBSTATE_ENABLED);
         if (weWork.PosExist ())
         {
                EnableMenuItem (ActiveDlg->GethMenu (),    IDM_ABSCHLUSS,   MF_ENABLED);
                if (ActiveDlg->GetToolbar2 () != NULL && 
                    ActiveDlg->GetToolbar2 ()->GetCfield ("absch") != NULL)
                {
                      ActiveDlg->GetToolbar2 ()->GetCfield ("absch")->Enable (TRUE);
                      ActiveDlg->GetToolbar2 ()->display ();
                }
         }
         else
         {
                EnableMenuItem (ActiveDlg->GethMenu (),    IDM_ABSCHLUSS,   MF_GRAYED);
                if (ActiveDlg->GetToolbar2 () != NULL && 
                    ActiveDlg->GetToolbar2 ()->GetCfield ("absch") != NULL)
                {
                      ActiveDlg->GetToolbar2 ()->GetCfield ("absch")->Enable (FALSE);
                      ActiveDlg->GetToolbar2 ()->display ();
                }
         }

         TestGenCharge ();
         fPos.SetCurrentName ("we_erf_kz");
         if (ReadBest && best_kopf.best_blg > 0)
         {
                 FromForm (dbfields); 
                 weWork.BestToWe ();
                 ReadBest = FALSE;
         }
         strcpy (wekopff.koresp_best0, we_kopf.koresp_best0);
         fPos.SetText ();
         return 0;
}

int WeDlg::TestBsdVerb (void)
{
         fPos.GetText ();
         if (wekopff.bsd_verb_kz[0] != 'J')
         {
             wekopff.bsd_verb_kz[0] = 'N';
             fPos.GetCfield ("bsd_verb_kz")->SetText ();
         }
         return 0;
}


int WeDlg::TestDate (void)
{
         CFIELD *Cfield;
        
         Cfield = ActiveDlg->GetCurrentCfield ();
         Cfield->GetText ();
         Cfield->SetText ();
         return 0;
}


int WeDlg::GetPtab (void)
{
/*
         char wert [5];
         CFIELD *Cfield;
*/
         return 0;
}

       

ItProg *WeDlg::WeKopfAfter [] = {
                                   new ItProg ("mdn",         ReadBzgMdn),
                                   new ItProg ("fil",         ReadBzgFil),
                                   new ItProg ("lief",        ReadBzgLief),
                                   new ItProg ("lief_rech_nr",ReadWe),                             
                                   new ItProg ("blg_typ",     EnterPos),                             
                                   new ItProg ("bsd_verb_kz",
                                                              TestBsdVerb),                             
                                   new ItProg ("we_dat",
                                                              TestDate),                             
                                   new ItProg ("blg_eing_dat",
                                                              TestDate),                             
                                   new ItProg ("we_erf_kz",
                                                              TestWeErfKz),                             
                                   new ItProg ("we_pr_kz",
                                                              TestPrErfKz),                             
                                   new ItProg ("ls_ident",
                                                              TestGenCharge),                             
								   NULL,
};

ItProg *WeDlg::WeKopfBefore [] = {
                                   new ItProg ("mdn",          SetKey9),
                                   new ItProg ("fil",          SetKey9),
                                   new ItProg ("lief",         SetKey9),
                                   new ItProg ("lief_rech_nr", SetKey9),
                                   new ItProg ("we_erf_kz",    SetKey9),                             
                                   new ItProg ("we_pr_kz",     SetKey9),                             
                                  NULL,
};

ItProg *WeDlg::WePosAfter [] = {
                                   new ItProg ("we_dat",       TestWeDat),
                                   new ItProg ("blg_eing_dat", TestBlgEingDat),
								   NULL,
};

ItFont *WeDlg::WeKopfFont [] = {
                                  new ItFont ("lief_rech_nr_txt",   &ltgrayfont),
                                  new ItFont ("blg_typ_txt"     ,   &ltgrayfont),
                                  NULL
};

FORMFIELD *WeDlg::dbheadfields [] = {
         new FORMFIELD ("mdn",       wekopff.mdn,       (short *)   &we_kopf.mdn,       FSHORT,   NULL),
         new FORMFIELD ("fil",       wekopff.fil,       (short *)   &we_kopf.fil,       FSHORT,   NULL),
         new FORMFIELD ("lief",      wekopff.lief,      (char *)    we_kopf.lief,       FCHAR,    NULL),
         new FORMFIELD ("lief_rech_nr",
                         wekopff.lief_rech_nr,  
						 (char *)   we_kopf.lief_rech_nr,     FCHAR,    NULL),
         new FORMFIELD ("blg_typ",   wekopff.blg_typ, 
		                 (char *) we_kopf.blg_typ,        FCHAR,  NULL),
         NULL,
};


FORMFIELD *WeDlg::dbfields [] = {
         new FORMFIELD ("we_status",    wekopff.we_status,       (short *) 
                                        &we_kopf.we_status,     FSHORT,  NULL),
         new FORMFIELD ("we_erf_kz",    wekopff.a_best_kz,       (char *)  
                                        we_kopf.a_best_kz,      FCHAR,   NULL),
         new FORMFIELD ("bsd_verb_kz",  wekopff.bsd_verb_kz,     (char *) 
                                        we_kopf.bsd_verb_kz,    FCHAR,   NULL),
         new FORMFIELD ("lfd",          wekopff.lfd,             (long *)  
                                        &we_kopf.lfd,           FLONG,   NULL),
         new FORMFIELD ("koresp_best0", wekopff.koresp_best0,    (char *) 
                                        we_kopf.koresp_best0,   FCHAR,   NULL),
         new FORMFIELD ("we_pr_kz",     wekopff.pr_erf_kz,       (char *)  
                                        we_kopf.pr_erf_kz,      FCHAR,   NULL),
         NULL,
};


char *WeDlg::EnableHead [] = {
                                "mdn",
                                "mdn_choise",
                                "fil",
                                "fil_choise",
                                "lief",
                                "lief_choise",
	                            "lief_rech_nr",
	                            "lief_rech_nr_choise",
                                "blg_typ", 
                                "blg_typ_choise", 
                                 NULL
};


char *WeDlg::EnableHeadMdn [] = {
                                "mdn",
                                 NULL
};

char *WeDlg::EnableHeadFil [] = {
                                "fil",
                                 NULL
};


char *WeDlg::EnablePos[] = {
	                         "we_erf_kz",
	                         "we_erf_kz_choise",
                             "bsd_verb_kz",
							 "we_dat",
                             "blg_eing_dat",
							 "koresp_best0",
                             "we_pr_kz",
                             "we_pr_kz_choise",
                             "ls_ident",
                             "ls_charge",
//                             "liste",
                             NULL,
};

                          
WeDlg::WeDlg (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel) :
         DLG (x, y, cx, cy, Caption, Size, Pixel)
{
          
             BorderType = HIGHCOLBORDER;
             Init (x, y, cx, cy, Caption, Size, Pixel);
}

WeDlg::WeDlg (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel, 
                  int BorderType) :
         DLG (x, y, cx, cy, Caption, Size, Pixel)
{
          
             this->BorderType = BorderType;
             Init (x, y, cx, cy, Caption, Size, Pixel);
}

WeDlg::WeDlg (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel, 
                  int BorderType, int EnterM) :
         DLG (x, y, cx, cy, Caption, Size, Pixel)
{
          
             this->BorderType = BorderType;
             Init (x, y, cx, cy, Caption, Size, Pixel);
}

WeDlg::~WeDlg ()
{
         ActiveDlg = OldDlg;
}



void WeDlg::Init (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel)
{
             int i;
             int xfull, yfull;

//             Work.Prepare ();

 
			 Debug.LogName = "35100";
             textWork = new TextWork ("35100.dlg");
             if (textWork->GetFeldnamen () == NULL)
             {
                 delete textWork;
                 textWork = NULL;
             }
             SysParKreditor = weWork.GetKreditor ();
             weWork.SetTextWork (textWork);
             ImageDelete.Image    = BMAP::LoadBitmap (hInstance, "DEL",       "DELMASK",      SysBkColor);
             ImageDelete.Inactive = BMAP::LoadBitmap (hInstance, "DELIA",     "DELMASK",      SysBkColor);
             ImageInsert.Image    = BMAP::LoadBitmap (hInstance, "INSERT",    "INSERTMASK",   SysBkColor);
             ImageInsert.Inactive = BMAP::LoadBitmap (hInstance, "INSERTIA",  "INSERTMASK",   SysBkColor);
             ImageList.Image      = BMAP::LoadBitmap (hInstance, "ARRDOWND",  "ARRDOWNDMASK", SysBkColor);
             ImageList.Inactive   = BMAP::LoadBitmap (hInstance, "ARRDOWNDI","ARRDOWNDMASK", SysBkColor);
             hwndCombo1 = NULL;
             hwndCombo2 = NULL;
             Combo1 = NULL;
             Combo2 = NULL;
             ColorSet = FALSE;
             LineSet = FALSE;
             Toolbar2 = NULL;
             DockList = TRUE;
             DockMode = TRUE;
             ListDlg = NULL;
             weDlgLst = NULL;
             hwndList = NULL;
             WithLiefBest = FALSE;
             WithAkrz     = FALSE;
             TwoRows      = FALSE;
			 WithEk       = FALSE;
			 WithPosEk    = FALSE;
			 InPrint      = FALSE;

             xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
             yfull = GetSystemMetrics (SM_CYFULLSCREEN);        

             StdSize = Size;
             dlgfont.FontHeight    = Size;
             InfoFont.FontHeight    = InfoSize;
             dlgposfont.FontHeight = Size;
             ltgrayfont.FontHeight = Size;
             cpfont.FontHeight     = cpsize;


             if (xfull < 1000) 
             {
                 dlgfont.FontHeight = 90;
                 InfoFont.FontHeight    = InfoSize - 30;
                 dlgposfont.FontHeight = 90;
                 ltgrayfont.FontHeight = 90;
                 cpfont.FontHeight = 150;
             }

             if (xfull < 800) 
             {

                 dlgfont.FontHeight = 70;
                 InfoFont.FontHeight    = InfoSize - 50;
                 dlgposfont.FontHeight = 70;
                 ltgrayfont.FontHeight = 70;
                 cpfont.FontHeight = 120;
             }

             fHead.SetFieldanz ();
             fPos.SetFieldanz ();
             fWeKopf.SetFieldanz ();

             fHead.GetCfield ("mdn_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fHead.GetCfield ("fil_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fHead.GetCfield ("lief_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fHead.GetCfield ("lief_rech_nr_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fHead.GetCfield ("blg_typ_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fPos.GetCfield ("we_erf_kz_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fPos.GetCfield ("we_pr_kz_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             ListBitmap   =   BMAP::LoadBitmap (hInstance, "ARRDOWND", "ARRDOWNDMASK"),
             ListBitmapi  =   BMAP::LoadBitmap (hInstance, "ARRDOWNDI", "ARRDOWNDMASK"),
             fPos.GetCfield ("liste")->SetBitmap (ListBitmapi);

             fHead.GetCfield ("mdn_choise")->SetTabstop (FALSE); 
             fHead.GetCfield ("fil_choise")->SetTabstop (FALSE); 
             fHead.GetCfield ("lief_choise")->SetTabstop (FALSE); 
             fHead.GetCfield ("lief_rech_nr_choise")->SetTabstop (FALSE); 
             fHead.GetCfield ("blg_typ_choise")->SetTabstop (FALSE); 
             fPos.GetCfield ("we_erf_kz_choise")->SetTabstop (FALSE); 
             fPos.GetCfield ("we_pr_kz_choise")->SetTabstop (FALSE); 
             fPos.GetCfield ("liste")->SetTabstop (FALSE); 


             for (i = 0; i < fHead.GetFieldanz (); i ++)
             {
//                       fHead.GetCfield ()[i]->SetFont (&dlgfont); 
//                       fHead.GetCfield ()[i]->SetPosFont (&dlgposfont); 
             }

             fHead.GetCfield ("we_erf_info")->SetFont (&InfoFont);

             SetTmFont (&dlgfont);
             for (i = 0; WeKopfBefore[i] != NULL; i ++)
             {
                 WeKopfBefore[i]->SetBefore (&fWeKopf);
             }

             for (i = 0; WeKopfAfter[i] != NULL; i ++)
             {
                 WeKopfAfter[i]->SetAfter (&fWeKopf);
             }

             for (i = 0; WePosAfter[i] != NULL; i ++)
             {
                 WePosAfter[i]->SetAfter (&fWeKopf);
             }

             if (BorderType == HIGHCOLBORDER)
             {

                for (i = 0; WeKopfFont[i] != NULL; i ++)
                {
                 WeKopfFont[i]->SetFont (&fWeKopf);
                }

                  fPos.GetCfield ("pos_frame")->SetBorder 
                 (new FILLEDBORDER (BLACKCOL, WHITECOL, HIGH, 0, GetSysColor (COLOR_3DFACE))); 
             }
             else if (BorderType == LOWCOLBORDER)
             {

                for (i = 0; WeKopfFont[i] != NULL; i ++)
                {
                       WeKopfFont[i]->SetFont (&fWeKopf);
                }
                fPos.GetCfield ("pos_frame")->SetBorder 
                 (new FILLEDBORDER (BLACKCOL, WHITECOL, LOW, 0, GetSysColor (COLOR_3DFACE))); 
             }
             else if (BorderType == HIGHBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, HIGH); 
             }
             else if (BorderType == LOWBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, LOW); 
             }
             else if (BorderType == RAISEDBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, RAISED); 
             }
             else if (BorderType == LINEBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, LINE); 
             }

/*             
             fHead.GetCfield ("we_erf_info")->SetShadow (TRUE);
             fHead.GetCfield ("we_erf_info")->SetShadowPos (2,2);
             fHead.GetCfield ("we_erf_info")->SetShadowColor (InfShadow);
*/

             TestAttr ();
             Enable (&fWeKopf, EnableHead, TRUE); 
             Enable (&fWeKopf, EnablePos,  FALSE);
			 EnableMdnFil ();

             SetDialog (&fWeKopf0);
             NewTabStops (&fWeKopf0);
             QmKopf = NULL;
             OldDlg = ActiveDlg;
             ActiveDlg = this;
             weWork.SetDlg (this);
}

void WeDlg::EnableMdnFil ()
{
			 if (sys_ben.mdn > 0) 
			 {
				 sprintf (wekopff.mdn, "%hd", sys_ben.mdn);
				 Enable (&fWeKopf, EnableHeadMdn, FALSE);
			 }
			 if (sys_ben.fil > 0) 
			 {
				 sprintf (wekopff.fil, "%hd", sys_ben.fil);
				 Enable (&fWeKopf, EnableHeadFil, FALSE);
			 }
}

int WeDlg::SetLiefRechNr (BOOL mode)
{
    if  (ActiveDlg->GetToolbar2 ()->GetCfield ("best") != NULL)
    {
                 ActiveDlg->GetToolbar2 ()->GetCfield ("best")->Enable (mode);
                 ActiveDlg->GetToolbar2 ()->GetCfield ("best")->display ();
    }
    if (mode == FALSE)
    {
                 EnableMenuItem (ActiveDlg->GethMenu (),    IDM_BEST,     MF_GRAYED);
    }
    else 
    {
                 EnableMenuItem (ActiveDlg->GethMenu (),    IDM_BEST,     MF_ENABLED);
    }
    return 0;
}

int WeDlg::SetKey9 ()
{
    EnableMenuItem (ActiveDlg->GethMenu (), IDM_CHOISE,   MF_ENABLED);
    return 0;
}

void WeDlg::FillHeadfields (void)
{
/*
        char wert [5];
        int dsqlstatus;
*/

        memcpy (&wekopff, &wekopff_null, sizeof (struct WEKOPFF));
        sprintf (wekopff.mdn,  "%4d", we_kopf.mdn);  
        sprintf (wekopff.fil,  "%4d", we_kopf.fil);  
        sprintf (wekopff.lief, "%s",  we_kopf.lief);  
        sprintf (wekopff.lief_rech_nr, "%s",  we_kopf.lief_rech_nr);  
        ToForm (dbfields); 
}


BOOL WeDlg::OnKeyReturn (void)
{
        return FALSE;
}

int WeDlg::DeleteWe (void)
{
       
        if (abfragejn (hWnd, "Wareneingang l�schen ?", "J") == 0)
        {
            return 0;
        }

        weWork.DeleteWe ();

        Enable (&fWeKopf, EnableHead, TRUE); 
        Enable (&fWeKopf, EnablePos,  FALSE);
	    EnableMdnFil ();
        weWork.CommitWork ();
        weWork.InitRec ();
        InitPos ();
        fPos.SetText ();
//270910        fHead.SetCurrentName ("lief");
		if (WeDlg::Ruecksprung == 0)
		{
			fHead.SetCurrentName ("mdn");
		}
		else if (WeDlg::Ruecksprung == 1)
		{
            strcpy ((char *)fHead.GetCfield ("lief")->GetFeld (), " ");
	        fHead.GetCfield ("lief")->SetText (); 
            strcpy ((char *)fHead.GetCfield ("fil")->GetFeld (), " ");
	        fHead.GetCfield ("fil")->SetText (); 
            strcpy ((char *)fHead.GetCfield ("fil_krz")->GetFeld (), " ");
	        fHead.GetCfield ("fil_krz")->SetText (); 
		            strcpy ((char *)fHead.GetCfield ("lief_krz")->GetFeld (), " ");
				    fHead.GetCfield ("lief_krz")->SetText (); 
			fHead.SetCurrentName ("fil");
		}
		else
		{
			fHead.SetCurrentName ("lief");
		}
        strcpy ((char *)fHead.GetCfield ("lief_rech_nr")->GetFeld (), " ");
        fHead.GetCfield ("lief_rech_nr")->SetText (); 
        ToolBar_SetState (ActiveDlg->GethwndTB (),IDM_DELALL, TBSTATE_INDETERMINATE);
        ToolBar_SetState (ActiveDlg->GethwndTB (), IDM_PRINT,   TBSTATE_ENABLED);
        EnableMenuItem (ActiveDlg->GethMenu (),    IDM_PRINT,   MF_ENABLED);
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELETE,   MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELALL,   MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),  IDM_LIST,   MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),   VK_F12,     MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),   IDM_QM,     MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),   IDM_BEST,   MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),   IDM_TXTDLG, MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),    IDM_ABSCHLUSS,   MF_GRAYED);
        SendMessage (fPos.GetCfield ("liste")->GethWnd (), 
                            BM_SETIMAGE, (WPARAM) IMAGE_BITMAP, 
 		  				    (LPARAM) ListBitmapi);
        if (ActiveDlg->GetToolbar2 () != NULL)
        {
                 if  (ActiveDlg->GetToolbar2 ()->GetCfield ("f6") != NULL)
                 {
                      ((ColButton *)ActiveDlg->GetToolbar2 ()->GetCfield ("f6")->GetFeld ())->bmp = 
                                    ImageList.Inactive;
                 }
                 if  (ActiveDlg->GetToolbar2 ()->GetCfield ("qs") != NULL)
                 {
                      ActiveDlg->GetToolbar2 ()->GetCfield ("qs")->Enable (FALSE);
                 }
                 if  (ActiveDlg->GetToolbar2 ()->GetCfield ("best") != NULL)
                 {
                      ActiveDlg->GetToolbar2 ()->GetCfield ("best")->Enable (FALSE);
                 }
                 if  (ActiveDlg->GetToolbar2 ()->GetCfield ("absch") != NULL)
                 {
                      ActiveDlg->GetToolbar2 ()->GetCfield ("absch")->Enable (FALSE);
                 }
                 ActiveDlg->GetToolbar2 ()->display ();
        }
        NewRec = FALSE;
/*
        ToolBar_SetState (ActiveDlg->GethwndTB (),IDM_DELETE, TBSTATE_INDETERMINATE);
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELETE,   MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELALL,   MF_GRAYED);
        weWork.InitRec ();
        FromForm (dbheadfields);
        ToForm (dbfields);
        fPos.SetText ();
        Enable (&fWeKopf, EnableHead, TRUE); 
	    EnableMdnFil ();
        if (atoi (wekopff.mdn) == 0)
        {
             Enable (&fWeKopf, EnableHeadFil, FALSE); 
        }
        Enable (&fWeKopf, EnablePos,  FALSE);
        fWeKopf.SetCurrentName ("lief");
*/
        return 0;
}
         

BOOL WeDlg::OnKeyDown (void)
{
/*
        HWND hWnd;

        hWnd == GetFocus ();

        if (fHead.GetCfield ("lief")->IsDisabled () == FALSE)
        {
            return FALSE;
        }
        if (_fList[0]->GethWnd () == NULL) 
        {
            return FALSE;
        }
        WriteRow ();
        SendMessage (_fList[0]->GethWnd (), WM_KEYDOWN, VK_DOWN, 0l);
        fPos.SetCurrentName ("lief_kz");
        fPos.SetCurrentName ("a");
*/
        return FALSE;
}


BOOL WeDlg::OnKeyUp (void)
{
/*
        HWND hWnd;
        int idx;

        hWnd == GetFocus ();

        if (hWnd == _fList[0]->GethWnd ())
        {
            return FALSE;
        }


        idx = SendMessage (_fList[0]->GethWnd (), LB_GETCURSEL, 0, 0l);

        if (fHead.GetCfield ("lief")->IsDisabled () == FALSE)
        {
            return FALSE;
        }

        SendMessage (_fList[0]->GethWnd (), WM_KEYDOWN, VK_UP, 0l);
        fPos.SetCurrentName ("lief_kz");
        fPos.SetCurrentName ("a");
*/
        return FALSE;
}


BOOL WeDlg::OnKey2 ()
{
        return TRUE;
}



BOOL WeDlg::OnKeyHome ()
{
        return TRUE;
}

BOOL WeDlg::OnKeyEnd ()
{
        return TRUE;

}
BOOL WeDlg::OnKey1 ()
{
        syskey = KEY1;
        Help ();
        return TRUE;
}

BOOL WeDlg::OnKey3 ()
{
        syskey = KEY3;
        return TRUE;
}

BOOL WeDlg::OnKey4 ()
{
        syskey = KEY4;
        CallInfo ();
        return TRUE;
}

void WeDlg::InitPos (void)
{
        int i;

        for (i = 0; i < fPos.GetFieldanz (); i ++)
        {
            if (fPos.GetCfield () [i]->GetAttribut == CEDIT)
            {
                      strcpy (fPos.GetCfield ()[i]->GetFeld (), "");
            }
        }
}

BOOL WeDlg::OnKey5 ()
{

        syskey = KEY5;
        InsMode = FALSE;
        syskey = KEY5;
        EnableMenuItem (hMenu, IDM_SHOWKONTRAKT,   MF_GRAYED);
		best_kopf.kontrakt_kz = 0l;
        if (fHead.GetCfield ("lief")->IsDisabled () == FALSE)
        {
                  DestroyWindow ();
                  return TRUE;
        }

        EnableMenuItem (hMenu, IDM_SHOWKONTRAKT,   MF_GRAYED);
		best_kopf.kontrakt_kz = 0l;
        if (abfragejn (hMainWindow, "�nderungen speichern ?", "J"))
        {
            return OnKey12 ();
        }
		else
		{
			if (flg_Storno == TRUE)
			{
				disp_mess ("Status wurde zur�ckgesetzt\nAbschluss muss noch einmal durchgef�hrt werden !",1);
			}
		}
        Enable (&fWeKopf, EnableHead, TRUE); 
	    EnableMdnFil ();
        if (atoi (wekopff.mdn) == 0)
        {
             Enable (&fWeKopf, EnableHeadFil, FALSE); 
        }
        Enable (&fWeKopf, EnablePos,  FALSE);
        weWork.RollbackWork ();
        weWork.InitRec ();
        InitPos ();
        fPos.SetText ();
        //270910 fHead.SetCurrentName ("lief");
		if (WeDlg::Ruecksprung == 0)
		{
			fHead.SetCurrentName ("mdn");
		}
		else if (WeDlg::Ruecksprung == 1)
		{
            strcpy ((char *)fHead.GetCfield ("lief")->GetFeld (), " ");
	        fHead.GetCfield ("lief")->SetText (); 
            strcpy ((char *)fHead.GetCfield ("fil")->GetFeld (), " ");
	        fHead.GetCfield ("fil")->SetText (); 
            strcpy ((char *)fHead.GetCfield ("fil_krz")->GetFeld (), " ");
	        fHead.GetCfield ("fil_krz")->SetText (); 
		            strcpy ((char *)fHead.GetCfield ("lief_krz")->GetFeld (), " ");
				    fHead.GetCfield ("lief_krz")->SetText (); 
			fHead.SetCurrentName ("fil");
		}
		else
		{
			fHead.SetCurrentName ("lief");
		}

        strcpy ((char *)fHead.GetCfield ("lief_rech_nr")->GetFeld (), " ");
        fHead.GetCfield ("lief_rech_nr")->SetText (); 
        ToolBar_SetState (ActiveDlg->GethwndTB (),IDM_DELALL, TBSTATE_INDETERMINATE);
        ToolBar_SetState (ActiveDlg->GethwndTB (), IDM_PRINT,   TBSTATE_ENABLED);
        EnableMenuItem (ActiveDlg->GethMenu (),    IDM_PRINT,   MF_ENABLED);

        EnableMenuItem (ActiveDlg->GethMenu (),   IDM_DELETE, MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),   IDM_DELALL, MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),   IDM_LIST,   MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),   VK_F12,     MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),   IDM_QM,     MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),   IDM_BEST,   MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),   IDM_TXTDLG, MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),    IDM_ABSCHLUSS,   MF_GRAYED);
        SendMessage (fPos.GetCfield ("liste")->GethWnd (), 
                           BM_SETIMAGE, (WPARAM) IMAGE_BITMAP, 
 						   (LPARAM) ListBitmapi);
        if (ActiveDlg->GetToolbar2 () != NULL)
        {
                 if  (ActiveDlg->GetToolbar2 ()->GetCfield ("f6") != NULL)
                 {
                      ((ColButton *)ActiveDlg->GetToolbar2 ()->GetCfield ("f6")->GetFeld ())->bmp = 
                                    ImageList.Inactive;
                 }
                 if  (ActiveDlg->GetToolbar2 ()->GetCfield ("qs") != NULL)
                 {
                      ActiveDlg->GetToolbar2 ()->GetCfield ("qs")->Enable (FALSE);
                 }
                 if  (ActiveDlg->GetToolbar2 ()->GetCfield ("best") != NULL)
                 {
                      ActiveDlg->GetToolbar2 ()->GetCfield ("best")->Enable (FALSE);
                 }
                 if  (ActiveDlg->GetToolbar2 ()->GetCfield ("absch") != NULL)
                 {
                      ActiveDlg->GetToolbar2 ()->GetCfield ("absch")->Enable (FALSE);
                 }
                 ActiveDlg->GetToolbar2 ()->display ();
        }
        return TRUE;
}


BOOL WeDlg::OnKey12 ()
{
        AutoNrClass AutoNr;

        syskey = KEY12;

		syskey = KEY12;
		if (DatError == TRUE) 
		{
			if (TestWeDat () == 0) 
			{
				return FALSE;
			}
			if (TestBlgEingDat () == 0) 
			{
				return FALSE;
			}
		}
        EnableMenuItem (hMenu, IDM_SHOWKONTRAKT,   MF_GRAYED);
        InsMode = FALSE;
        fWork->GetText ();
        FromForm (dbfields); 
        if (fHead.GetCfield ("lief")->IsDisabled ())
        {
               FromForm (dbfields);
/*
               if (NewRec)
               {
                   we_kopf.lfd = AutoNr.GetGJ (we_kopf.mdn, we_kopf.fil, "we_nr");
               }
*/

               we_kopf.we_dat = dasc_to_long (wekopff.we_dat);
               we_kopf.blg_eing_dat = dasc_to_long (wekopff.blg_eing_dat);
               weWork.WriteWeKopf ();
               Enable (&fWeKopf, EnableHead, TRUE); 
               Enable (&fWeKopf, EnablePos,  FALSE);
               weWork.SchreibeLief(); //Detlef 011208
			   EnableMdnFil ();
               weWork.CommitWork ();
               weWork.InitRec ();
               InitPos ();
               fPos.SetText ();
//270910               fHead.SetCurrentName ("lief");
				if (WeDlg::Ruecksprung == 0)
				{
					fHead.SetCurrentName ("mdn");
				}
				else if (WeDlg::Ruecksprung == 1)
				{
		            strcpy ((char *)fHead.GetCfield ("lief")->GetFeld (), " ");
				    fHead.GetCfield ("lief")->SetText (); 
					strcpy ((char *)fHead.GetCfield ("fil")->GetFeld (), " ");
					fHead.GetCfield ("fil")->SetText (); 
		            strcpy ((char *)fHead.GetCfield ("fil_krz")->GetFeld (), " ");
				    fHead.GetCfield ("fil_krz")->SetText (); 
		            strcpy ((char *)fHead.GetCfield ("lief_krz")->GetFeld (), " ");
				    fHead.GetCfield ("lief_krz")->SetText (); 
					fHead.SetCurrentName ("fil");
				}
				else
				{
					fHead.SetCurrentName ("lief");
				}
               strcpy ((char *)fHead.GetCfield ("lief_rech_nr")->GetFeld (), " ");
               fHead.GetCfield ("lief_rech_nr")->SetText (); 
               ToolBar_SetState (ActiveDlg->GethwndTB (),IDM_DELALL, TBSTATE_INDETERMINATE);
               ToolBar_SetState (ActiveDlg->GethwndTB (), IDM_PRINT,   TBSTATE_ENABLED);
               EnableMenuItem (ActiveDlg->GethMenu (),    IDM_PRINT,   MF_ENABLED);
               EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELETE,   MF_GRAYED);
               EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELALL,   MF_GRAYED);
               EnableMenuItem (ActiveDlg->GethMenu (),  IDM_LIST,   MF_GRAYED);
               EnableMenuItem (ActiveDlg->GethMenu (),   VK_F12,     MF_GRAYED);
               EnableMenuItem (ActiveDlg->GethMenu (),   IDM_QM,     MF_GRAYED);
               EnableMenuItem (ActiveDlg->GethMenu (),   IDM_BEST,   MF_GRAYED);
               EnableMenuItem (ActiveDlg->GethMenu (),   IDM_TXTDLG, MF_GRAYED);
               EnableMenuItem (ActiveDlg->GethMenu (),    IDM_ABSCHLUSS,   MF_GRAYED);
               SendMessage (fPos.GetCfield ("liste")->GethWnd (), 
                            BM_SETIMAGE, (WPARAM) IMAGE_BITMAP, 
 		  				    (LPARAM) ListBitmapi);
               if (ActiveDlg->GetToolbar2 () != NULL)
               {
                 if  (ActiveDlg->GetToolbar2 ()->GetCfield ("f6") != NULL)
                 {
                      ((ColButton *)ActiveDlg->GetToolbar2 ()->GetCfield ("f6")->GetFeld ())->bmp = 
                                    ImageList.Inactive;
                 }
                 if  (ActiveDlg->GetToolbar2 ()->GetCfield ("qs") != NULL)
                 {
                      ActiveDlg->GetToolbar2 ()->GetCfield ("qs")->Enable (FALSE);
                 }
                 if  (ActiveDlg->GetToolbar2 ()->GetCfield ("best") != NULL)
                 {
                      ActiveDlg->GetToolbar2 ()->GetCfield ("best")->Enable (FALSE);
                 }
                 if  (ActiveDlg->GetToolbar2 ()->GetCfield ("absch") != NULL)
                 {
                      ActiveDlg->GetToolbar2 ()->GetCfield ("absch")->Enable (FALSE);
                 }
                 ActiveDlg->GetToolbar2 ()->display ();
               }
               NewRec = FALSE;
			if (flg_Storno == TRUE)
			{
				disp_mess ("Status wurde zur�ckgesetzt\nAbschluss muss noch einmal durchgef�hrt werden !",1);
			}
        }
        return TRUE;
}

BOOL WeDlg::OnKeyDelete ()
{

        return FALSE;
}

BOOL WeDlg::OnKeyPrior ()
{

        if (vTrack != NULL)
        {
             SendMessage (hWnd, WM_VSCROLL, SB_PAGEUP, 0l);
             return TRUE;
        }
        return FALSE;
}

BOOL WeDlg::OnKeyNext ()
{
        if (vTrack != NULL)
        {
             SendMessage (hWnd, WM_VSCROLL, SB_PAGEDOWN, 0l);
             return TRUE;
        }
        return FALSE;
}

void WeDlg::CreateListQuikInfos (HWND hWnd)
{
       if (Toolbar2 == NULL) return;

       Toolbar2->GetCfield ("f5")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("f12")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("f9")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("f6")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("del")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("insert")->CreateQuikInfos (hWnd);
}


BOOL WeDlg::EnterWeList ()
{
        int DockY;
        RECT rect;
        CFIELD *CurrentCfield;

        if (weDlgLst != NULL) return FALSE;
		InEnterPos = TRUE;

        CurrentCfield = ActiveDlg->GetCurrentCfield ();
        if (fHead.GetCfield ("lief")->IsDisabled ())
        {
            if (ReadBest && best_kopf.best_blg > 0)
            {
                 FromForm (dbfields); 
                 weWork.BestToWe ();
                 ReadBest = FALSE;
            }
            we_kopf.we_dat = dasc_to_long (wekopff.we_dat);
            we_kopf.blg_eing_dat = dasc_to_long (wekopff.blg_eing_dat);
            if (Toolbar2 != NULL)
            {
                 ((ColButton *)Toolbar2->GetCfield ("del")->GetFeld ())->bmp = 
					 ImageDelete.Image;
                 ((ColButton *)Toolbar2->GetCfield ("insert")->GetFeld ())->bmp = 
                     ImageInsert.Image;
                 if (Toolbar2->GetCfield ("f6") != NULL)
                 {
                    ((ColButton *)Toolbar2->GetCfield ("f6")->GetFeld ())->bmp = 
                         ImageList.Inactive;
                 }
                 if  (ActiveDlg->GetToolbar2 ()->GetCfield ("best") != NULL)
                 {
                      ActiveDlg->GetToolbar2 ()->GetCfield ("best")->Enable (FALSE);
                      ActiveDlg->GetToolbar2 ()->GetCfield ("absch")->Enable (FALSE);
                      ActiveDlg->GetToolbar2 ()->GetCfield ("best")->display ();
                      ActiveDlg->GetToolbar2 ()->GetCfield ("absch")->display ();
                      ActiveDlg->GetToolbar2 ()->GetCfield ("bezugsquellen")->Enable (TRUE);
                      ActiveDlg->GetToolbar2 ()->GetCfield ("bezugsquellen")->display ();
                 }
                 Toolbar2->display ();
            }

            ChangeF6 (IDM_LIST);
            ToolBar_SetState (ActiveDlg->GethwndTB (),IDM_DELALL, TBSTATE_INDETERMINATE);
            EnableMenuItem (ActiveDlg->GethMenu (),   IDM_DELALL, MF_GRAYED);
            EnableMenuItem (ActiveDlg->GethMenu (),   IDM_DELETE, MF_ENABLED);
//            EnableMenuItem (ActiveDlg->GethMenu (),   IDM_EXTRA_DATA, MF_ENABLED);
            EnableMenuItem (ActiveDlg->GethMenu (),   VK_F8, MF_ENABLED);
            EnableMenuItem (ActiveDlg->GethMenu (),   IDM_BEST,   MF_GRAYED);
            EnableMenuItem (ActiveDlg->GethMenu (),   IDM_ABSCHLUSS,   MF_GRAYED);
            EnableMenuItem (ActiveDlg->GethMenu (),   IDM_TXTDLG, MF_GRAYED);
            Enable (&fWeKopf, EnablePos, FALSE); 

            GetClientRect (hWnd, &rect);
            DockY = fPos.GetCfield ("pos_frame")->GetYP ();
            weDlgLst = new WeDlgLst (0, 0, 0, 0, 
				                     "", 105, 1);
//            weDlgLst = new WeDlgLst (0, 8, 0, 0, 
//				                     "", 105, 0);
			weDlgLst->SetDlg (this);
            weDlgLst->SetBorderType (BorderType);
			weDlgLst->SethMenu (ActiveDlg->GethMenu ());

            if (LsIdentAttr > 0)
            {
                weDlgLst->SetDefaultIdent (wekopff.ls_ident);
            }
            if (LsChargeAttr > 0)
            {
                weDlgLst->SetDefaultCharge (wekopff.ls_charge);
            }

/*
		    if (LineSet)
			{
				ListDlg->GeteListe ()->SetLines (ListLines);
			}

			if (ColorSet)
			{
		        ListDlg->GeteListe ()->SetColor (ListColor);
		        ListDlg->GeteListe ()->SetBkColor (ListBkColor);
			}
*/
            weDlgLst->SetWork (&weWork);

            EnableMenuItem (ActiveDlg->GethMenu (),   IDM_CHOISE, MF_ENABLED);

/*
            if (DockList == 0)
            {
                     ListDlg->SetWinStyle (ListDlg->GetWinStyle () | WS_CAPTION | 
                                                            WS_SYSMENU |
                                                            WS_MINIMIZEBOX |
                                                            WS_MAXIMIZEBOX);
                     ListDlg->SetDockWin (FALSE);
                     ListDlg->SetDimension (750, 400);
                     ListDlg->SetModal (TRUE);
                     EnableWindow (hMainWindow, FALSE);
            }
            else
            {
                     if (DockMode)
                     {
                        ListDlg->SetWinStyle (WS_THICKFRAME | WS_CHILD | WS_CLIPSIBLINGS);
                        Enable (&fWeKopf, EnablePos, TRUE); 
                        DestroyPos ();
                     }
                     GetWindowRect (hWnd, &rect);
                     DockY = fPos.GetCfield ("pos_frame")->GetYP () + rect.top;
                     ListDlg->SetDockY (DockY);
            }
*/

            Enable (&fWeKopf, EnablePos, TRUE); 
            DestroyHead ();
            DestroyPos ();
			InvalidateRect (hWnd, NULL, TRUE);
			UpdateWindow (hWnd);
            weDlgLst->SetWithSearchBest (WithSearchBest);
            if (atoi (wekopff.a_best_kz) == 2)
            {
					weDlgLst->SetWithSearchBest (TRUE);
			}
            weDlgLst->SetStyle (WS_VISIBLE | WS_CHILD | WS_DLGFRAME);
			weDlgLst->SetNoMain (TRUE);
            weDlgLst->SetWinBackground (WinBackground);
			weDlgLst->OpenWindow (DLG::hInstance, hWnd);

		    GetWindowRect (hWnd, &rect);
   		    SendMessage (hWnd, WM_SIZE, rect.right-rect.left, rect.bottom-rect.top);

/*
            ListDlg->SethMenu (hMenu);
            ListDlg->SethwndTB (hwndTB);
            if (atoi (wekopff.pr_erf_kz) == 3)
            {
                    WithPosEk = TRUE;
            }
            else
            {
                    WithPosEk = FALSE;
            }
			if (atoi (wekopff.pr_erf_kz) == 3 && WithPosEk == FALSE)
			{
					ChangePosEk ();
			}
            if (atoi (fPos.GetCfield ("we_pr_kz")->GetFeld ()) == 1)
            {
//                    ListDlg->SetFieldAttr ("pr_ek", DISPLAYONLY);
                    ListDlg->SetFieldAttr ("pr_ek",     REMOVED);
                    ListDlg->DestroyListField ("pr_ek");
                    ListDlg->SetFieldAttr ("netto_ek", REMOVED);
                    ListDlg->DestroyListField ("netto_ek");
            }
            else
            {
                    ListDlg->SetFieldAttr ("pr_ek", EDIT);
            }
            if (WithLiefBest == FALSE && WithAkrz == FALSE 
                && TwoRows == FALSE)
            {
                    ListDlg->Seta_bz2Removed (TRUE);
            }
            else
            {
                    ListDlg->Seta_bz2Removed (FALSE);
            }
            if (WithLiefBest == FALSE)
            {
                    if (TwoRows == FALSE)
                    {
                           ListDlg->Setlief_bestRemoved (TRUE);
                    }
                    else if (WithAkrz)
                    {
                           ListDlg->Setlief_bestRemoved (TRUE);
                    }
                    else
                    {
                           ListDlg->Setlief_bestRemoved (FALSE);
                    }

                    ListDlg->SetWithLiefBest (FALSE);
            }
            else
            {
                    ListDlg->Setlief_bestRemoved (FALSE);
                    ListDlg->Seta_krzRemoved (TRUE);
                    ListDlg->SetWithLiefBest (TRUE);
            }
            if (WithAkrz == FALSE)
            {
                    ListDlg->Seta_krzRemoved (TRUE);
                    ListDlg->SetWithAkrz (FALSE);
            }
            else
            {
                    ListDlg->Seta_krzRemoved (FALSE);
                    ListDlg->Setlief_bestRemoved (TRUE);
                    ListDlg->SetWithAkrz (TRUE);
            }
			if (WithPosEk)
			{
				    ListDlg->SetWithPosEk (TRUE);
                    EnableMenuItem (ActiveDlg->GethMenu (),   IDM_EPOSEK,   MF_ENABLED);
			}
			else
			{
				    ListDlg->SetWithPosEk (FALSE);
			}

            if (ListButtons == FALSE)
            {
                    ListDlg->OrFieldAttr ("me_choise", REMOVED);
            }

            if (ButtonSize == BIG)
            {
                    ListDlg->SetButtonSize (BIG);
            }
*/
            weDlgLst->ProcessMessages ();
            SetPos ();
            CreateListQuikInfos (hMainWindow);
            SetWindowPos (hMainWindow, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
            EnableWindows (hWnd, TRUE);
            delete weDlgLst;
			weWork.SetLsts (NULL);
            weDlgLst = NULL;
            Enable (&fWeKopf, EnablePos,  TRUE); 
            EnableMenuItem (ActiveDlg->GethMenu (),   IDM_DELETE,   MF_GRAYED);
            EnableMenuItem (ActiveDlg->GethMenu (),   VK_F8,        MF_GRAYED);
            EnableMenuItem (ActiveDlg->GethMenu (),   IDM_EXTRA_DATA, MF_GRAYED);
            EnableMenuItem (ActiveDlg->GethMenu (),   IDM_MHD_DATA, MF_GRAYED);
            ToolBar_SetState (ActiveDlg->GethwndTB (),IDM_DELALL,   TBSTATE_ENABLED);
            EnableMenuItem (ActiveDlg->GethMenu (),   IDM_DELALL,   MF_ENABLED);
            EnableMenuItem (ActiveDlg->GethMenu (),   IDM_EPOSEK,   MF_GRAYED);
            EnableMenuItem (ActiveDlg->GethMenu (),   IDM_BEST,     MF_ENABLED);
            if (textWork != NULL)
            {
                 EnableMenuItem (ActiveDlg->GethMenu (),   IDM_TXTDLG,   MF_ENABLED);
            }
            if (weWork.PosExist ())
            {
                EnableMenuItem (ActiveDlg->GethMenu (),    IDM_ABSCHLUSS,   MF_ENABLED);
                if (ActiveDlg->GetToolbar2 () != NULL && 
                    ActiveDlg->GetToolbar2 ()->GetCfield ("absch") != NULL)
                {
                      ActiveDlg->GetToolbar2 ()->GetCfield ("absch")->Enable (TRUE);
                }
            }
            else
            {
                EnableMenuItem (ActiveDlg->GethMenu (),    IDM_ABSCHLUSS,   MF_GRAYED);
                if (ActiveDlg->GetToolbar2 () != NULL && 
                    ActiveDlg->GetToolbar2 ()->GetCfield ("absch") != NULL)
                {
                      ActiveDlg->GetToolbar2 ()->GetCfield ("absch")->Enable (FALSE);
                }
            }

  		    InEnterPos = FALSE;
            if (Toolbar2 != NULL)
            {
                 ((ColButton *)Toolbar2->GetCfield ("del")->GetFeld ())->bmp = 
					 ImageDelete.Inactive;
                 ((ColButton *)Toolbar2->GetCfield ("insert")->GetFeld ())->bmp = 
                     ImageInsert.Inactive;
                 if (Toolbar2->GetCfield ("f6") != NULL)
                 {
                    ((ColButton *)Toolbar2->GetCfield ("f6")->GetFeld ())->bmp = 
                        ImageList.Image;
                 }
                 if  (ActiveDlg->GetToolbar2 ()->GetCfield ("best") != NULL)
                 {
                      ActiveDlg->GetToolbar2 ()->GetCfield ("best")->Enable (TRUE);
                      ActiveDlg->GetToolbar2 ()->GetCfield ("bezugsquellen")->Enable (FALSE);
                 }
                 Toolbar2->display ();
            }
            ChangeF6 (IDM_INSERT);
            ActiveDlg->SetCurrentCfield (CurrentCfield);
            ActiveDlg->SetCurrentFocus ();
            TestEnablePrint ();
            return TRUE;
        }
        return FALSE;
} 


BOOL WeDlg::OnKey6 ()
{
        int DockY;
        RECT rect;
        CFIELD *CurrentCfield;

		if (DatError == TRUE) 
		{
			if (TestWeDat () == 0) 
			{
				return FALSE;
			}
			if (TestBlgEingDat () == 0) 
			{
				return FALSE;
			}
		}
		weWork.OhneFilEkAktionPar ();
  	    fPos.GetText ();
        FromForm (dbfields); 
		memcpy (&_a_bas, &_a_bas_null, sizeof (struct A_BAS)); //050809 damit es beim Scannen sauber l�uft
		if (ListType == 1)
		{
			return EnterWeList ();
		}

        if (ListDlg != NULL) return FALSE;

        CurrentCfield = ActiveDlg->GetCurrentCfield ();
        if (fHead.GetCfield ("lief")->IsDisabled ())
        {
            if (ReadBest && best_kopf.best_blg > 0)
            {
                 FromForm (dbfields); 
                 weWork.BestToWe ();
                 ReadBest = FALSE;
            }
            we_kopf.we_dat = dasc_to_long (wekopff.we_dat);
            we_kopf.blg_eing_dat = dasc_to_long (wekopff.blg_eing_dat);
            if (Toolbar2 != NULL)
            {
                 ((ColButton *)Toolbar2->GetCfield ("del")->GetFeld ())->bmp = 
					 ImageDelete.Image;
                 ((ColButton *)Toolbar2->GetCfield ("insert")->GetFeld ())->bmp = 
                     ImageInsert.Image;
                 if (Toolbar2->GetCfield ("f6") != NULL)
                 {
                    ((ColButton *)Toolbar2->GetCfield ("f6")->GetFeld ())->bmp = 
                         ImageList.Inactive;
                 }
                 if  (ActiveDlg->GetToolbar2 ()->GetCfield ("best") != NULL)
                 {
                      ActiveDlg->GetToolbar2 ()->GetCfield ("best")->Enable (FALSE);
                      ActiveDlg->GetToolbar2 ()->GetCfield ("absch")->Enable (FALSE);
                      ActiveDlg->GetToolbar2 ()->GetCfield ("best")->display ();
                      ActiveDlg->GetToolbar2 ()->GetCfield ("absch")->display ();
                 }
            }

            ChangeF6 (IDM_LIST);
            ToolBar_SetState (ActiveDlg->GethwndTB (),IDM_DELALL, TBSTATE_INDETERMINATE);
            EnableMenuItem (ActiveDlg->GethMenu (),   IDM_DELALL, MF_GRAYED);
            EnableMenuItem (ActiveDlg->GethMenu (),   IDM_DELETE, MF_ENABLED);
//            EnableMenuItem (ActiveDlg->GethMenu (),   IDM_EXTRA_DATA, MF_ENABLED);
            EnableMenuItem (ActiveDlg->GethMenu (),   VK_F8, MF_ENABLED);
            EnableMenuItem (ActiveDlg->GethMenu (),   IDM_BEST,   MF_GRAYED);
            EnableMenuItem (ActiveDlg->GethMenu (),   IDM_ABSCHLUSS,   MF_GRAYED);
            EnableMenuItem (ActiveDlg->GethMenu (),   IDM_TXTDLG, MF_GRAYED);
            Enable (&fWeKopf, EnablePos, FALSE); 
            ListDlg = new LISTDLG ();
			ListDlg->SetDlg (this);
			ListDlg->SethMenu (ActiveDlg->GethMenu ());
            if (LsIdentAttr > 0)
            {
                ListDlg->SetDefaultIdent (wekopff.ls_ident);
            }
            if (LsChargeAttr > 0)
            {
                ListDlg->SetDefaultCharge (wekopff.ls_charge);
            }
		    if (LineSet)
			{
				ListDlg->GeteListe ()->SetLines (ListLines);
			}

			if (ColorSet)
			{
		        ListDlg->GeteListe ()->SetColor (ListColor);
		        ListDlg->GeteListe ()->SetBkColor (ListBkColor);
			}
            ListDlg->SetWork (&weWork);
            EnableMenuItem (ActiveDlg->GethMenu (),   IDM_CHOISE, MF_ENABLED);

            if (DockList == 0)
            {
                     ListDlg->SetWinStyle (ListDlg->GetWinStyle () | WS_CAPTION | 
                                                            WS_SYSMENU |
                                                            WS_MINIMIZEBOX |
                                                            WS_MAXIMIZEBOX);
                     ListDlg->SetDockWin (FALSE);
                     ListDlg->SetDimension (750, 400);
                     ListDlg->SetModal (TRUE);
                     EnableWindow (hMainWindow, FALSE);
            }
            else
            {
                     if (DockMode)
                     {
                        ListDlg->SetWinStyle (WS_THICKFRAME | WS_CHILD | WS_CLIPSIBLINGS);
                        Enable (&fWeKopf, EnablePos, TRUE); 
                        DestroyPos ();
                     }
                     GetWindowRect (hWnd, &rect);
                     DockY = fPos.GetCfield ("pos_frame")->GetYP () + rect.top;
                     ListDlg->SetDockY (DockY);
            }
            ListDlg->SethMenu (hMenu);
            ListDlg->SethwndTB (hwndTB);
            if (atoi (wekopff.pr_erf_kz) == 3)
            {
                    WithPosEk = TRUE;
            }
            else
            {
                    WithPosEk = FALSE;
            }
			if (atoi (wekopff.pr_erf_kz) == 3 && WithPosEk == FALSE)
			{
					ChangePosEk ();
			}
            if (atoi (fPos.GetCfield ("we_pr_kz")->GetFeld ()) == 1)
            {
                    ListDlg->SetFieldAttr ("pr_ek",     REMOVED);
                    ListDlg->DestroyListField ("pr_ek");
                    ListDlg->SetFieldAttr ("pr_vk",     REMOVED);
                    ListDlg->DestroyListField ("pr_vk");
                    ListDlg->SetFieldAttr ("fil_ek",     REMOVED);
                    ListDlg->DestroyListField ("fil_ek");
                    ListDlg->SetFieldAttr ("netto_ek", REMOVED);
                    ListDlg->DestroyListField ("netto_ek");
					ListDlg->SetPrFieldText (FALSE);
            }
            else if (weWork.WeFilBewPar () == FALSE)
			{
				//LuD 310706 vk auch wenn we_fil_bew_par auf 0
//                    ListDlg->SetFieldAttr ("pr_vk",     REMOVED);
//                    ListDlg->DestroyListField ("pr_vk");
                    ListDlg->SetFieldAttr ("fil_ek",     REMOVED);
                    ListDlg->DestroyListField ("fil_ek");
			}
            else if (weWork.WeVkPar () > 0)
			{
                    ListDlg->SetFieldAttr ("pr_vk",     EDIT);
			}

            if (atoi (fPos.GetCfield ("we_pr_kz")->GetFeld ()) > 1)
			{
/*
                    ListDlg->SetFieldAttr ("pr_ek", EDIT);
                    ListDlg->SetFieldAttr ("netto_ek", EDIT);
					ListDlg->SetPrFieldText (TRUE);
*/
			}

            if (atoi (fPos.GetCfield ("we_pr_kz")->GetFeld ()) < 3)
            {
                    ListDlg->SetFieldAttr ("pos_wert", REMOVED);
                    ListDlg->DestroyListField ("pos_wert");
					ListDlg->SetInhFieldText (FALSE);
			}
            else
            {
					ListDlg->SetInhFieldText (TRUE);
            }
            if (WithLiefBest == FALSE && WithAkrz == FALSE 
                && TwoRows == FALSE)
            {
                    ListDlg->Seta_bz2Removed (TRUE);
            }
            else
            {
                    ListDlg->Seta_bz2Removed (FALSE);
            }
            if (WithLiefBest == FALSE)
            {
                    if (TwoRows == FALSE)
                    {
                           ListDlg->Setlief_bestRemoved (TRUE);
                    }
                    else if (WithAkrz)
                    {
                           ListDlg->Setlief_bestRemoved (TRUE);
                    }
                    else
                    {
                           ListDlg->Setlief_bestRemoved (FALSE);
                    }

                    ListDlg->SetWithLiefBest (FALSE);
            }
            else
            {
                    ListDlg->Setlief_bestRemoved (FALSE);
                    ListDlg->Seta_krzRemoved (TRUE);
                    ListDlg->SetWithLiefBest (TRUE);
            }
            if (WithAkrz == FALSE)
            {
                    ListDlg->Seta_krzRemoved (TRUE);
                    ListDlg->SetWithAkrz (FALSE);
            }
            else
            {
                    ListDlg->Seta_krzRemoved (FALSE);
                    ListDlg->Setlief_bestRemoved (TRUE);
                    ListDlg->SetWithAkrz (TRUE);
            }
			if (WithPosEk)
			{
				    ListDlg->SetWithPosEk (TRUE);
                    EnableMenuItem (ActiveDlg->GethMenu (),   IDM_EPOSEK,   MF_ENABLED);
			}
			else
			{
				    ListDlg->SetWithPosEk (FALSE);
			}

            ListDlg->SetWithSearchBest (WithSearchBest);
            if (ListButtons == FALSE)
            {
                    ListDlg->OrFieldAttr ("me_choise", REMOVED);
            }

            if (ButtonSize == BIG)
            {
                    ListDlg->SetButtonSize (BIG);
            }

            ListDlg->SethMainWindow (hWnd);
            ListDlg->SetTextMetric (&DlgTm);
            ListDlg->SetLineRow (100);
//          ListDlg->SetToolbar2 (this->Toolbar2);
//            CreateListQuikInfos (hMainWindow);
            ListDlg->EnterList ();
            if (DockMode)
            {
                     SetPos ();
            }
            delete ListDlg;
			weWork.SetLsts (NULL);
            CreateListQuikInfos (hMainWindow);
            SetWindowPos (hMainWindow, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
            EnableWindows (hWnd, TRUE);
            ListDlg = NULL;
            Enable (&fWeKopf, EnablePos,  TRUE); 
            EnableMenuItem (ActiveDlg->GethMenu (),   IDM_DELETE,   MF_GRAYED);
            EnableMenuItem (ActiveDlg->GethMenu (),   IDM_EXTRA_DATA, MF_GRAYED);
            EnableMenuItem (ActiveDlg->GethMenu (),   IDM_MHD_DATA, MF_GRAYED);
            EnableMenuItem (ActiveDlg->GethMenu (),   VK_F8,        MF_GRAYED);
            ToolBar_SetState (ActiveDlg->GethwndTB (),IDM_DELALL,   TBSTATE_ENABLED);
            EnableMenuItem (ActiveDlg->GethMenu (),   IDM_DELALL,   MF_ENABLED);
            EnableMenuItem (ActiveDlg->GethMenu (),   IDM_EPOSEK,   MF_GRAYED);
            EnableMenuItem (ActiveDlg->GethMenu (),   IDM_BEST,     MF_ENABLED);
            if (textWork != NULL)
            {
                 EnableMenuItem (ActiveDlg->GethMenu (),   IDM_TXTDLG,   MF_ENABLED);
            }
            if (weWork.PosExist ())
            {
                EnableMenuItem (ActiveDlg->GethMenu (),    IDM_ABSCHLUSS,   MF_ENABLED);
                if (ActiveDlg->GetToolbar2 () != NULL && 
                    ActiveDlg->GetToolbar2 ()->GetCfield ("absch") != NULL)
                {
                      ActiveDlg->GetToolbar2 ()->GetCfield ("absch")->Enable (TRUE);
                }
            }
            else
            {
                EnableMenuItem (ActiveDlg->GethMenu (),    IDM_ABSCHLUSS,   MF_GRAYED);
                if (ActiveDlg->GetToolbar2 () != NULL && 
                    ActiveDlg->GetToolbar2 ()->GetCfield ("absch") != NULL)
                {
                      ActiveDlg->GetToolbar2 ()->GetCfield ("absch")->Enable (FALSE);
                }
            }

            if (Toolbar2 != NULL)
            {
                 ((ColButton *)Toolbar2->GetCfield ("del")->GetFeld ())->bmp = 
					 ImageDelete.Inactive;
                 ((ColButton *)Toolbar2->GetCfield ("insert")->GetFeld ())->bmp = 
                     ImageInsert.Inactive;
                 if (Toolbar2->GetCfield ("f6") != NULL)
                 {
                    ((ColButton *)Toolbar2->GetCfield ("f6")->GetFeld ())->bmp = 
                        ImageList.Image;
                 }
                 if  (ActiveDlg->GetToolbar2 ()->GetCfield ("best") != NULL)
                 {
                      ActiveDlg->GetToolbar2 ()->GetCfield ("best")->Enable (TRUE);
                 }
                 Toolbar2->display ();
            }
            ChangeF6 (IDM_INSERT);
            ActiveDlg->SetCurrentCfield (CurrentCfield);
            ActiveDlg->SetCurrentFocus ();
            TestEnablePrint ();
            return TRUE;
        }
        return FALSE;
} 


BOOL WeDlg::OnKey7 ()
{
        if (fHead.GetCfield ("fil")->IsDisabled () && textWork != NULL)
        {
               return EnterTextDlg ();
        }
        return FALSE;
}

BOOL WeDlg::OnKey8 ()
{
        return EnterAbschlussDlg ();
}

BOOL WeDlg::OnMove (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (hWnd != this->hMainWindow) return FALSE; 
        DLG::OnMove (hWnd,msg,wParam,lParam); 
        if (ListDlg != NULL)
        {
            ListDlg->MoveMamain1 ();
        }
        return FALSE;
}
             

BOOL WeDlg::ShowMdn (void)
{
             char mdn_nr [6];
             SEARCHMDN SearchMdn;
 
             fHead.SetCurrentName ("mdn");
             SearchMdn.SetParams (DLG::hInstance, ActiveDlg->GethWnd ());
             SearchMdn.Setawin (ActiveDlg->GethMainWindow ());
             SearchMdn.Search ();
             if (SearchMdn.GetKey (mdn_nr) == TRUE)
             {
                 sprintf (wekopff.mdn, "%4d", atoi (mdn_nr));
                 weWork.GetMdnName (wekopff.mdn_krz,   atoi (wekopff.mdn));
                 fHead.SetText ();
                 fHead.SetCurrentName ("mdn");
             }
             return TRUE;
}


BOOL WeDlg::ShowFil (void)
{
             char fil_nr [6];
             SEARCHFIL SearchFil;
 
             if (atoi (wekopff.mdn) == 0) return TRUE;

             fHead.SetCurrentName ("fil");
             SearchFil.SetParams (DLG::hInstance, ActiveDlg->GethWnd (), 
                                                  atoi (wekopff.mdn));
             SearchFil.Setawin (ActiveDlg->GethMainWindow ());
             SearchFil.Search ();
             if (SearchFil.GetKey (fil_nr) == TRUE)
             {
                 sprintf (wekopff.fil, "%4d", atoi (fil_nr));
                 weWork.GetFilName (wekopff.fil_krz,   atoi (wekopff.mdn), atoi (wekopff.fil));
                 fHead.SetText ();
                 fHead.SetCurrentName ("fil");
             }
             return TRUE;
}

BOOL WeDlg::ShowLief (void)
{
             char lief [17];
             SEARCHLIEF SearchLief;
 
             fHead.SetCurrentName ("lief");
             SearchLief.SetParams (DLG::hInstance, ActiveDlg->GethWnd ()); 
             SearchLief.Setawin (ActiveDlg->GethMainWindow ());
                                                  
             SearchLief.Search ();
             if (SearchLief.GetKey (lief) == TRUE)
             {
                 sprintf (wekopff.lief, "%s", lief);
                 weWork.GetLiefName (wekopff.lief_krz,  atoi (wekopff.mdn), wekopff.lief);
                 fHead.SetText ();
                 fHead.SetCurrentName ("lief");
             }
             return TRUE;
}

BOOL WeDlg::ShowLief (char *stext)
{
             char lief [17];
             SEARCHLIEF SearchLief;
 
             fHead.SetCurrentName ("lief");
             SearchLief.SetParams (DLG::hInstance, ActiveDlg->GethWnd ()); 
             SearchLief.Setawin (ActiveDlg->GethMainWindow ());
                                       
             clipped (stext);
             SearchLief.Search (stext);
             if (SearchLief.GetKey (lief) == TRUE)
             {
                 sprintf (wekopff.lief, "%s", lief);
                 weWork.GetLiefName (wekopff.lief_krz,  atoi (wekopff.mdn), wekopff.lief);
                 fHead.SetText ();
                 PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
                 return TRUE;
             }
             return FALSE;
}

BOOL WeDlg::ShowWeKopf (void)
{
             char lief_rech_nr [17];
             SEARCHWEKOPF SearchWeKopf;
 
             fHead.SetCurrentName ("lief_rech_nr");
             fHead.GetText ();
             if (atoi (wekopff.mdn) == 0)
             {
                disp_mess ("Mandant > 0 eingeben", 2);
                fHead.SetCurrentName ("mdn");
                return 0;
             }
             clipped (wekopff.lief);
             if (strcmp (wekopff.lief, " ") <= 0)
             {
                 disp_mess ("Eine Lieferantennummer mu� eingegeben werden !!", 2);
                 fHead.SetCurrentName ("lief");
                 return TRUE;
             }
/*
             clipped (wekopff.lief_rech_nr);
             if (strcmp (wekopff.lief_rech_nr, " ") <= 0)
             {
                 disp_mess ("Eine Lieferscheinnummer mu� eingegeben werden !!", 2);
                 fHead.SetCurrentName ("lief");
                 return TRUE;
             }
*/
             FromForm (dbheadfields);
             SearchWeKopf.SetParams (DLG::hInstance, ActiveDlg->GethWnd ()); 
             SearchWeKopf.Setawin (ActiveDlg->GethMainWindow ());
                                                  
             SearchWeKopf.Search ();
             if (SearchWeKopf.GetKey (lief_rech_nr) == TRUE)
             {
                 sprintf (wekopff.lief_rech_nr, "%s", lief_rech_nr);
                 fHead.SetText ();
                 fHead.SetCurrentName ("lief_rech_nr");
             }
             return TRUE;
}

BOOL WeDlg::EnterTextDlg (void)
{
            TextDlg *textDlg = NULL;

            if (textWork == NULL) return FALSE;

            weWork.ReadTxt ();
            textDlg = new TextDlg (textWork , -1, -1, 60, 10, NULL, StdSize, FALSE);
            textDlg->SetStyle (WS_POPUP | WS_DLGFRAME | WS_CAPTION | WS_SYSMENU);
            textDlg->OpenScrollWindow (hInstance, hWnd);
            EnableWindows (hWnd, FALSE);
            textDlg->ProcessMessages ();
            delete textDlg;
            SetCurrentFocus ();
            return TRUE;
}


BOOL WeDlg::WriteWeKopf ()
{
        AutoNrClass AutoNr;

        syskey = KEY12;

        InsMode = FALSE;
        fWork->GetText ();
        FromForm (dbfields); 
        if (fHead.GetCfield ("lief")->IsDisabled ())
        {
               FromForm (dbfields);
/*
               if (NewRec)
               {
                   we_kopf.lfd = AutoNr.GetGJ (we_kopf.mdn, we_kopf.fil, "we_nr");
                   ToForm (dbfields);
               }
*/
               we_kopf.we_dat = dasc_to_long (wekopff.we_dat);
               we_kopf.blg_eing_dat = dasc_to_long (wekopff.blg_eing_dat);
               weWork.WriteWeKopf ();
               NewRec = FALSE;
               return TRUE;
        }
        return FALSE;
}



void WeDlg::PosEnd (void)
{
        EnableMenuItem (hMenu, IDM_SHOWKONTRAKT,   MF_GRAYED);
        Enable (&fWeKopf, EnableHead, TRUE); 
        Enable (&fWeKopf, EnablePos,  FALSE);
  	    EnableMdnFil ();
//        weWork.CommitWork ();
        weWork.InitRec ();
        InitPos ();
        fPos.SetText ();
        fHead.SetCurrentName ("lief");
        strcpy ((char *)fHead.GetCfield ("lief_rech_nr")->GetFeld (), " ");
        fHead.GetCfield ("lief_rech_nr")->SetText (); 
        ToolBar_SetState (ActiveDlg->GethwndTB (),IDM_DELALL, TBSTATE_INDETERMINATE);
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELETE,   MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),   IDM_EXTRA_DATA, MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),   IDM_MHD_DATA, MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELALL,   MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),  IDM_LIST,   MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),   VK_F12,     MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),   IDM_QM,     MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),   IDM_BEST,   MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),   IDM_TXTDLG, MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),    IDM_ABSCHLUSS,   MF_GRAYED);
        SendMessage (fPos.GetCfield ("liste")->GethWnd (), 
                            BM_SETIMAGE, (WPARAM) IMAGE_BITMAP, 
 		  				    (LPARAM) ListBitmapi);
        if (ActiveDlg->GetToolbar2 () != NULL)
        {
                 if  (ActiveDlg->GetToolbar2 ()->GetCfield ("f6") != NULL)
                 {
                      ((ColButton *)ActiveDlg->GetToolbar2 ()->GetCfield ("f6")->GetFeld ())->bmp = 
                                    ImageList.Inactive;
                 }
                 if  (ActiveDlg->GetToolbar2 ()->GetCfield ("qs") != NULL)
                 {
                      ActiveDlg->GetToolbar2 ()->GetCfield ("qs")->Enable (FALSE);
                 }
                 if  (ActiveDlg->GetToolbar2 ()->GetCfield ("best") != NULL)
                 {
                      ActiveDlg->GetToolbar2 ()->GetCfield ("best")->Enable (FALSE);
                 }
                 if  (ActiveDlg->GetToolbar2 ()->GetCfield ("absch") != NULL)
                 {
                      ActiveDlg->GetToolbar2 ()->GetCfield ("absch")->Enable (FALSE);
                 }
                 ActiveDlg->GetToolbar2 ()->display ();
        }
}

BOOL WeDlg::EnterAbschlussDlg (void)
{
            static char rosipath [512];
            static BOOL RosiOK = FALSE;
            char progname [512];
            char AbschlKz [2];

            if (RosiOK == FALSE)
            {
                   if (getenv (RSDIR) == NULL)
                   {
                       return FALSE;
                   }
                   strcpy (rosipath, getenv (RSDIR));
                   RosiOK = TRUE;
            }

            AbschlDlg *abschlDlg = NULL;

//            if (textWork == NULL) return FALSE;

            WriteWeKopf ();
			weWork.DecKontrakt ();
            abschlDlg = new AbschlDlg (-1, -1, 60, 10, "Abschlu� Wareneingang", StdSize, FALSE);
/*
            if (atoi (wekopff.pr_erf_kz) == 1)
            {
                abschlDlg->WriteAbschl ();
                sprintf (progname, "%s\\BIN\\rswrun weldr351 L "
                                     "%hd %hd %s %s D",
                                     rosipath,
                                     we_kopf.mdn, we_kopf.fil, we_kopf.lief,
									 we_kopf.lief_rech_nr);

                ProcWaitExecEx (progname, SW_SHOWMINIMIZED, 0, 40, -1, 0);
                syskey = KEY12;
            }
            else
*/
            {
                abschlDlg->SetStyle (WS_POPUP | WS_DLGFRAME | WS_CAPTION | WS_SYSMENU);
                abschlDlg->Pack (0, 2);
                abschlDlg->SetWinBackground (WinBackground);
                abschlDlg->OpenScrollWindow (hInstance, hMainWindow);
                EnableWindows (hWnd, FALSE);
                abschlDlg->ProcessMessages ();
            }
            strcpy (AbschlKz, abschlDlg->GetAbschlKz ());
            delete abschlDlg;
            EnableWindows (hWnd, TRUE);
            SetCurrentFocus ();
            if (syskey != KEY5)
            {
                  weWork.CommitWork ();
/***********
			        if(getenv("testmode") != NULL)
					{
						if (atoi(getenv("testweldr")) == 1)
						{
							sprintf (progname, "%s\\BIN\\rswrun -q c:\\temp\\weldr351.log weldr351 %s "
                                   "%hd %hd %s %s %s",
                                   rosipath, we_kopf.blg_typ,
                                   we_kopf.mdn, we_kopf.fil, we_kopf.lief,
							 we_kopf.lief_rech_nr, AbschlKz);
						}
						else
						{
							sprintf (progname, "%s\\BIN\\rswrun weldr351 %s "
                                   "%hd %hd %s %s %s",
                                   rosipath, we_kopf.blg_typ,
                                   we_kopf.mdn, we_kopf.fil, we_kopf.lief,
							 we_kopf.lief_rech_nr, AbschlKz);
						}
					}
					else
					{
*********************/
						sprintf (progname, "%s\\BIN\\rswrun weldr351 %s "
                                   "%hd %hd %s %s %s",
                                   rosipath, we_kopf.blg_typ,
                                   we_kopf.mdn, we_kopf.fil, we_kopf.lief,
							 we_kopf.lief_rech_nr, AbschlKz);
//					}


                  ProcWaitExecEx (progname, SW_SHOWMINIMIZED, 0, 40, -1, 0);
                  weWork.BeginWork ();
				  if (RwKopf)
				  {
                      weWork.ReWriteWeKopf ();
				  }
                  weWork.CommitWork ();
                  PosEnd ();
            }
            return TRUE;
}

BOOL WeDlg::ShowKontrakt (void)
{
	    char command [512];

		if (best_kopf.kontrakt_nr == 0l) return TRUE;

  	    best_kopf.mdn = we_kopf.mdn;
	    best_kopf.fil = we_kopf.fil;
		sprintf (command, "dbinfot -u\"Kontrakt %ld  Lieferant %s\" "
			              "kontrakt "
						  "\"where mdn = %hd and fil = %hd "
						  "and lief = '%s' and best_blg = %ld\"",
						  best_kopf.kontrakt_nr, we_kopf.lief,
						  best_kopf.mdn, best_kopf.fil,
						  we_kopf.lief, best_kopf.kontrakt_nr);
        ProcExec (command, SW_SHOWNORMAL, -1, 0, -1, 0);
		return TRUE;
}
						  

BOOL WeDlg::ShowBest (void)
{
             SEARCHBEST SearchBest;
             char best_blg [80];

			 SearchBest.Debug = &Debug;
             fHead.GetText ();
             if (atoi (wekopff.mdn) == 0) return FALSE;
             clipped (wekopff.lief);
             if (strcmp (wekopff.lief, " ") <= 0) return FALSE;

             FromForm (dbheadfields);
             best_kopf.mdn = we_kopf.mdn;
             best_kopf.fil = we_kopf.fil;
             strcpy (best_kopf.lief, we_kopf.lief);
			 Debug.Output ("---Auswahl Bestellungen---\n");

             SearchBest.SetParams (DLG::hInstance, ActiveDlg->GethWnd ()); 
             SearchBest.Setawin (ActiveDlg->GethMainWindow ());
                                                  
             SearchBest.Search ();
             if (SearchBest.GetKey (best_blg) == TRUE)
             {
				 Text text;
				 text.Format ("Auswahl beendet : Belegenummer = %s\n", best_blg);
			     Debug.Output (text);
                 weWork.SetMeStat (SearchBest.GetMeStat ());
                 best_kopf.best_blg = atol (best_blg);
				 text.Format ("Belegenummer als long = %ld\n", best_kopf.best_blg);
			     Debug.Output (text);
                 ActiveDlg->SetCurrentFocus ();
                 if (strcmp (clipped (wekopff.lief_rech_nr), " ") <= 0)
                 {
  				         if (weWork.GenLiefRechNr (ActiveDlg->GethMainWindow (), 
                                               wekopff.lief_rech_nr,
                                               NULL) != NULL)
//                                               ChargeMacro);
                         {
                             fHead.GetCfield ("lief_rech_nr")->SetText ();
                         }

                 }
				 text.Format ("Belegenummer nach Generierung = %ld\n", best_kopf.best_blg);
			     Debug.Output (text);
                 if (fHead.GetCfield ("lief_rech_nr")->IsDisabled ())
                 {
                         FromForm (dbfields); 
                         weWork.BestToWe ();
                 }
                 else
                 {
                         ReadBest = TRUE;
                 }

				 text.Format ("Belegenummer nach BestToWe = %ld\n", best_kopf.best_blg);
			     Debug.Output (text);
                 sprintf (wekopff.koresp_best0, "%ld", best_kopf.best_blg);                 
                 fPos.GetCfield ("koresp_best0")->SetText ();
				 text.Format ("Belegenummer vor Lesen = %ld\n", best_kopf.best_blg);
			     Debug.Output (text);
				 weWork.ReadBestKopf ();
				 if (best_kopf.kontrakt_nr > 0l)
				 {
                       EnableMenuItem (hMenu, IDM_SHOWKONTRAKT,   MF_ENABLED);
				 }
             }
             return TRUE;
}


int WeDlg::TestPtab (void)
{
             CFIELD *Cfield;
             char *Item_bz;


             EnableMenuItem (ActiveDlg->GethMenu (), IDM_CHOISE,   MF_GRAYED);

             if (syskey == KEY5) return 0;

             Cfield = ActiveDlg->GetCurrentCfield ();

             if (strcmp (Cfield->GetName (), "we_erf_kz") == 0)
             {
                   Item_bz = fWeKopf.GetCfield ("we_erf_kz_bz")->GetName ();
             }

             else if (strcmp (Cfield->GetName (), "we_pr_kz") == 0)
             {
                   Item_bz = fWeKopf.GetCfield ("we_pr_kz_bz")->GetName ();
             }

             Cfield->GetText ();
             if (weWork.GetPtab (fWeKopf.GetCfield (Item_bz)->GetFeld (),
			                     Cfield->GetName (),  
					   		     Cfield->GetFeld ()) == -1)
             {
                    disp_mess ("Ung�ltige Eingabe", 2);
                    fPos.SetCurrentName (Cfield->GetName ());
                    return 0;
             }
             fWeKopf.GetCfield (Item_bz)->SetText ();
             return 0;
}

int WeDlg::TestWeErfKz (void)
{
             TestPtab ();
             if (syskey != KEY5)
             {
                  strcpy (wekopff.a_best_info, ptabn.ptbez);
             }
             fHead.GetCfield ("we_erf_info")->Invalidate (TRUE);
             fHead.GetCfield ("we_erf_info")->SetText ();
             fWeKopf.GetCfield ("we_erf_kz")->GetText (); 
             if (atoi (wekopff.a_best_kz) == 1)
             {
                    CheckMenuItem (ActiveDlg->GethMenu (), IDM_LIEFBEST,   MF_UNCHECKED);
                    CheckMenuItem (ActiveDlg->GethMenu (), IDM_AKRZ,       MF_UNCHECKED);
                    ActiveDlg->SetWithLiefBest (FALSE);  
                    ActiveDlg->SetWithAkrz (FALSE);  
             }
             else if (atoi (wekopff.a_best_kz) == 2)
             {
                    CheckMenuItem (ActiveDlg->GethMenu (), IDM_LIEFBEST,   MF_CHECKED);
                    CheckMenuItem (ActiveDlg->GethMenu (), IDM_SEARCHBEST, MF_CHECKED);
                    CheckMenuItem (ActiveDlg->GethMenu (), IDM_AKRZ,       MF_UNCHECKED);
                    ActiveDlg->SetWithLiefBest (TRUE);  
//                    ActiveDlg->SetWithSearchBest (TRUE);  
                    ActiveDlg->SetWithAkrz (FALSE);  
             }
             else if (atoi (wekopff.a_best_kz) == 3)
             {
                    CheckMenuItem (ActiveDlg->GethMenu (), IDM_AKRZ,       MF_CHECKED);
                    CheckMenuItem (ActiveDlg->GethMenu (), IDM_LIEFBEST,   MF_UNCHECKED);
                    ActiveDlg->SetWithAkrz (TRUE);  
                    ActiveDlg->SetWithLiefBest (FALSE);  
             }
             else
             {
                    CheckMenuItem (ActiveDlg->GethMenu (), IDM_LIEFBEST,   MF_UNCHECKED);
                    CheckMenuItem (ActiveDlg->GethMenu (), IDM_AKRZ,       MF_UNCHECKED);
                    ActiveDlg->SetWithLiefBest (FALSE);  
                    ActiveDlg->SetWithAkrz (FALSE);  
             }
             return 0;
}

int WeDlg::TestGenCharge (void)
{
             
       char *charge; 

       if (ActiveDlg == NULL) return 0;

       fWeKopf.GetCfield ("ls_ident")->GetText (); 
       fWeKopf.GetCfield ("ls_charge")->GetText (); 
       clipped (wekopff.ls_charge);
       if (LsChargeAttr == 1 &&
             strcmp (wekopff.ls_charge, " ") <= 0)
       {
				charge = weWork.GenCharge (ActiveDlg->GethMainWindow (), 
                                           wekopff.ls_ident, ChargeMacro);
                if (charge != NULL)
                {
				        strcpy (wekopff.ls_charge, charge);
                }
                else
                {
                        disp_mess ("Charge kann nicht generiert werden", 2);
                }
                fWeKopf.GetCfield ("ls_charge")->SetText (); 
       }
       return 0;
}


             
int WeDlg::TestPrErfKz (void)
{
             TestPtab ();
             fWeKopf.GetCfield ("we_pr_kz")->GetText (); 
             if (atoi (wekopff.pr_erf_kz) == 1)
             {
                    CheckMenuItem (ActiveDlg->GethMenu (), IDM_EK,    MF_UNCHECKED);
                    CheckMenuItem (ActiveDlg->GethMenu (), IDM_POSEK, MF_UNCHECKED);
                    ActiveDlg->SetWithEk (FALSE);  
                    ActiveDlg->SetWithPosEk (FALSE);  
             }
             if (atoi (wekopff.pr_erf_kz) == 2)
             {
                    CheckMenuItem (ActiveDlg->GethMenu (), IDM_EK,    MF_CHECKED);
                    CheckMenuItem (ActiveDlg->GethMenu (), IDM_POSEK, MF_UNCHECKED);
                    ActiveDlg->SetWithEk (TRUE);  
                    ActiveDlg->SetWithPosEk (FALSE);  
             }
             if (atoi (wekopff.pr_erf_kz) == 3)
             {
                    CheckMenuItem (ActiveDlg->GethMenu (), IDM_EK,    MF_UNCHECKED);
                    CheckMenuItem (ActiveDlg->GethMenu (), IDM_POSEK, MF_CHECKED);
                    ActiveDlg->SetWithEk (FALSE);  
                    ActiveDlg->SetWithPosEk (TRUE);  
             }
             return 0;
}


BOOL WeDlg::ShowPtab (char *item, char *item_bz)
{
             if (weWork.ShowPtabEx (hWnd, (char *) fWeKopf.GetCfield (item)->GetFeld (),
				                    item) == -1)
			 {
				 ActiveDlg->SetCurrentFocus ();
				 return TRUE;
			 }
			 fWeKopf.GetCfield (item)->SetText ();
			 weWork.GetPtab (fWeKopf.GetCfield (item_bz)->GetFeld (),
				             item,  
							 fWeKopf.GetCfield (item)->GetFeld ());
			 fWeKopf.GetCfield (item_bz)->SetText ();
			 return TRUE;
}


BOOL WeDlg::OnKey9 ()
{
         CFIELD *Cfield;
         HWND hWnd;

         syskey = KEY9;
         hWnd = GetFocus ();


         if (ListDlg != NULL) return FALSE;

         if (hWnd == fHead.GetCfield ("mdn_choise")->GethWnd ())
         {
             return ShowMdn ();
         }

         if (hWnd == fHead.GetCfield ("fil_choise")->GethWnd ())
         {
             return ShowFil ();
         }

         if (hWnd == fHead.GetCfield ("lief_choise")->GethWnd ())
         {
             return ShowLief ();
         }
         if (hWnd == fHead.GetCfield ("lief_rech_nr_choise")->GethWnd ())
         {
             return ShowWeKopf ();
         }

         if (hWnd == fHead.GetCfield ("blg_typ_choise")->GethWnd ())
         {
             return ShowPtab ("blg_typ", "blg_typ_bz");
         }

         if (hWnd == fPos.GetCfield ("we_erf_kz_choise")->GethWnd ())
         {
             ShowPtab ("we_erf_kz", "we_erf_kz_bz");
             return TestWeErfKz ();
         }

         if (hWnd == fPos.GetCfield ("we_pr_kz_choise")->GethWnd ())
         {
             ShowPtab ("we_pr_kz", "we_pr_kz_bz");
             return TestPrErfKz ();
         }

         Cfield = ActiveDlg->GetCurrentCfield ();

         if (Cfield == NULL) 
         {
             return FALSE;
         }

         if (strcmp (Cfield->GetName (), "mdn") == 0)
         {
             return ShowMdn ();
         }

         if (strcmp (Cfield->GetName (), "fil") == 0)
         {
             return ShowFil ();
         }

         if (strcmp (Cfield->GetName (), "lief") == 0)
         {
             return ShowLief ();
         }
         if (strcmp (Cfield->GetName (), "lief_rech_nr") == 0)
         {
             return ShowWeKopf ();
         }

         if (strcmp (Cfield->GetName (), "blg_typ") == 0)
         {
             return ShowPtab ("blg_typ", "blg_typ_bz");
         }
         if (strcmp (Cfield->GetName (), "we_erf_kz") == 0)
         {
             ShowPtab ("we_erf_kz", "we_erf_kz_bz");
             return TestWeErfKz ();
         }
         if (strcmp (Cfield->GetName (), "we_pr_kz") == 0)
         {
             return ShowPtab ("we_pr_kz", "we_pr_kz_bz");
         }
         return FALSE;
}

BOOL WeDlg::OnKey10 ()
{
        if (fHead.GetCfield ("fil")->IsDisabled ())
        {
                EnterQmKopf ();
        }
        return TRUE;
}

BOOL WeDlg::OnKey11 ()
{
        if (weWork.PosExist () == FALSE)
		{
			   return FALSE;
		}
        if (fHead.GetCfield ("fil")->IsDisabled ())
		{
               EnterAbschlussDlg ();
		}
		return TRUE;
}

BOOL WeDlg::OnPaint (HWND hWnd,HDC hdc, UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (fWork && this->hWnd == hWnd)
        {
             PrintMess (MessText);
             fWork->display (hWnd, hdc);
             return TRUE;
        }
        return DLG::OnPaint (hWnd,hdc, msg, wParam,lParam);
}


BOOL WeDlg::OnActivate (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (LOWORD (wParam) == WA_ACTIVE ||
                LOWORD (wParam) == WA_CLICKACTIVE)
        {
              if (WithTabStops)
              {
                  SetTabFocus (); 
              }
              else
              {
                  fWork->SetFocus ();
              }
              return TRUE;
        }
        return FALSE;
}

BOOL WeDlg::ChangeDockList (void)
{
        if (DockList)
        {
            DockList = FALSE;
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_DOCKLIST,   MF_UNCHECKED);
        }
        else
        {
            DockList = TRUE;
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_DOCKLIST,   MF_CHECKED);
        }
        return TRUE;
}

BOOL WeDlg::ChangeMultiList (void)
{
        if (ListType == 1)
        {
            ListType = 0;
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_MULTILIST,   MF_UNCHECKED);
        }
        else
        {
            ListType = 1;
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_MULTILIST,   MF_CHECKED);
        }
        return TRUE;
}


BOOL WeDlg::ChangeSearchBest (void)
{
        if (WithSearchBest)
        {
            WithSearchBest = FALSE;
            if (ListDlg != NULL)
            {
                     ListDlg->SetWithSearchBest (WithSearchBest);
            }
            if (weDlgLst != NULL)
            {
                     weDlgLst->SetWithSearchBest (WithSearchBest);
            }
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_SEARCHBEST,   MF_UNCHECKED);
        }
        else
        {
            WithSearchBest = TRUE;
            if (ListDlg != NULL)
            {
                     ListDlg->SetWithSearchBest (WithSearchBest);
            }
            if (weDlgLst != NULL)
            {
                     weDlgLst->SetWithSearchBest (WithSearchBest);
            }
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_SEARCHBEST,   MF_CHECKED);
        }
        return TRUE;
}

BOOL WeDlg::ChangeLiefBest (void)
{
        if (WithLiefBest)
        {
            WithLiefBest = FALSE;
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_LIEFBEST,   MF_UNCHECKED);
  //          EnableMenuItem (ActiveDlg->GethMenu (), IDM_SEARCHBEST,  MF_ENABLED);
            strcpy (wekopff.a_best_kz, "1");
            fPos.GetCfield ("we_erf_kz")->SetText ();
  	        weWork.GetPtab (wekopff.a_best_kz_bz, "we_erf_kz",  
			  			    wekopff.a_best_kz);
            fPos.GetCfield ("we_erf_kz_bz")->SetText ();
            strcpy (wekopff.a_best_info, ptabn.ptbez);
            fHead.GetCfield ("we_erf_info")->Invalidate (TRUE);
            fHead.GetCfield ("we_erf_info")->SetText ();
        }
        else
        {
            WithAkrz = FALSE;
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_AKRZ,   MF_UNCHECKED);
            WithLiefBest = TRUE;
            CheckMenuItem (ActiveDlg->GethMenu (),  IDM_LIEFBEST,   MF_CHECKED);
            WithSearchBest = TRUE;
//            CheckMenuItem (ActiveDlg->GethMenu (), IDM_SEARCHBEST,   MF_CHECKED);
//            EnableMenuItem (ActiveDlg->GethMenu (), IDM_SEARCHBEST,  MF_GRAYED);
            strcpy (wekopff.a_best_kz, "2");
            fPos.GetCfield ("we_erf_kz")->SetText ();
  	        weWork.GetPtab (wekopff.a_best_kz_bz, "we_erf_kz",  
			  			    wekopff.a_best_kz);
            fPos.GetCfield ("we_erf_kz_bz")->SetText ();
            strcpy (wekopff.a_best_info, ptabn.ptbez);
            fHead.GetCfield ("we_erf_info")->Invalidate (TRUE);
            fHead.GetCfield ("we_erf_info")->SetText ();
        }
        return TRUE;
}


BOOL WeDlg::ChangeEk (void)
{
        if (WithEk)
        {
            WithEk = FALSE;
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_EK,   MF_UNCHECKED);
            strcpy (wekopff.pr_erf_kz, "1");
            fPos.GetCfield ("we_pr_kz")->SetText ();
  	        weWork.GetPtab (wekopff.pr_erf_kz_bz, "we_erf_kz",  
			  			    wekopff.pr_erf_kz);
            fPos.GetCfield ("we_pr_kz_bz")->SetText ();
        }
        else
        {
            WithPosEk = FALSE;
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_POSEK,   MF_UNCHECKED);
            WithEk = TRUE;
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_EK,   MF_CHECKED);
            strcpy (wekopff.pr_erf_kz, "2");
            fPos.GetCfield ("we_pr_kz")->SetText ();
  	        weWork.GetPtab (wekopff.pr_erf_kz_bz, "we_pr_kz",  
			  			    wekopff.pr_erf_kz);
            fPos.GetCfield ("we_pr_kz_bz")->SetText ();
        }
        return TRUE;
}


BOOL WeDlg::ChangePosEk (void)
{
        if (WithPosEk)
        {
            WithPosEk = FALSE;
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_POSEK,   MF_UNCHECKED);
            strcpy (wekopff.pr_erf_kz, "1");
            fPos.GetCfield ("we_pr_kz")->SetText ();
  	        weWork.GetPtab (wekopff.pr_erf_kz_bz, "we_pr_kz",  
			  			    wekopff.pr_erf_kz);
            fPos.GetCfield ("we_pr_kz_bz")->SetText ();
        }
        else
        {
            WithEk = FALSE;
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_EK,   MF_UNCHECKED);
            WithPosEk = TRUE;
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_POSEK,   MF_CHECKED);
            strcpy (wekopff.pr_erf_kz, "3");
            fPos.GetCfield ("we_pr_kz")->SetText ();
  	        weWork.GetPtab (wekopff.pr_erf_kz_bz, "we_pr_kz",  
			  			    wekopff.pr_erf_kz);
            fPos.GetCfield ("we_pr_kz_bz")->SetText ();
        }
        return TRUE;
}

BOOL WeDlg::ChangeAkrz (void)
{
        if (WithAkrz)
        {
            WithAkrz = FALSE;
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_AKRZ,   MF_UNCHECKED);
            strcpy (wekopff.a_best_kz, "1");
            fPos.GetCfield ("we_erf_kz")->SetText ();
  	        weWork.GetPtab (wekopff.a_best_kz_bz, "we_erf_kz",  
			  			    wekopff.a_best_kz);
            fPos.GetCfield ("we_erf_kz_bz")->SetText ();
            strcpy (wekopff.a_best_info, ptabn.ptbez);
            fHead.GetCfield ("we_erf_info")->Invalidate (TRUE);
            fHead.GetCfield ("we_erf_info")->SetText ();
        }
        else
        {
            WithLiefBest = FALSE;
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_LIEFBEST,   MF_UNCHECKED);
            WithAkrz = TRUE;
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_AKRZ,   MF_CHECKED);
            strcpy (wekopff.a_best_kz, "3");
            fPos.GetCfield ("we_erf_kz")->SetText ();
  	        weWork.GetPtab (wekopff.a_best_kz_bz, "we_erf_kz",  
			  			    wekopff.a_best_kz);
            fPos.GetCfield ("we_erf_kz_bz")->SetText ();
            strcpy (wekopff.a_best_info, ptabn.ptbez);
            fHead.GetCfield ("we_erf_info")->Invalidate (TRUE);
            fHead.GetCfield ("we_erf_info")->SetText ();
        }
        return TRUE;
}


BOOL WeDlg::EnterQmPos (HWND ListhWnd)
{
       if (ListDlg == NULL &&
		   weDlgLst == NULL) return FALSE;       

       weWork.FillQsPos ();
//       weWork.InitQm (weWork.GetYesPos (), weWork.GetNoPos (), weWork.Getqsanzpos ());
       weWork.QmDefaultYes (weWork.GetYesPos (), weWork.GetNoPos (), weWork.Getqsanzpos ());
       while (TRUE)
       {
                if (DockList == FALSE)
                {
                       QmKopf->SethMainWnd (ListhWnd);
                       EnableWindows (ListhWnd, FALSE);
                }
                else
                {
                       QmKopf->SethMainWnd (hWnd);
                }
                QmKopf->SetParams (weWork.GetUbPos (),  weWork.GetTextePos (), 
                                   weWork.GetYesPos (), weWork.GetNoPos (), WS_POPUP);
                QmKopf->SetTextBkColor (QmTextBkColor);
                EnableWindows (hMainWindow, FALSE);
                QmKopf->Enter ();
                EnableWindows (hMainWindow, TRUE);
                if (DockList == FALSE)
                {
                       EnableWindows (ListhWnd, TRUE);
                       SetActiveWindow (ListhWnd);
                       SetWindowPos (ListhWnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
                }
                else
                {
                       SetActiveWindow (hMainWindow);
                       SetWindowPos (hMainWindow, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
                }
                if (syskey == KEY12)
                {
				        if (weWork.TestQm (weWork.GetYesPos (), 
                                           weWork.GetNoPos (), 
                                           weWork.Getqsanzpos ()) == FALSE)
                        {
						         disp_mess ("Bitte alle Punkte bearbeiten", 2); 
						         continue;
                        }
		                weWork.WriteQsPos ();
                }
 			    break;
       }
       return TRUE;
}


BOOL WeDlg::EnterQmKopf (void)
{
       if (ListDlg != NULL) return FALSE;       

       weWork.FillQsHead ();
//       weWork.InitQm (weWork.GetYes (), weWork.GetNo (), weWork.Getqsanz ());
       weWork.QmDefaultYes (weWork.GetYes (), weWork.GetNo (), weWork.Getqsanz ());
       while (TRUE)
       {
                QmKopf->SethMainWnd (hWnd);
                QmKopf->SetParams (weWork.GetUb (),  weWork.GetTexte (), 
                                   weWork.GetYes (), weWork.GetNo (), WS_POPUP);
                QmKopf->SetTextBkColor (QmTextBkColor);
                EnableWindows (hWnd, FALSE);
                QmKopf->Enter ();
                EnableWindows (hWnd, TRUE);
                SetActiveWindow (hMainWindow);
                if (syskey == KEY12)
                {
				        if (weWork.TestQm (weWork.GetYes (), 
                                           weWork.GetNo (), 
                                           weWork.Getqsanz ()) == FALSE)
                        {
						         disp_mess ("Bitte alle Punkte bearbeiten", 2); 
						         continue;
                        }
		                weWork.WriteQs ();
                }
 			    break;
       }
       return TRUE;
}


BOOL WeDlg::ChangeZ2 (void)
{
        if (TwoRows)
        {
            TwoRows = FALSE;
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_Z2,   MF_UNCHECKED);
        }
        else
        {
            TwoRows = TRUE;
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_Z2,   MF_CHECKED);
        }
        return TRUE;
}

BOOL WeDlg::OnCloseUp (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
	    if ((HWND) lParam == hwndCombo1)
		{
			ChoiseCombo1 ();
			return TRUE;
		}
		else if ((HWND) lParam == hwndCombo2)
		{
			ChoiseCombo2 ();
			return TRUE;
		}
		return FALSE;
}

BOOL WeDlg::ChoisePrinter (void)
{
       char ClassName [81];

       if (GetClassName (hMainWindow, ClassName, 80) == 0)
       {
           return FALSE;
       }

       SethStdWindow (ClassName);
       PrChoise = TRUE;
	   ::ChoisePrinter0 ();
       PrChoise = FALSE;
       return TRUE;
}
        

void WeDlg::PrintOne (void)
{
        WriteWeKopf ();
        if (we_kopf.we_status > 2)
        {
            ChoisePrintOne ();
			return;
        }

        PrintBeleg ();
}

void WeDlg::PrintMulti (void)
{
            ChoiseDlg *choiseDlg = NULL;
			char buffer [512];


			InPrint = TRUE;
			strcpy (Beleg, "N");
			strcpy (Journal, "N");
			fPrintChoise.SetCfield (_fPrintChoise);
			fPrintChoise.SetFieldanz ();
			fPrintChoise.destroy ();
            choiseDlg = new ChoiseDlg (-1, -1, 30, 8, NULL, StdSize, FALSE);
			choiseDlg->SetParent (this);
			choiseDlg->SetNextTopWindow (hWnd);
			choiseDlg->SetDialog (&fPrintChoise);
//			choiseDlg->Pack (2, 4);
            choiseDlg->SetWinBackground (SysBkColor);
            choiseDlg->SetStyle (WS_POPUP | WS_DLGFRAME | WS_CAPTION | WS_SYSMENU);
            choiseDlg->OpenScrollWindow (hInstance, hWnd);
            EnableWindows (hWnd, FALSE);
            choiseDlg->ProcessMessages ();
//            EnableWindows (hWnd, TRUE);
			InPrint = FALSE;
            delete choiseDlg;
            SetCurrentFocus ();
			if (syskey == KEY12)
			{
			     if (strcmp (Beleg, "J") == 0)
				 {
					   sprintf (buffer, "70001 %s", Format);
                       ProcExec (buffer, SW_SHOWNORMAL, -1, 0, -1, 0);  
				 }
			     if (strcmp (Journal, "J") == 0)
				 {
					   sprintf (buffer, "70001 %s", JoFormat);
                       ProcExec (buffer, SW_SHOWNORMAL, -1, 0, -1, 0);  
				 }
			}
            SetCurrentFocus ();

}

void WeDlg::ChoisePrintOne (void)
{
            ChoiseDlg *choiseDlg = NULL;


			InPrint = TRUE;
			strcpy (Beleg, "N");
			strcpy (Journal, "N");
			fPrintChoise.SetCfield (_fPrintChoise);
			fPrintChoise.SetFieldanz ();
			fPrintChoise.destroy ();
            choiseDlg = new ChoiseDlg (-1, -1, 30, 8, NULL, StdSize, FALSE);
			choiseDlg->SetParent (this);
			choiseDlg->SetNextTopWindow (hWnd);
			choiseDlg->SetDialog (&fPrintChoise);
//			choiseDlg->Pack (2, 4);
            choiseDlg->SetWinBackground (SysBkColor);
            choiseDlg->SetStyle (WS_POPUP | WS_DLGFRAME | WS_CAPTION | WS_SYSMENU);
            choiseDlg->OpenScrollWindow (hInstance, hWnd);
            EnableWindows (hWnd, FALSE);
            choiseDlg->ProcessMessages ();
//            EnableWindows (hWnd, TRUE);
			InPrint = FALSE;
            delete choiseDlg;
            SetCurrentFocus ();
			if (syskey == KEY12)
			{
			     if (strcmp (Beleg, "J") == 0)
				 {
				       PrintBeleg ();
				 }
			     if (strcmp (Journal, "J") == 0)
				 {
				       PrintJournal ();
				 }
			}
            SetCurrentFocus ();
}

void WeDlg::PrintBeleg (void)
{
        char valfrom [20];
        char valto [20];

  	    SetAusgabe (PRINTER_OUT);

        if (LeseFormat (Format) == FALSE)
		{
		           return;
		}

        sprintf (valfrom, "%hd", we_kopf.mdn);
        sprintf (valto, "%hd", we_kopf.mdn);
        FillFormFeld ("we_kopf.mdn", valfrom, valto);

        sprintf (valfrom, "%hd", we_kopf.fil);
        sprintf (valto, "%hd", we_kopf.fil);
        FillFormFeld ("we_kopf.fil", valfrom, valto);

        sprintf (valfrom, "%s", we_kopf.lief);
        sprintf (valto, "%s", we_kopf.lief);
        FillFormFeld ("we_kopf.lief", valfrom, valto);

        sprintf (valfrom, "%s", we_kopf.lief_rech_nr);
        sprintf (valto, "%s", we_kopf.lief_rech_nr);
        FillFormFeld ("we_kopf.lief_rech_nr", valfrom, valto);

        sprintf (valfrom, "%s", we_kopf.blg_typ);
        sprintf (valto, "%s", we_kopf.blg_typ);
        FillFormFeld ("we_kopf.blg_typ", valfrom, valto);

        sprintf (valfrom, "%ld", we_kopf.lfd);
        sprintf (valto, "%ld", we_kopf.lfd);
        FillFormFeld ("we_kopf.lfd", valfrom, valto);

        dlong_to_asc (we_kopf.we_dat, valfrom);
        dlong_to_asc (we_kopf.we_dat, valto);
        FillFormFeld ("we_kopf.we_dat", valfrom, valto);

        StartPrint ();

}
        
void WeDlg::PrintJournal (void)
{
	     disp_mess ("Drucken von Journal mu� noch erstellt werden", 2);
}

void WeDlg::Print (void)
{
        if (fWork->GetCfield ("lief")->IsDisabled ())
        {
            PrintOne ();
        }
		else
		{
			PrintMulti ();
		}
}


BOOL WeDlg::OnCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
         
        if (ListDlg != NULL)
        {
              if (ListDlg->OnCommand (hWnd,msg,wParam,lParam))
              {
                  return TRUE;
              }
        }
        if (fWork)
        {
			  if (InPrint && LOWORD (wParam) == CANCEL_CTL)
			  {
                  PostMessage (NULL, WM_KEYDOWN, VK_F5, 0l);
                  return TRUE;
			  }

			  if (InPrint && LOWORD (wParam) == OK_CTL)
			  {
                  PostMessage (NULL, WM_KEYDOWN, VK_F12, 0l);
                  return TRUE;
			  }

              if (LOWORD (wParam) == CANCEL_CTL)
              {
                  return OnKey5 ();
              }
              if (LOWORD (wParam) == OK_CTL)
              {
                  return OnKey12 ();
              }

              if (PrChoise && LOWORD (wParam) == KEY12)
              {
                  PostMessage (NULL, WM_KEYDOWN, VK_F12, 0l);
                  return TRUE;
              }

              if (PrChoise && LOWORD (wParam) == KEY5)
              {
                  PostMessage (NULL, WM_KEYDOWN, VK_F5, 0l);
                  return TRUE;
              }
              if (LOWORD (wParam) == IDM_LIST)
              {
                  return OnKey6 ();
              }
              if (LOWORD (wParam) == IDM_CHOISE)
              {
                  return OnKey9 ();
              }
              if (LOWORD (wParam) == IDM_CHOISEPRINTER)
              {
                  return ChoisePrinter ();
              }
              if (LOWORD (wParam) == IDM_DOCKLIST)
              {
                  return ChangeDockList ();
              }

              if (LOWORD (wParam) == IDM_MULTILIST)
              {
                  return ChangeMultiList ();
              }

              if (LOWORD (wParam) == IDM_LIEFBEST)
              {
                  return ChangeLiefBest ();
              }

              if (LOWORD (wParam) == IDM_SEARCHBEST)
              {
                  return ChangeSearchBest ();
              }

              if (LOWORD (wParam) == IDM_EK)
              {
                  return ChangeEk ();
              }
              if (LOWORD (wParam) == IDM_POSEK)
              {
                  return ChangePosEk ();
              }

              if (LOWORD (wParam) == IDM_AKRZ)
              {
                  return ChangeAkrz ();
              }

              if (LOWORD (wParam) == IDM_Z2)
              {
                  return ChangeZ2 ();
              }

              if (LOWORD (wParam) == IDM_QM)
              {
                  return EnterQmKopf ();
              }

              if (LOWORD (wParam) == IDM_BEST)
              {
                  return ShowBest ();
              }

              if (LOWORD (wParam) == IDM_SHOWKONTRAKT)
              {
                  return ShowKontrakt ();
              }

              if (LOWORD (wParam) == IDM_TXTDLG)
              {
                  return EnterTextDlg ();
              }

              if (LOWORD (wParam) == IDM_ABSCHLUSS)
              {
                  return EnterAbschlussDlg ();
              }

 		      if (HIWORD (wParam)  == CBN_CLOSEUP)
			  {
				  return OnCloseUp (hWnd, msg, wParam, lParam);
			  }

              else if (HIWORD (wParam) == WM_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == EN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == CBN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == WM_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == LBN_SELCHANGE)
              {
              }
              else if (HIWORD (wParam) == EN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == CBN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == BN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == BN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
        }
        return DLG::OnCommand (hWnd, msg, wParam, lParam);
}


BOOL WeDlg::OnSysCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        
        if (wParam == SC_CLOSE)
        {
            if (abfragejn (hWnd, "Verarbeitung abbrechen ?", "N") == 0)
            {
                ActiveDlg->SetCurrentFocus ();
                return TRUE;
            }
            ExitProcess (0);
            syskey = KEY5;
        }
        return DLG::OnSysCommand (hWnd, msg, wParam, lParam); 
}

BOOL WeDlg::OnDestroy (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{

        if (hWnd == this->hWnd)
        {
             PostQuitMessage (0);
             fWeKopf.destroy ();
        }
        return TRUE;
}

void WeDlg::SetWinBackground (COLORREF Col)
{
          dlgfont.FontBkColor = Col;
          cpfont.FontBkColor = Col;
          DLG::SetWinBackground (Col);
}

void WeDlg::SetQmTextBkColor (COLORREF Col)
{
          QmTextBkColor = Col; 
}

COLORREF *WeDlg::GetQmTextBkPtr (void)
{
          return &QmTextBkColor; 
}

void WeDlg::ToClipboard (void)
{
	      DLG::ToClipboard ();
/*
          CFIELD *Cfield;
          HGLOBAL hb;
          LPVOID p;
          char *text;

          Cfield = GetCurrentCfield ();
          Cfield->GetText ();
          text = (char *) Cfield->GetFeld ();
          if (text == NULL) return;
          hb = GlobalAlloc (GHND, strlen (text) + 1);
          if (hb == NULL) return;
          p = GlobalLock (hb);
          strcpy ((char *) p, text);
          GlobalUnlock (hb);
          OpenClipboard (hWnd);
          EmptyClipboard ();
          SetClipboardData (CF_TEXT, hb);
          CloseClipboard ();
*/
}

void WeDlg::FromClipboard (void)
{

	      DLG::FromClipboard ();
/*
          CFIELD *Cfield;
          HGLOBAL hb;
          LPVOID p;
          char *text;

          Cfield = GetCurrentCfield ();
          text = (char *) Cfield->GetFeld ();
          if (text == NULL) return;
          if (IsClipboardFormatAvailable (CF_TEXT) == FALSE) return;
          OpenClipboard (hWnd);
          hb = GetClipboardData (CF_TEXT);
          if (hb == NULL)
          {
                 CloseClipboard ();
                 return;
          }
          p = GlobalLock (hb);
          if (strlen ((char *) p) > (unsigned int ) Cfield->GetLength ())
          {
                 GlobalUnlock (hb);
                 CloseClipboard ();
                 return;
          }
          strcpy (text, (char *) p);
          GlobalUnlock (hb);
          CloseClipboard ();
          Cfield->SetText ();
*/
}

void WeDlg::Help (void)
{
          CFIELD *Cfield;
          HELP *Help; 
          char *Item;

          Cfield = GetCurrentCfield ();
          if (Cfield == NULL) return;

          Item = Cfield->GetName ();

          Help = new HELP (HelpName, Item, hInstance,hMainWindow);
          delete Help;
          Cfield->SetFocus ();
}



HWND WeDlg::OpenWindow (HINSTANCE hInstance, HWND hMainWindow)
{
          HWND hWnd;

          Settchar ('|');
          hWnd = DLG::OpenWindow (hInstance, hMainWindow);
          return hWnd;
}

HWND WeDlg::OpenScrollWindow (HINSTANCE hInstance, HWND hMainWindow)
{
          HWND hWnd;

          Settchar ('|');
          hWnd = DLG::OpenScrollWindow (hInstance, hMainWindow);
	      QmKopf = new QMKOPF (hInstance, hWnd);
//		  QmKopf->SetDevice (0.75, 0.75);
          return hWnd;
}

void WeDlg::PrintComment (char *Name)
{
          HELP *Help; 
          char Comment [256];

          Help = new HELP (HelpName, Name, hInstance,hMainWindow, INITCOMM);
          Help->GetItem (Name, Comment);
          PrintMess (Comment);
          delete Help;
}

void WeDlg::CallInfo (void)
{
          DLG::CallInfo ();
}

void WeDlg::SetListLines (int ListLines)
{
          this->ListLines = ListLines;
          if (ListClass::ActiveList != NULL)
          {
		      ListClass::ActiveList->SetListLines (ListLines);
          }
          LineSet         = TRUE;
}

void WeDlg::SetListColors (COLORREF ListColor, COLORREF ListBkColor)
{
          this->ListColor   = ListColor;
          this->ListBkColor = ListBkColor;
          if (ListClass::ActiveList != NULL)
          {
		      ListClass::ActiveList->SetColors (ListColor,ListBkColor);
          }
          ColorSet          = TRUE;

}


HWND WeDlg::SetToolCombo (int Id, int tbn1, int tbn2, int ComboPtr)
{
         HWND hwndCombo; 

		 hwndCombo = DLG::SetToolCombo (Id, tbn1, tbn2);

		 switch (ComboPtr)
		 {
		     case 0:
                   hwndCombo1 = hwndCombo;
				   break;
			 case 1:
                   hwndCombo2 = hwndCombo;
				   break;
		 }
         return hwndCombo;
}

void WeDlg::SetComboTxt (HWND hwndCombo, char **Combo, int Select, int ComboPtr)
{

	  DLG::SetComboTxt (hwndCombo, Combo, Select);

 	  switch (ComboPtr)
	  {
		     case 0:
                   Combo1 = Combo;
				   break;
			 case 1:
                   Combo2 = Combo;
				   break;
	  }
}


void WeDlg::ChoiseCombo1 (void)
/**
Aktuell gewaehlten eintrag in Combobox 1 ermitteln.
**/
{
          int i;
          char text [80];

          GetWindowText (hwndCombo1,
                         text,
                         79);
          clipped (text);
          for (i = 0; Combo1[i]; i ++)
          {
              if (strcmp (Combo1[i],text) == 0)
              {
                   SetListLines (i);
                   break;
              }
          }
          if (ActiveDlg != NULL && 
              ActiveDlg->GetCurrentCfield ()->IsDisabled == FALSE)
          {
              ActiveDlg->SetCurrentFocus ();
          }
}

void WeDlg::ChoiseCombo2 (void)
/**
Aktuell gewaehlten eintrag in Combobox 1 ermitteln.
**/
{
          int i;
          char text [80];

          GetWindowText (hwndCombo2,
                         text,
                         79);
          clipped (text);
          for (i = 0; Combo2[i]; i ++)
          {
              if (strcmp (Combo2[i],text) == 0)
              {
                   SetListColors (Colors[i], BkColors[i]);
                   break;
              }
          }
          if (ActiveDlg != NULL && 
              ActiveDlg->GetCurrentCfield ()->IsDisabled == FALSE)
          {
              ActiveDlg->SetCurrentFocus ();
          }
}


int WeDlg::TestWeDat (void)
{
       char sysdatum [12];
	   long lsysdatum;
	   long ltestdat;
	   static BOOL InTest = FALSE;
	   char text [256];

	   if (InTest) return 0;

       if (ActiveDlg == NULL) return 0;
       if (syskey == KEY5) return 0;

       HWND hWnd;
       hWnd = GetFocus ();

	   DatError = FALSE;
	   InTest = TRUE;
	   sysdate (sysdatum);
	   lsysdatum = dasc_to_long (sysdatum);
       fWeKopf.GetCfield ("we_dat")->GetText (); 
       ltestdat = dasc_to_long (wekopff.we_dat);

	   if ((ltestdat - lsysdatum) > we_dat_plus)
	   {
		    sprintf(text,"Das Wareneingangsdatum liegt %d Tage in der Zukunft. Ist das OK ?",ltestdat - lsysdatum);
		    if (abfragejn (hWnd, text, "N") == 0)
			{
				   DatError = TRUE;
			}
//		   disp_mess ("Das Wareneingangsdatum ist zu gro�", 2);
	   }
	   else if ((lsysdatum - ltestdat) > we_dat_minus)
	   {
		    sprintf(text,"Das Wareneingangsdatum liegt schon  %d Tage zur�ck . Ist das OK ?",lsysdatum - ltestdat);
		    if (abfragejn (hWnd, text, "N") == 0)
			{
				   DatError = TRUE;
			}
//		   disp_mess ("Das Wareneingangsdatum ist zu klein", 2);
	   }

	   if (DatError)
	   {
             fPos.SetCurrentName ("we_dat");
	 	      InTest = FALSE;
              return 0;
	   }
       InTest = FALSE;
       return 1;
}


int WeDlg::TestBlgEingDat (void)
{
       char sysdatum [12];
	   long lsysdatum;
	   long ltestdat;
	   static BOOL InTest = FALSE;
	   char text [256];

	   if (InTest) return 0;

       if (ActiveDlg == NULL) return 0;
       if (syskey == KEY5) return 0;

       HWND hWnd;
       hWnd = GetFocus ();

	   DatError = FALSE;
	   InTest = TRUE;
	   sysdate (sysdatum);
	   lsysdatum = dasc_to_long (sysdatum);
       fWeKopf.GetCfield ("blg_eing_dat")->GetText (); 
       ltestdat = dasc_to_long (wekopff.blg_eing_dat);

	   if ((ltestdat - lsysdatum) > blg_eing_dat_plus)
	   {
		    sprintf(text,"Das Belegdatum liegt %d Tage in der Zukunft. Ist das OK ?",ltestdat - lsysdatum);
		    if (abfragejn (hWnd, text, "N") == 0)
			{
				   DatError = TRUE;
			}
	   }
	   else if ((lsysdatum - ltestdat) > blg_eing_dat_minus)
	   {
		    sprintf(text,"Das Belegdatum liegt schon  %d Tage zur�ck . Ist das OK ?",lsysdatum - ltestdat);
		    if (abfragejn (hWnd, text, "N") == 0)
			{
				   DatError = TRUE;
			}
	   }

	   if (DatError)
	   {
		      syskey = KEY5;
              fPos.SetCurrentName ("blg_eing_dat");
	 	      InTest = FALSE;
              return 0;
	   }
	   InTest = FALSE;
       return 1;
}
