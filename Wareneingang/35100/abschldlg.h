#ifndef _ABSCHLDLG_DEF
#define _ABSCHLDLG_DEF
#include "dlg.h"
#include "itprog.h"
#include "itfont.h"
#include "formfield.h"
#include "cmask.h"
#include "abschlwork.h" 

#define COMBOBOX_CTL 1001 
#define COMBOCLOSED 1002 

#define ZUSCH_EU_CTL 2001
#define LIEF_ZUSCH_EU_CTL 2002
#define INK_ZUSCH_EU_CTL 2003
#define MWST_SL1_CTL 2004
#define MWST_SL2_CTL 2005
#define MWST_SL3_CTL 2006
#define PRINT_CTL 2007
          
#define NOBORDER 0
#define EDITBORDER 1
#define EDITLOW 2
#define EDITHI 3

class AbschlDlg : virtual public DLG 
{
          private :
            static  mfont dlgfont;
            static  mfont dlgposfont;
            static mfont *Font;

            static struct ABSCHL Abschl;

            static int ColPos;
            static int ColPos2;
            static char combotext[];
            static CFIELD *_fDlg0[];
            static CFORM fDlg0; 
            static CFIELD *_fFoot[];
            static CFORM fFoot; 
            static CFIELD *_fDlg1[];
            static CFORM fDlg1; 
            static CFIELD *_fDlg[];
            static CFORM fDlg;
            static char *EnablePos[];
            static FORMFIELD *dbfields [];
            static char **ComboMwst1;
            static char **ComboMwst2;
            static char **ComboMwst3;

            char AbschlKz[2];
            AbschlWork Work;
          public :

	        static COLORREF SysBkColor;
            static int EditStyle;

            void SetAbschlKz (char *Kz)
            {
                   strcpy (this->AbschlKz, Kz);
            }

            char *GetAbschlKz (void)
            {
                return AbschlKz;
            }

            AbschlDlg (int, int, int, int, char *, int, BOOL);
            ~AbschlDlg ();
 	        void Init (int, int, int, int, char *, int, BOOL);
            void WriteAbschl (void);
            void RechneEkVkSpanne (void);
            void RechneEkVkAbSpanne (void);
            void RechneSummen (void);
            void OrEditAttr (DWORD);
            void NotEditAttr (DWORD);
            void OrEditAttrEx (DWORD);
            BOOL TestCombobox (void);
            BOOL TestEditField (void);
            BOOL OnKey5 (void);
            BOOL OnKey12 (void);
            BOOL OnKeyReturn (void);
            BOOL OnKeyDown (void);
            BOOL OnKeyUp (void);
            BOOL OnCommand (HWND,UINT,WPARAM,LPARAM);
            BOOL OnSysCommand (HWND,UINT,WPARAM,LPARAM);

            void SetWinBackground (COLORREF);
            void ToClipboard (void);
            void FromClipboard (void);
};
#endif
