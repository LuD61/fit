#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "zerldaten.h"

struct ZERLDATEN zerldaten, zerldaten_null;

void ZERLDATEN_CLASS::prepare (void)
{
            char *sqltext;

/*
            ins_quest ((char *) &zerldaten.mdn, 1, 0);
            ins_quest ((char *) &zerldaten.fil, 1, 0);     
            ins_quest ((char *) &zerldaten.a, 3, 0);
            ins_quest ((char *) zerldaten.ident_intern, 0, sizeof (zerldaten.ident_intern));
*/
            ins_quest ((char *) &zerldaten.lfd, 2, 0);
    out_quest ((char *) &zerldaten.mdn,1,0);
    out_quest ((char *) &zerldaten.fil,1,0);
    out_quest ((char *) &zerldaten.partie,2,0);
    out_quest ((char *) zerldaten.lief,0,17);
    out_quest ((char *) zerldaten.lief_rech_nr,0,17);
    out_quest ((char *) zerldaten.ident_extern,0,21);
    out_quest ((char *) zerldaten.ident_intern,0,21);
    out_quest ((char *) &zerldaten.a,3,0);
    out_quest ((char *) &zerldaten.a_gt,3,0);
    out_quest ((char *) zerldaten.kategorie,0,4);
    out_quest ((char *) &zerldaten.schnitt,1,0);
    out_quest ((char *) &zerldaten.gebland,1,0);
    out_quest ((char *) &zerldaten.mastland,1,0);
    out_quest ((char *) &zerldaten.schlaland,1,0);
    out_quest ((char *) &zerldaten.zerland,1,0);
    out_quest ((char *) zerldaten.esnum,0,11);
    out_quest ((char *) zerldaten.eznum1,0,11);
    out_quest ((char *) zerldaten.eznum2,0,11);
    out_quest ((char *) zerldaten.eznum3,0,11);
    out_quest ((char *) &zerldaten.dat,2,0);
    out_quest ((char *) &zerldaten.anz_gedruckt,1,0);
    out_quest ((char *) &zerldaten.anz_entnommen,1,0);
    out_quest ((char *) &zerldaten.lfd,2,0);
    out_quest ((char *) &zerldaten.lfd_i,2,0);
    out_quest ((char *) &zerldaten.aktiv,1,0);
    out_quest ((char *) &zerldaten.zerlegt,1,0);
            cursor = prepare_sql ("select zerldaten.mdn,  "
"zerldaten.fil,  zerldaten.partie,  zerldaten.lief,  "
"zerldaten.lief_rech_nr,  zerldaten.ident_extern,  "
"zerldaten.ident_intern,  zerldaten.a,  zerldaten.a_gt,  "
"zerldaten.kategorie,  zerldaten.schnitt,  zerldaten.gebland,  "
"zerldaten.mastland,  zerldaten.schlaland,  zerldaten.zerland,  "
"zerldaten.esnum,  zerldaten.eznum1,  zerldaten.eznum2,  "
"zerldaten.eznum3,  zerldaten.dat,  zerldaten.anz_gedruckt,  "
"zerldaten.anz_entnommen,  zerldaten.lfd,  zerldaten.lfd_i,  "
"zerldaten.aktiv,  zerldaten.zerlegt from zerldaten "

#line 31 "zerldaten.rpp"
                                  "where lfd = ?");
/*
                                  "where mdn = ? "
                                  "and   fil   = ? "
                                  "and   a   = ? "
                                  "and   ident_intern = ?");
*/

    ins_quest ((char *) &zerldaten.mdn,1,0);
    ins_quest ((char *) &zerldaten.fil,1,0);
    ins_quest ((char *) &zerldaten.partie,2,0);
    ins_quest ((char *) zerldaten.lief,0,17);
    ins_quest ((char *) zerldaten.lief_rech_nr,0,17);
    ins_quest ((char *) zerldaten.ident_extern,0,21);
    ins_quest ((char *) zerldaten.ident_intern,0,21);
    ins_quest ((char *) &zerldaten.a,3,0);
    ins_quest ((char *) &zerldaten.a_gt,3,0);
    ins_quest ((char *) zerldaten.kategorie,0,4);
    ins_quest ((char *) &zerldaten.schnitt,1,0);
    ins_quest ((char *) &zerldaten.gebland,1,0);
    ins_quest ((char *) &zerldaten.mastland,1,0);
    ins_quest ((char *) &zerldaten.schlaland,1,0);
    ins_quest ((char *) &zerldaten.zerland,1,0);
    ins_quest ((char *) zerldaten.esnum,0,11);
    ins_quest ((char *) zerldaten.eznum1,0,11);
    ins_quest ((char *) zerldaten.eznum2,0,11);
    ins_quest ((char *) zerldaten.eznum3,0,11);
    ins_quest ((char *) &zerldaten.dat,2,0);
    ins_quest ((char *) &zerldaten.anz_gedruckt,1,0);
    ins_quest ((char *) &zerldaten.anz_entnommen,1,0);
    ins_quest ((char *) &zerldaten.lfd_i,2,0);
    ins_quest ((char *) &zerldaten.aktiv,1,0);
    ins_quest ((char *) &zerldaten.zerlegt,1,0);
            sqltext = "update zerldaten set "
"zerldaten.mdn = ?,  zerldaten.fil = ?,  zerldaten.partie = ?,  "
"zerldaten.lief = ?,  zerldaten.lief_rech_nr = ?,  "
"zerldaten.ident_extern = ?,  zerldaten.ident_intern = ?,  "
"zerldaten.a = ?,  zerldaten.a_gt = ?,  zerldaten.kategorie = ?,  "
"zerldaten.schnitt = ?,  zerldaten.gebland = ?,  "
"zerldaten.mastland = ?,  zerldaten.schlaland = ?,  "
"zerldaten.zerland = ?,  zerldaten.esnum = ?,  zerldaten.eznum1 = ?,  "
"zerldaten.eznum2 = ?,  zerldaten.eznum3 = ?,  zerldaten.dat = ?,  "
"zerldaten.anz_gedruckt = ?,  zerldaten.anz_entnommen = ?,  "
"zerldaten.lfd_i = ?,  zerldaten.aktiv = ?,  "
"zerldaten.zerlegt = ? "

#line 40 "zerldaten.rpp"
                                  "where lfd = ?";
/*
                                  "where mdn = ? "
                                  "and   fil   = ? "
                                  "and   a   = ? "
                                  "and   ident_intern = ?";
*/
  
/*
            ins_quest ((char *) &zerldaten.mdn, 1, 0);
            ins_quest ((char *) &zerldaten.fil, 1, 0);     
            ins_quest ((char *) &zerldaten.a, 3, 0);
            ins_quest ((char *) zerldaten.ident_intern, 0, sizeof (zerldaten.ident_intern));
*/
            ins_quest ((char *) &zerldaten.lfd, 2, 0);

            upd_cursor = prepare_sql (sqltext);

    ins_quest ((char *) &zerldaten.mdn,1,0);
    ins_quest ((char *) &zerldaten.fil,1,0);
    ins_quest ((char *) &zerldaten.partie,2,0);
    ins_quest ((char *) zerldaten.lief,0,17);
    ins_quest ((char *) zerldaten.lief_rech_nr,0,17);
    ins_quest ((char *) zerldaten.ident_extern,0,21);
    ins_quest ((char *) zerldaten.ident_intern,0,21);
    ins_quest ((char *) &zerldaten.a,3,0);
    ins_quest ((char *) &zerldaten.a_gt,3,0);
    ins_quest ((char *) zerldaten.kategorie,0,4);
    ins_quest ((char *) &zerldaten.schnitt,1,0);
    ins_quest ((char *) &zerldaten.gebland,1,0);
    ins_quest ((char *) &zerldaten.mastland,1,0);
    ins_quest ((char *) &zerldaten.schlaland,1,0);
    ins_quest ((char *) &zerldaten.zerland,1,0);
    ins_quest ((char *) zerldaten.esnum,0,11);
    ins_quest ((char *) zerldaten.eznum1,0,11);
    ins_quest ((char *) zerldaten.eznum2,0,11);
    ins_quest ((char *) zerldaten.eznum3,0,11);
    ins_quest ((char *) &zerldaten.dat,2,0);
    ins_quest ((char *) &zerldaten.anz_gedruckt,1,0);
    ins_quest ((char *) &zerldaten.anz_entnommen,1,0);
    ins_quest ((char *) &zerldaten.lfd_i,2,0);
    ins_quest ((char *) &zerldaten.aktiv,1,0);
    ins_quest ((char *) &zerldaten.zerlegt,1,0);
            ins_cursor = prepare_sql ("insert into zerldaten ("
"mdn,  fil,  partie,  lief,  lief_rech_nr,  ident_extern,  ident_intern,  a,  a_gt,  "
"kategorie,  schnitt,  gebland,  mastland,  schlaland,  zerland,  esnum,  eznum1,  "
"eznum2,  eznum3,  dat,  anz_gedruckt,  anz_entnommen,  lfd_i,  aktiv,  zerlegt) "

#line 59 "zerldaten.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"); 

#line 61 "zerldaten.rpp"
/*
            ins_quest ((char *) &zerldaten.mdn, 1, 0);
            ins_quest ((char *) &zerldaten.fil, 1, 0);     
            ins_quest ((char *) &zerldaten.a, 3, 0);
            ins_quest ((char *) zerldaten.ident_intern, 0, sizeof (zerldaten.ident_intern));
*/
            ins_quest ((char *) &zerldaten.lfd, 2, 0);
            test_upd_cursor = prepare_sql ("select a from zerldaten "
                                  "where lfd = ?");
/*
                                  "where mdn = ? "
                                  "and   fil   = ? "
                                  "and   a   = ? "
                                  "and   ident_intern = ?");
*/


            ins_quest ((char *) &zerldaten.mdn, 1, 0);
            ins_quest ((char *) &zerldaten.fil, 1, 0);     
            ins_quest ((char *) &zerldaten.a, 3, 0);
            ins_quest ((char *) zerldaten.ident_extern, 0, sizeof (zerldaten.ident_extern));
	    ins_quest ((char *) zerldaten.lief,0,17);
	    ins_quest ((char *) zerldaten.lief_rech_nr,0,17);
            out_quest ((char *) &zerldaten.lfd, 2, 0);

            test_upd_cursor2 = prepare_sql ("select lfd from zerldaten "

                                  "where mdn = ? "
                                  "and   fil   = ? "
                                  "and   a   = ? "
                                  "and   ident_extern = ? "
                                  "and   lief = ? "
				  "and   lief_rech_nr = ?");

/*
            ins_quest ((char *) &zerldaten.mdn, 1, 0);
            ins_quest ((char *) &zerldaten.fil, 1, 0);     
            ins_quest ((char *) &zerldaten.a, 3, 0);
            ins_quest ((char *) zerldaten.ident_intern, 0, sizeof (zerldaten.ident_intern));
*/
            ins_quest ((char *) &zerldaten.lfd, 2, 0);
            del_cursor = prepare_sql ("delete from zerldaten "
                                  "where lfd = ?");
/*
                                  "where mdn = ? "
                                  "and   fil   = ? "
                                  "and   a   = ? "
                                  "and   ident_intern = ?");
*/
}

int ZERLDATEN_CLASS::dbinsert (void)
{
 	   if (ins_cursor == -1)
           {
                    prepare ();
           } 
           execute_curs (ins_cursor);
           return 0;
}

int ZERLDATEN_CLASS::dbmodify (void)
{
 	   if (upd_cursor == -1)
           {
                    prepare ();
           } 
           execute_curs (upd_cursor);
           return 0;
}

int ZERLDATEN_CLASS::dbtestinsert (void)
{
 	   if (test_upd_cursor2 == -1)
           {
                    prepare ();
           } 
           open_sql (test_upd_cursor2);
           return fetch_sql (test_upd_cursor2);
}
