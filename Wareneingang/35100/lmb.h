#ifndef _LMBDEF
#define _LMBDEF
#include "liefmebest.h"

class LMB_CLASS : public LIEFMEBEST_CLASS
{
       private :
               void prepare (void);
               int cursor_a;
       public :
               LMB_CLASS () : LIEFMEBEST_CLASS ()
               {
               }
               int dbreadfirst(void);
               int dbreadfirst_a(void);
               int dbread_a (void);
};
#endif
