#ifndef _WEWORK_DEF
#define _WEWORK_DEF
#include "lief_bzg.h"
#include "liefmebest.h"
#include "LiefBzgDlg.h"

#include "we_kopf.h"
#include "we_pos.h"
#include "we_jour.h"
#include "ptab.h"
#include "searchptab.h"
#include "mo_wepr.h"
#include "lief.h"
#include "mo_einh.h"
#include "mo_a_pr.h"
#include "qswekopf.h"
#include "qswepos.h"
#include "best_kopf.h"
#include "best_pos.h"
#include "textwork.h"
#include "mdn.h"
#include "dlg.h"
#include "listenter.h"
#include "bsd_buch.h"
#include "ExtraData.h"
#include "Rind.h"
#include "Zerldaten.h"
#include "Vector.h" 

#define GEDRUCKT 2
#define VERBUCHT 3


struct LSTS
{
        char int_pos [10];
        char a [15];
        char lief_best [20];
        char a_krz [10];
        char a_bz1 [26];
        char a_bz2 [26];
        char me [15];
        char best_me [15];
        char me_einh [6];
        char me_einh_bez [38];
        char best_me_einh [6];
        char best_me_einh_bez [38];
        char inh [12];
        char tara [10];
        char mhd [10];
        char pr_ek [12];
        char netto_ek [12];
        char pos_wert [12];
        char hbk_ztr [10];
        char pr_vk [10];
        char spanne [10];
        char pr_fil_ek [12];
        char akv [12];
        char ls_ident [22];
        char ls_charge [32];
        char p_nr[10];
        short anz_emb;
        double inh_einh;
		char me_kz[2];
		char scanfeld [80];
	    char hbk_dat [15];
        char lager_bez [38];
        short lager;
		BOOL  Emb;
        ListButton me_choise;
        ListButton pr_ek_choise;
};
          


class WeWork
{
    private :
        LIEF_BZG_CLASS LiefBzg;
        LIEF_CLASS Lief;
		ZERLDATEN_CLASS Zerldaten;

        BSD_BUCH_CLASS BsdBuch;
        WE_KOPF_CLASS WeKopf;
        WE_POS_CLASS WePosClass;
        WE_JOUR_CLASS WeJourClass;
        PTAB_CLASS Ptab;
        EINH_CLASS Einh;
        QSWEKOPF_CLASS QsWeKopf;
        QSWEPOS_CLASS QsWePos;
        BEST_KOPF_CLASS BestKopfClass;
        BEST_POS_CLASS BestPosClass;
        MDN_CLASS Mdn;
		RIND_CLASS Rind;

        TextWork *textWork;

        int liefbzg_cursor;
        char default_lief_kz [4];
        short sa;
 	    double pr_ek0;
	    double pr_ek_bto0;
	    double pr_ek0_euro;
	    double pr_ek_bto0_euro;
        int fil_sa;
        double pr_fil_ek;
        double pr_vk;
        double me_einh;
		int WorkMeEinh;
        WE_PREISE WePreis;
        AB_PREISE AbPreise;
//        struct LSTS *Lsts;
        struct LSTS *Lsts;
        static char *Ub[];
        static char *Texte0[];
        static int Yes0[];
        static int No0[];
        static int qsanz;

        static char *UbPos[];
        static char *TextePos0[];
        static int YesPos0[];
        static int NoPos0[];
        static int qsanzpos;

        char **Texte;
        int *Yes;
        int *No;
        char **TextePos;
        int *YesPos;
        int *NoPos;

        int best_koresp;
        BOOL multibest;
        char **TxtUpdates;
        static long *lieftab[];
        static char *korestab[];
        static long *termtab[];
        static long LongNull;
        DLG *Dlg;
        LISTENTER *ListDlg;
        int LstCursor;
        int TestKontraktMe;
        double akt_me;
        double AktBsdMe;
        int bsd_kz;
        static short akt_lager;
        BOOL MeStat;
        BOOL we_fil_bew_par;
        BOOL multimandant_par;
        short we_vk_par;
		BOOL WeFilBewParOK;
		BOOL MultimandantParOK;
		BOOL WeVkParOK;
		BOOL OhneFilEkAktionOK;

    public :
        static double inh;
        static double pr_ek_einh;
        static BOOL Weldr;
		static int ListDiff;

        void SetMeStat (BOOL MeStat)
        {
            this->MeStat = MeStat;
        }

        static void SetLager (short lager)
        {
            akt_lager = lager;
        }

        long GetLager (void)
        {
            return akt_lager;
        }

        void SetBsdKz (int b)
        {
            bsd_kz = b;
        }

        int GetBsdKz (void)
        {
            return bsd_kz;
        }

        void SetAktBsdMe (double me)
        {
            AktBsdMe = me;
        }

        double GetAktBsdMe (void)
        {
            return AktBsdMe;
        }

        void SetAktMe (double me)
        {
            akt_me = me;
        }

        double GetAktMe (void)
        {
            return akt_me;
        }

        void SetTestKontrakt (int k)
        {
            TestKontraktMe = k;
        }

        void SetDlg (DLG *Dlg)
        {
            this->Dlg = Dlg;
        }

        DLG *GetDlg (void)
        {
            return Dlg;
        }

        void SetListDlg (LISTENTER *ListDlg)
        {
            this->ListDlg = ListDlg;
        }

        LISTENTER *GetListDlg (void)
        {
            return ListDlg;
        }

        void SetTextWork (TextWork *textWork)
        {
            this->textWork = textWork;
        }

        TextWork *GetTextWork (void)
        {
            return textWork;
        }

        void SetTxtUpdates (char **TxtUpdates)
        {
            this->TxtUpdates = TxtUpdates;
        }

        char **GetTxtUpdates (void)
        {
            return TxtUpdates;
        }

        void NewTxtUpdates (void)
        {
            int i;

            if (TxtUpdates == NULL) return;

            for (i = 0; TxtUpdates[i] != NULL; i ++)
            {
                delete TxtUpdates[i];
            }
            TxtUpdates = NULL;
        }

        char **GetUb (void)
        {
            return Ub;
        }
        
        int Getqsanz ()
        {
            return qsanz;
        }

        void SetTexte (char **Texte)
        {
            this->Texte = Texte;
        }

        char **GetTexte (void)
        {
            return Texte;
        }

        void SetYes (int*Yes)
        {
            this->Yes = Yes;
        }

        int*GetYes (void)
        {
            return Yes;
        }

        void SetNo (int*No)
        {
            this->No = No;
        }

        int*GetNo (void)
        {
            return No;
        }


        char **GetUbPos (void)
        {
            return UbPos;
        }
        
        int Getqsanzpos ()
        {
            return qsanzpos;
        }

        void SetTextePos (char **TextePos)
        {
            this->TextePos = TextePos;
        }

        char **GetTextePos (void)
        {
            return TextePos;
        }

        void SetYesPos (int*YesPos)
        {
            this->YesPos = YesPos;
        }

        int*GetYesPos (void)
        {
            return YesPos;
        }

        void SetNoPos (int*NoPos)
        {
            this->NoPos = NoPos;
        }

        int*GetNoPos (void)
        {
            return NoPos;
        }


        void SetLsts (struct LSTS *Lsts)
        {
            this->Lsts = Lsts;
        }

        struct LSTS *GetLsts (void)
        {
            return Lsts;
        }

        double GetPrEk (void)
        {
            if (lief.waehrung == 2)
            {
                 return pr_ek0_euro;
            }
            else
            {
                 return pr_ek0;
            }
        }

        double GetPrEkBto (void)
        {
            if (lief.waehrung == 2)
            {
                 return pr_ek_bto0_euro;
            }
            else
            {
                 return pr_ek_bto0;
            }
        }

        short GetSa (void)
        {
            return sa;
        }

        double GetPrFilEk (void)
        {
            return pr_fil_ek;
        }

        double GetPrVk (void)
        {
            return pr_vk;
        }

        short GetFilSa (void)
        {
            return fil_sa;
        }
		short HoleLager (double a,short mdn);
		char* HoleLagerBez (short lager, short mdn);

        WeWork () : liefbzg_cursor (-1)
        {
            strcpy (default_lief_kz, "N");
            Lsts = NULL;
            TxtUpdates = NULL;
            Dlg = NULL;
            ListDlg = NULL;
			LstCursor = -1;
            TestKontraktMe = 1;
            bsd_kz = 1;
            akt_lager = 0;
            MeStat = FALSE;
            Texte = NULL;
            Yes   = NULL;
            No    = NULL;
            TextePos = NULL;
            YesPos   = NULL;
            NoPos    = NULL;
            WeFilBewParOK = FALSE;
            MultimandantParOK = FALSE;
            WeVkParOK = FALSE;
            we_fil_bew_par = FALSE;
            multimandant_par = FALSE;
            we_vk_par = 0;
        }

        void SetDefaultLiefKz (char *lief_kz)
        {
            strcpy (default_lief_kz, lief_kz);
        }

        void ExitError (BOOL);
        void TestNullDat (long *);
        void TestAllDat (void);
        void BeginWork (void);
        void CommitWork (void);
        void RollbackWork (void);
        void InitRec (void);
        int ReadLief (short, short, char *);
        int ReadLief (void);
        BOOL GenSqlout (char *, char *);
        BOOL GenSqlout (char *, char *, TextWork *);
        char *GetWeField (char *, int);
        int ReadBestTxt (void);
        int ReadTxt (void);
		int ReadWeKopf (void);
        BOOL LockWeKopf ();
        int Reada_pr (void);
        int Schreibea_pr (void);
        int CalcEkBto (void);
        int ReadPr (void);
        int ReadEan (double);
        int ReadEmb (double);
        int ReadA (double);
        int ReadA (double, char*);
        int ReadLief_besta (double);
        int ReadLief_besta (double,char*);
        int ReadLief_best (char *);
        int ReadA_krz (long);
        void FillRow (void);
        BOOL GenSqlin (char *, char *);
        BOOL GenSqlin (char *, char *, TextWork *);
        int  WriteTxt (void);
        void WriteWeKopf (void);
        void ReWriteWeKopf (void);
        long GethbkDat (long);
        double GetRabEff (void);
        double GetRabEffEu (void);
        double GetLiefWrtO (void);
        double GetLiefWrtOEu (void);
        double GetLiefWrtNto (void);
        double GetLiefWrtNtoEu (void);
        double GetLiefWrtAb (void);
        double GetLiefWrtAbEu (void);
        double GetMwstEu (void);
        double GetMwst (void);
        long GetWePosAnz (void);
        long GetWePosAnzMa (void);
        void WriteWePos (WE_POS_CLASS *, int);
        void WriteWeJour (WE_JOUR_CLASS *, int);
		void FillCountries ();
		int GetCountrieByKey (CVector *Countries, int key);
		void ReadCountries ();
		void WriteExtraData (CExtraData *ExtraData);
		void ReadExtraData (CExtraData *ExtraData);
		void FillZerlLfd ();
        void WritePr (void);
        void WriteLief (void);
        BOOL GetKreditor (void);
        int GetPtab (char *, char *, char *);
        int ShowPtab (char *dest, char *item);
        int ShowPtabEx (HWND, char *dest, char *item);
        int GetMdnName (char *, short);
        int GetFilName (char *, short, short);
        int GetLiefName (char *, short, char *);
        double GetMeVgl (double,char *, double, short);
        BOOL GetMeEinh (short, short, char *, double, char *);
        BOOL GetMeEinhBest (short, short, char *, double, char *);
		double GetMaxInh (short mdn, short fil, char *lief_nr, double a);
        int ChMeEinh (HWND);
        int ShowLager (HWND,double a);
        void InitRow (void);
        void FillRow (char *);
        BOOL PosExist (void);
        void DeleteAllPos (short, short, char *, char *, char *);
        void DeleteWe ();
        char *GenCharge (HWND, char *, char *);
        char *GenLiefRechNr (HWND, char *, char *);
        void FillQsHead (void);
        void FillTexteDefaults (void);
        void FillQsPos (void);
        void InitQm (int *, int *, int);
        void QmDefaultYes (int *, int *, int);
        void QmDefaultNo (int *, int *, int);
        BOOL TestQm (int *, int *, int);
        void WriteQs (void);
        void WriteQsPos (void);
        int TestQsPos (void);
        BOOL TestBestMulti (void);
        void BestToWe (void);
        int CreateLiefBzg (void);
        int TestLief_bzg (void);
        int TestLief_bzg (double, char *);
        void PrepareLst (void);
        void OpenLst (void);
        int FetchLst (void);
        void CloseLst (void);
        void ReadBestKopf (void);
        int  TestKontrakt (double a);
        int  UpdateKontrakt (double a);
        void DecKontrakt (double, double);
        void DecKontrakt (void);
        BOOL BsdArtikel (double);
        int  GetHbk (double, int); 
        void TestChargeDat (void);
        void BucheBsd (double, char*, double, short, double, char*, char*,short lager);
        BOOL WeFilBewPar ();
        BOOL OhneFilEkAktionPar ();
        BOOL MultimandantPar ();
        short WeVkPar ();
        BOOL EnterLiefBzgA (double);
        BOOL EnterLiefBzgA (double,char*);
        static void FuellePreise_we (short);
        void SchreibeLief (void);
		BOOL ReadExtraData ();

};

#endif