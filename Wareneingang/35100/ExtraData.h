// ExtraData.h: Schnittstelle f�r die Klasse CExtraData.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_EXTRADATA_H__A916018B_D35C_4ADE_9838_CF7E1402ADBC__INCLUDED_)
#define AFX_EXTRADATA_H__A916018B_D35C_4ADE_9838_CF7E1402ADBC__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "DataCollection.h"
#include "Text.h"

typedef struct
{
  char Charge [41]; 
} SCharge;

class CExtraData  
{
public:
	CDataCollection<SCharge> Chargen;
	char EZNr[41];
	char ESNr[41];
	char Countries[41];
	char IdentNr[41];
	CExtraData();
	virtual ~CExtraData();
	SCharge *Get (int idx);
};

#endif // !defined(AFX_EXTRADATA_H__A916018B_D35C_4ADE_9838_CF7E1402ADBC__INCLUDED_)
