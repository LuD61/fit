//100209    buchebsd beim delete
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include <string.h>
#include "comcthlp.h"
#include "itemc.h"
#include "wmaskc.h"
#include "mo_meld.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_intp.h"
#include "listdlg.h"
#include "inflib.h"
#include "wedlg.h"
#include "a_bas.h"
#include "lief_bzg.h"
#include "mo_menu.h"
#include "mo_stdac.h"
#include "sys_par.h"
#include "enterfunc.h"
#include "ListDlg.h"
#include "dlg.h"
#include "mo_liefch.h"
#include "mo_qa.h"

#ifdef _DEBUG
#include <crtdbg.h>
extern _CrtMemState memory;
#endif

#define IDM_CANCEL 3001
#ifndef IDM_WRITE
#define IDM_WRITE  3002
#endif

short AnzTagePr = 365;
bool flgLoesch0;
static int StdSize = STDSIZE;
BOOL LISTDLG::InEnterPos = FALSE;

static QueryClass QClass;
static int dlgsize    = STDSIZE;
static int ltgraysize = STDSIZE;
static int dlgpossize = STDSIZE;
static int cpsize     = 200;
double pr_vk_save = 0.0;
char AktChargennr [31];
char AktIdentnr [31];


static mfont dlgfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         GRAYCOL,
                         0,
                         NULL};

struct LSTS  LISTDLG::Lsts;
struct LSTS *LISTDLG::LstTab;


CFIELD *LISTDLG::_fChoise0 [] = {
    new CFIELD ("cancel", " abbrechen ", 10, 0, 0, 0, NULL, "",
                CBUTTON, 
                IDM_CANCEL, &dlgfont, 0, 0),
    new CFIELD ("write", " speichern ", 10, 0,12, 0, NULL, "",
                CBUTTON, 
                IDM_WRITE, &dlgfont, 0, 0),
    new CFIELD ("show",  "  ansehen  ", 10, 0,24, 0, NULL, "",
                CBUTTON, 
                IDM_CHOISE, &dlgfont, 0, 0),
    new CFIELD ("delete","  l�schen  ", 10, 0,36, 0, NULL, "",
                CBUTTON, 
                IDM_DELETE, &dlgfont, 0, 0),
    new CFIELD ("insert"," einf�gen  ", 10, 0,48, 0, NULL, "",
                CBUTTON, 
                IDM_INSERT, &dlgfont, 0, 0),
    NULL,
};

LISTDLG *LISTDLG::ActiveListDlg = NULL;

CFORM LISTDLG::fChoise0 (5, _fChoise0);

CFIELD *LISTDLG::_fChoise [] = {
    new CFIELD ("fChoise0", (char *) &fChoise0, 0, 0, -1 , -2, NULL, "",
                CFFORM, 
                500, &dlgfont, 0, 0),
    NULL,
};

CFORM LISTDLG::fChoise (1, _fChoise);

static ColButton CArtikel   = { "Artikel",  -1, -1, 
//                                "Bestell-Nummer",  -1, 1,
                                NULL,  0, 0,
                                NULL,  0, 0,
                                NULL, -1, -1,
                                NULL,  0, 0,
                                BLACKCOL,
                                LTGRAYCOL,
//                             BLUECOL,
//                             NOCOLPRESS | NOCOLBORDER,
                             0, 
};


//ITEM LISTDLG::ua              ("a",              "Artikel",         "", 0);
ITEM LISTDLG::ua              ("a",              (char *) &CArtikel,         "", 0);
ITEM LISTDLG::ua_bz1          ("a_bz1",          "Bezeichnung",     "", 0);
ITEM LISTDLG::ua_bz2          ("a_bz2",          "Bezeichnung",     "", 0);
ITEM LISTDLG::ubest_me        ("best_me",        "Best.Menge",      "", 0);
ITEM LISTDLG::ume             ("me",             "Menge",           "", 0);
ITEM LISTDLG::ume_einh_bez    ("me_einh_bez",    "Einh-Bez",        "", 0);
ITEM LISTDLG::uinh            ("inh",            "Inh/PosWert",     "", 0);
ITEM LISTDLG::utara           ("tara",           "Tara",            "", 0);
ITEM LISTDLG::upr_ek          ("pr_ek",          "EK/PrVK",         "", 0);
ITEM LISTDLG::unetto_ek       ("netto_ek",       "NtoEK /FilEK",    "", 0);
ITEM LISTDLG::upos_wert       ("pos_wert",       "Pos.Wert",        "", 0);
ITEM LISTDLG::uhbk_ztr        ("hbk_ztr",        "MHD",             "", 0);
ITEM LISTDLG::upr_vk          ("pr_vk",          "VK",              "", 0) ;
ITEM LISTDLG::uspanne         ("spanne",         "Spanne",          "",0);
ITEM LISTDLG::uls_ident       ("ls_ident",       "Ident-Nummer",    "",0);
ITEM LISTDLG::uls_charge      ("ls_charge",      "Chargen-Nummer",  "",0);
ITEM LISTDLG::ufiller         ("",               " ",               "",0);


void LISTDLG::SetPrFieldText (BOOL mode)
{
	if (mode == FALSE)
	{
		upr_ek.feld = "EK";
		unetto_ek.feld ="Netto_EK";
	}
	else
	{
		upr_ek.feld = "EK/PrVK";
		unetto_ek.feld = "NtEK /FlEK";
	}
}

void LISTDLG::SetInhFieldText (BOOL mode)
{
	if (mode == FALSE)
	{
		uinh.feld = "Inh";
	}
	else
	{
		uinh.feld = "Inh/PosWert";
	}
}


ITEM LISTDLG::iline ("", "1", "", 0);

ITEM LISTDLG::ia              ("a",              Lsts.a,            "", 0);
ITEM LISTDLG::ilief_best      ("lief_best",      Lsts.lief_best,    "", 0);
ITEM LISTDLG::ia_krz          ("a_krz",          Lsts.a_krz,        "", 0);
ITEM LISTDLG::ia_bz1          ("a_bz1",          Lsts.a_bz1,        "", 0);
ITEM LISTDLG::ia_bz2          ("a_bz2",          Lsts.a_bz2,        "", 0);
ITEM LISTDLG::ibest_me        ("best_me",        Lsts.best_me,      "", 0);
ITEM LISTDLG::ime             ("me",             Lsts.me,           "", 0);
ITEM LISTDLG::ime_einh_bez    ("me_einh_bez",    Lsts.me_einh_bez,  "", 0);
ITEM LISTDLG::ime_choise      ("me_choise",      (char *) &Lsts.me_choise, 
                                                                    "", 0);
ITEM LISTDLG::iinh            ("inh",            Lsts.inh,          "", 0);
ITEM LISTDLG::itara           ("tara",           Lsts.tara,         "", 0);
ITEM LISTDLG::ipr_ek          ("pr_ek",          Lsts.pr_ek,        "", 0);
ITEM LISTDLG::ipr_ek_choise   ("pr_ek_choise",   (char *) &Lsts.pr_ek_choise,"", 0); 
ITEM LISTDLG::inetto_ek       ("netto_ek",       Lsts.netto_ek,     "", 0);
ITEM LISTDLG::ipos_wert       ("pos_wert",       Lsts.pos_wert,     "", 0);
ITEM LISTDLG::ihbk_ztr        ("hbk_ztr",        Lsts.hbk_ztr,      "", 0);
ITEM LISTDLG::ipr_vk          ("pr_vk",          Lsts.pr_vk,        "", 0) ;
ITEM LISTDLG::ifil_ek         ("fil_ek",         Lsts.pr_fil_ek,    "", 0) ;
ITEM LISTDLG::ispanne         ("spanne",         Lsts.spanne,       "",0);
ITEM LISTDLG::ils_ident       ("ls_ident",       Lsts.ls_ident,     "",0);
ITEM LISTDLG::ils_charge      ("ls_charge",      Lsts.ls_charge,    "",0);


field LISTDLG::_UbForm0 [] = {
&ua,       19, 0, 0,  6,  NULL, "",  COLBUTTON, 0, 0, 0,     
&ua_bz1,   27, 0, 0, 25,  NULL, "",  BUTTON, 0, 0, 0, 
&ubest_me, 13, 0, 0, 52,  NULL, "",  BUTTON, 0, 0, 0,
&ume,      13, 0, 0, 65,  NULL, "",  BUTTON, 0, 0, 0,
&ume_einh_bez,
           12, 0, 0, 78,  NULL, "",  BUTTON, 0, 0, 0,
&upr_ek,   14, 0, 0, 90,  NULL, "",  BUTTON, 0, 0, 0,     
&unetto_ek,  
           11, 0, 0,104,  NULL, "",  BUTTON, 0, 0, 0,     
&uinh,     11, 0, 0,115,  NULL, "",  BUTTON, 0, 0, 0,
&uls_ident,23, 0, 0,126,  NULL, "",  BUTTON, 0, 0, 0,
&uls_charge,
           41, 0, 0,149,  NULL, "",  BUTTON, 0, 0, 0,    // vorher 34
&ufiller, 100, 0, 0,183,  NULL, "", BUTTON, 0, 0, 0,    // vorher 173
};

form LISTDLG::UbForm0 = {11, 0, 0, _UbForm0, 0, 0, 0, 0, NULL};

field LISTDLG::_UbForm [] = {
&ua,       19, 0, 0,  6,  NULL, "",  COLBUTTON, 0, 0, 0,     
&ua_bz1,   27, 0, 0, 25,  NULL, "",  BUTTON, 0, 0, 0, 
&ubest_me, 13, 0, 0, 52,  NULL, "",  BUTTON, 0, 0, 0,
&ume,      13, 0, 0, 65,  NULL, "",  BUTTON, 0, 0, 0,
&ume_einh_bez,
           12, 0, 0, 78,  NULL, "",  BUTTON, 0, 0, 0,
&upr_ek,   14, 0, 0, 90,  NULL, "",  BUTTON, 0, 0, 0,     
&unetto_ek,  
           11, 0, 0,104,  NULL, "",  BUTTON, 0, 0, 0,     
&uinh,     11, 0, 0,115,  NULL, "",  BUTTON, 0, 0, 0,
&uls_ident,23, 0, 0,126,  NULL, "",  BUTTON, 0, 0, 0,
&uls_charge,
           41, 0, 0,149,  NULL, "",  BUTTON, 0, 0, 0,   //vorher 34
&ufiller, 100, 0, 0,173,  NULL, "", BUTTON, 0, 0, 0,
};


form LISTDLG::UbForm = {11, 0, 0, _UbForm, 0, 0, 0, 0, NULL};

field LISTDLG::_LineForm0 [] = {
&iline,   1, 0, 0, 25,  NULL, "",  DISPLAYONLY, 0, 0, 0, 
&iline,   1, 0, 0, 52,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0, 65,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0, 78,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0, 90,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,103,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,115,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0,126,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0,149,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0,183,  NULL, "",  DISPLAYONLY, 0, 0, 0,    //vorher 173
};

form LISTDLG::LineForm0 = {10, 0, 0, _LineForm0,0, 0, 0, 0, NULL};

field LISTDLG::_LineForm [] = {
&iline,   1, 0, 0, 25,  NULL, "",  DISPLAYONLY, 0, 0, 0, 
&iline,   1, 0, 0, 52,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0, 65,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0, 78,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0, 90,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,103,  NULL, "",  DISPLAYONLY, 0, 0, 0,     
&iline,   1, 0, 0,115,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0,126,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0,149,  NULL, "",  DISPLAYONLY, 0, 0, 0,
&iline,   1, 0, 0,183,  NULL, "",  DISPLAYONLY, 0, 0, 0,  //vorher 173
};

form LISTDLG::LineForm = {10, 0, 0, _LineForm,0, 0, 0, 0, NULL};

field LISTDLG::_DataForm0 [] = {
&ia,     16, 0, 0,  8,  NULL, "%13.0f",  DISPLAYONLY,      0, 0, 0,     
&ilief_best, 
         17, 0, 1,  7,  NULL, "",      DISPLAYONLY,      0, 0, 0,     
&ia_krz,  9, 0, 1, 15,  NULL, "%8d",   DISPLAYONLY,      0, 0, 0,     
&ia_bz1, 25, 0, 0, 26,  NULL, "",      DISPLAYONLY, 0, 0, 0, 
&ibest_me,
         11, 0, 0, 53,  NULL, "%8.3f",  DISPLAYONLY, 0, 0, 0,
&ime,    11, 0, 0, 66,  NULL, "%8.3f",  NORMAL,      0, 0, 0,
/*
&ime_einh_bez,
         10, 0, 0, 66,  NULL, "",       DISPLAYONLY, 0, 0, 0,
*/


&ime_einh_bez,
          8, 0, 0, 79,  NULL, "",       DISPLAYONLY, 0, 0, 0,
// &ime_choise,
//          3, 0, 0, 74,  NULL, "",       BUTTON, 0, 0, 0,

&ime_choise,
          2, 1, 0, 87,  NULL, "",       BUTTON, 0, 0, 0,


&ipr_ek, 10, 0, 0, 91,  NULL, "%8.4f",  NORMAL,      0, 0, 0,     
&ipr_ek_choise,
          2, 1, 0, 101,  NULL, "",       BUTTON, 0, 0, 0,
&ipr_vk, 10, 0, 1, 91,  NULL, "%8.2f",  DISPLAYONLY, 0, 0, 0,     
&inetto_ek,  
         10, 0, 0,105,  NULL, "%8.4f",  DISPLAYONLY, 0, 0, 0,     
&iinh,   10, 0, 0,116,  NULL, "%8.3f",  DISPLAYONLY, 0, 0, 0,
&ils_ident,
         21, 0, 0,127,  NULL, "",       NORMAL, 0, 0, 0, 
&ils_charge,
         31, 0, 0,151,  NULL, "",       DISPLAYONLY, 0, 0, 0, 
&ia_bz2, 25, 0, 1, 26,  NULL, "",       DISPLAYONLY, 0, 0, 0, 
&ifil_ek,  
         10, 0, 1,105,  NULL, "%8.4f",  DISPLAYONLY, 0, 0, 0,     
&ipos_wert, 
         10, 0, 1,116,  NULL, "%8.4f",  DISPLAYONLY, 0, 0, 0,
};

form LISTDLG::DataForm0 = {18, 0, 0, _DataForm0,0, 0, 0, 0, NULL};


field LISTDLG::_DataForm [] = {
&ia,     14, 0, 0, 10,  NULL, "%13.0f",  DISPLAYONLY,      0, 0, 0,     
&ilief_best, 
         17, 0, 1,  7,  NULL, "",      DISPLAYONLY,      0, 0, 0,     
&ia_krz,  9, 0, 1, 15,  NULL, "%8d",   DISPLAYONLY,      0, 0, 0,     
&ia_bz1, 25, 0, 0, 26,  NULL, "",      DISPLAYONLY, 0, 0, 0, 
&ibest_me,
         11, 0, 0, 53,  NULL, "%8.3f",  DISPLAYONLY, 0, 0, 0,
&ime,    11, 0, 0, 66,  NULL, "%8.3f",  NORMAL,      0, 0, 0,
/*
&ime_einh_bez,
         10, 0, 0, 66,  NULL, "",       DISPLAYONLY, 0, 0, 0,
*/


&ime_einh_bez,
          8, 0, 0, 79,  NULL, "",       DISPLAYONLY, 0, 0, 0,
// &ime_choise,
//          3, 0, 0, 74,  NULL, "",       BUTTON, 0, 0, 0,

&ime_choise,
          2, 1, 0, 87,  NULL, "",       BUTTON, 0, 0, 0,


&ipr_ek, 10, 0, 0, 91,  NULL, "%8.4f",  NORMAL,      0, 0, 0,     
&ipr_ek_choise,
          2, 1, 0, 101,  NULL, "",       BUTTON, 0, 0, 0,
&ipr_vk, 10, 0, 1, 91,  NULL, "%8.2f",  DISPLAYONLY, 0, 0, 0,     
&inetto_ek,  
         10, 0, 0,105,  NULL, "%8.4f",  DISPLAYONLY, 0, 0, 0,     
&iinh,   10, 0, 0,116,  NULL, "%8.3f",  DISPLAYONLY, 0, 0, 0,
&ils_ident,
         21, 0, 0,127,  NULL, "",       NORMAL, 0, 0, 0, 
&ils_charge,
         31, 0, 0,151,  NULL, "",       DISPLAYONLY, 0, 0, 0, 
&ia_bz2, 25, 0, 1, 26,  NULL, "",       DISPLAYONLY, 0, 0, 0, 
&ifil_ek,  
         10, 0, 1,105,  NULL, "%8.4f",  DISPLAYONLY, 0, 0, 0,     
&ipos_wert, 
         10, 0, 1,116,  NULL, "%8.4f",  DISPLAYONLY, 0, 0, 0,
};

form LISTDLG::DataForm = {18, 0, 0, _DataForm,0, 0, 0, 0, NULL};

/*
int LISTDLG::ubrows [] = {0, 0, 0, 
                          1,
                          2,
                          3,4,5,6,
                          7,8,
                          1,
                          -1,
};
*/

int LISTDLG::ubrows0 [] = {0, 0, 0, 
                          1,
                          2,
                          3,3, 
                          4,4,4,5,6,
                          7,8,9,
                          1,5,6,7
                          -1,
};


int LISTDLG::ubrows [] = {0, 0, 0, 
                          1,
                          2,
                          3,3, 
                          4,4,4,5,6,
                          7,8,9,
                          1,5,6,
                          -1,
};


void LISTDLG::SetBaseForms ()
{
	for (int i = 0; i < UbForm0.fieldanz; i ++)
	{
		memcpy (&UbForm.mask[i], &UbForm0.mask[i], sizeof (field));
	}
	UbForm.fieldanz = UbForm0.fieldanz;

	for (i = 0; i < LineForm0.fieldanz; i ++)
	{
		memcpy (&LineForm.mask[i], &LineForm0.mask[i], sizeof (field));
	}
	LineForm.fieldanz = LineForm0.fieldanz;

	for (i = 0; i < DataForm0.fieldanz; i ++)
	{
		memcpy (&DataForm.mask[i], &DataForm0.mask[i], sizeof (field));
	}
	DataForm.fieldanz = DataForm0.fieldanz;
	memcpy (ubrows, ubrows0, sizeof (ubrows0));
}

struct CHATTR LISTDLG::ChAttra [] = {"a",     DISPLAYONLY, EDIT,
                              NULL,  0,           0};
struct CHATTR LISTDLG::ChAttrlief_best [] = {"lief_best", DISPLAYONLY, EDIT,
                                     NULL,   0,           0};

struct CHATTR LISTDLG::ChAttra_krz [] = {"a_krz", DISPLAYONLY, EDIT,
                                          NULL,   0,           0};

struct CHATTR *LISTDLG::ChAttr = ChAttra;

// BOOL LISTDLG::cfgOK = FALSE;

SEARCHA LISTDLG::SearchA;


BOOL LISTDLG::CallInfo (HWND hWnd, RECT *rect, char *Item, char *ItemValue, DWORD WinMode)
/**
Einstieg in Infosystem mit Artikel. Abgeleitet von Klasse Info.
**/
{
 		 char buffer [1024];
         BOOL ret;

         sprintf (buffer, "where mdn = %hd and fil = %hd and "
			              "lief = \"%s\" and a = %.0lf", 
                           we_kopf.mdn, 
                           we_kopf.fil, 
						   we_kopf.lief, 
						   ratod (Lsts.a));
         ret = Info::CallInfo (hWnd, rect, "liefa", buffer, WinMode); 
         SetListFocus ();
         return ret;
}


int LISTDLG::InfoProc (char **Item, char *Value, char *where)
/**
Spezielle Procedure fuer Info-System. Alter Ablauf, ist nicht mehr Aktiv 
**/
{

   *Item = "a";

   sprintf (where, "where mdn = %hd and fil = %hd and "
			"            lief = %s and a = %.0lf", 
                         we_kopf.mdn, 
                         we_kopf.fil, 
						 we_kopf.lief, 
						 ratod (Lsts.a));
   return 1;
}

void LISTDLG::SetButtonSize (int Size)
{
    int fpos;

    fpos = GetItemPos (&DataForm, "me_choise");
    if (fpos < 0) return;

    if (Size == SMALL)
    {
         DataForm.mask [fpos].length = 2;
         DataForm.mask [fpos].rows = 1;
    }
    else 
    {
         DataForm.mask [fpos].length = 3;
         DataForm.mask [fpos].rows = 0;
    }
    fpos = GetItemPos (&DataForm, "pr_ek_choise");
    if (fpos < 0) return;

    if (Size == SMALL)
    {
         DataForm.mask [fpos].length = 2;
         DataForm.mask [fpos].rows = 1;
    }
    else 
    {
         DataForm.mask [fpos].length = 3;
         DataForm.mask [fpos].rows = 0;
    }
}

void LISTDLG::SetWithLiefBest (BOOL b)
{
       WithLiefBest = b;

       if (WithLiefBest)
       {
           ChAttr = ChAttrlief_best;
           WithAkrz = FALSE;
       }
       else
       {
           if (ChAttr == ChAttrlief_best)
           {
                   ChAttr = ChAttra;
           }
       }
       eListe.SetChAttr (ChAttr); 
}

void LISTDLG::SetWithSearchBest (BOOL b)
{
       WithSearchBest = b;
}

void LISTDLG::SetWithAkrz (BOOL b)
{
       WithAkrz = b;

       if (WithAkrz)
       {
           ChAttr = ChAttra_krz;
           WithLiefBest = FALSE;
       }
       else
       {
           if (ChAttr == ChAttra_krz)
           {
                   ChAttr = ChAttra;
           }
       }
       eListe.SetChAttr (ChAttr); 
}
 
void LISTDLG::SetWithPosEk (BOOL b)
{
       WithPosEk = b;
}


void LISTDLG::GetSysPar (void)
{
       SYS_PAR_CLASS SysPar;

       strcpy (sys_par.sys_par_nam, "ls_charge_par");
       if (SysPar.dbreadfirst () == 0)
       {
                    ls_charge_par = atoi (sys_par.sys_par_wrt);
       }
       strcpy (sys_par.sys_par_nam, "lsc2_par");
       if (SysPar.dbreadfirst () == 0)
       {
                    lsc2_par = atoi (sys_par.sys_par_wrt);
       }
       strcpy (sys_par.sys_par_nam, "we_vk_par");
       if (SysPar.dbreadfirst () == 0)
       {

                 we_vk_par = atoi (sys_par.sys_par_wrt);
				 flg_vk_par = we_vk_par;
				 if (we_kopf.fil == 0) flg_vk_par = 0;     //z.Z nur editierbar bei Streckengesch�ft LuD 240406
       }
	   if (flg_vk_par > 0)
	   {
                SetFieldAttr ("pr_vk", EDIT);          
	   }




       SysPar.dbclose (); 
}

void LISTDLG::GetCfgValues (void)
{
       char cfg_v [512];
       COLORREF Color;

       if (ProgCfg == NULL) return;
	   if (cfgOK) return;

       cfgOK = TRUE;
       if (ProgCfg->GetCfgValue ("3DList", cfg_v) == TRUE)
       {
                    eListe.Set3D (atoi (cfg_v));
	   }
       if (ProgCfg->GetCfgValue ("3DSize", cfg_v) == TRUE)
       {
                    eListe.SetPlus3D (atoi (cfg_v));
	   }
       if (ProgCfg->GetCfgValue ("ListFocus", cfg_v) == TRUE)
       {
                    ListFocus = atoi (cfg_v);
	   }
        if (ProgCfg->GetCfgValue ("rowheight", cfg_v) == TRUE)
		{
			        RowHeight = ratod (cfg_v);
					if (RowHeight < (double) 1.0)
					{
						RowHeight = (double) 1.0;
					}
		}
        if (ProgCfg->GetCfgValue ("ubheight", cfg_v) == TRUE)
		{
			        UbHeight = atoi (cfg_v);
					SetUbHeight ();
		}
		eListe.SetRowHeight (RowHeight);


        if (ProgCfg->GetCfgValue ("NoRecNr", cfg_v) == TRUE)
        {
			        NoRecNr = atoi (cfg_v);
		            if (atoi (cfg_v))
					{
	                        SetNoRecNr ();
 		                    WeWork::ListDiff = 0;
					}
		}
        if (ProgCfg->GetCfgValue ("ListColor", cfg_v) == TRUE)
        {
                     GetListColor (&Color, cfg_v);
		             eListe.SetColor (Color);
        }
        if (ProgCfg->GetCfgValue ("ListBkColor", cfg_v) == TRUE)
        {
                     GetListColor (&Color, cfg_v);
		             eListe.SetBkColor (Color);
        }
        if (ProgCfg->GetCfgValue ("ListLine", cfg_v) == TRUE)
        {
		             eListe.SetLines (GetListLine (cfg_v));
        }
        if (ProgCfg->GetCfgValue ("ChargeMacro", cfg_v) == TRUE)
        {
                     if (strcmp (cfg_v, "NULL"))
                     {
                        ChargeMacro = new char [strlen (cfg_v) + 1];
                        if (ChargeMacro != NULL)
                        {
                          strcpy (ChargeMacro, cfg_v);
                        }
                     }
        }
        if (ProgCfg->GetCfgValue ("EditCharge", cfg_v) == TRUE)
        {
                     switch (atoi (cfg_v))
                     {
                     case 0 :
                          SetFieldAttr ("ls_charge", REMOVED);
                          break;
                     case 1:
                          SetFieldAttr ("ls_charge", DISPLAYONLY);
                          break;
                     case 2:
                          SetFieldAttr ("ls_charge", EDIT);             
                          break;
                     }
        }

        if (ProgCfg->GetCfgValue ("EditIdent", cfg_v) == TRUE)
        {
                     switch (atoi (cfg_v))
                     {
                     case 0 :
                          SetFieldAttr ("ls_ident", REMOVED);
                          break;
                     case 1:
                          SetFieldAttr ("ls_ident", DISPLAYONLY);
                          break;
                     case 2:
                          SetFieldAttr ("ls_ident", EDIT);
                          break;
                     }
        }
        if (ProgCfg->GetCfgValue ("abascharge", cfg_v) == TRUE)
        {
                     ABasCharge = atoi (cfg_v);
        }
        if (ProgCfg->GetCfgValue ("bsd_kz", cfg_v) == TRUE)
		{
			         bsd_kz = atoi (cfg_v);

		}
        if (ProgCfg->GetGroupDefault ("lager", cfg_v) == TRUE)
        {
                    akt_lager =  atoi (cfg_v);
		}
       if (ProgCfg->GetCfgValue ("TestKontrakt", cfg_v) == TRUE)
       {
                    TestKontraktMe = atoi (cfg_v);
	   }
        if (ProgCfg->GetCfgValue ("SearchLiefBest", cfg_v) == TRUE)
        {
                     WithSearchBest = atoi (cfg_v);
        }
}


void LISTDLG::TestRemoves (void)
{
        int fpos;

        fpos = GetItemPos (&DataForm, "ls_ident");
        if (fpos >= 0)
        {
            if (DataForm.mask[fpos].attribut & REMOVED)
            {
                DestroyListField ("ls_ident");
            }
        }
        fpos = GetItemPos (&DataForm, "ls_charge");
        if (fpos >= 0)
        {
            if (DataForm.mask[fpos].attribut & REMOVED)
            {
                DestroyListField ("ls_charge");
            }
        }
/*
        fpos = GetItemPos (&DataForm, "pr_ek");
        if (fpos >= 0)
        {
            if (DataForm.mask[fpos].attribut & REMOVED)
            {
                DestroyListField ("pr_ek");
            }
        }
        fpos = GetItemPos (&DataForm, "netto_ek");
        if (fpos >= 0)
        {
            if (DataForm.mask[fpos].attribut & REMOVED)
            {
                DestroyListField ("netto_ek");
            }
        }
*/
}


LISTDLG::LISTDLG () : KEYS (), LISTENTER () 
{

    ProgCfg = new PROG_CFG ("35100");
	hMenu = NULL;
    akt_lager = 0;
	Dlg = NULL;
	Emb = NULL;
    cfgOK = FALSE;
    WithLiefBest = NULL;
    WithAkrz = NULL;
	cursor = -1;
    TestKontraktMe = 1;
    bsd_kz = 1;

    LstTab = new LSTS [0x10000];

	SetBaseForms ();
	NoRecNrSet = FALSE;

    dataform  = &DataForm;
    ubform    = &UbForm;
    lineform  = &LineForm;

    eListe.SetUbRows (ubrows); 
	eListe.SetKeys (this);
    ls_charge_par = FALSE;
    lsc2_par = FALSE;
	we_vk_par = 0;
	flg_vk_par = 0;
    ABasCharge = FALSE;
    ChargeMacro = NULL;
    LISTENTER::GetCfgValues ();
    GetCfgValues ();

    GetSysPar ();
    TestRemoves ();
    if (NoRecNr)
    {
	    SetNoRecNr ();
		WeWork::ListDiff = 0;
    }
//    SetFieldLen ("netto_ek", 14);
	fChoise.Setchpos (0, 2);
    Work = NULL;
    ChAttr = ChAttra;
    eListe.SetChAttr (ChAttr); 
    Toolbar2 = NULL;
    strcpy (DefaultIdent,  "");
    strcpy (DefaultCharge, "");
    eListe.SetInfoProc (InfoProc);
    eListe.SetInfo (this);
    ArrDown = LoadBitmap (DLG::hInstance, "ARRDOWN");
    CArtikel.BkColor = GetSysColor (COLOR_3DFACE);
    InEnterPos = TRUE;
    ActiveListDlg = this;
}


LISTDLG::~LISTDLG ()
{
	if (Work != NULL)
	{
          Work->SetListDlg (NULL);
		  InEnterPos = FALSE;
}
    DestroyEmb ();
    fChoise.destroy ();
	if (LstTab != NULL)
	{
 	    delete []LstTab;
		LstTab = NULL;
	}
    if (ProgCfg != NULL)
    {
        ProgCfg->CloseCfg ();
        delete ProgCfg;
        ProgCfg = NULL;
    }
    if (ChargeMacro != NULL)
    {
        delete ChargeMacro;
        ChargeMacro = NULL;
    }
}


// Funktionen f�r den Zugriff auf die Daten. Sind als abstrakte Schnittstellen in
// listenter definiert.

unsigned char *LISTDLG::GetDataRec (void)
{
    return (unsigned char *) &Lsts;
}

int LISTDLG::GetDataRecLen (void)
{
    return sizeof (struct LSTS);
}

char *LISTDLG::GetListTab (int i)
{
    return (char *) &LstTab[i];
}

// Ende der Schnittstellen.


void LISTDLG::Seta_bz2Removed (BOOL removed)
{
    if (removed)
    {
            SetFieldAttr ("a_bz2", REMOVED);
    }
    else
    {
            SetFieldAttr ("a_bz2", DISPLAYONLY);
    }
}

void LISTDLG::Setlief_bestRemoved (BOOL removed)
{
    if (removed)
    {
            SetFieldAttr ("lief_best", REMOVED);
    }
    else
    {
            SetFieldAttr ("lief_best", DISPLAYONLY);
    }
}

void LISTDLG::Seta_krzRemoved (BOOL removed)
{
    if (removed)
    {
            SetFieldAttr ("a_krz", REMOVED);
    }
    else
    {
            SetFieldAttr ("a_krz", DISPLAYONLY);
    }
}

/*
void LISTDLG::Prepare (void)
{
	we_pos.mdn = we_kopf.mdn;
	we_pos.fil = we_kopf.fil;
	strcpy (we_pos.lief, we_kopf.lief);
	strcpy (we_pos.lief_rech_nr, we_kopf.lief_rech_nr);
	strcpy (we_pos.blg_typ, we_kopf.blg_typ);

	Wepos.sqlin ((short *) &we_pos.mdn, 1, 0);
	Wepos.sqlin ((short *) &we_pos.fil, 1, 0);
	Wepos.sqlin ((char *)  we_pos.lief, 0, 17);
	Wepos.sqlin ((char *)  we_pos.lief_rech_nr, 0, 17);
	Wepos.sqlin ((char *)  we_pos.blg_typ, 0, 2);

	Wepos.sqlout ((long *)   &we_pos.int_pos, 2, 0);
	Wepos.sqlout ((double *) &we_pos.a, 3, 0);

	cursor = Wepos.sqlcursor ("select int_pos, a from we_pos "
		                      "where mdn = ? "
							  "and   fil = ? "
							  "and   lief  = ? "
                              "and   lief_rech_nr = ? "
							  "and blg_typ = ? "
							  "order by int_pos,a");
}
*/

/*
void LISTDLG::Prepare (void)
{
	we_jour.mdn = we_kopf.mdn;
	we_jour.fil = we_kopf.fil;
	strcpy (we_jour.lief, we_kopf.lief);
	strcpy (we_jour.lief_rech_nr, we_kopf.lief_rech_nr);
	strcpy (we_jour.blg_typ, we_kopf.blg_typ);

	Wepos.sqlin ((short *) &we_jour.mdn, 1, 0);
	Wepos.sqlin ((short *) &we_jour.fil, 1, 0);
	Wepos.sqlin ((char *)  we_jour.lief, 0, 17);
	Wepos.sqlin ((char *)  we_jour.lief_rech_nr, 0, 17);
	Wepos.sqlin ((char *)  we_jour.blg_typ, 0, 2);

	Wepos.sqlout ((long *)   &we_jour.int_pos, 2, 0);
	Wepos.sqlout ((double *) &we_jour.a, 3, 0);

	cursor = Wepos.sqlcursor ("select int_pos, a from we_jour "
		                      "where mdn = ? "
							  "and   fil = ? "
							  "and   lief  = ? "
                              "and   lief_rech_nr = ? "
							  "and blg_typ = ? "
							  "order by int_pos,a");
}
*/

void LISTDLG::Prepare (void)
{
	we_jour.mdn = we_kopf.mdn;
	we_jour.fil = we_kopf.fil;
	strcpy (we_jour.lief, we_kopf.lief);
	strcpy (we_jour.lief_rech_nr, we_kopf.lief_rech_nr);
	strcpy (we_jour.blg_typ, we_kopf.blg_typ);

	Wejour.sqlin ((short *) &we_jour.mdn, 1, 0);
	Wejour.sqlin ((short *) &we_jour.fil, 1, 0);
	Wejour.sqlin ((char *)  we_jour.lief, 0, 17);
	Wejour.sqlin ((char *)  we_jour.lief_rech_nr, 0, 17);
	Wejour.sqlin ((char *)  we_jour.blg_typ, 0, 2);

	Wejour.sqlout ((long *)   &we_jour.int_pos, 2, 0);
	Wejour.sqlout ((double *) &we_jour.a, 3, 0);

	cursor = Wejour.sqlcursor ("select int_pos, a from we_jour "
		                      "where mdn = ? "
							  "and   fil = ? "
							  "and   lief  = ? "
                              "and   lief_rech_nr = ? "
							  "and blg_typ = ? "
							  "order by int_pos,a");
}

void LISTDLG::Open (void)
{
	if (cursor == -1)
	{
		Prepare ();
		if (cursor == -1) return;
	}

//	Wepos.sqlopen (cursor);
	Wejour.sqlopen (cursor);
}

int LISTDLG::Fetch (void)
{
	if (cursor == -1)
	{
		Prepare ();
		if (cursor == -1) return 100;
	}
/*
	if (Wepos.sqlfetch (cursor) != 0)
    {
         Wepos.dbreadfirst ();        
         return 100;
    }
	return Wepos.dbreadfirst ();
*/
	if (Wejour.sqlfetch (cursor) != 0)
    {
         Wejour.dbreadfirst ();        
         return 100;
    }
	return Wejour.dbreadfirst ();
}

void LISTDLG::Close (void)
{
	if (cursor != -1)
	{
		Wejour.sqlclose (cursor);
		cursor = -1;
	}
    Wejour.dbclose ();
}

void LISTDLG::TestEmb (void)
{
	    char buffer [80]; 

		DestroyEmb ();

	    if (Lsts.Emb)
		{
			sprintf (buffer, "Emballage / Anzahl Artikel : %d", Lsts.anz_emb);
			Emb = new EmbDlg (DLG::hInstance, mamain1, buffer);
		}
}

void LISTDLG::DestroyEmb ()
{
	     if (Emb != NULL)
		 {
             Emb->DestroyWindow (); 
			 delete Emb;
			 Emb = NULL;
		 }
}


void LISTDLG::TestCharge (void)
/**
Parameter fuer die Chargennummer bearbeiten.
**/
{

	    if (ls_charge_par == 0 || lsc2_par == 0)
		{
			return;
		}

        if (ABasCharge == FALSE) return;

		if (_a_bas.charg_hand == 0)
		{
                eListe.SetFieldAttr ("ls_charge", DISPLAYONLY);
		}
		else if (_a_bas.charg_hand == 1)
		{
                eListe.SetFieldAttr ("ls_charge", EDIT);
		}
		else if (_a_bas.charg_hand == 2)
		{
                eListe.SetFieldAttr ("ls_charge", DISPLAYONLY);
		}
}

void LISTDLG::TestIdentChargeDefaults (void)
{
        if (DataForm.mask [GetItemPos (&DataForm, "ls_ident")].attribut != REMOVED &&
            strcmp (DefaultIdent, "") > 0)
        {
            strcpy (Lsts.ls_ident, DefaultIdent);
        }
        if (DataForm.mask [GetItemPos (&DataForm, "ls_icharge")].attribut != REMOVED &&
            strcmp (DefaultCharge, "") > 0)
        {
            strcpy (Lsts.ls_charge, DefaultCharge);
        }
}


BOOL LISTDLG::ReadaDirect (void)
{
       int dsqlstatus;

       sysdate (Lsts.akv);
       Work->SetLsts (&Lsts);
       if (ratod (Lsts.a) == 0.0)
       {
           return TRUE;
       }
       memcpy (&lief_bzg, &lief_bzg_null, sizeof (struct LIEF_BZG));
       lief_bzg.mdn = we_kopf.mdn;
       lief_bzg.fil = we_kopf.fil;
       strcpy (lief_bzg.lief, we_kopf.lief);
       dsqlstatus = Work->ReadA (ratod (Lsts.a));
       if (dsqlstatus == 100)
       {
           return TRUE;
       }

      
       strcpy (Lsts.a_bz1, _a_bas.a_bz1);
       sprintf (Lsts.pr_ek, "%.3lf", Work->GetPrEkBto ());
       sprintf (Lsts.netto_ek, "%.3lf", Work->GetPrEk ());
       sprintf (Lsts.pr_fil_ek, "%.3lf", Work->GetPrFilEk ());
       sprintf (Lsts.pr_vk, "%.3lf", Work->GetPrVk ());
	   pr_vk_save = ratod(Lsts.pr_vk);
       strcpy  (Lsts.lief_best, lief_bzg.lief_best);
       TestIdentChargeDefaults ();
       return FALSE;
}



BOOL LISTDLG::Reada (void)
{
       int dsqlstatus;

       sysdate (Lsts.akv);
       Work->SetLsts (&Lsts);
	   if (!numeric (Lsts.a))
	   {
//		   SearchA.ReadA(Lsts.a);
//           QClass.searcha_direct (GetParent (GetParent (hMainWindow)),Lsts.a);
           QClass.searcha_direct (eListe.Getmamain3 (),Lsts.a);
	   }
       if (ratod (Lsts.a) == 0.0)
       {
           disp_mess ("Artikel-Nummer > 0 eingeben", 2);  
           eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
           eListe.ShowAktRow ();
           SetListFocus ();
           return TRUE;
       }
	   if (strlen(clipped(Lsts.lief_best)) == 0)
	   {
		if (HoleAnzLief_best(ratod(Lsts.a)) > 1)
		{
			ShowBzg (Lsts.a);
		}
	   }
       memcpy (&lief_bzg, &lief_bzg_null, sizeof (struct LIEF_BZG));
       lief_bzg.mdn = we_kopf.mdn;
       lief_bzg.fil = we_kopf.fil;
       strcpy (lief_bzg.lief, we_kopf.lief);
 	   dsqlstatus = Work->ReadA (ratod (Lsts.a),Lsts.lief_best);
       if (dsqlstatus == 100)
       {
           print_mess (2, "Artikel %.0lf nicht gefunden", ratod (Lsts.a));
       }

       if (dsqlstatus == -2)
       {
		   disp_mess ("Falscher Artikeltyp", 2);
       }

       if (dsqlstatus != 0)
       {
		   sprintf (Lsts.a, "%.0lf", 0.0);
           memcpy (&LstTab[eListe.GetAktRow ()], &Lsts, sizeof (struct LSTS));
           eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
           eListe.ShowAktRow ();
           SetListFocus ();
           return TRUE;
       }
 
       if (_a_bas.charg_hand == 1 || _a_bas.charg_hand == 9 || _a_bas.zerl_eti == 1)
	   {
            EnableMenuItem (hMenu,   IDM_EXTRA_DATA, MF_ENABLED);
	   }
	   else
	   {
            EnableMenuItem (hMenu,   IDM_EXTRA_DATA, MF_GRAYED);
	   }
       if (_a_bas.a > 0) EnableMenuItem (hMenu,   IDM_MHD_DATA, MF_ENABLED);
       if (Work->TestKontrakt (ratod (Lsts.a)) == -1)
       {
           eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
           eListe.ShowAktRow ();
           SetListFocus ();
           return TRUE;
       }

       LockWindowUpdate (eListe.Getmamain3 ());
	   TestEmb ();
       TestCharge ();
       strcpy (Lsts.a_bz1, _a_bas.a_bz1);
       sprintf (Lsts.pr_ek, "%lf", Work->GetPrEkBto ());
       sprintf (Lsts.netto_ek, "%lf", Work->GetPrEk ());
       sprintf (Lsts.pr_fil_ek, "%lf", Work->GetPrFilEk ());
       sprintf (Lsts.pr_vk, "%lf", Work->GetPrVk ());
		 pr_vk_save = ratod(Lsts.pr_vk);
       strcpy  (Lsts.lief_best, lief_bzg.lief_best);
	   sprintf  (Lsts.mhd, "%hd", _a_bas.hbk_ztr);
	   if (Lsts.inh_einh == 0.0) Lsts.inh_einh = 1.0;
       if (lief_bzg.me_kz[0] == '1')
       {
             sprintf (Lsts.pr_ek, "%.4lf", (double) ratod (Lsts.pr_ek) * 
                                                           Lsts.inh_einh);
             if (WeWork::pr_ek_einh != 0.0)
             {
                 sprintf (Lsts.pr_ek, "%.4lf", WeWork::pr_ek_einh);
             }
             Work->CalcEkBto ();
       }
       sprintf (Lsts.inh, "%.3lf", Lsts.inh_einh);
       TestIdentChargeDefaults ();
       memcpy (&LstTab[eListe.GetAktRow ()], &Lsts, sizeof (struct LSTS));
       eListe.ShowAktRow ();
       SetListFocus ();
       LockWindowUpdate (NULL);
       return FALSE;
}

int LISTDLG::HoleAnzLief_best (double art)
{
	static int cursor1 = -1;
	static int cursor2 = -1;
	static long anzahl;
	static double a;

	if (cursor == -1)
	{
	   Wejour.sqlin ((short *) &lief_bzg.mdn, 1, 0);
	   Wejour.sqlin ((short *) &lief_bzg.fil, 1, 0);
	   Wejour.sqlin ((char *) we_kopf.lief, 0, 17);
	   Wejour.sqlin ((double *) &a, 3, 0);
	   Wejour.sqlout ((long *) &anzahl, 2, 0);
	   cursor1 = Wejour.sqlcursor ("select count(*) from lief_bzg "
		                                 "where mdn = ? " 
						                 "and fil = ? "
						                 "and lief = ? "
						                 "and a = ?");

      Wejour.sqlin ((short *) &lief_bzg.mdn, 1, 0);
   	  Wejour.sqlin ((short *) &lief_bzg.fil, 1, 0);
	  Wejour.sqlin ((char *) we_kopf.lief, 0, 17);
	  Wejour.sqlin ((double *) &a, 3, 0);
	  Wejour.sqlout ((char *) &lief_bzg.lief_best, 0, 17);
	  cursor2 = Wejour.sqlcursor ("select lief_best from lief_bzg "
		                     "where mdn = ? " 
			                 "and fil = ? "
			                 "and lief = ? "
			                 "and a = ?");

	}

	a = art;
	lief_bzg.mdn = we_kopf.mdn;
	lief_bzg.fil = we_kopf.fil;
	anzahl = 0;
    
    Wejour.sqlopen (cursor1);
    Wejour.sqlfetch (cursor1);
    while (anzahl == 0)
       {
                  if (lief_bzg.fil > 0)
                  {
                      lief_bzg.fil = 0;
                  }
                  else if (lief_bzg.mdn > 0)
                  {
                      lief_bzg.mdn = 0;
                  }
                  else
                  {
                      break;
                  }
				  Wejour.sqlopen (cursor1);
				  Wejour.sqlfetch (cursor1);
       }
   	   if (anzahl == 1)
	   {
		    Wejour.sqlopen (cursor2);
			int dsql = Wejour.sqlfetch (cursor2);
		    if (dsql == 0) strcpy (Lsts.lief_best,lief_bzg.lief_best);
		}


	   return anzahl;
}


BOOL LISTDLG::Readlief_best (void)
{
       int dsqlstatus;

       memcpy (&lief_bzg, &lief_bzg_null, sizeof (struct LIEF_BZG));
       lief_bzg.mdn = we_kopf.mdn;
       lief_bzg.fil = we_kopf.fil;
       strcpy (lief_bzg.lief, we_kopf.lief);
       dsqlstatus = Work->ReadLief_best (Lsts.lief_best);
       if (dsqlstatus == 100)
       {
           print_mess (2, "Bestell-Nummer %s nicht gefunden", Lsts.lief_best);
           eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
           eListe.ShowAktRow ();
           SetListFocus ();
           return TRUE;
       }
       sprintf (Lsts.a, "%.0lf", lief_bzg.a);
       return Reada ();
}

BOOL LISTDLG::Reada_krz (void)
{
       int dsqlstatus;

       dsqlstatus = Work->ReadA_krz (atol (Lsts.a_krz));
       if (dsqlstatus == 100)
       {
           print_mess (2, "Artikelkurz-Nummer %ld nicht gefunden", atol (Lsts.a_krz));
           eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
           eListe.ShowAktRow ();
           SetListFocus ();
           return TRUE;
       }
       sprintf (Lsts.a, "%.0lf", lief_bzg.a);
       return Reada ();
}

BOOL LISTDLG::TestGenCharge (void)
{
       char *charge; 

       clipped (Lsts.ls_charge);
       if (_a_bas.charg_hand == 2 &&
             strcmp (Lsts.ls_charge, " ") <= 0)
       {
				charge = Work->GenCharge (hMainWindow, Lsts.ls_ident, ChargeMacro);
                if (charge != NULL)
                {
				        strcpy (Lsts.ls_charge, charge);
                }
                else
                {
                        disp_mess ("Charge kann nicht generiert werden", 2);
                }
                memcpy (&LstTab [eListe.GetAktRow ()], &Lsts, sizeof (struct LSTS));
                eListe.ShowAktRow ();
       }
       return FALSE;
}

BOOL LISTDLG::TestEkBto (void)
{
       Work->SetLsts (&Lsts);
       Work->CalcEkBto ();
       memcpy (&LstTab [eListe.GetAktRow ()], &Lsts, sizeof (struct LSTS));
       eListe.ShowAktRow ();
       return FALSE;
}

int LISTDLG::Schreibea_pr (void)
{
	if (we_vk_par > 1)
	{
		if (pr_vk_save != ratod(Lsts.pr_vk))
		{
			Work->Schreibea_pr();
		}
	}
	return 0;
}

BOOL LISTDLG::TestMe (void)
{
       double me;
/*
	   if (syskey == KEY9 || syskey == KEY5 ||
		   syskey == KEY12)
	   {
		   return FALSE;
	   }
*/

	   if (ratod (Lsts.a) > 0.0 && ratod (Lsts.me) == 0.0)
	   {
//		        disp_mess ("Menge > 0 eingeben", 2);
                eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
                eListe.ShowAktRow ();
                SetListFocus ();
				return TRUE;
	   }
		   
 	   sprintf (Lsts.pos_wert,  "%.4lf", ratod (Lsts.netto_ek) 
		                                 * ratod (Lsts.me));

	   if (Lsts.Emb) 
	   {
		        if (Lsts.anz_emb == 0) Lsts.anz_emb = 1;
		        me = ratod (Lsts.me) * Lsts.anz_emb;
				sprintf (Lsts.me, "%.3lf", me);
				DestroyEmb ();
				Lsts.anz_emb = 1;
				Lsts.Emb = FALSE;
	   }
       memcpy (&LstTab [eListe.GetAktRow ()], &Lsts, sizeof (struct LSTS));
       eListe.ShowAktRow ();
	   return FALSE;
}


BOOL LISTDLG::AfterCol (char *colname)
{

         if (strcmp (colname, "a") == 0)
         {
             return Reada ();
         }
         if (strcmp (colname, "lief_best") == 0)
         {
             return Readlief_best ();
         }
         if (strcmp (colname, "a_krz") == 0)
         {
             return Reada_krz ();
         }
         if (strcmp (colname, "ls_ident") == 0)
         {
             return TestGenCharge ();
         }
         if (strcmp (colname, "pr_ek") == 0)
         {
             return TestEkBto ();
         }
         if (strcmp (colname, "me_choise") == 0)
         {
             return ShowMeEinh ();
         }
         if (strcmp (colname, "pr_ek_choise") == 0)
         {
             return ShowLief ();  
         }
         if (strcmp (colname, "me") == 0)
         {
             return TestMe ();
         }
         if (strcmp (colname, "pr_vk") == 0)
         {
             return Schreibea_pr ();
         }
         return FALSE;
}

int LISTDLG::AfterRow (int Row)
{
         form * DataForm; 

         if (syskey == KEYUP && ratod (Lsts.a) == 0.0)
         {
                return OnRowDelete ();
         }
		 if (ratod(Lsts.pr_ek) > 9999.0)  //140111
		 {
			 disp_mess ("Ung�ltige Eingabe, Preis ist zu hoch", 2);
			 return -1;
		 }
		 if (ratod (Lsts.me) > 9999999.0)  //140111
		 {
			 disp_mess ("Ung�ltige Eingabe, Menge ist zu hoch", 2);
			 return -1;
		 }
		 if (ratod (Lsts.me) * ratod(Lsts.pr_ek)  > 9999999.0)  //140111
		 {
			 disp_mess ("Ung�ltige Eingabe, Preis * Menge ist zu hoch", 2);
			 return -1;
		 }

         memcpy (&LstTab [Row], &Lsts, sizeof (struct LSTS));
         Work->SetLsts (&Lsts);
         if (Work->UpdateKontrakt (ratod (Lsts.a)) == -1)
         {
                sprintf (Lsts.me, "%lf", Work->GetAktMe ());
                memcpy (&LstTab [Row], &Lsts, sizeof (struct LSTS));
                DataForm = eListe.GetDataForm ();
                int pos = GetItemPos (DataForm, "me");
                if (pos != -1)
                {
                     eListe.SetPos (eListe.GetAktRow (), pos);
                }
                eListe.ShowAktRow ();
                SetListFocus ();
                return -1;
         }
		 if (strcmp(AktChargennr,Lsts.ls_charge) != 0 || strcmp(AktIdentnr,Lsts.ls_ident) != 0)
		 {
	         Work->SetAktBsdMe (0.0);  //100209
	         Work->BucheBsd (ratod (Lsts.a),Lsts.lief_best, Work->GetAktMe() * -1, atoi (Lsts.me_einh),
                         ratod (Lsts.pr_ek) , AktChargennr, AktIdentnr,akt_lager);
		 }
         Work->BucheBsd (ratod (Lsts.a),Lsts.lief_best, ratod (Lsts.me), atoi (Lsts.me_einh),
                         ratod (Lsts.pr_ek), Lsts.ls_charge, Lsts.ls_ident,akt_lager);

         return 1;
}

int LISTDLG::BeforeRow (int Row)
{
         memcpy (&LstTab [Row], &Lsts, sizeof (struct LSTS));
         if (ratod (Lsts.a) != 0.0)
         {
               lese_a_bas (ratod (Lsts.a));
               TestCharge ();
         }
         strcpy(AktChargennr,Lsts.ls_charge);  //100209
         strcpy(AktIdentnr,Lsts.ls_charge);  //100209
         Work->SetAktMe (ratod (Lsts.me));
         Work->SetAktBsdMe (Work->GetMeVgl (
                       ratod (Lsts.a), Lsts.lief_best, ratod (Lsts.me), atoi (Lsts.me_einh)));
		 pr_vk_save = ratod(Lsts.pr_vk);
	     if (_a_bas.charg_hand == 1 || _a_bas.charg_hand == 9 || _a_bas.zerl_eti == 1)
		 {
            EnableMenuItem (hMenu,   IDM_EXTRA_DATA, MF_ENABLED);
		 }
	     else
		 {
            EnableMenuItem (hMenu,   IDM_EXTRA_DATA, MF_GRAYED);
		 }
       if (_a_bas.a > 0) EnableMenuItem (hMenu,   IDM_MHD_DATA, MF_ENABLED);
         return 1;
}

int LISTDLG::ShowA ()
{
        struct SA *sa;

        if (Modal)
        {  
               SearchA.SetWindowStyle (WS_POPUP | 
                                       WS_THICKFRAME |
                                       WS_CAPTION |
                                       WS_SYSMENU |
                                       WS_MINIMIZEBOX |
                                       WS_MAXIMIZEBOX);
               SearchA.SetParams (DLG::hInstance, hMainWindow);
        }
        else
        {  
               SearchA.SetWindowStyle (WS_POPUP | WS_THICKFRAME);
               SearchA.SetParams (DLG::hInstance, hMainWindow);
        }
//        SearchA.Setawin (eListe.GethMainWindow ());
        SearchA.Setawin (GetParent (GetParent (hMainWindow)));
        SearchA.SearchA ();
        if (Modal)
        {  
               EnableWindows (GetParent (eListe.GethMainWindow ()), FALSE);
        }
        if (syskey == KEY5)
        {
            SetListFocus ();
            return 0;
        }
        sa = SearchA.GetSa ();
        if (sa == NULL)
        {
            return 0;
        }

        strcpy (LstTab [eListe.GetAktRow ()].a, sa->a);
        eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
        eListe.ShowAktRow ();
        SetListFocus ();
        return 0;
}

int LISTDLG::ShowBzg ()
{
        struct SBZG *sbzg;
        SEARCHBZG SearchBzg; 
        char buffer [256];

        sprintf (buffer, "lief_bzg.lief = \"%s\" and (lief_bzg.mdn = %hd or lief_bzg.mdn = 0) "
			             "and (lief_bzg.fil = %hd or lief_bzg.fil = 0)", 
						 clipped (we_kopf.lief), we_kopf.mdn, we_kopf.fil);
        SearchBzg.SetQuery (buffer); 
        if (Modal)
        {  
               SearchBzg.SetWindowStyle (WS_POPUP | 
                                       WS_THICKFRAME |
                                       WS_CAPTION |
                                       WS_SYSMENU |
                                       WS_MINIMIZEBOX |
                                       WS_MAXIMIZEBOX);
               SearchBzg.SetParams (DLG::hInstance, hMainWindow);
        }
        else
        {  
               SearchBzg.SetWindowStyle (WS_POPUP | WS_THICKFRAME);
               SearchBzg.SetParams (DLG::hInstance, hMainWindow);
        }
//        SearchBzg.Setawin (eListe.GethMainWindow ());
        SearchBzg.Setawin (GetParent (GetParent (hMainWindow)));
        SearchBzg.Search (we_kopf.mdn);
        if (Modal)
        {  
               EnableWindows (GetParent (eListe.GethMainWindow ()), FALSE);
        }
        if (syskey == KEY5)
        {

            SetListFocus ();
            return 0;
        }
        sbzg = SearchBzg.GetSBzg ();
        if (sbzg == NULL)
        {
            return 0;
        }

        strcpy (LstTab [eListe.GetAktRow ()].a, sbzg->a);
        strcpy (LstTab [eListe.GetAktRow ()].lief_best, sbzg->lief_best);
        eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
        eListe.ShowAktRow ();
        SetListFocus ();
        return 0;
}
int LISTDLG::ShowBzg (char* ca)
{
        struct SBZG *sbzg;
        SEARCHBZG SearchBzg; 
        char buffer [256];

        sprintf (buffer, "lief_bzg.lief = \"%s\" and (lief_bzg.mdn = %hd or lief_bzg.mdn = 0) "
			             "and (lief_bzg.fil = %hd or lief_bzg.fil = 0)", 
						 clipped (we_kopf.lief), we_kopf.mdn, we_kopf.fil);
        SearchBzg.SetQuery (buffer); 
        if (Modal)
        {  
               SearchBzg.SetWindowStyle (WS_POPUP | 
                                       WS_THICKFRAME |
                                       WS_CAPTION |
                                       WS_SYSMENU |
                                       WS_MINIMIZEBOX |
                                       WS_MAXIMIZEBOX);
               SearchBzg.SetParams (DLG::hInstance, hMainWindow);
        }
        else
        {  
               SearchBzg.SetWindowStyle (WS_POPUP | WS_THICKFRAME);
               SearchBzg.SetParams (DLG::hInstance, hMainWindow);
        }
//        SearchBzg.Setawin (eListe.GethMainWindow ());
        SearchBzg.Setawin (GetParent (GetParent (hMainWindow)));
        SearchBzg.Search (we_kopf.mdn,ca);
        if (Modal)
        {  
               EnableWindows (GetParent (eListe.GethMainWindow ()), FALSE);
        }
        if (syskey == KEY5)
        {

            SetListFocus ();
            return 0;
        }
        sbzg = SearchBzg.GetSBzg ();
        if (sbzg == NULL)
        {
            return 0;
        }

        strcpy (LstTab [eListe.GetAktRow ()].a, sbzg->a);
        strcpy (LstTab [eListe.GetAktRow ()].lief_best, sbzg->lief_best);
        eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
        eListe.ShowAktRow ();
        SetListFocus ();
        return 0;
}


BOOL LISTDLG::PreTranslateMessage (MSG *msg)
{
     switch (msg->message)
     {
	 case WM_KEYDOWN:
		 if (msg->wParam == 'Z' || msg->wParam == 'z')
		 {
                  if (GetKeyState (VK_CONTROL) < 0)
				  {
					  OnExtraData ();
					  return TRUE;

				  }
		 }
		 if (msg->wParam == 'M' || msg->wParam == 'm')
		 {
                  if (GetKeyState (VK_CONTROL) < 0)
				  {
					  OnMHDData ();
					  return TRUE;

				  }
		 }
	 }
	 return FALSE;
}


BOOL LISTDLG::OnKey5 (void)
{
    if (abfragejn (hMainWindow, "Positionen speichern ?", "J"))
    {
        return OnKey12 ();
    }
	if (Emb !=NULL)
	{
		DestroyEmb ();
	}
	eListe.BreakList ();
	strcpy(Lsts.hbk_dat,""); //311009
	return TRUE;
}

BOOL LISTDLG::OnKey6 (void)
{
    eListe.InsertLine ();
    return TRUE;
}

BOOL LISTDLG::OnKeyInsert (void)
{
    eListe.InsertLine ();
    return TRUE;
}

BOOL LISTDLG::OnKey7 (void)
{
    eListe.DeleteLine ();
    return TRUE;
}

BOOL LISTDLG::OnKeyDel (void)
{
    if (GetKeyState (VK_CONTROL) < 0)
    {
          return OnRowDelete ();
    }
    return FALSE;
}


BOOL LISTDLG::OnRowDelete (void)
{
         Work->SetAktBsdMe (0.0);                  //100209
         Work->BucheBsd (ratod (Lsts.a),             //100209
							Lsts.lief_best,
                         ratod (Lsts.me) * -1,
                         atoi (Lsts.me_einh),
                         ratod (Lsts.pr_ek),
						 Lsts.ls_charge,
						 Lsts.ls_ident,akt_lager);

    eListe.DeleteLine ();
    return TRUE;
}


void LISTDLG::LadeExtraData (void) //Row 20.01.2009
{
	if (strlen(clipped(LISTDLG::Lsts.ls_ident)) == 0) return;
	int ListPos = eListe.GetAktRow ();
	ExtraDataItem = ExtraData.Find (ListPos);
 	if (ExtraDataItem != NULL)
	{
//			ExtraData.Drop (*ExtraDataItem);
		strcpy (ExtraDataItem->Data.IdentNr,LISTDLG::Lsts.ls_ident);
		strcpy (ExtraDataItem->Data.ESNr,lief.esnum);
		strcpy (ExtraDataItem->Data.EZNr,lief.eznum);
		ExtraData.Add (ExtraDataItem);

	}
	else
	{
		CWeExtraData NewExtraDataItem;
		NewExtraDataItem.ListPos = ListPos;
        if (Work->ReadExtraData ())
		{
			strcpy (NewExtraDataItem.Data.IdentNr,zerldaten.ident_extern);
			strcpy (NewExtraDataItem.Data.ESNr,zerldaten.esnum);
			strcpy (NewExtraDataItem.Data.EZNr,zerldaten.eznum1);
			Work->ReadCountries ();
			strcpy (NewExtraDataItem.Data.Countries,rind.ls_land);
		}
		else
		{
			strcpy (NewExtraDataItem.Data.IdentNr,LISTDLG::Lsts.ls_ident);
			strcpy (NewExtraDataItem.Data.ESNr,lief.esnum);
			strcpy (NewExtraDataItem.Data.EZNr,lief.eznum);
		}
		ExtraData.Add (NewExtraDataItem);

	}

}
void LISTDLG::LadeExtraData (int ListPos) //Row 20.01.2009
{
	if (strlen(clipped(LISTDLG::Lsts.ls_ident)) == 0) return;
	ExtraDataItem = ExtraData.Find (ListPos);
	if (ExtraDataItem != NULL)
	{
//			ExtraData.Drop (*ExtraDataItem);
		strcpy (ExtraDataItem->Data.IdentNr,LISTDLG::Lsts.ls_ident);
		strcpy (ExtraDataItem->Data.ESNr,lief.esnum);
		strcpy (ExtraDataItem->Data.EZNr,lief.eznum);
		ExtraData.Add (ExtraDataItem);

	}
	else
	{
		CWeExtraData NewExtraDataItem;
		NewExtraDataItem.ListPos = ListPos;
        if (Work->ReadExtraData ())
		{
			strcpy (NewExtraDataItem.Data.IdentNr,zerldaten.ident_extern);
			strcpy (NewExtraDataItem.Data.ESNr,zerldaten.esnum);
			strcpy (NewExtraDataItem.Data.EZNr,zerldaten.eznum1);
			Work->ReadCountries ();
			strcpy (NewExtraDataItem.Data.Countries,rind.ls_land);
		}
		else
		{
			strcpy (NewExtraDataItem.Data.IdentNr,LISTDLG::Lsts.ls_ident);
			strcpy (NewExtraDataItem.Data.ESNr,lief.esnum);
			strcpy (NewExtraDataItem.Data.EZNr,lief.eznum);
		}
		ExtraData.Add (NewExtraDataItem);

	}
}

BOOL LISTDLG::OnExtraData ()
{

	if (ratod (Lsts.a) == 0.0)
	{
		return FALSE;
	}
	if (_a_bas.charg_hand != 1 && _a_bas.charg_hand != 9 && _a_bas.zerl_eti != 1)
	{
			 return FALSE;
	}
	int ListPos = eListe.GetAktRow ();
	ExtraDataItem = ExtraData.Find (ListPos);
	if (ExtraDataItem != NULL)
	{
		ExtraDataDialog.SetData (&ExtraDataItem->Data);		
		if (!ExtraDataDialog.DoModal ())
		{
			ExtraData.Drop (*ExtraDataItem);
		}
	}
	else
	{
		CWeExtraData NewExtraDataItem;
		NewExtraDataItem.ListPos = ListPos;
		ExtraDataDialog.SetData (&NewExtraDataItem.Data);		
		if (ExtraDataDialog.DoModal ())
		{
			ExtraData.Add (NewExtraDataItem);
		}
	}
	ReadExtraData();

	return TRUE;
}

BOOL LISTDLG::OnKey8 (void)
/*
Standardauftrag.
*/
{
         double a;
         double me;
         double pr_vk;
//         double pr_nto;
         int ret;
         int row;
         int lrow;
		 int SaveRow, SaveColumn;
		 STDAC *Stdac = NULL;
		 struct STD_A *stda;


         if (strcmp (DataForm.mask[eListe.GetAktColumn ()].item->GetItemName (), "a") != 0) 		
         {
             return FALSE;
         }

         SaveRow     = eListe.GetAktRow ();
		 SaveColumn  = eListe.GetAktColumn ();

         EnableWindows (eListe.Getmamain3 (), FALSE); 
	     Stdac = new STDAC;
		 ret = Stdac->Show (hMainWin,
                                    we_kopf.mdn, we_kopf.fil,
                                    we_kopf.lief);
         EnableWindows (eListe.Getmamain3 (), TRUE); 
         UpdateWindow (eListe.Getmamain3 ());
         LockWindowUpdate (eListe.Getmamain3 ());
         if (ret == 0)
         {
		         eListe.SetPos (SaveRow, eListe.FirstColumn ());
                 eListe.ShowAktRow ();
 	             if (Stdac) delete Stdac; 
                 LockWindowUpdate (NULL);
                 return 0;
         }

		 if (ret == 1)
		 {
				  eListe.DestroyFocusWindow ();
                  InvalidateRect (eListe.Getmamain3 (), 0, TRUE);
                  UpdateWindow (eListe.Getmamain3 ());
                  row = 0;
                  lrow = eListe.GetAktRow ();
                  while (stda = Stdac->GetStda (row))
                  {
					    if (stda->nstatus == 0)
						{
							row ++;
							continue;
						}
					    a     = ratod (stda->a);
					    me    = ratod (stda->me);
					    pr_vk = ratod (stda->pr_vk);

                        sprintf (Lsts.a,  "%.0lf", a);
	                    if ((ratod (Lsts.a) != 0.0) ||
                            lrow >= eListe.GetRecanz ())
                        {
							  eListe.SetRecanz (lrow);
                        }
                        sprintf (Lsts.me, "%.3lf", me); 
                        row ++;
                        sprintf (Lsts.pr_ek, "%.2lf", pr_vk); 
                        ReadaDirect ();
                        memcpy (&LstTab [lrow], &Lsts,
                                sizeof (struct LSTS));
                        lrow ++;
                  }
                  if (ratod (LstTab[lrow].a) ||
                           lrow >= eListe.GetRecanz ())
                  {
							  eListe.SetRecanz (lrow);
                  }
                  AppendLine ();
                  syskey = KEYUP;
                  eListe.FocusUp ();
                  InvalidateRect (eListe.Getmamain3 (), 0, TRUE);
         }

		 else if (ret == 2)
		 {
				  eListe.DestroyFocusWindow ();
                  InvalidateRect (eListe.Getmamain3 (), 0, TRUE);
                  UpdateWindow (eListe.Getmamain3 ());
                  row = 0;
                  lrow = eListe.GetAktRow ();
                  while (stda = Stdac->GetStda (row))
                  {
					    a     = ratod (stda->a);
					    me    = ratod (stda->me);
					    pr_vk = ratod (stda->pr_vk);

                        sprintf (Lsts.a,  "%.0lf", a);
	                    if ((ratod (Lsts.a) != 0.0) ||
                            lrow >= eListe.GetRecanz ())
                        {
							  eListe.SetRecanz (lrow);
                        }
                        sprintf (Lsts.me, "%.3lf", me); 
                        row ++;
                        sprintf (Lsts.pr_ek, "%.2lf", pr_vk); 
                        ReadaDirect ();
                        memcpy (&LstTab [lrow], &Lsts,
                                sizeof (struct LSTS));
                        lrow ++;
                  }
                  AppendLine ();
                  syskey = KEYUP;
                  eListe.FocusUp ();
                  InvalidateRect (eListe.Getmamain3 (), 0, TRUE);
         }
         memcpy (&Lsts, &LstTab [SaveRow], sizeof (struct LSTS));

         eListe.ShowAktRow ();
 	     if (Stdac) delete Stdac; 
          LockWindowUpdate (NULL);
         return 0;
}


BOOL LISTDLG::OnKey9 (void)
{
    char * Item = eListe.GetAktItem ();
    if (strcmp (Item, "a") == 0)
    {
            if (WithSearchBest)
			{
               ShowBzg ();
			}
            else
			{
               ShowA ();
			}
//            ShowA ();
            return TRUE;
    }
    else if (strcmp (Item, "lief_best") == 0)
    {
               ShowBzg ();
	}
    else if (strcmp (Item, "me") == 0)
    {
            ShowMeEinh ();
            return TRUE;
    }
    else if (strcmp (Item, "pr_ek") == 0)
    {
            ShowLief ();
            return TRUE;
    }
    return 0;
}


BOOL LISTDLG::ShowMeEinh (void)
{
	syskey = KEY9; 
    if (ratod (Lsts.a) == 0.0)
    {
           disp_mess ("Artikel-Nummer > 0 eingeben", 2);  
           eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
           eListe.ShowAktRow ();
           SetListFocus ();
           return FALSE;
    }
    Work->SetLsts (&Lsts);
    Work->ChMeEinh (NULL);

    memcpy (&LstTab[eListe.GetAktRow ()], &Lsts, sizeof (struct LSTS));
    eListe.ShowAktRow ();
    SetListFocus ();

	return TRUE;
}

BOOL LISTDLG::OnKey10 (void)
{
    if (Dlg != NULL)
    {
		 if (ratod (Lsts.a) == 0.0)
		 {
			 return FALSE;
		 }
         if (atoi (Lsts.p_nr) == 0)
         {
             sprintf (Lsts.p_nr, "%d", eListe.GetAktRow ());
         }
         Work->SetLsts (&Lsts);
	     ((WeDlg *) Dlg)->EnterQmPos (eListe.Getmamain3 ());
         SetListFocus ();
         return TRUE;
	}
    return FALSE;
}

BOOL LISTDLG::OnKey11 (void)
{
    EnterF PosEK;
    char PosEk [12];

    if (WithPosEk == FALSE)
    {
         return FALSE;
    }
    if (Dlg != NULL)
    {
         if (ratod (Lsts.me) == 0,0)
         {
             return FALSE;
         }
         strcpy (PosEk, ""); 
         PosEK.EnterText (mamain1, "Positions-EK ", PosEk, 11, "8.3lf");
         if (syskey != KEY5)
         {
             sprintf (Lsts.netto_ek, "%.3lf", ratod (PosEk) / ratod (Lsts.me));
             memcpy (&LstTab[eListe.GetAktRow ()], &Lsts, sizeof (struct LSTS));
             eListe.ShowAktRow ();
         }
         SetListFocus ();
         return TRUE;
	}
    return FALSE;
}

BOOL LISTDLG::OnMHDData (void)
{
    EnterF PosMHD;
    char cMHD [12];
	long ldat = 0;

    if (Dlg != NULL)
    {
         if (ratod (Lsts.me) == 0,0)
         {
             return FALSE;
         }
         strcpy (cMHD, Lsts.hbk_dat); 
         while (syskey != KEY5)
         {
	         PosMHD.EnterText (mamain1, "MHD Datum", cMHD, 11, "");
			 ldat = dasc_to_long (cMHD);
			 if (ldat <= 0) continue;
			 dlong_to_asc(ldat, cMHD);
	         strcpy (Lsts.hbk_dat,cMHD); 
             memcpy (&LstTab[eListe.GetAktRow ()], &Lsts, sizeof (struct LSTS));
             eListe.ShowAktRow ();
			 break;
         }
         SetListFocus ();
         return TRUE;
	}
    return FALSE;
}

int LISTDLG::ToMemory (int i)
{

    LISTENTER::ToMemory (i);
    LadeExtraData (i);

//    memcpy (&LstTab [i], &Lsts, sizeof (LSTS));
    return 0;
}

void LISTDLG::StartList (void)
{
    if (atol (we_kopf.koresp_best0) == 0l)
    {
		DelEListField ("best_me");
    }
}


void LISTDLG::InitRow (void)
{

    LISTENTER::InitRow ();


    Work->SetLsts (&Lsts);
    Work->InitRow ();


    if (ArrDown != NULL)
    {
          Lsts.me_choise.SetBitMap (ArrDown);
    }
    if (ArrDown != NULL)
    {
          Lsts.pr_ek_choise.SetBitMap (ArrDown);
    }
}

void LISTDLG::uebertragen (void)
{
//    ZeroMemory (&Lsts, sizeof struct (LSTS));
    Work->SetLsts (&Lsts);
    Work->FillRow ();

    if (ArrDown != NULL)
    {
         Lsts.me_choise.SetBitMap (ArrDown);
    }
    if (ArrDown != NULL)
    {
         Lsts.pr_ek_choise.SetBitMap (ArrDown);
    }
    if (strcmp (clipped (Lsts.ls_ident), " ") <= 0 &&
        strcmp (DefaultIdent, " ") > 0)
    {
        strcpy (Lsts.ls_ident, DefaultIdent);
    }
    if (strcmp (clipped (Lsts.ls_charge), " ") <= 0 &&
        strcmp (DefaultCharge, " ") > 0)
    {
        strcpy (Lsts.ls_charge, DefaultCharge);
    }
}

BOOL LISTDLG::PruefeRec (int i)
{
    Work->SetLsts (&LstTab[i]);
    if (ratod (LstTab[i].me) == 0.0)
    {
        return FALSE;
    }
	return TRUE;
}

void LISTDLG::ReadExtraData (int ListPos)
{
	ExtraDataItem = ExtraData.Find (ListPos);
	if (ExtraDataItem == NULL) 
	{
		return;
	}

//    SCharge *Charge = ExtraDataItem->Data.Get (0);
    Work->WriteExtraData (&ExtraDataItem->Data);
}
void LISTDLG::ReadExtraData (void)   //Detlef 011208 
{
	int ListPos = eListe.GetAktRow ();
	ExtraDataItem = ExtraData.Find (ListPos);
	if (ExtraDataItem == NULL) 
	{
		return;
	}
	strcpy (LISTDLG::Lsts.ls_ident, ExtraDataItem->Data.IdentNr);
	strcpy (LISTDLG::LstTab[ListPos].ls_ident, ExtraDataItem->Data.IdentNr);


}

void LISTDLG::WriteRec (int i)
{
    Work->SetLsts (&LstTab[i]);
    if (ratod (LstTab[i].me) == 0.0 && flgLoesch0 == TRUE)
    {
        return;
    }

/*
    Work->WriteWePos (&Wepos, i);
    we_kopf.fil_ek_wrt += we_pos.pr_fil_ek * we_pos.me * we_pos.inh;
    we_kopf.fil_ek_wrt_eu += we_pos.pr_fil_ek_eu * we_pos.me * we_pos.inh;
    we_kopf.vk_wrt += we_pos.pr_vk * we_pos.me * we_pos.inh;
    we_kopf.vk_wrt_eu += we_pos.pr_vk_eu * we_pos.me * we_pos.inh;
*/
//    we_kopf.rab_eff += we_pos.rab_eff * we_pos.me;
//    we_kopf.rab_eff_eu += we_pos.rab_eff_eu * we_pos.me;

	if (we_jour.inh == 0.0) we_jour.inh = 1.0;
	ReadExtraData (i);
    Work->WriteWeJour (&Wejour, i);
    we_kopf.fil_ek_wrt += we_jour.pr_fil_ek * we_jour.me * we_jour.inh;
    we_kopf.fil_ek_wrt_eu += we_jour.pr_fil_ek_eu * we_jour.me * we_jour.inh;
    we_kopf.vk_wrt += we_jour.pr_vk * we_jour.me * we_jour.inh;
    we_kopf.vk_wrt_eu += we_jour.pr_vk_eu * we_jour.me * we_jour.inh;
}

BOOL LISTDLG::OnKey12 (void)
{
    int i;
    int recanz;
    int dsqlstatus;
	char text [256];

    if (AfterRow (eListe.GetAktRow ()) == -1)
    {
        return TRUE;
    }
    if (ratod (Lsts.a) == 0.0)
    {
        OnRowDelete ();
    }
    else
    {
        memcpy (&lief_bzg, &lief_bzg_null, sizeof (struct LIEF_BZG));
        lief_bzg.mdn = we_kopf.mdn;
        lief_bzg.fil = we_kopf.fil;
        strcpy (lief_bzg.lief, we_kopf.lief);
        dsqlstatus = Work->ReadA (ratod (Lsts.a));
        if (dsqlstatus == 100)
        {
              OnRowDelete ();
        }
    }
	int danz0 = 0;
    recanz = eListe.GetRecanz ();
    for (i = 0; i < recanz; i ++)
    {
        if (PruefeRec (i) == FALSE) danz0++;
    }
	flgLoesch0 = FALSE;
	if (danz0 > 0)
	{
		sprintf (text,"Es gibt %d Position(en) mit Menge 0\nSollen diese gel�scht werden ?",danz0);
	    if (abfragejn (hMainWindow, text, "J"))
		{
			flgLoesch0 = TRUE;
		}
	}


    Work->DeleteAllPos (we_kopf.mdn, 
                        we_kopf.fil, 
                        we_kopf.lief, 
                        we_kopf.lief_rech_nr, 
                        we_kopf.blg_typ);
    we_kopf.rab_eff = 0.0;
    we_kopf.rab_eff_eu = 0.0;
    we_kopf.fil_ek_wrt = 0.0;
    we_kopf.fil_ek_wrt_eu = 0.0;
    we_kopf.vk_wrt = 0.0;
    we_kopf.vk_wrt_eu = 0.0;
    for (i = 0; i < recanz; i ++)
    {
        WriteRec (i);
    }
	if (Emb !=NULL)
	{
		DestroyEmb ();
	}
	eListe.BreakList ();
	strcpy(Lsts.hbk_dat,""); //311009
    return TRUE;
}


BOOL LISTDLG::OnKeyUp (void)
{
    form *DataForm;
    int fpos;

    if (ratod (Lsts.a) == 0.0)
    {
        return OnRowDelete ();
    }

	/* 100406
    if (ratod (Lsts.me) == 0.0)
    {
        disp_mess ("Menege > 0 eingeben", 2);
        eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
        eListe.ShowAktRow ();
        SetListFocus ();
        return TRUE;
    }
	*/

    if (strcmp (eListe.GetAktItem (), "ls_charge") == 0)
    {
        DataForm = eListe.GetDataForm ();
        fpos = GetItemPos (DataForm, "ls_ident");
        if (fpos == -1)
        {
                eListe.SetPos (eListe.GetAktRow (), GetItemPos (DataForm, "me"));
        }
        else if (DataForm->mask[fpos].attribut != EDIT)
        {
                eListe.SetPos (eListe.GetAktRow (), GetItemPos (DataForm, "me"));
        }
        else
        {
                eListe.SetPos (eListe.GetAktRow (), GetItemPos (DataForm, "ls_ident"));
        }
    }
    return FALSE;
}

BOOL LISTDLG::OnKeyDown (void)
{
    form *DataForm;
    int fpos;

    if (ratod (Lsts.a) == 0.0)
    {
        return TRUE;
    }

	/* 100406
    if (ratod (Lsts.me) == 0.0)
    {
        disp_mess ("Menege > 0 eingeben", 2);
        eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
        eListe.ShowAktRow ();
        SetListFocus ();
        return TRUE;
    }
	*/

    if (strcmp (eListe.GetAktItem (), "ls_charge") == 0)
    {
        DataForm = eListe.GetDataForm ();
        fpos = GetItemPos (DataForm, "ls_ident");
        if (fpos == -1)
        {
                eListe.SetPos (eListe.GetAktRow (), GetItemPos (DataForm, "me"));
        }
        else if (DataForm->mask[fpos].attribut != EDIT)
        {
                eListe.SetPos (eListe.GetAktRow (), GetItemPos (DataForm, "me"));
        }
        else
        {
                eListe.SetPos (eListe.GetAktRow (), GetItemPos (DataForm, "ls_ident"));
        }
    }
    return FALSE;
}


void LISTDLG::CreateListQuikInfos (HWND hWnd)
{
       if (Toolbar2 == NULL) return;

       Toolbar2->GetCfield ("f5")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("f12")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("f9")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("del")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("insert")->CreateQuikInfos (hWnd);
}


BOOL LISTDLG::OnCommand (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    if (LOWORD (wParam) == IDM_CHOISE)
    {
        return OnKey9 ();
    }
    if (LOWORD (wParam) == IDM_CANCEL)
    {
        return OnKey5 ();
    }
    if (LOWORD (wParam) == IDM_WRITE)
    {
        return OnKey12 ();
    }
    if (LOWORD (wParam) == VK_DELETE)
    {
        return OnKeyDel ();
    }
    if (LOWORD (wParam) == IDM_DELETE)
    {
        return OnRowDelete ();
    }
    if (LOWORD (wParam) == IDM_EXTRA_DATA)
    {
        return OnExtraData ();
    }
    if (LOWORD (wParam) == IDM_MHD_DATA)
    {
        return OnMHDData ();
    }
    if (LOWORD (wParam) == VK_INSERT)
    {
        return OnKeyInsert ();
    }
    if (LOWORD (wParam) == IDM_INSERT)
    {
        return OnKey6 ();
    }
    if (LOWORD (wParam) == IDM_QM)
    {
        return OnKey10 ();
    }
    if (LOWORD (wParam) == IDM_EPOSEK)
    {
        return OnKey11 ();
    }
    if (HIWORD (wParam)  == CBN_CLOSEUP)
	{
       if (Dlg != NULL)
	   {
	           return ((WeDlg *) Dlg)->OnCloseUp (hWnd, msg, wParam, lParam);
	   }
	}
    return FALSE;
}

BOOL LISTDLG::OnPaint (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    HDC hdc;

    if (Modal == FALSE) return FALSE;
    if (this->mamain1 != hWnd) return FALSE; 
    
    hdc = BeginPaint (hWnd, &ps);
    fChoise.display (hWnd, hdc);
    EndPaint (hWnd,&ps);
    return FALSE;
}

BOOL LISTDLG::OnMove (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{

    if (Modal == FALSE) return FALSE;
    if (this->mamain1 != hWnd) return FALSE; 

    fChoise.MoveWindow ();
    return FALSE;
}



int LISTDLG::ShowLief (void)
/**
Lieferanten zum Artikel anzeigen.
**/
{
    LIEFAC *Liefac;
	char liefc [17];
	int ret;

	if (ratod (LISTDLG::Lsts.a) == 0.0)
	{
		disp_mess ("Keine Artikel-Nummer vorhanden", 2);
        return 0;
	}

	WeWork::FuellePreise_we(AnzTagePr);
//    EnableWindows (eListe.Getmamain3 (), FALSE); 
	Liefac = new LIEFAC;
    EnableWindow (hMainWindow, FALSE);
    ret = Liefac->Show (GetParent (GetParent (hMainWindow)), we_kopf.mdn, we_kopf.fil, ratod (LISTDLG::Lsts.a), liefc,AnzTagePr);
//    EnableWindows (eListe.Getmamain3 (), TRUE); 
    EnableWindow (hMainWindow, TRUE);
	delete Liefac;
//    GetCurrentCfield ()->SetFocus ();
	return 1;
}


int LISTDLG::AfterBzgPressed (void)
{
       int dsqlstatus;
	   WeWork *Work;
//	   CFORM *fWork;

	   if (syskey == KEY5) return FALSE;
	   if (syskey == KEYUP) return FALSE;
	   if (syskey == KEY7) return FALSE;

       if (ratod (Lsts.a) == 0.0)
	   {
		   return FALSE;
	   }
	   Work = ActiveListDlg->GetWork ();
       Work->SetLsts (&Lsts);
//	   fWork = &fWeLst0;
       sysdate (Lsts.akv);
//       Work->SetLsts (&LISTDLG::Lsts);
       memcpy (&lief_bzg, &lief_bzg_null, sizeof (struct LIEF_BZG));
       lief_bzg.mdn = we_kopf.mdn;
       lief_bzg.fil = we_kopf.fil;
       strcpy (lief_bzg.lief, we_kopf.lief);
       dsqlstatus = Work->ReadA (ratod (Lsts.a));

       if (dsqlstatus != 0)
       {
           return TRUE;
       }
       sprintf (Lsts.pr_ek, "%.4lf", Work->GetPrEkBto ());
       sprintf (Lsts.netto_ek, "%.4lf", Work->GetPrEk ());
       sprintf (Lsts.pr_fil_ek, "%.4lf", Work->GetPrFilEk ());
       sprintf (Lsts.pr_vk, "%.4lf", Work->GetPrVk ());
		 pr_vk_save = ratod(Lsts.pr_vk);
       strcpy  (Lsts.lief_best, lief_bzg.lief_best);
       if (lief_bzg.me_kz[0] == '1')
       {
             sprintf (Lsts.pr_ek, "%.4lf", (double) ratod (Lsts.pr_ek) *
                                                                    Lsts.inh_einh);
             if (WeWork::pr_ek_einh != 0.0)
             {
                 sprintf (Lsts.pr_ek, "%.4lf", WeWork::pr_ek_einh);
             }
             Work->CalcEkBto ();
       }
       sprintf (Lsts.inh, "%.3lf", Lsts.inh_einh);
           memcpy (&LstTab[ActiveListDlg->GetAktRow ()], &Lsts, sizeof (struct LSTS));
           ActiveListDlg->SetListFocus ();

//	   fPos.SetText ();
//       memcpy (&LISTDLG::LstTab[ActiveWeDlgLst->GetAktRow ()],
//		                 &LISTDLG::Lsts, sizeof (LISTDLG::Lsts));
//	   ActiveWeDlgLst->FillRow (ActiveWeDlgLst->GetAktRow ());
//	   fWork->SetText ();
//     fPos.SetCurrentName ("me");
       return FALSE;
}



