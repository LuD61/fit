#include <windows.h>
#include <stdio.h>
#include "strfkt.h"
#include "stdfkt.h"
#include "abschlwork.h"
#include "sys_par.h"
#include "Text.h"

#define CString Text

BOOL AbschlWork::Weldr = TRUE;

void AbschlWork::FillMwstSlCombo (char **Combo, int plus, int anz)
{
    PTAB_CLASS Ptab;
    int dsqlstatus;
    int i = 0;
	CString Item = "mwst";
	long stichtag;


	if (Stichtag ())
	{
		stichtag = dasc_to_long (tag);
		if (we_kopf.we_dat < stichtag)
		{
			Item = "mwst_alt";
		}
		else
		{
			Item = "mwst_neu";
		}
	}
    dsqlstatus = Ptab.lese_ptab_all (Item.GetBuffer ());

    while (dsqlstatus == 0)
    {
        if (Combo[i] == NULL) break;
        sprintf (Combo[i], "%d   (%s)", atoi (ptabn.ptwert) + plus, clipped (ptabn.ptbezk));
        dsqlstatus = Ptab.lese_ptab_all ();
        i ++;
        if (i == anz) break;
    }
    Combo[i] = NULL;
}

BOOL AbschlWork::Stichtag (void)
{
	if (!stichtagOK)
	{
		SYS_PAR_CLASS Syspar;

		strcpy (sys_par.sys_par_nam,"stichtag");
		if (Syspar.dbreadfirst () == 0)
		{
			stichtag = atoi (sys_par.sys_par_wrt); 
			strcpy (tag, sys_par.sys_par_besch);
			clipped (tag);
		}
		stichtagOK = TRUE;
	}
	return stichtag;
}

int AbschlWork::GetMwstAnz (void)
{
    PTAB_CLASS Ptab;
    int dsqlstatus;

	CString Item = "mwst";
	long stichtag;


	if (Stichtag ())
	{
		stichtag = dasc_to_long (tag);
		if (we_kopf.we_dat < stichtag)
		{
			Item = "mwst_alt";
		}
		else
		{
			Item = "mwst_neu";
		}
	}
    ptabk.ptanzpos = 0;
    dsqlstatus = Ptab.lese_ptab_all (Item.GetBuffer ());
    return ptabk.ptanzpos;
}

double AbschlWork::GetMwstFaktor (int mwst)
{
    PTAB_CLASS Ptab;
    int dsqlstatus;
    char wert [5];
    double faktor = 1.0;

	CString Item = "mwst";
	long stichtag;


	if (Stichtag ())
	{
		stichtag = dasc_to_long (tag);
		if (we_kopf.we_dat < stichtag)
		{
			Item = "mwst_alt";
		}
		else
		{
			Item = "mwst_neu";
		}
	}
    sprintf (wert, "%d", mwst);
    dsqlstatus = Ptab.lese_ptab (Item.GetBuffer (), wert);
    if (dsqlstatus == 0)
    {
        faktor = ratod (ptabn.ptwer1);
        if (faktor == 0.0) 
        {
            faktor = ratod (ptabn.ptbezk) / 100;
        }
    }
    return faktor;
}

void AbschlWork::WriteWeKopf (void)
{
    WE_KOPF_CLASS WeKopf;
    SYS_PAR_CLASS Syspar;
    BOOL kreditor = TRUE;

    strcpy (sys_par.sys_par_nam,"kreditor_par");
    if (Syspar.dbreadfirst () == 0)
    {
        kreditor = atoi (sys_par.sys_par_wrt); 
    }
    we_kopf.lief_wrt_nto = we_kopf.lief_wrt_nto_eu * 1.0;
    we_kopf.zusch        = we_kopf.zusch_eu * 1.0;
    we_kopf.lief_zusch   = we_kopf.lief_zusch_eu * 1.0;
    we_kopf.ink_zusch    = we_kopf.ink_zusch_eu * 1.0;
    we_kopf.mwst_ges     = we_kopf.mwst_ges_eu * 1.0;


    if (we_kopf.anz_mang > 0)
    {
          we_kopf.we_status    = 3;
    }
    else
    {
          we_kopf.we_status    = 4;
    }

    if (kreditor)
    {
          we_kopf.lief_s = atol (we_kopf.lief);
    }

    if (Weldr)
    {
// Mapnahmen f�r weldr351
          we_kopf.we_status    = 0;
    }

    WeKopf.dbupdate ();

/*
    rosipath = getenv ("RSDIR");
    if (rosipath == NULL) return;
    sprintf (progname, "%s\\BIN\\rswrun weldr L "
                                     "%hd %hd %s %s K",
                                     rosipath,
                                     we_kopf.mdn, we_kopf.fil, we_kopf.lief,
									 we_kopf.lief_rech_nr);

    ProcWaitExecEx (progname, SW_SHOWMINIMIZED, 0, 40, -1, 0);
*/
}

double AbschlWork::CalcVSteuer (void)
{
    double vs = 0.0;
    double mwst;
    char wert [5];
    int cursor;
	CString Item = "mwst";
	long stichtag;


	if (Stichtag ())
	{
		stichtag = dasc_to_long (tag);
		if (we_kopf.we_dat < stichtag)
		{
			Item = "mwst_alt";
		}
		else
		{
			Item = "mwst_neu";
		}
	}

    if (lief.steuer_kz[0] != 'V')
    {
        return vs;
    }


    WeJourClass.sqlin ((short *) &we_kopf.mdn, 1, 0);
    WeJourClass.sqlin ((short *) &we_kopf.fil, 1, 0);
    WeJourClass.sqlin ((char *)  we_kopf.lief, 0, 17);
    WeJourClass.sqlin ((char *)  we_kopf.lief_rech_nr, 0, 17);
    WeJourClass.sqlin ((char *)  &we_kopf.blg_typ, 0, 2);
    WeJourClass.sqlout ((double *) &we_jour.a, 3, 0);
    WeJourClass.sqlout ((double *) &we_jour.me, 3, 0);
    WeJourClass.sqlout ((double *) &we_jour.pr_ek_euro, 3, 0);
    WeJourClass.sqlout ((double *) &we_jour.pr_ek_nto_eu, 3, 0);
    cursor = WeJourClass.sqlcursor ("select a, me, pr_ek_euro, pr_ek_nto_eu from we_jour "
                               "where mdn = ? "
                               "and fil = ? "
                               "and lief = ? "
                               "and lief_rech_nr = ? "
                               "and blg_typ = ?");
    while (WeJourClass.sqlfetch (cursor) == 0)
    {
         
        mwst = 0;
        strcpy (wert, "");
		if (lief.mwst > 0) //FS-155
		{
			_a_bas.mwst = lief.mwst;
		}
		else
		{
			if (lese_a_bas (we_jour.a) != 0)
			{
				continue;
			}
		}

        sprintf (wert, "%d", _a_bas.mwst);
        Ptab.lese_ptab (Item.GetBuffer (), wert);
        mwst = ratod (ptabn.ptwer1);
        vs += we_jour.me * we_jour.pr_ek_nto_eu * mwst;
    }
    WeJourClass.sqlclose (cursor);
    return vs;
}

