#ifndef _QSWEPOS_DEF
#define _QSWEPOS_DEF

#include "windows.h"
#include "mo_draw.h"
#include "wmask.h"
#include "dbclass.h"

struct QSWEPOS {
   short     mdn;
   short     fil;
   long      lfd;
   char      lief_rech_nr[17];
   char      lief[17];
   double    a;
   long      int_pos;
   char      txt[61];
   short     wrt;
   double    temp;
   double    ph_wert;
   long      dat;
   char      pers_nam[9];
};
extern struct QSWEPOS qswepos, qswepos_null;

#line 10 "qswepos.rh"

class QSWEPOS_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               QSWEPOS_CLASS () : DB_CLASS ()
               {
               }
               int dbreadfirst (void);
};
#endif
