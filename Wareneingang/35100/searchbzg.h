#ifndef _SEARCHBZG_DEF
#define _SEARCHBZG_DEF
#include "wmaskc.h"
#include "cmask.h"
#include "mo_chqex.h"
#include "dbclass.h"
#include "ptab.h"

struct SBZG
{
        char a [15];
	  char a_bz1 [52];
	  char a_bz2 [26];
	  char lief_best [18];
	  char pr_ek [14];
	  char me_einh [12];
        char mdn [6];
        char fil [6];
};

class SEARCHBZG
{
       private :
           static ITEM ua;
           static ITEM ua_bz1;
           static ITEM ulief_best;
           static ITEM upr_ek;
           static ITEM ume_einh;

           static ITEM ia;
           static ITEM ia_bz1;
           static ITEM ilief_best;
           static ITEM ipr_ek;
           static ITEM ime_einh;


           static ITEM iline;

           static field _UbForm[];
           static form UbForm;

           static field _DataForm[];
           static form DataForm;

           static field _LineForm[];
           static form LineForm;

           static HINSTANCE hMainInst;
           static HWND hMainWindow;
           static DB_CLASS DbClass; 
           static HWND awin;
           static struct SBZG *sbzgtab;
           static struct SBZG sbzg;
           static int idx;
           static long anz;
           static CHQEX *Query;
           static int SearchField;
           static char *query;
           static int soa; 
           static int soa_bz1; 
           static int solief_best; 
           static int sopr_ek; 
           static int some_einh; 
           static short mdn;
           static PTAB_CLASS Ptab;

           int SearchPos;
           int OKPos;
           int CAPos;
           DWORD WindowStyle;
           BOOL SetStyle;
        public :
           SEARCHBZG ()
           {
                  SearchPos = 8;
                  OKPos = 9;
                  CAPos = 10;
                  SetStyle = FALSE;
                  mdn = 0;
                  WindowStyle = WS_POPUP | WS_THICKFRAME;
           }

           ~SEARCHBZG ()
           {
                  if (sbzgtab != NULL)
                  {
                      delete sbzgtab;
                      sbzgtab = NULL;
                  }
                  if (Query != NULL)
                  {
                      delete Query;
                      Query = NULL;
                  }
           }

           void SetQuery (char *q)
           {
               this->query = q;
/* ???
               if (query == NULL || (strcmp (query, q) != 0))
               {
                      query = q;

                      if (Query != NULL)
                      {
                             delete Query;
                             Query = NULL;
                      }
                      if (sbzgtab != NULL)
                      {
                             delete sbzgtab;
                             sbzgtab = NULL;
                      }
               }   
*/
           }

           char *GetQuery (void)
           {
                return query;
           }

           SBZG *GetSBzg (void)
           {
               if (idx == -1) return NULL;
               return &sbzg;
           }

           void Setawin (HWND awin)
           {
               this->awin = awin;
           }

           void SetWindowStyle (DWORD style)
           {
               WindowStyle = style;
               SetStyle = TRUE;
           }


           void FillBox (void);
           static void FillFormat (char *, int);
           static void FillVlines (char *, int);
           static void FillCaption (char *, int);
           static void FillRec (char *, int, int);
           static int sorta (const void *, const void *);
           static void SortA (HWND);
           static int sorta_bz1 (const void *, const void *);
           static void SortABz1 (HWND);
           static int sortlief_best (const void *, const void *);
           static void SortLiefBest (HWND);
           static int sortpr_ek (const void *, const void *);
           static void SortPrEk (HWND);
           static int sortme_einh (const void *, const void *);
           static void SortMeEinh (HWND);
           static void SortLst (int, HWND);
           static void UpdateList (void);

           static int SearchLst (char *);
           static int Read (char *);
           void SetParams (HINSTANCE, HWND);
           void Search (short);
           void Search (short,char *);
};  
#endif