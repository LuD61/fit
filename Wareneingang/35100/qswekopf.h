#ifndef _QSWEKOPF_DEF
#define _QSWEKOPF_DEF

#include "windows.h"
#include "mo_draw.h"
#include "wmask.h"
#include "dbclass.h"

struct QSWEKOPF {
   short     mdn;
   short     fil;
   long      lfd;
   char      lief_rech_nr[17];
   char      lief[17];
   char      txt[61];
   short     wrt;
   long      dat;
   char      pers_nam[9];
   char      txt2[61];
   char      txt3[61];
   char      txt4[61];
   char      txt5[61];
   char      txt6[61];
   char      txt7[61];
   char      txt8[61];
   char      txt9[61];
   char      txt10[61];
   short     wrt2;
   short     wrt3;
   short     wrt4;
   short     wrt5;
   short     wrt6;
   short     wrt7;
   short     wrt8;
   short     wrt9;
   short     wrt10;
};
extern struct QSWEKOPF qswekopf, qswekopf_null;

#line 10 "qswekopf.rh"

class QSWEKOPF_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               QSWEKOPF_CLASS () : DB_CLASS ()
               {
               }
               int dbreadfirst (void);
};
#endif
