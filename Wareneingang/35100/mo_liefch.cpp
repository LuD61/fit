#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include "comcthlp.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "stdfkt.h"
#include "wmaskc.h"
#include "colbut.h"
#include "cmask.h"
#include "lbox.h"
#include "mo_liefch.h"
#include "searchpreise_we.h"
#include "a_bas.h"


struct LIEF_A LIEFAC::liefa;
struct LIEF_A *LIEFAC::liefatab;
long LIEFAC::liefaanz = 0;
CHQ *LIEFAC::Choise1;
DB_CLASS LIEFAC::DbClass;

long LIEFAC::Getliefaanz (void)
{
	    return liefaanz;
}

struct LIEF_A *LIEFAC::GetLiefaTab (void)
{
	   return liefatab;
}

struct LIEF_A *LIEFAC::GetLiefa (int pos)
{
	   if (pos >= liefaanz) return NULL;
	   return &liefatab[pos];
}


/*
int LIEFAC::ArtOK (int pos)
{

 	   char buffer [256];

	   if (stdatab[pos].nstatus)
	   {
	             strcpy (stdatab[pos].status, " ");
				 stdatab[pos].nstatus = 0;
	   }
	   else
	   {
	             strcpy (stdatab[pos].status, "  #1");
				 stdatab[pos].nstatus = 1;
	   }
	   memcpy (&stda, &stdatab[pos], sizeof (LIEF_A));

       sprintf (buffer, " %-8s %-15s $%-25s$ $%-11s$ %-8s $%-11s$ %-13s",
		                stda.status,   
                        stda.a, 
				        stda.a_bz1,
		                stda.me_einh_bz, 
		                stda.pr_vk, 
						stda.dat,
						stda.me);

                 sprintf (buffer, " %-8s %-15s $%-25s$ %-13s",
		                stda.status,   
                        stda.a, 
				        stda.a_bz1,
						stda.me);
	   Choise1->UpdateRecord (buffer, pos);
	   return 0;

}
*/

int LIEFAC::LiefaOK (int pos)
/**
Telefon-Nummer wählen.
**/
{
	    syskey = KEY12;
		PostQuitMessage (0);
		return 0;
}


int LIEFAC::Show (HWND hWnd, short best_mdn, short best_fil, double a, char *liefc, short AnzTagePreise)
/**
Standardaufträge auswaehlen
**/
{
	  int cx, cy;
	  char buffer [256];
	  int cursor;
	  int cursor_lief;
	  int i;
      short mdn;
      short fil;
      short lief_mdn;
      short lief_fil;
      char lief [17];
      double pr_ek;  
	  char adr_krz [17];
	  int dsqlstatus;
	  char tchar;
	  HINSTANCE hMainInst; 
	  int ret;

	  tchar = Gettchar ();
	  Settchar ('$');

	  hMainInst = (HINSTANCE) GetClassLong (hWnd, GCL_HMODULE); 
	  Sel = LoadBitmap (hMainInst, "Sel");
	  Msk = LoadBitmap (hMainInst, "Msk");
	  liefaanz = 0;
	  DbClass.sqlin ((double *) &a, 3, 0);
	  DbClass.sqlout ((short *) &liefaanz, 2, 0);
	  cursor = DbClass.sqlcursor ("select count (*) "   
			                        "from lief_bzg "
									"where a = ?");

      dsqlstatus = DbClass.sqlopen (cursor); 
      dsqlstatus = DbClass.sqlfetch (cursor); 
      DbClass.sqlclose (cursor);
  	  if (dsqlstatus == 100) 
	  {
		  return 0;
	  }

	  if (liefaanz == 0)
	  {
		  disp_mess ("Keine Lieferanten vorhanden",2);
		  return 0;
	  }

        liefatab = new struct LIEF_A [liefaanz + 2];
	  if (liefatab == NULL)
	  {
		  disp_mess ("Fehler bei der Speicherzuornung", 2);
		  return 0;
	  }


	  cx = 62;
	  cy = 13;
	  Choise1 = new CHQ (cx, cy,"letzte Preise");
      Choise1->OpenWindow (hMainInst, hWnd);
      Choise1->AddAccelerator (VK_F12, 1, NULL);
      Choise1->AddAccelerator (VK_F5,  2, NULL);
	  sprintf (buffer, " %6s %7s %16s %16s %11s", "1", "1", "1", "1", "1"); 
	  Choise1->VLines (buffer, 3);
	  EnableWindows (hWnd, FALSE);

	  sprintf (buffer, " %-6s  %-7s %-16s %-16s %-11s", 
		                                            "Firma",
		                                            "Filiale", 
 									                "Lieferant", 
 									                "Name", 
		                                            "Preis"); 

	  Choise1->InsertCaption (buffer); 

	  DbClass.sqlin ((short *)   &a, 3, 0);
	  DbClass.sqlout ((short *)  &mdn, 1, 0);
	  DbClass.sqlout ((short *)  &fil, 1, 0);
	  DbClass.sqlout ((char *)   lief, 0, 17);
//	  DbClass.sqlout ((char *)   &adr_krz, 0, 17);
	  DbClass.sqlout ((double *) &pr_ek, 3, 0);

	  cursor = DbClass.sqlcursor ("select lief_bzg.mdn, lief_bzg.fil, "
			                         "lief_bzg.lief, lief_bzg.pr_ek "
			                         "from lief_bzg "
							 "where lief_bzg.a = ? "
							 "order by pr_ek");  

      DbClass.sqlin ((short*) &lief_mdn, 1, 0);
      DbClass.sqlin ((short*) &lief_fil, 1, 0);
      DbClass.sqlin ((char *) lief, 0, 17);
      DbClass.sqlout ((char *) adr_krz, 0, 17);
      cursor_lief = DbClass.sqlcursor ("select adr_krz from lief,adr "
                                       "where lief.mdn = ? "
                                       "and   lief.fil = ? "
                                       "and lief.lief  = ? " 
                                       "and lief.adr = adr.adr"); 

      i = 0;

	  while (DbClass.sqlfetch (cursor) == 0)
	  {
		             if (fil != 0 && fil != best_fil) continue;
		             if (mdn != 0 && mdn != best_mdn) continue;
                     sprintf (liefatab[i].mdn, "%4hd", mdn);
                     sprintf (liefatab[i].fil, "%4hd", fil);
                     strcpy (liefatab[i].lief, lief);
                     lief_mdn = mdn;
                     lief_fil = fil;
                     DbClass.sqlopen (cursor_lief);
                     while (DbClass.sqlfetch (cursor_lief) == 100)
                     {
                               if (lief_fil > 0)
                               {
                                       lief_fil = 0;
                               }  
                               else if (lief_mdn > 0)
                               {
                                       lief_mdn = 0;
                               }  
                               else
                               {
                                        break;
                               }
                               DbClass.sqlopen (cursor_lief);
                     }
                          
 
                     if (dsqlstatus == 0)
                     {
                                 strcpy (liefatab[i].adr_krz, adr_krz); 
                     }
                     else
                     {
                                 strcpy (liefatab[i].adr_krz, " "); 
                     }
                     sprintf (liefatab[i].pr_ek, "%6.2lf", pr_ek); 
 	                 sprintf (buffer, " %-6s  %-7s $%-16s$ $%-16s$ %-11s", 
		                                            liefatab[i].mdn,
		                                            liefatab[i].fil, 
 									                liefatab[i].lief, 
 									                liefatab[i].adr_krz, 
		                                            liefatab[i].pr_ek); 
	                 Choise1->InsertRecord (buffer);
		             i ++;
	  }

	  liefaanz = i;
	  DbClass.sqlclose (cursor);

      ret = 2;
	  while (ret > 1)
	  {
		Choise1->SetOkFunc (LiefaOK);
		Choise1->ProcessMessages ();
		Settchar (tchar);
		if (syskey != KEY5)
		{
					Choise1->GetText (buffer);
					i = Choise1->GetListpos ();
					strcpy (liefc, liefatab[i].lief);
					ret = 1;
		}
		else
		{
					ret = 0;
		}
		char datum[12];
		sysdate (datum);
		int ddatum = dasc_to_long (datum) - AnzTagePreise  ;

		if (syskey == KEY6)
		{
				SEARCHPREISE_WE SearchPreise_we; 
				char buffer [256];
	
				sprintf (buffer, "a = %.0f and (best_term > %d or we_dat > %d)", a,ddatum,ddatum);
				SearchPreise_we.SetQuery (buffer); 
				SearchPreise_we.SetWindowStyle (WS_POPUP | WS_THICKFRAME);
				SearchPreise_we.SetParams (hMainInst, hWnd);
				SearchPreise_we.Setawin (GetParent (hWnd));
				SearchPreise_we.Search (best_mdn);
				ret = 6;
		}
		if (syskey == KEY8)
		{
				SEARCHPREISE_WE SearchPreise_we; 
				char buffer [256];
	
					Choise1->GetText (buffer);
					i = Choise1->GetListpos ();
					strcpy (liefc, liefatab[i].lief);

				sprintf (buffer, "a = %.0f and preise_we.lief = \"%s\" and (best_term > %d or we_dat > %d) ", a,liefc,ddatum,ddatum);
				SearchPreise_we.SetQuery (buffer); 
				SearchPreise_we.SetWindowStyle (WS_POPUP | WS_THICKFRAME);
				SearchPreise_we.SetParams (hMainInst, hWnd);
				SearchPreise_we.Setawin (GetParent (hWnd));
				SearchPreise_we.Search (best_mdn);
				ret = 6;
		}
	  }
	

	  EnableWindows (hWnd, TRUE);
      Choise1->DestroyWindow ();
	  delete Choise1;
	  Choise1 = NULL;
	  currentfield = 0;
      return ret;
}

