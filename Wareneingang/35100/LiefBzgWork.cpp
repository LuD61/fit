#include <windows.h>
#include <stdio.h>
#include "LiefBzgWork.h"
#include "a_bas.h"


int LiefBzgWork::ReadLief (short mdn, short fil, char *lief_nr)
{
    int dsqlstatus;

    if (liefbzg_cursor != -1)
    {
        LiefBzg.sqlclose (liefbzg_cursor);
    }

    LiefBzg.sqlin ((short *) &lief_bzg.mdn, 1, 0);
    LiefBzg.sqlin ((short *) &lief_bzg.fil, 1, 0);
    LiefBzg.sqlin ((char *)  lief_bzg.lief, 0, 17);
    LiefBzg.sqlout ((double *)  &lief_bzg.a, 3, 0);
    liefbzg_cursor = LiefBzg.sqlcursor ("select a from lief_bzg "
                                        "where mdn = ? "
                                        "and fil = ? "
                                        "and lief = ? "
                                        "order by a");
    dsqlstatus = LiefBzg.sqlfetch (liefbzg_cursor);
    if (dsqlstatus == 0)
    {
        return ReadA ();
    }
    return dsqlstatus;
}

int LiefBzgWork::TestReadLief (void)
{
	extern short sql_mode;
	short sql_sav ;
	sql_sav = sql_mode;
	sql_mode = 1;
    int dsqlstatus;


    LiefBzg.sqlin ((short *) &lief_bzg.mdn, 1, 0);
    LiefBzg.sqlin ((short *) &lief_bzg.fil, 1, 0);
    LiefBzg.sqlin ((char *)  lief_bzg.lief, 0, 17);
    LiefBzg.sqlin ((double *)  &lief_bzg.a, 3, 0);
    dsqlstatus = LiefBzg.sqlcomm ("select a from lief_bzg "
                                        "where mdn = ? "
                                        "and fil = ? "
                                        "and lief = ? "
										"and a = ? "
                                        "for update");
	sql_mode = sql_sav;
    return dsqlstatus;
}
int LiefBzgWork::ReadLief (void)
{
    int dsqlstatus;

    if (liefbzg_cursor == -1)
    {
        return 100;
    }
    dsqlstatus = LiefBzg.sqlfetch (liefbzg_cursor);
    if (dsqlstatus == 0)
    {
        return ReadA ();
    }
    return dsqlstatus;
}

int LiefBzgWork::ReadA (void)
{
    int dsqlstatus;

    memcpy (&_a_bas, &_a_bas_null, sizeof (struct A_BAS));
    lese_a_bas (lief_bzg.a);

    dsqlstatus = LiefBzg.dbreadfirst ();
    return dsqlstatus;
}


int LiefBzgWork::ReadA (short mdn, short fil, char *lief_nr, double a,char *lief_best)
{
    int dsqlstatus;

    memcpy (&_a_bas, &_a_bas_null, sizeof (struct A_BAS));
    memcpy (&lief_bzg, &lief_bzg_null, sizeof (struct LIEF_BZG));
    lese_a_bas (a);

    lief_bzg.mdn = mdn;
    lief_bzg.fil = fil;
    strcpy (lief_bzg.lief, lief_nr);
    strcpy (lief_bzg.lief_best, lief_best);
    lief_bzg.a = a;
    dsqlstatus = LiefBzg.dbreadfirst ();
    while (dsqlstatus == 100)
    {
        if (lief_bzg.fil > 0)
        {
            lief_bzg.fil = 0;
        }
        else if (lief_bzg.mdn > 0)
        {
            lief_bzg.mdn = 0;
        }
        else
        {
            break;
        }
        dsqlstatus = LiefBzg.dbreadfirst ();
    }
    if (dsqlstatus == 100)
    {
        strcpy (lief_bzg.best_txt1, _a_bas.a_bz1);
        strcpy (lief_bzg.best_txt2, _a_bas.a_bz2);
        GetLiefDefaults ();
    }
    return dsqlstatus;
}

int LiefBzgWork::GetLiefDefaults (void)
{
    int dsqlstatus;

    LiefBzg.sqlin ((short *) &lief_bzg.mdn, 1, 0); 
    LiefBzg.sqlin ((short *) &lief_bzg.fil, 1, 0); 
    LiefBzg.sqlin ((char *)  lief_bzg.lief, 0, 17);
    LiefBzg.sqlout ((short *) &lief_bzg.lief_zeit, 1, 0); 
    LiefBzg.sqlout ((char *)  lief_bzg.lief_rht, 0, 2);
    dsqlstatus = LiefBzg.sqlcomm ("select lief_zeit, lief_rht from lief where mdn = ? "
                                                          "and fil = ? "
                                                          "and lief = ?");
    strcpy  (lief_bzg.lief_kz, default_lief_kz);
    if (lief_bzg.lief_rht[0] <= ' ')
    {
               strcpy  (lief_bzg.lief_rht, "M");
    }
    sprintf (lief_bzg.lief_best, "%.0lf", lief_bzg.a);
/*
    LiefBzg.sqlin ((double *)  &lief_bzg.a, 3, 0);
    if (LiefBzg.sqlcomm ("select a from a_best where a = ?") == 0)
    {
              strcpy (lief_bzg.lief_kz, "J");
    }
*/
    lief_bzg.min_best = 1;
    lief_bzg.me_einh_ek = 1;
    return dsqlstatus; 
}

BOOL LiefBzgWork::LiefmebestExist (void)
{
    int dsqlstatus;

    liefmebest.mdn = lief_bzg.mdn;
    liefmebest.fil = lief_bzg.fil;
    strcpy (liefmebest.lief, lief_bzg.lief);
    liefmebest.a   = lief_bzg.a;
    dsqlstatus = LiefMeBest.dbreadfirst ();
    return dsqlstatus == 100 ? FALSE : TRUE;
}

void LiefBzgWork::WriteLiefmebest (char *me_kz)
{
    int dsqlstatus;

    liefmebest.mdn = lief_bzg.mdn;
    liefmebest.fil = lief_bzg.fil;
    strcpy (liefmebest.lief, lief_bzg.lief);
    liefmebest.a   = lief_bzg.a;
    dsqlstatus = LiefMeBest.dbreadfirst ();
    if (dsqlstatus != 0) 
    {
        return;
    }

    if (atoi (me_kz) != atoi (old_me_kz))
    {
        liefmebest.pr_ek0 = 0.0;
        liefmebest.pr_ek1 = 0.0;
        liefmebest.pr_ek2 = 0.0;
        liefmebest.pr_ek3 = 0.0;
        liefmebest.pr_ek4 = 0.0;
        liefmebest.pr_ek5 = 0.0;
        liefmebest.pr_ek6 = 0.0;

    }
    if (atoi (me_kz) == 0)
    {
        liefmebest.pr_ek1 = lief_bzg.pr_ek;
    }
    else if (atoi (me_kz) == 1)
    {
        liefmebest.pr_ek0 = lief_bzg.pr_ek;
    }
    liefmebest.me_einh1 = lief_bzg.me_einh_ek;
    LiefMeBest.dbupdate ();
    strcpy (old_me_kz, me_kz); 
}


int LiefBzgWork::WriteA (void)
{
    int dsqlstatus;
    A_BEST_CLASS ABest;

    LiefBzg.sqlin ((short *)  &lief_bzg.mdn, 1, 0);
    LiefBzg.sqlin ((short *)  &lief_bzg.fil, 1, 0);
    LiefBzg.sqlin ((char *)   &lief_bzg.lief, 0, 17);
    LiefBzg.sqlin ((char *)   &lief_bzg.lief_best, 0, 17);
    LiefBzg.sqlin ((double *) &lief_bzg.a, 3, 0);
    dsqlstatus = LiefBzg.sqlcomm ("select a from lief_bzg "
                                  "where mdn = ? "
                                  "and fil = ? "
                                  "and lief = ? "
                                  "and lief_best = ? "
                                  "and a <> ?");
    if (dsqlstatus == 0)
    {
        return -1;
    }

    LiefBzg.dbupdate ();
    if (lief_bzg.lief_kz[0] == 'J')
    {
        a_best.mdn = lief_bzg.mdn;
        a_best.fil = lief_bzg.fil;
        a_best.a   = lief_bzg.a;
        dsqlstatus = ABest.dbreadfirst ();
        strcpy (a_best.lief, lief_bzg.lief);
        strcpy (a_best.lief_best, lief_bzg.lief_best);
        if (dsqlstatus == 100)
        {
             a_best.best_anz = 1;
             strcpy (a_best.best_auto, "N");
             a_best.me_einh_ek = lief_bzg.me_einh_ek;
             strcpy (a_best.umk_kz, "N");
        }
        ABest.dbupdate ();
    }
    return 0;
}

int LiefBzgWork::DeleteLief (void)
{
    int dsqlstatus;
    
    LiefBzg.sqlin ((short *)  &lief_bzg.mdn, 1, 0);
    LiefBzg.sqlin ((short *)  &lief_bzg.fil, 1, 0);
    LiefBzg.sqlin ((char *)   &lief_bzg.lief, 0, 17);
    dsqlstatus = LiefBzg.sqlcomm ("delete from lief_bzg "
                                  "where mdn = ? "
                                  "and fil = ? "
                                  "and lief = ?");
    return dsqlstatus;
}


int LiefBzgWork::DeleteA (void)
{
    LiefBzg.dbdelete ();
    return 0;
}

int LiefBzgWork::GetPtab (char *dest, char *item, char *wert)
{
    int dsqlstatus;

    dest[0] = 0;
    dsqlstatus = Ptab.lese_ptab (item, wert);  
    if (dsqlstatus == 100)
    {
        return -1;
    }
    strcpy (dest, ptabn.ptbezk);
    return 0;
}

int LiefBzgWork::ShowPtab (char *dest, char *item)
{
    int dsqlstatus;

    dsqlstatus = Ptab.Show (item);  
    if (syskey == KEY5)
    {
        return -1;
    }
    strcpy (dest, ptabn.ptwert);
    return 0;
}

int LiefBzgWork::GetMdnName (char *dest, short mdn)
{
    int dsqlstatus;

    _mdn.konversion = 1;
    if (mdn == 0)
    {
        strcpy (dest, "Zentrale");
        return 0;
    }
    dest[0] = 0;
    LiefBzg.sqlin ((short *) &mdn, 1, 0);
    LiefBzg.sqlout ((char *) dest, 0, 17);
    dsqlstatus =  LiefBzg.sqlcomm ("select adr.adr_krz from mdn,adr "
                                   "where mdn.mdn = ? " 
                                   "and adr.adr = mdn.adr");
    if (dsqlstatus == 100)
    {
        return 100;
    }
    Mdn.lese_mdn (mdn);
    if (_mdn.konversion == 0)
    {
        _mdn.konversion = 1.0;
    }
    return 0;
}

int LiefBzgWork::GetFilName (char *dest, short mdn, short fil)
{

    if (mdn == 0 || fil == 0)
    {
        strcpy (dest, "Mandant");
        return 0;
    }
    dest[0] = 0;
    LiefBzg.sqlin ((short *) &mdn, 1, 0);
    LiefBzg.sqlin ((short *) &fil, 1, 0);
    LiefBzg.sqlout ((char *) dest, 0, 17);
    return LiefBzg.sqlcomm ("select adr.adr_krz from fil,adr "
                             "where fil.mdn = ? " 
                             "and   fil.fil = ? " 
                             "and adr.adr = fil.adr");
}

int LiefBzgWork::GetLiefName (char *dest, short mdn, char *lief_nr)
{
/*
    dest[0] = 0;


    LiefBzg.sqlin ((short *) &mdn, 1, 0);
    LiefBzg.sqlin ((char *)  lief, 0, 17);
    LiefBzg.sqlout ((char *) dest, 0, 17);

    return LiefBzg.sqlcomm ("select adr.adr_krz from lief,adr "
                             "where lief.mdn = ? " 
                             "and   lief.lief = ? "
                             "and adr.adr = lief.adr");
*/
    int dsqlstatus;
    int cursor;

    dest[0] = 0;


    LiefBzg.sqlin ((short *) &mdn, 1, 0);
    LiefBzg.sqlin ((char *)  lief_nr, 0, 17);
    LiefBzg.sqlout ((char *) dest, 0, 17);

    cursor = LiefBzg.sqlcursor ("select adr.adr_krz from lief,adr "
                                "where lief.mdn = ? " 
                                "and   lief.lief = ? "
                                "and adr.adr = lief.adr");
    dsqlstatus = LiefBzg.sqlfetch (cursor);
    while (dsqlstatus == 100)
    {
        if (mdn > 0)
        {
            mdn = 0;
        }
        else
        {
            break;
        }
        dsqlstatus = LiefBzg.sqlopen (cursor);
        dsqlstatus = LiefBzg.sqlfetch (cursor);
    }
    LiefBzg.sqlclose (cursor);
    if (dsqlstatus == 0)
    {
        lief.mdn = mdn;
        strcpy (lief.lief, lief_nr);
        Lief.dbreadfirst ();
    }
    return dsqlstatus;
}


void LiefBzgWork::FillRow (char *buffer)
{
    if (lief.waehrung == 2)
    {
        sprintf (buffer, "%13.0lf  |%-24s|  |%-16s|  %8.4lf   |%-24s|",
                         lief_bzg.a, _a_bas.a_bz1, lief_bzg.lief_best,
                         lief_bzg.pr_ek_eur, lief_bzg.best_txt1);
    }
    else
    {
        sprintf (buffer, "%13.0lf  |%-24s|  |%-16s|  %8.4lf   |%-24s|",
                         lief_bzg.a, _a_bas.a_bz1, lief_bzg.lief_best,
                         lief_bzg.pr_ek, lief_bzg.best_txt1);
    }
}

void LiefBzgWork::BeginWork (void)
{
    ::beginwork ();
}

void LiefBzgWork::CommitWork (void)
{
    ::commitwork ();
}

void LiefBzgWork::RollbackWork (void)
{
    ::rollbackwork ();
}

void LiefBzgWork::InitRec (void)
{
    memcpy (&lief_bzg, &lief_bzg_null, sizeof (struct LIEF_BZG));
}