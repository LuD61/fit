/*** 
--------------------------------------------------------------------------------

 Fuer dieses Programm behalten wir uns alle Rechte, auch fuer den Fall der 
 Patenterteilung und der Eintragung eines anderen gewerblichen Schutz-     
 rechtes vor. Missbraeuchliche Verwendung, wie insbesondere Vervielfael-   
 tigung und Weitergabe an Dritte ist nicht gestattet; sie kann zivil- und  
 strafrechtlich geahndet werden.                                           

--------------------------------------------------------------------------------
-
-       Modulname               :       conf_env.h
-
-       Autor                   :       F.Folmer
-       Erstellungsdatum        :       07.07.95
-       Modifikationsdatum      :       TT.MM.JJ
-
-       Version                 :       1.00
-       Laendervariante         :       BRD
-
-       Rechner                 :       UNISYS 386
-       Betriebssystem          :       XENIX  386
-       Sprache                 :       C
-
-       Aenderungsjournal       :
-
-       lfd.    Version Datum           Name       Beschreibung
-       ------------------------------------------------------------------------
-       1       1.00    09.01.96	F.Folmer  Grundmodul
--------------------------------------------------------------------------------
-
-       Modulbeschreibung       :	Hilfsprogramm
-				
--------------------------------------------------------------------------------
***/

  #define   _getenv  config_environment

  char * config_environment(char *);

