#ifndef _MO_FLOG_DEF
#define _MO_FLOG_DEF

#include <string.h>
#include <stdlib.h>
#ifdef WIN32
static char slash = '\\';
#else
static char slash = '/';
#endif

class EventProt 
{
       private :
             char FileName [256];
             int MAXLINES;
       public :
             EventProt (int MAXLINES, char *FileName) : MAXLINES (MAXLINES)
             {
				     if (getenv ("LOGPATH"))
					 {
						 sprintf (this->FileName, "%s%c%s", getenv ("LOGPATH"), slash, FileName);
					 }
					 else
					 {
						 sprintf (this->FileName, "%s%c%s", getenv ("TMPPATH"), slash, FileName);
//                         strcpy (this->FileName, FileName);
					 }
             }
               
             void WriteFirstLine (char *FileText);
             void WriteDTlog (int, long, char *);
};

#endif
