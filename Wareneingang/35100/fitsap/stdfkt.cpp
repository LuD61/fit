#include <stdio.h>
#include <windows.h>
// #include <windowsx.h>
#ifndef CONSOLE
//#include "wmask.h"
#endif
#include "stdfkt.h"


int DCcopy (char *ziel, char *quelle)
/**
Datei kopieren.
**/
{
           FILE *fz;
           FILE *fq;
           int bytes;
           char buffer [0x1000];

           fq = fopen (quelle, "rb");
           if (fq == (FILE *) 0)
           {
                       return (-1);
           }

           fz = fopen (ziel, "wb");
           if (fz == (FILE *) 0)
           {
                       fclose (fq);
                       return (-1);
           }

           while  (bytes = fread (buffer, 1, 0x1000, fq))
           {
                        fwrite (buffer, 1, bytes, fz);
           }
           fclose (fq);
           fclose (fz);
           return (0);
}

int WaitConsoleExec (LPSTR prog, DWORD SW_CONSOLE)
/**
Console-Process starten und auf Ende Warten.
**/
{
        STARTUPINFO sti;
        PROCESS_INFORMATION pi;
        int ret;

        ZeroMemory (&sti, sizeof (sti));
        sti.cb = sizeof (sti);

        ret = CreateProcess (NULL,
                       prog,
                       NULL,
                       NULL,
                       NULL,
                       SW_CONSOLE,
                       NULL,
                       NULL,
                       &sti,
                       &pi);
        if (ret)
        {
                  CloseHandle (pi.hThread);
                  WaitForSingleObject (pi.hProcess, INFINITE);
                  CloseHandle (pi.hProcess);
        }
        return ret;
}

int ConsoleExec (LPSTR prog, DWORD SW_CONSOLE)
/**
Console-Process starten und nicht auf Ende Warten.
**/
{
        STARTUPINFO sti;
        PROCESS_INFORMATION pi;
        int ret;

        ZeroMemory (&sti, sizeof (sti));
        sti.cb = sizeof (sti);
        ret = CreateProcess (NULL,
                       prog,
                       NULL,
                       NULL,
                       NULL,
                       SW_CONSOLE,
                       NULL,
                       NULL,
                       &sti,
                       &pi);
        if (ret)
        {
                  CloseHandle (pi.hThread);
                  CloseHandle (pi.hProcess);
        }
        return ret;
}

int WaitConsoleExecU (LPSTR prog, DWORD SW_CONSOLE, char *out, char *errout)
/**
Console-Process starten und auf Ende Warten.
**/
{
        STARTUPINFO sti;
        PROCESS_INFORMATION pi;
        int ret;

        ZeroMemory (&sti, sizeof (sti));
        sti.cb = sizeof (sti);

        ret = CreateProcess (NULL,
                       prog,
                       NULL,
                       NULL,
                       NULL,
                       SW_CONSOLE,
                       NULL,
                       NULL,
                       &sti,
                       &pi);
        if (ret)
        {
                  CloseHandle (pi.hThread);
                  WaitForSingleObject (pi.hProcess, INFINITE);
                  CloseHandle (pi.hProcess);
        }
        return ret;
}

int ConsoleExecU (LPSTR prog, DWORD SW_CONSOLE, char *out, char *errout)
/**
Console-Process starten und nicht auf Ende Warten.
**/
{
        STARTUPINFO sti;
        PROCESS_INFORMATION pi;
        int ret;

        ZeroMemory (&sti, sizeof (sti));
        sti.cb = sizeof (sti);
        ret = CreateProcess (NULL,
                       prog,
                       NULL,
                       NULL,
                       NULL,
                       SW_CONSOLE,
                       NULL,
                       NULL,
                       &sti,
                       &pi);
        if (ret)
        {
                  CloseHandle (pi.hThread);
                  CloseHandle (pi.hProcess);
        }
        return ret;
}


int ProcExec (LPSTR prog, WORD SHOW_MODE, int x, int y, int cx, int cy)
/**
Fenster-Process starten und nicht auf Ende Warten.
**/
{
        STARTUPINFO sti;
        PROCESS_INFORMATION pi;
        int ret;

        ZeroMemory (&sti, sizeof (sti));
        sti.cb = sizeof (sti);
        if (x != -1)
        {
                     sti.dwFlags |= STARTF_USEPOSITION;
                     sti.dwX = x;
                     sti.dwY = y;
        }

        if (cx != -1)
        {
                     sti.dwFlags |= STARTF_USESIZE;
                     sti.dwXSize = cx;
                     sti.dwYSize = cy;
        }
        sti.wShowWindow = SHOW_MODE;

        ret = CreateProcess (NULL,
                       prog,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       &sti,
                       &pi);
        if (ret)
        {
                  CloseHandle (pi.hThread);
                  CloseHandle (pi.hProcess);
        }
        return ret;
}

DWORD ProcWaitExec (LPSTR prog, WORD SHOW_MODE, int x, int y, int cx, int cy)
/**
Fenster-Process starten und auf Ende Warten.
**/
{
        STARTUPINFO sti;
        PROCESS_INFORMATION pi;
        DWORD ExitCode; 
        int ret;

        ZeroMemory (&sti, sizeof (sti));
        sti.cb = sizeof (sti);
        if (x != -1)
        {
                     sti.dwFlags |= STARTF_USEPOSITION;
                     sti.dwX = x;
                     sti.dwY = y;
        }

        if (cx != -1)
        {
                     sti.dwFlags |= STARTF_USESIZE;
                     sti.dwXSize = cx;
                     sti.dwYSize = cy;
        }
        sti.wShowWindow = SHOW_MODE;
        sti.dwFlags |= STARTF_USESHOWWINDOW;

        ret = CreateProcess (NULL,
                       prog,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       &sti,
                       &pi);
        if (ret)
        {
                  CloseHandle (pi.hThread);
                  WaitForSingleObject (pi.hProcess, INFINITE);
        }
		else
        {
			      return -1;
        }				  
        GetExitCodeProcess (pi.hProcess, &ExitCode);
        while (ExitCode == STILL_ACTIVE)
        {
             Sleep (5);
             GetExitCodeProcess (pi.hProcess, &ExitCode);
        }
        CloseHandle (pi.hProcess);
        return ExitCode;
}

