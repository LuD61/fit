
/* ************************************************************************* */
/*                                                                           */
/*    Manager fuer Bauerngut                     sapfit.cpp                   */
/*    Erstellungsdatum 11.10.00                                              */
/*    Autor  F.Folmer                                                        */
/*                                                                           */
/*    Aenderung: 20.03.02                                                    */
/*                                                                           */
/*                                                                           */
/* ************************************************************************* */

#define VERSION "1.21"
#define DATUM "04.09.2002"

/* 1.00 11.10.00 Hauptprogramm                                               */
/* 1.01 08.02.01 Input-Dateien volle Namen in der .cfg-Datei                 */
/* 1.20 20.03.02 Input/Output-Dat. Namen: FROMH_FTMMMM_n.yyyymmdd.hhmmss.asc */
/* 1.21 04.09.02 Protokoll <sapfit_TTMM.pro> nach 2 Wo l�schen               */

#ifdef UNIX
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/types.h>
#include <prototypes.h>
#include "windtype.h"
#else
#include <direct.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include <windows.h>
#endif
//#include "dbclass.h"
#include "mo_flog.h"
#include "stdfkt.h"
#include "conf_env.h"
#include "mdn.h"

EventProt Eventprot (1000, "sapfit.log");
char ENV_DATEI[16] = {"sapfit.cfg"};
char PROGRAMM[10] = {"sapfit"};

#ifdef UNIX
#define SLESH   (char) 0x2f
#define SLESCH   (char) 0x2f
#define DEL      "rm"
#else
#define SLESH   (char) 0x5c
#define SLESCH   (char) 0x5c
#define DEL      "del"
#endif
#define STATUS_DAT   "sapfit.sta"
#define STEP     (int)1 

struct UDAT {
             int    satz_typ;
             char * inp_dat;
             char * if_dat;
             char * par;
             char * run_funk;
             char * zus_funk;
            };
struct UDAT udat1[] =
   { 1, NULL, "s01st.if", "", "asc_bws", "",
     3, NULL, "s03hwg.if", "", "asc_bws", "",
     3, NULL, "s03wg.if", "", "asc_bws", "",
     3, NULL, "s03ag.if", "", "asc_bws", "",
     5, NULL, "s05kun.if","", "asc_bws", "",
     6, NULL, "s06a_bas.if","", "asc_bws", "",
     6, NULL, "s06a_krz.if","", "asc_bws", "",
     6, NULL, "s06a_eig.if","", "asc_bws", "",
     6, NULL, "s06a_ean.if","", "asc_bws", "",
	 6, NULL, "s06a_leer.if","", "asc_bws", "",
     6, NULL, "s06ipr.if","", "asc_bws", "",
     7, NULL, "s07a_ean.if","", "asc_bws", "",
    10, NULL, "s10kun.if","", "asc_bws", "",
    31, NULL, "s31rab_s.if", "", "asc_bws", "",
	90, NULL, "s90aipr.if","", "asc_bws", "",
	90, NULL, "s90aiprz.if","", "asc_bws", "",
	90, NULL, "s90arabs.if","", "asc_bws", "",
     0, "", "","", "", ""};
struct UDAT udat2[] =
   {
    40, NULL, "s40lsk.if","", "asc_bws", "",
    41, NULL, "s41lsp.if","", "asc_bws", "",
     0, "", "","", "", ""};

struct STATUS_DATEN
	  {
	  char    kommando;
	  short   ger_liste;
	  };

extern struct MDN _mdn;
struct  STATUS_DATEN status_daten;
struct tm * strtime;

#ifndef UNIX
 #define R       "r"
 #define RPLUS   "r+"
 #define W       "w"
 #define WPLUS   "w+"
 #define BR      "rb"
 #define BRPLUS  "rb+"
 #define BW      "wb"
 #define BWPLUS  "wb+"
#else
 #define  R       "r"
 #define  RPLUS   "r+"
 #define  W       "w"
 #define  WPLUS   "w+"
 #define  BR      "r"
 #define  BRPLUS  "r+"
 #define  BW      "w"
 #define  BWPLUS  "w+"
#endif

#define BSLESCH        (char)0x5c 
#define BSLESH         (char)0x5c 

#define TIMER    (int)30
char markt[8];
char pfadhost[64];
char pfadkasse[64];
char pdathost1[96];
char pdathost2[96];
char dathost1[64];
char dathost2[64];
char pdathost1b[96];
char pdathost2b[96];
char dathost1b[64];
char dathost2b[64];
char datkasse[96];
char datmust1[64];
char datmust2[64];
/* 20.03.02 */
char endung1[4];
char endung2[4];
/* 20.03.02 */
int  timerhost=0;
int  timerkasse=0;
char * tmp;
char * etc;
char * bws;
char buffer [256];
char char_zwi [256];
/* Wochentage fuer die Sicherung */
char *days [] = {"Son", "Mon", "Die", "Mit", "Don", "Fre", "Sam", NULL};
short verz_geloescht = 0;    /* Marker   */

FILE * flog;
char logpath[64];
char senden_status[64];
char logbuch[64];
int   tst=0;
int warte_zeit;
int step;
short neustart=0;
short Log=0;
short Protokoll=0;
short Init=0;
short testen=0;
short einzeln=0;
short loeschen=1;
short ohne_aufruf=0;
short testlog=0;
short hilfe=0;
short version=0;
long error=0;
long exit_code=0;
int  wiederholung = 0;         /* Wiederholungszaehler */
int  tag=0;                   /* 04.09.02 FF */
int  monat=0;                 /* 04.09.02 FF */
char datum[11];               /* 04.09.02 FF */
char ldatum[11];              /* 04.09.02 FF */
long ldat=0;                  /* 04.09.02 FF */

/*  Prototyp */
int  dlong_to_asc (long, char *);
long dasc_to_long (char *);


/*  Prototyp */
char * _getenv (char *);
void DEBUG(int tst, char *format, ...);
long status_host_lesen(char*, struct STATUS_DATEN *);
long status_host_schreiben(char *, char);



/***************************************************************************/
/* Procedure :            DEBUG                                            */
/*-------------------------------------------------------------------------*/
/* Funktion :             debug trace output                               */
/*-------------------------------------------------------------------------*/
/* Kurzbeschreibung :                                                      */
/*                                                                         */
/*                                                                         */
/*                                                                         */
/*-------------------------------------------------------------------------*/
/* Aufruf :               DEBUG (level,format, ...)                        */
/* Funktionswert :        ----                                             */
/* Eingabeparameter :     char *format: format string to control           */
/*                                      data format/number of arguments    */
/*                                                                         */
/* Ausgabeparameter :                                                      */
/*                                                                         */
/*-------------------------------------------------------------------------*/

void DEBUG (int tst, char *format, ...)
{
va_list args;
va_start(args, format);

if((testen == 0 && testlog == 0) || tst == 0)
  return;

  if(testen)
       {
       vfprintf(stdout,format, args);
       fflush(stdout);
       }
  if(testlog != 0)
       {
       vfprintf(flog,format, args);
       fflush(flog);
       }

  va_end(args);

 return;
}

/*------------------------------------------------------------------*/
/*                     Hilfsfunktionen                              */

int tst_arg (char * arg)
/**
Argumente nach '-' testen.
**/
{
for (;*arg; arg += 1)
       {
       switch (*arg)
               {
               case 't' :
		       if(testlog == 0)
                            testen = 1;
                       break;
               case 'e' :
                       einzeln = 1;
                       break;
               case 'n' :
                       neustart = 1;
                       break;
               case 'h' :
                       hilfe = 1;
                       break;
               case 'd' :
                       testlog = 1;
                       testen = 0;
                       break;
               case 'v' :
                       version = 1;
                       break;
               case 'o' :
                       ohne_aufruf = 1;
                       break;
               case 's' :
		       step = (int)atoi(arg + 1);
                       break;
               case 'w' :
		       warte_zeit = (int)atoi(arg + 1);
                       break;
               case 'l' :
                       loeschen = 0;
                       break;
               case 'I' :
                       Init = 1;
                       break;
               case 'L' :
                       Log = 1;
                       break;
               case 'P' :
                       Protokoll = 1;
                       break;
               default:
                       break;
               }
       }
return(0);
}

int scroll_arg (char *arg[], int pos, int anz)
/**
Argumenete nach links scrollen.
**/
{
anz --;
for (;pos < anz; pos ++)
       {
       arg [pos] = arg [pos + 1];
       }
return (anz);
};
/*********** Datei sichern.  ***********/
void SaveDatei (char * quelle_pfad_dat, char * qdat)
{
        time_t timer;
        struct tm *ltime;
        char quelle [64];
        char ziel [64];
        char datei [32];
//        char zwi[8];
        char tzwi[8];
        char * p;

        time (&timer);
        ltime = localtime (&timer);

        sprintf (buffer, "%s%c%s", tmp, SLESH, days [ltime->tm_wday]);
        DEBUG(1,"Verz: buffer=%s\n",buffer);

//        sprintf(zwi,"%02d%02d",ltime->tm_hour,ltime->tm_min);

        strcpy (datei,qdat);
        p = strstr (datei,".ASC");
        if (p == NULL)
             {
             p = strstr (datei,".asc");
             if (p == NULL)
                  return; 
             else
                  sprintf(tzwi,".asc");
             }
        else
             sprintf(tzwi,".ASC");

//    strcpy(p,zwi);
//    strcat(datei,tzwi);
  	strcpy (quelle,quelle_pfad_dat);
  	sprintf (ziel,"%s%c%s", buffer, SLESH, datei);
      
	DEBUG (1,"Copy %s %s\n",quelle,ziel);
	//CopyFile (quelle, ziel, FALSE);   //quelle nicht geloescht
	CopyFile (quelle, ziel, TRUE);    //quelle geloescht
    if (loeschen)
		unlink (quelle);
} /* end of SaveDatei () */


/***
--------------------------------------------------------------------------------
##D8A
	Procedure	: dat_zeit_log()
	In		: char * char_zwi
			  short  parameter   0 Ausgabe ohne Trennzeichen
			                     1 Ausgabe mit Trennzeichen
	Out		: char * char_zwi
	Funktion	: Prozedur liefert Datum und Uhr als String.

##D8E
--------------------------------------------------------------------------------
***/
char * dat_zeit_log(char *puff,short parameter)
{
time_t uxtime;
   time((time_t *) &uxtime);
   strtime = localtime((time_t *) &uxtime); /* aktuelle Zeit holen */

/* Jahr 2000 FF */
if(strtime->tm_year >= 100)
    strtime->tm_year = strtime->tm_year % 100;
/* Jahr 2000 FF */

   /* 04.09.02 */
   if(parameter == 7)
      {
      sprintf(puff,"%2.2hu.%2.2hu.20%2.2hu %2.2hu:%2.2hu:%2.2hu ",
      strtime->tm_mday,strtime->tm_mon+1,strtime->tm_year,
      strtime->tm_hour,strtime->tm_min,strtime->tm_sec);
      return(puff);
      }

   if(parameter == 1)
      {
      sprintf(puff,"%2.2hu.%2.2hu.%2.2hu %2.2hu:%2.2hu:%2.2hu ",
      strtime->tm_mday,strtime->tm_mon+1,strtime->tm_year,
      strtime->tm_hour,strtime->tm_min,strtime->tm_sec);
      }
   else
      {
      sprintf(puff,"%2.2hu%2.2hu%2.2hu%2.2hu%2.2hu%2.2hu",
      strtime->tm_mday,strtime->tm_mon+1,strtime->tm_year,
      strtime->tm_hour,strtime->tm_min,strtime->tm_sec);
      }
return(puff);
} /* end of dat_zeit_log() */

/*------------ Ende Hilfsfunktionen -------------------------*/

/***
--------------------------------------------------------------------------------
	Procedure	: fehler_meldung
	In		:
			  long nr,         0  nur Text 1 Ausgabe
			  long error,      Error
			  char *text,      Text
	Out		: void
	Funktion	: Fehler-Anzeige

--------------------------------------------------------------------------------
***/
void fehler_meldung(long nr,long err,char *text)
{
char tmpbuf[81];
int len;

memset(tmpbuf,0x20,80);
dat_zeit_log(tmpbuf,(short)1);
len = strlen(tmpbuf);
tmpbuf[len - 1] = '|';
if(nr == 0)
     sprintf(&tmpbuf[len]," %s",text);
else
     sprintf(&tmpbuf[len]," %03ld | %s",err,text);
len = strlen(tmpbuf);
tmpbuf[len] = 0x20;
tmpbuf[80]=0x00;
DEBUG(2,"%s\n",tmpbuf);
Eventprot.WriteFirstLine(tmpbuf);
return;
}

/**********************************************************************/
long status_host_schreiben(char * datei_name, char kommando)
{
FILE * flog;

flog = fopen(datei_name,"w+");
if(flog == NULL)
      {
      DEBUG(1,"Datei %s kann nicht geoeffnet werden: SCHREIBEN\n",datei_name);
      return (long)402;
      }

fwrite(&kommando,1,1,flog);

fclose(flog);
#ifdef UNIX
chmod(datei_name,(int)00666);
#endif
return (long)0;
} /* end of status_host_schreiben() */

/**********************************************************************/
long status_host_lesen(char *datei_name, struct STATUS_DATEN * daten)
{
FILE * flog;
char   buffor[80];
char   kommando;

flog = fopen(datei_name,R);
if(flog == NULL)
      {
      kommando = '*';
      daten->kommando = kommando;
      daten->ger_liste=0;
      return (long)400;
      }
daten->ger_liste=0;
fread(buffor,sizeof(buffor),1,flog);
if(buffor[0] >= 'a' && buffor[0] <= 'z')
  daten->kommando = buffor[0] & 0xdf;
else
  daten->kommando = buffor[0];

fclose(flog);
/**
chmod(datei_name,(int)00666);
**/
return (long)0;
} /* end of status_host_lesen() */

/*********************** usage() **************************/
void usage(char * pname)
{
if(version != 0)
	{
	printf("\n%s: Version %s Datum %s\n",
	pname,VERSION,DATUM);
	printf("Copyright (c)   SE.TEC   2000\n");
        if(hilfe == 0)
	        {
                printf("\n");
		if(testen == 0 && testlog == 0)
	                exit(0);
	        }
	else
	        {
                printf("\n\nENTER-Taste");
		getchar();
                printf("\n\n\n\n\n\n");
	        }
	}
if(hilfe != 0)
   {
   printf("\n\nAufruf:  %s [-vhtdeILPnolsXX] &\n\n",pname);
   printf("Parameter: -v : Version\n");
   printf("           -h : Hilfe\n");
   printf("           -t : Testausgaben auf Bildschirm\n");
   printf("           -d : Testausgaben in die Datei <%s%c%s.tst>\n",tmp,SLESH,pname);
   printf("           -e : Nur ein Durchlauf <Testzweck>\n");
   printf("           -I : INIT-Datei: <%s%c%s.cfg> editieren\n",etc,SLESH,pname);
   printf("           -L : Log-Datei <%s%c%s.log> lesen\n",tmp,SLESH,pname);
   printf("           -P : Protokoll-Dat <%s%c%s_TTMM.pro> vom heute lesen\n",tmp,SLESH,pname);
   printf("           -n : Neustart des Programmes nach Init <INIT-Datei>\n");
   printf("           -o : ohne Aufruf <Testzweck>\n");
   printf("           -l : Die Dateien werden nicht geloescht <nur Testen>\n");
   printf("           -sXX : Anzahl sek fuer sleep in der Schleife: Default 1 sec\n");

   printf("Log-Datei:       %s%c%s.log\n",tmp,SLESH,pname);
   printf("INIT-Datei:      %s%c%s.cfg\n",etc,SLESH,pname);
   printf("Protokoll-Datei: %s%c%s_TTMM.pro\n",tmp,SLESH,pname);
   exit(0);
   }
}/* end of usage() */

/******************************************************************/
void env_lesen ()
{
        tmp = getenv("TMPPATH");
        if (tmp == NULL)
           {
#ifndef UNIX
                    tmp = "C:\\USER\\FIT\\TMP";
#else
                    tmp = "/user/fit/tmp";
#endif
           }
        etc = getenv("BWSETC");
        if (etc == NULL)
           {
#ifndef UNIX
                    etc = "C:\\USER\\FIT\\ETC";
#else
                    etc = "/user/fit/etc";
#endif
           }
        bws = getenv("BWS");
        if (bws == NULL)
           {
#ifndef UNIX
                    bws = "C:\\USER\\FIT";
#else
                    bws = "/user/fit";
#endif
           }
return;
}/* end of env_lesen() */

/******************************************************************/
void IPL_datei(char * pname)
{
char editor[32];
DWORD Exit_Code;

#ifdef UNIX
   sprintf (editor,"vi");
#else
   sprintf (editor,"notepad");
#endif

DEBUG(1,"Log=%d Init=%d Protokoll=%d\n",Log,Init,Protokoll);
#ifndef UNIX
        Sleep (1000);
#else
        sleep (1);
#endif
if (Log || Protokoll || Init)
   {
   if (Log)
      {
      sprintf(buffer,"%s %s%c%s.log",editor,tmp,SLESH,pname);
      DEBUG(1,"%s\n",buffer);
      Exit_Code = ProcWaitExec(buffer, SW_SHOW, -1, 0, -1, 0);
      }
   if (Init)
      {
      DEBUG(1,"INIT\n");
      sprintf(char_zwi,"Datei %s%c%s.cfg ist editiert",etc,SLESH,pname);
      fehler_meldung((long)1,(long)0,char_zwi);
      sprintf(char_zwi,"Programm <%s> soll neu gestertet werden",pname);
      printf("%s\n",char_zwi);
      DEBUG(1,"%s\n",char_zwi);
      fehler_meldung((long)1,(long)0,char_zwi);
      status_daten.kommando = 'Z';
      error=status_host_schreiben(senden_status,status_daten.kommando);
      if(error != (long)0)
	   {
           sprintf(char_zwi,"%s: Kommand %c schreiben",
           senden_status,status_daten.kommando);
           fehler_meldung((long)1,(long)error,char_zwi);
	   }
      sprintf(buffer,"%s %s%c%s.cfg",editor,etc,SLESH,pname);
      DEBUG(1,"%s\n",buffer);
      Exit_Code = ProcWaitExec(buffer, SW_SHOW, -1, 0, -1, 0);
      /******************* Neustart *****************
      ******************* Neustart *****************/
      if (neustart)
         {
         if (testen)
            sprintf(buffer,"%s -t",pname);
         else
            sprintf(buffer,"%s",pname);
         Exit_Code = ProcExec(buffer, SW_SHOW, -1, 0, -1, 0);
         }
      }
   if (Protokoll)
      {
	  /* 04.09.02 FF */
      sprintf(buffer,"%s %s%c%s_%02d%02d.pro",editor,tmp,SLESH,pname,tag,monat);
      DEBUG(1,"%s\n",buffer);
      Exit_Code = ProcWaitExec(buffer, SW_SHOW, -1, 0, -1, 0);
      }
   exit(0);
   }
else
   return;
}/*end of IPL_datei() */

/******************************************************************/
MDN_CLASS mdn_class;
int MDN_CLASS::lese_mdn_kun(short);
extern void opendbase(char *);
extern void beginwork(void);
extern void commitwork(void);

void konfiguration_holen()
{
DEBUG(1,"Konfiguration aus der <%s.cfg>\n",PROGRAMM);
if(_getenv("MARKT") == NULL)
{
    /* 20.03.02 */
	int dsqlstatus;

    DEBUG(1,"MARKT aus DB  bws\n");
	opendbase("bws");
    beginwork();
	dsqlstatus = mdn_class.lese_mdn_kun((short)1);
	if (dsqlstatus == 0)
	{
		sprintf(markt,"%04ld",_mdn.kun);
        DEBUG(1,"mdn.kun=%ld\n",_mdn.kun);
	}
	else
		sprintf(markt,"9999");
	commitwork();
    /* 20.03.02 */
}
else
{
    DEBUG(1,"MARKT aus der Konfiguration-Datei\n");
	sprintf(markt,"%s",_getenv("MARKT"));
}
DEBUG(1,"markt=%s\n",markt);

if(_getenv("PFADHOST") == NULL)
   sprintf(pfadhost,"%s",tmp);
else
   sprintf(pfadhost,"%s",_getenv("PFADHOST"));
DEBUG(1,"pfadhost=%s\n",pfadhost);

/****
if(_getenv("PFADKASSE") == NULL)
   sprintf(pfadkasse,"%s",tmp);
else
   sprintf(pfadkasse,"%s",_getenv("PFADKASSE"));
DEBUG(1,"pfadkasse=%s\n",pfadkasse);
****/

if(_getenv("DATHOST1") == NULL)
   {
   sprintf(pdathost1,"%s%cdathost1",pfadhost,SLESH);
   strcpy(dathost1,"dathost1");
   }
else
   {
   sprintf(pdathost1,"%s%c%s",pfadhost,SLESH,_getenv("DATHOST1"));
   strcpy(dathost1,_getenv("DATHOST1"));
   /* 20.03.02 FF A */
   if (strstr(dathost1,"MMMM") != NULL)
   {
       strncpy(strstr(pdathost1,"MMMM"),markt,4);
       strncpy(strstr(dathost1,"MMMM"),markt,4);
	   strcpy (datmust1,dathost1);
	   strcpy (strstr(datmust1,"."),".");
	   DEBUG(1,"datmust1=%s\n",datmust1);
   }
   /* 20.03.02 FF E */
   }
DEBUG(1,"pdathost1=%s\ndathost1=%s\n",pdathost1,dathost1);

if(_getenv("DATHOST2") == NULL)
   {
   sprintf(pdathost2,"%s%cdathost2",pfadhost,SLESH);
   strcpy(dathost2,"dathost2");
   }
else
   {
   sprintf(pdathost2,"%s%c%s",pfadhost,SLESH,_getenv("DATHOST2"));
   strcpy(dathost2,_getenv("DATHOST2"));
   /* 20.03.02 FF A */
   if (strstr(dathost2,"MMMM") != NULL)
   {
       strncpy(strstr(pdathost2,"MMMM"),markt,4);
       strncpy(strstr(dathost2,"MMMM"),markt,4);
	   strcpy (datmust2,dathost2);
	   strcpy (strstr(datmust2,"."),".");
	   DEBUG(1,"datmust2=%s\n",datmust2);
   }
   /* 20.03.02 FF E */
   }
DEBUG(1,"pdathost2=%s\ndathost2=%s\n",pdathost2, dathost2);

if(_getenv("DATKASSE") == NULL)
   sprintf(datkasse,"%s%cdatkasse",pfadkasse,SLESH);
else
   sprintf(datkasse,"%s%c%s",pfadkasse,SLESH,_getenv("DATKASSE"));
DEBUG(1,"datkasse=%s\n",datkasse);

if(_getenv("TIMERHOST") == NULL)
   timerhost = TIMER;
else
   timerhost = atoi(_getenv("TIMERHOST"));
DEBUG(1,"timerhost=%d\n",timerhost);

if(_getenv("TIMERKASSE") == NULL)
   timerkasse = TIMER;
else
   timerkasse = atoi(_getenv("TIMERKASSE"));
DEBUG(1,"timerkasse=%d\n",timerkasse);
if(_getenv("NEUSTART") == NULL)
   neustart = 0;
else
   neustart =(short) atoi(_getenv("NEUSTART"));
DEBUG(1,"neustart=%d\n",neustart);
return;
}/*end of konfiguration_holen() */

/******************************************************************/
void verzeichnis_loeschen()
{
    time_t timer;
    struct tm *ltime;
	char verz [64];
	char zwi [64];
    char * p;
	int i;
	FILE * fp;

        time (&timer);
        ltime = localtime (&timer);

#ifndef TESTEN
        if (ltime->tm_hour > 0 && verz_geloescht == 1)
           verz_geloescht = 0; 
#endif /* TESTEN */

        if (ltime->tm_hour == 0 && verz_geloescht == 0)
        {
           /* Loeschen der Dateien im Verzeichnis */
           sprintf (verz, "%s%c%s", tmp, SLESH, days [ltime->tm_wday]);
           sprintf(char_zwi,"Verzeichnis %s wird geloescht",verz);
           fehler_meldung((long)1,(long)0,char_zwi);
           DEBUG(1,"%s\n",char_zwi);
           strcpy (zwi,dathost1);
           /* 20.03.02 FF
		   p = strstr(zwi,".ASC");
           if (p == NULL)
               {
               p = strstr(zwi,".asc");
               if (p == NULL)
                   return;  
               }
           */
		   p = strstr(zwi,".yyyy");
// 051202
           if (p == NULL)
		   {
 		        p = strstr(zwi,".");
		   }
           if (p != NULL)
		   {
                strcpy (p,"*.*");
		   }
		   else
		   {
                strcpy (zwi,"*.*");
		   }

           sprintf(buffer,"%s %s%c%s",DEL, verz, SLESH, zwi);
           DEBUG(1,"buffer=%s\n",buffer);
           system(buffer);
           strcpy (zwi,dathost2);
           /* 20.03.02 FF
           p = strstr(zwi,".ASC");
           if (p == NULL)
               {
               p = strstr(zwi,".asc");
               if (p == NULL)
                   return;  
               }
           */
		   p = strstr(zwi,".yyyy");
// 051202
           if (p == NULL)
		   {
 		        p = strstr(zwi,".");
		   }
           if (p != NULL)
		   {
                strcpy (p,"*.*");
		   }
		   else
		   {
                strcpy (zwi,"*.*");
		   }
           sprintf(buffer,"%s %s%c%s",DEL, verz, SLESH, zwi);
           DEBUG(1,"buffer=%s\n",buffer);
           system(buffer);
           verz_geloescht = 1; 
           
		   /* 04.09.02 FF */
		   tag = ltime->tm_mday;
		   monat = ltime->tm_mon + 1;

           dat_zeit_log(char_zwi,(short)7);
    	   char_zwi[10] = 0x00;
		   sprintf(datum,char_zwi);
           ldat = dasc_to_long (datum);
		   ldat = ldat - 14;
		   dlong_to_asc (ldat,ldatum);

		   sprintf (buffer,"%s%c%s_%c%c%c%c.pro",tmp,SLESH,PROGRAMM,ldatum[0],ldatum[1],ldatum[3],ldatum[4]);
		   unlink(buffer);
		   sprintf(char_zwi,"%s geloescht",buffer);
           fehler_meldung((long)1,(long)0,char_zwi);
           
		   for (i=0; i<7; i++)
		   {
		       ldat = ldat - 1;
		       dlong_to_asc (ldat,ldatum);

		       sprintf (buffer,"%s%c%s_%c%c%c%c.pro",tmp,SLESH,PROGRAMM,ldatum[0],ldatum[1],ldatum[3],ldatum[4]);
		       fp = fopen(buffer , "r");
			   if (fp != NULL)
			   {
				   fclose(fp);
    			   unlink(buffer);
	    	       sprintf(char_zwi,"%s geloescht",buffer);
                   fehler_meldung((long)1,(long)0,char_zwi);
			   }
		   }
           /* 04.09.02 FF */
		}

#ifdef TESTEN
        if (ltime->tm_hour >= 10 && verz_geloescht == 0)
           {
           DEBUG(1,"STUNDE=%d verz_geloescht=%d\n",ltime->tm_hour,verz_geloescht);
           /* Loeschen der Dateien im Verzeichnis */
           sprintf (verz, "%s%c%s", tmp, SLESH, days [ltime->tm_wday]);
           sprintf(char_zwi,"Verzeichnis %s wird geloescht",verz);
           fehler_meldung((long)1,(long)0,char_zwi);
           DEBUG(1,"%s\n",char_zwi);
           strcpy (zwi,dathost1);
           /* 20.03.02 FF
		   p = strstr(zwi,".ASC");
           if (p == NULL)
               {
               p = strstr(zwi,".asc");
               if (p == NULL)
                   return;  
               }
           */
		   p = strstr(zwi,".yyyy");
// 051202
           if (p == NULL)
		   {
 		        p = strstr(zwi,".");
		   }
           if (p != NULL)
		   {
                strcpy (p,"*.*");
		   }
		   else
		   {
                strcpy (zwi,"*.*");
		   }
           sprintf(buffer,"%s %s%c%s",DEL, verz, SLESH, zwi);
           DEBUG(1,"buffer=%s:\n",buffer);
           system(buffer);
           strcpy (zwi,dathost2);
           /* 20.03.02 FF
		   p = strstr(zwi,".ASC");
           if (p == NULL)
               {
               p = strstr(zwi,".asc");
               if (p == NULL)
                   return;  
               }
           */
		   p = strstr(zwi,".yyyy");
// 051202
           if (p == NULL)
		   {
 		        p = strstr(zwi,".");
		   }
           if (p != NULL)
		   {
                strcpy (p,"*.*");
		   }
		   else
		   {
                strcpy (zwi,"*.*");
		   }
           sprintf(buffer,"%s %s%c%s",DEL, verz, SLESH, zwi);
           DEBUG(1,"buffer=%s:\n",buffer);
           system(buffer);
           verz_geloescht = 1; 
           }
#endif /* TESTEN */

        return;
} /* verzeichnis_loeschen () */

/******************************************************************/
void log_starten(short par)
{
if (par == 0)
    {
        sprintf(char_zwi,">>>>>>> START >>>> %s  Vers. %s >>>> %s >>>>>>>>",
        PROGRAMM,VERSION,DATUM);
	    status_daten.kommando = '*';
        error=status_host_schreiben(senden_status,status_daten.kommando);
        if(error != (long)0)
		{
            fehler_meldung((long)par,(long)error,char_zwi);
	        sprintf(char_zwi,"%s: Kommand %c schreiben",
	        senden_status,status_daten.kommando);
            fehler_meldung((long)1,(long)error,char_zwi);
	    }
    }
else
    {
        memset(char_zwi,'<',55);
        memcpy(&char_zwi[24]," ENDE ",6);
        char_zwi[55] = '\0';
        DEBUG(1,"\n");
    }
fehler_meldung((long)par,(long)error,char_zwi);
return;
}
/******************************************************************/
long bearbeitung_dat1 ()
{
int i;
long err=0;
int exit;
char zwi[64];
DWORD Exit_Code;
char * p;

      /* 20.03.02 */
 	  sprintf(pdathost1b,"%s",pdathost1);
      p = strstr(pdathost1b,"ASC");
      if (p == NULL)
	  {
          p = strstr(pdathost1b,"asc");
          if (p == NULL)
		  {
             sprintf(char_zwi,"Datnam %s FALSCH",pdathost1);
             printf("%s 1\n",char_zwi);
             fehler_meldung((long)1,(long)100,char_zwi);
             return  (1);
		  }
	  }
      DEBUG(1,"Host: Endung1: p=%s\n",p);
      strcpy (p,"CW");
      DEBUG(1,"unlink %s\n",pdathost1b);
      /* 20.03.02 */
	  unlink(pdathost1b);
      DEBUG (1,"rename %s %s\n",pdathost1, pdathost1b);
      exit = rename(pdathost1, pdathost1b);
      DEBUG (1,"exit=%d\n",exit);
      if(exit != 0)
      {
        err=101;
        sprintf(char_zwi,"Error bei RENAME %s",pdathost1);
        printf("%s\n",char_zwi);
        fehler_meldung((long)1,err,char_zwi);
      }
      else
      {
		/* BEARBEITUNG */
        for (i=0;;i++)
        {
           if (udat1[i].satz_typ == 0)
                 break;
           if( udat1[i].inp_dat == NULL)
		   {
              sprintf(buffer,"%s%cbin%c%s %s %s %s",bws,SLESH,SLESH,
              udat1[i].run_funk,pdathost1b, udat1[i].if_dat, udat1[i].par);
           }
		   else
		   {
			  sprintf(zwi,"%s%c%s",tmp,SLESH,udat1[i].inp_dat);
			  sprintf(buffer,"%s%cbin%c%s %s %s %s",bws,SLESH,SLESH,
              udat1[i].run_funk,zwi,udat1[i].if_dat, udat1[i].par);
           }
		   DEBUG(1,"TYP=%d: %s:\n",udat1[i].satz_typ,buffer);
           Exit_Code = ProcWaitExec(buffer, SW_SHOW, -1, 0, -1, 0);
           if(Exit_Code != (DWORD) 0)
           {
              if (err == 0) err = (long) 200;
              sprintf(char_zwi,"Bearbeitung: Satztyp %2d: <%s> ERROR",
              udat1[i].satz_typ,udat1[i].if_dat);
              fehler_meldung((long)1,(long)Exit_Code,char_zwi);
           }
		   else
		   {
		      if( udat1[i].inp_dat != NULL)
                  SaveDatei(zwi, udat1[i].inp_dat);
		   }
		}/* end of for */
      SaveDatei(pdathost1b, dathost1);
      sprintf(char_zwi,"Bearb: Host1 %s",dathost1);
      fehler_meldung((long)1,err,char_zwi);
      }
return (err);
} /* end of bearbeitung_dat1() */
/******************************************************************/

long bearbeitung_dat2 ()
{
int i;
long err=0;
int exit;
char zwi[64];
DWORD Exit_Code;
char * p;

      /* 20.03.02 */
 	  sprintf(pdathost2b,"%s",pdathost2);
      p = strstr(pdathost2b,"ASC");
      if (p == NULL)
	  {
          p = strstr(pdathost2b,"asc");
          if (p == NULL)
		  {
             sprintf(char_zwi,"Datnam %s FALSCH",pdathost2);
             printf("%s 1\n",char_zwi);
             fehler_meldung((long)1,(long)100,char_zwi);
             return  (1);
		  }
	  }
      DEBUG(1,"Host: Endung2: p=%s\n",p);
      strcpy (p,"CW");
      DEBUG(1,"unlink %s\n",pdathost2b);
      /* 20.03.02 */
      unlink (pdathost2b);
      DEBUG (1,"rename %s %s\n",pdathost2, pdathost2b);
      exit = rename(pdathost2, pdathost2b);
	  DEBUG (1,"exit=%d\n",exit);
      if(exit !=  0)
        {
        err=102;
        sprintf(char_zwi,"Error bei RENAME %s",pdathost2);
        printf("%s\n",char_zwi);
        fehler_meldung((long)1,err,char_zwi);
        }
      else
        {
        /* BEARBEITUNG */
        for (i=0;;i++)
           {
           if (udat2[i].satz_typ == 0)
                 break;
           if( udat2[i].inp_dat == NULL)
              {
              sprintf(buffer,"%s%cbin%c%s %s %s %s",bws,SLESH,SLESH,
              udat2[i].run_funk,pdathost2b, udat2[i].if_dat, udat2[i].par);
              }
           else
              {
			  sprintf(zwi,"%s%c%s",tmp,SLESH,udat1[i].inp_dat);
              sprintf(buffer,"%s%cbin%c%s %s %s %s",bws,SLESH,SLESH,
              udat2[i].run_funk,zwi,udat2[i].if_dat, udat2[i].par);
              }
           DEBUG(1,"TYP=%d: %s:\n",udat2[i].satz_typ,buffer);
           Exit_Code = ProcWaitExec(buffer, SW_SHOW, -1, 0, -1, 0);
           if(Exit_Code != (DWORD) 0)
              {
              if (err == 0) err = (long) 200;
              sprintf(char_zwi,"Bearbeitung: Satztyp %2d: <%s> ERROR",
              udat2[i].satz_typ,udat2[i].if_dat);
              fehler_meldung((long)1,(long)Exit_Code,char_zwi);
              }
		   else
		      if( udat2[i].inp_dat != NULL)
                  SaveDatei(zwi, udat2[i].inp_dat);
		} /* end of for() */

        SaveDatei(pdathost2b, dathost2);
        sprintf(char_zwi,"Bearb: Host2 %s",dathost2);
        fehler_meldung((long)1,err,char_zwi);
        }

return (err);
} /* end of bearbeitung_dat2() */

/******************************************************************/
long bearbeitung (int * k)
{
long err=0;
FILE * fp;
short bearb=0;

/* 20.03.02 FF A */
FILE * zfp;
DWORD Exit_Code;
char datnam[96];
char zwidat1[96];
char zwidat2[96];
char * p;
char *p1;
char *p2;

sprintf (zwidat1,"%s\\sapdat1.zwi",tmp);
sprintf (zwidat2,"%s\\sapdat2.zwi",tmp);

strcpy (datnam, pdathost1);
//if ((p=strstr(datnam,"yyyymmdd.hhmmss")) != 0)
if ((p=strstr(datnam,"yyyymmdd")) != 0)
    strcpy(p,"*");
p1 = p;
DEBUG(1,"datnam=%s\n",datnam);
sprintf (buffer,"DIR /OD %s.asc > %s",datnam,zwidat1);
DEBUG(1,"buffer=%s\n",buffer);
unlink (zwidat1);
Exit_Code = system(buffer);
if(Exit_Code != (DWORD) 0)
{
      DEBUG(1,"EXIT_CODE=%d\n",(int)Exit_Code);
}
else
{
      DEBUG(1,"EXIT_CODE=%d\n",(int)0);
      zfp = fopen (zwidat1, "r");
	  if (zfp != NULL)
	  {
		  while ((p = fgets (buffer, 128, zfp)) != NULL)
		  {
	          p = strstr (buffer, datmust1);
			  if (p == NULL)
				  continue;
			  p[strlen(p) - 1] = 0x00;
              sprintf(pdathost1,"%s%c%s",pfadhost,SLESH,p);
			  sprintf(dathost1,p);
			  DEBUG(1,"Datei=%s:\n",pdathost1);
              fp = fopen (pdathost1, "r");
              if (fp != NULL)
			  {
                  fclose(fp);
                  if(bearb++ == 0) DEBUG(1,"BEARBEITUNG\n");
                  err = bearbeitung_dat1();
                  *k = 0;
			  }
			  else
			  {
				  DEBUG(1,"fp=NULL\n");
			  }
		  }
	  }
}


strcpy (datnam, pdathost2);
//if ((p=strstr(datnam,"yyyymmdd")) != 0)
if ((p=strstr(datnam,"yyyymmdd")) != 0)
    strcpy(p,"*");
p2 = p;
DEBUG(1,"datnam=%s\n",datnam);
sprintf (buffer,"DIR /OD %s.asc > %s",datnam,zwidat2);
DEBUG(1,"buffer=%s\n",buffer);
unlink (zwidat2);
Exit_Code = system(buffer);
if(Exit_Code != (DWORD) 0)
{
      DEBUG(1,"EXIT_CODE=%d\n",(int)Exit_Code);
}
else
{
      DEBUG(1,"EXIT_CODE=%d\n",(int)0);
      zfp = fopen (zwidat2, "r");
	  if (zfp != NULL)
	  {
		  while ((p = fgets (buffer, 128, zfp)) != NULL)
		  {
	          p = strstr (buffer, datmust2);
			  if (p == NULL)
				  continue;
			  p[strlen(p) - 1] = 0x00;
              sprintf(pdathost2,"%s%c%s",pfadhost,SLESH,p);
			  sprintf(dathost2,p);
			  DEBUG(1,"Datei=%s:\n",pdathost2);
              fp = fopen (pdathost2, "r");
              if (fp != NULL)
			  {
                  fclose(fp);
                  if(bearb++ == 0) DEBUG(1,"BEARBEITUNG\n");
                  err = bearbeitung_dat2();
                  *k = 0;
			  }
			  else
			  {
				  DEBUG(1,"fp=NULL\n");
			  }
		  }
	  }
}
/* 20.03.02 FF E */

#ifdef ALT
if (p1 == NULL)
{
  fp = fopen (pdathost1, "r");
  if (fp != NULL)
   {
   fclose(fp);
   if(bearb++ == 0) DEBUG(1,"BEARBEITUNG\n");
   err = bearbeitung_dat1();
   *k = 0;
   }
}

if (p2 == NULL)
{
  fp = fopen (pdathost2, "r");
  if (fp != NULL)
   {
   fclose(fp);
   if(bearb++ == 0) DEBUG(1,"BEARBEITUNG\n");
   err = bearbeitung_dat2();
   *k = 0;
   }
}
#endif /* ALT */

return (err);
} /* end of bearbeitung() */

/************************* main ***********************************/
int main (int varanz, char *varargs [])
{
int i;
int k=0;
time_t uxtime;
time_t letzte_bearb=0;
long timer = 0;
char * p;
struct tm *ltime;
    
	step = STEP;
        /** Parameter  lesen **/
        if(varanz >= 2)
             for (i = 1; i < varanz; i ++)
                {
                if (varargs [i] [0] == '-')
                         {
                         tst_arg (&varargs[i][1]);
                         varanz = scroll_arg (varargs,i,varanz);
                         i --;
                         }
                }

        /* Environment Variablen lesen */
        env_lesen ();

        if(version != 0 || hilfe != 0)
        	usage(PROGRAMM);

        sprintf(logbuch,"%s%c%s.log",tmp,SLESH,PROGRAMM);
	sprintf(senden_status,"%s%c%s",tmp,SLESCH,STATUS_DAT);

        if(getenv("testmode") != NULL)
           {
              tst=(short)atoi(getenv("testmode"));
              if(tst > 1)
			  {testlog= 1; testen = 0;}
              else
                if(tst == 1)
				{testlog= 0; testen = 1;}
	            else
				{testlog= 0; testen = 0;}
           }
        sprintf(char_zwi,"%s%c%s.tst",tmp,SLESCH,PROGRAMM);
        if(testlog)
	        {
            flog = fopen(char_zwi,"w+");
	        if(flog != NULL)
                DEBUG(1,"TEST-LOG ist geoeffnet\n");
	        }
        DEBUG(1,"tmp=%s\n",tmp);
        DEBUG(1,"etc=%s\n",etc);
        DEBUG(1,"bws=%s\n",bws);
        DEBUG(1,"status_datei=%s\n",senden_status);

		time (&uxtime);
        ltime = localtime (&uxtime);
        
		tag = ltime->tm_mday;
		monat = ltime->tm_mon + 1;
		DEBUG(1,"tag=%d monat=%d\n",tag,monat);

        /* Konfiguration aus der Datei sapfit.cfg holen */
        konfiguration_holen();

        /* Parameter I, L, P i ausfuehren */
        IPL_datei(PROGRAMM);

        /* Log-Datei starten */
        log_starten(0);

/*** Temporaere Dateinamen Vorbereiten A ***/
sprintf(pdathost1b,"%s",pdathost1);
sprintf(pdathost2b,"%s",pdathost2);
p = strstr(pdathost1b,"ASC");
if (p == NULL)
   {
   p = strstr(pdathost1b,"asc");
   if (p == NULL)
       {
       sprintf(char_zwi,"Datnam %s FALSCH",pdathost1);
       printf("%s 1\n",char_zwi);
       fehler_meldung((long)1,(long)100,char_zwi);
       exit (1);
       }
   }
strcpy(endung1,p);
DEBUG(1,"\nHost: Endung1=%s\n",endung1);
strcpy (p,"CW");

p = strstr(pdathost2b,"ASC");
if (p == NULL)
   {
   p = strstr(pdathost2b,"asc");
   if (p == NULL)
       {
       sprintf(char_zwi,"Datnam %s FALSCH",pdathost2);
       printf("%s 2\n",char_zwi);
       fehler_meldung((long)1,(long)100,char_zwi);
       exit (1);
       }
   }
strcpy(endung2,p);
DEBUG(1,"Host: Endung2=%s\n",endung2);
strcpy (p,"CW");
DEBUG(1,"pdathost1b=%s\npdathost2b=%s\n",pdathost1b,pdathost2b);

sprintf(dathost1b,"%s",dathost1);
sprintf(dathost2b,"%s",dathost2);
p = strstr(dathost1b,"ASC");
if (p == NULL)
   {
   p = strstr(dathost1b,"asc");
   if (p == NULL)
       {
       sprintf(char_zwi,"Datnam %s FALSCH",dathost1b);
       printf("%s 3\n",char_zwi);
       fehler_meldung((long)1,(long)100,char_zwi);
       exit (1);
       }
   }
//DEBUG(1,"1 Endung: p=%s dathost1b=%s\n",p,dathost1b);
strcpy (p,"CW");

p = strstr(dathost2b,"ASC");
if (p == NULL)
   {
   p = strstr(dathost2b,"asc");
   if (p == NULL)
       {
       sprintf(char_zwi,"Datnam %s FALSCH",dathost2b);
       printf("%s 4\n",char_zwi);
       fehler_meldung((long)1,(long)100,char_zwi);
       exit (1);
       }
   }
//DEBUG(1,"2 Endung: p=%s\n",p);
strcpy (p,"CW");
DEBUG(1,"dathost1b=%s\ndathost2b=%s\n",dathost1b,dathost2b);
/*** Temporaere Dateinamen Vorbereiten  E ***/

timer = (long)timerhost;
DEBUG(1,"ANFANG timer=%ld\n",timer);
/*** Bearbeitungsschleife ***/
while (TRUE)
        {
        verzeichnis_loeschen();
        error=status_host_lesen(senden_status,&status_daten);
        if(error != (long)0)
	     {
	     sprintf(char_zwi,"%s: nicht vorh./kein Zugriff",
	     senden_status);
             fehler_meldung((long)1,(long)error,char_zwi);
             }
	if (status_daten.kommando == 'Z'|| status_daten.kommando == 'E')
             break;

        time ((time_t *) &uxtime);

        if ((long)uxtime >=(long)((long)letzte_bearb + timer))
             {
             letzte_bearb = uxtime;
             error = bearbeitung(&k);
/*
             fehler_meldung((long)1,(long)error,"Bearbeitung");
             DEBUG(1,"\nBEARBEITUNG %ld %ld\n",uxtime,letzte_bearb);
*/
             DEBUG(1,"B",k); k++;
             }

#ifndef UNIX
        Sleep (step * 1000);
#else
        sleep (step);
#endif

        if(testen)
              {
              k++; DEBUG(1,".",k);
              if (k%50 == 0) DEBUG(1,"\n");
              }

        if (einzeln)
              break;
        } /* end of while(1) */

/* Log-Datei beenden */
log_starten(1);

return 0;
} 
