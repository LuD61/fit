#ifndef _WINTYPEDEF
#define _WINTYPEDEF
#define LPSTR char *
#define DWORD unsigned long
#define WORD unsigned short
#define TRUE 1
#define FALSE 0
#define SW_SHOW 0 
#define SW_HIDE 0 

extern long nap (long);

#define Sleep nap

#define BOOL unsigned int

#endif
