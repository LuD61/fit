typedef struct
       {   char  feldname [20];
	   short feldlen;
	   char  feldtyp [2]; 
	   short nachkomma;
	   short dimension;
       } FELDER;

typedef struct
       {   FELDER *felder;
           char  **buffer;
       } RECORD;


class REC_CLASS {
         private :
               char wert1 [1024];
               char wert2 [1024];

               void rstrncpy (char *, char *, short);
               int nachkomma (double *, FELDER *);
               void dtformat (char *, char *, char *);
               void dkformat (char *, FELDER *);
               int instruct (FELDER *, char *);
               int feldpos (FELDER *, char *);
               int hole_wert (char *,char *,FELDER *, short, char *);
               int setze_wert (char *,char *,FELDER *, short, char *);
               void bintoasc (char *, char *, short); 
               void asctobin (char *, char *, short); 
               void sformat (char *, FELDER *);
               void lformat (char *format, FELDER *feld);
               void dformat (char *format, FELDER *feld);
               int cnachkomma (char *, FELDER *);
               int snachkomma (short *, FELDER *);
               int lnachkomma (long *, FELDER *);
               int dnko (double *, FELDER *);
               void dnformat (char *, FELDER *);

         public :
               int satzlen (FELDER *felder);
               int rec_wert (RECORD *, char *, char *, short);
               int set_rec_wert (RECORD *, char *, char *, short);
};
