
/* ************************************************************************* */
/*                                                                           */
/*    Manager fuer Bauerngut                     fitkasse.cpp                */
/*    Erstellungsdatum 11.10.00                                              */
/*    Autor  F.Folmer                                                        */
/*                                                                           */
/*    Aenderung: 20.03.02                                                    */
/*                                                                           */
/*                                                                           */
/* ************************************************************************* */

#define VERSION "1.22"
#define DATUM "12.11.2002"

/* 1.00 11.10.00 Hauptprogramm                                               */
/* 1.10 20.02.01 exit vom bws_asc <> 0 , Daten ignoriert                     */
/* 1.20 20.03.02 Dateinamen mit Datum/Zeit   yyyymmdd.hhmmss                 */
/* 1.21 04.09.02 Protokoll <fitkasse_TTMM.pro> nach 2 Wo l�schen             */
/* 1.22 12.11.02 LS werden updated nach getrenntem Ablauf "lsk_zwik.dat"     */

#ifdef UNIX
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/types.h>
#include <prototypes.h>
#include "windtype.h"
#else
#include <direct.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include <windows.h>
#endif
#include "dbclass.h"
#include "mo_flog.h"
#include "stdfkt.h"
#include "conf_env.h"
#include "mdn.h"

EventProt Eventprot (1000, "fitkasse.log");
char ENV_DATEI[16] = {"fitkasse.cfg"};
char PROGRAMM[10] = {"fitkasse"};
#define HOUTDAT  "koutdat"

#ifdef UNIX
#define SLESH   (char) 0x2f
#define SLESCH   (char) 0x2f
#define DEL      "rm"
#else
#define SLESH   (char) 0x5c
#define SLESCH   (char) 0x5c
#define DEL      "del"
#endif
#define STATUS_DAT   "fitkasse.sta"
#define STEP     (int)1 

struct STATUS_DATEN
	  {
	  char    kommando;
	  short   ger_liste;
	  };
struct  STATUS_DATEN status_daten;
struct tm * strtime;

struct UDAT {
             int    satz_typ;
             char * inp_dat;
             char * if_dat;
             char * par;
             char * run_funk;
             char * zus_funk;
            };

struct UDAT udat1[] =
   {
    29, NULL, "s29lsk1.if","", "bws_asc", "",
    29, "lsk_zwik.dat", "s29lsk.if","-e", "bws_asc", "",
     0, "", "","", "", ""};

#ifndef UNIX
 #define R       "r"
 #define RPLUS   "r+"
 #define W       "w"
 #define WPLUS   "w+"
 #define BR      "rb"
 #define BRPLUS  "rb+"
 #define BW      "wb"
 #define BWPLUS  "wb+"
#else
 #define  R       "r"
 #define  RPLUS   "r+"
 #define  W       "w"
 #define  WPLUS   "w+"
 #define  BR      "r"
 #define  BRPLUS  "r+"
 #define  BW      "w"
 #define  BWPLUS  "w+"
#endif

#define BSLESCH        (char)0x5c 
#define BSLESH         (char)0x5c 

#define TIMER    (int)30
char markt[8];
char pfadhost[64];
char kassename[64];
char pfadkasse[64];
char hostname[64];
char dathost1[96];
char dathost2[96];
char pdatkasse[96];
char datkasse[96];
char datmust[32];
char *tmp;
char *etc;
char *bws;
char buffer [256];
char char_zwi [256];
char ftpdat[64];
char ftpdats[64];
char ftppar[16];
char outdat[64];
char outtmp[64];
char hosttmp[64];
int  timerhost=0;
int  timerkasse=0;
int  ftp=0;
char slesh;
char slesch;

static char *days [] = {"Son", "Mon", "Die", "Mit", "Don", "Fre", "Sam", NULL};
short verz_geloescht = 0;    /* Marker   */

FILE * flog;
char logpath[64];
char senden_status[64];
char logbuch[64];
int   tst=0;
int warte_zeit;
int step;
short neustart=0;
short Log=0;
short Protokoll=0;
short Init=0;
short testen=0;
short einzeln=0;
short loeschen=1;
short ohne_aufruf=0;
short testlog=0;
short hilfe=0;
short version=0;
long error=0;
long exit_code=0;
int  wiederholung = 0;         /* Wiederholungszaehler */
int  tag=0;                   /* 04.09.02 FF */
int  monat=0;                 /* 04.09.02 FF */
char datum[11];               /* 04.09.02 FF */
char ldatum[11];              /* 04.09.02 FF */
long ldat=0;                  /* 04.09.02 FF */

/*  Prototyp */
int  dlong_to_asc (long, char *);
long dasc_to_long (char *);



/*  Prototyp */
char * _getenv (char *);
void DEBUG(int tst, char *format, ...);
long status_host_lesen(char*, struct STATUS_DATEN *);
long status_host_schreiben(char *, char);



/***************************************************************************/
/* Procedure :            DEBUG                                            */
/*-------------------------------------------------------------------------*/
/* Funktion :             debug trace output                               */
/*-------------------------------------------------------------------------*/
/* Kurzbeschreibung :                                                      */
/*                                                                         */
/*                                                                         */
/*                                                                         */
/*-------------------------------------------------------------------------*/
/* Aufruf :               DEBUG (level,format, ...)                        */
/* Funktionswert :        ----                                             */
/* Eingabeparameter :     char *format: format string to control           */
/*                                      data format/number of arguments    */
/*                                                                         */
/* Ausgabeparameter :                                                      */
/*                                                                         */
/*-------------------------------------------------------------------------*/

void DEBUG (int tst, char *format, ...)
{
va_list args;
va_start(args, format);

if((testen == 0 && testlog == 0) || tst == 0)
  return;

  if(testen)
       {
       vfprintf(stdout,format, args);
       fflush(stdout);
       }
  if(testlog != 0)
       {
       vfprintf(flog,format, args);
       fflush(flog);
       }

  va_end(args);

 return;
}

/*------------------------------------------------------------------*/
/*                     Hilfsfunktionen                              */

int tst_arg (char * arg)
/**
Argumente nach '-' testen.
**/
{
for (;*arg; arg += 1)
       {
       switch (*arg)
               {
               case 't' :
		       if(testlog == 0)
                            testen = 1;
                       break;
               case 'e' :
                       einzeln = 1;
                       break;
               case 'n' :
                       neustart = 1;
                       break;
               case 'h' :
                       hilfe = 1;
                       break;
               case 'd' :
                       testlog = 1;
                       testen = 0;
                       break;
               case 'v' :
                       version = 1;
                       break;
               case 'o' :
                       ohne_aufruf = 1;
                       break;
               case 's' :
		       step = (int)atoi(arg + 1);
                       break;
               case 'w' :
		       warte_zeit = (int)atoi(arg + 1);
                       break;
               case 'l' :
                       loeschen = 0;
                       break;
               case 'I' :
                       Init = 1;
                       break;
               case 'L' :
                       Log = 1;
                       break;
               case 'P' :
                       Protokoll = 1;
                       break;
               default:
                       break;
               }
       }
return(0);
}

int scroll_arg (char *arg[], int pos, int anz)
/**
Argumenete nach links scrollen.
**/
{
anz --;
for (;pos < anz; pos ++)
       {
       arg [pos] = arg [pos + 1];
       }
return (anz);
};
/*********** Datei sichern.  ***********/
void SaveDatei (char * quelle_pfad_dat, char * qdat)
{
        time_t timer;
        struct tm *ltime;
        char quelle [64];
        char ziel [64];
        char datei [32];
//        char zwi[8];

        time (&timer);
        ltime = localtime (&timer);

        sprintf (buffer, "%s%c%s", tmp, SLESH, days [ltime->tm_wday]);
        DEBUG(1,"Verz: buffer=%s\n",buffer);

//        sprintf(zwi,"%02d%02d",ltime->tm_hour,ltime->tm_min);

        strcpy (datei,qdat);

//        strcat(datei,zwi);

		strcpy (quelle,quelle_pfad_dat);
  		sprintf (ziel,"%s%c%s", buffer, SLESH, datei);
      
	DEBUG (1,"Copy %s %s\n",quelle,ziel);
	CopyFile (quelle, ziel, FALSE);

} /* end of SaveDatei () */

/***
--------------------------------------------------------------------------------
##D8A
	Procedure	: dat_zeit_log()
	In		: char * char_zwi
			  short  parameter   0 Ausgabe ohne Trennzeichen
			                     1 Ausgabe mit Trennzeichen
	Out		: char * char_zwi
	Funktion	: Prozedur liefert Datum und Uhr als String.

##D8E
--------------------------------------------------------------------------------
***/
char * dat_zeit_log(char *puff,short parameter)
{
time_t uxtime;
   time((time_t *) &uxtime);
   strtime = localtime((time_t *) &uxtime); /* aktuelle Zeit holen */

/* Jahr 2000 FF */
if(strtime->tm_year >= 100)
    strtime->tm_year = strtime->tm_year % 100;
/* Jahr 2000 FF */

   /* 04.09.02 */
   if(parameter == 7)
      {
      sprintf(puff,"%2.2hu.%2.2hu.20%2.2hu %2.2hu:%2.2hu:%2.2hu ",
      strtime->tm_mday,strtime->tm_mon+1,strtime->tm_year,
      strtime->tm_hour,strtime->tm_min,strtime->tm_sec);
      return(puff);
      }

   if(parameter == 1)
      {
      sprintf(puff,"%2.2hu.%2.2hu.%2.2hu %2.2hu:%2.2hu:%2.2hu ",
      strtime->tm_mday,strtime->tm_mon+1,strtime->tm_year,
      strtime->tm_hour,strtime->tm_min,strtime->tm_sec);
      }
   else
      {
      sprintf(puff,"%2.2hu%2.2hu%2.2hu%2.2hu%2.2hu%2.2hu",
      strtime->tm_mday,strtime->tm_mon+1,strtime->tm_year,
      strtime->tm_hour,strtime->tm_min,strtime->tm_sec);
      }
return(puff);
} /* end of dat_zeit_log() */

/*------------ Ende Hilfsfunktionen -------------------------*/

/***
--------------------------------------------------------------------------------
	Procedure	: fehler_meldung
	In		:
			  long nr,         0  nur Text 1 Ausgabe
			  long error,      Error
			  char *text,      Text
	Out		: void
	Funktion	: Fehler-Anzeige

--------------------------------------------------------------------------------
***/
void fehler_meldung(long nr,long err,char *text)
{
char tmpbuf[81];
int len;

memset(tmpbuf,0x20,80);
dat_zeit_log(tmpbuf,(short)1);
len = strlen(tmpbuf);
tmpbuf[len - 1] = '|';
if(nr == 0)
     sprintf(&tmpbuf[len]," %s",text);
else
     sprintf(&tmpbuf[len]," %03ld | %s",err,text);
len = strlen(tmpbuf);
tmpbuf[len] = 0x20;
tmpbuf[80]=0x00;
DEBUG(1,"%s\n",tmpbuf);

Eventprot.WriteFirstLine(tmpbuf);
return;
}
/*******************************************************/
long status_host_schreiben(char * datei_name, char kommando)
{
FILE * flog;

flog = fopen(datei_name,"w+");
if(flog == NULL)
      {
      DEBUG(1,"Datei %s kann nicht geoeffnet werden: SCHREIBEN\n",datei_name);
      return (long)402;
      }

fwrite(&kommando,1,1,flog);

fclose(flog);
#ifdef UNIX
chmod(datei_name,(int)00666);
#endif
return (long)0;
} /* end of status_host_schreiben() */

/**********************************************************************/
long status_host_lesen(char *datei_name, struct STATUS_DATEN * daten)
{
FILE * flog;
char   buffor[80];
char   kommando;

flog = fopen(datei_name,R);
if(flog == NULL)
      {
      kommando = '*';
      daten->kommando = kommando;
      daten->ger_liste=0;
      return (long)400;
      }
daten->ger_liste=0;
fread(buffor,sizeof(buffor),1,flog);
if(buffor[0] >= 'a' && buffor[0] <= 'z')
  daten->kommando = buffor[0] & 0xdf;
else
  daten->kommando = buffor[0];

fclose(flog);
/**
chmod(datei_name,(int)00666);
**/
return (long)0;
} /* end of status_host_lesen() */

/*********************** usage() **************************/
void usage(char * pname)
{
if(version != 0)
	{
	printf("\n%s: Version %s Datum %s\n",
	pname,VERSION,DATUM);
	printf("Copyright (c)   SE.TEC   2000\n");
        if(hilfe == 0)
	        {
                printf("\n");
		if(testen == 0 && testlog == 0)
	                exit(0);
	        }
	else
	        {
                printf("\n\nENTER-Taste");
		getchar();
                printf("\n\n\n\n\n\n");
	        }
	}
if(hilfe != 0)
   {
   printf("\n\nAufruf:  %s [-vhtdeILPnolsXX] &\n\n",pname);
   printf("Parameter: -v : Version\n");
   printf("           -h : Hilfe\n");
   printf("           -t : Testausgaben auf Bildschirm\n");
   printf("           -d : Testausgaben in die Datei <%s%c%s.tst>\n",tmp,SLESH,pname);
   printf("           -e : Nur ein Durchlauf <Testzweck>\n");
   printf("           -I : INIT-Datei: <%s%c%s.cfg> editieren\n",etc,SLESH,pname);
   printf("           -L : Log-Datei <%s%c%s.log> lesen\n",tmp,SLESH,pname);
   printf("           -P : Protokoll-Dat <%s%c%s_TTMM.pro> vom heute lesen\n",tmp,SLESH,pname);
   printf("           -n : Neustart des Programmes nach Init <INIT-Datei>\n");
   printf("           -o : ohne Aufruf <Testzweck>\n");
   printf("           -l : Die Dateien werden nicht geloescht <nur Testen>\n");
   printf("           -sXX : Anzahl sek fuer sleep in der Schleife: Default 1 sec\n");

   /******
   printf("Die Parameter kann man auch als Environment PAR_BAGUT definieren\n");
   ******/
   printf("Log-Datei:       %s%c%s.log\n",tmp,SLESH,pname);
   printf("INIT-Datei:      %s%c%s.cfg\n",etc,SLESH,pname);
   printf("Protokoll-Datei: %s%c%s_TTMM.pro\n",tmp,SLESH,pname);
   exit(0);
   }
}/* end of usage() */

/******************************************************************/
void env_lesen ()
{
        tmp = getenv("TMPPATH");
        if (tmp == NULL)
           {
#ifndef UNIX
                    tmp = "C:\\USER\\FIT\\TMP";
#else
                    tmp = "/user/fit/tmp";
#endif
           }
        etc = getenv("BWSETC");
        if (etc == NULL)
           {
#ifndef UNIX
                    etc = "C:\\USER\\FIT\\ETC";
#else
                    etc = "/user/fit/etc";
#endif
           }
        bws = getenv("BWS");
        if (bws == NULL)
           {
#ifndef UNIX
                    bws = "C:\\USER\\FIT";
#else
                    bws = "/user/fit";
#endif
           }
return;
}/* end of env_lesen() */

/******************************************************************/
void IPL_datei(char * pname)
{
char editor[32];
DWORD Exit_Code;

#ifdef UNIX
   sprintf (editor,"vi");
#else
   sprintf (editor,"notepad");
#endif

DEBUG(1,"Log=%d Init=%d Protokoll=%d\n",Log,Init,Protokoll);
#ifndef UNIX
        Sleep (1000);
#else
        sleep (1);
#endif
if (Log || Protokoll || Init)
   {
   if (Log)
      {
      sprintf(buffer,"%s %s%c%s.log",editor,tmp,SLESH,pname);
      DEBUG(1,"%s\n",buffer);
      Exit_Code = ProcWaitExec(buffer, SW_SHOW, -1, 0, -1, 0);
      }
   if (Init)
      {
      DEBUG(1,"INIT\n");
      sprintf(char_zwi,"Datei %s%c%s.cfg ist editiert",etc,SLESH,pname);
      fehler_meldung((long)1,(long)0,char_zwi);
      if (neustart)
         sprintf(char_zwi,"Programm <%s> wird neu gestartet",pname);
      else
         sprintf(char_zwi,"Programm <%s> soll neu gestartet werden",pname);
      printf("%s\n",char_zwi);
      fehler_meldung((long)1,(long)0,char_zwi);
      status_daten.kommando = 'Z';
      error=status_host_schreiben(senden_status,status_daten.kommando);
      if(error != (long)0)
	   {
           sprintf(char_zwi,"%s: Kommand %c schreiben",
           senden_status,status_daten.kommando);
           fehler_meldung((long)1,(long)error,char_zwi);
	   }
      sprintf(buffer,"%s %s%c%s.cfg",editor,etc,SLESH,pname);
      DEBUG(1,"%s\n",buffer);
      Exit_Code = ProcWaitExec(buffer, SW_SHOW, -1, 0, -1, 0);
      /******************* Neustart *****************
      ******************* Neustart *****************/
      if (neustart)
         {
         if (testen)
            sprintf(buffer,"%s -t",pname);
         else
            sprintf(buffer,"%s",pname);
         Exit_Code = ProcExec(buffer, SW_SHOW, -1, 0, -1, 0);
         }
      }
   if (Protokoll)
      {
	  /* 04.09.02 FF */
      sprintf(buffer,"%s %s%c%s_%02d%02d.pro",editor,tmp,SLESH,pname,tag,monat);
      DEBUG(1,"%s\n",buffer);
      Exit_Code = ProcWaitExec(buffer, SW_SHOW, -1, 0, -1, 0);
      }
   exit(0);
   }
else
   return;
}/*end of IPL_datei() */

/******************************************************************/
MDN_CLASS mdn_class;
int MDN_CLASS::lese_mdn_kun(short);
extern void opendbase(char *);
extern void beginwork(void);
extern void commitwork(void);
extern struct MDN _mdn;

void konfiguration_holen()
{
DEBUG(1,"Konfiguration aus der <%s.cfg>\n",PROGRAMM);
if(_getenv("MARKT") == NULL)
{
    /* 20.03.02 */
	int dsqlstatus;

    DEBUG(1,"MARKT aus DB  bws\n");
	opendbase("bws");
    beginwork();
	dsqlstatus = mdn_class.lese_mdn_kun((short)1);
	if (dsqlstatus == 0)
	{
		sprintf(markt,"%04ld",_mdn.kun);
        DEBUG(1,"mdn.kun=%ld\n",_mdn.kun);
	}
	else
		sprintf(markt,"9999");
	commitwork();
    /* 20.03.02 */
}
else
{
    DEBUG(1,"MARKT aus der Konfiguration-Datei\n");
	sprintf(markt,"%s",_getenv("MARKT"));
}
DEBUG(1,"markt=%s\n",markt);


if(_getenv("UNIX") == NULL)
{
	slesch = BSLESCH;
	slesh = BSLESH;
}
else
{
    if (atoi(_getenv("UNIX")) == 0)
	{
		slesch = BSLESCH;
		slesh = BSLESH;
	}
	else
	{
    	slesh = 0x2f;
	    slesch = 0x2f;
	    DEBUG(1,"UNIX=1\n");
	}
}

if(_getenv("FTP") == NULL)
    ftp = 0;
else
    ftp = atoi (_getenv("FTP"));
DEBUG(1,"ftp=%d\n",ftp);

if(ftp)
{
    if(_getenv("HOSTNAME") == NULL)
       sprintf(hostname,"");
    else
       sprintf(hostname,"%s",_getenv("HOSTNAME"));
    
	if(_getenv("FTPPAR") == NULL)
		sprintf(ftppar,"");
	else
		sprintf(ftppar,"%s",_getenv("FTPPAR"));
    DEBUG(1,"hostname=%s ftppar=%s\n",hostname,ftppar);
}

/********************************************
if(_getenv("PFADHOST") == NULL)
   sprintf(pfadhost,"%s",tmp);
else
   sprintf(pfadhost,"%s",_getenv("PFADHOST"));
DEBUG(1,"pfadhost=%s\n",pfadhost);

********************************************/

if(_getenv("PFADKASSE") == NULL)
   sprintf(pfadkasse,"%s",tmp);
else
   sprintf(pfadkasse,"%s",_getenv("PFADKASSE"));
DEBUG(1,"pfadkasse=%s\n",pfadkasse);

/*******************************************
if(_getenv("DATHOST1") == NULL)
   sprintf(dathost1,"%s%cdathost1",pfadhost,SLESH);
else
   sprintf(dathost1,"%s%c%s",pfadhost,slesh,_getenv("DATHOST1"));
DEBUG(1,"dathost1=%s\n",dathost1);

if(_getenv("DATHOST2") == NULL)
   sprintf(dathost2,"%s%cdathost2",pfadhost,slesh);
else
   sprintf(dathost2,"%s%c%s",pfadhost,slesh,_getenv("DATHOST2"));
DEBUG(1,"dathost2=%s\n",dathost2);

if(_getenv("OUTHOST") == NULL)
{
	sprintf(pouthost,"%s%couthost",pfadhost,slesh);
	sprintf(outhost,"outhost");
}
else
{
	sprintf(pouthost,"%s%c%s",pfadhost,slesh,_getenv("OUTHOST"));
	sprintf(outhost,"%s",_getenv("OUTHOST"));
}
DEBUG(1,"pouthost=%s outhost=%s\n",pouthost, outhost);
*********************************************/

if(_getenv("DATKASSE") == NULL)
{
   sprintf(pdatkasse,"%s%cdatkasse",pfadkasse,slesh);
   sprintf(datkasse,"datkasse");
}
else
{  
   sprintf(pdatkasse,"%s%c%s",pfadkasse,slesh,_getenv("DATKASSE"));
   sprintf(datkasse,"%s",_getenv("DATKASSE"));
}

/* 20.03.02 FF A */
if (strstr(datkasse,"MMMM") != NULL)
{
    strncpy(strstr(pdatkasse,"MMMM"),markt,4);
    strncpy(strstr(datkasse,"MMMM"),markt,4);
    strcpy (datmust,datkasse);
    strcpy (strstr(datmust,"."),".");
    DEBUG(1,"datmust=%s\n",datmust);
}
/* 20.03.02 FF E */

DEBUG(1,"pdatkasse=%s\ndatkasse=%s\n",pdatkasse,datkasse);

/*********************************************
if(_getenv("TIMERHOST") == NULL)
   timerhost = TIMER;
else
   timerhost = atoi(_getenv("TIMERHOST"));
DEBUG(1,"timerhost=%d\n",timerhost);

********************************************/

if(_getenv("TIMERKASSE") == NULL)
   timerkasse = TIMER;
else
   timerkasse = atoi(_getenv("TIMERKASSE"));
DEBUG(1,"timerkasse=%d\n",timerkasse);

if(_getenv("NEUSTART") == NULL)
   neustart = 0;
else
   neustart =(short) atoi(_getenv("NEUSTART"));
DEBUG(1,"neustart=%d\n",neustart);
return;
}/*end of konfiguration_holen() */

/******************************************************************/
void verzeichnis_loeschen()
{
    time_t timer;
    struct tm *ltime;
	char verz [64];
	char zwi [64];
	char * p;
	int i;
	FILE * fp;

	    time (&timer);
        ltime = localtime (&timer);

#ifndef TESTEN
        if (ltime->tm_hour > 0 && verz_geloescht == 1)
           verz_geloescht = 0; 
#endif /* TESTEN */

        if (ltime->tm_hour == 0 && verz_geloescht == 0)
        {
           /* Loeschen der Dateien im Verzeichnis */
           sprintf (verz, "%s%c%s", tmp, SLESH, days [ltime->tm_wday]);
           sprintf(char_zwi,"Verzeichnis %s wird geloescht",verz);
           fehler_meldung((long)1,(long)0,char_zwi);
           DEBUG(1,"%s\n",char_zwi);
           //sprintf (zwi,"%s*.*",HOUTDAT);
           if (ftp)
		       strcpy(zwi,ftpdat);
		   else
		       strcpy(zwi,datkasse);
		   p = strstr(zwi,".yyyy");
// 051202
           if (p == NULL)
		   {
 		        p = strstr(zwi,".");
		   }
           if (p != NULL)
		   {
                strcpy (p,"*.*");
		   }
		   else
		   {
                strcpy (zwi,"*.*");
		   }

           sprintf(buffer,"%s %s%c%s",DEL, verz, SLESH, zwi);
           DEBUG(1,"buffer=%s\n",buffer);
           system(buffer);
           verz_geloescht = 1; 

           /* 04.09.02 FF */
		   tag = ltime->tm_mday;
		   monat = ltime->tm_mon + 1;

           dat_zeit_log(char_zwi,(short)7);
    	   char_zwi[10] = 0x00;
		   sprintf(datum,char_zwi);
           ldat = dasc_to_long (datum);
		   ldat = ldat - 14;
		   dlong_to_asc (ldat,ldatum);

		   sprintf (buffer,"%s%c%s_%c%c%c%c.pro",tmp,SLESH,PROGRAMM,ldatum[0],ldatum[1],ldatum[3],ldatum[4]);
		   unlink(buffer);
		   sprintf(char_zwi,"%s geloescht",buffer);
           fehler_meldung((long)1,(long)0,char_zwi);
           
		   for (i=0; i<7; i++)
		   {
		       ldat = ldat - 1;
		       dlong_to_asc (ldat,ldatum);

		       sprintf (buffer,"%s%c%s_%c%c%c%c.pro",tmp,SLESH,PROGRAMM,ldatum[0],ldatum[1],ldatum[3],ldatum[4]);
		       fp = fopen(buffer , "r");
			   if (fp != NULL)
			   {
				   fclose(fp);
    			   unlink(buffer);
	    	       sprintf(char_zwi,"%s geloescht",buffer);
                   fehler_meldung((long)1,(long)0,char_zwi);
			   }
		   }
           /* 04.09.02 FF */
        }

#ifdef TESTEN
        if (ltime->tm_hour >= 10 && verz_geloescht == 0)
           {
           DEBUG(1,"STUNDE=%d verz_geloescht=%d\n",ltime->tm_hour,verz_geloescht);
           /* Loeschen der Dateien im Verzeichnis */
           sprintf (verz, "%s%c%s", tmp, SLESH, days [ltime->tm_wday]);
           sprintf(char_zwi,"Verzeichnis %s wird geloescht",verz);
           fehler_meldung((long)1,(long)0,char_zwi);
           DEBUG(1,"%s\n",char_zwi);
           //sprintf (zwi,"%s*.*",HOUTDAT);
           if (ftp)
		       strcpy(zwi,ftpdat);
		   else
		       strcpy(zwi,datkasse);
		   p = strstr(zwi,".yyyy");
// 051202
           if (p == NULL)
		   {
 		        p = strstr(zwi,".");
		   }
           if (p != NULL)
		   {
                strcpy (p,"*.*");
		   }
		   else
		   {
                strcpy (zwi,"*.*");
		   }

           sprintf(buffer,"%s %s%c%s",DEL, verz, SLESH, zwi);
           DEBUG(1,"buffer=%s:\n",buffer);
           system(buffer);
           verz_geloescht = 1; 
           }
#endif /* TESTEN */

        return;
} /* verzeichnis_loeschen () */

/******************************************************************/
void log_starten(short par)
{
if (par == 0)
        {
        sprintf(char_zwi,">>>>>>> START >>>> %s  Vers. %s >>>> %s >>>>>>>>",
        PROGRAMM,VERSION,DATUM);
	status_daten.kommando = '*';
        error=status_host_schreiben(senden_status,status_daten.kommando);
        if(error != (long)0)
	    {
            fehler_meldung((long)par,(long)error,char_zwi);
	    sprintf(char_zwi,"%s: Kommand %c schreiben",
	    senden_status,status_daten.kommando);
            fehler_meldung((long)1,(long)error,char_zwi);
	    }
        }
else
        {
        memset(char_zwi,'<',55);
        memcpy(&char_zwi[24]," ENDE ",6);
        char_zwi[55] = '\0';
        }
fehler_meldung((long)par,(long)error,char_zwi);
return;
}
/******************************************************************/
char *	datei_mit_datzeit(char * pdatnam, char * datnam)
{
	char datzeit[20];
    char * p;
    time_t timer;
    struct tm *ltime;

    time (&timer);
    ltime = localtime (&timer);

    sprintf (datzeit,"%04d%02d%02d.%02d%02d%02d",
		((ltime->tm_year%100) + 2000),
		ltime->tm_mon + 1,
		ltime->tm_mday,
		ltime->tm_hour,
		ltime->tm_min,
		ltime->tm_sec);
	DEBUG(1,"datnam=%s\ndatzeit=%s\n",datnam,datzeit);

    p = strstr(pdatnam,"yyyymmdd");
	if (p != NULL)
	{
		sprintf(p,"%s.ASC",datzeit);
	}
    p = strstr(datnam,"yyyymmdd");
	if (p != NULL)
	{
		sprintf(p,"%s.ASC",datzeit);
	}
	DEBUG(1,"DATNAM=%s\n",datnam);
	return(pdatnam);
}


/******************************************************************/
long bearbeitung (int * k)
{
int i;
char zwi[64];
long err=0;
short bearb = 0;
DWORD Exit_Code;
int anz=0,count=0;
FILE * fp;
char datpuf [64];  /* 20.02.01 */


if (bearb == 0)  DEBUG(1,"BEARBEITUNG\n");

/* Daten selektieren A */
for (i=0;;i++)
{
      if (udat1[i].satz_typ == 0)
          break;

      sprintf(datpuf,"%s%c%02dfitkas.puf",tmp,SLESH,udat1[i].satz_typ);
      unlink (datpuf);
      if( udat1[i].inp_dat == NULL)
	  {
              CopyFile (outdat, datpuf, FALSE);
              sprintf(buffer,"%s%cbin%c%s %s %s %s",bws,SLESH,SLESH,
              udat1[i].run_funk, outdat, udat1[i].if_dat, udat1[i].par);
      }
	  else
	  {
			  sprintf(zwi,"%s%c%s",tmp,SLESH,udat1[i].inp_dat);
              CopyFile (zwi, datpuf, FALSE);
      		  //12.11.02
			  //sprintf(buffer,"%s%cbin%c%s %s %s %s",bws,SLESH,SLESH,
              //udat1[i].run_funk,zwi,udat1[i].if_dat, udat1[i].par);
      		  //12.11.02
              CopyFile (outdat, datpuf, FALSE);
      		  sprintf(buffer,"%s%cbin%c%s %s%s %s %s",bws,SLESH,SLESH,
              udat1[i].run_funk,udat1[i].par,zwi,outdat,udat1[i].if_dat);
      }
	  DEBUG(1,"TYP=%d: %s:\n",udat1[i].satz_typ,buffer);
      //DEBUG(1,"BUFFER=%s\n",buffer);
	  Exit_Code = ProcWaitExec(buffer, SW_SHOW, -1, 0, -1, 0);
      if(Exit_Code != (DWORD) 0)
      {
          if (err == 0) err = (long) 200;
          sprintf(char_zwi,"Bearbeitung: Satztyp %2d: <%s> EXIT_ERR",
          udat1[i].satz_typ,udat1[i].if_dat);
          fehler_meldung((long)1,(long)Exit_Code,char_zwi);
          if( udat1[i].inp_dat == NULL)
		  {
               unlink (outdat);
			   CopyFile (datpuf, outdat, FALSE);
		  }
	      else
		  {
               unlink (zwi);
               CopyFile (datpuf, zwi, FALSE);
		  }
          sprintf(char_zwi,"Update-Fehler: Lieferschein ignoriert");
          fehler_meldung((long)1,(long)2,char_zwi);
      }
}/* end of for */
DEBUG(1,"ENDE SELECT\n");

/* Daten selektieren E */

/* Daten fuer Kasse vorhanden ? */
fp = fopen(outdat,"r");
if (fp == NULL)
{
	DEBUG(1,"Keine Daten fuer Kasse\n");
	return 0;
}
else
{
    anz = fread (char_zwi,1,1,fp);
    if (anz == 0)
	{
	     DEBUG(1,"Datei %s fuer Kasse 0 By: geloescht\n",outdat);
         fclose (fp);
		 unlink (outdat);
		 return 0;
	}
    fclose (fp);
}

/* Pruefen ob Datei auf dem Host vorhanden A */
if (ftp)
{
    datei_mit_datzeit(ftpdat,ftpdat);
    sprintf (buffer, "ftp %s -s:%s %s", ftppar, ftpdat,hostname);
    DEBUG(1,"buffer=%s\n",buffer);
    Exit_Code = ProcWaitExec(buffer, SW_SHOW, -1, 0, -1, 0);
    DEBUG(1,"CLOSE\n");
    fp = fopen(outtmp, "r");
    while (TRUE)
	{
        anz = fread (char_zwi,1,1,fp);
	    if (anz == 0 || count > 200) break;
	    count++;
	}
    fclose(fp);
    if (loeschen)
        unlink(outtmp);
    if (count == 0)
	{
	    DEBUG(1,"Dat %s nicht vorh. count=0\n",ftpdat);
        bearb = 1;
	}
    else
	{
        DEBUG (1,"Dat %s noch vorh.: count=%d\n",ftpdat,count);
	}
}
else
{
    datei_mit_datzeit(pdatkasse,datkasse);
	fp = fopen (pdatkasse, "r");
    if (fp == NULL)
		count = 0;
	else
	{
		while (TRUE)
		{
            anz = fread (char_zwi,1,1,fp);
	        if (anz == 0 || count > 200) break;
	        count++;
		}
		fclose (fp);
	}
    if (count == 0)
	{
	    DEBUG(1,"Dat %s nicht vorh. count=0\n",pdatkasse);
        bearb = 1;
	}
    else
	{
        DEBUG (1,"Dat %s noch vorh.: count=%d\n",pdatkasse,count);
	}
}

/* Pruefen ob Datei vorhanden E */
//exit(0);
/* Datei senden */
if (bearb == 1)
{
    if (ftp)
	{
    sprintf (buffer, "ftp %s -s:%s %s", ftppar, ftpdats,hostname);
    DEBUG(1,"buffer=%s\n",buffer);
    Exit_Code = ProcWaitExec(buffer, SW_SHOW, -1, 0, -1, 0);
    DEBUG(1,"CLOSE\n");
	}
	else
	{
		unlink (outtmp);
		CopyFile (outdat, outtmp, FALSE);
		unlink (pdatkasse);
	    DEBUG(1,"rename %s %s\n",outtmp,pdatkasse);
		Exit_Code = (DWORD) rename (outtmp,pdatkasse);
	}

	if (Exit_Code == (DWORD) 0)
	{
        if (ftp)
    		sprintf(char_zwi,"an Kasse %s",ftpdats);
	    else
    		sprintf(char_zwi,"an Kasse %s",datkasse);
		fehler_meldung((long)1,(long)0, char_zwi);
        SaveDatei(outdat, datkasse);
		if (loeschen)
		{
			DEBUG(1,"loeschen %s\n",outdat);
			unlink (outdat);
		}

	}
	else
	{
        if (ftp)
     		sprintf(char_zwi,"ERR %s Exit=%d",
	    	ftpdats,(int)Exit_Code);
	    else
     		sprintf(char_zwi,"ERR %s Exit=%d",
	    	datkasse,(int)Exit_Code);
		fehler_meldung((long)1,(long)50, char_zwi);
	}

}
if (bearb == 0) *k=0;

return (err);
}

/*************************************************************/
int vorbereitung ()
{
	int err=0;
	FILE * fp;
    FILE * fps;
	char zwi[132];

	sprintf (hosttmp,"%s%ckassetmp",pfadkasse,slesh);
    sprintf (outtmp,"%s%ckouttmp",tmp,SLESH);
    sprintf (outdat,"%s%c%s",tmp,SLESH,HOUTDAT);
 
    DEBUG(1,"hosttmp=%s\n",hosttmp);
	DEBUG(1,"outdat=%s\n",outdat);
	DEBUG(1,"outtmp=%s\n",outtmp);

	if (ftp == 0)
		return 0;
    
	sprintf (ftpdat,"%s%cftpkout.txt",etc,SLESH);
	sprintf (ftpdats,"%s%cftpkouts.txt",etc,SLESH);
     
    DEBUG(1,"ftpdat=%s\n",ftpdat);
	DEBUG(1,"ftpdats=%s\n",ftpdats);
	
	if (loeschen)
	    unlink(ftpdat);
	fp = fopen (ftpdat, "w");
	if (fp == NULL)
		return -1;
	if (loeschen)
	    unlink(ftpdats);
	fps = fopen (ftpdats, "w");
	if (fps == NULL)
		return -1;
    
	if (_getenv("HOSTUSER") == NULL)
		return -1;
	else
		sprintf(zwi,"%s\n",_getenv("HOSTUSER"));
    DEBUG(1,"HOSTUSER=%s",zwi);
	fwrite (zwi, strlen(zwi), 1, fp);
    fwrite (zwi, strlen(zwi), 1, fps);

	if (_getenv("HOSTPASS") == NULL)
		return -1;
	else
		sprintf(zwi,"%s\n",_getenv("HOSTPASS"));
	fwrite (zwi, strlen(zwi), 1, fp);
    fwrite (zwi, strlen(zwi), 1, fps);
    DEBUG(1,"HOSTPASS=%s",zwi);
	
	sprintf(zwi,"binary\n");
	fwrite (zwi, strlen(zwi), 1, fp);
    fwrite (zwi, strlen(zwi), 1, fps);
    
	sprintf(zwi,"get %s %s\n", pdatkasse, outtmp);
	fwrite (zwi, strlen(zwi), 1, fp);
	
	sprintf(zwi,"put %s %s\n", outdat, hosttmp);
    fwrite (zwi, strlen(zwi), 1, fps);
    
	sprintf(zwi,"rename %s %s\n", hosttmp, pdatkasse);
    fwrite (zwi, strlen(zwi), 1, fps);
    
	sprintf(zwi,"bye\n");
	fwrite (zwi, strlen(zwi), 1, fp);
    fwrite (zwi, strlen(zwi), 1, fps);

	fclose(fp);
    fclose(fps);
    return err;
} /* end of vorbereitung() */
/******************************************************************/

/************************* main ***********************************/
int main (int varanz, char *varargs [])
{
int i;
int k=0;
int fehler=0;
time_t uxtime;
time_t letzte_bearb=0;
long timer = 0;
struct tm *ltime;
   
	step = STEP;
        /** Parameter  lesen **/
        if(varanz >= 2)
             for (i = 1; i < varanz; i ++)
                {
                if (varargs [i] [0] == '-')
                         {
                         tst_arg (&varargs[i][1]);
                         varanz = scroll_arg (varargs,i,varanz);
                         i --;
                         }
                }

        /* Environment Variablen lesen */
        env_lesen ();

        if(version != 0 || hilfe != 0)
        	usage(PROGRAMM);

        sprintf(logbuch,"%s%c%s.log",tmp,SLESH,PROGRAMM);
	sprintf(senden_status,"%s%c%s",tmp,SLESCH,STATUS_DAT);

        if(getenv("testmode") != NULL)
           {
           tst=(short)atoi(getenv("testmode"));
           if(tst > 1)
	        {testlog= 1; testen = 0;}
           else
                if(tst == 1)
	           {testlog= 0; testen = 1;}
	        else
	           {testlog= 0; testen = 0;}
           }
        sprintf(char_zwi,"%s%c%s.tst",tmp,SLESCH,PROGRAMM);
        if(testlog)
	        {
                flog = fopen(char_zwi,"w+");
	        if(flog != NULL)
                      DEBUG(1,"TEST-LOG ist geoeffnet\n");
	        }
        DEBUG(1,"tmp=%s\n",tmp);
        DEBUG(1,"etc=%s\n",etc);
        DEBUG(1,"bws=%s\n",bws);
        DEBUG(1,"status_datei=%s\n",senden_status);

		time (&uxtime);
        ltime = localtime (&uxtime);
        
		tag = ltime->tm_mday;
		monat = ltime->tm_mon + 1;
		DEBUG(1,"tag=%d monat=%d\n",tag,monat);

        /* Konfiguration aus der Datei fitkasse.cfg holen */
        konfiguration_holen();

        /* Parameter I, L, P i ausfuehren */
        IPL_datei(PROGRAMM);

        /* Log-Datei starten */
        log_starten(0);

		/* Vorbereitung der ftp-Dateien */
		fehler = vorbereitung();
        if (fehler != 0)
		{
			sprintf(char_zwi,"Falsche HOST-Definitionen");
			fehler_meldung((long)1,(long)10,char_zwi);
            log_starten(1);
			exit(1);
		}

/*** Bearbeitungsschleife ***/
timer = (long)timerkasse;
DEBUG(1,"ANFANG timer=%ld\n",timer);
while (TRUE)
        {
        verzeichnis_loeschen();
        error=status_host_lesen(senden_status,&status_daten);
        if(error != (long)0)
	     {
	     sprintf(char_zwi,"%s: nicht vorh./kein Zugriff",
	     senden_status);
             fehler_meldung((long)1,(long)error,char_zwi);
             }
	if (status_daten.kommando == 'Z'|| status_daten.kommando == 'E')
             break;

        time ((time_t *) &uxtime);

        if ((long)uxtime >=(long)((long)letzte_bearb + timer))
             {
             letzte_bearb = uxtime;
             error = bearbeitung(&k);
             }

#ifndef UNIX
        Sleep (step * 1000);
#else
        sleep (step);
#endif
        if (einzeln)
              break;
        if(testen)
              {
              k++; DEBUG(1,".",k);
              if (k%50 == 0) DEBUG(1,"\n");
              }
        } /* end of while(1) */

/* Log-Datei beenden */
log_starten(1);

DEBUG(1,"ENDE\n");
return 0;
} 
