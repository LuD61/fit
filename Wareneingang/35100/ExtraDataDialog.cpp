// ExtraDataDialog.cpp: Implementierung der Klasse CExtraDataDialog.
//
//////////////////////////////////////////////////////////////////////

#include "ExtraDataDialog.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

CExtraDataDialog::CExtraDataDialog()
{
	ExtraData = NULL;
	Extra = LoadLibrary ("ExtraData.dll");
	if (Extra != NULL)
	{
			ExtraData = (CExtraData *  (*) (CExtraData *))
					  GetProcAddress ((HMODULE) Extra, "ExtraData");
	}
}

CExtraDataDialog::~CExtraDataDialog()
{
	ExtraD = NULL;
	ExtraDResult = NULL;
}

void CExtraDataDialog::SetData (CExtraData *Data)
{	
	ExtraD = Data;
	
}

CExtraData * CExtraDataDialog::GetData ()
{
	return ExtraDResult;
}

BOOL CExtraDataDialog::DoModal ()
{
	if (ExtraData == NULL) 
	{
		return FALSE;
	}

	ExtraDResult = (*ExtraData) (ExtraD);
	BOOL DelEntry = TRUE;

	if (strcmp (ExtraDResult->ESNr,"") != 0)
	{
		DelEntry = FALSE;
	}
	else if (strcmp (ExtraDResult->EZNr,"") != 0)
	{
		DelEntry = FALSE;
	}
	else if (strcmp (ExtraDResult->IdentNr,"") != 0)
	{
		DelEntry = FALSE;
	}
	else if (ExtraDResult->Chargen.anz > 0) 
	{
		DelEntry = FALSE;
	}
	if (DelEntry)
	{
		return FALSE;
	}
    else
	{
		SCharge *Charge;
		strcpy (ExtraD->ESNr,ExtraDResult->ESNr);
		strcpy (ExtraD->EZNr,ExtraDResult->EZNr);
		strcpy (ExtraD->IdentNr,ExtraDResult->IdentNr);
		strcpy (ExtraD->Countries,ExtraDResult->Countries);
		ExtraD->Chargen.Clear ();
		ExtraDResult->Chargen.Start ();
		while ((Charge = ExtraDResult->Chargen.GetNext ()) != NULL)
		{
			ExtraD->Chargen.Add (*Charge);
		}
	}
	return TRUE;
}