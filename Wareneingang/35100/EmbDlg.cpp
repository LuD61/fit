#include "EmbDlg.h"

int EmbDlg::StdSize = STDSIZE;

mfont EmbDlg::dlgfont = {
                         "ARIAL", 100, 0, 0,
                          BLUECOL,
                          YELLOWCOL,
                          1,
                          NULL};

EmbDlg::EmbDlg (HINSTANCE hInstance, HWND dhWnd, char *Text) :
		DLG (0, 0, 0, 0, "", StdSize, TRUE)
{
	    RECT rect;
	    RECT crect;
		HDC hdc;
	    TEXTMETRIC tm;
		SIZE size, Tsize;
		HFONT hFont, oldfont;
		int x,y;
		int cx, cy;
		int fx, fy;

		cField = NULL;
		cForm = NULL;
	    GetWindowRect (dhWnd, &rect);
	    GetClientRect (dhWnd, &crect);
    	Font = &dlgfont;
  		hdc = GetDC (NULL);
        hFont = SetDeviceFont (hdc, Font, &tm);
        oldfont = SelectObject (hdc, hFont);
	    GetTextMetrics (hdc, &tm);
		GetTextExtentPoint32 (hdc, "X", 1, &size);
		GetTextExtentPoint32 (hdc, Text, strlen (Text), &Tsize);
		DeleteObject (SelectObject (hdc, oldfont));

		cy = tm.tmHeight + tm.tmHeight / 3;
		cx = Tsize.cx + 2 * size.cx;

        y = max (0, rect.top - cy);
		x = max (0, (crect.right - cx) / 2 + rect.left);

        fy = max (0, (cy - Tsize.cy) / 2);
		fx = max (0, (cx - Tsize.cx) / 2);

 	    this->Text = new char [strlen (Text) + 1];
		if (this->Text == NULL) return;
		strcpy (this->Text,Text);
		cField = new CFIELD *[1];
        if (cField == NULL) 
		{
			delete this->Text;
			return;
		}

        cField[0] =  new CFIELD ("", this->Text,  0, 0, fx, fy,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, TRUE, TRANSPARENT);
		if (cField[0] == NULL)
		{
			return;
		}

		cForm = new CFORM (1, cField);
		if (cForm == NULL)
		{
			return;
		}

        SetDialog (cForm);

		SetLocation (x,y);
		SetDimension (cx,cy);
        SetWinBackground (YELLOWCOL);
		SetStyle (WS_BORDER | WS_POPUP);
		SetStyleEx (0);
		this->dhWnd = dhWnd;
		OpenWindow (hInstance, GetParent (dhWnd));
}

EmbDlg::~EmbDlg ()
{
	    if (Text != NULL)
		{
			delete Text;
			Text = NULL;
		}
		if (cField != NULL)
		{
            if (cField[0] != NULL) delete cField[0];
			delete cField;
			cField = NULL;
		}

		if (cForm != NULL)
		{
			delete cForm;
			cForm = NULL;
		}	
	    DestroyWindow ();
}

BOOL EmbDlg::OnMove (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
 	    RECT rect;
	    RECT crect;

        if (hWnd != hMainWindow) 
		{
		      if (OldDlg != NULL)
			  {
			        return OldDlg->OnMove (hWnd,msg, wParam,lParam);
			  }
 			  return FALSE;
		}

	    GetWindowRect (dhWnd, &rect);
	    GetClientRect (hWnd, &crect);
        y = max (0, rect.top - cy);
		x = max (0, (crect.right - cx) / 2 + rect.left);

        ::MoveWindow (this->hWnd, x, y, cx, cy, TRUE);
        if (OldDlg != NULL)
		{
		      return OldDlg->OnMove (hWnd,msg, wParam,lParam);
		}
 		return FALSE;
}








             
          
					 


