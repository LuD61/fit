//160610 mwst der Zuschl�ge fehlte
#include<stdio.h>
#include<stdarg.h>
#include<string.h>
#include<windows.h>
#include "stdfkt.h"
#include "strfkt.h"
#include "abschldlg.h" 

#define OK_CTL 5001
#define CANCEL_CTL 5002

inline void NULLERROR (void *Ptr) 
{
        if (Ptr == NULL) 
        {
            disp_mess ("Fehler bei dr Speicherzuordnung", 2);
            ExitProcess (1);
        }
}


static int StdSize = STDSIZE;
static int InfoSize = 150;

static int dlgsize    = STDSIZE;
static int ltgraysize = STDSIZE;
static int dlgpossize = STDSIZE;
static int cpsize     = 200;


struct ABSCHL AbschlDlg::Abschl;
int AbschlDlg::EditStyle = EDITBORDER;

mfont AbschlDlg::dlgfont = {
                         "ARIAL", 100, 0, 0,
                         BLACKCOL,
//                         LTGRAYCOL,
                         WHITECOL,
                         0,
                         NULL};

mfont AbschlDlg::dlgposfont = {
                         "ARIAL", 100, 0, 0,
                         BLACKCOL,
//                         LTGRAYCOL,
                         WHITECOL,
                         0,
                         NULL};

mfont *AbschlDlg::Font = &dlgposfont;


char AbschlDlg::combotext [80];

int AbschlDlg::ColPos  = 25;
int AbschlDlg::ColPos2 = 56;

char **AbschlDlg::ComboMwst1 = NULL;
char **AbschlDlg::ComboMwst2 = NULL;
char **AbschlDlg::ComboMwst3 = NULL;

CFIELD *AbschlDlg::_fDlg0 [] = {
                                new CFIELD ("anz_pos_txt", "Anzahl Positionen",
                                            0, 0, 1, 1, NULL, "", CDISPLAYONLY,
                                            500, Font, 0, 0),
                                new CFIELD ("anz_pos", Abschl.anz_pos,
                                            5, 0, ColPos, 1,  NULL, "%d", CREADONLY,
                                            500, Font, 0, TRANSPARENT | ES_RIGHT | WS_BORDER),
                                new CFIELD ("anz_mang_txt", "Mangel-Positionen",
                                            0, 0, 1, 2, NULL, "", CDISPLAYONLY,
                                            500, Font, 0, 0),
                                new CFIELD ("anz_mang", Abschl.anz_mang,
                                            5, 0, ColPos, 2, NULL, "%d", CREADONLY,
                                            500, Font, 0, TRANSPARENT | ES_RIGHT | WS_BORDER),
                                new CFIELD ("vk_wrt_eu_txt", "Summe VK",
                                            0, 0, 1, 3, NULL, "", CDISPLAYONLY,
                                            500, Font, 0, 0),
                                new CFIELD ("vk_wrt_eu", Abschl.vk_wrt_eu,
                                            13, 0, ColPos, 3, NULL, "%12.2lf", CREADONLY,
                                            500, Font, 0, TRANSPARENT | ES_RIGHT | WS_BORDER),

                                new CFIELD ("lfd_txt", "WE-Nummer", 0,	0, 41,2,  NULL, "", 
                                             CDISPLAYONLY,
                                                        500, Font, 0, TRANSPARENT),
                                new CFIELD ("lfd", Abschl.lfd, 10, 0,ColPos2,2,  NULL, 
					                         "%8d", 
                                             CREADONLY,
                                             500, Font, 0, TRANSPARENT | ES_RIGHT | WS_BORDER),
                                new CFIELD ("sp_ek_vk_txt", "Spanne EK/VK",
                                            0, 0, 41, 3, NULL, "", CDISPLAYONLY,
                                            500, Font, 0, 0),
                                new CFIELD ("sp_ek_vk", Abschl.sp_ek_vk,
                                             8, 0, ColPos2, 3, NULL, "%5.2lf", CREADONLY,
                                            500, Font, 0, TRANSPARENT | ES_RIGHT | WS_BORDER),

                                new CFIELD ("%", "%",
                                             0, 0, ColPos2 + 9, 3, NULL, "", CDISPLAYONLY,
                                            500, Font, 0, 0),

		                        new CFIELD ("Line1", "",
				                            80, 1, 0, 5, NULL, "", CBORDER,
  								            500, Font, 0, 0),

		                        new CFIELD ("lief_wrt_o_txt", "Lieferwert ohne Rabatt",
				                            80, 1, 0, 6, NULL, "", CDISPLAYONLY,
  								            500, Font, 0, 0),
                                new CFIELD ("lief_wrt_o", Abschl.lief_wrt_o_eu,
                                            13, 0, ColPos, 6, NULL, "%12.2lf", CREADONLY,
                                            500, Font, 0, TRANSPARENT | ES_RIGHT | WS_BORDER),
/*
		                        new CFIELD ("rab_eff_a_txt", "Summe Artikelrabatt",
				                            80, 1, 0, 7, NULL, "", CDISPLAYONLY,
  								            500, Font, 0, 0),
                                new CFIELD ("rab_eff_a", Abschl.rab_eff_a_eu,
                                            13, 0, ColPos, 7, NULL, "%12.2lf", CREADONLY,
                                            500, Font, 0, TRANSPARENT | ES_RIGHT | WS_BORDER),
*/
		                        new CFIELD ("rab_eff_xt", "Summe Rabatt",
				                            80, 1, 0, 7, NULL, "", CDISPLAYONLY,
  								            500, Font, 0, 0),
                                new CFIELD ("rab_eff", Abschl.rab_eff_eu,
                                            13, 0, ColPos, 7, NULL, "%12.2lf", CREADONLY,
                                            500, Font, 0, TRANSPARENT | ES_RIGHT | WS_BORDER),
		                        new CFIELD ("rab_eff_lief_txt", "Summe Lieferantenrabatt",
				                            80, 1, 0, 8, NULL, "", CDISPLAYONLY,
  								            500, Font, 0, 0),
                                new CFIELD ("rab_eff_lief", Abschl.rab_eff_lief_eu,
                                            13, 0, ColPos, 8, NULL, "%12.2lf", CREADONLY,
                                            500, Font, 0, TRANSPARENT | ES_RIGHT | WS_BORDER),
		                        new CFIELD ("lief_wrt_ab_txt", "Lieferwert abz. Rabatt",
				                            80, 1, 0, 9, NULL, "", CDISPLAYONLY,
  								            500, Font, 0, 0),
                                new CFIELD ("lief_wrt_ab", Abschl.lief_wrt_ab_eu,
                                            13, 0, ColPos, 9, NULL, "%12.2lf", CREADONLY,
                                            500, Font, 0, TRANSPARENT | ES_RIGHT | WS_BORDER),


		                        new CFIELD ("zusch_eu_txt", "Zuschlag",
				                            80, 1, 0, 10, NULL, "", CDISPLAYONLY,
  								            500, Font, 0, 0),
                                new CFIELD ("zusch_eu", Abschl.zusch_eu,
                                            13, 0, ColPos, 10, NULL, "%12.2lf", CEDIT,
                                            ZUSCH_EU_CTL, Font, 0, ES_RIGHT | WS_BORDER, 0),
		                        new CFIELD ("mwst_sl1_txt", "Schl�ssel Vorst.",
				                             0, 1, 41, 10, NULL, "", CDISPLAYONLY,
  								            500, Font, 0, 0),
                                new CFIELD ("mwst_sl1", Abschl.mwst_sl1,
                                              16, 5, ColPos2, 10, NULL, "", CCOMBOBOX,
                                            MWST_SL1_CTL, Font, 0, CBS_DROPDOWNLIST |
                                                                   WS_VSCROLL),
/*
                                new CFIELD ("mwst_sl1", Abschl.mwst_sl2,
                                              4, 0, ColPos2, 10, NULL, "%2d", CEDIT,
                                            MWST_SL1_CTL, Font, 0, 
                                            ES_RIGHT | WS_BORDER, 0),
                                new CFIELD ("mwst_sl1_choise", "", 2, 0, ColPos2 + 4, 
                                             10, NULL, "", CBUTTON,
                                             VK_F9, Font, 0, BS_BITMAP),
*/


		                        new CFIELD ("lief_zusch_eu_txt", "Lieferaufschlag",
				                            80, 1, 0, 11, NULL, "", CDISPLAYONLY,
  								            500, Font, 0, 0),
                                new CFIELD ("lief_zusch_eu", Abschl.lief_zusch_eu,
                                            13, 0, ColPos, 11, NULL, "%12.2lf", CEDIT,
                                            LIEF_ZUSCH_EU_CTL, Font, 0, ES_RIGHT | WS_BORDER, 0),
		                        new CFIELD ("mwst_sl2_txt", "Schl�ssel Vorst.",
				                             0, 1, 41, 11, NULL, "", CDISPLAYONLY,
                                             500, Font, 0, 0),
                                new CFIELD ("mwst_sl2", Abschl.mwst_sl2,
                                              16, 5, ColPos2, 11, NULL, "", CCOMBOBOX,
                                            MWST_SL2_CTL, Font, 0, CBS_DROPDOWNLIST |
                                                                   WS_VSCROLL),
/*
                                new CFIELD ("mwst_sl2", Abschl.mwst_sl2,
                                              4, 0, ColPos2, 11, NULL, "%2d", CEDIT,
                                            MWST_SL2_CTL, Font, 0, 
                                            ES_RIGHT | WS_BORDER, 0),
                                new CFIELD ("mwst_sl2_choise", "", 2, 0, ColPos2 + 4, 
                                             11, NULL, "", CBUTTON,
                                             VK_F9, Font, 0, BS_BITMAP),
*/


		                        new CFIELD ("ink_zusch_eu_txt", "Inkassoaufschlag",
				                            80, 1, 0, 12, NULL, "", CDISPLAYONLY,
  								            500, Font, 0, 0),
                                new CFIELD ("ink_zusch_eu", Abschl.ink_zusch_eu,
                                            13, 0, ColPos, 12, NULL, "%12.2lf", CEDIT,
                                            INK_ZUSCH_EU_CTL, Font, 0, ES_RIGHT | WS_BORDER, 0),
		                        new CFIELD ("mwst_sl3_txt", "Schl�ssel Vorst.",
				                              0, 1, 41, 12, NULL, "", CDISPLAYONLY,
  								            500, Font, 0, 0),
                                new CFIELD ("mwst_sl3", Abschl.mwst_sl3,
                                              16, 5, ColPos2, 12, NULL, "", CCOMBOBOX,
                                              MWST_SL3_CTL, Font, 0, CBS_DROPDOWNLIST |
                                                                     WS_VSCROLL),
/*
                                new CFIELD ("mwst_sl3", Abschl.mwst_sl3,
                                              4, 0, ColPos2, 12, NULL, "%2d", CEDIT,
                                            MWST_SL3_CTL, Font, 0, 
                                            ES_RIGHT | WS_BORDER, 0),
                                new CFIELD ("mwst_sl3_choise", "", 2, 0, ColPos2 + 4, 
                                             12, NULL, "", CBUTTON,
                                             VK_F9, Font, 0, BS_BITMAP),
*/

		                        new CFIELD ("Line2", "",
				                            80, 1, 0, 14, NULL, "", CBORDER,
  								            500, Font, 0, 0),

		                        new CFIELD ("lief_wrt_nto_txt", "Lieferwert Netto",
				                            80, 1, 0, 15, NULL, "", CDISPLAYONLY,
  								            500, Font, 0, 0),
                                new CFIELD ("lief_wrt_nto", Abschl.lief_wrt_nto_eu,
                                            13, 0, ColPos, 15, NULL, "%12.2lf", CREADONLY,
                                            500, Font, 0, TRANSPARENT | ES_RIGHT | WS_BORDER),

		                        new CFIELD ("mwst_ges_eu_txt", "Summe Vorsteuer",
				                            80, 1, 0, 16, NULL, "", CDISPLAYONLY,
  								            500, Font, 0, 0),
                                new CFIELD ("mwst_ges_eu", Abschl.mwst_ges_eu,
                                            13, 0, ColPos, 16, NULL, "%12.2lf", CREADONLY,
                                            500, Font, 0, TRANSPARENT | ES_RIGHT | WS_BORDER),
		                        new CFIELD ("lief_wrt_eu_txt", "Rechnungs-Betrag",
				                            80, 1, 0, 17, NULL, "", CDISPLAYONLY,
  								            500, Font, 0, 0),
                                new CFIELD ("lief_wrt_eu", Abschl.lief_wrt_eu,
                                            13, 0, ColPos, 17, NULL, "%12.2lf", CREADONLY,
                                            500, Font, 0, TRANSPARENT | ES_RIGHT | WS_BORDER),
                                NULL,
};

CFORM AbschlDlg::fDlg0 (7, _fDlg0);

CFIELD *AbschlDlg::_fFoot [] = {
                     new CFIELD ("print",  " Drucken  ", 10, 0, 0, 0,  NULL, "", 
                                 CBUTTON,
                                 PRINT_CTL, Font, 0, 0),
                     new CFIELD ("ok",     " Komplett ", 10, 0, 12, 0,  NULL, "", 
                                 CBUTTON,
                                 OK_CTL, Font, 0, 0),
                     new CFIELD ("cancel", " Abbruch  ", 10, 0,24, 0,  NULL, "", 
                                 CBUTTON,
                                 CANCEL_CTL, Font, 0, 0),
                     NULL,
};


CFORM AbschlDlg::fFoot (3, _fFoot);


CFIELD *AbschlDlg::_fDlg1 [] = {
                     new CFIELD ("fDlg0", (CFORM *) &fDlg0, 0, 0, 0, 0, NULL, "", CFFORM,
                                  500, Font, 0, 0),
                     new CFIELD ("fFoot", (CFORM *) &fFoot, 0, 0, -1, -2, NULL, "", CFFORM,
                                  500, Font, 0, 0),
                     NULL,                      
};


CFORM AbschlDlg::fDlg1 (2, _fDlg1);

CFIELD *AbschlDlg::_fDlg [] = {
                                new CFIELD ("fDlg1", (CFORM *)   &fDlg1,
                                            0, 0, -1,  1,  NULL, "", CFFORM,
                                            500, Font, 0, 0),
                                NULL,

};

CFORM AbschlDlg::fDlg (1, _fDlg);

FORMFIELD *AbschlDlg::dbfields [] = {
         new FORMFIELD ("anz_pos",      Abschl.anz_pos,           (long *) 
                                        &we_kopf.anz_pos,         FLONG,  NULL),
         new FORMFIELD ("anz_mang" ,    Abschl.anz_mang,          (char *)  
                                        &we_kopf.anz_mang,        FLONG,   NULL),
         new FORMFIELD ("vk_wrt_eu"  ,  Abschl.vk_wrt_eu,         (double *) 
                                        &we_kopf.vk_wrt_eu,       FDOUBLE,   NULL),
         new FORMFIELD ("lief_wrt_o",   Abschl.lief_wrt_o_eu,     (double *)  
                                        &we_kopf.lief_wrt_o_eu,   FDOUBLE,   NULL),
/*
         new FORMFIELD ("rab_eff_a_eu", Abschl.rab_eff_a_eu,      (double *) 
                                        &we_kopf.rab_eff_a_eu,    FDOUBLE,   NULL),
*/
         new FORMFIELD ("rab_eff_eu",   Abschl.rab_eff_eu,        (double *) 
                                        &we_kopf.rab_eff_eu,      FDOUBLE,   NULL),
         new FORMFIELD ("rab_eff_lief_eu", 
                                        Abschl.rab_eff_lief_eu,   (double *) 
                                        &we_kopf.rab_eff_lief_eu, FDOUBLE,   NULL),
         new FORMFIELD ("lief_wrt_ab",  Abschl.lief_wrt_ab_eu,    (double *)  
                                        &we_kopf.lief_wrt_ab_eu,   FDOUBLE,   NULL),
         new FORMFIELD ("mwst_ges_eu",  Abschl.mwst_ges_eu,       (double *)  
                                        &we_kopf.mwst_ges_eu,      FDOUBLE,   NULL),
         new FORMFIELD ("zusch_eu"  ,   Abschl.zusch_eu,          (double *)  
                                        &we_kopf.zusch_eu,        FDOUBLE,   NULL),
         new FORMFIELD ("lief_zusch_eu",Abschl.lief_zusch_eu,     (double *)  
                                        &we_kopf.lief_zusch_eu,        FDOUBLE,   NULL),
         new FORMFIELD ("ink_zusch_eu", Abschl.ink_zusch_eu,      (double *)  
                                        &we_kopf.ink_zusch_eu,    FDOUBLE,   NULL),
         new FORMFIELD ("lief_wrt_nto", Abschl.lief_wrt_nto_eu,   (double *)  
                                        &we_kopf.lief_wrt_nto_eu,  FDOUBLE,   NULL),
         new FORMFIELD ("lfd",          Abschl.lfd,   (long *)  
                                        &we_kopf.lfd,  FLONG,   NULL),
         NULL,
};

char *AbschlDlg::EnablePos[] = {
                                "zusch_eu",
                                "lief_zusch_eu",  
                                "ink_zusch_eu",  
//                                "mwst_sl1",
//                                "mwst_sl2",
//                                "mwst_sl3",
                                NULL,
};
                          
AbschlDlg::AbschlDlg (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel) :
         DLG (x, y, cx, cy, Caption, Size, Pixel)
{
          
             Init (x, y, cx, cy, Caption, Size, Pixel);
}

AbschlDlg::~AbschlDlg ()
{
         ActiveDlg = OldDlg;
}


void AbschlDlg::Init (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel)
{
//             int i;
//             int xfull, yfull;
             int canz;
             int i;

             strcpy (AbschlKz, "K"); 
             SetDialog (&fDlg);
             ActiveDlg = this;
             fDlg0.SetFieldanz ();
             fDlg.SetFieldanz ();
             fDlg.GetCfield ("Line1")->SetBorder (GRAYCOL, WHITECOL, RAISEDHLINE); 
             fDlg.GetCfield ("Line2")->SetBorder (GRAYCOL, WHITECOL, RAISEDHLINE); 
/*
             fDlg.GetCfield ("mwst_sl1_choise")->SetBitmap 
                            (LoadBitmap (hInstance, "ARRDOWN"));
             fDlg.GetCfield ("mwst_sl2_choise")->SetBitmap 
                            (LoadBitmap (hInstance, "ARRDOWN"));
             fDlg.GetCfield ("mwst_sl3_choise")->SetBitmap 
                            (LoadBitmap (hInstance, "ARRDOWN"));
*/
             SetWinBackground (LTGRAYCOL);
             if (EditStyle == NOBORDER)
             {
                   NotEditAttr (WS_BORDER);
             }
             else if (EditStyle == EDITLOW)
             {
                   NotEditAttr (WS_BORDER);
                   OrEditAttrEx (WS_EX_CLIENTEDGE);
             }
             else if (EditStyle == EDITHI)
             {
                   NotEditAttr (WS_BORDER);
                   OrEditAttr (WS_DLGFRAME);
             }
             
             fFoot.GetCfield ("ok")->SetTabstop (FALSE); 
             fFoot.GetCfield ("cancel")->SetTabstop (FALSE); 

             if (ComboMwst1 == NULL)
             {
                 canz = Work.GetMwstAnz ();
                 ComboMwst1 = new char *[canz + 1];
                 NULLERROR (ComboMwst1);   
                 for (i = 0; i < canz; i ++)
                 {
                     ComboMwst1[i] = new char [20];
                     NULLERROR (ComboMwst1[i]);
                 }
                 ComboMwst1[i] = NULL;

                 ComboMwst2 = new char *[canz + 1];
                 NULLERROR (ComboMwst2);   
                 for (i = 0; i < canz; i ++)
                 {
                     ComboMwst2[i] = new char [20];
                     NULLERROR (ComboMwst2[i]);
                 }
                 ComboMwst2[i] = NULL;

                 ComboMwst3 = new char *[canz + 1];
                 NULLERROR (ComboMwst3);   
                 for (i = 0; i < canz; i ++)
                 {
                     ComboMwst3[i] = new char [20];
                     NULLERROR (ComboMwst3[i]);
                 }
                 ComboMwst3[i] = NULL;
             }

             Work.FillMwstSlCombo (ComboMwst1, 10, canz);
             Work.FillMwstSlCombo (ComboMwst2, 20, canz);
             Work.FillMwstSlCombo (ComboMwst3, 30, canz);

             fDlg.GetCfield ("mwst_sl1")->SetCombobox (ComboMwst1); 
             fDlg.GetCfield ("mwst_sl2")->SetCombobox (ComboMwst2); 
             fDlg.GetCfield ("mwst_sl3")->SetCombobox (ComboMwst3); 


             ToForm (dbfields);

			 short imwst = lief.mwst;
             if (lief.mwst == 0) imwst = 1;

             fDlg.GetCfield ("mwst_sl1")->SetPosCombo (imwst - 1); 
             fDlg.GetCfield ("mwst_sl2")->SetPosCombo (imwst - 1); 
             fDlg.GetCfield ("mwst_sl3")->SetPosCombo (imwst - 1); 

             RechneSummen ();
             RechneEkVkAbSpanne ();
             SetDialog (&fDlg);
}


void AbschlDlg::OrEditAttr (DWORD Attr)
{
        int i; 

        for (i = 0; EnablePos[i] != NULL; i ++)
        {
            fDlg0.GetCfield (EnablePos[i])->SetBkMode 
             (fDlg0.GetCfield (EnablePos[i])->GetBkMode () | Attr);     
        }
}

void AbschlDlg::RechneEkVkSpanne (void)
{
        double sp;
        double lief_wrt;

        lief_wrt = ratod (Abschl.lief_wrt_eu);
        sp = (we_kopf.vk_wrt_eu - lief_wrt) * 100 / lief_wrt;
        sprintf (Abschl.sp_ek_vk, "%2lf", sp);
}

void AbschlDlg::RechneEkVkAbSpanne (void)
{
        double sp;
        double lief_wrt;

        lief_wrt = ratod (Abschl.lief_wrt_eu);
        sp = (we_kopf.vk_wrt_eu - lief_wrt) * 100 / we_kopf.vk_wrt_eu;
        sprintf (Abschl.sp_ek_vk, "%2lf", sp);
}

void AbschlDlg::RechneSummen (void)
{
          double LwNto;
          double SuVs = 0;
          double mwst_val1;
          double mwst_val2;
          double mwst_val3;

          LwNto = ratod (Abschl.lief_wrt_ab_eu) + 
                  ratod (Abschl.zusch_eu) +
                  ratod (Abschl.lief_zusch_eu) +
                  ratod (Abschl.ink_zusch_eu);

          sprintf (Abschl.lief_wrt_nto_eu, "%2lf", LwNto);

          mwst_val1 = ratod (Abschl.zusch_eu) *
                      Work.GetMwstFaktor (atoi (Abschl.mwst_sl1) - 10);
          mwst_val2 = ratod (Abschl.lief_zusch_eu) *
                      Work.GetMwstFaktor (atoi (Abschl.mwst_sl2) - 20);
          mwst_val3 = ratod (Abschl.ink_zusch_eu) *
                      Work.GetMwstFaktor (atoi (Abschl.mwst_sl3) - 30);
          SuVs = mwst_val1 + mwst_val2 + mwst_val3;
//          SuVs += we_kopf.mwst_ges_eu + mwst_val1 + mwst_val2 + mwst_val3;
//160610          SuVs = Work.CalcVSteuer ();
          SuVs += Work.CalcVSteuer ();  // 160610  mwst der Zuschl�ge m�ssen mit dabei !!!! 

// Runden 
		  long suvsl = (long) ((double) (SuVs + 0.005) * 100);
          sprintf (Abschl.mwst_ges_eu, "%.2lf", (double) suvsl / 100);
// Runden 
		  long lief_wrt_eul = (long) ((double) (LwNto + SuVs + 0.005) * 100);
          sprintf (Abschl.lief_wrt_eu, "%.2lf", (double) lief_wrt_eul / 100);
}
         

void AbschlDlg::NotEditAttr (DWORD Attr)
{
        int i; 

        for (i = 0; EnablePos[i] != NULL; i ++)
        {
            fDlg0.GetCfield (EnablePos[i])->SetBkMode 
             (fDlg0.GetCfield (EnablePos[i])->GetBkMode () & (Attr^0xFFFFFFFF));     
        }
}

void AbschlDlg::OrEditAttrEx (DWORD Attr)
{
        int i; 

        for (i = 0; EnablePos[i] != NULL; i ++)
        {
            fDlg0.GetCfield (EnablePos[i])->SetBkModeEx 
             (fDlg0.GetCfield (EnablePos[i])->GetBkModeEx () | Attr);     
        }
}


BOOL AbschlDlg::TestCombobox (void)
{
       CFIELD *Cfield;

       Cfield = GetCurrentCfield ();
       if (Cfield == NULL) return FALSE;

       if (strcmp (Cfield->GetName (), "mwst_sl1") == 0)
       {
           fDlg.GetText ();           
           RechneSummen ();
           RechneEkVkAbSpanne ();
           fDlg.SetText ();
           return TRUE;
       }
       else if (strcmp (Cfield->GetName (), "mwst_sl2") == 0)
       {
           fDlg.GetText ();           
           RechneSummen ();
           RechneEkVkAbSpanne ();
           fDlg.SetText ();
           return TRUE;
       }
       else if (strcmp (Cfield->GetName (), "mwst_sl3") == 0)
       {
           fDlg.GetText ();           
           RechneSummen ();
           RechneEkVkAbSpanne ();
           fDlg.SetText ();
           return TRUE;
       }
       return FALSE;
}


BOOL AbschlDlg::TestEditField (void)
{
       CFIELD *Cfield;

       Cfield = GetCurrentCfield ();
       if (Cfield == NULL) return FALSE;

       if (strcmp (Cfield->GetName (), "zusch_eu") == 0)
       {
           fDlg.GetText ();           
           RechneSummen ();
           RechneEkVkAbSpanne ();
           fDlg.SetText ();
       }
       else if (strcmp (Cfield->GetName (), "lief_zusch_eu") == 0)
       {
           fDlg.GetText ();           
           RechneSummen ();
           RechneEkVkAbSpanne ();
           fDlg.SetText ();
       }
       else if (strcmp (Cfield->GetName (), "ink_zusch_eu") == 0)
       {
           fDlg.GetText ();           
           RechneSummen ();
           RechneEkVkAbSpanne ();
           fDlg.SetText ();
       }
       return FALSE;
}

BOOL AbschlDlg::OnKeyReturn ()
{
       return TestEditField ();
}

BOOL AbschlDlg::OnKeyUp ()
{
       return TestEditField ();
}

BOOL AbschlDlg::OnKeyDown ()
{
       return TestEditField ();
}

BOOL AbschlDlg::OnKey5 ()
{
       syskey = KEY5;
       EnableWindows (hMainWindow, TRUE);
       fWork->GetText ();
       fWork->destroy ();    
       DestroyWindow ();
       return TRUE;
}

void AbschlDlg::WriteAbschl (void)
{
       fWork->GetText ();
       FromForm (dbfields);
       Work.WriteWeKopf ();
}


BOOL AbschlDlg::OnKey12 ()
{
       syskey = KEY12;
       EnableWindows (hMainWindow, TRUE);
       WriteAbschl ();
       fWork->destroy ();    
       DestroyWindow ();
       return TRUE;
}


BOOL AbschlDlg::OnCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
         
        if (fWork)
        {
              if (LOWORD (wParam) == CANCEL_CTL)
              {
                  return OnKey5 ();
              }
              if (LOWORD (wParam) == OK_CTL)
              {
                  SetAbschlKz ("K");
                  return OnKey12 ();
              }
              if (LOWORD (wParam) == PRINT_CTL)
              {
                  SetAbschlKz ("X");
                  return OnKey12 ();
              }
              if (HIWORD (wParam) == WM_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == EN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == CBN_KILLFOCUS)
              {
                    if (TestCombobox ()) return TRUE;
                    return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == WM_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == LBN_SELCHANGE)
              {
              }
              else if (HIWORD (wParam) == EN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == CBN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == BN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == BN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
        }
        return DLG::OnCommand (hWnd, msg, wParam, lParam);
}


BOOL AbschlDlg::OnSysCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        
        if (wParam == SC_CLOSE)
        {
            if (abfragejn (hWnd, "Verarbeitung abbrechen ?", "N") == 0)
            {
                ActiveDlg->SetCurrentFocus ();
                return TRUE;
            }
            ExitProcess (0);
            syskey = KEY5;
        }
        return DLG::OnSysCommand (hWnd, msg, wParam, lParam); 
}


void AbschlDlg::SetWinBackground (COLORREF Col)
{
          DLG::SetWinBackground (Col);
}

void AbschlDlg::ToClipboard (void)
{
          CFIELD *Cfield;
          HGLOBAL hb;
          LPVOID p;
          char *text;

          Cfield = GetCurrentCfield ();
          Cfield->GetText ();
          text = (char *) Cfield->GetFeld ();
          if (text == NULL) return;
          hb = GlobalAlloc (GHND, strlen (text) + 1);
          if (hb == NULL) return;
          p = GlobalLock (hb);
          strcpy ((char *) p, text);
          GlobalUnlock (hb);
          OpenClipboard (hWnd);
          EmptyClipboard ();
          SetClipboardData (CF_TEXT, hb);
          CloseClipboard ();
}

void AbschlDlg::FromClipboard (void)
{
          CFIELD *Cfield;
          HGLOBAL hb;
          LPVOID p;
          char *text;

          Cfield = GetCurrentCfield ();
          text = (char *) Cfield->GetFeld ();
          if (text == NULL) return;
          if (IsClipboardFormatAvailable (CF_TEXT) == FALSE) return;
          OpenClipboard (hWnd);
          hb = GetClipboardData (CF_TEXT);
          if (hb == NULL)
          {
                 CloseClipboard ();
                 return;
          }
          p = GlobalLock (hb);
          if (strlen ((char *) p) > (unsigned int ) Cfield->GetLength ())
          {
                 GlobalUnlock (hb);
                 CloseClipboard ();
                 return;
          }
          strcpy (text, (char *) p);
          GlobalUnlock (hb);
          CloseClipboard ();
          Cfield->SetText ();
}

