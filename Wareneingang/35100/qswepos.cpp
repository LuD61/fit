#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "wmask.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "qswepos.h"

struct QSWEPOS qswepos, qswepos_null;

void QSWEPOS_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &qswepos.mdn, 1, 0);
            ins_quest ((char *) &qswepos.fil, 1, 0);
            ins_quest ((char *) &qswepos.lief, 0, 17);
            ins_quest ((char *) &qswepos.lief_rech_nr, 0, 17);
            ins_quest ((char *) &qswepos.lfd, 2, 0);
            ins_quest ((char *) &qswepos.a, 3, 0);
            ins_quest ((char *) &qswepos.int_pos, 2, 0);
    out_quest ((char *) &qswepos.mdn,1,0);
    out_quest ((char *) &qswepos.fil,1,0);
    out_quest ((char *) &qswepos.lfd,2,0);
    out_quest ((char *) qswepos.lief_rech_nr,0,17);
    out_quest ((char *) qswepos.lief,0,17);
    out_quest ((char *) &qswepos.a,3,0);
    out_quest ((char *) &qswepos.int_pos,2,0);
    out_quest ((char *) qswepos.txt,0,61);
    out_quest ((char *) &qswepos.wrt,1,0);
    out_quest ((char *) &qswepos.temp,3,0);
    out_quest ((char *) &qswepos.ph_wert,3,0);
    out_quest ((char *) &qswepos.dat,2,0);
    out_quest ((char *) qswepos.pers_nam,0,9);
            cursor = prepare_sql ("select qswepos.mdn,  "
"qswepos.fil,  qswepos.lfd,  qswepos.lief_rech_nr,  qswepos.lief,  "
"qswepos.a,  qswepos.int_pos,  qswepos.txt,  qswepos.wrt,  qswepos.temp,  "
"qswepos.ph_wert,  qswepos.dat,  qswepos.pers_nam from qswepos "

#line 32 "qswepos.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   lief = ? "
                                  "and   lief_rech_nr = ? "
                                  "and lfd = ? "
                                  "and a = ? "
                                  "and int_pos = ?");

    ins_quest ((char *) &qswepos.mdn,1,0);
    ins_quest ((char *) &qswepos.fil,1,0);
    ins_quest ((char *) &qswepos.lfd,2,0);
    ins_quest ((char *) qswepos.lief_rech_nr,0,17);
    ins_quest ((char *) qswepos.lief,0,17);
    ins_quest ((char *) &qswepos.a,3,0);
    ins_quest ((char *) &qswepos.int_pos,2,0);
    ins_quest ((char *) qswepos.txt,0,61);
    ins_quest ((char *) &qswepos.wrt,1,0);
    ins_quest ((char *) &qswepos.temp,3,0);
    ins_quest ((char *) &qswepos.ph_wert,3,0);
    ins_quest ((char *) &qswepos.dat,2,0);
    ins_quest ((char *) qswepos.pers_nam,0,9);
            sqltext = "update qswepos set qswepos.mdn = ?,  "
"qswepos.fil = ?,  qswepos.lfd = ?,  qswepos.lief_rech_nr = ?,  "
"qswepos.lief = ?,  qswepos.a = ?,  qswepos.int_pos = ?,  "
"qswepos.txt = ?,  qswepos.wrt = ?,  qswepos.temp = ?,  "
"qswepos.ph_wert = ?,  qswepos.dat = ?,  qswepos.pers_nam = ? "

#line 41 "qswepos.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   lief = ? "
                                  "and   lief_rech_nr = ? "
                                  "and lfd = ? " 
                                  "and a = ? "
                                  "and int_pos = ?";

            ins_quest ((char *) &qswepos.mdn, 1, 0);
            ins_quest ((char *) &qswepos.fil, 1, 0);
            ins_quest ((char *) &qswepos.lief, 0, 17);
            ins_quest ((char *) &qswepos.lief_rech_nr, 0, 17);
            ins_quest ((char *) &qswepos.lfd, 2, 0);
            ins_quest ((char *) &qswepos.a, 3, 0);
            ins_quest ((char *) &qswepos.int_pos, 2, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &qswepos.mdn, 1, 0);
            ins_quest ((char *) &qswepos.fil, 1, 0);
            ins_quest ((char *) &qswepos.lief, 0, 17);
            ins_quest ((char *) &qswepos.lief_rech_nr, 0, 17);
            ins_quest ((char *) &qswepos.lfd, 2, 0);
            ins_quest ((char *) &qswepos.a, 3, 0);
            ins_quest ((char *) &qswepos.int_pos, 2, 0);
            test_upd_cursor = prepare_sql ("select lief from qswepos "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   lief = ? "
                                  "and   lief_rech_nr = ? "
                                  "and lfd = ? "
                                  "and a = ? "
                                  "and int_pos = ?");
            ins_quest ((char *) &qswepos.mdn, 1, 0);
            ins_quest ((char *) &qswepos.fil, 1, 0);
            ins_quest ((char *) &qswepos.lief, 0, 17);
            ins_quest ((char *) &qswepos.lief_rech_nr, 0, 17);
            ins_quest ((char *) &qswepos.lfd, 2, 0);
            ins_quest ((char *) &qswepos.a, 3, 0);
            ins_quest ((char *) &qswepos.int_pos, 2, 0);
            del_cursor = prepare_sql ("delete from qswepos "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   lief = ? "
                                  "and   lief_rech_nr = ? "
                                  "and lfd = ? "
                                  "and a = ? "
                                  "and int_pos = ?");
    ins_quest ((char *) &qswepos.mdn,1,0);
    ins_quest ((char *) &qswepos.fil,1,0);
    ins_quest ((char *) &qswepos.lfd,2,0);
    ins_quest ((char *) qswepos.lief_rech_nr,0,17);
    ins_quest ((char *) qswepos.lief,0,17);
    ins_quest ((char *) &qswepos.a,3,0);
    ins_quest ((char *) &qswepos.int_pos,2,0);
    ins_quest ((char *) qswepos.txt,0,61);
    ins_quest ((char *) &qswepos.wrt,1,0);
    ins_quest ((char *) &qswepos.temp,3,0);
    ins_quest ((char *) &qswepos.ph_wert,3,0);
    ins_quest ((char *) &qswepos.dat,2,0);
    ins_quest ((char *) qswepos.pers_nam,0,9);
            ins_cursor = prepare_sql ("insert into qswepos ("
"mdn,  fil,  lfd,  lief_rech_nr,  lief,  a,  int_pos,  txt,  wrt,  temp,  ph_wert,  dat,  "
"pers_nam) "

#line 89 "qswepos.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?)"); 

#line 91 "qswepos.rpp"
}

int QSWEPOS_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

