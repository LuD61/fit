#ifndef _WEDLGLST_DEF
#define _WEDLGLST_DEF
#include "windows.h"
#include "wework.h"
#include "dlglst.h"
#include "wework.h"
#include "itprog.h"
#include "itfont.h"
#include "searcha.h"
#include "EmbDlg.h"
#include "searchbzg.h"
#include "WeExtraDataCollection.h"
#include "ExtraDataDialog.h"

#ifndef OK_CTL
#define OK_CTL 5001
#endif
#ifndef CANCEL_CTL
#define CANCEL_CTL 5002
#endif

#define LA_CTL 5501
#define LA_BZ1_CTL 5502
#define LA_BZ2_CTL  5503
#define LLIEF_BEST_CTL  5504
#define LME_CTL  5505
#define LTARA_CTL  5506
#define LMHD_CTL  5507
#define LIDENT_CTL  5508
#define LCHARGE_CTL  5509
#define IDM_CH_MEEINH 5510
#define BEST_ME_CTL 5511
#define IDM_CH_PREISE 5512
#define LSCANFELD_CTL  5519
#define LA_INFOCTL 5520
#define LME_INFOCTL 5520
#define LCHG_INFOCTL 5520
#define LMHD_INFOCTL 5520
#define HBKDAT_CTL  5521
#define IDM_CH_LAGER 5522

class WeDlgLst : virtual public DlgLst
{
     private :
        static int StdSize;
        static int dlgsize;
        static int ltgraysize;
        static int dlgpossize;
        static int cpsize;

        static mfont dlgfont;
        static mfont ltgrayfont;
        static mfont dlgposfont;
        static mfont *Font;

		static CFIELD *_fPos[];
		static CFORM fPos;
		static CFIELD *_fList[];
		static CFORM fList;
		static CFIELD *_fFoot[];
		static CFORM fFoot;
		static CFIELD *_fWeLst[];
		static CFORM fWeLst;
		static CFIELD *_fWeLst0[];
		static CFORM fWeLst0;
        static ItFont *weLstFont [];
        static ItFont *InfoFont [];
        static ItProg *After [];
        static ItProg *Before [];

  	    WE_KOPF_CLASS Wekopf;
		WE_POS_CLASS Wepos;
		WE_JOUR_CLASS Wejour;
        WeWork *Work;
		EmbDlg * Emb;
        char DefaultIdent  [22];
        char DefaultCharge [22];
		static WeDlgLst *ActiveWeDlgLst;
        BOOL FillOk;
        BOOL WithSearchBest;
        BOOL AppendMode;
	    CWeExtraDataCollection ExtraData;
		CWeExtraData * ExtraDataItem;
		CExtraDataDialog ExtraDataDialog;

     public :
        static WeDlgLst * Instance;

        void SetWork (WeWork *weWork)
        {
               this->Work = weWork;
//               this->Work->SetListDlg (this);
        }

		WeWork *GetWork (void)
		{
			return Work;
		}

        WeDlgLst (int, int, int, int, char *, int, BOOL);
		~WeDlgLst ();

        void SetWithSearchBest (BOOL b)
        {
              WithSearchBest = b;
        }

        void SetDefaultIdent (char *Ident)
        {
              strcpy (DefaultIdent, Ident);
        }

        char *GetDefaultIdent (void)
        {
              return DefaultIdent;
        }

        void SetDefaultCharge (char *Charge)
        {
              strcpy (DefaultCharge, Charge);
        }

        char *GetDefaultCharge (void)
        {
              return DefaultCharge;
        }

        double GetAktArtikel (void)
		{
			return ratod (LISTDLG::Lsts.a);
		}


        PROG_CFG *ProgCfg;
        int ShowLief (void);
        void GetSysPar (void);
        void SetBorderType (int);
        BOOL OnCommand (HWND,UINT,WPARAM,LPARAM);
        BOOL OnKeyTab (void);
        BOOL OnKeyReturn (void);
        BOOL OnKeyUp (void);
        BOOL OnKeyDown (void);
        BOOL OnKey4 (void);
        BOOL OnKey5 (void);
        BOOL OnKey6 (void);
        BOOL OnKey7 (void);
        BOOL OnKey9 (void);
        BOOL OnKey10 (void);
        BOOL OnKey12 (void);
        BOOL OnRowDelete (void);
		BOOL OnExtraData (void);
		void LadeExtraData (void);
		void LadeExtraData (int);
        virtual BOOL OnPreTranslateMessage (MSG *);
		int ShowA (void);
		int ShowBzg (void);
        BOOL ShowMeEinh (void);
        BOOL ShowLager (double a);
        void FillListCaption (void);
	    void ReadExtraData (int i);
	    void ReadExtraData (void);
        int WriteRow (void);
        void FillRow (int);
        void FillRow (char *);
        void ReadPos (void);
        void uebertragen (void);
        void IdentCharge (void);
        void FillRow (void);
        void FillEnterList (int);
        BOOL NewArt (void);
        void Scroll (int, int);
        void WriteRec (int);
        BOOL PruefeRec (int);

        static int AfterPrEk (void);
        static int AfterLager (void);
        static int AfterLsIdent (void);
        static int AfterMHD (void);
        static int AfterA (void);
        static int BeforeA (void);
        int AfterScanfeld (void);
        int AfterScanfeldCharge (void);
        static int AfterBzgPressed (void);
        static int AfterLsCharge (void);
        void GetCfgValues (void);
		static BOOL Readlief_best (void);
        void CallInfo (void);
        int ShowBzg (char*); 
        int HoleAnzLief_best (double);
};
#endif
