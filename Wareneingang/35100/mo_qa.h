#ifndef _MO_AQDEF
#define _MO_AQDEF

class QueryClass
{
public :
        QueryClass ()
        {
        }
        int querya (HWND);
        int searcha (HWND hWnd);
        int querybest (HWND);
        int querykontrakt (HWND);
        int querybestex (HWND, BOOL);
        int querylief (HWND);
        int querya_direct (HWND, char *);
        int searcha_direct (HWND, char *);
        int searchlief_direct (HWND, char *);
};
#endif
