#include<stdio.h>
#include<stdarg.h>
#include<string.h>
#include<windows.h>
#include "stdfkt.h"
#include "strfkt.h"
#include "textdlg.h" 

mfont TextDlg::dlgfont = {
                          "ARIAL", STDSIZE, 0, 0,
                          BLACKCOL,
                          GRAYCOL,
                          0,
                          NULL};

TextDlg::TextDlg (TextWork *textWork, int x, int y, int cx, int cy, 
                  char *Caption, int Size, BOOL Pixel) : 
         DLG (x, y, cx, cy, Caption, Size, Pixel)

{
     int i, f;
     int updanz = 0;
     int *Wsize;

     Cfields = NULL;

     Offset = 2000;
     fieldanz = textWork->GetFieldanz ();;

     Wsize = textWork->GetWsize ();
     SetDimension (Wsize[0], Wsize[1]);
     Cfields = new CFIELD * [fieldanz * 2 + 1];
     if (Cfields == NULL) 
     {
                return;
     }

     Texte = textWork->GetTexte ();
     if (Texte == NULL) 
     {
                fieldanz = 0;
                return;
     }
     Felder = textWork->GetFelder ();
     if (Felder == NULL) 
     {
                fieldanz = 0;
                return;
     }

     Namen = textWork->GetNamen ();
     if (Namen == NULL) 
     {
                fieldanz = 0;
                return;
     }

     Feldnamen = textWork->GetFeldnamen ();

     Rows    = textWork->GetRows ();
     Columns = textWork->GetColumns ();
     Length  = textWork->GetLength ();

     i = f = 0;
    
     while (i < fieldanz)
     {
            Cfields[f] = new CFIELD ("", Texte [i], 0, 0, Columns [i], 
                                                          Rows [i], NULL, "",
                                     CDISPLAYONLY, 500, &dlgfont, 0, 0); 
            f ++;
            Cfields[f] = new CFIELD (Namen[i], Felder [i], Length [i], 0, 
                                     Columns [i] + textWork->GetTextlen (),
                                     Rows [i], NULL, "",
                                     CEDIT, Offset + i, &dlgfont, 0, 0); 
            f ++;
            i ++;
     }

     Cfields [f] = NULL;

     Cform = new CFORM (fieldanz, Cfields); 
     Cform->SetFieldanz ();
     SetDialog (Cform);
     NewTabStops (Cform);
     SetWinBackground (textWork->GetWinColor ());
}

TextDlg::~TextDlg ()
{
     int i;

     if (Cfields != NULL)
     {
         for (i = 0; Cfields[i] != NULL; i ++)
         {
             delete Cfields[i];
         }
         delete Cfields;
     }

     if (Cform != NULL)
     {
         delete Cform;
     }
}

void TextDlg::SetText (void)
{
      fWork->SetText ();
      SetTabStopPos (0);
}

BOOL TextDlg::OnKeyReturn (void)
{
      CFIELD *Cfield = GetCurrentCfield ();
      if (Cfield == fWork->GetCfield () [fWork->GetFieldanz () - 1])
      {
          return OnKey5 ();
      }
      return FALSE;
}

BOOL TextDlg::OnKey5 (void)
{
     EnableWindows (hMainWindow, TRUE);
     fWork->GetText ();
     fWork->destroy ();    
     DestroyWindow ();
     return TRUE;
}

BOOL TextDlg::OnKeyEscape (void)
{
     return OnKey5 ();
}


BOOL TextDlg::OnKey12 (void)
{
     return OnKey5 ();
}


BOOL TextDlg::OnSysCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        
        if (wParam == SC_CLOSE)
        {
            return OnKey5 ();
        }
        return DLG::OnSysCommand (hWnd, msg, wParam, lParam); 
}
           

    
     