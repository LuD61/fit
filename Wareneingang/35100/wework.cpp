//seit 19.5. OhneFilEkAktion_par
//141010 a_best soll wieder beschrieben werden (-> Wille)
//220910
// 220409 :
//LuD 090205
#include <windows.h>
#include <stdio.h>
#include "weWork.h"
#include "strfkt.h"
#include "a_bas.h"
#include "a_hndw.h"
#include "mo_menu.h"
#include "infmacro.h"
#include "mo_wdebug.h"
#include "a_kalkhndw.h"
#include "a_kalk_mat.h"
#include "a_kalkpreis.h"
#include "a_best.h"
#include "wmaskc.h"
#include "sys_par.h"
#include "preise_we.h"
#include "bild160.h" 

#ifdef _DEBUG
#include <crtdbg.h>
extern _CrtMemState memory;
#endif

BOOL OhneFilEkAktion_par;

extern HANDLE  hMainInst;
extern char LONGNULL[];

#define MAXTRIES 100

KEINHEIT keinheit_null;

char *WeWork::Ub[] = {"Qualit�tssicherung Hygiene",
                       NULL,
};

char *WeWork::Texte0[] = {"Laderaum hygienisch",
                          "Haken und Beh�ltnisse i.O.",
			              "Holzpaletten/Kartonagen",
			              "Fleisch korrekt verpackt",
			              "Kleidung hygienisch",
			              "Kopfbedeckung",
				           NULL, NULL
};

int WeWork::Yes0[] = {0, 0, 0, 0, 0, 0, 0, 0};
int WeWork::No0[]  = {0, 0, 0, 0, 0, 0, 0, 0};
int WeWork::qsanz = 6;
int WeWork::ListDiff = 6;
char LagerBez [18];

char *WeWork::korestab[10] =  {we_kopf.koresp_best0,
                               we_kopf.koresp_best1,
                               we_kopf.koresp_best2,
						       we_kopf.koresp_best3,
						       we_kopf.koresp_best4,
						       we_kopf.koresp_best5,
						       we_kopf.koresp_best6,
						       we_kopf.koresp_best7,
						       we_kopf.koresp_best8,
						       we_kopf.koresp_best9};


long *WeWork::lieftab[10] =     {&we_kopf.lief0_term,
                                 &we_kopf.lief1_term,
                                 &we_kopf.lief2_term,
					  	         &we_kopf.lief3_term,
						         &we_kopf.lief4_term,
						         &we_kopf.lief5_term,
						         &we_kopf.lief6_term,
						         &we_kopf.lief7_term,
						         &we_kopf.lief8_term,
						         &we_kopf.lief9_term};

long *WeWork::termtab[10] =     {&we_kopf.best0_term,
                                 &we_kopf.best1_term,
                                 &we_kopf.best2_term,
					  	         &we_kopf.best3_term,
						         &we_kopf.best4_term,
						         &we_kopf.best5_term,
						         &we_kopf.best6_term,
						         &we_kopf.best7_term,
						         &we_kopf.best8_term,
						         &we_kopf.best9_term};

long WeWork::LongNull = 0x80000000;

char *WeWork::UbPos[] = {"Qualit�tssicherung Fleisch",
                          NULL,
};

char *WeWork::TextePos0[] = {"Fettgehalt OK",
                             "Gewicht OK",
            	             "Zuschnitt OK",
			                 "Spezifikationen OK",
			                 "Zustand der Ware OK",
			                 "ordnungsgem�� gestempelt",
				              NULL, NULL
};

int WeWork::YesPos0[] = {0, 0, 0, 0, 0, 0, 0, 0};
int WeWork::NoPos0[]  = {0, 0, 0, 0, 0, 0, 0, 0};
int WeWork::qsanzpos = 6;

double WeWork::inh;
double WeWork::pr_ek_einh;
short WeWork::akt_lager = 0;
BOOL WeWork::Weldr = TRUE;
static PREISE_WE_CL preise_we_class;

void WeWork::ExitError (BOOL b)
{
    if (b)
    {
        disp_mess ("Speicher kann nicht zugeornet werden", 2);
        ExitProcess (1);
    }
}

void WeWork::TestNullDat (long *dat)
{
    if (*dat == 0l)
    {
        memcpy (dat, (long *) LONGNULL, sizeof (long));
    }
}

short WeWork::HoleLager (double a,short mdn)
{
	return akt_lager ; 
}

char* WeWork::HoleLagerBez (short lager,short mdn)
{

    LiefBzg.sqlin ((short *) &mdn, 1, 0);
    LiefBzg.sqlin ((short *) &lager, 1, 0);
    LiefBzg.sqlout ((char *)  LagerBez, 0, 17);
    LiefBzg.sqlcomm ("select lgr_bz from lgr where mdn = ? and lgr = ?");

	return LagerBez ; 
}


void WeWork::TestAllDat (void)
{
    TestNullDat (&we_kopf.best0_term);
    TestNullDat (&we_kopf.best1_term);
    TestNullDat (&we_kopf.best2_term);
    TestNullDat (&we_kopf.best3_term);
    TestNullDat (&we_kopf.best4_term);
    TestNullDat (&we_kopf.best5_term);
    TestNullDat (&we_kopf.best6_term);
    TestNullDat (&we_kopf.best7_term);
    TestNullDat (&we_kopf.best8_term);
    TestNullDat (&we_kopf.best9_term);

    TestNullDat (&we_kopf.lief0_term);
    TestNullDat (&we_kopf.lief1_term);
    TestNullDat (&we_kopf.lief2_term);
    TestNullDat (&we_kopf.lief3_term);
    TestNullDat (&we_kopf.lief4_term);
    TestNullDat (&we_kopf.lief5_term);
    TestNullDat (&we_kopf.lief6_term);
    TestNullDat (&we_kopf.lief7_term);
    TestNullDat (&we_kopf.lief8_term);
    TestNullDat (&we_kopf.lief9_term);

    TestNullDat (&we_kopf.akv);
    TestNullDat (&we_kopf.bearb);
    TestNullDat (&we_kopf.we_dat);
    TestNullDat (&we_kopf.blg_eing_dat);
}

int WeWork::ReadLief (short mdn, short fil, char *lief_nr)
{
    int dsqlstatus;

    if (liefbzg_cursor != -1)
    {
        LiefBzg.sqlclose (liefbzg_cursor);
    }

    LiefBzg.sqlin ((short *) &lief_bzg.mdn, 1, 0);
    LiefBzg.sqlin ((short *) &lief_bzg.fil, 1, 0);
    LiefBzg.sqlin ((char *)  lief_bzg.lief, 0, 17);
    LiefBzg.sqlout ((double *)  &lief_bzg.a, 3, 0);
    liefbzg_cursor = LiefBzg.sqlcursor ("select a from lief_bzg "
                                        "where mdn = ? "
                                        "and fil = ? "
                                        "and lief = ? "
                                        "order by a");
    dsqlstatus = LiefBzg.sqlfetch (liefbzg_cursor);
    return dsqlstatus;
}

int WeWork::ReadLief (void)
{
    int dsqlstatus;

    if (liefbzg_cursor == -1)
    {
        return 100;
    }
    dsqlstatus = LiefBzg.sqlfetch (liefbzg_cursor);
    return dsqlstatus;
}

BOOL WeWork::GenSqlin (char *Tab, char *Feld)
{
    char Feldname [256];
    int i;

    sprintf (Feldname, "%s.%s", Tab, Feld);
    for (i = 0; i < textWork->GetFieldanz (); i ++)
    {
        if (strcmp (Feldname, textWork->GetFeldnamen () [i]) == 0)
        {
            LiefBzg.sqlin ((char *) textWork->GetFelder () [i], 0, 
                                    textWork->GetLength () [i]);
            return TRUE;
        }
    }
    return FALSE;
}

BOOL WeWork::GenSqlin (char *Tab, char *Feld, TextWork * textWork)
{
    char Feldname [256];
    int i;

    sprintf (Feldname, "%s.%s", Tab, Feld);
    for (i = 0; i < textWork->GetFieldanz (); i ++)
    {
        if (strcmp (Feldname, textWork->GetFeldnamen () [i]) == 0)
        {
            LiefBzg.sqlin ((char *) textWork->GetFelder () [i], 0, 
                                    textWork->GetLength () [i]);
            return TRUE;
        }
    }
    return FALSE;
}


int WeWork::WriteTxt (void)
{
    char **Updates;
    char sql [0x1000];
    int anz;
    int i;

    if (textWork == NULL)
    {
        return 0;
    }

    Updates = textWork->GetUpdates ();
    if (Updates[0] == NULL)
    {
        return 0;
    }
    anz = wsplit (Updates[0], ";");
    if (anz < 2) return 0;

    sprintf (sql, "update %s set ", wort [0]);

    for (i = 1; i < anz; i ++)
    {
        if (GenSqlin (wort[0], wort[i]) == FALSE) continue;
        if (i > 1)
        {
            strcat (sql, ",");
        }
        strcat (sql, wort[i]);
        strcat (sql, " = ?");
    }

    strcat (sql, " where mdn = ? and fil = ? and lief = ? and lief_rech_nr = ?"
                 " and blg_typ = ?");    
         

    LiefBzg.sqlin ((short *) &we_kopf.mdn, 1, 0);
    LiefBzg.sqlin ((short *) &we_kopf.fil, 1, 0);
    LiefBzg.sqlin ((char *)  we_kopf.lief, 0, 17);
    LiefBzg.sqlin ((char *)  we_kopf.lief_rech_nr, 0, 17);
    LiefBzg.sqlin ((long *)  we_kopf.blg_typ, 0, 2);
    LiefBzg.sqlcomm (sql);
    return 0;
}

   
void WeWork::WriteWeKopf (void)
{
    SYS_PAR_CLASS Syspar;
    BOOL kreditor = TRUE;

    strcpy (sys_par.sys_par_nam,"kreditor_par");
    if (Syspar.dbreadfirst () == 0)
    {
        kreditor = atoi (sys_par.sys_par_wrt); 
    }
    TestAllDat ();
    we_kopf.anz_pos  = GetWePosAnz ();
    we_kopf.anz_mang = GetWePosAnzMa ();

    we_kopf.lief_wrt_o_eu = GetLiefWrtOEu ();
    we_kopf.lief_wrt_o    = GetLiefWrtO ();

//LuD 090205    we_kopf.lief_wrt_nto_eu = GetLiefWrtNtoEu ();
//LuD 090205    we_kopf.lief_wrt_nto    = GetLiefWrtNto ();

    we_kopf.lief_wrt_ab_eu = GetLiefWrtAbEu ();
    we_kopf.lief_wrt_ab    = GetLiefWrtAb ();

//LuD 090205     we_kopf.rab_eff_eu = GetRabEffEu ();
//LuD 090205     we_kopf.rab_eff    = GetRabEff ();

/*
    we_kopf.lief_wrt_ab_eu = GetLiefWrtAbEu ();
    we_kopf.lief_wrt_ab    = GetLiefWrtAb ();
*/


//LuD 090205 
//    we_kopf.lief_wrt_ab_eu = we_kopf.lief_wrt_o_eu - we_kopf.rab_eff_eu;
//    we_kopf.lief_wrt_ab    = we_kopf.lief_wrt_o    - we_kopf.rab_eff;
    we_kopf.lief_wrt_nto_eu = we_kopf.lief_wrt_ab_eu + we_kopf.zusch_eu + we_kopf.lief_zusch_eu;
    we_kopf.lief_wrt_nto = we_kopf.lief_wrt_ab + we_kopf.zusch + we_kopf.lief_zusch;
	we_kopf.rab_eff_eu = we_kopf.lief_wrt_o_eu - we_kopf.lief_wrt_ab_eu ; //LuD 090205
	we_kopf.rab_eff = we_kopf.lief_wrt_o - we_kopf.lief_wrt_ab ; //LuD 090205

/*
    we_kopf.mwst_ges_eu = GetMwstEu ();
    we_kopf.mwst_ges    = GetMwst ();
*/

    if (we_kopf.we_status == 0)
    {
              we_kopf.we_status = 1;
    }

    if (kreditor)
    {
        we_kopf.lief_s = atoi (we_kopf.lief);
    }

	strcpy (we_kopf.pers_nam, sys_ben.pers_nam);
	clipped (we_kopf.pers_nam);
	if (strcmp (we_kopf.pers_nam, " ") <= 0)
	{
		strcpy (we_kopf.pers_nam, "fit");
	}
    WeKopf.dbupdate ();
    WriteTxt ();
}

BOOL WeWork::LockWeKopf ()
{
	int sqls;
	extern short sql_mode;

	sqls = sql_mode;
	sql_mode = 1;
	if (WeKopf.dbupdate () != 0)
	{
		sql_mode = sqls;
		return FALSE;
	}
	WeKopf.dblock ();
	sql_mode = sqls;
	return TRUE;
}


void WeWork::ReWriteWeKopf (void)
{
    SYS_PAR_CLASS Syspar;
    BOOL kreditor = TRUE;

    WeKopf.dbreadfirst ();
    strcpy (sys_par.sys_par_nam,"kreditor_par");
    if (Syspar.dbreadfirst () == 0)
    {
        kreditor = atoi (sys_par.sys_par_wrt); 
    }
    TestAllDat ();
    we_kopf.anz_pos  = GetWePosAnz ();
    we_kopf.anz_mang = GetWePosAnzMa ();

    we_kopf.lief_wrt_o_eu = GetLiefWrtOEu ();
    we_kopf.lief_wrt_o    = GetLiefWrtO ();

//LuD 090205     we_kopf.lief_wrt_nto_eu = GetLiefWrtNtoEu ();
//LuD 090205     we_kopf.lief_wrt_nto    = GetLiefWrtNto ();

    we_kopf.lief_wrt_ab_eu = GetLiefWrtAbEu ();
    we_kopf.lief_wrt_ab    = GetLiefWrtAb ();

//LuD 090205     we_kopf.rab_eff_eu = GetRabEffEu ();
//LuD 090205     we_kopf.rab_eff    = GetRabEff ();


//LuD 090205 
//    we_kopf.lief_wrt_ab_eu = we_kopf.lief_wrt_o_eu - we_kopf.rab_eff_eu;
//    we_kopf.lief_wrt_ab    = we_kopf.lief_wrt_o    - we_kopf.rab_eff;
    we_kopf.lief_wrt_nto_eu = we_kopf.lief_wrt_ab_eu + we_kopf.zusch_eu + we_kopf.lief_zusch_eu;
    we_kopf.lief_wrt_nto = we_kopf.lief_wrt_ab + we_kopf.zusch + we_kopf.lief_zusch;
   
	we_kopf.rab_eff_eu = we_kopf.lief_wrt_o_eu - we_kopf.lief_wrt_ab_eu ; //LuD 090205
	we_kopf.rab_eff = we_kopf.lief_wrt_o - we_kopf.lief_wrt_ab ; //LuD 090205
  

    if (we_kopf.we_status == 0)
    {
              we_kopf.we_status = 1;
    }

    if (kreditor)
    {
        we_kopf.lief_s = atoi (we_kopf.lief);
    }

    WeKopf.dbupdate ();
}

/*
void WeWork::FillRow (void)
{
    int dsqlstatus;

    if (Lsts == NULL) return;

    we_pos.mdn = we_kopf.mdn;
    we_pos.fil = we_kopf.fil;
    strcpy (we_pos.lief, we_kopf.lief);
	strcpy (we_pos.lief_rech_nr, we_kopf.lief_rech_nr);
	strcpy (we_pos.blg_typ, we_kopf.blg_typ);

    sprintf (Lsts->a, "%.0lf", we_pos.a);
    strcpy (Lsts->lief_best, we_pos.lief_best);
    sprintf (Lsts->me, "%.3lf", we_pos.me);
    sprintf (Lsts->tara, "%.3lf", we_pos.tara);
    if (lief.waehrung == 2)
    {
           sprintf (Lsts->pr_ek,    "%.4lf", we_pos.pr_ek_euro);
           sprintf (Lsts->netto_ek, "%.4lf", we_pos.pr_ek_nto_eu);
    }
    else
    {
           sprintf (Lsts->pr_ek,    "%.4lf", we_pos.pr_ek);
           sprintf (Lsts->netto_ek, "%.4lf", we_pos.pr_ek_nto);
    }
    if (atoi (_mdn.waehr_prim) == 2)
    {
           sprintf (Lsts->pr_fil_ek, "%.4lf", we_pos.pr_fil_ek_eu);
           sprintf (Lsts->pr_vk,     "%.2lf", we_pos.pr_vk_eu);
    }
    else
    {
           sprintf (Lsts->pr_fil_ek, "%.4lf", we_pos.pr_fil_ek);
           sprintf (Lsts->pr_vk,     "%.2lf", we_pos.pr_vk);
    }
    sprintf (Lsts->me_einh, "%d", we_pos.me_einh);
    sprintf (Lsts->a_krz, "%ld", we_pos.a_krz);
    sprintf (Lsts->inh, "%.3lf", we_pos.inh);
    strcpy (Lsts->ls_ident,  we_pos.ls_ident);
    strcpy (Lsts->ls_charge, we_pos.ls_charge);
    sprintf (Lsts->p_nr, "%hd", we_pos.p_num);

    GetMeEinh (we_kopf.mdn, we_kopf.fil, we_kopf.lief, we_pos.a);
    Lsts->me_choise[0] = new ListButton ();
    Lsts->me_choise[0]->SetFeld ("..");
    dlong_to_asc (we_pos.akv, Lsts->akv);
    dsqlstatus = lese_a_bas (we_pos.a);
    if (dsqlstatus == 0)
    {
        strcpy (Lsts->a_bz1, _a_bas.a_bz1);
        strcpy (Lsts->a_bz2, _a_bas.a_bz2);
    }
}
*/

void WeWork::FillRow (void)
{
    int dsqlstatus;

    if (Lsts == NULL) return;

    we_jour.mdn = we_kopf.mdn;
    we_jour.fil = we_kopf.fil;
    strcpy (we_jour.lief, we_kopf.lief);
	strcpy (we_jour.lief_rech_nr, we_kopf.lief_rech_nr);
	strcpy (we_jour.blg_typ, we_kopf.blg_typ);

    sprintf (Lsts->a, "%.0lf", we_jour.a);
    strcpy (Lsts->lief_best, we_jour.lief_best);
    sprintf (Lsts->me, "%.3lf", we_jour.me);
    sprintf (Lsts->best_me, "%.3lf", we_jour.best_me);
    sprintf (Lsts->tara, "%.3lf", we_jour.tara);
    sprintf (Lsts->int_pos, "%hd", we_jour.int_pos);
    dlong_to_asc (we_jour.hbk_dat, Lsts->hbk_dat);  //220910

    if (lief.waehrung == 2)
    {
           sprintf (Lsts->pr_ek,    "%.4lf", we_jour.pr_ek_euro);
           sprintf (Lsts->netto_ek, "%.4lf", we_jour.pr_ek_nto_eu);
      	   sprintf (Lsts->pos_wert,  "%.4lf", we_jour.pr_ek_nto_eu * we_jour.me);
    }
    else
    {
           sprintf (Lsts->pr_ek,    "%.4lf", we_jour.pr_ek);
           sprintf (Lsts->netto_ek, "%.4lf", we_jour.pr_ek_nto);
      	   sprintf (Lsts->pos_wert,  "%.4lf", we_jour.pr_ek_nto * we_jour.me);
    }
    if (atoi (_mdn.waehr_prim) == 2)
    {
           sprintf (Lsts->pr_fil_ek, "%.4lf", we_jour.pr_fil_ek_eu);
           sprintf (Lsts->pr_vk,     "%lf", we_jour.pr_vk_eu);
    }
    else
    {
           sprintf (Lsts->pr_fil_ek, "%.4lf", we_jour.pr_fil_ek);
           sprintf (Lsts->pr_vk,     "%lf", we_jour.pr_vk);
    }
    sprintf (Lsts->me_einh, "%d", we_jour.me_einh);
    sprintf (Lsts->best_me_einh, "%d", we_jour.best_me_einh);
    sprintf (Lsts->a_krz, "%ld", we_jour.a_krz);
	if (we_jour.inh == 0.0) we_jour.inh = 1.0;
    sprintf (Lsts->inh, "%.3lf", we_jour.inh);
    strcpy (Lsts->ls_ident,  we_jour.ls_ident);
    strcpy (Lsts->ls_charge, we_jour.ls_charge);
    sprintf (Lsts->p_nr, "%hd", we_jour.p_num);

    Einh.AktBestEinh (we_kopf.mdn, we_kopf.fil, we_kopf.lief, 
                      ratod (Lsts->a),Lsts->lief_best, atoi (Lsts->me_einh));
    GetMeEinh (we_kopf.mdn, we_kopf.fil, we_kopf.lief, we_jour.a,we_jour.lief_best);
//    Lsts->me_choise[0] = new ListButton ();
//    Lsts->me_choise[0]->SetFeld ("..");

//    Lsts->me_choise.SetFeld ("..");
	if (atoi (Lsts->best_me_einh) != 0)
	{
        Einh.AktBestEinh (we_kopf.mdn, we_kopf.fil, we_kopf.lief, 
                         ratod (Lsts->a),Lsts->lief_best, atoi (Lsts->best_me_einh));
        GetMeEinhBest (we_kopf.mdn, we_kopf.fil, we_kopf.lief, we_jour.a, we_jour.lief_best);
	}
    Lsts->me_choise.SetFeld ("..");
    dlong_to_asc (we_jour.akv, Lsts->akv);
    dsqlstatus = lese_a_bas (we_jour.a);
    if (dsqlstatus == 0)
    {
        strcpy (Lsts->a_bz1, _a_bas.a_bz1);
        strcpy (Lsts->a_bz2, _a_bas.a_bz2);
    }
}


void WeWork::InitRow (void)
{
    if (Lsts == NULL) return;
    Lsts->me_choise.SetFeld ("..");
}


BOOL WeWork::GenSqlout (char *Tab, char *Feld)
{
    char Feldname [256];
    int i;

    sprintf (Feldname, "%s.%s", Tab, Feld);
    for (i = 0; i < textWork->GetFieldanz (); i ++)
    {
        if (strcmp (Feldname, textWork->GetFeldnamen () [i]) == 0)
        {
            LiefBzg.sqlout ((char *) textWork->GetFelder () [i], 0, 
                                     textWork->GetLength () [i]);
            return TRUE;
        }
    }
    return FALSE;
}

BOOL WeWork::GenSqlout (char *Tab, char *Feld, TextWork *textWork)
{
    char Feldname [256];
    int i;

    sprintf (Feldname, "%s.%s", Tab, Feld);
    for (i = 0; i < textWork->GetFieldanz (); i ++)
    {
        if (strcmp (Feldname, textWork->GetFeldnamen () [i]) == 0)
        {
            LiefBzg.sqlout ((char *) textWork->GetFelder () [i], 0, 
                                     textWork->GetLength () [i]);
            return TRUE;
        }
    }
    return FALSE;
}


char *WeWork::GetWeField (char *BestField, int anz)
{
    clipped (BestField);
    for (int i = 1; i < anz; i ++)
    {
        if (strcmp (wort[i], BestField) == 0)
        {
            return wort [i];
        }
    }
    return NULL;
}

int WeWork::ReadBestTxt (void)
{
    char **Selects;
    char **LiefSelects;
    char tab [81];
    char sql [0x1000];
    int anz;
    int i;
    int dsqlstatus;

    TextWork *bestTextWork;

    bestTextWork = new TextWork ("32400.dlg");
    if (bestTextWork->GetFeldnamen () == NULL)
    {
                 delete bestTextWork;
		         return 0;
    }

    Selects = bestTextWork->GetSelects ();
    if (Selects[0] == NULL)
    {
        delete bestTextWork;
        return 0;
    }
    anz = wsplit (Selects[0], ";");
    if (anz < 2) return 0;

    strcpy (tab, wort [0]);
    strcpy (sql, "select ");

    for (i = 1; i < anz; i ++)
    {
        if (GenSqlout (wort[0], wort[i], bestTextWork) == FALSE) continue;
        if (i > 1)
        {
            strcat (sql, ",");
        }
        strcat (sql, wort[i]);
    }

    strcat (sql, " from ");
    strcat (sql, wort [0]);
    strcat (sql, " where mdn = ? and fil = ? and lief = ? and best_blg = ?");
         

    LiefBzg.sqlin ((short *) &best_kopf.mdn, 1, 0);
    LiefBzg.sqlin ((short *) &best_kopf.fil, 1, 0);
    LiefBzg.sqlin ((char *)  best_kopf.lief, 0, 17);
    LiefBzg.sqlin ((long *)  &best_kopf.best_blg, 2, 0);
    dsqlstatus = LiefBzg.sqlcomm (sql);
    if (dsqlstatus != 0) 
    {
        delete bestTextWork;
        return 0;
    }

    LiefSelects = new char *[anz + 1];
    ExitError (LiefSelects == NULL);

    for (i = 1; i < anz; i ++)
    {
        LiefSelects[i] = new char [80];
        ExitError (LiefSelects [i] == NULL);
        strcpy (LiefSelects [i], wort [i]);
    }

    Selects = textWork->GetSelects ();
    if (Selects == NULL || Selects[0] == NULL)
    {
         for (i = 1; i < anz; i ++)
         {
                delete LiefSelects[i];
         }
         delete LiefSelects;
         delete bestTextWork;
         return 0;
    }
    int lanz = wsplit (Selects[0], ";");
    if (lanz < 2)
    {
         for (i = 1; i < anz; i ++)
         {
                delete LiefSelects[i];
         }
         delete LiefSelects;
         delete bestTextWork;
         return 0;
    }

    WeKopf.dbupdate ();
    sprintf (sql, "update %s set ", wort [0]);

    for (i = 1; i < anz; i ++)
    {
        if (i >= lanz) break;
        if (GenSqlin (tab, LiefSelects[i], bestTextWork) == FALSE) continue;
        if (i > 1)
        {
            strcat (sql, ",");
        }
        char *WeField = GetWeField (LiefSelects[i], lanz);
        if (WeField == NULL) continue;
        strcat (sql, WeField);
        strcat (sql, " = ?");
    }

    strcat (sql, " where mdn = ? and fil = ? and lief = ? and lief_rech_nr = ?"
                 " and blg_typ = ?");    
         
    LiefBzg.sqlin ((short *) &we_kopf.mdn, 1, 0);
    LiefBzg.sqlin ((short *) &we_kopf.fil, 1, 0);
    LiefBzg.sqlin ((char *)  we_kopf.lief, 0, 17);
    LiefBzg.sqlin ((char *)  we_kopf.lief_rech_nr, 0, 17);
    LiefBzg.sqlin ((long *)  we_kopf.blg_typ, 0, 2);
    LiefBzg.sqlcomm (sql);

    
    for (i = 1; i < anz; i ++)
    {
        delete LiefSelects[i];
    }
    delete LiefSelects;
    delete bestTextWork;
    return 0;
}

int WeWork::ReadTxt (void)
{
    char **Selects;
    char sql [0x1000];
    int anz;
    int i;
    int dsqlstatus;

    if (textWork == NULL)
    {
        return 0;
    }

    Selects = textWork->GetSelects ();
    if (Selects[0] == NULL)
    {
        return 0;
    }
    anz = wsplit (Selects[0], ";");
    if (anz < 2) return 0;

    strcpy (sql, "select ");

    for (i = 1; i < anz; i ++)
    {
        if (GenSqlout (wort[0], wort[i]) == FALSE) continue;
        if (i > 1)
        {
            strcat (sql, ",");
        }
        strcat (sql, wort[i]);
    }

    strcat (sql, " from ");
    strcat (sql, wort [0]);
    strcat (sql, " where mdn = ? and fil = ? and lief = ? and lief_rech_nr = ?"
                 " and blg_typ = ?");    
         

    LiefBzg.sqlin ((short *) &we_kopf.mdn, 1, 0);
    LiefBzg.sqlin ((short *) &we_kopf.fil, 1, 0);
    LiefBzg.sqlin ((char *)  we_kopf.lief, 0, 17);
    LiefBzg.sqlin ((char *)  we_kopf.lief_rech_nr, 0, 17);
    LiefBzg.sqlin ((long *)  we_kopf.blg_typ, 0, 2);
    dsqlstatus = LiefBzg.sqlcomm (sql);
    return 0;
}

int WeWork::ReadWeKopf (void)
{
	int dsqlstatus;
	  char sqlbuffer [1024];

    best_koresp = 0;
	best_kopf.kontrakt_nr = 0l;

	// 220409 : vorher lfd lesen, da unique Key im we_kopf mit lfd ist , 
		// wenn nicht �ber unique key gelesen wird , gibt locking-Probleme
      WeKopf.sqlout ((long *) &we_kopf.lfd, 2, 0);
	  sprintf (sqlbuffer, "select lfd from we_kopf "
		                  "where mdn = %hd "
						  "and   fil = %hd "
                          "and   lief_rech_nr = \"%s\" "
                          "and   lief = \"%s\" "
                          "and   blg_typ = \"%s\"",
						  we_kopf.mdn, we_kopf.fil,
						  we_kopf.lief_rech_nr, we_kopf.lief, we_kopf.blg_typ);
	  dsqlstatus = WeKopf.sqlcomm (sqlbuffer);
	  if (dsqlstatus) return dsqlstatus;




	dsqlstatus = WeKopf.dbreadfirst ();
    if (dsqlstatus == 0)
    {
        ReadTxt ();
    }
	return dsqlstatus;
}

long WeWork::GethbkDat (long datum)
{
    int mhd;

    if (strlen(clipped(Lsts->hbk_dat)) > 0)
    {
		return dasc_to_long (Lsts->hbk_dat);
	}

    if (atoi (Lsts->mhd) > 0)
    {
          mhd = atoi (Lsts->mhd);
    }
    else
    {
          mhd = _a_bas.hbk_ztr;
    }
    switch (_a_bas.hbk_kz[0])
    {
          case 'T' :
               return datum + mhd;
          case 'W' :
               return datum + mhd * 7;
          case 'M' :
              return datum + mhd * 30;
          default :
               return datum + mhd;
    }
    return datum;

}


/*
double WeWork::GetLiefWrtO (void)
{
    double lief_wrt_o;

    we_pos.mdn = we_kopf.mdn;
    we_pos.fil = we_kopf.fil;
    strcpy (we_pos.lief, we_kopf.lief);
	strcpy (we_pos.lief_rech_nr, we_kopf.lief_rech_nr);
	strcpy (we_pos.blg_typ, we_kopf.blg_typ);

    LiefBzg.sqlin ((short *) &we_pos.mdn, 1, 0);
    LiefBzg.sqlin ((short *) &we_pos.fil, 1, 0);
    LiefBzg.sqlin ((char *)  we_pos.lief, 0, 17);
    LiefBzg.sqlin ((char *)  we_pos.lief_rech_nr, 0, 17);
    LiefBzg.sqlin ((char *)  &we_pos.blg_typ, 0, 2);
    LiefBzg.sqlout ((double *) &lief_wrt_o, 3, 0);
    lief_wrt_o = 0.0;
    LiefBzg.sqlcomm ("select sum (pr_ek * me) from we_pos "
                     "where mdn = ? "
                     "and fil = ? "
                     "and lief = ? "
                     "and lief_rech_nr = ? "
                     "and blg_typ = ?");
    return lief_wrt_o;
}
*/


double WeWork::GetLiefWrtO (void)
{
	static int cursor = -1;
    static double lief_wrt_o;

    we_jour.mdn = we_kopf.mdn;
    we_jour.fil = we_kopf.fil;
    strcpy (we_jour.lief, we_kopf.lief);
	strcpy (we_jour.lief_rech_nr, we_kopf.lief_rech_nr);
	strcpy (we_jour.blg_typ, we_kopf.blg_typ);

	if (cursor == -1)
	{
		LiefBzg.sqlin ((short *) &we_jour.mdn, 1, 0);
		LiefBzg.sqlin ((short *) &we_jour.fil, 1, 0);
		LiefBzg.sqlin ((char *)  we_jour.lief, 0, 17);
		LiefBzg.sqlin ((char *)  we_jour.lief_rech_nr, 0, 17);
		LiefBzg.sqlin ((char *)  &we_jour.blg_typ, 0, 2);
		LiefBzg.sqlout ((double *) &lief_wrt_o, 3, 0);
	//Lud 090205 runden
		cursor = LiefBzg.sqlcursor ("select sum (round(pr_ek * me,2)) from we_jour "
                     "where mdn = ? "
                     "and fil = ? "
                     "and lief = ? "
                     "and lief_rech_nr = ? "
                     "and blg_typ = ?");
	}
    lief_wrt_o = 0.0;
	LiefBzg.sqlopen (cursor);
	LiefBzg.sqlfetch (cursor);

    LiefBzg.IsDoublenull (lief_wrt_o) ? lief_wrt_o = 0.0 : lief_wrt_o;
    return lief_wrt_o;
}

/*
double WeWork::GetLiefWrtOEu (void)
{
    double lief_wrt_o_eu;

    we_pos.mdn = we_kopf.mdn;
    we_pos.fil = we_kopf.fil;
    strcpy (we_pos.lief, we_kopf.lief);
	strcpy (we_pos.lief_rech_nr, we_kopf.lief_rech_nr);
	strcpy (we_pos.blg_typ, we_kopf.blg_typ);

    LiefBzg.sqlin ((short *) &we_pos.mdn, 1, 0);
    LiefBzg.sqlin ((short *) &we_pos.fil, 1, 0);
    LiefBzg.sqlin ((char *)  we_pos.lief, 0, 17);
    LiefBzg.sqlin ((char *)  we_pos.lief_rech_nr, 0, 17);
    LiefBzg.sqlin ((char *)  &we_pos.blg_typ, 0, 2);
    LiefBzg.sqlout ((double *) &lief_wrt_o_eu, 3, 0);
    lief_wrt_o_eu = 0.0;
    LiefBzg.sqlcomm ("select sum (pr_ek_euro * me) from we_pos "
                     "where mdn = ? "
                     "and fil = ? "
                     "and lief = ? "
                     "and lief_rech_nr = ? "
                     "and blg_typ = ?");
    return lief_wrt_o_eu;
}
*/


double WeWork::GetLiefWrtOEu (void)
{
	static int cursor = -1;
    static double lief_wrt_o_eu; 

    we_jour.mdn = we_kopf.mdn;
    we_jour.fil = we_kopf.fil;
    strcpy (we_jour.lief, we_kopf.lief);
	strcpy (we_jour.lief_rech_nr, we_kopf.lief_rech_nr);
	strcpy (we_jour.blg_typ, we_kopf.blg_typ);

	if (cursor == -1)
	{
		LiefBzg.sqlin ((short *) &we_jour.mdn, 1, 0);
		LiefBzg.sqlin ((short *) &we_jour.fil, 1, 0);
		LiefBzg.sqlin ((char *)  we_jour.lief, 0, 17);
		LiefBzg.sqlin ((char *)  we_jour.lief_rech_nr, 0, 17);
		LiefBzg.sqlin ((char *)  &we_jour.blg_typ, 0, 2);
		LiefBzg.sqlout ((double *) &lief_wrt_o_eu, 3, 0);
	//Lud 090205 runden
		cursor = LiefBzg.sqlcursor ("select sum (round(pr_ek_euro * me,2)) from we_jour "
                     "where mdn = ? "
                     "and fil = ? "
                     "and lief = ? "
                     "and lief_rech_nr = ? "
                     "and blg_typ = ?");
	}
    lief_wrt_o_eu = 0.0;
	LiefBzg.sqlopen (cursor);
	LiefBzg.sqlfetch (cursor);

    LiefBzg.IsDoublenull (lief_wrt_o_eu) ? lief_wrt_o_eu = 0.0 : lief_wrt_o_eu;
    return lief_wrt_o_eu;
}

double WeWork::GetLiefWrtNto (void)
{
	static int cursor = -1;
    static double lief_wrt_nto;

    we_jour.mdn = we_kopf.mdn;
    we_jour.fil = we_kopf.fil;
    strcpy (we_jour.lief, we_kopf.lief);
	strcpy (we_jour.lief_rech_nr, we_kopf.lief_rech_nr);
	strcpy (we_jour.blg_typ, we_kopf.blg_typ);

	if (cursor == -1)
	{
		LiefBzg.sqlin ((short *) &we_jour.mdn, 1, 0);
		LiefBzg.sqlin ((short *) &we_jour.fil, 1, 0);
		LiefBzg.sqlin ((char *)  we_jour.lief, 0, 17);
		LiefBzg.sqlin ((char *)  we_jour.lief_rech_nr, 0, 17);
		LiefBzg.sqlin ((char *)  &we_jour.blg_typ, 0, 2);
		LiefBzg.sqlout ((double *) &lief_wrt_nto, 3, 0);
		cursor = LiefBzg.sqlcursor ("select sum (pr_ek_nto * me) from we_jour "
                     "where mdn = ? "
                     "and fil = ? "
                     "and lief = ? "
                     "and lief_rech_nr = ? "
                     "and blg_typ = ?");
	}
    lief_wrt_nto = 0.0;
	LiefBzg.sqlopen (cursor);
	LiefBzg.sqlfetch (cursor);

    LiefBzg.IsDoublenull (lief_wrt_nto) ? lief_wrt_nto = 0.0 : lief_wrt_nto;
    return lief_wrt_nto;
}

double WeWork::GetLiefWrtNtoEu (void)
{
	static int cursor = -1;
    static double lief_wrt_nto_eu;

    we_jour.mdn = we_kopf.mdn;
    we_jour.fil = we_kopf.fil;
    strcpy (we_jour.lief, we_kopf.lief);
	strcpy (we_jour.lief_rech_nr, we_kopf.lief_rech_nr);
	strcpy (we_jour.blg_typ, we_kopf.blg_typ);

	if (cursor == -1)
	{
		LiefBzg.sqlin ((short *) &we_jour.mdn, 1, 0);
		LiefBzg.sqlin ((short *) &we_jour.fil, 1, 0);
		LiefBzg.sqlin ((char *)  we_jour.lief, 0, 17);
		LiefBzg.sqlin ((char *)  we_jour.lief_rech_nr, 0, 17);
		LiefBzg.sqlin ((char *)  &we_jour.blg_typ, 0, 2);
		LiefBzg.sqlout ((double *) &lief_wrt_nto_eu, 3, 0);
		lief_wrt_nto_eu = 0.0;
	//Lud 090205 runden
		cursor = LiefBzg.sqlcursor ("select sum (round (pr_ek_nto_eu * me,2)) from we_jour "
                     "where mdn = ? "
                     "and fil = ? "
                     "and lief = ? "
                     "and lief_rech_nr = ? "
                     "and blg_typ = ?");
	}
	lief_wrt_nto_eu = 0.0;
	LiefBzg.sqlopen (cursor);
	LiefBzg.sqlfetch (cursor);
    LiefBzg.IsDoublenull (lief_wrt_nto_eu) ? lief_wrt_nto_eu = 0.0 : lief_wrt_nto_eu;
    return lief_wrt_nto_eu;
}

/*
double WeWork::GetLiefWrtAb (void)
{
    double lief_wrt_ab;

    we_pos.mdn = we_kopf.mdn;
    we_pos.fil = we_kopf.fil;
    strcpy (we_pos.lief, we_kopf.lief);
	strcpy (we_pos.lief_rech_nr, we_kopf.lief_rech_nr);
	strcpy (we_pos.blg_typ, we_kopf.blg_typ);

    LiefBzg.sqlin ((short *) &we_pos.mdn, 1, 0);
    LiefBzg.sqlin ((short *) &we_pos.fil, 1, 0);
    LiefBzg.sqlin ((char *)  we_pos.lief, 0, 17);
    LiefBzg.sqlin ((char *)  we_pos.lief_rech_nr, 0, 17);
    LiefBzg.sqlin ((char *)  &we_pos.blg_typ, 0, 2);
    LiefBzg.sqlout ((double *) &lief_wrt_ab, 3, 0);
    lief_wrt_ab = 0.0;
    LiefBzg.sqlcomm ("select sum (pr_ek_nto * me) from we_pos "
                     "where mdn = ? "
                     "and fil = ? "
                     "and lief = ? "
                     "and lief_rech_nr = ? "
                     "and blg_typ = ?");
    return lief_wrt_ab;
}
*/

double WeWork::GetLiefWrtAb (void)
{
	static int cursor = -1;
    static double lief_wrt_ab;

    we_jour.mdn = we_kopf.mdn;
    we_jour.fil = we_kopf.fil;
    strcpy (we_jour.lief, we_kopf.lief);
	strcpy (we_jour.lief_rech_nr, we_kopf.lief_rech_nr);
	strcpy (we_jour.blg_typ, we_kopf.blg_typ);

	if (cursor == -1)
	{
		LiefBzg.sqlin ((short *) &we_jour.mdn, 1, 0);
		LiefBzg.sqlin ((short *) &we_jour.fil, 1, 0);
		LiefBzg.sqlin ((char *)  we_jour.lief, 0, 17);
		LiefBzg.sqlin ((char *)  we_jour.lief_rech_nr, 0, 17);
		LiefBzg.sqlin ((char *)  &we_jour.blg_typ, 0, 2);
		LiefBzg.sqlout ((double *) &lief_wrt_ab, 3, 0);
	//Lud 090205 runden
		cursor = LiefBzg.sqlcursor ("select sum (round (pr_ek_nto * me,2)) from we_jour "
                     "where mdn = ? "
                     "and fil = ? "
                     "and lief = ? "
                     "and lief_rech_nr = ? "
                     "and blg_typ = ?");
	}
    lief_wrt_ab = 0.0;
	LiefBzg.sqlopen (cursor);
	LiefBzg.sqlfetch (cursor);
    LiefBzg.IsDoublenull (lief_wrt_ab) ? lief_wrt_ab = 0.0 : lief_wrt_ab;
    return lief_wrt_ab;
}

/*
double WeWork::GetLiefWrtAbEu (void)
{
    double lief_wrt_ab_eu;

    we_pos.mdn = we_kopf.mdn;
    we_pos.fil = we_kopf.fil;
    strcpy (we_pos.lief, we_kopf.lief);
	strcpy (we_pos.lief_rech_nr, we_kopf.lief_rech_nr);
	strcpy (we_pos.blg_typ, we_kopf.blg_typ);

    LiefBzg.sqlin ((short *) &we_pos.mdn, 1, 0);
    LiefBzg.sqlin ((short *) &we_pos.fil, 1, 0);
    LiefBzg.sqlin ((char *)  we_pos.lief, 0, 17);
    LiefBzg.sqlin ((char *)  we_pos.lief_rech_nr, 0, 17);
    LiefBzg.sqlin ((char *)  &we_pos.blg_typ, 0, 2);
    LiefBzg.sqlout ((double *) &lief_wrt_ab_eu, 3, 0);
    lief_wrt_ab_eu = 0.0;
    LiefBzg.sqlcomm ("select sum (pr_ek_nto_eu * me) from we_pos "
                     "where mdn = ? "
                     "and fil = ? "
                     "and lief = ? "
                     "and lief_rech_nr = ? "
                     "and blg_typ = ?");
    return lief_wrt_ab_eu;
}
*/

double WeWork::GetLiefWrtAbEu (void)
{
	static int cursor = -1;
    static double lief_wrt_ab_eu;

    we_jour.mdn = we_kopf.mdn;
    we_jour.fil = we_kopf.fil;
    strcpy (we_jour.lief, we_kopf.lief);
	strcpy (we_jour.lief_rech_nr, we_kopf.lief_rech_nr);
	strcpy (we_jour.blg_typ, we_kopf.blg_typ);

	if (cursor == -1)
	{
		LiefBzg.sqlin ((short *) &we_jour.mdn, 1, 0);
		LiefBzg.sqlin ((short *) &we_jour.fil, 1, 0);
		LiefBzg.sqlin ((char *)  we_jour.lief, 0, 17);
		LiefBzg.sqlin ((char *)  we_jour.lief_rech_nr, 0, 17);
		LiefBzg.sqlin ((char *)  &we_jour.blg_typ, 0, 2);
		LiefBzg.sqlout ((double *) &lief_wrt_ab_eu, 3, 0);
	//Lud 090205 runden
		cursor = LiefBzg.sqlcursor ("select sum (round(pr_ek_nto_eu * me,2)) from we_jour "
                     "where mdn = ? "
                     "and fil = ? "
                     "and lief = ? "
                     "and lief_rech_nr = ? "
                     "and blg_typ = ?");
	}
    lief_wrt_ab_eu = 0.0;
	LiefBzg.sqlopen (cursor);
	LiefBzg.sqlfetch (cursor);
    LiefBzg.IsDoublenull (lief_wrt_ab_eu) ? lief_wrt_ab_eu = 0.0 : lief_wrt_ab_eu;
    return lief_wrt_ab_eu;
}

/*
double WeWork::GetRabEffEu (void)
{
    double value;

    we_pos.mdn = we_kopf.mdn;
    we_pos.fil = we_kopf.fil;
    strcpy (we_pos.lief, we_kopf.lief);
	strcpy (we_pos.lief_rech_nr, we_kopf.lief_rech_nr);
	strcpy (we_pos.blg_typ, we_kopf.blg_typ);

    LiefBzg.sqlin ((short *) &we_pos.mdn, 1, 0);
    LiefBzg.sqlin ((short *) &we_pos.fil, 1, 0);
    LiefBzg.sqlin ((char *)  we_pos.lief, 0, 17);
    LiefBzg.sqlin ((char *)  we_pos.lief_rech_nr, 0, 17);
    LiefBzg.sqlin ((char *)  &we_pos.blg_typ, 0, 2);
    LiefBzg.sqlout ((double *) &value, 3, 0);
    value = 0.0;
    LiefBzg.sqlcomm ("select sum (rab_eff_eu) from we_pos "
                     "where mdn = ? "
                     "and fil = ? "
                     "and lief = ? "
                     "and lief_rech_nr = ? "
                     "and blg_typ = ?");
    return value;
}
*/

double WeWork::GetRabEffEu (void)
{
	static int cursor = -1;
    static double value;

    we_jour.mdn = we_kopf.mdn;
    we_jour.fil = we_kopf.fil;
    strcpy (we_jour.lief, we_kopf.lief);
	strcpy (we_jour.lief_rech_nr, we_kopf.lief_rech_nr);
	strcpy (we_jour.blg_typ, we_kopf.blg_typ);

	if (cursor == -1)
	{
		LiefBzg.sqlin ((short *) &we_jour.mdn, 1, 0);
		LiefBzg.sqlin ((short *) &we_jour.fil, 1, 0);
		LiefBzg.sqlin ((char *)  we_jour.lief, 0, 17);
		LiefBzg.sqlin ((char *)  we_jour.lief_rech_nr, 0, 17);
		LiefBzg.sqlin ((char *)  &we_jour.blg_typ, 0, 2);
		LiefBzg.sqlout ((double *) &value, 3, 0);
		cursor = LiefBzg.sqlcursor ("select sum (rab_eff_eu) from we_jour "
                     "where mdn = ? "
                     "and fil = ? "
                     "and lief = ? "
                     "and lief_rech_nr = ? "
                     "and blg_typ = ?");
	}
    value = 0.0;
	LiefBzg.sqlopen (cursor);
	LiefBzg.sqlfetch (cursor);
    LiefBzg.IsDoublenull (value) ? value = 0.0 : value;
    return value;
}

/*
double WeWork::GetRabEff (void)
{
    double value;

    we_pos.mdn = we_kopf.mdn;
    we_pos.fil = we_kopf.fil;
    strcpy (we_pos.lief, we_kopf.lief);
	strcpy (we_pos.lief_rech_nr, we_kopf.lief_rech_nr);
	strcpy (we_pos.blg_typ, we_kopf.blg_typ);

    LiefBzg.sqlin ((short *) &we_pos.mdn, 1, 0);
    LiefBzg.sqlin ((short *) &we_pos.fil, 1, 0);
    LiefBzg.sqlin ((char *)  we_pos.lief, 0, 17);
    LiefBzg.sqlin ((char *)  we_pos.lief_rech_nr, 0, 17);
    LiefBzg.sqlin ((char *)  &we_pos.blg_typ, 0, 2);
    LiefBzg.sqlout ((double *) &value, 3, 0);
    value = 0.0;
    LiefBzg.sqlcomm ("select sum (rab_eff) from we_pos "
                     "where mdn = ? "
                     "and fil = ? "
                     "and lief = ? "
                     "and lief_rech_nr = ? "
                     "and blg_typ = ?");
    return value;
}
*/

double WeWork::GetRabEff (void)
{
	static int cursor = -1;
    static double value;

    we_jour.mdn = we_kopf.mdn;
    we_jour.fil = we_kopf.fil;
    strcpy (we_jour.lief, we_kopf.lief);
	strcpy (we_jour.lief_rech_nr, we_kopf.lief_rech_nr);
	strcpy (we_jour.blg_typ, we_kopf.blg_typ);

	if (cursor == -1)
	{
		LiefBzg.sqlin ((short *) &we_jour.mdn, 1, 0);
		LiefBzg.sqlin ((short *) &we_jour.fil, 1, 0);
		LiefBzg.sqlin ((char *)  we_jour.lief, 0, 17);
		LiefBzg.sqlin ((char *)  we_jour.lief_rech_nr, 0, 17);
		LiefBzg.sqlin ((char *)  &we_jour.blg_typ, 0, 2);
		LiefBzg.sqlout ((double *) &value, 3, 0);
		cursor = LiefBzg.sqlcursor ("select sum (rab_eff) from we_jour "
                     "where mdn = ? "
                     "and fil = ? "
                     "and lief = ? "
                     "and lief_rech_nr = ? "
                     "and blg_typ = ?");
	}
    value = 0.0;
	LiefBzg.sqlopen (cursor);
	LiefBzg.sqlfetch (cursor);
    LiefBzg.IsDoublenull (value) ? value = 0.0 : value;
    return value;
}

/*
double WeWork::GetMwstEu (void)
{ 
    double value;
    short mwst;
    double pr_ek;
    double me;
    int cursor;
    char wert [5];
    double ptwer1;

    we_pos.mdn = we_kopf.mdn;
    we_pos.fil = we_kopf.fil;
    strcpy (we_pos.lief, we_kopf.lief);
	strcpy (we_pos.lief_rech_nr, we_kopf.lief_rech_nr);
	strcpy (we_pos.blg_typ, we_kopf.blg_typ);

    LiefBzg.sqlin ((short *) &we_pos.mdn, 1, 0);
    LiefBzg.sqlin ((short *) &we_pos.fil, 1, 0);
    LiefBzg.sqlin ((char *)  we_pos.lief, 0, 17);
    LiefBzg.sqlin ((char *)  we_pos.lief_rech_nr, 0, 17);
    LiefBzg.sqlin ((char *)  &we_pos.blg_typ, 0, 2);
    LiefBzg.sqlout ((short *)  &mwst, 1, 0);
    LiefBzg.sqlout ((double *) &pr_ek,3, 0);
    LiefBzg.sqlout ((double *) &me, 3,0);
    cursor = LiefBzg.sqlcursor ("select mwst, pr_ek_nto_eu, me from we_pos "
                     "where mdn = ? "
                     "and fil = ? "
                     "and lief = ? "
                     "and lief_rech_nr = ? "
                     "and blg_typ = ?");
    value = 0.0;

    mwst = 1;
    while (LiefBzg.sqlfetch (cursor) == 0)
    {
             sprintf (wert, "%hd", mwst);
             if (Ptab.lese_ptab ("mwst", wert) != 0)
             {
                 continue;
             }
             ptwer1 = ratod (ptabn.ptwer1);
             if (ptwer1 == 0.0)
             {
                 ptwer1 = ratod (ptabn.ptwert) / 100;
             }
             value += (pr_ek * me * ptwer1);
             mwst = 1;
    }
    LiefBzg.sqlclose (cursor);
    return value;
}
*/

double WeWork::GetMwstEu (void)
{ 
    static double value;
    static short mwst;
    static double pr_ek;
    static double me;
    static int cursor = -1;
    static char wert [5];
    static double ptwer1;

    we_jour.mdn = we_kopf.mdn;
    we_jour.fil = we_kopf.fil;
    strcpy (we_jour.lief, we_kopf.lief);
	strcpy (we_jour.lief_rech_nr, we_kopf.lief_rech_nr);
	strcpy (we_jour.blg_typ, we_kopf.blg_typ);

	if (cursor == -1)
	{
		LiefBzg.sqlin ((short *) &we_jour.mdn, 1, 0);
		LiefBzg.sqlin ((short *) &we_jour.fil, 1, 0);
		LiefBzg.sqlin ((char *)  we_jour.lief, 0, 17);
		LiefBzg.sqlin ((char *)  we_jour.lief_rech_nr, 0, 17);
		LiefBzg.sqlin ((char *)  &we_jour.blg_typ, 0, 2);
		LiefBzg.sqlout ((short *)  &mwst, 1, 0);
		LiefBzg.sqlout ((double *) &pr_ek,3, 0);
		LiefBzg.sqlout ((double *) &me, 3,0);
		cursor = LiefBzg.sqlcursor ("select mwst, pr_ek_nto_eu, me from we_jour "
                     "where mdn = ? "
                     "and fil = ? "
                     "and lief = ? "
                     "and lief_rech_nr = ? "
                     "and blg_typ = ?");
	}
    value = 0.0;

    mwst = 1;
	LiefBzg.sqlopen (cursor);
    while (LiefBzg.sqlfetch (cursor) == 0)
    {
             sprintf (wert, "%hd", mwst);
             if (Ptab.lese_ptab ("mwst", wert) != 0)
             {
                 continue;
             }
             ptwer1 = ratod (ptabn.ptwer1);
             if (ptwer1 == 0.0)
             {
                 ptwer1 = ratod (ptabn.ptwert) / 100;
             }
             value += (pr_ek * me * ptwer1);
             mwst = 1;
    }
//    LiefBzg.sqlclose (cursor);
    return value;
}


/*
double WeWork::GetMwst (void)
{ 
    double value;
    short mwst;
    double pr_ek;
    double me;
    int cursor;
    char wert [5];
    double ptwer1;

    we_pos.mdn = we_kopf.mdn;
    we_pos.fil = we_kopf.fil;
    strcpy (we_pos.lief, we_kopf.lief);
	strcpy (we_pos.lief_rech_nr, we_kopf.lief_rech_nr);
	strcpy (we_pos.blg_typ, we_kopf.blg_typ);

    LiefBzg.sqlin ((short *) &we_pos.mdn, 1, 0);
    LiefBzg.sqlin ((short *) &we_pos.fil, 1, 0);
    LiefBzg.sqlin ((char *)  we_pos.lief, 0, 17);
    LiefBzg.sqlin ((char *)  we_pos.lief_rech_nr, 0, 17);
    LiefBzg.sqlin ((char *)  &we_pos.blg_typ, 0, 2);
    LiefBzg.sqlout ((short *)  &mwst, 1, 0);
    LiefBzg.sqlout ((double *) &pr_ek,3, 0);
    LiefBzg.sqlout ((double *) &me, 3,0);
    cursor = LiefBzg.sqlcursor ("select mwst, pr_ek_nto, me from we_pos "
                     "where mdn = ? "
                     "and fil = ? "
                     "and lief = ? "
                     "and lief_rech_nr = ? "
                     "and blg_typ = ?");
    value = 0.0;

    mwst = 1;
    while (LiefBzg.sqlfetch (cursor) == 0)
    {
             sprintf (wert, "%hd", mwst);
             if (Ptab.lese_ptab ("mwst", wert) != 0)
             {
                 continue;
             }
             ptwer1 = ratod (ptabn.ptwer1);
             if (ptwer1 == 0.0)
             {
                 ptwer1 = ratod (ptabn.ptwert) / 100;
             }
             value += (pr_ek * me * ptwer1);
             mwst = 1;
    }
    LiefBzg.sqlclose (cursor);
    return value;
}
*/

double WeWork::GetMwst (void)
{ 
    static double value;
    static short mwst;
    static double pr_ek;
    static double me;
    static int cursor = -1;
    static char wert [5];
    static double ptwer1;

    we_jour.mdn = we_kopf.mdn;
    we_jour.fil = we_kopf.fil;
    strcpy (we_jour.lief, we_kopf.lief);
	strcpy (we_jour.lief_rech_nr, we_kopf.lief_rech_nr);
	strcpy (we_jour.blg_typ, we_kopf.blg_typ);

	if (cursor == -1)
	{
		LiefBzg.sqlin ((short *) &we_jour.mdn, 1, 0);
		LiefBzg.sqlin ((short *) &we_jour.fil, 1, 0);
		LiefBzg.sqlin ((char *)  we_jour.lief, 0, 17);
		LiefBzg.sqlin ((char *)  we_jour.lief_rech_nr, 0, 17);
		LiefBzg.sqlin ((char *)  &we_jour.blg_typ, 0, 2);
		LiefBzg.sqlout ((short *)  &mwst, 1, 0);
		LiefBzg.sqlout ((double *) &pr_ek,3, 0);
		LiefBzg.sqlout ((double *) &me, 3,0);
		cursor = LiefBzg.sqlcursor ("select mwst, pr_ek_nto, me from we_jour "
                     "where mdn = ? "
                     "and fil = ? "
                     "and lief = ? "
                     "and lief_rech_nr = ? "
                     "and blg_typ = ?");
	}
    value = 0.0;

    mwst = 1;
    LiefBzg.sqlopen (cursor);
    while (LiefBzg.sqlfetch (cursor) == 0)
    {
             sprintf (wert, "%hd", mwst);
             if (Ptab.lese_ptab ("mwst", wert) != 0)
             {
                 continue;
             }
             ptwer1 = ratod (ptabn.ptwer1);
             if (ptwer1 == 0.0)
             {
                 ptwer1 = ratod (ptabn.ptwert) / 100;
             }
             value += (pr_ek * me * ptwer1);
             mwst = 1;
    }
//  LiefBzg.sqlclose (cursor);
    return value;
}


/*
long WeWork::GetWePosAnz (void)
{
    long anz;

    we_pos.mdn = we_kopf.mdn;
    we_pos.fil = we_kopf.fil;
    strcpy (we_pos.lief, we_kopf.lief);
	strcpy (we_pos.lief_rech_nr, we_kopf.lief_rech_nr);
	strcpy (we_pos.blg_typ, we_kopf.blg_typ);

    LiefBzg.sqlin ((short *) &we_pos.mdn, 1, 0);
    LiefBzg.sqlin ((short *) &we_pos.fil, 1, 0);
    LiefBzg.sqlin ((char *)  we_pos.lief, 0, 17);
    LiefBzg.sqlin ((char *)  we_pos.lief_rech_nr, 0, 17);
    LiefBzg.sqlin ((char *)  &we_pos.blg_typ, 0, 2);
    LiefBzg.sqlout ((long *) &anz, 2, 0);
    anz = 0l;
    LiefBzg.sqlcomm ("select count (*) from we_pos "
                     "where mdn = ? "
                     "and fil = ? "
                     "and lief = ? "
                     "and lief_rech_nr = ? "
                     "and blg_typ = ?");
    return anz;
}
*/

long WeWork::GetWePosAnz (void)
{
	static int cursor = -1;
    static long anz;

    we_jour.mdn = we_kopf.mdn;
    we_jour.fil = we_kopf.fil;
    strcpy (we_jour.lief, we_kopf.lief);
	strcpy (we_jour.lief_rech_nr, we_kopf.lief_rech_nr);
	strcpy (we_jour.blg_typ, we_kopf.blg_typ);

	if (cursor == -1)
	{
		LiefBzg.sqlin ((short *) &we_jour.mdn, 1, 0);
		LiefBzg.sqlin ((short *) &we_jour.fil, 1, 0);
		LiefBzg.sqlin ((char *)  we_jour.lief, 0, 17);
		LiefBzg.sqlin ((char *)  we_jour.lief_rech_nr, 0, 17);
		LiefBzg.sqlin ((char *)  we_jour.blg_typ, 0, 2);
		LiefBzg.sqlout ((long *) &anz, 2, 0);
		cursor = LiefBzg.sqlcursor ("select count (*) from we_jour "
                     "where mdn = ? "
                     "and fil = ? "
                     "and lief = ? "
                     "and lief_rech_nr = ? "
                     "and blg_typ = ?");
	}
    anz = 0l;
	LiefBzg.sqlopen (cursor);
	LiefBzg.sqlfetch (cursor);
    LiefBzg.IsLongnull (anz) ? anz = 0l : anz;
    return anz;
}
    
/*
long WeWork::GetWePosAnzMa (void)
{
    long anz;

    we_pos.mdn = we_kopf.mdn;
    we_pos.fil = we_kopf.fil;
    strcpy (we_pos.lief, we_kopf.lief);
	strcpy (we_pos.lief_rech_nr, we_kopf.lief_rech_nr);
	strcpy (we_pos.blg_typ, we_kopf.blg_typ);

    LiefBzg.sqlin ((short *) &we_pos.mdn, 1, 0);
    LiefBzg.sqlin ((short *) &we_pos.fil, 1, 0);
    LiefBzg.sqlin ((char *)  we_pos.lief, 0, 17);
    LiefBzg.sqlin ((char *)  we_pos.lief_rech_nr, 0, 17);
    LiefBzg.sqlin ((char *)  &we_pos.blg_typ, 0, 2);
    LiefBzg.sqlout ((long *) &anz, 2, 0);
    anz = 0l;
    LiefBzg.sqlcomm ("select count (*) from we_pos "
                     "where mdn = ? "
                     "and fil = ? "
                     "and lief = ? "
                     "and lief_rech_nr = ? "
                     "and blg_typ = ? "
                     "and qua_kz > \"0\" "
                     "and qua_kz is not null");
    return anz;
}
*/ 

long WeWork::GetWePosAnzMa (void)
{
	static int cursor = -1;
    static long anz;

    we_jour.mdn = we_kopf.mdn;
    we_jour.fil = we_kopf.fil;
    strcpy (we_jour.lief, we_kopf.lief);
	strcpy (we_jour.lief_rech_nr, we_kopf.lief_rech_nr);
	strcpy (we_jour.blg_typ, we_kopf.blg_typ);

	if (cursor == -1)
	{
		LiefBzg.sqlin ((short *) &we_jour.mdn, 1, 0);
		LiefBzg.sqlin ((short *) &we_jour.fil, 1, 0);
		LiefBzg.sqlin ((char *)  we_jour.lief, 0, 17);
		LiefBzg.sqlin ((char *)  we_jour.lief_rech_nr, 0, 17);
		LiefBzg.sqlin ((char *)  &we_jour.blg_typ, 0, 2);
		LiefBzg.sqlout ((long *) &anz, 2, 0);
		cursor = LiefBzg.sqlcursor ("select count (*) from we_jour "
                     "where mdn = ? "
                     "and fil = ? "
                     "and lief = ? "
                     "and lief_rech_nr = ? "
                     "and blg_typ = ? "
                     "and qua_kz > \"0\" "
                     "and qua_kz is not null");
	}
    anz = 0l;
	LiefBzg.sqlopen (cursor);
	LiefBzg.sqlfetch (cursor);
    LiefBzg.IsLongnull (anz) ? anz = 0l : anz;
    return anz;
}


void WeWork::WriteWePos (WE_POS_CLASS *Wepos, int row)
{
    char datum [12];

    if (Lsts == NULL) return;

    sysdate (datum);
    memcpy (&we_pos, &we_pos_null, sizeof (struct WE_POS));
    we_pos.mdn = we_kopf.mdn;
    we_pos.fil = we_kopf.fil;
    strcpy (we_pos.lief, clipped (we_kopf.lief));
	strcpy (we_pos.lief_rech_nr, clipped (we_kopf.lief_rech_nr));
	strcpy (we_pos.blg_typ, we_kopf.blg_typ);
 
    we_pos.a         = ratod (Lsts->a);
    lese_a_bas (we_pos.a);
    strcpy (we_pos.lief_best, clipped (Lsts->lief_best));
    we_pos.me        = ratod (Lsts->me);
    we_pos.tara      = ratod (Lsts->tara);
    we_pos.pr_ek     = ratod (Lsts->pr_ek);
    we_pos.pr_ek_nto = ratod (Lsts->netto_ek);
    we_pos.pr_fil_ek = ratod (Lsts->pr_fil_ek);
    we_pos.pr_vk     = ratod (Lsts->pr_vk);
    strcpy (we_pos.pers_nam, sys_ben.pers_nam);
    we_pos.p_num     = (row + 1) * 10; 
    we_pos.me_einh   = atoi (Lsts->me_einh);
    we_pos.bearb = dasc_to_long (datum);
    we_pos.akv   = dasc_to_long (Lsts->akv);
    we_pos.lief_s = atoi (we_pos.lief);
    we_pos.hbk_dat = GethbkDat (dasc_to_long (datum));
    we_pos.a_krz = atol (Lsts->a_krz);
    we_pos.inh   = ratod (Lsts->inh);
    if (we_pos.inh == 0.0)
    {
        we_pos.inh = 1.0;
    }
    strcpy (we_pos.ls_ident, Lsts->ls_ident);
    strcpy (we_pos.ls_charge, Lsts->ls_charge);
    if (_mdn.konversion == 0)
	{
			_mdn.konversion = 1;
	}
    if (lief.waehrung == 2)
    {
        we_pos.pr_ek_euro    = we_pos.pr_ek;
        we_pos.pr_ek_nto_eu  = we_pos.pr_ek_nto;
        we_pos.pr_ek *= _mdn.konversion;
        we_pos.pr_ek_nto *= _mdn.konversion;
    }
    else
    {
        we_pos.pr_ek_euro    = we_pos.pr_ek / _mdn.konversion;
        we_pos.pr_ek_nto_eu  = we_pos.pr_ek_nto / _mdn.konversion;
    }

    if (atoi (_mdn.waehr_prim) == 2)
    {
        we_pos.pr_fil_ek_eu = we_pos.pr_fil_ek;
        we_pos.pr_vk_eu = we_pos.pr_vk;
        we_pos.pr_fil_ek *= _mdn.konversion;
        we_pos.pr_vk *= _mdn.konversion;
    }
    else
    {
        we_pos.pr_fil_ek_eu = we_pos.pr_fil_ek /=_mdn.konversion;;
        we_pos.pr_vk_eu = we_pos.pr_vk /=_mdn.konversion;;
    }

    we_pos.rab_eff    = (we_pos.pr_ek - we_pos.pr_ek_nto) * we_pos.me;
    we_pos.rab_eff_eu = (we_pos.pr_ek_euro - we_pos.pr_ek_nto_eu) * we_pos.me;
    sprintf (we_pos.qua_kz, "%d",  TestQsPos ());
    we_pos.mwst = _a_bas.mwst;
    Wepos->dbupdate ();
}


BOOL WeWork::GetKreditor (void)
{
    SYS_PAR_CLASS Syspar;
    BOOL kreditor = TRUE;

    strcpy (sys_par.sys_par_nam,"kreditor_par");
    if (Syspar.dbreadfirst () == 0)
    {
        kreditor = atoi (sys_par.sys_par_wrt); 
    }
    return kreditor;
}

BOOL WeWork::WeFilBewPar ()
{
    SYS_PAR_CLASS Syspar;

    if (!WeFilBewParOK)
	{
	   strcpy (sys_par.sys_par_nam,"we_fil_bew_par");
		if (Syspar.dbreadfirst () == 0)
		{
			we_fil_bew_par = atoi (sys_par.sys_par_wrt); 
		}
        WeFilBewParOK = TRUE;
	}
    return we_fil_bew_par;
}
BOOL WeWork::OhneFilEkAktionPar ()
{
    SYS_PAR_CLASS Syspar;

    if (!OhneFilEkAktionOK)
	{
	   strcpy (sys_par.sys_par_nam,"OhneFilEkAktion");
		if (Syspar.dbreadfirst () == 0)
		{
			OhneFilEkAktion_par = atoi (sys_par.sys_par_wrt); 
		}
        OhneFilEkAktionOK = TRUE;
	}
    return OhneFilEkAktion_par;
}
BOOL WeWork::MultimandantPar ()
{
    SYS_PAR_CLASS Syspar;

    if (!MultimandantParOK)
	{
	   strcpy (sys_par.sys_par_nam,"multimandant");
		if (Syspar.dbreadfirst () == 0)
		{
			multimandant_par = atoi (sys_par.sys_par_wrt); 
		}
        MultimandantParOK = TRUE;
	}
    return multimandant_par;
}
short WeWork::WeVkPar ()
{
    SYS_PAR_CLASS Syspar;

    if (!WeVkParOK)
	{
	   strcpy (sys_par.sys_par_nam,"we_vk_par");
		if (Syspar.dbreadfirst () == 0)
		{
			we_vk_par = atoi (sys_par.sys_par_wrt); 
		    if (we_kopf.fil == 0) we_vk_par = 0;     //z.Z nur editierbar bei Streckengesch�ft LuD 240406

		}
        WeVkParOK = TRUE;
	}
    return we_vk_par;
}

/*
	template <> CDataCollection<SCharge> Chargen;
	char EZNr[41];
	char ESNr[41];
	char Countries[41];
	char IdentNr[41];

   double    a;
   char      ls_ident[21];
   char      ls_charge[21];
   char      ls_ez[21];
   char      ls_es[21];
   char      ls_land[21];
   short     gebland;
   short     mastland;
   short     schlaland;
   short     zerlland;

*/


void WeWork::FillCountries ()
{
		static CVector Countries;
		PTABN *p;
		char *c;
		Token t;

		if (Countries.GetCount () == 0)
		{
			int dsqlstatus = Ptab.lese_ptab_all ("staat");
			while (dsqlstatus == 0)
			{
				p = new PTABN;
				memcpy (p, &ptabn, sizeof (PTABN));
				Countries.Add (p);
				dsqlstatus = Ptab.lese_ptab_all ();
			}
		}

		t.SetSep (" ");
		t = rind.ls_land;
		int i = 0;
		while ((c = t.NextToken ()) != NULL)
		{
			Text L1 = c;
			L1.Trim ();
			Countries.FirstPosition ();
			while ((p = (PTABN *) Countries.GetNext ()) != NULL)
			{
				Text L2 = p->ptwer2;
				L2.Trim ();
				if (L1 == L2) break;
			}
			if (p == NULL)
			{
				continue;
			}
			switch (i)
			{
			case 0:
				rind.gebland = atoi (p->ptwert);
				break;
			case 1:
				rind.mastland = atoi (p->ptwert);
				break;
			case 2:
				rind.schlaland = atoi (p->ptwert);
				break;
			case 3:
				rind.zerlland = atoi (p->ptwert);
				break;
			}
			i ++;
			if (i > 3) break;
		}
}

int WeWork::GetCountrieByKey (CVector *Countries, int key)
{
	    PTABN *p;
		int i = 0;
		Countries->FirstPosition ();
		while ((p = (PTABN *) Countries->GetNext ()) != NULL)
		{
			if (key == atoi (p->ptwert))
			{
				return i;
			}
			i ++;
		}
		return 0;
}

void WeWork::ReadCountries ()
{
		static CVector Countries;
		PTABN *p;
		Token t;

		if (Countries.GetCount () == 0)
		{
			int dsqlstatus = Ptab.lese_ptab_all ("staat");
			while (dsqlstatus == 0)
			{
				p = new PTABN;
				memcpy (p, &ptabn, sizeof (PTABN));
				Countries.Add (p);
				dsqlstatus = Ptab.lese_ptab_all ();
			}
		}

		Text Laender = "";
        p = (PTABN *) Countries.Get (GetCountrieByKey (&Countries, zerldaten.gebland));
		Laender += p->ptwer2;
		Laender.Trim ();
		Laender += " ";
        p = (PTABN *) Countries.Get (GetCountrieByKey (&Countries, zerldaten.mastland));
		Laender += p->ptwer2;
		Laender.Trim ();
		Laender += " ";
        p = (PTABN *) Countries.Get (GetCountrieByKey (&Countries, zerldaten.schlaland));
		Laender += p->ptwer2;
		Laender.Trim ();
		Laender += " ";
        p = (PTABN *) Countries.Get (GetCountrieByKey (&Countries, zerldaten.zerland));
		Laender += p->ptwer2;
		Laender.Trim ();
        strcpy (rind.ls_land , Laender.GetBuffer ());
}

void WeWork::FillZerlLfd ()
{
	static int cursor = -1;
	static int ins_cursor = -1;
	static long lfd = 0;

	lfd = 0;
	if (cursor == -1)
	{
		Zerldaten.sqlout ((long *) &lfd, 2, 0);
		cursor = Zerldaten.sqlcursor ("select max (lfd) from zerldaten");
		Zerldaten.sqlin ((long *) &zerldaten.lfd, 2, 0);
		Zerldaten.sqlin ((long *) &zerldaten.lfd_i, 2, 0);
		ins_cursor = Zerldaten.sqlcursor ("update zerldaten set lfd_i = ? where lfd = ?");
	}
	if (cursor == -1) return;
	Zerldaten.sqlopen (cursor);
	if (Zerldaten.sqlfetch (cursor) == 0)
	{
		zerldaten.lfd = lfd;
		zerldaten.lfd_i = lfd;
		Zerldaten.sqlexecute (ins_cursor);
	}
}


void WeWork::WriteExtraData (CExtraData *ExtraData)
{
	char datum[12];

	sysdate (datum);

    if (Lsts == NULL) return;
    if (_a_bas.charg_hand != 9 && _a_bas.zerl_eti != 1) return;  
    SCharge *Charge = ExtraData->Get (0);
    rind.a = ratod (Lsts->a);
//	strcpy (rind.ls_charge, Charge->Charge);
	strcpy (rind.ls_ident, ExtraData->IdentNr);
	strcpy (rind.ls_ez, ExtraData->EZNr);
	strcpy (rind.ls_es, ExtraData->ESNr);
	strcpy (rind.ls_land, ExtraData->Countries);
	FillCountries ();

//	Rind.dbupdate ();
	strcpy (Lsts->ls_ident, ExtraData->IdentNr);
	strcpy (zerldaten.ident_intern, Lsts->ls_ident); 
	strcpy (zerldaten.ident_extern, Lsts->ls_ident); 
	zerldaten.a = ratod (Lsts->a); 
	zerldaten.a_gt = ratod (Lsts->a); 
	strcpy (zerldaten.eznum1, ExtraData->EZNr); 
	strcpy (zerldaten.esnum,  ExtraData->ESNr); 
	zerldaten.gebland = rind.gebland;
	zerldaten.mastland = rind.mastland;
	zerldaten.schlaland = rind.schlaland;
	zerldaten.zerland = rind.zerlland;
	strcpy (zerldaten.lief, we_kopf.lief);
	strcpy (zerldaten.lief_rech_nr, we_kopf.lief_rech_nr);
	zerldaten.mdn = we_kopf.mdn;
	zerldaten.fil = we_kopf.fil;
	zerldaten.dat = dasc_to_long (datum);
	zerldaten.aktiv = 1;
	strcpy(lief.eznum, zerldaten.eznum1);
	strcpy(lief.esnum,zerldaten.esnum);
	if (Zerldaten.dbtestinsert () == 0)
	{
		Zerldaten.dbmodify ();
	}
	else
	{
		Zerldaten.dbinsert ();
		FillZerlLfd ();
	}
}

void WeWork::ReadExtraData (CExtraData *ExtraData)
{
	char datum[12];

	sysdate (datum);

    if (Lsts == NULL) return;

	zerldaten.mdn = we_kopf.mdn;
	zerldaten.fil = we_kopf.fil;
	zerldaten.dat = dasc_to_long (datum);
	zerldaten.aktiv = 1;
}


void WeWork::WriteWeJour (WE_JOUR_CLASS *Wejour, int row)
{
    char datum [12];
    SYS_PAR_CLASS Syspar;
    BOOL kreditor = TRUE;

    if (Lsts == NULL) return;

    strcpy (sys_par.sys_par_nam,"kreditor_par");
    if (Syspar.dbreadfirst () == 0)
    {
        kreditor = atoi (sys_par.sys_par_wrt); 
    }

    sysdate (datum);
    memcpy (&we_jour, &we_jour_null, sizeof (struct WE_JOUR));
    we_jour.mdn = we_kopf.mdn;
    we_jour.fil = we_kopf.fil;
    strcpy (we_jour.lief, clipped (we_kopf.lief));
	strcpy (we_jour.lief_rech_nr, clipped (we_kopf.lief_rech_nr));
	strcpy (we_jour.blg_typ, we_kopf.blg_typ);
    if (kreditor)
    {
          we_jour.lief_s = atol (we_jour.lief);
    }
 
    we_jour.a         = ratod (Lsts->a);
    lese_a_bas (we_jour.a);
    strcpy (we_jour.lief_best, clipped (Lsts->lief_best));
    we_jour.me        = ratod (Lsts->me);
	if (strcmp (we_jour.blg_typ, "X") == 0)
	{
/* blg_typ macht in Felgeprogrammen die Menge negativ, deshalb wird nicht
   mehr mit -1 multiprilziert. 01.02.2005. Anordnung von Detlef  
		we_jour.me *= -1;
*/
	}
    we_jour.best_me   = ratod (Lsts->best_me);
    we_jour.tara      = ratod (Lsts->tara);
    we_jour.pr_ek     = ratod (Lsts->pr_ek);
    we_jour.pr_ek_nto = ratod (Lsts->netto_ek);
    we_jour.pr_fil_ek = ratod (Lsts->pr_fil_ek);
    we_jour.pr_vk     = ratod (Lsts->pr_vk);
    strcpy (we_jour.pers_nam, sys_ben.pers_nam);
    we_jour.p_num     = (row + 1) * 10; 
    we_jour.int_pos   = (row + 1) * 10; 
    we_jour.me_einh   = atoi (Lsts->me_einh);
    we_jour.bearb = dasc_to_long (datum);
    we_jour.akv   = dasc_to_long (Lsts->akv);
    we_jour.lief_s = atoi (we_jour.lief);
    we_jour.hbk_dat = GethbkDat (dasc_to_long (datum));
    we_jour.a_krz = atol (Lsts->a_krz);
    we_jour.inh   = ratod (Lsts->inh);
    strcpy (we_jour.me_kz,  Lsts->me_kz);
    if (we_jour.inh == 0.0)
    {
        we_jour.inh = 1.0;
    }
    strcpy (we_jour.ls_ident, Lsts->ls_ident);
    strcpy (we_jour.ls_charge, Lsts->ls_charge);
    if (_mdn.konversion == 0)
	{
			_mdn.konversion = 1;
	}
    if (lief.waehrung == 2)
    {
        we_jour.pr_ek_euro    = we_jour.pr_ek;
        we_jour.pr_ek_nto_eu  = we_jour.pr_ek_nto;
        we_jour.pr_ek *= _mdn.konversion;
        we_jour.pr_ek_nto *= _mdn.konversion;
    }
    else
    {
        we_jour.pr_ek_euro    = we_jour.pr_ek / _mdn.konversion;
        we_jour.pr_ek_nto_eu  = we_jour.pr_ek_nto / _mdn.konversion;
    }

	//250506
	if (we_jour.pr_ek_nto_eu <= 0.0)
	{
		we_jour.pr_fil_ek = we_jour.pr_ek_nto_eu;
	}

    if (atoi (_mdn.waehr_prim) == 2)
    {
        we_jour.pr_fil_ek_eu = we_jour.pr_fil_ek;
        we_jour.pr_vk_eu = we_jour.pr_vk;
        we_jour.pr_fil_ek *= _mdn.konversion;
        we_jour.pr_vk *= _mdn.konversion;
    }
    else
    {
        we_jour.pr_fil_ek_eu = we_jour.pr_fil_ek /=_mdn.konversion;;
        we_jour.pr_vk_eu = we_jour.pr_vk /=_mdn.konversion;;
    }

    we_jour.rab_eff    = (we_jour.pr_ek - we_jour.pr_ek_nto) * we_jour.me;
    we_jour.rab_eff_eu = (we_jour.pr_ek_euro - we_jour.pr_ek_nto_eu) * we_jour.me;
    sprintf (we_jour.qua_kz, "%d",  TestQsPos ());
	if (lief.steuer_kz[0] == 'V')
	{
		if (lief.mwst > 0)
		{
			_a_bas.mwst = lief.mwst; 
		}
	}
    we_jour.mwst = _a_bas.mwst;
//    we_jour.mwst = _a_bas.mwst + 10;   22.04.2004 + 10 rausnehemen Sigi
    Wejour->dbupdate ();
    WritePr ();

	if (strcmp (we_jour.blg_typ, "X") != 0)
	{
	   preise_we.mdn = we_jour.mdn;
	   preise_we.a = we_jour.a;
       strcpy (preise_we.lief, we_jour.lief);
       strcpy (preise_we.lief_rech_nr, we_jour.lief_rech_nr);
	   preise_we.best_blg = 0;
	   preise_we_class.dbreadfirst ();

	   preise_we.we_dat = we_kopf.we_dat;
	   preise_we.pr_ek_w = we_jour.pr_ek_euro;
	   preise_we.me_einh_w = we_jour.me_einh;
	   preise_we_class.dbupdate ();
	}

}

void WeWork::WritePr (void)
{
    A_KALKPREIS_CLASS A_kalkpreis;
	char Time[10];
    double pr_ek_nto = we_jour.pr_ek_nto_eu; 
    double we_me     = we_jour.me; 

    lief_bzg.mdn = we_jour.mdn;
    lief_bzg.fil = we_jour.fil;
    ReadLief_besta (we_jour.a);
    
//    if (lief_bzg.me_kz[0] == '1')
    {
             if (Lsts->inh_einh == 0)
             {
                 Lsts->inh_einh = lief_bzg.min_best;
             }
             if (Lsts->inh_einh != 0)
             {
                 pr_ek_nto  = (double) pr_ek_nto / Lsts->inh_einh;
                 we_me      = (double) we_me * Lsts->inh_einh;
             }
    }
    if (_a_bas.a_typ == 1)
    {
           A_KALKHNDW_CLASS *Hndw = new A_KALKHNDW_CLASS ();
           a_kalkhndw.mdn = we_jour.mdn;
           a_kalkhndw.fil = we_jour.fil;
           a_kalkhndw.a   = we_jour.a;
           int dsqlstatus = Hndw->dbreadfirst ();
           if (dsqlstatus == 0)
           {
               a_kalkhndw.pr_ek3 = a_kalkhndw.pr_ek2;
               a_kalkhndw.pr_ek2 = a_kalkhndw.pr_ek1;
               a_kalkhndw.we_me3 = a_kalkhndw.we_me2;
               a_kalkhndw.we_me2 = a_kalkhndw.we_me1;
           }
           a_kalkhndw.pr_ek1 = pr_ek_nto;
           a_kalkhndw.sk_teilk = pr_ek_nto;
           a_kalkhndw.sk_vollk = pr_ek_nto;
           a_kalkhndw.we_me1 = we_me;
           Hndw->dbupdate ();
		   if (MultimandantPar () == TRUE)
		   {
			   a_kalkhndw.mdn = 0;
			   a_kalkhndw.fil = 0;
	           Hndw->dbupdate ();
		   }
	       delete Hndw;
		   memcpy (&a_kalkpreis, &a_kalkpreis_null, sizeof (A_KALKPREIS));
		   a_kalkpreis.mdn = a_kalkhndw.mdn;
		   a_kalkpreis.fil = a_kalkhndw.fil;
		   a_kalkpreis.a = a_kalkhndw.a;
		   A_kalkpreis.dbreadfirst ();
		   a_kalkpreis.mat_o_b = a_kalkhndw.pr_ek1;
		   a_kalkpreis.hk_vollk = a_kalkhndw.pr_ek1;
		   a_kalkpreis.hk_teilk = a_kalkhndw.pr_ek1;
		   a_kalkpreis.sk_vollk = a_kalkhndw.sk_vollk;
		   a_kalkpreis.sk_teilk = a_kalkhndw.sk_teilk;
		   a_kalkpreis.fil_ek_vollk = a_kalkhndw.fil_ek_vollk;
		   a_kalkpreis.fil_ek_teilk = a_kalkhndw.fil_ek_teilk;
		   a_kalkpreis.dat = we_kopf.we_dat;
		   strcpy (a_kalkpreis.programm, "35100");
		   systime (Time);
		   strncpy (a_kalkpreis.zeit, Time,5);
		   Time[5] = 0;
		   A_kalkpreis.dbupdate ();
		   if (MultimandantPar () == TRUE)
		   {
			   a_kalkpreis.mdn = 0;
			   a_kalkpreis.fil = 0;
			   A_kalkpreis.dbupdate ();
		   }
           WriteLief ();
    }
    if (_a_bas.a_typ == 5 ||
             _a_bas.a_typ == 6 ||
             _a_bas.a_typ == 9)
    {
           A_KALK_MAT_CLASS *Mat = new A_KALK_MAT_CLASS ();
           a_kalk_mat.mdn = we_jour.mdn;
           a_kalk_mat.fil = we_jour.fil;
           a_kalk_mat.a   = we_jour.a;
           int dsqlstatus = Mat->dbreadfirst ();
           a_kalk_mat.mat_o_b = pr_ek_nto;
           a_kalk_mat.hk_teilk = pr_ek_nto;
           a_kalk_mat.hk_vollk = pr_ek_nto;
           Mat->dbupdate ();
		   if (MultimandantPar () == TRUE)
		   {
			   a_kalk_mat.mdn = 0;
			   a_kalk_mat.fil = 0;
			   Mat->dbupdate ();
		   }
           delete Mat;
		   memcpy (&a_kalkpreis, &a_kalkpreis_null, sizeof (A_KALKPREIS));
		   a_kalkpreis.mdn = a_kalk_mat.mdn;
		   a_kalkpreis.fil = a_kalk_mat.fil;
		   a_kalkpreis.a = a_kalk_mat.a;
		   A_kalkpreis.dbreadfirst ();
		   a_kalkpreis.mat_o_b  = a_kalk_mat.mat_o_b;
		   a_kalkpreis.hk_vollk = a_kalk_mat.hk_vollk;
		   a_kalkpreis.hk_teilk = a_kalk_mat.hk_teilk;
		   a_kalkpreis.dat = we_kopf.we_dat;
		   strcpy (a_kalkpreis.programm, "35100");
		   systime (Time);
		   strncpy (a_kalkpreis.zeit, Time,5);
		   Time[5] = 0;
		   A_kalkpreis.dbupdate ();
		   if (MultimandantPar () == TRUE)
		   {
			   a_kalkpreis.mdn = 0;
			   a_kalkpreis.fil = 0;
			   A_kalkpreis.dbupdate ();
		   }
    }
    if (_a_bas.a_typ2 == 1)
    {
           A_KALKHNDW_CLASS *Hndw = new A_KALKHNDW_CLASS ();
           a_kalkhndw.mdn = we_jour.mdn;
           a_kalkhndw.fil = we_jour.fil;
           a_kalkhndw.a   = we_jour.a;
           int dsqlstatus = Hndw->dbreadfirst ();
           if (dsqlstatus == 0)
           {
               a_kalkhndw.pr_ek3 = a_kalkhndw.pr_ek2;
               a_kalkhndw.pr_ek2 = a_kalkhndw.pr_ek1;
               a_kalkhndw.we_me3 = a_kalkhndw.we_me2;
               a_kalkhndw.we_me2 = a_kalkhndw.we_me1;
           }
           a_kalkhndw.pr_ek1 = pr_ek_nto;
           a_kalkhndw.we_me1 = we_me;
           a_kalkhndw.sk_teilk = pr_ek_nto;
           a_kalkhndw.sk_vollk = pr_ek_nto;
           Hndw->dbupdate ();
		   if (MultimandantPar () == TRUE)
		   {
			   a_kalkhndw.mdn = 0;
			   a_kalkhndw.fil = 0;
	           Hndw->dbupdate ();
		   }
           delete Hndw;
		   memcpy (&a_kalkpreis, &a_kalkpreis_null, sizeof (A_KALKPREIS));
		   a_kalkpreis.mdn = a_kalkhndw.mdn;
		   a_kalkpreis.fil = a_kalkhndw.fil;
		   a_kalkpreis.a = a_kalkhndw.a;
		   A_kalkpreis.dbreadfirst ();
		   a_kalkpreis.mat_o_b = a_kalkhndw.pr_ek1;
		   a_kalkpreis.hk_vollk = a_kalkhndw.pr_ek1;
		   a_kalkpreis.hk_teilk = a_kalkhndw.pr_ek1;
		   a_kalkpreis.sk_vollk = a_kalkhndw.sk_vollk;
		   a_kalkpreis.sk_teilk = a_kalkhndw.sk_teilk;
		   a_kalkpreis.fil_ek_vollk = a_kalkhndw.fil_ek_vollk;
		   a_kalkpreis.fil_ek_teilk = a_kalkhndw.fil_ek_teilk;
		   a_kalkpreis.dat = we_kopf.we_dat;
		   strcpy (a_kalkpreis.programm, "35100");
		   systime (Time);
		   strncpy (a_kalkpreis.zeit, Time,5);
		   Time[5] = 0;
		   A_kalkpreis.dbupdate ();
		   if (MultimandantPar () == TRUE)
		   {
			   a_kalkpreis.mdn = 0;
			   a_kalkpreis.fil = 0;
			   A_kalkpreis.dbupdate ();
		   }
           WriteLief ();
    }
    if (_a_bas.a_typ2 == 5 ||
             _a_bas.a_typ2 == 6 ||
             _a_bas.a_typ2 == 9)
    {
           A_KALK_MAT_CLASS *Mat = new A_KALK_MAT_CLASS ();
           a_kalk_mat.mdn = we_jour.mdn;
           a_kalk_mat.fil = we_jour.fil;
           a_kalk_mat.a   = we_jour.a;
           int dsqlstatus = Mat->dbreadfirst ();
           a_kalk_mat.mat_o_b = pr_ek_nto;
           a_kalk_mat.hk_teilk = pr_ek_nto;
           a_kalk_mat.hk_vollk = pr_ek_nto;
           Mat->dbupdate ();
		   if (MultimandantPar () == TRUE)
		   {
			   a_kalk_mat.mdn = 0;
			   a_kalk_mat.fil = 0;
			   Mat->dbupdate ();
		   }
           delete Mat;
		   memcpy (&a_kalkpreis, &a_kalkpreis_null, sizeof (A_KALKPREIS));
		   a_kalkpreis.mdn = a_kalk_mat.mdn;
		   a_kalkpreis.fil = a_kalk_mat.fil;
		   a_kalkpreis.a = a_kalk_mat.a;
		   A_kalkpreis.dbreadfirst ();
		   a_kalkpreis.mat_o_b  = a_kalk_mat.mat_o_b;
		   a_kalkpreis.hk_vollk = a_kalk_mat.hk_vollk;
		   a_kalkpreis.hk_teilk = a_kalk_mat.hk_teilk;
		   a_kalkpreis.dat = we_kopf.we_dat;
		   strcpy (a_kalkpreis.programm, "35100");
		   systime (Time);
		   strncpy (a_kalkpreis.zeit, Time,5);
		   Time[5] = 0;
		   A_kalkpreis.dbupdate ();
		   if (MultimandantPar () == TRUE)
		   {
			   a_kalkpreis.mdn = 0;
			   a_kalkpreis.fil = 0;
			   A_kalkpreis.dbupdate ();
		   }
    }
}

void WeWork::WriteLief (void)
{
    A_BEST_CLASS Best;

    a_best.mdn = we_jour.mdn;
    a_best.fil = we_jour.fil;
    a_best.a   = we_jour.a;
    Best.dbreadfirst ();
    strcpy (a_best.letzt_lief_nr, we_jour.lief);
//    Best.dbupdate ();
    Best.dbupdate (); //141010
}


int WeWork::Reada_pr (void)
/**
Artikelpreis aus a_pr holen.
**/
{
	static int cursor1 = -1;
	static int cursor2 = -1;
    static char datum [12];
    static short mdn_gr;
    static short fil_gr;

	if (cursor1 == -1)
	{
        LiefBzg.sqlin ((short *) &we_kopf.mdn, 1, 0);
        LiefBzg.sqlout ((short *) &mdn_gr, 1, 0);
        cursor1 = LiefBzg.sqlcursor ("select mdn_gr from mdn where mdn = ?");

        LiefBzg.sqlin ((short *) &we_kopf.mdn, 1, 0);
        LiefBzg.sqlin ((short *) &we_kopf.fil, 1, 0);
        LiefBzg.sqlout ((short *) &fil_gr, 1, 0);
        cursor2 = LiefBzg.sqlcursor ("select fil_gr from fil where mdn = ? and fil = ?");
	}

//	sysdate (datum);  
    dlong_to_asc (we_kopf.we_dat,datum);  //300507 Datum aus we_dat , sonst aFehler beim nachtr�glichen Erfassen
    mdn_gr = fil_gr = 0;

    if (we_kopf.mdn > 0)
    {
		LiefBzg.sqlopen (cursor1);
		LiefBzg.sqlfetch (cursor1);
    }
    if (we_kopf.mdn > 0 && we_kopf.fil > 0)
    {
		LiefBzg.sqlopen (cursor2);
		LiefBzg.sqlfetch (cursor2);
    }

    sa = 0;
    pr_fil_ek = pr_vk = 0.0;
    fil_sa = AbPreise.fetch_preis_tag (mdn_gr, we_kopf.mdn, fil_gr, we_kopf.fil, _a_bas.a,
                                   datum, &pr_fil_ek, &pr_vk);                                                                  
    return 0;
}
int WeWork::Schreibea_pr (void)
{
	char command [60];
	AbPreise.schreibe_a_pr_vk(-1, we_kopf.mdn,-1,we_kopf.fil,_a_bas.a,ratod(Lsts->pr_vk));
	sprintf (command, "B %.0lf -1 %hd -1 %hd",_a_bas.a,we_kopf.mdn,we_kopf.fil);
	bild160 (command);
	return (0);
}
int WeWork::CalcEkBto (void) 
{

    pr_ek_bto0 = ratod (Lsts->pr_ek);
    pr_ek0  = pr_ek_bto0 - (double) WePreis.GetRabEk 
                                    (we_kopf.mdn, we_kopf.fil, 
                                     we_kopf.lief, 
		                           1, pr_ek_bto0, _a_bas.me_einh,_a_bas.a,_a_bas.ag,_a_bas.wg);
    sprintf (Lsts->netto_ek, "%.4lf", pr_ek0);
	sprintf (Lsts->pos_wert,  "%.4lf", pr_ek0 * ratod (Lsts->me));
    return 0;
}

int WeWork::ReadPr (void)
/**
Artikelpreis holen.
**/
{
       char lieferdat [12];
       char we_dat [12];
       int dsqlstatus;
       
       sysdate (lieferdat);
	   dlong_to_asc (we_kopf.we_dat, we_dat);

	   sa = 0;
/*
       dsqlstatus = WePreis.preis_holen ( we_kopf.mdn,
                                          we_kopf.fil,
                                          we_kopf.lief,
                                          _a_bas.a);
*/
       dsqlstatus = WePreis.preis_holen ( we_kopf.mdn,
                                          we_kopf.fil,
                                          we_kopf.lief,
										  we_dat,
                                          _a_bas.a);
	   pr_ek_bto0 = lief_bzg.pr_ek; 
	   pr_ek_bto0_euro = lief_bzg.pr_ek_eur; 
	   if (lief_bzg.pr_ek > 0.0)
	   {
              pr_ek0  = pr_ek_bto0 - (double) WePreis.GetRabEk 
				                                      (we_kopf.mdn, we_kopf.fil, 
			                                           we_kopf.lief, 
	  						                           1, pr_ek_bto0, _a_bas.me_einh,_a_bas.a,_a_bas.ag,_a_bas.wg);
	   }
	   else
	   {
			  pr_ek0 = (double) 0.0;
	   }
/*
	   if (lief_bzg.pr_ek_eur > 0.0)
	   {
              pr_ek0_euro  = pr_ek_bto0_euro - (double) WePreis.GetRabEk 
				                                      (we_pos.mdn, we_pos.fil, 
			                                           we_kopf.lief, 
	  						                           1, pr_ek_bto0_euro, _a_bas.me_einh);
	   }
*/
	   if (lief_bzg.pr_ek_eur > 0.0)
	   {
              pr_ek0_euro  = pr_ek_bto0_euro - (double) WePreis.GetRabEk 
				                                      (we_jour.mdn, we_jour.fil, 
			                                           we_kopf.lief, 
	  						                           1, pr_ek_bto0_euro, _a_bas.me_einh,_a_bas.a,_a_bas.ag,_a_bas.wg);
	   }
	   else
	   {
			  pr_ek0_euro = (double) 0.0;
	   }
       return 0;
}

int WeWork::ReadEan (double ean)
{
	   static int cursor = -1;
       static double a;
       int dsqlstatus;

	   if (cursor == -1)
	   {
			LiefBzg.sqlin  ((double *) &ean, 3, 0);
			LiefBzg.sqlout ((double *) &a, 3, 0);
			cursor = LiefBzg.sqlcursor ("select a from a_ean where ean = ?");
	   }

	   LiefBzg.sqlopen (cursor);
       dsqlstatus = LiefBzg.sqlfetch (cursor);
       if (dsqlstatus == 0)
       {
           sprintf (Lsts->a, "%.0lf", a);
       }
       return dsqlstatus;
}

int WeWork::ReadEmb (double emb)
{
       double a;
       short anz_emb;
       short anz;
       int dsqlstatus;
       int cursor;

       anz = 1;
       LiefBzg.sqlin  ((double *) &emb, 3, 0);
       LiefBzg.sqlout ((double *) &a, 3, 0);
       LiefBzg.sqlout ((short *)  &anz_emb, 1, 0);
       cursor = LiefBzg.sqlcursor ("select unt_emb, anz_emb from a_emb where emb = ?");
       dsqlstatus = LiefBzg.sqlfetch (cursor);
       if (dsqlstatus == 100)
       {
           LiefBzg.sqlclose (cursor);
           return dsqlstatus;
       }
       while (dsqlstatus == 0)
       {
           anz *= anz_emb;
           emb = a;
           LiefBzg.sqlopen (cursor);
           dsqlstatus = LiefBzg.sqlfetch (cursor);
       }
       sprintf (Lsts->a, "%.0lf", emb);
       Lsts->anz_emb = anz;
	   Lsts->Emb = TRUE;
       LiefBzg.sqlclose (cursor);
       return 0;
}



int WeWork::CreateLiefBzg (void)
{
       LiefBzgDlg *Dlg;

       lief_bzg.mdn = we_kopf.mdn;
       lief_bzg.fil = we_kopf.fil;
       strcpy (lief_bzg.lief, we_kopf.lief);
       Dlg = new LiefBzgDlg (-1, -1, 89, 16, "Lieferanten-Bezugsquellen",105, FALSE,
                             RAISEDBORDER, ENTERADATA);
//       Dlg = new LiefBzgDlg (-1, -1, 89, 25, "Lieferanten-Bezugsquellen", 116, FALSE,
//                             HIGHCOLBORDER, ENTERHEAD);
       Dlg->SetStyle (WS_VISIBLE | WS_POPUP | WS_THICKFRAME | WS_CAPTION | WS_SYSMENU);
       Dlg->SetStyleEx (NULL);
       Dlg->SetWinBackground (LTGRAYCOL);
       EnableWindow (Dlg->GethMainWindow (), FALSE);
	   if (ListDlg != NULL)
	   {
              EnableWindows (ListDlg->GeteListe ()->Getmamain3 (), FALSE);
	   }
       Dlg->SetNextTopWindow (Dlg->GethWnd ());
//       Dlg->SetWinBackground (RGB (0,128,192));
       if (this->Dlg != NULL)
       {
           Dlg->OpenScrollWindow (DLG::hInstance, this->Dlg->GethMainWindow ());
       }
       else
       {
              Dlg->OpenScrollWindow (DLG::hInstance, NULL);
       }
	   Dlg->ProcessMessages ();
       EnableWindow (Dlg->GethMainWindow (), TRUE);
	   if (ListDlg != NULL)
	   {
             EnableWindows (ListDlg->GeteListe ()->Getmamain3 (), TRUE);
	   }
       delete Dlg;
       if (syskey == KEY5)
       {
                 return -1;
       }
       return 0; 
}

BOOL WeWork::EnterLiefBzgA (double a)
{
	//sollte so nicht mehr aufgerufer werden 
	   if (a == 0.0)
	   {
		   return FALSE;
	   }
	   lief_bzg.a = a;
       if (CreateLiefBzg () != -1)
	   {
	    ReadLief_besta (a);
		   /*  ToDO
           memcpy (&bestptab[eListe.GetAktRow ()], &bestps, 
 							      sizeof (struct AUFPS));
           eListe.ShowAktRow ();
		   */
	   }
	   return TRUE;
}

BOOL WeWork::EnterLiefBzgA (double a, char* lief_best)
{
	   if (a == 0.0)
	   {
		   return FALSE;
	   }
	   lief_bzg.a = a;
	   strcpy (lief_bzg.lief_best,lief_best);
       if (CreateLiefBzg () != -1)
	   {
	    ReadLief_besta (a);
		   /*  ToDO
           memcpy (&bestptab[eListe.GetAktRow ()], &bestps, 
 							      sizeof (struct AUFPS));
           eListe.ShowAktRow ();
		   */
	   }
	   return TRUE;
}

/*
int WeWork::TestLief_bzg (void)

//Test, ob fuer den Lieferanten ein Eintrag in lief_bzg existiert.

{

	   int dsqlstatus;
       LIEF_BZG_CLASS lief_bzg_class;
	char ca [30];

       lief_bzg.mdn = we_kopf.mdn;
       lief_bzg.fil = we_kopf.fil;
       strcpy (lief_bzg.lief, we_kopf.lief);
       lief_bzg.a = ratod (Lsts->a);
       dsqlstatus = lief_bzg_class.dbreadfirst ();
       while (dsqlstatus == 100)
       {
                  if (lief_bzg.fil > 0)
                  {
                      lief_bzg.fil = 0;
                  }
                  else if (lief_bzg.mdn > 0)
                  {
                      lief_bzg.mdn = 0;
                  }
                  else
                  {
                      break;
                  }
                  dsqlstatus = lief_bzg_class.dbreadfirst ();
       }
       if (dsqlstatus == 0) return 0; 
	   strcpy (ca,Lsts->a);    //ToDO Nur eine Kr�cke , Nach der MessageBox verliert bestps die akt. Werte !!!??
       if (abfragejn (NULL,
                       "Der Artikel ist nicht im Sortiment des Lieferanten\n"
                       "Artikel anlegen ?", "J"))
       {
                       if (CreateLiefBzg () == 0)
                       {
						   strcpy (Lsts->a,ca);    //ToDO Nur eine Kr�cke , Nach der MessageBox verliert bestps die akt. Werte !!!??
                           return TestLief_bzg ();
                       }
                       return -1;
       }
       return -1;
}
*/
int WeWork::TestLief_bzg (void)
{
     lief_bzg.a = ratod (Lsts->a);
	 strcpy(lief_bzg.lief_best,Lsts->lief_best);
     return TestLief_bzg (lief_bzg.a,lief_bzg.lief_best);
}

int WeWork::TestLief_bzg (double a, char *lief_best)
/**
Test, ob fuer den Lieferanten ein Eintrag in lief_bzg existiert.
**/
{
	char ca [30];
//	char cpos [20];
	char clief_best [17];

	   int dsqlstatus;
       LIEF_BZG_CLASS lief_bzg_class;

       lief_bzg.mdn = we_kopf.mdn;
       lief_bzg.fil = we_kopf.fil;
       strcpy (lief_bzg.lief, we_kopf.lief);
       strcpy (lief_bzg.lief_best, lief_best);
       lief_bzg.a = a;
       dsqlstatus = lief_bzg_class.dbreadfirst ();
       while (dsqlstatus == 100)
       {
                  if (lief_bzg.fil > 0)
                  {
                      lief_bzg.fil = 0;
                  }
                  else if (lief_bzg.mdn > 0)
                  {
                      lief_bzg.mdn = 0;
                  }
                  else
                  {
                      break;
                  }
                  dsqlstatus = lief_bzg_class.dbreadfirst ();
       }
       if (dsqlstatus == 0) return 0; 

//erst mal generell zulassen                   if (we_neu_a_par == 0)
//                   {
//                       disp_mess ("Der Artikel ist nicht im Sortiment des Lieferanten.", 2);
//                       return -1;
//                   }

				   strcpy (ca,Lsts->a);    //ToDO Nur eine Kr�cke , Nach der MessageBox verliert bestps die akt. Werte !!!??
//				   strcpy (cpos,Lsts->pos);
				   strcpy (clief_best,Lsts->lief_best);
                   if (abfragejn (NULL,
                       "Der Artikel ist nicht im Sortiment des Lieferanten\n"
                       "Artikel anlegen ?", "J"))
                   {

                       if (CreateLiefBzg () == 0)
                       {
						   strcpy (Lsts->a,ca);    //ToDO Nur eine Kr�cke , Nach der MessageBox verliert bestps die akt. Werte !!!??
//						   strcpy (Lsts->posi,cpos);
						   strcpy (Lsts->lief_best,lief_bzg.lief_best);
                           return TestLief_bzg (lief_bzg.a,Lsts->lief_best);
                       }
                       return -1;
                   }
       return -1;
}

int WeWork::ReadA (double a)
{
	char ca [14];
//sollte so nicht mehr aufgerufen werden !!
	sprintf(ca,"%13.0f",a);
	return ReadA (a,Lsts->lief_best);
}

int WeWork::ReadA (double a,char* lief_best) 
{ 
    int dsqlstatus;
	static int cursor1 = -1;
	static int cursor2 = -1;
	static int cursor3 = -1;
	static int cursor4 = -1;
    static long a_krz;
    static double tara;
    BOOL TypOK = FALSE;

	if (cursor1 == -1)
	{
        LiefBzg.sqlin ((double *) &_a_bas.a, 3, 0);
        LiefBzg.sqlout ((long *) &a_krz, 2, 0);
        cursor1 = LiefBzg.sqlcursor ("select a_krz from a_krz where a = ?");

        LiefBzg.sqlin ((double *) &_a_bas.a, 3, 0);
        LiefBzg.sqlout ((double *) &tara, 3, 0);
        cursor2 = LiefBzg.sqlcursor ("select tara from a_hndw where a = ?");

        LiefBzg.sqlin ((double *) &_a_bas.a, 3, 0);
        LiefBzg.sqlout ((double *) &tara, 3, 0);
        cursor3  = LiefBzg.sqlcursor ("select tara from a_eig where a = ?");

        LiefBzg.sqlin ((double *) &_a_bas.a, 3, 0);
        LiefBzg.sqlout ((double *) &tara, 3, 0);
        cursor4 = LiefBzg.sqlcursor ("select tara from a_eig_div where a = ?");

	}

    Einh.SetBestEinh (1);
    Lsts->anz_emb = 1;
    Lsts->Emb = FALSE;
    dsqlstatus = lese_a_bas (a);
    if (dsqlstatus == 100)
    {
             dsqlstatus = ReadEan (a);
             a = ratod (Lsts->a);
             dsqlstatus = lese_a_bas (a);
    }
    
    if (dsqlstatus == 100)
    {
             dsqlstatus = ReadEmb (a);
             a = ratod (Lsts->a);
             dsqlstatus = lese_a_bas (a);
    }

    if (dsqlstatus == 100)
    {
             return dsqlstatus;
    }

    if (atoi (Lsts->a_krz) == 0)
    {
		LiefBzg.sqlopen (cursor1);
		if (LiefBzg.sqlfetch (cursor1) == 0)
        {
            sprintf (Lsts->a_krz, "%ld", a_krz);
        }
    }

    tara = 0.0;
    switch (_a_bas.a_typ)
    {
       case 1 :
		   LiefBzg.sqlopen (cursor2);
		   dsqlstatus = LiefBzg.sqlfetch (cursor2);
           TypOK = TRUE;
           break;
       case 2 :
		   LiefBzg.sqlopen (cursor3);
		   dsqlstatus = LiefBzg.sqlfetch (cursor3);
           TypOK = TRUE;
           break;
       case 3 :
		   LiefBzg.sqlopen (cursor4);
		   dsqlstatus = LiefBzg.sqlfetch (cursor4);
           TypOK = TRUE;
           break;
       case 5 :
       case 6 :
       case 8 :
       case 9 :
       case 10 :
       case 11 :
           TypOK = TRUE;
           break;
    }

    if (TypOK == FALSE)
    {
       switch (_a_bas.a_typ2)
       {
         case 1 :
			 LiefBzg.sqlopen (cursor2);
			 dsqlstatus = LiefBzg.sqlfetch (cursor2);
             TypOK = TRUE;
             break;
         case 2 :
			 LiefBzg.sqlopen (cursor3);
			 dsqlstatus = LiefBzg.sqlfetch (cursor3);
             TypOK = TRUE;
             break;
         case 3 :
			 LiefBzg.sqlopen (cursor4);
			 dsqlstatus = LiefBzg.sqlfetch (cursor4);
             TypOK = TRUE;
             break;
         case 5 :
         case 6 :
         case 8 :
         case 9 :
         case 10 :
         case 11 :
             TypOK = TRUE;
             break;
       }
    }

    if (!TypOK)
    {
        return -2;
    }
    sprintf (Lsts->tara, "%.3lf", tara);
    sprintf (Lsts->inh, "%.3lf", (double) 1.0);

 	if (TestLief_bzg () == -1)
	{
		   return -1;
	}

	if (lief_bzg.min_best == 0)
	{
		lief_bzg.min_best = 1;
	}
    ReadPr ();
    ReadLief_besta (a,lief_best);
    Reada_pr ();
	strcpy (Lsts->me_kz, lief_bzg.me_kz);
	if (atoi (lief_bzg.me_kz) == 1)
	{
		      Einh.SetBestEinh (0);
	}
	else
	{
  	          Einh.SetBestEinh (1);
              sprintf (Lsts->inh, "%.3lf", lief_bzg.min_best);
	}
    GetMeEinh (we_kopf.mdn, we_kopf.fil, we_kopf.lief, a, lief_best);
    me_einh = ratod (Lsts->me_einh);
    return 0;
}

int WeWork::ReadLief_besta (double a)
{
    int dsqlstatus;
    short mdn;
    short fil;

    mdn = lief_bzg.mdn;
    fil = lief_bzg.fil;

    lief_bzg.a = a;
    dsqlstatus = LiefBzg.dbreadfirst ();
    while (dsqlstatus == 100)
    {
        if (lief_bzg.fil > 0)
        {
            lief_bzg.fil = 0;
        }
        else if (lief_bzg.mdn > 0)
        {
            lief_bzg.mdn = 0;
        }
        else
        {
            break;
        }
        dsqlstatus = LiefBzg.dbreadfirst ();
    }
    lief_bzg.mdn = mdn;
    lief_bzg.fil = fil;
    return dsqlstatus;
}
int WeWork::ReadLief_besta (double a, char* lief_best)
{
    int dsqlstatus;
    short mdn;
    short fil;

    mdn = lief_bzg.mdn;
    fil = lief_bzg.fil;

    lief_bzg.a = a;
	strcpy(lief_bzg.lief_best,lief_best);
    dsqlstatus = LiefBzg.dbreadfirst ();
    while (dsqlstatus == 100)
    {
        if (lief_bzg.fil > 0)
        {
            lief_bzg.fil = 0;
        }
        else if (lief_bzg.mdn > 0)
        {
            lief_bzg.mdn = 0;
        }
        else
        {
            break;
        }
        dsqlstatus = LiefBzg.dbreadfirst ();
    }
    lief_bzg.mdn = mdn;
    lief_bzg.fil = fil;
    return dsqlstatus;
}
     
int WeWork::ReadLief_best (char *lief_best)
{
    int dsqlstatus;
    int cursor;
    short mdn;
    short fil;

    mdn = lief_bzg.mdn;
    fil = lief_bzg.fil;

    LiefBzg.sqlin ((short *) &mdn, 1, 0);
    LiefBzg.sqlin ((short *) &fil, 1, 0);
    LiefBzg.sqlin ((char *)  lief_bzg.lief, 0, 17);
    LiefBzg.sqlin ((double *)  lief_best, 0, 17);
    LiefBzg.sqlout ((double *)  &lief_bzg.a, 3, 0);
    cursor = LiefBzg.sqlcursor ("select a from lief_bzg "
                                        "where mdn = ? "
                                        "and fil = ? "
                                        "and lief = ? "
                                        "and lief_best = ?");
    dsqlstatus = LiefBzg.sqlfetch (cursor);
    while (dsqlstatus == 100)
    {
        if (fil > 0)
        {
            fil = 0;
        }
        else if (mdn > 0)
        {
            mdn = 0;
        }
        else
        {
            break;
        }
        LiefBzg.sqlopen (cursor);
        dsqlstatus = LiefBzg.sqlfetch (cursor);
    }
    LiefBzg.sqlclose (cursor);
    return dsqlstatus;
}
     
int WeWork::ReadA_krz (long a_krz)
{
    int dsqlstatus;

    LiefBzg.sqlin ((long *) &a_krz, 2, 0);
    LiefBzg.sqlout ((double *)  &lief_bzg.a, 3, 0);
    dsqlstatus = LiefBzg.sqlcomm ("select a from a_krz where a_krz = ?");
    return dsqlstatus;
}

int WeWork::GetPtab (char *dest, char *item, char *wert)
{
    int dsqlstatus;

    dest[0] = 0;
    dsqlstatus = Ptab.lese_ptab (item, wert);  
    if (dsqlstatus == 100)
    {
        return -1;
    }
    strcpy (dest, ptabn.ptbezk);
    return 0;
}

int WeWork::ShowPtab (char *dest, char *item)
{
    int dsqlstatus;

    dsqlstatus = Ptab.Show (item);  
    if (syskey == KEY5)
    {
        return -1;
    }
    strcpy (dest, ptabn.ptwert);
    return 0;
}

int WeWork::ShowPtabEx (HWND hMainWindow, char *dest, char *item)
{
       char wert [80];
       SEARCHPTAB *SearchPtab;
	   HINSTANCE hMainInst;


       hMainInst = (HINSTANCE) GetWindowLong (hMainWindow, GWL_HINSTANCE);
       SearchPtab = new SEARCHPTAB (item);
       SearchPtab->SetParams (hMainInst, hMainWindow); 
       SearchPtab->Setawin (hMainWindow);
                                                  
       SearchPtab->Search ();
       if (SearchPtab->GetKey (wert) == TRUE)
       {
           delete SearchPtab;
           strcpy (dest, wert);
		   return 0;
       }
       delete SearchPtab;
       return -1;
}


int WeWork::GetMdnName (char *dest, short mdn)
{
    int dsqlstatus;

    if (mdn == 0)
    {
        strcpy (dest, "Zentrale");
        return 0;
    }
    dest[0] = 0;
    LiefBzg.sqlin ((short *) &mdn, 1, 0);
    LiefBzg.sqlout ((char *) dest, 0, 17);
    dsqlstatus =  LiefBzg.sqlcomm ("select adr.adr_krz from mdn,adr "
                                   "where mdn.mdn = ? " 
                                   "and adr.adr = mdn.adr");
    if (dsqlstatus == 100)
    {
        return 100;
    }
    return Mdn.lese_mdn (mdn);
}


int WeWork::GetFilName (char *dest, short mdn, short fil)
{

    if (mdn == 0 || fil == 0)
    {
        strcpy (dest, "Mandant");
        return 0;
    }
    dest[0] = 0;
    LiefBzg.sqlin ((short *) &mdn, 1, 0);
    LiefBzg.sqlin ((short *) &fil, 1, 0);
    LiefBzg.sqlout ((char *) dest, 0, 17);
    return LiefBzg.sqlcomm ("select adr.adr_krz from fil,adr "
                             "where fil.mdn = ? " 
                             "and   fil.fil = ? " 
                             "and adr.adr = fil.adr");
}

int WeWork::GetLiefName (char *dest, short mdn, char *lief_nr)
{
    int dsqlstatus;
    int cursor;

    dest[0] = 0;


    LiefBzg.sqlin ((short *) &mdn, 1, 0);
    LiefBzg.sqlin ((char *)  lief_nr, 0, 17);
    LiefBzg.sqlout ((char *) dest, 0, 17);

    cursor = LiefBzg.sqlcursor ("select adr.adr_krz from lief,adr "
                                "where lief.mdn = ? " 
                                "and   lief.lief = ? "
                                "and adr.adr = lief.adr");
    dsqlstatus = LiefBzg.sqlfetch (cursor);
    while (dsqlstatus == 100)
    {
        if (mdn > 0)
        {
            mdn = 0;
        }
        else
        {
            break;
        }
        dsqlstatus = LiefBzg.sqlopen (cursor);
        dsqlstatus = LiefBzg.sqlfetch (cursor);
    }
    LiefBzg.sqlclose (cursor);
    if (dsqlstatus == 0)
    {
        lief.mdn = mdn;
        strcpy (lief.lief, lief_nr);
        Lief.dbreadfirst ();
        we_kopf.lief_adr = lief.adr;
    }
    return dsqlstatus;
}


void WeWork::FillRow (char *buffer)
{
    sprintf (buffer, "%13.0lf  |%-24s|  |%-16s|  %8.4lf   |%-24s|",
                     lief_bzg.a, _a_bas.a_bz1, lief_bzg.lief_best,
                     lief_bzg.pr_ek, lief_bzg.best_txt1);
}

void WeWork::BeginWork (void)
{
    ::beginwork ();
}

void WeWork::CommitWork (void)
{
    ::commitwork ();
}

void WeWork::RollbackWork (void)
{
    ::rollbackwork ();
}

void WeWork::InitRec (void)
{
    memcpy (&we_kopf, &we_kopf_null, sizeof (struct WE_KOPF));
}


double WeWork::GetMeVgl (double a,char *lief_best, double me, short me_einh_lief)
/**
Mengeneinheit fuer Lieferant und Artikel holen.
**/
{
         KEINHEIT keinheit;
         double me_vgl;

         Einh.AktBestEinh (we_kopf.mdn, we_kopf.fil,
                           we_kopf.lief, a,lief_best, me_einh_lief);
         Einh.GetLiefEinh (we_kopf.mdn, we_kopf.fil,
                           we_kopf.lief, a, lief_best, &keinheit);
         if (keinheit.me_einh_lief == keinheit.me_einh_bas)
         {
                      me_vgl = me;
         }
         else
         {
                     if (keinheit.inh <= (double) 0.0)
                     {
                                   keinheit.inh = (double) 1.0;
                     }
                     me_vgl = me * keinheit.inh;
         }
         return me_vgl;
}


BOOL WeWork::GetMeEinh (short mdn, short fil, char *lief_nr, double a, char *lief_best)
{

        KEINHEIT keinheit;

        memcpy (&keinheit, &keinheit_null, sizeof (KEINHEIT));


        Einh.GetLiefEinh (mdn, fil, lief_nr,
                                a,lief_best, &keinheit);
		WorkMeEinh = keinheit.me_einh_lief;
        if (Lsts == NULL) return FALSE;
        strcpy  (Lsts->me_einh_bez, keinheit.me_einh_lief_bez);
        sprintf (Lsts->me_einh,     "%hd",   keinheit.me_einh_lief);
		if (keinheit.inh == 0)
		{
			keinheit.inh = 1;
		}
        Lsts->inh_einh = keinheit.inh;
        inh = keinheit.inh;
        pr_ek_einh = keinheit.pr_ek;
        return TRUE;
}


BOOL WeWork::GetMeEinhBest (short mdn, short fil, char *lief_nr, double a, char *lief_best)
{

        KEINHEIT keinheit;

        memcpy (&keinheit, &keinheit_null, sizeof (KEINHEIT));


        Einh.GetLiefEinh (mdn, fil, lief_nr,
                                a, lief_best, &keinheit);
        if (Lsts == NULL) return FALSE;
        strcpy  (Lsts->best_me_einh_bez, keinheit.me_einh_lief_bez);
        sprintf (Lsts->best_me_einh,     "%hd",   keinheit.me_einh_lief);
        return TRUE;
}

double WeWork::GetMaxInh (short mdn, short fil, char *lief_nr, double a) //inhalt gesamter Palette in Grundeinheit
{
    KEINHEIT keinheit;
    memcpy (&keinheit, &keinheit_null, sizeof (KEINHEIT));

	liefmebest.mdn = mdn;
	liefmebest.fil = fil;
	strcpy(liefmebest.lief,lief_nr); ;
	liefmebest.a = a;
	Einh.GetMaxInh (mdn,fil,lief_nr,a, &keinheit);
	return keinheit.inh;

}

int WeWork::ChMeEinh (HWND hWnd)
/**
Auswahl Auftragsmengeneinheit.
**/
{
       int me_einh; 

       me_einh = atoi (Lsts->me_einh);
       liefmebest.mdn = we_kopf.mdn;
       liefmebest.fil = we_kopf.fil;
       strcpy (liefmebest.lief, we_kopf.lief);
       liefmebest.a = ratod (Lsts->a);

       if (ratod (Lsts->a))
       {
           Einh.AktBestEinh (we_kopf.mdn, we_kopf.fil, we_kopf.lief, 
                                   ratod (Lsts->a),Lsts->lief_best, atoi (Lsts->me_einh));
       }
       else
       {
           Einh.SetBestEinh (1);
       }
//       EnableWindow (mamain1, FALSE);
       if (hWnd == NULL && Dlg != NULL)
       {
            Einh.ChoiseEinhEx (Dlg->GethMainWindow ());
       }
       else 
       {
            Einh.ChoiseEinhEx (hWnd);
       }
//       EnableWindow (mamain1, TRUE);
       _a_bas.a = ratod (Lsts->a);
       if (syskey == KEY5) 
	   {
		   return 0;
	   }

       GetMeEinh (we_kopf.mdn, we_kopf.fil, we_kopf.lief, _a_bas.a, Lsts->lief_best);
       if (atoi (Lsts->me_einh) != me_einh)
       {
           {
             sprintf (Lsts->pr_ek, "%.4lf", (double) ratod (Lsts->pr_ek) / 
                                                     ratod (Lsts->inh));
             sprintf (Lsts->pr_ek, "%.4lf", (double) ratod (Lsts->pr_ek) * 
                                                     Lsts->inh_einh);

             if (pr_ek_einh != 0.0)
             {
//                  sprintf (Lsts->pr_ek, "%.4lf", pr_ek_einh);
             }

             CalcEkBto ();
           }
           sprintf (Lsts->inh, "%.3lf", Lsts->inh_einh);
       }
       return 0;
}

int WeWork::ShowLager (HWND hWnd, double a)
/**
Auswahl Auftragsmengeneinheit.
**/
{
       int me_einh; 

       me_einh = atoi (Lsts->me_einh);
       liefmebest.mdn = we_kopf.mdn;
       liefmebest.fil = we_kopf.fil;
       strcpy (liefmebest.lief, we_kopf.lief);
       liefmebest.a = ratod (Lsts->a);

       if (ratod (Lsts->a))
       {
           Einh.AktBestEinh (we_kopf.mdn, we_kopf.fil, we_kopf.lief, 
                                   ratod (Lsts->a),Lsts->lief_best, atoi (Lsts->me_einh));
       }
       else
       {
           Einh.SetBestEinh (1);
       }
//       EnableWindow (mamain1, FALSE);
       if (hWnd == NULL && Dlg != NULL)
       {
            Einh.ChoiseLagerEx (Dlg->GethMainWindow ());
       }
       else 
       {
            Einh.ChoiseEinhEx (hWnd);
       }
//       EnableWindow (mamain1, TRUE);
       _a_bas.a = ratod (Lsts->a);
       if (syskey == KEY5) 
	   {
		   return 0;
	   }

       GetMeEinh (we_kopf.mdn, we_kopf.fil, we_kopf.lief, _a_bas.a, Lsts->lief_best);
       if (atoi (Lsts->me_einh) != me_einh)
       {
           {
             sprintf (Lsts->pr_ek, "%.4lf", (double) ratod (Lsts->pr_ek) / 
                                                     ratod (Lsts->inh));
             sprintf (Lsts->pr_ek, "%.4lf", (double) ratod (Lsts->pr_ek) * 
                                                     Lsts->inh_einh);

             if (pr_ek_einh != 0.0)
             {
//                  sprintf (Lsts->pr_ek, "%.4lf", pr_ek_einh);
             }

             CalcEkBto ();
           }
           sprintf (Lsts->inh, "%.3lf", Lsts->inh_einh);
       }
       return 0;
}

/*
BOOL WeWork::PosExist (void)
{
        int dsqlstatus;

        LiefBzg.sqlin ((short *) &we_kopf.mdn, 1, 0);
        LiefBzg.sqlin ((short *) &we_kopf.fil, 1, 0);
        LiefBzg.sqlin ((char *)  we_kopf.lief, 0, 17);
        LiefBzg.sqlin ((char *)  we_kopf.lief_rech_nr, 0, 17);
        LiefBzg.sqlin ((char *)  we_kopf.blg_typ, 0, 2);
        dsqlstatus = LiefBzg.sqlcomm ("select lief from we_pos where mdn = ? "
                                            "and   fil = ? " 
                                            "and   lief = ? " 
                                            "and   lief_rech_nr = ? " 
                                            "and   blg_typ = ?");
        return dsqlstatus == 0 ? TRUE : FALSE;
}
*/

BOOL WeWork::PosExist (void)
{
        int dsqlstatus;

        LiefBzg.sqlin ((short *) &we_kopf.mdn, 1, 0);
        LiefBzg.sqlin ((short *) &we_kopf.fil, 1, 0);
        LiefBzg.sqlin ((char *)  we_kopf.lief, 0, 17);
        LiefBzg.sqlin ((char *)  we_kopf.lief_rech_nr, 0, 17);
        LiefBzg.sqlin ((char *)  we_kopf.blg_typ, 0, 2);
        dsqlstatus = LiefBzg.sqlcomm ("select lief from we_jour where mdn = ? "
                                            "and   fil = ? " 
                                            "and   lief = ? " 
                                            "and   lief_rech_nr = ? " 
                                            "and   blg_typ = ?");
        return dsqlstatus == 0 ? TRUE : FALSE;
}

/*    
void WeWork::DeleteAllPos (short mdn, short fil, char *lief, char * lief_rech_nr, char *blg_typ)
{
        LiefBzg.sqlin ((short *) &mdn, 1, 0);
        LiefBzg.sqlin ((short *) &fil, 1, 0);
        LiefBzg.sqlin ((char *)  lief, 0, 17);
        LiefBzg.sqlin ((char *)  lief_rech_nr, 0, 17);
        LiefBzg.sqlin ((char *)  blg_typ, 0, 2);
        LiefBzg.sqlcomm ("delete from we_pos where mdn = ? "
                                            "and   fil = ? " 
                                            "and   lief = ? " 
                                            "and   lief_rech_nr = ? " 
                                            "and   blg_typ = ?");
}
*/

void WeWork::DeleteAllPos (short mdn, short fil, char *lief, char * lief_rech_nr, char *blg_typ)
{
        LiefBzg.sqlin ((short *) &mdn, 1, 0);
        LiefBzg.sqlin ((short *) &fil, 1, 0);
        LiefBzg.sqlin ((char *)  lief, 0, 17);
        LiefBzg.sqlin ((char *)  lief_rech_nr, 0, 17);
        LiefBzg.sqlin ((char *)  blg_typ, 0, 2);
        LiefBzg.sqlcomm ("delete from we_jour where mdn = ? "
                                            "and   fil = ? " 
                                            "and   lief = ? " 
                                            "and   lief_rech_nr = ? " 
                                            "and   blg_typ = ?");
}

void WeWork::DeleteWe ()
{
	DeleteAllPos (we_kopf.mdn,  we_kopf.fil, we_kopf.lief, we_kopf.lief_rech_nr, we_kopf.blg_typ);
	WeKopf.dbdelete ();
}


char *WeWork::GenCharge (HWND hMainWindow, char *ident, char *prog)
{
        static char Charge [21];
        static InfMacro *infMacro = NULL;
        static int lfd = 0;
        char buffer [1024];
        char we_dat [12];

        dlong_to_asc (we_kopf.we_dat, we_dat);
        strcpy (Charge, "  ");
        sprintf (buffer, "ident=%s;lief=%s;we_dat=%s", ident, we_kopf.lief, we_dat);
        SetDebug ();
        SetDebugMain (hMainWindow);
        if (infMacro == NULL)
        {
               infMacro = new InfMacro ("gencharge.mco");
               if (infMacro == NULL) return NULL;
        }

        if (infMacro->LoadMacro () == FALSE)
        {
            return NULL;
        }
        strcpy (buffer, "");
        if (ident!= NULL)
        {
            sprintf (buffer, "ident=%s;lief=%s;we_dat=%s", ident, we_kopf.lief, we_dat);
        }
        else
        {
            sprintf (buffer, "lief=%s;we_dat=%s", we_kopf.lief, we_dat);
        }
        if (prog == NULL)
        {
                infMacro->RunMacro (buffer, "charge", Charge);
        }
        else
        {
                infMacro->RunMacro (buffer, "charge", Charge, prog);
        }
        return Charge;
}


char *WeWork::GenLiefRechNr (HWND hMainWindow, char *lief_rech_nr,  char *prog)
{
//        static char lief_rech_nr [21];
        static InfMacro *infMacro = NULL;
        static int lfd = 0;
        char buffer [1024];

        SetDebug ();
        SetDebugMain (hMainWindow);
        if (infMacro == NULL)
        {
               infMacro = new InfMacro ("genliefrechnr.mco");
               if (infMacro == NULL) return NULL;
        }

        if (infMacro->LoadMacro () == FALSE)
        {
            return NULL;
        }
        strcpy (lief_rech_nr, "  ");
        sprintf (buffer, "best_blg=%ld;lief=%s", best_kopf.best_blg, we_kopf.lief);
        if (prog == NULL)
        {
                infMacro->RunMacro (buffer, "liefrechnr", lief_rech_nr);
        }
        else
        {
                infMacro->RunMacro (buffer, "liefrechnr", lief_rech_nr, prog);
        }
        return lief_rech_nr;
}

void WeWork::FillQsHead ()
/**
Texte fuer Qualitaetssicherung fuellen.
**/
{
    int dsqlstatus; 
    int cursor;
    char txt [25];
    int i;
    static BOOL OK = FALSE;
	short anz;

    if (OK) return;

    OK = TRUE;
    Texte = Texte0;
    Yes   = Yes0;
    No    = No0;
    TextePos = TextePos0;
    YesPos   = YesPos0;
    NoPos    = NoPos0;
    LiefBzg.sqlout ((short *) &anz, 1, 0);
    dsqlstatus = LiefBzg.sqlcomm ("select count (*) from qstxt where qstyp = 1");
    if (dsqlstatus != 0)
    {
        return;
    }

	if (anz == 0)
	{
		return;
	}

	qsanz = anz;

    Texte = new char* [qsanz + 2];
    Yes   = new int [qsanz + 2];
    No    = new int [qsanz + 2];

    for (i = 0; i < qsanz; i ++)
    {
        Yes[i] = No[i] = 0;
    }


    LiefBzg.sqlout ((char *) txt, 0, 25);
    cursor = LiefBzg.sqlcursor ("select txt,sort from qstxt where qstyp = 1 "
                                "order by sort");
    if (cursor < 0) 
    {
        return;
    }

    i = 0;
    while (LiefBzg.sqlfetch (cursor) == 0)
    {
        Texte [i] = new char [25];
        strcpy (Texte[i], txt);
        i ++;
    }

    Texte[i] = NULL;
    LiefBzg.sqlclose (cursor);
}

void WeWork::FillTexteDefaults (void)
{
    TextePos = TextePos0;
    YesPos   = YesPos0;
    NoPos    = NoPos0;
}

void WeWork::FillQsPos ()
/**
Texte fuer Qualitaetssicherung fuellen.
**/
{
    int dsqlstatus; 
    int cursor;
    char txt [25];
    int i;
    static BOOL OK = FALSE;
    short anz;
    short hwg;
    short wg;
    long ag;
    double a;


    if (TextePos != NULL && TextePos != TextePos0)
    {
        delete TextePos;
    }
    if (YesPos != NULL && YesPos != YesPos0)
    {
        delete YesPos;
    }
    if (NoPos != NULL && NoPos != NoPos0)
    {
        delete NoPos;
    }
    a = ratod (Lsts->a);
    LiefBzg.sqlin ((double *) &a, 3, 0);
    LiefBzg.sqlout ((short *) &hwg, 1, 0);
    LiefBzg.sqlout ((short *) &wg, 1, 0);
    LiefBzg.sqlout ((long *)  &ag, 2, 0);
    dsqlstatus = LiefBzg.sqlcomm ("select hwg, wg, ag from a_bas where a = ?");
    if (dsqlstatus == 100)
    {
        FillTexteDefaults ();
        return;
    }
    OK = TRUE;
    anz = 0;
    LiefBzg.sqlout ((short *) &anz, 1, 0);
    LiefBzg.sqlin  ((long *) &ag, 2, 0);
    dsqlstatus = LiefBzg.sqlcomm ("select count (*) from qstxt where qstyp = 2 and ag = ?");
    if (dsqlstatus != 0 || anz == 0)
    {
            ag = 0l;
            LiefBzg.sqlout ((short *) &anz, 1, 0);
            LiefBzg.sqlin  ((short *) &wg, 1, 0);
            dsqlstatus = LiefBzg.sqlcomm ("select count (*) from qstxt where qstyp = 2 "
                                          "and wg = ? and ag = 0;");
    }

    if (dsqlstatus != 0 || anz == 0)
    {
            wg = 0l;
            LiefBzg.sqlout ((short *) &anz, 1, 0);
            LiefBzg.sqlin  ((short *) &hwg, 1, 0);
            dsqlstatus = LiefBzg.sqlcomm ("select count (*) from qstxt where qstyp = 2 "
                                          "and hwg = ? and wg = 0 and ag = 0");
    }

    if (dsqlstatus != 0 || anz == 0)
    {
            hwg = 0l;
            LiefBzg.sqlout ((short *) &anz, 1, 0);
            dsqlstatus = LiefBzg.sqlcomm ("select count (*) from qstxt where qstyp = 2 "
                                          "and hwg = 0 and wg = 0 and ag = 0");
    }

    if (dsqlstatus != 0 || anz == 0)
    {
        FillTexteDefaults ();
        return;
    }

    qsanzpos = anz;

    TextePos = new char* [qsanzpos + 2];
    YesPos   = new int [qsanzpos + 2];
    NoPos    = new int [qsanzpos + 2];

    for (i = 0; i < qsanzpos; i ++)
    {
        YesPos[i] = NoPos[i] = 0;
    }

    if (ag > 0l)
    {
        LiefBzg.sqlout ((char *) txt, 0, 25);
        LiefBzg.sqlin  ((long *) &ag, 2, 0);
        cursor = LiefBzg.sqlcursor ("select txt,sort from qstxt where qstyp = 2 "
                                "and ag = ? " 
                                "order by sort");
    }
    else if (wg > 0)
    {
        LiefBzg.sqlout ((char *) txt, 0, 25);
        LiefBzg.sqlin  ((short *) &wg, 1, 0);
        cursor = LiefBzg.sqlcursor ("select txt,sort from qstxt where qstyp = 2 "
                                "and wg = ? and ag = 0 " 
                                "order by sort");
    }
    else if (hwg > 0)
    {
        LiefBzg.sqlout ((char *) txt, 0, 25);
        LiefBzg.sqlin  ((short *) &hwg, 1, 0);
        cursor = LiefBzg.sqlcursor ("select txt,sort from qstxt where qstyp = 2 "
                                "and hwg = ? and wg = 0 and ag = 0 " 
                                "order by sort");
    }
    else
    {
        LiefBzg.sqlout ((char *) txt, 0, 25);
        cursor = LiefBzg.sqlcursor ("select txt,sort from qstxt where qstyp = 2 "
                                "and hwg = 0 and wg = 0 and ag = 0 " 
                                "order by sort");
    }
    if (cursor < 0) 
    {
        FillTexteDefaults ();
        return;
    }

    i = 0;
    while (LiefBzg.sqlfetch (cursor) == 0)
    {
        TextePos [i] = new char [25];
        strcpy (TextePos[i], txt);
        i ++;
    }

    TextePos[i] = NULL;
    LiefBzg.sqlclose (cursor);
}


void WeWork::InitQm (int *Yes, int *No, int anz)
/**
Eingaben pruefen.
**/
{
	      int i;

		  for (i = 0; i < anz; i ++)
		  {
			  Yes[i] = 0;
			  No [i] = 0;
		  }
}

void WeWork::QmDefaultYes (int *Yes, int *No, int anz)
/**
Eingaben pruefen.
**/
{
	      int i;

		  for (i = 0; i < anz; i ++)
		  {
			  Yes[i] = 1;
			  No [i] = 0;
		  }
}

void WeWork::QmDefaultNo (int *Yes, int *No, int anz)
/**
Eingaben pruefen.
**/
{
	      int i;

		  for (i = 0; i < anz; i ++)
		  {
			  Yes[i] = 0;
			  No [i] = 1;
		  }
}


BOOL WeWork::TestQm (int *Yes, int *No, int anz)
/**
Eingaben pruefen.
**/
{
	      int i;

		  for (i = 0; i < anz; i ++)
		  {
			  if (Yes[i] == 0 &&
				  No [i] == 0)
			  {
				  return FALSE;
			  }
		  }
		  return TRUE;
}


void WeWork::WriteQs (void)
/**
Eingaben aus Qualit�tssicherung speichern.
**/
{
	     int i;
		 char datum [12];
		 
		 sysdate (datum);
		 qswekopf.mdn = we_kopf.mdn;
		 qswekopf.fil = we_kopf.fil;
		 strcpy (qswekopf.lief, we_kopf.lief);
		 strcpy (qswekopf.lief_rech_nr, we_kopf.lief_rech_nr);

         LiefBzg.sqlin ((short *) &qswekopf.mdn, 1, 0);
         LiefBzg.sqlin ((short *) &qswekopf.fil, 1, 0);
         LiefBzg.sqlin ((char *)  qswekopf.lief, 0, 17);
         LiefBzg.sqlin ((short *) qswekopf.lief_rech_nr, 0, 17);
         LiefBzg.sqlcomm ("delete from qswekopf where mdn = ? "
                                               "and   fil = ? "
                                               "and   lief = ? "
                                               "and   lief_rech_nr = ?");
		 qswekopf.dat = dasc_to_long (datum);
         qswekopf.lfd = (int) 1;
	     QsWeKopf.dbreadfirst ();
		 strcpy (qswekopf.pers_nam, sys_ben.pers_nam);
		 //250210 qswetext jetzt mit 1:1 beziehung zu we_kopf , wenn Anzahl Positionen < 11 
		 //       so ist beim Belegdruck keine Vorselektion n�tig !!
		 for (i = 0; i < qsanz; i ++)
		 {

			 if (i == 0) 
			 {
				strcpy (qswekopf.txt, Texte[i]);
				qswekopf.wrt = Yes[i];
				QsWeKopf.dbupdate ();
			 }
			 if (i == 1) 
			 {
				strcpy (qswekopf.txt2, Texte[i]);
				qswekopf.wrt2 = Yes[i];
				QsWeKopf.dbupdate ();
			 }
			 if (i == 2) 
			 {
				strcpy (qswekopf.txt3, Texte[i]);
				qswekopf.wrt3 = Yes[i];
				QsWeKopf.dbupdate ();
			 }
			 if (i == 3) 
			 {
				strcpy (qswekopf.txt4, Texte[i]);
				qswekopf.wrt4 = Yes[i];
				QsWeKopf.dbupdate ();
			 }
			 if (i == 4) 
			 {
				strcpy (qswekopf.txt5, Texte[i]);
				qswekopf.wrt5 = Yes[i];
				QsWeKopf.dbupdate ();
			 }
			 if (i == 5) 
			 {
				strcpy (qswekopf.txt6, Texte[i]);
				qswekopf.wrt6 = Yes[i];
				QsWeKopf.dbupdate ();
			 }
			 if (i == 6) 
			 {
				strcpy (qswekopf.txt7, Texte[i]);
				qswekopf.wrt7 = Yes[i];
				QsWeKopf.dbupdate ();
			 }
			 if (i == 7) 
			 {
				strcpy (qswekopf.txt8, Texte[i]);
				qswekopf.wrt8 = Yes[i];
				QsWeKopf.dbupdate ();
			 }
			 if (i == 8) 
			 {
				strcpy (qswekopf.txt9, Texte[i]);
				qswekopf.wrt9 = Yes[i];
				QsWeKopf.dbupdate ();
			 }
			 if (i == 9) 
			 {
				strcpy (qswekopf.txt10, Texte[i]);
				qswekopf.wrt10 = Yes[i];
				QsWeKopf.dbupdate ();
			 }
			 if (i > 9)
			 {
 				qswekopf.lfd = (int) i + 1;
				strcpy (qswekopf.txt, Texte[i]);
				qswekopf.wrt = Yes[i];
				QsWeKopf.dbupdate ();
			 }
		 }

		 /*** 250210
		 for (i = 0; i < qsanz; i ++)
		 {
 		          qswekopf.lfd = (int) i + 1;
				  strcpy (qswekopf.txt, Texte[i]);
				  qswekopf.wrt = Yes[i];
				  QsWeKopf.dbupdate ();
		 }
		 *******/

}

void WeWork::WriteQsPos (void)
/**
Eingaben aus Qualit�tssicherung speichern.
**/
{
	     int i;
		 char datum [12];

		 sysdate (datum);
		 qswepos.mdn = we_kopf.mdn;
		 qswepos.fil = we_kopf.fil;
		 strcpy (qswepos.lief, we_kopf.lief);
		 strcpy (qswepos.lief_rech_nr, we_kopf.lief_rech_nr);
		 qswepos.dat = dasc_to_long (datum);
         qswepos.lfd = (int) 1;
         qswepos.a   = ratod (Lsts->a);
         qswepos.int_pos   = atoi (Lsts->p_nr);

         LiefBzg.sqlin ((short *)  &qswepos.mdn, 1, 0);
         LiefBzg.sqlin ((short *)  &qswepos.fil, 1, 0);
         LiefBzg.sqlin ((char *)   qswepos.lief, 0, 17);
         LiefBzg.sqlin ((short *)  qswepos.lief_rech_nr, 0, 17);
         LiefBzg.sqlin ((double *) &qswepos.a, 3, 0);
//         LiefBzg.sqlin ((short *)  &qswepos.int_pos, 1, 0);
         LiefBzg.sqlcomm ("delete from qswepos where mdn = ? "
                                               "and   fil = ? "
                                               "and   lief = ? "
                                               "and   lief_rech_nr = ? " 
                                               "and    a = ?");

//         qswepos.temp      = ratod (aufps.temp);
//         qswepos.ph_wert   = ratod (aufps.ph_wert);
	     QsWePos.dbreadfirst ();
		 strcpy (qswepos.pers_nam, sys_ben.pers_nam);

		 for (i = 0; i < qsanzpos; i ++)
		 {
 		          qswepos.lfd = (int) i + 1;
				  strcpy (qswepos.txt, TextePos[i]);
				  qswepos.wrt = YesPos[i];
				  QsWePos.dbupdate ();
		 }
}

/*
int WeWork::TestQsPos (void)
{
         int dsqlstatus;

		 qswepos.mdn = we_kopf.mdn;
		 qswepos.fil = we_kopf.fil;
		 strcpy (qswepos.lief, we_kopf.lief);
		 strcpy (qswepos.lief_rech_nr, we_kopf.lief_rech_nr);
         qswepos.a   =   we_pos.a;
         LiefBzg.sqlin ((short *)  &qswepos.mdn, 1, 0);
         LiefBzg.sqlin ((short *)  &qswepos.fil, 1, 0);
         LiefBzg.sqlin ((char *)   qswepos.lief, 0, 17);
         LiefBzg.sqlin ((short *)  qswepos.lief_rech_nr, 0, 17);
         LiefBzg.sqlin ((double *) &qswepos.a, 3, 0);
         dsqlstatus = LiefBzg.sqlcomm ("select a from qswepos "
                                           "where mdn = ? "
                                           "and   fil = ? "
                                           "and   lief = ? "
                                           "and   lief_rech_nr = ? "
                                           "and    a = ? "
                                           "and   wrt = 0");
         if (dsqlstatus == 0)
         {
             return 1;
         }
         return 0;
}
*/

int WeWork::TestQsPos (void)
{
         int dsqlstatus;

		 qswepos.mdn = we_kopf.mdn;
		 qswepos.fil = we_kopf.fil;
		 strcpy (qswepos.lief, we_kopf.lief);
		 strcpy (qswepos.lief_rech_nr, we_kopf.lief_rech_nr);
         qswepos.a   =   we_jour.a;
         LiefBzg.sqlin ((short *)  &qswepos.mdn, 1, 0);
         LiefBzg.sqlin ((short *)  &qswepos.fil, 1, 0);
         LiefBzg.sqlin ((char *)   qswepos.lief, 0, 17);
         LiefBzg.sqlin ((short *)  qswepos.lief_rech_nr, 0, 17);
         LiefBzg.sqlin ((double *) &qswepos.a, 3, 0);
         dsqlstatus = LiefBzg.sqlcomm ("select a from qswepos "
                                           "where mdn = ? "
                                           "and   fil = ? "
                                           "and   lief = ? "
                                           "and   lief_rech_nr = ? "
                                           "and    a = ? "
                                           "and   wrt = 0");
         if (dsqlstatus == 0)
         {
             return 1;
         }
         return 0;
}

BOOL WeWork::TestBestMulti (void)
/**
Pruefen, ob bei vorhandenem Wareneingang noch Bestellungen zugeordnet werden koennen.
**/
{
	   BOOL BuOK;

       best_koresp = 0;
	   BuOK = FALSE;
	   if (strcmp (we_kopf.koresp_best0, "                ") <= 0)
	   {
		           BuOK = TRUE;
	   }
	   if (BuOK) return TRUE;
	   if (multibest == FALSE) return FALSE;
	   for (best_koresp = 1; best_koresp <10; best_koresp ++)
	   {
 	               if (strcmp (korestab[best_koresp], "                ") <= 0)
				   {
							  return TRUE;
				   }
				   best_koresp ++;
	   }				   
       return FALSE;
}		         

/*
void WeWork::BestToWe (void)
{
    int dsqlstatus;
    char datum [12];
    int i;
    extern short sql_mode;
    short sqlmode;

    dsqlstatus = BestKopfClass.dbreadfirst ();
//	if (dsqlstatus != 0 || (druck_zwang && 
//								                     best_kopf.bearb_stat != 2))
	if (dsqlstatus != 0)
    {
               print_mess (2, "Bestellung %ld nicht gefunden", best_kopf.best_blg);
               return;
    }
 	TestBestMulti ();
	sprintf (korestab[best_koresp], "%ld", best_kopf.best_blg);
	*termtab[best_koresp] = best_kopf.best_term;
	*lieftab[best_koresp] = best_kopf.lief_term;

    memcpy (&we_pos, &we_pos_null, sizeof (we_pos));
    sysdate (datum);
    we_pos.mdn = we_kopf.mdn;
    we_pos.fil = we_kopf.fil;
    strcpy (we_pos.lief, we_kopf.lief);
    strcpy (we_pos.lief_rech_nr, we_kopf.lief_rech_nr);
	strcpy (we_pos.blg_typ, we_kopf.blg_typ);
	i = 0;
    strcpy (best_kopf.lief, we_kopf.lief);
	best_pos.mdn      = we_kopf.mdn;
	best_pos.fil      = we_kopf.fil;
    best_pos.best_blg = best_kopf.best_blg;
	strcpy (best_pos.lief, we_kopf.lief);
    sqlmode = sql_mode;
    sql_mode = 1;
	dsqlstatus = BestKopfClass.dblock ();
    if (dsqlstatus < 0)
    {
        print_mess (2, "Auf die Bestellung kann im Moment nicht zugegriffen werden. Status %d",
                    dsqlstatus);
        return;
    }

    sql_mode = sqlmode;
	dsqlstatus = BestPosClass.dbreadfirst_o ("order by a");
	while (dsqlstatus == 0)
	{
 			      we_pos.a         = best_pos.a;
		          dsqlstatus = WePosClass.dbreadfirst ();
		          if (dsqlstatus == 100)
				  {
		                        we_pos.akv = dasc_to_long (datum);
				  }
                  dsqlstatus   = lese_a_bas (we_pos.a);

		          we_pos.bearb = dasc_to_long (datum);
				  we_pos.inh       = best_pos.inh;
				  strcpy (we_pos.lief_best, best_pos.lief_best);
				  we_pos.best_me   = best_pos.me;
				  strcpy (we_pos.sa_kz, best_pos.sa_kz);
				  we_pos.me_einh   = best_pos.me_einh;
				  we_pos.int_pos   = i + 1;
				  we_pos.hbk_dat   = *((long *) LONGNULL);
                  we_pos.pr_ek     = best_pos.pr_ek_bto;
                  we_pos.pr_ek_nto = best_pos.pr_ek;
                  we_pos.pr_ek_euro   = best_pos.pr_ek_euro_bto;
                  we_pos.pr_ek_nto_eu = best_pos.pr_ek_euro;

                  ReadPr (); 
                  we_pos.pr_ek         = pr_ek_bto0;
                  we_pos.pr_ek_nto     = pr_ek0;
                  we_pos.pr_ek_euro    = pr_ek_bto0_euro;
                  we_pos.pr_ek_nto_eu  = pr_ek0_euro;
                  Reada_pr ();

                  we_pos.pr_fil_ek = GetPrFilEk ();
                  we_pos.pr_vk     = GetPrVk ();

                  if (atoi (_mdn.waehr_prim) == 2)
                  {
                           we_pos.pr_fil_ek_eu = we_pos.pr_fil_ek;
                           we_pos.pr_vk_eu = we_pos.pr_vk;
                           we_pos.pr_fil_ek *= _mdn.konversion;
                           we_pos.pr_vk *= _mdn.konversion;
                  }
                  else
                  {
                           we_pos.pr_fil_ek_eu = we_pos.pr_fil_ek /=_mdn.konversion;;
                           we_pos.pr_vk_eu = we_pos.pr_vk /=_mdn.konversion;;
                  }

				  WePosClass.dbupdate ();
				  i++;
				  dsqlstatus = BestPosClass.dbread_o ();
    }
	BestPosClass.dbclose_o ();
	BestKopfClass.dbreadfirst ();
	best_kopf.bearb_stat = VERBUCHT;
	BestKopfClass.dbupdate ();
}
*/

void WeWork::BestToWe (void)
{
    int dsqlstatus;
    char datum [12];
    int i;
    extern short sql_mode;
    short sqlmode;

    dsqlstatus = BestKopfClass.dbreadfirst ();
//	if (dsqlstatus != 0 || (druck_zwang && 
//								                     best_kopf.bearb_stat != 2))
	if (dsqlstatus != 0)
    {
               print_mess (2, "Bestellung %ld nicht gefunden", best_kopf.best_blg);
               return;
    }
 	TestBestMulti ();
	sprintf (korestab[best_koresp], "%ld", best_kopf.best_blg);
	*termtab[best_koresp] = best_kopf.best_term;
	*lieftab[best_koresp] = best_kopf.lief_term;

    memcpy (&we_jour, &we_jour_null, sizeof (we_jour));
    sysdate (datum);
    we_jour.mdn = we_kopf.mdn;
    we_jour.fil = we_kopf.fil;
    strcpy (we_jour.lief, we_kopf.lief);
    strcpy (we_jour.lief_rech_nr, we_kopf.lief_rech_nr);
	strcpy (we_jour.blg_typ, we_kopf.blg_typ);
	i = 0;
    strcpy (best_kopf.lief, we_kopf.lief);
	best_pos.mdn      = we_kopf.mdn;
	best_pos.fil      = we_kopf.fil;
    best_pos.best_blg = best_kopf.best_blg;
	strcpy (best_pos.lief, we_kopf.lief);
    sqlmode = sql_mode;
    sql_mode = 1;
	dsqlstatus = BestKopfClass.dblock ();
    if (dsqlstatus < 0)
    {
        print_mess (2, "Auf die Bestellung kann im Moment nicht zugegriffen werden. Status %d",
                    dsqlstatus);
        return;
    }

    sql_mode = sqlmode;
	dsqlstatus = BestPosClass.dbreadfirst_o ("order by a");
	while (dsqlstatus == 0)
	{
			//150812 A
				  lief_bzg.a = best_pos.a;
				  strcpy (lief_bzg.lief_best, best_pos.lief_best);
		          TestLief_bzg (lief_bzg.a,lief_bzg.lief_best);
		    //150812 E
 			      we_jour.a         = best_pos.a;
		          dsqlstatus = WePosClass.dbreadfirst ();
		          if (dsqlstatus == 100)
				  {
		                        we_jour.akv = dasc_to_long (datum);
				  }
                  dsqlstatus   = lese_a_bas (we_jour.a);

		          we_jour.bearb = dasc_to_long (datum);
				  we_jour.inh       = best_pos.inh;
				  strcpy (we_jour.lief_best, best_pos.lief_best);
				  we_jour.best_me   = best_pos.me;
                  if (MeStat)
                  {
                         if (atoi (lief_bzg.me_kz) == 1)
						 {
         				         we_jour.me   = best_pos.me * best_pos.inh;
						 }
						 else
						 {
         				         we_jour.me   = best_pos.me;
						 }
				  }
				  strcpy (we_jour.sa_kz, best_pos.sa_kz);
				  we_jour.me_einh   = best_pos.me_einh;
				  we_jour.best_me_einh   = best_pos.me_einh;
				  lief_bzg.mdn = we_kopf.mdn;
				  lief_bzg.fil = we_kopf.fil;
				  strcpy (lief_bzg.lief, we_kopf.lief);
                  ReadLief_besta (we_jour.a);
				  strcpy (we_jour.me_kz, lief_bzg.me_kz);
                  if (atoi (lief_bzg.me_kz) == 1)
				  {
		                 Einh.SetBestEinh (0);
                         GetMeEinh (we_kopf.mdn, we_kopf.fil, we_kopf.lief, we_jour.a, we_jour.lief_best);
						 we_jour.me_einh = WorkMeEinh;
				  }
				  we_jour.int_pos   = i + 1;
				  we_jour.hbk_dat   = *((long *) LONGNULL);
				  best_pos.inh = (best_pos.inh == 0) ? 1 : best_pos.inh;
//				  if (atoi (lief_bzg.me_kz) == 1)
				  if (atoi (lief_bzg.me_kz) == 0)   //150812
				  {

                         we_jour.pr_ek     = best_pos.pr_ek_bto / best_pos.inh;
                         we_jour.pr_ek_nto = best_pos.pr_ek / best_pos.inh;
                         we_jour.pr_ek_euro   = best_pos.pr_ek_euro_bto / best_pos.inh;
                         we_jour.pr_ek_nto_eu = best_pos.pr_ek_euro / best_pos.inh;
//150812						 we_jour.inh = 1;
				  }
			      else
				  {
                         we_jour.pr_ek     = best_pos.pr_ek_bto;
                         we_jour.pr_ek_nto = best_pos.pr_ek;
                         we_jour.pr_ek_euro   = best_pos.pr_ek_euro_bto;
                         we_jour.pr_ek_nto_eu = best_pos.pr_ek_euro;
						 we_jour.inh = 1; //150812
				  }


/*
                  ReadPr (); 
                  we_jour.pr_ek         = pr_ek_bto0;
                  we_jour.pr_ek_nto     = pr_ek0;
                  we_jour.pr_ek_euro    = pr_ek_bto0_euro;
                  we_jour.pr_ek_nto_eu  = pr_ek0_euro;
*/
                  Reada_pr ();

                  we_jour.pr_fil_ek = GetPrFilEk ();
                  we_jour.pr_vk     = GetPrVk ();
				  //250506
				  if (we_jour.pr_ek_nto_eu <= 0.0)
				  {
				  	  we_jour.pr_fil_ek = we_jour.pr_ek_nto_eu;
				  }

                  if (_mdn.konversion == 0)
				  {
			               _mdn.konversion = 1;
				  }
                  if (atoi (_mdn.waehr_prim) == 2)
                  {
                           we_jour.pr_fil_ek_eu = we_jour.pr_fil_ek;
                           we_jour.pr_vk_eu = we_jour.pr_vk;
                           we_jour.pr_fil_ek *= _mdn.konversion;
                           we_jour.pr_vk *= _mdn.konversion;
                  }
                  else
                  {
                           we_jour.pr_fil_ek_eu = we_jour.pr_fil_ek /=_mdn.konversion;;
                           we_jour.pr_vk_eu = we_pos.pr_vk /=_mdn.konversion;;
                  }

                  if (MeStat)
				  {
					  if (Lsts == NULL)
					  {
						  Lsts = new LSTS;
					  }
                      AktBsdMe = 0;
					  FillRow ();
                      BucheBsd (ratod (Lsts->a),Lsts->lief_best, ratod (Lsts->me), atoi (Lsts->me_einh),
                                ratod (Lsts->pr_ek),Lsts->ls_charge, Lsts->ls_ident,Lsts->lager);
				  }
				  WeJourClass.dbupdate ();
				  i++;
				  dsqlstatus = BestPosClass.dbread_o ();
    }
    if (MeStat)
	{
		delete Lsts;
		Lsts = NULL;
	}

	BestPosClass.dbclose_o ();
	BestKopfClass.dbreadfirst ();
	best_kopf.bearb_stat = VERBUCHT;
	BestKopfClass.dbupdate ();
    ReadBestTxt ();
}


void WeWork::ReadBestKopf (void)
{
	best_kopf.kontrakt_nr = 0l;
    BestKopfClass.dbreadfirst ();
}


int WeWork::TestKontrakt (double a)
{
	   if (best_kopf.kontrakt_nr <= 0l) return 0;

	   LiefBzg.sqlin ((short *) &best_kopf.mdn, 1, 0);
	   LiefBzg.sqlin ((short *) &best_kopf.fil, 1, 0);
	   LiefBzg.sqlin ((char *) best_kopf.lief, 0, 17);
	   LiefBzg.sqlin ((long *) &best_kopf.kontrakt_nr, 2, 0);
	   LiefBzg.sqlin ((double *) &a, 3, 0);
	   int dsqlstatus = LiefBzg.sqlcomm ("select a from best_pos "
		                                 "where mdn = ? " 
						                 "and fil = ? "
						                 "and lief = ? "
						                 "and best_blg = ? "
						                 "and a = ?");
       if (dsqlstatus == 100)
	   {
		   print_mess (2, "Artikel %.0lf ist nicht im Kontrakt %ld",
			              a, best_kopf.kontrakt_nr);
		   return -1;
	   }
	   return 0;
}

int WeWork::UpdateKontrakt (double a)
{
       double auf_me, kontrakt_me; 
       double me;
       short sql_s;
       char buffer [256];
       extern short sql_mode;

       if (Lsts == NULL) return 0;
	   if (best_kopf.kontrakt_nr <= 0l) return 0;
       if (TestKontraktMe == 0) return 0;

       sql_s = sql_mode;
	   LiefBzg.sqlin ((short *) &best_kopf.mdn, 1, 0);
	   LiefBzg.sqlin ((short *) &best_kopf.fil, 1, 0);
	   LiefBzg.sqlin ((char *)  best_kopf.lief, 0, 17);
	   LiefBzg.sqlin ((long *)  &best_kopf.kontrakt_nr, 2, 0);
	   LiefBzg.sqlin ((double *) &a, 3, 0);
	   LiefBzg.sqlout ((double *) &auf_me, 3, 0);
	   LiefBzg.sqlout ((double *) &kontrakt_me, 3, 0);
	   int dsqlstatus = LiefBzg.sqlcomm ("select me,kontrakt_me from best_pos "
		                                 "where mdn = ? " 
						                 "and fil = ? "
						                 "and lief = ? "
						                 "and best_blg = ? "
						                 "and a = ?");
       if (dsqlstatus == 100)
	   {
		   print_mess (2, "Artikel %.0lf ist nicht im Kontrakt %ld",
			              a, best_kopf.kontrakt_nr);
		   return -1;
	   }
       me = ratod (Lsts->me) - akt_me;
       if (me > (auf_me - kontrakt_me))
       {
           if (TestKontraktMe == 2)
           {
               sprintf (buffer, "Wareneingangsmenge %.3lf ist zu gro�.\n"
                                "Restmenge im Kontrakt : %.3lf\n"
                                "Speichern ?",
                                 ratod (Lsts->me), (auf_me - kontrakt_me));
              if (abfragejn (NULL, 
  					          buffer , "N") == 0)
              {
                     akt_me = 0.0;
                     return -1;
              }
           }
           else 
           {
               print_mess (2, "Wareneingangsmenge %.3lf ist zu gro�.\n"
                              "Restmenge im Kontrakt : %.3lf",
                              ratod (Lsts->me), (auf_me - kontrakt_me));
               akt_me = 0.0;
               return -1;
           }

       }
       kontrakt_me += me;
	   LiefBzg.sqlin ((double *) &kontrakt_me, 3, 0);
	   LiefBzg.sqlin ((short *) &best_kopf.mdn, 1, 0);
	   LiefBzg.sqlin ((short *) &best_kopf.fil, 1, 0);
	   LiefBzg.sqlin ((char *) best_kopf.lief, 0, 17);
	   LiefBzg.sqlin ((long *) &best_kopf.kontrakt_nr, 2, 0);
	   LiefBzg.sqlin ((double *) &a, 3, 0);
       int cursor = LiefBzg.sqlcursor ("update best_pos set kontrakt_me = ? "
		                               "where mdn = ? " 
						               "and fil = ? "
						               "and lief = ? "
						               "and best_blg = ? "
                                       "and a = ?");
       int tries = 0;
       dsqlstatus = LiefBzg.sqlexecute (cursor); 
       while (dsqlstatus < 0)
       {
           tries ++;
           if (tries == MAXTRIES) 
           {
               disp_mess ("Kontrakt konnte nicht korrigiert werden", 2);
               return -1;
           }
           Sleep (10);
           dsqlstatus = LiefBzg.sqlexecute (cursor); 
       }
	   return 0;
}

void WeWork::DecKontrakt (double a, double me)
{
	if (best_kopf.kontrakt_nr == 0l) return;

	best_pos.mdn = we_kopf.mdn;
	best_pos.fil = we_kopf.fil;
	best_pos.best_blg = best_kopf.kontrakt_nr;
	strcpy (best_pos.lief, we_kopf.lief);
	best_pos.a = a;
    int dsqlstatus = BestPosClass.dbreadfirst ();
	if (dsqlstatus != 0) return;
	best_pos.me -= me;
    BestPosClass.dbupdate ();
}



/*
void WeWork::DecKontrakt (void)
{
	double a;
	double me;


    we_pos.mdn = we_kopf.mdn;
    we_pos.fil = we_kopf.fil;
    strcpy (we_pos.lief, we_kopf.lief);
	strcpy (we_pos.lief_rech_nr, we_kopf.lief_rech_nr);
	strcpy (we_pos.blg_typ, we_kopf.blg_typ);

    LiefBzg.sqlin ((short *) &we_pos.mdn, 1, 0);
    LiefBzg.sqlin ((short *) &we_pos.fil, 1, 0);
    LiefBzg.sqlin ((char *)  we_pos.lief, 0, 17);
    LiefBzg.sqlin ((char *)  we_pos.lief_rech_nr, 0, 17);
    LiefBzg.sqlin ((char *)  &we_pos.blg_typ, 0, 2);
    LiefBzg.sqlout ((double *) &a,3, 0);
    LiefBzg.sqlout ((double *) &me, 3,0);
    int cursor = LiefBzg.sqlcursor ("select a, me from we_pos "
                     "where mdn = ? "
                     "and fil = ? "
                     "and lief = ? "
                     "and lief_rech_nr = ? "
                     "and blg_typ = ?");

    while (LiefBzg.sqlfetch (cursor) == 0)
    {
		   DecKontrakt (a, me);
    }
    LiefBzg.sqlclose (cursor);
}
*/
         
void WeWork::DecKontrakt (void)
{
	double a;
	double me;


    we_jour.mdn = we_kopf.mdn;
    we_jour.fil = we_kopf.fil;
    strcpy (we_jour.lief, we_kopf.lief);
	strcpy (we_jour.lief_rech_nr, we_kopf.lief_rech_nr);
	strcpy (we_jour.blg_typ, we_kopf.blg_typ);

    LiefBzg.sqlin ((short *) &we_jour.mdn, 1, 0);
    LiefBzg.sqlin ((short *) &we_jour.fil, 1, 0);
    LiefBzg.sqlin ((char *)  we_jour.lief, 0, 17);
    LiefBzg.sqlin ((char *)  we_jour.lief_rech_nr, 0, 17);
    LiefBzg.sqlin ((char *)  &we_jour.blg_typ, 0, 2);
    LiefBzg.sqlout ((double *) &a,3, 0);
    LiefBzg.sqlout ((double *) &me, 3,0);
    int cursor = LiefBzg.sqlcursor ("select a, me from we_jour "
                     "where mdn = ? "
                     "and fil = ? "
                     "and lief = ? "
                     "and lief_rech_nr = ? "
                     "and blg_typ = ?");

    while (LiefBzg.sqlfetch (cursor) == 0)
    {
		   DecKontrakt (a, me);
    }
    LiefBzg.sqlclose (cursor);
}


/*
void WeWork::PrepareLst (void)
{
	we_pos.mdn = we_kopf.mdn;
	we_pos.fil = we_kopf.fil;
	strcpy (we_pos.lief, we_kopf.lief);
	strcpy (we_pos.lief_rech_nr, we_kopf.lief_rech_nr);
	strcpy (we_pos.blg_typ, we_kopf.blg_typ);

	LiefBzg.sqlin ((short *) &we_pos.mdn, 1, 0);
	LiefBzg.sqlin ((short *) &we_pos.fil, 1, 0);
	LiefBzg.sqlin ((char *)  we_pos.lief, 0, 17);
	LiefBzg.sqlin ((char *)  we_pos.lief_rech_nr, 0, 17);
	LiefBzg.sqlin ((char *)  we_pos.blg_typ, 0, 2);

	LiefBzg.sqlout ((long *)   &we_pos.int_pos, 2, 0);
	LiefBzg.sqlout ((double *) &we_pos.a, 3, 0);

	LstCursor = WePosClass.sqlcursor ("select int_pos, a from we_pos "
		                      "where mdn = ? "
							  "and   fil = ? "
							  "and   lief  = ? "
                              "and   lief_rech_nr = ? "
							  "and blg_typ = ? "
							  "order by int_pos,a");
}
*/

void WeWork::PrepareLst (void)
{

	LiefBzg.sqlin ((short *) &we_jour.mdn, 1, 0);
	LiefBzg.sqlin ((short *) &we_jour.fil, 1, 0);
	LiefBzg.sqlin ((char *)  we_jour.lief, 0, 17);
	LiefBzg.sqlin ((char *)  we_jour.lief_rech_nr, 0, 17);
	LiefBzg.sqlin ((char *)  we_jour.blg_typ, 0, 2);

	LiefBzg.sqlout ((long *)   &we_jour.int_pos, 2, 0);
	LiefBzg.sqlout ((double *) &we_jour.a, 3, 0);

	LstCursor = WePosClass.sqlcursor ("select int_pos, a from we_jour "
		                      "where mdn = ? "
							  "and   fil = ? "
							  "and   lief  = ? "
                              "and   lief_rech_nr = ? "
							  "and blg_typ = ? "
							  "order by int_pos,a");
}

void WeWork::OpenLst (void)
{
	if (LstCursor == -1)
	{
		PrepareLst ();
		if (LstCursor == -1) return;
	}

	we_jour.mdn = we_kopf.mdn;
	we_jour.fil = we_kopf.fil;
	strcpy (we_jour.lief, we_kopf.lief);
	strcpy (we_jour.lief_rech_nr, we_kopf.lief_rech_nr);
	strcpy (we_jour.blg_typ, we_kopf.blg_typ);
	WePosClass.sqlopen (LstCursor);
}

int WeWork::FetchLst (void)
{
	if (LstCursor == -1)
	{
		PrepareLst ();
		if (LstCursor == -1) return 100;
	}

/*
	if (WePosClass.sqlfetch (LstCursor) != 0)
    {
         WePosClass.dbreadfirst ();        
         return 100;
    }
	return WePosClass.dbreadfirst ();
*/
	if (WeJourClass.sqlfetch (LstCursor) != 0)
    {
         WeJourClass.dbreadfirst ();        
         return 100;
    }
	int dsqlstatus = WeJourClass.dbreadfirst ();
    lese_a_bas (we_jour.a);
    return dsqlstatus;
}

BOOL WeWork::ReadExtraData ()
{
    if (_a_bas.charg_hand != 9 && _a_bas.zerl_eti != 1)
	{
		return FALSE;
	}
	strcpy (zerldaten.ident_extern, Lsts->ls_ident); 
	zerldaten.a = ratod (Lsts->a); 
	strcpy (zerldaten.lief, we_kopf.lief);
	strcpy (zerldaten.lief_rech_nr, we_kopf.lief_rech_nr);
	zerldaten.mdn = we_kopf.mdn;
	zerldaten.fil = we_kopf.fil;
	if (Zerldaten.dbtestinsert () == 0)
	{
		if (Zerldaten.dbreadfirst () == 0)
		{
			return TRUE;
		}
	}
	return FALSE;
}

void WeWork::CloseLst (void)
{
	if (LstCursor != -1)
	{
//		WePosClass.sqlclose (LstCursor);
		WeJourClass.sqlclose (LstCursor);
		LstCursor = -1;
	}
}


BOOL WeWork::BsdArtikel (double a)
/**
Bestandskennzeichen in a_bas pruefen.
**/
{
	     char bsd_kz [2];

		 strcpy (bsd_kz, "N");
		 LiefBzg.sqlin ((double *) &a, 3, 0);
		 LiefBzg.sqlout ((char *) bsd_kz, 0, 2);
		 LiefBzg.sqlcomm ("select bsd_kz from a_bas where a = ?");
		 if (bsd_kz[0] == 'J') return TRUE;
		 return FALSE;
}


int WeWork::GetHbk (double a, int hbk_ztr) 
{
         char hbk_kz[2]; 

         LiefBzg.sqlin ((double *) &a,3, 0);
         LiefBzg.sqlout ((char *) hbk_kz, 0, 2);
         int dsqlstatus = LiefBzg.sqlcomm ("select hbk_kz from a_bas "
                                            "where a = ?");
         if (dsqlstatus != 0)
         {
             return  hbk_ztr;
         }
         switch (hbk_kz[0])
         {
            case 'T' :
                 return hbk_ztr;
            case 'W' : 
                 return hbk_ztr * 7;
            case 'M' :
                 return hbk_ztr * 30;
         }
         return hbk_ztr;
}

void WeWork::TestChargeDat (void)
{
    static int ChargePar = -1;
    char sys_par_wrt [2];
    double a_charge;


    if (ChargePar == -1)
    {
        ChargePar = 0;
        strcpy (sys_par_wrt, "0");
        LiefBzg.sqlout ((char *) sys_par_wrt, 0, 2);
        LiefBzg.sqlcomm ("select sys_par_wrt from sys_par "
                         "where sys_par_nam = \"lsc2_par\"");
        ChargePar = atoi (sys_par_wrt); 
        if (ChargePar == 0)
        {
              LiefBzg.sqlout ((char *) sys_par_wrt, 0, 2);
              LiefBzg.sqlcomm ("select sys_par_wrt from sys_par "
                               "where sys_par_nam = \"lsc3_par\"");
              ChargePar = atoi (sys_par_wrt); 
        }
    }

    if (strcmp (Lsts->ls_charge, " ") <= 0) 
    {
        return;
    }
    if (ChargePar == 0)
    {
        return;
    }

    lese_a_bas (bsd_buch.a);
    if (_a_bas.charg_hand != 1)
    {
        return;
    }

    a_charge = _a_bas.a;
    if (_a_bas.a_grund != 0.0)
    {
        a_charge = _a_bas.a_grund;
    } 

    LiefBzg.sqlin ((short *)  &we_kopf.mdn, 1, 0); 
    LiefBzg.sqlin ((short *)  &we_kopf.fil, 1, 0); 
    LiefBzg.sqlin ((double *) &a_charge, 3, 0); 
    LiefBzg.sqlin ((char *)   bsd_buch.chargennr, 0, 31);
    LiefBzg.sqlout ((long *) &bsd_buch.verf_dat, 2, 0);
    int dsqlstatus = LiefBzg.sqlcomm ("select verf_dat from bsds "
                                      "where mdn = ? "
                                      "and fil = ? "
                                      "and a = ? "
                                      "and chargennr = ?");
}

void WeWork::BucheBsd (double a,char *lief_best, double auf_me, short me_einh_lief, double pr_ek_bto , char *chargennr, char *identnr, short lager)
/**
Bestandsbuchung vorbereiten.
**/
{
	double me_vgl;
	double buchme;
	char datum [12];

    if (Lsts == NULL) return;
	if (bsd_kz == 0) return;
	if (we_kopf.bsd_verb_kz[0] != 'J')
	{
		return;
	}

	if (lager > 0) akt_lager = lager;
	if (BsdArtikel (a) == FALSE) return;
    lese_a_bas (a);
    me_vgl = GetMeVgl (a,lief_best, auf_me,me_einh_lief);
	if (me_vgl == AktBsdMe) return;
	buchme = me_vgl - AktBsdMe;

	bsd_buch.nr  = best_kopf.best_blg;
	strcpy (bsd_buch.blg_typ, "WE");   // Achtung Kennzeichen W noch �berpr�fen
 	bsd_buch.mdn = we_kopf.mdn;
	bsd_buch.fil = we_kopf.fil;
	bsd_buch.kun_fil = 0;
	bsd_buch.a   = a;
	sysdate (datum);
//	bsd_buch.dat = dasc_to_long (datum);
	bsd_buch.dat = we_kopf.we_dat;
	systime (bsd_buch.zeit);
	strcpy (bsd_buch.pers, sys_ben.pers);

    sprintf (bsd_buch.bsd_lgr_ort, "%ld", akt_lager);

	bsd_buch.qua_status = 0;
	bsd_buch.me = buchme;
	if (strcmp (we_kopf.blg_typ, "X") == 0)
	{
		bsd_buch.me *= -1;
	}
	bsd_buch.bsd_ek_vk = ratod (Lsts->netto_ek) / Lsts->inh_einh;
//    strcpy (bsd_buch.chargennr, clipped (Lsts->ls_charge));
	if (strlen(clipped(identnr)) > 1)
	{
	    strcpy (bsd_buch.chargennr, clipped (identnr));
	}
	else
	{
	    strcpy (bsd_buch.chargennr, clipped (chargennr));
	}
    strcpy (bsd_buch.ident_nr, clipped(identnr));
    strcpy (bsd_buch.herk_nachw, "");
    sprintf (bsd_buch.lief, "%s", we_kopf.lief);
    bsd_buch.auf = best_kopf.best_blg;
	if (dasc_to_long (Lsts->hbk_dat) > 0)  //050809
	{
		bsd_buch.verf_dat = dasc_to_long (Lsts->hbk_dat);
	}
	else
	{
	    if (atoi (Lsts->mhd) > 0)
		{
            bsd_buch.verf_dat = bsd_buch.dat + 
                                GetHbk (bsd_buch.a, atoi (Lsts->mhd));
	    }
		else 
		{	
			TestChargeDat ();
		}
	}
//    bsd_buch.verf_dat = we_jour.hbk_dat
    strcpy  (bsd_buch.verfall, "");
    bsd_buch.delstatus = 0;
	bsd_buch.auf = atoi(we_kopf.lief_rech_nr);
    strcpy  (bsd_buch.err_txt, "19.02.2009");
	BsdBuch.dbinsert ();
}

void WeWork::FuellePreise_we (short AnzTagePreise)
/**
preise_we f�llen, falls leer
**/
{
   int cursork, cursorp, cursorwek, cursorwep;
   long we_dat,best_term, best_blg;
   double pr_ek_w, pr_ek_b;
   short me_einh_w, me_einh_b;
   char datum [12];
   char lief_rech_nr [16];
   long ddatum ; 
    static DB_CLASS DbClass;

	if (DbClass.sqlcomm ("select a from preise_we ") == 100)
	{
		sysdate (datum);
		ddatum = dasc_to_long (datum) - AnzTagePreise  ;

	   DbClass.sqlin ((short *) &we_kopf.mdn, 1, 0);
	   DbClass.sqlin ((short *) &we_kopf.fil, 1, 0);
	   DbClass.sqlin ((long *)  &ddatum, 2, 0);
	   DbClass.sqlout ((long *) &best_blg, 2, 0);
	   DbClass.sqlout ((long *) &best_term, 2, 0);
	   DbClass.sqlout ((char *) preise_we.lief, 0, 16);
	   cursork = DbClass.sqlcursor ("select best_blg, best_term,lief from best_kopf where mdn = ? "
		                                                  "and fil = ? "
														  "and best_term  > ? "
														  "order by best_term desc, "
														  "best_blg desc");

	   DbClass.sqlin ((short *) &we_kopf.mdn, 1, 0);
	   DbClass.sqlin ((short *) &we_kopf.fil, 1, 0);
	   DbClass.sqlin ((long *)  &best_blg, 2, 0);
	   DbClass.sqlout ((double *) &preise_we.a, 3, 0);
	   DbClass.sqlout ((double *) &pr_ek_b, 3, 0);
	   DbClass.sqlout ((short *) &me_einh_b, 1, 0);
	   cursorp = DbClass.sqlcursor ("select a,pr_ek,me_einh from best_pos "
		                            "where mdn = ? "
									"and fil = ? "
									"and best_blg = ? ");

       while (DbClass.sqlfetch (cursork) == 0)
	   {
		   if (DbClass.sqlopen (cursorp)) break;
		   while (DbClass.sqlfetch (cursorp) == 0)
		   {
				preise_we.mdn = we_kopf.mdn;
				preise_we.best_blg = best_blg;
				strcpy(preise_we.lief_rech_nr,"");
				if (preise_we_class.dbreadfirst () != 0)
				{
					preise_we.best_blg = best_blg;
					strcpy (preise_we.lief_rech_nr,"");
					preise_we.we_dat = 0;
					preise_we.pr_ek_w = 0;
					preise_we.me_einh_w = 0;
				}
			
				preise_we.best_term = best_term;
				preise_we.pr_ek_b = pr_ek_b;
				preise_we.me_einh_b = me_einh_b;
				preise_we_class.dbupdate ();
		   }
	   }

	   DbClass.sqlin ((short *) &we_kopf.mdn, 1, 0);
	   DbClass.sqlin ((short *) &we_kopf.fil, 1, 0);
	   DbClass.sqlin ((long *)  &ddatum, 2, 0);

	   DbClass.sqlout ((char *) &lief_rech_nr, 0, 16);
	   DbClass.sqlout ((long *) &preise_we.best_blg, 2, 0);
	   DbClass.sqlout ((long *) &we_dat, 2, 0);
	   DbClass.sqlout ((char *) preise_we.lief, 0, 16);
	   cursorwek = DbClass.sqlcursor ("select lief_rech_nr, koresp_best0, we_dat,lief from we_kopf where mdn = ? "
		                                                  "and fil = ? "
														  "and we_dat  > ? and blg_typ <> \"X\" "
														  "order by we_dat desc, "
														  "lief_rech_nr desc");

	   DbClass.sqlin ((short *) &we_kopf.mdn, 1, 0);
	   DbClass.sqlin ((short *) &we_kopf.fil, 1, 0);
	   DbClass.sqlin ((char *)  &lief_rech_nr, 0, 16);
	   DbClass.sqlin ((char *) preise_we.lief, 0, 16);

	   DbClass.sqlout ((double *) &preise_we.a, 3, 0);
	   DbClass.sqlout ((double *) &pr_ek_w, 3, 0);
	   DbClass.sqlout ((short *) &me_einh_w, 1, 0);
	   cursorwep = DbClass.sqlcursor ("select a,pr_ek_euro,me_einh from we_jour "
		                            "where mdn = ? "
									"and fil = ? "
									"and lief_rech_nr = ? "
									"and lief = ?");

       while (DbClass.sqlfetch (cursorwek) == 0)
	   {
		   if (DbClass.sqlopen (cursorwep)) break;
		   while (DbClass.sqlfetch (cursorwep) == 0)
		   {
				preise_we.mdn = we_kopf.mdn;
				strcpy (preise_we.lief_rech_nr,lief_rech_nr);
				if (preise_we_class.dbreadfirst () != 0)
				{
					preise_we.best_blg = 0;
					preise_we.best_term = 0;
					preise_we.pr_ek_b = 0;
					preise_we.me_einh_b = 0;
				}
			
				strcpy (preise_we.lief_rech_nr,lief_rech_nr);
				preise_we.we_dat = we_dat;
				preise_we.pr_ek_w = pr_ek_w;
				preise_we.me_einh_w = me_einh_w;
				preise_we_class.dbupdate ();
				preise_we.best_blg = 0;
		   }
	   }

	   DbClass.sqlclose (cursorp);
	   DbClass.sqlclose (cursork);
	   DbClass.sqlclose (cursorwep);
	   DbClass.sqlclose (cursorwek);
	}
}



void WeWork::SchreibeLief (void) //Detlef 011208
{
    extern short sql_mode;
    short sqlmode;
    static DB_CLASS DbClass;

	sqlmode = sql_mode;
	sql_mode = 1;  //continue  wenn DS gesperrt, dann eben kein update
	    DbClass.sqlin ((char *)   lief.esnum, 0, 10);
	    DbClass.sqlin ((char *)   lief.eznum, 0, 10);
	    DbClass.sqlin ((short *)  &lief.geburt, 1, 0);
	    DbClass.sqlin ((short *)  &lief.mast, 1, 0);
	    DbClass.sqlin ((short *)  &lief.schlachtung, 1, 0);
	    DbClass.sqlin ((short *)  &lief.zerlegung, 1, 0);
	    DbClass.sqlin ((char *)   we_kopf.lief, 0, 16);
		DbClass.sqlcomm ("update lief set esnum = ?, eznum = ?,geburt = ?,mast = ?, "
										 " schlachtung = ?, zerlegung = ? "
                                      "where lief = ? ");


	sql_mode = sqlmode;
}

