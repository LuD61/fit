#include <windows.h>
#include "searchlief_best.h"
#include "strfkt.h"
#include "mo_wmess.h"
#include "lbox.h"


int SEARCHLIEF_BEST::idx;
long SEARCHLIEF_BEST::anz;
CHQEX *SEARCHLIEF_BEST::Query = NULL;
DB_CLASS SEARCHLIEF_BEST::DbClass; 
HINSTANCE SEARCHLIEF_BEST::hMainInst;
HWND SEARCHLIEF_BEST::hMainWindow;
HWND SEARCHLIEF_BEST::awin;


int SEARCHLIEF_BEST::SearchLst (char *sebuff)
/**
Nach Kurzname in Liste suchen.
**/
{
	   Query->SearchList ();
	   return 0;
}

int SEARCHLIEF_BEST::ReadLst (char *name)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int cursor;
	  int i;
      WMESS Wmess;
      short mdn_nr;
      char adr_krz [17];


	  anz = 0;
      clipped (name);
      if (strcmp (name, " ") == 0) name[0] = 0;

      if (strlen (name) == 0 ||
          name[0] <= ' ')
      {
	          sprintf (buffer, "select mdn.mdn, adr.adr_krz "
	 			       "from mdn, adr "
                       "where adr.adr = mdn.adr "
                       "order by mdn");
      }
      else
      {
	          sprintf (buffer, "select mdn.mdn, adr.adr_krz "
	 			       "from mdn,adr "
                       "where adr_krz matches \"%s\" "
                       "and adr.adr_krz = mdn.adr_krz", name);
      }
      DbClass.sqlout ((short *) &mdn_nr, 1, 0);
      DbClass.sqlout ((char *) adr_krz, 0, 17);
      cursor = DbClass.sqlcursor (buffer);
      if (cursor < 0) 
      {
          return -1;
      }
  	  i = 0;
      Wmess.Message (hMainInst, hMainWindow, " \nBitte warten.....\n"
  	                                         "Die Daten werden selektiert.\n ");  
	  while (DbClass.sqlfetch (cursor) == 0)
      {
 	      sprintf (buffer, "    %4d  |%-16s|", mdn_nr, adr_krz);
	      Query->InsertRecord (buffer);
		  i ++;
      }
      anz = i;
	  DbClass.sqlclose (cursor);
 	  Wmess.Destroy ();
	  EnableWindow (awin, FALSE);
	  return 0;
}

void SEARCHLIEF_BEST::SetParams (HINSTANCE hMainInst, HWND hMainWindow)
{
      this->hMainInst   = hMainInst;
      this->hMainWindow = hMainWindow;
      awin = hMainWindow;
}

BOOL SEARCHLIEF_BEST::GetKey (char * key)
{
      int anz;
     
      if (idx == -1) return FALSE;
      anz = wsplit (Key, "|");
      if (anz < 1) return FALSE;
      strcpy (key, wort[0]);
      return TRUE;
}


void SEARCHLIEF_BEST::Search (void)
{
  	  int cx, cy;
	  char buffer [256];

	  idx = -1;
      cx = 48;
      cy = 25;
      Settchar ('|');
      EnableWindow (hMainWindow, FALSE);
      Query = new CHQEX (cx, cy);
      Query->EnableSort (TRUE);
      Query->OpenWindow (hMainInst, hMainWindow);
      sprintf (buffer, "%s %s",
                          "%d",
                          "%s");
      Query->RowAttr (buffer);
	  sprintf (buffer, "    %6s", "1"); 
	  Query->VLines (buffer, 3);
      sprintf (buffer, "4;4 11;16");
      Query->RowPos (buffer);
	  EnableWindow (awin, FALSE);

	  sprintf (buffer, "    %4s  %-16s", "Mdn", "Name"); 
	  Query->InsertCaption (buffer);

	  Query->SetFillDb (ReadLst);
	  Query->SetSearchLst (SearchLst);
      ReadLst ("");
      Query->SetSortRow (1, TRUE);
	  Query->ProcessMessages ();
      EnableWindow (hMainWindow, TRUE);
      if (syskey != KEY5)
      {
              idx = Query->GetSel ();
              if (idx != -1)
              {
                     Query->GetText (Key);
              }
      }
	  EnableWindow (awin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (awin);
}

