#ifndef _COMBODLG_DEF
#define _COMBODLG_DEF
#include "dlg.h"
#include "itprog.h"
#include "itfont.h"
#include "formfield.h"
#include "cmask.h"

#define COMBOBOX_CTL 1001 
#define COMBOCLOSED 1002 

class ComboDlg : virtual public DLG 
{
          private :
            static  mfont dlgfont;
            static  mfont dlgposfont;
           static mfont *Font;

            static char combotext[];
            static char cEinhTxt[];
            static CFIELD *_fCombo0[];
            static CFORM fCombo0; 
            static CFIELD *_fCombo[];
            static CFORM fCombo;
            BOOL CbClosed;
          public :

	    static COLORREF SysBkColor;
            ComboDlg (int, int, int, int, char *, int, BOOL, char*);
            ~ComboDlg ();
 	    void Init (int, int, int, int, char *, int, BOOL,char*);
            void SetComboBox (char **);
            BOOL OnKey5 (void);
            BOOL OnKeyReturn (void);
            BOOL OnCloseUp (HWND,UINT,WPARAM,LPARAM);
            BOOL OnCommand (HWND,UINT,WPARAM,LPARAM);
            BOOL OnSysCommand (HWND,UINT,WPARAM,LPARAM);

            void SetWinBackground (COLORREF);
            void ToClipboard (void);
            void FromClipboard (void);
};
#endif
