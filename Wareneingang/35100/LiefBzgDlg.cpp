#include<stdio.h>
#include<stdarg.h>
#include<string.h>
#include<windows.h>
#include "stdfkt.h"
#include "strfkt.h"
#include "LiefBzgdlg.h" 
#include "MeEinhDlg.h" 
#include "mo_gdruck.h" 
#include "colbut.h"
#include "help.h"
#include "lbox.h"
#include "lief_bzg.h"
#include "a_bas.h"
#include "searchmdn.h"
#include "searchlief_best.h"
#include "searchfil.h"
#include "searchlief.h"
#include "searcha.h"
#ifdef BIWAK
#include "conf_env.h"
#endif
#include "mo_progcfg.h"


static int StdSize = STDSIZE;

static int dlgsize    = STDSIZE;
static int ltgraysize = STDSIZE;
static int dlgpossize = STDSIZE;
static int cpsize     = 200;


static mfont dlgfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         GRAYCOL,
                         0,
                         NULL};

static mfont ltgrayfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         LTGRAYCOL,
                        0,
                         NULL};

static mfont checkfonth = {
                           "ARIAL", STDSIZE, 0, 0,
                            BLACKCOL,
                            LTGRAYCOL,
                            0,
                            NULL};

static mfont dlgposfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         LTGRAYCOL,
                         0,
                         NULL};


static mfont cpfont = {
                        "Times New Roman", 200, 0, 1,
                         BLUECOL,
                         GRAYCOL,
                         0,
                         NULL};

static mfont *Font = &dlgposfont;

struct BZGF
{
      char mdn [7];
      char mdn_krz [20];
      char fil [7];
      char fil_krz [18];
      char lief [18];
      char lief_krz [20];
      char default_lief_kz [4];
      char default_me_kz [16];
      char a [15];
      char a_bz1 [26];
      char a_bz2 [26];
      char lief_best [18];
      char best_txt1 [26];
      char best_txt2 [26];
      char lief_kz [4];
      char lief_rht [4];
      char lief_rht_bez [10];
      char lief_zeit [6];
      char me_einh_ek [6];
      char me_einh_ek_bez [10];
      char min_best [10];;
      char pr_ek [10];
      char pr_ek_eur [10];
      char me_kz[4];
      char dat [12];
      char zeit[8];
} bzgf, bzgf_null;


CFIELD *_fHead [] = {
                     new CFIELD ("mdn_txt", "Mandant",  0, 0, 1, 0,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("mdn", bzgf.mdn,  6, 0, 10, 0,  NULL, "%4d", CEDIT,
                                 MDN_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("mdn_choise", "", 2, 0, 16, 0, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("mdn_krz", bzgf.mdn_krz, 19, 0, 30, 0,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, 0),
                     new CFIELD ("fil_txt", "Filiale",  0, 0, 50, 0,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("fil", bzgf.fil,  6, 0, 58, 0,  NULL, "%4d", CEDIT,
                                 FIL_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("fil_choise", "", 2, 0, 64, 0, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("fil_krz", bzgf.fil_krz, 17, 0, 67, 0,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, 0),
                     new CFIELD ("lief_txt", "Lieferant",  0, 0, 1, 2,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("lief", bzgf.lief, 17, 0, 10, 2,  NULL, "", CEDIT,
                                 LIEF_CTL, Font, 0, 0),
                     new CFIELD ("lief_choise", "", 2, 0, 27, 2, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("lief_krz", bzgf.lief_krz, 17, 0, 30, 2,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, 0),
/*
                     new CFIELD ("default_lief_kz_txt", "Haupt-Lieferant",  0, 0,50, 2,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("default_lief_kz", bzgf.default_lief_kz, 2, 0, 66, 2,  NULL, "", CEDIT,
                                 DEFAULT_LIEF_KZ_CTL, Font, 0, ES_UPPERCASE),
*/
                     
                     new CFIELD ("default_lief_kz", "Hauptlieferant", bzgf.default_lief_kz,
                                 15, 0, 50, 2,  NULL, "", CBUTTON,
                                 DEFAULT_LIEF_KZ_CTL, Font, 0, BS_AUTOCHECKBOX | WS_BORDER),

                     new CFIELD ("default_me_kz", bzgf.default_me_kz,
                                 17, 5, 66, 2,  NULL, "", CCOMBOBOX,
                                 DEFAULT_ME_KZ_CTL, Font, 0,    CBS_DROPDOWNLIST |
                                                        WS_VSCROLL),
                     NULL,
};

CFORM fHead (6, _fHead);


CFIELD *_fPos [] = {
                     new CFIELD ("pos_frame", "",    85,10, 1, 4,  NULL, "",
                                 CBORDER,   
                                 500, Font, 0, TRANSPARENT),    
                     new CFIELD ("a_txt", "Artikel",  0, 0, 2, 5,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("a", bzgf.a, 14, 0, 2, 6,  NULL, "%13.0lf", CEDIT,
                                 A_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("a_choise", "", 2, 0, 16, 6, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("lief_kz_txt", "Hauptlieferant",  0, 0, 19, 5,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("lief_kz", bzgf.lief_kz, 2, 0, 19, 6,  NULL, "", CEDIT,
                                 LIEF_KZ_CTL, Font, 0, ES_UPPERCASE),
                     new CFIELD ("a_bz1_txt", "Artikeltext1", 0, 0, 31, 5,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("a_bz1", bzgf.a_bz1, 25, 0, 31, 6,  NULL, "", CREADONLY,
                                 A_BZ1_CTL, Font, 0, 0),
                     new CFIELD ("best_txt1_txt", "Bestellext1", 0, 0, 60, 5,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("best_txt1", bzgf.best_txt1, 25, 0, 60, 6,  NULL, "", CEDIT,
                                 BEST_TXT1_CTL, Font, 0, 0),

                     new CFIELD ("lief_best_txt", "BestellNr",  0, 0, 2, 7,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("lief_best", bzgf.lief_best, 17, 0, 2, 8,  NULL, "", CEDIT,
                                 LIEF_BEST_CTL, Font, 0, 0),
                     new CFIELD ("pr_ek_txt", "EK-Preis",  0, 0, 19, 7,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("pr_ek", bzgf.pr_ek, 9, 0, 19, 8,  NULL, "%8.4lf", CEDIT,
                                 PR_EK_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("a_bz2_txt", "Artikeltext2", 0, 0, 31, 7,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("a_bz2", bzgf.a_bz2, 25, 0, 31, 8,  NULL, "", CREADONLY,
                                 A_BZ2_CTL, Font, 0, 0),
                     new CFIELD ("best_txt2_txt", "Bestellext2", 0, 0, 60, 7,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("best_txt2", bzgf.best_txt2, 25, 0, 60, 8,  NULL, "", CEDIT,
                                 BEST_TXT2_CTL, Font, 0, 0),
                     new CFIELD ("me_kz_txt", "Mengen-KZ",  0, 0, 2, 9,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("me_kz", bzgf.me_kz, 17, 5, 2,10,  NULL, "", CCOMBOBOX,
                                 ME_KZ_CTL, Font, 0,    CBS_DROPDOWNLIST |
                                                        WS_VSCROLL),

                     new CFIELD ("min_best_txt", "Inhalt", 0, 0, 19, 9,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("min_best", bzgf.min_best, 8, 0, 19, 10,  NULL, "%7.3lf", CEDIT,
                                 MIN_BEST_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("me_einh_ek_txt", "Einheit", 0, 0, 31, 9,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("me_einh_ek", bzgf.me_einh_ek, 3, 0, 31, 10,  NULL, "%2d", CEDIT,
                                 ME_EINH_EK_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("me_einh_ek_choise", "", 2, 0, 34, 10, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("me_einh_ek_bez", bzgf.me_einh_ek_bez, 9, 0, 37, 10,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, 0),
                     new CFIELD ("lief_zeit_txt", "Lieferzeit", 0, 0, 50, 9,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("lief_zeit", bzgf.lief_zeit, 5, 0, 50, 10,  NULL, "%4d", CEDIT,
                                 LIEF_ZEIT_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("lief_rht_txt", "Lieferrythmus", 0, 0, 60, 9,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("lief_rht", bzgf.lief_rht, 3, 0, 60, 10,  NULL, "", CEDIT,
                                 LIEF_RHT_CTL, Font, 0, ES_UPPERCASE),
                     new CFIELD ("lief_rht_choise", "", 2, 0, 63,10, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("lief_rht_bez", bzgf.lief_rht_bez, 9, 0, 66, 10,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, 0),
                     NULL,
};

CFORM fPos (29, _fPos);

CFIELD *_fList [] = {
       			     new CFIELD ("bzgLst", "",
				                     85,11, 1,12, NULL, "", CLISTBOX,
												  BZG_LST_CTL, Font, 0, 0),
                     NULL,
};

CFORM fList (1, _fList);
    

CFIELD *_fFoot [] = {
                     new CFIELD ("ok",      "    OK    ", 10, 0, 0, 0,  NULL, "", 
                                 CBUTTON,
                                 OK_CTL, Font, 0, 0),
                     new CFIELD ("cancel", " Abbruch  ", 10, 0,12, 0,  NULL, "", 
                                 CBUTTON,
                                 CANCEL_CTL, Font, 0, 0),
                     NULL,
};


CFORM fFoot (2, _fFoot);


CFIELD *_fBzg [] = {
                     new CFIELD ("fBzg1", (CFORM *) &fHead, 0, 0, 0, 0, NULL, "", CFFORM,
                                   HEADCTL, Font, 0, 0),
                     new CFIELD ("fBzg2", (CFORM *) &fPos,  0, 0, 0, 0, NULL, "", CFFORM,
                                   POSCTL, Font, 0, 0),
                     new CFIELD ("fBzg3", (CFORM *) &fFoot, 0, 0, -1, -2, NULL, "", CFFORM,
                                   FOOTCTL, Font, 0, 0),
                     NULL,                      
};

CFORM fBzg (3, _fBzg);

CFIELD *_fBzg0 [] = { 
                     new CFIELD ("fBzg", (CFORM *) &fBzg, 0, 0, -1, -1, NULL, "", CFFORM,
                                   500, Font, 0, 0),
                     NULL,
};
CFORM fBzg0 (1, _fBzg0);

LiefBzgWork LiefBzgDlg::liefBzgWork;
int LiefBzgDlg::EnterMode = ENTERHEAD;
int LiefBzgDlg::ListRows = 0;
SEARCHA LiefBzgDlg::SearchA;
SEARCHLIEF_BEST LiefBzgDlg::SearchLief_best;
BOOL LiefBzgDlg::InsMode = FALSE;
BOOL LiefBzgDlg::WriteOK = FALSE;
double LiefBzgDlg::saveda = 0.0;
char *LiefBzgDlg::HelpName = "18510.cmd";
LiefBzgDlg *LiefBzgDlg::ActiveLiefBzg = NULL;
BOOL LiefBzgDlg::QuikEnter = FALSE;
HBITMAP LiefBzgDlg::SelBmp;
int LiefBzgDlg::MeKzDefault = 1;



char *LiefBzgDlg::MeKzComboOrg[] = {"Bestelleinheit", 
                                 "Verkaufseinheit" , 
                                  NULL};

char *LiefBzgDlg::MeKzCombo0[] = {"Bestelleinheit", 
                                  NULL};

char *LiefBzgDlg::MeKzCombo1[] = {"Verkaufseinheit" , 
                                   NULL};

char **LiefBzgDlg::MeKzCombo = MeKzComboOrg; 

void LiefBzgDlg::SetListDimension (int cx, int cy)
{
         _fList[0]->SetCX (cx);
         _fList[0]->SetCY (cy);
}

void LiefBzgDlg::DiffListDimension (int cx, int cy)
{
         _fList[0]->SetCX (_fList[0]->GetCXorg () + cx);
         _fList[0]->SetCY (_fList[0]->GetCYorg () + cy);
}


int LiefBzgDlg::ReadBzgMdn (void)
{
         if (syskey == 0) return 0;
         if (syskey == KEY9) return 0;

         fPos.GetText ();
         FromForm (dbheadfields);
         liefBzgWork.GetMdnName (bzgf.mdn_krz,   atoi (bzgf.mdn));
         if (atoi (bzgf.mdn) == 0)
         {
             sprintf (bzgf.fil, "%4d", 0);
             liefBzgWork.GetFilName (bzgf.fil_krz,   atoi (bzgf.mdn), atoi (bzgf.fil));
             Enable (&fBzg, EnableHeadFil, FALSE); 
             fHead.SetCurrentName ("lief");
         }
         else
         {
             if (fHead.GetCfield ("fil")->IsDisabled ())
             {
                    Enable (&fBzg, EnableHeadFil, TRUE); 
                    fHead.SetCurrentName ("fil");
             }
         }
         if (syskey == KEYUP)
         {
                    fHead.SetCurrentName ("lief");
         }
         fHead.SetText ();
         return 0;
}

int LiefBzgDlg::ReadBzgFil (void)
{
          
         if (syskey == 0) return 0;
         if (syskey == KEY9) return 0;
         fPos.GetText ();
         FromForm (dbheadfields);
         liefBzgWork.GetFilName (bzgf.fil_krz,   atoi (bzgf.mdn), atoi (bzgf.fil));
         fHead.SetText ();
         return 0;
}

int LiefBzgDlg::ReadBzgLief (void)
{
        
         if (DLG::MousePressed)
         {
             return FALSE;
         }

         if (syskey != KEYCR)
         {
             return FALSE;
         }

         
         if (syskey == 0) return 0;
         if (syskey == KEY9) return 0;

         fHead.GetText ();
         if (bzgf.lief[0] == 0)
         {
             return 0;
         }
         FromForm (dbheadfields);
         if (liefBzgWork.GetLiefName (bzgf.lief_krz, lief_bzg.mdn, lief_bzg.lief) == 100)
         {
             disp_mess ("Lieferant ist nicht angelegt", 2);
             fHead.SetCurrentName ("lief");
             return TRUE;
         }

         fHead.SetText ();
         return 0;
}

int LiefBzgDlg::EnterPos (void)
{
         switch (syskey)
         {
               case KEYDOWN :
               case KEYTAB :
                   fHead.SetCurrentName ("mdn");
                   return TRUE;
         }
        
         if (DLG::MousePressed)
         {
             return FALSE;
         }

         if (syskey != KEYCR)
         {
             return FALSE;
         }
         
         fHead.GetText ();

         if (bzgf.default_lief_kz[0] != 'J')
         {
             strcpy (bzgf.default_lief_kz, "N");
             fHead.GetCfield ("default_lief_kz")->SetText ();
         }
         liefBzgWork.SetDefaultLiefKz (bzgf.default_lief_kz);

         if (bzgf.lief[0] == 0)
         {
             return FALSE;
         }

         FromForm (dbheadfields);

         FillListRows ();
         if (ListRows > 0)
         {
             FillEnterList (0);
             ToolBar_SetState (ActiveDlg->GethwndTB (),   IDM_DELALL, TBSTATE_ENABLED);
             EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELETE,   MF_ENABLED);
             EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELALL,   MF_ENABLED);
             EnableMenuItem (ActiveDlg->GethMenu (),      IDM_LIEFMEBEST,   MF_ENABLED);
             if (ActiveLiefBzg->GetToolbar2 () != NULL)
             {
                 ((ColButton *)ActiveLiefBzg->GetToolbar2 ()->GetCfield ("del")->GetFeld ())->bmp = 
                     LoadBitmap (hInstance, "DEL");
                 if  (ActiveLiefBzg->GetToolbar2 ()->GetCfield ("meeinh") != NULL)
                 {
                      ActiveLiefBzg->GetToolbar2 ()->GetCfield ("meeinh")->Enable (TRUE);
                 }
                 ActiveLiefBzg->GetToolbar2 ()->display ();
             }
         }
         ReadBzgA ();
         Enable (&fBzg, EnableHead, FALSE); 
         if (QuikEnter)
         {
              Enable (&fBzg, EnableQuik,  TRUE); 
         }
         else
         {
              Enable (&fBzg, EnablePos,  TRUE); 
         }
         fPos.SetCurrentName ("a");
         liefBzgWork.BeginWork ();

         return 0;
}

int LiefBzgDlg::TestLiefKz (void)
{
         
         fPos.GetText ();

         if (bzgf.lief_kz[0] != 'J')
         {
             strcpy (bzgf.lief_kz, "N");
             fPos.GetCfield ("lief_kz")->SetText ();
         }
         return 0;
}

int LiefBzgDlg::ListChanged (WPARAM wParam, LPARAM lParam)
{
        int idx;
        char me_kz[5];

        if (LOWORD (wParam) != BZG_LST_CTL) return 0; 
        InsMode = FALSE;
        if (WriteOK) return 0;

        fBzg.GetText ();
        if (ratod (bzgf.a) == 0.0) return 0;
        FromForm (dbfields); 
        if (MeKzDefault == 2)
        {
                 sprintf (me_kz, "%d", 0);
        }
        else if (MeKzDefault == 3)
        {
                 sprintf (me_kz, "%d", 1);
        }
        else
        {
                 sprintf (me_kz, "%d", fPos.GetCfield ("me_kz")->GetComboPos ());
        }
        liefBzgWork.WriteLiefmebest (me_kz);
        if (lief.waehrung == 2)
        {
                FromForm (dbfieldseuro); 
                lief_bzg.pr_ek = lief_bzg.pr_ek * _mdn.konversion;
        }
        else
        {
                lief_bzg.pr_ek_eur = lief_bzg.pr_ek / _mdn.konversion;
        }
        strcpy (lief_bzg.me_kz, me_kz);
        liefBzgWork.WriteA ();

        idx = SendMessage (_fList[0]->GethWnd (), LB_GETCURSEL, 0, 0l);
        if (idx != -1)
        {
            FillEnterList (idx);
        }
        return TRUE;
}

void LiefBzgDlg::FillEnterList (int idx)
{
         char text [512];
         int anz;
         HWND lBox;

         lBox = _fList[0]->GethWnd ();
         if (lBox == NULL) return;

	     SendMessage (lBox, LB_GETTEXT, idx, (LPARAM) text);
         anz = wsplit (text, " ");
         if (anz == 0) return;
         sprintf (bzgf.a, "%13.0lf", ratod (wort[0]));
         fPos.SetText ();
         ReadBzgA ();
}

int LiefBzgDlg::SaveA (void)
{
         saveda = ratod (bzgf.a);
         return 0;
}
         
int LiefBzgDlg::ReadBzgA (void)
{
         int dsqlstatus;
         char wert[5];
          
         fPos.GetText ();
         if (ratod (bzgf.a) == 0.0)
         {
             return 0;
         }

         WriteOK = FALSE;
         dsqlstatus = liefBzgWork.ReadA 
             (lief_bzg.mdn, lief_bzg.fil, bzgf.lief, ratod (bzgf.a),bzgf.lief_best);
         ToForm (dbfields); 
         if (lief.waehrung == 2)
         {
             ToForm (dbfieldseuro);
         }
         lief_bzg.mdn = atoi (bzgf.mdn);  
         lief_bzg.fil = atoi (bzgf.fil);  
         sprintf (lief_bzg.lief, "%s",  bzgf.lief);  
         sprintf (wert, "%d", lief_bzg.me_einh_ek);

         liefBzgWork.GetPtab (bzgf.me_einh_ek_bez, "me_einh_ek", wert);
         liefBzgWork.GetPtab (bzgf.lief_rht_bez  , "lief_rht",   lief_bzg.lief_rht);
         if (dsqlstatus == 0)
         {
             fPos.GetCfield ("me_kz")->SetPosCombo (atoi (lief_bzg.me_kz));
         }
         else
         {
             strcpy (bzgf.me_kz, bzgf.default_me_kz);
         }
         fPos.SetText ();
         if (dsqlstatus == 100)
         {
             InsertRow ();
         }
         else
         {
              SelectRow ();
         }
         return 0;
}

int LiefBzgDlg::GetPtab (void)
{
         char wert [5];
         CFIELD *Cfield;


         fPos.GetText ();


         
		 Cfield = ActiveDlg->GetCurrentCfield ();
         if (strcmp (Cfield->GetName (), "me_einh_ek") == 0)
         {
             sprintf (wert, "%d", atoi (bzgf.me_einh_ek));
             liefBzgWork.GetPtab (bzgf.me_einh_ek_bez, "me_einh_ek", wert);
             fPos.GetCfield ("me_einh_ek_bez")->SetText ();         
         }
         else if (strcmp (Cfield->GetName (), "lief_rht") == 0)
         {
             sprintf (wert, "%s", bzgf.lief_rht);
             liefBzgWork.GetPtab (bzgf.lief_rht_bez, "lief_rht", wert);
             fPos.GetCfield ("lief_rht_bez")->SetText (); 
             if (EnterMode == ENTERHEAD && syskey == KEYCR) 
             {
                    if (_fList[0]->GethWnd () != NULL)
                    {
                           PostMessage (ActiveDlg->GethWnd (), WM_KEYDOWN, VK_DOWN, 0l);
                           return 1;
                    } 
             }
         }
         else if (QuikEnter && strcmp (Cfield->GetName (), "min_best") == 0)
         {
             if (EnterMode == ENTERHEAD && syskey == KEYCR) 
             {
                    if (_fList[0]->GethWnd () != NULL)
                    {
                           PostMessage (ActiveDlg->GethWnd (), WM_KEYDOWN, VK_DOWN, 0l);
                           return 1;
                    } 
             }
         }
         return 0;
}


int LiefBzgDlg::TestMinBest (void)
{
         if (QuikEnter)
         {
             if (EnterMode == ENTERHEAD && syskey == KEYCR) 
             {
                    if (_fList[0]->GethWnd () != NULL)
                    {
                           PostMessage (ActiveDlg->GethWnd (), WM_KEYDOWN, VK_DOWN, 0l);
                           return 1;
                    } 
             }
         }
         return 0;
}
        

int LiefBzgDlg::WrPrEk (void)
{
         fPos.GetCfield ("pr_ek")->GetText ();
         fPos.GetCfield ("pr_ek")->SetText ();
         return 0;
}


ItProg *LiefBzgDlg::BzgAfter [] = {
                                   new ItProg ("mdn",         ReadBzgMdn),
                                   new ItProg ("fil",         ReadBzgFil),
                                   new ItProg ("lief",        ReadBzgLief),
/*
                                   new ItProg ("default_lief_kz", 
                                                              EnterPos),
*/
                                   new ItProg ("a",           ReadBzgA),
                                   new ItProg ("pr_ek",       WrPrEk),
                                   new ItProg ("me_einh_ek",  GetPtab),
                                   new ItProg ("lief_rht",    GetPtab),
                                   new ItProg ("lief_kz",     TestLiefKz),
                                   new ItProg ("min_best",    TestMinBest),
                                   NULL,
};

ItProg *LiefBzgDlg::BzgBefore [] = {new ItProg ("a",           SaveA),
                                    NULL,
};

ItFont *LiefBzgDlg::BzgFont [] = {
                                  new ItFont ("a_txt",           &ltgrayfont),
                                  new ItFont ("a_bz1_txt",       &ltgrayfont),
                                  new ItFont ("me_kz_txt",       &ltgrayfont),
                                  NULL
};

FORMFIELD *LiefBzgDlg::dbheadfields [] = {
         new FORMFIELD ("mdn",       bzgf.mdn,       (short *)   &lief_bzg.mdn,       FSHORT,   NULL),
         new FORMFIELD ("fil",       bzgf.fil,       (short *)   &lief_bzg.fil,       FSHORT,   NULL),
         new FORMFIELD ("lief",      bzgf.lief,      (char *)    lief_bzg.lief,       FCHAR,    NULL),
         NULL,
};

FORMFIELD *LiefBzgDlg::dbfields [] = {
         new FORMFIELD ("lief_kz",   bzgf.lief_kz,   (char *)   lief_bzg.lief_kz,     FCHAR,    NULL),
         new FORMFIELD ("a",         bzgf.a,         (double *) &lief_bzg.a,          FDOUBLE,  NULL),
         new FORMFIELD ("a_bz1",     bzgf.a_bz1,     (char *)   _a_bas.a_bz1,         FCHAR,    NULL),
         new FORMFIELD ("a_bz2",     bzgf.a_bz2,     (char *)   _a_bas.a_bz2,         FCHAR,    NULL),
         new FORMFIELD ("best_txt1", bzgf.best_txt1, (char *)   lief_bzg.best_txt1,   FCHAR,    NULL),
         new FORMFIELD ("best_txt2", bzgf.best_txt2, (char *)   lief_bzg.best_txt2,   FCHAR,    NULL),
         new FORMFIELD ("lief_best", bzgf.lief_best, (char *)   lief_bzg.lief_best,   FCHAR,    NULL),
         new FORMFIELD ("pr_ek",     bzgf.pr_ek,     (double *) &lief_bzg.pr_ek,      FDOUBLE, "%8.4lf"),
         new FORMFIELD ("min_best",  bzgf.min_best,  (double *) &lief_bzg.min_best,   FDOUBLE, "%7.3lf"),
         new FORMFIELD ("me_einh_ek",bzgf.me_einh_ek,(short *)  &lief_bzg.me_einh_ek, FSHORT,  "%2d"),
         new FORMFIELD ("lief_zeit", bzgf.me_einh_ek,(short *)  &lief_bzg.me_einh_ek, FSHORT,  "%2d"),
         new FORMFIELD ("lief_rht",  bzgf.lief_rht,  (char *)   lief_bzg.lief_rht,    FCHAR,  ""),
         NULL,
};


FORMFIELD *LiefBzgDlg::dbfieldseuro [] = {
         new FORMFIELD ("pr_ek",     bzgf.pr_ek,     (double *) &lief_bzg.pr_ek_eur,      FDOUBLE, "%8.4lf"),
         NULL,
};


char *LiefBzgDlg::EnableHead [] = {
                                "mdn",
                                "mdn_choise",
                                "fil",
                                "fil_choise",
                                "lief",
                                "lief_choise",
                                "default_lief_kz",
                                "default_me_kz",
                                 NULL
};

char *LiefBzgDlg::EnableHeadFil [] = {
                                     "fil",
                                     NULL
};

char *LiefBzgDlg::EnablePosA[] = {"a",
                                  "a_choise", 
                                  NULL,
};

char *LiefBzgDlg::EnablePos[] = {"a",
                                 "lief_kz", 
                                 "best_txt1",
                                 "best_txt2",
                                 "lief_best",
                                 "me_kz",
                                 "pr_ek",
                                 "min_best",
                                 "me_einh_ek",
                                 "lief_zeit",
                                 "lief_rht",
                                  NULL,
};

char *LiefBzgDlg::EnableQuik[] = {"a",
                                  "lief_best",
                                  "pr_ek",
                                  "min_best",
                                  NULL,
};

                          
LiefBzgDlg::LiefBzgDlg (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel) :
         DLG (x, y, cx, cy, Caption, Size, Pixel)
{
          
             BorderType = HIGHCOLBORDER;
             Init (x, y, cx, cy, Caption, Size, Pixel);
}

LiefBzgDlg::LiefBzgDlg (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel, 
                  int BorderType) :
         DLG (x, y, cx, cy, Caption, Size, Pixel)
{
          
             this->BorderType = BorderType;
             Init (x, y, cx, cy, Caption, Size, Pixel);
}

LiefBzgDlg::LiefBzgDlg (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel, 
                  int BorderType, int EnterM) :
         DLG (x, y, cx, cy, Caption, Size, Pixel)
{
          
             this->BorderType = BorderType;
             EnterMode = EnterM;
             Init (x, y, cx, cy, Caption, Size, Pixel);
}


void LiefBzgDlg::Init (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel)
{
             PROG_CFG ProgCfg ("18510");
             char cfg_v [256];
             int i;
             int xfull, yfull;

//             Work.Prepare ();


             if (ProgCfg.GetCfgValue ("MeKzDefault", cfg_v) == TRUE)
             {
                    MeKzDefault = atoi (cfg_v);
             }

             ActiveLiefBzg = this; 
             xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
             yfull = GetSystemMetrics (SM_CYFULLSCREEN);        


             QuikEnter = FALSE;
             StdSize = Size;
             dlgfont.FontHeight    = Size;
             dlgposfont.FontHeight = Size;
             ltgrayfont.FontHeight = Size;
             cpfont.FontHeight     = cpsize;


             if (xfull < 1000) 
             {
                 dlgfont.FontHeight = 90;
                 dlgposfont.FontHeight = 90;
                 ltgrayfont.FontHeight = 90;
                 checkfonth.FontHeight = 90;
                 cpfont.FontHeight = 150;
             }

             if (xfull < 800) 
             {

                 dlgfont.FontHeight = 70;
                 dlgposfont.FontHeight = 70;
                 ltgrayfont.FontHeight = 70;
                 checkfonth.FontHeight = 70;
                 cpfont.FontHeight = 120;
             }

             fHead.SetFieldanz ();
             fPos.SetFieldanz ();
             fFoot.SetFieldanz ();
             fBzg.SetFieldanz ();

             fHead.GetCfield ("mdn_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fHead.GetCfield ("fil_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fHead.GetCfield ("lief_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));

             fHead.GetCfield ("mdn_choise")->SetTabstop (FALSE); 
             fHead.GetCfield ("fil_choise")->SetTabstop (FALSE); 
             fHead.GetCfield ("lief_choise")->SetTabstop (FALSE); 

             fPos.GetCfield ("a_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fPos.GetCfield ("me_einh_ek_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fPos.GetCfield ("lief_rht_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));

             fPos.GetCfield ("a_choise")->SetTabstop (FALSE); 
             fPos.GetCfield ("lief_rht_choise")->SetTabstop (FALSE); 
             fPos.GetCfield ("me_einh_ek_choise")->SetTabstop (FALSE); 

             for (i = 0; i < fHead.GetFieldanz (); i ++)
             {
//                       fHead.GetCfield ()[i]->SetFont (&dlgfont); 
//                       fHead.GetCfield ()[i]->SetPosFont (&dlgposfont); 
             }

             SetTmFont (&dlgfont);
             for (i = 0; BzgBefore[i] != NULL; i ++)
             {
                 BzgBefore[i]->SetBefore (&fBzg);
             }

             for (i = 0; BzgAfter[i] != NULL; i ++)
             {
                 BzgAfter[i]->SetAfter (&fBzg);
             }


             if (BorderType == HIGHCOLBORDER)
             {

                for (i = 0; BzgFont[i] != NULL; i ++)
                {
                 BzgFont[i]->SetFont (&fBzg);
                }

                fPos.GetCfield ("pos_frame")->SetBorder 
                 (new FILLEDBORDER (BLACKCOL, WHITECOL, HIGH, 0, LTGRAYCOL)); 
             }
             else if (BorderType == LOWCOLBORDER)
             {

                for (i = 0; BzgFont[i] != NULL; i ++)
                {
                 BzgFont[i]->SetFont (&fBzg);
                }
                fPos.GetCfield ("pos_frame")->SetBorder 
                 (new FILLEDBORDER (BLACKCOL, WHITECOL, LOW, 0, LTGRAYCOL)); 
             }
             else if (BorderType == HIGHBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, HIGH); 
             }
             else if (BorderType == LOWBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, LOW); 
             }
             else if (BorderType == RAISEDBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, RAISED); 
             }
             else if (BorderType == LINEBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, LINE); 
             }

             
             if (MeKzDefault == 2)
             {
                     MeKzCombo = MeKzCombo0;
             }
             else if (MeKzDefault == 3)
             {
                     MeKzCombo = MeKzCombo1;
             }
             fPos.GetCfield ("me_kz")->SetCombobox (MeKzCombo); 
             strcpy (bzgf.me_kz, MeKzComboOrg [MeKzDefault % 2]);

//             fPos.GetCfield ("me_kz")->FillComboBox (); 
             if (EnterMode != ENTERHEAD)
             {
                     fHead.RemoveCfield ("default_lief_kz");
                     fHead.RemoveCfield ("default_lief_kz_txt");
                     fHead.RemoveCfield ("default_me_kz");
//                     fHead.RemoveCfield ("default_lief_kz_txt");
                     FillHeadfields ();
                     Enable (&fBzg, EnableHead, FALSE); 
                     Enable (&fBzg, EnablePos,  TRUE); 
                     SetDialog (&fBzg);
             }
             else
             {
                     fHead.GetCfield ("default_me_kz")->SetCombobox (MeKzCombo); 
                     strcpy (bzgf.default_me_kz, MeKzComboOrg [MeKzDefault % 2]);
//                     fHead.GetCfield ("default_me_kz")->FillComboBox (); 
                     strcpy (bzgf.default_lief_kz, "N");
                     fBzg.InsertCfieldAt (_fList[0], "fBzg3");
                     Enable (&fBzg, EnableHead, TRUE); 
                     Enable (&fBzg, EnablePos,  FALSE);
                     fBzg.RemoveCfield ("fBzg3");
 			         _fList[0]->Setchsize (0, 1);
                     SetDialog (&fBzg0);
             }
             if (EnterMode == ENTERADATA)
             {
                     Enable (&fBzg, EnablePosA, FALSE); 
                     SetDialog (&fBzg);
             }
}


void LiefBzgDlg::FillHeadfields (void)
{
        char wert [5];
        int dsqlstatus;

        strcpy (bzgf.default_me_kz, MeKzComboOrg [MeKzDefault % 2]);
        strcpy (bzgf.me_kz, bzgf.default_me_kz);
        memcpy (&bzgf, &bzgf_null, sizeof (struct BZGF));
        sprintf (bzgf.mdn,  "%4d", lief_bzg.mdn);  
        sprintf (bzgf.fil,  "%4d", lief_bzg.fil);  
        sprintf (bzgf.lief, "%s",  lief_bzg.lief);  
        sprintf (bzgf.a,    "%13.0lf", lief_bzg.a);  
        strcpy (bzgf.lief_best, lief_bzg.lief_best);
        dsqlstatus = liefBzgWork.ReadA (lief_bzg.mdn, lief_bzg.fil, bzgf.lief, lief_bzg.a,bzgf.lief_best); 
        if (dsqlstatus == 0)
        {
               sprintf (bzgf.mdn,  "%4d", lief_bzg.mdn);  
               sprintf (bzgf.fil,  "%4d", lief_bzg.fil);  
               fPos.GetCfield ("me_kz")->SetPosCombo (atoi (lief_bzg.me_kz));
        }
        else
        {
               lief_bzg.mdn = atoi (bzgf.mdn);  
               lief_bzg.fil = atoi (bzgf.fil);  
        }

        liefBzgWork.GetMdnName (bzgf.mdn_krz,   atoi (bzgf.mdn));
        liefBzgWork.GetFilName (bzgf.fil_krz,   atoi (bzgf.mdn), atoi (bzgf.fil));
        liefBzgWork.GetLiefName (bzgf.lief_krz, lief_bzg.mdn, lief_bzg.lief);

        sprintf (wert, "%d", lief_bzg.me_einh_ek);
        liefBzgWork.GetPtab (bzgf.me_einh_ek_bez, "me_einh_ek", wert);
        liefBzgWork.GetPtab (bzgf.lief_rht_bez  , "lief_rht",   lief_bzg.lief_rht);
        ToForm (dbfields); 
        sprintf (lief_bzg.lief, "%s",  bzgf.lief);  
        fPos.GetCfield ("me_kz")->SetPosCombo (atoi (lief_bzg.me_kz));
}


BOOL LiefBzgDlg::OnKeyReturn (void)
{
        HWND hWnd;
        
        hWnd = GetFocus ();
        if (hWnd == fFoot.GetCfield ("cancel")->GethWnd ())
        {
            return OnKey5 ();
        }
        else if (hWnd == fFoot.GetCfield ("ok")->GethWnd ())
        {
            return OnKey12 ();
        }
        else if (EnterMode != ENTERHEAD) 
        {
            return FALSE;
        }
        else if (hWnd == fWork->GetCfield ("default_me_kz")->GethWnd ())
        {
            syskey = KEYCR;
            EnterPos ();
            return TRUE;
        }
        return FALSE;
}

int LiefBzgDlg::WriteRow (void)
{
        char me_kz [5];

        InsMode = FALSE;
        if (WriteOK) return 0;
        fBzg.GetText ();
        if (ratod (bzgf.a) == 0.0) return 0;
        FromForm (dbfields); 
        if (MeKzDefault == 2)
        {
                 sprintf (me_kz, "%d", 0);
        }
        else if (MeKzDefault == 3)
        {
                 sprintf (me_kz, "%d", 1);
        }
        else
        {
                 sprintf (me_kz, "%d", fPos.GetCfield ("me_kz")->GetComboPos ());
        }
        liefBzgWork.WriteLiefmebest (me_kz);
        if (lief.waehrung == 2)
        {
                FromForm (dbfieldseuro); 
                lief_bzg.pr_ek = lief_bzg.pr_ek * _mdn.konversion;
        }
        else
        {
                lief_bzg.pr_ek_eur = lief_bzg.pr_ek / _mdn.konversion;
        }

        strcpy (lief_bzg.me_kz, me_kz);
        liefBzgWork.WriteA ();
        FillRow ();
        WriteOK = FALSE;
        return 0;
}

int LiefBzgDlg::DeleteLief (void)
{
         HWND lBox;
          
        if (EnterMode != ENTERHEAD) return 0;
        lBox = _fList[0]->GethWnd ();
        if (lBox == NULL) return 0;
        
        if (abfragejn (hWnd, "Alle Position l�schen ?", "J") == 0)
        {
            return 0;
        }

        liefBzgWork.DeleteLief ();

        ToolBar_SetState (ActiveDlg->GethwndTB (),   IDM_DELALL,   TBSTATE_INDETERMINATE);
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELETE,   MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELALL,   MF_GRAYED);
        if (ActiveLiefBzg->GetToolbar2 () != NULL)
        {
                 ((ColButton *)ActiveLiefBzg->GetToolbar2 ()->GetCfield ("del")->GetFeld ())->bmp = 
                     LoadBitmap (hInstance, "DELIA");
                 ActiveLiefBzg->GetToolbar2 ()->display ();
        }
        liefBzgWork.InitRec ();
        FromForm (dbheadfields);
        ToForm (dbfields);
        fPos.SetText ();
        InvalidateRect (lBox, NULL, TRUE);
        UpdateWindow (lBox);
        fBzg.SetCurrentName ("a");
        SendMessage (lBox, LB_RESETCONTENT, 0, 0l);
        ListRows = 0;
        return 0;
}
         

int LiefBzgDlg::DeleteRow (void)
{
        HWND lBox;
        int idx;
        char buffer [512];
        int anz;
        int dsqlstatus;
        char wert [5];

        lBox = _fList[0]->GethWnd ();
        if (lBox == NULL) return 0;

        idx = SendMessage (_fList[0]->GethWnd (), LB_GETCURSEL, 0, 0l);
        InsMode = FALSE;
        fBzg.GetText ();
        FromForm (dbfields); 
        liefBzgWork.DeleteA ();

        ScrollList (lBox, idx, SCDELETE);
        ListRows = SendMessage (_fList[0]->GethWnd (), LB_GETCOUNT, 0, 0l);
        if (ListRows == 0)
        {
               ToolBar_SetState (ActiveDlg->GethwndTB (),   IDM_DELALL, TBSTATE_INDETERMINATE);
               EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELETE,   MF_GRAYED);
               EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELALL,   MF_GRAYED);
               if (ActiveLiefBzg->GetToolbar2 () != NULL)
               {
                 ((ColButton *)ActiveLiefBzg->GetToolbar2 ()->GetCfield ("del")->GetFeld ())->bmp = 
                     LoadBitmap (hInstance, "DELIA");
                 ActiveLiefBzg->GetToolbar2 ()->display ();
               }
               liefBzgWork.InitRec ();
               FromForm (dbheadfields);
               ToForm (dbfields);
               fPos.SetText ();
               InvalidateRect (lBox, NULL, TRUE);
               UpdateWindow (lBox);
               return 0;
        }
        if (idx > ListRows - 1) idx --;
        SendMessage (lBox, LB_GETTEXT, idx, (LPARAM) buffer);
        anz = wsplit (buffer, " ");
        if (anz == 0) return 0;

         sprintf (bzgf.a, "%13.0lf", ratod (wort[0]));

         dsqlstatus = liefBzgWork.ReadA 
             (lief_bzg.mdn, lief_bzg.fil, bzgf.lief, ratod (bzgf.a),bzgf.lief_best);

         ToForm (dbfields); 
         sprintf (wert, "%d", lief_bzg.me_einh_ek);

         liefBzgWork.GetPtab (bzgf.me_einh_ek_bez, "me_einh_ek", wert);
         liefBzgWork.GetPtab (bzgf.lief_rht_bez  , "lief_rht",   lief_bzg.lief_rht);
         fPos.GetCfield ("me_kz")->SetPosCombo (atoi (lief_bzg.me_kz));
         fPos.SetText ();

         InvalidateRect (lBox, NULL, TRUE);
         UpdateWindow (lBox);
         return 0;
}

void LiefBzgDlg::FillRow (void)
{
        char buffer [512];
        int idx;

        if (_fList[0]->GethWnd () == NULL) return;

        idx = SendMessage (_fList[0]->GethWnd (), LB_GETCURSEL, 0, 0l);
        if (idx < 0) return;

        liefBzgWork.FillRow (buffer);

        SendMessage (_fList[0]->GethWnd (), LB_INSERTSTRING, idx,  
                           (LPARAM) (char *) buffer);
}

void LiefBzgDlg::InsertRow (void)
{
        int i;
        int anz;
        char buffer [512];
        HWND lBox;
        double a;

        if (InsMode) return;

        lBox = _fList[0]->GethWnd ();
        if (lBox == NULL) return;


        ListRows = SendMessage (lBox, LB_GETCOUNT, 0, 0l);

        for (i = 0; i < ListRows; i ++)
        {

   	           SendMessage (lBox, LB_GETTEXT, i, (LPARAM) buffer);
               anz = wsplit (buffer, " ");
               if (anz == 0) continue;
               a = ratod (wort[0]);
               if (ratod (bzgf.a) < a) break;
        }
        liefBzgWork.FillRow (buffer);

        ScrollList (lBox, i, SCINSERT);
        InvalidateRect (lBox, NULL, TRUE);
        UpdateWindow (lBox);
        SendMessage (lBox, LB_INSERTSTRING, i, (LPARAM) (char *) buffer);
        SendMessage (lBox, LB_SETCURSEL, (WPARAM) i, (LPARAM) 0l);
        InsMode = TRUE;
}

void LiefBzgDlg::SelectRow (void)
{
        int i;
        int anz;
        char buffer [512];
        HWND lBox;
        int ListPos;
        double a;

        lBox = _fList[0]->GethWnd ();
        if (lBox == NULL) return;


        ListRows = SendMessage (lBox, LB_GETCOUNT, 0, 0l);
        ListPos  = SendMessage (_fList[0]->GethWnd (), LB_GETCURSEL, 0, 0l);
         
        for (i = 0; i < ListRows; i ++)
        {

   	           SendMessage (lBox, LB_GETTEXT, i, (LPARAM) buffer);
               anz = wsplit (buffer, " ");
               if (anz == 0) continue;
               a = ratod (wort[0]);
               if (ratod (bzgf.a) == a) break;
        }
        liefBzgWork.SetOldMeKz (lief_bzg.me_kz);
        if (liefBzgWork.LiefmebestExist ())
        {
               fPos.GetCfield ("min_best")->Enable (FALSE);
        }
        else
        {
               fPos.GetCfield ("min_best")->Enable (TRUE);
        }


                
        if (i == ListPos) return;

        SendMessage (lBox, LB_SETCURSEL, (WPARAM) i, (LPARAM) 0l);
}

BOOL LiefBzgDlg::OnKeyDown (void)
{
        HWND hWnd;


        hWnd == GetFocus ();

        if (hWnd == _fList[0]->GethWnd ())
        {
            return FALSE;
        }
        if (fHead.GetCfield ("lief")->IsDisabled () == FALSE)
        {
            return FALSE;
        }
        if (_fList[0]->GethWnd () == NULL) 
        {
            return FALSE;
        }
//        LockWindowUpdate (hWnd);
        WriteRow ();
        syskey = KEYDOWN;
        SendMessage (_fList[0]->GethWnd (), WM_KEYDOWN, VK_DOWN, 0l);

        if (QuikEnter)
        {
                fPos.SetCurrentName ("lief_best");
        }
        else
        {
                fPos.SetCurrentName ("lief_kz");
        }

        fPos.SetCurrentName ("a");
        fPos.GetCfield ("a")->SetSel ();
        LockWindowUpdate (NULL);
        return TRUE;
}


BOOL LiefBzgDlg::OnKeyUp (void)
{
        HWND hWnd;
        int idx;

        hWnd == GetFocus ();

        if (hWnd == _fList[0]->GethWnd ())
        {
            return FALSE;
        }


        idx = SendMessage (_fList[0]->GethWnd (), LB_GETCURSEL, 0, 0l);

        if (fHead.GetCfield ("lief")->IsDisabled () == FALSE)
        {
            return FALSE;
        }
        if (_fList[0]->GethWnd () == NULL) 
        {
            return FALSE;
        }
        WriteRow ();
/*
        if (idx <= 0) 
        {
            return TRUE;
        }
*/

        LockWindowUpdate (hWnd);
        syskey = KEYUP;
        SendMessage (_fList[0]->GethWnd (), WM_KEYDOWN, VK_UP, 0l);
        if (QuikEnter)
        {
                fPos.SetCurrentName ("lief_best");
        }
        else
        {
                fPos.SetCurrentName ("lief_kz");
        }
        fPos.SetCurrentName ("a");
        LockWindowUpdate (NULL);
        return TRUE;
}


BOOL LiefBzgDlg::OnKey2 ()
{
        return TRUE;
}



BOOL LiefBzgDlg::OnKeyHome ()
{
        return TRUE;
}

BOOL LiefBzgDlg::OnKeyEnd ()
{
        return TRUE;

}
BOOL LiefBzgDlg::OnKey1 ()
{
        syskey = KEY1;
        Help ();
        return TRUE;
}

BOOL LiefBzgDlg::OnKey3 ()
{
        syskey = KEY3;
        return TRUE;
}

BOOL LiefBzgDlg::OnKey4 ()
{
        syskey = KEY4;
        CallInfo ();
        return TRUE;
}

BOOL LiefBzgDlg::OnKey5 ()
{

        syskey = KEY5;
        InsMode = FALSE;
        syskey = KEY5;
        if (fHead.GetCfield ("lief")->IsDisabled () == FALSE)
        {
                  DestroyWindow ();
                  return TRUE;
        }
        else if (EnterMode != ENTERHEAD)
        {
                  DestroyWindow ();
                  return TRUE;
        }
        Enable (&fBzg, EnableHead, TRUE); 
        if (atoi (bzgf.mdn) == 0)
        {
             Enable (&fBzg, EnableHeadFil, FALSE); 
        }
        Enable (&fBzg, EnablePos,  FALSE);
        SendMessage (_fList[0]->GethWnd (), LB_RESETCONTENT, 0, 0l);
        liefBzgWork.RollbackWork ();
        liefBzgWork.InitRec ();
        ToForm (dbfields);
        fPos.SetText ();
        fHead.SetCurrentName ("lief");
        ToolBar_SetState (ActiveDlg->GethwndTB (),   IDM_DELALL,   TBSTATE_INDETERMINATE);
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELETE,   MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELALL,   MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_LIEFMEBEST,MF_GRAYED);
        if (ActiveLiefBzg->GetToolbar2 () != NULL)
        {
                 ((ColButton *)ActiveLiefBzg->GetToolbar2 ()->GetCfield ("del")->GetFeld ())->bmp = 
                     LoadBitmap (hInstance, "DELIA");
                 if  (ActiveLiefBzg->GetToolbar2 ()->GetCfield ("meeinh") != NULL)
                 {
                      ActiveLiefBzg->GetToolbar2 ()->GetCfield ("meeinh")->Enable (FALSE);
                 }
                 ActiveLiefBzg->GetToolbar2 ()->display ();
        }
        return TRUE;
}

BOOL LiefBzgDlg::OnKey12 ()
{
        syskey = KEY12;
        char me_kz [5];

        if (liefBzgWork.TestReadLief () == -243)   //DS gesperrt !!
		{
			disp_mess ("Daten k�nnen nicht gespeichert werden , Datensatz ist von einem anderen Benutzer gesperrt ",1);
			OnKey5 ();
			return TRUE;
		}

        InsMode = FALSE;
        fWork->GetText ();
        if (ratod (bzgf.a) == 0.0) return 0;
        FromForm (dbfields); 
        if (strcmp (lief_bzg.lief_best, " ") <= 0)
        {
               sprintf (lief_bzg.lief_best, "%.0lf", lief_bzg.a);
        }
        if (MeKzDefault == 2)
        {
                 sprintf (me_kz, "%d", 0);
        }
        else if (MeKzDefault == 3)
        {
                 sprintf (me_kz, "%d", 1);
        }
        else
        {
                 sprintf (me_kz, "%d", fPos.GetCfield ("me_kz")->GetComboPos ());
        }
        liefBzgWork.WriteLiefmebest (me_kz);
        if (lief.waehrung == 2)
        {
                FromForm (dbfieldseuro); 
                lief_bzg.pr_ek = lief_bzg.pr_ek * _mdn.konversion;
        }
        else
        {
                lief_bzg.pr_ek_eur = lief_bzg.pr_ek / _mdn.konversion;
        }
        clipped (lief_bzg.lief_best);
        if (strcmp (lief_bzg.lief_best, " ") <= 0)
        {
               sprintf (lief_bzg.lief_best, "%.0lf", lief_bzg.a);
        }
        strcpy (lief_bzg.me_kz, me_kz);
        if (EnterMode != ENTERHEAD)
        {
               if (liefBzgWork.WriteA () == -1)
               {
                   syskey = KEY5;
               }
               DestroyWindow ();
               return TRUE;
        }
        if (fHead.GetCfield ("lief")->IsDisabled ())
        {
               WriteRow ();
               Enable (&fBzg, EnableHead, TRUE); 
               if (atoi (bzgf.mdn) == 0)
               {
                        Enable (&fBzg, EnableHeadFil, FALSE); 
               }
               Enable (&fBzg, EnablePos,  FALSE);
               SendMessage (_fList[0]->GethWnd (), LB_RESETCONTENT, 0, 0l);
               liefBzgWork.CommitWork ();
               liefBzgWork.InitRec ();
               ToForm (dbfields);
               fPos.SetText ();
               fHead.SetCurrentName ("lief");
               ToolBar_SetState (ActiveDlg->GethwndTB (),   IDM_DELALL, TBSTATE_INDETERMINATE);
               EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELETE,   MF_GRAYED);
               EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELALL,   MF_GRAYED);
               EnableMenuItem (ActiveDlg->GethMenu (),      IDM_LIEFMEBEST,MF_GRAYED);
               if (ActiveLiefBzg->GetToolbar2 () != NULL)
               {
                 ((ColButton *)ActiveLiefBzg->GetToolbar2 ()->GetCfield ("del")->GetFeld ())->bmp = 
                     LoadBitmap (hInstance, "DELIA");
                 if  (ActiveLiefBzg->GetToolbar2 ()->GetCfield ("meeinh") != NULL)
                 {
                      ActiveLiefBzg->GetToolbar2 ()->GetCfield ("meeinh")->Enable (FALSE);
                 }
                 ActiveLiefBzg->GetToolbar2 ()->display ();
               }
        }
        return TRUE;
}

BOOL LiefBzgDlg::OnKey8 ()
{
        if (fHead.GetCfield ("lief")->IsDisabled ())
        {
            EnterLiefmebest ();
            return TRUE;
        }
        return FALSE;
}
        

BOOL LiefBzgDlg::OnKeyDelete ()
{
        CFIELD *Cfield;  

        if (EnterMode != ENTERHEAD) return FALSE;

        if (_fList[0]->GethWnd () == NULL) return FALSE;

        ListRows = SendMessage (_fList[0]->GethWnd (), LB_GETCOUNT, 0, 0l);
        if (ListRows == NULL) return FALSE;

        if (abfragejn (hWnd, "Position l�schen ?", "J"))
        {
            DeleteRow ();
        }
        Cfield = ActiveDlg->GetCurrentCfield ();
        if (Cfield != NULL)
        {
            Cfield->SetFocus ();
        }
        return TRUE;
}

BOOL LiefBzgDlg::OnKeyPrior ()
{

        if (vTrack != NULL)
        {
             SendMessage (hWnd, WM_VSCROLL, SB_PAGEUP, 0l);
             return TRUE;
        }
        return FALSE;
}

BOOL LiefBzgDlg::OnKeyNext ()
{
        if (vTrack != NULL)
        {
             SendMessage (hWnd, WM_VSCROLL, SB_PAGEDOWN, 0l);
             return TRUE;
        }
        return FALSE;
}


BOOL LiefBzgDlg::OnKey6 ()
{
        return TRUE;
} 


BOOL LiefBzgDlg::OnKey7 ()
{
        return FALSE;
}


BOOL LiefBzgDlg::Showme_einh_ek (void)
{
             char wert [5]; 

             liefBzgWork.ShowPtab (bzgf.me_einh_ek, "me_einh_ek");
             if (syskey != KEY5)
             {
                   fPos.GetCfield ("me_einh_ek")->SetText ();         
                   sprintf (wert, "%d", atoi (bzgf.me_einh_ek));
                   liefBzgWork.GetPtab (bzgf.me_einh_ek_bez, "me_einh_ek", wert);
                   fPos.GetCfield ("me_einh_ek_bez")->SetText ();         
             }
             fPos.SetCurrentName ("me_einh_ek");
             return TRUE;
}

BOOL LiefBzgDlg::ShowMdn (void)
{
             char mdn_nr [6];
             SEARCHMDN SearchMdn;
 
             SearchMdn.SetParams (DLG::hInstance, ActiveDlg->GethWnd ());
             SearchMdn.Setawin (ActiveDlg->GethMainWindow ());
             SearchMdn.Search ();
             if (SearchMdn.GetKey (mdn_nr) == TRUE)
             {
                 sprintf (bzgf.mdn, "%4d", atoi (mdn_nr));
                 liefBzgWork.GetMdnName (bzgf.mdn_krz,   atoi (bzgf.mdn));
                 fHead.SetText ();
                 fHead.SetCurrentName ("mdn");
             }
             return TRUE;
}


BOOL LiefBzgDlg::ShowFil (void)
{
             char fil_nr [6];
             SEARCHFIL SearchFil;
 
             if (atoi (bzgf.mdn) == 0) return TRUE;

             SearchFil.SetParams (DLG::hInstance, ActiveDlg->GethWnd (), 
                                                  atoi (bzgf.mdn));
             SearchFil.Setawin (ActiveDlg->GethMainWindow ());
             SearchFil.Search ();
             if (SearchFil.GetKey (fil_nr) == TRUE)
             {
                 sprintf (bzgf.fil, "%4d", atoi (fil_nr));
                 liefBzgWork.GetFilName (bzgf.fil_krz,   atoi (bzgf.mdn), atoi (bzgf.fil));
                 fHead.SetText ();
                 fHead.SetCurrentName ("fil");
             }
             return TRUE;
}

BOOL LiefBzgDlg::ShowLief (void)
{
             char lief [17];
             SEARCHLIEF SearchLief;
 
             SearchLief.SetParams (DLG::hInstance, ActiveDlg->GethWnd ()); 
             SearchLief.Setawin (ActiveDlg->GethMainWindow ());
                                                  
             SearchLief.Search ();
             if (SearchLief.GetKey (lief) == TRUE)
             {
                 sprintf (bzgf.lief, "%s", lief);
                 liefBzgWork.GetLiefName (bzgf.lief_krz,  atoi (bzgf.mdn), bzgf.lief);
                 fHead.SetText ();
                 fHead.SetCurrentName ("lief");
             }
             return TRUE;
}


int LiefBzgDlg::ShowA ()
{
        struct SA *sa;

        SearchA.SetParams (DLG::hInstance, ActiveDlg->GethWnd ());
        SearchA.Setawin (ActiveDlg->GethMainWindow ());
        SearchA.SearchA ();
        ActiveDlg->Validate ();
        if (syskey == KEY5)
        {
            return 0;
        }
        sa = SearchA.GetSa ();
        if (sa == NULL)
        {
            return 0;
        }
        strcpy (bzgf.a, sa->a);
        fPos.SetText ();
        ReadBzgA ();
        return 0;
}

int LiefBzgDlg::ShowLief_best ()
{
             char mdn_nr [17];
 
             SearchLief_best.SetParams (DLG::hInstance, ActiveDlg->GethWnd ());
             SearchLief_best.Setawin (ActiveDlg->GethMainWindow ());
             SearchLief_best.Search ();
             if (SearchLief_best.GetKey (mdn_nr) == TRUE)
             {
                 sprintf (bzgf.mdn, "%4d", atoi (mdn_nr));
                 liefBzgWork.GetMdnName (bzgf.mdn_krz,   atoi (bzgf.mdn));
                 fPos.SetText ();
             }
             return TRUE;

}

BOOL LiefBzgDlg::Showlief_rht (void)
{
             char wert [5]; 

             liefBzgWork.ShowPtab (bzgf.lief_rht, "lief_rht");
             if (syskey != KEY5)
             {
                   fPos.GetCfield ("lief_rht")->SetText ();         
                   sprintf (wert, "%s", bzgf.lief_rht);
                   liefBzgWork.GetPtab (bzgf.lief_rht_bez, "lief_rht", wert);
                   fPos.GetCfield ("lief_rht_bez")->SetText ();         
             }
             fPos.SetCurrentName ("lief_rht");
             return TRUE;
}


BOOL LiefBzgDlg::OnKey9 ()
{
         CFIELD *Cfield;
         HWND hWnd;

         syskey = KEY9;
         hWnd = GetFocus ();

         if (hWnd == fPos.GetCfield ("me_einh_ek_choise")->GethWnd ())
         {
             return Showme_einh_ek ();
         }

         if (hWnd == fPos.GetCfield ("lief_rht_choise")->GethWnd ())
         {
             return Showlief_rht ();
         }

         if (hWnd == fHead.GetCfield ("mdn_choise")->GethWnd ())
         {
             return ShowMdn ();
         }

         if (hWnd == fHead.GetCfield ("fil_choise")->GethWnd ())
         {
             return ShowFil ();
         }

         if (hWnd == fHead.GetCfield ("lief_choise")->GethWnd ())
         {
             return ShowLief ();
         }

         if (hWnd == fPos.GetCfield ("a_choise")->GethWnd ())
         {
             return ShowA ();
         }


         Cfield = ActiveDlg->GetCurrentCfield ();

         if (strcmp (Cfield->GetName (), "mdn") == 0)
         {
             return ShowMdn ();
         }

         if (strcmp (Cfield->GetName (), "fil") == 0)
         {
             return ShowFil ();
         }

         if (strcmp (Cfield->GetName (), "lief") == 0)
         {
             return ShowLief ();
         }

         if (strcmp (Cfield->GetName (), "a") == 0)
         {
             return ShowA ();
         }

         if (strcmp (Cfield->GetName (), "me_einh_ek") == 0)
         {
             return Showme_einh_ek ();
         }

         if (strcmp (Cfield->GetName (), "lief_rht") == 0)
         {
             return Showlief_rht ();
         }

         return FALSE;
}

BOOL LiefBzgDlg::OnKey10 ()
{
        return ChangeQuikEnter ();
}

BOOL LiefBzgDlg::OnPaint (HWND hWnd,HDC hdc, UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (fWork && this->hWnd == hWnd)
        {
             PrintMess (MessText);
             fWork->display (hWnd, hdc);
             return TRUE;
        }
        return DLG::OnPaint (hWnd,hdc, msg, wParam,lParam);
}


BOOL LiefBzgDlg::OnActivate (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (LOWORD (wParam) == WA_ACTIVE ||
                LOWORD (wParam) == WA_CLICKACTIVE)
        {
              if (WithTabStops)
              {
                  SetTabFocus (); 
              }
              else
              {
                  fWork->SetFocus ();
              }
              return TRUE;
        }
        return FALSE;
}

BOOL LiefBzgDlg::EnterLiefmebest (void)
{
       MeEinhDlg *Dlg;
       HWND hWnd;

       WriteRow ();
       hWnd = ActiveDlg->GethWnd ();
       liefmebest.mdn = atoi (bzgf.mdn);
       liefmebest.fil = atoi (bzgf.fil);
       strcpy (liefmebest.lief, bzgf.lief);
       liefmebest.a, ratod (bzgf.a);
       Dlg = new MeEinhDlg (-1, -1, 89, 21, "Lieferanten-Einheiten",105, FALSE,
                             RAISEDBORDER, ENTERADATA);
       Dlg->SetStyle (WS_VISIBLE | WS_POPUP | WS_THICKFRAME | WS_CAPTION | WS_SYSMENU);
       Dlg->SetStyleEx (NULL);
       Dlg->SetWinBackground (LTGRAYCOL);
       Dlg->SetNextTopWindow (hMainWindow);
       EnableWindow (hMainWindow, FALSE);
       Dlg->OpenScrollWindow (DLG::hInstance, hWnd);
	   Dlg->ProcessMessages ();
       delete Dlg;
       if (syskey == KEY5)
       {
                 return -1;
       }
       ReadBzgA ();
       return 0; 
}


BOOL LiefBzgDlg::ChangeQuikEnter (void)
{
        ColButton *Cub;

        if (QuikEnter)
        {
            QuikEnter = FALSE;
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_QUIK,   MF_UNCHECKED);
            if (Toolbar2 != NULL && Toolbar2->GetCfield ("quik") != NULL)
            {
                   Cub = (ColButton *) Toolbar2->GetCfield ("quik")->GetFeld ();
                   Cub->bmp = NULL;
//                   Toolbar2->GetCfield ("quik")->destroy ();
                   Toolbar2->GetCfield ("quik")->display ();
            }
            if (fHead.GetCfield ("lief")->IsDisabled ())
            {
                    Enable (&fBzg, EnablePos,  TRUE); 
                    SetCurrentName ("a");
            }
        }
        else
        {
            QuikEnter = TRUE;
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_QUIK,   MF_CHECKED);
            if (Toolbar2 != NULL && Toolbar2->GetCfield ("quik") != NULL)
            {
                    Cub = (ColButton *) Toolbar2->GetCfield ("quik")->GetFeld ();
                    Cub->bmp = SelBmp;
                    Toolbar2->GetCfield ("quik")->display ();
            }
            if (fHead.GetCfield ("lief")->IsDisabled ())
            {
                    Enable (&fBzg, EnablePos,   FALSE); 
                    Enable (&fBzg, EnableQuik,  TRUE); 
                    SetCurrentName ("a");
            }
        }
        if (liefBzgWork.LiefmebestExist ())
        {
              fPos.GetCfield ("min_best")->Enable (FALSE);
        }
        SetCurrentFocus ();
        return TRUE;
}


BOOL LiefBzgDlg::OnCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (fWork)
        {

              if (LOWORD (wParam) == CANCEL_CTL)
              {
                  return OnKey5 ();
              }
              if (LOWORD (wParam) == OK_CTL)
              {
                  return OnKey12 ();
              }
              if (LOWORD (wParam) == IDM_CHOISE)
              {
                  return OnKey9 ();
              }

              if (LOWORD (wParam) == IDM_QUIK)
              {
                  return ChangeQuikEnter ();
              }

              if (LOWORD (wParam) == IDM_LIEFMEBEST)
              {
                  return EnterLiefmebest ();
              }

              if (HIWORD (wParam) == WM_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              if (HIWORD (wParam) == EN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == CBN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == WM_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == LBN_SELCHANGE)
              {
                  return ListChanged (wParam, lParam);
              }
              else if (HIWORD (wParam) == EN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == CBN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == BN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == BN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
        }
        return DLG::OnCommand (hWnd, msg, wParam, lParam);
}


BOOL LiefBzgDlg::OnSysCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        
        if (wParam == SC_CLOSE)
        {
            if (EnterMode == ENTERHEAD && 
                abfragejn (hWnd, "Verarbeitung abbrechen ?", "N") == 0)
            {
                ActiveDlg->SetCurrentFocus ();
                return TRUE;
            }
            syskey = KEY5;
        }
        return DLG::OnSysCommand (hWnd, msg, wParam, lParam); 
}

BOOL LiefBzgDlg::OnDestroy (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{

        if (hWnd == this->hWnd)
        {
             fBzg.RemoveCfield ("bzgLst");
             PostQuitMessage (0);
             fBzg.destroy ();
        }
        return TRUE;
}

void LiefBzgDlg::SetWinBackground (COLORREF Col)
{
          dlgfont.FontBkColor = Col;
          cpfont.FontBkColor = Col;
          DLG::SetWinBackground (Col);
}


void LiefBzgDlg::ToClipboard (void)
{
          CFIELD *Cfield;
          HGLOBAL hb;
          LPVOID p;
          char *text;

          Cfield = GetCurrentCfield ();
          Cfield->GetText ();
          text = (char *) Cfield->GetFeld ();
          if (text == NULL) return;
          hb = GlobalAlloc (GHND, strlen (text) + 1);
          if (hb == NULL) return;
          p = GlobalLock (hb);
          strcpy ((char *) p, text);
          GlobalUnlock (hb);
          OpenClipboard (hWnd);
          EmptyClipboard ();
          SetClipboardData (CF_TEXT, hb);
          CloseClipboard ();
}

void LiefBzgDlg::FromClipboard (void)
{
          CFIELD *Cfield;
          HGLOBAL hb;
          LPVOID p;
          char *text;

          Cfield = GetCurrentCfield ();
          text = (char *) Cfield->GetFeld ();
          if (text == NULL) return;
          if (IsClipboardFormatAvailable (CF_TEXT) == FALSE) return;
          OpenClipboard (hWnd);
          hb = GetClipboardData (CF_TEXT);
          if (hb == NULL)
          {
                 CloseClipboard ();
                 return;
          }
          p = GlobalLock (hb);
          if (strlen ((char *) p) > (unsigned int ) Cfield->GetLength ())
          {
                 GlobalUnlock (hb);
                 CloseClipboard ();
                 return;
          }
          strcpy (text, (char *) p);
          GlobalUnlock (hb);
          CloseClipboard ();
          Cfield->SetText ();
}

void LiefBzgDlg::Help (void)
{
          CFIELD *Cfield;
          HELP *Help; 
          char *Item;

          Cfield = GetCurrentCfield ();
          if (Cfield == NULL) return;

          Item = Cfield->GetName ();

          Help = new HELP (HelpName, Item, hInstance,hMainWindow);
          delete Help;
          Cfield->SetFocus ();
}


void LiefBzgDlg::FillListRows (void)
{
          char buffer [512];
          HWND lBox;
          int dsqlstatus;

          lBox = _fList[0]->GethWnd ();
          if (lBox == NULL) return;
          dsqlstatus = liefBzgWork.ReadLief 
             (lief_bzg.mdn, lief_bzg.fil, bzgf.lief);
          while (dsqlstatus == 0)
          {
                   liefBzgWork.FillRow (buffer);

                   SendMessage (lBox, LB_INSERTSTRING, -1,  
                                 (LPARAM) (char *) buffer);
                   dsqlstatus = liefBzgWork.ReadLief ();
          }
          ListRows = SendMessage (lBox, LB_GETCOUNT, 0, 0l);
          if (ListRows > 0)
          {
                   SendMessage (lBox, LB_SETCURSEL, 0, 0l);
          }
}
           

void LiefBzgDlg::FillListCaption (void)
{
          char buffer [512];
          HWND lBox;

          lBox = _fList[0]->GethWnd ();
          if (lBox == NULL) return;

          sprintf (buffer, "%13s  %-24s  %-16s  %8s   %-24s", 
                           "Artikel", "Bezeichnung", "Bestell-Nr", "EK-Preis",
                           "Bestelltext1");  
          SendMessage (lBox, LB_TITLE, -1,  
                                 (LPARAM) (char *) buffer);
          sprintf (buffer, "%15s%26s%18s%11s%27s", 
                           "1","1", "1", "1", "1");
          SendMessage (lBox, LB_VPOS, 3, (LPARAM) buffer);  
          sprintf (buffer, "0;13 16;24 43;16 59;8 72;24");
          SendMessage (lBox, LB_RPOS, 0, (LPARAM) buffer);  
          sprintf (buffer, "%s %s %s %s %s", "%d","%s","%s","%d","%s");
          SendMessage (lBox, LB_ROWATTR, 0,  
                                 (LPARAM) (char *) buffer);
}


HWND LiefBzgDlg::OpenWindow (HINSTANCE hInstance, HWND hMainWindow)
{
          HWND hWnd;

          Settchar ('|');
          hWnd = DLG::OpenWindow (hInstance, hMainWindow);
          FillListCaption ();
          return hWnd;
}

HWND LiefBzgDlg::OpenScrollWindow (HINSTANCE hInstance, HWND hMainWindow)
{
          HWND hWnd;

          Settchar ('|');
          hWnd = DLG::OpenScrollWindow (hInstance, hMainWindow);
          FillListCaption ();
          return hWnd;
}

void LiefBzgDlg::PrintComment (char *Name)
{
          HELP *Help; 
          char Comment [256];

          Help = new HELP (HelpName, Name, hInstance,hMainWindow, INITCOMM);
          Help->GetItem (Name, Comment);
          PrintMess (Comment);
          delete Help;
}

void LiefBzgDlg::CallInfo (void)
{
          DLG::CallInfo ();
}






