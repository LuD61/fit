#include<stdio.h>
#include<stdarg.h>
#include<string.h>
#include<windows.h>
#include "stdfkt.h"
#include "strfkt.h"
#include "textwork.h" 
#include "dlg.h" 


TextWork::TextWork (char *File) 

{
     FILE *fp;
     char *etc;
     char buffer [512];
     int i;
     int Row = 1;
     int TextCol = 1;
     int anz;
     int updanz = 0;
     int selanz = 0;
     int keyanz = 0;

     Texte   = NULL;
     Felder  = NULL;   
     Namen   = NULL;   
     Feldnamen = NULL;
     Rows = NULL;
     Columns = NULL;
     Length = NULL;
     Selects[0] = NULL;
     Updates[0] = NULL;
     Textlen = 20;
     Wsize[0] = 60;
     Wsize[1] = 10;
     WinColor = GetSysColor (COLOR_3DFACE);

     etc = getenv ("BWSETC");
     if (etc == NULL) return;
     sprintf (buffer, "%s\\%s", etc, File);
     fp = fopen (buffer, "r");
     if (fp == NULL) return;

     headfieldanz = 0;

     while (fgets (buffer, 511, fp))
     {
         cr_weg (buffer);
         if (strupcmp (buffer, "$HEAD",  5) == 0) break;
     }

     while (fgets (buffer, 511, fp))
     {
         if (buffer[0] == '#')
         {
              continue;
         }
         cr_weg (buffer);
         anz = wsplit (buffer, " ");
         if (anz < 2) continue;
         if (strupcmp (wort[0], "Startrow", strlen (wort[0])) == 0)
         {
                    Row = atoi (wort[1]);
         }
         else if (strupcmp (wort[0], "TextCol", strlen (wort[0])) == 0)
         {
                    TextCol = atoi (wort[1]);
         }
         else if (strupcmp (wort[0], "Textlen", strlen (wort[0])) == 0)
         {
                    Textlen = atoi (wort[1]);
         }
         else if (strupcmp (wort[0], "WINCOLOR", strlen (wort[0])) == 0)
         {
                    anz = wsplit (buffer, "{}");
                    if (anz < 2) continue;
                    WinColor = DLG::GetColor (wort [1]);
         }
         else if (strupcmp (wort[0], "WINSIZE", strlen (wort[0])) == 0)
         {
                    anz = wsplit (buffer, "{}");
                    if (anz < 2) continue;
                    strcpy (buffer, wort[1]);
                    anz = wsplit (buffer, ";");
                    if (anz < 2) continue;
                    Wsize[0] = atoi (wort[0]);
                    Wsize[1] = atoi (wort[1]);
         }
         else if (strupcmp (wort[0], "DBSELECT", 8) == 0)
         {
                    anz = wsplit (buffer, "{}");
                    if (anz < 2) continue;
                    Selects [selanz] = new char [0x1000];
                    if (Selects [selanz] == NULL) continue;
                    strcpy (Selects [selanz], wort[1]);
                    selanz ++;
         }
         else if (strupcmp (wort[0], "DBUPDATE", 8) == 0)
         {
                    anz = wsplit (buffer, "{}");
                    if (anz < 2) continue;
                    Updates [updanz] = new char [0x1000];
                    if (Updates [updanz] == NULL) continue;
                    strcpy (Updates [updanz], wort[1]);
                    updanz ++;
         }

         else if (strupcmp (wort[0], "DBKEY", 5) == 0)
         {
                    anz = wsplit (buffer, "{}");
                    if (anz < 2) continue;
                    Keys [keyanz] = new char [0x1000];
                    if (Keys [keyanz] == NULL) continue;
                    strcpy (Keys [keyanz], wort[1]);
                    keyanz ++;
         }

         if (strcmp (buffer, "$") == 0) break;
         headfieldanz ++;
     }

     Updates [updanz] = NULL;

     fseek (fp, 0l, 0);
     while (fgets (buffer, 511, fp))
     {
         cr_weg (buffer);
         if (strupcmp (buffer, "$POSITION",  9) == 0) break;
     }

     fieldanz = 0;

     long fpos = ftell (fp);

     while (fgets (buffer, 511, fp)) fieldanz ++;

     fseek (fp, fpos, 0);

     Texte = new char *[fieldanz * sizeof (char *)];
     if (Texte == NULL) 
     {
                fieldanz = 0;
                fclose (fp);
                return;
     }
     Felder = new char *[fieldanz * sizeof (char *)];
     if (Felder == NULL) 
     {
                fieldanz = 0;
                fclose (fp);
                return;
     }
     Namen = new char *[fieldanz * sizeof (char *)];
     if (Namen == NULL) 
     {
                fieldanz = 0;
                fclose (fp);
                return;
     }

     Feldnamen = new char *[fieldanz * sizeof (char *)];

     Rows = new int [fieldanz];
     Columns = new int [fieldanz];
     Length = new int [fieldanz]; 

     i = 0;
    
     while (fgets (buffer, 511, fp))
     {
            if (buffer[0] == '#')
            {
                  continue;
            }
            cr_weg (buffer);
            anz = wsplit (buffer, ";");
            if (anz < 3) continue;
            Texte[i]  = new char [256]; 
            Felder[i] = new char [256]; 
            Feldnamen[i] = new char [256]; 
            Namen[i]  = new char [10];
            strcpy (Feldnamen[i], wort [0]);
            strcpy (Texte[i], wort[1]);
            strcpy (Felder[i], "");           
            sprintf (Namen[i], "field%d", i);
            Length[i] = atoi (wort[2]);
            if (anz > 3)
            {
                    Rows [i] = atoi (wort[3]);
                    Row = Rows [i];
            }
            else
            {
                    Rows [i] = Row;
            } 
            if (anz > 4)
            {
                    Columns [i] = atoi (wort[4]);
            }
            else
            {
                    Columns [i] = TextCol;
            }
                    
            i ++;
            Row ++;
     }

     fclose (fp); 
     fieldanz = i; 
     Texte [i] = NULL;
     Namen [i] = NULL;
     Felder [i] = NULL;
}

TextWork::~TextWork ()
{
     int i;

     if (Texte != NULL)
     {
         for (i = 0; i < fieldanz; i ++)
         {
             delete Texte [i];
         }
         delete Texte;
     }
  
     if (Felder != NULL)
     {
         for (i = 0; i < fieldanz; i ++)
         {
             delete Felder [i];
         }
         delete Felder;
     }

     if (Namen != NULL)
     {
         for (i = 0; i < fieldanz; i ++)
         {
             delete Namen [i];
         }
         delete Namen;
     }

     if (Feldnamen != NULL)
     {
         for (i = 0; i < fieldanz; i ++)
         {
             delete Feldnamen [i];
         }
         delete Feldnamen;
     }

     if (Rows !=NULL)
     {
         delete Rows;
         Rows = NULL;
     }

     if (Columns != NULL)
     {
         delete Columns;
         Columns = NULL;
     }
     if (Length != NULL)
     {
         delete Length;
         Length = NULL;
     }

}

    
     