#ifndef _EmbDlg_Def
#define _EmbDlg_Def
#include <windows.h>
#include "dlg.h"

class EmbDlg : public virtual DLG
{
    private :
         static int StdSize;
         static mfont dlgfont;
		 HWND dhWnd;
		 char *Text;
		 CFIELD **cField;
		 CFORM *cForm;

    public :
        EmbDlg (HINSTANCE, HWND, char *);
        ~EmbDlg ();
        BOOL OnMove (HWND,UINT,WPARAM,LPARAM);

};
#endif