#ifndef _BEST_KOPF_DEF
#define _BEST_KOPF_DEF
#include "searchbest.h"
#include "dbclass.h"

struct BEST_KOPF {
   short     bearb_stat;
   long      best_blg;
   long      best_term;
   long      best_txt;
   short     fil;
   long      lfd;
   char      lief[17];
   long      lief_s;
   long      lief_term;
   short     mdn;
   long      we_dat;
   char      pers_nam[9];
   long      fil_adr;
   long      lief_adr;
   char      lieferzeit[6];
   short     waehrung;
   short     delstatus;
   short     kontrakt_kz;
   long      kontrakt_nr;
   long      kontrakt_von;
   long      kontrakt_bis;
   char      lief_ref_nr[18];
   short     zahl_kond;
   char      freifeld1[37];
   char      freifeld2[37];
   char      freifeld3[37];
};
extern struct BEST_KOPF best_kopf, best_kopf_null;

#line 7 "best_kopf.rh"

class BEST_KOPF_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
               static short cursor_ausw;
               char beststring0[0x1000];
               int bestchange;
               static SEARCHBEST *SearchBest;
       public :
               BEST_KOPF_CLASS () : DB_CLASS ()
               {
                    strcpy (beststring0, "Start");
               }
               int dbreadfirst (void);
               void ListToMamain1 (HWND);
               int ShowBuBestQuery (HWND, int); 
               int PrepareBestQuery (form *, char **);

               int ShowBuBestQueryEx (HWND, int); 
               int ShowBestLbox (HWND, int, char *); 
               int PrepareBestQueryEx (form *, char **);
               static int ReadLst (char *);
};
#endif
