#ifndef _WEDLG_DEF
#define _WEDLG_DEF
#include "dlg.h"
#include "itprog.h"
#include "itfont.h"
#include "formfield.h"
#include "searcha.h"
#include "listdlg.h"
#include "WeDlgLst.h"
#include "textwork.h"
#include "mo_qm.h"
#include "Debug.h"

#define RSDIR "RSDIR"

#define MDN_CTL 1801
#define FIL_CTL 1802
#define LIEF_CTL 1803

#define LIEF_RECH_NR_CTL 1804
#define BLG_TYP_CTL 1805

#define A_BEST_KZ_CTL 1806
#define BSD_VERB_KZ_CTL 1807
#define WE_DAT_CTL 1808
#define BLG_EING_DAT_CTL 1809
#define KORESP_BEST0_CTL 1810
#define PR_ERF_KZ_CTL 1811
#define LS_IDENT_CTL 1812
#define LS_CHARGE_CTL 1813
#define BELEG_CTL 1814
#define JOURNAL_CTL 1815

#define HEADCTL 700
#define POSCTL 701
#define FOOTCTL 702

#define IDM_LIST 2001
#define IDM_CHOISE 2002
#define IDM_DOCKLIST 2003
#define IDM_INSERT 2004
#define IDM_LIEFBEST 2005
#define IDM_AKRZ 2006
#define IDM_Z2 2007
#define IDM_QM 2008
#define IDM_POSEK 2009
#define IDM_NOEK 2010
#define IDM_EK 2011
#define IDM_EPOSEK 2012
#define IDM_BEST 2013
#define IDM_TXTDLG 2014
#define IDM_ABSCHLUSS 2015
#define IDM_CHOISEPRINTER 2016
#define IDM_MULTILIST 2017
#define IDM_SHOWKONTRAKT 2018
#define IDM_SEARCHBEST 2019
#define IDM_EXTRA_DATA 2020
#define IDM_MHD_DATA 2021


#define RAISEDBORDER 0
#define HIGHBORDER 1
#define LOWBORDER 2
#define HIGHCOLBORDER 3
#define LOWCOLBORDER 4
#define LINEBORDER 5
#include "weWork.h"
#include "searcha.h"

#define ENTERHEAD 0
#define ENTERA 1
#define ENTERADATA 2

#define IDM_DELALL 5000


class WeDlg : virtual public DLG 
{
          private :

             static int StdSize;
             static int InfoSize;
             static int dlgsize;
             static int ltgraysize;
             static int dlgpossize;
             static int cpsize;

             static mfont dlgfont;
             static mfont InfoFont;
             static COLORREF InfShadow;

             static mfont ltgrayfont;
             static mfont dlgposfont;
             static mfont cpfont;
             static mfont *Font;

             static CFIELD *_fHead [];
             static CFORM fHead;

             static CFIELD *_fPos[];
             static CFORM fPos;

             static CFIELD *_fWeKopf [];
             static CFORM fWeKopf;

             static CFIELD *_fWeKopf0 []; 
             static CFORM fWeKopf0;

             static CFIELD *_fPrintChoise0 []; 
             static CFIELD *_fPrintChoise1[]; 
             static CFORM fPrintChoise0;
             static CFIELD **_fPrintChoise; 
             static CFIELD *_fPrintFoot []; 
             static CFORM fPrintFoot;
             static CFORM fPrintChoise;

             static ItProg *WeKopfAfter [];
             static ItProg *WeKopfBefore [];
             static ItProg *WePosAfter [];
             static ItFont *WeKopfFont [];
             static FORMFIELD *dbfields [];
             static FORMFIELD *dbheadfields [];
             static char *EnableHead[];
             static char *EnableHeadMdn[];
             static char *EnableHeadFil[];
             static char *EnablePos [];
			 static char Beleg [];
			 static char Journal [];
             int    BorderType;
             static WeWork weWork;
             static int ListRows;
             static BOOL InsMode;
             static BOOL WriteOK;
             static char *HelpName;
             static BOOL NewRec;
             static HBITMAP ListBitmap;
             static HBITMAP ListMask;
             static HBITMAP ListBitmapi;
			 static BitImage ImageDelete;
			 static BitImage ImageInsert;
			 static BitImage ImageList;
			 static COLORREF Colors[];
			 static COLORREF BkColors[];
             static COLORREF QmTextBkColor;
             static BOOL PrChoise;
			 static BOOL ReadBest;
			 static BOOL SysParKreditor;

			 BOOL InPrint;
             BOOL DockList;
             BOOL DockMode;
             LISTDLG *ListDlg;
             WeDlgLst *weDlgLst;
             CFORM *Toolbar2;
             COLORREF ListColor;
             COLORREF ListBkColor;
             BOOL ColorSet;
             int ListLines;
             BOOL LineSet;
             HWND hwndCombo1;
             HWND hwndCombo2;
             char **Combo1;
             char **Combo2;
             HWND *hwndList;
             BOOL WithLiefBest;
             BOOL WithAkrz;
             BOOL TwoRows;
             BOOL WithEk;
             BOOL WithPosEk;
             QMKOPF *QmKopf;
             WeDlg *OldDlg;
             TextWork *textWork;

          public :

            CDebug Debug;
			static BOOL InEnterPos ;
            static BOOL WithSearchBest;
 		    static COLORREF SysBkColor;
            static WeDlg *ActiveDlg;
            static int LsChargeAttr;
            static int Ruecksprung;
            static int LsIdentAttr;
            static char *ChargeMacro;
            static BOOL ListButtons;
            static int ButtonSize;
            static char Format[];
            static char JoFormat[];
		    static BOOL AutoBest;
		    static BOOL RwKopf;
 		    static int ListType;
			static int we_dat_plus;
			static int we_dat_minus;
			static int blg_eing_dat_plus;
			static int blg_eing_dat_minus;
			static BOOL flg_Storno;

            TextWork *GettextWork ()
            {
                  return textWork;
            }

            double GetAktArtikel (void)
            {
				   if (weDlgLst == NULL) return 0.0;
                   return weDlgLst->GetAktArtikel();
            }
            double AfterBzgPressed (void)
            {
				   if (weDlgLst == NULL) return 0.0;
                   return weDlgLst->AfterBzgPressed();
            }

            void SetWithLiefBest (BOOL b)
            {
                  WithLiefBest = b;
            }

            void SetWithSearchBest (BOOL b)
            {
                  WithSearchBest = b;
            }

            void SetInEnterPos (BOOL b)
            {
                  InEnterPos = b;
            }

            BOOL GetWithLiefBest (void)
            {
                   return WithLiefBest;
            }
            BOOL GetInEnterPos (void)
            {
                   return InEnterPos;
            }

            BOOL GetWithSearchBest (void)
            {
                   return WithSearchBest;
            }

            void SetWithAkrz (BOOL b)
            {
                  WithAkrz = b;
            }

            BOOL GetWithAkrz (void)
            {
                   return WithAkrz;
            }

            void SetTwoRows (BOOL b)
            {
                TwoRows = b;
            }

            void SetWithEk (BOOL b)
            {
                  WithEk = b;
            }

            void SetWithPosEk (BOOL b)
            {
                  WithEk = b;
            }

            BOOL GetTooRows (void)
            {
                return TwoRows;
            }

            void SethwndList (HWND *hwndList)
            {
                this->hwndList = hwndList;
            }

            HWND *GethwndList (void)
            {
                return hwndList;
            }

            void SetToolbar2 (CFORM *Toolbar2)
            {
                this->Toolbar2 = Toolbar2;
            }

            CFORM *GetToolbar2 (void)
            {
                return Toolbar2;
            }

            void SetDockList (BOOL DockList)
            {
                this->DockList = DockList;
            }

            BOOL GetDockList (void)
            {
                return DockList;
            }

            void SetDockMode (BOOL DockMode)
            {
                this->DockMode = DockMode;
            }

            BOOL GetDockMode (void)
            {
                return DockMode;
            }

 	        WeDlg (int, int, int, int, char *, int, BOOL);
 	        WeDlg (int, int, int, int, char *, int, BOOL, int);
 	        WeDlg (int, int, int, int, char *, int, BOOL, int, int);
            ~WeDlg ();
 	        void Init (int, int, int, int, char *, int, BOOL);
			void EnableMdnFil ();
            void TestAttr (void);
            void FillHeadfields (void);
            BOOL EnterWeList (void);

            BOOL OnKeyDown (void);
            BOOL OnKeyUp (void);
            BOOL OnKey1 (void);
            BOOL OnKey2 (void);
            BOOL OnKey3 (void);
            BOOL OnKey4 (void);
            BOOL OnKey5 (void);
            BOOL OnKey6 (void);
            BOOL OnKey7 (void);
            BOOL OnKey8 (void);
            BOOL OnKey9 (void);
            BOOL OnKey10 (void);
            BOOL OnKey11 (void);
            BOOL OnKey12 (void);
            BOOL OnKeyDelete (void);
            BOOL OnKeyPrior (void);
            BOOL OnKeyNext (void);
            BOOL OnKeyHome (void);
            BOOL OnKeyEnd (void);
            BOOL OnKeyReturn (void);
            BOOL OnPaint (HWND,HDC,UINT,WPARAM,LPARAM);
            BOOL OnActivate (HWND,UINT,WPARAM,LPARAM);
            BOOL OnCloseUp (HWND,UINT,WPARAM,LPARAM);
            BOOL OnCommand (HWND,UINT,WPARAM,LPARAM);
            BOOL OnSysCommand (HWND,UINT,WPARAM,LPARAM);
            BOOL OnDestroy (HWND,UINT,WPARAM,LPARAM);
            BOOL OnMove (HWND,UINT,WPARAM,LPARAM);
            BOOL ChangeDockList (void);
            BOOL ChangeMultiList (void);
            BOOL ChangeLiefBest (void);
            BOOL ChangeSearchBest (void);
            BOOL ChangeAkrz (void);
            BOOL ChangeZ2 (void);
            BOOL ChangeEk (void);
            BOOL ChangePosEk (void);
            BOOL EnterQmKopf (void);
            BOOL EnterQmPos (HWND);

            void PrintComment (char *);
            HWND OpenWindow (HANDLE, HWND);
            HWND OpenScrollWindow (HANDLE, HWND);
            void FillListCaption (void);
            void SetWinBackground (COLORREF);
            void ToClipboard (void);
            void FromClipboard (void);
            void Help (void);
            BOOL ShowMdn (void);
            BOOL ShowFil (void);
            BOOL ShowLief (void);
            BOOL ShowLief (char *);
            BOOL ShowWeKopf (void);
            BOOL EnterTextDlg (void);
            BOOL WriteWeKopf ();
            void PosEnd (void);
            BOOL EnterAbschlussDlg (void);
            BOOL ShowKontrakt (void);
            BOOL ShowBest (void);
            BOOL ShowPtab (char *, char *);
            void CreateListQuikInfos (HWND);
            void ChangeF6 (DWORD);
            int DeleteWe (void);
            void CallInfo (void);
            void SetListColors (COLORREF, COLORREF);
            void SetListLines (int);
            HWND SetToolCombo (int, int, int, int);
            void SetComboTxt (HWND, char **, int, int);
			void ChoiseCombo1 (void);
			void ChoiseCombo2 (void);
            BOOL ChoisePrinter (void);
            void ChoisePrintOne (void);
            void PrintOne (void);
            void PrintMulti (void);
            void PrintBeleg (void);
            void PrintJournal (void);
            void Print (void);

            static int WriteRow (void);
            static int DeleteRow (void);
            static void FillRow (void);
            static void InsertRow (void);
            static void FillEnterList (int);
            static int ReadBzgMdn (void);
            static int ReadBzgFil (void);
            static int ReadBzgLief (void);
            static int ReadWe (void);
            static void TestEnablePrint (void);
            static int EnterPos (void);
            static int EnableKey10 (void);
            static int DisableKey10 (void);
            static int GetPtab (void);
            static int TestGenCharge (void);
            static int TestBsdVerb (void);
            static int TestDate (void);
            static void InitPos (void);
            static int SetKey9 (void);
            static int SetLiefRechNr (BOOL);
            static int TestPtab (void);
            static int TestWeErfKz (void);
            static int TestPrErfKz (void);
            static void SetQmTextBkColor (COLORREF);
            static COLORREF *GetQmTextBkPtr (void);
			static void SetPrintChoiseMode (int);
			static int TestWeDat ();
			static int TestBlgEingDat ();
            void SetPos (void);
            void DestroyPos (void);
            void DestroyHead (void);
};
#endif
