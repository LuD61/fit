#ifndef _SEARCHPREISE_WE_DEF
#define _SEARCHPREISE_WE_DEF
#include "wmaskc.h"
#include "cmask.h"
#include "mo_chqex.h"
#include "dbclass.h"
#include "ptab.h"

struct SPREISE_WE
{
        char a [15];
	  char a_bz1 [26];
	  char lief_best [18];
	  char lief_rech_nr [18];
	  char best_term [12];
	  char we_dat [12];
	  char pr_ek_b [14];
	  char pr_ek_w [14];
	  char me_einh_b [12];
	  char me_einh_w [12];
        char mdn [6];
        char fil [6];
};

class SEARCHPREISE_WE
{
       private :
           static ITEM ua;
           static ITEM ua_bz1;
           static ITEM ulief_best;
           static ITEM ulief_rech_nr;
           static ITEM ubest_term;
           static ITEM uwe_dat;
           static ITEM upr_ek;
           static ITEM ume_einh;
           static ITEM upr_ek_we;
           static ITEM ume_einh_we;

           static ITEM ia;
           static ITEM ia_bz1;
           static ITEM ilief_best;
           static ITEM ilief_rech_nr;
           static ITEM ibest_term;
           static ITEM ipr_ek;
           static ITEM ime_einh;
           static ITEM iwe_dat;
           static ITEM ipr_ek_we;
           static ITEM ime_einh_we;


           static ITEM iline;

           static field _UbForm[];
           static form UbForm;

           static field _DataForm[];
           static form DataForm;

           static field _LineForm[];
           static form LineForm;

           static HINSTANCE hMainInst;
           static HWND hMainWindow;
           static DB_CLASS DbClass;
           static HWND awin;
           static struct SPREISE_WE *spreise_wetab;
           static struct SPREISE_WE spreise_we;
           static int idx;
           static long anz;
           static CHQEX *Query;
           static int SearchField;
           static char *query;
           static int sobest_term; 
           static int sowe_dat; 
           static int soa_bz1;
           static int solief_best;
           static int solief_rech_nr;
           static int sopr_ek_b;
           static int sopr_ek_w;
           static int some_einh_b;
           static int some_einh_w;
           static short mdn;
           static PTAB_CLASS Ptab;

           int SearchPos;
           int OKPos;
           int CAPos;
           DWORD WindowStyle;
           BOOL SetStyle;
        public :
           SEARCHPREISE_WE ()
           {
                  SearchPos = 8;
                  OKPos = 9;
                  CAPos = 10;
                  SetStyle = FALSE;
                  mdn = 0;
                  WindowStyle = WS_POPUP | WS_THICKFRAME;
           }

           ~SEARCHPREISE_WE ()
           {
                  if (spreise_wetab != NULL)
                  {
                      delete spreise_wetab;
                      spreise_wetab = NULL;
                  }
                  if (Query != NULL)
                  {
                      delete Query;
                      Query = NULL;
                  }
           }

           void SetQuery (char *q)
           {
               this->query = q;
           }

           char *GetQuery (void)
           {
                return query;
           }

           SPREISE_WE *GetSPreise_we (void)
           {
               if (idx == -1) return NULL;
               return &spreise_we;
           }

           void Setawin (HWND awin)
           {
               this->awin = awin;
           }

           void SetWindowStyle (DWORD style)
           {
               WindowStyle = style;
               SetStyle = TRUE;
           }


           void FillBox (void);
           static void FillFormat (char *, int);
           static void FillVlines (char *, int);
           static void FillCaption (char *, int);
           static void FillRec (char *, int, int);
           static int sortbest_term (const void *, const void *);
           static void SortBest_term (HWND);
           static int sortwe_dat (const void *, const void *);
           static void SortWE_dat (HWND);
           static int sorta_bz1 (const void *, const void *);
           static void SortABz1 (HWND);
           static int sortlief_best (const void *, const void *);
           static void SortLiefBest (HWND);
           static int sortlief_rech_nr (const void *, const void *);
           static void SortLiefRechNr (HWND);
           static int sortpr_ek_b (const void *, const void *);
           static int sortpr_ek_w (const void *, const void *);
           static void SortPrEkB (HWND);
           static void SortPrEkW (HWND);
           static int sortme_einh_b (const void *, const void *);
           static int sortme_einh_w (const void *, const void *);
           static void SortMeEinhB (HWND);
           static void SortMeEinhW (HWND);
           static void SortLst (int, HWND);
           static void UpdateList (void);

           static int SearchLst (char *);
           static int Read (char *);
           void SetParams (HINSTANCE, HWND);
           void Search (short);
};
#endif
