# Microsoft Developer Studio Project File - Name="35100" - Package Owner=<4>
# Von Microsoft Developer Studio generierte Erstellungsdatei, Format Version 5.00
# ** NICHT BEARBEITEN **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=35100 - Win32 Debug
!MESSAGE Dies ist kein g�ltiges Makefile. Zum Erstellen dieses Projekts mit\
 NMAKE
!MESSAGE verwenden Sie den Befehl "Makefile exportieren" und f�hren Sie den\
 Befehl
!MESSAGE 
!MESSAGE NMAKE /f "35100.mak".
!MESSAGE 
!MESSAGE Sie k�nnen beim Ausf�hren von NMAKE eine Konfiguration angeben
!MESSAGE durch Definieren des Makros CFG in der Befehlszeile. Zum Beispiel:
!MESSAGE 
!MESSAGE NMAKE /f "35100.mak" CFG="35100 - Win32 Debug"
!MESSAGE 
!MESSAGE F�r die Konfiguration stehen zur Auswahl:
!MESSAGE 
!MESSAGE "35100 - Win32 Release" (basierend auf  "Win32 (x86) Application")
!MESSAGE "35100 - Win32 Debug" (basierend auf  "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "35100 - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /W3 /GX /O2 /I "d:\informix\incl\esql" /I "..\include" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_PFONT" /FR /YX /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o NUL /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o NUL /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib comctl32.lib isqlt07c.lib /nologo /subsystem:windows /machine:I386 /out:"d:\user\fit\bin\35100.exe" /libpath:"d:\informix\lib"

!ELSEIF  "$(CFG)" == "35100 - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /W3 /Gm /GX /Zi /Od /I "d:\informix\incl\esql" /I "..\include" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_PFONT" /FR /YX /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o NUL /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o NUL /win32
# ADD BASE RSC /l 0x407 /d "_DEBUG"
# ADD RSC /l 0x407 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib comctl32.lib isqlt07c.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept /libpath:"d:\informix\lib"

!ENDIF 

# Begin Target

# Name "35100 - Win32 Release"
# Name "35100 - Win32 Debug"
# Begin Source File

SOURCE=.\35100.cpp
# End Source File
# Begin Source File

SOURCE=.\35100.rc

!IF  "$(CFG)" == "35100 - Win32 Release"

!ELSEIF  "$(CFG)" == "35100 - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\A_BAS.CPP
# End Source File
# Begin Source File

SOURCE=.\a_best.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\A_HNDW.CPP
# End Source File
# Begin Source File

SOURCE=.\a_kalk_mat.cpp
# End Source File
# Begin Source File

SOURCE=.\a_kalkhndw.cpp
# End Source File
# Begin Source File

SOURCE=.\a_kalkpreis.cpp
# End Source File
# Begin Source File

SOURCE=.\a_kalkpreis.h
# End Source File
# Begin Source File

SOURCE=.\a_lgr.cpp
# End Source File
# Begin Source File

SOURCE=.\a_lgr.h
# End Source File
# Begin Source File

SOURCE=.\A_PR.CPP
# End Source File
# Begin Source File

SOURCE=.\abschldlg.cpp
# End Source File
# Begin Source File

SOURCE=.\abschlwork.cpp
# End Source File
# Begin Source File

SOURCE=.\AKT_KRZ.CPP
# End Source File
# Begin Source File

SOURCE=.\AKTION.CPP
# End Source File
# Begin Source File

SOURCE=.\best_kopf.cpp
# End Source File
# Begin Source File

SOURCE=.\best_pos.cpp
# End Source File
# Begin Source File

SOURCE=.\bild160.h
# End Source File
# Begin Source File

SOURCE=.\bild160dll.lib
# End Source File
# Begin Source File

SOURCE=.\bsd_buch.cpp
# End Source File
# Begin Source File

SOURCE=.\choisedlg.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\cmask.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\colbut.cpp
# End Source File
# Begin Source File

SOURCE=.\combodlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DataCollection.cpp
# End Source File
# Begin Source File

SOURCE=.\DataCollection.h
# End Source File
# Begin Source File

SOURCE=..\libsrc\dbclass.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\DBFUNC.CPP
# End Source File
# Begin Source File

SOURCE=.\Debug.cpp
# End Source File
# Begin Source File

SOURCE=.\Debug.h
# End Source File
# Begin Source File

SOURCE=..\libsrc\dlg.cpp
# End Source File
# Begin Source File

SOURCE=.\dlglst.cpp
# End Source File
# Begin Source File

SOURCE=.\EmbDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\EmbDlg.h
# End Source File
# Begin Source File

SOURCE=..\libsrc\enterfunc.cpp
# End Source File
# Begin Source File

SOURCE=.\ExtraData.cpp
# End Source File
# Begin Source File

SOURCE=.\ExtraData.h
# End Source File
# Begin Source File

SOURCE=.\ExtraDataDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\ExtraDataDialog.h
# End Source File
# Begin Source File

SOURCE=..\libsrc\FIL.CPP
# End Source File
# Begin Source File

SOURCE=.\fit0.ico
# End Source File
# Begin Source File

SOURCE=.\fit01.ico
# End Source File
# Begin Source File

SOURCE=.\fit02.ico
# End Source File
# Begin Source File

SOURCE=..\libsrc\FORMFELD.CPP
# End Source File
# Begin Source File

SOURCE=..\lib\gprintl.lib
# End Source File
# Begin Source File

SOURCE=..\libsrc\help.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\image.cpp
# End Source File
# Begin Source File

SOURCE=..\lib\inflib.lib
# End Source File
# Begin Source File

SOURCE=.\infmacro.cpp
# End Source File
# Begin Source File

SOURCE=.\infmacros.h
# End Source File
# Begin Source File

SOURCE=.\Integer.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\ITEM.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\itemc.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\lbox.cpp
# End Source File
# Begin Source File

SOURCE=.\lgr.cpp
# End Source File
# Begin Source File

SOURCE=.\lgr.h
# End Source File
# Begin Source File

SOURCE=.\lief.cpp
# End Source File
# Begin Source File

SOURCE=.\lief_bzg.cpp
# End Source File
# Begin Source File

SOURCE=.\lief_bzgn.cpp
# End Source File
# Begin Source File

SOURCE=.\lief_bzgn.h
# End Source File
# Begin Source File

SOURCE=.\LiefBzgDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\LiefBzgWork.cpp
# End Source File
# Begin Source File

SOURCE=.\liefmebest.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\listclex.cpp
# End Source File
# Begin Source File

SOURCE=.\listdlg.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\listenter.cpp
# End Source File
# Begin Source File

SOURCE=.\lmb.cpp
# End Source File
# Begin Source File

SOURCE=..\lib\macro.lib
# End Source File
# Begin Source File

SOURCE=..\libsrc\MDN.CPP
# End Source File
# Begin Source File

SOURCE=.\MeEinhDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\MeEinhWork.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\message.cpp
# End Source File
# Begin Source File

SOURCE=.\MO_A_PR.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\mo_arg.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_ch.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_chq.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\mo_chqex.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\MO_CURSO.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\MO_DRAW.CPP
# End Source File
# Begin Source File

SOURCE=.\MO_EINH.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\mo_enter.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\mo_gdruck.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\mo_inf.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_liefch.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\mo_lsgen.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\MO_MELD.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\MO_MENU.CPP
# End Source File
# Begin Source File

SOURCE=.\mo_nr.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\mo_progcfg.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_qa.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_qm.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_stdac.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\mo_vers.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_wdebug.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_wepr.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\mo_wmess.cpp
# End Source File
# Begin Source File

SOURCE=.\oarrow.cur
# End Source File
# Begin Source File

SOURCE=.\preise_we.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\Process.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\PTAB.CPP
# End Source File
# Begin Source File

SOURCE=.\qswekopf.cpp
# End Source File
# Begin Source File

SOURCE=.\qswepos.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\RBrush.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\rdonly.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\RFont.cpp
# End Source File
# Begin Source File

SOURCE=.\rind.cpp
# End Source File
# Begin Source File

SOURCE=.\rind.h
# End Source File
# Begin Source File

SOURCE=..\libsrc\RPen.cpp
# End Source File
# Begin Source File

SOURCE=.\scanean.cpp
# End Source File
# Begin Source File

SOURCE=.\searcha.cpp
# End Source File
# Begin Source File

SOURCE=.\searchbest.cpp
# End Source File
# Begin Source File

SOURCE=.\searchbzg.cpp
# End Source File
# Begin Source File

SOURCE=.\searchfil.cpp
# End Source File
# Begin Source File

SOURCE=.\searchlief.cpp
# End Source File
# Begin Source File

SOURCE=.\searchlief_best.cpp
# End Source File
# Begin Source File

SOURCE=.\searchmdn.cpp
# End Source File
# Begin Source File

SOURCE=.\searchpreise_we.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\searchptab.cpp
# End Source File
# Begin Source File

SOURCE=.\searchwekopf.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\STDFKT.CPP
# End Source File
# Begin Source File

SOURCE=.\stnd_best.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\STRFKT.CPP
# End Source File
# Begin Source File

SOURCE=.\sys_par.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\Text.cpp
# End Source File
# Begin Source File

SOURCE=.\textdlg.cpp
# End Source File
# Begin Source File

SOURCE=.\textwork.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\VBrush.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\Vector.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\VFont.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\VPen.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\we_jour.cpp
# End Source File
# Begin Source File

SOURCE=.\we_kopf.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\we_pos.cpp
# End Source File
# Begin Source File

SOURCE=.\wedlg.cpp
# End Source File
# Begin Source File

SOURCE=.\WeDlgLst.cpp
# End Source File
# Begin Source File

SOURCE=.\WeDlgLst.h
# End Source File
# Begin Source File

SOURCE=.\WeExtraData.cpp
# End Source File
# Begin Source File

SOURCE=.\WeExtraData.h
# End Source File
# Begin Source File

SOURCE=.\WeExtraDataCollection.cpp
# End Source File
# Begin Source File

SOURCE=.\WeExtraDataCollection.h
# End Source File
# Begin Source File

SOURCE=.\wework.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\wmaskc.cpp
# End Source File
# Begin Source File

SOURCE=.\zerldaten.cpp
# End Source File
# Begin Source File

SOURCE=.\zerldaten.h
# End Source File
# End Target
# End Project
