#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "fanggebiet.h"

struct SAIS sais, sais_null;

/* Variablen fuer Auswahl mit Buttonueberschrift    */


void FANGGEBIET_CLASS::prepare (void)
{
    out_quest ((char *) &sais.delstatus,1,0);
    out_quest ((char *) &sais.jr,1,0);
    out_quest ((char *) sais.pers_nam,0,9);
    out_quest ((char *) &sais.sais,1,0);
    out_quest ((char *) &sais.st_aktiv,2,0);
    out_quest ((char *) &sais.st_bearbeit,2,0);
    out_quest ((char *) sais.st_bz_1,0,25);
    out_quest ((char *) sais.st_bz_2,0,25);
    out_quest ((char *) &sais.st_nordb,2,0);
    out_quest ((char *) &sais.st_nordv,2,0);
    out_quest ((char *) &sais.st_vkztb,2,0);
    out_quest ((char *) &sais.st_vkztv,2,0);
    out_quest ((char *) &sais.st_vordb,2,0);
    out_quest ((char *) &sais.st_vordv,2,0);
         cursor_fanggebiet = prepare_sql ("select "
"sais.delstatus,  sais.jr,  sais.pers_nam,  sais.sais,  sais.st_aktiv,  "
"sais.st_bearbeit,  sais.st_bz_1,  sais.st_bz_2,  sais.st_nordb,  "
"sais.st_nordv,  sais.st_vkztb,  sais.st_vkztv,  sais.st_vordb,  "
"sais.st_vordv from sais where sais > 0 order by sais");

#line 23 "fanggebiet.rpp"

}

int FANGGEBIET_CLASS::lese_fanggebiet (void)
/**
Tabelle sais
**/
{
         if (cursor_fanggebiet == -1)
         {
            prepare ();
       	    open_sql (cursor_fanggebiet);
         }
       	fetch_sql (cursor_fanggebiet);
         
         return sqlstatus;
}

int FANGGEBIET_CLASS::open_fanggebiet (void)
/**
Tabelle sais
**/
{
       	open_sql (cursor_fanggebiet);
         
        return sqlstatus;
}

