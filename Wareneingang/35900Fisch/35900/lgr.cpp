#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "a_lgr.h"
#include "lgr.h"

struct LGR lgr, lgr_null;

/* Variablen fuer Auswahl mit Buttonueberschrift    */


void LGR_CLASS::prepare (void)
{
//         ins_quest ((char *) &a_lgr.a, 3, 0);

    out_quest ((char *) &lgr.mdn,1,0);
    out_quest ((char *) &lgr.fil,1,0);
    out_quest ((char *) &lgr.lgr,1,0);
    out_quest ((char *) lgr.lgr_bz,0,25);
    out_quest ((char *) &lgr.delstatus,1,0);
    out_quest ((char *) &lgr.lgr_kz,1,0);
    out_quest ((char *) lgr.lgr_smt_kz,0,4);
    out_quest ((char *) &lgr.lgr_gr,1,0);
    out_quest ((char *) lgr.lgr_kla,0,2);
    out_quest ((char *) lgr.abr_period,0,2);
    out_quest ((char *) &lgr.adr,2,0);
    out_quest ((char *) &lgr.afl,1,0);
    out_quest ((char *) lgr.bli_kz,0,2);
    out_quest ((char *) &lgr.dat_ero,2,0);
    out_quest ((char *) &lgr.fl_lad,3,0);
    out_quest ((char *) &lgr.fl_nto,3,0);
    out_quest ((char *) &lgr.fl_vk_ges,3,0);
    out_quest ((char *) &lgr.frm,1,0);
    out_quest ((char *) &lgr.iakv,2,0);
    out_quest ((char *) lgr.inv_rht,0,2);
    out_quest ((char *) lgr.ls_abgr,0,2);
    out_quest ((char *) lgr.ls_kz,0,2);
    out_quest ((char *) &lgr.ls_sum,1,0);
    out_quest ((char *) lgr.pers,0,13);
    out_quest ((char *) &lgr.pers_anz,1,0);
    out_quest ((char *) lgr.pos_kum,0,2);
    out_quest ((char *) lgr.pr_ausw,0,2);
    out_quest ((char *) lgr.pr_bel_entl,0,2);
    out_quest ((char *) lgr.pr_lgr_kz,0,2);
    out_quest ((char *) &lgr.pr_lst,2,0);
    out_quest ((char *) lgr.pr_vk_kz,0,2);
    out_quest ((char *) &lgr.reg_bed_theke_lng,3,0);
    out_quest ((char *) &lgr.reg_kt_lng,3,0);
    out_quest ((char *) &lgr.reg_kue_lng,3,0);
    out_quest ((char *) &lgr.reg_lng,3,0);
    out_quest ((char *) &lgr.reg_tks_lng,3,0);
    out_quest ((char *) &lgr.reg_tkt_lng,3,0);
    out_quest ((char *) lgr.smt_kz,0,2);
    out_quest ((char *) &lgr.sonst_einh,1,0);
    out_quest ((char *) &lgr.sprache,1,0);
    out_quest ((char *) lgr.sw_kz,0,2);
    out_quest ((char *) &lgr.tou,2,0);
    out_quest ((char *) &lgr.vrs_typ,1,0);
    out_quest ((char *) lgr.inv_akv,0,2);
         cursor_lgr = prepare_sql ("select lgr.mdn,  lgr.fil,  "
"lgr.lgr,  lgr.lgr_bz,  lgr.delstatus,  lgr.lgr_kz,  lgr.lgr_smt_kz,  "
"lgr.lgr_gr,  lgr.lgr_kla,  lgr.abr_period,  lgr.adr,  lgr.afl,  lgr.bli_kz,  "
"lgr.dat_ero,  lgr.fl_lad,  lgr.fl_nto,  lgr.fl_vk_ges,  lgr.frm,  lgr.iakv,  "
"lgr.inv_rht,  lgr.ls_abgr,  lgr.ls_kz,  lgr.ls_sum,  lgr.pers,  "
"lgr.pers_anz,  lgr.pos_kum,  lgr.pr_ausw,  lgr.pr_bel_entl,  "
"lgr.pr_lgr_kz,  lgr.pr_lst,  lgr.pr_vk_kz,  lgr.reg_bed_theke_lng,  "
"lgr.reg_kt_lng,  lgr.reg_kue_lng,  lgr.reg_lng,  lgr.reg_tks_lng,  "
"lgr.reg_tkt_lng,  lgr.smt_kz,  lgr.sonst_einh,  lgr.sprache,  lgr.sw_kz,  "
"lgr.tou,  lgr.vrs_typ,  lgr.inv_akv from lgr");

#line 26 "lgr.rpp"

}
void LGR_CLASS::prepare (char *order )
{
    if ( order)
    {
    out_quest ((char *) &lgr.mdn,1,0);
    out_quest ((char *) &lgr.fil,1,0);
    out_quest ((char *) &lgr.lgr,1,0);
    out_quest ((char *) lgr.lgr_bz,0,25);
    out_quest ((char *) &lgr.delstatus,1,0);
    out_quest ((char *) &lgr.lgr_kz,1,0);
    out_quest ((char *) lgr.lgr_smt_kz,0,4);
    out_quest ((char *) &lgr.lgr_gr,1,0);
    out_quest ((char *) lgr.lgr_kla,0,2);
    out_quest ((char *) lgr.abr_period,0,2);
    out_quest ((char *) &lgr.adr,2,0);
    out_quest ((char *) &lgr.afl,1,0);
    out_quest ((char *) lgr.bli_kz,0,2);
    out_quest ((char *) &lgr.dat_ero,2,0);
    out_quest ((char *) &lgr.fl_lad,3,0);
    out_quest ((char *) &lgr.fl_nto,3,0);
    out_quest ((char *) &lgr.fl_vk_ges,3,0);
    out_quest ((char *) &lgr.frm,1,0);
    out_quest ((char *) &lgr.iakv,2,0);
    out_quest ((char *) lgr.inv_rht,0,2);
    out_quest ((char *) lgr.ls_abgr,0,2);
    out_quest ((char *) lgr.ls_kz,0,2);
    out_quest ((char *) &lgr.ls_sum,1,0);
    out_quest ((char *) lgr.pers,0,13);
    out_quest ((char *) &lgr.pers_anz,1,0);
    out_quest ((char *) lgr.pos_kum,0,2);
    out_quest ((char *) lgr.pr_ausw,0,2);
    out_quest ((char *) lgr.pr_bel_entl,0,2);
    out_quest ((char *) lgr.pr_lgr_kz,0,2);
    out_quest ((char *) &lgr.pr_lst,2,0);
    out_quest ((char *) lgr.pr_vk_kz,0,2);
    out_quest ((char *) &lgr.reg_bed_theke_lng,3,0);
    out_quest ((char *) &lgr.reg_kt_lng,3,0);
    out_quest ((char *) &lgr.reg_kue_lng,3,0);
    out_quest ((char *) &lgr.reg_lng,3,0);
    out_quest ((char *) &lgr.reg_tks_lng,3,0);
    out_quest ((char *) &lgr.reg_tkt_lng,3,0);
    out_quest ((char *) lgr.smt_kz,0,2);
    out_quest ((char *) &lgr.sonst_einh,1,0);
    out_quest ((char *) &lgr.sprache,1,0);
    out_quest ((char *) lgr.sw_kz,0,2);
    out_quest ((char *) &lgr.tou,2,0);
    out_quest ((char *) &lgr.vrs_typ,1,0);
    out_quest ((char *) lgr.inv_akv,0,2);
         cursor_lgr = prepare_sql ("select lgr.mdn,  lgr.fil,  "
"lgr.lgr,  lgr.lgr_bz,  lgr.delstatus,  lgr.lgr_kz,  lgr.lgr_smt_kz,  "
"lgr.lgr_gr,  lgr.lgr_kla,  lgr.abr_period,  lgr.adr,  lgr.afl,  lgr.bli_kz,  "
"lgr.dat_ero,  lgr.fl_lad,  lgr.fl_nto,  lgr.fl_vk_ges,  lgr.frm,  lgr.iakv,  "
"lgr.inv_rht,  lgr.ls_abgr,  lgr.ls_kz,  lgr.ls_sum,  lgr.pers,  "
"lgr.pers_anz,  lgr.pos_kum,  lgr.pr_ausw,  lgr.pr_bel_entl,  "
"lgr.pr_lgr_kz,  lgr.pr_lst,  lgr.pr_vk_kz,  lgr.reg_bed_theke_lng,  "
"lgr.reg_kt_lng,  lgr.reg_kue_lng,  lgr.reg_lng,  lgr.reg_tks_lng,  "
"lgr.reg_tkt_lng,  lgr.smt_kz,  lgr.sonst_einh,  lgr.sprache,  lgr.sw_kz,  "
"lgr.tou,  lgr.vrs_typ,  lgr.inv_akv from lgr "

#line 33 "lgr.rpp"
                               " %s ",order);
    }
    else
    {
    out_quest ((char *) &lgr.mdn,1,0);
    out_quest ((char *) &lgr.fil,1,0);
    out_quest ((char *) &lgr.lgr,1,0);
    out_quest ((char *) lgr.lgr_bz,0,25);
    out_quest ((char *) &lgr.delstatus,1,0);
    out_quest ((char *) &lgr.lgr_kz,1,0);
    out_quest ((char *) lgr.lgr_smt_kz,0,4);
    out_quest ((char *) &lgr.lgr_gr,1,0);
    out_quest ((char *) lgr.lgr_kla,0,2);
    out_quest ((char *) lgr.abr_period,0,2);
    out_quest ((char *) &lgr.adr,2,0);
    out_quest ((char *) &lgr.afl,1,0);
    out_quest ((char *) lgr.bli_kz,0,2);
    out_quest ((char *) &lgr.dat_ero,2,0);
    out_quest ((char *) &lgr.fl_lad,3,0);
    out_quest ((char *) &lgr.fl_nto,3,0);
    out_quest ((char *) &lgr.fl_vk_ges,3,0);
    out_quest ((char *) &lgr.frm,1,0);
    out_quest ((char *) &lgr.iakv,2,0);
    out_quest ((char *) lgr.inv_rht,0,2);
    out_quest ((char *) lgr.ls_abgr,0,2);
    out_quest ((char *) lgr.ls_kz,0,2);
    out_quest ((char *) &lgr.ls_sum,1,0);
    out_quest ((char *) lgr.pers,0,13);
    out_quest ((char *) &lgr.pers_anz,1,0);
    out_quest ((char *) lgr.pos_kum,0,2);
    out_quest ((char *) lgr.pr_ausw,0,2);
    out_quest ((char *) lgr.pr_bel_entl,0,2);
    out_quest ((char *) lgr.pr_lgr_kz,0,2);
    out_quest ((char *) &lgr.pr_lst,2,0);
    out_quest ((char *) lgr.pr_vk_kz,0,2);
    out_quest ((char *) &lgr.reg_bed_theke_lng,3,0);
    out_quest ((char *) &lgr.reg_kt_lng,3,0);
    out_quest ((char *) &lgr.reg_kue_lng,3,0);
    out_quest ((char *) &lgr.reg_lng,3,0);
    out_quest ((char *) &lgr.reg_tks_lng,3,0);
    out_quest ((char *) &lgr.reg_tkt_lng,3,0);
    out_quest ((char *) lgr.smt_kz,0,2);
    out_quest ((char *) &lgr.sonst_einh,1,0);
    out_quest ((char *) &lgr.sprache,1,0);
    out_quest ((char *) lgr.sw_kz,0,2);
    out_quest ((char *) &lgr.tou,2,0);
    out_quest ((char *) &lgr.vrs_typ,1,0);
    out_quest ((char *) lgr.inv_akv,0,2);
         cursor_lgr = prepare_sql ("select lgr.mdn,  lgr.fil,  "
"lgr.lgr,  lgr.lgr_bz,  lgr.delstatus,  lgr.lgr_kz,  lgr.lgr_smt_kz,  "
"lgr.lgr_gr,  lgr.lgr_kla,  lgr.abr_period,  lgr.adr,  lgr.afl,  lgr.bli_kz,  "
"lgr.dat_ero,  lgr.fl_lad,  lgr.fl_nto,  lgr.fl_vk_ges,  lgr.frm,  lgr.iakv,  "
"lgr.inv_rht,  lgr.ls_abgr,  lgr.ls_kz,  lgr.ls_sum,  lgr.pers,  "
"lgr.pers_anz,  lgr.pos_kum,  lgr.pr_ausw,  lgr.pr_bel_entl,  "
"lgr.pr_lgr_kz,  lgr.pr_lst,  lgr.pr_vk_kz,  lgr.reg_bed_theke_lng,  "
"lgr.reg_kt_lng,  lgr.reg_kue_lng,  lgr.reg_lng,  lgr.reg_tks_lng,  "
"lgr.reg_tkt_lng,  lgr.smt_kz,  lgr.sonst_einh,  lgr.sprache,  lgr.sw_kz,  "
"lgr.tou,  lgr.vrs_typ,  lgr.inv_akv from lgr "

#line 38 "lgr.rpp"
                               " ");
    }
         ins_quest ((char *) &a_lgr.a, 3, 0);
    out_quest ((char *) &lgr.mdn,1,0);
    out_quest ((char *) &lgr.fil,1,0);
    out_quest ((char *) &lgr.lgr,1,0);
    out_quest ((char *) lgr.lgr_bz,0,25);
    out_quest ((char *) &lgr.delstatus,1,0);
    out_quest ((char *) &lgr.lgr_kz,1,0);
    out_quest ((char *) lgr.lgr_smt_kz,0,4);
    out_quest ((char *) &lgr.lgr_gr,1,0);
    out_quest ((char *) lgr.lgr_kla,0,2);
    out_quest ((char *) lgr.abr_period,0,2);
    out_quest ((char *) &lgr.adr,2,0);
    out_quest ((char *) &lgr.afl,1,0);
    out_quest ((char *) lgr.bli_kz,0,2);
    out_quest ((char *) &lgr.dat_ero,2,0);
    out_quest ((char *) &lgr.fl_lad,3,0);
    out_quest ((char *) &lgr.fl_nto,3,0);
    out_quest ((char *) &lgr.fl_vk_ges,3,0);
    out_quest ((char *) &lgr.frm,1,0);
    out_quest ((char *) &lgr.iakv,2,0);
    out_quest ((char *) lgr.inv_rht,0,2);
    out_quest ((char *) lgr.ls_abgr,0,2);
    out_quest ((char *) lgr.ls_kz,0,2);
    out_quest ((char *) &lgr.ls_sum,1,0);
    out_quest ((char *) lgr.pers,0,13);
    out_quest ((char *) &lgr.pers_anz,1,0);
    out_quest ((char *) lgr.pos_kum,0,2);
    out_quest ((char *) lgr.pr_ausw,0,2);
    out_quest ((char *) lgr.pr_bel_entl,0,2);
    out_quest ((char *) lgr.pr_lgr_kz,0,2);
    out_quest ((char *) &lgr.pr_lst,2,0);
    out_quest ((char *) lgr.pr_vk_kz,0,2);
    out_quest ((char *) &lgr.reg_bed_theke_lng,3,0);
    out_quest ((char *) &lgr.reg_kt_lng,3,0);
    out_quest ((char *) &lgr.reg_kue_lng,3,0);
    out_quest ((char *) &lgr.reg_lng,3,0);
    out_quest ((char *) &lgr.reg_tks_lng,3,0);
    out_quest ((char *) &lgr.reg_tkt_lng,3,0);
    out_quest ((char *) lgr.smt_kz,0,2);
    out_quest ((char *) &lgr.sonst_einh,1,0);
    out_quest ((char *) &lgr.sprache,1,0);
    out_quest ((char *) lgr.sw_kz,0,2);
    out_quest ((char *) &lgr.tou,2,0);
    out_quest ((char *) &lgr.vrs_typ,1,0);
    out_quest ((char *) lgr.inv_akv,0,2);
         cursor_a_lgr = prepare_sql ("select lgr.mdn,  lgr.fil,  "
"lgr.lgr,  lgr.lgr_bz,  lgr.delstatus,  lgr.lgr_kz,  lgr.lgr_smt_kz,  "
"lgr.lgr_gr,  lgr.lgr_kla,  lgr.abr_period,  lgr.adr,  lgr.afl,  lgr.bli_kz,  "
"lgr.dat_ero,  lgr.fl_lad,  lgr.fl_nto,  lgr.fl_vk_ges,  lgr.frm,  lgr.iakv,  "
"lgr.inv_rht,  lgr.ls_abgr,  lgr.ls_kz,  lgr.ls_sum,  lgr.pers,  "
"lgr.pers_anz,  lgr.pos_kum,  lgr.pr_ausw,  lgr.pr_bel_entl,  "
"lgr.pr_lgr_kz,  lgr.pr_lst,  lgr.pr_vk_kz,  lgr.reg_bed_theke_lng,  "
"lgr.reg_kt_lng,  lgr.reg_kue_lng,  lgr.reg_lng,  lgr.reg_tks_lng,  "
"lgr.reg_tkt_lng,  lgr.smt_kz,  lgr.sonst_einh,  lgr.sprache,  lgr.sw_kz,  "
"lgr.tou,  lgr.vrs_typ,  lgr.inv_akv from lgr,a_lgr where a_lgr.mdn = lgr.mdn and a_lgr.lgr = lgr.lgr and a_lgr.a = ? "

#line 42 "lgr.rpp"
                               " ");

}

int LGR_CLASS::lese_lgr (char *order)
/**
Tabelle lgr
**/
{
         if (cursor_lgr == -1)
         {
            prepare (order);
         }
         if (a_lgr.a > 0.0)
         {
         	open_sql (cursor_a_lgr);
         	fetch_sql (cursor_a_lgr);
         }
         else
         {
         	open_sql (cursor_lgr);
         	fetch_sql (cursor_lgr);
         }
         
         return sqlstatus;
}

int LGR_CLASS::lese_lgr (void)
/**
Naechsten Satz aus Tabelle lgr lesen.
**/
{
	 if (a_lgr.a > 0.0)
	 {
         	fetch_sql (cursor_a_lgr);
         }
         else
	 {
         	fetch_sql (cursor_lgr);
         }
         
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int LGR_CLASS::lese_lgr_bz (int lager)
{

        ins_quest ((char *) &lager, 2, 0);
        out_quest ((char *) lgr.lgr_bz,0,25);
		return sqlcomm ("select lgr.lgr_bz from lgr where lgr = ? ");
		/*
         if (cursor_lgr_bz == -1)
         {
		     ins_quest ((char *) &lager, 2, 0);
    out_quest ((char *) &lgr.mdn,1,0);
    out_quest ((char *) &lgr.fil,1,0);
    out_quest ((char *) &lgr.lgr,1,0);
    out_quest ((char *) lgr.lgr_bz,0,25);
    out_quest ((char *) &lgr.delstatus,1,0);
    out_quest ((char *) &lgr.lgr_kz,1,0);
    out_quest ((char *) lgr.lgr_smt_kz,0,4);
    out_quest ((char *) &lgr.lgr_gr,1,0);
    out_quest ((char *) lgr.lgr_kla,0,2);
    out_quest ((char *) lgr.abr_period,0,2);
    out_quest ((char *) &lgr.adr,2,0);
    out_quest ((char *) &lgr.afl,1,0);
    out_quest ((char *) lgr.bli_kz,0,2);
    out_quest ((char *) &lgr.dat_ero,2,0);
    out_quest ((char *) &lgr.fl_lad,3,0);
    out_quest ((char *) &lgr.fl_nto,3,0);
    out_quest ((char *) &lgr.fl_vk_ges,3,0);
    out_quest ((char *) &lgr.frm,1,0);
    out_quest ((char *) &lgr.iakv,2,0);
    out_quest ((char *) lgr.inv_rht,0,2);
    out_quest ((char *) lgr.ls_abgr,0,2);
    out_quest ((char *) lgr.ls_kz,0,2);
    out_quest ((char *) &lgr.ls_sum,1,0);
    out_quest ((char *) lgr.pers,0,13);
    out_quest ((char *) &lgr.pers_anz,1,0);
    out_quest ((char *) lgr.pos_kum,0,2);
    out_quest ((char *) lgr.pr_ausw,0,2);
    out_quest ((char *) lgr.pr_bel_entl,0,2);
    out_quest ((char *) lgr.pr_lgr_kz,0,2);
    out_quest ((char *) &lgr.pr_lst,2,0);
    out_quest ((char *) lgr.pr_vk_kz,0,2);
    out_quest ((char *) &lgr.reg_bed_theke_lng,3,0);
    out_quest ((char *) &lgr.reg_kt_lng,3,0);
    out_quest ((char *) &lgr.reg_kue_lng,3,0);
    out_quest ((char *) &lgr.reg_lng,3,0);
    out_quest ((char *) &lgr.reg_tks_lng,3,0);
    out_quest ((char *) &lgr.reg_tkt_lng,3,0);
    out_quest ((char *) lgr.smt_kz,0,2);
    out_quest ((char *) &lgr.sonst_einh,1,0);
    out_quest ((char *) &lgr.sprache,1,0);
    out_quest ((char *) lgr.sw_kz,0,2);
    out_quest ((char *) &lgr.tou,2,0);
    out_quest ((char *) &lgr.vrs_typ,1,0);
    out_quest ((char *) lgr.inv_akv,0,2);
			 cursor_lgr_bz = prepare_sql ("select lgr.mdn,  lgr.fil,  "
"lgr.lgr,  lgr.lgr_bz,  lgr.delstatus,  lgr.lgr_kz,  lgr.lgr_smt_kz,  "
"lgr.lgr_gr,  lgr.lgr_kla,  lgr.abr_period,  lgr.adr,  lgr.afl,  lgr.bli_kz,  "
"lgr.dat_ero,  lgr.fl_lad,  lgr.fl_nto,  lgr.fl_vk_ges,  lgr.frm,  lgr.iakv,  "
"lgr.inv_rht,  lgr.ls_abgr,  lgr.ls_kz,  lgr.ls_sum,  lgr.pers,  "
"lgr.pers_anz,  lgr.pos_kum,  lgr.pr_ausw,  lgr.pr_bel_entl,  "
"lgr.pr_lgr_kz,  lgr.pr_lst,  lgr.pr_vk_kz,  lgr.reg_bed_theke_lng,  "
"lgr.reg_kt_lng,  lgr.reg_kue_lng,  lgr.reg_lng,  lgr.reg_tks_lng,  "
"lgr.reg_tkt_lng,  lgr.smt_kz,  lgr.sonst_einh,  lgr.sprache,  lgr.sw_kz,  "
"lgr.tou,  lgr.vrs_typ,  lgr.inv_akv from lgr "

#line 101 "lgr.rpp"
                               "where lgr = ? ");
         }
         open_sql (cursor_lgr_bz);
         fetch_sql (cursor_lgr_bz);
         return sqlstatus;
		 */
}
