#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "a_lgr.h"

struct A_LGR a_lgr, a_lgr_null;

void A_LGR_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &a_lgr.mdn, 1, 0);
            ins_quest ((char *) &a_lgr.lgr, 2, 0);
            ins_quest ((char *) &a_lgr.a, 3, 0);
    out_quest ((char *) &a_lgr.a,3,0);
    out_quest ((char *) &a_lgr.a_lgr_kap,3,0);
    out_quest ((char *) &a_lgr.delstatus,1,0);
    out_quest ((char *) &a_lgr.fil,1,0);
    out_quest ((char *) &a_lgr.lgr,2,0);
    out_quest ((char *) &a_lgr.lgr_pla_kz,1,0);
    out_quest ((char *) &a_lgr.lgr_typ,1,0);
    out_quest ((char *) &a_lgr.mdn,1,0);
    out_quest ((char *) a_lgr.haupt_lgr,0,2);
    out_quest ((char *) a_lgr.lgr_platz,0,11);
    out_quest ((char *) &a_lgr.min_bestand,3,0);
    out_quest ((char *) &a_lgr.meld_bestand,3,0);
    out_quest ((char *) &a_lgr.hoechst_bestand,3,0);
    out_quest ((char *) &a_lgr.inh_wanne,2,0);
    out_quest ((char *) &a_lgr.buch_artikel,3,0);
            cursor = prepare_sql ("select a_lgr.a,  "
"a_lgr.a_lgr_kap,  a_lgr.delstatus,  a_lgr.fil,  a_lgr.lgr,  "
"a_lgr.lgr_pla_kz,  a_lgr.lgr_typ,  a_lgr.mdn,  a_lgr.haupt_lgr,  "
"a_lgr.lgr_platz,  a_lgr.min_bestand,  a_lgr.meld_bestand,  "
"a_lgr.hoechst_bestand,  a_lgr.inh_wanne,  a_lgr.buch_artikel from a_lgr "

#line 25 "a_lgr.rpp"
                                  "where mdn = ? and lgr = ? and a = ? "
                                  "order by lgr");

    ins_quest ((char *) &a_lgr.a,3,0);
    ins_quest ((char *) &a_lgr.a_lgr_kap,3,0);
    ins_quest ((char *) &a_lgr.delstatus,1,0);
    ins_quest ((char *) &a_lgr.fil,1,0);
    ins_quest ((char *) &a_lgr.lgr,2,0);
    ins_quest ((char *) &a_lgr.lgr_pla_kz,1,0);
    ins_quest ((char *) &a_lgr.lgr_typ,1,0);
    ins_quest ((char *) &a_lgr.mdn,1,0);
    ins_quest ((char *) a_lgr.haupt_lgr,0,2);
    ins_quest ((char *) a_lgr.lgr_platz,0,11);
    ins_quest ((char *) &a_lgr.min_bestand,3,0);
    ins_quest ((char *) &a_lgr.meld_bestand,3,0);
    ins_quest ((char *) &a_lgr.hoechst_bestand,3,0);
    ins_quest ((char *) &a_lgr.inh_wanne,2,0);
    ins_quest ((char *) &a_lgr.buch_artikel,3,0);
            sqltext = "update a_lgr set a_lgr.a = ?,  "
"a_lgr.a_lgr_kap = ?,  a_lgr.delstatus = ?,  a_lgr.fil = ?,  "
"a_lgr.lgr = ?,  a_lgr.lgr_pla_kz = ?,  a_lgr.lgr_typ = ?,  "
"a_lgr.mdn = ?,  a_lgr.haupt_lgr = ?,  a_lgr.lgr_platz = ?,  "
"a_lgr.min_bestand = ?,  a_lgr.meld_bestand = ?,  "
"a_lgr.hoechst_bestand = ?,  a_lgr.inh_wanne = ?,  "
"a_lgr.buch_artikel = ? "

#line 29 "a_lgr.rpp"
                                  "where mdn = ? and lgr = ? and a = ? ";

            ins_quest ((char *) &a_lgr.mdn, 1, 0);
            ins_quest ((char *) &a_lgr.lgr, 2, 0);
            ins_quest ((char *) &a_lgr.a, 3, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &a_lgr.mdn, 1, 0);
            ins_quest ((char *) &a_lgr.lgr, 2, 0);
            ins_quest ((char *) &a_lgr.a, 3, 0);
            test_upd_cursor = prepare_sql ("select lgr from a_lgr "
                                  "where mdn = ? and lgr = ? and a = ? ");

            ins_quest ((char *) &a_lgr.mdn, 1, 0);
            ins_quest ((char *) &a_lgr.lgr, 2, 0);
            ins_quest ((char *) &a_lgr.a, 3, 0);
            del_cursor = prepare_sql ("delete from a_lgr "
                                  "where mdn = ? and lgr = ? and a = ? ");


    ins_quest ((char *) &a_lgr.a,3,0);
    ins_quest ((char *) &a_lgr.a_lgr_kap,3,0);
    ins_quest ((char *) &a_lgr.delstatus,1,0);
    ins_quest ((char *) &a_lgr.fil,1,0);
    ins_quest ((char *) &a_lgr.lgr,2,0);
    ins_quest ((char *) &a_lgr.lgr_pla_kz,1,0);
    ins_quest ((char *) &a_lgr.lgr_typ,1,0);
    ins_quest ((char *) &a_lgr.mdn,1,0);
    ins_quest ((char *) a_lgr.haupt_lgr,0,2);
    ins_quest ((char *) a_lgr.lgr_platz,0,11);
    ins_quest ((char *) &a_lgr.min_bestand,3,0);
    ins_quest ((char *) &a_lgr.meld_bestand,3,0);
    ins_quest ((char *) &a_lgr.hoechst_bestand,3,0);
    ins_quest ((char *) &a_lgr.inh_wanne,2,0);
    ins_quest ((char *) &a_lgr.buch_artikel,3,0);
            ins_cursor = prepare_sql ("insert into a_lgr (a,  "
"a_lgr_kap,  delstatus,  fil,  lgr,  lgr_pla_kz,  lgr_typ,  mdn,  haupt_lgr,  "
"lgr_platz,  min_bestand,  meld_bestand,  hoechst_bestand,  inh_wanne,  "
"buch_artikel) "

#line 50 "a_lgr.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?)"); 

#line 52 "a_lgr.rpp"
}
int A_LGR_CLASS::dbreadfirst (void)
/**

Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

