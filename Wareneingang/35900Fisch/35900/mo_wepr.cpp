/*
-----------------------------------------------------------------------------
-
-	Modulname		:	mo_wepr
-
-       Autor                   :       W. Roth
-       Erstellungsdatum        :       11.10.99
-       Modifikationsdatum      :       11.10.99
-
-	Projekt			:	BWS
-	Version			:	1.03
-	Laendervariante		:	BRD
-
-       Sprache                 :       C++
-
-	Aenderungsjournal	:
-
-	lfd. -	Version	Datum	    Name	Beschreibung
-       ----------------------------------------------------------------------

-
------------------------------------------------------------------------------
-
-	Modulbeschreibung	:  	Prozeduren zur Preisfindung fuer
-					den Wareneingang
-
-					FIT - VERSION
-
------------------------------------------------------------------------------
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include "wmaskc.h"
#include "strfkt.h"
#include "mo_curso.h"
#include "mdn.h"
#include "fil.h"
#include "we_kopf.h"
#include "we_pos.h"
#include "mo_wepr.h"
#include "lief.h"
#include "lief_bzg.h"
#include "lief_bzgn.h"

extern int _WMDEBUG;
extern int writelog (char *, ...);
extern void disp_mess (char *, int modus);

static LIEF_BZG_CLASS LiefBzg;
static LIEF_BZGN_CLASS LiefBzgn;
static LIEF_CLASS LiefClass;
static DB_CLASS DbClass;

/*---------------------------*/
/*--- P R O C E D U R E N ---*/
/*---------------------------*/


int WE_PREISE::preis_holen (short dmdn, short dfil, char *dlief,
                            double da)
/**
Ek-Preis hierarchisch holen.
**/
{
             int dsqlstatus;
  
             lief_bzg.mdn = dmdn;
             lief_bzg.fil = dfil;
             lief_bzg.a   = da;
             lief_bzg.pr_ek   = 0.0;
             lief_bzg.pr_ek_eur   = 0.0;
             strcpy (lief_bzg.lief, dlief);
			 if (strlen(lief_bzg.lief_best) > 0) //020209
			 {
	             dsqlstatus = LiefBzg.dbreadfirstbest ();
				while (dsqlstatus == 100)
				{
                        if (lief_bzg.fil)
                        {
                                   lief_bzg.fil = 0;
                        }
                        else if (lief_bzg.mdn)
                        {
                                   lief_bzg.mdn = 0;
                        }
                        else
                        {
                                   break;
                        }   
                        dsqlstatus = LiefBzg.dbreadfirstbest ();
				}
				return dsqlstatus;
			 }


             dsqlstatus = LiefBzg.dbreadfirst ();
             while (dsqlstatus == 100)
             {
                        if (lief_bzg.fil)
                        {
                                   lief_bzg.fil = 0;
                        }
                        else if (lief_bzg.mdn)
                        {
                                   lief_bzg.mdn = 0;
                        }
                        else
                        {
                                   break;
                        }   
                        dsqlstatus = LiefBzg.dbreadfirst ();
              }
              return dsqlstatus;
}


int WE_PREISE::preis_holen (short dmdn, short dfil, char *dlief, char *dat,
                            double da)
/**
Ek-Preis hierarchisch holen.
**/
{
             int dsqlstatus;
             int dsqlstatusn;
  
             lief_bzg.mdn = dmdn;
             lief_bzg.fil = dfil;
             lief_bzg.a   = da;
             lief_bzg.pr_ek   = 0.0;
             lief_bzg.pr_ek_eur   = 0.0;
             strcpy (lief_bzg.lief, dlief);
             dsqlstatus = LiefBzg.dbreadfirst ();
             while (dsqlstatus == 100)
             {
                        if (lief_bzg.fil)
                        {
                                   lief_bzg.fil = 0;
                        }
                        else if (lief_bzg.mdn)
                        {
                                   lief_bzg.mdn = 0;
                        }
                        else
                        {
                                   break;
                        }   
                        dsqlstatus = LiefBzg.dbreadfirst ();
             }
             lief_bzgn.mdn = dmdn;
             lief_bzgn.fil = dfil;
             lief_bzgn.dat = dasc_to_long (dat);
             strcpy (lief_bzgn.lief_best, lief_bzg.lief_best);
             lief_bzgn.a   = da;
             lief_bzgn.pr_ek   = 0.0;
             lief_bzgn.pr_ek_eur   = 0.0;
             strcpy (lief_bzgn.lief, dlief);
             dsqlstatusn = LiefBzgn.dbreadfirst_dat ();
             while (dsqlstatusn == 100)
             {
                        if (lief_bzgn.fil)
                        {
                                   lief_bzgn.fil = 0;
                        }
                        else if (lief_bzgn.mdn)
                        {
                                   lief_bzgn.mdn = 0;
                        }
                        else
                        {
                                   break;
                        }   
                        dsqlstatusn = LiefBzgn.dbreadfirst_dat ();
              }
			  if (dsqlstatusn != 0)
			  {
				  return dsqlstatus;
			  }

              if (dsqlstatus != 0)
			  {
				  lief_bzg.pr_ek = lief_bzgn.pr_ek;
				  lief_bzg.pr_ek_eur = lief_bzgn.pr_ek_eur;
				  return dsqlstatusn;
			  }
			  if (lief_bzgn.fil != 0 && lief_bzgn.fil == 0)
			  {
				  return dsqlstatus;
			  }

			  if (lief_bzg.mdn != 0 && lief_bzgn.mdn == 0)
			  {
				  return dsqlstatus;
			  }

              if (lief_bzgn.pr_ek_eur != 0.0)
			  {
				  lief_bzg.pr_ek = lief_bzgn.pr_ek;
				  lief_bzg.pr_ek_eur = lief_bzgn.pr_ek_eur;
				  return dsqlstatusn;
			  }
              return dsqlstatus;
}


int WE_PREISE::fetchrab (int cursor)
{
	          int dsqlstatus;

	          DbClass.sqlopen (cursor);
	          dsqlstatus = DbClass.sqlfetch (cursor);
			  while (dsqlstatus == 100)
			  {
				  if (rfil > 0)
				  {
					  rfil = 0;
				  }
				  else if (rmdn > 0)
				  {
					  rmdn = 0;
				  }
				  else
				  {
					  break;
				  }
  	              DbClass.sqlopen (cursor);
	              dsqlstatus = DbClass.sqlfetch (cursor);
			  }
			  return dsqlstatus;
}


double WE_PREISE::GetRabEk (short mdn, short fil, char *lieferant, 
							 double inh, double pr_ek, int me_kz,double a,int ag, short wg)
/**
Generalrabatte verrechnen.
**/
{
	          int kette_curs;
			  int gruppe_curs;
 	          int dsqlstatus;
			  double rab;
			  short lfd;
          
              rab = 0.0;
              rmdn = mdn;
			  rfil = fil;
              DbClass.sqlin ((short *) &rmdn, 1, 0); 
              DbClass.sqlin ((short *) &rfil, 1, 0); 
              DbClass.sqlin ((char *)  lieferant, 0, 17); 
              DbClass.sqlout ((double *)  &proz_sum, 3, 0); 
              DbClass.sqlout ((double *)  &lfd, 1, 0); 
              kette_curs = DbClass.sqlcursor ("select proz_sum,lfd from lief_r_kte "
				                              "where mdn = ? "
											  "and fil = ? "
											  "and lief = ? "
											  "order by lfd"); 

              DbClass.sqlin ((short *) &rmdn, 1, 0); 
              DbClass.sqlin ((short *) &rfil, 1, 0); 
              DbClass.sqlin ((char *)  lieferant, 0, 17); 
              DbClass.sqlout ((double *)  &rab_proz, 3, 0); 
              DbClass.sqlout ((double *)  &lfd, 1, 0); 
              gruppe_curs = DbClass.sqlcursor ("select rab_proz, lfd from lief_r_gr "
				                               "where mdn = ? "
								  			   "and fil = ? "
											   "and lief = ? "
											   "order by lfd");
			  
			  if (inh == 0) inh = 1;
			  proz_sum = rab_proz = (double) 0.0;
              if (strcmp (lief.rab_abl, "G") == 0)
			  {
                         dsqlstatus = fetchrab (gruppe_curs);
                         if (IsDoublenull (rab_proz)) rab_proz = (double) 0.0;
                         if (me_kz == 2)
						 {
							 rab = (double) (pr_ek / inh) * (rab_proz / 100);
						 }
						 else
						 {
							 rab = (double) pr_ek * (rab_proz / 100);
						 }
			  }
              else if (strcmp (lief.rab_abl, "K") == 0)
			  {
                         dsqlstatus = fetchrab (kette_curs);
                         if (IsDoublenull (proz_sum)) proz_sum = (double) 0.0;
                         if (me_kz == 2)
						 {
							 rab = (double) (pr_ek / inh) * (proz_sum / 100);
						 }
						 else
						 {
							 rab = (double) pr_ek * (proz_sum / 100);
						 }
			  }
              else if (strcmp (lief.rab_abl, "L") == 0)
			  {
				       rab  = (double) GetRabAEk 
				                                      (mdn, fil, 
			                                           lieferant,a,ag,wg, 
	  						                           inh, pr_ek, me_kz,lief.lst_nr);

			  }
			  DbClass.sqlclose (gruppe_curs);
			  DbClass.sqlclose (kette_curs);
			  return rab;
}


double WE_PREISE::GetRabAEk (short mdn, short fil, char *lieferant, double a, int ag, short wg,  
							 double inh, double pr_ek, int me_kz, int lst_nr)
/**
Artikelrabatte verrechnen.
**/
{
	          int liste_a_curs;
 	          int dsqlstatus;
			  double rab;
			  double nr;
			  char rab_st [5];
			  char rab_typ [3];
          
              rab = 0.0;
              rmdn = mdn;
			  rfil = fil;
              DbClass.sqlin ((short *) &rmdn, 1, 0); 
              DbClass.sqlin ((short *) &rfil, 1, 0); 
              DbClass.sqlin ((long *)  &lst_nr, 2, 0); 
              DbClass.sqlin ((double *)  &nr, 3, 0); 
              DbClass.sqlin ((char *)  rab_typ, 0, 4); 
              DbClass.sqlout ((double *)  &rab, 3, 0); 
              DbClass.sqlout ((char *)  rab_st, 0, 4); 
              liste_a_curs = DbClass.sqlcursor ("select rab_wrt,rab_st from lief_r_lis "
				                              "where mdn = ? "
											  "and fil = ? "
											  "and lst_nr = ? "
											  "and a = ? "
											  "and rab_typ = ? "); 


              sprintf (rab_typ, "%s", "AR");
			  nr = a;
              dsqlstatus = fetchrab (liste_a_curs);
              if (IsDoublenull (rab)) rab = (double) 0.0;
              if (strcmp(clipped(rab_st), "%") == 0)
			  {
				if (me_kz == 2)
				{
					rab = (double) (pr_ek / inh) * (rab / 100);
				}
				else
				{
					rab = (double) pr_ek * (rab / 100);
				}
			  }
			  else
			  {
				  rab = rab / inh;
			  }
			  if (dsqlstatus == 100)
			  {
	              rmdn = mdn;
				  rfil = fil;
	              sprintf (clipped(rab_typ), "%s", "AG");
				  nr = (double) ag;
	              dsqlstatus = fetchrab (liste_a_curs);
		          if (IsDoublenull (rab)) rab = (double) 0.0;
		          if (strcmp(clipped (rab_st), "%") == 0)
				  {
				      if (me_kz == 2)
					  {
						rab = (double) (pr_ek / inh) * (rab / 100);
					}
					else
					{
						rab = (double) pr_ek * (rab / 100);
					}
				  }
				  else
				  {
					  rab = rab / inh;
				  }
			  }
			  if (dsqlstatus == 100)
			  {
	              rmdn = mdn;
				  rfil = fil;
	              sprintf (rab_typ, "%s", "WG");
				  nr = (double) wg;
	              dsqlstatus = fetchrab (liste_a_curs);
		          if (IsDoublenull (rab)) rab = (double) 0.0;
	              if (strcmp(clipped(rab_st), "%") == 0)
				  {
				    if (me_kz == 2)
					{
						rab = (double) (pr_ek / inh) * (rab / 100);
					}
					else
					{
						rab = (double) pr_ek * (rab / 100);
					}
				  }
				  else
				  {
				 		rab = rab / inh;
				  }
			  }
			  DbClass.sqlclose (liste_a_curs);

              return rab;
}
