#ifndef _WE_TXT_DEF
#define _WE_TXT_DEF

#include "dbclass.h"

struct WE_TXT {
   long      nr;
   char      txt[61];
   long      zei;
};
extern struct WE_TXT we_txt, we_txt_null;

#line 7 "we_txt.rh"

class WE_TXT_CLASS : public DB_CLASS 
{
       private :
               int del_cursor_posi;
               void prepare (void);
       public :
               WE_TXT_CLASS () : DB_CLASS ()
               {
                         del_cursor_posi = -1;
               }
               int dbreadfirst (void);
               int delete_wepposi (void); 
};
#endif
