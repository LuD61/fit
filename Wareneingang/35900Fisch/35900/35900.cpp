//021109 Schwundabzug
//290709 Preise hier nicht mehr holen , sind schon da aus der Bestellung !!!
	     //Chargeneingabe > 4 Stellen ging nicht mehr 
//200709 bei nur Drucken , wird schon we_jour erzeugt und we_pos gel�scht
//190709 Bestandsbuchung jetzt hier, nicht mehr in weldr.rsr (M�fli)

//130509 Sollmenge in der Wiegemaske : angezeigt wird kg-wert aus der Bestellung 
//120908 es soll nur auf best. Abteilungen zugrgriffen werden
//110908 Temperaturen und PH speichern
//110908 Scannen auch mit charg_hand = 1
//100908 qswetext jetzt mit 1:1 beziehung zu we_kopf , wenn Anzahl Positionen < 11 
//100908 mit Bestellhinweis
//100908 bei Abbruch mit "Zur�ck"  darf nicht geschrieben werden bie QSKopf
//100908 wenn abgeschlossen, nicht nochmal schreiben
//100908 Bestellung darf erst aus Liste raus, wenn der entsprechende Wareneingang schon abgeschlossen ist
//090908 Bestellungen schon zu anfang aktivieren
//090908 cfg_ohne_preis auch �ber cfg
//090908 QsZwang : Direktflug ind die QS-Maske
#include<stdio.h>
#include<stdarg.h>
#include<string.h>
#include<windows.h>
#include<winbase.h>
#include "fit.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_curso.h"
#include "mo_menu.h"
#include "mo_expl.h"
#include "mo_draw.h"
#include "wmask.h"
#include "mo_meld.h"
#include "a_bas.h"
#include "a_hndw.h"
#include "ptab.h"
#include "dbfunc.h"
#include "fnb.h"
#include "mdn.h"
#include "fil.h"
#include "lief.h"
#include "we_kopf.h"
#include "we_pos.h"
#include "best_kopf.h"
#include "best_pos.h"
#include "a_pr.h"
#include "akt_krz.h"
#include "aktion.h"
#include "mo_a_pr.h"
#include "mo_geb.h"
#include "mo_numme.h"
#include "sys_peri.h"
#include "pht_wg.h"
#include "sys_par.h"
#include "mo_progcfg.h"
#include "datum.h"
#include "dbclass.h"
#include "mo_wepr.h"
#include "lief_bzg.h"
#include "mo_nr.h"
#include "we_wiepro.h"
#include "mo_gdruck.h"
#include "nublock.h"
#include "mo_qm.h"
#include "qswekopf.h"
#include "qswepos.h"
#include "a_kalkhndw.h"
#include "a_kalk_mat.h"
#include "a_kalkpreis.h"
#include "a_best.h"
#include "touchf.h"
#include "textblock.h"
#include "Text.h"
#include "Vector.h"
#include "rind.h"
#include "zerldaten.h"
#include "bsd_buch.h"
#include "mo_passw.h"
#include "scanean.h"
#include "Integer.h"
#include "we_txt.h"
#include "ls.h"
#include "lgr.h"
#include "a_lgr.h"
#include "prp_user.h"
#include "Sounds.h"
#include "resource.h"
#include "BkBitmap.h"

#define _CPP 1
#include "etubi.h"

static BOOL GraphikMode = FALSE;


// LOOK&Feel 18.11.2010 RoW
#define LOOK_AND_FEEL CLookAndFeel::GetInstance ()
BOOL NewLookAndFeel = FALSE;
BOOL LogoDynBackground = TRUE;
static CBkBitmap BkBitmap;
//COLORREF LogoBackground = RGB (0, 0, 128);
COLORREF LogoBackground = RGB (0, 100, 255);
HBRUSH hbrBackground = NULL;
BOOL DynColor = FALSE;

#define AUFMAX 1000
#define GEDRUCKT 2
#define VERBUCHT 3

#define ENTER 0
#define SUCHEN 1
#define AUSWAHL 2

#define BWS "BWS"
#define RSDIR "RSDIR"

#define sql_in ins_quest
#define sql_out out_quest

#define BAR 6
#define SOFORT 1

#define WIEGEN 9
#define AUSZEICHNEN 1
#define STOP 2
#define MINUS -1
#define PLUS 1

#define VK_CLS 910

#define STANDARD 0
#define ENTNAHME 1

static int screenwidth  = 800;
static int screenheight = 580;
static BOOL GebTara = FALSE;
static int  PassWord = FALSE;

static int EnterTyp = ENTER;
char Time[10];

static int break_lief_enter = 0;

static int ReadLS (void);
static char sqlstring [0x1000];
static BOOL stopauswahl   = FALSE;
static BOOL rdoptimize = FALSE;
static BOOL rdoptimizepos = FALSE;
static BOOL rdoptactive = TRUE;;
static BOOL query_changed = TRUE;
static int mit_trans = 1;
static int paz_direkt = 0;
static int auto_eti = 0;
static long auszeichner;
static int ls_charge_par = 0;
static int lsc2_par = 0;
static int multimandant_par = 0;
static long StdWaage;
static double AktPrWieg[1000];
static double StornoWieg;
static int AktWiegPos = 0;
static BOOL CalcOn = FALSE;
static int aufart9999;
static short auf_art = 0;
static BOOL best_ausw_direct = FALSE;
static BOOL liste_direct = FALSE;
static BOOL a_ausw_direct = FALSE;
static BOOL lief_ausw_direct = FALSE;
static BOOL hbk_direct = FALSE;
static int wieg_neu_direct = 0;
static BOOL wieg_direct = FALSE;
static BOOL hand_direct = FALSE;
static BOOL lief_bzg_zwang = FALSE;
static BOOL druck_zwang = FALSE;
static BOOL sort_a      = FALSE;
static BOOL hand_tara_sum = 0;
static BOOL fest_tara_sum = 0;
static BOOL fest_tara_anz = TRUE;
static BOOL EktoMeEinh = FALSE;
static long AutoSleep = 20;
static short QsZwang = FALSE;
static BOOL autocursor = TRUE;
static BOOL DrKomplett = TRUE;
static BOOL ScannMode = FALSE;
static BOOL InScanning = FALSE;
static double akt_buch_me;

static int WiegeTyp = STANDARD;
BOOL ScanMulti = FALSE;
int Schwundabzug = 0;

static char KomDatum [12];
char cha [21];

static char Zuruecktext[20];
  char zeilbuffer[80] ;

static char *Abteilungen   = NULL;
static BOOL WithSpezEti =  FALSE;
static BOOL WithLagerAuswahl =  TRUE;
static BOOL WithZusatz =  FALSE;
static short generiere_charge =  0;
static char *eti_kiste   = NULL;
static char *eti_nve   = NULL;
static char *eti_ez      = NULL;
static char *eti_ev      = NULL;
static char *eti_artikel = NULL;

static char *eti_ki_drk  = NULL;
static char *eti_nve_drk  = NULL;
static char *eti_ez_drk  = NULL;
static char *eti_ev_drk  = NULL;
static char *eti_a_drk   = NULL;

static int eti_ki_typ  = -1;
static int eti_nve_typ  = -1;
static int eti_ez_typ  = -1;
static int eti_ev_typ  = -1;
static int eti_a_typ   = -1;


static double pr_ek_bto0;
static double pr_ek0;
static double pr_ek_bto0_euro;
static double pr_ek0_euro;

static int testmode = 0;
extern BOOL ColBorder;
static BOOL colborder = FALSE;
static BOOL lsdefault = FALSE;
static BOOL multibest = TRUE;

static char LONGNULL []   = {(UCHAR) 0x00, (UCHAR) 0x00, (UCHAR) 0x00, (UCHAR) 0x80};
static long LongNull = 0x80000000;

static int best_koresp;
static char *korestab[10] =   {we_kopf.koresp_best0,
                               we_kopf.koresp_best1,
                               we_kopf.koresp_best2,
						       we_kopf.koresp_best3,
						       we_kopf.koresp_best4,
						       we_kopf.koresp_best5,
						       we_kopf.koresp_best6,
						       we_kopf.koresp_best7,
						       we_kopf.koresp_best8,
						       we_kopf.koresp_best9};

static long *termtab[10] =      {&we_kopf.best0_term,
                                 &we_kopf.best1_term,
                                 &we_kopf.best2_term,
					  	         &we_kopf.best3_term,
						         &we_kopf.best4_term,
						         &we_kopf.best5_term,
						         &we_kopf.best6_term,
						         &we_kopf.best7_term,
						         &we_kopf.best8_term,
						         &we_kopf.best9_term};

static long *lieftab[10] =      {&we_kopf.lief0_term,
                                 &we_kopf.lief1_term,
                                 &we_kopf.lief2_term,
					  	         &we_kopf.lief3_term,
						         &we_kopf.lief4_term,
						         &we_kopf.lief5_term,
						         &we_kopf.lief6_term,
						         &we_kopf.lief7_term,
						         &we_kopf.lief8_term,
						         &we_kopf.lief9_term};




static int dsqlstatus;




static int PtX = -1;
static int PtY = -1;
long akt_lager = 0;
long default_lager = 0;

//  Prototypen

int      PASCAL WinMain(HANDLE,HANDLE,LPSTR,int);
LONG FAR PASCAL WndProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL GebProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL FitLogoProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL UhrProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL EntNumProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL EntNuProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL TouchfProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL PreisProc(HWND,UINT, WPARAM,LPARAM);
LONG FAR PASCAL ListKopfProc(HWND,UINT, WPARAM,LPARAM);
LONG FAR PASCAL PtFussProc(HWND,UINT, WPARAM,LPARAM);
LONG FAR PASCAL MsgProc(HWND,UINT, WPARAM,LPARAM);
LONG FAR PASCAL PosLeerProc(HWND,UINT, WPARAM,LPARAM);
int             ShowWKopf (HWND, char * , int, char *,
                 int, form *, form *, form *);
void            fetch_std_geb (void);
void            update_kun_geb (void);
long            GetStdAuszeichner (void);
long            GetStdWaage (void);
int             AuswahlSysPeri (void);
int             AuswahlKopf (void);
int				AuswahlLager (int);
int				AuswahlALager (int,bool);
int				AuswahlLager (void);
void            IsDblClck (int);
void            GetAufArt9999 (void);
void            TestBestMulti (void);
int             WriteLS (void);
char *          GetErfGewKz (short, short, double);
char *          GetLiefBest (void);
void            DestroyKistBmp (void);
void            ReadKopftext (void);
int             ReadPr (void);
int             Reada_pr (void);
int		ReadWEBestell (int);
int		TestWE (void);
int DoScann (void);
void Nachdruck(void);

void BucheBsd (double a,char *lief_best, double auf_me, double auf_stk, short me_einh_lief, double pr_ek_bto , char *chargennr, char *identnr);
int             ShowPtBox (HWND, char *, int, char *, int, form *,
                           form *, form *);
int AuswahlWiegArt (void);
int AuswahlAbteilung (void);
int AuswahlWiePro (void);

int IsWiegMessage (MSG *);
void InitAufKopf (void);

static     void SelectAktCtr (void);
void            InitFirstInstance(HANDLE);
BOOL            InitNewInstance(HANDLE, int);
int             ProcessMessages(void);
LONG            MenuJob(HWND, WPARAM, LPARAM);
void            AuftragKopf (int);
void            LiefKopf (void);
void            EnterNumBox (HWND, char *, char *, int, char *);
void            EnterCalcBox (HWND, char *, char *, int, char *);
void            EnterPreisBox (HWND, int, int);
void            EnterListe (void);
void            ArtikelWiegen (HWND);
void            ArtikelHand (void);
void            WriteRind ();
void            WaageInit (void);
static int      DbAuswahl (short,void (*) (char *), void (*) (char *));
void            SetStorno (int (*) (void), int);
int             ShowFestTara (char *);
int             SetKomplett (void);
void            DisplayLiefKopf (void);
void            CloseLiefKopf (void);
void            LiefFocus (void);
int             print_messG (int, char *, ...);
void            disp_messG (char *, int);
int             abfragejnG (HWND, char *, char *);
int             AuswahlWieg (void);
void            fnbexec (int, char *[]);
BOOL            TestPosStatus (void);
BOOL            LsLeerGut (void);
static void     SetEti (double, double, double);
static void     SetEtiStk (double);
void            setautotara (BOOL);
void            EnterLeerPos (void);
void	 	    GetGebGewicht (char *);
int             deltaraw (void);
BOOL            TestQs (BOOL);
BOOL            TestQsPosA (double);
BOOL            TestQsPos (void);
void            WriteParamKiste (int);
void            WriteParamEZ (int);
void            WriteParamEV (int);
void            WriteParamArt (int);

void            EnterRCharge (int);
void            EnterREz (int);
void            EnterREs (int);
void            EnterLand (int);
int             ShowTierLand (char *);
void            BucheChargenBsd ();
void            SetScannMode ();
int 		    ScanEan128Charge (Text& Ean, BOOL Ident);
int             ChangeChar (int key);
void            DrawNoteIcon ();
void            DrawNoteIcon (HDC hdc);
void            SetListRowProc (void (*) (HDC, RECT *, int));
long            GenWepTxt (void);
void            DrawRowNoteIcon (HDC hdc, RECT *rect, int idx);
int             AuswahlTemperature (void);
void            DoChargeZwang (void);
void GenLief_rech_nr (short di);
static          int TestNewArt (double, long);
int				TestNewArtCharge (double a, Text& ListCharge);
// LOOK&Feel 18.11.2010 RoW
void			NewAufKopfAttributes ();
void			NewWeKopfAttributes ();


int artikelOK (char *buffer, BOOL DispMode=TRUE);
int artikelOK (double a, BOOL DispMode=TRUE)
{
	char buffer [20];
	sprintf (buffer, "%.0lf", a);
	return artikelOK (buffer, DispMode);
}


int ScanA (void);
int ScanArtikel (double a);
int ScanEan128 (Text& Ean128, BOOL DispMode=TRUE);
char *GenCharge (void);


BOOL            Lock_Lsp (void);
static int      la_zahl = 0;
static int      AktKopf = 0;


static BOOL SmallScann = TRUE;
static SCANEAN Scanean ((Text) "scanean.cfg");
static long scangew = 0l;
CEan128Date Ean128Date;
static BOOL Scannen = FALSE;
static int Scanlen = 14;
static BOOL ScanMode = FALSE;
static BOOL BreakScan = FALSE;
static BOOL ScanAFromEan = TRUE;
static int Ean128Eanlen = 12;
static BOOL ChargeZwang = FALSE;
static BOOL LagerZwang = TRUE;
static BOOL ScanCharge = FALSE;
static BOOL MultiTemperature = TRUE;
static BOOL MesswerteZwang = FALSE;

//  Globale Variablen


HWND ForegroundWindow;

void GetForeground (void)
{
	ForegroundWindow = GetForegroundWindow ();
}

void SetForeground (void)
{
	SetForegroundWindow (ForegroundWindow);
}


HWND     hMainWindow;
HWND     hFitWindow = NULL;
HWND     BuDialog1;

HWND     BackWindow1;
HWND     BackWindow2;
HWND     BackWindow3;

HWND     LogoWindow;

HWND     FitWindow = NULL;
HWND     UhrWindow = NULL;
HWND     MessWindow = NULL;
HANDLE   hMainInst;
static   HWND eWindow = NULL;
static   HWND InsWindow = NULL;
static   HWND PtWindow = NULL;
static   HWND eKopfWindow = NULL;
static   HWND eFussWindow = NULL;
static   HWND PtFussWindow = NULL;
static   HWND GebWindow = NULL;
static   HWND GebWindButton = NULL;

LPSTR    lpszMainTitle = "TOUCH Wareneingang ";
LPSTR    lpszClassName = "fit";
HBITMAP  fitbmp;
HBITMAP   pfeill;
HBITMAP   pfeilo;
HBITMAP   pfeilu;
HBITMAP   pfeilr;
HBITMAP   hbmOld;
HDC       hdc;
HDC       hdcMemory;
BITMAP    bm;
BITMAP    fitbm;
BITMAP    hfitbm;
HDC       fithdc;
int       procwait = 0;
int       IsFhs = 0;
BOOL      LockAuf = 1;
BOOL      AufLock = 0;
double scrfx, scrfy;
static int sysxplus = 50;
int Sebmpw;
int Sebmph;

/*
int lmx = 10;
int lmx2 = 0;
int lmy = 50;
int lmxs = 20;
int lmys = 0;
*/


//WA_PREISE WaPreis;
WE_PREISE WePreis;
AB_PREISE AbPreise;

static BOOL KistPaint = FALSE;
static BMAP *Kist;
static BMAP *KistG;
HBITMAP KistBmp;
HBITMAP KistBmpG;
POINT kistpoint;
RECT KistRect;
BOOL KistCol = FALSE;

static BMAP *KistMask;
HBITMAP KistMaskBmp;

HBRUSH LtBlueBrush;
HBRUSH YellowBrush;



static char waadir [128] = {"C:\\waakt"};
static char ergebnis [128] = {"C:\\waakt\\ergebnis"};
static int WaaInit = 0;
static HANDLE waaLibrary = NULL;
static char *fnbname = "fnb";
static char *fnbInstancename = "setzefnbInstance";

int (*fnbproc) (int, char **);
int (*fnbInstanceproc) (void *);
static BOOL neuer_we;

static DWORD GebColor = RGB (0, 0, 255);

/* Globale Parameter fuer printlogo                   */

static long twait = 50;
static long twaitU = 500;
static int plus =  3;
static int xplus = 3;
static int ystretch = 0;
static int xstretch = 0;
static int logory = 0;
static int logorx = 1;
static int sign = 1;
static int signx = 1;

static short akt_mdn;
static short akt_fil;

static char fitpath [256];
static char rosipath [256];
static char progname [256];

struct MENUE scmenue;
struct MENUE leermenue;

static int ListeAktiv = 0;
static int PtListeAktiv = 0;
static int NumEnterAktiv = 0;
static int PreisAktiv = 0;
static int KopfEnterAktiv = 0;
static int WiegenAktiv = 0;
static int LiefEnterAktiv = 0;
static int KomplettAktiv = 0;
static int WorkModus = WIEGEN;
static HWND NumEnterWindow;
static HWND AufKopfWindow;
static HWND WiegWindow;



WE_KOPF_CLASS WeKopfClass;
WE_POS_CLASS WePosClass;
BEST_KOPF_CLASS BestKopfClass;
BEST_POS_CLASS BestPosClass;
LIEF_CLASS LiefClass;
class LGR_CLASS lgr_class;
class A_LGR_CLASS a_lgr_class;
class PRP_USER_CLASS Prp_user;

class LS_CLASS ls_class;
//class LSPT_CLASS lspt_class;
// class KUN_CLASS kun_class;
static int      enterpass = 0;
static TEXTBLOCK *textblock = NULL;
class FIL_CLASS fil_class;
class MDN_CLASS mdn_class;
class ADR_CLASS adr_class;
class PTAB_CLASS ptab_class;
class MENUE_CLASS menue_class;
//class EINH_CLASS einh_class;
class HNDW_CLASS hndw_class;
class A_KALKPREIS_CLASS A_kalkpreis;
class GEB_CLASS geb_class;
class SYS_PERI_CLASS sys_peri_class;
class PHT_WG_CLASS pht_wg_class;
class SYS_PAR_CLASS sys_par_class;
static DB_CLASS DbClass;
static PROG_CFG ProgCfg ("35900");
static BMAP bMap;
static AutoNrClass AutoNr;
static WE_WIEPRO_CLASS WeWiePro;
static LIEF_BZG_CLASS LiefBzg;
static NUBLOCK NuBlock;
static TOUCHF Touchf;
static QSWEKOPF_CLASS QsWeKopf;
static QSWEPOS_CLASS QsWePos;
static QMKOPF *Qmkopf;
static RIND_CLASS Rind;
static ZERLDATEN_CLASS Zerldaten;
static CVector Chargen;
static BSD_BUCH_CLASS BsdBuch;
static WE_TXT_CLASS cWept;

static int tara_kz;
static int mhd_kz;

HICON NoteIcon = NULL;
BOOL DrawNote = FALSE;

// HMENU hMenu;
static char *mentab[] = {scmenue.menue1,
                         scmenue.menue2,
                         scmenue.menue3,
                         scmenue.menue4,
                         scmenue.menue5,
                         scmenue.menue6,
                         scmenue.menue7,
                         scmenue.menue8,
                         scmenue.menue9,
                         scmenue.menue0};
static char *zeitab [] = {scmenue.zei1,
                          scmenue.zei2,
                          scmenue.zei3,
                          scmenue.zei4,
                          scmenue.zei5,
                          scmenue.zei6,
                          scmenue.zei7,
                          scmenue.zei8,
                          scmenue.zei9,
                          scmenue.zei0};

static char *mentab0[] = {menue.menue1,
                          menue.menue2,
                          menue.menue3,
                          menue.menue4,
                          menue.menue5,
                          menue.menue6,
                          menue.menue7,
                          menue.menue8,
                          menue.menue9,
                          menue.menue0};
static char *zeitab0[] = {menue.zei1,
                          menue.zei2,
                          menue.zei3,
                          menue.zei4,
                          menue.zei5,
                          menue.zei6,
                          menue.zei7,
                          menue.zei8,
                          menue.zei9,
                          menue.zei0};


static HMENU hMenu;

/* Masken fuer Logofenster                                 */

mfont fittextlogo = {"Arial", 1000, 0, 3, RGB (255, 0, 0),
                                               RGB (0, 255, 255),
                                               1,
                                               NULL};

mfont fittextlogo2 = {"Arial", 200, 0, 3, RGB (0, 0, 255),
                                      RGB (0, 255, 255),
                                      1,
                                      NULL};

mfont buttonfont = {"", 90, 0, 1,
                                       RGB (0, 255, 255),
                                       BLUECOL,
                                       1,
                                       NULL};

mfont uhrfont     = {"Arial", 80, 0, 1,
                                       BLACKCOL,
                                       LTGRAYCOL,
                                       1,
                                       NULL};

field _fittext[] = {"     f i t", 0, 0, 50, -1, 0, "", DISPLAYONLY, 0, 0, 0};

form fittext = {1, 0, 0, _fittext, 0, 0, 0, 0, &fittextlogo};

field _fittext2[] = {"in Frische und Fleisch", 0, 0, 250, -1, 0, "",
                       DISPLAYONLY, 0, 0, 0};

form fittext2 = {1, 0, 0, _fittext2, 0, 0, 0, 0, &fittextlogo2};

static int menu_ebene = 0;
static int cfg_ohne_preis = 0;
static int cfg_MehrfachChargen = 1;
static int DruckAuswahl = 1;
static int immer_preis = 0;
static int ls_leer_zwang = 0;

struct FITMENU
{
           char menutext[80];
           char menuprog[256];
           int progtyp;
           int start_mode;
};

struct FITMENU fitmenu [11];

static char auftrag_nr [22];
static char bestell_nr [22];
static char lief_nr [22];
static char kun_nr [22];
static char lieferant [22];
static char kun_krz1 [17];
static char lief_krz1 [17];
static char bestellhinweis [60];
static char lieferdatum  [22];
static char beldatum  [22];
static char komm_dat [22];
static char lieferzeit   [22];
static char ls_stat [22];
static char ls_stat_txt [27];
static char LAN [22];

static int menfunc1 (void);
static int menfunc2 (void);
static int menfunc3 (void);
static int menfunc4 (void);
static int menfunc5 (void);
static int menfunc6 (void);
static int menfunc7 (void);
static int menfunc8 (void);
static int menfunc9 (void);
static int menfunc10 (void);
static int menfunc0 (void);
static int menfuncLgr (void);
static int menfuncALgr (void);

static int func1 (void);
static int func2 (void);
static int func3 (void);
static int func4 (void);
static int func5 (void);
static int func6 (void);
static int func7 (void);
static int func8 (void);


static short Mandant = 1;
static short Filiale = 0;

static char *Mtxt = "Mandant";
static char *Ftxt = "Filiale";

static char Mandanttxt[20];
static char Abteilungtxt[20];
static char Lagertxt[30];
static char Buchartikeltxt[30];
static char Filialetxt[20];


COLORREF BuBkColor1 = BLUECOL;


ColButton Ende = {"Ende", -1, -1,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   RGB (0, 255, 255),
                   BuBkColor1,
                   TRUE};


ColButton Auftrag  =   {"&Auftrag-Nr", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
						 BuBkColor1,
                         -1};

ColButton Lieferschein  =   {"&Lieferschein", -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               RGB (0, 255, 255),
							   BuBkColor1,
                               TRUE};

ColButton Liste    = {
                        "L&iste", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
						 BuBkColor1,
                         -1};

ColButton Suchen   =  {"&Suchen", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
						 BuBkColor1,
                          -1};



ColButton Bestellung   =  {"&Bestellungen", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
						 BuBkColor1,
                         -1};


ColButton QmKopf   =  {"&QM Kopf", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
						 BuBkColor1,
                         -1};


ColButton Geraet = {
                        "&Ger�t", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
						 BuBkColor1,
                         TRUE};

ColButton WiegArt = {
                        "&Wiegart", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
						 BuBkColor1,
                         TRUE};
ColButton ColAbteilung = {
                        Abteilungtxt, -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
						 BuBkColor1,
                         TRUE};

ColButton ColLager = {
                        Lagertxt, -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
						 BuBkColor1,
                         TRUE};

ColButton CMandant = {
                         Mandanttxt, -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
						 BuBkColor1,
                         TRUE};

ColButton CFiliale = {
                         Filialetxt, -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
						 BuBkColor1,
                         TRUE};

ColButton KDatum    =  {"&Komm-Datum", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
						 BuBkColor1,
                         -1};

ColButton Vorratsauftrag =  {
                        "Vor.Auftrag", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
						 BuBkColor1,
                         -1};

ColButton BestInv =  {
                        "&Bestand, Inventur", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
						 BuBkColor1,
                         TRUE};

ColButton Auswertungen =  {
                        "Auswer&tungen", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton Anwendungen =  {
                        "Anwen&dungen", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton Systemfunktionen =  {
                        "Systemf&unktionen", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton Benutzer =  {
                        "Ben&utzer", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};



ColButton Hilfe =  {
//                        "Hilfe &?", -1, -1,
                        "Startwiegung", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         -1};


ColButton Hauptmenue =  {
                        "Hauptmen&�", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton Back  =  {
                        Zuruecktext, -1, -1,
//                        "Zur�ck F5", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton BuOK = {"OK", -1, -1,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   RGB (0, 255, 255),
                   BLUECOL,
                   -1};

ColButton BuOKS = {"OK", -1, -1,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   RGB (0, 255, 255),
                   BLUECOL,
                   10};

ColButton BuKomplett =
                  {"komplett", -1, -1,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   RGB (0, 255, 255),
                   REDCOL,
                   -1};


ColButton PfeilL       =  {
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         pfeill, -1, -1,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         10};

ColButton PfeilO     =  {
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         pfeilo, -1, -1,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         10};

ColButton PfeilU     =  {
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         pfeilu, -1, -1,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         10};

ColButton PfeilR     =  {
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         pfeilr, -1, -1,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         10};


static int bsb  = 85;

static int bss0 = 73;
static int bss  = 77;
static int bsz  = 40;
static int bsz0 = 36;
static int AktButton = 0;


field _MainButton0[] = {
(char *) &Lieferschein,     128, bsz0, 115, -1, 0, "", COLBUTTON, 0,
                                                     menfunc3, 0,
(char *) &Liste,            128, bsz0, 115 + 1 * bsz, -1, 0, "", COLBUTTON, 0,
                                                     menfunc2, 0,
(char *) &Bestellung,       128, bsz0, 115 + 2 * bsz, -1, 0, "", COLBUTTON, 0,
                                                     menfunc8, 0,
(char *) &Geraet,           128, bsz0, 115 + 3 * bsz, -1, 0, "", COLBUTTON, 0,
                                                     menfunc6, 0,
(char *) &QmKopf,           128, bsz0, 115 + 4 * bsz, -1, 0, "", COLBUTTON, 0,
                                                     menfunc7, 0,
(char *) &WiegArt,          128, bsz0, 115 + 5 * bsz, -1, 0, "", COLBUTTON, 0,
                                                     menfunc9, 0,
(char *) &ColAbteilung,        128, bsz0, 115 + 6 * bsz, -1, 0, "", COLBUTTON, 0,
                                                     menfunc10, 0,
(char *) &CMandant,         128, bsz0, 115 + 7 * bsz, -1, 0, "", COLBUTTON, 0,
                                                     menfunc4, 0,
(char *) &CFiliale,         128, bsz0, 115 + 8 * bsz, -1, 0, "", COLBUTTON, 0,
                                                     menfunc5, 0,
/*
(char *) &ColLager,        128, bsz0, 115 + 9 * bsz, -1, 0, "", COLBUTTON, 0,
                                                     menfuncLgr, 0,
*/
(char *) &Ende,             128, bsz0, 130 + 9 * bsz,  -1, 0, "", COLBUTTON, 0,
                                                          menfunc0, 0};
form MainButton = {10, 0, 0, _MainButton0, 0, 0, 0, 0, &buttonfont};
//form MainButton = {11, 0, 0, _MainButton0, 0, 0, 0, 0, &buttonfont};

// LOOK&Feel 18.11.2010 RoW
void SetDynBkColor ()
{
	BuBkColor1 = LOOK_AND_FEEL->GetDynColorBlue ();
	for (int i = 0; i < MainButton.fieldanz; i ++)
	{
		ColButton *Col = (ColButton *) MainButton.mask[i].feld;
		Col->BkColor = BuBkColor1;
	}
	NUBLOCK::SetDynBkColor ();
}


field _MainButton2[] = {
(char *) &Hilfe,             bss0, bsz0, 5, 4+7*bss, 0, "", COLBUTTON, 0,
                                                              func7, 0,
(char *) &Back,              bss0, bsz0, 5, 4+6*bss, 0, "", COLBUTTON, 0,
 *                                                              func5, 0,
(char *) &BuOK,              bss0, bsz0, 5, 4+5*bss, 0, "", COLBUTTON, 0,
 *                                                              func6, 0,
(char *) &BuKomplett,        bss0, bsz0, 5, 4+4*bss, 0, "", COLBUTTON, 0,
 *                                                              func8, 0,
(char *) &PfeilR,            bss0, bsz0, 5, 4+3*bss, 0, "", COLBUTTON, 0,
                                                          func4, 0,
(char *) &PfeilU,            bss0, bsz0, 5, 4+2*bss, 0, "", COLBUTTON, 0,
                                                          func3, 0,
(char *) &PfeilO,            bss0, bsz0, 5, 4+1*bss, 0, "", COLBUTTON, 0,
                                                          func2, 0,
(char *) &PfeilL,            bss0,  bsz0,5, 4,     0, "", COLBUTTON, 0,
                                                          func1, 0};

form MainButton2 = {8, 0, 0, _MainButton2, 0, 0, 0, 0, &buttonfont};

static char datum [12];
static char zeit [12];
static char datumzeit [24];

/*
static field _uhrform [] = {
datum,            0, 0,  5, -1, 0, "", DISPLAYONLY, 0, 0, 0,
zeit,             0, 0, 20, -1, 0, "", DISPLAYONLY, 0, 0, 0};
*/

static field _uhrform [] = {
datumzeit,        0, 0,  -1, -1, 0, "", DISPLAYONLY, 0, 0, 0};

static form uhrform = {1, 0, 0, _uhrform, 0, 0, 0, 0, &uhrfont};


struct AUFP_S
{
        char pnr [5];
        char s [2];
        char a [14];
        char a_best [17];
        char a_bz1 [25];
        char a_bz2 [25];
        char a_me_einh [5];
        char auf_me [13];
        char lief_me [13];
        char auf_me_vgl [13];
        char tara [13];
        char auf_me_bz [10];
        char lief_me_bz [10];
        char auf_pr_vk [13];
        char auf_lad_pr [13];
        char basis_me_bz [7];
        char erf_kz [2];
        char rest [13];
        char ls_charge [31];
        char lsp_txt [10];
        char me_einh_kun [5];
        char me_einh [5];
        char hbk_date [11];
        char erz_date [11];
        char ls_vk_euro [13];
        char ls_vk_fremd [13];
        char ls_lad_euro [13];
        char ls_lad_fremd [13];
        char rab_satz [10];
		char hnd_gew [2];
		char erf_gew_kz [2];
		char anz_einh [13];
		char inh [13];
		char temp [10];
		char temp2 [10];
		char temp3 [10];
		char ph_wert [10];
        char pr_fil_ek [10];
        char pr_fil_ek_eu [10];
		int  me_kz;
		double min_best;
		char ident_nr [21];
		char ez_nr [11];
		char es_nr [11];
		char laender [22];
		char txtschwundabzug [30];   
		int lager;
		char a_krz [13];
};

struct AUFP_S aufps, aufp_null;
struct AUFP_S aufparr [AUFMAX];
int aufpanz = 0;
int aufpidx = 0;
int aufpanz_upd = 0;
static char auf_me [12];



//******************************** SCANNEN ******************************************
//******************************** SCANNEN ******************************************
//******************************** SCANNEN ******************************************
class CScanMessage
{
public:
	CSounds Sounds;
	BOOL Extern;
	Text ErrorName;
	Text OkName;
	Text ScanOkName;
	Text CompleteName;
	CScanMessage ()
	{
		char *etc;
		Extern = FALSE;
		etc = getenv ("BWSETC");
		if (etc != NULL)
		{
			ErrorName.Format ("%s\\wav\\Error.wav", etc);
			ScanOkName.Format ("%s\\wav\\ScanOK.wav", etc);
			OkName.Format ("%s\\wav\\OK.wav", etc);
			CompleteName.Format ("%s\\wav\\Complete.wav", etc);
		}
		else
		{
			ErrorName = "";
			ScanOkName = "";
			OkName = "";
			CompleteName = "";
		}

	};

	virtual void Error ()
	{
		if (Extern)
		{
			Sounds.SetSoundName (ErrorName);
			Sounds.Play ();
		}
		else
		{
			Sounds.Play (IDR_ERROR);
		}
			
	}

	virtual void ScanOK ()
	{
		if (Extern)
		{
			Sounds.SetSoundName (ScanOkName);
			Sounds.Play ();
		}
		else
		{
			Sounds.Play (IDR_SCANOK);
		}
	}

	virtual void OK ()
	{
		if (Extern)
		{
			Sounds.SetSoundName (OkName);
			Sounds.Play ();
		}
		else
		{
			Sounds.Play (IDR_OK);
		}
	}

	virtual void Complete ()
	{
		if (Extern)
		{
			Sounds.SetSoundName (CompleteName);
			Sounds.Play ();
		}
		else
		{
			Sounds.Play (IDR_COMPLETE);
		}
	}
};

CScanMessage ScanMessage;


class CScanValues 
{
public:
	double a;
	double eangew;
	double gew;
	Text Charge;
	Text Mhd;
	double auf_me;
	Text ACharge;
	BOOL aflag;
	BOOL gewflag;
	BOOL ChargeFlag;
	BOOL MhdFlag;
	CScanValues ()
	{
		a = 0.0;
		eangew = 0.0;
		gew = 0.0;
        Charge = "";
		Mhd = "";
		auf_me = 0;
		ACharge = "";
		aflag = FALSE;
		gewflag = FALSE;
		ChargeFlag = FALSE;
		MhdFlag = FALSE;
	}

	void Init ()
	{
		eangew = 0.0;
		a = 0.0;
        Charge = "";
		Mhd = "";
		aflag = FALSE;
		gewflag = FALSE;
		ChargeFlag = FALSE;
		MhdFlag = FALSE;
	}

	void SetA (double a)
	{
		this->a = a;
		aflag = TRUE;
	}

	void SetGew (double gew)
	{
		this->eangew = gew;
		this->gew += gew;
		gewflag = TRUE;
	}

	void SetCharge (Text& Charge)
	{
		this->Charge = Charge;
		ChargeFlag = TRUE;
	}

	void SetMhd (Text& Mhd)
	{
		this->Mhd = Mhd;
		MhdFlag = TRUE;
	}

	void IncAufMe ()
	{
		auf_me ++;
	}

	BOOL SetACharge ()
	{
		if (ACharge != "")
		{
			if (ACharge != Charge) return FALSE;
// Sp�ter eventuell eine neue Position erzeugen
			ACharge = Charge;
			return TRUE;
		}
        ACharge = Charge;
		return TRUE;
	}

	BOOL ScanOK ()
	{
		if (!aflag) return FALSE;
		if (!gewflag) return FALSE;
		if (!ChargeFlag) return FALSE;
		return TRUE;
	}

	BOOL ScanValuesOK ()
	{
		if (a == 0.0) return FALSE;
		if (gew == 0.0) return FALSE;
		if (Charge == "") return FALSE;
		return TRUE;
	}
};

CScanValues *ScanValues = NULL;
















struct WIEP
{
    char a [15];
    char a_bz1 [26];
    char brutto [12];
    char dat [15];
    char zeit [10];
}; 

struct WIEP wiep, wieparr[50];

static int wiepidx;
static int wiepanz = 0;

int GetAktButton (void)
/**
Aktiven Button holen.
**/
{
        HWND hWnd;
        int i;

        hWnd = GetFocus ();

        for (i = 0; i < MainButton.fieldanz; i ++)
        {
                    if (hWnd == MainButton.mask[i].feldid)
                    {
                               return (i);
                    }
        }

        for (i = 0; i < MainButton2.fieldanz; i ++)
        {
                    if (hWnd == MainButton2.mask[i].feldid)
                    {
                            return (MainButton.fieldanz + i);
                    }
        }
        return (AktButton);
}

field *GetAktField ()
/**
Field zu AktButton holen.
**/
{
        if (AktButton < MainButton.fieldanz)
        {
                      return (&MainButton.mask [AktButton]);
        }
        return (&MainButton2.mask[AktButton - MainButton.fieldanz]);
}


BOOL TestOK (void)
/**
Test, ob OK-Button aktiv ist.
**/
{
	     if (BuOK.aktivate > -1)
		 {
			 return TRUE;
		 }
		 return FALSE;
}








//=====================================================
//============== Fangart =========================== 
//=====================================================

struct FANGART
{
	short aktiv;
    char nr [10];
    char bz [33];
    char bzk [25];
};


struct FANGART fangartarr[50], fangart;

static int fangartidx;
static int fangartanz = 0;


mfont FangFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                               RGB (255, 255, 255),
                               1,
                               NULL};

static field _fangartform [] =
{
        fangart.bzk,    6, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        fangart.bz,     20, 1, 0,7, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form ffangartform = {2, 0, 0, _fangartform, 0, 0, 0, 0, &FangFont};

static field _ffangartub [] =
{
        "Wert",              6, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        "Fangart",      20, 1, 0,7, 0, "", DISPLAYONLY, 0, 0, 0
};

static form ffangartub = {2, 0, 0, _ffangartub, 0, 0, 0, 0, &FangFont};

static field _ffangartvl [] =
{ 
	"1",                  1,  0, 0, 6, 0, "", NORMAL, 0, 0, 0,
};

static form ffangartvl = {1, 0, 0, _ffangartvl, 0, 0, 0, 0, &FangFont};


void HoleFangart (double artikel)
{
	int  aktiv;
		char *c;
		Token fa;
		char cfangart[513];
		char cText [12] = {"Fangmethode"};

		if (strcmp (clipped(prp_user.name1), cText) == 0) strcpy(cfangart,prp_user.wert1);
		if (strcmp (clipped(prp_user.name2), cText) == 0) strcpy(cfangart,prp_user.wert2);
		if (strcmp (clipped(prp_user.name3), cText) == 0) strcpy(cfangart,prp_user.wert3);
		if (strcmp (clipped(prp_user.name4), cText) == 0) strcpy(cfangart,prp_user.wert4);
		if (strcmp (clipped(prp_user.name5), cText) == 0) strcpy(cfangart,prp_user.wert5);
		if (strcmp (clipped(prp_user.name6), cText) == 0) strcpy(cfangart,prp_user.wert6);
		if (strcmp (clipped(prp_user.name7), cText) == 0) strcpy(cfangart,prp_user.wert7);
		if (strcmp (clipped(prp_user.name8), cText) == 0) strcpy(cfangart,prp_user.wert8);
		if (strcmp (clipped(prp_user.name9), cText) == 0) strcpy(cfangart,prp_user.wert9);
		if (strcmp (clipped(prp_user.name10), cText) == 0) strcpy(cfangart,prp_user.wert10);
		if (strcmp (clipped(prp_user.name11), cText) == 0) strcpy(cfangart,prp_user.wert11);
		if (strcmp (clipped(prp_user.name12), cText) == 0) strcpy(cfangart,prp_user.wert12);
		if (strcmp (clipped(prp_user.name13), cText) == 0) strcpy(cfangart,prp_user.wert13);
		if (strcmp (clipped(prp_user.name14), cText) == 0) strcpy(cfangart,prp_user.wert14);
		if (strcmp (clipped(prp_user.name15), cText) == 0) strcpy(cfangart,prp_user.wert15);
		if (strcmp (clipped(prp_user.name16), cText) == 0) strcpy(cfangart,prp_user.wert16);
		if (strcmp (clipped(prp_user.name17), cText) == 0) strcpy(cfangart,prp_user.wert17);
		if (strcmp (clipped(prp_user.name18), cText) == 0) strcpy(cfangart,prp_user.wert18);
		if (strcmp (clipped(prp_user.name19), cText) == 0) strcpy(cfangart,prp_user.wert19);
		if (strcmp (clipped(prp_user.name20), cText) == 0) strcpy(cfangart,prp_user.wert20);
		if (strcmp (clipped(prp_user.name21), cText) == 0) strcpy(cfangart,prp_user.wert21);
		if (strcmp (clipped(prp_user.name22), cText) == 0) strcpy(cfangart,prp_user.wert22);
		if (strcmp (clipped(prp_user.name23), cText) == 0) strcpy(cfangart,prp_user.wert23);
		if (strcmp (clipped(prp_user.name24), cText) == 0) strcpy(cfangart,prp_user.wert24);
		if (strcmp (clipped(prp_user.name25), cText) == 0) strcpy(cfangart,prp_user.wert25);
		if (strcmp (clipped(prp_user.name26), cText) == 0) strcpy(cfangart,prp_user.wert26);
		if (strcmp (clipped(prp_user.name27), cText) == 0) strcpy(cfangart,prp_user.wert27);
		if (strcmp (clipped(prp_user.name28), cText) == 0) strcpy(cfangart,prp_user.wert28);
		if (strcmp (clipped(prp_user.name29), cText) == 0) strcpy(cfangart,prp_user.wert29);
		if (strcmp (clipped(prp_user.name30), cText) == 0) strcpy(cfangart,prp_user.wert30);
		fangartanz = 0;
        if (fangartanz == 0)
        {
               dsqlstatus = ptab_class.lese_ptab_all ("fangart");
               while (dsqlstatus == 0)
               {
				    aktiv = 0;
					if (_a_bas.bereich == atoi(ptabn.ptwert))
					{
						aktiv = 1;
					}
					else 
					{
						fa.SetSep (",");

						fa = clipped(cfangart);
						int i = 0;
						Text L2 = ptabn.ptbezk;
						L2.Trim ();
						while ((c = fa.NextToken ()) != NULL)
						{
							Text L1 = c;
							L1.Trim ();
							if (L1 == L2) 
							{
								aktiv = 1;
								break;
							}
						}
					}
					if (aktiv == 1)
					{
	                   strcpy (fangartarr[fangartanz].bz,  ptabn.ptbez);
		               strcpy (fangartarr[fangartanz].bzk,  ptabn.ptbezk);
			           strcpy (fangartarr[fangartanz].nr, ptabn.ptwert);
				       fangartanz ++;
					   if (fangartanz == 20) break;
					}
					dsqlstatus = ptab_class.lese_ptab_all ();
               }
        }
}



int ShowFangart (char *value)
{
        EnableWindow (WiegWindow, FALSE);
        fangartidx = ShowPtBox (WiegWindow, (char *) fangartarr, fangartanz,
                     (char *) &fangart,(int) sizeof (struct FANGART),
                     &ffangartform, &ffangartvl, &ffangartub);
        EnableWindow (WiegWindow, TRUE);
        strcpy (value, fangartarr [fangartidx].nr);
        return fangartidx;
}







//=====================================================
//============== Fanggebiet =========================== 
//=====================================================

struct FANGGEBIET
{
	short aktiv;
    char nr [10];
    char bz [33];
    char bzk [25];
};


struct FANGGEBIET fanggebietarr[50], fanggebiet;

static int fanggebietidx;
static int fanggebietanz = 0;



static field _fanggebietform [] =
{
        fanggebiet.nr,    6, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        fanggebiet.bz,     20, 1, 0,7, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form ffanggebietform = {2, 0, 0, _fanggebietform, 0, 0, 0, 0, &FangFont};

static field _ffanggebietub [] =
{
        "Wert",              6, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        "Fanggebiet",      20, 1, 0,7, 0, "", DISPLAYONLY, 0, 0, 0
};

static form ffanggebietub = {2, 0, 0, _ffanggebietub, 0, 0, 0, 0, &FangFont};

static field _ffanggebietvl [] =
{ 
	"1",                  1,  0, 0, 6, 0, "", NORMAL, 0, 0, 0,
};

static form ffanggebietvl = {1, 0, 0, _ffanggebietvl, 0, 0, 0, 0, &FangFont};


void HoleFanggebiet (double artikel)
{
	int  aktiv;
		char *c;
		Token fg;
		char cfanggebiet[513];
		char cText [11] = {"Fanggebiet"};

		if (strcmp (clipped(prp_user.name1), cText) == 0) strcpy(cfanggebiet,prp_user.wert1);
		if (strcmp (clipped(prp_user.name2), cText) == 0) strcpy(cfanggebiet,prp_user.wert2);
		if (strcmp (clipped(prp_user.name3), cText) == 0) strcpy(cfanggebiet,prp_user.wert3);
		if (strcmp (clipped(prp_user.name4), cText) == 0) strcpy(cfanggebiet,prp_user.wert4);
		if (strcmp (clipped(prp_user.name5), cText) == 0) strcpy(cfanggebiet,prp_user.wert5);
		if (strcmp (clipped(prp_user.name6), cText) == 0) strcpy(cfanggebiet,prp_user.wert6);
		if (strcmp (clipped(prp_user.name7), cText) == 0) strcpy(cfanggebiet,prp_user.wert7);
		if (strcmp (clipped(prp_user.name8), cText) == 0) strcpy(cfanggebiet,prp_user.wert8);
		if (strcmp (clipped(prp_user.name9), cText) == 0) strcpy(cfanggebiet,prp_user.wert9);
		if (strcmp (clipped(prp_user.name10), cText) == 0) strcpy(cfanggebiet,prp_user.wert10);
		if (strcmp (clipped(prp_user.name11), cText) == 0) strcpy(cfanggebiet,prp_user.wert11);
		if (strcmp (clipped(prp_user.name12), cText) == 0) strcpy(cfanggebiet,prp_user.wert12);
		if (strcmp (clipped(prp_user.name13), cText) == 0) strcpy(cfanggebiet,prp_user.wert13);
		if (strcmp (clipped(prp_user.name14), cText) == 0) strcpy(cfanggebiet,prp_user.wert14);
		if (strcmp (clipped(prp_user.name15), cText) == 0) strcpy(cfanggebiet,prp_user.wert15);
		if (strcmp (clipped(prp_user.name16), cText) == 0) strcpy(cfanggebiet,prp_user.wert16);
		if (strcmp (clipped(prp_user.name17), cText) == 0) strcpy(cfanggebiet,prp_user.wert17);
		if (strcmp (clipped(prp_user.name18), cText) == 0) strcpy(cfanggebiet,prp_user.wert18);
		if (strcmp (clipped(prp_user.name19), cText) == 0) strcpy(cfanggebiet,prp_user.wert19);
		if (strcmp (clipped(prp_user.name20), cText) == 0) strcpy(cfanggebiet,prp_user.wert20);
		if (strcmp (clipped(prp_user.name21), cText) == 0) strcpy(cfanggebiet,prp_user.wert21);
		if (strcmp (clipped(prp_user.name22), cText) == 0) strcpy(cfanggebiet,prp_user.wert22);
		if (strcmp (clipped(prp_user.name23), cText) == 0) strcpy(cfanggebiet,prp_user.wert23);
		if (strcmp (clipped(prp_user.name24), cText) == 0) strcpy(cfanggebiet,prp_user.wert24);
		if (strcmp (clipped(prp_user.name25), cText) == 0) strcpy(cfanggebiet,prp_user.wert25);
		if (strcmp (clipped(prp_user.name26), cText) == 0) strcpy(cfanggebiet,prp_user.wert26);
		if (strcmp (clipped(prp_user.name27), cText) == 0) strcpy(cfanggebiet,prp_user.wert27);
		if (strcmp (clipped(prp_user.name28), cText) == 0) strcpy(cfanggebiet,prp_user.wert28);
		if (strcmp (clipped(prp_user.name29), cText) == 0) strcpy(cfanggebiet,prp_user.wert29);
		if (strcmp (clipped(prp_user.name30), cText) == 0) strcpy(cfanggebiet,prp_user.wert30);
		fanggebietanz = 0;
        if (fanggebietanz == 0)
        {
               dsqlstatus = ptab_class.lese_ptab_all ("fanggebiet");
               while (dsqlstatus == 0)
               {
				    aktiv = 0;
					if (_a_bas.sais == atoi(ptabn.ptwert))
					{
						aktiv = 1;
					}
					else 
					{
						fg.SetSep (",");
						fg = clipped(cfanggebiet);
						int i = 0;
						Text L2 = ptabn.ptwert;
						L2.Trim ();
						while ((c = fg.NextToken ()) != NULL)
						{
							Text L1 = c;
							L1.Trim ();
							if (L1 == L2) 
							{
								aktiv = 1;
								break;
							}
						}
					}
					if (aktiv == 1)
					{
	                   strcpy (fanggebietarr[fanggebietanz].bz,  ptabn.ptbez);
		               strcpy (fanggebietarr[fanggebietanz].bzk,  ptabn.ptbezk);
			           strcpy (fanggebietarr[fanggebietanz].nr, ptabn.ptwert);
				       fanggebietanz ++;
					   if (fanggebietanz == 20) break;
					}
					dsqlstatus = ptab_class.lese_ptab_all ();
               }
        }
}


int ShowFanggebiet (char *value)
{
        EnableWindow (WiegWindow, FALSE);
        fanggebietidx = ShowPtBox (WiegWindow, (char *) fanggebietarr, fanggebietanz,
                     (char *) &fanggebiet,(int) sizeof (struct FANGGEBIET),
                     &ffanggebietform, &ffanggebietvl, &ffanggebietub);
        EnableWindow (WiegWindow, TRUE);
        strcpy (value, fanggebietarr [fanggebietidx].nr);
        return fanggebietidx;
}













int menfuncLgr (void)
/**
Function fuer Button 8
**/
{
         HWND aktfocus;

         aktfocus = GetFocus ();
		 if (akt_lager < 1) akt_lager = default_lager;
         akt_lager = AuswahlLager (akt_lager);
		 default_lager = akt_lager;
		 a_lgr.a = 0;
         if ( lgr_class.lese_lgr_bz (akt_lager) == 0) sprintf (Lagertxt,"Lager: %s",lgr.lgr_bz);
         AktButton = GetAktButton ();
         return 0;
}
int menfuncALgr (void)
/**
Function fuer Button 8
**/
{
         HWND aktfocus;

         aktfocus = GetFocus ();
		 if (akt_lager < 1) akt_lager = default_lager;
         akt_lager = AuswahlALager (akt_lager,TRUE);
         if ( lgr_class.lese_lgr_bz (akt_lager) == 0) sprintf (Lagertxt,"Lager: %s",lgr.lgr_bz);
			 a_lgr.mdn = we_kopf.mdn;
			 a_lgr.lgr = lgr.lgr;
			 if (a_lgr_class.dbreadfirst() == 0)
			 {
				 sprintf (Buchartikeltxt, "");
				 if (a_lgr.buch_artikel > 0)
				 {
					 sprintf (Buchartikeltxt, "Art. %13.0lf",a_lgr.buch_artikel);
				 }
			 }
		 a_lgr.mdn = we_kopf.mdn;
		 a_lgr.lgr = lgr.lgr;
		 a_lgr_class.dbreadfirst();
         AktButton = GetAktButton ();
         return 0;
}

int menfunc1 (void)
/**
Function fuer Button 1
**/
{
         HWND aktfocus;

         stopauswahl = FALSE;
         InitAufKopf ();
         AuftragKopf (ENTER);
         aktfocus = GetFocus ();
         SetColFocus (MainButton.mask[0].feldid);
         AktButton = GetAktButton ();
         return 0;
}

int menfunc2 (void)
/**
Function fuer Button 2
**/
{

         HWND aktfocus;
		 extern form LiefKopfE;
		 extern form LiefKopfED;


         if (AufLock) return 0;

		 double test = ratod(lief_nr);
//         if (atol (lief_nr) > 0 && LiefEnterAktiv) //100908 wenn strlen > 10 gehts nicht mit atol
         if (ratod (lief_nr) > 0.0 && LiefEnterAktiv)
         {
                      EnterListe ();
                      break_lief_enter = 1;
         }


         aktfocus = GetFocus ();
         AktButton = GetAktButton ();

         return 0;
}

int menfunc3 (void)
/**
Function fuer Button 3
**/
{
         HWND aktfocus;

         auf_art = 0;
         LiefKopf ();
 	     strcpy(we_kopf.lief,"");
         aktfocus = GetFocus ();
         SetColFocus (MainButton.mask[0].feldid);
         AktButton = GetAktButton ();
         return 0;
}

int ItPos (form *frm, char *It)
/**
Position eines Maskenfeldes ermitteln.
**/
{
	      int i;

		  for (i = 0; i < frm->fieldanz; i ++)
		  {
			  if (frm->mask[i].feld == It) return i;
		  }
		  return 0;
}

int menfunc4 (void)
/**
Function fuer Button 4
**/
{
         HWND aktfocus;
		 char mdn [5];

		 sprintf (mdn, "%hd", Mandant);
         EnterNumBox (LogoWindow,  "  Mandant  ", mdn, 5, "%4d");
		 if (syskey == KEY5) return 0;
		 Mandant = atoi (mdn);
		 sprintf (Mandanttxt, "%s %hd", Mtxt, Mandant);
	     display_field (BackWindow2, &MainButton.mask[ItPos (&MainButton, (char *) &CMandant)]);

         aktfocus = GetFocus ();
         AktButton = GetAktButton ();


         return 0;
}


int menfunc5 (void)
/**
Function fuer Button 4
**/
{
         HWND aktfocus;
		 char fil [5];

		 sprintf (fil, "%hd", Filiale);
         EnterNumBox (LogoWindow,  "  Filiale  ", fil, 5, "%4d");
		 if (syskey == KEY5) return 0;
		 Filiale = atoi (fil);
		 sprintf (Filialetxt, "%s %hd", Ftxt, Filiale);
	     display_field (BackWindow2, &MainButton.mask[ItPos(&MainButton, (char *) &CFiliale)]);

         aktfocus = GetFocus ();
         AktButton = GetAktButton ();


         return 0;
}

int menfunc6 (void)
/**
Function fuer Button 6
**/
{
         HWND aktfocus;

         aktfocus = GetFocus ();
         AuswahlSysPeri ();
         AktButton = GetAktButton ();
         return 0;
}

void MainBuInactivQm (void)
/**
Alle Button auf Inaktiv setzen.
**/
{
          ActivateColButton (BackWindow2, &MainButton, 0, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 1, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 2, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 3, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 4, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 5, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 6, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 7, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 8, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 9, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 10, -1, 1);
}

void MainBuActivQm (void)
/**
Alle Button auf Inaktiv setzen.
**/
{
          ActivateColButton (BackWindow2, &MainButton, 1, 1, 1);
          ActivateColButton (BackWindow2, &MainButton, 2, 1, 1);
          ActivateColButton (BackWindow2, &MainButton, 4, 1, 1);
          ActivateColButton (BackWindow2, &MainButton, 9, 1, 1);
          set_fkt (NULL, 6);
}

void MainBuInactivQmPos (void)
/**
Alle Button auf Inaktiv setzen.
**/
{
          MainBuInactivQm ();
          ActivateColButton (BackWindow3, &MainButton2, 0, -1, 1);
          ActivateColButton (BackWindow3, &MainButton2, 3, -1, 1);
}

void MainBuActivQmPos (void)
/**
Alle Button auf Inaktiv setzen.
**/
{
          MainBuActivQm ();
          ActivateColButton (BackWindow3, &MainButton2, 0, 1, 1);
          ActivateColButton (BackWindow3, &MainButton2, 3, 1, 1);
}


BOOL QmAktiv = FALSE;

static char *Ub[] = {"Qualit�tssicherung Hygiene",
                      NULL,
};


static char *Texte0[] = {"Laderaum hygienisch",
                         "Haaken und Beh�ltnisse i.O.",
			             "Holzpaletten/Kartonagen",
			             "Fleisch korrekt verpackt",
			             "Kleidung hygienisch",
			             "Kopfbedeckung",
				          NULL, NULL
};


char **Texte = Texte0;

static int Yes0[] = {0, 0, 0, 0, 0, 0, 0, 0};
static int No0[]  = {0, 0, 0, 0, 0, 0, 0, 0};

static int *Yes = Yes0;
static int *No  = No0;
static int qsanz = 6;

void FillQsHead ()
/**
Texte fuer Qualitaetssicherung fuellen.
**/
{
    int dsqlstatus;
    int cursor;
    char txt [25];
    int i;

    DbClass.sqlout ((short *) &qsanz, 1, 0);
    dsqlstatus = DbClass.sqlcomm ("select count (*) from qstxt where qstyp = 1 "
                                                               "and lief = \" \"");
    if (dsqlstatus != 0)
    {
        return;
    }

    if (qsanz == 0)
    {
        return;
    }

    Texte = new char* [qsanz + 2];
    Yes   = new int [qsanz + 2];
    No    = new int [qsanz + 2];

    for (i = 0; i < qsanz; i ++)
    {
        Yes[i] = No[i] = 0;
    }


    DbClass.sqlout ((char *) txt, 0, 25);
    cursor = DbClass.sqlcursor ("select txt,sort from qstxt where qstyp = 1 "
                                                            "and lief = \" \" "
                                                            "order by sort");
    if (cursor < 0)
    {
        return;
    }

    i = 0;
    while (DbClass.sqlfetch (cursor) == 0)
    {
        Texte [i] = new char [25];
        strcpy (Texte[i], txt);
        i ++;
    }

    Texte[i] = NULL;
    DbClass.sqlclose (cursor);
}


void FillQsHead (char *lief_nr)
/**
Texte fuer Qualitaetssicherung fuellen.
**/
{
    int cursor;
    char txt [25];
    char lief [17];
    int i;

    strcpy (lief, lief_nr);
    DbClass.sqlout ((short *) &qsanz, 1, 0);
    DbClass.sqlin ((char *) lief, 0, 17);
    cursor = DbClass.sqlcursor ("select count (*) from qstxt where qstyp = 1 "
                                                               "and lief = ?");
    qsanz = 0;
    DbClass.sqlfetch (cursor);
    while (qsanz == 0)
    {
        if (strcmp (lief, " ") > 0)
        {
            strcpy (lief, " ");
        }
		else
        {
            break;
        }
        DbClass.sqlopen (cursor);
        DbClass.sqlfetch (cursor);
    }

    DbClass.sqlclose (cursor);
        
    if (qsanz == 0)
    {
		return;
    }

    for (i = 0; Texte[i] != NULL; i ++)
    {
          delete Texte[i];
    }
    delete Texte;
    delete Yes;
    delete No;

    Texte = new char* [qsanz + 2];
    Yes   = new int [qsanz + 2];
    No    = new int [qsanz + 2];

    for (i = 0; i < qsanz; i ++)
    {
        Yes[i] = No[i] = 0;
    }


    DbClass.sqlout ((char *) txt, 0, 25);
    DbClass.sqlin ((char *) lief, 0, 17);
    cursor = DbClass.sqlcursor ("select txt,sort from qstxt where qstyp = 1 "
                                                            "and lief = ? "
                                                            "order by sort");
    if (cursor < 0)
    {
        return;
    }

    i = 0;
    while (DbClass.sqlfetch (cursor) == 0)
    {
        Texte [i] = new char [25];
        strcpy (Texte[i], txt);
        i ++;
    }

    Texte[i] = NULL;
    DbClass.sqlclose (cursor);
}



void InitQm (int *Yes, int *No, int anz)
/**
Eingaben pruefen.
**/
{
	      int i;

		  for (i = 0; i < anz; i ++)
		  {
			  Yes[i] = 0;
			  No [i] = 0;
		  }
}


BOOL TestQm (int *Yes, int *No, int anz)
/**
Eingaben pruefen.
**/
{
	      int i;

		  for (i = 0; i < anz; i ++)
		  {
			  if (Yes[i] == 0 &&
				  No [i] == 0)
			  {
				  return FALSE;
			  }
		  }
		  return TRUE;
}

BOOL TestQs (BOOL direkt)
{
         if (QsZwang == 0)
         {
             return TRUE;
         }

		 qswekopf.mdn = Mandant;
		 qswekopf.fil = Filiale;
		 strcpy (qswekopf.lief, we_kopf.lief);
		 strcpy (qswekopf.lief_rech_nr, we_kopf.lief_rech_nr);
		 qswekopf.dat = dasc_to_long (datum);
         qswekopf.lfd = (int) 1;
	     int dsqlstatus = QsWeKopf.dbreadfirst ();
         if (dsqlstatus != 0 && direkt)
         {
			 menfunc7();//LuD 090908
			 if (syskey == KEY5) return FALSE;
//             disp_messG ("Qualt�tsmerkmale wurden noch nicht erfasst", 2);
//             return FALSE;
         }
         return TRUE;
}


void WriteQs (void)
/**
Eingaben aus Qualit�tssicherung speichern.
**/
{
	     int i;
		 char datum [12];

		 beginwork ();
		 sysdate (datum);
		 qswekopf.mdn = Mandant;
		 qswekopf.fil = Filiale;
		 strcpy (qswekopf.lief, we_kopf.lief);
		 strcpy (qswekopf.lief_rech_nr, we_kopf.lief_rech_nr);
		 qswekopf.dat = dasc_to_long (datum);
         qswekopf.lfd = (int) 1;
	     QsWeKopf.dbreadfirst ();
		 strcpy (qswekopf.pers_nam, sys_ben.pers_nam);
		 //100908 qswetext jetzt mit 1:1 beziehung zu we_kopf , wenn Anzahl Positionen < 11 
		 //       so ist beim Belegdruck keine Vorselektion n�tig !!
		 for (i = 0; i < qsanz; i ++)
		 {

			 if (i == 0) 
			 {
				strcpy (qswekopf.txt, Texte[i]);
				qswekopf.wrt = Yes[i];
				QsWeKopf.dbupdate ();
			 }
			 if (i == 1) 
			 {
				strcpy (qswekopf.txt2, Texte[i]);
				qswekopf.wrt2 = Yes[i];
				QsWeKopf.dbupdate ();
			 }
			 if (i == 2) 
			 {
				strcpy (qswekopf.txt3, Texte[i]);
				qswekopf.wrt3 = Yes[i];
				QsWeKopf.dbupdate ();
			 }
			 if (i == 3) 
			 {
				strcpy (qswekopf.txt4, Texte[i]);
				qswekopf.wrt4 = Yes[i];
				QsWeKopf.dbupdate ();
			 }
			 if (i == 4) 
			 {
				strcpy (qswekopf.txt5, Texte[i]);
				qswekopf.wrt5 = Yes[i];
				QsWeKopf.dbupdate ();
			 }
			 if (i == 5) 
			 {
				strcpy (qswekopf.txt6, Texte[i]);
				qswekopf.wrt6 = Yes[i];
				QsWeKopf.dbupdate ();
			 }
			 if (i == 6) 
			 {
				strcpy (qswekopf.txt7, Texte[i]);
				qswekopf.wrt7 = Yes[i];
				QsWeKopf.dbupdate ();
			 }
			 if (i == 7) 
			 {
				strcpy (qswekopf.txt8, Texte[i]);
				qswekopf.wrt8 = Yes[i];
				QsWeKopf.dbupdate ();
			 }
			 if (i == 8) 
			 {
				strcpy (qswekopf.txt9, Texte[i]);
				qswekopf.wrt9 = Yes[i];
				QsWeKopf.dbupdate ();
			 }
			 if (i == 9) 
			 {
				strcpy (qswekopf.txt10, Texte[i]);
				qswekopf.wrt10 = Yes[i];
				QsWeKopf.dbupdate ();
			 }
			 if (i > 9)
			 {
 				qswekopf.lfd = (int) i + 1;
				strcpy (qswekopf.txt, Texte[i]);
				qswekopf.wrt = Yes[i];
				QsWeKopf.dbupdate ();
			 }
		 }
		 commitwork ();
}


int menfunc7 (void)
/**
Function fuer Button 7
**/
{
         HWND aktfocus;
		 extern form LiefKopfT;
		 extern form LiefKopfE;
		 extern form LiefKopfTD;
		 extern form LiefKopfED;

		 QmAktiv = TRUE;
		 if (LiefKopfE.mask [1].feldid > 0)
		 {
			GetWindowText (LiefKopfE.mask [1].feldid,
                        LiefKopfE.mask [1].feld,
                        LiefKopfE.mask[1].length);
		 }
         FillQsHead (lieferant);
		 if (AufKopfWindow)
		 {
	            Qmkopf->SethMainWnd (AufKopfWindow);
                CloseControls (&LiefKopfT);
                CloseControls (&LiefKopfE);
                CloseControls (&LiefKopfTD);
                CloseControls (&LiefKopfED);
		 }
		 else
		 {
	            Qmkopf->SethMainWnd (LogoWindow);
		 }
         aktfocus = GetFocus ();
		 MainBuInactivQm ();
	     InitQm (Yes, No, qsanz);
		 while (TRUE)
		 {
//		        Qmkopf->SetParams (Ub, Texte, Yes, No, WS_CHILD);
		        Qmkopf->SetParamsEx (Ub, Texte, Yes, No,
			                  WS_CHILD, 0, TRUE);
		        Qmkopf->Enter ();
		        if (syskey == KEY12)
				{
					if (TestQm (Yes, No, qsanz) == FALSE)
					{
						disp_messG ("Bitte alle Punkte bearbeiten", 2);
						continue;
					}
			        WriteQs ();
				}
				break;
		 }
		 MainBuActivQm ();
		 if (AufKopfWindow)
		 {
                display_form (AufKopfWindow, &LiefKopfT, 0, 0);
                create_enter_form  (AufKopfWindow, &LiefKopfE,  0, 0);
                display_form (AufKopfWindow, &LiefKopfTD, 0, 0);
                create_enter_form  (AufKopfWindow, &LiefKopfED, 0, 0);
		 }
         AktButton = GetAktButton ();
		 QmAktiv = FALSE;
         return 0;
}


static char *UbPos[] = {"Qualit�tssicherung Fleisch",
                      NULL,
};

static char *TextePos0[] = {"Fettgehalt OK",
                           "Gewicht OK",
		           	       "Zuschnitt OK",
			               "Spezifikationen OK",
			               "Zustand der Ware OK",
			               "ordnungsgem�� gestempelt",
				           NULL, NULL
};

static int YesPos0[] = {0, 0, 0, 0, 0, 0, 0, 0};
static int NoPos0[]  = {0, 0, 0, 0, 0, 0, 0, 0};
static int qsanzpos = 6;

static char **TextePos = TextePos0;
static int *YesPos = YesPos0;
static int *NoPos  = NoPos0;

void FillQsPos ()
/**
Texte fuer Qualitaetssicherung fuellen.
**/
{
    int dsqlstatus;
    int cursor;
    char txt [25];
    int i;

    DbClass.sqlout ((short *) &qsanzpos, 1, 0);
    dsqlstatus = DbClass.sqlcomm ("select count (*) from qstxt where qstyp = 2 "
                                                               "and hwg = 0 "
                                                               "and wg = 0 "
                                                               "and ag = 0");
    if (dsqlstatus != 0)
    {
        return;
    }

    if (qsanzpos == 0)
    {
        return;
    }

    TextePos = new char* [qsanzpos + 2];
    YesPos   = new int [qsanzpos + 2];
    NoPos    = new int [qsanzpos + 2];

    for (i = 0; i < qsanzpos; i ++)
    {
        YesPos[i] = NoPos[i] = 0;
    }


    DbClass.sqlout ((char *) txt, 0, 25);
    cursor = DbClass.sqlcursor ("select txt,sort from qstxt where qstyp = 2 "
                                                               "and hwg = 0 "
                                                               "and wg = 0 "
                                                               "and ag = 0 "
                                                               "order by sort");
    if (cursor < 0)
    {
        return;
    }

    i = 0;
    while (DbClass.sqlfetch (cursor) == 0)
    {
        TextePos [i] = new char [25];
        strcpy (TextePos[i], txt);
        i ++;
    }

    TextePos[i] = NULL;
    DbClass.sqlclose (cursor);
}


void FillQsPos (double a)
/**
Texte fuer Qualitaetssicherung fuellen.
**/
{
    int cursor;
    char txt [25];
    int i;
    short hwg = 0;
    short wg = 0;
    short ag = 0l;

    DbClass.sqlout ((short *)  &hwg, 1, 0);
    DbClass.sqlout ((short *)  &wg,  1, 0);
    DbClass.sqlout ((long *)   &ag,  2, 0);
    DbClass.sqlin  ((double *) &a,   3, 0);
    DbClass.sqlcomm ("select hwg, wg, ag from a_bas where a = ?");

    DbClass.sqlout ((short *) &qsanzpos, 1, 0);
    DbClass.sqlin ((short *)  &hwg, 1, 0);
    DbClass.sqlin ((short *)  &wg,  1, 0);
    DbClass.sqlin ((long *)   &ag,  2, 0);
    cursor = DbClass.sqlcursor ("select count (*) from qstxt where qstyp = 2 "
                                                               "and hwg = ? "
                                                               "and wg = ? "
                                                               "and ag = ?");
   qsanzpos = 0;
   DbClass.sqlfetch (cursor); 
   while (qsanzpos == 0)
    {
        if (ag > 0l)
        {
            ag = 0l;
        }
        else if (wg > 0)
        {
            wg = 0;
        }
        else if (hwg > 0)
        {
            hwg = 0;
        }
        else
        {
            break;
        }
        DbClass.sqlopen (cursor);
        DbClass.sqlfetch (cursor); 
    }
    DbClass.sqlclose (cursor);

    if (qsanzpos == 0)
    {
        return;
    }

    if (TextePos != NULL &&
        TextePos != TextePos0)
    {
       for (i = 0; TextePos[i] != NULL; i ++)
       {
           delete TextePos[i];
       }
       delete TextePos;
       delete YesPos;
       delete NoPos;
    }

    TextePos = new char* [qsanzpos + 2];
    YesPos   = new int [qsanzpos + 2];
    NoPos    = new int [qsanzpos + 2];

    for (i = 0; i < qsanzpos; i ++)
    {
        YesPos[i] = NoPos[i] = 0;
    }


    DbClass.sqlout ((char *) txt, 0, 25);
    DbClass.sqlin ((short *)  &hwg, 1, 0);
    DbClass.sqlin ((short *)  &wg,  1, 0);
    DbClass.sqlin ((long *)   &ag,  2, 0);
    cursor = DbClass.sqlcursor ("select txt,sort from qstxt where qstyp = 2 "
                                                            "and hwg = ? "
                                                            "and wg = ? "
                                                            "and ag = ? "
                                                            "order by sort");
    if (cursor < 0)
    {
        return;
    }

    i = 0;
    while (DbClass.sqlfetch (cursor) == 0)
    {
        TextePos [i] = new char [25];
        strcpy (TextePos[i], txt);
        i ++;
    }

    TextePos[i] = NULL;
    DbClass.sqlclose (cursor);
}


BOOL TestQsPosA (double a)
{
         
         if (QsZwang != 1)
         {
             return TRUE;
         }
		 qswepos.mdn = Mandant;
		 qswepos.fil = Filiale;
		 strcpy (qswepos.lief, clipped (we_kopf.lief));
		 strcpy (qswepos.lief_rech_nr, clipped (we_kopf.lief_rech_nr));
         qswepos.a   = a;
         DbClass.sqlin ((short *) &qswepos.mdn, 1, 0);
         DbClass.sqlin ((short *) &qswepos.fil, 1, 0);
         DbClass.sqlin ((char *)  qswepos.lief, 0, 17);
         DbClass.sqlin ((char *)  qswepos.lief_rech_nr, 0, 17);
         DbClass.sqlin ((double *) &qswepos.a, 3, 0);
	     int dsqlstatus = DbClass.sqlcomm ("select a from qswepos "
                                            "where mdn = ? "
                                            "and fil = ? "
                                            "and lief = ? "
                                            "and lief_rech_nr = ? "
                                            "and a = ?"); 
         if (dsqlstatus != 0)
         {
             print_messG (2, "Qualit�tsmermale f�r Artikel %.0lf\n"
                             "wurden noch nicht erfasst", a);
             return FALSE;
         }
         return TRUE;
}

BOOL TestQsPos (void)
{
         if (QsZwang != 1)
         {
             return TRUE;
         }
         for (int i = 0; i < aufpanz; i ++)
         {
             if (TestQsPosA (ratod (aufparr[i].a)) == FALSE)
             {
                 return FALSE;
             }
         }
         return TRUE;
}


void WriteQsPos (void)
/**
Eingaben aus Qualit�tssicherung speichern.
**/
{
	     int i;
		 char datum [12];
		 extern int aufpidx;

         aufpidx = SendMessage  (Getlbox (), LB_GETCURSEL, 0l, 0l);
         memcpy (&aufps, &aufparr[aufpidx], sizeof (struct AUFP_S));

		 sysdate (datum);
		 qswepos.mdn = Mandant;
		 qswepos.fil = Filiale;
		 strcpy (qswepos.lief, we_kopf.lief);
		 strcpy (qswepos.lief_rech_nr, we_kopf.lief_rech_nr);
		 qswepos.dat = dasc_to_long (datum);
         qswepos.lfd = (int) 1;
         qswepos.a   = ratod (aufps.a);
         qswepos.int_pos   = atol (aufps.pnr);
	     QsWePos.dbreadfirst ();
         qswepos.temp      = ratod (aufps.temp);
         qswepos.temp2     = ratod (aufps.temp2);
         qswepos.temp3     = ratod (aufps.temp3);
         qswepos.ph_wert   = ratod (aufps.ph_wert);
		 strcpy (qswepos.pers_nam, sys_ben.pers_nam);

		 for (i = 0; i < qsanzpos; i ++)
		 {
 		          qswepos.lfd = (int) i + 1;
				  strcpy (qswepos.txt, TextePos[i]);
				  qswepos.wrt = YesPos[i];
				  QsWePos.dbupdate ();
		 }
}

void HoleQsMess (int idx) //021209
{
		 char datum [12];
		 extern int aufpidx;

		 sysdate (datum);
		 qswepos.mdn = Mandant;
		 qswepos.fil = Filiale;
		 strcpy (qswepos.lief, we_kopf.lief);
		 strcpy (qswepos.lief_rech_nr, we_kopf.lief_rech_nr);
		 qswepos.dat = dasc_to_long (datum);
         qswepos.lfd = (int) 1;
         qswepos.a   = ratod (aufparr[idx].a);
         qswepos.int_pos   = atol (aufparr[idx].pnr);
	     if (QsWePos.dbreadfirst () == 0)
		 {
			sprintf(aufparr[idx].temp,"%3.1lf",qswepos.temp);
			sprintf(aufparr[idx].temp2,"%3.1lf",qswepos.temp2);
			sprintf(aufparr[idx].temp3,"%3.1lf",qswepos.temp3);
			sprintf(aufparr[idx].ph_wert,"%4.2lf",qswepos.ph_wert);
		 }
}

void WriteQsMess (void) //110908
/**
Eingaben aus Qualit�tssicherung speichern nur Messwerte nur lfd = 1.
**/
{
		 char datum [12];
		 extern int aufpidx;


         aufpidx = SendMessage  (Getlbox (), LB_GETCURSEL, 0l, 0l);
         memcpy (&aufps, &aufparr[aufpidx], sizeof (struct AUFP_S));

		 sysdate (datum);
		 qswepos.mdn = Mandant;
		 qswepos.fil = Filiale;
		 strcpy (qswepos.lief, we_kopf.lief);
		 strcpy (qswepos.lief_rech_nr, we_kopf.lief_rech_nr);
		 qswepos.dat = dasc_to_long (datum);
         qswepos.lfd = (int) 1;
         qswepos.a   = ratod (aufps.a);
         qswepos.int_pos   = atol (aufps.pnr);
	     QsWePos.dbreadfirst ();
         qswepos.temp      = ratod (aufps.temp);
         qswepos.temp2     = ratod (aufps.temp2);
         qswepos.temp3     = ratod (aufps.temp3);
         qswepos.ph_wert   = ratod (aufps.ph_wert);
		 strcpy (qswepos.pers_nam, sys_ben.pers_nam);
         qswepos.lfd = 1;
	     QsWePos.dbupdate ();
		 if (Schwundabzug > 0 && _a_bas.sw > 0.0 ) //021109
		 {
			 if (qswepos.temp > Schwundabzug || qswepos.temp2 > Schwundabzug || qswepos.temp3 > Schwundabzug ) 
			 {
				sprintf (aufps.txtschwundabzug, "(Schwundabzug: %.1lf %s.)",_a_bas.sw,"%"); //021109
				memcpy (&aufparr[aufpidx],&aufps, sizeof (struct AUFP_S));
			 }
			 else
			 {
				sprintf (aufps.txtschwundabzug, ""); //021109
				memcpy (&aufparr[aufpidx],&aufps, sizeof (struct AUFP_S));
			 }

		 }


}


int doQsPos (void)
/**
Function fuer Button 7
**/
{

		 if (eWindow == NULL) return 0;
         int aufpidx = SendMessage  (Getlbox (), LB_GETCURSEL, 0l, 0l);
         FillQsPos (ratod (aufparr[aufpidx].a));
         if (qsanzpos == 0)
         {
             print_messG (2, "Es sind keine Qualit�tstexte f�r Artikel %.0lf\n"
                             "angelegt", ratod (aufparr[aufpidx].a));
             return 0;
         }
		 QmAktiv = TRUE;
//         Qmkopf->SethMainWnd (eWindow);
         Qmkopf->SethMainWnd (GethListBox ());
		 MainBuInactivQmPos ();
         EnableWindow (eFussWindow, FALSE);
	     InitQm (YesPos, NoPos, qsanzpos);
		 while (TRUE)
		 {
		        Qmkopf->SetParamsEx (UbPos, TextePos, YesPos, NoPos,
			                  WS_POPUP, WS_EX_CLIENTEDGE, TRUE);
		        Qmkopf->Enter ();
		        if (syskey == KEY12)
				{
					if (TestQm (YesPos, NoPos, qsanzpos) == FALSE)
					{
						disp_messG ("Bitte alle Punkte bearbeiten", 2);
						continue;
					}
		           WriteQsPos ();
				}
				break;
		 }
         EnableWindow (eFussWindow, TRUE);
		 MainBuActivQmPos ();
		 QmAktiv = FALSE;
         return 0;
}

// Auswahl ueber Bestellungen

mfont cbestfont = {"Courier New", 120, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                0,
                                NULL};

struct CBE
{
char best_blg [9];
char best_term [12];
char lief_term [12];
};

static struct CBE cbest, *cbestarr = NULL;
static int cbestanz;

static int sortcbest ();
static int sortcbestt ();
static int sortclieft ();

static field _cbestform [] = {
	cbest.best_blg,     8, 0, 0, 1, 0, "%8d",    DISPLAYONLY, 0, 0, 0,
	cbest.best_term,   10, 0, 0,11, 0, "",       DISPLAYONLY, 0, 0, 0,
	cbest.lief_term,   10, 0, 0,23, 0, "",       DISPLAYONLY, 0, 0, 0,
};

static form cbestform = {3, 0, 0, _cbestform, 0, 0, 0, 0, &cbestfont};


static field _bestub [] =
{
        " Best-Nr",        10, 1, 0, 0, 0, "", BUTTON, 0, sortcbest, 0,
        " Best.Term",      12, 1, 0,10, 0, "", BUTTON, 0, sortcbestt, 1,
        " Lief.Term",      12, 1, 0,22, 0, "", BUTTON, 0, sortclieft, 2,
};

static form bestub = {3, 0, 0, _bestub, 0, 0, 0, 0, &cbestfont};

static field _bestvl [] =
{
        "1",                1, 1, 0,10, 0, "", DISPLAYONLY, 0, 0, 0,
        "1",                1, 1, 0,22, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form bestvl = {2, 0, 0, _bestvl, 0, 0, 0, 0, &cbestfont};
static int cbestsort = 1;
static int cbtsort = 1;
static int cltsort = 1;


static int sortcbest0 (const void *elem1, const void *elem2)
{
	        struct CBE *el1;
	        struct CBE *el2;

			el1 = (struct CBE *) elem1;
			el2 = (struct CBE *) elem2;
	        return ((int) (atol (el1->best_blg) - atol (el2->best_blg)) * cbestsort);
}


static int sortcbest (void)
/**
Nach Bestellnummer sortieren.
**/
{
	        qsort (cbestarr, cbestanz, sizeof (struct CBE),
				   sortcbest0);
			cbestsort *= -1;
            ShowNewElist ((char *) cbestarr,
                             cbestanz,
                            (int) sizeof (struct CBE));
			return 0;
}


static int sortcbestt0 (const void *elem1, const void *elem2)
{
	        struct CBE *el1;
	        struct CBE *el2;

			el1 = (struct CBE *) elem1;
			el2 = (struct CBE *) elem2;
            long dat1 = dasc_to_long (el1->best_term);
            long dat2 = dasc_to_long (el2->best_term);
            return (dat1 - dat2) * cbtsort;
//			return ( (int) (strcmp (el1->best_term, el2->best_term)) * cbtsort);
}

static int sortcbestt (void)
/**
Nach Bestelltermin sortieren.
**/
{
	        qsort (cbestarr, cbestanz, sizeof (struct CBE),
				   sortcbestt0);
			cbtsort *= -1;
            ShowNewElist ((char *) cbestarr,
                             cbestanz,
                            (int) sizeof (struct CBE));
			return 0;
}
static int sortclieft0 (const void *elem1, const void *elem2)
{
	        struct CBE *el1;
	        struct CBE *el2;

			el1 = (struct CBE *) elem1;
			el2 = (struct CBE *) elem2;
            long dat1 = dasc_to_long (el1->lief_term);
            long dat2 = dasc_to_long (el2->lief_term);
            return (dat1 - dat2) * cltsort;
//			return ( (int) (strcmp (el1->lief_term, el2->lief_term)) * cltsort);
}


static int sortclieft (void)
/**
Nach Liefertermin sortieren.
**/
{
	        qsort (cbestarr, cbestanz, sizeof (struct CBE),
				   sortclieft0);
			cltsort *= -1;
            ShowNewElist ((char *) cbestarr,
                             cbestanz,
                            (int) sizeof (struct CBE));
			return 0;
}


int dobestchoise ()
{
	    static long bestanz = 0;
		long best_blg;
		char best_term [12];
		char lief_term [12];
		int cursor;
		int idx;
		int i;

		if (scrfx > (double) 1.0)
		{
                cbestfont.FontHeight = (int) (double) ((double) 120 *  scrfx);
		}
		cbestsort = cbtsort = cltsort = 1;
		if (cbestarr)
		{
			    GlobalFree (cbestarr);
		}
        DbClass.sqlout  ((long *) &bestanz, 2, 0);
		DbClass.sqlin   ((short *) &Mandant ,1, 0);
		DbClass.sqlin   ((short *) &Filiale ,1, 0);
		DbClass.sqlin   ((char *) lieferant, 0, 17);
        DbClass.sqlcomm ("select count (*) from best_kopf where mdn = ? "
			                                              "and fil = ? "
														  "and lief = ? "
														  "and bearb_stat <= 2");
		if (bestanz == 0)
		{
			print_messG (2, "Keine Bestellungen f�r Lieferant %s\n"
				            "vorhanden", lieferant);
			return 0;
		}

        cbestarr = (struct CBE *) GlobalAlloc (GMEM_FIXED, bestanz * sizeof (struct CBE));
		if (cbestarr == NULL)
		{
			disp_messG ("Fehler beim Zuweisen von Speicherplatz", 2);
			return 0;
		}

        DbClass.sqlout  ((long *)  &best_blg, 2, 0);
        DbClass.sqlout  ((char *)  best_term, 0, 11);
        DbClass.sqlout  ((char *)  lief_term, 0, 11);
		DbClass.sqlin   ((short *) &Mandant ,1, 0);
		DbClass.sqlin   ((short *) &Filiale ,1, 0);
		DbClass.sqlin   ((char *) lieferant, 0, 17);
        if (druck_zwang)
        {
		cursor = DbClass.sqlcursor ("select best_blg, best_term, lief_term from best_kopf "
			                        "where mdn = ? "
									"and fil = ? "
									"and lief = ? "
									"and bearb_stat = 2 order by best_blg");
        }
        else
        {
		cursor = DbClass.sqlcursor ("select best_blg, best_term, lief_term from best_kopf "
			                        "where mdn = ? "
									"and fil = ? "
									"and lief = ? "
									"and bearb_stat <= 2 order by best_blg");
        }
		i = 0;
		while (DbClass.sqlfetch (cursor) == 0)
		{
			sprintf (cbestarr[i].best_blg, "%ld", best_blg);
			strcpy  (cbestarr[i].best_term, best_term);
			strcpy  (cbestarr[i].lief_term, lief_term);
			i = i + 1;
		}
		DbClass.sqlclose (cursor);

		bestanz = cbestanz = i;
        SetHoLines (1);
        SetVeLines (1);
		PtY = 410;
		if (scrfx > (double) 1.0)
		{
			PtY = (int) (double) ((double) PtY * scrfx);
		}
        syskey = 0;

        EnableWindow (NuBlock.GeteNumWindow (), FALSE);
        idx = ShowWKopf (NuBlock.GeteNumWindow (), (char *) cbestarr, bestanz,
                        (char *) &cbest, sizeof (struct CBE),
                         &cbestform, &bestvl, &bestub);
		CloseControls (&bestub);
        EnableWindows (AufKopfWindow, FALSE);
        EnableWindow (NuBlock.GeteNumWindow (), TRUE);
        SetHoLines (1);
        if (syskey == KEY5 || syskey == KEYESC)
        {
			    GlobalFree (cbestarr);
		        cbestarr = NULL;
                return FALSE;
        }

        strcpy (NuBlock.Geteditbuff (), cbestarr[idx].best_blg);
		GlobalFree (cbestarr);
		cbestarr = NULL;
		Sleep (50);
		display_form (NuBlock.GeteNumWindow (), NuBlock.GetEditForm (), 0, 0);
		NuBlock.SetEditFocus ();

		if (best_ausw_direct)
		{
		       NuBlock.NumEnterBreak ();
		}
		return 0;
}

// Auswahl ueber Lieferanten

mfont clieffont = {"Courier New", 120, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                0,
                                NULL};

struct CLI
{
char lief [17];
char name [37];
};

static struct CLI clief, *cliefarr = NULL;
static int cliefanz;

static int sortclief ();
static int sortcname ();

static field _cliefform [] = {
	clief.lief,        16, 0, 0, 1, 0, "",    DISPLAYONLY, 0, 0, 0,
	clief.name,        36, 0, 0,19, 0, "",    DISPLAYONLY, 0, 0, 0,
};

static form cliefform = {2, 0, 0, _cliefform, 0, 0, 0, 0, &clieffont};


static field _liefub [] =
{
        " Lieferant",      18, 1, 0, 0, 0, "", BUTTON, 0, sortclief, 0,
        " Name",           40, 1, 0,18, 0, "", BUTTON, 0, sortcname, 1,
};

static form liefub = {2, 0, 0, _liefub, 0, 0, 0, 0, &clieffont};

static field _liefvl [] =
{
        "1",                1, 1, 0,18, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form liefvl = {1, 0, 0, _liefvl, 0, 0, 0, 0, &clieffont};
static int cliefsort = 1;
static int cnamesort = 1;


static int sortclief0 (const void *elem1, const void *elem2)
{
	        struct CLI *el1;
	        struct CLI *el2;

			el1 = (struct CLI *) elem1;
			el2 = (struct CLI *) elem2;
			return ( (int) (strcmp (el1->lief, el2->lief)) * cliefsort);
}


static int sortclief (void)
/**
Nach Lieferantennummer sortieren.
**/
{
	        qsort (cliefarr, cliefanz, sizeof (struct CLI),
				   sortclief0);
			cliefsort *= -1;
            ShowNewElist ((char *) cliefarr,
                             cliefanz,
                            (int) sizeof (struct CLI));
			return 0;
}


static int sortcname0 (const void *elem1, const void *elem2)
{
	        struct CLI *el1;
	        struct CLI *el2;

			el1 = (struct CLI *) elem1;
			el2 = (struct CLI *) elem2;
			return ( (int) (strcmp (el1->name, el2->name)) * cnamesort);
}

static int sortcname (void)
/**
Nach Lieferantennamen sortieren.
**/
{
	        qsort (cliefarr, cliefanz, sizeof (struct CLI),
				   sortcname0);
			cnamesort *= -1;
            ShowNewElist ((char *) cliefarr,
                             cliefanz,
                            (int) sizeof (struct CLI));
			return 0;
}
int doliefchoise ()
{
	    static long liefanz = 0;
		char lief [17];
		char name [37];
		int cursor;
		int idx;
		int i;

		if (scrfx > (double) 1.0)
		{
                clieffont.FontHeight = (int) (double) ((double) 120 *  scrfx);
		}
		cliefsort = cnamesort = 1;
		if (cliefarr)
		{
			    GlobalFree (cliefarr);
		}
        DbClass.sqlout  ((long *) &liefanz, 2, 0);
        DbClass.sqlcomm ("select count (*) from lief where lief <> \"-1\"");
		if (liefanz == 0)
		{
			return 0;
		}

        cliefarr = (struct CLI *) GlobalAlloc (GMEM_FIXED, liefanz * sizeof (struct CLI));
		if (cliefarr == NULL)
		{
			disp_messG ("Fehler beim Zuweisen von Speicherplatz", 2);
			return 0;
		}

        DbClass.sqlout  ((char *)  lief, 0, 17);
        DbClass.sqlout  ((char *)  name, 0, 37);
		cursor = DbClass.sqlcursor ("select lief.lief, adr.adr_nam1 from lief,adr "
									"where lief.lief <> \"-1\" and adr.adr = lief.adr "
									"order by lief");
		i = 0;
		while (DbClass.sqlfetch (cursor) == 0)
		{
			strcpy  (cliefarr[i].lief, lief);
			strcpy  (cliefarr[i].name, name);
			i = i + 1;
		}
		DbClass.sqlclose (cursor);

		liefanz = cliefanz = i;
        SetHoLines (1);
        SetVeLines (1);
		PtY = 410;
		if (scrfx > (double) 1.0)
		{
			PtY = (int) (double) ((double) PtY * scrfx);
		}
        syskey = 0;

        EnableWindow (NuBlock.GeteNumWindow (), FALSE);
        idx = ShowWKopf (NuBlock.GeteNumWindow (), (char *) cliefarr, liefanz,
                        (char *) &clief, sizeof (struct CLI),
                         &cliefform, &liefvl, &liefub);
		CloseControls (&liefub);
        EnableWindows (AufKopfWindow, FALSE);
        EnableWindow (NuBlock.GeteNumWindow (), TRUE);
        SetHoLines (1);
        if (syskey == KEY5 || syskey == KEYESC)
        {
			    GlobalFree (cbestarr);
		        cbestarr = NULL;
                return FALSE;
        }

        strcpy (NuBlock.Geteditbuff (), cliefarr[idx].lief);
		GlobalFree (cliefarr);
		cliefarr = NULL;
		Sleep (50);
		display_form (NuBlock.GeteNumWindow (), NuBlock.GetEditForm (), 0, 0);
		NuBlock.SetEditFocus ();
		if (lief_ausw_direct)
		{
			   NuBlock.NumEnterBreak ();
		}
		return 0;
}
// Auswahl ueber Artikel (a_bas)

mfont cartfont = {"Courier New", 120, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                0,
                                NULL};

struct CA
{
char a [14];
char a_bz1 [25];
char a_bz2 [25];
};

static struct CA ca, *caarr = NULL;
static int caanz;

static int sortca ();
static int sortcbz ();

static field _cartform [] = {
	ca.a,         13, 0, 0, 1, 0, "%13.0f", DISPLAYONLY, 0, 0, 0,
	ca.a_bz1,     24, 0, 0,16, 0, "",       DISPLAYONLY, 0, 0, 0,
	ca.a_bz2,     24, 0, 0,41, 0, "",       DISPLAYONLY, 0, 0, 0,
};

static form cartform = {3, 0, 0, _cartform, 0, 0, 0, 0, &cartfont};


static field _artub [] =
{
        "    Artikel",     15, 1, 0, 0, 0, "", BUTTON, 0, sortca, 0,
        " Bezeichnung 1",  25, 1, 0,15, 0, "", BUTTON, 0, sortcbz, 1,
        " Bezeichnung 2",  40, 1, 0,40, 0, "", BUTTON, 0, 0, 2,
};

static form artub = {3, 0, 0, _artub, 0, 0, 0, 0, &cartfont};

static field _artvl [] =
{
        "1",                1, 1, 0,15, 0, "", DISPLAYONLY, 0, 0, 0,
        "1",                1, 1, 0,40, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form artvl = {2, 0, 0, _artvl, 0, 0, 0, 0, &cartfont};
static int casort = 1;
static int cbsort = 1;


static int sortca0 (const void *elem1, const void *elem2)
{
	        struct CA *el1;
	        struct CA *el2;

			el1 = (struct CA *) elem1;
			el2 = (struct CA *) elem2;
	        return ((int) (ratod(el1->a) - ratod (el2->a)) * casort);
}


static int sortca (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (caarr, caanz, sizeof (struct CA),
				   sortca0);
			casort *= -1;
            ShowNewElist ((char *) caarr,
                             caanz,
                            (int) sizeof (struct CA));
			return 0;
}


static int sortcbz0 (const void *elem1, const void *elem2)
{
	        struct CA *el1;
	        struct CA *el2;

			el1 = (struct CA *) elem1;
			el2 = (struct CA *) elem2;
			return ( (int) (strcmp (el1->a_bz1, el2->a_bz1)) * cbsort);
}

static int sortcbz (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (caarr, caanz, sizeof (struct CA),
				   sortcbz0);
			cbsort *= -1;
            ShowNewElist ((char *) caarr,
                             caanz,
                            (int) sizeof (struct CA));
			return 0;
}


int doartchoise (void)
/**
Auswahl ueber Artikel
**/
{
	    static long aanz = 0;
		double a;
		char a_bz1 [25];
		char a_bz2 [25];
		int cursor;
		int idx;
		int i;

		if (scrfx > (double) 1.0)
		{
                cartfont.FontHeight = (int) (double) ((double) 120 *  scrfx);
		}
		casort = cbsort = 1;
		if (caarr)
		{
			    GlobalFree (caarr);
		}
        DbClass.sqlout  ((long *) &aanz, 2, 0);
        DbClass.sqlcomm ("select count (*) from a_bas where a_typ  in (1,5,6,9,10,11,14 ) or "
			                                                "a_typ2 in (1,5,6,9,10,11,14 )");
		if (aanz == 0)
		{
			disp_messG ("Fehler beim Zuwaeisen von Speicherplatz", 2);
  	        EnableWindows (eKopfWindow, FALSE);
            EnableWindows (eFussWindow, FALSE);
            EnableWindows (BackWindow2, FALSE);
            EnableWindows (BackWindow3, FALSE);
            EnableWindows (eWindow, FALSE);
			return 0;
		}

        caarr = (struct CA *) GlobalAlloc (GMEM_FIXED, aanz * sizeof (struct CA));
		if (caarr == NULL)
		{
			disp_messG ("Fehler beim Zuweisen von Speicherplatz", 2);
  	        EnableWindows (eKopfWindow, FALSE);
            EnableWindows (eFussWindow, FALSE);
            EnableWindows (BackWindow2, FALSE);
            EnableWindows (BackWindow3, FALSE);
            EnableWindows (eWindow, FALSE);
			return 0;
		}

        DbClass.sqlout  ((double *) &a, 3, 0);
        DbClass.sqlout  ((char *)  a_bz1, 0, 25);
        DbClass.sqlout  ((char *)  a_bz2, 0, 25);
		if (sort_a == FALSE)
		{
		         cursor = DbClass.sqlcursor ("select a, a_bz1, a_bz2 from a_bas "
			                        "where a > 0 and a_typ in "
									"(1,5,6,9,10,11,14) or a_typ2 in"
									"(1,5,6,9,10,11,14) order by a_bz1");
		}
		else
		{
		         cursor = DbClass.sqlcursor ("select a, a_bz1, a_bz2 from a_bas "
			                        "where a > 0 and a_typ in "
									"(1,5,6,9,10,11,14) or a_typ2 in"
									"(1,5,6,9,10,11,14) order by a");
		}
		i = 0;
		while (DbClass.sqlfetch (cursor) == 0)
		{
			sprintf (caarr[i].a, "%13.0lf", a);
			strcpy  (caarr[i].a_bz1, a_bz1);
			strcpy  (caarr[i].a_bz2, a_bz2);
			i = i + 1;
		}
		DbClass.sqlclose (cursor);

		aanz = caanz = i;
        SetHoLines (1);
        SetVeLines (1);
		PtY = 410;
		if (scrfx > (double) 1.0)
		{
			PtY = (int) (double) ((double) PtY * scrfx);
		}
        syskey = 0;

        EnableWindow (NuBlock.GeteNumWindow (), FALSE);
        idx = ShowWKopf (NuBlock.GeteNumWindow (), (char *) caarr, aanz,
                        (char *) &ca, sizeof (struct CA),
                         &cartform, &artvl, &artub);
		CloseControls (&artub);
        EnableWindow (NuBlock.GeteNumWindow (), TRUE);
        SetHoLines (1);
	    EnableWindows (eKopfWindow, FALSE);
        EnableWindows (eFussWindow, FALSE);
        EnableWindows (BackWindow2, FALSE);
        EnableWindows (BackWindow3, FALSE);
        EnableWindows (eWindow, FALSE);
        if (syskey == KEY5 || syskey == KEYESC)
        {
			    GlobalFree (caarr);
		        caarr = NULL;
                return FALSE;
        }

        strcpy (NuBlock.Geteditbuff (), caarr[idx].a);
		GlobalFree (caarr);
		caarr = NULL;
		Sleep (50);
		display_form (NuBlock.GeteNumWindow (), NuBlock.GetEditForm (), 0, 0);
		NuBlock.SetEditFocus ();
		if (a_ausw_direct)
		{
			   NuBlock.NumEnterBreak ();
		}
		return 0;
}


// Auswahl ueber Artikel (Bezugsquellen)

mfont cbzgfont = {"Courier New", 120, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                0,
                                NULL};

struct CBZG
{
char a [14];
char a_best [17];
char a_bz1 [25];
};

static struct CBZG cbzg, *cbzgarr = NULL;
static int cbzganz;

static int sortcba ();
static int sortcbzg ();
static int sortcbbz ();

static field _cbzgform [] = {
	cbzg.a,         13, 0, 0, 1, 0, "%13.0f", DISPLAYONLY, 0, 0, 0,
	cbzg.a_best,    16, 0, 0,16, 0, "",       DISPLAYONLY, 0, 0, 0,
	cbzg.a_bz1,     24, 0, 0,34, 0, "",       DISPLAYONLY, 0, 0, 0,
};

static form cbzgform = {3, 0, 0, _cbzgform, 0, 0, 0, 0, &cbzgfont};


static field _bzgub [] =
{
        "    Artikel",     15, 1, 0, 0, 0, "", BUTTON, 0, sortcba, 0,
        " LAN",            17, 1, 0,15, 0, "", BUTTON, 0, sortcbzg, 1,
        " Bezeichnung",    40, 1, 0,32, 0, "", BUTTON, 0, sortcbbz, 2,
};

static form bzgub = {3, 0, 0, _bzgub, 0, 0, 0, 0, &cbzgfont};

static field _bzgvl [] =
{
        "1",                1, 1, 0,15, 0, "", DISPLAYONLY, 0, 0, 0,
        "1",                1, 1, 0,32, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form bzgvl = {2, 0, 0, _bzgvl, 0, 0, 0, 0, &cbzgfont};

static int cbasort   = 1;
static int cbzgsort = 1;
static int cbbsort   = 1;


static int sortcba0 (const void *elem1, const void *elem2)
{
	        struct CBZG *el1;
	        struct CBZG *el2;

			el1 = (struct CBZG *) elem1;
			el2 = (struct CBZG *) elem2;
	        return ((int) (ratod(el1->a) - ratod (el2->a)) * cbasort);
}


static int sortcba (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (cbzgarr, cbzganz, sizeof (struct CBZG),
				   sortcba0);
			cbasort *= -1;
            ShowNewElist ((char *) cbzgarr,
                             cbzganz,
                            (int) sizeof (struct CBZG));
			return 0;
}

static int sortcbzg0 (const void *elem1, const void *elem2)
{
	        struct CBZG *el1;
	        struct CBZG *el2;

			el1 = (struct CBZG *) elem1;
			el2 = (struct CBZG *) elem2;
	        return (strcmp (el1->a_best, el2->a_best) * cbzgsort);
}


static int sortcbzg (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (cbzgarr, cbzganz, sizeof (struct CBZG),
				   sortcbzg0);
			cbzgsort *= -1;
            ShowNewElist ((char *) cbzgarr,
                             cbzganz,
                            (int) sizeof (struct CBZG));
			return 0;
}


static int sortcbbz0 (const void *elem1, const void *elem2)
{
	        struct CBZG *el1;
	        struct CBZG *el2;

			el1 = (struct CBZG *) elem1;
			el2 = (struct CBZG *) elem2;
			return ((int) (strcmp (el1->a_bz1, el2->a_bz1)) * cbbsort);
}

static int sortcbbz (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (cbzgarr, cbzganz, sizeof (struct CBZG),
				   sortcbbz0);
			cbbsort *= -1;
            ShowNewElist ((char *) cbzgarr,
                             cbzganz,
                            (int) sizeof (struct CBZG));
			return 0;
}

BOOL GetBzgMdn (double a, short mdn, short fil, char *lief, char *a_best)
{
	    int cursor;
		int dsqlstatus;

	    DbClass.sqlin ((short *) &mdn, 1, 0);
	    DbClass.sqlin ((short *) &fil, 1, 0);
	    DbClass.sqlin ((char *)  lief, 0, 17);
		DbClass.sqlin ((double *) &a, 3, 0);

        DbClass.sqlout  ((char *)  a_best, 0, 17);
		cursor = DbClass.sqlcursor ("select lief_best from lief_bzg "
			                        "where mdn = ? "
									"and fil = ? "
									"and lief = ? "
									"and a = ?");
		if (cursor == -1) return FALSE;

		dsqlstatus = DbClass.sqlfetch (cursor);
		while (dsqlstatus == 100)
		{
			if (fil)
			{
				fil = 0;
			}
			else if (mdn)
			{
				mdn = 0;
			}
			else
			{
                DbClass.sqlclose (cursor);
				return FALSE;
			}
		    dsqlstatus = DbClass.sqlopen (cursor);
		    dsqlstatus = DbClass.sqlfetch (cursor);
		}
        DbClass.sqlclose (cursor);
		return TRUE;
}


int dobzgchoise (void)
/**
Auswahl ueber Artikel
**/
{
	    static long bzganz = 0;
		double a;
		char a_best [17];
		char a_bz1 [25];
		short mdn;
		short fil;
		double akt_a;
		int cursor;
		int idx;
		int i;

		if (scrfx > (double) 1.0)
		{
                cbzgfont.FontHeight = (int) (double) ((double) 120 *  scrfx);
		}
		casort = cbzgsort = cbsort = 1;
		if (cbzgarr)
		{
			    GlobalFree (cbzgarr);
		}
		DbClass.sqlin ((char *) lieferant, 0, 17);
        DbClass.sqlout  ((long *) &bzganz, 2, 0);
        DbClass.sqlcomm ("select count (*) from lief_bzg where lief = ?");
		if (bzganz == 0)
		{
			disp_messG ("Kein Eintrag in Bezugsquellen", 2);
  	        EnableWindows (eKopfWindow, FALSE);
            EnableWindows (eFussWindow, FALSE);
            EnableWindows (BackWindow2, FALSE);
            EnableWindows (BackWindow3, FALSE);
            EnableWindows (eWindow, FALSE);
			return 0;
		}

        cbzgarr = (struct CBZG *) GlobalAlloc (GMEM_FIXED, bzganz * sizeof (struct CBZG));
		if (cbzgarr == NULL)
		{
			disp_messG ("Fehler beim Zuaeisen von Speicherplatz", 2);
  	        EnableWindows (eKopfWindow, FALSE);
            EnableWindows (eFussWindow, FALSE);
            EnableWindows (BackWindow2, FALSE);
            EnableWindows (BackWindow3, FALSE);
            EnableWindows (eWindow, FALSE);
			return 0;
		}

/*
		DbClass.sqlin ((short *) &mdn, 1, 0);
		DbClass.sqlin ((short *) fil, 1, 0);
        DbClass.sqlout  ((double *) &a, 3, 0);
        DbClass.sqlout  ((char *)  a_best, 0, 17);
        DbClass.sqlout  ((char *)  a_bz1, 0, 25);
		cursor = DbClass.sqlcursor ("select lief_bzg.a, lief_bzg.lief_best, a_bas.a_bz1 from "
			                        "lief_bzg,a_bas "
			                        "where (lief_bzg.mdn = ? or lief_bzg.mdn = 0) "
									"and (lief_bzg.fil = ? or lief_bzg.fil = 0) "
									"and lief = ? and lief_bzg.a = a_bas.a "
									"order by a_bz1");
*/

		DbClass.sqlin ((char *) lieferant, 0, 17);
        DbClass.sqlout  ((double *) &a, 3, 0);
        DbClass.sqlout  ((char *)  a_best, 0, 17);
        DbClass.sqlout  ((char *)  a_bz1, 0, 25);
		cursor = DbClass.sqlcursor ("select lief_bzg.a, lief_bzg.lief_best, a_bas.a_bz1 from "
			                        "lief_bzg,a_bas "
									"where lief = ? and lief_bzg.a = a_bas.a "
									"order by a_bz1");
		i = 0;
		akt_a = (double) 0;
		mdn = we_kopf.mdn;
		fil = we_kopf.fil;
		while (DbClass.sqlfetch (cursor) == 0)
		{
			if (a == akt_a) continue;
			akt_a = a;
			if (GetBzgMdn (a, mdn, fil, lieferant, a_best) == FALSE) continue;
			sprintf (cbzgarr[i].a, "%13.0lf", a);
			strcpy  (cbzgarr[i].a_best, a_best);
			strcpy  (cbzgarr[i].a_bz1,  a_bz1);
			i = i + 1;
		}
		DbClass.sqlclose (cursor);

		bzganz = cbzganz = i;
        SetHoLines (1);
        SetVeLines (1);
		PtY = 410;
		if (scrfx > (double) 1.0)
		{
			PtY = (int) (double) ((double) PtY * scrfx);
		}
        syskey = 0;

        EnableWindow (NuBlock.GeteNumWindow (), FALSE);
        idx = ShowWKopf (NuBlock.GeteNumWindow (), (char *) cbzgarr, bzganz,
                        (char *) &cbzg, sizeof (struct CBZG),
                         &cbzgform, &bzgvl, &bzgub);
		CloseControls (&bzgub);
        EnableWindow (NuBlock.GeteNumWindow (), TRUE);
        SetHoLines (1);
	    EnableWindows (eKopfWindow, FALSE);
        EnableWindows (eFussWindow, FALSE);
        EnableWindows (BackWindow2, FALSE);
        EnableWindows (BackWindow3, FALSE);
        EnableWindows (eWindow, FALSE);
        if (syskey == KEY5 || syskey == KEYESC)
        {
			    GlobalFree (caarr);
		        caarr = NULL;
                return FALSE;
        }

        strcpy (NuBlock.Geteditbuff (), cbzgarr[idx].a_best);
		GlobalFree (cbzgarr);
		cbzgarr = NULL;
		Sleep (50);
		display_form (NuBlock.GeteNumWindow (), NuBlock.GetEditForm (), 0, 0);
		NuBlock.SetEditFocus ();

		if (a_ausw_direct)
		{
			   NuBlock.NumEnterBreak ();
		}

		return 0;
}

void BestpTxt2WepTxt ()
{
	static int cursor = -1;

	if (cursor == -1)
	{
		DbClass.sqlin ((long *) &best_pos.best_txt, 2, 0);
		DbClass.sqlout ((long *) &we_txt.zei, 2, 0);
		DbClass.sqlout ((char *) we_txt.txt, 0, sizeof (we_txt.txt));
		cursor = DbClass.sqlcursor ("select zei, txt from bestpt where nr = ? order by zei");
	}
	if (cursor == -1)
	{
		return;
	}
	we_pos.we_txt = GenWepTxt ();
	if (we_pos.we_txt != 0l)
	{
		we_txt.nr = we_pos.we_txt;
		DbClass.sqlopen (cursor);
        while (DbClass.sqlfetch (cursor) == 0)
		{
			cWept.dbupdate ();
		}

	}
}


void BestToWe (long best_blg)
/**
Bestellung in Wareneingang uebernehmen.
**/
{
	     int i;
		 int dsqlstatus;
		 char datum [12];

         memcpy (&we_pos, &we_pos_null, sizeof (we_pos));
		 sysdate (datum);
		 we_pos.mdn = we_kopf.mdn;
		 we_pos.fil = we_kopf.fil;
         strcpy (we_pos.lief, we_kopf.lief);
         strcpy (we_pos.lief_rech_nr, we_kopf.lief_rech_nr);
		 strcpy (we_pos.blg_typ, we_kopf.blg_typ);
		 i = 0;
		 best_pos.mdn      = we_kopf.mdn;
		 best_kopf.mdn      = we_kopf.mdn;
		 best_pos.fil      = we_kopf.fil;
		 strcpy (best_pos.lief, we_kopf.lief);
		 best_pos.best_blg = best_blg;
		 dsqlstatus = BestPosClass.dbreadfirst_o ("order by a");
		 while (dsqlstatus == 0)
		 {
 			      we_pos.a         = best_pos.a;
		          dsqlstatus = WePosClass.dbreadfirst ();
		          if (dsqlstatus == 100)
				  {
		                        we_pos.akv = dasc_to_long (datum);
		                        we_pos.erz_dat = LongNull;
				  }
		          we_pos.bearb = dasc_to_long (datum);
				  we_pos.inh       = best_pos.inh;
				  strcpy (we_pos.lief_best, best_pos.lief_best);
				  we_pos.best_me   = best_pos.me;
				  we_pos.gew   = best_pos.me * best_pos.inh; //130509
 				  strcpy (we_pos.sa_kz, best_pos.sa_kz);
				  we_pos.me_einh   = best_pos.me_einh;
				  we_pos.int_pos   = i + 1;
				  we_pos.hbk_dat   = LongNull;
                  we_pos.pr_ek     = best_pos.pr_ek_bto;
                  we_pos.pr_ek_nto = best_pos.pr_ek;
                  we_pos.pr_ek_euro   = best_pos.pr_ek_euro_bto;
                  we_pos.pr_ek_nto_eu = best_pos.pr_ek_euro;
                  we_pos.pr_fix = best_pos.pr_fix;
                  we_pos.we_txt = 0l;
				  if (best_pos.best_txt != 0l)
				  {
					  BestpTxt2WepTxt ();
				  }
/*** 290709 Preise hier nicht mehr holen , sind schon da aus der Bestellung !!!
//                  ReadPr ();
//                  we_pos.pr_ek         = pr_ek_bto0;
//                  we_pos.pr_ek_nto     = pr_ek0;
//                  we_pos.pr_ek_euro    = pr_ek_bto0_euro;
//                  we_pos.pr_ek_nto_eu  = pr_ek0_euro;

                  Reada_pr ();

				  best_pos.inh = (best_pos.inh == 0.0) ? 1.0 : best_pos.inh;
                  dsqlstatus =   WePreis.preis_holen (we_kopf.mdn,
                                                      we_kopf.fil,
                                                      we_kopf.lief,
                                                      we_pos.a);
//                  if (lief_bzg.me_kz[0] == '1')   nicht n�tig, stimmt immer
				  {
                               we_pos.pr_ek        = we_pos.pr_ek /  best_pos.inh;
                               we_pos.pr_ek_euro   = we_pos.pr_ek_euro /  best_pos.inh;
                               we_pos.pr_ek_nto    = we_pos.pr_ek_nto /  best_pos.inh;
                               we_pos.pr_ek_nto_eu = pr_ek0_euro /  best_pos.inh;
              				   we_pos.inh          = 1.0;
				  }


**/

//                  we_pos.pr_fil_ek = GetPrFilEk ();
//                  we_pos.pr_vk     = GetPrVk ();
				  WePosClass.dbupdate ();
				  i++;
				  dsqlstatus = BestPosClass.dbread_o ();
		 }
		 BestPosClass.dbclose_o ();
		 best_kopf.bearb_stat = VERBUCHT;
		 BestKopfClass.dbupdate ();
}


int menfunc8 (void)
/**
Function fuer Button 8
**/
{
         HWND aktfocus;
		 int dsqlstatus;
		 char best_blg [10];



  if (strlen(we_kopf.lief) == 0)
  {
         stopauswahl = FALSE;

//		 query_changed = TRUE;
//		 if (rdoptimize)
//		 {
                  EnterTyp = AUSWAHL;
                  AuftragKopf (AUSWAHL);
				  while (stopauswahl == FALSE)
				  {
                         AuftragKopf (AUSWAHL);
				  }
                  InitAufKopf ();
                  aktfocus = GetFocus ();
                  AktButton = GetAktButton ();
				  strcpy(we_kopf.lief,"");
				  return 0;
//		 }

         AuftragKopf (AUSWAHL);
         if (syskey == KEY5)
         {
                       InitAufKopf ();
         }
         else
         {
                      EnterTyp = ENTER;
                      AuftragKopf (ENTER);
         }
         aktfocus = GetFocus ();
         AktButton = GetAktButton ();
   	     strcpy(we_kopf.lief,"");
         return 0;
  }











		 while (TRUE)
		 {
                 NuBlock.SetChoise (dobestchoise, 0);
				 strcpy (best_blg, "0");
                 NuBlock.EnterNumBox (AufKopfWindow,"  Bestell-Nr  ",
                                               best_blg, 9, "%8d",
	      									  EntNuProc);
		         if (syskey != KEY5 && syskey != KEYESC)
				 {
					         strcpy(bestellhinweis,"");
  		                     best_kopf.mdn = we_kopf.mdn;
		                     best_kopf.fil = we_kopf.fil;
		                     best_kopf.best_blg = atol (best_blg);
							 strcpy (best_kopf.lief, we_kopf.lief);
		                     dsqlstatus = BestKopfClass.dbreadfirst ();
		                     if (dsqlstatus != 0 || (druck_zwang &&
								                     best_kopf.bearb_stat != 2))
							 {
				                   print_messG (2, "Bestellung %ld nicht gefunden", best_kopf.best_blg);
							 }
		                     else if (dsqlstatus != 0 || best_kopf.bearb_stat > 2)
							 {
				                   print_messG (2, "Bestellung %ld nicht gefunden", best_kopf.best_blg);
							 }
			                 else
							 {
 				                   TestBestMulti ();
								   sprintf (korestab[best_koresp], "%ld", best_kopf.best_blg);
								   *termtab[best_koresp] = best_kopf.best_term;
								   *lieftab[best_koresp] = best_kopf.lief_term;
								   sprintf(bestellhinweis,"Bestellbeleg: %ld   %s",best_kopf.best_blg,best_kopf.freifeld1); //100908 
								   beginwork ();
 			                       BestToWe (atol (best_blg));
                                   if (QsZwang)
                                   {
                                       menfunc7 ();
                                   }
                                   WriteLS ();
								   break;
							 }
				 }
				 else
				 {
					 break;
				 }
		 }
         aktfocus = GetFocus ();
         SetColFocus (MainButton.mask[0].feldid);
         AktButton = GetAktButton ();
         return 0;
}


int menfunc9 (void)
/**
Function fuer Button 9
**/
{
         HWND aktfocus;

         aktfocus = GetFocus ();
         AuswahlWiegArt ();
         AktButton = GetAktButton ();
         return 0;
}
/**
int menfunc10 (void)
{
         HWND aktfocus;

         aktfocus = GetFocus ();
//         AuswahlAbteilung ();
         AuswahlWiegArt ();
         AktButton = GetAktButton ();
         return 0;
}
***/
int menfunc10 (void)
/**
Function fuer Button 9
**/
{
         HWND aktfocus;
		 int Abteilung = 0;
         aktfocus = GetFocus ();
         Abteilung = AuswahlAbteilung () + 1;
		 if (Abteilungen)
		 {
			sprintf(Abteilungen,"%ld",Abteilung);
		 }
		 else
		 {
            Abteilungen = (char *) GlobalAlloc (GMEM_FIXED , 2);
			sprintf(Abteilungen,"%ld",Abteilung);
		 }

		 if (Abteilung == 1)
		 {
			sprintf (Abteilungtxt, "Abt.Fleisch");
		 }
		 else if (Abteilung == 2)
		 {
			sprintf (Abteilungtxt, "Abt.Handelsware");
		 }
		 else
		 {
			sprintf (Abteilungtxt, "Abteilungen");
		 }
	     display_field (BackWindow2, &MainButton.mask[ItPos (&MainButton, (char *) &ColAbteilung)]);

         AktButton = GetAktButton ();
         return 0;
}


void menue_end (void)
/**
Menue beenden.
**/
{
         PostQuitMessage (0);
}

int menfunc0 (void)
/**
Function fuer Button 10
**/
{
         HWND aktfocus;
         field *feld;

         feld = &MainButton2.mask[3];
         aktfocus = GetFocus ();
         menue_end ();
         return 0;
}

int func1 (void)
/**
Button fuer Pfeil links bearbeiten.
**/
{
         field *feld;

         if (KomplettAktiv)
         {
                 // PostMessage (NULL, WM_CHAR,  (WPARAM) ' ', 0l);
                 SelectAktCtr ();
                 return 1;
         }

         if (ListeAktiv)
         {
                 PostMessage (eWindow, WM_KEYDOWN,  (WPARAM) VK_LEFT, 0l);
                 return 1;
         }

         if (PtListeAktiv)
         {
                 PostMessage (PtWindow, WM_KEYDOWN,  (WPARAM) VK_LEFT, 0l);
                 return 1;
         }

         if (NumEnterAktiv)
         {
                 SetFocus (NumEnterWindow);
                 SendMessage (NumEnterWindow, WM_KEYDOWN,
                                     (WPARAM) VK_LEFT, 0l);
                 return 1;
         }

         feld = GetAktField ();
         SetFocus (feld->feldid);
         PostMessage (GetParent (feld->feldid), WM_KEYDOWN,
                                            (WPARAM) VK_LEFT,
                                            0l);
         feld = GetAktField ();
         SetColFocus (feld->feldid);
         display_form (BackWindow3, &MainButton2, 0, 0);
         AktButton = GetAktButton ();
         return 0;
}

int func2 (void)
/**
Button fuer Pfeil oben bearbeiten.
**/
{
         field *feld;

         if (KomplettAktiv)
         {
                 PostMessage (AktivDialog, WM_KEYDOWN,  (WPARAM) VK_UP, 0l);
                 return 1;
         }
         if (ListeAktiv)
         {
                 PostMessage (eWindow, WM_KEYDOWN,  (WPARAM) VK_UP, 0l);
                 return 1;
         }
         if (PtListeAktiv)
         {
                 PostMessage (PtWindow, WM_KEYDOWN,  (WPARAM) VK_UP, 0l);
                 return 1;
         }
         if (NumEnterAktiv)
         {
                 SetFocus (NumEnterWindow);
                 SendMessage (NumEnterWindow, WM_KEYDOWN,
                                     (WPARAM) VK_UP, 0l);
                 return 1;
         }
         feld = GetAktField ();
         SetFocus (feld->feldid);
         PostMessage (GetParent (feld->feldid), WM_KEYDOWN,
                                            (WPARAM) VK_UP,
                                            0l);
         feld = GetAktField ();
         SetColFocus (feld->feldid);
         display_form (BackWindow3, &MainButton2, 0, 0);
         AktButton = GetAktButton ();
         return 0;
}

int func3 (void)
/**
Button fuer Pfeil unten bearbeiten.
**/
{
         field *feld;

         if (KomplettAktiv)
         {
                 PostMessage (AktivDialog, WM_KEYDOWN,  (WPARAM) VK_DOWN, 0l);
                 return 1;
         }
         if (ListeAktiv)
         {
                 PostMessage (eWindow, WM_KEYDOWN,  (WPARAM) VK_DOWN, 0l);
                 return 1;
         }
         if (PtListeAktiv)
         {
                 PostMessage (PtWindow, WM_KEYDOWN,  (WPARAM) VK_DOWN, 0l);
                 return 1;
         }
         if (NumEnterAktiv)
         {
                 SetFocus (NumEnterWindow);
                 SendMessage (NumEnterWindow, WM_KEYDOWN,
                                     (WPARAM) VK_DOWN, 0l);
                 return 1;
         }
         feld = GetAktField ();
         SetFocus (feld->feldid);
         PostMessage (GetParent (feld->feldid), WM_KEYDOWN,
                                            (WPARAM) VK_DOWN,
                                             0l);
         feld = GetAktField ();
         SetColFocus (feld->feldid);
         display_form (BackWindow3, &MainButton2, 0, 0);
         AktButton = GetAktButton ();
         return 0;
}

int func4 (void)
/**
Button fuer Pfeil rechts bearbeiten.
**/
{
         field *feld;

         if (KomplettAktiv)
         {
                 PostMessage (AktivDialog, WM_KEYDOWN,  (WPARAM) ' ', 0l);
                 return 1;
         }

         if (ListeAktiv)
         {
                 PostMessage (eWindow, WM_KEYDOWN,  (WPARAM) VK_RIGHT, 0l);
                 return 1;
         }
         if (PtListeAktiv)
         {
                 PostMessage (PtWindow, WM_KEYDOWN,  (WPARAM) VK_RIGHT, 0l);
                 return 1;
         }
         if (NumEnterAktiv)
         {
                 SetFocus (NumEnterWindow);
                 SendMessage (NumEnterWindow, WM_KEYDOWN,
                                     (WPARAM) VK_RIGHT, 0l);
                 return 1;
         }

         feld = GetAktField ();
         SetFocus (feld->feldid);
         PostMessage (GetParent (feld->feldid), WM_KEYDOWN,
                                            (WPARAM) VK_RIGHT,
                                            0l);
         feld = GetAktField ();
         SetColFocus (feld->feldid);
         display_form (BackWindow3, &MainButton2, 0, 0);
         AktButton = GetAktButton ();
         return 0;
}

void backmenue (char *men)
/**
Vorhergehendes Menue ermitteln.
**/
{
          char *pos;

          menue_end ();
          return;

          pos = men + strlen (men) - 1;

          for (; pos >= men; pos --)
          {
                    if (*pos != '0')
                     {
                            *pos = '0';
                            break;
                    }
          }
}


int func5 (void)
/**
Function fuer Zurueckbutton bearbeiten
**/
{
         if (PtListeAktiv)
         {
                 PostMessage (PtWindow, WM_KEYDOWN,  (WPARAM) VK_F5, 0l);
                 return 1;
         }

         if (ListeAktiv)
         {
                 PostMessage (eWindow, WM_KEYDOWN,  (WPARAM) VK_F5, 0l);
                 return 1;
         }

         if (KomplettAktiv)
         {
                 PostMessage (AktivDialog, WM_KEYDOWN,  (WPARAM) VK_F5, 0l);
                 return 1;
         }
         if (NumEnterAktiv)
         {
                 SetFocus (NumEnterWindow);
                 PostMessage (NumEnterWindow, WM_KEYDOWN,
                                     (WPARAM) VK_F5, 0l);
                 return 1;
         }

         if (KopfEnterAktiv)
         {
                 PostMessage (AufKopfWindow, WM_KEYDOWN,
                                     (WPARAM) VK_F5, 0l);
                 return 1;
         }


         backmenue (menue.prog);
         return 0;
}

int dokey5 (void)
/**
Aktion bei Taste F5 (zurueck)
**/
{
         field *feld;

         menue_end ();
         return 0;

         backmenue (menue.prog);
         feld = &MainButton.mask[0];
         SetFocus (feld->feldid);
         AktButton = GetAktButton ();
         display_form (BackWindow2, &MainButton, 0, 0);
         UpdateWindow (BackWindow2);
         display_form (BackWindow3, &MainButton2, 0, 0);
         AktButton = GetAktButton ();
         return 0;
}


int func6 (void)
/**
Function fuer OK setzen.
**/
{
	     if (QmAktiv)
		 {
                  PostMessage (AufKopfWindow,
                               WM_KEYDOWN,  (WPARAM) VK_F12, 0l);
				  return 1;
		 }

         if (PtListeAktiv)
         {
                  PostMessage (PtWindow,
                               WM_KEYDOWN,  (WPARAM) VK_RETURN, 0l);
                 return 1;
         }


         if (KomplettAktiv)
         {
                 SelectAktCtr ();
                 return 1;
         }

         if (KopfEnterAktiv && ListeAktiv == 0)
         {
                 if (EnterTyp == SUCHEN)
                 {
                        PostMessage (AufKopfWindow,
                               WM_KEYDOWN,  (WPARAM) VK_F12, 0l);
                 }
                 else if (EnterTyp == AUSWAHL)
                 { 
					  int ls_direct = liste_direct;
					  liste_direct = 0;

						     strcpy(bestellhinweis,"");
  		                     best_kopf.mdn = akt_mdn;
		                     best_kopf.fil = akt_fil;
		                     best_kopf.best_blg = best_kopf.best_blg;
							 strcpy (best_kopf.lief, lieferant);
		                     dsqlstatus = BestKopfClass.dbreadfirst ();
							 if (dsqlstatus == 0)
							 {
								sprintf(bestellhinweis,"Bestellbeleg: %ld   %s",best_kopf.best_blg,best_kopf.freifeld1);
							 }

					  if(ReadLS() != -1) //100908 wenn abgeschlossen, nicht nochmal schreiben
					  {
						if (QsZwang)
						{
								menfunc7 ();
								if (syskey == KEY5)   //100908 bei Abbruch mit "Zur�ck"  darf nicht geschrieben werden
								{
			    					  liste_direct = ls_direct;
				                      PostMessage (AufKopfWindow,
					                   WM_KEYDOWN,  (WPARAM) VK_RETURN, 0l);
									  return 1;
								}
						}
						TestBestMulti ();
  						sprintf (korestab[best_koresp], "%ld", best_kopf.best_blg);
						*termtab[best_koresp] = best_kopf.best_term;
						*lieftab[best_koresp] = best_kopf.lief_term;
						beginwork ();
						BestToWe (best_kopf.best_blg);
						WriteLS ();
    					  liste_direct = ls_direct;

	                      PostMessage (AufKopfWindow,
                               WM_KEYDOWN,  (WPARAM) VK_F12, 0l);
					  }
					  else
					  {
  						liste_direct = ls_direct;

                        PostMessage (AufKopfWindow,
                               WM_KEYDOWN,  (WPARAM) VK_RETURN, 0l);  //100908 herVK_RETURN, da sonst WriteLS 
					  }
                 }
                 else
                 {
                        PostMessage (AufKopfWindow,
                               WM_KEYDOWN,  (WPARAM) VK_RETURN, 0l);
                 }
                 return 1;
         }

         if (LiefEnterAktiv && ListeAktiv == 0) 
         {
                 syskey = KEY12;
                 PostMessage (AufKopfWindow,
                               WM_KEYDOWN,  (WPARAM) VK_F12, 0l);
                 return 1;
         }

         if (ListeAktiv == 0)
         {
                 return 0;
         }

         PostMessage (eWindow, WM_KEYDOWN,  (WPARAM) VK_RETURN, 0l);
         return 0;
}

int func7 (void)
/**
Function fuer OK setzen.
**/
{
         extern BOOL StartWiegen;
         if (KomplettAktiv) return 0;

         memcpy (&aufps, &aufp_null, sizeof (aufps));
         StartWiegen = TRUE;
//         beginwork ();
         ArtikelWiegen (LogoWindow);
//         commitwork ();
         StartWiegen = FALSE;
         memcpy (&aufps, &aufparr[0], sizeof (aufps));
         
         return 0;
}

int func8 (void)
/**
Function fuer Benutzer bearbeiten
**/
{
         if (ListeAktiv == 0) return 0;
         if (KomplettAktiv) return 0;

         EnableWindow (eKopfWindow, FALSE);
         EnableWindow (eFussWindow, FALSE);
         SetKomplett ();
         EnableWindow (eKopfWindow, TRUE);
         EnableWindow (eFussWindow, TRUE);
         break_list ();
         return 0;
}


/*
void printlogo (HBITMAP hbr, int x, int y, DWORD mode)
{

        int bmy;
        int bmx;
        HDC      hdc;

        GetObject (hbr, sizeof (BITMAP), &bm);
        bmy = bm.bmHeight / 2;
        bmx = bm.bmWidth / 2;
        hdc = fithdc;
        hdcMemory  = CreateCompatibleDC(hdc);
        hbmOld = SelectObject (hdcMemory, hbr);

        SetViewportOrgEx (hdc, bmx, bmy, NULL);
        SetStretchBltMode (hdc, COLORONCOLOR);
        StretchBlt (hdc, -bmx + xstretch / 2, -bmy + ystretch / 2,
                         bm.bmWidth - xstretch, bm.bmHeight - ystretch,
                         hdcMemory,0, 0,
                         bm.bmWidth, bm.bmHeight, mode);

        SelectObject (hdcMemory, hbmOld);

        DeleteDC (hdcMemory);
        if (sign == 1)
        {
            ystretch += (plus * logory);
            if (ystretch >= bm.bmHeight * 2)
            {
                        ystretch = bm.bmHeight * 2;
                        sign = -1;
            }
        }
        else if (sign == -1)
        {
            ystretch -= (plus * logory);
            if (ystretch <= 0)
            {
                        ystretch = 0;
                        sign = 1;
            }
        }
        if (signx == 1)
        {
            xstretch += (xplus * logorx);
            if (xstretch >= bm.bmWidth * 2)
            {
                        xstretch = bm.bmWidth * 2;
                        signx = -1;
            }
        }
        else if (signx == -1)
        {
            xstretch -= (xplus * logorx);
            if (xstretch <= 0)
            {
                        xstretch = 0;
                        signx = 1;
            }
        }
}
*/

void paintlogo (HDC hdc, HBITMAP hbr, int x, int y, DWORD mode)
{

        int bmy;
        int bmx;

        GetObject (hbr, sizeof (BITMAP), &bm);
        bmy = bm.bmHeight / 2;
        bmx = bm.bmWidth / 2;
        hdcMemory  = CreateCompatibleDC(hdc);
        hbmOld = SelectObject (hdcMemory, hbr);

        SetViewportOrgEx (hdc, bmx, bmy, NULL);
        SetStretchBltMode (hdc, COLORONCOLOR);
        StretchBlt (hdc, -bmx + xstretch / 2, -bmy + ystretch / 2,
                         bm.bmWidth - xstretch, bm.bmHeight - ystretch,
                         hdcMemory,0, 0,
                         bm.bmWidth, bm.bmHeight, mode);

        SelectObject (hdcMemory, hbmOld);

        DeleteDC (hdcMemory);
        if (sign == 1)
        {
            ystretch += (plus * logory);
            if (ystretch >= bm.bmHeight * 2)
            {
                        ystretch = bm.bmHeight * 2;
                        sign = -1;
            }
        }
        else if (sign == -1)
        {
            ystretch -= (plus * logory);
            if (ystretch <= 0)
            {
                        ystretch = 0;
                        sign = 1;
            }
        }
        if (signx == 1)
        {
            xstretch += (xplus * logorx);
            if (xstretch >= bm.bmWidth * 2)
            {
                        xstretch = bm.bmWidth * 2;
                        signx = -1;
            }
        }
        else if (signx == -1)
        {
            xstretch -= (xplus * logorx);
            if (xstretch <= 0)
            {
                        xstretch = 0;
                        signx = 1;
            }
        }
}

void strechbmp (HBITMAP hbr, int x, int y, DWORD mode)
{

        int bmy;
        int bmx;
		int bmyst;
        int bmxst;
        HDC      hdc;

        xstretch = ystretch = 0;
        GetObject (hbr, sizeof (BITMAP), &bm);
		bmxst = (int) (double) ((double) bm.bmWidth * scrfx);
		bmyst = (int) (double) ((double) bm.bmHeight * scrfy);

        bmy = bmyst / 2;
        bmx = bmxst / 2;
//        bmy = bm.bmHeight / 2;
//        bmx = bm.bmWidth / 2;
        hdc = fithdc;
        hdcMemory  = CreateCompatibleDC(hdc);
        hbmOld = SelectObject (hdcMemory, hbr);

        SetViewportOrgEx (hdc, bmx, bmy, NULL);
        SetStretchBltMode (hdc, COLORONCOLOR);
        StretchBlt (hdc, -bmx + xstretch / 2, -bmy + ystretch / 2,
                         bmxst - xstretch, bmyst - ystretch,
                         hdcMemory,0, 0,
                         bm.bmWidth, bm.bmHeight, mode);

        SelectObject (hdcMemory, hbmOld);

        DeleteDC (hdcMemory);
}


void printlogo (HBITMAP hbr, int x, int y, DWORD mode)
{

        int bmy;
        int bmx;
		int bmyst;
        int bmxst;
        HDC      hdc;

//        GetObject (hbr, sizeof (BITMAP), &bm);
		bmxst = (int) (double) ((double) bm.bmWidth * scrfx);
		bmyst = (int) (double) ((double) bm.bmHeight * scrfy);

        bmy = bmyst / 2;
        bmx = bmxst / 2;
//        bmy = bm.bmHeight * scrfy / 2);
//        bmx = bm.bmWidth / 2;
//        hdc = fithdc;

/*
		xstretch = (int) (double) ((double) xstretch * scrfx);
		ystretch = (int) (double) ((double) ystretch * scrfy);
*/

        hdc = GetDC (FitWindow);
        hdcMemory  = CreateCompatibleDC(hdc);
        hbmOld = SelectObject (hdcMemory, hbr);

        SetViewportOrgEx (hdc, bmx, bmy, NULL);
        SetStretchBltMode (hdc, COLORONCOLOR);
        StretchBlt (hdc, -bmx + xstretch / 2, -bmy + ystretch / 2,
                         bmxst - xstretch, bmyst - ystretch,
                         hdcMemory,0, 0,
                         bm.bmWidth, bm.bmHeight, mode);

//        SelectObject (hdcMemory, hbmOld);

        DeleteDC (hdcMemory);
        ReleaseDC (FitWindow, hdc);
        if (sign == 1)
        {
            ystretch += (plus * logory);
            if (ystretch >= bmyst * 2)
            {
                        ystretch = bmyst * 2;
                        sign = -1;
            }
        }
        else if (sign == -1)
        {
            ystretch -= (plus * logory);
            if (ystretch <= 0)
            {
                        ystretch = 0;
                        sign = 1;
            }
        }
        if (signx == 1)
        {
            xstretch += (xplus * logorx);
            if (xstretch >= bmxst * 2)
            {
                        xstretch = bmxst * 2;
                        signx = -1;
            }
        }
        else if (signx == -1)
        {
            xstretch -= (xplus * logorx);
            if (xstretch <= 0)
            {
                        xstretch = 0;
                        signx = 1;
            }
        }
}

void LogoBitmap (HDC hdc)
/**
Bitmap in Lo�gofenster schreiben.
**/
{
	    RECT rect;

		GetClientRect (LogoWindow, &rect);

		bMap.StrechBitmap (hdc, rect.right, rect.bottom);
}


/*
void strechbmp (HBITMAP hbr, int x, int y, DWORD mode)
{

        HDC      hdc;

        GetObject (hbr, sizeof (BITMAP), &bm);
        hdc = fithdc;
        hdcMemory  = CreateCompatibleDC(hdc);
        hbmOld = SelectObject (hdcMemory, hbr);

        // SetViewportOrgEx (hdc, bmx, bmy, NULL);
        SetStretchBltMode (hdc, COLORONCOLOR);
        StretchBlt (hdc, 0, 0,
                         bm.bmWidth, bm.bmHeight,
                         hdcMemory,0, 0,
                         bm.bmWidth, bm.bmHeight, mode);

        SelectObject (hdcMemory, hbmOld);
        DeleteDC (hdcMemory);
}
*/

void printbmp (HBITMAP hbr, int x, int y, DWORD mode)
{
        RECT rect;

        GetClientRect (hMainWindow,  &rect);
        GetObject (hbr, sizeof (BITMAP), &bm);
        x = (rect.right - bm.bmWidth) / 2;
        y = (rect.bottom - bm.bmHeight) / 2;
        hdc = GetDC (hFitWindow);
        hdcMemory  = CreateCompatibleDC(hdc);
        hbmOld = SelectObject (hdcMemory, hbr);

        BitBlt (hdc, x, y, bm.bmWidth, bm.bmHeight,
                           hdcMemory,0, 0, mode);

        SelectObject (hdcMemory, hbmOld);

        DeleteDC (hdcMemory);
        ReleaseDC (hFitWindow, hdc);
}

int ScrollMenue ()
/**
Leere MenueZeilen entfernrn.
**/
{
        int i, j;

        memcpy (&scmenue, &menue, sizeof (MENUE));
        memcpy (&menue,   &leermenue, sizeof (MENUE));
        menue.mdn = scmenue.mdn;
        menue.fil = scmenue.fil;
        strcpy (menue.pers, scmenue.pers);
        strcpy (menue.prog, scmenue.prog);
        menue.anz_menue = scmenue.anz_menue;

        for (i = 0,j = 0; j < 10; j ++)
        {
                clipped (mentab[j]);
                if (strcmp (mentab[j], " ") > 0)
                {
                                    strcpy (mentab0[i], mentab[j]);
                                    strcpy (zeitab0[i], zeitab[j]);
                                    i ++;
                }
        }
        return (i);
}

void GetAutotara (void)
/**
Default fuer Atou-Tara holen und setzen.
**/
{
	    char tara_auto [2];

        strcpy (tara_auto, "N");
        DbClass.sqlin   ((long *) &StdWaage, 2, 0);
        DbClass.sqlout  ((char *) tara_auto, 0, 2);
        DbClass.sqlcomm ("select tara_auto from opt_mci where sys = ?");
		if (tara_auto [0] == 'J')
		{
			setautotara (TRUE);
		}
		else
		{
			setautotara (FALSE);
		}
}


//  Hauptprogramm

int      PASCAL WinMain(HANDLE hInstance,HANDLE hPrevInstance,
                        LPSTR lpszCmdLine,int nCmdShow)
{
        HCURSOR oldcursor;
        char cfg_v [80];
		HDC hdc;
		char buffer [256];

		sprintf (Abteilungtxt, "Abteilungen");
		sprintf (Zuruecktext,"Zur�ck F5");


	    GetForeground ();
        GraphikMode = IsGdiPrint ();
        if (ProgCfg.GetCfgValue ("Abteilungen", cfg_v) ==TRUE)   //120908 nur best. Abteilungen zulassen (String z.B. "1,2,3")
        {
			         if (strcmp (clipped(cfg_v), "NO"))
					 {
                           Abteilungen = (char *) GlobalAlloc (GMEM_FIXED , strlen (cfg_v) + 1);
				           strcpy (Abteilungen, cfg_v);
					       if (Abteilungen)
						   {
							 if (atoi(Abteilungen) == 1)
							 {
								sprintf (Abteilungtxt, "Abt.Fleisch");
							 }
							 else if (atoi(Abteilungen) == 2)
							 {
								sprintf (Abteilungtxt, "Abt.Handelsware");
							 }

						   }
					 }
        }
        if (ProgCfg.GetCfgValue ("Schwundabzug", cfg_v) == TRUE)
		{
			         Schwundabzug = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("ScanMulti", cfg_v) == TRUE)
		{
			         ScanMulti = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("DruckAuswahl", cfg_v) ==TRUE)
        {
                    DruckAuswahl = atoi (cfg_v);  //LuD 020609
        }
        if (ProgCfg.GetCfgValue ("ohne_preis", cfg_v) ==TRUE)
        {
                    cfg_ohne_preis = atoi (cfg_v);  //LuD 090908
        }
        if (ProgCfg.GetCfgValue ("MehrfachChargen", cfg_v) ==TRUE)
        {
                    cfg_MehrfachChargen = atoi (cfg_v);  //LuD 171110
        }
        if (ProgCfg.GetCfgValue ("auto_eti", cfg_v) ==TRUE)
        {
                    auto_eti = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("colborder", cfg_v) ==TRUE)
        {
                     colborder = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("lsdefault", cfg_v) ==TRUE)
        {
                     lsdefault = min (1, max (0,atoi (cfg_v)));
        }

        if (ProgCfg.GetCfgValue ("best_auswahl_direct", cfg_v) ==TRUE)
        {
                     best_ausw_direct = min (1, max (0,atoi (cfg_v)));
        }

        if (ProgCfg.GetCfgValue ("liste_direct", cfg_v) ==TRUE)
        {
                     liste_direct = min (1, max (0,atoi (cfg_v)));
        }

        if (ProgCfg.GetCfgValue ("lief_bzg_zwang", cfg_v) ==TRUE)
        {
                     lief_bzg_zwang = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("druck_zwang", cfg_v) ==TRUE)
        {
                     druck_zwang = atoi (cfg_v);
        }


        if (ProgCfg.GetCfgValue ("a_auswahl_direct", cfg_v) ==TRUE)
        {
                     a_ausw_direct = atoi (cfg_v);
        }

        if (ProgCfg.GetCfgValue ("lief_auswahl_direct", cfg_v) ==TRUE)
        {
                     lief_ausw_direct = atoi (cfg_v);
        }

        if (ProgCfg.GetCfgValue ("hbk_direct", cfg_v) ==TRUE)
        {
                     hbk_direct = atoi (cfg_v);
        }

        if (ProgCfg.GetCfgValue ("multibest", cfg_v) ==TRUE)
        {
                     multibest = min (1, max (0,atoi (cfg_v)));
        }

        if (ProgCfg.GetCfgValue ("wieg_neu_direct", cfg_v) ==TRUE)
        {
                     wieg_neu_direct = atoi (cfg_v);
        }

        if (ProgCfg.GetCfgValue ("AutoSleep", cfg_v) ==TRUE)
        {
                     AutoSleep = atol (cfg_v);
        }

        if (ProgCfg.GetCfgValue ("wieg_direct", cfg_v) ==TRUE)
        {
                     wieg_direct = atoi (cfg_v);
        }

        if (ProgCfg.GetCfgValue ("hand_direct", cfg_v) ==TRUE)
        {
                     hand_direct = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("sort_a", cfg_v) == TRUE)
        {
		             sort_a =  min (1, max (0, atoi (cfg_v)));
        }

        if (ProgCfg.GetCfgValue ("hand_tara_sum", cfg_v) == TRUE)
        {
		             hand_tara_sum =  atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("fest_tara_sum", cfg_v) == TRUE)
        {
		             fest_tara_sum =  atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("fest_tara_anz", cfg_v) == TRUE)
        {
		             fest_tara_anz =  atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("EinhEk", cfg_v) == TRUE)
        {
		             EktoMeEinh =  atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("autocursor", cfg_v) == TRUE)
		{
			         autocursor = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("DrKomplett", cfg_v) == TRUE)
		{
			         DrKomplett = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("MultiTemperature", cfg_v) == TRUE)
		{
			         MultiTemperature = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("MesswerteZwang", cfg_v) == TRUE)
		{
			         MesswerteZwang = atoi (cfg_v);
		}

        if (getenv ("testmode"))
        {
                     testmode = atoi (getenv ("testmode"));
        }
        if (ProgCfg.GetCfgValue ("mdn_default", cfg_v) ==TRUE)
        {
                    akt_mdn = atoi (cfg_v);
                    Mandant = atoi (cfg_v);

        }
        if (ProgCfg.GetCfgValue ("PassWord", cfg_v) == TRUE)
		{
			         PassWord = atoi (cfg_v);
					 SetPassWordFlag (PassWord);
		}
        if (ProgCfg.GetCfgValue ("fil_default", cfg_v) ==TRUE)
        {
                    akt_fil = atoi (cfg_v);
                    Filiale = atoi (cfg_v);
        }

        if (ProgCfg.GetCfgValue ("QsZwang", cfg_v) ==TRUE)
        {
                    QsZwang = atoi (cfg_v); 
					//QSZwang = 1 : Zwang bie Kopf und Pos
					//QSZwang = 2 : Zwang nur bei Kopf
        }
        if (ProgCfg.GetCfgValue ("Rindfleischdaten", cfg_v) ==TRUE)
        {
			         WithZusatz = atoi (cfg_v); 
		}
        if (ProgCfg.GetCfgValue ("generiere_charge", cfg_v) ==TRUE)
        {
			         generiere_charge = atoi (cfg_v); 
		}
        if (ProgCfg.GetCfgValue ("ScannMode", cfg_v) ==TRUE)
        {
                    ScannMode = atoi (cfg_v);
        }

		WithSpezEti = FALSE;
        if (ProgCfg.GetCfgValue ("eti_kiste", cfg_v) ==TRUE)
        {
			         if (strcmp (cfg_v, "NO"))
					 {
                           eti_kiste = (char *) GlobalAlloc (GMEM_FIXED , strlen (cfg_v) + 1);
					       if (eti_kiste)
						   {
					           strcpy (eti_kiste, cfg_v);
						   }
					 }
		}
        if (ProgCfg.GetCfgValue ("eti_ki_drk", cfg_v) ==TRUE)
        {
			         if (strcmp (cfg_v, "NO"))
					 {
                           eti_ki_drk = (char *) GlobalAlloc (GMEM_FIXED , strlen (cfg_v) + 1);
					       if (eti_ki_drk)
						   {
					           strcpy (eti_ki_drk, cfg_v);
						   }
					 }
		}

        if (ProgCfg.GetCfgValue ("eti_ki_typ", cfg_v) ==TRUE)
        {
                     eti_ki_typ = atoi (cfg_v);
		}

        if (ProgCfg.GetCfgValue ("eti_nve", cfg_v) ==TRUE)
        {
			         if (strcmp (cfg_v, "NO"))
					 {
                           eti_nve = (char *) GlobalAlloc (GMEM_FIXED , strlen (cfg_v) + 1);
					       if (eti_nve)
						   {
					           strcpy (eti_nve, cfg_v);
						   }
					 }
		}
        if (ProgCfg.GetCfgValue ("eti_nve_drk", cfg_v) ==TRUE)
        {
			         if (strcmp (cfg_v, "NO"))
					 {
                           eti_nve_drk = (char *) GlobalAlloc (GMEM_FIXED , strlen (cfg_v) + 1);
					       if (eti_nve_drk)
						   {
					           strcpy (eti_nve_drk, cfg_v);
						   }
					 }
		}

        if (ProgCfg.GetCfgValue ("eti_nve_typ", cfg_v) ==TRUE)
        {
                     eti_nve_typ = atoi (cfg_v);
		}

        if (ProgCfg.GetCfgValue ("eti_ez", cfg_v) ==TRUE)
        {
			         if (strcmp (cfg_v, "NO"))
					 {
                            eti_ez = (char *) GlobalAlloc (GMEM_FIXED , strlen (cfg_v) + 1);
					        if (eti_ez)
							{
					           strcpy (eti_ez, cfg_v);
							}
					 }
		}
        if (ProgCfg.GetCfgValue ("eti_ez_drk", cfg_v) ==TRUE)
        {
			         if (strcmp (cfg_v, "NO"))
					 {
                            eti_ez_drk = (char *) GlobalAlloc (GMEM_FIXED , strlen (cfg_v) + 1);
					        if (eti_ez_drk)
							{
					           strcpy (eti_ez_drk, cfg_v);
							}
					 }
		}

        if (ProgCfg.GetCfgValue ("eti_ez_typ", cfg_v) ==TRUE)
        {
                     eti_ez_typ = atoi (cfg_v);
		}

        if (ProgCfg.GetCfgValue ("eti_ev", cfg_v) ==TRUE)
        {
			         if (strcmp (cfg_v, "NO"))
					 {
                            eti_ev = (char *) GlobalAlloc (GMEM_FIXED , strlen (cfg_v) + 1);
					        if (eti_ev)
							{
					           strcpy (eti_ev, cfg_v);
							}
					 }
		}

        if (ProgCfg.GetCfgValue ("eti_ev_drk", cfg_v) ==TRUE)
        {
			         if (strcmp (cfg_v, "NO"))
					 {
                            eti_ev_drk = (char *) GlobalAlloc (GMEM_FIXED , strlen (cfg_v) + 1);
					        if (eti_ev_drk)
							{
					           strcpy (eti_ev_drk, cfg_v);
							}
					 }
		}

        if (ProgCfg.GetCfgValue ("eti_ev_typ", cfg_v) ==TRUE)
        {
                     eti_ev_typ = atoi (cfg_v);
		}

        if (ProgCfg.GetCfgValue ("eti_artikel", cfg_v) ==TRUE)
        {
			         if (strcmp (cfg_v, "NO"))
					 {
                            eti_artikel = (char *) GlobalAlloc (GMEM_FIXED , strlen (cfg_v) + 1);
					        if (eti_artikel)
							{
					           strcpy (eti_artikel, cfg_v);
							}
					 }
		}

        if (ProgCfg.GetCfgValue ("eti_a_drk", cfg_v) ==TRUE)
        {
			         if (strcmp (cfg_v, "NO"))
					 {
                            eti_a_drk = (char *) GlobalAlloc (GMEM_FIXED , strlen (cfg_v) + 1);
					        if (eti_a_drk)
							{
					           strcpy (eti_a_drk, cfg_v);
							}
					 }
		}

        if (ProgCfg.GetCfgValue ("eti_a_typ", cfg_v) ==TRUE)
        {
                     eti_a_typ = atoi (cfg_v);
		}

        if (ProgCfg.GetCfgValue ("EnterEtiAnz", cfg_v) ==TRUE)
        {
			         TOUCHF::EnterAnz = atoi (cfg_v);
		}

        if (ProgCfg.GetCfgValue ("Scannen", cfg_v) == TRUE)
		{
			         Scannen = atoi (cfg_v);
		}

        if (ProgCfg.GetCfgValue ("SmallScann", cfg_v) == TRUE)
		{
			         SmallScann = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("Scanlen", cfg_v) == TRUE)
		{
			         Scanlen = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("ChargeZwang", cfg_v) ==TRUE)
        {
					ChargeZwang = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("LagerZwang", cfg_v) ==TRUE)
        {
					LagerZwang = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("ScanCharge", cfg_v) ==TRUE)
        {
					ScanCharge = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("LagerAuswahl", cfg_v) ==TRUE)
        {
					WithLagerAuswahl = atoi (cfg_v);
        }
        if (ProgCfg.GetGroupDefault ("lager", cfg_v) == TRUE)
        {
                    default_lager =  atol (cfg_v);
		}

// LOOK&Feel 18.11.2010 RoW

       if (ProgCfg.GetCfgValue ("NewLookAndFeel", cfg_v) == TRUE)
       {
		            LOOK_AND_FEEL->SetDynColor ((BOOL) atoi (cfg_v));
					DynColor = (BOOL) atoi (cfg_v);
					NewLookAndFeel = atoi (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("LogoDynBackground", cfg_v) == TRUE)
       {
					LogoDynBackground = atoi (cfg_v);
					if (LogoDynBackground > 1)
					{
							NewAufKopfAttributes ();
							NewWeKopfAttributes ();
					}
       }

       if (ProgCfg.GetCfgValue ("LookAndFeelButtonColor", cfg_v) == TRUE)
       {
		   LOOK_AND_FEEL->SetDynBkColor (clipped (cfg_v));
       }

       if (NewLookAndFeel)
	   {
			SetDynBkColor ();
	   }

        Scanean.Read ();

		if (eti_kiste && eti_ki_drk)
		{
			         WithSpezEti = TRUE;
		}
		else
		{
			eti_kiste = eti_ki_drk = NULL;
		}

		if (eti_ez && eti_ez_drk)
		{
			         WithSpezEti = TRUE;
		}
		else
		{
			eti_ez = eti_ez_drk = NULL;
		}

		if (eti_ev && eti_ev_drk)
		{
			         WithSpezEti = TRUE;
		}
		else
		{
			eti_ev = eti_ev_drk = NULL;
		}

		if (eti_artikel && eti_a_drk)
		{
			         WithSpezEti = TRUE;
		}
		else
		{
			eti_artikel = eti_a_drk = NULL;
		}

		Touchf.SetEtiAttr (eti_kiste, eti_ez, eti_ev, eti_artikel);

		SetScannMode ();
        Kist = new BMAP;
		if (Kist)
		{
                  hdc = GetDC (hMainWindow);
		          sprintf (buffer, "%s\\kiste2.bmp", getenv ("BWSETC"));
		          KistBmp = Kist->ReadBitmap (hdc, buffer);
				  ReleaseDC (hMainWindow, hdc);
		}
        KistG= new BMAP;
		if (KistG)
		{
                  hdc = GetDC (hMainWindow);
		          sprintf (buffer, "%s\\kiste2g.bmp", getenv ("BWSETC"));
		          KistBmpG = KistG->ReadBitmap (hdc, buffer);
				  ReleaseDC (hMainWindow, hdc);
		}

        KistMask = new BMAP;
		if (KistMask)
		{
                  hdc = GetDC (hMainWindow);
		          sprintf (buffer, "%s\\kiste2.msk", getenv ("BWSETC"));
		          KistMaskBmp = KistMask->ReadBitmap (hdc, buffer);
				  ReleaseDC (hMainWindow, hdc);
		}

		NoteIcon = LoadIcon (hInstance, "NOTEICON");

		LtBlueBrush = CreateSolidBrush (BLUECOL);
		YellowBrush = CreateSolidBrush (YELLOWCOL);
        opendbase ("bws");
		akt_lager = default_lager;
        if ( lgr_class.lese_lgr_bz (akt_lager) == 0) sprintf (Lagertxt,"Lager: %s",lgr.lgr_bz);
         memcpy (&a_lgr, &a_lgr_null, sizeof (struct A_LGR));

        sysdate (KomDatum);
        menue_class.SetSysBen ();
//        if (sys_ben.berecht != 0)
//        {
//                   cfg_ohne_preis = 1;
//        }

        if (getenv ("WAAKT"))
        {
                    strcpy (waadir, getenv ("WAAKT"));
                    sprintf (ergebnis, "%s\\ergebnis", waadir);
        }

        menue_class.Lesesys_inst ();
        FillQsHead ();
        FillQsPos();

        auszeichner = GetStdAuszeichner ();
        sys_peri_class.lese_sys (auszeichner);

        StdWaage = GetStdWaage ();
        sys_peri_class.lese_sys (StdWaage);
        la_zahl = sys_peri.la_zahl;

		GetAutotara ();

        strcpy (sys_par.sys_par_nam, "immer_preis");
        if (sys_par_class.dbreadfirst () == 0)
        {
                    immer_preis = atoi (sys_par.sys_par_wrt);
        }

        strcpy (sys_par.sys_par_nam, "ls_leer_zwang");
        if (sys_par_class.dbreadfirst () == 0)
        {
                    ls_leer_zwang = atoi (sys_par.sys_par_wrt);
        }

        strcpy (sys_par.sys_par_nam, "ls_charge_par");
        if (sys_par_class.dbreadfirst () == 0)
        {
                    ls_charge_par = atoi (sys_par.sys_par_wrt);
        }
        strcpy (sys_par.sys_par_nam, "lsc2_par");
        if (sys_par_class.dbreadfirst () == 0)
        {
                    lsc2_par = atoi (sys_par.sys_par_wrt);
        }

        strcpy (sys_par.sys_par_nam, "multimandant");
        if (sys_par_class.dbreadfirst () == 0)
        {
                    multimandant_par = atoi (sys_par.sys_par_wrt);
        }

        sprintf (Mandanttxt, "%s %hd", Mtxt, Mandant);
        sprintf (Filialetxt, "%s %hd", Ftxt, Filiale);

		ColBorder = colborder;
        if (strcmp (clipped (sys_inst.projekt), "F H S"))
        {
                    fitbmp   = LoadBitmap (hInstance, "fit1");
        }
        else
        {
                    fitbmp   = LoadBitmap (hInstance, "fhs");
                    IsFhs = 1;
        }
        PfeilL.bmp = LoadBitmap (hInstance, "pfeill");
        PfeilR.bmp = LoadBitmap (hInstance, "pfeilr");
        PfeilU.bmp = LoadBitmap (hInstance, "pfeilu");
        PfeilO.bmp = LoadBitmap (hInstance, "pfeilo");
        GetObject (fitbmp, sizeof (BITMAP), &bm);

        hMainInst = hInstance;
        if (getenv (BWS))
        {
                   strcpy (fitpath, getenv (BWS));
        }
        else
        {
                   strcpy (fitpath, "\\USER\\BWS8000");
        }


        strcpy (rosipath, getenv (RSDIR));

        GetObject (fitbmp, sizeof (BITMAP), &fitbm);

        InitFirstInstance (hInstance);
        if (InitNewInstance (hInstance, nCmdShow) == 0) return 0;

		Qmkopf = new QMKOPF (hMainInst, LogoWindow);
		Qmkopf->SetDevice (scrfy, scrfy);
//		Qmkopf->SetDevice (0.75, 0.75);
        ActivateColButton (BackWindow2, &MainButton,  2, 1, 1); //LUD 090908 Bestellungen schon zu anfang aktivieren

        oldcursor = SetCursor (LoadCursor (NULL, IDC_WAIT));
        WaageInit ();
        SetCursor (oldcursor);
        SetFocus (MainButton.mask[0].feldid);
        return ProcessMessages ();
}


void InitFirstInstance(HANDLE hInstance)
{
        WNDCLASS wc;

		HDC hdc;
		int col;

		col = 0;
		hdc = GetDC (NULL);
		col = GetDeviceCaps (hdc, BITSPIXEL);
		ReleaseDC (NULL, hdc);
		if (col < 16) ColBorder = FALSE;

        wc.style         =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                            | CS_CLASSDC;
        wc.lpfnWndProc   =  WndProc;
        wc.cbClsExtra    =  0;
        wc.cbWndExtra    =  0;
        wc.hInstance     =  hInstance;
        wc.hIcon         =  LoadIcon (hInstance, "FITICON");
        wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
        wc.lpszMenuName  =  "";
        wc.lpszClassName =  lpszClassName;

        RegisterClass(&wc);

        wc.style         =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                            | CS_OWNDC;
        wc.lpfnWndProc   =  FitLogoProc;
        wc.lpszMenuName  =  "";
        wc.hbrBackground =  GetStockObject (WHITE_BRUSH);
        wc.lpszClassName =  "Fitlogo";
        RegisterClass(&wc);

        wc.lpfnWndProc   =  UhrProc;
        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
        wc.lpszClassName =  "Uhr";
        RegisterClass(&wc);


        wc.lpfnWndProc   =  WndProc;
        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
        wc.lpszClassName =  "Backwind";

        RegisterClass(&wc);

        wc.hbrBackground =  CreateSolidBrush (RGB (0, 255, 255));
        wc.lpszClassName =  "LogoWind";
        RegisterClass(&wc);

        wc.lpfnWndProc   =  GebProc;
        wc.hbrBackground =  CreateSolidBrush (GebColor);
        wc.lpszClassName =  "GebWind";
        RegisterClass(&wc);

        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
        wc.lpszClassName =  "GebWindB";
        RegisterClass(&wc);

        wc.lpfnWndProc   =   MsgProc;
        wc.hbrBackground =   GetStockObject (WHITE_BRUSH);
        wc.lpszClassName =  "AufWiegWhite";
        RegisterClass(&wc);

        wc.lpfnWndProc   =   MsgProc;
        wc.hbrBackground =   CreateSolidBrush (RGB (0, 0, 255));
        wc.lpszClassName =  "AufWiegBlue";
        RegisterClass(&wc);

        wc.lpfnWndProc   =   PosLeerProc;
        wc.hbrBackground =   CreateSolidBrush (RGB (0, 255, 255));
        wc.lpszClassName =  "PosLeer";
        RegisterClass(&wc);

        wc.lpfnWndProc   =   PosLeerProc;
        wc.hbrBackground =   GetStockObject (WHITE_BRUSH);
        wc.lpszClassName =  "PosLeerKopf";
        RegisterClass(&wc);
}

void hKorrMainButton (double fcx, double fcy)
/**
Position von Mainbutton korrigieren.
**/
{
	    fittextlogo.FontHeight = (short) (double)
			           ((double) fittextlogo.FontHeight * fcy);
	    fittextlogo2.FontHeight = (short) (double)
			           ((double) fittextlogo2.FontHeight * fcy);
//	    fittextlogo3.FontHeight = (short) (double)
//			           ((double) fittextlogo3.FontHeight * fcy);
	    if (fittext.mask[0].pos[0] != -1)
		{
			     fittext.mask[0].pos[0] = (short) (double)
					 ((double) fittext.mask[0].pos[0] * fcy);
		}
		if (fittext.mask[0].pos[1] != -1)
		{
			     fittext.mask[0].pos[1] = (short) (double)
					 ((double) fittext.mask[0].pos[1] * fcx);

		}

	    if (fittext2.mask[0].pos[0] != -1)
		{
			     fittext2.mask[0].pos[0] = (short) (double)
					 ((double) fittext2.mask[0].pos[0] * fcy);
		}
		if (fittext2.mask[0].pos[1] != -1)
		{
			     fittext2.mask[0].pos[1] = (short) (double)
					 ((double) fittext2.mask[0].pos[1] * fcx);

		}

/*
	    if (fittext3.mask[0].pos[0] != -1)
		{
			     fittext3.mask[0].pos[0] = (short) (double)
					 ((double) fittext3.mask[0].pos[0] * fcy);
		}
		if (fittext3.mask[0].pos[1] != -1)
		{
			     fittext3.mask[0].pos[1] = (short) (double)
					 ((double) fittext3.mask[0].pos[1] * fcx);

		}
*/
}


void KorrMainButton (form * MainButton, double fcx, double fcy)
/**
Position von Mainbutton korrigieren.
**/
{
	    int i;

		for (i = 0; i < MainButton->fieldanz; i ++)
		{
			if (MainButton->mask[i].pos[0] != -1)
			{
			     MainButton->mask[i].pos[0] = (short) (double)
					 ((double) MainButton->mask[i].pos[0] * fcy);
			}
			if (MainButton->mask[i].pos[1] != -1)
			{
			     MainButton->mask[i].pos[1] = (short) (double)
					 ((double) MainButton->mask[i].pos[1] * fcx);

			}
	        MainButton->mask[i].rows = (short) (double)
			 ((double) MainButton->mask[i].rows * fcy);
	        MainButton->mask[i].length = (short) (double)
			 ((double) MainButton->mask[i].length * fcx);
		}
}

void KorrMainButton2 (form * MainButton, double fcx, double fcy)
/**
Position von Mainbutton korrigieren.
**/
{
	    int i;

		for (i = 0; i < MainButton->fieldanz; i ++)
		{
			if (MainButton->mask[i].pos[0] != -1)
			{
			     MainButton->mask[i].pos[0] = (short) (double)
					 ((double) MainButton->mask[i].pos[0] * fcy);
			}
			if (MainButton->mask[i].pos[1] != -1)
			{
			     MainButton->mask[i].pos[1] = (short) (double)
					 ((double) MainButton->mask[i].pos[1] * fcx);

			}
	        MainButton->mask[i].rows = (short) (double)
			 ((double) MainButton->mask[i].rows * fcy);
	        MainButton->mask[i].length = (short) (double)
			 ((double) MainButton->mask[i].length * fcx);
		}
}

// LOOK&Feel 18.11.2010 RoW

void InitLogo (void)
{
		RECT rect;


		GetClientRect (LogoWindow, &rect);
		if (LogoDynBackground && NewLookAndFeel)
		{
			BkBitmap.SetBmColor (LogoBackground);
			BkBitmap.SetWidth (rect.right); 
			BkBitmap.SetHeight (rect.bottom);
			BkBitmap.SetPlanes (32);
			HBITMAP bitmap = BkBitmap.Create (); 
			hbrBackground = CreatePatternBrush (bitmap);
            SetClassLong (LogoWindow, GCL_HBRBACKGROUND, (long) hbrBackground);

		}
}

void InitWeKopfBackground (void)
{
		RECT rect;


		GetClientRect (AufKopfWindow, &rect);
		if ((LogoDynBackground > 1) && NewLookAndFeel)
		{
//			BkBitmap.SetBmColor (LogoBackground);
			BkBitmap.SetBmColor (RGB (0, 100, 255));
			BkBitmap.SetWidth (rect.right); 
			BkBitmap.SetHeight (rect.bottom);
			BkBitmap.SetPlanes (32);
			HBITMAP bitmap = BkBitmap.Create (); 
			hbrBackground = CreatePatternBrush (bitmap);
            SetClassLong (AufKopfWindow, GCL_HBRBACKGROUND, (long) hbrBackground);

		}
}



BOOL InitNewInstance(HANDLE hInstance, int nCmdShow)
{
        RECT rect;
        RECT rect1;
        HCURSOR oldcursor;
		TEXTMETRIC tm;
		HDC hdc;
		double fcx, fcy;
        extern mfont ListFont;
		extern form lFussform;
		extern form EtiButton;
		extern form EtiSpezButton;
		extern form LgrSpezButton;
		extern form ZusatzButton;
        extern form fScanform;
        extern mfont lScanFont;

        hMainWindow = CreateWindow (lpszClassName,
                                    lpszMainTitle,
                                    WS_DLGFRAME | WS_CAPTION | WS_SYSMENU |
                                    WS_MINIMIZEBOX,
                                    0, 0,
                                    800, 580,
                                    NULL,
                                    NULL,
                                    hInstance,
                                    NULL);
        if (hMainWindow == 0) return FALSE;

        ShowWindow(hMainWindow, SW_SHOWMAXIMIZED);

        enterpass = 0;
        hdc = GetDC (hMainWindow);
		GetTextMetrics (hdc, &tm);
		ReleaseDC (hMainWindow, hdc);

		GetWindowRect (hMainWindow, &rect);
		rect.bottom -= (tm.tmHeight + tm.tmHeight / 2);
        ShowWindow(hMainWindow, nCmdShow);
        MoveWindow (hMainWindow, rect.left,
                                rect.top, rect.right, rect.bottom, TRUE);


        fcx = fcy = 1;
        int xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
        int yfull = GetSystemMetrics (SM_CYFULLSCREEN);        
        if (rect.right > 900 || rect.right < 700)
        {
		            fcx = (double) rect.right / 800;
 		            fcy = (double) rect.bottom / 562;
//		            fcx = (double) xfull / 800;
// 		            fcy = (double) yfull / 600;
                    hKorrMainButton (fcx, fcy);
		            KorrMainButton (&MainButton, fcx, fcy);
		            KorrMainButton2 (&MainButton2, fcx, fcy);
		            KorrMainButton2 (&lFussform, fcx, fcy);
		            KorrMainButton2 (&fScanform, fcx, fcy);
					ListFont.FontHeight = (int) (double)
						((double)  ListFont.FontHeight * fcy);
					ListFont.FontWidth = (int) (double)
						((double)  ListFont.FontWidth * fcx);
					EtiButton.mask[0].pos[1] = (int)
						(double) ((double) EtiButton.mask[0].pos[1] *
						         fcx);
					EtiButton.mask[1].pos[1] = (int)
						(double) ((double) EtiButton.mask[1].pos[1] *
						         fcx);
					EtiSpezButton.mask[0].pos[1] = (int)
						         (double) ((double) EtiSpezButton.mask[0].pos[1] *
						                    fcx);
					LgrSpezButton.mask[0].pos[1] = (int)
						         (double) ((double) LgrSpezButton.mask[0].pos[1] *
						                    fcx);
					ZusatzButton.mask[0].pos[1] = (int)
						         (double) ((double) ZusatzButton.mask[0].pos[1] *
						                    fcx);
					lScanFont.FontHeight = 320;
        }

		scrfx = fcx;
		scrfy = fcy;
        PasswScreenParams (scrfx, scrfy);
        hFitWindow = hMainWindow;
		NuBlock.MainInstance (hMainInst, hMainWindow);
		NuBlock.ScreenParam (scrfx, scrfy);
		Touchf.MainInstance (hMainInst, hMainWindow);
		Touchf.ScreenParam (scrfx, scrfy);
		Touchf.SetWriteParams (WriteParamKiste, WriteParamEZ,
			                   WriteParamEV, WriteParamArt);


//        ShowWindow(hMainWindow, nCmdShow);
//        UpdateWindow(hMainWindow);

        hFitWindow = hMainWindow;

        oldcursor = SetCursor (LoadCursor (NULL, IDC_WAIT));
        strcpy (menue.prog, "00000");
        strcpy (menue.prog, "00000");
        SetCursor (oldcursor);

        GetClientRect (hMainWindow,  &rect);

        Sebmpw = (int) (double) ((double) (fitbm.bmWidth + 40) *
                                           scrfx);
        BackWindow1  = CreateWindowEx (
                              WS_EX_CLIENTEDGE,
                              "Backwind",
                              "",
                              WS_CHILD | WS_VISIBLE,
                              10, 10,
/*
                              rect.right - (fitbm.bmWidth + 40),
                              rect.bottom - 100,
*/
                              rect.right - Sebmpw,
                              rect.bottom - 100,
                              hMainWindow,
                              NULL,
                              hInstance,
                              NULL);


        enterpass = 1;
		if (PassWord)
		{
            if (! EnterPasswTouch (hInstance, BackWindow1))
			{
                      enterpass = 0;
                      return FALSE;
			}
		}

        GetClientRect (BackWindow1,  &rect1);
        Sebmpw = (int) (double) ((double) (fitbm.bmWidth + 25) *
                                           scrfx);

        BackWindow2  = CreateWindowEx (
                              WS_EX_CLIENTEDGE,
                              "Backwind",
                              "",
                              WS_CHILD | WS_VISIBLE,
//                              rect.right - (fitbm.bmWidth + 25),
                              rect.right - Sebmpw,
                              10,
//                              fitbm.bmWidth + 15,
                              (int) (double) ((double) (fitbm.bmWidth + 15) * scrfx),
                              rect.bottom - 20,
                              hMainWindow,
                              NULL,
                              hInstance,
                              NULL);

        BackWindow3  = CreateWindowEx (
                              WS_EX_CLIENTEDGE,
                              "Backwind",
                              "",
                              WS_CHILD | WS_VISIBLE,
                              10,
                              rect.bottom - (int) ((double) 60 * scrfy),
//                              rect.bottom - 60,
//                              rect.right - (fitbm.bmWidth + 40),
                              rect.right - (int) (double) ((double) (fitbm.bmWidth + 40) *
                                           scrfx),
//                              rect.bottom - 20 - (rect.bottom - 70),
                              (int) ((double) (rect.bottom - 20 - (rect.bottom - 70)) * scrfy),
                              hMainWindow,
                              NULL,
                              hInstance,
                              NULL);


        LogoWindow  = CreateWindowEx (
                              WS_EX_CLIENTEDGE,
                              "LogoWind",
                              "",
                              WS_CHILD | WS_VISIBLE,
                              5, 5,
                              rect1.right - 10,
                              rect1.bottom - 10,
                              BackWindow1,
                              NULL,
                              hInstance,
                              NULL);
		InitLogo ();

        UpdateWindow (BackWindow1);
        UpdateWindow (BackWindow2);
        UpdateWindow (BackWindow3);
        UpdateWindow (LogoWindow);
		NuBlock.SetParent (LogoWindow);
        return TRUE;
}

int CreateFitWindow (void)
/**
FitWindow erzeugen.
**/
{

        FitWindow = CreateWindow   ("Fitlogo",
                                    "",
                                    WS_CHILD | WS_VISIBLE | WS_DLGFRAME,
                                    5, 5,
//                                    fitbm.bmWidth, fitbm.bmHeight,
                                    (int) (double) ((double) fitbm.bmWidth * scrfx),
									(int) (double) ((double) fitbm.bmHeight * scrfy),
                                    BackWindow2,
                                    NULL,
                                    hMainInst,
                                    NULL);
        fithdc = GetDC (FitWindow);
        if (IsFhs == 0)
        {
                    SetTimer (FitWindow , 1, twait, 0);
        }
        return TRUE;
}

int CreateUhrWindow (void)
/**
UhrWindow erzeugen.
**/
{
        RECT rect;
        int x,y, cx, cy;

        GetClientRect (LogoWindow,  &rect);
        cx = fitbm.bmWidth;
        cy = 13;
        x = 5;
        y = fitbm.bmHeight + 5;

        UhrWindow = CreateWindowEx (
                                    0,
                                    "Uhr",
                                    "",
                                    WS_CHILD | WS_VISIBLE,
                                    x, y,
                                    cx, cy,
                                    BackWindow2,
                                    NULL,
                                    hMainInst,
                                    NULL);
        SetTimer (UhrWindow , 1, twaitU, 0);
        return TRUE;
}


int IsHotKey (MSG *msg)
/**
HotKey Testen.
**/
{
         UCHAR taste;
         int i;
         ColButton *ColBut;
         char *pos;

         if (msg->message != WM_CHAR)
         {
                      return FALSE;
         }

         taste = (char) msg->wParam;
         for (i = 0; i < MainButton.fieldanz; i ++)
         {
                      ColBut = (ColButton *) MainButton.mask[i].feld;
                      if (ColBut->text1)
                      {
                           if ((pos = strchr (ColBut->text1, '&')) &&
                              ((UCHAR) toupper (*(pos + 1)) == toupper (taste)))
                           {
                                   SetFocus (MainButton.mask[i].feldid);
                                   AktButton = GetAktButton ();
                                   SendMessage (AktivWindow,WM_KEYDOWN,
                                                VK_RETURN, 0);
                                   return TRUE;
                           }
                      }
         }

         for (i = 0; i < MainButton2.fieldanz; i ++)
         {
                      ColBut = (ColButton *) MainButton2.mask[i].feld;
                      if (ColBut->text1)
                      {
                           if ((pos = strchr (ColBut->text1, '&')) &&
                              ((UCHAR) toupper (*(pos + 1)) == toupper (taste)))
                           {
                                   SetFocus (MainButton2.mask[i].feldid);
                                   AktButton = GetAktButton ();
                                   return TRUE;
                           }
                      }
         }
         return FALSE;
}

void PrevKey (void)
/**
Vorhergenden Button aktivieren.
**/
{

         if (AktButton >= MainButton.fieldanz) return;
         AktButton = GetAktButton ();
         if (AktButton == 0)
         {
                     AktButton = MainButton.fieldanz - 1;
         }
         else
         {
                     AktButton --;
         }
         if (AktButton < MainButton.fieldanz)
         {
               SetFocus (
                 MainButton.mask [AktButton].feldid);
         }
         else
         {
               SetFocus (
                 MainButton2.mask [AktButton - MainButton.fieldanz].feldid);
         }
}


void NextKey (void)
/**
Naechsten Button aktivieren.
**/
{
	     ColButton *Col;

         if (AktButton >= MainButton.fieldanz) return;

         AktButton = GetAktButton ();
         if (AktButton == MainButton.fieldanz - 1)
         {
                     AktButton = 0;
         }
         else
         {
                     AktButton ++;
         }

         if (AktButton < MainButton.fieldanz)
         {
 		       Col = (ColButton *) MainButton.mask[AktButton].feld;
               SetFocus (
                 MainButton.mask [AktButton].feldid);
         }
         else
         {
               SetFocus (
                 MainButton2.mask [AktButton - MainButton.fieldanz].feldid);
         }
}

void RightKey (void)
/**
Button nach rechts.
**/
{

         AktButton = GetAktButton ();
         if (AktButton <  MainButton.fieldanz)
         {
                     AktButton = MainButton.fieldanz +
                                 MainButton2.fieldanz - 5;
         }
         else if (AktButton == MainButton.fieldanz)
         {
                     AktButton = MainButton.fieldanz - 1;
         }
         else
         {
                     AktButton --;
         }
         if (AktButton < MainButton.fieldanz)
         {
               SetFocus (
                 MainButton.mask [AktButton].feldid);
         }
         else
         {
               SetFocus (
                 MainButton2.mask [AktButton - MainButton.fieldanz].feldid);
         }
}

void LeftKey (void)
/**
Button nach rechts.
**/
{

         AktButton = GetAktButton ();
         if (AktButton <  MainButton.fieldanz)
         {
                     AktButton = MainButton.fieldanz;
         }
         else if (AktButton == MainButton.fieldanz +
                               MainButton2.fieldanz - 5)
         {
                     AktButton = MainButton.fieldanz - 1;
         }
         else
         {
                     AktButton ++;
         }

         if (AktButton < MainButton.fieldanz)
         {
               SetFocus (
                 MainButton.mask [AktButton].feldid);
         }
         else
         {
               SetFocus (
                 MainButton2.mask [AktButton - MainButton.fieldanz].feldid);
         }
}


void ActionKey (void)
/**
Button-Aktion ausfuehren.
**/
{
         field *feld;

         AktButton = GetAktButton ();
         feld = GetAktField ();

         if (feld->after != (int (*) (void)) 0)
         {
                    (*feld->after) ();
         }
         else if (feld->BuId != 0)
         {
                    SendMessage (GetParent (feld->feldid), WM_COMMAND,
                                            MAKELONG (feld->BuId,
                                            feld->feldid),
                                            0l);
         }
}


int IsKeyPress (MSG *msg)
/**
HotKey Testen.
**/
{
         static char keypressed = 0;
         static HWND keyhwnd = 0;
         int taste;
         int i;
         ColButton *ColBut;
         char *pos;



         switch (msg->message)
         {
              case WM_KEYDOWN :
              {
                     if (keypressed)
                     {
                                  return FALSE;
                     }
                     taste = (int) msg->wParam;
                     if (taste == VK_UP)
                     {
                               PrevKey ();
                               return TRUE;
                     }
                     if (taste == VK_DOWN)
                     {
                               NextKey ();
                               return TRUE;
                     }
                     if (taste == VK_RIGHT)
                     {
                               RightKey ();
                               return TRUE;
                     }
                     if (taste == VK_LEFT)
                     {
                               LeftKey ();
                               return TRUE;
                     }
                     if (taste == VK_TAB)
                     {
                         if (GetKeyState (VK_SHIFT) < 0)
                         {
                              PrevKey ();
                              return TRUE;
                         }
                         NextKey ();
                         return TRUE;
                     }
                     if (taste == VK_RETURN)
                     {
                               ActionKey ();
                               return TRUE;
                     }
                     for (i = 0; i < MainButton.fieldanz; i ++)
                     {
                         ColBut = (ColButton *) MainButton.mask[i].feld;
                         if (ColBut->text1)
                         {
                              if ((pos = strchr (ColBut->text1, '&')) &&
                                 ((UCHAR) toupper (*(pos + 1)) == taste))
                              {
                                        keypressed = i + 1;
                                        keyhwnd = MainButton.mask[i].feldid;
                                        SendMessage (keyhwnd,
                                                WM_LBUTTONDOWN,
                                                0l,
                                                0l);
                                        return TRUE;
                              }
                         }
                    }

                    for (i = 0; i < MainButton2.fieldanz; i ++)
                    {
                         ColBut = (ColButton *) MainButton2.mask[i].feld;
                         if (ColBut->text1)
                         {
                              if ((pos = strchr (ColBut->text1, '&')) &&
                                 ((UCHAR) toupper (*(pos + 1)) == taste))
                              {
                                         keypressed = i + MainButton.fieldanz;
                                         keyhwnd = MainButton2.mask[i].feldid;
                                         SendMessage (keyhwnd,
                                                WM_LBUTTONDOWN,
                                                0l,
                                                0l);
                                         return TRUE;
                              }
                         }
                      }
              }
              case WM_KEYUP :
              {
                     if (!keypressed)
                     {
                                  return FALSE;
                     }
                     taste = (int) msg->wParam;
                     for (i = 0; i < MainButton.fieldanz; i ++)
                     {
                         ColBut = (ColButton *) MainButton.mask[i].feld;
                         if (ColBut->text1)
                         {
                             if ((pos = strchr (ColBut->text1, '&')) &&
                                ((UCHAR) toupper (*(pos + 1)) == taste))
                             {
                                   if (keyhwnd != MainButton.mask[i].feldid)
                                   {
                                                return FALSE;
                                   }
                                   keypressed = 0;
                                   keyhwnd = MainButton.mask[i].feldid;
                                   SendMessage (keyhwnd,
                                                WM_LBUTTONUP,
                                                0l,
                                                0l);
                                   keyhwnd = NULL;
                                   return TRUE;
                            }
                         }
                    }

                    for (i = 0; i < MainButton2.fieldanz; i ++)
                    {
                         ColBut = (ColButton *) MainButton2.mask[i].feld;
                         if (ColBut->text1)
                         {
                              if ((pos = strchr (ColBut->text1, '&')) &&
                                 ((UCHAR) toupper (*(pos + 1)) == taste))
                              {
                                   if (keyhwnd != MainButton2.mask[i].feldid)
                                   {
                                                return FALSE;
                                   }
                                   keypressed = 0;
                                   keyhwnd = MainButton2.mask[i].feldid;
                                   SendMessage (keyhwnd,
                                                WM_LBUTTONUP,
                                                0l,
                                                0l);
                                   keyhwnd = NULL;
                                   return TRUE;
                            }
                         }
                      }
             }
         }
         return FALSE;
}


int     ProcessMessages(void)
{
       MSG msg;

        while (GetMessage (&msg, NULL, 0, 0))
        {
              if (IsMouseMessage (&msg));
              else if (IsHotKey (&msg));
              else if (IsKeyPress (&msg));
              else
              {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
              }
        }
        DestroyFonts ();
	    SetForeground ();
		closedbase ();
        return msg.wParam;
}


LONG FAR PASCAL WndProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        PAINTSTRUCT ps;
		HDC hdc;

        switch(msg)
        {
              case WM_PAINT :
                    if (hWnd == BackWindow2)
                    {
                             if (FitWindow == 0)
                             {
                                        CreateFitWindow ();
                             }
                             display_form (BackWindow2,&MainButton, 0, 0);
                    }
                    else if (hWnd == BackWindow3)
                    {
                             if (MainButton2.mask[0].feldid == 0)
                             {
                                  display_form (BackWindow3,&MainButton2, 0, 0);
                             }
                    }
                    else if (hWnd == FitWindow)
                    {
                           hdc = BeginPaint (hWnd, &ps);
                           paintlogo (hdc, fitbmp, 0, 0, SRCCOPY);
                           EndPaint (hWnd, &ps);
                    }
// LOOK&Feel 18.11.2010 RoW
					else
					{
						hdc = BeginPaint (hWnd, &ps);
						DRAWTRANSPARENTS (hWnd, hdc);
						EndPaint (hWnd, &ps);
					}
                    break;
              case WM_KEYDOWN :
                         switch (wParam)
                         {
                              case VK_F5 :
                                      func5 ();
                                      return 0;

                         }
                         break;
              case WM_COMMAND :
                    if (LOWORD (wParam) == KEY5)
                    {
                            syskey = KEY5;
                            SendKey (VK_F5);
                            break;
                    }
                    else if (HIWORD (wParam) == LBN_SELCHANGE)
                    {
						    DrawNoteIcon ();
                    }
                    return MenuJob(hWnd,wParam,lParam);
              case WM_DESTROY :
                    if (hWnd == hMainWindow)
                    {
	                          SetForeground ();
	                          closedbase ();
                              ExitProcess (0);
                             return 0;
                    }
                    return 0;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

LONG MenuJob(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
        HMENU hMenu;

        hMenu = GetMenu (hWnd);

        switch (wParam)
        {
              case CM_HROT :
                     logorx = (logorx + 1) % 2;
                     if (logorx)
                     {
                               CheckMenuItem (hMenu, CM_HROT, MF_CHECKED);
                               CheckMenuItem (hMenu, CM_VROT, MF_UNCHECKED);
                               logory = 0;
                     }
                     else
                     {
                               CheckMenuItem (hMenu, CM_HROT, MF_UNCHECKED);
                     }
                     sign = signx = 1;
                     xstretch = ystretch = 0;
                     return 0;
              case CM_VROT :
                     logory = (logory + 1) % 2;
                     if (logory)
                     {
                               CheckMenuItem (hMenu, CM_VROT, MF_CHECKED);
                               CheckMenuItem (hMenu, CM_HROT, MF_UNCHECKED);
                               logorx = 0;
                     }
                     else
                     {
                               CheckMenuItem (hMenu, CM_VROT, MF_UNCHECKED);
                     }
                     sign = signx = 1;
                     xstretch = ystretch = 0;
                     return 0;
              case CM_PROGRAMMENDE :
                     DeleteObject (fitbmp);
                     DestroyWindow (hMainWindow);
                     KillTimer (FitWindow, 1);
                     ReleaseDC (FitWindow, fithdc);
                     PostQuitMessage(0);
                     return 0;
        }
        return (0);
}

LONG FAR PASCAL UhrProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        switch(msg)
        {
              case WM_TIMER :
                    switch (wParam)
                    {
                        case 1 :
                           sysdate (datum);
                           systime (zeit);
                           sprintf (datumzeit, "%s %s", datum, zeit);
                           InvalidateRect (UhrWindow, NULL, TRUE);
                           UpdateWindow (UhrWindow);
                           return 0;
                    }
                    break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

LONG FAR PASCAL FitLogoProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        PAINTSTRUCT ps;

        switch(msg)
        {
              case WM_TIMER :
                    switch (wParam)
                    {
                        case 1 :
                           printlogo (fitbmp, 0, 0, SRCCOPY);
                           return 0;
                    }
                    break;
              case WM_PAINT :
                    if (IsFhs)
                    {
                                fithdc = BeginPaint (hWnd, &ps);
                                strechbmp (fitbmp, 0, 0, SRCCOPY);
                                EndPaint (hWnd, &ps);
                    }
                    break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}


/** Ab hier Eingabe einer Nummer in einem extra Fenster mit
    numerischen Block fuer die Maus.
**/



static form *eForm = NULL;
static HWND ehWnd = NULL;
static int (*eFormProc) (WPARAM) = NULL;

static HWND eNumWindow;
static break_num_enter = 0;

mfont EditNumFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                                         RGB (255, 255, 255),
                                         1,
                                         NULL};

/*
mfont TextNumFont = {"Arial", 200, 0, 1, RGB (0, 0, 0),
                                         RGB (255, 255, 255),
                                         1,
                                         NULL};
*/

mfont TextNumFont = {"Arial", 200, 0, 1, BLACKCOL,
                                         LTGRAYCOL,
                                         1,
                                         NULL};

mfont numfieldfont = {"", 150, 0, 1,
                                       RGB (255, 255, 255),
                                       BLUECOL,
                                       1,
                                       NULL};

mfont ctrlfont = {"", 150, 0, 1,
                                       RGB (255, 255, 255),
                                       BLUECOL,
                                       1,
                                       NULL};

mfont stornofont = {"", 150, 0, 1,
                                       RGB (255, 0, 0),
                                       BLUECOL,
                                       1,
                                       NULL};

mfont OKfont = {"", 150, 0, 1,
                                       RGB (255, 255, 255),
                                       BLUECOL,
                                       1,
                                       NULL};

mfont Cafont = {"", 90, 0, 1,
                                       RGB (255, 255, 255),
                                       REDCOL,
                                       1,
                                       NULL};

ColButton Num1      =   {"1", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton Num2      =   {"2", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton Num3      =   {"3", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton Num4      =   {"4", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton Num5      =   {"5", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton Num6      =   {"6", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton Num7      =   {"7", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton Num8      =   {"8", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton Num9      =   {"9", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton Num0      =   {"0", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton NumP     =   {",", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                        14};

ColButton NumM     =   {"-", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};


ColButton NumLeft   =   {"", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         pfeill, -1, -1,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton NumRight   =   {"", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         pfeilr, -1, -1,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton NumDel     =   {"DEL", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         REDCOL,
                         14};

ColButton NumPlus   =   {"+", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton NumMinus   =   {"-", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton NumCls    =   {"C", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         REDCOL,
                         14};
ColButton NumAbbruch  =   {"Abbruch", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         REDCOL,
                         14};

ColButton NumOK       =   {"OK", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         BLACKCOL,
                         GREENCOL,
                         14};

ColButton BuStorno =   {"&Storno", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         REDCOL,
                         14};


static int nbsb  = 85;

static int nbsx   = 5;
int ny    = 10;
/*
static int nbss0 = 80;
static int nbss  = 64;
static int nbsz  = 40;
static int nbsz0 = 64;
*/
static int nbss0 = 64;
static int nbss  = 64;
static int nbsz  = 40;
static int nbsz0 = 64;
static int nAktButton = 0;

static int donum1 ();
static int donum2 ();
static int donum3 ();
static int donum4 ();
static int donum5 ();
static int donum6 ();
static int donum7 ();
static int donum8 ();
static int donum9 ();
static int donum0 ();
static int donump ();
static int donumm ();

static int doleft ();
static int doright ();
static int dodel ();
static int doabbruch ();
static int doOK ();
static int doplus ();
static int dominus ();
static int docls ();

static field _NumField[] = {
(char *) &Num1,          nbss0, nbsz0, ny, nbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum1, 0,
(char *) &Num2,          nbss0, nbsz0, ny, nbsx + 2 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum2, 0,
(char *) &Num3,          nbss0, nbsz0, ny, nbsx + 3 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum3, 0,
(char *) &Num4,          nbss0, nbsz0, ny + 1 * nbsz0, nbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum4, 0,
(char *) &Num5,          nbss0, nbsz0, ny + 1 * nbsz0, nbsx + 2 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum5, 0,
(char *) &Num6,          nbss0, nbsz0, ny + 1 * nbsz0, nbsx + 3 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum6, 0,
(char *) &Num7,          nbss0, nbsz0, ny + 2 * nbsz0, nbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum7, 0,
(char *) &Num8,          nbss0, nbsz0, ny + 2 * nbsz0, nbsx + 2 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum8, 0,
(char *) &Num9,          nbss0, nbsz0, ny + 2 * nbsz0, nbsx + 3 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum9, 0,
(char *) &Num0,          nbss0, nbsz0, ny + 2 * nbsz0, nbsx, 0, "",
                                                           COLBUTTON, 0,
                                                           donum0, 0,
(char *) &NumP,          nbss0, nbsz0, ny + 1 * nbsz0, nbsx, 0, "",
                                                           COLBUTTON, 0,
                                                           donump, 0,
(char *) &NumM,          nbss0, nbsz0, ny, nbsx, 0, "",
                                                           COLBUTTON, 0,
                                                           donumm, 0};

static form NumField = {12, 0, 0, _NumField, 0, 0, 0, 0, &numfieldfont};

/*
static int cbss0 = 80;
static int cbss  = 64;
static int cbsz  = 40;
static int cbsz0 = 64;
*/

static int cbss0 = 64;
static int cbss  = 64;
static int cbsz  = 40;
static int cbsz0 = 64;
static int cbsx = nbsx + 5 * nbss0;

static field _NumControl1[] = {
(char *) &NumLeft,       cbss0, cbsz0, ny, cbsx,             0, "",
                                                           COLBUTTON, 0,
                                                           doleft, 0,
(char *) &NumRight,       cbss0, cbsz0, ny, cbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           doright, 0,
(char *) &NumDel,         cbss0, cbsz0, ny, cbsx            , 0, "",
                                                           COLBUTTON, 0,
                                                           dodel, 0,
(char *) &NumAbbruch,     cbss0, cbsz0, ny, cbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           doabbruch, 0};

static form NumControl1 = {3, 0, 0, _NumControl1, 0, 0, 0, 0, &ctrlfont};

static field _NumControl2[] = {
(char *) &NumPlus,       cbss0, cbsz0, ny, cbsx,             0, "",
                                                           COLBUTTON, 0,
                                                           doplus, 0,
(char *) &NumMinus,       cbss0, cbsz0, ny, cbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           dominus, 0,
(char *) &NumCls,         cbss0, cbsz0, ny, cbsx            , 0, "",
                                                           COLBUTTON, 0,
                                                           docls, 0,
(char *) &NumAbbruch,     cbss0, cbsz0, ny, cbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           doabbruch, 0};

static form NumControl2 = {3, 0, 0, _NumControl2, 0, 0, 0, 0, &ctrlfont};

static form NumControl;


static field _CaControl[] = {
(char *) &NumAbbruch,     cbss0, cbsz0, ny, cbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           doabbruch, 0};
static form CaControl = {1, 0, 0, _CaControl, 0, 0, 0, 0, &Cafont};

static field _OKControl[] = {
(char *) &NumOK,         cbss0 * 2,
                                cbsz0, ny, cbsx,             0, "",
                                                           COLBUTTON, 0,
                                                             doOK, 0};
static form OKControl = {1, 0, 0, _OKControl, 0, 0, 0, 0, &OKfont};


static char editbuff [40];
static double editsum;
static char eaction = ' ';

static field _EditForm [] = {
editbuff,             0, 0, 0, 0, 0, "", EDIT, 0, 0, 0};

static form EditForm = {1, 0, 0, _EditForm, 0, 0, 0, 0, &EditNumFont};


static field _TextForm [] = {
editbuff,             0, 0, 0, 0, 0, "", READONLY, 0, 0, 0};

static form TextForm = {1, 0, 0, _TextForm, 0, 0, 0, 0, &TextNumFont};


static field _StornoForm [] = {
(char *) &BuStorno,       100, 64, 0, 0, 0, "", COLBUTTON, 0, 0, 0};

static form StornoForm = {1, 0, 0, _StornoForm, 0, 0, 0, 0, &stornofont};


void SetStorno (int (*sproc) (void), int BuId)
/**
Procedure fuer Storno Setzen.
**/
{
           StornoForm.mask[0].after = sproc;
           StornoForm.mask[0].BuId  = BuId;
}

int donum1 ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '1', 0l);
        return 1;
}

int donum2 ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '2', 0l);
        return 1;
}

int donum3 ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '3', 0l);
        return 1;
}

int donum4 ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '4', 0l);
        return 1;
}

int donum5 ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '5', 0l);
        return 1;
}

int donum6 ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '6', 0l);
        return 1;
}

int donum7 ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '7', 0l);
        return 1;
}

int donum8 ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '8', 0l);
        return 1;
}

int donum9 ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '9', 0l);
        return 1;
}

int donum0 ()
/**
Aktion bei Button ,
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '0', 0l);
        return 1;
}

int donump ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '.', 0l);
        return 1;
}

int donumm ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '-', 0l);
        return 1;
}


int doleft ()
/**
Aktion bei Button links
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_KEYDOWN,
                    (WPARAM) VK_LEFT, 0l);
        return 1;
}

int doright ()
/**
Aktion bei Button rechts
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_KEYDOWN,
                    (WPARAM) VK_RIGHT, 0l);
        return 1;
}

int dodel ()
/**
Aktion bei Button rechts
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_KEYDOWN,
                    (WPARAM) VK_DELETE, 0l);
        return 1;
}


int docls (void)
/**
EditFenster loeschen.
**/
{
        memset (editbuff, ' ', 39);
        editbuff [39] = (char) 0;
		editsum = (double) 0.0;
		strcpy (editbuff, "");
		CloseControls (&EditForm);
        create_enter_form (eNumWindow, &EditForm, 0, 0);
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
		return 1;
}

int doplus (void)
/**
EditFenster loeschen.
**/
{
	    GetWindowText (EditForm.mask[0].feldid, editbuff, 39);
		if (eaction == '+')
		{
		        editsum += ratod (editbuff);
		}
		else if (eaction == '-')
		{
		        editsum -= ratod (editbuff);
		}
		else
		{
		        editsum = ratod (editbuff);
		}
		eaction = '+';
	    sprintf (editbuff, "%lf", editsum);
		CloseControls (&EditForm);
        create_enter_form (eNumWindow, &EditForm, 0, 0);
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
		return 1;
}

int dominus (void)
/**
EditFenster loeschen.
**/
{
	    GetWindowText (EditForm.mask[0].feldid, editbuff, 39);
		if (eaction == '+')
		{
		        editsum += ratod (editbuff);
		}
		else if (eaction == '-')
		{
		        editsum -= ratod (editbuff);
		}
		else
		{
		        editsum = ratod (editbuff);
		}

		eaction = '-';
	    sprintf (editbuff, "%lf", editsum);
		CloseControls (&EditForm);
        create_enter_form (eNumWindow, &EditForm, 0, 0);
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
		return 1;
}

int dogleich (void)
/**
EditFenster loeschen.
**/
{
	    GetWindowText (EditForm.mask[0].feldid, editbuff, 39);
		if (eaction == '+')
		{
		        editsum += ratod (editbuff);
		}
		else if (eaction == '-')
		{
		        editsum -= ratod (editbuff);
		}
		else
		{
		        editsum = ratod (editbuff);
		}

		eaction = ' ';
	    sprintf (editbuff, "%lf", editsum);
		CloseControls (&EditForm);
        create_enter_form (eNumWindow, &EditForm, 0, 0);
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
		return 1;
}

int doabbruch ()
/**
Aktion bei Button F5
**/
{
        SetFocus (EditForm.mask[0].feldid);
        PostMessage (EditForm.mask[0].feldid, WM_KEYDOWN,
                    (WPARAM) VK_F5, 0l);
        return 1;
}

int doOK ()
/**
Aktion bei Button rechts
**/
{
	    if (CalcOn && eaction != ' ')
		{
			      dogleich ();
				  return 1;
		}
        SetFocus (EditForm.mask[0].feldid);
        PostMessage (EditForm.mask[0].feldid, WM_KEYDOWN,
                    (WPARAM) VK_RETURN, 0l);
        return 1;
}

void NumEnterBreak ()
/**
Endeflag fuer Erfassung setzen.
**/
{
        break_num_enter = 1;
}

HWND IsNumChild (POINT *mousepos)
/**
Test, ob sich der Maus-Cursor in einem KindFensert befinden.
**/
{
        int i;

        for (i = 0; i < NumField.fieldanz; i ++)
        {
                   if (MouseinWindow (NumField.mask[i].feldid,
                                      mousepos))
                   {
                                 return (NumField.mask[i].feldid);
                   }
        }

        for (i = 0; i < NumControl.fieldanz; i ++)
        {
                   if (MouseinWindow (NumControl.mask[i].feldid,
                                      mousepos))
                   {
                                 return (NumControl.mask[i].feldid);
                   }
        }

        for (i = 0; i < OKControl.fieldanz; i ++)
        {
                   if (MouseinWindow (OKControl.mask[i].feldid,
                                      mousepos))
                   {
                                 return (OKControl.mask[i].feldid);
                   }
        }
        for (i = 0; i < CaControl.fieldanz; i ++)
        {
                   if (MouseinWindow (CaControl.mask[i].feldid,
                                      mousepos))
                   {
                                 return (CaControl.mask[i].feldid);
                   }
        }

        for (i = 0; i < EditForm.fieldanz; i ++)
        {
                   if (MouseinWindow (EditForm.mask[i].feldid,
                                      mousepos))
                   {
                                 return (EditForm.mask[i].feldid);
                   }
        }
        if (StornoForm.mask[0].feldid == 0)
        {
                   return FALSE;
        }

        for (i = 0; i < StornoForm.fieldanz; i ++)
        {
                   if (MouseinWindow (StornoForm.mask[i].feldid,
                                      mousepos))
                   {
                                 return (StornoForm.mask[i].feldid);
                   }
        }
        return NULL;
}


LONG FAR PASCAL EntNumProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        POINT mousepos;
        HWND enchild;
        static int SendChild = 0;
        int HasFocus = 1;

        GetCursorPos (&mousepos);

        switch(msg)
        {
              case WM_PAINT :
/*
                    display_form (eNumWindow, &NumField, 0, 0);
                    display_form (eNumWindow, &NumControl, 0, 0);
                    display_form (eNumWindow, &OKControl, 0, 0);
                    display_form (eNumWindow, &TextForm, 0, 0);
                    display_form (eNumWindow, &EditForm, 0, 0);
                    display_form (eNumWindow, &CaControl, 0, 0);
*/

				    if (textblock)
					{
						textblock->OnPaint (hWnd, msg, wParam, lParam);
					}
					else
					{
						display_form (eNumWindow, &NumField, 0, 0);
						display_form (eNumWindow, &NumControl, 0, 0);
						display_form (eNumWindow, &OKControl, 0, 0);
						display_form (eNumWindow, &TextForm, 0, 0);
						display_form (eNumWindow, &EditForm, 0, 0);
						display_form (eNumWindow, &CaControl, 0, 0);

						if (StornoForm.mask[0].after)
						{
                              display_form (eNumWindow, &StornoForm, 0, 0);
						}	
						else if (StornoForm.mask[0].BuId)
						{
                              display_form (eNumWindow, &StornoForm, 0, 0);
						}

						SetFocus (EditForm.mask[0].feldid);
					}
					break;
/*
                        display_form (eNumWindow, &NumField, 0, 0);
                        display_form (eNumWindow, &NumControl, 0, 0);
                        display_form (eNumWindow, &OKControl, 0, 0);
					    if (eForm == NULL)
						{
                             display_form (eNumWindow, &TextForm, 0, 0);
                             display_form (eNumWindow, &EditForm, 0, 0);
					         SendMessage (EditForm.mask[0].feldid, EM_SETSEL,
                                                 (WPARAM) 0, MAKELONG (-1, 0));
						}
					    else
						{
                             display_form (eNumWindow, &fAuswahlEinh, 0, 0);
						}

                        display_form (eNumWindow, &CaControl, 0, 0);


                        if (StornoForm.mask[0].after)
						{
                              display_form (eNumWindow, &StornoForm, 0, 0);
						}
                        else if (StornoForm.mask[0].BuId)
						{
                              display_form (eNumWindow, &StornoForm, 0, 0);
						}

                        if (ChoiseForm.mask[0].after)
						{
                              display_form (eNumWindow, &ChoiseForm, 0, 0);
						}
                        else if (ChoiseForm.mask[0].BuId)
						{
                              display_form (eNumWindow, &ChoiseForm, 0, 0);
						}

					    if (eForm == NULL)
						{
                               SetFocus (EditForm.mask[0].feldid);
						}
					}

                    break;
*/


              case WM_LBUTTONDOWN :
              case WM_LBUTTONUP :
              case WM_MOUSEMOVE :
				    if (textblock)
					{
						textblock->OnButton (hWnd, msg, wParam, lParam);
					}
					else
					{
						enchild = IsNumChild (&mousepos);
//                    MouseTest (enchild, msg);
						if (enchild)
						{
                              SendMessage (enchild, msg, wParam, lParam);
                              return TRUE;
						}
                    }
                    return TRUE;
              case WM_ACTIVATE :
                    switch (wParam)
                    {
                         case WA_ACTIVE :
                         case WA_CLICKACTIVE :
//                              SetCapture (eNumWindow);
                              break;
                    }
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

LONG FAR PASCAL EntNuProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{

        switch(msg)
        {
              case WM_PAINT :
				    NuBlock.OnPaint (hWnd, msg, wParam, lParam);
					break;

              case WM_LBUTTONDOWN :
              case WM_LBUTTONUP :
              case WM_MOUSEMOVE :
				    NuBlock.OnButton (hWnd, msg, wParam, lParam);
                    return TRUE;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}


LONG FAR PASCAL TouchfProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{

        switch(msg)
        {
		      case WM_DIRECT :
				    Touchf.StartDirect ();
				    return 0;
              case WM_PAINT :
/*
				    if (hWnd == NuBlock.eNumWindow)
					{
						NuBlock.OnPaint (hWnd, msg, wParam, lParam);
					}
					else
*/
					{
						Touchf.OnPaint (hWnd, msg, wParam, lParam);
					}
					break;

              case WM_LBUTTONDOWN :
              case WM_LBUTTONUP :
              case WM_MOUSEMOVE :
				    Touchf.OnButton (hWnd, msg, wParam, lParam);
                    return TRUE;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}


void RegisterNumEnter (void)
/**
Fenster fuer numerische Eingabe registrieren
**/
{
        static int registered = 0;
        WNDCLASS wc;

        if (registered == 0)
        {
                   wc.style =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                               | CS_OWNDC;
                   wc.cbClsExtra    =  0;
                   wc.cbWndExtra    =  0;
                   wc.hInstance     =  hMainInst;
                   wc.hIcon         =  LoadIcon (hMainInst, "FITICON");
                   wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                   wc.lpszMenuName  =  "";
                   wc.lpfnWndProc   =  EntNumProc;
                   wc.lpszMenuName  =  "";
                   wc.hbrBackground =  CreateSolidBrush (RGB (0, 0, 120));
                   wc.lpszClassName =  "EnterNum";
                   RegisterClass(&wc);
                   registered = 1;
        }
}

int IsNumKey (MSG *msg)
/**
HotKey Testen.
**/
{
         static char keypressed = 0;
         static HWND keyhwnd = 0;
         ColButton *ColBut;
         int taste;
         char *pos;

         switch (msg->message)
         {
	          case EM_SETSEL :
				  break;
              case WM_KEYDOWN :
              {
                     switch (msg->wParam)
                     {
                            case VK_F5 :
                                    NumEnterBreak ();
                                    syskey = KEY5;
                                    return TRUE;
                            case VK_DELETE :
								     docls ();
									 return TRUE;
                            case VK_RETURN :
								    if (CalcOn && eaction != ' ')
									{
										dogleich ();
										return TRUE;
									}
                                    NumEnterBreak ();
                                    GetWindowText (EditForm.mask[0].feldid,
                                                   EditForm.mask [0].feld,
                                                   EditForm.mask [0].length);
                                    syskey = KEYCR;
                                    return TRUE;
                     }
                     taste = (int) msg->wParam;
                     ColBut = (ColButton *) StornoForm.mask[0].feld;
                     if (ColBut->text1)
                     {
                           if ((pos = strchr (ColBut->text1, '&')) &&
                               ((UCHAR) toupper (*(pos + 1)) == taste))
                           {
                                        keypressed = 1;
                                        keyhwnd = StornoForm.mask[0].feldid;
                                        SendMessage (keyhwnd,
                                                WM_LBUTTONDOWN,
                                                0l,
                                                0l);
                                        return TRUE;
                            }
                     }
                     return FALSE;
              }
              case WM_KEYUP :
              {
                         taste = (int) msg->wParam;
                         ColBut = (ColButton *) StornoForm.mask[0].feld;
                         if (ColBut->text1)
                         {
                             if ((pos = strchr (ColBut->text1, '&')) &&
                                ((UCHAR) toupper (*(pos + 1)) == taste))
                             {
                                   if (keyhwnd != StornoForm.mask[0].feldid)
                                   {
                                                return FALSE;
                                   }
                                   keypressed = 0;
                                   keyhwnd = StornoForm.mask[0].feldid;
                                   SendMessage (keyhwnd,
                                                WM_LBUTTONUP,
                                                0l,
                                                0l);
                                   keyhwnd = NULL;
                                   return TRUE;
                            }
                         }
                         return FALSE;
              }
              case WM_CHAR :
              {
				     switch (msg->wParam)
					 {
					       case '+' :
								     doplus ();
									 return TRUE;
						   case '-' :
								     dominus ();
									 return TRUE;
					 }
                     SendMessage (EditForm.mask[0].feldid,
                                   msg->message,
                                   msg->wParam,
                                   msg->lParam);
                                   return TRUE;
               }
          }

          return FALSE;
}


void EnterNumBox (HWND hWnd, char *text, char *nummer, int nlen, char *pic)
/**
Fenster fuer numerische Eingabe oeffnen.
**/
{
        RECT rect;
        MSG msg;
        TEXTMETRIC tm;
        HFONT hFont;
        HDC hdc;
        int x, y, cx, cy;
        int i, j;
        int tlen;
        int elen, epos;
        int eheight;
        HCURSOR oldcursor;
        static int BitMapOK = 0;
        HWND shListBox;
        HWND slbox;
        struct LMENUE smenue;
		static BOOL NumKorr = FALSE;

		if (NumKorr == FALSE)
		{
			      NumKorr = TRUE;
				  nbsz0 = (int) (double) ((double) nbsz0 * scrfy);
				  nbss0 = (int) (double) ((double) nbss0 * scrfx);
		          KorrMainButton (&NumField, scrfx, scrfy);
		          KorrMainButton (&NumControl1, scrfx, scrfy);
		          KorrMainButton (&NumControl2, scrfx, scrfy);
		          KorrMainButton (&StornoForm, scrfx, scrfy);
		          KorrMainButton (&OKControl, scrfx, scrfy);
		          KorrMainButton (&CaControl, scrfx, scrfy);
				  ctrlfont.FontHeight = (int) (double)
					  ((double) ctrlfont.FontHeight * scrfy);
				  ctrlfont.FontWidth = (int) (double)
					  ((double) ctrlfont.FontWidth * scrfx);
		}

		if (CalcOn)
		{
			memcpy (&NumControl, &NumControl2, sizeof (form));
			editsum = (double) 0.0;
			eaction = ' ';
		}
		else
		{
			memcpy (&NumControl, &NumControl1, sizeof (form));
		}

        GetMenue (&smenue);
        MenTextSave (&smenue);
        shListBox = GethListBox ();
        slbox = Getlbox ();
        tlen = strlen (text);

        if (BitMapOK == 0)
        {
                 NumLeft.bmp = LoadBitmap (hMainInst, "pfeill");
                 NumRight.bmp = LoadBitmap (hMainInst, "pfeilr");
                 NumPlus.bmp  = LoadBitmap (hMainInst, "plus");
                 NumMinus.bmp = LoadBitmap (hMainInst, "minus");
                 NumCls.bmp = LoadBitmap (hMainInst, "clear");
                 BitMapOK = 1;
        }

        RegisterNumEnter ();
        GetWindowRect (LogoWindow, &rect);
        x = rect.left;
        y = (int) (double) ((double) rect.bottom - (3 * nbsz0) - (46 * scrfy));
        cx = rect.right - rect.left;
        cy = rect.bottom - y;

        ny = (int) (double) ((double) cy - (3 * nbsz0) - (30 * scrfy));
        for (i = 0,j = 0; j < NumField.fieldanz - 3; i ++,j += 3)
        {
                     NumField.mask[j].pos[0] = ny + i * nbsz0;
                     NumField.mask[j + 1].pos[0] = ny + i * nbsz0;
                     NumField.mask[j + 2].pos[0] = ny + i * nbsz0;
        }
        NumField.mask[j].pos[0] = ny + 2 * nbsz0;
        NumField.mask[j + 1].pos[0] = ny + 1 * nbsz0;
        NumField.mask[j + 2].pos[0] = ny;


        OKControl.mask[0].pos[0]  = ny;

        NumControl.mask[0].pos[0] = ny + 1 * nbsz0;
        NumControl.mask[1].pos[0] = ny + 1 * nbsz0;
        NumControl.mask[2].pos[0] = ny + 2 * nbsz0;
        NumControl.mask[3].pos[0] = ny + 2 * nbsz0;
        CaControl.mask[0].pos[0]  = ny + 2 * nbsz0;

        cbsx = cx - 2 * nbss0 - 5;
        OKControl.mask[0].pos[1]  = cbsx;

        NumControl.mask[0].pos[1] = cbsx;
        NumControl.mask[1].pos[1] = cbsx + nbss0;
        NumControl.mask[2].pos[1] = cbsx;
        NumControl.mask[3].pos[1] = cbsx + nbss0;
        CaControl.mask[0].pos[1] = cbsx + nbss0;

        epos = NumControl.mask[0].pos[1] - (NumField.mask[2].pos[1] + nbss0);
        EditNumFont.FontHeight = 200;
        while (TRUE)
        {
                 spezfont (&EditNumFont);
                 hFont = SetWindowFont (hMainWindow);
                 hdc = GetDC (hMainWindow);
                 SelectObject (hdc, hFont);
                 GetTextMetrics (hdc, &tm);
                 DeleteObject (hFont);
                 ReleaseDC (hMainWindow, hdc);

                 elen = tm.tmAveCharWidth * nlen;
                 if (elen < epos) break;
                 EditNumFont.FontHeight -= 20;
                 if (EditNumFont.FontHeight <= 80) break;
        }

        TextNumFont.FontHeight = 200;

        epos = max (0, (epos - elen) / 2);
        epos = NumField.mask[2].pos[1] + nbss0 + epos;
        eheight = tm.tmHeight;
        eheight += eheight / 3;
        EditForm.mask[0].length = elen;
        EditForm.mask[0].rows   = eheight;
        EditForm.mask[0].pos[1] = epos;
        EditForm.mask[0].pos[0] = ny;

        epos = NumControl.mask[0].pos[1] - (NumField.mask[2].pos[1] + nbss0);
        elen = StornoForm.mask[0].length;
        epos = max (0, (epos - elen) / 2);
        epos = NumField.mask[2].pos[1] + nbss0 + epos;
        StornoForm.mask[0].pos[1] = epos;
        StornoForm.mask[0].pos[0] = cy - 30 - StornoForm.mask[0].rows;

        if (pic)
        {
                       EditForm.mask[0].picture = pic;
        }

        spezfont (&TextNumFont);
        hFont = SetWindowFont (hMainWindow);
        hdc = GetDC (hMainWindow);
        SelectObject (hdc, hFont);
        GetTextMetrics (hdc, &tm);
        DeleteObject (hFont);
        ReleaseDC (hMainWindow, hdc);

        elen = tm.tmAveCharWidth * strlen (text);
        epos = NumControl.mask[0].pos[1] - (NumField.mask[2].pos[1] + nbss0);
        epos = max (0, (epos - elen) / 2);
        epos = NumField.mask[2].pos[1] + nbss0 + epos;
        eheight = tm.tmHeight;
        TextForm.mask[0].length = elen;
        TextForm.mask[0].rows   = eheight;
        TextForm.mask[0].pos[1] = epos;
        TextForm.mask[0].pos[0] = (short)
                            EditForm.mask[0].pos[0] + 2 * (short) tm.tmHeight;
        TextForm.mask[0].feld = text;

        while (TRUE)
        {
                 spezfont (&TextNumFont);
                 hFont = SetWindowFont (hMainWindow);
                 hdc = GetDC (hMainWindow);
                 SelectObject (hdc, hFont);
                 GetTextMetrics (hdc, &tm);
                 DeleteObject (hFont);
                 ReleaseDC (hMainWindow, hdc);

                 elen = tm.tmAveCharWidth * tlen;
                 if (elen < epos) break;
                 TextNumFont.FontHeight -= 20;
                 if (TextNumFont.FontHeight <= 80) break;
        }

        spezfont (&TextNumFont);
        hFont = SetWindowFont (hMainWindow);
        hdc = GetDC (hMainWindow);
        SelectObject (hdc, hFont);
        GetTextMetrics (hdc, &tm);
        DeleteObject (hFont);
        ReleaseDC (hMainWindow, hdc);
        elen = tm.tmAveCharWidth * strlen (text);

        TextNumFont.FontHeight -= 20;

        spezfont (&TextNumFont);
        hFont = SetWindowFont (hMainWindow);
        hdc = GetDC (hMainWindow);
        SelectObject (hdc, hFont);
        GetTextMetrics (hdc, &tm);
        DeleteObject (hFont);
        ReleaseDC (hMainWindow, hdc);
        eheight = tm.tmHeight + tm.tmHeight / 3;
        epos = NumControl.mask[0].pos[1] - (NumField.mask[2].pos[1] + nbss0);
        epos = max (0, (epos - elen) / 2);
        epos = NumField.mask[2].pos[1] + nbss0 + epos;
        TextForm.mask[0].length = elen;
        TextForm.mask[0].rows   = eheight;
        TextForm.mask[0].pos[1] = epos;

		if (EditForm.font->hFont)
		{
			     DeleteObject (EditForm.font->hFont);
		         EditForm.font->hFont = NULL;
		}
		if (TextForm.font->hFont)
		{
		        DeleteObject (TextForm.font->hFont);
		        TextForm.font->hFont = NULL;
		}

        memset (editbuff, ' ', 39);
        editbuff [39] = (char) 0;
        memcpy (editbuff, nummer, strlen (nummer));

        eNumWindow  = CreateWindowEx (
                              0,
                              "EnterNum",
                              "",
                              WS_POPUP | WS_CAPTION,
                              x, y,
                              cx, cy,
                              // hMainWindow,
                              hWnd,
                              NULL,
                              hMainInst,
                              NULL);

        create_enter_form (eNumWindow, &EditForm, 0, 0);
        ShowWindow (eNumWindow, SW_SHOW);
        UpdateWindow (eNumWindow);
        SetFocus (EditForm.mask[0].feldid);

        EnableWindows (hWnd, FALSE);
        oldcursor = SetCursor (LoadCursor (NULL, IDC_ARROW));

        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));

        NumEnterWindow = EditForm.mask[0].feldid;
//        SetCapture (eNumWindow);

        NumEnterAktiv = 1;
        break_num_enter = 0;
        while (GetMessage (&msg, NULL, 0, 0))
        {
              if (IsNumKey (&msg));
              else
              {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
              }
              if (break_num_enter) break;
        }
        display_form (eNumWindow, &EditForm, 0, 0);
        SetActiveWindow (hMainWindow);
        EnableWindows (hWnd, TRUE);
        CloseControls (&NumField);
        CloseControls (&NumControl);
        CloseControls (&OKControl);
        CloseControls (&CaControl);
        CloseControls (&EditForm);
        CloseControls (&TextForm);
        if (StornoForm.mask[0].after)
        {
                    CloseControls (&StornoForm);
                    StornoForm.mask[0].after = NULL;
                    StornoForm.mask[0].BuId = 0;
        }
        else if (StornoForm.mask[0].BuId)
        {
                    CloseControls (&StornoForm);
                    StornoForm.mask[0].after = NULL;
                    StornoForm.mask[0].BuId = 0;
        }
//        ReleaseCapture ();
        DestroyWindow (eNumWindow);
		eNumWindow = NULL;
        NumEnterAktiv = 0;
        strcpy (nummer, clipped (editbuff));
        SetCursor (oldcursor);

        SetMenue (&smenue);
        SethListBox (shListBox);
        Setlbox (slbox);
}

void EnterCalcBox (HWND hWnd, char *text, char *nummer, int nlen, char *pic)
/**
Numbox als rechner aufrufen.
**/
{
	     CalcOn = TRUE;
         EnterNumBox (hWnd, text, nummer, nlen, pic);
		 CalcOn = FALSE;
}


/**

  Listenerfassung

**/

// static int lfbsz0 = 36;
static int lfbszs = 45;
static int lfbsz0 = 42;
static int lfbss0 = 73;

static int SetLAN (void);
static int doinsert (void);
static int domess (void);
static int dodelete (void);
static int Auszeichnen ();
static int HbkInput ();
static int PreisInput ();
static int Nachliefern ();
static int Nichtliefern ();
static int SysChoise ();
static int SwitchwKopf ();
void arrtowepos (int);
void LeseWePos (void) ;
BOOL LeseLspUpd (void);
static int CheckHbk ();

static int sorta (void);
static int sortb (void);
static int sortame (void);
static int sortlme (void);
static int sorts (void);


mfont ListFont = {"Courier New", 200, 150, 1, RGB (0, 0, 0),
                                      RGB (255, 255, 255),
                                      0,
                                      NULL};
mfont InsFontT = {"Courier New", 110, 0, 1, BLACKCOL,
                                            LTGRAYCOL,
                                            1,
                                            NULL};

mfont InsFont = {"Courier New", 150, 0, 1, BLACKCOL,
                                           LTGRAYCOL,
                                           1,
                                           NULL};

mfont lKopfFont = {"Courier New", 150, 0, 1, BLACKCOL,
                                             LTGRAYCOL,
                                             1,
                                             NULL};

mfont lFussFont = {"", 90, 0, 1,       RGB (0, 255, 255),
                                       BLUECOL,
                                       1,
                                       NULL};

ColButton BuInsert = {"Neu", -1, 5,
                       "F6", -1, 21,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       RGB (0, 255, 255),
                       BLUECOL,
                       FALSE};


ColButton BuPreise = { "Preise", -1, 5,
                      "F7", -1, 21,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       RGB (0, 255, 255),
                       BLUECOL,
                       FALSE};


ColButton BuDel   = {"l�schen", -1, 5,
                      "F9", -1, 21,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      RGB (0, 255, 255),
                      BLUECOL,
                      FALSE};

ColButton BuSys =   {"Ger�t", -1, 5,
                      "F11", -1, 21,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      RGB (0, 255, 255),
                      BLUECOL,
                      FALSE};

ColButton BuQsPos =   {"QS Pos", -1, 5,
                       "F2", -1, 21,
                        NULL, 0, 0,
                        NULL, 0, 0,
                        NULL, 0, 0,
                        RGB (0, 255, 255),
                        BLUECOL,
                        TRUE};

ColButton BuMess =   {"Messwerte", -1, 5,
                       "F3", -1, 21,
                        NULL, 0, 0,
                        NULL, 0, 0,
                        NULL, 0, 0,
                        RGB (0, 255, 255),
                        BLUECOL,
                        TRUE};
char *Wiegekopf = "Wiegekopf";
char *Scann     = "Scannen";


ColButton BuKopf =   {Wiegekopf, -1, 5,
                      "F10", -1, 21,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      RGB (0, 255, 255),
                      BLUECOL,
                      FALSE};

ColButton BuAusZ = {"HBK", -1, 5,
                     "F8", -1, 21,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
//                     BLACKCOL,
//                     YELLOWCOL,
                      RGB (0, 255, 255),
                      BLUECOL,
                     -1};

void SetScannMode ()
{
	if (ScannMode)
	{
		BuSys.text1 = Scann;
	}
	else
	{
//		BuKopf.text1 = Wiegekopf;
		BuSys.text1 = "Ger�t";
	}
}

static field _lKopfform [] =
{
        "Auftrag",      0, 0, -1, 10, 0, "",     DISPLAYONLY, 0, 0, 0,
        auftrag_nr,     0, 0, -1, 100, 0, "%8d", DISPLAYONLY, 0, 0, 0,
		"LAN",          0, 0, -1, 120, 0, "",    DISPLAYONLY, 0, 0, 0,
		LAN,            0, 0, -1, 150, 0, "",    DISPLAYONLY, 0, 0, 0,
};

static form lKopfform = {4, 0, 0, _lKopfform, 0, 0, 0, 0, &lKopfFont};

field _lFussform[] = {
(char *) &BuPreise,      lfbss0, lfbsz0,-1, 10 + 0 * (15 + lfbss0),   0, "", COLBUTTON, 0,
                                                           PreisInput, 0,
(char *) &BuInsert,      lfbss0, lfbsz0,-1, 10 + 4* (15 + lfbss0),     0, "", COLBUTTON, 0,
                                                           doinsert,  0,
(char *) &BuAusZ,        lfbss0, lfbsz0,-1, 10 + 1 * (15 + lfbss0),    0, "", COLBUTTON, 0,
                                                           HbkInput, 0,
(char *) &BuDel,         lfbss0, lfbsz0,-1, 10 + 2 * (15 + lfbss0),   0, "", COLBUTTON, 0,
                                                           dodelete, 0,
(char *) &BuMess,        lfbss0, lfbsz0,-1, 10 + 3 * (15 + lfbss0),   0, "", COLBUTTON, 0,
                                                           domess, 0,
(char *) &BuQsPos,       lfbss0, lfbsz0,-1, 10 + 5 * (15 + lfbss0),   0, "", COLBUTTON, 0,
                                                           doQsPos, 0,
(char *) &BuSys,         lfbss0, lfbsz0,-1, 40 + 6 * (15 + lfbss0),   0, "", COLBUTTON, 0,
                                                           SysChoise, 0,
(char *) &BuKopf,        lfbss0, lfbsz0,-1, 40 + 7 * (15 + lfbss0),   0, "", COLBUTTON, 0,
                                                           SwitchwKopf, 0,
};

form lFussform = {8, 0, 0, _lFussform, 0, 0, 0, 0, &lFussFont};


mfont lScanFont = {"", 270, 0, 0,       RGB (0, 255, 255),
                                        BLUECOL,
                                        1,
                                        NULL};



extern int lfbss0;
extern int lfbszs;


extern int BreakEanEnter ();
char eanbuffer [256] = {""};

field _fScanform[] = {
eanbuffer,   6 * lfbss0, lfbszs, -1, 10 + 0 * (15 + lfbss0),      0, "255", EDIT | SCROLL, 0, BreakEanEnter, 0,
};         

form fScanform = {1, 0, 0, _fScanform, 0, 0, 0, 0, &lScanFont};

static field _insform [] =
{
        aufps.a,            12, 0, 1, 1, 0, "%11.0f", READONLY, 0, 0, 0,
        aufps.a_bz1,        20, 0, 1, 14,0, "",       READONLY, 0, 0, 0,
        aufps.ls_vk_euro,   10, 0, 1, 36, 0, "%9.3f", READONLY, 0, 0, 0,
        aufps.ls_lad_euro,  10, 0, 1, 47, 0, "%9.3f", READONLY , 0, 0, 0,
};

static form insform = {4, 0, 0, _insform, 0, 0, 0, 0, &InsFont};

static field _insub [] = {
"Artikel-Nr",            0, 0, 0,  1, 0, "", DISPLAYONLY, 0, 0, 0,
"Bezeichnung",           0, 0, 0, 14, 0, "", DISPLAYONLY, 0, 0, 0,
"EK-Preis",              0, 0, 0, 36, 0, "", DISPLAYONLY, 0, 0, 0,
"VK-Preis",              0, 0, 0, 47, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form insub = { 4, 0, 0,_insub, 0, 0, 0, 0, &InsFontT};


static field _testform [] =
{
        aufps.a,           11, 1, 0, 0, 0, "%11.0f", EDIT, 0, 0, 0,
        aufps.a_bz1,       20, 1, 0, 13,0, "",       READONLY, 0, 0, 0,
//        aufps.auf_me,       9, 1, 0, 35, 0, "%9.3f", EDIT, 0, 0, 0,
        aufps.auf_me,       6, 1, 0, 35, 0, "%6.0f", EDIT, 0, 0, 0,
		aufps.auf_me_bz,    2, 1, 0, 42, 0, "",      READONLY, 0, 0, 0,
        aufps.lief_me,      9, 1, 0, 46, 0, "%9.3f", READONLY , 0, 0, 0,
        aufps.s,            2, 1, 0, 57, 0, "",      READONLY, 0, 0, 0,
};


static form testform = { 6, 0, 4,_testform, 0, CheckHbk, 0, 0, &ListFont};

/*
static field _testub [] = {
"Artikel-Nr",           11, 1, 0, 0, 0, "", DISPLAYONLY, 0, 0, 0,
"Bezeichnung",          20, 1, 0,11, 0, "", DISPLAYONLY, 0, 0, 0,
"Auf-Me",                9, 1, 0,35, 0, "", DISPLAYONLY, 0, 0, 0,
"Lief-Me",               9, 1, 0,46, 0, "", DISPLAYONLY, 0, 0, 0,
"S",                     1, 1, 0,57, 0, "", DISPLAYONLY, 0, 0, 0
};
*/

static field _testub [] = {
"Artikel-Nr",           12, 1, 0, 0, 0, "", BUTTON, 0, sorta,  106,
"Bezeichnung",          22, 1, 0,12, 0, "", BUTTON, 0, sortb,  102,
"Best-Me",              11, 1, 0,34, 0, "", BUTTON, 0, sortame,103,
"Lief-Me",              11, 1, 0,45, 0, "", BUTTON, 0, sortlme,104,
"S",                     4, 1, 0,56, 0, "", BUTTON, 0, sorts,  105
};

static form testub = { 5, 0, 0,_testub, 0, 0, 0, 0, &ListFont};

static field _testvl [] =
{      "1",                  1,  0, 0, 12, 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 34 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 45 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 56 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 60 , 0, "", NORMAL, 0, 0, 0
};

static form testvl = {5, 0, 0,_testvl, 0, 0, 0, 0, &ListFont};

static int asort = 1;
static int bsort = 1;
static int amesort = 1;
static int lmesort = 1;
static int ssort = 1;


static int sorta0 (const void *elem1, const void *elem2)
{
	        struct AUFP_S *el1;
	        struct AUFP_S *el2;

			el1 = (struct AUFP_S *) elem1;
			el2 = (struct AUFP_S *) elem2;
	        return ((int) (ratod(el1->a) - ratod (el2->a)) * asort);
}


static int sorta (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufparr, aufpanz, sizeof (struct AUFP_S),
				   sorta0);
			asort *= -1;
            ActivateColButton (eFussWindow, &lFussform, 2, -1, 1);
            ShowNewElist ((char *) aufparr,
                             aufpanz,
                            (int) sizeof (struct AUFP_S));
			return 0;
}


static int sortb0 (const void *elem1, const void *elem2)
{
	        struct AUFP_S *el1;
	        struct AUFP_S *el2;

			el1 = (struct AUFP_S *) elem1;
			el2 = (struct AUFP_S *) elem2;
	        return ((int) (strcmp (el1->a_bz1,el2->a_bz1)) * bsort);
}


static int sortb (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufparr, aufpanz, sizeof (struct AUFP_S),
				   sortb0);
			bsort *= -1;
            ActivateColButton (eFussWindow, &lFussform, 2, -1, 1);
            ShowNewElist ((char *) aufparr,
                             aufpanz,
                            (int) sizeof (struct AUFP_S));
			return 0;
}


static int sortame0 (const void *elem1, const void *elem2)
{
	        struct AUFP_S *el1;
	        struct AUFP_S *el2;

			el1 = (struct AUFP_S *) elem1;
			el2 = (struct AUFP_S *) elem2;
			if (ratod (el1->auf_me) > ratod (el2->auf_me))
			{
				          return (1 * amesort);
            }
			if (ratod (el1->auf_me) < ratod (el2->auf_me))
			{
				          return (-1 * amesort);
            }
			return 0;
}

static int sortame (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufparr, aufpanz, sizeof (struct AUFP_S),
				   sortame0);
			amesort *= -1;
            ActivateColButton (eFussWindow, &lFussform, 2, -1, 1);
            ShowNewElist ((char *) aufparr,
                             aufpanz,
                            (int) sizeof (struct AUFP_S));
			return 0;
}

static int sortlme0 (const void *elem1, const void *elem2)
{
	        struct AUFP_S *el1;
	        struct AUFP_S *el2;

			el1 = (struct AUFP_S *) elem1;
			el2 = (struct AUFP_S *) elem2;
			if (ratod (el1->lief_me) > ratod (el2->lief_me))
			{
				          return (1 * lmesort);
            }
			if (ratod (el1->lief_me) < ratod (el2->lief_me))
			{
				          return (-1 * lmesort);
            }
			return 0;
}


static int sortlme (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufparr, aufpanz, sizeof (struct AUFP_S),
				   sortlme0);
			lmesort *= -1;
            ActivateColButton (eFussWindow, &lFussform, 2, -1, 1);
            ShowNewElist ((char *) aufparr,
                             aufpanz,
                            (int) sizeof (struct AUFP_S));
			return 0;
}

static int sorts0 (const void *elem1, const void *elem2)
{
	        struct AUFP_S *el1;
	        struct AUFP_S *el2;

			el1 = (struct AUFP_S *) elem1;
			el2 = (struct AUFP_S *) elem2;
	        return ((atoi (el1->s) - atoi (el2->s)) * ssort);
}


static int sorts (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufparr, aufpanz, sizeof (struct AUFP_S),
				   sorts0);
			ssort *= -1;
            ActivateColButton (eFussWindow, &lFussform, 2, -1, 1);
            ShowNewElist ((char *) aufparr,
                             aufpanz,
                            (int) sizeof (struct AUFP_S));
			return 0;
}


BOOL IsAuszArtikel (void)
/**
Feld pr_ausz pruefen.
**/
{
            char pr_ausz [2];

            if (lese_a_bas (ratod (aufps.a)) != 0)
            {
                          return FALSE;
            }
            strcpy (pr_ausz, "N");
            switch (_a_bas.a_typ)
            {
                   case 1 :
                         strcpy (a_hndw.pr_ausz, "N");
                         hndw_class.lese_a_hndw (_a_bas.a);
                         strcpy (pr_ausz, a_hndw.pr_ausz);
                         break;
                   case 2 :
                         strcpy (a_eig.pr_ausz, "N");
                         hndw_class.lese_a_eig (_a_bas.a);
                         strcpy (pr_ausz, a_eig.pr_ausz);
                         a_hndw.tara = a_eig.tara;
                         break;
                   case 3 :
                         strcpy (a_eig_div.pr_ausz, "N");
                         hndw_class.lese_a_eig_div (_a_bas.a);
                         strcpy (pr_ausz, a_eig_div.pr_ausz);
                         a_hndw.tara = a_eig_div.tara;
                         break;
                   default :
                         return FALSE;
            }
            if (pr_ausz[0] == 'N')
            {
                    return FALSE;
            }
            return TRUE;
}

int SetLAN (void)
/**
Bestellnummer des Lieferanten im Kopf anzeigen.
**/
{
			strcpy (LAN, aufps.a_best);
			display_field (eKopfWindow, &lKopfform.mask[3]);
			return 0;
}


int CheckHbk ()
/**
Pruefen, ob der Artikel ausgezeichnet werden kann.
**/
{
	        lese_a_bas (ratod (aufps.a));
// Default-Halbarkeits-KZ = 'T' Tage  07.08.2002
/*
            if (_a_bas.hbk_kz[0] <= ' ')
            {
                strcpy (_a_bas.hbk_kz, "T");
            }
*/

			if (InScanning == 0) //110809
			{
            if (_a_bas.hbk_kz[0] > ' ')
            {
                         ActivateColButton (eFussWindow, &lFussform, 2, 0, 1);
            }
            else
            {
                         ActivateColButton (eFussWindow, &lFussform, 2, -1, 1);
            }
			}
			SetLAN ();
            return (0);
}

void GetMeEinh (void)
/**
Mengeneinheit fuer Kunde und Artikel holen.
**/
{
/*
         KEINHEIT keinheit;
         double auf_me, auf_me_vgl;

         einh_class.GetKunEinh (lsk.mdn, lsk.fil,
                                lsk.kun, _a_bas.a, &keinheit);

         strcpy (aufps.auf_me_bz,    keinheit.me_einh_kun_bez);
         strcpy (aufps.lief_me_bz,   keinheit.me_einh_bas_bez);
         sprintf (aufps.me_einh_kun, "%d", keinheit.me_einh_kun);
         sprintf (aufps.me_einh,     "%d", keinheit.me_einh_bas);
         clipped (aufps.auf_me_bz);
         clipped (aufps.lief_me_bz);
         if (keinheit.me_einh_kun == keinheit.me_einh_bas)
         {
                     strcpy (aufps.auf_me_vgl, aufps.auf_me);
         }
         else
         {
                     if (keinheit.inh <= (double) 0.0)
                     {
                                   keinheit.inh = (double) 1.0;
                     }
                     auf_me = (double) ratod (aufps.auf_me);
                     auf_me_vgl = auf_me * keinheit.inh;
                     sprintf (aufps.auf_me_vgl, "%.3lf", auf_me_vgl);
         }
*/
         return;
}

int lese_lief_bzg (char *buffer, double *a)
{
	     int dsqlstatus;

	     lief_bzg.mdn = we_kopf.mdn;
	     lief_bzg.fil = we_kopf.fil;
		 strcpy (lief_bzg.lief, we_kopf.lief);
		 strcpy (lief_bzg.lief_best, buffer);
		 dsqlstatus = LiefBzg.dbreadfirstbest ();
		 while (dsqlstatus == 100)
		 {
			 if (lief_bzg.fil)
			 {
				 lief_bzg.fil = 0;
			 }
			 else if (lief_bzg.mdn)
			 {
				 lief_bzg.mdn = 0;
			 }
			 else
			 {
				 break;
			 }
  		     dsqlstatus = LiefBzg.dbreadfirstbest ();
		 }
		 if (dsqlstatus == 0)
		 {
			 *a = lief_bzg.a;
	         dsqlstatus = lese_a_bas (*a);
		 }
		 else
		 {
			 *a = (double) 0;
		 }
		 if (dsqlstatus == 0) return 0;

// jetzt Suche �ber lief_bzg.ean
	     lief_bzg.mdn = we_kopf.mdn;
	     lief_bzg.fil = we_kopf.fil;
		 strcpy (lief_bzg.lief, we_kopf.lief);
		 strncpy (lief_bzg.ean, buffer,16);
//		 lief_bzg.ean = ratod(buffer);
		 dsqlstatus = LiefBzg.dbreadfirstean ();
		 while (dsqlstatus == 100)
		 {
			 if (lief_bzg.fil)
			 {
				 lief_bzg.fil = 0;
			 }
			 else if (lief_bzg.mdn)
			 {
				 lief_bzg.mdn = 0;
			 }
			 else
			 {
				 break;
			 }
  		     dsqlstatus = LiefBzg.dbreadfirstean ();
		 }
		 if (dsqlstatus == 0)
		 {
			 *a = lief_bzg.a;
	         dsqlstatus = lese_a_bas (*a);
		 }
		 else
		 {
			 *a = (double) 0;
		 }
		 return dsqlstatus;
}

int Reada_pr (void)
/**
Artikelpreis aus a_pr holen.
**/
{
    char datum [12];
    short mdn_gr;
    short fil_gr;
    short sa;
    double pr_fil_ek, pr_vk;

    sysdate (datum);
    mdn_gr = fil_gr = 0;

    if (we_kopf.mdn > 0)
    {
        LiefBzg.sqlin ((short *) &we_kopf.mdn, 1, 0);
        LiefBzg.sqlout ((short *) &mdn_gr, 1, 0);
        LiefBzg.sqlcomm ("select mdn_gr from mdn where mdn = ?");
    }
    if (we_kopf.mdn > 0 && we_kopf.fil > 0)
    {
        LiefBzg.sqlin ((short *) &we_kopf.mdn, 1, 0);
        LiefBzg.sqlin ((short *) &we_kopf.fil, 1, 0);
        LiefBzg.sqlout ((short *) &fil_gr, 1, 0);
        LiefBzg.sqlcomm ("select fil_gr from fil where mdn = ? and fil = ?");
    }

    sa = 0;
    pr_fil_ek = pr_vk = 0.0;
    sa = AbPreise.fetch_preis_tag (mdn_gr, we_kopf.mdn, fil_gr, we_kopf.fil, _a_bas.a,
                                   datum, &pr_fil_ek, &pr_vk);
    mdn_class.lese_mdn (Mandant);
    if (atoi (_mdn.waehr_prim) == 2)
    {
        sprintf (aufps.pr_fil_ek_eu, "%4lf", pr_fil_ek);
        sprintf (aufps.pr_fil_ek, "%4lf", pr_fil_ek * _mdn.konversion);
        sprintf (aufps.ls_lad_euro,"%4lf", pr_vk);
        sprintf (aufps.auf_lad_pr,"%4lf", pr_vk * _mdn.konversion);
    }
    else
    {
        sprintf (aufps.pr_fil_ek, "%4lf", pr_fil_ek);
        sprintf (aufps.pr_fil_ek_eu, "%4lf", pr_fil_ek / _mdn.konversion);
        sprintf (aufps.auf_lad_pr,"%4lf", pr_vk);
        sprintf (aufps.ls_lad_euro,"%4lf", pr_vk / _mdn.konversion);
    }
    return 0;
}

void CalcEkBto (void)
{
       pr_ek_bto0 = lief_bzg.pr_ek;
       pr_ek_bto0_euro = lief_bzg.pr_ek_eur;
	   if (lief_bzg.pr_ek > 0.0)
	   {
              pr_ek0  = pr_ek_bto0 - (double) WePreis.GetRabEk
				                                      (we_kopf.mdn, we_kopf.fil,
			                                           we_kopf.lief,
	  						                           1, pr_ek_bto0, _a_bas.me_einh, _a_bas.a, _a_bas.ag, _a_bas.wg);

	   }
	   else
	   {
			  pr_ek0 = (double) 0.0;
	   }
	   if (lief_bzg.pr_ek_eur > 0.0)
	   {
              pr_ek0_euro  = pr_ek_bto0_euro - (double) WePreis.GetRabEk
				                                      (we_kopf.mdn, we_kopf.fil,
			                                           we_kopf.lief,
	  						                           1, pr_ek_bto0_euro, _a_bas.me_einh, _a_bas.a, _a_bas.ag, _a_bas.wg);
	   }
	   else
	   {
			  pr_ek0_euro = (double) 0.0;
	   }
}

int ReadPr (void)
{
       dsqlstatus =   WePreis.preis_holen (we_kopf.mdn,
                                           we_kopf.fil,
                                           we_kopf.lief,
                                           we_pos.a);
       if (dsqlstatus != 0)
       {
            return 100;
       }

//       pr_ek_bto0 = lief_bzg.pr_ek;
       pr_ek_bto0 = lief_bzg.pr_ek_eur; //280809 erst mal nur aus eur-Felder
       pr_ek_bto0_euro = lief_bzg.pr_ek_eur;
	   if (lief_bzg.pr_ek > 0.0)
	   {
              pr_ek0  = pr_ek_bto0 - (double) WePreis.GetRabEk
				                                      (we_kopf.mdn, we_kopf.fil,
			                                           we_kopf.lief,
	  						                           1, pr_ek_bto0, _a_bas.me_einh, _a_bas.a, _a_bas.ag, _a_bas.wg);
	   }
	   else
	   {
			  pr_ek0 = (double) 0.0;
	   }
	   if (lief_bzg.pr_ek_eur > 0.0)
	   {
              pr_ek0_euro  = pr_ek_bto0_euro - (double) WePreis.GetRabEk
				                                      (we_kopf.mdn, we_kopf.fil,
			                                           we_kopf.lief,
	  						                           1, pr_ek_bto0_euro, _a_bas.me_einh, _a_bas.a, _a_bas.ag, _a_bas.wg);
	   }
	   else
	   {
			  pr_ek0_euro = (double) 0.0;
	   }
       return 0;
}

int ScanLiefEan (char *buffer, double *a)
{
	  dsqlstatus = lese_lief_bzg (buffer, a);

	  if (dsqlstatus != 0)
	  {
 			dsqlstatus = lese_a_bas (ratod (buffer));
			*a = ratod (buffer);
			if (dsqlstatus == 100)
			{
						dsqlstatus = ScanArtikel (*a);
			}
	  }
	  else
	  {
 			dsqlstatus = lese_a_bas (*a);
	  }

	  return dsqlstatus;
}

int ArtikelFromScann (char *buffer, BOOL DispMode)
{

    double a;

    if (strlen (buffer) >= 16)
	{
			 ScanEan128 (Text (buffer), DispMode);
	}
	else
	{
			 ScanLiefEan (buffer, &a);
	}
    return dsqlstatus;
}


int artikelOK (char *buffer, BOOL DispMode)
/**
Artikel-Nummer in Basisdaten suchen.
**/
{
	    double a;

		if (InScanning)
		{
			dsqlstatus = ArtikelFromScann (buffer, FALSE);
		}
	    else if (atoi (lief.we_erf_kz) != 2)
		{
			     if (strlen (buffer) >= 16)
				 {
					 ScanEan128 (Text (buffer));
				 }
				 else
				 {
					a = ratod (buffer);
					dsqlstatus = lese_a_bas (ratod (buffer));
					if (dsqlstatus == 100)
					{
						dsqlstatus = ScanArtikel (a);
					}
				 }
		}
		else
		{
		         dsqlstatus = lese_lief_bzg (buffer, &a);
		}
        if (dsqlstatus != 0)
        {
			if (DispMode && InScanning)
			{
                    disp_messG ("Die Artikelnummer wurde nicht gefunden.\n"
						        "Interne Artikelnummer �ber <neu>\n"
								"eingeben", 2);
			}
			else if (DispMode)
			{
                    disp_messG ("Falsche Artikelnummer", 2);
			}
            return FALSE;
        }
        sprintf (aufps.a, "%11.0lf", _a_bas.a);
        strcpy (aufps.a_bz1, _a_bas.a_bz1);
        strcpy (aufps.a_bz2, _a_bas.a_bz2);
		sprintf (aufps.me_einh, "%hd", _a_bas.me_einh);
        strcpy (aufps.hnd_gew, _a_bas.hnd_gew);
        strcpy (aufps.erf_gew_kz, GetErfGewKz (we_kopf.mdn, we_kopf.fil,_a_bas.a));
        we_pos.a = _a_bas.a;
        dsqlstatus = ReadPr ();
/*
        dsqlstatus =   WePreis.preis_holen (we_kopf.mdn,
                                            we_kopf.fil,
                                            we_kopf.lief,
                                             _a_bas.a);
*/
         if (lief_bzg_zwang && dsqlstatus != 0)
         {
			          disp_messG ("Kein Eintrag in den Bezugsquellen", 2);
					  return FALSE;

         }
		 else if (dsqlstatus == 0)
		 {
			 we_pos.a = _a_bas.a;
//020209			 strcpy (aufps.a_best, GetLiefBest ());
			 strcpy (aufps.a_best, lief_bzg.lief_best); //020209
		 }
		 if (lief_bzg.me_einh_ek)
		 {
 		          sprintf (aufps.me_einh, "%hd", lief_bzg.me_einh_ek);
		 }

		 if (atoi (aufps.me_einh) == 0) strcpy (aufps.me_einh, "2");

         Reada_pr ();
//         if (EktoMeEinh && lief_bzg.me_kz[0] == '1')
/*
         if (lief_bzg.me_kz[0] == '1')
         {
             pr_ek_bto0      = pr_ek_bto0 *  lief_bzg.min_best;
             pr_ek_bto0_euro = pr_ek_bto0_euro *  lief_bzg.min_best;
             pr_ek0          = pr_ek0*  lief_bzg.min_best;
             pr_ek0_euro     = pr_ek0_euro *  lief_bzg.min_best;
         }
*/

		 lief_bzg.min_best = (lief_bzg.min_best == 0) ? 1 : lief_bzg.min_best;
         if (lief_bzg.me_kz[0] == '0')
         {
             pr_ek_bto0      = pr_ek_bto0 /  lief_bzg.min_best;
             pr_ek_bto0_euro = pr_ek_bto0_euro /  lief_bzg.min_best;
             pr_ek0          = pr_ek0 /  lief_bzg.min_best;
             pr_ek0_euro     = pr_ek0_euro /  lief_bzg.min_best;
         }
		 sprintf (aufps.inh, "%.0lf", 1.0);
         sprintf (aufps.auf_pr_vk,   "%8.4lf", pr_ek_bto0);
         sprintf (aufps.ls_vk_euro,  "%8.4lf", pr_ek_bto0_euro);
         sprintf (aufps.auf_lad_pr,   "%8.4lf", pr_ek0);
         sprintf (aufps.ls_lad_euro,  "%8.4lf", pr_ek0_euro);
		 aufps.me_kz = atoi (lief_bzg.me_kz);
		 aufps.min_best = lief_bzg.min_best;

		 CheckHbk ();
         display_form (InsWindow, &insform, 0, 0);
         return TRUE;
}


void KorrInsPos (TEXTMETRIC *tm)
/**
Positionen in insform korrigieren.
**/
{
        static int posOK = 0;
        int i;

        if (posOK) return;

        posOK = 1;
        for (i = 0; i < insform.fieldanz; i ++)
        {
                    insform.mask[i].length *= (short) tm->tmAveCharWidth;
                    insform.mask[i].pos[1] *= (short) tm->tmAveCharWidth;
                    insform.mask[i].pos[0] *= (short) tm->tmHeight;
        }

        for (i = 0; i < insub.fieldanz; i ++)
        {
                    insub.mask[i].pos[1] *= (short) tm->tmAveCharWidth;
                    insub.mask[i].pos[0] *= (short) tm->tmHeight;
        }
}

void OpenInsert (void)
/**
Fenster fuer Insert-Row oeffnen.
**/
{
        RECT rect;
        RECT frmrect;
        HFONT hFont;
        TEXTMETRIC tm;
        int x, y, cx, cy;

        GetWindowRect (hMainWindow, &rect);
        GetFrmRectEx (&insform, insform.font, &frmrect);

        x = rect.left + 5;
        y = rect.top + 60;
        cx = frmrect.right + 26;
        cy = rect.bottom - rect.top - 200;

        spezfont (&InsFont);
        hFont = SetWindowFont (hMainWindow);
        hdc = GetDC (hMainWindow);
        SelectObject (hdc, hFont);
        GetTextMetrics (hdc, &tm);
        ReleaseDC (hMainWindow, hdc);
        DeleteObject (hFont);

        KorrInsPos (&tm);

        InsWindow = CreateWindow ("ListKopf",
                                    "",
                                    WS_POPUP | WS_VISIBLE | WS_DLGFRAME,
                                    x, y,
                                    cx + 24, 72,
                                    eWindow,
                                    NULL,
                                    hMainInst,
                                    NULL);
}

void DeleteInsert (void)
/**
Insertwindow schliessen.
**/
{
       CloseControls (&insform);
       CloseControls (&insub);
       DestroyWindow (InsWindow);
       InsWindow = NULL;
}

BOOL NewPosi (void)
/**
Positionsnummern neu vergeben und in Datenbank schreiben.
**/
{
/*
          int i;

          beginwork ();
          if (LeseLspUpd () == FALSE) return FALSE;
          ls_class.delete_lspls (lsk.mdn, lsk.fil, lsk.ls);
          for (i = 0; i < aufpanz_upd; i ++)
          {
                   sprintf (aufparr[i].pnr, "%d", (i + 1) * 10);
                   arrtowepos (i);
                   ls_class.update_lsp (lsp.mdn, lsp.fil, lsp.ls,
                                        lsp.a, lsp.posi);
          }
          commitwork ();
*/
          return TRUE;

}

BOOL DelLeerPos (void)
/**
Positionsnummern neu vergeben und in Datenbank schreiben.
**/
{
/*
          int i;

          beginwork ();
          if (LeseLspUpd () == FALSE) return FALSE;
          ls_class.delete_lspls (lsk.mdn, lsk.fil, lsk.ls);
          for (i = 0; i < aufpanz_upd; i ++)
          {
                   if (atoi (aufparr[i].s) > 2)
                   {
                         arrtowepos (i);
                         ls_class.update_lsp (lsp.mdn, lsp.fil, lsp.ls,
                                              lsp.a, lsp.posi);
                   }
          }
          commitwork ();
          if (eWindow)
          {
                   LeseWePos ();
                   ShowNewElist ((char *) aufparr,
                                aufpanz,
                                (int) sizeof (struct AUFP_S));
          }
*/
          return TRUE;

}

BOOL SollToIst (void)
/**
Bei nicht bearbeiteten Positionen Ist-Wert auf Soll-Wert setzen.
**/
{
/*
          int i;

          beginwork ();
          if (LeseLspUpd () == FALSE) return FALSE;
          ls_class.delete_lspls (lsk.mdn, lsk.fil, lsk.ls);
          for (i = 0; i < aufpanz_upd; i ++)
          {
                   if (atoi (aufparr[i].s) < 3)
                   {
                       strcpy (aufparr[i].lief_me,
                               aufparr[i].auf_me_vgl);
                       strcpy (aufparr[i].s, "3");
                   }
                   arrtowepos (i);
                   ls_class.update_lsp (lsp.mdn, lsp.fil, lsp.ls,
                                              lsp.a, lsp.posi);
          }
          commitwork ();
          if (eWindow)
          {
                   LeseWePos ();
                   ShowNewElist ((char *) aufparr,
                                aufpanz,
                                (int) sizeof (struct AUFP_S));
          }
*/
          return TRUE;
}

BOOL SetAllPosStat (void)
/**
Bei nicht bearbeiteten Positionen Ist-Wert auf Soll-Wert setzen.
**/
{
/*
          int i;

          beginwork ();
          if (LeseLspUpd () == FALSE) return FALSE;
          ls_class.delete_lspls (lsk.mdn, lsk.fil, lsk.ls);
          for (i = 0; i < aufpanz_upd; i ++)
          {
                   if (atoi (aufparr[i].s) < 3)
                   {
                       strcpy (aufparr[i].s, "3");
                   }
                   arrtowepos (i);
                   ls_class.update_lsp (lsp.mdn, lsp.fil, lsp.ls,
                                              lsp.a, lsp.posi);
          }
          commitwork ();
          if (eWindow)
          {
                   LeseWePos ();
                   ShowNewElist ((char *) aufparr,
                                aufpanz,
                                (int) sizeof (struct AUFP_S));
          }
*/
          return TRUE;
}

long GetNextPosi (int pos)
/**
Naechste gueltige Positionnummer ermitteln.
**/
{
          long ap;
          long posi;

          if (pos > 0)
          {
                strcpy (aufparr[pos].pnr, aufparr[pos - 1].pnr);
          }
          for (pos ++;pos <= aufpanz; pos ++)
          {
                 ap = atol (aufparr[pos].pnr);
                 if ((ap % 10) == 0) break;
          }
          posi = atol (aufparr[pos - 1].pnr) + 1;
          return (posi);
}

void ScrollAufp (int pos)
/**
aufparr ab pos zum Einfuegen scrollen.
**/
{
         int i;

         for (i = aufpanz; i > pos; i --)
         {
                       memcpy (&aufparr [i], &aufparr [i + 1],
                                sizeof (struct AUFP_S));
         }
         aufpanz ++;
}

void ScrollAufpDel (int pos)
/**
aufparr ab pos nach Oben scrollen.
**/
{
         int i;

         for (i = pos; i < aufpanz; i ++)
         {
                       memcpy (&aufparr [i], &aufparr [i + 1],
                                sizeof (struct AUFP_S));
         }
         aufpanz --;
}


int dodelete (void)
/**
Eine Zeile aus Liste loeschen.
**/
{
	     int pos;

		 if (BuDel.aktivate == -1) return (0);
		 if (abfragejnG (eWindow, "Position wirklich l�schen ?", "N") == 0)
		 {
			 return (0);
		 }


         pos = GetListPos ();
         arrtowepos (pos);
         beginwork ();
//190709
			dsqlstatus = WePosClass.dbreadfirst ();
					BucheBsd (we_pos.a,
							we_pos.lief_best,
                         we_pos.me * -1,
                         we_pos.anz_einh * -1,
                         we_pos.me_einh,
                         we_pos.pr_ek,
						 we_pos.ls_charge,
						 we_pos.ls_ident);

		 DbClass.sqlin ((short *)  &we_pos.mdn, 1, 0);
		 DbClass.sqlin ((short *)  &we_pos.fil, 1, 0);
		 DbClass.sqlin ((char *)   &we_pos.lief_rech_nr, 0, 17);
		 DbClass.sqlin ((char *)   &we_pos.lief, 0, 17);
		 DbClass.sqlin ((double *) &we_pos.a, 3, 0);
		 DbClass.sqlin ((short *)  &we_pos.int_pos, 2, 0);
		 DbClass.sqlcomm ("delete from we_pos  "
			              "where mdn = ? "
						  "and fil = ? "
						  "and lief_rech_nr = ? "
						  "and lief = ? "
						  "and a  = ? "
						  "and int_pos = ?");

         commitwork ();
         LeseWePos ();
         ShowNewElist ((char *) aufparr,
                                aufpanz,
                                (int) sizeof (struct AUFP_S));
		 if (aufpanz == 0)
		 {
            ActivateColButton (BackWindow3,  &MainButton2, 2, -1, 1);
            ActivateColButton (eFussWindow, &lFussform,   0, -1, 1);
            ActivateColButton (eFussWindow, &lFussform,   2, -1, 1);
            ActivateColButton (eFussWindow, &lFussform,   3, -1, 1);
            ActivateColButton (eFussWindow, &lFussform,   4, -1, 1);
            ActivateColButton (eFussWindow, &lFussform,   5, -1, 1);
		 }
		 if (pos >= aufpanz -1)
		 {
			 pos = aufpanz - 1;
		 }
		 if (pos >= 0)
		 {
			SetListPos (pos);
		 }
         return 0;
}

int domess (void)
/**
Eine Zeile in Liste einfuegen.
**/
{
	     char buffer [10];
		 int aufpidx;

         aufpidx = SendMessage  (Getlbox (), LB_GETCURSEL, 0l, 0l);
         memcpy (&aufps, &aufparr[aufpidx], sizeof (struct AUFP_S));
		 if (MultiTemperature)
		 {
			AuswahlTemperature ();
			memcpy (&aufparr[aufpidx], &aufps, sizeof (struct AUFP_S));
            WriteQsMess (); //110908 Temperaturen und PH speichern
			return 0;
		 }

	     EnableWindows (hMainWindow, FALSE);
		 strcpy (buffer, aufps.temp);
         NuBlock.EnterNumBox (eWindow,"  Temperatur  ",
                                         buffer,    5, "%3.1f" ,
	      						 	     EntNuProc);
		 if (syskey == KEY5)
		 {
             EnableWindows (hMainWindow, TRUE);
			 return 0;
		 }
		 strcpy (aufps.temp, buffer);

		 strcpy (buffer, aufps.ph_wert);
         NuBlock.EnterNumBox (eWindow,"  PH-Wert  ",
                                         buffer, 6, "%4.2f" ,
	      						 	     EntNuProc);
		 if (syskey == KEY5)
		 {
             EnableWindows (hMainWindow, TRUE);
			 return 0;
		 }
		 strcpy (aufps.ph_wert, buffer);

         memcpy (&aufparr[aufpidx], &aufps, sizeof (struct AUFP_S));
         WriteQsMess (); //110908 Temperaturen und PH speichern
         EnableWindows (hMainWindow, TRUE);
		 return 0;
}

int doinsert0 (void)
/**
Eine Zeile in Liste einfuegen.
**/
{
         struct AUFP_S aufp;
         char buffer [80];
         int pos;
         long akt_posi;

		 scangew = 0l;
         OpenInsert ();
         memcpy (&aufps, &aufp_null, sizeof (struct AUFP_S));
         while (TRUE)
         {
                   strcpy (buffer, "0");
				   if (atoi (we_kopf.a_best_kz) != 2)
				   {
/*
                           EnterNumBox (eWindow,
                                "  Artikel  ", buffer, 12,
                                   testform.mask[0].picture);
*/
                          NuBlock.SetChoise (doartchoise, 0, dobzgchoise, 0,
							                 "Auswahl\nArtikel", "Auswahl\nBezugsquellen");
						  EnableWindows (hMainWindow, FALSE);
  	                      EnableWindows (eKopfWindow, FALSE);
                          EnableWindows (eFussWindow, FALSE);
                          EnableWindows (BackWindow2, FALSE);
                          EnableWindows (BackWindow3, FALSE);
                          EnableWindows (eWindow, TRUE);
						  NuBlock.SetCharChangeProc (ChangeChar);
                          NuBlock.EnterNumBox (eWindow,"  Artikel  ",
                                              buffer, -1,"",
	      									  EntNuProc);
  	                      EnableWindows (eKopfWindow, TRUE);
                          EnableWindows (eFussWindow, TRUE);
                          EnableWindows (BackWindow2, TRUE);
                          EnableWindows (BackWindow3, TRUE);
                          EnableWindows (eWindow, TRUE);
						  EnableWindows (hMainWindow, TRUE);
 				   }
				   else
				   {
/*
                           EnterNumBox (eWindow,
                                "  Bestell-Nr  ", buffer, 17,
                                   "");
*/
                          NuBlock.SetChoise (dobzgchoise, 0);
						  EnableWindows (AufKopfWindow, FALSE);
  	                      EnableWindows (eKopfWindow, FALSE);
                          EnableWindows (eFussWindow, FALSE);
                          EnableWindows (BackWindow2, FALSE);
                          EnableWindows (BackWindow3, FALSE);
                          EnableWindows (eWindow, FALSE);
						  NuBlock.SetCharChangeProc (ChangeChar);
                          NuBlock.EnterNumBox (eWindow,"  Bestell-Nr  ",
                                               buffer, -1, "" ,
	      									  EntNuProc);
  	                      EnableWindows (eKopfWindow, TRUE);
                          EnableWindows (eFussWindow, TRUE);
                          EnableWindows (BackWindow2, TRUE);
                          EnableWindows (BackWindow3, TRUE);
                          EnableWindows (eWindow, TRUE);
						  EnableWindows (AufKopfWindow, TRUE);
				   }
                   InvalidateRect (eWindow, NULL, TRUE);
                   UpdateWindow (eWindow);
                   SetDblClck (IsDblClck, 0);
                   if ((double) ratod (buffer) <= (double) 0.0)
                   {
                              memcpy (&aufps, &aufp, sizeof (struct AUFP_S));
                              current_form = &testform;
                              DeleteInsert ();
                              return 0;
                   }
                   if (syskey == KEY5)
                   {
                              memcpy (&aufps, &aufp, sizeof (struct AUFP_S));
                              current_form = &testform;
                              DeleteInsert ();
                              return 0;
                   }
                   if (artikelOK (buffer))
                   {
                              break;
                   }
                   current_form = &testform;
                   DeleteInsert ();
                   return 0;
         }

         if (scangew != 0l)
         {
            sprintf (aufps.lief_me, "%.3lf", (double) scangew / 1000);
         }

		 if (hbk_direct)
		 {
		           strcpy (aufps.hbk_date, "");

// Default-Halbarkeits-KZ = 'T' Tage  07.08.2002

/*
                   if (_a_bas.hbk_kz[0] <= ' ')
                   {
                           strcpy (_a_bas.hbk_kz, "T");
                   }
*/

		           if (_a_bas.hbk_kz[0] > ' ')
				   {
                           sysdate (buffer);
                           EnterNumBox (eWindow,
                                " Haltbarkeitsdatum ", buffer, 11,
                                   "dd.mm.yyyy");
                           InvalidateRect (eWindow, NULL, TRUE);
                           UpdateWindow (eWindow);
                           if (syskey != KEY5)
						   {
					          strcpy (aufps.hbk_date,buffer);
						   }
				   }
         }


         if (cfg_ohne_preis == 0 && immer_preis)
         {
                   EnterPreisBox (eWindow, -1, -1);
         }

         current_form = &testform;
         DeleteInsert ();
         InvalidateRect (eWindow, NULL, TRUE);
         UpdateWindow (eWindow);
//         pos = GetListPos () + 1;
         pos = aufpanz;
         akt_posi = GetNextPosi (pos);
         sprintf (aufps.pnr, "%ld", akt_posi);
         ScrollAufp (pos);
		 strcpy (aufps.ls_charge, "");
         memcpy (&aufparr [pos], &aufps, sizeof (struct AUFP_S));
         arrtowepos (pos);
         beginwork ();
         WePosClass.dbupdate ();
         commitwork ();
         LeseWePos ();
         SetListPos (pos);
         ShowNewElist ((char *) aufparr,
                                aufpanz,
                                (int) sizeof (struct AUFP_S));
		 if (aufpanz == 1)
		 {
            ActivateColButton (BackWindow3,  &MainButton2, 2, 0, 1);
            ActivateColButton (eFussWindow, &lFussform,   0, 0, 1);
            ActivateColButton (eFussWindow, &lFussform,   2, 0, 1);
            ActivateColButton (eFussWindow, &lFussform,   3, 0, 1);
            ActivateColButton (eFussWindow, &lFussform,   4, 0, 1);
            ActivateColButton (eFussWindow, &lFussform,   5, 0, 1);
		 }
         memcpy (&aufps, &aufparr [pos], sizeof (struct AUFP_S));
         CheckHbk ();
         SetListPos (pos);
		 if (wieg_neu_direct && MesswerteZwang == 0) //130509 MesswerteZwang 
		 {
                      IsDblClck (pos);
		 }
         return 0;
}


int doinsert (void)
{
	    if (BuInsert.aktivate == -1) return (0);
	    while (TRUE)
		{
	             doinsert0 ();
//	             ScanA ();
				 if (syskey == KEY5) break;
				 if (wieg_neu_direct != 2) break;
		}
		return 0;
}


int StartScanning (void)
{
        InScanning = TRUE;
	    while (TRUE)
		{
	             doinsert0 ();
//	             ScanA ();
				 if (syskey == KEY5) break;
				 if (wieg_neu_direct != 2) break;
		}
        InScanning = FALSE;
		return 0;
}

int endlist (void)
/**
Liste beenden.
**/
{
        break_list ();
        return (1);
}

void arrtowepos (int idx)
/**
Satz aufarr in we_pos uebertragen.
**/
{
	    char datum [12];
		int dsqlstatus;

	    sysdate (datum);
	    we_pos.mdn = we_kopf.mdn;
        we_pos.fil = we_kopf.fil;
	    strcpy (we_pos.lief,         we_kopf.lief);
		if (numeric (we_pos.lief))
		{
			we_pos.lief_s = atol (we_pos.lief);
		}
	    strcpy (we_pos.lief_rech_nr, we_kopf.lief_rech_nr);
	    strcpy (we_pos.blg_typ,      we_kopf.blg_typ);
        we_pos.int_pos  = atol (aufparr[idx].pnr);
        we_pos.a        = (double) ratod (aufparr[idx].a);
		strcpy (we_pos.lief_best,aufparr[idx].a_best);
		we_pos.bearb  = dasc_to_long (datum);
		we_pos.hbk_dat = dasc_to_long (aufparr[idx].hbk_date);
		we_pos.erz_dat = dasc_to_long (aufparr[idx].erz_date);
		if (we_pos.hbk_dat == 0l) we_pos.hbk_dat = LongNull;
		if (we_pos.erz_dat == 0l) we_pos.erz_dat = LongNull;

		dsqlstatus = WePosClass.dblock ();
		if (dsqlstatus == 100)
		{
                		we_pos.akv = dasc_to_long (datum);
						we_pos.erz_dat = LongNull;
		}

        we_pos.best_me    = (double) ratod (aufparr[idx].auf_me);
        we_pos.me         = (double) ratod (aufparr[idx].lief_me);
        we_pos.pr_ek      = (double) ratod (aufparr[idx].auf_pr_vk);
        we_pos.pr_ek_euro = (double) ratod (aufparr[idx].ls_vk_euro);
        we_pos.lager	  = aufparr[idx].lager;
		if (we_pos.pr_ek > 0.0)
		{
              we_pos.pr_ek_nto  = we_pos.pr_ek - (double) WePreis.GetRabEk (we_kopf.mdn, we_kopf.fil,
			                                           we_kopf.lief,
	  						                           1, we_pos.pr_ek, _a_bas.me_einh, _a_bas.a, _a_bas.ag, _a_bas.wg);
		}
		else
		{
			  we_pos.pr_ek_nto = (double) 0.0;
		}
		if (we_pos.pr_ek_euro > 0.0)
		{
              we_pos.pr_ek_nto_eu  = we_pos.pr_ek_euro - (double) WePreis.GetRabEk (we_kopf.mdn, we_kopf.fil,
			                                               we_kopf.lief,
	  						                            1, we_pos.pr_ek_euro, _a_bas.me_einh, _a_bas.a, _a_bas.ag, _a_bas.wg);
		}
		else
		{
			  we_pos.pr_ek_nto_eu = (double) 0.0;
		}

        we_pos.pr_vk    = (double) ratod (aufparr[idx].auf_lad_pr);
        we_pos.pr_vk_eu = (double) ratod (aufparr[idx].ls_lad_euro);
        we_pos.pr_fil_ek = (double) ratod (aufparr[idx].pr_fil_ek);
        we_pos.pr_fil_ek_eu = (double) ratod (aufparr[idx].pr_fil_ek_eu);
        we_pos.tara     = (double) ratod (aufparr[idx].tara);
        we_pos.anz_einh = (double) ratod (aufparr[idx].anz_einh);
        we_pos.a_krz = (long) ratod (aufparr[idx].a_krz);
//011009		if (we_pos.anz_einh == 0.0) we_pos.anz_einh = 1.0;
        we_pos.inh      = (double) ratod (aufparr[idx].inh);
		if (we_pos.inh == 0.0) we_pos.inh = 1.0;



// Inhalt beim Touchscreen immer auf 1

		we_pos.inh = 1.0;

        we_pos.me_einh     = atoi (aufparr[idx].me_einh);
        strcpy (we_pos.ls_charge, aufparr[idx].ls_charge);
        strcpy (we_pos.ls_ident,  aufparr[idx].ident_nr);
        strcpy (we_pos.es_nr,  aufparr[idx].es_nr);
        strcpy (we_pos.ez_nr,  aufparr[idx].ez_nr);
		we_pos.we_txt = atoi (aufparr[idx].lsp_txt);

		if (generiere_charge > 0)
		{
			// 23.06.2009 : hier nochmal f�llen, weil charge unterwegs verloren geht (nur auf toucher, auf server nicht ??!!)
				strcpy (we_pos.ls_charge, GenCharge ());
		}
}


long GenLText (void)
/**
Text-Nr fuer lsp.lsp_txt genereieren.
**/
{
        int dsqlstatus;
        short mdn;
        short fil;
        long gen_nr;
        extern short sql_mode;

        mdn = Mandant;
        fil = Filiale;

        sql_mode = 1;
        dsqlstatus = nvholid (mdn, fil, "lspt_txt");
        if (dsqlstatus == 100)
        {
              dsqlstatus = nvanmprf (mdn,
                                     fil,
                                     "lspt_txt",
                                     (long) 1,
                                     (long) 999999,
                                     (long) 10,
                                      "");

              if (dsqlstatus == 0)
              {
                          dsqlstatus = nvholid (mdn,
                                                fil,
                                                "lspt_txt");
              }
         }
         gen_nr = auto_nr.nr_nr;
         sql_mode = 0;
         return gen_nr;
}


void TextInsert (void)
/**
Text fuer Nicht- osder Nachliefern einfuegen.
**/
{
/*
       int aufidx;
       long lsp_txt;


       beginwork ();
       if (Lock_WePos () == FALSE)
       {
            commitwork ();
            return;
       }
       aufidx = GetListPos ();
       memcpy (&aufparr [aufidx], &aufps, sizeof (struct AUFP_S));

       lsp_txt = atol (aufparr [aufidx].lsp_txt);
       if (lsp_txt == 0l)
       {
           lsp_txt = GenLText ();
       }
       if (lsp_txt == 0l) return;
       strcpy (aufparr[aufidx].s, "3");
       sprintf (aufparr[aufidx].lsp_txt, "%ld", lsp_txt);
       memcpy (&aufps, &aufparr [aufidx], sizeof (struct AUFP_S));
       lspt.nr = lsp_txt;
       lspt.zei = 10;
       lspt_class.dbupdate ();
       arrtowepos (aufidx);
       WePosClass.dbupdate ();
       commitwork ();
       ShowNewElist ((char *) aufparr,
                      aufpanz,
                      (int) sizeof (struct AUFP_S));
*/
}

int Nachliefern ()
/**
Status NACHLIEFERN setzen.
**/
{
      ptab_class.lese_ptab ("na_lief_kz", "2");
      TextInsert ();
      return (0);
}


int Nichtliefern ()
/**
Status NICHTLIEFERN setzen.
**/
{
      ptab_class.lese_ptab ("na_lief_kz", "1");
      TextInsert ();
      return (0);
}


int SwitchwKopf ()
{
        static int arganz;
        static char *args[4];
        char kopf [4];

        if (la_zahl < 0) return 0;

        if (la_zahl == 2)
        {
            AktKopf = (AktKopf  + 1) % la_zahl;
            sprintf (kopf, "%d", AktKopf + 1);
            arganz = 4;
            args[0] = "Leer";
            args[1] = "6";
            args[2] = waadir;
            args[3] = (char *) kopf;
            fnbexec (arganz, args);
            print_messG (1, "Wiegkopf %s gew�hlt", kopf);
            return 0;
        }
        SaveMenue ();
        EnableWindow (eWindow, FALSE);
        EnableWindow (eKopfWindow, FALSE);
        EnableWindow (eFussWindow, FALSE);
        AktKopf = AuswahlKopf ();
        EnableWindow (eWindow, TRUE);
        EnableWindow (eKopfWindow, TRUE);
        EnableWindow (eFussWindow, TRUE);
        SetActiveWindow (eWindow);
        AktivWindow = eWindow;
        RestoreMenue ();
        SetDblClck (IsDblClck, 0);

        sprintf (kopf, "%d", AktKopf + 1);
        arganz = 4;
        args[0] = "Leer";
        args[1] = "6";
        args[2] = waadir;
        args[3] = (char *) kopf;
        fnbexec (arganz, args);
        return 0;
}


int SysChoise ()
/**
Auswahl ueber Geraete.
**/
{
		if (ScannMode)
		{
            return DoScann ();
		}
        SaveMenue ();
        EnableWindow (eWindow, FALSE);
        EnableWindow (eKopfWindow, FALSE);
        EnableWindow (eFussWindow, FALSE);
        AuswahlSysPeri ();
        EnableWindow (eWindow, TRUE);
        EnableWindow (eKopfWindow, TRUE);
        EnableWindow (eFussWindow, TRUE);
        SetActiveWindow (eWindow);
        AktivWindow = eWindow;
        RestoreMenue ();
        SetDblClck (IsDblClck, 0);
        return (0);
}


int PreisInput (void)
/**
Preise eingeben.
**/
{

        aufpidx = SendMessage  (Getlbox (), LB_GETCURSEL, 0l, 0l) - 1;
        EnterPreisBox (eWindow, -1, -1);
		if (aufpidx >= 0)
		{
                memcpy (&aufparr [aufpidx], &aufps, sizeof (struct AUFP_S));
                arrtowepos (aufpidx);
                beginwork ();
                WePosClass.dbupdate ();
                commitwork ();
		}
		return 0;
}

int HbkInput (void)
/**
Haltbarkeitsdatum eingeben.
**/
{
	    char buffer [12];

//        aufpidx = SendMessage  (Getlbox (), LB_GETCURSEL, 0l, 0l) - 1;
        aufpidx = SendMessage  (Getlbox (), LB_GETCURSEL, 0l, 0l);

		if (dasc_to_long (aufps.hbk_date) > 0l)
		{
			       strcpy (buffer, aufps.hbk_date);
		}
		else
		{
                   sysdate (buffer);
		}
        EnterNumBox (eWindow, " Haltbarkeitsdatum ", buffer, 11, "dd.mm.yyyy");
        InvalidateRect (eWindow, NULL, TRUE);
        UpdateWindow (eWindow);
        if (syskey != KEY5)
        {
	               strcpy (aufps.hbk_date,buffer);
                   memcpy (&aufparr[aufpidx], &aufps, sizeof (struct AUFP_S));
        }
		return 0;
}
int Auszeichnen ()
/**
Reaktion auf Doppelclick in der Liste.
**/
{
        if (BuAusZ.aktivate == -1) return (0);
        WorkModus = AUSZEICHNEN;
        PostMessage (eWindow, WM_KEYDOWN,  (WPARAM) VK_RETURN, 0l);
        return (0);
}


void IsDblClck (int idx)
/**
Reaktion auf Doppelclick in der Liste.
**/
{
	    char datum [12];
		int dsqlstatus;

	    if (TestOK () == FALSE) return;
        beginwork ();
        if (Lock_Lsp () == FALSE)
        {
            commitwork ();
            return;
        }
        aufpidx = idx;
        memcpy (&aufps, &aufparr[aufpidx], sizeof (struct AUFP_S));
		if (MesswerteZwang == 1)  //130509
		{
            if ( (double) ratod (aufps.lief_me) < 0.001)
			{
				domess(); 
			}
		}
        strcpy (auf_me, aufps.rest);


	    sysdate (datum);
	    we_pos.mdn = we_kopf.mdn;
        we_pos.fil = we_kopf.fil;
	    strcpy (we_pos.lief,         we_kopf.lief);
		if (numeric (we_pos.lief))
		{
			we_pos.lief_s = atol (we_pos.lief);
		}
	    strcpy (we_pos.lief_rech_nr, we_kopf.lief_rech_nr);
	    strcpy (we_pos.blg_typ,      we_kopf.blg_typ);
        we_pos.int_pos  = atol (aufps.pnr);
        we_pos.a        = (double) ratod (aufps.a);
		strcpy (we_pos.lief_best,aufps.a_best);
		we_pos.bearb  = dasc_to_long (datum);
		we_pos.hbk_dat = dasc_to_long (aufps.hbk_date);
		if (we_pos.hbk_dat == 0l) we_pos.hbk_dat = LongNull;
		we_pos.erz_dat = dasc_to_long (aufps.erz_date);
		if (we_pos.erz_dat == 0l) we_pos.erz_dat = LongNull;

		we_pos.me = (double) ratod(aufps.lief_me);
		we_pos.anz_einh = (double) ratod(aufps.anz_einh);
		we_pos.me_einh = atoi(aufps.me_einh);
		we_pos.pr_ek = (double) ratod(aufps.auf_pr_vk);
	    strcpy (we_pos.ls_charge, aufps.ls_charge);
	    strcpy (we_pos.ls_ident, aufps.ident_nr);
		
		dsqlstatus = WePosClass.dblock ();
		if (dsqlstatus == 100)
		{
                		we_pos.akv = dasc_to_long (datum);
						we_pos.erz_dat = LongNull;
		}
		else
		{
		    //190709
			if (we_pos.me != 0.0)
			{
				dsqlstatus = WePosClass.dbreadfirst ();
					BucheBsd (we_pos.a,
							we_pos.lief_best,
                         we_pos.me * -1,
                         we_pos.anz_einh * -1,
                         we_pos.me_einh,
                         we_pos.pr_ek,
						 we_pos.ls_charge,
						 we_pos.ls_ident);
			}
		}



        if (strcmp (aufps.hnd_gew, "G") == 0 || strcmp (aufps.erf_gew_kz, "J") == 0)
        {
                       ArtikelWiegen (eFussWindow);
        }
        else
        {
//                       ArtikelHand ();
                       ArtikelWiegen (eFussWindow);
        }
        WorkModus = WIEGEN;
        SetDblClck (IsDblClck, 0);
        current_form = &testform;
        if (ratod (aufps.a) != 0.0)
        {
               strcpy (aufps.rest, auf_me);
			   aufps.lager = akt_lager;
               memcpy (&aufparr[aufpidx], &aufps, sizeof (struct AUFP_S));
               arrtowepos (idx);
               WePosClass.dbupdate ();
			   WriteRind ();
			   BucheChargenBsd ();
			    //190709
				BucheBsd (we_pos.a,
							we_pos.lief_best,
                         we_pos.me ,
                         we_pos.anz_einh ,
                         we_pos.me_einh,
                         we_pos.pr_ek,
						 we_pos.ls_charge,
						 we_pos.ls_ident);
        }
	    ShowNewElist ((char *) aufparr,
                             aufpanz,
                            (int) sizeof (struct AUFP_S));
        commitwork ();
		SetListPos (aufpidx);
		SetActiveWindow (eWindow);
        if (autocursor)
        {
                   PostMessage (eWindow, WM_KEYDOWN, VK_DOWN, 0l);
        }
        return;
}

BOOL TestSysWg (void)
/**
Pruefen, ob die Warengruppe dem aaktiven Geraet zugeordnet ist.
**/
{
        pht_wg.sys = auszeichner;
        pht_wg.wg  = _a_bas.wg;
        if (pht_wg_class.dbreadfirst_wg () != 0)
        {
            return FALSE;
        }
        return TRUE;
}


char *GetErfGewKz (short smdn, short sfil, double sa)
/**
erf_gew_kz aus a_best holen.
**/
{
	     static int cursor = -1;
		 static short mdn;
		 static short fil;
		 static double a;
		 static char erf_gew_kz [2];
		 int dsqlstatus;

		 if (cursor == -1)
		 {
			 DbClass.sqlin ((short *) &mdn, 1, 0);
			 DbClass.sqlin ((short *) &fil, 1, 0);
			 DbClass.sqlin ((double *) &a,3, 0);
			 DbClass.sqlout ((char *) erf_gew_kz, 0, 2);
			 cursor = DbClass.sqlcursor ("select erf_gew_kz from a_best "
				                         "where mdn = ? "
										 "and fil = ? "
										 "and a = ?");
		 }
		 mdn = smdn;
		 fil = sfil;
		 a =   sa;
         strcpy (erf_gew_kz, "J");

		 DbClass.sqlopen (cursor);
		 dsqlstatus = DbClass.sqlfetch (cursor);
		 while (dsqlstatus == 100)
		 {
			 if (fil > 0)
			 {
				 fil = 0;
			 }
			 else if (mdn > 0)
			 {
				 mdn = 0;
			 }
			 else
			 {
				 break;
			 }
  		     DbClass.sqlopen (cursor);
		     dsqlstatus = DbClass.sqlfetch (cursor);
		 }
		 return erf_gew_kz;
}

char *GetLiefBest (void)
{
	     static char lief_best [17];
		 static short fil;
		 static short mdn;
		 static int cursor;
		 static int dsqlstatus;

		 strcpy (lief_best, " ");
		 DbClass.sqlin ((short *)  &mdn, 1, 0);
		 DbClass.sqlin ((short *)  &fil, 1, 0);
		 DbClass.sqlin ((char *)   we_kopf.lief, 0, 17);
		 DbClass.sqlin ((double *) &we_pos.a, 3, 0);
		 DbClass.sqlout ((char *)  lief_best, 0, 17);
		 cursor = DbClass.sqlcursor ("select lief_best from lief_bzg "
			                                              "where mdn = ? "
			                                              "and fil = ? "
			                                              "and lief = ? "
			                       						  "and a = ?");
		 mdn = we_kopf.mdn;
		 fil = we_kopf.fil;
		 dsqlstatus = open_sql (cursor);
		 dsqlstatus = fetch_sql (cursor);
		 while (dsqlstatus == 100)
		 {
			 if (fil)
			 {
				 fil = 0;
			 }
			 else if (mdn)
			 {
				 mdn = 0;
			 }
			 else
			 {
				 break;
			 }
		     dsqlstatus = open_sql (cursor);
		     dsqlstatus = fetch_sql (cursor);
		 }
         DbClass.sqlclose (cursor);
		 return lief_best;
}


BOOL wepostoarr (int idx)
/**
Satz aus we_pos in aufarr uebertragen.
**/
{
//	    char wert [4];


        lese_a_bas (we_pos.a);

        memcpy (&aufparr[idx], &aufp_null, sizeof (aufps));
        strcpy (aufparr[idx].hnd_gew, _a_bas.hnd_gew);
        strcpy (aufparr[idx].erf_gew_kz, GetErfGewKz (we_kopf.mdn, we_kopf.fil,we_pos.a));

        sprintf (aufparr[idx].s, "%1d", 0);
        sprintf (aufparr[idx].pnr, "%ld", we_pos.int_pos);
        sprintf (aufparr[idx].a, "%.0lf", we_pos.a);
//020209		strcpy (aufparr[idx].a_best, GetLiefBest ());
		strcpy (aufparr[idx].a_best, we_pos.lief_best); //020209
        strcpy (aufparr[idx].a_bz1,_a_bas.a_bz1);
        sprintf (aufparr[idx].a_me_einh, "%hd", _a_bas.me_einh);
        sprintf (aufparr[idx].auf_me, "%.3lf", we_pos.best_me);
        sprintf (aufparr[idx].lief_me, "%.3lf", we_pos.me);
        sprintf (aufparr[idx].auf_me_vgl, "%.3lf", we_pos.gew);   //130509
        sprintf (aufparr[idx].auf_pr_vk, "%.2lf", we_pos.pr_ek);
        sprintf (aufparr[idx].ls_vk_euro, "%.2lf", we_pos.pr_ek_euro);
        sprintf (aufparr[idx].auf_lad_pr, "%.2lf", we_pos.pr_ek_nto);
        sprintf (aufparr[idx].ls_lad_euro, "%.2lf", we_pos.pr_ek_nto_eu);
        sprintf (aufparr[idx].pr_fil_ek, "%.4lf",   we_pos.pr_fil_ek);
        sprintf (aufparr[idx].pr_fil_ek_eu, "%.4lf",   we_pos.pr_fil_ek_eu);

        sprintf (aufparr[idx].tara, "%.3lf", we_pos.tara);
        sprintf (aufparr[idx].anz_einh, "%.3lf", we_pos.anz_einh);
        sprintf (aufparr[idx].a_krz, "%.3lf", we_pos.a_krz);
        sprintf (aufparr[idx].inh,      "%.3lf", we_pos.inh);
        sprintf (aufparr[idx].me_einh,  "%hd",   we_pos.me_einh);
        dlong_to_asc (we_pos.hbk_dat, aufparr[idx].hbk_date);
        dlong_to_asc (we_pos.erz_dat, aufparr[idx].erz_date);

		// hier m�ssen Werte noch geholt werden 
		HoleQsMess(idx);
		/**
        sprintf (aufparr[idx].temp,      "%3.1lf", (double) 0.0);
        sprintf (aufparr[idx].temp2,      "%3.1lf", (double) 0.0);
        sprintf (aufparr[idx].temp3,      "%3.1lf", (double) 0.0);
        sprintf (aufparr[idx].ph_wert,   "%4.2lf", (double) 0.0);
		**/


        strcpy (aufparr[idx].ls_charge, we_pos.ls_charge);
        strcpy (aufparr[idx].ident_nr, we_pos.ls_ident);
        strcpy (aufparr[idx].es_nr, we_pos.es_nr);
        strcpy (aufparr[idx].ez_nr, we_pos.ez_nr);
        sprintf (aufparr[idx].lsp_txt,  "%ld",   we_pos.we_txt);
		aufparr[idx].lager = we_pos.lager;

/*
		sprintf (wert, "%d", _a_bas.me_einh);
        ptab_class.lese_ptab ("me_einh", wert);
        strcpy (aufparr[idx].basis_me_bz, ptabn.ptbezk);

		sprintf (wert, "%d", we_pos.me_kz);
        ptab_class.lese_ptab ("me_einh", wert);
        strcpy (aufparr[idx].auf_me_bz, ptabn.ptbezk);
*/

        return 0;
}

BOOL LeseLspUpd (void)
/**
Daten aus lsp in aufarr einlesen.
**/
{
        extern short sql_mode;
        aufpanz = 0;
        aufpidx = 0;

        sql_mode = 1;
		we_pos.mdn = we_kopf.mdn;
		we_pos.fil = we_kopf.fil;
        strcpy (we_pos.lief_rech_nr, we_kopf.lief_rech_nr);
        strcpy (we_pos.lief, we_kopf.lief);
        strcpy (we_pos.blg_typ, we_kopf.blg_typ);
        dsqlstatus = WePosClass.dbreadfirst_o ("order by int_pos, a");
        while (dsqlstatus == 0)
        {
                   if (WePosClass.dblock () != 0)
                   {
                       return FALSE;
                   }
                   if (wepostoarr (aufpanz) != 0)
                   {
                       dsqlstatus = WePosClass.dbread_o ();
                       continue;
                   }

                   aufpanz ++;
                   if (aufpanz == AUFMAX) break;
                   dsqlstatus = WePosClass.dbread_o ();
        }
        aufpanz_upd = aufpanz;
        return TRUE;
}


void LeseWePos (void)
/**
Daten aus lsp in aufarr einlesen.
**/
{
        aufpanz = 0;
        aufpidx = 0;
		we_pos.mdn = we_kopf.mdn;
		we_pos.fil = we_kopf.fil;
		strcpy (we_pos.lief, we_kopf.lief);
		strcpy (we_pos.lief_rech_nr, we_kopf.lief_rech_nr);
		strcpy (we_pos.blg_typ, we_kopf.blg_typ);
        dsqlstatus = WePosClass.dbreadfirst_o ("order by int_pos, a");
        while (dsqlstatus == 0)
        {
                   if (wepostoarr (aufpanz) != 0)
                   {
                       dsqlstatus = WePosClass.dbread_o ();
                       continue;
                   }


                   aufpanz ++;
                   if (aufpanz == AUFMAX) break;
                   dsqlstatus = WePosClass.dbread_o ();
        }
		if (InScanning == 0) //110809
		{
		if (aufpanz == 0)
		{
//            ActivateColButton (BackWindow3, &MainButton2, 2, -1, 1);
//            ActivateColButton (eFussWindow, &lFussform,   0, -1, 0);
            ActivateColButton (eFussWindow, &lFussform,   2, -1, 0);
            ActivateColButton (eFussWindow, &lFussform,   3, -1, 0);
            ActivateColButton (eFussWindow, &lFussform,   4, -1, 0);
            ActivateColButton (eFussWindow, &lFussform,   5, -1, 0);
		}
		else
		{
//            ActivateColButton (BackWindow3, &MainButton2, 0, 0, 1);
//            ActivateColButton (eFussWindow, &lFussform, 0, 0, 0);
            ActivateColButton (eFussWindow, &lFussform, 2, 0, 0);
            ActivateColButton (eFussWindow, &lFussform, 3, 0, 0);
            ActivateColButton (eFussWindow, &lFussform, 4, 0, 0);
            ActivateColButton (eFussWindow, &lFussform, 5, 0, 0);
		}
		}
}

void RegisterListe (void)
/**
Kopf - und Fuss - Fenster fuer Liste registrieren
**/
{
        static int registered = 0;
        WNDCLASS wc;

        if (registered == 0)
        {
                   wc.style =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                               | CS_OWNDC;
                   wc.cbClsExtra    =  0;
                   wc.cbWndExtra    =  0;
                   wc.hInstance     =  hMainInst;
                   wc.hIcon         =  LoadIcon (hMainInst, "FITICON");
                   wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                   wc.lpszMenuName  =  "";
                   wc.lpfnWndProc   =  ListKopfProc;
                   wc.lpszMenuName  =  "";
                   wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
                   wc.lpszClassName =  "ListKopf";
                   RegisterClass(&wc);
                   registered = 1;
        }
}


LONG FAR PASCAL ListKopfProc(HWND hWnd,UINT msg,
                          WPARAM wParam,LPARAM lParam)
{
        POINT mousepos;

        GetCursorPos (&mousepos);

        switch(msg)
        {
              case WM_PAINT :
                    if (hWnd == eKopfWindow)
                    {
                             SetListEWindow (0);
                             display_form (eKopfWindow, &lKopfform, 0, 0);
//                             SetListEWindow (1);
                    }
                    if (hWnd == eFussWindow)
                    {
                             display_form (eFussWindow, &lFussform, 0, 0);
                    }
                    if (hWnd == InsWindow)
                    {
                             SetListEWindow (0);
                             display_form (InsWindow, &insub, 0, 0);
                             display_form (InsWindow, &insform, 0, 0);
//                             SetListEWindow (1);
                    }
                    break;
              case WM_LBUTTONDOWN :
              case WM_LBUTTONUP :
              case WM_MOUSEMOVE :
                    return TRUE;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

void DrawRowNoteIcon (HDC hdc, RECT *rect, int idx)
{
	if (eWindow != GetActiveWindow ()) return;;


	if (eWindow != NULL &&NoteIcon != NULL)
	{
		if (atol (aufparr[idx].lsp_txt) != 0)
		{
			DrawIcon (hdc,rect->left + 10, rect->top, NoteIcon);
			DrawNote = TRUE;
		}
		
	}
}

void DrawNoteIcon (HDC hdc)
{
	if (eWindow != GetActiveWindow ()) return;;
	
	if (eWindow != NULL && NoteIcon != NULL)
	{
		if (atol (aufps.lsp_txt) != 0)
		{
			DrawIcon (hdc,10, 50, NoteIcon);
			DrawNote = TRUE;
		}
		
	}
}


void DrawNoteIcon ()
{
	if (!ListeAktiv) return;
	RECT rect;
	rect.top = 0 + 50;
	rect.left = 10;
	rect.bottom = 32 + 50;
	rect.right = 32 + 10;
	InvalidateRect (Getlbox (), &rect, TRUE);
}



void EnterListe (void)
/**
Listenerfassung
**/
{
        RECT rect;
        RECT frmrect;
        int x, y, cx, cy;
        HCURSOR oldcursor;
        static long ls_alt = 0;
        HWND phWnd;
        HWND shListBox;
        HWND slbox;
        struct LMENUE smenue;
		TEXTMETRIC tm;


        if (TestQs (1) == FALSE)
        {
            return;
        }

        memcpy (&aufps, &aufp_null, sizeof (aufps));
        memcpy (&aufparr[0], &aufps, sizeof (aufps));

		commitwork ();
		beginwork ();
        if (strcmp (we_kopf.lief_rech_nr, "                ") <= 0) return;
        if (strcmp (we_kopf.lief, "                ") <= 0) return;

        if (LiefEnterAktiv)
        {
   	        SetTextMetrics (hMainWindow, &tm, &lKopfFont);
            lKopfform.mask[0].feld = "Lieferschein";
            lKopfform.mask[0].length = 0;
            lKopfform.mask[1].feld = lief_nr;
            lKopfform.mask[1].pos[1] = 160;
			lKopfform.mask[2].pos[1] = (short)  160 + 18 * (short) tm.tmAveCharWidth;
			lKopfform.mask[3].pos[1] = (short)  lKopfform.mask[2].pos[1] + 4 *
				                       (short)  tm.tmAveCharWidth;
        }
        else
        {
            lKopfform.mask[0].feld = "Auftrag";
            lKopfform.mask[0].length = 0;
            lKopfform.mask[1].feld = auftrag_nr;
            lKopfform.mask[1].pos[1] = 100;
        }

        GetMenue (&smenue);
        MenTextSave (&smenue);
        shListBox = GethListBox ();
        slbox = Getlbox ();

        ActivateColButton (eFussWindow, &lFussform, 2, 0, 0);
		if (cfg_ohne_preis == 1)
		{
                  ActivateColButton (eFussWindow, &lFussform, 0, -1, 0);
                  set_fkt (NULL, 7);
		}
		else
		{
                  ActivateColButton (eFussWindow, &lFussform, 0, 0, 0);
                  set_fkt (PreisInput, 7);
		}

        if (AufKopfWindow)
        {
                   phWnd = AufKopfWindow;
        }
        else
        {
                   phWnd = LogoWindow;
        }
        SetActiveWindow (phWnd);
        SetAktivWindow (phWnd);
        oldcursor = SetCursor (LoadCursor (NULL, IDC_WAIT));
        LeseWePos ();

        RegisterNumEnter ();
        RegisterListe ();
        GetWindowRect (hMainWindow, &rect);
        GetFrmRectEx (&testform, testform.font, &frmrect);

        x = rect.left + 5;
        y = rect.top + 60;
//        cx = frmrect.right + 15;
//        cy = rect.bottom - rect.top - 200;
        cx = (int) (double) ((double) frmrect.right + 15 * scrfx);
        cy = (int) (double) ((double) rect.bottom - rect.top - 200 * scrfy);


        SetAktivWindow (phWnd);
        set_fkt (doQsPos, 2);
        set_fkt (domess, 3);
        set_fkt (endlist, 5);
        set_fkt (doinsert, 6);
        set_fkt (HbkInput, 8);
        set_fkt (dodelete, 9);
        set_fkt (SysChoise, 11);
        set_fkt (SetKomplett, 12);
        SetDblClck (IsDblClck, 0);
        ListeAktiv = 1;
//        SetListEWindow (1);
        SetListFont (TRUE);
        spezfont (testform.font);
        eKopfWindow = CreateWindow ("ListKopf",
                                    "",
                                    WS_POPUP | WS_DLGFRAME,
                                    x, y - 35,
                                    (int) ((double) cx + 24 * scrfx),
									(int) ((double) 36 * scrfy),
                                    phWnd,
                                    NULL,
                                    hMainInst,
                                    NULL);

        eWindow = OpenListWindowEnEx (x, y, cx, cy);

        GetWindowRect (eWindow, &rect);
        eFussWindow = CreateWindow ("ListKopf",
                                    "",
                                    WS_POPUP | WS_DLGFRAME,
                                    x, rect.bottom - 2,
                                    (int) ((double) cx + 24 * scrfx),
									(int) ((double) 50 * scrfy),
                                    phWnd,
                                    NULL,
                                    hMainInst,
                                    NULL);
        ShowWindow (eKopfWindow, SW_SHOWNORMAL);
        ShowWindow (eFussWindow, SW_SHOWNORMAL);
        ElistVl (&testvl);
        ElistUb (&testub);
	    SetListRowProc (DrawRowNoteIcon);
        ActivateColButton (BackWindow3, &MainButton2, 3, 1, 1);
		if (aufpanz == 0)
		{
                   ActivateColButton (BackWindow3, &MainButton2, 2, -1, 1);
		}
/*
        if (WiegeTyp == STANDARD)
		{
                   ActivateColButton (BackWindow3, &MainButton2, 0, -1, 1);
		}
*/

        ShowElist ((char *) aufparr,
                    aufpanz,
                  (char *) &aufps,
                  (int) sizeof (struct AUFP_S),
                  &testform);
        SetCursor (oldcursor);
        EnterElist (eWindow, (char *) aufparr,
                             aufpanz,
                             (char *) &aufps,
                             (int) sizeof (struct AUFP_S),
                             &testform);
         NewPosi ();
         CloseEWindow (eWindow);
         CloseControls (&lKopfform);
         CloseControls (&lFussform);
         if (testub.mask[0].attribut & BUTTON)
         {
                    CloseControls (&testub);
         }
         DestroyWindow (eFussWindow);
         DestroyWindow (eKopfWindow);
         set_fkt (NULL, 2);
         set_fkt (NULL, 3);
         set_fkt (NULL, 5);
         set_fkt (NULL, 6);
         set_fkt (NULL, 7);
         set_fkt (NULL, 8);
         set_fkt (NULL, 9);
         set_fkt (NULL, 11);
         set_fkt (NULL, 12);
         SetListFont (FALSE);
         ListeAktiv = 0;
         ActivateColButton (BackWindow3, &MainButton2, 3, -1, 1);
	     SetListRowProc (NULL);
         SetDblClck (NULL, 1);
//         if (mit_trans)  commitwork ();
         SetMenue (&smenue);
         SethListBox (shListBox);
         Setlbox (slbox);
         set_fkt (menfunc2, 6);
 		 commitwork ();
}


/** Auswahl ueber Auftraege                                 */

struct AUF_S
{
        char best_blg [10];
        char lief [10];
        char adr_krz [18];
        char best_term [12];
        char lief_term [12];
        char bearb_stat [3];
};

struct AUF_S aufs, aufs_null;
struct AUF_S aufsarr [AUFMAX];
int aufanz = 0;
int aufidx = 0;

static int aufsort ();
static int kunsort ();
static int kunnsort ();
static int datsort ();
static int kdatsort ();
static int statsort ();

static field _aufform [] =
{
        aufs.best_blg,          9, 1, 0, 0, 0, "%8d", READONLY, 0, 0, 0,
        aufs.lief,          9, 1, 0, 10,0, "%8d", READONLY, 0, 0, 0,
        aufs.adr_krz,    17, 1, 0, 21,0, "",    READONLY, 0, 0, 0,
        aufs.best_term,    9, 1, 0, 40,0, "dd.mm.yy", READONLY, 0, 0, 0,
        aufs.lief_term,     9, 1, 0, 49,0, "dd.mm.yy", READONLY, 0, 0, 0,
        aufs.bearb_stat,      2, 1, 0, 58, 0, "%1d", READONLY, 0, 0, 0
};


static form aufform = { 6, 0, 0,_aufform, 0, 0, 0, 0, &ListFont};

/*
static field _aufub [] = {
"Auftrag",            9, 1, 0, 0, 0, "", DISPLAYONLY, 0, 0, 0,
"Kunden-Nr",         10, 1, 0,10, 0, "", DISPLAYONLY, 0, 0, 0,
"Kunden-Name",       17, 1, 0,21, 0, "", DISPLAYONLY, 0, 0, 0,
"Lieferdatum",       11, 1, 0,40, 0, "", DISPLAYONLY, 0, 0, 0,
"S",                  1, 1, 0,57, 0, "", DISPLAYONLY, 0, 0, 0
};
*/

static field _aufub [] = {
"Best.Nr",            9, 1, 0, 0, 0, "", BUTTON, 0, aufsort,  102,
"Lieferant",         11, 1, 0, 9, 0, "", BUTTON, 0, kunsort,  103,
"Name",              20, 1, 0,20, 0, "", BUTTON, 0, kunnsort, 104,
"B-Datum",            9, 1, 0,40, 0, "", BUTTON, 0, datsort,  105,
"L-Datum",            9, 1, 0,49, 0, "", BUTTON, 0, kdatsort,  105,
"A",                  2, 1, 0,58, 0, "", BUTTON, 0, statsort, 106,
" ",                  4, 1, 0,60, 0, "", BUTTON, 0, 0, 0,
};

static form aufub = { 6, 0, 0,_aufub, 0, 0, 0, 0, &ListFont};

static field _aufvl [] =
{      "1",                  1,  0, 0,  9, 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 20 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 40 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 49 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 58 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 60 , 0, "", NORMAL, 0, 0, 0,
};
static form aufvl = {6, 0, 0,_aufvl, 0, 0, 0, 0, &ListFont};


static int faufsort = 1;
static int fkunsort = 1;
static int fkunnsort = 1;
static int fdatsort = 1;
static int fkdatsort = 1;
static int fstatsort = 1;

static int aufsort0 (const void *elem1, const void *elem2)
{
	        struct AUF_S *el1;
	        struct AUF_S *el2;

		  el1 = (struct AUF_S *) elem1;
		  el2 = (struct AUF_S *) elem2;
	        return ((int) (atol(el1->best_blg) - atol (el2->best_blg)) *
				                                  faufsort);
}


static int aufsort (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufsarr, aufanz, sizeof (struct AUF_S),
				   aufsort0);
		  faufsort *= -1;
              ShowNewElist ((char *) aufsarr,
                             aufanz,
                            (int) sizeof (struct AUF_S));
		  return 0;
}

static int kunsort0 (const void *elem1, const void *elem2)
{
	        struct AUF_S *el1;
	        struct AUF_S *el2;

		  el1 = (struct AUF_S *) elem1;
		  el2 = (struct AUF_S *) elem2;
	        return ((int) (atol(el1->lief) - atol (el2->lief)) *
		                                  fkunsort);
}

static int kunsort (void)
/**
Nach Artikelnummer sortieren.
**/
{
	      qsort (aufsarr, aufanz, sizeof (struct AUF_S),
				    kunsort0);
		fkunsort *= -1;
            ShowNewElist ((char *) aufsarr,
                             aufanz,
                            (int) sizeof (struct AUF_S));
		return 0;
}


static int kunnsort0 (const void *elem1, const void *elem2)
{
	        struct AUF_S *el1;
	        struct AUF_S *el2;

		  el1 = (struct AUF_S *) elem1;
		  el2 = (struct AUF_S *) elem2;
	        return ((int) strcmp(el1->adr_krz, el2->adr_krz) *
				                                  fkunnsort);
}

static int kunnsort (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufsarr, aufanz, sizeof (struct AUF_S),
				   kunnsort0);
 		  fkunnsort *= -1;
              ShowNewElist ((char *) aufsarr,
                             aufanz,
                            (int) sizeof (struct AUF_S));
		  return 0;
}


static int datsort0 (const void *elem1, const void *elem2)
{
	        struct AUF_S *el1;
	        struct AUF_S *el2;
              long dat1;
              long dat2;

		  el1 = (struct AUF_S *) elem1;
		  el2 = (struct AUF_S *) elem2;
              dat1 = dasc_to_long (el1->best_term);
              dat2 = dasc_to_long (el2->best_term);
              if (dat1 > dat2)
              {
                            return  (1 * fdatsort);
              }
              else
              if (dat1 < dat2)
              {
                            return  (-1 * fdatsort);
              }
              return 0;
}

static int datsort (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufsarr, aufanz, sizeof (struct AUF_S),
				   datsort0);
		  fdatsort *= -1;
              ShowNewElist ((char *) aufsarr,
                             aufanz,
                            (int) sizeof (struct AUF_S));
		  return 0;
}
static int kdatsort0 (const void *elem1, const void *elem2)
{
	        struct AUF_S *el1;
	        struct AUF_S *el2;
              long dat1;
              long dat2;

		  el1 = (struct AUF_S *) elem1;
		  el2 = (struct AUF_S *) elem2;
              dat1 = dasc_to_long (el1->lief_term);
              dat2 = dasc_to_long (el2->lief_term);
              if (dat1 > dat2)
              {
                            return  (1 * fkdatsort);
              }
              else
              if (dat1 < dat2)
              {
                            return  (-1 * fkdatsort);
              }
              return 0;
}

static int kdatsort (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufsarr, aufanz, sizeof (struct AUF_S),
				   datsort0);
		  fkdatsort *= -1;
              ShowNewElist ((char *) aufsarr,
                             aufanz,
                            (int) sizeof (struct AUF_S));
		  return 0;
}


static int statsort0 (const void *elem1, const void *elem2)
{
	        struct AUF_S *el1;
	        struct AUF_S *el2;

		  el1 = (struct AUF_S *) elem1;
		  el2 = (struct AUF_S *) elem2;
	        return ((int) (atoi(el1->bearb_stat) - atoi (el2->bearb_stat)) *
		                                  fstatsort);
}

static int statsort (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufsarr, aufanz, sizeof (struct AUF_S),
				   statsort0);
		  fstatsort *= -1;
              ShowNewElist ((char *) aufsarr,
                             aufanz,
                            (int) sizeof (struct AUF_S));
		  return 0;
}


void IsAwClck (int idx)
/**
Reaktion auf Doppelclick in der Liste.
**/
{
        aufidx = idx;
		{
            break_list ();
		}
return;
}


static int DbAuswahl (short cursor,void (*fillub) (char *),
                      void (*fillvalues) (char *))
/**
Auswahl ueber Auftraege.
**/
{
        RECT rect;
        RECT frmrect;
        int x, y, cx, cy;
        static long ls_alt = 0;
        HCURSOR oldcursor;

        aufanz = 0;
        oldcursor = SetCursor (LoadCursor (NULL, IDC_WAIT));
        while (fetch_scroll (cursor, NEXT) == 0)
        {
                adr_class.lese_adr (lief.adr);
                sprintf (aufsarr[aufanz].best_blg, "%8ld", best_kopf.best_blg);
                sprintf (aufsarr[aufanz].lief, "%8s",  best_kopf.lief);
                dlong_to_asc (best_kopf.best_term, aufsarr[aufanz].best_term);
                dlong_to_asc (best_kopf.lief_term, aufsarr[aufanz].lief_term);
                strcpy  (aufsarr[aufanz].adr_krz, _adr.adr_krz);
                sprintf (aufsarr [aufanz].bearb_stat, "%1hd", best_kopf.bearb_stat);
                aufanz ++;
                if (aufanz == AUFMAX) break;
        }

        RegisterNumEnter ();
        GetWindowRect (hMainWindow, &rect);
        GetFrmRectEx (&testform, testform.font, &frmrect);

        x = rect.left + 5;
        y =  rect.top + 20;
        cx = (int) (double) ((double) frmrect.right + 15 * scrfx);
        cy = (int) (double) ((double) rect.bottom - rect.top - 120 * scrfy);

        set_fkt (endlist, 5);
        syskey = 1;
        SetDblClck (IsAwClck, 0);
        ListeAktiv = 2;
        SetAktivWindow (hMainWindow);
        SetListFont (TRUE);
        spezfont (testform.font);
        eWindow = OpenListWindowEnEx (x, y, cx, cy);
        ElistVl (&aufvl);
        ElistUb (&aufub);
        ShowElist ((char *) aufsarr,
                    aufanz,
                  (char *) &aufs,
                  (int) sizeof (struct AUF_S),
                  &aufform);
	    SetCursor (oldcursor);
		 if (aufanz > 0)
		 {
            ActivateColButton (BackWindow3,  &MainButton2, 2, 0, 1);
            ActivateColButton (eFussWindow, &lFussform,   0, 0, 1);
            ActivateColButton (eFussWindow, &lFussform,   2, 0, 1);
            ActivateColButton (eFussWindow, &lFussform,   3, 0, 1);
            ActivateColButton (eFussWindow, &lFussform,   4, 0, 1);
            ActivateColButton (eFussWindow, &lFussform,   5, 0, 1);
		 }
	    EnterElist (eWindow, (char *) aufsarr,
                             aufanz,
                             (char *) &aufs,
                             (int) sizeof (struct AUF_S),
                             &aufform);
        if (aufub.mask[0].attribut & BUTTON)
        {
                    CloseControls (&aufub);
        }
        CloseEWindow (eWindow);
        set_fkt (NULL, 5);
        SetListFont (FALSE);
        ListeAktiv = 0;
        SetDblClck (NULL, 1);
        return aufidx;
}

/**

  Auftragskopf anzeigen und Eingabefelder waehlen.

**/

mfont AufKopfFontT = {"Courier New", 200, 0, 1, BLACKCOL,
                                                RGB (0, 255, 255),
                                                0,
                                                NULL};

mfont AufKopfFontE = {"Courier New", 200, 0, 1,
                                                RGB (0, 0, 0),
                                                RGB (255, 255, 255),
                                                0,
                                                NULL};


mfont AufKopfFontTD = {"Courier New", 150, 0, 1, BLACKCOL,
                                                RGB (0, 255, 255),
                                                0,
                                                NULL};

mfont AufKopfFontED = {"Courier New", 150, 0, 1,
                                                RGB (0, 0, 0),
                                                RGB (255, 255, 255),
                                                0,
                                                NULL};


static char KundeFiliale [14] = {"Kunden-Nr   :"};
static int ReadAuftrag (void);
static int AufAuswahl (void);
static int BreakAuswahl (void);

static field _AufKopfT[] = {
"Auftrag-Nr  :",     12, 0, 1, 2, 0, "", DISPLAYONLY, 0, 0, 0};

form AufKopfT = {1, 0, 0, _AufKopfT, 0, 0, 0, 0, &AufKopfFontT};

static field _AufKopfT_new[] = {
"Auftrag-Nr  :",     12, 0, 1, 2, 0, "", WTRANSPARENT, 0, 0, 0};


static field _AufKopfTD[] = {
KundeFiliale,        13, 0, 4, 2, 0, "", DISPLAYONLY, 0, 0, 0,
kun_krz1,            16, 0, 4,28, 0, "", DISPLAYONLY,        0, 0, 0,
"Lieferdatum :",     13, 0, 5, 2, 0, "", DISPLAYONLY, 0, 0, 0,
"Komm.-Datum :",     13, 0, 6, 2, 0, "", DISPLAYONLY, 0, 0, 0,
"Lieferzeit  :",     13, 0, 7, 2, 0, "", DISPLAYONLY, 0, 0, 0,
"Status      :",     13, 0, 8, 2, 0, "", DISPLAYONLY, 0, 0, 0,
 ls_stat_txt,        16, 0, 8,28, 0, "", DISPLAYONLY,        0, 0, 0
};

static field _AufKopfTD_new[] = {
KundeFiliale,        13, 0, 4, 2, 0, "", WTRANSPARENT, 0, 0, 0,
kun_krz1,            16, 0, 4,28, 0, "", WTRANSPARENT, 0, 0, 0,
"Lieferdatum :",     13, 0, 5, 2, 0, "", WTRANSPARENT, 0, 0, 0,
"Komm.-Datum :",     13, 0, 6, 2, 0, "", WTRANSPARENT, 0, 0, 0,
"Lieferzeit  :",     13, 0, 7, 2, 0, "", WTRANSPARENT, 0, 0, 0,
"Status      :",     13, 0, 8, 2, 0, "", WTRANSPARENT, 0, 0, 0,
 ls_stat_txt,        16, 0, 8,28, 0, "", WTRANSPARENT, 0, 0, 0,
};

form AufKopfTD = {7,0, 0, _AufKopfTD, 0, 0, 0, 0, &AufKopfFontTD};


static field _AufKopfE[] = {
  auftrag_nr,           9, 0, 1,18, 0, "%8d", EDIT,        0, ReadAuftrag, 0};

form AufKopfE = {1, 0, 0, _AufKopfE, 0, 0, 0, 0, &AufKopfFontE};


static field _AufKopfED[] = {
  kun_nr,              20, 0, 4,18, 0, "%8d", EDIT,        0, 0, 0,
  lieferdatum,         20, 0, 5,18, 0, "dd.mm.yyyy",
                                              EDIT,        0, 0, 0,
  komm_dat,            20, 0, 6,18, 0, "dd.mm.yyyy",
                                              EDIT,        0, 0, 0,
  lieferzeit,          20, 0, 7,18, 0, "",
                                              EDIT,        0, 0, 0,
  ls_stat,             20, 0, 8,18, 0, "",
                                              EDIT,        0, BreakAuswahl, 0
};

form AufKopfED = {5, 0, 0, _AufKopfED, 0, 0, 0, 0, &AufKopfFontED};


// LOOK&Feel 18.11.2010 RoW
void NewAufKopfAttributes ()
{
	AufKopfT.mask = _AufKopfT_new;
	AufKopfTD.mask = _AufKopfTD_new;
}

static field _AufKopfQuery[] = {
  auftrag_nr,           9, 0, 1,18, 0, "", EDIT,        0, 0, 0,
  kun_nr,               9, 0, 4,18, 0, "", EDIT,        0, 0, 0,
  lieferdatum,         11, 0, 5,18, 0, "",
                                           EDIT,        0, 0, 0,
  komm_dat,            11, 0, 6,18, 0, "",
                                           EDIT,        0, 0, 0,
  lieferzeit,           6, 0, 7,18, 0, "",
                                              EDIT,        0, 0, 0,
  ls_stat,              2, 0, 8,18, 0, "",
                                              EDIT,        0, 0, 0
};

static form AufKopfQuery = {6, 0, 0, _AufKopfQuery, 0, 0, 0, 0, 0};
static char *AufKopfnamen[] = {"auf", "kun", "lieferdat", "komm_dat",
                                     "lieferzeit",
                                     "ls_stat"};

static int break_kopf_enter = 0;
static int aspos = 0;
static int asend = 4;

int BreakAuswahl (void)
/**
Auswahl ueber Auftraege.
**/
{
        break_kopf_enter = 1;
        return 0;
}

/************
class CTestNewAuf
{
public :

	static char qmdn_nr[];
	static char qfil_nr[];
	static char qbest_blg[];
	static char qlief_nr[];
	static char qbest_term[];
	static char qlief_term[];
	static char qbearb_stat[];


	static field _AufKopfQuery0[];

	static form AufKopfQuery0;
	static char *AufKopfnamen0[];

	CQuery query;
    long ls;
	long auf;
	long lief;
    long best_term;
	long lief_term;
    short bearb_stat;
    short wieg_kompl;
    long tou;
	short teil_smt;
    char kommname[13];
	short auf_art;
	int cursor;

	CVector SelTab;
	CVector NewTab;
	Text Query;


	CTestNewAuf ();
	~CTestNewAuf ();
    void CloneQuery ();
	int NewAuswahl ();
    BOOL NotInAuf (void *);
    void Run ();
//    BOOL TestTou (void);
//    BOOL TestKommissionierer (void);
//    BOOL TestSmt (void);
};

char CTestNewAuf::qmdn_nr [5];
char CTestNewAuf::qfil_nr [5];
char CTestNewAuf::qbest_blg [22];
char CTestNewAuf::qlief_nr [22];
char CTestNewAuf::qbest_term  [22];
char CTestNewAuf::qlief_term [22];
char CTestNewAuf::qbearb_stat [22];

field CTestNewAuf::_AufKopfQuery0[] = {
  qmdn_nr,               5, 0, 1, 18,0, "", EDIT,        0, 0, 0,
  qfil_nr,               5, 0, 1, 18,0, "", EDIT,        0, 0, 0,
  qbest_blg,           9, 0, 1,18, 0, "", EDIT,        0, 0, 0,
  qlief_nr,               9, 0, 4,18, 0, "", EDIT,        0, 0, 0,
  qbest_term,         11, 0, 5,18, 0, "",
                                           EDIT,        0, 0, 0,
  qlief_term,            11, 0, 6,18, 0, "",
                                           EDIT,        0, 0, 0,
  qbearb_stat,              2, 0, 8,18, 0, "",
                                              EDIT,        0, 0, 0,
  qbest_blg,                 9, 0, 1,18, 0, "",
                                              EDIT,     0, 0, 0,
};

form CTestNewAuf::AufKopfQuery0 = {8, 0, 0, _AufKopfQuery0, 0, 0, 0, 0, 0};
char *CTestNewAuf::AufKopfnamen0[] = {"mdn", "fil", "best_blg", "lief", "best_term", "lief_term",
                                     "bearb_stat"};



  **********/
int AufAuswahl (void)
/**
Auswahl ueber Auftraege.
**/
{
	//LuD 100908 Bestellung darf erst aus Liste raus, wenn der entsprechende Wareneingang schon abgeschlossen ist
	// Lud 200709  we_status abfrage von 3 auf 1 
        SetDbAwSpez (DbAuswahl);
        break_kopf_enter = 1;
        if (druck_zwang)
        {
             sprintf (sqlstring ,
		     "select best_blg, best_term, lief_term,best_kopf.lief,lief.adr,best_kopf.freifeld1 from best_kopf,lief  "
			                        "where best_kopf.mdn = %d "
									"and best_kopf.fil = %d "
									"and best_kopf.lief = lief.lief  "
									"(best_kopf.lief_term <= today +3 and best_kopf.lief_term > today -3) "
									"and ( "
									"best_kopf.bearb_stat = 2 "
									"or best_kopf.best_blg in (select koresp_best0 from we_kopf where "
									"mdn = %d and fil = %d and we_dat >= today -2 and we_status < 1) "
									") "
									" ", Mandant,Filiale,Mandant,Filiale);
        }
        else
        {
             sprintf (sqlstring ,
		     "select best_blg, best_term, lief_term,best_kopf.lief,lief.adr,best_kopf.freifeld1 from best_kopf,lief "
			                        "where best_kopf.mdn = %d "
									"and best_kopf.fil = %d "
									"and best_kopf.lief = lief.lief and "
									"(best_kopf.lief_term <= today +3  and best_kopf.lief_term > today -3) "
									"and ( "
									"best_kopf.bearb_stat < 3 "
									"or best_kopf.best_blg in (select koresp_best0 from we_kopf where "
									"mdn = %d and fil = %d and we_dat >= today -2 and we_status < 1) "
									") "
									" ", Mandant,Filiale,Mandant,Filiale);
        }
		if (Abteilungen) //120908 nur best.Abteilungen
		{
			sprintf(sqlstring,"%s and best_kopf.abteilung in (%s) ",sqlstring,Abteilungen );
		}
		sprintf(sqlstring,"%s%s",sqlstring,"order by best_kopf.best_blg");


//        ReadQuery (&AufKopfQuery0, AufKopfnamen0);
        if (ls_class.Showlsk (sqlstring) == 0 && stopauswahl == 0)
        {
					 stopauswahl = 1;
			         strcpy(bestellhinweis,"");

					 if (syskey == KEY5) 
					 {
	                     sprintf (bestell_nr, "%ld", 0);
						 return 0;
					 }
                     sprintf (bestell_nr, "%ld", best_kopf.best_blg);
					 we_kopf.mdn = Mandant;
					 we_kopf.fil = Filiale;
				     sprintf(bestellhinweis,"Bestellbeleg: %ld   %s",best_kopf.best_blg,best_kopf.freifeld1);
					 if (ReadWEBestell(best_kopf.best_blg) == 0)  //100908 direct in Liste, wenn WE schon da
					 {
						 sprintf(lief_nr,"%s",we_kopf.lief_rech_nr);
						 sprintf(lieferant,"%s",we_kopf.lief);
						 LiefEnterAktiv = 1;
						 menfunc2 ();
				         LiefEnterAktiv = 0;
						 return 0;
					 }
                     sprintf (lieferant, "%s", best_kopf.lief);
                     sprintf (we_kopf.lief, "%s", best_kopf.lief);
					GenLief_rech_nr(0);
					 sprintf(we_kopf.lief_rech_nr,"%s",lief_nr);
					short di = 1;
					 while (TestWE() == 0)  //140509
					 {
						GenLief_rech_nr(di);
						strcpy(we_kopf.lief_rech_nr,lief_nr);
						 sprintf(we_kopf.lief_rech_nr,"%s",lief_nr);
						 di ++;
					 }
					 menfunc3();
					 /*
				     lsk.ls = atol (aufs.ls);
                     if (aufanz)
                     {
                               LockAuf = 0;
                               if (ReadAuftragLs () == -1)
							   {
								    syskey = KEY5;
									stopauswahl = TRUE;
									SetDbAwSpez (NULL);
									return 0;
							   }
                               LockAuf = 1;
                     }
                     else
                     {
                               InitAufKopf ();
                     }
                     SetDbAwSpez (NULL);
					 if (EnterTyp == AUSWAHL)
					 {
						   SmtChanged = FALSE;
					 }
					 menfunc2 ();
					 */
                     return 0;
        }
        InitAufKopf ();
        SetDbAwSpez (NULL);
  	    if (EnterTyp == AUSWAHL)
		{
//				SmtChanged = FALSE;
		}
        return 0;

}

void FillAufForm (void)
/**
Datenbankfelder in Maske uebertragn.
**/
{
/*
             kun_class.lese_kun (1, 0, lsk.kun);
             sprintf (kun_nr, "%ld", lsk.kun);
             dlong_to_asc (lsk.lieferdat, lieferdatum);
             dlong_to_asc (lsk.komm_dat, komm_dat);
             strcpy (lieferzeit, lsk.lieferzeit);
             sprintf (ls_stat, "%hd", lsk.ls_stat);
             strcpy (kun_krz1, kun.kun_krz1);
             ptab_class.lese_ptab ("ls_stat", ls_stat);
             strcpy (ls_stat_txt, ptabn.ptbezk);
*/
}

BOOL Lock_Lsk (void)
/**
Lieferschein sperren.
**/
{
    short sqlm;
    extern short sql_mode;

    if (LockAuf == 0)
        return TRUE;
    sqlm = sql_mode;
    sql_mode = 1;
    dsqlstatus = WeKopfClass.dblock ();
    if (dsqlstatus < 0)
    {
        print_messG (2, "Satz ist gesperrt");
        sql_mode = sqlm;
        AufLock = 1;
        ActivateColButton (BackWindow2, &MainButton, 1, -1, 1);
        ActivateColButton (BackWindow2, &MainButton, 2, -1, 1);
        ActivateColButton (BackWindow2, &MainButton, 4, -1, 1);
        return FALSE;
    }
    AufLock = 0;
    ActivateColButton (BackWindow2, &MainButton, 1, 1, 1);
    ActivateColButton (BackWindow2, &MainButton, 4, 1, 1);
    sql_mode = sqlm;
    return TRUE;
}

BOOL Lock_Lsp (void)
/**
Lieferschein sperren.
**/
{
/*
    short sqlm;
    extern short sql_mode;

    sqlm = sql_mode;
    sql_mode = 1;
    dsqlstatus = ls_class.lock_lsp (1, 0, lsp.ls, ratod (aufps.a),
                                                  atol (aufps.pnr));
    if (dsqlstatus < 0)
    {
               print_messG (2, "Position wird von einem anderen "
                               "Benutzer bearbeitet");
               return FALSE;
    }
    sql_mode = sqlm;
*/
    return TRUE;
}


int ReadAuftrag (void)
/**
Lieferschein nach Auftragsnummer lesen.
**/
{
/*
            if (atol (auftrag_nr) <= 0l) return (-1);

            dsqlstatus = ls_class.lese_lsk_auf (1, 0, atol (auftrag_nr));
            if (dsqlstatus)
            {
                   ActivateColButton (BackWindow2, &MainButton, 4, -1, 1);
                   set_fkt (NULL, 6);
                   print_messG (0, "Auftrag %s nicht gefunden", auftrag_nr);
                   return -1;
            }

//            if (Lock_Lsk () == FALSE) return 0;

            ActivateColButton (BackWindow2, &MainButton, 4, 1, 1);
            set_fkt (menfunc2, 6);
            FillAufForm ();
*/
            return 0;
}


void KonvTextPos (mfont *Font1,  mfont *Font2, int *z, int *s)
/**
Textposition von einer Schriftgroesse zur anderen konvertieren.
**/
{
         TEXTMETRIC tm1, tm2;
         HDC hdc;
         HFONT hFont, oldfont;
         int ax, ay;

         spezfont (Font1);
         hFont = SetWindowFont (hMainWindow);
         hdc = GetDC (hMainWindow);
         oldfont = SelectObject (hdc, hFont);
         GetTextMetrics (hdc, &tm1);
         DeleteObject (SelectObject (hdc, oldfont));
         ReleaseDC (hMainWindow, hdc);
         DeleteObject (hFont);

         spezfont (Font2);
         hFont = SetWindowFont (hMainWindow);
         hdc = GetDC (hMainWindow);
         oldfont = SelectObject (hdc, hFont);
         GetTextMetrics (hdc, &tm2);
         DeleteObject (SelectObject (hdc, oldfont));
         ReleaseDC (hMainWindow, hdc);
         DeleteObject (hFont);

         ax = *s;
         ay = *z;

         ax = ax * tm1.tmAveCharWidth / tm2.tmAveCharWidth;
         ay = ay * tm1.tmHeight / tm2.tmHeight;

         *s = ax;
         *z = ay;
}

void KonvTextAbsPos (mfont *Font, int *cy, int *cx)
/**
Absolute Textposition fuer eine Schriftgroesse ermitteln.
**/
{
         TEXTMETRIC tm;
         HDC hdc;
         HFONT hFont, oldfont;

         spezfont (Font);
         hFont = SetWindowFont (hMainWindow);
         hdc = GetDC (hMainWindow);
         oldfont = SelectObject (hdc, hFont);
         GetTextMetrics (hdc, &tm);
         DeleteObject (SelectObject (hdc, oldfont));
         ReleaseDC (hMainWindow, hdc);
         DeleteObject (hFont);
         *cx *= tm.tmAveCharWidth;
         *cy *= tm.tmHeight;
}

void KonvTextStrLen (mfont *Font, char *Text, int *cx)
/**
Absolute Textposition fuer eine Schriftgroesse ermitteln.
**/
{
         TEXTMETRIC tm;
         HDC hdc;
         HFONT hFont, oldfont;

         spezfont (Font);
         hFont = SetWindowFont (hMainWindow);
         hdc = GetDC (hMainWindow);
         oldfont = SelectObject (hdc, hFont);
         GetTextMetrics (hdc, &tm);
		 SIZE size;
         GetTextExtentPoint (hdc, Text, strlen (Text), &size);
         DelFont (SelectObject (hdc, oldfont));
         ReleaseDC (hMainWindow, hdc);
//         DeleteObject (hFont);
         *cx = size.cx;
}

void GetTextPos (mfont *Font, int *cy, int *cx)
/**
Zeichenorientierte Textposition fuer eine Schriftgroesse ermitteln.
**/
{
         TEXTMETRIC tm;
         HDC hdc;
         HFONT hFont, oldfont;

         spezfont (Font);
         hFont = SetWindowFont (hMainWindow);
         hdc = GetDC (hMainWindow);
         oldfont = SelectObject (hdc, hFont);
         GetTextMetrics (hdc, &tm);
         DeleteObject (SelectObject (hdc, oldfont));
         ReleaseDC (hMainWindow, hdc);
         DeleteObject (hFont);

         *cx /= tm.tmAveCharWidth;
         *cy /= tm.tmHeight;
}



void FontTextLen (mfont *Font, char *text, int *cx, int *cy)
/**
Laenge und Hoehe eines Textes fuer den gewaehlten Font holen.
**/
{
         TEXTMETRIC tm;
         HDC hdc;
         HFONT hFont, oldfont;
         SIZE size;

         spezfont (Font);
         hFont = SetWindowFont (hMainWindow);
         hdc = GetDC (hMainWindow);
         oldfont = SelectObject (hdc, hFont);
         GetTextMetrics (hdc, &tm);
         GetTextExtentPoint (hdc, text, *cx, &size);
         DeleteObject (SelectObject (hdc, oldfont));
         ReleaseDC (hMainWindow, hdc);
         DeleteObject (hFont);

         *cx = size.cx;
         *cy = tm.tmHeight;
}

void SetAufFocus (int pos)
/**
Focus auf Eingabefelder AuftragKopfsetzen.
**/
{
        switch (pos)
        {
               case 0 :
                      SetFocus (AufKopfE.mask[0].feldid);
                      SendMessage (AufKopfE.mask[0].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
                      break;
               default :
                      SetFocus (AufKopfED.mask[pos - 1].feldid);
                      SendMessage (AufKopfED.mask[pos -1].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
                      break;
        }
}

void PrintKLines ()
/**
Icons am Bildschirm anzeigen.
**/
{
         RECT rect;
         PAINTSTRUCT ps;
         HDC hdc;
         HPEN hPen;
         int x, y;

         GetClientRect (AufKopfWindow, &rect);

         hdc = BeginPaint (AufKopfWindow, &ps);
// LOOK&Feel 18.11.2010 RoW
		 DRAWTRANSPARENTS (AufKopfWindow, hdc);

         hPen = CreatePen (PS_SOLID, 2, BLACKCOL);
         SelectObject (hdc, hPen);
         x = rect.right - 4;
         y = 10;
         MoveToEx (hdc, 2, y, NULL);
         LineTo (hdc, x, y);
         DeleteObject (hPen);

         hPen = CreatePen (PS_SOLID, 2, GRAYCOL);
         SelectObject (hdc, hPen);
         MoveToEx (hdc, 2, y - 1, NULL);
         LineTo (hdc, x, y - 2);
         DeleteObject (hPen);

         hPen = CreatePen (PS_SOLID, 2, BLACKCOL);
         SelectObject (hdc, hPen);
         x = rect.right - 4;
         y = 150;
         MoveToEx (hdc, 2, y, NULL);
         LineTo (hdc, x, y);
         DeleteObject (hPen);

         hPen = CreatePen (PS_SOLID, 2, GRAYCOL);
         SelectObject (hdc, hPen);
         MoveToEx (hdc, 2, y - 1, NULL);
         LineTo (hdc, x, y - 2);
         DeleteObject (hPen);
         EndPaint (WiegWindow, &ps);
         return;

         hPen = CreatePen (PS_SOLID, 2, BLACKCOL);
         SelectObject (hdc, hPen);
         x = rect.right - 4;
         y = 300;
         MoveToEx (hdc, 2, y, NULL);
         LineTo (hdc, x, y);
         DeleteObject (hPen);

         hPen = CreatePen (PS_SOLID, 2, GRAYCOL);
         SelectObject (hdc, hPen);
         MoveToEx (hdc, 2, y - 1, NULL);
         LineTo (hdc, x, y - 2);
         DeleteObject (hPen);

         EndPaint (WiegWindow, &ps);
}

static int NoDisplay;


LONG FAR PASCAL AufKopfProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        switch(msg)
        {

              case WM_PAINT :
				    if (QmAktiv) break;
                    if (LiefEnterAktiv)
                    {
                                DisplayLiefKopf ();
                                PrintKLines ();
                                if (MessWindow == 0)
                                {
                                        LiefFocus ();
                                }
                    }
                    else if (KopfEnterAktiv)
                    {
                                display_form (AufKopfWindow, &AufKopfT, 0, 0);
                                display_form (AufKopfWindow, &AufKopfTD, 0, 0);
                                if (AufKopfE.mask[0].feldid)
                                {
                                      display_form (AufKopfWindow, &AufKopfE, 0, 0);
                                }
                                if (AufKopfE.mask[0].feldid || asend == 0)
                                {
                                      display_form (AufKopfWindow, &AufKopfED, 0, 0);
                                }
                                PrintKLines ();
                                if (MessWindow == 0)
                                {
                                         SetAufFocus (aspos);
                                }
                    }
                    break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}


void RegisterKopfEnter (void)
/**
Fenster fuer Auftragskopf registrieren
**/
{
        static int registered = 0;
        WNDCLASS wc;

        if (registered == 0)
        {
                   wc.style =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                               | CS_OWNDC;
                   wc.cbClsExtra    =  0;
                   wc.cbWndExtra    =  0;
                   wc.hInstance     =  hMainInst;
                   wc.hIcon         =  LoadIcon (hMainInst, "FITICON");
                   wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                   wc.lpszMenuName  =  "";
                   wc.lpfnWndProc   =  AufKopfProc;
                   wc.lpszMenuName  =  "";
                   wc.hbrBackground =  CreateSolidBrush (RGB (0, 255, 255));
                   wc.lpszClassName =  "AufKopf";
                   RegisterClass(&wc);
                   registered = 1;
        }
}

void GetEditText (void)
/**
Text aus Editierfeld holen.
**/
{
        switch (aspos)
        {
                 case 0 :
                    GetWindowText (AufKopfE.mask [0].feldid,
                                   AufKopfE.mask [0].feld,
                                   AufKopfE.mask[0].length);
                    break;
                 case 1 :
                    GetWindowText (AufKopfED.mask [0].feldid,
                                   AufKopfED.mask [0].feld,
                                   AufKopfED.mask[0].length);
                    break;
                 case 2 :
                    GetWindowText (AufKopfED.mask [1].feldid,
                                   AufKopfED.mask [1].feld,
                                   AufKopfED.mask[1].length);
                    break;
                 case 3 :
                    GetWindowText (AufKopfED.mask [2].feldid,
                                   AufKopfED.mask [2].feld,
                                   AufKopfED.mask[2].length);
                    break;
                 case 4 :
                    GetWindowText (AufKopfED.mask [3].feldid,
                                   AufKopfED.mask [3].feld,
                                   AufKopfED.mask[3].length);

                 case 5 :
                    GetWindowText (AufKopfED.mask [4].feldid,
                                   AufKopfED.mask [4].feld,
                                   AufKopfED.mask[4].length);
                    break;
        }
}

int MenKey (int taste)
{
    int i;
    ColButton *ColBut;
    char *pos;

    for (i = 0; i < MainButton.fieldanz; i ++)
    {
           ColBut = (ColButton *) MainButton.mask[i].feld;
           if (ColBut->aktivate < 0) continue;
           if (ColBut->text1)
           {
                 if ((pos = strchr (ColBut->text1, '&')) &&
                    ((UCHAR) toupper (*(pos + 1)) == taste))
                 {
                             if (MainButton.mask[i].after)
                             {
                                 (*MainButton.mask[i].after) ();
                             }
                             return TRUE;
                  }
           }
    }
    return FALSE;
}

int IsKopfMessage (MSG *msg)
/**
HotKey Testen.
**/
{
         POINT mousepos;

         switch (msg->message)
         {
              case WM_KEYDOWN :
              {
                     switch (msg->wParam)
                     {
                            case VK_F5 :
                                    break_kopf_enter = 1;
                                    return TRUE;
                            case VK_F6 :
                                    syskey = KEY6;
                                    testkeys ();
                                    return TRUE;
                            case VK_F12 :
								    if (TestOK () == FALSE) return TRUE;
                                    syskey = KEY12;
                                    testkeys ();
                                    return TRUE;
                            case VK_DOWN :
                            case VK_RETURN :
                                    GetEditText ();
                                    if (asend == 0 && AufKopfE.mask[0].after)
                                    {
                                         if ((*AufKopfE.mask[0].after) () == -1)
                                         {
                                                  return TRUE;
                                         }
                                    }
                                    if (aspos > 0 &&
                                        AufKopfED.mask [aspos - 1].after)
                                    {
                                      (*AufKopfED.mask [aspos - 1].after) ();
                                    }
                                    aspos ++;
                                    if (aspos > asend) aspos = 0;
                                    display_form (AufKopfWindow,
                                                  &AufKopfE, 0, 0);
                                    display_form (AufKopfWindow,
                                                  &AufKopfED, 0, 0);
                                    SetAufFocus (aspos);
                                    return TRUE;
                            case VK_UP :
                                    GetEditText ();
                                    aspos --;
                                    if (aspos < 0) aspos = asend;
                                    display_form (AufKopfWindow,
                                                  &AufKopfE, 0, 0);
                                    display_form (AufKopfWindow,
                                                  &AufKopfED, 0, 0);
                                    SetAufFocus (aspos);
                                    return TRUE;
                            case VK_TAB :
                                    GetEditText ();
                                    if (GetKeyState (VK_SHIFT) < 0)
                                    {
                                            aspos --;
                                            if (aspos < 0) aspos = 4;
                                            SetAufFocus (aspos);
                                     }
                                     else
                                     {
                                            aspos ++;
                                            if (aspos > 4) aspos = 0;
                                            SetAufFocus (aspos);
                                     }
                                     display_form (AufKopfWindow,
                                                 &AufKopfE, 0, 0);
                                     display_form (AufKopfWindow,
                                                 &AufKopfED, 0, 0);
                                      return TRUE;
                            default :
                                    return MenKey (msg->wParam);
                     }
                     break;
              }
              case WM_LBUTTONDOWN :
              {
                    GetCursorPos (&mousepos);

                    if (MouseinWindow (AufKopfE.mask[0].feldid, &mousepos))
                    {
                          EnterNumBox (AufKopfWindow, "  Auftrags-Nr  ",
                                       auftrag_nr, 9, AufKopfE.mask[0].picture);
                          if (asend == 0 && AufKopfE.mask[0].after)
                          {
                                if ((*AufKopfE.mask[0].after) () == -1)
                                {
                                       return TRUE;
                                }
                          }
                          display_form (AufKopfWindow, &AufKopfE, 0, 0);
						  PostMessage (msg->hwnd, WM_KEYDOWN, VK_RETURN, 0l);
                          return TRUE;
                    }
                    else if (MouseinWindow (AufKopfED.mask[0].feldid, &mousepos))
                    {
                          if (asend == 0) return TRUE;
                          EnterNumBox (AufKopfWindow,"  Kunden-Nr  ",
                                          kun_nr, 9, AufKopfED.mask[0].picture);
                          display_form (AufKopfWindow, &AufKopfE, 0, 0);
						  PostMessage (msg->hwnd, WM_KEYDOWN, VK_RETURN, 0l);
                          return TRUE;
                    }
                    else if (MouseinWindow (AufKopfED.mask[1].feldid, &mousepos))
                    {
                         if (asend == 0) return TRUE;
                         EnterNumBox (AufKopfWindow,"  Lieferdatum  ",
                                      lieferdatum, 11,
//                                      "dd.mm.yyyy");
                                            AufKopfED.mask[1].picture);
                         display_form (AufKopfWindow, &AufKopfE, 0, 0);
					     PostMessage (msg->hwnd, WM_KEYDOWN, VK_RETURN, 0l);
                         return TRUE;
                    }
                    else if (MouseinWindow (AufKopfED.mask[2].feldid, &mousepos))
                    {
                         if (asend == 0) return TRUE;
                         EnterNumBox (AufKopfWindow,"  Komm.-Datum  ",
                                      komm_dat   , 11,
//                                      "dd.mm.yyyy");
                                          AufKopfED.mask[2].picture);
                         display_form (AufKopfWindow, &AufKopfE, 0, 0);
					     PostMessage (msg->hwnd, WM_KEYDOWN, VK_RETURN, 0l);
                         return TRUE;
                    }
                    else if (MouseinWindow (AufKopfED.mask[3].feldid, &mousepos))
                    {
                         if (asend == 0) return TRUE;
                         EnterNumBox (AufKopfWindow,"  Lieferzeit  ",
                                      lieferzeit, 6,
                                            AufKopfED.mask[3].picture);
                         display_form (AufKopfWindow, &AufKopfE, 0, 0);
					     PostMessage (msg->hwnd, WM_KEYDOWN, VK_RETURN, 0l);
                         return TRUE;
                    }
                    else if (MouseinWindow (AufKopfED.mask[4].feldid, &mousepos))
                    {
                         if (asend == 0) return TRUE;
                         EnterNumBox (AufKopfWindow,"  Status  ",
                                         ls_stat,  2,
                                            AufKopfED.mask[4].picture);
                         display_form (AufKopfWindow, &AufKopfE, 0, 0);
					     PostMessage (msg->hwnd, WM_KEYDOWN, VK_RETURN, 0l);
                         return TRUE;
                    }
             }
          }

          return FALSE;
}

void KonvAufKopf (void)
/**
Spaltenposition von AufKopfED anpassen.
**/
{
        static int KonvOK = 0;
        int z, s;
        int i;

        if (KonvOK) return;

        KonvOK = 1;
        for (i = 0; i < AufKopfED.fieldanz; i ++)
        {
                    s = AufKopfED.mask[i].pos [1];
                    KonvTextPos (AufKopfE.font,
                                 AufKopfED.font,
                                 &z, &s);
                    AufKopfED.mask[i].pos [1] = s;
        }

        s = AufKopfTD.mask[1].pos [1];
        KonvTextPos (AufKopfE.font,
                    AufKopfED.font,
                    &z, &s);
        AufKopfTD.mask[1].pos [1] = s;

        s = AufKopfTD.mask[5].pos [1];
        KonvTextPos (AufKopfE.font,
                    AufKopfED.font,
                    &z, &s);
        AufKopfTD.mask[5].pos [1] = s;
}

void InitAufKopf (void)
/**
Auftrag-Kopf initialisieren.
**/
{
        int i;

        AufKopfE.mask[0].feld[0] = (char) 0;

        for (i = 0; i < AufKopfED.fieldanz; i ++)
        {
                      AufKopfED.mask[i].feld[0] = (char) 0;
        }
        kun_krz1[0] = (char) 0;
        ls_stat_txt[0] = (char) 0;
}


void MainBuInactiv (void)
/**
Alle Button auf Inaktiv setzen.
**/
{
          ActivateColButton (BackWindow2, &MainButton, 0, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 2, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 3, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 4, -1, 1);
}

void MainBuActiv (void)
/**
Alle Button auf Inaktiv setzen.
**/
{
          ActivateColButton (BackWindow2, &MainButton, 0, 1, 1);
          ActivateColButton (BackWindow2, &MainButton, 1, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 2, 1, 1);
          ActivateColButton (BackWindow2, &MainButton, 3, 1, 1);
          ActivateColButton (BackWindow2, &MainButton, 4, -1, 1);
          set_fkt (NULL, 6);
}


void AuftragKopf (int eType)
/**
Auftragskopf bearbeiten.
**/
{
        RECT rect;
        MSG msg;
        int x, y, cx, cy;

        if (KopfEnterAktiv) return;
        if (LiefEnterAktiv) return;

        MainBuInactiv ();

        if (eType == ENTER)
        {
            beginwork ();
        }
        else
        {
                   InitAufKopf ();
        }

        if (atoi (auftrag_nr))
        {
                  if (ReadAuftrag () == -1) return;;
        }
        CloseLiefKopf ();
        if (eType == AUSWAHL)
        {
                     KopfEnterAktiv = 2;
                     asend = 5;
//                     InitAufKopf ();
                     AufKopfE.mask[0].picture = "";
                     AufKopfED.mask[0].picture = "";
                     AufKopfED.mask[1].picture = "";
                     AufKopfED.mask[2].picture = "";

                     AufKopfED.mask[0].attribut = EDIT;
                     AufKopfED.mask[1].attribut = EDIT;
                     AufKopfED.mask[2].attribut = EDIT;
                     AufKopfED.mask[3].attribut = EDIT;
                     AufKopfED.mask[4].attribut = EDIT;
                     AufKopfTD.mask[1].attribut = REMOVED;
                     AufKopfTD.mask[6].attribut = REMOVED;

                     AufKopfE.mask[0].length = 20;
                     AufKopfED.mask[0].length = 20;
                     AufKopfED.mask[1].length = 20;
                     AufKopfED.mask[2].length = 20;
                     AufKopfED.mask[3].length = 20;
                     AufKopfED.mask[4].length = 20;
                     strcpy (ls_stat, "< 3");
                     strcpy (komm_dat, KomDatum);
                     AufAuswahl ();
					 strcpy(bestellhinweis,"");
                     KopfEnterAktiv = 0;
                     MainBuActiv ();
                     return;
        }

        if (eType == ENTER)
        {
                     asend = 0;
                     AufKopfE.mask[0].picture  = "%8d";
                     AufKopfED.mask[0].picture = "%8d";
                     AufKopfED.mask[1].picture = "dd.mm.yyyy";
                     AufKopfED.mask[2].picture = "dd.mm.yyyy";

                     AufKopfED.mask[0].attribut = READONLY;
                     AufKopfED.mask[1].attribut = READONLY;
                     AufKopfED.mask[2].attribut = READONLY;
                     AufKopfED.mask[3].attribut = READONLY;
                     AufKopfED.mask[4].attribut = READONLY;
                     AufKopfTD.mask[1].attribut = DISPLAYONLY;
                     AufKopfTD.mask[6].attribut = DISPLAYONLY;

                     AufKopfE.mask[0].length = 9;
                     AufKopfED.mask[0].length = 9;
                     AufKopfED.mask[1].length = 11;
                     AufKopfED.mask[2].length = 11;
                     AufKopfED.mask[3].length = 6;
                     AufKopfED.mask[4].length = 2;
        }
        else if (eType == SUCHEN)
        {
                     asend = 5;
                     set_fkt (BreakAuswahl, 12);
                     InitAufKopf ();
                     AufKopfE.mask[0].picture = "";
                     AufKopfED.mask[0].picture = "";
                     AufKopfED.mask[1].picture = "";
                     AufKopfED.mask[2].picture = "";

                     AufKopfED.mask[0].attribut = EDIT;
                     AufKopfED.mask[1].attribut = EDIT;
                     AufKopfED.mask[2].attribut = EDIT;
                     AufKopfED.mask[3].attribut = EDIT;
                     AufKopfED.mask[4].attribut = EDIT;
                     AufKopfTD.mask[1].attribut = REMOVED;
                     AufKopfTD.mask[6].attribut = REMOVED;

                     AufKopfE.mask[0].length = 20;
                     AufKopfED.mask[0].length = 20;
                     AufKopfED.mask[1].length = 20;
                     AufKopfED.mask[2].length = 20;
                     AufKopfED.mask[3].length = 20;
                     AufKopfED.mask[4].length = 20;
        }

        aspos = 0;
        RegisterKopfEnter ();
        GetWindowRect (LogoWindow, &rect);
        x = rect.left;
        y = rect.top;
        cx = rect.right - rect.left;
        cy = rect.bottom - rect.top;

        KonvAufKopf ();

        KopfEnterAktiv = 1;
        if (AufKopfWindow == 0)
        {
                  AufKopfWindow  = CreateWindowEx (
                              0,
                              "AufKopf",
                              "",
                              WS_POPUP,
                              x, y,
                              cx, cy,
                              hMainWindow,
                              NULL,
                              hMainInst,
                              NULL);
        }

        if (AufKopfWindow == NULL)
        {
                    KopfEnterAktiv = 0;
                    MainBuActiv ();
                    return;
        }
		InitWeKopfBackground ();
        ShowWindow (AufKopfWindow, SW_SHOW);
        UpdateWindow (AufKopfWindow);
        create_enter_form (AufKopfWindow, &AufKopfE, 0, 0);
        if (asend > 0)
        {
                  create_enter_form (AufKopfWindow, &AufKopfED, 0, 0);
        }
        if (eType == ENTER)
        {
               set_fkt (menfunc2, 6);
        }
        SetAufFocus (aspos);
        break_kopf_enter = 0;
        while (GetMessage (&msg, NULL, 0, 0))
        {
              if (IsKopfMessage (&msg));
              else
              {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
              }
              if (break_kopf_enter) break;
        }
        CloseControls (&AufKopfT);
        CloseControls (&AufKopfE);
        CloseControls (&AufKopfTD);
        CloseControls (&AufKopfED);
        if (eType == ENTER)
        {
               set_fkt (NULL, 6);
        }

        if (eType == ENTER)
        {
                     DestroyWindow (AufKopfWindow);
                     AufKopfWindow = NULL;
                     commitwork ();
//                     InitAufKopf ();
        }

        if (eType == SUCHEN)
        {
                     set_fkt (NULL, 12);
                     DestroyWindow (AufKopfWindow);
                     AufKopfWindow = NULL;
                     AufAuswahl ();
        }
        KopfEnterAktiv = 0;
        MainBuActiv ();
}

/**

Wiegen

**/


#define BUTARA  502
static int break_wieg = 0;

HWND WiegKg;
HWND WiegCtr;

static char WiegText [30] = {"Kiloware"};
static char WiegMe [12] = {"14.150"};


mfont WiegKgFont  = {"Courier New", 300, 0, 1, BLACKCOL,
                                               WHITECOL,
                                               1,
                                               NULL};

mfont WiegKgFontMe = {"Courier New", 300, 0, 1, BLACKCOL,
                                                RGB (0, 255, 255),
                                                1,
                                                NULL};


mfont WiegKgFontT  = {"Courier New", 150, 0, 1, WHITECOL,
                                                BLUECOL,
                                                1,
                                                NULL};

mfont WiegBuFontE  = {"Courier New", 200, 0, 1, BLACKCOL,
                                                REDCOL,
                                                1,
                                                NULL};

mfont WiegBuFontW  = {"Courier New", 150, 0, 1, BLACKCOL,
                                                GREENCOL,
                                                1,
                                                NULL};

mfont EtiFont  = {"Courier New", 120, 0, 1, WHITECOL,
                                            BLUECOL,
                                            1,
                                            NULL};

mfont EtiSpezFont  = {"Courier New", 120, 0, 1, WHITECOL,
                                                BLUECOL,
                                                1,
                                                NULL};

mfont LgrSpezFont  = {"Courier New", 120, 0, 1, WHITECOL,
                                                BLUECOL,
                                                1,
                                                NULL};

mfont ZusatzFont  = {"Courier New", 100, 0, 1, WHITECOL,
                                                BLUECOL,
                                                1,
                                                NULL};

ColButton WieOK = { "OK", -1, -1,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     BLACKCOL,
                     GREENCOL,
                     2};


ColButton WieEnde = {"Zur�ck", -1, -1,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     BLACKCOL,
                     REDCOL,
                     2};

ColButton WieWie = {"Wiegen", -1, -1,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     BLACKCOL,
                     GREENCOL,
                     2};

ColButton WieHand = {"Hand", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      BLACKCOL,
                      YELLOWCOL,
                      2};

ColButton WiePreis = {"Preise", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      BLACKCOL,
                      YELLOWCOL,
                      2};

ColButton WieTara = {"Tara", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      // BLACKCOL,
                      // RGB (0, 255, 255),
                      WHITECOL,
                      RGB (0, 0, 255),
                      2};

ColButton AutoTara = {"Auto-", -1,15,
                      "Tara ", -1,40,
                      "aus",   -1,65,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       // BLACKCOL,
                       // RGB (0, 255, 255),
                       WHITECOL,
                       RGB (0, 0, 255),
                       3};

ColButton DelTara = { "Tara   ", -1, 25,
                      "L�schen", -1, 50,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      RGB (0, 0, 255),
                      2};

ColButton FestTara = {"Fest", -1, 25,
                      "Tara", -1, 50,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      RGB (0, 0, 255),
                      2};


ColButton SetTara1 = { "Tara  ", -1, 15,
                       "�ber- ", -1, 40,
                       "nehmen", -1, 65,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       WHITECOL,
                       RGB (0, 0, 255),
                       2};

ColButton SetTara2 = { "Start-",  -1, 15,
                       "wiegen",  -1,  50,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       WHITECOL,
                       RGB (0, 0, 255),
                       2};

ColButton SetTara;

ColButton HandTara = {"Hand", -1, 25,
                      "Tara", -1, 50,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      RGB (0, 0, 255),
                      2};


ColButton Etikett = {"Etikett", -1, -1,
                     "", 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      2};

ColButton SumEti  = {"Summen", -1,   5,
                     "Etikett",-1 , 30,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      2};

ColButton EtiSpez = {"Etiketten", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      2};

ColButton LgrSpez = {Lagertxt, -1, -1,
                      Buchartikeltxt, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      2};

ColButton Zusatz = {"Zusatz", -1, 10,
                     "daten", -1, 30,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     WHITECOL,
                     BLUECOL,
                     2};

static int current_wieg;
static long last_anz;
static int CtrMode;
static int wiegend = 9;
static int etiend = 6;
static int dowiegen (void);
static int doWOK (void);
static int doWF5 (void); //130509
static int dohand (void);
static int dotara (void);
static int dogebinde (void);
static int dopreis (void);
static int doauto (void);
static int deltara (void);
static int handtara (void);
static int festtara (void);
static int settara (void);
static int settara0 (void);
static int closetara (void);
static int doetikett (void);
static int doetispez (void);
static int dolgrspez (void);
static int dozusatz (void);
static int dosumeti (void);
static int PosTexte ();
static int autoan = 0;
static int autopos = 1;
static char *anaus[] = {"aus", "ein"};
static char chargen_nr [18];
static int WiegY;
static double akt_tara = (double) 0.0;
static char last_charge [21] = {""};

static int bsize = 100;
static int babs = 100;

static int bsizek = 60;

static field _WieButton[] = {
(char *) &WieTara,          bsize, bsize, -1, 275, 0, "", COLBUTTON, 0,
                                                      dotara, BUTARA,
(char *) &WiePreis,         bsize, bsize, -1, 275, 0, "", COLBUTTON, 0,
                                                      dopreis, 0,
(char *) &WieHand,          bsize, bsize, -1, 15, 0, "", COLBUTTON, 0,
                                                      dohand, 0,
(char *) &WieWie,           bsize, bsize, -1, 145, 0, "", COLBUTTON, 0,
                                                      dowiegen, 0,
(char *) &WieOK,            bsize, bsize, -1, 275, 0, "", COLBUTTON, 0,
                                                      doWOK, 0,
(char *) &WieEnde,          bsize, bsize, -1, 405, 0, "", COLBUTTON, 0,
                                                      doWF5, 0};

static form WieButton = {6, 0, 0, _WieButton, 0, 0, 0, 0, &WiegBuFontW};


static BOOL WithEti = FALSE;;
static int eti_nr;
static char krz_txt [18];

static BOOL WithSumEti = FALSE;;
static int sum_eti_nr;
static char sum_krz_txt [18];

static double eti_netto;
static double eti_brutto;
static double eti_tara;
static double eti_stk;

static double sum_eti_netto;
static double sum_eti_brutto;
static double sum_eti_tara;
static double sum_eti_stk;

static void SetEti (double netto, double brutto, double tara)
/**
Wete fuer Etikettendruck setzen.
**/
{
	      eti_netto  = netto;
          eti_brutto = brutto;
		  eti_tara   = tara;
	      sum_eti_netto  += netto;
          sum_eti_brutto += brutto;
		  sum_eti_tara   += tara;
}


static void SetEtiStk (double stk)
/**
Wete fuer Etikettendruck setzen.
**/
{
	      eti_stk  = stk;
	      sum_eti_stk  += stk;
}

static field _EtiButton[] = {
(char *) &Etikett,          bsize, bsizek, 30, 700, 0, "", COLBUTTON, 0,
                                                      doetikett, 0,
(char *) &SumEti,           bsize, bsizek,100, 700, 0, "", COLBUTTON, 0,
                                                      dosumeti, 0};

form EtiButton = {2, 0, 0, _EtiButton, 0, 0, 0, 0, &EtiFont};

static field _EtiSpezButton[] = {
(char *) &EtiSpez,          bsize, bsizek, 300, 700, 0, "", COLBUTTON, 0,
                                                      doetispez, 0
};

form EtiSpezButton = {1, 0, 0, _EtiSpezButton, 0, 0, 0, 0, &EtiSpezFont};


static field _LgrSpezButton[] = {
(char *) &LgrSpez,          bsize + 120, bsizek, 200, 620, 0, "", COLBUTTON, 0,
                                                      menfuncALgr, 0
};

form LgrSpezButton = {1, 0, 0, _LgrSpezButton, 0, 0, 0, 0, &LgrSpezFont};



static field _ZusatzButton[] = {
(char *) &Zusatz,          bsize, bsizek, 300, 700, 0, "", COLBUTTON, 0,
                                                      dozusatz, 0
};

form ZusatzButton = {1, 0, 0, _ZusatzButton, 0, 0, 0, 0, &ZusatzFont};

static int bsize2 = 100;
static int babs2 = 100;

static field _WieButtonT[] = {
(char *) &AutoTara,         bsize, bsize, -1, 275, 0, "", COLBUTTON, 0,
                                                      doauto, 0,
(char *) &DelTara,          bsize2, bsize2, -1, 145, 0, "", COLBUTTON, 0,
                                                      deltara, 0,
(char *) &FestTara,         bsize2, bsize2, -1, 15, 0, "", COLBUTTON, 0,
                                                      festtara, 0,
(char *) &HandTara,         bsize2, bsize2, -1, 405, 0, "", COLBUTTON, 0,
                                                      handtara, 0,
(char *) &SetTara,          bsize2, bsize2, -1, 15, 0, "", COLBUTTON, 0,
                                                      settara0, 0,
(char *) &WieEnde,          bsize2, bsize2, -1, 405, 0, "", COLBUTTON, 0,
                                                      0, VK_F5};

form WieButtonT = {6, 0, 0, _WieButtonT, 0, 0, 0, 0, &WiegBuFontW};

static field _WiegKgT [] = {
WiegText,           0, 0, 10, -1, 0, "", DISPLAYONLY, 0, 0, 0};

static form WiegKgT = {1, 0, 0, _WiegKgT, 0, 0, 0, 0, &WiegKgFontT};

static field _WiegKgE [] = {
WiegMe,             0, 0, 0, -1, 0, "%11.3f", READONLY, 0, 0, 0};

static form WiegKgE = {1, 0, 0, _WiegKgE, 0, 0, 0, 0, &WiegKgFont};

static field _WiegKgMe [] = {
"KG",                   3, 1, 0, 0, 0, "", DISPLAYONLY, 0, 0, 0};

static form WiegKgMe = {1, 0, 0, _WiegKgMe, 0, 0, 0, 0, &WiegKgFontMe};


mfont ArtWFont = {"Courier New", 150, 0, 1, RGB (0, 0, 0),
                                      RGB (255, 255, 255),
                                      1,
                                      NULL};
mfont ArtWFontT = {"Courier New", 120, 0, 1, RGB (0, 0, 0),
                                        RGB (0, 255, 255),
                                        1,
                                        NULL};

mfont StartWFont = {"Courier New", 170, 0, 1, 
//                                              RGB (0, 0, 0),
//                                              RGB (0, 255, 255),
                                              BLUECOL,
                                              WHITECOL,
                                              1,
                                              NULL};
/*
mfont ArtWCFont = {"Courier New", 100, 0, 0, RGB (0, 0, 0),
                                      RGB (0, 255, 255),
                                      1,
                                      NULL};
                                      */

mfont ArtWCFont = {"Courier New", 150, 0, 1, RGB (0, 0, 0),
                                      RGB (255, 255, 255),
                                      1,
                                      NULL};

mfont ArtWCFontT = {"Courier New", 120, 0, 1, RGB (0, 0, 0),
                                        RGB (0, 255, 255),
                                        1,
                                        NULL};

mfont ftarafont  = {"Courier New", 150, 0, 1, RGB (0, 0, 0),
                                              RGB (255, 255, 255),
                                              1,
                                              NULL};

mfont ftaratfont  = {"Courier New", 150, 0, 1, RGB (0, 0, 0),
                                               RGB (0, 255, 255),
                                               1,
                                               NULL};
mfont zustxtfont  = {"Courier New", 150, 0, 1, BLUECOL,
                                              RGB (0, 255, 255),
                                              1,
                                              NULL};

COLORREF TexteForgroundCol = RGB (255, 255, 255);
COLORREF TexteBackgroundCol = RGB (0, 0, 255);

mfont ftextefont  = {"Courier New", 150, 0, 1, TexteForgroundCol,
                                                  TexteBackgroundCol,
												  1,
                                                  NULL};


ColButton STexte = {"Texte", -1, -1,
						NULL, 0, 0,
						NULL, 0, 0,
						NULL, 0, -1,
						NULL, 0, 0,
						TexteForgroundCol,
                        TexteBackgroundCol,
						0};

static field _artwconstT [] =
{
"Artikel",                 9, 1, 3, 3,  0, "", DISPLAYONLY,    0, 0, 0,
"Artikelbezeichnung",     18, 1, 3,14,  0, "", DISPLAYONLY,    0, 0, 0,
"Lieferschein",            7, 1, 1, 3,  0, "", DISPLAYONLY,    0, 0, 0,
"Lieferant",               9, 1, 1,14,  0, "", DISPLAYONLY,    0, 0, 0};

static form artwconstT = {4, 0, 0,_artwconstT, 0, 0, 0, 0, &ArtWCFontT};

static field _startwieg[] = 
{
    "Startwiegung",           13, 1, 8, 3, 0,  "", READONLY,    0, 0, 0
};

static form startwieg = {1, 0, 0, _startwieg, 0, 0, 0, 0, &StartWFont};


static char summe1 [40];
static char summe2 [40];
static char summe3 [40];
static char zsumme1 [40];
static char zsumme2 [40];
static char zsumme3 [40];

static char vorgabe1[40] = {"100"};
static char veinh1[10] = {"kg"};
static char vorgabe2[40] = {"100"};
static char veinh2[10] = {"kg"};

BOOL StartWiegen = FALSE;
BOOL Entnahmehand = FALSE;

static double EntnahmeStartGew = 0.0;
static double EntnahmeActGew   = 0.0;
static double EntnahmeTara     = 0.0;

static char wiegzahl [40];

static field _artwconst [] =
{
        aufps.a,            9, 1,  4, 3,  0, "%8.0f",  READONLY, 0, 0, 0,
        aufps.a_bz1,       21, 1,  4, 14, 0, "",       READONLY, 0, 0, 0,
        auftrag_nr,         9, 1,  2,  3, 0, "%8d",    READONLY , 0, 0, 0,
        lief_krz1,          21, 1,  2, 14, 0, "",       READONLY , 0, 0, 0
};


static form artwconst = {4, 0, 0,_artwconst, 0, 0, 0, 0, &ArtWCFont};



static form artwformT;
static form artwform;


char *ChargenNr = "Chargen-Nr";
char *IdentNr   = "Ident-Nr";


// Masken fuer Wiegen

static field _artwformTW [] =
{
ChargenNr,                10, 1, 5, 3,  0, "", DISPLAYONLY,    0, 0, 0,
"Restmenge",              10, 1, 1,37,  0, "", DISPLAYONLY,    0, 0, 0,
"Liefermenge",            11, 1, 5,37,  0, "", DISPLAYONLY,    0, 0, 0,
aufps.lief_me_bz,         10, 1, 7,37,  0, "", DISPLAYONLY,    0, 0, 0,
"Wiegungen",              10, 1, 1,37,  0, "", DISPLAYONLY,    0, 0, 0,
"Bestellmenge",           13, 1, 5,23,  0, "", DISPLAYONLY,    0, 0, 0,
aufps.auf_me_bz,          10, 1, 7,23,  0, "", DISPLAYONLY,    0, 0, 0,
"Sollmenge",              11, 1, 3,37,  0, "", DISPLAYONLY,    0, 0, 0,
"Box",	                  10, 1, 1,49,  0, "", DISPLAYONLY,    0, 0, 0,
"Anzahl",                 10, 1, 5,49,  0, "", DISPLAYONLY,    0, 0, 0,
};

static form artwformTW = {10, 0, 0,_artwformTW, 0, 0, 0, 0, &ArtWFontT};


static field _artwformW [] =
{
        aufps.ls_charge,   19, 0, 6,  3, 0, "",       EDIT,     0, 0, 0,
        auf_me,            10, 1, 2, 37, 0, "%9.3f",  READONLY , 0, 0, 0,
        wiegzahl,          10, 1, 2, 37, 0, "%9.0f",  READONLY, 0, 0, 0,
        aufps.lief_me,     10, 1, 6, 37, 0, "%9.3f",  READONLY, 0, 0, 0,
        aufps.auf_me,      12, 1, 6, 23, 0, "%11.3f", READONLY, 0, 0, 0,
        aufps.auf_me_vgl,  10, 1, 4, 37, 0, "%9.3f",  READONLY, 0, 0, 0,
        aufps.ident_nr,    10, 1, 2, 49, 0, "",       READONLY, 0, 0, 0,
        aufps.anz_einh,    10, 1, 6, 49, 0, "%9.0f",  READONLY, 0, 0, 0,
        zsumme1,            4, 1, 4, 61, 0, "%3d",    REMOVED, 0, 0, 0,
        zsumme2,            4, 1, 6, 61, 0, "%3d",    REMOVED, 0, 0, 0,
};

static form artwformW = {10, 0, 0,_artwformW, 0, 0, 0, 0, &ArtWFont};

static char ActEntnahmeGew [20] = {"0.0"};

static field _fEntnahmeGewTxt [] =
{
"Aktuelles Gewicht",       20, 1, 1,49,  0, "", DISPLAYONLY,    0, 0, 0,
};

static form fEntnahmeGewTxt = {1, 0, 0,_fEntnahmeGewTxt, 0, 0, 0, 0, &ArtWFontT};

static field _fEntnahmeGew [] =
{
ActEntnahmeGew,            10, 1, 2,49,  0, "%9.3f", READONLY,    0, 0, 0,
};

static form fEntnahmeGew = {1, 0, 0,_fEntnahmeGew, 0, 0, 0, 0, &ArtWFont};

// Masken fuer Auszeichnung

static field _artwformTP [] =
{
"Chargen-Nr",             10, 1, 5, 3,  0, "", DISPLAYONLY,    0, 0, 0,
"Vorgaben",                10, 1, 1,37,  0, "", DISPLAYONLY,    0, 0, 0,
veinh2,                    9, 1, 5,37,  0, "", DISPLAYONLY,    0, 0, 0,
aufps.lief_me_bz,         10, 1, 7,37,  0, "", REMOVED,        0, 0, 0,
"Liefermenge",            13, 1, 5,23,  0, "", DISPLAYONLY,        0, 0, 0,
aufps.lief_me_bz,         10, 1, 7,23,  0, "", DISPLAYONLY,    0, 0, 0,
aufps.auf_me_bz,          10, 1, 7,37,  0, "", DISPLAYONLY,    0, 0, 0,
veinh1,                    9, 1, 3,37,  0, "", DISPLAYONLY,    0, 0, 0,
"Summen",                  6, 1, 1,49,  0, "", DISPLAYONLY,    0, 0, 0,
"Summe2",                  6, 1, 3,49,  0, "", REMOVED,    0, 0, 0,
"Summe3",                  6, 1, 5,49,  0, "", REMOVED,    0, 0, 0,
"kg",                      6, 1, 7,49,  0, "", REMOVED,    0, 0, 0,
"St�ck",                   7, 1, 3, 61, 0, "", DISPLAYONLY , 0, 0, 0,
"Karton",                  7, 1, 5, 61, 0, "", DISPLAYONLY , 0, 0, 0,
"Palette",                 7, 1, 7, 61, 0, "", DISPLAYONLY , 0, 0, 0,
veinh1,                    9, 1, 3, 49,  0, "", DISPLAYONLY,    0, 0, 0,
veinh2,                    9, 1, 5, 49,  0, "", DISPLAYONLY,    0, 0, 0,
aufps.auf_me_bz,          10, 1, 7, 49,  0, "", DISPLAYONLY,    0, 0, 0,
};

static form artwformTP = {18, 0, 0,_artwformTP, 0, 0, 0, 0, &ArtWFontT};


static field _artwformP [] =
{
        aufps.ls_charge,   19, 0, 6,  3, 0, "",       EDIT,     0, 0, 0,
        vorgabe1,          12, 1, 2, 37, 0, "%11.3f", READONLY, 0, 0, 0,
        aufps.lief_me,     10, 1, 6, 23, 0, "%9.3f",  READONLY,  0, 0, 0,
        aufps.auf_me,      12, 1, 6, 37, 0, "%11.3f", READONLY, 0, 0, 0,
        vorgabe2,          12, 1, 4, 37, 0, "%11.3f",  READONLY, 0, 0, 0,
        summe1,            10, 1, 2, 49, 0, "%9.3f",  READONLY, 0, 0, 0,
        summe2,            10, 1, 4, 49, 0, "%9.3f",  READONLY, 0, 0, 0,
        summe3,            10, 1, 6, 49, 0, "%9.3f",  READONLY, 0, 0, 0,
        zsumme1,            4, 1, 2, 61, 0, "%3d",    READONLY, 0, 0, 0,
        zsumme2,            4, 1, 4, 61, 0, "%3d",    READONLY, 0, 0, 0,
        zsumme3,            4, 1, 6, 61, 0, "%3d",    READONLY, 0, 0, 0,
};

static form artwformP = {11, 0, 0,_artwformP, 0, 0, 0, 0, &ArtWFont};

// Ende Maske fuer Preisauszeichnung

static field _ftaratxt [] =
{
"Tara",                   5, 1,  8, 13, 0, "",  DISPLAYONLY, 0, 0, 0
};

static form ftaratxt = {1, 0, 0, _ftaratxt, 0, 0, 0, 0, &ftaratfont};

//021109
static field _sw_abzug [] =
{
aufps.txtschwundabzug,                   5, 1,  8, 23, 0, "",  DISPLAYONLY, 0, 0, 0
};
static form sw_abzug = {1, 0, 0, _sw_abzug, 0, 0, 0, 0, &zustxtfont};


static field _ftara [] =
{
        aufps.tara,        8, 1,  8, 20, 0, "%7.3f",  READONLY, 0, 0, 0
};

static form ftara = {1, 0, 0, _ftara, 0, 0, 0, 0, &ftarafont};


static field _ftexte [] =
{
 (char *) &STexte,          12, 2,  8, 40, 0, "",  COLBUTTON, 0, PosTexte, 0
};

static form ftexte = {1, 0, 0, _ftexte, 0, 0, 0, 0, &ftextefont};

void SwitchLiefAuf (void)
/**
Text fuer Autrags-Nr oder Lieferschein belegen.
**/
{
         if (LiefEnterAktiv)
         {
                   artwconstT.mask[2].feld   = "Lieferschein";
                   artwconstT.mask[2].length = 0;
                   artwconst.mask[2].feld    = lief_nr;
         }
         else
         {
                   artwconstT.mask[2].feld   = "Auftrag";
                   artwconstT.mask[2].length = 0;
                   artwconst.mask[2].feld    = auftrag_nr;
         }
}


void write_wewiepro (double brutto, double netto, double tara)
/**
In Tabelle we_wiepro schreiben.
**/
{
         char datum [12];
         char zeit  [12];
         char zeits  [12];
		 char hh [3];
		 char mm [3];
		 char ss [3];

		 sysdate (datum);
         systime (zeit);
		 //zeitformat "hhmmss LuD 27.02.2009"  
                 memcpy (hh, (char *) zeit, 2);
                 hh[2] = 0;
                 memcpy (mm, (char *) zeit + 3, 2);
                 mm[2] = 0;
                 memcpy (ss, (char *) zeit + 6, 2);
                 ss[2] = 0;
				 sprintf (zeits,"%s%s%s",hh,mm,ss);


		 we_wiepro.mdn = we_kopf.mdn;
         we_wiepro.fil = we_kopf.fil;
         we_wiepro.a   = ratod (aufps.a);
         we_wiepro.best_blg = 0;
         strcpy (we_wiepro.blg_typ, we_kopf.blg_typ);
         strcpy (we_wiepro.lief_rech_nr, we_kopf.lief_rech_nr);
         we_wiepro.lief_s = we_kopf.lief_s ;
		 we_wiepro.me = brutto;
         we_wiepro.tara = tara;
         memcpy (we_wiepro.zeit, zeits, 6);
		 we_wiepro.zeit[6] = 0;	 ;
         we_wiepro.dat = dasc_to_long (datum);
         we_wiepro.sys = sys_peri.sys;
         strcpy (we_wiepro.pers, sys_ben.pers_nam);
         strcpy (we_wiepro.erf_kz, aufps.erf_kz);
         we_wiepro.anzahl = last_anz;
         strcpy (we_wiepro.qua_kz, "0");
         strcpy (we_wiepro.lief, we_kopf.lief);
         strcpy (we_wiepro.ls_charge, aufps.ls_charge);
         strcpy (we_wiepro.ls_ident, " ");
		 WeWiePro.dbinsert ();
         we_wiepro.es_nr = 0l;
         strcpy (we_wiepro.pers, "");
}


int GetGewicht (char *datei, double *brutto, double *netto, double *tara)
/**
Gewichtswerte aus der Datei ergebnis holen.
**/
{
         FILE *fp;
         char buffer [81];

         fp = fopen (datei, "r");
         if (fp == NULL) return (-1);

/*
         if (testmode)
         {
                    disp_messG ("ergebnis ge�ffnet", 1);
         }
*/

         while (fgets (buffer, 80, fp))
         {
/*
               if (testmode)
               {
                     print_messG (1, "gelesen %s", buffer);
               }
*/
               switch (buffer [0])
               {
                        case 'B' :
                                *brutto = (double) ratod (&buffer[1]);
                                break;
                        case 'N' :
                                *netto = (double) ratod (&buffer[1]);
                                break;
                        case 'T' :
                                *tara = (double) ratod (&buffer[1]);
                                break;
                        case 'E' :
                                if (strlen (&buffer[1]) < 7)
                                {
                                    sprintf (we_wiepro.pers, "%06ld", atol (&buffer[1]));
                                    we_wiepro.es_nr = atol (&buffer[1]);
                                }
                                else
                                {
                                    sprintf (we_wiepro.pers, "%012.0lf", 
                                             ratod (&buffer[1]));
                                }
               }
         }
         fclose (fp);

         if (*netto == (double) 0)
         {
                        *netto = *brutto - *tara;
         }
         else if (*brutto == (double) 0)
         {
                        *brutto = *netto + *tara;
         }

         if (*tara == (double) 0.0)
         {
                        *tara = *brutto - *netto;
         }

         if (WiegeTyp == STANDARD)
         {
/*
               if (autoan)
               {
                       akt_tara = *brutto;
               }
               else
*/
               {
                       akt_tara = *tara;
               }
         }


         if (WiegeTyp == ENTNAHME && StartWiegen == FALSE)
         {
                   double gew = EntnahmeActGew - *netto;
                   gew -= EntnahmeTara;
                   if (gew < 0.0)
                   {
                       gew = 0.0;
                   }
                   EntnahmeActGew = *netto;
                   sprintf (ActEntnahmeGew, "%.3lf",EntnahmeActGew);
                   *netto = gew;
                   *tara  = EntnahmeTara;
                   *brutto = *netto + *tara;
         }

         if (StartWiegen == FALSE)
         {
		               write_wewiepro (*brutto, *netto, *tara);
         }

/*
         if (testmode)
         {
                    print_messG (1,"%.2lf  %.2lf %-2lf", *brutto, *netto, *tara);
         }
*/
         return 0;
}

void fnbexec (int arganz, char *argv[])
/**
**/
{
        char aufruf [256];
        int i;

        if (getenv (BWS) == NULL) return;
        if (fnbproc != NULL)
		{
                     if (fnbInstanceproc != NULL)
					 {
			              (*fnbInstanceproc) (hMainInst);
					 }
			         int ret = (*fnbproc) (arganz, argv);
					 return;
		}

        sprintf (aufruf, "%s\\fnb.exe", waadir);

        for (i = 1; i < arganz; i ++)
        {
                     strcat (aufruf, " ");
                     strcat (aufruf, argv[i]);
        }

//        WaitConsoleExec (aufruf, CREATE_NO_WINDOW);
        ProcWaitExec (aufruf, SW_SHOWMINIMIZED, -1, 0, -1, 0);
}


BOOL WaaInitOk (char *wa)
/**
Test, ob die Waage schon initialisiert wurde.
**/
{
        static char *waadirs [20];
        static int waaz = 0;
        int i;

        if (waaz == 20) return FALSE;
        clipped (wa);
        for (i = 0; i < waaz; i ++)
        {
            if (strcmp (wa, waadirs [i]) == 0)
            {
                return TRUE;
            }
        }
        waadirs [i] = (char *) malloc (strlen (wa) + 3);
        if (waadirs [i] == NULL)
        {
            return FALSE;
        }
        strcpy (waadirs[i], wa);
        if (waaz < 20) waaz ++;
        return FALSE;
}



void WaageInit (void)
/**
Waage Initialisieren.
**/
{
        static int arganz;
        static char *args[4];
		char WaaDll [512];



        if (WaaInitOk (waadir)) return;
		waaLibrary = fnbproc = NULL;
        sprintf (WaaDll, "%s\\fnb.dll", waadir);
        waaLibrary = LoadLibrary (WaaDll);
        if (waaLibrary != NULL)
		{
               fnbInstanceproc = (int (*) (void*)) GetProcAddress (waaLibrary, fnbInstancename);
               fnbproc = (int  (*) (int, char **)) GetProcAddress (waaLibrary, fnbname);
		}

        deltaraw ();
        WaaInit = 1;
        arganz = 3;
        args[0] = "Leer";
        args[1] = "0";
        args[2] = waadir;
        fnbexec (arganz, args);
}

int doWOK (void)
/**
Wiegen durchfuehren.
**/
{
	    if (ChargeZwang == 1) DoChargeZwang(); //130509
	    if (artwform.mask[0].attribut == EDIT)
		{
                 GetWindowText (artwform.mask[0].feldid,
                      artwform.mask[0].feld,
                      artwform.mask[0].length);
		}
        strcpy (aufps.s, "3");
        break_wieg = 1;
        return 0;
}
int doWF5 (void)
/**
Wiegen durchfuehren.
**/
{
	    if (ChargeZwang == 1) DoChargeZwang(); //130509
	    if (artwform.mask[0].attribut == EDIT)
		{
                 GetWindowText (artwform.mask[0].feldid,
                      artwform.mask[0].feld,
                      artwform.mask[0].length);
		}
//        strcpy (aufps.s, "3");
        break_wieg = 1;
        return 0;
}


void setautotara (BOOL an)
/**
Wiegen durchfuehren.
**/
{
	    if (an)
		{
              AutoTara.aktivate = 4;
              autoan = 1;
        }
        else
        {
              AutoTara.aktivate = 3;
              autoan = 0;
        }
        AutoTara.text3 = anaus [autoan];
}


int doauto (void)
/**
Wiegen durchfuehren.
**/
{
        if (AutoTara.aktivate == 4)
        {
              autoan = 1;
        }
        else
        {
              autoan = 0;
        }
        AutoTara.text3 = anaus [autoan];
        current_wieg = autopos;
        return 0;
}

void PressAuto ()
/**
Autotara druecken.
**/
{
       SendMessage (WieButton.mask[autopos].feldid, WM_LBUTTONDOWN, 0l, 0l);
       SendMessage (WieButton.mask[autopos].feldid, WM_LBUTTONUP, 0l, 0l);
}

int festtara (void)
/**
Wiegen durchfuehren.
**/
{
        double netto;
        double brutto;
        double tara;
        static int arganz;
        static char ftara [11];
        static char *args[5];
        char buffer [20];


        syskey = 0;
        ShowFestTara (buffer);

        if (syskey == KEY5)
        {
                  return 0;
        }


        sprintf (ftara, "%.3lf", (double) ratod (buffer));
        if (fest_tara_anz)
        {
  		        NuBlock.SetParent (LogoWindow);
                sprintf (buffer, "%d", 1);
                NuBlock.EnterCalcBox (WiegWindow,"  Anzahl ",  buffer, 5, "%4df", EntNuProc);
                if (syskey != KEY5 && atoi (buffer) > 1)
                {
                   sprintf (ftara, "%.3lf", (double) ratod (ftara) * atoi (buffer));
                }
        }

		if (fest_tara_sum || autoan)
		{
                  akt_tara = akt_tara + (double) ratod (ftara);
		}
		else
		{
                  akt_tara = (double) ratod (ftara);
		}
        sprintf (aufps.tara, "%.3lf", akt_tara);
        sprintf (ftara, "%.3lf", akt_tara);
        if (WiegeTyp == ENTNAHME)
        {
                  EntnahmeTara = akt_tara;
                  return 0;
        }
        arganz = 4;
        args[0] = "Leer";
        args[1] = "23";
        args[2] = waadir;
        args[3] = (char *) ftara;
        fnbexec (arganz, args);
        brutto = netto = tara = (double) 0.0;
        GetGewicht (ergebnis, &brutto, &netto, &tara);
        return 0;

/*
        ShowFestTara (buffer);
        sprintf (ftara, "%.3lf", (double) ratod (buffer));
        arganz = 4;
        args[0] = "Leer";
        args[1] = "23";
        args[2] = waadir;
        args[3] = (char *) ftara;
        fnbexec (arganz, args);
        brutto = netto = tara = (double) 0.0;
        GetGewicht (ergebnis, &brutto, &netto, &tara);
        sprintf (aufps.tara, "%.3lf", ratod (ftara));
        display_form (WiegKg, &WiegKgE, 0, 0);
        return 0;
*/
}

int deltaraw (void)
/**
Wiegen durchfuehren.
**/
{
        static int arganz;
        static char *args[4];

        arganz = 3;
        args[0] = "Leer";
        args[1] = "5";
        args[2] = waadir;
        fnbexec (arganz, args);
        return 0;
}

int deltara (void)
/**
Wiegen durchfuehren.
**/
{
        double netto;
        double brutto;
        double tara;
        static int arganz;
        static char *args[4];

        sprintf (aufps.tara, "%.3lf", (double) 0.0);
        if (WiegeTyp == ENTNAHME)
        {
                  EntnahmeTara = 0.0;
                  akt_tara = 0.0;
//             	  display_form (WiegWindow, &ftara, 0, 0);
//                  return 0;
        }
        arganz = 3;
        args[0] = "Leer";
        args[1] = "5";
        args[2] = waadir;
        fnbexec (arganz, args);
        brutto = netto = tara = (double) 0.0;
        GetGewicht (ergebnis, &brutto, &netto, &tara);
        akt_tara = (double) 0.0;
		display_form (WiegWindow, &ftara, 0, 0);
        return 0;

/*
        arganz = 3;
        args[0] = "Leer";
        args[1] = "5";
        args[2] = waadir;
        fnbexec (arganz, args);
        brutto = netto = tara = (double) 0.0;
        GetGewicht (ergebnis, &brutto, &netto, &tara);
        sprintf (aufps.tara, "%.3lf", (double) 0.0);
        display_form (WiegKg, &WiegKgE, 0, 0);
        akt_tara = (double) 0.0;
        return 0;
*/
}

int settara (void)
/**
Wiegen durchfuehren.
**/
{
        double netto;
        double brutto;
        double tara;
        static int arganz;
        static char *args[4];

        arganz = 3;
        args[0] = "Leer";
        args[1] = "4";
        args[2] = waadir;
        fnbexec (arganz, args);
        if (WiegeTyp == STANDARD)
        {
             Sleep (AutoSleep);
             arganz = 3;
             args[0] = "Leer";
             args[1] = "11";
             args[2] = waadir;
             fnbexec (arganz, args);
        }
        brutto = netto = tara = (double) 0.0;
        GetGewicht (ergebnis, &brutto, &netto, &tara);
		sprintf (aufps.tara, "%.3lf", akt_tara);
        if (WiegeTyp == ENTNAHME)
        {
                  EntnahmeTara = akt_tara;
        }
		display_form (WiegWindow, &ftara, 0, 0);
        return 0;

/*
        arganz = 3;
        args[0] = "Leer";
        args[1] = "4";
        args[2] = waadir;
        fnbexec (arganz, args);
        brutto = netto = tara = (double) 0.0;
        GetGewicht (ergebnis, &brutto, &netto, &tara);
        sprintf (aufps.tara, "%.3lf", tara);
        display_form (WiegKg, &WiegKgE, 0, 0);
        return 0;
*/
}

int settara0 (void)
{
        if (WiegeTyp == STANDARD)
        {
            return settara ();
        }
        StartWiegen = TRUE;
        dowiegen ();
        StartWiegen = FALSE;
        sprintf (ActEntnahmeGew, "%.3lf", EntnahmeActGew);
        return 0;
}


int setgebindetara (void)
{
	    GebTara = TRUE;
        NuBlock.NumEnterBreak ();
	    return 0;
}

int dogebindetara (char *ftara)
{
	    sprintf (ftara, "%7.3lf", (double) 0.0);
		EnterLeerPos ();
	    GebTara = FALSE;
		if (syskey == KEY5) return 0;
		GetGebGewicht (ftara);
	    return 0;
}

int handtara (void)
/**
Wiegen durchfuehren.
**/
{
        double netto;
        double brutto;
        double tara;
        static char ftara [11];
        static int arganz;
        static char *args[4];

        sprintf (ftara, "%7.3lf", (double) 0.0);
//        BuChoise.text1 = "&Gebinde";
		NuBlock.SetParent (LogoWindow);
        NuBlock.SetChoise (setgebindetara, 0, "&Gebinde");
        NuBlock.EnterCalcBox (WiegWindow,"  Tara  ",  ftara, 8, "%7.3f", EntNuProc);
        SetAktivWindow (WiegWindow);
		if (GebTara)
		{
			dogebindetara (ftara);
		}

		if (hand_tara_sum || autoan)
		{
                  akt_tara = akt_tara + (double) ratod (ftara);
		}
		else
		{
                  akt_tara = (double) ratod (ftara);
		}

        sprintf (aufps.tara, "%.3lf", akt_tara);
        sprintf (ftara, "%.3lf", akt_tara);
        if (WiegeTyp == ENTNAHME)
        {
                  EntnahmeTara = akt_tara;
                  current_wieg = 2;
                  return 0;
        }
        arganz = 4;
        args[0] = "Leer";
        args[1] = "23";
        args[2] = waadir;
        args[3] = (char *) ftara;
        fnbexec (arganz, args);
        brutto = netto = tara = (double) 0.0;
        GetGewicht (ergebnis, &brutto, &netto, &tara);
        current_wieg = 2;
        return 0;

/*
        akt_tara = akt_tara + (double) ratod (ftara);
        sprintf (ftara, "%.3lf", akt_tara);
        arganz = 4;
        args[0] = "Leer";
        args[1] = "23";
        args[2] = waadir;
        args[3] = (char *) ftara;
        fnbexec (arganz, args);
        brutto = netto = tara = (double) 0.0;
        GetGewicht (ergebnis, &brutto, &netto, &tara);
        current_wieg = 2;
        sprintf (aufps.tara, "%.3lf", akt_tara);
        display_form (WiegKg, &WiegKgE, 0, 0);
        return 0;
*/
}

static int StornoSet = 0;

int dostorno (void)
/**
SornoFlag setzen.
**/
{
       StornoSet = 1;
       PostMessage (EditForm.mask[0].feldid, WM_KEYDOWN,
                    (WPARAM) VK_RETURN, 0l);
       return 0;
}

BOOL TestAllStorno (void)
/**
Abfragem, ob die ganze Position stormiert werden soll.
**/
{
         double rest;
         double lief_me;

         if (WorkModus == AUSZEICHNEN && AktWiegPos)
         {
                 if (abfragejnG (WiegWindow,
                         "Einzelne Wiegung stornieren", "J"))
                 {
                          AuswahlWieg ();
                          rest    = (double) ratod (auf_me);
                          lief_me = (double) ratod (aufps.lief_me);
                          rest += StornoWieg;
                          lief_me -= StornoWieg;
                          sprintf (auf_me, "%.3lf", rest);
                          sprintf (aufps.lief_me, "%3lf", lief_me);
                          return FALSE;
                 }
         }

         if (abfragejnG (WiegWindow,
                         "Die gesamte Position stornieren", "N"))
         {
             return TRUE;
         }
         int idx = AuswahlWiePro ();
         if (idx == -1)
         {
             StornoSet = 0;
             return FALSE;
         }
         if (idx > wiepanz)
         {
             StornoSet = 0;
             return FALSE;
         }
         memcpy (&wiep, &wieparr[idx], sizeof (wiep));
         rest    = (double) ratod (auf_me);
         rest += ratod (wiep.brutto);
         if (WiegeTyp == ENTNAHME)
         {
                 StartWiegen = TRUE;
                 dowiegen ();
                 StartWiegen = FALSE;
                 sprintf (ActEntnahmeGew, "%.3lf", EntnahmeActGew);
          }
          lief_me = ratod (aufps.lief_me);
          lief_me -= ratod (wiep.brutto);
 	      sprintf (aufps.a_krz, "%.3lf", atoi (aufps.a_krz) - 1);
  	      strcpy (wiegzahl, aufps.a_krz);
/*
          strcpy (auf_me, aufps.auf_me_vgl);
          strcpy (summe1, "0");
          strcpy (summe2, "0");
          strcpy (summe3, "0");
          strcpy (zsumme1, "0");
          strcpy (zsumme2, "0");
          strcpy (zsumme3, "0");
*/
          sprintf (aufps.lief_me, "%3lf", lief_me);
          strcpy (WiegMe, "0.000");
          memcpy (&aufparr [aufpidx], &aufps, sizeof (struct AUFP_S));
          arrtowepos (aufpidx);
          WePosClass.dbupdate ();

          lief_me = ratod (wiep.brutto) * -1;
          last_anz = 1;
          write_wewiepro (lief_me, lief_me, (double) 0.0);
          StornoSet = 0;
          commitwork ();
          beginwork ();
          return FALSE;
}

void stornoaction (void)
/**
Storno ausfuehren.
**/
{
        double lief_me;
        double rest;
        double wiegme;

        lief_me = (double) ratod (aufps.lief_me);
        rest    = (double) ratod (auf_me);
        wiegme = ratod (editbuff);
        if (wiegme != (double) 0.0)
        {
            rest += wiegme;
            lief_me -= wiegme;
            sprintf (auf_me, "%.3lf", rest);
        }
        else
        {
            if (TestAllStorno () == FALSE) 
            {
                return;
            }
            rest += lief_me;
            if (WiegeTyp == ENTNAHME)
            {
//                   EntnahmeActGew += lief_me;
                   StartWiegen = TRUE;
                   dowiegen ();
                   StartWiegen = FALSE;
                   sprintf (ActEntnahmeGew, "%.3lf", EntnahmeActGew);
            }
            lief_me = (double) 0;
  		    sprintf (aufps.anz_einh, "%.3lf", 0.0);
  		    sprintf (aufps.a_krz, "%.3lf", 0.0);
		    strcpy (wiegzahl, aufps.a_krz);
            strcpy (auf_me, aufps.auf_me_vgl);
            strcpy (summe1, "0");
            strcpy (summe2, "0");
            strcpy (summe3, "0");
            strcpy (zsumme1, "0");
            strcpy (zsumme2, "0");
            strcpy (zsumme3, "0");
        }
        sprintf (aufps.lief_me, "%3lf", lief_me);
        strcpy (WiegMe, "0.000");
//        StornoSet = 0;
        memcpy (&aufparr [aufpidx], &aufps, sizeof (struct AUFP_S));
        arrtowepos (aufpidx);
        WePosClass.dbupdate ();
        commitwork ();
        beginwork ();
}

int dohand (void)
/**
Wiegen durchfuehren.
**/
{
        double alt;
        double netto;

		DestroyKistBmp ();
        StornoSet = 0;
        SetStorno (dostorno, 0);
        sprintf (WiegMe, "%.3lf", (double) 0);
        EnterCalcBox (WiegWindow,"  Gewicht  ",  WiegMe, 12, WiegKgE.mask[0].picture);
        if (syskey != KEY5)
        {
              if (WiegeTyp == ENTNAHME && StartWiegen)
              {
                     EntnahmeStartGew = ratod (WiegMe);
                     EntnahmeActGew   = EntnahmeStartGew;
                     sprintf (ActEntnahmeGew, "%lf", EntnahmeActGew);
                     display_form (WiegWindow, &fEntnahmeGew, 0, 0);
                     return 0;
              }
              strcpy (aufps.erf_kz, "H");
              if (StornoSet)
              {
				  netto = ratod (aufps.lief_me);
				  netto *= (double) -1.0;
				  last_anz = atol (aufps.a_krz);
                  stornoaction ();
                  if (StornoSet)
                  {
                         write_wewiepro (netto, netto, (double) 0.0);
                  }
                  StornoSet = 0;
              }
              else
              {
                  netto = (double) ratod (WiegMe);
				  if (netto <= 99999.99)
				  {
                           if (WiegeTyp == ENTNAHME && Entnahmehand)
                           {
                                double gew = EntnahmeActGew - netto;
                                gew -= EntnahmeTara;
                                EntnahmeActGew = netto;
                                sprintf (ActEntnahmeGew, "%.3lf",EntnahmeActGew);
                                netto = gew;
                                sprintf (WiegMe, "%.3lf", netto);
                           }

                           write_wewiepro (netto, netto, (double) 0.0 );

                           alt = (double) ratod (auf_me);
                           alt -= netto;
                           sprintf (auf_me, "%3lf", alt);
                           alt = (double) ratod (aufps.lief_me);
                           alt += netto;
						   if (alt <= 99999.999)
						   {
                                   sprintf (aufps.lief_me, "%3lf", alt);
						   }
				  }
                  memcpy (&aufparr [aufpidx], &aufps, sizeof (struct AUFP_S));
                  arrtowepos (aufpidx);
                  WePosClass.dbupdate ();
                  commitwork ();
                  beginwork ();
				  SetEti (netto, netto, 0.0);
              }
              display_form (WiegKg, &WiegKgE, 0, 0);
              display_form (WiegWindow, &artwform, 0, 0);
              display_form (WiegWindow, &artwconst, 0, 0);
        }
        current_wieg = 2;
		if (hand_direct)
		{
               strcpy (aufps.s, "3");
               break_wieg = 1;
		}
        return 0;
}

int GetDefault (char *env, char *wert)
/**
Wert aus bws_default holen.
**/
{
        char *etc;
        int anz;
        char buffer [512];
        FILE *fp;

        etc = getenv ("BWSETC");
        if (etc == (char *) 0)
        {
                    etc = "C:\\USER\\FIT\\ETC";
        }

        sprintf (buffer, "%s\\bws_defa", etc);
        fp = fopen (buffer, "r");
/*
        if (fp == NULL)
        {
                    sprintf (buffer, "%s\\bws_defa", etc);
                    fp = fopen (buffer, "r");
        }
*/
        if (fp == NULL) return -1;

        clipped (env);
        while (fgets (buffer, 511, fp))
        {
                     cr_weg (buffer);
                     anz = split (buffer);
                     if (anz < 2) continue;
                     if (strupcmp (wort[1], env, strlen (env)) == 0)
                     {
                                 strcpy (wert, wort [2]);
                                 fclose (fp);
                                 return 0;
                     }
         }
         fclose (fp);
         return (-1);
}

long GetStdAuszeichner (void)
/**
System-Nummer fuer Touch-Screen holen.
**/
{
        static long auszeichner = 0;
        char defwert [20];

        if (auszeichner) return auszeichner;

        if (GetDefault ("TOUCH-AUSZEICHNER", defwert) == -1)
        {
                             return (long) 1;
        }
        auszeichner = atol (defwert);
        if (GetDefault ("PAZ_DIREKT", defwert) != -1)
        {
                        paz_direkt = atoi (defwert);
        }
        return auszeichner;
}

long GetStdWaage (void)
/**
System-Nummer fuer Touch-Screen holen.
**/
{
        static long waage = 0;
        char defwert [20];

        if (waage) return waage;

        if (GetDefault ("TOUCH-WAAGE", defwert) == -1)
        {
                             return (long) 1;
        }
        waage = atol (defwert);
        return waage;
}

void GetAufArt9999 (void)
/**
System-Nummer fuer Touch-Screen holen.
**/
{
/*
        char defwert [20];
        if (GetDefault ("aufart9999", defwert) == -1)
        {
                             return (long) 1;
        }
        aufart9999 = atol (defwert);
        return waage;
*/
		aufart9999 = 0;
        strcpy (sys_par.sys_par_nam, "aufart9999");
        if (sys_par_class.dbreadfirst () == 0)
        {
                    aufart9999 = atoi (sys_par.sys_par_wrt);
        }
}

static long start_aufme;
static long start_liefme;
static long alarm_me = 0;
static short alarm_ok = 0;
static HWND AlarmWindow = NULL;
static short a_me_einh;
static int me_faktor = 1;
static long akt_me = 0;
static int break_ausz = 0;
int wiegx, wiegy, wiegcx, wiegcy;
int alarmy;
static int GxStop = 0;

static char amenge [80];

mfont AlarmFont  = {"Courier New", 150, 0, 1, BLACKCOL,
                                              LTGRAYCOL,
                                              1,
                                              NULL};

static field _alfield [] =
                 {"A C H T U N G ! ! !", 0, 0, 40, -1, 0, "", DISPLAYONLY, 0, 0, 0,
                  amenge,                0, 0, 70, -1, 0, "", DISPLAYONLY, 0, 0, 0};

static form alform = {2, 0, 0, _alfield, 0, 0, 0, 0, &AlarmFont};

mfont MessFont  = {"Courier New", 150, 0, 1, BLACKCOL,
                                             WHITECOL,
                                             1,
                                             NULL};

mfont MessFontBlue  = {"Courier New", 180, 0, 1, WHITECOL,
                                                 BLUECOL,
                                                 1,
                                                 NULL};

static char msgstring  [80];
static char msgstring1 [80];
static char msgstring2 [80];

static int msgy = 1;
static int oky = 4;
static int okcy = 3;
static int okcx = 8;
static int BreakKey5 (void);
static int BreakKey12 (void);
static int BreakOK (void);

static field _msgfield [] =
{
                  msgstring,  0, 0, 1, -1, 0, "", DISPLAYONLY, 0, 0, 0,
                  msgstring1, 0, 0, 2, -1, 0, "", DISPLAYONLY, 0, 0, 0,
                  msgstring2, 0, 0, 3, -1, 0, "", DISPLAYONLY, 0, 0, 0,
                  " OK ",     0, 2, 4, -1, 0, "", BUTTON, 0, 0, KEYCR,
};


static form msgform = {1, 0, 0, _msgfield, 0, 0, 0, 0, &MessFont};

static field _msgfieldjn [] =
{
                  msgstring,  0, 0, 1, -1, 0, "", DISPLAYONLY, 0, 0, 0,
                  msgstring1, 0, 0, 2, -1, 0, "", DISPLAYONLY, 0, 0, 0,
                  msgstring2, 0, 0, 3, -1, 0, "", DISPLAYONLY, 0, 0, 0,
                  "  Ja  ",   0, 2, 4, -1, 0, "", BUTTON, 0, 0,  KEY12,
                  " Nein ",   0, 2, 4, -1, 0, "", BUTTON, 0, 0,  KEY5,
};


static form msgformjn = {5, 0, 0, _msgfieldjn, 0, 0, 0, 0, &MessFont};

static form *aktmessform;


void TestMessage (void)
/**
Window-Meldungen testen.
**/
{
         MSG msg;

         if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE) == 0) return;

         if (msg.message == WM_KEYDOWN && msg.wParam != VK_RETURN) return;

         if (IsWiegMessage (&msg) == 0)
         {
                          TranslateMessage(&msg);
                          DispatchMessage(&msg);
         }
}

void WritePrcomm (char *comm)
/**
Datei Prcomm schreiben.
**/
{
        FILE *fp;
        char *tmp;
        char buffer [512];
        int z;

        tmp = getenv ("TMPPATH");
        if (tmp == NULL)
        {
                  tmp = "C:\\USER\\FIT\\TMP";
        }
        sprintf (buffer, "%s\\prcomm", tmp);
        z = 0;
        fp = fopen (buffer, "w");
        while (fp == NULL)
        {
                    if (z == 1000) break;
                    TestMessage ();
                    if (break_ausz) break;
                    Sleep (10);
                    z ++;
                    fp = fopen (buffer, "w");
        }
        if (fp == NULL) return;
        fprintf (fp, "%s\n", comm);
        fclose (fp);
}

void Alarm (void)
/**
Warnung beim Ueberschreiten der Alarmmenge.
**/
{
        int i;

        if (AlarmWindow) return;

        sprintf (amenge, "Sollmenge fast erreicht");

/*
        AlarmWindow  = CreateWindowEx (
                              0,
                              "AufWiegCtr",
                              "",
                              WS_POPUP | WS_DLGFRAME,
                              wiegx, wiegy - wiegcy - 5,
                              wiegcx, wiegcy,
                              WiegWindow,
                              NULL,
                              hMainInst,
                              NULL);
*/
        AlarmWindow  = CreateWindowEx (
                              0,
                              "AufWiegCtr",
                              "",
                              WS_POPUP | WS_DLGFRAME,
                              wiegx,
//                              wiegy,
                              alarmy,
                              wiegcx, wiegcy,
                              WiegWindow,
                              NULL,
                              hMainInst,
                              NULL);
        if (AlarmWindow == NULL) return;
        ShowWindow (AlarmWindow, SW_SHOWNORMAL);
        UpdateWindow (AlarmWindow);
        for (i = 0; i < 3; i ++)
        {
                    MessageBeep (0xFFFFFFFF);
        }
}


LONG FAR PASCAL MsgProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        switch(msg)
        {

              case WM_PAINT :
                    if (hWnd == MessWindow)
                    {
                            display_form (hWnd, aktmessform, 0, 0);
                    }
                    break;
             case WM_COMMAND :
                    if (LOWORD (wParam) == KEY5 ||
                        LOWORD (wParam) == VK_F5)
                     {
                            syskey = KEY5;
                            SendKey (VK_F5);
                            break;
                    }
                    if (LOWORD (wParam) == KEYCR)
                    {
                            syskey = KEYCR;
                            SendKey (VK_RETURN);
                            break;
                    }
                    if (LOWORD (wParam) == KEY12)
                    {
                            syskey = KEY12;
                            SendKey (VK_F12);
                            break;
                    }
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}


void CreateMessage (HWND hWnd, char *message)
/**
Warnung beim Ueberschreiten der Alarmmenge.
**/
{
        int x, y, cx, cy;
        int i;
        RECT rect;
        TEXTMETRIC tm;

        if (MessWindow) return;

        aktmessform = &msgform;
        for (i = 0; i < msgform.fieldanz; i ++)
        {
            msgform.mask[i].length = 0;
        }

        msgform.fieldanz = 1;
        msgform.mask[0].pos[0] = -1;
        msgform.mask[0].pos[1] = -1;
        msgform.font = &MessFont;
        SetTextMetrics (hWnd, &tm, msgform.font);

        GetClientRect (hWnd, &rect);
        strcpy (msgstring, message);

        cx = (strlen (msgstring) + 3) * tm.tmAveCharWidth;
        x = (rect.right - cx) / 2;
        if (x == 0) x = 0;

        cy = tm.tmHeight * 2;
        y = (rect.bottom - cy) / 2 ;
        MessWindow  = CreateWindowEx (
                              0,
                              "AufWiegWhite",
                              "",
                              WS_POPUP | WS_DLGFRAME,
                              x, y,
                              cx, cy,
                              hWnd,
                              NULL,
                              hMainInst,
                              NULL);
        if (MessWindow == NULL) return;
        ShowWindow (MessWindow, SW_SHOWNORMAL);
        UpdateWindow (MessWindow);
}

void DestroyMessage (void)
{
        if (MessWindow == NULL) return;
        CloseControls (&msgform);
        DestroyWindow (MessWindow);
        MessWindow = 0;
}

int BreakKey5 ()
{
        syskey = KEY5;
        break_enter ();
        return 0;
}

int BreakKey12 ()
{
        syskey = KEY12;
        break_enter ();
        return 0;
}

int BreakOK ()
{
        break_enter ();
        return 0;
}


int CreateMessageJN (HWND hWnd, char *message, char *def)
/**
Warnung beim Ueberschreiten der Alarmmenge.
**/
{
        int x, y, cx, cy;
        RECT rect;
        TEXTMETRIC tm;
        char *ltext;
        int anz;
        int i;
        int okpos;
        unsigned int len, pos;
        HWND OldFocus;

        if (MessWindow) return 0;

        aktmessform = &msgformjn;
        OldFocus = GetFocus ();
        anz = wsplit (message, "\n");
        if (anz == 0) return 0;

        for (i = 0; i < msgformjn.fieldanz; i ++)
        {
            msgformjn.mask[i].length = 0;
        }

        msgformjn.font = &MessFontBlue;
        SetTextMetrics (hWnd, &tm, msgformjn.font);

        msgformjn.mask[1].attribut = REMOVED;
        msgformjn.mask[2].attribut = REMOVED;
        strcpy (msgstring, wort[0]);
        len = strlen (msgstring);
        ltext = msgstring;
        okpos = oky;
        if (anz > 1)
        {
            strcpy (msgstring1, wort[1]);
            if (strlen (msgstring1) > len)
            {
                 len = strlen (msgstring1);
                 ltext = msgstring1;
            }
            msgformjn.mask[1].attribut = DISPLAYONLY;
            okpos += 1;
        }
        if (anz > 2)
        {
            strcpy (msgstring2, wort[2]);
            if (strlen (msgstring2) > len)
            {
                 len = strlen (msgstring2);
                 ltext = msgstring2;
            }
            msgformjn.mask[2].attribut = DISPLAYONLY;
            okpos += 1;
        }

        msgform.fieldanz = 5;

        GetClientRect (hMainWindow, &rect);

        cx = (len + 3) * tm.tmAveCharWidth;

        x = (rect.right - cx) / 2;
        if (x == 0) x = 0;

        cy = tm.tmHeight * 4;
        y = (rect.bottom - cy) / 2 ;

        msgformjn.mask[0].pos[0] = tm.tmHeight * msgy;
        msgformjn.mask[1].pos[0] = (int) (double) (tm.tmHeight * (msgy + 1 * 1.5));
        msgformjn.mask[2].pos[0] = (int) (double) (tm.tmHeight * (msgy + 2 * 1.5));
        msgformjn.mask[msgformjn.fieldanz - 2].pos[0] =
                                 (int) (double) (tm.tmHeight * okpos);
        msgformjn.mask[msgformjn.fieldanz - 1].pos[0] =
                                 (int) (double) (tm.tmHeight * okpos);

        msgformjn.mask[msgformjn.fieldanz - 2].rows   = tm.tmHeight * okcy;
        msgformjn.mask[msgformjn.fieldanz - 2].length = tm.tmAveCharWidth * okcx;
        msgformjn.mask[msgformjn.fieldanz - 1].rows   = tm.tmHeight * okcy;
        msgformjn.mask[msgformjn.fieldanz - 1].length = tm.tmAveCharWidth * okcx;
        strcpy (msgformjn.mask[msgformjn.fieldanz - 2].feld, "  Ja  ");
        strcpy (msgformjn.mask[msgformjn.fieldanz - 1].feld, " Nein ");

        pos = (len + 3 - 16) / 2;
        if (pos <= 0) pos = 0;

        msgformjn.mask[msgformjn.fieldanz - 2].pos[1] = pos * tm.tmAveCharWidth;
        msgformjn.mask[msgformjn.fieldanz - 1].pos[1] = (pos + 9)
                                                     * tm.tmAveCharWidth;

        cy =  msgformjn.mask[msgformjn.fieldanz - 1].pos[0] +
              msgformjn.mask[msgformjn.fieldanz - 1].rows +
              tm.tmHeight;
        y = (rect.bottom - cy) / 2 ;
        MessWindow  = CreateWindowEx (
                              0,
                              "AufWiegBlue",
                              "Meldung",
                              WS_POPUP | WS_DLGFRAME,
                              x, y,
                              cx, cy,
                              NULL,
                              NULL,
                              hMainInst,
                              NULL);
        if (MessWindow == NULL) return 0;
        ShowWindow (MessWindow, SW_SHOWNORMAL);
//        SetWindowPos (MessWindow, HWND_TOPMOST, 0, 0, 0, 0,
//                   SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
        UpdateWindow (MessWindow);
        display_form (MessWindow, &msgformjn, 0, 0);
        save_fkt (5);
        save_fkt (12);
        set_fkt (BreakKey5, 5);
        set_fkt (BreakKey12, 12);
        no_break_end ();
        EnableWindows (hMainWindow, FALSE);
        EnableWindows (hWnd, FALSE);
        SetButtonTab (TRUE);
        enter_form (MessWindow, &msgformjn, 0, 0);
        SetButtonTab (FALSE);
        EnableWindows (hWnd, TRUE);
        EnableWindows (hMainWindow, TRUE);
        DestroyMessage ();
        SetFocus (OldFocus);
        restore_fkt (5);
        restore_fkt (12);
        if (syskey == KEY5) return 0;
        return 1;
}


void CreateMessageOK (HWND hWnd, char *message)
/**
Warnung beim Ueberschreiten der Alarmmenge.
**/
{
        int x, y, cx, cy;
        RECT rect;
        TEXTMETRIC tm;
        char *ltext;
        int anz;
        int i;
        int okpos;
        unsigned int len;
        HWND OldFocus;

        if (MessWindow) return;

        aktmessform = &msgform;
        OldFocus = GetFocus ();
        anz = wsplit (message, "\n");
        if (anz == 0) return;

        for (i = 0; i < msgform.fieldanz; i ++)
        {
            msgform.mask[i].length = 0;
        }

        msgform.font = &MessFontBlue;
        SetTextMetrics (hWnd, &tm, msgform.font);

        msgform.mask[1].attribut = REMOVED;
        msgform.mask[2].attribut = REMOVED;
        strcpy (msgstring, wort[0]);
        len = strlen (msgstring);
        ltext = msgstring;
        okpos = oky;
        if (anz > 1)
        {
            strcpy (msgstring1, wort[1]);
            if (strlen (msgstring1) > len)
            {
                 len = strlen (msgstring1);
                 ltext = msgstring1;
            }
            msgform.mask[1].attribut = DISPLAYONLY;
            okpos += 1;
        }
        if (anz > 2)
        {
            strcpy (msgstring2, wort[2]);
            if (strlen (msgstring2) > len)
            {
                 len = strlen (msgstring2);
                 ltext = msgstring2;
            }
            msgform.mask[2].attribut = DISPLAYONLY;
            okpos += 1;
        }

        msgform.fieldanz = 4;

        GetClientRect (hMainWindow, &rect);

        cx = (len + 3) * tm.tmAveCharWidth;

        x = (rect.right - cx) / 2;
        if (x == 0) x = 0;

        cy = tm.tmHeight * 4;
        y = (rect.bottom - cy) / 2 ;

        msgform.mask[0].pos[0] = tm.tmHeight * msgy;
        msgform.mask[1].pos[0] = (int) (double) (tm.tmHeight * (msgy + 1 * 1.5));
        msgform.mask[2].pos[0] = (int) (double) (tm.tmHeight * (msgy + 2 * 1.5));
        msgform.mask[msgform.fieldanz - 1].pos[0] =
                                 (int) (double) (tm.tmHeight * okpos);

        msgform.mask[msgform.fieldanz - 1].rows   = tm.tmHeight * okcy;
        msgform.mask[msgform.fieldanz - 1].length = tm.tmAveCharWidth * okcx;
        strcpy (msgform.mask[msgform.fieldanz - 1].feld, " OK ");

        cy =  msgform.mask[msgform.fieldanz - 1].pos[0] +
              msgform.mask[msgform.fieldanz - 1].rows +
              tm.tmHeight;
        y = (rect.bottom - cy) / 2 ;
        MessWindow  = CreateWindowEx (
                              0,
                              "AufWiegBlue",
                              "Meldung",
                              WS_POPUP | WS_DLGFRAME,
                              x, y,
                              cx, cy,
                              NULL,
                              NULL,
                              hMainInst,
                              NULL);
        if (MessWindow == NULL) return;
        ShowWindow (MessWindow, SW_SHOWNORMAL);
//        SetWindowPos (MessWindow, HWND_TOPMOST, 0, 0, 0, 0,
//                   SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
        UpdateWindow (MessWindow);
        display_form (MessWindow, &msgform, 0, 0);
        set_fkt (BreakOK, 12);
        break_end ();
        EnableWindows (hMainWindow, FALSE);
        EnableWindows (hWnd, FALSE);
        SetButtonTab (TRUE);
        enter_form (MessWindow, &msgform, 0, 0);
        SetButtonTab (FALSE);
        EnableWindows (hWnd, TRUE);
        EnableWindows (hMainWindow, TRUE);
        DestroyMessage ();
        SetFocus (OldFocus);
}

void disp_messG (char *text, int modus)
{
        if (AktivWindow == NULL)
        {
                 AktivWindow = GetActiveWindow ();
        }
        if (modus == 2)
        {
                    MessageBeep (0xFFFFFFFF);
        }
        CreateMessageOK (AktivWindow, text);
}

static char messbuffer [1024];

int print_messG (int mode, char * format, ...)
/**
Ausgabe in dbfile.
**/
{
        va_list args;

        va_start (args, format);
        vsprintf (messbuffer, format, args);
        va_end (args);
        disp_messG (messbuffer, mode);
        return (0);
}


int abfragejnG (HWND hWnd, char *text, char *def)
/**
Ausgabe in dbfile.
**/
{
        return CreateMessageJN (hWnd, text, def);
}


void WaitStat (char *stat)
/**
Ergebnis-Datei vom Preisauszeichner lesen. Auf stat warten
**/
{
        FILE *fp;
        char *tmp;
        char buffer [512];
        char satz [512];
        int z;

        tmp = getenv ("TMPPATH");
        if (tmp == NULL)
        {
                  tmp = "C:\\USER\\FIT\\TMP";
        }
        sprintf (buffer, "%s\\prstat", tmp);

        z = 0;
        while (TRUE)
        {
                  if (z == 30000) break;
                  fp = fopen (buffer, "r");
                  if (fp == NULL)
                  {
                              z ++;
                              Sleep (10);
                              continue;
                  }
                  if (fgets (satz, 511, fp) == 0)
                  {
                              z ++;
                              fclose (fp);
                              Sleep (10);
                              continue;
                  }
                  fclose (fp);
                  cr_weg (satz);
                  split (satz);
                  if (strcmp (wort[1], stat) == 0) break;
                  z ++;
                  TestMessage ();
                  if (break_ausz) break;
                  Sleep (10);
         }
}


static long sup;
static long sup0;
static long sup1;
static short kunme_einh = 0;
static long  kunmez = 0l;
static long  kunme = 0l;
static double akt_zsu1 = (double) 0;
static double akt_zsu2 = (double) 0;
static char rec [256] = {" "};


void TestSu (double zsu1, double zsu2, double zsu3)
{
         sup = (long) zsu2;
         sup0 = (long) zsu3;
}


int AuszKomplett (void)
/**
Test, ob die Auftragsmenge erreicht ist.
**/
{
        long auf_me_kun;

        auf_me_kun = atol (aufps.auf_me);
        switch (kunme_einh)
        {
            case 2 :
                 if (ratod (auf_me) <= (double) 0.0)
                 {
                         return 1;
                 }
                 return 0;
            case 1 :
                 if (kunme > auf_me_kun)
                 {
                         return 1;
                 }
                 return 0;
            case 8 :
                 if (sup1 > auf_me_kun)
                 {
                         return 1;
                 }
                 return 0;
            case 16 :
                 if (sup0 > auf_me_kun)
                 {
                         return 1;
                 }
                 return 0;
        }
        return 0;
}


void GetKunMe ()
/**
Auftragsmenge in Kundenbestelleinheit
**/
{
         int me_einh_kun;

         me_einh_kun = atoi (aufps.me_einh_kun);
         switch (me_einh_kun)
         {
             case 2 :
                   kunme_einh = 2;
                   break;
             case 6 :
             case 7 :
                   kunme_einh = 8;
                   break;
             case 10 :
                   kunme_einh = 16;
                   break;
             default :
                   kunme_einh = 1;
                   break;
         }
}

void FillSummen (double su1, double su2,
                 double zsu1, double zsu2)
/**
Aktuellen Inhalte der einzelnen Summen in Abhaengigkeit
von der Einheit fuellen.
**/
{

         switch (a_kun_geb.geb_fill)
         {
               case 2 :
                    sprintf (summe1, "%.3lf", su1 / me_faktor);
                    break;
               case 1:
                    sprintf (summe1, "%.0lf", zsu1);
                    break;
               default :
                    sprintf (summe1, "%.3lf", su1 / me_faktor);
                    break;
         }

         switch (a_kun_geb.pal_fill)
         {
               case 2 :
                    sprintf (summe2, "%.3lf", su2 / me_faktor);
                    break;
               case 1:
                    sprintf (summe2, "%.0lf", zsu2);
                    break;
               case 5:
                    sprintf (summe2, "%ld", sup);
                    break;
               default :
                    sprintf (summe2, "%.3lf", su2 / me_faktor);
                    break;
         }
}

void FillSumme3 (double su3, double zsu3)
/**
Aktuellen Inhalte von Summe3 in Abhaengigkeit
von der Einheit fuellen.
**/
{

        sprintf (summe3, "%ld", sup0);
}

int KorrMe (char *satz)
/**
Auftragsmenge korrigieren.
**/
{
         int anz;
         long me;
         long zielme;
         double me_vgl;
         double su1;
         double su2;
         double zsu1;
         double zsu2;
         double zsu3;

         if (atoi (aufps.a_me_einh) == 2)
         {
                    me_vgl = ratod (aufps.auf_me_vgl) * 1000;
         }
         else
         {
                    me_vgl = ratod (aufps.auf_me_vgl);
         }

         cr_weg (satz);
         anz = split (satz);
         if (anz < 9)
         {
                   return 1;
         }

         if (wort[1][0] == 'E')
         {
                   return 0;
         }

         if (GxStop) return 1;


		 if (strcmp (rec," ") ==  0) strcpy (rec,satz);
		 if (strcmp (satz, rec) == 0) return 1;

		 strcpy (rec, satz);
         me   = atol (wort[9]);

         zsu1 = ratod (wort[4]);
         su1  = ratod (wort[5]);
         zsu2 = ratod (wort[6]);
         su2  = ratod (wort[7]);
         zsu3 = ratod (wort[8]);

         TestSu (zsu1, zsu2, zsu3);
         FillSummen (su1, su2, zsu1, zsu2);

         zsu2 = (double) sup;


         sprintf (zsumme1, "%.0lf", zsu1);
		 if (a_kun_geb.pal_fill == 5 || a_kun_geb.pal_fill == 8)
		 {
                 sprintf (zsumme2, "%.0lf", zsu2);
		 }
		 else if (kunme_einh == 8)
		 {
                 sprintf (zsumme2, "%.0lf", zsu3);
		 }
         if (!alarm_ok && me > 0l)
         {
                     alarm_me = (long) ((double) me_vgl - 3 * me);
                     alarm_ok = 1;
         }

         kunme ++;
         zielme = start_aufme - me;
         sprintf (auf_me, "%.3lf", (double) ((double) zielme / me_faktor));

         zielme = start_liefme + me;
         sprintf (aufps.lief_me, "%.3lf", (double) ((double) zielme / me_faktor));
         FillSumme3 ((double) zielme, zsu3);
         zsu3 = (double) sup0;
		 if (kunme_einh == 16)
		 {
                   sprintf (zsumme3, "%.0lf", zsu3);
		 }
         AktPrWieg[AktWiegPos] = ((double) (me - akt_me)) / me_faktor;
         sprintf (WiegMe, "%.3lf", (double) ((double) (me - akt_me) / me_faktor));


         display_field (WiegWindow, &artwform.mask[1], 0, 0);
         display_field (WiegWindow, &artwform.mask[2], 0, 0);

         display_field (WiegWindow, &artwform.mask[3], 0, 0);
         display_field (WiegWindow, &artwform.mask[4], 0, 0);
         display_field (WiegWindow, &artwform.mask[5], 0, 0);
         display_field (WiegWindow, &artwform.mask[6], 0, 0);
         display_field (WiegWindow, &artwform.mask[7], 0, 0);
         display_field (WiegWindow, &artwform.mask[8], 0, 0);
         display_field (WiegWindow, &artwform.mask[9], 0, 0);
         display_field (WiegWindow, &artwform.mask[10], 0, 0);



         display_field (WiegKg,     &WiegKgE.mask[0], 0, 0);

         memcpy (&aufparr [aufpidx], &aufps, sizeof (struct AUFP_S));
         arrtowepos (aufpidx);
         WePosClass.dbupdate ();
         commitwork ();
         beginwork ();

         SetFocus (WieButton.mask[3].feldid);
         if (AktWiegPos < 998) AktWiegPos ++;
         akt_me = (long) me;

         return 1;
}

void PollAusz (void)
/**
Ergebnis-Datei vom Preisauszeichner lesen.
**/
{
        FILE *fp;
        char *tmp;
        char buffer [512];
        char satz [512];
        int pollanz;

        a_me_einh = atoi (aufps.a_me_einh);
        alarm_me = 0l;
        alarm_ok = 0;
        kunme = 0;
        kunmez = 0l;
        GetKunMe ();
        sup = (long) 0;
        sup0 = 0l;
        sup1 = 0l;
		akt_zsu1 = (double) 0.0;
		akt_zsu2 = (double) 0.0;
		strcpy (rec, " ");
        if (a_me_einh == 2)
        {
                      me_faktor = 1000;
        }
        else
        {
                      me_faktor = 1;
        }

        start_aufme  =  (long) ((double) ratod (auf_me) * me_faktor);
        start_liefme =  (long) ((double) ratod (aufps.lief_me) * me_faktor);

        tmp = getenv ("TMPPATH");
        if (tmp == NULL)
        {
                  tmp = "C:\\USER\\FIT\\TMP";
        }
        sprintf (buffer, "%s\\prstat", tmp);

        GxStop = pollanz = 0;
        while (TRUE)
        {
                  TestMessage ();
                  if (GxStop) pollanz ++;
                  if (pollanz >= 40) break;
                  fp = fopen (buffer, "r");
                  if (fp == NULL)
                  {
                              TestMessage ();
                              Sleep (50);
                              continue;
                  }
                  if (fgets (satz, 511, fp) == 0)
                  {
                              TestMessage ();
                              fclose (fp);
                              Sleep (50);
                              continue;
                  }
                  fclose (fp);
                  if (KorrMe (satz) == 0)
                  {
                              break;
                  }
                  TestMessage ();
                  SetFocus (WieButton.mask[3].feldid);
                  Sleep (50);
         }
}

void starte_prakt (void)
/**
Prgramm prakt starten.
**/
{
        char *tmp;
        char *bws;
        char buffer [512];

        tmp = getenv ("TMPPATH");
        if (tmp == NULL)
        {
                  tmp = "C:\\USER\\FIT\\TMP";
        }
        sprintf (buffer, "%s\\prstat", tmp);
        unlink (buffer);

        bws = getenv ("BWS");
        if (bws == NULL)
        {
                  bws = "C:\\USER\\FIT";
        }
        sprintf (buffer, "%s\\bin\\gxakt", bws);

        if (ProcExec (buffer, SW_SHOWMINNOACTIVE, -1, 0, -1, 0) == 0)
        {
                       print_messG (2, "Fehler beim Start von %s", buffer);
//                       break_ausz = 1;
        }
}

void SetAuszText (int mode)
/**
Text fuer AuszeicnenButton setzen.
**/
{
         if (mode == 0)
         {
               WieWie.text1 = "Aus-";
               WieWie.text2 = "zeichnen";
               WieWie.tx1 = -1;
               WieWie.ty1 = 25;
               WieWie.tx2 = -1;
               WieWie.ty2 = 50;
         }
         else
         {
               WieWie.text1 = "Stop";
               WieWie.text2 = "";
               WieWie.tx1 = -1;
               WieWie.ty1 = -1;
               WieWie.tx2 = -1;
               WieWie.ty2 = -1;
          }
          WieWie.aktivate = 2;
          ActivateColButton (WiegCtr, &WieButton, 3, 2, 1);
          SetFocus (WieButton.mask[3].feldid);
}

void BuInactiv (void)
/**
Alle Button auf Inaktiv setzen.
**/
{
          ActivateColButton (WiegCtr, &WieButton, 0, -1, 1);
          ActivateColButton (WiegCtr, &WieButton, 1, -1, 1);
          ActivateColButton (WiegCtr, &WieButton, 2, -1, 1);
          ActivateColButton (WiegCtr, &WieButton, 3, -1, 1);
          ActivateColButton (WiegCtr, &WieButton, 4, -1, 1);
          ActivateColButton (WiegCtr, &WieButton, 5, -1, 1);
          ActivateColButton (WiegCtr, &WieButton, 6, -1, 1);
}

void BuActiv (void)
/**
Alle Button auf Inaktiv setzen.
**/
{
          ActivateColButton (WiegCtr, &WieButton, 0, 2, 1);
          ActivateColButton (WiegCtr, &WieButton, 1, 2, 1);
          ActivateColButton (WiegCtr, &WieButton, 2, 2, 1);
          ActivateColButton (WiegCtr, &WieButton, 3, 2, 1);
          ActivateColButton (WiegCtr, &WieButton, 4, 2, 1);
          ActivateColButton (WiegCtr, &WieButton, 5, 2, 1);
          ActivateColButton (WiegCtr, &WieButton, 6, 2, 1);
}

static void FileError (void)
{

          disp_messG ("Es wurden keine Daten zum Auszeichnen gefunden\n"
                     "Pr�fen Sie bitte den Verkaufspreis\n und die Hauptwarengruppenzuordnung",
                     2);
          return;
}


BOOL KunGxOk (void)
/**
Pruefen, ob die Dateien fuer gxakt leer sind.
**/
{
        char *tmp;
        char dname [256];
        HANDLE fp;
        DWORD fsize;

        tmp = getenv ("TMPPATH");
        if (tmp == (char *) 0)
        {
                        tmp = "C:\\USER\\FIT\\TMP";
        }
        sprintf (dname, "%s\\kungx", tmp);
        fp = CreateFile (dname, GENERIC_READ, 0, NULL,
                         OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
        if (fp == INVALID_HANDLE_VALUE)
        {
                         FileError ();
                         return FALSE;
        }

        fsize = GetFileSize (fp, NULL);
        if (fsize == (DWORD) 0 ||
            fsize == 0xFFFFFFFF)
        {
                         CloseHandle (fp);
                         FileError ();
                         return FALSE;
        }

        CloseHandle (fp);
        return TRUE;
}



int doauszeichnen (void)
/**
Auftragsposition auszeichnen.
**/
{
        char buffer [512];
        char *tmp;
        double auf_me_vgl;
        double auf_me_kun;
        DWORD ex;

        akt_me = 0;
        break_ausz = 0;
        tmp = getenv ("TMPPATH");
        if (tmp == (char *) 0)
        {
                        tmp = "C:\\USER\\FIT\\TMP";
        }

        GetKunMe ();
        if (atoi (aufps.me_einh_kun) == 2)
        {
                    auf_me_kun = ratod (aufps.auf_me) * 1000;
        }
        else
        {
                    auf_me_kun = ratod (aufps.auf_me);
        }
        if (auf_me_kun <= (double) 0.0)
        {

                       disp_messG ("Menge 0 kann nicht ausgezeichnet werden", 2);
                       return (0);
        }
        if (atoi (aufps.a_me_einh) == 2)
        {
                    auf_me_vgl = ratod (aufps.auf_me_vgl) * 1000;
        }
/*
        if (auf_me_vgl <= (double) 0.0)
        {

                       disp_messG ("Menge 0 kann nicht ausgezeichnet werden", 2);
                       return (0);
        }
*/

        if (ratod (auf_me) <= (double) 0.0)
        {

                       disp_messG ("Die Sollmenge ist erreicht", 2);
                       return (0);
        }
        if (paz_direkt == 3)
        {
/*
                      RunBwsasc (auszeichner, auf_me_vgl);
                      BuInactiv ();
*/
        }
        else if (paz_direkt == 2)
        {

/*
                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungx kungx.if %ld %ld %s %.0lf %s",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr), lieferdatum, auf_me_vgl,
                          aufps.auf_lad_pr);
*/
                      sprintf (buffer, "pr_paz -dokt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungx kungx.if %ld %ld %s %.0lf %s %hd",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr), lieferdatum, auf_me_kun,
                          aufps.auf_lad_pr, kunme_einh);

                      BuInactiv ();
                      ex = ProcWaitExec (buffer, SW_SHOWNORMAL,
                                                 -1, 0, -1, 0);
                      if (ex != 0l)
                      {
                          print_messG (2,
                              "Fehler beim 1. Start von pr_paz");
                          BuActiv ();
                          return (-1);
                      }

                      sprintf (buffer, "pr_paz -odkt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungtex kungtex.if %ld %ld",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr));

                      ex = ProcWaitExec (buffer, SW_SHOWNORMAL,
                                                 -1, 0, -1, 0);
                      if (ex != 0l)
                      {
                          print_messG (2,
                              "Fehler beim 2. Start von pr_paz");
                          BuActiv ();
                          return (-1);
                      }
       }
        else if (paz_direkt == 1)
        {
/*
                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungx kungx.if %ld %ld %s %.0lf %s",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr), lieferdatum, auf_me_vgl,
                          aufps.auf_lad_pr);
*/
                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungx kungx.if %ld %ld %s %.0lf %s %hd",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr), lieferdatum, auf_me_kun,
                          aufps.auf_lad_pr, kunme_einh);


                      BuInactiv ();
                      CreateBatch (buffer);

                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungtex kungtex.if %ld %ld",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr));

                     RunBatch (buffer);
        }
        else
        {
/*
                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungx kungx.if %ld %ld %s %.0lf %s",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr), lieferdatum, auf_me_vgl,
                          aufps.auf_lad_pr);
*/
                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungx kungx.if %ld %ld %s %.0lf %s %hd",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr), lieferdatum, auf_me_kun,
                          aufps.auf_lad_pr, kunme_einh );

                      BuInactiv ();
                      CreateMessage (WiegWindow, "Die Daten werden vorbereitet");
                      ex = ProcWaitExec (buffer, SW_HIDE,
                                                 -1, 0, -1, 0);
                      if (ex == (long) -1)
                      {
                          DestroyMessage ();
                          AktivWindow = WiegWindow;
                          print_messG (2,
                              "Fehler beim 1. Start von pr_paz");
                          BuActiv ();
                          return (-1);
                      }
                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungtex kungtex.if %ld %ld",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr));

                      ex = ProcWaitExec (buffer, SW_HIDE,
                                                 -1, 0, -1, 0);
                      if (ex == (long) -1)
                      {
                          DestroyMessage ();
                          AktivWindow = WiegWindow;
                          print_messG (2,
                              "Fehler beim 2. Start von pr_paz");
                          BuActiv ();
                          return (-1);
                      }
                      DestroyMessage ();
                      AktivWindow = WiegWindow;
       }
       if (! KunGxOk ())
       {
                    BuActiv ();
                    WorkModus = AUSZEICHNEN;
                    return (-1);
        }

        WorkModus = STOP;
        WritePrcomm ("G");
        if (! break_ausz)
        {
                   SetAuszText (1);
                   starte_prakt ();
                   SetActiveWindow (WiegCtr);
        }
        if (! break_ausz)
        {
                   PollAusz ();
        }
        WritePrcomm ("E");
        SetAuszText (0);
        BuActiv ();
        WorkModus = AUSZEICHNEN;
        if (AlarmWindow)
        {
                     CloseControls (&alform);
                     DestroyWindow (AlarmWindow);
                     AlarmWindow = NULL;
        }
        return 0;
}

int StopAuszeichnen (void)
{

        GxStop = 1;
        if (abfragejnG (WiegWindow, "Auszeichnen stoppen ?", "N") == 0)
        {
            GxStop = 0;
            return 0;
        }
        WritePrcomm ("E");
        GxStop = 1;
        return 0;
}

void IncWiegZahl (void)
/**
Wieganzahl erhoehen.
**/
{
	    sprintf (wiegzahl, "%ld", atol (wiegzahl) + 1);
}

void PaintKist (HDC hdc)
/**
Kiste zeichnen.
**/
{
	    if (KistPaint == FALSE) return;

        KistMask->DrawBitmapEx (hdc, KistRect.left, KistRect.top, SRCAND);
		if (KistCol)
		{
                 KistG->DrawBitmapEx (hdc, KistRect.left, KistRect.top, SRCPAINT);
		}
		else
		{
                 Kist->DrawBitmapEx (hdc, KistRect.left, KistRect.top, SRCPAINT);
		}
}

void PaintKistBmp (void)
{
	    if (KistPaint == TRUE) return;
		if (KistCol)
		{
			KistCol = FALSE;
			SetClassLong (WiegKg, GCL_HBRBACKGROUND, (long) LtBlueBrush);
		}
		else
		{
			KistCol = TRUE;
			SetClassLong (WiegKg, GCL_HBRBACKGROUND, (long) YellowBrush);
		}
	    KistPaint = TRUE;
		MessageBeep (MB_ICONASTERISK);
		InvalidateRect (WiegWindow, &KistRect, TRUE);
		InvalidateRect (WiegKg, NULL, TRUE);
		UpdateWindow (WiegWindow);
		UpdateWindow (WiegKg);
}

void DestroyKistBmp (void)
{
	    if (KistPaint == FALSE) return;
	    KistPaint = FALSE;
		InvalidateRect (WiegWindow, &KistRect, TRUE);
		UpdateWindow (WiegWindow);
}


void SetKistRect (int x, int y)
/**
Position und Groesse fuer WQiegBitmap setzen.
**/
{
	    POINT bpoint;
		HDC hdc;

		hdc = GetDC (WiegWindow);
		Kist->BitmapSize (hdc, &bpoint);
		ReleaseDC (WiegWindow, hdc);
		KistRect.left = 50;
		KistRect.top  = y;
		KistRect.right   = KistRect.left + bpoint.x;
		KistRect.bottom  = KistRect.top  + bpoint.y;
}




int dowiegen (void)
/**
Wiegen durchfuehren.
**/
{
        double netto;
        double brutto;
        double tara;
        double alt;
        static int arganz;
        static char *args[4];
		static char WiegAnz[20];

        if (WorkModus == AUSZEICHNEN)
        {
                       return (doauszeichnen ());
        }
        else if (WorkModus == STOP)
        {
                       return (StopAuszeichnen ());
        }
		DestroyKistBmp ();

        arganz = 3;
        args[0] = "Leer";
        args[1] = "11";
        args[2] = waadir;
        fnbexec (arganz, args);
/* Test  */

/*
		Sleep (2000);
*/

/* Testende  */

        brutto = netto = tara = (double) 0.0;
        GetGewicht (ergebnis, &brutto, &netto, &tara);
/*
		if (aufps.hnd_gew[0] == 'H' || aufps.erf_gew_kz[0] == 'N')
		{
                SetStorno (dostorno, 0);
                sprintf (WiegAnz, "%.0lf", (double) 1);
                EnterCalcBox (WiegWindow,"  Anzahl  ",  WiegAnz, 12, "%8.3lf");
                if (syskey != KEY5)
				{
					SetEtiStk (ratod (WiegAnz));
				}
				else
				{
                    SetEtiStk ((double) 1.0);
				}
		}
		else
		{
                SetEtiStk ((double) 1.0);
		}
*/

        if (WiegeTyp == ENTNAHME && StartWiegen)
        {
                   EntnahmeStartGew = brutto;
                   EntnahmeActGew   = brutto;
                   sprintf (ActEntnahmeGew, "%lf", EntnahmeActGew);
                   display_form (WiegWindow, &fEntnahmeGew, 0, 0);
                   return 0;
        }

		if (netto <= 99999.999)
		{
                SetEti (netto, brutto, tara);
		}
        if (tara == (double) 0.0)
        {
                   strcpy (aufps.erf_kz, "W");
        }
        else
        {
                   strcpy (aufps.erf_kz, "E");
        }
		if (netto <= 99999.999)
		{
                  alt = (double) ratod (auf_me);
                  alt -= netto;
                  sprintf (WiegMe, "%11.3lf", netto);
                  sprintf (auf_me, "%3lf", alt);
                  alt = (double) ratod (aufps.lief_me);
                  alt += netto;
				  if (alt <= 99999.999)
				  {
                           sprintf (aufps.lief_me, "%3lf", alt);
				  }
		}
		if (tara <= 99999.999)
		{
                  sprintf (aufps.tara, "%.3lf", tara);
		}
        if (WiegeTyp == STANDARD && autoan) 
        {
            akt_tara = brutto;
            Sleep (AutoSleep);
            settara ();
        }
        if (netto > 0.0)
        {
		    IncWiegZahl ();
        }
        display_form (WiegKg, &WiegKgE, 0, 0);
        display_form (WiegWindow, &artwform, 0, 0);
        display_form (WiegWindow, &artwconst, 0, 0);
        if (WiegeTyp == STANDARD && autoan) settara ();
		PaintKistBmp ();
        current_wieg = 3;
        if (ratod (aufps.a) != 0.0)
        {
		          strcpy (aufps.a_krz, wiegzahl);
                  memcpy (&aufparr [aufpidx], &aufps, sizeof (struct AUFP_S));
                  arrtowepos (aufpidx);
                  WePosClass.dbupdate ();
        }
        commitwork ();
        beginwork ();
		if (auto_eti)
		{
                 doetikett ();
		}
//        return 0;
		if (wieg_direct && StartWiegen == FALSE )
		{
		            doWOK ();
		}
        return 0;
}

void PrintWLines ()
/**
Icons am Bildschirm anzeigen.
**/
{
         RECT rect, frect;
         PAINTSTRUCT ps;
         HDC hdc;
         HPEN hPen;
         int x, y;

         GetClientRect (WiegWindow, &rect);
         GetFrmRect (&artwform, &frect);

         hdc = BeginPaint (WiegWindow, &ps);

         hPen = CreatePen (PS_SOLID, 2, BLACKCOL);
         SelectObject (hdc, hPen);
         x = rect.right - 4;
         y = 10;
         MoveToEx (hdc, 2, y, NULL);
         LineTo (hdc, x, y);
         DeleteObject (hPen);

         hPen = CreatePen (PS_SOLID, 2, GRAYCOL);
         SelectObject (hdc, hPen);
         MoveToEx (hdc, 2, y - 1, NULL);
         LineTo (hdc, x, y - 2);
         DeleteObject (hPen);


         hPen = CreatePen (PS_SOLID, 2, BLACKCOL);
         SelectObject (hdc, hPen);
         x = rect.right - 4;
         y = frect.bottom + 25;
         alarmy = y;
         MoveToEx (hdc, 2, y, NULL);
         LineTo (hdc, x, y);
         DeleteObject (hPen);

         hPen = CreatePen (PS_SOLID, 2, GRAYCOL);
         SelectObject (hdc, hPen);
         MoveToEx (hdc, 2, y - 1, NULL);
         LineTo (hdc, x, y - 2);
         DeleteObject (hPen);

		 PaintKist (hdc);

         EndPaint (WiegWindow, &ps);
}


LONG FAR PASCAL WiegProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        switch(msg)
        {

              case WM_PAINT :
                    if (hWnd == MessWindow)
                    {
                            display_form (hWnd, &msgform, 0, 0);
                    }


                    if (hWnd == WiegKg)
                    {
                            display_form (hWnd, &WiegKgT, 0, 0);
                            display_form (hWnd, &WiegKgE, 0, 0);
                    }
                    else if (hWnd == WiegCtr)
                    {       if (CtrMode == 0)
                            {
                                    display_form (hWnd, &WieButton, 0, 0);
                                    SetFocus (
                                           WieButton.mask[current_wieg].feldid);
                            }
                            else
                            {
                                    display_form (hWnd, &WieButtonT, 0, 0);
                                    SetFocus (
                                           WieButtonT.mask[current_wieg].feldid);
                            }
                    }
                    else if (hWnd == WiegWindow)
                    {
                            display_form (hWnd, &WiegKgMe, 0, 0);
                            display_form (hWnd, &artwformT, 0, 0);
                            display_form (hWnd, &artwconstT, 0, 0);
                            if (WiegeTyp == ENTNAHME)
                            {
                                  if (StartWiegen)
                                  {
                                        display_form (hWnd, &startwieg, 0, 0);
                                  }
                                  display_form (hWnd, &fEntnahmeGewTxt, 0, 0);
                                  display_form (hWnd, &fEntnahmeGew, 0, 0);
                            }
                            display_form (hWnd, &EtiButton, 0, 0);
							if (WithSpezEti)
							{
                                 display_form (hWnd, &EtiSpezButton, 0, 0);
							}
							if (WithLagerAuswahl)
							{
	                            display_form (hWnd, &LgrSpezButton, 0, 0);
							}
							if (WithZusatz && (_a_bas.charg_hand == 1 || _a_bas.charg_hand == 9 ||
								_a_bas.zerl_eti == 1))
							{
                                 display_form (hWnd, &ZusatzButton, 0, 0);
							}

							if (_a_bas.charg_hand == 9)
							{
								artwformTW.mask[0].feld = IdentNr;
								artwform.mask[0].feld = aufps.ident_nr;
							}
							else
							{
								artwformTW.mask[0].feld = ChargenNr;
								artwform.mask[0].feld = aufps.ls_charge;
							}

                            if (artwform.mask[0].feldid)
                            {
                                 display_form (hWnd, &artwformTW, 0, 0);
                                 display_form (hWnd, &artwform, 0, 0);
                                 display_form (hWnd, &artwconst, 0, 0);
                            }
                            display_form (hWnd, &ftara, 0, 0);
                            display_form (hWnd, &ftaratxt, 0, 0);
                            display_form (hWnd, &sw_abzug, 0, 0); //021109
							display_form (hWnd, &ftexte, 0, 0);
                            PrintWLines ();
                    }
                    else if (hWnd == AlarmWindow)
                    {
                            display_form (hWnd, &alform, 0, 0);
                    }
                    break;
             case WM_COMMAND :
                    if (LOWORD (wParam) == KEY5 ||
                        LOWORD (wParam) == VK_F5)
                    {
                            syskey = KEY5;
                            SendKey (VK_F5);
                            break;
                    }
                    if (LOWORD (wParam) == KEYCR)
                    {
                            syskey = KEYCR;
                            SendKey (VK_RETURN);
                            break;
                    }
                    if (LOWORD (wParam) == BUTARA)
                    {
                            PostMessage (WiegWindow, WM_USER, wParam, 0l);
                            break;
                    }
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}


void RegisterWieg (void)
/**
Fenster fuer Auftragskopf registrieren
**/
{
        static int registered = 0;
        WNDCLASS wc;

        if (registered == 0)
        {
                   wc.style =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                               | CS_OWNDC;
                   wc.cbClsExtra    =  0;
                   wc.cbWndExtra    =  0;
                   wc.hInstance     =  hMainInst;
                   wc.hIcon         =  LoadIcon (hMainInst, "FITICON");
                   wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                   wc.lpszMenuName  =  "";
                   wc.lpfnWndProc   =  WiegProc;
                   wc.lpszMenuName  =  "";
                   wc.hbrBackground =  CreateSolidBrush (RGB (0, 255, 255));
                   wc.lpszClassName =  "AufWieg";
                   RegisterClass(&wc);

                   // wc.hbrBackground =  CreateSolidBrush (RGB (0, 0, 120));
                   wc.hbrBackground =   GetStockObject (LTGRAY_BRUSH);
                   wc.lpszClassName =  "AufWiegCtr";
                   RegisterClass(&wc);

                   wc.lpfnWndProc   =   WiegProc;
                   wc.hbrBackground =  CreateSolidBrush (RGB (0, 0, 255));
                   wc.lpszClassName =  "AufWiegKg";
                   RegisterClass(&wc);
                   registered = 1;
        }
}

void TestEnter (void)
/**
Eingabefeld testen.
**/
{
       GetWindowText (artwform.mask[0].feldid,
                      artwform.mask[0].feld,
                      artwform.mask[0].length);
}


HWND *FocusTab [20];
int FocusTabCount = 0;
int SpezEtiPos = -1;
int SpezLgrPos = -1;
int ZusatzPos = -1;


void SetFocusTab (form *wform)
{
	   for (int i = 0; i < wform->fieldanz; i ++)
	   {
		   FocusTab[i] = &wform->mask[i].feldid;
	   }
	   FocusTabCount = i;
       if (CtrMode == 1) return;

//	   if (WithEti || WithSumEti)
	   {
		   FocusTab[i] = &EtiButton.mask[0].feldid;
		   i ++;
		   FocusTab[i] = &EtiButton.mask[1].feldid;
		   i ++;
	   }
	   if (WithZusatz)
	   {
		   FocusTab[i] = &ZusatzButton.mask[0].feldid;
		   ZusatzPos = i;
		   i ++;
	   }
	   if (WithSpezEti)
	   {
		   FocusTab[i] = &EtiSpezButton.mask[0].feldid;
		   SpezEtiPos = i;
		   i ++;
	   }
	   if (WithSpezEti)
	   {
		   FocusTab[i] = &LgrSpezButton.mask[0].feldid;
		   SpezLgrPos = i;
			i ++;
	   }
	   FocusTabCount = i;
}

void SetWiegFocus (void)
/**
Focus fuer Wiegmaske setzen.
**/
{
       form *wform;

       if (CtrMode == 1)
       {
                  wform = &WieButtonT;
       }
       else
       {
                  wform = &WieButton;
       }

	   SetFocusTab (wform);
	   if (current_wieg >= FocusTabCount) current_wieg = 0;
       SetFocus (*FocusTab [current_wieg]);
	   return;

       if (current_wieg < etiend)
       {
                  SetFocus (wform->mask[current_wieg].feldid);
       }
       else if (current_wieg < wiegend)
	   {
                  SetFocus (EtiButton.mask[current_wieg - etiend].feldid);
	   }
	   else if (WithSpezEti && (current_wieg - wiegend < EtiSpezButton.fieldanz))
	   {
                  SetFocus (EtiSpezButton.mask[current_wieg - wiegend].feldid);
	   }
	   else if (WithLagerAuswahl && (current_wieg - wiegend < LgrSpezButton.fieldanz))
	   {
                  SetFocus (LgrSpezButton.mask[current_wieg - wiegend].feldid);
	   }
	   else if (WithZusatz && (current_wieg - wiegend < ZusatzButton.fieldanz))
	   {
                  SetFocus (ZusatzButton.mask[current_wieg - wiegend].feldid);
	   }
       else if (artwform.mask[0].attribut != EDIT)
	   {
		          current_wieg = 0;
                  SetFocus (wform->mask[current_wieg].feldid);
	   }
	   else
       {
                  SetFocus (artwform.mask[0].feldid);
                  SendMessage (artwform.mask[0].feldid, EM_SETSEL,
                               (WPARAM) 0, MAKELONG (-1, 0));
       }
}

int ReturnActionW (void)
/**
Returntaste behandeln.
**/
{
       form *wform;
	   int current;
	   int we;
	   we = wiegend;
	   if (WithSpezEti)
	   {
		   we ++;
	   }
	   if (WithLagerAuswahl)
	   {
		   we ++;
	   }
	   if (WithZusatz)
	   {
		   we ++;
	   }


       if (current_wieg == SpezEtiPos)
	   {
                  if (EtiSpezButton.mask[current_wieg - wiegend].after)
				  {
                       (*EtiSpezButton.mask[current_wieg - wiegend].after) ();
				  }
                  else if (EtiSpezButton.mask[current_wieg - wiegend].BuId)
				  {
                       SendKey
                                 (EtiSpezButton.mask[current_wieg - wiegend].BuId);
				  }
				  return TRUE;
	   }

       if (current_wieg == SpezLgrPos)
	   {
                  if (LgrSpezButton.mask[current_wieg - wiegend].after)
				  {
                       (*LgrSpezButton.mask[current_wieg - wiegend].after) ();
				  }
                  else if (LgrSpezButton.mask[current_wieg - wiegend].BuId)
				  {
                       SendKey
                                 (LgrSpezButton.mask[current_wieg - wiegend].BuId);
				  }
				  return TRUE;
	   }
       if (current_wieg == ZusatzPos)
	   {
                  if (ZusatzButton.mask[0].after)
				  {
                       (*ZusatzButton.mask[0].after) ();
				  }
                  else if (ZusatzButton.mask[0].BuId)
				  {
                       SendKey
                                 (ZusatzButton.mask[0].BuId);
				  }
				  return TRUE;
	   }

       if (WithSpezEti && (current_wieg == (wiegend + EtiSpezButton.fieldanz)))
       {
                  PostMessage (WiegWindow, WM_KEYDOWN, VK_DOWN, 0l);
                  return TRUE;
       }
       else if (WithLagerAuswahl && (current_wieg == (wiegend + LgrSpezButton.fieldanz)))
       {
                  PostMessage (WiegWindow, WM_KEYDOWN, VK_DOWN, 0l);
                  return TRUE;
       }
	   else if ((WithSpezEti == FALSE) && (current_wieg == wiegend))
       {
                  PostMessage (WiegWindow, WM_KEYDOWN, VK_DOWN, 0l);
                  return TRUE;
       }


       if (current_wieg == wiegend)
//       if (current_wieg == we)
       {
                  PostMessage (WiegWindow, WM_KEYDOWN, VK_DOWN, 0l);
                  return TRUE;
       }

	   if (current_wieg >= etiend)
	   {
	              wform = &EtiButton;
				  current = current_wieg - etiend;
	   }

       else if (CtrMode == 1)
       {
                  wform = &WieButtonT;
				  current = current_wieg;
       }
       else
       {
                  wform = &WieButton;
				  current = current_wieg;
       }

       if (CtrMode == 0 && current == autopos)
       {
                        PressAuto ();
       }
       else if (wform->mask[current].after)
       {
                       (*wform->mask[current].after) ();
       }
       else if (wform->mask[current].BuId)
       {
                       SendKey
                                 (wform->mask[current].BuId);
       }

	   else if (WithSpezEti)
	   {
                  if (EtiSpezButton.mask[current_wieg - wiegend].after)
				  {
                       (*EtiSpezButton.mask[current_wieg - wiegend].after) ();
				  }
                  else if (EtiSpezButton.mask[current_wieg - wiegend].BuId)
				  {
                       SendKey
                                 (EtiSpezButton.mask[current_wieg - wiegend].BuId);
				  }

	   }
	   else if (WithLagerAuswahl)
	   {
                  if (LgrSpezButton.mask[current_wieg - wiegend].after)
				  {
                       (*LgrSpezButton.mask[current_wieg - wiegend].after) ();
				  }
                  else if (LgrSpezButton.mask[current_wieg - wiegend].BuId)
				  {
                       SendKey
                                 (LgrSpezButton.mask[current_wieg - wiegend].BuId);
				  }
	   }
       return TRUE;
}

int TestAktivButton (int sign, int current_wieg)
/**
Status von Colbutton pruefen.
**/
{
       ColButton *Cub;
       int run;
       form *wform, *wform0;;
	   int current;

       if (CtrMode == 1)
       {
                  wform0= &WieButtonT;
       }
       else
       {
                  wform0= &WieButton;
       }

       run = 1;
       while (TRUE)
       {
              if (current_wieg >= wform0->fieldanz + EtiButton.fieldanz) break;
   	          if (current_wieg >= etiend)
			  {
				  wform = &EtiButton;
				  current = current_wieg - etiend;
			  }
			  else
			  {
				  wform = wform0;
				  current = current_wieg;
			  }
              Cub = (ColButton *) wform->mask[current].feld;
              if (((wform->mask[current].attribut & REMOVED) == 0) &&
				   (Cub->aktivate != -1)) break;
			  current_wieg += sign;
              if (sign == PLUS && current_wieg > wiegend)
              {
                                if (run == 2) break;
                                current_wieg = 0;
                                run ++;
              }
              else if (sign == MINUS && current_wieg < 0)
              {
                                if (run == 2) break;
                                current_wieg = wiegend;
                                run ++;
              }
       }
       return (current_wieg);
}


int IsWiegMessage (MSG *msg)
/**
HotKey Testen.
**/
{
         POINT mousepos;

         switch (msg->message)
         {
              case WM_KEYDOWN :
              {
                     TestEnter ();
                     switch (msg->wParam)
                     {
                            case VK_F5 :
                                    if (CtrMode == 1)
                                    {
                                          closetara ();
                                    }
                                    else
                                    {
                                          break_wieg = 1;
                                    }
                                    return TRUE;
                            case VK_RETURN :
                                    return (ReturnActionW ());
                            case VK_DOWN :
                            case VK_RIGHT :
                                    current_wieg ++;
                                    if (current_wieg > wiegend)
                                                current_wieg = 0;
                                    current_wieg = TestAktivButton (PLUS, current_wieg);
                                    SetWiegFocus ();
                                    return TRUE;
                            case VK_UP :
                            case VK_LEFT :
                                   current_wieg --;
                                   if (current_wieg < 0)
                                                current_wieg = wiegend;
                                    current_wieg = TestAktivButton (MINUS, current_wieg);
                                    SetWiegFocus ();
                                    return TRUE;
                            case VK_TAB :
                                    if (GetKeyState (VK_SHIFT) < 0)
                                    {
                                             current_wieg --;
                                             if (current_wieg < 0)
                                                  current_wieg = wiegend;
                                             current_wieg =
                                                 TestAktivButton (MINUS, current_wieg);
                                     }
                                     else
                                     {
                                             current_wieg ++;
                                             if (current_wieg > wiegend)
                                                  current_wieg = 0;
                                             current_wieg =
                                                 TestAktivButton (PLUS, current_wieg);
                                     }
                                     SetWiegFocus ();
                                     return TRUE;
                            default :
                                    return FALSE;
                     }
                     break;
              }
              case WM_LBUTTONDOWN :
              {
                    GetCursorPos (&mousepos);

                    if (MouseinWindow (artwform.mask[0].feldid, &mousepos) &&
						artwform.mask[0].attribut == EDIT)
                    {
						char buffer [255];
						if (_a_bas.charg_hand == 9)
						{
                          strcpy (buffer, aufps.ident_nr);
						  NuBlock.SetCharChangeProc (ChangeChar);
						  /*110908 so ist es auf den Touchern bei M�fli h�ngengeblieben
                          NuBlock.EnterNumBox (LogoWindow, "  Ident-Nr  ",
                                       buffer, -1, "", EntNuProc);
									   */
                          NuBlock.EnterNumBox (WiegWindow, "  Ident-Nr  ",
                                       buffer, -1, "", EntNuProc);    
						}
						else 
						{
                          strcpy (buffer, aufps.ls_charge); //110908
						  NuBlock.SetCharChangeProc (ChangeChar);//110908
/* 110908
                          EnterNumBox (WiegWindow,"  Chargen-Nr  ",
                                       buffer, 17,
                                       artwform.mask[0].picture);
									   */
                          NuBlock.EnterNumBox (WiegWindow,"  Chargen-Nr  ",
                                       buffer, -1, "", EntNuProc);    
						}
						if (syskey != KEY5)
						{
								if ((_a_bas.charg_hand == 9 || _a_bas.charg_hand == 1) && strlen (buffer) > 4)
								{
									Text Ean = buffer;
									ScanEan128Charge (Ean, 0);
								}
								else
								{
									strcpy (aufps.ls_charge, buffer); 
								}

								display_form (WiegWindow, &artwform, 0, 0);
								current_wieg = TestAktivButton (PLUS, current_wieg);
								SetWiegFocus ();
						 }
						 return TRUE;

                    }
              }
              case WM_USER :
                    if (LOWORD (msg->wParam) == BUTARA)
                    {
                          dotara ();
                          return TRUE;
                    }
         }
         return FALSE;
}

void PosArtFormW (void)
/**
Position fuer artform festlegen.
**/
{
        static int posOK = 0;
        int i, s, z;


        if (posOK) return;

        posOK = 1;
        for (i = 0; i < artwformT.fieldanz; i ++)
        {
                    s = artwformT.mask[i].pos [1];
                    z = artwformT.mask[i].pos [0];
                    KonvTextAbsPos (artwform.font,
                                    &z, &s);
                    artwformT.mask[i].pos [1] = s;
                    artwformT.mask[i].pos [0] = z;
                    s = artwformT.mask[i].length;
                    z = artwformT.mask[i].rows;
                    KonvTextAbsPos (artwform.font,
                                    &z, &s);
                    artwformT.mask[i].length = s;
                    artwformT.mask[i].rows = z;
        }
        for (i = 0; i < artwform.fieldanz; i ++)
        {
                    s = artwform.mask[i].pos [1];
                    z = artwform.mask[i].pos [0];
                    KonvTextAbsPos (artwform.font,
                                    &z, &s);
                    artwform.mask[i].pos [1] = s;
                    artwform.mask[i].pos [0] = z;
                    s = artwform.mask[i].length;
                    z = artwform.mask[i].rows;
                    KonvTextAbsPos (artwform.font,
                                    &z, &s);
                    artwform.mask[i].length = s;
                    artwform.mask[i].rows = z;
        }
        for (i = 0; i < fEntnahmeGewTxt.fieldanz; i ++)
        {
                    s = fEntnahmeGewTxt.mask[i].pos [1];
                    z = fEntnahmeGewTxt.mask[i].pos [0];
                    KonvTextAbsPos (fEntnahmeGew.font,
                                    &z, &s);
                    fEntnahmeGewTxt.mask[i].pos [1] = s;
                    fEntnahmeGewTxt.mask[i].pos [0] = z;
                    s = fEntnahmeGewTxt.mask[i].length;
                    z = fEntnahmeGewTxt.mask[i].rows;
                    KonvTextAbsPos (fEntnahmeGew.font,
                                    &z, &s);
                    fEntnahmeGewTxt.mask[i].length = s;
                    fEntnahmeGewTxt.mask[i].rows = z;
        }
        for (i = 0; i < fEntnahmeGew.fieldanz; i ++)
        {
                    s = fEntnahmeGew.mask[i].pos [1];
                    z = fEntnahmeGew.mask[i].pos [0];
                    KonvTextAbsPos (fEntnahmeGew.font,
                                    &z, &s);
                    fEntnahmeGew.mask[i].pos [1] = s;
                    fEntnahmeGew.mask[i].pos [0] = z;
                    s = fEntnahmeGew.mask[i].length;
                    z = fEntnahmeGew.mask[i].rows;
                    KonvTextAbsPos (fEntnahmeGew.font,
                                    &z, &s);
                    fEntnahmeGew.mask[i].length = s;
                    fEntnahmeGew.mask[i].rows = z;
        }
}

void PosArtFormC (void)
/**
Position fuer artform festlegen.
**/
{
        static int posOK = 0;
        int i, s, z;

        if (posOK) return;

        posOK = 1;
        for (i = 0; i < artwconstT.fieldanz; i ++)
        {
                    s = artwconstT.mask[i].pos [1];
                    z = artwconstT.mask[i].pos [0];
                    KonvTextAbsPos (artwconst.font,
                                    &z, &s);
                    artwconstT.mask[i].pos [1] = s;
                    artwconstT.mask[i].pos [0] = z;
                    s = artwconstT.mask[i].length;
                    z = artwconstT.mask[i].rows;
                    KonvTextAbsPos (artwconst.font,
                                    &z, &s);
                    artwconstT.mask[i].length = s;
                    artwconstT.mask[i].rows = z;
        }
        for (i = 0; i < startwieg.fieldanz; i ++)
        {
                    s = startwieg.mask[i].pos [1];
                    z = startwieg.mask[i].pos [0];
                    KonvTextAbsPos (startwieg.font,
                                    &z, &s);
                    startwieg.mask[i].pos [1] = s;
                    startwieg.mask[i].pos [0] = z;
                    s = startwieg.mask[i].length;
                    z = startwieg.mask[i].rows;
                    KonvTextAbsPos (startwieg.font,
                                    &z, &s);
                    startwieg.mask[i].length = s;
                    startwieg.mask[i].rows = z;
        }
        for (i = 0; i < artwconst.fieldanz; i ++)
        {
                    s = artwconst.mask[i].pos [1];
                    z = artwconst.mask[i].pos [0];
                    KonvTextAbsPos (artwconst.font,
                                    &z, &s);
                    artwconst.mask[i].pos [1] = s;
                    artwconst.mask[i].pos [0] = z;
                    s = artwconst.mask[i].length;
                    z = artwconst.mask[i].rows;
                    KonvTextAbsPos (artwconst.font,
                                    &z, &s);
                    artwconst.mask[i].length = s;
                    artwconst.mask[i].rows = z;
        }
}

void PosArtFormP (void)
/**
Position fuer artform festlegen.
**/
{
        static int posOK = 0;
        int i, s, z;

        if (posOK) return;

        posOK = 1;
        for (i = 0; i < artwformT.fieldanz; i ++)
        {
                    s = artwformT.mask[i].pos [1];
                    z = artwformT.mask[i].pos [0];
                    KonvTextAbsPos (artwform.font,
                                    &z, &s);
                    artwformT.mask[i].pos [1] = s;
                    artwformT.mask[i].pos [0] = z;
                    s = artwformT.mask[i].length;
                    z = artwformT.mask[i].rows;
                    KonvTextAbsPos (artwform.font,
                                    &z, &s);
                    artwformT.mask[i].length = s;
                    artwformT.mask[i].rows = z;
        }
        for (i = 0; i < artwform.fieldanz; i ++)
        {
                    s = artwform.mask[i].pos [1];
                    z = artwform.mask[i].pos [0];
                    KonvTextAbsPos (artwform.font,
                                    &z, &s);
                    artwform.mask[i].pos [1] = s;
                    artwform.mask[i].pos [0] = z;
                    s = artwform.mask[i].length;
                    z = artwform.mask[i].rows;
                    KonvTextAbsPos (artwform.font,
                                    &z, &s);
                    artwform.mask[i].length = s;
                    artwform.mask[i].rows = z;
        }
        for (i = 0; i < fEntnahmeGewTxt.fieldanz; i ++)
        {
                    s = fEntnahmeGewTxt.mask[i].pos [1];
                    z = fEntnahmeGewTxt.mask[i].pos [0];
                    KonvTextAbsPos (fEntnahmeGewTxt.font,
                                    &z, &s);
                    fEntnahmeGewTxt.mask[i].pos [1] = s;
                    fEntnahmeGewTxt.mask[i].pos [0] = z;
                    s = fEntnahmeGewTxt.mask[i].length;
                    z = fEntnahmeGewTxt.mask[i].rows;
                    KonvTextAbsPos (fEntnahmeGewTxt.font,
                                    &z, &s);
                    fEntnahmeGewTxt.mask[i].length = s;
                    fEntnahmeGewTxt.mask[i].rows = z;
        }
        for (i = 0; i < fEntnahmeGew.fieldanz; i ++)
        {
                    s = fEntnahmeGew.mask[i].pos [1];
                    z = fEntnahmeGew.mask[i].pos [0];
                    KonvTextAbsPos (fEntnahmeGew.font,
                                    &z, &s);
                    fEntnahmeGew.mask[i].pos [1] = s;
                    fEntnahmeGew.mask[i].pos [0] = z;
                    s = fEntnahmeGew.mask[i].length;
                    z = fEntnahmeGew.mask[i].rows;
                    KonvTextAbsPos (fEntnahmeGewTxt.font,
                                    &z, &s);
                    fEntnahmeGew.mask[i].length = s;
                    fEntnahmeGew.mask[i].rows = z;
        }
}

void PosMeForm (int yw, int xw, int cyw, int cxw)
/**
Position fuer WiegKgMe festlegen.
**/
{
        static int posOK = 0;
        int s, z;

        if (posOK) return;

        posOK = 1;

        WiegKgMe.mask[0].pos[0] = yw + WiegKgE.mask[0].pos[0];
        WiegKgMe.mask[0].pos[1] = xw + cxw + 30;
        s = WiegKgMe.mask[0].length;
        z = WiegKgMe.mask[0].rows;
        KonvTextAbsPos (WiegKgMe.font, &z, &s);
        WiegKgMe.mask[0].length = s;
        WiegKgMe.mask[0].rows = z;
}

void GetZentrierWerte (int *buglen, int *buabs, int wlen, int anz,
                       int abs, int size)
/**
Werte fuer Zentrierung ermitteln.
**/
{
        int blen;
        int spos;

        while (TRUE)
        {
                 blen = anz * (size + abs) - abs;
                 spos = (wlen - blen) / 2;
                 if (spos > 9)
                 {
                              *buglen = blen;
                              *buabs  = abs + size;
                              return;
                 }
                 abs -= 10;
                 if (abs <= 0) return;
       }
}

int closetara (void)
/**
Tara-Leiste schliessen.
**/
{
        CtrMode = 0;
        CloseControls (&WieButtonT);
        current_wieg = 0;
        if (cfg_ohne_preis > 0)
        {
                 wiegend = 8;
        }
        else
        {
                 wiegend = 9;
        }
        return 0;
}

void testeti (void)
/**
Wiegen durchfuehren.
**/
{
		double a;
		int dsqlstatus;

		a = ratod (aufps.a);

		DbClass.sqlin ((long *)   &sys_peri.sys, 2, 0);
		DbClass.sqlin ((double *) &a, 3, 0);
        DbClass.sqlout ((long *) &eti_nr, 2, 0);
        DbClass.sqlout ((char *) krz_txt, 0, 17);
		dsqlstatus = DbClass.sqlcomm ("select eti_nr, krz_txt from klebdest "
			                          "where sys = ? "
									  "and a = ? "
									  "and we = 1 "
									  "and einz = 1");
		if (dsqlstatus)
		{
			WithEti = FALSE;
		}
		else
		{
			WithEti = TRUE;
		}

		DbClass.sqlin ((long *)   &sys_peri.sys, 2, 0);
		DbClass.sqlin ((double *) &a, 3, 0);
        DbClass.sqlout ((long *) &sum_eti_nr, 2, 0);
        DbClass.sqlout ((char *) sum_krz_txt, 0, 17);
		dsqlstatus = DbClass.sqlcomm ("select eti_nr, krz_txt from klebdest "
			                          "where sys = ? "
									  "and a = ? "
									  "and we = 1 "
									  "and summ = 1");
		if (dsqlstatus)
		{
			WithSumEti = FALSE;
		}
		else
		{
			WithSumEti = TRUE;
		}
		if (WithEti == FALSE && WithSumEti == FALSE)
		{
			EtiButton.mask[0].attribut = REMOVED;
			EtiButton.mask[1].attribut = REMOVED;
			wiegend = 9;
		}
		else
		{
			EtiButton.mask[0].attribut = COLBUTTON;
			EtiButton.mask[1].attribut = COLBUTTON;
			wiegend = 9;
		}

/*
		wiegend = 6;
		if (WithEti) wiegend ++;
		if (WithSumEti) wiegend ++;
		if (WithSpezEti) wiegend ++;
		if (WithZusatz) wiegend ++;
*/
		if (WithEti)
		{
            ActivateColButton (NULL, &EtiButton, 0, 2, 0);
		}
		else
		{
            ActivateColButton (NULL, &EtiButton, 0, -1, 0);
		}

		if (WithSumEti)
		{
            ActivateColButton (NULL, &EtiButton, 1, 2, 0);
		}
		else
		{
            ActivateColButton (NULL, &EtiButton, 1, -1, 0);
		}
}




static char EtiPath [512];

void WriteParamKiste (int etianz)
/**
Parameter-Datei fuer Kistenetikettschreiben.
**/
{
	    char *tmp;
		char *etc;
	    char dname [512];
		FILE *pa;

		tmp = getenv ("TMPPATH");
		if (tmp == NULL) return;
		etc = getenv ("BWSETC");
		if (etc == NULL) return;


        adr_class.lese_adr (lief.adr);

		sprintf (dname, "%s\\parafile", tmp);
		pa = fopen (dname, "w");
		if (pa == NULL) return;

		fprintf (pa, "$$LIEF$;%s\n",      we_kopf.lief);
		fprintf (pa, "$$LIEFNR$;%s\n",    we_kopf.lief_rech_nr);
		fprintf (pa, "$$ABEZ1;%s\n",     aufps.a_bz1);
		fprintf (pa, "$$ABEZ2;%s\n",     aufps.a_bz2);
		fprintf (pa, "$$ARTNR;%.0lf\n",  ratod (aufps.a));
		fprintf (pa, "$$WEDAT;%s\n",     lieferdatum);
		fprintf (pa, "$$NETTOG;%.3lf\n",  eti_netto);
		fprintf (pa, "$$TARAG;%.3lf\n",   eti_tara);
		fprintf (pa, "$$BRUTTOG;%.3lf\n", eti_brutto);
		fprintf (pa, "$$STUECK;%.2lf\n",  eti_stk );
		fprintf (pa, "$$LIEFNAME;%s\n",   lief_krz1);
		fprintf (pa, "$$CHARGE;%s\n",     aufps.ls_charge);
		fprintf (pa, "$$MHD;%s\n",        aufps.hbk_date);
		fprintf (pa, "$$NAME1;%s\n",      _adr.adr_nam1);
		fprintf (pa, "$$NAME2;%s\n",      _adr.adr_nam2);
		fprintf (pa, "$$STRASSE;%s\n",    _adr.str);
		fprintf (pa, "$$PLZ;%s\n",        _adr.plz);
		fprintf (pa, "$$ORT;%s\n",        _adr.ort1);
		fprintf (pa, "$$ANZAHL;%d\n",     etianz);
		fprintf (pa, "$$WIEGANZ;%ld\n",  atol (wiegzahl));

		fclose (pa);

/*
        sprintf (progname, "%s\\BIN\\rswrun put_eti %s %s\\%s %s\\parafile %d",
                                     rosipath, eti_ki_drk, etc, eti_kiste, tmp, etianz);
        ProcWaitExec (progname, SW_SHOWMINIMIZED, 0, 40, -1, 0);
*/

        sprintf (EtiPath, "%s\\%s", etc, eti_kiste);

        int ret = PutEti (eti_ki_drk, EtiPath, dname, etianz, eti_ki_typ) ;
        if (ret == -1)
        {
            disp_messG ("Etikett kann nicht gedruckt werden", 2);
        }
}

void WriteParamEZ (int etianz)
/**
Parameter-Datei fuer Kistenetikettschreiben.
**/
{
	    char *tmp;
		char *etc;
	    char dname [512];
		FILE *pa;

		tmp = getenv ("TMPPATH");
		if (tmp == NULL) return;
		etc = getenv ("BWSETC");
		if (etc == NULL) return;

        adr_class.lese_adr (lief.adr);

		sprintf (dname, "%s\\parafile", tmp);
		pa = fopen (dname, "w");
		if (pa == NULL) return;
		fprintf (pa, "$$LIEF;%s\n",      we_kopf.lief);
		fprintf (pa, "$$LIEFNR;%s\n",    we_kopf.lief_rech_nr);
		fprintf (pa, "$$ABEZ1;%s\n",     aufps.a_bz1);
		fprintf (pa, "$$ABEZ2;%s\n",     aufps.a_bz2);
		fprintf (pa, "$$ARTNR;%.0lf\n",  ratod (aufps.a));
		fprintf (pa, "$$WEDAT;%s\n",     lieferdatum);
		fprintf (pa, "$$NETTOG;%.3lf\n",  eti_netto);
		fprintf (pa, "$$TARAG;%.3lf\n",   eti_tara);
		fprintf (pa, "$$BRUTTOG;%.3lf\n", eti_brutto);
		fprintf (pa, "$$STUECK;%.2lf\n",  eti_stk );
		fprintf (pa, "$$LIEFNAME;%s\n",   lief_krz1);
		fprintf (pa, "$$CHARGE;%s\n",     aufps.ls_charge);
		fprintf (pa, "$$MHD;%s\n",        aufps.hbk_date);
		fprintf (pa, "$$NAME1;%s\n",      _adr.adr_nam1);
		fprintf (pa, "$$NAME2;%s\n",      _adr.adr_nam2);
		fprintf (pa, "$$STRASSE;%s\n",    _adr.str);
		fprintf (pa, "$$PLZ;%s\n",        _adr.plz);
		fprintf (pa, "$$ORT;%s\n",        _adr.ort1);
		fprintf (pa, "$$ANZAHL;%d\n",     etianz);
		fprintf (pa, "$$WIEGANZ;%ld\n",  atol (wiegzahl));

		fclose (pa);
/*
        sprintf (progname, "%s\\BIN\\rswrun put_eti %s %s\\%s %s\\parafile %d",
                                     rosipath, eti_ez_drk, etc, eti_ez, tmp, etianz);
        ProcWaitExec (progname, SW_SHOWMINIMIZED, 0, 40, -1, 0);
*/

        sprintf (EtiPath, "%s\\%s", etc, eti_ez);

        int ret = PutEti (eti_ez_drk, EtiPath, dname, etianz, eti_ez_typ) ;
        if (ret == -1)
        {
            disp_messG ("Etikett kann nicht gedruckt werden", 2);
        }

}

void WriteParamEV (int etianz)
/**
Parameter-Datei fuer Kistenetikettschreiben.
**/
{
	    char *tmp;
		char *etc;
	    char dname [512];
		FILE *pa;

		tmp = getenv ("TMPPATH");
		if (tmp == NULL) return;
		etc = getenv ("BWSETC");
		if (etc == NULL) return;


        adr_class.lese_adr (lief.adr);

		sprintf (dname, "%s\\parafile", tmp);
		pa = fopen (dname, "w");
		if (pa == NULL) return;
		fprintf (pa, "$$LIEF;%s\n",      we_kopf.lief);
		fprintf (pa, "$$LIEFNR;%s\n",    we_kopf.lief_rech_nr);
		fprintf (pa, "$$ABEZ1;%s\n",     aufps.a_bz1);
		fprintf (pa, "$$ABEZ2;%s\n",     aufps.a_bz2);
		fprintf (pa, "$$ARTNR;%.0lf\n",  ratod (aufps.a));
		fprintf (pa, "$$WEDAT;%s\n",     lieferdatum);
		fprintf (pa, "$$NETTOG;%.3lf\n",  eti_netto);
		fprintf (pa, "$$TARAG;%.3lf\n",   eti_tara);
		fprintf (pa, "$$BRUTTOG;%.3lf\n", eti_brutto);
		fprintf (pa, "$$STUECK;%.2lf\n",  eti_stk );
		fprintf (pa, "$$LIEFNAME;%s\n",   lief_krz1);
		fprintf (pa, "$$CHARGE;%s\n",     aufps.ls_charge);
		fprintf (pa, "$$MHD;%s\n",        aufps.hbk_date);
		fprintf (pa, "$$NAME1;%s\n",      _adr.adr_nam1);
		fprintf (pa, "$$NAME2;%s\n",      _adr.adr_nam2);
		fprintf (pa, "$$STRASSE;%s\n",    _adr.str);
		fprintf (pa, "$$PLZ;%s\n",        _adr.plz);
		fprintf (pa, "$$ORT;%s\n",        _adr.ort1);
		fprintf (pa, "$$ANZAHL;%d\n",     etianz);

		fclose (pa);
/*
        sprintf (progname, "%s\\BIN\\rswrun put_eti %s %s\\%s %s\\parafile %d",
                                     rosipath, eti_ev_drk, etc, eti_ev, tmp, etianz);
        ProcWaitExec (progname, SW_SHOWMINIMIZED, 0, 40, -1, 0);
*/

        sprintf (EtiPath, "%s\\%s", etc, eti_ev);

        int ret = PutEti (eti_ev_drk, EtiPath, dname, etianz, eti_ev_typ) ;
        if (ret == -1)
        {
            disp_messG ("Etikett kann nicht gedruckt werden", 2);
        }

}

void WriteParamArt (int etianz)
/**
Parameter-Datei fuer Kistenetikettschreiben.
**/
{
	    char *tmp;
		char *etc;
	    char dname [512];
		FILE *pa;
		char cdatum [12];

		tmp = getenv ("TMPPATH");
		if (tmp == NULL) return;
		etc = getenv ("BWSETC");
		if (etc == NULL) return;
		sysdate(cdatum);


        adr_class.lese_adr (lief.adr);

		sprintf (dname, "%s\\parafile", tmp);
		pa = fopen (dname, "w");
		if (pa == NULL) return;
		fprintf (pa, "$$LIEF;%s\n",      we_kopf.lief);
		fprintf (pa, "$$LIEFNR;%s\n",    we_kopf.lief_rech_nr);
		fprintf (pa, "$$CHARGE;%s\n",     aufps.ls_charge);
		fprintf (pa, "$$DATUM;%s\n",     cdatum);
		fprintf (pa, "$$WEDAT;%s\n",     lieferdatum);
		fprintf (pa, "$$ABEZ1;%s\n",     aufps.a_bz1);
		fprintf (pa, "$$ABEZ2;%s\n",     aufps.a_bz2);
		fprintf (pa, "$$ARTNR;%.0lf\n",  ratod (aufps.a));
		fprintf (pa, "$$NETTOG;%.3lf\n",  eti_netto);
		fprintf (pa, "$$TARAG;%.3lf\n",   eti_tara);
		fprintf (pa, "$$BRUTTOG;%.3lf\n", eti_brutto);
		fprintf (pa, "$$STUECK;%.2lf\n",  eti_stk );
		fprintf (pa, "$$LIEFNAME;%s\n",   lief_krz1);
		fprintf (pa, "$$MHD;%s\n",        aufps.hbk_date);
		fprintf (pa, "$$NAME1;%s\n",      _adr.adr_nam1);
		fprintf (pa, "$$NAME2;%s\n",      _adr.adr_nam2);
		fprintf (pa, "$$STRASSE;%s\n",    _adr.str);
		fprintf (pa, "$$PLZ;%s\n",        _adr.plz);
		fprintf (pa, "$$ORT;%s\n",        _adr.ort1);
		fprintf (pa, "$$ANZAHL;%d\n",     etianz);
		fprintf (pa, "$$WIEGANZ;%ld\n",  atol (wiegzahl));

		fclose (pa);
/*
        sprintf (progname, "%s\\BIN\\rswrun put_eti %s %s\\%s %s\\parafile %d",
                                     rosipath, eti_a_drk, etc, eti_artikel, tmp, etianz);
        ProcWaitExec (progname, SW_SHOWMINIMIZED, 0, 40, -1, 0);
*/

        sprintf (EtiPath, "%s\\%s", etc, eti_artikel);

        int ret = PutEti (eti_a_drk, EtiPath, dname, etianz, eti_a_typ) ;
        if (ret == -1)
        {
            disp_messG ("Etikett kann nicht gedruckt werden", 2);
        }
}




void WriteParafile (char *tmp)
/**
Parameter-Datei fuer put_eti schreiben.
**/
{
	    char dname [512];
		FILE *pa;

		sprintf (dname, "%s\\parafile", tmp);
		pa = fopen (dname, "w");
		if (pa == NULL) return;
		fprintf (pa, "$$LIEF;%s\n",      we_kopf.lief);
		fprintf (pa, "$$LIEFNR;%s\n",    we_kopf.lief_rech_nr);
		fprintf (pa, "$$ABEZ1;%s\n",     aufps.a_bz1);
		fprintf (pa, "$$ABEZ2;%s\n",     aufps.a_bz2);
		fprintf (pa, "$$ARTNR;%.0lf\n",  ratod (aufps.a));
		fprintf (pa, "$$WEDAT;%s\n",     lieferdatum);
		fprintf (pa, "$$NETTOG;%.3lf\n",  eti_netto);
		fprintf (pa, "$$TARAG;%.3lf\n",   eti_tara);
		fprintf (pa, "$$BRUTTOG;%.3lf\n", eti_brutto);
		fprintf (pa, "$$STUECK;%.2lf\n",  eti_stk );
		fprintf (pa, "$$ANZAHL;%ld\n",    atol (wiegzahl));
		fprintf (pa, "$$LIEFNAME;%s\n",   lief_krz1);
		fprintf (pa, "$$CHARGE;%s\n",     aufps.ls_charge);
		fprintf (pa, "$$MHD;%s\n",        aufps.hbk_date);
		fclose (pa);

		syskey = 0;
}

void WriteSumParafile (char *tmp)
/**
Parameter-Datei fuer put_eti schreiben.
**/
{
	    char dname [512];
		FILE *pa;

		sprintf (dname, "%s\\parafile", tmp);
		pa = fopen (dname, "w");
		if (pa == NULL) return;
		fprintf (pa, "$$LIEF;%s\n",      we_kopf.lief);
		fprintf (pa, "$$LIEFNR;%s\n",    we_kopf.lief_rech_nr);
		fprintf (pa, "$$ABEZ1;%s\n",     aufps.a_bz1);
		fprintf (pa, "$$ABEZ2;%s\n",     aufps.a_bz2);
		fprintf (pa, "$$ARTNR;%.0lf\n",  ratod (aufps.a));
		fprintf (pa, "$$WEDAT;%s\n",     lieferdatum);
		fprintf (pa, "$$NETTOG;%.3lf\n",  sum_eti_netto);
		fprintf (pa, "$$TARAG;%.3lf\n",   sum_eti_tara);
		fprintf (pa, "$$BRUTTOG;%.3lf\n", sum_eti_brutto);
		fprintf (pa, "$$STUECK;%.2lf\n",  sum_eti_stk );
		fprintf (pa, "$$ANZAHL;%ld\n",    atol (wiegzahl));
		fprintf (pa, "$$LIEFNAME;%s\n",   lief_krz1);
		fprintf (pa, "$$CHARGE;%s\n",     aufps.ls_charge);
		fprintf (pa, "$$MHD;%s\n",        aufps.hbk_date);
		fclose (pa);

/*
        sum_eti_netto  = (double) 0.0 ;
        sum_eti_brutto = (double) 0.0;
		sum_eti_tara   = (double) 0.0;
		sum_eti_stk    = (double) 0.0;
*/
}

void FromChargeBsd ()
{
	TEXTBLOCK::Rows.DestroyAll ();
	Chargen.FirstPosition ();
	Text *txt;
	while ((txt = (Text *) Chargen.GetNext ()) != NULL)
	{
		TEXTBLOCK::Rows.Add (new Text (*txt));
	}
}

void ToChargeBsd ()
{
	Chargen.DestroyAll ();
	int anz = TEXTBLOCK::Rows.GetCount ();
	for (int i = anz - 1; i >= 0; i --)
	{
		Text *txt = (Text *) TEXTBLOCK::Rows.Get (i);
		txt->Trim ();
		if (*txt != "")
		{
			break;
		}
	}

	anz = i + 1;
	for (i = 0; i < anz; i ++)
	{
		Text *txt = (Text *) TEXTBLOCK::Rows.Get (i);
		Text *Charge = new Text (*txt);
		Chargen.Add (Charge);
	}
	TEXTBLOCK::Rows.DestroyAll ();
}


void BucheChargeBsd (double a, short me_einh_kun, double pr_vk, char *charge)
/**
Bestandsbuchung vorbereiten.
**/
{
//	double lief_me_vgl;
	double buchme;
	char datum [12];
//    long lgrort;

/*
	if (bsd_kz == 0) return;
	if (we_kopf.bsd_verb_kz[0] != 'J')
	{
		return;
	}

	if (BsdArtikel (a) == FALSE) return;
*/

//    me_vgl = GetMeVgl (a, auf_me,me_einh_lief);
//if (me_vgl == AktBsdMe) return;
	buchme = 0;

	bsd_buch.nr  = best_kopf.best_blg;
	strcpy (bsd_buch.blg_typ, "WC"); 
 	bsd_buch.mdn = we_kopf.mdn;
	bsd_buch.fil = we_kopf.fil;
	bsd_buch.kun_fil = 0;
	bsd_buch.a   = a;
	sysdate (datum);
	bsd_buch.dat = dasc_to_long (datum);
	systime (bsd_buch.zeit);
	strcpy (bsd_buch.pers, sys_ben.pers);

//    sprintf (bsd_buch.bsd_lgr_ort, "%ld", akt_lager);

    sprintf (bsd_buch.bsd_lgr_ort, "%ld", 0l);
	bsd_buch.qua_status = 0;
	bsd_buch.me = 0;
	bsd_buch.bsd_ek_vk = pr_vk;
    strcpy (bsd_buch.chargennr, charge);
    strcpy (bsd_buch.ident_nr, aufps.ident_nr);
    strcpy (bsd_buch.herk_nachw, "");
    sprintf (bsd_buch.lief, "%s", we_kopf.lief);
    bsd_buch.auf = best_kopf.best_blg;
    strcpy  (bsd_buch.verfall, "");
    bsd_buch.delstatus = 0;
    strcpy  (bsd_buch.err_txt, we_kopf.lief_rech_nr);
	BsdBuch.dbinsert ();
}

void BucheChargenBsd ()
{

	Chargen.FirstPosition ();
	Text *Charge;
	while ((Charge = (Text *) Chargen.GetNext ()) != NULL)
	{
		
        if (lief.waehrung == 2)
        {
				BucheChargeBsd (ratod (aufps.a), atoi (aufps.me_einh_kun), 
			           ratod (aufps.ls_lad_euro), Charge->GetBuffer ());
		}
		else
        {
				BucheChargeBsd (ratod (aufps.a), atoi (aufps.me_einh_kun), 
			           ratod (aufps.auf_lad_pr), Charge->GetBuffer ());
		}
					   
	}
}


void EnterRCharge (int value)
{
		textblock = new TEXTBLOCK;
		textblock->SetLongText (TRUE);
	    FromChargeBsd ();
		EnableWindows (hMainWindow, FALSE);
		EnableWindow (WiegWindow, FALSE);
        textblock->MainInstance (hMainInst, WiegWindow);
        textblock->ScreenParam (scrfx, scrfy);
        textblock->SetUpshift (TRUE);
        textblock->EnterTextBox (BackWindow1, "Chargen", "", 0, "", EntNumProc);
		EnableWindows (hMainWindow, TRUE);
		EnableWindow (WiegWindow, TRUE);
		delete textblock;
		textblock = NULL;
		if (syskey != KEY5)
		{
				ToChargeBsd ();
		}
		syskey = 0;
		return;
}

void EnterREz (int value)
{
	    char buffer [80] = {""};
		
		strcpy (buffer, aufps.ez_nr);
		textblock = new TEXTBLOCK;
		textblock->SetLongText (FALSE);
		EnableWindows (hMainWindow, FALSE);
		EnableWindow (WiegWindow, FALSE);
        textblock->MainInstance (hMainInst, WiegWindow);
        textblock->ScreenParam (scrfx, scrfy);
        textblock->SetUpshift (TRUE);
        textblock->EnterTextBox (BackWindow1, "EZ-Nummer", buffer, 20, "", EntNumProc);
		EnableWindows (hMainWindow, TRUE);
		EnableWindow (WiegWindow, TRUE);
		delete textblock;
		textblock = NULL;
		if (syskey != KEY5)
		{
			strcpy (aufps.ez_nr, buffer);
		}
		syskey = 0;
}

void EnterREs (int value)
{

	    char buffer [80] = {""};
		
		strcpy (buffer, aufps.es_nr);
		textblock = new TEXTBLOCK;
		textblock->SetLongText (FALSE);
		EnableWindows (hMainWindow, FALSE);
		EnableWindow (WiegWindow, FALSE);
        textblock->MainInstance (hMainInst, WiegWindow);
        textblock->ScreenParam (scrfx, scrfy);
        textblock->SetUpshift (TRUE);
        textblock->EnterTextBox (BackWindow1, "ES-Nummer", buffer, 20, "", EntNumProc);
		EnableWindows (hMainWindow, TRUE);
		EnableWindow (WiegWindow, TRUE);
		delete textblock;
		textblock = NULL;
		if (syskey != KEY5)
		{
			strcpy (aufps.es_nr, buffer);
		}
		syskey = 0;
}

void EnterRLand (int value)
{
	    char buffer [80] = {""};
	    
        ShowTierLand (buffer);
		if (syskey != KEY5)
		{
			strcpy (aufps.laender, buffer);
		}
		syskey = 0;
}


void SetBuTexteZusatzdaten (void)
{
        static Text text1 ;
        static Text text2 ; 
        static Text text3 ; 
        static Text text4 ; 
        static Text text5 ; 

        text1 = "Mehrfach\n&Chargen"; 
        text2 = "Fang&art"; 
        text3 = "Fang&gebiet"; 
        text4 = "Fang&datum"; 
        text5 = "&Box"; 
	int i;
			for  (i = 0;i < fangartanz;i++) 
			{
				if (atoi(fangartarr[i].nr) == atoi(aufps.ez_nr))
				{
					text2 = "Fang&art\n";
					text2 += clipped(fangartarr[i].bz);
					break;
				}
			}
			for  (i = 0;i < fanggebietanz;i++) 
			{
				if (atoi(fanggebietarr[i].nr) == atoi(aufps.es_nr))
				{
					text3 = "Fang&gebiet\n";
					text3 += clipped(fanggebietarr[i].bz);
					break;
				}
			}
			if (strlen(aufps.erz_date) > 5) 
			{
		        text4 = "Fang&datum\n"; 
				text4 += clipped(aufps.erz_date);
			}
			if (strlen(aufps.ident_nr) > 0) 
			{
		        text5 = "&Box\n"; 
				text5 += clipped(aufps.ident_nr);
			}
			if (cfg_MehrfachChargen == 1)
			{
				Touchf.SetEtiText (text1.GetBuffer (),
				           text2.GetBuffer (),
				           text3.GetBuffer (),
				           text4.GetBuffer (),
						   text5.GetBuffer ());
			}
			else
			{
				Touchf.SetEtiText (
				           text2.GetBuffer (),
				           text3.GetBuffer (),
				           text4.GetBuffer (),
						   text5.GetBuffer ());
			}
}


void AuswahlFangart (int value)
{
	    char buffer [80] = {""};
        static Text text1 = "Mehrfach\n&Chargen"; 
        static Text text2 = "Fang&art"; 
        static Text text3 = "Fang&gebiet"; 
        static Text text4 = "Fang&datum"; 
        static Text text5 = "&Box"; 
		int i;
	    
        ShowFangart (buffer);
		if (syskey != KEY5)
		{
	        text1 = "Mehrfach\n&Chargen"; 
			text2 = "Fang&art"; 
			text3 = "Fang&gebiet"; 
			text4 = "Fang&datum"; 
			text5 = "&Box"; 


			for  (i = 0;i < fangartanz;i++) 
			{
				if (atoi(fangartarr[i].nr) == atoi(buffer))
				{
					/*
					text2 = "Fang&art\n";
					text2 += fangartarr[i].bz;
					Touchf.SetEtiText (text1.GetBuffer (),
				           text2.GetBuffer (),
				           text3.GetBuffer (),
				           text4.GetBuffer (),
						   text5.GetBuffer ());
						   */
					strcpy (aufps.ez_nr, buffer);
					SetBuTexteZusatzdaten();
					break;
				}
			}
		}
		syskey = 0;
}
void AuswahlFanggebiet (int value)
{
	    char buffer [80] = {""};
        static Text text1 = "Mehrfach\n&Chargen"; 
        static Text text2 = "Fang&art"; 
        static Text text3 = "Fang&gebiet"; 
        static Text text4 = "Fang&datum"; 
        static Text text5 = "&Box"; 

	    int i;
        ShowFanggebiet (buffer);
		if (syskey != KEY5)
		{
	        text1 = "Mehrfach\n&Chargen"; 
			text2 = "Fang&art"; 
			text3 = "Fang&gebiet"; 
			text4 = "Fang&datum"; 
			text5 = "&Box"; 


			for  (i = 0;i < fanggebietanz;i++) 
			{
				if (atoi(fanggebietarr[i].nr) == atoi(buffer))
				{
					/*
					text3 = "Fang&gebiet\n";
					text3 += fanggebietarr[i].bz;
					Touchf.SetEtiText (text1.GetBuffer (),
				           text2.GetBuffer (),
				           text3.GetBuffer (),
				           text4.GetBuffer (),
						   text5.GetBuffer ());
						   */
					strcpy (aufps.es_nr, buffer);
					SetBuTexteZusatzdaten();
					break;
				}
			}
		


		}
		syskey = 0;
}

void EnterBoxNr (int value)
{

	    char buffer [80] = {""};
		
		strcpy (buffer, aufps.ident_nr);
			  NuBlock.SetCharChangeProc (ChangeChar);
			  NuBlock.SetParent (eWindow);
              NuBlock.EnterNumBox (WiegWindow, "  Box-Nr  ",
                                   buffer, -1, "", EntNuProc);
		if (syskey != KEY5)
		{
			if (strlen (buffer) > 4)
			{
				Text Ean = buffer;
				ScanEan128Charge (Ean,1);
			}
			else
			{
				strcpy (aufps.ident_nr, buffer);
			}
			SetBuTexteZusatzdaten();
		}
		syskey = 0;
}


void EnterFangdatum (int value)
{

	    char buffer [80] = {""};
		
			  if (strlen(aufps.erz_date) < 6)
			  {
				sysdate (buffer);
			  }
			  else
			  {
				  strcpy(buffer,aufps.erz_date);
			  }

			  NuBlock.SetCharChangeProc (ChangeChar);
			  NuBlock.SetParent (eWindow);
              NuBlock.EnterDateBox (WiegWindow, "  Fangdatum  ",
                                   buffer, 11, "dd.mm.yyyy", EntNuProc);
		if (syskey != KEY5)
		{
			strcpy (aufps.erz_date, buffer);
			SetBuTexteZusatzdaten();
		}
		syskey = 0;
}

void FillCountries ()
{
		static CVector Countries;
		PTABN *p;
		char *c;
		Token t;

		if (Countries.GetCount () == 0)
		{
			int dsqlstatus = ptab_class.lese_ptab_all ("staat");
			while (dsqlstatus == 0)
			{
				p = new PTABN;
				memcpy (p, &ptabn, sizeof (PTABN));
				Countries.Add (p);
				dsqlstatus = ptab_class.lese_ptab_all ();
			}
		}

		t.SetSep (" ");
		t = aufps.laender;
		int i = 0;
		while ((c = t.NextToken ()) != NULL)
		{
			Text L1 = c;
			L1.Trim ();
			Countries.FirstPosition ();
			while ((p = (PTABN *) Countries.GetNext ()) != NULL)
			{
				Text L2 = p->ptwer2;
				L2.Trim ();
				if (L1 == L2) break;
			}
			if (p == NULL)
			{
				continue;
			}
			switch (i)
			{
			case 0:
				rind.gebland = atoi (p->ptwert);
				break;
			case 1:
				rind.mastland = atoi (p->ptwert);
				break;
			case 2:
				rind.schlaland = atoi (p->ptwert);
				break;
			case 3:
				rind.zerlland = atoi (p->ptwert);
				break;
			}
			i ++;
			if (i > 3) break;
		}
}


void FillZerlLfd ()
{
	static int cursor = -1;
	static int ins_cursor = -1;
	static long lfd = 0;

	lfd = 0;
	if (cursor == -1)
	{
		DbClass.sqlout ((long *) &lfd, 2, 0);
		cursor = DbClass.sqlcursor ("select max (lfd) from zerldaten");
		DbClass.sqlin ((long *) &zerldaten.lfd, 2, 0);
		DbClass.sqlin ((long *) &zerldaten.lfd_i, 2, 0);
		ins_cursor = DbClass.sqlcursor ("update zerldaten set lfd_i = ? where lfd = ?");
	}
	if (cursor == -1) return;
	DbClass.sqlopen (cursor);
	if (DbClass.sqlfetch (cursor) == 0)
	{
		zerldaten.lfd = lfd;
		zerldaten.lfd_i = lfd;
		DbClass.sqlexecute (ins_cursor);
	}
}
                                 

void WriteRind ()
{
	    char datum[12];
		
		sysdate (datum);
	    Text LsIdent = aufps.ident_nr;
        Text LsEz    = aufps.ez_nr;
		Text LsEs    = aufps.es_nr;
		Text LsLand  = aufps.laender;

		LsIdent.TrimRight ();
		LsEz.TrimRight ();
		LsEs.TrimRight ();
		LsLand.TrimRight ();
		if (LsIdent == "")
		{
//			if (LsEz != "" || LsEs != "" || LsLand != "")
//			{
//				disp_messG ("Es wurde keine Identnummer erfasst", 2);
//			}
			return;
		}

	    Rind.dbreadfirst ();
		strcpy (rind.ls_ident, aufps.ident_nr); 
		rind.a = ratod (aufps.a); 
		strcpy (rind.ls_ez,    aufps.ez_nr); 
		strcpy (rind.ls_es,    aufps.es_nr); 
		strcpy (rind.ls_land,  aufps.laender); 
		FillCountries ();
//        Rind.dbupdate (); 
		strcpy (zerldaten.ident_intern, aufps.ident_nr); 
		strcpy (zerldaten.ident_extern, aufps.ident_nr); 
		zerldaten.a = ratod (aufps.a); 
		zerldaten.a_gt = ratod (aufps.a); 
		strcpy (zerldaten.eznum1, aufps.ez_nr); 
		strcpy (zerldaten.esnum,  aufps.es_nr); 
		zerldaten.gebland = rind.gebland;
		zerldaten.mastland = rind.mastland;
		zerldaten.schlaland = rind.schlaland;
		zerldaten.zerland = rind.zerlland;
		strcpy (zerldaten.lief, we_kopf.lief);
		strcpy (zerldaten.lief_rech_nr, we_kopf.lief_rech_nr);
		zerldaten.mdn = we_kopf.mdn;
		zerldaten.fil = we_kopf.fil;
		zerldaten.dat = dasc_to_long (datum);
		zerldaten.aktiv = 1;
        Zerldaten.dbinsert ();
		FillZerlLfd ();

}

int doetikett (void)
/**
Wiegen durchfuehren.
**/
{
//	    char progname [512];
		char *etc;
		int etianz;
		char *tmp;
	    char dname [512];


        DestroyKistBmp ();
        if (WithEti == FALSE) return 0;

		tmp = getenv ("TMPPATH");
		if (tmp == NULL) tmp = "c:\\user\\fit\\tmp";
		sprintf (dname, "%s\\parafile", tmp);
		etc = getenv ("BWSETC");
		if (etc == NULL) etc = "c:\\user\\fit\\etc";

		tmp = getenv ("TMPPATH");
		if (tmp == NULL) tmp = "c:\\user\\fit\\tmp";

		WriteParafile (tmp);

		etianz = 1;
/*
        sprintf (progname, "%s\\BIN\\rswrun put_eti %s %s\\eti%04d.prm %s\\parafile %d",
                                     rosipath, krz_txt, etc, eti_nr, tmp, etianz);
        ProcWaitExec (progname, SW_SHOWMINIMIZED, 0, 40, -1, 0);
*/



        sprintf (EtiPath, "%s\\eti%04d.prm", etc, eti_nr);

        int ret = PutEti (krz_txt, EtiPath, dname, etianz, -1) ;
        if (ret == -1)
        {
            disp_messG ("Etikett kann nicht gedruckt werden", 2);
        }
        return 0;
}

int dosumeti (void)
/**
Wiegen durchfuehren.
**/
{
//	    char progname [256];
		char *etc;
		int etianz;
		char *tmp;
	    char dname [512];


        DestroyKistBmp ();
        if (WithSumEti == FALSE) return 0;

		tmp = getenv ("TMPPATH");
		if (tmp == NULL) tmp = "c:\\user\\fit\\tmp";
		sprintf (dname, "%s\\parafile", tmp);
		etc = getenv ("BWSETC");
		if (etc == NULL) etc = "c:\\user\\fit\\etc";

		tmp = getenv ("TMPPATH");
		if (tmp == NULL) tmp = "c:\\user\\fit\\tmp";

		WriteSumParafile (tmp);

		etianz = 1;
/*
        sprintf (progname, "%s\\BIN\\rswrun put_eti %s %s\\eti%04d.prm %s\\parafile %d",
                                     rosipath, sum_krz_txt, etc, sum_eti_nr, tmp, etianz);
        ProcWaitExec (progname, SW_SHOWMINIMIZED, 0, 40, -1, 0);
*/

        sprintf (EtiPath, "%s\\eti%04d.prm", etc, eti_nr);

        int ret = PutEti (krz_txt, EtiPath, dname, etianz, -1) ;
        if (ret == -1)
        {
            disp_messG ("Etikett kann nicht gedruckt werden", 2);
        }
        return 0;
}

int doetispez (void)
/**
Wiegen durchfuehren.
**/
{
        static Text text1 = "&Kiste"; 
        static Text text2 = "&EZ"; 
        static Text text3 = "E&V"; 
        static Text text4 = "&Artikel"; 


		Touchf.SetParent (LogoWindow);
		Touchf.SetEtiAttr (eti_kiste, eti_ez, eti_ev, eti_artikel);
		Touchf.SetEtiText (text1.GetBuffer (),
				           text2.GetBuffer (),
				           text3.GetBuffer (),
				           text4.GetBuffer ());
		Touchf.SetWriteParams (WriteParamKiste, WriteParamEZ,
			                   WriteParamEV, WriteParamArt);

		Touchf.EnterFunkBox (WiegWindow, -1, -1, TouchfProc);
		return 0;
}

int dolgrspez (void)
/**
Lagerauswahl
**/
{
		return 0;
}

int dozusatz (void)
/**
Wiegen durchfuehren.
**/
{

	    BOOL EnterAnz = TOUCHF::EnterAnz;


		prp_user.a = _a_bas.a;
		Prp_user.dbreadfirst();

		HoleFangart(_a_bas.a);
		HoleFanggebiet(_a_bas.a);

		if (fanggebietanz == 1) 
		{
			strcpy (aufps.es_nr, fanggebietarr[0].nr);

		}
		if (fangartanz == 1) 
		{
			strcpy (aufps.ez_nr, fangartarr[0].nr);
		}
		

	    TOUCHF::EnterAnz = FALSE;
		Touchf.SetParent (WiegWindow);
		Touchf.SetEtiAttr ("", "", "", "");
		SetBuTexteZusatzdaten ();
		/*
		Touchf.SetEtiText (text1.GetBuffer (),
				           text2.GetBuffer (),
				           text3.GetBuffer (),
				           text4.GetBuffer (),
						   text5.GetBuffer ());
						   */

		if (cfg_MehrfachChargen == 1)
		{
			Touchf.SetWriteParams (EnterRCharge, AuswahlFangart,
			                   AuswahlFanggebiet, EnterFangdatum, EnterBoxNr);
		}
		else
		{
			Touchf.SetWriteParams (AuswahlFangart,
			                   AuswahlFanggebiet, EnterFangdatum, EnterBoxNr);
		}

		Touchf.EnterFunkBox (WiegWindow, -1, -1, TouchfProc);
	    TOUCHF::EnterAnz = EnterAnz;
		return 0;
}


int dotara (void)
/**
Wiegen durchfuehren.
**/
{
		DestroyKistBmp ();
        if (WieTara.aktivate == -1) return 0;

        if (WorkModus == AUSZEICHNEN)
        {
                        dogebinde ();
                        return (0);
        }
        CtrMode = 1;
        CloseControls (&WieButton);
        current_wieg = 0;
        CtrMode = 1;
        wiegend = 9;
        return 0;
}


int doanzahl (void)
/**
Stueckeingabe durchfuehren.
**/
{
		static char WiegAnz[20];

        SetStorno (NULL, 0);
        last_anz = 0l;
        sprintf (WiegAnz, "%.0lf", (double) 1);
        EnterCalcBox (WiegWindow,"  Anzahl  ",  WiegAnz, 12, "%8.0lf");
        if (syskey != KEY5)
		{
					SetEtiStk (ratod (WiegAnz));
		}
  	    else
		{
                    SetEtiStk ((double) 1.0);
					strcpy (WiegAnz, "1");
		}

        display_form (WiegKg, &WiegKgE, 0, 0);
        display_form (WiegWindow, &artwform, 0, 0);
        display_form (WiegWindow, &artwconst, 0, 0);
        if (autoan) settara ();
        current_wieg = 1;
		last_anz = atol (WiegAnz);
//		sprintf (aufps.anz_einh, "%.3lf", ratod (aufps.anz_einh) + ratod (WiegAnz));
//		last_anz = atol (aufps.anz_einh);
		sprintf (aufps.a_krz, "%.3lf", ratod (WiegAnz));
		strcpy (wiegzahl, aufps.a_krz);
        memcpy (&aufparr [aufpidx], &aufps, sizeof (struct AUFP_S));
        arrtowepos (aufpidx);
        WePosClass.dbupdate ();
        commitwork ();
        beginwork ();
        return 0;
}
int dostueck (void)
/**
Stueckeingabe durchfuehren.
**/
{
		static char WiegAnz[20];

        SetStorno (NULL, 0);
        last_anz = 0l;

		strcpy (WiegAnz, "1");
//		sprintf(WiegAnz,"%d",atoi(aufps.anz_einh));
        EnterCalcBox (WiegWindow,"  Anzahl  ",  WiegAnz, 12, "%8.0lf");

		sprintf(aufps.anz_einh,"%d",atoi(WiegAnz) + atoi(aufps.anz_einh));
        display_form (WiegKg, &WiegKgE, 0, 0);
        display_form (WiegWindow, &artwform, 0, 0);
        display_form (WiegWindow, &artwconst, 0, 0);
		last_anz = atol (aufps.anz_einh);
        memcpy (&aufparr [aufpidx], &aufps, sizeof (struct AUFP_S));
        arrtowepos (aufpidx);
        WePosClass.dbupdate ();
        commitwork ();
        beginwork ();
        return 0;
}

int dopreis (void)
/**
Wiegen durchfuehren.
**/
{
        HWND fhWnd;

		DestroyKistBmp ();
		if (strcmp (WiePreis.text1, "Anzahl") == 0)
		{
//			return doanzahl ();
			return dostueck ();
		}

        fhWnd = GetFocus ();
        EnterPreisBox (WiegWindow, -1, WiegY - 86);
        SetFocus (fhWnd);
        return 0;
}

void ArtikelHand (void)
/**
Artikel-Menge von Hand eingeben.
**/
{
        double alt;
        double netto;

        StornoSet = 0;
        SetStorno (dostorno, 0);
        sprintf (WiegMe, "%.3lf", (double) 0);
        EnterCalcBox (eWindow,"  Anzahl  ",  WiegMe, 12, WiegKgE.mask[0].picture);
        if (syskey != KEY5)
        {
              strcpy (aufps.erf_kz, "H");
              if (StornoSet)
              {
                  stornoaction ();
              }
              else
              {
                  netto = (double) ratod (WiegMe);
                  alt = (double) ratod (auf_me);
                  alt -= netto;
                  sprintf (auf_me, "%3lf", alt);
                  alt = (double) ratod (aufps.lief_me);
                  alt += netto;
                  sprintf (aufps.lief_me, "%3lf", alt);
                  strcpy (aufps.s, "3");
                  SetEtiStk (ratod (WiegMe));
				  SetEti ((double) 0.0, (double) 0.0, (double) 0.0);
              }
        }
}

char *GenCharge (void)
/**
Chargen-Nummer generieren.
**/
{

		char datum [12];
		long datl;
		int kw;
		int wt;
		char yy [5];
		char ww [3];
		char ta [3];
		char w [2];
		char lz [3];


		datl = we_kopf.we_dat;
		if (generiere_charge == 1) datl += 2;
		dlong_to_asc (datl, datum);
		strcpy (yy, &datum[8]);
		kw = get_woche (datum);
		wt = get_wochentag (datum);
		sprintf (ww, "%02d", kw);
		sprintf (lz, "%02d", lief.lief_zeit);
		sprintf (w, "%01d", wt);
		sprintf (ta, "%02d", wt);
		if (generiere_charge == 2)
		{
			if (lief.lief_zeit == 0)
			{
				sprintf (cha, "%s%s",ww,w);
			}
			else
			{
//				sprintf (cha, "%s%s%s",ww,w,lz); //H�nnger , ab 8.2.10 anders
				sprintf (cha, "%s%s%s",lz,ww,ta); //H�nnger , ab 8.2.10 
			}
		}
		else if (generiere_charge == 3)
		{
			sprintf (cha, "%s%s%s",ww,w,lief.lief);
		}
		else
		{
			sprintf (cha, "%s%s",yy,ww);
		}
		return cha;
}


void TestCharge (void)
/**
Parameter fuer die Chargennummer bearbeiten.
**/
{

	    if (ls_charge_par == 0 || lsc2_par == 0)
		{
			//011009
			if (generiere_charge > 0 && _a_bas.charg_hand == 1)
			{
			    artwformW.mask[0].attribut = READONLY;
			    artwformP.mask[0].attribut = READONLY;
				strcpy (aufps.ls_charge, GenCharge ());
			}
			return;
		}
		if (_a_bas.charg_hand == 0)
		{
			    artwformW.mask[0].attribut = READONLY;
			    artwformP.mask[0].attribut = READONLY;
				if (strcmp (aufps.ls_charge, " ") <= 0)
				{
					strcpy (aufps.ls_charge,last_charge);
				}
		}
		else if (_a_bas.charg_hand == 1)
		{
			if (generiere_charge > 0)
			{
			    artwformW.mask[0].attribut = READONLY;
			    artwformP.mask[0].attribut = READONLY;
				strcpy (aufps.ls_charge, GenCharge ());
			}
			else
			{

			    artwformW.mask[0].attribut = EDIT;
			    artwformP.mask[0].attribut = EDIT;
				if (strcmp (aufps.ls_charge, " ") <= 0)
				{
					strcpy (aufps.ls_charge,last_charge);
				}
			}
		}
		else if (_a_bas.charg_hand == 2)
		{
			    artwformW.mask[0].attribut = READONLY;
			    artwformP.mask[0].attribut = READONLY;
				strcpy (aufps.ls_charge, GenCharge ());
		}
		else if (_a_bas.charg_hand == 3)
		{
			    artwformW.mask[0].attribut = READONLY;
			    artwformP.mask[0].attribut = READONLY;
				strcpy (aufps.ls_charge, "");
		}
}

void ArtikelWiegen (HWND hWnd)
/**
Artikel wiegen.
**/
{
        RECT rect;
        MSG msg;
        int x, y, cx, cy;
        int xw, yw, cxw, cyw;
        int xc, yc, cxc, cyc;
        int wlen, wrows;
        int buglen, buabs;
        int z, s;
        static int first = 0;

		Chargen.DestroyAll ();
	    a_lgr.a = ratod(aufps.a);
		if (aufps.lager == 0 && LagerZwang > 0)
		{
			AuswahlALager(akt_lager,FALSE);
		}

        if (TestQsPosA (ratod (aufps.a)) == FALSE)
        {
            return;
        }
        if (WiegeTyp == ENTNAHME && StartWiegen == FALSE &&
            EntnahmeActGew == 0.0)
        {
            disp_messG ("Es wurde keine Startwiegung durchgef�hrt", 2);
            return;
        }
        last_anz = 0l;
        if (WiegenAktiv) return;

        if (StartWiegen)
        {
            EntnahmeStartGew = EntnahmeActGew = 0.0;
            memcpy (&SetTara, &SetTara2, sizeof (SetTara));
        }
        else if (WiegeTyp == STANDARD)
        {
            EntnahmeStartGew = EntnahmeActGew = 0.0;
            memcpy (&SetTara, &SetTara1, sizeof (SetTara));
        }
        else
        {
            deltaraw ();
            akt_tara = EntnahmeTara;
            sprintf (aufps.tara, "%.3lf", akt_tara);
            memcpy (&SetTara, &SetTara2, sizeof (SetTara));
//            sprintf (ftara, "%.3lf", akt_tara);
        }


        if (strcmp (aufps.hnd_gew, "G") == 0 || strcmp (aufps.erf_gew_kz, "J") == 0 ||
            WiegeTyp == ENTNAHME)
		{
          ActivateColButton (WiegCtr, &WieButton, 3, 2, 0);
          ActivateColButton (WiegCtr, &WieButton, 0, 2, 0);
		}
		else
		{
          ActivateColButton (WiegCtr, &WieButton, 3, -1, 0);
          ActivateColButton (WiegCtr, &WieButton, 0, -1, 0);
		}
		if (ratod (aufps.auf_me) == (double) 0.0)
		{
			artwformTW.mask[1].attribut = REMOVED;
			artwformW.mask[1].attribut = REMOVED;
		}
		else
		{
			artwformTW.mask[1].attribut = DISPLAYONLY;
			artwformW.mask[1].attribut  = READONLY;
		}

		strcpy (wiegzahl, aufps.a_krz);
        sum_eti_netto  = (double) 0.0 ;
        sum_eti_brutto = (double) 0.0;
		sum_eti_tara   = (double) 0.0;
		sum_eti_stk    = (double) 0.0;
        SwitchLiefAuf ();
		TestCharge ();
		testeti ();
        if (WorkModus == AUSZEICHNEN)
        {
//               WieWie.text1 = "Auszeichn.";
               strcpy (summe1, "0");
               strcpy (summe2, "0");
               strcpy (summe3, "0");
               strcpy (zsumme1, "0");
               strcpy (zsumme2, "0");
               strcpy (zsumme3, "0");
               WieWie.text1 = "Aus-";
               WieWie.text2 = "zeichnen";
               WieWie.tx1 = -1;
               WieWie.ty1 = 25;
               WieWie.tx2 = -1;
               WieWie.ty2 = 50;
//               WieTara.aktivate = -1;
               WieTara.text1 = "Gebinde";
               WieTara.text2 = "Tara";
               WieTara.tx1 = -1;
               WieTara.ty1 = 25;
               WieTara.tx2 = -1;
               WieTara.ty2 = 50;
               fetch_std_geb ();
               memcpy (&artwformT,&artwformTP, sizeof (form));
               memcpy (&artwform,&artwformP, sizeof (form));
               PosArtFormP ();
        }
        else
        {
               WieWie.text1 = "Wiegen";
               WieWie.text2 = NULL;
               WieWie.tx1 = -1;
               WieWie.ty1 = -1;
               WieWie.tx2 = 0;
               WieWie.ty2 = 0;
//               WieTara.aktivate = 2;
               WieTara.text1 = "Tara";
               WieTara.text2 = NULL;
               WieTara.tx1 = -1;
               WieTara.ty1 = -1;
               WieTara.tx2 = 0;
               WieTara.ty2 = 0;
               memcpy (&artwformT,&artwformTW, sizeof (form));
               memcpy (&artwform,&artwformW, sizeof (form));
               PosArtFormW ();
        }

        if (aufps.hnd_gew[0] == 'H' || aufps.erf_gew_kz[0] == 'N' || cfg_ohne_preis == 2)
		{
               WiePreis.text1 = "Anzahl";
		}
		else
		{
               WiePreis.text1 = "Preise";
		}

        WieButton.mask[1].BuId = 0;
        CtrMode = 0;

        if (WiegeTyp == ENTNAHME)
        {
               AutoTara.text3 = "";
               AutoTara.aktivate = -1;
        }
        else
        {
               AutoTara.text3 = anaus [autoan];
               if (autoan)
               {
                      AutoTara.aktivate = 4;
               }
               else
               {
                      AutoTara.aktivate = 3;
               }
        }
        SetListEWindow (0);
        strcpy (WiegMe, "0.000");
        sprintf (summe1, "%.0lf", (double) 0);
        sprintf (summe2, "%.0lf", (double) 0);
        sprintf (zsumme1, "0");
        sprintf (zsumme2, "0");
        AktWiegPos = 0;
        RegisterWieg ();
        GetWindowRect (hMainWindow, &rect);
        x = rect.left;
        y = rect.top + 24;
        cx = rect.right - rect.left;
        cy = rect.bottom - rect.top - 24;

        PosArtFormC ();
        wlen = strlen (WiegText);
        FontTextLen (WiegKgT.font, WiegText, &wlen,
                                             &wrows);
        WiegKgT.mask[0].length = wlen;
        WiegKgT.mask[0].rows   = wrows;

        WiegKgE.mask[0].pos[0]  = WiegKgT.mask[0].pos[0] + 2 * wrows;

        wlen = 11;
        FontTextLen (WiegKgE.font, "0000000,000", &wlen,
                                                  &wrows);

        WiegKgE.mask[0].length = wlen;
        WiegKgE.mask[0].rows   = wrows;

        if (wlen > WiegKgT.mask[0].length)
        {
                   cxw = wlen;
        }
        else
        {
                   cxw = WiegKgT.mask[0].length;
        }

        cxw += 20;

        cyw = WiegKgE.mask[0].pos[0] + wrows + wrows / 2 + 10;
        xw = (cx - cxw) / 2;
        yw = (cy - cxw) / 2 + 120;

        PosMeForm (yw, xw, cyw, cxw);

        if (first == 0)
        {
                  ftaratxt.mask[0].pos[1] = xw;
                  ftaratxt.mask[0].pos[0] = yw - 40;

                  z = yw - 40;
                  s = xw;
                  GetTextPos (ftaratxt.font, &z, &s);
                  s += 7;
                  KonvTextAbsPos (ftaratxt.font, &z, &s);
                  ftara.mask[0].pos[0] = z + 10;
                  ftara.mask[0].pos[1] = s;

                  s = strlen (ftaratxt.mask[0].feld);
                  FontTextLen (ftaratxt.font, ftaratxt.mask[0].feld, &s, &z);
                  ftaratxt.mask[0].length = s;
                  ftaratxt.mask[0].rows   = z;

                  s = 8;
                  FontTextLen (ftara.font, "0000.000", &s, &z);
                  ftara.mask[0].length = s;
                  ftara.mask[0].rows   = z;

                  ftexte.mask[0].length = s;
                  ftexte.mask[0].rows = z * 2;


				//021109 A 
                  sw_abzug.mask[0].pos[1] = xw; 
                  sw_abzug.mask[0].pos[0] = yw - 80; 
                  z = yw - 80;
                  s = xw ;
                  GetTextPos (sw_abzug.font, &z, &s);
                  s += 7;
                  KonvTextAbsPos (sw_abzug.font, &z, &s);

                  s = strlen (sw_abzug.mask[0].feld);
                  FontTextLen (sw_abzug.font, sw_abzug.mask[0].feld, &s, &z);
                  sw_abzug.mask[0].length = s;
                  sw_abzug.mask[0].rows   = z;
				 //021109 E
				  

/*
                  KonvTextStrLen (ftexte.font, "Textex ", &s);
                  STexte.bmpx = s;
*/

                  first = 1;
        }

		EtiSpezButton.mask[0].pos[0] = cy - 160 - 3 * bsize;
		LgrSpezButton.mask[0].pos[0] = cy - 160 -  bsize;
		ZusatzButton.mask[0].pos[0] = cy -  160 - 5 - 2 * bsize;

        WiegWindow  = CreateWindowEx (
                              0,
                              "AufWieg",
                              "",
                              WS_POPUP,
                              x, y,
                              cx, cy,
                              // hMainWindow,
                              hWnd,
                              NULL,
                              hMainInst,
                              NULL);
        if (WiegWindow == NULL) return;


        EnableWindow (hMainWindow, FALSE);

        WiegY = yw + cyw;
        wiegx  = xw;
        wiegy  = yw;
        wiegcx = cxw;
        wiegcy = cyw;

        WiegKg  = CreateWindowEx (
                              0,
                              "AufWiegKg",
                              "",
                              WS_CHILD | WS_BORDER,
                              xw, yw,
                              cxw, cyw,
                              WiegWindow,
                              NULL,
                              hMainInst,
                              NULL);

		KistCol = FALSE;
		SetClassLong (WiegKg, GCL_HBRBACKGROUND, (long) LtBlueBrush);
		SetKistRect (xw, yw);

        cxc = cx - 20;
        cyc = 150;
        xc  = 10;
        yc  = cy - 160;

        if (cfg_ohne_preis == 1 && strcmp (WiePreis.text1, "Preise") == 0)
        {
              WieButton.mask[1].attribut = REMOVED;
              GetZentrierWerte (&buglen, &buabs, cxc, 5, babs, bsize);
              WieButton.mask[0].pos[1] = (cxc - buglen) / 2;
              WieButton.mask[2].pos[1] = WieButton.mask[0].pos[1] + buabs;
              WieButton.mask[3].pos[1] = WieButton.mask[2].pos[1] + buabs;
              WieButton.mask[4].pos[1] = WieButton.mask[3].pos[1] + buabs;
              WieButton.mask[5].pos[1] = WieButton.mask[4].pos[1] + buabs;
        }
        else
        {
              WieButton.mask[1].attribut = COLBUTTON;
              GetZentrierWerte (&buglen, &buabs, cxc, 6, babs, bsize);
              WieButton.mask[0].pos[1] = (cxc - buglen) / 2;
              WieButton.mask[1].pos[1] = WieButton.mask[0].pos[1] + buabs;
              WieButton.mask[2].pos[1] = WieButton.mask[1].pos[1] + buabs;
              WieButton.mask[3].pos[1] = WieButton.mask[2].pos[1] + buabs;
              WieButton.mask[4].pos[1] = WieButton.mask[3].pos[1] + buabs;
              WieButton.mask[5].pos[1] = WieButton.mask[4].pos[1] + buabs;
        }

        GetZentrierWerte (&buglen, &buabs, cxc, 6, babs2, bsize2);
        WieButtonT.mask[0].pos[1] = (cxc - buglen) / 2;
        WieButtonT.mask[1].pos[1] = WieButtonT.mask[0].pos[1] + buabs;
        WieButtonT.mask[2].pos[1] = WieButtonT.mask[1].pos[1] + buabs;
        WieButtonT.mask[3].pos[1] = WieButtonT.mask[2].pos[1] + buabs;
        WieButtonT.mask[4].pos[1] = WieButtonT.mask[3].pos[1] + buabs;
        WieButtonT.mask[5].pos[1] = WieButtonT.mask[4].pos[1] + buabs;

        WiegCtr  = CreateWindowEx (
                              WS_EX_CLIENTEDGE,
                              "AufWiegCtr",
                              "",
                              WS_CHILD,
                              xc, yc,
                              cxc, cyc,
                              WiegWindow,
                              NULL,
                              hMainInst,
                              NULL);

		if (first < 2)
		{
				  RECT CtrRect;
				  GetWindowRect (WiegCtr, &CtrRect);
				  POINT LeftPoint;
				  LeftPoint.x = CtrRect.left;
				  LeftPoint.y = CtrRect.top;
				  ScreenToClient (WiegWindow, &LeftPoint);
				  LeftPoint.y -= ftexte.mask[0].rows;
				  ftexte.mask[0].pos[0] = (short) LeftPoint.y;
				  ftexte.mask[0].pos[1] = WieButton.mask[0].pos[1] + (short) LeftPoint.x;
				  first = 2;
		}

        ShowWindow (WiegWindow, SW_SHOW);
        UpdateWindow (WiegWindow);
        ShowWindow (WiegKg, SW_SHOW);
        UpdateWindow (WiegKg);
        ShowWindow (WiegCtr, SW_SHOW);
        UpdateWindow (WiegCtr);
        create_enter_form (WiegWindow, &artwform, 0, 0);
        display_form (WiegWindow, &artwconst, 0, 0);
        current_wieg = TestAktivButton (PLUS, 0);
        SetFocus (WieButton.mask[current_wieg].feldid);

        WiegenAktiv = 1;
        current_wieg = 0;
        break_wieg = 0;

        while (GetMessage (&msg, NULL, 0, 0))
        {
              if (IsWiegMessage (&msg));
              else
              {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
              }
              if (break_wieg) break;
        }
        if (AlarmWindow)
        {
                     CloseControls (&alform);
                     DestroyWindow (AlarmWindow);
                     AlarmWindow = NULL;
        }
		a_lgr.a = 0.0;
        if ( lgr_class.lese_lgr_bz (akt_lager) == 0) sprintf (Lagertxt,"Lager: %s",lgr.lgr_bz);
		DestroyKistBmp ();
        EnableWindow (hMainWindow, TRUE);
        CloseControls (&WiegKgT);
        CloseControls (&WiegKgE);
        CloseControls (&WieButton);
        CloseControls (&EtiButton);
        CloseControls (&EtiSpezButton);
        CloseControls (&LgrSpezButton);
        CloseControls (&ZusatzButton);
        CloseControls (&artwform);
        CloseControls (&artwformT);
        CloseControls (&artwconst);
        CloseControls (&artwconstT);
        CloseControls (&startwieg);
        CloseControls (&fEntnahmeGewTxt);
        CloseControls (&fEntnahmeGew);
        CloseControls (&WiegKgMe);
        CloseControls (&ftara);
        CloseControls (&ftaratxt);
        CloseControls (&sw_abzug);
        CloseFontControls (&ftexte);
        DestroyWindow (WiegWindow);
//        SetListEWindow (1);
        WiegenAktiv = 0;
}

/** Ab hier Auswahl fuer Preiseingabe
**/

static HWND ePreisWindow;
static break_preis_enter = 0;


mfont PreisTextFont = {"Arial", 200, 0, 1, RGB (0, 0, 0),
                                         RGB (255, 255, 255),
                                         1,
                                         NULL};


mfont preisfont   = {"", 120, 0, 1,
                     RGB (255, 0, 0),
                     BLUECOL,
                     1,
                     NULL};


ColButton BuVkPreis =   {"&EK-Preis", -1, 25,
                         "Preis", -1, 60,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         2};

ColButton BuLadPreis =   {"&Netto-", -1, 25,
                          "Preis", -1, 60,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 0, 0),
                         YELLOWCOL,
                         2};

ColButton BuPreisOK =   {"&Zur�ck", -1, -1,
                          NULL, 0, 0,
                          NULL, 0, 0,
                          NULL, 0, 0,
                          NULL, 0, 0,
                          RGB (255, 255, 255),
                          REDCOL,
                          2};

static int dovk ();
static int dolad ();
static int doprok ();

static HWND fhWnd;
static int pnbss0 = 80;
int pny    = 3;
int pnx    = 1;
static int pcbss0 = 100;
static int pcbss  = 64;
static int pcbsz  = 64;
static int pcbsz0 = 100;

static field _PrWahl[] = {
(char *) &BuVkPreis,      pcbss0, pcbsz0, pny, pnx,        0, "",
                                                           COLBUTTON, 0,
                                                           dovk, 0,
(char *) &BuLadPreis,     pcbss0, pcbsz0, pny, pnx + 1 * pcbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           dolad, 0,
(char *) &BuPreisOK,      pcbss0, pcbsz0, pny, pnx + 2 * pcbss0 , 0, "",
                                                           COLBUTTON, 0,
                                                           doprok, 0};

static form PrWahl = {3, 0, 0, _PrWahl, 0, 0, 0, 0, &preisfont};

static char preisbuff [256];

static field _PrTextForm [] = {
preisbuff,             0, 0, 0, 0, 0, "", DISPLAYONLY, 0, 0, 0};

static form PrTextForm = {1, 0, 0, _PrTextForm, 0, 0, 0, 0, &PreisTextFont};

void PreisEnterBreak ()
/**
Endeflag fuer Erfassung setzen.
**/
{
        break_preis_enter = 1;
}

int dovk (void)
{
        char preis [20];

        mdn_class.lese_mdn (Mandant);
        if (lief.waehrung == 2)
        {
                    strcpy (preis, aufps.ls_vk_euro);
        }
        else
        {
                    strcpy (preis, aufps.auf_pr_vk);
        }
        EnterCalcBox (ePreisWindow,"    EK-Preis    ", preis, 9, "%8.4f");
        SetActiveWindow (ePreisWindow);
        if (ratod (preis) > (double) 0.0)
        {
                   if (lief.waehrung == 2)
                   {
                              we_pos.pr_ek_euro =  ratod (preis);

                              we_pos.pr_ek_nto_eu  = we_pos.pr_ek_euro -
                                        (double) WePreis.GetRabEk (we_kopf.mdn, we_kopf.fil,
			                                           we_kopf.lief,
	  						                           1, we_pos.pr_ek_euro, _a_bas.me_einh, _a_bas.a, _a_bas.ag, _a_bas.wg);

                              strcpy (aufps.ls_vk_euro, preis);
                              sprintf (aufps.auf_pr_vk, "%lf", ratod (aufps.ls_vk_euro) *
                                                                _mdn.konversion);
                              sprintf (aufps.ls_lad_euro, "%lf", we_pos.pr_ek_nto_eu);
                              sprintf (aufps.auf_lad_pr, "%lf", we_pos.pr_ek_nto_eu *
                                                                _mdn.konversion);
                   }
                   else
                   {
                              we_pos.pr_ek =  ratod (preis);

                              we_pos.pr_ek_nto  = we_pos.pr_ek -
                                        (double) WePreis.GetRabEk (we_kopf.mdn, we_kopf.fil,
			                                           we_kopf.lief,
	  						                           1, we_pos.pr_ek, _a_bas.me_einh, _a_bas.a, _a_bas.ag, _a_bas.wg);

                              strcpy (aufps.auf_pr_vk, preis);
                              sprintf (aufps.ls_vk_euro, "%lf", ratod (aufps.auf_pr_vk) /
                                                                _mdn.konversion);
                              sprintf (aufps.auf_lad_pr, "%lf", we_pos.pr_ek_nto);
                              sprintf (aufps.ls_lad_euro, "%lf", we_pos.pr_ek_nto /
                                                                _mdn.konversion);
                   }
                   if (InsWindow)
                   {
                             InvalidateRect (InsWindow, NULL, TRUE);
                             UpdateWindow (InsWindow);
                   }
        }
		if (we_pos.pr_ek > 0.0)
		{
              we_pos.pr_ek_nto  = we_pos.pr_ek - (double) WePreis.GetRabEk (we_kopf.mdn, we_kopf.fil,
			                                           we_kopf.lief,
	  						                           1, we_pos.pr_ek, _a_bas.me_einh, _a_bas.a, _a_bas.ag, _a_bas.wg);
		}
		else
		{
			  we_pos.pr_ek_nto = (double) 0.0;
		}
		if (we_pos.pr_ek_euro > 0.0)
		{
              we_pos.pr_ek_nto_eu  = we_pos.pr_ek_euro - (double) WePreis.GetRabEk (we_kopf.mdn, we_kopf.fil,
			                                               we_kopf.lief,
	  						                            1, we_pos.pr_ek_euro, _a_bas.me_einh, _a_bas.a, _a_bas.ag, _a_bas.wg);
		}
		else
		{
			  we_pos.pr_ek_nto_eu = (double) 0.0;
		}
        SetFocus (fhWnd);
        return 0;
}

int dolad (void)
{
        char preis [20];


        mdn_class.lese_mdn (Mandant);
        if (ratod (_mdn.waehr_prim) == 2)
        {
                    strcpy (preis, aufps.ls_lad_euro);
        }
        else
        {
                    strcpy (preis, aufps.auf_lad_pr);
        }
        EnterCalcBox (ePreisWindow,"  Netto EK  ", preis, 9, "%8.4f");
        if (ratod (preis) > (double) 0.0)
        {
                   if (ratod (_mdn.waehr_prim) == 2)
                   {
                              strcpy (aufps.ls_lad_euro, preis);
                              sprintf (aufps.auf_lad_pr, "%4lf", ratod (aufps.ls_lad_euro) *
                                                                _mdn.konversion);
                   }
                   else
                   {
                              strcpy (aufps.auf_lad_pr, preis);
                              sprintf (aufps.ls_lad_euro, "%4lf", ratod (aufps.auf_lad_pr) /
                                                                  _mdn.konversion);
                   }
                   if (InsWindow)
                   {
                             InvalidateRect (InsWindow, NULL, TRUE);
                             UpdateWindow (InsWindow);
                   }
        }
        SetActiveWindow (ePreisWindow);
        SetFocus (fhWnd);
        return 0;
}

int doprok (void)
{
        PreisEnterBreak ();
        return 0;
}

HWND IsPreisChild (POINT *mousepos)
/**
Test, ob sich der Maus-Cursor in einem KindFensert befinden.
**/
{
        int i;

        for (i = 0; i < PrWahl.fieldanz; i ++)
        {
                   if (MouseinWindow (PrWahl.mask[i].feldid,
                                      mousepos))
                   {
                                 return (PrWahl.mask[i].feldid);
                   }
        }

        return NULL;
}


LONG FAR PASCAL PreisProc(HWND hWnd,UINT msg,
                          WPARAM wParam,LPARAM lParam)
{
        POINT mousepos;
        HWND enchild;
        static int SendChild = 0;
        int HasFocus = 1;

        GetCursorPos (&mousepos);

        switch(msg)
        {
              case WM_PAINT :
                    display_form (ePreisWindow, &PrWahl, 0, 0);
                    SetFocus (fhWnd);
                    break;
              case WM_LBUTTONDOWN :
              case WM_LBUTTONUP :
              case WM_MOUSEMOVE :
                    enchild = IsPreisChild (&mousepos);
//                    MouseTest (enchild, msg);
                    if (enchild)
                    {
                              SendMessage (enchild, msg, wParam, lParam);
                              return TRUE;
                    }
                    return TRUE;
              case WM_ACTIVATE :
                    switch (wParam)
                    {
                         case WA_ACTIVE :
                         case WA_CLICKACTIVE :
                              SetCapture (ePreisWindow);
                              break;
                    }
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

void RegisterPreis (void)
/**
Fenster fuer Preis registrieren.
**/
{
        static int registered = 0;
        WNDCLASS wc;

        if (registered == 0)
        {
                   wc.style =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                               | CS_OWNDC;
                   wc.cbClsExtra    =  0;
                   wc.cbWndExtra    =  0;
                   wc.hInstance     =  hMainInst;
                   wc.hIcon         =  LoadIcon (hMainInst, "FITICON");
                   wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                   wc.lpszMenuName  =  "";
                   wc.lpfnWndProc   =  PreisProc;
                   wc.lpszMenuName  =  "";
                   wc.hbrBackground =  CreateSolidBrush (LTGRAYCOL);
                   wc.lpszClassName =  "EnterPreis";
                   RegisterClass(&wc);
                   registered = 1;
        }
}

int PrGetAktButton (void)
/**
Aktiven Button holen.
**/
{
        HWND hWnd;
        int i;

        hWnd = GetFocus ();

        for (i = 0; i < PrWahl.fieldanz; i ++)
        {
                    if (hWnd == PrWahl.mask[i].feldid)
                    {
                               return (i);
                    }
        }
        return (0);
}


void PrPrevKey (void)
/**
Vorhergenden Button aktivieren.
**/
{
         int AktButton;

         AktButton = PrGetAktButton ();
         if (AktButton == 0)
         {
                     AktButton = PrWahl.fieldanz - 1;
         }
         else
         {
                     AktButton --;
         }
         SetFocus (PrWahl.mask [AktButton].feldid);
         fhWnd = GetFocus ();
}

void PrNextKey (void)
/**
Naechsten Button aktivieren.
**/
{
         int AktButton;

         AktButton = PrGetAktButton ();
         if (AktButton == PrWahl.fieldanz - 1)
         {
                     AktButton = 0;
         }
         else
         {
                     AktButton ++;
         }
         SetFocus (PrWahl.mask [AktButton].feldid);
         fhWnd = GetFocus ();
}

void PrActionKey (void)
/**
Button-Aktion ausfuehren.
**/
{
         int AktButton;
         field *feld;

         AktButton = PrGetAktButton ();
         feld =  &PrWahl.mask[AktButton];

         if (feld->after != (int (*) (void)) 0)
         {
                    (*feld->after) ();
         }
         else if (feld->BuId != 0)
         {
                    SendMessage (GetParent (feld->feldid), WM_COMMAND,
                                            MAKELONG (feld->BuId,
                                            feld->feldid),
                                            0l);
         }
}

int IsPreisKey (MSG *msg)
/**
HotKey Testen.
**/
{
         static char keypressed = 0;
         static HWND keyhwnd = 0;
         ColButton *ColBut;
         int taste;
         int i;
         char *pos;

         switch (msg->message)
         {
              case WM_KEYDOWN :
              {
                     if (keypressed)
                     {
                                  return FALSE;
                     }
                     switch (msg->wParam)
                     {
                            case VK_F5 :
                               PreisEnterBreak ();
                               syskey = KEY5;
                               return TRUE;
                            case  VK_UP :
                               PrPrevKey ();
                               return TRUE;
                            case VK_DOWN :
                               PrNextKey ();
                               return TRUE;
                            case VK_RIGHT :
                               PrNextKey ();
                               return TRUE;
                            case VK_LEFT :
                               PrPrevKey ();
                               return TRUE;
                            case VK_TAB :
                               if (GetKeyState (VK_SHIFT) < 0)
                               {
                                      PrPrevKey ();
                                      return TRUE;
                               }
                               PrNextKey ();
                               return TRUE;
                             case VK_RETURN :
                               PrActionKey ();
                               return TRUE;
                     }
                     taste = (int) msg->wParam;
                     for (i = 0; i < 3; i ++)
                     {
                        ColBut = (ColButton *) PrWahl.mask[i].feld;
                        if (ColBut->text1)
                        {
                           if ((pos = strchr (ColBut->text1, '&')) &&
                               ((UCHAR) toupper (*(pos + 1)) == taste))
                           {
                                        keypressed = 1;
                                        keyhwnd = PrWahl.mask[i].feldid;
                                        SendMessage (keyhwnd,
                                                WM_LBUTTONDOWN,
                                                0l,
                                                0l);
                                        return TRUE;
                            }
                       }
                     }
                     return FALSE;
              }
              case WM_KEYUP :
              {
                     taste = (int) msg->wParam;
                     for (i = 0; i < 3; i ++)
                     {
                         if (keyhwnd == PrWahl.mask[i].feldid)
                         {
                                   keypressed = 0;
                                   SendMessage (keyhwnd,
                                                WM_LBUTTONUP,
                                                0l,
                                                0l);
                                   keyhwnd = NULL;
                                   return TRUE;
                         }
                     }
                     return FALSE;
              }
          }
          return FALSE;
}


void EnterPreisBox (HWND hWnd, int px, int py)
/**
Fenster fuer Preisauswahl oeffnen.
**/
{
        RECT rect;
        MSG msg;
        int x, y, cx, cy;
        HCURSOR oldcursor;
        static int BitMapOK = 0;

        RegisterPreis ();
        GetWindowRect (hWnd, &rect);
        cx = 3 * pcbss0 + 7;
        cy = pcbsz0 + 10;
        if (px == -1)
        {
                  x = rect.left + (rect.right - rect.left - cx) / 2;
        }
        else
        {
                  x = px;
        }
        if (py == -1)
        {
                  y = (rect.bottom - cy) / 2;
        }
        else
        {
                  y = py;
        }

        ePreisWindow  = CreateWindowEx (
                              WS_EX_CLIENTEDGE,
                              "EnterPreis",
                              "",
                              WS_POPUP,
                              x, y,
                              cx, cy,
                              hWnd,
                              NULL,
                              hMainInst,
                              NULL);

        ShowWindow (ePreisWindow, SW_SHOW);
        UpdateWindow (ePreisWindow);
        oldcursor = SetCursor (LoadCursor (NULL, IDC_ARROW));
        SetFocus (PrWahl.mask[0].feldid);
        fhWnd = GetFocus ();
        SetCapture (ePreisWindow);

        PreisAktiv = 1;
        break_preis_enter = 0;
        while (GetMessage (&msg, NULL, 0, 0))
        {
              if (IsPreisKey (&msg));
              else
              {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
              }
              if (break_preis_enter) break;
        }
        CloseControls (&PrWahl);
        ReleaseCapture ();
        DestroyWindow (ePreisWindow);
        PreisAktiv = 0;
        SetCursor (oldcursor);
}

/*

Auswahl fuer Festtara.

*/

struct PT
{
        char ptbez [33];
        char ptwer1 [9];
};

struct PT ptab, ptabarr [20];
static int ptabanz = 0;


static int ptidx;

mfont PtFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                               RGB (255, 255, 255),
                               1,
                               NULL};

static field _ftaraform [] =
{
        ptab.ptbez,     12, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        ptab.ptwer1,     9, 1, 0,13, 0, "%7.3f", DISPLAYONLY, 0, 0, 0
};

static form ftaraform = {2, 0, 0, _ftaraform, 0, 0, 0, 0, &PtFont};

static field _ftaraub [] =
{
        "Bezeichnung",     12, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        "Tara",             9, 1, 0,13, 0, "", DISPLAYONLY, 0, 0, 0
};

static form ftaraub = {2, 0, 0, _ftaraub, 0, 0, 0, 0, &PtFont};

static field _ftaravl [] =
{      "1",                  1,  0, 0, 12, 0, "", NORMAL, 0, 0, 0,
};

static form ftaravl = {1, 0, 0, _ftaravl, 0, 0, 0, 0, &PtFont};

int ShowFestTara (char *pttara)
/**
Ausahlfenster fuer Festtara anzeigen.
**/
{
        if (ptabanz == 0)
        {
               dsqlstatus = ptab_class.lese_ptab_all ("tara_fest");
               while (dsqlstatus == 0)
               {
                   strcpy (ptabarr[ptabanz].ptbez,  ptabn.ptbez);
                   strcpy (ptabarr[ptabanz].ptwer1, ptabn.ptwer1);
                   ptabanz ++;
                   if (ptabanz == 20) break;
                   dsqlstatus = ptab_class.lese_ptab_all ();
               }
        }
        EnableWindow (WiegWindow, FALSE);
        ptidx = ShowPtBox (WiegWindow, (char *) ptabarr, ptabanz,
                     (char *) &ptab,(int) sizeof (struct PT),
                     &ftaraform, &ftaravl, &ftaraub);
        EnableWindow (WiegWindow, TRUE);
        strcpy (pttara, ptabarr [ptidx].ptwer1);
        return ptidx;
}






/*

L�nderauswahl fuer Rindfleisch.

*/


static field _flandform [] =
{
        ptab.ptwer1,    20, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
//        ptab.ptbez,     12, 1, 0,10, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form flandform = {1, 0, 0, _flandform, 0, 0, 0, 0, &PtFont};

static field _flandub [] =
{
        "Wert",              20, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
//        "Beschreibung",      12, 1, 0,10, 0, "", DISPLAYONLY, 0, 0, 0
};

static form flandub = {1, 0, 0, _flandub, 0, 0, 0, 0, &PtFont};

static field _flandvl [] =
{ 
	"1",                  1,  0, 0, 10, 0, "", NORMAL, 0, 0, 0,
};

static form flandvl = {0, 0, 0, _flandvl, 0, 0, 0, 0, &PtFont};

int ShowTierLand (char *value)
/**
Ausahlfenster fuer Festtara anzeigen.
**/
{
        if (ptabanz == 0)
        {
               dsqlstatus = ptab_class.lese_ptab_all ("tier_land");
               while (dsqlstatus == 0)
               {
                   strcpy (ptabarr[ptabanz].ptbez,  ptabn.ptbez);
                   strcpy (ptabarr[ptabanz].ptwer1, ptabn.ptwer1);
                   ptabanz ++;
                   if (ptabanz == 20) break;
                   dsqlstatus = ptab_class.lese_ptab_all ();
               }
        }
        EnableWindow (WiegWindow, FALSE);
        ptidx = ShowPtBox (WiegWindow, (char *) ptabarr, ptabanz,
                     (char *) &ptab,(int) sizeof (struct PT),
                     &flandform, &flandvl, &flandub);
        EnableWindow (WiegWindow, TRUE);
        strcpy (value, ptabarr [ptidx].ptwer1);
        return ptidx;
}


/*

Auswahl fuer Drucker.

*/

struct DR
{
        char drucker [33];
};

struct DR ldrucker, ldruckarr [20];
static int ldruckanz = 0;
static int ldidx;

mfont DrFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                               RGB (255, 255, 255),
                               1,
                               NULL};
static field _ldruckform [] =
{
        ldrucker.drucker,    22, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form ldruckform = {1, 0, 0, _ldruckform, 0, 0, 0, 0, &DrFont};

static field _ldruckub [] =
{
        "Drucker",     22, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form ldruckub = {1, 0, 0, _ldruckub, 0, 0, 0, 0, &DrFont};


void BelegeGdiDrucker (void)
/**
Drucker Win.ini holen
**/
{
       int i;
	   char *pr;
//	   char AktDevice [80];
       char ziel [256];
       char quelle [256];
       char *frmenv;

       frmenv = getenv ("FRMPATH");
       if (frmenv == NULL) return;

       GetAllPrinters ();

       i = 0;
       while (TRUE)
       {
                pr = GetNextPrinter ();
                if (pr == NULL) break;
                strcpy (ldruckarr[i].drucker, pr);
                i ++;
       }
       ldruckanz = i;

       sprintf (quelle, "%s\\%s", getenv ("FRMPATH"), "gdi.cfg");
       sprintf (ziel, "%s\\%s", getenv ("FRMPATH"), "drucker.akt");
       CopyFile (quelle, ziel, FALSE);
       sprintf (ziel, "%s\\%s.%s", getenv ("FRMPATH"),
                                 clipped (sys_ben.pers_nam),
                                 "akt");
       CopyFile (quelle, ziel, FALSE);

}


void BelegeDrucker (void)
/**
Drucker aus Formatverzeichnis holen.
**/
{
      char frmdir [256];
      char *frmenv;
      WIN32_FIND_DATA lpffd;
      HANDLE hFile;
      char *p;
      int i;
      static int druckerOK = 0;

      if (druckerOK) return;

      druckerOK = 1;
      if (GraphikMode)
      {
               BelegeGdiDrucker ();
               return;
      }
      frmenv = getenv ("FRMPATH");
      if (frmenv == NULL) return;

      sprintf (frmdir, "%s\\*.cfg", frmenv);


      hFile = FindFirstFile (frmdir, &lpffd);

      if (hFile == INVALID_HANDLE_VALUE)
      {
                      return;
      }

      i = 0;
      p = strchr (lpffd.cFileName, '.');
      if (p) *p = (char) 0;

      strcpy (ldruckarr[i].drucker, lpffd.cFileName);
      i ++;

      while (FindNextFile (hFile, &lpffd))
      {
               p = strchr (lpffd.cFileName, '.');
               if (p) *p = (char) 0;
               strcpy (ldruckarr[i].drucker, lpffd.cFileName);
               i ++;
      }
      FindClose (hFile);

      ldruckanz = i;
}

void drucker_akt (char *dname)
/**
Ausgewaehlten Drucker in drucker.akt_uebertragen.
**/
{
        char ziel [256];
        char quelle [256];

		if (GraphikMode)
		{
		        SetPrinter (dname);
				return;
		}
        sprintf (quelle, "%s\\%s.cfg", getenv ("FRMPATH"), dname);
        sprintf (ziel, "%s\\%s", getenv ("FRMPATH"), "drucker.akt");
        CopyFile (quelle, ziel, FALSE);
        sprintf (ziel, "%s\\%s.%s", getenv ("FRMPATH"),
                                 clipped (sys_ben.pers_nam),
                                 "akt");
        CopyFile (quelle, ziel, FALSE);
}

int DruckChoise ()
/**
Ausahlfenster fuer Festtara anzeigen.
**/
{
        char aktdrucker [80];

        BelegeDrucker ();
        ldidx = ShowPtBox (eWindow, (char *) ldruckarr, ldruckanz,
                     (char *) &ldrucker,(int) sizeof (struct DR),
                     &ldruckform, NULL, &ldruckub);
        strcpy (aktdrucker, ldruckarr [ldidx].drucker);
        clipped (aktdrucker);
        drucker_akt (aktdrucker);
        CloseControls (&testub);
		display_form (Getlbox (), &testub, 0, 0);
        return ldidx;
}


/** Ptabanzeige
**/

/*
static int ptbss0 = 73;
static int ptbss  = 77;
static int ptbsz  = 40;
static int ptbsz0 = 36;
*/

static int ptbss0 = 103;
static int ptbss  = 107;
static int ptbsz  = 60;
static int ptbsz0 = 56;


field _PtButton[] = {
(char *) &Back,         ptbss0, ptbsz0, -1, 4+0*ptbss, 0, "", COLBUTTON, 0,
 *                                                        func5, 0,
(char *) &BuOKS,        ptbss0, ptbsz0, -1, 4+1*ptbss, 0, "", COLBUTTON, 0,
 *                                                        func6, 0,
(char *) &PfeilU,       ptbss0, ptbsz0, -1, 4+2*ptbss, 0, "", COLBUTTON, 0,
                                                          func3, 0,
(char *) &PfeilO,       ptbss0, ptbsz0, -1, 4+3*ptbss, 0, "", COLBUTTON, 0,
                                                          func2, 0
};

form PtButton = {4, 0, 0, _PtButton, 0, 0, 0, 0, &buttonfont};


void IsPtClck (int idx)
/**
Reaktion auf Doppelclick in der Liste.
**/
{
        ptidx = idx;
        break_list ();
        return;
}

void RegisterPt (void)
/**
Kopf - und Fuss - Fenster fuer Liste registrieren
**/
{
        static int registered = 0;
        WNDCLASS wc;

        if (registered == 0)
        {
                   wc.style =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                               | CS_OWNDC;
                   wc.cbClsExtra    =  0;
                   wc.cbWndExtra    =  0;
                   wc.hInstance     =  hMainInst;
                   wc.hIcon         =  LoadIcon (hMainInst, "FITICON");
                   wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                   wc.lpszMenuName  =  "";
                   wc.lpfnWndProc   =  PtFussProc;
                   wc.lpszMenuName  =  "";
                   wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
                   wc.lpszClassName =  "PtFuss";
                   RegisterClass(&wc);
                   registered = 1;
        }
}

HWND IsPtFussChild (POINT *mousepos)
/**
Test, ob sich der Maus-Cursor in einem KindFensert befinden.
**/
{
        int i;

        for (i = 0; i < PtButton.fieldanz; i ++)
        {
                   if (MouseinWindow (PtButton.mask[i].feldid,
                                      mousepos))
                   {
                                 return (PtButton.mask[i].feldid);
                   }
        }
        return NULL;
}

int MouseInPt (POINT *mpos)
{
        HWND hListBox;

        hListBox = GethListBox ();

        if (MouseinDlg (hListBox, mpos)) return TRUE;
        if (MouseinDlg (PtFussWindow, mpos)) return TRUE;
        return FALSE;
}


LONG FAR PASCAL PtFussProc(HWND hWnd,UINT msg,
                           WPARAM wParam,LPARAM lParam)
{
        HWND enchild;
        POINT mousepos;
        HWND lbox;

        GetCursorPos (&mousepos);

        switch(msg)
        {
              case WM_PAINT :
                    display_form (PtFussWindow, &PtButton, 0, 0);
                    break;
              case WM_LBUTTONDOWN :
              case WM_LBUTTONUP :
              case WM_MOUSEMOVE :
                    enchild = IsPtFussChild (&mousepos);
//                    MouseTest (enchild, msg);
                    if (enchild)
                    {
                              SendMessage (enchild, msg, wParam, lParam);
                              return TRUE;
                    }
                    lbox = Getlbox ();
                    if (MouseinWindow (lbox, &mousepos))
                    {
                              ScreenToClient (lbox, &mousepos);
                              lParam = MAKELONG (mousepos.x, mousepos.y);
                              SendMessage (lbox, msg, wParam, lParam);
                              return TRUE;
                    }
                    return TRUE;
              case WM_VSCROLL :
                    lbox = Getlbox ();
                    if (MouseinWindow (lbox, &mousepos))
                    {
                              SendMessage (lbox, msg, wParam, lParam);
                    }
                    return TRUE;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

int ShowPtBox (HWND hWnd, char * ptabarr, int ptabanz, char * ptab,
               int ptsize, form *ptform, form *ptvl, form *ptub)
{
        RECT rect;
        RECT frmrect;
        int x, y, cx, cy;
        HWND shListBox;
        HWND slbox;
        struct LMENUE smenue;

        GetMenue (&smenue);
        MenTextSave (&smenue);
        shListBox = GethListBox ();
        slbox = Getlbox ();

        RegisterPt ();
        GetWindowRect (hMainWindow, &rect);
        GetFrmRectEx (ptform, ptform->font, &frmrect);

        x = rect.left + 50;
        y = rect.top + 20;
        cx = frmrect.right + 45;
        cy = 185;
		cy = 46 * ptabanz;
		if (cy < 150) cy = 150;
		if (cy > 480) cy = 480;

        set_fkt (endlist, 5);
        SetDblClck (IsPtClck, 0);
        PtListeAktiv = 1;
        SetAktivWindow (hWnd);
//        SetListEWindow (1);
        SetListFont (TRUE);
        spezfont (ptform->font);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_VISIBLE);
        PtWindow = OpenListWindowBoEx (x, y, cx, cy);
//        PtWindow = OpenListWindowEnEx (x, y, cx, cy);
        GetWindowRect (PtWindow, &rect);
        y = rect.bottom;
		cx = rect.right - rect.left;
        PtFussWindow = CreateWindow ("PtFuss",
                                    "",
                                    WS_POPUP | WS_VISIBLE | WS_DLGFRAME,
                                    x,
//                                    y + cy + 23,
                                    y,
                                    cx, 70,
                                    hWnd,
                                    NULL,
                                    hMainInst,
                                    NULL);
        if (ptvl) ElistVl (ptvl);
        if (ptub) ElistUb (ptub);
        ShowElist ((char *) ptabarr,
                  ptabanz,
                  (char *) ptab,
                  (int) ptsize,
                  ptform);
//        SetMouseLock (TRUE);
        SetMouseLockProc (MouseInPt);
        EnterElist (PtWindow, (char *) ptabarr,
                             ptabanz,
                             (char *) ptab,
                             (int) ptsize,
                             ptform);
        CloseControls (&PtButton);
        CloseEWindow (PtWindow);
        init_break_list ();
//        SetMouseLock (FALSE);
        SetMouseLockProc (NULL);
        DestroyWindow (PtFussWindow);
        PtListeAktiv = 0;
        SetMenue (&smenue);
        SethListBox (shListBox);
        Setlbox (slbox);
        SetAktivWindow (hWnd);
        return ptidx;
}

/**
Lieferschein auf komplett setzen.
**/

static int ldruck   = 0;
static int ldrfr    = 0;
static int lfrei    = 0;
static int lbar     = 0;
static int lsofort  = 0;
static int drwahl   = 0;

static int *lbuttons [] = {&ldruck, &ldrfr, &drwahl, NULL};

static int Setldruck (void);
static int Setlfrei (void);
static int Setldrfr (void);

/*
static int Setlbar (void);
static int Setlsofort (void);
*/
static int Setdrwahl (void);

static int BreakBox (void);
static int BoxBreak (void);

static int IsDrChoise;


static field _cbox [] = {
" LS Drucken      ",     22, 2, 0, 6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             Setldruck, 101,
" LS Komplett setzen" ,  22, 2, 2, 6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             Setlfrei, 102,
" Druckerwahl     ",     19, 2, 4, 6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             Setdrwahl, 106,
"   OK    ",             10, 1, 7, 2,  0, "", BUTTON, 0, BreakBox, 107,
" Abbruch ",             10, 1, 7, 15, 0, "", BUTTON, 0, BoxBreak, KEY5};

form cbox = {5, 0, 0, _cbox, 0,0,0,0,&ListFont};

int KomplettEnd (void)
/**
EnterButton abbrechen.
**/
{
           if (syskey == KEY5)
           {
                    break_enter ();
           }
           return 1;
}

void UnsetBox (int pos)
/**
Buttona auf selectierte setzen. (ausser pos);
**/
{
        int i;

        for (i = 0; lbuttons [i]; i ++)
        {
                  if (i == pos) continue;

                  SendMessage (cbox.mask[i].feldid, BM_SETCHECK, 0, 0l);
                  *lbuttons [i] = 0;
        }
}


int Setldruck (void)
/**
Druck setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (cbox.mask[0].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       ldruck = 0;
          }
          else
          {
                       ldruck = 1;
          }
          if (syskey == KEYCR)
          {
                       SendMessage (cbox.mask[0].feldid, BM_SETCHECK,
                       !ldruck, 0l);
                       ldruck = !ldruck;
          }
          UnsetBox (0);
          return 1;
}
void ldruckdefault (void)
{
          ldruck = 1;
//          SendMessage (cbox.mask[0].feldid, BM_SETCHECK,
//          ldruck, 0l);
          UnsetBox (0);
}


int Setldrfr (void)
/**
Freigabe und Druck setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (cbox.mask[1].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                        ldrfr = 0;
          }
          else
          {
                        ldrfr = 1;
          }
          if (syskey == KEYCR)
          {
                       SendMessage (cbox.mask[1].feldid, BM_SETCHECK,
                       !ldrfr, 0l);
                       ldrfr = !ldrfr;
          }
          UnsetBox (1);
          return 1;
}

int Setlfrei (void)

/**
Freigabe setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (cbox.mask[1].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       lfrei = 0;
          }
          else
          {
                       lfrei = 1;
          }
          if (syskey == KEYCR)
          {
                       SendMessage (cbox.mask[1].feldid, BM_SETCHECK,
                       !lfrei, 0l);
                       lfrei = !lfrei;
          }
          UnsetBox (1);
          return 1;
}

int Setlbar (void)

/**
Barverkauf setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (cbox.mask[3].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       lbar = 0;
          }
          else
          {
                       lbar = 1;
          }
          if (syskey == KEYCR)
          {
                       SendMessage (cbox.mask[3].feldid, BM_SETCHECK,
                       !lbar, 0l);
                       lbar = !lbar;
          }
          UnsetBox (3);
          return 1;
}

int Setlsofort (void)

/**
Sofortrechnung setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (cbox.mask[4].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       lsofort = 0;
          }
          else
          {
                       lsofort = 1;
          }
          if (syskey == KEYCR)
          {
                       SendMessage (cbox.mask[4].feldid, BM_SETCHECK,
                       !lsofort, 0l);
                       lsofort = !lsofort;
          }
          UnsetBox (4);
          return 1;
}

int Setdrwahl (void)

/**
Sofortrechnung setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          DruckChoise ();
          set_fkt (KomplettEnd, 5);
          SetFocus (cbox.mask[5].feldid);
          return 1;
}


int BreakBox (void)
/**
EnterButton abbrechen.
**/
{
           if (syskey == KEYUP || syskey == KEYDOWN) return 0;
           syskey = KEYCR;
           break_enter ();
           return 1;
}

int BoxBreak (void)
/**
EnterButton abbrechen.
**/
{
           if (syskey == KEYUP || syskey == KEYDOWN) return 0;
           syskey = KEY5;
           break_enter ();
           return 1;
}

int MouseInButton (POINT *mpos)
{
        int i;

        if (MouseinDlg (MainButton2.mask[2].feldid, mpos))
        {
                  return TRUE;
        }

        for (i = 5; i < 7; i ++)
        {
                if (MouseinDlg (MainButton2.mask[i].feldid, mpos))
                {
                                   return TRUE;
                }
        }
        return FALSE;
}

static void SelectAktCtr (void)
/**
Aktiven Button in EnterButton selectieren.
**/
{
        syskey = KEYCR;
        if (cbox.mask[currentfield].after)
        {
                       (*cbox.mask[currentfield].after) ();
        }
        syskey = KEYCR;
}

BOOL SetLskStatus (int ls_stat)
/**
ls_stat auf 3 setzen.
**/
{
        return TRUE;
}

void BearbKomplett (void)
/**
Auswahl aus Komplett bearbeiten.
**/
{
	short BestandBuchen = 0 ; //weldr darf nicht mehr buchen, da dies jetzt schon hier geschieht 190709
	short JourErzeugen = 1 ; //we_jour wird schon geschrieben, wenn nur gedruckt wurde 200709


         if (strcmp (sys_ben.pers_nam, " ") <= 0)
         {
             strcpy (sys_ben.pers_nam, "fit");
         }
         commitwork ();

         if (ldruck && DrKomplett)
         {
// 09.12.2003 aud Vorschalg von Detlef wird beim Drucken der Lieferschein sofort
// auf komplett gesetzt. Ist einstellbar �ber DrKomplett

                sprintf (progname, "%s\\BIN\\rswrun -q dl weldr L "
                                     "%hd %hd %s %s X %hd %hd",
                                     rosipath,
                                     we_kopf.mdn, we_kopf.fil, we_kopf.lief, we_kopf.lief_rech_nr, DruckAuswahl,BestandBuchen);
                ProcWaitExecEx (progname, SW_SHOWMINIMIZED, 0, 40, -1, 0);
         }
         else if (ldruck)
         {
                sprintf (progname, "%s\\BIN\\rswrun weldr L "
                                     "%hd %hd %s %s D %hd %hd %hd",
                                     rosipath,
                                     we_kopf.mdn, we_kopf.fil, we_kopf.lief, we_kopf.lief_rech_nr, DruckAuswahl,BestandBuchen,JourErzeugen);
                ProcWaitExecEx (progname, SW_SHOWMINIMIZED, 0, 40, -1, 0);
         }
         else if (lfrei)
         {

                sprintf (progname, "%s\\BIN\\rswrun weldr L "
                                     "%hd %hd %s %s K %hd %hd",
                                     rosipath,
                                     we_kopf.mdn, we_kopf.fil, we_kopf.lief,
									 we_kopf.lief_rech_nr, DruckAuswahl,BestandBuchen);

                ProcWaitExecEx (progname, SW_SHOWMINIMIZED, 0, 40, -1, 0);
				break_list ();
         }
         beginwork ();
}

void WriteLief (int idx)
{
    A_BEST_CLASS Best;

    a_best.mdn = akt_mdn;
    a_best.fil = akt_fil;
    a_best.a   = ratod (aufparr[idx].a);
    Best.dbreadfirst ();
    strcpy (a_best.letzt_lief_nr, we_kopf.lief);
    Best.dbupdate ();
}



void WritePr (int idx)
{
    double pr_ek_nto = ratod (aufparr[idx].ls_lad_euro); 
	double inh = aufparr[idx].min_best;
	int me_kz = aufparr[idx].me_kz;
    double we_me = ratod (aufparr[idx].auf_me); 

	if (inh == 0.0) 
	{
		inh = 1.0;
	}

    if (EktoMeEinh || me_kz == 2)
    {
			 pr_ek_nto /= inh;
    }

  
    if (_a_bas.a_typ == 1)
    {
           A_KALKHNDW_CLASS *Hndw = new A_KALKHNDW_CLASS ();
           a_kalkhndw.mdn = akt_mdn;
           a_kalkhndw.fil = akt_fil;
           a_kalkhndw.a   = ratod (aufparr[idx].a);
           int dsqlstatus = Hndw->dbreadfirst ();
           if (dsqlstatus == 0)
           {
               a_kalkhndw.pr_ek3 = a_kalkhndw.pr_ek2;
               a_kalkhndw.pr_ek2 = a_kalkhndw.pr_ek1;
               a_kalkhndw.we_me3 = a_kalkhndw.we_me2;
               a_kalkhndw.we_me2 = a_kalkhndw.we_me1;
           }
           a_kalkhndw.pr_ek1 = pr_ek_nto;
           a_kalkhndw.we_me1 = we_me;
           Hndw->dbupdate ();
		   if (multimandant_par == TRUE)
		   {
			   a_kalkhndw.mdn = 0;
			   a_kalkhndw.fil = 0;
	           Hndw->dbupdate ();
		   }
	       delete Hndw;
		   memcpy (&a_kalkpreis, &a_kalkpreis_null, sizeof (A_KALKPREIS));
		   a_kalkpreis.mdn = a_kalkhndw.mdn;
		   a_kalkpreis.fil = a_kalkhndw.fil;
		   a_kalkpreis.a = a_kalkhndw.a;
		   A_kalkpreis.dbreadfirst ();
		   a_kalkpreis.mat_o_b = a_kalkhndw.pr_ek1;
		   a_kalkpreis.hk_vollk = a_kalkhndw.pr_ek1;
		   a_kalkpreis.hk_teilk = a_kalkhndw.pr_ek1;
		   a_kalkpreis.sk_vollk = a_kalkhndw.sk_vollk;
		   a_kalkpreis.sk_teilk = a_kalkhndw.sk_teilk;
		   a_kalkpreis.fil_ek_vollk = a_kalkhndw.fil_ek_vollk;
		   a_kalkpreis.fil_ek_teilk = a_kalkhndw.fil_ek_teilk;
		   a_kalkpreis.dat = we_kopf.we_dat;
		   strcpy (a_kalkpreis.programm, "35100");
		   systime (Time);
		   strncpy (a_kalkpreis.zeit, Time,5);
		   Time[5] = 0;
		   A_kalkpreis.dbupdate ();
		   if (multimandant_par == TRUE)
		   {
			   a_kalkpreis.mdn = 0;
			   a_kalkpreis.fil = 0;
			   A_kalkpreis.dbupdate ();
		   }
           WriteLief (idx);
    }
    if (_a_bas.a_typ == 5 ||
             _a_bas.a_typ == 6 ||
             _a_bas.a_typ == 9)
    {
           A_KALK_MAT_CLASS *Mat = new A_KALK_MAT_CLASS ();
           a_kalk_mat.mdn = akt_mdn;
           a_kalk_mat.fil = akt_fil;
           a_kalk_mat.a   = ratod (aufparr[idx].a);
           int dsqlstatus = Mat->dbreadfirst ();
           a_kalk_mat.mat_o_b = pr_ek_nto;
           Mat->dbupdate ();
		   if (multimandant_par == TRUE)
		   {
			   a_kalk_mat.mdn = 0;
			   a_kalk_mat.fil = 0;
			   Mat->dbupdate ();
		   }
           delete Mat;
		   memcpy (&a_kalkpreis, &a_kalkpreis_null, sizeof (A_KALKPREIS));
		   a_kalkpreis.mdn = a_kalk_mat.mdn;
		   a_kalkpreis.fil = a_kalk_mat.fil;
		   a_kalkpreis.a = a_kalk_mat.a;
		   A_kalkpreis.dbreadfirst ();
		   a_kalkpreis.mat_o_b  = a_kalk_mat.mat_o_b;
		   a_kalkpreis.hk_vollk = a_kalk_mat.hk_vollk;
		   a_kalkpreis.hk_teilk = a_kalk_mat.hk_teilk;
		   a_kalkpreis.dat = we_kopf.we_dat;
		   strcpy (a_kalkpreis.programm, "35100");
		   systime (Time);
		   strncpy (a_kalkpreis.zeit, Time,5);
		   Time[5] = 0;
		   A_kalkpreis.dbupdate ();
		   if (multimandant_par == TRUE)
		   {
			   a_kalkpreis.mdn = 0;
			   a_kalkpreis.fil = 0;
			   A_kalkpreis.dbupdate ();
		   }
           WriteLief (idx);
    }
    if (_a_bas.a_typ2 == 1)
    {
           A_KALKHNDW_CLASS *Hndw = new A_KALKHNDW_CLASS ();
           a_kalkhndw.mdn = akt_mdn;
           a_kalkhndw.fil = akt_fil;
           a_kalkhndw.a   = ratod (aufparr[idx].a);
           int dsqlstatus = Hndw->dbreadfirst ();
           if (dsqlstatus == 0)
           {
               a_kalkhndw.pr_ek3 = a_kalkhndw.pr_ek2;
               a_kalkhndw.pr_ek2 = a_kalkhndw.pr_ek1;
               a_kalkhndw.we_me3 = a_kalkhndw.we_me2;
               a_kalkhndw.we_me2 = a_kalkhndw.we_me1;
           }
           a_kalkhndw.pr_ek1 = pr_ek_nto;
           a_kalkhndw.we_me1 = we_me;
           Hndw->dbupdate ();
		   if (multimandant_par == TRUE)
		   {
			   a_kalkhndw.mdn = 0;
			   a_kalkhndw.fil = 0;
	           Hndw->dbupdate ();
		   }
           delete Hndw;
		   memcpy (&a_kalkpreis, &a_kalkpreis_null, sizeof (A_KALKPREIS));
		   a_kalkpreis.mdn = a_kalkhndw.mdn;
		   a_kalkpreis.fil = a_kalkhndw.fil;
		   a_kalkpreis.a = a_kalkhndw.a;
		   A_kalkpreis.dbreadfirst ();
		   a_kalkpreis.mat_o_b = a_kalkhndw.pr_ek1;
		   a_kalkpreis.hk_vollk = a_kalkhndw.pr_ek1;
		   a_kalkpreis.hk_teilk = a_kalkhndw.pr_ek1;
		   a_kalkpreis.sk_vollk = a_kalkhndw.sk_vollk;
		   a_kalkpreis.sk_teilk = a_kalkhndw.sk_teilk;
		   a_kalkpreis.fil_ek_vollk = a_kalkhndw.fil_ek_vollk;
		   a_kalkpreis.fil_ek_teilk = a_kalkhndw.fil_ek_teilk;
		   a_kalkpreis.dat = we_kopf.we_dat;
		   strcpy (a_kalkpreis.programm, "35100");
		   systime (Time);
		   strncpy (a_kalkpreis.zeit, Time,5);
		   Time[5] = 0;
		   A_kalkpreis.dbupdate ();
		   if (multimandant_par == TRUE)
		   {
			   a_kalkpreis.mdn = 0;
			   a_kalkpreis.fil = 0;
			   A_kalkpreis.dbupdate ();
		   }
           WriteLief (idx);
    }
    if (_a_bas.a_typ2 == 5 ||
             _a_bas.a_typ2 == 6 ||
             _a_bas.a_typ2 == 9)
    {
           A_KALK_MAT_CLASS *Mat = new A_KALK_MAT_CLASS ();
           a_kalk_mat.mdn = akt_mdn;
           a_kalk_mat.fil = akt_fil;
           a_kalk_mat.a   = ratod (aufparr[idx].a);
           int dsqlstatus = Mat->dbreadfirst ();
           a_kalk_mat.mat_o_b = pr_ek_nto;
		   if (multimandant_par  == TRUE)
		   {
			   a_kalk_mat.mdn = 0;
			   a_kalk_mat.fil = 0;
			   Mat->dbupdate ();
		   }
           delete Mat;
		   memcpy (&a_kalkpreis, &a_kalkpreis_null, sizeof (A_KALKPREIS));
		   a_kalkpreis.mdn = a_kalk_mat.mdn;
		   a_kalkpreis.fil = a_kalk_mat.fil;
		   a_kalkpreis.a = a_kalk_mat.a;
		   A_kalkpreis.dbreadfirst ();
		   a_kalkpreis.mat_o_b  = a_kalk_mat.mat_o_b;
		   a_kalkpreis.hk_vollk = a_kalk_mat.hk_vollk;
		   a_kalkpreis.hk_teilk = a_kalk_mat.hk_teilk;
		   a_kalkpreis.dat = we_kopf.we_dat;
		   strcpy (a_kalkpreis.programm, "35100");
		   systime (Time);
		   strncpy (a_kalkpreis.zeit, Time,5);
		   Time[5] = 0;
		   A_kalkpreis.dbupdate ();
		   if (multimandant_par == TRUE)
		   {
			   a_kalkpreis.mdn = 0;
			   a_kalkpreis.fil = 0;
			   A_kalkpreis.dbupdate ();
		   }
    }
}


void WritePreise (void)
/*
Einkaufspreise zurueckschreiben.
**/
{
	//290709 mit sql_mode = 1!!!
	    short sqlm;
    extern short sql_mode;

    sqlm = sql_mode;
    sql_mode = 1;

         for (int i = 0; i < aufpanz; i ++)
         {
             lese_a_bas (ratod (aufparr[i].a));
             WritePr (i);
         }
   sql_mode = sqlm;
}

int SetKomplett ()
/**
Komplett setzen.
**/
{
         int x,y;
         int cx, cy;
		 int cy0;
         int mx;
         RECT rect;
         TEXTMETRIC tm;
         HDC hdc;
         HFONT hFont;
//		 int i;

/*
         if (abfragejnG (eWindow,
                          "Wareneingang abschlie�en ?\n", "J") == 0)

		 {
                          return 0;
		 }
*/

         if (TestQsPos () == FALSE)
         {
             return 0;
         }
 	     y =  3;
		 cy = 7;
         set_fkt (KomplettEnd, 5);
         spezfont (&ListFont);
         hFont = SetWindowFont (hMainWindow);
         hdc = GetDC (hMainWindow);
         SelectObject (hdc, hFont);
         GetTextMetrics (hdc, &tm);
         DelFont (hFont);
         ReleaseDC (hMainWindow, hdc);
         GetClientRect (eWindow, &rect);
         mx = rect.right / tm.tmAveCharWidth;
         GetFrmSize (&cbox, &cx, &cy0);

         x = max (0, (mx - cx) / 2);

         KomplettAktiv = 1;

         beginwork ();
         WritePreise ();
         commitwork ();
         ldruck = ldrfr = lfrei = lbar = lsofort = drwahl = 0;
         SetMouseLockProc (MouseInButton);
         SetBorder (WS_VISIBLE | WS_DLGFRAME | WS_POPUP);
//		 ldruckdefault(); //020609 geht so nicht 
         if (EnterButtonWindow (eWindow, &cbox, y, x, cy, cx) == -1)
         {
                        KomplettAktiv = 0;
                        set_fkt (endlist, 5);
                        CloseControls (&testub);
                        display_form (Getlbox (), &testub, 0, 0);
						syskey = 0;
                        return 0;
         }

	     syskey = 0;
         CloseControls (&testub);
         display_form (Getlbox (), &testub, 0, 0);
         KomplettAktiv = 0;
         SetMouseLockProc (NULL);
		 InvalidateRect (eWindow, NULL, TRUE);
		 UpdateWindow (eWindow);
         BearbKomplett ();
         set_fkt (endlist, 5);
         return 0;
}


/*   Gebinde eingeben          */

mfont GebUbFont = {"Courier New", 190, 0, 1, RGB (255, 255, 255),
                                             GebColor,
                                             0,
                                             NULL};
mfont GebFont   = {"Courier New", 190, 0, 1, RGB (255, 255, 255),
                                             GebColor,
                                             0,
                                             NULL};

static char gebinde [4] = {"N"};
static char palette [4] = {"N"};
static char geb_einh [4] = {"1"};
static char pal_einh [4] = {"1"};
static char geb_me [9];
static char pal_me [9];
static char gtara [9];
static char gtara_einh [3] = {"2"};

static int gebcx = 620;
static int gebcy = 400;

static int break_geb = 0;

static int buttony = gebcy - bsz0 - 20;
static int buttonpos = 5;

static int current_geb = 0;
static int gebend = 6;

static int gbanz = 0;

static int testgebinde ();
static int testpalette ();
static int testgeb_einh ();
static int testpal_einh ();

static int gebfunc1 (void);
static int gebfunc4 (void);

field _gebub[] = {
           "Etikett",  8, 0, 1,10, 0, "", DISPLAYONLY, 0, 0, 0,
           "Einheit",  8, 0, 1,20, 0, "", DISPLAYONLY, 0, 0, 0,
           "Menge  ",  0, 0, 1,30, 0, "", DISPLAYONLY, 0, 0, 0,
           "Gebinde",  10, 0, 3,  1, 0, "",      DISPLAYONLY, 0, 0, 0,
           "Palette",  10, 0, 5,  1, 0, "",      DISPLAYONLY, 0, 0, 0,
           gtara_einh,  3, 0, 7, 22, 0, "%2d",   READONLY, 0, 0, 0,
           "Einzel-Tara", 11, 0, 7,  1, 0, "",   DISPLAYONLY, 0, 0, 0,
};

form gebub = {7, 0, 0, _gebub, 0, 0, 0, 0, &GebUbFont};


field _gebform[] = {

         gebinde,     2, 0, 3, 13, 0, "",      NORMAL, 0, testgebinde, 0,
         geb_einh,    3, 0, 3, 22, 0, "%2d",   NORMAL, 0, testgeb_einh, 0,
         geb_me,      8, 0, 3, 30, 0, "%3.3f", NORMAL, 0, 0, 0,

         palette,     2, 0, 5, 13, 0, "",      NORMAL, 0, testpalette, 0,
         pal_einh,    3, 0, 5, 22, 0, "%2d",   NORMAL, 0, testpal_einh, 0,
         pal_me,      8, 0, 5, 30, 0, "%3.3f", NORMAL, 0, 0, 0,
         gtara,       8, 0, 7, 30, 0, "%3.3f", NORMAL, 0, 0, 0,
};

form gebform = {7, 0, 0, _gebform, 0, 0, 0, 0, &GebFont};

field _GebButton[] = {
(char *) &Back,              bss0, bsz0, buttonpos, 4+5*bss, 0, "", COLBUTTON, 0,
 *                                                              func5, 0,
(char *) &BuOK,              bss0, bsz0, buttonpos, 4+4*bss, 0, "", COLBUTTON, 0,
 *                                                              func6, 0,
(char *) &PfeilR,            bss0, bsz0, buttonpos, 4+3*bss, 0, "", COLBUTTON, 0,
                                                          gebfunc4, 0,
(char *) &PfeilU,            bss0, bsz0, buttonpos, 4+2*bss, 0, "", COLBUTTON, 0,
                                                          func3, 0,
(char *) &PfeilO,            bss0, bsz0, buttonpos, 4+1*bss, 0, "", COLBUTTON, 0,
                                                          func2, 0,
(char *) &PfeilL,            bss0, bsz0, buttonpos, 4,     0, "", COLBUTTON, 0,
                                                          gebfunc1, 0};

form GebButton = {6, 0, 0, _GebButton, 0, 0, 0, 0, &buttonfont};

int gebfunc1 (void)
/**
Button fuer Pfeil links bearbeiten.
**/
{

         SetFocus (gebform.mask[current_geb].feldid);
         SendMessage (gebform.mask[current_geb].feldid, WM_KEYDOWN,  (WPARAM) VK_LEFT, 0l);
         return 1;
}

int gebfunc4 (void)
/**
Button fuer Pfeil links bearbeiten.
**/
{

         SetFocus (gebform.mask[current_geb].feldid);
         SendMessage (gebform.mask[current_geb].feldid, WM_KEYDOWN,  (WPARAM) VK_RIGHT, 0l);
         return 1;
}

void PrintGLines ()
/**
Icons am Bildschirm anzeigen.
**/
{
//         RECT rect, frect;
         PAINTSTRUCT ps;
         HDC hdc;
         HPEN hPen;
         int x, y;

         hdc = BeginPaint (GebWindow, &ps);

/*
         GetFrmRectEx (&gebform, &GebFont, &frect);

         hPen = CreatePen (PS_SOLID, 2, BLACKCOL);
         SelectObject (hdc, hPen);
         x = gebcx - 4;
         y = frect.top - 30;
         MoveToEx (hdc, 2, y, NULL);
         LineTo (hdc, x, y);
         DeleteObject (hPen);

         hPen = CreatePen (PS_SOLID, 2, GRAYCOL);
         SelectObject (hdc, hPen);
         MoveToEx (hdc, 2, y - 1, NULL);
         LineTo (hdc, x, y - 2);
         DeleteObject (hPen);
*/

         y = buttony - 10;
         x = gebcx - 4;

         hPen = CreatePen (PS_SOLID, 2, BLACKCOL);
          SelectObject (hdc, hPen);
         MoveToEx (hdc, 2, y, NULL);
         LineTo (hdc, x, y);
         DeleteObject (hPen);

         hPen = CreatePen (PS_SOLID, 2, GRAYCOL);
         SelectObject (hdc, hPen);
         MoveToEx (hdc, 2, y - 1, NULL);
         LineTo (hdc, x, y - 2);
         DeleteObject (hPen);

         EndPaint (WiegWindow, &ps);
}

void SetGebFocus (void)
/**
Focus fuer Wiegmaske setzen.
**/
{

          SetFocus (gebform.mask[current_geb].feldid);
          SendMessage (gebform.mask[current_geb].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
}


LONG FAR PASCAL GebProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{

        switch(msg)
        {
              case WM_PAINT :
                    if (hWnd == GebWindow)
                    {
                                 display_form (GebWindow, &gebub, 0, 0);
                                 display_form (GebWindow, &gebform, 0, 0);
                        //        PrintGLines ();
                                 SetGebFocus ();
                    }
                    else if (hWnd == GebWindButton)
                    {
                                 display_form (GebWindButton, &GebButton, 0, 0);
                    }
                    break;
              case WM_KEYDOWN :
                         switch (wParam)
                         {
                              case VK_F5 :
                                      func5 ();
                                      return 0;

                         }
                         break;
              case WM_COMMAND :
                    if (LOWORD (wParam) == KEY5)
                    {
                            syskey = KEY5;
                            SendKey (VK_F5);
                            break;
                    }
                    break;
              case WM_DESTROY :
                    if (hWnd == hMainWindow)
                    {
	                         SetForeground ();
	                         closedbase ();
                             ExitProcess (0);
                             return 0;
                    }
                    return 0;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

static int testgeb_einh (void)
/**
Felde Kundenbestell-Einheit testen.
**/
{
/*
	       if (ptab_class.lese_ptab ("me_einh_fill",
                   geb_einh)
			!= 0)
		{
					strcpy (geb_einh, "0");
		}
*/
            display_form (GebWindow, &gebform, 0, 0);
		return 0;
}

static int testpal_einh (void)
/**
Felde Kundenbestell-Einheit testen.
**/
{
/*
	       if (ptab_class.lese_ptab ("me_einh_fill",
                   pal_einh)
			!= 0)
		{
					strcpy (pal_einh, "0");
		}
*/
            display_form (GebWindow, &gebform, 0, 0);
		return 0;
}


int testgebinde ()
/**
Eingabe fuer Gebinde testen.
**/
{
         if (gebinde[0] == 'J' || gebinde[0] == 'j')
         {
                            gebinde[0] = 'J';
                            display_form (GebWindow, &gebform, 0, 0);
                            return 0;
         }

         if (atoi (gebinde))
         {
                            gebinde[0] = 'J';
                            display_form (GebWindow, &gebform, 0, 0);
                            return 0;
         }
         gebinde[0] = 'N';
         display_form (GebWindow, &gebform, 0, 0);
         return 0;
}

int testpalette ()
/**
Eingabe fuer Gebinde testen.
**/
{
         if (palette[0] == 'J' || palette[0] == 'j')
         {
                            palette[0] = 'J';
                            return 0;
         }

         if (atoi (palette))
         {
                            palette[0] = 'J';
                            return 0;
         }
         palette[0] = 'N';
         return 0;
}

struct PT ptabmh, ptabmharr [30];
static int gebanz = 0;

static field _fptabform [] =
{
        ptabmh.ptbez,     12, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        ptabmh.ptwer1,     9, 1, 0,13, 0, "%2d", DISPLAYONLY, 0, 0, 0
};

static form fptabform = {2, 0, 0, _fptabform, 0, 0, 0, 0, &PtFont};

static field _fptabub [] =
{
        "Bezeichnung",     12, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        "Wert",             9, 1, 0,13, 0, "", DISPLAYONLY, 0, 0, 0
};

static form fptabub = {2, 0, 0, _fptabub, 0, 0, 0, 0, &PtFont};

static field _fptabvl [] =
{      "1",                  1,  0, 0, 12, 0, "", NORMAL, 0, 0, 0,
};

static form fptabvl = {1, 0, 0, _fptabvl, 0, 0, 0, 0, &PtFont};



int ShowGebMeEinh (char *me_einh)
/**
Ausahlfenster fuer Festtara anzeigen.
**/
{
        if (gebanz == 0)
        {
               dsqlstatus = ptab_class.lese_ptab_all ("me_einh_fill");
               while (dsqlstatus == 0)
               {
                   strcpy (ptabmharr[gebanz].ptbez,  ptabn.ptbezk);
                   strcpy (ptabmharr[gebanz].ptwer1, ptabn.ptwert);
                   gebanz ++;
                   if (gebanz == 30) break;
                   dsqlstatus = ptab_class.lese_ptab_all ();
               }
        }
        syskey = 0;
        ptidx = ShowPtBox (GebWindow, (char *) ptabmharr, gebanz,
                     (char *) &ptabmh,(int) sizeof (struct PT),
                     &fptabform, &fptabvl, &fptabub);
        if (syskey != KEY5 && syskey != KEYESC)
        {
                     strcpy (me_einh, ptabmharr [ptidx].ptwer1);
        }
        return ptidx;
}

int testgebfields (void)
/**
Eingabefelder testen.
**/
{
        if (current_geb == 0)
        {
                   testgebinde ();
        }
        else if (current_geb == 3)
        {
                   testpalette ();
        }
        else if (current_geb == 1)
        {
                   testgeb_einh ();
        }
        else if (current_geb == 4)
        {
                   testpal_einh ();
        }
        return (0);
}

void gebaktion (void)
/**
Aktion auf aktivem Feld.
**/
{
        switch (current_geb)
        {
                  case 0 :
                          if (gebinde[0] == 'J')
                          {
                                       gebinde[0] = 'N';
                          }
                          else
                          {
                                       gebinde[0] = 'J';
                          }
                          testgebinde ();
                          display_form (GebWindow, &gebform, 0, 0);
                          break;
                  case 1 :
                          ShowGebMeEinh (geb_einh);
                          display_form (GebWindow, &gebform, 0, 0);
                          break;
                  case 2 :
                          EnterCalcBox (GebWindow,"  Gebinde-Menge  ",
                                       geb_me, gebform.mask[2].length,
                                       gebform.mask[2].picture);
                          display_form (GebWindow, &gebform, 0, 0);
                          break;

                  case 3 :
                          if (palette[0] == 'J')
                          {
                                       palette[0] = 'N';
                          }
                          else
                          {
                                       palette[0] = 'J';
                          }
                          testpalette ();
                          display_form (GebWindow, &gebform, 0, 0);
                          break;
                  case 4 :
                          ShowGebMeEinh (pal_einh);
                          display_form (GebWindow, &gebform, 0, 0);
                          break;
                  case 5 :
                          EnterCalcBox (GebWindow,"  Paletten-Menge  ",
                                       pal_me, gebform.mask[5].length,
                                       gebform.mask[5].picture);
                          display_form (GebWindow, &gebform, 0, 0);
                          break;
                  case 6 :
                          EnterCalcBox (GebWindow,"       Tara       ",
                                       gtara, gebform.mask[6].length,
                                       gebform.mask[6].picture);
                          display_form (GebWindow, &gebform, 0, 0);
                          break;
             }
}

int IsGebMessage (MSG *msg)
/**
HotKey Testen.
**/
{
         POINT mousepos;

         switch (msg->message)
         {
              case WM_KEYDOWN :
              {
                     switch (msg->wParam)
                     {
                            case VK_ESCAPE :
                            case VK_F5 :
                                    break_geb = 1;
                                    return TRUE;
                            case VK_RETURN :
                                    gebaktion ();
                                    return TRUE;
                            case VK_DOWN :
                                    testgebfields ();
                                    current_geb ++;
                                    if (current_geb > gebend)
                                                current_geb = 0;
                                    SetGebFocus ();
                                    return TRUE;
                            case VK_UP :
                                   testgebfields ();
                                   current_geb --;
                                   if (current_geb < 0)
                                                current_geb = gebend;
                                    SetGebFocus ();
                                    return TRUE;
                            case VK_TAB :
                                    testgebfields ();
                                    if (GetKeyState (VK_SHIFT) < 0)
                                    {
                                             current_geb --;
                                             if (current_geb < 0)
                                                  current_geb = gebend;
                                     }
                                     else
                                     {
                                             current_geb ++;
                                             if (current_geb > gebend)
                                                  current_geb = 0;
                                     }
                                     SetGebFocus ();
                                     return TRUE;

                             case VK_F9 :
                                     syskey = KEY9;
                                     if (current_geb == 1)
                                     {
                                               ShowGebMeEinh (geb_einh);
                                               display_form (GebWindow, &gebform, 0, 0);
                                     }
                                     else if (current_geb == 4)
                                     {
                                               ShowGebMeEinh (pal_einh);
                                               display_form (GebWindow, &gebform, 0, 0);
                                     }
                                     return TRUE;

                            default :
                                    return FALSE;
                     }
                     break;
              }
              case WM_LBUTTONDOWN :
              {
                    GetCursorPos (&mousepos);

                    if (MouseinWindow (gebform.mask[0].feldid, &mousepos))
                    {
                          current_geb = 0;
/*
                          EnterNumBox (GebWindow,"  Gebinde-Etikett  ",
                                       gebinde, gebform.mask[0].length,
                                       "%1d");
*/
                          if (gebinde[0] == 'J')
                          {
                                       gebinde[0] = 'N';
                          }
                          else
                          {
                                       gebinde[0] = 'J';
                          }
                          testgebinde ();
                          display_form (GebWindow, &gebform, 0, 0);
//                          PostMessage (GebWindow, WM_KEYDOWN, VK_RETURN, 0l);
                          return TRUE;
                    }
                    else if (MouseinWindow (gebform.mask[1].feldid, &mousepos))
                    {
/*
                          EnterNumBox (GebWindow,"  Gebinde-Einheit  ",
                                       geb_einh, gebform.mask[1].length,
                                       gebform.mask[1].picture);
*/
                          current_geb = 1;
                          ShowGebMeEinh (geb_einh);
                          display_form (GebWindow, &gebform, 0, 0);
//                          PostMessage (GebWindow, WM_KEYDOWN, VK_RETURN, 0l);
                          return TRUE;
                    }
                    else if (MouseinWindow (gebform.mask[2].feldid, &mousepos))
                    {
                          current_geb = 2;
                          EnterCalcBox (GebWindow,"  Gebinde-Menge  ",
                                       geb_me, gebform.mask[2].length,
                                       gebform.mask[2].picture);
                          display_form (GebWindow, &gebform, 0, 0);
//                          PostMessage (GebWindow, WM_KEYDOWN, VK_RETURN, 0l);
                          return TRUE;
                    }

                    else if (MouseinWindow (gebform.mask[3].feldid, &mousepos))
                    {
                          current_geb = 3;
/*
                          EnterNumBox (GebWindow,"  Paletten-Etikett  ",
                                       palette, gebform.mask[3].length,
                                       "%1d");
*/
                          if (palette[0] == 'J')
                          {
                                       palette[0] = 'N';
                          }
                          else
                          {
                                       palette[0] = 'J';
                          }
                          testpalette ();
                          display_form (GebWindow, &gebform, 0, 0);
//                          PostMessage (GebWindow, WM_KEYDOWN, VK_RETURN, 0l);
                          return TRUE;
                    }
                    else if (MouseinWindow (gebform.mask[4].feldid, &mousepos))
                    {
/*
                          EnterNumBox (GebWindow,"  Paletten-Einheit  ",
                                       pal_einh, gebform.mask[4].length,
                                       gebform.mask[4].picture);
*/
                          current_geb = 4;
                          ShowGebMeEinh (pal_einh);
                          display_form (GebWindow, &gebform, 0, 0);
                          return TRUE;
                    }
                    else if (MouseinWindow (gebform.mask[5].feldid, &mousepos))
                    {
                          current_geb = 5;
                          EnterCalcBox (GebWindow,"  Paletten-Menge  ",
                                       pal_me, gebform.mask[5].length,
                                       gebform.mask[5].picture);
                          display_form (GebWindow, &gebform, 0, 0);
                          return TRUE;
                    }
                    else if (MouseinWindow (gebform.mask[6].feldid, &mousepos))
                    {
                          current_geb = 6;
                          EnterCalcBox (GebWindow, "       Tara       ",
                                       gtara, gebform.mask[6].length,
                                       gebform.mask[6].picture);
                          display_form (GebWindow, &gebform, 0, 0);
                          return TRUE;
                    }
              }
              case WM_USER :
                    if (LOWORD (msg->wParam) == BUTARA)
                    {
                          dotara ();
                          return TRUE;
                    }
         }
         return FALSE;
}


int dogebinde ()
/**
Parameter fuer Gebinde - und Palettenetikett eingeben.
**/
{
        MSG msg;
        int  x, y;
        int cx, cy;

        cx = gebcx;
        x = (screenwidth - cx) / 2;
        cy = gebcy;
        y = (screenheight - cy) / 2;

        GebWindow  = CreateWindowEx (
                              0,
                              "GebWind",
                              "",
                              WS_POPUP | WS_DLGFRAME | WS_VISIBLE,
                              x, y,
                              cx, cy,
                              WiegWindow,
                              NULL,
                              hMainInst,
                              NULL);
        if (GebWindow == NULL) return (0);

        x = 5;
        cx -= 15;
        y = buttony;
        cy = bsz0 + 10;

        GebWindButton  = CreateWindowEx (
                              WS_EX_CLIENTEDGE,
                              "GebWindB",
                              "",
                              WS_CHILD | WS_VISIBLE,
                              x, y,
                              cx, cy,
                              GebWindow,
                              NULL,
                              hMainInst,
                              NULL);

        EnableWindow (WiegWindow, FALSE);
        create_enter_form (GebWindow, &gebform, 0, 0);
        current_geb = 0;
        SetGebFocus ();
        break_geb = 0;
        while (GetMessage (&msg, NULL, 0, 0))
        {
              if (IsGebMessage (&msg));
              else
              {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
              }
              if (break_geb) break;
        }
        CloseControls (&gebub);
        CloseControls (&gebform);
        CloseControls (&GebButton);
        DestroyWindow (GebWindButton);
        DestroyWindow (GebWindow);
        GebWindow = NULL;
        SetActiveWindow (WiegWindow);
        EnableWindow (WiegWindow, TRUE);
        update_kun_geb ();
        return (0);
}

void ToVorgabe (char *veinh, short einh)
/**
Vorgabe-Einheit als Klartext.
**/
{
        switch (einh)
        {
           case 1 :
                strcpy (veinh, "St�ck");
                break;
           case 2:
                strcpy (veinh, "kg");
                break;
           case 4:
                strcpy (veinh, "Preis");
                break;
           case 5:
                strcpy (veinh, "Karton");
                break;
           case 6:
                strcpy (veinh, "Palette");
                break;
           default:
                strcpy (veinh, "kg");
                break;
        }
}


void gebindelsp (void)
/**
Gebindevorgaben in lsp testen.
Wenn brauchbare Gebindewerte in der lsp vorhanden sind,
Gebindewerte aus a_kun_gx ueberschreiben.
**/
{
/*
        short   me_einh_kun1;
        double  auf_me1;
        double  inh1;
        short   me_einh_kun2;
        double  auf_me2;
        double  inh2;
        short   me_einh_kun3;
        double  auf_me3;
        double  inh3;

        me_einh_kun1 = atoi (aufps.me_einh_kun1);
        auf_me1      = ratod (aufps.auf_me1);
        inh1         = ratod (aufps.inh1);

        me_einh_kun2 = atoi (aufps.me_einh_kun2);
        auf_me2      = ratod (aufps.auf_me2);
        inh2         = ratod (aufps.inh2);

        me_einh_kun3 = atoi (aufps.me_einh_kun3);
        auf_me3      = ratod (aufps.auf_me3);
        inh3         = ratod (aufps.inh3);

        if (me_einh_kun1 <= 0 &&
            me_einh_kun2 <= 0 &&
            me_einh_kun3 <= 0)
        {
            return;
        }

        if (me_einh_kun1 == 2)
        {
              a_kun_geb.geb_fill = 2;
              sprintf (geb_einh, "%hd", a_kun_geb.geb_fill);
              ToVorgabe (veinh1, a_kun_geb.geb_fill);
        }
        else
        {
              a_kun_geb.geb_fill = 1;
              sprintf (geb_einh, "%hd", a_kun_geb.geb_fill);
              ToVorgabe (veinh1, a_kun_geb.geb_fill);
        }

        if (me_einh_kun2 == 5)
        {
              a_kun_geb.pal_fill = 5;
              sprintf (pal_einh, "%hd", a_kun_geb.pal_fill);
              ToVorgabe (veinh2, a_kun_geb.pal_fill);
        }
        else if (me_einh_kun2 == 2)
        {
              a_kun_geb.pal_fill = 2;
              sprintf (pal_einh, "%hd", a_kun_geb.pal_fill);
              ToVorgabe (veinh2, a_kun_geb.pal_fill);
        }
        else
        {
              a_kun_geb.pal_fill = 1;
              sprintf (pal_einh, "%hd", a_kun_geb.pal_fill);
              ToVorgabe (veinh2, a_kun_geb.pal_fill);
        }

        if (auf_me2 > (double) 0.0 && me_einh_kun2)
        {
              if (inh3)
              {
                  auf_me2 = inh3;
              }
              sprintf (pal_me,   "%.0lf",  auf_me2);
              strcpy (vorgabe2, pal_me);
        }
		else if (inh3)
		{
              sprintf (pal_me,   "%.3lf",  inh3);
              strcpy (vorgabe2, pal_me);
		}

        if (auf_me1 > (double) 0.0 && me_einh_kun1)
        {
              if (inh2)
              {
                  auf_me1 = inh2;
              }
              sprintf (geb_me,   "%.3lf", auf_me1);
              strcpy (vorgabe1, geb_me);
        }
		else if (inh2)
		{
              sprintf (geb_me,   "%.3lf",  inh2);
              strcpy (vorgabe1, geb_me);
		}
*/
}


void fetch_std_geb (void)
/**
Standard-Gebindeeinstellungen aus a_kun lesen und in kun_geb uebertragen.
Der Eintrag wird nur temporaer benutzt        .
**/
{
/*
        short sqm;
        extern short sql_mode;


        strcpy (a_kun_geb.geb_eti, "N");
        strcpy (a_kun_geb.pal_eti, "N");
        a_kun_geb.geb_fill = 0;
        a_kun_geb.pal_fill = 0;
        a_kun_geb.geb_anz = 0;
        a_kun_geb.pal_anz = 0;

        a_kun_geb.mdn = 1;
        a_kun_geb.fil = 0;
        a_kun_geb.kun = lsk.kun;
        a_kun_geb.a   = ratod (aufps.a);
        a_kun_geb.tara   = 0;
        a_kun_geb.mhd   = 0;
        strcpy (gebinde, a_kun_geb.geb_eti);

        sprintf (geb_einh, "%hd", a_kun_geb.geb_fill);
        ToVorgabe (veinh1, a_kun_geb.geb_fill);

        sprintf (geb_me,   "%.3lf",
            (double) ((double) a_kun_geb.geb_anz / 1000));
        strcpy (vorgabe1, geb_me);

        strcpy (palette, a_kun_geb.pal_eti);

        sprintf (pal_einh, "%hd", a_kun_geb.pal_fill);
        ToVorgabe (veinh2, a_kun_geb.pal_fill);

        sprintf (pal_me,   "%.3lf",
            (double) ((double) a_kun_geb.pal_anz / 1000));
        strcpy (vorgabe2, pal_me);

        gebindelsp ();
        a_kun_geb.geb_anz = (long) (double) (ratod (geb_me) * 1000);
        a_kun_geb.pal_anz = (long) (double) (ratod (pal_me) * 1000);

        sprintf (gtara,     "%.3lf", a_kun_geb.tara);

        if (tara_kz == 1)
        {
                  sprintf (gtara,     "%.3lf", a_hndw.tara);
                  a_kun_geb.tara =  a_hndw.tara;
        }
        if (mhd_kz == 1)
        {
              if (_a_bas.hbk_kz[0] == 'T' ||
                  _a_bas.hbk_kz[0] <= ' ')
              {
                  a_kun_geb.mhd =  _a_bas.hbk_ztr;
              }
        }
        sprintf (aufps.tara,     "%.3lf", a_kun_geb.tara);
        sqm = sql_mode;
        sql_mode = 1;
        geb_class.update_kun_geb ();
        sql_mode = sqm;
*/
}

void updategeblsp (void)
/**
Gebinde in lsp aktialisieren.
**/
{
/*
 	    sprintf (aufps.inh2, "%.3lf", (double) ((double)
				        a_kun_geb.geb_anz / 1000));
	    sprintf (aufps.inh3, "%.3lf", (double) ((double)
				        a_kun_geb.pal_anz / 1000));

		sprintf (aufps.me_einh_kun1, "%d", a_kun_geb.geb_fill);
		sprintf (aufps.me_einh_kun2, "%d", a_kun_geb.pal_fill);

        memcpy (&aufparr [aufpidx], &aufps, sizeof (struct AUFP_S));
*/
}


void update_kun_geb (void)
/**
Tabelle kun_geb aktualisieren.
**/
{
/*
        short sqm;
        extern short sql_mode;

        sqm = sql_mode;
        strcpy (a_kun_geb.geb_eti, gebinde);
        a_kun_geb.geb_fill = atoi (geb_einh);
        ToVorgabe (veinh1, a_kun_geb.geb_fill);
        strcpy (vorgabe1, geb_me);
        a_kun_geb.geb_anz =  (long) (double) (ratod (geb_me) * 1000);

        strcpy (a_kun_geb.pal_eti, palette);
        a_kun_geb.pal_fill = atoi (pal_einh);
        ToVorgabe (veinh2, a_kun_geb.pal_fill);
        strcpy (vorgabe2, pal_me);
        a_kun_geb.pal_anz =  (long) (double) ratod (pal_me) * 1000;
        a_kun_geb.tara    =  ratod (gtara);
        sprintf (aufps.tara,     "%.3lf", a_kun_geb.tara);
        sql_mode = 1;
        geb_class.update_kun_geb ();
        sql_mode = sqm;
        updategeblsp ();
*/
}


/**

Neuen Lieferschein eingeben.

**/

mfont LiefKopfFontT = {"Courier New", 200, 0, 1, BLACKCOL,
                                                RGB (0, 255, 255),
                                                0,
                                                NULL};

mfont LiefKopfFontE = {"Courier New", 200, 0, 1,
                                                RGB (0, 0, 0),
                                                RGB (255, 255, 255),
                                                0,
                                                NULL};


mfont LiefKopfFontTD = {"Courier New", 150, 0, 1, BLACKCOL,
                                                RGB (0, 255, 255),
                                                0,
                                                NULL};

mfont LiefKopfFontED = {"Courier New", 150, 0, 1,
                                                RGB (0, 0, 0),
                                                RGB (255, 255, 255),
                                                0,
                                                NULL};


static char KundeFil0 [14] = {"Kunden-Nr   :"};
static char KundeFil1 [14] = {"Filial-Nr   :"};
static char kunfil[3];
static long gen_lief_nr;

static field _LiefKopfT[] = {
"Lieferschein-Nr:",     16, 0, 1, 2, 0, "", DISPLAYONLY, 0, 0, 0,
"Lieferant      :",     16, 0, 2, 2, 0, "", DISPLAYONLY, 0, 0, 0,
};

// LOOK&Feel 18.11.2010 RoW
static field _LiefKopfT_new[] = {
"Lieferschein-Nr ",     16, 0, 1, 2, 0, "", WTRANSPARENT, 0, 0, 0,
"Lieferant       ",     16, 0, 2, 2, 0, "", WTRANSPARENT, 0, 0, 0,
};

form LiefKopfT = {2, 0, 0, _LiefKopfT, 0, 0, 0, 0, &LiefKopfFontT};


static field _LiefKopfTD[] = {
lief_krz1,        17, 0, 5,18, 0, "", DISPLAYONLY, 0, 0, 0,
"WE.Datum      ", 13, 0, 7, 2, 0, "", DISPLAYONLY, 0, 0, 0,
"Bel.Eing.Dat  ", 13, 0, 8, 2, 0, "", DISPLAYONLY, 0, 0, 0,
bestellhinweis,   60, 0, 9, 2, 0, "", DISPLAYONLY, 0, 0, 0,
"Status        ", 13, 0,12, 2, 0, "", DISPLAYONLY, 0, 0, 0,
ls_stat_txt,      16, 0,12,28, 0, "", DISPLAYONLY, 0, 0, 0
};

// LOOK&Feel 18.11.2010 RoW
static field _LiefKopfTD_new[] = {
lief_krz1,        17, 0, 5,18, 0, "", WTRANSPARENT, 0, 0, 0,
"WE.Datum      ", 13, 0, 7, 2, 0, "", WTRANSPARENT, 0, 0, 0,
"Bel.Eing.Dat  ", 13, 0, 8, 2, 0, "", WTRANSPARENT, 0, 0, 0,
bestellhinweis,   60, 0, 9, 2, 0, "", WTRANSPARENT, 0, 0, 0,
"Status        ", 13, 0,12, 2, 0, "", WTRANSPARENT, 0, 0, 0,
ls_stat_txt,      16, 0,12,28, 0, "", WTRANSPARENT, 0, 0, 0
};

form LiefKopfTD = {6, 0, 0, _LiefKopfTD, 0, 0, 0, 0, &LiefKopfFontTD};

// LOOK&Feel 18.11.2010 RoW
void NewWeKopfAttributes ()
{
	LiefKopfT.mask = _LiefKopfT_new;
	LiefKopfTD.mask = _LiefKopfTD_new;
}

static int ls_stat_pos = 5;
static int setlspos = 0;
static int SetKunFil (void);
static int ReadLief (void);


static field _LiefKopfE[] = {
  lief_nr,               17, 0, 1,18, 0, "", EDIT,        0, ReadLS, 0,
  lieferant,             17, 0, 2,18, 0, "",   EDIT,      0, ReadLS, 0,
};

form LiefKopfE = {2, 0, 0, _LiefKopfE, 0, 0, 0, 0, &LiefKopfFontE};


static field _LiefKopfED[] = {
  lieferdatum,         11, 0, 7,18, 0, "dd.mm.yyyy",
                                              EDIT,     0, 0, 0,
  beldatum,            11, 0, 8,18, 0, "dd.mm.yyyy",
                                              EDIT,     0, 0, 0,
  ls_stat,              2, 0,12,18, 0, "",
                                              READONLY, 0, 0, 0
};

form LiefKopfED = {3, 0, 0, _LiefKopfED, 0, 0, 0, 0, &LiefKopfFontED};


static field _LiefKopfBestT[] = {
"Bestellung    ", 13, 0,17, 2, 0, "", DISPLAYONLY, 0, 0, 0
};

form LiefKopfBestT = {1, 0, 0, _LiefKopfBestT, 0, 0, 0, 0, &LiefKopfFontTD};

static field _LiefKopfBest[] = {
  bestell_nr,              11, 0,17,18, 0, "",
                                              READONLY, 0, 0, 0
};

form LiefKopfBest = {1, 0, 0, _LiefKopfBest, 0, 0, 0, 0, &LiefKopfFontED};
static int lspos = 0;
static int lsend = 4;

void FillLSForm (void)
/**
Datenbankfelder in Maske uebertragn.
**/
{
/*
             dlong_to_asc (we_kopf.we_dat, lieferdatum);
             dlong_to_asc (we_kopf.blg_eing_dat, beldatum);
             strcpy (lieferzeit, lsk.lieferzeit);
             sprintf (ls_stat, "%hd", we_kopf.we_status);
             strcpy (kun_krz1, _adr.adr_krz);
			 sprintf (ls_stat, "%hd", we_kopf.we_status);
*/
/*
             ptab_class.lese_ptab ("we_stat", ls_stat);
             strcpy (ls_stat_txt, ptabn.ptbezk);
*/
}

void FromLSForm (void)
/**
Datenbankfelder in Maske uebertragn.
**/
{
             short mdn;
             short fil;

             mdn = Mandant;
             fil = Filiale;

             we_kopf.we_dat       = dasc_to_long (lieferdatum);
             we_kopf.blg_eing_dat = dasc_to_long (beldatum);
             we_kopf.we_status    = atoi (ls_stat);
}

static int readstatus = 0;
static int freestat = 0;

void FreeLS (void)
/**
Lieferschein-Nummer freigeben.
**/
{
     short mdn;
     short fil;

	 return;
     mdn = Mandant;
     fil = Filiale;

     if (freestat == 1) return;

     beginwork ();
     dsqlstatus = nveinid (mdn, fil,
                          "ls",  gen_lief_nr);
     commitwork ();
     freestat = 1;
}

void UpdateLfd (void)
/**
Laufende Nummer in we_kopf updaten.
**/
{
	  char sqlbuffer [1024];

	  sprintf (sqlbuffer, "update we_kopf set lfd = %ld "
		                  "where mdn = %hd "
						  "and   fil = %hd "
                          "and   lief_rech_nr = \"%s\" "
                          "and   lief = \"%s\" "
                          "and   blg_typ = \"%s\"",
						  we_kopf.lfd, we_kopf.mdn, we_kopf.fil,
						  we_kopf.lief_rech_nr, we_kopf.lief, we_kopf.blg_typ);
	  DbClass.sqlcomm (sqlbuffer);
}


int WriteLS (void)
/**
Lieferscheinkopf schreiben.
**/
{
    char datum [12];
    long dat1, dat2;

    if (readstatus > 1) return 0;

	if (strcmp (lief_nr,   "                ") <= 0) return 0;
	if (strcmp (lieferant, "                ") <= 0) return 0;

    if (TestQs (0) == FALSE)
    {
        return 0;
    }

    sysdate (datum);
    dat1 = dasc_to_long (datum);
    dat2 = dasc_to_long (lieferdatum);
    if (dat1 > dat2)
    {
        disp_messG ("Lieferdatum nicht korrekt", 2);
        return (0);
    }

	if (numeric (we_kopf.lief))
	{
		we_kopf.lief_s = atol (we_kopf.lief);
	}

    FromLSForm ();
    if (Lock_Lsk () == FALSE)
    {
        PostQuitMessage (0);
        break_lief_enter = 1;
        rollbackwork ();
        return 0;
    }
    beginwork ();
	if (neuer_we)
	{
         we_kopf.lfd = AutoNr.GetGJ(1, 0, "we_nr");
//         we_kopf.lfd = AutoNr.GetNr(0);
		 UpdateLfd ();
	}
    WeKopfClass.dbupdate ();
    ActivateColButton (BackWindow2, &MainButton, 1, 1, 1);
    set_fkt (menfunc2, 6);
    commitwork ();
	neuer_we = FALSE;
	if (liste_direct)
	{
		menfunc2 ();
	}
    return 0;
}


int  ReadLfd (void)
/**
Laufende Nummer in we_kopf updaten.
**/
{
	  char sqlbuffer [1024];
	  int dsqlstatus;


      DbClass.sqlout ((long *) &we_kopf.lfd, 2, 0);

	  sprintf (sqlbuffer, "select lfd from we_kopf "
		                  "where mdn = %hd "
						  "and   fil = %hd "
                          "and   lief_rech_nr = \"%s\" "
                          "and   lief = \"%s\" "
                          "and   blg_typ = \"%s\"",
						  we_kopf.mdn, we_kopf.fil,
						  we_kopf.lief_rech_nr, we_kopf.lief, we_kopf.blg_typ);
	  dsqlstatus = DbClass.sqlcomm (sqlbuffer);
      return dsqlstatus;
}

int  ReadWEBestell (int best_nr)
/**
Laufende Nummer in we_kopf updaten.
**/
{
	  char sqlbuffer [1024];
	  char cbest_nr [17];
	  int dsqlstatus;

	  sprintf (cbest_nr,"%ld",best_nr);

      DbClass.sqlout ((char *) &we_kopf.lief, 0, 17);
      DbClass.sqlout ((char *) &we_kopf.lief_rech_nr, 0, 17);
      DbClass.sqlout ((long *) &we_kopf.lfd, 2, 0);
      DbClass.sqlout ((char *) &we_kopf.blg_typ, 0, 2);

	  sprintf (sqlbuffer, "select lief,lief_rech_nr,lfd,blg_typ from we_kopf "
		                  "where mdn = %hd "
						  "and   fil = %hd "
						  "and koresp_best0 = \"%s\" ",
						  Mandant, Filiale,
						  cbest_nr);
	  dsqlstatus = DbClass.sqlcomm (sqlbuffer);
      return dsqlstatus;
}
int  TestWE (void)
/**
Laufende Nummer in we_kopf updaten.
**/
{
	  char clief [17];
	  char clief_rech_nr [17];
	  int dsqlstatus;

	  sprintf (clief,"%s",we_kopf.lief);
	  sprintf (clief_rech_nr,"%s",we_kopf.lief_rech_nr);

      DbClass.sqlout ((short *) &we_kopf.mdn, 1, 0);
      DbClass.sqlin ((short *) &we_kopf.mdn, 1, 0);
      DbClass.sqlin ((short *) &we_kopf.fil, 1, 0);
      DbClass.sqlin ((char *) we_kopf.lief, 0, 17);
      DbClass.sqlin ((char *) we_kopf.lief_rech_nr, 0, 17);

	  dsqlstatus = DbClass.sqlcomm ("select mdn from we_kopf "
		                  "where mdn = ? "
						  "and   fil = ? "
						  "and   lief = ? "
						  "and   lief_rech_nr = ? ");
      return dsqlstatus;
}

void TestBestMulti (void)
/**
Pruefen, ob bei vorhandenem Wareneingang noch Bestellungen zugeordnet werden koennen.
**/
{
	   BOOL BuOK;

       best_koresp = 0;
	   BuOK = FALSE;
	   if (strcmp (we_kopf.koresp_best0, "                ") <= 0)
	   {
		           BuOK = TRUE;
                   ActivateColButton (BackWindow2, &MainButton,  2, 1, 1);
	   }
	   if (multibest == FALSE) return;
	   if (BuOK) return;
	   for (best_koresp = 1; best_koresp <10; best_koresp ++)
	   {
 	               if (strcmp (korestab[best_koresp], "                ") <= 0)
				   {
                              ActivateColButton (BackWindow2, &MainButton,  2, 1, 1);
							  return;
				   }
				   best_koresp ++;
	   }
}


int ReadLS (void)
/**
Lieferschein nach Lieferschein-Nr und Lieferant lesen.
**/
{
	        int dsqlstatus;
	        char datum [12];
            readstatus = 0;
			int i;

			if (strcmp (lieferant, "                 ") <= 0) return 0;
			if (strcmp (lief_nr, "                 ") <= 0) return 0;
			for (i = 0; i < 10; i ++) *termtab[i] = LongNull;
			for (i = 0; i < 10; i ++) *lieftab[i] = LongNull;
			sysdate (datum);
            freestat = 0;
			strcpy (we_kopf.lief_rech_nr, lief_nr);
			we_kopf.mdn = Mandant;
			we_kopf.fil = Filiale;
			we_kopf.lfd = 0;
			strcpy (we_kopf.blg_typ, "L");
			strcpy (we_kopf.lief, lieferant);
			ReadLfd ();
            best_koresp = 0;
            dsqlstatus = WeKopfClass.dbreadfirst ();
            if (ReadLief () == 100)
			{
                ActivateColButton (BackWindow2, &MainButton, 2, -1, 1);
				return 100;
			}
            if (dsqlstatus == 0 && we_kopf.we_status < 1)  //200709 vorher 3
            {

				   we_kopf.bearb = dasc_to_long (datum);
                   beginwork ();
                   if (Lock_Lsk () == FALSE)
                   {
                          break_lief_enter = 1;
                          return -1;
                   }
                   ActivateColButton (BackWindow3, &MainButton2, 2, 1, 1);
                   ActivateColButton (BackWindow2, &MainButton,  1, 1, 1);
				   TestBestMulti ();

                   set_fkt (menfunc2, 6);
                   FillLSForm ();
				   neuer_we = FALSE;
  	               if (liste_direct)
				   {
		                    menfunc2 ();
				   }
            }
            else if (dsqlstatus == 100)
            {
				   disp_messG ("Neuer Wareneingang", 1);
                   ActivateColButton (BackWindow2, &MainButton, 2, 1, 1);
				   we_kopf.akv   = dasc_to_long (datum);
				   we_kopf.bearb = dasc_to_long (datum);
				   we_kopf.we_dat = dasc_to_long (datum);
				   we_kopf.blg_eing_dat = dasc_to_long (datum);
				   strcpy (we_kopf.pr_erf_kz, lief.we_pr_kz);
				   strcpy (we_kopf.a_best_kz, lief.we_erf_kz);
				   we_kopf.we_status = 0;
                   beginwork ();
				   WeKopfClass.dbupdate ();
                   if (Lock_Lsk () == FALSE)
                   {
                          break_lief_enter = 1;
                          return -1;
                   }
                   ActivateColButton (BackWindow3, &MainButton2,  2,  1, 1);
                   ActivateColButton (BackWindow2, &MainButton,   1,  -1, 1);
                   set_fkt (menfunc2, 6);
				   neuer_we = TRUE;
            }
			else if (we_kopf.we_status > 2)
			{
				   disp_messG ("Lieferschein schon abgeschlossen", 2);
			        EnableWindow (hMainWindow, FALSE);
			        EnableWindow (AufKopfWindow, FALSE);

                   if (abfragejnG (NULL, "Soll der Beleg nachgedruckt werden  ?", "J"))
				   {
					   Nachdruck();
				   }
			        EnableWindow (hMainWindow, TRUE);
			        EnableWindow (AufKopfWindow, TRUE);

				   return -1;
			}
			else if (we_kopf.we_status > 0)
			{
				   disp_messG ("Lieferschein ist schon gedruckt", 2);
			        EnableWindow (hMainWindow, FALSE);
			        EnableWindow (AufKopfWindow, FALSE);
                   if (abfragejnG (NULL, "Soll der Beleg nachgedruckt werden  ?", "J"))
				   {
					   Nachdruck();
				   }
			        EnableWindow (hMainWindow, TRUE);
			        EnableWindow (AufKopfWindow, TRUE);
				   return -1;
			}
            return 0;
}


void Nachdruck()
{
	  FILE *lst ;
  char datenam[22] ;
  char iformname[22] ;
  char buffer [512] ;
  char komma[128] ;
 int label = 0;

  strcpy(iformname,"35502");
  

  sprintf ( datenam , "%s.llf", iformname ) ;

  if (getenv ("TMPPATH"))
  {
    sprintf (buffer, "%s\\%s", getenv("TMPPATH"), datenam);
  }
  else
  {
    strcpy (buffer, datenam );
  }
  remove (buffer) ;

  if ( (lst = fopen (buffer, "wt" )) == NULL ) 
  {
                     print_mess (2, "%s kann nicht ge�ffnet werden", buffer);
                     return ;
  }


        memset ( zeilbuffer, '\0', 128 ) ;
        sprintf ( zeilbuffer, "NAME %s" , iformname ) ;
        fprintf ( lst , "%s\n" , zeilbuffer ) ;


        memset ( zeilbuffer, '\0', 128 ) ;
        sprintf ( zeilbuffer, "DRUCK %hd" , DruckAuswahl ) ;
        fprintf ( lst , "%s\n" , zeilbuffer ) ;

        memset ( zeilbuffer, '\0', 128 ) ;
        sprintf ( zeilbuffer, "LABEL %hd" , label ) ;
        fprintf ( lst , "%s\n" , zeilbuffer ) ;


        memset ( zeilbuffer, '\0', 80 ) ;
        sprintf ( zeilbuffer, "%-16s %12s %12s","#Feldname", "von", "bis") ;
        fprintf ( lst , "%s\n" , zeilbuffer ) ;

        memset ( zeilbuffer, '\0', 80 ) ;
        sprintf ( zeilbuffer, "%-16s %12s %12s","lief_rech_nr", lief_nr, lief_nr) ;
       fprintf ( lst , "%s\n" , zeilbuffer ) ;

        memset ( zeilbuffer, '\0', 80 ) ;
        sprintf ( zeilbuffer, "%-16s %12s %12s","lief", lieferant, lieferant) ;
        fprintf ( lst , "%s\n" , zeilbuffer ) ;

		fclose ( lst ) ;




  if (getenv ("TMPPATH"))
  {
    sprintf (komma, "%s -name %s -datei %s\\%s","dr70001",iformname, getenv("TMPPATH"), datenam);
  }
  else
  {
    strcpy (komma, datenam );
  }

    HCURSOR oldcursor = SetCursor ( LoadCursor ( NULL , IDC_WAIT)) ;

    ProcWaitExecEx ( komma, SW_SHOWNORMAL, 0, 40, -1, 0);
}



int SetKunFil (void)
{

          display_form (AufKopfWindow, &LiefKopfTD, 0, 0);
          display_form (AufKopfWindow, &LiefKopfED, 0, 0);
          return (0);
}


void FillLskAdr (void)
/**
Adressfelder in Lsk fuellen.
**/
{
}

int ReadLief (void)
{
          short mdn;
          short fil;

          mdn = Mandant;
		  fil = Filiale;

          lief.mdn = mdn;
          lief.fil = fil;
		  strcpy (lief.lief, lieferant);
          kun_krz1[0] = (char) 0;
          dsqlstatus = LiefClass.dbreadfirst ();
		  if (dsqlstatus == 100 && lief.mdn)
		  {
			         lief.mdn = 0;
                     dsqlstatus = LiefClass.dbreadfirst ();
		  }
          if (dsqlstatus == 0)
          {
                      adr_class.lese_adr (lief.adr);
                      strcpy (lief_krz1, _adr.adr_krz);
                      FillLskAdr ();
          }
          if (dsqlstatus)
          {
			      print_messG (2, "Lieferant %s nicht gefunden",
					               lieferant);
				  lspos = 1;
				  setlspos = 1;
                  strcpy (lieferant, " ");
				  return 100;
          }

          current_form = &LiefKopfTD;
          display_field (AufKopfWindow, &LiefKopfTD.mask[0], 0, 0);
          return 0;
}


void DisplayLiefKopf (void)
/**
Masken fuer LiefKopf anzeigen.
**/
{
          display_form (AufKopfWindow, &LiefKopfT, 0, 0);
          display_form (AufKopfWindow, &LiefKopfTD, 0, 0);
          if (LiefKopfE.mask[0].feldid)
          {
              display_form (AufKopfWindow, &LiefKopfE, 0, 0);
              display_form (AufKopfWindow, &LiefKopfED, 0, 0);
          }
}

void CloseLiefKopf (void)
/**
Masken fuer LiefKopf anzeigen.
**/
{
        CloseControls (&LiefKopfT);
        CloseControls (&LiefKopfE);
        CloseControls (&LiefKopfTD);
        CloseControls (&LiefKopfED);
}

void GetEditTextL (void)
/**
Text aus Editierfeld holen.
**/
{
        switch (lspos)
        {
                 case 0 :
                    GetWindowText (LiefKopfE.mask [0].feldid,
                                   LiefKopfE.mask [0].feld,
                                   LiefKopfE.mask[0].length);
                    break;
                 case 1 :
                    GetWindowText (LiefKopfE.mask [1].feldid,
                                   LiefKopfE.mask [1].feld,
                                   LiefKopfE.mask[1].length);
                    break;
                 case 2 :
                    GetWindowText (LiefKopfED.mask [0].feldid,
                                   LiefKopfED.mask [0].feld,
                                   LiefKopfED.mask[0].length);
                    break;
                 case 3 :
                    GetWindowText (LiefKopfED.mask [1].feldid,
                                   LiefKopfED.mask [1].feld,
                                   LiefKopfED.mask[1].length);
                    break;
                 case 4 :
                    GetWindowText (LiefKopfED.mask [2].feldid,
                                   LiefKopfED.mask [2].feld,
                                   LiefKopfED.mask[2].length);
                    break;
        }
        if (lspos == 0 && LiefKopfE.mask[0].after)
        {
                    (*LiefKopfE.mask[0].after) ();
        }
        else if (lspos == 1 && LiefKopfE.mask[1].after)
        {
                    (*LiefKopfE.mask[1].after) ();
        }
        else if (lspos &&  LiefKopfED.mask[lspos - 2].after)
        {
                    (*LiefKopfED.mask[lspos - 2].after) ();
        }
}


void LiefAfter (int pos)
{
        lspos = pos;
        if (lspos == 0 && LiefKopfE.mask[0].after)
        {
                    (*LiefKopfE.mask[0].after) ();
        }
        else if (lspos == 1 && LiefKopfE.mask[1].after)
        {
                    (*LiefKopfE.mask[1].after) ();
        }
        else if (lspos &&  LiefKopfED.mask[lspos - 2].after)
        {
                    (*LiefKopfED.mask[lspos - 2].after) ();
        }
}

void SetLiefFocus (int pos)
/**
Focus auf Eingabefelder AuftragKopfsetzen.
**/
{
	    setlspos = 0;
        switch (pos)
        {
               case 0 :
                      SetFocus (LiefKopfE.mask[0].feldid);
                      SendMessage (LiefKopfE.mask[0].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
                      break;
               case 1 :
                      SetFocus (LiefKopfE.mask[1].feldid);
                      SendMessage (LiefKopfE.mask[1].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
                      break;
               default :

				   SetFocus (LiefKopfED.mask[pos - 2].feldid);
                      SendMessage (LiefKopfED.mask[pos - 2].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
                      break;
        }
}

void LiefFocus (void)
{
        SetLiefFocus (lspos);
}


int IsLiefMessage (MSG *msg)
/**
HotKey Testen.
**/
{
         POINT mousepos;

         switch (msg->message)
         {
              case WM_KEYDOWN :
              {
                     switch (msg->wParam)
                     {
                            case VK_F5 :
                                    break_lief_enter = 1;
                                    return TRUE;
                            case VK_F6 :
                                    syskey = KEY6;
                                    testkeys ();
                                    return TRUE;
                            case VK_F12 :
								    if (TestOK () == FALSE) return TRUE;
                                    syskey = KEY12;
                                    WriteLS ();
                                    return TRUE;
                            case VK_DOWN :
                            case VK_RETURN :
                                    GetEditTextL ();
									if (setlspos == 0)
									{
                                           lspos ++;
									}
                                    if (lspos > lsend - 1) lspos = 0;
                                    SetLiefFocus (lspos);
                                    return TRUE;
                            case VK_UP :
                                    GetEditTextL ();
									if (setlspos == 0)
									{
                                            lspos --;
									}
                                    if (lspos < 0) lspos = lsend - 1;
                                    SetLiefFocus (lspos);
                                    return TRUE;
                            case VK_TAB :
                                    GetEditTextL ();
                                    if (GetKeyState (VK_SHIFT) < 0)
                                    {
									        if (setlspos == 0)
											{
                                                    lspos --;
											}
                                            if (lspos < 0) lspos = lsend - 1;
                                            SetLiefFocus (lspos);
                                     }
                                     else
                                     {
        									if (setlspos == 0)
											{
                                                     lspos ++;
											}
                                            if (lspos > lsend - 1) lspos = 0;
                                            SetLiefFocus (lspos);
                                     }
                                      return TRUE;
                            default :
                                    return FALSE;
                     }
                     break;
              }
              case WM_LBUTTONDOWN :
              {
				    setlspos = 0;
                    GetCursorPos (&mousepos);

                    if (MouseinWindow (LiefKopfE.mask[0].feldid, &mousepos))
                    {
/*
                          EnterNumBox (AufKopfWindow, "  Lieferschein-Nr  ",
                                       lief_nr, 16, LiefKopfE.mask[0].picture);
*/
                          NuBlock.EnterNumBox (AufKopfWindow, "  Lieferschein-Nr  ",
                                       lief_nr, 16, LiefKopfE.mask[0].picture, EntNuProc);
                          display_field (AufKopfWindow, &LiefKopfE.mask[0], 0, 0);
                          LiefAfter (0);
                          return TRUE;
                    }
                    else if (MouseinWindow (LiefKopfE.mask[1].feldid, &mousepos))
                    {
/*
                          EnterNumBox (AufKopfWindow, "  Lieferanten-Nr  ",
                                       lieferant, 16, LiefKopfE.mask[1].picture);
*/
                          NuBlock.SetChoise (doliefchoise, 0);
                          NuBlock.EnterNumBox (AufKopfWindow,"  Lieferanten-Nr  ",
                                               lieferant, 16, LiefKopfE.mask[1].picture,
	      									  EntNuProc);
                          display_field (AufKopfWindow, &LiefKopfE.mask[1], 0, 0);
                          LiefAfter (1);
                          return TRUE;
                    }
                    else if (MouseinWindow (LiefKopfED.mask[0].feldid, &mousepos))
                    {
                         NuBlock.EnterNumBox (AufKopfWindow,"  WE-Datum  ",
                                      lieferdatum, 11,
                                            LiefKopfED.mask[0].picture, EntNuProc);
                         display_field (AufKopfWindow, &LiefKopfED.mask[0], 0, 0);
                         LiefAfter (2);
//					     PostMessage (msg->hwnd, WM_KEYDOWN, VK_RETURN, 0l);
                         return TRUE;
                    }
                    else if (MouseinWindow (LiefKopfED.mask[1].feldid, &mousepos))
                    {
                         NuBlock.EnterNumBox (AufKopfWindow,"  Bel.Eing.Datum  ",
                                      beldatum, 11,
                                            LiefKopfED.mask[1].picture, EntNuProc);
                         display_field (AufKopfWindow, &LiefKopfED.mask[1], 0, 0);
                         LiefAfter (3);
//					     PostMessage (msg->hwnd, WM_KEYDOWN, VK_RETURN, 0l);
                         return TRUE;
                    }
             }
          }

          return FALSE;
}

void KonvLiefKopf (void)
/**
Spaltenposition von LiefKopfED anpassen.
**/
{
        static int KonvOK = 0;
        int z, s;
        int i;

        if (KonvOK) return;

        KonvOK = 1;
        for (i = 0; i < LiefKopfED.fieldanz; i ++)
        {
                    s = LiefKopfED.mask[i].pos [1];
                    KonvTextPos (LiefKopfE.font,
                                 LiefKopfED.font,
                                 &z, &s);
                    LiefKopfED.mask[i].pos [1] = s;
        }

        s = LiefKopfTD.mask[0].pos [1];
        KonvTextPos (LiefKopfE.font,
                    LiefKopfED.font,
                    &z, &s);
        LiefKopfTD.mask[0].pos [1] = s;
/*
        s = LiefKopfTD.mask[1].pos [1];
        KonvTextPos (LiefKopfE.font,
                    LiefKopfED.font,
                    &z, &s);
        LiefKopfTD.mask[1].pos [1] = s;

        s = LiefKopfTD.mask[2].pos [1];
        KonvTextPos (LiefKopfE.font,
                    LiefKopfED.font,
                    &z, &s);
        LiefKopfTD.mask[2].pos [1] = s;

        s = LiefKopfTD.mask[3].pos [1];
        KonvTextPos (LiefKopfE.font,
                    LiefKopfED.font,
                    &z, &s);
        LiefKopfTD.mask[3].pos [1] = s;
*/
}

void LiefInit (field *feld)
/**
Feld Initialisieren.
**/
{
        memset (feld->feld, ' ', feld->length);
        feld->feld[feld->length - 1] = (char) 0;
}

void InitLiefKopf (void)
/**
Auftrag-Kopf initialisieren.
**/
{
        int i;
        LiefKopfE.mask[0].feld[0] = (char) 0;
        LiefKopfE.mask[1].feld[0] = (char) 0;

        for (i = 0; i < LiefKopfED.fieldanz; i ++)
        {
                      LiefInit (&LiefKopfED.mask[i]);
        }
        LiefInit (&LiefKopfTD.mask[0]);
        LiefInit (&LiefKopfTD.mask[ls_stat_pos]);
}

void GenLief_rech_nr (short di)
{
	char cdi[2];
	sprintf(cdi,"%hd",di);
			sysdate (datum);
			memcpy (lief_nr, &datum[6], 4);
			memcpy (&lief_nr[4], &datum[3], 2);
			memcpy (&lief_nr[6], &datum[0], 2);
			if (di == 0)
			{
				lief_nr[8] = 0;
				return;
			}
			memcpy (&lief_nr[8], &cdi[0], 2);
			lief_nr[10] = 0;
			return;

}
void GenLS (void)
/**
Lieferscheinnummer generieren.
**/
{
        int dsqlstatus;
        short mdn;
        short fil;

        mdn = Mandant;
        fil = Filiale;
return;
        beginwork ();
        dsqlstatus = nvholid (mdn, fil, "ls");
        if (dsqlstatus == 100)
        {
              dsqlstatus = nvanmprf (mdn,
                                     fil,
                                     "ls",
                                     (long) 1,
                                     (long) 999999,
                                     (long) 10,
                                      "");

              if (dsqlstatus == 0)
              {
                          dsqlstatus = nvholid (mdn,
                                                fil,
                                                "ls");
              }
         }
         commitwork ();
         sprintf (lief_nr, "%ld", auto_nr.nr_nr);
         gen_lief_nr = auto_nr.nr_nr;
         sysdate (lieferdatum);
}



void LiefKopf (void)
/**
Auftragskopf bearbeiten.
**/
{
        RECT rect;
        MSG msg;
        int x, y, cx, cy;
//		char datum[12];

        if (KopfEnterAktiv == 1) return;
        if (LiefEnterAktiv) return;

        MainBuInactiv ();
        freestat = 0;
        CloseControls (&AufKopfT);
        CloseControls (&AufKopfE);
        CloseControls (&AufKopfTD);
        CloseControls (&AufKopfED);

        lspos = 0;
        RegisterKopfEnter ();
        GetWindowRect (LogoWindow, &rect);
        x = rect.left;
        y = rect.top;
        cx = rect.right - rect.left;
        cy = rect.bottom - rect.top;

//		lsk.auf_art = auf_art;
        KonvLiefKopf ();
        if (KopfEnterAktiv == 0)
		{
			InitLiefKopf ();
		}
		if (lsdefault && best_kopf.best_blg == 0)
		{
			GenLief_rech_nr(0);
			/*
			sysdate (datum);
			memcpy (lief_nr, &datum[6], 4);
			memcpy (&lief_nr[4], &datum[3], 2);
			memcpy (&lief_nr[6], &datum[0], 2);
			lief_nr[8] = 0;
			*/
		}
		sysdate (lieferdatum);
		sysdate (beldatum);
        GenLS ();

        beginwork ();
        LiefEnterAktiv = 1;
		strcpy (ls_stat,"0");
        if (AufKopfWindow == 0)
        {
                  AufKopfWindow  = CreateWindowEx (
                              0,
                              "AufKopf",
                              "",
                              WS_POPUP,
                              x, y,
                              cx, cy,
                              hMainWindow,
                              NULL,
                              hMainInst,
                              NULL);
        }

        if (AufKopfWindow == NULL)
        {
                 LiefEnterAktiv = 0;
                 return;
        }
		InitWeKopfBackground ();
        ShowWindow (AufKopfWindow, SW_SHOW);
        UpdateWindow (AufKopfWindow);
        create_enter_form (AufKopfWindow, &LiefKopfE, 0, 0);
        create_enter_form (AufKopfWindow, &LiefKopfED, 0, 0);
//        if (KopfEnterAktiv == 2)  create_enter_form (AufKopfWindow, &LiefKopfBest, 0, 0);
//        if (KopfEnterAktiv == 2)  create_enter_form (AufKopfWindow, &LiefKopfBestT, 0, 0);
        SetLiefFocus (lspos);
        break_lief_enter = 0;
        while (GetMessage (&msg, NULL, 0, 0))
        {
              if (IsLiefMessage (&msg));
              else
              {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
              }
              if (break_lief_enter) break;
        }
        CloseControls (&LiefKopfT);
        CloseControls (&LiefKopfE);
        CloseControls (&LiefKopfTD);
        CloseControls (&LiefKopfED);

//		if (KopfEnterAktiv == 2) CloseControls (&LiefKopfBest);
//		if (KopfEnterAktiv == 2) CloseControls (&LiefKopfBestT);
        DestroyWindow (AufKopfWindow);
        AufKopfWindow = NULL;
        LiefEnterAktiv = 0;
        MainBuActiv ();
		rollbackwork ();
        ActivateColButton (BackWindow3, &MainButton2, 2, -1, 1);
}


/* Auswahl ueber Geraete (sys_peri    */

#define MCI 6
#define GX  16

struct SYSPS
{
        char sys [9];
        char peri_typ [3];
        char txt [61];
        char peri_nam [21];
};


struct SYSPS sysarr [50], sysps;


static int sysidx;
static int sysanz = 0;

mfont SysFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                1,
                                NULL};

static field _sysform [] =
{
        sysps.sys,      12, 1, 0, 1, 0, "%8d", DISPLAYONLY, 0, 0, 0,
        sysps.peri_typ,  4, 1, 0,13, 0, "",    DISPLAYONLY, 0, 0, 0,
        sysps.txt,      20, 1, 0,17, 0, "",    DISPLAYONLY, 0, 0, 0
};

static form sysform = {3, 0, 0, _sysform, 0, 0, 0, 0, &SysFont};

static field _sysub [] =
{
        "Ger�te-Nr",     12, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        "Typ",            4, 1, 0,13, 0, "", DISPLAYONLY, 0, 0, 0,
        "Ger�t",         20, 1, 0,17, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form sysub = {3, 0, 0, _sysub, 0, 0, 0, 0, &SysFont};

static field _sysvl [] =
{
    "1",                  1,  0, 0, 12, 0, "", NORMAL, 0, 0, 0,
    "1",                  1,  0, 0, 16, 0, "", NORMAL, 0, 0, 0,
};

static form sysvl = {2, 0, 0, _sysvl, 0, 0, 0, 0, &SysFont};

int ShowSysPeri (HWND hWnd, char * ptabarr, int ptabanz, char * ptab,
                 int ptsize, form *ptform, form *ptvl, form *ptub, int laenge,int breite, HWND pWindow)
{
        RECT rect;
        RECT frmrect;
        int x, y, cx, cy;
        HWND shListBox;
        HWND slbox;
        struct LMENUE smenue;

        GetMenue (&smenue);
        MenTextSave (&smenue);
        shListBox = GethListBox ();
        slbox = Getlbox ();

        RegisterPt ();
        GetWindowRect (hMainWindow, &rect);
        GetFrmRectEx (ptform, ptform->font, &frmrect);

        x = rect.left + sysxplus;
        y = rect.top + 20;
        cx = frmrect.right + laenge;
//        cy = 185;
        cy = breite;

        set_fkt (endlist, 5);
        SetDblClck (IsPtClck, 0);
        PtListeAktiv = 1;
        SetAktivWindow (hWnd);
//        SetListEWindow (1);
        SetListFont (TRUE);
        spezfont (ptform->font);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_VISIBLE);
        PtWindow = OpenListWindowBoEx (x, y, cx, cy);
//        PtWindow = OpenListWindowEnEx (x, y, cx, cy);
        GetWindowRect (PtWindow, &rect);
        y = rect.bottom;
		cx = rect.right - rect.left;
        PtFussWindow = CreateWindow ("PtFuss",
                                    "",
                                    WS_POPUP | WS_VISIBLE | WS_DLGFRAME,
                                    x,
//                                    y + cy + 23,
                                    y,
                                    cx, 70,
                                    hWnd,
                                    NULL,
                                    hMainInst,
                                    NULL);
        if (ptvl) ElistVl (ptvl);
        if (ptub) ElistUb (ptub);
        EnableWindow (hMainWindow, FALSE);
        EnableWindow (pWindow, FALSE);
        ShowElist ((char *) ptabarr,
                  ptabanz,
                  (char *) ptab,
                  (int) ptsize,
                  ptform);
//        SetMouseLock (TRUE);
        SetMouseLockProc (MouseInPt);
        EnterElist (PtWindow, (char *) ptabarr,
                             ptabanz,
                             (char *) ptab,
                             (int) ptsize,
                             ptform);
        CloseControls (&PtButton);
        CloseEWindow (PtWindow);
        SetActiveWindow (hMainWindow);
        EnableWindow (hMainWindow, TRUE);
        EnableWindow (pWindow, TRUE);
        init_break_list ();
//        SetMouseLock (FALSE);
        SetMouseLockProc (NULL);
        DestroyWindow (PtFussWindow);
        PtListeAktiv = 0;
        SetMenue (&smenue);
        SethListBox (shListBox);
        Setlbox (slbox);
        SetAktivWindow (hWnd);
        return ptidx;
}


int AuswahlSysPeri (void)
/**
Ausahlfenster ueber Geraete anzeigen.
**/
{
        if (sysanz == 0)
        {
               dsqlstatus = sys_peri_class.lese_sys_peri ("order by sys");
               while (dsqlstatus == 0)
               {
                   if (sys_peri.peri_typ == MCI)
                   {
                       sprintf (sysarr[sysanz].sys, "%ld",
                                     sys_peri.sys);
                       sprintf (sysarr[sysanz].peri_typ,"%hd",
                                     sys_peri.peri_typ);
                       strcpy (sysarr[sysanz].txt, sys_peri.txt);
                       strcpy (sysarr[sysanz].peri_nam,
                                      sys_peri.peri_nam);
                       sysanz ++;
                       if (sysanz == 50) break;
                   }
                   dsqlstatus = sys_peri_class.lese_sys_peri ();
               }
        }
        if (sysanz == 0)
        {
               disp_messG ("Keine Ger�te gefunden", 1);
               return 0;
        }
        syskey = 0;
        sysidx = ShowSysPeri (LogoWindow, (char *) sysarr, sysanz,
                     (char *) &sysps,(int) sizeof (struct SYSPS),
                     &sysform, &sysvl, &sysub, 15,185,hMainWindow);
        if (syskey == KEY5 || syskey == KEYESC)
        {
                return 0;
        }
        if (atoi (sysarr[sysidx].peri_typ) == 6)
        {
                strcpy (waadir, sysarr[sysidx].peri_nam);
                clipped (waadir);
                sprintf (ergebnis, "%s\\ergebnis", waadir);
                WaaInit = 0;
                WaageInit ();
                sys_peri_class.lese_sys (atol (sysarr[sysidx].sys));
                la_zahl = sys_peri.la_zahl;
                AktKopf = 0;
        }
        else
        {
              auszeichner = atol (sysarr[sysidx].sys);
//            F�r Preisauszeichner noch nicht aktiv.
        }

        return sysidx;
}

// Wiegart ausw�hlen. Standard oder Entnahmeverwiegung

struct WGA
{
    char wiegart [30];
}; 

struct WGA wiegart;

struct WGA wgaarr[] = {"  Standardverwiegung",
                       "  Entnahmeverwiegung",
                       0};

static int wgaidx;
static int wgaanz = 2;

mfont WgaFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                1,
                                NULL};

static field _wgaform [] =
{
        wiegart.wiegart,  22, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form wgaform = {1, 0, 0, _wgaform, 0, 0, 0, 0, &WgaFont};

static field _wgaub [] =
{
        "Wiegart",       22, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form wgaub = {1, 0, 0, _wgaub, 0, 0, 0, 0, &WgaFont};

static field _wgavl [] =
{
    "1",                  1,  0, 0, 23, 0, "", NORMAL, 0, 0, 0,
};

static form wgavl = {0, 0, 0, _wgavl, 0, 0, 0, 0, &WgaFont};

int AuswahlWiegArt (void)
/**
Ausahlfenster ueber Geraete anzeigen.
**/
{

        Entnahmehand = FALSE;
        syskey = 0;
        wgaidx = ShowSysPeri (LogoWindow, (char *) wgaarr, wgaanz,
                     (char *) &wiegart,(int) sizeof (struct WGA),
                     &wgaform, &wgavl, &wgaub, 15,185,hMainWindow);
        if (syskey == KEY5 || syskey == KEYESC)
        {
                return 0;
        }
        WiegeTyp = wgaidx;
        if (WiegeTyp == STANDARD)
        {
                 ActivateColButton (BackWindow3, &MainButton2, 0, -1, 1);
        }
        else if (WiegeTyp == ENTNAHME)
        {
                 if (abfragejnG (NULL, "Handeingabe wie Entnahmewiegung ?", "J"))
                 {
                     Entnahmehand = TRUE;
                 }
                 ActivateColButton (BackWindow3, &MainButton2, 0, 1, 1);
        }
        return wgaidx;
}

struct WABT
{
    char abteilung [30];
}; 

struct WABT abteilung;

struct WABT abtarr[] = {"Abt. Fleisch",
                       "Abt. Handelsware ",
                       0};

static int abtidx;
static int abtanz = 2;

mfont AbtFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                1,
                                NULL};

static field _abtform [] =
{
        abteilung.abteilung,  22, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form abtform = {1, 0, 0, _abtform, 0, 0, 0, 0, &AbtFont};

static field _abtaub [] =
{
        "Abteilung",       22, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form abtaub = {1, 0, 0, _abtaub, 0, 0, 0, 0, &AbtFont};

static field _abtavl [] =
{
    "1",                  1,  0, 0, 23, 0, "", NORMAL, 0, 0, 0,
};

static form abtavl = {0, 0, 0, _abtavl, 0, 0, 0, 0, &AbtFont};

int AuswahlAbteilung (void)
/**
Ausahlfenster ueber Geraete anzeigen.
**/
{

        syskey = 0;
        abtidx = ShowSysPeri (LogoWindow, (char *) abtarr, abtanz,
                     (char *) &abteilung,(int) sizeof (struct WABT),
                     &abtform, &abtavl, &abtaub, 15,185,hMainWindow);
        if (syskey == KEY5 || syskey == KEYESC)
        {
                return 0;
        }
        return abtidx;
}



// Lager ausw�hlen. Schwein oder Rind

struct LAGER
{
    char lgr [10];
    char lgr_bz [25];
};


struct LAGER lgrarr[50], lager;

static int lgridx;
static int lgranz = 0;

mfont LgrFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                1,
                                NULL};

static field _lgrform [] =
{
        lager.lgr,  8, 1, 0, 1, 0, "%ld", DISPLAYONLY, 0, 0, 0,
        lager.lgr_bz,  24, 1, 0, 9, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form lgrform = {2, 0, 0, _lgrform, 0, 0, 0, 0, &LgrFont};

static field _lgrub [] =
{
        "Lager",       8, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        "Bezeichnung",       24, 1, 0, 9, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form lgrub = {2, 0, 0, _lgrub, 0, 0, 0, 0, &LgrFont};

static field _lgrvl [] =
{
    "1",                  1,  0, 0, 9, 0, "", NORMAL, 0, 0, 0,
};

static form lgrvl = {1, 0, 0, _lgrvl, 0, 0, 0, 0, &LgrFont};

int AuswahlLager (int lgrsav)
/**
Ausahlfenster ueber vorh. L�ger anzeigen.
**/
{
	  lgranz = 0 ; // immer wieder neu lesen, wg. Artikel-Lagerzuordnung
      while (TRUE)
	  {
        if (lgranz == 0)
        {
               dsqlstatus = lgr_class.lese_lgr ("order by lgr");
               while (dsqlstatus == 0)
               {
                       sprintf (lgrarr[lgranz].lgr, "%ld",
                                     lgr.lgr);
                       strcpy (lgrarr[lgranz].lgr_bz, lgr.lgr_bz);
                       lgranz ++;
                       if (lgranz == 50) break;
                   dsqlstatus = lgr_class.lese_lgr ();
               }
        }
        if (lgranz == 0)
        {
               disp_messG ("Kein Lager gefunden", 1);
               return lgrsav;
        }
        syskey = 0;
        lgridx = ShowSysPeri (LogoWindow, (char *) lgrarr, lgranz,
                     (char *) &lager,(int) sizeof (struct LAGER),
                     &lgrform, &lgrvl, &lgrub, 15,185,hMainWindow);
        if (syskey == KEY5 || syskey == KEYESC)
        {
                return lgrsav;
        }
		if (atoi (lgrarr[lgridx].lgr) < -1) 
		{
              disp_messG ("Diese Auswahl ist zur Zeit\n"
                          "nicht m�glich", 2);
			continue;
		}
		break;
	  }

			akt_lager = atoi (lgrarr[lgridx].lgr);
			lgr.lgr = (short) akt_lager;
	        if ( lgr_class.lese_lgr_bz (akt_lager) == 0) sprintf (Lagertxt,"Lager: %s",lgr.lgr_bz);
			 a_lgr.mdn = we_kopf.mdn;
			 a_lgr.lgr = lgr.lgr;
			 if (a_lgr_class.dbreadfirst() == 0)
			 {
				 sprintf (Buchartikeltxt, "");
				 if (a_lgr.buch_artikel > 0)
				 {
					 sprintf (Buchartikeltxt, "Art. %13.0lf",a_lgr.buch_artikel);
				 }
			 }
        return atoi (lgrarr[lgridx].lgr); 
}




int AuswahlLager (void)
/**
Ausahlfenster ueber L�ger anzeigen wenn zuordnung in a_lgr nicht eindeutig.
**/
{
	  lgranz = 0 ; // immer wieder neu lesen, wg. Artikel-Lagerzuordnung
	  if (aufps.lager > 0) 
	  {
			akt_lager = aufps.lager;
			lgr.lgr = (short) akt_lager;
	        if ( lgr_class.lese_lgr_bz (akt_lager) == 0) sprintf (Lagertxt,"Lager: %s",lgr.lgr_bz);
			 a_lgr.mdn = we_kopf.mdn;
			 a_lgr.lgr = lgr.lgr;
			 if (a_lgr_class.dbreadfirst() == 0)
			 {
				 sprintf (Buchartikeltxt, "");
				 if (a_lgr.buch_artikel > 0)
				 {
					 sprintf (Buchartikeltxt, "Art. %13.0lf",a_lgr.buch_artikel);
				 }
			 }

		  return akt_lager;
	  }
      while (TRUE)
	  {
        if (lgranz == 0)
        {
               dsqlstatus = lgr_class.lese_lgr ("order by lgr");
               while (dsqlstatus == 0)
               {
                       sprintf (lgrarr[lgranz].lgr, "%ld",
                                     lgr.lgr);
                       strcpy (lgrarr[lgranz].lgr_bz, lgr.lgr_bz);
                       lgranz ++;
                       if (lgranz == 50) break;
                   dsqlstatus = lgr_class.lese_lgr ();
               }
        }
		lgridx = 0;
        if (lgranz == 0)
        {
               disp_messG ("Kein Lager gefunden", 1);
               return 0;
        }
		if (lgranz > 1)
		{
			syskey = 0;
			lgridx = ShowSysPeri (LogoWindow, (char *) lgrarr, lgranz,
						(char *) &lager,(int) sizeof (struct LAGER),
						&lgrform, &lgrvl, &lgrub, 15,185,hMainWindow);
			if (syskey == KEY5 || syskey == KEYESC)
			{
					continue;
			}
			if (atoi (lgrarr[lgridx].lgr) < -1) 
			{
				disp_messG ("Diese Auswahl ist zur Zeit\n"
							"nicht m�glich", 2);
				continue;
			}
		}
		break;
	  }


	  akt_lager = atoi (lgrarr[lgridx].lgr); 
	  lgr.lgr = (short) akt_lager;
        if ( lgr_class.lese_lgr_bz (akt_lager) == 0) sprintf (Lagertxt,"Lager: %s",lgr.lgr_bz);
//         memcpy (&a_lgr, &a_lgr_null, sizeof (struct A_LGR));
		 a_lgr.mdn = we_kopf.mdn;
		 a_lgr.lgr = lgr.lgr;
		 a_lgr_class.dbreadfirst();

       return akt_lager ;
}


int AuswahlALager (int lgrsav, bool lAuswahlZwang)
{
      while (TRUE)
	  {
	    a_lgr.a = ratod(aufps.a);  
  	    lgranz = 0 ; // immer wieder neu lesen, wg. Artikel-Lagerzuordnung
        if (lgranz == 0)
        {
               dsqlstatus = lgr_class.lese_lgr ("order by lgr");
               while (dsqlstatus == 0)
               {
                       sprintf (lgrarr[lgranz].lgr, "%ld",
                                     lgr.lgr);
                       strcpy (lgrarr[lgranz].lgr_bz, lgr.lgr_bz);
                       lgranz ++;
                       if (lgranz == 50) break;
                   dsqlstatus = lgr_class.lese_lgr ();
               }
        }
		if (lgranz == 1 && lAuswahlZwang == FALSE)
		{
				akt_lager = atoi (lgrarr[0].lgr);
				return akt_lager;
		}
		/***
        if (lgranz == 0)
        {
               disp_messG ("Kein Lager gefunden", 1);
               return lgrsav;
        }
		***/
        syskey = 0;
		 sprintf(Zuruecktext,"Zuordn.�ndern");
        lgridx = ShowSysPeri (LogoWindow, (char *) lgrarr, lgranz,
                     (char *) &lager,(int) sizeof (struct LAGER),
                     &lgrform, &lgrvl, &lgrub,15,185,WiegWindow);
		 sprintf(Zuruecktext,"Zurueck F5");
        if (syskey == KEY5 || syskey == KEYESC)
        {
			//  Hier jetzt die Gesamtauswahl, 
		      lgranz = 0 ; // immer wieder neu lesen, wg. Artikel-Lagerzuordnung
			   a_lgr.a = 0.0;
			   syskey = 0;
               dsqlstatus = lgr_class.lese_lgr ("order by lgr");
               while (dsqlstatus == 0)
               {
                       sprintf (lgrarr[lgranz].lgr, "%ld",
                                     lgr.lgr);
                       strcpy (lgrarr[lgranz].lgr_bz, lgr.lgr_bz);
                       lgranz ++;
                       if (lgranz == 50) break;
                   dsqlstatus = lgr_class.lese_lgr ();
               }
		        lgridx = ShowSysPeri (LogoWindow, (char *) lgrarr, lgranz,
                     (char *) &lager,(int) sizeof (struct LAGER),
                     &lgrform, &lgrvl, &lgrub,15,185,WiegWindow);
			    if (syskey == KEY5 || syskey == KEYESC)
				{
					    break;
				}
//                AktivWindow = WiegWindow;
//				disp_messG ("Die Artikel-Lagerzuordnung wird aktualisiert\n"
//                          "", 2);
			a_lgr.lgr = atoi (lgrarr[lgridx].lgr);
		    a_lgr.a = ratod(aufps.a);  
			a_lgr.lgr_pla_kz = 0;
			a_lgr.mdn = Mandant;
	        EnableWindow (hMainWindow, FALSE);
		    EnableWindow (WiegWindow, FALSE);
			if (a_lgr_class.dbreadfirst() == 100)
			{
	          if (abfragejnG (NULL,
                          "Lager in die Artikel-Lagerzuordnung hinzuf�gen ?.\n"
                          "", "J"))
			  {
					a_lgr_class.dbupdate();
			  }
			}
			else 
			{
	          if (abfragejnG (NULL,
                          "Lager ist schon vorhanden !\n"
                          "Soll das Lager aus der Artikel-Lagerzuordnung entfernt werden ?", "J"))
				a_lgr_class.dbdelete();
			}
	        EnableWindow (hMainWindow, TRUE);
		    EnableWindow (WiegWindow, TRUE);
			//hier jetzt mit einbuchen in a_lgr oder l�schen aus a_lgr
			continue;
		}
		if (atoi (lgrarr[lgridx].lgr) < -1) 
		{
              disp_messG ("Diese Auswahl ist zur Zeit\n"
                          "nicht m�glich", 2);
			continue;
		}
		break;
	  }
		   if (syskey == KEY5) 
		   {
			   return (lgrsav);
		   }
		   else
		   {
				akt_lager = atoi (lgrarr[lgridx].lgr);
		   }
			lgr.lgr = (short) akt_lager;
	        if ( lgr_class.lese_lgr_bz (akt_lager) == 0) sprintf (Lagertxt,"%s",lgr.lgr_bz);
			 a_lgr.mdn = Mandant;
			 a_lgr.lgr = lgr.lgr;
			 if (a_lgr_class.dbreadfirst() == 0)
			 {
				 sprintf (Buchartikeltxt, "");
				 if (a_lgr.buch_artikel > 0)
				 {
					 sprintf (Buchartikeltxt, "Art. %13.0lf",a_lgr.buch_artikel);
				 }
			 }

      return atoi (lgrarr[lgridx].lgr); 
}



// Auswahl �ber Wiegungen aus wiepro


mfont WiepFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                                 RGB (255, 255, 255),
                                 1,
                                 NULL};

static field _wiepform [] =
{
        wiep.a,      14, 1, 0,  1, 0, "%13.0f", DISPLAYONLY, 0, 0, 0,
        wiep.a_bz1,  20, 1, 0, 17, 0, "",       DISPLAYONLY, 0, 0, 0,
        wiep.brutto, 11, 1, 0, 39, 0, "%7.3",   DISPLAYONLY, 0, 0, 0,
        wiep.zeit,    6, 1, 0, 51, 0, "",       DISPLAYONLY, 0, 0, 0,
};

static form wiepform = {4, 0, 0, _wiepform, 0, 0, 0, 0, &WiepFont};

static field _wiepub [] =
{
        "Artikel",     15, 1, 0,  1, 0, "",   DISPLAYONLY, 0, 0, 0,
        "Bezeichnung", 22, 1, 0, 16, 0, "",   DISPLAYONLY, 0, 0, 0,
        "Gewicht",     12, 1, 0, 38, 0, "",   DISPLAYONLY, 0, 0, 0,
        "Zeit",         7, 1, 0, 50, 0, "",   DISPLAYONLY, 0, 0, 0,
};

static form wiepub = {4, 0, 0, _wiepub, 0, 0, 0, 0, &WiepFont};

static field _wiepvl [] =
{
    "1",                  1,  0, 0, 16, 0, "", NORMAL, 0, 0, 0,
    "1",                  1,  0, 0, 38, 0, "", NORMAL, 0, 0, 0,
    "1",                  1,  0, 0, 50, 0, "", NORMAL, 0, 0, 0,
};

static form wiepvl = {3, 0, 0, _wiepvl, 0, 0, 0, 0, &WiepFont};

int AuswahlWiePro (void)
/**
Ausahlfenster ueber Geraete anzeigen.
**/
{
        int cursor;
        int idx;

        sql_in ((char *)  &akt_mdn, 1, 0);
        sql_in ((char *)  we_kopf.lief_rech_nr , 0, 17);
        sql_in ((char *)  &we_kopf.lief_s , 2, 0);
        sql_out ((char *) wiep.a, 0, 14);
        sql_out ((char *) wiep.a_bz1, 0, 25);
        sql_out ((char *) wiep.brutto, 0, 11);
        sql_out ((char *) wiep.dat, 0, 11);
        sql_out ((char *) wiep.zeit, 0, 9);
        cursor = prepare_sql ("select we_wiepro.a, a_bas.a_bz1, me, dat, zeit "
                                      "from we_wiepro,a_bas "
                                      "where a_bas.a = we_wiepro.a "
                                      "and we_wiepro.mdn = ? "
                                      "and we_wiepro.lief_rech_nr = ? "
                                      "and we_wiepro.lief_s = ? "
                                      "order by dat desc, zeit desc");

        idx = 0;
        while (fetch_sql (cursor) == 0)
        {
                memcpy (&wieparr[idx], &wiep, sizeof (wiep));
                idx ++;
                if (idx == 20)
                {
                    break;
                }
        }
        close_sql (cursor);
        wiepanz = idx;

        sysxplus = 0;
        syskey = 0;
        wiepidx = ShowSysPeri (WiegWindow, (char *) wieparr, wiepanz,
                     (char *) &wiep,(int) sizeof (struct WIEP),
                     &wiepform, &wiepvl, &wiepub, 15,185,hMainWindow);
        sysxplus = 50;
        if (syskey == KEY5 || syskey == KEYESC)
        {
                return -1;
        }
        return wiepidx;
}
















/* Auswahl ueber Wiegungen                    */


struct WIEGT
{
        char wieg [5];
        char menge [12];
};


struct WIEGT wiegarr [1000], wiegt;


static int wiegidx;
static int wieganz = 0;

mfont WiegFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                1,
                                NULL};

static field _wiegform [] =
{
        wiegt.wieg,      9, 1, 0, 1, 0, "%7d",   DISPLAYONLY, 0, 0, 0,
        wiegt.menge,    13, 1, 0,10, 0, ":9.3f", DISPLAYONLY, 0, 0, 0,
};

static form wiegform = {2, 0, 0, _wiegform, 0, 0, 0, 0, &WiegFont};

static field _wiegub [] =
{
        "Wiegung",        9, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        "Menge",         13, 1, 0,10, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form wiegub = {2, 0, 0, _wiegub, 0, 0, 0, 0, &WiegFont};

static field _wiegvl [] =
{
    "1",                  1,  0, 0,  9, 0, "", NORMAL, 0, 0, 0,
    "1",                  1,  0, 0, 22, 0, "", NORMAL, 0, 0, 0,
};

static form wiegvl = {2, 0, 0, _wiegvl, 0, 0, 0, 0, &WiegFont};

int ShowWieg (HWND hWnd, char * ptabarr, int ptabanz, char * ptab,
                 int ptsize, form *ptform, form *ptvl, form *ptub)
{
        RECT rect;
        RECT frmrect;
        int x, y, cx, cy;
        HWND shListBox;
        HWND slbox;
        struct LMENUE smenue;

        GetMenue (&smenue);
        MenTextSave (&smenue);
        shListBox = GethListBox ();
        slbox = Getlbox ();

        RegisterPt ();
        GetWindowRect (hMainWindow, &rect);
        GetFrmRectEx (ptform, ptform->font, &frmrect);

        x = rect.left + 50;
        y = rect.top + 20;
        cx = frmrect.right + 15;
        cy = 185;

        set_fkt (endlist, 5);
        SetDblClck (IsPtClck, 0);
        PtListeAktiv = 1;
        SetAktivWindow (hWnd);
//        SetListEWindow (1);
        SetListFont (TRUE);
        spezfont (ptform->font);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_VISIBLE);
//        PtWindow = OpenListWindowEnEx (x, y, cx, cy);
        PtWindow = OpenListWindowBoEx (x, y, cx, cy);
//        PtWindow = OpenListWindowEnEx (x, y, cx, cy);
        GetWindowRect (PtWindow, &rect);
        y = rect.bottom;
		cx = rect.right - rect.left;
        PtFussWindow = CreateWindow ("PtFuss",
                                    "",
                                    WS_POPUP | WS_VISIBLE | WS_DLGFRAME,
                                    x,
//                                    y + cy + 23,
                                    y,
                                    cx, 70,
                                    hWnd,
                                    NULL,
                                    hMainInst,
                                    NULL);
        if (ptvl) ElistVl (ptvl);
        if (ptub) ElistUb (ptub);
        EnableWindow (hMainWindow, FALSE);
        ShowElist ((char *) ptabarr,
                  ptabanz,
                  (char *) ptab,
                  (int) ptsize,
                  ptform);
//        SetMouseLock (TRUE);
        SetMouseLockProc (MouseInPt);
        EnterElist (PtWindow, (char *) ptabarr,
                             ptabanz,
                             (char *) ptab,
                             (int) ptsize,
                             ptform);
        CloseControls (&PtButton);
        CloseEWindow (PtWindow);
        SetActiveWindow (hMainWindow);
        EnableWindow (hMainWindow, TRUE);
        init_break_list ();
//        SetMouseLock (FALSE);
        SetMouseLockProc (NULL);
        DestroyWindow (PtFussWindow);
        PtListeAktiv = 0;
        SetMenue (&smenue);
        SethListBox (shListBox);
        Setlbox (slbox);
        SetAktivWindow (hWnd);
        return ptidx;
}

int AuswahlWieg (void)
/**
Ausahlfenster ueber Wiegungen anzeigen
**/
{
        int i;
        int j;

        for (i = AktWiegPos - 1, j = 0; i >= 0; i --, j ++)
        {
            sprintf (wiegarr[j].wieg, "%d", i + 1);
            sprintf (wiegarr[j].menge, "%.3lf", AktPrWieg[i]);
        }
        wieganz = j;

        if (wieganz == 0)
        {
               return 0;
        }
        syskey = 0;
        wiegidx = ShowWieg (WiegWindow, (char *) wiegarr, wieganz,
                     (char *) &wiegt,(int) sizeof (struct WIEGT),
                     &wiegform, &wiegvl, &wiegub);
        if (syskey == KEY5 || syskey == KEYESC)
        {
                return 0;
        }
        StornoWieg = ratod (wiegarr[wiegidx].menge);
        return TRUE;
}


/* Auswahl ueber Wiegekoepfe                    */


struct WKOPF
{
        char nr [10];
        char akt [10];
};


struct WKOPF wkopfarr [20], wkopf;


static int kopfidx;
static int kopfanz = 0;

mfont KopfFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                1,
                                NULL};

static field _wkopfform [] =
{
        wkopf.nr,     11, 1, 0, 1, 0, "%4d",   DISPLAYONLY, 0, 0, 0,
        wkopf.akt,    13, 1, 0,12, 0, "",      DISPLAYONLY, 0, 0, 0,
};

static form wkopfform = {2, 0, 0, _wkopfform, 0, 0, 0, 0, &KopfFont};

static field _wkopfub [] =
{
        "Wiegekopf",       11, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        "  aktiv",         13, 1, 0,12, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form wkopfub = {2, 0, 0, _wkopfub, 0, 0, 0, 0, &KopfFont};

static field _wkopfvl [] =
{
    "1",                  1,  0, 0, 12, 0, "", NORMAL, 0, 0, 0,
    "1",                  1,  0, 0, 24, 0, "", NORMAL, 0, 0, 0,
};

static form wkopfvl = {2, 0, 0, _wkopfvl, 0, 0, 0, 0, &KopfFont};

int ShowWKopf (HWND hWnd, char * ptabarr, int ptabanz, char * ptab,
                 int ptsize, form *ptform, form *ptvl, form *ptub)
{
        RECT rect;
        RECT frmrect;
        int x, y, cx, cy;
        HWND shListBox;
        HWND slbox;
        struct LMENUE smenue;

        GetMenue (&smenue);
        MenTextSave (&smenue);
        shListBox = GethListBox ();
        slbox = Getlbox ();

        RegisterPt ();
        GetWindowRect (hMainWindow, &rect);
        GetFrmRectEx (ptform, ptform->font, &frmrect);

        x = rect.left + 50;
        y = rect.top + 20;
        cx = frmrect.right + 15;
		if (PtY > -1)
		{
			cy = PtY;
			PtY = -1;
		}
		else
		{
            cy = 185;
		}

        set_fkt (endlist, 5);
        SetDblClck (IsPtClck, 0);
        PtListeAktiv = 1;
        SetAktivWindow (hWnd);
        SetListFont (TRUE);
        spezfont (ptform->font);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_VISIBLE);
        PtWindow = OpenListWindowBoEx (x, y, cx, cy);
        GetWindowRect (PtWindow, &rect);
        y = rect.bottom;
		cx = rect.right - rect.left;
        PtFussWindow = CreateWindow ("PtFuss",
                                    "",
                                    WS_POPUP | WS_VISIBLE | WS_DLGFRAME,
                                    x,
                                    y,
                                    cx, 70,
                                    hWnd,
                                    NULL,
                                    hMainInst,
                                    NULL);
        if (ptvl) ElistVl (ptvl);
        if (ptub) ElistUb (ptub);
        EnableWindow (hMainWindow, FALSE);
        ShowElist ((char *) ptabarr,
                  ptabanz,
                  (char *) ptab,
                  (int) ptsize,
                  ptform);
//        SetMouseLock (TRUE);
//        SetMouseLockProc (MouseInPt);
        EnterElist (PtWindow, (char *) ptabarr,
                             ptabanz,
                             (char *) ptab,
                             (int) ptsize,
                             ptform);
        CloseControls (&PtButton);
        CloseEWindow (PtWindow);
        SetActiveWindow (hMainWindow);
        EnableWindow (hMainWindow, TRUE);
        init_break_list ();
//        SetMouseLock (FALSE);
//        SetMouseLockProc (NULL);
        DestroyWindow (PtFussWindow);
        PtListeAktiv = 0;
        SetMenue (&smenue);
        SethListBox (shListBox);
        Setlbox (slbox);
        SetAktivWindow (hWnd);
        return ptidx;
}


int AuswahlKopf (void)
/**
Ausahlfenster ueber Geraete anzeigen.
**/
{
        int i;

        for (i = 0; i < la_zahl; i ++)
        {
                       sprintf (wkopfarr[i].nr, "%d", i + 1);
                       if (i == AktKopf)
                       {
                               sprintf (wkopfarr[i].akt, "   * ");
                       }
                       else
                       {
                               sprintf (wkopfarr[i].akt, "     ");
                       }
        }
        kopfanz = i;
        if (kopfanz == 0)
        {
               return 0;
        }
        syskey = 0;
        kopfidx = ShowWKopf (LogoWindow, (char *) wkopfarr, kopfanz,
                     (char *) &wkopf,(int) sizeof (struct WKOPF),
                     &wkopfform, &wkopfvl, &wkopfub);
        if (syskey == KEY5 || syskey == KEYESC)
        {
                return 0;
        }
        return kopfidx;
}


/* Auswahl bei leeren Positionen   */

mfont KtFont = {"Courier New", 150, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                1,
                                NULL};

static char ktext [61];
static char ktextarr [10][61];

static int ktidx;

static field _ktform [] =
{
        ktext,     50, 1, 0, 1, 0, "",   DISPLAYONLY, 0, 0, 0,
};

static form ktform = {1, 0, 0, _ktform, 0, 0, 0, 0, &KtFont};

static field _ktub [] =
{
        "Auswahl",     50, 1, 0, 1, 0, "",   DISPLAYONLY, 0, 0, 0,
};

static form ktub = {1, 0, 0, _ktub, 0, 0, 0, 0, &KtFont};


BOOL LeerPosAuswahl ()
{
        static BOOL CpOK = FALSE;

        if (! CpOK)
        {
            strcpy (ktextarr[0],
                "Unbearbeitete Positionen ignorieren");
            strcpy (ktextarr[1],
                "Unbearbeitete Positionen l�schen");
            strcpy (ktextarr[2],
                "Ist-Menge = Soll-Menge");
        }
        SetHoLines (0);
        syskey = 0;
        if (eWindow)
        {
                EnableWindow (eWindow, FALSE);
                EnableWindow (eFussWindow, FALSE);
                EnableWindow (eKopfWindow, FALSE);
        }
        ktidx = ShowWKopf (eWindow, (char *) ktextarr, 3,
                     (char *) &ktext, 61,
                     &ktform, NULL, &ktub);
        if (eWindow)
        {
                EnableWindow (eWindow, TRUE);
                EnableWindow (eFussWindow, TRUE);
                EnableWindow (eKopfWindow, TRUE);
        }

        SetHoLines (1);
        if (syskey == KEY5 || syskey == KEYESC)
        {
                return FALSE;
        }
        switch (ktidx)
        {
                case 0 :
                    SetAllPosStat ();
                    return TRUE;
                case 1 :
                    DelLeerPos ();
                    return TRUE;
                case 2:
                    SollToIst ();
                    return TRUE;
        }
        return TRUE;
}



BOOL PosLeer (void)
/**
Reaktion auf nicht abgeschlossene Positionen.
**/
{
          EnableWindow (hMainWindow, FALSE);
          EnableWindow (eWindow, FALSE);
          EnableWindow (eFussWindow, FALSE);
          EnableWindow (eKopfWindow, FALSE);
          if (abfragejnG (eWindow,
                          "Es sind nicht bearbeitete Positionen vorhanden.\n"
                          "Abbrechen ?", "J"))
          {
              EnableWindow (hMainWindow, TRUE);
              EnableWindow (eWindow, TRUE);
              EnableWindow (eFussWindow, TRUE);
              EnableWindow (eKopfWindow, TRUE);
              return FALSE;
          }
          EnableWindow (hMainWindow, TRUE);
          EnableWindow (eWindow, TRUE);
          EnableWindow (eFussWindow, TRUE);
          EnableWindow (eKopfWindow, TRUE);
          return LeerPosAuswahl ();
}


BOOL TestPosis (void)
/**
Einzelne Positionen testen.
**/
{
          int i;

          for (i = 0; i < aufpanz_upd; i ++)
          {
              if (atoi (aufparr[i].s) < 3)
              {
                  return PosLeer ();
              }
          }
          return TRUE;
}


BOOL TestPosStatus (void)
/**
Test, ob der Lieferschein auf komplett gesetzt werden darf.
**/
{
          if (NewPosi () == FALSE)
          {
              disp_messG ("Der Lieferschein wird noch von einem\n"
                          "anderen Benutzer bearbeitet", 2);
              return FALSE;
          }

          if (TestPosis () == FALSE)
          {
              return FALSE;
          }
          return TRUE;
}

int LeerGutExist (void)
/**
Test, ob Leergutartikel erfasst wurden.
**/
{
          int i;
          int lganz;

          lganz = 0;
          for (i = 0; i < aufpanz_upd; i ++)
          {
              if (lese_a_bas (ratod (aufparr[i].a)) != 0)
              {
                  continue;
              }
              if (_a_bas.a_typ == 11)
              {
                  lganz += atoi (aufparr[i].lief_me);
              }
          }
          return lganz;
}

BOOL PrintAufkleber (int lganz)
/**
Druck fuer Aufkleber starten.
**/
{
/*
          char buffer [256];
          DWORD ret;

          sprintf (buffer, "%d", lganz);

          EnterCalcBox (eWindow," Anzahl Aufkleber", buffer, 9,
                                   "%4d");

/?
          EnterNumBox (eWindow,
              " Wieviele Aufkleber sollen ausgedruckt werden", buffer, 9,
                                   "%4d");
?/
          lganz = atoi (buffer);
          if (lganz == 0) return TRUE;

          if (strcmp (sys_ben.pers_nam, " ") <= 0)
          {
             strcpy (sys_ben.pers_nam, "fit");
          }
          sprintf (buffer, "rswrun 53800 0 %s %hd %hd %ld %d",
                                 clipped (sys_ben.pers_nam),
                                 lsk.mdn, lsk.fil, lsk.kun, lganz);
          SetAktivWindow (eWindow);

          ret = ProcExec (buffer, SW_SHOWMINIMIZED, -1, 0, -1, 0);
          SetAktivWindow (eWindow);
          if (ret == 0)
          {
              disp_messG ("Fehler beim Start des Etikettendrucks",2);
          }
          else
          {
              disp_messG ("Etikettendruck gestartet", 2);
          }
*/
          return TRUE;
}


BOOL LsLeerGut (void)
/**
Leergut testen.
**/
{
         int lganz;

         if (ls_leer_zwang == 0) return TRUE;
         if (lganz = LeerGutExist ())
         {
             return PrintAufkleber (lganz);
         }
         if (abfragejnG (eWindow, "Es wurde kein Leergutartikel erfasst\n"
                                  "Leergut erfassen", "J"))
         {
// Durch PostMessage wuerde das Fenster zum Erfassen von Artikeln
// geoeffnet.
//             PostMessage (eWindow, WM_KEYDOWN, VK_F6, 0l);
             return FALSE;
         }
         return TRUE;
}


/**
Leergut als Kopf oder Fusstext eingeben.
**/

#define LEERMAX 100

form *fAuswahlEinh;

struct LP
{
	double lart;
	int    lanz;
} LeerPosArt [LEERMAX];


static BOOL BreakLeerEnter = FALSE;
static HWND lPosWindow;
static HWND hWndLeer;

static char KopfText[61];
static char KopfTextTab[LEERMAX][61];
static char KopfTextFTab[10][61];
static int kopftextfpos;
static int kopftextfscroll;

static int kopftextpos = 0;
static int kopftextanz = 0;
static int ktpos = 0;
static int ktend = 0;
static TEXTMETRIC tmLP;
static BOOL CrCaret = FALSE;
static char *ztab[] = {" = ", " + "};
static int  zpos = 0;
static BOOL NumIn = FALSE;

static char *meeinhLP[100];
static char *meeinhLPK[100];
static double leer_art [100];

static int meeinhpos  = 0;
static int palettepos = 0;
static int ptanz = 0;
static int ptscroll = 0;
static int ptstart  = 0;
static int ptend    = 0;
static int LeerScrollDown (void);
static int LeerScrollUp (void);

mfont PosLeerFontE = {"Courier New", 200, 0, 1,
                                     RGB (0, 0, 0),
                                     RGB (255, 255, 255),
                                     1,
                                     NULL};

COLORREF LeerBuColor = RGB (0, 255, 255);
COLORREF LeerBuColorPf = RGB (0, 0, 255);


static field _PosLeerE[] = {
  KopfTextFTab[0],          61, 0, 0, 1, 0, "", DISPLAYONLY,        0, 0, 0,
  KopfTextFTab[1],          61, 0, 1, 1, 0, "", DISPLAYONLY,        0, 0, 0,
  KopfTextFTab[2],          61, 0, 2, 1, 0, "", DISPLAYONLY,        0, 0, 0,
  KopfTextFTab[3],          61, 0, 3, 1, 0, "", DISPLAYONLY,        0, 0, 0};

form PosLeerE = {4, 0, 0, _PosLeerE, 0, 0, 0, 0, &PosLeerFontE};

int StopPosLeer (void)
{
	     BreakLeerEnter = TRUE;
		 return (0);
  }


LONG FAR PASCAL PosLeerProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        switch(msg)
        {

             case WM_PAINT :
				    if (hWnd == hWndLeer)
					{
				           display_form (hWnd, &PosLeerE, 0, 0);
					}
                    break;

             case WM_KEYDOWN :
				    if (wParam == VK_F5)
					{
						syskey = KEY5;
						StopPosLeer ();
					}
					break;
             case WM_COMMAND :
                    if (LOWORD (wParam) == KEY5 ||
                        LOWORD (wParam) == VK_F5)
                     {
                            syskey = KEY5;
                            SendKey (VK_F5);
                            break;
                    }
                    if (LOWORD (wParam) == KEYCR)
                    {
                            syskey = KEYCR;
                            SendKey (VK_RETURN);
                            break;
                    }
                    if (LOWORD (wParam) == KEY12)
                    {
                            syskey = KEY12;
                            SendKey (VK_F12);
                            break;
                    }
                    if (LOWORD (wParam) == KEYUP)
                    {
                            syskey = KEYUP;
							if (currentfield > ptstart)
							{
								currentfield --;
							    SetFocus (fAuswahlEinh->mask[currentfield].feldid);
							}
							else
							{
								LeerScrollDown ();
							}
							meeinhpos = ptscroll + currentfield - 1;
                            break;
                    }
                    if (LOWORD (wParam) == KEYDOWN)
                    {
                            syskey = KEYUP;
							if (currentfield < ptend)
							{
								currentfield ++;
							    SetFocus (fAuswahlEinh->mask[currentfield].feldid);
							}
							else
							{
								LeerScrollUp ();
							}
							meeinhpos = ptscroll + currentfield - 1;
                            break;
					}

        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

static void KonvFormParams (void)
/**
Laenge des Eingabefeldes anpassen.
**/
{
	     static BOOL paramsOK = FALSE;
		 HDC hdc;
		 HFONT hFont, oldfont;
		 int i;

		 if (paramsOK) return;

		 paramsOK = TRUE;
         spezfont (&PosLeerFontE);
         hFont = SetWindowFont (hMainWindow);
         hdc = GetDC (hMainWindow);
         oldfont = SelectObject (hdc, hFont);
         GetTextMetrics (hdc, &tmLP);
         DelFont (SelectObject (hdc, oldfont));
         ReleaseDC (hMainWindow, hdc);

		 for (i = 0; i < PosLeerE.fieldanz; i ++)
		 {
			 PosLeerE.mask[i].length = PosLeerE.mask[i].length * (short) tmLP.tmAveCharWidth;
			 if (PosLeerE.mask[i].rows == 0)
			 {
				 PosLeerE.mask[i].rows = (int) (double) ((double) tmLP.tmHeight * 1.3);
			 }
			 else
			 {
				 PosLeerE.mask[i].rows = PosLeerE.mask[i].rows * (short) tmLP.tmHeight;
			 }

			 if (PosLeerE.mask[i].pos[0] != -1)
			 {
				 PosLeerE.mask[i].pos[0] = PosLeerE.mask[i].pos[0] * (short) tmLP.tmHeight;
			 }

			 if (PosLeerE.mask[i].pos[1] != -1)
			 {
				 PosLeerE.mask[i].pos[1] = PosLeerE.mask[i].pos[1] * (short) tmLP.tmAveCharWidth;
			 }
		 }
}

void SetLPCaret (void)
{
	int x,y;
	int CaretH;
	BOOL cret;

	DestroyCaret ();

    CaretH = (int) (double) ((double) tmLP.tmHeight * 1.1);
    SetFocus (PosLeerE.mask[kopftextfpos].feldid);
    cret = CreateCaret (PosLeerE.mask[kopftextfpos].feldid, NULL, 0, CaretH);
    CrCaret = TRUE;

	x = ktpos * tmLP.tmAveCharWidth;
	y = 0;
	cret = ShowCaret (PosLeerE.mask[kopftextfpos].feldid);
	cret = SetCaretPos (x,y);
	ShowCaret (PosLeerE.mask[kopftextfpos].feldid);
}

static void DispWindowText (HWND hWnd, char *text)
/**
Text in Fenster anzeigen.
**/
{
	 InvalidateRect (hWnd, NULL, TRUE);
	 UpdateWindow (hWnd);
}

static void ToKopfFtab (void)
/**
Kopfdelder aus Array in FormFelder uebertragen.
**/
{
      int i, j;

	  while (kopftextfscroll < 0) kopftextfscroll ++;

      for (i = 0, j = kopftextfscroll; i < PosLeerE.fieldanz; i ++, j ++)
	  {
		  strcpy (KopfTextFTab[i], KopfTextTab[j]);
	  }
	  display_form (hWndLeer, &PosLeerE, 0, 0);
}



static void EinhToStr (void)
/**
Einheit an String anhaengen.
**/
{
	char Einheit[20];
	char *MeH;

	if (NumIn == FALSE) return;


    strcpy (Einheit, meeinhLPK[meeinhpos]);
	MeH = strtok (Einheit, " ");
	strcat (KopfTextFTab[kopftextfpos], " ");
    strcat (KopfTextFTab[kopftextfpos], MeH);

	if (zpos == 0)
	{
	        strcat (KopfTextFTab[kopftextfpos], ztab [zpos]);
	        zpos = 2;
	}

	ktend = strlen (KopfTextFTab[kopftextfpos]);
	ktpos = ktend;
    DispWindowText (PosLeerE.mask[kopftextfpos].feldid, NULL);
	SetLPCaret ();
	NumIn = FALSE;
    SetFocus (fAuswahlEinh->mask[currentfield].feldid);
}

void TestKopfText (char *kText)
/**
Beim Verlassen einer Zeile, den Kopfinhalt testen.
**/
{
	 int len;
	 char *gpos;


	 len = strlen (kText);
	 if (kText[len - 1] != '=' &&
		 kText[len - 2] != '=')
	 {
		 return;
	 }

	 gpos = strchr (kText, '=');

	 for (gpos = gpos - 1;  gpos != kText; gpos -=1)
	 {
		 if (*gpos > ' ')
		 {
			 *(gpos + 1) = 0;
			 return;
		 }
	 }
}


void TestKopfTextBef (char *kText)
/**
Vor dem Enter einer Zeile, den Kopfinhalt testen.
**/
{
	 if (strcmp (kText, " ") <= 0) return;

	 if (strchr (kText, '=') == NULL)
	 {
		 strcat (kText, " = ");
	 }
}

void ScrollKopfTextDown (int zeilen)
/**
Kopftext nach unten scrollen.
**/
{

	if (kopftextfscroll > 0)
	{
		kopftextfscroll --;
	}
	ToKopfFtab ();
}


void ScrollKopfTextUp (int zeilen)
/**
Kopftext nach oben scrollen.
**/
{
	if (kopftextfscroll < 95)
	{
		kopftextfscroll ++;
	}
	ToKopfFtab ();
}


void NextKopfZeile (void)
{
	int pos;

	EinhToStr ();
	strcpy (KopfTextTab[kopftextpos], KopfTextFTab [kopftextfpos]);
	TestKopfText (KopfTextTab[kopftextpos]);
	if (kopftextpos > 59) return;

	if (strcmp (KopfTextTab[kopftextpos], " ") <= 0) return;

	kopftextpos ++;
	if (kopftextfpos < PosLeerE.fieldanz - 1)
	{
	          kopftextfpos ++;
	}
	else
	{
              ScrollKopfTextUp (1);
	}
	if (kopftextpos >= kopftextanz) kopftextanz = kopftextpos + 1;
    strcpy (KopfTextFTab[kopftextfpos], KopfTextTab[kopftextpos]);
	clipped (KopfTextFTab[kopftextfpos]);
	TestKopfTextBef (KopfTextFTab[kopftextfpos]);
    DispWindowText (PosLeerE.mask[kopftextfpos].feldid, NULL);
	ktend = strlen (KopfTextFTab[kopftextfpos]);
	strcpy (KopfText,KopfTextFTab[kopftextfpos]);
    ktpos = ktend;
	if (ktend == 0)
	{
 	         zpos = 0;
	}
	else
	{
		if (strchr (KopfText, '+'))
		{
		     zpos = 1;
		}
		else if (strchr (KopfText, '='))
		{
            pos = (int) strlen (KopfText);
			if (KopfText[pos - 1] == '=' ||
				KopfText[pos - 2] == '=')
			{
           	        zpos = 2;
			}
			else
			{
				    zpos = 1;
			}
		}
		else
		{
			zpos = 0;
		}
	}
	strcpy (KopfTextFTab[kopftextfpos], KopfText);
    NumIn = FALSE;
}


void PriorKopfZeile (void)
{
	int pos;

	EinhToStr ();
    GetWindowText (PosLeerE.mask[0].feldid, KopfText, 60);
	strcpy (KopfTextTab[kopftextpos], KopfTextFTab[kopftextfpos]);
	TestKopfText (KopfTextTab[kopftextpos]);
	if (kopftextpos < 1) return;
	kopftextpos --;
	if (kopftextfpos > 0)
	{
	          kopftextfpos --;
	}
	else
	{
              ScrollKopfTextDown (1);
	}
    strcpy (KopfTextFTab[kopftextfpos], KopfTextTab[kopftextpos]);
	strcpy (KopfText,KopfTextFTab[kopftextfpos]);
	clipped (KopfTextFTab[kopftextfpos]);
	TestKopfTextBef (KopfTextFTab[kopftextfpos]);
    DispWindowText (PosLeerE.mask[kopftextfpos].feldid, NULL);
	ktend = strlen (KopfTextFTab[kopftextfpos]);
	strcpy (KopfText,KopfTextFTab[kopftextfpos]);
    ktpos = ktend;
	if (ktend == 0)
	{
 	         zpos = 0;
	}
	else
	{
		if (strchr (KopfText, '+'))
		{
		     zpos = 1;
		}
		else if (strchr (KopfText, '='))
		{
            pos = (int) strlen (KopfText);
			if (KopfText[pos - 1] == '=' ||
				KopfText[pos - 2] == '=')
			{
           	        zpos = 2;
			}
			else
			{
				    zpos = 1;
			}
		}
		else
		{
			zpos = 0;
		}
	}
	strcpy (KopfTextFTab[kopftextfpos], KopfText);
    NumIn = FALSE;
}


void SetThisCaret(void)
{
	int pos;

	if (ktend == 0)
	{
 	         zpos = 0;
	}
	else
	{
		if (strchr (KopfText, '+'))
		{
		     zpos = 1;
		}
		else if (strchr (KopfText, '='))
		{
            pos = (int) strlen (KopfText);
			if (KopfText[pos - 1] == '=' ||
				KopfText[pos - 2] == '=')
			{
           	        zpos = 2;
			}
			else
			{
				    zpos = 1;
			}
		}
		else
		{
			zpos = 0;
		}
	}
    NumIn = FALSE;
	ktend = strlen (KopfText);
    ktpos = ktend;
	SetLPCaret ();
}


int ProcPosLeer (WPARAM wParam)
/**
Aktion auf Tasten in EnterNum ausfuehren.
**/
{
	      if (wParam >= (WPARAM) '0' &&
			  wParam <= (WPARAM) '9')
		  {
              	   if (NumIn == FALSE && zpos == 2)
				   {
	                      zpos = 1;
				   }
	               else if (NumIn == FALSE && zpos == 1)
				   {
	                      strcat (KopfTextFTab[kopftextfpos], ztab [zpos]);
						  ktpos = strlen (KopfTextFTab[kopftextfpos]);
				   }

			       KopfTextFTab[kopftextfpos][ktpos] = (char) wParam;
				   if (ktpos < 59) ktpos ++;
				   if (ktend < ktpos) ktend = ktpos;
			       KopfTextFTab[kopftextfpos][ktend] = (char) 0;
				   DispWindowText (PosLeerE.mask[kopftextfpos].feldid, NULL);
                   SetFocus (eNumWindow);
	               NumIn = TRUE;
		  }
		  else if (wParam == (WPARAM) ' ')
		  {
			       EinhToStr ();
		  }

		  else if (wParam == VK_F5)
		  {
			       syskey = KEY5;
  		           PostMessage (NULL, WM_USER, 0, 0);
                   PostMessage (NULL, WM_KEYDOWN,
                               (WPARAM) VK_F5, 0l);
                   SetFocus (eNumWindow);
		  }
		  else if (wParam == (WPARAM) '=')
		  {
			       EinhToStr ();
                   SetFocus (eNumWindow);
		  }
		  else if (wParam == (WPARAM) VK_RETURN)
		  {
			       EinhToStr ();
//                 SetFocus (eNumWindow);
			       syskey = KEYCR;
  		           PostMessage (NULL, WM_USER, 0, 0);
                   PostMessage (NULL, WM_KEYDOWN,
                               (WPARAM) VK_RETURN, 0l);
                   SetFocus (eNumWindow);
		  }
		  else if (wParam == VK_CLS)
		  {
 		          zpos = 0;
		          NumIn = FALSE;
                  memset (KopfTextFTab[kopftextfpos], ' ', 60);
                  KopfTextFTab[kopftextfpos] [0] = (char) 0;
				  ktpos = ktend = 0;
			      DispWindowText (PosLeerE.mask[kopftextpos].feldid, NULL);
                  SetFocus (eNumWindow);
		  }

		  else if (wParam == VK_DOWN)
		  {
			      NextKopfZeile ();
                  SetFocus (eNumWindow);
		  }
		  else if (wParam == VK_UP)
		  {
			      PriorKopfZeile ();
                  SetFocus (eNumWindow);
		  }

          SetLPCaret ();

          SetFocus (fAuswahlEinh->mask[currentfield].feldid);
		  return 1;
}

static int MeProc (HWND hWnd)
/**
Mengeneinheit wechseln.
**/
{
		  meeinhpos ++;
		  if (meeinhLP[meeinhpos] == NULL)
		  {
			  meeinhpos = 0;
		  }
          TextForm.mask[0].feld = meeinhLP[meeinhpos];
		  InvalidateRect (hWnd, NULL, TRUE);
		  return 0;
}

static void strzcpy (char *dest, char *source, int len)
/**
source in dest zentrieren.
**/
{
	    int zpos;

		memset (dest, ' ', len);
		dest[len] = (char) 0;
		if ((int) strlen (source) > len - 1)
		{
			source[len - 1] = (char) 0;
		}
		zpos = (int) (double) ((double) (len - strlen (source)) / 2 + 0.5);
		zpos = max (0, zpos);
		memcpy (&dest[zpos], source, strlen (source));
}

void FillEinhLP (void)
/**
Einheiten aus Prueftabelle lesen.
**/
{
	    static BOOL EinhOK = FALSE;
		int dsqlstatus;

		if (EinhOK) return;

		EinhOK = TRUE;

		ptanz = 0;
		palettepos = ptanz;
        dsqlstatus = ptab_class.lese_ptab_all ("me_einh_leer");
        while (dsqlstatus == 0)
        {
			if ((meeinhLP[ptanz] = (char *) malloc (40)) == 0)
			{
				break;
			}
			if ((meeinhLPK[ptanz] = (char *) malloc (10)) == 0)
			{
				break;
			}
            strzcpy (meeinhLP[ptanz],  clipped (ptabn.ptbez), 16);
            strcpy  (meeinhLPK[ptanz], clipped (ptabn.ptbezk));
			leer_art[ptanz] = ratod (ptabn.ptwer1);
			if (strupcmp (ptabn.ptbez, "PALETTE", 7) == 0)
			{
				palettepos = ptanz;
			}
            ptanz ++;
            if (ptanz == 99) break;
            dsqlstatus = ptab_class.lese_ptab_all ();
        }
		meeinhLP[ptanz]  = NULL;
 	    meeinhLPK[ptanz] = NULL;
		ptscroll = 0;
}

void LeerBuScrollDown (int start, int end, int ze)
{
	    int i;
		ColButton *ColBut1, *ColBut2;

        while (start - ze + 1 < 0) ze --;
		for (i = end; i > start; i --)
		{
			ColBut1 = (ColButton *) fAuswahlEinh->mask[i].feld;
			ColBut2 = (ColButton *) fAuswahlEinh->mask[i - ze].feld;
			ColBut1->text1 = ColBut2->text1;
		}
		ColBut1 = (ColButton *) fAuswahlEinh->mask[i].feld;
		ColBut1->text1 = "";
}


int GetMeEinhPos (char *text)
{
	    int i;

		for (i = 0; i < ptanz; i ++)
		{
			if (strcmp (text, meeinhLP[i]) == 0)
			{
				return i;
			}
		}
		return meeinhpos;
}


int SetLeerText (void)
{
		ColButton *ColBut;

	    ColBut = (ColButton *) fAuswahlEinh->mask[currentfield].feld;
		meeinhpos = GetMeEinhPos (ColBut->text1);
		EinhToStr ();
		return 0;
}

int LeerScrollUp (void)
{
        int i, j;
		int scrollanz;
		int scrollplus;
		ColButton *ColBut;

		scrollanz  = fAuswahlEinh->fieldanz - 2;
		scrollplus = fAuswahlEinh->fieldanz - 3;

		scrollplus = max (0, scrollplus);
        while (scrollplus > 0)
		{
			if ((ptscroll + scrollplus + scrollanz) <= ptanz)
			{
				break;
			}
			scrollplus --;
		}

		ptscroll += scrollplus;
		for (i = ptscroll, j = 1; j <= scrollanz; i ++, j ++)
		{
			ColBut = (ColButton *) fAuswahlEinh->mask[j].feld;
			ColBut->text1 = meeinhLP[i];
		}
		display_form (NuBlock.GeteNumWindow (), fAuswahlEinh, 0, 0);
		return 0;
}

int LeerScrollDown (void)
{
        int i, j;
		int scrollanz;
		int scrollplus;
		ColButton *ColBut;

		scrollanz  = fAuswahlEinh->fieldanz - 2;
		scrollplus = fAuswahlEinh->fieldanz - 3;

		scrollplus = max (0, scrollplus);
        while (scrollplus > 0)
		{
			if ((ptscroll - scrollplus) >= 0)
			{
				break;
			}
			scrollplus --;
		}


		ptscroll -= scrollplus;
		for (i = ptscroll, j = 1; j <= scrollanz; i ++, j ++)
		{
			ColBut = (ColButton *) fAuswahlEinh->mask[j].feld;
			ColBut->text1 = meeinhLP[i];
		}
		display_form (NuBlock.GeteNumWindow (), fAuswahlEinh, 0, 0);
		return 0;
}


void SetLeerBuColors (void)
{
	    int i;
		ColButton *ColBut;

		for (i = 0; i < fAuswahlEinh->fieldanz; i ++)
		{
			ColBut = (ColButton *) fAuswahlEinh->mask[i].feld;
			ColBut->BkColor = LeerBuColor;
		}
}

void SetLeerBuTxt (void)
{
	    int i;
		ColButton *ColBut;

		currentfield = 0;
		ptstart = 0;
		ptend = fAuswahlEinh->fieldanz - 1;
        if (pfeilu == NULL)
		{
                  pfeilu = LoadBitmap (hMainInst, "pfeilu");
		}
        if (pfeilo == NULL)
		{
                  pfeilo = LoadBitmap (hMainInst, "pfeilo");
		}

		for (i = 0; i < fAuswahlEinh->fieldanz; i ++)
		{
			if (meeinhLP[i] == NULL) break;
			ColBut = (ColButton *) fAuswahlEinh->mask[i].feld;
			ColBut->text1 = meeinhLP[i];
			fAuswahlEinh->mask[i].after = SetLeerText;
		}
		if (ptanz >= fAuswahlEinh->fieldanz)
		{
			i = fAuswahlEinh->fieldanz - 1;
			ColBut = (ColButton *) fAuswahlEinh->mask[i].feld;
			ColBut->text1 = "";
			ColBut->bmp   = pfeilu;
			ColBut->BkColor = LeerBuColorPf;
			ColBut->aktivate = 14;
			fAuswahlEinh->mask[i].after = LeerScrollUp;
 		    currentfield = 1;
			ptstart = 1;
			ptend = fAuswahlEinh->fieldanz - 2;

		}
        LeerBuScrollDown (0, fAuswahlEinh->fieldanz - 2, 1);
		ColBut = (ColButton *) fAuswahlEinh->mask[0].feld;
		ColBut->text1 = "";
		ColBut->bmp   = pfeilo;
		ColBut->BkColor = LeerBuColorPf;
		ColBut->aktivate = 14;
		fAuswahlEinh->mask[0].after = LeerScrollDown;
}

void EnterLeerPos (void)
/**
Leergut auf Positionsebene einegen.
**/
{
	    int x,y, cx, cy;
		HFONT hFont;
        HDC hdc;
		TEXTMETRIC tm;
		int i;
		int pos;
		HWND ParentWindow;

        fAuswahlEinh = NuBlock.GetAuswahlEinh ();
		if (GebTara)
		{
		        ParentWindow = WiegWindow;
		}
		else
		{
		        ParentWindow = eWindow;
		}
		FillEinhLP ();
		if (ptanz == 0)
		{
			disp_messG ("Keine Pr�ftabelleneintr�ge f�r Leergut", 2);
		    return;
		}
		RECT eRect, fRect;

 	    save_fkt (4);
		save_fkt (5);
		save_fkt (6);
		save_fkt (7);
		save_fkt (8);
		save_fkt (9);
		save_fkt (10);
		save_fkt (11);

		set_fkt (NULL, 4);
		set_fkt (NULL, 5);
		set_fkt (NULL, 6);
		set_fkt (NULL, 7);
		set_fkt (NULL, 8);
		set_fkt (NULL, 9);
		set_fkt (NULL, 10);
		set_fkt (NULL, 11);

		set_fkt (StopPosLeer, 5);

		NuBlock.SetNum3 (TRUE);

        if (eWindow)
        {
		      GetWindowRect (eWindow,     &eRect);
		      GetWindowRect (eFussWindow, &fRect);
        }
        else
        {
		      GetWindowRect (LogoWindow,  &eRect);
		      GetWindowRect (BackWindow3, &fRect);
        }


		EnableWindows (ParentWindow, FALSE);
		EnableWindows (BackWindow3, FALSE);
		EnableWindows (BackWindow2, FALSE);
        x  = eRect.left;
		y  = eRect.top;
		cx = eRect.right - eRect.left;
		cy = eRect.bottom - eRect.top + fRect.bottom - fRect.top;

        PosLeerFontE.FontHeight = 200;
        while (TRUE)
        {
                 spezfont (&PosLeerFontE);
                 hFont = SetWindowFont (hMainWindow);
                 hdc = GetDC (hMainWindow);
                 SelectObject (hdc, hFont);
                 GetTextMetrics (hdc, &tm);
                 DelFont (hFont);
                 ReleaseDC (hMainWindow, hdc);

                 if ((tm.tmAveCharWidth * 60) < cx) break;
                 PosLeerFontE.FontHeight -= 5;
                 if (PosLeerFontE.FontHeight <= 80) break;
        }
        KonvFormParams ();
        spezfont (&PosLeerFontE);
        hFont = SetWindowFont (hMainWindow);
        hdc = GetDC (hMainWindow);
        SelectObject (hdc, hFont);
        GetTextMetrics (hdc, &tm);
        for (i = 1; i < PosLeerE.fieldanz; i ++)
		{
			PosLeerE.mask[i].pos[0] = (int) (double)
				((double) PosLeerE.mask[i - 1].pos[0] + tm.tmHeight * 1.3);
		}

        lPosWindow = CreateWindow ("PosLeer",
                                    "",
                                    WS_VISIBLE | WS_POPUP | WS_DLGFRAME,
                                    x, y,
                                    cx, cy,
                                    ParentWindow,
                                    NULL,
                                    hMainInst,
                                    NULL);
		if (lPosWindow == NULL)
		{
  		    EnableWindows (ParentWindow, TRUE);
		    EnableWindows (BackWindow3,  TRUE);
		    EnableWindows (BackWindow2,  TRUE);
			return;
		}

		x = (cx - 62 * tm.tmAveCharWidth) / 2;
		y = (int) (double) ((double) 1 * tm.tmHeight * 1.3);
		cx = 62 * tm.tmAveCharWidth;
		cy = (int) (double) ((double) 4 * tm.tmHeight * 1.3);

		hWndLeer = CreateWindowEx (WS_EX_CLIENTEDGE,
			                       "PosLeerKopf",
								   "",
                                    WS_VISIBLE | WS_CHILD,
                                    x, y,
                                    cx, cy,
                                    lPosWindow,
                                    NULL,
                                    hMainInst,
                                    NULL);

		NuBlock.SetParent (lPosWindow);
		NuBlock.SetNumZent (TRUE, 0, (int) (double) ((double) 30 * scrfy));
//		FillEinhLP ();
		SetLeerBuColors ();
		SetLeerBuTxt ();
		ktpos = ktend = 0;
		meeinhpos = palettepos;
		kopftextpos = 0;
		zpos = 0;
		NumIn = FALSE;
		CrCaret = FALSE;
		KopfText[ktend] = (char) 0;
        create_enter_form (hWndLeer, &PosLeerE, 0, 0);

        NuBlock.SetEform (&PosLeerE, lPosWindow, ProcPosLeer);
        NuBlock.SetMeProc (MeProc);

        ReadKopftext ();
        strcpy (KopfTextFTab[0], KopfTextTab[0]);
        strcpy (KopfText, KopfTextTab[0]);
	    TestKopfTextBef (KopfTextFTab[0]);
   	    ktend = strlen (KopfTextFTab[0]);

        ktpos = ktend;
	    strcpy (KopfText,KopfTextFTab[0]);
	    if (ktend == 0)
		{
 	         zpos = 0;
		}
	    else
		{
		     if (strchr (KopfText, '+'))
			 {
		            zpos = 1;
			 }
		     else if (strchr (KopfText, '='))
			 {
                    pos = (int) strlen (KopfText);
			        if (KopfText[pos - 1] == '=' ||
				        KopfText[pos - 2] == '=')
					{
           	                  zpos = 2;
					}
			        else
					{
				              zpos = 1;
					}
			 }
		     else
			 {
			        zpos = 0;
			 }
		}

	    strcpy (KopfTextFTab[0], KopfText);
        NumIn = FALSE;

        DispWindowText (PosLeerE.mask[0].feldid, KopfTextFTab[0]);
        NuBlock.SetNumCaretProc (SetThisCaret);

        NuBlock.EnterCalcBox (lPosWindow, meeinhLP[1], "", 40, "", EntNuProc);
	    strcpy (KopfTextTab[kopftextpos], KopfTextFTab[kopftextfpos]);
	    TestKopfText (KopfTextTab[kopftextpos]);

		SetActiveWindow (lPosWindow);

		set_fkt (StopPosLeer, 5);

		EnableWindows (ParentWindow, TRUE);
		EnableWindows (BackWindow3, TRUE);
		EnableWindows (BackWindow2, TRUE);

		DestroyWindow (hWndLeer);
		DestroyWindow (lPosWindow);

		DestroyCaret ();
		SetActiveWindow (ParentWindow);
        ActivateColButton (BackWindow3, &MainButton2, 3, 0, 1);
 	    restore_fkt (4);
		restore_fkt (5);
		restore_fkt (6);
		restore_fkt (7);
		restore_fkt (8);
		restore_fkt (9);
		restore_fkt (10);
		restore_fkt (11);
}


void ReadKopftext (void)
/**
Kopftext lesen.
**/
{
		int zei;
		int i;


		for (i = 0; i < PosLeerE.fieldanz; i ++)
		{
		          memset (KopfTextFTab[i], ' ', 60);
				  KopfTextFTab[i][60] = 0;
	 		      clipped (KopfTextFTab[i]);
		}
		kopftextfpos = kopftextfscroll = 0;


		for (i = 0; i < LEERMAX; i ++)
		{
		          memset (KopfTextTab[i], ' ', 60);
				  KopfTextTab[i][60] = 0;
	 		      clipped (KopfTextTab[i]);
		}

		kopftextanz = zei = 0;

/*
		if (lsk.kopf_txt == 0l)
		{
		           ToKopfFtab ();
				   return;
		}
*/
		if (GebTara) return;

}


static void InitLeerPosArt (void)
/**
Tabelle mit Leergutartikel initialisieren.
**/
{
	    int i;

		for (i = 0; i < LEERMAX; i ++)
		{
			LeerPosArt[i].lart = (double) 0.0;
			LeerPosArt[i].lanz = 0;
		}
}

static void SetLeerPosArt (double art, int anz)
/**
Tabelle mit Leergutartikel initialisieren.
**/
{
	    int i;

		if (anz == 0) return;
		if (art == (double) 0.0) return;

		for (i = 0; LeerPosArt[i].lanz; i ++)
		{
			if (i == LEERMAX) return;
			if (LeerPosArt[i].lart == art)
			{
				break;
			}
		}

		LeerPosArt[i].lart  = art;
		LeerPosArt[i].lanz += anz;
}

static double GetLeerArt (char *LeerBez)
/**
Artikel-Nummer zu Leergutbezeichnung holen.
**/
{
	    int i;

		for (i = 0; i < ptanz; i ++)
		{
			if (strcmp (meeinhLPK[i], LeerBez) == 0)
			{
				return leer_art[i];
			}
		}
		return (double) 0.0;
}

static BOOL GetNextEinh (char *LeerZeile, int *anz, char *LeerBez, BOOL mode)
/**
Nexte Leergutbezeichnung und Leergutanzahl aus Zeile holen.
**/
{
	     static char *pos = NULL;
		 static char *LZ = NULL;
		 char anzstr [10];
		 int i;

		 if (LZ != LeerZeile)
		 {
			 pos = LeerZeile;
		 }
		 if (mode == FALSE)
		 {
			 pos = LeerZeile;
		 }

		 for (; *pos; pos += 1)
		 {
			 if (*pos == '+');
			 else if (*pos == '=');
			 else if (*pos > ' ')
			 {
				 break;
			 }
		 }

         if (*pos == 0)
		 {
			  *anz = 0;
              LeerBez[0] = 0;
			  return FALSE;
		 }

		 i = 0;
		 for (; *pos; pos += 1)
		 {
			 if (*pos < '0' || *pos > '9')
			 {
				 break;
			 }
			 anzstr[i] = *pos;
			 i ++;
		 }

         if (*pos == 0)
		 {
			  *anz = 0;
              LeerBez[0] = 0;
			  return FALSE;
		 }

		 LZ = LeerZeile;
         anzstr[i] = 0;
		 *anz = atoi (anzstr);
		 for (; *pos; pos += 1)
		 {
			 if (*pos == '+');
			 else if (*pos == '=');
			 else if (*pos > ' ')
			 {
				 break;
			 }
		 }

         if (*pos == 0)
		 {
              LeerBez[0] = 0;
			  return FALSE;
		 }


		 i = 0;
		 for (; *pos; pos += 1)
		 {
			 if (*pos <= ' ')
			 {
				 break;
			 }
             LeerBez[i] = *pos;
			 i ++;
		 }
		 LeerBez[i] = 0;
		 return TRUE;
}

static BOOL IsLeerGutZeile (char *Zeile)
/**
Test, ob die Zeile eine Leergutzeile ist.
**/
{
	     int i, j;
		 int anz;

	     for (i = 0; i < ptanz; i ++)
		 {
			 if (strstr (Zeile, meeinhLPK[i]) == NULL)
			 {
				 break;
			 }
		 }
		 if (i == ptanz) return FALSE;

		 anz = wsplit (Zeile, " ");
		 if (anz == 0) return FALSE;

		 for (i = 0; i < anz; i ++)
		 {
			 if (strcmp (wort[i], "+") == 0) continue;
			 if (strcmp (wort[i], "=") == 0)continue;
			 if (numeric (wort[i])) continue;
  	         for (j = 0; j < ptanz; j ++)
			 {
			        if (strcmp (wort[i], meeinhLPK[j]) == 0)
					{
				              break;
					}
			 }
			 if (j == ptanz) return FALSE;
		 }
		 return TRUE;
}

static BOOL IsLeerGut (double a)
/**
Artikeltyp pruefen.
**/
{
	     int dsqlstatus;

		 if (a <= (double) 0.0) return FALSE;
		 dsqlstatus = lese_a_bas (a);
		 if (dsqlstatus == 100) return FALSE;
         if (_a_bas.a_typ != 11) return FALSE;
		 return TRUE;
}


void WriteLeerZeileArt (char *LeerZeile)
/**
Eine Zeile fuer Leegutartikel bearbeiten.
**/
{
	     double art;
		 int anz;
		 char Bez[20];

		 if (IsLeerGutZeile(LeerZeile) == FALSE) return;

		 if (GetNextEinh (LeerZeile, &anz, Bez, 0) == FALSE) return;

		 art = GetLeerArt (Bez);
		 if (IsLeerGut (art))
		 {
			 SetLeerPosArt (art, anz);
		 }

		 while (GetNextEinh (LeerZeile, &anz, Bez, 1))
		 {
           		 art = GetLeerArt (Bez);
		         if (IsLeerGut (art))
				 {
				         SetLeerPosArt (art, anz);
				 }
		 }
}

static BOOL IsinLeerPos (double art)
/**
Test, ob Leergutartikel aud Positionen im Kopftext erfasst wurden.
**/
{
        int i;

		for (i = 0; LeerPosArt[i].lanz; i ++)
		{
			if (i == LEERMAX) return FALSE;
			if (LeerPosArt[i].lart == art)
			{
				return TRUE;
			}
		}
		return FALSE;
}


double GetGebGew (char *LeerZeile)
/**
Eine Zeile fuer Leegutartikel bearbeiten.
**/
{
	     double art;
		 int anz;
		 char Bez[20];
		 double gew = 0.0;

		 if (IsLeerGutZeile(LeerZeile) == FALSE) return 0.0;

		 if (GetNextEinh (LeerZeile, &anz, Bez, 0) == FALSE) return 0.0;

		 art = GetLeerArt (Bez);
		 if (IsLeerGut (art))
		 {
		         gew += _a_bas.a_gew * anz;
		 }

		 while (GetNextEinh (LeerZeile, &anz, Bez, 1))
		 {
           		 art = GetLeerArt (Bez);
		         if (IsLeerGut (art))
				 {
				         gew += _a_bas.a_gew * anz;
				 }
		 }
		 return gew;
}


void GetGebGewicht (char *ftara)
/**
Gewicht fuer Gebinde holen.
**/
{
	     int i;
		 double gew = 0.0;

 		 if (kopftextanz == 0)
		 {
			kopftextanz ++;
		 }
		 InitLeerPosArt ();
		 for (i = 0; i < kopftextanz; i ++)
		 {
			 gew += GetGebGew (KopfTextTab[i]);
		 }
		 sprintf (ftara, "%7.3lf", gew);
}


static double ean_a = 0l;
static double ean = 0l;


int ScanArtikel (double a)
{
        Text Ean;
        Text EanDb;
        Text EanA;
        STRUCTEAN *EanStruct;
		static short cursor_ean = -1;
        double ean = 0l;

		writelog ("Scanartikel %.0lf\n", a);
        scangew = 0l;
        Ean.Format ("%13.0lf", a);
		if (Ean.GetLength () >= 12)
		{
			EanStruct = Scanean.GetStructean (Ean);
		}
		else
		{
			EanStruct = NULL;
		}
		writelog ("EanStruct %ld\n", EanStruct);
		char *sc;
        if (EanStruct != NULL)
        {
            switch (Scanean.GetType (Ean))
            {
            case EANGEW :
				if (Ean.GetLength () < 13)
				{
					break;
				}
                EanDb = Scanean.GetEanForDb (Ean);
                ean = ratod (EanDb.GetBuffer ());
				sc = Scanean.GetEanGew (Ean);
				if (sc != NULL)
				{
					scangew = atol (Scanean.GetEanGew (Ean));
				}
				else
				{
					EanStruct = NULL;
				}
                break;
            case EANPR :
                EanDb = Scanean.GetEanForDb (Ean);
                ean = ratod (EanDb.GetBuffer ());
                BreakScan = TRUE;
                break;
            default :
                ean = ratod (Ean.GetBuffer ());
                BreakScan = TRUE;
/*
                if (ScanKunEan (ean) == 0)
                {
                    return 0;
                }
*/
            }
        }
/*
        else
        {
                ean = ratod (Ean.GetBuffer ());
				writelog ("ScanEan : EAN %.0lf", ean);
                BreakScan = TRUE;
                if (ScanKunEan (ean) == 0)
                {
				    writelog ("ScanKunEan OK gefunden: a %.0lf", _a_bas.a);
                    return 0;
                }
			    writelog ("ScanKunEan OK nicht gefunden");
        }
*/

        if (cursor_ean == -1)
		{
              DbClass.sqlout ((double *) &ean_a, 3, 0);
              DbClass.sqlin  ((double *) &ean, 3, 0);
              cursor_ean = DbClass.sqlcursor ("select a from a_ean where ean = ?");
		}
        ean = ratod (Ean.GetBuffer ());
        writelog ("ScanEan: EAN %.0lf", ean);
		DbClass.sqlopen (cursor_ean);
		dsqlstatus = DbClass.sqlfetch (cursor_ean);

        if (dsqlstatus == 0)
        {
			     writelog ("ScanEan OK gefunden: a %.0lf", _a_bas.a);
                 dsqlstatus = lese_a_bas (ean_a);
        }
        if (dsqlstatus == 0)
        {
                 return dsqlstatus;
        }
        if (EanStruct != NULL && EanStruct->GetAFromEan () == TRUE)
//            ((EanStruct->GetType ().GetBuffer ())[0] == 'G' ||
//            (EanStruct->GetType ().GetBuffer ())[0] != 'P'))
        {
                 EanA = Ean.SubString (2, EanStruct->GetLen ());
                 a = ratod (EanA.GetBuffer ());
                 dsqlstatus = lese_a_bas (a);
        }
        return dsqlstatus;
}


BOOL TestArtikel (double a)
{

        dsqlstatus = ScanArtikel (a);
        if (dsqlstatus != 0)
        {
                     disp_messG ("Falsche Artikelnummer", 2);
                     return FALSE;
        }
        return TRUE;
}

int BreakEanEnter ()
{
	  break_enter ();
	  return 0;
}

void SetScannFocus ()
{
	  if (fScanform.mask[0].feldid != NULL)
	  {
		  SetFocus (fScanform.mask[0].feldid);
	  }
}


void EnterEan (char *buffer)
{

       save_fkt (5);
       save_fkt (12);
       set_fkt (BreakEanEnter, 5);
       set_fkt (BreakEanEnter, 12);
	   
	   strcpy (eanbuffer, "");
	   SetEnterCharChangeProc (ChangeChar);
       enter_form (eFussWindow, &fScanform, 0, 0);
       CloseControls (&fScanform);
	   SetEnterCharChangeProc (NULL);
	   if (syskey != KEY5)
	   {
		   strcpy (buffer, eanbuffer);
	   }
       restore_fkt (5);
       restore_fkt (12);
}


//===== TODO Multiscan ==============
BOOL MeOK ()
{
// Vorl�ufig kein Abbruch, wenn die Menge erreicht ist.
	return FALSE;

	if (atoi (aufps.s) > 2) return TRUE;
	if (ratod (aufps.auf_me) == 0) return FALSE;

	if (ScanValues->auf_me >= ratod (aufps.auf_me))
	{
		return TRUE;
	}
	return FALSE;
}

int TestNewArt (double a, long posi)
/**
Test, ob der Artikel bei diesem Lieferschein existiert.
**/
{
	     int i;

	     for (i = 0; i < aufpanz; i ++)
		 {
			 if (a == ratod (aufparr[i].a) && posi == -1)
			 {
				 break;
			 }
			 else if (a == ratod (aufparr[i].a) &&
				      posi == atol (aufparr[i].pnr) )
			 {
					  break;
			 }
		 }
		 if (i == aufpanz) return -1;
		 return (i);
}


int TestNewArtCharge (double a, Text& ListCharge)
/**
Test, ob der Artikel bei diesem Lieferschein existiert.
**/
{
	     int i;

	     for (i = 0; i < aufpanz; i ++)
		 {
			 Text Charge = aufparr[i].ls_charge;
			 Charge.Trim ();
			 if (a == ratod (aufparr[i].a) &&
				      ListCharge == Charge)
			 {
					  break;
			 }
			 if (a == ratod (aufparr[i].a) && aufparr[i].s[0] < '3' &&
				      Charge == "")
			 {
					  break;
			 }
		 }
		 if (i == aufpanz) return -1;
		 return (i);
}

BOOL TestScanArticleExist ()
{
	int art_pos;
	art_pos = TestNewArt (ScanValues->a, -1);
	if (art_pos == -1)
	{
		aufpidx = aufpanz;
		aufpanz ++;
		sprintf (aufps.auf_me, "%.3lf", 0.0);
		sprintf (aufps.lief_me, "%.3lf", 0.0);
		sprintf (aufps.pnr, "%ld", (aufpidx + 1) * 10);
		lsp.posi = (aufpidx + 1) * 10;
		strcpy (aufps.ls_charge, ScanValues->Charge.GetBuffer ());
		memcpy (&aufparr[aufpidx], &aufps, sizeof (struct AUFP_S));
         arrtowepos (aufpidx);
         beginwork ();
         WePosClass.dbupdate ();
		 //=== TODO hier noch Bestandsbuchung
         commitwork ();
         LeseWePos ();
         SetListPos (aufpidx);

		ScanValues->auf_me = 0.0;
		SetListPos (GetListPos () + 1);
		InvalidateRect (eWindow, NULL, FALSE);
		return FALSE;
	}
	return TRUE;
}

void CompareCharge ()
{
	int art_pos;

	Text Charge = aufps.ls_charge;
	Charge.Trim ();

    art_pos = TestNewArtCharge (ScanValues->a, ScanValues->Charge);
//	if (art_pos != -1 && art_pos != aufpidx)
	if (art_pos != -1)
	{
		aufpidx = art_pos;
		memcpy (&aufps, &aufparr[aufpidx],sizeof (struct AUFP_S)); 
		if (ScanValues->Charge.GetBuffer () != "")
		{
			strcpy (aufps.ls_charge, ScanValues->Charge.GetBuffer ());
		}
		return;
	}


	double auf_me_ges = ratod (aufps.auf_me);

	aufpidx = aufpanz;
	aufpanz ++;
    strcpy (aufps.s, "2");
	sprintf (aufps.lief_me, "%.3lf", 0.0);
	sprintf (aufps.pnr, "%ld", (aufpidx + 1) * 10);
	lsp.posi = (aufpidx + 1) * 10;
	strcpy (aufps.ls_charge, ScanValues->Charge.GetBuffer ());
    memcpy (&aufparr[aufpidx], &aufps, sizeof (struct AUFP_S));
         arrtowepos (aufpidx);
         beginwork ();
         WePosClass.dbupdate ();
		 //=== TODO hier noch Bestandsbuchung
         commitwork ();
         LeseWePos ();
         SetListPos (aufpidx);
	ScanValues->auf_me = 0.0;
	SetListPos (GetListPos () + 1);
    InvalidateRect (eWindow, NULL, FALSE);
}



BOOL MultiScan ()
{
	char buffer [512];

	ScanValues = new CScanValues;
//	ScanValues->auf_me = ratod (aufps.auf_me); 
	if (ScanValues->auf_me < 0.0) ScanValues->auf_me = 0.0;

    while (!MeOK ())
	{
		while (!ScanValues->ScanOK ())
		{
			strcpy (buffer, "");
			if (SmallScann == FALSE)
			{
		       NuBlock.SetCharChangeProc (ChangeChar);
               NuBlock.EnterNumBox (eWindow,"  Scannen  ",
                                    buffer, -1,
                                    "", EntNuProc);
			}
			else
			{
			 EnterEan (buffer);
			}

			if (syskey == KEY5) return FALSE;

			if (strlen (buffer) > 14)
			{
				Text Ean = buffer;
				ScanEan128 (Ean);
			}
			else
			{
				if (TestArtikel (ratod (buffer)) == FALSE)
				{
					continue;
				}
				ScanValues->SetA (_a_bas.a);
				double gew = (double) ((double) scangew / 1000.0);
				if (artikelOK (_a_bas.a, TRUE) == FALSE)
				{
					continue;
				}
				ScanValues->SetGew (gew);
				break;
			}
		}
		ScanValues->SetACharge ();
		if (TestScanArticleExist ())
		{
			CompareCharge ();
		}
		ScanValues->IncAufMe ();
		ScanMessage.OK ();
		if (_a_bas.me_einh != 2 && _a_bas.me_einh != 3 && _a_bas.me_einh != 4)
		{
			if (_a_bas.a_gew != 0)
			{
				ScanValues->eangew /= _a_bas.a_gew;
				ScanValues->eangew += 0.499; // ab der 1. Dezimalstelle auf ganze Zahlen aufrunden
			}
			sprintf (aufps.lief_me, "%.0lf", ratod (aufps.lief_me) + ScanValues->eangew);
		}
		else
		{
			sprintf (aufps.lief_me, "%.3lf", ratod (aufps.lief_me) + ScanValues->eangew);
		}
         beginwork ();
		sprintf (aufps.auf_me,"%.3lf", ScanValues->auf_me);
		write_wewiepro (ScanValues->eangew, ScanValues->eangew, 0.0);
		ScanValues->Init ();
	    strcpy (aufps.s, "3");
        memcpy (&aufparr[aufpidx], &aufps, sizeof (struct AUFP_S));
         arrtowepos (aufpidx);
         WePosClass.dbupdate ();
		 //=== TODO hier noch Bestandsbuchung
         commitwork ();
         LeseWePos ();
         SetListPos (aufpidx);
        ShowNewElist ((char *) aufparr,
                               aufpanz,
                               (int) sizeof (struct AUFP_S));
        memcpy (&aufps, &aufparr[aufpidx], sizeof (struct AUFP_S));
		 if (aufpanz == 1)
		 {
            ActivateColButton (BackWindow3,  &MainButton2, 2, 0, 1);
            ActivateColButton (eFussWindow, &lFussform,   0, 0, 1);
            ActivateColButton (eFussWindow, &lFussform,   2, 0, 1);
            ActivateColButton (eFussWindow, &lFussform,   3, 0, 1);
            ActivateColButton (eFussWindow, &lFussform,   4, 0, 1);
            ActivateColButton (eFussWindow, &lFussform,   5, 0, 1);
		 }

	}
	ScanMessage.Complete ();

    delete ScanValues;
	ScanValues = NULL;
	return TRUE;
}


BOOL ScanMultiA ()
{

        struct AUFP_S aufps_save, aufps_save0;
        int idxsave;

        beginwork ();

        aufpidx = GetListPos ();


        memcpy (&aufps_save, &aufps,sizeof (struct AUFP_S));
        idxsave = aufpidx;
        aufpidx = 0;

        memcpy (&aufps_save0, &aufparr[aufpidx],sizeof (struct AUFP_S));


        if (Lock_Lsp () == FALSE)
        {
            commitwork ();
            return FALSE;
        }

        memcpy (&aufparr[aufpidx], &aufps, sizeof (struct AUFP_S));
        strcpy (auf_me, aufps.rest);
        akt_buch_me = ratod (aufps.lief_me);
        memcpy (&aufps, &aufparr[aufpidx], sizeof (struct AUFP_S));

		if (!MultiScan ()) 
        {
            commitwork ();

            return FALSE;
        }

        memcpy (&aufparr[aufpidx], &aufps, sizeof (struct AUFP_S));
         arrtowepos (aufpidx);
         beginwork ();
         WePosClass.dbupdate ();
		 //=== TODO hier noch Bestandsbuchung
         commitwork ();
         LeseWePos ();
         SetListPos (aufpidx);

        aufpidx = idxsave;
        ShowNewElist ((char *) aufparr,
                               aufpanz,
                               (int) sizeof (struct AUFP_S));

			 if (aufpanz == 1)
			 {
			    ActivateColButton (BackWindow3,  &MainButton2, 2, 0, 1);
				ActivateColButton (eFussWindow, &lFussform,   0, 0, 1);
				ActivateColButton (eFussWindow, &lFussform,   2, 0, 1);
				ActivateColButton (eFussWindow, &lFussform,   3, 0, 1);
				ActivateColButton (eFussWindow, &lFussform,   4, 0, 1);
				ActivateColButton (eFussWindow, &lFussform,   5, 0, 1);
			}

        return TRUE;
}

 //============ END TODO MultiScan
int ScanA (void)
/**
Eine Zeile in Liste einfuegen.
**/
{
//	     KEINHEIT keinheit;
         struct AUFP_S aufp;
         char buffer [256];
//         char me_buffer [20];
         int pos;
         long akt_posi;
//		 double auf_me auf_me_vgl;

		 // ===== TODO MultiScan
 		 if (ScanMulti)
		 {
			 return ScanMultiA ();
		 }

        scangew = 0l;
         ActivateColButton (BackWindow3, &MainButton2, 3, -1, 1);
         ActivateColButton (BackWindow3, &MainButton2, 0, -1, 1);
         memcpy (&aufp, &aufps, sizeof (struct AUFP_S));
         memcpy (&aufps, &aufp_null, sizeof (struct AUFP_S));

         while (TRUE)
         {
       		       EnableWindow (InsWindow, FALSE);
//       		       EnableWindow (BackWindow3, FALSE);

		           if (SmallScann)
				   {
 			             EnterEan (buffer);
				   }
				   else if (Scanlen != 14)
				   {
                         strcpy (buffer, "");
                         EnterNumBox (eWindow,
                                      "  Artikel  ", buffer, Scanlen,
                                                    "");
				   }
				   else
				   {
                         strcpy (buffer, "0");
                         EnterNumBox (eWindow,
                                      "  Artikel  ", buffer, 14,
                                                    "%13.0f");
				   }
//				   EnableWindow (BackWindow3, TRUE);
                   InvalidateRect (eWindow, NULL, TRUE);
                   UpdateWindow (eWindow);
                   memcpy (&aufps, &aufp_null, sizeof (struct AUFP_S));


                   if ((strlen (buffer) <= 13) && ((double) ratod (buffer) <= (double) 0.0))
                   {
                              memcpy (&aufps, &aufp, sizeof (struct AUFP_S));
                              DeleteInsert ();
                              return 0;
                   }
                   if (syskey == KEY5)
                   {
                              memcpy (&aufps, &aufp, sizeof (struct AUFP_S));
                              DeleteInsert ();
                              return 0;
                   }
                   OpenInsert ();
				   clipped (buffer);
				   if (strlen (buffer) >=16)
				   {
					          Text Ean = buffer;
							  int ret = ScanEan128 (Ean);
							  if (ret == 0)
							  {
								  break;
							  }
				   }
                   else if (artikelOK (buffer, TRUE))
                   {
                              break;
                   }
                   memcpy (&aufps, &aufp, sizeof (struct AUFP_S));
                   DeleteInsert ();
                   ActivateColButton (BackWindow3, &MainButton2, 3, 0, 1);
				   ActivateColButton (BackWindow3, &MainButton2, 0, 0, 1);
                   return 0;
         }

         if (scangew != 0l)
         {
            sprintf (aufps.lief_me, "%.3lf", (double) scangew / 1000);
         }
		 if (hbk_direct)
		 {
		           strcpy (aufps.hbk_date, "");

// Default-Halbarkeits-KZ = 'T' Tage  07.08.2002

/*
                   if (_a_bas.hbk_kz[0] <= ' ')
                   {
                           strcpy (_a_bas.hbk_kz, "T");
                   }
*/

		           if (_a_bas.hbk_kz[0] > ' ')
				   {
                           sysdate (buffer);
                           EnterNumBox (eWindow,
                                " Haltbarkeitsdatum ", buffer, 11,
                                   "dd.mm.yyyy");
                           InvalidateRect (eWindow, NULL, TRUE);
                           UpdateWindow (eWindow);
                           if (syskey != KEY5)
						   {
					          strcpy (aufps.hbk_date,buffer);
						   }
				   }
         }


         if (cfg_ohne_preis == 0 && immer_preis)
         {
                   EnterPreisBox (eWindow, -1, -1);
         }

         current_form = &testform;
         DeleteInsert ();
         InvalidateRect (eWindow, NULL, TRUE);
         UpdateWindow (eWindow);
         pos = GetListPos () + 1;
         akt_posi = GetNextPosi (pos);
         sprintf (aufps.pnr, "%ld", akt_posi);
         ScrollAufp (pos);
		 strcpy (aufps.ls_charge, "");
         memcpy (&aufparr [pos], &aufps, sizeof (struct AUFP_S));
         arrtowepos (pos);
         beginwork ();
         WePosClass.dbupdate ();
         commitwork ();
         LeseWePos ();
         SetListPos (pos);
         ShowNewElist ((char *) aufparr,
                                aufpanz,
                                (int) sizeof (struct AUFP_S));
		 if (aufpanz == 1)
		 {
            ActivateColButton (BackWindow3,  &MainButton2, 2, 0, 1);
            ActivateColButton (eFussWindow, &lFussform,   0, 0, 1);
            ActivateColButton (eFussWindow, &lFussform,   2, 0, 1);
            ActivateColButton (eFussWindow, &lFussform,   3, 0, 1);
            ActivateColButton (eFussWindow, &lFussform,   4, 0, 1);
            ActivateColButton (eFussWindow, &lFussform,   5, 0, 1);
		 }
         memcpy (&aufps, &aufparr [pos], sizeof (struct AUFP_S));
         CheckHbk ();
		 if (wieg_neu_direct)
		 {
                      IsDblClck (pos);
		 }
         return 0;
}


int DoScann ()
/**
Scannen f�r Artikel starten.
**/
{
        unsigned int scancount = 0; 

		if (ScanMode)
		{
			return 0;
		}

        InScanning = TRUE;
		for (int i = 0; i < lFussform.fieldanz; i ++)
		{
                 ActivateColButton (eFussWindow, &lFussform, i, -1, 0);
        }
		for (i = 0; i < MainButton.fieldanz; i ++)
		{
                 ActivateColButton (BackWindow2, &MainButton, i, -1, 1);
		}
        EnableWindow (BackWindow2, FALSE);
        if (eWindow != NULL)
        {
            EnableWindow (eWindow, FALSE);
        }

        ActivateColButton (BackWindow3, &MainButton2, 3, -1, 1);
        BreakScan = FALSE;
        syskey = 0;
        while (TRUE)
        {
               BOOL ret = ScanA ();
               if (ret)
               {
                   scancount ++;
               }
               if (syskey == KEY5 || BreakScan)
               {
                   break;
               }
/*
               if (ret && eti_ez != NULL)
               {
                    PrintEti ();
               }
*/
/*
		       if (autocursor && scancount >= (unsigned int) lsp.auf_me)
               {
                   if (aufidx < (int) GetListAnz () - 1)
                   {
                         scancount = 0;
                         SetListPos (GetListPos () + 1);
                         InvalidateRect (eWindow, NULL, FALSE);
                   }
                   else
                   {
                         break;
                   }
               }
*/
        }
        syskey = 0; 
		for (i = 0; i < lFussform.fieldanz; i ++)
		{
                 ActivateColButton (eFussWindow, &lFussform, i, 0, 0);
        }
		for (i = 0; i < MainButton.fieldanz; i ++)
		{
                 ActivateColButton (BackWindow2, &MainButton, i, 1, 1);
		}
        ActivateColButton (BackWindow3, &MainButton2, 3, 0, 1);
        EnableWindow (BackWindow2, TRUE);
        if (eWindow != NULL)
        {
            EnableWindow (eWindow, TRUE);
        }
        InScanning = FALSE;
        return 0;
}




int ScanEan128Ai01 (Text& Ean128)
{
        double ean1 = 0.0;

		if (ScanValues == NULL) return -1;

		if (ScanValues->aflag) return 0;

		Text *ean = Scanean.GetEan128Ai (Text ("01"), Ean128);
		if (ean == NULL)
		{

				return 100;
		}

		Text Ean = ean->SubString (1, Ean128Eanlen);
		delete ean;
		double a = ratod (Ean.GetBuffer ());
		ean1 = a;
		if (artikelOK (a, TRUE) == FALSE)
		{
			return 100;
		}
		ScanValues->SetA (_a_bas.a);
		return 0;
}

int ScanEan128Ai31 (Text& Ean128)
{
        double gew = 0.0;

		if (ScanValues == NULL) return -1;

		if (ScanValues->gewflag) return 0;

		Text *Gew = Scanean.GetEan128Ai (Text ("3103"), Ean128);
		if (Gew != NULL)
		{

				gew = ratod (Gew->GetBuffer ()) / 1000.0;
				delete Gew;
				ScanValues->SetGew (gew);
				return 0;
		}

		Gew = Scanean.GetEan128Ai (Text ("3102"), Ean128);
		if (Gew != NULL)
		{

				gew = ratod (Gew->GetBuffer ()) / 100.0;
				delete Gew;
				ScanValues->SetGew (gew);
				return 0;
		}

		Gew = Scanean.GetEan128Ai (Text ("3101"), Ean128);
		if (Gew != NULL)
		{

				gew = ratod (Gew->GetBuffer ()) / 10.0;
				delete Gew;
				ScanValues->SetGew (gew);
				return 0;
		}

// St�ck ohne Nachkomma wird im Moment nicht benutzt

		Gew = Scanean.GetEan128Ai (Text ("30"), Ean128);
		if (Gew != NULL)
		{

				gew = ratod (Gew->GetBuffer ());
				delete Gew;
				ScanValues->SetGew (gew);
				return 0;
		}

		Gew = Scanean.GetEan128Ai (Text ("37"), Ean128);
		if (Gew != NULL)
		{

				gew = ratod (Gew->GetBuffer ());
				delete Gew;
				ScanValues->SetGew (gew);
				return 0;
		}

		return 100;
}


int ScanEan128Ai15 (Text& Ean128)
{

		if (ScanValues == NULL) return -1;

		if (ScanValues->MhdFlag) return 0;
		Text Mhd = "";
		Text *mhd = Scanean.GetEan128Ai (Text ("15"), Ean128);
		if (mhd == NULL)
		{
			return 100;
		}
		Mhd = *mhd;
		delete mhd;
		ScanValues->SetMhd (Mhd);
        return 0;
}


int ScanEan128Ai10 (Text& Ean128)
{

		if (ScanValues == NULL) return -1;
		if (ScanValues->ChargeFlag) return 0;
		Text Charge = "";
		if (_a_bas.charg_hand == 0) 
		{
			ScanValues->SetCharge (Charge);
			return 0;
		}
		Text *charge = Scanean.GetEan128Ai (Text ("10"), Ean128);

		if (charge != NULL)
		{
				Charge = *charge;
				delete charge;
				ScanValues->SetCharge (Charge);
				return 0;
		}
        return 0;
}


int ScanEan128MultiAi (Text& Ean128)
{
	ScanEan128Ai01 (Ean128);
	ScanEan128Ai31 (Ean128);
	ScanEan128Ai10 (Ean128);

//	ScanEan128Ai15 (Ean128);
    return 0; 
}



int ScanEan128Ai (Text& Ean128, BOOL DispMode=TRUE)
{
        if (ScanMulti)
		{
			return ScanEan128MultiAi (Ean128);
		}

        double ean1 = 0.0;
		double Gew;


		Text *ean = Scanean.GetEan128Ai (Text ("01"), Ean128);
		if (ean == NULL)
		{

		        disp_messG ("Fehler bei Ean128 : Eancode", 2);
				return 100;
		}

		Text Ean = ean->SubString (1, Ean128Eanlen);
		delete ean;
		double a = ratod (Ean.GetBuffer ());
		ean1 = a;
		if (artikelOK (a, FALSE) == FALSE)
		{
			return 100;
		}

		sprintf (aufps.a, "%.0lf", _a_bas.a);
		BreakScan = FALSE;

		if (_a_bas.hnd_gew[0] == 'G')
		{
			Text *gew = Scanean.GetEan128Ai (Text ("3103"), Ean128);
			if (gew == NULL)
			{
				/*** wenn dies drin ist , schmierts auf dem tolle-Rechner ab !! l�st sich nicht starten
				delete gew;
				Text *gew = Scanean.GetEan128Ai (Text ("3102"), Ean128); //Detlef 031208 
				if (gew == NULL)
				{
					Gew = ratod (gew->GetBuffer ());
					delete gew;
				}
				*********/

//		        disp_messG ("Fehler bei Ean128 : keine Gewichtauszeichnung", 2);
//				return 100;
			}
			else
			{
				Gew = ratod (gew->GetBuffer ());
				delete gew;
			}
		}
		else
		{
			Text *gew = Scanean.GetEan128Ai (Text ("30"), Ean128);
			if (gew == NULL)
			{
				gew = Scanean.GetEan128Ai (Text ("37"), Ean128);
			}
			if (gew == NULL)
			{
//		        disp_messG ("Fehler bei Ean128 : keine St�ckauszeichnung", 2);
//				return 100;
			}
			else
			{
				Gew = ratod (gew->GetBuffer ()) * 1000.0;
				delete gew;
			}
		}
		Text Mhd = "";
		Text *mhd = Scanean.GetEan128Ai (Text ("15"), Ean128);
//		if (mhd == NULL)
		if (mhd != NULL)
		{
				Text Mhd = *mhd;
				Ean128Date = Mhd;
				strcpy (aufps.hbk_date, Ean128Date.ToString ().GetBuffer ());
				delete mhd;
		}
		else
		{
			Ean128Date = Mhd;
			strcpy (aufps.hbk_date, Ean128Date.ToString ().GetBuffer ());
		}
		Text Charge = "";
		Text *charge = Scanean.GetEan128Ai (Text ("10"), Ean128);

		if (ChargeZwang && _a_bas.charg_hand == 1 && charge == NULL)
		{
		        disp_messG ("Fehler bei Ean128 : Charge", 2);
				return 100;
		}

		if (charge != NULL)
		{
					
			    if (_a_bas.charg_hand == 9)
				{
					strcpy (aufps.ident_nr, charge->GetBuffer ());
				}
				else
				{
					Charge = *charge;
					strcpy (aufps.ls_charge, Charge.GetBuffer ());
				}
		}
		else
		{
			    if (_a_bas.charg_hand == 9)
				{
					strcpy (aufps.ident_nr, "");
				}
				else
				{
					strcpy (aufps.ls_charge, "");
				}
		}
		sprintf (aufps.a, "%.0lf", _a_bas.a);
		sprintf (aufps.s, "%hd", 2);
		scangew = (long) (Gew);
		sprintf (aufps.me_einh, "%hd", 2);
		memcpy (&ptabn, &ptabn_null, sizeof (struct PTABN));
		if (ptab_class.lese_ptab ("me_einh", aufps.me_einh) == 0)
		{
                  strcpy (aufps.lief_me_bz, ptabn.ptbezk);
		}
        return 0;
}


int ScanEan128Charge (Text& Ean128, BOOL Ident)
{
		Text Charge = "";
		Text *charge = Scanean.GetEan128Ai (Text ("10"), Ean128);

/*
		if (ChargeZwang && _a_bas.charg_hand == 1 && charge == NULL)
		{
		        disp_messG ("Fehler bei Ean128 : Charge", 2);
				return 100;
		}
*/

		if (charge != NULL)
		{
				Charge = *charge;
				if (Charge.GetLength () > 20)
				{
					  Charge = Charge.SubString (0, 20);
				}
			    if (_a_bas.charg_hand == 9)
				{
					strcpy (aufps.ident_nr, Charge.GetBuffer ());
					Charge = "";
				}
				else
				{
					strcpy (aufps.ls_charge, Charge.GetBuffer ());
					Charge = ""; //110908
				}
		}
		else
		{
			    if (_a_bas.charg_hand == 9 || Ident)
				{
					strcpy (aufps.ident_nr, Ean128.GetBuffer ()); //290709  
				}
				else
				{
					strcpy (aufps.ls_charge, Ean128.GetBuffer ());
				}
		}
//		sprintf (aufps.a, "%.0lf", _a_bas.a);
//		sprintf (aufps.s, "%hd", 2);
        return 0;
}



int ScanEan128 (Text& Ean128, BOOL DispMode)
{
/*
	if (Text (Ean128.SubString (0, 2)) == "00")
	{
		return ScanEan128Nve (Ean128);
	}
*/
	return ScanEan128Ai (Ean128, DispMode);
}

// RoW 12.11.2007 Ean128 mit Endezeichen

int ChangeChar (int key)
{

	CInteger Key (key);
	Scanean.AiInfos.FirstPosition ();
    AiInfo *ai;
	while ((ai = (AiInfo *) Scanean.AiInfos.GetNext ()) != NULL)
	{
		ai->EndChar.FirstPosition ();
		CInteger *ec;
		while ((ec = (CInteger *) ai->EndChar.GetNext ()) != NULL)
		{
			if (Key == *ec)
			{
				return ai->EndCharEdit;
			}
		}
	}
	return key;
}



long GenWepTxt0 (void)
/**
Nummer fuer Lieferschein-Positionstexte generieren.
**/
{
	int count; 
	long nr;

	nr = 0l;
    nr = AutoNr.GetNr (nr);
	count = 0;
	while (nr == 0l)
	{
		         Sleep (10);
		         count ++;
				 if (count == 10) break;
                 nr = AutoNr.GetNr (nr);
	}
	return nr;
}

BOOL TxtNrExist (long nr0)
{
	   int dsqlstatus;
	   static int cursor = -1;
	   static long nr;
	   
	   if (cursor == -1)
	   {
			DbClass.sqlin ((long *)  &nr, 2, 0);
			cursor = DbClass.sqlcursor ("select nr from we_txt where nr = ?");
	        if (cursor == -1) return FALSE;
	   }
	   nr = nr0;
	   DbClass.sqlopen (cursor);
	   dsqlstatus = DbClass.sqlfetch (cursor);
	   if (dsqlstatus != 0)
	   {
		    return FALSE;
	   }
	   return TRUE;
}


long GenWepTxt (void)
/**
Nummer fuer Lieferschein-Positionstexte generieren.
**/
{
	 long nr;

	 while (TRUE)
	 {
		 nr = GenWepTxt0 ();
		 if (TxtNrExist (nr) == FALSE) break;
		 if (nr > 99999999) 
		 {
			 return 0l;
		 }
	 }
	 return nr;
}


void FromPosTxt ()
{
	TEXTBLOCK::Rows.Init ();
	we_txt.nr = atol (aufps.lsp_txt);
	int dsqlstatus = cWept.dbreadfirst ();
	while (dsqlstatus == 0)
	{
		Text *txt = new Text (we_txt.txt);
		TEXTBLOCK::Rows.Add (txt);
	    dsqlstatus = cWept.dbread ();
	}
}


void ToPosTxt ()
{
	static int del_cursor = -1;
    int aufidx;
	
	beginwork ();
	if (del_cursor == -1)
	{
		DbClass.sqlin ((long *) &we_txt.nr, 2, 0);
		del_cursor = DbClass.sqlcursor ("delete from we_txt where nr = ?");
	}
    aufidx = GetListPos ();
    memcpy (&aufps, &aufparr [aufidx], sizeof (struct AUF_S));
	if (we_txt.nr == 0)
	{
       int wetxt = GenLText ();
	   if (wetxt == 0l)
	   {
		   return;
	   }
	   we_txt.nr = wetxt;
	   sprintf (aufps.lsp_txt, "%ld", we_txt.nr); 
       memcpy (&aufparr [aufidx], &aufps, sizeof (struct AUFP_S));
	}

	int anz = TEXTBLOCK::Rows.GetCount ();
	for (int i = anz - 1; i >= 0; i --)
	{
		Text *txt = (Text *) TEXTBLOCK::Rows.Get (i);
		txt->Trim ();
		if (*txt != "")
		{
			break;
		}
	}

	DbClass.sqlexecute (del_cursor);
	anz = i + 1;
	for (i = 0; i < anz; i ++)
	{

		Text *txt = (Text *) TEXTBLOCK::Rows.Get (i);
		we_txt.zei = (i + 1) * 10;
		strncpy (we_txt.txt, txt->GetBuffer (), 60);
		we_txt.txt[60] = 0;
        cWept.dbupdate ();
	}
    arrtowepos (aufidx);
    WePosClass.dbupdate ();
	commitwork ();
}



int PosTexte ()
{
            textblock = new TEXTBLOCK;
			textblock->SetLongText (TRUE);
	        FromPosTxt ();
			EnableWindows (hMainWindow, FALSE);
			EnableWindow (eWindow, FALSE);
			textblock->MainInstance (hMainInst, eWindow);
            textblock->ScreenParam (scrfx, scrfy);
			textblock->SetTopMost (TRUE);
            textblock->EnterTextBox (BackWindow1, "Pos-Texte", "", 0, "", EntNumProc);
			EnableWindows (hMainWindow, TRUE);
			EnableWindow (eWindow, TRUE);

			InvalidateRect (hMainWindow, NULL, TRUE);
			InvalidateRect (eWindow, NULL, TRUE);
			InvalidateRect (eFussWindow, NULL, TRUE);
			UpdateWindow (hMainWindow);
			UpdateWindow (eWindow);
			UpdateWindow (eFussWindow);

		    delete textblock;
		    textblock = NULL;
			if (syskey == KEY5)
			{
				return 0;
			}
			ToPosTxt ();
			return 0;
}


/* Auswahl ueber Temperaturen    */


struct TEMPS
{
        char temptext [20];
        char temp [6];
};


struct TEMPS temparr [4], temps;


static int tempidx;
static int tempanz = 0;

mfont TempFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                1,
                                NULL};

static field _tempform [] =
{
        temps.temptext, 16, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        temps.temp,      6, 1, 0,17, 0,     "%3.1",    DISPLAYONLY, 0, 0, 0,
};

static form tempform = {2, 0, 0, _tempform, 0, 0, 0, 0, &TempFont};

static field _tempub [] =
{
        "Temperatur",    16, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        "Wert",           6, 1, 0,17, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form tempub = {2, 0, 0, _tempub, 0, 0, 0, 0, &TempFont};

static field _tempvl [] =
{
    "1",                  1,  0, 0, 17, 0, "", NORMAL, 0, 0, 0,
};

static form tempvl = {1, 0, 0, _tempvl, 0, 0, 0, 0, &TempFont};


void EnterTemperature (int idx)
/**
Reaktion auf Doppelclick in der Liste.
**/
{
	    char buffer[20];
        tempidx = idx;
		if (tempidx == 0)
		{
			int cx = (int) (scrfx * 600.0);
  			int cy = (int) (scrfy * 30);
			NuBlock.SetNumParent (hMainWindow);
			NuBlock.SetNumZent (FALSE, cx, cy);
			strcpy (buffer, aufps.temp);
			NuBlock.EnterNumBox (hMainWindow,"  Temperatur 1 ",
                                         buffer,    5, "%3.1f" ,
	      						 	     EntNuProc);
			if (syskey != KEY5)
			{
				strcpy (aufps.temp, buffer);
				strcpy (temparr[0].temp, buffer); 
				ShowNewElist ((char *) temparr,
							   tempanz,
							   sizeof (struct TEMPS));
				SetListPos (0);
			}
		}
		else if (tempidx == 1)
		{
			int cx = (int) (scrfx * 600.0);
  			int cy = (int) (scrfy * 30);
			NuBlock.SetNumParent (hMainWindow);
			NuBlock.SetNumZent (FALSE, cx, cy);
			strcpy (buffer, aufps.temp2);
			NuBlock.EnterNumBox (hMainWindow,"  Temperatur 2 ",
                                         buffer,    5, "%3.1f" ,
	      						 	     EntNuProc);
			if (syskey != KEY5)
			{
				strcpy (aufps.temp2, buffer);
				strcpy (temparr[1].temp, buffer); 
				ShowNewElist ((char *) temparr,
							   tempanz,
							   sizeof (struct TEMPS));
				SetListPos (1);
			}
		}
		else if (tempidx == 2)
		{
			int cx = (int) (scrfx * 600.0);
  			int cy = (int) (scrfy * 30);
			NuBlock.SetNumParent (hMainWindow);
			NuBlock.SetNumZent (FALSE, cx, cy);
			strcpy (buffer, aufps.temp3);
			NuBlock.EnterNumBox (hMainWindow,"  Temperatur 3 ",
                                         buffer,    5, "%3.1f" ,
	      						 	     EntNuProc);
			if (syskey != KEY5)
			{
				strcpy (aufps.temp3, buffer);
				strcpy (temparr[2].temp, buffer); 
				ShowNewElist ((char *) temparr,
							   tempanz,
							   sizeof (struct TEMPS));
				SetListPos (2);
			}
		}
		else if (tempidx == 3)
		{
			int cx = (int) (scrfx * 600.0);
  			int cy = (int) (scrfy * 30);
			NuBlock.SetNumParent (hMainWindow);
			NuBlock.SetNumZent (FALSE, cx, cy);
			strcpy (buffer, aufps.ph_wert);
			NuBlock.EnterNumBox (hMainWindow,"  PH-Wert  ",
                                         buffer, 6, "%4.2f" ,
	      						 	     EntNuProc);
			if (syskey != KEY5)
			{
				strcpy (aufps.ph_wert, buffer);
				strcpy (temparr[3].temp, buffer); 
				ShowNewElist ((char *) temparr,
							   tempanz,
							   sizeof (struct TEMPS));
				SetListPos (3);
			}
		}
		NuBlock.SetNumParent (NULL);
		NuBlock.SetNumZent (FALSE, 0, 0);
        return;
}

int ShowTemperature (HWND hWnd, char * ptabarr, int ptabanz, char * ptab,
					int ptsize, form *ptform, form *ptvl, form *ptub)
{
        RECT rect;
        RECT frmrect;
        int x, y, cx, cy;
        HWND shListBox;
        HWND slbox;
        struct LMENUE smenue;

        GetMenue (&smenue);
        MenTextSave (&smenue);
        shListBox = GethListBox ();
        slbox = Getlbox ();

        RegisterPt ();
        GetWindowRect (hMainWindow, &rect);
        GetFrmRectEx (ptform, ptform->font, &frmrect);

        x = rect.left + sysxplus;
        y = rect.top + 20;
        cx = frmrect.right + 15;
        cy = 185;

        set_fkt (endlist, 5);
        SetDblClck (EnterTemperature, 0);
        PtListeAktiv = 1;
        SetAktivWindow (hWnd);
        SetListFont (TRUE);
        spezfont (ptform->font);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_VISIBLE);
        PtWindow = OpenListWindowBoEx (x, y, cx, cy);
//        PtWindow = OpenListWindowEnEx (x, y, cx, cy);
        GetWindowRect (PtWindow, &rect);
        y = rect.bottom;
		cx = rect.right - rect.left;
        PtFussWindow = CreateWindow ("PtFuss",
                                    "",
                                    WS_POPUP | WS_VISIBLE | WS_DLGFRAME,
                                    x,
//                                    y + cy + 23,
                                    y,
                                    cx, 70,
                                    hWnd,
                                    NULL,
                                    hMainInst,
                                    NULL);
        if (ptvl) ElistVl (ptvl);
        if (ptub) ElistUb (ptub);
        EnableWindows (hMainWindow, FALSE);
        ShowElist ((char *) ptabarr,
                  ptabanz,
                  (char *) ptab,
                  (int) ptsize,
                  ptform);
        SetMouseLockProc (MouseInPt);
        EnterElist (PtWindow, (char *) ptabarr,
                             ptabanz,
                             (char *) ptab,
                             (int) ptsize,
                             ptform);
        CloseControls (&PtButton);
        CloseEWindow (PtWindow);
        SetActiveWindow (hMainWindow);
        EnableWindows (hMainWindow, TRUE);
        init_break_list ();
//        SetMouseLock (FALSE);
        SetMouseLockProc (NULL);
        DestroyWindow (PtFussWindow);
        PtListeAktiv = 0;
        SetMenue (&smenue);
        SethListBox (shListBox);
        Setlbox (slbox);
        SetAktivWindow (hWnd);
        return tempidx;
}


int AuswahlTemperature (void)
/**
Ausahlfenster ueber Geraete anzeigen.
**/
{
        EnableWindow (eWindow, FALSE);
        EnableWindow (eKopfWindow, FALSE);
        EnableWindow (eFussWindow, FALSE);
	    tempanz = 0;
        sprintf (temparr[tempanz].temptext, "Temperatur 1");
        sprintf (temparr[tempanz].temp,"%s", aufps.temp);
	    tempanz ++;
        sprintf (temparr[tempanz].temptext, "Temperatur 2");
        sprintf (temparr[tempanz].temp,"%s", aufps.temp2);
	    tempanz ++;
        sprintf (temparr[tempanz].temptext, "Temperatur 3");
        sprintf (temparr[tempanz].temp,"%s", aufps.temp3);
	    tempanz ++;
        sprintf (temparr[tempanz].temptext, "PH-Wert");
        sprintf (temparr[tempanz].temp,"%s", aufps.ph_wert);
	    tempanz ++;

        syskey = 0;
        sysidx = ShowTemperature (LogoWindow, (char *) temparr, tempanz,
                     (char *) &temps,(int) sizeof (struct TEMPS),
                     &tempform, &tempvl, &tempub);
        EnableWindow (eWindow, TRUE);
        EnableWindow (eKopfWindow, TRUE);
        EnableWindow (eFussWindow, TRUE);
        SetDblClck (IsDblClck, 0);
        if (syskey == KEY5 || syskey == KEYESC)
        {
                return 0;
        }
        return sysidx;
}




void DoChargeZwang (void)
{

						char buffer [255];
						if (strlen(clipped(aufps.ls_charge)) > 0)
						{
							return;
						}
						if (_a_bas.charg_hand == 9)
						{
                          strcpy (buffer, aufps.ident_nr);
						  NuBlock.SetCharChangeProc (ChangeChar);
						  /*110908 so ist es auf den Touchern bei M�fli h�ngengeblieben
                          NuBlock.EnterNumBox (LogoWindow, "  Ident-Nr  ",
                                       buffer, -1, "", EntNuProc);
									   */
                          NuBlock.EnterNumBox (WiegWindow, "  Ident-Nr  ",
                                       buffer, -1, "", EntNuProc);    
						}
						else if (_a_bas.charg_hand == 1)
						{
                          strcpy (buffer, aufps.ls_charge); //110908
						  NuBlock.SetCharChangeProc (ChangeChar);//110908
/* 110908
                          EnterNumBox (WiegWindow,"  Chargen-Nr  ",
                                       buffer, 17,
                                       artwform.mask[0].picture);
									   */
                          NuBlock.EnterNumBox (WiegWindow,"  Chargen-Nr  ",
                                       buffer, -1, "", EntNuProc);    
						}
						if (syskey != KEY5)
						{
								if ((_a_bas.charg_hand == 9 || _a_bas.charg_hand == 1) && strlen (buffer) > 4)
								{
									Text Ean = buffer;
									ScanEan128Charge (Ean,0);
								}
								else
								{
									strcpy (aufps.ls_charge, buffer); 
								}

								display_form (WiegWindow, &artwform, 0, 0);
								current_wieg = TestAktivButton (PLUS, current_wieg);
								SetWiegFocus ();
						 }
}


BOOL BsdArtikel (double a)
/**
Bestandskennzeichen in a_bas pruefen.
**/
{
	     char bsd_kz [2];

		 strcpy (bsd_kz, "N");
		 LiefBzg.sqlin ((double *) &a, 3, 0);
		 LiefBzg.sqlout ((char *) bsd_kz, 0, 2);
		 LiefBzg.sqlcomm ("select bsd_kz from a_bas where a = ?");
		 if (bsd_kz[0] == 'J') return TRUE;
		 return FALSE;
}

void BucheBsd (double a,char *lief_best, double auf_me, double auf_stk, short me_einh_lief, double pr_ek_bto , char *chargennr, char *identnr)
// 190709
//Bestandsbuchung vorbereiten.

{
	double buchme;
	double buchstk;
	char datum [12];

//	if (we_kopf.bsd_verb_kz[0] != 'J')
//	{
//		return;
//	}

	if (BsdArtikel (a) == FALSE) return;
	buchme = auf_me;
	buchstk = auf_stk;

	bsd_buch.nr  = best_kopf.best_blg;
	strcpy (bsd_buch.blg_typ, "WE");   // Achtung Kennzeichen W noch �berpr�fen
 	bsd_buch.mdn = we_kopf.mdn;
	bsd_buch.fil = we_kopf.fil;
	bsd_buch.kun_fil = 0;
	bsd_buch.a   = a;
	if (a_lgr.buch_artikel > 0.9 && a_lgr.buch_artikel < 9999999999999.0)
	{
		if (a_lgr.a == a) bsd_buch.a = a_lgr.buch_artikel;
	}
	sysdate (datum);
//	bsd_buch.dat = dasc_to_long (datum);
//Metro 	bsd_buch.dat = we_kopf.we_dat;
	if (we_pos.erz_dat == LongNull)
	{
 		bsd_buch.dat = 0;
	}
	else
	{
 		bsd_buch.dat = we_pos.erz_dat;   // Metro : erz_dat = Fangdatum
	}

	systime (bsd_buch.zeit);
	strcpy (bsd_buch.pers, sys_ben.pers);

    sprintf (bsd_buch.bsd_lgr_ort, "%ld", akt_lager);

	bsd_buch.qua_status = 0;
	bsd_buch.me = buchme;
	bsd_buch.me2 = buchstk;
	if (strcmp (we_kopf.blg_typ, "X") == 0)
	{
		bsd_buch.me *= -1;
	}
	bsd_buch.bsd_ek_vk = we_pos.pr_ek_nto ;
    strcpy (bsd_buch.chargennr, clipped (chargennr));
    strcpy (bsd_buch.ident_nr, clipped(identnr));
    strcpy (bsd_buch.herk_nachw, "");
	sprintf(bsd_buch.herk_nachw,"Fa=%d;Fg=%d",atoi(we_pos.ez_nr),atoi(we_pos.es_nr));
    sprintf (bsd_buch.lief, "%s", we_kopf.lief);
    bsd_buch.auf = best_kopf.best_blg;
    bsd_buch.verf_dat = we_pos.hbk_dat;
    strcpy  (bsd_buch.verfall, "");
    bsd_buch.delstatus = 0;
	bsd_buch.auf = atoi(we_kopf.lief_rech_nr);
    strcpy  (bsd_buch.err_txt, "19.02.2009");
	BsdBuch.dbinsert ();
}

