#include <stdio.h>
#include <stdlib.h>
#include "strfkt.h"
#include "scanean.h"
#include "Integer.h"


BOOL SCANEAN::ReadStandard (FILE *fp, char *buffer)
{
        int i = 0;

   
        while (fgets (buffer, 511, fp))
        {
            if (buffer[0] == '[')
            {
                Structean[i] = NULL;
                return TRUE;
            }
            Token *token = new Token (buffer, " ");
            if (token == NULL)
            {
                continue;
            }
            int Tanz = token->GetAnzToken ();
            if (Tanz < 2)
            {
                continue;
            }
            if (Tanz == 2)
            {
                 Structean[i] = new STRUCTEAN (atoi (token->GetToken(0)), token->GetToken(1),
                                               4, FALSE); 
            }
            else if (Tanz == 3)
            {
                 Structean[i] = new STRUCTEAN (atoi (token->GetToken(0)), token->GetToken(1),
                                               atoi (token->GetToken(2)), FALSE);
            }
            else if (Tanz >= 4)
            {
                 Structean[i] = new STRUCTEAN (atoi (token->GetToken(0)), token->GetToken(1),
                                               atoi (token->GetToken(2)),
                                               atoi (token->GetToken(3)));
            }
            i ++;
        }
        Structean[i] = NULL;
        return FALSE;
}


BOOL SCANEAN::ReadEan128 (FILE *fp, char *buffer)
{
        int i = 0;

        while (fgets (buffer, 511, fp))
        {
            if (buffer[0] == '[')
            {
                Structean128[i] = NULL;
                return TRUE;
            }
            Token *token = new Token (buffer, " ");
            if (token == NULL)
            {
                continue;
            }
            int Tanz = token->GetAnzToken ();
            if (Tanz < 5)
            {
                continue;
            }

            if (Tanz == 5)
            {
                 Structean128[i] = new STRUCTEAN128 (atoi (token->GetToken(0)),
                                               atoi (token->GetToken(1)),
                                               atoi (token->GetToken(2)),
											   atoi (token->GetToken(3)),
											   atoi (token->GetToken(4)));
            }
            i ++;
        }
        Structean128[i] = NULL;
        return FALSE;
}

BOOL SCANEAN::ReadEan128Ai (FILE *fp, char *buffer)
{
        int i = 0;

        while (fgets (buffer, 511, fp))
        {
            if (buffer[0] == '[')
            {
                return TRUE;
            }
            Token *token = new Token (buffer, " ");
            if (token == NULL)
            {
                continue;
            }
            int Tanz = token->GetAnzToken ();
            if (Tanz < 2)
            {
                continue;
            }

		    Text Ai = token->NextToken ();
			int len = atoi (token->NextToken ());
			SetAi (Ai, len);
            if (Tanz > 2)
			{
				Text EndChar = token->NextToken ();
				SetAiEndChar (Ai,EndChar);
			}

            if (Tanz > 2)
			{
				Text EndCharEdit = token->NextToken ();
				SetAiEndCharEdit (Ai,EndCharEdit);
			}
			delete token;
        }
        return FALSE;
}

void SCANEAN::SetAi (Text& Ai, int len)
{
	    AiInfo *aiInfo;
		AiInfos.FirstPosition ();
		while ((aiInfo = (AiInfo *) AiInfos.GetNext ()) != NULL)
		{
			if (aiInfo->Ai == Ai)
			{
				aiInfo->Len = len;
				return;
			}
		}

	    aiInfo = new AiInfo (Ai, len);
	    AiInfos.Add (aiInfo);
}


void SCANEAN::SetAiEndChar (Text& Ai, Text& EndChar)
{
    AiInfo *aiInfo;
	AiInfos.FirstPosition ();
	while ((aiInfo = (AiInfo *) AiInfos.GetNext ()) != NULL)
	{
			if (aiInfo->Ai == Ai)
			{
				break;
			}
	}

	if (aiInfo == NULL)
	{
			aiInfo = new AiInfo (Ai, 0);
			AiInfos.Add (aiInfo);
	}

	Token t;
	t.SetSep (",");
	t = EndChar;

	if (t.GetAnzToken () == 0) return;

    CInteger *eInt = NULL;
	Text ec = t.NextToken ();
	Text ecs = ec.SubString (0,2);
	ecs.MakeUpper ();
	if (ecs == "0X")
	{
		eInt = new CInteger ();
		eInt->FromHex (ec);
	}
	else
	{
		eInt = new CInteger (ec);
	}

	aiInfo->EndChar.Add (eInt);
	if (aiInfo->EndCharEdit == 0)
	{
			aiInfo->EndCharEdit = eInt->IntValue ();
	}

    LPSTR c;
    while ((c = t.NextToken ()) != NULL)
	{
		ec = c;
	    ecs = ec.SubString (0,2);
	    ecs.MakeUpper ();
		if (ecs == "0X")
		{
			eInt = new CInteger ();
			eInt->FromHex (ec);
		}
		else
		{
			eInt = new CInteger (ec);
		}
		aiInfo->EndChar.Add (eInt);
	}
}

void SCANEAN::SetAiEndCharEdit (Text& Ai, Text& EndCharEdit)
{
    AiInfo *aiInfo;
	AiInfos.FirstPosition ();
	while ((aiInfo = (AiInfo *) AiInfos.GetNext ()) != NULL)
	{
			if (aiInfo->Ai == Ai)
			{
				break;
			}
	}
    aiInfo->EndCharEdit = atoi (EndCharEdit.GetBuffer ());
	if (aiInfo->EndCharEdit == 0)
	{
		aiInfo->EndCharEdit = (int) EndCharEdit.GetBuffer ()[0];
	}
}


BOOL SCANEAN::Read (void)
{
        char buffer [512];
        char *etc;

        if (CfgName.GetLen () == 0)
        {
            return FALSE;
        }

        etc = getenv ("BWSETC");
        if (etc == NULL)
        {
            return FALSE;
        }

        sprintf (buffer, "%s\\%s", etc, CfgName.GetBuffer ());

        FILE *fp = fopen (buffer, "r");
        if (fp == NULL)
        {
            return FALSE;
        }

        while (fgets (buffer, 511, fp))
        {
            if (memcmp (buffer, "[Standard]", strlen ("[Standard]")) == 0)
            {
                if (ReadStandard (fp, buffer) == FALSE)
                {
                    break;
                }
            }
            if (memcmp (buffer, "[EAN128]", strlen ("[EAN128]")) == 0)
            {
                if (ReadEan128 (fp, buffer) == NULL)
                {
                    break;
                }
            }
            if (memcmp (buffer, "[EAN128AI]", strlen ("[EAN128AI]")) == 0)
            {
                if (ReadEan128Ai (fp, buffer) == NULL)
                {
                    break;
                }
            }
        }
        fclose (fp);
        return TRUE;
}

int SCANEAN::GetType (Text& Ean)
{
        Text EanCode = Ean.SubString (0, 2);
        short Code = atoi (EanCode.GetBuffer ());
        for (int i = 0; Structean[i] != NULL; i ++)
        {
            if (Code == Structean[i]->GetCode ())
            {
                break;
            }
        }
        if (Structean[i] == NULL)
        {
             return EANSTD;
        }
        Text Type = Structean[i]->GetType ();
        switch ((Type.GetBuffer ())[0])
        {
             case  'G' :
                 return EANGEW;
             case 'P' :
                 return EANPR;
             default :
                 return EANSTD;
        }
}

STRUCTEAN *SCANEAN::GetStructean (Text& Ean)
{
        Ean.TrimLeft ();
        Text EanCode = Ean.SubString (0, 2);
        short Code = atoi (EanCode.GetBuffer ());
        for (int i = 0; Structean[i] != NULL; i ++)
        {
            if (Code == Structean[i]->GetCode ())
            {
                return Structean[i];
            }
        }
        return NULL;
}

STRUCTEAN128 *SCANEAN::GetStructean128 (Text& Ean)
{
        Ean.TrimLeft ();
        Text EanCode = Ean.SubString (0, 2);
        short Id = atoi (EanCode.GetBuffer ());
        for (int i = 0; Structean128[i] != NULL; i ++)
        {
            if (Id == Structean128[i]->GetId ())
            {
                return Structean128[i];
            }
        }
        EanCode = Ean.SubString (0, 3);
        Id = atoi (EanCode.GetBuffer ());
        for (i = 0; Structean128[i] != NULL; i ++)
        {
            if (Id == Structean128[i]->GetId ())
            {
                return Structean128[i];
            }
        }
        return NULL;
}

int SCANEAN::GetLen (Text& Ean)
{
        Text EanCode = Ean.SubString (0, 2);
        short Code = atoi (EanCode.GetBuffer ());
        for (int i = 0; Structean[i] != NULL; i ++)
        {
            if (Code == Structean[i]->GetCode ())
            {
                break;
            }
        }
        if (Structean[i] == NULL)
        {
             return 0;
        }
        return Structean[i]->GetLen ();
}

int SCANEAN::GetCode (Text& Ean)
{
        Text EanCode = Ean.SubString (0, 2);
        return atoi (EanCode.GetBuffer ());
}

char *SCANEAN::GetEanForDb (Text Ean)
{

        Text EanRest; 
        char format[10];

        int Len = GetLen (Ean);  
        if (Len == 0)
        {
            return Ean.GetBuffer ();
        }
        sprintf (format, "%c0%dd", '%', 10 - Len);
        EanDb = Ean.SubString (0, 2 + Len);
        EanRest.Format (format, 0);        
        EanDb + EanRest.GetBuffer ();;
        return EanDb.GetBuffer ();
}
        
char *SCANEAN::GetEanForDb (Text Ean, int len)
{

        Text EanRest; 
        char format[10];

        int Len = len;  
        if (Len == 0)
        {
            return Ean.GetBuffer ();
        }
        sprintf (format, "%c0%dd", '%', 10 - Len);
        EanDb = Ean.SubString (0, 2 + Len);
        EanRest.Format (format, 0);        
        EanDb + EanRest.GetBuffer ();;
        return EanDb.GetBuffer ();
}
        
char *SCANEAN::GetEanGew (Text Ean)
{
        if (Ean.GetLength () < 12) return NULL;

        EanGew = Ean.SubString (7, 5);
        return EanGew.GetBuffer ();
}

char *SCANEAN::GetEan128Varpart (Text& Ean128)
{
        STRUCTEAN128 *Ean;	    

		Ean = GetStructean128 (Ean128);
		if (Ean == NULL)
		{
			return NULL;
		}

        int idlen = (Ean->GetId () > 99) ? 3 : 2;
		int fixedlen = Ean->GetFixedlen ();
		int varlen   = Ean->GetVarlen ();

		Varpart = Ean128.SubString (idlen + fixedlen, varlen);
		return Varpart.GetBuffer ();
}

Text*SCANEAN::GetEan128Ai (Text& Code, Text& EAN128)
{
		char *c = Code.GetBuffer ();
		int len = Code.GetLen ();

		int Len128 = EAN128.GetLen ();
        char *e = EAN128.GetBuffer ();
		
        for  (int i = 0; i < Len128;)
		{
			AiInfos.FirstPosition ();
			AiInfo *aiInfo;
			while ((aiInfo = (AiInfo *) AiInfos.GetNext ()) != NULL)
			{
                 int aiLen = aiInfo->Ai.GetLen ();
				 if (memcmp (aiInfo->Ai.GetBuffer (), &e[i], aiLen) == 0)
				 {
			         if (memcmp (&e[i], c, len) == 0)
					 {
				               Text *Result = new Text ();
				               i += len; 
							   len = aiInfo->Len;
					           if (aiInfo->EndCharEdit != 0)
							   {
								   for (int j = 0; j < len; j ++)
								   {
									   int c = (int) EAN128.GetBuffer ()[i + j];
//									   if (c == aiInfo->EndCharEdit) break;

// 12.11.2007 Hier muss wahrscheinlich auch aiInfo->EndChar gepr�ft werden.

									   if (IsEndChar (aiInfo, (int) (unsigned char) c)) break; 
								   }
								   len = j;
							   }
							   *Result = EAN128.SubString (i, len);
				               return Result;
					 }
					 if (aiInfo->EndChar.GetCount () == 0)
					 {
						i += aiInfo->Len + aiLen;
					 }
					 else
					 {
						 i += aiLen;
						 for (int j = 0;j < aiInfo->Len && e[i] != 0; i ++, j ++)
						 {
							 if (i >= Len128) break;
							 if (IsEndChar (aiInfo, (int) (unsigned char) e[i])) 
							 {
								 i ++;
								 break;
							 }
						 }
						 if (j == aiInfo->Len)
						 {
							 if (e[i] != 0 && IsEndChar (aiInfo, (int) (unsigned char) e[i + 1]))
							 {
								 i ++;
							 }
						 }
					 }
					 break;
				 }
                 if (e[i] == 0) break;
			}
			if (aiInfo == NULL)
			{
				return NULL;
			}
		}
		return NULL;
}

BOOL SCANEAN::IsEndChar (AiInfo *ai, int c)
{
	if (c == ai->EndCharEdit)
	{
		return TRUE;
	}

	CInteger Key (c);
	ai->EndChar.FirstPosition ();
    CInteger *endKey;
	while ((endKey = (CInteger *) ai->EndChar.GetNext ()) != NULL)
	{
		if (Key == *endKey)
		{
			return TRUE;
		}

	}

	return FALSE;
}


CEan128Date::CEan128Date ()
{
		Init ();
}


CEan128Date::CEan128Date (Text& date)
{
         SetDate (date);
}


CEan128Date& CEan128Date::operator= (Text &date)
{
	     SetDate (date);
		 return *this;
}

void CEan128Date::SetDate (Text& date)
{
			char day [3] = "01";
			char month [3] = "01";
			char year [3] = "00";

			memcpy (year, &date.GetBuffer ()[0], 2);
			memcpy (month, &date.GetBuffer ()[2], 2);
			memcpy (day, &date.GetBuffer ()[4], 2);
			Day = atoi (day);
			Month = atoi (month);
			Year = atoi (year);
			Year = (Year < 90) ? Year + 2000 : Year + 1900;
}

CEan128Date::~CEan128Date () {}

void CEan128Date::Init ()
{
	    Day = Month = Year = 0;
		Date = "";
}

BOOL CEan128Date::IsEmpty ()
{
	    if (Day == 0)
		{
			return TRUE;
		}
		return FALSE;
}

Text& CEan128Date::ToString ()
{
	    if (Day != 0)
		{
			Date.Format ("%02d.%02d.%04d", Day, Month, Year);
		}
		return Date;
}

void STRUCTEAN128::SetEndChar (Text eChar)
{
	Token t;
	t.SetSep (",");
	t = eChar;

	if (t.GetAnzToken () == 0) return;

    CInteger *eInt = NULL;
	Text ec = t.NextToken ();
	Text ecs = ec.SubString (0,2);
	ecs.MakeUpper ();
	if (ecs == "0X")
	{
		eInt = new CInteger ();
		eInt->FromHex (ec);
	}
	else
	{
		eInt = new CInteger (ec);
	}

	EndChar.Add (eInt);
	if (EndCharEdit == 0)
	{
			EndCharEdit = eInt->IntValue ();
	}

    LPSTR c;
    while ((c = t.NextToken ()) != NULL)
	{
		ec = c;
	    ecs = ec.SubString (0,2);
	    ecs.MakeUpper ();
		if (ecs == "0X")
		{
			eInt = new CInteger ();
			eInt->FromHex (ec);
		}
		else
		{
			eInt = new CInteger (ec);
		}
		EndChar.Add (eInt);
	}
}


// RoW 22.02.2005 Ean128 mit Endezeichen

SCANEAN *SCANEAN::Scanean = NULL;

int SCANEAN::ChangeChar (int key)
{
	CInteger Key (key);
	AiInfos.FirstPosition ();
    AiInfo *ai;
	while ((ai = (AiInfo *) AiInfos.GetNext ()) != NULL)
	{
		ai->EndChar.FirstPosition ();
		CInteger *ec;
		while ((ec = (CInteger *) ai->EndChar.GetNext ()) != NULL)
		{
			if (Key == *ec)
			{
				return ai->EndCharEdit;
			}
		}
	}
	return key;
}