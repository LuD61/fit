/*
 * Copyright (c) 2004 Wilhelm Roth, 
 * Company  SETEC-GMBH Inc. All  Rights Reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * -Redistributions of source code must retain the above copyright
 *  notice, this list of conditions and the following disclaimer.
 * 
 * -Redistribution in binary form must reproduct the above copyright
 *  notice, this list of conditions and the following disclaimer in
 *  the documentation and/or other materials provided with the distribution.
 * 
 * Neither the name of SETEC-GMBH, Inc. or the names of contributors
 * may be used to endorse or promote products derived from this software
 * without specific prior written permission.
 * 
 * This software is provided "AS IS," without a warranty of any kind. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING
 * ANY IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
 * OR NON-INFRINGEMENT, ARE HEREBY EXCLUDED. SUN AND ITS LICENSORS SHALL NOT
 * BE LIABLE FOR ANY DAMAGES OR LIABILITIES SUFFERED BY LICENSEE AS A RESULT
 * OF OR RELATING TO USE, MODIFICATION OR DISTRIBUTION OF THE SOFTWARE OR ITS
 * DERIVATIVES. IN NO EVENT WILL SUN OR ITS LICENSORS BE LIABLE FOR ANY LOST
 * REVENUE, PROFIT OR DATA, OR FOR DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL,
 * INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER CAUSED AND REGARDLESS OF THE THEORY
 * OF LIABILITY, ARISING OUT OF THE USE OF OR INABILITY TO USE SOFTWARE, EVEN
 * IF SUN HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * 
 * You acknowledge that Software is not designed, licensed or intended for
 * use in the design, construction, operation or maintenance of any nuclear
 * facility.
 */

#define CONSOLE

#include <windows.h>
#include <stdio.h>
#include "DbTime.h"
#include "DbClass.h"


DayTime::DayTime (char *t)
{
	     Create (t);
}

DayTime::DayTime (CString *t)
{
	     Create (t);
}

DayTime *DayTime::Create (char *t)
{
	     CString txt (t);
	     Create (&txt);
		 return this;
}

DayTime *DayTime::Create (CString *t)
{
	     char hour[] = {"00"};
	     char min[]  = {"00"};
	     char sec[]  = {"00"};
         Time = *t;
		 int len = t->GetLength ();
		 if (len >=2)
		 {
			 memcpy (hour, Time.Mid (0, 2), 2);
		 }
		 if (len >=5)
		 {
			 memcpy (min, Time.Mid (3, 2), 2);
		 }
		 if (len >=8)
		 {
			 memcpy (sec, Time.Mid (6, 2), 2);
		 }
/*
		 lTime = atoi (hour) * 24 * 3600 + 
			     atoi (min) * 3600 +
				 atoi (sec);
*/
		 lTime = atoi (hour) * 3600 + 
			     atoi (min)  * 60 +
				 atoi (sec);
		 return this;
}

long DayTime::GetLongTime ()
{
	     return lTime;
}

CString *DayTime::GetTextTime ()
{
	     return &Time;
}









void DbTime::ToDbDate (CString& Date, DATE_STRUCT *DbDate)
{
	TCHAR date[12];

    strcpy (date, Date.GetBuffer ());
	FromGerDate (DbDate, date);
}

void DbTime::FromDbDate (CString& Date, DATE_STRUCT *DbDate)
{
	TCHAR date[12];

	if (DbDate->year <= 1900 &&
        DbDate->month <= 1 &&
        DbDate->day <= 1)
	{
		Date = "";
	}
	else
	{
		ToGerDate (DbDate, date);
		Date = date;
	}
}

int DbTime::FromRecDate (DATE_STRUCT *sqldate, LPTSTR cdate)
{
//	   int anz;
       char day [3];
       char month [3];
       char year [5];
	   int len;

	   sqldate->day = 1; 
	   sqldate->month = 1; 
	   sqldate->year = 1900; 

	   len = (int) strlen (cdate);
	   if (len < 6)
	   {
                      return (0);
	   }
       strcpy (day,   "01"); 
       strcpy (month, "01"); 
       strcpy (year,  "1900"); 
	   memcpy (day, &cdate[0], 2);
	   memcpy (month, &cdate[2], 2);
	   if (len == 8)
	   {
         	   memcpy (year, &cdate[4], 4);
	   }
	   else if (len == 6)
	   {
		       memcpy (&year[2], &cdate[4], 2);
			   if (atoi (&year[2]) < 80)
			   {
				   memcpy (year, "20", 2);
			   }
	   }


	   sqldate->day = atoi (day); 
	   sqldate->month = atoi (month); 
	   sqldate->year = atoi (year);
	   return (0);
}
		

int DbTime::FromOdbcDate (DATE_STRUCT *sqldate, LPTSTR cdate)
{
	   int anz;
//           char ndate [12];

	   sqldate->day = 1; 
	   sqldate->month = 1; 
	   sqldate->year = 1900; 
	   Token t;
	   t.SetSep ("-");
	   t = cdate;
	   anz = t.GetAnzToken ();
	   if (anz < 3)
	   {
                      return FromRecDate (sqldate, cdate);
	   }

	   sqldate->day = atoi (t.GetToken (0)); 
	   sqldate->month = atoi (t.GetToken (1)); 
	   sqldate->year = atoi (t.GetToken (2));
	   return (0);
}
		
int DbTime::FromGerDate (DATE_STRUCT *sqldate, LPTSTR cdate)
{
	   int anz;
	   sqldate->day = 1; 
	   sqldate->month = 1; 
	   sqldate->year = 1900; 
	   Token t;
	   t.SetSep (".");
	   t = cdate;
	   anz = t.GetAnzToken ();
	   if (anz < 3)
	   {
               return (FromOdbcDate (sqldate, cdate));
	   }

	   sqldate->day = atoi (t.GetToken (0)); 
	   sqldate->month = atoi (t.GetToken (1)); 
	   sqldate->year = atoi (t.GetToken (2));
	   return (0);
}

int DbTime::ToGerDate (DATE_STRUCT *sqldate, LPTSTR cdate)
{
	   sprintf (cdate, "%02hd.%02hd.%02hd", sqldate->day,
											  sqldate->month,
											  sqldate->year);
	   return (0);
}

int DbTime::FromOdbcTime (TIME_STRUCT *sqltime, LPTSTR ctime)
{
	   int anz;

	   sqltime->hour = 0; 
	   sqltime->minute = 0; 
	   sqltime->second = 0; 
	   Token t;
	   t.SetSep ("-");
	   t = ctime;
	   anz = t.GetAnzToken ();
	   if (anz < 3)
	   {
               return (0);
	   }

	   sqltime->hour = atoi (t.GetToken (2)); 
	   sqltime->minute = atoi (t.GetToken (1)); 
	   sqltime->second = atoi (t.GetToken (0));
	   return (0);
}

int DbTime::FromGerTime (TIME_STRUCT *sqltime, LPTSTR ctime)
{
	   int anz;

	   sqltime->hour = 0; 
	   sqltime->minute = 0; 
	   sqltime->second = 0; 
	   Token t;
	   t.SetSep (":");
	   t = ctime;
	   anz = t.GetAnzToken ();
	   if (anz < 3)
	   {
               return (FromOdbcTime (sqltime, ctime));
	   }

	   sqltime->hour   = atoi (t.GetToken (0)); 
	   sqltime->minute = atoi (t.GetToken (1)); 
	   sqltime->second = atoi (t.GetToken (2));
	   return (0);
}

int DbTime::ToGerTime (TIME_STRUCT *sqltime, LPTSTR ctime)
{
	   sprintf (ctime, "%02hd:%02hd:%02hd", sqltime->hour,
											  sqltime->minute,
											  sqltime->second);
	   return (0);
}

int DbTime::FromOdbcTimestamp (TIMESTAMP_STRUCT *sqltime, LPTSTR ctime)
{
       DATE_STRUCT dt;
       TIME_STRUCT tm; 

	   sqltime->day = 0; 
	   sqltime->month = 0; 
	   sqltime->year = 0; 
	   sqltime->hour = 0; 
	   sqltime->minute = 0; 
	   sqltime->second = 0; 
           if (strlen (ctime) < 19)
           {
                    return (0);
           }
           FromOdbcDate (&dt, ctime);
           FromOdbcTime (&tm, &ctime[11]);

	   sqltime->day    = dt.day; 
	   sqltime->month  = dt.month; 
	   sqltime->year   = dt.year;
	   sqltime->hour   = tm.hour; 
	   sqltime->minute = tm.minute; 
	   sqltime->second = tm.second;
	   return (0);
}

int DbTime::FromGerTimestamp (TIMESTAMP_STRUCT *sqltime, LPTSTR ctime)
{

       DATE_STRUCT dt;
       TIME_STRUCT tm; 

	   sqltime->day = 0; 
	   sqltime->month = 0; 
	   sqltime->year = 0; 
	   sqltime->hour = 0; 
	   sqltime->minute = 0; 
	   sqltime->second = 0; 
           FromGerDate (&dt, ctime);
	   sqltime->day    = dt.day; 
	   sqltime->month  = dt.month; 
	   sqltime->year    = dt.year;
           if (strlen (ctime) < 11)
           {
                return (0);
           }
           FromGerTime (&tm, &ctime[10]);

	   sqltime->hour   = tm.hour; 
	   sqltime->minute = tm.minute; 
	   sqltime->second = tm.second;
	   return (0);
}

int DbTime::ToGerTimestamp (TIMESTAMP_STRUCT *sqltime, LPTSTR ctime)
{
	   sprintf (ctime, "%02hd.%02hd.%02hd %02hd:%02hd:%02hd ",
                                            sqltime->day,
		                                    sqltime->month,
											sqltime->year,
                                            sqltime->hour,
		                                    sqltime->minute,
											sqltime->second);
	   return (0);
}

int DbTime::CompareDate (DATE_STRUCT *Date1, DATE_STRUCT *Date2)
{
	DbTime d1 (Date1);
	DbTime d2 (Date2);

	time_t t1 = d1.GetTime ();
	time_t t2 = d2.GetTime ();
	if (t1 > t2) return 1;
	if (t1 < t2) return -1;
	return 0;
}


DbTime::DbTime ()
{
		 time_t timer;
		 struct tm *ltime;

		 time (&timer);
		 ltime = localtime (&timer);
		 ds.year = ltime->tm_year + 1900;
		 ds.month = ltime->tm_mon + 1;
 		 ds.day   = ltime->tm_mday;
		 st.wYear  = ds.year;
		 st.wMonth = ds.month;
		 st.wDay   = ds.day;
		 st.wHour  = 0;
		 st.wMinute = 0;
		 st.wSecond = 0;
		 st.wMilliseconds = 0;
		 tDate.Format ("%02hd.%02hd.%04hd", st.wDay, st.wMonth, st.wYear);
		 SystemTimeToFileTime (&st, &ft);
		 FileTimeToSystemTime (&ft, &st);
		 memcpy (&ul, &ft, sizeof (ft));
		 ldat = (time_t) (ul.QuadPart / (1000 * 3600 * 24));
		 ldat /= 10000;
}


DbTime::DbTime (char *date)
{
		 ToDbDate (CString (date), &ds);
	     if (IsNull ())
		 {
			 ldat = 0l;
			 memset (&st, 0, sizeof (SYSTEMTIME));
			 memset (&ft, 0, sizeof (FILETIME));
			 memset (&ul, 0, sizeof (ULARGE_INTEGER));
		 }
		 else
		 {
			 st.wYear  = ds.year;
			 st.wMonth = ds.month;
			 st.wDay   = ds.day;
			 st.wHour  = 0;
			 st.wMinute = 0;
			 st.wSecond = 0;
			 st.wMilliseconds = 0;
			 tDate.Format ("%02hd.%02hd.%04hd", st.wDay, st.wMonth, st.wYear);
			 SystemTimeToFileTime (&st, &ft);
			 FileTimeToSystemTime (&ft, &st);
			 memcpy (&ul, &ft, sizeof (ft));
			 ldat = (time_t) (ul.QuadPart / (1000 * 3600 * 24));
			 ldat /= 10000;
		 }
}


DbTime::DbTime (DATE_STRUCT *ds)
{
	     memcpy (&this->ds, ds, sizeof (DATE_STRUCT));  
		 if (IsNull ())
		 {
			 ldat = 0l;
			 memset (&st, 0, sizeof (SYSTEMTIME));
			 memset (&ft, 0, sizeof (FILETIME));
			 memset (&ul, 0, sizeof (ULARGE_INTEGER));
		 }
		 else
		 {
			 st.wYear  = ds->year;
			 st.wMonth = ds->month;
			 st.wDay   = ds->day;
			 st.wHour  = 0;
			 st.wMinute = 0;
			 st.wSecond = 0;
			 st.wMilliseconds = 0;
			 tDate.Format ("%02hd.%02hd.%04hd", st.wDay, st.wMonth, st.wYear);
			 SystemTimeToFileTime (&st, &ft);
			 FileTimeToSystemTime (&ft, &st);
			 memcpy (&ul, &ft, sizeof (ft));
			 ldat = (time_t) (ul.QuadPart / (1000 * 3600 * 24));
			 ldat /= 10000;
		 }
}

DbTime::DbTime (long ldate)
{
	SetTime ((time_t) ldate);
}

BOOL DbTime::IsNull ()
{
	if (ds.year == 0 || ds.month == 0 || ds.day == 0)
	{
		return TRUE;
	}
	return FALSE;
}

time_t DbTime::GetTime ()
{
	     return ldat;
}

int DbTime::GetWeekDay ()
{
	     return st.wDayOfWeek;
}

CString* DbTime::GetStringTime ()
{
	     return &tDate;
}

void DbTime::SetDateStruct ()
{
	     ds.year  = st.wYear;
	     ds.month = st.wMonth;
	     ds.day   = st.wDay;
}

DATE_STRUCT *DbTime::GetDateStruct (DATE_STRUCT *d)
{
	     memcpy (d, &ds, sizeof (ds));
         return d;
}

void DbTime::SetTime (time_t t)
{
	     ldat = t;
		 ldat *= 10000;
		 ul.QuadPart = ldat;
		 ul.QuadPart *= (1000 * 3600 * 24);
         memcpy (&ft, &ul,  sizeof (ft));
	     FileTimeToSystemTime (&ft, &st);
		 tDate.Format ("%02hd.%02hd.%04hd", st.wDay, st.wMonth, st.wYear + 100);
		 SetDateStruct ();
}

#ifdef WIN32
DbTime::operator time_t () const
{
		 return ldat;
}

DbTime::operator DATE_STRUCT () const
{
		 return ds;
}

DbTime::operator LPSTR () const
{
		 return (LPSTR) tDate;
}

BOOL DbTime::operator== (long time)
{
		 return (time == ldat);
}

BOOL DbTime::operator== (LPSTR pdate)
{
		 return (tDate == CString (pdate));
}

BOOL DbTime::operator<= (long time)
{
		 return ldat <= time;
}

BOOL DbTime::operator<= (LPSTR pdate)
{
	     DbTime d (pdate);
		 time_t time = d; 
		 return ldat <= time;
}

DbTime& DbTime::operator+ (int d)
{
	     time_t t1 = GetTime ();
         t1 += d;
		 SetTime (t1);
         memcpy (&ft, &ul, sizeof (ft));
	     FileTimeToSystemTime (&ft, &st);
		 tDate.Format ("%02hd.%02hd.%04hd", st.wDay, st.wMonth, st.wYear);
		 SetDateStruct ();
         return *this;
}

void DbTime::operator+= (int d)
{
	     time_t t1 = GetTime ();
         t1 += d;
		 SetTime (t1);
         memcpy (&ft, &ul, sizeof (ft));
	     FileTimeToSystemTime (&ft, &st);
		 tDate.Format ("%02hd.%02hd.%04hd", st.wDay, st.wMonth, st.wYear);
		 SetDateStruct ();
}

DbTime& DbTime::operator- (int d)
{
	     time_t t1 = GetTime ();
         t1 -= d;
		 SetTime (t1);
         memcpy (&ft, &ul, sizeof (ft));
	     FileTimeToSystemTime (&ft, &st);
		 tDate.Format ("%02hd.%02hd.%04hd", st.wDay, st.wMonth, st.wYear);
		 SetDateStruct ();
         return *this;
}

void DbTime::operator-= (int d)
{
	     time_t t1 = GetTime ();
         t1 -= d;
		 SetTime (t1);
         memcpy (&ft, &ul, sizeof (ft));
	     FileTimeToSystemTime (&ft, &st);
		 tDate.Format ("%02hd.%02hd.%04hd", st.wDay, st.wMonth, st.wYear);
		 SetDateStruct ();
}
#endif

DbTime* DbTime::Add (int d)
{
	     time_t t1 = GetTime ();
         t1 += d;
		 SetTime (t1);
         memcpy (&ft, &ul, sizeof (ft));
	     FileTimeToSystemTime (&ft, &st);
		 tDate.Format ("%02hd.%02hd.%04hd", st.wDay, st.wMonth, st.wYear);
		 SetDateStruct ();
         return this;
}

DbTime* DbTime::Sub (int d)
{
	     time_t t1 = GetTime ();
         t1 -= d;
		 SetTime (t1);
         memcpy (&ft, &ul, sizeof (ft));
	     FileTimeToSystemTime (&ft, &st);
		 tDate.Format ("%02hd.%02hd.%04hd", st.wDay, st.wMonth, st.wYear);
		 SetDateStruct ();
         return this;
}

long DbTime::ParseLong (DATE_STRUCT *date)
{
	DbTime t (date);
	return (long) t.GetTime ();
}

void DbTime::ToDateStruct (DATE_STRUCT *date, long ldate)
{
	DbTime t (ldate);
	t.GetDateStruct (date);
}

void DbTime::TestDateValue (DATE_STRUCT *date)
{
	long ldat;

	DbTime t (date);
	ldat = t.GetTime ();
	if (ldat <= 0)
	{
		date->year = 1900;
		date->month = 1;
		date->day = 1;
	}
}

