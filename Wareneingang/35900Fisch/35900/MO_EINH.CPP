#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#ifndef CONSOLE
#include "wmask.h"
#include "mo_meld.h"
#endif
#include "mo_curso.h"
#include "strfkt.h"
#include "a_bas.h"
#include "a_hndw.h"
#include "a_kun.h"
#include "kun.h"
#include "ptab.h"
#include "mo_einh.h"
#include "kumebest.h"

static KUN_CLASS kun_class;
static HNDW_CLASS hndw_class;
static PTAB_CLASS ptab_class;
static KMB_CLASS kmb_class;

void EINH_CLASS::GetBasEinh (double a, KEINHEIT *keinheit)
/**
Basis-Einheit aus a_bas holen.
**/
{
        char wert [5];

        keinheit->me_einh_bas = _a_bas.me_einh;
        memcpy (&ptabn, &ptabn_null, sizeof (struct PTABN));
        sprintf (wert, "%hd", _a_bas.me_einh);

        dsqlstatus = ptab_class.lese_ptab ("me_einh", wert);

        if (dsqlstatus == 0)
        {
                   strcpy (keinheit->me_einh_bas_bez, ptabn.ptbezk);
        }
}
        
void EINH_CLASS::GetKunEinhBas (double a, KEINHEIT *keinheit)
/**
Kunden-Einheit aus a_hndw, a_eig oder a_eig_div holen.
**/
{
         char wert [5];

         switch (_a_bas.a_typ)
         {
                     case 1 :
                           dsqlstatus = hndw_class.lese_a_hndw (a);
                           break;
                     case 2 :
                           dsqlstatus = hndw_class.lese_a_eig (a);
                           break;
                     case 3 :
                           dsqlstatus = hndw_class.lese_a_eig_div (a);
                           break;
                     default :
                           return;
         }

         if (dsqlstatus)
         {
                     return;
         }

         switch (_a_bas.a_typ)
         {
                     case 2 :
                           a_hndw.me_einh_kun = a_eig.me_einh_ek;
                           a_hndw.inh         = a_eig.inh;
                           break;
                     case 3 :
                           a_hndw.me_einh_kun = a_eig_div.me_einh_ek;
                           a_hndw.inh         = a_eig_div.inh;
                           break;
          }

          keinheit->me_einh_kun = a_hndw.me_einh_kun;
          keinheit->inh      = a_hndw.inh;

          memcpy (&ptabn, &ptabn_null, sizeof (struct PTABN));
          sprintf (wert, "%hd", a_hndw.me_einh_kun);

          dsqlstatus = ptab_class.lese_ptab ("me_einh_kun", wert);

          if (dsqlstatus == 0)
          {
                   strcpy (keinheit->me_einh_kun_bez, ptabn.ptbezk);
          }
}
 
int EINH_CLASS::ReadKmb (void)
/**
Combofelder fuellen.
**/
{
        int dsqlstatus;


        dsqlstatus = kmb_class.dbreadfirst ();
        if (dsqlstatus != 0 && kumebest.kun > 0)
        {
            dsqlstatus = kmb_class.dbreadfirst_bra ();
        }
        if (dsqlstatus != 0)
        {
            dsqlstatus = kmb_class.dbreadfirst_a ();
        }
        return dsqlstatus;
}

double EINH_CLASS::GetInh (int pos, double inh0)
/**
Inhalt in Basiseinheit ermitteln.
**/
{
        double inh;

        switch (pos)
        {
        case 0 :
                return inh0;
        case 1 :
                if (kumebest.knuepf1 == 1) return inh0; 
                inh = kumebest.inh1 * inh0;
                return GetInh (kumebest.knuepf1, inh);
        case 2 :
                if (kumebest.knuepf2 == 2) return inh0; 
                inh = kumebest.inh2 * inh0;
                return GetInh (kumebest.knuepf2, inh);
        case 3 :
                if (kumebest.knuepf3 == 3) return inh0; 
                inh = kumebest.inh3 * inh0;
                return GetInh (kumebest.knuepf3, inh);
        case 4 :
                if (kumebest.knuepf4 == 4) return inh0; 
                inh = kumebest.inh4 * inh0;
                return GetInh (kumebest.knuepf4, inh);
        case 5 :
                if (kumebest.knuepf5 == 5) return inh0; 
                inh = kumebest.inh5 * inh0;
                return GetInh (kumebest.knuepf5, inh);
        case 6 :
                if (kumebest.knuepf6 == 6) return inh0; 
                inh = kumebest.inh6 * inh0;
                return GetInh (kumebest.knuepf6, inh);
        }
        return inh0;
}


void EINH_CLASS::FillKmb (KEINHEIT *keinheit)
/**
Einheit aus kumebest fuellen.
**/
{
        char wert [5];

        switch (AufEinh)
        {
             case 0 :
                   keinheit->me_einh_kun = kumebest.me_einh0;
                   keinheit->inh      = kumebest.inh0;
                   sprintf (wert, "%hd", kumebest.me_einh0);
                   break;
             case 1 :
                   keinheit->me_einh_kun = kumebest.me_einh1;
                   keinheit->inh      = GetInh (kumebest.knuepf1, 
                                                kumebest.inh1);
                   sprintf (wert, "%hd", kumebest.me_einh1);
                   break;
             case 2 :
                   keinheit->me_einh_kun = kumebest.me_einh2;
                   keinheit->inh      = GetInh (kumebest.knuepf2, 
                                                kumebest.inh2);
                   sprintf (wert, "%hd", kumebest.me_einh2);
                   break;
             case 3 :
                   keinheit->me_einh_kun = kumebest.me_einh3;
                   keinheit->inh      = GetInh (kumebest.knuepf3, 
                                                kumebest.inh3);
                   sprintf (wert, "%hd", kumebest.me_einh3);
                   break;
             case 4 :
                   keinheit->me_einh_kun = kumebest.me_einh4;
                   keinheit->inh      = GetInh (kumebest.knuepf4, 
                                                kumebest.inh4);
                   sprintf (wert, "%hd", kumebest.me_einh4);
                   break;
             case 5 :
                   keinheit->me_einh_kun = kumebest.me_einh5;
                   keinheit->inh      = GetInh (kumebest.knuepf5, 
                                                kumebest.inh5);
                   sprintf (wert, "%hd", kumebest.me_einh5);
                   break;
             case 6 :
                   keinheit->me_einh_kun = kumebest.me_einh6;
                   keinheit->inh      = GetInh (kumebest.knuepf6, 
                                                kumebest.inh6);
                   sprintf (wert, "%hd", kumebest.me_einh6);
                   break;
        }

        memcpy (&ptabn, &ptabn_null, sizeof (struct PTABN));
        dsqlstatus = ptab_class.lese_ptab ("me_einh_kun", wert);
        if (dsqlstatus == 0)
        {
                    strcpy (keinheit->me_einh_kun_bez,
                                      ptabn.ptbezk);
        }
}
         
void EINH_CLASS::AktAufEinh (short mdn, short fil, long kun_nr,
                             double a, int me_einh_kun)
/**
Mengeneinheit als Klartext fuer Kunde und Artikel holen.
**/
{

        kumebest.mdn = mdn;
        kumebest.fil = fil;
        kumebest.kun = kun_nr;
        kumebest.a   = a;
        kun_class.lese_kun (mdn, fil, kun_nr); 

        if (ReadKmb () == 0)
        {
                if (kumebest.me_einh0 == me_einh_kun)
                {
                    AufEinh = 0;
                }
                else if (kumebest.me_einh1 == me_einh_kun)
                {
                    AufEinh = 1;
                }
                else if (kumebest.me_einh2 == me_einh_kun)
                {
                    AufEinh = 2;
                }
                else if (kumebest.me_einh3 == me_einh_kun)
                {
                    AufEinh = 3;
                }
                else if (kumebest.me_einh4 == me_einh_kun)
                {
                    AufEinh = 4;
                }
                else if (kumebest.me_einh5 == me_einh_kun)
                {
                    AufEinh = 5;
                }
                else if (kumebest.me_einh6 == me_einh_kun)
                {
                    AufEinh = 6;
                }
                else
                {
                    AufEinh = 1;
                }
                return;
        }
        dsqlstatus = kun_class.lese_a_kun (mdn, fil, kun_nr, a);
        if (dsqlstatus == 0)
        {
                  if (me_einh_kun == a_kun.me_einh_kun)
                  {
                            AufEinh = 1;
                  }
                  else
                  {
                            AufEinh = 0;
                  }
                  return;

        }
        dsqlstatus = kun_class.lese_kun (mdn, fil, kun_nr);
        if (dsqlstatus == 0 && strcmp (kun.kun_bran2, "0") > 0)
        {
                 dsqlstatus = kun_class.lese_a_kun_bran (mdn, fil,
                                                kun.kun_bran2,
                                                a);
                   
                 if (dsqlstatus == 0)
                 {
                       if (me_einh_kun == a_kun.me_einh_kun)
                       {
                            AufEinh = 1;
                       }
                       else
                       {
                            AufEinh = 0;
                       }
                       return;
                 }
        }
        AufEinh = 0;
        dsqlstatus = lese_a_bas (a);
        if (dsqlstatus)
        {
                     return;
        }
        switch (_a_bas.a_typ)
         {
                     case 1 :
                           dsqlstatus = hndw_class.lese_a_hndw (a);
                           break;
                     case 2 :
                           dsqlstatus = hndw_class.lese_a_eig (a);
                           break;
                     case 3 :
                           dsqlstatus = hndw_class.lese_a_eig_div (a);
                           break;
                     default :
                           return;
         }

         switch (_a_bas.a_typ)
         {
                     case 2 :
                           a_hndw.me_einh_kun = a_eig.me_einh_ek;
                           break;
                     case 3 :
                           a_hndw.me_einh_kun = a_eig_div.me_einh_ek;
                           break;
         }
         if (me_einh_kun == a_hndw.me_einh_kun)
         {
                     AufEinh = 1;
         }
}

void EINH_CLASS::GetKunEinh (short mdn, short fil, long kun_nr,
                            double a, KEINHEIT *keinheit)
/**
Mengeneinheit als Klartext fuer Kunde und Artikel holen.
**/
{
        char wert [5];

        kumebest.mdn = mdn;
        kumebest.fil = fil;
        kumebest.kun = kun_nr;
        kumebest.a   = a;
        kun_class.lese_kun (mdn, fil, kun_nr); 

        if (ReadKmb () == 0)
        {
                  GetBasEinh (a, keinheit);
                  FillKmb (keinheit);
                  return;
        }

        dsqlstatus = lese_a_bas (a);
        dsqlstatus = kun_class.lese_a_kun (mdn, fil, kun_nr, a);
        if (dsqlstatus == 0)
        {
                  keinheit->me_einh_kun = a_kun.me_einh_kun;
                  keinheit->inh      = a_kun.inh;
                  memcpy (&ptabn, &ptabn_null, sizeof (struct PTABN));
                  sprintf (wert, "%hd", a_kun.me_einh_kun);

                  dsqlstatus = ptab_class.lese_ptab ("me_einh_kun",
                                           wert);

                  if (dsqlstatus == 0)
                  {
                             strcpy (keinheit->me_einh_kun_bez,
                                     ptabn.ptbezk);
                  }
                  GetBasEinh (a, keinheit);
                  if (AufEinh == 0)
                  {
                              keinheit->me_einh_kun = keinheit->me_einh_bas;
                              strcpy (keinheit->me_einh_kun_bez,
                                      keinheit->me_einh_bas_bez); 
                  }
                  return;
        }

        dsqlstatus = kun_class.lese_kun (mdn, fil, kun_nr);
        if (dsqlstatus || strcmp (kun.kun_bran2, "  ") <= 0)
        {
                   GetKunEinhBas (a, keinheit);
                   GetBasEinh (a, keinheit);
                   if (AufEinh == 0)
                   {
                              keinheit->me_einh_kun = keinheit->me_einh_bas;
                              strcpy (keinheit->me_einh_kun_bez,
                                      keinheit->me_einh_bas_bez); 
                   }
                   return;
        }
        dsqlstatus = kun_class.lese_a_kun_bran (mdn, fil,
                                                kun.kun_bran2,
                                                a);
                   
        if (dsqlstatus == 0)
        {
                  keinheit->me_einh_kun = a_kun.me_einh_kun;
                  keinheit->inh      = a_kun.inh;
                  memcpy (&ptabn, &ptabn_null, sizeof (struct PTABN));
                  sprintf (wert, "%hd", a_kun.me_einh_kun);

                  dsqlstatus = ptab_class.lese_ptab ("me_einh_kun",
                                           wert);

                  if (dsqlstatus == 0)
                  {
                             strcpy (keinheit->me_einh_kun_bez,
                                     ptabn.ptbezk);
                  }
                  GetBasEinh (a, keinheit);
                  if (AufEinh == 0)
                  {
                              keinheit->me_einh_kun = keinheit->me_einh_bas;
                              strcpy (keinheit->me_einh_kun_bez,
                                      keinheit->me_einh_bas_bez); 
                  }
                  return;
        }
        GetKunEinhBas (a, keinheit);
        GetBasEinh (a, keinheit);
        if (AufEinh == 0)
        {
                  keinheit->me_einh_kun = keinheit->me_einh_bas;
                  strcpy (keinheit->me_einh_kun_bez,
                          keinheit->me_einh_bas_bez); 
        }
}                                   


