#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmask.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "dbclass.h"
#include "sys_peri.h"
#include "dbfunc.h"

struct SYS_PERI sys_peri, sys_peri_null;
struct DAT_ABS dat_abs, dat_abs_null;
struct KONFIG_TAG konfig_tag, konfig_tag_null;
struct OPT_CLS opt_cls, opt_cls_null;
struct ABS_STAT abs_stat, abs_stat_null;

void SYS_PERI_CLASS::out_quest_all  (void)
{
    out_quest ((char *) &sys_peri.anschluss,1,0);
    out_quest ((char *) &sys_peri.anz_waehl_wied,1,0);
    out_quest ((char *) &sys_peri.auszeit,1,0);
    out_quest ((char *) &sys_peri.baudrate,1,0);
    out_quest ((char *) &sys_peri.dat_abruf,2,0);
    out_quest ((char *) &sys_peri.dat_update,2,0);
    out_quest ((char *) &sys_peri.daten_bit,1,0);
    out_quest ((char *) &sys_peri.drv_pid,2,0);
    out_quest ((char *) &sys_peri.fil,1,0);
    out_quest ((char *) &sys_peri.kanal,1,0);
    out_quest ((char *) &sys_peri.mdn,1,0);
    out_quest ((char *) sys_peri.modem_typ,0,3);
    out_quest ((char *) sys_peri.parity,0,2);
    out_quest ((char *) sys_peri.peri_nam,0,21);
    out_quest ((char *) &sys_peri.peri_typ,1,0);
    out_quest ((char *) &sys_peri.port_typ,1,0);
    out_quest ((char *) &sys_peri.protokoll,1,0);
    out_quest ((char *) &sys_peri.retry_max,1,0);
    out_quest ((char *) &sys_peri.stat,1,0);
    out_quest ((char *) &sys_peri.status_abruf,1,0);
    out_quest ((char *) &sys_peri.stop_bit,1,0);
    out_quest ((char *) &sys_peri.sys,2,0);
    out_quest ((char *) &sys_peri.sys_abruf,1,0);
    out_quest ((char *) &sys_peri.sys_update,1,0);
    out_quest ((char *) sys_peri.tel,0,17);
    out_quest ((char *) sys_peri.term,0,13);
    out_quest ((char *) sys_peri.txt,0,61);
    out_quest ((char *) sys_peri.zeit_abruf,0,7);
    out_quest ((char *) sys_peri.b_check,0,2);
    out_quest ((char *) &sys_peri.la_zahl,1,0);
}


void SYS_PERI_CLASS::prepare_sys  (void)
{
    out_quest_all ();
    ins_quest ((char *) &sys_peri.sys, 2, 0);
    cursor_sys = prepare_sql ("select sys_peri.anschluss,  "
"sys_peri.anz_waehl_wied,  sys_peri.auszeit,  sys_peri.baudrate,  "
"sys_peri.dat_abruf,  sys_peri.dat_update,  sys_peri.daten_bit,  "
"sys_peri.drv_pid,  sys_peri.fil,  sys_peri.kanal,  sys_peri.mdn,  "
"sys_peri.modem_typ,  sys_peri.parity,  sys_peri.peri_nam,  "
"sys_peri.peri_typ,  sys_peri.port_typ,  sys_peri.protokoll,  "
"sys_peri.retry_max,  sys_peri.stat,  sys_peri.status_abruf,  "
"sys_peri.stop_bit,  sys_peri.sys,  sys_peri.sys_abruf,  "
"sys_peri.sys_update,  sys_peri.tel,  sys_peri.term,  sys_peri.txt,  "
"sys_peri.zeit_abruf,  sys_peri.b_check,  sys_peri.la_zahl from sys_peri "
                                "where sys = ?");
}

void SYS_PERI_CLASS::prepare_mdn  (void)
{
    out_quest_all ();
    ins_quest ((char *) &sys_peri.mdn, 1, 0);
    cursor_mdn = prepare_sql ("select sys_peri.anschluss,  "
"sys_peri.anz_waehl_wied,  sys_peri.auszeit,  sys_peri.baudrate,  "
"sys_peri.dat_abruf,  sys_peri.dat_update,  sys_peri.daten_bit,  "
"sys_peri.drv_pid,  sys_peri.fil,  sys_peri.kanal,  sys_peri.mdn,  "
"sys_peri.modem_typ,  sys_peri.parity,  sys_peri.peri_nam,  "
"sys_peri.peri_typ,  sys_peri.port_typ,  sys_peri.protokoll,  "
"sys_peri.retry_max,  sys_peri.stat,  sys_peri.status_abruf,  "
"sys_peri.stop_bit,  sys_peri.sys,  sys_peri.sys_abruf,  "
"sys_peri.sys_update,  sys_peri.tel,  sys_peri.term,  sys_peri.txt,  "
"sys_peri.zeit_abruf,  sys_peri.b_check,  sys_peri.la_zahl from sys_peri "
                                "where mdn = ? "
                                "order by sys");
}

void SYS_PERI_CLASS::prepare_fil  (void)
{
    out_quest_all ();
    ins_quest ((char *) &sys_peri.mdn, 1, 0);
    ins_quest ((char *) &sys_peri.fil, 1, 0);
    cursor_fil = prepare_sql ("select sys_peri.anschluss,  "
"sys_peri.anz_waehl_wied,  sys_peri.auszeit,  sys_peri.baudrate,  "
"sys_peri.dat_abruf,  sys_peri.dat_update,  sys_peri.daten_bit,  "
"sys_peri.drv_pid,  sys_peri.fil,  sys_peri.kanal,  sys_peri.mdn,  "
"sys_peri.modem_typ,  sys_peri.parity,  sys_peri.peri_nam,  "
"sys_peri.peri_typ,  sys_peri.port_typ,  sys_peri.protokoll,  "
"sys_peri.retry_max,  sys_peri.stat,  sys_peri.status_abruf,  "
"sys_peri.stop_bit,  sys_peri.sys,  sys_peri.sys_abruf,  "
"sys_peri.sys_update,  sys_peri.tel,  sys_peri.term,  sys_peri.txt,  "
"sys_peri.zeit_abruf,  sys_peri.b_check,  sys_peri.la_zahl from sys_peri "
                                "where mdn = ? "
                                "and   fil = ? "
                                "order by sys");
}

void SYS_PERI_CLASS::prepare_sys_peri  (char *order)
{
    out_quest_all ();
    if (order)
    {
      cursor_sys_peri = prepare_sql ("select "
"sys_peri.anschluss,  sys_peri.anz_waehl_wied,  sys_peri.auszeit,  "
"sys_peri.baudrate,  sys_peri.dat_abruf,  sys_peri.dat_update,  "
"sys_peri.daten_bit,  sys_peri.drv_pid,  sys_peri.fil,  sys_peri.kanal,  "
"sys_peri.mdn,  sys_peri.modem_typ,  sys_peri.parity,  "
"sys_peri.peri_nam,  sys_peri.peri_typ,  sys_peri.port_typ,  "
"sys_peri.protokoll,  sys_peri.retry_max,  sys_peri.stat,  "
"sys_peri.status_abruf,  sys_peri.stop_bit,  sys_peri.sys,  "
"sys_peri.sys_abruf,  sys_peri.sys_update,  sys_peri.tel,  "
"sys_peri.term,  sys_peri.txt,  sys_peri.zeit_abruf,  sys_peri.b_check,  "
"sys_peri.la_zahl from sys_peri %s", order);
    }
    else
    {
      cursor_sys_peri = prepare_sql ("select "
"sys_peri.anschluss,  sys_peri.anz_waehl_wied,  sys_peri.auszeit,  "
"sys_peri.baudrate,  sys_peri.dat_abruf,  sys_peri.dat_update,  "
"sys_peri.daten_bit,  sys_peri.drv_pid,  sys_peri.fil,  sys_peri.kanal,  "
"sys_peri.mdn,  sys_peri.modem_typ,  sys_peri.parity,  "
"sys_peri.peri_nam,  sys_peri.peri_typ,  sys_peri.port_typ,  "
"sys_peri.protokoll,  sys_peri.retry_max,  sys_peri.stat,  "
"sys_peri.status_abruf,  sys_peri.stop_bit,  sys_peri.sys,  "
"sys_peri.sys_abruf,  sys_peri.sys_update,  sys_peri.tel,  "
"sys_peri.term,  sys_peri.txt,  sys_peri.zeit_abruf,  sys_peri.b_check,  "
"sys_peri.la_zahl from sys_peri");
    }
}

int SYS_PERI_CLASS::lese_sys (long sys)
/**
Tabelle sys_peri lesen.
**/
{
         if (cursor_sys == -1)
         {
                      prepare_sys ();
         }
         sys_peri.sys = sys;
         open_sql (cursor_sys);
         fetch_sql (cursor_sys);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int SYS_PERI_CLASS::lese_sys (void)
/**
Naechsten Satz aus Tabelle sys_peri lesen.
**/
{
         fetch_sql (cursor_sys);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int SYS_PERI_CLASS::lese_mdn (short mdn)
/**
Tabelle sys_peri lesen.
**/
{
         if (cursor_mdn == -1)
         {
                      prepare_mdn ();
         }
         sys_peri.mdn = mdn;
         open_sql (cursor_mdn);
         fetch_sql (cursor_mdn);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int SYS_PERI_CLASS::lese_mdn (void)
/**
Naechsten Satz aus Tabelle sys_peri lesen.
**/
{
         fetch_sql (cursor_mdn);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int SYS_PERI_CLASS::lese_fil (short mdn, short fil)
/**
Tabelle sys_peri lesen.
**/
{
         if (cursor_fil == -1)
         {
                      prepare_fil ();
         }
         sys_peri.mdn = mdn;
         sys_peri.fil = fil;
         open_sql (cursor_fil);
         fetch_sql (cursor_fil);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int SYS_PERI_CLASS::lese_fil (void)
/**
Naechsten Satz aus Tabelle sys_peri lesen.
**/
{
         fetch_sql (cursor_fil);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int SYS_PERI_CLASS::lese_sys_peri (char *order)
/**
Tabelle sys_peri lesen.
**/
{
         if (cursor_sys_peri == -1)
         {
                      prepare_sys_peri (order);
         }
         open_sql (cursor_sys_peri);
         fetch_sql (cursor_sys_peri);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int SYS_PERI_CLASS::lese_sys_peri (void)
/**
Naechsten Satz aus Tabelle sys_peri lesen.
**/
{
         fetch_sql (cursor_sys_peri);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}



void SYS_PERI_CLASS::close_sys (void)
/**
Cursor sys schliessen.
**/
{
         if (cursor_sys == -1) return;

         close_sql (cursor_sys);
         cursor_sys = -1;
}

void SYS_PERI_CLASS::close_mdn (void)
/**
Cursor mdn schliessen.
**/
{
         if (cursor_mdn == -1) return;

         close_sql (cursor_mdn);
         cursor_mdn = -1;
}

void SYS_PERI_CLASS::close_fil (void)
/**
Cursor fil schliessen.
**/
{
         if (cursor_fil == -1) return;

         close_sql (cursor_fil);
         cursor_fil = -1;
}

void SYS_PERI_CLASS::close_sys_peri (void)
/**
Cursor sys_peri schliessen.
**/
{
         if (cursor_sys_peri == -1) return;

         close_sql (cursor_sys_peri);
         cursor_sys_peri = -1;
}

void DAT_ABS_CLASS::prepare (void)
/**
Cursor fuer Tabelle dat_abs.
**/
{
          out_quest ((char *) &dat_abs.akt_tag, 2, 0); 
          out_quest ((char *) dat_abs.a_heute, 0, 2); 
          cursor_dat_abs = prepare_sql ("select akt_tag, a_heute from dat_abs");

          ins_quest ((char *) &dat_abs.akt_tag, 2, 0); 
          ins_quest ((char *) dat_abs.a_heute, 0, 2); 
          update_cursor = prepare_sql ("update dat_abs set akt_tag = ?,a_heute = ?");

          ins_quest ((char *) &dat_abs.akt_tag, 2, 0); 
          ins_quest ((char *) dat_abs.a_heute, 0, 2); 
          insert_cursor = prepare_sql ("insert into dat_abs (akt_tag, a_heute) "
                                       "values (?,?)");

          delete_cursor = prepare_sql ("delete from dat_abs");
}

int DAT_ABS_CLASS::Lesedat_abs (void)
/**
In Tabelle dat_abs lesen.
**/
{
          if (cursor_dat_abs == -1)
          {
                           prepare ();
          }

          open_sql (cursor_dat_abs);
          fetch_sql (cursor_dat_abs);
          return sqlstatus;
}


int DAT_ABS_CLASS::Updatedat_abs (void)
/**
In Tabelle dat_abs aendern.
**/
{
          if (cursor_dat_abs == -1)
          {
                           prepare ();
          }

          execute_curs (update_cursor);
          return sqlstatus;
}
          
int DAT_ABS_CLASS::Insertdat_abs (void)
/**
In Tabelle dat_abs einfuegen.
**/
{
          if (cursor_dat_abs == -1)
          {
                           prepare ();
          }

          execute_curs (insert_cursor);
          return sqlstatus;
}
          
int DAT_ABS_CLASS::Deletedat_abs (void)
/**
In Tabelle dat_abs loeschen.
**/
{
          if (cursor_dat_abs == -1)
          {
                           prepare ();
          }

          execute_curs (delete_cursor);
          return sqlstatus;
}


void DAT_ABS_CLASS::Closedat_abs (void)
/**
Cursor fuer dat_abs schliessen.
**/
{
          if (cursor_dat_abs == -1) return;

          close_sql (cursor_dat_abs);          
          close_sql (update_cursor);          
          close_sql (insert_cursor);          
          close_sql (delete_cursor);          
          cursor_dat_abs = -1;
}

// Class fuer konfig_tag

#ifndef _NODBCLASS
void KONFIG_TAG_CLASS::prepare (void)
{
    ins_quest ((char *) &konfig_tag.sys, 2, 0); 
    out_quest ((char *) konfig_tag.a_modif,0,2);
    out_quest ((char *) konfig_tag.a_sum_kz,0,2);
    out_quest ((char *) konfig_tag.bed_sum_kz,0,2);
    out_quest ((char *) konfig_tag.err_kz,0,2);
    out_quest ((char *) &konfig_tag.fil,1,0);
    out_quest ((char *) konfig_tag.freq_ums_kz,0,2);
    out_quest ((char *) konfig_tag.kase_sum_kz,0,2);
    out_quest ((char *) konfig_tag.kasi_sum_kz,0,2);
    out_quest ((char *) konfig_tag.lng_bon_kz,0,2);
    out_quest ((char *) &konfig_tag.mdn,1,0);
    out_quest ((char *) konfig_tag.mwst_kz,0,2);
    out_quest ((char *) &konfig_tag.sys,2,0);
    out_quest ((char *) konfig_tag.verk_sum_kz,0,2);
    out_quest ((char *) konfig_tag.waa_sum_kz,0,2);
    out_quest ((char *) konfig_tag.wg_sum_kz,0,2);
    out_quest ((char *) &konfig_tag.auto_abruf,1,0);
     cursor = prepare_sql ("select konfig_tag.a_modif,  "
"konfig_tag.a_sum_kz,  konfig_tag.bed_sum_kz,  konfig_tag.err_kz,  "
"konfig_tag.fil,  konfig_tag.freq_ums_kz,  konfig_tag.kase_sum_kz,  "
"konfig_tag.kasi_sum_kz,  konfig_tag.lng_bon_kz,  konfig_tag.mdn,  "
"konfig_tag.mwst_kz,  konfig_tag.sys,  konfig_tag.verk_sum_kz,  "
"konfig_tag.waa_sum_kz,  konfig_tag.wg_sum_kz,  "
"konfig_tag.auto_abruf "
                           "from konfig_tag "
                           "where konfig_tag.sys = ?");
}

int KONFIG_TAG_CLASS::dbreadfirst (void)
{
            if (cursor == -1)
            {
                         this->prepare ();
            }
            return this->DB_CLASS::dbreadfirst ();
}
         
// Class fuer opt_cls

void OPT_CLS_CLASS::prepare (void)
{
    ins_quest ((char *) &opt_cls.sys, 2, 0);
    out_quest ((char *) &opt_cls.a_anz,2,0);
    out_quest ((char *) opt_cls.a_sp_kz,0,2);
    out_quest ((char *) &opt_cls.anz_txt,2,0);
    out_quest ((char *) opt_cls.dfue_passwd,0,33);
    out_quest ((char *) &opt_cls.fil,1,0);
    out_quest ((char *) opt_cls.hbk_kz,0,2);
    out_quest ((char *) &opt_cls.max_a,2,0);
    out_quest ((char *) &opt_cls.mdn,1,0);
    out_quest ((char *) opt_cls.mwst_sp_kz,0,2);
    out_quest ((char *) opt_cls.pr_ek_kz,0,2);
    out_quest ((char *) opt_cls.pr_sond_kz,0,2);
    out_quest ((char *) opt_cls.pr_ueb,0,2);
    out_quest ((char *) &opt_cls.stnd_sg,1,0);
    out_quest ((char *) opt_cls.sum_kun_bon,0,2);
    out_quest ((char *) &opt_cls.sys,2,0);
    out_quest ((char *) &opt_cls.zus_txt,1,0);
     cursor = prepare_sql ("select opt_cls.a_anz,  "
"opt_cls.a_sp_kz,  opt_cls.anz_txt,  opt_cls.dfue_passwd,  opt_cls.fil,  "
"opt_cls.hbk_kz,  opt_cls.max_a,  opt_cls.mdn,  opt_cls.mwst_sp_kz,  "
"opt_cls.pr_ek_kz,  opt_cls.pr_sond_kz,  opt_cls.pr_ueb,  "
"opt_cls.stnd_sg,  opt_cls.sum_kun_bon,  opt_cls.sys,  opt_cls.zus_txt from opt_cls where sys = ?");
}

int OPT_CLS_CLASS::dbreadfirst (void)
{
            if (cursor == -1)
            {
                         this->prepare ();
            }
            return this->DB_CLASS::dbreadfirst ();
}
#endif
         
          
void ABS_STAT_CLASS::prepare (void)
/**
Cursor fuer Tabelle dat_abs.
**/
{
         ins_quest ((char *) &abs_stat.sys, 2, 0); 
         out_quest ((char *) &abs_stat.sys,2,0);
         out_quest ((char *) &abs_stat.peri_typ,1,0);
         out_quest ((char *) abs_stat.par,0,49);
         out_quest ((char *) &abs_stat.status,1,0);
         out_quest ((char *) &abs_stat.sqlstat,2,0);
         cursor = prepare_sql ("select abs_stat.sys,  "
           "abs_stat.peri_typ,  abs_stat.par,  abs_stat.status,  abs_stat.sqlstat from abs_stat "
                        "where sys = ?");

         ins_quest ((char *) &abs_stat.sys, 2, 0); 
         ins_quest ((char *) abs_stat.par, 0, 49); 
         out_quest ((char *) &abs_stat.sys,2,0);
         out_quest ((char *) &abs_stat.peri_typ,1,0);
         out_quest ((char *) abs_stat.par,0,49);
         out_quest ((char *) &abs_stat.status,1,0);
         out_quest ((char *) &abs_stat.sqlstat,2,0);
         cursor_par = prepare_sql ("select abs_stat.sys,  "
           "abs_stat.peri_typ,  abs_stat.par,  abs_stat.status,  abs_stat.sqlstat from abs_stat "
                        "where sys = ? and par = ?");

         ins_quest ((char *) &abs_stat.sys,2,0); 
         ins_quest ((char *) &abs_stat.peri_typ,1,0);
         ins_quest ((char *) abs_stat.par,0,49);
         ins_quest ((char *) &abs_stat.status,1,0);
         ins_quest ((char *) &abs_stat.sqlstat,2,0);
         ins_quest ((char *) &abs_stat.sys, 2, 0); 
         update_cursor = prepare_sql ("update abs_stat set "
                 "abs_stat.sys = ?,  abs_stat.peri_typ = ?,  abs_stat.par = ?,  "
                 "abs_stat.status = ?,  abs_stat.sqlstat = ? "
                 "where sys = ?");

         ins_quest ((char *) &abs_stat.sys,2,0);
         ins_quest ((char *) &abs_stat.peri_typ,1,0);
         ins_quest ((char *) abs_stat.par,0,49);
         ins_quest ((char *) &abs_stat.status,1,0);
         ins_quest ((char *) &abs_stat.sqlstat,2,0);
         insert_cursor = prepare_sql ("insert into abs_stat (sys,  "
                                   "peri_typ,  par,  status,  sqlstat)" 
                                   "  values "
                                   "(?,?,?,?,?)");
          ins_quest ((char *) &abs_stat.sys, 2, 0);
          test_upd_cursor = prepare_sql ("select sys from abs_stat where sys = ?");   

          ins_quest ((char *) &abs_stat.sys, 2, 0); 
          delete_cursor = prepare_sql ("delete from abs_stat where sys = ?");
}


int ABS_STAT_CLASS::dbreadfirst (void)
/**
In Tabelle dat_abs lesen.
**/
{
          if (cursor == -1)
          {
                           this->prepare ();
          }

          open_sql (cursor);
          fetch_sql (cursor);
          return sqlstatus;
}

int ABS_STAT_CLASS::dbread (void)
/**
In Tabelle dat_abs lesen.
**/
{
          fetch_sql (cursor);
          return sqlstatus;
}
int ABS_STAT_CLASS::dbreadfirstpar (void)
/**
In Tabelle dat_abs lesen.
**/
{
          if (cursor == -1)
          {
                           this->prepare ();
          }

          open_sql (cursor_par);
          fetch_sql (cursor_par);
          return sqlstatus;
}

int ABS_STAT_CLASS::dbreadpar (void)
/**
In Tabelle dat_abs lesen.
**/
{
          fetch_sql (cursor_par);
          return sqlstatus;
}


int ABS_STAT_CLASS::dbupdate (void)
/**
In Tabelle dat_abs aendern.
**/
{
          if (cursor == -1)
          {
                           prepare ();
          }

          open_sql (test_upd_cursor);
          fetch_sql (test_upd_cursor);
          if (sqlstatus == 0)
          {
                        execute_curs (update_cursor);
          }
          if (sqlstatus == 100)
          {
                        execute_curs (insert_cursor);
          }
          return sqlstatus;
}

int ABS_STAT_CLASS::dbdelall (void)
/**
Tabelle abs_stat loeschen.
**/
{
          execute_sql ("delete from abs_stat");
          return sqlstatus;
} 
          
          