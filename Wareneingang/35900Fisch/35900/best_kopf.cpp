#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "a_kun.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "best_kopf.h"

struct BEST_KOPF best_kopf, best_kopf_null;

void BEST_KOPF_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &best_kopf.mdn, 1, 0);
            ins_quest ((char *) &best_kopf.fil, 1, 0);
            ins_quest ((char *) &best_kopf.best_blg, 2, 0);
            ins_quest ((char *) &best_kopf.lief, 0, 17);
    out_quest ((char *) &best_kopf.bearb_stat,1,0);
    out_quest ((char *) &best_kopf.best_blg,2,0);
    out_quest ((char *) &best_kopf.best_term,2,0);
    out_quest ((char *) &best_kopf.best_txt,2,0);
    out_quest ((char *) &best_kopf.fil,1,0);
    out_quest ((char *) &best_kopf.lfd,2,0);
    out_quest ((char *) best_kopf.lief,0,17);
    out_quest ((char *) &best_kopf.lief_s,2,0);
    out_quest ((char *) &best_kopf.lief_term,2,0);
    out_quest ((char *) &best_kopf.mdn,1,0);
    out_quest ((char *) &best_kopf.we_dat,2,0);
    out_quest ((char *) best_kopf.pers_nam,0,9);
    out_quest ((char *) &best_kopf.fil_adr,2,0);
    out_quest ((char *) &best_kopf.lief_adr,2,0);
    out_quest ((char *) best_kopf.lieferzeit,0,6);
    out_quest ((char *) best_kopf.freifeld1,0,37);
            cursor = prepare_sql ("select "
"best_kopf.bearb_stat,  best_kopf.best_blg,  best_kopf.best_term,  "
"best_kopf.best_txt,  best_kopf.fil,  best_kopf.lfd,  best_kopf.lief,  "
"best_kopf.lief_s,  best_kopf.lief_term,  best_kopf.mdn,  "
"best_kopf.we_dat,  best_kopf.pers_nam,  best_kopf.fil_adr,  "
"best_kopf.lief_adr,  best_kopf.lieferzeit,best_kopf.freifeld1 from best_kopf "

#line 29 "best_kopf.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   best_blg = ? "
                                  "and   lief = ? ");

    ins_quest ((char *) &best_kopf.bearb_stat,1,0);
    ins_quest ((char *) &best_kopf.best_blg,2,0);
    ins_quest ((char *) &best_kopf.best_term,2,0);
    ins_quest ((char *) &best_kopf.best_txt,2,0);
    ins_quest ((char *) &best_kopf.fil,1,0);
    ins_quest ((char *) &best_kopf.lfd,2,0);
    ins_quest ((char *) best_kopf.lief,0,17);
    ins_quest ((char *) &best_kopf.lief_s,2,0);
    ins_quest ((char *) &best_kopf.lief_term,2,0);
    ins_quest ((char *) &best_kopf.mdn,1,0);
    ins_quest ((char *) &best_kopf.we_dat,2,0);
    ins_quest ((char *) best_kopf.pers_nam,0,9);
    ins_quest ((char *) &best_kopf.fil_adr,2,0);
    ins_quest ((char *) &best_kopf.lief_adr,2,0);
    ins_quest ((char *) best_kopf.lieferzeit,0,6);
    ins_quest ((char *) best_kopf.freifeld1,0,37);
            sqltext = "update best_kopf set "
"best_kopf.bearb_stat = ?,  best_kopf.best_blg = ?,  "
"best_kopf.best_term = ?,  best_kopf.best_txt = ?,  "
"best_kopf.fil = ?,  best_kopf.lfd = ?,  best_kopf.lief = ?,  "
"best_kopf.lief_s = ?,  best_kopf.lief_term = ?,  best_kopf.mdn = ?,  "
"best_kopf.we_dat = ?,  best_kopf.pers_nam = ?,  "
"best_kopf.fil_adr = ?,  best_kopf.lief_adr = ?,  "
"best_kopf.lieferzeit = ?, best_kopf.freifeld1 = ? "

#line 35 "best_kopf.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   best_blg = ? "
                                  "and   lief = ?";

            ins_quest ((char *) &best_kopf.mdn, 1, 0);
            ins_quest ((char *) &best_kopf.fil, 1, 0);
            ins_quest ((char *) &best_kopf.best_blg, 2, 0);
            ins_quest ((char *) &best_kopf.lief, 0, 17);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &best_kopf.mdn, 1, 0);
            ins_quest ((char *) &best_kopf.fil, 1, 0);
            ins_quest ((char *) &best_kopf.best_blg, 2, 0);
            ins_quest ((char *) &best_kopf.lief, 0, 17);
            test_upd_cursor = prepare_sql ("select best_blg from best_kopf "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   best_blg = ? "
                                  "and   lief = ? ");
            ins_quest ((char *) &best_kopf.mdn, 1, 0);
            ins_quest ((char *) &best_kopf.fil, 1, 0);
            ins_quest ((char *) &best_kopf.best_blg, 2, 0);
            ins_quest ((char *) &best_kopf.lief, 0, 17);
            del_cursor = prepare_sql ("delete from best_kopf "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   best_blg = ? "
                                  "and   lief = ? ");
    ins_quest ((char *) &best_kopf.bearb_stat,1,0);
    ins_quest ((char *) &best_kopf.best_blg,2,0);
    ins_quest ((char *) &best_kopf.best_term,2,0);
    ins_quest ((char *) &best_kopf.best_txt,2,0);
    ins_quest ((char *) &best_kopf.fil,1,0);
    ins_quest ((char *) &best_kopf.lfd,2,0);
    ins_quest ((char *) best_kopf.lief,0,17);
    ins_quest ((char *) &best_kopf.lief_s,2,0);
    ins_quest ((char *) &best_kopf.lief_term,2,0);
    ins_quest ((char *) &best_kopf.mdn,1,0);
    ins_quest ((char *) &best_kopf.we_dat,2,0);
    ins_quest ((char *) best_kopf.pers_nam,0,9);
    ins_quest ((char *) &best_kopf.fil_adr,2,0);
    ins_quest ((char *) &best_kopf.lief_adr,2,0);
    ins_quest ((char *) best_kopf.lieferzeit,0,6);
    ins_quest ((char *) best_kopf.freifeld1,0,37);
            ins_cursor = prepare_sql ("insert into best_kopf ("
"bearb_stat,  best_blg,  best_term,  best_txt,  fil,  lfd,  lief,  lief_s,  lief_term,  "
"mdn,  we_dat,  pers_nam,  fil_adr,  lief_adr,  lieferzeit, freifeld1) "

#line 65 "best_kopf.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?)"); 

#line 67 "best_kopf.rpp"
}
int BEST_KOPF_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

