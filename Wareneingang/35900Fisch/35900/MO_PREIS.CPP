/*
-----------------------------------------------------------------------------
-
-	Modulname		:	mo_preisf
-
-       Autor                   :       W. Roth
-       Erstellungsdatum        :       16.06.98
-       Modifikationsdatum      :       16.06.98
-
-	Projekt			:	BWS
-	Version			:	1.03
-	Laendervariante		:	BRD
-
-       Sprache                 :       C++
-
-	Aenderungsjournal	:
-
-	lfd. -	Version	Datum	    Name	Beschreibung
-       ----------------------------------------------------------------------

-
------------------------------------------------------------------------------
-
-	Modulbeschreibung	:  	Prozeduren zur Preisfindung fuer
-					Kunden und Filialen
-
-					BWS - VERSION
-
------------------------------------------------------------------------------
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include "strfkt.h"
#include "mo_curso.h"
#include "mdn.h"
#include "fil.h"
#include "kun.h"
#include "a_pr.h"
#include "akt_krz.h"
#include "aktion.h"
#include "mo_a_pr.h"
#include "mo_preis.h"

extern int _WMDEBUG;
extern int writelog (char *, ...);
extern void disp_mess (char *, int modus);

static struct ipr {
   short     mdn;
   long      pr_gr_stuf;
   long      kun_pr;
   double    a;
   double    vk_pr_i;
   double    ld_pr;
   short     aktion_nr;
   char      a_akt_kz[2];
   char      modif[2];
} ipr, ipr_null;

static struct akiprgrstk {
   short     mdn;
   short     aki_nr;
   char      zus_bz[25];
   long      pr_gr_stuf;
   long      aki_von;
   long      aki_bis;
   short     delstatus;
} akiprgrstk, akiprgrstk_null;

static struct akiprgrstp {
   short     mdn;
   short     aki_nr;
   double    a;
   double    ld_pr;
   double    aki_pr;
   double    me_min;
   double    me_max;
   double    me;
} akiprgrstp, akiprgrstp_null;

static struct akikunlsk {
   short     mdn;
   short     aki_nr;
   char      zus_bz[25];
   long      pr_gr_stuf;
   long      kun_pr;
   long      aki_von;
   long      aki_bis;
   short     delstatus;
} akikunlsk, akikunlsk_null;

static struct akikunlsp {
   short     mdn;
   short     aki_nr;
   double    a;
   double    ld_pr;
   double    aki_pr;
   double    me_min;
   double    me_max;
   double    me;
} akikunlsp, akikunlsp_null;

static struct akikunprp {
   short     mdn;
   long      kun;
   double    a;
   double    ld_pr;
   double    aki_pr;
   long      aki_von;
   long      aki_bis;
} akikunprp, akikunprp_null;


static struct {
   short     mdn;
   short     aki_nr;
   char      zus_bz[25];
   long      pr_gr_stuf;
   long      aki_von;
   long      aki_bis;
   short     delstatus;
} aktion, aktion_null;

static struct {
   short     mdn;
   long      pr_gr_stuf;
   long      kun_pr;
   double    a;
   double    vk_pr_i;
   double    ld_pr;
   short     aktion_nr;
   char      a_akt_kz[2];
   char      modif[2];
} i_pr, i_pr_null;

static struct {
   short     mdn;
   long      pr_gr_stuf;
   long      kun_pr;
   double    a;
   double    vk_pr_i;
   double    ld_pr;
   short     aktion_nr;
   char      a_akt_kz[2];
   char      modif[2];
} tsave_ipr, tsave_ipr_null;

static struct {
   short     mdn;
   long      kun;
   double    a;
   double    ld_pr;
   double    aki_pr;
   long      aki_von;
   long      aki_bis;
} tsave_akikunprp, tsave_akikunprp_null;

static short tauschen = 0;


/*---------------------------*/
/*--- P R O C E D U R E N ---*/
/*---------------------------*/

/*-------------------------------------------------------------------------
-
-	Operatorname		: preise_holen
-
-	In			: dmdn        like kun.mdn
-				  dfil        like kun.fil
-				  dkun_fil    smallint
-				  dkun        like kun.kun
-				  da          like lsp.a
-
-
-				  ddatum_preisf date
-
-
-
-	Out			: status      smallint CTRUE/CFALSE
-				              CTRUE = Preis gefunden
-				  dskz        smallint CTRUE/CFALSE
-				              CTRUE = Sonderpreis
-				  dvk         like lsp.ls_vk_pr
-				  dlad        like lsp.ls_lad_pr
-
-	Beschreibung		: Die Prozedur liest die Preise aus den
-				  entsprechenden Tabellen
-
-				  perform disp_msg(CENBED2009,1) muss in die
-				  aufrufende Procedur wenn Rueckgabestatus
-				  = CFALSE !!!
-
-------------------------------------------------------------------------*/

int WA_PREISE::preis_hol (PREISIN *preisin, PREISOUT *preisout)
{
        short status;
 

        status = preise_holen (preisin->mdn,
                               preisin->fil,
                               preisin->kun_fil,
                               preisin->kun,
                               preisin->a,
                               preisin->datum,
                               &preisout->sa,
                               &preisout->pr_ek,
                               &preisout->pr_vk);
        return (status);
}
 

int WA_PREISE::preise_holen (short dmdn, short dfil, short dkun_fil,
                             long dkun, double da, char *ddatum,
                             short *sa, double *pr_ek, double *pr_vk)
/**
Zweite Procedure . Preis holen mit direkter Uebergabe der Parameter.
**/
{

/*--- lokale Deklarationen ---*/

        short dpr_stu;
        long  dpr_lst;

        // tst = 2;

        debug ("mdn = %hd fil = %hd kun_fil = %hd "
                "kun = %hd a = %.0lf datum = %s\n" ,
                 dmdn, dfil, dkun_fil, dkun, da, ddatum);
          
	tsave_ipr  = tsave_ipr_null;
	tsave_akikunprp = tsave_akikunprp_null;
        *sa = 0;
        *pr_ek = *pr_vk = (double) 0.0;
        if (dkun_fil == 0)
        {
                  dstat = fetch_gruppen (dmdn, dfil, 
                               &dkun, &dpr_stu, &dpr_lst, &dkun_fil);
                  debug ("Status nach fetch_gruppen %hd\n", dstat);
                  if (dstat == 0) return (dstat);
        }
	switch (dkun_fil)
        {
		case 0 :
                        strcpy (sysdatum,ddatum);
                        kun.kun = dkun;
                        debug ("pr_stu %hd pr_lst %ld\n", dpr_stu, dpr_lst);
                        dstat = fetch_ku_preis (dmdn,dkun,dpr_stu,dpr_lst,da,
	                                        sa, pr_ek, pr_vk);
                        break;

		case 1 :
                        _mdn.mdn = dmdn;
                        _mdn.mdn_gr = 0;
                        mdn_class.lese_mdn (dmdn);
                        _fil.mdn = dmdn;
                        _fil.fil = (short) dkun;
                        _fil.fil_gr = 0;
                        fil_class.lese_fil (dmdn, _fil.fil);
                        if (tauschen)
                        {
                             *sa = ab_preise.fetch_preis_tag (_mdn.mdn_gr, 
                                        _mdn.mdn, 
                                        _fil.fil_gr, 
                                        _fil.fil, 
                                        da,
                                        ddatum,
			                pr_vk, pr_ek);
                             if (*pr_ek > (double) 0)
                             {
                                    dstat = 0;
                             }
                             else
                             {
				    dstat = 0;
		             } 
                         }
                         else
                         {
                             *sa = ab_preise.fetch_preis_tag (_mdn.mdn_gr, 
                                        _mdn.mdn, 
                                        _fil.fil_gr, 
                                        _fil.fil, 
                                        da,
                                        ddatum,
			                pr_ek, pr_vk);
                             if (*pr_vk > (double) 0)
                             {
                                         dstat = 0;
                             }
			     else
                             {
				         dstat = 0;
			     } 
                        }
			break;
		default :
			break;
	}
	return (dstat);
}

/*-------------------------------------------------------------------------
-
-	Operatorname		: fetch_ku_preis  

        In                      : dmdn
                                  dkun 
                                  dpr_stu
                                  dpr_lst
-
-	Beschreibung		: Kundenpreis holen.
-
-------------------------------------------------------------------------*/

int WA_PREISE::fetch_ku_preis (short dmdn, long dkun, short dpr_stu, long dpr_lst,
                double da, short *dskz, double *dvk, double *dlad)
{

          short dsa_kz;
          short keine_aktion;

          akiprgrstp = akiprgrstp_null;
          akikunprp  = akikunprp_null;
          strcpy (sa_flag, " ");
          aktion = aktion_null;

          *dskz = 0;
          *dvk = *dlad = (double) 0.0;
          debug ("fetch_std_preis %d %d %ld %.0lf",
                  dmdn, dpr_stu, dpr_lst, da);
          dsqlstatus =  fetch_std_preis (dmdn,dpr_stu,dpr_lst,da);
          debug ("dsqlstatus nach fetch_std_preis %d\n", dsqlstatus);
          if (dsqlstatus) return (0);

/* Aktion der Preisgruppenstufe holen            */
          sqlstatus = prep_akt ();

          if (ipr.kun_pr > 0)
          {
                  open_sql (fetch_akt_nr);
                  fetch_sql (fetch_akt_nr);
          }
          keine_aktion = 1;
          if (ipr.aktion_nr > 0)
          {

/* Preisgruppenaktion suchen                     */
                  open_sql (test_termin);
                  fetch_sql (test_termin);
                  if (sqlstatus == 0)
                  {
                           open_sql (fetch_akt);
                           fetch_sql (fetch_akt);
                           memcpy (&aktion, &akiprgrstk, sizeof (aktion));
                           keine_aktion = 0;
                  }
          }
          if (keine_aktion)
          {

/* Preisgruppenaktion suchen die noch nicht aktiviert sind, aber im Datum liegen*/
                  fetchprstakt (dpr_stu);
                  if (sqlstatus && ipr.pr_gr_stuf != dpr_stu)
                  {
                           fetchprstakt (ipr.pr_gr_stuf);
                  }
          }

/* Aktionen auf Kundenpreislisten suchen.
*/
          fetchkunakt (dpr_lst);
          if (sqlstatus && ipr.kun_pr != dpr_lst)
          {
                   fetchkunakt (ipr.kun_pr);
          }

/* Kundenaktion suchen                             */

          open_sql (fetch_kun_akt);
          fetch_sql (fetch_kun_akt);

/* Kundenaktionspreis uebertragen wenn kleiner bisheriger Preis  */

          ipr.vk_pr_i = kleinste_aktion ();
          if (sa_flag[0] == '*')
          {
                          dsa_kz = 1;
          }
          else
          {
                          dsa_kz = 0;
                          aktion = aktion_null;
          }
          *dskz = dsa_kz;
          *dvk  = ipr.vk_pr_i;
          *dlad = ipr.ld_pr;
          debug ("Preise OK vk_pr = %.2lf  lad_pr = %.2lf\n", 
                  ipr.vk_pr_i, ipr.ld_pr);
          return (0);
}

/*-------------------------------------------------------------------------
-
-	Operatorname		: fetch_std_preis  

        In                      : dmdn
                                  dpr_stu
                                  dpr_lst
-
-	Beschreibung		: Kundenstandardpreis holen.
-
-------------------------------------------------------------------------*/

int WA_PREISE::fetch_std_preis (short dmdn, short dpr_stu, long dpr_lst, double da)
{

        long sql_error;

        int std_preis;
        ins_quest ((char *) &dmdn,   1, 0);
        ins_quest ((char *) &dpr_stu, 1, 0);
        ins_quest ((char *) &dpr_lst, 2, 0);
        ins_quest ((char *) &da, 3, 0);
        out_quest ((char *) &ipr.mdn,1,0);
        out_quest ((char *) &ipr.pr_gr_stuf,2,0);
        out_quest ((char *) &ipr.kun_pr,2,0);
        out_quest ((char *) &ipr.a,3,0);
        out_quest ((char *) &ipr.vk_pr_i,3,0);
        out_quest ((char *) &ipr.ld_pr,3,0);
        out_quest ((char *) &ipr.aktion_nr,1,0);
        out_quest ((char *) ipr.a_akt_kz,0,2);
        out_quest ((char *) ipr.modif,0,2);
        std_preis = prepare_sql ("select ipr.mdn,  "
"ipr.pr_gr_stuf,  ipr.kun_pr,  ipr.a,  ipr.vk_pr_i,  ipr.ld_pr,  "
"ipr.aktion_nr,  ipr.a_akt_kz,  ipr.modif from ipr "
                                 "where ipr.mdn        = ? "
                                 "and   ipr.pr_gr_stuf = ? "
                                 "and   ipr.kun_pr     = ? "
                                 "and   ipr.a          = ? ");

        fetch_sql (std_preis);
        while (sqlstatus)
        {
                   if (dpr_lst > 0)
                   {
                            dpr_lst = 0;
                   }
                   else if (dpr_stu > 0)
                   {
                            dpr_stu = 0;
                   }
                   else
                   {
                            break;
                   }
                   open_sql (std_preis);
                   fetch_sql (std_preis);
                   debug ("fetch_sql (std_preis) sqlstatus %d", sqlstatus);
        }
        sql_error = sqlstatus;
        close_sql (std_preis);
        if (sql_error == 0)
        {
                   fetch_iv_pr (dmdn,dpr_stu,dpr_lst,da);
        }
        memcpy (&i_pr, &ipr, sizeof (ipr));
        return (sql_error);
}
          
int WA_PREISE::fetch_iv_pr (short dmdn, short dpr_stu, long dpr_lst, double da)
/**
Preis aus Vortabelle holen, wenn vorhanden.
**/
{

        double vk_pr;
        double vk_lad;

        out_quest ((char *) &vk_pr, 3, 0);
        out_quest ((char *) &vk_lad, 3, 0);
        execute_sql ("select iv_pr.vk_pr_i, iv_pr.ld_pr from iv_pr "
                     "where iv_pr.mdn        = %hd "
                     "and   iv_pr.pr_gr_stuf = %hd "
                     "and   iv_pr.kun_pr     = %d "
                     "and   iv_pr.a          = %.0lf "
                     "and   iv_pr.gue_ab    <= \"%s\" ",
                     dmdn, dpr_stu, dpr_lst, da, sysdatum);
        if (sqlstatus == 0)
        {
                 ipr.vk_pr_i = vk_pr;
                 ipr.ld_pr   = vk_lad;
        }
        return 0;
}
           

double WA_PREISE::kleinste_aktion ()
/**
Feststellen, welcher Aktionspreis kleiner ist.
**/

{
         if (akiprgrstp.aki_pr == 0 )
         {
                if (akikunprp.aki_pr > 0)
                {
                       strcpy (sa_flag,"*");
                       aktion.aki_von = akikunprp.aki_von;
                       aktion.aki_bis = akikunprp.aki_bis;
                       ipr.ld_pr = akikunprp.ld_pr;
                       return (akikunprp.aki_pr);
                }
                else if (akikunlsp.aki_pr > 0)
                {
                       strcpy (sa_flag, "*");
                       ipr.ld_pr = akikunlsp.ld_pr;
                       return (akikunlsp.aki_pr);
                }
                else
                {
                       return (ipr.vk_pr_i);
                }
         }

         if (akikunprp.aki_pr > 0)
         {
                strcpy (sa_flag, "*");
                aktion.aki_von = akikunprp.aki_von;
                aktion.aki_bis = akikunprp.aki_bis;
                ipr.ld_pr = akikunprp.ld_pr;
                return (akikunprp.aki_pr);
         }

/* Version > Kundenpreislistenaktion hat Vorrang vor Preigruppenaktion */

         if (akikunlsp.aki_pr > 0)
         {
                 akiprgrstp.aki_pr = akikunlsp.aki_pr;
                 akiprgrstp.ld_pr  = akikunlsp. ld_pr;
         }
 
         if (ipr.kun_pr = 0)
         {
                strcpy (sa_flag, "*");
                ipr.ld_pr = akiprgrstp.ld_pr;
                return (akiprgrstp.aki_pr);
         }

         if (akiprgrstp.aki_pr < ipr.vk_pr_i)
         {
                strcpy (sa_flag, "*");
                ipr.ld_pr = akiprgrstp.ld_pr;
                return (akiprgrstp.aki_pr);
         }
         return (ipr.vk_pr_i);
}

/*-------------------------------------------------------------------------
-
-	Operatorname		: fetchprstakt
-
-	Beschreibung		: Aktionspreis fuer Artikel in akiprgrstufp
                                  suchen 
-
-------------------------------------------------------------------------*/

int WA_PREISE::fetchprstakt (long dpr_stuf)
{
          akiprgrstk.pr_gr_stuf = dpr_stuf;
          open_sql (fetch_pos_dat);
          fetch_sql (fetch_pos_dat);
          while(! sqlstatus)
          {
                     akiprgrstk.aki_nr  = akiprgrstp.aki_nr;
                     open_sql (fetch_kopf_dat);
                     fetch_sql (fetch_kopf_dat);
                     if (sqlstatus == 0) break;
                     fetch_sql (fetch_pos_dat);
          }
          if (sqlstatus == 0)
          {
                      memcpy (&aktion, &akiprgrstk, sizeof (aktion));
          }
          if (sqlstatus)
          {
                    akiprgrstp = akiprgrstp_null;
                    akiprgrstk = akiprgrstk_null;
          }
          return sqlstatus;
}

/*-------------------------------------------------------------------------
-
-	Operatorname		: fetchkunakt
-
-	Beschreibung		: Aktionspreis fuer Artikel in akikunlsp
                                  suchen 
-
-------------------------------------------------------------------------*/

int WA_PREISE::fetchkunakt (long dkun_pr)
{
          akikunlsk.kun_pr = dkun_pr;
          open_sql (kun_akt_pos);
          fetch_sql (kun_akt_pos);
          while (! sqlstatus)
          {
                     open_sql (kun_akt_kopf);
                     fetch_sql (kun_akt_kopf);
                     if (sqlstatus == 0) break;
                     fetch_sql (kun_akt_pos);
          }
          if (sqlstatus == 0)
          {
                           aktion.aki_von = akikunlsk.aki_von;
                           aktion.aki_bis = akikunlsk.aki_bis;
          }
          if (sqlstatus)
          {
                    akikunlsk = akikunlsk_null;
                    akikunlsp = akikunlsp_null;
          }
          return sqlstatus;
}

/*-------------------------------------------------------------------------
-
-	Operatorname		: prep_akt
-
-	Beschreibung		: Cursor fuer Aktionspreise 
-
-------------------------------------------------------------------------*/

int WA_PREISE::prep_akt (void)
{
        static short prepared = 0;
        char datum [12];

        sysdate (datum);
        if (sysdatum[0] == 0) strcpy (sysdatum, datum);

        if (prepared) return (0);

        prepared = 1;
             
        ins_quest ((char *) &ipr.mdn, 1, 0);
        ins_quest ((char *) &ipr.aktion_nr, 1, 0);
        ins_quest ((char *) &ipr.a, 3, 0);
        out_quest ((char *) &akiprgrstp.mdn,1,0);
        out_quest ((char *) &akiprgrstp.aki_nr,1,0);
        out_quest ((char *) &akiprgrstp.a,3,0);
        out_quest ((char *) &akiprgrstp.ld_pr,3,0);
        out_quest ((char *) &akiprgrstp.aki_pr,3,0);
        out_quest ((char *) &akiprgrstp.me_min,3,0);
        out_quest ((char *) &akiprgrstp.me_max,3,0);
        out_quest ((char *) &akiprgrstp.me,3,0);
        fetch_akt = prepare_sql ("select akiprgrstp.mdn,  "
"akiprgrstp.aki_nr,  akiprgrstp.a,  akiprgrstp.ld_pr,  "
"akiprgrstp.aki_pr,  akiprgrstp.me_min,  akiprgrstp.me_max,  "
"akiprgrstp.me from akiprgrstp "
                                 "where akiprgrstp.mdn    = ? "
                                 "and   akiprgrstp.aki_nr = ? "
                                 "and   akiprgrstp.a      = ? ");

        if (sqlstatus) return (sqlstatus);

        ins_quest ((char *) &ipr.mdn, 1, 0);
        ins_quest ((char *) &ipr.a, 3, 0);
        out_quest ((char *) &akiprgrstp.mdn,1,0);
        out_quest ((char *) &akiprgrstp.aki_nr,1,0);
        out_quest ((char *) &akiprgrstp.a,3,0);
        out_quest ((char *) &akiprgrstp.ld_pr,3,0);
        out_quest ((char *) &akiprgrstp.aki_pr,3,0);
        out_quest ((char *) &akiprgrstp.me_min,3,0);
        out_quest ((char *) &akiprgrstp.me_max,3,0);
        out_quest ((char *) &akiprgrstp.me,3,0);
        fetch_pos_dat = prepare_sql ("select akiprgrstp.mdn,  "
"akiprgrstp.aki_nr,  akiprgrstp.a,  akiprgrstp.ld_pr,  "
"akiprgrstp.aki_pr,  akiprgrstp.me_min,  akiprgrstp.me_max,  "
"akiprgrstp.me from akiprgrstp "
                                     "where akiprgrstp.mdn    = ? "
                                     "and   akiprgrstp.a      =  ?");

        out_quest ((char *) &akiprgrstk.aki_von, 2, 0);
        out_quest ((char *) &akiprgrstk.aki_bis, 2, 0);
        ins_quest ((char *) &ipr.mdn, 1, 0);
        ins_quest ((char *) &akiprgrstk.aki_nr, 1, 0);
        ins_quest ((char *) &akiprgrstk.pr_gr_stuf, 2, 0);
        ins_quest ((char *) sysdatum, 0, 10);
        ins_quest ((char *) sysdatum, 0, 10);
        fetch_kopf_dat = prepare_sql 
               ("select akiprgrstk.aki_von, akiprgrstk.aki_bis from akiprgrstk "
                "where akiprgrstk.mdn          = ? "
                "and   akiprgrstk.aki_nr       = ? "
                "and   akiprgrstk.pr_gr_stuf   = ? "
                "and   akiprgrstk.aki_von     <= ? "
                "and   akiprgrstk.aki_bis     >= ? ");

        out_quest ((char *) &ipr.aktion_nr, 1, 0);
        ins_quest ((char *) &ipr.mdn, 1, 0);
        ins_quest ((char *) &ipr.a, 3, 0);
        ins_quest ((char *) &ipr.pr_gr_stuf, 2, 0);
        fetch_akt_nr = prepare_sql ("select ipr.aktion_nr from ipr "
                                    "where ipr.mdn = ? "
                                    "and   ipr.a = ? "
                                    "and   ipr.pr_gr_stuf = ? "
                                    "and   ipr.kun_pr = 0");
        if (sqlstatus) return (sqlstatus);

        out_quest ((char *) &akiprgrstk.aki_von, 2, 0);
        out_quest ((char *) &akiprgrstk.aki_bis, 2, 0);
        ins_quest ((char *) &ipr.mdn, 1, 0);
        ins_quest ((char *) &ipr.aktion_nr, 1, 0);
        ins_quest ((char *) sysdatum, 0, 10);
        ins_quest ((char *) sysdatum, 0, 10);
        test_termin = prepare_sql 
               ("select akiprgrstk.aki_von, akiprgrstk.aki_bis from akiprgrstk "
                "where akiprgrstk.mdn     = ? "
                "and   akiprgrstk.aki_nr  = ? "
                "and   akiprgrstk.aki_von <= ? "
                "and   akiprgrstk.aki_bis >= ? ");

        ins_quest ((char *) &ipr.mdn, 1, 0);
        ins_quest ((char *) &kun.kun, 2, 0);
        ins_quest ((char *) &ipr.a, 3, 0);
        ins_quest ((char *) sysdatum, 0, 10);
        ins_quest ((char *) sysdatum, 0, 10);
        out_quest ((char *) &akikunprp.mdn,1,0);
        out_quest ((char *) &akikunprp.kun,2,0);
        out_quest ((char *) &akikunprp.a,3,0);
        out_quest ((char *) &akikunprp.ld_pr,3,0);
        out_quest ((char *) &akikunprp.aki_pr,3,0);
        out_quest ((char *) &akikunprp.aki_von,2,0);
        out_quest ((char *) &akikunprp.aki_bis,2,0);
        fetch_kun_akt = prepare_sql ("select akikunprp.mdn,  "
"akikunprp.kun,  akikunprp.a,  akikunprp.ld_pr,  akikunprp.aki_pr,  "
"akikunprp.aki_von,  akikunprp.aki_bis from akikunprp "
                                     "where akikunprp.mdn = ? "
                                     "and   akikunprp.kun = ? "
                                     "and   akikunprp.a   = ? "
                                     "and   akikunprp.aki_von <= ? "
                                     "and   akikunprp.aki_bis >= ? ");

/* Aktionen auf Kundenpreislisten                  */

        ins_quest ((char *) &ipr.mdn, 1, 0);
        ins_quest ((char *) &ipr.a, 3, 0);
        out_quest ((char *) &akikunlsp.mdn,1,0);
        out_quest ((char *) &akikunlsp.aki_nr,1,0);
        out_quest ((char *) &akikunlsp.a,3,0);
        out_quest ((char *) &akikunlsp.ld_pr,3,0);
        out_quest ((char *) &akikunlsp.aki_pr,3,0);
        out_quest ((char *) &akikunlsp.me_min,3,0);
        out_quest ((char *) &akikunlsp.me_max,3,0);
        out_quest ((char *) &akikunlsp.me,3,0);
        kun_akt_pos = prepare_sql ("select akikunlsp.mdn,  "
"akikunlsp.aki_nr,  akikunlsp.a,  akikunlsp.ld_pr,  akikunlsp.aki_pr,  "
"akikunlsp.me_min,  akikunlsp.me_max,  akikunlsp.me from akikunlsp "
                                   "where akikunlsp.mdn    = ? "
                                   "and   akikunlsp.a      = ?");
        out_quest ((char *) &akikunlsk.aki_von, 2, 0);
        out_quest ((char *) &akikunlsk.aki_bis, 2, 0);
        ins_quest ((char *) &ipr.mdn, 1, 0);
        ins_quest ((char *) &akikunlsp.aki_nr, 1, 0);
        ins_quest ((char *) &akikunlsk.kun_pr, 2, 0);
        ins_quest ((char *) sysdatum, 0, 10);
        ins_quest ((char *) sysdatum, 0, 10);
        kun_akt_kopf = prepare_sql 
               ("select akikunlsk.aki_von, akikunlsk.aki_bis from akikunlsk "
                "where akikunlsk.mdn          = ? "
                "and   akikunlsk.aki_nr       = ? "
                "and   akikunlsk.kun_pr       = ? "
                "and   akikunlsk.aki_von     <= ? "
                "and   akikunlsk.aki_bis     >= ? ");
        return (sqlstatus);
}

int WA_PREISE::fetch_gruppen (short dmdn, short dfil, long *dkun,
                   short *dpr_stuf, long *dpr_lst, short *dkun_fil)
/**
Preisgruppen fuer Kunde holen.
**/
{
       tauschen = 0;
       if (*dkun_fil == 1) return (1);
       dsqlstatus = kun_class.lese_kun (dmdn, dfil, *dkun);
       if (dsqlstatus) return (0);

       *dpr_stuf = kun.pr_stu;
       *dpr_lst  = kun.pr_lst;

       if (*dpr_lst == -9) 
       {
                    *dkun_fil = 1;
                    tauschen = 1;
                    *dkun = (int) dfil;  
       }
       return (1);
}

int WA_PREISE::debug (char *format, ...)
{
       va_list args;
       char buffer [256];

       if (tst == 0) return (0);

       _WMDEBUG = 1;
       va_start (args, format);
       vsprintf (buffer, format, args);
       va_end (args);
       if (tst == 1)
       {
                  writelog (buffer);
       }
       else if (tst == 2)
       {
                  disp_mess (buffer, 0);
       }
       _WMDEBUG = 0;
       return (0);
}
