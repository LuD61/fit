#ifndef _TEXTBLOCK_DEF
#define _TEXTBLOCK_DEF
#include "Vector.h"

class TEXTBLOCK 
{
       private :
             int break_num_enter;
             form *eForm;
             HWND ehWnd;
             int (*MeLPProc) (HWND);
             void (*SetNumCaret) (void);
             BOOL Num3Mode;
             HWND NumParent;
             BOOL NumZent;                  // zentriert in x-Richtung  
             int Numlcx;                    // Fenster-breite  
             int Numlcy;                    // y - Abstand vom unteren Rand
             char neditbuff [40];
             double neditsum;
             char neaction;
             mfont nTextNumFont;
             field _nTextForm[1]; 
             form nTextForm;
             mfont nEditNumFont;
             field _nEditForm[1]; 
             form nEditForm;
			 BOOL TopMost;

       public :
		   static CVector Rows;
		   static int Row;
           TEXTBLOCK ()
           {
                break_num_enter = 0;
                eForm = NULL;
                ehWnd = NULL;
                MeLPProc = NULL;
                SetNumCaret = NULL;
                Num3Mode = FALSE;
                NumParent = NULL;
                NumZent = FALSE; 
                Numlcx = 0;    
                Numlcy = 0;    
                neaction = ' ';
                _nEditForm[0].feld     = neditbuff;
                _nEditForm[0].length   = 0;
                _nEditForm[0].rows     = 0;
                _nEditForm[0].pos[0]   = 0;
                _nEditForm[0].pos[1]   = 0;
                _nEditForm[0].feldid   = NULL;
                _nEditForm[0].picture  = "";
                _nEditForm[0].attribut = EDIT;
                _nEditForm[0].before   = NULL;
                _nEditForm[0].after    = NULL;
                _nEditForm[0].BuId    = 0;

                nEditNumFont.FontName       = "Courier New";
                nEditNumFont.FontHeight     = 200;
                nEditNumFont.FontWidth      = 0;
                nEditNumFont.FontAttribute  = 1;
                nEditNumFont.FontColor      = RGB (0, 0, 0) ;
                nEditNumFont.FontBkColor    = RGB (255, 255, 255);
                nEditNumFont.PosMode        = 1;
                nEditNumFont.hFont          = NULL;

                nEditForm.fieldanz  = 1;
                nEditForm.frmstart  = 0;
                nEditForm.frmscroll = 0;
                nEditForm.mask      = _nEditForm;
                nEditForm.caption   = 0;
                nEditForm.before    = NULL;
                nEditForm.after     = NULL;
                nEditForm.cbfield   = 0;
                nEditForm.font      = &nEditNumFont;

                _nTextForm[0].feld     = neditbuff;
                _nTextForm[0].length   = 0;
                _nTextForm[0].rows     = 0;
                _nTextForm[0].pos[0]   = 0;
                _nTextForm[0].pos[1]   = 0;
                _nTextForm[0].feldid   = NULL;
                _nTextForm[0].picture  = "";
                _nTextForm[0].attribut = EDIT;
                _nTextForm[0].before   = NULL;
                _nTextForm[0].after    = NULL;
                _nTextForm[0].BuId    = 0;

                nTextNumFont.FontName       = "Courier New";
                nTextNumFont.FontHeight     = 200;
                nTextNumFont.FontWidth      = 0;
                nTextNumFont.FontAttribute  = 1;
                nTextNumFont.FontColor      = RGB (0, 0, 0) ;
                nTextNumFont.FontBkColor    = RGB (192, 192, 192);
                nTextNumFont.PosMode        = 1;
                nTextNumFont.hFont          = NULL;

                nTextForm.fieldanz  = 1;
                nTextForm.frmstart  = 0;
                nTextForm.frmscroll = 0;
                nTextForm.mask      = _nTextForm;
                nTextForm.caption   = 0;
                nTextForm.before    = NULL;
                nTextForm.after     = NULL;
                nTextForm.cbfield   = 0;
                nTextForm.font      = &nTextNumFont;
				Rows.Init ();
				Row = 0;
				TopMost = FALSE;
           }

		   void SetTopMost (BOOL TopMost)
		   {
			   this->TopMost = TopMost;
		   }

           void SetEform (form *eF, HWND ehW, int (*eFP) (WPARAM))
           {
	         eForm     = eF;
 	         ehWnd     = ehW;
           }

           void SetMeProc (int (*MeLp) (HWND))
           {
	        MeLPProc = MeLp;
           }

           void SetNumCaretProc (void (*NuCr) (void))
           {
	         SetNumCaret = NuCr;
           }

           void SetNum3 (BOOL mode)
           {
	         Num3Mode = min (TRUE, mode);
           }

           void SetNumParent (HWND hWnd)
           {
	         NumParent = hWnd;
           }

           void SetNumZent (BOOL mode, int x, int y)
           {
	         NumZent = mode;
	         Numlcx = x;              
	         Numlcy = y;
           }
		   void SetLongText (BOOL);
           void MainInstance (HANDLE, HWND);
           void SetParent (HWND);
           void ScreenParam (double scrx, double scry);
           void SetStorno (int (*) (void), int);	    
           void SetChoise (int (*) (void), int);
           void SetUpshift (BOOL);
           void NumEnterBreak ();
           int IsNumKey (MSG *);
           HWND IsNumChild (POINT *);
           static void RegisterNumEnter (WNDPROC);
           int sNumKey (MSG *);
           void KorrMainButton (form *,  double, double);
		   void fillAlphaString (void);
		   void fillAlphaField (void);
           void CentAlphaField (int);
           void EnterTextBox (HWND, char *, char *,  int, char *, WNDPROC);
           int OnPaint (HWND,UINT, WPARAM,LPARAM);
           int OnButton (HWND,UINT, WPARAM,LPARAM);
};

#endif