#ifndef _FANGGEBIETDEF
#define _FANGGEBIETDEF
#include "dbclass.h"

struct SAIS {
   short     delstatus;
   short     jr;
   char      pers_nam[9];
   short     sais;
   long      st_aktiv;
   long      st_bearbeit;
   char      st_bz_1[25];
   char      st_bz_2[25];
   long      st_nordb;
   long      st_nordv;
   long      st_vkztb;
   long      st_vkztv;
   long      st_vordb;
   long      st_vordv;
};
extern struct SAIS sais, sais_null;

#line 6 "fanggebiet.rh"

class FANGGEBIET_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
               short cursor_fanggebiet;

       public :
               FANGGEBIET_CLASS () : DB_CLASS ()
               {
                    cursor_fanggebiet = -1;

               }
               int lese_fanggebiet (void);
               int open_fanggebiet (void);
};
#endif
