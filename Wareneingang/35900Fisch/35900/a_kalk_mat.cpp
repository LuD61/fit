#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "wmaskc.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "a_kalk_mat.h"

struct A_KALK_MAT a_kalk_mat, a_kalk_mat_null;

void A_KALK_MAT_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &a_kalk_mat.mdn, 1, 0);
            ins_quest ((char *) &a_kalk_mat.fil, 1, 0);
            ins_quest ((char *) &a_kalk_mat.a, 3, 0);
    out_quest ((char *) &a_kalk_mat.a,3,0);
    out_quest ((char *) &a_kalk_mat.bearb_sk,1,0);
    out_quest ((char *) &a_kalk_mat.delstatus,1,0);
    out_quest ((char *) &a_kalk_mat.fil,1,0);
    out_quest ((char *) &a_kalk_mat.hk_teilk,3,0);
    out_quest ((char *) &a_kalk_mat.hk_vollk,3,0);
    out_quest ((char *) &a_kalk_mat.kost,3,0);
    out_quest ((char *) &a_kalk_mat.mat_o_b,3,0);
    out_quest ((char *) &a_kalk_mat.mdn,1,0);
    out_quest ((char *) &a_kalk_mat.sp_hk,3,0);
    out_quest ((char *) &a_kalk_mat.dat,2,0);
            cursor = prepare_sql ("select a_kalk_mat.a,  "
"a_kalk_mat.bearb_sk,  a_kalk_mat.delstatus,  a_kalk_mat.fil,  "
"a_kalk_mat.hk_teilk,  a_kalk_mat.hk_vollk,  a_kalk_mat.kost,  "
"a_kalk_mat.mat_o_b,  a_kalk_mat.mdn,  a_kalk_mat.sp_hk,  "
"a_kalk_mat.dat from a_kalk_mat "

#line 22 "a_kalk_mat.rpp"
                                  "where mdn = ? "
                                  "and fil = ? "
                                  "and a = ?");
    ins_quest ((char *) &a_kalk_mat.a,3,0);
    ins_quest ((char *) &a_kalk_mat.bearb_sk,1,0);
    ins_quest ((char *) &a_kalk_mat.delstatus,1,0);
    ins_quest ((char *) &a_kalk_mat.fil,1,0);
    ins_quest ((char *) &a_kalk_mat.hk_teilk,3,0);
    ins_quest ((char *) &a_kalk_mat.hk_vollk,3,0);
    ins_quest ((char *) &a_kalk_mat.kost,3,0);
    ins_quest ((char *) &a_kalk_mat.mat_o_b,3,0);
    ins_quest ((char *) &a_kalk_mat.mdn,1,0);
    ins_quest ((char *) &a_kalk_mat.sp_hk,3,0);
    ins_quest ((char *) &a_kalk_mat.dat,2,0);
            sqltext = "update a_kalk_mat set "
"a_kalk_mat.a = ?,  a_kalk_mat.bearb_sk = ?,  "
"a_kalk_mat.delstatus = ?,  a_kalk_mat.fil = ?,  "
"a_kalk_mat.hk_teilk = ?,  a_kalk_mat.hk_vollk = ?,  "
"a_kalk_mat.kost = ?,  a_kalk_mat.mat_o_b = ?,  a_kalk_mat.mdn = ?,  "
"a_kalk_mat.sp_hk = ?,  a_kalk_mat.dat = ? "

#line 26 "a_kalk_mat.rpp"
                                  "where mdn = ? "
                                  "and fil = ? "
                                  "and a = ?";
            ins_quest ((char *) &a_kalk_mat.mdn, 1, 0);
            ins_quest ((char *) &a_kalk_mat.fil, 1, 0);
            ins_quest ((char *) &a_kalk_mat.a, 3, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &a_kalk_mat.mdn, 1, 0);
            ins_quest ((char *) &a_kalk_mat.fil, 1, 0);
            ins_quest ((char *) &a_kalk_mat.a, 3, 0);
            test_upd_cursor = prepare_sql ("select a from a_kalk_mat "
                                  "where mdn = ? "
                                  "and fil = ? "
                                  "and a = ?");
            ins_quest ((char *) &a_kalk_mat.mdn, 1, 0);
            ins_quest ((char *) &a_kalk_mat.fil, 1, 0);
            ins_quest ((char *) &a_kalk_mat.a, 3, 0);
            del_cursor = prepare_sql ("delete from a_kalk_mat "
                                  "where mdn = ? "
                                  "and fil = ? "
                                  "and a = ?");
    ins_quest ((char *) &a_kalk_mat.a,3,0);
    ins_quest ((char *) &a_kalk_mat.bearb_sk,1,0);
    ins_quest ((char *) &a_kalk_mat.delstatus,1,0);
    ins_quest ((char *) &a_kalk_mat.fil,1,0);
    ins_quest ((char *) &a_kalk_mat.hk_teilk,3,0);
    ins_quest ((char *) &a_kalk_mat.hk_vollk,3,0);
    ins_quest ((char *) &a_kalk_mat.kost,3,0);
    ins_quest ((char *) &a_kalk_mat.mat_o_b,3,0);
    ins_quest ((char *) &a_kalk_mat.mdn,1,0);
    ins_quest ((char *) &a_kalk_mat.sp_hk,3,0);
    ins_quest ((char *) &a_kalk_mat.dat,2,0);
            ins_cursor = prepare_sql ("insert into a_kalk_mat ("
"a,  bearb_sk,  delstatus,  fil,  hk_teilk,  hk_vollk,  kost,  mat_o_b,  mdn,  sp_hk,  dat) "

#line 49 "a_kalk_mat.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?)"); 

#line 51 "a_kalk_mat.rpp"
}

