#include<stdio.h>
#include<stdarg.h>
#include<string.h>
#include<windows.h>
#include "stdfkt.h"
#include "strfkt.h"
#include "spezdlg.h" 
#include "mo_gdruck.h" 
#include "colbut.h"
#include "help.h"
#include "lbox.h"
#include "searchmdn.h"
#include "searchfil.h"
#include "searchnr.h"
#include "searchlief.h"
#include "lipreise_k.h"
#include "lipreise_p.h"
#include "ptab.h"
#ifdef BIWAK
#include "conf_env.h"
#endif
#include "mo_progcfg.h"
#include "datum.h"

struct DEFAULTFF defaultlistf, defaultlistf_null;

static int StdSize = STDSIZE;

static int dlgsize    = STDSIZE;
static int ltgraysize = STDSIZE;
static int dlgpossize = STDSIZE;
static int cpsize     = 200;

static struct PMENUE MenuePosEnter = {"Positionen erfassen F6",      "G",  NULL, IDM_LIST}; 
static struct PMENUE MenueInsert   = {"einf�gen F6",                 "G",  NULL, IDM_INSERT}; 


static mfont dlgfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         GRAYCOL,
                         0,
                         NULL};

static mfont ltgrayfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         LTGRAYCOL,
                        0,
                         NULL};

static mfont checkfonth = {
                           "ARIAL", STDSIZE, 0, 0,
                            BLACKCOL,
                            LTGRAYCOL,
                            0,
                            NULL};

static mfont dlgposfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         LTGRAYCOL,
                         0,
                         NULL};


static mfont cpfont = {
                        "Times New Roman", 200, 0, 1,
                         BLUECOL,
                         GRAYCOL,
                         0,
                         NULL};



COLORREF SpezDlg::Colors[] = {BLACKCOL,
                            WHITECOL,
                            WHITECOL,
                            WHITECOL};

COLORREF SpezDlg::BkColors[] = {WHITECOL,
                            BLUECOL,
                            BLACKCOL,
                            GRAYCOL};

HBITMAP SpezDlg::ListBitmap  = NULL;
HBITMAP SpezDlg::ListBitmapi = NULL;
BitImage SpezDlg::ImageDelete;
BitImage SpezDlg::ImageInsert;
BitImage SpezDlg::ImageList;
COLORREF SpezDlg::SysBkColor = LTGRAYCOL;
BOOL SpezDlg::ListButtons = FALSE;
int SpezDlg::ButtonSize = SMALL;
double SpezDlg::ArtikelVon = 1;
double SpezDlg::ArtikelBis = 9999999999;
int SpezDlg::liste_breit_ab = 10;

mfont *SpezDlg::Font = &dlgposfont;

CFIELD *SpezDlg::_fHead [] = {

// Hier Kopffelder eintragen

                     new CFIELD ("mdn_txt", "Mandant",  0, 0, 1, 1,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("mdn", defaultlistf.mdn,  6, 0, 18, 1,  NULL, "%4d", CEDIT,
                                 MDN_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("mdn_choise", "", 2, 0, 24, 1, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("mdn_krz", defaultlistf.mdn_krz, 19, 0, 30, 1,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, 0),
                     new CFIELD ("fil", defaultlistf.fil,  6, 0, 58, 1,  NULL, "%4d", CREMOVED,
                                 FIL_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("fil_choise", "", 2, 0, 64, 1, NULL, "", CREMOVED,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("lipreise_txt", "Listennummer",  0, 0, 1, 2,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("lipreise_txt", "Artikel hinzuf�gen",  0, 0, 87, 1,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("lipreise_nr", defaultlistf.nr, 6, 0, 18, 2,  NULL, "%4d", CEDIT,
                                 HWG_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("nr_choise", "", 2, 0, 24, 2, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("lief_txt", "Lieferanten",  0, 0, 1, 4,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("kw_txt", "Woche / Jahr",  0, 0, 1, 3,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("w_j_txt", "/",  0, 0, 23, 3,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("datumvon_txt", "Datum : ",  0, 0, 41, 3,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("datumbis_txt", "bis:",  0, 0, 60, 3,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
/***
                     new CFIELD ("datumvon_txt", "Datum                von: ",  0, 0, 1, 3,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("datumbis_txt", "bis:",  0, 0, 31, 3,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
*****/
                     new CFIELD ("basispreis_txt", "Basis Preis:",  0, 0, 87, 3,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     NULL,
};

CFORM SpezDlg::fHead (16, _fHead);


CFIELD *SpezDlg::_fPos [] = {
                     new CFIELD ("pos_frame", "",    200,13, 0, 6,  NULL, "", 
                                 CBORDER,   
                                 500, Font, 0, TRANSPARENT),    
                     new CFIELD ("lipreise_a", defaultlistf.a, 13, 0, 103, 1,  NULL, "%0.lf", CEDIT,
                                 A_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("lipreise_a_choise", "", 2, 0, 116, 1, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("lipreise_nr_bz", defaultlistf.nr_bz, 26, 0, 30, 2,  NULL, "", 
                                 CEDIT,
                                 NR_BZ_CTL, Font, 0, 0),
                     new CFIELD ("kw", defaultlistf.kw, 4, 0, 18, 3,  NULL, "%2d", CEDIT,
                                 KW_CTL, Font, 0, ES_RIGHT),

                     new CFIELD ("jr", defaultlistf.jr, 6, 0, 25, 3,  NULL, "%4d", CEDIT,
                                 JR_CTL, Font, 0, ES_RIGHT),


                     new CFIELD ("dat_von", defaultlistf.dat_von, 11, 0,48,3,  NULL, "dd.mm.yyyy", 
                                 CEDIT,
                                 DAT_VON_CTL, Font, 0, 0),

                     new CFIELD ("dat_bis", defaultlistf.dat_bis, 11, 0,64,3,  NULL, "dd.mm.yyyy", 
                                 CEDIT,
                                 DAT_BIS_CTL, Font, 0, 0),
                     new CFIELD ("basis_preis", defaultlistf.basis_preis, 13, 0, 103, 3,  NULL, "%.2lf", CEDIT,
                                 BPREIS_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("lief1", defaultlistf.lief1, 8, 0, 18, 4,  NULL, "%s", CEDIT,
                                 LIEF1_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("lief1_choise", "", 2, 0, 26, 4, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),

                     new CFIELD ("lief2", defaultlistf.lief2, 8, 0, 28, 4,  NULL, "%d", CEDIT,
                                 LIEF2_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("lief2_choise", "", 2, 0, 36, 4, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("lief3", defaultlistf.lief3, 8, 0, 38, 4,  NULL, "%d", CEDIT,
                                 LIEF3_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("lief3_choise", "", 2, 0, 46, 4, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("lief4", defaultlistf.lief4, 8, 0, 48, 4,  NULL, "%d", CEDIT,
                                 LIEF4_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("lief4_choise", "", 2, 0, 56, 4, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("lief5", defaultlistf.lief5, 8, 0, 58, 4,  NULL, "%d", CEDIT,
                                 LIEF5_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("lief5_choise", "", 2, 0, 66, 4, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("lief6", defaultlistf.lief6, 8, 0, 68, 4,  NULL, "%d", CEDIT,
                                 LIEF6_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("lief6_choise", "", 2, 0, 76, 4, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("lief7", defaultlistf.lief7, 8, 0, 78, 4,  NULL, "%d", CEDIT,
                                 LIEF7_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("lief7_choise", "", 2, 0, 86, 4, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("lief8", defaultlistf.lief8, 8, 0, 88, 4,  NULL, "%d", CEDIT,
                                 LIEF8_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("lief8_choise", "", 2, 0, 96, 4, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("lief9", defaultlistf.lief9, 8, 0, 98, 4,  NULL, "%d", CEDIT,
                                 LIEF9_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("lief9_choise", "", 2, 0, 106, 4, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("lief10", defaultlistf.lief10, 8, 0, 108, 4,  NULL, "%d", CEDIT,
                                 LIEF10_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("lief10_choise", "", 2, 0, 116, 4, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("lief11", defaultlistf.lief11, 8, 0, 18, 5,  NULL, "%d", CEDIT,
                                 LIEF11_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("lief11_choise", "", 2, 0, 26, 5, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("lief12", defaultlistf.lief12, 8, 0, 28, 5,  NULL, "%d", CEDIT,
                                 LIEF12_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("lief12_choise", "", 2, 0, 36, 5, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("lief13", defaultlistf.lief13, 8, 0, 38, 5,  NULL, "%d", CEDIT,
                                 LIEF13_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("lief13_choise", "", 2, 0, 46, 5, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("lief14", defaultlistf.lief14, 8, 0, 48, 5,  NULL, "%d", CEDIT,
                                 LIEF14_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("lief14_choise", "", 2, 0, 56, 5, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
// Hier Positionsfelder eintragen

                     NULL,
};



CFORM SpezDlg::fPos (35, _fPos);


CFIELD *SpezDlg::_fcField [] = {
                     new CFIELD ("fcField1", (CFORM *) &fHead, 0, 0, 0, 0, NULL, "", CFFORM,
                                   HEADCTL, Font, 0, 0),
                     new CFIELD ("fcField2", (CFORM *) &fPos,  0, 0, 0, 0, NULL, "", CFFORM,
                                   POSCTL, Font, 0, 0),
                     NULL,                      
};

CFORM SpezDlg::fcForm (2, _fcField);

CFIELD *SpezDlg::_fcField0 [] = { 
                     new CFIELD ("fcForm", (CFORM *) &fcForm, 0, 0, -1, 1, NULL, "", CFFORM,
                                   500, Font, 0, 0),
                     NULL,
};
CFORM SpezDlg::fcForm0 (1, _fcField0);

Work SpezDlg::work;
char *SpezDlg::HelpName = "defaultlist.cmd";
SpezDlg *SpezDlg::ActiveSpezDlg = NULL;
HBITMAP SpezDlg::SelBmp;
int SpezDlg::pos = 0;
char SpezDlg::ptwert[5];


void SpezDlg::ChangeF6 (DWORD Id)
{

    if (Id == IDM_LIST)
    {
         if (DeleteMenu (ActiveDlg->GethMenu (), IDM_LIST , MF_BYCOMMAND)) 
         {
                InsertMenu (ActiveDlg->GethMenu (), IDM_DOCKLIST, MF_BYCOMMAND | MF_STRING, 
                            MenueInsert.menid, MenueInsert.text);
                            
         }
    }
    else if (Id == IDM_INSERT)
    {
         if (DeleteMenu (ActiveDlg->GethMenu (), IDM_INSERT , MF_BYCOMMAND))
         {
                 InsertMenu (ActiveDlg->GethMenu (), IDM_DOCKLIST, MF_BYCOMMAND | MF_STRING, 
                            MenuePosEnter.menid, MenuePosEnter.text);
         }
    }
}

int SpezDlg::ReadMdn (void)
{
         if (syskey == 0) return 0;
         if (syskey == KEY9) return 0;

         fPos.GetText ();
         FromForm (dbheadfields);
         work.GetMdnName (defaultlistf.mdn_krz,   atoi (defaultlistf.mdn));
         if (atoi (defaultlistf.mdn) == 0)
         {
             sprintf (defaultlistf.fil, "%4d", 0);
             work.GetFilName (defaultlistf.fil_krz,   atoi (defaultlistf.mdn), atoi (defaultlistf.fil));
             Enable (&fcForm, EnableHeadFil, FALSE); 
             fHead.SetCurrentName ("text1");
         }
         else
         {
             if (fHead.GetCfield ("fil")->IsDisabled ())
             {
                    Enable (&fcForm, EnableHeadFil, TRUE); 
                    fHead.SetCurrentName ("fil");
             }
         }
         if (syskey == KEYUP)
         {
                    fHead.SetCurrentName ("kun");
         }
         fHead.SetText ();
         return 0;
}

int SpezDlg::ReadFil (void)
{
          
         if (syskey == 0) return 0;
         if (syskey == KEY9) return 0;
         fPos.GetText ();
         FromForm (dbheadfields);
         work.GetFilName (defaultlistf.fil_krz,   atoi (defaultlistf.mdn), atoi (defaultlistf.fil));
         fHead.SetText ();
         return 0;
}

int SpezDlg::HoleDatum (void)
{
	short kw, jr;
	int StartDat;
          
         if (syskey == 0) return 0;
         if (syskey == KEY9) return 0;
         fPos.GetText ();
		kw =  atoi (defaultlistf.kw) ;
		jr =  atoi (defaultlistf.jr) ;
		if (kw < 1 || kw > 53)
		{
	            fHead.SetCurrentName ("kw");
				return 0;
		}
		if (jr < 2000)
		{
	            fHead.SetCurrentName ("jr");
				return 0;
		}
        StartDat = first_kw_day ((short) kw, (short) jr);


         dlong_to_asc (StartDat, defaultlistf.dat_von);
         dlong_to_asc (StartDat+6, defaultlistf.dat_bis);
         fHead.SetText ();
         fPos.SetText ();


		 return 0;
}
int SpezDlg::ReadNr (void)
{
          
         if (syskey == 0) return 0;
         if (syskey == KEY9) return 0;
         fPos.GetText ();
		 if (work.GetMatrix () == LIEFERANT && atoi (defaultlistf.nr) == 0)
		 {
			 return 0;
		 }

         Enable (&fcForm, EnableHead, FALSE); 
         Enable (&fcForm, EnablePos, TRUE); 
         FromForm (dbheadfields);
//         if (work.GetNrBez (defaultlistf.nr_bz,   atoi (defaultlistf.nr)) == 100)
         if (work.Readlipreise_k (atoi(defaultlistf.mdn),   atoi (defaultlistf.nr)) == 100)
         {
                 disp_mess ("Listennummer nicht gefunden, wird neu angelegt",2);
                 fPos.SetCurrentName ("lipreise_nr_bz");
                 return 0;
         }
         ToForm (dbfields);
         dlong_to_asc (lipreise_k.dat_von, defaultlistf.dat_von);
         dlong_to_asc (lipreise_k.dat_bis, defaultlistf.dat_bis);
         fHead.SetText ();
         fPos.SetText ();
         fPos.SetCurrentName ("lipreise_nr_bz");
             ToolBar_SetState (ActiveDlg->GethwndTB (),   IDM_DELALL,   TBSTATE_ENABLED);
//             EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELETE,   MF_ENABLED);
             EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELALL,   MF_ENABLED);
         return 0;
}

int SpezDlg::ArtikelHinzufuegen (void)
{
          
         if (syskey == KEY5) return 0;
         if (syskey == 0) return 0;
         if (syskey == KEY9) return 0;
         fPos.GetText ();
		 if (work.GetMatrix () == LIEFERANT && atoi (defaultlistf.nr) == 0)
		 {
			 return 0;
		 }

         Enable (&fcForm, EnableHead, FALSE); 
         Enable (&fcForm, EnablePos, TRUE); 
         FromForm (dbfields);
         lipreise_k.dat_von = dasc_to_long (defaultlistf.dat_von);
         lipreise_k.dat_bis = dasc_to_long (defaultlistf.dat_bis);
		 lipreise_p.mdn = lipreise_k.mdn;
		 lipreise_p.nr = lipreise_k.nr;
		 if (lipreise_p.a > 0.0)
		 {
               work.BeginWork ();
	           work.Writelipreise_p ();
               work.CommitWork ();
			   lipreise_p.a = 0.0;
	         ToForm (dbfields); 
	         dlong_to_asc (lipreise_k.dat_von, defaultlistf.dat_von);
		     dlong_to_asc (lipreise_k.dat_bis, defaultlistf.dat_bis);
		     fHead.SetText ();
			 fPos.SetText ();
			fPos.SetCurrentName ("lipreise_a");
			return 0;
		 }
         ToForm (dbfields); 
         dlong_to_asc (lipreise_k.dat_von, defaultlistf.dat_von);
         dlong_to_asc (lipreise_k.dat_bis, defaultlistf.dat_bis);
         fHead.SetText ();
         fPos.SetText ();
         return 0;
}

int SpezDlg::EnterPos (void)
{

//Hier Postionsdialog eintragen, falls vorhanden.
/*
         switch (syskey)
         {
               case KEYDOWN :
               case KEYTAB :
                   fHead.SetCurrentName ("mdn");
                   return TRUE;
         }
        
         if (DLG::MousePressed)
         {
             return FALSE;
         }

         if (syskey != KEYCR)
         {
             return FALSE;
         }

         
         fHead.GetText ();

         FromForm (dbheadfields);

         work.BeginWork ();
*/

         ToolBar_SetState (ActiveDlg->GethwndTB (),   IDM_DELALL, TBSTATE_ENABLED);
         EnableMenuItem (ActiveDlg->GethMenu (),      IDM_LIST,     MF_ENABLED);
         EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELETE,   MF_GRAYED);
         EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELALL,   MF_GRAYED);
         EnableMenuItem (ActiveDlg->GethMenu (),      VK_INSERT,    MF_GRAYED);
         EnableMenuItem (ActiveDlg->GethMenu (),      VK_F10,       MF_GRAYED);
         if (ActiveSpezDlg->GetToolbar2 () != NULL)
         {
                 if  (ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f6") != NULL)
                 {
                      ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f6")->GetFeld ())->bmp = 
                                   ImageList.Image;
                 }
                 ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("del")->GetFeld ())->bmp = 
					 ImageDelete.Inactive;
                 ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("insert")->GetFeld ())->bmp = 
					 ImageInsert.Inactive;
                 ActiveSpezDlg->GetToolbar2 ()->display ();
         }
// Bei zus�tzlichen Positionseingaben Positionsfelder aktiv setzen
         Enable (&fcForm, EnableHead, FALSE); 
         Enable (&fcForm, EnablePos,  TRUE); 
// Erstes Positionsfeld setzen. 
         fPos.SetCurrentName ("lipreise_nr_bz");

         return 0;
}


int SpezDlg::GetPtab (void)
{
//         char wert [5];
         CFIELD *Cfield;


         fPos.GetText ();

         Cfield = ActiveDlg->GetCurrentCfield ();
         return 0;
}


ItProg *SpezDlg::After [] = {
                                   new ItProg ("mdn",         ReadMdn),
                                   new ItProg ("fil",         ReadFil),
                                   new ItProg ("lipreise_nr",         ReadNr),
                                   new ItProg ("lipreise_a",         ArtikelHinzufuegen),
                                   new ItProg ("kw",         HoleDatum),
                                   new ItProg ("jr",         HoleDatum),
                                   NULL,
};

ItProg *SpezDlg::Before [] = {NULL,
};

ItFont *SpezDlg::itFont [] = {
                                  new ItFont ("mdn_krz",       &ltgrayfont),
                                  new ItFont ("nr_bz",          &ltgrayfont),
                                  NULL
};

FORMFIELD *SpezDlg::dbheadfields [] = {
         new FORMFIELD ("mdn",       defaultlistf.mdn,       (short *)   &lipreise_k.mdn,       FSHORT,   NULL),
         new FORMFIELD ("lipreise_nr",       defaultlistf.nr,       (short *)   &lipreise_k.nr,       FSHORT,   NULL),
           NULL,
};

FORMFIELD *SpezDlg::dbfields [] = {
         new FORMFIELD ("lipreise_nr_bz",       defaultlistf.nr_bz,       (short *)   &lipreise_k.nr_bz,       FCHAR,   NULL),
         new FORMFIELD ("lief1",       defaultlistf.lief1,       (short *)   &lipreise_k.lief1,       FCHAR,   NULL),
         new FORMFIELD ("lief2",       defaultlistf.lief2,       (short *)   &lipreise_k.lief2,       FCHAR,   NULL),
         new FORMFIELD ("lief3",       defaultlistf.lief3,       (short *)   &lipreise_k.lief3,       FCHAR,   NULL),
         new FORMFIELD ("lief4",       defaultlistf.lief4,       (short *)   &lipreise_k.lief4,       FCHAR,   NULL),
         new FORMFIELD ("lief5",       defaultlistf.lief5,       (short *)   &lipreise_k.lief5,       FCHAR,   NULL),
         new FORMFIELD ("lief6",       defaultlistf.lief6,       (short *)   &lipreise_k.lief6,       FCHAR,   NULL),
         new FORMFIELD ("lief7",       defaultlistf.lief7,       (short *)   &lipreise_k.lief7,       FCHAR,   NULL),
         new FORMFIELD ("lief8",       defaultlistf.lief8,       (short *)   &lipreise_k.lief8,       FCHAR,   NULL),
         new FORMFIELD ("lief9",       defaultlistf.lief9,       (short *)   &lipreise_k.lief9,       FCHAR,   NULL),
         new FORMFIELD ("lief10",       defaultlistf.lief10,       (short *)   &lipreise_k.lief10,       FCHAR,   NULL),
         new FORMFIELD ("lief11",       defaultlistf.lief11,       (short *)   &lipreise_k.lief11,       FCHAR,   NULL),
         new FORMFIELD ("lief12",       defaultlistf.lief12,       (short *)   &lipreise_k.lief12,       FCHAR,   NULL),
         new FORMFIELD ("lief13",       defaultlistf.lief13,       (short *)   &lipreise_k.lief13,       FCHAR,   NULL),
         new FORMFIELD ("lief14",       defaultlistf.lief14,       (short *)   &lipreise_k.lief14,       FCHAR,   NULL),
         new FORMFIELD ("lipreise_a",       defaultlistf.a,       (short *)   &lipreise_p.a,       FDOUBLE,   NULL),
         new FORMFIELD ("basis_preis",       defaultlistf.basis_preis,     (short *)   &lipreise_k.basis_preis,       FDOUBLE,   NULL),
         new FORMFIELD ("kw",       defaultlistf.kw,     (short *)   &lipreise_k.kw,       FSHORT,   NULL),
         new FORMFIELD ("jr",       defaultlistf.jr,     (short *)   &lipreise_k.jr,       FSHORT,   NULL),
         NULL,
};



char *SpezDlg::EnableHead [] = {
                                "mdn",
                                "mdn_choise",
                                "fil",
                                "fil_choise",
                                "lipreise_nr",
                                "nr_choise",
                                 NULL
};

char *SpezDlg::EnableHeadFil [] = {
                                     "fil",
                                     NULL
};


char *SpezDlg::EnablePos[] = {
                                "lipreise_nr_bz",
								"jr",
								"kw",
								"dat_von",
								"dat_bis",
								"basis_preis",
                                "lief1",
                                "lief2",
                                "lief3",
                                "lief4",
                                "lief5",
                                "lief6",
                                "lief7",
                                "lief8",
                                "lief9",
                                "lief10",
                                "lief11",
                                "lief12",
                                "lief13",
                                "lief14",
								"lief1_choise",
								"lief2_choise",
								"lief3_choise",
								"lief4_choise",
								"lief5_choise",
								"lief6_choise",
								"lief7_choise",
								"lief8_choise",
								"lief9_choise",
								"lief10_choise",
								"lief11_choise",
								"lief12_choise",
								"lief13_choise",
								"lief14_choise",
								"lipreise_a",
								"lipreise_a_choise",
                               NULL,
};

char *SpezDlg::EnableA [] = {
                                     "a",
                                     "a_choice",
                                     NULL
};

                         
SpezDlg::SpezDlg (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel) :
         DLG (x, y, cx, cy, Caption, Size, Pixel)
{
          
             BorderType = HIGHCOLBORDER;
             Init (x, y, cx, cy, Caption, Size, Pixel);
}

SpezDlg::SpezDlg (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel, 
                  int BorderType) :
         DLG (x, y, cx, cy, Caption, Size, Pixel)
{
          
             this->BorderType = BorderType;
             Init (x, y, cx, cy, Caption, Size, Pixel);
}

SpezDlg::SpezDlg (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel, 
                  int BorderType, int EnterM) :
         DLG (x, y, cx, cy, Caption, Size, Pixel)
{
          
             this->BorderType = BorderType;
//             EnterMode = EnterM;
             Init (x, y, cx, cy, Caption, Size, Pixel);
}


void SpezDlg::Init (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel)
{
             PROG_CFG ProgCfg ("18510");
//             char cfg_v [256];
             int i;
             int xfull, yfull;

//             Work.Prepare ();

             ImageDelete.Image    = BMAP::LoadBitmap (hInstance, "DEL",       "DELMASK",      SysBkColor);
             ImageDelete.Inactive = BMAP::LoadBitmap (hInstance, "DELIA",     "DELMASK",      SysBkColor);
             ImageInsert.Image    = BMAP::LoadBitmap (hInstance, "INSERT",    "INSERTMASK",   SysBkColor);
             ImageInsert.Inactive = BMAP::LoadBitmap (hInstance, "INSERTIA",  "INSERTMASK",   SysBkColor);
             ImageList.Image      = BMAP::LoadBitmap (hInstance, "ARRDOWND",  "ARRDOWNDMASK", SysBkColor);
             ImageList.Inactive   = BMAP::LoadBitmap (hInstance, "ARRDOWNDI","ARRDOWNDMASK", SysBkColor);
             hwndCombo1 = NULL;
             hwndCombo2 = NULL;
             Combo1 = NULL;
             Combo2 = NULL;
             ColorSet = FALSE;
             LineSet = FALSE;
             Toolbar2 = NULL;
             DockList = TRUE;
             ListDlg = NULL;
             hwndList = NULL;

             ActiveSpezDlg = this; 
             xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
             yfull = GetSystemMetrics (SM_CYFULLSCREEN);        

             ltgrayfont.FontBkColor = SysBkColor;

             StdSize = Size;
             dlgfont.FontHeight    = Size;
             dlgposfont.FontHeight = Size;
             ltgrayfont.FontHeight = Size;
             cpfont.FontHeight     = cpsize;


             if (xfull < 1000) 
             {
                 dlgfont.FontHeight = 90;
                 dlgposfont.FontHeight = 90;
                 ltgrayfont.FontHeight = 90;
                 checkfonth.FontHeight = 90;
                 cpfont.FontHeight = 150;
             }

             if (xfull < 800) 
             {

                 dlgfont.FontHeight = 70;
                 dlgposfont.FontHeight = 70;
                 ltgrayfont.FontHeight = 70;
                 checkfonth.FontHeight = 70;
                 cpfont.FontHeight = 120;
             }

             fHead.SetFieldanz ();
             fPos.SetFieldanz ();
             fcForm.SetFieldanz ();

             fHead.GetCfield ("mdn_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fHead.GetCfield ("fil_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fHead.GetCfield ("nr_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fPos.GetCfield ("lief1_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fPos.GetCfield ("lief2_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fPos.GetCfield ("lief3_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fPos.GetCfield ("lief4_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fPos.GetCfield ("lief5_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fPos.GetCfield ("lief6_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fPos.GetCfield ("lief7_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fPos.GetCfield ("lief8_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fPos.GetCfield ("lief9_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fPos.GetCfield ("lief10_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fPos.GetCfield ("lief11_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fPos.GetCfield ("lief12_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fPos.GetCfield ("lief13_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fPos.GetCfield ("lief14_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fPos.GetCfield ("lipreise_a_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));

             fHead.GetCfield ("mdn_choise")->SetTabstop (FALSE); 
             fHead.GetCfield ("fil_choise")->SetTabstop (FALSE); 
             fHead.GetCfield ("nr_choise")->SetTabstop (FALSE); 

             fPos.GetCfield ("lief1_choise")->SetTabstop (FALSE); 
             fPos.GetCfield ("lief2_choise")->SetTabstop (FALSE); 
             fPos.GetCfield ("lief3_choise")->SetTabstop (FALSE); 
             fPos.GetCfield ("lief4_choise")->SetTabstop (FALSE); 
             fPos.GetCfield ("lief5_choise")->SetTabstop (FALSE); 
             fPos.GetCfield ("lief6_choise")->SetTabstop (FALSE); 
             fPos.GetCfield ("lief7_choise")->SetTabstop (FALSE); 
             fPos.GetCfield ("lief8_choise")->SetTabstop (FALSE); 
             fPos.GetCfield ("lief9_choise")->SetTabstop (FALSE); 
             fPos.GetCfield ("lief10_choise")->SetTabstop (FALSE); 
             fPos.GetCfield ("lief11_choise")->SetTabstop (FALSE); 
             fPos.GetCfield ("lief12_choise")->SetTabstop (FALSE); 
             fPos.GetCfield ("lief13_choise")->SetTabstop (FALSE); 
             fPos.GetCfield ("lief14_choise")->SetTabstop (FALSE); 
             fPos.GetCfield ("lipreise_a_choise")->SetTabstop (FALSE); 

             for (i = 0; i < fHead.GetFieldanz (); i ++)
             {
//                       fHead.GetCfield ()[i]->SetFont (&dlgfont); 
//                       fHead.GetCfield ()[i]->SetPosFont (&dlgposfont); 
             }

             SetTmFont (&dlgfont);
             for (i = 0; Before[i] != NULL; i ++)
             {
                 Before[i]->SetBefore (&fcForm);
             }

             for (i = 0; After[i] != NULL; i ++)
             {
                 After[i]->SetAfter (&fcForm);
             }


             for (i = 0; itFont[i] != NULL; i ++)
             {
                         itFont[i]->SetFont (&fcForm);
             }
             fPos.GetCfield ("pos_frame")->SetBorder (GRAYCOL, WHITECOL, RAISEDHLINE); 
//             fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, RAISED); 
/*
             if (BorderType == HIGHCOLBORDER)
             {

                for (i = 0; itFont[i] != NULL; i ++)
                {
                         itFont[i]->SetFont (&fcForm);
                }

                fPos.GetCfield ("pos_frame")->SetBorder 
                 (new FILLEDBORDER (BLACKCOL, WHITECOL, HIGH, 0, LTGRAYCOL)); 
             }
             else if (BorderType == LOWCOLBORDER)
             {

                for (i = 0; itFont[i] != NULL; i ++)
                {
                         itFont[i]->SetFont (&fcForm);
                }
                fPos.GetCfield ("pos_frame")->SetBorder 
                 (new FILLEDBORDER (BLACKCOL, WHITECOL, LOW, 0, LTGRAYCOL)); 
             }
             else if (BorderType == HIGHBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, HIGH); 
             }
             else if (BorderType == LOWBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, LOW); 
             }
             else if (BorderType == RAISEDBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, RAISED); 
             }
             else if (BorderType == LINEBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, LINE); 
             }
*/

             char datum [12];
             sysdate (datum);
             int kw = get_woche (datum);
             sprintf (defaultlistf.kw, "%d", kw);
			 sprintf(defaultlistf.a_von, "%13.0f",ArtikelVon);
			 sprintf(defaultlistf.a_bis, "%13.0f",ArtikelBis);
             Enable (&fcForm, EnableHead, TRUE); 
             Enable (&fcForm, EnablePos,  FALSE);
             SetDialog (&fcForm0);
}

void SpezDlg::RemoveA (BOOL b)
{
/*
	    int attrATxt = fWork->GetCfield ("a_txt")->GetAttribut ();
	    int attrA = fWork->GetCfield ("a")->GetAttribut ();
	    int attrCh = fWork->GetCfield ("a_choice")->GetAttribut ();
		if (b)
		{
			attrATxt = CREMOVED;
			attrA = CREMOVED;
			attrCh = CREMOVED;
		}
		else if ((attrA & CREMOVED) != 0)
		{
			attrATxt = CDISPLAYONLY;
			attrA = CEDIT;
			attrCh = CBUTTON;
		}
        fWork->GetCfield ("a_txt")->SetAttribut (attrATxt); 
        fWork->GetCfield ("a")->SetAttribut (attrA); 
        fWork->GetCfield ("a_choice")->SetAttribut (attrCh); 
*/
}





BOOL SpezDlg::OnKeyReturn (void)
{
        HWND hWnd;
        
        hWnd = GetFocus ();
        return FALSE;
}


BOOL SpezDlg::OnKeyDown (void)
{
        return FALSE;
}


BOOL SpezDlg::OnKeyUp (void)
{
        return FALSE;
}


BOOL SpezDlg::OnKey2 ()
{
        return TRUE;
}

BOOL SpezDlg::OnKeyHome ()
{
        return TRUE;
}

BOOL SpezDlg::OnKeyEnd ()
{
        return TRUE;

}
BOOL SpezDlg::OnKey1 ()
{
        syskey = KEY1;
        Help ();
        return TRUE;
}

BOOL SpezDlg::OnKey3 ()
{
        syskey = KEY3;
        return TRUE;
}

BOOL SpezDlg::OnKey4 ()
{
        syskey = KEY4;
        CallInfo ();
        return TRUE;
}

BOOL SpezDlg::OnKey5 ()
{

        syskey = KEY5;
        syskey = KEY5;
        if (fHead.GetCfield ("mdn")->IsDisabled () == FALSE)
        {
                  DestroyWindow ();
                  return TRUE;
        }
        Enable (&fcForm, EnableHead, TRUE); 
        Enable (&fcForm, EnablePos,  FALSE);
        work.RollbackWork ();
        work.InitRec ();
        ToForm (dbfields);
         dlong_to_asc (lipreise_k.dat_von, defaultlistf.dat_von);
         dlong_to_asc (lipreise_k.dat_bis, defaultlistf.dat_bis);
        fPos.SetText ();
        fHead.SetCurrentName ("mdn");
        ToolBar_SetState (ActiveDlg->GethwndTB (),   IDM_DELALL,   TBSTATE_INDETERMINATE);
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_LIST,     MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELETE,   MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELALL,   MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      VK_INSERT,    MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      VK_F10,       MF_GRAYED);
        if (ActiveSpezDlg->GetToolbar2 () != NULL)
        {
                 ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f6")->GetFeld ())->bmp = 
					 ImageDelete.Inactive;
                 ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("del")->GetFeld ())->bmp = 
					 ImageDelete.Inactive;
                 ((ColButton *)Toolbar2->GetCfield ("insert")->GetFeld ())->bmp = 
                     ImageInsert.Inactive;
                 if  (ActiveSpezDlg->GetToolbar2 ()->GetCfield ("Texte") != NULL)
                 {
                      ActiveSpezDlg->GetToolbar2 ()->GetCfield ("Texte")->Enable (FALSE);
                 }
                 ActiveSpezDlg->GetToolbar2 ()->display ();
        }
        return TRUE;
}

BOOL SpezDlg::OnKey12 ()
{
        syskey = KEY12;

        fWork->GetText ();
        FromForm (dbheadfields); 
        FromForm (dbfields); 
         lipreise_k.dat_von = dasc_to_long (defaultlistf.dat_von);
         lipreise_k.dat_bis = dasc_to_long (defaultlistf.dat_bis);
        if (fHead.GetCfield ("mdn")->IsDisabled ())
        {
               Enable (&fcForm, EnableHead, TRUE); 
               Enable (&fcForm, EnablePos,  FALSE);
               work.BeginWork ();
	           work.Writelipreise_k ();
               work.CommitWork ();
               work.InitRec ();
               ToForm (dbfields);
			     dlong_to_asc (lipreise_k.dat_von, defaultlistf.dat_von);
				 dlong_to_asc (lipreise_k.dat_bis, defaultlistf.dat_bis);

               fPos.SetText ();
               fHead.SetCurrentName ("mdn");
               ToolBar_SetState (ActiveDlg->GethwndTB (),   IDM_DELALL, TBSTATE_INDETERMINATE);
               EnableMenuItem (ActiveDlg->GethMenu (),      IDM_LIST,     MF_GRAYED);
               EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELETE,   MF_GRAYED);
               EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELALL,   MF_GRAYED);
               EnableMenuItem (ActiveDlg->GethMenu (),      VK_INSERT,    MF_GRAYED);
               EnableMenuItem (ActiveDlg->GethMenu (),      VK_F10,       MF_GRAYED);
               if (ActiveSpezDlg->GetToolbar2 () != NULL)
               {
                 ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f6")->GetFeld ())->bmp = 
					 ImageDelete.Inactive;
                 ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("del")->GetFeld ())->bmp = 
					 ImageDelete.Inactive;
                 ((ColButton *)Toolbar2->GetCfield ("insert")->GetFeld ())->bmp = 
                     ImageInsert.Inactive;
                 if  (ActiveSpezDlg->GetToolbar2 ()->GetCfield ("Texte") != NULL)
                 {
                      ActiveSpezDlg->GetToolbar2 ()->GetCfield ("Texte")->Enable (FALSE);
                 }
                 ActiveSpezDlg->GetToolbar2 ()->display ();
               }
        }
        return TRUE;
}


BOOL SpezDlg::OnKey10 (void)
{
      return FALSE;
}


BOOL SpezDlg::OnKeyDelete (void)
{
        return FALSE;
}

BOOL SpezDlg::OnKeyInsert ()
{
        return FALSE;
}


BOOL SpezDlg::OnKeyPrior ()
{
        return FALSE;
}

BOOL SpezDlg::OnKeyNext ()
{
        return FALSE;
}


BOOL SpezDlg::OnKey6 ()
{
        if (!fHead.GetCfield ("mdn")->IsDisabled ())
        {
			return TRUE;
		}
        FromForm (dbfields); 
        work.BeginWork ();
		
		if (work.PruefeLipreise_Prot() == 1) 
		{
            if (abfragejn (hWnd, "Alte Preise ansehen ?", "N") == 0)
            {
				work.Setflg_preise_prot(0);
			}
			else
			{
				work.Setflg_preise_prot(1);
			}
		}

        fWork->GetText ();
        FromForm (dbheadfields); 
        FromForm (dbfields); 
         lipreise_k.dat_von = dasc_to_long (defaultlistf.dat_von);
         lipreise_k.dat_bis = dasc_to_long (defaultlistf.dat_bis);

        work.Writelipreise_k ();
        work.CommitWork ();
        if (ListDlg != NULL)
        {
            return TRUE;
        }


		work.SetArtikelVon(ratod(defaultlistf.a_von));
		work.SetArtikelBis(ratod(defaultlistf.a_bis));
        if (work.GetMatrix () == ARTIKEL && atoi (defaultlistf.nr) == 0)
        {
            disp_mess ("Es wurde kein Sortiment erfasst",2);
            fHead.SetCurrentName ("hwg");
            return TRUE;
        }


		LISTDLG::lipreise_nr = (short) atoi (defaultlistf.nr);
        if (atoi (defaultlistf.kw) == 0)
        {
            disp_mess ("Es wurde keine Kalenderwoche erfasst",2);
            fHead.SetCurrentName ("kw");
            return TRUE;
        }

        DLG::OnMove (hMainWindow, WM_MOVE, 0, 0l);
        UpdateWindow (hMainWindow);
        fHead.GetText (); 
                 ((ColButton *)Toolbar2->GetCfield ("insert")->GetFeld ())->bmp = 
                     ImageInsert.Image;
        LISTDLG::work = &work; 
        ListDlg = new LISTDLG ((short) atoi (defaultlistf.mdn), 
                               (short) atoi (defaultlistf.nr),
                               (short) atoi (defaultlistf.kw));
        ShowList ();
        ListDlg->SetWork (&work);
// Button, wenn vorhanden Initialisieren
        if (work.GetLsts () != NULL)
        {
//              work.GetLsts ()->choise [0] = NULL;
        }
// Liste ist schon vorhanden, Liste l�schen und dann Editiermodus mit ListEnter
//        ListDlg->DestroyWindows ();

// Liste ist schon vorhanden, Wechsel in Editiermodus 
/*
        if (DockList)
        {
              ListDlg->WorkList ();
        }
        else
        {
              ListDlg->EnterList ();
        }
*/

// Liste nicht vorhanden, Editiermodus mit EnterListe
        Enable (&fcForm, EnableHead, FALSE); 
        Enable (&fcForm, EnablePos, TRUE); 
        work.BeginWork ();
        ListDlg->EnterList ();
        Enable (&fcForm, EnableHead, TRUE); 
        Enable (&fcForm, EnablePos, FALSE); 

// Liste nach EnterList wieder anzeigen, wie in ShowList
//      if (DockList)
//        {
//              ListDlg->ShowList ();
//        }

        delete ListDlg;
        ListDlg = NULL;
        fHead.SetCurrentName ("mdn");
        DLG::OnMove (hMainWindow, WM_MOVE, 0, 0l);
		work.Setflg_prot_details(0);
        return TRUE;
} 


BOOL SpezDlg::OnKey7 ()
{
		work.Setflg_prot_details(1);

		return OnKey6(); //240909
//        return FALSE;
}


BOOL SpezDlg::ShowMdn (void)
{
             char mdn_nr [6];
             SEARCHMDN SearchMdn;
 
             SearchMdn.SetParams (DLG::hInstance, ActiveDlg->GethWnd ());
             SearchMdn.Setawin (ActiveDlg->GethMainWindow ());
             SearchMdn.Search ();
             if (SearchMdn.GetKey (mdn_nr) == TRUE)
             {
                 sprintf (defaultlistf.mdn, "%4d", atoi (mdn_nr));
                 work.GetMdnName (defaultlistf.mdn_krz,   atoi (defaultlistf.mdn));
                 fHead.SetText ();
                 fHead.SetCurrentName ("mdn");
             }
             return TRUE;
}
BOOL SpezDlg::ShowArtikel (void)
{
           //  char a_nr [14];
        struct SA *sa;
             SEARCHA SearchA;
 
             SearchA.SetParams (DLG::hInstance, ActiveDlg->GethWnd ());
             SearchA.Setawin (ActiveDlg->GethMainWindow ());
             SearchA.SearchA ();
        sa = SearchA.GetSa ();
        if (sa == NULL)
        {
            return TRUE;
        }
         sprintf (defaultlistf.a, "%13.0lf", ratod (sa->a));
             fPos.SetText ();
             fPos.SetCurrentName ("lipreise_a");
			 /*
             if (SearchA.GetKey (a_nr) == TRUE)
             {
                 sprintf (defaultlistf.a, "%13.0lf", ratod (a_nr));
//                 work.GetMdnName (defaultlistf.mdn_krz,   atoi (defaultlistf.mdn));
                 fPos.SetText ();
                 fPos.SetCurrentName ("lipreise_a");
             }
			 */
             return TRUE;
}

BOOL SpezDlg::ShowLief (short dlief)
{
             char lief_nr [6];
             SEARCHLIEF SearchLief;
 
             SearchLief.SetParams (DLG::hInstance, ActiveDlg->GethWnd ());
             SearchLief.Setawin (ActiveDlg->GethMainWindow ());
             SearchLief.Search ();
             if (SearchLief.GetKey (lief_nr) == TRUE)
             {

                 if (dlief == 1) sprintf (defaultlistf.lief1, "%s", lief_nr);
                 if (dlief == 2) sprintf (defaultlistf.lief2, "%s", lief_nr);
                 if (dlief == 3) sprintf (defaultlistf.lief3, "%s", lief_nr);
                 if (dlief == 4) sprintf (defaultlistf.lief4, "%s", lief_nr);
                 if (dlief == 5) sprintf (defaultlistf.lief5, "%s", lief_nr);
                 if (dlief == 6) sprintf (defaultlistf.lief6, "%s", lief_nr);
                 if (dlief == 7) sprintf (defaultlistf.lief7, "%s", lief_nr);
                 if (dlief == 8) sprintf (defaultlistf.lief8, "%s", lief_nr);
                 if (dlief == 9) sprintf (defaultlistf.lief9, "%s", lief_nr);
                 if (dlief == 10) sprintf (defaultlistf.lief10, "%s", lief_nr);
                 if (dlief == 11) sprintf (defaultlistf.lief11, "%s", lief_nr);
                 if (dlief == 12) sprintf (defaultlistf.lief12, "%s", lief_nr);
                 if (dlief == 13) sprintf (defaultlistf.lief13, "%s", lief_nr);
                 if (dlief == 14) sprintf (defaultlistf.lief14, "%s", lief_nr);
//                 work.GetMdnName (defaultlistf.mdn_krz,   atoi (defaultlistf.mdn));
                 fPos.SetText ();
                 if (dlief == 1) fHead.SetCurrentName ("lief1");
                 if (dlief == 2) fHead.SetCurrentName ("lief2");
                 if (dlief == 3) fHead.SetCurrentName ("lief3");
                 if (dlief == 4) fHead.SetCurrentName ("lief4");
                 if (dlief == 5) fHead.SetCurrentName ("lief5");
                 if (dlief == 6) fHead.SetCurrentName ("lief6");
                 if (dlief == 7) fHead.SetCurrentName ("lief7");
                 if (dlief == 8) fHead.SetCurrentName ("lief8");
                 if (dlief == 9) fHead.SetCurrentName ("lief9");
                 if (dlief == 10) fHead.SetCurrentName ("lief10");
                 if (dlief == 11) fHead.SetCurrentName ("lief11");
                 if (dlief == 12) fHead.SetCurrentName ("lief12");
                 if (dlief == 13) fHead.SetCurrentName ("lief13");
                 if (dlief == 14) fHead.SetCurrentName ("lief14");
             }
             return TRUE;
}

BOOL SpezDlg::ShowNr (void)
{
             short nr; 
//             short wg;
//             long ag;
             SEARCHNR SearchNr;
 
             SearchNr.SetParams (DLG::hInstance, ActiveDlg->GethWnd ()); 
             SearchNr.Setawin (ActiveDlg->GethMainWindow ());
                                                  
             SearchNr.Search ();
             if (SearchNr.GetKey (&nr) == TRUE)
             {
                 sprintf (defaultlistf.nr, "%d", nr);
                 work.GetNrBez (defaultlistf.nr_bz, nr);
                 fHead.SetText ();
                 fHead.SetCurrentName ("nr");
             }
             return TRUE;
}


BOOL SpezDlg::ShowFil (void)
{
             char fil_nr [6];
             SEARCHFIL SearchFil;
 
             if (atoi (defaultlistf.mdn) == 0) return TRUE;

             SearchFil.SetParams (DLG::hInstance, ActiveDlg->GethWnd (), 
                                                  atoi (defaultlistf.mdn));
             SearchFil.Setawin (ActiveDlg->GethMainWindow ());
             SearchFil.Search ();
             if (SearchFil.GetKey (fil_nr) == TRUE)
             {
                 sprintf (defaultlistf.fil, "%4d", atoi (fil_nr));
                 work.GetFilName (defaultlistf.fil_krz,   atoi (defaultlistf.mdn), atoi (defaultlistf.fil));
                 fHead.SetText ();
                 fHead.SetCurrentName ("fil");
             }
             return TRUE;
}


BOOL SpezDlg::OnKey9 ()
{
         CFIELD *Cfield;
         HWND hWnd;

         syskey = KEY9;
         hWnd = GetFocus ();


         if (hWnd == fHead.GetCfield ("mdn_choise")->GethWnd ())
         {
             return ShowMdn ();
         }

         if (hWnd == fHead.GetCfield ("fil_choise")->GethWnd ())
         {
             return ShowFil ();
         }

         if (hWnd == fHead.GetCfield ("nr_choise")->GethWnd ())
         {
             return ShowNr ();
         }

         if (hWnd == fPos.GetCfield ("lief1_choise")->GethWnd ())
         {
             return ShowLief (1);
         }
         if (hWnd == fPos.GetCfield ("lief2_choise")->GethWnd ())
         {
             return ShowLief (2);
         }
         if (hWnd == fPos.GetCfield ("lief3_choise")->GethWnd ())
         {
             return ShowLief (3);
         }
         if (hWnd == fPos.GetCfield ("lief4_choise")->GethWnd ())
         {
             return ShowLief (4);
         }
         if (hWnd == fPos.GetCfield ("lief5_choise")->GethWnd ())
         {
             return ShowLief (5);
         }
         if (hWnd == fPos.GetCfield ("lief6_choise")->GethWnd ())
         {
             return ShowLief (6);
         }
         if (hWnd == fPos.GetCfield ("lief7_choise")->GethWnd ())
         {
             return ShowLief (7);
         }
         if (hWnd == fPos.GetCfield ("lief8_choise")->GethWnd ())
         {
             return ShowLief (8);
         }
         if (hWnd == fPos.GetCfield ("lief9_choise")->GethWnd ())
         {
             return ShowLief (9);
         }
         if (hWnd == fPos.GetCfield ("lief10_choise")->GethWnd ())
         {
             return ShowLief (10);
         }
         if (hWnd == fPos.GetCfield ("lief11_choise")->GethWnd ())
         {
             return ShowLief (11);
         }
         if (hWnd == fPos.GetCfield ("lief12_choise")->GethWnd ())
         {
             return ShowLief (12);
         }
         if (hWnd == fPos.GetCfield ("lief13_choise")->GethWnd ())
         {
             return ShowLief (13);
         }
         if (hWnd == fPos.GetCfield ("lief14_choise")->GethWnd ())
         {
             return ShowLief (14);
         }
         if (hWnd == fPos.GetCfield ("lipreise_a_choise")->GethWnd ())
         {
             return ShowArtikel ();
         }

         Cfield = ActiveDlg->GetCurrentCfield ();

         if (strcmp (Cfield->GetName (), "mdn") == 0)
         {
             return ShowMdn ();
         }
         if (strcmp (Cfield->GetName (), "lipreise_a") == 0)
         {
             return ShowArtikel ();
         }

         if (strcmp (Cfield->GetName (), "fil") == 0)
         {
             return ShowFil ();
         }
         if (strcmp (Cfield->GetName (), "lief1") == 0)
         {
             return ShowLief (1);
         }
         if (strcmp (Cfield->GetName (), "lief2") == 0)
         {
             return ShowLief (2);
         }
         if (strcmp (Cfield->GetName (), "lief3") == 0)
         {
             return ShowLief (3);
         }
         if (strcmp (Cfield->GetName (), "lief4") == 0)
         {
             return ShowLief (4);
         }
         if (strcmp (Cfield->GetName (), "lief5") == 0)
         {
             return ShowLief (5);
         }
         if (strcmp (Cfield->GetName (), "lief6") == 0)
         {
             return ShowLief (6);
         }
         if (strcmp (Cfield->GetName (), "lief7") == 0)
         {
             return ShowLief (7);
         }
         if (strcmp (Cfield->GetName (), "lief8") == 0)
         {
             return ShowLief (8);
         }
         if (strcmp (Cfield->GetName (), "lief9") == 0)
         {
             return ShowLief (9);
         }
         if (strcmp (Cfield->GetName (), "lief10") == 0)
         {
             return ShowLief (10);
         }
         if (strcmp (Cfield->GetName (), "lief11") == 0)
         {
             return ShowLief (11);
         }
         if (strcmp (Cfield->GetName (), "lief12") == 0)
         {
             return ShowLief (12);
         }
         if (strcmp (Cfield->GetName (), "lief13") == 0)
         {
             return ShowLief (13);
         }
         if (strcmp (Cfield->GetName (), "lief14") == 0)
         {
             return ShowLief (14);
         }
         if (strcmp (Cfield->GetName (), "lipreise_nr") == 0)
         {
             return ShowNr ();
         }
         return FALSE;
}


BOOL SpezDlg::OnPaint (HWND hWnd,HDC hdc, UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (fWork && this->hWnd == hWnd)
        {
             PrintMess (MessText);
             fWork->display (hWnd, hdc);
             return TRUE;
        }
        return DLG::OnPaint (hWnd,hdc, msg, wParam,lParam);
}


BOOL SpezDlg::OnMove (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (hWnd != this->hMainWindow) return FALSE; 
        DLG::OnMove (hWnd,msg,wParam,lParam); 
        if (ListDlg != NULL)
        {
            ListDlg->MoveMamain1 ();
        }
        return FALSE;
}

BOOL SpezDlg::OnActivate (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (LOWORD (wParam) == WA_ACTIVE ||
                LOWORD (wParam) == WA_CLICKACTIVE)
        {
              if (WithTabStops)
              {
                  SetTabFocus (); 
              }
              else
              {
                  fWork->SetFocus ();
              }
              return TRUE;
        }
        return FALSE;
}

BOOL SpezDlg::ChangeDockList (void)
{
        if (DockList)
        {
            DockList = FALSE;
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_DOCKLIST,   MF_UNCHECKED);
        }
        else
        {
            DockList = TRUE;
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_DOCKLIST,   MF_CHECKED);
        }
        return TRUE;
}


BOOL SpezDlg::ChangeScrPrint (void)
{
        if (LISTDLG::ScrPrint)
        {
            LISTDLG::ScrPrint = FALSE;
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_SCRPRINT,   MF_UNCHECKED);
        }
        else
        {
            LISTDLG::ScrPrint = TRUE;
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_SCRPRINT,   MF_CHECKED);
        }
        return TRUE;
}


BOOL SpezDlg::ChangeMatrix (void)
{
        if (work.GetMatrix () == LIEFERANT)
        {
            work.SetMatrix (ARTIKEL);
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_MALIEF,   MF_UNCHECKED);
        }
        else
        {
            work.SetMatrix (LIEFERANT);
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_MALIEF,   MF_CHECKED);
        }
        return TRUE;
}


BOOL SpezDlg::OnCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (ListDlg != NULL)
        {
              if (ListDlg->OnCommand (hWnd,msg,wParam,lParam))
              {
                  return TRUE;
              }
        }
        if (fWork)
        {

              if (LOWORD (wParam) == CANCEL_CTL)
              {
                  return OnKey5 ();
              }
              if (LOWORD (wParam) == OK_CTL)
              {
                  return OnKey12 ();
              }
              if (LOWORD (wParam) == IDM_CHOISE)
              {
                  return OnKey9 ();
              }

              if (LOWORD (wParam) == IDM_LIST)
              {
                  return OnKey6 ();
              }

              if (LOWORD (wParam) == IDM_PRINT)
              {
                  if (ListDlg != NULL)
                  {
                         return ListDlg->OnCommand (hWnd,msg,wParam,lParam);
                  }
              }

              if (LOWORD (wParam) == IDM_SCRPRINT)
              {
                  return ChangeScrPrint ();
              }

              if (LOWORD (wParam) == IDM_DOCKLIST)
              {
                  return ChangeDockList ();
              }

              if (LOWORD (wParam) == IDM_MALIEF)
              {
                  return ChangeMatrix ();
              }

              if (HIWORD (wParam) == WM_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              if (HIWORD (wParam) == EN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == CBN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == WM_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == LBN_SELCHANGE)
              {
//                  return ListChanged (wParam, lParam);
              }
              else if (HIWORD (wParam) == EN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == CBN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == BN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == BN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
        }
        return DLG::OnCommand (hWnd, msg, wParam, lParam);
}


BOOL SpezDlg::OnSysCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        
        if (wParam == SC_CLOSE)
        {
            if (abfragejn (hWnd, "Verarbeitung abbrechen ?", "N") == 0)
            {
                ActiveDlg->SetCurrentFocus ();
                return TRUE;
            }
            ExitProcess (0);;
        }
        return DLG::OnSysCommand (hWnd, msg, wParam, lParam); 
}

BOOL SpezDlg::OnDestroy (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{

        if (hWnd == this->hWnd)
        {
             PostQuitMessage (0);
             fcForm.destroy ();
        }
        return TRUE;
}

void SpezDlg::SetWinBackground (COLORREF Col)
{
          dlgfont.FontBkColor = Col;
          cpfont.FontBkColor = Col;
          DLG::SetWinBackground (Col);
}


void SpezDlg::ToClipboard (void)
{
          CFIELD *Cfield;
          HGLOBAL hb;
          LPVOID p;
          char *text;

          Cfield = GetCurrentCfield ();
          Cfield->GetText ();
          text = (char *) Cfield->GetFeld ();
          if (text == NULL) return;
          hb = GlobalAlloc (GHND, strlen (text) + 1);
          if (hb == NULL) return;
          p = GlobalLock (hb);
          strcpy ((char *) p, text);
          GlobalUnlock (hb);
          OpenClipboard (hWnd);
          EmptyClipboard ();
          SetClipboardData (CF_TEXT, hb);
          CloseClipboard ();
}

void SpezDlg::FromClipboard (void)
{
          CFIELD *Cfield;
          HGLOBAL hb;
          LPVOID p;
          char *text;

          Cfield = GetCurrentCfield ();
          text = (char *) Cfield->GetFeld ();
          if (text == NULL) return;
          if (IsClipboardFormatAvailable (CF_TEXT) == FALSE) return;
          OpenClipboard (hWnd);
          hb = GetClipboardData (CF_TEXT);
          if (hb == NULL)
          {
                 CloseClipboard ();
                 return;
          }
          p = GlobalLock (hb);
          if (strlen ((char *) p) > (unsigned int ) Cfield->GetLength ())
          {
                 GlobalUnlock (hb);
                 CloseClipboard ();
                 return;
          }
          strcpy (text, (char *) p);
          GlobalUnlock (hb);
          CloseClipboard ();
          Cfield->SetText ();
}

void SpezDlg::Help (void)
{
          CFIELD *Cfield;
          HELP *Help; 
          char *Item;

          Cfield = GetCurrentCfield ();
          if (Cfield == NULL) return;

          Item = Cfield->GetName ();

          Help = new HELP (HelpName, Item, hInstance,hMainWindow);
          delete Help;
          Cfield->SetFocus ();
}

HWND SpezDlg::OpenWindow (HINSTANCE hInstance, HWND hMainWindow)
{
          HWND hWnd;

          Settchar ('|');
          hWnd = DLG::OpenWindow (hInstance, hMainWindow);
//          FillListCaption ();
          EnterPos ();
          return hWnd;
}

void SpezDlg::DestroyPos (void)
{
    fPos.destroy ();
    fcForm.SetFieldanz (fcForm.GetFieldanz () - 1);
}

void SpezDlg::SetPos (void)
{
    fcForm.SetFieldanz (2);
    fcForm0.Invalidate (TRUE);
    fcForm0.MoveWindow ();
    UpdateWindow (hWnd);
}

HWND SpezDlg::OpenScrollWindow (HINSTANCE hInstance, HWND hMainWindow)
{
          HWND hWnd;

          EnableMenuItem (ActiveSpezDlg->GethMenu (),    IDM_DEL,   MF_GRAYED);

          Settchar ('|');
          hWnd = DLG::OpenScrollWindow (hInstance, hMainWindow);
//LUD          EnterPos ();
//          ListDlg = new LISTDLG ();
          return hWnd;
}

void SpezDlg::ShowList (void)
{
          int DockY;
          RECT rect;

		  ListDlg->SetDlg (this);
          ListDlg->SetWork (&work);
          if (DockList == FALSE)
          {
                 ListDlg->SetWinStyle (ListDlg->GetWinStyle () | WS_CAPTION | 
                                                                 WS_SYSMENU |
                                                                 WS_MINIMIZEBOX |
                                                                 WS_MAXIMIZEBOX);
                ListDlg->SetDockWin (FALSE);
                ListDlg->SetDimension (750, 400);
                ListDlg->SetModal (TRUE);
          }
          else
          {
                if (DockMode)
                {
                   ListDlg->SetWinStyle (WS_THICKFRAME | WS_CHILD | WS_CLIPSIBLINGS);
                   Enable (&fcForm, EnablePos, TRUE); 
//                DestroyPos ();
                }
                GetWindowRect (hWnd, &rect);
                DockY = fPos.GetCfield ("pos_frame")->GetYP () + rect.top;
//                DockY = 309;
                ListDlg->SetDockY (DockY);
          }

          ListDlg->SethMenu (hMenu);
          ListDlg->SethwndTB (hwndTB);
          ListDlg->SethMainWindow (hWnd);
          ListDlg->SetTextMetric (&DlgTm);
          ListDlg->SetLineRow (100);
// Liste gleich anzeigen
//          if (DockList)
//          {
//               ListDlg->ShowList ();
//          }
}

void SpezDlg::PrintComment (char *Name)
{
          HELP *Help; 
          char Comment [256];

          Help = new HELP (HelpName, Name, hInstance,hMainWindow, INITCOMM);
          Help->GetItem (Name, Comment);
          PrintMess (Comment);
          delete Help;
}

void SpezDlg::CallInfo (void)
{
          DLG::CallInfo ();
}



void SpezDlg::SetListLines (int ListLines)
{
          this->ListLines = ListLines;
          if (ListClass::ActiveList != NULL)
          {
		      ListClass::ActiveList->SetListLines (ListLines);
          }
          LineSet         = TRUE;
}

void SpezDlg::SetListColors (COLORREF ListColor, COLORREF ListBkColor)
{
          this->ListColor   = ListColor;
          this->ListBkColor = ListBkColor;
          if (ListClass::ActiveList != NULL)
          {
		      ListClass::ActiveList->SetColors (ListColor,ListBkColor);
          }
          ColorSet          = TRUE;

}


HWND SpezDlg::SetToolCombo (int Id, int tbn1, int tbn2, int ComboPtr)
{
         HWND hwndCombo; 

		 hwndCombo = DLG::SetToolCombo (Id, tbn1, tbn2);

		 switch (ComboPtr)
		 {
		     case 0:
                   hwndCombo1 = hwndCombo;
				   break;
			 case 1:
                   hwndCombo2 = hwndCombo;
				   break;
		 }
         return hwndCombo;
}

void SpezDlg::SetComboTxt (HWND hwndCombo, char **Combo, int Select, int ComboPtr)
{

	  DLG::SetComboTxt (hwndCombo, Combo, Select);

 	  switch (ComboPtr)
	  {
		     case 0:
                   Combo1 = Combo;
				   break;
			 case 1:
                   Combo2 = Combo;
				   break;
	  }
}


BOOL SpezDlg::OnCloseUp (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
	    if ((HWND) lParam == hwndCombo1)
		{
			ChoiseCombo1 ();
			return TRUE;
		}
		else if ((HWND) lParam == hwndCombo2)
		{
			ChoiseCombo2 ();
			return TRUE;
		}
		return FALSE;
}


void SpezDlg::ChoiseCombo1 (void)
/**
Aktuell gewaehlten eintrag in Combobox 1 ermitteln.
**/
{
          int i;
          char text [80];

          GetWindowText (hwndCombo1,
                         text,
                         79);
          clipped (text);
          for (i = 0; Combo1[i]; i ++)
          {
              if (strcmp (Combo1[i],text) == 0)
              {
                   SetListLines (i);
                   break;
              }
          }
          if (ActiveDlg != NULL && 
              ActiveDlg->GetCurrentCfield ()->IsDisabled == FALSE)
          {
              ActiveDlg->SetCurrentFocus ();
          }
}

void SpezDlg::ChoiseCombo2 (void)
/**
Aktuell gewaehlten eintrag in Combobox 1 ermitteln.
**/
{
          int i;
          char text [80];

          GetWindowText (hwndCombo2,
                         text,
                         79);
          clipped (text);
          for (i = 0; Combo2[i]; i ++)
          {
              if (strcmp (Combo2[i],text) == 0)
              {
                   SetListColors (Colors[i], BkColors[i]);
                   break;
              }
          }
          if (ActiveDlg != NULL && 
              ActiveDlg->GetCurrentCfield ()->IsDisabled == FALSE)
          {
              ActiveDlg->SetCurrentFocus ();
          }
}


void SpezDlg::ProcessMessages (void)
{
//          ShowList ();
          DLG::ProcessMessages ();
}

int SpezDlg::Deletelipreise (void)
{
/*
         HWND lBox;
          
        lBox = _fList[0]->GethWnd ();
        if (lBox == NULL) return 0;
*/
        
        if (abfragejn (ActiveDlg->GethWnd (), "Listennummer l�schen ?", "J") == 0)
        {
            return 0;
        }

        work.Deletelipreise ();

        Enable (&fcForm, EnableHead, TRUE); 
        Enable (&fcForm, EnablePos,  FALSE);


        ToolBar_SetState (ActiveDlg->GethwndTB (),   IDM_DELALL,   TBSTATE_INDETERMINATE);
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELETE,   MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELALL,   MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      VK_INSERT,    MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      VK_F10,       MF_GRAYED);
        if (ActiveSpezDlg->GetToolbar2 () != NULL)
        {
                 ((ColButton *)Toolbar2->GetCfield ("del")->GetFeld ())->bmp = 
					 ImageDelete.Inactive;
                 ((ColButton *)Toolbar2->GetCfield ("insert")->GetFeld ())->bmp = 
                     ImageInsert.Inactive;
                 ActiveSpezDlg->GetToolbar2 ()->display ();
        }
        work.CommitWork ();
        work.InitRec ();
        FromForm (dbheadfields);
        ToForm (dbfields);
         dlong_to_asc (lipreise_k.dat_von, defaultlistf.dat_von);
         dlong_to_asc (lipreise_k.dat_bis, defaultlistf.dat_bis);
//        SetCombos ();
        fPos.SetText ();
/*
        InvalidateRect (lBox, NULL, TRUE);
        UpdateWindow (lBox);
*/
        fHead.SetCurrentName ("lipreise_nr");
/*
        SendMessage (lBox, LB_RESETCONTENT, 0, 0l);
        ListRows = 0;
*/
        return 0;
}

