#include "lief_bzgn.h"

struct LIEF_BZGN lief_bzgn, lief_bzgn_null;

void LIEF_BZGN_CLASS::prepare (void)
{
            char *sqltext; 

            ins_quest ((char *) &lief_bzgn.mdn, 1, 0);
            ins_quest ((char *) &lief_bzgn.fil, 1, 0);
            ins_quest ((char *) &lief_bzgn.lief, 0, 17);
            ins_quest ((char *) &lief_bzgn.dat, 2, 0);
            ins_quest ((char *) lief_bzgn.lief_best, 0, 17);
    out_quest ((char *) &lief_bzgn.a,3,0);
    out_quest ((char *) &lief_bzgn.fil,1,0);
    out_quest ((char *) lief_bzgn.lief,0,17);
    out_quest ((char *) lief_bzgn.lief_best,0,17);
    out_quest ((char *) &lief_bzgn.lief_s,2,0);
    out_quest ((char *) &lief_bzgn.mdn,1,0);
    out_quest ((char *) &lief_bzgn.pr_ek,3,0);
    out_quest ((char *) &lief_bzgn.dat,2,0);
    out_quest ((char *) lief_bzgn.zeit,0,7);
    out_quest ((char *) &lief_bzgn.pr_ek_eur,3,0);
            cursor = prepare_sql ("select lief_bzgn.a,  "
"lief_bzgn.fil,  lief_bzgn.lief,  lief_bzgn.lief_best,  "
"lief_bzgn.lief_s,  lief_bzgn.mdn,  lief_bzgn.pr_ek,  lief_bzgn.dat,  "
"lief_bzgn.zeit,  lief_bzgn.pr_ek_eur from lief_bzgn "

#line 15 "lief_bzgn.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   lief = ? "
                                  "and   dat = ? "
                                  "and   lief_best = ?" );

            ins_quest ((char *) &lief_bzgn.mdn, 1, 0);
            ins_quest ((char *) &lief_bzgn.fil, 1, 0);
            ins_quest ((char *) &lief_bzgn.lief, 0, 17);
            ins_quest ((char *) &lief_bzgn.dat, 2, 0);
            ins_quest ((char *) lief_bzgn.lief_best, 0, 17);
    out_quest ((char *) &lief_bzgn.a,3,0);
    out_quest ((char *) &lief_bzgn.fil,1,0);
    out_quest ((char *) lief_bzgn.lief,0,17);
    out_quest ((char *) lief_bzgn.lief_best,0,17);
    out_quest ((char *) &lief_bzgn.lief_s,2,0);
    out_quest ((char *) &lief_bzgn.mdn,1,0);
    out_quest ((char *) &lief_bzgn.pr_ek,3,0);
    out_quest ((char *) &lief_bzgn.dat,2,0);
    out_quest ((char *) lief_bzgn.zeit,0,7);
    out_quest ((char *) &lief_bzgn.pr_ek_eur,3,0);
            cursor_dat = prepare_sql ("select lief_bzgn.a,  "
"lief_bzgn.fil,  lief_bzgn.lief,  lief_bzgn.lief_best,  "
"lief_bzgn.lief_s,  lief_bzgn.mdn,  lief_bzgn.pr_ek,  lief_bzgn.dat,  "
"lief_bzgn.zeit,  lief_bzgn.pr_ek_eur from lief_bzgn "

#line 27 "lief_bzgn.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   lief = ? "
                                  "and   dat <= ? "
                                  "and   lief_best = ? order by dat" );

            ins_quest ((char *) &lief_bzgn.mdn, 1, 0);
            ins_quest ((char *) &lief_bzgn.fil, 1, 0);
            ins_quest ((char *) &lief_bzgn.lief, 0, 17);
            ins_quest ((char *) lief_bzgn.lief_best, 0, 17);
    out_quest ((char *) &lief_bzgn.a,3,0);
    out_quest ((char *) &lief_bzgn.fil,1,0);
    out_quest ((char *) lief_bzgn.lief,0,17);
    out_quest ((char *) lief_bzgn.lief_best,0,17);
    out_quest ((char *) &lief_bzgn.lief_s,2,0);
    out_quest ((char *) &lief_bzgn.mdn,1,0);
    out_quest ((char *) &lief_bzgn.pr_ek,3,0);
    out_quest ((char *) &lief_bzgn.dat,2,0);
    out_quest ((char *) lief_bzgn.zeit,0,7);
    out_quest ((char *) &lief_bzgn.pr_ek_eur,3,0);
            cursor_best = prepare_sql ("select lief_bzgn.a,  "
"lief_bzgn.fil,  lief_bzgn.lief,  lief_bzgn.lief_best,  "
"lief_bzgn.lief_s,  lief_bzgn.mdn,  lief_bzgn.pr_ek,  lief_bzgn.dat,  "
"lief_bzgn.zeit,  lief_bzgn.pr_ek_eur from lief_bzgn "

#line 38 "lief_bzgn.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   lief = ? "
                                  "and   lief_best = ?" );

            ins_quest ((char *) &lief_bzgn.mdn, 1, 0);
            ins_quest ((char *) &lief_bzgn.fil, 1, 0);
            ins_quest ((char *) &lief_bzgn.lief, 0, 17);
            ins_quest ((char *) &lief_bzgn.dat, 2, 0);
            ins_quest ((char *) &lief_bzgn.a, 0, 17);
    out_quest ((char *) &lief_bzgn.a,3,0);
    out_quest ((char *) &lief_bzgn.fil,1,0);
    out_quest ((char *) lief_bzgn.lief,0,17);
    out_quest ((char *) lief_bzgn.lief_best,0,17);
    out_quest ((char *) &lief_bzgn.lief_s,2,0);
    out_quest ((char *) &lief_bzgn.mdn,1,0);
    out_quest ((char *) &lief_bzgn.pr_ek,3,0);
    out_quest ((char *) &lief_bzgn.dat,2,0);
    out_quest ((char *) lief_bzgn.zeit,0,7);
    out_quest ((char *) &lief_bzgn.pr_ek_eur,3,0);
            cursor_a = prepare_sql ("select lief_bzgn.a,  "
"lief_bzgn.fil,  lief_bzgn.lief,  lief_bzgn.lief_best,  "
"lief_bzgn.lief_s,  lief_bzgn.mdn,  lief_bzgn.pr_ek,  lief_bzgn.dat,  "
"lief_bzgn.zeit,  lief_bzgn.pr_ek_eur from lief_bzgn "

#line 49 "lief_bzgn.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   lief = ? "
                                  "and   dat = ? "
                                  "and   a = ?" );


    ins_quest ((char *) &lief_bzgn.a,3,0);
    ins_quest ((char *) &lief_bzgn.fil,1,0);
    ins_quest ((char *) lief_bzgn.lief,0,17);
    ins_quest ((char *) lief_bzgn.lief_best,0,17);
    ins_quest ((char *) &lief_bzgn.lief_s,2,0);
    ins_quest ((char *) &lief_bzgn.mdn,1,0);
    ins_quest ((char *) &lief_bzgn.pr_ek,3,0);
    ins_quest ((char *) &lief_bzgn.dat,2,0);
    ins_quest ((char *) lief_bzgn.zeit,0,7);
    ins_quest ((char *) &lief_bzgn.pr_ek_eur,3,0);
            sqltext = "update lief_bzgn set lief_bzgn.a = ?,  "
"lief_bzgn.fil = ?,  lief_bzgn.lief = ?,  lief_bzgn.lief_best = ?,  "
"lief_bzgn.lief_s = ?,  lief_bzgn.mdn = ?,  lief_bzgn.pr_ek = ?,  "
"lief_bzgn.dat = ?,  lief_bzgn.zeit = ?,  lief_bzgn.pr_ek_eur = ? "

#line 57 "lief_bzgn.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   lief = ? "
                                  "and   dat = ? "
                                  "and   lief_best = ?";

            ins_quest ((char *) &lief_bzgn.mdn, 1, 0);
            ins_quest ((char *) &lief_bzgn.fil, 1, 0);
            ins_quest ((char *) &lief_bzgn.lief, 0, 17);
            ins_quest ((char *) &lief_bzgn.dat, 2, 0);
            ins_quest ((char *) &lief_bzgn.lief_best, 0, 17);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &lief_bzgn.mdn, 1, 0);
            ins_quest ((char *) &lief_bzgn.fil, 1, 0);
            ins_quest ((char *) &lief_bzgn.lief, 0, 17);
            ins_quest ((char *) &lief_bzgn.dat, 2, 0);
            ins_quest ((char *) &lief_bzgn.lief_best, 0, 17);
            test_upd_cursor = prepare_sql ("select a from lief_bzgn "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   lief = ? "
                                  "and   dat = ? "
                                  "and   lief_best = ?");
            ins_quest ((char *) &lief_bzgn.mdn, 1, 0);
            ins_quest ((char *) &lief_bzgn.fil, 1, 0);
            ins_quest ((char *) &lief_bzgn.lief, 0, 17);
            ins_quest ((char *) &lief_bzgn.dat, 2, 0);
            ins_quest ((char *) &lief_bzgn.lief_best, 0, 17);
            del_cursor = prepare_sql ("delete from lief_bzgn "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   lief = ? "
                                  "and   dat = ? "
                                  "and   lief_best = ?");
    ins_quest ((char *) &lief_bzgn.a,3,0);
    ins_quest ((char *) &lief_bzgn.fil,1,0);
    ins_quest ((char *) lief_bzgn.lief,0,17);
    ins_quest ((char *) lief_bzgn.lief_best,0,17);
    ins_quest ((char *) &lief_bzgn.lief_s,2,0);
    ins_quest ((char *) &lief_bzgn.mdn,1,0);
    ins_quest ((char *) &lief_bzgn.pr_ek,3,0);
    ins_quest ((char *) &lief_bzgn.dat,2,0);
    ins_quest ((char *) lief_bzgn.zeit,0,7);
    ins_quest ((char *) &lief_bzgn.pr_ek_eur,3,0);
            ins_cursor = prepare_sql ("insert into lief_bzgn ("
"a,  fil,  lief,  lief_best,  lief_s,  mdn,  pr_ek,  dat,  zeit,  pr_ek_eur) "

#line 93 "lief_bzgn.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?)");

#line 95 "lief_bzgn.rpp"
}

int LIEF_BZGN_CLASS::dbreadfirst_best ()
{
	if (cursor_best == -1)
        {
               prepare ();
        }
        if (cursor_best == -1) return 100;

        int dsqlstatus = sqlopen (cursor_best);
        if (dsqlstatus == 0)
        {
		return sqlfetch (cursor_best); 
        }
        return dsqlstatus;         
}

 
int LIEF_BZGN_CLASS::dbread_best ()
{
        if (cursor_best == -1) return 100;

	return sqlfetch (cursor_best); 
} 

int LIEF_BZGN_CLASS::dbreadfirst_a ()
{
	if (cursor_a == -1)
        {
               prepare ();
        }
        if (cursor_a == -1) return 100;

        int dsqlstatus = sqlopen (cursor_a);
        if (dsqlstatus == 0)
        {
		return sqlfetch (cursor_a); 
        }
        return dsqlstatus;         
}

 
int LIEF_BZGN_CLASS::dbread_a ()
{
        if (cursor_a == -1) return 100;

	return sqlfetch (cursor_a); 
} 

int LIEF_BZGN_CLASS::dbreadfirst_dat ()
{
	if (cursor_a == -1)
        {
               prepare ();
        }
        if (cursor_a == -1) return 100;

        int dsqlstatus = sqlopen (cursor_dat);
        if (dsqlstatus == 0)
        {
		return sqlfetch (cursor_dat); 
        }
        return dsqlstatus;         
}

 
int LIEF_BZGN_CLASS::dbread_dat ()
{
        if (cursor_dat == -1) return 100;

	return sqlfetch (cursor_dat); 
} 
