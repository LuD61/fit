#ifndef _BEST_POS_DEF
#define _BEST_POS_DEF

#include "dbclass.h"

struct BEST_POS {
   double    a;
   double    anz_einh;
   short     bearb_stat;
   long      best_blg;
   long      best_term;
   short     fil;
   double    inh;
   char      lief[17];
   char      lief_best[17];
   long      lief_s;
   long      lief_term;
   short     mdn;
   double    me;
   double    me_ist;
   short     p_num;
   double    pr_ek;
   char      sa_kz[2];
   char      sti_pro_kz[2];
   short     me_einh;
   char      pr_abw[2];
   double    fakt;
   double    pr_ek_bto;
   long      best_txt;
   double    pr_ek_euro;
   double    pr_ek_euro_bto;
   double    pr_ek_fremd;
   double    pr_ek_fremd_bto;
   double    kontrakt_me;
   double    kontrakt_me_soll;
   double    paz;
   long      inh_ek;
   double    lakt_rab;
   double    aakt_rab;
   double    aakt_pr;
};
extern struct BEST_POS best_pos, best_pos_null;

#line 7 "best_pos.rh"

class BEST_POS_CLASS : public DB_CLASS 
{
       private :
               int cursor_o; 
               int del_best_curs;
               void prepare (void);
               void prepare_o (char *);
       public :
               BEST_POS_CLASS () : DB_CLASS (), cursor_o (-1), del_best_curs (-1)
               {
               }
               int dbreadfirst (void);
               int dbreadfirst_o (char *);
               int dbread_o (void);
               int dbdeletebest (void);
               int dbclose (void);
               int dbclose_o (void);
              
};
#endif
