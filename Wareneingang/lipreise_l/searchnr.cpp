#include <windows.h>
#include "searchnr.h"
#include "strfkt.h"
#include "mo_wmess.h"
#include "lbox.h"


int SEARCHNR::idx;
long SEARCHNR::anz;
CHQEX *SEARCHNR::Query = NULL;
DB_CLASS SEARCHNR::DbClass; 
HINSTANCE SEARCHNR::hMainInst;
HWND SEARCHNR::hMainWindow;
HWND SEARCHNR::awin;
short SEARCHNR::mdn_nr = 0;


int SEARCHNR::SearchLst (char *sebuff)
/**
Nach Kurzname in Liste suchen.
**/
{
	   Query->SearchList ();
	   return 0;
}

int SEARCHNR::ReadLst (char *name)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int cursor;
	  int i;
      WMESS Wmess;
      short hwg;
      char hwg_bz1 [33];


	  anz = 0;
      clipped (name);
      if (strcmp (name, " ") == 0) name[0] = 0;

      if (strlen (name) == 0 ||
          name[0] <= ' ')
      {
	          sprintf (buffer, "select nr, nr_bz "
	 			       "from lipreise_k ");
      }
      else
      {
	          sprintf (buffer, "select nr, nr_bz "
	 			       "from lipreise_k "
                       "where nr_bz matches \"%s\" order by nr ",
                        name);
      }
      DbClass.sqlout ((short *) &hwg, 1, 0);
      DbClass.sqlout ((char *) hwg_bz1, 0, 25);
      cursor = DbClass.sqlcursor (buffer);
      if (cursor < 0) 
      {
          return -1;
      }
  	  i = 0;
      Wmess.Message (hMainInst, hMainWindow, " \nBitte warten.....\n"
  	                                         "Die Daten werden selektiert.\n ");  
	  while (DbClass.sqlfetch (cursor) == 0)
      {
 	      sprintf (buffer, "    %4d  |%-24s|", hwg, hwg_bz1);
	      Query->InsertRecord (buffer);
		  i ++;
      }
      anz = i;
	  DbClass.sqlclose (cursor);
 	  Wmess.Destroy ();
	  EnableWindow (awin, FALSE);
	  return 0;
}

void SEARCHNR::SetParams (HINSTANCE hMainInst, HWND hMainWindow)
{
      this->hMainInst   = hMainInst;
      this->hMainWindow = hMainWindow;
      awin = hMainWindow;
}

BOOL SEARCHNR::GetKey (short *key1)
{
      int anz;
     
      if (idx == -1) return FALSE;
      anz = wsplit (Key, "|");
      if (anz < 2) return FALSE;
      *key1 = atoi (wort[0]);
      return TRUE;
}


void SEARCHNR::Search (void)
{
  	  int cx, cy;
	  char buffer [256];

	  idx = -1;
      cx = 48;
      cy = 25;
      Settchar ('|');
      EnableWindow (hMainWindow, FALSE);
      Query = new CHQEX (cx, cy);
      Query->EnableSort (TRUE);
      Query->OpenWindow (hMainInst, hMainWindow);
      sprintf (buffer, "%s %s",
                          "%d" 
                          "%s");
      Query->RowAttr (buffer);
	  sprintf (buffer, "    %6s", "1"); 
	  Query->VLines (buffer, 3);
      sprintf (buffer, "4;4 11;24");
      Query->RowPos (buffer);
	  EnableWindow (awin, FALSE);

	  sprintf (buffer, "    %4s  %-26s", "NR", "Bezeichnung"); 
	  Query->InsertCaption (buffer);

	  Query->SetFillDb (ReadLst);
	  Query->SetSearchLst (SearchLst);
      ReadLst ("");
      Query->SetSortRow (0, TRUE);
	  Query->ProcessMessages ();
      EnableWindow (hMainWindow, TRUE);
      if (syskey != KEY5)
      {
              idx = Query->GetSel ();
              if (idx != -1)
              {
                     Query->GetText (Key);
              }
      }
	  EnableWindow (awin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (awin);
}

