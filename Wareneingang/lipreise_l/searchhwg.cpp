#include <windows.h>
#include "searchhwg.h"
#include "strfkt.h"
#include "mo_wmess.h"
#include "lbox.h"


int SEARCHHWG::idx;
long SEARCHHWG::anz;
CHQEX *SEARCHHWG::Query = NULL;
DB_CLASS SEARCHHWG::DbClass; 
HINSTANCE SEARCHHWG::hMainInst;
HWND SEARCHHWG::hMainWindow;
HWND SEARCHHWG::awin;
short SEARCHHWG::mdn_nr = 0;


int SEARCHHWG::SearchLst (char *sebuff)
/**
Nach Kurzname in Liste suchen.
**/
{
	   Query->SearchList ();
	   return 0;
}

int SEARCHHWG::ReadLst (char *name)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int cursor;
	  int i;
      WMESS Wmess;
      short hwg;
      char hwg_bz1 [33];


	  anz = 0;
      clipped (name);
      if (strcmp (name, " ") == 0) name[0] = 0;

      if (strlen (name) == 0 ||
          name[0] <= ' ')
      {
	          sprintf (buffer, "select ptlfnr, ptbez "
	 			       "from ptabn "
                       "where ptitem = \"a_typ\" order by ptlfnr ");
      }
      else
      {
	          sprintf (buffer, "select ptlfnr, ptbez "
	 			       "from ptabn "
                       "where ptitem = \"a_typ\" "
                       "and ptbez matches \"%s\" order by ptlfnr ",
                        name);
      }
      DbClass.sqlout ((short *) &hwg, 1, 0);
      DbClass.sqlout ((char *) hwg_bz1, 0, 25);
      cursor = DbClass.sqlcursor (buffer);
      if (cursor < 0) 
      {
          return -1;
      }
  	  i = 0;
      Wmess.Message (hMainInst, hMainWindow, " \nBitte warten.....\n"
  	                                         "Die Daten werden selektiert.\n ");  
	  while (DbClass.sqlfetch (cursor) == 0)
      {
 	      sprintf (buffer, "    %4d  |%-24s|", hwg, hwg_bz1);
	      Query->InsertRecord (buffer);
		  i ++;
      }
      anz = i;
	  DbClass.sqlclose (cursor);
 	  Wmess.Destroy ();
	  EnableWindow (awin, FALSE);
	  return 0;
}

void SEARCHHWG::SetParams (HINSTANCE hMainInst, HWND hMainWindow)
{
      this->hMainInst   = hMainInst;
      this->hMainWindow = hMainWindow;
      awin = hMainWindow;
}

BOOL SEARCHHWG::GetKey (short *key1)
{
      int anz;
     
      if (idx == -1) return FALSE;
      anz = wsplit (Key, "|");
      if (anz < 2) return FALSE;
      *key1 = atoi (wort[0]);
      return TRUE;
}


void SEARCHHWG::Search (void)
{
  	  int cx, cy;
	  char buffer [256];

	  idx = -1;
      cx = 48;
      cy = 25;
      Settchar ('|');
      EnableWindow (hMainWindow, FALSE);
      Query = new CHQEX (cx, cy);
      Query->EnableSort (TRUE);
      Query->OpenWindow (hMainInst, hMainWindow);
      sprintf (buffer, "%s %s",
                          "%d" 
                          "%s");
      Query->RowAttr (buffer);
	  sprintf (buffer, "    %6s", "1"); 
	  Query->VLines (buffer, 3);
      sprintf (buffer, "4;4 11;24");
      Query->RowPos (buffer);
	  EnableWindow (awin, FALSE);

	  sprintf (buffer, "    %4s  %-26s", "HWG", "Bezeichnung"); 
	  Query->InsertCaption (buffer);

	  Query->SetFillDb (ReadLst);
	  Query->SetSearchLst (SearchLst);
      ReadLst ("");
      Query->SetSortRow (0, TRUE);
	  Query->ProcessMessages ();
      EnableWindow (hMainWindow, TRUE);
      if (syskey != KEY5)
      {
              idx = Query->GetSel ();
              if (idx != -1)
              {
                     Query->GetText (Key);
              }
      }
	  EnableWindow (awin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (awin);
}

