#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "wmask.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "lipreise_p.h"

struct LIPREISE_P lipreise_p, lipreise_p_null;

void LIPREISE_P_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &lipreise_p.nr, 1, 0);
            ins_quest ((char *) &lipreise_p.a, 3, 0);
    out_quest ((char *) &lipreise_p.mdn,1,0);
    out_quest ((char *) &lipreise_p.nr,1,0);
    out_quest ((char *) &lipreise_p.a,3,0);
    out_quest ((char *) &lipreise_p.pr_ek1,3,0);
    out_quest ((char *) &lipreise_p.pr_ek2,3,0);
    out_quest ((char *) &lipreise_p.pr_ek3,3,0);
    out_quest ((char *) &lipreise_p.pr_ek4,3,0);
    out_quest ((char *) &lipreise_p.pr_ek5,3,0);
    out_quest ((char *) &lipreise_p.pr_ek6,3,0);
    out_quest ((char *) &lipreise_p.pr_ek7,3,0);
    out_quest ((char *) &lipreise_p.pr_ek8,3,0);
    out_quest ((char *) &lipreise_p.pr_ek9,3,0);
    out_quest ((char *) &lipreise_p.pr_ek10,3,0);
    out_quest ((char *) &lipreise_p.pr_ek11,3,0);
    out_quest ((char *) &lipreise_p.pr_ek12,3,0);
    out_quest ((char *) &lipreise_p.pr_ek13,3,0);
    out_quest ((char *) &lipreise_p.pr_ek14,3,0);
    out_quest ((char *) &lipreise_p.posi,1,0);
            cursor = prepare_sql ("select lipreise_p.mdn,  "
"lipreise_p.nr,  lipreise_p.a,  lipreise_p.pr_ek1,  lipreise_p.pr_ek2,  "
"lipreise_p.pr_ek3,  lipreise_p.pr_ek4,  lipreise_p.pr_ek5,  "
"lipreise_p.pr_ek6,  lipreise_p.pr_ek7,  lipreise_p.pr_ek8,  "
"lipreise_p.pr_ek9,  lipreise_p.pr_ek10,  lipreise_p.pr_ek11,  "
"lipreise_p.pr_ek12,  lipreise_p.pr_ek13,  lipreise_p.pr_ek14,  "
"lipreise_p.posi from lipreise_p "

#line 26 "lipreise_p.rpp"
                                  "where nr = ? and a = ? ");
            ins_quest ((char *) &lipreise_p.nr, 1, 0);
            ins_quest ((char *) &lipreise_p.a, 3, 0);
    out_quest ((char *) &lipreise_p.mdn,1,0);
    out_quest ((char *) &lipreise_p.nr,1,0);
    out_quest ((char *) &lipreise_p.a,3,0);
    out_quest ((char *) &lipreise_p.pr_ek1,3,0);
    out_quest ((char *) &lipreise_p.pr_ek2,3,0);
    out_quest ((char *) &lipreise_p.pr_ek3,3,0);
    out_quest ((char *) &lipreise_p.pr_ek4,3,0);
    out_quest ((char *) &lipreise_p.pr_ek5,3,0);
    out_quest ((char *) &lipreise_p.pr_ek6,3,0);
    out_quest ((char *) &lipreise_p.pr_ek7,3,0);
    out_quest ((char *) &lipreise_p.pr_ek8,3,0);
    out_quest ((char *) &lipreise_p.pr_ek9,3,0);
    out_quest ((char *) &lipreise_p.pr_ek10,3,0);
    out_quest ((char *) &lipreise_p.pr_ek11,3,0);
    out_quest ((char *) &lipreise_p.pr_ek12,3,0);
    out_quest ((char *) &lipreise_p.pr_ek13,3,0);
    out_quest ((char *) &lipreise_p.pr_ek14,3,0);
    out_quest ((char *) &lipreise_p.posi,1,0);
            cursor_best = prepare_sql ("select "
"lipreise_p.mdn,  lipreise_p.nr,  lipreise_p.a,  lipreise_p.pr_ek1,  "
"lipreise_p.pr_ek2,  lipreise_p.pr_ek3,  lipreise_p.pr_ek4,  "
"lipreise_p.pr_ek5,  lipreise_p.pr_ek6,  lipreise_p.pr_ek7,  "
"lipreise_p.pr_ek8,  lipreise_p.pr_ek9,  lipreise_p.pr_ek10,  "
"lipreise_p.pr_ek11,  lipreise_p.pr_ek12,  lipreise_p.pr_ek13,  "
"lipreise_p.pr_ek14,  lipreise_p.posi from lipreise_p "

#line 30 "lipreise_p.rpp"
                                  "where nr = ? and a = ?  ");


    ins_quest ((char *) &lipreise_p.mdn,1,0);
    ins_quest ((char *) &lipreise_p.nr,1,0);
    ins_quest ((char *) &lipreise_p.a,3,0);
    ins_quest ((char *) &lipreise_p.pr_ek1,3,0);
    ins_quest ((char *) &lipreise_p.pr_ek2,3,0);
    ins_quest ((char *) &lipreise_p.pr_ek3,3,0);
    ins_quest ((char *) &lipreise_p.pr_ek4,3,0);
    ins_quest ((char *) &lipreise_p.pr_ek5,3,0);
    ins_quest ((char *) &lipreise_p.pr_ek6,3,0);
    ins_quest ((char *) &lipreise_p.pr_ek7,3,0);
    ins_quest ((char *) &lipreise_p.pr_ek8,3,0);
    ins_quest ((char *) &lipreise_p.pr_ek9,3,0);
    ins_quest ((char *) &lipreise_p.pr_ek10,3,0);
    ins_quest ((char *) &lipreise_p.pr_ek11,3,0);
    ins_quest ((char *) &lipreise_p.pr_ek12,3,0);
    ins_quest ((char *) &lipreise_p.pr_ek13,3,0);
    ins_quest ((char *) &lipreise_p.pr_ek14,3,0);
    ins_quest ((char *) &lipreise_p.posi,1,0);
            sqltext = "update lipreise_p set "
"lipreise_p.mdn = ?,  lipreise_p.nr = ?,  lipreise_p.a = ?,  "
"lipreise_p.pr_ek1 = ?,  lipreise_p.pr_ek2 = ?,  "
"lipreise_p.pr_ek3 = ?,  lipreise_p.pr_ek4 = ?,  "
"lipreise_p.pr_ek5 = ?,  lipreise_p.pr_ek6 = ?,  "
"lipreise_p.pr_ek7 = ?,  lipreise_p.pr_ek8 = ?,  "
"lipreise_p.pr_ek9 = ?,  lipreise_p.pr_ek10 = ?,  "
"lipreise_p.pr_ek11 = ?,  lipreise_p.pr_ek12 = ?,  "
"lipreise_p.pr_ek13 = ?,  lipreise_p.pr_ek14 = ?,  "
"lipreise_p.posi = ? "

#line 34 "lipreise_p.rpp"
                                  "where nr = ? and a = ?  ";

            ins_quest ((char *) &lipreise_p.nr, 1, 0);
            ins_quest ((char *) &lipreise_p.a, 3, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &lipreise_p.nr, 1, 0);
            ins_quest ((char *) &lipreise_p.a, 3, 0);
            test_upd_cursor = prepare_sql ("select mdn from lipreise_p "
                                  "where nr = ?  and a = ? ");
            ins_quest ((char *) &lipreise_p.nr, 1, 0);
            del_cursor = prepare_sql ("delete from lipreise_p "
                                  "where nr = ?  ");
    ins_quest ((char *) &lipreise_p.mdn,1,0);
    ins_quest ((char *) &lipreise_p.nr,1,0);
    ins_quest ((char *) &lipreise_p.a,3,0);
    ins_quest ((char *) &lipreise_p.pr_ek1,3,0);
    ins_quest ((char *) &lipreise_p.pr_ek2,3,0);
    ins_quest ((char *) &lipreise_p.pr_ek3,3,0);
    ins_quest ((char *) &lipreise_p.pr_ek4,3,0);
    ins_quest ((char *) &lipreise_p.pr_ek5,3,0);
    ins_quest ((char *) &lipreise_p.pr_ek6,3,0);
    ins_quest ((char *) &lipreise_p.pr_ek7,3,0);
    ins_quest ((char *) &lipreise_p.pr_ek8,3,0);
    ins_quest ((char *) &lipreise_p.pr_ek9,3,0);
    ins_quest ((char *) &lipreise_p.pr_ek10,3,0);
    ins_quest ((char *) &lipreise_p.pr_ek11,3,0);
    ins_quest ((char *) &lipreise_p.pr_ek12,3,0);
    ins_quest ((char *) &lipreise_p.pr_ek13,3,0);
    ins_quest ((char *) &lipreise_p.pr_ek14,3,0);
    ins_quest ((char *) &lipreise_p.posi,1,0);
            ins_cursor = prepare_sql ("insert into lipreise_p ("
"mdn,  nr,  a,  pr_ek1,  pr_ek2,  pr_ek3,  pr_ek4,  pr_ek5,  pr_ek6,  pr_ek7,  pr_ek8,  pr_ek9,  "
"pr_ek10,  pr_ek11,  pr_ek12,  pr_ek13,  pr_ek14,  posi) "

#line 48 "lipreise_p.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?)"); 

#line 50 "lipreise_p.rpp"
}
