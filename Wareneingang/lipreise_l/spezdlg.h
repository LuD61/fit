#ifndef _SPEZDLG_DEF
#define _SPEZDLG_DEF
#include "dlg.h"
#include "itprog.h"
#include "itfont.h"
#include "formfield.h"
#include "listdlg.h"
//#include "mo_txt.h"
#include "Work.h"
#include "cmask.h"

#define MDN_CTL 1801
#define FIL_CTL 1802

#define HWG_CTL 1804
#define KW_CTL 1805
#define A_CTL 1806
#define AVON_CTL 1807
#define ABIS_CTL 1808
#define JR_CTL 1809

#define NR_BZ_CTL 1900
#define DAT_VON_CTL 1901
#define DAT_BIS_CTL 1902
#define BPREIS_CTL 1903
#define LIEF1_CTL 1911
#define LIEF2_CTL 1912
#define LIEF3_CTL 1913
#define LIEF4_CTL 1914
#define LIEF5_CTL 1915
#define LIEF6_CTL 1916
#define LIEF7_CTL 1917
#define LIEF8_CTL 1918
#define LIEF9_CTL 1919
#define LIEF10_CTL 1920
#define LIEF11_CTL 1921
#define LIEF12_CTL 1922
#define LIEF13_CTL 1923
#define LIEF14_CTL 1924

#define IDM_CHOISE 2002
#define IDM_QUIK   2003

#define HEADCTL 700
#define POSCTL 701

#define RAISEDBORDER 0
#define HIGHBORDER 1
#define LOWBORDER 2
#define HIGHCOLBORDER 3
#define LOWCOLBORDER 4
#define LINEBORDER 5



#define IDM_LIST 2001
#define IDM_DOCKLIST 2003
#define IDM_INSERT 2004
#define IDM_DELALL 5000
#define IDM_CHOISEPRINTER 2016
#define IDM_MALIEF 2017
#define IDM_MAA 2018
#define IDM_SCRPRINT 2019

#define ENTERHEAD 0


struct DEFAULTFF
{
      char mdn [7];
      char mdn_krz [20];
      char fil [7];
      char fil_krz [18];
      char nr [10];
      char nr_bz [28];
      char jr [10];
	  char dat_von [12];
	  char dat_bis [12];
	  char basis_preis [16];
      char kw [5];
      char a [15];
      char a_bz1 [28];
	  char a_von [14];
	  char a_bis [14];
	  char lief1 [17];
	  char lief2 [17];
	  char lief3 [17];
	  char lief4 [17];
	  char lief5 [17];
	  char lief6 [17];
	  char lief7 [17];
	  char lief8 [17];
	  char lief9 [17];
	  char lief10 [17];
	  char lief11 [17];
	  char lief12 [17];
	  char lief13 [17];
	  char lief14 [17];
}; //defaultlistf, defaultlistf_null;
extern struct DEFAULTFF defaultlistf, defaultlistf_null;





class SpezDlg : virtual public DLG 
{
          private :
             static mfont *Font;
             static CFIELD *_fHead [];
             static CFIELD *_fPos [];
             static CFORM fHead;
             static CFORM fPos;
             static CFIELD *_fcField [];
             static CFORM fcForm;
             static CFIELD *_fcField0 [];
             static CFORM fcForm0;

             static ItProg *After [];
             static ItProg *Before [];
             static ItFont *itFont [];
             static FORMFIELD *dbfields [];
             static FORMFIELD *dbheadfields [];
             static char *EnableHead[];
             static char *EnableHeadFil[];
             static char *EnablePos [];
             static char *EnableA [];
 			 static BitImage ImageDelete;
			 static BitImage ImageInsert;
             int    BorderType;
             static Work work;
             static BOOL WriteOK;
             static char *HelpName;
             static BOOL NewRec;
             static HBITMAP ListBitmap;
             static HBITMAP ListMask;
             static HBITMAP ListBitmapi;
			 static BitImage ImageList;
			 static COLORREF Colors[];
			 static COLORREF BkColors[];
             CFORM *Toolbar2;

             static SpezDlg *ActiveSpezDlg;
             static int pos;
             static char ptwert[];
             LISTDLG *ListDlg;
             COLORREF ListColor;
             COLORREF ListBkColor;
             BOOL ColorSet;
             int ListLines;
             BOOL LineSet;
             HWND hwndCombo1;
             HWND hwndCombo2;
             char **Combo1;
             char **Combo2;
             HWND *hwndList;
             BOOL DockList;
             BOOL DockMode;

          public :

            static BOOL ListButtons;
            static int ButtonSize;
			static double ArtikelVon;
			static int liste_breit_ab;
			static double ArtikelBis;
            static HBITMAP SelBmp;
            static int MeKzDefault;
            static COLORREF SysBkColor;

            void SethwndList (HWND *hwndList)
            {
                this->hwndList = hwndList;
            }

            HWND *GethwndList (void)
            {
                return hwndList;
            }

            void SetToolbar2 (CFORM *Toolbar2)
            {
                this->Toolbar2 = Toolbar2;
            }

            CFORM *GetToolbar2 (void)
            {
                return Toolbar2;
            }

            void SetDockList (BOOL DockList)
            {
                this->DockList = DockList;
            }

            BOOL GetDockList (void)
            {
                return DockList;
            }

            void SetDockMode (BOOL DockMode)
            {
                this->DockMode = DockMode;
            }

            BOOL GetDockMode (void)
            {
                return DockMode;
            }

 	        SpezDlg (int, int, int, int, char *, int, BOOL);
 	        SpezDlg (int, int, int, int, char *, int, BOOL, int);
 	        SpezDlg (int, int, int, int, char *, int, BOOL, int, int);
 	        void Init (int, int, int, int, char *, int, BOOL);
            BOOL OnKeyDown (void);
            BOOL OnKeyUp (void);
            BOOL OnKey1 (void);
            BOOL OnKey2 (void);
            BOOL OnKey3 (void);
            BOOL OnKey4 (void);
            BOOL OnKey5 (void);
            BOOL OnKey6 (void);
            BOOL OnKey7 (void);
            BOOL OnKey9 (void);
            BOOL OnKey10 (void);
            BOOL OnKey12 (void);
            BOOL OnKeyDelete (void);
            BOOL OnKeyInsert (void);
            BOOL OnKeyPrior (void);
            BOOL OnKeyNext (void);
            BOOL OnKeyHome (void);
            BOOL OnKeyEnd (void);
            BOOL OnKeyReturn (void);
            BOOL OnPaint (HWND,HDC,UINT,WPARAM,LPARAM);
            BOOL OnActivate (HWND,UINT,WPARAM,LPARAM);
            BOOL OnCommand (HWND,UINT,WPARAM,LPARAM);
            BOOL OnSysCommand (HWND,UINT,WPARAM,LPARAM);
            BOOL OnDestroy (HWND,UINT,WPARAM,LPARAM);
            BOOL OnMove (HWND,UINT,WPARAM,LPARAM);
            BOOL OnCloseUp (HWND,UINT,WPARAM,LPARAM);
            BOOL ChangeDockList (void);
            BOOL ChangeMatrix (void);
            BOOL ChangeScrPrint (void);
            void PrintComment (char *);
            HWND OpenWindow (HANDLE, HWND);
            HWND OpenScrollWindow (HANDLE, HWND);
            void SetWinBackground (COLORREF);
            void ToClipboard (void);
            void FromClipboard (void);
            void Help (void);
            int Deletelipreise (void);
            BOOL ShowMdn (void);
            BOOL ShowArtikel (void);
            BOOL ShowLief (short);
            BOOL ShowNr (void);
            BOOL ShowFil (void);
            BOOL ShowHwg (void);
            void CallInfo (void);
            void SetListColors (COLORREF, COLORREF);
            static int ReadMdn (void);
            static int ReadFil (void);
            static int ReadNr (void);
            static int HoleDatum (void);
            static int ArtikelHinzufuegen (void);
            static int ReadHwg (void);
            static int EnterPos (void);
            static int GetPtab (void);
            HWND SetToolCombo (int, int, int, int);
            void SetComboTxt (HWND, char **, int, int);
			void ChoiseCombo1 (void);
			void ChoiseCombo2 (void);
            void SetListLines (int);
            void ChangeF6 (DWORD);
            void DestroyPos (void);
            void SetPos (void);
            void ShowList (void);
            void ProcessMessages (void);
            void RemoveA (BOOL);
};
#endif
