#include <windows.h>
#include <stdio.h>
#include "Work.h"
#include "strfkt.h"
#include "mo_menu.h"
//#include "mo_wdebug.h"

extern char LONGNULL[];

#define MAXTRIES 100

long Work::LongNull = 0x80000000;


void Work::ExitError (BOOL b)
{
    if (b)
    {
        disp_mess ("Speicher kann nicht zugeornet werden", 2);
        ExitProcess (1);
    }
}

void Work::TestNullDat (long *dat)
{
    if (*dat == 0l)
    {
        memcpy (dat, (long *) LONGNULL, sizeof (long));
    }
}

   

void Work::FillRow (void)
{

    if (Lsts == NULL) return;
}


void Work::InitRow (void)
{
    if (Lsts == NULL) return;
}

int Work::GetPtab (char *dest, char *item, char *wert)
{
    int dsqlstatus;

    dest[0] = 0;
    dsqlstatus = Ptab.lese_ptab (item, wert);  
    if (dsqlstatus == 100)
    {
        return -1;
    }
    strcpy (dest, ptabn.ptbezk);
    return 0;
}

int Work::ShowPtab (char *dest, char *item)
{
    int dsqlstatus;

    dsqlstatus = Ptab.Show (item);  
    if (syskey == KEY5)
    {
        return -1;
    }
    strcpy (dest, ptabn.ptwert);
    return 0;
}

int Work::ShowPtabEx (HWND hMainWindow, char *dest, char *item)
{
       char wert [80];
       SEARCHPTAB *SearchPtab;
	   HINSTANCE hMainInst;


       hMainInst = (HINSTANCE) GetWindowLong (hMainWindow, GWL_HINSTANCE);
       SearchPtab = new SEARCHPTAB (item);
       SearchPtab->SetParams (hMainInst, hMainWindow); 
       SearchPtab->Setawin (hMainWindow);
                                                  
       SearchPtab->Search ();
       if (SearchPtab->GetKey (wert) == TRUE)
       {
           delete SearchPtab;
           strcpy (dest, wert);
		   return 0;
       }
       delete SearchPtab;
       return -1;
}


int Work::GetMdnName (char *dest, short mdn)
{
    int dsqlstatus;

    if (mdn == 0)
    {
        strcpy (dest, "Zentrale");
        return 0;
    }
    dest[0] = 0;
    DbClass.sqlin ((short *) &mdn, 1, 0);
    DbClass.sqlout ((char *) dest, 0, 17);
    dsqlstatus =  DbClass.sqlcomm ("select adr.adr_krz from mdn,adr "
                                   "where mdn.mdn = ? " 
                                   "and adr.adr = mdn.adr");
    if (dsqlstatus == 100)
    {
        return 100;
    }
    return Mdn.lese_mdn (mdn);
}


int Work::GetFilName (char *dest, short mdn, short fil)
{

    if (mdn == 0 || fil == 0)
    {
        strcpy (dest, "Mandant");
        return 0;
    }
    dest[0] = 0;
    DbClass.sqlin ((short *) &mdn, 1, 0);
    DbClass.sqlin ((short *) &fil, 1, 0);
    DbClass.sqlout ((char *) dest, 0, 17);
    return DbClass.sqlcomm ("select adr.adr_krz from fil,adr "
                             "where fil.mdn = ? " 
                             "and   fil.fil = ? " 
                             "and adr.adr = fil.adr");
}

int Work::GetNrBez (char *dest, short nr)
{
    int dsqlstatus;

    if (nr == 0) return 0;
    dest[0] = 0;
    DbClass.sqlin ((short *) &nr, 1, 0);
    DbClass.sqlout ((char *) dest, 0, 25);
    dsqlstatus =  DbClass.sqlcomm ("select nr_bz from lipreise_k "
                                   "where nr = ? "); 
    if (dsqlstatus == 100)
    {
        return 100;
    }
    return 0;
}


void Work::BeginWork (void)
{
    ::beginwork ();
}

void Work::CommitWork (void)
{
    ::commitwork ();
}

void Work::RollbackWork (void)
{
    ::rollbackwork ();
}

void Work::InitRec (void)
{
}



void Work::DeleteAllPos (void)
{
    DbClass.sqlin ((long *) &lipreise_k.nr, 1, 0);
	DbClass.sqlcomm ("delete from lipreise_p where nr = ?");
}


void Work::PrepareLst (void)
{
}

void Work::OpenLst (void)
{
	if (LstCursor == -1)
	{
		PrepareLst ();
		if (LstCursor == -1) return;
	}
}

int Work::FetchLst (void)
{
	if (LstCursor == -1)
	{
		PrepareLst ();
		if (LstCursor == -1) return 100;
	}
    return 0;
}

void Work::CloseLst (void)
{
	if (LstCursor != -1)
	{
		LstCursor = -1;
	}
}

long Work::GetZeileAnz (short nr)
{
    long anz;

    MatrixTyp = ARTIKEL;
    DbClass.sqlin ((short *) &nr, 1, 0);
    cursor = DbClass.sqlcursor ("select a from lipreise_p where nr = ? and a > 0 ");
    anz = 0;
    while (DbClass.sqlfetch (cursor) == 0)
    {
           if (anz >= MAXA) break;
           anz ++;
    }
    DbClass.sqlclose (cursor);
    cursor = -1;
    return anz;
}


long Work::ZeileFirst (short nr)
{
    
    DbClass.sqlin ((short *) &nr, 1, 0);
    DbClass.sqlout ((double *) &_a_bas.a, 3, 0);
    cursor = DbClass.sqlcursor ("select a from lipreise_p where (nr = ?) " 
                                "and a > 0 "
                                "order by posi");
    int dsqlstatus = DbClass.sqlfetch (cursor);
    if (dsqlstatus == 0)
    {
        dsqlstatus = lese_a_bas (_a_bas.a);
    }
    return dsqlstatus;
}

long Work::ZeileNext (void)
{

    if (cursor == -1) 
    {
        return 100;
    }
    int dsqlstatus = DbClass.sqlfetch (cursor);
    if (dsqlstatus == 0)
    {
        dsqlstatus = lese_a_bas (_a_bas.a);
    }
    else
    {
        DbClass.sqlclose (cursor);
        cursor = -1;
    }
    return dsqlstatus;
}

long Work::GetLiefAnz (short nr)
{
    long anz;
    char sqltext [500];


    this->lipreise_nr = nr;
    MatrixTyp = LIEFERANT;
/***
    cursor = DbClass.sqlcursor ("select distinct (lief.lief) from lief, lief_bzg "
                                "where lief.lief > \"               \" "
								"and lief_bzg.lief = lief.lief "
                                "and lief.lief != \"-1\"");
****/
    
	    DbClass.sqlin ((short *) &nr, 1, 0);
	    DbClass.sqlout ((char *) lief_bzg.lief, 0, 17);
	    DbClass.sqlout ((char *) _adr.adr_krz, 0, 17);

		strcpy (sqltext,"select distinct (lief.lief), adr.adr_krz from lief,adr,lipreise_k "); 
	    strcat(sqltext,"where lief.lief > \"               \" ");
	    strcat(sqltext,"and lief.lief != \"-1\" ");
	    strcat(sqltext,"and lief.lief != \"0\" ");
	    strcat(sqltext,"and adr.adr = lief.adr ");
	    strcat(sqltext,"and lipreise_k.nr = ? ");
		strcat(sqltext,"and lief.lief in (lipreise_k.lief1,lipreise_k.lief2,lipreise_k.lief3,lipreise_k.lief4,lipreise_k.lief5,lipreise_k.lief6,lipreise_k.lief7,lipreise_k.lief8,lipreise_k.lief9,lipreise_k.lief10,lipreise_k.lief11,lipreise_k.lief12) ");
    cursor = DbClass.sqlcursor (sqltext);
    anz = 0;
    while (DbClass.sqlfetch (cursor) == 0)
    {
           if (anz >= MAXA) break;
           anz ++;
    }
    DbClass.sqlclose (cursor);
    cursor = -1;
    return anz;
}

long Work::HoleLief (short nr,short z)
{
            char sqltext [500];
            char text [50];
    

    this->lipreise_nr = nr;
	    DbClass.sqlin ((short *) &nr, 1, 0);
	    DbClass.sqlout ((char *) lief_bzg.lief, 0, 17);
	    DbClass.sqlout ((char *) _adr.adr_krz, 0, 17);

		strcpy (sqltext,"select distinct (lief.lief), adr.adr_krz from lief,adr,lipreise_k "); 
	    strcat(sqltext,"where lief.lief > \"               \" ");
	    strcat(sqltext,"and lief.lief != \"-1\" ");
	    strcat(sqltext,"and lief.lief != \"0\" ");
	    strcat(sqltext,"and adr.adr = lief.adr ");
	    strcat(sqltext,"and lipreise_k.nr = ? ");
		strcat(sqltext,"and lief.lief = ");
		sprintf (text,"lipreise_k.lief%hd",z);
		strcat(sqltext,text);
        return DbClass.sqlcomm (sqltext);
}

long Work::LiefFirst (short nr)
{
            char sqltext [500];
    

    this->lipreise_nr = nr;
	    DbClass.sqlin ((short *) &nr, 1, 0);
	    DbClass.sqlout ((char *) lief_bzg.lief, 0, 17);
	    DbClass.sqlout ((char *) _adr.adr_krz, 0, 17);

		strcpy (sqltext,"select distinct (lief.lief), adr.adr_krz from lief,adr,lipreise_k "); 
	    strcat(sqltext,"where lief.lief > \"               \" ");
	    strcat(sqltext,"and lief.lief != \"-1\" ");
	    strcat(sqltext,"and lief.lief != \"0\" ");
	    strcat(sqltext,"and adr.adr = lief.adr ");
	    strcat(sqltext,"and lipreise_k.nr = ? ");
		strcat(sqltext,"and lief.lief in (lipreise_k.lief1,lipreise_k.lief2,lipreise_k.lief3,lipreise_k.lief4,lipreise_k.lief5,lipreise_k.lief6,lipreise_k.lief7,lipreise_k.lief8,lipreise_k.lief9,lipreise_k.lief10,lipreise_k.lief11,lipreise_k.lief12) ");
       strcat(sqltext,"order by lief.lief");
    cursor = DbClass.sqlcursor (sqltext);
    int dsqlstatus = DbClass.sqlfetch (cursor);
    return dsqlstatus;
}

long Work::LiefNext (void)
{

    if (cursor == -1) 
    {
        return 100;
    }
    int dsqlstatus = DbClass.sqlfetch (cursor);
    if (dsqlstatus == 100)
    {
        DbClass.sqlclose (cursor);
        cursor = -1;
    }
    return dsqlstatus;
}

BOOL Work::GetABz1 (double a, char *a_bz1)
{
	  strcpy (a_bz1, "");
	  if (lese_a_bas (a) == 0)
	  {
		  strcpy (a_bz1, _a_bas.a_bz1);
		  return TRUE;
	  }
	  return FALSE;
}


int Work::PrepareLief (void)
{
 
     DbClass.sqlin ((short *)  &lipreise_nr, 1, 0);
     DbClass.sqlout ((double *) &_a_bas.a, 3, 0);
     DbClass.sqlout ((char *)   _a_bas.a_bz1, 0, 25);
//     DbClass.sqlout ((double *) &_a_bas.a, 3, 0);
     DbClass.sqlout ((double *)  &lipreise_art, 3, 0);
     DbClass.sqlout ((short *)  &lipreise_posi, 1, 0);

	 //060609 order by von posi auf a ge�ndert 

	 if (order_by_posi == 1)
	 {
		cursor = DbClass.sqlcursor ("select distinct (a_bas.a), a_bas.a_bz1,lipreise_p.a,lipreise_p.posi from a_bas,lipreise_p "
		                        "where "
								"a_bas.a = lipreise_p.a "
								"and lipreise_p.nr = ? "
                                "and a_bas.a > 0 "
                                "order by lipreise_p.posi");
	 }
	 else
	 {
		cursor = DbClass.sqlcursor ("select distinct (a_bas.a), a_bas.a_bz1,lipreise_p.a,lipreise_p.posi from a_bas,lipreise_p "
		                        "where "
								"a_bas.a = lipreise_p.a "
								"and lipreise_p.nr = ? "
                                "and a_bas.a > 0 "
                                "order by lipreise_p.a");
	 }

     DbClass.sqlin ((char *)   lief_bzg.lief, 0, 17);
     DbClass.sqlin ((short *)  &lief_bzg.mdn, 1,0);
     DbClass.sqlin ((double *) &_a_bas.a, 3,0);
     DbClass.sqlout ((double *) &lief_bzg.pr_ek_eur, 3,0);
     cursor_a = DbClass.sqlcursor ("select pr_ek_eur from lief_bzg "
                                   "where lief = ? "
                                   "and mdn = ? "
                                   "and a = ?");

     DbClass.sqlin ((char *)   lief_bzg.lief, 0, 17);
     DbClass.sqlin ((short *)  &lief_bzg.mdn, 1,0);
     DbClass.sqlin ((double *) &_a_bas.a, 3,0);
     DbClass.sqlin ((short *)  &lipreise_k.jr, 1,0);
     DbClass.sqlin ((short *)  &lipreise_k.kw, 1,0);
     DbClass.sqlout ((double *) &lief_bzg.pr_ek_eur, 3,0);
     DbClass.sqlout ((double *) &lipreise_prot.kw, 1,0);
     cursor_a_prot = DbClass.sqlcursor ("select pr_ek,kw from lipreise_prot "
                                   "where lief = ? "
                                   "and mdn = ? "
                                   "and a = ? "
								   "and  jr = ? and kw = ? " );

     DbClass.sqlin ((char *)   lief_bzg.lief, 0, 17);
     DbClass.sqlin ((short *)  &lief_bzg.mdn, 1,0);
     DbClass.sqlin ((double *) &_a_bas.a, 3,0);
     DbClass.sqlout ((double *) &lief_bzg.pr_ek_eur, 3,0);
     DbClass.sqlout ((double *) &lipreise_prot.kw, 1,0);
     DbClass.sqlout ((double *) &lipreise_prot.jr, 1,0);
     cursor_a_prot_all = DbClass.sqlcursor ("select pr_ek,kw,jr from lipreise_prot "
                                   "where lief = ? "
                                   "and mdn = ? "
                                   "and a = ? "
								   "order by jr desc, kw desc ");
     return cursor;
}




int Work::PruefeLipreise_Prot (void)
{
//     DbClass.sqlin ((short *)  &lipreise_k.jr, 1,0);
//     DbClass.sqlin ((short *)  &lipreise_k.kw, 1,0);
//     DbClass.sqlin ((short *)  &lipreise_k.jr, 1,0);
     DbClass.sqlout ((double *) &lief_bzg.pr_ek_eur, 3,0);
     int dsqlstatus = DbClass.sqlcomm("select pr_ek from lipreise_prot ");
//								   "where  "
//								   " (jr = ? and kw <= ?) or jr < ? " );

	 flg_preise_prot = 0;
	 if (dsqlstatus == 0) flg_preise_prot = 1;



     return flg_preise_prot;
}


     
int Work::Prepare (void)
{

     if (MatrixTyp == LIEFERANT)
     {
         return PrepareLief (); 
     }
/*
     DbClass.sqlout ((char *)   lief_bzg.lief, 0, 17);
     DbClass.sqlin ((short *)  &lief_bzg.mdn, 1,0);
     cursor = DbClass.sqlcursor ("select lief from lief_bzg where mdn = ? order by lief");
*/
     DbClass.sqlout ((char *)   lief_bzg.lief, 0, 17);
     DbClass.sqlout ((char *) _adr.adr_krz, 0, 17);
//     DbClass.sqlin ((char *)   lief_bzg.lief, 0, 17);
     cursor = DbClass.sqlcursor ("select lief, adr_krz from lief,adr "
                                 "where lief > \"-1\" "
                                 "and adr.adr = lief.adr "
                                 "order by lief");
     DbClass.sqlin ((char *)   lief_bzg.lief, 0, 17);
     DbClass.sqlin ((short *)  &lief_bzg.mdn, 1,0);
     DbClass.sqlin ((double *) &_a_bas.a, 3,0);
     DbClass.sqlout ((double *) &lief_bzg.pr_ek_eur, 3,0);
     cursor_a = DbClass.sqlcursor ("select pr_ek_eur from lief_bzg "
                                   "where lief = ? "
                                   "and mdn = ? "
                                   "and a = ?");
     return cursor;
}

void Work::Open (void)
{
	if (cursor == -1)
	{
		Prepare ();
		if (cursor == -1) return;
	}
    DbClass.sqlopen (cursor);
}

int Work::Fetch (void)
{
	if (cursor == -1)
	{
		Prepare ();
		if (cursor == -1) return 100;
	}
    int dsqlstatus = DbClass.sqlfetch (cursor);
/*
    if (dsqlstatus == 0)
    {
        DbClass.sqlopen (cursor_lief);
        DbClass.sqlfetch (cursor_lief);
    }
*/
	return dsqlstatus;
}

int Work::FetchLiefBzg ()
{
	 if (cursor == -1)
	 {
		 Prepare ();
	 }
	 if (Mandant_fest != -1) lief_bzg.mdn = Mandant_fest;
     lief_bzg.pr_ek_eur = 0.0;
     open_sql (cursor_a);
     return fetch_sql (cursor_a);
}
int Work::FetchLipreise_Prot ()
{
	if (flg_preise_prot == 0) return 100;
	 if (cursor == -1)
	 {
		 Prepare ();
	 }
	 if (Mandant_fest != -1) lief_bzg.mdn = Mandant_fest;
     lief_bzg.pr_ek_eur = 0.0;
     open_sql (cursor_a_prot);
     return fetch_sql (cursor_a_prot);
}

int Work::FetchAllLipreise_Prot ()
{
	if (flg_preise_prot == 0) return 100;
	 if (cursor == -1)
	 {
		 Prepare ();
	 }
	 if (Mandant_fest != -1) lief_bzg.mdn = Mandant_fest;
     lief_bzg.pr_ek_eur = 0.0;
     open_sql (cursor_a_prot_all);
     return fetch_sql (cursor_a_prot_all);
}

void Work::Close (void)
{

	if (cursor != -1)
	{
        DbClass.sqlclose (cursor);
        DbClass.sqlclose (cursor_lief);
        DbClass.sqlclose (cursor_a);
        DbClass.sqlclose (cursor_a_prot);
        DbClass.sqlclose (cursor_a_prot_all);
		cursor = -1;
		cursor_lief = -1;
		cursor_a = -1;
		cursor_a_prot = -1;
		cursor_a_prot_all = -1;
	}
}

int Work::WriteRec (form *UbForm, int Feldanz,int di)
{
    LIEF_BZG_CLASS LiefBzg;
    double pr_ek;
    char ek_old [20];
    char ek_new [20];

    if (Lsts == NULL) return 0;

    if (MatrixTyp == LIEFERANT)
    {
         return WriteRecLief (UbForm, Feldanz,di);
    }

    strcpy (lief_bzg.lief, Lsts[0]->GetFeld ());

    for (int i = 1; i < Feldanz; i ++)
    {
        pr_ek = ratod (Lsts[i]->GetFeld ());
        if (UbForm->mask[i].attribut == BUTTON)
        {
            lief_bzg.a = ratod (UbForm->mask[i].item->GetFeldPtr ());
        }
        else if (UbForm->mask[i].attribut == COLBUTTON)
        {
            ColButton *Cub = (ColButton *) UbForm->mask[i].item->GetFeldPtr ();
            lief_bzg.a = ratod (Cub->text1);
        }
        lief_bzg.pr_ek = 0.0;
        if (lief_bzg.a == 0.0) continue;
        int dsqlstatus = LiefBzg.dbreadfirst ();
        if (dsqlstatus != 0) 
        {
            if (pr_ek == 0.0)
            {
                   continue;
            }
            sprintf (lief_bzg.lief_best, "%.0lf", lief_bzg.a);
			lief_bzg.min_best = 1.0;
			lief_bzg.me_einh_ek = 2; //erst mal
			sprintf (lief_bzg.best_txt1, "%s", "");

        }

        sprintf (ek_old, "%.4lf", lief_bzg.pr_ek_eur);
        sprintf (ek_new, "%.4lf", pr_ek);
        if (strcmp (ek_old, ek_new) == 0) continue;
        lief_bzg.pr_ek_eur = pr_ek;
//        lief_bzg.pr_ek = pr_ek * 1.95583;
		if (Mandant_fest != -1)
		{
			lief_bzg.mdn = Mandant_fest;
		}
        dsqlstatus = LiefBzg.dbupdate ();
        if (dsqlstatus != 0)
        {
            return dsqlstatus;
        }
    }
    return 0;
}


int Work::WriteRecLief (form *UbForm, int Feldanz,int di)
{
    LIEF_BZG_CLASS LiefBzg;
    double pr_ek;
    char ek_old [20];
    char ek_new [20];
	char cdat [12];
	int dsqlstatus; 
		int best_cursor;
		int upd_best_cursor;
		int we_cursor;
		int upd_we_cursor;

    lief_bzg.a = ratod (Lsts[0]->GetFeld ());
	if (Mandant_fest != -1)
	{
		lief_bzg.mdn = Mandant_fest;
	}
	else
	{
		lief_bzg.mdn = lipreise_k.mdn;
	}
	lipreise_p.mdn = lipreise_k.mdn;
	lipreise_p.nr = lipreise_k.nr;
	lipreise_p.a = lief_bzg.a;
	dsqlstatus = lese_a_bas (lipreise_p.a);
	if (dsqlstatus != 0) return dsqlstatus;
	memcpy (&lipreise_prot, &lipreise_prot_null, sizeof (lipreise_prot));    
    for (int i = 1; i < Feldanz; i ++)
    {
		//ToDO 090609 : Wenn der Preis aus lipreise_prot kam, darf nicht wieder in lief_bzg geschrieben werden !!
        pr_ek = ratod (Lsts[i]->GetFeld ());
        if (UbForm->mask[i].attribut == BUTTON)
        {
            strcpy (lief_bzg.lief, UbForm->mask[i].item->GetFeldPtr ());
        }
        else if (UbForm->mask[i].attribut == COLBUTTON)
        {
            ColButton *Cub = (ColButton *) UbForm->mask[i].item->GetFeldPtr ();
            strcpy (lief_bzg.lief, Cub->text1);
        }


		lief.mdn = lipreise_k.mdn;
		strcpy(lief.lief,lief_bzg.lief); 
		if (Lief.dbreadfirst() != 0)
		{
			lief.mdn = 0;
			Lief.dbreadfirst();
		}

		if (strcmp(lief_bzg.lief,lipreise_k.lief1) == 0) lipreise_p.pr_ek1 = pr_ek;
		if (strcmp(lief_bzg.lief,lipreise_k.lief2) == 0) lipreise_p.pr_ek2 = pr_ek;
		if (strcmp(lief_bzg.lief,lipreise_k.lief3) == 0) lipreise_p.pr_ek3 = pr_ek;
		if (strcmp(lief_bzg.lief,lipreise_k.lief4) == 0) lipreise_p.pr_ek4 = pr_ek;
		if (strcmp(lief_bzg.lief,lipreise_k.lief5) == 0) lipreise_p.pr_ek5 = pr_ek;
		if (strcmp(lief_bzg.lief,lipreise_k.lief6) == 0) lipreise_p.pr_ek6 = pr_ek;
		if (strcmp(lief_bzg.lief,lipreise_k.lief7) == 0) lipreise_p.pr_ek7 = pr_ek;
		if (strcmp(lief_bzg.lief,lipreise_k.lief8) == 0) lipreise_p.pr_ek8 = pr_ek;
		if (strcmp(lief_bzg.lief,lipreise_k.lief9) == 0) lipreise_p.pr_ek9 = pr_ek;
		if (strcmp(lief_bzg.lief,lipreise_k.lief10) == 0) lipreise_p.pr_ek10 = pr_ek;
		if (strcmp(lief_bzg.lief,lipreise_k.lief11) == 0) lipreise_p.pr_ek11 = pr_ek;
		if (strcmp(lief_bzg.lief,lipreise_k.lief12) == 0) lipreise_p.pr_ek12 = pr_ek;
		if (strcmp(lief_bzg.lief,lipreise_k.lief13) == 0) lipreise_p.pr_ek13 = pr_ek;
		if (strcmp(lief_bzg.lief,lipreise_k.lief14) == 0) lipreise_p.pr_ek14 = pr_ek;
		lipreise_p.posi = di;
        lief_bzg.pr_ek = 0.0;
        if (lief_bzg.a == 0.0) continue;
        int dsqlstatus = LiefBzg.dbreadfirst ();
        if (dsqlstatus != 0) 
        {
            if (pr_ek == 0.0)
            {
                   continue;
            }
            sprintf (lief_bzg.lief_best, "%.0lf", lief_bzg.a);
			lief_bzg.min_best = 1.0;
			lief_bzg.me_einh_ek = 2; //erst mal
            sprintf (lief_bzg.best_txt1, "%s", "");
        }

        sprintf (ek_old, "%.4lf", lief_bzg.pr_ek_eur);
        sprintf (ek_new, "%.4lf", pr_ek);
//240909        if (strcmp (ek_old, ek_new) == 0) continue;
		lipreise_prot.bzg_ek_alt = lief_bzg.pr_ek_eur;
        lief_bzg.pr_ek_eur = pr_ek;

        lief_bzg.pr_ek = pr_ek * 1.95583;
		if (Mandant_fest != -1)
		{
			lief_bzg.mdn = Mandant_fest;
		}
        if (strcmp (ek_old, ek_new) != 0) 
		{
			dsqlstatus = LiefBzg.dbupdate ();  //nur schreiben, wenn was ge�ndert
		
			if (dsqlstatus != 0)
			{
	            return dsqlstatus;
			}
		}
		lipreise_prot.mdn = lief_bzg.mdn;

		sysdate (cdat);
		lipreise_prot.dat = dasc_to_long(cdat);
		systime(lipreise_prot.zeit);
		strncpy(lipreise_prot.lief,lief_bzg.lief,strlen(lief_bzg.lief));
		lipreise_prot.jr = lipreise_k.jr;
		lipreise_prot.kw = lipreise_k.kw;
		lipreise_prot.a = lief_bzg.a;
		lipreise_prot.pr_ek = pr_ek;
		lipreise_prot.best_blg = 0;
		strcpy(lipreise_prot.lief_rech_nr, " ");
        if (strcmp (ek_old, ek_new) == 0) 
		{
			dsqlstatus = Lipreise_prot.dbupdate ();  //240909 wenn nichts ge�ndert, trotzdem speichern und raus
			continue;
		}

//=====================================================================================
//======   best_pos    =================================================================
//=====================================================================================

		DbClass.sqlout ((short *) &best_pos.mdn, 1, 0);
		DbClass.sqlout ((short *) &best_pos.fil, 1, 0);
		DbClass.sqlout ((long *)  &best_pos.best_blg, 2, 0);
		DbClass.sqlout ((double *)  &lipreise_prot.best_ek_alt, 3, 0);
		DbClass.sqlin ((double *)  &lief_bzg.a, 3, 0);
		DbClass.sqlin ((char *)  lief_bzg.lief, 0, 17);
		DbClass.sqlin ((long *)  &lipreise_k.dat_von, 2, 0);
		DbClass.sqlin ((long *)  &lipreise_k.dat_bis, 2, 0);
		DbClass.sqlin ((double *)  &pr_ek, 3, 0);

		// nur bearb_stat < 3, ab 3 ist die Bestellung schon �bergeen am WE , dann kann dort der Preis ge�ndert werden 
		best_cursor = DbClass.sqlcursor ("select best_kopf.mdn,best_kopf.fil,best_kopf.best_blg,best_pos.pr_ek_euro_bto from best_pos,best_kopf "
			                        "where best_pos.a = ? and best_kopf.lief = ?  and best_kopf.lief_term between ? and ?  and best_kopf.bearb_stat < 3 "
									"and best_pos.pr_ek_euro_bto <> ? and best_pos.pr_fix = 0 and best_kopf.mdn = best_pos.mdn and  "
									"best_kopf.fil = best_pos.fil and best_kopf.best_blg = best_pos.best_blg");



		DbClass.sqlin ((double *)  &best_pos.pr_ek_euro_bto, 3, 0);
		DbClass.sqlin ((double *)  &best_pos.pr_ek_bto, 3, 0);
		DbClass.sqlin ((double *)  &best_pos.pr_ek_euro, 3, 0);
		DbClass.sqlin ((double *)  &best_pos.pr_ek, 3, 0);

		DbClass.sqlin ((short *) &best_pos.mdn, 1, 0);
		DbClass.sqlin ((short *) &best_pos.fil, 1, 0);
		DbClass.sqlin ((long *)  &best_pos.best_blg, 2, 0);
		DbClass.sqlin ((char *)  lief_bzg.lief, 0, 17);
		DbClass.sqlin ((double *)  &lief_bzg.a, 3, 0);
		upd_best_cursor = DbClass.sqlcursor ("update best_pos set pr_ek_euro_bto = ?, pr_ek_bto = ?, "
										"pr_ek_euro = ?, pr_ek = ? "
			                        "where mdn = ? "
									"and fil   = ? "
									"and best_blg = ? "
									"and lief = ? "
									"and a = ?");

        while (DbClass.sqlfetch (best_cursor) == 0)
		{
			best_pos.pr_ek_euro_bto = pr_ek;
			best_pos.pr_ek_euro = pr_ek;
			best_pos.pr_ek_bto = pr_ek;

            best_pos.pr_ek_euro  = pr_ek - (double) WePreis.GetRabEk 
		                           (best_pos.mdn, best_pos.fil, 
		                           lief_bzg.lief, 
	  	                           1, pr_ek, _a_bas.me_einh,_a_bas.a,_a_bas.ag,_a_bas.wg);
			best_pos.pr_ek = best_pos.pr_ek_euro;

			dsqlstatus = DbClass.sqlexecute (upd_best_cursor);
			if (dsqlstatus == 0)
			{
				if (lipreise_prot.best_blg > 0 && lipreise_prot.best_blg != best_pos.best_blg)
				{
			        dsqlstatus = Lipreise_prot.dbupdate ();
				}
				lipreise_prot.best_blg = best_pos.best_blg;
			}
		}
        dsqlstatus = Lipreise_prot.dbupdate ();
		DbClass.sqlclose (upd_best_cursor);
		DbClass.sqlclose (best_cursor);

		lipreise_prot.best_blg = 0;
		strcpy(lipreise_prot.lief_rech_nr,"");

//=====================================================================================
//======   we_pos    =================================================================
//=====================================================================================

		DbClass.sqlout ((short *) &we_jour.mdn, 1, 0);
		DbClass.sqlout ((short *) &we_jour.fil, 1, 0);
		DbClass.sqlout ((char *)  we_jour.lief_rech_nr, 0, 17);
		DbClass.sqlout ((char *)  we_jour.blg_typ, 0, 2);
		DbClass.sqlout ((double *)  &lipreise_prot.we_ek_alt, 3, 0);
		DbClass.sqlin ((double *)  &lief_bzg.a, 3, 0);
		DbClass.sqlin ((char *)  lief_bzg.lief, 0, 17);
		DbClass.sqlin ((long *)  &lipreise_k.dat_von, 2, 0);
		DbClass.sqlin ((long *)  &lipreise_k.dat_bis, 2, 0);
		DbClass.sqlin ((double *)  &pr_ek, 3, 0);

		we_cursor = DbClass.sqlcursor ("select we_kopf.mdn,we_kopf.fil,we_kopf.lief_rech_nr, "
										"we_kopf.blg_typ,we_pos.pr_ek_euro from we_kopf,we_pos "
			                        "where we_pos.a = ? and we_kopf.lief = ?  and we_kopf.we_dat between ? and ? "
									"and we_pos.pr_ek_euro <> ? and we_kopf.we_status <= 4  and we_pos.pr_fix = 0 "
									"and we_kopf.mdn = we_pos.mdn and we_kopf.fil = we_pos.fil and "
									"we_kopf.lief_rech_nr = we_pos.lief_rech_nr and we_kopf.lief = we_pos.lief "
									"and we_kopf.blg_typ = we_pos.blg_typ ");



		DbClass.sqlin ((double *)  &we_jour.pr_ek_euro, 3, 0);
		DbClass.sqlin ((double *)  &we_jour.pr_ek_nto_eu, 3, 0);
		DbClass.sqlin ((double *)  &we_jour.pr_ek, 3, 0);
		DbClass.sqlin ((double *)  &we_jour.pr_ek_nto, 3, 0);

		DbClass.sqlin ((short *) &we_jour.mdn, 1, 0);
		DbClass.sqlin ((short *) &we_jour.fil, 1, 0);
		DbClass.sqlin ((char *)  we_jour.lief_rech_nr, 0, 17);
		DbClass.sqlin ((char *)  lief_bzg.lief, 0, 16);
		DbClass.sqlin ((char *)  we_jour.blg_typ, 0, 2);
		DbClass.sqlin ((double *)  &lief_bzg.a, 3, 0);
		upd_we_cursor = DbClass.sqlcursor ("update we_pos set pr_ek_euro = ?, pr_ek_nto_eu = ?, "
										"pr_ek = ?, pr_ek_nto = ? "
			                        "where mdn = ? "
									"and fil   = ? "
									"and lief_rech_nr = ? "
									"and lief = ? "
									"and blg_typ = ? "
									"and a = ?");


		BOOL flg_we_gefunden = FALSE;
        while (DbClass.sqlfetch (we_cursor) == 0)
		{
			flg_we_gefunden = TRUE;
			we_jour.pr_ek_euro = pr_ek;
			we_jour.pr_ek = pr_ek;

            we_jour.pr_ek_nto_eu  = pr_ek - (double) WePreis.GetRabEk 
		                           (best_pos.mdn, best_pos.fil, 
		                           lief_bzg.lief, 
	  	                           1, pr_ek, _a_bas.me_einh,_a_bas.a,_a_bas.ag,_a_bas.wg);
			we_jour.pr_ek_nto = we_jour.pr_ek_nto_eu;

			dsqlstatus = DbClass.sqlexecute (upd_we_cursor);
			if (dsqlstatus == 0)
			{
				if (lipreise_prot.lief_rech_nr > 0 && lipreise_prot.lief_rech_nr != we_jour.lief_rech_nr)
				{
			        dsqlstatus = Lipreise_prot.dbupdate ();
				}
				strcpy(lipreise_prot.lief_rech_nr,we_jour.lief_rech_nr);
			}
		}
		if (flg_we_gefunden = TRUE)  dsqlstatus = Lipreise_prot.dbupdate ();
		DbClass.sqlclose (upd_we_cursor);
		DbClass.sqlclose (we_cursor);





//=====================================================================================
//======   we_jour    =================================================================
//=====================================================================================
		DbClass.sqlout ((short *) &we_jour.mdn, 1, 0);
		DbClass.sqlout ((short *) &we_jour.fil, 1, 0);
		DbClass.sqlout ((char *)  we_jour.lief_rech_nr, 0, 17);
		DbClass.sqlout ((char *)  we_jour.blg_typ, 0, 2);
		DbClass.sqlout ((double *)  &lipreise_prot.we_ek_alt, 3, 0);
		DbClass.sqlin ((double *)  &lief_bzg.a, 3, 0);
		DbClass.sqlin ((char *)  lief_bzg.lief, 0, 17);
		DbClass.sqlin ((long *)  &lipreise_k.dat_von, 2, 0);
		DbClass.sqlin ((long *)  &lipreise_k.dat_bis, 2, 0);
		DbClass.sqlin ((double *)  &pr_ek, 3, 0);

		we_cursor = DbClass.sqlcursor ("select we_kopf.mdn,we_kopf.fil,we_kopf.lief_rech_nr, "
										"we_kopf.blg_typ,we_jour.pr_ek_euro from we_kopf,we_jour "
			                        "where we_jour.a = ? and we_kopf.lief = ?  and we_kopf.we_dat between ? and ? "
									"and we_jour.pr_ek_euro <> ? and we_kopf.we_status <= 4 and we_jour.pr_fix = 0 "
									"and we_kopf.mdn = we_jour.mdn and we_kopf.fil = we_jour.fil and "
									"we_kopf.lief_rech_nr = we_jour.lief_rech_nr and we_kopf.lief = we_jour.lief "
									"and we_kopf.blg_typ = we_jour.blg_typ ");



		DbClass.sqlin ((double *)  &we_jour.pr_ek_euro, 3, 0);
		DbClass.sqlin ((double *)  &we_jour.pr_ek_nto_eu, 3, 0);
		DbClass.sqlin ((double *)  &we_jour.pr_ek, 3, 0);
		DbClass.sqlin ((double *)  &we_jour.pr_ek_nto, 3, 0);

		DbClass.sqlin ((short *) &we_jour.mdn, 1, 0);
		DbClass.sqlin ((short *) &we_jour.fil, 1, 0);
		DbClass.sqlin ((char *)  we_jour.lief_rech_nr, 0, 17);
		DbClass.sqlin ((char *)  lief_bzg.lief, 0, 16);
		DbClass.sqlin ((char *)  we_jour.blg_typ, 0, 2);
		DbClass.sqlin ((double *)  &lief_bzg.a, 3, 0);
		upd_we_cursor = DbClass.sqlcursor ("update we_jour set pr_ek_euro = ?, pr_ek_nto_eu = ?, "
										"pr_ek = ?, pr_ek_nto = ? "
			                        "where mdn = ? "
									"and fil   = ? "
									"and lief_rech_nr = ? "
									"and lief = ? "
									"and blg_typ = ? "
									"and a = ?");


		flg_we_gefunden = FALSE;
        while (DbClass.sqlfetch (we_cursor) == 0)
		{
			flg_we_gefunden = TRUE;
			we_jour.pr_ek_euro = pr_ek;
			we_jour.pr_ek = pr_ek;

            we_jour.pr_ek_nto_eu  = pr_ek - (double) WePreis.GetRabEk 
		                           (best_pos.mdn, best_pos.fil, 
		                           lief_bzg.lief, 
	  	                           1, pr_ek, _a_bas.me_einh,_a_bas.a,_a_bas.ag,_a_bas.wg);
			we_jour.pr_ek_nto = we_jour.pr_ek_nto_eu;

			dsqlstatus = DbClass.sqlexecute (upd_we_cursor);
			if (dsqlstatus == 0)
			{
				if (lipreise_prot.lief_rech_nr > 0 && lipreise_prot.lief_rech_nr != we_jour.lief_rech_nr)
				{
			        dsqlstatus = Lipreise_prot.dbupdate ();
				}
				strcpy(lipreise_prot.lief_rech_nr,we_jour.lief_rech_nr);
			}
		}
		if (flg_we_gefunden = TRUE)  dsqlstatus = Lipreise_prot.dbupdate ();
		DbClass.sqlclose (upd_we_cursor);
		DbClass.sqlclose (we_cursor);

    }
	if (strcmp(lipreise_k.lief1,"0") == 0) lipreise_p.pr_ek1 = 0.0;
	if (strcmp(lipreise_k.lief2,"0") == 0) lipreise_p.pr_ek2 = 0.0;
	if (strcmp(lipreise_k.lief3,"0") == 0) lipreise_p.pr_ek3 = 0.0;
	if (strcmp(lipreise_k.lief4,"0") == 0) lipreise_p.pr_ek4 = 0.0;
	if (strcmp(lipreise_k.lief5,"0") == 0) lipreise_p.pr_ek5 = 0.0;
	if (strcmp(lipreise_k.lief6,"0") == 0) lipreise_p.pr_ek6 = 0.0;
	if (strcmp(lipreise_k.lief7,"0") == 0) lipreise_p.pr_ek7 = 0.0;
	if (strcmp(lipreise_k.lief8,"0") == 0) lipreise_p.pr_ek8 = 0.0;
	if (strcmp(lipreise_k.lief9,"0") == 0) lipreise_p.pr_ek9 = 0.0;
	if (strcmp(lipreise_k.lief10,"0") == 0) lipreise_p.pr_ek10 = 0.0;
	if (strcmp(lipreise_k.lief11,"0") == 0) lipreise_p.pr_ek11 = 0.0;
	if (strcmp(lipreise_k.lief12,"0") == 0) lipreise_p.pr_ek12 = 0.0;
	if (strcmp(lipreise_k.lief13,"0") == 0) lipreise_p.pr_ek13 = 0.0;
	if (strcmp(lipreise_k.lief14,"0") == 0) lipreise_p.pr_ek14 = 0.0;
	Lipreise_p.dbupdate();
    return 0;
}
int Work::Deletelipreise (void)
{
    Lipreise_k.dbdelete ();
    DeleteAllPos ();
    return 0;
}

int Work::Readlipreise_k (short mdn, short nr)
{
    int dsqlstatus;

    memcpy (&lipreise_k, &lipreise_k_null, sizeof (lipreise_k));
    lipreise_k.mdn = mdn;
    lipreise_k.nr = nr;
    dsqlstatus = Lipreise_k.dbreadfirst ();
    return dsqlstatus;
}

int Work::Writelipreise_k (void)
{
     int dsqlstatus , di;
    lipreise_k.anzlief = 0;
	di = 1;
	while (di == 1)
	{
		di = 0;
		if (atoi(lipreise_k.lief1) == 0 && atoi(lipreise_k.lief2) != 0 ) 
		{
			strcpy(lipreise_k.lief1,lipreise_k.lief2);
			strcpy(lipreise_k.lief2,"0");
			di = 1;
		}
		if (atoi(lipreise_k.lief2) == 0 && atoi(lipreise_k.lief3) != 0 ) 
		{
			strcpy(lipreise_k.lief2,lipreise_k.lief3);
			strcpy(lipreise_k.lief3,"0");
			di = 1;
		}
		if (atoi(lipreise_k.lief3) == 0 && atoi(lipreise_k.lief4) != 0 ) 
		{
			strcpy(lipreise_k.lief3,lipreise_k.lief4);
			strcpy(lipreise_k.lief4,"0");
			di = 1;
		}
		if (atoi(lipreise_k.lief4) == 0 && atoi(lipreise_k.lief5) != 0 ) 
		{
			strcpy(lipreise_k.lief4,lipreise_k.lief5);
			strcpy(lipreise_k.lief5,"0");
			di = 1;
		}
		if (atoi(lipreise_k.lief5) == 0 && atoi(lipreise_k.lief6) != 0 ) 
		{
			strcpy(lipreise_k.lief5,lipreise_k.lief6);
			strcpy(lipreise_k.lief6,"0");
			di = 1;
		}
		if (atoi(lipreise_k.lief6) == 0 && atoi(lipreise_k.lief7) != 0 ) 
		{
			strcpy(lipreise_k.lief6,lipreise_k.lief7);
			strcpy(lipreise_k.lief7,"0");
			di = 1;
		}
		if (atoi(lipreise_k.lief7) == 0 && atoi(lipreise_k.lief8) != 0 ) 
		{
			strcpy(lipreise_k.lief7,lipreise_k.lief8);
			strcpy(lipreise_k.lief8,"0");
			di = 1;
		}
		if (atoi(lipreise_k.lief8) == 0 && atoi(lipreise_k.lief9) != 0 ) 
		{
			strcpy(lipreise_k.lief8,lipreise_k.lief9);
			strcpy(lipreise_k.lief9,"0");
			di = 1;
		}
		if (atoi(lipreise_k.lief9) == 0 && atoi(lipreise_k.lief10) != 0 ) 
		{
			strcpy(lipreise_k.lief9,lipreise_k.lief10);
			strcpy(lipreise_k.lief10,"0");
			di = 1;
		}
		if (atoi(lipreise_k.lief10) == 0 && atoi(lipreise_k.lief11) != 0 ) 
		{
			strcpy(lipreise_k.lief10,lipreise_k.lief11);
			strcpy(lipreise_k.lief11,"0");
			di = 1;
		}
		if (atoi(lipreise_k.lief11) == 0 && atoi(lipreise_k.lief12) != 0 ) 
		{
			strcpy(lipreise_k.lief11,lipreise_k.lief12);
			strcpy(lipreise_k.lief12,"0");
			di = 1;
		}
		if (atoi(lipreise_k.lief12) == 0 && atoi(lipreise_k.lief13) != 0 ) 
		{
			strcpy(lipreise_k.lief12,lipreise_k.lief13);
			strcpy(lipreise_k.lief13,"0");
			di = 1;
		}
		if (atoi(lipreise_k.lief13) == 0 && atoi(lipreise_k.lief14) != 0 ) 
		{
			strcpy(lipreise_k.lief13,lipreise_k.lief14);
			strcpy(lipreise_k.lief14,"0");
			di = 1;
		}
	}







	if (atoi(lipreise_k.lief1)> 0) 
	{
	     DbClass.sqlin ((char *)  lipreise_k.lief1, 0, 17);
		DbClass.sqlout ((char *)   lipreise_k.lief1_bz, 0, 25);

		 dsqlstatus = DbClass.sqlcomm ("select adr.adr_krz from lief,adr "
		                        "where "
								"lief.adr = adr.adr "
								"and lief.lief = ? ");
		 if (dsqlstatus == 100) 
		 {
			 strcpy(lipreise_k.lief1,"0");
		 }
		 else lipreise_k.anzlief++;
	}
	if (atoi(lipreise_k.lief2)> 0) 
	{
	     DbClass.sqlin ((char *)  lipreise_k.lief2, 0, 17);
		DbClass.sqlout ((char *)   lipreise_k.lief2_bz, 0, 25);

		 dsqlstatus = DbClass.sqlcomm ("select adr.adr_krz from lief,adr "
		                        "where "
								"lief.adr = adr.adr "
								"and lief.lief = ? ");
		 if (dsqlstatus == 100) 
		 {
	 			 strcpy(lipreise_k.lief2,"0");
		 }
		 else lipreise_k.anzlief++;
	}
	if (atoi(lipreise_k.lief3)> 0) 
	{
	     DbClass.sqlin ((char *)  lipreise_k.lief3, 0, 17);
		DbClass.sqlout ((char *)   lipreise_k.lief3_bz, 0, 25);

		 dsqlstatus = DbClass.sqlcomm ("select adr.adr_krz from lief,adr "
		                        "where "
								"lief.adr = adr.adr "
								"and lief.lief = ? ");
		 if (dsqlstatus == 100) 
		 {
			 strcpy(lipreise_k.lief3,"0");
		 }
		 else lipreise_k.anzlief++;
	}
	if (atoi(lipreise_k.lief4)> 0) 
	{
	     DbClass.sqlin ((char *)  lipreise_k.lief4, 0, 17);
		DbClass.sqlout ((char *)   lipreise_k.lief4_bz, 0, 25);

		 dsqlstatus = DbClass.sqlcomm ("select adr.adr_krz from lief,adr "
		                        "where "
								"lief.adr = adr.adr "
								"and lief.lief = ? ");
		 if (dsqlstatus == 100) 
		 {
			 strcpy(lipreise_k.lief4,"0");
		 }
		 else lipreise_k.anzlief++;
	}
	if (atoi(lipreise_k.lief5)> 0) 
	{
	     DbClass.sqlin ((char *)  lipreise_k.lief5, 0, 17);
		DbClass.sqlout ((char *)   lipreise_k.lief5_bz, 0, 25);

		 dsqlstatus = DbClass.sqlcomm ("select adr.adr_krz from lief,adr "
		                        "where "
								"lief.adr = adr.adr "
								"and lief.lief = ? ");
		 if (dsqlstatus == 100) 
		 {
			 strcpy(lipreise_k.lief5,"0");
		 }
		 else lipreise_k.anzlief++;
	}
	if (atoi(lipreise_k.lief6)> 0) 
	{
	     DbClass.sqlin ((char *)  lipreise_k.lief6, 0, 17);
		DbClass.sqlout ((char *)  lipreise_k.lief6_bz, 0, 25);

		 dsqlstatus = DbClass.sqlcomm ("select adr.adr_krz from lief,adr "
		                        "where "
								"lief.adr = adr.adr "
								"and lief.lief = ? ");
		 if (dsqlstatus == 100) 
		 {
			 strcpy(lipreise_k.lief6,"0");
		 }
		 else lipreise_k.anzlief++;
	}
	if (atoi(lipreise_k.lief7)> 0) 
	{
	     DbClass.sqlin ((char *)  lipreise_k.lief7, 0, 17);
		DbClass.sqlout ((char *)  lipreise_k.lief7_bz, 0, 25);

		 dsqlstatus = DbClass.sqlcomm ("select adr.adr_krz from lief,adr "
		                        "where "
								"lief.adr = adr.adr "
								"and lief.lief = ? ");
		 if (dsqlstatus == 100) 
		 {
			 strcpy(lipreise_k.lief7,"0");
		 }
		 else lipreise_k.anzlief++;
	}
	if (atoi(lipreise_k.lief8)> 0) 
	{
	     DbClass.sqlin ((char *)  lipreise_k.lief8, 0, 17);
		DbClass.sqlout ((char *)   lipreise_k.lief8_bz, 0, 25);

		 dsqlstatus = DbClass.sqlcomm ("select adr.adr_krz from lief,adr "
		                        "where "
								"lief.adr = adr.adr "
								"and lief.lief = ? ");
		 if (dsqlstatus == 100) 
		 {
			 strcpy(lipreise_k.lief8,"0");
		 }
		 else lipreise_k.anzlief++;
	}
	if (atoi(lipreise_k.lief9)> 0) 
	{
	     DbClass.sqlin ((char *)  lipreise_k.lief9, 0, 17);
		DbClass.sqlout ((char *)   lipreise_k.lief9_bz, 0, 25);

		 dsqlstatus = DbClass.sqlcomm ("select adr.adr_krz from lief,adr "
		                        "where "
								"lief.adr = adr.adr "
								"and lief.lief = ? ");
		 if (dsqlstatus == 100) 
		 {
			 strcpy(lipreise_k.lief9,"0");
		 }
		 else lipreise_k.anzlief++;
	}
	if (atoi(lipreise_k.lief10)> 0) 
	{
	     DbClass.sqlin ((char *)  lipreise_k.lief10, 0, 17);
		DbClass.sqlout ((char *)   lipreise_k.lief10_bz, 0, 25);

		 dsqlstatus = DbClass.sqlcomm ("select adr.adr_krz from lief,adr "
		                        "where "
								"lief.adr = adr.adr "
								"and lief.lief = ? ");
		 if (dsqlstatus == 100) 
		 {
			 strcpy(lipreise_k.lief10,"0");
		 }
		 else lipreise_k.anzlief++;
	}
	if (atoi(lipreise_k.lief11)> 0) 
	{
	     DbClass.sqlin ((char *)  lipreise_k.lief11, 0, 17);
		DbClass.sqlout ((char *)  lipreise_k.lief11_bz, 0, 25);

		 dsqlstatus = DbClass.sqlcomm ("select adr.adr_krz from lief,adr "
		                        "where "
								"lief.adr = adr.adr "
								"and lief.lief = ? ");
		 if (dsqlstatus == 100) 
		 {
			 strcpy(lipreise_k.lief11,"0");
		 }
		 else lipreise_k.anzlief++;
	}
	if (atoi(lipreise_k.lief12)> 0) 
	{
	     DbClass.sqlin ((char *)  lipreise_k.lief12, 0, 17);
		DbClass.sqlout ((char *)  lipreise_k.lief12_bz, 0, 25);

		 dsqlstatus = DbClass.sqlcomm ("select adr.adr_krz from lief,adr "
		                        "where "
								"lief.adr = adr.adr "
								"and lief.lief = ? ");
		 if (dsqlstatus == 100) 
		 {
			 strcpy(lipreise_k.lief12,"0");
		 }
		 else lipreise_k.anzlief++;
	}
	if (atoi(lipreise_k.lief13)> 0) 
	{
	     DbClass.sqlin ((char *)  lipreise_k.lief13, 0, 17);
		DbClass.sqlout ((char *)  lipreise_k.lief13_bz, 0, 25);

		 dsqlstatus = DbClass.sqlcomm ("select adr.adr_krz from lief,adr "
		                        "where "
								"lief.adr = adr.adr "
								"and lief.lief = ? ");
		 if (dsqlstatus == 100) 
		 {
			 strcpy(lipreise_k.lief13,"0");
		 }
		 else lipreise_k.anzlief++;
	}
	if (atoi(lipreise_k.lief14)> 0) 
	{
	     DbClass.sqlin ((char *)  lipreise_k.lief14, 0, 17);
		DbClass.sqlout ((char *)   lipreise_k.lief14_bz, 0, 25);

		 dsqlstatus = DbClass.sqlcomm ("select adr.adr_krz from lief,adr "
		                        "where "
								"lief.adr = adr.adr "
								"and lief.lief = ? ");
		 if (dsqlstatus == 100) 
		 {
			 strcpy(lipreise_k.lief14,"0");
		 }
		 else lipreise_k.anzlief++;
	}
    Lipreise_k.dbupdate ();
    return 0;
}


int Work::Deletelipreise_k (void)
{
    Lipreise_k.dbdelete ();
    return 0;
}
int Work::Readlipreise_p (short mdn, short nr, double a)
{
    int dsqlstatus;

    memcpy (&lipreise_p, &lipreise_p_null, sizeof (lipreise_p));
    lipreise_p.mdn = mdn;
    lipreise_p.nr = nr;
    dsqlstatus = Lipreise_p.dbreadfirst ();
    return dsqlstatus;
}

int Work::Writelipreise_p (void)
{

    Lipreise_p.dbupdate ();
    return 0;
}


int Work::Deletelipreise_p (void)
{
    Lipreise_p.dbdelete ();
    return 0;
}
