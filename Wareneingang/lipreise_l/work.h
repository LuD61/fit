#ifndef _WEWORK_DEF
#define _WEWORK_DEF
#include "ptab.h"
#include "searchptab.h"
#include "mdn.h"
#include "dlg.h"
#include "a_bas.h"
#include "listenter.h"
#include "lief_bzg.h"
#include "lief.h"
#include "lipreise_k.h"
#include "lipreise_p.h"
#include "lipreise_prot.h"
#include "best_pos.h"
#include "we_jour.h"
#include "mo_wepr.h"
#include "dbclass.h"
#include "wmaskc.h"


#define MAXA 3000

#define ARTIKEL 0
#define LIEFERANT 1


class CFELD
{
  private :
    char feldname [40];
    char *feldadr;
    int feldlen;
  public :
      CFELD ()
      {
          feldadr  = NULL;
          feldlen  = 0;
      }

      CFELD (char *feldname, char *feldadr)
      {
          strncpy (this->feldname,feldname, 39);
          this->feldname[39] = 0;
          this->feldadr  = feldadr;
          this->feldlen  = strlen (feldadr);
      }

      CFELD (char *feldname, char *feldadr, int len)
      {
          strncpy (this->feldname,feldname, 39);
          this->feldname[39] = 0;
          this->feldadr  = feldadr;
          this->feldlen  = len;
      }


      void SetFeld (char *feldadr, int len)
      {
          this->feldadr  = feldadr;
          this->feldlen  = len;
      }
           
      void SetFeld (char *feldadr)
      {
          this->feldadr  = feldadr;
      }

      void Init (void)
      {
          if (feldadr == NULL) return;
          memset (feldadr, 0, feldlen - 1);
          feldadr [feldlen - 1] = 0;
      }

      void Init (char c)
      {
          if (feldadr == NULL) return;
          memset (feldadr, c, feldlen - 1);
          feldadr [feldlen - 1] = 0;
      }

      char *GetFeld (void)
      {
          return feldadr;
      }

      char *GetFeldname (void)
      {
          return feldname;
      }

      char *GetFeld (char *feldname)
      {
          if (strcmp (this->feldname, feldname) == 0)
          {
              return feldadr;
          }
          return NULL;
      }

      CFELD *GetClass (void)
      {
          return this;
      }

      CFELD *GetClass (char *feldname)
      {
          if (strcmp (this->feldname, feldname) == 0)
          {
              return this;
          }
          return NULL;
      }

      int GetLen (void)
      {
          return feldlen;
      }

      static int GetRecAnz (CFELD **Felder)
      {

          for (int i = 0; Felder [i] != NULL; i ++)
          {
          }
          return i;
      }

      static int GetRecLen (CFELD **Felder)
      {
          for (int i = 0, len = 0; Felder [i] != NULL; i ++)
          {
              len += Felder[i]->GetLen ();
          }
          return len;
      }

      static int GetFeldpos (CFELD **Felder, char *Feldname)
      {
          for (int i = 0; Felder[i] != NULL; i ++)
          {
              if (Felder[i]->GetClass (Feldname) != NULL)
              {
                  return i;
              }
          }
          return -1;
      }
};


class Work
{
    private :
        PTAB_CLASS Ptab;
        MDN_CLASS Mdn;
        LIPREISE_K_CLASS Lipreise_k;
        LIEF_CLASS Lief;
        LIPREISE_P_CLASS Lipreise_p;
        LIPREISE_PROT_CLASS Lipreise_prot;
        WE_PREISE WePreis;

        CFELD **Lsts;
        int reclen;
        int feldanz;
        DB_CLASS DbClass;

        static long LongNull;
        DLG *Dlg;
        LISTENTER *ListDlg;
        int LstCursor;
        int cursor;
        int cursor_a;
        int cursor_a_prot;
        int cursor_a_prot_all;
        int cursor_lief;
        int MatrixTyp;
        short lipreise_nr;
        short lipreise_posi;
        double lipreise_art;
		double a;
		char a_bz1[25];

    public :

        int Readlipreise_k (short,short);
        int Writelipreise_k (void);
        int Deletelipreise_k (void);
        int Deletelipreise (void);
        int Readlipreise_p (short,short,double);
        int Writelipreise_p (void);
        int Deletelipreise_p (void);
		short Mandant_fest ;
		short order_by_posi ;
        void SetDlg (DLG *Dlg)
        {
            this->Dlg = Dlg;
        }

        DLG *GetDlg (void)
        {
            return Dlg;
        }

        void SetListDlg (LISTENTER *ListDlg)
        {
            this->ListDlg = ListDlg;
        }

        void SetMatrix (int MatrixTyp)
        {
            this->MatrixTyp = MatrixTyp;
        }

        int GetMatrix (void)
        {
            return (MatrixTyp);
        }

        LISTENTER *GetListDlg (void)
        {
            return ListDlg;
        }

        void SetLsts (CFELD **Lsts)
        {
            this->Lsts = Lsts;
        }
        void SetLieferanten (char *Lief)
        {
			strncpy (this->Lieferanten,Lief,sizeof(this->Lieferanten));
        }
        void SetLieferantenTyp (char *Lief)
        {
			strncpy (this->LieferantenTyp,Lief,sizeof(this->LieferantenTyp));
        }
        void SetArtikelVon (double avon)
        {
			this->ArtikelVon = avon;
        }
        void SetArtikelBis (double abis)
        {
			this->ArtikelBis = abis;
		}
        void Setflg_preise_prot (short flg_pr)
        {
			this->flg_preise_prot = flg_pr;
        }
        void Setflg_prot_details (short flg_pr)
        {
			this->flg_prot_details = flg_pr;
        }
		double Getflg_preise_prot (void)
		{
			return flg_preise_prot;
		}
		double Getflg_prot_details (void)
		{
			return flg_prot_details;
		}
		double GetArtikelVon (void)
		{
			return ArtikelVon;
		}
		double GetArtikelBis (void)
		{
			return ArtikelBis;
		}

        CFELD **GetLsts (void)
        {
            return Lsts;
        }


        Work ()
        {
            Lsts = NULL;
            Dlg = NULL;
            ListDlg = NULL;
			LstCursor = -1;
//            MatrixTyp = ARTIKEL;
            MatrixTyp = LIEFERANT;
            lipreise_nr = 0;
			Mandant_fest = -1;
			order_by_posi = 0;
        }


        void ExitError (BOOL);
        void TestNullDat (long *);
        void TestAllDat (void);
        void BeginWork (void);
        void CommitWork (void);
        void RollbackWork (void);
        void InitRec (void);
        void FillRow (void);
        int PruefeLipreise_Prot (void);
        int GetPtab (char *, char *, char *);
        int ShowPtab (char *dest, char *item);
        int ShowPtabEx (HWND, char *dest, char *item);
        int GetMdnName (char *, short);
        int GetFilName (char *, short, short);
        int GetNrBez (char *, short);
        void FillRow (char *);
        void DeleteAllPos (void);
        void PrepareLst (void);
        void OpenLst (void);
        int FetchLst (void);
        void CloseLst (void);
        void InitRow (void);
        long GetZeileAnz (short);
		BOOL GetABz1 (double, char *);
        long ZeileFirst (short);
        long ZeileNext (void);
        long GetLiefAnz (short);
        long LiefFirst (short);
        long HoleLief (short,short);
        long LiefNext (void);
        int PrepareLief (void);
        int Prepare (void);
        void Open (void);
        int Fetch (void);
        int FetchLiefBzg (void);
        int FetchLipreise_Prot (void);
        int FetchAllLipreise_Prot (void);
        void Close (void);
        int WriteRec (form *,int,int);
        int WriteRecLief (form *,int,int);
		char Lieferanten [200];
		char LieferantenTyp [200];
		double ArtikelVon;
		double ArtikelBis;
		short flg_preise_prot;
		short flg_prot_details;
};

#endif