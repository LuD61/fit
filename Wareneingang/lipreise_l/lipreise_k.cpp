#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "wmask.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "lipreise_k.h"

struct LIPREISE_K lipreise_k, lipreise_k_null;

void LIPREISE_K_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &lipreise_k.nr, 1, 0);
    out_quest ((char *) &lipreise_k.mdn,1,0);
    out_quest ((char *) &lipreise_k.nr,1,0);
    out_quest ((char *) lipreise_k.nr_bz,0,25);
    out_quest ((char *) lipreise_k.lief1,0,17);
    out_quest ((char *) lipreise_k.lief2,0,17);
    out_quest ((char *) lipreise_k.lief3,0,17);
    out_quest ((char *) lipreise_k.lief4,0,17);
    out_quest ((char *) lipreise_k.lief5,0,17);
    out_quest ((char *) lipreise_k.lief6,0,17);
    out_quest ((char *) lipreise_k.lief7,0,17);
    out_quest ((char *) lipreise_k.lief8,0,17);
    out_quest ((char *) lipreise_k.lief9,0,17);
    out_quest ((char *) lipreise_k.lief10,0,17);
    out_quest ((char *) lipreise_k.lief11,0,17);
    out_quest ((char *) lipreise_k.lief12,0,17);
    out_quest ((char *) lipreise_k.lief13,0,17);
    out_quest ((char *) lipreise_k.lief14,0,17);
    out_quest ((char *) &lipreise_k.anzlief,1,0);
    out_quest ((char *) &lipreise_k.basis_preis,3,0);
    out_quest ((char *) &lipreise_k.dat_von,2,0);
    out_quest ((char *) &lipreise_k.dat_bis,2,0);
    out_quest ((char *) lipreise_k.lief1_bz,0,17);
    out_quest ((char *) lipreise_k.lief2_bz,0,17);
    out_quest ((char *) lipreise_k.lief3_bz,0,17);
    out_quest ((char *) lipreise_k.lief4_bz,0,17);
    out_quest ((char *) lipreise_k.lief5_bz,0,17);
    out_quest ((char *) lipreise_k.lief6_bz,0,17);
    out_quest ((char *) lipreise_k.lief7_bz,0,17);
    out_quest ((char *) lipreise_k.lief8_bz,0,17);
    out_quest ((char *) lipreise_k.lief9_bz,0,17);
    out_quest ((char *) lipreise_k.lief10_bz,0,17);
    out_quest ((char *) lipreise_k.lief11_bz,0,17);
    out_quest ((char *) lipreise_k.lief12_bz,0,17);
    out_quest ((char *) lipreise_k.lief13_bz,0,17);
    out_quest ((char *) lipreise_k.lief14_bz,0,17);
    out_quest ((char *) &lipreise_k.kw,1,0);
    out_quest ((char *) &lipreise_k.jr,1,0);
            cursor = prepare_sql ("select lipreise_k.mdn,  "
"lipreise_k.nr,  lipreise_k.nr_bz,  lipreise_k.lief1,  "
"lipreise_k.lief2,  lipreise_k.lief3,  lipreise_k.lief4,  "
"lipreise_k.lief5,  lipreise_k.lief6,  lipreise_k.lief7,  "
"lipreise_k.lief8,  lipreise_k.lief9,  lipreise_k.lief10,  "
"lipreise_k.lief11,  lipreise_k.lief12,  lipreise_k.lief13,  "
"lipreise_k.lief14,  lipreise_k.anzlief,  lipreise_k.basis_preis,  "
"lipreise_k.dat_von,  lipreise_k.dat_bis,  lipreise_k.lief1_bz,  "
"lipreise_k.lief2_bz,  lipreise_k.lief3_bz,  lipreise_k.lief4_bz,  "
"lipreise_k.lief5_bz,  lipreise_k.lief6_bz,  lipreise_k.lief7_bz,  "
"lipreise_k.lief8_bz,  lipreise_k.lief9_bz,  lipreise_k.lief10_bz,  "
"lipreise_k.lief11_bz,  lipreise_k.lief12_bz,  lipreise_k.lief13_bz,  "
"lipreise_k.lief14_bz,  lipreise_k.kw,  lipreise_k.jr from lipreise_k "

#line 25 "lipreise_k.rpp"
                                  "where nr = ? ");
            ins_quest ((char *) &lipreise_k.nr, 1, 0);
    out_quest ((char *) &lipreise_k.mdn,1,0);
    out_quest ((char *) &lipreise_k.nr,1,0);
    out_quest ((char *) lipreise_k.nr_bz,0,25);
    out_quest ((char *) lipreise_k.lief1,0,17);
    out_quest ((char *) lipreise_k.lief2,0,17);
    out_quest ((char *) lipreise_k.lief3,0,17);
    out_quest ((char *) lipreise_k.lief4,0,17);
    out_quest ((char *) lipreise_k.lief5,0,17);
    out_quest ((char *) lipreise_k.lief6,0,17);
    out_quest ((char *) lipreise_k.lief7,0,17);
    out_quest ((char *) lipreise_k.lief8,0,17);
    out_quest ((char *) lipreise_k.lief9,0,17);
    out_quest ((char *) lipreise_k.lief10,0,17);
    out_quest ((char *) lipreise_k.lief11,0,17);
    out_quest ((char *) lipreise_k.lief12,0,17);
    out_quest ((char *) lipreise_k.lief13,0,17);
    out_quest ((char *) lipreise_k.lief14,0,17);
    out_quest ((char *) &lipreise_k.anzlief,1,0);
    out_quest ((char *) &lipreise_k.basis_preis,3,0);
    out_quest ((char *) &lipreise_k.dat_von,2,0);
    out_quest ((char *) &lipreise_k.dat_bis,2,0);
    out_quest ((char *) lipreise_k.lief1_bz,0,17);
    out_quest ((char *) lipreise_k.lief2_bz,0,17);
    out_quest ((char *) lipreise_k.lief3_bz,0,17);
    out_quest ((char *) lipreise_k.lief4_bz,0,17);
    out_quest ((char *) lipreise_k.lief5_bz,0,17);
    out_quest ((char *) lipreise_k.lief6_bz,0,17);
    out_quest ((char *) lipreise_k.lief7_bz,0,17);
    out_quest ((char *) lipreise_k.lief8_bz,0,17);
    out_quest ((char *) lipreise_k.lief9_bz,0,17);
    out_quest ((char *) lipreise_k.lief10_bz,0,17);
    out_quest ((char *) lipreise_k.lief11_bz,0,17);
    out_quest ((char *) lipreise_k.lief12_bz,0,17);
    out_quest ((char *) lipreise_k.lief13_bz,0,17);
    out_quest ((char *) lipreise_k.lief14_bz,0,17);
    out_quest ((char *) &lipreise_k.kw,1,0);
    out_quest ((char *) &lipreise_k.jr,1,0);
            cursor_best = prepare_sql ("select "
"lipreise_k.mdn,  lipreise_k.nr,  lipreise_k.nr_bz,  lipreise_k.lief1,  "
"lipreise_k.lief2,  lipreise_k.lief3,  lipreise_k.lief4,  "
"lipreise_k.lief5,  lipreise_k.lief6,  lipreise_k.lief7,  "
"lipreise_k.lief8,  lipreise_k.lief9,  lipreise_k.lief10,  "
"lipreise_k.lief11,  lipreise_k.lief12,  lipreise_k.lief13,  "
"lipreise_k.lief14,  lipreise_k.anzlief,  lipreise_k.basis_preis,  "
"lipreise_k.dat_von,  lipreise_k.dat_bis,  lipreise_k.lief1_bz,  "
"lipreise_k.lief2_bz,  lipreise_k.lief3_bz,  lipreise_k.lief4_bz,  "
"lipreise_k.lief5_bz,  lipreise_k.lief6_bz,  lipreise_k.lief7_bz,  "
"lipreise_k.lief8_bz,  lipreise_k.lief9_bz,  lipreise_k.lief10_bz,  "
"lipreise_k.lief11_bz,  lipreise_k.lief12_bz,  lipreise_k.lief13_bz,  "
"lipreise_k.lief14_bz,  lipreise_k.kw,  lipreise_k.jr from lipreise_k "

#line 28 "lipreise_k.rpp"
                                  "where nr = ? ");


    ins_quest ((char *) &lipreise_k.mdn,1,0);
    ins_quest ((char *) &lipreise_k.nr,1,0);
    ins_quest ((char *) lipreise_k.nr_bz,0,25);
    ins_quest ((char *) lipreise_k.lief1,0,17);
    ins_quest ((char *) lipreise_k.lief2,0,17);
    ins_quest ((char *) lipreise_k.lief3,0,17);
    ins_quest ((char *) lipreise_k.lief4,0,17);
    ins_quest ((char *) lipreise_k.lief5,0,17);
    ins_quest ((char *) lipreise_k.lief6,0,17);
    ins_quest ((char *) lipreise_k.lief7,0,17);
    ins_quest ((char *) lipreise_k.lief8,0,17);
    ins_quest ((char *) lipreise_k.lief9,0,17);
    ins_quest ((char *) lipreise_k.lief10,0,17);
    ins_quest ((char *) lipreise_k.lief11,0,17);
    ins_quest ((char *) lipreise_k.lief12,0,17);
    ins_quest ((char *) lipreise_k.lief13,0,17);
    ins_quest ((char *) lipreise_k.lief14,0,17);
    ins_quest ((char *) &lipreise_k.anzlief,1,0);
    ins_quest ((char *) &lipreise_k.basis_preis,3,0);
    ins_quest ((char *) &lipreise_k.dat_von,2,0);
    ins_quest ((char *) &lipreise_k.dat_bis,2,0);
    ins_quest ((char *) lipreise_k.lief1_bz,0,17);
    ins_quest ((char *) lipreise_k.lief2_bz,0,17);
    ins_quest ((char *) lipreise_k.lief3_bz,0,17);
    ins_quest ((char *) lipreise_k.lief4_bz,0,17);
    ins_quest ((char *) lipreise_k.lief5_bz,0,17);
    ins_quest ((char *) lipreise_k.lief6_bz,0,17);
    ins_quest ((char *) lipreise_k.lief7_bz,0,17);
    ins_quest ((char *) lipreise_k.lief8_bz,0,17);
    ins_quest ((char *) lipreise_k.lief9_bz,0,17);
    ins_quest ((char *) lipreise_k.lief10_bz,0,17);
    ins_quest ((char *) lipreise_k.lief11_bz,0,17);
    ins_quest ((char *) lipreise_k.lief12_bz,0,17);
    ins_quest ((char *) lipreise_k.lief13_bz,0,17);
    ins_quest ((char *) lipreise_k.lief14_bz,0,17);
    ins_quest ((char *) &lipreise_k.kw,1,0);
    ins_quest ((char *) &lipreise_k.jr,1,0);
            sqltext = "update lipreise_k set "
"lipreise_k.mdn = ?,  lipreise_k.nr = ?,  lipreise_k.nr_bz = ?,  "
"lipreise_k.lief1 = ?,  lipreise_k.lief2 = ?,  lipreise_k.lief3 = ?,  "
"lipreise_k.lief4 = ?,  lipreise_k.lief5 = ?,  lipreise_k.lief6 = ?,  "
"lipreise_k.lief7 = ?,  lipreise_k.lief8 = ?,  lipreise_k.lief9 = ?,  "
"lipreise_k.lief10 = ?,  lipreise_k.lief11 = ?,  "
"lipreise_k.lief12 = ?,  lipreise_k.lief13 = ?,  "
"lipreise_k.lief14 = ?,  lipreise_k.anzlief = ?,  "
"lipreise_k.basis_preis = ?,  lipreise_k.dat_von = ?,  "
"lipreise_k.dat_bis = ?,  lipreise_k.lief1_bz = ?,  "
"lipreise_k.lief2_bz = ?,  lipreise_k.lief3_bz = ?,  "
"lipreise_k.lief4_bz = ?,  lipreise_k.lief5_bz = ?,  "
"lipreise_k.lief6_bz = ?,  lipreise_k.lief7_bz = ?,  "
"lipreise_k.lief8_bz = ?,  lipreise_k.lief9_bz = ?,  "
"lipreise_k.lief10_bz = ?,  lipreise_k.lief11_bz = ?,  "
"lipreise_k.lief12_bz = ?,  lipreise_k.lief13_bz = ?,  "
"lipreise_k.lief14_bz = ?,  lipreise_k.kw = ?,  lipreise_k.jr = ? "

#line 32 "lipreise_k.rpp"
                                  "where nr = ? ";

            ins_quest ((char *) &lipreise_k.nr, 1, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &lipreise_k.nr, 1, 0);
            test_upd_cursor = prepare_sql ("select mdn from lipreise_k "
                                  "where nr = ? ");
            ins_quest ((char *) &lipreise_k.nr, 1, 0);
            del_cursor = prepare_sql ("delete from lipreise_k "
                                  "where nr = ? ");
    ins_quest ((char *) &lipreise_k.mdn,1,0);
    ins_quest ((char *) &lipreise_k.nr,1,0);
    ins_quest ((char *) lipreise_k.nr_bz,0,25);
    ins_quest ((char *) lipreise_k.lief1,0,17);
    ins_quest ((char *) lipreise_k.lief2,0,17);
    ins_quest ((char *) lipreise_k.lief3,0,17);
    ins_quest ((char *) lipreise_k.lief4,0,17);
    ins_quest ((char *) lipreise_k.lief5,0,17);
    ins_quest ((char *) lipreise_k.lief6,0,17);
    ins_quest ((char *) lipreise_k.lief7,0,17);
    ins_quest ((char *) lipreise_k.lief8,0,17);
    ins_quest ((char *) lipreise_k.lief9,0,17);
    ins_quest ((char *) lipreise_k.lief10,0,17);
    ins_quest ((char *) lipreise_k.lief11,0,17);
    ins_quest ((char *) lipreise_k.lief12,0,17);
    ins_quest ((char *) lipreise_k.lief13,0,17);
    ins_quest ((char *) lipreise_k.lief14,0,17);
    ins_quest ((char *) &lipreise_k.anzlief,1,0);
    ins_quest ((char *) &lipreise_k.basis_preis,3,0);
    ins_quest ((char *) &lipreise_k.dat_von,2,0);
    ins_quest ((char *) &lipreise_k.dat_bis,2,0);
    ins_quest ((char *) lipreise_k.lief1_bz,0,17);
    ins_quest ((char *) lipreise_k.lief2_bz,0,17);
    ins_quest ((char *) lipreise_k.lief3_bz,0,17);
    ins_quest ((char *) lipreise_k.lief4_bz,0,17);
    ins_quest ((char *) lipreise_k.lief5_bz,0,17);
    ins_quest ((char *) lipreise_k.lief6_bz,0,17);
    ins_quest ((char *) lipreise_k.lief7_bz,0,17);
    ins_quest ((char *) lipreise_k.lief8_bz,0,17);
    ins_quest ((char *) lipreise_k.lief9_bz,0,17);
    ins_quest ((char *) lipreise_k.lief10_bz,0,17);
    ins_quest ((char *) lipreise_k.lief11_bz,0,17);
    ins_quest ((char *) lipreise_k.lief12_bz,0,17);
    ins_quest ((char *) lipreise_k.lief13_bz,0,17);
    ins_quest ((char *) lipreise_k.lief14_bz,0,17);
    ins_quest ((char *) &lipreise_k.kw,1,0);
    ins_quest ((char *) &lipreise_k.jr,1,0);
            ins_cursor = prepare_sql ("insert into lipreise_k ("
"mdn,  nr,  nr_bz,  lief1,  lief2,  lief3,  lief4,  lief5,  lief6,  lief7,  lief8,  lief9,  "
"lief10,  lief11,  lief12,  lief13,  lief14,  anzlief,  basis_preis,  dat_von,  "
"dat_bis,  lief1_bz,  lief2_bz,  lief3_bz,  lief4_bz,  lief5_bz,  lief6_bz,  "
"lief7_bz,  lief8_bz,  lief9_bz,  lief10_bz,  lief11_bz,  lief12_bz,  lief13_bz,  "
"lief14_bz,  kw,  jr) "

#line 44 "lipreise_k.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"); 

#line 46 "lipreise_k.rpp"
}
