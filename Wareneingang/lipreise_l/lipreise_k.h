#ifndef _LIPREISE_K_DEF
#define _LIPREISE_K_DEF

#include "windows.h"
#include "mo_draw.h"
#include "wmask.h"
#include "dbclass.h"

struct LIPREISE_K {
   short     mdn;
   short     nr;
   char      nr_bz[25];
   char      lief1[17];
   char      lief2[17];
   char      lief3[17];
   char      lief4[17];
   char      lief5[17];
   char      lief6[17];
   char      lief7[17];
   char      lief8[17];
   char      lief9[17];
   char      lief10[17];
   char      lief11[17];
   char      lief12[17];
   char      lief13[17];
   char      lief14[17];
   short     anzlief;
   double    basis_preis;
   long      dat_von;
   long      dat_bis;
   char      lief1_bz[17];
   char      lief2_bz[17];
   char      lief3_bz[17];
   char      lief4_bz[17];
   char      lief5_bz[17];
   char      lief6_bz[17];
   char      lief7_bz[17];
   char      lief8_bz[17];
   char      lief9_bz[17];
   char      lief10_bz[17];
   char      lief11_bz[17];
   char      lief12_bz[17];
   char      lief13_bz[17];
   char      lief14_bz[17];
   short     kw;
   short     jr;
};
extern struct LIPREISE_K lipreise_k, lipreise_k_null;

#line 10 "lipreise_k.rh"

class LIPREISE_K_CLASS : public DB_CLASS 
{
       private :
               int cursor_best;
               void prepare (void);
       public :
               LIPREISE_K_CLASS () : DB_CLASS ()
               {
               }
               ~LIPREISE_K_CLASS ()
               {
                   dbclose ();
               } 
};
#endif
