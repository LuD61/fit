#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "wmask.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "lipreise_prot.h"

struct LIPREISE_PROT lipreise_prot, lipreise_prot_null;

void LIPREISE_PROT_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &lipreise_prot.mdn, 1, 0);
            ins_quest ((char *) &lipreise_prot.dat, 2, 0);
            ins_quest ((char *) &lipreise_prot.lief, 0, 17);
            ins_quest ((char *) &lipreise_prot.a, 3, 0);
            ins_quest ((char *) &lipreise_prot.best_blg, 2, 0);
            ins_quest ((char *) &lipreise_prot.lief_rech_nr, 0, 17);
            ins_quest ((char *) &lipreise_prot.jr, 1, 0);
            ins_quest ((char *) &lipreise_prot.kw, 1, 0);
    out_quest ((char *) &lipreise_prot.mdn,1,0);
    out_quest ((char *) &lipreise_prot.dat,2,0);
    out_quest ((char *) lipreise_prot.zeit,0,9);
    out_quest ((char *) lipreise_prot.lief,0,17);
    out_quest ((char *) &lipreise_prot.a,3,0);
    out_quest ((char *) &lipreise_prot.pr_ek,3,0);
    out_quest ((char *) &lipreise_prot.bzg_ek_alt,3,0);
    out_quest ((char *) &lipreise_prot.best_ek_alt,3,0);
    out_quest ((char *) &lipreise_prot.we_ek_alt,3,0);
    out_quest ((char *) &lipreise_prot.best_blg,2,0);
    out_quest ((char *) lipreise_prot.lief_rech_nr,0,17);
    out_quest ((char *) &lipreise_prot.jr,1,0);
    out_quest ((char *) &lipreise_prot.kw,1,0);
            cursor = prepare_sql ("select lipreise_prot.mdn,  "
"lipreise_prot.dat,  lipreise_prot.zeit,  lipreise_prot.lief,  "
"lipreise_prot.a,  lipreise_prot.pr_ek,  lipreise_prot.bzg_ek_alt,  "
"lipreise_prot.best_ek_alt,  lipreise_prot.we_ek_alt,  "
"lipreise_prot.best_blg,  lipreise_prot.lief_rech_nr,  "
"lipreise_prot.jr,  lipreise_prot.kw from lipreise_prot "

#line 32 "lipreise_prot.rpp"
                                  "where mdn = ? and dat = ? and lief = ? and a = ?  "
                                  "and best_blg = ? and lief_rech_nr = ? and jr = ? and kw = ? ");

            ins_quest ((char *) &lipreise_prot.mdn, 1, 0);
            ins_quest ((char *) &lipreise_prot.dat, 2, 0);
            ins_quest ((char *) &lipreise_prot.lief, 0, 17);
            ins_quest ((char *) &lipreise_prot.a, 3, 0);
            ins_quest ((char *) &lipreise_prot.best_blg, 2, 0);
            ins_quest ((char *) &lipreise_prot.lief_rech_nr, 0, 17);
            ins_quest ((char *) &lipreise_prot.jr, 1, 0);
            ins_quest ((char *) &lipreise_prot.kw, 1, 0);
    ins_quest ((char *) &lipreise_prot.mdn,1,0);
    ins_quest ((char *) &lipreise_prot.dat,2,0);
    ins_quest ((char *) lipreise_prot.zeit,0,9);
    ins_quest ((char *) lipreise_prot.lief,0,17);
    ins_quest ((char *) &lipreise_prot.a,3,0);
    ins_quest ((char *) &lipreise_prot.pr_ek,3,0);
    ins_quest ((char *) &lipreise_prot.bzg_ek_alt,3,0);
    ins_quest ((char *) &lipreise_prot.best_ek_alt,3,0);
    ins_quest ((char *) &lipreise_prot.we_ek_alt,3,0);
    ins_quest ((char *) &lipreise_prot.best_blg,2,0);
    ins_quest ((char *) lipreise_prot.lief_rech_nr,0,17);
    ins_quest ((char *) &lipreise_prot.jr,1,0);
    ins_quest ((char *) &lipreise_prot.kw,1,0);
            sqltext = "update lipreise_prot set "
"lipreise_prot.mdn = ?,  lipreise_prot.dat = ?,  "
"lipreise_prot.zeit = ?,  lipreise_prot.lief = ?,  "
"lipreise_prot.a = ?,  lipreise_prot.pr_ek = ?,  "
"lipreise_prot.bzg_ek_alt = ?,  lipreise_prot.best_ek_alt = ?,  "
"lipreise_prot.we_ek_alt = ?,  lipreise_prot.best_blg = ?,  "
"lipreise_prot.lief_rech_nr = ?,  lipreise_prot.jr = ?,  "
"lipreise_prot.kw = ? "

#line 44 "lipreise_prot.rpp"
                                  "where mdn = ? and dat = ? and lief = ? and a = ?  "
                                  "and best_blg = ? and lief_rech_nr = ? and jr = ? and kw = ? ";

            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &lipreise_prot.mdn, 1, 0);
            ins_quest ((char *) &lipreise_prot.dat, 2, 0);
            ins_quest ((char *) &lipreise_prot.lief, 0, 17);
            ins_quest ((char *) &lipreise_prot.a, 3, 0);
            ins_quest ((char *) &lipreise_prot.best_blg, 2, 0);
            ins_quest ((char *) &lipreise_prot.lief_rech_nr, 0, 17);
            ins_quest ((char *) &lipreise_prot.jr, 1, 0);
            ins_quest ((char *) &lipreise_prot.kw, 1, 0);
            test_upd_cursor = prepare_sql ("select mdn from lipreise_prot "
                                  "where mdn = ? and dat = ? and lief = ? and a = ?  "
                                  "and best_blg = ? and lief_rech_nr = ? and jr = ? and kw = ? ");
                                  
                                  
            ins_quest ((char *) &lipreise_prot.mdn, 1, 0);
            ins_quest ((char *) &lipreise_prot.dat, 2, 0);
            ins_quest ((char *) &lipreise_prot.lief, 0, 17);
            ins_quest ((char *) &lipreise_prot.a, 3, 0);
            ins_quest ((char *) &lipreise_prot.best_blg, 2, 0);
            ins_quest ((char *) &lipreise_prot.lief_rech_nr, 0, 17);
            ins_quest ((char *) &lipreise_prot.jr, 1, 0);
            ins_quest ((char *) &lipreise_prot.kw, 1, 0);
            del_cursor = prepare_sql ("delete from lipreise_prot "
                                  "where mdn = ? and dat = ? and lief = ? and a = ?  "
                                  "and best_blg = ? and lief_rech_nr = ? and jr = ? and kw = ? ");
    ins_quest ((char *) &lipreise_prot.mdn,1,0);
    ins_quest ((char *) &lipreise_prot.dat,2,0);
    ins_quest ((char *) lipreise_prot.zeit,0,9);
    ins_quest ((char *) lipreise_prot.lief,0,17);
    ins_quest ((char *) &lipreise_prot.a,3,0);
    ins_quest ((char *) &lipreise_prot.pr_ek,3,0);
    ins_quest ((char *) &lipreise_prot.bzg_ek_alt,3,0);
    ins_quest ((char *) &lipreise_prot.best_ek_alt,3,0);
    ins_quest ((char *) &lipreise_prot.we_ek_alt,3,0);
    ins_quest ((char *) &lipreise_prot.best_blg,2,0);
    ins_quest ((char *) lipreise_prot.lief_rech_nr,0,17);
    ins_quest ((char *) &lipreise_prot.jr,1,0);
    ins_quest ((char *) &lipreise_prot.kw,1,0);
            ins_cursor = prepare_sql ("insert into lipreise_prot ("
"mdn,  dat,  zeit,  lief,  a,  pr_ek,  bzg_ek_alt,  best_ek_alt,  we_ek_alt,  best_blg,  "
"lief_rech_nr,  jr,  kw) "

#line 74 "lipreise_prot.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?)"); 

#line 76 "lipreise_prot.rpp"
}
