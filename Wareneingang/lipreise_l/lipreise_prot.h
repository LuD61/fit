#ifndef _LIPREISE_PROT_DEF
#define _LIPREISE_PROT_DEF

#include "windows.h"
#include "mo_draw.h"
#include "wmask.h"
#include "dbclass.h"

struct LIPREISE_PROT {
   short     mdn;
   long      dat;
   char      zeit[9];
   char      lief[17];
   double    a;
   double    pr_ek;
   double    bzg_ek_alt;
   double    best_ek_alt;
   double    we_ek_alt;
   long      best_blg;
   char      lief_rech_nr[17];
   short     jr;
   short     kw;
};
extern struct LIPREISE_PROT lipreise_prot, lipreise_prot_null;

#line 10 "lipreise_prot.rh"

class LIPREISE_PROT_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               LIPREISE_PROT_CLASS () : DB_CLASS ()
               {
               }
               ~LIPREISE_PROT_CLASS ()
               {
                   dbclose ();
               } 
};
#endif
