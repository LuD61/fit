//240909  wenn keine alten Preise gefunden werden, dann die akt. holen 
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include <string.h>
#include "comcthlp.h"
#include "itemc.h"
#include "wmaskc.h"
#include "mo_meld.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_intp.h"
#include "listdlg.h"
#include "inflib.h"
#include "spezdlg.h"
#include "enterfunc.h"
#include "ListDlg.h"
#include "dlg.h"
#include "mo_wmess.h"
#include "gprintl.h"
#include "mo_gdruck.h"

#define IDM_CANCEL 3001
#ifndef IDM_WRITE
#define IDM_WRITE  3002
#endif

static int StdSize = STDSIZE;

static int dlgsize    = STDSIZE;
static int ltgraysize = STDSIZE;
static int dlgpossize = STDSIZE;
static int cpsize     = 200;
static int row;
static char *pline;
static unsigned char ESC = 27;
static unsigned char NORM   = 70;
static unsigned char FETT1  = 80;
static unsigned char FETT2  = 81;
static unsigned char ENG  = 0;
static unsigned int FETT = 0;
static char Lieferanten[200];

static mfont dlgfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         GRAYCOL,
                         0,
                         NULL};

static ColButton DefCol = {NULL,    -1, 0, 
                           NULL,    -1, 1,
                           NULL,  0,  0,
                           NULL,  0,  0,
                           NULL,  0,  0,
                           BLACKCOL,
                           LTGRAYCOL,
                           2,
                           NULL,
                           NULL,
                           TRUE,
};

static mfont printfont   = {"Arial", 80, 0, 0, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};

static mfont printfettfont   = {"Arial", 100, 0, 1, RGB (255, 0, 0),
                                         RGB (0, 255, 255),
                                         1,
                                         NULL};

char *LISTDLG::Rec;
int LISTDLG::RecLen;
CFELD **LISTDLG::Lsts;
char *LISTDLG::LstTab[0x10000];
Work *LISTDLG::work = NULL;
BOOL LISTDLG::NoRecNrOK = FALSE;
int Spaltenbreite = 15;

CFIELD *LISTDLG::_fChoise0 [] = {
    new CFIELD ("cancel", " abbrechen ", 10, 0, 0, 0, NULL, "",
                CBUTTON, 
                IDM_CANCEL, &dlgfont, 0, 0),
    new CFIELD ("write", " speichern ", 10, 0,12, 0, NULL, "",
                CBUTTON, 
                IDM_WRITE, &dlgfont, 0, 0),
    new CFIELD ("show",  "  ansehen  ", 10, 0,24, 0, NULL, "",
                CBUTTON, 
                IDM_CHOISE, &dlgfont, 0, 0),
    new CFIELD ("delete","  l�schen  ", 10, 0,36, 0, NULL, "",
                CBUTTON, 
                IDM_DELETE, &dlgfont, 0, 0),
    new CFIELD ("insert"," einf�gen  ", 10, 0,48, 0, NULL, "",
                CBUTTON, 
                IDM_INSERT, &dlgfont, 0, 0),
    NULL,
};

CFORM LISTDLG::fChoise0 (5, _fChoise0);

CFIELD *LISTDLG::_fChoise [] = {
    new CFIELD ("fChoise0", (char *) &fChoise0, 0, 0, -1 , -2, NULL, "",
                CFFORM, 
                500, &dlgfont, 0, 0),
    NULL,
};

CFORM LISTDLG::fChoise (1, _fChoise);


ITEM **LISTDLG::ufelder;
ITEM LISTDLG::ufiller         ("",               " ",               "",0);

ITEM LISTDLG::iline ("", "1", "", 0);

field *LISTDLG::_UbForm;


form LISTDLG::UbForm = {6, 0, 0, _UbForm, 0, 0, 0, 0, NULL};

field *LISTDLG::_LineForm;

form LISTDLG::LineForm = {4, 0, 0, _LineForm,0, 0, 0, 0, NULL};

field *LISTDLG::_DataForm;

form LISTDLG::DataForm = {5, 0, 0, _DataForm,0, 0, 0, 0, NULL};


int *LISTDLG::ubrows;


// struct CHATTR LISTDLG::ChAttra [] = {"feld1a",     DISPLAYONLY, EDIT,
struct CHATTR LISTDLG::ChAttra [] = {"a",     DISPLAYONLY, EDIT,
                                      NULL,  0,           0};

struct CHATTR *LISTDLG::ChAttr = ChAttra;

BOOL LISTDLG::ScrPrint = FALSE;
BOOL LISTDLG::LLPrint = TRUE;
short LISTDLG::mdn;
short LISTDLG::kw;
short LISTDLG::lipreise_nr = 0;
short LISTDLG::lipreise_posi = 0;
double LISTDLG::lipreise_art = 0;
SEARCHA LISTDLG::SearchA;


BOOL LISTDLG::CallInfo (HWND hWnd, RECT *rect, char *Item, char *ItemValue, DWORD WinMode)
/**
Einstieg in Infosystem mit Artikel. Abgeleitet von Klasse Info.
**/
{
 		 char buffer [1024];
         BOOL ret;

         sprintf (buffer, "where a = 2");
         ret = Info::CallInfo (hWnd, rect, "a", buffer, WinMode); 
         SetListFocus ();
         return ret;
}


int LISTDLG::InfoProc (char **Item, char *Value, char *where)
/**
Spezielle Procedure fuer Info-System. Alter Ablauf, ist nicht mehr Aktiv 
**/
{

   *Item = "a";

   sprintf (where, "where a = 2");
   return 1;
}

void LISTDLG::SetButtonSize (int Size)
{
    int fpos;

    return;
    fpos = GetItemPos (&DataForm, "choise");
    if (fpos < 0) return;

    if (Size == SMALL)
    {
         DataForm.mask [fpos].length = 2;
         DataForm.mask [fpos].rows = 1;
    }
    else 
    {
         DataForm.mask [fpos].length = 3;
         DataForm.mask [fpos].rows = 0;
    }
}


void LISTDLG::GetCfgValues (void)
{
       char cfg_v [512];
       COLORREF Color;
	   char clief [20];

       if (ProgCfg == NULL) return;
	   if (cfgOK) return;

       cfgOK = TRUE;
	   strcpy(Lieferanten,"0");
	   work->SetLieferanten(Lieferanten);
//	   strcpy(clief,"LieferantenTyp");
//	   sprintf(clief,"%s%d",clipped(clief),hwg);

       if (ProgCfg->GetCfgValue ("Mandant_fest", cfg_v) == TRUE)
       {
                    work->Mandant_fest =  atoi (cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("order_by_posi", cfg_v) == TRUE)
       {
                    work->order_by_posi =  atoi (cfg_v);
	   }
       if (ProgCfg->GetCfgValue (clief, cfg_v) == TRUE)
       {
					strncpy(Lieferanten,cfg_v,sizeof(Lieferanten));
			  	    work->SetLieferantenTyp(Lieferanten);
	   }
       if (ProgCfg->GetCfgValue ("Lieferanten", cfg_v) == TRUE)
       {
					strncpy(Lieferanten,cfg_v,sizeof(Lieferanten));
			  	    work->SetLieferanten(Lieferanten);
	   }
       if (ProgCfg->GetCfgValue ("3DList", cfg_v) == TRUE)
       {
                    eListe.Set3D (atoi (cfg_v));
	   }
       if (ProgCfg->GetCfgValue ("3DSize", cfg_v) == TRUE)
       {
                    eListe.SetPlus3D (atoi (cfg_v));
	   }
       if (ProgCfg->GetCfgValue ("ListFocus", cfg_v) == TRUE)
       {
                    ListFocus = atoi (cfg_v);
	   }
        if (ProgCfg->GetCfgValue ("rowheight", cfg_v) == TRUE)
		{
			        RowHeight = ratod (cfg_v);
					if (RowHeight < (double) 1.0)
					{
						RowHeight = (double) 1.0;
					}
		}
        if (ProgCfg->GetCfgValue ("ubheight", cfg_v) == TRUE)
		{
			        UbHeight = atoi (cfg_v);
					SetUbHeight ();
		}
		eListe.SetRowHeight (RowHeight);

/***
        if (ProgCfg->GetCfgValue ("NoRecNr", cfg_v) == TRUE)
        {
			        NoRecNr = atoi (cfg_v);
		            if (atoi (cfg_v))
					{
	                        SetNoRecNr ();
					}
		}
		**/
        if (ProgCfg->GetCfgValue ("ListColor", cfg_v) == TRUE)
        {
                     GetListColor (&Color, cfg_v);
		             eListe.SetColor (Color);
        }
        if (ProgCfg->GetCfgValue ("ListBkColor", cfg_v) == TRUE)
        {
                     GetListColor (&Color, cfg_v);
		             eListe.SetBkColor (Color);
        }
        if (ProgCfg->GetCfgValue ("ListLine", cfg_v) == TRUE)
        {
		             eListe.SetLines (GetListLine (cfg_v));
        }
        if (ProgCfg->GetCfgValue ("PrintBackground", cfg_v) == TRUE)
        {
		             PrintBackground = atoi (cfg_v);
        }
}


void LISTDLG::TestRemoves (void)
{
        int fpos;

        fpos = GetItemPos (&DataForm, "feld3");
        if (fpos >= 0)
        {
            if (DataForm.mask[fpos].attribut & REMOVED)
            {
                DestroyListField ("feld3");
            }
        }
}



void LISTDLG::GenLiefForms (short lipreise_nr)
{
        char iname [80];

		Spaltenbreite = 15;
 	    DefCol.BkColor = GetSysColor (COLOR_3DFACE);
        eListe.SetNoScrollBkColor (DefCol.BkColor);
        UbForm.fieldanz = (short) work->GetLiefAnz (lipreise_nr) + 2;
		if (lipreise_k.anzlief > 10) Spaltenbreite = 13;
		if (lipreise_k.anzlief > 11) Spaltenbreite = 12;
		if (lipreise_k.anzlief == 13)
		{
			Spaltenbreite = 11;
	        UbForm.fieldanz = 15;
		}
		if (lipreise_k.anzlief == 14)
		{
			Spaltenbreite = 11;
	        UbForm.fieldanz = 16;
		}

        _UbForm = new field [UbForm.fieldanz];
        DLG::ExitError ((_UbForm == NULL), "Fehler bei der Speicherzuordnung");
        _LineForm = new field [UbForm.fieldanz];
        DLG::ExitError ((_LineForm == NULL), "Fehler bei der Speicherzuordnung");

        UbForm.mask = _UbForm;
        UbForm.frmstart  = 0;
        UbForm.frmscroll = 0;
        UbForm.caption   = NULL;
        UbForm.before    = NULL;
        UbForm.after     = NULL;
        UbForm.cbfield   = NULL;
        UbForm.font      = NULL;

        memcpy (&LineForm, &UbForm, sizeof (form));
        LineForm.mask    = _LineForm;

        int i = 0;
        int pos = 0;

        sprintf (iname, "a");
        char *ibez = new char [80]; 
        DLG::ExitError ((ibez == NULL), "Fehler bei der Speicherzuordnung");
        ColButton *Cub = new ColButton;
        DLG::ExitError ((Cub == NULL), "Fehler bei der Speicherzuordnung");
        memcpy (Cub, &DefCol, sizeof (ColButton));
        sprintf (ibez, "Artikel");
        Cub->text1 = ibez;
        Cub->ty1 = -1;

        _UbForm[i].item = new ITEM (iname, (char *) Cub, "", NULL);
        _UbForm[i].length    = 26;
        _UbForm[i].rows      = 0;
        _UbForm[i].pos[0]    = 0;
        _UbForm[i].pos[1]    = pos;
        _UbForm[i].feldid    = NULL;
        _UbForm[i].picture   = "";
        _UbForm[i].attribut  = COLBUTTON;
        _UbForm[i].before    = NULL;
        _UbForm[i].after     = NULL;
        _UbForm[i].BuId      = 0;

        pos += _UbForm[i].length;
        memcpy (&_LineForm[i], &_UbForm[0], sizeof (field));
        _LineForm[i].item = &iline;
        _LineForm[i].length    = 1;
        _LineForm[i].pos[1]    = pos;
        _LineForm[i].attribut  = DISPLAYONLY;
        i ++;

//        int dsqlstatus = work->LiefFirst (lipreise_nr);
		int z = 1;
        int dsqlstatus = work->HoleLief (lipreise_nr,z);
        while (dsqlstatus == 0 && z <= 14)
        {
            if (i >= UbForm.fieldanz - 1) break;
            sprintf (iname, "lief%s", clipped (lief_bzg.lief));

            ibez = new char [80]; 
            DLG::ExitError ((ibez == NULL), "Fehler bei der Speicherzuordnung");
            sprintf (ibez, "%s", lief_bzg.lief);
            ColButton *Cub = new ColButton;
            DLG::ExitError ((Cub == NULL), "Fehler bei der Speicherzuordnung");
            memcpy (Cub, &DefCol, sizeof (ColButton));
            Cub->text1 = ibez;
            Cub->text2 = new char [25];
            DLG::ExitError ((Cub->text2 == NULL), "Fehler bei der Speicherzuordnung");
            strcpy (Cub->text2, _adr.adr_krz);

            memcpy (&_UbForm[i], &_UbForm[0], sizeof (field));
            _UbForm[i].item = new ITEM (iname, (char *) Cub, "", NULL);
            _UbForm[i].length    = Spaltenbreite;
            _UbForm[i].pos[1]    = pos;
            pos += _UbForm[i].length;

            memcpy (&_LineForm[i], &_UbForm[0], sizeof (field));
            _LineForm[i].item = &iline;
            _LineForm[i].length    = 1;
            _LineForm[i].pos[1]    = pos;
            _LineForm[i].attribut  = DISPLAYONLY;
			 z++;
            i ++; 
			 if (z > 14) break;
	        dsqlstatus = work->HoleLief (lipreise_nr,z);
        }

        _UbForm[i].item = &ufiller;
        _UbForm[i].length    = 100;
        _UbForm[i].rows      = 0;
        _UbForm[i].pos[0]    = 0;
        _UbForm[i].pos[1]    = pos;
        _UbForm[i].feldid    = NULL;
        _UbForm[i].picture   = "";
        _UbForm[i].attribut  = BUTTON;
        _UbForm[i].before    = NULL;
        _UbForm[i].after     = NULL;
        _UbForm[i].BuId      = 0;

        DataForm.fieldanz = UbForm.fieldanz;
        _DataForm = new field [UbForm.fieldanz + 1];
        DLG::ExitError ((_DataForm == NULL), "Fehler bei der Speicherzuordnung");
        DataForm.mask = _DataForm;
        Lsts = new CFELD * [DataForm.fieldanz + 1];
        DLG::ExitError ((Lsts == NULL), "Fehler bei der Speicherzuordnung");

        RecLen = 19 + 13 * DataForm.fieldanz + 19;
        Rec = new char [RecLen];
        DLG::ExitError ((Rec == NULL), "Fehler bei der Speicherzuordnung");
        memset (Rec, ' ', RecLen);

        char *Rpos = Rec;

        pos = 7;
        i = 0;

        strcpy (iname, "a");
        Lsts [i] = new CFELD (iname, Rpos, 19);
        DLG::ExitError ((Lsts[i] == NULL), "Fehler bei der Speicherzuordnung");
        Lsts[i]->Init ();
        memcpy (&_DataForm[i], &_UbForm[0], sizeof (field));

        _DataForm[i].item = new ITEM (iname, Lsts[i]->GetFeld (), "", NULL);
        _DataForm[i].length    = 18;
        _DataForm[i].pos[1]    = pos + 1;
        _DataForm[i].picture   = "%13.0f";
        _DataForm[i].attribut    = DISPLAYONLY;
        pos  += Spaltenbreite;
        Rpos += _DataForm[i].length;

        for (i = 1; i < DataForm.fieldanz - 1; i ++)
        {
                 sprintf (iname, "pr%d", i);

                 Lsts [i] = new CFELD (iname, Rpos, 13);
                 DLG::ExitError ((Lsts[i] == NULL), "Fehler bei der Speicherzuordnung");
                 Lsts[i]->Init ();
                 memcpy (&_DataForm[i], &_UbForm[0], sizeof (field));

                 _DataForm[i].item = new ITEM (iname, Lsts[i]->GetFeld (), "", NULL);
                 _DataForm[i].length    = 12;
                 _DataForm[i].pos[1]    = pos + 7;
                 if (work->Getflg_preise_prot() == 0) _DataForm[i].picture   = "%6.2f";    // nur bei akt. Preisen
                 _DataForm[i].attribut  = EDIT;
                 pos += Spaltenbreite;
                 Rpos += 13;
        }
        strcpy (iname, "a_bz1");
        Lsts [i] = new CFELD (iname, Rpos, 19);
        DLG::ExitError ((Lsts[i] == NULL), "Fehler bei der Speicherzuordnung");
        Lsts[i]->Init ();
        memcpy (&_DataForm[i], &_UbForm[0], sizeof (field));
        Lsts[i + 1] = NULL;

        _DataForm[i].item = new ITEM (iname, Lsts[i]->GetFeld (), "", NULL);
        _DataForm[i].length      = 24;
        _DataForm[i].pos[0]      = 1;
        _DataForm[i].pos[1]      = 1;
        _DataForm[i].attribut    = DISPLAYONLY;

        ubrows = new int [DataForm.fieldanz];
        DLG::ExitError ((ubrows == NULL), "Fehler bei der Speicherzuordnung");
        for (i = 0; i < DataForm.fieldanz - 1; i ++)
        {
            ubrows[i] = i;
        }
        ubrows[i] = 0;
}
     

void LISTDLG::GenForms (short lipreise_nr)
{
        char iname [20];

 	    DefCol.BkColor = GetSysColor (COLOR_3DFACE);
        eListe.SetNoScrollBkColor (DefCol.BkColor);
        UbForm.fieldanz = (short) work->GetZeileAnz (lipreise_nr) + 2;
        _UbForm = new field [UbForm.fieldanz];
        DLG::ExitError ((_UbForm == NULL), "Fehler bei der Speicherzuordnung");
        _LineForm = new field [UbForm.fieldanz];
        DLG::ExitError ((_LineForm == NULL), "Fehler bei der Speicherzuordnung");

        UbForm.mask = _UbForm;
        UbForm.frmstart  = 0;
        UbForm.frmscroll = 0;
        UbForm.caption   = NULL;
        UbForm.before    = NULL;
        UbForm.after     = NULL;
        UbForm.cbfield   = NULL;
        UbForm.font      = NULL;

        memcpy (&LineForm, &UbForm, sizeof (form));
        LineForm.mask    = _LineForm;

        int i = 0;
        int pos = 0;

        sprintf (iname, "lief");
        char *ibez = new char [80]; 
        DLG::ExitError ((ibez == NULL), "Fehler bei der Speicherzuordnung");
        ColButton *Cub = new ColButton;
        DLG::ExitError ((Cub == NULL), "Fehler bei der Speicherzuordnung");
        memcpy (Cub, &DefCol, sizeof (ColButton));
        sprintf (ibez, "Lieferant");
        Cub->text1 = ibez;
        Cub->ty1 = -1;

        _UbForm[i].item = new ITEM (iname, (char *) Cub, "", NULL);
        _UbForm[i].length    = 26;
        _UbForm[i].rows      = 0;
        _UbForm[i].pos[0]    = 0;
        _UbForm[i].pos[1]    = pos;
        _UbForm[i].feldid    = NULL;
        _UbForm[i].picture   = "";
        _UbForm[i].attribut  = COLBUTTON;
        _UbForm[i].before    = NULL;
        _UbForm[i].after     = NULL;
        _UbForm[i].BuId      = 0;

        pos += _UbForm[i].length;
        memcpy (&_LineForm[i], &_UbForm[0], sizeof (field));
        _LineForm[i].item = &iline;
        _LineForm[i].length    = 1;
        _LineForm[i].pos[1]    = pos;
        _LineForm[i].attribut  = DISPLAYONLY;
        i ++;

        int dsqlstatus = work->ZeileFirst (lipreise_nr);
        while (dsqlstatus == 0)
        {
            if (i >= UbForm.fieldanz - 1) break;
            sprintf (iname, "a%.0lf", _a_bas.a);

            ibez = new char [80]; 
            DLG::ExitError ((ibez == NULL), "Fehler bei der Speicherzuordnung");
            sprintf (ibez, "%.0lf", _a_bas.a);
            ColButton *Cub = new ColButton;
            DLG::ExitError ((Cub == NULL), "Fehler bei der Speicherzuordnung");
            memcpy (Cub, &DefCol, sizeof (ColButton));
            Cub->text1 = ibez;
            Cub->text2 = new char [25];
            DLG::ExitError ((Cub->text2 == NULL), "Fehler bei der Speicherzuordnung");
            strcpy (Cub->text2, _a_bas.a_bz1);

            memcpy (&_UbForm[i], &_UbForm[0], sizeof (field));
            _UbForm[i].item = new ITEM (iname, (char *) Cub, "", NULL);
            _UbForm[i].length    = 16;
            _UbForm[i].pos[1]    = pos;
            pos += _UbForm[i].length;

            memcpy (&_LineForm[i], &_UbForm[0], sizeof (field));
            _LineForm[i].item = &iline;
            _LineForm[i].length    = 1;
            _LineForm[i].pos[1]    = pos;
            _LineForm[i].attribut  = DISPLAYONLY;
            dsqlstatus = work->ZeileNext ();
            i ++;
        }

        _UbForm[i].item = &ufiller;
        _UbForm[i].length    = 100;
        _UbForm[i].rows      = 0;
        _UbForm[i].pos[0]    = 0;
        _UbForm[i].pos[1]    = pos;
        _UbForm[i].feldid    = NULL;
        _UbForm[i].picture   = "";
        _UbForm[i].attribut  = BUTTON;
        _UbForm[i].before    = NULL;
        _UbForm[i].after     = NULL;
        _UbForm[i].BuId      = 0;

        DataForm.fieldanz = UbForm.fieldanz;
        _DataForm = new field [UbForm.fieldanz + 1];
        DLG::ExitError ((_DataForm == NULL), "Fehler bei der Speicherzuordnung");
        DataForm.mask = _DataForm;
        Lsts = new CFELD * [DataForm.fieldanz + 1];
        DLG::ExitError ((Lsts == NULL), "Fehler bei der Speicherzuordnung");

        RecLen = 18 + 13 * DataForm.fieldanz + 18;
        Rec = new char [RecLen];
        DLG::ExitError ((Rec == NULL), "Fehler bei der Speicherzuordnung");
        memset (Rec, ' ', RecLen);

        char *Rpos = Rec;

        pos = 7;
        i = 0;

        strcpy (iname, "lief");
        Lsts [i] = new CFELD (iname, Rpos, 18);
        DLG::ExitError ((Lsts[i] == NULL), "Fehler bei der Speicherzuordnung");
        Lsts[i]->Init ();
        memcpy (&_DataForm[i], &_UbForm[0], sizeof (field));

        _DataForm[i].item = new ITEM (iname, Lsts[i]->GetFeld (), "", NULL);
        _DataForm[i].length    = 17;
        _DataForm[i].pos[1]    = pos + 1;
        _DataForm[i].attribut    = DISPLAYONLY;
        pos  += 20;
        Rpos += _DataForm[i].length;

        for (i = 1; i < DataForm.fieldanz - 1; i ++)
        {
                 sprintf (iname, "pr%d", i);

                 Lsts [i] = new CFELD (iname, Rpos, 13);
                 DLG::ExitError ((Lsts[i] == NULL), "Fehler bei der Speicherzuordnung");
                 Lsts[i]->Init ();
                 memcpy (&_DataForm[i], &_UbForm[0], sizeof (field));

                 _DataForm[i].item = new ITEM (iname, Lsts[i]->GetFeld (), "", NULL);
                 _DataForm[i].length    = 12;
                 _DataForm[i].pos[1]    = pos + 1;
                 _DataForm[i].picture   = "%10.4f";
                 _DataForm[i].attribut  = EDIT;
                 pos += 16;
                 Rpos += 13;
        }
        strcpy (iname, "adr_krz");
        Lsts [i] = new CFELD (iname, Rpos, 18);
        DLG::ExitError ((Lsts[i] == NULL), "Fehler bei der Speicherzuordnung");
        Lsts[i]->Init ();
        memcpy (&_DataForm[i], &_UbForm[0], sizeof (field));
        Lsts[i + 1] = NULL;

        _DataForm[i].item = new ITEM (iname, Lsts[i]->GetFeld (), "", NULL);
        _DataForm[i].length      = 17;
        _DataForm[i].pos[0]      = 1;
        _DataForm[i].pos[1]      = 7;
        _DataForm[i].attribut    = DISPLAYONLY;

        ubrows = new int [DataForm.fieldanz];
        DLG::ExitError ((ubrows == NULL), "Fehler bei der Speicherzuordnung");
        for (i = 0; i < DataForm.fieldanz - 1; i ++)
        {
            ubrows[i] = i;
        }
        ubrows[i] = 0;
}
     

void LISTDLG::DeleteForms (void)
{
        int i;

        if (Lsts != NULL)
        {
            for (i = 0; i < DataForm.fieldanz; i ++)
            {
                if (Lsts[i] != NULL)
                {
//                    delete Lsts[i]->GetFeldname ();
                    delete Lsts[i];
                }
            }
            delete Lsts;
            Lsts = NULL;
        }

        if (_UbForm != NULL)
        {
          for (i = 0; i < UbForm.fieldanz; i++)
          {
            if (UbForm.mask[i].item != NULL &&
                UbForm.mask[i].item != &ufiller)
            {
                if (UbForm.mask[i].attribut & COLBUTTON)
                {
                      ColButton *Cub = (ColButton *) UbForm.mask[i].item->GetFeldPtr ();
                      if (Cub != NULL)
                      {
                          if (Cub->text1 != NULL) delete Cub->text1;
                          if (Cub->text2 != NULL) delete Cub->text2;
                      }
                      delete Cub;
                }
                else if (UbForm.mask[i].item->GetFeldPtr () != NULL)
                {
                      delete UbForm.mask[i].item->GetFeldPtr ();
                }
                delete UbForm.mask[i].item;
            }
          }
          delete _UbForm; 
          _UbForm = NULL;
        }
        UbForm.fieldanz = 0; 

        if (_LineForm != NULL)
        {
          delete _LineForm; 
        }
        LineForm.fieldanz = 0;

        if (_DataForm != NULL)
        {
          for (i = 0; i < DataForm.fieldanz; i++)
          {
            if (DataForm.mask[i].item != NULL)
            {
                delete DataForm.mask[i].item;
            }
          }
          delete _DataForm;
          _DataForm = NULL;
        }

        DataForm.fieldanz = 0; 

        if (Rec != NULL)
        {
            delete Rec;
            Rec = NULL;
        }

        if (ubrows != NULL)
        {
             delete ubrows;
        }
}
     

LISTDLG::LISTDLG (short mdn, short lipreise_nr, short k) : KEYS (), LISTENTER () 
{

    if (work == NULL) return;
    ProgCfg = new PROG_CFG ("lipreise_l");
	Dlg = NULL;
    cfgOK = FALSE;
	cursor = -1;
    lief_bzg.mdn = mdn;
    LISTENTER::GetCfgValues ();
    GetCfgValues ();
    if (work->GetMatrix () == ARTIKEL)
    {
            GenForms (lipreise_nr);
    }
    else if (work->GetMatrix () == LIEFERANT)
    {
            GenLiefForms (lipreise_nr);
    }
    else
    {
            GenLiefForms (lipreise_nr);
    }


//    LstTab = new char * [0x10000];
//    for (int i = 0; i < 0x10000; i ++)

    dataform  = &DataForm;
    ubform    = &UbForm;
    lineform  = &LineForm;
    eListe.SetUbRows (ubrows); 
	eListe.SetKeys (this);

    eListe.SetHscrollStart (1);
    UbForm.ScrollStart = 1;

//    GetSysPar ();
    UbHeight = 3;
	SetUbHeight ();

    TestRemoves ();

//    SetNoRecNr ();
	fChoise.Setchpos (0, 2);
    ChAttr = ChAttra;
    Toolbar2 = NULL;
    eListe.SetInfoProc (InfoProc);
    eListe.SetInfo (this);
    ArrDown = NULL;
    ArrDown = LoadBitmap (DLG::hInstance, "ARRDOWN");
    eListe.SetTestAppend (TestAppend);
//    eListe.SetNoMove (TRUE);
    pagelen = 66;
    pagelen0 = 59;
    ublen = 4;
    PrintFont = 0;
    RowPrintFont = 0;
    BOOL UsePrintForm = FALSE;
    BOOL QuerPrint = FALSE;
    DrkDeviceFile = NULL;
    PrintBackground = FALSE;
    kw = k;
    eListe.SetChAttr (ChAttr); 
}


LISTDLG::~LISTDLG ()
{
    int i, anz;
	if (work != NULL)
	{
          work->SetListDlg (NULL);
	}
    fChoise.destroy ();
	if (LstTab != NULL)
	{

        anz =   eListe.GetRecanz ()	;
        for (i = 0; i < anz; i ++)
        {
/*
            if (LstTab[i].choise[0] != NULL)
            {
                delete LstTab[i].choise[0];
            }
*/
        }

//  	    delete LstTab;
//		LstTab = NULL;
	}
    if (ProgCfg != NULL)
    {
        ProgCfg->CloseCfg ();
        delete ProgCfg;
        ProgCfg = NULL;
    }
    DeleteForms ();
    NoRecNrOK = FALSE;
}


int LISTDLG::TestAppend (void)
{
    switch (syskey)
    {
         case KEY6 :
           if (lipreise_nr == 0)
		   {
                return 1;
		   }
		   return 1;
         case KEY7 :
           if (lipreise_nr == 0)
		   {
                return 1;
		   }
		   return 1;
         case KEY8 :
           if (lipreise_nr == 0)
		   {
                return 1;
		   }
		   return 0;
    }
    return 1;
}

// Funktionen f�r den Zugriff auf die Daten. Sind als abstrakte Schnittstellen in
// listenter definiert.

unsigned char *LISTDLG::GetDataRec (void)
{
    return (unsigned char *) Rec;
}

int LISTDLG::GetDataRecLen (void)
{
    return RecLen;
}

char *LISTDLG::GetListTab (int i)
{
    LstTab[i] = new char [RecLen];
    DLG::ExitError ((LstTab[i] == NULL), "Fehler bei der Speicherzuordnung");
    memset (LstTab[i], ' ' , RecLen);
    return (char *) LstTab[i];
}

// Ende der Schnittstellen.


void LISTDLG::Setfeld2Removed (BOOL removed)
{
    if (removed)
    {
            SetFieldAttr ("feld2", REMOVED);
    }
    else
    {
            SetFieldAttr ("feld2", EDIT);
    }
}


void LISTDLG::Prepare (void)
{
     cursor = work->Prepare ();
}

void LISTDLG::Open (void)
{
    work->Open ();
}

double LISTDLG::GetA (field *feld)
{
    if ((feld->attribut & COLBUTTON) == 0)
    {
        return ratod (feld->item->GetFeldPtr ());
    }

    ColButton *Cub = (ColButton *) feld->item->GetFeldPtr ();
    if (Cub != NULL)
    {
        return ratod (Cub->text1);
    }
    return 0.0;
}


char * LISTDLG::GetLief (field *feld)
{
    if ((feld->attribut & COLBUTTON) == 0)
    {
        return clipped (feld->item->GetFeldPtr ());
    }

    ColButton *Cub = (ColButton *) feld->item->GetFeldPtr ();
    if (Cub != NULL)
    {
        return clipped (Cub->text1);
    }
    return NULL;
}

int LISTDLG::Fetch (void)
{
    work->SetLsts (Lsts);
    int dsqlstatus =work->Fetch ();
    if (dsqlstatus == 100)
    {
          return dsqlstatus;
    }
    if (work->GetMatrix () == LIEFERANT)
    {
          return FetchLief ();
    }

    clipped (lief_bzg.lief);
    strcpy (Lsts[0]->GetFeld (), lief_bzg.lief);
    int fpos = CFELD::GetFeldpos (Lsts, "adr_krz");
    if (fpos != -1)
    {
           strcpy (Lsts[fpos]->GetFeld (), _adr.adr_krz);
    }
    for (int i = 1; i < UbForm.fieldanz - 1; i ++)
    {
           _a_bas.a = GetA (&UbForm.mask[i]);
           if (_a_bas.a > 0)
           {
               work->FetchLiefBzg ();
               sprintf (Lsts[i]->GetFeld (), "%.4lf", lief_bzg.pr_ek_eur);
           }
    }
    return dsqlstatus;
}


int LISTDLG::FetchLief (void)
{

    sprintf (Lsts[0]->GetFeld (), "%.0lf", _a_bas.a);
    clipped (Lsts[0]->GetFeld ());
    int fpos = CFELD::GetFeldpos (Lsts, "a_bz1");
    if (fpos != -1)
    {
           strcpy (Lsts[fpos]->GetFeld (), clipped (_a_bas.a_bz1));
    }
	lief_bzg.a = _a_bas.a;
	//090609 Wenn Schon Preise erstellt worden sind, dann die nehmen 
    for (int i = 1; i < UbForm.fieldanz - 1; i ++)
    {
           char * lief = GetLief (&UbForm.mask[i]);
           if (lief != NULL)
           {
               strcpy (lief_bzg.lief, lief);
			   if (work->Getflg_preise_prot() == 0)
			   {
					work->FetchLiefBzg ();
	  			   sprintf (Lsts[i]->GetFeld (), "%.2lf", lief_bzg.pr_ek_eur);
			   }
			   else
			   {
					lipreise_prot.kw = 0;
					work->FetchLipreise_Prot ();
					if (lief_bzg.pr_ek_eur == 0.0)
					{
						work->FetchAllLipreise_Prot ();
						if (lief_bzg.pr_ek_eur == 0.0)
						{
							work->FetchLiefBzg ();   //240909  wenn keine alten Preise gefunden werden, dann die akt. holen
//220909						sprintf (Lsts[i]->GetFeld (),"%s", "");
						}
					}
					else
					{
						if (lipreise_prot.kw != lipreise_k.kw)
						{
//220909		  					sprintf (Lsts[i]->GetFeld (), "%.2lf (KW%hd)", lief_bzg.pr_ek_eur,lipreise_prot.kw);
						}
						else
						{
//220909		  					sprintf (Lsts[i]->GetFeld (), "\"%.2lf\"", lief_bzg.pr_ek_eur);
						}
					}
				   if (work->Getflg_prot_details() == 0 || lipreise_prot.kw == 0)
				   {
	  		  		   sprintf (Lsts[i]->GetFeld (), "%.2lf", lief_bzg.pr_ek_eur); //220909
				   }
				   else
				   {
	  					sprintf (Lsts[i]->GetFeld (), "%.2lf (KW%hd)", lief_bzg.pr_ek_eur,lipreise_prot.kw);
				   }
			   }

           }
    }
    return 0;
}


void LISTDLG::Close (void)
{
    work->Close ();
	if (cursor != -1)
	{
		cursor = -1;
	}
}


BOOL LISTDLG::AfterCol (char *colname)
{
	      char a_bz1[25];
/*
         if (strcmp (colname, "feld1") == 0)
         {
             return TRUE;
         }
*/
		  if (strcmp (colname, "a") == 0)
         {

 	         int fpos = GetItemPos (&DataForm, "a");
 	         _a_bas.a = ratod (DataForm.mask[fpos].item->GetFeldPtr ()); 
			 if (work->GetABz1 (_a_bas.a, a_bz1) == FALSE)
			 {
				 print_mess (2, "Artikel %.0lf nicht gefunden", _a_bas.a);
				 return FALSE;
			 }
			 fpos = GetItemPos (&DataForm, "a");
			 strcpy (DataForm.mask[fpos].item->GetFeldPtr (), a_bz1);
			 FetchLief ();
             memcpy (LstTab [eListe.GetAktRow ()], Rec, RecLen);
			 InvalidateRect (eListe.Getmamain3 (), NULL, TRUE);
             return FALSE;
         }
         return FALSE;
}

int LISTDLG::AfterRow (int Row)
{
//         form * DataForm; 

/*
         if (syskey == KEYUP && strcmp (clipped (Lsts.feld1), " ") <= 0)
         {
                return OnKeyDel ();
         }
*/
         memcpy (LstTab [Row], Rec, RecLen);
         work->SetLsts (Lsts);
         return 1;
}

int LISTDLG::BeforeRow (int Row)
{
         memcpy (LstTab [Row], Rec, RecLen);
         return 1;
}


int LISTDLG::ShowA ()
{

        struct SA *sa;

        int fpos = GetItemPos (&DataForm, "a");
		if (fpos != eListe.GetAktColumn ())
		{
			return 0;
		}
//        if (Modal)
        {  
               SearchA.SetWindowStyle (WS_POPUP | 
                                       WS_THICKFRAME |
                                       WS_CAPTION |
                                       WS_SYSMENU |
                                       WS_MINIMIZEBOX |
                                       WS_MAXIMIZEBOX);
               SearchA.SetParams (DLG::hInstance, hMainWindow);
        }
/*
        else
        {  
               SearchA.SetWindowStyle (WS_POPUP | WS_THICKFRAME);
               SearchA.SetParams (DLG::hInstance, hMainWindow);
        }
*/
        SearchA.Setawin (eListe.GethMainWindow ());
        SearchA.SearchA ();
//        if (Modal)
        {  
//               EnableWindows (GetParent (eListe.GethMainWindow ()), FALSE);
        }
        if (syskey == KEY5)
        {
            SetListFocus ();
            return 0;
        }
        sa = SearchA.GetSa ();
        if (sa == NULL)
        {
            return 0;
        }


 	    strcpy (DataForm.mask[fpos].item->GetFeldPtr (), sa->a); 
//        eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
        memcpy (LstTab [eListe.GetAktRow ()], Rec, RecLen);
		PostMessage (eListe.Getmamain3 (), WM_KEYDOWN, VK_RETURN, 0l);

//		AfterCol ("a");

        SetListFocus ();
        return 0;
}


BOOL LISTDLG::OnKey5 (void)
{
    if (abfragejn (hMainWindow, "Positionen speichern ?", "J"))
    {
        return OnKey12 ();
    }
	eListe.BreakList ();
    if (work != NULL)
    {
        work->RollbackWork ();
    }
	return TRUE;
}

BOOL LISTDLG::OnKey6 (void)
{
    eListe.InsertLine ();
	OnKey9();
    return TRUE;
}

BOOL LISTDLG::OnKey8 (void)
{
    eListe.AppendLine ();
    return TRUE;
}

BOOL LISTDLG::OnKeyInsert (void)
{
    eListe.AppendLine ();
    return TRUE;
}

BOOL LISTDLG::OnKey7 (void)
{
    memcpy (ausgabesatz, SwSaetze[eListe.GetAktRow()], zlen);
	lipreise_p.mdn = lipreise_k.mdn;
	lipreise_p.nr = lipreise_k.nr;
	lipreise_p.a = ratod((char*)ausgabesatz);
	work->Deletelipreise_p();
    eListe.DeleteLine ();
    if (lipreise_nr == 0)
    {
		InvalidateRect (eListe.Getmamain3 (), NULL, TRUE);
	}
    return TRUE;
}

BOOL LISTDLG::OnKeyDel (void)
{
   memcpy (ausgabesatz, SwSaetze[eListe.GetAktRow()], zlen);
	lipreise_p.mdn = lipreise_k.mdn;
	lipreise_p.nr = lipreise_k.nr;
	lipreise_p.a = ratod((char*)ausgabesatz);
	work->Deletelipreise_p();
    eListe.DeleteLine ();
    if (lipreise_nr == 0)
    {
		InvalidateRect (eListe.Getmamain3 (), NULL, TRUE);
	}
 


 //   eListe.DeleteLine ();
    return TRUE;
}


BOOL LISTDLG::OnKey9 (void)
{
    char * Item = eListe.GetAktItem ();
    if (strcmp (Item, "a") == 0)
    {
        ShowA ();
        return TRUE;
    }
    return FALSE;
}


BOOL LISTDLG::OnKey10 (void)
{
    if (Dlg != NULL)
    {
         work->SetLsts (Lsts);
// Hier Workfunktion einsetzen
         SetListFocus ();
         return TRUE;
	}
    return FALSE;
}

BOOL LISTDLG::OnKey11 (void)
{

    return FALSE;
}


int LISTDLG::ToMemory (int i)
{

    LISTENTER::ToMemory (i);
    return 0;
}


void LISTDLG::InitRow (void)
{

    LISTENTER::InitRow ();


    work->SetLsts (Lsts);

//    if (ArrDown != NULL && Lsts.choise[0] != NULL)
    {
//        Lsts.choise[0]->SetBitMap (ArrDown);
    }


}

void LISTDLG::uebertragen (void)
{
    work->SetLsts (Lsts);
    work->FillRow ();
//    if (ArrDown != NULL && Lsts.choise[0] != NULL)
    {
//        Lsts.choise[0]->SetBitMap (ArrDown);
    }
}

int LISTDLG::WriteRec (int i)
{
    memcpy (Rec, LstTab [i], RecLen);
    work->SetLsts (Lsts);
    return work->WriteRec (&UbForm, DataForm.fieldanz - 1,i);
}

BOOL LISTDLG::OnKey12 (void)
{
    int i;
    int recanz;
    short sql_s;
    WMESS Wmess;
    extern short sql_mode;
//    int dsqlstatus;

    if (AfterRow (eListe.GetAktRow ()) == -1)
    {
        return TRUE;
    }
/*
    if (Leere Zeile == 0.0)
    {
        OnKeyDel ();
    }
*/
    else
    {
    }

    recanz = eListe.GetRecanz ();

//    work->DeleteAllPos ();
    
//	((SpezDlg *) Dlg)->OnKey12 ();
	lipreise_k.dat_von = dasc_to_long (defaultlistf.dat_von);
    lipreise_k.dat_bis = dasc_to_long (defaultlistf.dat_bis);
	lipreise_k.mdn = atoi(defaultlistf.mdn);
    if (work->Getflg_preise_prot() == 0)
	{
		Wmess.Message (hMainInst, hMainWindow, " \nBitte warten.....\n"
                                            "Die Preise werden geschrieben.\n ");  
		sql_s = sql_mode;
		sql_mode = 1;
		for (i = 0; i < recanz; i ++)
		{
	        int dsqlstatus = WriteRec (i);
			if (dsqlstatus != 0)
			{
	            Wmess.Destroy ();
				disp_mess ("Fehler beim Schreiben der Preise\n"
						"Die Datens�tze werden eventuell an einem anderen Arbeitsplatz\n"
						"bearbeitet", 2);
				sql_mode = sql_s;
				eListe.SetFeldFocus0 (eListe.GetAktRow (), eListe.GetAktRow ());
				return TRUE;
			}
		}
		sql_mode = sql_s;
		Wmess.Destroy ();
	}
	else
	{
				disp_mess ("Dies sind alte Preise, Sie werden daher nicht gespeichert!",2);
	}

//	eListe.BreakList ();
    if (work != NULL)
    {
        work->CommitWork ();
    }
	eListe.BreakList ();
//    disp_mess ("Die Preise wurden fehlerfrei geschrieben", 2);
//    eListe.SetFeldFocus0 (eListe.GetAktRow (), eListe.GetAktRow ());
    return TRUE;
}


BOOL LISTDLG::OnKeyUp (void)
{
//    form *DataForm;
//    int fpos;
/*
    if (Leere Zeile)
    {
        return OnKeyDel ();
    }
*/

    return FALSE;
}

BOOL LISTDLG::OnKeyDown (void)
{
//    form *DataForm;
//    int fpos;

/*
    if (Leere Zeile)
    {
        return TRUE;
    }
*/

    return FALSE;
}


void LISTDLG::PrintCaption (FILE *fp, char *buffer)
{
        char datum [12];
        char b[80];
        int len;

        Page ++;
        fprintf (fp, "\n");
        fprintf (fp, "%c%c", ESC, FETT1);

        sysdate (datum);
        memset (buffer, ' ', 78);
        buffer [78] = 0;
        memcpy (&buffer[1], "Preisliste vom  ", strlen ("Preisliste vom  "));
        memcpy (&buffer[16], datum, strlen (datum));
        memcpy (&buffer[30], "Mandant", strlen ("Mandant"));
        sprintf (b, "%2d", _mdn.mdn);
        memcpy (&buffer[38], b, strlen (b));
        work->GetMdnName (b, _mdn.mdn);
        b[16] = 0;
        memcpy (&buffer[41], b, strlen (b));
        memcpy (&buffer[69], "Seite", strlen ("Seite"));
        sprintf (&buffer[75], "%d", Page);
        fprintf (fp, "%s\n", buffer);

        memset (buffer, ' ', 78);
        buffer [78] = 0;
        memcpy (&buffer[1], "Kalenderwoche  ", strlen ("Kalenderwoche  "));
        sprintf (b, "%2d", kw);
        memcpy (&buffer[16], b, strlen (b));
        memcpy (&buffer[30], "NR", strlen ("NR"));
        sprintf (b, "%2d", lipreise_nr);
        memcpy (&buffer[38], b, strlen (b));
		strcpy (b, "");
        work->GetNrBez (b, lipreise_nr);
        sprintf (&buffer[41], "%s", b);
        b[16] = 0;
        fprintf (fp, "%s\n", buffer);

        fprintf (fp, "%c%c", ESC, FETT2);
        fprintf (fp, "\n");


        if (ENG > 0)
        {
                fprintf (fp, "%c%c", ESC, ENG);
        }
        if (FETT)
        {
                fprintf (fp, "%c%c", ESC, FETT1);
        }

		eListe.FillCubPrintUb (buffer, 0);
   	    fprintf (fp, "%s\n", buffer);
		eListe.FillCubPrintUb (buffer, 1);
   	    fprintf (fp, "%s\n", buffer);
		len = strlen (buffer);
        sprintf (buffer, "#LINE%d", len);
   	    fprintf (fp, "%s\n", buffer);

/*
        pline = new char [len + 10];
		memset (pline, '-', len);
		pline [len] = 0;
   	    fprintf (fp, "%s\n", pline);
        delete pline;
*/
        row = 7;
}


int LISTDLG::GetPageRows (char *Printer, mfont *mFont)
/**
Seitenlaenge fuer gewaehlten Drucker holen.
**/
{
     HDC hdc;
     int RowHeight;
     int PageRows;
     int cxPage;
     int cyPage;
     SIZE size;
     HFONT oldfont;
     TEXTMETRIC tm;


     hdc = GetPrinterbyName (Printer);

     if (hdc == NULL) return 0;
     cxPage = GetDeviceCaps (hdc, HORZRES);
     cyPage = GetDeviceCaps (hdc, VERTRES);
	 HFONT hFont = SetDeviceFont (hdc, mFont, &tm);
     oldfont = SelectObject (hdc, hFont);
     GetTextExtentPoint32 (hdc, "X", 1, &size);
	 RowHeight = (int) (double) ((double) tm.tmHeight * 1.0); 
     PageRows = (int) (double) ((double) cyPage / RowHeight);
     SelectObject (hdc, (HFONT) DeleteObject (hFont));
/*
	 hFont = SetDeviceFont (hdc, &mFont, &tm);
     oldfont = SelectObject (hdc, hFont);
     GetTextExtentPoint32 (hdc, "X", 1, &size);
     SelectObject (hdc, oldfont);
     PageCols = (int) (double) ((double) cxPage / size.cx);
     SelectObject (hdc, (HFONT) DeleteObject (hFont));
*/
     return PageRows;
}


void LISTDLG::ScrPrintCaption (HDC hdc, TEXTMETRIC *printtm, int page)
{
     TEXTMETRIC capttm;
     char buffer [80];
     char datum [12];
     SIZE size;
     int x, y;

     HFONT hFont   = SetDeviceFont (hdc, &printfettfont, &capttm);
     HFONT oldfont = SelectObject (hdc, hFont);
     GetTextMetrics (hdc, &capttm); 
     GetTextExtentPoint32 (hdc, "X", 1, &size);
     capttm.tmAveCharWidth = size.cx;
     SetTextColor (hdc,BLACKCOL);
     ::SetBkColor (hdc,WHITECOL);
     y = capttm.tmHeight;
     x = capttm.tmAveCharWidth;

     sysdate (datum);
     sprintf (buffer, "Preisliste vom ");
     TextOut (hdc, x, y, buffer, strlen (buffer));
     x = 15 * capttm.tmAveCharWidth;
     sprintf (buffer, "%s", datum);
     TextOut (hdc, x, y, buffer, strlen (buffer));

     x = 40 * capttm.tmAveCharWidth;
     sprintf (buffer, "Mandant");
     TextOut (hdc, x, y, buffer, strlen (buffer));

     x = 56 * capttm.tmAveCharWidth;
     sprintf (buffer, "%hd", _mdn.mdn);
     TextOut (hdc, x, y, buffer, strlen (buffer));

     x = 60 * capttm.tmAveCharWidth;
     work->GetMdnName ( buffer, _mdn.mdn);
     TextOut (hdc, x, y, buffer, strlen (buffer));

     sprintf (buffer, "Seite %d", page);
     GetTextExtentPoint32 (hdc, buffer, strlen (buffer), &size);
     x = GetDeviceCaps (hdc, HORZRES) - size.cx - 5;
     TextOut (hdc, x, y, buffer, strlen (buffer));

     y += (int) (double) ((double) capttm.tmHeight * 1.5);
     x = capttm.tmAveCharWidth;
     sprintf (buffer, "Kalenderwoche");
     TextOut (hdc, x, y, buffer, strlen (buffer));

     x = 15 * capttm.tmAveCharWidth;
     sprintf (buffer, "%hd", kw);
     TextOut (hdc, x, y, buffer, strlen (buffer));

     x = 40 * capttm.tmAveCharWidth;
     sprintf (buffer, "Listennummer");
     TextOut (hdc, x, y, buffer, strlen (buffer));

     x = 56 * capttm.tmAveCharWidth;
     sprintf (buffer, "%hd", lipreise_nr);
     TextOut (hdc, x, y, buffer, strlen (buffer));

     x = 60 * capttm.tmAveCharWidth;
	 strcpy (buffer, "");
     work->GetNrBez ( buffer, lipreise_nr);
     TextOut (hdc, x, y, buffer, strlen (buffer));
     DeleteObject (SelectObject (hdc, oldfont));   
}

void LISTDLG::LLPrintList (void)
{
  FILE *lst ;
   char datenam[22] ;
  char komma[128] ;
  char buffer [512] ;
  char zeilbuffer[80] ;
  static char iformname[30] ;
  char zwi_von[25] ;
  char zwi_bis[25] ;

  OnKey12();
  if (lipreise_k.anzlief > SpezDlg::liste_breit_ab)
  {
	strcpy (iformname,"lipreise"); 
  }
  else
  {
	strcpy (iformname,"lipreiseh");
  }

	sprintf ( datenam , "%s.llf", iformname ) ;

  if (getenv ("TMPPATH"))
  {
    sprintf (buffer, "%s\\%s", getenv("TMPPATH"), datenam);
  }
  else
  {
    strcpy (buffer, datenam );
  }
  remove (buffer) ;
  if ( (lst = fopen (buffer, "wt" )) == NULL ) 
  {
                     print_mess (2, "%s kann nicht ge�ffnet werden", buffer);
                     return ;
  }

        sprintf ( zeilbuffer, "NAME %s" , iformname ) ;
        fprintf ( lst , "%s\n" , zeilbuffer ) ;

        memset ( zeilbuffer, '\0', 128 ) ;
        sprintf ( zeilbuffer, "DRUCK %hd" , 1 ) ;
        fprintf ( lst , "%s\n" , zeilbuffer ) ;

        memset ( zeilbuffer, '\0', 128 ) ;
        sprintf ( zeilbuffer, "LABEL %hd" , 0 ) ; 
        fprintf ( lst , "%s\n" , zeilbuffer ) ;

        memset ( zeilbuffer, '\0', 80 ) ;
        sprintf ( zeilbuffer, "%-16s %12s %12s","#Feldname", "von", "bis") ;
        fprintf ( lst , "%s\n" , zeilbuffer ) ;

        sprintf ( zwi_von , "%hd" , lipreise_k.nr ) ;
        sprintf ( zwi_bis  ,"%hd" , lipreise_k.nr ) ;

          sprintf ( zeilbuffer, "%-16s %12s %12s","nr", zwi_von, zwi_bis) ;
          fprintf ( lst , "%s\n" , zeilbuffer ) ;

    fclose ( lst ) ;
  if (getenv ("TMPPATH"))
  {
    sprintf (komma, "%s -name %s -datei %s\\%s","dr70001",iformname, getenv("TMPPATH"), datenam);
  }
  else
  {
    strcpy (komma, datenam );
  }

    HCURSOR oldcursor = SetCursor ( LoadCursor ( NULL , IDC_WAIT)) ;

    ProcWaitExecEx ( komma, SW_SHOWNORMAL, 0, 40, -1, 0);
//	eListe.BreakList ();
    eListe.SetFeldFocus0 (eListe.GetAktRow (),eListe.GetAktColumn ());

}
      
void LISTDLG::ScrPrintList (void)
{
        TEXTMETRIC printtm;
        TEXTMETRIC scrtm;
        SIZE size;
        int scrRowHeightP;
        int scrUbHeight;
        RECT rect;
        COLORREF BkCol;
        int page = 0;
        DOCINFO di = {sizeof (DOCINFO), "dbinfo", NULL};
        
/*
        if (QuerPrint)
        {
              SetQuerDefault (TRUE);
        }
*/


        if (SelectPrinter () == FALSE)
        {
            return;
        }


/*
        if (::SetPage () == FALSE)
        {
            return;
        }
*/

        BkCol = eListe.GetBkColor ();
        if (PrintBackground == FALSE)
        {
            eListe.SetBkColor (WHITECOL);
        }

        char *Printer = GetPrinterName ();
        if (Printer == NULL)
        {
            Printer = GetPrinterDevName ();
        }
        if (Printer == NULL)
        {
            return;
        }
//        memcpy (&printfont, eListe.GetAktFont (), sizeof (mfont));
//        memcpy (&printfettfont, eListe.GetAktFont (), sizeof (mfont));

        printfont.FontBkColor = WHITECOL;
        printfettfont.FontBkColor = WHITECOL;
        printfettfont.FontAttribute = 1;

        HDC hdc = GetPrinterbyName (Printer);
        if (hdc == NULL) return;
        eListe.PrintHdc = TRUE;
        eListe.ublen = 6;
        if (StartDoc (hdc, &di) == 0) return; 

   	    HFONT hFont   = SetDeviceFont (hdc, &printfont, &printtm);
        HFONT oldfont = SelectObject (hdc, hFont);

        rect.top = 0;
        rect.left = 0;
        rect.bottom = GetDeviceCaps (hdc, VERTRES);
        rect.right  = GetDeviceCaps (hdc, HORZRES);
        eListe.pagelen = GetPageRows (Printer, &printfont) - 2;

        eListe.pagelen0 = (int) (double) ((double) 
                    eListe.pagelen / RowHeight - ublen);
        eListe.pagelen0 /= eListe.GetRecHeight ();
        eListe.pagelen0 --;
        eListe.PageStdCols = GetPageStdCols (Printer);


	    GetTextExtentPoint32 (hdc, "X", 1, &size);
        printtm.tmAveCharWidth = size.cx;

        if (rect.bottom > (eListe.pagelen + 1) * printtm.tmHeight)
        {
            rect.bottom = (eListe.pagelen + 1) * printtm.tmHeight;
        }

        memcpy (&scrtm, eListe.GetListTextMetrics (),   sizeof (TEXTMETRIC)); 
        memcpy (eListe.GetListTextMetrics (), &printtm, sizeof (TEXTMETRIC)); 
        scrUbHeight = eListe.GetUbHeight ();
        eListe.SetUbHeight (scrUbHeight * printtm.tmHeight / scrtm.tmHeight);

        scrRowHeightP = eListe.GetRowHeightP (); 
		eListe.SetRowHeightP ((int) (double) ((double) printtm.tmHeight * RowHeight));

        eListe.printscrollpos = 0;
        eListe.PageStart = 0;
        while (eListe.printscrollpos < eListe.GetRecanz ())
        {
              eListe.PageEnd = eListe.PageStart + eListe.pagelen * printtm.tmHeight;
              if (StartPage (hdc) == 0) return;
              if (PrintBackground && (eListe.GetBkColor () != WHITECOL))
              {
                    HBRUSH hBrush = CreateSolidBrush (LTGRAYCOL);
                    SelectObject (hdc, hBrush);
                    SelectObject (hdc, GetStockObject (NULL_PEN));
                    Rectangle (hdc,0, 0, GetDeviceCaps (hdc, HORZRES), 
                                         GetDeviceCaps (hdc, VERTRES));
                    DeleteObject (hBrush); 
              }
              page ++;
              ScrPrintCaption (hdc, &printtm, page);
              eListe.FillUbPFrmRow (hdc, eListe.Getfrmrow (), &UbForm, 0);
              eListe.ShowPrintForms (hdc, 2);
              eListe.PrintPVlines (hdc, &rect);
              eListe.PrintPHlines (hdc, &rect);
              EndPage (hdc);
              eListe.printscrollpos += eListe.pagelen0;
              eListe.PageStart = eListe.PageEnd;
        }
        DeleteObject (SelectObject (hdc, oldfont));
    	EndDoc (hdc);
        memcpy (eListe.GetListTextMetrics (), &scrtm,   sizeof (TEXTMETRIC)); 
        eListe.SetRowHeightP (scrRowHeightP); 
        eListe.SetUbHeight (scrUbHeight);
        eListe.PrintHdc = FALSE;
        eListe.SetBkColor (BkCol);
        SetListFocus ();
}


void LISTDLG::PrintList (void)
/**
Liste drucken.
**/
{

	    char *tmp;
		char buffer [0x1000];
		char listname [256];
		char command  [512];
		char *p;
		FILE *fp;
		int anz;
		int i;
		int len;
		gPrintl Gprint;
//        DWORD ExitCode;


        if (LLPrint)
        {
            LLPrintList ();
            return;
        }
        if (ScrPrint)
        {
            ScrPrintList ();
            return;
        }

        pagelen = 60;
        PageStdCols = 80;
        Page = 0;

/*
        if (QuerPrint)
        {
              SetQuerDefault (TRUE);
        }
        if (DrkDeviceFile != NULL)
        {
             SetDrkDeviceFile (DrkDeviceFile);
        }
        else
        {
             SetDrkDeviceFile (NULL);
        }
        if (RowPrintFont != 0)
        {
            SetRowPrintFont (RowPrintFont);
        }
*/

        if (SelectPrinter ())
        {
             pagelen  = ::GetPageRows (NULL) - 3;
             pagelen0 = pagelen;
             PageStdCols = GetPageStdCols (NULL);
/*
             if (BeforePrint != NULL)
             {
                 ChangePrintParams ();
             }
*/
        }
        else
        {
             return;
        }

        len = eListe.GetUbFrmlen (UbForm.fieldanz - 1);
        frmlen = len;
        ENG = GetRowWidthStz (len);
        if (PrintFont != 0)
        {
            ENG = PrintFont;
        }

  	    strcpy (listname, "lipreise.lst");
		p = strchr (listname, '.');
		if (p) *p = 0;

		tmp = getenv ("TMPPATH");
        if (tmp == NULL) 
		{
			sprintf (buffer, "%s.lst", listname);
		}
		else
		{
			sprintf (buffer, "%s\\%s.lst", tmp,listname);
		}

		fp = fopen (buffer, "w");
		if (fp == NULL)
		{
			print_mess (2, "Druckdatei kann nicht ge�ffnet werden");
			return;
		}

        PrintCaption (fp, buffer);
        anz = eListe.GetRecanz ();

        if (ENG > 0)
        {
                fprintf (fp, "%c%c", ESC, NORM);
        }
        if (FETT)
        {
                fprintf (fp, "%c%c", ESC, FETT2);
        }

        for (i = 0; i < anz; i ++)
		{
			if (row + 1 >= pagelen0)
			{
                    for (;row < pagelen0; row ++) 
                    {
                        fprintf (fp, "\n");
                    }

                    fprintf (fp, "%c%c", ESC, NORM);
		            PrintCaption (fp, buffer);
			}
            FromMemory (i);
            if (ENG > 0)
            {
                fprintf (fp, "%c%c", ESC, ENG);
            }
            if (FETT)
            {
                fprintf (fp, "%c%c", ESC, FETT1);
            }
            eListe.FillPrintRow (buffer, 0);
 			fprintf (fp, "%s\n", buffer);
            row ++;
            eListe.FillPrintRow (buffer, 1);
 			fprintf (fp, "%s\n", buffer);

            if (FETT)
            {
                fprintf (fp, "%c%c", ESC, FETT2);
            }

            if (ENG > 0)
            {
                fprintf (fp, "%c%c", ESC, NORM);
            }

			row ++;
		}
		fclose (fp);
//		GlobalFree (pline);

        if (tmp == NULL) 
		{
			sprintf (buffer, "%s.lst", listname);
		}
		else
		{
			sprintf (buffer, "%s\\%s.lst", tmp,listname);
		}

/*
        if (PrintFile != NULL)
        {
            strcpy (buffer, PrintFile);
        }
*/
	    sprintf (command, "-p%d %s", 1, buffer); 
		Gprint.Print (command, SW_SHOWNORMAL);
//        SetQuerDefault (FALSE);
        eListe.SetFeldFocus0 (eListe.GetAktRow (),eListe.GetAktColumn ());
}

void LISTDLG::CreateListQuikInfos (HWND hWnd)
{
       if (Toolbar2 == NULL) return;

       Toolbar2->GetCfield ("f5")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("f12")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("f9")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("del")->CreateQuikInfos (hWnd);
       Toolbar2->GetCfield ("insert")->CreateQuikInfos (hWnd);
}


BOOL LISTDLG::OnCommand (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    if (LOWORD (wParam) == IDM_CHOISE)
    {
        return OnKey9 ();
    }
    if (LOWORD (wParam) == IDM_CANCEL)
    {
        return OnKey5 ();
    }
    if (LOWORD (wParam) == IDM_WRITE)
    {
        return OnKey12 ();
    }
    if (LOWORD (wParam) == VK_DELETE)
    {
        return OnKeyDel ();
    }
    if (LOWORD (wParam) == IDM_DELETE)
    {
        return OnKeyDel ();
    }
    if (LOWORD (wParam) == IDM_PRINT)
    {
        PrintList ();
        return TRUE;
    }
    if (LOWORD (wParam) == VK_INSERT)
    {
        return OnKeyInsert ();
    }
    if (LOWORD (wParam) == IDM_INSERT)
    {
        return OnKey6 ();
    }
    if (HIWORD (wParam)  == CBN_CLOSEUP)
	{
       if (Dlg != NULL)
	   {
	           return ((SpezDlg *) Dlg)->OnCloseUp (hWnd, msg, wParam, lParam);
	   }
	}
    return FALSE;
}

BOOL LISTDLG::OnPaint (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    HDC hdc;

    if (Modal == FALSE) return FALSE;
    if (this->mamain1 != hWnd) return FALSE; 
    
    hdc = BeginPaint (hWnd, &ps);
    fChoise.display (hWnd, hdc);
    EndPaint (hWnd,&ps);
    return FALSE;
}

BOOL LISTDLG::OnMove (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{

    if (Modal == FALSE) return FALSE;
    if (this->mamain1 != hWnd) return FALSE; 

    fChoise.MoveWindow ();
    return FALSE;
}


void LISTDLG::SetNoRecNr (void)
{
	   int i;

	   eListe.SetNoRecNr (TRUE);

       if (NoRecNrOK) return;
	   for (i = 0; i < dataform->fieldanz; i ++)
	   {
		   dataform->mask[i].pos[1] -= 6;
	   }
	   for (i = 0; i < ubform->fieldanz; i ++)
	   {
		   ubform->mask[i].pos[1] -= 6;
	   }
	   for (i = 0; i < lineform->fieldanz; i ++)
	   {
		   lineform->mask[i].pos[1] -= 6;
	   }
       NoRecNrOK = TRUE;
}

void LISTDLG::EnterList ()
{
	LISTENTER::EnterList ();
	work->Close ();
}




