#ifndef _LIPREISE_P_DEF
#define _LIPREISE_P_DEF

#include "windows.h"
#include "mo_draw.h"
#include "wmask.h"
#include "dbclass.h"

struct LIPREISE_P {
   short     mdn;
   short     nr;
   double    a;
   double    pr_ek1;
   double    pr_ek2;
   double    pr_ek3;
   double    pr_ek4;
   double    pr_ek5;
   double    pr_ek6;
   double    pr_ek7;
   double    pr_ek8;
   double    pr_ek9;
   double    pr_ek10;
   double    pr_ek11;
   double    pr_ek12;
   double    pr_ek13;
   double    pr_ek14;
   short     posi;
};
extern struct LIPREISE_P lipreise_p, lipreise_p_null;

#line 10 "lipreise_p.rh"

class LIPREISE_P_CLASS : public DB_CLASS 
{
       private :
               int cursor_best;
               void prepare (void);
       public :
               LIPREISE_P_CLASS () : DB_CLASS ()
               {
               }
               ~LIPREISE_P_CLASS ()
               {
                   dbclose ();
               } 
};
#endif
