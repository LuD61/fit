#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "a_kun.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "best_pos.h" 

struct BEST_POS best_pos, best_pos_null;

void BEST_POS_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &best_pos.mdn, 1, 0);
            ins_quest ((char *) &best_pos.fil, 1, 0);
            ins_quest ((char *) &best_pos.best_blg, 2, 0);
            ins_quest ((char *) &best_pos.lief, 0, 17);
            ins_quest ((char *) best_pos.lief_best, 0, sizeof (best_pos.lief_best));
            ins_quest ((char *) &best_pos.a, 3, 0);
    out_quest ((char *) &best_pos.a,3,0);
    out_quest ((char *) &best_pos.anz_einh,3,0);
    out_quest ((char *) &best_pos.bearb_stat,1,0);
    out_quest ((char *) &best_pos.best_blg,2,0);
    out_quest ((char *) &best_pos.best_term,2,0);
    out_quest ((char *) &best_pos.fil,1,0);
    out_quest ((char *) &best_pos.inh,3,0);
    out_quest ((char *) best_pos.lief,0,17);
    out_quest ((char *) best_pos.lief_best,0,17);
    out_quest ((char *) &best_pos.lief_s,2,0);
    out_quest ((char *) &best_pos.lief_term,2,0);
    out_quest ((char *) &best_pos.mdn,1,0);
    out_quest ((char *) &best_pos.me,3,0);
    out_quest ((char *) &best_pos.me_ist,3,0);
    out_quest ((char *) &best_pos.p_num,1,0);
    out_quest ((char *) &best_pos.pr_ek,3,0);
    out_quest ((char *) best_pos.sa_kz,0,2);
    out_quest ((char *) best_pos.sti_pro_kz,0,2);
    out_quest ((char *) &best_pos.me_einh,1,0);
    out_quest ((char *) best_pos.pr_abw,0,2);
    out_quest ((char *) &best_pos.fakt,3,0);
    out_quest ((char *) &best_pos.pr_ek_bto,3,0);
    out_quest ((char *) &best_pos.best_txt,2,0);
    out_quest ((char *) &best_pos.pr_ek_euro,3,0);
    out_quest ((char *) &best_pos.pr_ek_euro_bto,3,0);
    out_quest ((char *) &best_pos.pr_ek_fremd,3,0);
    out_quest ((char *) &best_pos.pr_ek_fremd_bto,3,0);
    out_quest ((char *) &best_pos.kontrakt_me,3,0);
    out_quest ((char *) &best_pos.kontrakt_me_soll,3,0);
    out_quest ((char *) &best_pos.paz,3,0);
    out_quest ((char *) &best_pos.inh_ek,2,0);
    out_quest ((char *) &best_pos.lakt_rab,3,0);
    out_quest ((char *) &best_pos.aakt_rab,3,0);
    out_quest ((char *) &best_pos.aakt_pr,3,0);
            cursor = prepare_sql ("select best_pos.a,  "
"best_pos.anz_einh,  best_pos.bearb_stat,  best_pos.best_blg,  "
"best_pos.best_term,  best_pos.fil,  best_pos.inh,  best_pos.lief,  "
"best_pos.lief_best,  best_pos.lief_s,  best_pos.lief_term,  "
"best_pos.mdn,  best_pos.me,  best_pos.me_ist,  best_pos.p_num,  "
"best_pos.pr_ek,  best_pos.sa_kz,  best_pos.sti_pro_kz,  "
"best_pos.me_einh,  best_pos.pr_abw,  best_pos.fakt,  "
"best_pos.pr_ek_bto,  best_pos.best_txt,  best_pos.pr_ek_euro,  "
"best_pos.pr_ek_euro_bto,  best_pos.pr_ek_fremd,  "
"best_pos.pr_ek_fremd_bto,  best_pos.kontrakt_me,  "
"best_pos.kontrakt_me_soll,  best_pos.paz,  best_pos.inh_ek,  "
"best_pos.lakt_rab,  best_pos.aakt_rab,  best_pos.aakt_pr from best_pos "

#line 31 "best_pos.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   best_blg = ? "
                                  "and   lief = ? "
                                  "and   lief_best = ? "
                                  "and   a = ?"); 

    ins_quest ((char *) &best_pos.a,3,0);
    ins_quest ((char *) &best_pos.anz_einh,3,0);
    ins_quest ((char *) &best_pos.bearb_stat,1,0);
    ins_quest ((char *) &best_pos.best_blg,2,0);
    ins_quest ((char *) &best_pos.best_term,2,0);
    ins_quest ((char *) &best_pos.fil,1,0);
    ins_quest ((char *) &best_pos.inh,3,0);
    ins_quest ((char *) best_pos.lief,0,17);
    ins_quest ((char *) best_pos.lief_best,0,17);
    ins_quest ((char *) &best_pos.lief_s,2,0);
    ins_quest ((char *) &best_pos.lief_term,2,0);
    ins_quest ((char *) &best_pos.mdn,1,0);
    ins_quest ((char *) &best_pos.me,3,0);
    ins_quest ((char *) &best_pos.me_ist,3,0);
    ins_quest ((char *) &best_pos.p_num,1,0);
    ins_quest ((char *) &best_pos.pr_ek,3,0);
    ins_quest ((char *) best_pos.sa_kz,0,2);
    ins_quest ((char *) best_pos.sti_pro_kz,0,2);
    ins_quest ((char *) &best_pos.me_einh,1,0);
    ins_quest ((char *) best_pos.pr_abw,0,2);
    ins_quest ((char *) &best_pos.fakt,3,0);
    ins_quest ((char *) &best_pos.pr_ek_bto,3,0);
    ins_quest ((char *) &best_pos.best_txt,2,0);
    ins_quest ((char *) &best_pos.pr_ek_euro,3,0);
    ins_quest ((char *) &best_pos.pr_ek_euro_bto,3,0);
    ins_quest ((char *) &best_pos.pr_ek_fremd,3,0);
    ins_quest ((char *) &best_pos.pr_ek_fremd_bto,3,0);
    ins_quest ((char *) &best_pos.kontrakt_me,3,0);
    ins_quest ((char *) &best_pos.kontrakt_me_soll,3,0);
    ins_quest ((char *) &best_pos.paz,3,0);
    ins_quest ((char *) &best_pos.inh_ek,2,0);
    ins_quest ((char *) &best_pos.lakt_rab,3,0);
    ins_quest ((char *) &best_pos.aakt_rab,3,0);
    ins_quest ((char *) &best_pos.aakt_pr,3,0);
            sqltext = "update best_pos set best_pos.a = ?,  "
"best_pos.anz_einh = ?,  best_pos.bearb_stat = ?,  "
"best_pos.best_blg = ?,  best_pos.best_term = ?,  best_pos.fil = ?,  "
"best_pos.inh = ?,  best_pos.lief = ?,  best_pos.lief_best = ?,  "
"best_pos.lief_s = ?,  best_pos.lief_term = ?,  best_pos.mdn = ?,  "
"best_pos.me = ?,  best_pos.me_ist = ?,  best_pos.p_num = ?,  "
"best_pos.pr_ek = ?,  best_pos.sa_kz = ?,  best_pos.sti_pro_kz = ?,  "
"best_pos.me_einh = ?,  best_pos.pr_abw = ?,  best_pos.fakt = ?,  "
"best_pos.pr_ek_bto = ?,  best_pos.best_txt = ?,  "
"best_pos.pr_ek_euro = ?,  best_pos.pr_ek_euro_bto = ?,  "
"best_pos.pr_ek_fremd = ?,  best_pos.pr_ek_fremd_bto = ?,  "
"best_pos.kontrakt_me = ?,  best_pos.kontrakt_me_soll = ?,  "
"best_pos.paz = ?,  best_pos.inh_ek = ?,  best_pos.lakt_rab = ?,  "
"best_pos.aakt_rab = ?,  best_pos.aakt_pr = ? "

#line 39 "best_pos.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   best_blg = ? "
                                  "and   lief = ? "
                                  "and   lief_best = ? "
                                  "and   a = ?";


            ins_quest ((char *) &best_pos.mdn, 1, 0);
            ins_quest ((char *) &best_pos.fil, 1, 0);
            ins_quest ((char *) &best_pos.best_blg, 2, 0);
            ins_quest ((char *) &best_pos.lief, 0, 17);
            ins_quest ((char *) best_pos.lief_best, 0, sizeof (best_pos.lief_best));
            ins_quest ((char *) &best_pos.a, 3, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &best_pos.mdn, 1, 0);
            ins_quest ((char *) &best_pos.fil, 1, 0);
            ins_quest ((char *) &best_pos.best_blg, 2, 0);
            ins_quest ((char *) &best_pos.lief, 0, 17);
            ins_quest ((char *) best_pos.lief_best, 0, sizeof (best_pos.lief_best));
            ins_quest ((char *) &best_pos.a, 3, 0);
            test_upd_cursor = prepare_sql ("select a from best_pos "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   best_blg = ? "
                                  "and   lief = ? "
                                  "and   lief_best = ? "
                                  "and   a = ?");
            ins_quest ((char *) &best_pos.mdn, 1, 0);
            ins_quest ((char *) &best_pos.fil, 1, 0);
            ins_quest ((char *) &best_pos.best_blg, 2, 0);
            ins_quest ((char *) &best_pos.lief, 0, 17);
            ins_quest ((char *) best_pos.lief_best, 0, sizeof (best_pos.lief_best));
            ins_quest ((char *) &best_pos.a, 3, 0);
            del_cursor = prepare_sql ("delete from best_pos "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   best_blg = ? "
                                  "and   lief = ? "
                                  "and   lief_best = ? "
                                  "and   a = ?");
            ins_quest ((char *) &best_pos.mdn, 1, 0);
            ins_quest ((char *) &best_pos.fil, 1, 0);
            ins_quest ((char *) &best_pos.best_blg, 2, 0);
            ins_quest ((char *) &best_pos.lief, 0, 17);
            del_best_curs = prepare_sql ("delete from best_pos "
                                         "where mdn = ? "
                                         "and   fil = ? "
                                         "and   best_blg = ? "
                                         "and   lief = ?");
    ins_quest ((char *) &best_pos.a,3,0);
    ins_quest ((char *) &best_pos.anz_einh,3,0);
    ins_quest ((char *) &best_pos.bearb_stat,1,0);
    ins_quest ((char *) &best_pos.best_blg,2,0);
    ins_quest ((char *) &best_pos.best_term,2,0);
    ins_quest ((char *) &best_pos.fil,1,0);
    ins_quest ((char *) &best_pos.inh,3,0);
    ins_quest ((char *) best_pos.lief,0,17);
    ins_quest ((char *) best_pos.lief_best,0,17);
    ins_quest ((char *) &best_pos.lief_s,2,0);
    ins_quest ((char *) &best_pos.lief_term,2,0);
    ins_quest ((char *) &best_pos.mdn,1,0);
    ins_quest ((char *) &best_pos.me,3,0);
    ins_quest ((char *) &best_pos.me_ist,3,0);
    ins_quest ((char *) &best_pos.p_num,1,0);
    ins_quest ((char *) &best_pos.pr_ek,3,0);
    ins_quest ((char *) best_pos.sa_kz,0,2);
    ins_quest ((char *) best_pos.sti_pro_kz,0,2);
    ins_quest ((char *) &best_pos.me_einh,1,0);
    ins_quest ((char *) best_pos.pr_abw,0,2);
    ins_quest ((char *) &best_pos.fakt,3,0);
    ins_quest ((char *) &best_pos.pr_ek_bto,3,0);
    ins_quest ((char *) &best_pos.best_txt,2,0);
    ins_quest ((char *) &best_pos.pr_ek_euro,3,0);
    ins_quest ((char *) &best_pos.pr_ek_euro_bto,3,0);
    ins_quest ((char *) &best_pos.pr_ek_fremd,3,0);
    ins_quest ((char *) &best_pos.pr_ek_fremd_bto,3,0);
    ins_quest ((char *) &best_pos.kontrakt_me,3,0);
    ins_quest ((char *) &best_pos.kontrakt_me_soll,3,0);
    ins_quest ((char *) &best_pos.paz,3,0);
    ins_quest ((char *) &best_pos.inh_ek,2,0);
    ins_quest ((char *) &best_pos.lakt_rab,3,0);
    ins_quest ((char *) &best_pos.aakt_rab,3,0);
    ins_quest ((char *) &best_pos.aakt_pr,3,0);
            ins_cursor = prepare_sql ("insert into best_pos ("
"a,  anz_einh,  bearb_stat,  best_blg,  best_term,  fil,  inh,  lief,  lief_best,  "
"lief_s,  lief_term,  mdn,  me,  me_ist,  p_num,  pr_ek,  sa_kz,  sti_pro_kz,  me_einh,  "
"pr_abw,  fakt,  pr_ek_bto,  best_txt,  pr_ek_euro,  pr_ek_euro_bto,  "
"pr_ek_fremd,  pr_ek_fremd_bto,  kontrakt_me,  kontrakt_me_soll,  paz,  "
"inh_ek,  lakt_rab,  aakt_rab,  aakt_pr) "

#line 91 "best_pos.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"); 

#line 93 "best_pos.rpp"
}

void BEST_POS_CLASS::prepare_o (char *order)
{

            ins_quest ((char *) &best_pos.mdn, 1, 0);
            ins_quest ((char *) &best_pos.fil, 1, 0);
            ins_quest ((char *) &best_pos.best_blg, 2, 0);
            ins_quest ((char *) &best_pos.lief, 0, 17);
            if (order)
            {
    out_quest ((char *) &best_pos.a,3,0);
    out_quest ((char *) &best_pos.anz_einh,3,0);
    out_quest ((char *) &best_pos.bearb_stat,1,0);
    out_quest ((char *) &best_pos.best_blg,2,0);
    out_quest ((char *) &best_pos.best_term,2,0);
    out_quest ((char *) &best_pos.fil,1,0);
    out_quest ((char *) &best_pos.inh,3,0);
    out_quest ((char *) best_pos.lief,0,17);
    out_quest ((char *) best_pos.lief_best,0,17);
    out_quest ((char *) &best_pos.lief_s,2,0);
    out_quest ((char *) &best_pos.lief_term,2,0);
    out_quest ((char *) &best_pos.mdn,1,0);
    out_quest ((char *) &best_pos.me,3,0);
    out_quest ((char *) &best_pos.me_ist,3,0);
    out_quest ((char *) &best_pos.p_num,1,0);
    out_quest ((char *) &best_pos.pr_ek,3,0);
    out_quest ((char *) best_pos.sa_kz,0,2);
    out_quest ((char *) best_pos.sti_pro_kz,0,2);
    out_quest ((char *) &best_pos.me_einh,1,0);
    out_quest ((char *) best_pos.pr_abw,0,2);
    out_quest ((char *) &best_pos.fakt,3,0);
    out_quest ((char *) &best_pos.pr_ek_bto,3,0);
    out_quest ((char *) &best_pos.best_txt,2,0);
    out_quest ((char *) &best_pos.pr_ek_euro,3,0);
    out_quest ((char *) &best_pos.pr_ek_euro_bto,3,0);
    out_quest ((char *) &best_pos.pr_ek_fremd,3,0);
    out_quest ((char *) &best_pos.pr_ek_fremd_bto,3,0);
    out_quest ((char *) &best_pos.kontrakt_me,3,0);
    out_quest ((char *) &best_pos.kontrakt_me_soll,3,0);
    out_quest ((char *) &best_pos.paz,3,0);
    out_quest ((char *) &best_pos.inh_ek,2,0);
    out_quest ((char *) &best_pos.lakt_rab,3,0);
    out_quest ((char *) &best_pos.aakt_rab,3,0);
    out_quest ((char *) &best_pos.aakt_pr,3,0);
                    cursor_o = prepare_sql ("select "
"best_pos.a,  best_pos.anz_einh,  best_pos.bearb_stat,  "
"best_pos.best_blg,  best_pos.best_term,  best_pos.fil,  best_pos.inh,  "
"best_pos.lief,  best_pos.lief_best,  best_pos.lief_s,  "
"best_pos.lief_term,  best_pos.mdn,  best_pos.me,  best_pos.me_ist,  "
"best_pos.p_num,  best_pos.pr_ek,  best_pos.sa_kz,  "
"best_pos.sti_pro_kz,  best_pos.me_einh,  best_pos.pr_abw,  "
"best_pos.fakt,  best_pos.pr_ek_bto,  best_pos.best_txt,  "
"best_pos.pr_ek_euro,  best_pos.pr_ek_euro_bto,  "
"best_pos.pr_ek_fremd,  best_pos.pr_ek_fremd_bto,  "
"best_pos.kontrakt_me,  best_pos.kontrakt_me_soll,  best_pos.paz,  "
"best_pos.inh_ek,  best_pos.lakt_rab,  best_pos.aakt_rab,  "
"best_pos.aakt_pr from best_pos "

#line 105 "best_pos.rpp"
                                          "where mdn = ? "
                                          "and   fil = ? "
                                          "and   best_blg = ? "
                                          "and   lief = ? %s", order);
            }
            else
            {
    out_quest ((char *) &best_pos.a,3,0);
    out_quest ((char *) &best_pos.anz_einh,3,0);
    out_quest ((char *) &best_pos.bearb_stat,1,0);
    out_quest ((char *) &best_pos.best_blg,2,0);
    out_quest ((char *) &best_pos.best_term,2,0);
    out_quest ((char *) &best_pos.fil,1,0);
    out_quest ((char *) &best_pos.inh,3,0);
    out_quest ((char *) best_pos.lief,0,17);
    out_quest ((char *) best_pos.lief_best,0,17);
    out_quest ((char *) &best_pos.lief_s,2,0);
    out_quest ((char *) &best_pos.lief_term,2,0);
    out_quest ((char *) &best_pos.mdn,1,0);
    out_quest ((char *) &best_pos.me,3,0);
    out_quest ((char *) &best_pos.me_ist,3,0);
    out_quest ((char *) &best_pos.p_num,1,0);
    out_quest ((char *) &best_pos.pr_ek,3,0);
    out_quest ((char *) best_pos.sa_kz,0,2);
    out_quest ((char *) best_pos.sti_pro_kz,0,2);
    out_quest ((char *) &best_pos.me_einh,1,0);
    out_quest ((char *) best_pos.pr_abw,0,2);
    out_quest ((char *) &best_pos.fakt,3,0);
    out_quest ((char *) &best_pos.pr_ek_bto,3,0);
    out_quest ((char *) &best_pos.best_txt,2,0);
    out_quest ((char *) &best_pos.pr_ek_euro,3,0);
    out_quest ((char *) &best_pos.pr_ek_euro_bto,3,0);
    out_quest ((char *) &best_pos.pr_ek_fremd,3,0);
    out_quest ((char *) &best_pos.pr_ek_fremd_bto,3,0);
    out_quest ((char *) &best_pos.kontrakt_me,3,0);
    out_quest ((char *) &best_pos.kontrakt_me_soll,3,0);
    out_quest ((char *) &best_pos.paz,3,0);
    out_quest ((char *) &best_pos.inh_ek,2,0);
    out_quest ((char *) &best_pos.lakt_rab,3,0);
    out_quest ((char *) &best_pos.aakt_rab,3,0);
    out_quest ((char *) &best_pos.aakt_pr,3,0);
                    cursor_o = prepare_sql ("select "
"best_pos.a,  best_pos.anz_einh,  best_pos.bearb_stat,  "
"best_pos.best_blg,  best_pos.best_term,  best_pos.fil,  best_pos.inh,  "
"best_pos.lief,  best_pos.lief_best,  best_pos.lief_s,  "
"best_pos.lief_term,  best_pos.mdn,  best_pos.me,  best_pos.me_ist,  "
"best_pos.p_num,  best_pos.pr_ek,  best_pos.sa_kz,  "
"best_pos.sti_pro_kz,  best_pos.me_einh,  best_pos.pr_abw,  "
"best_pos.fakt,  best_pos.pr_ek_bto,  best_pos.best_txt,  "
"best_pos.pr_ek_euro,  best_pos.pr_ek_euro_bto,  "
"best_pos.pr_ek_fremd,  best_pos.pr_ek_fremd_bto,  "
"best_pos.kontrakt_me,  best_pos.kontrakt_me_soll,  best_pos.paz,  "
"best_pos.inh_ek,  best_pos.lakt_rab,  best_pos.aakt_rab,  "
"best_pos.aakt_pr from best_pos "

#line 113 "best_pos.rpp"
                                          "where mdn = ? "
                                          "and   fil = ? "
                                          "and   nest_blg = ? "
                                          "and   lief = ?");
           }
}

int BEST_POS_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int BEST_POS_CLASS::dbreadfirst_o (char *order)
/**
Ersten Satz aus Tabelle lesen.
**/
{
     
         if (cursor == -1)
         {
                this->prepare ();
         }
         if (cursor_o == -1)
         {
                this->prepare_o (order);
         }
         open_sql (cursor_o);
         fetch_sql (cursor_o);
         return (sqlstatus);
}

int BEST_POS_CLASS::dbread_o ()
/**
Ersten Satz aus Tabelle lesen.
**/
{
       
         fetch_sql (cursor_o);
         return (sqlstatus);
}

int BEST_POS_CLASS::dbdeletebest ()
/**
Alles Positionen einer Bestellung loeschen.
**/
{
         if (del_best_curs == -1)
         {
                     prepare ();
         }
         return execute_curs (del_best_curs);
}

int BEST_POS_CLASS::dbclose_o ()
/**
Cursor schliessen.
**/
{
         if (del_best_curs)
         {
                      close_sql (del_best_curs);
                      del_best_curs = -1;
         }
         this->DB_CLASS::dbclose ();
         return 0;
}


