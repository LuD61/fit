#ifndef _RBRUSH_DEF
#define _RBRUSH_DEF
#include <windows.h>

class RBrush
{
   private :
	   COLORREF color;
	   HBRUSH hBrush;
	   int Instances;
   public :
	   RBrush ();
	   RBrush (COLORREF, HBRUSH);
	   ~RBrush ();
       BOOL operator== (RBrush&);
       RBrush& operator=(RBrush&);

	   void SetColor (COLORREF);
	   COLORREF GetColor ();
	   void SetBrush (HBRUSH);
	   HBRUSH GetBrush ();
	   void inc ();
	   void dec ();
	   int GetInstances ();
};
#endif
