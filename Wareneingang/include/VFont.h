#ifndef _VFONT_DEF
#define _VFONT_DEF
#include "vector.h"
#include "RFont.h"

class VFont : public CVector
{
private :
	HFONT StdFont;
public :
	VFont ();
	~VFont ();
	void SetStdFont (HFONT);
	BOOL Exist (RFont*);
	BOOL CanDestroy (HFONT);
};
#endif