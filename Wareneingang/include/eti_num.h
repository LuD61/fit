struct ETI_NUM
{
	long eti;
};

extern struct ETI_NUM eti_num, eti_num_null;

class ETI_NUM_CLASS 
{
       private :
               short cursor;
               short test_upd_cursor;
               short upd_cursor;
               short ins_cursor;
               short del_cursor;
               void prepare (void);

       public :
               ETI_NUM_CLASS ()
               {
                         cursor       = -1;
                         test_upd_cursor = -1;
                         upd_cursor      = -1;
                         ins_cursor      = -1;
                         del_cursor      = -1;
               }

               int lese_eti_num (long);
               int lese_eti_num (void);
               int update_eti_num (long);
               int delete_eti_num (long);
               void close_eti_num (void);
               int ShowAll (void);
               int ShowAllBu (HWND, int);
};
