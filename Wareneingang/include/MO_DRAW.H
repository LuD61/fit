#ifndef DRAWOK
#define DRAWOK

class FIGURES
      {
           private :
               POINT hexapoint [6];
               POINT dim3 [20];
               int rand;
               int shad;
               double stretch;
               struct HEXASTRUCT *hexastructs [1000];
               void PrintText (struct HEXASTRUCT *, HDC, char *, int, int,
                               COLORREF, COLORREF, int);
               void PrintHexaText (HDC, HWND, char *);
               void PaintHexaRand (HDC, int);
               void SetSpezColor (COLORREF, COLORREF, HDC);
          public :
               FIGURES ()
               {
                          rand = 10;
                          stretch = 0.7;
                          shad = 100;
               }
               void SetRand (int);
               void DisplayHexaText (HWND hWnd, HDC hdc);
               void PaintHexagon (HDC, int, int, int, COLORREF);
               void PaintHexagon3Dim (HDC, int, int, int, COLORREF, int);
               void DelHexagon (HDC, int, int, int, HBRUSH);
               void DelHexagon3Dim (HDC, int, int, int, HBRUSH, int);
               HWND CreateHexagon (HWND, int, int, int, COLORREF, int);
               void DestroyHexagon (HWND);
               void UpdateHexagon (HWND, HWND);
               void PaintAllHexagon (HWND, HDC);
               void SetHexaFont (HWND, HFONT);
               int MouseInHexagon (HWND, POINT *);
               HFONT GetHexaFont (HWND);
               HWND GetHexaParent (HWND);
};

class BMAP
{
          public :
              BITMAPINFO FAR *bmap;
              HBITMAP hBitmap;
              HBITMAP hBitmapOrg;
              HBITMAP hBitmapZoom;
              HWND bmphWnd;
              BMAP () : hBitmap (NULL), hBitmapZoom (NULL),
                        bmap (NULL)
              {
              }
         
			  void SetBitmap (HBITMAP hBitmap)
			  {
				  this-> hBitmap = hBitmap;
                  hBitmapOrg = hBitmap;
			  }

			  HBITMAP GetBitmap (void)
			  {
				  return hBitmap;
			  }

              void TestEnvironment (char *, char *);
              HBITMAP ReadBitmap (HDC, char *);
              void DestroyBitmap (void);
              void BitmapSize (HDC,POINT *);
              void DrawBitmap (HDC, int, int);
              void DrawBitmapExt (HDC, int, int);
              void DrawBitmap (HDC, HBITMAP, int, int);
              void DrawCompatibleBitmap (HDC, HBITMAP, int, int);
              void DrawCompatibleBitmap (HDC, int, int);
              void DrawBitmapEx (HDC, int , int, DWORD);
              void StrechBitmap (HDC, int, int);
              void StrechBitmap (HDC, HDC, int, int);
              void StrechBitmap (HDC, int, int, double, double);
              void DrawBitmapPart (HDC, int, int, int, int, int, int);
              void DrawBitmapPart (HDC, HBITMAP, 
                                   int, int, int, int, int, int);
              void ZoomBitmap (HDC, int, int, int, int, int, int, double);
              void StrechBitmapMem (HWND, int, int);
			  static HBITMAP PrintBitmapMem (HBITMAP, HBITMAP, COLORREF);
              static HBITMAP LoadBitmap (HINSTANCE, char *, char *);
              static HBITMAP LoadBitmap (HINSTANCE, char *, char *, COLORREF BkColor);
};      

#endif  // DRAWOK
