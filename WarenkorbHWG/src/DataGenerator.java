

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import org.jfree.data.category.DefaultCategoryDataset;

import fit.informixconnector.InformixConnector;


public class DataGenerator {
	
	private InformixConnector informixConnector;


	public DataGenerator (final InformixConnector informixConnector) {
		this.informixConnector = informixConnector;		
	}


	public Warenkorb getValues() throws SQLException, IOException {
		
		final Warenkorb warenkorb = new Warenkorb();
		final String stmt = "SELECT hwg.hwg, hwg_bz1, prozent, qm, profil, gueltig_von, gueltig_bis from hwg, outer hwg_best_wk where hwg.hwg = hwg_best_wk.hwg and hwg.hwg != -1 order by hwg.hwg;";
		
		final ResultSet ordersResultSet = informixConnector.executeQuery(stmt);
		
		while(ordersResultSet.next()) {
			
			final int hwg = ordersResultSet.getInt("hwg");
			String hwg_bz1 = ordersResultSet.getString("hwg_bz1");			
			final double prozent = ordersResultSet.getDouble("prozent");
			final double qm = ordersResultSet.getDouble("qm");
			final int profil = ordersResultSet.getShort("profil");
			final Date gueltig_von = ordersResultSet.getDate("gueltig_von");
			final Date gueltig_bis = ordersResultSet.getDate("gueltig_bis");
			
			final HWG_Warenkorb hwg_Warenkorb = new HWG_Warenkorb(hwg, hwg_bz1, prozent, qm, profil, gueltig_von, gueltig_bis);
			warenkorb.addHWGWarenkrob(hwg_Warenkorb);			
		}		
		return warenkorb;
	}
	
	
	
	public DefaultCategoryDataset getPercentValues(final DefaultCategoryDataset categoryDataset) throws SQLException, IOException {
		
		final String stmt = "SELECT hwg_bz1, prozent from hwg, hwg_best_wk where hwg.hwg = hwg_best_wk.hwg and prozent != 0;";
		
		final ResultSet ordersResultSet = informixConnector.executeQuery(stmt);
		
		while(ordersResultSet.next()) {
			
			String hwg_bz1 = ordersResultSet.getString("hwg_bz1");			
			final double prozent = ordersResultSet.getDouble("prozent");
			categoryDataset.addValue(prozent, hwg_bz1, "HWG");			
		}		
		return categoryDataset;
	}
	
	
	
	public void saveValues(final HWG_Warenkorb hwg_warenkorb, final BarChart chart) {
				
		String deleteStmt = "delete from hwg_best_wk where hwg = " + hwg_warenkorb.getHwg() + ";";
		
		Date gueltig_von = new java.sql.Date(new Date().getTime());
		Date gueltig_bis = new java.sql.Date(new Date().getTime());
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
		String gueltig_von_String = simpleDateFormat.format(gueltig_von);
		String gueltig_bis_String = simpleDateFormat.format(gueltig_bis);
		
		String insertstmt = 
				"INSERT INTO hwg_best_wk" +
				"(hwg," +
				"prozent," +
				"qm," +
				"profil," +
				"gueltig_von," +
				"gueltig_bis" +
				")	VALUES (" +
				hwg_warenkorb.getHwg() + "," +
				hwg_warenkorb.getProzent() + "," +
				hwg_warenkorb.getQm() + "," +
				hwg_warenkorb.getProfil() + ",'" +
				gueltig_von_String + "','" +
				gueltig_bis_String + "'" +
				"); ";
		
		this.informixConnector.executeStatement(deleteStmt);			
		this.informixConnector.executeStatement(insertstmt);
		
		chart.categoryDataset.setValue(hwg_warenkorb.getProzent(), hwg_warenkorb.getHwg_bz1(), "HWG");
			
	}	
}
