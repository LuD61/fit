

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;


public class Warenkorb {
	
	private ArrayList<HWG_Warenkorb> hwg_Warenkorbs = null;
	
	public Warenkorb() {		
		this.hwg_Warenkorbs = new ArrayList<HWG_Warenkorb>();
	}
	
	
	public Iterator<HWG_Warenkorb> getHWG_Warenkorb() {
		
		return this.hwg_Warenkorbs.iterator();
	}
	
	
	public void addHWGWarenkrob(final HWG_Warenkorb hwg_Warenkorb) {
		this.hwg_Warenkorbs.add(hwg_Warenkorb);
	}
	
	
	public int getWarenkorbSize() {
		return this.hwg_Warenkorbs.size();
	}
	
	
	public Object getValueAt(int rowIndex, int columnIndex) {
		
		final HWG_Warenkorb hwg_Warenkorb = this.hwg_Warenkorbs.get(rowIndex);
		
		switch ( columnIndex ) {
		
		case 0:
			return hwg_Warenkorb.getHwg();
		case 1:
			return hwg_Warenkorb.getHwg_bz1();
		case 2:
			return hwg_Warenkorb.getProzent();
		case 3:
			return hwg_Warenkorb.getQm();
		case 4:
			return hwg_Warenkorb.getProfil();
		case 5:
			return hwg_Warenkorb.getGueltig_von();
		case 6:
			return hwg_Warenkorb.getGueltig_bis();			
		}
		
		return "";
	}
	
	
	public void setValueAt(Object newValue, int rowIndex, int columnIndex, final DataGenerator dataGenerator, final BarChart chart) {
		
		final HWG_Warenkorb hwg_Warenkorb = this.hwg_Warenkorbs.get(rowIndex);
		
		switch ( columnIndex ) {
		
		case 0:
			hwg_Warenkorb.setHwg(Integer.valueOf((String) newValue).intValue());
			break;
		case 1:
			hwg_Warenkorb.setHwg_bz1((String) newValue);
			break;
		case 2:
			hwg_Warenkorb.setProzent(Double.valueOf((String) newValue).doubleValue());
			break;
		case 3:
			hwg_Warenkorb.setQm(Double.valueOf((String) newValue).doubleValue());
			break;
		case 4:
			hwg_Warenkorb.setProfil(Integer.valueOf((String) newValue).intValue());
			break;
		case 5:
			hwg_Warenkorb.setGueltig_von((Date) newValue);
			break;
		case 6:
			hwg_Warenkorb.setGueltig_bis((Date) newValue);
			break;
		}
		
		dataGenerator.saveValues(hwg_Warenkorb, chart);
		
	}
	
}
