import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.border.EmptyBorder;
import javax.swing.JTable;
import javax.swing.table.TableModel;
import javax.swing.ListSelectionModel;
import javax.swing.JLabel;
import java.awt.Font;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JLayeredPane;
import javax.swing.JButton;
import javax.swing.UIManager;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

import org.jfree.chart.ChartPanel;

import fit.informixconnector.InformixConnector;


public class WarenkorbDialog extends JFrame {

	private JLayeredPane contentPane;
	private JTable table;
	private JScrollPane scrollPane;
	private static InformixConnector informixConnector;

	/**
	 * Launch the application.
	 */
	
	
	public static void main(String[] args) {
		
		try {
			
			informixConnector = new InformixConnector();			
			informixConnector.createConnection();
		
		
			javax.swing.SwingUtilities.invokeLater(new Runnable() {
		        public void run() {
		            try {
		            	UIManager.setLookAndFeel( "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel" );
		            	
		    			WarenkorbDialog frame = new WarenkorbDialog();
		    			frame.setExtendedState(MAXIMIZED_BOTH);
		    			frame.setVisible(true);	        			
		            } catch (Exception e) {
		                e.printStackTrace();
		            }
		        }
		    });
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	

	/**
	 * Create the frame.
	 */
	public WarenkorbDialog() {		
				
		DataGenerator dataGenerator = new DataGenerator(informixConnector);
		
		setTitle("Warenkorbaufteilung nach Hauptwarengruppen");
		setAlwaysOnTop(true);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1024, 650);
		contentPane = new JLayeredPane();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblBitteHauptwarengruppeAuswhlen = new JLabel("Warenkorbaufteilung nach Hauptwarengruppen");
		lblBitteHauptwarengruppeAuswhlen.setBounds(26, 26, 458, 35);
		lblBitteHauptwarengruppeAuswhlen.setFont(new Font("Dialog", Font.BOLD, 14));
		contentPane.add(lblBitteHauptwarengruppeAuswhlen);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(26, 73, 535, 248);
		contentPane.add(scrollPane);		
		
		table = new JTable();
		table.setColumnSelectionAllowed(true);
		table.setCellSelectionEnabled(true);
		table.setRowSelectionAllowed(false);
		
		BarChart chart = new BarChart("Verteilung Hauptwarengruppen", "Warenkorbdefinition", "Warenkorb", "y", dataGenerator);
		//chart.new ActionTimer(1000).start();
		ChartPanel chartPanel = chart.getChart();
		
		TableModel tableModel = new HWGTableModel(dataGenerator, chart);		
		table.setModel(tableModel);
		scrollPane.setViewportView(table);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setFillsViewportHeight(true);
		
		JButton btnAnwendungBeenden = new JButton("Anwendung beenden");
		btnAnwendungBeenden.setBounds(26, 344, 189, 23);
		btnAnwendungBeenden.setIcon(new ImageIcon(WarenkorbDialog.class.getResource("/javax/swing/plaf/metal/icons/ocean/paletteClose.gif")));
		btnAnwendungBeenden.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				final int returnValue = JOptionPane.showConfirmDialog(contentPane, "M�chten Sie die Anwendung jetzt schlie�en", "Anwendung beenden", JOptionPane.CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
				if ( returnValue == JOptionPane.YES_OPTION ) {
					informixConnector.closeConnection();
					System.exit(0);					
				}
				
			}
		});
		contentPane.add(btnAnwendungBeenden);
		
		JPanel panel = new JPanel();
		panel.setBounds(586, 73, 400, 600);
		panel.setLayout(new BorderLayout());
	    panel.add(chartPanel, BorderLayout.NORTH);
		contentPane.add(panel);
		
	
		
	}
}
