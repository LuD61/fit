

import java.util.Date;



public class HWG_Warenkorb {
	

	private int hwg;
	private String hwg_bz1;
	private double prozent;
	private double qm;
	private int profil;
	private Date gueltig_von;
	private Date gueltig_bis;
	
	
	public HWG_Warenkorb(final int hwg, final String hwg_bz1, final double prozent, final double qm, final int profil, final Date gueltig_von, final Date gueltig_bis) {
		
		this.setHwg(hwg);
		this.setHwg_bz1(hwg_bz1);
		this.setProzent(prozent);
		this.setQm(qm);
		this.setProfil(profil);
		this.setGueltig_von(gueltig_von);
		this.setGueltig_bis(gueltig_bis);		
	}



	public int getHwg() {
		return hwg;
	}



	public void setHwg(int hwg) {
		this.hwg = hwg;
	}



	public String getHwg_bz1() {
		return hwg_bz1;
	}



	public void setHwg_bz1(String hwg_bz1) {
		this.hwg_bz1 = hwg_bz1;
	}



	public double getProzent() {
		return prozent;
	}



	public void setProzent(double prozent) {
		this.prozent = prozent;
	}



	public double getQm() {
		return qm;
	}



	public void setQm(double qm) {
		this.qm = qm;
	}



	public int getProfil() {
		return profil;
	}



	public void setProfil(int profil) {
		this.profil = profil;
	}



	public Date getGueltig_von() {
		return gueltig_von;
	}



	public void setGueltig_von(Date gueltig_von) {
		this.gueltig_von = gueltig_von;
	}



	public Date getGueltig_bis() {
		return gueltig_bis;
	}



	public void setGueltig_bis(Date gueltig_bis) {
		this.gueltig_bis = gueltig_bis;
	}
	
}
