import java.io.IOException;
import java.sql.SQLException;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;


public class HWGTableModel implements TableModel {
	
	private Warenkorb warenkorb;
	private DataGenerator dataGenerator;
	private BarChart chart;
	
	public HWGTableModel (final DataGenerator dataGenerator, final BarChart chart) {		

		Warenkorb warenkorb = null;
		try {
			this.dataGenerator = dataGenerator;
			this.chart = chart;
			warenkorb = this.dataGenerator.getValues();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.warenkorb = warenkorb;
	}

	@Override
	public void addTableModelListener(TableModelListener l) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		// TODO Auto-generated method stub
		return Object.class;
	}

	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return 4;
	}

	@Override
	public String getColumnName(int columnIndex) {
		// TODO Auto-generated method stub
		
		switch(columnIndex) {
		case 0:
			return "Hauptwarengruppe";
		case 1:
			return "Bezeichnung";
		case 2:
			return "%-Anteil am Warenkorb";
		case 3:
			return "m� Angabe";
		}
		return "";
	}

	@Override
	public int getRowCount() {
		
		return this.warenkorb.getWarenkorbSize();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		
		return this.warenkorb.getValueAt(rowIndex, columnIndex);
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		
		if ( columnIndex == 2 || columnIndex == 3 ) {
			return true;
		} else {
			return false;
		}
		
	}

	@Override
	public void removeTableModelListener(TableModelListener l) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {		
		this.warenkorb.setValueAt(aValue, rowIndex, columnIndex, this.dataGenerator, this.chart);		
	}

}
