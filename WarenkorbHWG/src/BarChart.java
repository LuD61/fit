
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.NumberFormat;

import javax.swing.Timer;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.StackedBarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DatasetChangeEvent;
import org.jfree.data.general.DatasetChangeListener;
import org.jfree.data.general.DatasetGroup;
import org.jfree.data.general.DatasetUtilities;
import org.jfree.data.jdbc.JDBCCategoryDataset;
import org.jfree.ui.ApplicationFrame;


public class BarChart extends ApplicationFrame implements DatasetChangeListener {

    private static final long serialVersionUID = 1L;
    public DefaultCategoryDataset categoryDataset = null;
    private ChartPanel chartPanel = null;
    private DataGenerator dataGenerator = null;
    private JFreeChart chart = null;
    

    {
        ChartFactory.setChartTheme(new StandardChartTheme("JFree/Shadow",
                true));
    }

    /**
     * Creates a new bar instance.
     *
     * @param title  the frame title.
     */
    public BarChart(final String title, final String chartTitle, final String domainAxisTitle, final String rangeAxisTitle, final DataGenerator dataGenerator) {
        
    	super(title);
    	this.dataGenerator = dataGenerator;    	
    	this.categoryDataset = new DefaultCategoryDataset();
    	this.categoryDataset.addChangeListener(this);    	
    	this.executeDataSet();
        JFreeChart chart = createChart(this.categoryDataset, chartTitle, domainAxisTitle, rangeAxisTitle);
        
        this.chartPanel = new ChartPanel(chart);
        this.chartPanel.setPreferredSize(new Dimension(300, 500));
        this.chartPanel.setFillZoomRectangle(true);
        this.chartPanel.setMouseWheelEnabled(true);
        this.chartPanel.setDisplayToolTips(true);
        this.chartPanel.setDomainZoomable(true);
        
        setContentPane(this.chartPanel);
    }
    
    
    
    /**
     * Returns a sample dataset.
     *
     * @return The dataset.
     */
    private void executeDataSet() {
    	
    	try {
			this.categoryDataset =  this.dataGenerator.getPercentValues(this.categoryDataset);			
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
    }
    
    
    public ChartPanel getChart() {
    	
    	return this.chartPanel;
    }
    
    

    /**
     * Creates a sample chart.
     *
     * @param dataset  the dataset.
     *
     * @return The chart.
     */
    public JFreeChart createChart(CategoryDataset dataset, final String chartTitle, final String domainAxisTitle, final String rangeAxisTitle) {

        // create the chart...

    	
        this.chart = ChartFactory.createStackedBarChart(
            "",	       							// chart title
            domainAxisTitle,               		// domain axis label
            "%",			           			// range axis label
            dataset,                  			// data
            PlotOrientation.VERTICAL, 			// orientation
            true,                     			// include legend
            true,                     			// tooltips?
            false                     			// URLs?
        );

        // set the background color for the chart...
        chart.setBackgroundPaint(Color.white);

        // get a reference to the plot for further customisation...
        CategoryPlot plot = (CategoryPlot) chart.getPlot();
        

        // set the range axis to display integers only...
        NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setNumberFormatOverride(NumberFormat.getPercentInstance());

        // disable bar outlines...        
        StackedBarRenderer br = new StackedBarRenderer(true); //enable perc. display
        br.setBarPainter(new StandardBarPainter());
        br.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
        br.setBaseItemLabelsVisible(true);
        chart.getCategoryPlot().setRenderer(br);
        //br.setRenderAsPercentages(true);

        CategoryAxis domainAxis = plot.getDomainAxis();
        domainAxis.setCategoryLabelPositions(
                CategoryLabelPositions.createUpRotationLabelPositions(
                        Math.PI / 6.0));

        return chart;

    }
    
//    
//    /**
//    * The data generator.
//    */
//    class ActionTimer extends Timer implements ActionListener {
//    /**
//		 * 
//		 */
//		private static final long serialVersionUID = 1L;
//
//
//	/**
//    * Constructor.
//    *
//    * @param interval the interval (in milliseconds)
//    */
//	    ActionTimer(int interval) {
//	    	super(interval, null);
//	    	addActionListener(this);	    	
//	    }
//    
//
//		@Override
//		public void actionPerformed(ActionEvent e) {
//			executeDataSet();
//		}
//
//    }


	@Override
	public void datasetChanged(DatasetChangeEvent arg0) {
	}

}
