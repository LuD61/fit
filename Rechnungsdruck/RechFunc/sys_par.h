#pragma once
#include "DbClass.h"

class CSys_par
{
private :
	DB_CLASS *db;
	int cursor;
public:
	char sys_par_nam[19] ;
	char sys_par_wrt [3];
	char sys_par_besch [33];
	CSys_par(void);
	~CSys_par(void);
    void SetDb (DB_CLASS *db);
	void Prepare ();
	int Read ();
	void Close ();
};
