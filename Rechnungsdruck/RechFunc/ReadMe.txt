========================================================================
    MICROSOFT FOUNDATION CLASS LIBRARY : RechFunc-Projekt�bersicht
========================================================================


Der Anwendungs-Assistent hat diese RechFunc-DLL erstellt.
Diese DLL zeigt die prinzipielle Anwendung der Microsoft Foundation Classes
und dient als Ausgangspunkt f�r die Erstellung Ihrer eigenen DLL.

Die Datei enth�lt eine Zusammenfassung des Inhalts der Dateien f�r die RechFunc-DLL.

RechFunc.vcproj
    Dies ist die Hauptprojektdatei f�r VC++-Projekte, die vom Anwendungs-Assistenten erstellt wird. 
    Sie enth�lt Informationen �ber die Version von Visual C++, mit der 
    die Datei generiert wurde, �ber die Plattformen, Konfigurationen und Projektfeatures,
    die mit dem Anwendungs-Assistenten ausgew�hlt wurden.

RechFunc.h
    Die ist die Hauptheaderdatei f�r die DLL. Sie deklariert die
    CRechFuncApp-Klasse.

RechFunc.cpp
    Dies ist die wichtigste DLL-Quelldatei. Sie enth�lt die Klasse CRechFuncApp.
RechFunc.rc
    Hierbei handelt es sich um eine Auflistung aller Ressourcen von Microsoft Windows, die
    vom Programm verwendet werden. Sie enth�lt die Symbole, Bitmaps und Cursors, die im
    Unterverzeichnis RES gespeichert sind. Diese Datei l�sst sich direkt in Microsoft
    Visual C++ bearbeiten.

res\RechFunc.rc2
    Diese Datei enth�lt Ressourcen, die nicht mit Microsoft 
    Visual C++ bearbeitet wurden. F�gen Sie alle Ressourcen in diese Datei ein,
    die nicht mit dem Ressourcen-Editor bearbeitet werden k�nnen.

RechFunc.def
    Diese Datei enth�lt Informationen �ber die DLL, die zum
    Ausf�hren mit Microsoft Windows erforderlich ist. Sie definiert Parameter,
    z.B. den Namen und die Beschreibung der DLL. Die Datei exportiert
    Funktionen der DLL.

/////////////////////////////////////////////////////////////////////////////
Weitere Standarddateien:

StdAfx.h, StdAfx.cpp
    Mit diesen Dateien werden vorkompilierte Headerdateien (PCH)
    mit der Bezeichnung RechFunc.pch und eine vorkompilierte Typdatei mit der Bezeichnung StdAfx.obj erstellt.

Resource.h
    Dies ist die Standardheaderdatei, die neue Ressourcen-IDs definiert.
    Die Datei wird mit Microsoft Visual C++ gelesen und aktualisiert.

/////////////////////////////////////////////////////////////////////////////
Weitere Hinweise:

Der Anwendungs-Assistent verwendet "TODO:"-Kommentare, um Teile des Quellcodes anzuzeigen, die hinzugef�gt oder angepasst werden m�ssen.

/////////////////////////////////////////////////////////////////////////////
