// RechFuncDlg.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "RechFunc.h"
#include "RechFuncDlg.h"
#include "ToolbarItem.h"
#include "Bmap.h"
#include "AboutDialog.h"
#include "Rech.h"
#include "Process.h"
#include "Kun.h"
#include ".\rechfuncdlg.h"

// CRechFuncDlg-Dialogfeld

// #define PROGRAMM "cmd /k echo rswrun rechbearb"

 #define PROGRAMM "rswrun rechbearb"

int CRechFuncDlg::SortRow = 0;
int CRechFuncDlg::Sort1 = 1;
int CRechFuncDlg::Sort2 = 1;
int CRechFuncDlg::Sort3 = 1;
int CRechFuncDlg::Sort4 = 1;
DATE_STRUCT SDatum;

IMPLEMENT_DYNAMIC(CRechFuncDlg, CDialog)
CRechFuncDlg::CRechFuncDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CRechFuncDlg::IDD, pParent)
	, m_mdn(1)
	, m_rech(0)
	, m_kun(0)
	, m_date_rech(_T(""))
	, m_date_von(_T(""))
	, m_date_bis(_T(""))
	, m_blg_typ(_T("R"))
{
	char datum [12];

	listView1 = NULL;
	Caption = "Rechnung";
	BlgTyp = "R";
	RechType = RESET;
	sysdate (datum);
	m_date_bis = datum;
	m_date_rech = datum;
	AddDate (datum, -30);
	m_date_von = datum;


}

CRechFuncDlg::~CRechFuncDlg()
{
}

void CRechFuncDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_MDN, m_mdn);
	DDX_Text(pDX, IDC_RECH2, m_rech);
	DDX_Text(pDX, IDC_KUN, m_kun);
	DDX_Text(pDX, IDC_DATE_RECH, m_date_rech);
	DDX_Text(pDX, IDC_DATE_VON, m_date_von);
	DDX_Text(pDX, IDC_DATE_BIS, m_date_bis);
	DDX_Text(pDX, IDC_KUN2, m_blg_typ);
	DDX_Control(pDX, IDC_BLG_TYP, m_BlgTyp);
}


BEGIN_MESSAGE_MAP(CRechFuncDlg, CDialog)
	//{{AFX_MSG_MAP(RechFuncDlg)
	ON_WM_PAINT()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_F5, OnF5)
	ON_BN_CLICKED(IDC_F12, OnF12)
	ON_BN_CLICKED(IDC_F7, OnF7)
	ON_BN_CLICKED(IDC_DELETE, OnDelete)
	ON_BN_CLICKED(IDC_SCHOICE, OnBearbeitenAnzeigen)
	ON_COMMAND(IDC_KEY9, OnKey9)
	ON_BN_CLICKED(IDC_PRINT, OnPrint)
	ON_WM_DRAWITEM()
	ON_NOTIFY_EX(TTN_NEEDTEXT, 0, OnToolTipNotify)
	//}}AFX_MSG_MAP
	ON_COMMAND(ID_DATEI_BEENDEN, OnDateiBeenden)
	ON_COMMAND(ID_DATEI_AUSF, OnDateiAusf)
	ON_COMMAND(ID_BEARBEITEN_ANZEIGEN, OnBearbeitenAnzeigen)
	ON_COMMAND(ID__INFO, OnInfo)
	ON_NOTIFY(LVN_COLUMNCLICK, IDC_LIST1, OnLvnColumnclickList1)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST1, OnNMDblclkList1)
	ON_EN_SETFOCUS(IDC_MDN, OnEnKillfocusMdn)
	ON_EN_KILLFOCUS(IDC_KUN, OnEnKillfocusKun)
	ON_EN_KILLFOCUS(IDC_DATE_VON, OnEnKillfocusdatvon)
	ON_EN_KILLFOCUS(IDC_DATE_BIS, OnEnKillfocusdatbis)
	ON_EN_KILLFOCUS(IDC_DATE_RECH, OnEnKillfocusdatrech)
	ON_EN_CHANGE(IDC_KUN2, OnEnChangeKun2)
	ON_CBN_SELCHANGE(IDC_BLG_TYP, OnCbnSelchangeBlgTyp)
END_MESSAGE_MAP()

void CRechFuncDlg::OnEnKillfocusdatvon()
{
	UpdateData (TRUE);
	DbClass.FromGerDate (&SDatum, m_date_von.GetBuffer (0));
    m_date_von = DbClass.ToGerDate4 (&SDatum, m_date_von.GetBuffer (0));
	CWnd *cWnd = GetDlgItem (IDC_DATE_VON);
	if (cWnd != NULL)
	{
		cWnd->SetWindowText (m_date_von);
	}
}
void CRechFuncDlg::OnEnKillfocusdatbis()
{
	UpdateData (TRUE);
	DbClass.FromGerDate (&SDatum, m_date_bis.GetBuffer (0));
    m_date_von = DbClass.ToGerDate4 (&SDatum, m_date_bis.GetBuffer (0));
	CWnd *cWnd = GetDlgItem (IDC_DATE_BIS);
	if (cWnd != NULL)
	{
		cWnd->SetWindowText (m_date_bis);
	}
}
void CRechFuncDlg::OnEnKillfocusdatrech()
{
	UpdateData (TRUE);
	DbClass.FromGerDate (&SDatum, m_date_rech.GetBuffer (0));
    m_date_von = DbClass.ToGerDate4 (&SDatum, m_date_rech.GetBuffer (0));
	CWnd *cWnd = GetDlgItem (IDC_DATE_RECH);
	if (cWnd != NULL)
	{
		cWnd->SetWindowText (m_date_rech);
	}
}

// CRechFuncDlg-Meldungshandler
BOOL CRechFuncDlg::OnInitDialog()
{
    HICON hiconItem;     // icon for list-view items 
	CDialog::OnInitDialog();

	// Hinzuf�gen des Men�befehls "Info..." zum Systemmen�.

	m_BlgTyp.AddString (_T("R Rechnung"));
	m_BlgTyp.AddString (_T("B Barverkauf"));
// 141113 : Sofortrechnung als Typ gibbet nicht !!!	m_BlgTyp.AddString (_T("S Sofortrechnung"));
	m_BlgTyp.SetCurSel (0);
	m_BlgTyp.SetDroppedWidth (120);
	SetWindowText (Caption.GetBuffer (0)); 
    CButton *Button = (CButton *) GetDlgItem (IDOK);
	if (Button != NULL)
	{
		Button->DestroyWindow ();
	}

    Button = (CButton *) GetDlgItem (IDCANCEL);
	if (Button != NULL)
	{
		Button->DestroyWindow ();
	}

	Menu.LoadMenu (IDR_MENU1);
	SetMenu (&Menu);
    Border.SetType (LOW);
    Border.SetYSpace (10);
    EnableToolTips (TRUE);
    ToolButton.Create (this);
/*
	ToolButton.SetVisible (IDC_PRINT2, FALSE);
	ToolButton.SetVisible (IDC_F7, FALSE);
*/

	ToolButton.Destroy (IDC_PRINT2);
	ToolButton.Destroy (IDC_F7);
	ToolButton.Destroy (IDC_LINE);
	ToolButton.Destroy (IDC_LINE);
    ToolButton.CreateStdToolbar ();

	ToolButton.SetTooltipText ("Ausf�hren", IDC_F12);
  
    CToolbarItem *Tb6 = new CToolbarItem (IDC_SCHOICE, CString (" Anzeigen "), this);
    ToolButton.Add (Tb6); 

    CRect Rect;

    Rect.left = 0;
    Rect.top = 0;
    Rect.right = 0;
    Rect.bottom = 0;

    CForm::HelpName = "16550.cmg";
    int ret = StatusBar.Create (WS_CHILD | WS_VISIBLE | 
                                CBRS_BOTTOM | SBARS_SIZEGRIP, 
                                Rect, this, 5012); 
	int StatusParts [] = {400, -1};
    StatusBar.SetWindowText ("Ready");
	StatusBar.SetParts (2, StatusParts);
	StatusBar.GetWindowRect (&StatusRect);

    hLargeDir = ImageList_Create(GetSystemMetrics(SM_CXICON), 
        GetSystemMetrics(SM_CYICON), ILC_MASK, 1, 1); 
    hSmallDir = ImageList_Create(GetSystemMetrics(SM_CXSMICON), 
        GetSystemMetrics(SM_CYSMICON), ILC_MASK, 1, 1); 

//    hiconItem = LoadIcon(AfxGetApp()->m_hInstance, 
//		                 MAKEINTRESOURCE(IDI_ICON2)); 
    hiconItem = LoadIcon(NULL, 
		                 IDI_APPLICATION); 
    ImageList_AddIcon(hLargeDir, hiconItem); 
    ImageList_AddIcon(hSmallDir, hiconItem); 
    DestroyIcon(hiconItem); 

    hiconItem = LoadIcon(AfxGetApp()->m_hInstance, 
		                 MAKEINTRESOURCE(IDI_ICON2)); 
    ImageList_AddIcon(hLargeDir, hiconItem); 
    ImageList_AddIcon(hSmallDir, hiconItem); 
    DestroyIcon(hiconItem); 

    listView1 = (CListCtrl *) GetDlgItem (IDC_LIST1);
    HWND hWndListView1 = listView1->m_hWnd;

//    ListView_SetImageList(hWndListView1, hLargeDir, LVSIL_NORMAL); 
//    ListView_SetImageList(hWndListView1, hSmallDir, LVSIL_SMALL); 

    DWORD Style = SetStyle (IDC_LIST1, LVS_REPORT);


//    SetWindowText ("Auswahl Mandanten");

    SetCol (IDC_LIST1, "Rechnungs-Nr",       0, 120, LVCFMT_RIGHT);
    SetCol (IDC_LIST1, "Lieferschein-Nr",    1, 120, LVCFMT_RIGHT);
    SetCol (IDC_LIST1, "Lieferdatum"  ,      2, 120, LVCFMT_LEFT);
    SetCol (IDC_LIST1, "Rechnungsdatum"  ,   3, 120, LVCFMT_LEFT);
    DWORD style = listView1->GetExtendedStyle ();
    style |= LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT;
    listView1->SetExtendedStyle (style);

	return TRUE;
}

BOOL CRechFuncDlg::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Speziellen Code hier einf�gen und/oder Basisklasse aufrufen
// 	 CWnd *cWnd;
	 if (pMsg->message == WM_KEYDOWN)
	 {
		 switch (pMsg->wParam)
			 {
		 
		        case VK_RETURN :
                     if (listView1 != (CListCtrl *) GetFocus ())
                     {
          	 		        NextDlgCtrl();
			                return TRUE;
					 }
					 SelectRech ();
					 return TRUE;
			 case VK_F5 :
                     OnCancel ();
 			         return TRUE;
             case VK_F9 :
		             OnKey9 ();
                     break;
		     case VK_F12 :
                     OnF12 ();
				     return TRUE;

 		    case VK_DOWN :
                     if (listView1 != (CListCtrl *) GetFocus ())
                     {
          	  	            NextDlgCtrl();
						    return TRUE;
                     }
				     break;
 		    case VK_UP :
                     if (listView1 != (CListCtrl *) GetFocus ())
                     {
          	  	            PrevDlgCtrl();
							return TRUE;
                     }
				     break;
		 }
	 }
     return CDialog::PreTranslateMessage(pMsg);

}

void CRechFuncDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	// TODO: Code f�r die Behandlungsroutine f�r Nachrichten hier einf�gen und/oder Standard aufrufen

    if (ToolButton.DrawToolbar (nIDCtl, lpDrawItemStruct->hDC)) 
    {
        return;
    }

	CDialog::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

void CRechFuncDlg::OnPrint() 
{
	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen
	
}

void CRechFuncDlg::OnF5() 
{
	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen

    OnCancel ();
}

void CRechFuncDlg::OnF12() 
{
	if (RechType == RESET)
	{
		RechReset ();
	}
	else if (RechType == RESTORE)
	{
		RechRestore ();
	}
}

void CRechFuncDlg::RechReset ()
{
	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen
	CProcess process;

	LPSTR bws = getenv ("BWS");
	if (bws == NULL) return;

	Programm = PROGRAMM;

    if (listView1->GetSelectedCount () < 2)
	{
		UpdateData (TRUE);
		if (m_rech == 0l) return;
		CString Text;
		Text.Format (_T("Rechnung %ld zur�cksetzen ?"), m_rech);
        int ret = MessageBox (Text.GetBuffer (0), "",
                              MB_YESNO | MB_DEFBUTTON2 |
                                         MB_ICONWARNING);
		if (ret == IDNO) return;
	    Command.Format ("%s Z %hd %ld %s", Programm.GetBuffer (0), m_mdn, m_rech, BlgTyp.GetBuffer (0));
		CString Message;
		Message.Format (_T("Rechnung %ld wird zur�ckgesetzt"), m_rech);
        StatusBar.SetWindowText (Message.GetBuffer (0));
        process.SetCommand (Command);
		process.Start ();
		process.WaitForEnd ();
		OnBearbeitenAnzeigen();
        StatusBar.SetWindowText ("Ready");
        return;
	}

    int ret = MessageBox (_T ("Selektierte Rechnungen zur�cksetzen ?"), "",
                              MB_YESNO | MB_DEFBUTTON2 |
                                         MB_ICONWARNING);
	if (ret == IDNO) return;

    int idx   = listView1->GetNextItem (-1, LVNI_SELECTED);
    while (idx != -1)
    {
            CString Text = listView1->GetItemText (idx, 0);
	        m_rech = atol (Text.GetBuffer (0));
	        Command.Format ("%s Z %hd %ld %s", Programm.GetBuffer (0), 
				             m_mdn, m_rech, BlgTyp.GetBuffer (0));
			CString Message;
		    Message.Format (_T("Rechnung %ld wird zur�ckgesetzt"), m_rech);
            StatusBar.SetWindowText (Message.GetBuffer (0));
            process.SetCommand (Command);
  		    process.Start ();
		    process.WaitForEnd ();
            idx   = listView1->GetNextItem (idx, LVNI_SELECTED);
	}
	UpdateData (FALSE);
	OnBearbeitenAnzeigen();
    StatusBar.SetWindowText ("Ready");
}

void CRechFuncDlg::RechRestore ()
{
	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen
	CProcess process;

	LPSTR bws = getenv ("BWS");
	if (bws == NULL) return;

	Programm = PROGRAMM;

    if (listView1->GetSelectedCount () < 2)
	{
		UpdateData (TRUE);
		if (m_rech == 0l) return;
		CString Text;
		Text.Format (_T("Rechnung %ld wiederherstellen ?"), m_rech);
        int ret = MessageBox (Text.GetBuffer (0), "",
                              MB_YESNO | MB_DEFBUTTON2 |
                                         MB_ICONWARNING);
		if (ret == IDNO) return;
		if ( exnutzer[0] != '\0' )	// 160513
			Command.Format ("%s R %hd %ld %s %s %s", Programm.GetBuffer (0), m_mdn, m_rech, BlgTyp.GetBuffer (0),m_date_rech, exnutzer );
		else
			Command.Format ("%s R %hd %ld %s %s", Programm.GetBuffer (0), m_mdn, m_rech, BlgTyp.GetBuffer (0),m_date_rech);
		CString Message;
		Message.Format (_T("Rechnung %ld wird wiederhergestellt"), m_rech);
        StatusBar.SetWindowText (Message.GetBuffer (0));
        process.SetCommand (Command);
		process.Start ();
		process.WaitForEnd ();
		OnBearbeitenAnzeigen();
        StatusBar.SetWindowText ("Ready");
        return;
	}

    int ret = MessageBox (_T ("Selektierte Rechnungen wiederherstellen ?"), "",
                              MB_YESNO | MB_DEFBUTTON2 |
                                         MB_ICONWARNING);
	if (ret == IDNO) return;

    int idx   = listView1->GetNextItem (-1, LVNI_SELECTED);
    while (idx != -1)
    {
            CString Text = listView1->GetItemText (idx, 0);
	        m_rech = atol (Text.GetBuffer (0));
			if ( exnutzer[0] != '\0' )	// 160513
				Command.Format ("%s R %hd %ld %s %s %s", Programm.GetBuffer (0), m_mdn, m_rech, BlgTyp.GetBuffer (0),m_date_rech, exnutzer );
			else
				Command.Format ("%s R %hd %ld %s", Programm.GetBuffer (0),m_mdn, m_rech, BlgTyp.GetBuffer (0));
			CString Message;
		    Message.Format (_T("Rechnung %ld wird wiederhergestellt"), m_rech);
            StatusBar.SetWindowText (Message.GetBuffer (0));
            process.SetCommand (Command);
  		    process.Start ();
		    process.WaitForEnd ();
            idx   = listView1->GetNextItem (idx, LVNI_SELECTED);
	}
	UpdateData (FALSE);
	OnBearbeitenAnzeigen();
    StatusBar.SetWindowText ("Ready");
}

void CRechFuncDlg::OnF7() 
{
	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen

//    Delete ();
}

void CRechFuncDlg::OnKey9 () 
{
	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen

    OnBearbeitenAnzeigen ();
}

void CRechFuncDlg::OnDelete () 
{
	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen

//    Delete ();
}

BOOL CRechFuncDlg::OnToolTipNotify(UINT nIDCtl,NMHDR *pNMHDR, LRESULT *pResult)
{
    if (ToolButton.OnToolTipNotify (nIDCtl, pNMHDR, pResult))
    {
        return TRUE;
    }
    TOOLTIPTEXT *pTTT = (TOOLTIPTEXT *) pNMHDR;
    return FALSE;
}

void CRechFuncDlg::OnDateiBeenden()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	OnCancel ();
}
void CRechFuncDlg::OnPaint() 
{
	{
		CPaintDC cDC (this); // Ger�tekontext f�r Zeichnen
        Border.Print (this, &cDC, 50);
		CDialog::OnPaint();
	}
}

void CRechFuncDlg::OnDateiAusf()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	OnF12 ();
}

void CRechFuncDlg::OnBearbeitenAnzeigen()
{
	char lieferdat [20];
	char rechdat [20];
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.

    UpdateData (TRUE);
	DATE_STRUCT DatRech;
	DATE_STRUCT DatVon;
	DATE_STRUCT DatBis;
    DbClass.FromGerDate (&DatRech, m_date_rech.GetBuffer (0));
    DbClass.FromGerDate (&DatVon, m_date_von.GetBuffer (0));
    DbClass.FromGerDate (&DatBis, m_date_bis.GetBuffer (0));

	RechTab.SetRechType (RechType);
	RechTab.SetQueryFields (m_kun,m_mdn, &DatVon, &DatBis, &DatRech);
	RechTab.Read ();
	CRech *rech;
	int i = 0;
	listView1->DeleteAllItems ();
	RechTab.FirstPosition ();
	while ((rech = (CRech *) RechTab.GetNext ()) != NULL)
	{
		    CString cRech;
			cRech.Format ("%ld", rech->GetRechNr ());
			InsertItem (i, -1);
            SetItemText (cRech.GetBuffer (0), i, 0);
		    CString cLs;
			cLs.Format ("%ld", rech->GetLs ());
            SetItemText (cLs.GetBuffer (0), i, 1);
            DbClass.ToGerDate (rech->GetLieferdat (), lieferdat);
            SetItemText (lieferdat, i, 2);
            DbClass.ToGerDate (rech->GetRechdat (), rechdat);
            SetItemText (rechdat, i, 3);
	}
    for (int i = 0; i < listView1->GetItemCount (); i ++)
    {
           listView1->SetItemData (i, (LPARAM) i);
    }
}


BOOL CRechFuncDlg::InsertItem (int idx, int Image)
{
   LV_ITEM Item;

   Item.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM | LVIF_STATE; 
   Item.state      = 0; 
   Item.stateMask  = 0; 
   Item.iImage     = Image;
   Item.iItem      = idx;
   Item.iSubItem   = 0;
   Item.pszText    = NULL;
   Item.lParam     = (LPARAM) idx;
   return listView1->InsertItem (&Item);
}

BOOL CRechFuncDlg::SetItemText (char *Txt, int idx, int pos)
{

   return listView1->SetItemText (idx, pos, Txt);
}


void CRechFuncDlg::OnInfo()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	CAboutDialog dlg;
	dlg.DoModal ();
}

BOOL CRechFuncDlg::SetCol (DWORD Id, char *Txt, int idx, int Width)
{  
   LV_COLUMN Col;

   CListCtrl *listView = GetListView (Id);
   HWND hWndListView = listView->m_hWnd;

   Col.mask = LVCF_TEXT | LVCF_FMT | LVCF_WIDTH | LVCF_SUBITEM;   
   Col.fmt = LVCFMT_LEFT;
   Col.cx  = Width;
   Col.pszText = Txt;
   Col.iSubItem = idx;
   return ListView_InsertColumn(hWndListView, idx, &Col); 
}

BOOL CRechFuncDlg::SetCol (DWORD Id, char *Txt, int idx, int Width, int Align)
{  
   LV_COLUMN Col;

   CListCtrl *listView = GetListView (Id);
   HWND hWndListView = listView->m_hWnd;

   Col.mask = LVCF_TEXT | LVCF_FMT | LVCF_WIDTH | LVCF_SUBITEM;   
   Col.fmt = Align;
   Col.cx  = Width;
   Col.pszText = Txt;
   Col.iSubItem = idx;
   return ListView_InsertColumn(hWndListView, idx, &Col); 
}

DWORD CRechFuncDlg::SetStyle (DWORD Id, DWORD st)
{
    CListCtrl *listView = (CListCtrl *) GetDlgItem (Id);

    DWORD Style = GetWindowLong (listView->m_hWnd, GWL_STYLE);

    Style &= ~(LVS_ICON | LVS_SMALLICON | LVS_LIST | LVS_REPORT);
    SetWindowLong (listView->m_hWnd, GWL_STYLE, Style |= LVS_REPORT); 
    Style = st;
    ListStyle = st;
    return Style;
}

CListCtrl *CRechFuncDlg::GetListView (DWORD Id)
{
    CListCtrl *listView = (CListCtrl *) GetDlgItem (Id);
    return listView;
}

void CRechFuncDlg::GetItemRect (CRect *Rect, int Id)
{
	CRect pRect;
	CRect cRect;
	CRect pRectC;
	CRect cRectC;

	Rect->top = 0;
	Rect->left = 0;
	Rect->right = 0;
	Rect->bottom = 0;
	GetWindowRect (&pRect);
	CWnd *Item = GetDlgItem (Id);
	GetClientRect (&pRectC);

	if (Item == NULL) return;


	Item->GetWindowRect (cRect);
	Item->GetClientRect (&cRectC);
	Rect->top  = cRect.top  - pRect.top - GetSystemMetrics (SM_CYCAPTION) -
		         GetSystemMetrics (SM_CYSIZEFRAME) - GetSystemMetrics (SM_CYMENUSIZE);
	Rect->left = cRect.left - pRect.left - GetSystemMetrics (SM_CXSIZEFRAME);
	Rect->right = Rect->left + cRectC.right;
	Rect->bottom = Rect->top + cRectC.bottom;
}

void CRechFuncDlg::OnSize(UINT nType, int cx, int cy)
{
	CRect Rect;
	CRect pRect;

	if (listView1 == NULL)
	{
		return;
	}

	GetWindowRect (&pRect);
    StatusRect.top = pRect.bottom - pRect.top - 5;

	StatusBar.MoveWindow (StatusRect.left, 
		                      StatusRect.top,
		                      StatusRect.right - StatusRect.left,
		                      StatusRect.bottom - StatusRect.top,TRUE);
	GetItemRect (&Rect, IDC_MDN);
	int lx = Rect.right + 60;
	int ly = Rect.top;
	int lcx = cx - lx - 20;
	int lcy = cy - ly - 20;
    listView1->MoveWindow (lx,ly, lcx, lcy, TRUE);
}

int CRechFuncDlg::CompareDate (CString& strItem1,CString& strItem2)
{
    SYSTEMTIME st1;
    SYSTEMTIME st2;
    FILETIME ft1;
    FILETIME ft2;
    ULARGE_INTEGER ul1;
	ULARGE_INTEGER ul2;

	char num2[] = {"00"};
	char num4[] = {"0000"};
    memcpy (num2, &strItem1.GetBuffer (0)[0], 2);
	st1.wDay = atoi (num2);
    memcpy (num2, &strItem1.GetBuffer (20)[3], 2);
    st1.wMonth  = atoi (num2);
    memcpy (num4, &strItem1.GetBuffer (20)[6], 4);
	st1.wYear  = atoi (num4);
	st1.wHour  = 0;
	st1.wMinute  = 0;
	st1.wSecond  = 0;
	st1.wMilliseconds = 0;

	SystemTimeToFileTime (&st1, &ft1);
    memcpy (&ul1, &ft1, sizeof (ft1));
    memcpy (num2, &strItem2.GetBuffer (20)[0], 2);
	st2.wDay = atoi (num2);
    memcpy (num2, &strItem2.GetBuffer (20)[3], 2);
	st2.wMonth  = atoi (num2);
    memcpy (num4, &strItem2.GetBuffer (20)[6], 4);
	st2.wYear  = atoi (num4);
	st2.wHour  = 0;
	st2.wMinute  = 0;
	st2.wSecond  = 0;
	st2.wMilliseconds = 0;

	SystemTimeToFileTime (&st2, &ft2);
    memcpy (&ul2, &ft2, sizeof (ft2));
	if (ul1.QuadPart > ul2.QuadPart)
	{
			return 1;
	}
	else if (ul1.QuadPart < ul2.QuadPart)
	{
			return  -1;
	}
    return 0;
 }

int CALLBACK CRechFuncDlg::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   if (SortRow == 0)
   {
	   return atol (strItem1.GetBuffer (0)) -  atol (strItem2.GetBuffer (0)) * Sort1;
   }
   else if (SortRow == 1)
   {
       return (atol (strItem1.GetBuffer (0)) - atol (strItem2.GetBuffer (0))) * Sort2;
   }
   else if (SortRow == 2)
   {
	   return CompareDate (strItem1, strItem2) * Sort3;
   }
   else if (SortRow == 3)
   {
	   return CompareDate (strItem1, strItem2) * Sort4;
   }
   return strcmp(strItem2.GetBuffer (0), strItem1.GetBuffer (0)) * Sort1;
}


void  CRechFuncDlg::Sort (CListCtrl *ListBox)
{
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
              break;
        case 2:
              Sort3 *= -1;
              break;
        case 3:
              Sort4 *= -1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
    }
}

void CRechFuncDlg::OnLvnColumnclickList1(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen
	SortRow = pNMListView->iSubItem;
    Sort (listView1);
	*pResult = 0;
}

void CRechFuncDlg::SetDbase (LPSTR database)
{
	DbClass.opendbase (database);
	RechTab.SetDb (&DbClass);
}


void CRechFuncDlg::OnNMDblclkList1(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	*pResult = 0;
	SelectRech ();
}

void CRechFuncDlg::SelectRech ()
{
    int idx   = listView1->GetNextItem (-1, LVNI_SELECTED);
	if (idx == -1)
	{
		return;
	}

    CString Text = listView1->GetItemText (idx, 0);
	m_rech = atol (Text.GetBuffer (0));
    UpdateData (FALSE);
}

void CRechFuncDlg::OnEnKillfocusKun()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
    ReadKun ();
}
void CRechFuncDlg::OnEnKillfocusMdn()
{
	if (RechType == RESET)
	{
		CWnd *cWnd = GetDlgItem (IDC_DATE_RECH);
		if (cWnd != NULL)
		{
			cWnd->EnableWindow(FALSE);
		}
	}
}

void CRechFuncDlg::ReadKun ()	
{
    CKun Kun;
     
	Kun.SetDb (&DbClass);
	UpdateData (TRUE);
	Kun.mdn = m_mdn;
	Kun.kun = m_kun;
	if (Kun.Read () != 0)
	{
		CString Message;
		Message.Format (_T("Kunde %ld nicht gefunden"), m_kun);
		MessageBox (Message.GetBuffer (0), "", MB_OK | MB_ICONERROR);
		return;
	}
	CWnd *cWnd = GetDlgItem (IDC_KUN_KRZ1);
	if (cWnd != NULL)
	{
		cWnd->SetWindowText (Kun.kun_krz1);
	}
}

int CRechFuncDlg::sysdate (char *datum)
{
 time_t timer;
 struct tm *ltime;

 time (&timer);
 ltime = localtime (&timer);
 sprintf (datum, "%02d.%02d.%04d", ltime->tm_mday,
                                   ltime->tm_mon + 1,
                                   ltime->tm_year + 1900);
 return (0);
}

ULARGE_INTEGER CRechFuncDlg::GetNanoDay ()
{
    SYSTEMTIME st;
    FILETIME ft;
    ULARGE_INTEGER ul1;
    ULARGE_INTEGER ul2;

	st.wDay = 1;
    st.wMonth  = 1;
	st.wYear  = 1989;
	st.wHour  = 0;
	st.wMinute  = 0;
	st.wSecond  = 0;
	st.wMilliseconds = 0;
	SystemTimeToFileTime (&st, &ft);
    memcpy (&ul1, &ft, sizeof (ft));

	st.wDay = 2;
    st.wMonth  = 1;
	st.wYear  = 1989;
	st.wHour  = 0;
	st.wMinute  = 0;
	st.wSecond  = 0;
	st.wMilliseconds = 0;
	SystemTimeToFileTime (&st, &ft);
    memcpy (&ul2, &ft, sizeof (ft));

	ul2.QuadPart -= ul1.QuadPart ;
	return ul2;
}

void CRechFuncDlg::AddDate (char *datum, long days)
{
    SYSTEMTIME st1;
    FILETIME ft1;
    ULARGE_INTEGER ul1;
    ULARGE_INTEGER ulday;

	char num2[] = {"00"};
	char num4[] = {"0000"};
    memcpy (num2, &datum[0], 2);
	st1.wDay = atoi (num2);
    memcpy (num2, &datum[3], 2);
    st1.wMonth  = atoi (num2);
    memcpy (num4, &datum[6], 4);
	st1.wYear  = atoi (num4);
	st1.wHour  = 0;
	st1.wMinute  = 0;
	st1.wSecond  = 0;
	st1.wMilliseconds = 0;

	SystemTimeToFileTime (&st1, &ft1);
    memcpy (&ul1, &ft1, sizeof (ft1));
	ulday = GetNanoDay ();
	ul1.QuadPart += ulday.QuadPart * days;
    memcpy (&ft1, &ul1, sizeof (ft1));
	FileTimeToSystemTime (&ft1, &st1);
	CString Date;
	Date.Format ("%02hd.%02hd.%04hd",st1.wDay, st1.wMonth, st1.wYear);
	strcpy (datum, Date.GetBuffer (0));
}


void CRechFuncDlg::OnEnChangeKun2()
{
	// TODO:  Falls dies ein RICHEDIT-Steuerelement ist, wird das Kontrollelement
	// diese Benachrichtigung nicht senden, es sei denn, Sie setzen den CDialog::OnInitDialog() au�er Kraft.
	// Funktion und Aufruf CRichEditCtrl().SetEventMask()
	// mit dem ENM_CHANGE-Flag ORed in der Eingabe.

	// TODO:  F�gen Sie hier Ihren Code f�r die Kontrollbenachrichtigungsbehandlung ein.
}

void CRechFuncDlg::OnCbnSelchangeBlgTyp()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	int idx = m_BlgTyp.GetCurSel ();
	if (idx != -1)
	{
		CString Buffer;
		m_BlgTyp.GetLBText (idx, Buffer);
		int start = 0;
		BlgTyp = Buffer.Tokenize (" ", start);
	}
}
