#pragma once
#include "DbClass.h"

class CKun
{
private :
	DB_CLASS *db;
	int cursor;
public:
	short mdn;
	long kun;
	char kun_krz1[17];

	CKun(void);
	~CKun(void);
    void SetDb (DB_CLASS *db);
	void Prepare ();
	int Read ();
	void Close ();
};
