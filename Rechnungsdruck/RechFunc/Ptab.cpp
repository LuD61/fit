#include "StdAfx.h"
#include "ptab.h"

CPtab::CPtab() 
{
	this->PtItem = "";
	db = NULL;
	cursor = -1;
}

CPtab::CPtab(CString& PtItem) 
{
	this->PtItem = PtItem;
	db = NULL;
	cursor = -1;
}

CPtab::~CPtab(void)
{
	Close ();
}

void CPtab::SetDb (DB_CLASS *db)
{
	this->db = db;
}

void CPtab::SetPtItem (CString& PtItem)
{
	this->PtItem = PtItem;
}


void CPtab::Prepare ()
{
	if (db == NULL)
	{
		return;
	}

	if (cursor != -1)
	{
		Close ();
	}

	db->sqlin ((char *)  ptitem, SQLCHAR, sizeof (ptitem));
	db->sqlout ((long *) &SamRech.ptlfnr, SQLLONG, 0);
	db->sqlout ((char *) SamRech.ptwert, SQLCHAR, sizeof (SamRech.ptwert));
	db->sqlout ((char *) SamRech.ptbez, SQLCHAR, sizeof (SamRech.ptbez));
	db->sqlout ((char *) SamRech.ptbezk, SQLCHAR, sizeof (SamRech.ptbezk));
	db->sqlout ((char *) SamRech.ptwer1, SQLCHAR, sizeof (SamRech.ptwer1));
	db->sqlout ((char *) SamRech.ptwer2, SQLCHAR, sizeof (SamRech.ptwer2));
    cursor = db->sqlcursor ("select ptlfnr, ptwert, ptbez, ptbezk, "
		                    "ptwer1, ptwer2 from ptabn "
							"where ptitem = ? "
							"order by ptlfnr");
}

int CPtab::Read ()
{
	if (db == NULL)
	{
		return 100;
	}
	if (cursor == -1)
	{
		Prepare ();
	}
	strcpy (ptitem, PtItem.GetBuffer (20));
	CSam_rech *ptabn = new CSam_rech ();
// echt doof wegen rech.sam_rech -Ueberschreibung,- besser:  Einzelstapel erzwingen !!!
	strcpy(SamRech.ptwert,"*");
	strcpy(SamRech.ptbez,"alle");
	strcpy(SamRech.ptbezk,"alle");
	*ptabn = SamRech;
	Add (ptabn);


	db->sqlopen (cursor);
    while (db->sqlfetch (cursor) == 0)
	{
		CSam_rech *ptabn = new CSam_rech ();
		*ptabn = SamRech;
		Add (ptabn);
	}
/*
	if (strcmp(Trim(ptitem),"sam_rech") == 0 
	{
	    while (db->sqlfetch (cursor) == 0)
		{
			CSam_rech *ptabn = new CSam_rech ();
			*ptabn = SamRech;
			Add (ptabn);
		}
	}
	else if (strcmp(Trim(ptitem),"kun_typ") == 0 
	{
	    while (db->sqlfetch (cursor) == 0)
		{
			CKun_typ *ptabn = new CKun_typ ();
			*ptabn = KunTyp;
			Add (ptabn);
		}
	}
	*/
	return 0;
}

void CPtab::Close ()
{
	if (db == NULL)
	{
		return;
	}
	if (cursor != -1)
	{
		db->sqlclose (cursor);
		cursor = -1;
	}
}
