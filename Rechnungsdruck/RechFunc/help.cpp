#include "stdafx.h" 
#include "help.h" 

#ifdef _DEBUG
#include <crtdbg.h>
extern _CrtMemState memory;
#endif

COLORREF HELP::Background = DKYELLOWCOL;

HELP::HELP (char *Item, CStatusBarCtrl *hMainWindow) 
{
          this->hMainWindow = hMainWindow;
          HelpName = "texte.cmd";
          Tk = NULL;
          Init (Item);
}

HELP::HELP (char *Item, CStatusBarCtrl *hMainWindow, BOOL Mode) 
{
         this->hMainWindow = hMainWindow;
         HelpName = "texte.cmd";
         Tk = NULL;
         if (Mode == INITHELP)
         {
                Init (Item);
         }
         else if (Mode == INITCOMM)
         {
                InitComment (Item);
         }
}

HELP::HELP (char *HelpName, char *Item, CStatusBarCtrl *hMainWindow) 
{
         this->hMainWindow = hMainWindow;
         this->HelpName = HelpName;
         Tk = NULL;
         Init (Item);
}

HELP::HELP (char *HelpName, char *Item, CStatusBarCtrl *hMainWindow, BOOL Mode) 
{
         this->hMainWindow = hMainWindow;
         this->HelpName = HelpName;
         Tk = NULL;
         if (Mode == INITHELP)
         {
                Init (Item);
         }
         else if (Mode == INITCOMM)
         {
                InitComment (Item);
         }
}

HELP::~HELP ()
{
         if (Tk != NULL)
         {
             delete Tk;
         }
}

char *HELP::Clipped (char *Str)
{
         CString Itm = Str;
         Itm.TrimRight (); 
         strcpy (Str, Itm.GetBuffer (512)); 
         return Str;
}

char *HELP::GetItpos (char *Item)
{
          int i;
          char It [21];
          char *p;

          Clipped (Item);
          p = head;
          for (i = 0; i < items; i ++, p += 28)
          {
              memcpy (It, p, 20);
              It[20] = 0;
              Clipped (It);
              if (strcmp (It, Item) == 0)
              {
                  return p;
              }
          }
          return NULL;
}

char * HELP::GetItem (char *Item, char *text)
{
         if ((Rows == NULL) || (Rows[0] == NULL))
         {
             text[0] = 0;
         }
         else
         {
             strcpy (text, Rows[0]);
         }
         return text;
}

BOOL HELP::MakeCform (int anz)
{
         int i;
         int len;
         int x;
         int y;

         cx = 0;
         cy = anz;
         for (i = 0; i < anz; i ++)
         {
             char *p = Tk->GetToken (i);
             Clipped (p);
             len = (int)strlen (p);
             if (len > cx)
             {
                 cx = len;
             }
             Rows[i] = new char [len + 1];
             if (Rows[i] == NULL) return FALSE;
             strcpy (Rows[i], p);
         }
         Rows[i] = NULL;

         x = 1; 
         y = 0;
         return TRUE;
}


BOOL HELP::ReadItem (char *HelpName, char *Item)
{
         FILE *fp;
         char *etc;
         char fname [512];
         char *itpos;
         int anz;
         unsigned char t[2] = " ";

         etc = getenv ("BWSETC");
         t[0] = (unsigned char) 0xFF;
         if (etc == NULL) return NULL;
         fp = NULL;
         sprintf (fname, "%s\\%s", etc, HelpName);
         fp = fopen (fname, "rb");
         if (fp == NULL)
         {
             return FALSE;
         }
         hlen = 0;

         if (fread ((long *) &hlen, 1, sizeof (long), fp) <= 0)
         {
             fclose (fp);
             return FALSE;
         }
         head = new char [hlen];
         if (head == NULL)
         {
             fclose (fp);
             return FALSE;
         }

         hlen -= sizeof (long);
         items = hlen / 28;
         if (fread (head, 1, hlen, fp) < (unsigned int) hlen)
         {
             delete head;
             head = NULL;
             fclose (fp);
             return FALSE;
         }
         itpos = GetItpos (Item);
         if (itpos == NULL)
         {
             delete head;
             head = NULL;
             fclose (fp);
             return FALSE;
         }
         itpos += 20;
         memcpy (&offset, (long *) itpos, sizeof (long));
         itpos += sizeof (long);
         memcpy (&dlen, (long *) itpos, sizeof (long));
         fseek (fp, offset, 0);
         itemdata = new char [dlen + 1];
         if (itemdata == NULL)
         {
             delete head;
             head = NULL;
             fclose (fp);
             return FALSE;
         }

         if (fread (itemdata, 1, dlen, fp) == 0)
         if (itemdata == NULL)
         {
             delete itemdata;
             itemdata = NULL;
             delete head;
             head = NULL;
             fclose (fp);
             return FALSE;
         }
         delete head;
         head = NULL;
         fclose (fp);
         itemdata [dlen] = 0; 
         if (Tk != NULL)
         {
             delete Tk;
             Tk = NULL;
         }
         Tk  = new CToken (itemdata, (char *) t);
         anz = Tk->GetAnzToken (); 
         delete itemdata;
         itemdata = NULL;
         if (anz <= 0)
         {
             delete head;
             head = NULL;
             fclose (fp);
             return FALSE;
         }
         Rows = new char *[anz + 1];
         return MakeCform (anz);
}


void HELP::InitComment (char *Item)
{
         Rows     = NULL; 
         head     = NULL; 
         itemdata = NULL; 
         BOOL HelpOK;

         if (HelpName == NULL) 
         {
             return;
         }
         HelpOK = ReadItem (HelpName,Item);
         if ((HelpOK == FALSE) &&
              strcmp (HelpName, "texte.cmd"))
         {
             HelpOK = ReadItem ("texte.cmd", Item);
         }
         if (HelpOK == FALSE)
         {
             HelpOK = ReadItem (HelpName, "nocomm");
         }
         if (HelpOK == FALSE)
         {
             if (ReadItem ("texte.cmd", "nocomm") == FALSE)
             {
                    return;
             }
         }
         if (HelpOK)
         {
             hMainWindow->SetWindowText (Rows[0]);
         }
}

void HELP::Init (char *Item)
{
         BOOL HelpOK;

         HelpOK = ReadItem (HelpName,Item);
         if ((HelpOK == FALSE) &&
              strcmp (HelpName, "texte.cmd"))
         {
             HelpOK = ReadItem ("texte.cmd", Item);
         }
         if (HelpOK == FALSE)
         {
             if (ReadItem (HelpName, "nohelp") == FALSE)
             {
                    return;
             }
         }
/*
         SetStyle (WS_VISIBLE | WS_POPUP | WS_BORDER);
         SetWinBackground (Background);
         if (GetBValue (Background) > 128 && 
             GetRValue (Background) > 128 &&
             GetGValue (Background) > 128)
         {
              dlgposfont.FontColor = BLACKCOL;
         }
         SetDialog (fHelp);
         SetDimension (cx, cy);
         OpenWindow (hInstance, hMainWindow);
         ProcessMessages ();
         DestroyWindow ();
*/
}

BOOL HELP::OnKey (int taste)
{
    return FALSE;
}

BOOL HELP::OnKeyReturn ()
{
    return FALSE;
}

BOOL HELP::OnKey3 ()
{
    return FALSE;
}

BOOL HELP::OnCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
    return FALSE;
}


BOOL HELP::OnSysCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
    return FALSE;
}

BOOL HELP::OnDestroy (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
    return FALSE;
}

void HELP::SetWinBackground (COLORREF Col)
{
}

void HELP::SetBackground (COLORREF Col)
{
          Background = Col;
}



