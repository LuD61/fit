#include "StdAfx.h"
#include "rech.h"

CRech::CRech(void)
{
	rech_nr = 0l;
	kun = 0l;
	ls = 0l;
	memset (&lieferdat, 0, sizeof (lieferdat));
	memset (&rech_dat,  0, sizeof (rech_dat));
	strcpy (blg_typ, "R");
}

CRech::~CRech(void)
{
}

void CRech::SetRechNr (long rech_nr)
{
	this->rech_nr = rech_nr;
}
long CRech::GetRechNr ()
{
	return rech_nr;
}
void CRech::SetKun (long kun)
{
	this->kun = kun;
}

long CRech::GetKun ()
{
	return kun;
}

void CRech::SetLs (long ls)
{
	this->ls = ls;
}

long CRech::GetLs ()
{
	return ls;
}

void CRech::SetBlgTyp (LPSTR blg_typ)
{
	memcpy (this->blg_typ, blg_typ, 1);
}

LPSTR CRech::GetBlgTyp ()
{
	return blg_typ;
}

void CRech::SetLieferdat (DATE_STRUCT *lieferdat)
{
	memcpy (&this->lieferdat, lieferdat, sizeof (DATE_STRUCT));
}
DATE_STRUCT *CRech::GetLieferdat ()
{
	return &lieferdat;
}
void CRech::SetRechdat (DATE_STRUCT *rech_dat)
{
	memcpy (&this->rech_dat, rech_dat, sizeof (DATE_STRUCT));
}
DATE_STRUCT *CRech::GetRechdat ()
{
	return &rech_dat;
}
