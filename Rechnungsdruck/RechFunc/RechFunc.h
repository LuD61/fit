// RechFunc.h : Hauptheaderdatei f�r die RechFunc DLL
//

#pragma once

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// Hauptsymbole


// CRechFuncApp
// Siehe RechFunc.cpp f�r die Implementierung dieser Klasse
//

// extern "C" BOOL PASCAL EXPORT RechStart();
#define EXPORT extern "C" _declspec (dllexport)

 EXPORT bool RechStart(char*);	// 160513 
 EXPORT bool RechRestoreStart(char*);	// 160513
 EXPORT bool RechPrintStart(char*);	// 160513
 EXPORT bool RechNachDruckStart(char*);	// 141113


class CRechFuncApp : public CWinApp
{
public:
	CRechFuncApp();

// �berschreibungen
public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
};

extern char exnutzer[99] ;	// 160513
