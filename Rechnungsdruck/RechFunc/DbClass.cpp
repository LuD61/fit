#include "stdafx.h"
#include "DbClass.h"
#include "Token.h"

short DB_CLASS::ShortNull = (short) 0x8000;
long DB_CLASS::LongNull = (long) 0x80000000;
double DB_CLASS::DoubleNull = (double) 0xffffffffffffffff;
CString DB_CLASS::mainname;
HENV DB_CLASS::mainhenv = NULL;
HDBC DB_CLASS::mainhdbc = NULL;

int DB_CLASS::FromRecDate (DATE_STRUCT *sqldate, char *cdate)
{
       char day [3];
       char month [3];
       char year [5];
	   int len;

	   sqldate->day = 0; 
	   sqldate->month = 0; 
	   sqldate->year = 0; 

	   len = (int) strlen (cdate);
	   if (len < 6)
	   {
                      return (0);
	   }
       strcpy (day,   "01"); 
       strcpy (month, "01"); 
       strcpy (year,  "1900"); 
	   memcpy (day, &cdate[0], 2);
	   memcpy (month, &cdate[2], 2);
	   if (len == 8)
	   {
         	   memcpy (year, &cdate[4], 4);
	   }
	   else if (len == 6)
	   {
		       memcpy (&year[2], &cdate[4], 2);
			   if (atoi (&year[2]) < 80)
			   {
				   memcpy (year, "20", 2);
			   }
	   }


	   sqldate->day = atoi (day); 
	   sqldate->month = atoi (month); 
	   sqldate->year = atoi (year);
	   return (0);
}
		

int DB_CLASS::FromOdbcDate (DATE_STRUCT *sqldate, char *cdate)
{
	   int anz;

	   sqldate->day = 0; 
	   sqldate->month = 0; 
	   sqldate->year = 0; 
	   CToken t;
	   t.SetSep ("-");
	   t = cdate;
	   anz = t.GetAnzToken ();
	   if (anz < 3)
	   {
                      return FromRecDate (sqldate, cdate);
	   }

	   sqldate->day = atoi (t.GetToken (2)); 
	   sqldate->month = atoi (t.GetToken (1)); 
	   sqldate->year = atoi (t.GetToken (0));
	   return (0);
}
		
int DB_CLASS::FromGerDate (DATE_STRUCT *sqldate, char *cdate)
{
	   int anz;
	   sqldate->day = 0; 
	   sqldate->month = 0; 
	   sqldate->year = 0; 
	   CToken t;
	   t.SetSep (".");
	   t = cdate;
	   anz = t.GetAnzToken ();
	   if (anz < 3)
	   {
               return (FromOdbcDate (sqldate, cdate));
	   }

	   sqldate->day = atoi (t.GetToken (0)); 
	   sqldate->month = atoi (t.GetToken (1)); 
	   sqldate->year = atoi (t.GetToken (2));
	   return (0);
}

int DB_CLASS::ToGerDate (DATE_STRUCT *sqldate, char *cdate)
{
	   sprintf (cdate, "%02hd.%02hd.%02hd", sqldate->day,
		                                    sqldate->month,
											sqldate->year);
	   return (0);
}
char* DB_CLASS::ToGerDate4 (DATE_STRUCT *sqldate, char *cdate)
{

	if (sqldate->year < 1000)
	{
		sqldate->year = sqldate->year + 2000;
	}
	   sprintf (cdate, "%02hd.%02hd.%02hd", sqldate->day,
		                                    sqldate->month,
											sqldate->year);
	   return (cdate);
}

int DB_CLASS::FromOdbcTime (TIME_STRUCT *sqltime, char *ctime)
{
	   int anz;

	   sqltime->hour = 0; 
	   sqltime->minute = 0; 
	   sqltime->second = 0; 
	   CToken t;
	   t.SetSep ("-");
	   t = ctime;
	   anz = t.GetAnzToken ();
	   if (anz < 3)
	   {
               return (0);
	   }

	   sqltime->hour = atoi (t.GetToken (2)); 
	   sqltime->minute = atoi (t.GetToken (1)); 
	   sqltime->second = atoi (t.GetToken (0));
	   return (0);
}

int DB_CLASS::FromGerTime (TIME_STRUCT *sqltime, char *ctime)
{
	   int anz;

	   sqltime->hour = 0; 
	   sqltime->minute = 0; 
	   sqltime->second = 0; 
	   CToken t;
	   t.SetSep (":");
	   t = ctime;
	   anz = t.GetAnzToken ();
	   if (anz < 3)
	   {
               return (FromOdbcTime (sqltime, ctime));
	   }

	   sqltime->hour   = atoi (t.GetToken (0)); 
	   sqltime->minute = atoi (t.GetToken (1)); 
	   sqltime->second = atoi (t.GetToken (2));
	   return (0);
}

int DB_CLASS::ToGerTime (TIME_STRUCT *sqltime, char *ctime)
{
	   sprintf (ctime, "%02hd:%02hd:%02hd", sqltime->hour,
		                                    sqltime->minute,
											sqltime->second);
	   return (0);
}

int DB_CLASS::FromOdbcTimestamp (TIMESTAMP_STRUCT *sqltime, char *ctime)
{
       DATE_STRUCT dt;
       TIME_STRUCT tm; 

	   sqltime->day = 0; 
	   sqltime->month = 0; 
	   sqltime->year = 0; 
	   sqltime->hour = 0; 
	   sqltime->minute = 0; 
	   sqltime->second = 0; 
           if (strlen (ctime) < 19)
           {
                    return (0);
           }
           FromOdbcDate (&dt, ctime);
           FromOdbcTime (&tm, &ctime[11]);

	   sqltime->day    = dt.day; 
	   sqltime->month  = dt.month; 
	   sqltime->year   = dt.year;
	   sqltime->hour   = tm.hour; 
	   sqltime->minute = tm.minute; 
	   sqltime->second = tm.second;
	   return (0);
}

int DB_CLASS::FromGerTimestamp (TIMESTAMP_STRUCT *sqltime, char *ctime)
{

       DATE_STRUCT dt;
       TIME_STRUCT tm; 

	   sqltime->day = 0; 
	   sqltime->month = 0; 
	   sqltime->year = 0; 
	   sqltime->hour = 0; 
	   sqltime->minute = 0; 
	   sqltime->second = 0; 
           FromGerDate (&dt, ctime);
	   sqltime->day    = dt.day; 
	   sqltime->month  = dt.month; 
	   sqltime->year    = dt.year;
           if (strlen (ctime) < 11)
           {
                return (0);
           }
           FromGerTime (&tm, &ctime[10]);

	   sqltime->hour   = tm.hour; 
	   sqltime->minute = tm.minute; 
	   sqltime->second = tm.second;
	   return (0);
}

int DB_CLASS::ToGerTimestamp (TIMESTAMP_STRUCT *sqltime, char *ctime)
{
	   sprintf (ctime, "%02hd.%02hd.%02hd %02hd:%02hd:%02hd ",
                                            sqltime->day,
		                                    sqltime->month,
											sqltime->year,
                                            sqltime->hour,
		                                    sqltime->minute,
											sqltime->second);
	   return (0);
}

BOOL DB_CLASS::opendbase (char *dbase)
{
     name = dbase;

     int retcode = SQLAllocEnv(&henv);
     if (retcode == SQL_SUCCESS); 
	 else
	 {
             GetError (NULL);
             return FALSE;
	 }
     DBase.Name = name;
     DBase.henv = henv;

     retcode = SQLAllocConnect(henv, &hdbc); /* Connection handle */

     if (retcode == SQL_SUCCESS); 
	 else
	 {
             GetError (NULL);
             return FALSE;
	 }
        /* Connect to data source */

     retcode = SQLConnect(hdbc, (UCHAR *) dbase, SQL_NTS, 
		                        (UCHAR *) "", SQL_NTS, 
								(UCHAR *) "", SQL_NTS);
     DBase.hdbc = hdbc;
	 mainname = name;
	 mainhenv = henv;
	 mainhdbc = hdbc;
	 OwnConnect = TRUE;
     if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO);
     else
     {
             GetError (NULL);
             return FALSE;
     }
     sqlstatus = 0;
     sqlcomm ("set isolation to dirty read");  
     return TRUE;
}


BOOL DB_CLASS::closedbase (char *dbase)
/**
Datenbank schliessen.
**/

{
	 if (OwnConnect == FALSE)
	 {
		 return FALSE;
	 }

	 if (hdbc == mainhdbc)
	 {
		 closedbase ();
	 }
	 else
	 {
        if (hdbc != NULL)
		{
              SQLDisconnect(hdbc);
              SQLFreeConnect(hdbc);
		}
        if (hdbc != NULL)
		{
              SQLFreeEnv(henv);
		}
	 }
     hdbc = NULL;
     henv = NULL;
     return TRUE;
}


BOOL DB_CLASS::closedbase ()
/**
Datenbank schliessen.
**/

{
	 if (mainhdbc != NULL)
	 {
           SQLDisconnect(mainhdbc);
           SQLFreeConnect(mainhdbc);
	 }
	 if (mainhenv != NULL)
	 {
           SQLFreeEnv(mainhenv);
	 }
     mainhdbc = NULL;
     mainhenv = NULL;
     return TRUE;
}

int DB_CLASS::sqlconnect (char *server, char *user, char *passw)
{

     int retcode = SQLAllocEnv(&henv);
     if (retcode == SQL_SUCCESS); 
	 else
	 {
		     printf ("Fehler bei SQLAllocEnv\n");
		     exit (1);
	 }
     retcode = SQLAllocConnect(henv, &hdbc); /* Connection handle */

     if (retcode == SQL_SUCCESS); 
	 else
	 {
		     printf ("Fehler bei SQLAllocConnect\n");
		     exit (1);
	 }
     return (0);
}


int DB_CLASS::sqlconnectdbase (char *server, char *user, char *passw, char *dbase)
{
     opendbase (dbase);
     return sqlstatus;
}

int DB_CLASS::beginwork (void)
{
    if (InWork)
    {
        commitwork ();
    }
    sqlcomm ("begin work");
    InWork = TRUE;
    return 0;
}

int DB_CLASS::commitwork (void)
{
    if (InWork)
    {
        sqlcomm ("commit work");
//        SQLTransact (henv, hdbc, SQL_COMMIT);
        InWork = FALSE;
    }
    return 0;
}

int DB_CLASS::rollbackwork (void)
{
    if (InWork)
    {
        sqlcomm ("rollback work");
//        SQLTransact (henv, hdbc, SQL_ROLLBACK);
        InWork = FALSE;
    }
    return 0;
}

int DB_CLASS::sqlcomm (char *statement)
{
	 int retcode = SQLAllocStmt (hdbc, &hstmDirect);
     if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
	 {
		     return -1;
	 }

     retcode = TestOut (hstmDirect);
     retcode = TestIn (hstmDirect);

     if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
	 {
		     return -1;
	 }
     retcode = SQLExecDirect((HSTMT) hstmDirect, 
		                  (unsigned char *) statement,
						  (SDWORD) strlen ((char *) statement));
     if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
     {
             GetError (hstmDirect);
             return -1;
     }

     CString St = statement;
     St.MakeUpper ();
     if (St.Find ("SELECT") > -1)
     {
             retcode = SQLFetch(hstmDirect); 
             if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) 
             {
	                 sqlstatus = 0; 
             }
             else if (retcode == SQL_NO_DATA)
             {
                     sqlstatus = 100;
             }
             else
             { 
                    GetError (hstmDirect);
                    return -1;  
             }
     }
     return sqlstatus;
}

int DB_CLASS::sqlcursor (char *statement)
{
    int cursor;

    for (cursor  = 0; cursor < MAXCURS; cursor ++)
    {
        if (CursTab[cursor] == 0)
        {
            CursTab[cursor] = 1;
            break;
        }
    }

	 int retcode = SQLAllocStmt (hdbc, &HstmtTab[cursor]);
     if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
	 {
             GetError (NULL);
             CursTab[cursor] = 0;
		     return -1;
	 }

     HSTMT Cursor = HstmtTab[cursor];
     retcode = SQLPrepare(Cursor, 
		                  (UCHAR *)statement,
						  (SDWORD) strlen ((char *) statement));
     if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
	 {
             GetError (Cursor);
             CursTab[cursor] = 0;
		     return -1;
	 }

     retcode = TestIn (Cursor);
     if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
	 {
             GetError (Cursor);
             CursTab[cursor] = 0;
		     return -1;
	 }

     retcode = TestOut (Cursor);

     if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
	 {
             GetError (Cursor);
             CursTab[cursor] = 0;
		     return -1;
	 }
     return cursor;
}



int DB_CLASS::sqlopen (int cursor)
{
     if (cursor < 0)
     {
         return -1;
     }

     if (CursTab[cursor] == 0)
     {
         return -1;
     }

     HSTMT Cursor = HstmtTab[cursor];


  	 int retcode = SQLFreeStmt (Cursor, SQL_CLOSE);
     if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO)
     {
                 sqlstatus = 0;
     } 
     else
     {
                 GetError (Cursor);
 		         sqlstatus = 0 - retcode;
                 return -1;
     }


     retcode = SQLExecute(Cursor); 
     if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
     {
                 GetError (Cursor);
  		         sqlstatus = 0 - retcode;
                 return -1;
     } 

     CursTab[cursor] = 2;
     return sqlstatus;
}


int DB_CLASS::sqlfetch (int cursor)
{
 
        int dsqlstatus;
        int retcode;

        if (cursor < 0)
        {
             return -1;
        }

        if (CursTab[cursor] == 0)
        { 
             return -1;
        }

        if (CursTab [cursor] == 1)
        {
                    sqlopen (cursor);
        }
        HSTMT Cursor = HstmtTab[cursor];

        retcode = SQLFetch(Cursor); 
        if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) 
        {
	           sqlstatus = 0; 
        }
        else if (retcode == SQL_NO_DATA)
        {
                 sqlstatus = 100;
        }
        else
        {
                 GetError (Cursor);
                 return -1;  
        }
        dsqlstatus = sqlstatus;

        return sqlstatus;
}


int DB_CLASS::sqlexecute (int cursor)
/**
Cursor oeffnen.
**/
{
        if (cursor < 0)
        {
             return -1;
        }

        if (CursTab[cursor] == 0)
        { 
             return -1;
        }

        HSTMT Cursor = HstmtTab[cursor];

  	    int retcode = SQLFreeStmt (Cursor, SQL_CLOSE);
        if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO)
		{
                 sqlstatus = 0;
		} 
        else
		{
                 GetError (Cursor);
 		         sqlstatus = 0 - retcode;
                 return -1;
		}
        retcode = SQLExecute(Cursor); 
        if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO)
        {
                 sqlstatus = 0;
        } 
	    else
        {
                 GetError (Cursor);
                 return -1;
        }
        return sqlstatus;
}


int DB_CLASS::sqlclose (int cursor)
/**
Speicherbereiche fuer sqlerte freigeben.
**/
{
     if (CursTab[cursor] < 0 ||
         CursTab[cursor] > 2)
     {
         return -1;
     }

     HSTMT Cursor = HstmtTab[cursor];
     SQLFreeStmt(Cursor, SQL_DROP);
	 CursTab[cursor] = 0;
     return (0);
}



void DB_CLASS::sqlout (void *var, int typ, int len)
{
     if (OutAnz == MAXVARS)
     {
         return;
     }

     OutVars[OutAnz].var = var;
     OutVars[OutAnz].len = len;
     OutVars[OutAnz].typ = typ;
     OutAnz ++;
}


void DB_CLASS::sqlin (void *var, int typ, int len)
{
     if (InAnz == MAXVARS)
     {
         return;
     }

     InVars[InAnz].var = var;
     InVars[InAnz].len = len;
     InVars[InAnz].typ = typ;
     InAnz ++;
}


static SDWORD pcbValue;

int DB_CLASS::TestOut (HSTMT Cursor)
{
     int retcode = 0;

     for (int i = 0; i < OutAnz; i ++)
     {
         switch (OutVars[i].typ)
         {
                 case SQLCHAR:
                        retcode = SQLBindCol(Cursor, i + 1, 
                                         SQL_CHAR,  
                                         (char *) OutVars[i].var, 
                                                  OutVars[i].len, 
                                                  &pcbValue); 
                        break;
                 case SQLSHORT:
                        retcode = SQLBindCol(Cursor, i + 1, 
                                         SQL_C_SHORT,  
                                         (short *) OutVars[i].var, 
                                                   sizeof (short), 
                                                   &pcbValue); 
                        break;
                 case SQLLONG:
                        retcode = SQLBindCol(Cursor, i + 1, 
                                         SQL_C_LONG,  
                                         (long *) OutVars[i].var, 
                                                   sizeof (long), 
                                                   &pcbValue); 
                        break;
                 case SQLDOUBLE:
                        retcode = SQLBindCol(Cursor, i + 1, 
                                         SQL_DOUBLE,  
                                         (double *)  OutVars[i].var, 
                                                     sizeof (double), 
                                                     &pcbValue); 
                        break;
                 case SQLDATE:
                        retcode = SQLBindCol(Cursor, i + 1, 
                                         SQL_C_DATE,  
                                         (long *)  OutVars[i].var, 
                                                   sizeof (long), 
                                                   &pcbValue); 
                        break;
         }
     }
     OutAnz = 0;
     return retcode;
}


int DB_CLASS::TestIn (HSTMT Cursor)
{
     int retcode = 0;

     for (int i = 0; i < InAnz; i ++)
     {
         switch (InVars[i].typ)
         {
                 case SQLCHAR:
                        retcode = SQLBindParameter(Cursor, i + 1, SQL_PARAM_INPUT,
                                         SQL_C_CHAR, SQL_CHAR, InVars[i].len, 0,  
                                         (char *) InVars[i].var, 
                                                  0, 
                                                  &cbLen); 
                        break;
                 case SQLSHORT:
                        retcode = SQLBindParameter(Cursor, i + 1, SQL_PARAM_INPUT,
                                         SQL_C_SSHORT, SQL_SMALLINT, 0, 0,  
                                         (short *) InVars[i].var, 
                                                   sizeof (short), 
                                                   &cbLen); 
                        break;
                 case SQLLONG:
                        retcode = SQLBindParameter(Cursor, i + 1, SQL_PARAM_INPUT,
                                         SQL_C_SLONG, SQL_INTEGER, 0, 0,  
                                         (long *) InVars[i].var, 
                                                   0, 
                                                   &cbLen); 
                        break;
                 case SQLDOUBLE:
                        retcode = SQLBindParameter(Cursor, i + 1, SQL_PARAM_INPUT,
                                         SQL_C_DOUBLE, SQL_DOUBLE, 0, 0,   
                                         (double *)  InVars[i].var, 
                                                     0, 
                                                     &cbLen); 
                        break;
                 case SQLDATE:
                        retcode = SQLBindParameter(Cursor, i + 1, SQL_PARAM_INPUT,
                                         SQL_C_DATE, SQL_DATE, 0, 0,   
                                         (DATE_STRUCT *)  InVars[i].var, 
                                                     0, 
                                                     &cbLen); 
                        break;
         }
     }
     InAnz = 0;
     return retcode;
}


void DB_CLASS::GetError	(HSTMT Cursor)
{ 
     UCHAR szSqlState[512];
     SDWORD pfNativeError;
     UCHAR szErrorMsg [512]; 

     int retcode = SQLError(henv, 
                             hdbc, 
                             Cursor, 
                             szSqlState, &pfNativeError,
                             szErrorMsg, 512, NULL);
     CString ErrText;
     ErrText.Format ("Fehler %ld\n%s", pfNativeError, szErrorMsg);
 
     if (sql_mode == 0)
     {
            MessageBox (NULL, ErrText, "", MB_ICONERROR);
            ErrText = szErrorMsg;
            (*SqlErrorProc) (pfNativeError, ErrText);
     }
}

BOOL DB_CLASS::ErrProc	(SDWORD ErrStatus, CString& ErrText)
{
    ExitProcess (ErrStatus);
    return TRUE;
}

int DB_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
             prepare ();
         }
         sqlopen (cursor);
         sqlfetch (cursor);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int DB_CLASS::dbread (void)
/**
Naechsten Satz aus Tabelle lesen.
**/
{
         sqlfetch (cursor);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int DB_CLASS::dbupdate (void)
/**
Tabelle eti Updaten.
**/
{

         if (test_upd_cursor == -1)
         {
             prepare ();
         }
         sqlopen (test_upd_cursor);
         sqlfetch (test_upd_cursor);
         if (sqlstatus == 100)
         {
                   sqlexecute (ins_cursor);
         }  
         else if (sqlstatus == 0)
         {
                   sqlexecute (upd_cursor);
         }  
          
         return sqlstatus;
} 

int DB_CLASS::dblock (void)
/**
Tabelle eti Updaten.
**/
{

         if (test_upd_cursor == -1)
         {
             prepare ();
         }
         sqlopen (test_upd_cursor);
         sqlfetch (test_upd_cursor);
          
         return sqlstatus;
} 

int DB_CLASS::dbdelete (void)
/**
Tabelle eti lesen.
**/
{

         if (test_upd_cursor == -1)
         {
             prepare ();
         }
         sqlopen (test_upd_cursor);
         sqlfetch (test_upd_cursor);
         if (sqlstatus == 0)
         {
                      sqlexecute (del_cursor);
         }
         return sqlstatus;
}

void DB_CLASS::dbclose (void)
/**
Cursor fuer eti schliessen.
**/
{
         if (cursor == -1) return;

         sqlclose (cursor); 
         sqlclose (upd_cursor); 
         sqlclose (ins_cursor); 
         sqlclose (del_cursor); 
         sqlclose (test_upd_cursor);

         cursor = -1;
         upd_cursor = -1;
         ins_cursor = -1;
         del_cursor = -1;
         test_upd_cursor = -1;
         cursor_ausw = -1;
}

int DB_CLASS::dbmove (int mode)
/**
Scroll-Cursor lesen.
**/
{
         int scrollakt;

         if (cursor_ausw == -1)
         {
                    return (-1);
         }

         scrollakt = scrollpos;
         switch (mode)
         {
             case FIRST :
                         scrollpos = 1;
                         break;
             case NEXT :     
                         scrollpos ++;
                         break;
             case PRIOR :
                         if (scrollpos > 1)
                         {
                                 scrollpos --;
                         }
                         break;
//             case LAST :      
             case CURRENT :
                         break;
             default :
                   return (-1);
         }
//         fetch_scroll (cursor_ausw, mode);
         if (sqlstatus != 0)
         {
             scrollpos = scrollakt;
         }
         return (sqlstatus);
}

int DB_CLASS::dbmove (int mode, int pos)
/**
Scroll-Cursor lesen.
**/
{
         int scrollakt;

         if (cursor_ausw == -1)
         {
                    return (-1);
         }
         scrollakt = scrollpos;
         switch (mode)
         {
             case DBABSOLUTE :      
             case RELATIVE :      
                    break;
             default :
                   return (-1);
         }
//         fetch_scroll (cursor_ausw, mode, pos);
         if (sqlstatus != 0)
         {
             scrollpos = scrollakt;
         }
         return (sqlstatus);
}

int DB_CLASS::dbcanmove (int mode)
/**
Scroll-Cursor testen.
**/
{
         int status; 

         if (cursor_ausw == -1)
         {
                    return (-1);
         }

         switch (mode)
         {
             case FIRST :
                         scrollpos = 1;
                         break;
             case NEXT :     
                         scrollpos ++;
                         break;
             case PRIOR :
                         if (scrollpos > 1)
                         {
                                 scrollpos --;
                         }
                         break;
//             case LAST :      
             case CURRENT :
                         break;
             default :
                   return (-1);
         }
//         fetch_scroll (cursor_ausw, mode);
         if (sqlstatus != 0)
         {
             status = FALSE;
         }
         else
         {
             status = TRUE;
         }
         dbmove (DBABSOLUTE, scrollpos);
         return (status);
}

int DB_CLASS::dbcanmove (int mode, int pos)
/**
Scroll-Cursor lesen.
**/
{
         int status;

         if (cursor_ausw == -1)
         {
                    return (-1);
         }
         switch (mode)
         {
             case DBABSOLUTE :      
             case RELATIVE :      
                    break;
             default :
                   return (-1);
         }
//         fetch_scroll (cursor_ausw, mode, pos);
         if (sqlstatus != 0)
         {
             status = FALSE;
         }
         else
         {
             status = TRUE;
         }
         dbmove (DBABSOLUTE, scrollpos);
         return (status);
}

