#include "StdAfx.h"
#include "kun.h"

CKun::CKun(void)
{
	mdn = 1;
	kun = 0;
	strcpy (kun_krz1 , "");
	db = NULL;
	cursor = -1;
}

CKun::~CKun(void)
{
	if (db == NULL) return;
	if (cursor == -1) return;
	Close ();
}

void CKun::SetDb (DB_CLASS *db)
{
	this->db = db;
}

void CKun::Prepare ()
{
	if (db == NULL) return;
	if (cursor != -1)
	{
		Close ();
	}

	db->sqlin ((short *)&mdn, SQLSHORT, 0);
	db->sqlin ((long *) &kun, SQLLONG, 0);
	db->sqlout ((char *) kun_krz1, SQLCHAR, sizeof (kun_krz1));
	cursor = db->sqlcursor ("select adr.adr_krz from kun, adr "
		                    "where adr.adr = kun.adr1 "
							"and kun.mdn = ? "
							"and kun.kun = ?");
}

int CKun::Read ()
{
	if (db == NULL) return 100;

	if (cursor == -1)
	{
		Prepare ();
	}

	db->sqlopen (cursor);
	return db->sqlfetch (cursor);
}


void CKun::Close ()
{
	if (db != NULL && cursor != -1)
	{
		db->sqlclose (cursor);
		cursor = -1;
	}
}


