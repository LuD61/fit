#pragma once
#include "afxwin.h"


// CNachdruck-Dialogfeld

class CNachdruck : public CDialog
{
	DECLARE_DYNAMIC(CNachdruck)

public:
	CNachdruck(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CNachdruck();

// Dialogfelddaten
	enum { IDD = IDD_NACHDRUCK };

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL PreTranslateMessage(MSG* pMsg);


	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedCheckn();
	afx_msg void OnBnClickedChecks();
	afx_msg void OnBnClickedCheckr();
	afx_msg void OnBnClickedCheckb();
	afx_msg void OnBnClickedCheckg();
	CButton mb_checkn;
	CButton mb_checks;
	CButton mb_checkr;
	CButton mb_checkb;
	CButton mb_checkg;
	BOOL vb_checkn;
	BOOL vb_checks;
	BOOL vb_checkr;
	BOOL vb_checkg;
	BOOL vb_checkb;
	afx_msg void OnBnClickedOk();
	CEdit m_mdnn;
	CEdit m_rechnr;
	CEdit m_rechdat;
	afx_msg void OnEnKillfocusRechdat();
    int sysdate (char *datum);

	CString v_rechnr;
	CString v_rechdat;
	CString v_mdnn;
};
