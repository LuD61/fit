// Nachdruck.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "RechFunc.h"
#include "Nachdruck.h"
#include "Process.h"
#include "DBclass.h"


#define PROGRAMM "rswrun rechbearb"


DB_CLASS DbClass;

// CNachdruck-Dialogfeld

IMPLEMENT_DYNAMIC(CNachdruck, CDialog)

CNachdruck::CNachdruck(CWnd* pParent /*=NULL*/)
	: CDialog(CNachdruck::IDD, pParent)
	, vb_checkn(TRUE)
	, vb_checks(FALSE)
	, vb_checkr(TRUE)
	, vb_checkg(FALSE)
	, vb_checkb(FALSE)
	, v_rechnr(_T(""))
	, v_rechdat(_T(""))
	, v_mdnn(_T("1"))
{

	char datum [15];

	sysdate (datum);
	v_rechdat.Format("%s" ,_T(datum));

}

CNachdruck::~CNachdruck()
{
}

void CNachdruck::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CHECKN, mb_checkn);
	DDX_Control(pDX, IDC_CHECKS, mb_checks);
	DDX_Control(pDX, IDC_CHECKR, mb_checkr);
	DDX_Control(pDX, IDC_CHECKB, mb_checkb);
	DDX_Control(pDX, IDC_CHECKG, mb_checkg);
	DDX_Check(pDX, IDC_CHECKN, vb_checkn);
	DDX_Check(pDX, IDC_CHECKS, vb_checks);
	DDX_Check(pDX, IDC_CHECKR, vb_checkr);
	DDX_Check(pDX, IDC_CHECKG, vb_checkg);
	DDX_Check(pDX, IDC_CHECKB, vb_checkb);
	DDX_Control(pDX, IDC_MDNN, m_mdnn);
	DDX_Control(pDX, IDC_RECHNR, m_rechnr);
	DDX_Control(pDX, IDC_RECHDAT, m_rechdat);
	DDX_Text(pDX, IDC_RECHNR, v_rechnr);
	DDX_Text(pDX, IDC_RECHDAT, v_rechdat);
	DDX_Text(pDX, IDC_MDNN, v_mdnn);
}


BEGIN_MESSAGE_MAP(CNachdruck, CDialog)
	ON_BN_CLICKED(IDC_CHECKN, &CNachdruck::OnBnClickedCheckn)
	ON_BN_CLICKED(IDC_CHECKS, &CNachdruck::OnBnClickedChecks)
	ON_BN_CLICKED(IDC_CHECKR, &CNachdruck::OnBnClickedCheckr)
	ON_BN_CLICKED(IDC_CHECKB, &CNachdruck::OnBnClickedCheckb)
	ON_BN_CLICKED(IDC_CHECKG, &CNachdruck::OnBnClickedCheckg)
	ON_BN_CLICKED(IDOK, &CNachdruck::OnBnClickedOk)
	ON_EN_KILLFOCUS(IDC_RECHDAT, &CNachdruck::OnEnKillfocusRechdat)
END_MESSAGE_MAP()


// CNachdruck-Meldungshandler
int CNachdruck::sysdate (char *datum)
{
 time_t timer;
 struct tm *ltime;

 time (&timer);
 ltime = localtime (&timer);
 sprintf (datum, "%02d.%02d.%04d", ltime->tm_mday,
                                   ltime->tm_mon + 1,
                                   ltime->tm_year + 1900);
 return (0);
}

void CNachdruck::OnBnClickedCheckn()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	UpdateData(TRUE);
	vb_checkn = TRUE;
	vb_checks = FALSE;

	vb_checkr = TRUE;
	vb_checkb = FALSE;
	vb_checkg = FALSE;

	mb_checkr.EnableWindow( TRUE);
	mb_checkb.EnableWindow( TRUE);
	mb_checkg.EnableWindow( TRUE);
	m_rechdat.EnableWindow(FALSE);

	UpdateData(FALSE);
}

void CNachdruck::OnBnClickedChecks()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	UpdateData(TRUE);
	vb_checks = TRUE;
	vb_checkn = FALSE;

	vb_checkr = TRUE;
	vb_checkb = FALSE;
	vb_checkg = FALSE;

	mb_checkr.EnableWindow( FALSE);
	mb_checkb.EnableWindow( FALSE);
	mb_checkg.EnableWindow( FALSE);
	m_rechdat.EnableWindow( TRUE);

	UpdateData(FALSE);

}

void CNachdruck::OnBnClickedCheckr()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	//	Radio-button emulieren
	vb_checkr = TRUE;
	vb_checkb = FALSE;
	vb_checkg = FALSE;
	UpdateData(FALSE);
}

void CNachdruck::OnBnClickedCheckb()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	//	Radio-button emulieren
	vb_checkb = TRUE;
	vb_checkr = FALSE;
	vb_checkg = FALSE;
	UpdateData(FALSE);
}

void CNachdruck::OnBnClickedCheckg()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	//	Radio-button emulieren
	vb_checkg = TRUE;
	vb_checkb = FALSE;
	vb_checkr = FALSE;
	UpdateData(FALSE);
}

void CNachdruck::OnBnClickedOk()
{


	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen
	CProcess process;

	LPSTR bws = getenv ("BWS");
	if (bws == NULL) return;

	CString Programm;
	Programm = PROGRAMM;
  
	UpdateData (TRUE);

	CString Message;
	CString Command;

	char ctyp[10] ;
	sprintf ( ctyp,"R");

	if (vb_checkn == TRUE )
	{

		
		if (vb_checkr == TRUE ) sprintf ( ctyp, "R" );
		if (vb_checkb == TRUE ) sprintf ( ctyp, "B" );
		if (vb_checkg == TRUE ) sprintf ( ctyp, "G" );

		Message.Format (_T("Rechnung wird nachgedruckt"));
//		StatusBar.SetWindowText (Message.GetBuffer (0));



		if ( exnutzer[0] != '\0' )
			Command.Format ("%s N %s %s %s %s", Programm.GetBuffer (0), v_mdnn.GetBuffer (0) , v_rechnr.GetBuffer(0), ctyp, exnutzer );
		else
		Command.Format ("%s N %s %s %s", Programm.GetBuffer (0), v_mdnn.GetBuffer (0) , v_rechnr.GetBuffer(0), ctyp);
		process.SetCommand (Command);
		process.Start ();
		process.WaitForEnd ();
	}
	else
	{
		// es kann dann nur noch storno sein, das ist nur f�r Rechnungen definiert ....
		
		Message.Format (_T("Rechnung wird storniert"));
//		StatusBar.SetWindowText (Message.GetBuffer (0));

		if ( exnutzer[0] != '\0' )
		Command.Format ("%s S %s %s %s %s %s",
				Programm.GetBuffer (0), v_mdnn.GetBuffer (0) , v_rechnr.GetBuffer(0), ctyp, v_rechdat.GetBuffer(0), exnutzer);
		else
		Command.Format ("%s S %s %s %s %s %s",
		Programm.GetBuffer (0), v_mdnn.GetBuffer (0) , v_rechnr.GetBuffer(0), ctyp, v_rechdat.GetBuffer(0), exnutzer);
		process.SetCommand (Command);
		process.Start ();
		process.WaitForEnd ();
	}


//	StatusBar.SetWindowText ("Ready");

	OnOK();
}

void CNachdruck::OnEnKillfocusRechdat()
{
	UpdateData (TRUE);
	DATE_STRUCT SDatum;
	DbClass.FromGerDate (&SDatum, v_rechdat.GetBuffer (0));
    v_rechdat = DbClass.ToGerDate4 (&SDatum, v_rechdat.GetBuffer (0));
	CWnd *cWnd = GetDlgItem (IDC_RECHDAT);
	if (cWnd != NULL)
	{
		cWnd->SetWindowText (v_rechdat);
	}

	UpdateData (FALSE);
}

BOOL CNachdruck::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_rechdat.EnableWindow(FALSE);

	return TRUE;
}


BOOL CNachdruck::PreTranslateMessage(MSG* pMsg) 
{
// 	 CWnd *cWnd;
	 if (pMsg->message == WM_KEYDOWN)
	 {
		 switch (pMsg->wParam)
			 {
		 
		        case VK_RETURN :
 //                    if (listView1 != (CListCtrl *) GetFocus ())
 //                    {
          	 		        NextDlgCtrl();
			                return TRUE;
//					 }
//					 SelectRech ();
//					 return TRUE;
			 case VK_F5 :
                     OnCancel ();
 			         return TRUE;
             case VK_F9 :
//		             OnKey9 ();
//                     break;
		     case VK_F12 :
                     OnBnClickedOk ();
				     return TRUE;

 		    case VK_DOWN :
//                     if (listView1 != (CListCtrl *) GetFocus ())
//                     {
          	  	            NextDlgCtrl();
						    return TRUE;
//                     }
//				     break;
 		    case VK_UP :
//                     if (listView1 != (CListCtrl *) GetFocus ())
//                     {
          	  	            PrevDlgCtrl();
							return TRUE;
//                     }
//				     break;
		 }
	 }
     return CDialog::PreTranslateMessage(pMsg);

}

