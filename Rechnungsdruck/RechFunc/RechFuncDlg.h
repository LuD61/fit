#pragma once
#include "Vector.h"
#include "Form.h"
#include "ToolButton.h"
#include "DbClass.h"
#include "RechTab.h"
#include "afxwin.h"

#define IDC_SCHOICE 2001

#define RESET 1
#define RESTORE 2

// CRechFuncDlg-Dialogfeld

class CRechFuncDlg : public CDialog
{
	DECLARE_DYNAMIC(CRechFuncDlg)

public:
	CRechFuncDlg(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CRechFuncDlg();

// Dialogfelddaten
	enum { IDD = IDD_FORMVIEW };
	HBITMAP ArrDown;
    HBITMAP F5;
    HBITMAP F12;
    HBITMAP F7;
    HBITMAP BPrint;
    CToolButton ToolButton;
    CStatusBarCtrl StatusBar;
    CFont DlgFont;
	CRect StatusRect;
	static int SortRow;
    static int Sort1;
    static int Sort2;
    static int Sort3;
    static int Sort4;

	DB_CLASS DbClass;
	CRechTab RechTab;
    CVector Toolbar;
    BORDER Border; 
	HICON m_hIcon;
	CMenu Menu;
    HIMAGELIST hLargeDir;    // image list for icon view 
    HIMAGELIST hSmallDir;    // image list for other views 
    HIMAGELIST hLargeFile;   // image list for icon view 
    HIMAGELIST hSmallFile;   // image list for other views 
	CImageList im;
    DWORD ListStyle;
    CListCtrl *listView1;
	CString BlgTyp;
	CString Programm;
	CString Command;

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	// Generierte Message-Map-Funktionen
	//{{AFX_MSG(CRechFuncDlg)
    afx_msg void OnSize(UINT, int, int);
	afx_msg void OnPaint();
	afx_msg void OnF5();
	afx_msg void OnF12();
	afx_msg void OnKey9();
	afx_msg void OnPrint();
	afx_msg void OnF7();
	afx_msg void OnDelete();
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg BOOL OnToolTipNotify(UINT nIDCtl,NMHDR *pNMHDR, LRESULT *pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	short m_mdn;
	long m_rech;
	long m_kun;
	CString m_date_rech;
	CString m_date_von;
	CString m_date_bis;

	CString Caption;
	int RechType;

	afx_msg void OnDateiBeenden();
	afx_msg void OnDateiAusf();
	afx_msg void OnBearbeitenAnzeigen();
	afx_msg void OnInfo();
	afx_msg void OnEnKillfocusdatvon();
	afx_msg void OnEnKillfocusdatbis();
	afx_msg void OnEnKillfocusdatrech();

    BOOL SetCol (DWORD, char *, int, int, int);
    BOOL SetCol (DWORD, char *, int, int);
    DWORD SetStyle (DWORD, DWORD);
    CListCtrl *GetListView (DWORD);
    BOOL SetItemText (char *Txt, int idx, int pos);
    BOOL InsertItem (int idx, int Image);
    void GetItemRect (CRect *Rect, int Id);
	afx_msg void OnLvnColumnclickList1(NMHDR *pNMHDR, LRESULT *pResult);
    virtual void Sort (CListCtrl *);
    void SetDbase (LPSTR database);
    static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
    static int CompareDate (CString& strItem1,CString& strItem2);
	afx_msg void OnNMDblclkList1(NMHDR *pNMHDR, LRESULT *pResult);
    void SelectRech ();
    void RechReset ();
    void RechRestore ();
	afx_msg void OnEnKillfocusKun();
	afx_msg void OnEnKillfocusMdn();
	void ReadKun ();	
    int sysdate (char *datum);
    void AddDate (char *datum, long days);
    ULARGE_INTEGER GetNanoDay ();
	CString m_blg_typ;
	CComboBox m_BlgTyp;
	afx_msg void OnEnChangeKun2();
	afx_msg void OnCbnSelchangeBlgTyp();
};

