#ifndef _HELP_DEF
#define _HELP_DEF
#include "Token.h"

#define WHITECOL  RGB (255,255,255)
#define BLACKCOL  RGB (0,0,0)
#define LTGRAYCOL RGB (192,192,192)
#define GRAYCOL RGB (128,128,128)
#define BLUECOL RGB (0, 0, 255)
#define GREENCOL RGB (0, 255, 0)
#define REDCOL RGB (255, 0, 0)
#define YELLOWCOL RGB (255, 255, 0)
#define DKYELLOWCOL RGB (175, 175, 0)


#ifndef OK_CTL
#define OK_CTL 9603
#endif
#ifndef CANCEL_CTL
#define CANCEL_CTL 9604 
#endif

#define INITHELP 0
#define INITCOMM 1

class HELP 
{
          private :
              int cx, cy;
              char *head;
              char *itemdata;
              long hlen;
              long dlen;
              long offset;
              int items;
              char **Rows;
              CStatusBarCtrl *hMainWindow;
              HINSTANCE hInstance;
              char *HelpName;
              CToken *Tk;
              static COLORREF Background;

          public :
   	          HELP (char *, CStatusBarCtrl*);
   	          HELP (char *, CStatusBarCtrl*, BOOL);
   	          HELP (char *, char *, CStatusBarCtrl*);
   	          HELP (char *, char *, CStatusBarCtrl*, BOOL);
  	          ~HELP ();

              char *Clipped (char *);
	          void Init (char *);
 	          void InitComment (char *);
              char *GetItpos (char *);
              BOOL ReadItem (char *,char *);
              char *GetItem (char *, char *);
              BOOL MakeCform (int); 
              BOOL OnKey (int);
              BOOL OnKey3 (void);
              BOOL OnKeyReturn (void);
              BOOL OnCommand (HWND,UINT,WPARAM,LPARAM);
              BOOL OnSysCommand (HWND,UINT,WPARAM,LPARAM);
              BOOL OnDestroy (HWND,UINT,WPARAM,LPARAM);
              BOOL OnLButtonDown (HWND, UINT,WPARAM,LPARAM);
              void SetWinBackground (COLORREF);
              static void SetBackground (COLORREF);
};
#endif
