#include "StdAfx.h"
#include "rech.h"
#include "rechtab.h"
#include "DbClass.h"
#define RESET 1
#define RESTORE 2

CRechTab::CRechTab(void)
{
	RechType = 0;
	db = NULL;
	ikun = 0l;
	cursor = -1;
	memset (&idat_von, 0, sizeof (idat_von));
	memset (&idat_bis, 0, sizeof (idat_bis));
	memset (&idat_rech, 0, sizeof (idat_rech));
	strcpy (blg_typ, "R"); 
}

CRechTab::~CRechTab(void)
{
	if (db == NULL) return;
	if (cursor == -1) return;
	Close ();
}

void CRechTab::SetDb (DB_CLASS *db)
{
	this->db = db;
}
void CRechTab::SetRechType (int type)
{
	this->RechType = type;
}

void CRechTab::SetQueryFields (long kun, short mdn, DATE_STRUCT *dat_von, DATE_STRUCT *dat_bis, DATE_STRUCT *dat_rech)
{
	this->ikun = kun;
	this->imdn = mdn;
	memcpy (&this->idat_von, dat_von, sizeof (DATE_STRUCT));
	memcpy (&this->idat_bis, dat_bis, sizeof (DATE_STRUCT));
	memcpy (&this->idat_rech, dat_rech, sizeof (DATE_STRUCT));
}

void CRechTab::Prepare ()
{

	if (cursor != -1)
	{
		Close ();
	}

	db->sqlin ((long *) &ikun, SQLLONG, 0);
	db->sqlin ((DATE_STRUCT *) &idat_von, SQLDATE, 0);
	db->sqlin ((DATE_STRUCT *) &idat_bis, SQLDATE, 0);
	db->sqlin ((char *) blg_typ, SQLCHAR, sizeof (blg_typ));

	db->sqlin ((short *) &imdn, SQLSHORT, 0);	// 141113

	db->sqlout ((long *) &orech, SQLLONG, 0);
	db->sqlout ((long *) &okun, SQLLONG, 0);
	db->sqlout ((long *) &ols, SQLLONG, 0);
	if (RechType == RESET)
	{
	db->sqlout ((DATE_STRUCT *) &orech_dat, SQLDATE, 0);
	db->sqlout ((DATE_STRUCT *) &olieferdat, SQLDATE, 0);
	cursor = db->sqlcursor ("select lsk.rech, lsk.kun, lsk.ls, rech.rech_dat, "
		                   "lieferdat from lsk, rech "
						   "where rech.rech_nr = lsk.rech "
						   "and lsk.rech > 0 "
						   "and lsk.inka_nr = ? "
						   "and lieferdat >= ? "
						   "and lieferdat <= ? "
						   "and rech.blg_typ = ?"
						   "and lsk.mdn = ? and lsk.mdn = rech.mdn " );
	}
	else 
	{
	db->sqlout ((DATE_STRUCT *) &olieferdat, SQLDATE, 0);
	cursor = db->sqlcursor ("select lsk.rech, lsk.kun, lsk.ls, "
		                   "lieferdat from lsk "
						   "where lsk.rech > 0 "
						   "and lsk.inka_nr = ? "
						   "and lsk.lieferdat >= ? "
						   "and lsk.lieferdat <= ? "
						   "and lsk.blg_typ = ?"						// blg_typ : das fehlte  bisher :141113
						   "and lsk.mdn = ? "	// 141113
						   "and lsk.ls_stat < 6");
	}

}

void CRechTab::Read ()
{
	if (cursor == -1)
	{
		Prepare ();
	}

	DestroyAll ();
	db->sqlopen (cursor);
	   orech_dat.day = 0; 
	   orech_dat.month = 0; 
	   orech_dat.year = 0; 

	while (db->sqlfetch (cursor) == 0)
	{
		CRech *rech = new CRech ();
		if (rech == NULL) break;
		rech->SetRechNr (orech);
		rech->SetLs (ols);
		rech->SetKun (okun);
  	    if (RechType == RESET) rech->SetRechdat (&orech_dat);
		rech->SetLieferdat (&olieferdat);
		Add (rech);
	    orech_dat.day = 0; 
	    orech_dat.month = 0; 
	    orech_dat.year = 0; 
	}
}

void CRechTab::Close ()
{
	if (db != NULL && cursor != -1)
	{
		db->sqlclose (cursor);
		cursor = -1;
	}
	DestroyAll ();
}


