#include "StdAfx.h"
#include "sys_par.h"

CSys_par::CSys_par(void)
{
	strcpy (sys_par_wrt , "");
	strcpy (sys_par_nam , "");
	strcpy (sys_par_besch , "");
	db = NULL;
	cursor = -1;
}

CSys_par::~CSys_par(void)
{
	if (db == NULL) return;
	if (cursor == -1) return;
	Close ();
}

void CSys_par::SetDb (DB_CLASS *db)
{
	this->db = db;
}

void CSys_par::Prepare ()
{
	if (db == NULL) return;
	if (cursor != -1)
	{
		Close ();
	}

	db->sqlin ((char *) sys_par_nam, SQLCHAR,sizeof(sys_par_nam) );
	db->sqlout ((char *) &sys_par_wrt, SQLCHAR, sizeof(sys_par_wrt));
	db->sqlout ((char *) sys_par_besch, SQLCHAR, sizeof (sys_par_besch));
	cursor = db->sqlcursor ("select sys_par_wrt, sys_par_besch from sys_par "
		                    "where sys_par_nam = ? "
							);
}

int CSys_par::Read ()
{
	if (db == NULL) return 100;

	if (cursor == -1)
	{
		Prepare ();
	}

	db->sqlopen (cursor);
	return db->sqlfetch (cursor);
}


void CSys_par::Close ()
{
	if (db != NULL && cursor != -1)
	{
		db->sqlclose (cursor);
		cursor = -1;
	}
}

