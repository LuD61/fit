#include "stdafx.h"
#include "ColButton.h"

CColButton::CColButton () : CButton ()
{
    Active  = FALSE;
    Pressed = FALSE;
    InButton = FALSE;
    Id      = 0;
}

void CColButton::SetActive (BOOL b)
{
    Active = b;
}

BOOL CColButton::GetActive (void)
{
    return Active;
}


BEGIN_MESSAGE_MAP(CColButton, CButton)
	//{{AFX_MSG_MAP(CColButton)
	     ON_WM_LBUTTONDOWN()
	     ON_WM_LBUTTONUP()
	     ON_WM_MOUSEMOVE()
	     ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CColButton::OnMouseMove(UINT nFlags, CPoint point) 
{
	// TODO: Code f�r die Behandlungsroutine f�r Nachrichten hier einf�gen und/oder Standard aufrufen

    if (Active == TRUE && InButton == FALSE)
    {
        return;
    }

    if (Active == FALSE && InButton == TRUE)
    {
        return;
    }


    if ((nFlags & MK_LBUTTON) != 0)
    {
         Pressed = TRUE;
    }
    Active = TRUE;
    InButton = TRUE;
    if (Pressed)
    {
        DownBorder ();
    }
    else
    {
        UpBorder ();
        SetTimer (1, 100, NULL);
    }
//	CButton::OnMouseMove(nFlags, point);
}

void CColButton::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Code f�r die Behandlungsroutine f�r Nachrichten hier einf�gen und/oder Standard aufrufen

	CButton::OnLButtonDown(nFlags, point);
    if (InButton)
    {
         DownBorder ();
         Pressed = TRUE;
    }
}

void CColButton::OnLButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Code f�r die Behandlungsroutine f�r Nachrichten hier einf�gen und/oder Standard aufrufen
    if (InButton)
    {
          UpBorder ();
	      CButton::OnLButtonUp(nFlags, point);
          Pressed = FALSE;
    }
    else if (Pressed)
    {
          Active = FALSE;
          KillTimer (1);
          RedrawWindow (NULL, NULL, RDW_ERASE | RDW_INVALIDATE);
          InButton = FALSE;
          Pressed = FALSE;
          ReleaseCapture ();
    }
    else 
    {
	      CButton::OnLButtonUp(nFlags, point);
          Pressed = FALSE;
    }
}

void CColButton::UpBorder (void)
{
     CRect Rect; 

     GetClientRect (&Rect);
     CDC *pDC = GetDC ();
     CPen *Pen = new CPen (PS_SOLID, 1, RGB (255,255,255));
     CPen *oldPen =  pDC->SelectObject (Pen);
     pDC->MoveTo (Rect.left, Rect.top);
     pDC->LineTo (Rect.left, Rect.bottom);
     pDC->MoveTo (Rect.left, Rect.top);
     pDC->LineTo (Rect.right, Rect.top);
     pDC->SelectObject (oldPen);
     delete Pen;

     Pen = new CPen (PS_SOLID, 1, RGB (120,120,120));
     pDC->SelectObject (Pen);
     pDC->MoveTo (Rect.left, Rect.bottom - 1);
     pDC->LineTo (Rect.right, Rect.bottom - 1);
     pDC->MoveTo (Rect.right - 1, Rect.top);
     pDC->LineTo (Rect.right - 1, Rect.bottom - 1);
     pDC->SelectObject (oldPen);
     delete Pen;
     ReleaseDC (pDC);
}

void CColButton::DownBorder (void)
{
     CRect Rect; 
     GetClientRect (&Rect);
     CDC *pDC = GetDC ();
     CPen *Pen = new CPen (PS_SOLID, 1, RGB (120,120,120));
     CPen *oldPen =  pDC->SelectObject (Pen);
     pDC->MoveTo (Rect.left, Rect.top);
     pDC->LineTo (Rect.left, Rect.bottom);
     pDC->MoveTo (Rect.left, Rect.top);
     pDC->LineTo (Rect.right, Rect.top);
     pDC->SelectObject (oldPen);
     delete Pen;

     Pen = new CPen (PS_SOLID, 1, RGB (255,255,255));
     pDC->SelectObject (Pen);
     pDC->MoveTo (Rect.left, Rect.bottom - 1);
     pDC->LineTo (Rect.right, Rect.bottom - 1);
     pDC->MoveTo (Rect.right - 1, Rect.top);
     pDC->LineTo (Rect.right - 1, Rect.bottom - 1);
     pDC->SelectObject (oldPen);
     delete Pen;
     ReleaseDC (pDC);
}

void CColButton::SetId (int Id)
{
    this->Id = Id;
}

int CColButton::GetId (void)
{
    return Id;
}

void CColButton::NoBorder (void)
{
     Active = FALSE;
}

void CColButton::OnTimer(UINT nIDEvent) 
{
    POINT Point;
    RECT Rect;
	// TODO: Code f�r die Behandlungsroutine f�r Nachrichten hier einf�gen und/oder Standard aufrufen
    if (nIDEvent == 1)
    {
/*
	    if (Active == FALSE)
        {
                  KillTimer (1);
                  return;
        }
*/
        GetClientRect (&Rect);
        GetCursorPos (&Point);
        ScreenToClient (&Point);
        CPoint P = Point;
        if (IsInButton (P) == FALSE)
        {
            InButton = FALSE;
            if (Pressed == FALSE)
            {
                 Active = FALSE;
                 KillTimer (1);
                 RedrawWindow (NULL, NULL, RDW_ERASE | RDW_INVALIDATE);
            }
            else
            {
                UpBorder ();
                Pressed = TRUE;
            }
        }
        else
        {
            InButton = TRUE;
        }
        return;
    }
	CButton::OnTimer(nIDEvent);
}

BOOL CColButton::IsInButton (CPoint& Point)
{
    RECT Rect;

    GetClientRect (&Rect);
    if (Rect.bottom < Point.y ||
        Rect.right < Point.x ||
        Point.x < 0 ||
        Point.y < 0)
    {
        return FALSE;
    }
    return TRUE;
}
