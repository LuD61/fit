#ifndef _TOOLBUTTENDEF
#define _TOOLBUTTENDEF

#include "Vector.h"
#include "ToolbarItem.h"

class CToolButton
{
  protected :
      HBITMAP F5;
      HBITMAP F12;
      HBITMAP F7;
      HBITMAP BPrint;
      CVector StdToolbar;
      CVector *Toolbar;
      int startx;
      int tbx;
      int tby;

      int bitmapcx;
      int bitmapcy;

      int horzyup;
      int horzydown;
      int horzcx;
      int horzcy;

      int vertsepcx;
      int vertsepcy;

      int spacecx;
      int wspacecx;

  public :
      CToolButton ();
      ~CToolButton ();
      void SetSpace (int);
      void SetWSpace (int);
      void Create (CWnd *);
      void CreateStdToolbar ();
      void Create (CVector *);
      void Add (CToolbarItem *);
	  void Destroy (int);
	  void SetTooltipText (LPSTR, int);
	  void SetVisible (int, BOOL);
	  void SetActive (int, BOOL);
      BOOL DrawToolbar (int, HDC);
	  BOOL OnToolTipNotify(int,NMHDR *, LRESULT *);
      static CFont *SetFont (CDC *);
      int GetCX (CToolbarItem *);
};

#endif