//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by RechFunc.rc
//
#define IDOK2                           2
#define IDD_FORMVIEW                    101
#define IDD_RECHVIEW                    102
#define IDC_MDN                         1000
#define IDC_RECH2                       1001
#define ID_DATEI_BEENDEN                1001
#define IDC_KUN                         1002
#define IDC_DATE_VON                    1003
#define IDC_DATE_BIS                    1004
#define ID_PRINT                        1004
#define ID_BEARBEITEN_ANZEIGEN          1005
#define IDC_KUN2                        1005
#define ID__INFO                        1006
#define IDC_LIST1                       1006
#define IDI_ICON2                       1006
#define IDD_ABOUTBOX                    1007
#define IDR_ACCELERATOR1                1007
#define IDC_KUN_KRZ1                    1007
#define ID_DATEI_DRUCKERWAHL            1008
#define IDD_NACHDRUCK                   1009
#define IDC_KUN_VON                     1010
#define IDC_KUN_BIS                     1011
#define IDC_BRAN_VON                    1012
#define IDC_BRAN_BIS                    1013
#define IDC_KUGRU1_VON                  1014
#define IDC_KUGRU1_BIS                  1015
#define IDC_KGRU2_VON                   1016
#define IDC_KUGRU2_BIS                  1017
#define IDC_RECHTYP                     1018
#define IDC_KOPFTEXT                    1019
#define IDC_FUSSTEXT                    1020
#define IDC_RECHTYP2                    1021
#define IDC_FUSSTEXT2                   1021
#define IDC_PRINTER                     1022
#define IDC_RECHTYPLIST                 1022
#define IDR_MENU1                       1023
#define IDC_PROGRESS1                   1023
#define IDR_MENU2                       1024
#define IDC_COMBO1                      1024
#define IDC_BLG_TYP                     1024
#define ID_DATEI_AUSF                   1025
#define IDC_CHECKR                      1025
#define IDC_DATE_RECH                   1026
#define IDC_CHECKG                      1026
#define IDC_KUNTYP                      1027
#define IDC_CHECKB                      1027
#define IDC_CHECKN                      1028
#define IDC_CHECKS                      1029
#define IDC_EDIT1                       1030
#define IDC_MDNN                        1030
#define IDC_RECHNR                      1031
#define IDC_EDIT3                       1032
#define IDC_RECHDAT                     1032

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        1010
#define _APS_NEXT_COMMAND_VALUE         32777
#define _APS_NEXT_CONTROL_VALUE         1033
#define _APS_NEXT_SYMED_VALUE           1000
#endif
#endif
