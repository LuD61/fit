#ifndef _TOOLBAREX_DEF
#define _TOOLBAREX_DEF
#include "ColButton.h"

#define VERT 1
#define TBDOWN 2
#define TBUP 3
#define SPACE 4
#define TBTEXT 5

#define IDC_F5                          9026
#define IDC_F12                         9027
#define IDC_F7                          9028
#define IDC_PRINT                       9025
#define IDC_PRINT2                      9029
#define IDC_LINE                        9034
// #define IDC_HOTKEY1                     9030
#define IDC_SEPERATOR                   9031
#define IDC_KEY9                        9035
#define IDC_DELETE                      9036


class CToolbarItem
{
    protected :
        CButton *Button;
        CString Text;
        CString TooltipText;
        int Id;
        HBITMAP Hb;
        int Seperator;
        CWnd *Parent;
        BOOL Active;
    public :
        CToolbarItem ();
        CToolbarItem (int ,HBITMAP, CWnd *);
        CToolbarItem (int, int, CWnd *);
        CToolbarItem (int, CString&, CWnd *);
        ~CToolbarItem ();
        void CreateButton (int, int, int, int);
        BOOL IsCtrl (int);
        BOOL IsTooltipCtrl (HWND, TOOLTIPTEXT *);
        void SetId (int);
        void SetHb (HBITMAP);
        void SetText (CString&);
        void SetSeperator (BOOL);
        void SetParent (CWnd *);
        void SetActive (BOOL);
        void SetTooltipText (char *);
        BOOL GetActive (void);
        int GetId (void);
        BOOL GetSeperator (void);
        CWnd *GetParent (void);
        HBITMAP GetHb (void);
        void GetHbSize (CPoint *);
        CString& GetText (void);
        void Display (HDC);
        void Display (HDC, int, int);
        void CentItem (HDC);
        void DrawSeperator (HDC);
        void DisplayText (HDC);
		void Destroy ();
		void SetVisible (BOOL);
		void Enable (BOOL);
};
#endif