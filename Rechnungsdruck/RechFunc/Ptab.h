#pragma once
#include "Vector.h"
#include "Sam_rech.h"
#include "DbClass.h"


class CPtab : public CVector
{
private :
	 CString PtItem;
	 char ptitem [21];
	 CSam_rech SamRech;
	 DB_CLASS *db;
	 int cursor;
public:
	CPtab();
	CPtab(CString& ptitem);
	~CPtab(void);
	void Prepare ();
	int Read ();
	void Close ();
	void SetDb (DB_CLASS *);
	void SetPtItem (CString&);
};
