#pragma once
#include "vector.h"
#include "DbClass.h"

class CRechTab :
	public CVector
{
public:
	DB_CLASS *db;
	int RechType;
	int cursor;
	long ikun;
	short imdn;	// 141113
	char blg_typ [2];
	DATE_STRUCT idat_von;
	DATE_STRUCT idat_bis;
	DATE_STRUCT idat_rech;
	long orech;
	long ols;
	long okun;
	DATE_STRUCT olieferdat;
	DATE_STRUCT orech_dat;

public:
	CRechTab(void);
	~CRechTab(void);
	void SetDb (DB_CLASS *db);
	void SetRechType (int type);
	void SetQueryFields (long kun, short mdn, DATE_STRUCT *dat_von, DATE_STRUCT *dat_bis, DATE_STRUCT *dat_rech);
	void Prepare ();
	void Read ();
	void Close ();
};
