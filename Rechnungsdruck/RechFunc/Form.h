#ifndef _FORM_DEF
#define _FORM_DEF
#include <odbcinst.h>
#include "Vector.h"

#define FRMCHAR 0
#define FRMSHORT 1
#define FRMLONG 2
#define FRMDOUBLE 3
#define FRMDATE 4
#define FRMBOOL 5
#define FRMCHARBOOL 6

#define LISTBOX 1
#define COMBOBOX 2
#define COMBOLIST 3

#define LINE 0
#define RAISED 1
#define HIGH 2
#define LOW 3
#define VLINE 4
#define RAISEDVLINE 5
#define HLINE 6
#define RAISEDHLINE 7

#define WHITECOL  RGB (255,255,255)
#define BLACKCOL  RGB (0,0,0)
#define LTGRAYCOL RGB (192,192,192)
#define GRAYCOL RGB (128,128,128)
#define BLUECOL RGB (0, 0, 255)
#define GREENCOL RGB (0, 255, 0)
#define REDCOL RGB (255, 0, 0)
#define YELLOWCOL RGB (255, 255, 0)
#define DKYELLOWCOL RGB (175, 175, 0)


class CForm
{
   protected :
       int Id;
       void *VarDB;
       int  Type;
       char *Picture;
       int VarLen;
       int ListboxType;
       char *DbItem;
       CString Name;
       CString Datum;
   public :
       static CString HelpName;
       static BYTE *DlgItempos;

       CForm ();
       CForm (int Id, void *VarDB, int Type, int VarLen);
       CForm (int Id, void *VarDB, int Type, char *Picture);
       CForm (int Id, void *VarDB, int Type, int VarLen, char *Name);
       CForm (int Id, void *VarDB, int Type, char *Picture, char *Name);
       ~CForm ();

       void SetItem (int Id, void *VarDB, int Type, char *Picture);

       int GetId (void);
       void *GetVarDB (void);
       int GetType (void);
       void SetListboxType (int);
       int GetListboxType (void);
       void SetDbItem (char *);
       char * GetDbItem (void);
       void PrintComment (CStatusBarCtrl *);

       void Enable (CWnd *, BOOL);
       CString& ToString (CString&);
       void FromString (CString&);
       CString& DateToString (DATE_STRUCT *);
       void StringToDate (char *, DATE_STRUCT *);
       void TestDate ();

       virtual void ToScreen (CWnd *);
       virtual void FromScreen (CWnd *);
       virtual void FillListBox (CWnd *, char *) {}
       virtual void FillListBox (CWnd *, char **){}
       virtual void FillPtBox (void *, CWnd *, char *){}
       virtual void FillPtBox (void *, CWnd *){}
       virtual void FillPtBoxLong (void *, CWnd *, char *){}
       virtual void FillPtBoxLong (void *, CWnd *){}

       static BOOL Comment (CVector *Frm, int Id,CStatusBarCtrl *Comm);

       static WORD *AddHeader (WORD *, LPSTR);
       static WORD *AddFont (WORD *, WORD, LPSTR);
       static void InitDlgItems (BYTE *);
       static BOOL AddItem (DLGITEMTEMPLATE *, WORD,  LPCSTR);
       static BOOL AddItem (DLGITEMTEMPLATE *, LPCSTR, LPCSTR);
};

class CListForm : virtual public CForm
{
   protected :
        CListBox *lb; 
        CComboBox *cb; 
       
   public :
       CListForm (int Id, void *VarDB, int Type, int VarLen, int ListboxType);
       CListForm (int Id, void *VarDB, int Type, char *Picture, int ListboxType);
       CListForm (int Id, void *VarDB, int Type, int VarLen, int ListboxType, char *DbItem);
       CListForm (int Id, void *VarDB, int Type, char *Picture, int ListboxType, char *DbItem);

       void FillListBox (CWnd *, char *);
       void FillListBox (CWnd *, char **);
       void FillPtBox (void *, CWnd *, char *);
       void FillPtBox (void *, CWnd *);
       void FillPtBoxLong (void *, CWnd *, char *);
       void FillPtBoxLong (void *, CWnd *);
       void GetText (CWnd *, int, CString&);
       void ToScreen (CWnd *);
       void FromScreen (CWnd *);
};


class BORDER
{
        private : 
            int Width;
            COLORREF Col;
            COLORREF HiCol;
            int Type;
            int xspace;
            int yspace;
            int ystart;
            CRect *BorderRect;
        public :
            BORDER ()
            {
                Width = 0;
                Col   = GRAYCOL;
                HiCol = WHITECOL;
                Type  = RAISED;
                xspace = yspace = 5;
                ystart = 20;
                BorderRect = NULL;
            }

            BORDER (int Type)
            {
                Width = 0;
                Col   = GRAYCOL;
                HiCol = WHITECOL;
                this->Type  = Type;
                xspace = yspace = 5;
                ystart = 20;
                BorderRect = NULL;
            }

            BORDER (COLORREF Col, COLORREF HiCol)
            {
                Width = 0;
                this->Col   = Col;
                this->HiCol = HiCol;
                Type  = RAISED;
                xspace = yspace = 5;
                ystart = 20;
                BorderRect = NULL;
            }

            BORDER (COLORREF Col, COLORREF HiCol, int Type)
            {
                Width = 0;
                this->Col   = Col;
                this->HiCol = HiCol;
                this->Type  = Type;
                xspace = yspace = 5;
                ystart = 20;
                BorderRect = NULL;
            }

            BORDER (COLORREF Col, COLORREF HiCol, int Type, int Width)
            {
                this->Width = Width;
                this->Col   = Col;
                this->HiCol = HiCol;
                this->Type  = Type;
                BorderRect = NULL;
            }

            void SetXSpace (int xspace)
            {
                this->xspace = xspace;
            }

            int GetXSpace (void)
            {
                return xspace;
            }

            void SetYSpace (int yspace)
            {
                this->yspace = yspace;
            }

            int GetYSpace (void)
            {
                return yspace;
            }

            void SetYStart (int ystart)
            {
                this->ystart = ystart;
            }

            int GetYStart (void)
            {
                return ystart;
            }

            void SetBorderRect (CRect *BorderRect)
            {
                this->BorderRect = BorderRect;
            }

            CRect *GetBorderRect (void)
            {
                return BorderRect;
            }


            void  SetColors (COLORREF Col, COLORREF HiCol)
            {
                this->Col   = Col;
                this->HiCol = HiCol;
            }

            void  SetCol (COLORREF Col)
            {
                this->Col   = Col;
            }

            void  SetHiCol (COLORREF HiCol)
            {
                this->HiCol   = HiCol;
            }

            void SetType (int Type)
            {
                this->Type = Type;
            }

            void SetWidth (int Width)
            {
                this->Width = Width;
            }

            COLORREF GetCol (void)
            {
                return Col;
            }

            COLORREF GetHiCol (void)
            {
                return HiCol;
            }

            int GetType (void)
            {
                return Type;
            }

            int GetWidth (void)
            {
                return Width;
            }

            void  PrintVLine (CWnd *, CDC *, int, int, int);
            void  PrintRaisedVLine (CWnd *, CDC *, int, int, int);
            void  PrintHLine (CWnd *, CDC *, int, int, int);
            void  PrintRaisedHLine (CWnd *, CDC *, int, int, int);
            void  PrintLine (CWnd *, CDC *, int, int, int, int);
            void  PrintRaised (CWnd *, CDC *, int, int, int, int);
            void  PrintHigh (CWnd *, CDC *, int, int, int, int);
            void  PrintLow (CWnd *, CDC *, int, int, int, int);
            virtual void  Print (CWnd *, CDC *, int, int, int, int);
            virtual void  Print (CWnd *, CDC *);
            virtual void  Print (CWnd *, CDC *, int);
            virtual void  Print (CWnd *, CDC *, int, int);
            virtual void  Print (CWnd *, CDC *, int, int, int);
};
#endif