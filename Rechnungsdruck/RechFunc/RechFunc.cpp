// RechFunc.cpp : Definiert die Initialisierungsroutinen f�r die DLL.
//

#include "stdafx.h"
#include "RechFunc.h"
#include "RechFuncDlg.h"
#include "RechPrint.h"
#include "Nachdruck.h"	// 141113
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//
//	Hinweis:
//
//		Wird diese DLL dynamisch an die MFC-DLLs gebunden,
//		muss bei allen von dieser DLL exportierten Funktionen,
//		die MFC-Aufrufe durchf�hren, das Makro AFX_MANAGE_STATE
//		direkt am Beginn der Funktion eingef�gt sein.
//
//		Beispiel:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//		// Hier normaler Funktionsrumpf
//		}
//
//		Es ist sehr wichtig, dass dieses Makro in jeder Funktion
//		vor allen MFC-Aufrufen angezeigt wird. Dies bedeutet, dass es
//		als erste Anweisung innerhalb der Funktion ausgef�hrt werden
//		muss, sogar vor jeglichen Deklarationen von Objektvariablen,
//		da ihre Konstruktoren Aufrufe in die MFC-DLL generieren
//		k�nnten.
//
//		Siehe MFC Technical Notes 33 und 58 f�r weitere
//		Details.
//

char exnutzer[99] ;	// 160513

// extern "C" BOOL PASCAL EXPORT RechStart()
EXPORT bool RechStart(char * eexnutzer )
{

	sprintf ( exnutzer,"%s",eexnutzer) ;	// 160513

	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CRechFuncDlg dlg;
//	m_pMainWnd = &dlg;
	dlg.SetDbase ("bws");
	dlg.Caption  = "Rechnungen zur�cksetzen";
	dlg.RechType = RESET;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// ZU ERLEDIGEN: F�gen Sie hier Code ein, um ein Schlie�en des
		//  Dialogfelds �ber OK zu steuern
	}
	else if (nResponse == IDCANCEL)
	{
		// ZU ERLEDIGEN: F�gen Sie hier Code ein, um ein Schlie�en des
		//  Dialogfelds �ber "Abbrechen" zu steuern
	}

	return TRUE;          
}

EXPORT bool RechRestoreStart(char * eexnutzer)
{

	sprintf ( exnutzer,"%s",eexnutzer) ;	// 160513

	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CRechFuncDlg dlg;
//	m_pMainWnd = &dlg;
	dlg.SetDbase ("bws");
	dlg.Caption  = "Rechnungen wiederherstellen";
	dlg.RechType = RESTORE;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// ZU ERLEDIGEN: F�gen Sie hier Code ein, um ein Schlie�en des
		//  Dialogfelds �ber OK zu steuern
	}
	else if (nResponse == IDCANCEL)
	{
		// ZU ERLEDIGEN: F�gen Sie hier Code ein, um ein Schlie�en des
		//  Dialogfelds �ber "Abbrechen" zu steuern
	}

	return TRUE;          
}

EXPORT bool RechPrintStart(char * eexnutzer )
{

	sprintf ( exnutzer,"%s",eexnutzer) ;	// 160513

	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CRechPrint dlg;
//	m_pMainWnd = &dlg;
	dlg.SetDbase ("bws");
	dlg.Caption  = "Rechnungen drucken";
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// ZU ERLEDIGEN: F�gen Sie hier Code ein, um ein Schlie�en des
		//  Dialogfelds �ber OK zu steuern
	}
	else if (nResponse == IDCANCEL)
	{
		// ZU ERLEDIGEN: F�gen Sie hier Code ein, um ein Schlie�en des
		//  Dialogfelds �ber "Abbrechen" zu steuern
	}

	return TRUE;          
}

EXPORT bool RechNachDruckStart(char * eexnutzer )
{

	sprintf ( exnutzer,"%s",eexnutzer) ;	// 160513

	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CNachdruck dlg;
//	m_pMainWnd = &dlg;
//	dlg.SetDbase ("bws");
//	dlg.Caption  = "Nachdruck/Storno";
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// ZU ERLEDIGEN: F�gen Sie hier Code ein, um ein Schlie�en des
		//  Dialogfelds �ber OK zu steuern
	}
	else if (nResponse == IDCANCEL)
	{
		// ZU ERLEDIGEN: F�gen Sie hier Code ein, um ein Schlie�en des
		//  Dialogfelds �ber "Abbrechen" zu steuern
	}

	return TRUE;          
}


// CRechFuncApp

BEGIN_MESSAGE_MAP(CRechFuncApp, CWinApp)
END_MESSAGE_MAP()


// CRechFuncApp-Erstellung

CRechFuncApp::CRechFuncApp()
{
	// TODO: Hier Code zur Konstruktion einf�gen
	// Alle wichtigen Initialisierungen in InitInstance positionieren
}


// Das einzige CRechFuncApp-Objekt

CRechFuncApp theApp;


// CRechFuncApp Initialisierung

BOOL CRechFuncApp::InitInstance()
{
	CWinApp::InitInstance();

	AfxEnableControlContainer();

	// Standardinitialisierung
	// Wenn Sie diese Funktionen nicht nutzen und die Gr��e Ihrer fertigen 
	//  ausf�hrbaren Datei reduzieren wollen, sollten Sie die nachfolgenden
	//  spezifischen Initialisierungsroutinen, die Sie nicht ben�tigen, entfernen.

#ifdef _AFXDLL
	Enable3dControls();			// Diese Funktion bei Verwendung von MFC in gemeinsam genutzten DLLs aufrufen
#else
	Enable3dControlsStatic();	// Diese Funktion bei statischen MFC-Anbindungen aufrufen
#endif

	return TRUE;
}
