#pragma once
#include "afxwin.h"
#include "Vector.h"
#include "Form.h"
#include "ToolButton.h"
#include "DbClass.h"
#include "Ptab.h"

// CRechPrint-Dialogfeld

#define IDC_PRCHOICE 2002

class CRechPrint : public CDialog
{
	DECLARE_DYNAMIC(CRechPrint)

public:
	CRechPrint(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CRechPrint();

// Dialogfelddaten
	enum { IDD = IDD_RECHVIEW };
	HBITMAP ArrDown;
    HBITMAP F5;
    HBITMAP F12;
    HBITMAP F7;
    HBITMAP BPrint;
    CToolButton ToolButton;
    CStatusBarCtrl StatusBar;
    CFont DlgFont;
	CRect StatusRect;
	CString Caption;
	DB_CLASS DbClass;
    BORDER Border; 
	HICON m_hIcon;
	CMenu Menu;
	CPtab ptab;
	CString Programm;
	CString Command;

protected:
	afx_msg void OnPaint();
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg BOOL OnToolTipNotify(UINT nIDCtl,NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnF5();
	afx_msg void OnF12();
	afx_msg void OnF7();
    afx_msg void OnSize(UINT, int, int);

	DECLARE_MESSAGE_MAP()
public:
	CEdit Mdn;
	short m_mdn;
	CString m_date_rech;
	CString m_date_von;
	CString m_date_bis;
	long m_kun_von;
	long m_kun_bis;
	CString m_bran_von;
	CString m_bran_bis;
	short m_kugru1_von;
	short m_kugru1_bis;
	short m_kugru2_von;
	short m_kugru2_bis;
	CString m_sam_rech;
	CComboBox *Rechtyp;
	CListBox *RechtypList;
	long m_kopftext;
	long m_fusstext;
	CString Params;

    void SetDbase (LPSTR database);
	afx_msg void OnEnChangeMdn();
	afx_msg void OnEnChangeKopftext();
	afx_msg void OnDateiBeenden();
	afx_msg void OnDateiAusf();
	afx_msg void OnCbnSelchangeRechtyp();
	afx_msg void OnEnSetfocusMdn();
	afx_msg void OnEnKillfocusdatvon();
	afx_msg void OnEnKillfocusdatbis();
	afx_msg void OnEnKillfocusdatrech();
    void RechPrint ();
    void RechFree ();
	afx_msg void OnPrint();
	void MakeParams ();
	void MakeParams (CSam_rech *Sam_rech);
    int sysdate (char *datum);
    void AddDate (char *datum, long days);
    ULARGE_INTEGER GetNanoDay ();
	afx_msg void OnInfo();
	afx_msg void OnDateiDruckerwahl();
	CString m_fil;
};
