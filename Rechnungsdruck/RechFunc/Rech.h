#pragma once

class CRech
{
private:
	long rech_nr;
	long kun;
	long ls;
	char blg_typ [2];
	DATE_STRUCT lieferdat;
	DATE_STRUCT rech_dat;
public:
	CRech(void);
	~CRech(void);
	void SetRechNr (long);
	long GetRechNr ();
	void SetKun (long);
	long GetKun ();
	void SetLs (long);
	long GetLs ();
	void SetBlgTyp (LPSTR blg_typ);
	LPSTR GetBlgTyp ();
	void SetLieferdat (DATE_STRUCT *_);
	DATE_STRUCT *GetLieferdat ();
	void SetRechdat (DATE_STRUCT *_);
	DATE_STRUCT *GetRechdat ();
};
