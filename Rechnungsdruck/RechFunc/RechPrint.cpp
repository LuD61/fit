// RechPrint.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "RechFunc.h"
#include "RechPrint.h"
#include "Process.h"
#include "AboutDialog.h"

#include "sys_par.h"	// 160513

// #define PROGRAMM "cmd /k echo rswrun rechbearb"

#define PROGRAMM "rswrun rechbearb"

#define MAXTYPES 20

// CRechPrint-Dialogfeld

	DATE_STRUCT Datum;

IMPLEMENT_DYNAMIC(CRechPrint, CDialog)
CRechPrint::CRechPrint(CWnd* pParent /*=NULL*/)
	: CDialog(CRechPrint::IDD, pParent)
	, m_mdn(1)
	, m_date_rech(_T(""))
	, m_date_von(_T(""))
	, m_date_bis(_T(""))
	, m_kun_von(1)
	, m_kun_bis(99999999)
	, m_bran_von(_T(""))
	, m_bran_bis(_T(""))
	, m_kugru1_von(1)
	, m_kugru1_bis(9999)
	, m_kugru2_von(1)
	, m_kugru2_bis(9999)
	, m_kopftext(0)
	, m_fusstext(0)
	, m_fil(_T("*"))
{
	char datum [12];
	Caption = "Rechnung";
	sysdate (datum);
	m_date_bis = datum;
	m_date_rech = datum;
	AddDate (datum, -30);
	m_date_von = datum;
}

CRechPrint::~CRechPrint()
{
}

void CRechPrint::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_MDN, Mdn);
	DDX_Text(pDX, IDC_MDN, m_mdn);
	DDX_Text(pDX, IDC_DATE_RECH, m_date_rech);
	DDX_Text(pDX, IDC_DATE_VON, m_date_von);
	DDX_Text(pDX, IDC_DATE_BIS, m_date_bis);
	DDX_Text(pDX, IDC_KUN_VON, m_kun_von);
	DDX_Text(pDX, IDC_KUN_BIS, m_kun_bis);
	DDX_Text(pDX, IDC_BRAN_VON, m_bran_von);
	DDX_Text(pDX, IDC_BRAN_BIS, m_bran_bis);
	DDX_Text(pDX, IDC_KUGRU1_VON, m_kugru1_von);
	DDX_Text(pDX, IDC_KUGRU1_BIS, m_kugru1_bis);
	DDX_Text(pDX, IDC_KGRU2_VON, m_kugru2_von);
	DDX_Text(pDX, IDC_KUGRU2_BIS, m_kugru2_bis);
	//	DDX_Control(pDX, IDC_RECHTYP, Rechtyp);
	DDX_Text(pDX, IDC_KOPFTEXT, m_kopftext);
	DDX_Text(pDX, IDC_FUSSTEXT, m_fusstext);
	DDX_Text(pDX, IDC_FUSSTEXT2, m_fil);
}


BEGIN_MESSAGE_MAP(CRechPrint, CDialog)
	//{{AFX_MSG_MAP(RechFuncDlg)
	ON_BN_CLICKED(IDC_F5, OnF5)
	ON_BN_CLICKED(IDC_F12, OnF12)
	ON_BN_CLICKED(IDC_PRINT2, OnF7)
	ON_BN_CLICKED(IDC_PRCHOICE, OnDateiDruckerwahl)
	ON_WM_PAINT()
	ON_WM_DRAWITEM()
	ON_WM_SIZE()
	ON_NOTIFY_EX(TTN_NEEDTEXT, 0, OnToolTipNotify)
	//}}AFX_MSG_MAP
	ON_EN_CHANGE(IDC_MDN, OnEnChangeMdn)
	ON_EN_CHANGE(IDC_KOPFTEXT, OnEnChangeKopftext)
	ON_COMMAND(ID_DATEI_BEENDEN, OnDateiBeenden)
	ON_COMMAND(ID_DATEI_AUSF, OnDateiAusf)
	ON_CBN_SELCHANGE(IDC_RECHTYP, OnCbnSelchangeRechtyp)
	ON_COMMAND(ID_PRINT, OnPrint)
	ON_COMMAND(ID__INFO, OnInfo)
	ON_COMMAND(ID_DATEI_DRUCKERWAHL, OnDateiDruckerwahl)
	ON_EN_SETFOCUS(IDC_MDN, OnEnSetfocusMdn)
	ON_EN_KILLFOCUS(IDC_DATE_VON, OnEnKillfocusdatvon)
	ON_EN_KILLFOCUS(IDC_DATE_BIS, OnEnKillfocusdatbis)
	ON_EN_KILLFOCUS(IDC_DATE_RECH, OnEnKillfocusdatrech)
END_MESSAGE_MAP()

void CRechPrint::OnEnSetfocusMdn()
{
/*
	CWnd *cWnd = GetDlgItem (IDC_KGRU2_VON);
	if (cWnd != NULL) cWnd->EnableWindow(FALSE);
	CWnd *cWnd1 = GetDlgItem (IDC_KUGRU2_BIS);
	if (cWnd1 != NULL) cWnd1->EnableWindow(FALSE);
	CWnd *cWnd2 = GetDlgItem (IDC_KOPFTEXT);
	if (cWnd2 != NULL) cWnd2->EnableWindow(FALSE);
	CWnd *cWnd3 = GetDlgItem (IDC_FUSSTEXT);
	if (cWnd3 != NULL) cWnd3->EnableWindow(FALSE);
*/

}

void CRechPrint::OnEnKillfocusdatvon()
{
	UpdateData (TRUE);
	DbClass.FromGerDate (&Datum, m_date_von.GetBuffer (0));
    m_date_von = DbClass.ToGerDate4 (&Datum, m_date_von.GetBuffer (0));
	CWnd *cWnd = GetDlgItem (IDC_DATE_VON);
	if (cWnd != NULL)
	{
		cWnd->SetWindowText (m_date_von);
	}
}
void CRechPrint::OnEnKillfocusdatbis()
{
	UpdateData (TRUE);
	DbClass.FromGerDate (&Datum, m_date_bis.GetBuffer (0));
    m_date_von = DbClass.ToGerDate4 (&Datum, m_date_bis.GetBuffer (0));
	CWnd *cWnd = GetDlgItem (IDC_DATE_BIS);
	if (cWnd != NULL)
	{
		cWnd->SetWindowText (m_date_bis);
	}
}
void CRechPrint::OnEnKillfocusdatrech()
{
	UpdateData (TRUE);

	DbClass.FromGerDate (&Datum, m_date_rech.GetBuffer (0));
    m_date_von = DbClass.ToGerDate4 (&Datum, m_date_rech.GetBuffer (0));
	CWnd *cWnd = GetDlgItem (IDC_DATE_RECH);
	if (cWnd != NULL)
	{
		cWnd->SetWindowText (m_date_rech);
	}
}

BOOL CRechPrint::OnInitDialog()
{
	CDialog::OnInitDialog();
	SetWindowText (Caption.GetBuffer (0)); 

	Menu.LoadMenu (IDR_MENU2);
	SetMenu (&Menu);
    Border.SetType (LOW);
    Border.SetYSpace (10);
    EnableToolTips (TRUE);
    ToolButton.Create (this);
/*
	ToolButton.SetVisible (IDC_PRINT2, FALSE);
	ToolButton.SetVisible (IDC_F7, FALSE);
*/

//	ToolButton.Destroy (IDC_PRINT2);
	ToolButton.Destroy (IDC_F7);
	ToolButton.Destroy (IDC_LINE);
	ToolButton.Destroy (IDC_LINE);
    ToolButton.CreateStdToolbar ();

	ToolButton.SetTooltipText (_T("aus freigeg. erstellen"), IDC_F12);

    CToolbarItem *TbPrintChoise = new CToolbarItem (IDC_PRCHOICE, CString (" Druckerwahl "), this);
    ToolButton.Add (TbPrintChoise); 

    CRect Rect;

    Rect.left = 0;
    Rect.top = 0;
    Rect.right = 0;
    Rect.bottom = 0;
    CForm::HelpName = "16550.cmg";
    int ret = StatusBar.Create (WS_CHILD | WS_VISIBLE | 
                                CBRS_BOTTOM | SBARS_SIZEGRIP, 
                                Rect, this, 5012); 
	int StatusParts [] = {400, -1};
    StatusBar.SetWindowText ("Ready");
	StatusBar.SetParts (2, StatusParts);
	StatusBar.GetWindowRect (&StatusRect);

	ptab.SetPtItem (CString ("sam_rech"));
	ptab.Read ();
	ptab.FirstPosition ();
	CSam_rech * Sam_rech;
	Rechtyp = (CComboBox *) GetDlgItem (IDC_RECHTYP);
	while ((Sam_rech = (CSam_rech *) ptab.GetNext ()) != NULL)
	{
       	 Rechtyp->InsertString (-1, Sam_rech->ptbez);
	}
	Rechtyp->SetCurSel (0);
	Rechtyp->ShowWindow (SW_HIDE);

	ptab.FirstPosition ();
	ptab.GetNext ();
	RechtypList = (CListBox *) GetDlgItem (IDC_RECHTYPLIST);
	while ((Sam_rech = (CSam_rech *) ptab.GetNext ()) != NULL)
	{
       	 RechtypList->InsertString (-1, Sam_rech->ptbez);
	}


	return TRUE;
}

BOOL CRechPrint::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Speziellen Code hier einf�gen und/oder Basisklasse aufrufen
 	 CWnd *cWnd;

	 if (pMsg->message == WM_KEYDOWN)
	 {
		 switch (pMsg->wParam)
			 {
		 
		        case VK_RETURN :

//                     if (listView1 != (CListCtrl *) GetFocus ())
                     {
          	 		        NextDlgCtrl();
			                return TRUE;
					 }
//					 SelectRech ();
					 return TRUE;
			 case VK_F5 :
                     OnCancel ();
 			         return TRUE;
			 case VK_F7 :
                     OnF7 ();
 			         return TRUE;
             case VK_F9 :
//		             OnKey9 ();
                     break;
		     case VK_F12 :
                     OnF12 ();
				     return TRUE;

 		    case VK_DOWN :
				cWnd = GetFocus ();
				if (cWnd == RechtypList) break;
				if (cWnd == Rechtyp) break;
  	            NextDlgCtrl();
			    return TRUE;
 		    case VK_UP :
				cWnd = GetFocus ();
				if (cWnd == RechtypList) break;
				if (cWnd == Rechtyp) break;
  	            PrevDlgCtrl();
				return TRUE;
		 }
	 }
     return CDialog::PreTranslateMessage(pMsg);

}

void CRechPrint::OnPaint() 
{
	{
		CPaintDC cDC (this); // Ger�tekontext f�r Zeichnen
        Border.Print (this, &cDC, 50);
		CDialog::OnPaint();
	}
}

void CRechPrint::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	// TODO: Code f�r die Behandlungsroutine f�r Nachrichten hier einf�gen und/oder Standard aufrufen

    if (ToolButton.DrawToolbar (nIDCtl, lpDrawItemStruct->hDC)) 
    {
        return;
    }

	CDialog::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

void CRechPrint::OnF5() 
{
	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen

    OnCancel ();
}

void CRechPrint::OnF7() 
{
	RechPrint ();
}


void CRechPrint::OnF12() 
{
	RechFree ();
}

/* ---> die tokens heissen im rechbearb.rsr aber von Anfang an anders,
daher passt nur recht wenig  ..... alles neu am 160513 

void CRechPrint::MakeParams ()
{
	int idx = Rechtyp->GetCurSel ();
	if (idx == -1) idx = 0;

	CSam_rech *Sam_rech = (CSam_rech *) ptab.Get (idx);
	CString sam_rech = Sam_rech->ptwert;


	m_date_rech.Trim ();
	m_date_von.Trim ();
	m_date_bis.Trim ();
	m_bran_von.Trim ();
	m_bran_bis.Trim ();
	sam_rech.Trim ();
	Params.Format ("mdn=%hd;"
 		           "rech_dat=%s;"
 		           "dat_von=%s;"
				   "dat_bis=%s;"
				   "kun_von=%ld;"
				   "kun_bis=%ld;"
				   "branche_von=%s;"
				   "branche_bis=%s;"
				   "kugru1_von=%hd;"
				   "kugru1_bis=%hd"
				   "kugru2_von=%hd;"
				   "kugru2_bis=%hd;"
				   "sam_rech=%s;"
				   "kopf_txt=%ld;"
				   "fuss_txt=%ld;"
				   "fil=%s",
				   m_mdn,
				   m_date_rech,
				   m_date_von,
				   m_date_bis,
				   m_kun_von,
				   m_kun_bis,
				   m_bran_von,
				   m_bran_bis,
				   m_kugru1_von,
				   m_kugru1_bis,
				   m_kugru2_von,
				   m_kugru2_bis,
				   sam_rech,
				   m_kopftext,
				   m_fusstext,
				   m_fil);
}

void CRechPrint::MakeParams (CSam_rech *Sam_rech)
{
	CString sam_rech = Sam_rech->ptwert;

	m_date_rech.Trim ();
	m_date_von.Trim ();
	m_date_bis.Trim ();
	m_bran_von.Trim ();
	m_bran_bis.Trim ();
	sam_rech.Trim ();
	Params.Format ("mdn=%hd;"
 		           "rech_dat=%s;"
 		           "dat_von=%s;"
				   "dat_bis=%s;"
				   "kun_von=%ld;"
				   "kun_bis=%ld;"
				   "branche_von=%s;"
				   "branche_bis=%s;"
				   "kugru1_von=%hd;"
				   "kugru1_bis=%hd"
				   "kugru2_von=%hd;"
				   "kugru2_bis=%hd;"
				   "sam_rech=%s;"
				   "kopf_txt=%ld;"
				   "fuss_txt=%ld;"
				   "fil=%s",
				   m_mdn,
				   m_date_rech,
				   m_date_von,
				   m_date_bis,
				   m_kun_von,
				   m_kun_bis,
				   m_bran_von,
				   m_bran_bis,
				   m_kugru1_von,
				   m_kugru1_bis,
				   m_kugru2_von,
				   m_kugru2_bis,
				   sam_rech,
				   m_kopftext,
				   m_fusstext,
				   m_fil);
}
<------------------ */


/* ---> die tokens heissen im rechbearb.rsr aber von Anfang an anders,
daher funktionierte manches bisher nicht  ..... alles neu am 160513 
< -------------- */

void CRechPrint::MakeParams ()
{
	int idx = Rechtyp->GetCurSel ();
	if (idx == -1) idx = 0;

	CSam_rech *Sam_rech = (CSam_rech *) ptab.Get (idx);
	CString sam_rech = Sam_rech->ptwert;


	m_date_rech.Trim ();
	m_date_von.Trim ();
	m_date_bis.Trim ();
	m_bran_von.Trim ();
	m_bran_bis.Trim ();
	sam_rech.Trim ();

	if ( exnutzer[0] != '\0' )
		Params.Format ("mdn=%hd;"
 		           "rech_dat=%s;"
 		           "dat_von=%s;"
				   "dat_bis=%s;"
				   "kun_von=%ld;"
				   "kun_bis=%ld;"
				   "bran_von=%s;"
				   "bran_bis=%s;"
				   "kugru_von=%hd;"
				   "kugru_bis=%hd;"		// hier fehlte ein semikolon 180814
//				   "kugru2_von=%hd;"
//				   "kugru2_bis=%hd;"
				   "sam_rech=%s;"
				   "kopf_txt=%ld;"
				   "fuss_txt=%ld;"
				   "fil=%s;"
				   "nutzer=%s;",
				   m_mdn,
				   m_date_rech,
				   m_date_von,
				   m_date_bis,
				   m_kun_von,
				   m_kun_bis,
				   m_bran_von,
				   m_bran_bis,
				   m_kugru1_von,
				   m_kugru1_bis,
//				   m_kugru2_von,
//				   m_kugru2_bis,
				   sam_rech,
				   m_kopftext,
				   m_fusstext,
				   m_fil,
				   exnutzer);
	else
		Params.Format ("mdn=%hd;"
 		           "rech_dat=%s;"
 		           "dat_von=%s;"
				   "dat_bis=%s;"
				   "kun_von=%ld;"
				   "kun_bis=%ld;"
				   "bran_von=%s;"
				   "bran_bis=%s;"
				   "kugru_von=%hd;"
				   "kugru_bis=%hd;"	// 180814 : hier fehlte ein semikolon
//				   "kugru2_von=%hd;"
//				   "kugru2_bis=%hd;"
				   "sam_rech=%s;"
				   "kopf_txt=%ld;"
				   "fuss_txt=%ld;"
				   "fil=%s;",
				   m_mdn,
				   m_date_rech,
				   m_date_von,
				   m_date_bis,
				   m_kun_von,
				   m_kun_bis,
				   m_bran_von,
				   m_bran_bis,
				   m_kugru1_von,
				   m_kugru1_bis,
//				   m_kugru2_von,
//				   m_kugru2_bis,
				   sam_rech,
				   m_kopftext,
				   m_fusstext,
				   m_fil);
}

void CRechPrint::MakeParams (CSam_rech *Sam_rech)
{
	CString sam_rech = Sam_rech->ptwert;

	m_date_rech.Trim ();
	m_date_von.Trim ();
	m_date_bis.Trim ();
	m_bran_von.Trim ();
	m_bran_bis.Trim ();
	sam_rech.Trim ();
	if ( exnutzer[0] != '\0' )
		Params.Format ("mdn=%hd;"
 		           "rech_dat=%s;"
 		           "dat_von=%s;"
				   "dat_bis=%s;"
				   "kun_von=%ld;"
				   "kun_bis=%ld;"
				   "bran_von=%s;"
				   "bran_bis=%s;"
				   "kugru_von=%hd;"
				   "kugru_bis=%hd;"	// 180814 : hier fehlte ein semikolon
//				   "kugru2_von=%hd;"
//				   "kugru2_bis=%hd;"
				   "sam_rech=%s;"
				   "kopf_txt=%ld;"
				   "fuss_txt=%ld;"
				   "fil=%s;"
				   "nutzer=%s;",
				   m_mdn,
				   m_date_rech,
				   m_date_von,
				   m_date_bis,
				   m_kun_von,
				   m_kun_bis,
				   m_bran_von,
				   m_bran_bis,
				   m_kugru1_von,
				   m_kugru1_bis,
//				   m_kugru2_von,
//				   m_kugru2_bis,
				   sam_rech,
				   m_kopftext,
				   m_fusstext,
				   m_fil,
				   exnutzer);
	else
		Params.Format ("mdn=%hd;"
 		           "rech_dat=%s;"
 		           "dat_von=%s;"
				   "dat_bis=%s;"
				   "kun_von=%ld;"
				   "kun_bis=%ld;"
				   "bran_von=%s;"
				   "bran_bis=%s;"
				   "kugru_von=%hd;"
				   "kugru_bis=%hd;"		// 180814 : hier fehlte ein semikolon
//				   "kugru2_von=%hd;"
//				   "kugru2_bis=%hd;"
				   "sam_rech=%s;"
				   "kopf_txt=%ld;"
				   "fuss_txt=%ld;"
				   "fil=%s;",
				   m_mdn,
				   m_date_rech,
				   m_date_von,
				   m_date_bis,
				   m_kun_von,
				   m_kun_bis,
				   m_bran_von,
				   m_bran_bis,
				   m_kugru1_von,
				   m_kugru1_bis,
//				   m_kugru2_von,
//				   m_kugru2_bis,
				   sam_rech,
				   m_kopftext,
				   m_fusstext,
				   m_fil);
}



void CRechPrint::RechFree ()
{
	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen
	CProcess process;

	LPSTR bws = getenv ("BWS");
	if (bws == NULL) return;

	Programm = PROGRAMM;
  
	UpdateData (TRUE);

	int SelItems [MAXTYPES];

	int Items = RechtypList->GetSelItems (MAXTYPES, SelItems);

	CString Message;
	Message.Format (_T("Rechnungen werden erstellt"));
	StatusBar.SetWindowText (Message.GetBuffer (0));
	for (int i = 0; i < Items; i ++)
	{
		int idx = SelItems[i];
		CSam_rech *Sam_rech = (CSam_rech *) ptab.Get (idx + 1);
		// Das ist aber kritisch, falls irgendjemand die ptab verbiegt
		MakeParams (Sam_rech);
		Command.Format ("%s F %s", Programm.GetBuffer (0), Params.GetBuffer (0));
		process.SetCommand (Command);
		process.Start ();
		process.WaitForEnd ();
	}

	StatusBar.SetWindowText ("Ready");
}

void CRechPrint::RechPrint ()
{
	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen
	CProcess process;

	LPSTR bws = getenv ("BWS");
	if (bws == NULL) return;

	Programm = PROGRAMM;

	UpdateData (TRUE);

	int SelItems [MAXTYPES];

	int Items = RechtypList->GetSelItems (MAXTYPES, SelItems);

	CString Message;
	Message.Format (_T("Rechnungen werden erstellt"));
	StatusBar.SetWindowText (Message.GetBuffer (0));
	for (int i = 0; i < Items; i ++)
	{
		int idx = SelItems[i];
		CSam_rech *Sam_rech = (CSam_rech *) ptab.Get (idx + 1);
		MakeParams (Sam_rech);
	    Command.Format ("%s D %s", Programm.GetBuffer (0), Params.GetBuffer (0));
	    process.SetCommand (Command);
		process.Start ();
		process.WaitForEnd ();
	}
    StatusBar.SetWindowText ("Ready");
}

void CRechPrint::SetDbase (LPSTR database)
{
	DbClass.opendbase (database);
	ptab.SetDb (&DbClass);
}

BOOL CRechPrint::OnToolTipNotify(UINT nIDCtl,NMHDR *pNMHDR, LRESULT *pResult)
{
    if (ToolButton.OnToolTipNotify (nIDCtl, pNMHDR, pResult))
    {
        return TRUE;
    }
    TOOLTIPTEXT *pTTT = (TOOLTIPTEXT *) pNMHDR;
    return FALSE;
}

void CRechPrint::OnSize(UINT nType, int cx, int cy)
{
	CRect Rect;
	CRect pRect;

	if (StatusBar.m_hWnd == NULL)
	{
		return;
	}

	GetWindowRect (&pRect);
    StatusRect.top = pRect.bottom - pRect.top - 5;

	StatusBar.MoveWindow (StatusRect.left, 
		                      StatusRect.top,
		                      StatusRect.right - StatusRect.left,
		                      StatusRect.bottom - StatusRect.top,TRUE);
/*
     CDC *cDC = GetDC ();
     Border.Print (this, cDC, 50);
	 ReleaseDC (cDC);
*/
	InvalidateRect (NULL, TRUE);
}

void CRechPrint::OnEnChangeMdn()
{
	// TODO:  Falls dies ein RICHEDIT-Steuerelement ist, wird das Kontrollelement
	// diese Benachrichtigung nicht senden, es sei denn, Sie setzen den CDialog::OnInitDialog() au�er Kraft.
	// Funktion und Aufruf CRichEditCtrl().SetEventMask()
	// mit dem ENM_CHANGE-Flag ORed in der Eingabe.

	// TODO:  F�gen Sie hier Ihren Code f�r die Kontrollbenachrichtigungsbehandlung ein.
}

void CRechPrint::OnEnChangeKopftext()
{
	// TODO:  Falls dies ein RICHEDIT-Steuerelement ist, wird das Kontrollelement
	// diese Benachrichtigung nicht senden, es sei denn, Sie setzen den CDialog::OnInitDialog() au�er Kraft.
	// Funktion und Aufruf CRichEditCtrl().SetEventMask()
	// mit dem ENM_CHANGE-Flag ORed in der Eingabe.

	// TODO:  F�gen Sie hier Ihren Code f�r die Kontrollbenachrichtigungsbehandlung ein.
}

void CRechPrint::OnDateiBeenden()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	OnCancel ();
}

void CRechPrint::OnDateiAusf()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	OnF12 ();
}

void CRechPrint::OnCbnSelchangeRechtyp()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
}

void CRechPrint::OnPrint()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	OnF7 ();
}

int CRechPrint::sysdate (char *datum)
{
 time_t timer;
 struct tm *ltime;

 time (&timer);
 ltime = localtime (&timer);
 sprintf (datum, "%02d.%02d.%04d", ltime->tm_mday,
                                   ltime->tm_mon + 1,
                                   ltime->tm_year + 1900);
 return (0);
}


ULARGE_INTEGER CRechPrint::GetNanoDay ()
{
    SYSTEMTIME st;
    FILETIME ft;
    ULARGE_INTEGER ul1;
    ULARGE_INTEGER ul2;

	st.wDay = 1;
    st.wMonth  = 1;
	st.wYear  = 1989;
	st.wHour  = 0;
	st.wMinute  = 0;
	st.wSecond  = 0;
	st.wMilliseconds = 0;
	SystemTimeToFileTime (&st, &ft);
    memcpy (&ul1, &ft, sizeof (ft));

	st.wDay = 2;
    st.wMonth  = 1;
	st.wYear  = 1989;
	st.wHour  = 0;
	st.wMinute  = 0;
	st.wSecond  = 0;
	st.wMilliseconds = 0;
	SystemTimeToFileTime (&st, &ft);
    memcpy (&ul2, &ft, sizeof (ft));

	ul2.QuadPart -= ul1.QuadPart ;
	return ul2;
}

void CRechPrint::AddDate (char *datum, long days)
{
    SYSTEMTIME st1;
    FILETIME ft1;
    ULARGE_INTEGER ul1;
    ULARGE_INTEGER ulday;

	char num2[] = {"00"};
	char num4[] = {"0000"};
    memcpy (num2, &datum[0], 2);
	st1.wDay = atoi (num2);
    memcpy (num2, &datum[3], 2);
    st1.wMonth  = atoi (num2);
    memcpy (num4, &datum[6], 4);
	st1.wYear  = atoi (num4);
	st1.wHour  = 0;
	st1.wMinute  = 0;
	st1.wSecond  = 0;
	st1.wMilliseconds = 0;

	SystemTimeToFileTime (&st1, &ft1);
    memcpy (&ul1, &ft1, sizeof (ft1));
	ulday = GetNanoDay ();
	ul1.QuadPart += ulday.QuadPart * days;
    memcpy (&ft1, &ul1, sizeof (ft1));
	FileTimeToSystemTime (&ft1, &st1);
	CString Date;
	Date.Format ("%02hd.%02hd.%04hd",st1.wDay, st1.wMonth, st1.wYear);
	strcpy (datum, Date.GetBuffer (0));
}

void CRechPrint::OnInfo()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	CAboutDialog dlg;
	dlg.DoModal ();
}

void CRechPrint::OnDateiDruckerwahl()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.


	int llrech1 = 0 ;

    CSys_par Sys_par;
 	Sys_par.SetDb (&DbClass);
	sprintf ( Sys_par.sys_par_nam , "llrech1");
	if (Sys_par.Read () == 0)
	{ 
		llrech1 = atoi ( Sys_par.sys_par_wrt );
		if ( llrech1 != 1 ) llrech1 = 0 ;
	}
	

	CProcess process;
	if ( llrech1 )
	{
		// dr561 -wahl 1 -formtyp 5610000 -nutzer " # clipped ( sys_ben.pers_nam )

		char kommando [256];
		if ( exnutzer[0] != '\0' )
			sprintf ( kommando, "dr561 -wahl 1 -formtyp 5610000 -nutzer %s" , exnutzer );
		else
			sprintf ( kommando, "dr561 -wahl 1 -formtyp 5610000" );

		process.SetCommand (kommando);
	}
	else
		process.SetCommand ("sprinter");
    process.Start ();
	process.WaitForEnd ();
}
