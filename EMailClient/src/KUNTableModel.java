import java.io.IOException;
import java.sql.SQLException;
import java.util.Iterator;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;


public class KUNTableModel implements TableModel {
	
	private Kunden kunden;
	private DataGenerator dataGenerator;
	
	public KUNTableModel (final DataGenerator dataGenerator) {		

		try {
			this.dataGenerator = dataGenerator;
			this.kunden = this.dataGenerator.getKunden();
			this.dataGenerator.getAlleRechnungen(this.kunden);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}

	@Override
	public void addTableModelListener(TableModelListener l) {		
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return Object.class;
	}

	@Override
	public int getColumnCount() {
		return 5;
	}

	@Override
	public String getColumnName(int columnIndex) {
		
		switch(columnIndex) {
		case 0:
			return "Kunde";
		case 1:
			return "Bezeichnung";
		case 2:
			return "Ort";
		case 3:
			return "eMail";
		case 4:
			return "Partner";
		}
		return "";
	}

	@Override
	public int getRowCount() {		
		return this.kunden.getKundenSize();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {		
		return this.kunden.getValueAt(rowIndex, columnIndex);
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;		
	}

	@Override
	public void removeTableModelListener(TableModelListener l) {		
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {		
		this.kunden.setValueAt(aValue, rowIndex, columnIndex, this.dataGenerator);		
	}
	
	
	public Iterator<Kunde> getKunden() {		
		return this.kunden.getKunden();		
	}
	
	
	public Kunde getKunde(final int row) {
		int kun = (int) this.kunden.getValueAt(row, 0);
		return this.kunden.getKunde(kun);
	}

}
