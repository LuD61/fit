

import java.sql.Date;

public class Log {
	
	
	private Date rech_dat;
	private Date versandt;
	private int kun;
	private int rech_nr;
	private int send_stat;
	
		
	public Log(Date rech_dat, Date versandt, int kun, int rech_nr, int send_stat) {
		super();
		this.rech_dat = rech_dat;
		this.versandt = versandt;
		this.kun = kun;
		this.rech_nr = rech_nr;
		this.send_stat = send_stat;
	}
	
	
	public Date getRech_dat() {
		return rech_dat;
	}
	public void setRech_dat(Date rech_dat) {
		this.rech_dat = rech_dat;
	}
	public Date getVersandt() {
		return versandt;
	}
	public void setVersandt(Date versandt) {
		this.versandt = versandt;
	}
	public int getKun() {
		return kun;
	}
	public void setKun(int kun) {
		this.kun = kun;
	}
	public int getRech_nr() {
		return rech_nr;
	}
	public void setRech_nr(int rech_nr) {
		this.rech_nr = rech_nr;
	}
	public int getSend_stat() {
		return send_stat;
	}
	public void setSend_stat(int send_stat) {
		this.send_stat = send_stat;
	}

	
	

}
