import java.util.ArrayList;





public class Kunde {
	
	private int kun;
	private int mdn;
	private String adr_nam1;
	private String plz;
	private String ort1;
	private String email;
	private String partner;
	private ArrayList<Rechnung> rechnungen;
	
	
	public Kunde(final int kun, final String adr_nam1, final String plz, final String ort1, final String email, final String partner, int mdn) {
		
		this.setKun(kun);
		this.setMdn(mdn);
		this.setAdr_nam1(adr_nam1);
		this.setPlz(plz);
		this.setOrt1(ort1);
		this.setEmail(email);
		this.setPartner(partner);
		this.setRechnungen(new ArrayList<Rechnung>());
	}


	public int getKun() {
		return kun;
	}


	public void setKun(int kun) {
		this.kun = kun;
	}


	public String getAdr_nam1() {
		return adr_nam1;
	}


	public void setAdr_nam1(String adr_nam1) {
		this.adr_nam1 = adr_nam1;
	}


	public String getPlz() {
		return plz;
	}


	public void setPlz(String plz) {
		this.plz = plz;
	}


	public String getOrt1() {
		return ort1;
	}


	public void setOrt1(String ort1) {
		this.ort1 = ort1;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getPartner() {
		return partner;
	}


	public void setPartner(String partner) {
		this.partner = partner;
	}


	/**
	 * @return the mdn
	 */
	public int getMdn() {
		return mdn;
	}


	/**
	 * @param mdn the mdn to set
	 */
	public void setMdn(int mdn) {
		this.mdn = mdn;
	}


	/**
	 * @return the rechnungen
	 */
	public ArrayList<Rechnung> getRechnungen() {
		return rechnungen;
	}

	
	/**
	 * @return the rechnungen
	 */
	public ArrayList<Rechnung> getRechnungen(int[] i) {
		
		final ArrayList<Rechnung> temp = new ArrayList<Rechnung>();
		for (int j = 0; j < i.length; j++) {
			temp.add(this.rechnungen.get(i[j]));
		}
		return temp;		
	}
	

	/**
	 * @param rechnungen the rechnungen to set
	 */
	public void setRechnungen(ArrayList<Rechnung> rechnungen) {
		this.rechnungen = rechnungen;
	}
	
	
	public void addRechnung(final Rechnung rechnung) {
		this.rechnungen.add(rechnung);
	}
	
	
	
	
}
