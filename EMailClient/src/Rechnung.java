

import java.sql.Date;

public class Rechnung {
	
		
	private Date rech_dat;
	private int rech_nr;
	private int send_stat;
	private double rech_bto_eur;
	
	
	public Rechnung(Date rech_dat, int rech_nr, double rech_bto_eur, int sendStat) {
		this.rech_dat = rech_dat;
		this.rech_nr = rech_nr;
		this.rech_bto_eur = rech_bto_eur;
		this.setSend_stat(sendStat);
	}


	public Date getRech_dat() {
		return rech_dat;
	}


	public void setRech_dat(Date rech_dat) {
		this.rech_dat = rech_dat;
	}


	public int getRech_nr() {
		return rech_nr;
	}


	public void setRech_nr(int rech_nr) {
		this.rech_nr = rech_nr;
	}


	public double getRech_bto_eur() {
		return rech_bto_eur;
	}


	public void setRech_bto_eur(double rech_bto_eur) {
		this.rech_bto_eur = rech_bto_eur;
	}


	/**
	 * @return the send_stat
	 */
	public int getSend_stat() {
		return send_stat;
	}


	/**
	 * @param send_stat the send_stat to set
	 */
	public void setSend_stat(int send_stat) {
		this.send_stat = send_stat;
	}

}
