

import java.util.ArrayList;
import java.util.Iterator;


public class Kunden {
	
	private ArrayList<Kunde> email_kunden = null;
	
	public Kunden() {		
		this.email_kunden = new ArrayList<Kunde>();
	}
	
	
	public Iterator<Kunde> getKunden() {		
		return this.email_kunden.iterator();
	}
	
	
	public void addKunde(final Kunde kunde) {
		this.email_kunden.add(kunde);
	}
	
	
	public int getKundenSize() {
		return this.email_kunden.size();
	}
	
	
	public Kunde getKunde(final int kun) {
		
		Iterator<Kunde> it_kunde = this.email_kunden.iterator();
		
		while (it_kunde.hasNext()) {
			Kunde kunde = (Kunde) it_kunde.next();
			if ( kunde.getKun() == kun ) {
				return kunde;
			}
		}
		return null;			
	}
	
	
	public Object getValueAt(int rowIndex, int columnIndex) {
		
		final Kunde kunde = this.email_kunden.get(rowIndex);
		
		switch ( columnIndex ) {
		
		case 0:
			return kunde.getKun();
		case 1:
			return kunde.getAdr_nam1();
		case 2:
			return kunde.getOrt1();
		case 3:
			return kunde.getEmail();
		case 4:
			return kunde.getPartner();			
		}
		
		return "";
	}
	
	
	public void setValueAt(Object newValue, int rowIndex, int columnIndex, final DataGenerator dataGenerator) {
		
		final Kunde kunde = this.email_kunden.get(rowIndex);
		
		switch ( columnIndex ) {
		
		case 0:
			kunde.setKun(Integer.valueOf((String) newValue).intValue());
			break;
		case 1:
			kunde.setAdr_nam1((String) newValue);
			break;
		case 2:
			kunde.setOrt1((String) newValue);
			break;
		case 3:
			kunde.setEmail((String) newValue);
			break;
		case 4:
			kunde.setPartner((String) newValue);
			break;
		}
		
		dataGenerator.saveValues(kunde);
		
	}
	
}
