import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.mail.SendFailedException;
import javax.swing.JFrame;
import javax.swing.border.EmptyBorder;
import javax.swing.JTable;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import javax.swing.ListSelectionModel;
import javax.swing.JLabel;
import java.awt.Font;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JLayeredPane;
import javax.swing.JButton;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

import net.atlanticbb.tantlinger.io.IOUtils;
import net.atlanticbb.tantlinger.shef.HTMLEditorPane;

import org.jfree.chart.ChartPanel;

import MailService.SendJavaMail;

import fit.informixconnector.InformixConnector;

import javax.swing.JTabbedPane;
import java.awt.Color;
import javax.swing.border.BevelBorder;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import java.awt.CardLayout;
import javax.swing.JList;
import javax.swing.JTextArea;
import javax.swing.JComboBox;
import java.awt.GridLayout;
import java.awt.FlowLayout;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.AbstractListModel;
import javax.swing.border.SoftBevelBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.Icon;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;



public class KundenDialog extends JFrame {

	private JLayeredPane contentPane;
	private static InformixConnector informixConnector;
	private JTable table;
	private HTMLEditorPane editorPane;
	private KUNTableModel tableModel;
	private SendJavaMail sendJavaMail;
	private File[] files;
	private JTextField tBetreff;
	private JTable table_1;
	private ArtikelTabelle tableModel2;
	private RechTableModel rechModel;
	private JTable table_2;
	private JTable table_3;
	private DataGenerator dataGenerator;

	/**
	 * Launch the application.
	 */
	
	
	public static void main(String[] args) {		
		
		try {
			
			informixConnector = new InformixConnector();			
			informixConnector.createConnection();
			
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			KundenDialog frame = new KundenDialog();					
			frame.setVisible(true);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	

	/**
	 * Create the frame.
	 */
	public KundenDialog() {		
				
		dataGenerator = new DataGenerator(informixConnector);
		editorPane = new HTMLEditorPane();
		sendJavaMail = new SendJavaMail();
		String emailRechFilePath = System.getenv("BWS");
		emailRechFilePath = emailRechFilePath + "\\e-mail-rechnung";
		
		File file = new File(emailRechFilePath);
		files = file.listFiles();		
		
		setTitle("eMail Rechnungsversand");	
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1100, 771);
		contentPane = new JLayeredPane();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(10, 72, 1064, 633);
		contentPane.add(tabbedPane);
		
		tableModel = new KUNTableModel(dataGenerator);
		tableModel2 = new ArtikelTabelle(dataGenerator);
		rechModel = new RechTableModel();
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(Color.WHITE);
		tabbedPane.addTab("Kunden f\u00FCr eMail Rechnung", (Icon) null, panel_2, null);
		panel_2.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 1039, 583);
		panel_2.add(scrollPane);
		table = new JTable(tableModel);		
		table.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		scrollPane.setViewportView(table);
		table.setModel(tableModel);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setFillsViewportHeight(true);
		table.setCellSelectionEnabled(false);
		table.setRowSelectionAllowed(true);
		table.getTableHeader().setResizingAllowed(true);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		tabbedPane.addTab("Aktuelle Rechnungen an Kunden senden", null, panel, null);
		
		editorPane.setBounds(10, 46, 627, 548);
		panel.add(editorPane);
		panel.setLayout(null);
				
		JButton btnNewButton = new JButton("HTML Vorlage \u00F6ffnen");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
					openFile(editorPane);
			}			
		});
		btnNewButton.setBounds(664, 36, 343, 47);
		panel.add(btnNewButton);
		
		JButton btnRechnungenVerschicken = new JButton("Rechnungen an alle Kunden verschicken");
		btnRechnungenVerschicken.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {			
				rechnungenVerschicken();				
			}
		});
		btnRechnungenVerschicken.setBounds(664, 107, 343, 47);
		panel.add(btnRechnungenVerschicken);
		
		tBetreff = new JTextField();
		tBetreff.setBounds(66, 11, 571, 20);
		panel.add(tBetreff);
		tBetreff.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Betreff");
		lblNewLabel.setBounds(10, 14, 46, 14);
		panel.add(lblNewLabel);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(Color.WHITE);
		tabbedPane.addTab("Rechnung an Kunde senden", null, panel_3, null);
		tabbedPane.setEnabledAt(2, true);
		panel_3.setLayout(null);
		
		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(10, 11, 522, 581);
		panel_3.add(scrollPane_2);
		
		table_2 = new JTable((TableModel) null);
		table_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {				
				final Kunde kunde = tableModel.getKunde(table_2.getSelectedRow());
				rechModel.setRechnungen(kunde.getRechnungen());
				table_3.updateUI();
			}
		});
		scrollPane_2.setViewportView(table_2);
		table_2.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table_2.setFillsViewportHeight(true);
		table_2.setCellSelectionEnabled(false);
		table_2.setRowSelectionAllowed(true);
		table_2.setModel(tableModel);
		table_2.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		
		JScrollPane scrollPane_3 = new JScrollPane();
		scrollPane_3.setBounds(582, 47, 448, 292);
		panel_3.add(scrollPane_3);
		
		table_3 = new JTable((TableModel) null);
		scrollPane_3.setViewportView(table_3);
		table_3.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		table_3.setRowSelectionAllowed(true);
		table_3.setFillsViewportHeight(true);
		table_3.setCellSelectionEnabled(false);
		table_3.setModel(rechModel);
		table_3.setRowSelectionAllowed(true);
		table_3.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		
		JButton button = new JButton("Rechnungen an Kunde verschicken");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				final Kunde kunde = tableModel.getKunde(table_2.getSelectedRow());
				final ArrayList<Rechnung> t = kunde.getRechnungen(table_3.getSelectedRows());
				String s = "";
				
				for (int i = 0; i < t.size(); i++) {
					s = s + String.valueOf(t.get(i).getRech_nr()) + " | ";
				}
				
				int i = JOptionPane.showConfirmDialog(contentPane, "M�chten Sie die Rechnung " + s + " an Kunde " + kunde.getAdr_nam1() + " versenden?", "Rechnung verschicken", JOptionPane.YES_NO_OPTION);
				if ( i == JOptionPane.YES_OPTION) {
					rechnungAnKundeVerschicken(kunde, t);	
				}				
			}
		});
		button.setBounds(642, 366, 343, 47);
		panel_3.add(button);
		
		JPanel panel_1 = new JPanel();
		panel_1.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentShown(ComponentEvent arg0) {
				
				tableModel2 = new ArtikelTabelle(dataGenerator);
				table_1.setModel(tableModel2);
				table_1.updateUI();				
			}
		});
		panel_1.setLayout(null);
		panel_1.setBackground(Color.WHITE);
		tabbedPane.addTab("\u00DCbertragungsprotokoll", null, panel_1, null);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(27, 24, 1008, 557);
		panel_1.add(scrollPane_1);
		
		table_1 = new JTable();
		table_1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane_1.setViewportView(table_1);
		table_1.setFillsViewportHeight(true);
		table_1.setCellSelectionEnabled(true);
		table_1.setModel(tableModel2);		
		table_1.setRowSelectionAllowed(true);

		
		JLabel lblEmailRechnungsversand = new JLabel("eMail Rechnungsversand");
		lblEmailRechnungsversand.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblEmailRechnungsversand.setBounds(33, 11, 974, 50);
		contentPane.add(lblEmailRechnungsversand);
	
	}	
	
	
    private void openFile(final HTMLEditorPane editor) {
		
		InputStream in = null;
		File file = null;
	    JFileChooser fc = new JFileChooser();
	    fc.setCurrentDirectory(new File(System.getenv("BWS")));	        	    
	    fc.setFileFilter( new FileNameExtensionFilter("Plaintext: txt, html", "txt", "html", "htm") );        
        fc.removeChoosableFileFilter(fc.getAcceptAllFileFilter());

	    int state = fc.showOpenDialog( null );

	    if ( state == JFileChooser.APPROVE_OPTION )
	    {
	      file = fc.getSelectedFile();
	      try {
			in = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		}       	    
	    
	    if ( file != null ) {
	    	
			try {
	            editor.setText(IOUtils.read(in));
			} catch(IOException ex) {
	            ex.printStackTrace();
			} finally {
	            IOUtils.close(in);
			}
	    	
	    }	
	
    }
    
    
    private void rechnungenVerschicken() {		
		
		final String htmlContent = editorPane.getText();				
		final Iterator<Kunde> kunden = tableModel.getKunden();
		
		String betreff = tBetreff.getText();
		if ( betreff.isEmpty() ) {
			betreff = "Ihre aktuelle Rechnung. Vielen Dank f�r Ihren Einkauf.";
		}
		
		int counter = 0;
		
		while (kunden.hasNext()) {
			Kunde kunde = (Kunde) kunden.next();
			final ArrayList<Rechnung> rechnungen = kunde.getRechnungen();				
			
			if ( rechnungen.size() > 0 ) {

				final String eMail = kunde.getEmail();											
				
				for (int i = 0; i < rechnungen.size(); i++) {
					
					if ( rechnungen.get(i).getSend_stat() == 0 ) {

						try {						
							double d = 12345.67890;
							DecimalFormat df = new DecimalFormat( "r100000000" );
							final String formattedRechNr = df.format(rechnungen.get(i).getRech_nr());
							
							File file = getFile(formattedRechNr);
							
							if ( file != null ) {
								sendJavaMail.sendEmail(htmlContent, betreff, eMail, file);
								JOptionPane.showMessageDialog(contentPane, "Email an " + kunde.getAdr_nam1() + " mit Rechnung " + rechnungen.get(i).getRech_nr() + " erfolgreich versendet.", "Email", JOptionPane.INFORMATION_MESSAGE);
								dataGenerator.loggingEmailRechnung(kunde.getMdn(), rechnungen.get(i).getRech_nr());
								counter++;
							}								
							
						} catch (SQLException | IOException e1) {
							e1.printStackTrace();
						}						
					}	
				}						
			}						
		}		
		JOptionPane.showMessageDialog(contentPane, "Es wurden " + counter + " Rechnungen versendet.", "Email", JOptionPane.INFORMATION_MESSAGE);   	
    }
    
    
    
    private void rechnungAnKundeVerschicken(final Kunde kunde, final ArrayList<Rechnung> rechnungen) {		
		
		final String htmlContent = editorPane.getText();				
		
		String betreff = tBetreff.getText();
		if ( betreff.isEmpty() ) {
			betreff = "Ihre aktuelle Rechnung. Vielen Dank f�r Ihren Einkauf.";
		}
		
		int counter = 0;				
		
		if ( rechnungen.size() > 0 ) {

			final String eMail = kunde.getEmail();											
			
			for (int i = 0; i < rechnungen.size(); i++) {
				try {
					
					double d = 12345.67890;
					DecimalFormat df = new DecimalFormat( "r100000000" );
					final String formattedRechNr = df.format(rechnungen.get(i).getRech_nr());
					
					File file = getFile(formattedRechNr);
					
					if ( file != null ) {
						sendJavaMail.sendEmail(htmlContent, betreff, eMail, file);
						JOptionPane.showMessageDialog(contentPane, "Email an " + kunde.getAdr_nam1() + " mit Rechnung " + rechnungen.get(i).getRech_nr() + " erfolgreich versendet.", "Email", JOptionPane.INFORMATION_MESSAGE);
						dataGenerator.loggingEmailRechnung(kunde.getMdn(), rechnungen.get(i).getRech_nr());
						counter++;
					}								
					
				} catch (SQLException | IOException e1) {
					e1.printStackTrace();
				}	
			}						
		}
		
		JOptionPane.showMessageDialog(contentPane, "Es wurden " + counter + " Rechnungen versendet.", "Email", JOptionPane.INFORMATION_MESSAGE);   	
    }
    
    
    
    
    private File getFile(String fileName) {
    	
    	fileName = fileName + ".PDF";
    	
    	for (int i = 0; i < files.length; i++) {
			
    		if ( files[i].getName().equals(fileName) ) {
    			return files[i];
    		}
    		
		}
    	
    	return null;
    }
}
