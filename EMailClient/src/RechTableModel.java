import java.util.ArrayList;
import java.util.Date;

import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;


public class RechTableModel extends AbstractTableModel implements TableModel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7836094489848117805L;
	private ArrayList<Rechnung> rechnungen;
	
	
	public RechTableModel () {
		super();
		this.rechnungen = new ArrayList<Rechnung>();		
	}

	@Override
	public void addTableModelListener(TableModelListener l) {		
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		
		switch(columnIndex) {
		case 0:
			return Integer.class;
		case 1:
			return Date.class;
		case 2:
			return Double.class;
		}
		return null;
	}

	@Override
	public int getColumnCount() {
		return 3;
	}

	@Override
	public String getColumnName(int columnIndex) {
		
		switch(columnIndex) {
		case 0:
			return "Rechnung Nr.";
		case 1:
			return "Rechnungsdatum";
		case 2:
			return "Betrag";
		}
		return "";
	}

	@Override
	public int getRowCount() {		
		return this.rechnungen.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		
		final Rechnung rechnung = this.rechnungen.get(rowIndex);
		
		switch ( columnIndex ) {		
			case 0:
				return rechnung.getRech_nr();
			case 1:
				return rechnung.getRech_dat();
			case 2:
				return rechnung.getRech_bto_eur();			
		}
		return rechnung;		
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;		
	}

	@Override
	public void removeTableModelListener(TableModelListener l) {		
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {				
	}
	
	public void setRechnungen(final ArrayList<Rechnung> rechungen) {
		this.rechnungen = rechungen;
		fireTableDataChanged();
	}

}
