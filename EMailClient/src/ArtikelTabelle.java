import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;


public class ArtikelTabelle extends AbstractTableModel implements TableModel {
	
	private DataGenerator dataGenerator;
	private ArrayList<Log> logs;
	
	public ArtikelTabelle (final DataGenerator dataGenerator) {		

		try {
			this.dataGenerator = dataGenerator;
			this.logs = this.dataGenerator.getLog();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void addTableModelListener(TableModelListener l) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		// TODO Auto-generated method stub
		return Object.class;
	}

	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return 5;
	}

	@Override
	public String getColumnName(int columnIndex) {
		// TODO Auto-generated method stub
		
		switch(columnIndex) {
		case 0:
			return "Kunde";
		case 1:
			return "Rechnungsnummer";
		case 2:
			return "Rechnungsdatum";
		case 3:
			return "Versandt am";
		case 4:
			return "Sendungsstatus";
		}
		return "";
	}

	@Override
	public int getRowCount() {		
		return this.logs.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {		
		Log log = this.logs.get(rowIndex);
		
		switch (columnIndex) {
		case 0:
			return log.getKun();
		case 1:
			return log.getRech_nr();
		case 2:
			return log.getRech_dat();
		case 3:
			return log.getVersandt();
		case 4:
			return log.getSend_stat();
		}
		return null;
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;		
	}

	@Override
	public void removeTableModelListener(TableModelListener l) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {		
		// TODO Auto-generated method stub
	}
	
}
