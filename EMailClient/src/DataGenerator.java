import java.io.IOException;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import fit.informixconnector.InformixConnector;

public class DataGenerator {

	private InformixConnector informixConnector;

	public DataGenerator(final InformixConnector informixConnector) {
		this.informixConnector = informixConnector;
	}

	public Kunden getKunden() throws SQLException, IOException {

		final Kunden kunden = new Kunden();
		final String stmt = "select kun.mdn, kun, adr_nam1, plz, ort1, email, partner from kun, adr where kun.adr3 = adr.adr and email_rech == 'J' order by kun;";

		final ResultSet ordersResultSet = informixConnector.executeQuery(stmt);

		while (ordersResultSet.next()) {

			final int mdn = ordersResultSet.getInt("mdn");
			final int kun = ordersResultSet.getInt("kun");
			String adr_nam1 = ordersResultSet.getString("adr_nam1");
			String plz = ordersResultSet.getString("plz");
			String ort1 = ordersResultSet.getString("ort1");
			String email = ordersResultSet.getString("email");
			String partner = ordersResultSet.getString("partner");

			final Kunde kunde = new Kunde(kun, adr_nam1, plz, ort1, email,
					partner, mdn);
			kunden.addKunde(kunde);
		}
		return kunden;
	}

	public Kunden getAlleRechnungen(Kunden kunden) throws SQLException, IOException {

		final String stmt = "select distinct rech.rech_nr, rech_dat, rech_sum_bto, rech.kun, send_stat from rech, outer emailRechLog, kun" +
							" where rech_stat = 1 and blg_typ in ('R','G') and rech.rech_nr = emailRechLog.rech_nr" +
							" and rech.kun = kun.kun " +
							" and kun.email_rech = 'J' " +				
							" order by kun, rech_dat desc, rech_nr;";

		final ResultSet ordersResultSet = informixConnector.executeQuery(stmt);

		while (ordersResultSet.next()) {

			final int kun = ordersResultSet.getInt("kun");
			final int rechNr = ordersResultSet.getInt("rech_nr");
			Date rechDat = ordersResultSet.getDate("rech_dat");
			double brutto = ordersResultSet.getDouble("rech_sum_bto");
			final int sendStat = ordersResultSet.getInt("send_stat");			

			final Rechnung rechnung = new Rechnung(rechDat, rechNr, brutto, sendStat);

			Kunde kunde = kunden.getKunde(kun);
			if (kunde != null) {
				kunde.addRechnung(rechnung);
			}
		}
		return kunden;
	}


	public ArrayList<Log> getLog() throws SQLException, IOException {

		final ArrayList<Log> logs = new ArrayList<>();

		final String stmt = "select e.rech_nr, r.kun, e.send_stat, r.rech_dat, e.versandt from emailrechlog as e, rech as r where e.rech_nr = r.rech_nr order by e.rech_nr;";

		final ResultSet ordersResultSet = informixConnector.executeQuery(stmt);

		while (ordersResultSet.next()) {

			final int rechNr = ordersResultSet.getInt("rech_nr");
			final int kun = ordersResultSet.getInt("kun");
			final int send_stat = ordersResultSet.getInt("send_stat");
			Date rechDat = ordersResultSet.getDate("rech_dat");
			Date versandt = ordersResultSet.getDate("versandt");

			final Log log = new Log(rechDat, versandt, kun, rechNr, send_stat);
			logs.add(log);
		}
		return logs;
	}

	public void saveValues(final Kunde kunde) {
		String updateStmt = "update kun set email_rech = '"
				+ kunde.getPartner() + "' where kun = " + kunde.getKun();
		this.informixConnector.executeStatement(updateStmt);
	}

	public void loggingEmailRechnung(final int mdn, final int rech_nr)
			throws SQLException, IOException {

		java.sql.Date export = new Date(new java.util.Date().getTime());
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
		String dateSendString = simpleDateFormat.format(export);

		String insertstmt = "INSERT INTO emailRechLog" + "(mdn," + "rech_nr,"
				+ "send_stat," + "versandt" + ")	VALUES (" + mdn + ","
				+ rech_nr + "," + 1 + ",'" + dateSendString + "'" + "); ";

		this.informixConnector.executeStatement(insertstmt);
	}

}
