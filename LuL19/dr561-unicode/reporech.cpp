#include "stdafx.h"
#include "DbClass.h"
#include "reporech.h"
#ifdef LL19
#include "cmbtll19.h"
#else
#ifdef LL12
#include "cmbtll12.h"
#else
#include "cmbtll11.h"
#endif
#endif

struct REPORZU reporzu, reporzu_null, reporzu1, reporzu2,reporzu3,reporzu4,reporzu5,reporzu6 ;	// 090709 260612 +4+5+6

struct REPORECH reporech, reporech_null;
extern DB_CLASS dbClass;
static long anzzfelder = -1;

int mdn = 1;
int rechnr;
TCHAR blgtyp[4] ;



int REPORECH_CLASS::dbcount (void)
/**
Tabelle reprech lesen.
**/
{

         if (test_upd_cursor == -1)
         {
             prepare ();
         }
         dbClass.sqlopen (count_cursor);
         dbClass.sqlfetch (count_cursor);
         if (sqlstatus == 0)
         {
                      return anzzfelder;
         }
         return sqlstatus;   //050803
}

int REPORZU_CLASS::lesereporzu ()
{
      int di = dbClass.sqlfetch (readcursor);
		if ( !di)
		{
			MultiByteToWideChar( 0,0, reporzu.rab_bz , -1 , reporzu.ucrab_bz ,sizeof(reporzu.ucrab_bz) );
			MultiByteToWideChar( 0,0, reporzu.gruppe , -1 , reporzu.ucgruppe ,sizeof(reporzu.ucgruppe) );
			MultiByteToWideChar( 0,0, reporzu.typ , -1 , reporzu.uctyp ,sizeof(reporzu.uctyp) );
			MultiByteToWideChar( 0,0, reporzu.wertart , -1 , reporzu.ucwertart ,sizeof(reporzu.ucwertart) );
		}
	  return di;
}



int REPORECH_CLASS::lesereporech (int fehlercode)
{
	  if (fehlercode == LL_WRN_REPEAT_DATA)
	  {
         return 0;    // Hier kein fetch , da DS nicht mehr auf Seite passt , und nochmal 
					  // ausegeben werden mu�
	  }
      int di = dbClass.sqlfetch (readcursor);
	  if ( !di )
	  {
//		  MultiByteToWideChar( CP_UTF8,0, reporech.a_bz1 , -1 , reporech.uca_bz1, sizeof(reporech.uca_bz1) );
//		  MultiByteToWideChar( CP_UTF8,0, reporech.a_bz2 , -1 , reporech.uca_bz2 ,sizeof(reporech.uca_bz2) );
		  MultiByteToWideChar( 0,0, reporech.a_bz1 , -1 , reporech.uca_bz1, sizeof(reporech.uca_bz1) );
		  MultiByteToWideChar( 0,0, reporech.a_bz2 , -1 , reporech.uca_bz2 ,sizeof(reporech.uca_bz2) );
		  MultiByteToWideChar( 0,0, reporech.a_kun , -1 , reporech.uca_kun ,sizeof(reporech.uca_kun) );

		  MultiByteToWideChar( 0,0, reporech.smwst1p , -1 , reporech.ucsmwst1p, sizeof(reporech.ucsmwst1p));
		  MultiByteToWideChar( 0,0, reporech.smwst2p , -1 , reporech.ucsmwst2p ,sizeof(reporech.ucsmwst2p));
		  MultiByteToWideChar( 0,0, reporech.smwst3p , -1 , reporech.ucsmwst3p ,sizeof(reporech.ucsmwst3p));
		  MultiByteToWideChar( 0,0, reporech.zu_stoff , -1 , reporech.uczu_stoff ,sizeof(reporech.uczu_stoff));

		  MultiByteToWideChar( 0,0, reporech.frm_name , -1 , reporech.ucfrm_name ,sizeof(reporech.ucfrm_name));
		  MultiByteToWideChar( 0,0, reporech.tel , -1 , reporech.uctel ,sizeof(reporech.uctel));
		  MultiByteToWideChar( 0,0, reporech.fax , -1 , reporech.ucfax ,sizeof(reporech.ucfax));
		  MultiByteToWideChar( 0,0, reporech.auf_me_bz , -1 , reporech.ucauf_me_bz ,sizeof(reporech.ucauf_me_bz));

		  MultiByteToWideChar( 0,0, reporech.belk_txt , -1 , reporech.ucbelk_txt ,sizeof(reporech.ucbelk_txt));
		  MultiByteToWideChar( 0,0, reporech.belf_txt , -1 , reporech.ucbelf_txt ,sizeof(reporech.ucbelf_txt));
		  MultiByteToWideChar( 0,0, reporech.zako_txt , -1 , reporech.uczako_txt ,sizeof(reporech.uczako_txt));
		  MultiByteToWideChar( 0,0, reporech.pmwsts , -1 , reporech.ucpmwsts ,sizeof(reporech.ucpmwsts));
		  MultiByteToWideChar( 0,0, reporech.ust_id , -1 , reporech.ucust_id ,sizeof(reporech.ucust_id));
		  MultiByteToWideChar( 0,0, reporech.nr_bei_rech , -1 , reporech.ucnr_bei_rech ,sizeof(reporech.ucnr_bei_rech));

		  MultiByteToWideChar( 0,0, reporech.auf_ext , -1 , reporech.ucauf_ext ,sizeof(reporech.ucauf_ext));
		  MultiByteToWideChar( 0,0, reporech.kun_nam , -1 , reporech.uckun_nam ,sizeof(reporech.uckun_nam));
		  MultiByteToWideChar( 0,0, reporech.lsret , -1 , reporech.uclsret ,sizeof(reporech.uclsret));
		  MultiByteToWideChar( 0,0, reporech.bbn , -1 , reporech.ucbbn ,sizeof(reporech.ucbbn));
		  MultiByteToWideChar( 0,0, reporech.rech_dat , -1 , reporech.ucrech_dat ,sizeof(reporech.ucrech_dat));

		  MultiByteToWideChar( 0,0, reporech.lief_me_bz , -1 , reporech.uclief_me_bz ,sizeof(reporech.uclief_me_bz));
		  MultiByteToWideChar( 0,0, reporech.sa_kz , -1 , reporech.ucsa_kz ,sizeof(reporech.ucsa_kz));
		  MultiByteToWideChar( 0,0, reporech.erf_kz , -1 , reporech.ucerf_kz ,sizeof(reporech.ucerf_kz));
		  MultiByteToWideChar( 0,0, reporech.ls_charge , -1 , reporech.ucls_charge ,sizeof(reporech.ucls_charge));

	  }

	  return di;
}

int REPORZU_CLASS::openreporzu (void)
{

	if ( readcursor < 0 ) prepare ();
         return dbClass.sqlopen (readcursor);
}

int REPORECH_CLASS::openreporech (void)
{
         return dbClass.sqlopen (readcursor);
}

void REPORECH_CLASS::prepare (void)
{


	dbClass.sqlin ((long *) &rechnr, SQLLONG, 0);
	dbClass.sqlin ((TCHAR *) blgtyp, SQLCHAR, 4);
	dbClass.sqlin ((long *) &mdn, SQLLONG, 0);
    dbClass.sqlout ((long *)  &anzzfelder, SQLLONG, 0);


    count_cursor = dbClass.sqlcursor (_T("select count(*) from reporech ")
										_T("where reporech.rech_nr = ? and reporech.blg_typ = ? ")
										_T("and reporech.mdn = ? "));
										
	test_upd_cursor = 1;


	dbClass.sqlin ((long *) &rechnr, SQLLONG, 0);
	dbClass.sqlin ((char *) blgtyp, SQLCHAR, 4);
	dbClass.sqlin ((long *) &mdn, SQLLONG, 0);

// 150605 : lsnr dazu gebaut


    dbClass.sqlout ((double *) &reporech.a,SQLDOUBLE,0);
    dbClass.sqlout ((char *) reporech.a_bz1,SQLCHAR,25);
    dbClass.sqlout ((char *) reporech.a_bz2,SQLCHAR,25);
    dbClass.sqlout ((long *) &reporech.lpos_txt,SQLLONG,0);
    dbClass.sqlout ((long *) &reporech.kopf_txt,SQLLONG,0);
    dbClass.sqlout ((long *) &reporech.fuss_txt,SQLLONG,0);
    dbClass.sqlout ((char *) reporech.a_kun,SQLCHAR,14);
    dbClass.sqlout ((char *) reporech.zu_stoff,SQLCHAR,3);
    dbClass.sqlout ((double *) &reporech.gn_pkt_gbr,SQLDOUBLE,0);
    dbClass.sqlout ((long *) &reporech.adr2,SQLLONG,0);
    dbClass.sqlout ((long *) &reporech.adr3,SQLLONG,0);
    dbClass.sqlout ((double *) &reporech.snetto1,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &reporech.snetto2,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &reporech.snetto3,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &reporech.smwst1,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &reporech.smwst2,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &reporech.smwst3,SQLDOUBLE,0);
    dbClass.sqlout ((char *) reporech.smwst1p,SQLCHAR,7);
	dbClass.sqlout ((char *) reporech.smwst2p,SQLCHAR,7);
	dbClass.sqlout ((char *) reporech.smwst3p,SQLCHAR,7);
	dbClass.sqlout ((short *) &reporech.smwst1s,SQLSHORT,0);
	dbClass.sqlout ((short *) &reporech.smwst2s,SQLSHORT,0);
	dbClass.sqlout ((short *) &reporech.smwst3s,SQLSHORT,0);
    dbClass.sqlout ((double *) &reporech.rech_summ,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &reporech.end_rab,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &reporech.zahl_betr,SQLDOUBLE,0);
    dbClass.sqlout ((long *) &reporech.fil,SQLLONG,0);
	dbClass.sqlout ((char *) reporech.belk_txt,SQLCHAR,81);	// 61->81 am 300106
	dbClass.sqlout ((char *) reporech.belf_txt,SQLCHAR,81);	// 61->81 am 300106
	dbClass.sqlout ((char *) reporech.zako_txt,SQLCHAR,81);	// 61->81 am 300106
	dbClass.sqlout ((char *) reporech.pmwsts,SQLCHAR,3);
    dbClass.sqlout ((long *) &reporech.kun,SQLLONG,0);
    dbClass.sqlout ((double *) &reporech.ges_rabp,SQLDOUBLE,0);
    dbClass.sqlout ((long *) &reporech.mdn,SQLLONG,0);
	dbClass.sqlout ((char *) reporech.ust_id,SQLCHAR,17);
	dbClass.sqlout ((char *) reporech.nr_bei_rech,SQLCHAR,17);
    dbClass.sqlout ((double *) &reporech.alt_pr,SQLDOUBLE,0);
	dbClass.sqlout ((char *) reporech.lsret,SQLCHAR,2);
	dbClass.sqlout ((char *) reporech.auf_ext,SQLCHAR,31);	// 270212 17->31
	dbClass.sqlout ((short *) &reporech.a_typ,SQLSHORT,0);
	dbClass.sqlout ((char *) reporech.kun_nam,SQLCHAR,46);
	dbClass.sqlout ((char *) reporech.bbn,SQLCHAR,17);
    dbClass.sqlout ((long *) &reporech.mdnadr,SQLLONG,0);
	dbClass.sqlout ((char *) reporech.rech_dat,SQLCHAR,12);
    dbClass.sqlout ((short *) &reporech.order3,SQLSHORT,0);
    dbClass.sqlout ((long *) &reporech.rech_nr,SQLLONG,0);
	dbClass.sqlout ((char *) reporech.erf_kz,SQLCHAR,2);
    dbClass.sqlout ((double *) &reporech.lief_me,SQLDOUBLE,0);
	dbClass.sqlout ((char *) reporech.lief_me_bz,SQLCHAR,7);
    dbClass.sqlout ((double *) &reporech.vk_pr,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &reporech.auf_me,SQLDOUBLE,0);
	dbClass.sqlout ((char *) reporech.sa_kz,SQLCHAR,2);
	dbClass.sqlout ((short *) &reporech.order2,SQLSHORT,0);
    dbClass.sqlout ((double *) &reporech.einz_rabp,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &reporech.zeil_sum,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &reporech.prab_wert,SQLDOUBLE,0);
    dbClass.sqlout ((long *) &reporech.rpos_txt,SQLLONG,0);
    dbClass.sqlout ((long *) &reporech.ktx_jebel,SQLLONG,0);
    dbClass.sqlout ((long *) &reporech.ftx_jebel,SQLLONG,0);
	dbClass.sqlout ((short *) &reporech.lief_art,SQLSHORT,0);
	dbClass.sqlout ((char *)  reporech.tel,SQLCHAR,21);
	dbClass.sqlout ((char *)  reporech.fax,SQLCHAR,21);
	dbClass.sqlout ((char *)  reporech.auf_me_bz,SQLCHAR,7);
    dbClass.sqlout ((double *) &reporech.lief_me1,SQLDOUBLE,0);
	dbClass.sqlout ((char *)  reporech.blg_typ,SQLCHAR,2);
	dbClass.sqlout ((char *)  reporech.frm_name,SQLCHAR,6);
	dbClass.sqlout ((char *)  reporech.akv,SQLCHAR,11);
	dbClass.sqlout ((char *)  reporech.ls_charge,SQLCHAR,31);
	dbClass.sqlout ((long *)  &reporech.lsnr,SQLLONG,0 );
	dbClass.sqlout ((long *)  &reporech.txt_rech,SQLLONG,0 );	// 101014


            readcursor = dbClass.sqlcursor (_T("select ")


		
	_T(" a, a_bz1, a_bz2, lpos_txt, kopf_txt, fuss_txt, a_kun, zu_stoff, gn_pkt_gbr, ")
	_T(" adr2, adr3, snetto1, snetto2, snetto3, smwst1, smwst2, smwst3, ")

    _T(" reporech.smwst1p, reporech.smwst2p, reporech.smwst3p, reporech.smwst1s, ")
	_T(" reporech.smwst2s, reporech.smwst3s, reporech.rech_summ, reporech.end_rab, ")
	_T(" reporech.zahl_betr, reporech.fil, reporech.belk_txt, reporech.belf_txt, ")
	_T(" reporech.zako_txt, reporech.pmwsts, reporech.kun, reporech.ges_rabp, ")
	_T(" reporech.mdn, reporech.ust_id, reporech.nr_bei_rech, reporech.alt_pr, ")
    
	_T(" lsret, auf_ext, a_typ, kun_nam, bbn, mdnadr, rech_dat, order3, ")

	_T(" reporech.rech_nr, reporech.erf_kz, reporech.lief_me, reporech.lief_me_bz, ")
    _T(" reporech.vk_pr, reporech.auf_me, reporech.sa_kz, reporech.order2, ")
	_T(" reporech.einz_rabp, reporech.zeil_sum, reporech.prab_wert, reporech.rpos_txt, ")
	_T(" reporech.ktx_jebel, reporech.ftx_jebel, reporech.lief_art, reporech.tel, ")
	_T(" reporech.fax, reporech.auf_me_bz, reporech.lief_me1, reporech.blg_typ, ")
	_T(" reporech.frm_name, reporech.akv, reporech.ls_charge, reporech.lsnr, reporech.txt_rech ")
	_T(" from reporech where ")
	_T(" reporech.rech_nr = ? and reporech.blg_typ = ? and reporech.mdn = ? ")
	_T(" order by order3 ") ) ;


}

void REPORZU_CLASS::prepare (void)
{


	dbClass.sqlin ((long *) &rechnr, SQLLONG, 0);
	dbClass.sqlin ((char *) blgtyp, SQLCHAR, 4);
	dbClass.sqlin ((long *) &mdn, SQLLONG, 0);


	dbClass.sqlout ((short *) &reporzu.order2,SQLSHORT,0);
    dbClass.sqlout ((char *)   reporzu.typ,SQLCHAR, 3 );
    dbClass.sqlout ((char *)   reporzu.gruppe,SQLCHAR, 7 );
    dbClass.sqlout ((char *)   reporzu.wertart,SQLCHAR, 2 );

    dbClass.sqlout ((double *) &reporzu.me,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &reporzu.faktor,SQLDOUBLE,0);
	dbClass.sqlout ((char *)   reporzu.rab_bz,SQLCHAR,37);
    dbClass.sqlout ((double *) &reporzu.geswert,SQLDOUBLE,0);

	readcursor = dbClass.sqlcursor (_T("select ")
		
	_T(" order2, typ, gruppe, wertart ")
	_T(" , me, faktor, rab_bz, geswert ")
	_T(" from reporzu where ")
	_T(" reporzu.rech_nr = ? and reporzu.blg_typ = ? and reporzu.mdn = ? ")
	_T(" order by order2 ") ) ;

}
