/**** C/C++ run-time linkage constants and function definitions for LL11.DLL ****/
/****  (c) 1991,..,1999,2000,01,02,03,04... combit GmbH, Konstanz, Germany  ****/
/****  [build of 2005-07-08 11:07:11] ****/

#ifndef _LL11_H
#define _LL11_H
#define _LL11_HX

#if !defined(WIN32)
  #pragma message("you should define WIN32 to ensure a correct compilation");
  #pragma message("(I assume WIN32, cross your fingers to proceed)");
  #define WIN32 1
#endif

#ifndef _LL11_MUST_NOT_INCLUDE_OLE2_H
  #if defined(WIN32)
    #include <ole2.h>
  #endif
#endif

#ifndef EXPLICIT_TYPES
  #define EXPLICIT_TYPES
  #ifndef INT
    typedef int INT; /* you should comment this out if you have any problems with INT */
  #endif
  #ifndef CHAR
    typedef char CHAR; /* you should comment this out if you have any problems with CHAR */
  #endif
  typedef unsigned char UINT8;
  typedef unsigned short UINT16;
  typedef signed char INT8;
  typedef signed short INT16;
  #ifndef _BASETSD_H_ /* MSVC6 defines these itself in basetsd.h */
    typedef unsigned long UINT32;
    typedef signed long INT32;
  #endif
#endif

#ifndef DLLPROC
  #define DLLPROC WINAPI
#endif
#ifndef DLLCPROC
  #ifdef WIN32
    #define DLLCPROC WINAPI
   #else
    #define DLLCPROC _far _cdecl
  #endif
#endif


#ifndef CMBTLANG_DEFAULT
 #define CMBTLANG_DEFAULT    -1
 #define CMBTLANG_GERMAN      0
 #define CMBTLANG_ENGLISH     1
 #define CMBTLANG_ARABIC      2
 #define CMBTLANG_AFRIKAANS   3
 #define CMBTLANG_ALBANIAN    4
 #define CMBTLANG_BASQUE      5
 #define CMBTLANG_BULGARIAN   6
 #define CMBTLANG_BYELORUSSIAN 7
 #define CMBTLANG_CATALAN     8
 #define CMBTLANG_CHINESE     9
 #define CMBTLANG_CROATIAN    10
 #define CMBTLANG_CZECH       11
 #define CMBTLANG_DANISH      12
 #define CMBTLANG_DUTCH       13
 #define CMBTLANG_ESTONIAN    14
 #define CMBTLANG_FAEROESE    15
 #define CMBTLANG_FARSI       16
 #define CMBTLANG_FINNISH     17
 #define CMBTLANG_FRENCH      18
 #define CMBTLANG_GREEK       19
 #define CMBTLANG_HEBREW      20
 #define CMBTLANG_HUNGARIAN   21
 #define CMBTLANG_ICELANDIC   22
 #define CMBTLANG_INDONESIAN  23
 #define CMBTLANG_ITALIAN     24
 #define CMBTLANG_JAPANESE    25
 #define CMBTLANG_KOREAN      26
 #define CMBTLANG_LATVIAN     27
 #define CMBTLANG_LITHUANIAN  28
 #define CMBTLANG_NORWEGIAN   29
 #define CMBTLANG_POLISH      30
 #define CMBTLANG_PORTUGUESE  31
 #define CMBTLANG_ROMANIAN    32
 #define CMBTLANG_RUSSIAN     33
 #define CMBTLANG_SLOVAK      34
 #define CMBTLANG_SLOVENIAN   35
 #define CMBTLANG_SERBIAN     36
 #define CMBTLANG_SPANISH     37
 #define CMBTLANG_SWEDISH     38
 #define CMBTLANG_THAI        39
 #define CMBTLANG_TURKISH     40
 #define CMBTLANG_UKRAINIAN   41
 #define CMBTLANG_CHINESE_TRADITIONAL   48
#endif


/*--- type declarations ---*/

#ifndef HJOB
  #define HJOB                           INT
#endif
#ifndef HLLJOB
  #define HLLJOB                         INT
#endif
#ifndef HLLEXPR
  #define HLLEXPR                        LPVOID
#endif
#ifndef HLLINTERF
  #define HLLINTERF                      LPVOID
#endif
#ifndef HSTG
  #define HSTG                           UINT32
#endif
#ifndef HLLSTG
  #define HLLSTG                         UINT32
#endif
#ifndef HLLRTFOBJ
  #define HLLRTFOBJ                      UINT32
#endif
#ifndef _PRECT
  #define _PRECT                         RECT FAR *
#endif
#ifndef _PCRECT
  #define _PCRECT                        const RECT FAR *
#endif
#ifndef HLISTPOS
  #define HLISTPOS                       UINT32
#endif
#ifndef _LPHANDLE
  #define _LPHANDLE                      HANDLE FAR *
#endif
#ifndef _LPINT32
  #define _LPINT32                       INT32 FAR *
#endif
#ifndef _LPUINT32
  #define _LPUINT32                      UINT32 FAR *
#endif
#ifndef _LCID
  #define _LCID                          UINT32
#endif
#ifndef PHGLOBAL
  #define PHGLOBAL                       HGLOBAL FAR *
#endif
#ifndef LLPUINT
  #define LLPUINT                        UINT FAR *
#endif
#ifndef _PDEVMODE
  #define _PDEVMODE                      DEVMODE FAR *
#endif
#ifndef _PDEVMODEA
  #define _PDEVMODEA                     DEVMODEA FAR *
#endif
#ifndef _PDEVMODEW
  #define _PDEVMODEW                     DEVMODEW FAR *
#endif
#ifndef _PCDEVMODE
  #define _PCDEVMODE                     const DEVMODE FAR *
#endif
#ifndef _PCDEVMODEA
  #define _PCDEVMODEA                    const DEVMODEA FAR *
#endif
#ifndef _PCDEVMODEW
  #define _PCDEVMODEW                    const DEVMODEW FAR *
#endif
#ifndef PSCLLCOLUMN
  #define PSCLLCOLUMN                    scLlColumn FAR *
#endif
#ifndef PVARIANT
  #define PVARIANT                       VARIANT FAR *
#endif
#ifndef CTL_GUID
  #define CTL_GUID                       const GUID
#endif
#ifndef CTL_PUNK
  #define CTL_PUNK                       IUnknown FAR *
#endif
#ifndef CTL_PPUNK
  #define CTL_PPUNK                      IUnknown FAR * FAR *
#endif
#ifndef _PSYSTEMTIME
  #define _PSYSTEMTIME                   SYSTEMTIME FAR *
#endif
#ifndef _PCSYSTEMTIME
  #define _PCSYSTEMTIME                  const SYSTEMTIME FAR *
#endif
#ifndef PSCLLPROJECTUSERDATA
  #define PSCLLPROJECTUSERDATA           scLlProjectUserData FAR *
#endif

/*--- constant declarations ---*/

#define LL_JOBOPENFLAG_NOLLXPRELOAD    (0x00001000)        
#define LL_JOBOPENFLAG_ONLYEXACTLANGUAGE (0x00002000)         /* do not look for '@@' LNG file */
#define LL_DEBUG_CMBTLL                (0x0001)             /* debug CMBTLLnn.DLL */
#define LL_DEBUG_CMBTDWG               (0x0002)             /* debug CMBTDWnn.DLL */
#define LL_DEBUG_CMBTLS                (0x0080)             /* debug CMBTLSnn.DLL */
#define LL_DEBUG_CMBTLL_NOCALLBACKS    (0x0004)            
#define LL_DEBUG_CMBTLL_NOSTORAGE      (0x0008)            
#define LL_DEBUG_CMBTLL_NOWAITDLG      (0x0010)            
#define LL_DEBUG_CMBTLL_NOSYSINFO      (0x0020)            
#define LL_DEBUG_CMBTLL_LOGTOFILE      (0x0040)            
#define LL_VERSION_MAJOR               (1)                  /* direct return of major version (f.ex. 1) */
#define LL_VERSION_MINOR               (2)                  /* direct return of minor version (f.ex. 13) */
#define LL_VERSION_SERNO_LO            (3)                  /* LOWORD(serial number) */
#define LL_VERSION_SERNO_HI            (4)                  /* HIWORD(serial number) */
#define LL_VERSION_OEMNO               (5)                  /* OEM number */
#define LL_VERSION_RESMAJOR            (11)                 /* internal, for LlRCGetVersion: resource version */
#define LL_VERSION_RESMINOR            (12)                 /* internal, for LlRCGetVersion: resource version */
#define LL_VERSION_RESLANGUAGE         (14)                 /* internal, for LlRCGetVersion: resource language */
#define LL_CMND_DRAW_USEROBJ           (0)                  /* callback for LL_DRAWING_USEROBJ */
#define LL_CMND_EDIT_USEROBJ           (1)                  /* callback for LL_DRAWING_USEROBJ_DLG */
#define LL_CMND_TABLELINE              (10)                 /* callback for LL_CB_TABLELINE */
#define LL_TABLE_LINE_HEADER           (0)                 
#define LL_TABLE_LINE_BODY             (1)                 
#define LL_TABLE_LINE_FOOTER           (2)                 
#define LL_TABLE_LINE_FILL             (3)                 
#define LL_TABLE_LINE_GROUP            (4)                 
#define LL_TABLE_LINE_GROUPFOOTER      (5)                 
#define LL_CMND_TABLEFIELD             (11)                 /* callback for LL_CB_TABLEFIELD */
#define LL_TABLE_FIELD_HEADER          (0)                 
#define LL_TABLE_FIELD_BODY            (1)                 
#define LL_TABLE_FIELD_FOOTER          (2)                 
#define LL_TABLE_FIELD_FILL            (3)                 
#define LL_TABLE_FIELD_GROUP           (4)                 
#define LL_TABLE_FIELD_GROUPFOOTER     (5)                 
#define LL_CMND_EVALUATE               (12)                 /* callback for "External$" function */
#define LL_CMND_OBJECT                 (20)                 /* callback of LL_CB_OBJECT */
#define LL_CMND_PAGE                   (21)                 /* callback of LL_CB_PAGE */
#define LL_CMND_PROJECT                (22)                 /* callback of LL_CB_PROJECT */
#define LL_CMND_DRAW_GROUP_BEGIN       (23)                 /* callback for LlPrintBeginGroup */
#define LL_CMND_DRAW_GROUP_END         (24)                 /* callback for LlPrintEndGroup */
#define LL_CMND_DRAW_GROUPLINE         (25)                 /* callback for LlPrintGroupLine */
#define LL_RSP_GROUP_IMT               (0)                 
#define LL_RSP_GROUP_NEXTPAGE          (1)                 
#define LL_RSP_GROUP_OK                (2)                 
#define LL_RSP_GROUP_DRAWFOOTER        (3)                 
#define LL_CMND_HELP                   (30)                 /* lParam: HIWORD=HELP_xxx, LOWORD=Context # */
#define LL_CMND_ENABLEMENU             (31)                 /* undoc: lParam/LOWORD(lParam) = HMENU */
#define LL_CMND_MODIFYMENU             (32)                 /* undoc: lParam/LOWORD(lParam) = HMENU */
#define LL_CMND_SELECTMENU             (33)                 /* undoc: lParam=ID (return TRUE if processed) */
#define LL_CMND_GETVIEWERBUTTONSTATE   (34)                 /* HIWORD(lParam)=ID, LOWORD(lParam)=state */
#define LL_CMND_VARHELPTEXT            (35)                 /* lParam=LPSTR(Name), returns LPSTR(Helptext) */
#define LL_INFO_METER                  (37)                 /* lParam = addr(scLlMeterInfo) */
#define LL_METERJOB_LOAD               (1)                 
#define LL_METERJOB_SAVE               (2)                 
#define LL_METERJOB_CONSISTENCYCHECK   (3)                 
#define LL_METERJOB_PASS2              (4)                 
#define LL_NTFY_FAILSFILTER            (1000)               /* data set fails filter expression */
#define LL_NTFY_VIEWERBTNCLICKED       (38)                 /* user presses a preview button (action will be done). lParam=ID. result: 0=allowed, 1=not allowed */
#define LL_CMND_DLGEXPR_VARBTN         (39)                 /* lParam: @scLlDlgExprVarExt, return: IDOK for ok */
#define LL_CMND_HOSTPRINTER            (40)                 /* lParam: scLlPrinter */
#define LL_PRN_CREATE_DC               (1)                  /* scLlPrinter._nCmd values */
#define LL_PRN_DELETE_DC               (2)                 
#define LL_PRN_SET_ORIENTATION         (3)                 
#define LL_PRN_GET_ORIENTATION         (4)                 
#define LL_PRN_EDIT                    (5)                  /* unused */
#define LL_PRN_GET_DEVICENAME          (6)                 
#define LL_PRN_GET_DRIVERNAME          (7)                 
#define LL_PRN_GET_PORTNAME            (8)                 
#define LL_PRN_RESET_DC                (9)                 
#define LL_PRN_COMPARE_PRINTER         (10)                
#define LL_PRN_GET_PHYSPAGE            (11)                
#define LL_PRN_SET_PHYSPAGE            (12)                
#define LL_PRN_GET_PAPERFORMAT         (13)                 /* fill _nPaperFormat */
#define LL_PRN_SET_PAPERFORMAT         (14)                 /* _nPaperFormat, _xPaperSize, _yPaperSize */
#define LL_OEM_TOOLBAR_START           (41)                
#define LL_OEM_TOOLBAR_END             (50)                
#define LL_NTFY_EXPRERROR              (51)                 /* lParam = LPCSTR(error text) */
#define LL_CMND_CHANGE_DCPROPERTIES_CREATE (52)                 /* lParam = addr(scLlPrinter), _hDC is valid */
#define LL_CMND_CHANGE_DCPROPERTIES_DOC (53)                 /* lParam = addr(scLlPrinter), _hDC is valid */
#define LL_CMND_CHANGE_DCPROPERTIES_PAGE (54)                 /* lParam = addr(scLlPrinter), _hDC is valid */
#define LL_CMND_CHANGE_DCPROPERTIES_PREPAGE (56)                 /* lParam = addr(scLlPrinter), _hDC and _pszBuffer( DEVMODE* ) are valid */
#define LL_CMND_MODIFY_METAFILE        (57)                 /* lParam = handle of metafile (32 bit: enh. metafile) */
#define LL_INFO_PRINTJOBSUPERVISION    (58)                 /* lParam = addr(scLlPrintJobInfo) */
#define LL_CMND_DELAYEDVALUE           (59)                 /* lParam = addr(scLlDelayedValue) */
#define LL_CMND_SUPPLY_USERDATA        (60)                 /* lParam = addr(scLlProjectUserData) */
#define LL_CMND_SAVEFILENAME           (61)                 /* lParam = LPCTSTR(Filename) */
#define LL_QUERY_IS_VARIABLE_OR_FIELD  (62)                 /* lParam = addr(scLlDelayDefineFieldOrVariable), must be enabled by CB mask. If returns TRUE, the var must be defined in the callback... */
#define LL_INTERNAL_MAXEVENTNUMBER     (62)                 /* internal: max. event number */
#define OBJECT_LABEL                   (1)                  /* old - please do not use any more */
#define OBJECT_LIST                    (2)                 
#define OBJECT_CARD                    (3)                 
#define LL_PROJECT_LABEL               (1)                  /* new names... */
#define LL_PROJECT_LIST                (2)                 
#define LL_PROJECT_CARD                (3)                 
#define LL_OBJ_MARKER                  (0)                  /* internal use only */
#define LL_OBJ_TEXT                    (1)                  /* the following are used in the object callback */
#define LL_OBJ_RECT                    (2)                 
#define LL_OBJ_LINE                    (3)                 
#define LL_OBJ_BARCODE                 (4)                 
#define LL_OBJ_DRAWING                 (5)                 
#define LL_OBJ_TABLE                   (6)                 
#define LL_OBJ_TEMPLATE                (7)                 
#define LL_OBJ_ELLIPSE                 (8)                 
#define LL_OBJ_GROUP                   (9)                  /* internal use only */
#define LL_OBJ_RTF                     (10)                
#define LL_OBJ_LLX                     (11)                
#define LL_OBJ_INPUT                   (12)                
#define LL_OBJ_LAST                    (12)                 /* last object type (for loops as upper bound) */
#define LL_OBJ_PAGE                    (255)                /* for exporter */
#define LL_DELAYEDVALUE                (0x80000000)        
#define LL_TYPEMASK                    (0x7ff00000)        
#define LL_TABLE_FOOTERFIELD           (0x00008000)         /* 'or'ed for footline-only fields // reserved also for Variables (see "$$xx$$")!!!! */
#define LL_TABLE_GROUPFIELD            (0x00004000)         /* 'or'ed for groupline-only fields */
#define LL_TABLE_HEADERFIELD           (0x00002000)         /* 'or'ed for headline-only fields */
#define LL_TABLE_BODYFIELD             (0x00001000)         /* 'or'ed for headline-only fields */
#define LL_TABLE_GROUPFOOTERFIELD      (0x00000800)         /* 'or'ed for group-footer-line-only fields */
#define LL_TABLE_FIELDTYPEMASK         (0x0000f800)         /* internal use */
#define LL_BARCODE                     (0x40000000)        
#define LL_BARCODE_EAN13               (0x40000000)        
#define LL_BARCODE_EAN8                (0x40000001)        
#define LL_BARCODE_UPCA                (0x40000002)        
#define LL_BARCODE_UPCE                (0x40000003)        
#define LL_BARCODE_3OF9                (0x40000004)        
#define LL_BARCODE_25INDUSTRIAL        (0x40000005)        
#define LL_BARCODE_25INTERLEAVED       (0x40000006)        
#define LL_BARCODE_25DATALOGIC         (0x40000007)        
#define LL_BARCODE_25MATRIX            (0x40000008)        
#define LL_BARCODE_POSTNET             (0x40000009)        
#define LL_BARCODE_FIM                 (0x4000000A)        
#define LL_BARCODE_CODABAR             (0x4000000B)        
#define LL_BARCODE_EAN128              (0x4000000C)        
#define LL_BARCODE_CODE128             (0x4000000D)        
#define LL_BARCODE_DP_LEITCODE         (0x4000000E)        
#define LL_BARCODE_DP_IDENTCODE        (0x4000000F)        
#define LL_BARCODE_GERMAN_PARCEL       (0x40000010)        
#define LL_BARCODE_CODE93              (0x40000011)        
#define LL_BARCODE_MSI                 (0x40000012)        
#define LL_BARCODE_CODE11              (0x40000013)        
#define LL_BARCODE_MSI_10_CD           (0x40000014)        
#define LL_BARCODE_MSI_10_10           (0x40000015)        
#define LL_BARCODE_MSI_11_10           (0x40000016)        
#define LL_BARCODE_MSI_PLAIN           (0x40000017)        
#define LL_BARCODE_EAN14               (0x40000018)        
#define LL_BARCODE_UCC14               (0x40000019)        
#define LL_BARCODE_CODE39              (0x4000001A)        
#define LL_BARCODE_CODE39_CRC43        (0x4000001B)        
#define LL_BARCODE_PZN                 (0x4000001C)        
#define LL_BARCODE_CODE39_EXT          (0x4000001D)        
#define LL_BARCODE_JAPANESE_POSTAL     (0x4000001E)        
#define LL_BARCODE_RM4SCC              (0x4000001F)        
#define LL_BARCODE_RM4SCC_CRC          (0x40000020)        
#define LL_BARCODE_LLXSTART            (0x40000040)        
#define LL_BARCODE_PDF417              (0x40000040)        
#define LL_BARCODE_MAXICODE            (0x40000041)        
#define LL_BARCODE_MAXICODE_UPS        (0x40000042)        
#define LL_BARCODE_DATAMATRIX          (0x40000044)        
#define LL_BARCODE_AZTEC               (0x40000045)        
#define LL_BARCODE_METHODMASK          (0x000000ff)        
#define LL_BARCODE_WITHTEXT            (0x00000100)        
#define LL_BARCODE_WITHOUTTEXT         (0x00000200)        
#define LL_BARCODE_TEXTDONTCARE        (0x00000000)        
#define LL_DRAWING                     (0x20000000)        
#define LL_DRAWING_HMETA               (0x20000001)        
#define LL_DRAWING_USEROBJ             (0x20000002)        
#define LL_DRAWING_USEROBJ_DLG         (0x20000003)        
#define LL_DRAWING_HBITMAP             (0x20000004)        
#define LL_DRAWING_HICON               (0x20000005)        
#define LL_DRAWING_HEMETA              (0x20000006)        
#define LL_DRAWING_HDIB                (0x20000007)         /* global handle to BITMAPINFO and bits */
#define LL_DRAWING_METHODMASK          (0x000000ff)        
#define LL_META_MAXX                   (10000)             
#define LL_META_MAXY                   (10000)             
#define LL_TEXT                        (0x10000000)        
#define LL_TEXT_ALLOW_WORDWRAP         (0x10000000)        
#define LL_TEXT_DENY_WORDWRAP          (0x10000001)        
#define LL_TEXT_FORCE_WORDWRAP         (0x10000002)        
#define LL_NUMERIC                     (0x08000000)        
#define LL_NUMERIC_LOCALIZED           (0x08000001)        
#define LL_DATE                        (0x04000000)         /* LL's own julian */
#define LL_DATE_DELPHI_1               (0x04000001)        
#define LL_DATE_DELPHI                 (0x04000002)         /* DELPHI 2, 3, 4: OLE DATE */
#define LL_DATE_MS                     (0x04000002)         /* MS C/Basic: OLE DATE */
#define LL_DATE_OLE                    (0x04000002)         /* generic: OLE DATE */
#define LL_DATE_VFOXPRO                (0x04000003)         /* nearly LL's own julian, has an offset of 1! */
#define LL_DATE_DMY                    (0x04000004)         /* <d><sep><m><sep><yyyy>. Year MUST be 4 digits! */
#define LL_DATE_MDY                    (0x04000005)         /* <m><sep><d><sep><yyyy>. Year MUST be 4 digits! */
#define LL_DATE_YMD                    (0x04000006)         /* <yyyy><sep><m><sep><d>. Year MUST be 4 digits! */
#define LL_DATE_YYYYMMDD               (0x04000007)         /* <yyyymmdd> */
#define LL_DATE_LOCALIZED              (0x04000008)         /* localized (automatic VariantConversion) */
#define LL_DATE_METHODMASK             (0x000000ff)        
#define LL_BOOLEAN                     (0x02000000)        
#define LL_RTF                         (0x01000000)        
#define LL_HTML                        (0x00800000)        
#define LL_LLXOBJECT                   (0x00100000)         /* internal use only */
#define LL_FIXEDNAME                   (0x8000)            
#define LL_NOSAVEAS                    (0x4000)            
#define LL_EXPRCONVERTQUIET            (0x1000)             /* convert to new expressions without warning box */
#define LL_NONAMEINTITLE               (0x0800)             /* no file name appended to title */
#define LL_PRVOPT_PRN_USEDEFAULT       (0x00000000)        
#define LL_PRVOPT_PRN_ASKPRINTERIFNEEDED (0x00000001)        
#define LL_PRVOPT_PRN_ASKPRINTERALWAYS (0x00000002)        
#define LL_PRVOPT_PRN_ALWAYSUSEDEFAULT (0x00000003)        
#define LL_PRVOPT_PRN_ASSIGNMASK       (0x00000003)         /* used by L&L */
#define LL_OPTION_COPIES               (0)                  /* compatibility only, please use LL_PRNOPT_... */
#define LL_OPTION_STARTPAGE            (1)                  /* compatibility only, please use LL_PRNOPT_PAGE */
#define LL_OPTION_PAGE                 (1)                  /* compatibility only, please use LL_PRNOPT_... */
#define LL_OPTION_OFFSET               (2)                  /* compatibility only, please use LL_PRNOPT_... */
#define LL_OPTION_COPIES_SUPPORTED     (3)                  /* compatibility only, please use LL_PRNOPT_... */
#define LL_OPTION_FIRSTPAGE            (5)                  /* compatibility only, please use LL_PRNOPT_... */
#define LL_OPTION_LASTPAGE             (6)                  /* compatibility only, please use LL_PRNOPT_... */
#define LL_OPTION_JOBPAGES             (7)                  /* compatibility only, please use LL_PRNOPT_... */
#define LL_OPTION_PRINTORDER           (8)                  /* compatibility only, please use LL_PRNOPT_... */
#define LL_PRNOPT_COPIES               (0)                 
#define LL_COPIES_HIDE                 (-32768)             /* anything negative... */
#define LL_PRNOPT_STARTPAGE            (1)                 
#define LL_PRNOPT_PAGE                 (1)                  /* alias; please do not use STARTPAGE any more... */
#define LL_PAGE_HIDE                   (-32768)             /* must be exactly this value! */
#define LL_PRNOPT_OFFSET               (2)                 
#define LL_PRNOPT_COPIES_SUPPORTED     (3)                 
#define LL_PRNOPT_UNITS                (4)                  /* only GetOption() */
#define LL_UNITS_MM_DIV_10             (0)                  /* for LL_OPTION_UNITS and LL_OPTION_UNITS_DEFAULT */
#define LL_UNITS_INCH_DIV_100          (1)                 
#define LL_UNITS_INCH_DIV_1000         (2)                 
#define LL_UNITS_SYSDEFAULT_LORES      (3)                  /* for LL_OPTION_UNITS_DEFAULT only */
#define LL_UNITS_SYSDEFAULT            (4)                  /* for LL_OPTION_UNITS_DEFAULT only */
#define LL_UNITS_MM_DIV_100            (5)                 
#define LL_UNITS_MM_DIV_1000           (6)                 
#define LL_PRNOPT_FIRSTPAGE            (5)                 
#define LL_PRNOPT_LASTPAGE             (6)                 
#define LL_PRNOPT_JOBPAGES             (7)                 
#define LL_PRNOPT_PRINTORDER           (8)                 
#define LL_PRINTORDER_HORZ_LTRB        (0)                 
#define LL_PRINTORDER_VERT_LTRB        (1)                 
#define LL_PRINTORDER_HORZ_RBLT        (2)                 
#define LL_PRINTORDER_VERT_RBLT        (3)                 
#define LL_PRNOPT_PRINTORDER_P1        (9)                  /* for future support */
#define LL_PRNOPT_PRINTORDER_P2        (10)                 /* for future support */
#define LL_PRNOPT_DEFPRINTERINSTALLED  (11)                 /* returns 0 for no default printer, 1 for default printer present */
#define LL_PRNOPT_PRINTDLG_DESTMASK    (12)                 /* any combination of the ones below... Default: all. Outdated, please use LL_OPTIONSTR_EXPORTS_ALLOWED */
#define LL_DESTINATION_PRN             (1)                 
#define LL_DESTINATION_PRV             (2)                 
#define LL_DESTINATION_FILE            (4)                 
#define LL_DESTINATION_EXTERN          (8)                 
#define LL_DESTINATION_MSFAX           (16)                 /* reserved */
#define LL_PRNOPT_PRINTDLG_DEST        (13)                 /* default destination; outdated, please use LL_PRNOPTSTR_EXPORT */
#define LL_PRNOPT_PRINTDLG_ONLYPRINTERCOPIES (14)                 /* show copies option in dialog only if they are supported by the printer. default: FALSE */
#define LL_PRNOPT_JOBID                (17)                
#define LL_PRNOPT_PAGEINDEX            (18)                
#define LL_PRNOPT_USES2PASS            (19)                 /* r/o */
#define LL_PRNOPTSTR_PRINTDST_FILENAME (0)                  /* print to file: default filename (LlGet/SetPrintOptionString) */
#define LL_PRNOPTSTR_EXPORTDESCR       (1)                  /* r/o, returns the description of the export chosen */
#define LL_PRNOPTSTR_EXPORT            (2)                  /* sets default exporter to use / returns the name of the export chosen */
#define LL_PRNOPTSTR_PRINTJOBNAME      (3)                  /* set name to be given to StartDoc() (lpszMessage of LlPrintWithBoxStart() */
#define LL_PRNOPTSTR_PRESTARTDOCESCSTRING (4)                  /* sent before StartDoc() */
#define LL_PRNOPTSTR_POSTENDDOCESCSTRING (5)                  /* sent after EndDoc() */
#define LL_PRNOPTSTR_PRESTARTPAGEESCSTRING (6)                  /* sent before StartPage() */
#define LL_PRNOPTSTR_POSTENDPAGEESCSTRING (7)                  /* sent after EndPage() */
#define LL_PRNOPTSTR_PRESTARTPROJECTESCSTRING (8)                  /* sent before first StartPage() of project */
#define LL_PRNOPTSTR_POSTENDPROJECTESCSTRING (9)                  /* sent after last EndPage() of project */
#define LL_PRINT_V1POINTX              (0x0000)            
#define LL_PRINT_NORMAL                (0x0100)            
#define LL_PRINT_PREVIEW               (0x0200)            
#define LL_PRINT_STORAGE               (0x0200)             /* same as LL_PRINT_PREVIEW */
#define LL_PRINT_FILE                  (0x0400)            
#define LL_PRINT_USERSELECT            (0x0800)            
#define LL_PRINT_EXPORT                (0x0800)             /* same as LL_PRINT_USERSELECT */
#define LL_PRINT_MODEMASK              (0x0f00)            
#define LL_PRINT_MULTIPLE_JOBS         (0x1000)            
#define LL_PRINT_KEEPJOB               (0x2000)            
#define LL_PRINT_FILENAME_IS_HGLOBAL   (0x4000)             /* reserved, not yet used */
#define LL_BOXTYPE_NONE                (-1)                
#define LL_BOXTYPE_NORMALMETER         (0)                 
#define LL_BOXTYPE_BRIDGEMETER         (1)                 
#define LL_BOXTYPE_NORMALWAIT          (2)                 
#define LL_BOXTYPE_BRIDGEWAIT          (3)                 
#define LL_BOXTYPE_EMPTYWAIT           (4)                 
#define LL_BOXTYPE_EMPTYABORT          (5)                 
#define LL_BOXTYPE_STDWAIT             (6)                 
#define LL_BOXTYPE_STDABORT            (7)                 
#define LL_BOXTYPE_MAX                 (7)                 
#define LL_FILE_ALSONEW                (0x8000)            
#define LL_FCTPARATYPE_DOUBLE          (0x01)              
#define LL_FCTPARATYPE_DATE            (0x02)              
#define LL_FCTPARATYPE_STRING          (0x04)              
#define LL_FCTPARATYPE_BOOL            (0x08)              
#define LL_FCTPARATYPE_DRAWING         (0x10)              
#define LL_FCTPARATYPE_BARCODE         (0x20)              
#define LL_FCTPARATYPE_ALL             (0x3f)              
#define LL_FCTPARATYPE_PARA1           (0x8001)            
#define LL_FCTPARATYPE_PARA2           (0x8002)            
#define LL_FCTPARATYPE_PARA3           (0x8003)            
#define LL_FCTPARATYPE_PARA4           (0x8004)            
#define LL_FCTPARATYPE_SAME            (0x803f)            
#define LL_EXPRTYPE_DOUBLE             (1)                 
#define LL_EXPRTYPE_DATE               (2)                 
#define LL_EXPRTYPE_STRING             (3)                 
#define LL_EXPRTYPE_BOOL               (4)                 
#define LL_EXPRTYPE_DRAWING            (5)                 
#define LL_EXPRTYPE_BARCODE            (6)                 
#define LL_OPTION_NEWEXPRESSIONS       (0)                  /* default: TRUE */
#define LL_OPTION_ONLYONETABLE         (1)                  /* default: FALSE */
#define LL_OPTION_TABLE_COLORING       (2)                  /* default: LL_COLORING_LL */
#define LL_COLORING_LL                 (0)                 
#define LL_COLORING_PROGRAM            (1)                 
#define LL_COLORING_DONTCARE           (2)                 
#define LL_OPTION_SUPERVISOR           (3)                  /* default: FALSE */
#define LL_OPTION_UNITS                (4)                  /* default: see LL_OPTION_METRIC */
#define LL_OPTION_TABSTOPS             (5)                  /* default: LL_TABS_DELETE */
#define LL_TABS_DELETE                 (0)                 
#define LL_TABS_EXPAND                 (1)                 
#define LL_OPTION_CALLBACKMASK         (6)                  /* default: 0x00000000 */
#define LL_CB_PAGE                     (0x40000000)         /* callback for each page */
#define LL_CB_PROJECT                  (0x20000000)         /* callback for each label */
#define LL_CB_OBJECT                   (0x10000000)         /* callback for each object */
#define LL_CB_HELP                     (0x08000000)         /* callback for HELP (F1/Button) */
#define LL_CB_TABLELINE                (0x04000000)         /* callback for table line */
#define LL_CB_TABLEFIELD               (0x02000000)         /* callback for table field */
#define LL_CB_QUERY_IS_VARIABLE_OR_FIELD (0x01000000)         /* callback for delayload (LL_QUERY_IS_VARIABLE_OR_FIELD) */
#define LL_OPTION_CALLBACKPARAMETER    (7)                  /* default: 0 */
#define LL_OPTION_HELPAVAILABLE        (8)                  /* default: TRUE */
#define LL_OPTION_SORTVARIABLES        (9)                  /* default: TRUE */
#define LL_OPTION_SUPPORTPAGEBREAK     (10)                 /* default: TRUE */
#define LL_OPTION_SHOWPREDEFVARS       (11)                 /* default: TRUE */
#define LL_OPTION_USEHOSTPRINTER       (13)                 /* default: FALSE // use host printer via callback */
#define LL_OPTION_EXTENDEDEVALUATION   (14)                 /* allows expressions in chevrons (amwin mode) */
#define LL_OPTION_TABREPRESENTATIONCODE (15)                 /* default: 247 (0xf7) */
#define LL_OPTION_METRIC               (18)                 /* default: depends on Windows defaults */
#define LL_OPTION_ADDVARSTOFIELDS      (19)                 /* default: FALSE */
#define LL_OPTION_MULTIPLETABLELINES   (20)                 /* default: TRUE */
#define LL_OPTION_CONVERTCRLF          (21)                 /* default: FALSE */
#define LL_OPTION_WIZ_FILENEW          (22)                 /* default: FALSE */
#define LL_OPTION_RETREPRESENTATIONCODE (23)                 /* default: LL_CHAR_NEWLINE (182) */
#define LL_OPTION_PRVZOOM_PERC         (25)                 /* initial preview zoom */
#define LL_OPTION_PRVRECT_LEFT         (26)                 /* initial preview position */
#define LL_OPTION_PRVRECT_TOP          (27)                
#define LL_OPTION_PRVRECT_WIDTH        (28)                
#define LL_OPTION_PRVRECT_HEIGHT       (29)                
#define LL_OPTION_STORAGESYSTEM        (30)                 /* 0=LX4-compatible, 1=STORAGE */
#define LL_STG_COMPAT4                 (0)                 
#define LL_STG_STORAGE                 (1)                 
#define LL_OPTION_COMPRESSSTORAGE      (31)                 /* 32 bit, STORAGE only [TRUE/FALSE] */
#define LL_OPTION_NOPARAMETERCHECK     (32)                 /* you need a bit more speed? */
#define LL_OPTION_NONOTABLECHECK       (33)                 /* don't check on "NO_TABLEOBJECT" error */
#define LL_OPTION_DRAWFOOTERLINEONPRINT (34)                 /* delay footerline printing to LlPrint(). Default FALSE */
#define LL_OPTION_PRVZOOM_LEFT         (35)                 /* initial preview position in percent of screen */
#define LL_OPTION_PRVZOOM_TOP          (36)                
#define LL_OPTION_PRVZOOM_WIDTH        (37)                
#define LL_OPTION_PRVZOOM_HEIGHT       (38)                
#define LL_OPTION_SPACEOPTIMIZATION    (40)                 /* default: TRUE */
#define LL_OPTION_REALTIME             (41)                 /* default: FALSE */
#define LL_OPTION_AUTOMULTIPAGE        (42)                 /* default: TRUE */
#define LL_OPTION_USEBARCODESIZES      (43)                 /* default: FALSE */
#define LL_OPTION_MAXRTFVERSION        (44)                 /* default: 0x100 (1.0) */
#define LL_OPTION_VARSCASESENSITIVE    (46)                 /* default: FALSE */
#define LL_OPTION_DELAYTABLEHEADER     (47)                 /* default: FALSE */
#define LL_OPTION_OFNDIALOG_EXPLORER   (48)                 /* default: Win16: FALSE, WIN32: NewShell present */
#define LL_OPTION_OFN_NOPLACESBAR      (0x40000000)        
#define LL_OPTION_EMFRESOLUTION        (49)                 /* default: 100 for 1/100 mm */
#define LL_OPTION_SETCREATIONINFO      (50)                 /* default: TRUE */
#define LL_OPTION_XLATVARNAMES         (51)                 /* default: TRUE */
#define LL_OPTION_LANGUAGE             (52)                 /* returns current language (r/o) */
#define LL_OPTION_PHANTOMSPACEREPRESENTATIONCODE (54)                 /* default: LL_CHAR_PHANTOMSPACE */
#define LL_OPTION_LOCKNEXTCHARREPRESENTATIONCODE (55)                 /* default: LL_CHAR_LOCK */
#define LL_OPTION_EXPRSEPREPRESENTATIONCODE (56)                 /* default: LL_CHAR_EXPRSEP */
#define LL_OPTION_DEFPRINTERINSTALLED  (57)                 /* r/o */
#define LL_OPTION_CALCSUMVARSONINVISIBLELINES (58)                 /* default: FALSE - only default value if no preferences in project */
#define LL_OPTION_NOFOOTERPAGEWRAP     (59)                 /* default: FALSE - only default value if no preferences in project */
#define LL_OPTION_IMMEDIATELASTPAGE    (64)                 /* default: FALSE */
#define LL_OPTION_LCID                 (65)                 /* default: LOCALE_USER_DEFAULT */
#define LL_OPTION_TEXTQUOTEREPRESENTATIONCODE (66)                 /* default: 1 */
#define LL_OPTION_SCALABLEFONTSONLY    (67)                 /* default: TRUE */
#define LL_OPTION_NOTIFICATIONMESSAGEHWND (68)                 /* default: NULL (parent window handle) */
#define LL_OPTION_DEFDEFFONT           (69)                 /* default: GetStockObject(ANSI_VAR_FONT) */
#define LL_OPTION_CODEPAGE             (70)                 /* default: CP_ACP; set codepage to use for conversions. */
#define LL_OPTION_FORCEFONTCHARSET     (71)                 /* default: FALSE; set font's charset to the codepage according to LL_OPTION_LCID. Default: FALSE */
#define LL_OPTION_COMPRESSRTF          (72)                 /* default: TRUE; compress RTF text > 1024 bytes in project file */
#define LL_OPTION_ALLOW_LLX_EXPORTERS  (74)                 /* default: TRUE; allow ILlXExport extensions */
#define LL_OPTION_SUPPORTS_PRNOPTSTR_EXPORT (75)                 /* default: FALSE: hides "set to default" button in "export option" tab in designer */
#define LL_OPTION_DEBUGFLAG            (76)                
#define LL_OPTION_SKIPRETURNATENDOFRTF (77)                 /* default: FALSE */
#define LL_OPTION_INTERCHARSPACING     (78)                 /* default: FALSE: allows character interspacing in case of block justify */
#define LL_OPTION_INCLUDEFONTDESCENT   (79)                 /* default: FALSE (compatibility) */
#define LL_OPTION_RESOLUTIONCOMPATIBLETO9X (80)                 /* default: FALSE (on NT/2K, else TRUE) */
#define LL_OPTION_USECHARTFIELDS       (81)                 /* default: FALSE */
#define LL_OPTION_OFNDIALOG_NOPLACESBAR (82)                 /* default: FALSE; do not use "Places" bar in NT2K? */
#define LL_OPTION_SKETCH_COLORDEPTH    (83)                 /* default: 1 */
#define LL_OPTION_FINAL_TRUE_ON_LASTPAGE (84)                 /* default: FALSE: internal use */
#define LL_OPTION_LLXAUTOSORTAXIS      (85)                 /* default: FALSE */
#define LL_OPTION_INTERCHARSPACING_FORCED (86)                 /* default: FALSE: forces character interspacing calculation in TEXT objects (possibly dangerous and slow) */
#define LL_OPTION_RTFAUTOINCREMENT     (87)                 /* default: FALSE, to increment RTF char pointer if nothing can be printed */
#define LL_OPTION_UNITS_DEFAULT        (88)                 /* default: LL_OPTION_UNITS_SYSDEFAULT. Use for contols that query the units, where we need to return "sysdefault" also */
#define LL_OPTION_NO_MAPI              (89)                 /* default: FALSE. Inhibit MAPI load for preview */
#define LL_OPTION_TOOLBARSTYLE         (90)                 /* default: LL_OPTION_TOOLBARSTYLE_STANDARD|LL_OPTION_TOOLBARSTYLEFLAG_DOCKABLE */
#define LL_OPTION_TOOLBARSTYLE_STANDARD (0)                  /* OFFICE97 alike style */
#define LL_OPTION_TOOLBARSTYLE_OFFICEXP (1)                  /* DOTNET/OFFICE_XP alike style */
#define LL_OPTION_TOOLBARSTYLE_OFFICE2003 (2)                 
#define LL_OPTION_TOOLBARSTYLEMASK     (0x0f)              
#define LL_OPTION_TOOLBARSTYLEFLAG_GRADIENT (0x80)               /* starting with XP, use gradient style */
#define LL_OPTION_TOOLBARSTYLEFLAG_DOCKABLE (0x40)               /* dockable toolbars? */
#define LL_OPTION_TOOLBARSTYLEFLAG_CANCLOSE (0x20)               /* internal use only */
#define LL_OPTION_MENUSTYLE            (91)                 /* default: LL_OPTION_MENUSTYLE_STANDARD */
#define LL_OPTION_MENUSTYLE_STANDARD_WITHOUT_BITMAPS (0)                  /* values: see CTL */
#define LL_OPTION_MENUSTYLE_STANDARD   (1)                 
#define LL_OPTION_MENUSTYLE_OFFICEXP   (2)                 
#define LL_OPTION_MENUSTYLE_OFFICE2003 (3)                 
#define LL_OPTION_RULERSTYLE           (92)                 /* default: LL_OPTION_RULERSTYLE_FLAT */
#define LL_OPTION_RULERSTYLE_FLAT      (0x10)              
#define LL_OPTION_RULERSTYLE_GRADIENT  (0x80)              
#define LL_OPTION_STATUSBARSTYLE       (93)                
#define LL_OPTION_STATUSBARSTYLE_STANDARD (0)                 
#define LL_OPTION_STATUSBARSTYLE_OFFICEXP (1)                 
#define LL_OPTION_STATUSBARSTYLE_OFFICE2003 (2)                 
#define LL_OPTION_TABBARSTYLE          (94)                
#define LL_OPTION_TABBARSTYLE_STANDARD (0)                 
#define LL_OPTION_TABBARSTYLE_OFFICEXP (1)                 
#define LL_OPTION_TABBARSTYLE_OFFICE2003 (2)                 
#define LL_OPTION_DROPWINDOWSTYLE      (95)                
#define LL_OPTION_DROPWINDOWSTYLE_STANDARD (0)                 
#define LL_OPTION_DROPWINDOWSTYLE_OFFICEXP (1)                 
#define LL_OPTION_DROPWINDOWSTYLE_OFFICE2003 (2)                 
#define LL_OPTION_DROPWINDOWSTYLEMASK  (0x0f)              
#define LL_OPTION_DROPWINDOWSTYLEFLAG_CANCLOSE (0x20)              
#define LL_OPTION_INTERFACEWRAPPER     (96)                 /* returns IL<n>* */
#define LL_OPTION_FONTQUALITY          (97)                 /* LOGFONT.lfQuality, default: DEFAULT_QUALITY */
#define LL_OPTION_FONTPRECISION        (98)                 /* LOGFONT.lfOutPrecision, default: OUT_STRING_PRECIS */
#define LL_OPTION_UISTYLE              (99)                 /* UI collection, w/o */
#define LL_OPTION_UISTYLE_STANDARD     (0)                  /* 90=0x40, 91=1, 92=0x10, 93=0, 94=0, 95=0x20 */
#define LL_OPTION_UISTYLE_OFFICEXP     (1)                  /* 90=0x41, 91=2, 92=0x10, 93=1, 94=1, 95=0x21 */
#define LL_OPTION_UISTYLE_OFFICE2003   (2)                  /* 90=0x42, 91=3, 92=0x10, 93=2, 94=2, 95=0x22 */
#define LL_OPTION_NOFILEVERSIONUPGRADEWARNING (100)                /* default: FALSE */
#define LL_OPTION_UPDATE_FOOTER_ON_DATALINEBREAK_AT_FIRST_LINE (101)                /* default: FALSE */
#define LL_OPTION_ESC_CLOSES_PREVIEW   (102)                /* shall ESC close the preview window (default: FALSE) */
#define LL_OPTION_VIEWER_ASSUMES_TEMPFILE (103)                /* shall the viewer assume that the file is a temporary file (and not store values in it)? default TRUE */
#define LL_OPTION_CALC_USED_VARS       (104)                /* default: TRUE */
#define LL_OPTION_BOTTOMALIGNMENT_WIN9X_UNLIKE_NT (105)                /* default: TRUE */
#define LL_OPTION_NOPRINTJOBSUPERVISION (106)                /* default: TRUE */
#define LL_OPTION_CALC_SUMVARS_ON_PARTIAL_LINES (107)                /* default: FALSE */
#define LL_OPTION_BLACKNESS_SCM        (108)                /* default: 0 */
#define LL_OPTION_PROHIBIT_USERINTERACTION (109)                /* default: FALSE */
#define LL_OPTION_PERFMON_INSTALL      (110)                /* w/o, TRUE to install, FALSE to uninstall */
#define LL_OPTION_VARLISTBUCKETCOUNT   (112)                /* applied to future jobs only, default 1000 */
#define LL_OPTION_MSFAXALLOWED         (113)                /* global flag - set at start of LL! Will allow/prohibit fax detection. Default: TRUE */
#define LL_OPTION_AUTOPROFILINGTICKS   (114)                /* global flag - set at start of LL! Activates LL's thread profiling */
#define LL_OPTION_PROJECTBACKUP        (115)                /* default: TRUE */
#define LL_OPTION_ERR_ON_FILENOTFOUND  (116)                /* default: FALSE */
#define LL_OPTION_NOFAXVARS            (117)                /* default: FALSE */
#define LL_OPTION_NOMAILVARS           (118)                /* default: FALSE */
#define LL_OPTION_PATTERNRESCOMPATIBILITY (119)                /* default: FALSE */
#define LL_OPTION_NODELAYEDVALUECACHING (120)                /* default: FALSE */
#define LL_OPTION_FEATURE              (1000)              
#define LL_OPTION_FEATURE_CLEARALL     (0)                 
#define LL_OPTION_FEATURE_SUPPRESS_JPEG_DISPLAY (1)                 
#define LL_OPTION_FEATURE_SUPPRESS_JPEG_CREATION (2)                 
#define LL_OPTION_VARLISTDISPLAY       (121)                /* default: LL_OPTION_VARLISTDISPLAY_VARSORT_DECLARATIONORDER | LL_OPTION_VARLISTDISPLAY_FOLDERPOS_DECLARATIONORDER, see also LL_OPTION_SORTVARIABLES */
#define LL_OPTION_VARLISTDISPLAY_VARSORT_DECLARATIONORDER (0x0000)            
#define LL_OPTION_VARLISTDISPLAY_VARSORT_ALPHA (0x0001)            
#define LL_OPTION_VARLISTDISPLAY_VARSORT_MASK (0x000f)            
#define LL_OPTION_VARLISTDISPLAY_FOLDERPOS_DECLARATIONORDER (0x0000)            
#define LL_OPTION_VARLISTDISPLAY_FOLDERPOS_ALPHA (0x0010)             /* only if LL_OPTION_VARLISTDISPLAY_VARSORT_ALPHA is set */
#define LL_OPTION_VARLISTDISPLAY_FOLDERPOS_TOP (0x0020)            
#define LL_OPTION_VARLISTDISPLAY_FOLDERPOS_BOTTOM (0x0030)            
#define LL_OPTION_VARLISTDISPLAY_FOLDERPOS_MASK (0x00f0)            
#define LL_OPTION_WORKAROUND_RTFBUG_EMPTYFIRSTPAGE (122)               
#define LL_OPTION_FORMULASTRINGCOMPARISONS_CASESENSITIVE (123)                /* default: TRUE */
#define LL_OPTION_FIELDS_IN_PROJECTPARAMETERS (124)                /* default: FALSE */
#define LL_OPTION_CHECKWINDOWTHREADEDNESS (125)                /* default: FALSE */
#define LL_OPTION_ISUSED_WILDCARD_AT_START (126)                /* default: FALSE */
#define LL_OPTION_ROOT_MUST_BE_MASTERTABLE (127)                /* default: FALSE */
#define LL_OPTIONSTR_LABEL_PRJEXT      (0)                  /* internal... (compatibility to L6) */
#define LL_OPTIONSTR_LABEL_PRVEXT      (1)                  /* internal... (compatibility to L6) */
#define LL_OPTIONSTR_LABEL_PRNEXT      (2)                  /* internal... (compatibility to L6) */
#define LL_OPTIONSTR_CARD_PRJEXT       (3)                  /* internal... (compatibility to L6) */
#define LL_OPTIONSTR_CARD_PRVEXT       (4)                  /* internal... (compatibility to L6) */
#define LL_OPTIONSTR_CARD_PRNEXT       (5)                  /* internal... (compatibility to L6) */
#define LL_OPTIONSTR_LIST_PRJEXT       (6)                  /* internal... (compatibility to L6) */
#define LL_OPTIONSTR_LIST_PRVEXT       (7)                  /* internal... (compatibility to L6) */
#define LL_OPTIONSTR_LIST_PRNEXT       (8)                  /* internal... (compatibility to L6) */
#define LL_OPTIONSTR_LLXPATHLIST       (12)                
#define LL_OPTIONSTR_SHORTDATEFORMAT   (13)                
#define LL_OPTIONSTR_DECIMAL           (14)                 /* decimal point, default: system */
#define LL_OPTIONSTR_THOUSAND          (15)                 /* thousands separator, default: system */
#define LL_OPTIONSTR_CURRENCY          (16)                 /* currency symbol, default: system */
#define LL_OPTIONSTR_EXPORTS_AVAILABLE (17)                 /* r/o */
#define LL_OPTIONSTR_EXPORTS_ALLOWED   (18)                
#define LL_OPTIONSTR_DEFDEFFONT        (19)                 /* in "{(r,g,b),size,<logfont>}" */
#define LL_OPTIONSTR_EXPORTFILELIST    (20)                
#define LL_OPTIONSTR_MAILTO            (24)                 /* default TO: address for mailing from viewer */
#define LL_OPTIONSTR_MAILTO_CC         (25)                 /* default CC: address for mailing from viewer */
#define LL_OPTIONSTR_MAILTO_BCC        (26)                 /* default BCC: address for mailing from viewer */
#define LL_OPTIONSTR_MAILTO_SUBJECT    (27)                 /* default subject for mailing from viewer */
#define LL_OPTIONSTR_SAVEAS_PATH       (28)                 /* default filename for saving the LL file from viewer */
#define LL_OPTIONSTR_LABEL_PRJDESCR    (29)                 /* "Etikett" ... */
#define LL_OPTIONSTR_CARD_PRJDESCR     (30)                
#define LL_OPTIONSTR_LIST_PRJDESCR     (31)                
#define LL_OPTIONSTR_LLFILEDESCR       (32)                 /* "Vorschau-Datei" */
#define LL_OPTIONSTR_PROJECTPASSWORD   (33)                 /* w/o, of course :) */
#define LL_OPTIONSTR_FAX_RECIPNAME     (34)                
#define LL_OPTIONSTR_FAX_RECIPNUMBER   (35)                
#define LL_OPTIONSTR_FAX_QUEUENAME     (36)                
#define LL_OPTIONSTR_FAX_SENDERNAME    (37)                
#define LL_OPTIONSTR_FAX_SENDERCOMPANY (38)                
#define LL_OPTIONSTR_FAX_SENDERDEPT    (39)                
#define LL_OPTIONSTR_FAX_SENDERBILLINGCODE (40)                
#define LL_OPTIONSTR_FAX_AVAILABLEQUEUES (42)                 /* r/o (Tab-separated) [job can be -1 or a valid job] */
#define LL_OPTIONSTR_LOGFILEPATH       (43)                
#define LL_OPTIONSTR_LICENSINGINFO     (44)                 /* w/o, SERNO to define licensing state */
#define LL_OPTIONSTR_PRINTERALIASLIST  (45)                 /* multiple "PrnOld=PrnNew1[;PrnNew2[;...]]", erase with NULL or "" */
#define LL_OPTIONSTR_PREVIEWFILENAME   (46)                 /* path of preview file (directory will be overridden by LlSetPrinterDefaultsDir(), if given) */
#define LL_OPTIONSTR_EXPORTS_ALLOWED_IN_PREVIEW (47)                 /* set in preview file */
#define LL_OPTIONSTR_HELPFILENAME      (48)                
#define LL_OPTIONSTR_NULLVALUE         (49)                 /* string which represents the NULL value */
#define LL_SYSCOMMAND_MINIMIZE         (-1)                
#define LL_SYSCOMMAND_MAXIMIZE         (-2)                
#define LL_DLGBOXMODE_3DBUTTONS        (0x8000)             /* 'or'ed */
#define LL_DLGBOXMODE_3DFRAME2         (0x4000)             /* 'OR'ed */
#define LL_DLGBOXMODE_3DFRAME          (0x1000)             /* 'OR'ed */
#define LL_DLGBOXMODE_NOBITMAPS        (0x2000)             /* 'or'ed */
#define LL_DLGBOXMODE_DONTCARE         (0x0000)             /* load from INI */
#define LL_DLGBOXMODE_SAA              (0x0001)            
#define LL_DLGBOXMODE_ALT1             (0x0002)            
#define LL_DLGBOXMODE_ALT2             (0x0003)            
#define LL_DLGBOXMODE_ALT3             (0x0004)            
#define LL_DLGBOXMODE_ALT4             (0x0005)            
#define LL_DLGBOXMODE_ALT5             (0x0006)            
#define LL_DLGBOXMODE_ALT6             (0x0007)            
#define LL_DLGBOXMODE_ALT7             (0x0008)            
#define LL_DLGBOXMODE_ALT8             (0x0009)             /* Win95 */
#define LL_DLGBOXMODE_ALT9             (0x000A)             /* Win98 */
#define LL_DLGBOXMODE_ALT10            (0x000B)             /* Win98 with gray/color button bitmaps like IE4 */
#define LL_DLGBOXMODE_TOOLTIPS98       (0x0800)             /* 'OR'ed - sliding tooltips */
#define LL_CTL_ADDTOSYSMENU            (0x00000004)         /* from CTL */
#define LL_CTL_ALSOCHILDREN            (0x00000010)        
#define LL_CTL_CONVERTCONTROLS         (0x00010000)        
#define LL_GROUP_ALWAYSFOOTER          (0x40000000)        
#define LL_PRINTERCONFIG_SAVE          (1)                 
#define LL_PRINTERCONFIG_RESTORE       (2)                 
#define LL_RTFTEXTMODE_RTF             (0x0000)            
#define LL_RTFTEXTMODE_PLAIN           (0x0001)            
#define LL_RTFTEXTMODE_EVALUATED       (0x0000)            
#define LL_RTFTEXTMODE_RAW             (0x0002)            
#define LL_ERR_BAD_JOBHANDLE           (-1)                 /* bad jobhandle */
#define LL_ERR_TASK_ACTIVE             (-2)                 /* LlDefineLayout() only once in a job */
#define LL_ERR_BAD_OBJECTTYPE          (-3)                 /* nObjType must be one of the allowed values (obsolete constant) */
#define LL_ERR_BAD_PROJECTTYPE         (-3)                 /* nObjType must be one of the allowed values */
#define LL_ERR_PRINTING_JOB            (-4)                 /* print job not opened, no print object */
#define LL_ERR_NO_BOX                  (-5)                 /* LlPrintSetBoxText(...) called when no abort box exists! */
#define LL_ERR_ALREADY_PRINTING        (-6)                 /* LlPrintWithBoxStart(...): another print job is being done, please wait or try LlPrintStart(...) */
#define LL_ERR_NOT_YET_PRINTING        (-7)                 /* LlPrintGetOptionString... */
#define LL_ERR_NO_PROJECT              (-10)                /* object with requested name does not exist (former ERR_NO_OBJECT) */
#define LL_ERR_NO_PRINTER              (-11)                /* printer couldn't be opened */
#define LL_ERR_PRINTING                (-12)                /* error while printing */
#define LL_ERR_EXPORTING               (-13)                /* error while exporting */
#define LL_ERR_NEEDS_VB                (-14)                /* '11...' needs VB.EXE */
#define LL_ERR_BAD_PRINTER             (-15)                /* PrintOptionsDialog(): no printer available */
#define LL_ERR_NO_PREVIEWMODE          (-16)                /* Preview functions: not in preview mode */
#define LL_ERR_NO_PREVIEWFILES         (-17)                /* PreviewDisplay(): no file found */
#define LL_ERR_PARAMETER               (-18)                /* bad parameter (usually NULL pointer) */
#define LL_ERR_BAD_EXPRESSION          (-19)                /* bad expression in LlExprEvaluate() and LlExprType() */
#define LL_ERR_BAD_EXPRMODE            (-20)                /* bad expression mode (LlSetExpressionMode()) */
#define LL_ERR_NO_TABLE                (-21)                /* not used */
#define LL_ERR_CFGNOTFOUND             (-22)                /* on LlPrintStart(), LlPrintWithBoxStart() [not found] */
#define LL_ERR_EXPRESSION              (-23)                /* on LlPrintStart(), LlPrintWithBoxStart() */
#define LL_ERR_CFGBADFILE              (-24)                /* on LlPrintStart(), LlPrintWithBoxStart() [read error, bad format] */
#define LL_ERR_BADOBJNAME              (-25)                /* on LlPrintEnableObject() - not a ':' at the beginning */
#define LL_ERR_NOOBJECT                (-26)                /* on LlPrintEnableObject() - "*" and no object in project */
#define LL_ERR_UNKNOWNOBJECT           (-27)                /* on LlPrintEnableObject() - object with that name not existing */
#define LL_ERR_NO_TABLEOBJECT          (-28)                /* LlPrint...Start() and no list in Project, or: */
#define LL_ERR_NO_OBJECT               (-29)                /* LlPrint...Start() and no object in project */
#define LL_ERR_NO_TEXTOBJECT           (-30)                /* LlPrintGetTextCharsPrinted() and no printable text in Project! */
#define LL_ERR_UNKNOWN                 (-31)                /* LlPrintIsVariableUsed(), LlPrintIsFieldUsed() */
#define LL_ERR_BAD_MODE                (-32)                /* LlPrintFields(), LlPrintIsFieldUsed() called on non-OBJECT_LIST */
#define LL_ERR_CFGBADMODE              (-33)                /* on LlDefineLayout(), LlPrint...Start(): file is in wrong expression mode */
#define LL_ERR_ONLYWITHONETABLE        (-34)                /* on LlDefinePageSeparation(), LlDefineGrouping() */
#define LL_ERR_UNKNOWNVARIABLE         (-35)                /* on LlGetVariableContents() */
#define LL_ERR_UNKNOWNFIELD            (-36)                /* on LlGetFieldContents() */
#define LL_ERR_UNKNOWNSORTORDER        (-37)                /* on LlGetFieldContents() */
#define LL_ERR_NOPRINTERCFG            (-38)                /* on LlPrintCopyPrinterConfiguration() - no or bad file */
#define LL_ERR_SAVEPRINTERCFG          (-39)                /* on LlPrintCopyPrinterConfiguration() - file could not be saved */
#define LL_ERR_RESERVED                (-40)                /* function not yet implemeted */
#define LL_ERR_NOVALIDPAGES            (-41)                /* could also be that 16 bit Viewer tries to open 32bit-only storage */
#define LL_ERR_NOTINHOSTPRINTERMODE    (-42)                /* cannot be done in Host Printer Mode (LlSetPrinterInPrinterFile()) */
#define LL_ERR_NOTFINISHED             (-43)                /* appears when a project reset() is done, but the table not finished */
#define LL_ERR_BUFFERTOOSMALL          (-44)                /* LlXXGetOptionStr() */
#define LL_ERR_BADCODEPAGE             (-45)                /* LL_OPTION_CODEPAGE */
#define LL_ERR_CANNOTCREATETEMPFILE    (-46)                /* cannot create temporary file */
#define LL_ERR_NODESTINATION           (-47)                /* no valid export destination */
#define LL_ERR_NOCHART                 (-48)                /* no chart control present */
#define LL_ERR_TOO_MANY_CONCURRENT_PRINTJOBS (-49)                /* WebServer: not enough print process licenses */
#define LL_ERR_BAD_WEBSERVER_LICENSE   (-50)                /* WebServer: bad license file */
#define LL_ERR_NO_WEBSERVER_LICENSE    (-51)                /* WebServer: no license file */
#define LL_ERR_INVALIDDATE             (-52)                /* LlSystemTimeFromLocaleString(): date not valid! */
#define LL_ERR_DRAWINGNOTFOUND         (-53)                /* only if LL_OPTION_ERR_ON_FILENOTFOUND set */
#define LL_ERR_NOUSERINTERACTION       (-54)                /* a call is used which would show a dialog, but LL is in Webserver mode */
#define LL_ERR_BADDATABASESTRUCTURE    (-55)                /* the project that is loading has a table that is not supported by the database */
#define LL_ERR_USER_ABORTED            (-99)                /* user aborted printing */
#define LL_ERR_BAD_DLLS                (-100)               /* DLLs not up to date (CTL, DWG, UTIL) */
#define LL_ERR_NO_LANG_DLL             (-101)               /* no or out-of-date language resource DLL */
#define LL_ERR_NO_MEMORY               (-102)               /* out of memory */
#define LL_ERR_EXCEPTION               (-104)               /* there was a GPF during the API execution. Any action that follows might cause problems! */
#define LL_ERR_LICENSEVIOLATION        (-105)               /* your license does not allow this call (see LL_OPTIONSTR_LICENSINGINFO) */
#define LL_ERR_NOT_SUPPORTED_IN_THIS_OS (-106)               /* the OS does not support this function */
#define LL_WRN_ISNULL                  (-995)               /* LlExprEvaluate[Var]() */
#define LL_WRN_TABLECHANGE             (-996)              
#define LL_WRN_PRINTFINISHED           (-997)               /* LlRTFDisplay() */
#define LL_WRN_REPEAT_DATA             (-998)               /* notification: page is full, prepare for next page */
#define LL_CHAR_TEXTQUOTE              (1)                 
#define LL_CHAR_PHANTOMSPACE           (2)                 
#define LL_CHAR_LOCK                   (3)                 
#define LL_CHAR_NEWLINE                (182)                /* "�" */
#define LL_CHAR_EXPRSEP                (164)                /* "�" */
#define LL_CHAR_TAB                    (247)                /* "�" */
#define LL_CHAR_EAN128NUL              (255)               
#define LL_CHAR_EAN128FNC1             (254)               
#define LL_CHAR_EAN128FNC2             (253)               
#define LL_CHAR_EAN128FNC3             (252)               
#define LL_CHAR_EAN128FNC4             (251)               
#define LL_CHAR_CODE93NUL              (255)               
#define LL_CHAR_CODE93EXDOLLAR         (254)               
#define LL_CHAR_CODE93EXPERC           (253)               
#define LL_CHAR_CODE93EXSLASH          (252)               
#define LL_CHAR_CODE93EXPLUS           (251)               
#define LL_CHAR_CODE39NUL              (255)               
#define LL_DLGEXPR_VAREXTBTN_ENABLE    (0x00000001)         /* callback for simple Wizard extension */
#define LL_DLGEXPR_VAREXTBTN_DOMODAL   (0x00000002)        
#define LL_LLX_EXTENSIONTYPE_EXPORT    (1)                 
#define LL_LLX_EXTENSIONTYPE_BARCODE   (2)                 
#define LL_LLX_EXTENSIONTYPE_OBJECT    (3)                  /* nyi */
#define LL_LLX_EXTENSIONTYPE_WIZARD    (4)                  /* nyi */
#define LL_DECLARECHARTROW_FOR_OBJECTS (0x00000001)        
#define LL_DECLARECHARTROW_FOR_TABLECOLUMNS (0x00000002)         /* body only */
#define LL_DECLARECHARTROW_FOR_TABLECOLUMNS_FOOTERS (0x00000004)        
#define LL_GETCHARTOBJECTCOUNT_CHARTOBJECTS (1)                 
#define LL_GETCHARTOBJECTCOUNT_CHARTOBJECTS_BEFORE_TABLE (2)                 
#define LL_GETCHARTOBJECTCOUNT_CHARTCOLUMNS (3)                  /* body only */
#define LL_GETCHARTOBJECTCOUNT_CHARTCOLUMNS_FOOTERS (4)                 
#define LL_GRIPT_DIM_SCM               (1)                 
#define LL_GRIPT_DIM_PERC              (2)                 
#define LL_PARAMETERFLAG_PUBLIC        (0x00000000)        
#define LL_PARAMETERFLAG_PRIVATE       (0x40000000)        
#define LL_PARAMETERFLAG_FORMULA       (0x00000000)        
#define LL_PARAMETERFLAG_VALUE         (0x20000000)        
#define LL_PARAMETERFLAG_GLOBAL        (0x00000000)        
#define LL_PARAMETERFLAG_LOCAL         (0x10000000)        
#define LL_PARAMETERFLAG_MASK          (0xffff0000)        
#define LL_PARAMETERTYPE_USER          (0)                 
#define LL_PARAMETERTYPE_FAX           (1)                 
#define LL_PARAMETERTYPE_MAIL          (2)                 
#define LL_PARAMETERTYPE_MASK          (0x0000ffff)        
#define LL_LOCCONVERSION_LCID          (0)                 
#define LL_LOCCONVERSION_COUNTRYPREFIX (1)                 
#define LL_LOCCONVERSION_COUNTRYISONAME (2)                 
#define LL_LOCCONVERSION_DIALPREFIX    (3)                  /* Not yet implemented */

/*--- function declaration ---*/

#if !defined(_RC_INVOKED_) && !defined(RC_INVOKED)

#ifdef __ZTC__ /* Zortech C++ */
#pragma ZTC align 1
#elif __WATCOMC__ > 1000 /* Watcom C++ >= 10.5 */
#pragma pack(push,1)
#elif __BORLANDC__ /* Borland C++ */
#pragma option -a1
#else
#pragma pack(1) /* MS, Watcom <= 10.0, ... */
#endif

#ifdef __cplusplus
extern "C" {
#endif

extern HINSTANCE hDLLLL11;
extern INT nDLLLL11Counter;

#ifdef IMPLEMENTATION
  #define extern /* */
  HINSTANCE hDLLLL11 = NULL;
  INT       nDLLLL11Counter = 0;
#endif


 #ifndef IMPLEMENTATION
 typedef struct
  {
  UINT  _nSize;     // size of the structure
  LPARAM _lParam;    // parameter (most likely address of structure)
  LPARAM _lReply;    // reply (defaults to 0)
   UINT32   _lUserParameter;        // user parameter
  } scLlCallback, FAR *PSCLLCALLBACK;
 
 typedef struct
  {
  UINT  _nSize;     // size of the structure
  HWND _hWnd;     // L&L mainframe wnd
  UINT _nTotal;    // total count of objects
  UINT _nCurrent;    // current object number (0=start,...,total=end)
  UINT _nJob;     // LL_METERINFO_... constants
  } scLlMeterInfo, FAR *PSCLLMETERINFO;
 
 typedef struct
  {
  UINT  _nSize;     // size of the structure
  LPCSTR _pszContents;   // contents of the parameter string (do not overwrite!!!)
  BOOL _bEvaluate;    // TRUE on evaluation, FALSE on syntax check (fill _bError, _szError)!
   CHAR _szNewValue[0x4000+1]; // new value
   UINT _bError;    // FALSE
  CHAR _szError[128];   // error text
  } scLlExtFctA;
   typedef struct
    {
    UINT    _nSize;
    LPCWSTR _pszContents;
    BOOL   _bEvaluate;
     WCHAR   _szNewValue[0x4000+1];
     UINT   _bError;
    WCHAR   _szError[128];
    } scLlExtFctW;
   #ifdef UNICODE
     typedef scLlExtFctW scLlExtFct, FAR* PSCLLEXTFCT;
    #else
     typedef scLlExtFctA scLlExtFct, FAR* PSCLLEXTFCT;
 #endif
 
 typedef struct     // internal use (same struct as BASIC struct)
  {
  UINT  _nSize;     // size of the structure
  LPVOID _pszContents;   // contents of the parameter string (do not overwrite!!!)
  BOOL _bEvaluate;    // TRUE on evaluation, FALSE on syntax check (fill _bError, _szError)!
   LPVOID _szNewValue;   // new value
   UINT _bError;    // FALSE
  LPVOID _szError;    // error text
  } scLlExtFctXXX, FAR *PSCLLEXTFCTXXX;
 
 #if defined(USE_OCX)
  typedef struct     // internal use (same struct as BASIC struct)
  {
  UINT  _nSize;     // size of the structure
  LPVOID _pszContents;   // contents of the parameter string (do not overwrite!!!)
  BOOL _bEvaluate;    // TRUE on evaluation, FALSE on syntax check (fill _bError, _szError)!
  BSTR _szNewValue;   // new value
  UINT _bError;    // FALSE
  BSTR _szError;    // error text
  } scLlExtFctXXX_B, FAR *PSCLLEXTFCTXXX_B;
 #endif
 
 typedef struct
  {
  UINT  _nSize;     // size of the structure
  LPCSTR _pszName;    // name of the object
  INT  _nType;     // see LL_OBJ_... values
  BOOL _bPreDraw;    // TRUE on call before draw, FALSE on call afterwards
  HDC  _hRefDC;      // HDC to get information from
  HDC  _hPaintDC;     // HDC to paint on
  RECT _rcPaint;    // RECT in paint units (mm/10 or inch/100)
  } scLlObjectA;
   typedef struct
    {
    UINT  _nSize;
    LPCWSTR _pszName;
    INT _nType;
    BOOL _bPreDraw;
    HDC _hRefDC;
    HDC _hPaintDC;
    RECT _rcPaint;
    } scLlObjectW;
   #ifdef UNICODE
     typedef scLlObjectW scLlObject, FAR* PSCLLOBJECT;
    #else
     typedef scLlObjectA scLlObject, FAR* PSCLLOBJECT;
   #endif
 
 #if defined(USE_OCX)
 typedef struct
  {
  UINT  _nSize;     // size of the structure
  BSTR _pszName;    // name of the object
  INT  _nType;     // see LL_OBJ_... values
  BOOL _bPreDraw;    // TRUE on call before draw, FALSE on call afterwards
  HDC  _hRefDC;      // HDC to get information from
  HDC  _hPaintDC;     // HDC to paint on
  RECT _rcPaint;    // RECT in paint units (mm/10 or inch/100)
  } scLlObject_B, FAR *PSCLLOBJECT_B;
 #endif
 
 typedef struct
  {
  UINT  _nSize;     // size of the structure
  BOOL _bDesignerPreview;  // flag if in Preview Mode or not
  BOOL _bPreDraw;    // TRUE on call before draw, FALSE on call afterwards
  HDC  _hRefDC;      // HDC to get information from
  HDC  _hPaintDC;     // HDC to paint on
   } scLlPage, FAR *PSCLLPAGE;
 
 typedef struct
  {
  UINT  _nSize;     // size of the structure
  BOOL _bDesignerPreview;  // flag if in Preview Mode or not
  BOOL _bPreDraw;    // TRUE on call before draw, FALSE on call afterwards
  HDC  _hRefDC;      // HDC to get information from
  HDC  _hPaintDC;     // HDC to paint on
  RECT _rcPaint;    // RECT in paint units (mm/10 or inch/100)
   } scLlProject, FAR *PSCLLPROJECT;
 
 typedef struct
  {
  UINT  _nSize;     // size of the structure
  LPCSTR _pszName;    // name of the variable
  LPCSTR _pszContents;   // contents of the variable (valid if def�d by VariableExt())
  INT32 _lPara;     // lPara of the variable
  LPVOID _lpPtr;     // lpPtr of the variable
  HANDLE _hPara;     // hContents of the variable (valid if def�d by VariableExtHandle())
  BOOL _bIsotropic;   // "Isotropic" flag
  LPCSTR _pszParameters;   // for editable USERDWG: parameters
  HDC  _hRefDC;      // HDC to get information from
  HDC  _hPaintDC;     // HDC to paint on
  RECT _rcPaint;    // RECT in paint units (mm/10 or inch/100)
   INT  _nPaintMode;   // 0: Preview, 1: FastPreview, 2: Workspace (NYI)
  } scLlDrawUserObjA;
   typedef struct
    {
    UINT   _nSize;
    LPCWSTR _pszName;
    LPCWSTR _pszContents;
    INT32  _lPara;
    LPVOID _lpPtr;
    HANDLE _hPara;
    BOOL  _bIsotropic;
    LPCWSTR _pszParameters;
    HDC  _hRefDC;
    HDC  _hPaintDC;
    RECT  _rcPaint;
     INT  _nPaintMode;
    } scLlDrawUserObjW;
   #ifdef UNICODE
     typedef scLlDrawUserObjW scLlDrawUserObj, FAR* PSCLLDRAWUSEROBJ;
    #else
     typedef scLlDrawUserObjA scLlDrawUserObj, FAR* PSCLLDRAWUSEROBJ;
   #endif
 
 #if defined(USE_OCX)
 typedef struct
  {
  UINT  _nSize;     // size of the structure
  BSTR _pszName;    // name of the variable
  BSTR _pszContents;   // contents of the variable (valid if def�d by VariableExt())
  INT32 _lPara;     // lPara of the variable
  LPVOID _lpPtr;     // lpPtr of the variable
  HANDLE _hPara;     // hContents of the variable (valid if def�d by VariableExtHandle())
  BOOL _bIsotropic;   // "Isotropic" flag
  BSTR _pszParameters;  // for editable USERDWG: parameters
  HDC  _hRefDC;      // HDC to get information from
  HDC  _hPaintDC;     // HDC to paint on
  RECT _rcPaint;    // RECT in paint units (mm/10 or inch/100)
   INT  _nPaintMode;   // 0: Preview, 1: FastPreview, 2: Workspace (NYI)
  } scLlDrawUserObj_B, FAR *PSCLLDRAWUSEROBJ_B;
 #endif
 
 typedef struct
  {
  UINT  _nSize;     // size of the structure
  LPCSTR _pszName;    // name of the variable
  INT32 _lPara;     // lPara of the variable
  LPVOID _lpPtr;     // lpPtr of the variable
  HANDLE _hPara;     // hContents of the variable (valid if def�d by VariableExtHandle())
  BOOL _bIsotropic;   // "Isotropic" flag
  HWND _hWnd;     // parent window for dialog
  LPSTR _pszParameters;   // parameter buffer
  INT  _nParaBufSize;   // max size of buffer (1024+1)
  } scLlEditUserObjA;
   typedef struct
    {
    UINT     _nSize;
    LPCWSTR _pszName;
    INT32  _lPara;
    LPVOID _lpPtr;
    HANDLE _hPara;
    BOOL  _bIsotropic;
     HWND  _hWnd;
     LPWSTR _pszParameters;
     INT  _nParaBufSize;
    } scLlEditUserObjW;
   #ifdef UNICODE
     typedef scLlEditUserObjW scLlEditUserObj, FAR* PSCLLEDITUSEROBJ;
    #else
     typedef scLlEditUserObjA scLlEditUserObj, FAR* PSCLLEDITUSEROBJ;
   #endif
 
 #if defined(USE_OCX)
 typedef struct
  {
  UINT  _nSize;     // size of the structure
  BSTR _pszName;    // name of the variable
  INT32 _lPara;     // lPara of the variable
  LPVOID _lpPtr;     // lpPtr of the variable
  HANDLE _hPara;     // hContents of the variable (valid if def�d by VariableExtHandle())
  BOOL _bIsotropic;   // "Isotropic" flag
  HWND _hWnd;     // parent window for dialog
  BSTR _pszParameters;   // parameter buffer
  INT  _nParaBufSize;   // max size of buffer (1024+1)
  } scLlEditUserObj_B, FAR *PSCLLEDITUSEROBJ_B;
 #endif
 
 typedef struct
  {
  UINT  _nSize;     // size of the structure
  INT  _nType;     // LL_TABLE_LINE_xxx constant
  HDC  _hRefDC;      // HDC to get information from
  HDC  _hPaintDC;     // HDC to paint on
  RECT _rcPaint;    // RECT in paint units (mm/10 or inch/100)
  INT  _nPageLine;       // number of line on page
  INT  _nLine;     // absolute number of line in table
  INT  _nLineDef;    // table line definition
  BOOL _bZebra;    // zebra mode selected by user (only body line!)
  RECT _rcSpacing;    // spaces around the frame
  } scLlTableLine, FAR *PSCLLTABLELINE;
 
 typedef struct
  {
  UINT  _nSize;     // size of the structure
  INT  _nType;     // LL_TABLE_FIELD_xxx constant
  HDC  _hRefDC;      // HDC to get information from
  HDC  _hPaintDC;     // HDC to paint on
  RECT _rcPaint;    // RECT in paint units (mm/10 or inch/100)
  INT  _nLineDef;    // table line definition
  INT  _nIndex;       // column index (0..)
  RECT _rcSpacing;    // spaces around the frame
  } scLlTableField, FAR *PSCLLTABLEFIELD;
 
 typedef struct
   {
  UINT  _nSize;     // size of the structure
  INT  _xPos;     // column position (paint units)
  INT  _nWidth;    // width (paint units)
  HFONT _fntColumnX;   // column font (NULL)
   } scLlColumn;
 
 typedef struct
  {
  UINT  _nSize;     // size of the structure
  HDC  _hRefDC;    // HDC to get information from
  HDC  _hPaintDC;     // HDC to paint on
  RECT _rcPaint;    // suggested RECT in paint units (mm/10 or inch/100), change .bottom value
   HFONT _fntHeaderX;   // header default font
   HFONT _fntBodyX;    // body   default font
   HFONT _fntFooterX;   // footer default font
  INT  _nHeight;    // height of one body line (incl. spacing)
  BOOL _bPaint;    // do paint, or do just calculate?
  INT32 _lParam;    // function argument: user parameter 1
  LPVOID _lpParam;    // function argument: user parameter 2
  INT  _nColumns;    // number of columns in this table
  PSCLLCOLUMN _lpColumns;      // pointer to an array of column info
  } scLlGroup, FAR *PSCLLGROUP;
 
 #if defined(USE_OCX)
 typedef struct
  {
  UINT  _nSize;     // size of the structure
  HDC  _hRefDC;    // HDC to get information from
  HDC  _hPaintDC;     // HDC to paint on
  RECT _rcPaint;    // suggested RECT in paint units (mm/10 or inch/100), change .bottom value
   HFONT _fntHeader;    // header default font
   HFONT _fntBody;    // body   default font
   HFONT _fntFooter;    // footer default font
  INT  _nHeight;    // height of one body line (incl. spacing)
  BOOL _bPaint;    // do paint, or do just calculate?
  BOOL _bFrameLeft;   // frame on left side selected?
  BOOL _bFrameRight;   // frame on right side selected?
  INT32 _lParam;    // function argument: user parameter 1
  BSTR _lpParam;    // function argument: user parameter 2
  INT  _nColumns;    // number of columns in this table
  PSCLLCOLUMN _lpColumns;      // pointer to an array of column info
  } scLlGroup_B, FAR *PSCLLGROUP_B;
 #endif
 
 typedef struct
  {
  UINT _nSize;     // size of the structure
  BOOL _bFirst;    // first or second printer?
   INT  _nCmd;     // i command
   HWND    _hWnd;     // i
   HDC  _hDC;     // i/o
   INT16 _nOrientation;   // i/o
  BOOL _bPhysPage;    // i/o, new L5
   UINT    _nBufSize;    // i
   LPSTR   _pszBuffer;    // i, fill
   INT     _nUniqueNumber;   // i
   INT  _nUniqueNumberCompare;  // i
   INT  _nPaperFormat;   // i/o (JobID for LL_CMND_CHANGE_DCPROPERTIES_DOC)
   INT    _xPaperSize;   // i/o, mm/10
   INT    _yPaperSize;   // i/o, mm/10
  } scLlPrinterA;
 #if WIN16
   typedef scLlPrinterA scLlPrinter, FAR* PSCLLPRINTER;
 #else
   typedef struct
    {
    UINT  _nSize;
    BOOL  _bFirst;
     INT  _nCmd;
     HWND     _hWnd;
     HDC  _hDC;
     INT16  _nOrientation;
    BOOL  _bPhysPage;
     UINT     _nBufSize;
     LPWSTR    _pszBuffer;
     INT      _nUniqueNumber;
     INT  _nUniqueNumberCompare;
     INT  _nPaperFormat;   // i/o (JobID for LL_CMND_CHANGE_DCPROPERTIES_DOC)
     INT    _xPaperSize;   // i/o, mm/10
     INT    _yPaperSize;   // i/o, mm/10
    } scLlPrinterW;
   #ifdef UNICODE
     typedef scLlPrinterW scLlPrinter, FAR* PSCLLPRINTER;
    #else
     typedef scLlPrinterA scLlPrinter, FAR* PSCLLPRINTER;
   #endif
 #endif
 
 typedef LRESULT (FAR PASCAL *LLNOTIFICATION)(UINT nMessage, LPARAM lParam, UINT32 lUserParam); // use MakeProcInstance or smart callbacks
 
 typedef struct                    // for VC++
   {
   UINT32   FAR *ppStruct;
  LPVOID nIndex;
   } PTRPARAM;
 typedef struct                    // for VC++
   {
  INT  FAR *ppnID;
  INT  FAR *ppnType;
  LPVOID nIndex;
   } HELPPARAM;
 typedef struct                    // for VC++
   {
   INT     FAR *ppnFunction;
  LPVOID nIndex;
   } NTFYBTNPARAM;
 typedef struct
  {
  UINT  _nSize;     // size of the structure
  UINT _nFunction;    // function code (LL_DLGEXPR_VAREXTBTN_xxx)
   HWND _hWndDialog;   // dialog handle
  LPCSTR _pszPage;    // "CondDlgVar"
   CHAR _szValue[0x4000+1];  // new value
   UINT _bFields;    // FALSE
  UINT32 _nMask;     // LL_TEXT, ... OR LL_FOOTERFIELD...
  } scLlDlgExprVarExtA;
   typedef struct
    {
    UINT     _nSize;
    UINT  _nFunction;
     HWND  _hWndDialog;
    LPCWSTR _pszPage;
     WCHAR  _szValue[0x4000+1];
     UINT  _bFields;
    UINT32 _nMask;
    } scLlDlgExprVarExtW;
   #ifdef UNICODE
     typedef scLlDlgExprVarExtW scLlDlgExprVarExt, FAR* PSCLLDLGEXPRVAREXT;
    #else
     typedef scLlDlgExprVarExtA scLlDlgExprVarExt, FAR* PSCLLDLGEXPRVAREXT;
   #endif
 
 typedef struct
  {
  UINT _nSize;
  HWND _hWnd;
  UINT _nType;
  HMENU _hMenu;
  UINT _nParam;
  } scLlToolbar, FAR *PSCLLTOOLBAR;
 
 struct scLlDlgEditLineExParams
   {
   INT   _nSize;
   BOOL  _bEnableAskString;
  };
 struct scLlDlgEditLineExParams8
   {
   INT   _nSize;
   BOOL  _bEnableAskString;
   UINT  _bIncludeChartFields;
  };
 struct scLlPrintJobInfo
  {
  INT   _nSize;
  HLLJOB  _hLlJob; // the LL job that issued the job
  TCHAR  _szDevice[80]; // printer device name
  DWORD  _dwJobID; // an internal ID, NOT THE QUEUE ID (as different queues could have different IDs)
  DWORD  _dwState; // state (JOB_STATUS_xxx, see Windows API)
  };
 
 struct scLlDelayDefineFieldOrVariable
  {
  INT   _nSize;
  BOOL  _bAlsoField;
  LPCTSTR  _pszVarName;
  };
 
 struct scLlDelayedValue
  {
  INT   _nSize;
  LPCTSTR  _pszVarName;
  LPTSTR  _pszContents;
  UINT  _nBufSize; // size of buffer '_pszContents' points to. If a larger buffer is needed, _pszContents must be changed to point to that (static) buffer in the host application
  HANDLE  _hContents;
  };
 
 struct scLlProjectUserData
  {
  INT   _nSize;
  BOOL  _bStoring;
  IStream* _pContentStream;
  };
 #endif // ifndef IMPLEMENTATION
 
 
 #ifdef _DEF_LLXINTERFACE // LL internal
   #include "lxoem.h"
 #endif // _DEF_LLXINTERFACE



/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef   HLLJOB ( DLLPROC *PFNLLJOBOPEN)(
	INT                  nLanguage);
#endif /* IMPLEMENTATION */

extern PFNLLJOBOPEN LlJobOpen;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef   HLLJOB ( DLLPROC *PFNLLJOBOPENLCID)(
	_LCID                nLCID);
#endif /* IMPLEMENTATION */

extern PFNLLJOBOPENLCID LlJobOpenLCID;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef     void ( DLLPROC *PFNLLJOBCLOSE)(
	HLLJOB               hLlJob);
#endif /* IMPLEMENTATION */

extern PFNLLJOBCLOSE LlJobClose;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef     void ( DLLPROC *PFNLLSETDEBUG)(
	INT                  nOnOff);
#endif /* IMPLEMENTATION */

extern PFNLLSETDEBUG LlSetDebug;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef     UINT ( DLLPROC *PFNLLGETVERSION)(
	INT                  nCmd);
#endif /* IMPLEMENTATION */

extern PFNLLGETVERSION LlGetVersion;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef     UINT ( DLLPROC *PFNLLGETNOTIFICATIONMESSAGE)(
	HLLJOB               hLlJob);
#endif /* IMPLEMENTATION */

extern PFNLLGETNOTIFICATIONMESSAGE LlGetNotificationMessage;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLSETNOTIFICATIONMESSAGE)(
	HLLJOB               hLlJob,
	UINT                 nMessage);
#endif /* IMPLEMENTATION */

extern PFNLLSETNOTIFICATIONMESSAGE LlSetNotificationMessage;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef  FARPROC ( DLLPROC *PFNLLSETNOTIFICATIONCALLBACK)(
	HLLJOB               hLlJob,
	FARPROC              lpfnNotify);
#endif /* IMPLEMENTATION */

extern PFNLLSETNOTIFICATIONCALLBACK LlSetNotificationCallback;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDefineFieldO LlDefineFieldA
     #else
       #define LlDefineField LlDefineFieldA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDEFINEFIELDA)(
	HLLJOB               hLlJob,
	LPCSTR               pszVarName,
	LPCSTR               lpbufContents);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDEFINEFIELDA LlDefineFieldA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDefineField LlDefineFieldW
     #else
       #define LlDefineFieldO LlDefineFieldW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDEFINEFIELDW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszVarName,
	LPCWSTR              lpbufContents);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDEFINEFIELDW LlDefineFieldW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDefineFieldExtO LlDefineFieldExtA
     #else
       #define LlDefineFieldExt LlDefineFieldExtA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDEFINEFIELDEXTA)(
	HLLJOB               hLlJob,
	LPCSTR               pszVarName,
	LPCSTR               lpbufContents,
	INT32                lPara,
	LPVOID               lpPtr);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDEFINEFIELDEXTA LlDefineFieldExtA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDefineFieldExt LlDefineFieldExtW
     #else
       #define LlDefineFieldExtO LlDefineFieldExtW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDEFINEFIELDEXTW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszVarName,
	LPCWSTR              lpbufContents,
	INT32                lPara,
	LPVOID               lpPtr);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDEFINEFIELDEXTW LlDefineFieldExtW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDefineFieldExtHandleO LlDefineFieldExtHandleA
     #else
       #define LlDefineFieldExtHandle LlDefineFieldExtHandleA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDEFINEFIELDEXTHANDLEA)(
	HLLJOB               hLlJob,
	LPCSTR               pszVarName,
	HANDLE               hContents,
	INT32                lPara,
	LPVOID               lpPtr);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDEFINEFIELDEXTHANDLEA LlDefineFieldExtHandleA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDefineFieldExtHandle LlDefineFieldExtHandleW
     #else
       #define LlDefineFieldExtHandleO LlDefineFieldExtHandleW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDEFINEFIELDEXTHANDLEW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszVarName,
	HANDLE               hContents,
	INT32                lPara,
	LPVOID               lpPtr);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDEFINEFIELDEXTHANDLEW LlDefineFieldExtHandleW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef     void ( DLLPROC *PFNLLDEFINEFIELDSTART)(
	HLLJOB               hLlJob);
#endif /* IMPLEMENTATION */

extern PFNLLDEFINEFIELDSTART LlDefineFieldStart;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDefineVariableO LlDefineVariableA
     #else
       #define LlDefineVariable LlDefineVariableA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDEFINEVARIABLEA)(
	HLLJOB               hLlJob,
	LPCSTR               pszVarName,
	LPCSTR               lpbufContents);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDEFINEVARIABLEA LlDefineVariableA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDefineVariable LlDefineVariableW
     #else
       #define LlDefineVariableO LlDefineVariableW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDEFINEVARIABLEW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszVarName,
	LPCWSTR              lpbufContents);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDEFINEVARIABLEW LlDefineVariableW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDefineVariableExtO LlDefineVariableExtA
     #else
       #define LlDefineVariableExt LlDefineVariableExtA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDEFINEVARIABLEEXTA)(
	HLLJOB               hLlJob,
	LPCSTR               pszVarName,
	LPCSTR               lpbufContents,
	INT32                lPara,
	LPVOID               lpPtr);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDEFINEVARIABLEEXTA LlDefineVariableExtA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDefineVariableExt LlDefineVariableExtW
     #else
       #define LlDefineVariableExtO LlDefineVariableExtW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDEFINEVARIABLEEXTW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszVarName,
	LPCWSTR              lpbufContents,
	INT32                lPara,
	LPVOID               lpPtr);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDEFINEVARIABLEEXTW LlDefineVariableExtW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDefineVariableExtHandleO LlDefineVariableExtHandleA
     #else
       #define LlDefineVariableExtHandle LlDefineVariableExtHandleA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDEFINEVARIABLEEXTHANDLEA)(
	HLLJOB               hLlJob,
	LPCSTR               pszVarName,
	HANDLE               hContents,
	INT32                lPara,
	LPVOID               lpPtr);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDEFINEVARIABLEEXTHANDLEA LlDefineVariableExtHandleA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDefineVariableExtHandle LlDefineVariableExtHandleW
     #else
       #define LlDefineVariableExtHandleO LlDefineVariableExtHandleW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDEFINEVARIABLEEXTHANDLEW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszVarName,
	HANDLE               hContents,
	INT32                lPara,
	LPVOID               lpPtr);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDEFINEVARIABLEEXTHANDLEW LlDefineVariableExtHandleW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDefineVariableNameO LlDefineVariableNameA
     #else
       #define LlDefineVariableName LlDefineVariableNameA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDEFINEVARIABLENAMEA)(
	HLLJOB               hLlJob,
	LPCSTR               pszVarName);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDEFINEVARIABLENAMEA LlDefineVariableNameA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDefineVariableName LlDefineVariableNameW
     #else
       #define LlDefineVariableNameO LlDefineVariableNameW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDEFINEVARIABLENAMEW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszVarName);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDEFINEVARIABLENAMEW LlDefineVariableNameW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef     void ( DLLPROC *PFNLLDEFINEVARIABLESTART)(
	HLLJOB               hLlJob);
#endif /* IMPLEMENTATION */

extern PFNLLDEFINEVARIABLESTART LlDefineVariableStart;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDefineSumVariableO LlDefineSumVariableA
     #else
       #define LlDefineSumVariable LlDefineSumVariableA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDEFINESUMVARIABLEA)(
	HLLJOB               hLlJob,
	LPCSTR               pszVarName,
	LPCSTR               lpbufContents);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDEFINESUMVARIABLEA LlDefineSumVariableA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDefineSumVariable LlDefineSumVariableW
     #else
       #define LlDefineSumVariableO LlDefineSumVariableW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDEFINESUMVARIABLEW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszVarName,
	LPCWSTR              lpbufContents);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDEFINESUMVARIABLEW LlDefineSumVariableW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDefineLayoutO LlDefineLayoutA
     #else
       #define LlDefineLayout LlDefineLayoutA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDEFINELAYOUTA)(
	HLLJOB               hLlJob,
	HWND                 hWnd,
	LPCSTR               pszTitle,
	UINT                 nObjType,
	LPCSTR               pszObjName);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDEFINELAYOUTA LlDefineLayoutA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDefineLayout LlDefineLayoutW
     #else
       #define LlDefineLayoutO LlDefineLayoutW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDEFINELAYOUTW)(
	HLLJOB               hLlJob,
	HWND                 hWnd,
	LPCWSTR              pszTitle,
	UINT                 nObjType,
	LPCWSTR              pszObjName);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDEFINELAYOUTW LlDefineLayoutW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDlgEditLineO LlDlgEditLineA
     #else
       #define LlDlgEditLine LlDlgEditLineA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDLGEDITLINEA)(
	HLLJOB               hLlJob,
	HWND                 hWnd,
	LPSTR                lpBuf,
	INT                  nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDLGEDITLINEA LlDlgEditLineA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDlgEditLine LlDlgEditLineW
     #else
       #define LlDlgEditLineO LlDlgEditLineW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDLGEDITLINEW)(
	HLLJOB               hLlJob,
	HWND                 hWnd,
	LPWSTR               lpBuf,
	INT                  nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDLGEDITLINEW LlDlgEditLineW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDlgEditLineExO LlDlgEditLineExA
     #else
       #define LlDlgEditLineEx LlDlgEditLineExA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDLGEDITLINEEXA)(
	HLLJOB               hLlJob,
	HWND                 hWnd,
	LPSTR                pszBuffer,
	UINT                 nBufSize,
	UINT32               nParaTypes,
	LPCSTR               pszTitle,
	BOOL                 bTable,
	LPVOID               pvReserved);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDLGEDITLINEEXA LlDlgEditLineExA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDlgEditLineEx LlDlgEditLineExW
     #else
       #define LlDlgEditLineExO LlDlgEditLineExW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDLGEDITLINEEXW)(
	HLLJOB               hLlJob,
	HWND                 hWnd,
	LPWSTR               pszBuffer,
	UINT                 nBufSize,
	UINT32               nParaTypes,
	LPCWSTR              pszTitle,
	BOOL                 bTable,
	LPVOID               pvReserved);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDLGEDITLINEEXW LlDlgEditLineExW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPreviewSetTempPathO LlPreviewSetTempPathA
     #else
       #define LlPreviewSetTempPath LlPreviewSetTempPathA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPREVIEWSETTEMPPATHA)(
	HLLJOB               hLlJob,
	LPCSTR               pszPath);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPREVIEWSETTEMPPATHA LlPreviewSetTempPathA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPreviewSetTempPath LlPreviewSetTempPathW
     #else
       #define LlPreviewSetTempPathO LlPreviewSetTempPathW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPREVIEWSETTEMPPATHW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszPath);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPREVIEWSETTEMPPATHW LlPreviewSetTempPathW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPreviewDeleteFilesO LlPreviewDeleteFilesA
     #else
       #define LlPreviewDeleteFiles LlPreviewDeleteFilesA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPREVIEWDELETEFILESA)(
	HLLJOB               hLlJob,
	LPCSTR               pszObjName,
	LPCSTR               pszPath);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPREVIEWDELETEFILESA LlPreviewDeleteFilesA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPreviewDeleteFiles LlPreviewDeleteFilesW
     #else
       #define LlPreviewDeleteFilesO LlPreviewDeleteFilesW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPREVIEWDELETEFILESW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszObjName,
	LPCWSTR              pszPath);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPREVIEWDELETEFILESW LlPreviewDeleteFilesW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPreviewDisplayO LlPreviewDisplayA
     #else
       #define LlPreviewDisplay LlPreviewDisplayA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPREVIEWDISPLAYA)(
	HLLJOB               hLlJob,
	LPCSTR               pszObjName,
	LPCSTR               pszPath,
	HWND                 Wnd);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPREVIEWDISPLAYA LlPreviewDisplayA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPreviewDisplay LlPreviewDisplayW
     #else
       #define LlPreviewDisplayO LlPreviewDisplayW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPREVIEWDISPLAYW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszObjName,
	LPCWSTR              pszPath,
	HWND                 Wnd);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPREVIEWDISPLAYW LlPreviewDisplayW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPreviewDisplayExO LlPreviewDisplayExA
     #else
       #define LlPreviewDisplayEx LlPreviewDisplayExA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPREVIEWDISPLAYEXA)(
	HLLJOB               hLlJob,
	LPCSTR               pszObjName,
	LPCSTR               pszPath,
	HWND                 Wnd,
	UINT32               nOptions,
	LPVOID               pOptions);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPREVIEWDISPLAYEXA LlPreviewDisplayExA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPreviewDisplayEx LlPreviewDisplayExW
     #else
       #define LlPreviewDisplayExO LlPreviewDisplayExW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPREVIEWDISPLAYEXW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszObjName,
	LPCWSTR              pszPath,
	HWND                 Wnd,
	UINT32               nOptions,
	LPVOID               pOptions);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPREVIEWDISPLAYEXW LlPreviewDisplayExW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLPRINT)(
	HLLJOB               hLlJob);
#endif /* IMPLEMENTATION */

extern PFNLLPRINT LlPrint;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLPRINTABORT)(
	HLLJOB               hLlJob);
#endif /* IMPLEMENTATION */

extern PFNLLPRINTABORT LlPrintAbort;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef     BOOL ( DLLPROC *PFNLLPRINTCHECKLINEFIT)(
	HLLJOB               hLlJob);
#endif /* IMPLEMENTATION */

extern PFNLLPRINTCHECKLINEFIT LlPrintCheckLineFit;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLPRINTEND)(
	HLLJOB               hLlJob,
	INT                  nPages);
#endif /* IMPLEMENTATION */

extern PFNLLPRINTEND LlPrintEnd;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLPRINTFIELDS)(
	HLLJOB               hLlJob);
#endif /* IMPLEMENTATION */

extern PFNLLPRINTFIELDS LlPrintFields;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLPRINTFIELDSEND)(
	HLLJOB               hLlJob);
#endif /* IMPLEMENTATION */

extern PFNLLPRINTFIELDSEND LlPrintFieldsEnd;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLPRINTGETCURRENTPAGE)(
	HLLJOB               hLlJob);
#endif /* IMPLEMENTATION */

extern PFNLLPRINTGETCURRENTPAGE LlPrintGetCurrentPage;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLPRINTGETITEMSPERPAGE)(
	HLLJOB               hLlJob);
#endif /* IMPLEMENTATION */

extern PFNLLPRINTGETITEMSPERPAGE LlPrintGetItemsPerPage;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLPRINTGETITEMSPERTABLE)(
	HLLJOB               hLlJob);
#endif /* IMPLEMENTATION */

extern PFNLLPRINTGETITEMSPERTABLE LlPrintGetItemsPerTable;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintGetRemainingItemsPerTableO LlPrintGetRemainingItemsPerTableA
     #else
       #define LlPrintGetRemainingItemsPerTable LlPrintGetRemainingItemsPerTableA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTGETREMAININGITEMSPERTABLEA)(
	HLLJOB               hLlJob,
	LPCSTR               pszField);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTGETREMAININGITEMSPERTABLEA LlPrintGetRemainingItemsPerTableA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintGetRemainingItemsPerTable LlPrintGetRemainingItemsPerTableW
     #else
       #define LlPrintGetRemainingItemsPerTableO LlPrintGetRemainingItemsPerTableW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTGETREMAININGITEMSPERTABLEW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszField);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTGETREMAININGITEMSPERTABLEW LlPrintGetRemainingItemsPerTableW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintGetRemItemsPerTableO LlPrintGetRemItemsPerTableA
     #else
       #define LlPrintGetRemItemsPerTable LlPrintGetRemItemsPerTableA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTGETREMITEMSPERTABLEA)(
	HLLJOB               hLlJob,
	LPCSTR               pszField);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTGETREMITEMSPERTABLEA LlPrintGetRemItemsPerTableA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintGetRemItemsPerTable LlPrintGetRemItemsPerTableW
     #else
       #define LlPrintGetRemItemsPerTableO LlPrintGetRemItemsPerTableW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTGETREMITEMSPERTABLEW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszField);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTGETREMITEMSPERTABLEW LlPrintGetRemItemsPerTableW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLPRINTGETOPTION)(
	HLLJOB               hLlJob,
	INT                  nIndex);
#endif /* IMPLEMENTATION */

extern PFNLLPRINTGETOPTION LlPrintGetOption;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintGetPrinterInfoO LlPrintGetPrinterInfoA
     #else
       #define LlPrintGetPrinterInfo LlPrintGetPrinterInfoA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTGETPRINTERINFOA)(
	HLLJOB               hLlJob,
	LPSTR                pszPrn,
	UINT                 nPrnLen,
	LPSTR                pszPort,
	UINT                 nPortLen);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTGETPRINTERINFOA LlPrintGetPrinterInfoA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintGetPrinterInfo LlPrintGetPrinterInfoW
     #else
       #define LlPrintGetPrinterInfoO LlPrintGetPrinterInfoW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTGETPRINTERINFOW)(
	HLLJOB               hLlJob,
	LPWSTR               pszPrn,
	UINT                 nPrnLen,
	LPWSTR               pszPort,
	UINT                 nPortLen);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTGETPRINTERINFOW LlPrintGetPrinterInfoW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintOptionsDialogO LlPrintOptionsDialogA
     #else
       #define LlPrintOptionsDialog LlPrintOptionsDialogA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTOPTIONSDIALOGA)(
	HLLJOB               hLlJob,
	HWND                 hWnd,
	LPCSTR               pszText);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTOPTIONSDIALOGA LlPrintOptionsDialogA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintOptionsDialog LlPrintOptionsDialogW
     #else
       #define LlPrintOptionsDialogO LlPrintOptionsDialogW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTOPTIONSDIALOGW)(
	HLLJOB               hLlJob,
	HWND                 hWnd,
	LPCWSTR              pszText);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTOPTIONSDIALOGW LlPrintOptionsDialogW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLPRINTSELECTOFFSETEX)(
	HLLJOB               hLlJob,
	HWND                 hWnd);
#endif /* IMPLEMENTATION */

extern PFNLLPRINTSELECTOFFSETEX LlPrintSelectOffsetEx;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintSetBoxTextO LlPrintSetBoxTextA
     #else
       #define LlPrintSetBoxText LlPrintSetBoxTextA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTSETBOXTEXTA)(
	HLLJOB               hLlJob,
	LPCSTR               szText,
	INT                  nPercentage);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTSETBOXTEXTA LlPrintSetBoxTextA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintSetBoxText LlPrintSetBoxTextW
     #else
       #define LlPrintSetBoxTextO LlPrintSetBoxTextW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTSETBOXTEXTW)(
	HLLJOB               hLlJob,
	LPCWSTR              szText,
	INT                  nPercentage);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTSETBOXTEXTW LlPrintSetBoxTextW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLPRINTSETOPTION)(
	HLLJOB               hLlJob,
	INT                  nIndex,
	INT                  nValue);
#endif /* IMPLEMENTATION */

extern PFNLLPRINTSETOPTION LlPrintSetOption;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLPRINTUPDATEBOX)(
	HLLJOB               hLlJob);
#endif /* IMPLEMENTATION */

extern PFNLLPRINTUPDATEBOX LlPrintUpdateBox;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintStartO LlPrintStartA
     #else
       #define LlPrintStart LlPrintStartA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTSTARTA)(
	HLLJOB               hLlJob,
	UINT                 nObjType,
	LPCSTR               pszObjName,
	INT                  nPrintOptions,
	INT                  dummy);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTSTARTA LlPrintStartA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintStart LlPrintStartW
     #else
       #define LlPrintStartO LlPrintStartW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTSTARTW)(
	HLLJOB               hLlJob,
	UINT                 nObjType,
	LPCWSTR              pszObjName,
	INT                  nPrintOptions,
	INT                  dummy);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTSTARTW LlPrintStartW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintWithBoxStartO LlPrintWithBoxStartA
     #else
       #define LlPrintWithBoxStart LlPrintWithBoxStartA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTWITHBOXSTARTA)(
	HLLJOB               hLlJob,
	UINT                 nObjType,
	LPCSTR               pszObjName,
	INT                  nPrintOptions,
	INT                  nBoxType,
	HWND                 hWnd,
	LPCSTR               pszTitle);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTWITHBOXSTARTA LlPrintWithBoxStartA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintWithBoxStart LlPrintWithBoxStartW
     #else
       #define LlPrintWithBoxStartO LlPrintWithBoxStartW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTWITHBOXSTARTW)(
	HLLJOB               hLlJob,
	UINT                 nObjType,
	LPCWSTR              pszObjName,
	INT                  nPrintOptions,
	INT                  nBoxType,
	HWND                 hWnd,
	LPCWSTR              pszTitle);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTWITHBOXSTARTW LlPrintWithBoxStartW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrinterSetupO LlPrinterSetupA
     #else
       #define LlPrinterSetup LlPrinterSetupA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTERSETUPA)(
	HLLJOB               hLlJob,
	HWND                 hWnd,
	UINT                 nObjType,
	LPCSTR               pszObjName);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTERSETUPA LlPrinterSetupA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrinterSetup LlPrinterSetupW
     #else
       #define LlPrinterSetupO LlPrinterSetupW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTERSETUPW)(
	HLLJOB               hLlJob,
	HWND                 hWnd,
	UINT                 nObjType,
	LPCWSTR              pszObjName);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTERSETUPW LlPrinterSetupW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlSelectFileDlgTitleExO LlSelectFileDlgTitleExA
     #else
       #define LlSelectFileDlgTitleEx LlSelectFileDlgTitleExA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLSELECTFILEDLGTITLEEXA)(
	HLLJOB               hLlJob,
	HWND                 hWnd,
	LPCSTR               pszTitle,
	UINT                 nObjType,
	LPSTR                pszObjName,
	UINT                 nBufSize,
	LPVOID               pReserved);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLSELECTFILEDLGTITLEEXA LlSelectFileDlgTitleExA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlSelectFileDlgTitleEx LlSelectFileDlgTitleExW
     #else
       #define LlSelectFileDlgTitleExO LlSelectFileDlgTitleExW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLSELECTFILEDLGTITLEEXW)(
	HLLJOB               hLlJob,
	HWND                 hWnd,
	LPCWSTR              pszTitle,
	UINT                 nObjType,
	LPWSTR               pszObjName,
	UINT                 nBufSize,
	LPVOID               pReserved);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLSELECTFILEDLGTITLEEXW LlSelectFileDlgTitleExW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef     void ( DLLPROC *PFNLLSETDLGBOXMODE)(
	UINT                 nMode);
#endif /* IMPLEMENTATION */

extern PFNLLSETDLGBOXMODE LlSetDlgboxMode;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef     UINT ( DLLPROC *PFNLLGETDLGBOXMODE)(
	void);
#endif /* IMPLEMENTATION */

extern PFNLLGETDLGBOXMODE LlGetDlgboxMode;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlExprParseO LlExprParseA
     #else
       #define LlExprParse LlExprParseA
   #endif
#endif
  #ifdef WIN32
  typedef  HLLEXPR ( DLLPROC *PFNLLEXPRPARSEA)(
	HLLJOB               hLlJob,
	LPCSTR               lpExprText,
	BOOL                 bIncludeFields);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLEXPRPARSEA LlExprParseA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlExprParse LlExprParseW
     #else
       #define LlExprParseO LlExprParseW
   #endif
#endif
  #ifdef WIN32
  typedef  HLLEXPR ( DLLPROC *PFNLLEXPRPARSEW)(
	HLLJOB               hLlJob,
	LPCWSTR              lpExprText,
	BOOL                 bIncludeFields);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLEXPRPARSEW LlExprParseW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLEXPRTYPE)(
	HLLJOB               hLlJob,
	HLLEXPR              lpExpr);
#endif /* IMPLEMENTATION */

extern PFNLLEXPRTYPE LlExprType;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlExprErrorO LlExprErrorA
     #else
       #define LlExprError LlExprErrorA
   #endif
#endif
  #ifdef WIN32
  typedef     void ( DLLPROC *PFNLLEXPRERRORA)(
	HLLJOB               hLlJob,
	LPSTR                pszBuf,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLEXPRERRORA LlExprErrorA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlExprError LlExprErrorW
     #else
       #define LlExprErrorO LlExprErrorW
   #endif
#endif
  #ifdef WIN32
  typedef     void ( DLLPROC *PFNLLEXPRERRORW)(
	HLLJOB               hLlJob,
	LPWSTR               pszBuf,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLEXPRERRORW LlExprErrorW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef     void ( DLLPROC *PFNLLEXPRFREE)(
	HLLJOB               hLlJob,
	HLLEXPR              lpExpr);
#endif /* IMPLEMENTATION */

extern PFNLLEXPRFREE LlExprFree;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlExprEvaluateO LlExprEvaluateA
     #else
       #define LlExprEvaluate LlExprEvaluateA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLEXPREVALUATEA)(
	HLLJOB               hLlJob,
	HLLEXPR              lpExpr,
	LPSTR                pszBuf,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLEXPREVALUATEA LlExprEvaluateA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlExprEvaluate LlExprEvaluateW
     #else
       #define LlExprEvaluateO LlExprEvaluateW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLEXPREVALUATEW)(
	HLLJOB               hLlJob,
	HLLEXPR              lpExpr,
	LPWSTR               pszBuf,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLEXPREVALUATEW LlExprEvaluateW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlExprGetUsedVarsO LlExprGetUsedVarsA
     #else
       #define LlExprGetUsedVars LlExprGetUsedVarsA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLEXPRGETUSEDVARSA)(
	HLLJOB               hLlJob,
	HLLEXPR              lpExpr,
	LPSTR                pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLEXPRGETUSEDVARSA LlExprGetUsedVarsA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlExprGetUsedVars LlExprGetUsedVarsW
     #else
       #define LlExprGetUsedVarsO LlExprGetUsedVarsW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLEXPRGETUSEDVARSW)(
	HLLJOB               hLlJob,
	HLLEXPR              lpExpr,
	LPWSTR               pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLEXPRGETUSEDVARSW LlExprGetUsedVarsW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLSETOPTION)(
	HLLJOB               hLlJob,
	INT                  nMode,
	UINT32               nValue);
#endif /* IMPLEMENTATION */

extern PFNLLSETOPTION LlSetOption;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef   UINT32 ( DLLPROC *PFNLLGETOPTION)(
	HLLJOB               hLlJob,
	INT                  nMode);
#endif /* IMPLEMENTATION */

extern PFNLLGETOPTION LlGetOption;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlSetOptionStringO LlSetOptionStringA
     #else
       #define LlSetOptionString LlSetOptionStringA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLSETOPTIONSTRINGA)(
	HLLJOB               hLlJob,
	INT                  nIndex,
	LPCSTR               pszBuffer);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLSETOPTIONSTRINGA LlSetOptionStringA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlSetOptionString LlSetOptionStringW
     #else
       #define LlSetOptionStringO LlSetOptionStringW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLSETOPTIONSTRINGW)(
	HLLJOB               hLlJob,
	INT                  nIndex,
	LPCWSTR              pszBuffer);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLSETOPTIONSTRINGW LlSetOptionStringW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlGetOptionStringO LlGetOptionStringA
     #else
       #define LlGetOptionString LlGetOptionStringA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLGETOPTIONSTRINGA)(
	HLLJOB               hLlJob,
	INT                  nIndex,
	LPSTR                pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLGETOPTIONSTRINGA LlGetOptionStringA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlGetOptionString LlGetOptionStringW
     #else
       #define LlGetOptionStringO LlGetOptionStringW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLGETOPTIONSTRINGW)(
	HLLJOB               hLlJob,
	INT                  nIndex,
	LPWSTR               pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLGETOPTIONSTRINGW LlGetOptionStringW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintSetOptionStringO LlPrintSetOptionStringA
     #else
       #define LlPrintSetOptionString LlPrintSetOptionStringA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTSETOPTIONSTRINGA)(
	HLLJOB               hLlJob,
	INT                  nIndex,
	LPCSTR               pszBuffer);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTSETOPTIONSTRINGA LlPrintSetOptionStringA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintSetOptionString LlPrintSetOptionStringW
     #else
       #define LlPrintSetOptionStringO LlPrintSetOptionStringW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTSETOPTIONSTRINGW)(
	HLLJOB               hLlJob,
	INT                  nIndex,
	LPCWSTR              pszBuffer);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTSETOPTIONSTRINGW LlPrintSetOptionStringW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintGetOptionStringO LlPrintGetOptionStringA
     #else
       #define LlPrintGetOptionString LlPrintGetOptionStringA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTGETOPTIONSTRINGA)(
	HLLJOB               hLlJob,
	INT                  nIndex,
	LPSTR                pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTGETOPTIONSTRINGA LlPrintGetOptionStringA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintGetOptionString LlPrintGetOptionStringW
     #else
       #define LlPrintGetOptionStringO LlPrintGetOptionStringW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTGETOPTIONSTRINGW)(
	HLLJOB               hLlJob,
	INT                  nIndex,
	LPWSTR               pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTGETOPTIONSTRINGW LlPrintGetOptionStringW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLDESIGNERPROHIBITACTION)(
	HLLJOB               hLlJob,
	INT                  nMenuID);
#endif /* IMPLEMENTATION */

extern PFNLLDESIGNERPROHIBITACTION LlDesignerProhibitAction;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDesignerProhibitFunctionO LlDesignerProhibitFunctionA
     #else
       #define LlDesignerProhibitFunction LlDesignerProhibitFunctionA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDESIGNERPROHIBITFUNCTIONA)(
	HLLJOB               hLlJob,
	LPCSTR               pszFunction);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDESIGNERPROHIBITFUNCTIONA LlDesignerProhibitFunctionA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDesignerProhibitFunction LlDesignerProhibitFunctionW
     #else
       #define LlDesignerProhibitFunctionO LlDesignerProhibitFunctionW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDESIGNERPROHIBITFUNCTIONW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszFunction);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDESIGNERPROHIBITFUNCTIONW LlDesignerProhibitFunctionW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintEnableObjectO LlPrintEnableObjectA
     #else
       #define LlPrintEnableObject LlPrintEnableObjectA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTENABLEOBJECTA)(
	HLLJOB               hLlJob,
	LPCSTR               pszObjectName,
	BOOL                 bEnable);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTENABLEOBJECTA LlPrintEnableObjectA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintEnableObject LlPrintEnableObjectW
     #else
       #define LlPrintEnableObjectO LlPrintEnableObjectW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTENABLEOBJECTW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszObjectName,
	BOOL                 bEnable);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTENABLEOBJECTW LlPrintEnableObjectW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlSetFileExtensionsO LlSetFileExtensionsA
     #else
       #define LlSetFileExtensions LlSetFileExtensionsA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLSETFILEEXTENSIONSA)(
	HLLJOB               hLlJob,
	INT                  nObjType,
	LPCSTR               pszObjectExt,
	LPCSTR               pszPrinterExt,
	LPCSTR               pszSketchExt);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLSETFILEEXTENSIONSA LlSetFileExtensionsA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlSetFileExtensions LlSetFileExtensionsW
     #else
       #define LlSetFileExtensionsO LlSetFileExtensionsW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLSETFILEEXTENSIONSW)(
	HLLJOB               hLlJob,
	INT                  nObjType,
	LPCWSTR              pszObjectExt,
	LPCWSTR              pszPrinterExt,
	LPCWSTR              pszSketchExt);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLSETFILEEXTENSIONSW LlSetFileExtensionsW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintGetTextCharsPrintedO LlPrintGetTextCharsPrintedA
     #else
       #define LlPrintGetTextCharsPrinted LlPrintGetTextCharsPrintedA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTGETTEXTCHARSPRINTEDA)(
	HLLJOB               hLlJob,
	LPCSTR               pszObjectName);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTGETTEXTCHARSPRINTEDA LlPrintGetTextCharsPrintedA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintGetTextCharsPrinted LlPrintGetTextCharsPrintedW
     #else
       #define LlPrintGetTextCharsPrintedO LlPrintGetTextCharsPrintedW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTGETTEXTCHARSPRINTEDW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszObjectName);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTGETTEXTCHARSPRINTEDW LlPrintGetTextCharsPrintedW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintGetFieldCharsPrintedO LlPrintGetFieldCharsPrintedA
     #else
       #define LlPrintGetFieldCharsPrinted LlPrintGetFieldCharsPrintedA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTGETFIELDCHARSPRINTEDA)(
	HLLJOB               hLlJob,
	LPCSTR               pszObjectName,
	LPCSTR               pszField);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTGETFIELDCHARSPRINTEDA LlPrintGetFieldCharsPrintedA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintGetFieldCharsPrinted LlPrintGetFieldCharsPrintedW
     #else
       #define LlPrintGetFieldCharsPrintedO LlPrintGetFieldCharsPrintedW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTGETFIELDCHARSPRINTEDW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszObjectName,
	LPCWSTR              pszField);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTGETFIELDCHARSPRINTEDW LlPrintGetFieldCharsPrintedW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintIsVariableUsedO LlPrintIsVariableUsedA
     #else
       #define LlPrintIsVariableUsed LlPrintIsVariableUsedA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTISVARIABLEUSEDA)(
	HLLJOB               hLlJob,
	LPCSTR               pszVarName);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTISVARIABLEUSEDA LlPrintIsVariableUsedA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintIsVariableUsed LlPrintIsVariableUsedW
     #else
       #define LlPrintIsVariableUsedO LlPrintIsVariableUsedW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTISVARIABLEUSEDW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszVarName);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTISVARIABLEUSEDW LlPrintIsVariableUsedW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintIsFieldUsedO LlPrintIsFieldUsedA
     #else
       #define LlPrintIsFieldUsed LlPrintIsFieldUsedA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTISFIELDUSEDA)(
	HLLJOB               hLlJob,
	LPCSTR               pszFieldName);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTISFIELDUSEDA LlPrintIsFieldUsedA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintIsFieldUsed LlPrintIsFieldUsedW
     #else
       #define LlPrintIsFieldUsedO LlPrintIsFieldUsedW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTISFIELDUSEDW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszFieldName);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTISFIELDUSEDW LlPrintIsFieldUsedW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintOptionsDialogTitleO LlPrintOptionsDialogTitleA
     #else
       #define LlPrintOptionsDialogTitle LlPrintOptionsDialogTitleA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTOPTIONSDIALOGTITLEA)(
	HLLJOB               hLlJob,
	HWND                 hWnd,
	LPCSTR               pszTitle,
	LPCSTR               pszText);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTOPTIONSDIALOGTITLEA LlPrintOptionsDialogTitleA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintOptionsDialogTitle LlPrintOptionsDialogTitleW
     #else
       #define LlPrintOptionsDialogTitleO LlPrintOptionsDialogTitleW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTOPTIONSDIALOGTITLEW)(
	HLLJOB               hLlJob,
	HWND                 hWnd,
	LPCWSTR              pszTitle,
	LPCWSTR              pszText);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTOPTIONSDIALOGTITLEW LlPrintOptionsDialogTitleW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlSetPrinterToDefaultO LlSetPrinterToDefaultA
     #else
       #define LlSetPrinterToDefault LlSetPrinterToDefaultA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLSETPRINTERTODEFAULTA)(
	HLLJOB               hLlJob,
	INT                  nObjType,
	LPCSTR               pszObjName);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLSETPRINTERTODEFAULTA LlSetPrinterToDefaultA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlSetPrinterToDefault LlSetPrinterToDefaultW
     #else
       #define LlSetPrinterToDefaultO LlSetPrinterToDefaultW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLSETPRINTERTODEFAULTW)(
	HLLJOB               hLlJob,
	INT                  nObjType,
	LPCWSTR              pszObjName);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLSETPRINTERTODEFAULTW LlSetPrinterToDefaultW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLDEFINESORTORDERSTART)(
	HLLJOB               hLlJob);
#endif /* IMPLEMENTATION */

extern PFNLLDEFINESORTORDERSTART LlDefineSortOrderStart;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDefineSortOrderO LlDefineSortOrderA
     #else
       #define LlDefineSortOrder LlDefineSortOrderA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDEFINESORTORDERA)(
	HLLJOB               hLlJob,
	LPCSTR               pszIdentifier,
	LPCSTR               pszText);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDEFINESORTORDERA LlDefineSortOrderA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDefineSortOrder LlDefineSortOrderW
     #else
       #define LlDefineSortOrderO LlDefineSortOrderW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDEFINESORTORDERW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszIdentifier,
	LPCWSTR              pszText);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDEFINESORTORDERW LlDefineSortOrderW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintGetSortOrderO LlPrintGetSortOrderA
     #else
       #define LlPrintGetSortOrder LlPrintGetSortOrderA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTGETSORTORDERA)(
	HLLJOB               hLlJob,
	LPSTR                pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTGETSORTORDERA LlPrintGetSortOrderA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintGetSortOrder LlPrintGetSortOrderW
     #else
       #define LlPrintGetSortOrderO LlPrintGetSortOrderW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTGETSORTORDERW)(
	HLLJOB               hLlJob,
	LPWSTR               pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTGETSORTORDERW LlPrintGetSortOrderW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDefineGroupingO LlDefineGroupingA
     #else
       #define LlDefineGrouping LlDefineGroupingA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDEFINEGROUPINGA)(
	HLLJOB               hLlJob,
	LPCSTR               pszSortorder,
	LPCSTR               pszIdentifier,
	LPCSTR               pszText);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDEFINEGROUPINGA LlDefineGroupingA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDefineGrouping LlDefineGroupingW
     #else
       #define LlDefineGroupingO LlDefineGroupingW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDEFINEGROUPINGW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszSortorder,
	LPCWSTR              pszIdentifier,
	LPCWSTR              pszText);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDEFINEGROUPINGW LlDefineGroupingW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintGetGroupingO LlPrintGetGroupingA
     #else
       #define LlPrintGetGrouping LlPrintGetGroupingA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTGETGROUPINGA)(
	HLLJOB               hLlJob,
	LPSTR                pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTGETGROUPINGA LlPrintGetGroupingA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintGetGrouping LlPrintGetGroupingW
     #else
       #define LlPrintGetGroupingO LlPrintGetGroupingW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTGETGROUPINGW)(
	HLLJOB               hLlJob,
	LPWSTR               pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTGETGROUPINGW LlPrintGetGroupingW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlAddCtlSupportO LlAddCtlSupportA
     #else
       #define LlAddCtlSupport LlAddCtlSupportA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLADDCTLSUPPORTA)(
	HWND                 hWnd,
	UINT32               nFlags,
	LPCSTR               pszInifile);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLADDCTLSUPPORTA LlAddCtlSupportA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlAddCtlSupport LlAddCtlSupportW
     #else
       #define LlAddCtlSupportO LlAddCtlSupportW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLADDCTLSUPPORTW)(
	HWND                 hWnd,
	UINT32               nFlags,
	LPCWSTR              pszInifile);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLADDCTLSUPPORTW LlAddCtlSupportW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLPRINTBEGINGROUP)(
	HLLJOB               hLlJob,
	LPARAM               lParam,
	LPVOID               lpParam);
#endif /* IMPLEMENTATION */

extern PFNLLPRINTBEGINGROUP LlPrintBeginGroup;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLPRINTENDGROUP)(
	HLLJOB               hLlJob,
	LPARAM               lParam,
	LPVOID               lpParam);
#endif /* IMPLEMENTATION */

extern PFNLLPRINTENDGROUP LlPrintEndGroup;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLPRINTGROUPLINE)(
	HLLJOB               hLlJob,
	LPARAM               lParam,
	LPVOID               lpParam);
#endif /* IMPLEMENTATION */

extern PFNLLPRINTGROUPLINE LlPrintGroupLine;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLPRINTGROUPHEADER)(
	HLLJOB               hLlJob,
	LPARAM               lParam);
#endif /* IMPLEMENTATION */

extern PFNLLPRINTGROUPHEADER LlPrintGroupHeader;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintGetFilterExpressionO LlPrintGetFilterExpressionA
     #else
       #define LlPrintGetFilterExpression LlPrintGetFilterExpressionA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTGETFILTEREXPRESSIONA)(
	HLLJOB               hLlJob,
	LPSTR                pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTGETFILTEREXPRESSIONA LlPrintGetFilterExpressionA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintGetFilterExpression LlPrintGetFilterExpressionW
     #else
       #define LlPrintGetFilterExpressionO LlPrintGetFilterExpressionW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTGETFILTEREXPRESSIONW)(
	HLLJOB               hLlJob,
	LPWSTR               pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTGETFILTEREXPRESSIONW LlPrintGetFilterExpressionW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLPRINTWILLMATCHFILTER)(
	HLLJOB               hLlJob);
#endif /* IMPLEMENTATION */

extern PFNLLPRINTWILLMATCHFILTER LlPrintWillMatchFilter;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLPRINTDIDMATCHFILTER)(
	HLLJOB               hLlJob);
#endif /* IMPLEMENTATION */

extern PFNLLPRINTDIDMATCHFILTER LlPrintDidMatchFilter;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlGetFieldContentsO LlGetFieldContentsA
     #else
       #define LlGetFieldContents LlGetFieldContentsA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLGETFIELDCONTENTSA)(
	HLLJOB               hLlJob,
	LPCSTR               pszName,
	LPSTR                pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLGETFIELDCONTENTSA LlGetFieldContentsA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlGetFieldContents LlGetFieldContentsW
     #else
       #define LlGetFieldContentsO LlGetFieldContentsW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLGETFIELDCONTENTSW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszName,
	LPWSTR               pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLGETFIELDCONTENTSW LlGetFieldContentsW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlGetVariableContentsO LlGetVariableContentsA
     #else
       #define LlGetVariableContents LlGetVariableContentsA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLGETVARIABLECONTENTSA)(
	HLLJOB               hLlJob,
	LPCSTR               pszName,
	LPSTR                pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLGETVARIABLECONTENTSA LlGetVariableContentsA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlGetVariableContents LlGetVariableContentsW
     #else
       #define LlGetVariableContentsO LlGetVariableContentsW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLGETVARIABLECONTENTSW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszName,
	LPWSTR               pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLGETVARIABLECONTENTSW LlGetVariableContentsW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlGetSumVariableContentsO LlGetSumVariableContentsA
     #else
       #define LlGetSumVariableContents LlGetSumVariableContentsA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLGETSUMVARIABLECONTENTSA)(
	HLLJOB               hLlJob,
	LPCSTR               pszName,
	LPSTR                pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLGETSUMVARIABLECONTENTSA LlGetSumVariableContentsA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlGetSumVariableContents LlGetSumVariableContentsW
     #else
       #define LlGetSumVariableContentsO LlGetSumVariableContentsW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLGETSUMVARIABLECONTENTSW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszName,
	LPWSTR               pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLGETSUMVARIABLECONTENTSW LlGetSumVariableContentsW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlGetUserVariableContentsO LlGetUserVariableContentsA
     #else
       #define LlGetUserVariableContents LlGetUserVariableContentsA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLGETUSERVARIABLECONTENTSA)(
	HLLJOB               hLlJob,
	LPCSTR               pszName,
	LPSTR                pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLGETUSERVARIABLECONTENTSA LlGetUserVariableContentsA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlGetUserVariableContents LlGetUserVariableContentsW
     #else
       #define LlGetUserVariableContentsO LlGetUserVariableContentsW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLGETUSERVARIABLECONTENTSW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszName,
	LPWSTR               pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLGETUSERVARIABLECONTENTSW LlGetUserVariableContentsW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlGetVariableTypeO LlGetVariableTypeA
     #else
       #define LlGetVariableType LlGetVariableTypeA
   #endif
#endif
  #ifdef WIN32
  typedef    INT32 ( DLLPROC *PFNLLGETVARIABLETYPEA)(
	HLLJOB               hLlJob,
	LPCSTR               pszName);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLGETVARIABLETYPEA LlGetVariableTypeA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlGetVariableType LlGetVariableTypeW
     #else
       #define LlGetVariableTypeO LlGetVariableTypeW
   #endif
#endif
  #ifdef WIN32
  typedef    INT32 ( DLLPROC *PFNLLGETVARIABLETYPEW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszName);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLGETVARIABLETYPEW LlGetVariableTypeW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlGetFieldTypeO LlGetFieldTypeA
     #else
       #define LlGetFieldType LlGetFieldTypeA
   #endif
#endif
  #ifdef WIN32
  typedef    INT32 ( DLLPROC *PFNLLGETFIELDTYPEA)(
	HLLJOB               hLlJob,
	LPCSTR               pszName);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLGETFIELDTYPEA LlGetFieldTypeA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlGetFieldType LlGetFieldTypeW
     #else
       #define LlGetFieldTypeO LlGetFieldTypeW
   #endif
#endif
  #ifdef WIN32
  typedef    INT32 ( DLLPROC *PFNLLGETFIELDTYPEW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszName);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLGETFIELDTYPEW LlGetFieldTypeW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintGetColumnInfoO LlPrintGetColumnInfoA
     #else
       #define LlPrintGetColumnInfo LlPrintGetColumnInfoA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTGETCOLUMNINFOA)(
	HLLJOB               hLlJob,
	LPCSTR               pszObjectName,
	INT                  nCol,
	PSCLLCOLUMN          pCol);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTGETCOLUMNINFOA LlPrintGetColumnInfoA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintGetColumnInfo LlPrintGetColumnInfoW
     #else
       #define LlPrintGetColumnInfoO LlPrintGetColumnInfoW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTGETCOLUMNINFOW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszObjectName,
	INT                  nCol,
	PSCLLCOLUMN          pCol);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTGETCOLUMNINFOW LlPrintGetColumnInfoW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlSetPrinterDefaultsDirO LlSetPrinterDefaultsDirA
     #else
       #define LlSetPrinterDefaultsDir LlSetPrinterDefaultsDirA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLSETPRINTERDEFAULTSDIRA)(
	HLLJOB               hLlJob,
	LPCSTR               pszDir);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLSETPRINTERDEFAULTSDIRA LlSetPrinterDefaultsDirA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlSetPrinterDefaultsDir LlSetPrinterDefaultsDirW
     #else
       #define LlSetPrinterDefaultsDirO LlSetPrinterDefaultsDirW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLSETPRINTERDEFAULTSDIRW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszDir);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLSETPRINTERDEFAULTSDIRW LlSetPrinterDefaultsDirW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlCreateSketchO LlCreateSketchA
     #else
       #define LlCreateSketch LlCreateSketchA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLCREATESKETCHA)(
	HLLJOB               hLlJob,
	UINT                 nObjType,
	LPCSTR               lpszObjName);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLCREATESKETCHA LlCreateSketchA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlCreateSketch LlCreateSketchW
     #else
       #define LlCreateSketchO LlCreateSketchW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLCREATESKETCHW)(
	HLLJOB               hLlJob,
	UINT                 nObjType,
	LPCWSTR              lpszObjName);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLCREATESKETCHW LlCreateSketchW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLVIEWERPROHIBITACTION)(
	HLLJOB               hLlJob,
	INT                  nMenuID);
#endif /* IMPLEMENTATION */

extern PFNLLVIEWERPROHIBITACTION LlViewerProhibitAction;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintCopyPrinterConfigurationO LlPrintCopyPrinterConfigurationA
     #else
       #define LlPrintCopyPrinterConfiguration LlPrintCopyPrinterConfigurationA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTCOPYPRINTERCONFIGURATIONA)(
	HLLJOB               hLlJob,
	LPCSTR               lpszFilename,
	INT                  nFunction);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTCOPYPRINTERCONFIGURATIONA LlPrintCopyPrinterConfigurationA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintCopyPrinterConfiguration LlPrintCopyPrinterConfigurationW
     #else
       #define LlPrintCopyPrinterConfigurationO LlPrintCopyPrinterConfigurationW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTCOPYPRINTERCONFIGURATIONW)(
	HLLJOB               hLlJob,
	LPCWSTR              lpszFilename,
	INT                  nFunction);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTCOPYPRINTERCONFIGURATIONW LlPrintCopyPrinterConfigurationW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlSetPrinterInPrinterFileO LlSetPrinterInPrinterFileA
     #else
       #define LlSetPrinterInPrinterFile LlSetPrinterInPrinterFileA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLSETPRINTERINPRINTERFILEA)(
	HLLJOB               hLlJob,
	UINT                 nObjType,
	LPCSTR               pszObjName,
	INT                  nPrinter,
	LPCSTR               pszPrinter,
	_PCDEVMODEA          pDevMode);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLSETPRINTERINPRINTERFILEA LlSetPrinterInPrinterFileA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlSetPrinterInPrinterFile LlSetPrinterInPrinterFileW
     #else
       #define LlSetPrinterInPrinterFileO LlSetPrinterInPrinterFileW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLSETPRINTERINPRINTERFILEW)(
	HLLJOB               hLlJob,
	UINT                 nObjType,
	LPCWSTR              pszObjName,
	INT                  nPrinter,
	LPCWSTR              pszPrinter,
	_PCDEVMODEW          pDevMode);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLSETPRINTERINPRINTERFILEW LlSetPrinterInPrinterFileW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef HLLRTFOBJ ( DLLPROC *PFNLLRTFCREATEOBJECT)(
	HLLJOB               hLlJob);
#endif /* IMPLEMENTATION */

extern PFNLLRTFCREATEOBJECT LlRTFCreateObject;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLRTFDELETEOBJECT)(
	HLLJOB               hLlJob,
	HLLRTFOBJ            hRTF);
#endif /* IMPLEMENTATION */

extern PFNLLRTFDELETEOBJECT LlRTFDeleteObject;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlRTFSetTextO LlRTFSetTextA
     #else
       #define LlRTFSetText LlRTFSetTextA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLRTFSETTEXTA)(
	HLLJOB               hLlJob,
	HLLRTFOBJ            hRTF,
	LPCSTR               pszText);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLRTFSETTEXTA LlRTFSetTextA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlRTFSetText LlRTFSetTextW
     #else
       #define LlRTFSetTextO LlRTFSetTextW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLRTFSETTEXTW)(
	HLLJOB               hLlJob,
	HLLRTFOBJ            hRTF,
	LPCWSTR              pszText);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLRTFSETTEXTW LlRTFSetTextW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef     UINT ( DLLPROC *PFNLLRTFGETTEXTLENGTH)(
	HLLJOB               hLlJob,
	HLLRTFOBJ            hRTF,
	INT                  nFlags);
#endif /* IMPLEMENTATION */

extern PFNLLRTFGETTEXTLENGTH LlRTFGetTextLength;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlRTFGetTextO LlRTFGetTextA
     #else
       #define LlRTFGetText LlRTFGetTextA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLRTFGETTEXTA)(
	HLLJOB               hLlJob,
	HLLRTFOBJ            hRTF,
	INT                  nFlags,
	LPSTR                pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLRTFGETTEXTA LlRTFGetTextA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlRTFGetText LlRTFGetTextW
     #else
       #define LlRTFGetTextO LlRTFGetTextW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLRTFGETTEXTW)(
	HLLJOB               hLlJob,
	HLLRTFOBJ            hRTF,
	INT                  nFlags,
	LPWSTR               pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLRTFGETTEXTW LlRTFGetTextW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLRTFEDITOBJECT)(
	HLLJOB               hLlJob,
	HLLRTFOBJ            hRTF,
	HWND                 hWnd,
	HDC                  hPrnDC,
	INT                  nProjectType,
	BOOL                 bModal);
#endif /* IMPLEMENTATION */

extern PFNLLRTFEDITOBJECT LlRTFEditObject;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLRTFCOPYTOCLIPBOARD)(
	HLLJOB               hLlJob,
	HLLRTFOBJ            hRTF);
#endif /* IMPLEMENTATION */

extern PFNLLRTFCOPYTOCLIPBOARD LlRTFCopyToClipboard;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLRTFDISPLAY)(
	HLLJOB               hLlJob,
	HLLRTFOBJ            hRTF,
	HDC                  hDC,
	_PRECT               pRC,
	BOOL                 bRestart,
	LLPUINT              pnState);
#endif /* IMPLEMENTATION */

extern PFNLLRTFDISPLAY LlRTFDisplay;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLRTFEDITORPROHIBITACTION)(
	HLLJOB               hLlJob,
	HLLRTFOBJ            hRTF,
	INT                  nControlID);
#endif /* IMPLEMENTATION */

extern PFNLLRTFEDITORPROHIBITACTION LlRTFEditorProhibitAction;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLRTFEDITORINVOKEACTION)(
	HLLJOB               hLlJob,
	HLLRTFOBJ            hRTF,
	INT                  nControlID);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLRTFEDITORINVOKEACTION LlRTFEditorInvokeAction;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDebugOutputO LlDebugOutputA
     #else
       #define LlDebugOutput LlDebugOutputA
   #endif
#endif
  #ifdef WIN32
  typedef     void ( DLLPROC *PFNLLDEBUGOUTPUTA)(
	INT                  nIndent,
	LPCSTR               pszText);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDEBUGOUTPUTA LlDebugOutputA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDebugOutput LlDebugOutputW
     #else
       #define LlDebugOutputO LlDebugOutputW
   #endif
#endif
  #ifdef WIN32
  typedef     void ( DLLPROC *PFNLLDEBUGOUTPUTW)(
	INT                  nIndent,
	LPCWSTR              pszText);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDEBUGOUTPUTW LlDebugOutputW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef HLISTPOS ( DLLPROC *PFNLLENUMGETFIRSTVAR)(
	HLLJOB               hLlJob,
	UINT32               nFlags);
#endif /* IMPLEMENTATION */

extern PFNLLENUMGETFIRSTVAR LlEnumGetFirstVar;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef HLISTPOS ( DLLPROC *PFNLLENUMGETFIRSTFIELD)(
	HLLJOB               hLlJob,
	UINT32               nFlags);
#endif /* IMPLEMENTATION */

extern PFNLLENUMGETFIRSTFIELD LlEnumGetFirstField;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef HLISTPOS ( DLLPROC *PFNLLENUMGETNEXTENTRY)(
	HLLJOB               hLlJob,
	HLISTPOS             nPos,
	UINT32               nFlags);
#endif /* IMPLEMENTATION */

extern PFNLLENUMGETNEXTENTRY LlEnumGetNextEntry;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlEnumGetEntryO LlEnumGetEntryA
     #else
       #define LlEnumGetEntry LlEnumGetEntryA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLENUMGETENTRYA)(
	HLLJOB               hLlJob,
	HLISTPOS             nPos,
	LPSTR                pszNameBuf,
	UINT                 nNameBufSize,
	LPSTR                pszContBuf,
	UINT                 nContBufSize,
	_LPHANDLE            pHandle,
	_LPINT32             pType);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLENUMGETENTRYA LlEnumGetEntryA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlEnumGetEntry LlEnumGetEntryW
     #else
       #define LlEnumGetEntryO LlEnumGetEntryW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLENUMGETENTRYW)(
	HLLJOB               hLlJob,
	HLISTPOS             nPos,
	LPWSTR               pszNameBuf,
	UINT                 nNameBufSize,
	LPWSTR               pszContBuf,
	UINT                 nContBufSize,
	_LPHANDLE            pHandle,
	_LPINT32             pType);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLENUMGETENTRYW LlEnumGetEntryW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLPRINTRESETOBJECTSTATES)(
	HLLJOB               hLlJob);
#endif /* IMPLEMENTATION */

extern PFNLLPRINTRESETOBJECTSTATES LlPrintResetObjectStates;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlXSetParameterO LlXSetParameterA
     #else
       #define LlXSetParameter LlXSetParameterA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLXSETPARAMETERA)(
	HLLJOB               hLlJob,
	INT                  nExtensionType,
	LPCSTR               pszExtensionName,
	LPCSTR               pszKey,
	LPCSTR               pszValue);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLXSETPARAMETERA LlXSetParameterA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlXSetParameter LlXSetParameterW
     #else
       #define LlXSetParameterO LlXSetParameterW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLXSETPARAMETERW)(
	HLLJOB               hLlJob,
	INT                  nExtensionType,
	LPCWSTR              pszExtensionName,
	LPCWSTR              pszKey,
	LPCWSTR              pszValue);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLXSETPARAMETERW LlXSetParameterW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlXGetParameterO LlXGetParameterA
     #else
       #define LlXGetParameter LlXGetParameterA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLXGETPARAMETERA)(
	HLLJOB               hLlJob,
	INT                  nExtensionType,
	LPCSTR               pszExtensionName,
	LPCSTR               pszKey,
	LPSTR                pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLXGETPARAMETERA LlXGetParameterA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlXGetParameter LlXGetParameterW
     #else
       #define LlXGetParameterO LlXGetParameterW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLXGETPARAMETERW)(
	HLLJOB               hLlJob,
	INT                  nExtensionType,
	LPCWSTR              pszExtensionName,
	LPCWSTR              pszKey,
	LPWSTR               pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLXGETPARAMETERW LlXGetParameterW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLPRINTRESETPROJECTSTATE)(
	HLLJOB               hJob);
#endif /* IMPLEMENTATION */

extern PFNLLPRINTRESETPROJECTSTATE LlPrintResetProjectState;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef     void ( DLLPROC *PFNLLDEFINECHARTFIELDSTART)(
	HLLJOB               hLlJob);
#endif /* IMPLEMENTATION */

extern PFNLLDEFINECHARTFIELDSTART LlDefineChartFieldStart;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDefineChartFieldExtO LlDefineChartFieldExtA
     #else
       #define LlDefineChartFieldExt LlDefineChartFieldExtA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDEFINECHARTFIELDEXTA)(
	HLLJOB               hLlJob,
	LPCSTR               pszVarName,
	LPCSTR               pszContents,
	INT32                lPara,
	LPVOID               lpPtr);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDEFINECHARTFIELDEXTA LlDefineChartFieldExtA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDefineChartFieldExt LlDefineChartFieldExtW
     #else
       #define LlDefineChartFieldExtO LlDefineChartFieldExtW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDEFINECHARTFIELDEXTW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszVarName,
	LPCWSTR              pszContents,
	INT32                lPara,
	LPVOID               lpPtr);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDEFINECHARTFIELDEXTW LlDefineChartFieldExtW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLPRINTDECLARECHARTROW)(
	HLLJOB               hLlJob,
	UINT                 nFlags);
#endif /* IMPLEMENTATION */

extern PFNLLPRINTDECLARECHARTROW LlPrintDeclareChartRow;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLPRINTGETCHARTOBJECTCOUNT)(
	HLLJOB               hLlJob,
	UINT                 nType);
#endif /* IMPLEMENTATION */

extern PFNLLPRINTGETCHARTOBJECTCOUNT LlPrintGetChartObjectCount;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintIsChartFieldUsedO LlPrintIsChartFieldUsedA
     #else
       #define LlPrintIsChartFieldUsed LlPrintIsChartFieldUsedA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTISCHARTFIELDUSEDA)(
	HLLJOB               hLlJob,
	LPCSTR               pszFieldName);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTISCHARTFIELDUSEDA LlPrintIsChartFieldUsedA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintIsChartFieldUsed LlPrintIsChartFieldUsedW
     #else
       #define LlPrintIsChartFieldUsedO LlPrintIsChartFieldUsedW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTISCHARTFIELDUSEDW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszFieldName);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTISCHARTFIELDUSEDW LlPrintIsChartFieldUsedW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlGetChartFieldContentsO LlGetChartFieldContentsA
     #else
       #define LlGetChartFieldContents LlGetChartFieldContentsA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLGETCHARTFIELDCONTENTSA)(
	HLLJOB               hLlJob,
	LPCSTR               pszName,
	LPSTR                pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLGETCHARTFIELDCONTENTSA LlGetChartFieldContentsA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlGetChartFieldContents LlGetChartFieldContentsW
     #else
       #define LlGetChartFieldContentsO LlGetChartFieldContentsW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLGETCHARTFIELDCONTENTSW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszName,
	LPWSTR               pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLGETCHARTFIELDCONTENTSW LlGetChartFieldContentsW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef HLISTPOS ( DLLPROC *PFNLLENUMGETFIRSTCHARTFIELD)(
	HLLJOB               hLlJob,
	UINT32               nFlags);
#endif /* IMPLEMENTATION */

extern PFNLLENUMGETFIRSTCHARTFIELD LlEnumGetFirstChartField;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef  FARPROC ( DLLPROC *PFNLLSETNOTIFICATIONCALLBACKEXT)(
	HLLJOB               hLlJob,
	INT                  nEvent,
	FARPROC              lpfnNotify);
#endif /* IMPLEMENTATION */

extern PFNLLSETNOTIFICATIONCALLBACKEXT LlSetNotificationCallbackExt;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLEXPREVALUATEVAR)(
	HLLJOB               hLlJob,
	HLLEXPR              lpExpr,
	PVARIANT             pVar);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLEXPREVALUATEVAR LlExprEvaluateVar;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLEXPRTYPEVAR)(
	HLLJOB               hLlJob,
	HLLEXPR              lpExpr);
#endif /* IMPLEMENTATION */

extern PFNLLEXPRTYPEVAR LlExprTypeVar;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlGetPrinterFromPrinterFileO LlGetPrinterFromPrinterFileA
     #else
       #define LlGetPrinterFromPrinterFile LlGetPrinterFromPrinterFileA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLGETPRINTERFROMPRINTERFILEA)(
	HLLJOB               hJob,
	UINT                 nObjType,
	LPCSTR               pszObjectName,
	INT                  nPrinter,
	LPSTR                pszPrinter,
	LLPUINT              pnPrinterBufSize,
	_PDEVMODEA           pDevMode,
	LLPUINT              pnDevModeBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLGETPRINTERFROMPRINTERFILEA LlGetPrinterFromPrinterFileA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlGetPrinterFromPrinterFile LlGetPrinterFromPrinterFileW
     #else
       #define LlGetPrinterFromPrinterFileO LlGetPrinterFromPrinterFileW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLGETPRINTERFROMPRINTERFILEW)(
	HLLJOB               hJob,
	UINT                 nObjType,
	LPCWSTR              pszObjectName,
	INT                  nPrinter,
	LPWSTR               pszPrinter,
	LLPUINT              pnPrinterBufSize,
	_PDEVMODEW           pDevMode,
	LLPUINT              pnDevModeBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLGETPRINTERFROMPRINTERFILEW LlGetPrinterFromPrinterFileW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintGetRemainingSpacePerTableO LlPrintGetRemainingSpacePerTableA
     #else
       #define LlPrintGetRemainingSpacePerTable LlPrintGetRemainingSpacePerTableA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTGETREMAININGSPACEPERTABLEA)(
	HLLJOB               hLlJob,
	LPCSTR               pszField,
	INT                  nDimension);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTGETREMAININGSPACEPERTABLEA LlPrintGetRemainingSpacePerTableA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintGetRemainingSpacePerTable LlPrintGetRemainingSpacePerTableW
     #else
       #define LlPrintGetRemainingSpacePerTableO LlPrintGetRemainingSpacePerTableW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTGETREMAININGSPACEPERTABLEW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszField,
	INT                  nDimension);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTGETREMAININGSPACEPERTABLEW LlPrintGetRemainingSpacePerTableW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  #ifdef WIN32
  typedef     void ( DLLPROC *PFNLLDRAWTOOLBARBACKGROUND)(
	HDC                  hDC,
	_PRECT               pRC,
	BOOL                 bHorz,
	INT                  nTBMode);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDRAWTOOLBARBACKGROUND LlDrawToolbarBackground;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlSetDefaultProjectParameterO LlSetDefaultProjectParameterA
     #else
       #define LlSetDefaultProjectParameter LlSetDefaultProjectParameterA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLSETDEFAULTPROJECTPARAMETERA)(
	HLLJOB               hLlJob,
	LPCSTR               pszParameter,
	LPCSTR               pszValue,
	UINT32               nFlags);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLSETDEFAULTPROJECTPARAMETERA LlSetDefaultProjectParameterA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlSetDefaultProjectParameter LlSetDefaultProjectParameterW
     #else
       #define LlSetDefaultProjectParameterO LlSetDefaultProjectParameterW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLSETDEFAULTPROJECTPARAMETERW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszParameter,
	LPCWSTR              pszValue,
	UINT32               nFlags);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLSETDEFAULTPROJECTPARAMETERW LlSetDefaultProjectParameterW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlGetDefaultProjectParameterO LlGetDefaultProjectParameterA
     #else
       #define LlGetDefaultProjectParameter LlGetDefaultProjectParameterA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLGETDEFAULTPROJECTPARAMETERA)(
	HLLJOB               hLlJob,
	LPCSTR               pszParameter,
	LPSTR                pszBuffer,
	INT                  nBufSize,
	_LPUINT32            pnFlags);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLGETDEFAULTPROJECTPARAMETERA LlGetDefaultProjectParameterA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlGetDefaultProjectParameter LlGetDefaultProjectParameterW
     #else
       #define LlGetDefaultProjectParameterO LlGetDefaultProjectParameterW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLGETDEFAULTPROJECTPARAMETERW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszParameter,
	LPWSTR               pszBuffer,
	INT                  nBufSize,
	_LPUINT32            pnFlags);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLGETDEFAULTPROJECTPARAMETERW LlGetDefaultProjectParameterW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintSetProjectParameterO LlPrintSetProjectParameterA
     #else
       #define LlPrintSetProjectParameter LlPrintSetProjectParameterA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTSETPROJECTPARAMETERA)(
	HLLJOB               hLlJob,
	LPCSTR               pszParameter,
	LPCSTR               pszValue,
	UINT32               nFlags);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTSETPROJECTPARAMETERA LlPrintSetProjectParameterA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintSetProjectParameter LlPrintSetProjectParameterW
     #else
       #define LlPrintSetProjectParameterO LlPrintSetProjectParameterW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTSETPROJECTPARAMETERW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszParameter,
	LPCWSTR              pszValue,
	UINT32               nFlags);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTSETPROJECTPARAMETERW LlPrintSetProjectParameterW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintGetProjectParameterO LlPrintGetProjectParameterA
     #else
       #define LlPrintGetProjectParameter LlPrintGetProjectParameterA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTGETPROJECTPARAMETERA)(
	HLLJOB               hLlJob,
	LPCSTR               pszParameter,
	BOOL                 bEvaluated,
	LPSTR                pszBuffer,
	INT                  nBufSize,
	_LPUINT32            pnFlags);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTGETPROJECTPARAMETERA LlPrintGetProjectParameterA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintGetProjectParameter LlPrintGetProjectParameterW
     #else
       #define LlPrintGetProjectParameterO LlPrintGetProjectParameterW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTGETPROJECTPARAMETERW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszParameter,
	BOOL                 bEvaluated,
	LPWSTR               pszBuffer,
	INT                  nBufSize,
	_LPUINT32            pnFlags);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTGETPROJECTPARAMETERW LlPrintGetProjectParameterW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef     BOOL ( DLLPROC *PFNLLCREATEOBJECT)(
	CTL_GUID             pIID,
	CTL_PPUNK            ppI);
#endif /* IMPLEMENTATION */

extern PFNLLCREATEOBJECT LlCreateObject;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlExprContainsVariableO LlExprContainsVariableA
     #else
       #define LlExprContainsVariable LlExprContainsVariableA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLEXPRCONTAINSVARIABLEA)(
	HLLJOB               hLlJob,
	HLLEXPR              hExpr,
	LPCSTR               pszVariable);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLEXPRCONTAINSVARIABLEA LlExprContainsVariableA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlExprContainsVariable LlExprContainsVariableW
     #else
       #define LlExprContainsVariableO LlExprContainsVariableW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLEXPRCONTAINSVARIABLEW)(
	HLLJOB               hLlJob,
	HLLEXPR              hExpr,
	LPCWSTR              pszVariable);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLEXPRCONTAINSVARIABLEW LlExprContainsVariableW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLEXPRISCONSTANT)(
	HLLJOB               hLlJob,
	HLLEXPR              hExpr);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLEXPRISCONSTANT LlExprIsConstant;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlLocSystemTimeFromLocaleStringO LlLocSystemTimeFromLocaleStringA
     #else
       #define LlLocSystemTimeFromLocaleString LlLocSystemTimeFromLocaleStringA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLLOCSYSTEMTIMEFROMLOCALESTRINGA)(
	LCID                 nLCID,
	LPCSTR               pszDateTime,
	_PSYSTEMTIME         pST,
	BOOL                 bAddCentury);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLLOCSYSTEMTIMEFROMLOCALESTRINGA LlLocSystemTimeFromLocaleStringA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlLocSystemTimeFromLocaleString LlLocSystemTimeFromLocaleStringW
     #else
       #define LlLocSystemTimeFromLocaleStringO LlLocSystemTimeFromLocaleStringW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLLOCSYSTEMTIMEFROMLOCALESTRINGW)(
	LCID                 nLCID,
	LPCWSTR              pszDateTime,
	_PSYSTEMTIME         pST,
	BOOL                 bAddCentury);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLLOCSYSTEMTIMEFROMLOCALESTRINGW LlLocSystemTimeFromLocaleStringW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlLocSystemTimeToLocaleStringO LlLocSystemTimeToLocaleStringA
     #else
       #define LlLocSystemTimeToLocaleString LlLocSystemTimeToLocaleStringA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLLOCSYSTEMTIMETOLOCALESTRINGA)(
	LCID                 nLCID,
	_PCSYSTEMTIME        pST,
	LPSTR                pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLLOCSYSTEMTIMETOLOCALESTRINGA LlLocSystemTimeToLocaleStringA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlLocSystemTimeToLocaleString LlLocSystemTimeToLocaleStringW
     #else
       #define LlLocSystemTimeToLocaleStringO LlLocSystemTimeToLocaleStringW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLLOCSYSTEMTIMETOLOCALESTRINGW)(
	LCID                 nLCID,
	_PCSYSTEMTIME        pST,
	LPWSTR               pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLLOCSYSTEMTIMETOLOCALESTRINGW LlLocSystemTimeToLocaleStringW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlLocConvertCountryToO LlLocConvertCountryToA
     #else
       #define LlLocConvertCountryTo LlLocConvertCountryToA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLLOCCONVERTCOUNTRYTOA)(
	HLLJOB               hJob,
	LPCSTR               pszCountryID,
	INT                  nIndex,
	LPSTR                pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLLOCCONVERTCOUNTRYTOA LlLocConvertCountryToA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlLocConvertCountryTo LlLocConvertCountryToW
     #else
       #define LlLocConvertCountryToO LlLocConvertCountryToW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLLOCCONVERTCOUNTRYTOW)(
	HLLJOB               hJob,
	LPCWSTR              pszCountryID,
	INT                  nIndex,
	LPWSTR               pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLLOCCONVERTCOUNTRYTOW LlLocConvertCountryToW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlProfileStartO LlProfileStartA
     #else
       #define LlProfileStart LlProfileStartA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPROFILESTARTA)(
	HANDLE               hThread,
	LPCSTR               pszDescr,
	LPCSTR               pszFilename,
	INT                  nTicksMS);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPROFILESTARTA LlProfileStartA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlProfileStart LlProfileStartW
     #else
       #define LlProfileStartO LlProfileStartW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPROFILESTARTW)(
	HANDLE               hThread,
	LPCWSTR              pszDescr,
	LPCWSTR              pszFilename,
	INT                  nTicksMS);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPROFILESTARTW LlProfileStartW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  #ifdef WIN32
  typedef     void ( DLLPROC *PFNLLPROFILEEND)(
	HANDLE               hThread);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPROFILEEND LlProfileEnd;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  #ifdef WIN32
  typedef     void ( DLLPROC *PFNLLDUMPMEMORY)(
	BOOL                 bDumpAll);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDUMPMEMORY LlDumpMemory;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDbAddTableO LlDbAddTableA
     #else
       #define LlDbAddTable LlDbAddTableA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDBADDTABLEA)(
	HLLJOB               hJob,
	LPCSTR               pszTableID,
	LPCSTR               pszDisplayName);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDBADDTABLEA LlDbAddTableA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDbAddTable LlDbAddTableW
     #else
       #define LlDbAddTableO LlDbAddTableW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDBADDTABLEW)(
	HLLJOB               hJob,
	LPCWSTR              pszTableID,
	LPCWSTR              pszDisplayName);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDBADDTABLEW LlDbAddTableW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDbAddTableRelationO LlDbAddTableRelationA
     #else
       #define LlDbAddTableRelation LlDbAddTableRelationA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDBADDTABLERELATIONA)(
	HLLJOB               hJob,
	LPCSTR               pszTableID,
	LPCSTR               pszParentTableID,
	LPCSTR               pszRelationID,
	LPCSTR               pszRelationDisplayName);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDBADDTABLERELATIONA LlDbAddTableRelationA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDbAddTableRelation LlDbAddTableRelationW
     #else
       #define LlDbAddTableRelationO LlDbAddTableRelationW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDBADDTABLERELATIONW)(
	HLLJOB               hJob,
	LPCWSTR              pszTableID,
	LPCWSTR              pszParentTableID,
	LPCWSTR              pszRelationID,
	LPCWSTR              pszRelationDisplayName);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDBADDTABLERELATIONW LlDbAddTableRelationW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDbAddTableSortOrderO LlDbAddTableSortOrderA
     #else
       #define LlDbAddTableSortOrder LlDbAddTableSortOrderA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDBADDTABLESORTORDERA)(
	HLLJOB               hJob,
	LPCSTR               pszTableID,
	LPCSTR               pszSortOrderID,
	LPCSTR               pszSortOrderDisplayName);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDBADDTABLESORTORDERA LlDbAddTableSortOrderA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDbAddTableSortOrder LlDbAddTableSortOrderW
     #else
       #define LlDbAddTableSortOrderO LlDbAddTableSortOrderW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDBADDTABLESORTORDERW)(
	HLLJOB               hJob,
	LPCWSTR              pszTableID,
	LPCWSTR              pszSortOrderID,
	LPCWSTR              pszSortOrderDisplayName);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDBADDTABLESORTORDERW LlDbAddTableSortOrderW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintDbGetCurrentTableO LlPrintDbGetCurrentTableA
     #else
       #define LlPrintDbGetCurrentTable LlPrintDbGetCurrentTableA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTDBGETCURRENTTABLEA)(
	HLLJOB               hJob,
	LPSTR                pszTableID,
	UINT                 nTableIDLength,
	BOOL                 bCompletePath);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTDBGETCURRENTTABLEA LlPrintDbGetCurrentTableA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintDbGetCurrentTable LlPrintDbGetCurrentTableW
     #else
       #define LlPrintDbGetCurrentTableO LlPrintDbGetCurrentTableW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTDBGETCURRENTTABLEW)(
	HLLJOB               hJob,
	LPWSTR               pszTableID,
	UINT                 nTableIDLength,
	BOOL                 bCompletePath);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTDBGETCURRENTTABLEW LlPrintDbGetCurrentTableW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintDbGetCurrentTableRelationO LlPrintDbGetCurrentTableRelationA
     #else
       #define LlPrintDbGetCurrentTableRelation LlPrintDbGetCurrentTableRelationA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTDBGETCURRENTTABLERELATIONA)(
	HLLJOB               hJob,
	LPSTR                pszRelationID,
	UINT                 nRelationIDLength);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTDBGETCURRENTTABLERELATIONA LlPrintDbGetCurrentTableRelationA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintDbGetCurrentTableRelation LlPrintDbGetCurrentTableRelationW
     #else
       #define LlPrintDbGetCurrentTableRelationO LlPrintDbGetCurrentTableRelationW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTDBGETCURRENTTABLERELATIONW)(
	HLLJOB               hJob,
	LPWSTR               pszRelationID,
	UINT                 nRelationIDLength);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTDBGETCURRENTTABLERELATIONW LlPrintDbGetCurrentTableRelationW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintDbGetCurrentTableSortOrderO LlPrintDbGetCurrentTableSortOrderA
     #else
       #define LlPrintDbGetCurrentTableSortOrder LlPrintDbGetCurrentTableSortOrderA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTDBGETCURRENTTABLESORTORDERA)(
	HLLJOB               hJob,
	LPSTR                pszSortOrderID,
	UINT                 nSortOrderIDLength);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTDBGETCURRENTTABLESORTORDERA LlPrintDbGetCurrentTableSortOrderA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlPrintDbGetCurrentTableSortOrder LlPrintDbGetCurrentTableSortOrderW
     #else
       #define LlPrintDbGetCurrentTableSortOrderO LlPrintDbGetCurrentTableSortOrderW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTDBGETCURRENTTABLESORTORDERW)(
	HLLJOB               hJob,
	LPWSTR               pszSortOrderID,
	UINT                 nSortOrderIDLength);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTDBGETCURRENTTABLESORTORDERW LlPrintDbGetCurrentTableSortOrderW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDBDUMPSTRUCTURE)(
	HLLJOB               hJob);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDBDUMPSTRUCTURE LlDbDumpStructure;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLPRINTDBGETROOTTABLECOUNT)(
	HLLJOB               hJob);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLPRINTDBGETROOTTABLECOUNT LlPrintDbGetRootTableCount;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDbSetMasterTableO LlDbSetMasterTableA
     #else
       #define LlDbSetMasterTable LlDbSetMasterTableA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDBSETMASTERTABLEA)(
	HLLJOB               hJob,
	LPCSTR               pszTableID);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDBSETMASTERTABLEA LlDbSetMasterTableA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDbSetMasterTable LlDbSetMasterTableW
     #else
       #define LlDbSetMasterTableO LlDbSetMasterTableW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDBSETMASTERTABLEW)(
	HLLJOB               hJob,
	LPCWSTR              pszTableID);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDBSETMASTERTABLEW LlDbSetMasterTableW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDbGetMasterTableO LlDbGetMasterTableA
     #else
       #define LlDbGetMasterTable LlDbGetMasterTableA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDBGETMASTERTABLEA)(
	HLLJOB               hJob,
	LPSTR                pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDBGETMASTERTABLEA LlDbGetMasterTableA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDbGetMasterTable LlDbGetMasterTableW
     #else
       #define LlDbGetMasterTableO LlDbGetMasterTableW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDBGETMASTERTABLEW)(
	HLLJOB               hJob,
	LPWSTR               pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDBGETMASTERTABLEW LlDbGetMasterTableW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlXSetExportParameterO LlXSetExportParameterA
     #else
       #define LlXSetExportParameter LlXSetExportParameterA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLXSETEXPORTPARAMETERA)(
	HLLJOB               hLlJob,
	LPCSTR               pszExtensionName,
	LPCSTR               pszKey,
	LPCSTR               pszValue);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLXSETEXPORTPARAMETERA LlXSetExportParameterA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlXSetExportParameter LlXSetExportParameterW
     #else
       #define LlXSetExportParameterO LlXSetExportParameterW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLXSETEXPORTPARAMETERW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszExtensionName,
	LPCWSTR              pszKey,
	LPCWSTR              pszValue);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLXSETEXPORTPARAMETERW LlXSetExportParameterW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlXGetExportParameterO LlXGetExportParameterA
     #else
       #define LlXGetExportParameter LlXGetExportParameterA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLXGETEXPORTPARAMETERA)(
	HLLJOB               hLlJob,
	LPCSTR               pszExtensionName,
	LPCSTR               pszKey,
	LPSTR                pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLXGETEXPORTPARAMETERA LlXGetExportParameterA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlXGetExportParameter LlXGetExportParameterW
     #else
       #define LlXGetExportParameterO LlXGetExportParameterW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLXGETEXPORTPARAMETERW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszExtensionName,
	LPCWSTR              pszKey,
	LPWSTR               pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLXGETEXPORTPARAMETERW LlXGetExportParameterW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlXlatNameO LlXlatNameA
     #else
       #define LlXlatName LlXlatNameA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLXLATNAMEA)(
	HLLJOB               hLlJob,
	LPCSTR               pszName,
	LPSTR                pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLXLATNAMEA LlXlatNameA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlXlatName LlXlatNameW
     #else
       #define LlXlatNameO LlXlatNameW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLXLATNAMEW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszName,
	LPWSTR               pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLXLATNAMEW LlXlatNameW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDefineVariableVarO LlDefineVariableVarA
     #else
       #define LlDefineVariableVar LlDefineVariableVarA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDEFINEVARIABLEVARA)(
	HLLJOB               hLlJob,
	LPCSTR               pszName,
	const PVARIANT       pValue,
	INT32                lPara,
	LPVOID               lpPtr);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDEFINEVARIABLEVARA LlDefineVariableVarA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDefineVariableVar LlDefineVariableVarW
     #else
       #define LlDefineVariableVarO LlDefineVariableVarW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDEFINEVARIABLEVARW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszName,
	const PVARIANT       pValue,
	INT32                lPara,
	LPVOID               lpPtr);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDEFINEVARIABLEVARW LlDefineVariableVarW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDefineFieldVarO LlDefineFieldVarA
     #else
       #define LlDefineFieldVar LlDefineFieldVarA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDEFINEFIELDVARA)(
	HLLJOB               hLlJob,
	LPCSTR               pszName,
	const PVARIANT       pValue,
	INT32                lPara,
	LPVOID               lpPtr);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDEFINEFIELDVARA LlDefineFieldVarA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDefineFieldVar LlDefineFieldVarW
     #else
       #define LlDefineFieldVarO LlDefineFieldVarW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDEFINEFIELDVARW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszName,
	const PVARIANT       pValue,
	INT32                lPara,
	LPVOID               lpPtr);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDEFINEFIELDVARW LlDefineFieldVarW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDefineChartFieldVarO LlDefineChartFieldVarA
     #else
       #define LlDefineChartFieldVar LlDefineChartFieldVarA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDEFINECHARTFIELDVARA)(
	HLLJOB               hLlJob,
	LPCSTR               pszName,
	const PVARIANT       pValue,
	INT32                lPara,
	LPVOID               lpPtr);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDEFINECHARTFIELDVARA LlDefineChartFieldVarA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDefineChartFieldVar LlDefineChartFieldVarW
     #else
       #define LlDefineChartFieldVarO LlDefineChartFieldVarW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDEFINECHARTFIELDVARW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszName,
	const PVARIANT       pValue,
	INT32                lPara,
	LPVOID               lpPtr);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDEFINECHARTFIELDVARW LlDefineChartFieldVarW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDesignerProhibitEditingObjectO LlDesignerProhibitEditingObjectA
     #else
       #define LlDesignerProhibitEditingObject LlDesignerProhibitEditingObjectA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDESIGNERPROHIBITEDITINGOBJECTA)(
	HLLJOB               hLlJob,
	LPCSTR               pszObject);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDESIGNERPROHIBITEDITINGOBJECTA LlDesignerProhibitEditingObjectA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlDesignerProhibitEditingObject LlDesignerProhibitEditingObjectW
     #else
       #define LlDesignerProhibitEditingObjectO LlDesignerProhibitEditingObjectW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLDESIGNERPROHIBITEDITINGOBJECTW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszObject);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLDESIGNERPROHIBITEDITINGOBJECTW LlDesignerProhibitEditingObjectW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlGetUsedIdentifiersO LlGetUsedIdentifiersA
     #else
       #define LlGetUsedIdentifiers LlGetUsedIdentifiersA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLGETUSEDIDENTIFIERSA)(
	HLLJOB               hLlJob,
	LPCSTR               pszProjectName,
	LPSTR                pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLGETUSEDIDENTIFIERSA LlGetUsedIdentifiersA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlGetUsedIdentifiers LlGetUsedIdentifiersW
     #else
       #define LlGetUsedIdentifiersO LlGetUsedIdentifiersW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLGETUSEDIDENTIFIERSW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszProjectName,
	LPWSTR               pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLGETUSEDIDENTIFIERSW LlGetUsedIdentifiersW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLINTERNALATTACHAPP)(
	HLLJOB               hLlJob,
	HLLJOB               hLlJobToAttach);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLINTERNALATTACHAPP LlInternalAttachApp;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLINTERNALDETACHAPP)(
	HLLJOB               hLlJob,
	HLLJOB               hLlJobToDetach);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLINTERNALDETACHAPP LlInternalDetachApp;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlGetUserSectionDataO LlGetUserSectionDataA
     #else
       #define LlGetUserSectionData LlGetUserSectionDataA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLGETUSERSECTIONDATAA)(
	HLLJOB               hLlJob,
	LPCSTR               pszProjectName,
	PSCLLPROJECTUSERDATA pPUD);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLGETUSERSECTIONDATAA LlGetUserSectionDataA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlGetUserSectionData LlGetUserSectionDataW
     #else
       #define LlGetUserSectionDataO LlGetUserSectionDataW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLGETUSERSECTIONDATAW)(
	HLLJOB               hLlJob,
	LPCWSTR              pszProjectName,
	PSCLLPROJECTUSERDATA pPUD);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLGETUSERSECTIONDATAW LlGetUserSectionDataW;

INT LL11xLoad(void);
void LL11xUnload(void);

#ifdef IMPLEMENTATION
#if defined(WIN32)
  #define PROCIMPLL11(p) TEXT(#p)
  #if defined(__WATCOMC__)
    #define LOADDLLPROCLL11(fn,pfn,n) \
      fn = (pfn)GetProcAddress(hDLLLL11,TEXT("_") PROCIMPLL11(fn) TEXT("@") PROCIMPLL11(n));
    #define LOADDLLCPROCLL11(fn,pfn,n) \
      fn = (pfn)GetProcAddress(hDLLLL11,TEXT("_") PROCIMPLL11(fn));
   #else
    #define LOADDLLPROCLL11(fn,pfn,n) \
      *(FARPROC*)&fn = GetProcAddress(hDLLLL11,TEXT("_") PROCIMPLL11(fn) TEXT("@") PROCIMPLL11(n));
    #define LOADDLLCPROCLL11(fn,pfn,n) \
      *(FARPROC*)&fn = GetProcAddress(hDLLLL11,TEXT("_") PROCIMPLL11(fn));
  #endif
#endif // WIN32

INT LL11xLoad(void)
{
  if (nDLLLL11Counter++ > 0)
    return(0);

  INT nError = SetErrorMode(SEM_NOOPENFILEERRORBOX);

#ifdef WIN32
  #ifdef UNICODE
    hDLLLL11 = LoadLibrary(L"CULL11.DLL");
  #else
    hDLLLL11 = LoadLibrary("CMLL11.DLL");
  #endif
#else
  hDLLLL11 = LoadLibrary("CMLL11.DLL");
#endif
  SetErrorMode(nError);


#ifdef WIN32
  if (hDLLLL11 == NULL) return(-1);
#else
  if (hDLLLL11 < HINSTANCE_ERROR) return(-1);
#endif

  LOADDLLPROCLL11(LlJobOpen,PFNLLJOBOPEN,4);
  LOADDLLPROCLL11(LlJobOpenLCID,PFNLLJOBOPENLCID,4);
  LOADDLLPROCLL11(LlJobClose,PFNLLJOBCLOSE,4);
  LOADDLLPROCLL11(LlSetDebug,PFNLLSETDEBUG,4);
  LOADDLLPROCLL11(LlGetVersion,PFNLLGETVERSION,4);
  LOADDLLPROCLL11(LlGetNotificationMessage,PFNLLGETNOTIFICATIONMESSAGE,4);
  LOADDLLPROCLL11(LlSetNotificationMessage,PFNLLSETNOTIFICATIONMESSAGE,8);
  LOADDLLPROCLL11(LlSetNotificationCallback,PFNLLSETNOTIFICATIONCALLBACK,8);
  LOADDLLPROCLL11(LlDefineFieldA,PFNLLDEFINEFIELDA,12);
  LOADDLLPROCLL11(LlDefineFieldW,PFNLLDEFINEFIELDW,12);
  LOADDLLPROCLL11(LlDefineFieldExtA,PFNLLDEFINEFIELDEXTA,20);
  LOADDLLPROCLL11(LlDefineFieldExtW,PFNLLDEFINEFIELDEXTW,20);
  LOADDLLPROCLL11(LlDefineFieldExtHandleA,PFNLLDEFINEFIELDEXTHANDLEA,20);
  LOADDLLPROCLL11(LlDefineFieldExtHandleW,PFNLLDEFINEFIELDEXTHANDLEW,20);
  LOADDLLPROCLL11(LlDefineFieldStart,PFNLLDEFINEFIELDSTART,4);
  LOADDLLPROCLL11(LlDefineVariableA,PFNLLDEFINEVARIABLEA,12);
  LOADDLLPROCLL11(LlDefineVariableW,PFNLLDEFINEVARIABLEW,12);
  LOADDLLPROCLL11(LlDefineVariableExtA,PFNLLDEFINEVARIABLEEXTA,20);
  LOADDLLPROCLL11(LlDefineVariableExtW,PFNLLDEFINEVARIABLEEXTW,20);
  LOADDLLPROCLL11(LlDefineVariableExtHandleA,PFNLLDEFINEVARIABLEEXTHANDLEA,20);
  LOADDLLPROCLL11(LlDefineVariableExtHandleW,PFNLLDEFINEVARIABLEEXTHANDLEW,20);
  LOADDLLPROCLL11(LlDefineVariableNameA,PFNLLDEFINEVARIABLENAMEA,8);
  LOADDLLPROCLL11(LlDefineVariableNameW,PFNLLDEFINEVARIABLENAMEW,8);
  LOADDLLPROCLL11(LlDefineVariableStart,PFNLLDEFINEVARIABLESTART,4);
  LOADDLLPROCLL11(LlDefineSumVariableA,PFNLLDEFINESUMVARIABLEA,12);
  LOADDLLPROCLL11(LlDefineSumVariableW,PFNLLDEFINESUMVARIABLEW,12);
  LOADDLLPROCLL11(LlDefineLayoutA,PFNLLDEFINELAYOUTA,20);
  LOADDLLPROCLL11(LlDefineLayoutW,PFNLLDEFINELAYOUTW,20);
  LOADDLLPROCLL11(LlDlgEditLineA,PFNLLDLGEDITLINEA,16);
  LOADDLLPROCLL11(LlDlgEditLineW,PFNLLDLGEDITLINEW,16);
  LOADDLLPROCLL11(LlDlgEditLineExA,PFNLLDLGEDITLINEEXA,32);
  LOADDLLPROCLL11(LlDlgEditLineExW,PFNLLDLGEDITLINEEXW,32);
  LOADDLLPROCLL11(LlPreviewSetTempPathA,PFNLLPREVIEWSETTEMPPATHA,8);
  LOADDLLPROCLL11(LlPreviewSetTempPathW,PFNLLPREVIEWSETTEMPPATHW,8);
  LOADDLLPROCLL11(LlPreviewDeleteFilesA,PFNLLPREVIEWDELETEFILESA,12);
  LOADDLLPROCLL11(LlPreviewDeleteFilesW,PFNLLPREVIEWDELETEFILESW,12);
  LOADDLLPROCLL11(LlPreviewDisplayA,PFNLLPREVIEWDISPLAYA,16);
  LOADDLLPROCLL11(LlPreviewDisplayW,PFNLLPREVIEWDISPLAYW,16);
  LOADDLLPROCLL11(LlPreviewDisplayExA,PFNLLPREVIEWDISPLAYEXA,24);
  LOADDLLPROCLL11(LlPreviewDisplayExW,PFNLLPREVIEWDISPLAYEXW,24);
  LOADDLLPROCLL11(LlPrint,PFNLLPRINT,4);
  LOADDLLPROCLL11(LlPrintAbort,PFNLLPRINTABORT,4);
  LOADDLLPROCLL11(LlPrintCheckLineFit,PFNLLPRINTCHECKLINEFIT,4);
  LOADDLLPROCLL11(LlPrintEnd,PFNLLPRINTEND,8);
  LOADDLLPROCLL11(LlPrintFields,PFNLLPRINTFIELDS,4);
  LOADDLLPROCLL11(LlPrintFieldsEnd,PFNLLPRINTFIELDSEND,4);
  LOADDLLPROCLL11(LlPrintGetCurrentPage,PFNLLPRINTGETCURRENTPAGE,4);
  LOADDLLPROCLL11(LlPrintGetItemsPerPage,PFNLLPRINTGETITEMSPERPAGE,4);
  LOADDLLPROCLL11(LlPrintGetItemsPerTable,PFNLLPRINTGETITEMSPERTABLE,4);
  LOADDLLPROCLL11(LlPrintGetRemainingItemsPerTableA,PFNLLPRINTGETREMAININGITEMSPERTABLEA,8);
  LOADDLLPROCLL11(LlPrintGetRemainingItemsPerTableW,PFNLLPRINTGETREMAININGITEMSPERTABLEW,8);
  LOADDLLPROCLL11(LlPrintGetRemItemsPerTableA,PFNLLPRINTGETREMITEMSPERTABLEA,8);
  LOADDLLPROCLL11(LlPrintGetRemItemsPerTableW,PFNLLPRINTGETREMITEMSPERTABLEW,8);
  LOADDLLPROCLL11(LlPrintGetOption,PFNLLPRINTGETOPTION,8);
  LOADDLLPROCLL11(LlPrintGetPrinterInfoA,PFNLLPRINTGETPRINTERINFOA,20);
  LOADDLLPROCLL11(LlPrintGetPrinterInfoW,PFNLLPRINTGETPRINTERINFOW,20);
  LOADDLLPROCLL11(LlPrintOptionsDialogA,PFNLLPRINTOPTIONSDIALOGA,12);
  LOADDLLPROCLL11(LlPrintOptionsDialogW,PFNLLPRINTOPTIONSDIALOGW,12);
  LOADDLLPROCLL11(LlPrintSelectOffsetEx,PFNLLPRINTSELECTOFFSETEX,8);
  LOADDLLPROCLL11(LlPrintSetBoxTextA,PFNLLPRINTSETBOXTEXTA,12);
  LOADDLLPROCLL11(LlPrintSetBoxTextW,PFNLLPRINTSETBOXTEXTW,12);
  LOADDLLPROCLL11(LlPrintSetOption,PFNLLPRINTSETOPTION,12);
  LOADDLLPROCLL11(LlPrintUpdateBox,PFNLLPRINTUPDATEBOX,4);
  LOADDLLPROCLL11(LlPrintStartA,PFNLLPRINTSTARTA,20);
  LOADDLLPROCLL11(LlPrintStartW,PFNLLPRINTSTARTW,20);
  LOADDLLPROCLL11(LlPrintWithBoxStartA,PFNLLPRINTWITHBOXSTARTA,28);
  LOADDLLPROCLL11(LlPrintWithBoxStartW,PFNLLPRINTWITHBOXSTARTW,28);
  LOADDLLPROCLL11(LlPrinterSetupA,PFNLLPRINTERSETUPA,16);
  LOADDLLPROCLL11(LlPrinterSetupW,PFNLLPRINTERSETUPW,16);
  LOADDLLPROCLL11(LlSelectFileDlgTitleExA,PFNLLSELECTFILEDLGTITLEEXA,28);
  LOADDLLPROCLL11(LlSelectFileDlgTitleExW,PFNLLSELECTFILEDLGTITLEEXW,28);
  LOADDLLPROCLL11(LlSetDlgboxMode,PFNLLSETDLGBOXMODE,4);
  LOADDLLPROCLL11(LlGetDlgboxMode,PFNLLGETDLGBOXMODE,0);
  LOADDLLPROCLL11(LlExprParseA,PFNLLEXPRPARSEA,12);
  LOADDLLPROCLL11(LlExprParseW,PFNLLEXPRPARSEW,12);
  LOADDLLPROCLL11(LlExprType,PFNLLEXPRTYPE,8);
  LOADDLLPROCLL11(LlExprErrorA,PFNLLEXPRERRORA,12);
  LOADDLLPROCLL11(LlExprErrorW,PFNLLEXPRERRORW,12);
  LOADDLLPROCLL11(LlExprFree,PFNLLEXPRFREE,8);
  LOADDLLPROCLL11(LlExprEvaluateA,PFNLLEXPREVALUATEA,16);
  LOADDLLPROCLL11(LlExprEvaluateW,PFNLLEXPREVALUATEW,16);
  LOADDLLPROCLL11(LlExprGetUsedVarsA,PFNLLEXPRGETUSEDVARSA,16);
  LOADDLLPROCLL11(LlExprGetUsedVarsW,PFNLLEXPRGETUSEDVARSW,16);
  LOADDLLPROCLL11(LlSetOption,PFNLLSETOPTION,12);
  LOADDLLPROCLL11(LlGetOption,PFNLLGETOPTION,8);
  LOADDLLPROCLL11(LlSetOptionStringA,PFNLLSETOPTIONSTRINGA,12);
  LOADDLLPROCLL11(LlSetOptionStringW,PFNLLSETOPTIONSTRINGW,12);
  LOADDLLPROCLL11(LlGetOptionStringA,PFNLLGETOPTIONSTRINGA,16);
  LOADDLLPROCLL11(LlGetOptionStringW,PFNLLGETOPTIONSTRINGW,16);
  LOADDLLPROCLL11(LlPrintSetOptionStringA,PFNLLPRINTSETOPTIONSTRINGA,12);
  LOADDLLPROCLL11(LlPrintSetOptionStringW,PFNLLPRINTSETOPTIONSTRINGW,12);
  LOADDLLPROCLL11(LlPrintGetOptionStringA,PFNLLPRINTGETOPTIONSTRINGA,16);
  LOADDLLPROCLL11(LlPrintGetOptionStringW,PFNLLPRINTGETOPTIONSTRINGW,16);
  LOADDLLPROCLL11(LlDesignerProhibitAction,PFNLLDESIGNERPROHIBITACTION,8);
  LOADDLLPROCLL11(LlDesignerProhibitFunctionA,PFNLLDESIGNERPROHIBITFUNCTIONA,8);
  LOADDLLPROCLL11(LlDesignerProhibitFunctionW,PFNLLDESIGNERPROHIBITFUNCTIONW,8);
  LOADDLLPROCLL11(LlPrintEnableObjectA,PFNLLPRINTENABLEOBJECTA,12);
  LOADDLLPROCLL11(LlPrintEnableObjectW,PFNLLPRINTENABLEOBJECTW,12);
  LOADDLLPROCLL11(LlSetFileExtensionsA,PFNLLSETFILEEXTENSIONSA,20);
  LOADDLLPROCLL11(LlSetFileExtensionsW,PFNLLSETFILEEXTENSIONSW,20);
  LOADDLLPROCLL11(LlPrintGetTextCharsPrintedA,PFNLLPRINTGETTEXTCHARSPRINTEDA,8);
  LOADDLLPROCLL11(LlPrintGetTextCharsPrintedW,PFNLLPRINTGETTEXTCHARSPRINTEDW,8);
  LOADDLLPROCLL11(LlPrintGetFieldCharsPrintedA,PFNLLPRINTGETFIELDCHARSPRINTEDA,12);
  LOADDLLPROCLL11(LlPrintGetFieldCharsPrintedW,PFNLLPRINTGETFIELDCHARSPRINTEDW,12);
  LOADDLLPROCLL11(LlPrintIsVariableUsedA,PFNLLPRINTISVARIABLEUSEDA,8);
  LOADDLLPROCLL11(LlPrintIsVariableUsedW,PFNLLPRINTISVARIABLEUSEDW,8);
  LOADDLLPROCLL11(LlPrintIsFieldUsedA,PFNLLPRINTISFIELDUSEDA,8);
  LOADDLLPROCLL11(LlPrintIsFieldUsedW,PFNLLPRINTISFIELDUSEDW,8);
  LOADDLLPROCLL11(LlPrintOptionsDialogTitleA,PFNLLPRINTOPTIONSDIALOGTITLEA,16);
  LOADDLLPROCLL11(LlPrintOptionsDialogTitleW,PFNLLPRINTOPTIONSDIALOGTITLEW,16);
  LOADDLLPROCLL11(LlSetPrinterToDefaultA,PFNLLSETPRINTERTODEFAULTA,12);
  LOADDLLPROCLL11(LlSetPrinterToDefaultW,PFNLLSETPRINTERTODEFAULTW,12);
  LOADDLLPROCLL11(LlDefineSortOrderStart,PFNLLDEFINESORTORDERSTART,4);
  LOADDLLPROCLL11(LlDefineSortOrderA,PFNLLDEFINESORTORDERA,12);
  LOADDLLPROCLL11(LlDefineSortOrderW,PFNLLDEFINESORTORDERW,12);
  LOADDLLPROCLL11(LlPrintGetSortOrderA,PFNLLPRINTGETSORTORDERA,12);
  LOADDLLPROCLL11(LlPrintGetSortOrderW,PFNLLPRINTGETSORTORDERW,12);
  LOADDLLPROCLL11(LlDefineGroupingA,PFNLLDEFINEGROUPINGA,16);
  LOADDLLPROCLL11(LlDefineGroupingW,PFNLLDEFINEGROUPINGW,16);
  LOADDLLPROCLL11(LlPrintGetGroupingA,PFNLLPRINTGETGROUPINGA,12);
  LOADDLLPROCLL11(LlPrintGetGroupingW,PFNLLPRINTGETGROUPINGW,12);
  LOADDLLPROCLL11(LlAddCtlSupportA,PFNLLADDCTLSUPPORTA,12);
  LOADDLLPROCLL11(LlAddCtlSupportW,PFNLLADDCTLSUPPORTW,12);
  LOADDLLPROCLL11(LlPrintBeginGroup,PFNLLPRINTBEGINGROUP,12);
  LOADDLLPROCLL11(LlPrintEndGroup,PFNLLPRINTENDGROUP,12);
  LOADDLLPROCLL11(LlPrintGroupLine,PFNLLPRINTGROUPLINE,12);
  LOADDLLPROCLL11(LlPrintGroupHeader,PFNLLPRINTGROUPHEADER,8);
  LOADDLLPROCLL11(LlPrintGetFilterExpressionA,PFNLLPRINTGETFILTEREXPRESSIONA,12);
  LOADDLLPROCLL11(LlPrintGetFilterExpressionW,PFNLLPRINTGETFILTEREXPRESSIONW,12);
  LOADDLLPROCLL11(LlPrintWillMatchFilter,PFNLLPRINTWILLMATCHFILTER,4);
  LOADDLLPROCLL11(LlPrintDidMatchFilter,PFNLLPRINTDIDMATCHFILTER,4);
  LOADDLLPROCLL11(LlGetFieldContentsA,PFNLLGETFIELDCONTENTSA,16);
  LOADDLLPROCLL11(LlGetFieldContentsW,PFNLLGETFIELDCONTENTSW,16);
  LOADDLLPROCLL11(LlGetVariableContentsA,PFNLLGETVARIABLECONTENTSA,16);
  LOADDLLPROCLL11(LlGetVariableContentsW,PFNLLGETVARIABLECONTENTSW,16);
  LOADDLLPROCLL11(LlGetSumVariableContentsA,PFNLLGETSUMVARIABLECONTENTSA,16);
  LOADDLLPROCLL11(LlGetSumVariableContentsW,PFNLLGETSUMVARIABLECONTENTSW,16);
  LOADDLLPROCLL11(LlGetUserVariableContentsA,PFNLLGETUSERVARIABLECONTENTSA,16);
  LOADDLLPROCLL11(LlGetUserVariableContentsW,PFNLLGETUSERVARIABLECONTENTSW,16);
  LOADDLLPROCLL11(LlGetVariableTypeA,PFNLLGETVARIABLETYPEA,8);
  LOADDLLPROCLL11(LlGetVariableTypeW,PFNLLGETVARIABLETYPEW,8);
  LOADDLLPROCLL11(LlGetFieldTypeA,PFNLLGETFIELDTYPEA,8);
  LOADDLLPROCLL11(LlGetFieldTypeW,PFNLLGETFIELDTYPEW,8);
  LOADDLLPROCLL11(LlPrintGetColumnInfoA,PFNLLPRINTGETCOLUMNINFOA,16);
  LOADDLLPROCLL11(LlPrintGetColumnInfoW,PFNLLPRINTGETCOLUMNINFOW,16);
  LOADDLLPROCLL11(LlSetPrinterDefaultsDirA,PFNLLSETPRINTERDEFAULTSDIRA,8);
  LOADDLLPROCLL11(LlSetPrinterDefaultsDirW,PFNLLSETPRINTERDEFAULTSDIRW,8);
  LOADDLLPROCLL11(LlCreateSketchA,PFNLLCREATESKETCHA,12);
  LOADDLLPROCLL11(LlCreateSketchW,PFNLLCREATESKETCHW,12);
  LOADDLLPROCLL11(LlViewerProhibitAction,PFNLLVIEWERPROHIBITACTION,8);
  LOADDLLPROCLL11(LlPrintCopyPrinterConfigurationA,PFNLLPRINTCOPYPRINTERCONFIGURATIONA,12);
  LOADDLLPROCLL11(LlPrintCopyPrinterConfigurationW,PFNLLPRINTCOPYPRINTERCONFIGURATIONW,12);
  LOADDLLPROCLL11(LlSetPrinterInPrinterFileA,PFNLLSETPRINTERINPRINTERFILEA,24);
  LOADDLLPROCLL11(LlSetPrinterInPrinterFileW,PFNLLSETPRINTERINPRINTERFILEW,24);
  LOADDLLPROCLL11(LlRTFCreateObject,PFNLLRTFCREATEOBJECT,4);
  LOADDLLPROCLL11(LlRTFDeleteObject,PFNLLRTFDELETEOBJECT,8);
  LOADDLLPROCLL11(LlRTFSetTextA,PFNLLRTFSETTEXTA,12);
  LOADDLLPROCLL11(LlRTFSetTextW,PFNLLRTFSETTEXTW,12);
  LOADDLLPROCLL11(LlRTFGetTextLength,PFNLLRTFGETTEXTLENGTH,12);
  LOADDLLPROCLL11(LlRTFGetTextA,PFNLLRTFGETTEXTA,20);
  LOADDLLPROCLL11(LlRTFGetTextW,PFNLLRTFGETTEXTW,20);
  LOADDLLPROCLL11(LlRTFEditObject,PFNLLRTFEDITOBJECT,24);
  LOADDLLPROCLL11(LlRTFCopyToClipboard,PFNLLRTFCOPYTOCLIPBOARD,8);
  LOADDLLPROCLL11(LlRTFDisplay,PFNLLRTFDISPLAY,24);
  LOADDLLPROCLL11(LlRTFEditorProhibitAction,PFNLLRTFEDITORPROHIBITACTION,12);
  LOADDLLPROCLL11(LlRTFEditorInvokeAction,PFNLLRTFEDITORINVOKEACTION,12);
  LOADDLLPROCLL11(LlDebugOutputA,PFNLLDEBUGOUTPUTA,8);
  LOADDLLPROCLL11(LlDebugOutputW,PFNLLDEBUGOUTPUTW,8);
  LOADDLLPROCLL11(LlEnumGetFirstVar,PFNLLENUMGETFIRSTVAR,8);
  LOADDLLPROCLL11(LlEnumGetFirstField,PFNLLENUMGETFIRSTFIELD,8);
  LOADDLLPROCLL11(LlEnumGetNextEntry,PFNLLENUMGETNEXTENTRY,12);
  LOADDLLPROCLL11(LlEnumGetEntryA,PFNLLENUMGETENTRYA,32);
  LOADDLLPROCLL11(LlEnumGetEntryW,PFNLLENUMGETENTRYW,32);
  LOADDLLPROCLL11(LlPrintResetObjectStates,PFNLLPRINTRESETOBJECTSTATES,4);
  LOADDLLPROCLL11(LlXSetParameterA,PFNLLXSETPARAMETERA,20);
  LOADDLLPROCLL11(LlXSetParameterW,PFNLLXSETPARAMETERW,20);
  LOADDLLPROCLL11(LlXGetParameterA,PFNLLXGETPARAMETERA,24);
  LOADDLLPROCLL11(LlXGetParameterW,PFNLLXGETPARAMETERW,24);
  LOADDLLPROCLL11(LlPrintResetProjectState,PFNLLPRINTRESETPROJECTSTATE,4);
  LOADDLLPROCLL11(LlDefineChartFieldStart,PFNLLDEFINECHARTFIELDSTART,4);
  LOADDLLPROCLL11(LlDefineChartFieldExtA,PFNLLDEFINECHARTFIELDEXTA,20);
  LOADDLLPROCLL11(LlDefineChartFieldExtW,PFNLLDEFINECHARTFIELDEXTW,20);
  LOADDLLPROCLL11(LlPrintDeclareChartRow,PFNLLPRINTDECLARECHARTROW,8);
  LOADDLLPROCLL11(LlPrintGetChartObjectCount,PFNLLPRINTGETCHARTOBJECTCOUNT,8);
  LOADDLLPROCLL11(LlPrintIsChartFieldUsedA,PFNLLPRINTISCHARTFIELDUSEDA,8);
  LOADDLLPROCLL11(LlPrintIsChartFieldUsedW,PFNLLPRINTISCHARTFIELDUSEDW,8);
  LOADDLLPROCLL11(LlGetChartFieldContentsA,PFNLLGETCHARTFIELDCONTENTSA,16);
  LOADDLLPROCLL11(LlGetChartFieldContentsW,PFNLLGETCHARTFIELDCONTENTSW,16);
  LOADDLLPROCLL11(LlEnumGetFirstChartField,PFNLLENUMGETFIRSTCHARTFIELD,8);
  LOADDLLPROCLL11(LlSetNotificationCallbackExt,PFNLLSETNOTIFICATIONCALLBACKEXT,12);
  LOADDLLPROCLL11(LlExprEvaluateVar,PFNLLEXPREVALUATEVAR,12);
  LOADDLLPROCLL11(LlExprTypeVar,PFNLLEXPRTYPEVAR,8);
  LOADDLLPROCLL11(LlGetPrinterFromPrinterFileA,PFNLLGETPRINTERFROMPRINTERFILEA,32);
  LOADDLLPROCLL11(LlGetPrinterFromPrinterFileW,PFNLLGETPRINTERFROMPRINTERFILEW,32);
  LOADDLLPROCLL11(LlPrintGetRemainingSpacePerTableA,PFNLLPRINTGETREMAININGSPACEPERTABLEA,12);
  LOADDLLPROCLL11(LlPrintGetRemainingSpacePerTableW,PFNLLPRINTGETREMAININGSPACEPERTABLEW,12);
  LOADDLLPROCLL11(LlDrawToolbarBackground,PFNLLDRAWTOOLBARBACKGROUND,16);
  LOADDLLPROCLL11(LlSetDefaultProjectParameterA,PFNLLSETDEFAULTPROJECTPARAMETERA,16);
  LOADDLLPROCLL11(LlSetDefaultProjectParameterW,PFNLLSETDEFAULTPROJECTPARAMETERW,16);
  LOADDLLPROCLL11(LlGetDefaultProjectParameterA,PFNLLGETDEFAULTPROJECTPARAMETERA,20);
  LOADDLLPROCLL11(LlGetDefaultProjectParameterW,PFNLLGETDEFAULTPROJECTPARAMETERW,20);
  LOADDLLPROCLL11(LlPrintSetProjectParameterA,PFNLLPRINTSETPROJECTPARAMETERA,16);
  LOADDLLPROCLL11(LlPrintSetProjectParameterW,PFNLLPRINTSETPROJECTPARAMETERW,16);
  LOADDLLPROCLL11(LlPrintGetProjectParameterA,PFNLLPRINTGETPROJECTPARAMETERA,24);
  LOADDLLPROCLL11(LlPrintGetProjectParameterW,PFNLLPRINTGETPROJECTPARAMETERW,24);
  LOADDLLPROCLL11(LlCreateObject,PFNLLCREATEOBJECT,8);
  LOADDLLPROCLL11(LlExprContainsVariableA,PFNLLEXPRCONTAINSVARIABLEA,12);
  LOADDLLPROCLL11(LlExprContainsVariableW,PFNLLEXPRCONTAINSVARIABLEW,12);
  LOADDLLPROCLL11(LlExprIsConstant,PFNLLEXPRISCONSTANT,8);
  LOADDLLPROCLL11(LlLocSystemTimeFromLocaleStringA,PFNLLLOCSYSTEMTIMEFROMLOCALESTRINGA,16);
  LOADDLLPROCLL11(LlLocSystemTimeFromLocaleStringW,PFNLLLOCSYSTEMTIMEFROMLOCALESTRINGW,16);
  LOADDLLPROCLL11(LlLocSystemTimeToLocaleStringA,PFNLLLOCSYSTEMTIMETOLOCALESTRINGA,16);
  LOADDLLPROCLL11(LlLocSystemTimeToLocaleStringW,PFNLLLOCSYSTEMTIMETOLOCALESTRINGW,16);
  LOADDLLPROCLL11(LlLocConvertCountryToA,PFNLLLOCCONVERTCOUNTRYTOA,20);
  LOADDLLPROCLL11(LlLocConvertCountryToW,PFNLLLOCCONVERTCOUNTRYTOW,20);
  LOADDLLPROCLL11(LlProfileStartA,PFNLLPROFILESTARTA,16);
  LOADDLLPROCLL11(LlProfileStartW,PFNLLPROFILESTARTW,16);
  LOADDLLPROCLL11(LlProfileEnd,PFNLLPROFILEEND,4);
  LOADDLLPROCLL11(LlDumpMemory,PFNLLDUMPMEMORY,4);
  LOADDLLPROCLL11(LlDbAddTableA,PFNLLDBADDTABLEA,12);
  LOADDLLPROCLL11(LlDbAddTableW,PFNLLDBADDTABLEW,12);
  LOADDLLPROCLL11(LlDbAddTableRelationA,PFNLLDBADDTABLERELATIONA,20);
  LOADDLLPROCLL11(LlDbAddTableRelationW,PFNLLDBADDTABLERELATIONW,20);
  LOADDLLPROCLL11(LlDbAddTableSortOrderA,PFNLLDBADDTABLESORTORDERA,16);
  LOADDLLPROCLL11(LlDbAddTableSortOrderW,PFNLLDBADDTABLESORTORDERW,16);
  LOADDLLPROCLL11(LlPrintDbGetCurrentTableA,PFNLLPRINTDBGETCURRENTTABLEA,16);
  LOADDLLPROCLL11(LlPrintDbGetCurrentTableW,PFNLLPRINTDBGETCURRENTTABLEW,16);
  LOADDLLPROCLL11(LlPrintDbGetCurrentTableRelationA,PFNLLPRINTDBGETCURRENTTABLERELATIONA,12);
  LOADDLLPROCLL11(LlPrintDbGetCurrentTableRelationW,PFNLLPRINTDBGETCURRENTTABLERELATIONW,12);
  LOADDLLPROCLL11(LlPrintDbGetCurrentTableSortOrderA,PFNLLPRINTDBGETCURRENTTABLESORTORDERA,12);
  LOADDLLPROCLL11(LlPrintDbGetCurrentTableSortOrderW,PFNLLPRINTDBGETCURRENTTABLESORTORDERW,12);
  LOADDLLPROCLL11(LlDbDumpStructure,PFNLLDBDUMPSTRUCTURE,4);
  LOADDLLPROCLL11(LlPrintDbGetRootTableCount,PFNLLPRINTDBGETROOTTABLECOUNT,4);
  LOADDLLPROCLL11(LlDbSetMasterTableA,PFNLLDBSETMASTERTABLEA,8);
  LOADDLLPROCLL11(LlDbSetMasterTableW,PFNLLDBSETMASTERTABLEW,8);
  LOADDLLPROCLL11(LlDbGetMasterTableA,PFNLLDBGETMASTERTABLEA,12);
  LOADDLLPROCLL11(LlDbGetMasterTableW,PFNLLDBGETMASTERTABLEW,12);
  LOADDLLPROCLL11(LlXSetExportParameterA,PFNLLXSETEXPORTPARAMETERA,16);
  LOADDLLPROCLL11(LlXSetExportParameterW,PFNLLXSETEXPORTPARAMETERW,16);
  LOADDLLPROCLL11(LlXGetExportParameterA,PFNLLXGETEXPORTPARAMETERA,20);
  LOADDLLPROCLL11(LlXGetExportParameterW,PFNLLXGETEXPORTPARAMETERW,20);
  LOADDLLPROCLL11(LlXlatNameA,PFNLLXLATNAMEA,16);
  LOADDLLPROCLL11(LlXlatNameW,PFNLLXLATNAMEW,16);
  LOADDLLPROCLL11(LlDefineVariableVarA,PFNLLDEFINEVARIABLEVARA,20);
  LOADDLLPROCLL11(LlDefineVariableVarW,PFNLLDEFINEVARIABLEVARW,20);
  LOADDLLPROCLL11(LlDefineFieldVarA,PFNLLDEFINEFIELDVARA,20);
  LOADDLLPROCLL11(LlDefineFieldVarW,PFNLLDEFINEFIELDVARW,20);
  LOADDLLPROCLL11(LlDefineChartFieldVarA,PFNLLDEFINECHARTFIELDVARA,20);
  LOADDLLPROCLL11(LlDefineChartFieldVarW,PFNLLDEFINECHARTFIELDVARW,20);
  LOADDLLPROCLL11(LlDesignerProhibitEditingObjectA,PFNLLDESIGNERPROHIBITEDITINGOBJECTA,8);
  LOADDLLPROCLL11(LlDesignerProhibitEditingObjectW,PFNLLDESIGNERPROHIBITEDITINGOBJECTW,8);
  LOADDLLPROCLL11(LlGetUsedIdentifiersA,PFNLLGETUSEDIDENTIFIERSA,16);
  LOADDLLPROCLL11(LlGetUsedIdentifiersW,PFNLLGETUSEDIDENTIFIERSW,16);
  LOADDLLPROCLL11(LlInternalAttachApp,PFNLLINTERNALATTACHAPP,8);
  LOADDLLPROCLL11(LlInternalDetachApp,PFNLLINTERNALDETACHAPP,8);
  LOADDLLPROCLL11(LlGetUserSectionDataA,PFNLLGETUSERSECTIONDATAA,12);
  LOADDLLPROCLL11(LlGetUserSectionDataW,PFNLLGETUSERSECTIONDATAW,12);
  return(0);
}

void LL11xUnload(void)
{
  if (--nDLLLL11Counter > 0)
    return;

#ifdef WIN32
  if (hDLLLL11 != NULL)
#else
  if (hDLLLL11 >= HINSTANCE_ERROR)
#endif
    {
    FreeLibrary(hDLLLL11);
    hDLLLL11 = NULL;
    LlJobOpen = NULL;
    LlJobOpenLCID = NULL;
    LlJobClose = NULL;
    LlSetDebug = NULL;
    LlGetVersion = NULL;
    LlGetNotificationMessage = NULL;
    LlSetNotificationMessage = NULL;
    LlSetNotificationCallback = NULL;
    LlDefineFieldA = NULL;
    LlDefineFieldW = NULL;
    LlDefineFieldExtA = NULL;
    LlDefineFieldExtW = NULL;
    LlDefineFieldExtHandleA = NULL;
    LlDefineFieldExtHandleW = NULL;
    LlDefineFieldStart = NULL;
    LlDefineVariableA = NULL;
    LlDefineVariableW = NULL;
    LlDefineVariableExtA = NULL;
    LlDefineVariableExtW = NULL;
    LlDefineVariableExtHandleA = NULL;
    LlDefineVariableExtHandleW = NULL;
    LlDefineVariableNameA = NULL;
    LlDefineVariableNameW = NULL;
    LlDefineVariableStart = NULL;
    LlDefineSumVariableA = NULL;
    LlDefineSumVariableW = NULL;
    LlDefineLayoutA = NULL;
    LlDefineLayoutW = NULL;
    LlDlgEditLineA = NULL;
    LlDlgEditLineW = NULL;
    LlDlgEditLineExA = NULL;
    LlDlgEditLineExW = NULL;
    LlPreviewSetTempPathA = NULL;
    LlPreviewSetTempPathW = NULL;
    LlPreviewDeleteFilesA = NULL;
    LlPreviewDeleteFilesW = NULL;
    LlPreviewDisplayA = NULL;
    LlPreviewDisplayW = NULL;
    LlPreviewDisplayExA = NULL;
    LlPreviewDisplayExW = NULL;
    LlPrint = NULL;
    LlPrintAbort = NULL;
    LlPrintCheckLineFit = NULL;
    LlPrintEnd = NULL;
    LlPrintFields = NULL;
    LlPrintFieldsEnd = NULL;
    LlPrintGetCurrentPage = NULL;
    LlPrintGetItemsPerPage = NULL;
    LlPrintGetItemsPerTable = NULL;
    LlPrintGetRemainingItemsPerTableA = NULL;
    LlPrintGetRemainingItemsPerTableW = NULL;
    LlPrintGetRemItemsPerTableA = NULL;
    LlPrintGetRemItemsPerTableW = NULL;
    LlPrintGetOption = NULL;
    LlPrintGetPrinterInfoA = NULL;
    LlPrintGetPrinterInfoW = NULL;
    LlPrintOptionsDialogA = NULL;
    LlPrintOptionsDialogW = NULL;
    LlPrintSelectOffsetEx = NULL;
    LlPrintSetBoxTextA = NULL;
    LlPrintSetBoxTextW = NULL;
    LlPrintSetOption = NULL;
    LlPrintUpdateBox = NULL;
    LlPrintStartA = NULL;
    LlPrintStartW = NULL;
    LlPrintWithBoxStartA = NULL;
    LlPrintWithBoxStartW = NULL;
    LlPrinterSetupA = NULL;
    LlPrinterSetupW = NULL;
    LlSelectFileDlgTitleExA = NULL;
    LlSelectFileDlgTitleExW = NULL;
    LlSetDlgboxMode = NULL;
    LlGetDlgboxMode = NULL;
    LlExprParseA = NULL;
    LlExprParseW = NULL;
    LlExprType = NULL;
    LlExprErrorA = NULL;
    LlExprErrorW = NULL;
    LlExprFree = NULL;
    LlExprEvaluateA = NULL;
    LlExprEvaluateW = NULL;
    LlExprGetUsedVarsA = NULL;
    LlExprGetUsedVarsW = NULL;
    LlSetOption = NULL;
    LlGetOption = NULL;
    LlSetOptionStringA = NULL;
    LlSetOptionStringW = NULL;
    LlGetOptionStringA = NULL;
    LlGetOptionStringW = NULL;
    LlPrintSetOptionStringA = NULL;
    LlPrintSetOptionStringW = NULL;
    LlPrintGetOptionStringA = NULL;
    LlPrintGetOptionStringW = NULL;
    LlDesignerProhibitAction = NULL;
    LlDesignerProhibitFunctionA = NULL;
    LlDesignerProhibitFunctionW = NULL;
    LlPrintEnableObjectA = NULL;
    LlPrintEnableObjectW = NULL;
    LlSetFileExtensionsA = NULL;
    LlSetFileExtensionsW = NULL;
    LlPrintGetTextCharsPrintedA = NULL;
    LlPrintGetTextCharsPrintedW = NULL;
    LlPrintGetFieldCharsPrintedA = NULL;
    LlPrintGetFieldCharsPrintedW = NULL;
    LlPrintIsVariableUsedA = NULL;
    LlPrintIsVariableUsedW = NULL;
    LlPrintIsFieldUsedA = NULL;
    LlPrintIsFieldUsedW = NULL;
    LlPrintOptionsDialogTitleA = NULL;
    LlPrintOptionsDialogTitleW = NULL;
    LlSetPrinterToDefaultA = NULL;
    LlSetPrinterToDefaultW = NULL;
    LlDefineSortOrderStart = NULL;
    LlDefineSortOrderA = NULL;
    LlDefineSortOrderW = NULL;
    LlPrintGetSortOrderA = NULL;
    LlPrintGetSortOrderW = NULL;
    LlDefineGroupingA = NULL;
    LlDefineGroupingW = NULL;
    LlPrintGetGroupingA = NULL;
    LlPrintGetGroupingW = NULL;
    LlAddCtlSupportA = NULL;
    LlAddCtlSupportW = NULL;
    LlPrintBeginGroup = NULL;
    LlPrintEndGroup = NULL;
    LlPrintGroupLine = NULL;
    LlPrintGroupHeader = NULL;
    LlPrintGetFilterExpressionA = NULL;
    LlPrintGetFilterExpressionW = NULL;
    LlPrintWillMatchFilter = NULL;
    LlPrintDidMatchFilter = NULL;
    LlGetFieldContentsA = NULL;
    LlGetFieldContentsW = NULL;
    LlGetVariableContentsA = NULL;
    LlGetVariableContentsW = NULL;
    LlGetSumVariableContentsA = NULL;
    LlGetSumVariableContentsW = NULL;
    LlGetUserVariableContentsA = NULL;
    LlGetUserVariableContentsW = NULL;
    LlGetVariableTypeA = NULL;
    LlGetVariableTypeW = NULL;
    LlGetFieldTypeA = NULL;
    LlGetFieldTypeW = NULL;
    LlPrintGetColumnInfoA = NULL;
    LlPrintGetColumnInfoW = NULL;
    LlSetPrinterDefaultsDirA = NULL;
    LlSetPrinterDefaultsDirW = NULL;
    LlCreateSketchA = NULL;
    LlCreateSketchW = NULL;
    LlViewerProhibitAction = NULL;
    LlPrintCopyPrinterConfigurationA = NULL;
    LlPrintCopyPrinterConfigurationW = NULL;
    LlSetPrinterInPrinterFileA = NULL;
    LlSetPrinterInPrinterFileW = NULL;
    LlRTFCreateObject = NULL;
    LlRTFDeleteObject = NULL;
    LlRTFSetTextA = NULL;
    LlRTFSetTextW = NULL;
    LlRTFGetTextLength = NULL;
    LlRTFGetTextA = NULL;
    LlRTFGetTextW = NULL;
    LlRTFEditObject = NULL;
    LlRTFCopyToClipboard = NULL;
    LlRTFDisplay = NULL;
    LlRTFEditorProhibitAction = NULL;
    LlRTFEditorInvokeAction = NULL;
    LlDebugOutputA = NULL;
    LlDebugOutputW = NULL;
    LlEnumGetFirstVar = NULL;
    LlEnumGetFirstField = NULL;
    LlEnumGetNextEntry = NULL;
    LlEnumGetEntryA = NULL;
    LlEnumGetEntryW = NULL;
    LlPrintResetObjectStates = NULL;
    LlXSetParameterA = NULL;
    LlXSetParameterW = NULL;
    LlXGetParameterA = NULL;
    LlXGetParameterW = NULL;
    LlPrintResetProjectState = NULL;
    LlDefineChartFieldStart = NULL;
    LlDefineChartFieldExtA = NULL;
    LlDefineChartFieldExtW = NULL;
    LlPrintDeclareChartRow = NULL;
    LlPrintGetChartObjectCount = NULL;
    LlPrintIsChartFieldUsedA = NULL;
    LlPrintIsChartFieldUsedW = NULL;
    LlGetChartFieldContentsA = NULL;
    LlGetChartFieldContentsW = NULL;
    LlEnumGetFirstChartField = NULL;
    LlSetNotificationCallbackExt = NULL;
    LlExprEvaluateVar = NULL;
    LlExprTypeVar = NULL;
    LlGetPrinterFromPrinterFileA = NULL;
    LlGetPrinterFromPrinterFileW = NULL;
    LlPrintGetRemainingSpacePerTableA = NULL;
    LlPrintGetRemainingSpacePerTableW = NULL;
    LlDrawToolbarBackground = NULL;
    LlSetDefaultProjectParameterA = NULL;
    LlSetDefaultProjectParameterW = NULL;
    LlGetDefaultProjectParameterA = NULL;
    LlGetDefaultProjectParameterW = NULL;
    LlPrintSetProjectParameterA = NULL;
    LlPrintSetProjectParameterW = NULL;
    LlPrintGetProjectParameterA = NULL;
    LlPrintGetProjectParameterW = NULL;
    LlCreateObject = NULL;
    LlExprContainsVariableA = NULL;
    LlExprContainsVariableW = NULL;
    LlExprIsConstant = NULL;
    LlLocSystemTimeFromLocaleStringA = NULL;
    LlLocSystemTimeFromLocaleStringW = NULL;
    LlLocSystemTimeToLocaleStringA = NULL;
    LlLocSystemTimeToLocaleStringW = NULL;
    LlLocConvertCountryToA = NULL;
    LlLocConvertCountryToW = NULL;
    LlProfileStartA = NULL;
    LlProfileStartW = NULL;
    LlProfileEnd = NULL;
    LlDumpMemory = NULL;
    LlDbAddTableA = NULL;
    LlDbAddTableW = NULL;
    LlDbAddTableRelationA = NULL;
    LlDbAddTableRelationW = NULL;
    LlDbAddTableSortOrderA = NULL;
    LlDbAddTableSortOrderW = NULL;
    LlPrintDbGetCurrentTableA = NULL;
    LlPrintDbGetCurrentTableW = NULL;
    LlPrintDbGetCurrentTableRelationA = NULL;
    LlPrintDbGetCurrentTableRelationW = NULL;
    LlPrintDbGetCurrentTableSortOrderA = NULL;
    LlPrintDbGetCurrentTableSortOrderW = NULL;
    LlDbDumpStructure = NULL;
    LlPrintDbGetRootTableCount = NULL;
    LlDbSetMasterTableA = NULL;
    LlDbSetMasterTableW = NULL;
    LlDbGetMasterTableA = NULL;
    LlDbGetMasterTableW = NULL;
    LlXSetExportParameterA = NULL;
    LlXSetExportParameterW = NULL;
    LlXGetExportParameterA = NULL;
    LlXGetExportParameterW = NULL;
    LlXlatNameA = NULL;
    LlXlatNameW = NULL;
    LlDefineVariableVarA = NULL;
    LlDefineVariableVarW = NULL;
    LlDefineFieldVarA = NULL;
    LlDefineFieldVarW = NULL;
    LlDefineChartFieldVarA = NULL;
    LlDefineChartFieldVarW = NULL;
    LlDesignerProhibitEditingObjectA = NULL;
    LlDesignerProhibitEditingObjectW = NULL;
    LlGetUsedIdentifiersA = NULL;
    LlGetUsedIdentifiersW = NULL;
    LlInternalAttachApp = NULL;
    LlInternalDetachApp = NULL;
    LlGetUserSectionDataA = NULL;
    LlGetUserSectionDataW = NULL;
    }
}
#endif

#ifdef __cplusplus
}
#endif

#ifdef __ZTC__ /* Zortech C++ */
#pragma ZTC align
#elif __WATCOMC__ > 1000 /* Watcom C++ */
#pragma pack(pop)
#elif __BORLANDC__ /* Borland C++ */
#pragma option -a.
#else
#pragma pack() /* MS C++ */
#endif

#ifdef IMPLEMENTATION
#undef extern
#endif

#endif  /* #ifndef _RC_INVOKED_ */

#endif  /* #ifndef _LL11_H */

