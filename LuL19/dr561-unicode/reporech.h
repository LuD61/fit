#ifndef _REPORECH_DEF
#define _REPORECH_DEF

struct REPORZU
{
 short order2 ;
 char 	typ[3] ;
 char  gruppe[7] ;
 char  wertart[2] ;
 double me ;
 double faktor ;
 char  rab_bz [37] ;
 double geswert ;

 TCHAR uctyp[3] ;
 TCHAR ucgruppe[7] ;
 TCHAR ucwertart[2] ;
 TCHAR ucrab_bz [37] ;
 
};

extern struct REPORZU reporzu, reporzu_null, reporzu1, reporzu2, reporzu3 , reporzu4, reporzu5,reporzu6 ;	// 260612 +4+5+6


class REPORZU_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int lesereporzu (void);
               int openreporzu (void);
               REPORZU_CLASS () : DB_CLASS ()
               {
               }
};

struct REPORECH {
 double a;
 char  a_bz1[25];
 char  a_bz2[25];
 TCHAR  uca_bz1[25];
 TCHAR  uca_bz2[25];
 long lpos_txt;
 long kopf_txt;
 long fuss_txt;
 char  a_kun[14];
 TCHAR  uca_kun[14];
 char  zu_stoff[2];
 TCHAR  uczu_stoff[2];
 double gn_pkt_gbr;
 long adr2;
 long adr3;
 double snetto1;
 double snetto2;
 double snetto3;
 double smwst1;
 double smwst2;
 double  smwst3;
 char  smwst1p[7];
 char  smwst2p[7];
 char  smwst3p[7];

 TCHAR ucsmwst1p[7];
 TCHAR ucsmwst2p[7];
 TCHAR ucsmwst3p[7];

 short smwst1s;
 short smwst2s;
 short smwst3s;
 double rech_summ;
 double end_rab;
 double zahl_betr;
 long fil;
 char  belk_txt[81];	// 61->81 300106
 char  belf_txt[81];	// 61->81 300106 
 char  zako_txt[81];	// 61->81 300106
 char  pmwsts[3];

 TCHAR  ucbelk_txt[81];	// 61->81 300106
 TCHAR  ucbelf_txt[81];	// 61->81 300106 
 TCHAR  uczako_txt[81];	// 61->81 300106
 TCHAR  ucpmwsts[3];
 long kun;
 double ges_rabp;
 long mdn;
 char  ust_id[17];
 char  nr_bei_rech[17];
 TCHAR  ucust_id[17];
 TCHAR  ucnr_bei_rech[17];
 double alt_pr;
 char  lsret[2];
 char  auf_ext[31];	// 270212 17->31
 short a_typ;
 char  kun_nam[46];
 char  bbn[17];
 long mdnadr;
 char  rech_dat[12];

 TCHAR  uclsret[2];
 TCHAR  ucauf_ext[31];
 TCHAR  uckun_nam[46];
 TCHAR  ucbbn[17];
 TCHAR  ucrech_dat[12];


 short order3;
 long rech_nr;
 char  erf_kz[2];
 double lief_me;
 char  lief_me_bz[6];
 double vk_pr;
 double auf_me;
 char  sa_kz[2];
 short order2;
 double einz_rabp;
 double zeil_sum;
 double prab_wert;
 long rpos_txt;
 long ktx_jebel;
 long ftx_jebel;
 short lief_art;
 char  tel[21];
 char  fax[21];
 char  auf_me_bz[7];
 TCHAR  uctel[21];
 TCHAR  ucfax[21];
 TCHAR  ucauf_me_bz[7];
 TCHAR  uclief_me_bz[6];
 TCHAR  ucsa_kz[2];
 TCHAR  ucerf_kz[2];
 TCHAR  ucls_charge[31];

 double lief_me1;
 char  blg_typ[2];
 char  frm_name[6];
 TCHAR  ucfrm_name[6];
 char  akv[11];
 char  ls_charge[31];
 long lsnr;	// 150605
 long txt_rech;	// 101014

};

extern struct REPORECH reporech, reporech_null;


class REPORECH_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int dbcount (void);
               int lesereporech (int);
               int openreporech (void);
//               void GetVerarbInfo (void);
               REPORECH_CLASS () : DB_CLASS ()
               {
               }
};
#endif

