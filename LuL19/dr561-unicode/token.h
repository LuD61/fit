#ifndef TOKEN_DEF
#define TOKEN_DEF
#include <string.h> 
#include <stdarg.h> 

class CToken
{
  private :
     CString Buffer;
     CString **Tokens;
     CString sep;
     int AnzToken;
     int AktToken;
  public :			// unicode : diverse char-> TCHAR ....
     CToken ();
     CToken (TCHAR *, TCHAR *);
     CToken (CString&, TCHAR *);
     ~CToken ();
     const CToken& operator=(TCHAR *);
     const CToken& operator=(CString&);
     void GetTokens (TCHAR *);
     void SetSep (TCHAR *);
     TCHAR * NextToken (void);
     TCHAR * GetToken (int);
     int GetAnzToken (void);
};
#endif
