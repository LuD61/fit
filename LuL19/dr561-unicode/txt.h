#ifndef _TXT_DEF
#define _TXT_DEF

struct PTXT {
long nr;
long zei;
char txt[61];
TCHAR uctxt[61];


};
extern struct PTXT lspt, lspt_null;
extern struct PTXT aufpt, aufpt_null;	// 130707
extern struct PTXT angpt, angpt_null;	// 130707
extern struct PTXT retpt, retpt_null;
extern struct PTXT ls_txt, ls_txt_null;


class LSPT_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int dbcount (void);
               int leselspt (int);
               int openlspt (void);
//               void GetVerarbInfo (void);
               LSPT_CLASS () : DB_CLASS ()
               {
               }
};

class AUFPT_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int dbcount (void);
               int leseaufpt (int);
               int openaufpt (void);
               AUFPT_CLASS () : DB_CLASS ()
               {
               }
};

class ANGPT_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int dbcount (void);
               int leseangpt (int);
               int openangpt (void);
               ANGPT_CLASS () : DB_CLASS ()
               {
               }
};

class RETPT_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int dbcount (void);
               int leseretpt (int);
               int openretpt (void);
//               void GetVerarbInfo (void);
               RETPT_CLASS () : DB_CLASS ()
               {
               }
};


class LS_TXT_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int dbcount (void);
               int lesels_txt (int);
               int openls_txt (void);
//               void GetVerarbInfo (void);
               LS_TXT_CLASS () : DB_CLASS ()
               {
               }
};

// 151014 A
struct AUFPT_VERKFRAGE {
// vermutlich gibt es da noch viel mehr mehr oder weniger Gammel-Felder in dieser tabelle. 
// die werden dann auf Anforderung unterstützt, da das hier alles relativ unausgegoren wirkt ....
long txt_ls1;
long txt_rech1;
long txt_ls2;
long txt_rech2;
long txt_ls3;
long txt_rech3;
long txt_ls4;
long txt_rech4;
long txt_ls5;
long txt_rech5;
long txt_ls6;
long txt_rech6;
long txt_ls7;
long txt_rech7;
long txt_ls8;
long txt_rech8;
long txt_ls9;
long txt_rech9;
long txt_ls10;
long txt_rech10;
long ls;
short mdn;
double a;
// 031114 dazu :
short def1;
short def2;
short def3;
short def4;
short def5;
short def6;
short def7;
short def8;
short def9;
short def10;

};
extern struct AUFPT_VERKFRAGE aufpt_verkfrage, aufpt_verkfrage_null;

class AUFPT_VERKFRAGE_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
		   ~AUFPT_VERKFRAGE_CLASS();
            int leseaufpt_verkfrage (short emdn , long els, double ea );
			char *ctxt_ls1;
			char *ctxt_rech1;
			char *ctxt_ls2;
			char *ctxt_rech2;
			char *ctxt_ls3;
			char *ctxt_rech3;
			char *ctxt_ls4;
			char *ctxt_rech4;
			char *ctxt_ls5;
			char *ctxt_rech5;
			char *ctxt_ls6;
			char *ctxt_rech6;
			char *ctxt_ls7;
			char *ctxt_rech7;
			char *ctxt_ls8;
			char *ctxt_rech8;
			char *ctxt_ls9;
			char *ctxt_rech9;
			char *ctxt_ls10;
			char *ctxt_rech10;

			TCHAR *ucctxt_ls1;
			TCHAR *ucctxt_rech1;
			TCHAR *ucctxt_ls2;
			TCHAR *ucctxt_rech2;
			TCHAR *ucctxt_ls3;
			TCHAR *ucctxt_rech3;
			TCHAR *ucctxt_ls4;
			TCHAR *ucctxt_rech4;
			TCHAR *ucctxt_ls5;
			TCHAR *ucctxt_rech5;
			TCHAR *ucctxt_ls6;
			TCHAR *ucctxt_rech6;
			TCHAR *ucctxt_ls7;
			TCHAR *ucctxt_rech7;
			TCHAR *ucctxt_ls8;
			TCHAR *ucctxt_rech8;
			TCHAR *ucctxt_ls9;
			TCHAR *ucctxt_rech9;
			TCHAR *ucctxt_ls10;
			TCHAR *ucctxt_rech10;

               AUFPT_VERKFRAGE_CLASS () : DB_CLASS ()
               {
			   }
} ;
// 151014 E


// 101014 A
struct ATEXTE {
    long sys;
    short waa;
    long txt_nr;
    char txt [1025];
    char disp_txt[1025];
    TCHAR uctxt [1025];
    TCHAR ucdisp_txt[1025];
	
	short alignment;
    short send_ctrl;
    short txt_typ;
    short txt_platz;
    double a;

};
extern struct ATEXTE atexte, atexte_null;

class ATEXTE_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int leseatexte (long);

               ATEXTE_CLASS () : DB_CLASS ()
               {
			   }
} ;

// 101014 E


#endif

