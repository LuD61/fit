#include "stdafx.h"
#include "DbClass.h"
#include "kun.h"
#ifdef LL19
#include "cmbtll19.h"
#else
#ifdef LL12
#include "cmbtll12.h"
#else
#include "cmbtll11.h"
#endif
#endif
struct KUN kun,  kun_null;

struct TOU tou, tou_save, tou_null;	// 301107 : Tour dazu fuer DFW


extern int dietzosep ;	// 231213

extern DB_CLASS dbClass;

// int itxt_nr ;

static int anzzfelder ;


int TOU_CLASS::lesetou (int fehlercode)
{
	  if (fehlercode == LL_WRN_REPEAT_DATA)
	  {
         return 0;    // Hier kein fetch , da DS nicht mehr auf Seite passt , und nochmal 
					  // ausgegeben werden mu�
	  }
      int di = dbClass.sqlfetch (readcursor);

	  if ( !di)
	  {
		  MultiByteToWideChar( 0,0, tou.tou_bz , -1 , tou.uctou_bz ,sizeof(tou.uctou_bz));

	  }

	  return di;
}
int TOU_CLASS::opentou (void)
{

		if ( readcursor < 0 ) prepare ();	// ob das hier richtig ist ?

         return dbClass.sqlopen (readcursor);
}
void TOU_CLASS::prepare (void)
{

test_upd_cursor = 1;

	dbClass.sqlin ((long *) &tou.tou, SQLLONG, 0);


	dbClass.sqlout (( char *) tou.tou_bz,SQLCHAR, 49 ) ;
/* ->
	dbClass.sqlout (( char *)  tou.fz_kla,SQLCHAR,3) ;
	dbClass.sqlout (( char *)  tou.fz,SQLCHAR,13) ; 
	dbClass.sqlout (( char *)  tou.srt_zeit,SQLCHAR,6 ) ;
	dbClass.sqlout (( char *)  tou.dau,SQLCHAR,6 ) ;
	dbClass.sqlout (( long *) &tou.lng,SQLLONG,0) ;
	dbClass.sqlout (( char *)  tou.fah_1[13];
	dbClass.sqlout (( char *)  tou.fah_2[13];
	dbClass.sqlout (( short *)&tou.delstatus;
	dbClass.sqlout (( long *) &tou.lgr;
	dbClass.sqlout (( long *) &tou.leitw_typ;
	dbClass.sqlout (( long *) &tou.htou;
	dbClass.sqlout (( long *) &tou.adr;
	dbClass.sqlout (( short *)&tou.eigentour;
	dbClass.sqlout (( long *) &tou.lgkonto;

< ----- */

	readcursor = dbClass.sqlcursor (_T("select ")

	_T(" tou_bz  ")

	_T(" from tou where tou = ?  ") ) ;
	
 }




int KUN_CLASS::dbcount (void)
/**
Tabelle kun lesen.
**/
{

         if (test_upd_cursor == -1)
         {
             prepare ();
         }
         dbClass.sqlopen (count_cursor);
         dbClass.sqlfetch (count_cursor);
         if (sqlstatus == 0)
         {
                      return anzzfelder;
         }
         return sqlstatus;

}




int KUN_CLASS::lesekun ()
{

	long savekun = kun.kun ;						// 211010 : sauberer initialisieren
	short savemdn = kun.mdn ;						// 211010 : sauberer initialisieren
	memcpy ( &kun,&kun_null, sizeof(struct KUN));	// 211010 : sauberer initialisieren 

	int di = dbClass.sqlfetch (readcursor);

	kun.kun = savekun ;						// 211010 : sauberer initialisieren
	kun.mdn = savemdn ;						// 211010 : sauberer initialisieren

	if ( !di)
	{
		  MultiByteToWideChar( 0,0, kun.bank_kun , -1 , kun.ucbank_kun ,sizeof(kun.ucbank_kun));
		  MultiByteToWideChar( 0,0, kun.kto_nr , -1 , kun.uckto_nr ,sizeof(kun.uckto_nr));
		  MultiByteToWideChar( 0,0, kun.kun_bran2 , -1 , kun.uckun_bran2 ,sizeof(kun.uckun_bran2));
		  MultiByteToWideChar( 0,0, kun.mandatref , -1 , kun.ucmandatref ,sizeof(kun.ucmandatref));
		  MultiByteToWideChar( 0,0, kun.ust_id16 , -1 , kun.ucust_id16 ,sizeof(kun.ucust_id16));
		  MultiByteToWideChar( 0,0, kun.kun_krz1 , -1 , kun.uckun_krz1 ,sizeof(kun.uckun_krz1));

		  MultiByteToWideChar( 0,0, kun.ust_nummer , -1 , kun.ucust_nummer ,sizeof(kun.ucust_nummer));
		  MultiByteToWideChar( 0,0, kun.fak_kz , -1 , kun.ucfak_kz ,sizeof(kun.ucfak_kz));

	}


	  return di;
}

int KUN_CLASS::openkun (void)
{

		if ( readcursor < 0 ) prepare ();	// ob das hier richtig ist ?

         return dbClass.sqlopen (readcursor);
}



void KUN_CLASS::prepare (void)
{


	dbClass.sqlin ((long *) &kun.kun, SQLLONG, 0);
	dbClass.sqlin ((short *) &kun.mdn, SQLSHORT, 0);
    dbClass.sqlout ((long *)  &anzzfelder, SQLLONG, 0);


    count_cursor = dbClass.sqlcursor (_T("select count(*) from kun ")
										_T("where kun.kun = ? and kun.mdn = ? "));
										
	test_upd_cursor = 1;


	dbClass.sqlin ((long *) &kun.kun, SQLLONG, 0);
	dbClass.sqlin ((short *) &kun.mdn, SQLSHORT, 0);


dbClass.sqlout ((short *) &kun.mdn, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.fil, SQLSHORT, 0);
dbClass.sqlout ((long *) &kun.kun, SQLLONG, 0);
dbClass.sqlout ((long *) &kun.adr1, SQLLONG, 0);
dbClass.sqlout ((long *) &kun.adr2, SQLLONG, 0);  
dbClass.sqlout ((long *) &kun.adr3, SQLLONG, 0); 
dbClass.sqlout ((char *) kun.kun_seit, SQLCHAR, 11);
dbClass.sqlout ((long *) &kun.txt_nr1, SQLLONG, 0);
dbClass.sqlout ((char *) kun.frei_txt1, SQLCHAR, 265);	// 300514

dbClass.sqlout ((char *) kun.kun_krz1, SQLCHAR, 17);
dbClass.sqlout ((char *) kun.kun_bran, SQLCHAR, 2);
dbClass.sqlout ((char *) kun.kun_krz2, SQLCHAR, 17);
dbClass.sqlout ((char *) kun.kun_krz3, SQLCHAR, 17);
dbClass.sqlout ((short *) &kun.kun_typ, SQLSHORT, 0);
dbClass.sqlout ((long *) &kun.bbn, SQLLONG, 0);
dbClass.sqlout ((short *) &kun.pr_stu, SQLSHORT, 0);
dbClass.sqlout ((long *) &kun.pr_lst, SQLLONG, 0);
dbClass.sqlout ((char *) kun.vereinb, SQLCHAR, 6);
dbClass.sqlout ((long *) &kun.inka_nr, SQLLONG, 0);
dbClass.sqlout ((long *) &kun.vertr1, SQLLONG, 0);
dbClass.sqlout ((long *) &kun.vertr2, SQLLONG, 0);
dbClass.sqlout ((char *) kun.statk_period, SQLCHAR, 2);
dbClass.sqlout ((char *) kun.a_period, SQLCHAR, 2);
dbClass.sqlout ((short *) &kun.sprache, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.sprache2, SQLSHORT, 0);	// 251114
dbClass.sqlout ((long *) &kun.txt_nr2, SQLLONG, 0);
dbClass.sqlout ((char *) kun.frei_txt2, SQLCHAR, 265);	// 300514
dbClass.sqlout ((char *) kun.freifeld1, SQLCHAR, 9);
dbClass.sqlout ((char *) kun.freifeld2, SQLCHAR, 9);
dbClass.sqlout ((long *) &kun.tou, SQLLONG, 0);
dbClass.sqlout ((char *) kun.vers_art, SQLCHAR, 3);
dbClass.sqlout ((short *) &kun.lief_art, SQLSHORT, 0);
dbClass.sqlout ((char *) kun.fra_ko_ber, SQLCHAR, 3);
dbClass.sqlout ((short *) &kun.rue_schei, SQLSHORT, 0);
dbClass.sqlout ((char *) kun.form_typ1, SQLCHAR, 3);
dbClass.sqlout ((short *) &kun.auflage1, SQLSHORT, 0);
dbClass.sqlout ((char *) kun.freifeld3, SQLCHAR, 9);
dbClass.sqlout ((char *) kun.freifeld4, SQLCHAR, 9);
dbClass.sqlout ((short *) &kun.zahl_art, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.zahl_ziel, SQLSHORT, 0);
dbClass.sqlout ((char *) kun.form_typ2, SQLCHAR, 3);
dbClass.sqlout ((short *) &kun.auflage2, SQLSHORT, 0);
dbClass.sqlout ((long *) &kun.txt_nr3, SQLLONG, 0);
dbClass.sqlout ((char *) kun.frei_txt3, SQLCHAR, 265);	// 300514
dbClass.sqlout ((char *) kun.nr_bei_rech, SQLCHAR, 17);
dbClass.sqlout ((char *) kun.rech_st, SQLCHAR, 3);
dbClass.sqlout ((short *) &kun.sam_rech, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.einz_ausw, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.gut, SQLSHORT, 0);
dbClass.sqlout ((char *) kun.rab_schl, SQLCHAR, 9);	// 031204 5->9

dbClass.sqlout ((double *) &kun.bonus1, SQLDOUBLE, 0);
dbClass.sqlout ((double *) &kun.bonus2, SQLDOUBLE, 0);
dbClass.sqlout ((double *) &kun.tdm_grenz1, SQLDOUBLE, 0);
dbClass.sqlout ((double *) &kun.tdm_grenz2, SQLDOUBLE, 0);
dbClass.sqlout ((double *) &kun.jr_plan_ums, SQLDOUBLE, 0);
dbClass.sqlout ((char *) kun.deb_kto, SQLCHAR, 9);
dbClass.sqlout ((double *) &kun.kred_lim, SQLDOUBLE, 0);
dbClass.sqlout ((short *) &kun.inka_zaehl, SQLSHORT, 0);
dbClass.sqlout ((char *) kun.bank_kun, SQLCHAR, 37);
dbClass.sqlout ((long *) &kun.blz, SQLLONG, 0);
dbClass.sqlout ((char *) kun.kto_nr, SQLCHAR, 17);
dbClass.sqlout ((short *) &kun.hausbank, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.kun_of_po, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.kun_of_lf, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.kun_of_best, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.delstatus, SQLSHORT, 0);
dbClass.sqlout ((char *) kun.kun_bran2, SQLCHAR, 3);
dbClass.sqlout ((long *) &kun.rech_fuss_txt, SQLLONG, 0);
dbClass.sqlout ((long *) &kun.ls_fuss_txt, SQLLONG, 0);
dbClass.sqlout ((char *) kun.ust_id, SQLCHAR, 12);
dbClass.sqlout ((long *) &kun.rech_kopf_txt, SQLLONG, 0);
dbClass.sqlout ((long *) &kun.ls_kopf_txt, SQLLONG, 0);
dbClass.sqlout ((char *) kun.gn_pkt_kz, SQLCHAR, 2);
dbClass.sqlout ((char *) kun.sw_rab, SQLCHAR, 2);
dbClass.sqlout ((char *) kun.bbs, SQLCHAR, 9);
dbClass.sqlout ((long *) &kun.inka_nr2, SQLLONG, 0);
dbClass.sqlout ((short *) &kun.sw_fil_gr, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.sw_fil, SQLSHORT, 0);
dbClass.sqlout ((char *) kun.ueb_kz, SQLCHAR, 2);
dbClass.sqlout ((char *) kun.modif, SQLCHAR, 2);
dbClass.sqlout ((short *) &kun.kun_leer_kz, SQLSHORT, 0);
dbClass.sqlout ((char *) kun.ust_id16, SQLCHAR, 17);
dbClass.sqlout ((char *) kun.iln, SQLCHAR, 17);
dbClass.sqlout ((short *) &kun.waehrung, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.pr_ausw, SQLSHORT, 0);
dbClass.sqlout ((char *) kun.pr_hier, SQLCHAR, 2);
dbClass.sqlout ((short *) &kun.pr_ausw_ls, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.pr_ausw_re, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.kun_gr1, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.kun_gr2, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.eg_kz, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.bonitaet, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.kred_vers, SQLSHORT, 0);
dbClass.sqlout ((long *) &kun.kst, SQLLONG, 0);
dbClass.sqlout ((char *) kun.edi_typ, SQLCHAR, 2);
dbClass.sqlout ((long *) &kun.sedas_dta, SQLLONG, 0);
dbClass.sqlout ((char *) kun.sedas_kz, SQLCHAR, 2);
dbClass.sqlout ((short *) &kun.sedas_umf, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.sedas_abr, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.sedas_gesch, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.sedas_satz, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.sedas_med, SQLSHORT, 0);
dbClass.sqlout ((char *) kun.sedas_nam, SQLCHAR, 11);
dbClass.sqlout ((short *) &kun.sedas_abk1, SQLSHORT, 0);
dbClass.sqlout ((short *)&kun.sedas_abk2, SQLSHORT, 0);
dbClass.sqlout ((short *)&kun.sedas_abk3, SQLSHORT, 0);
dbClass.sqlout ((char *)  kun.sedas_nr1, SQLCHAR, 9);
dbClass.sqlout ((char *)  kun.sedas_nr2, SQLCHAR, 9);
dbClass.sqlout ((char *)  kun.sedas_nr3, SQLCHAR, 9);
dbClass.sqlout ((short *)&kun.sedas_vb1, SQLSHORT, 0);
dbClass.sqlout ((short *)&kun.sedas_vb2, SQLSHORT, 0);
dbClass.sqlout ((short *)&kun.sedas_vb3, SQLSHORT, 0);
dbClass.sqlout ((char *)  kun.sedas_iln, SQLCHAR, 17);
dbClass.sqlout ((long *) &kun.kond_kun, SQLLONG, 0);
dbClass.sqlout ((short *)&kun.kun_schema, SQLSHORT, 0);
dbClass.sqlout ((char *)  kun.plattform, SQLCHAR, 17);
dbClass.sqlout ((char *)  kun.be_log, SQLCHAR, 4);
dbClass.sqlout ((long *) &kun.stat_kun, SQLLONG, 0);
dbClass.sqlout ((char *)  kun.ust_nummer, SQLCHAR, 25);
dbClass.sqlout ((char *)  kun.fak_kz, SQLCHAR, 2);
dbClass.sqlout ((long *) &kun.fak_nr, SQLLONG, 0);
// 231213 
	if ( !dietzosep )
	{
		dbClass.sqlout ((short *)&kun.tagesepa, SQLSHORT, 0);	// 251113
		dbClass.sqlout ((char *)  kun.mandatref, SQLCHAR, 37);	// 251113
	
		readcursor = dbClass.sqlcursor (_T("select ")

		_T(" mdn, fil, kun, adr1, adr2, adr3, kun_seit, txt_nr1, frei_txt1, ")
		_T(" kun_krz1, kun_bran, kun_krz2, kun_krz3, kun_typ, bbn, pr_stu, ")
		_T(" pr_lst, vereinb, inka_nr, vertr1, vertr2, statk_period, a_period, ")
		_T(" sprache, sprache2, txt_nr2, frei_txt2, freifeld1, freifeld2, tou, vers_art, ")
		_T(" lief_art, fra_ko_ber, rue_schei, form_typ1, auflage1, freifeld3, ")
		_T(" freifeld4, zahl_art, zahl_ziel, form_typ2, auflage2, txt_nr3, ")
		_T(" frei_txt3, nr_bei_rech, rech_st, sam_rech, einz_ausw, gut, rab_schl, ")
		_T(" bonus1, bonus2, tdm_grenz1, tdm_grenz2, jr_plan_ums, deb_kto, ")
		_T(" kred_lim, inka_zaehl, bank_kun, blz, kto_nr, hausbank, kun_of_po, ")
		_T(" kun_of_lf, kun_of_best, delstatus, kun_bran2, rech_fuss_txt, ")
		_T(" ls_fuss_txt, ust_id, rech_kopf_txt, ls_kopf_txt, gn_pkt_kz, sw_rab, ")
		_T(" bbs, inka_nr2, sw_fil_gr, sw_fil, ueb_kz, modif, kun_leer_kz,")
		_T(" ust_id16, iln, waehrung, pr_ausw, pr_hier, pr_ausw_ls, ")
		_T(" pr_ausw_re, kun_gr1, kun_gr2, eg_kz, bonitaet, kred_vers, kst, ")
		_T(" edi_typ, sedas_dta, sedas_kz, sedas_umf, sedas_abr, sedas_gesch,")
		_T(" sedas_satz, sedas_med, sedas_nam, sedas_abk1, sedas_abk2, sedas_abk3, ")
		_T(" sedas_nr1, sedas_nr2, sedas_nr3, sedas_vb1, sedas_vb2, sedas_vb3, ")
		_T(" sedas_iln, kond_kun, kun_schema, plattform, be_log, stat_kun, ")
		_T(" ust_nummer, fak_kz, fak_nr, tagesepa, mandatref ")

		_T(" from kun where kun = ? and mdn = ? ") ) ;
	}
	else
	{
		readcursor = dbClass.sqlcursor (_T("select ")

		_T(" mdn, fil, kun, adr1, adr2, adr3, kun_seit, txt_nr1, frei_txt1, ")
		_T(" kun_krz1, kun_bran, kun_krz2, kun_krz3, kun_typ, bbn, pr_stu, ")
		_T(" pr_lst, vereinb, inka_nr, vertr1, vertr2, statk_period, a_period, ")
		_T(" sprache, txt_nr2, frei_txt2, freifeld1, freifeld2, tou, vers_art, ")
		_T(" lief_art, fra_ko_ber, rue_schei, form_typ1, auflage1, freifeld3, ")
		_T(" freifeld4, zahl_art, zahl_ziel, form_typ2, auflage2, txt_nr3, ")
		_T(" frei_txt3, nr_bei_rech, rech_st, sam_rech, einz_ausw, gut, rab_schl, ")
		_T(" bonus1, bonus2, tdm_grenz1, tdm_grenz2, jr_plan_ums, deb_kto, ")
		_T(" kred_lim, inka_zaehl, bank_kun, blz, kto_nr, hausbank, kun_of_po, ")
		_T(" kun_of_lf, kun_of_best, delstatus, kun_bran2, rech_fuss_txt, ")
		_T(" ls_fuss_txt, ust_id, rech_kopf_txt, ls_kopf_txt, gn_pkt_kz, sw_rab, ")
		_T(" bbs, inka_nr2, sw_fil_gr, sw_fil, ueb_kz, modif, kun_leer_kz,")
		_T(" ust_id16, iln, waehrung, pr_ausw, pr_hier, pr_ausw_ls, ")
		_T(" pr_ausw_re, kun_gr1, kun_gr2, eg_kz, bonitaet, kred_vers, kst, ")
		_T(" edi_typ, sedas_dta, sedas_kz, sedas_umf, sedas_abr, sedas_gesch,")
		_T(" sedas_satz, sedas_med, sedas_nam, sedas_abk1, sedas_abk2, sedas_abk3, ")
		_T(" sedas_nr1, sedas_nr2, sedas_nr3, sedas_vb1, sedas_vb2, sedas_vb3, ")
		_T(" sedas_iln, kond_kun, kun_schema, plattform, be_log, stat_kun, ")
		_T(" ust_nummer, fak_kz, fak_nr ")

		_T(" from kun where kun = ? and mdn = ? ") ) ;
	}

}

struct AUFK aufk,  aufk_null;	// 130707
struct ANGK angk,  angk_null;	// 130707

struct LSK lsk,  lsk_null;
extern DB_CLASS dbClass;


int LSK_CLASS::leselsk (int fehlercode)
{
	  if (fehlercode == LL_WRN_REPEAT_DATA)
	  {
         return 0;    // Hier kein fetch , da DS nicht mehr auf Seite passt , und nochmal 
					  // ausgegeben werden mu�
	  }
      int di = dbClass.sqlfetch (readcursor);
	  if ( !di)
	  {
	  		  MultiByteToWideChar( 0,0, lsk.pers_nam , -1 , lsk.ucpers_nam ,sizeof(lsk.ucpers_nam));
	  		  MultiByteToWideChar( 0,0, lsk.hinweis , -1 , lsk.uchinweis ,sizeof(lsk.uchinweis));
	  		  MultiByteToWideChar( 0,0, lsk.lieferzeit , -1 , lsk.uclieferzeit ,sizeof(lsk.uclieferzeit));
	  }

	  return di;
}

int AUFK_CLASS::leseaufk (int fehlercode)
{
	  if (fehlercode == LL_WRN_REPEAT_DATA)
	  {
         return 0;   
	  }
      int di = dbClass.sqlfetch (readcursor);
	  if ( !di)
	  {
	  		  MultiByteToWideChar( 0,0, aufk.pers_nam , -1 , aufk.ucpers_nam ,sizeof(aufk.ucpers_nam));
	  		  MultiByteToWideChar( 0,0, aufk.hinweis , -1 , aufk.uchinweis ,sizeof(aufk.uchinweis));
	  }

	  return di;
}
int ANGK_CLASS::leseangk (int fehlercode)
{
	  if (fehlercode == LL_WRN_REPEAT_DATA)
	  {
         return 0;   
	  }
      int di = dbClass.sqlfetch (readcursor);
	  if ( !di)
	  {
	  		  MultiByteToWideChar( 0,0, angk.hinweis , -1 , angk.uchinweis ,sizeof(angk.uchinweis));
	  }
	  return di;
}

int LSK_CLASS::openlsk (void)
{
		if ( readcursor < 0 ) prepare ();	// ob das hier richtig ist ?
         return dbClass.sqlopen (readcursor);
}

int AUFK_CLASS::openaufk (void)
{
		if ( readcursor < 0 ) prepare ();
	 return dbClass.sqlopen (readcursor);
}

int ANGK_CLASS::openangk (void)
{
		if ( readcursor < 0 ) prepare ();
	 return dbClass.sqlopen (readcursor);
}


void LSK_CLASS::prepare (void)
{
										
	test_upd_cursor = 1;

	dbClass.sqlin ((long *) &lsk.ls, SQLLONG, 0);
	dbClass.sqlin ((short *) &lsk.mdn, SQLSHORT, 0);


//		long ls;

    dbClass.sqlout ((long *) &lsk.adr, SQLLONG, 0);
	
// short mdn;
	dbClass.sqlout ((long *)	&lsk.auf, SQLLONG, 0 );
	dbClass.sqlout ((short *)	&lsk.kun_fil, SQLSHORT, 0 );
	dbClass.sqlout ((long *)	&lsk.kun, SQLLONG, 0 );
	dbClass.sqlout ((short *)	&lsk.fil, SQLSHORT, 0 );
	dbClass.sqlout ((char *)	 lsk.feld_bz1, SQLCHAR, 20 );
	dbClass.sqlout ((char *)	 lsk.lieferdat, SQLCHAR, 11 );
	dbClass.sqlout ((char *)	 lsk.lieferzeit, SQLCHAR, 6);
	dbClass.sqlout ((char *)	 lsk.hinweis, SQLCHAR, 202 );
	dbClass.sqlout ((short *)	&lsk.ls_stat, SQLSHORT, 0 );
	dbClass.sqlout ((char *)	 lsk.kun_krz1, SQLCHAR, 17 );
	dbClass.sqlout ((double *)	&lsk.auf_sum, SQLDOUBLE, 0 );
	dbClass.sqlout ((char *)	 lsk.feld_bz2, SQLCHAR, 12 );
	dbClass.sqlout ((double *)	&lsk.lim_er, SQLDOUBLE, 0 );
	dbClass.sqlout ((char *)	 lsk.partner, SQLCHAR, 37 );
	dbClass.sqlout ((long *)	&lsk.pr_lst, SQLLONG, 0 );
	dbClass.sqlout ((char *)	 lsk.feld_bz3, SQLCHAR, 8);
	dbClass.sqlout ((short *)	&lsk.pr_stu, SQLSHORT, 0 );
	dbClass.sqlout ((long *)	&lsk.vertr, SQLLONG, 0 );
	dbClass.sqlout ((long *)	&lsk.tou, SQLLONG, 0 );
	dbClass.sqlout ((char *)	 lsk.adr_nam1, SQLCHAR, 37 );
	dbClass.sqlout ((char *)	 lsk.adr_nam2, SQLCHAR, 37 );
	dbClass.sqlout ((char *)	 lsk.pf, SQLCHAR, 17 );
	dbClass.sqlout ((char *)	 lsk.str, SQLCHAR, 37 );
	dbClass.sqlout ((char *)	 lsk.plz, SQLCHAR, 9 );

	dbClass.sqlout ((char *)	 lsk.ort1, SQLCHAR, 37 );
	dbClass.sqlout ((double *)	&lsk.of_po, SQLDOUBLE, 0 );
	dbClass.sqlout ((short *)	&lsk.delstatus, SQLSHORT, 0 );
	dbClass.sqlout ((long *)	&lsk.rech, SQLLONG, 0 );
	dbClass.sqlout ((char *)	 lsk.blg_typ, SQLCHAR, 2 );
	dbClass.sqlout ((double *)	&lsk.zeit_dec, SQLDOUBLE, 0 );
	dbClass.sqlout ((long *)	&lsk.kopf_txt, SQLLONG, 0 );
	dbClass.sqlout ((long *)	&lsk.fuss_txt, SQLLONG, 0 );
	dbClass.sqlout ((long *)	&lsk.inka_nr, SQLLONG, 0 );
	dbClass.sqlout ((char *)	 lsk.auf_ext, SQLCHAR, 31 );	// 270212 17->31
	dbClass.sqlout ((long *)	&lsk.teil_smt, SQLLONG, 0 );
	dbClass.sqlout ((char *)	 lsk.pers_nam, SQLCHAR, 9 );
	dbClass.sqlout ((double *)	&lsk.brutto, SQLDOUBLE, 0 );
	dbClass.sqlout ((char *)	 lsk.komm_dat, SQLCHAR, 11 );
	dbClass.sqlout ((double *)	&lsk.of_ek, SQLDOUBLE, 0 );
	dbClass.sqlout ((double *)	&lsk.of_po_euro, SQLDOUBLE, 0 );
	dbClass.sqlout ((double *)	&lsk.of_po_fremd, SQLDOUBLE, 0 );
	dbClass.sqlout ((double *)	&lsk.of_ek_euro, SQLDOUBLE, 0 );
	dbClass.sqlout ((double *)	&lsk.of_ek_fremd, SQLDOUBLE, 0 );
	dbClass.sqlout ((short *)	&lsk.waehrung, SQLSHORT, 0 );
	dbClass.sqlout ((char *)	 lsk.ueb_kz, SQLCHAR, 2 );
	dbClass.sqlout ((double *)	&lsk.gew, SQLDOUBLE, 0 );
	dbClass.sqlout ((short *)	&lsk.auf_art, SQLSHORT, 0 );
	dbClass.sqlout ((short *)	&lsk.fak_typ, SQLSHORT, 0 );
	dbClass.sqlout ((short *)	&lsk.ccmarkt, SQLSHORT, 0 );
	dbClass.sqlout ((long *)	&lsk.gruppe, SQLLONG, 0 );
	dbClass.sqlout ((long *)	&lsk.tou_nr, SQLLONG, 0 );
	dbClass.sqlout ((short *)	&lsk.wieg_kompl, SQLSHORT, 0 );
	dbClass.sqlout ((char *)	 lsk.best_dat, SQLCHAR, 11 );
	dbClass.sqlout ((char *)	 lsk.hinweis2, SQLCHAR, 31 );
	dbClass.sqlout ((char *)	 lsk.hinweis3, SQLCHAR, 31 );
	dbClass.sqlout ((char *)	 lsk.fix_dat, SQLCHAR, 11 );
	dbClass.sqlout ((char *)	 lsk.komm_name, SQLCHAR, 13 );


	readcursor = dbClass.sqlcursor (_T("select ")


	_T(" adr, auf, kun_fil, kun, fil, feld_bz1, lieferdat, lieferzeit ")
	_T(" , hinweis, ls_stat, kun_krz1, auf_sum, feld_bz2, lim_er ")
	_T(" , partner, pr_lst, feld_bz3, pr_stu, vertr, tou, adr_nam1 ")
	_T(" , adr_nam2, pf, str, plz, ort1, of_po, delstatus, rech, blg_typ ")
	_T(" , zeit_dec, kopf_txt, fuss_txt, inka_nr, lsk.auf_ext, teil_smt ")
	_T(" , pers_nam, brutto, komm_dat, of_ek, of_po_euro, of_po_fremd ")
	_T(" , of_ek_euro, of_ek_fremd, waehrung, ueb_kz, gew, auf_art ")
	_T(" , fak_typ, ccmarkt, gruppe, tou_nr, wieg_kompl, best_dat ")
	_T(" , hinweis2, hinweis3, fix_dat, komm_name ")
	
	_T(" from lsk where ls = ? and mdn = ? ") ) ;
	
}

void ANGK_CLASS::prepare (void)
{
	test_upd_cursor = 1;
	dbClass.sqlin ((long *) &angk.ang, SQLLONG, 0);
	dbClass.sqlin ((short *) &angk.mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &angk.fil, SQLSHORT, 0 );
	dbClass.sqlout ((char *) lsk.hinweis, SQLCHAR, 202 );
	dbClass.sqlout (( long *) &angk.tou, SQLLONG , 0 ) ;
	dbClass.sqlout (( char *) lsk.lieferzeit, SQLCHAR , 6 ) ;
	// 201207 : tou dazu .....
	readcursor = dbClass.sqlcursor (_T("select hinweis, tou, lieferzeit ")
		_T(" from angk where ang = ? and mdn = ? and fil = ? ") ) ;
	
}

void AUFK_CLASS::prepare (void)
{
	test_upd_cursor = 1;
	dbClass.sqlin ((long *) &aufk.auf, SQLLONG, 0);
	dbClass.sqlin ((short *) &aufk.mdn, SQLSHORT, 0);	// 071108 : hier stand bis eben aufk.auf ??!!
	dbClass.sqlin ((short *) &aufk.fil, SQLSHORT, 0 );

	dbClass.sqlout ((char *)  lsk.hinweis, SQLCHAR, 202 );
	dbClass.sqlout ((long *)  &aufk.tou, SQLLONG, 0 );
	dbClass.sqlout ((char *)  lsk.lieferzeit, SQLCHAR, 6 );
// 201207 : tour dazu ....	
	dbClass.sqlout ((char *)  aufk.pers_nam, SQLCHAR, 9 );
// 281113 : pers_nam dazu dazu ....	
	readcursor = dbClass.sqlcursor (_T("select hinweis,tou, lieferzeit, pers_nam ")

	_T(" from aufk where auf = ? and mdn = ? and fil = ? ") ) ;
	
}


struct RETK retk,  retk_null;


int RETK_CLASS::leseretk (int fehlercode)
{
	  if (fehlercode == LL_WRN_REPEAT_DATA)
	  {
         return 0;    // Hier kein fetch , da DS nicht mehr auf Seite passt , und nochmal 
					  // ausgegeben werden mu�
	  }
      int di = dbClass.sqlfetch (readcursor);
	  if ( !di)
	  {
	  		  MultiByteToWideChar( 0,0, retk.hinweis , -1 , retk.uchinweis ,sizeof(retk.uchinweis));
	  }

	  return di;
}


int RETK_CLASS::openretk (void)
{

		if ( readcursor < 0 ) prepare ();	// ob das hier richtig ist ?

         return dbClass.sqlopen (readcursor);
}



void RETK_CLASS::prepare (void)
{

 										
	test_upd_cursor = 1;


	dbClass.sqlin ((short *) &retk.mdn, SQLSHORT, 0);
	dbClass.sqlin ((long *) &retk.ret, SQLLONG, 0);


			// short mdn ;
    dbClass.sqlout ((short  *) &retk.fil, SQLSHORT, 0 ) ;
			// long  ret ;
    dbClass.sqlout ((long   *) &retk.ls, SQLLONG, 0 ) ;
    dbClass.sqlout ((short  *) &retk.kun_fil, SQLSHORT, 0 ) ;
    dbClass.sqlout ((char   *)  retk.feld_bz1, SQLCHAR, 20 ) ;
    dbClass.sqlout ((long   *) &retk.kun, SQLLONG, 0 ) ;

	dbClass.sqlout ((char   *)  retk.ret_dat, SQLCHAR,11 ) ;
    dbClass.sqlout ((char   *)  retk.lieferdat, SQLCHAR,11 ) ;
    dbClass.sqlout ((char   *)  retk.hinweis, SQLCHAR, 202 ) ;
    dbClass.sqlout ((short  *) &retk.ret_stat, SQLSHORT, 0 ) ;
    dbClass.sqlout ((char   *)  retk.feld_bz2, SQLCHAR, 12 ) ;
    dbClass.sqlout ((char   *)  retk.kun_krz1, SQLCHAR,17 ) ;
    dbClass.sqlout ((char   *)  retk.feld_bz3, SQLCHAR,8 ) ;

	dbClass.sqlout ((long   *) &retk.adr, SQLLONG, 0 ) ;
    dbClass.sqlout ((short  *) &retk.delstatus, SQLSHORT, 0 ) ;
    dbClass.sqlout ((long   *) &retk.rech, SQLLONG, 0 ) ;
    dbClass.sqlout ((char   *)  retk.blg_typ, SQLCHAR,2 ) ;
    dbClass.sqlout ((long   *) &retk.kopf_txt, SQLLONG, 0 ) ;
    dbClass.sqlout ((long   *) &retk.fuss_txt, SQLLONG, 0 ) ;
    dbClass.sqlout ((long   *) &retk.vertr, SQLLONG, 0 ) ;
   
	dbClass.sqlout ((long   *) &retk.inka_nr, SQLLONG, 0 ) ;
    dbClass.sqlout ((double *) &retk.of_po, SQLDOUBLE, 0 ) ;
    dbClass.sqlout ((short  *) &retk.teil_smt, SQLSHORT, 0 ) ;
    dbClass.sqlout ((char   *)  retk.pers_nam, SQLCHAR,9 ) ;
    dbClass.sqlout ((double *) &retk.of_ek, SQLDOUBLE, 0 ) ;
    dbClass.sqlout ((double *) &retk.of_po_euro, SQLDOUBLE, 0 ) ;
    dbClass.sqlout ((double *) &retk.of_po_fremd, SQLDOUBLE, 0 ) ;

	dbClass.sqlout ((double *) &retk.of_ek_euro, SQLDOUBLE, 0 ) ;
    dbClass.sqlout ((double *) &retk.of_ek_fremd, SQLDOUBLE, 0 ) ;
    dbClass.sqlout ((short  *) &retk.waehrung, SQLSHORT, 0 ) ;
    dbClass.sqlout ((short  *) &retk.ret_grund, SQLSHORT, 0 ) ;
    dbClass.sqlout ((long   *) &retk.gruppe, SQLLONG, 0 ) ;
    dbClass.sqlout ((char   *)  retk.ueb_kz, SQLCHAR,2 ) ;

	
	readcursor = dbClass.sqlcursor (_T("select ")

	_T(" fil ,ls ,kun_fil ,feld_bz1 ,kun ")
	_T(" ,ret_dat ,lieferdat ,hinweis ,ret_stat ,feld_bz2 ,kun_krz1 ,feld_bz3 ")
	_T(" ,adr ,delstatus ,rech ,blg_typ ,kopf_txt ,fuss_txt ,vertr ")
	_T(" ,inka_nr ,of_po ,teil_smt ,pers_nam ,of_ek ,of_po_euro ,of_po_fremd ")
	_T(" ,of_ek_euro ,of_ek_fremd ,waehrung ,ret_grund ,gruppe ,ueb_kz ")
	
	_T(" from retk where mdn = ? and ret = ? ") 
	) ;	
}


