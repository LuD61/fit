//=============================================================================
//
//  Project: List & Label
//           Copyright (c) 1998-2002 combit GmbH, All Rights Reserved
//
//  Authors: combit Software Team
//
//-----------------------------------------------------------------------------
//
//  Module:  LLMFC - List & Label MFC Sample Application
//
//=============================================================================

// mainfrm.h : interface of the CMainFrame class
// #include "mo_progcfg.h"


class CMainFrame : public CFrameWnd
{
protected: // create from serialization only
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

// GrJ 161203     PROG_CFG *ProgCfg;
// Attributes
	BOOL m_bDebug;

// Operations
public:

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual	void AssertValid() const;
	virtual	void Dump(CDumpContext& dc) const;
#endif

	static UINT m_uLuLMessageBase;	// message base for LuL messages


// Generated message map functions
protected:

	void DoListPrint();
	void DoLabelPrint();
	void GetCfgValues();
	void InterpretCommandLine();
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnStartDebug();
	afx_msg void OnStopDebug();
	afx_msg void OnEditLabel();
	afx_msg void OnEditList();
	afx_msg void OnPrintLabel();
	afx_msg void OnPrintReport();
	afx_msg void OnUpdateStartDebug(CCmdUI* pCmdUI);
	afx_msg void OnUpdateStopDebug(CCmdUI* pCmdUI);
	//}}AFX_MSG
	afx_msg LRESULT OnLulMessage(WPARAM wParam, LPARAM lParam);

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
