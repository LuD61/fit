#include "stdafx.h"
#include "DbClass.h"
#include "txt.h"
#ifdef LL19
#include "cmbtll19.h"
#else
#ifdef LL12
#include "cmbtll12.h"
#else
#include "cmbtll11.h"
#endif
#endif
struct PTXT lspt,  lspt_null;
struct PTXT angpt, angpt_null;	// 130707
struct PTXT aufpt, aufpt_null;	// 130707
struct PTXT retpt,  retpt_null;
struct PTXT ls_txt,  ls_txt_null;
struct ATEXTE atexte,  atexte_null;	// 101014
struct AUFPT_VERKFRAGE aufpt_verkfrage, aufpt_verkfrage_null;	// 151014


extern DB_CLASS dbClass;

int itxt_nr ;
static int anzzfelder ;

int LSPT_CLASS::dbcount (void)
{
	if (test_upd_cursor == -1)
    {
		prepare ();
    }
    dbClass.sqlopen (count_cursor);
    dbClass.sqlfetch (count_cursor);
    if (sqlstatus == 0)
    {
		return anzzfelder;
    }
    return sqlstatus;
}

// 130707
int ANGPT_CLASS::dbcount (void)
{
	if (test_upd_cursor == -1)
    {
		prepare ();
    }
    dbClass.sqlopen (count_cursor);
    dbClass.sqlfetch (count_cursor);
    if (sqlstatus == 0)
    {
		return anzzfelder;
    }
    return sqlstatus;
}

// 130707
int AUFPT_CLASS::dbcount (void)
{
	if (test_upd_cursor == -1)
    {
		prepare ();
	}
    dbClass.sqlopen (count_cursor);
    dbClass.sqlfetch (count_cursor);
    if (sqlstatus == 0)
    {
		return anzzfelder;
    }
    return sqlstatus;
}

int RETPT_CLASS::dbcount (void)
/**
Tabelle retpt lesen.
**/
{

         if (test_upd_cursor == -1)
         {
             prepare ();
         }
         dbClass.sqlopen (count_cursor);
         dbClass.sqlfetch (count_cursor);
         if (sqlstatus == 0)
         {
                      return anzzfelder;
         }
         return sqlstatus;

}

int LS_TXT_CLASS::dbcount (void)
/**
Tabelle ls_txt lesen.
**/
{

         if (test_upd_cursor == -1)
         {
             prepare ();
         }
         dbClass.sqlopen (count_cursor);
         dbClass.sqlfetch (count_cursor);
         if (sqlstatus == 0)
         {
                      return anzzfelder;
         }
         return sqlstatus;

}


int LSPT_CLASS::leselspt (int fehlercode)
{
	  if (fehlercode == LL_WRN_REPEAT_DATA)
	  {
         return 0;    // Hier kein fetch , da DS nicht mehr auf Seite passt , und nochmal 
					  // ausegeben werden mu�
	  }
  	  	memcpy ( &lspt, &lspt_null , sizeof ( struct PTXT )) ;	// 290506

      int di = dbClass.sqlfetch (readcursor);
	  if ( !di)
	  {
	  		MultiByteToWideChar( CP_ACP,0, lspt.txt , -1 , lspt.uctxt, sizeof(lspt.uctxt) );
	  }

	  return di;
}

int ANGPT_CLASS::leseangpt (int fehlercode)
{
	  if (fehlercode == LL_WRN_REPEAT_DATA)
	  {
         return 0;  
	  }
  	  memcpy ( &angpt, &angpt_null , sizeof ( struct PTXT )) ;
      int di = dbClass.sqlfetch (readcursor);
	  if ( !di)
	  {
	  		MultiByteToWideChar( CP_ACP,0, angpt.txt , -1 , angpt.uctxt, sizeof(angpt.uctxt) );
	  }
	  return di;
}

int AUFPT_CLASS::leseaufpt (int fehlercode)
{
	  if (fehlercode == LL_WRN_REPEAT_DATA)
	  {
         return 0;  
	  }
  	  memcpy ( &aufpt, &aufpt_null , sizeof ( struct PTXT )) ;
      int di = dbClass.sqlfetch (readcursor);
  	  if ( !di)
	  {
	  		MultiByteToWideChar( CP_ACP,0, aufpt.txt , -1 , aufpt.uctxt, sizeof(aufpt.uctxt) );
	  }

	  return di;
}


int RETPT_CLASS::leseretpt (int fehlercode)
{
	  if (fehlercode == LL_WRN_REPEAT_DATA)
	  {
         return 0;    // Hier kein fetch , da DS nicht mehr auf Seite passt , und nochmal 
					  // ausegeben werden mu�
	  }

  	memcpy ( &retpt, &retpt_null , sizeof ( struct PTXT )) ;	// 290506
    int di = dbClass.sqlfetch (readcursor);
	  if ( !di)
	  {
	  		MultiByteToWideChar( CP_ACP,0, retpt.txt , -1 , retpt.uctxt, sizeof(retpt.uctxt) );
	  }

	  return di;
}

int LS_TXT_CLASS::lesels_txt (int fehlercode)
{
	  if (fehlercode == LL_WRN_REPEAT_DATA)
	  {
         return 0;    // Hier kein fetch , da DS nicht mehr auf Seite passt , und nochmal 
					  // ausegeben werden mu�
	  }
  	  	memcpy ( &ls_txt, &ls_txt_null , sizeof ( struct PTXT )) ;	// 290506

      int di = dbClass.sqlfetch (readcursor);
	  if ( !di)
	  {
	  		MultiByteToWideChar( CP_ACP,0, ls_txt.txt , -1 , ls_txt.uctxt, sizeof(ls_txt.uctxt) );
	  }

	  return di;
}


int LSPT_CLASS::openlspt (void)
{

		if ( readcursor < 0 ) prepare ();	// ob das hier richtig ist ?

         return dbClass.sqlopen (readcursor);
}

int ANGPT_CLASS::openangpt (void)
{
		if ( readcursor < 0 ) prepare ();	
		 return dbClass.sqlopen (readcursor);
}

int AUFPT_CLASS::openaufpt (void)
{
		if ( readcursor < 0 ) prepare ();	
		 return dbClass.sqlopen (readcursor);
}


int RETPT_CLASS::openretpt (void)
{

		if ( readcursor < 0 ) prepare ();	// ob das hier richtig ist ?

         return dbClass.sqlopen (readcursor);
}


int LS_TXT_CLASS::openls_txt (void)
{

		if ( readcursor < 0 ) prepare ();	// ob das hier richtig ist ?

         return dbClass.sqlopen (readcursor);
}

void LSPT_CLASS::prepare (void)
{
	dbClass.sqlin ((long *) &lspt.nr, SQLLONG, 0);
    dbClass.sqlout ((long *)  &anzzfelder, SQLLONG, 0);

    count_cursor = dbClass.sqlcursor (_T("select count(*) from lspt ")
										_T("where lspt.nr = ? "));
										
	test_upd_cursor = 1;


	dbClass.sqlin ((long *) &lspt.nr, SQLLONG, 0);

	dbClass.sqlout ((long *) &lspt.zei,SQLLONG, 0);
	dbClass.sqlout ((char *) lspt.txt,SQLCHAR, 81);	// 61->81 300106

		readcursor = dbClass.sqlcursor (_T("select ")

	_T(" zei, txt ")
	_T(" from lspt where nr = ? order by zei") ) ;

}

void ANGPT_CLASS::prepare (void)
{
	dbClass.sqlin ((long *) &angpt.nr, SQLLONG, 0);
    dbClass.sqlout ((long *)  &anzzfelder, SQLLONG, 0);

    count_cursor = dbClass.sqlcursor (_T("select count(*) from angpt ")
										_T("where angpt.nr = ? "));
										
	test_upd_cursor = 1;

	dbClass.sqlin ((long *) &angpt.nr, SQLLONG, 0);

	dbClass.sqlout ((long *) &angpt.zei,SQLLONG, 0);
	dbClass.sqlout ((char *) angpt.txt,SQLCHAR, 81);	
// Achtung : text wird 2 mal gelesen und zus�tzlich in lspt.txt geschrieben
	dbClass.sqlout ((char *) lspt.txt,SQLCHAR, 81);

		readcursor = dbClass.sqlcursor (_T("select ")

	_T(" zei, txt , txt ")
	_T(" from angpt where nr = ? order by zei") ) ;
	
}

void AUFPT_CLASS::prepare (void)
{
	dbClass.sqlin ((long *) &aufpt.nr, SQLLONG, 0);
    dbClass.sqlout ((long *)  &anzzfelder, SQLLONG, 0);

    count_cursor = dbClass.sqlcursor (_T("select count(*) from aufpt ")
										_T("where aufpt.nr = ? "));
										
	test_upd_cursor = 1;

	dbClass.sqlin ((long *) &aufpt.nr, SQLLONG, 0);

	dbClass.sqlout ((long *) &aufpt.zei,SQLLONG, 0);
	dbClass.sqlout ((char *) aufpt.txt,SQLCHAR, 81);	
// Achtung : text wird 2 mal gelesen und zus�tzlich in lspt.txt geschrieben
	dbClass.sqlout ((char *) lspt.txt,SQLCHAR, 81);

		readcursor = dbClass.sqlcursor (_T("select ")

	_T(" zei, txt , txt ")
	_T(" from aufpt where nr = ? order by zei") ) ;
	
}

void RETPT_CLASS::prepare (void)
{

	dbClass.sqlin ((long *) &retpt.nr, SQLLONG, 0);
    dbClass.sqlout ((long *)  &anzzfelder, SQLLONG, 0);

    count_cursor = dbClass.sqlcursor (_T("select count(*) from retpt ")
										_T("where retpt.nr = ? "));
										
	test_upd_cursor = 1;


	dbClass.sqlin ((long *) &retpt.nr, SQLLONG, 0);

	dbClass.sqlout ((long *) &retpt.zei,SQLLONG, 0);
	dbClass.sqlout ((char *) retpt.txt,SQLCHAR, 81);	//	61->81 300106

		readcursor = dbClass.sqlcursor (_T("select ")

	_T(" zei, txt ")
	_T(" from retpt where nr = ? order by zei") ) ;

}

void LS_TXT_CLASS::prepare (void)
{
	dbClass.sqlin ((long *) &ls_txt.nr, SQLLONG, 0);
    dbClass.sqlout ((long *)  &anzzfelder, SQLLONG, 0);

    count_cursor = dbClass.sqlcursor (_T("select count(*) from ls_txt ")
										_T("where ls_txt.nr = ? "));
										
	test_upd_cursor = 1;

	dbClass.sqlin ((long *) &ls_txt.nr, SQLLONG, 0);

	dbClass.sqlout ((long *) &ls_txt.zei,SQLLONG, 0);
	dbClass.sqlout ((char *) ls_txt.txt,SQLCHAR, 81);	// 61->81 300106 

		readcursor = dbClass.sqlcursor (_T("select ")

	_T(" zei, txt ")
	_T(" from ls_txt where nr = ? order by zei ") ) ;

}

// 101014 A

int ATEXTE_CLASS::leseatexte (long exttext)
{
	if ( readcursor < 0 ) prepare();

	memcpy ( &atexte, &atexte_null, sizeof ( struct ATEXTE ));
	atexte.txt_nr = exttext;
	int di = dbClass.sqlopen (readcursor);
	di = dbClass.sqlfetch (readcursor);
	if ( !di)
	{
		MultiByteToWideChar( CP_ACP,0, atexte.txt , -1 , atexte.uctxt, sizeof(atexte.uctxt) );
		MultiByteToWideChar( CP_ACP,0, atexte.disp_txt , -1 , atexte.ucdisp_txt, sizeof(atexte.ucdisp_txt) );
	}

	return di;
}

void ATEXTE_CLASS::prepare (void)
{

	dbClass.sqlin ((long *) &atexte.txt_nr, SQLLONG, 0);

//	dbClass.sqlout ((long *) &atxte.sys, SQLLONG, 0);
//	dbClass.sqlout ((short *) &atxte.waa, SQLSHORT, 0);
// //	dbClass.sqlout ((long *) &atxte.txt_nr, SQLLONG, 0);
	dbClass.sqlout ((char *) &atexte.txt, SQLCHAR, 1025 );
	dbClass.sqlout ((char *) &atexte.disp_txt, SQLCHAR, 1025);
//	dbClass.sqlout ((short *) &atxte.alignment, SQLSHORT, 0);
//	dbClass.sqlout ((short *) &atxte.send_ctrl, SQLSHORT, 0);
//	dbClass.sqlout ((short *) &atxte.txt_typ, SQLSHORT, 0);
//	dbClass.sqlout ((short *) &atxte.txt_platz, SQLSHORT, 0);
//	dbClass.sqlout ((double *) &atxte.a, SQLDOUBLE, 0);


	readcursor = dbClass.sqlcursor (
		_T(" select txt, disp_txt from atexte ")
		_T(" where txt_nr = ? ")
	);

}
// 101014 E
// 151014

int AUFPT_VERKFRAGE_CLASS::leseaufpt_verkfrage (short emdn, long els, double ea )
{
	if ( readcursor < 0 )
	{	aufpt_verkfrage.mdn = emdn - 1 ;	// Start erzwingen
		// Speicher bereitstellen und urladen 

	ctxt_ls1	= (char*) malloc ( sizeof ( atexte.disp_txt));
	ctxt_rech1	= (char*) malloc ( sizeof( atexte.disp_txt));
	ctxt_ls2	= (char*) malloc ( sizeof( atexte.disp_txt));
	ctxt_rech2	= (char*) malloc ( sizeof( atexte.disp_txt));
	ctxt_ls3	= (char*) malloc ( sizeof( atexte.disp_txt));
	ctxt_rech3	= (char*) malloc ( sizeof( atexte.disp_txt));
	ctxt_ls4	= (char*) malloc ( sizeof( atexte.disp_txt));
	ctxt_rech4	= (char*) malloc ( sizeof( atexte.disp_txt));
	ctxt_ls5	= (char*) malloc ( sizeof( atexte.disp_txt));
	ctxt_rech5	= (char*) malloc ( sizeof( atexte.disp_txt));
	ctxt_ls6	= (char*) malloc ( sizeof( atexte.disp_txt));
	ctxt_rech6	= (char*) malloc ( sizeof( atexte.disp_txt));
	ctxt_ls7	= (char*) malloc ( sizeof( atexte.disp_txt));
	ctxt_rech7	= (char*) malloc ( sizeof( atexte.disp_txt));
	ctxt_ls8	= (char*) malloc ( sizeof( atexte.disp_txt));
	ctxt_rech8	= (char*) malloc ( sizeof( atexte.disp_txt));
	ctxt_ls9	= (char*) malloc ( sizeof( atexte.disp_txt));
	ctxt_rech9	= (char*) malloc ( sizeof( atexte.disp_txt));
	ctxt_ls10	= (char*) malloc ( sizeof( atexte.disp_txt));
	ctxt_rech10	= (char*) malloc ( sizeof( atexte.disp_txt));

	ucctxt_ls1		= (TCHAR*) malloc ( sizeof ( atexte.ucdisp_txt));
	ucctxt_rech1	= (TCHAR*) malloc ( sizeof( atexte.ucdisp_txt));
	ucctxt_ls2		= (TCHAR*) malloc ( sizeof( atexte.ucdisp_txt));
	ucctxt_rech2	= (TCHAR*) malloc ( sizeof( atexte.ucdisp_txt));
	ucctxt_ls3		= (TCHAR*) malloc ( sizeof( atexte.ucdisp_txt));
	ucctxt_rech3	= (TCHAR*) malloc ( sizeof( atexte.ucdisp_txt));
	ucctxt_ls4		= (TCHAR*) malloc ( sizeof( atexte.ucdisp_txt));
	ucctxt_rech4	= (TCHAR*) malloc ( sizeof( atexte.ucdisp_txt));
	ucctxt_ls5		= (TCHAR*) malloc ( sizeof( atexte.ucdisp_txt));
	ucctxt_rech5	= (TCHAR*) malloc ( sizeof( atexte.ucdisp_txt));
	ucctxt_ls6		= (TCHAR*) malloc ( sizeof( atexte.ucdisp_txt));
	ucctxt_rech6	= (TCHAR*) malloc ( sizeof( atexte.ucdisp_txt));
	ucctxt_ls7		= (TCHAR*) malloc ( sizeof( atexte.ucdisp_txt));
	ucctxt_rech7	= (TCHAR*) malloc ( sizeof( atexte.ucdisp_txt));
	ucctxt_ls8		= (TCHAR*) malloc ( sizeof( atexte.ucdisp_txt));
	ucctxt_rech8	= (TCHAR*) malloc ( sizeof( atexte.ucdisp_txt));
	ucctxt_ls9		= (TCHAR*) malloc ( sizeof( atexte.ucdisp_txt));
	ucctxt_rech9	= (TCHAR*) malloc ( sizeof( atexte.ucdisp_txt));
	ucctxt_ls10		= (TCHAR*) malloc ( sizeof( atexte.ucdisp_txt));
	ucctxt_rech10	= (TCHAR*) malloc ( sizeof( atexte.ucdisp_txt));


		prepare();
	}
	if ( aufpt_verkfrage.mdn == emdn && aufpt_verkfrage.ls == els && aufpt_verkfrage.a == ea )
		return 0 ;	// es passt bereits alles ...
	memcpy ( &aufpt_verkfrage, &aufpt_verkfrage_null, sizeof ( struct AUFPT_VERKFRAGE ));
	aufpt_verkfrage.a = ea ;
	aufpt_verkfrage.mdn = emdn ;
	aufpt_verkfrage.ls = els ;

	sprintf( ctxt_ls1,"");
	sprintf( ctxt_rech1,"");
	sprintf( ctxt_ls2,"");
	sprintf( ctxt_rech2,"");
	sprintf( ctxt_ls3,"");
	sprintf( ctxt_rech3,"");
	sprintf( ctxt_ls4,"");
	sprintf( ctxt_rech4,"");
	sprintf( ctxt_ls5,"");
	sprintf( ctxt_rech5,"");
	sprintf( ctxt_ls6,"");
	sprintf( ctxt_rech6,"");
	sprintf( ctxt_ls7,"");
	sprintf( ctxt_rech7,"");
	sprintf( ctxt_ls8,"");
	sprintf( ctxt_rech8,"");
	sprintf( ctxt_ls9,"");
	sprintf( ctxt_rech9,"");
	sprintf( ctxt_ls10,"");
	sprintf( ctxt_rech10,"");

	swprintf( ucctxt_ls1,_T(""));
	swprintf( ucctxt_rech1,_T(""));
	swprintf( ucctxt_ls2,_T(""));
	swprintf( ucctxt_rech2,_T(""));
	swprintf( ucctxt_ls3,_T(""));
	swprintf( ucctxt_rech3,_T(""));
	swprintf( ucctxt_ls4,_T(""));
	swprintf( ucctxt_rech4,_T(""));
	swprintf( ucctxt_ls5,_T(""));
	swprintf( ucctxt_rech5,_T(""));
	swprintf( ucctxt_ls6,_T(""));
	swprintf( ucctxt_rech6,_T(""));
	swprintf( ucctxt_ls7,_T(""));
	swprintf( ucctxt_rech7,_T(""));
	swprintf( ucctxt_ls8,_T(""));
	swprintf( ucctxt_rech8,_T(""));
	swprintf( ucctxt_ls9,_T(""));
	swprintf( ucctxt_rech9,_T(""));
	swprintf( ucctxt_ls10,_T(""));
	swprintf( ucctxt_rech10,_T(""));

	if ( ea == -777 )
		return 0 ;

	int di = dbClass.sqlopen (readcursor);
	di = dbClass.sqlfetch (readcursor);
	if ( !di)
	{
		MultiByteToWideChar( CP_ACP,0, ctxt_ls1 , -1 , ucctxt_ls1, sizeof(ucctxt_ls1) );
		MultiByteToWideChar( CP_ACP,0, ctxt_ls2 , -1 , ucctxt_ls2, sizeof(ucctxt_ls2) );
		MultiByteToWideChar( CP_ACP,0, ctxt_ls3 , -1 , ucctxt_ls3, sizeof(ucctxt_ls3) );
		MultiByteToWideChar( CP_ACP,0, ctxt_ls4 , -1 , ucctxt_ls4, sizeof(ucctxt_ls4) );
		MultiByteToWideChar( CP_ACP,0, ctxt_ls5 , -1 , ucctxt_ls5, sizeof(ucctxt_ls5) );
		MultiByteToWideChar( CP_ACP,0, ctxt_ls6 , -1 , ucctxt_ls6, sizeof(ucctxt_ls6) );
		MultiByteToWideChar( CP_ACP,0, ctxt_ls7 , -1 , ucctxt_ls7, sizeof(ucctxt_ls7) );
		MultiByteToWideChar( CP_ACP,0, ctxt_ls8 , -1 , ucctxt_ls8, sizeof(ucctxt_ls8) );
		MultiByteToWideChar( CP_ACP,0, ctxt_ls9 , -1 , ucctxt_ls9, sizeof(ucctxt_ls9) );
		MultiByteToWideChar( CP_ACP,0, ctxt_ls10, -1 , ucctxt_ls10,sizeof(ucctxt_ls10));


		MultiByteToWideChar( CP_ACP,0, ctxt_rech1 , -1 , ucctxt_rech1, sizeof(ucctxt_ls1) );
		MultiByteToWideChar( CP_ACP,0, ctxt_rech2 , -1 , ucctxt_rech2, sizeof(ucctxt_rech2) );
		MultiByteToWideChar( CP_ACP,0, ctxt_rech3 , -1 , ucctxt_rech3, sizeof(ucctxt_rech3) );
		MultiByteToWideChar( CP_ACP,0, ctxt_rech4 , -1 , ucctxt_rech4, sizeof(ucctxt_rech4) );
		MultiByteToWideChar( CP_ACP,0, ctxt_rech5 , -1 , ucctxt_rech5, sizeof(ucctxt_rech5) );
		MultiByteToWideChar( CP_ACP,0, ctxt_rech6 , -1 , ucctxt_rech6, sizeof(ucctxt_rech6) );
		MultiByteToWideChar( CP_ACP,0, ctxt_rech7 , -1 , ucctxt_rech7, sizeof(ucctxt_rech7) );
		MultiByteToWideChar( CP_ACP,0, ctxt_rech8 , -1 , ucctxt_rech8, sizeof(ucctxt_rech8) );
		MultiByteToWideChar( CP_ACP,0, ctxt_rech9 , -1 , ucctxt_rech9, sizeof(ucctxt_rech9) );
		MultiByteToWideChar( CP_ACP,0, ctxt_rech10, -1 , ucctxt_rech10,sizeof(ucctxt_rech10));


	}

	return di;
}

AUFPT_VERKFRAGE_CLASS::~AUFPT_VERKFRAGE_CLASS()
{
	if (  ctxt_ls1 == NULL )
		return;
	free (ctxt_ls1);
	free (ctxt_rech1);
	free (ctxt_ls2);
	free (ctxt_rech2);
	free (ctxt_ls3);
	free (ctxt_rech3);
	free (ctxt_ls4);
	free (ctxt_rech4);
	free (ctxt_ls5);
	free (ctxt_rech5);
	free (ctxt_ls6);
	free (ctxt_rech6);
	free (ctxt_ls7);
	free (ctxt_rech7);
	free (ctxt_ls8);
	free (ctxt_rech8);
	free (ctxt_ls9);
	free (ctxt_rech9);
	free (ctxt_ls10);
	free (ctxt_rech10);

	free (ucctxt_ls1);
	free (ucctxt_rech1);
	free (ucctxt_ls2);
	free (ucctxt_rech2);
	free (ucctxt_ls3);
	free (ucctxt_rech3);
	free (ucctxt_ls4);
	free (ucctxt_rech4);
	free (ucctxt_ls5);
	free (ucctxt_rech5);
	free (ucctxt_ls6);
	free (ucctxt_rech6);
	free (ucctxt_ls7);
	free (ucctxt_rech7);
	free (ucctxt_ls8);
	free (ucctxt_rech8);
	free (ucctxt_ls9);
	free (ucctxt_rech9);
	free (ucctxt_ls10);
	free (ucctxt_rech10);

}

void AUFPT_VERKFRAGE_CLASS::prepare (void)
{
	dbClass.sqlin ((long *) &aufpt_verkfrage.ls, SQLLONG, 0);
	dbClass.sqlin ((short *) &aufpt_verkfrage.mdn, SQLSHORT, 0);
	dbClass.sqlin ((double *) &aufpt_verkfrage.a, SQLDOUBLE, 0);

// 031114 : def1 .. def10 dazu
	dbClass.sqlout ((short *) &aufpt_verkfrage.def1, SQLSHORT, 0);
	dbClass.sqlout ((short *) &aufpt_verkfrage.def2, SQLSHORT, 0);
	dbClass.sqlout ((short *) &aufpt_verkfrage.def3, SQLSHORT, 0);
	dbClass.sqlout ((short *) &aufpt_verkfrage.def4, SQLSHORT, 0);
	dbClass.sqlout ((short *) &aufpt_verkfrage.def5, SQLSHORT, 0);
	dbClass.sqlout ((short *) &aufpt_verkfrage.def6, SQLSHORT, 0);
	dbClass.sqlout ((short *) &aufpt_verkfrage.def7, SQLSHORT, 0);
	dbClass.sqlout ((short *) &aufpt_verkfrage.def8, SQLSHORT, 0);
	dbClass.sqlout ((short *) &aufpt_verkfrage.def9, SQLSHORT, 0);
	dbClass.sqlout ((short *) &aufpt_verkfrage.def10, SQLSHORT, 0);


	dbClass.sqlout ((long *) &aufpt_verkfrage.txt_ls1, SQLLONG, 0);
	dbClass.sqlout ((long *) &aufpt_verkfrage.txt_ls2, SQLLONG, 0);
	dbClass.sqlout ((long *) &aufpt_verkfrage.txt_ls3, SQLLONG, 0);
	dbClass.sqlout ((long *) &aufpt_verkfrage.txt_ls4, SQLLONG, 0);
	dbClass.sqlout ((long *) &aufpt_verkfrage.txt_ls5, SQLLONG, 0);
	dbClass.sqlout ((long *) &aufpt_verkfrage.txt_ls6, SQLLONG, 0);
	dbClass.sqlout ((long *) &aufpt_verkfrage.txt_ls7, SQLLONG, 0);
	dbClass.sqlout ((long *) &aufpt_verkfrage.txt_ls8, SQLLONG, 0);
	dbClass.sqlout ((long *) &aufpt_verkfrage.txt_ls9, SQLLONG, 0);
	dbClass.sqlout ((long *) &aufpt_verkfrage.txt_ls10, SQLLONG, 0);

	dbClass.sqlout ((long *) &aufpt_verkfrage.txt_rech1, SQLLONG, 0);
	dbClass.sqlout ((long *) &aufpt_verkfrage.txt_rech2, SQLLONG, 0);
	dbClass.sqlout ((long *) &aufpt_verkfrage.txt_rech3, SQLLONG, 0);
	dbClass.sqlout ((long *) &aufpt_verkfrage.txt_rech4, SQLLONG, 0);
	dbClass.sqlout ((long *) &aufpt_verkfrage.txt_rech5, SQLLONG, 0);
	dbClass.sqlout ((long *) &aufpt_verkfrage.txt_rech6, SQLLONG, 0);
	dbClass.sqlout ((long *) &aufpt_verkfrage.txt_rech7, SQLLONG, 0);	
	dbClass.sqlout ((long *) &aufpt_verkfrage.txt_rech8, SQLLONG, 0);
	dbClass.sqlout ((long *) &aufpt_verkfrage.txt_rech9, SQLLONG, 0);
	dbClass.sqlout ((long *) &aufpt_verkfrage.txt_rech10, SQLLONG, 0);

	dbClass.sqlout ((char *) ctxt_ls1, SQLCHAR, sizeof(atexte.disp_txt));
	dbClass.sqlout ((char *) ctxt_ls2, SQLCHAR, sizeof(atexte.disp_txt));
	dbClass.sqlout ((char *) ctxt_ls3, SQLCHAR, sizeof(atexte.disp_txt));
	dbClass.sqlout ((char *) ctxt_ls4, SQLCHAR, sizeof(atexte.disp_txt));
	dbClass.sqlout ((char *) ctxt_ls5, SQLCHAR, sizeof(atexte.disp_txt));
	dbClass.sqlout ((char *) ctxt_ls6, SQLCHAR, sizeof(atexte.disp_txt));
	dbClass.sqlout ((char *) ctxt_ls7, SQLCHAR, sizeof(atexte.disp_txt));
	dbClass.sqlout ((char *) ctxt_ls8, SQLCHAR, sizeof(atexte.disp_txt));
	dbClass.sqlout ((char *) ctxt_ls9, SQLCHAR, sizeof(atexte.disp_txt));
	dbClass.sqlout ((char *) ctxt_ls10, SQLCHAR, sizeof(atexte.disp_txt));

	dbClass.sqlout ((char *) ctxt_rech1, SQLCHAR, sizeof(atexte.disp_txt));
	dbClass.sqlout ((char *) ctxt_rech2, SQLCHAR, sizeof(atexte.disp_txt));
	dbClass.sqlout ((char *) ctxt_rech3, SQLCHAR, sizeof(atexte.disp_txt));
	dbClass.sqlout ((char *) ctxt_rech4, SQLCHAR, sizeof(atexte.disp_txt));
	dbClass.sqlout ((char *) ctxt_rech5, SQLCHAR, sizeof(atexte.disp_txt));
	dbClass.sqlout ((char *) ctxt_rech6, SQLCHAR, sizeof(atexte.disp_txt));
	dbClass.sqlout ((char *) ctxt_rech7, SQLCHAR, sizeof(atexte.disp_txt));	
	dbClass.sqlout ((char *) ctxt_rech8, SQLCHAR, sizeof(atexte.disp_txt));
	dbClass.sqlout ((char *) ctxt_rech9, SQLCHAR, sizeof(atexte.disp_txt));
	dbClass.sqlout ((char *) ctxt_rech10, SQLCHAR, sizeof(atexte.disp_txt));

	readcursor = dbClass.sqlcursor (
		_T(" select ")
		_T("  def1, def2, def3, def4, def5, def6, def7, def8, def9, def10 ") 
		_T(" ,txt_ls1, txt_ls2, txt_ls3, txt_ls4, txt_ls5, txt_ls6, txt_ls7, txt_ls8, txt_ls9, txt_ls10 ") 
		_T(" ,txt_rech1, txt_rech2, txt_rech3, txt_rech4, txt_rech5, txt_rech6, txt_rech7, txt_rech8, txt_rech9, txt_rech10 ") 
		_T(" ,( select disp_txt from atexte where txt_nr = txt_ls1)")
		_T(" ,( select disp_txt from atexte where txt_nr = txt_ls2)")
		_T(" ,( select disp_txt from atexte where txt_nr = txt_ls3)")
		_T(" ,( select disp_txt from atexte where txt_nr = txt_ls4)")
		_T(" ,( select disp_txt from atexte where txt_nr = txt_ls5)")
		_T(" ,( select disp_txt from atexte where txt_nr = txt_ls6)")
		_T(" ,( select disp_txt from atexte where txt_nr = txt_ls7)")
		_T(" ,( select disp_txt from atexte where txt_nr = txt_ls8)")
		_T(" ,( select disp_txt from atexte where txt_nr = txt_ls9)")
		_T(" ,( select disp_txt from atexte where txt_nr = txt_ls10)")

		_T(" ,( select disp_txt from atexte where txt_nr = txt_rech1)")
		_T(" ,( select disp_txt from atexte where txt_nr = txt_rech2)")
		_T(" ,( select disp_txt from atexte where txt_nr = txt_rech3)")
		_T(" ,( select disp_txt from atexte where txt_nr = txt_rech4)")
		_T(" ,( select disp_txt from atexte where txt_nr = txt_rech5)")
		_T(" ,( select disp_txt from atexte where txt_nr = txt_rech6)")
		_T(" ,( select disp_txt from atexte where txt_nr = txt_rech7)")
		_T(" ,( select disp_txt from atexte where txt_nr = txt_rech8)")
		_T(" ,( select disp_txt from atexte where txt_nr = txt_rech9)")
		_T(" ,( select disp_txt from atexte where txt_nr = txt_rech10)")
		_T(" from aufpt_verkfrage ")
		_T(" where ls = ? and mdn = ? and a = ? ")
	);
}
 
