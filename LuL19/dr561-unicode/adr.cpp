#include "stdafx.h"
#include "DbClass.h"
#include "adr.h"
#ifdef LL19
#include "cmbtll19.h"
#else
#ifdef LL12
#include "cmbtll12.h"
#else
#include "cmbtll11.h"
#endif
#endif

struct ADR adr,  adr_null;
struct LEER_LSDR leer_lsdr,  leer_lsdr_null;
struct ADR adr1, adr2, adr3, kunadr1;	// 011014
extern DB_CLASS dbClass;

int iadr ;
static int anzzfelder ;

int ADR_CLASS::dbcount (void)
/**
Tabelle adr lesen.
**/
{

         if (test_upd_cursor == -1)
         {
             prepare ();
         }
         dbClass.sqlopen (count_cursor);
         dbClass.sqlfetch (count_cursor);
         if (sqlstatus == 0)
         {
                      return anzzfelder;
         }
         return sqlstatus;   //050803
}

int ADR_CLASS::leseadr ()
{
	  long saveadr = adr.adr ;						// 211010 : sauberer initialisieren
	  memcpy ( &adr,&adr_null, sizeof(struct ADR));	// 211010 : sauberer initialisieren 

      int di = dbClass.sqlfetch (readcursor);
	  adr.adr = saveadr ;							// 211010 : sauberer initialisieren

		if (!di)
		{
  		  MultiByteToWideChar( 0,0, adr.adr_nam1 , -1 , adr.ucadr_nam1 ,sizeof(adr.ucadr_nam1));
 		  MultiByteToWideChar( 0,0, adr.adr_nam2 , -1 , adr.ucadr_nam2 ,sizeof(adr.ucadr_nam2));
 		  MultiByteToWideChar( 0,0, adr.adr_nam3 , -1 , adr.ucadr_nam3 ,sizeof(adr.ucadr_nam3));
 		  MultiByteToWideChar( 0,0, adr.ort1 , -1 , adr.ucort1 ,sizeof(adr.ucort1));
 		  MultiByteToWideChar( 0,0, adr.ort2 , -1 , adr.ucort2 ,sizeof(adr.ucort2));
 		  MultiByteToWideChar( 0,0, adr.partner , -1 , adr.ucpartner ,sizeof(adr.ucpartner));
 		  MultiByteToWideChar( 0,0, adr.pf , -1 , adr.ucpf ,sizeof(adr.ucpf));	// 111114 : mea culpa : hier stand ucplz
 		  MultiByteToWideChar( 0,0, adr.plz_pf , -1 , adr.ucplz_pf ,sizeof(adr.ucplz_pf));
 		  MultiByteToWideChar( 0,0, adr.plz , -1 , adr.ucplz ,sizeof(adr.ucplz));
 		  MultiByteToWideChar( 0,0, adr.str , -1 , adr.ucstr ,sizeof(adr.ucstr));
 		  MultiByteToWideChar( 0,0, adr.iln , -1 , adr.uciln ,sizeof(adr.uciln));
 		  MultiByteToWideChar( 0,0, adr.email , -1 , adr.ucemail ,sizeof(adr.ucemail));
 		  MultiByteToWideChar( 0,0, adr.swift , -1 , adr.ucswift ,sizeof(adr.ucswift));
 		  MultiByteToWideChar( 0,0, adr.mobil , -1 , adr.ucmobil ,sizeof(adr.ucmobil));
 		  MultiByteToWideChar( 0,0, adr.iban , -1 , adr.uciban ,sizeof(adr.uciban));
 		  MultiByteToWideChar( 0,0, adr.tel , -1 , adr.uctel ,sizeof(adr.uctel));
 		  MultiByteToWideChar( 0,0, adr.fax , -1 , adr.ucfax ,sizeof(adr.ucfax));

		}

	  return di;
}

int ADR_CLASS::openadr (void)
{

		if ( readcursor < 0 ) prepare ();	// ob das hier richtig ist ?

         return dbClass.sqlopen (readcursor);
}

void ADR_CLASS::prepare (void)
{


	dbClass.sqlin ((long *) &adr.adr, SQLLONG, 0);
    dbClass.sqlout ((long *)  &anzzfelder, SQLLONG, 0);


    count_cursor = dbClass.sqlcursor (_T("select count(*) from adr ")
										_T("where adr.adr = ? "));
										
	test_upd_cursor = 1;


	dbClass.sqlin ((long *) &adr.adr, SQLLONG, 0);

	dbClass.sqlout ((long *) &adr.adr,SQLLONG, 0);
	dbClass.sqlout ((TCHAR *) adr.adr_krz,SQLCHAR, 17);
	dbClass.sqlout ((TCHAR *) adr.adr_nam1, SQLCHAR, 37);
	dbClass.sqlout ((TCHAR *) adr.adr_nam2, SQLCHAR, 37);
	dbClass.sqlout ((TCHAR *) adr.adr_nam3, SQLCHAR, 37);
	dbClass.sqlout ((short *) &adr.adr_typ, SQLSHORT, 0);
	dbClass.sqlout ((TCHAR *) adr.adr_verkt, SQLCHAR, 17);
	dbClass.sqlout ((short *) &adr.anr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &adr.delstatus, SQLSHORT, 0);
	dbClass.sqlout ((TCHAR *) adr.fax, SQLCHAR, 21);
	dbClass.sqlout ((short *) &adr.fil, SQLSHORT,0);
	dbClass.sqlout ((TCHAR *) adr.geb_dat, SQLCHAR, 12);
	dbClass.sqlout ((short *) &adr.land, SQLSHORT, 0);
	dbClass.sqlout ((short *) &adr.mdn, SQLSHORT,  0);
	dbClass.sqlout ((TCHAR *) adr.merkm_1, SQLCHAR, 3);
	dbClass.sqlout ((TCHAR *) adr.merkm_2, SQLCHAR, 3);
	dbClass.sqlout ((TCHAR *) adr.merkm_3, SQLCHAR, 3);
	dbClass.sqlout ((TCHAR *) adr.merkm_4, SQLCHAR, 3);
	dbClass.sqlout ((TCHAR *) adr.merkm_5, SQLCHAR, 3);
	dbClass.sqlout ((TCHAR *) adr.modem, SQLCHAR, 21);
	dbClass.sqlout ((TCHAR *) adr.ort1, SQLCHAR, 37);
	dbClass.sqlout ((TCHAR *) adr.ort2, SQLCHAR, 37);

	dbClass.sqlout ((TCHAR *) adr.partner, SQLCHAR, 37);
	dbClass.sqlout ((TCHAR *) adr.pf, SQLCHAR, 17);
	dbClass.sqlout ((TCHAR *) adr.plz, SQLCHAR, 9);
	dbClass.sqlout ((short *) &adr.staat, SQLSHORT, 0);
	dbClass.sqlout ((TCHAR *) adr.str,SQLCHAR, 37);

	dbClass.sqlout ((TCHAR *) adr.tel, SQLCHAR, 21);
	dbClass.sqlout ((TCHAR *) adr.telex, SQLCHAR, 21);
	dbClass.sqlout ((long *) &adr.txt_nr, SQLLONG, 0);

	dbClass.sqlout ((TCHAR *) adr.plz_postf, SQLCHAR,9);
	dbClass.sqlout ((TCHAR *) adr.plz_pf,SQLCHAR, 9);
	dbClass.sqlout ((TCHAR *) adr.iln, SQLCHAR, 33 );
	dbClass.sqlout ((TCHAR *) adr.email, SQLCHAR, 37);
	dbClass.sqlout ((TCHAR *) adr.swift, SQLCHAR, 25);
	dbClass.sqlout ((TCHAR *) adr.mobil, SQLCHAR, 21);	// 301111
	dbClass.sqlout ((TCHAR *) adr.iban, SQLCHAR, 25);	// 300813

	readcursor = dbClass.sqlcursor (_T("select ")

	_T(" adr, adr_krz, adr_nam1, adr_nam2,adr_nam3, adr_typ, adr_verkt, anr, ") 
	_T(" delstatus, fax, fil, geb_dat, land, mdn, merkm_1, merkm_2, ")
	_T(" merkm_3, merkm_4, merkm_5, modem, ort1, ort2, ")
	_T(" partner, pf, plz, staat, str, tel, telex, txt_nr, plz_postf, ")
	_T(" plz_pf, iln, email, swift,mobil,iban ")
	_T(" from adr where adr = ? ") ) ;
	

}

// 140307 
int LEER_LSDR_CLASS::leseleer_lsdr ()
{
      int di = dbClass.sqlfetch (readcursor);

	  return di;
}

int LEER_LSDR_CLASS::openleer_lsdr (void)
{

		if ( readcursor < 0 ) prepare ();	// ob das hier richtig ist ?

         return dbClass.sqlopen (readcursor);
}

void LEER_LSDR_CLASS::prepare (void)
{

	dbClass.sqlin ((short *) &leer_lsdr.mdn, SQLSHORT, 0);
	dbClass.sqlin ((long *) &leer_lsdr.ls, SQLLONG, 0);
    dbClass.sqlin ((char *) leer_lsdr.blg_typ, SQLCHAR, 2);

 	test_upd_cursor = 1;


	dbClass.sqlout ((short *) &leer_lsdr.fil,SQLSHORT,0) ;
	dbClass.sqlout ((double *) &leer_lsdr.a, SQLDOUBLE,0)  ;
	dbClass.sqlout ((long *) &leer_lsdr.me_stk_zu, SQLLONG ,0) ;
	dbClass.sqlout ((long *) &leer_lsdr.me_stk_abn, SQLLONG ,0) ;
	dbClass.sqlout ((long *) &leer_lsdr.stk, SQLLONG ,0) ;
	dbClass.sqlout ((short *) &leer_lsdr.stat, SQLSHORT ,0) ;

		readcursor = dbClass.sqlcursor (_T("select ")
		_T(" fil, a, me_stk_zu, me_stk_abn, stk, stat ") 
		_T(" from leer_lsdr where ")
		_T(" mdn = ? and ls = ? and blg_typ = ? ")
		_T(" order by a ") );
		

}

