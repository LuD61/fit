#ifndef _ZUSTAB_DEF
#define _ZUSTAB_DEF

struct SYS_PAR {

char sys_par_nam[21];	// Texte lieber zu lang als zu kurz beim lesen
char sys_par_wrt[3];
char sys_par_besch[37];

TCHAR ucsys_par_nam[21];
TCHAR ucsys_par_wrt[3];
TCHAR ucsys_par_besch[37];


long zei;
short delstatus;
};
extern struct SYS_PAR sys_par, sys_par_null;


class SYS_PAR_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
//              int dbcount (void);
               int lesesys_par (int);
               int opensys_par (void);
               SYS_PAR_CLASS () : DB_CLASS ()
               {
               }
};

struct A_ZUS_TXT {

double a;
long zei;
short sg1;
char txt[61];
TCHAR uctxt[61];
};
extern struct A_ZUS_TXT a_zus_txt, a_zus_txt_null;


class A_ZUS_TXT_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
//              int dbcount (void);
               int lesea_zus_txt (int);
               int opena_zus_txt (void);
               A_ZUS_TXT_CLASS () : DB_CLASS ()
               {
               }
};

// 110506 
struct PTABN {

char ptwert[4];
char ptitem[19];
long ptlfnr ;
char ptbez[33];
char ptbezk[9];
char ptwer1[9];
char ptwer2[9];

TCHAR ucptwert[4];
TCHAR ucptitem[19];
TCHAR ucptbez[33];
TCHAR ucptbezk[9];
TCHAR ucptwer1[9];
TCHAR ucptwer2[9];



};

extern struct PTABN ptabn, ptabn_null;


class PTABN_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int leseptabn (void);
               int openptabn (void);
               PTABN_CLASS () : DB_CLASS ()
               {
               }
};

struct A_KUN_TXT {
long	kun ;
long	a ;	// ACHTUNG ACHTUNG
char	txt1[129] ;
char	txt2[129] ;
char	txt3[129] ;
char	txt4[129] ;
char	txt5[129] ;

TCHAR	uctxt1[129] ;
TCHAR	uctxt2[129] ;
TCHAR	uctxt3[129] ;
TCHAR	uctxt4[129] ;
TCHAR	uctxt5[129] ;
short sort ;

};
extern struct A_KUN_TXT a_kun_txt, a_kun_txt_null;

class A_KUN_TXT_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int lesea_kun_txt (void);
               int opena_kun_txt (void);
               A_KUN_TXT_CLASS () : DB_CLASS ()
               {
               }
};

// 190214 : a_bas_erw zugef�gt
struct A_BAS_ERW {
	double a;	// decimal(13,0),
    char pp_a_bz1[100];	// TCHAR(99),
    char pp_a_bz2[100];	// TCHAR(99),
    char lgr_tmpr[100];	// TCHAR(99),
    char lupine[3];	// TCHAR(1),
    char schutzgas[3];	// TCHAR(1),

    TCHAR ucpp_a_bz1[100];	// TCHAR(99),
    TCHAR ucpp_a_bz2[100];	// TCHAR(99),
    TCHAR uclgr_tmpr[100];	// TCHAR(99),
    TCHAR uclupine[3];	// TCHAR(1),
    TCHAR ucschutzgas[3];	// TCHAR(1),
	
	short huelle;	// smallint,
    short shop_wg1;	// smallint,
    short shop_wg2;	//  smallint,
    short shop_wg3;	// smallint,
    short shop_wg4;	// smallint,
    short shop_wg5;	// smallint,
    double tara2;	// decimal(8,3),
    short a_tara2;	// smallint,
    double tara3;	// decimal(8,3),
    short a_tara3;	// smallint,
    double tara4;	// decimal(8,3),
    short a_tara4;	// smallint,
    double salz;	// decimal(8,3),
    double davonfett;	// decimal(8,3),
    double davonzucker;	// decimal(8,3),
    double ballaststoffe;	// decimal(8,3),
    char shop_aktion[3];	// TCHAR(1),
	char shop_neu[3];	// TCHAR(1),
    char shop_tv[3];	// TCHAR(1),

    TCHAR ucshop_aktion[3];	// TCHAR(1),
	TCHAR ucshop_neu[3];	// TCHAR(1),
    TCHAR ucshop_tv[3];	// TCHAR(1),

    double shop_agew;	// decimal(8,3),
    char a_bez[65];	// TCHAR(62),
    char zutat[2002];	// TCHAR(2000),
    TCHAR uca_bez[65];	// TCHAR(62),
    TCHAR uczutat[2002];	// TCHAR(2000),
    short userdef1;	// smallint,
    short userdef2;	// smallint,
	short userdef3;	// smallint,
    short minstaffgr;	// smallint,
    short l_pack;	// smallint,
    short b_pack;	// smallint,
    short h_pack;	// smallint

};
extern struct A_BAS_ERW a_bas_erw, a_bas_erw_null;

class A_BAS_ERW_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int lesea_bas_erw (void);
               int opena_bas_erw (void);
               A_BAS_ERW_CLASS () : DB_CLASS ()
               {
               }
};

#endif

