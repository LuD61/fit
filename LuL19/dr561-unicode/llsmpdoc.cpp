//=============================================================================
//
//  Project: List & Label
//           Copyright (c) 1998-2002 combit GmbH, All Rights Reserved
//
//  Authors: combit Software Team
//
//-----------------------------------------------------------------------------
//
//  Module:  LLMFC - List & Label MFC Sample Application
//
//=============================================================================

// llsmpdoc.cpp : implementation of the CLlSampleDoc class

#include "stdafx.h"

#include "llmfc.h"
#include "llsmpdoc.h"

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLlSampleDoc

IMPLEMENT_DYNCREATE(CLlSampleDoc, CDocument)

BEGIN_MESSAGE_MAP(CLlSampleDoc, CDocument)
	//{{AFX_MSG_MAP(CLlSampleDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLlSampleDoc construction/destruction

CLlSampleDoc::CLlSampleDoc()
{
	// TODO: add one-time construction code here
}

CLlSampleDoc::~CLlSampleDoc()
{
}

BOOL CLlSampleDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;
	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CLlSampleDoc serialization

void CLlSampleDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}


/////////////////////////////////////////////////////////////////////////////
// CLlSampleDoc diagnostics

#ifdef _DEBUG
void CLlSampleDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CLlSampleDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CLlSampleDoc commands
