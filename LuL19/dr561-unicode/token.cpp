#include "stdafx.h"
#include "Token.h"


CToken::CToken ()
{
     Tokens   = NULL;
     AnzToken = NULL;
     AktToken = NULL;
}

CToken::CToken (TCHAR *Txt, TCHAR *sep)	// unicode
{
     Tokens = NULL;
     Buffer = Txt;
     Buffer.TrimRight ();
     this->sep = sep;
     GetTokens (Txt);
}

CToken::CToken (CString& Txt, TCHAR *sep)	// unicode
{
     Tokens = NULL;
     Buffer = Txt;
     Buffer.TrimRight ();
     this->sep = sep;
     GetTokens ( Txt.GetBuffer (512));
}


CToken::~CToken ()
{
     if (Tokens != NULL)
     {
         for (int i = 0; i < AnzToken; i ++)
         {
               delete Tokens[i];
         }
         delete Tokens;
     }
}

const CToken& CToken::operator=(TCHAR *Txt)
{
     if (Tokens == NULL)
     {
           for (int i = 0; i < AnzToken; i ++)
           {
               delete Tokens[i];
           }
           delete Tokens;
           Tokens = NULL;
     }
     Buffer = Txt;
     GetTokens (Txt);
     return *this;
}

const CToken& CToken::operator=(CString& Txt)
{
     if (Tokens == NULL)
     {
           for (int i = 0; i < AnzToken; i ++)
           {
               delete Tokens[i];
           }
           delete Tokens;
           Tokens = NULL;
     }
     Buffer = Txt;
     GetTokens (Txt.GetBuffer (512));
     return *this;
}

void CToken::GetTokens (TCHAR *Txt)
{
     AnzToken = 0;
     wchar_t *b = Buffer.GetBuffer (512);
//     TCHAR *p = strtok (b, sep.GetBuffer (512));
     wchar_t *p = wcstok (b, sep.GetBuffer (512));
     while (p != NULL)
     {
             AnzToken ++;
//             p = strtok (NULL, sep.GetBuffer (512));
             p = wcstok (NULL, sep.GetBuffer (512));
     }
     Tokens = new CString *[AnzToken];
     if (Tokens == NULL)
     {
             return;
     }

     Buffer = Txt;
     Buffer.TrimRight ();
     b = Buffer.GetBuffer (512);
//     p = strtok (b, sep.GetBuffer (512));
     p = wcstok (b, sep.GetBuffer (512));
     int i = 0;
     while (p != NULL)
     {
             Tokens[i] = new CString (p);
//             p = strtok (NULL, sep.GetBuffer (512));
             p = wcstok (NULL, sep.GetBuffer (512));
             i ++;
     }
     AktToken = 0;
}

void CToken::SetSep (TCHAR *sep)
{
     this->sep = sep;
}

TCHAR * CToken::NextToken (void)
{
     if (AktToken == AnzToken)
     {
          AktToken = 0;
          return NULL;
     }
     AktToken ++;
     return Tokens[AktToken - 1]->GetBuffer (512);
}

TCHAR * CToken::GetToken (int idx)
{
     if (idx < 0 || idx >= AnzToken)
     {
           return NULL;
     }

     return Tokens [idx]->GetBuffer (512);
}

int CToken::GetAnzToken (void)
{
     return AnzToken;
}
