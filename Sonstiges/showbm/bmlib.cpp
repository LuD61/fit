#include <windows.h>
#include "bmlib.h"
#include "wmaskc.h"
#include "mo_bm.h"


HWND hMainWindow;

extern BmClass BitMap;

int WINAPI DllMain (HINSTANCE hInstance, DWORD fdwReason, 
                                        PVOID pvReserved)
{
        return TRUE;
}

        
EXPORT BOOL  _ShowBm (char *params)
{
        
	  BitMap.Showbm (&hMainWindow, params);
      return TRUE;
}

EXPORT BOOL  _DestroyBm (void)
{
        
	  BitMap.DestroyBMWindow ();
      return TRUE;
}

EXPORT BOOL  _MoveBm (void)
{
        
	  BitMap.OnMove (hMainWindow); 
      return TRUE;
}
