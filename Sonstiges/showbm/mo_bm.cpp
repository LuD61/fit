#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include "comcthlp.h"
#include "itemc.h"
#include "item.h"
#include "wmaskc.h"
#include "mo_draw.h"
#include "mo_meld.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_bm.h"
#include "mo_arg.h"
#include "dbclass.h"

LONG FAR PASCAL WndProcBm(HWND,UINT,WPARAM,LPARAM);

static int bmm = 0;
static char caption [256] = "Bitmap Reader";
static int captmode = 1;
static int bmrow    = CW_USEDEFAULT;
static int bmcolumn = CW_USEDEFAULT;
static int bmcrow   = CW_USEDEFAULT;
static int bmccolumn = CW_USEDEFAULT;
static int bmtopmost = 0;

	    char *etc;
		char buffer [512];
		FILE *fp;
		double a_bas_a ;
		char ctext [50];
 int Savemode = 0;

static DB_CLASS DbClass;

static char bmp_pfad [256];  

static void tst_arg (char *arg)
{
          for (; *arg; arg += 1)
          {
                 switch (*arg)
                 {
                            case 'b' :
                                     captmode = atoi (arg + 1);
                                     return;
                            case 'c' :
                                     strcpy (caption, arg + 1);
                                     return;
                            case 'm' :
                                     bmm = atoi (arg + 1); 
                                     return;
                            case 'n' :
                                     captmode = 0;
                                     break;
                            case 'z' :
                                     bmrow = atoi (arg + 1); 
                                     return;
                            case 's' :
                                     bmcolumn = atoi (arg + 1); 
                                     return;
                            case 'S' :
                                     Savemode = 1;
									 sprintf (ctext,"Savemode = %ld",Savemode);
                                     break;

                            case 'h' :
                                     bmcrow = atoi (arg + 1); 
                                     return;
                            case 'w' :
                                     bmccolumn = atoi (arg + 1); 
                                     return;
                            case 'v' :
                                     bmtopmost = 1; 
                                     break;
                 }
          }
          return;
}

void BmClass::DestroyBMWindow (void)
/**
BitmapWindow schliessen.
**/
{
       if (mamain2) 
       {
           DestroyWindow (mamain2);
           mamain2 = NULL;
       }
       if (this->hMainWindow)
       {
           DestroyWindow (this->hMainWindow);
           this->hMainWindow = NULL;
       }
}

HWND BmClass::InitBMWindow (HWND hMainWindow)
/**
Listfenster oeffnen.
**/
{
       int x,y;
       int cx,cy;
       RECT rect;


       if (mamain2)
       {
           DestroyWindow (mamain2);
       }

       this->hMainWindow = hMainWindow;
       this->hMainInst = (HANDLE) GetWindowLong (hMainWindow, 
                                               GWL_HINSTANCE);
       y = 0;
       x = 0;
       GetClientRect (hMainWindow, &rect);
       cy = rect.bottom;
       cx = rect.right;

       mamain2 = CreateWindowEx (
//                                WS_EX_CLIENTEDGE, 
                                0,
                                "hBmWindow",
                                "",       
                                WS_CHILD | WS_VISIBLE,
                                x, y,
                                cx, cy,
                                hMainWindow,
                                NULL,
                                hMainInst,
                                NULL);

        SetClassLong (mamain2,GCL_HBRBACKGROUND, 
                         (long) CreateSolidBrush (BkColor));
        bmphWnd = mamain2;
        return mamain2;
}

BOOL BmClass::BmToScr (char *bmfile)
/**
Bitmap lesen und auf Bildschirm anzeigen.
**/
{
        HDC hdc;

        if (mamain2 == NULL) return FALSE; 
        hdc = GetDC (mamain2);  
        hBitmap = ReadBitmap (hdc, bmfile);
        if (hBitmap == NULL) return FALSE;
        DrawBitmap (hdc, hBitmap, 0, 0);
        bmx = bmy = 0;
        TestTrack ();
        TestVTrack ();
        return TRUE;
}

BOOL BmClass::SetBmToScr (char *bmfile)
/**
Bitmap lesen und auf Bildschirm anzeigen.
**/
{
        HDC hdc;

        hdc = GetDC (mamain2);  
        hBitmap = ReadBitmap (hdc, bmfile);
        if (hBitmap == NULL) return FALSE;
        bmx = bmy = 0;
        TestTrack ();
        TestVTrack ();
        return TRUE;
}

BOOL BmClass::BmToScrEx (char *bmfile)
/**
Groesse des Window an Bitmap anpassen und anzeigen.
**/
{
        HDC hdc;
        RECT rect, crect;
        BITMAP bm;
        int px, py;
        int bytes;

        if (hMainWindow == NULL) return FALSE; 
        if (mamain2 == NULL) return FALSE; 

        hdc = GetDC (mamain2);  
        hBitmap = ReadBitmap (hdc, bmfile);
        if (hBitmap == NULL) return FALSE;

        GetWindowRect (hMainWindow, &rect);
        GetClientRect (hMainWindow, &crect);
        px = rect.right - rect.left - crect.right + 2;
        py = rect.bottom - rect.top - crect.bottom + 2;
        bytes = GetObject (hBitmap, sizeof (BITMAP), &bm);
        rect.right  = bm.bmWidth + px;
        rect.bottom = bm.bmHeight + py;
        MoveWindow (hMainWindow, rect.left, rect.top,
                                 rect.right, rect.bottom,
                                 TRUE);

        DrawBitmap (hdc, hBitmap, 0, 0);
        bmx = bmy = 0;
        TestTrack ();
        TestVTrack ();
        return TRUE;
}

BOOL BmClass::SetBmToScrEx (char *bmfile)
/**
Groesse des Window an Bitmap anpassen und anzeigen.
**/
{
        HDC hdc;
        RECT rect, crect;
        BITMAP bm;
        int px, py;
        int bytes;

        if (hMainWindow == NULL) return FALSE; 
        if (mamain2 == NULL) return FALSE; 

        hdc = GetDC (mamain2);  
        hBitmap = ReadBitmap (hdc, bmfile);
        if (hBitmap == NULL) return FALSE;

        GetWindowRect (hMainWindow, &rect);
        GetClientRect (hMainWindow, &crect);
        px = rect.right - rect.left - crect.right + 2;
        py = rect.bottom - rect.top - crect.bottom + 2;
        bytes = GetObject (hBitmap, sizeof (BITMAP), &bm);
        rect.right  = bm.bmWidth + px;
        rect.bottom = bm.bmHeight + py;
        MoveWindow (hMainWindow, rect.left, rect.top,
                                 rect.right, rect.bottom,
                                 TRUE);

        bmx = bmy = 0;
        TestTrack ();
        TestVTrack ();
        return TRUE;
}


BOOL BmClass::BmToScrCe (char *bmfile)
/**
Bitmap in Window zentrieren.
**/
{
        HDC hdc;
        RECT rect;
        BITMAP bm;
        int bytes;

        if (hMainWindow == NULL) return FALSE; 
        if (mamain2 == NULL) return FALSE; 

        hdc = GetDC (mamain2);  
        hBitmap = ReadBitmap (hdc, bmfile);
        if (hBitmap == NULL) return FALSE;

        GetClientRect (hMainWindow, &rect);
        bytes = GetObject (hBitmap, sizeof (BITMAP), &bm);
        bmx = max (0, (rect.right  - bm.bmWidth)  / 2);
        bmy = max (0, (rect.bottom - bm.bmHeight) / 2);

       
        DrawBitmap (hdc, hBitmap, bmx, bmy);
        bmmode = 0;
        TestTrack ();
        TestVTrack ();
        return TRUE;
}

BOOL BmClass::SetBmToScrCe (char *bmfile)
/**
Bitmap in Window zentrieren.
**/
{
        HDC hdc;
        RECT rect;
        BITMAP bm;
        int bytes;

        if (hMainWindow == NULL) return FALSE; 
        if (mamain2 == NULL) return FALSE; 

        hdc = GetDC (mamain2);  
        hBitmap = ReadBitmap (hdc, bmfile);
        if (hBitmap == NULL) return FALSE;

        GetClientRect (hMainWindow, &rect);
        bytes = GetObject (hBitmap, sizeof (BITMAP), &bm);
        bmx = max (0, (rect.right  - bm.bmWidth)  / 2);
        bmy = max (0, (rect.bottom - bm.bmHeight) / 2);

        bmmode = 0;
        TestTrack ();
        TestVTrack ();
        return TRUE;
}


BOOL BmClass::BmToScrStr (char *bmfile)
/**
Groesse der Bitmap dem Window anpassen.
**/
{
        HDC hdc;
        RECT rect;

        if (hMainWindow == NULL) return FALSE; 
        if (mamain2 == NULL) return FALSE; 

        hdc = GetDC (mamain2);  
        hBitmap = ReadBitmap (hdc, bmfile);
        if (hBitmap == NULL) return FALSE;

        GetClientRect (hMainWindow, &rect);

        bmcx = rect.right;
        bmcy = rect.bottom;
        StrechBitmap (hdc, bmcx, bmcy);
        bmmode = 2;
        return TRUE;
}

BOOL BmClass::SetBmToScrStr (char *bmfile)
/**
Groesse der Bitmap dem Window anpassen.
**/
{
        HDC hdc;
        RECT rect;

        if (hMainWindow == NULL) return FALSE; 
        if (mamain2 == NULL) return FALSE; 

        hdc = GetDC (mamain2);  
        hBitmap = ReadBitmap (hdc, bmfile);
        if (hBitmap == NULL) return FALSE;

        GetClientRect (hMainWindow, &rect);

        bmcx = rect.right;
        bmcy = rect.bottom;
        bmmode = 2;
        return TRUE;
}

BOOL BmClass::SetBmToScrStrX (char *bmfile)
/**
Groesse der Bitmap dem Window anpassen.
**/
{
        HDC hdc;
        RECT rect;
		int bytes;
        BITMAP bm;
		int x, y, cx, cy;

        if (hMainWindow == NULL) return FALSE; 
        if (mamain2 == NULL) return FALSE; 

        hdc = GetDC (mamain2);  
        hBitmap = ReadBitmap (hdc, bmfile);
        if (hBitmap == NULL) return FALSE;

        GetWindowRect (hMainWindow, &rect);
        bytes = GetObject (hBitmap, sizeof (BITMAP), &bm);
        cx = bm.bmWidth + 50;
		x = rect.right - cx;
		y = rect.top;
		cy = rect.bottom - rect.top;

		MoveWindow (hMainWindow, x, y, cx, cy, TRUE);

        GetClientRect (hMainWindow, &rect);
//        bmcx = bm.bmWidth;
//        bmcy = bm.bmHeight;
        bmmode = 3;
        return TRUE;
}

void BmClass::OnMove (HWND MainWind)
/**
Aktion bei WM_PAINT-Meldung.
**/
{
        RECT rect;
        int x,y, cx, cy;

        if (hMainWindow == NULL) return;
        GetWindowRect (MainWind, &rect);
        x = rect.left - bxdiff;
        y = rect.top  - bydiff;
        cx = bcx;
        cy = bcy;
        MoveWindow (hMainWindow, x,y, cx, cy, TRUE);
}

BOOL BmClass::OnPaint (HWND hWnd)
/**
Aktion bei WM_PAINT-Meldung.
**/
{
        PAINTSTRUCT ps;
        HDC hdc;
        RECT rect;
        BITMAP bm;
		int bytes;
//		double fx, fy;
        

        if (hWnd != mamain2) return FALSE;
        if (mamain2 == NULL) return FALSE;

        GetClientRect (mamain2, &rect);
        hdc = BeginPaint (mamain2, &ps);
        if (bmmode == 2)
        {
               StrechBitmap (hdc, bmcx, bmcy);
        }
		else if (bmmode == 3)
		{
               bytes = GetObject (hBitmap, sizeof (BITMAP), &bm);
               DrawBitmapPart (hdc, 0, 0, scrollpos, scrollvpos,
                               bm.bmWidth, bm.bmHeight);
        } 
        else
        {
               ZoomBitmap (hdc, bmx, bmy, scrollpos, scrollvpos,
                                rect.right, rect.bottom, Zoom);
        }
        EndPaint (mamain2, &ps);

        return TRUE;
}

void BmClass::ScrollDown (void)
/**
Eine Zeile nach unten Scrollen.
**/
{
         HDC hdc;
         RECT rect;
         RECT vrect;
         RECT hrect;
         int bytes;
         BITMAP bm;

         GetClientRect (mamain2, &rect);
         bytes = GetObject (hBitmap, sizeof (BITMAP), &bm);
         if (vTrack)
         {
                GetClientRect (vTrack, &vrect);
                rect.right -= vrect.right;
         }
         if (hTrack)
         {
                GetClientRect (hTrack, &hrect);
                rect.bottom -= hrect.bottom;
         }

//         scrollvpos += nvPage;
         scrollvpos ++;
         if (scrollvpos > nvMax)
         {
                 scrollvpos = nvMax;
         }

         SetScrollPos (vTrack, SB_CTL, scrollvpos, TRUE);

         hdc = GetDC (mamain2);
/*
         DrawBitmapPart (hdc, bmx, bmy, scrollpos, scrollvpos,
                         rect.right, rect.bottom);
*/
         ZoomBitmap (hdc, bmx, bmy, scrollpos, scrollvpos,
                         rect.right, rect.bottom, Zoom);
         ReleaseDC (mamain2, hdc);
}
         
void BmClass::ScrollUp (void)
/**
Eine Zeile nach unten Scrollen.
**/
{
         HDC hdc;
         RECT rect;
         RECT vrect;
         RECT hrect;

         GetClientRect (mamain2, &rect);
         if (vTrack)
         {
                GetClientRect (vTrack, &vrect);
                rect.right -= vrect.right;
         }
         if (hTrack)
         {
                GetClientRect (hTrack, &hrect);
                rect.bottom -= hrect.bottom;
         }

//         scrollvpos -= nvPage;
         scrollvpos -= 1;
         if (scrollvpos < 0)
         {
                 scrollvpos = 0;
         }
         SetScrollPos (vTrack, SB_CTL, scrollvpos, TRUE);
         hdc = GetDC (mamain2);
/*
         DrawBitmapPart (hdc, bmx, bmy, scrollpos, scrollvpos,
                         rect.right, rect.bottom);
*/
         ZoomBitmap (hdc, bmx, bmy, scrollpos, scrollvpos,
                         rect.right, rect.bottom, Zoom);
         ReleaseDC (mamain2, hdc);
}
         
void BmClass::ScrollVPageDown (void)
/**
Eine Zeile nach unten Scrollen.
**/
{
         HDC hdc;
         RECT rect;
         RECT vrect;
         RECT hrect;
         int bytes;
         BITMAP bm;

         GetClientRect (mamain2, &rect);
         bytes = GetObject (hBitmap, sizeof (BITMAP), &bm);
         if (vTrack)
         {
                GetClientRect (vTrack, &vrect);
                rect.right -= vrect.right;
         }
         if (hTrack)
         {
                GetClientRect (hTrack, &hrect);
                rect.bottom -= hrect.bottom;
         }

         scrollvpos += nvPage;
         if (scrollvpos > nvMax)
         {
                 scrollvpos = nvMax;
         }

         SetScrollPos (vTrack, SB_CTL, scrollvpos, TRUE);

         hdc = GetDC (mamain2);
/*
         DrawBitmapPart (hdc, bmx, bmy, scrollpos, scrollvpos,
                         rect.right, rect.bottom);
*/
         ZoomBitmap (hdc, bmx, bmy, scrollpos, scrollvpos,
                         rect.right, rect.bottom, Zoom);
         ReleaseDC (mamain2, hdc);
}
         
void BmClass::ScrollVPageUp (void)
/**
Eine Zeile nach unten Scrollen.
**/
{
         HDC hdc;
         RECT rect;
         RECT vrect;
         RECT hrect;

         GetClientRect (mamain2, &rect);
         if (vTrack)
         {
                GetClientRect (vTrack, &vrect);
                rect.right -= vrect.right;
         }
         if (hTrack)
         {
                GetClientRect (hTrack, &hrect);
                rect.bottom -= hrect.bottom;
         }

         scrollvpos -= nvPage;
         if (scrollvpos < 0)
         {
                 scrollvpos = 0;
         }
         SetScrollPos (vTrack, SB_CTL, scrollvpos, TRUE);
         hdc = GetDC (mamain2);
/*
         DrawBitmapPart (hdc, bmx, bmy, scrollpos, scrollvpos,
                         rect.right, rect.bottom);
*/
         ZoomBitmap (hdc, bmx, bmy, scrollpos, scrollvpos,
                         rect.right, rect.bottom, Zoom);
         ReleaseDC (mamain2, hdc);
}
         
void BmClass::SetVPos (int pos)
/**
Position setzen.
**/
{
         HDC hdc;
         RECT rect;
         RECT vrect;
         RECT hrect;

         GetClientRect (mamain2, &rect);
         if (vTrack)
         {
                GetClientRect (vTrack, &vrect);
                rect.right -= vrect.right;
         }
         if (hTrack)
         {
                GetClientRect (hTrack, &hrect);
                rect.bottom -= hrect.bottom;
         }
         scrollvpos = pos;
         SetScrollPos (vTrack, SB_CTL, scrollvpos,
                                   TRUE);
         hdc = GetDC (mamain2);
/*
         DrawBitmapPart (hdc, bmx, bmy, scrollpos, scrollvpos,
                         rect.right, rect.bottom);
*/
         ZoomBitmap (hdc, bmx, bmy, scrollpos, scrollvpos,
                         rect.right, rect.bottom, Zoom);
         ReleaseDC (mamain2, hdc);
}
                      

void BmClass::VScroll (WPARAM wParam, LPARAM lParam)
/**
Meldung WM_HSCROLL erhalten.
**/
{
         switch (LOWORD (wParam))
         {
             case SB_LINEDOWN :
             {    
                     ScrollDown ();
                     break;
             }
             case SB_LINEUP :
             {    
                     ScrollUp ();
                     break;
             }

             case SB_PAGEDOWN :
             {    
                     ScrollVPageDown ();
                     break;
             }
             case SB_PAGEUP :
             {    
                     ScrollVPageUp ();
                     break;
             }

             case SB_THUMBPOSITION :
             case SB_THUMBTRACK :
                     SetVPos (HIWORD (wParam));
                     break;
         }
}

void BmClass::ScrollRight (void)
/**
Eine Zeile nach rechts Scrollen.
**/
{
         HDC hdc;
         RECT rect;
         RECT vrect;
         RECT hrect;

         GetClientRect (mamain2, &rect);
         if (vTrack)
         {
                GetClientRect (vTrack, &vrect);
                rect.right -= vrect.right;
         }
         if (hTrack)
         {
                GetClientRect (hTrack, &hrect);
                rect.bottom -= hrect.bottom;
         }

//         scrollpos += nhPage;
         scrollpos ++;
         if (scrollpos > nhMax)
         {
                 scrollpos = nhMax;
         }

         SetScrollPos (hTrack, SB_CTL, scrollpos, TRUE);

         hdc = GetDC (mamain2);
/*
         DrawBitmapPart (hdc, bmx, bmy, scrollpos, scrollvpos,
                         rect.right, rect.bottom);
*/
         ZoomBitmap (hdc, bmx, bmy, scrollpos, scrollvpos,
                         rect.right, rect.bottom, Zoom);
         ReleaseDC (mamain2, hdc);
}
         
void BmClass::ScrollLeft (void)
/**
Eine Zeile nach links Scrollen.
**/
{
         HDC hdc;
         RECT vrect;
         RECT hrect;


         RECT rect;

         GetClientRect (mamain2, &rect);
         if (vTrack)
         {
                GetClientRect (vTrack, &vrect);
                rect.right -= vrect.right;
         }
         if (hTrack)
         {
                GetClientRect (hTrack, &hrect);
                rect.bottom -= hrect.bottom;
         }

         scrollpos --;
         if (scrollpos < 0)
         {
                 scrollpos = 0;
         }
         SetScrollPos (hTrack, SB_CTL, scrollpos, TRUE);
         hdc = GetDC (mamain2);
/*
         DrawBitmapPart (hdc, bmx, bmy, scrollpos, scrollvpos,
                         rect.right, rect.bottom);
*/
         ZoomBitmap (hdc, bmx, bmy, scrollpos, scrollvpos,
                         rect.right, rect.bottom, Zoom);
         ReleaseDC (mamain2, hdc);
}


void BmClass::ScrollHPageDown (void)
/**
Eine Zeile nach rechts Scrollen.
**/
{
         HDC hdc;
         RECT rect;
         RECT vrect;
         RECT hrect;

         GetClientRect (mamain2, &rect);
         if (vTrack)
         {
                GetClientRect (vTrack, &vrect);
                rect.right -= vrect.right;
         }
         if (hTrack)
         {
                GetClientRect (hTrack, &hrect);
                rect.bottom -= hrect.bottom;
         }

         scrollpos += nhPage;
         if (scrollpos > nhMax)
         {
                 scrollpos = nhMax;
         }

         SetScrollPos (hTrack, SB_CTL, scrollpos, TRUE);

         hdc = GetDC (mamain2);
/*
         DrawBitmapPart (hdc, bmx, bmy, scrollpos, scrollvpos,
                         rect.right, rect.bottom);
*/

         ZoomBitmap (hdc, bmx, bmy, scrollpos, scrollvpos,
                         rect.right, rect.bottom, Zoom);
         ReleaseDC (mamain2, hdc);
}
         
void BmClass::ScrollHPageUp (void)
/**
Eine Zeile nach links Scrollen.
**/
{
         HDC hdc;
         RECT vrect;
         RECT hrect;


         RECT rect;

         GetClientRect (mamain2, &rect);
         if (vTrack)
         {
                GetClientRect (vTrack, &vrect);
                rect.right -= vrect.right;
         }
         if (hTrack)
         {
                GetClientRect (hTrack, &hrect);
                rect.bottom -= hrect.bottom;
         }

         scrollpos -= nhPage;
         if (scrollpos < 0)
         {
                 scrollpos = 0;
         }
         SetScrollPos (hTrack, SB_CTL, scrollpos, TRUE);
         hdc = GetDC (mamain2);
/*
         DrawBitmapPart (hdc, bmx, bmy, scrollpos, scrollvpos,
                         rect.right, rect.bottom);
*/

         ZoomBitmap (hdc, bmx, bmy, scrollpos, scrollvpos,
                         rect.right, rect.bottom, Zoom);
         ReleaseDC (mamain2, hdc);
}

         
void BmClass::SetHPos (int pos)
/**
Position setzen.
**/
{
         HDC hdc;
         RECT rect;
         RECT vrect;
         RECT hrect;

         GetClientRect (mamain2, &rect);
         if (vTrack)
         {
                GetClientRect (vTrack, &vrect);
                rect.right -= vrect.right;
         }
         if (hTrack)
         {
                GetClientRect (hTrack, &hrect);
                rect.bottom -= hrect.bottom;
         }
         scrollpos = pos;
         SetScrollPos (hTrack, SB_CTL, scrollpos,
                                   TRUE);
         hdc = GetDC (mamain2);
/*
         DrawBitmapPart (hdc, bmx, bmy, scrollpos, scrollvpos,
                         rect.right, rect.bottom);
*/
         ZoomBitmap (hdc, bmx, bmy, scrollpos, scrollvpos,
                         rect.right, rect.bottom, Zoom);
         ReleaseDC (mamain2, hdc);
}
                      

void BmClass::HScroll (WPARAM wParam, LPARAM lParam)
/**
Meldung WM_HSCROLL erhalten.
**/
{
         switch (LOWORD (wParam))
         {
             case SB_LINEDOWN :
             {    
                     ScrollRight ();
                     break;
             }
             case SB_LINEUP :
             {    
                     ScrollLeft ();
                     break;
             }

             case SB_PAGEDOWN :
             {    
                     ScrollHPageDown ();
                     break;
             }
             case SB_PAGEUP :
             {    
                     ScrollHPageUp ();
                     break;
             }
             case SB_THUMBPOSITION :
             case SB_THUMBTRACK :
                     SetHPos (HIWORD (wParam));
                     break;
         }
}


void BmClass::OnVScroll (HWND hWnd,UINT msg, WPARAM wParam,LPARAM lParam)
{
         if (vTrack == NULL) return;
         if ((HWND) lParam == vTrack)
         {
             VScroll (wParam, lParam);
         }
}

void BmClass::OnHScroll (HWND hWnd,UINT msg, WPARAM wParam,LPARAM lParam)
{
         if (hTrack == NULL) return;
         if ((HWND) lParam == hTrack)
         {
             HScroll (wParam, lParam);
         }
}

void BmClass::OnSize (HWND hWnd)
/**
Aktion bei WM_SIZE-Meldung.
**/
{
        
        if (mamain2 == NULL) return;

        MoveBitmap (); 
        if (bmmode != 2)
        {
              TestTrack ();
              TestVTrack ();
        }
        return;
}

void BmClass::SetZoom (double zm0)
{
//    int zm;
    BITMAP bm;
    int bytes;
    double zy;
    int cx, cy;

    if (mamain2 == NULL) return;
    if (zm0 == (double) 0)
    {
        Zoom = 1;
        TestTrack ();
        TestVTrack ();
        return;
    }
    bytes = GetObject (hBitmapOrg, sizeof (BITMAP), &bm);

    zy = (double) bm.bmHeight / bm.bmWidth;
    Zoom = zm0;
/*
    if (zm0 > (double) 1)
    {
             zm = (int) (zm0 + 0.5);
             zm0 = (double) zm;
    }
*/
            

    cx = (int) ((double) bm.bmWidth * zm0);
    cy = (int) ((double) bm.bmHeight * zm0 * zy);

    StrechBitmapMem (mamain2, cx, cy);
    TestTrack ();
    TestVTrack ();
}
    




void BmClass::MoveBitmap (void)
/**
Groesse der Bitmap aendern.
**/
{
       int x,y;
       int cx,cy;
       RECT rect;

       if (mamain2 == NULL) return;

       y = 0;
       x = 0;
       GetClientRect (hMainWindow, &rect);
       cy = rect.bottom;
       cx = rect.right;
       bmcx = rect.right;
       bmcy = rect.bottom;
       scrollpos = 0;
       scrollvpos = 0;
       MoveWindow (mamain2, x, y, cx, cy, TRUE);
       SetScrollPos (vTrack, SB_CTL, scrollvpos, TRUE);
       SetScrollPos (hTrack, SB_CTL, scrollpos, TRUE);
}

void BmClass::TestTrack (void)
{
       RECT rect;
       int x,y,cx,cy;
	   SCROLLINFO scinfo;
       BITMAP bm;
       int bytes;

       if (TrackNeeded ())
       {
           CreateTrack ();
       }
       else
       {
           DestroyTrack ();
       }
       if (hTrack == NULL) return;

       GetClientRect (mamain2, &rect);
       bytes = GetObject (hBitmap, sizeof (BITMAP), &bm);
       x = 0;
       y = rect.bottom - 16;
       cy = 16;
       cx = rect.right;
       MoveWindow (hTrack, x, y, cx, cy, TRUE);
	   scinfo.cbSize = sizeof (SCROLLINFO);
	   scinfo.fMask  = SIF_PAGE | SIF_RANGE;
	   scinfo.nMin   = 0;
	   scinfo.nMax   = bm.bmWidth;
       nhPage        = rect.right;
       nhMax         = bm.bmWidth - rect.right;
 	   scinfo.nPage  = nhPage;
       SetScrollInfo  (hTrack, SB_CTL, &scinfo, TRUE);
}

void BmClass::TestVTrack (void)
{
       RECT rect;
       int x,y,cx,cy;
	   SCROLLINFO scinfo;
       BITMAP bm;
       int bytes;

       if (VTrackNeeded ())
       {
           CreateVTrack ();
       }
       else
       {
           DestroyVTrack ();
       }
       if (vTrack == NULL) return;

       GetClientRect (mamain2, &rect);
       bytes = GetObject (hBitmap, sizeof (BITMAP), &bm);
       

       x = rect.right - 16;
       cx = 16;
       y = 0;
       cy = rect.bottom;
       if (hTrack)
       {
                 cy -= 16;
       }
       MoveWindow (vTrack, x, y, cx, cy, TRUE);

	   scinfo.cbSize = sizeof (SCROLLINFO);
	   scinfo.fMask  = SIF_PAGE | SIF_RANGE;

	   scinfo.nMin   = 0;
	   scinfo.nMax   = bm.bmHeight;

       nvMax         = bm.bmHeight - rect.bottom;
       nvPage        = rect.bottom;

       scinfo.nPage  = nvPage;
       SetScrollInfo  (vTrack, SB_CTL, &scinfo, TRUE);
       SetScrollPos (vTrack, SB_CTL, scrollvpos, TRUE);
}


BOOL BmClass::TrackNeeded (void)
/**
Test, ob ein Schieberegler benoetigt wird.
**/
{
        RECT rect;
        BITMAP bm;
        int bytes;


        if (hBitmap == NULL) return FALSE;

        bytes = GetObject (hBitmap, sizeof (BITMAP), &bm);
        GetClientRect (mamain2, &rect);
        bm.bmWidth = (int) (double) ((double) bm.bmWidth * Zoom);
        if (bm.bmWidth > rect.right)
        {
            return TRUE;
        }
        return FALSE;
}

void BmClass::CreateTrack (void)
/**
Bildlaufleiste generieren.
**/
{
        int x,y;
        int cx, cy;
        RECT rect;
		SCROLLINFO scinfo;
        BITMAP bm;
        int bytes;

        if (hTrack) return;

        GetClientRect (mamain2, &rect);
        bytes = GetObject (hBitmap, sizeof (BITMAP), &bm);
        bm.bmWidth = (int) (double) ((double) bm.bmWidth * Zoom);

        x = 0;
        y = rect.bottom - 16;
        cy = 16;
        cx = rect.right;

        hTrack = CreateWindow ("scrollbar",
                               NULL,
                               WS_CHILD | WS_VISIBLE | SBS_HORZ,
                               x, y, cx, cy,
                               mamain2,
                               (HMENU) 701,
                               hMainInst,
                               0);


		 scinfo.cbSize = sizeof (SCROLLINFO);
		 scinfo.fMask  = SIF_PAGE | SIF_RANGE;
		 scinfo.nMin   = 0;
		 scinfo.nMax   = bm.bmWidth;
         nhPage        = rect.right;
         nhMax         = bm.bmWidth - rect.right;
 	     scinfo.nPage  = nhPage;
         SetScrollInfo  (hTrack, SB_CTL, &scinfo, TRUE);
}

void BmClass::DestroyTrack (void)
/**
Bildlaufleiste loeschen.
**/
{
          if (hTrack)
          {
              DestroyWindow (hTrack);
              hTrack = NULL;
          }
}

BOOL BmClass::VTrackNeeded (void)
/**
Test, ob ein Schieberegler benoetigt wird.
**/
{
        RECT rect;
        int cy;
        BITMAP bm;
        int bytes;

        bytes = GetObject (hBitmap, sizeof (BITMAP), &bm);

        GetClientRect (mamain2, &rect);
        bm.bmHeight = (int) (double) ((double) bm.bmHeight * Zoom);
        cy = bm.bmHeight;


        if (hTrack) 
        {

// untere Bildlaufleiste abziehen

            cy += 16; 
        }
        if (cy > rect.bottom)
        {
                    return TRUE;
        }
        return FALSE;
}

void BmClass::CreateVTrack (void)
/**
Bildlaufleiste generieren.
**/
{
        int x,y;
        int cx, cy;
        RECT rect;
		SCROLLINFO scinfo;
        BITMAP bm;
        int bytes;


        if (vTrack) return;
        GetClientRect (mamain2, &rect);
        bytes = GetObject (hBitmap, sizeof (BITMAP), &bm);
        bm.bmHeight = (int) (double) ((double) bm.bmHeight * Zoom);

        x = rect.right - 16;
        cx = 16;
        y = 0;
        cy = rect.bottom;
        if (hTrack)
        {
                 cy -= 16;
        }

        vTrack = CreateWindow ("scrollbar",
                               NULL,
                               WS_CHILD | WS_VISIBLE | SBS_VERT,
                               x, y, cx, cy,
                               mamain2,
                               (HMENU) 702,
                               hMainInst,
                               0);
        scinfo.cbSize = sizeof (SCROLLINFO);
	    scinfo.fMask  = SIF_PAGE | SIF_RANGE;

	    scinfo.nMin   = 0;
	    scinfo.nMax   = bm.bmHeight;

        nvMax         = bm.bmHeight - rect.bottom;
        nvPage        = rect.bottom;

        scinfo.nPage  = nvPage;
        SetScrollInfo  (vTrack, SB_CTL, &scinfo, TRUE);
}


void BmClass::DestroyVTrack (void)
/**
Bildlaufleiste loeschen.
**/
{
          if (vTrack)
          {
              DestroyWindow (vTrack);
              vTrack = NULL;
          }
}

BOOL hole_bild_pfad (double a)
{
				char a_bas_bild  [128] ;
			    a_bas_a = a;

			    DbClass.sqlin ((double *) &a_bas_a, 3, 0);
		        DbClass.sqlout ((char *) a_bas_bild, 0, 128);
				DbClass.sqlcomm ("select bild from a_bas where a = ?");
				if (sqlstatus == 0)
				{
					if (strlen (a_bas_bild) > 10)
					{
						strcpy (bmp_pfad,a_bas_bild);
					} else return FALSE;
				} else return FALSE;
				return TRUE;

}


void BmClass::GetWindowPos (HWND Parent, HWND Child)
/**
Position zum Hauptfenster ermitteln.
**/
{
        RECT pRect;
        RECT cRect;

        GetWindowRect (Parent, &pRect);
        GetWindowRect (Child,  &cRect);

        bxdiff = pRect.left - cRect.left;
        bydiff = pRect.top  - cRect.top;

        bcx    = cRect.right - cRect.left; 
        bcy    = cRect.bottom - cRect.top;
}


int BmClass::Showbm(HWND *hMain, LPSTR lpszCmdLine)
{
       int anz;
       char *varargs[20];
       char argtab[20][80];
       int i;
       HWND MainWindow;
       HWND hWnd;
  		int x, y, cx, cy;


       anz = wsplit (lpszCmdLine, " ");
       for (i = 0; i < anz; i ++)
       {
            strcpy (argtab[i], wort[i]);
            varargs[i] = (char *) argtab[i];
		    sprintf (ctext,"varargs = %s",varargs[i]);
       }
       argtst (&anz, varargs, tst_arg);
	   sprintf (ctext,"Savemode = %ld",Savemode);

   	   strcpy (bmp_pfad, varargs[0]);	


       SetStdProc (WndProcBm);

       SetEnvFont ();

       DestroyBMWindow ();
       MainWindow = GetActiveWindow ();
           hWnd = GetParent (MainWindow);
		/**
       while (TRUE)
       {
           hWnd = GetParent (MainWindow);
           if (hWnd == NULL) break;
       }
	   **/
       *hMain = MainWindow;

       hMainInst = (HANDLE) GetClassLong (MainWindow, GCL_HMODULE);

       InitFirstInstance (hMainInst);
           
           
       SetAktivWindow (MainWindow);

       SethStdWindow ("BmWindow");
       if (captmode == 1)
       {
              SetBorder (WS_CAPTION | WS_THICKFRAME | 
                         WS_MAXIMIZEBOX | WS_MINIMIZEBOX |
                         WS_SYSMENU);
       }
       else if (captmode == 2)
       {
              SetBorder (WS_POPUP);
       }
       else if (captmode == 3)
       {
              SetBorder (WS_POPUP | WS_BORDER);
       }
       else if (captmode == 4)
       {
              SetBorder (WS_POPUP);
       }
       else if (captmode == 5)
       {
              SetBorder (WS_CHILD);
       }
       else
       {
              SetBorder (WS_POPUP | WS_THICKFRAME);
       }

	   sprintf (ctext,"Savemode = %ld",Savemode);
	   if (Savemode == 1)  //aus gesicherten Werte aus bild.rct die position ermitteln 
       {
			bmmode = 2;
			if (strlen (clipped(bmp_pfad)) < 14)
			{
				if (hole_bild_pfad (ratod(bmp_pfad)) == FALSE) return 0;
			}
			etc = getenv ("BWSETC");
			if (etc == NULL) return NULL;
			sprintf (buffer, "%s\\%s.rct", etc,"51100_bmp");
			fp = fopen (buffer, "r");
			if (fp == NULL) return 0;

			while (fgets (buffer, 511, fp))
			{
				cr_weg (buffer);
				anz = wsplit (buffer, " ");
				if (anz < 2) continue;
				if (strcmp (wort[0], "left") == 0)
				{
					x = atoi (wort[1]);
				}
				else if (strcmp (wort[0], "top") == 0)
				{
					y = atoi (wort[1]);
				}
				else if (strcmp (wort[0], "right") == 0)
				{
					cx = atoi (wort[1]);
				}
				else if (strcmp (wort[0], "bottom") == 0)
				{
					cy = atoi (wort[1]);
				}
			}
			fclose (fp);

              hMainWindow = OpenWindowRect (x, 
                                    y, 
                                    cx, 
                                    cy, hMainInst,
                                    caption);  
       }

       else if (captmode < 4)
       {
              hMainWindow = OpenWindowChC (bmcrow, 
                                    bmccolumn, 
                                    bmrow, 
                                    bmcolumn, hMainInst,
                                    caption);  
       }
       else
       {
              hMainWindow = OpenWindowChEx (bmcrow, 
                                    bmccolumn, 
                                    bmrow, 
                                    bmcolumn, hMainInst,
                                    caption);  
       }
       if (bmtopmost)
       {
               SetWindowPos (hMainWindow, HWND_TOPMOST, 0, 0, 0, 0,
                     SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
       }

       GetWindowPos (MainWindow, hMainWindow);
       if (anz < 1) 
       {
           return (0);
       }
       bmmode = bmm;
       InitBMWindow (hMainWindow); 
       if (bmmode == 1)
       {
           SetBmToScrEx (bmp_pfad);
       }
       else if (bmmode == 2)
       {
           SetBmToScrStr (bmp_pfad);
       }
       else
       {
           SetBmToScrCe (bmp_pfad);
       }

       return 0;
}

void BmClass::InitFirstInstance(HANDLE hInstance)
{
        WNDCLASS wc;
        wc.style         =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                            | CS_CLASSDC;
        wc.lpfnWndProc   =  WndProcBm;
        wc.cbClsExtra    =  0;
        wc.cbWndExtra    =  0;
        wc.hInstance     =  hInstance;
        wc.hIcon         =  LoadIcon (hInstance, "ROSIICON");
        wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
        wc.lpszMenuName  =  "";
        wc.lpszClassName =  "BmWindow";

        RegisterClass(&wc);

        wc.hIcon         =  LoadIcon (hInstance, "NOICON");
        wc.lpszMenuName  =  NULL;
        wc.lpszClassName =  "ListMain";
        RegisterClass(&wc);

        wc.hbrBackground =  GetStockObject (WHITE_BRUSH);
        wc.lpszClassName =  "hBmWindow";
        RegisterClass(&wc);

        hMainInst = hInstance;
        return;
}

BmClass BitMap;

LONG FAR PASCAL WndProcBm(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{

        switch(msg)
        {
              case WM_PAINT :
                      BitMap.OnPaint (hWnd);
                      break;
              case WM_HSCROLL :
                       BitMap.OnHScroll (hWnd, msg, wParam, lParam);
                       break;
              case WM_VSCROLL :
                       BitMap.OnVScroll (hWnd, msg, wParam, lParam);
                       break;
              case WM_KEYDOWN :
                       switch (wParam)
                       {
                              case VK_DOWN :
                                   BitMap.OnVScroll (hWnd, WM_VSCROLL,
                                                     SB_LINEDOWN, (LPARAM) BitMap.GetvTrack ());
                                   break;
                              case VK_UP :
                                   BitMap.OnVScroll (hWnd, WM_VSCROLL,
                                                     SB_LINEUP, (LPARAM) BitMap.GetvTrack ());
                                   break;
                              case VK_NEXT :
                                   BitMap.OnVScroll (hWnd, WM_VSCROLL,
                                                     SB_PAGEDOWN,  (LPARAM) BitMap.GetvTrack ());
                                   break;
                              case VK_PRIOR :
                                   BitMap.OnVScroll (hWnd, WM_VSCROLL,
                                                     SB_PAGEUP,  (LPARAM) BitMap.GetvTrack ());
                                   break;
                              case VK_RIGHT :
                                   BitMap.OnHScroll (hWnd, WM_HSCROLL,
                                                     SB_LINEDOWN,  (LPARAM) BitMap.GethTrack ());
                                   break;
                              case VK_LEFT :
                                   BitMap.OnHScroll (hWnd, WM_HSCROLL,
                                                     SB_LINEUP,  (LPARAM) BitMap.GethTrack ());
                                   break;
                       }
              case WM_SIZE :
                       BitMap.OnSize (hWnd);
                       break;
              case WM_DESTROY :
                       if (hWnd == BitMap.Getmamain2 ())
                       {
                                BitMap.DestroyBitmap ();
                       }
                       break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

