# Microsoft Developer Studio Project File - Name="bmlib" - Package Owner=<4>
# Von Microsoft Developer Studio generierte Erstellungsdatei, Format Version 5.00
# ** NICHT BEARBEITEN **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=bmlib - Win32 Debug
!MESSAGE Dies ist kein g�ltiges Makefile. Zum Erstellen dieses Projekts mit\
 NMAKE
!MESSAGE verwenden Sie den Befehl "Makefile exportieren" und f�hren Sie den\
 Befehl
!MESSAGE 
!MESSAGE NMAKE /f "bmlib.mak".
!MESSAGE 
!MESSAGE Sie k�nnen beim Ausf�hren von NMAKE eine Konfiguration angeben
!MESSAGE durch Definieren des Makros CFG in der Befehlszeile. Zum Beispiel:
!MESSAGE 
!MESSAGE NMAKE /f "bmlib.mak" CFG="bmlib - Win32 Debug"
!MESSAGE 
!MESSAGE F�r die Konfiguration stehen zur Auswahl:
!MESSAGE 
!MESSAGE "bmlib - Win32 Release" (basierend auf\
  "Win32 (x86) Dynamic-Link Library")
!MESSAGE "bmlib - Win32 Debug" (basierend auf\
  "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "bmlib - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /I "d:\informix\incl\esql" /I "..\..\include" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /YX /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o NUL /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o NUL /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /dll /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib comctl32.lib  isqlt07c.lib /nologo /subsystem:windows /dll /machine:I386 /out:"d:\user\fit\bin\bmlib.dll" /libpath:"d:\informix\lib"

!ELSEIF  "$(CFG)" == "bmlib - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /Zi /Od /I "d:\informix\incl\esql" /I "..\..\include" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /FR /YX /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o NUL /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o NUL /win32
# ADD BASE RSC /l 0x407 /d "_DEBUG"
# ADD RSC /l 0x407 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib comctl32.lib   isqlt07c.lib /nologo /subsystem:windows /dll /debug /machine:I386 /out:"d:\user\fit\bin\bmlib.dll" /pdbtype:sept /libpath:"d:\informix\lib"

!ENDIF 

# Begin Target

# Name "bmlib - Win32 Release"
# Name "bmlib - Win32 Debug"
# Begin Source File

SOURCE=..\bmlib.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libsrc\cmask.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libsrc\colbut.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libsrc\dbclass.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libsrc\DBFUNC.CPP
# End Source File
# Begin Source File

SOURCE=..\..\libsrc\dlg.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libsrc\FIL.CPP
# End Source File
# Begin Source File

SOURCE=..\..\libsrc\help.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libsrc\image.cpp
# End Source File
# Begin Source File

SOURCE=..\..\lib\inflib.lib
# End Source File
# Begin Source File

SOURCE=..\..\libsrc\itemc.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libsrc\lbox.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libsrc\MDN.CPP
# End Source File
# Begin Source File

SOURCE=..\..\libsrc\mo_arg.cpp
# End Source File
# Begin Source File

SOURCE=..\mo_bm.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libsrc\MO_CURSO.CPP
# End Source File
# Begin Source File

SOURCE=..\..\libsrc\MO_DRAW.CPP
# End Source File
# Begin Source File

SOURCE=..\..\libsrc\MO_MELD.CPP
# End Source File
# Begin Source File

SOURCE=..\..\libsrc\MO_MENU.CPP
# End Source File
# Begin Source File

SOURCE=..\..\libsrc\RBrush.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libsrc\rdonly.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libsrc\RFont.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libsrc\RPen.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libsrc\STDFKT.CPP
# End Source File
# Begin Source File

SOURCE=..\..\libsrc\STRFKT.CPP
# End Source File
# Begin Source File

SOURCE=..\..\libsrc\Text.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libsrc\VBrush.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libsrc\Vector.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libsrc\VFont.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libsrc\VPen.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libsrc\wmaskc.cpp
# End Source File
# End Target
# End Project
