#ifndef _eBmDef
#define _eBmDef

class BmClass : public BMAP
{
      private :
             HWND    hTrack;
             HWND    vTrack;
             TEXTMETRIC tm;
             PAINTSTRUCT aktpaint;
             COLORREF Color;
             COLORREF BkColor;
             int bmx, bmy;
             int bmcx, bmcy;
             int bmmode;
             int bxdiff, bydiff;
             int bcx, bcy;
             int scrollpos;
             int scrollvpos;
             int nvPage, nhPage;
             int nvMax,  nhMax;
             double Zoom;

      public :
             HANDLE  hMainInst;
             HWND    hMainWindow; 
             HWND    mamain2; 
             BmClass ()
             {
                    Color = RGB (0, 0, 0);;
                    BkColor = RGB (255, 255, 255); 
                    mamain2 = NULL;  
                    bmx = bmy = 0;
                    bmcx = bmcy = 0;
                    bmmode = 0;
                    scrollpos = scrollvpos = 0;
                    Zoom = (double) 1;
             }

             double GetZoom (void)
             {
                    return Zoom;
             }


             HWND Getmamain2 (void)
             {
                 return mamain2;
             }

             HWND GetvTrack (void)
             {
                 return vTrack;
             }

             HWND GethTrack (void)
             {
                 return hTrack;
             }

             void DestroyBMWindow (void);
             HWND InitBMWindow (HWND hMainWindow);
             BOOL BmToScr (char *);
             BOOL BmToScrEx (char *);
             BOOL BmToScrCe (char *);
             BOOL BmToScrStr (char *);

             BOOL SetBmToScr (char *);
             BOOL SetBmToScrEx (char *);
             BOOL SetBmToScrCe (char *);
             BOOL SetBmToScrStr (char *);
             BOOL SetBmToScrStrX (char *);

             BOOL OnPaint (HWND);
             void OnSize (HWND); 
             void OnMove (HWND); 
             void ScrollDown (void);
             void ScrollUp (void);
             void ScrollVPageDown (void);
             void ScrollVPageUp (void);
             void SetVPos (int);
             void VScroll (WPARAM, LPARAM);
             void OnVScroll (HWND,UINT, WPARAM,LPARAM);

             void ScrollRight (void);
             void ScrollLeft (void);
             void ScrollHPageDown (void);
             void ScrollHPageUp (void);
             void SetHPos (int);
             void HScroll (WPARAM, LPARAM);
             void OnHScroll (HWND,UINT, WPARAM,LPARAM);

             void SetZoom (double);
             void MoveBitmap (void);

             BOOL     TrackNeeded (void);
             void     CreateTrack (void);
             void     DestroyTrack (void); 

             BOOL     VTrackNeeded (void);
             void     CreateVTrack (void);
             void     DestroyVTrack (void);
             void     TestTrack (void);
             void     TestVTrack (void);

             void     GetWindowPos (HWND, HWND);
             int      Showbm(HWND *, LPSTR lpszCmdLine);
             void     InitFirstInstance(HANDLE hInstance);

};

#endif 
