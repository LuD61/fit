# Von Microsoft Developer Studio generierte NMAKE-Datei, basierend auf showbm.dsp
!IF "$(CFG)" == ""
CFG=showbm - Win32 Debug
!MESSAGE Keine Konfiguration angegeben. showbm - Win32 Debug wird als Standard\
 verwendet.
!ENDIF 

!IF "$(CFG)" != "showbm - Win32 Release" && "$(CFG)" != "showbm - Win32 Debug"
!MESSAGE Ung�ltige Konfiguration "$(CFG)" angegeben.
!MESSAGE Sie k�nnen beim Ausf�hren von NMAKE eine Konfiguration angeben
!MESSAGE durch Definieren des Makros CFG in der Befehlszeile. Zum Beispiel:
!MESSAGE 
!MESSAGE NMAKE /f "showbm.mak" CFG="showbm - Win32 Debug"
!MESSAGE 
!MESSAGE F�r die Konfiguration stehen zur Auswahl:
!MESSAGE 
!MESSAGE "showbm - Win32 Release" (basierend auf  "Win32 (x86) Application")
!MESSAGE "showbm - Win32 Debug" (basierend auf  "Win32 (x86) Application")
!MESSAGE 
!ERROR Eine ung�ltige Konfiguration wurde angegeben.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

!IF  "$(CFG)" == "showbm - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release

!IF "$(RECURSE)" == "0" 

ALL : "..\..\..\..\fit\bin\showbm.exe"

!ELSE 

ALL : "..\..\..\..\fit\bin\showbm.exe"

!ENDIF 

CLEAN :
	-@erase "$(INTDIR)\cmask.obj"
	-@erase "$(INTDIR)\colbut.obj"
	-@erase "$(INTDIR)\DBFUNC.OBJ"
	-@erase "$(INTDIR)\dlg.obj"
	-@erase "$(INTDIR)\help.obj"
	-@erase "$(INTDIR)\image.obj"
	-@erase "$(INTDIR)\itemc.obj"
	-@erase "$(INTDIR)\lbox.obj"
	-@erase "$(INTDIR)\mo_arg.obj"
	-@erase "$(INTDIR)\mo_bm.obj"
	-@erase "$(INTDIR)\mo_chqex.obj"
	-@erase "$(INTDIR)\MO_CURSO.OBJ"
	-@erase "$(INTDIR)\MO_DRAW.OBJ"
	-@erase "$(INTDIR)\MO_MELD.OBJ"
	-@erase "$(INTDIR)\RBrush.obj"
	-@erase "$(INTDIR)\rdonly.obj"
	-@erase "$(INTDIR)\RFont.obj"
	-@erase "$(INTDIR)\RPen.obj"
	-@erase "$(INTDIR)\showbm.obj"
	-@erase "$(INTDIR)\STDFKT.OBJ"
	-@erase "$(INTDIR)\STRFKT.OBJ"
	-@erase "$(INTDIR)\Text.obj"
	-@erase "$(INTDIR)\VBrush.obj"
	-@erase "$(INTDIR)\vc50.idb"
	-@erase "$(INTDIR)\Vector.obj"
	-@erase "$(INTDIR)\VFont.obj"
	-@erase "$(INTDIR)\VPen.obj"
	-@erase "$(INTDIR)\wmaskc.obj"
	-@erase "..\..\..\..\fit\bin\showbm.exe"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /ML /W3 /GX /O2 /I "..\include" /D "NDEBUG" /D "WIN32" /D\
 "_WINDOWS" /D "DBN" /D "NINFO" /Fp"$(INTDIR)\showbm.pch" /YX /Fo"$(INTDIR)\\"\
 /Fd"$(INTDIR)\\" /FD /c 
CPP_OBJS=.\Release/
CPP_SBRS=.

.c{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /o NUL /win32 
RSC=rc.exe
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\showbm.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib\
 advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib\
 odbccp32.lib comctl32.lib /nologo /subsystem:windows /incremental:no\
 /pdb:"$(OUTDIR)\showbm.pdb" /machine:I386 /out:"d:\user\fit\bin\showbm.exe" 
LINK32_OBJS= \
	"$(INTDIR)\cmask.obj" \
	"$(INTDIR)\colbut.obj" \
	"$(INTDIR)\DBFUNC.OBJ" \
	"$(INTDIR)\dlg.obj" \
	"$(INTDIR)\help.obj" \
	"$(INTDIR)\image.obj" \
	"$(INTDIR)\itemc.obj" \
	"$(INTDIR)\lbox.obj" \
	"$(INTDIR)\mo_arg.obj" \
	"$(INTDIR)\mo_bm.obj" \
	"$(INTDIR)\mo_chqex.obj" \
	"$(INTDIR)\MO_CURSO.OBJ" \
	"$(INTDIR)\MO_DRAW.OBJ" \
	"$(INTDIR)\MO_MELD.OBJ" \
	"$(INTDIR)\RBrush.obj" \
	"$(INTDIR)\rdonly.obj" \
	"$(INTDIR)\RFont.obj" \
	"$(INTDIR)\RPen.obj" \
	"$(INTDIR)\showbm.obj" \
	"$(INTDIR)\STDFKT.OBJ" \
	"$(INTDIR)\STRFKT.OBJ" \
	"$(INTDIR)\Text.obj" \
	"$(INTDIR)\VBrush.obj" \
	"$(INTDIR)\Vector.obj" \
	"$(INTDIR)\VFont.obj" \
	"$(INTDIR)\VPen.obj" \
	"$(INTDIR)\wmaskc.obj"

"..\..\..\..\fit\bin\showbm.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "showbm - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
# Anfang eines benutzerdefinierten Makros
OutDir=.\Debug
# Ende eines benutzerdefinierten Makros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\showbm.exe" "$(OUTDIR)\showbm.bsc"

!ELSE 

ALL : "$(OUTDIR)\showbm.exe" "$(OUTDIR)\showbm.bsc"

!ENDIF 

CLEAN :
	-@erase "$(INTDIR)\cmask.obj"
	-@erase "$(INTDIR)\cmask.sbr"
	-@erase "$(INTDIR)\colbut.obj"
	-@erase "$(INTDIR)\colbut.sbr"
	-@erase "$(INTDIR)\DBFUNC.OBJ"
	-@erase "$(INTDIR)\DBFUNC.SBR"
	-@erase "$(INTDIR)\dlg.obj"
	-@erase "$(INTDIR)\dlg.sbr"
	-@erase "$(INTDIR)\help.obj"
	-@erase "$(INTDIR)\help.sbr"
	-@erase "$(INTDIR)\image.obj"
	-@erase "$(INTDIR)\image.sbr"
	-@erase "$(INTDIR)\itemc.obj"
	-@erase "$(INTDIR)\itemc.sbr"
	-@erase "$(INTDIR)\lbox.obj"
	-@erase "$(INTDIR)\lbox.sbr"
	-@erase "$(INTDIR)\mo_arg.obj"
	-@erase "$(INTDIR)\mo_arg.sbr"
	-@erase "$(INTDIR)\mo_bm.obj"
	-@erase "$(INTDIR)\mo_bm.sbr"
	-@erase "$(INTDIR)\mo_chqex.obj"
	-@erase "$(INTDIR)\mo_chqex.sbr"
	-@erase "$(INTDIR)\MO_CURSO.OBJ"
	-@erase "$(INTDIR)\MO_CURSO.SBR"
	-@erase "$(INTDIR)\MO_DRAW.OBJ"
	-@erase "$(INTDIR)\MO_DRAW.SBR"
	-@erase "$(INTDIR)\MO_MELD.OBJ"
	-@erase "$(INTDIR)\MO_MELD.SBR"
	-@erase "$(INTDIR)\RBrush.obj"
	-@erase "$(INTDIR)\RBrush.sbr"
	-@erase "$(INTDIR)\rdonly.obj"
	-@erase "$(INTDIR)\rdonly.sbr"
	-@erase "$(INTDIR)\RFont.obj"
	-@erase "$(INTDIR)\RFont.sbr"
	-@erase "$(INTDIR)\RPen.obj"
	-@erase "$(INTDIR)\RPen.sbr"
	-@erase "$(INTDIR)\showbm.obj"
	-@erase "$(INTDIR)\showbm.sbr"
	-@erase "$(INTDIR)\STDFKT.OBJ"
	-@erase "$(INTDIR)\STDFKT.SBR"
	-@erase "$(INTDIR)\STRFKT.OBJ"
	-@erase "$(INTDIR)\STRFKT.SBR"
	-@erase "$(INTDIR)\Text.obj"
	-@erase "$(INTDIR)\Text.sbr"
	-@erase "$(INTDIR)\VBrush.obj"
	-@erase "$(INTDIR)\VBrush.sbr"
	-@erase "$(INTDIR)\vc50.idb"
	-@erase "$(INTDIR)\vc50.pdb"
	-@erase "$(INTDIR)\Vector.obj"
	-@erase "$(INTDIR)\Vector.sbr"
	-@erase "$(INTDIR)\VFont.obj"
	-@erase "$(INTDIR)\VFont.sbr"
	-@erase "$(INTDIR)\VPen.obj"
	-@erase "$(INTDIR)\VPen.sbr"
	-@erase "$(INTDIR)\wmaskc.obj"
	-@erase "$(INTDIR)\wmaskc.sbr"
	-@erase "$(OUTDIR)\showbm.bsc"
	-@erase "$(OUTDIR)\showbm.exe"
	-@erase "$(OUTDIR)\showbm.ilk"
	-@erase "$(OUTDIR)\showbm.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MLd /W3 /Gm /GX /Zi /Od /I "..\include" /D "_DEBUG" /D\
 "WIN32" /D "_WINDOWS" /D "DBN" /D "NINFO" /FR"$(INTDIR)\\"\
 /Fp"$(INTDIR)\showbm.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
CPP_OBJS=.\Debug/
CPP_SBRS=.\Debug/

.c{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /o NUL /win32 
RSC=rc.exe
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\showbm.bsc" 
BSC32_SBRS= \
	"$(INTDIR)\cmask.sbr" \
	"$(INTDIR)\colbut.sbr" \
	"$(INTDIR)\DBFUNC.SBR" \
	"$(INTDIR)\dlg.sbr" \
	"$(INTDIR)\help.sbr" \
	"$(INTDIR)\image.sbr" \
	"$(INTDIR)\itemc.sbr" \
	"$(INTDIR)\lbox.sbr" \
	"$(INTDIR)\mo_arg.sbr" \
	"$(INTDIR)\mo_bm.sbr" \
	"$(INTDIR)\mo_chqex.sbr" \
	"$(INTDIR)\MO_CURSO.SBR" \
	"$(INTDIR)\MO_DRAW.SBR" \
	"$(INTDIR)\MO_MELD.SBR" \
	"$(INTDIR)\RBrush.sbr" \
	"$(INTDIR)\rdonly.sbr" \
	"$(INTDIR)\RFont.sbr" \
	"$(INTDIR)\RPen.sbr" \
	"$(INTDIR)\showbm.sbr" \
	"$(INTDIR)\STDFKT.SBR" \
	"$(INTDIR)\STRFKT.SBR" \
	"$(INTDIR)\Text.sbr" \
	"$(INTDIR)\VBrush.sbr" \
	"$(INTDIR)\Vector.sbr" \
	"$(INTDIR)\VFont.sbr" \
	"$(INTDIR)\VPen.sbr" \
	"$(INTDIR)\wmaskc.sbr"

"$(OUTDIR)\showbm.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib\
 advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib\
 odbccp32.lib comctl32.lib /nologo /subsystem:windows /incremental:yes\
 /pdb:"$(OUTDIR)\showbm.pdb" /debug /machine:I386 /out:"$(OUTDIR)\showbm.exe"\
 /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\cmask.obj" \
	"$(INTDIR)\colbut.obj" \
	"$(INTDIR)\DBFUNC.OBJ" \
	"$(INTDIR)\dlg.obj" \
	"$(INTDIR)\help.obj" \
	"$(INTDIR)\image.obj" \
	"$(INTDIR)\itemc.obj" \
	"$(INTDIR)\lbox.obj" \
	"$(INTDIR)\mo_arg.obj" \
	"$(INTDIR)\mo_bm.obj" \
	"$(INTDIR)\mo_chqex.obj" \
	"$(INTDIR)\MO_CURSO.OBJ" \
	"$(INTDIR)\MO_DRAW.OBJ" \
	"$(INTDIR)\MO_MELD.OBJ" \
	"$(INTDIR)\RBrush.obj" \
	"$(INTDIR)\rdonly.obj" \
	"$(INTDIR)\RFont.obj" \
	"$(INTDIR)\RPen.obj" \
	"$(INTDIR)\showbm.obj" \
	"$(INTDIR)\STDFKT.OBJ" \
	"$(INTDIR)\STRFKT.OBJ" \
	"$(INTDIR)\Text.obj" \
	"$(INTDIR)\VBrush.obj" \
	"$(INTDIR)\Vector.obj" \
	"$(INTDIR)\VFont.obj" \
	"$(INTDIR)\VPen.obj" \
	"$(INTDIR)\wmaskc.obj"

"$(OUTDIR)\showbm.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 


!IF "$(CFG)" == "showbm - Win32 Release" || "$(CFG)" == "showbm - Win32 Debug"
SOURCE=..\libsrc\cmask.cpp

!IF  "$(CFG)" == "showbm - Win32 Release"

DEP_CPP_CMASK=\
	"..\include\cmask.h"\
	"..\include\colbut.h"\
	"..\include\comcthlp.h"\
	"..\include\image.h"\
	"..\include\inflib.h"\
	"..\include\itemc.h"\
	"..\include\mo_draw.h"\
	"..\include\rdonly.h"\
	"..\include\strfkt.h"\
	"..\include\wmaskc.h"\
	"..\libsrc\lbox.h"\
	

"$(INTDIR)\cmask.obj" : $(SOURCE) $(DEP_CPP_CMASK) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "showbm - Win32 Debug"

DEP_CPP_CMASK=\
	"..\include\cmask.h"\
	"..\include\colbut.h"\
	"..\include\comcthlp.h"\
	"..\include\image.h"\
	"..\include\inflib.h"\
	"..\include\itemc.h"\
	"..\include\mo_draw.h"\
	"..\include\rdonly.h"\
	"..\include\strfkt.h"\
	"..\include\wmaskc.h"\
	"..\libsrc\lbox.h"\
	

"$(INTDIR)\cmask.obj"	"$(INTDIR)\cmask.sbr" : $(SOURCE) $(DEP_CPP_CMASK)\
 "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\libsrc\colbut.cpp

!IF  "$(CFG)" == "showbm - Win32 Release"

DEP_CPP_COLBU=\
	"..\include\colbut.h"\
	"..\include\itemc.h"\
	"..\include\mo_draw.h"\
	"..\include\strfkt.h"\
	"..\include\wmaskc.h"\
	

"$(INTDIR)\colbut.obj" : $(SOURCE) $(DEP_CPP_COLBU) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "showbm - Win32 Debug"

DEP_CPP_COLBU=\
	"..\include\colbut.h"\
	"..\include\itemc.h"\
	"..\include\mo_draw.h"\
	"..\include\strfkt.h"\
	"..\include\wmaskc.h"\
	

"$(INTDIR)\colbut.obj"	"$(INTDIR)\colbut.sbr" : $(SOURCE) $(DEP_CPP_COLBU)\
 "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\libsrc\DBFUNC.CPP

!IF  "$(CFG)" == "showbm - Win32 Release"

DEP_CPP_DBFUN=\
	"..\include\comcthlp.h"\
	"..\include\dbfunc.h"\
	"..\include\itemc.h"\
	"..\include\MO_CURSO.H"\
	"..\include\mo_draw.h"\
	"..\include\mo_meld.h"\
	"..\include\strfkt.h"\
	"..\include\wmaskc.h"\
	

"$(INTDIR)\DBFUNC.OBJ" : $(SOURCE) $(DEP_CPP_DBFUN) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "showbm - Win32 Debug"


"$(INTDIR)\DBFUNC.OBJ"	"$(INTDIR)\DBFUNC.SBR" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\libsrc\dlg.cpp

!IF  "$(CFG)" == "showbm - Win32 Release"

DEP_CPP_DLG_C=\
	"..\include\cmask.h"\
	"..\include\colbut.h"\
	"..\include\comcthlp.h"\
	"..\include\dlg.h"\
	"..\include\formfield.h"\
	"..\include\image.h"\
	"..\include\itemc.h"\
	"..\include\MO_CURSO.H"\
	"..\include\mo_draw.h"\
	"..\include\mo_gdruck.h"\
	"..\include\mo_meld.h"\
	"..\include\stdfkt.h"\
	"..\include\strfkt.h"\
	"..\include\wmaskc.h"\
	

"$(INTDIR)\dlg.obj" : $(SOURCE) $(DEP_CPP_DLG_C) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "showbm - Win32 Debug"

DEP_CPP_DLG_C=\
	"..\include\cmask.h"\
	"..\include\colbut.h"\
	"..\include\comcthlp.h"\
	"..\include\dlg.h"\
	"..\include\formfield.h"\
	"..\include\image.h"\
	"..\include\itemc.h"\
	"..\include\MO_CURSO.H"\
	"..\include\mo_draw.h"\
	"..\include\mo_gdruck.h"\
	"..\include\mo_meld.h"\
	"..\include\stdfkt.h"\
	"..\include\strfkt.h"\
	"..\include\wmaskc.h"\
	

"$(INTDIR)\dlg.obj"	"$(INTDIR)\dlg.sbr" : $(SOURCE) $(DEP_CPP_DLG_C)\
 "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\libsrc\help.cpp

!IF  "$(CFG)" == "showbm - Win32 Release"

DEP_CPP_HELP_=\
	"..\include\cmask.h"\
	"..\include\comcthlp.h"\
	"..\include\dlg.h"\
	"..\include\formfield.h"\
	"..\include\help.h"\
	"..\include\image.h"\
	"..\include\itemc.h"\
	"..\include\itfont.h"\
	"..\include\itprog.h"\
	"..\include\MO_CURSO.H"\
	"..\include\mo_draw.h"\
	"..\include\mo_meld.h"\
	"..\include\stdfkt.h"\
	"..\include\strfkt.h"\
	"..\include\text.h"\
	"..\include\wmaskc.h"\
	

"$(INTDIR)\help.obj" : $(SOURCE) $(DEP_CPP_HELP_) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "showbm - Win32 Debug"

DEP_CPP_HELP_=\
	"..\include\cmask.h"\
	"..\include\comcthlp.h"\
	"..\include\dlg.h"\
	"..\include\formfield.h"\
	"..\include\help.h"\
	"..\include\image.h"\
	"..\include\itemc.h"\
	"..\include\itfont.h"\
	"..\include\itprog.h"\
	"..\include\MO_CURSO.H"\
	"..\include\mo_draw.h"\
	"..\include\mo_meld.h"\
	"..\include\stdfkt.h"\
	"..\include\strfkt.h"\
	"..\include\text.h"\
	"..\include\wmaskc.h"\
	

"$(INTDIR)\help.obj"	"$(INTDIR)\help.sbr" : $(SOURCE) $(DEP_CPP_HELP_)\
 "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\libsrc\image.cpp
DEP_CPP_IMAGE=\
	"..\include\image.h"\
	"..\include\mo_draw.h"\
	

!IF  "$(CFG)" == "showbm - Win32 Release"


"$(INTDIR)\image.obj" : $(SOURCE) $(DEP_CPP_IMAGE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "showbm - Win32 Debug"


"$(INTDIR)\image.obj"	"$(INTDIR)\image.sbr" : $(SOURCE) $(DEP_CPP_IMAGE)\
 "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\libsrc\itemc.cpp

!IF  "$(CFG)" == "showbm - Win32 Release"

DEP_CPP_ITEMC=\
	"..\include\cmask.h"\
	"..\include\comcthlp.h"\
	"..\include\dlg.h"\
	"..\include\formfield.h"\
	"..\include\help.h"\
	"..\include\image.h"\
	"..\include\inflib.h"\
	"..\include\itemc.h"\
	"..\include\itfont.h"\
	"..\include\itprog.h"\
	"..\include\MO_CURSO.H"\
	"..\include\mo_draw.h"\
	"..\include\mo_meld.h"\
	"..\include\strfkt.h"\
	"..\include\wmaskc.h"\
	

"$(INTDIR)\itemc.obj" : $(SOURCE) $(DEP_CPP_ITEMC) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "showbm - Win32 Debug"

DEP_CPP_ITEMC=\
	"..\include\cmask.h"\
	"..\include\comcthlp.h"\
	"..\include\dlg.h"\
	"..\include\formfield.h"\
	"..\include\help.h"\
	"..\include\image.h"\
	"..\include\inflib.h"\
	"..\include\itemc.h"\
	"..\include\itfont.h"\
	"..\include\itprog.h"\
	"..\include\MO_CURSO.H"\
	"..\include\mo_draw.h"\
	"..\include\mo_meld.h"\
	"..\include\strfkt.h"\
	"..\include\wmaskc.h"\
	

"$(INTDIR)\itemc.obj"	"$(INTDIR)\itemc.sbr" : $(SOURCE) $(DEP_CPP_ITEMC)\
 "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\libsrc\lbox.cpp

!IF  "$(CFG)" == "showbm - Win32 Release"

DEP_CPP_LBOX_=\
	"..\include\comcthlp.h"\
	"..\include\image.h"\
	"..\include\itemc.h"\
	"..\include\mo_draw.h"\
	"..\include\strfkt.h"\
	"..\include\wmaskc.h"\
	"..\libsrc\lbox.h"\
	

"$(INTDIR)\lbox.obj" : $(SOURCE) $(DEP_CPP_LBOX_) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "showbm - Win32 Debug"

DEP_CPP_LBOX_=\
	"..\include\comcthlp.h"\
	"..\include\image.h"\
	"..\include\itemc.h"\
	"..\include\mo_draw.h"\
	"..\include\strfkt.h"\
	"..\include\wmaskc.h"\
	"..\libsrc\lbox.h"\
	

"$(INTDIR)\lbox.obj"	"$(INTDIR)\lbox.sbr" : $(SOURCE) $(DEP_CPP_LBOX_)\
 "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\libsrc\mo_arg.cpp

!IF  "$(CFG)" == "showbm - Win32 Release"


"$(INTDIR)\mo_arg.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "showbm - Win32 Debug"


"$(INTDIR)\mo_arg.obj"	"$(INTDIR)\mo_arg.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=.\mo_bm.cpp

!IF  "$(CFG)" == "showbm - Win32 Release"

DEP_CPP_MO_BM=\
	"..\include\comcthlp.h"\
	"..\include\item.h"\
	"..\include\itemc.h"\
	"..\include\mo_arg.h"\
	"..\include\mo_draw.h"\
	"..\include\mo_meld.h"\
	"..\include\stdfkt.h"\
	"..\include\strfkt.h"\
	"..\include\wmaskc.h"\
	".\mo_bm.h"\
	

"$(INTDIR)\mo_bm.obj" : $(SOURCE) $(DEP_CPP_MO_BM) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "showbm - Win32 Debug"

DEP_CPP_MO_BM=\
	"..\include\comcthlp.h"\
	"..\include\item.h"\
	"..\include\itemc.h"\
	"..\include\mo_arg.h"\
	"..\include\mo_draw.h"\
	"..\include\mo_meld.h"\
	"..\include\stdfkt.h"\
	"..\include\strfkt.h"\
	"..\include\wmaskc.h"\
	".\mo_bm.h"\
	

"$(INTDIR)\mo_bm.obj"	"$(INTDIR)\mo_bm.sbr" : $(SOURCE) $(DEP_CPP_MO_BM)\
 "$(INTDIR)"


!ENDIF 

SOURCE=..\libsrc\mo_chqex.cpp

!IF  "$(CFG)" == "showbm - Win32 Release"

DEP_CPP_MO_CH=\
	"..\include\cmask.h"\
	"..\include\colbut.h"\
	"..\include\comcthlp.h"\
	"..\include\dlg.h"\
	"..\include\formfield.h"\
	"..\include\image.h"\
	"..\include\itemc.h"\
	"..\include\mo_chqex.h"\
	"..\include\MO_CURSO.H"\
	"..\include\mo_draw.h"\
	"..\include\mo_meld.h"\
	"..\include\stdfkt.h"\
	"..\include\strfkt.h"\
	"..\include\wmaskc.h"\
	"..\libsrc\lbox.h"\
	

"$(INTDIR)\mo_chqex.obj" : $(SOURCE) $(DEP_CPP_MO_CH) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "showbm - Win32 Debug"

DEP_CPP_MO_CH=\
	"..\include\cmask.h"\
	"..\include\colbut.h"\
	"..\include\comcthlp.h"\
	"..\include\dlg.h"\
	"..\include\formfield.h"\
	"..\include\image.h"\
	"..\include\itemc.h"\
	"..\include\mo_chqex.h"\
	"..\include\MO_CURSO.H"\
	"..\include\mo_draw.h"\
	"..\include\mo_meld.h"\
	"..\include\stdfkt.h"\
	"..\include\strfkt.h"\
	"..\include\wmaskc.h"\
	"..\libsrc\lbox.h"\
	

"$(INTDIR)\mo_chqex.obj"	"$(INTDIR)\mo_chqex.sbr" : $(SOURCE) $(DEP_CPP_MO_CH)\
 "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\libsrc\MO_CURSO.CPP

!IF  "$(CFG)" == "showbm - Win32 Release"

DEP_CPP_MO_CU=\
	"..\include\MO_CURSO.H"\
	"..\include\strfkt.h"\
	"..\include\tcursor.h"\
	

"$(INTDIR)\MO_CURSO.OBJ" : $(SOURCE) $(DEP_CPP_MO_CU) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "showbm - Win32 Debug"


"$(INTDIR)\MO_CURSO.OBJ"	"$(INTDIR)\MO_CURSO.SBR" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\libsrc\MO_DRAW.CPP

!IF  "$(CFG)" == "showbm - Win32 Release"

DEP_CPP_MO_DR=\
	"..\include\itemc.h"\
	"..\include\mo_draw.h"\
	"..\include\strfkt.h"\
	"..\include\text.h"\
	"..\include\wmaskc.h"\
	

"$(INTDIR)\MO_DRAW.OBJ" : $(SOURCE) $(DEP_CPP_MO_DR) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "showbm - Win32 Debug"

DEP_CPP_MO_DR=\
	"..\include\itemc.h"\
	"..\include\mo_draw.h"\
	"..\include\strfkt.h"\
	"..\include\text.h"\
	"..\include\wmaskc.h"\
	

"$(INTDIR)\MO_DRAW.OBJ"	"$(INTDIR)\MO_DRAW.SBR" : $(SOURCE) $(DEP_CPP_MO_DR)\
 "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\libsrc\MO_MELD.CPP

!IF  "$(CFG)" == "showbm - Win32 Release"

DEP_CPP_MO_ME=\
	"..\include\cmask.h"\
	"..\include\colbut.h"\
	"..\include\comcthlp.h"\
	"..\include\dlg.h"\
	"..\include\formfield.h"\
	"..\include\image.h"\
	"..\include\itemc.h"\
	"..\include\MO_CURSO.H"\
	"..\include\mo_draw.h"\
	"..\include\mo_meld.h"\
	"..\include\stdfkt.h"\
	"..\include\strfkt.h"\
	"..\include\wmaskc.h"\
	

"$(INTDIR)\MO_MELD.OBJ" : $(SOURCE) $(DEP_CPP_MO_ME) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "showbm - Win32 Debug"

DEP_CPP_MO_ME=\
	"..\include\cmask.h"\
	"..\include\colbut.h"\
	"..\include\comcthlp.h"\
	"..\include\dlg.h"\
	"..\include\formfield.h"\
	"..\include\image.h"\
	"..\include\itemc.h"\
	"..\include\MO_CURSO.H"\
	"..\include\mo_draw.h"\
	"..\include\mo_meld.h"\
	"..\include\stdfkt.h"\
	"..\include\strfkt.h"\
	"..\include\wmaskc.h"\
	

"$(INTDIR)\MO_MELD.OBJ"	"$(INTDIR)\MO_MELD.SBR" : $(SOURCE) $(DEP_CPP_MO_ME)\
 "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\libsrc\RBrush.cpp
DEP_CPP_RBRUS=\
	"..\include\rbrush.h"\
	

!IF  "$(CFG)" == "showbm - Win32 Release"


"$(INTDIR)\RBrush.obj" : $(SOURCE) $(DEP_CPP_RBRUS) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "showbm - Win32 Debug"


"$(INTDIR)\RBrush.obj"	"$(INTDIR)\RBrush.sbr" : $(SOURCE) $(DEP_CPP_RBRUS)\
 "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\libsrc\rdonly.cpp

!IF  "$(CFG)" == "showbm - Win32 Release"

DEP_CPP_RDONL=\
	"..\include\itemc.h"\
	"..\include\mo_draw.h"\
	"..\include\rdonly.h"\
	"..\include\strfkt.h"\
	"..\include\wmaskc.h"\
	

"$(INTDIR)\rdonly.obj" : $(SOURCE) $(DEP_CPP_RDONL) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "showbm - Win32 Debug"

DEP_CPP_RDONL=\
	"..\include\itemc.h"\
	"..\include\mo_draw.h"\
	"..\include\rdonly.h"\
	"..\include\strfkt.h"\
	"..\include\wmaskc.h"\
	

"$(INTDIR)\rdonly.obj"	"$(INTDIR)\rdonly.sbr" : $(SOURCE) $(DEP_CPP_RDONL)\
 "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\libsrc\RFont.cpp

!IF  "$(CFG)" == "showbm - Win32 Release"

DEP_CPP_RFONT=\
	"..\include\rfont.h"\
	"..\include\text.h"\
	

"$(INTDIR)\RFont.obj" : $(SOURCE) $(DEP_CPP_RFONT) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "showbm - Win32 Debug"

DEP_CPP_RFONT=\
	"..\include\rfont.h"\
	"..\include\text.h"\
	

"$(INTDIR)\RFont.obj"	"$(INTDIR)\RFont.sbr" : $(SOURCE) $(DEP_CPP_RFONT)\
 "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\libsrc\RPen.cpp
DEP_CPP_RPEN_=\
	"..\include\rpen.h"\
	

!IF  "$(CFG)" == "showbm - Win32 Release"


"$(INTDIR)\RPen.obj" : $(SOURCE) $(DEP_CPP_RPEN_) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "showbm - Win32 Debug"


"$(INTDIR)\RPen.obj"	"$(INTDIR)\RPen.sbr" : $(SOURCE) $(DEP_CPP_RPEN_)\
 "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=.\showbm.cpp

!IF  "$(CFG)" == "showbm - Win32 Release"

DEP_CPP_SHOWB=\
	"..\include\comcthlp.h"\
	"..\include\itemc.h"\
	"..\include\mo_arg.h"\
	"..\include\mo_draw.h"\
	"..\include\mo_meld.h"\
	"..\include\strfkt.h"\
	"..\include\wmaskc.h"\
	".\mo_bm.h"\
	

"$(INTDIR)\showbm.obj" : $(SOURCE) $(DEP_CPP_SHOWB) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "showbm - Win32 Debug"

DEP_CPP_SHOWB=\
	"..\include\comcthlp.h"\
	"..\include\itemc.h"\
	"..\include\mo_arg.h"\
	"..\include\mo_draw.h"\
	"..\include\mo_meld.h"\
	"..\include\strfkt.h"\
	"..\include\wmaskc.h"\
	".\mo_bm.h"\
	

"$(INTDIR)\showbm.obj"	"$(INTDIR)\showbm.sbr" : $(SOURCE) $(DEP_CPP_SHOWB)\
 "$(INTDIR)"


!ENDIF 

SOURCE=..\libsrc\STDFKT.CPP

!IF  "$(CFG)" == "showbm - Win32 Release"

DEP_CPP_STDFK=\
	"..\include\itemc.h"\
	"..\include\mo_draw.h"\
	"..\include\stdfkt.h"\
	"..\include\wmaskc.h"\
	

"$(INTDIR)\STDFKT.OBJ" : $(SOURCE) $(DEP_CPP_STDFK) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "showbm - Win32 Debug"

DEP_CPP_STDFK=\
	"..\include\itemc.h"\
	"..\include\mo_draw.h"\
	"..\include\stdfkt.h"\
	"..\include\wmaskc.h"\
	

"$(INTDIR)\STDFKT.OBJ"	"$(INTDIR)\STDFKT.SBR" : $(SOURCE) $(DEP_CPP_STDFK)\
 "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\libsrc\STRFKT.CPP
DEP_CPP_STRFK=\
	"..\include\strfkt.h"\
	

!IF  "$(CFG)" == "showbm - Win32 Release"


"$(INTDIR)\STRFKT.OBJ" : $(SOURCE) $(DEP_CPP_STRFK) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "showbm - Win32 Debug"


"$(INTDIR)\STRFKT.OBJ"	"$(INTDIR)\STRFKT.SBR" : $(SOURCE) $(DEP_CPP_STRFK)\
 "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\libsrc\Text.cpp

!IF  "$(CFG)" == "showbm - Win32 Release"

DEP_CPP_TEXT_=\
	"..\include\text.h"\
	"..\include\vector.h"\
	

"$(INTDIR)\Text.obj" : $(SOURCE) $(DEP_CPP_TEXT_) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "showbm - Win32 Debug"

DEP_CPP_TEXT_=\
	"..\include\text.h"\
	"..\include\vector.h"\
	

"$(INTDIR)\Text.obj"	"$(INTDIR)\Text.sbr" : $(SOURCE) $(DEP_CPP_TEXT_)\
 "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\libsrc\VBrush.cpp

!IF  "$(CFG)" == "showbm - Win32 Release"

DEP_CPP_VBRUS=\
	"..\include\rbrush.h"\
	"..\include\vbrush.h"\
	"..\include\vector.h"\
	

"$(INTDIR)\VBrush.obj" : $(SOURCE) $(DEP_CPP_VBRUS) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "showbm - Win32 Debug"

DEP_CPP_VBRUS=\
	"..\include\rbrush.h"\
	"..\include\vbrush.h"\
	"..\include\vector.h"\
	

"$(INTDIR)\VBrush.obj"	"$(INTDIR)\VBrush.sbr" : $(SOURCE) $(DEP_CPP_VBRUS)\
 "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\libsrc\Vector.cpp
DEP_CPP_VECTO=\
	"..\include\vector.h"\
	

!IF  "$(CFG)" == "showbm - Win32 Release"


"$(INTDIR)\Vector.obj" : $(SOURCE) $(DEP_CPP_VECTO) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "showbm - Win32 Debug"


"$(INTDIR)\Vector.obj"	"$(INTDIR)\Vector.sbr" : $(SOURCE) $(DEP_CPP_VECTO)\
 "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\libsrc\VFont.cpp

!IF  "$(CFG)" == "showbm - Win32 Release"

DEP_CPP_VFONT=\
	"..\include\rfont.h"\
	"..\include\text.h"\
	"..\include\vector.h"\
	"..\include\vfont.h"\
	

"$(INTDIR)\VFont.obj" : $(SOURCE) $(DEP_CPP_VFONT) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "showbm - Win32 Debug"

DEP_CPP_VFONT=\
	"..\include\rfont.h"\
	"..\include\text.h"\
	"..\include\vector.h"\
	"..\include\vfont.h"\
	

"$(INTDIR)\VFont.obj"	"$(INTDIR)\VFont.sbr" : $(SOURCE) $(DEP_CPP_VFONT)\
 "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\libsrc\VPen.cpp

!IF  "$(CFG)" == "showbm - Win32 Release"

DEP_CPP_VPEN_=\
	"..\include\rpen.h"\
	"..\include\vector.h"\
	"..\include\vpen.h"\
	

"$(INTDIR)\VPen.obj" : $(SOURCE) $(DEP_CPP_VPEN_) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "showbm - Win32 Debug"

DEP_CPP_VPEN_=\
	"..\include\rpen.h"\
	"..\include\vector.h"\
	"..\include\vpen.h"\
	

"$(INTDIR)\VPen.obj"	"$(INTDIR)\VPen.sbr" : $(SOURCE) $(DEP_CPP_VPEN_)\
 "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\libsrc\wmaskc.cpp

!IF  "$(CFG)" == "showbm - Win32 Release"

DEP_CPP_WMASK=\
	"..\include\comcthlp.h"\
	"..\include\itemc.h"\
	"..\include\message.h"\
	"..\include\mo_draw.h"\
	"..\include\rbrush.h"\
	"..\include\rfont.h"\
	"..\include\rpen.h"\
	"..\include\strfkt.h"\
	"..\include\text.h"\
	"..\include\vbrush.h"\
	"..\include\vector.h"\
	"..\include\vfont.h"\
	"..\include\vpen.h"\
	"..\include\wmaskc.h"\
	

"$(INTDIR)\wmaskc.obj" : $(SOURCE) $(DEP_CPP_WMASK) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "showbm - Win32 Debug"

DEP_CPP_WMASK=\
	"..\include\comcthlp.h"\
	"..\include\itemc.h"\
	"..\include\message.h"\
	"..\include\mo_draw.h"\
	"..\include\rbrush.h"\
	"..\include\rfont.h"\
	"..\include\rpen.h"\
	"..\include\strfkt.h"\
	"..\include\text.h"\
	"..\include\vbrush.h"\
	"..\include\vector.h"\
	"..\include\vfont.h"\
	"..\include\vpen.h"\
	"..\include\wmaskc.h"\
	

"$(INTDIR)\wmaskc.obj"	"$(INTDIR)\wmaskc.sbr" : $(SOURCE) $(DEP_CPP_WMASK)\
 "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 


!ENDIF 

