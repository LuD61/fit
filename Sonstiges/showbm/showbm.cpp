#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include "comcthlp.h"
#include "strfkt.h"
#include "itemc.h"
#include "wmaskc.h"
#include "mo_draw.h"
#include "mo_bm.h"
#include "mo_meld.h"
#include "mo_arg.h"
#include "a_bas.h"
#include "dbclass.h"

int      PASCAL WinMain(HANDLE,HANDLE,LPSTR,int);
LONG FAR PASCAL WndProc(HWND,UINT,WPARAM,LPARAM);
static int ProcessMessages(void);
void     InitFirstInstance(HANDLE);
BOOL     InitNewInstance(HANDLE, int);


HANDLE  hMainInst;
HWND    hMainWindow;
HWND    mamain1;
HWND    mamain2 = NULL; 
HMENU hMenu;

extern BmClass BitMap;
extern	    char *etc;
extern		char buffer [512];
extern		FILE *fp;
extern		double a_bas_a ;

static DB_CLASS DbClass;

static char bmp_pfad [256];  
static char bmp_pfad2 [256];  
int test = 0;

static int bmmode;
static char caption [256] = "Bitmap Reader";
static int captmode = 1;
static int Savemode = 0;
static int bmrow = CW_USEDEFAULT;
static int bmcolumn = CW_USEDEFAULT;
static int bmcrow = CW_USEDEFAULT;
static int bmccolumn = CW_USEDEFAULT;
static int bmtopmost = 0;

struct PMENUE dateimen[] = {
						    "&Drucken",    " ", NULL, IDM_PRINT,
							"",               "S", NULL, 0, 
	                        "&Beenden", " ", NULL, KEY5,
						     NULL, NULL, NULL, 0};

struct PMENUE bearbmen[] = {
	                        "&Zoomfaktor",  " ", NULL, KEY6, 
                             NULL, NULL, NULL, 0};

struct PMENUE menuetab[] = {"&Datei",      "M", dateimen, 0, 
                            "&Bearbeiten", "M", bearbmen, 0, 
						     NULL, NULL, NULL, 0};


/* Daten fuer Zoofaktor           */

HWND zoom = NULL;

static char tzoom[] = {"Zoomfaktor  "};

static ITEM vHeightT ("" , tzoom, "",  0);
static ITEM vHeight ("" , "      ", "",   0);
static ITEM vOK     ("",  "OK",  "", 0);
static ITEM vCancel ("",  "Abbrechen", "", 0);


static int testcr (void);

static field _fzoom [] = {
&vHeightT, 13, 0, 1, 1, 0, "", DISPLAYONLY, 0, 0, 0, 
&vHeight,   6, 0, 1,15, 0, "%3d", EDIT | UPDOWN, 0, testcr, 0, 
&vOK,       4, 0, 5, 4, 0, "", BUTTON | DEFBUTTON, 0, testcr, KEY12,
&vCancel,  11, 0, 5, 9, 0, "", BUTTON, 0, testcr, KEY5, 
};

static form fzoom = {4, 0, 0, _fzoom, 0, 0, 0, 0, NULL}; 

static struct UDTAB udtab[] = {500, 1, 100};
static int udanz = 1;

static TEXTMETRIC tm;

int eBreak (void)
{
       syskey = KEY5;
       break_enter ();
       return 0;
}

int eOk (void)
{
       syskey = KEY12;

       GetWindowText (fzoom.mask [1].feldid,
                      fzoom.mask [1].item->GetFeldPtr (),
                      fzoom.mask [1].length);
       break_enter ();
       return 0;
}


int testcr (void)
{
      
      if (syskey == KEYCR)
      {
           if (currentfield == 3)
           {
               syskey = KEY5;
           }
           break_enter ();
       }
       else if (syskey == KEY12)
       {
           break_enter ();
       }
       else if (syskey == KEY5)
       {
           break_enter ();
       }
       else if (syskey == KEYESC)
       {
           break_enter ();
       }
       return 0;
}


void ZoomLines (HDC hdc)
{
         static HPEN hPenG = NULL;
         static HPEN hPenW = NULL;
         RECT rect;
         int x, y;
         int cx, cy;

         GetClientRect (zoom, &rect);
         x = tm.tmAveCharWidth / 2;
         y = tm.tmHeight / 2;

         cx = rect.right - tm.tmAveCharWidth / 2;
         cy = rect.bottom - tm.tmHeight / 2;
         if (hPenG == NULL)
         {
                   hPenG = CreatePen (PS_SOLID, 0, GRAYCOL);
                   hPenW = CreatePen (PS_SOLID, 0, WHITECOL);
         }
         SelectObject (hdc, hPenG);

         MoveToEx (hdc, x, y, NULL);
         LineTo (hdc, cx, y);

         MoveToEx (hdc, x, y, NULL);
         LineTo (hdc, x, cy);

         MoveToEx (hdc, cx, y, NULL);
         LineTo (hdc, cx, cy);

         MoveToEx (hdc, x, cy, NULL);
         LineTo (hdc, cx, cy);

         SelectObject (hdc, hPenW);


         y ++;
         x ++;
         cx --;
         cy --;
         MoveToEx (hdc, x, y, NULL);
         LineTo (hdc, cx, y);

         MoveToEx (hdc, x, y, NULL);
         LineTo (hdc, x, cy);

         cx += 2;
         cy += 2;
         MoveToEx (hdc, cx, y, NULL);
         LineTo (hdc, cx, cy);

         MoveToEx (hdc, x, cy, NULL);
         LineTo (hdc, cx, cy);

}



void SetZoom (void)
/**
Font eingeben.
**/
{
        HDC hdc; 
        double zm;
        double zoom0;

        stdfont ();

        save_fkt (5);
        save_fkt (12);

        set_fkt (eBreak, 5);
        set_fkt (eOk, 12);

        sprintf (fzoom.mask[1].item->GetFeldPtr (), "%3d", 
                 100);       

        SetTmFont (hMainWindow, &tm);
        no_break_end ();
        SetButtonTab (TRUE);
        DisablehWnd (hMainWindow);

        zoom  = CreateWindow (
                                 "hStdWindow",
                                 "Zoom-Faktor",
                                 WS_POPUP | 
                                 WS_VISIBLE | 
                                 WS_CAPTION | 
                                 WS_DLGFRAME, 
                                 40, 40,
                                 300, 180,
                                 GetActiveWindow (),
                                 NULL,
                                 hMainInst,
                                 NULL);

        ShowWindow (zoom, SW_SHOW);
        UpdateWindow (zoom);

        hdc = GetDC (zoom);
        ZoomLines (hdc);
        ReleaseDC (zoom, hdc);
        SetUpDownTab (udtab , udanz);
        DisplayAfterEnter (FALSE);
        enter_form (zoom, &fzoom, 0, 0);
        DestroyWindow (zoom);
        DisplayAfterEnter (TRUE);
        SetAktivWindow (hMainWindow);
        zoom = NULL;
        SetButtonTab (FALSE);
        restore_fkt (5);
        restore_fkt (12);

        if (syskey == KEY5) 
        {
            syskey = 9;
            return;
        }
        if (syskey == KEYESC)
        {
            syskey = 9;
            return;
        }

        syskey = 0;
        zm = ratod (fzoom.mask[1].item->GetFeldPtr ());
        zm /= 100;
        zoom0 = BitMap.GetZoom ();

        zoom0 = zoom0 * zm;
        BitMap.SetZoom (zoom0);
        InvalidateRect (BitMap.Getmamain2 (), NULL, TRUE);
        UpdateWindow (BitMap.Getmamain2 ());
        BitMap.TestTrack ();
        BitMap.TestVTrack ();
}


int dokey5 ()
/**
Aktion bei Taste F5
**/
{
        break_enter ();
        return 1;
}

void tst_arg (char *arg)
{
          for (; *arg; arg += 1)
          {
                 switch (*arg)
                 {
                           case 'b' :
                                     captmode = atoi (arg + 1);
                                     return;
                            case 'c' :
                                     strcpy (caption, arg + 1);
                                     return;
                            case 'm' :
                                     bmmode = atoi (arg + 1); 
                                     return;
                            case 'n' :
                                     captmode = 0;
                                     break;
                            case 'S' :
                                     Savemode = 1;
                                     break;
                            case 'z' :
                                     bmrow = atoi (arg + 1); 
                                     return;
                            case 's' :
                                     bmcolumn = atoi (arg + 1); 
                                     return;
                            case 'h' :
                                     bmcrow = atoi (arg + 1); 
                                     return;
                            case 'w' :
                                     bmccolumn = atoi (arg + 1); 
                                     return;
                            case 'v' :
                                     bmtopmost = 1; 
                                     break;
                 }
          }
          return;
}

static void hilfe (void)
{
       MoveWindow (hMainWindow, 0, 0, 0, 0, TRUE);
       ShowWindow (hMainWindow, SW_HIDE);
       disp_mess ("showbm -nvcTitle -mSizemode -zZeile -sSpalte -hH�he -wWeite Bitmap\n"
                  "          -n  Keine �berschrift\n"
                  "          -v  Bild immer im Vordergrund\n"
                  "          -m1 Windowgr��e wie Bitmap\n"
                  "          -m2 Bitmapgr��e wie Window" , 2);
}

BOOL hole_bmp_pfad (void)
{
				char a_bas_bild  [128] ;
			    a_bas_a = ratod(bmp_pfad);

			    DbClass.sqlin ((double *) &a_bas_a, 3, 0);
		        DbClass.sqlout ((char *) a_bas_bild, 0, 128);
				DbClass.sqlcomm ("select bild from a_bas where a = ?");
				if (sqlstatus == 0)
				{
					if (strlen (a_bas_bild) > 10)
					{
						strcpy (bmp_pfad,a_bas_bild);
					} else return FALSE;
				} else return FALSE;
				return TRUE;

}


int    PASCAL WinMain(HANDLE hInstance,HANDLE hPrevInstance,
                        LPSTR lpszCmdLine,int nCmdShow)
{
       int anz;
       char *varargs[20];
       char argtab[20][80];
       int i;
		int x, y, cx, cy;

		 char teststr [255];

//		strcpy(teststr,"-S d:\\user\\fit\\bilder\\artikel\\4.bmp");
		strcpy(teststr,"-S 4");

       anz = wsplit (lpszCmdLine, " ");
//       anz = wsplit (teststr, " ");
       for (i = 0; i < anz; i ++)
       {
            strcpy (argtab[i], wort[i]);
            varargs[i] = (char *) argtab[i];
       }
       argtst (&anz, varargs, tst_arg);
	   strcpy (bmp_pfad, varargs[0]);	
       SetStdProc (WndProc);
       InitFirstInstance (hInstance);

       SetEnvFont ();
       SetAktivWindow (NULL);

       if (captmode == 1)
       {
              SetBorder (WS_CAPTION | WS_THICKFRAME | 
                         WS_MAXIMIZEBOX | WS_MINIMIZEBOX |
                         WS_SYSMENU);
       }
       else if (captmode == 2)
       {
              SetBorder (WS_POPUP);
       }
       else if (captmode == 3)
       {
              SetBorder (WS_POPUP | WS_BORDER);
       }
       else if (captmode == 4)
       {
              SetBorder (WS_POPUP);
       }
       else
       {
              SetBorder (WS_POPUP | WS_THICKFRAME);
       }


	   if (Savemode == 1)  //aus gesicherten Werte aus bild.rct die position ermitteln 
       {
			bmmode = 2;
			if (strlen (clipped(bmp_pfad)) < 14)
			{
		        opendbase ("bws");
				if (hole_bmp_pfad () == FALSE) return 0;
			}
			etc = getenv ("BWSETC");
			if (etc == NULL) return NULL;
			sprintf (buffer, "%s\\%s.rct", etc,"51100_bmp");
			fp = fopen (buffer, "r");
			if (fp == NULL) return 0;

			while (fgets (buffer, 511, fp))
			{
				cr_weg (buffer);
				anz = wsplit (buffer, " ");
				if (anz < 2) continue;
				if (strcmp (wort[0], "left") == 0)
				{
					x = atoi (wort[1]);
				}
				else if (strcmp (wort[0], "top") == 0)
				{
					y = atoi (wort[1]);
				}
				else if (strcmp (wort[0], "right") == 0)
				{
					cx = atoi (wort[1]);
				}
				else if (strcmp (wort[0], "bottom") == 0)
				{
					cy = atoi (wort[1]);
				}
			}
			fclose (fp);

              hMainWindow = OpenWindowRect (x, 
                                    y, 
                                    cx, 
                                    cy, hMainInst,
                                    caption);  
       }
       else if (captmode < 4)
       {
              hMainWindow = OpenWindowChC (bmcrow, 
                                    bmccolumn, 
                                    bmrow, 
                                    bmcolumn, hMainInst,
                                    caption);  
       }
       else
       {
              hMainWindow = OpenWindowChEx (bmcrow, 
                                    bmccolumn, 
                                    bmrow, 
                                    bmcolumn, hMainInst,
                                    caption);  
       }

       if (captmode == 1 && bmmode == 0)
       {
	          hMenu = MakeMenue (menuetab);
  	          SetMenu (hMainWindow, hMenu);
       }

       if (bmtopmost)
       {
               SetWindowPos (hMainWindow, HWND_TOPMOST, 0, 0, 0, 0,
                     SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
       }
       if (anz < 1) 
       {
           hilfe ();
           return (0);
       }
       BitMap.InitBMWindow (hMainWindow); 
	   BOOL ret = TRUE;
       if (bmmode == 1)
       {
           ret = BitMap.SetBmToScrEx (bmp_pfad);
       }
       else if (bmmode == 2)
       {
           ret = BitMap.SetBmToScrStr (bmp_pfad);
       }
       else if (bmmode == 3)
       {
           ret = BitMap.SetBmToScrStrX (bmp_pfad);
       }
       else
       {
           ret = BitMap.SetBmToScrCe (bmp_pfad);
       }

     if (ret == FALSE) return 0;
	 ProcessMessages ();
       return 0;
}

void InitFirstInstance(HANDLE hInstance)
{
        WNDCLASS wc;
        wc.style         =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                            | CS_CLASSDC;
        wc.lpfnWndProc   =  WndProc;
        wc.cbClsExtra    =  0;
        wc.cbWndExtra    =  0;
        wc.hInstance     =  hInstance;
        wc.hIcon         =  LoadIcon (hInstance, "ROSIICON");
        wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
        wc.lpszMenuName  =  "";
        wc.lpszClassName =  "hStdWindow";

        RegisterClass(&wc);

        wc.hIcon         =  LoadIcon (hInstance, "NOICON");
        wc.lpszMenuName  =  NULL;
        wc.lpszClassName =  "ListMain";
        RegisterClass(&wc);

        wc.hbrBackground =  GetStockObject (WHITE_BRUSH);
        wc.lpszClassName =  "hBmWindow";
        RegisterClass(&wc);

        hMainInst = hInstance;
        return;
}

void holeArtikel (void)
{
				sprintf (buffer, "%s\\%s", etc,"showbm.a");
				fp = fopen (buffer, "r");
				if (fp != NULL) 
				{
					if (fgets (buffer, 511, fp))
					{
						cr_weg (buffer);
					}
					fclose (fp);
					strcpy (bmp_pfad,buffer);
					if (ratod(buffer) != a_bas_a)
					{
						if (hole_bmp_pfad () == TRUE)
						{
							BitMap.InitBMWindow (hMainWindow); 
							BitMap.SetBmToScrStr (bmp_pfad);
		 				    test = 0;
						}
					}
				}
}

static int ProcessMessages(void)
{
        MSG msg;

        while (GetMessage (&msg, NULL, 0, 0))
        {
             if (msg.message == WM_KEYDOWN)
             {
                       switch (msg.wParam)
                       {
                              case VK_F5 :
                                       return msg.wParam;
                              case VK_F6 :
                                       SetZoom ();
                                       break;
                              case VK_F7 :
                                       holeArtikel ();
                                       break;
                       }
             }
             TranslateMessage(&msg);
             DispatchMessage(&msg);
		}
        return msg.wParam;
}
/**
static int ProcessMessages(void)
{
        MSG msg;

        while (GetMessage (&msg, NULL, 0, 0))
        {
             if (msg.message == WM_KEYDOWN)
             {
                       switch (msg.wParam)
                       {
                              case VK_F5 :
                                       return msg.wParam;
                              case VK_F6 :
                                       SetZoom ();
                                       break;
                              case VK_F7 :
                                       holeArtikel ();
                                       break;
                       }
             }
             if (msg.message == WM_PAINT)
             {
				 test++;
			 }
             TranslateMessage(&msg);
             DispatchMessage(&msg);
			 if (test == 2 && a_bas_a)
			 {
				   test = 0;
                   SendKey (VK_F7);
			 }
		}
        return msg.wParam;
}
*/



LONG FAR PASCAL WndProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        PAINTSTRUCT ps;
        HDC hdc;

        switch(msg)
        {
              case WM_PAINT :
                      if (hWnd == hMainWindow)
                      {
                              hdc = BeginPaint (hMainWindow, &ps);
                              EndPaint (mamain1, &ps);
                      }
                      else if (hWnd == zoom)
                      { 
                              hdc = BeginPaint (zoom, &ps);
                              ZoomLines (hdc); 
                              EndPaint (zoom, &ps);
                      } 
                      else
                      {
                              BitMap.OnPaint (hWnd);
                      }
                      break;
              case WM_HSCROLL :
                       BitMap.OnHScroll (hWnd, msg, wParam, lParam);
                       break;
                      
              case WM_VSCROLL :
                       BitMap.OnVScroll (hWnd, msg, wParam, lParam);
                       break;
              case WM_KEYDOWN :
                       switch (wParam)
                       {
                              case VK_DOWN :
                                   BitMap.OnVScroll (hWnd, WM_VSCROLL,
                                                     SB_LINEDOWN, (LPARAM) BitMap.GetvTrack ());
                                   break;
                              case VK_UP :
                                   BitMap.OnVScroll (hWnd, WM_VSCROLL,
                                                     SB_LINEUP, (LPARAM) BitMap.GetvTrack ());
                                   break;
                              case VK_NEXT :
                                   BitMap.OnVScroll (hWnd, WM_VSCROLL,
                                                     SB_PAGEDOWN,  (LPARAM) BitMap.GetvTrack ());
                                   break;
                              case VK_PRIOR :
                                   BitMap.OnVScroll (hWnd, WM_VSCROLL,
                                                     SB_PAGEUP,  (LPARAM) BitMap.GetvTrack ());
                                   break;
                              case VK_RIGHT :
                                   BitMap.OnHScroll (hWnd, WM_HSCROLL,
                                                     SB_LINEDOWN,  (LPARAM) BitMap.GethTrack ());
                                   break;
                              case VK_LEFT :
                                   BitMap.OnHScroll (hWnd, WM_VSCROLL,
                                                     SB_LINEUP,  (LPARAM) BitMap.GethTrack ());
                                   break;
                       }

              case WM_MOVE :
                      break;
              case WM_SIZE :
                              BitMap.OnSize (hWnd);
                      break;

              case WM_NOTIFY :
                    {
                              break;
                    }

              case WM_DESTROY :
                      if (hWnd == hMainWindow)
                      {
                             ExitProcess (0);
                             return 0;
                      }
                      break;

              case WM_COMMAND :
                    if (LOWORD (wParam) == KEY2)
                    {
                            syskey = KEY2;
                            SendKey (VK_F2);
                            break;
                    }
                    if (LOWORD (wParam) == KEY5)
                    {
                            syskey = KEY5;
                            SendKey (VK_F5);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY6)
                    {
                            syskey = KEY6;
                            SendKey (VK_F6);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY7)
                    {
                            syskey = KEY7;
                            SendKey (VK_F7);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY8)
                    {
                            syskey = KEY8;
                            SendKey (VK_F8);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY9)
                    {
                            syskey = KEY9;
                            SendKey (VK_F9);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY10)
                    {
                            syskey = KEY10;
                            SendKey (VK_F10);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY11)
                    {
                            syskey = KEY11;
                            SendKey (VK_F11);
                            break;
                    }
                    if (LOWORD (wParam) == KEY12)
                    {
                            syskey = KEY12;
                            SendKey (VK_F12);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYPGD)
                    {
                            syskey = KEYPGD;
                            SendKey (VK_NEXT);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYPGU)
                    {
                            syskey = KEYPGU;
                            SendKey (VK_PRIOR);
                            break;
                    }
                    else if (LOWORD (wParam) == IDM_INFO)
                    {
                                  syskey = KEY4;
                                  SendKey (VK_F4);
                    }
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}






/**************
#include <windows.h> 
#include <stdio.h> 
 
#define THREADCOUNT 4 
DWORD dwTlsIndex; 
 
VOID ErrorExit(LPSTR); 
 
VOID CommonFunc(VOID) 
{ 
   LPVOID lpvData; 
 
// Retrieve a data pointer for the current thread. 
 
   lpvData = TlsGetValue(dwTlsIndex); 
   if ((lpvData == 0) && (GetLastError() != ERROR_SUCCESS)) 
      ErrorExit("TlsGetValue error"); 
 
// Use the data stored for the current thread. 
 
   printf("common: thread %d: lpvData=%lx\n", 
      GetCurrentThreadId(), lpvData); 
 
   Sleep(5000); 
} 
 
DWORD WINAPI ThreadFunc(VOID) 
{ 
   LPVOID lpvData; 
 
// Initialize the TLS index for this thread. 
 
   lpvData = (LPVOID) LocalAlloc(LPTR, 256); 
   if (! TlsSetValue(dwTlsIndex, lpvData)) 
      ErrorExit("TlsSetValue error"); 
 
   printf("thread %d: lpvData=%lx\n", GetCurrentThreadId(), lpvData); 
 
   CommonFunc(); 
 
// Release the dynamic memory before the thread returns. 
 
   lpvData = TlsGetValue(dwTlsIndex); 
   if (lpvData != 0) 
      LocalFree((HLOCAL) lpvData); 
 
   return 0; 
} 
 
int main(VOID) 
{ 
   DWORD IDThread; 
   HANDLE hThread[THREADCOUNT]; 
   int i; 
 
// Allocate a TLS index. 
 
   if ((dwTlsIndex = TlsAlloc()) == TLS_OUT_OF_INDEXES) 
      ErrorExit("TlsAlloc failed"); 
 
// Create multiple threads. 
 
   for (i = 0; i < THREADCOUNT; i++) 
   { 
      hThread[i] = CreateThread(NULL, // default security attributes 
         0,                           // use default stack size 
         (LPTHREAD_START_ROUTINE) ThreadFunc, // thread function 
         NULL,                    // no thread function argument 
         0,                       // use default creation flags 
         &IDThread);              // returns thread identifier 
 
   // Check the return value for success. 
      if (hThread[i] == NULL) 
         ErrorExit("CreateThread error\n"); 
   } 
 
   for (i = 0; i < THREADCOUNT; i++) 
      WaitForSingleObject(hThread[i], INFINITE); 
 
   TlsFree(dwTlsIndex);

   return 0; 
} 
 
VOID ErrorExit (LPSTR lpszMessage) 
{ 
   fprintf(stderr, "%s\n", lpszMessage); 
   ExitProcess(0); 
}
********/