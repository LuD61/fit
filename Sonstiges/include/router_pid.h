#ifndef _ROUTER_PID_DEF
#define _ROUTER_PID_DEF

extern struct ROUTER_PID router_pid, router_pid_null;

class ROUTER_PID_CLASS
{
       private :
            short cursor_mdm;
            short cursor_router_pid;
            short cursor_router_pid_ip;

            void out_quest_all (void);
            void prepare_mdm (void);       
            void prepare_router_pid (char *);       
            void prepare_router_pid_ip (void);       
       public:
           ROUTER_PID_CLASS ()
           {
                    cursor_mdm      = -1;
                    cursor_router_pid = -1;
                    cursor_router_pid_ip = -1;
           }
           int lese_mdm (short);
           int lese_mdm (void);
           int lese_router_ip (char *);
           int lese_router_ip (void);
           int lese_router_pid_ip (void);

           void close_mdm (void);
           void close_router_ip (void);
           void close_router_pid_ip (void);
};

#endif