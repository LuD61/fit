#ifndef _PRG_PROT_DEF
#define _PRG_PROT_DEF

#include "windows.h"
#include "mo_draw.h"
#include "wmask.h"
#include "dbclass.h"

struct PRG_PROT {
   long      propramm;
   long      bearb;
   char      zeit[9];
   char      pers_nam[9];
   short     mdn;
   long      pr_gr_stuf;
   long      kun;
   long      kun_pr;
   double    a;
   double    vk_pr_i;
   double    vk_pr_eu;
   double    ld_pr;
   double    ld_pr_eu;
   short     aktion_nr;
   long      aki_von;
   long      aki_bis;
   short     loesch;
   char      kond_art[5];
   char      tabelle[21];
};
extern struct PRG_PROT prg_prot, prg_prot_null;

#line 10 "prg_prot.rh"

class PRG_PROT_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               PRG_PROT_CLASS () : DB_CLASS ()
               {
               }
               int dbupdate (void);
};
#endif
