#include <windows.h>
#include "string.h"
#include "itemc.h"
#include "inflib.h"
#include "help.h"

#ifndef CONSOLE
extern HWND hMainWindow;
#endif

#ifdef _DEBUG
#include <crtdbg.h>
extern _CrtMemState memory;
#endif

char *ITEM::HelpName = NULL;
void (*ITEM::PrintMess) (char *) = NULL;

void ITEM::SetHelpName (char *Name)
{
       HelpName = Name;
}

void ITEM::SetPrintMess (void (*Pr) (char *))
{
       PrintMess = Pr;
}

void ITEM::SetFormatFeld (char *format, ...)
{
	   va_list args;

       va_start (args, format);
       vsprintf (feld, format, args);
       va_end (args);
}

int ITEM::GetDBItem (char *itname)
{
	   return 0;
}

void ITEM::PrintInfo (void)
{
       return;
} 
                
void ITEM::PrintHelp (void)
{
       return;
}    
             
void ITEM::PrintComment (void)
{
          HELP *Help; 
          char Comment [256];
          HINSTANCE hInstance;

          if (PrintMess == NULL) return;


          hInstance = (HINSTANCE) GetClassLong (hMainWindow, GCL_HMODULE); 
//_CrtMemCheckpoint( &memory );
          Help = new HELP (HelpName, itemname, hInstance,hMainWindow, INITCOMM);
          Help->GetItem (itemname, Comment);
          (*PrintMess) (Comment);
          delete Help;
//_CrtMemCheckpoint( &memory );
}    
             
void ITEM::CallInfo (void)
{
#ifndef NINFO       
	   char where [256];

	   if (strlen (itemname))
	   {

			 sprintf (where, "where %s = %s", itemname, feld);
     	     _CallInfoEx (hMainWindow, NULL, itemname, where, 0l); 
	   }
#endif
       return; 
} 
