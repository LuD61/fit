#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "a_kun.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "we_jour.h"

struct WE_JOUR we_jour, we_jour_null;

void WE_JOUR_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &we_jour.mdn, 1, 0);
            ins_quest ((char *) &we_jour.fil, 1, 0);
            ins_quest ((char *) &we_jour.lief_rech_nr, 0, 17);
            ins_quest ((char *) &we_jour.lief, 0, 17);
            ins_quest ((char *) &we_jour.a, 3, 0);
            ins_quest ((char *) &we_jour.blg_typ, 0, 2);
            ins_quest ((char *) &we_jour.int_pos, 2, 0);
    out_quest ((char *) &we_jour.a,3,0);
    out_quest ((char *) &we_jour.abw_lief_proz,3,0);
    out_quest ((char *) &we_jour.abw_rab_betr,3,0);
    out_quest ((char *) &we_jour.abw_rab_nat,3,0);
    out_quest ((char *) &we_jour.abw_rab_proz,3,0);
    out_quest ((char *) &we_jour.abw_zusch_proz,3,0);
    out_quest ((char *) &we_jour.akv,2,0);
    out_quest ((char *) &we_jour.alarm_prio,2,0);
    out_quest ((char *) &we_jour.anz_einh,3,0);
    out_quest ((char *) &we_jour.bearb,2,0);
    out_quest ((char *) &we_jour.best_blg,2,0);
    out_quest ((char *) &we_jour.best_me,3,0);
    out_quest ((char *) we_jour.best_zuord_kz,0,2);
    out_quest ((char *) we_jour.blg_typ,0,2);
    out_quest ((char *) we_jour.diff_kz,0,2);
    out_quest ((char *) &we_jour.fil,1,0);
    out_quest ((char *) &we_jour.hbk_dat,2,0);
    out_quest ((char *) &we_jour.inh,3,0);
    out_quest ((char *) &we_jour.int_pos,2,0);
    out_quest ((char *) we_jour.lief,0,17);
    out_quest ((char *) we_jour.lief_best,0,17);
    out_quest ((char *) we_jour.lief_rech_nr,0,17);
    out_quest ((char *) &we_jour.lief_s,2,0);
    out_quest ((char *) &we_jour.mdn,1,0);
    out_quest ((char *) &we_jour.me,3,0);
    out_quest ((char *) &we_jour.me_einh,1,0);
    out_quest ((char *) &we_jour.mwst,1,0);
    out_quest ((char *) &we_jour.p_num,1,0);
    out_quest ((char *) we_jour.pers_nam,0,9);
    out_quest ((char *) we_jour.pr_aend_kz,0,2);
    out_quest ((char *) &we_jour.pr_ek,3,0);
    out_quest ((char *) &we_jour.pr_ek_nto,3,0);
    out_quest ((char *) &we_jour.pr_vk,3,0);
    out_quest ((char *) we_jour.qua_kz,0,2);
    out_quest ((char *) &we_jour.rab_eff,3,0);
    out_quest ((char *) we_jour.rab_kz,0,2);
    out_quest ((char *) we_jour.sa_kz,0,2);
    out_quest ((char *) we_jour.skto_kz,0,2);
    out_quest ((char *) &we_jour.tara,3,0);
    out_quest ((char *) &we_jour.tara_proz,3,0);
    out_quest ((char *) &we_jour.we_txt,2,0);
    out_quest ((char *) we_jour.sti_pro_kz,0,2);
    out_quest ((char *) we_jour.me_kz,0,2);
    out_quest ((char *) &we_jour.a_krz,2,0);
    out_quest ((char *) &we_jour.we_kto,2,0);
    out_quest ((char *) &we_jour.buch_kz,1,0);
    out_quest ((char *) &we_jour.pr_fil_ek,3,0);
    out_quest ((char *) &we_jour.lager,2,0);
    out_quest ((char *) we_jour.ls_ident,0,21);
    out_quest ((char *) we_jour.ls_charge,0,21);
    out_quest ((char *) &we_jour.pr_ek_euro,3,0);
    out_quest ((char *) &we_jour.pr_ek_nto_eu,3,0);
    out_quest ((char *) &we_jour.pr_vk_eu,3,0);
    out_quest ((char *) &we_jour.rab_eff_eu,3,0);
    out_quest ((char *) &we_jour.pr_fil_ek_eu,3,0);
    out_quest ((char *) &we_jour.best_me_einh,1,0);
            cursor = prepare_sql ("select we_jour.a,  "
"we_jour.abw_lief_proz,  we_jour.abw_rab_betr,  we_jour.abw_rab_nat,  "
"we_jour.abw_rab_proz,  we_jour.abw_zusch_proz,  we_jour.akv,  "
"we_jour.alarm_prio,  we_jour.anz_einh,  we_jour.bearb,  "
"we_jour.best_blg,  we_jour.best_me,  we_jour.best_zuord_kz,  "
"we_jour.blg_typ,  we_jour.diff_kz,  we_jour.fil,  we_jour.hbk_dat,  "
"we_jour.inh,  we_jour.int_pos,  we_jour.lief,  we_jour.lief_best,  "
"we_jour.lief_rech_nr,  we_jour.lief_s,  we_jour.mdn,  we_jour.me,  "
"we_jour.me_einh,  we_jour.mwst,  we_jour.p_num,  we_jour.pers_nam,  "
"we_jour.pr_aend_kz,  we_jour.pr_ek,  we_jour.pr_ek_nto,  "
"we_jour.pr_vk,  we_jour.qua_kz,  we_jour.rab_eff,  we_jour.rab_kz,  "
"we_jour.sa_kz,  we_jour.skto_kz,  we_jour.tara,  we_jour.tara_proz,  "
"we_jour.we_txt,  we_jour.sti_pro_kz,  we_jour.me_kz,  we_jour.a_krz,  "
"we_jour.we_kto,  we_jour.buch_kz,  we_jour.pr_fil_ek,  we_jour.lager,  "
"we_jour.ls_ident,  we_jour.ls_charge,  we_jour.pr_ek_euro,  "
"we_jour.pr_ek_nto_eu,  we_jour.pr_vk_eu,  we_jour.rab_eff_eu,  "
"we_jour.pr_fil_ek_eu,  we_jour.best_me_einh from we_jour "

#line 32 "we_jour.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   lief_rech_nr = ? "
                                  "and   lief = ? "
                                  "and   a = ? "
                                  "and   blg_typ = ? "
                                  "and   int_pos = ?");

    ins_quest ((char *) &we_jour.a,3,0);
    ins_quest ((char *) &we_jour.abw_lief_proz,3,0);
    ins_quest ((char *) &we_jour.abw_rab_betr,3,0);
    ins_quest ((char *) &we_jour.abw_rab_nat,3,0);
    ins_quest ((char *) &we_jour.abw_rab_proz,3,0);
    ins_quest ((char *) &we_jour.abw_zusch_proz,3,0);
    ins_quest ((char *) &we_jour.akv,2,0);
    ins_quest ((char *) &we_jour.alarm_prio,2,0);
    ins_quest ((char *) &we_jour.anz_einh,3,0);
    ins_quest ((char *) &we_jour.bearb,2,0);
    ins_quest ((char *) &we_jour.best_blg,2,0);
    ins_quest ((char *) &we_jour.best_me,3,0);
    ins_quest ((char *) we_jour.best_zuord_kz,0,2);
    ins_quest ((char *) we_jour.blg_typ,0,2);
    ins_quest ((char *) we_jour.diff_kz,0,2);
    ins_quest ((char *) &we_jour.fil,1,0);
    ins_quest ((char *) &we_jour.hbk_dat,2,0);
    ins_quest ((char *) &we_jour.inh,3,0);
    ins_quest ((char *) &we_jour.int_pos,2,0);
    ins_quest ((char *) we_jour.lief,0,17);
    ins_quest ((char *) we_jour.lief_best,0,17);
    ins_quest ((char *) we_jour.lief_rech_nr,0,17);
    ins_quest ((char *) &we_jour.lief_s,2,0);
    ins_quest ((char *) &we_jour.mdn,1,0);
    ins_quest ((char *) &we_jour.me,3,0);
    ins_quest ((char *) &we_jour.me_einh,1,0);
    ins_quest ((char *) &we_jour.mwst,1,0);
    ins_quest ((char *) &we_jour.p_num,1,0);
    ins_quest ((char *) we_jour.pers_nam,0,9);
    ins_quest ((char *) we_jour.pr_aend_kz,0,2);
    ins_quest ((char *) &we_jour.pr_ek,3,0);
    ins_quest ((char *) &we_jour.pr_ek_nto,3,0);
    ins_quest ((char *) &we_jour.pr_vk,3,0);
    ins_quest ((char *) we_jour.qua_kz,0,2);
    ins_quest ((char *) &we_jour.rab_eff,3,0);
    ins_quest ((char *) we_jour.rab_kz,0,2);
    ins_quest ((char *) we_jour.sa_kz,0,2);
    ins_quest ((char *) we_jour.skto_kz,0,2);
    ins_quest ((char *) &we_jour.tara,3,0);
    ins_quest ((char *) &we_jour.tara_proz,3,0);
    ins_quest ((char *) &we_jour.we_txt,2,0);
    ins_quest ((char *) we_jour.sti_pro_kz,0,2);
    ins_quest ((char *) we_jour.me_kz,0,2);
    ins_quest ((char *) &we_jour.a_krz,2,0);
    ins_quest ((char *) &we_jour.we_kto,2,0);
    ins_quest ((char *) &we_jour.buch_kz,1,0);
    ins_quest ((char *) &we_jour.pr_fil_ek,3,0);
    ins_quest ((char *) &we_jour.lager,2,0);
    ins_quest ((char *) we_jour.ls_ident,0,21);
    ins_quest ((char *) we_jour.ls_charge,0,21);
    ins_quest ((char *) &we_jour.pr_ek_euro,3,0);
    ins_quest ((char *) &we_jour.pr_ek_nto_eu,3,0);
    ins_quest ((char *) &we_jour.pr_vk_eu,3,0);
    ins_quest ((char *) &we_jour.rab_eff_eu,3,0);
    ins_quest ((char *) &we_jour.pr_fil_ek_eu,3,0);
    ins_quest ((char *) &we_jour.best_me_einh,1,0);
            sqltext = "update we_jour set we_jour.a = ?,  "
"we_jour.abw_lief_proz = ?,  we_jour.abw_rab_betr = ?,  "
"we_jour.abw_rab_nat = ?,  we_jour.abw_rab_proz = ?,  "
"we_jour.abw_zusch_proz = ?,  we_jour.akv = ?,  "
"we_jour.alarm_prio = ?,  we_jour.anz_einh = ?,  we_jour.bearb = ?,  "
"we_jour.best_blg = ?,  we_jour.best_me = ?,  "
"we_jour.best_zuord_kz = ?,  we_jour.blg_typ = ?,  "
"we_jour.diff_kz = ?,  we_jour.fil = ?,  we_jour.hbk_dat = ?,  "
"we_jour.inh = ?,  we_jour.int_pos = ?,  we_jour.lief = ?,  "
"we_jour.lief_best = ?,  we_jour.lief_rech_nr = ?,  "
"we_jour.lief_s = ?,  we_jour.mdn = ?,  we_jour.me = ?,  "
"we_jour.me_einh = ?,  we_jour.mwst = ?,  we_jour.p_num = ?,  "
"we_jour.pers_nam = ?,  we_jour.pr_aend_kz = ?,  we_jour.pr_ek = ?,  "
"we_jour.pr_ek_nto = ?,  we_jour.pr_vk = ?,  we_jour.qua_kz = ?,  "
"we_jour.rab_eff = ?,  we_jour.rab_kz = ?,  we_jour.sa_kz = ?,  "
"we_jour.skto_kz = ?,  we_jour.tara = ?,  we_jour.tara_proz = ?,  "
"we_jour.we_txt = ?,  we_jour.sti_pro_kz = ?,  we_jour.me_kz = ?,  "
"we_jour.a_krz = ?,  we_jour.we_kto = ?,  we_jour.buch_kz = ?,  "
"we_jour.pr_fil_ek = ?,  we_jour.lager = ?,  we_jour.ls_ident = ?,  "
"we_jour.ls_charge = ?,  we_jour.pr_ek_euro = ?,  "
"we_jour.pr_ek_nto_eu = ?,  we_jour.pr_vk_eu = ?,  "
"we_jour.rab_eff_eu = ?,  we_jour.pr_fil_ek_eu = ?,  "
"we_jour.best_me_einh = ? "

#line 41 "we_jour.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   lief_rech_nr = ? "
                                  "and   lief = ? "
                                  "and   a = ? "
                                  "and   blg_typ = ? "
                                  "and   int_pos = ?"; 


            ins_quest ((char *) &we_jour.mdn, 1, 0);
            ins_quest ((char *) &we_jour.fil, 1, 0);
            ins_quest ((char *) &we_jour.lief_rech_nr, 0, 17);
            ins_quest ((char *) &we_jour.lief, 0, 17);
            ins_quest ((char *) &we_jour.a, 2, 0);
            ins_quest ((char *) &we_jour.blg_typ, 0, 2);
            ins_quest ((char *) &we_jour.int_pos, 2, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &we_jour.mdn, 1, 0);
            ins_quest ((char *) &we_jour.fil, 1, 0);
            ins_quest ((char *) &we_jour.lief_rech_nr, 0, 17);
            ins_quest ((char *) &we_jour.lief, 0, 17);
            ins_quest ((char *) &we_jour.a, 3, 0);
            ins_quest ((char *) &we_jour.blg_typ, 0, 2);
            ins_quest ((char *) &we_jour.int_pos, 2, 0);
            test_upd_cursor = prepare_sql ("select a from we_jour "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   lief_rech_nr = ? "
                                  "and   lief = ? "
                                  "and   a = ? "
                                  "and   blg_typ = ? "
                                  "and int_pos = ?");
            ins_quest ((char *) &we_jour.mdn, 1, 0);
            ins_quest ((char *) &we_jour.fil, 1, 0);
            ins_quest ((char *) &we_jour.lief_rech_nr, 0, 17);
            ins_quest ((char *) &we_jour.lief, 0, 17);
            ins_quest ((char *) &we_jour.a, 3, 0);
            ins_quest ((char *) &we_jour.blg_typ, 0, 2);
            ins_quest ((char *) &we_jour.int_pos, 2, 0);
            del_cursor = prepare_sql ("delete from we_jour "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   lief_rech_nr = ? "
                                  "and   lief = ? "
                                  "and   a = ? "
                                  "and   blg_typ = ? "
                                  "and   int_pos = ?");
    ins_quest ((char *) &we_jour.a,3,0);
    ins_quest ((char *) &we_jour.abw_lief_proz,3,0);
    ins_quest ((char *) &we_jour.abw_rab_betr,3,0);
    ins_quest ((char *) &we_jour.abw_rab_nat,3,0);
    ins_quest ((char *) &we_jour.abw_rab_proz,3,0);
    ins_quest ((char *) &we_jour.abw_zusch_proz,3,0);
    ins_quest ((char *) &we_jour.akv,2,0);
    ins_quest ((char *) &we_jour.alarm_prio,2,0);
    ins_quest ((char *) &we_jour.anz_einh,3,0);
    ins_quest ((char *) &we_jour.bearb,2,0);
    ins_quest ((char *) &we_jour.best_blg,2,0);
    ins_quest ((char *) &we_jour.best_me,3,0);
    ins_quest ((char *) we_jour.best_zuord_kz,0,2);
    ins_quest ((char *) we_jour.blg_typ,0,2);
    ins_quest ((char *) we_jour.diff_kz,0,2);
    ins_quest ((char *) &we_jour.fil,1,0);
    ins_quest ((char *) &we_jour.hbk_dat,2,0);
    ins_quest ((char *) &we_jour.inh,3,0);
    ins_quest ((char *) &we_jour.int_pos,2,0);
    ins_quest ((char *) we_jour.lief,0,17);
    ins_quest ((char *) we_jour.lief_best,0,17);
    ins_quest ((char *) we_jour.lief_rech_nr,0,17);
    ins_quest ((char *) &we_jour.lief_s,2,0);
    ins_quest ((char *) &we_jour.mdn,1,0);
    ins_quest ((char *) &we_jour.me,3,0);
    ins_quest ((char *) &we_jour.me_einh,1,0);
    ins_quest ((char *) &we_jour.mwst,1,0);
    ins_quest ((char *) &we_jour.p_num,1,0);
    ins_quest ((char *) we_jour.pers_nam,0,9);
    ins_quest ((char *) we_jour.pr_aend_kz,0,2);
    ins_quest ((char *) &we_jour.pr_ek,3,0);
    ins_quest ((char *) &we_jour.pr_ek_nto,3,0);
    ins_quest ((char *) &we_jour.pr_vk,3,0);
    ins_quest ((char *) we_jour.qua_kz,0,2);
    ins_quest ((char *) &we_jour.rab_eff,3,0);
    ins_quest ((char *) we_jour.rab_kz,0,2);
    ins_quest ((char *) we_jour.sa_kz,0,2);
    ins_quest ((char *) we_jour.skto_kz,0,2);
    ins_quest ((char *) &we_jour.tara,3,0);
    ins_quest ((char *) &we_jour.tara_proz,3,0);
    ins_quest ((char *) &we_jour.we_txt,2,0);
    ins_quest ((char *) we_jour.sti_pro_kz,0,2);
    ins_quest ((char *) we_jour.me_kz,0,2);
    ins_quest ((char *) &we_jour.a_krz,2,0);
    ins_quest ((char *) &we_jour.we_kto,2,0);
    ins_quest ((char *) &we_jour.buch_kz,1,0);
    ins_quest ((char *) &we_jour.pr_fil_ek,3,0);
    ins_quest ((char *) &we_jour.lager,2,0);
    ins_quest ((char *) we_jour.ls_ident,0,21);
    ins_quest ((char *) we_jour.ls_charge,0,21);
    ins_quest ((char *) &we_jour.pr_ek_euro,3,0);
    ins_quest ((char *) &we_jour.pr_ek_nto_eu,3,0);
    ins_quest ((char *) &we_jour.pr_vk_eu,3,0);
    ins_quest ((char *) &we_jour.rab_eff_eu,3,0);
    ins_quest ((char *) &we_jour.pr_fil_ek_eu,3,0);
    ins_quest ((char *) &we_jour.best_me_einh,1,0);
            ins_cursor = prepare_sql ("insert into we_jour ("
"a,  abw_lief_proz,  abw_rab_betr,  abw_rab_nat,  abw_rab_proz,  "
"abw_zusch_proz,  akv,  alarm_prio,  anz_einh,  bearb,  best_blg,  best_me,  "
"best_zuord_kz,  blg_typ,  diff_kz,  fil,  hbk_dat,  inh,  int_pos,  lief,  lief_best,  "
"lief_rech_nr,  lief_s,  mdn,  me,  me_einh,  mwst,  p_num,  pers_nam,  pr_aend_kz,  "
"pr_ek,  pr_ek_nto,  pr_vk,  qua_kz,  rab_eff,  rab_kz,  sa_kz,  skto_kz,  tara,  "
"tara_proz,  we_txt,  sti_pro_kz,  me_kz,  a_krz,  we_kto,  buch_kz,  pr_fil_ek,  "
"lager,  ls_ident,  ls_charge,  pr_ek_euro,  pr_ek_nto_eu,  pr_vk_eu,  "
"rab_eff_eu,  pr_fil_ek_eu,  best_me_einh) "

#line 90 "we_jour.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"); 

#line 92 "we_jour.rpp"
}
int WE_JOUR_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

