#ifndef _FIL_INVGES_DEF
#define _FIL_INVGES_DEF
//#include "searchbest.h"
#include "dbclass.h"

struct FIL_INVGES {
   short     delstatus;
   long      dat;
   short     fil;
   short     mdn;
   double    ums_ek_bew;
   double    ums_vk_bew;
};
extern struct FIL_INVGES fil_invges, fil_invges_null;

#line 7 "fil_invges.rh"

class FIL_INVGES_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
               static short cursor_ausw;
               char beststring0[0x1000];
               int bestchange;
  //             static SEARCHBEST *SearchBest;
       public :
               FIL_INVGES_CLASS () : DB_CLASS ()
               {
                    strcpy (beststring0, "Start");
               }
               int dbreadfirst (void);
               void ListToMamain1 (HWND);
//               int ShowBuBestQuery (HWND, int);
//               int PrepareBestQuery (form *, char **);

  //             int ShowBuBestQueryEx (HWND, int);
//               int ShowBestLbox (HWND, int, char *);
//               int PrepareBestQueryEx (form *, char **);
//               static int ReadLst (char *);
};
#endif
