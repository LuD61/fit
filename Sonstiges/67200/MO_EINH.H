struct KE
{
          short me_einh_lief;
          char  me_einh_lief_bez [20];
          double inh;
          double pr_ek;
          short me_einh_bas;
          char  me_einh_bas_bez [20];
          short me_einh1;
          double inh1;
          short me_einh2;
          double inh2;         
          short me_einh3;
          double inh3;
};

typedef struct KE KEINHEIT;


class EINH_CLASS
{
           private :
                int dsqlstatus;
                int BestEinh;
           public :
                EINH_CLASS () : BestEinh (1)
                {
                }
                int GetBestEinh (void)
                {
                    return BestEinh;
                }
                void SetBestEinh (int einh)
                {
                    BestEinh = einh;
                }
                void AktBestEinh (short, short, char *, double, int);
                void NextBestEinh (short,short,char *,double,int, 
                                  KEINHEIT *);
                double GetInh (int, double);
                void FillGeb0 (KEINHEIT *, int);
                void FillGeb1P (KEINHEIT *, int);
                void FillGeb1M (KEINHEIT *, int);
                void FillGeb2 (KEINHEIT *, int);
                void FillGeb (KEINHEIT *);
                void FillKmb (KEINHEIT *);
                int  ReadKmb (void);
                void GetLiefEinh (short, short, char *, double, KEINHEIT *);
                void GetBasEinh (double, KEINHEIT *);
                void GetLiefEinhBas (double, KEINHEIT *);
                void FillOtab (int);
                void InitCombo (void);
                void ChoiseCombo (void);
                void ChoiseEinh (void);
};
