#ifndef _SEARCHA_DEF
#define _SEARCHA_DEF
#include "wmaskc.h"
#include "cmask.h"
#include "mo_chqex.h"
#include "dbclass.h"

struct SA
{
	  char a [14];
	  char a_bz1 [25];
	  char a_bz2 [25];
};

class SEARCHA
{
       private :
           static HINSTANCE hMainInst;
           static HWND hMainWindow;
           static DB_CLASS DbClass; 
           static HWND awin;
           static struct SA *satab;
           static struct SA sa;
           static int idx;
           static long aanz;
           static CHQEX *Query;
           static int SearchField;
           static int soa; 
           static int soa_bz1; 
           static int soa_bz2; 

           int SearchPos;
           int OKPos;
           int CAPos;
           DWORD WindowStyle;
           BOOL SetStyle;
        public :
           SEARCHA ()
           {
                  SearchPos = 8;
                  OKPos = 9;
                  CAPos = 10;
                  SetStyle = FALSE;
                  WindowStyle = WS_POPUP | WS_THICKFRAME;
           }

           ~SEARCHA ()
           {
                  if (satab != NULL)
                  {
                      delete satab;
                      satab = NULL;
                  }
           }

           SA *GetSa (void)
           {
               if (idx == -1) return NULL;
               return &sa;
           }

           void Setawin (HWND awin)
           {
               this->awin = awin;
           }

           void SetWindowStyle (DWORD style)
           {
               WindowStyle = style;
               SetStyle = TRUE;
           }


           void FillBox (void);
           SA *GetNextSa (void);
           SA *GetPriorSa (void);
           SA *GetFirstSa (void);
           SA *GetLastSa (void);
           static void FillFormat (char *);
           static void FillVlines (char *);
           static void FillCaption (char *);
           static void FillRec (char *, int);
           static int sorta (const void *, const void *);
           static void SortA (HWND);
           static int sorta_bz1 (const void *, const void *);
           static void SortA_bz1 (HWND);
           static int sorta_bz2 (const void *, const void *);
           static void SortA_bz2 (HWND);
           static void SortLst (int, HWND);
           static void UpdateList (void);

           static int SearchLst (char *);
           static int ReadA (char *);
           void SetParams (HINSTANCE, HWND);
           void SearchA (void);
};  
#endif