#include <windows.h>
#include "searchfilinv.h"
#include "strfkt.h"
#include "mo_wmess.h"
#include "lbox.h"


int SEARCHFILINV::idx;
long SEARCHFILINV::anz;
CHQEX *SEARCHFILINV::Query = NULL;
DB_CLASS SEARCHFILINV::DbClass; 
HINSTANCE SEARCHFILINV::hMainInst;
HWND SEARCHFILINV::hMainWindow;
HWND SEARCHFILINV::awin;


int SEARCHFILINV::SearchLst (char *sebuff)
/**
Nach Kurzname in Liste suchen.
**/
{
	   Query->SearchList ();
	   return 0;
}

int SEARCHFILINV::ReadLst (char *name)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int cursor;
	  int i;
      WMESS Wmess;
      short mdn_nr;
      long ldat;
	  double wert;
  	  char* invdat = "";


	  anz = 0;
      clipped (name);
      if (strcmp (name, " ") == 0) name[0] = 0;

      if (strlen (name) == 0 ||
          name[0] <= ' ')
      {
	          sprintf (buffer, "select mdn,dat,ums_vk_bew "
	 			       "from fil_invges "
                       "order by mdn,dat desc");
      }
      else
      {
	          sprintf (buffer, "select mdn,dat,ums_vk_bew "
	 			       "from fil_invges "
                       "order by mdn,dat desc");
      }
      DbClass.sqlout ((short *) &mdn_nr, 1, 0);
      DbClass.sqlout ((long *) &ldat, 2, 0);
      DbClass.sqlout ((double *) &wert, 3, 0);
      cursor = DbClass.sqlcursor (buffer);
      if (cursor < 0) 
      {
          return -1;
      }
  	  i = 0;
      Wmess.Message (hMainInst, hMainWindow, " \nBitte warten.....\n"
  	                                         "Die Daten werden selektiert.\n ");  
	  while (DbClass.sqlfetch (cursor) == 0)
      {

	      dlong_to_asc (ldat, invdat);
 	      sprintf (buffer, "    %4d  |%-12s| %12.2lf", mdn_nr, invdat, wert);
	      Query->InsertRecord (buffer);
		  i ++;
      }
      anz = i;
	  DbClass.sqlclose (cursor);
 	  Wmess.Destroy ();
	  EnableWindow (awin, FALSE);
	  return 0;
}

void SEARCHFILINV::SetParams (HINSTANCE hMainInst, HWND hMainWindow)
{
      this->hMainInst   = hMainInst;
      this->hMainWindow = hMainWindow;
      awin = hMainWindow;
}

BOOL SEARCHFILINV::GetKey (char * key)
{
      int anz;
     
      if (idx == -1) return FALSE;
      anz = wsplit (Key, "|");
      if (anz < 1) return FALSE;
      strcpy (key, wort[1]);
      return TRUE;
}


void SEARCHFILINV::Search (void)
{
  	  int cx, cy;
	  char buffer [256];

	  idx = -1;
      cx = 48;
      cy = 25;
      Settchar ('|');
      EnableWindow (hMainWindow, FALSE);
      Query = new CHQEX (cx, cy);
      Query->EnableSort (TRUE);
      Query->OpenWindow (hMainInst, hMainWindow);
      sprintf (buffer, "%s %s",
                          "%d",
                          "%s");
      Query->RowAttr (buffer);
	  sprintf (buffer, "    %6s", "1"); 
	  Query->VLines (buffer, 3);
      sprintf (buffer, "4;4 11;16");
      Query->RowPos (buffer);
	  EnableWindow (awin, FALSE);

      sprintf (buffer, "    %4s  |%-12s| %-14s", "Mdn", "Datum", "Wert");
	  Query->InsertCaption (buffer);

	  Query->SetFillDb (ReadLst);
	  Query->SetSearchLst (SearchLst);
      ReadLst ("");
//      Query->SetSortRow (1, TRUE);
	  Query->ProcessMessages ();
      EnableWindow (hMainWindow, TRUE);
      if (syskey != KEY5)
      {
              idx = Query->GetSel ();
              if (idx != -1)
              {
                     Query->GetText (Key);
              }
      }
	  EnableWindow (awin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (awin);
}

