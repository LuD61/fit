#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include "comcthlp.h"
#include "strfkt.h"
#include "stdfkt.h"
#include "wmaskc.h"
#include "colbut.h"
#include "dbclass.h"
#include "cmask.h"
#include "mo_ch.h"
#include "lbox.h"

#define BuOk      1001
#define BuCancel  1002
#define LIBOX     1003
#define BuDial    1004
#define BuAll     1005


static mfont mFont = {
                     "MS Sans Serif", 
                      100, 
                      0, 
                      0, 
                      BLACKCOL, 
                      LTGRAYCOL,
                      0, 0};


static CFIELD **_fListe;
static CFORM *fListe;

static CFIELD **_fButton;
static CFORM *fButton;

static CFIELD **_fWork;
static CFORM *fWork;

int (*CH::CaFunc) (int) = NULL;
int (*CH::OkFunc) (int) = NULL;
int (*CH::DialFunc) (int) = NULL;
int (*CH::SpezFunc) (int) = NULL;

CH::CH (int cx, int cy, char *Bu1, char *Bu2, char *Bu3)
{
			TEXTMETRIC tm;
			HFONT hFont, oldfont;
			SIZE size;
			HDC hdc;
			int x,y;
			char *BuText1;
			char *BuText2;
			char *BuText3;

			vkidx = 0;
			imgidx = 0;
			Images[0] = NULL;
			BuText1 = "W�hlen";
			BuText2 = "OK";
			BuText3 = "Abbruch";
			if (Bu1) BuText1 = Bu1;
			if (Bu2) BuText2 = Bu2;
			if (Bu3) BuText3 = Bu3;
 	        this->Font = &mFont;  
			
			hdc = GetDC (NULL);
            hFont = SetDeviceFont (hdc, Font, &tm);
            oldfont = SelectObject (hdc, hFont);
	        GetTextMetrics (hdc, &tm);
			GetTextExtentPoint32 (hdc, "X", 1, &size);
			DeleteObject (SelectObject (hdc, oldfont));
	

			this->cx = size.cx * cx;
			this->cy = tm.tmHeight * cy;

            _fListe    = new CFIELD * [1];
			_fListe[0] = new CFIELD ("Liste", "Liste",
				                     cx - 2, cy - 4, 1, 1, NULL, "", CLISTBOX,
												 LIBOX, Font, 0, 0);

			fListe     = new CFORM (1, _fListe);
			fListe->Setchsize (2, 2);

			x = 0;
			y = (cy - 2) * tm.tmHeight;
			cx = 10 * size.cx;
			cy = (int) (double) ((double) tm.tmHeight * 1.3);
			
			_fButton    = new CFIELD * [3];
			_fButton[0] = new CFIELD ("Dial", BuText1, cx, cy, x, y, 
				                                  NULL, "", CBUTTON,
                                                  BuDial, Font, 1, BS_PUSHBUTTON);				                                   
			_fButton[1] = new CFIELD ("OK", BuText2, cx, cy, x + 12 * size.cx, y, 
				                                  NULL, "", CBUTTON,
                                                  BuOk, Font, 1, BS_PUSHBUTTON);				                                   
 
			_fButton[2] = new CFIELD ("Cancel", BuText3, 
				                                  cx , cy, x + 24 * size.cx, y,
				                                  NULL, "", CBUTTON,
                                                  BuCancel, Font, 1, BS_PUSHBUTTON);				    
			fButton     = new CFORM (3, _fButton);

            _fWork    = new CFIELD * [2];
			_fWork[0] = new CFIELD ("fListe", fListe, 0, 0, 0, 0, NULL, "", 
				                                 CFFORM,
				                                 980, Font, 0, 0);
			_fWork[1] = new CFIELD ("Button", fButton, 0, 0, -1, -2, NULL, "", CFFORM,
				                                 981, Font, 0, 0);

			fWork   = new CFORM (2, _fWork);
}

CH::CH (int cx, int cy, char *Bu1, char *Bu2, char *Bu3, char *Bu4)
{
			TEXTMETRIC tm;
			HFONT hFont, oldfont;
			SIZE size;
			HDC hdc;
			int x,y;
			char *BuText1;
			char *BuText2;
			char *BuText3;
			char *BuText4;

			vkidx = 0;
			imgidx = 0;
			Images[0] = NULL;
			BuText1 = "Alle";
			BuText2 = "W�hlen";
			BuText3 = "OK";
			BuText4 = "Abbruch";
			if (Bu1) BuText1 = Bu1;
			if (Bu2) BuText2 = Bu2;
			if (Bu3) BuText3 = Bu3;
			if (Bu4) BuText4 = Bu4;
 	        this->Font = &mFont;  

			hdc = GetDC (NULL);
            hFont = SetDeviceFont (hdc, Font, &tm);
            oldfont = SelectObject (hdc, hFont);
	        GetTextMetrics (hdc, &tm);
			GetTextExtentPoint32 (hdc, "X", 1, &size);
			DeleteObject (SelectObject (hdc, oldfont));
	

			this->cx = size.cx * cx;
			this->cy = tm.tmHeight * cy;

            _fListe    = new CFIELD * [1];
			_fListe[0] = new CFIELD ("Liste", "Liste",
				                     cx - 2, cy - 4, 1, 1, NULL, "", CLISTBOX,
												 LIBOX, Font, 0, 0);

			fListe     = new CFORM (1, _fListe);
			fListe->Setchsize (2, 2);

			x = 0;
			y = (cy - 2) * tm.tmHeight;
			cx = 10 * size.cx;
			cy = (int) (double) ((double) tm.tmHeight * 1.3);
			
			_fButton    = new CFIELD * [4];
			_fButton[0] = new CFIELD ("All", BuText1, cx, cy, x, y, 
				                                  NULL, "", CBUTTON,
                                                  BuAll, Font, 1, BS_PUSHBUTTON);				                                   
			_fButton[1] = new CFIELD ("Dial", BuText2, cx, cy, x + 12 * size.cx, y, 
				                                  NULL, "", CBUTTON,
                                                  BuDial, Font, 1, BS_PUSHBUTTON);				                                   
			_fButton[2] = new CFIELD ("OK", BuText3, cx, cy, x + 24 * size.cx, y, 
				                                  NULL, "", CBUTTON,
                                                  BuOk, Font, 1, BS_PUSHBUTTON);				                                   
 
			_fButton[3] = new CFIELD ("Cancel", BuText4, 
				                                  cx , cy, x + 36 * size.cx, y,
				                                  NULL, "", CBUTTON,
                                                  BuCancel, Font, 1, BS_PUSHBUTTON);				    
			fButton     = new CFORM (4, _fButton);

            _fWork    = new CFIELD * [2];
			_fWork[0] = new CFIELD ("fListe", fListe, 0, 0, 0, 0, NULL, "", 
				                                 CFFORM,
				                                 980, Font, 0, 0);
			_fWork[1] = new CFIELD ("Button", fButton, 0, 0, -1, -2, NULL, "", CFFORM,
				                                 981, Font, 0, 0);

			fWork   = new CFORM (2, _fWork);
}


CH::~CH ()
{
	        int i;

	        fWork->destroy (); 

			if (fListe)
			{
				for (i = 0; i < fListe->GetFieldanz (); i ++)
				{
			              if (fListe->GetCfield ()[i]->GetAttribut () != CFFORM)
						  {
			                        delete fListe->GetCfield () [i];
						  }
				}
				delete fListe->GetCfield ();
		        delete fListe;
				fListe = NULL;
			}

			if (fButton)
			{
				for (i = 0; i < fButton->GetFieldanz (); i ++)
				{
			              if (fButton->GetCfield ()[i]->GetAttribut () != CFFORM)
						  {
			                        delete fButton->GetCfield () [i];
						  }
				}
				delete fButton->GetCfield ();
		        delete fButton;
				fButton = NULL;
			}

			if (fWork)
			{
				for (i = 0; i < fWork->GetFieldanz (); i ++)
				{
			              if (fWork->GetCfield ()[i]->GetAttribut () != CFFORM)
						  {
			                        delete fWork->GetCfield () [i];
						  }
				}
				delete fWork->GetCfield ();
		        delete fWork;
				fWork = NULL;
			}

            CaFunc   = NULL;
            OkFunc   = NULL;
            DialFunc = NULL;
            SpezFunc = NULL;
			for (i = 0; i < imgidx; i ++)
			{
				if (Images[i]) delete Images[i];
				Images[i] = NULL;
			}
}

void CH::SetCaFunc (int (*CaF) (int))
{
	       CaFunc = CaF;
}

void CH::SetOkFunc (int (*OkF) (int))
{
	       OkFunc = OkF;
}

void CH::SetDialFunc (int (*DialF) (int))
{
	       DialFunc = DialF;
}

void CH::SetSpezFunc (int (*SpezF) (int))
{
	       SpezFunc = SpezF;
}

void CH::AddAccelerator (int vkey, int BuNr, int (*funk) (void))
{
	        Vkeys[vkidx].SethWnd (hWnd);  
            Vkeys[vkidx].SetBuId ((DWORD) 0);
	        switch (BuNr)
			{
			case 1 :
	                Vkeys[vkidx].SetBuId (BuAll);
					break;
			case 2 :
	                Vkeys[vkidx].SetBuId (BuDial);
					break;
			case 3 :
	                Vkeys[vkidx].SetBuId (BuOk);
					break;
			case 4 :
	                Vkeys[vkidx].SetBuId (BuCancel);
					break;
			}
			Vkeys[vkidx].SetVkey (vkey);
			Vkeys[vkidx].SetFunk (funk);
			if (vkidx < 20) vkidx ++;
}

void CH::AddImage (HBITMAP ibmp, HBITMAP imsk)
{
	        if (imgidx == MAXIMAGE - 1) return;
			Images[imgidx] = new IMG (hWnd, ibmp, imsk);
			imgidx ++;
			Images[imgidx] = NULL;
		    fWork->SetImage (Images, LIBOX);
}

void CH::SetCurrentID (DWORD ID)
{
	       if (fWork->SetCurrentID (ID))
		   {
			   currentfield = ID;
		   }
}

void CH::SetCurrentName (char *name)
{
	       if (fWork->SetCurrentName (name))
		   {
			   currentfield = fWork->GetID (name);
		   }
}

void CH::EnableID (DWORD ID, BOOL flag)
{
	       fWork->EnableID (ID, flag);
}

void CH::EnableName (char *name, BOOL flag)
{
	       fWork->EnableName (name, flag);
}

void CH::CheckID (DWORD ID, BOOL flag)
{
	       fWork->CheckID (ID, flag);
}

void CH::CheckName (char *name, BOOL flag)
{
	       fWork->CheckName (name, flag);
}

BOOL CH::TestButtons (HWND hWndBu)
{
	       if (hWndBu == fWork->GethWndID (BuAll))
		   {
			   SendMessage (hWnd, WM_COMMAND, MAKELONG (BuAll, VK_RETURN),
				                                    (LPARAM) hWnd); 
			   return TRUE;
		   }
	       if (hWndBu == fWork->GethWndID (BuDial))
		   {
			   SendMessage (hWnd, WM_COMMAND, MAKELONG (BuDial, VK_RETURN),
				                                    (LPARAM) hWnd); 
			   return TRUE;
		   }
	       if (hWndBu == fWork->GethWndID (BuOk))
		   {
			   SendMessage (hWnd, WM_COMMAND, MAKELONG (BuOk, VK_RETURN),
				                                    (LPARAM) hWnd); 
			   return TRUE;
		   }
	       if (hWndBu == fWork->GethWndID (BuCancel))
		   {
			   SendMessage (hWnd, WM_COMMAND, MAKELONG (BuCancel, VK_RETURN),
				                                    (LPARAM) hWnd); 
			   return TRUE;
		   }
		   return FALSE;
}

BOOL CH::IsVKey (int vkey)
{
	      switch (vkey)
		  {
		         case VK_F1 :
		         case VK_F2 :
		         case VK_F3 :
		         case VK_F4 :
		         case VK_F5 :
		         case VK_F6 :
		         case VK_F7 :
		         case VK_F8 :
		         case VK_F9 :
		         case VK_F10 :
		         case VK_F11 :
		         case VK_F12 :
		         case VK_F13 :
		         case VK_F14 :
		         case VK_F15 :
		         case VK_F16 :
		         case VK_F17 :
		         case VK_F18 :
		         case VK_F19 :
		         case VK_F20 :
					 return TRUE;
		  }
		  return FALSE;
}
	       
BOOL CH::TestVKeys (int vkey)
{
	      int i;

		  if (IsVKey (vkey) == FALSE)
		  {
			  return FALSE;
		  }
		  for (i = 0; i < vkidx; i ++)
		  {
			  if (Vkeys[i].DoVkey (vkey)) return TRUE;
		  }
		  return FALSE;
}

void CH::ProcessMessages (void)
{
	      MSG msg;
		  HWND hWnd;

		  listpos = 0;
          hWnd = fWork->GethWndID (LIBOX);
		  fWork->SetCurrent (0);
		  fListe->GetCfield () [0]->SetFocus ();
          SendMessage (hWnd, LB_SETCURSEL, 0, 0L);
          while (GetMessage (&msg, NULL, 0, 0))
		  {
			  if (msg.message == WM_KEYDOWN)
			  {
			      if (msg.wParam == VK_TAB)
				  {

                      if (GetKeyState (VK_SHIFT) < 0)
                      {
                              syskey = KEYSTAB;
//                              fWork->PriorFormField (); 
                              fWork->PriorField (); 
					  }
					  else
					  {
					          syskey = KEYTAB;
//                              fWork->NextFormField (); 
                              fWork->NextField (); 
					  }
					  continue;
				  }

                  else if (TestVKeys (msg.wParam))
				  {
					       continue;
				  }

				  else if (msg.hwnd == fWork->GethWndName ("Liste"));
				  else if (msg.wParam == VK_DOWN)
				  {

					  syskey = KEYDOWN;
                      fWork->NextField (); 
					  continue;
				  }
				  else if (msg.wParam == VK_UP)
				  {

					  syskey = KEYUP;
                      fWork->PriorField (); 
					  continue;
				  }
				  else if (msg.wParam == VK_RETURN)
				  {

					  if (TestButtons (msg.hwnd))
					  {
					           continue;
					  }
				  }
			  }
              TranslateMessage(&msg);
			  DispatchMessage(&msg);
		  }
          listpos = SendMessage (hWnd, LB_GETCURSEL, 0, 0L);
}


CALLBACK CH::CProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        PAINTSTRUCT ps;
		HDC hdc;

        switch(msg)
        {
              case WM_PAINT :
                    hdc = BeginPaint (hWnd, &ps);
					fWork->display (hWnd, hdc);
                    EndPaint (hWnd, &ps);
					break;
              case WM_SIZE :
				    MoveWindow ();
					break;
              case WM_COMMAND :
                    if (LOWORD (wParam) == BuAll)
                    {
						   syskey = KEYCR;
						   if (SpezFunc)
						   {
							   (*SpezFunc) (GetSel ());
						   }
						   else
						   {
                                PostQuitMessage (0);
						   }
                    }
                    if (LOWORD (wParam) == BuOk)
                    {
						   syskey = KEYCR;
						   if (OkFunc)
						   {
							   (*OkFunc) (GetSel ());
						   }
						   else
						   {
                                PostQuitMessage (0);
						   }
                    }
                    if (LOWORD (wParam) == BuDial)
                    {
						   syskey = KEYCR;
						   if (DialFunc)
						   {
							   (*DialFunc) (GetSel ());
						   }
                    }
                    else if (LOWORD (wParam) == BuCancel)
                    {
						   syskey = KEY5;
                           PostQuitMessage (0);
                    }
                    else if (LOWORD (wParam) == LIBOX)
                    {
                           if (HIWORD (wParam) == LBN_DBLCLK)
                           {
						          syskey = KEYCR;
					 	          if (OkFunc)
								  {
							              (*OkFunc) (GetSel ());
								  }
						          else
								  {
                                           PostQuitMessage (0);
								  }
						   }
                           if (HIWORD (wParam) == VK_RETURN)
                           {
						          syskey = KEYCR;
					 	          if (OkFunc)
								  {
							              (*OkFunc) (GetSel ());
								  }
						          else
								  {
                                           PostQuitMessage (0);
								  }
						   }
                           if (HIWORD (wParam) == LBN_SELCHANGE)
                           {
		                          fWork->SetCurrent (0);
						   }
                    }
                    break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

void CH::InsertCaption (char *buffer)
{
	     HWND hWnd;

         hWnd = fWork->GethWndID (LIBOX);
		 
		 SendMessage (hWnd, LB_TITLE, -1,  
                                 (LPARAM) (char *) buffer);
}



void CH::InsertRecord (char *buffer)
{
	     HWND hWnd;

         hWnd = fWork->GethWndID (LIBOX);

         SendMessage (hWnd, LB_INSERTSTRING, -1,  
                                 (LPARAM) (char *) buffer);
}

void CH::UpdateRecord (char *buffer, int pos)
{
	     HWND hWnd;

         hWnd = fWork->GethWndID (LIBOX);

         SendMessage (hWnd, LB_INSERTSTRING, pos,  
                                 (LPARAM) (char *) buffer);
}

void CH::GetText (char *buffer)
{
	     HWND hWnd;
		 int idx;

         hWnd = fWork->GethWndID (LIBOX);

	     idx = SendMessage (hWnd, LB_GETCURSEL, 0, 0l);
	     SendMessage (hWnd, LB_GETTEXT, idx, (LPARAM) buffer);
}

int CH::GetSel (void)
{
	     HWND hWnd;
		 int idx;

         hWnd = fWork->GethWndID (LIBOX);

	     idx = SendMessage (hWnd, LB_GETCURSEL, 0, 0l);
		 return idx;
}

void CH::VLines (char *vpos, int hlines)
{
	       HWND hWnd;

		   hWnd = fWork->GethWndName ("Liste");
           SendMessage (hWnd, LB_CAPTSIZE, 0,  
                                 (LPARAM) 130);
           SendMessage (hWnd, LB_VPOS, hlines,  
                                 (LPARAM) (char *) vpos);
}

void CH::DestroyWindow (void)
{
	       fWork->destroy ();
		   ::DestroyWindow (hWnd);
}
						
void CH::OpenWindow (HANDLE hInstance, HWND hMainWindow)
{
		  int x, y;
		  BOOL registered = FALSE;
          WNDCLASS wc;
		  RECT rect, rect1;

		  this->hInstance   = hInstance;
		  this->hMainWindow = hMainWindow;
//		  SetColBorderM (TRUE);

		  if (registered == FALSE)
		  {
                  wc.style =  CS_BYTEALIGNWINDOW | CS_CLASSDC;
                  wc.lpfnWndProc   =  (WNDPROC) CProc;
                  wc.cbClsExtra    =  0;
                  wc.cbWndExtra    =  0;
                  wc.hInstance     =  hInstance;
                  wc.hIcon         =  LoadIcon (hInstance, "FITICON");
                  wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                  wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
                  wc.lpszMenuName  =  "";
                  wc.lpszClassName =  "CChoiseWind";

                  RegisterClass(&wc);
				  registered = TRUE;
		  }

		  GetClientRect (hMainWindow, &rect);
		  GetWindowRect (hMainWindow, &rect1);
/*
		  cx = rect.right;
		  cy = rect.bottom;
*/

		  x  = max (0, (rect.right - cx) / 2)  + rect1.left;
		  y  = max (0, (rect.bottom - cy) / 2) + rect1.top;

          hWnd = CreateWindowEx (0, 
                                 "CChoiseWind",
                                 "",
                                 WS_VISIBLE | WS_POPUP | WS_THICKFRAME,
                                 x, y,
                                 cx, cy,
                                 hMainWindow,
                                 NULL,
                                 hInstance,
                                 NULL);
		  ShowWindow (hWnd, SW_SHOWNORMAL);
		  UpdateWindow (hWnd);
}

void CH::MoveWindow (void)
{
/*
		  RECT rect;

		  GetClientRect (hMainWindow, &rect);
          ::MoveWindow (hWnd, rect.left, rect.top, rect.right, rect.bottom, TRUE);
*/
	      fWork->MoveWindow ();
		  fButton->Invalidate (FALSE);
}

